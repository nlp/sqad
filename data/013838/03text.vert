<s>
Alžběta	Alžběta	k1gFnSc1
Ruská	ruský	k2eAgFnSc1d1
</s>
<s>
O	o	k7c6
ruské	ruský	k2eAgFnSc6d1
carevně	carevna	k1gFnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Alžběta	Alžběta	k1gFnSc1
I.	I.	kA
Petrovna	Petrovna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
Alžběta	Alžběta	k1gFnSc1
Ruská	ruský	k2eAgFnSc1d1
Alžběta	Alžběta	k1gFnSc1
v	v	k7c6
řeholním	řeholní	k2eAgInSc6d1
šatěMučednice	šatěMučednice	k1gFnSc2
Datum	datum	k1gNnSc4
narození	narození	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1864	#num#	k4
Datum	datum	k1gNnSc4
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1918	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
53	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
Alžběta	Alžběta	k1gFnSc1
Ruská	ruský	k2eAgFnSc1d1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Alžběta	Alžběta	k1gFnSc1
Hesensko-Darmstadtská	Hesensko-Darmstadtský	k2eAgFnSc1d1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1864	#num#	k4
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1918	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
lomu	lom	k1gInSc6
nedaleko	nedaleko	k7c2
Alapajevska	Alapajevsko	k1gNnSc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
původem	původ	k1gInSc7
hesenská	hesenský	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
<g/>
,	,	kIx,
sňatkem	sňatek	k1gInSc7
ruská	ruský	k2eAgFnSc1d1
velkokněžna	velkokněžna	k1gFnSc1
a	a	k8xC
posléze	posléze	k6eAd1
řeholnice	řeholnice	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
a	a	k8xC
mučednickou	mučednický	k2eAgFnSc4d1
smrt	smrt	k1gFnSc4
od	od	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
kanonizována	kanonizován	k2eAgFnSc1d1
ruskou	ruský	k2eAgFnSc7d1
pravoslavnou	pravoslavný	k2eAgFnSc7d1
církví	církev	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Původ	původ	k1gInSc1
<g/>
,	,	kIx,
dětství	dětství	k1gNnSc1
a	a	k8xC
mládí	mládí	k1gNnSc1
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	se	k3xPyFc4
jako	jako	k9
Elisabeth	Elisabeth	k1gInSc1
Alexandra	Alexandr	k1gMnSc2
Luise	Luisa	k1gFnSc6
Alice	Alice	k1gFnSc1
von	von	k1gInSc4
Hessen-Darmstadt	Hessen-Darmstadt	k1gInSc1
und	und	k?
bei	bei	k?
Rhein	Rhein	k1gInSc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
druhá	druhý	k4xOgFnSc1
dcera	dcera	k1gFnSc1
hesenského	hesenský	k2eAgMnSc2d1
vévody	vévoda	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
manželky	manželka	k1gFnPc1
Alice	Alice	k1gFnSc2
<g/>
,	,	kIx,
dcery	dcera	k1gFnSc2
britské	britský	k2eAgFnSc2d1
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
matkou	matka	k1gFnSc7
vychovávána	vychováván	k2eAgFnSc1d1
v	v	k7c6
hluboké	hluboký	k2eAgFnSc6d1
víře	víra	k1gFnSc6
v	v	k7c4
Boha	bůh	k1gMnSc4
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
ní	on	k3xPp3gFnSc7
se	se	k3xPyFc4
účastnila	účastnit	k5eAaImAgFnS
dobročinných	dobročinný	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
jí	on	k3xPp3gFnSc3
bylo	být	k5eAaImAgNnS
čtrnáct	čtrnáct	k4xCc4
let	rok	k1gInPc2
<g/>
,	,	kIx,
její	její	k3xOp3gInSc4
matka	matka	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
<g/>
,	,	kIx,
nakazivši	nakazit	k5eAaPmDgFnS
se	se	k3xPyFc4
tyfem	tyfus	k1gInSc7
od	od	k7c2
Alžbětiny	Alžbětin	k2eAgFnSc2d1
mladší	mladý	k2eAgFnSc2d2
sestry	sestra	k1gFnSc2
Alix	Alix	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
chvíle	chvíle	k1gFnSc2
byly	být	k5eAaImAgFnP
hesenské	hesenský	k2eAgFnPc1d1
princezny	princezna	k1gFnPc1
vychovávány	vychováván	k2eAgFnPc1d1
na	na	k7c6
britském	britský	k2eAgInSc6d1
královském	královský	k2eAgInSc6d1
dvoře	dvůr	k1gInSc6
u	u	k7c2
své	svůj	k3xOyFgFnSc2
babičky	babička	k1gFnSc2
<g/>
,	,	kIx,
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rodinném	rodinný	k2eAgInSc6d1
kruhu	kruh	k1gInSc6
ji	on	k3xPp3gFnSc4
nazývali	nazývat	k5eAaImAgMnP
Ella	Ellum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
pověstná	pověstný	k2eAgFnSc1d1
svou	svůj	k3xOyFgFnSc7
mimořádnou	mimořádný	k2eAgFnSc7d1
krásou	krása	k1gFnSc7
<g/>
,	,	kIx,
půvabem	půvab	k1gInSc7
a	a	k8xC
elegancí	elegance	k1gFnSc7
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
výrazné	výrazný	k2eAgNnSc4d1
hudební	hudební	k2eAgNnSc4d1
nadání	nadání	k1gNnSc4
<g/>
,	,	kIx,
ráda	rád	k2eAgFnSc1d1
a	a	k8xC
dobře	dobře	k6eAd1
kreslila	kreslit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německý	německý	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Vilém	Vilém	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
ucházel	ucházet	k5eAaImAgInS
o	o	k7c4
její	její	k3xOp3gFnSc4
ruku	ruka	k1gFnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
však	však	k9
jemně	jemně	k6eAd1
odmítnut	odmítnout	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikdy	nikdy	k6eAd1
na	na	k7c4
ni	on	k3xPp3gFnSc4
nemohl	moct	k5eNaImAgMnS
zapomenout	zapomenout	k5eAaPmF
a	a	k8xC
když	když	k8xS
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
perzekuci	perzekuce	k1gFnSc3
šlechty	šlechta	k1gFnSc2
spojené	spojený	k2eAgInPc4d1
s	s	k7c7
Romanovci	Romanovec	k1gMnPc7
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
,	,	kIx,
snažil	snažit	k5eAaImAgMnS
se	s	k7c7
všemi	všecek	k3xTgInPc7
prostředky	prostředek	k1gInPc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
opustila	opustit	k5eAaPmAgFnS
zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Velkokněžna	velkokněžna	k1gFnSc1
</s>
<s>
Velkokněžna	velkokněžna	k1gFnSc1
Jelizaveta	Jelizaveta	k1gFnSc1
Fjodorovna	Fjodorovna	k1gFnSc1
a	a	k8xC
velkokníže	velkokníže	k1gMnSc1
Sergej	Sergej	k1gMnSc1
Alexandrovič	Alexandrovič	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1903	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
roku	rok	k1gInSc2
1883	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Darmstadtu	Darmstadt	k1gInSc6
zasnoubila	zasnoubit	k5eAaPmAgFnS
s	s	k7c7
ruským	ruský	k2eAgMnSc7d1
velkoknížetem	velkokníže	k1gMnSc7
Sergejem	Sergej	k1gMnSc7
Alexandrovičem	Alexandrovič	k1gMnSc7
(	(	kIx(
<g/>
1857	#num#	k4
<g/>
–	–	k?
<g/>
1905	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pátým	pátý	k4xOgMnSc7
synem	syn	k1gMnSc7
cara	car	k1gMnSc2
Alexandra	Alexandr	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatba	svatba	k1gFnSc1
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
půl	půl	k1xP
roku	rok	k1gInSc2
nato	nato	k6eAd1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1884	#num#	k4
v	v	k7c6
chrámu	chrám	k1gInSc6
carského	carský	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
Zimního	zimní	k2eAgInSc2d1
paláce	palác	k1gInSc2
v	v	k7c6
Petrohradě	Petrohrad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželé	manžel	k1gMnPc1
se	se	k3xPyFc4
usadili	usadit	k5eAaPmAgMnP
v	v	k7c6
Sergejevském	Sergejevský	k2eAgInSc6d1
paláci	palác	k1gInSc6
na	na	k7c6
Něvském	něvský	k2eAgInSc6d1
prospektu	prospekt	k1gInSc6
v	v	k7c6
Petrohradě	Petrohrad	k1gInSc6
a	a	k8xC
Alžběta	Alžběta	k1gFnSc1
se	se	k3xPyFc4
výrazně	výrazně	k6eAd1
podílela	podílet	k5eAaImAgFnS
na	na	k7c6
společenském	společenský	k2eAgInSc6d1
životě	život	k1gInSc6
carského	carský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Velkokněžna	velkokněžna	k1gFnSc1
se	se	k3xPyFc4
z	z	k7c2
vlastní	vlastní	k2eAgFnSc2d1
iniciativy	iniciativa	k1gFnSc2
zajímala	zajímat	k5eAaImAgFnS
o	o	k7c6
historii	historie	k1gFnSc6
Ruska	Rusko	k1gNnSc2
a	a	k8xC
začala	začít	k5eAaPmAgFnS
se	se	k3xPyFc4
učit	učit	k5eAaImF
ruskému	ruský	k2eAgMnSc3d1
jazyku	jazyk	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebaže	třebaže	k8xS
nebyla	být	k5eNaImAgFnS
po	po	k7c6
právní	právní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
povinna	povinen	k2eAgFnSc1d1
konvertovat	konvertovat	k5eAaBmF
k	k	k7c3
pravoslaví	pravoslaví	k1gNnSc3
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1891	#num#	k4
toto	tento	k3xDgNnSc1
vyznání	vyznání	k1gNnSc1
dobrovolně	dobrovolně	k6eAd1
přijala	přijmout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jejích	její	k3xOp3gMnPc2
dopisů	dopis	k1gInPc2
otci	otec	k1gMnSc3
plyne	plynout	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
rozhodnutí	rozhodnutí	k1gNnSc1
navýsost	navýsost	k6eAd1
niterné	niterný	k2eAgNnSc1d1
a	a	k8xC
podepřené	podepřený	k2eAgNnSc1d1
hlubokou	hluboký	k2eAgFnSc7d1
reflexí	reflexe	k1gFnSc7
a	a	k8xC
modlitbou	modlitba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
té	ten	k3xDgFnSc6
příležitosti	příležitost	k1gFnSc6
přijala	přijmout	k5eAaPmAgFnS
jméno	jméno	k1gNnSc4
Alžběta	Alžběta	k1gFnSc1
(	(	kIx(
<g/>
Jelizaveta	Jelizaveta	k1gFnSc1
<g/>
)	)	kIx)
Fjodorovna	Fjodorovna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Fjodorovna	Fjodorovna	k1gFnSc1
</s>
<s>
Pár	pár	k4xCyI
neměl	mít	k5eNaImAgMnS
vlastní	vlastní	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
Alžběta	Alžběta	k1gFnSc1
a	a	k8xC
Sergej	Sergej	k1gMnSc1
však	však	k9
adoptovali	adoptovat	k5eAaPmAgMnP
velkoknížete	velkokníže	k1gMnSc4
Dmitrije	Dmitrije	k1gMnSc4
Pavloviče	Pavlovič	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
sestru	sestra	k1gFnSc4
<g/>
,	,	kIx,
velkokněžnu	velkokněžna	k1gFnSc4
Marii	Maria	k1gFnSc4
Pavlovnu	Pavlovna	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
jejich	jejich	k3xOp3gFnSc1
matka	matka	k1gFnSc1
<g/>
,	,	kIx,
řecká	řecký	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
Alexandra	Alexandra	k1gFnSc1
Georgijevna	Georgijevna	k1gFnSc1
(	(	kIx(
<g/>
dcera	dcera	k1gFnSc1
řeckého	řecký	k2eAgMnSc2d1
krále	král	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
I.	I.	kA
<g/>
)	)	kIx)
zemřela	zemřít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1891	#num#	k4
při	při	k7c6
předčasném	předčasný	k2eAgNnSc6d1
Dmitrijově	Dmitrijův	k2eAgNnSc6d1
narození	narození	k1gNnSc6
a	a	k8xC
jejich	jejich	k3xOp3gMnSc1
otec	otec	k1gMnSc1
(	(	kIx(
<g/>
Sergejův	Sergejův	k2eAgMnSc1d1
mladší	mladý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
Pavel	Pavel	k1gMnSc1
Alexandrovič	Alexandrovič	k1gMnSc1
–	–	k?
nejmladší	mladý	k2eAgMnSc1d3
<g/>
,	,	kIx,
šestý	šestý	k4xOgMnSc1
syn	syn	k1gMnSc1
cara	car	k1gMnSc2
Alexandra	Alexandr	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
se	se	k3xPyFc4
podruhé	podruhé	k6eAd1
morganaticky	morganaticky	k6eAd1
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Olgou	Olga	k1gFnSc7
Valerianovnou	Valerianovný	k2eAgFnSc7d1
von	von	k1gInSc4
Pistolkors	Pistolkorsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomtéž	týž	k3xTgInSc6
roce	rok	k1gInSc6
se	se	k3xPyFc4
velkokníže	velkokníže	k1gMnSc1
Sergej	Sergej	k1gMnSc1
Alexandrovič	Alexandrovič	k1gMnSc1
stal	stát	k5eAaPmAgMnS
generálním	generální	k2eAgMnSc7d1
gubernátorem	gubernátor	k1gMnSc7
Moskvy	Moskva	k1gFnSc2
a	a	k8xC
spolu	spolu	k6eAd1
se	s	k7c7
ženou	žena	k1gFnSc7
se	se	k3xPyFc4
nastěhovali	nastěhovat	k5eAaPmAgMnP
do	do	k7c2
Kremlu	Kreml	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ujav	Ujav	k1gMnSc1
se	se	k3xPyFc4
nových	nový	k2eAgFnPc2d1
povinností	povinnost	k1gFnPc2
<g/>
,	,	kIx,
velkokníže	velkokníže	k1gNnSc1wR
<g/>
,	,	kIx,
jednak	jednak	k8xC
systematicky	systematicky	k6eAd1
a	a	k8xC
promyšleně	promyšleně	k6eAd1
vypudil	vypudit	k5eAaPmAgMnS
z	z	k7c2
Moskvy	Moskva	k1gFnSc2
židovské	židovský	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
<g/>
,	,	kIx,
jednak	jednak	k8xC
podnikal	podnikat	k5eAaImAgMnS
tvrdé	tvrdý	k2eAgInPc4d1
kroky	krok	k1gInPc4
proti	proti	k7c3
nelegálním	legální	k2eNgFnPc3d1
levicovým	levicový	k2eAgFnPc3d1
organizacím	organizace	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
odpověď	odpověď	k1gFnSc4
na	na	k7c4
to	ten	k3xDgNnSc4
zorganizovali	zorganizovat	k5eAaPmAgMnP
eseři	eser	k1gMnPc1
řadu	řada	k1gFnSc4
atentátů	atentát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bombový	bombový	k2eAgInSc4d1
atentát	atentát	k1gInSc4
dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
roku	rok	k1gInSc2
1905	#num#	k4
byl	být	k5eAaImAgInS
pro	pro	k7c4
esery	eser	k1gMnPc4
úspěšný	úspěšný	k2eAgInSc1d1
<g/>
:	:	kIx,
člen	člen	k1gInSc1
eserské	eserský	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
Ivan	Ivan	k1gMnSc1
Kaljajev	Kaljajev	k1gMnSc1
hodil	hodit	k5eAaPmAgMnS,k5eAaImAgMnS
bombu	bomba	k1gFnSc4
na	na	k7c4
projíždějící	projíždějící	k2eAgInSc4d1
vůz	vůz	k1gInSc4
velkoknížete	velkokníže	k1gNnSc2wR
a	a	k8xC
na	na	k7c6
místě	místo	k1gNnSc6
ho	on	k3xPp3gMnSc4
zabil	zabít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alžběta	Alžběta	k1gFnSc1
byla	být	k5eAaImAgFnS
do	do	k7c2
několika	několik	k4yIc2
minut	minuta	k1gFnPc2
na	na	k7c6
místě	místo	k1gNnSc6
a	a	k8xC
podle	podle	k7c2
svědků	svědek	k1gMnPc2
měla	mít	k5eAaImAgFnS
osobně	osobně	k6eAd1
hledat	hledat	k5eAaImF
ostatky	ostatek	k1gInPc4
svého	svůj	k1gMnSc2
muže	muž	k1gMnSc2
v	v	k7c6
troskách	troska	k1gFnPc6
vozu	vůz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
dní	den	k1gInPc2
po	po	k7c6
atentátu	atentát	k1gInSc6
navštívila	navštívit	k5eAaPmAgFnS
vraha	vrah	k1gMnSc4
svého	svůj	k3xOyFgMnSc4
muže	muž	k1gMnSc4
ve	v	k7c6
vězení	vězení	k1gNnSc6
a	a	k8xC
odpustila	odpustit	k5eAaPmAgFnS
mu	on	k3xPp3gMnSc3
a	a	k8xC
předala	předat	k5eAaPmAgFnS
mu	on	k3xPp3gNnSc3
Evangelium	evangelium	k1gNnSc4
<g/>
;	;	kIx,
chtěla	chtít	k5eAaImAgFnS
ho	on	k3xPp3gInSc4
přesvědčit	přesvědčit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
kál	kát	k5eAaImAgMnS
a	a	k8xC
ochránil	ochránit	k5eAaPmAgMnS
tak	tak	k9
svou	svůj	k3xOyFgFnSc4
duši	duše	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
Kaljajev	Kaljajev	k1gMnSc1
to	ten	k3xDgNnSc4
odmítl	odmítnout	k5eAaPmAgMnS
(	(	kIx(
<g/>
toto	tento	k3xDgNnSc4
setkání	setkání	k1gNnSc4
zobrazil	zobrazit	k5eAaPmAgInS
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
dramatu	drama	k1gNnSc6
Spravedliví	spravedlivý	k2eAgMnPc1d1
Albert	Albert	k1gMnSc1
Camus	Camus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
vznesla	vznést	k5eAaPmAgFnS
k	k	k7c3
carovi	car	k1gMnSc3
Mikuláši	Mikuláš	k1gMnSc3
II	II	kA
<g/>
.	.	kIx.
prosbu	prosba	k1gFnSc4
o	o	k7c4
milost	milost	k1gFnSc4
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
jí	jíst	k5eAaImIp3nS
však	však	k9
nevyhověl	vyhovět	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaljajev	Kaljajev	k1gMnSc1
byl	být	k5eAaImAgMnS
odsouzen	odsoudit	k5eAaPmNgMnS,k5eAaImNgMnS
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
a	a	k8xC
23	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1905	#num#	k4
oběšen	oběšen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
popravou	poprava	k1gFnSc7
údajně	údajně	k6eAd1
odmítl	odmítnout	k5eAaPmAgMnS
políbit	políbit	k5eAaPmF
kříž	kříž	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Přes	přes	k7c4
naléhání	naléhání	k1gNnSc4
její	její	k3xOp3gFnSc2
rodiny	rodina	k1gFnSc2
odjet	odjet	k5eAaPmF
z	z	k7c2
Ruska	Rusko	k1gNnSc2
se	se	k3xPyFc4
Alžběta	Alžběta	k1gFnSc1
rozhodla	rozhodnout	k5eAaPmAgFnS
zůstat	zůstat	k5eAaPmF
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záhy	záhy	k6eAd1
po	po	k7c6
smrti	smrt	k1gFnSc6
manžela	manžel	k1gMnSc2
prodala	prodat	k5eAaPmAgFnS
své	svůj	k3xOyFgInPc4
šperky	šperk	k1gInPc4
(	(	kIx(
<g/>
vyjma	vyjma	k7c2
těch	ten	k3xDgInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
náležely	náležet	k5eAaImAgInP
dynastii	dynastie	k1gFnSc4
Romanovců	Romanovec	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
za	za	k7c4
utržené	utržený	k2eAgInPc4d1
peníze	peníz	k1gInPc4
koupila	koupit	k5eAaPmAgFnS
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
usedlost	usedlost	k1gFnSc4
se	s	k7c7
čtyřmi	čtyři	k4xCgInPc7
domy	dům	k1gInPc7
a	a	k8xC
velkou	velká	k1gFnSc7
zahradou	zahrada	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
měl	mít	k5eAaImAgMnS
sídlo	sídlo	k1gNnSc4
klášter	klášter	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
založila	založit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1909	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
patronkami	patronka	k1gFnPc7
byly	být	k5eAaImAgFnP
Marie	Marie	k1gFnSc1
a	a	k8xC
Marta	Marta	k1gFnSc1
a	a	k8xC
měl	mít	k5eAaImAgInS
se	s	k7c7
všestrannou	všestranný	k2eAgFnSc7d1
charitativní	charitativní	k2eAgFnSc7d1
lékařskou	lékařský	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
zaměřit	zaměřit	k5eAaPmF
na	na	k7c4
pomoc	pomoc	k1gFnSc4
nejubožejším	ubohý	k2eAgInSc7d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
před	před	k7c7
složením	složení	k1gNnSc7
řeholního	řeholní	k2eAgInSc2d1
slibu	slib	k1gInSc2
se	se	k3xPyFc4
velkokněžna	velkokněžna	k1gFnSc1
stáhla	stáhnout	k5eAaPmAgFnS
ze	z	k7c2
společenského	společenský	k2eAgInSc2d1
i	i	k8xC
veřejného	veřejný	k2eAgInSc2d1
života	život	k1gInSc2
a	a	k8xC
začala	začít	k5eAaPmAgFnS
pracovat	pracovat	k5eAaImF
jako	jako	k9
milosrdná	milosrdný	k2eAgFnSc1d1
sestra	sestra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
zapojila	zapojit	k5eAaPmAgFnS
aktivně	aktivně	k6eAd1
do	do	k7c2
pomoci	pomoc	k1gFnSc2
ruské	ruský	k2eAgFnSc2d1
armádě	armáda	k1gFnSc3
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
raněným	raněný	k2eAgMnPc3d1
vojákům	voják	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
pomáhala	pomáhat	k5eAaImAgFnS
i	i	k9
válečným	válečný	k2eAgMnPc3d1
zajatcům	zajatec	k1gMnPc3
<g/>
,	,	kIx,
kterými	který	k3yQgFnPc7,k3yIgFnPc7,k3yRgFnPc7
byly	být	k5eAaImAgFnP
přeplněny	přeplnit	k5eAaPmNgInP
špitály	špitál	k1gInPc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
obviněna	obvinit	k5eAaPmNgFnS
z	z	k7c2
napomáhání	napomáhání	k1gNnPc2
Němcům	Němec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Měla	mít	k5eAaImAgFnS
jednoznačně	jednoznačně	k6eAd1
negativní	negativní	k2eAgInSc4d1
vztah	vztah	k1gInSc4
ke	k	k7c3
Grigoriji	Grigoriji	k1gMnSc3
Rasputinovi	Rasputin	k1gMnSc3
<g/>
,	,	kIx,
oblíbenci	oblíbenec	k1gMnPc1
své	svůj	k3xOyFgFnSc2
sestry	sestra	k1gFnSc2
<g/>
,	,	kIx,
carevny	carevna	k1gFnSc2
Alexandry	Alexandra	k1gFnSc2
Fjodorovny	Fjodorovna	k1gFnSc2
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gNnSc7
nikdy	nikdy	k6eAd1
nesetkala	setkat	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
před	před	k7c7
abdikací	abdikace	k1gFnSc7
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Mikuláše	mikuláš	k1gInSc2
II	II	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
Jelizaveta	Jelizaveta	k1gFnSc1
Fjodorovna	Fjodorovna	k1gFnSc1
pokoušela	pokoušet	k5eAaImAgFnS
(	(	kIx(
<g/>
neúspěšně	úspěšně	k6eNd1
<g/>
)	)	kIx)
přesvědčit	přesvědčit	k5eAaPmF
Alexandru	Alexandr	k1gMnSc3
Fjodorovnu	Fjodorovna	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Rasputina	Rasputina	k1gFnSc1
vykázala	vykázat	k5eAaPmAgFnS
od	od	k7c2
dvora	dvůr	k1gInSc2
<g/>
;	;	kIx,
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
poslední	poslední	k2eAgInSc1d1
rozhovor	rozhovor	k1gInSc1
v	v	k7c6
životě	život	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
spolu	spolu	k6eAd1
sestry	sestra	k1gFnPc1
měly	mít	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vraždu	vražda	k1gFnSc4
Rasputina	Rasputin	k2eAgFnSc1d1
ocenila	ocenit	k5eAaPmAgFnS
Jelizaveta	Jelizaveta	k1gFnSc1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
vlastenecký	vlastenecký	k2eAgInSc1d1
čin	čin	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Smrt	smrt	k1gFnSc1
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
bolševici	bolševik	k1gMnPc1
zmocnili	zmocnit	k5eAaPmAgMnP
vlády	vláda	k1gFnPc4
<g/>
,	,	kIx,
odmítla	odmítnout	k5eAaPmAgFnS
opustit	opustit	k5eAaPmF
Rusko	Rusko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
roku	rok	k1gInSc2
1918	#num#	k4
byla	být	k5eAaImAgFnS
uvězněna	uvěznit	k5eAaPmNgFnS
a	a	k8xC
posléze	posléze	k6eAd1
odvezena	odvézt	k5eAaPmNgFnS
z	z	k7c2
Moskvy	Moskva	k1gFnSc2
do	do	k7c2
Permu	perm	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
ji	on	k3xPp3gFnSc4
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
členy	člen	k1gMnPc7
rodiny	rodina	k1gFnSc2
Romanovových	Romanovová	k1gFnPc2
převezli	převézt	k5eAaPmAgMnP
do	do	k7c2
Jekatěrinburgu	Jekatěrinburg	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	on	k3xPp3gNnSc4
internovali	internovat	k5eAaBmAgMnP
v	v	k7c6
hostinci	hostinec	k1gInSc6
А	А	k?
н	н	k?
(	(	kIx(
<g/>
v	v	k7c6
současnosti	současnost	k1gFnSc6
v	v	k7c6
budově	budova	k1gFnSc6
na	na	k7c6
křižovatce	křižovatka	k1gFnSc6
Leninovy	Leninův	k2eAgFnSc2d1
a	a	k8xC
Vajnerovy	Vajnerův	k2eAgFnSc2d1
ulice	ulice	k1gFnSc2
sídlí	sídlet	k5eAaImIp3nS
vedení	vedení	k1gNnSc1
Federální	federální	k2eAgFnSc2d1
bezpečnostní	bezpečnostní	k2eAgFnSc2d1
služby	služba	k1gFnSc2
a	a	k8xC
Hlavní	hlavní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
vnitra	vnitro	k1gNnSc2
Sverdlovské	sverdlovský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
nato	nato	k6eAd1
byla	být	k5eAaImAgFnS
skupina	skupina	k1gFnSc1
převezena	převézt	k5eAaPmNgFnS
do	do	k7c2
města	město	k1gNnSc2
Alapajevsk	Alapajevsk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelizaveta	Jelizaveta	k1gFnSc1
Fjodorovna	Fjodorovna	k1gFnSc1
neztrácela	ztrácet	k5eNaImAgFnS
duchapřítomnost	duchapřítomnost	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
dopisech	dopis	k1gInPc6
nabádala	nabádat	k5eAaImAgFnS,k5eAaBmAgFnS
zbylé	zbylý	k2eAgFnPc4d1
sestry	sestra	k1gFnPc4
zachovávat	zachovávat	k5eAaImF
lásku	láska	k1gFnSc4
k	k	k7c3
Bohu	bůh	k1gMnSc3
a	a	k8xC
bližním	bližní	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Apalajevsku	Apalajevsko	k1gNnSc6
byli	být	k5eAaImAgMnP
vězňové	vězeň	k1gMnPc1
zavřeni	zavřen	k2eAgMnPc1d1
v	v	k7c6
budově	budova	k1gFnSc6
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodnes	dodnes	k6eAd1
vedle	vedle	k7c2
její	její	k3xOp3gFnSc2
budovy	budova	k1gFnSc2
roste	růst	k5eAaImIp3nS
jabloň	jabloň	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
měla	mít	k5eAaImAgFnS
podle	podle	k7c2
ústního	ústní	k2eAgNnSc2d1
lidového	lidový	k2eAgNnSc2d1
podání	podání	k1gNnSc2
zasadit	zasadit	k5eAaPmF
velkokněžna	velkokněžna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
noci	noc	k1gFnSc6
na	na	k7c4
18	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
roku	rok	k1gInSc2
1918	#num#	k4
<g/>
,	,	kIx,
den	den	k1gInSc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
komunisté	komunista	k1gMnPc1
zavraždili	zavraždit	k5eAaPmAgMnP
bývalého	bývalý	k2eAgMnSc4d1
cara	car	k1gMnSc4
Mikuláše	Mikuláš	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
rodinu	rodina	k1gFnSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
Jelizaveta	Jelizavet	k2eAgFnSc1d1
Fjodorovna	Fjodorovna	k1gFnSc1
i	i	k8xC
její	její	k3xOp3gMnPc1
společníci	společník	k1gMnPc1
(	(	kIx(
<g/>
tři	tři	k4xCgMnPc1
synové	syn	k1gMnPc1
velkoknížete	velkokníže	k1gMnSc2
Konstantina	Konstantin	k1gMnSc2
Konstantinoviče	Konstantinovič	k1gMnSc2
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
Konstantin	Konstantin	k1gMnSc1
a	a	k8xC
Igor	Igor	k1gMnSc1
<g/>
,	,	kIx,
velkokníže	velkokníže	k1gMnSc1
Sergej	Sergej	k1gMnSc1
Michajlovič	Michajlovič	k1gMnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
tajemník	tajemník	k1gMnSc1
Fjodor	Fjodor	k1gMnSc1
Semjonovič	Semjonovič	k1gMnSc1
Remez	Remez	k1gMnSc1
<g/>
,	,	kIx,
kníže	kníže	k1gMnSc1
Vladimír	Vladimír	k1gMnSc1
Pavlovič	Pavlovič	k1gMnSc1
Palei	Palei	k1gNnPc2
a	a	k8xC
jeptiška	jeptiška	k1gFnSc1
Varvara	Varvara	k1gFnSc1
Jakovlevová	Jakovlevová	k1gFnSc1
<g/>
,	,	kIx,
deportovaná	deportovaný	k2eAgFnSc1d1
společně	společně	k6eAd1
s	s	k7c7
velkokněžnou	velkokněžna	k1gFnSc7
<g/>
)	)	kIx)
bolševiky	bolševik	k1gMnPc4
zavražděni	zavražděn	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Vasilije	Vasilije	k1gFnSc2
Rjabova	Rjabův	k2eAgInSc2d1
<g/>
,	,	kIx,
očitého	očitý	k2eAgMnSc2d1
svědka	svědek	k1gMnSc2
události	událost	k1gFnSc2
<g/>
,	,	kIx,
vrahové	vrah	k1gMnPc1
vtrhli	vtrhnout	k5eAaPmAgMnP
v	v	k7c6
noci	noc	k1gFnSc6
do	do	k7c2
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
věznění	vězněný	k2eAgMnPc1d1
spali	spát	k5eAaImAgMnP
<g/>
,	,	kIx,
a	a	k8xC
řekli	říct	k5eAaPmAgMnP
jim	on	k3xPp3gMnPc3
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
převezeni	převézt	k5eAaPmNgMnP
na	na	k7c4
bezpečnější	bezpečný	k2eAgNnSc4d2
místo	místo	k1gNnSc4
<g/>
;	;	kIx,
zavázali	zavázat	k5eAaPmAgMnP
jim	on	k3xPp3gMnPc3
oči	oko	k1gNnPc4
a	a	k8xC
spoutali	spoutat	k5eAaPmAgMnP
ruce	ruka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
poslechli	poslechnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
velkokníže	velkokníže	k1gMnSc1
Sergej	Sergej	k1gMnSc1
Michajlovič	Michajlovič	k1gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	on	k3xPp3gInPc4
povezou	vézt	k5eAaImIp3nP,k5eAaPmIp3nP
na	na	k7c4
popravu	poprava	k1gFnSc4
<g/>
,	,	kIx,
nato	nato	k6eAd1
byl	být	k5eAaImAgInS
střelen	střelit	k5eAaPmNgInS
do	do	k7c2
paže	paže	k1gFnSc2
a	a	k8xC
podvolil	podvolit	k5eAaPmAgMnS
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vězňové	vězeň	k1gMnPc1
dostali	dostat	k5eAaPmAgMnP
příkaz	příkaz	k1gInSc4
jít	jít	k5eAaImF
–	–	k?
se	s	k7c7
zavázanýma	zavázaný	k2eAgNnPc7d1
očima	oko	k1gNnPc7
a	a	k8xC
spoutanýma	spoutaný	k2eAgFnPc7d1
rukama	ruka	k1gFnPc7
–	–	k?
po	po	k7c6
deskách	deska	k1gFnPc6
až	až	k6eAd1
k	k	k7c3
ústí	ústí	k1gNnSc3
šachty	šachta	k1gFnSc2
dolu	dol	k1gInSc2
<g/>
,	,	kIx,
Sergej	Sergej	k1gMnSc1
opět	opět	k6eAd1
jako	jako	k9
jediný	jediný	k2eAgMnSc1d1
neuposlechl	uposlechnout	k5eNaPmAgMnS
<g/>
,	,	kIx,
vrhl	vrhnout	k5eAaImAgMnS,k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
ozbrojence	ozbrojenec	k1gMnPc4
a	a	k8xC
byl	být	k5eAaImAgMnS
zastřelen	zastřelit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc4
ostatní	ostatní	k2eAgMnPc1d1
ozbrojenci	ozbrojenec	k1gMnPc1
uhodili	uhodit	k5eAaPmAgMnP
do	do	k7c2
hlavy	hlava	k1gFnSc2
<g/>
,	,	kIx,
živé	živá	k1gFnPc4
svrhli	svrhnout	k5eAaPmAgMnP
do	do	k7c2
šachty	šachta	k1gFnSc2
a	a	k8xC
hodili	hodit	k5eAaImAgMnP,k5eAaPmAgMnP
za	za	k7c7
nimi	on	k3xPp3gMnPc7
dva	dva	k4xCgInPc4
granáty	granát	k1gInPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
destruovali	destruovat	k5eAaImAgMnP
šachtu	šachta	k1gFnSc4
a	a	k8xC
živé	živý	k2eAgInPc4d1
i	i	k8xC
mrtvé	mrtvý	k2eAgInPc4d1
zasypali	zasypat	k5eAaPmAgMnP
(	(	kIx(
<g/>
zraněná	zraněný	k2eAgFnSc1d1
Alžběta	Alžběta	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
poměrně	poměrně	k6eAd1
rychle	rychle	k6eAd1
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
granátům	granát	k1gInPc3
unikli	uniknout	k5eAaPmAgMnP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byla	být	k5eAaImAgNnP
těla	tělo	k1gNnPc1
vytažena	vytáhnout	k5eAaPmNgNnP
z	z	k7c2
šachty	šachta	k1gFnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
některé	některý	k3yIgFnPc1
oběti	oběť	k1gFnPc1
pád	pád	k1gInSc4
přežily	přežít	k5eAaPmAgFnP
a	a	k8xC
umíraly	umírat	k5eAaImAgFnP
dlouze	dlouho	k6eAd1
v	v	k7c6
důsledku	důsledek	k1gInSc6
zranění	zranění	k1gNnSc2
a	a	k8xC
hladu	hlad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zranění	zranění	k1gNnSc1
knížete	kníže	k1gMnSc2
Ivana	Ivan	k1gMnSc2
bylo	být	k5eAaImAgNnS
ovázáno	ovázat	k5eAaPmNgNnS
kusem	kus	k1gInSc7
Jelizavetina	Jelizavetin	k2eAgNnSc2d1
roucha	roucho	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rolníci	rolník	k1gMnPc1
z	z	k7c2
okolí	okolí	k1gNnSc2
vyprávěli	vyprávět	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
několik	několik	k4yIc4
dní	den	k1gInPc2
se	se	k3xPyFc4
z	z	k7c2
šachty	šachta	k1gFnSc2
ozýval	ozývat	k5eAaImAgInS
zpěv	zpěv	k1gInSc1
modliteb	modlitba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Čtyři	čtyři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
nato	nato	k6eAd1
<g/>
,	,	kIx,
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1918	#num#	k4
<g/>
,	,	kIx,
obsadila	obsadit	k5eAaPmAgFnS
Alapajevsk	Alapajevsk	k1gInSc4
Bílá	bílý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatky	ostatek	k1gInPc1
zavražděných	zavražděný	k2eAgMnPc2d1
byly	být	k5eAaImAgFnP
vyzvednuty	vyzvednout	k5eAaPmNgFnP
z	z	k7c2
šachty	šachta	k1gFnSc2
a	a	k8xC
uloženy	uložit	k5eAaPmNgFnP
do	do	k7c2
hrobů	hrob	k1gInPc2
<g/>
,	,	kIx,
s	s	k7c7
příchodem	příchod	k1gInSc7
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
byla	být	k5eAaImAgNnP
těla	tělo	k1gNnPc1
převezena	převézt	k5eAaPmNgNnP
dále	daleko	k6eAd2
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
opakovaně	opakovaně	k6eAd1
<g/>
,	,	kIx,
několikrát	několikrát	k6eAd1
po	po	k7c6
sobě	sebe	k3xPyFc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
roku	rok	k1gInSc2
1920	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
Pekingu	Peking	k1gInSc6
našel	najít	k5eAaPmAgMnS
arcibiskup	arcibiskup	k1gMnSc1
Ruské	ruský	k2eAgFnSc2d1
duchovní	duchovní	k2eAgFnSc2d1
misie	misie	k1gFnSc2
Innokentij	Innokentij	k1gMnSc1
(	(	kIx(
<g/>
Fugurovskij	Fugurovskij	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
byly	být	k5eAaImAgInP
dva	dva	k4xCgInPc1
hroby	hrob	k1gInPc1
–	–	k?
velkokněžny	velkokněžna	k1gFnSc2
Jelizavety	Jelizaveta	k1gFnSc2
Fjodorovny	Fjodorovna	k1gFnSc2
a	a	k8xC
sestry	sestra	k1gFnSc2
Varvary	Varvar	k1gInPc4
–	–	k?
převezeny	převézt	k5eAaPmNgInP
do	do	k7c2
Šanghaje	Šanghaj	k1gFnSc2
a	a	k8xC
dále	daleko	k6eAd2
parníkem	parník	k1gInSc7
do	do	k7c2
Port	porta	k1gFnPc2
Saidu	Said	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
1921	#num#	k4
byly	být	k5eAaImAgFnP
pohřbeny	pohřbít	k5eAaPmNgFnP
v	v	k7c6
chrámu	chrám	k1gInSc6
sv.	sv.	kA
Marie	Maria	k1gFnSc2
Magdaleny	Magdalena	k1gFnSc2
v	v	k7c6
Getsemanské	getsemanský	k2eAgFnSc6d1
zahradě	zahrada	k1gFnSc6
na	na	k7c6
úpatí	úpatí	k1gNnSc6
Olivetské	olivetský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
bylo	být	k5eAaImAgNnS
naplněno	naplnit	k5eAaPmNgNnS
přání	přání	k1gNnSc1
velkokněžny	velkokněžna	k1gFnSc2
být	být	k5eAaImF
pohřbena	pohřbít	k5eAaPmNgNnP
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
vyjádřila	vyjádřit	k5eAaPmAgFnS
při	při	k7c6
pouti	pouť	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1888	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
byly	být	k5eAaImAgFnP
velkokněžna	velkokněžna	k1gFnSc1
Jelizaveta	Jelizaveta	k1gFnSc1
Fjodorovna	Fjodorovna	k1gFnSc1
a	a	k8xC
sestra	sestra	k1gFnSc1
Varvara	Varvara	k1gFnSc1
Ruskou	ruský	k2eAgFnSc4d1
pravoslavnou	pravoslavný	k2eAgFnSc4d1
církví	církev	k1gFnPc2
zařazeny	zařadit	k5eAaPmNgInP
mezi	mezi	k7c4
svaté	svatý	k2eAgMnPc4d1
novomučedníky	novomučedník	k1gMnPc4
(	(	kIx(
<g/>
strastotěrpěc	strastotěrpěc	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
kanonizovány	kanonizovat	k5eAaBmNgFnP
Ruskou	ruský	k2eAgFnSc7d1
pravoslavnou	pravoslavný	k2eAgFnSc7d1
církví	církev	k1gFnSc7
za	za	k7c7
hranicemi	hranice	k1gFnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2004	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
byly	být	k5eAaImAgInP
jejich	jejich	k3xOp3gInPc1
ostatky	ostatek	k1gInPc1
převezeny	převézt	k5eAaPmNgInP
postupně	postupně	k6eAd1
do	do	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
SNS	SNS	kA
a	a	k8xC
Baltských	baltský	k2eAgFnPc2d1
republik	republika	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
poklonilo	poklonit	k5eAaPmAgNnS
více	hodně	k6eAd2
než	než	k8xS
sedm	sedm	k4xCc1
milionů	milion	k4xCgInPc2
lidí	člověk	k1gMnPc2
<g/>
;	;	kIx,
poté	poté	k6eAd1
byly	být	k5eAaImAgInP
navráceny	navrátit	k5eAaPmNgInP
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Velkokněžně	velkokněžna	k1gFnSc3
je	být	k5eAaImIp3nS
zasvěceno	zasvěcen	k2eAgNnSc1d1
několik	několik	k4yIc1
pravoslavných	pravoslavný	k2eAgInPc2d1
klášterů	klášter	k1gInPc2
v	v	k7c6
Bělorusku	Bělorusko	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Rusku	Rusko	k1gNnSc6
a	a	k8xC
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
roku	rok	k1gInSc2
2009	#num#	k4
ruská	ruský	k2eAgFnSc1d1
Generální	generální	k2eAgFnSc1d1
prokuratura	prokuratura	k1gFnSc1
Jelizavetu	Jelizavet	k2eAgFnSc4d1
Fjodorovnu	Fjodorovna	k1gFnSc4
posmrtně	posmrtně	k6eAd1
rehabilitovala	rehabilitovat	k5eAaBmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
svatou	svatá	k1gFnSc4
a	a	k8xC
mučednici	mučednice	k1gFnSc4
ji	on	k3xPp3gFnSc4
uznávají	uznávat	k5eAaImIp3nP
pravoslavní	pravoslavný	k2eAgMnPc1d1
i	i	k8xC
někteří	některý	k3yIgMnPc1
protestanti	protestant	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
I.	I.	kA
Hesenský	hesenský	k2eAgInSc4d1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hesenský	hesenský	k2eAgInSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Hesensko-Darmstadtská	Hesensko-Darmstadtský	k2eAgFnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Hesenský	hesenský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Bádenský	bádenský	k2eAgMnSc1d1
</s>
<s>
Vilemína	Vilemína	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Bádenská	bádenský	k2eAgFnSc1d1
</s>
<s>
Amálie	Amálie	k1gFnSc1
Hesensko-Darmstadtská	Hesensko-Darmstadtský	k2eAgFnSc1d1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hesenský	hesenský	k2eAgInSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Vilém	Vilém	k1gMnSc1
Pruský	pruský	k2eAgMnSc1d1
</s>
<s>
Frederika	Frederika	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Hesensko-Darmstadtská	Hesensko-Darmstadtský	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Pruská	pruský	k2eAgFnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
V.	V.	kA
Hesensko-Homburský	Hesensko-Homburský	k2eAgMnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Anna	Anna	k1gFnSc1
Hesensko-Homburská	Hesensko-Homburský	k2eAgFnSc1d1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Hesensko-Darmstadtská	Hesensko-Darmstadtský	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Ruská	ruský	k2eAgFnSc1d1
</s>
<s>
František	František	k1gMnSc1
Sasko-Kobursko-Saalfeldský	Sasko-Kobursko-Saalfeldský	k2eAgMnSc1d1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
I.	I.	kA
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgInSc5d1
</s>
<s>
Augusta	Augusta	k1gMnSc1
Reuss	Reussa	k1gFnPc2
Ebersdorf	Ebersdorf	k1gMnSc1
</s>
<s>
Albert	Albert	k1gMnSc1
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgMnSc1d1
</s>
<s>
Augustus	Augustus	k1gInSc1
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Sasko-Gothajsko-Altenburská	Sasko-Gothajsko-Altenburský	k2eAgFnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Šarlota	Šarlota	k1gFnSc1
Meklenbursko-Zvěřínská	Meklenbursko-Zvěřínský	k2eAgFnSc1d1
</s>
<s>
Alice	Alice	k1gFnSc1
Sasko-Koburská	Sasko-Koburský	k2eAgFnSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
August	August	k1gMnSc1
Hannoverský	hannoverský	k2eAgMnSc1d1
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
Meklenbursko-Střelická	Meklenbursko-Střelický	k2eAgFnSc1d1
</s>
<s>
královna	královna	k1gFnSc1
Viktorie	Viktoria	k1gFnSc2
</s>
<s>
František	František	k1gMnSc1
Sasko-Kobursko-Saalfeldský	Sasko-Kobursko-Saalfeldský	k2eAgMnSc1d1
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
Sasko-Kobursko-Saalfeldská	Sasko-Kobursko-Saalfeldský	k2eAgFnSc1d1
</s>
<s>
Augusta	Augusta	k1gMnSc1
Reuss	Reussa	k1gFnPc2
Ebersdorf	Ebersdorf	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Alžběta	Alžběta	k1gFnSc1
Ruská	ruský	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://www.peoples.ru/state/sacred/ella/history.html	http://www.peoples.ru/state/sacred/ella/history.html	k1gMnSc1
</s>
<s>
http://religion.ng.ru/printed/history/2004-08-04/5_elizaveta.html	http://religion.ng.ru/printed/history/2004-08-04/5_elizaveta.html	k1gMnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
js	js	k?
<g/>
2010561340	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118835815	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
6659	#num#	k4
9782	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
85384233	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
3267821	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
85384233	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
