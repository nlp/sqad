<s>
Lord	lord	k1gMnSc1	lord
Mord	morda	k1gFnPc2	morda
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
spisovatele	spisovatel	k1gMnSc2	spisovatel
Miloše	Miloš	k1gMnSc2	Miloš
Urbana	Urban	k1gMnSc2	Urban
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Argo	Argo	k6eAd1	Argo
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
době	doba	k1gFnSc6	doba
velké	velký	k2eAgFnSc2d1	velká
pražské	pražský	k2eAgFnSc2d1	Pražská
asanace	asanace	k1gFnSc2	asanace
bývalého	bývalý	k2eAgNnSc2d1	bývalé
židovského	židovský	k2eAgNnSc2d1	Židovské
gheta	gheto	k1gNnSc2	gheto
<g/>
.	.	kIx.	.
</s>
<s>
Hrdinou	Hrdina	k1gMnSc7	Hrdina
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgMnSc1d1	fiktivní
hrabě	hrabě	k1gMnSc1	hrabě
Arco	Arco	k1gMnSc1	Arco
<g/>
,	,	kIx,	,
mladý	mladý	k2eAgMnSc1d1	mladý
nemocný	nemocný	k2eAgMnSc1d1	nemocný
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
oddávavá	oddávavý	k2eAgFnSc1d1	oddávavý
neřestem	neřest	k1gFnPc3	neřest
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
užívání	užívání	k1gNnSc2	užívání
drog	droga	k1gFnPc2	droga
postupně	postupně	k6eAd1	postupně
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
rozum	rozum	k1gInSc4	rozum
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
přestává	přestávat	k5eAaImIp3nS	přestávat
rozeznávat	rozeznávat	k5eAaImF	rozeznávat
své	svůj	k3xOyFgFnPc4	svůj
děsivé	děsivý	k2eAgFnPc4d1	děsivá
představy	představa	k1gFnPc4	představa
od	od	k7c2	od
skutečnosti	skutečnost	k1gFnSc2	skutečnost
a	a	k8xC	a
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
se	se	k3xPyFc4	se
na	na	k7c4	na
svým	svůj	k3xOyFgInSc7	svůj
lehkomyslným	lehkomyslný	k2eAgInSc7d1	lehkomyslný
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
prolíná	prolínat	k5eAaImIp3nS	prolínat
etnické	etnický	k2eAgNnSc1d1	etnické
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
Čechy	Čech	k1gMnPc7	Čech
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc7	Němec
a	a	k8xC	a
Židy	Žid	k1gMnPc7	Žid
<g/>
,	,	kIx,	,
praktiky	praktika	k1gFnPc4	praktika
c.k.	c.k.	k?	c.k.
policejní	policejní	k2eAgFnSc2d1	policejní
mašinérie	mašinérie	k1gFnSc2	mašinérie
a	a	k8xC	a
mocensko-ekonomické	mocenskokonomický	k2eAgFnSc2d1	mocensko-ekonomický
machinace	machinace	k1gFnSc2	machinace
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
bouráním	bourání	k1gNnSc7	bourání
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
obyvatel	obyvatel	k1gMnPc2	obyvatel
staré	starý	k2eAgFnSc2d1	stará
městské	městský	k2eAgFnSc2d1	městská
čtvrti	čtvrt	k1gFnSc2	čtvrt
"	"	kIx"	"
<g/>
v	v	k7c6	v
Židech	Žid	k1gMnPc6	Žid
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
podivných	podivný	k2eAgFnPc2d1	podivná
okolností	okolnost	k1gFnPc2	okolnost
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Vraždy	vražda	k1gFnPc1	vražda
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hrabě	hrabě	k1gMnSc1	hrabě
Arca	Arca	k1gMnSc1	Arca
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
připisovány	připisovat	k5eAaImNgInP	připisovat
starému	starý	k2eAgNnSc3d1	staré
židovskému	židovský	k2eAgNnSc3d1	Židovské
strašidlu	strašidlo	k1gNnSc3	strašidlo
nazývanému	nazývaný	k2eAgInSc3d1	nazývaný
"	"	kIx"	"
<g/>
Masíčko	masíčko	k1gNnSc4	masíčko
<g/>
"	"	kIx"	"
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
Kleinfleich	Kleinfleich	k1gInSc1	Kleinfleich
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
někomu	někdo	k3yInSc3	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
vydává	vydávat	k5eAaImIp3nS	vydávat
<g/>
.	.	kIx.	.
</s>
