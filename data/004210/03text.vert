<s>
Tajnosti	tajnost	k1gFnPc1	tajnost
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
režisérky	režisérka	k1gFnSc2	režisérka
Alice	Alice	k1gFnSc2	Alice
Nellis	Nellis	k1gFnSc2	Nellis
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
v	v	k7c6	v
životě	život	k1gInSc6	život
Julie	Julie	k1gFnSc1	Julie
(	(	kIx(	(
<g/>
Iva	Iva	k1gFnSc1	Iva
Bittová	Bittová	k1gFnSc1	Bittová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
Niny	Nina	k1gFnSc2	Nina
Simone	Simon	k1gMnSc5	Simon
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
koupit	koupit	k5eAaPmF	koupit
si	se	k3xPyFc3	se
klavír	klavír	k1gInSc4	klavír
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
jmenovat	jmenovat	k5eAaImF	jmenovat
Právě	právě	k9	právě
dnes	dnes	k6eAd1	dnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
–	–	k?	–
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
film	film	k1gInSc1	film
roku	rok	k1gInSc2	rok
<g />
.	.	kIx.	.
</s>
<s>
2007	[number]	k4	2007
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
koupit	koupit	k5eAaPmF	koupit
piano	piano	k6eAd1	piano
<g/>
,	,	kIx,	,
Darina	Darina	k1gFnSc1	Darina
Křivánková	Křivánková	k1gFnSc1	Křivánková
<g/>
,	,	kIx,	,
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Gabriela	Gabriela	k1gFnSc1	Gabriela
Kudelová	Kudelová	k1gFnSc1	Kudelová
<g/>
,	,	kIx,	,
Premiere	Premier	k1gInSc5	Premier
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Tajnosti	tajnost	k1gFnPc1	tajnost
|	|	kIx~	|
PREMIERE	PREMIERE	kA	PREMIERE
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
Cinema	Cinema	k1gFnSc1	Cinema
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Tajnosti	tajnost	k1gFnPc1	tajnost
-	-	kIx~	-
recenze	recenze	k1gFnSc1	recenze
filmu	film	k1gInSc2	film
|	|	kIx~	|
časopis	časopis	k1gInSc1	časopis
CINEMA	CINEMA	kA	CINEMA
Proč	proč	k6eAd1	proč
jsou	být	k5eAaImIp3nP	být
Tajnosti	tajnost	k1gFnPc4	tajnost
plné	plný	k2eAgFnSc2d1	plná
radosti	radost	k1gFnSc2	radost
<g/>
,	,	kIx,	,
Mirka	Mirka	k1gFnSc1	Mirka
Spáčilová	Spáčilová	k1gFnSc1	Spáčilová
<g/>
,	,	kIx,	,
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
Tajnosti	tajnost	k1gFnSc2	tajnost
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Tajnosti	tajnost	k1gFnSc2	tajnost
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
