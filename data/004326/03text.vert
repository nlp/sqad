<s>
Slavnosti	slavnost	k1gFnSc2	slavnost
sněženek	sněženka	k1gFnPc2	sněženka
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
barevný	barevný	k2eAgInSc1d1	barevný
film	film	k1gInSc1	film
natočený	natočený	k2eAgInSc1d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
režisérem	režisér	k1gMnSc7	režisér
Jiřím	Jiří	k1gMnSc7	Jiří
Menzelem	Menzel	k1gMnSc7	Menzel
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
knihy	kniha	k1gFnSc2	kniha
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hrabala	Hrabal	k1gMnSc2	Hrabal
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ve	v	k7c6	v
filmu	film	k1gInSc6	film
i	i	k8xC	i
sám	sám	k3xTgInSc1	sám
hraje	hrát	k5eAaImIp3nS	hrát
malou	malý	k2eAgFnSc4d1	malá
epizodní	epizodní	k2eAgFnSc4d1	epizodní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
svérázných	svérázný	k2eAgMnPc6d1	svérázný
obyvatelích	obyvatel	k1gMnPc6	obyvatel
rekreační	rekreační	k2eAgFnSc1d1	rekreační
osady	osada	k1gFnPc1	osada
a	a	k8xC	a
blízké	blízký	k2eAgFnPc1d1	blízká
vesnice	vesnice	k1gFnPc1	vesnice
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejich	jejich	k3xOp3gFnPc6	jejich
zálibách	záliba	k1gFnPc6	záliba
a	a	k8xC	a
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
vztazích	vztah	k1gInPc6	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
polabské	polabský	k2eAgFnSc6d1	Polabská
osadě	osada	k1gFnSc6	osada
Kersko	Kersko	k1gNnSc1	Kersko
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
Sadské	sadský	k2eAgFnSc2d1	Sadská
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
.....	.....	k?	.....
Jarin	Jarin	k1gMnSc1	Jarin
Franc	Franc	k1gMnSc1	Franc
Blažena	Blažena	k1gFnSc1	Blažena
Holišová	Holišová	k1gFnSc1	Holišová
.....	.....	k?	.....
paní	paní	k1gFnSc1	paní
Francová	Francová	k1gFnSc1	Francová
Blanka	Blanka	k1gFnSc1	Blanka
Lormanová	Lormanová	k1gFnSc1	Lormanová
.....	.....	k?	.....
mladá	mladý	k2eAgFnSc1d1	mladá
Francová	Francová	k1gFnSc1	Francová
Jaromír	Jaromír	k1gMnSc1	Jaromír
Hanzlík	Hanzlík	k1gMnSc1	Hanzlík
.....	.....	k?	.....
Leli	Leli	k1gNnSc1	Leli
Johanna	Johanna	k1gFnSc1	Johanna
Tesařová	Tesařová	k1gFnSc1	Tesařová
.....	.....	k?	.....
družka	družka	k1gFnSc1	družka
Leliho	Leli	k1gMnSc2	Leli
Jiří	Jiří	k1gMnSc2	Jiří
Schmitzer	Schmitzer	k1gMnSc1	Schmitzer
.....	.....	k?	.....
hostinský	hostinský	k1gMnSc1	hostinský
Láďa	Láďa	k1gMnSc1	Láďa
Novák	Novák	k1gMnSc1	Novák
Marie	Marie	k1gFnSc1	Marie
Spurná	Spurná	k1gFnSc1	Spurná
.....	.....	k?	.....
paní	paní	k1gFnSc1	paní
Nováková	Nováková	k1gFnSc1	Nováková
Libuše	Libuše	k1gFnSc1	Libuše
Šafránková	Šafránková	k1gFnSc1	Šafránková
.....	.....	k?	.....
učitelka	učitelka	k1gFnSc1	učitelka
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
.....	.....	k?	.....
řidič	řidič	k1gInSc1	řidič
trabanta	trabant	k1gMnSc2	trabant
Zdena	Zdena	k1gFnSc1	Zdena
Hadrbolcová	Hadrbolcová	k1gFnSc1	Hadrbolcová
.....	.....	k?	.....
vedoucí	vedoucí	k1gMnSc1	vedoucí
prodejny	prodejna	k1gFnSc2	prodejna
Jednoty	jednota	k1gFnSc2	jednota
Pavel	Pavel	k1gMnSc1	Pavel
Vondruška	Vondruška	k1gMnSc1	Vondruška
.....	.....	k?	.....
Holý	Holý	k1gMnSc1	Holý
Petr	Petr	k1gMnSc1	Petr
Brukner	Brukner	k1gMnSc1	Brukner
.....	.....	k?	.....
traktorista	traktorista	k1gMnSc1	traktorista
Janeček	Janeček	k1gMnSc1	Janeček
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
.....	.....	k?	.....
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
Junek	Junek	k1gMnSc1	Junek
Slavnosti	slavnost	k1gFnSc2	slavnost
sněženek	sněženka	k1gFnPc2	sněženka
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Slavnosti	slavnost	k1gFnPc1	slavnost
sněženek	sněženka	k1gFnPc2	sněženka
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
