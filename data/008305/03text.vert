<p>
<s>
Krocan	krocan	k1gMnSc1	krocan
(	(	kIx(	(
<g/>
Meleagris	Meleagris	k1gInSc1	Meleagris
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
ptáka	pták	k1gMnSc2	pták
z	z	k7c2	z
monotypické	monotypický	k2eAgFnSc2d1	monotypický
podčeledě	podčeleď	k1gFnSc2	podčeleď
krocani	krocan	k1gMnPc1	krocan
příslušející	příslušející	k2eAgInSc4d1	příslušející
do	do	k7c2	do
čeledě	čeleď	k1gFnSc2	čeleď
bažantovitých	bažantovitý	k2eAgMnPc2d1	bažantovitý
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
krocana	krocan	k1gMnSc2	krocan
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
krůta	krůta	k1gFnSc1	krůta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zootechnické	zootechnický	k2eAgFnSc6d1	zootechnická
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
domácí	domácí	k2eAgMnPc1d1	domácí
krocani	krocan	k1gMnPc1	krocan
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
krůty	krůta	k1gFnPc1	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
maso	maso	k1gNnSc4	maso
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vžilo	vžít	k5eAaPmAgNnS	vžít
označení	označení	k1gNnSc1	označení
krůtí	krůtí	k2eAgNnSc1d1	krůtí
maso	maso	k1gNnSc1	maso
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
krocaní	krocaní	k2eAgFnSc1d1	krocaní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
krocan	krocan	k1gMnSc1	krocan
má	mít	k5eAaImIp3nS	mít
nejasnou	jasný	k2eNgFnSc4d1	nejasná
etymologii	etymologie	k1gFnSc4	etymologie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
slovo	slovo	k1gNnSc1	slovo
morák	morák	k1gMnSc1	morák
(	(	kIx(	(
<g/>
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
ptáka	pták	k1gMnSc4	pták
dovezeného	dovezený	k2eAgNnSc2d1	dovezené
ze	z	k7c2	z
zámoří	zámoří	k1gNnSc2	zámoří
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
(	(	kIx(	(
<g/>
moriak	moriak	k6eAd1	moriak
<g/>
)	)	kIx)	)
a	a	k8xC	a
přežívá	přežívat	k5eAaImIp3nS	přežívat
v	v	k7c6	v
moravských	moravský	k2eAgInPc6d1	moravský
dialektech	dialekt	k1gInPc6	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
nazývá	nazývat	k5eAaImIp3nS	nazývat
morka	morka	k1gFnSc1	morka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zařazení	zařazení	k1gNnSc1	zařazení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
měl	mít	k5eAaImAgMnS	mít
krocan	krocan	k1gMnSc1	krocan
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
čeleď	čeleď	k1gFnSc4	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
morfologickém	morfologický	k2eAgNnSc6d1	morfologické
zkoumání	zkoumání	k1gNnSc6	zkoumání
a	a	k8xC	a
molekulární	molekulární	k2eAgFnSc6d1	molekulární
analýze	analýza	k1gFnSc6	analýza
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
krocana	krocan	k1gMnSc4	krocan
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
jen	jen	k9	jen
podčeleď	podčeleď	k1gFnSc1	podčeleď
v	v	k7c6	v
čeledi	čeleď	k1gFnSc2	čeleď
bažantovitých	bažantovitý	k2eAgInPc2d1	bažantovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Krocan	krocan	k1gMnSc1	krocan
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Nového	Nového	k2eAgInSc2d1	Nového
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
Guatemale	Guatemala	k1gFnSc6	Guatemala
a	a	k8xC	a
v	v	k7c6	v
Belize	Beliza	k1gFnSc6	Beliza
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
pastviny	pastvina	k1gFnPc1	pastvina
<g/>
,	,	kIx,	,
savany	savana	k1gFnPc1	savana
i	i	k9	i
sezónně	sezónně	k6eAd1	sezónně
zaplavovaná	zaplavovaný	k2eAgNnPc1d1	zaplavované
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
ale	ale	k8xC	ale
poblíž	poblíž	k6eAd1	poblíž
vzrostlých	vzrostlý	k2eAgFnPc2d1	vzrostlá
křovin	křovina	k1gFnPc2	křovina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
světlých	světlý	k2eAgInPc2d1	světlý
řídkých	řídký	k2eAgInPc2d1	řídký
listnatých	listnatý	k2eAgInPc2d1	listnatý
i	i	k8xC	i
jehličnatých	jehličnatý	k2eAgInPc2d1	jehličnatý
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
v	v	k7c6	v
takových	takový	k3xDgInPc6	takový
rozsáhlých	rozsáhlý	k2eAgInPc6d1	rozsáhlý
lesích	les	k1gInPc6	les
přímo	přímo	k6eAd1	přímo
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
dostatek	dostatek	k1gInSc4	dostatek
prostoru	prostor	k1gInSc2	prostor
pro	pro	k7c4	pro
vzlétnutí	vzlétnutí	k1gNnSc4	vzlétnutí
na	na	k7c4	na
větve	větev	k1gFnPc4	větev
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
hřaduje	hřadovat	k5eAaImIp3nS	hřadovat
a	a	k8xC	a
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
těžký	těžký	k2eAgMnSc1d1	těžký
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
největší	veliký	k2eAgMnSc1d3	veliký
váží	vážit	k5eAaImIp3nS	vážit
až	až	k9	až
10	[number]	k4	10
kg	kg	kA	kg
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
až	až	k9	až
120	[number]	k4	120
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
krůta	krůta	k1gFnSc1	krůta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
lehčí	lehký	k2eAgFnSc4d2	lehčí
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
krk	krk	k1gInSc4	krk
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
jsou	být	k5eAaImIp3nP	být
krátká	krátký	k2eAgNnPc1d1	krátké
a	a	k8xC	a
zakulacená	zakulacený	k2eAgFnSc1d1	zakulacená
<g/>
,	,	kIx,	,
letky	letka	k1gFnPc1	letka
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
bronzově	bronzově	k6eAd1	bronzově
lesklé	lesklý	k2eAgInPc1d1	lesklý
<g/>
,	,	kIx,	,
ocasní	ocasní	k2eAgNnPc1d1	ocasní
a	a	k8xC	a
nadocasní	nadocasní	k2eAgNnPc1d1	nadocasní
péra	péro	k1gNnPc1	péro
jsou	být	k5eAaImIp3nP	být
šedá	šedý	k2eAgNnPc1d1	šedé
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
mramorovaná	mramorovaný	k2eAgFnSc1d1	mramorovaná
<g/>
,	,	kIx,	,
na	na	k7c6	na
špičkách	špička	k1gFnPc6	špička
ukončena	ukončit	k5eAaPmNgFnS	ukončit
rezavě	rezavě	k6eAd1	rezavě
hnědě	hnědě	k6eAd1	hnědě
a	a	k8xC	a
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
pruhem	pruh	k1gInSc7	pruh
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
pruh	pruh	k1gInSc1	pruh
namodralý	namodralý	k2eAgInSc1d1	namodralý
<g/>
,	,	kIx,	,
černě	černě	k6eAd1	černě
olemovaný	olemovaný	k2eAgMnSc1d1	olemovaný
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
krocani	krocan	k1gMnPc1	krocan
dále	daleko	k6eAd2	daleko
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
ocasních	ocasní	k2eAgMnPc2d1	ocasní
pér	pér	k?	pér
modré	modrý	k2eAgFnSc2d1	modrá
skvrny	skvrna	k1gFnSc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
krk	krk	k1gInSc1	krk
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
peří	peří	k1gNnSc2	peří
<g/>
,	,	kIx,	,
hrubá	hrubý	k2eAgFnSc1d1	hrubá
kůže	kůže	k1gFnSc1	kůže
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
je	být	k5eAaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
brady	brada	k1gFnSc2	brada
nebo	nebo	k8xC	nebo
krku	krk	k1gInSc2	krk
visí	viset	k5eAaImIp3nS	viset
různě	různě	k6eAd1	různě
velké	velký	k2eAgInPc4d1	velký
masité	masitý	k2eAgInPc4d1	masitý
<g/>
,	,	kIx,	,
červené	červený	k2eAgInPc4d1	červený
laloky	lalok	k1gInPc4	lalok
<g/>
,	,	kIx,	,
krk	krk	k1gInSc1	krk
je	být	k5eAaImIp3nS	být
posetý	posetý	k2eAgInSc1d1	posetý
masitými	masitý	k2eAgInPc7d1	masitý
výrůstky	výrůstek	k1gInPc7	výrůstek
<g/>
.	.	kIx.	.
</s>
<s>
Vybarvení	vybarvení	k1gNnSc1	vybarvení
krku	krk	k1gInSc2	krk
i	i	k8xC	i
hlavy	hlava	k1gFnSc2	hlava
je	být	k5eAaImIp3nS	být
odvislé	odvislý	k2eAgNnSc1d1	odvislé
od	od	k7c2	od
ročního	roční	k2eAgNnSc2d1	roční
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
od	od	k7c2	od
emocí	emoce	k1gFnPc2	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Silně	silně	k6eAd1	silně
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
lehčí	lehký	k2eAgFnSc1d2	lehčí
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
nápadně	nápadně	k6eAd1	nápadně
vybarvená	vybarvený	k2eAgFnSc1d1	vybarvená
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
"	"	kIx"	"
<g/>
vousy	vous	k1gInPc4	vous
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
horní	horní	k2eAgFnSc2d1	horní
hrudí	hrudí	k1gNnSc4	hrudí
jim	on	k3xPp3gMnPc3	on
visí	viset	k5eAaImIp3nS	viset
chomáč	chomáč	k1gInSc1	chomáč
hrubých	hrubý	k2eAgFnPc2d1	hrubá
štětin	štětina	k1gFnPc2	štětina
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
až	až	k8xS	až
30	[number]	k4	30
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
mají	mít	k5eAaImIp3nP	mít
samci	samec	k1gMnPc1	samec
ostruhy	ostruha	k1gFnSc2	ostruha
přes	přes	k7c4	přes
2	[number]	k4	2
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průběžně	průběžně	k6eAd1	průběžně
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
různě	různě	k6eAd1	různě
velkých	velký	k2eAgNnPc6d1	velké
hejnech	hejno	k1gNnPc6	hejno
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
se	se	k3xPyFc4	se
potulují	potulovat	k5eAaImIp3nP	potulovat
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
rychle	rychle	k6eAd1	rychle
utíkají	utíkat	k5eAaImIp3nP	utíkat
(	(	kIx(	(
<g/>
až	až	k9	až
10	[number]	k4	10
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
a	a	k8xC	a
ulétají	ulétat	k5eAaImIp3nP	ulétat
na	na	k7c4	na
blízké	blízký	k2eAgInPc4d1	blízký
stromy	strom	k1gInPc4	strom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Strava	strava	k1gFnSc1	strava
==	==	k?	==
</s>
</p>
<p>
<s>
Potravu	potrava	k1gFnSc4	potrava
si	se	k3xPyFc3	se
hledají	hledat	k5eAaImIp3nP	hledat
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
chytají	chytat	k5eAaImIp3nP	chytat
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
pavouky	pavouk	k1gMnPc4	pavouk
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
bezobratlé	bezobratlý	k2eAgMnPc4d1	bezobratlý
živočichy	živočich	k1gMnPc4	živočich
nebo	nebo	k8xC	nebo
uloví	ulovit	k5eAaPmIp3nP	ulovit
i	i	k9	i
drobné	drobný	k2eAgMnPc4d1	drobný
hlodavce	hlodavec	k1gMnPc4	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Sbírají	sbírat	k5eAaImIp3nP	sbírat
bobule	bobule	k1gFnPc4	bobule
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc4	semeno
<g/>
,	,	kIx,	,
spadané	spadaný	k2eAgInPc4d1	spadaný
plody	plod	k1gInPc4	plod
a	a	k8xC	a
ozobávají	ozobávat	k5eAaImIp3nP	ozobávat
zelené	zelený	k2eAgFnPc4d1	zelená
části	část	k1gFnPc4	část
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Potřebují	potřebovat	k5eAaImIp3nP	potřebovat
občasný	občasný	k2eAgInSc4d1	občasný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
vodě	voda	k1gFnSc3	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Krocani	krocan	k1gMnPc1	krocan
jsou	být	k5eAaImIp3nP	být
ptáci	pták	k1gMnPc1	pták
polygamní	polygamní	k2eAgMnSc1d1	polygamní
<g/>
,	,	kIx,	,
samec	samec	k1gMnSc1	samec
mívá	mívat	k5eAaImIp3nS	mívat
více	hodně	k6eAd2	hodně
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
pářením	páření	k1gNnSc7	páření
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tokání	tokání	k1gNnSc3	tokání
samce	samec	k1gInSc2	samec
<g/>
,	,	kIx,	,
zvětšují	zvětšovat	k5eAaImIp3nP	zvětšovat
a	a	k8xC	a
vybarvují	vybarvovat	k5eAaImIp3nP	vybarvovat
se	se	k3xPyFc4	se
jim	on	k3xPp3gInPc3	on
barevné	barevný	k2eAgInPc1d1	barevný
výrůstky	výrůstek	k1gInPc1	výrůstek
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
krku	krk	k1gInSc6	krk
a	a	k8xC	a
předvádějí	předvádět	k5eAaImIp3nP	předvádět
se	se	k3xPyFc4	se
na	na	k7c6	na
volném	volný	k2eAgNnSc6d1	volné
prostranství	prostranství	k1gNnSc6	prostranství
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
obíhají	obíhat	k5eAaImIp3nP	obíhat
dokola	dokola	k6eAd1	dokola
za	za	k7c4	za
roztahování	roztahování	k1gNnSc4	roztahování
pestrých	pestrý	k2eAgInPc2d1	pestrý
ocasních	ocasní	k2eAgInPc2d1	ocasní
pér	pér	k?	pér
<g/>
,	,	kIx,	,
svěšování	svěšování	k1gNnSc3	svěšování
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
hlasitého	hlasitý	k2eAgNnSc2d1	hlasité
houkání	houkání	k1gNnSc2	houkání
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
sokové	sok	k1gMnPc1	sok
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
i	i	k8xC	i
poperou	poprat	k5eAaPmIp3nP	poprat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samice	samice	k1gFnPc1	samice
si	se	k3xPyFc3	se
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
až	až	k8xS	až
květnu	květen	k1gInSc6	květen
hnízdo	hnízdo	k1gNnSc4	hnízdo
v	v	k7c6	v
hustém	hustý	k2eAgNnSc6d1	husté
křoví	křoví	k1gNnSc6	křoví
nebo	nebo	k8xC	nebo
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
chráněném	chráněný	k2eAgNnSc6d1	chráněné
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
důlek	důlek	k1gInSc1	důlek
vystlaný	vystlaný	k2eAgInSc1d1	vystlaný
listím	listí	k1gNnSc7	listí
a	a	k8xC	a
trávou	tráva	k1gFnSc7	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
snesení	snesení	k1gNnSc6	snesení
8	[number]	k4	8
až	až	k9	až
15	[number]	k4	15
žlutě	žlutě	k6eAd1	žlutě
až	až	k6eAd1	až
okrově	okrově	k6eAd1	okrově
tečkovaných	tečkovaný	k2eAgNnPc2d1	tečkované
vajec	vejce	k1gNnPc2	vejce
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
zasedá	zasedat	k5eAaImIp3nS	zasedat
a	a	k8xC	a
opouští	opouštět	k5eAaImIp3nS	opouštět
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
inkubace	inkubace	k1gFnSc1	inkubace
trvá	trvat	k5eAaImIp3nS	trvat
okolo	okolo	k7c2	okolo
28	[number]	k4	28
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Vylíhnutá	vylíhnutý	k2eAgNnPc1d1	vylíhnuté
ochmýřená	ochmýřený	k2eAgNnPc1d1	ochmýřené
mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
schopna	schopen	k2eAgNnPc1d1	schopno
již	již	k6eAd1	již
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
sama	sám	k3xTgFnSc1	sám
opustit	opustit	k5eAaPmF	opustit
hnízdo	hnízdo	k1gNnSc4	hnízdo
a	a	k8xC	a
zobat	zobat	k5eAaImF	zobat
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
je	on	k3xPp3gNnSc4	on
vodí	vodit	k5eAaImIp3nS	vodit
<g/>
,	,	kIx,	,
ochraňuje	ochraňovat	k5eAaImIp3nS	ochraňovat
před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
pod	pod	k7c7	pod
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
asi	asi	k9	asi
10	[number]	k4	10
dnů	den	k1gInPc2	den
již	již	k6eAd1	již
poletují	poletovat	k5eAaImIp3nP	poletovat
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
schopna	schopen	k2eAgNnPc1d1	schopno
ke	k	k7c3	k
hřadování	hřadování	k1gNnSc3	hřadování
vylétnout	vylétnout	k5eAaPmF	vylétnout
na	na	k7c4	na
strom	strom	k1gInSc4	strom
za	za	k7c7	za
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
se	se	k3xPyFc4	se
o	o	k7c4	o
vejce	vejce	k1gNnPc4	vejce
ani	ani	k8xC	ani
kuřata	kuře	k1gNnPc4	kuře
nezajímá	zajímat	k5eNaImIp3nS	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
Příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
jsou	být	k5eAaImIp3nP	být
kuřata	kuře	k1gNnPc1	kuře
pohlavně	pohlavně	k6eAd1	pohlavně
dospělá	dospělý	k2eAgNnPc1d1	dospělé
<g/>
,	,	kIx,	,
samci	samec	k1gMnSc3	samec
však	však	k9	však
většinou	většinou	k6eAd1	většinou
neobstojí	obstát	k5eNaPmIp3nP	obstát
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
se	s	k7c7	s
staršími	starší	k1gMnPc7	starší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
krocan	krocan	k1gMnSc1	krocan
dožívá	dožívat	k5eAaImIp3nS	dožívat
i	i	k9	i
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
ptáků	pták	k1gMnPc2	pták
pro	pro	k7c4	pro
obživu	obživa	k1gFnSc4	obživa
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
domestikován	domestikovat	k5eAaBmNgInS	domestikovat
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
chován	chovat	k5eAaImNgInS	chovat
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
krůty	krůta	k1gFnPc4	krůta
<g/>
)	)	kIx)	)
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světe	svět	k1gInSc5	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
různě	různě	k6eAd1	různě
vysazován	vysazován	k2eAgMnSc1d1	vysazován
do	do	k7c2	do
neobydlených	obydlený	k2eNgFnPc2d1	neobydlená
zalesněných	zalesněný	k2eAgFnPc2d1	zalesněná
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Volně	volně	k6eAd1	volně
žijící	žijící	k2eAgMnPc1d1	žijící
krocani	krocan	k1gMnPc1	krocan
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
přirozeného	přirozený	k2eAgInSc2d1	přirozený
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
chránění	chránění	k1gNnSc2	chránění
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
loveni	loven	k2eAgMnPc1d1	loven
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
anglofonních	anglofonní	k2eAgFnPc6d1	anglofonní
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
pečený	pečený	k2eAgMnSc1d1	pečený
krocan	krocan	k1gMnSc1	krocan
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
však	však	k9	však
domácí	domácí	k2eAgMnSc1d1	domácí
<g/>
)	)	kIx)	)
typickým	typický	k2eAgInSc7d1	typický
svátečním	sváteční	k2eAgInSc7d1	sváteční
pokrmem	pokrm	k1gInSc7	pokrm
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
a	a	k8xC	a
Den	den	k1gInSc4	den
díkůvzdání	díkůvzdání	k1gNnSc2	díkůvzdání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
krocan	krocan	k1gMnSc1	krocan
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgInPc7	dva
druhy	druh	k1gInPc7	druh
žijících	žijící	k2eAgInPc2d1	žijící
na	na	k7c6	na
oddělených	oddělený	k2eAgNnPc6d1	oddělené
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
krocan	krocan	k1gMnSc1	krocan
paví	paví	k2eAgMnSc1d1	paví
(	(	kIx(	(
<g/>
Meleagris	Meleagris	k1gInSc1	Meleagris
ocellata	ocelle	k1gNnPc1	ocelle
<g/>
)	)	kIx)	)
Cuvier	Cuvier	k1gInSc1	Cuvier
<g/>
,	,	kIx,	,
1820	[number]	k4	1820
</s>
</p>
<p>
<s>
krocan	krocan	k1gMnSc1	krocan
divoký	divoký	k2eAgMnSc1d1	divoký
(	(	kIx(	(
<g/>
Meleagris	Meleagris	k1gInSc1	Meleagris
gallopavo	gallopava	k1gFnSc5	gallopava
<g/>
)	)	kIx)	)
Linnaeus	Linnaeus	k1gMnSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
6	[number]	k4	6
podruhů	podruh	k1gMnPc2	podruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Meleagris	Meleagris	k1gInSc1	Meleagris
gallopavo	gallopava	k1gFnSc5	gallopava
<g/>
)	)	kIx)	)
L.	L.	kA	L.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
gallopavo	gallopava	k1gFnSc5	gallopava
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Meleagris	Meleagris	k1gInSc1	Meleagris
gallopavo	gallopava	k1gFnSc5	gallopava
<g/>
)	)	kIx)	)
L.	L.	kA	L.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
intermedia	intermedium	k1gNnPc1	intermedium
<g/>
)	)	kIx)	)
Sennett	Sennett	k1gInSc1	Sennett
<g/>
,	,	kIx,	,
1879	[number]	k4	1879
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Meleagris	Meleagris	k1gInSc1	Meleagris
gallopavo	gallopava	k1gFnSc5	gallopava
<g/>
)	)	kIx)	)
L.	L.	kA	L.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
merriami	merria	k1gFnPc7	merria
<g/>
)	)	kIx)	)
Nelson	Nelson	k1gMnSc1	Nelson
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Meleagris	Meleagris	k1gInSc1	Meleagris
gallopavo	gallopava	k1gFnSc5	gallopava
<g/>
)	)	kIx)	)
L.	L.	kA	L.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
mexicana	mexicana	k1gFnSc1	mexicana
<g/>
)	)	kIx)	)
Gould	Gould	k1gMnSc1	Gould
<g/>
,	,	kIx,	,
1856	[number]	k4	1856
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Meleagris	Meleagris	k1gInSc1	Meleagris
gallopavo	gallopava	k1gFnSc5	gallopava
<g/>
)	)	kIx)	)
L.	L.	kA	L.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
osceola	osceola	k1gFnSc1	osceola
<g/>
)	)	kIx)	)
Scott	Scott	k1gMnSc1	Scott
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Meleagris	Meleagris	k1gInSc1	Meleagris
gallopavo	gallopava	k1gFnSc5	gallopava
<g/>
)	)	kIx)	)
L.	L.	kA	L.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
silvestris	silvestris	k1gInSc1	silvestris
<g/>
)	)	kIx)	)
Vieillot	Vieillot	k1gInSc1	Vieillot
<g/>
,	,	kIx,	,
1817Z	[number]	k4	1817Z
krocana	krocan	k1gMnSc4	krocan
divokého	divoký	k2eAgInSc2d1	divoký
byla	být	k5eAaImAgFnS	být
domestifikaci	domestifikace	k1gFnSc4	domestifikace
vyšlechtěna	vyšlechtěn	k2eAgFnSc1d1	vyšlechtěna
</s>
</p>
<p>
<s>
krůta	krůta	k1gFnSc1	krůta
domácí	domácí	k2eAgFnSc1d1	domácí
(	(	kIx(	(
<g/>
Meleagris	Meleagris	k1gFnSc1	Meleagris
gallopavo	gallopava	k1gFnSc5	gallopava
<g/>
)	)	kIx)	)
L.	L.	kA	L.
f.	f.	k?	f.
(	(	kIx(	(
<g/>
domestica	domestica	k1gMnSc1	domestica
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
krocan	krocan	k1gMnSc1	krocan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
krocan	krocan	k1gMnSc1	krocan
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Meleagris	Meleagris	k1gInSc1	Meleagris
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
