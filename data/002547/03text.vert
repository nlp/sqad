<s>
Idiot	idiot	k1gMnSc1	idiot
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
lékařství	lékařství	k1gNnSc2	lékařství
zastaralé	zastaralý	k2eAgNnSc4d1	zastaralé
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
postiženého	postižený	k2eAgMnSc4d1	postižený
těžkou	těžký	k2eAgFnSc7d1	těžká
mentální	mentální	k2eAgFnSc7d1	mentální
retardací	retardace	k1gFnSc7	retardace
(	(	kIx(	(
<g/>
též	též	k9	též
slabomyslností	slabomyslnost	k1gFnSc7	slabomyslnost
či	či	k8xC	či
oligofrenií	oligofrenie	k1gFnSc7	oligofrenie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
s	s	k7c7	s
narušenými	narušený	k2eAgFnPc7d1	narušená
psychickými	psychický	k2eAgFnPc7d1	psychická
funkcemi	funkce	k1gFnPc7	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Možnosti	možnost	k1gFnPc1	možnost
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
při	při	k7c6	při
takto	takto	k6eAd1	takto
silném	silný	k2eAgNnSc6d1	silné
postižení	postižení	k1gNnSc6	postižení
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
silně	silně	k6eAd1	silně
omezené	omezený	k2eAgFnPc1d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
psychiatrie	psychiatrie	k1gFnSc1	psychiatrie
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
slovo	slovo	k1gNnSc1	slovo
idiot	idiot	k1gMnSc1	idiot
nepoužívá	používat	k5eNaImIp3nS	používat
zejména	zejména	k9	zejména
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jeho	jeho	k3xOp3gInSc2	jeho
pejorativního	pejorativní	k2eAgInSc2d1	pejorativní
výrazu	výraz	k1gInSc2	výraz
a	a	k8xC	a
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
jej	on	k3xPp3gInSc4	on
podle	podle	k7c2	podle
míry	míra	k1gFnSc2	míra
deficitu	deficit	k1gInSc2	deficit
označením	označení	k1gNnSc7	označení
těžká	těžký	k2eAgFnSc1d1	těžká
nebo	nebo	k8xC	nebo
hluboká	hluboký	k2eAgFnSc1d1	hluboká
mentální	mentální	k2eAgFnSc1d1	mentální
retardace	retardace	k1gFnSc1	retardace
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
podle	podle	k7c2	podle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
třídění	třídění	k1gNnSc2	třídění
mentální	mentální	k2eAgFnSc2d1	mentální
retardace	retardace	k1gFnSc2	retardace
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
idiocii	idiocie	k1gFnSc4	idiocie
IQ	iq	kA	iq
jedince	jedinec	k1gMnSc2	jedinec
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
35	[number]	k4	35
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
idiocii	idiocie	k1gFnSc4	idiocie
prostou	prostý	k2eAgFnSc4d1	prostá
<g/>
,	,	kIx,	,
při	při	k7c6	při
IQ	iq	kA	iq
nižším	nízký	k2eAgInSc6d2	nižší
než	než	k8xS	než
20	[number]	k4	20
o	o	k7c4	o
idiocii	idiocie	k1gFnSc4	idiocie
těžkou	těžký	k2eAgFnSc4d1	těžká
<g/>
.	.	kIx.	.
</s>
<s>
Snížené	snížený	k2eAgInPc1d1	snížený
IQ	iq	kA	iq
však	však	k8xC	však
není	být	k5eNaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
kritériem	kritérion	k1gNnSc7	kritérion
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
MKN	MKN	kA	MKN
10	[number]	k4	10
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
hluboké	hluboký	k2eAgFnSc2d1	hluboká
idiocie	idiocie	k1gFnSc2	idiocie
chápání	chápání	k1gNnSc1	chápání
a	a	k8xC	a
používání	používání	k1gNnSc1	používání
řeči	řeč	k1gFnSc2	řeč
omezeno	omezit	k5eAaPmNgNnS	omezit
na	na	k7c6	na
porozumění	porozumění	k1gNnSc2	porozumění
základním	základní	k2eAgInPc3d1	základní
příkazům	příkaz	k1gInPc3	příkaz
a	a	k8xC	a
na	na	k7c4	na
vyhovění	vyhovění	k1gNnSc2	vyhovění
jednoduchým	jednoduchý	k2eAgInPc3d1	jednoduchý
požadavkům	požadavek	k1gInPc3	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Postižený	postižený	k1gMnSc1	postižený
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zpravidla	zpravidla	k6eAd1	zpravidla
získat	získat	k5eAaPmF	získat
nejzákladnější	základní	k2eAgFnPc1d3	nejzákladnější
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
zrakově-prostorové	zrakověrostorový	k2eAgFnPc1d1	zrakově-prostorová
dovednosti	dovednost	k1gFnPc1	dovednost
v	v	k7c6	v
třídění	třídění	k1gNnSc1	třídění
a	a	k8xC	a
srovnávání	srovnávání	k1gNnSc1	srovnávání
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
při	při	k7c6	při
vhodném	vhodný	k2eAgInSc6d1	vhodný
dohledu	dohled	k1gInSc6	dohled
a	a	k8xC	a
vedení	vedení	k1gNnSc1	vedení
podílet	podílet	k5eAaImF	podílet
malým	malý	k2eAgNnSc7d1	malé
dílem	dílo	k1gNnSc7	dílo
na	na	k7c6	na
domácích	domácí	k2eAgInPc6d1	domácí
a	a	k8xC	a
praktických	praktický	k2eAgInPc6d1	praktický
úkonech	úkon	k1gInPc6	úkon
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
organickou	organický	k2eAgFnSc4d1	organická
etiologii	etiologie	k1gFnSc4	etiologie
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
těžké	těžký	k2eAgInPc4d1	těžký
neurologické	urologický	k2eNgInPc4d1	urologický
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgInPc4d1	jiný
tělesné	tělesný	k2eAgInPc4d1	tělesný
nedostatky	nedostatek	k1gInPc4	nedostatek
postihující	postihující	k2eAgFnSc1d1	postihující
hybnost	hybnost	k1gFnSc1	hybnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
epilepsie	epilepsie	k1gFnSc2	epilepsie
a	a	k8xC	a
poškození	poškození	k1gNnSc2	poškození
zraku	zrak	k1gInSc2	zrak
a	a	k8xC	a
sluchu	sluch	k1gInSc2	sluch
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
časté	častý	k2eAgNnSc1d1	časté
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
u	u	k7c2	u
pohyblivých	pohyblivý	k2eAgMnPc2d1	pohyblivý
pacientů	pacient	k1gMnPc2	pacient
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nejtěžší	těžký	k2eAgFnPc1d3	nejtěžší
formy	forma	k1gFnPc1	forma
pervazivních	pervazivní	k2eAgFnPc2d1	pervazivní
vývojových	vývojový	k2eAgFnPc2d1	vývojová
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
atypický	atypický	k2eAgInSc4d1	atypický
autismus	autismus	k1gInSc4	autismus
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
idiot	idiot	k1gMnSc1	idiot
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
idiō	idiō	k?	idiō
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
znamenalo	znamenat	k5eAaImAgNnS	znamenat
"	"	kIx"	"
<g/>
soukromník	soukromník	k1gMnSc1	soukromník
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
městských	městský	k2eAgInPc6d1	městský
státech	stát	k1gInPc6	stát
antického	antický	k2eAgNnSc2d1	antické
Řecka	Řecko	k1gNnSc2	Řecko
označovalo	označovat	k5eAaImAgNnS	označovat
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
starali	starat	k5eAaImAgMnP	starat
jen	jen	k9	jen
o	o	k7c4	o
své	svůj	k3xOyFgFnPc4	svůj
soukromé	soukromý	k2eAgFnPc4d1	soukromá
záležitosti	záležitost	k1gFnPc4	záležitost
a	a	k8xC	a
vyhýbali	vyhýbat	k5eAaImAgMnP	vyhýbat
se	se	k3xPyFc4	se
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
veřejných	veřejný	k2eAgFnPc6d1	veřejná
věcech	věc	k1gFnPc6	věc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dialozích	dialog	k1gInPc6	dialog
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
z	z	k7c2	z
Kusy	kus	k1gInPc7	kus
(	(	kIx(	(
<g/>
†	†	k?	†
1464	[number]	k4	1464
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
jako	jako	k9	jako
"	"	kIx"	"
<g/>
idiota	idiot	k1gMnSc4	idiot
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
soukromník	soukromník	k1gMnSc1	soukromník
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
velmi	velmi	k6eAd1	velmi
bystrý	bystrý	k2eAgMnSc1d1	bystrý
řemeslník	řemeslník	k1gMnSc1	řemeslník
bez	bez	k7c2	bez
formálního	formální	k2eAgNnSc2d1	formální
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
idiot	idiot	k1gMnSc1	idiot
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
též	též	k9	též
jako	jako	k9	jako
nadávka	nadávka	k1gFnSc1	nadávka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
pochází	pocházet	k5eAaImIp3nS	pocházet
dále	daleko	k6eAd2	daleko
nepříliš	příliš	k6eNd1	příliš
často	často	k6eAd1	často
používaný	používaný	k2eAgInSc1d1	používaný
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
idiot	idiot	k1gMnSc1	idiot
savant	savant	k1gMnSc1	savant
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
učený	učený	k2eAgMnSc1d1	učený
idiot	idiot	k1gMnSc1	idiot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označující	označující	k2eAgFnPc1d1	označující
osoby	osoba	k1gFnPc1	osoba
s	s	k7c7	s
mentálním	mentální	k2eAgInSc7d1	mentální
deficitem	deficit	k1gInSc7	deficit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	s	k7c7	s
současným	současný	k2eAgNnSc7d1	současné
vysokým	vysoký	k2eAgNnSc7d1	vysoké
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jednostranně	jednostranně	k6eAd1	jednostranně
zaměřeným	zaměřený	k2eAgNnSc7d1	zaměřené
nadáním	nadání	k1gNnSc7	nadání
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
zapamatování	zapamatování	k1gNnSc4	zapamatování
si	se	k3xPyFc3	se
zdánlivě	zdánlivě	k6eAd1	zdánlivě
neomezeného	omezený	k2eNgNnSc2d1	neomezené
množství	množství	k1gNnSc2	množství
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
výjimečnou	výjimečný	k2eAgFnSc4d1	výjimečná
schopnost	schopnost	k1gFnSc4	schopnost
numerických	numerický	k2eAgFnPc2d1	numerická
operací	operace	k1gFnPc2	operace
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
sčítání	sčítání	k1gNnSc2	sčítání
<g/>
)	)	kIx)	)
či	či	k8xC	či
malování	malování	k1gNnSc1	malování
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
symptomatiky	symptomatika	k1gFnSc2	symptomatika
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
sebepřeceňování	sebepřeceňování	k1gNnSc1	sebepřeceňování
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
kritického	kritický	k2eAgNnSc2d1	kritické
myšlení	myšlení	k1gNnSc2	myšlení
a	a	k8xC	a
zařazování	zařazování	k1gNnSc2	zařazování
vědomostí	vědomost	k1gFnPc2	vědomost
do	do	k7c2	do
širších	široký	k2eAgFnPc2d2	širší
souvislostí	souvislost	k1gFnPc2	souvislost
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
rysy	rys	k1gInPc1	rys
podobné	podobný	k2eAgInPc1d1	podobný
některým	některý	k3yIgInPc3	některý
poruchám	porucha	k1gFnPc3	porucha
autistického	autistický	k2eAgNnSc2d1	autistické
spektra	spektrum	k1gNnSc2	spektrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mentální	mentální	k2eAgFnSc1d1	mentální
retardace	retardace	k1gFnSc1	retardace
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
idiot	idiot	k1gMnSc1	idiot
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
