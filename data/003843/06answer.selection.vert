<s>
Největší	veliký	k2eAgFnSc4d3	veliký
sbírku	sbírka	k1gFnSc4	sbírka
planých	planý	k2eAgFnPc2d1	planá
i	i	k8xC	i
kulturních	kulturní	k2eAgFnPc2d1	kulturní
růží	růž	k1gFnPc2	růž
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
navštívit	navštívit	k5eAaPmF	navštívit
v	v	k7c6	v
Botanické	botanický	k2eAgFnSc6d1	botanická
zahradě	zahrada	k1gFnSc6	zahrada
Chotobuz	Chotobuz	k1gInSc4	Chotobuz
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
kolem	kolem	k7c2	kolem
850	[number]	k4	850
odrůd	odrůda	k1gFnPc2	odrůda
kulturních	kulturní	k2eAgFnPc2d1	kulturní
růží	růž	k1gFnPc2	růž
a	a	k8xC	a
přes	přes	k7c4	přes
100	[number]	k4	100
taxonů	taxon	k1gInPc2	taxon
původních	původní	k2eAgInPc2d1	původní
botanických	botanický	k2eAgInPc2d1	botanický
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
