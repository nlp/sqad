<p>
<s>
Hyacint	hyacint	k1gInSc1	hyacint
(	(	kIx(	(
<g/>
Hyacinthus	Hyacinthus	k1gInSc1	Hyacinthus
L.	L.	kA	L.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
jednoděložných	jednoděložný	k2eAgFnPc2d1	jednoděložná
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
chřestovité	chřestovitý	k2eAgFnSc2d1	chřestovitý
(	(	kIx(	(
<g/>
Asparagaceae	Asparagacea	k1gFnSc2	Asparagacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
hyacintovité	hyacintovitý	k2eAgFnSc2d1	hyacintovitý
(	(	kIx(	(
<g/>
Hyacinthaceae	Hyacinthacea	k1gFnSc2	Hyacinthacea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
liliovité	liliovitý	k2eAgFnSc2d1	liliovitá
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
pojetí	pojetí	k1gNnSc6	pojetí
(	(	kIx(	(
<g/>
Liliaceae	Liliaceae	k1gNnSc1	Liliaceae
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
rodu	rod	k1gInSc2	rod
hyacint	hyacint	k1gInSc1	hyacint
(	(	kIx(	(
<g/>
Hyacinthus	Hyacinthus	k1gInSc1	Hyacinthus
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
příbuzného	příbuzný	k2eAgInSc2d1	příbuzný
rodu	rod	k1gInSc2	rod
Hyacinthella	Hyacinthello	k1gNnSc2	Hyacinthello
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vytrvalé	vytrvalý	k2eAgFnPc4d1	vytrvalá
pozemní	pozemní	k2eAgFnPc4d1	pozemní
byliny	bylina	k1gFnPc4	bylina
<g/>
,	,	kIx,	,
s	s	k7c7	s
cibulemi	cibule	k1gFnPc7	cibule
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
rostliny	rostlina	k1gFnPc1	rostlina
s	s	k7c7	s
oboupohlavnými	oboupohlavný	k2eAgInPc7d1	oboupohlavný
květy	květ	k1gInPc7	květ
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
přízemní	přízemní	k2eAgFnSc6d1	přízemní
růžici	růžice	k1gFnSc6	růžice
(	(	kIx(	(
<g/>
cca	cca	kA	cca
2-6	[number]	k4	2-6
listů	list	k1gInPc2	list
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
,	,	kIx,	,
přisedlé	přisedlý	k2eAgFnPc1d1	přisedlá
<g/>
,	,	kIx,	,
s	s	k7c7	s
listovými	listový	k2eAgFnPc7d1	listová
pochvami	pochva	k1gFnPc7	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Čepele	čepel	k1gFnPc1	čepel
listů	list	k1gInPc2	list
jsou	být	k5eAaImIp3nP	být
celokrajné	celokrajný	k2eAgFnPc1d1	celokrajná
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
čárkovité	čárkovitý	k2eAgFnPc1d1	čárkovitá
až	až	k8xS	až
kopinaté	kopinatý	k2eAgFnPc1d1	kopinatá
<g/>
,	,	kIx,	,
žilnatina	žilnatina	k1gFnSc1	žilnatina
je	být	k5eAaImIp3nS	být
souběžná	souběžný	k2eAgFnSc1d1	souběžná
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
oboupohlavné	oboupohlavný	k2eAgInPc1d1	oboupohlavný
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
květenstvích	květenství	k1gNnPc6	květenství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vrcholových	vrcholový	k2eAgInPc6d1	vrcholový
hroznech	hrozen	k1gInPc6	hrozen
<g/>
.	.	kIx.	.
</s>
<s>
Okvětí	okvětí	k1gNnSc1	okvětí
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
6	[number]	k4	6
okvětních	okvětní	k2eAgInPc2d1	okvětní
lístků	lístek	k1gInPc2	lístek
ve	v	k7c6	v
2	[number]	k4	2
přeslenech	přeslen	k1gInPc6	přeslen
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
srostlé	srostlý	k2eAgFnSc2d1	srostlá
v	v	k7c4	v
krátkou	krátký	k2eAgFnSc4d1	krátká
trubku	trubka	k1gFnSc4	trubka
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
modré	modrý	k2eAgNnSc1d1	modré
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc1	některý
kultivary	kultivar	k1gInPc1	kultivar
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
odlišných	odlišný	k2eAgFnPc2d1	odlišná
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinek	tyčinka	k1gFnPc2	tyčinka
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Gyneceum	Gyneceum	k1gNnSc1	Gyneceum
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
ze	z	k7c2	z
3	[number]	k4	3
plodolistů	plodolist	k1gInPc2	plodolist
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
synkarpní	synkarpnit	k5eAaPmIp3nP	synkarpnit
<g/>
,	,	kIx,	,
semeník	semeník	k1gInSc1	semeník
je	být	k5eAaImIp3nS	být
svrchní	svrchní	k2eAgInSc1d1	svrchní
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
tobolka	tobolka	k1gFnSc1	tobolka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
asi	asi	k9	asi
3	[number]	k4	3
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
žádný	žádný	k3yNgInSc4	žádný
druh	druh	k1gInSc4	druh
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
hyacint	hyacint	k1gInSc1	hyacint
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
hyacint	hyacint	k1gInSc1	hyacint
východní	východní	k2eAgInSc1d1	východní
(	(	kIx(	(
<g/>
Hyacintus	Hyacintus	k1gInSc1	Hyacintus
orientalis	orientalis	k1gInSc1	orientalis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
pěstovaná	pěstovaný	k2eAgFnSc1d1	pěstovaná
okrasná	okrasný	k2eAgFnSc1d1	okrasná
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hyacint	hyacinta	k1gFnPc2	hyacinta
v	v	k7c6	v
zahradnictví	zahradnictví	k1gNnSc6	zahradnictví
==	==	k?	==
</s>
</p>
<p>
<s>
Čím	čí	k3xOyRgNnSc7	čí
větší	veliký	k2eAgFnSc1d2	veliký
je	být	k5eAaImIp3nS	být
cibule	cibule	k1gFnSc1	cibule
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
je	být	k5eAaImIp3nS	být
potom	potom	k6eAd1	potom
květenství	květenství	k1gNnSc1	květenství
<g/>
.	.	kIx.	.
</s>
<s>
Množení	množení	k1gNnSc4	množení
těchto	tento	k3xDgFnPc2	tento
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
,	,	kIx,	,
provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
naříznutím	naříznutí	k1gNnSc7	naříznutí
cibule	cibule	k1gFnSc2	cibule
křížem	křížem	k6eAd1	křížem
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
použijí	použít	k5eAaPmIp3nP	použít
stimulátory	stimulátor	k1gInPc7	stimulátor
a	a	k8xC	a
na	na	k7c4	na
cibuli	cibule	k1gFnSc4	cibule
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
dceřiné	dceřiný	k2eAgFnPc1d1	dceřiná
cibulky	cibulka	k1gFnPc1	cibulka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dopěstovávají	dopěstovávat	k5eAaImIp3nP	dopěstovávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
druhů	druh	k1gInPc2	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Hyacinthus	Hyacinthus	k1gInSc4	Hyacinthus
litwinowii	litwinowie	k1gFnSc4	litwinowie
–	–	k?	–
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Asie	Asie	k1gFnSc2	Asie
</s>
</p>
<p>
<s>
Hyacinthus	Hyacinthus	k1gInSc1	Hyacinthus
orientalis	orientalis	k1gInSc1	orientalis
(	(	kIx(	(
<g/>
hyacint	hyacint	k1gInSc1	hyacint
východní	východní	k2eAgInSc1d1	východní
<g/>
)	)	kIx)	)
–	–	k?	–
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Asie	Asie	k1gFnSc2	Asie
</s>
</p>
<p>
<s>
Hyacinthus	Hyacinthus	k1gMnSc1	Hyacinthus
transcaspicus	transcaspicus	k1gMnSc1	transcaspicus
–	–	k?	–
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Asie	Asie	k1gFnSc2	Asie
</s>
</p>
<p>
<s>
a	a	k8xC	a
možná	možná	k9	možná
další	další	k2eAgMnPc1d1	další
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Klíč	klíč	k1gInSc1	klíč
ke	k	k7c3	k
Květeně	květena	k1gFnSc3	květena
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Kubát	Kubát	k1gMnSc1	Kubát
K.	K.	kA	K.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
Květena	květena	k1gFnSc1	květena
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
Dostál	Dostál	k1gMnSc1	Dostál
J.	J.	kA	J.
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hyacint	hyacinta	k1gFnPc2	hyacinta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hyacint	hyacinta	k1gFnPc2	hyacinta
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Hyacinthus	Hyacinthus	k1gInSc1	Hyacinthus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Hyacint	hyacint	k1gInSc1	hyacint
na	na	k7c6	na
zahradnickém	zahradnický	k2eAgInSc6d1	zahradnický
portále	portál	k1gInSc6	portál
Zeleň	zeleň	k1gFnSc1	zeleň
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
http://delta-intkey.com/angio/www/index.htm	[url]	k6eAd1	http://delta-intkey.com/angio/www/index.htm
</s>
</p>
<p>
<s>
http://rbg-web2.rbge.org.uk/FE/fe.html	[url]	k1gMnSc1	http://rbg-web2.rbge.org.uk/FE/fe.html
</s>
</p>
