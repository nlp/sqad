<s>
Obec	obec	k1gFnSc1	obec
Běstvina	Běstvina	k1gFnSc1	Běstvina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Chrudim	Chrudim	k1gFnSc1	Chrudim
v	v	k7c6	v
Pardubickém	pardubický	k2eAgInSc6d1	pardubický
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
498	[number]	k4	498
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
přírodovědci	přírodovědec	k1gMnPc7	přírodovědec
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
každoroční	každoroční	k2eAgNnSc1d1	každoroční
odborné	odborný	k2eAgNnSc1d1	odborné
soustředění	soustředění	k1gNnSc1	soustředění
nejúspěšnějších	úspěšný	k2eAgMnPc2d3	nejúspěšnější
řešitelů	řešitel	k1gMnPc2	řešitel
krajských	krajský	k2eAgMnPc2d1	krajský
kol	kol	k7c2	kol
biologické	biologický	k2eAgFnSc2d1	biologická
a	a	k8xC	a
chemické	chemický	k2eAgFnSc2d1	chemická
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1137	[number]	k4	1137
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
Běstvina	Běstvin	k2eAgFnSc1d1	Běstvin
Kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
před	před	k7c7	před
zámkem	zámek	k1gInSc7	zámek
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
Památný	památný	k2eAgInSc4d1	památný
platan	platan	k1gInSc4	platan
CHKO	CHKO	kA	CHKO
Železné	železný	k2eAgFnSc2d1	železná
hory	hora	k1gFnSc2	hora
Běstvina	Běstvina	k1gFnSc1	Běstvina
Pařížov	Pařížov	k1gInSc1	Pařížov
Rostejn	Rostejn	k1gMnSc1	Rostejn
Spačice	Spačice	k1gFnSc1	Spačice
Vestec	Vestec	k1gInSc1	Vestec
Galerie	galerie	k1gFnSc1	galerie
Běstvina	Běstvina	k1gFnSc1	Běstvina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Běstvina	Běstvin	k2eAgNnSc2d1	Běstvin
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Ofliciální	Ofliciální	k2eAgFnSc2d1	Ofliciální
stránky	stránka	k1gFnSc2	stránka
Letního	letní	k2eAgNnSc2d1	letní
odborného	odborný	k2eAgNnSc2d1	odborné
soustředění	soustředění	k1gNnSc2	soustředění
biologů	biolog	k1gMnPc2	biolog
a	a	k8xC	a
chemiků	chemik	k1gMnPc2	chemik
Běstvina	Běstvin	k2eAgInSc2d1	Běstvin
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
mikroregionu	mikroregion	k1gInSc2	mikroregion
Železné	železný	k2eAgFnPc4d1	železná
hory	hora	k1gFnPc4	hora
zámek	zámek	k1gInSc4	zámek
Běstvina	Běstvin	k2eAgInSc2d1	Běstvin
na	na	k7c4	na
Hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
