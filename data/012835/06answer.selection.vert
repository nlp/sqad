<s>
Myš	myš	k1gFnSc1	myš
je	být	k5eAaImIp3nS	být
české	český	k2eAgNnSc4d1	české
rodové	rodový	k2eAgNnSc4d1	rodové
jméno	jméno	k1gNnSc4	jméno
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
drobných	drobný	k2eAgMnPc2d1	drobný
hlodavců	hlodavec	k1gMnPc2	hlodavec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
myšovitých	myšovití	k1gMnPc2	myšovití
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
nomenklatuře	nomenklatura	k1gFnSc6	nomenklatura
rozřazených	rozřazený	k2eAgFnPc2d1	rozřazená
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
