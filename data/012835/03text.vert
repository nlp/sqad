<p>
<s>
Myš	myš	k1gFnSc1	myš
je	být	k5eAaImIp3nS	být
české	český	k2eAgNnSc4d1	české
rodové	rodový	k2eAgNnSc4d1	rodové
jméno	jméno	k1gNnSc4	jméno
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
drobných	drobný	k2eAgMnPc2d1	drobný
hlodavců	hlodavec	k1gMnPc2	hlodavec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
myšovitých	myšovití	k1gMnPc2	myšovití
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
nomenklatuře	nomenklatura	k1gFnSc6	nomenklatura
rozřazených	rozřazený	k2eAgFnPc2d1	rozřazená
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
myš	myš	k1gFnSc1	myš
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
kosmopolitně	kosmopolitně	k6eAd1	kosmopolitně
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
myš	myš	k1gFnSc1	myš
domácí	domácí	k1gMnSc1	domácí
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
musculus	musculus	k1gInSc1	musculus
<g/>
)	)	kIx)	)
-	-	kIx~	-
hospodářský	hospodářský	k2eAgMnSc1d1	hospodářský
škůdce	škůdce	k1gMnSc1	škůdce
<g/>
,	,	kIx,	,
mnohými	mnohé	k1gNnPc7	mnohé
nevítaný	vítaný	k2eNgMnSc1d1	nevítaný
host	host	k1gMnSc1	host
v	v	k7c6	v
domě	dům	k1gInSc6	dům
i	i	k8xC	i
hospodářských	hospodářský	k2eAgFnPc6d1	hospodářská
budovách	budova	k1gFnPc6	budova
<g/>
,	,	kIx,	,
invazní	invazní	k2eAgInSc1d1	invazní
druh	druh	k1gInSc1	druh
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
)	)	kIx)	)
i	i	k9	i
domácí	domácí	k2eAgMnSc1d1	domácí
mazlíček	mazlíček	k1gMnSc1	mazlíček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Myši	myš	k1gFnPc4	myš
rodu	rod	k1gInSc2	rod
Mus	Musa	k1gFnPc2	Musa
jsou	být	k5eAaImIp3nP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
do	do	k7c2	do
podčeledi	podčeleď	k1gFnSc2	podčeleď
pravých	pravý	k2eAgFnPc2d1	pravá
myší	myš	k1gFnPc2	myš
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
krysami	krysa	k1gFnPc7	krysa
a	a	k8xC	a
potkany	potkan	k1gMnPc7	potkan
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
podčeledi	podčeleď	k1gFnSc2	podčeleď
Muridae	Murida	k1gInSc2	Murida
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
dalších	další	k2eAgInPc2d1	další
30	[number]	k4	30
rodů	rod	k1gInPc2	rod
myší	myš	k1gFnPc2	myš
<g/>
.	.	kIx.	.
7	[number]	k4	7
rodů	rod	k1gInPc2	rod
myší	myš	k1gFnPc2	myš
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
podčeledi	podčeleď	k1gFnSc2	podčeleď
myší	myš	k1gFnPc2	myš
stromových	stromový	k2eAgFnPc2d1	stromová
a	a	k8xC	a
2	[number]	k4	2
rody	rod	k1gInPc7	rod
do	do	k7c2	do
podčeledi	podčeleď	k1gFnSc2	podčeleď
myší	myš	k1gFnPc2	myš
lamelozubých	lamelozubý	k2eAgFnPc2d1	lamelozubý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc1	taxonomie
rodu	rod	k1gInSc2	rod
Mus	Musa	k1gFnPc2	Musa
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
Mus	Musa	k1gFnPc2	Musa
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
38	[number]	k4	38
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Mus	Musa	k1gFnPc2	Musa
</s>
</p>
<p>
<s>
podrod	podrod	k1gInSc1	podrod
Pyromys	Pyromysa	k1gFnPc2	Pyromysa
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
platythrix	platythrix	k1gInSc4	platythrix
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
saxicola	saxicola	k1gFnSc1	saxicola
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
Phillipsova	Phillipsův	k2eAgFnSc1d1	Phillipsova
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
philipsi	philips	k1gMnSc5	philips
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
Shortridgeova	Shortridgeův	k2eAgFnSc1d1	Shortridgeův
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
shortridgei	shortridgei	k6eAd1	shortridgei
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
Fernandova	Fernandův	k2eAgFnSc1d1	Fernandova
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
fernandoni	fernandon	k1gMnPc1	fernandon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podrod	podrod	k1gInSc1	podrod
Coelomys	Coelomysa	k1gFnPc2	Coelomysa
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
Mayorova	Mayorův	k2eAgFnSc1d1	Mayorův
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
mayori	mayori	k6eAd1	mayori
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
pahari	pahar	k1gFnSc2	pahar
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
crociduroides	crociduroidesa	k1gFnPc2	crociduroidesa
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
vulkánová	vulkánový	k2eAgFnSc1d1	vulkánový
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
vulcani	vulcan	k1gMnPc1	vulcan
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
famulus	famulus	k1gMnSc1	famulus
</s>
</p>
<p>
<s>
podrod	podrod	k1gInSc1	podrod
Mus	Musa	k1gFnPc2	Musa
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
caroli	caroli	k6eAd1	caroli
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
plavohnědá	plavohnědý	k2eAgFnSc1d1	plavohnědý
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
cervicolor	cervicolor	k1gInSc1	cervicolor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
Cookova	Cookov	k1gInSc2	Cookov
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
cookii	cookie	k1gFnSc6	cookie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
boguda	bogudo	k1gNnSc2	bogudo
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
booduga	booduga	k1gFnSc1	booduga
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
terricolor	terricolora	k1gFnPc2	terricolora
</s>
</p>
<p>
<s>
myš	myš	k1gFnSc1	myš
domácí	domácí	k1gFnSc2	domácí
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
musculus	musculus	k1gInSc1	musculus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
myš	myš	k1gFnSc1	myš
středozemní	středozemní	k2eAgFnSc2d1	středozemní
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
spretus	spretus	k1gInSc1	spretus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
makedonská	makedonský	k2eAgFnSc1d1	makedonská
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
macedonicus	macedonicus	k1gInSc1	macedonicus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
spicilegus	spicilegus	k1gMnSc1	spicilegus
</s>
</p>
<p>
<s>
podrod	podrod	k1gInSc1	podrod
Nannomys	Nannomysa	k1gFnPc2	Nannomysa
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
Callewaertova	Callewaertův	k2eAgFnSc1d1	Callewaertův
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
callewaerti	callewaert	k1gMnPc1	callewaert
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
setulosus	setulosus	k1gMnSc1	setulosus
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
šedobřichá	šedobřichat	k5eAaPmIp3nS	šedobřichat
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
triton	triton	k1gInSc1	triton
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
ropuší	ropuší	k2eAgFnSc1d1	ropuší
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
bufo	bufa	k1gFnSc5	bufa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
tenellus	tenellus	k1gMnSc1	tenellus
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
haussa	haussa	k1gFnSc1	haussa
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
Mattheyova	Mattheyovo	k1gNnSc2	Mattheyovo
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
mattheyi	mattheyi	k6eAd1	mattheyi
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
indutus	indutus	k1gMnSc1	indutus
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
Setzerova	Setzerův	k2eAgFnSc1d1	Setzerův
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
setzeri	setzeri	k6eAd1	setzeri
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
musculoides	musculoidesa	k1gFnPc2	musculoidesa
</s>
</p>
<p>
<s>
myš	myš	k1gFnSc1	myš
africká	africký	k2eAgFnSc1d1	africká
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
minutoides	minutoides	k1gInSc1	minutoides
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
orangiae	orangiaat	k5eAaPmIp3nS	orangiaat
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
mahomet	mahomet	k1gInSc1	mahomet
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
sorella	sorello	k1gNnSc2	sorello
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
kasajská	kasajský	k2eAgFnSc1d1	kasajský
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
kasaicus	kasaicus	k1gInSc1	kasaicus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
Neaveova	Neaveův	k2eAgFnSc1d1	Neaveův
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
neavei	avei	k6eNd1	avei
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
oubanguii	oubanguie	k1gFnSc3	oubanguie
</s>
</p>
<p>
<s>
Mus	Musa	k1gFnPc2	Musa
goundae	goundaat	k5eAaPmIp3nS	goundaat
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
Baouleova	Baouleův	k2eAgFnSc1d1	Baouleův
(	(	kIx(	(
<g/>
Mus	Musa	k1gFnPc2	Musa
baoulei	baoulei	k6eAd1	baoulei
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Jiné	jiný	k2eAgInPc1d1	jiný
rody	rod	k1gInPc1	rod
myší	myš	k1gFnSc7	myš
z	z	k7c2	z
podčeledi	podčeleď	k1gFnSc2	podčeleď
Muridae	Murida	k1gFnSc2	Murida
==	==	k?	==
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Abditomys	Abditomysa	k1gFnPc2	Abditomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Acomys	Acomysa	k1gFnPc2	Acomysa
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
bodlinatá	bodlinatý	k2eAgFnSc1d1	bodlinatá
(	(	kIx(	(
<g/>
Acomys	Acomys	k1gInSc1	Acomys
cahirinus	cahirinus	k1gInSc1	cahirinus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Arvicanthis	Arvicanthis	k1gFnSc2	Arvicanthis
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Canariomys	Canariomysa	k1gFnPc2	Canariomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Chiropodomys	Chiropodomysa	k1gFnPc2	Chiropodomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Chiruromys	Chiruromysa	k1gFnPc2	Chiruromysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Coccymys	Coccymysa	k1gFnPc2	Coccymysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Haeromys	Haeromysa	k1gFnPc2	Haeromysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Hapalomys	Hapalomysa	k1gFnPc2	Hapalomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Heimyscus	Heimyscus	k1gInSc1	Heimyscus
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Hydromys	Hydromysa	k1gFnPc2	Hydromysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Hylomyscus	Hylomyscus	k1gInSc1	Hylomyscus
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Kadarsanomys	Kadarsanomysa	k1gFnPc2	Kadarsanomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Leggadina	Leggadin	k2eAgInSc2d1	Leggadin
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Lemniscomys	Lemniscomysa	k1gFnPc2	Lemniscomysa
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
páskovaná	páskovaný	k2eAgFnSc1d1	páskovaná
(	(	kIx(	(
<g/>
Lemniscomys	Lemniscomys	k1gInSc1	Lemniscomys
striatus	striatus	k1gInSc1	striatus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc1	myš
zebrovaná	zebrovaný	k2eAgFnSc1d1	zebrovaná
(	(	kIx(	(
<g/>
Lemniscomys	Lemniscomys	k1gInSc1	Lemniscomys
barbarus	barbarus	k1gInSc1	barbarus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Lorentzimys	Lorentzimysa	k1gFnPc2	Lorentzimysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Malpaisomys	Malpaisomysa	k1gFnPc2	Malpaisomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Mayermys	Mayermysa	k1gFnPc2	Mayermysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Microhydromys	Microhydromysa	k1gFnPc2	Microhydromysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Muriculus	Muriculus	k1gInSc1	Muriculus
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Neohydromys	Neohydromysa	k1gFnPc2	Neohydromysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Notomys	Notomysa	k1gFnPc2	Notomysa
</s>
</p>
<p>
<s>
Klokanomyš	Klokanomyš	k5eAaPmIp2nS	Klokanomyš
spinifexová	spinifexový	k2eAgFnSc1d1	spinifexový
(	(	kIx(	(
<g/>
Nanomys	Nanomys	k1gInSc1	Nanomys
alexis	alexis	k1gFnSc2	alexis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Oenomys	Oenomysa	k1gFnPc2	Oenomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Phloeomys	Phloeomysa	k1gFnPc2	Phloeomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Oenomys	Oenomysa	k1gFnPc2	Oenomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Pogonomelomys	Pogonomelomysa	k1gFnPc2	Pogonomelomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Pogonomys	Pogonomysa	k1gFnPc2	Pogonomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Pseudohydromys	Pseudohydromysa	k1gFnPc2	Pseudohydromysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Rhabdomys	Rhabdomysa	k1gFnPc2	Rhabdomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Rhynchomys	Rhynchomysa	k1gFnPc2	Rhynchomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Spelaeomys	Spelaeomysa	k1gFnPc2	Spelaeomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Vandeleuria	Vandeleurium	k1gNnSc2	Vandeleurium
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Vernaya	Vernay	k1gInSc2	Vernay
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Zelotomys	Zelotomysa	k1gFnPc2	Zelotomysa
</s>
</p>
<p>
<s>
==	==	k?	==
Stromové	stromový	k2eAgFnSc2d1	stromová
myši	myš	k1gFnSc2	myš
==	==	k?	==
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Dendromus	Dendromus	k1gInSc1	Dendromus
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Dendroprionomys	Dendroprionomysa	k1gFnPc2	Dendroprionomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Deomys	Deomysa	k1gFnPc2	Deomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Leimacomys	Leimacomysa	k1gFnPc2	Leimacomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Malacothrix	Malacothrix	k1gInSc1	Malacothrix
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Megadendromus	Megadendromus	k1gInSc1	Megadendromus
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Prionomys	Prionomysa	k1gFnPc2	Prionomysa
</s>
</p>
<p>
<s>
==	==	k?	==
Lamelozubé	Lamelozubý	k2eAgFnSc2d1	Lamelozubý
myši	myš	k1gFnSc2	myš
==	==	k?	==
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Otomys	Otomysa	k1gFnPc2	Otomysa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Parotomys	Parotomysa	k1gFnPc2	Parotomysa
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
myš	myš	k1gFnSc1	myš
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Myš	myš	k1gFnSc1	myš
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
myš	myš	k1gFnSc1	myš
ve	v	k7c6	v
WikislovníkuSystém	WikislovníkuSystý	k2eAgInSc6d1	WikislovníkuSystý
podčeledi	podčeleď	k1gFnSc6	podčeleď
Murinae	Murinae	k1gFnSc7	Murinae
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Mus	Musa	k1gFnPc2	Musa
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Systém	systém	k1gInSc1	systém
podčeledi	podčeleď	k1gFnSc2	podčeleď
Murinae	Murina	k1gFnSc2	Murina
</s>
</p>
