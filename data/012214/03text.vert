<p>
<s>
Křídlok	Křídlok	k1gInSc1	Křídlok
(	(	kIx(	(
<g/>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
bobovité	bobovitý	k2eAgFnSc2d1	bobovitá
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnPc1d1	vysoká
a	a	k8xC	a
s	s	k7c7	s
opěrnými	opěrný	k2eAgInPc7d1	opěrný
pilíři	pilíř	k1gInPc7	pilíř
u	u	k7c2	u
paty	pata	k1gFnSc2	pata
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
mimo	mimo	k7c4	mimo
Austrálie	Austrálie	k1gFnPc4	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
nápadné	nápadný	k2eAgInPc4d1	nápadný
ploché	plochý	k2eAgInPc4d1	plochý
okrouhlé	okrouhlý	k2eAgInPc4d1	okrouhlý
plody	plod	k1gInPc4	plod
se	s	k7c7	s
širokým	široký	k2eAgNnSc7d1	široké
křídlem	křídlo	k1gNnSc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
vyhledávané	vyhledávaný	k2eAgInPc1d1	vyhledávaný
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
ceněné	ceněný	k2eAgNnSc1d1	ceněné
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
křídlok	křídlok	k1gInSc1	křídlok
jsou	být	k5eAaImIp3nP	být
středně	středně	k6eAd1	středně
velké	velký	k2eAgInPc1d1	velký
až	až	k8xS	až
vysoké	vysoký	k2eAgInPc1d1	vysoký
<g/>
,	,	kIx,	,
opadavé	opadavý	k2eAgInPc1d1	opadavý
nebo	nebo	k8xC	nebo
stálezelené	stálezelený	k2eAgInPc1d1	stálezelený
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
u	u	k7c2	u
paty	pata	k1gFnSc2	pata
kmene	kmen	k1gInSc2	kmen
často	často	k6eAd1	často
s	s	k7c7	s
opěrnými	opěrný	k2eAgInPc7d1	opěrný
pilíři	pilíř	k1gInPc7	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
hladká	hladký	k2eAgFnSc1d1	hladká
nebo	nebo	k8xC	nebo
mělce	mělce	k6eAd1	mělce
až	až	k9	až
hluboce	hluboko	k6eAd1	hluboko
brázditá	brázditý	k2eAgFnSc1d1	brázditá
a	a	k8xC	a
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
červenou	červený	k2eAgFnSc4d1	červená
pryskyřici	pryskyřice	k1gFnSc4	pryskyřice
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
deštníkovitého	deštníkovitý	k2eAgInSc2d1	deštníkovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Listy	lista	k1gFnPc1	lista
jsou	být	k5eAaImIp3nP	být
střídavé	střídavý	k2eAgFnPc1d1	střídavá
<g/>
,	,	kIx,	,
lichozpeřené	lichozpeřený	k2eAgFnPc1d1	lichozpeřený
<g/>
,	,	kIx,	,
složené	složený	k2eAgFnPc1d1	složená
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
5	[number]	k4	5
až	až	k9	až
11	[number]	k4	11
střídavých	střídavý	k2eAgFnPc2d1	střídavá
nebo	nebo	k8xC	nebo
řidčeji	řídce	k6eAd2	řídce
téměř	téměř	k6eAd1	téměř
vstřícných	vstřícný	k2eAgInPc2d1	vstřícný
celokrajných	celokrajný	k2eAgInPc2d1	celokrajný
lístků	lístek	k1gInPc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Palisty	palist	k1gInPc1	palist
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgInPc1d1	drobný
<g/>
,	,	kIx,	,
opadavé	opadavý	k2eAgInPc1d1	opadavý
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnPc1d1	žlutá
až	až	k6eAd1	až
žlutooranžové	žlutooranžový	k2eAgFnPc1d1	žlutooranžová
<g/>
,	,	kIx,	,
max	max	kA	max
<g/>
.	.	kIx.	.
2	[number]	k4	2
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
motýlovité	motýlovitý	k2eAgFnPc1d1	motýlovitá
<g/>
,	,	kIx,	,
v	v	k7c6	v
bohatých	bohatý	k2eAgInPc6d1	bohatý
úžlabních	úžlabní	k2eAgInPc6d1	úžlabní
hroznech	hrozen	k1gInPc6	hrozen
nebo	nebo	k8xC	nebo
latách	lata	k1gFnPc6	lata
<g/>
.	.	kIx.	.
</s>
<s>
Kalich	kalich	k1gInSc1	kalich
je	být	k5eAaImIp3nS	být
zakončen	zakončit	k5eAaPmNgInS	zakončit
5	[number]	k4	5
drobnými	drobný	k2eAgInPc7d1	drobný
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
kalich	kalich	k1gInSc1	kalich
<g/>
.	.	kIx.	.
</s>
<s>
Pavéza	pavéza	k1gFnSc1	pavéza
je	být	k5eAaImIp3nS	být
vejčitá	vejčitý	k2eAgFnSc1d1	vejčitá
až	až	k6eAd1	až
okrouhlá	okrouhlý	k2eAgFnSc1d1	okrouhlá
<g/>
,	,	kIx,	,
10	[number]	k4	10
až	až	k9	až
19	[number]	k4	19
mm	mm	kA	mm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
lehce	lehko	k6eAd1	lehko
vykrojená	vykrojený	k2eAgFnSc1d1	vykrojená
<g/>
,	,	kIx,	,
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
krátce	krátce	k6eAd1	krátce
nehetnatá	hetnatý	k2eNgFnSc1d1	hetnatý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
s	s	k7c7	s
bělavou	bělavý	k2eAgFnSc7d1	bělavá
nebo	nebo	k8xC	nebo
fialovou	fialový	k2eAgFnSc7d1	fialová
kresbou	kresba	k1gFnSc7	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
jsou	být	k5eAaImIp3nP	být
okrouhle	okrouhle	k6eAd1	okrouhle
vejčitá	vejčitý	k2eAgNnPc1d1	vejčité
až	až	k8xS	až
vejčitě	vejčitě	k6eAd1	vejčitě
oválná	oválný	k2eAgFnSc1d1	oválná
<g/>
,	,	kIx,	,
10	[number]	k4	10
až	až	k9	až
18	[number]	k4	18
mm	mm	kA	mm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
volná	volný	k2eAgFnSc1d1	volná
<g/>
.	.	kIx.	.
</s>
<s>
Člunek	člunek	k1gInSc1	člunek
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
mm	mm	kA	mm
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
<g/>
,	,	kIx,	,
ouškatý	ouškatý	k2eAgInSc1d1	ouškatý
<g/>
,	,	kIx,	,
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
až	až	k9	až
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
srostlý	srostlý	k2eAgInSc1d1	srostlý
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinek	tyčinka	k1gFnPc2	tyčinka
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
jednobratré	jednobratrý	k2eAgFnPc1d1	jednobratrý
nebo	nebo	k8xC	nebo
dvoubratré	dvoubratrý	k2eAgFnPc1d1	dvoubratrý
<g/>
.	.	kIx.	.
</s>
<s>
Semeník	semeník	k1gInSc1	semeník
je	být	k5eAaImIp3nS	být
stopkatý	stopkatý	k2eAgMnSc1d1	stopkatý
až	až	k8xS	až
přisedlý	přisedlý	k2eAgMnSc1d1	přisedlý
<g/>
,	,	kIx,	,
se	s	k7c7	s
2	[number]	k4	2
až	až	k9	až
7	[number]	k4	7
vajíčky	vajíčko	k1gNnPc7	vajíčko
a	a	k8xC	a
nitkovitou	nitkovitý	k2eAgFnSc7d1	nitkovitá
zahnutou	zahnutý	k2eAgFnSc7d1	zahnutá
čnělkou	čnělka	k1gFnSc7	čnělka
nesoucí	nesoucí	k2eAgFnSc4d1	nesoucí
nenápadnou	nápadný	k2eNgFnSc4d1	nenápadná
bliznu	blizna	k1gFnSc4	blizna
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
ploché	plochý	k2eAgFnPc1d1	plochá
<g/>
,	,	kIx,	,
nepukavé	pukavý	k2eNgFnPc1d1	nepukavá
<g/>
,	,	kIx,	,
okrouhlého	okrouhlý	k2eAgInSc2d1	okrouhlý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
tenké	tenký	k2eAgInPc1d1	tenký
nebo	nebo	k8xC	nebo
kožovité	kožovitý	k2eAgInPc1d1	kožovitý
<g/>
,	,	kIx,	,
kolem	kolem	k6eAd1	kolem
dokola	dokola	k6eAd1	dokola
se	s	k7c7	s
širokým	široký	k2eAgNnSc7d1	široké
pevným	pevný	k2eAgNnSc7d1	pevné
křídlem	křídlo	k1gNnSc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
nebo	nebo	k8xC	nebo
po	po	k7c6	po
straně	strana	k1gFnSc6	strana
plodu	plod	k1gInSc2	plod
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
až	až	k6eAd1	až
3	[number]	k4	3
podlouhlá	podlouhlý	k2eAgFnSc1d1	podlouhlá
či	či	k8xC	či
ledvinovitá	ledvinovitý	k2eAgNnPc1d1	ledvinovitý
semena	semeno	k1gNnPc1	semeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
křídlok	křídlok	k1gInSc1	křídlok
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
asi	asi	k9	asi
35	[number]	k4	35
až	až	k9	až
40	[number]	k4	40
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
v	v	k7c6	v
tropech	trop	k1gMnPc6	trop
téměř	téměř	k6eAd1	téměř
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
P.	P.	kA	P.
santalinoides	santalinoides	k1gInSc1	santalinoides
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
Africe	Afrika	k1gFnSc6	Afrika
i	i	k8xC	i
v	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Křídloky	Křídlok	k1gInPc1	Křídlok
nejčastěji	často	k6eAd3	často
rostou	růst	k5eAaImIp3nP	růst
ve	v	k7c6	v
vlhkých	vlhký	k2eAgInPc6d1	vlhký
tropických	tropický	k2eAgInPc6d1	tropický
deštných	deštný	k2eAgInPc6d1	deštný
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
na	na	k7c6	na
zaplavovaných	zaplavovaný	k2eAgFnPc6d1	zaplavovaná
půdách	půda	k1gFnPc6	půda
podél	podél	k7c2	podél
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
lagun	laguna	k1gFnPc2	laguna
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
rostou	růst	k5eAaImIp3nP	růst
na	na	k7c6	na
suchých	suchý	k2eAgInPc6d1	suchý
horských	horský	k2eAgInPc6d1	horský
svazích	svah	k1gInPc6	svah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsahové	obsahový	k2eAgFnPc1d1	obsahová
látky	látka	k1gFnPc1	látka
==	==	k?	==
</s>
</p>
<p>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
asijských	asijský	k2eAgInPc2d1	asijský
druhů	druh	k1gInPc2	druh
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
santalinus	santalinus	k1gMnSc1	santalinus
a	a	k8xC	a
P.	P.	kA	P.
dalbergioides	dalbergioides	k1gInSc1	dalbergioides
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
červené	červený	k2eAgNnSc4d1	červené
barvivo	barvivo	k1gNnSc4	barvivo
santalin	santalina	k1gFnPc2	santalina
a	a	k8xC	a
žlutý	žlutý	k2eAgInSc1d1	žlutý
flavonoid	flavonoid	k1gInSc1	flavonoid
santal	santat	k5eAaPmAgInS	santat
<g/>
.	.	kIx.	.
</s>
<s>
Jádrové	jádrový	k2eAgNnSc1d1	jádrové
dřevo	dřevo	k1gNnSc1	dřevo
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
santalinus	santalinus	k1gMnSc1	santalinus
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
terpenoidy	terpenoid	k1gInPc4	terpenoid
eudesmol	eudesmola	k1gFnPc2	eudesmola
<g/>
,	,	kIx,	,
pterokarpol	pterokarpola	k1gFnPc2	pterokarpola
<g/>
,	,	kIx,	,
iso-pterokarpolon	isoterokarpolon	k1gInSc1	iso-pterokarpolon
<g/>
,	,	kIx,	,
kryptomeridiol	kryptomeridiol	k1gInSc1	kryptomeridiol
<g/>
,	,	kIx,	,
pterokarptriol	pterokarptriol	k1gInSc1	pterokarptriol
a	a	k8xC	a
pterokarpdiolon	pterokarpdiolon	k1gInSc1	pterokarpdiolon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
jsou	být	k5eAaImIp3nP	být
obsaženy	obsažen	k2eAgInPc1d1	obsažen
triterpenoidy	triterpenoid	k1gInPc1	triterpenoid
-	-	kIx~	-
beta-ampyron	betampyron	k1gInSc1	beta-ampyron
<g/>
,	,	kIx,	,
lupenon	lupenon	k1gInSc1	lupenon
a	a	k8xC	a
deriváty	derivát	k1gInPc1	derivát
lupeolu	lupeol	k1gInSc2	lupeol
<g/>
,	,	kIx,	,
v	v	k7c6	v
běli	běl	k1gFnSc6	běl
kyselina	kyselina	k1gFnSc1	kyselina
acetyl-oleanolová	acetylleanolový	k2eAgFnSc1d1	acetyl-oleanolový
<g/>
,	,	kIx,	,
aldehyd	aldehyd	k1gInSc1	aldehyd
této	tento	k3xDgFnSc2	tento
kyseliny	kyselina	k1gFnSc2	kyselina
a	a	k8xC	a
erythrodiol	erythrodiola	k1gFnPc2	erythrodiola
<g/>
.	.	kIx.	.
</s>
<s>
Jádrové	jádrový	k2eAgNnSc1d1	jádrové
dřevo	dřevo	k1gNnSc1	dřevo
P.	P.	kA	P.
dalbergioides	dalbergioidesa	k1gFnPc2	dalbergioidesa
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pterostilben	pterostilbna	k1gFnPc2	pterostilbna
<g/>
,	,	kIx,	,
pterokarpin	pterokarpina	k1gFnPc2	pterokarpina
<g/>
,	,	kIx,	,
liquiritigenin	liquiritigenina	k1gFnPc2	liquiritigenina
a	a	k8xC	a
isoliquiritigenin	isoliquiritigenina	k1gFnPc2	isoliquiritigenina
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
a	a	k8xC	a
jádrové	jádrový	k2eAgNnSc1d1	jádrové
dřevo	dřevo	k1gNnSc1	dřevo
křídloku	křídlok	k1gInSc2	křídlok
vakovitého	vakovitý	k2eAgInSc2d1	vakovitý
(	(	kIx(	(
<g/>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
marsupium	marsupium	k1gNnSc1	marsupium
<g/>
)	)	kIx)	)
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
isoflavonoidy	isoflavonoida	k1gFnPc1	isoflavonoida
<g/>
,	,	kIx,	,
terpenoidy	terpenoida	k1gFnPc1	terpenoida
and	and	k?	and
tanniny	tannina	k1gFnSc2	tannina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
účinné	účinný	k2eAgFnPc4d1	účinná
látky	látka	k1gFnPc4	látka
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
náleží	náležet	k5eAaImIp3nS	náležet
epikatechin	epikatechin	k1gInSc1	epikatechin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zástupci	zástupce	k1gMnPc1	zástupce
==	==	k?	==
</s>
</p>
<p>
<s>
křídlok	křídlok	k1gInSc1	křídlok
lékařský	lékařský	k2eAgInSc1d1	lékařský
(	(	kIx(	(
<g/>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
officinalis	officinalis	k1gFnSc2	officinalis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
křídlok	křídlok	k1gInSc1	křídlok
vakovitý	vakovitý	k2eAgInSc1d1	vakovitý
(	(	kIx(	(
<g/>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
marsupium	marsupium	k1gNnSc1	marsupium
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
africké	africký	k2eAgInPc1d1	africký
a	a	k8xC	a
asijské	asijský	k2eAgInPc1d1	asijský
druhy	druh	k1gInPc1	druh
křídloků	křídlok	k1gInPc2	křídlok
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
vyhledávané	vyhledávaný	k2eAgNnSc4d1	vyhledávané
a	a	k8xC	a
ceněné	ceněný	k2eAgNnSc4d1	ceněné
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
Africe	Afrika	k1gFnSc6	Afrika
je	být	k5eAaImIp3nS	být
těžen	těžit	k5eAaImNgMnS	těžit
zejména	zejména	k9	zejména
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
soyauxii	soyauxie	k1gFnSc4	soyauxie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
africký	africký	k2eAgInSc1d1	africký
padouk	padouk	k1gInSc1	padouk
<g/>
.	.	kIx.	.
</s>
<s>
Mívá	mívat	k5eAaImIp3nS	mívat
korálově	korálově	k6eAd1	korálově
červenou	červený	k2eAgFnSc4d1	červená
až	až	k8xS	až
červenohnědou	červenohnědý	k2eAgFnSc4d1	červenohnědá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
<g/>
,	,	kIx,	,
těžké	těžký	k2eAgNnSc1d1	těžké
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
soustruží	soustružit	k5eAaImIp3nS	soustružit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ceněno	ceněn	k2eAgNnSc4d1	ceněno
např.	např.	kA	např.
na	na	k7c4	na
luxusní	luxusní	k2eAgFnPc4d1	luxusní
podlahy	podlaha	k1gFnPc4	podlaha
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
odolné	odolný	k2eAgNnSc1d1	odolné
proti	proti	k7c3	proti
otěru	otěr	k1gInSc3	otěr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
dřevo	dřevo	k1gNnSc1	dřevo
používáno	používat	k5eAaImNgNnS	používat
již	již	k6eAd1	již
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
afrických	africký	k2eAgInPc2d1	africký
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
těžen	těžit	k5eAaImNgInS	těžit
např.	např.	kA	např.
P.	P.	kA	P.
angolensis	angolensis	k1gFnPc2	angolensis
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
načervenalé	načervenalý	k2eAgNnSc1d1	načervenalé
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xC	jako
muninga	muninga	k1gFnSc1	muninga
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
teakového	teakový	k2eAgNnSc2d1	teakové
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
asijských	asijský	k2eAgInPc2d1	asijský
druhů	druh	k1gInPc2	druh
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
atraktivní	atraktivní	k2eAgNnSc1d1	atraktivní
dřevo	dřevo	k1gNnSc1	dřevo
P.	P.	kA	P.
dalbergioides	dalbergioides	k1gInSc1	dalbergioides
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgInSc1d1	pocházející
z	z	k7c2	z
Andamanských	Andamanský	k2eAgInPc2d1	Andamanský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
obchodováno	obchodován	k2eAgNnSc1d1	obchodováno
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
padouk	padouk	k1gMnSc1	padouk
andaman	andaman	k1gMnSc1	andaman
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
používáno	používán	k2eAgNnSc1d1	používáno
na	na	k7c4	na
luxusní	luxusní	k2eAgInSc4d1	luxusní
nábytek	nábytek	k1gInSc4	nábytek
<g/>
,	,	kIx,	,
kulečníkové	kulečníkový	k2eAgInPc1d1	kulečníkový
stoly	stol	k1gInPc1	stol
<g/>
,	,	kIx,	,
podlahy	podlaha	k1gFnPc1	podlaha
<g/>
,	,	kIx,	,
držadla	držadlo	k1gNnPc1	držadlo
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Obdobné	obdobný	k2eAgNnSc1d1	obdobné
využití	využití	k1gNnSc1	využití
má	mít	k5eAaImIp3nS	mít
i	i	k8xC	i
dřevo	dřevo	k1gNnSc1	dřevo
P.	P.	kA	P.
macrocarpus	macrocarpus	k1gInSc1	macrocarpus
z	z	k7c2	z
Barmy	Barma	k1gFnSc2	Barma
a	a	k8xC	a
P.	P.	kA	P.
indicus	indicus	k1gInSc1	indicus
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
P.	P.	kA	P.
indicus	indicus	k1gInSc4	indicus
voní	vonět	k5eAaImIp3nP	vonět
po	po	k7c6	po
růžích	růž	k1gFnPc6	růž
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odolné	odolný	k2eAgFnPc1d1	odolná
proti	proti	k7c3	proti
termitům	termit	k1gInPc3	termit
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc1d1	podobné
dřevo	dřevo	k1gNnSc1	dřevo
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
druhy	druh	k1gInPc1	druh
P.	P.	kA	P.
echinatus	echinatus	k1gInSc4	echinatus
a	a	k8xC	a
P.	P.	kA	P.
blancoi	blancoi	k6eAd1	blancoi
<g/>
.	.	kIx.	.
</s>
<s>
Křídlok	Křídlok	k1gInSc1	Křídlok
vakovitý	vakovitý	k2eAgInSc1d1	vakovitý
(	(	kIx(	(
<g/>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
marsupium	marsupium	k1gNnSc1	marsupium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
teakem	teak	k1gInSc7	teak
a	a	k8xC	a
dalbergií	dalbergie	k1gFnSc7	dalbergie
nejčastěji	často	k6eAd3	často
pěstovanou	pěstovaný	k2eAgFnSc7d1	pěstovaná
indickou	indický	k2eAgFnSc7d1	indická
dřevinou	dřevina	k1gFnSc7	dřevina
<g/>
.	.	kIx.	.
<g/>
Pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
draco	draco	k1gMnSc1	draco
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
sangre	sangr	k1gInSc5	sangr
de	de	k?	de
draco	draco	k1gNnSc1	draco
(	(	kIx(	(
<g/>
dračí	dračí	k2eAgFnSc1d1	dračí
krev	krev	k1gFnSc1	krev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
využívána	využívat	k5eAaPmNgFnS	využívat
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
medicíně	medicína	k1gFnSc6	medicína
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
politur	politura	k1gFnPc2	politura
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
pterokarpů	pterokarp	k1gInPc2	pterokarp
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgNnPc4d1	nazývané
kino	kino	k1gNnSc4	kino
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
malabarské	malabarský	k2eAgNnSc1d1	malabarský
kino	kino	k1gNnSc1	kino
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
asijský	asijský	k2eAgInSc1d1	asijský
křídlok	křídlok	k1gInSc1	křídlok
vakovitý	vakovitý	k2eAgInSc1d1	vakovitý
(	(	kIx(	(
<g/>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
marsupium	marsupium	k1gNnSc1	marsupium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
vydělávání	vydělávání	k1gNnSc3	vydělávání
jemných	jemný	k2eAgFnPc2d1	jemná
kůží	kůže	k1gFnPc2	kůže
<g/>
,	,	kIx,	,
přibarvování	přibarvování	k1gNnSc3	přibarvování
vín	víno	k1gNnPc2	víno
a	a	k8xC	a
jako	jako	k8xS	jako
léčivo	léčivo	k1gNnSc1	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
afrických	africký	k2eAgInPc2d1	africký
druhů	druh	k1gInPc2	druh
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
kino	kino	k1gNnSc1	kino
druh	druh	k1gInSc1	druh
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
erinaceus	erinaceus	k1gMnSc1	erinaceus
<g/>
.	.	kIx.	.
<g/>
Celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
druhů	druh	k1gInPc2	druh
pterokarpů	pterokarp	k1gInPc2	pterokarp
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
medicíně	medicína	k1gFnSc6	medicína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
léčivo	léčivo	k1gNnSc1	léčivo
zejména	zejména	k6eAd1	zejména
druhy	druh	k1gMnPc4	druh
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
dalbergioides	dalbergioides	k1gMnSc1	dalbergioides
<g/>
,	,	kIx,	,
P.	P.	kA	P.
indicus	indicus	k1gInSc1	indicus
<g/>
,	,	kIx,	,
P.	P.	kA	P.
marsupium	marsupium	k1gNnSc1	marsupium
a	a	k8xC	a
P.	P.	kA	P.
santalinus	santalinus	k1gInSc1	santalinus
<g/>
.	.	kIx.	.
</s>
<s>
Pasta	pasta	k1gFnSc1	pasta
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
P.	P.	kA	P.
santalinus	santalinus	k1gInSc1	santalinus
je	být	k5eAaImIp3nS	být
aplikována	aplikovat	k5eAaBmNgFnS	aplikovat
zevně	zevně	k6eAd1	zevně
na	na	k7c4	na
spáleniny	spálenina	k1gFnPc4	spálenina
a	a	k8xC	a
bolesti	bolest	k1gFnPc4	bolest
hlavy	hlava	k1gFnPc4	hlava
<g/>
,	,	kIx,	,
plody	plod	k1gInPc4	plod
slouží	sloužit	k5eAaImIp3nP	sloužit
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
úplavice	úplavice	k1gFnSc2	úplavice
<g/>
.	.	kIx.	.
</s>
<s>
Odvar	odvar	k1gInSc1	odvar
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
indicus	indicus	k1gMnSc1	indicus
je	být	k5eAaImIp3nS	být
podáván	podávat	k5eAaImNgMnS	podávat
při	při	k7c6	při
edémech	edém	k1gInPc6	edém
a	a	k8xC	a
na	na	k7c4	na
žlučníkové	žlučníkový	k2eAgInPc4d1	žlučníkový
kameny	kámen	k1gInPc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Kino	kino	k1gNnSc1	kino
z	z	k7c2	z
kůry	kůra	k1gFnSc2	kůra
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
používáno	používán	k2eAgNnSc1d1	používáno
na	na	k7c4	na
boláky	bolák	k1gInPc4	bolák
a	a	k8xC	a
při	při	k7c6	při
průjmu	průjem	k1gInSc6	průjem
<g/>
.	.	kIx.	.
</s>
<s>
Kino	kino	k1gNnSc1	kino
z	z	k7c2	z
druhu	druh	k1gInSc2	druh
P.	P.	kA	P.
marsupium	marsupium	k1gNnSc1	marsupium
má	mít	k5eAaImIp3nS	mít
silně	silně	k6eAd1	silně
stahující	stahující	k2eAgInPc4d1	stahující
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
kůra	kůra	k1gFnSc1	kůra
P.	P.	kA	P.
santalinus	santalinus	k1gInSc4	santalinus
jako	jako	k8xC	jako
astringens	astringens	k1gInSc4	astringens
při	při	k7c6	při
průjmech	průjem	k1gInPc6	průjem
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
a	a	k8xC	a
kůra	kůra	k1gFnSc1	kůra
afrického	africký	k2eAgInSc2d1	africký
P.	P.	kA	P.
angolensis	angolensis	k1gInSc4	angolensis
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgInPc1d1	používán
při	při	k7c6	při
ošetřování	ošetřování	k1gNnSc6	ošetřování
problémů	problém	k1gInPc2	problém
s	s	k7c7	s
menstruací	menstruace	k1gFnSc7	menstruace
<g/>
.	.	kIx.	.
</s>
<s>
Kůra	kůra	k1gFnSc1	kůra
P.	P.	kA	P.
rotundifolius	rotundifolius	k1gInSc1	rotundifolius
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
užívána	užívat	k5eAaImNgFnS	užívat
k	k	k7c3	k
inhalacích	inhalace	k1gFnPc6	inhalace
při	při	k7c6	při
potížích	potíž	k1gFnPc6	potíž
s	s	k7c7	s
horními	horní	k2eAgFnPc7d1	horní
cestami	cesta	k1gFnPc7	cesta
dýchacími	dýchací	k2eAgFnPc7d1	dýchací
a	a	k8xC	a
při	pře	k1gFnSc3	pře
zvracení	zvracení	k1gNnSc2	zvracení
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
pterokarpů	pterokarp	k1gMnPc2	pterokarp
jsou	být	k5eAaImIp3nP	být
pěstovány	pěstován	k2eAgFnPc1d1	pěstována
jako	jako	k8xS	jako
okrasné	okrasný	k2eAgFnPc1d1	okrasná
dřeviny	dřevina	k1gFnPc1	dřevina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
P.	P.	kA	P.
podocarpus	podocarpus	k1gMnSc1	podocarpus
ve	v	k7c6	v
Venezuele	Venezuela	k1gFnSc6	Venezuela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Zimbabwe	Zimbabwe	k1gFnSc6	Zimbabwe
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
druhy	druh	k1gInPc7	druh
P.	P.	kA	P.
echinatus	echinatus	k1gInSc1	echinatus
<g/>
,	,	kIx,	,
P.	P.	kA	P.
rotundifolius	rotundifolius	k1gInSc1	rotundifolius
a	a	k8xC	a
P.	P.	kA	P.
sericeus	sericeus	k1gInSc1	sericeus
<g/>
,	,	kIx,	,
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
P.	P.	kA	P.
stevensonii	stevensonie	k1gFnSc3	stevensonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
rozšíření	rozšíření	k1gNnSc4	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
acapulcensis	acapulcensis	k1gInSc1	acapulcensis
-	-	kIx~	-
Střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
albopubescens	albopubescens	k1gInSc1	albopubescens
-	-	kIx~	-
Zaire	Zair	k1gMnSc5	Zair
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
amazonum	amazonum	k1gInSc1	amazonum
-	-	kIx~	-
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
angolensis	angolensis	k1gInSc1	angolensis
-	-	kIx~	-
tropická	tropický	k2eAgFnSc1d1	tropická
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
antunesii	antunesie	k1gFnSc3	antunesie
-	-	kIx~	-
Angola	Angola	k1gFnSc1	Angola
<g/>
,	,	kIx,	,
Zambie	Zambie	k1gFnSc1	Zambie
a	a	k8xC	a
Namibie	Namibie	k1gFnSc1	Namibie
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
brenanii	brenanie	k1gFnSc3	brenanie
-	-	kIx~	-
Zambie	Zambie	k1gFnSc1	Zambie
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
claessensii	claessensie	k1gFnSc3	claessensie
-	-	kIx~	-
Zambie	Zambie	k1gFnSc1	Zambie
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
dalbergioides	dalbergioides	k1gMnSc1	dalbergioides
-	-	kIx~	-
Andamany	Andamany	k1gFnPc1	Andamany
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
echinatus	echinatus	k1gMnSc1	echinatus
-	-	kIx~	-
?	?	kIx.	?
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
erinaceus	erinaceus	k1gMnSc1	erinaceus
-	-	kIx~	-
tropická	tropický	k2eAgFnSc1d1	tropická
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
gilletii	gilletie	k1gFnSc4	gilletie
-	-	kIx~	-
Zaire	Zair	k1gMnSc5	Zair
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
hockii	hockie	k1gFnSc4	hockie
-	-	kIx~	-
Zaire	Zair	k1gMnSc5	Zair
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
homblei	homblei	k1gNnSc1	homblei
-	-	kIx~	-
Zaire	Zair	k1gMnSc5	Zair
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
indicus	indicus	k1gMnSc1	indicus
-	-	kIx~	-
Indie	Indie	k1gFnSc1	Indie
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
až	až	k6eAd1	až
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Tichomoří	Tichomoří	k1gNnSc2	Tichomoří
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
lucens	lucens	k1gInSc1	lucens
-	-	kIx~	-
tropická	tropický	k2eAgFnSc1d1	tropická
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
macrocarpus	macrocarpus	k1gMnSc1	macrocarpus
-	-	kIx~	-
Indie	Indie	k1gFnSc1	Indie
až	až	k8xS	až
Vietnam	Vietnam	k1gInSc1	Vietnam
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
marsupium	marsupium	k1gNnSc1	marsupium
-	-	kIx~	-
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Nepál	Nepál	k1gInSc1	Nepál
a	a	k8xC	a
Sri	Sri	k1gFnSc1	Sri
Lanka	lanko	k1gNnSc2	lanko
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
mildbraedii	mildbraedie	k1gFnSc3	mildbraedie
-	-	kIx~	-
tropická	tropický	k2eAgFnSc1d1	tropická
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
mutondo	mutondo	k1gNnSc1	mutondo
-	-	kIx~	-
Zaire	Zair	k1gMnSc5	Zair
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
officinalis	officinalis	k1gInSc1	officinalis
-	-	kIx~	-
Střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
orbiculatus	orbiculatus	k1gMnSc1	orbiculatus
-	-	kIx~	-
Mexiko	Mexiko	k1gNnSc1	Mexiko
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
osun	osun	k1gMnSc1	osun
-	-	kIx~	-
tropická	tropický	k2eAgFnSc1d1	tropická
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
rohrii	rohrie	k1gFnSc3	rohrie
-	-	kIx~	-
Střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
rotundifolius	rotundifolius	k1gMnSc1	rotundifolius
-	-	kIx~	-
tropická	tropický	k2eAgFnSc1d1	tropická
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
santalinoides	santalinoides	k1gMnSc1	santalinoides
-	-	kIx~	-
tropická	tropický	k2eAgFnSc1d1	tropická
Afrika	Afrika	k1gFnSc1	Afrika
a	a	k8xC	a
tropická	tropický	k2eAgFnSc1d1	tropická
Amerika	Amerika	k1gFnSc1	Amerika
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
santalinus	santalinus	k1gMnSc1	santalinus
-	-	kIx~	-
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Sri	Sri	k1gFnSc1	Sri
Lanka	lanko	k1gNnSc2	lanko
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
soyauxii	soyauxie	k1gFnSc3	soyauxie
-	-	kIx~	-
tropická	tropický	k2eAgFnSc1d1	tropická
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
ternatus	ternatus	k1gMnSc1	ternatus
-	-	kIx~	-
Brazílie	Brazílie	k1gFnSc1	Brazílie
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
tessmannii	tessmannie	k1gFnSc3	tessmannie
-	-	kIx~	-
Gabon	Gabon	k1gInSc1	Gabon
<g/>
,	,	kIx,	,
Zaire	Zair	k1gInSc5	Zair
a	a	k8xC	a
Rovníková	rovníkový	k2eAgFnSc1d1	Rovníková
Guinea	Guinea	k1gFnSc1	Guinea
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
tinctorius	tinctorius	k1gMnSc1	tinctorius
-	-	kIx~	-
tropická	tropický	k2eAgFnSc1d1	tropická
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
velutinus	velutinus	k1gMnSc1	velutinus
-	-	kIx~	-
Zaire	Zair	k1gInSc5	Zair
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
villosus	villosus	k1gMnSc1	villosus
-	-	kIx~	-
Brazílie	Brazílie	k1gFnSc1	Brazílie
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gMnSc1	Pterocarpus
violaceus	violaceus	k1gMnSc1	violaceus
-	-	kIx~	-
Brazílie	Brazílie	k1gFnSc1	Brazílie
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
zehntneri	zehntner	k1gFnSc2	zehntner
-	-	kIx~	-
Brazílie	Brazílie	k1gFnSc2	Brazílie
</s>
</p>
<p>
<s>
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
zenkeri	zenker	k1gFnSc2	zenker
-	-	kIx~	-
Kamerun	Kamerun	k1gInSc1	Kamerun
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
křídlok	křídlok	k1gInSc1	křídlok
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Pterocarpus	Pterocarpus	k1gInSc1	Pterocarpus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
