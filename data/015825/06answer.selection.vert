<s>
73	#num#	k4
<g/>
.	.	kIx.
tankový	tankový	k2eAgInSc1d1
prapor	prapor	k1gInSc1
„	„	k?
<g/>
Hanácký	hanácký	k2eAgInSc1d1
<g/>
“	“	k?
s	s	k7c7
posádkou	posádka	k1gFnSc7
v	v	k7c6
Přáslavicích	Přáslavice	k1gFnPc6
je	být	k5eAaImIp3nS
vojenský	vojenský	k2eAgInSc1d1
útvar	útvar	k1gInSc1
Pozemních	pozemní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
sestavě	sestava	k1gFnSc6
7	#num#	k4
<g/>
.	.	kIx.
mechanizované	mechanizovaný	k2eAgFnSc2d1
brigády	brigáda	k1gFnSc2
a	a	k8xC
jediná	jediný	k2eAgFnSc1d1
tanková	tankový	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
české	český	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
začátku	začátek	k1gInSc2
roku	rok	k1gInSc2
2018	#num#	k4
provozuje	provozovat	k5eAaImIp3nS
celkem	celkem	k6eAd1
50	#num#	k4
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
30	#num#	k4
modernizovaných	modernizovaný	k2eAgFnPc2d1
T-72M4	T-72M4	k1gFnPc2
CZ	CZ	kA
a	a	k8xC
20	#num#	k4
původních	původní	k2eAgFnPc2d1
T-	T-	k1gFnPc2
<g/>
72	#num#	k4
<g/>
M	M	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>