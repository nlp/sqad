<s>
73	#num#	k4
<g/>
.	.	kIx.
tankový	tankový	k2eAgInSc1d1
prapor	prapor	k1gInSc1
</s>
<s>
73	#num#	k4
<g/>
.	.	kIx.
tankový	tankový	k2eAgInSc1d1
prapor	prapor	k1gInSc1
Znak	znak	k1gInSc1
73	#num#	k4
<g/>
.	.	kIx.
tankového	tankový	k2eAgInSc2d1
praporu	prapor	k1gInSc2
Znak	znak	k1gInSc1
73	#num#	k4
<g/>
.	.	kIx.
tankového	tankový	k2eAgInSc2d1
praporu	prapor	k1gInSc2
Země	zem	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
2005	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
prapor	prapor	k1gInSc1
Posádka	posádka	k1gFnSc1
</s>
<s>
Přáslavice	Přáslavice	k1gFnSc1
Velitelé	velitel	k1gMnPc5
</s>
<s>
podplukovník	podplukovník	k1gMnSc1
Vít	Vít	k1gMnSc1
Ducháček	Ducháček	k1gMnSc1
Nadřazené	nadřazený	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
mechanizovaná	mechanizovaný	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
</s>
<s>
73	#num#	k4
<g/>
.	.	kIx.
tankový	tankový	k2eAgInSc1d1
prapor	prapor	k1gInSc1
„	„	k?
<g/>
Hanácký	hanácký	k2eAgInSc1d1
<g/>
“	“	k?
s	s	k7c7
posádkou	posádka	k1gFnSc7
v	v	k7c6
Přáslavicích	Přáslavice	k1gFnPc6
je	být	k5eAaImIp3nS
vojenský	vojenský	k2eAgInSc1d1
útvar	útvar	k1gInSc1
Pozemních	pozemní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
sestavě	sestava	k1gFnSc6
7	#num#	k4
<g/>
.	.	kIx.
mechanizované	mechanizovaný	k2eAgFnSc2d1
brigády	brigáda	k1gFnSc2
a	a	k8xC
jediná	jediný	k2eAgFnSc1d1
tanková	tankový	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
české	český	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
začátku	začátek	k1gInSc2
roku	rok	k1gInSc2
2018	#num#	k4
provozuje	provozovat	k5eAaImIp3nS
celkem	celkem	k6eAd1
50	#num#	k4
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
30	#num#	k4
modernizovaných	modernizovaný	k2eAgFnPc2d1
T-72M4	T-72M4	k1gFnPc2
CZ	CZ	kA
a	a	k8xC
20	#num#	k4
původních	původní	k2eAgFnPc2d1
T-	T-	k1gFnPc2
<g/>
72	#num#	k4
<g/>
M	M	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
73	#num#	k4
<g/>
.	.	kIx.
tankový	tankový	k2eAgInSc1d1
prapor	prapor	k1gInSc1
je	být	k5eAaImIp3nS
historickým	historický	k2eAgMnSc7d1
nástupcem	nástupce	k1gMnSc7
7	#num#	k4
<g/>
.	.	kIx.
protitankového	protitankový	k2eAgInSc2d1
dělostřeleckého	dělostřelecký	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
únoru	únor	k1gInSc6
1945	#num#	k4
rámci	rámec	k1gInSc6
4	#num#	k4
<g/>
.	.	kIx.
brigády	brigáda	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
československého	československý	k2eAgInSc2d1
armádního	armádní	k2eAgInSc2d1
sboru	sbor	k1gInSc2
v	v	k7c6
SSSR	SSSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
řadě	řada	k1gFnSc6
reorganizací	reorganizace	k1gFnPc2
byl	být	k5eAaImAgInS
útvar	útvar	k1gInSc1
pod	pod	k7c7
názvem	název	k1gInSc7
33	#num#	k4
<g/>
.	.	kIx.
tankový	tankový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
v	v	k7c6
květnu	květen	k1gInSc6
1966	#num#	k4
přemístěn	přemístit	k5eAaPmNgInS
do	do	k7c2
posádky	posádka	k1gFnSc2
Přáslavice	Přáslavice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
října	říjen	k1gInSc2
1994	#num#	k4
byl	být	k5eAaImAgInS
pluk	pluk	k1gInSc1
transformován	transformovat	k5eAaBmNgInS
na	na	k7c4
73	#num#	k4
<g/>
.	.	kIx.
mechanizovaný	mechanizovaný	k2eAgInSc1d1
prapor	prapor	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
prosinci	prosinec	k1gInSc6
2003	#num#	k4
na	na	k7c4
73	#num#	k4
<g/>
.	.	kIx.
smíšený	smíšený	k2eAgInSc1d1
mechanizovaný	mechanizovaný	k2eAgInSc1d1
prapor	prapor	k1gInSc1
a	a	k8xC
v	v	k7c6
lednu	leden	k1gInSc6
2005	#num#	k4
na	na	k7c4
73	#num#	k4
<g/>
.	.	kIx.
tankový	tankový	k2eAgInSc1d1
prapor	prapor	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
</s>
<s>
Tanky	tank	k1gInPc1
a	a	k8xC
další	další	k2eAgFnSc1d1
technika	technika	k1gFnSc1
praporu	prapor	k1gInSc2
</s>
<s>
73	#num#	k4
<g/>
.	.	kIx.
tankový	tankový	k2eAgInSc1d1
prapor	prapor	k1gInSc1
je	být	k5eAaImIp3nS
vyzbrojen	vyzbrojit	k5eAaPmNgInS
především	především	k9
dvěma	dva	k4xCgInPc7
verzemi	verze	k1gFnPc7
sovětského	sovětský	k2eAgInSc2d1
tanku	tank	k1gInSc2
T-	T-	k1gFnSc2
<g/>
72	#num#	k4
<g/>
,	,	kIx,
licenčně	licenčně	k6eAd1
vyráběného	vyráběný	k2eAgInSc2d1
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prapor	prapor	k1gInSc1
tabulkově	tabulkově	k6eAd1
disponuje	disponovat	k5eAaBmIp3nS
30	#num#	k4
modernizovanými	modernizovaný	k2eAgInPc7d1
tanky	tank	k1gInPc7
T-72M4	T-72M4	k1gFnSc2
CZ	CZ	kA
<g/>
,	,	kIx,
27	#num#	k4
v	v	k7c4
základní	základní	k2eAgInPc4d1
a	a	k8xC
3	#num#	k4
ve	v	k7c6
velitelské	velitelský	k2eAgFnSc6d1
úpravě	úprava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
omezené	omezený	k2eAgFnSc3d1
provozuschopnosti	provozuschopnost	k1gFnSc3
těchto	tento	k3xDgInPc2
tanků	tank	k1gInPc2
jimi	on	k3xPp3gMnPc7
byla	být	k5eAaImAgNnP
na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
2018	#num#	k4
plně	plně	k6eAd1
vyzbrojena	vyzbrojit	k5eAaPmNgFnS
pouze	pouze	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
tanková	tankový	k2eAgFnSc1d1
rota	rota	k1gFnSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
2	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
rota	rota	k1gFnSc1
obdržely	obdržet	k5eAaPmAgInP
reaktivované	reaktivovaný	k2eAgInPc1d1
(	(	kIx(
<g/>
nemodernizované	modernizovaný	k2eNgInPc1d1
<g/>
)	)	kIx)
tanky	tank	k1gInPc1
T-	T-	k1gFnSc1
<g/>
72	#num#	k4
<g/>
M	M	kA
<g/>
1	#num#	k4
<g/>
,	,	kIx,
kterými	který	k3yIgInPc7,k3yQgInPc7,k3yRgInPc7
je	být	k5eAaImIp3nS
v	v	k7c6
počtu	počet	k1gInSc6
10	#num#	k4
kusů	kus	k1gInPc2
vybavena	vybavit	k5eAaPmNgFnS
také	také	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
tanková	tankový	k2eAgFnSc1d1
rota	rota	k1gFnSc1
Aktivních	aktivní	k2eAgFnPc2d1
záloh	záloha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
další	další	k2eAgFnSc3d1
technice	technika	k1gFnSc3
praporu	prapor	k1gInSc2
patří	patřit	k5eAaImIp3nP
vyprošťovací	vyprošťovací	k2eAgInPc1d1
tanky	tank	k1gInPc1
VT-72M4	VT-72M4	k1gFnSc2
CZ	CZ	kA
<g/>
,	,	kIx,
bojová	bojový	k2eAgNnPc1d1
vozidla	vozidlo	k1gNnPc1
pěchoty	pěchota	k1gFnSc2
BVP-	BVP-	k1gFnSc2
<g/>
2	#num#	k4
<g/>
,	,	kIx,
osobní	osobní	k2eAgInPc1d1
terénní	terénní	k2eAgInPc1d1
automobily	automobil	k1gInPc1
UAZ-469	UAZ-469	k1gFnSc2
a	a	k8xC
nákladní	nákladní	k2eAgInPc1d1
automobily	automobil	k1gInPc1
Tatra	Tatra	k1gFnSc1
810	#num#	k4
a	a	k8xC
Tatra	Tatra	k1gFnSc1
815	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojáci	voják	k1gMnPc1
jsou	být	k5eAaImIp3nP
vyzbrojeni	vyzbrojit	k5eAaPmNgMnP
pistolemi	pistol	k1gFnPc7
CZ	CZ	kA
75	#num#	k4
SP-01	SP-01	k1gMnPc2
Phantom	Phantom	k1gInSc4
<g/>
,	,	kIx,
útočnými	útočný	k2eAgFnPc7d1
puškami	puška	k1gFnPc7
CZ	CZ	kA
805	#num#	k4
BREN	BREN	kA
<g/>
,	,	kIx,
samopaly	samopal	k1gInPc7
CZ	CZ	kA
Scorpion	Scorpion	k1gInSc1
EVO	Eva	k1gFnSc5
3	#num#	k4
<g/>
,	,	kIx,
odstřelovacími	odstřelovací	k2eAgFnPc7d1
puškami	puška	k1gFnPc7
SVD	SVD	kA
a	a	k8xC
ručními	ruční	k2eAgFnPc7d1
protitankovými	protitankový	k2eAgFnPc7d1
zbraněmi	zbraň	k1gFnPc7
RPG-7	RPG-7	k1gFnSc7
a	a	k8xC
RPG-	RPG-	k1gFnSc7
<g/>
75	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
Modernizovaný	modernizovaný	k2eAgInSc4d1
tank	tank	k1gInSc4
T-72M4	T-72M4	k1gFnPc2
CZ	CZ	kA
</s>
<s>
Nemodernizovaný	modernizovaný	k2eNgInSc4d1
tank	tank	k1gInSc4
T-72	T-72	k1gFnSc2
</s>
<s>
Bojové	bojový	k2eAgNnSc1d1
vozidlo	vozidlo	k1gNnSc1
pěchoty	pěchota	k1gFnSc2
BVP-2	BVP-2	k1gFnSc2
</s>
<s>
Organizační	organizační	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
73	#num#	k4
<g/>
.	.	kIx.
tankového	tankový	k2eAgInSc2d1
praporu	prapor	k1gInSc2
na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
2018	#num#	k4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Velení	velení	k1gNnSc1
</s>
<s>
Štáb	štáb	k1gInSc1
</s>
<s>
Velitelská	velitelský	k2eAgFnSc1d1
rota	rota	k1gFnSc1
</s>
<s>
Velitelská	velitelský	k2eAgFnSc1d1
četa	četa	k1gFnSc1
</s>
<s>
Spojovací	spojovací	k2eAgFnSc1d1
četa	četa	k1gFnSc1
</s>
<s>
Ženijní	ženijní	k2eAgFnSc1d1
četa	četa	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
tanková	tankový	k2eAgFnSc1d1
rota	rota	k1gFnSc1
</s>
<s>
1	#num#	k4
velitelský	velitelský	k2eAgInSc4d1
tank	tank	k1gInSc4
</s>
<s>
3	#num#	k4
tankové	tankový	k2eAgFnPc4d1
čety	četa	k1gFnPc4
po	po	k7c6
3	#num#	k4
tancích	tanec	k1gInPc6
T-72M4	T-72M4	k1gFnSc2
CZ	CZ	kA
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
tanková	tankový	k2eAgFnSc1d1
rota	rota	k1gFnSc1
</s>
<s>
1	#num#	k4
velitelský	velitelský	k2eAgInSc4d1
tank	tank	k1gInSc4
</s>
<s>
3	#num#	k4
tankové	tankový	k2eAgFnPc4d1
čety	četa	k1gFnPc4
po	po	k7c6
3	#num#	k4
tancích	tanec	k1gInPc6
T-72M1	T-72M1	k1gFnSc2
(	(	kIx(
<g/>
tabulkově	tabulkově	k6eAd1
T-72M4	T-72M4	k1gMnSc1
CZ	CZ	kA
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
tanková	tankový	k2eAgFnSc1d1
rota	rota	k1gFnSc1
</s>
<s>
1	#num#	k4
velitelský	velitelský	k2eAgInSc4d1
tank	tank	k1gInSc4
</s>
<s>
3	#num#	k4
tankové	tankový	k2eAgFnPc4d1
čety	četa	k1gFnPc4
po	po	k7c6
3	#num#	k4
tancích	tanec	k1gInPc6
T-72M1	T-72M1	k1gFnSc2
(	(	kIx(
<g/>
tabulkově	tabulkově	k6eAd1
T-72M4	T-72M4	k1gMnSc1
CZ	CZ	kA
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
tanková	tankový	k2eAgFnSc1d1
rota	rota	k1gFnSc1
Aktivních	aktivní	k2eAgFnPc2d1
záloh	záloha	k1gFnPc2
</s>
<s>
1	#num#	k4
velitelský	velitelský	k2eAgInSc4d1
tank	tank	k1gInSc4
</s>
<s>
3	#num#	k4
tankové	tankový	k2eAgFnPc4d1
čety	četa	k1gFnPc4
po	po	k7c6
3	#num#	k4
tancích	tanec	k1gInPc6
T-72M1	T-72M1	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
plánováno	plánovat	k5eAaImNgNnS
rozšíření	rozšíření	k1gNnSc1
na	na	k7c4
2	#num#	k4
velitelské	velitelský	k2eAgInPc4d1
tanky	tank	k1gInPc4
a	a	k8xC
6	#num#	k4
tankových	tankový	k2eAgFnPc2d1
čet	četa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Mechanizovaná	mechanizovaný	k2eAgFnSc1d1
četa	četa	k1gFnSc1
se	s	k7c7
3	#num#	k4
vozidly	vozidlo	k1gNnPc7
BVP-2	BVP-2	k1gFnSc7
</s>
<s>
Četa	četa	k1gFnSc1
zabezpečení	zabezpečení	k1gNnSc2
</s>
<s>
Mechanizovaná	mechanizovaný	k2eAgFnSc1d1
rota	rota	k1gFnSc1
</s>
<s>
1	#num#	k4
velitelské	velitelský	k2eAgFnPc1d1
bojové	bojový	k2eAgFnPc1d1
vozidlo	vozidlo	k1gNnSc4
pěchoty	pěchota	k1gFnSc2
</s>
<s>
3	#num#	k4
mechanizované	mechanizovaný	k2eAgFnPc4d1
čety	četa	k1gFnPc4
po	po	k7c4
3	#num#	k4
vozidlech	vozidlo	k1gNnPc6
BVP-2	BVP-2	k1gFnSc2
</s>
<s>
Rota	rota	k1gFnSc1
logistiky	logistika	k1gFnSc2
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
četa	četa	k1gFnSc1
</s>
<s>
Zásobovací	zásobovací	k2eAgFnSc1d1
četa	četa	k1gFnSc1
</s>
<s>
Četa	četa	k1gFnSc1
oprav	oprava	k1gFnPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Historie	historie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
73	#num#	k4
<g/>
tankovyprapor	tankovyprapor	k1gInSc1
<g/>
.	.	kIx.
<g/>
army	arma	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
STRATILÍK	STRATILÍK	kA
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armáda	armáda	k1gFnSc1
zvětšila	zvětšit	k5eAaPmAgFnS
své	svůj	k3xOyFgNnSc4
tankové	tankový	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
dvojnásobně	dvojnásobně	k6eAd1
<g/>
.	.	kIx.
euro	euro	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2018-02-28	2018-02-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
SOUŠEK	souška	k1gFnPc2
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tankisté	tankista	k1gMnPc1
musí	muset	k5eAaImIp3nP
improvizovat	improvizovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATM	ATM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
50	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
4823	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Technika	technika	k1gFnSc1
a	a	k8xC
zbraně	zbraň	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
73	#num#	k4
<g/>
tankovyprapor	tankovyprapor	k1gInSc1
<g/>
.	.	kIx.
<g/>
army	arma	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Struktura	struktura	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
73	#num#	k4
<g/>
tankovyprapor	tankovyprapor	k1gInSc1
<g/>
.	.	kIx.
<g/>
army	arma	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
72	#num#	k4
<g/>
.	.	kIx.
mechanizovaný	mechanizovaný	k2eAgInSc1d1
prapor	prapor	k1gInSc1
Přáslavice	Přáslavice	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
