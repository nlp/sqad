<s>
Řád	řád	k1gInSc1
britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
</s>
<s>
Řád	řád	k1gInSc1
britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
</s>
<s>
The	The	k?
Most	most	k1gInSc1
Excellent	Excellent	k1gMnSc1
Order	Order	k1gMnSc1
of	of	k?
the	the	k?
British	British	k1gInSc1
Empire	empir	k1gInSc5
</s>
<s>
Řádový	řádový	k2eAgInSc1d1
odznak	odznak	k1gInSc1
5	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
</s>
<s>
Uděluje	udělovat	k5eAaImIp3nS
Panovník	panovník	k1gMnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
záslužný	záslužný	k2eAgInSc1d1
řád	řád	k1gInSc1
</s>
<s>
Založeno	založen	k2eAgNnSc1d1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1917	#num#	k4
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Heslo	heslo	k1gNnSc1
</s>
<s>
For	forum	k1gNnPc2
God	God	k1gMnSc7
and	and	k?
the	the	k?
empire	empir	k1gInSc5
(	(	kIx(
<g/>
Pro	pro	k7c4
Boha	bůh	k1gMnSc4
a	a	k8xC
říši	říše	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Způsobilost	způsobilost	k1gFnSc1
</s>
<s>
občané	občan	k1gMnPc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
občané	občan	k1gMnPc1
Commonwealth	Commonwealtha	k1gFnPc2
Realms	Realms	k1gInSc4
nebo	nebo	k8xC
kdokoliv	kdokoliv	k3yInSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
vykonal	vykonat	k5eAaPmAgMnS
významný	významný	k2eAgInSc4d1
čin	čin	k1gInSc4
pro	pro	k7c4
Spojené	spojený	k2eAgNnSc4d1
království	království	k1gNnSc4
</s>
<s>
Status	status	k1gInSc1
</s>
<s>
nadále	nadále	k6eAd1
udílen	udílen	k2eAgInSc1d1
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
</s>
<s>
Hlava	hlava	k1gFnSc1
řádu	řád	k1gInSc2
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Třídy	třída	k1gFnPc1
</s>
<s>
velkokřížrytíř-komandérkomandérdůstojníkčlenstříbrná	velkokřížrytíř-komandérkomandérdůstojníkčlenstříbrný	k2eAgFnSc1d1
záslužná	záslužný	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
odznakem	odznak	k1gInSc7
je	být	k5eAaImIp3nS
pozlacený	pozlacený	k2eAgInSc1d1
šedě	šedě	k6eAd1
smaltovaný	smaltovaný	k2eAgInSc1d1
pisánský	pisánský	k2eAgInSc1d1
kříž	kříž	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
kulatém	kulatý	k2eAgInSc6d1
středu	střed	k1gInSc6
vyobrazeny	vyobrazen	k2eAgFnPc1d1
hlavy	hlava	k1gFnPc1
Jiřího	Jiří	k1gMnSc2
V.	V.	kA
a	a	k8xC
královny	královna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Ostatní	ostatní	k2eAgNnSc1d1
vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2
</s>
<s>
Královský	královský	k2eAgInSc1d1
řád	řád	k1gInSc1
Viktoriin	Viktoriin	k2eAgInSc1d1
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2
</s>
<s>
různé	různý	k2eAgFnPc1d1
<g/>
,	,	kIx,
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
udělené	udělený	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
Řádu	řád	k1gInSc2
britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
</s>
<s>
Řádová	řádový	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
velkokříže	velkokříž	k1gInSc2
</s>
<s>
Stuha	stuha	k1gFnSc1
civilní	civilní	k2eAgFnSc2d1
divize	divize	k1gFnSc2
</s>
<s>
Stuha	stuha	k1gFnSc1
vojenské	vojenský	k2eAgFnSc2d1
divize	divize	k1gFnSc2
</s>
<s>
Řád	řád	k1gInSc1
britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Most	most	k1gInSc1
Excellent	Excellent	k1gMnSc1
Order	Order	k1gMnSc1
of	of	k?
the	the	k?
British	British	k1gInSc1
Empire	empir	k1gInSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
britské	britský	k2eAgNnSc4d1
vyznamenání	vyznamenání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založil	založit	k5eAaPmAgMnS
ho	on	k3xPp3gNnSc4
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1917	#num#	k4
král	král	k1gMnSc1
Jiří	Jiří	k1gMnSc1
V.	V.	kA
jako	jako	k8xC,k8xS
všeobecný	všeobecný	k2eAgInSc4d1
záslužný	záslužný	k2eAgInSc4d1
řád	řád	k1gInSc4
pro	pro	k7c4
civilní	civilní	k2eAgFnPc4d1
osoby	osoba	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
přidána	přidat	k5eAaPmNgFnS
vojenská	vojenský	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
určená	určený	k2eAgFnSc1d1
důstojníkům	důstojník	k1gMnPc3
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
v	v	k7c6
míru	mír	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Vzhled	vzhled	k1gInSc1
řádu	řád	k1gInSc2
</s>
<s>
Odznakem	odznak	k1gInSc7
je	být	k5eAaImIp3nS
pozlacený	pozlacený	k2eAgInSc1d1
šedě	šedě	k6eAd1
smaltovaný	smaltovaný	k2eAgInSc1d1
pisánský	pisánský	k2eAgInSc1d1
kříž	kříž	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kulatém	kulatý	k2eAgInSc6d1
středu	střed	k1gInSc6
jsou	být	k5eAaImIp3nP
vyobrazeny	vyobrazen	k2eAgFnPc1d1
hlavy	hlava	k1gFnPc1
Jiřího	Jiří	k1gMnSc2
V.	V.	kA
a	a	k8xC
královny	královna	k1gFnPc1
Marie	Maria	k1gFnSc2
hledící	hledící	k2eAgFnPc1d1
doleva	doleva	k6eAd1
<g/>
,	,	kIx,
obklopené	obklopený	k2eAgMnPc4d1
červenofialovým	červenofialový	k2eAgInSc7d1
prstencem	prstenec	k1gInSc7
s	s	k7c7
heslem	heslo	k1gNnSc7
For	forum	k1gNnPc2
God	God	k1gFnSc2
and	and	k?
the	the	k?
empire	empir	k1gInSc5
–	–	k?
Pro	pro	k7c4
Boha	bůh	k1gMnSc4
a	a	k8xC
říši	říše	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kříž	kříž	k1gInSc1
je	být	k5eAaImIp3nS
zavěšen	zavěsit	k5eAaPmNgInS
na	na	k7c6
koruně	koruna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Hvězda	hvězda	k1gFnSc1
velkokříže	velkokříž	k1gInSc2
je	být	k5eAaImIp3nS
osmicípá	osmicípý	k2eAgFnSc1d1
<g/>
,	,	kIx,
stříbrná	stříbrnat	k5eAaImIp3nS
brilantující	brilantující	k2eAgNnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
odlesky	odlesk	k1gInPc7
lehce	lehko	k6eAd1
ionizujícího	ionizující	k2eAgNnSc2d1
skla	sklo	k1gNnSc2
a	a	k8xC
se	s	k7c7
středovým	středový	k2eAgInSc7d1
medailonem	medailon	k1gInSc7
řádu	řád	k1gInSc2
uprostřed	uprostřed	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hvězda	hvězda	k1gFnSc1
rytíře-komandéra	rytíře-komandéra	k1gFnSc1
je	být	k5eAaImIp3nS
stříbrná	stříbrná	k1gFnSc1
brilantující	brilantující	k2eAgFnSc1d1
ve	v	k7c6
tvaru	tvar	k1gInSc6
kosočtverce	kosočtverec	k1gInSc2
se	s	k7c7
stejným	stejný	k2eAgInSc7d1
středem	střed	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Stuha	stuha	k1gFnSc1
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
fialová	fialový	k2eAgFnSc1d1
<g/>
,	,	kIx,
u	u	k7c2
vojenské	vojenský	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
navíc	navíc	k6eAd1
s	s	k7c7
červeným	červený	k2eAgInSc7d1
středním	střední	k2eAgInSc7d1
proužkem	proužek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1936	#num#	k4
je	být	k5eAaImIp3nS
však	však	k9
sytě	sytě	k6eAd1
růžová	růžový	k2eAgFnSc1d1
s	s	k7c7
šedými	šedý	k2eAgInPc7d1
okraji	okraj	k1gInPc7
a	a	k8xC
u	u	k7c2
vojenské	vojenský	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
s	s	k7c7
šedým	šedý	k2eAgInSc7d1
proužkem	proužek	k1gInSc7
uprostřed	uprostřed	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Řetěz	řetěz	k1gInSc1
je	být	k5eAaImIp3nS
nošen	nosit	k5eAaImNgInS
při	při	k7c6
slavnostních	slavnostní	k2eAgFnPc6d1
příležitostech	příležitost	k1gFnPc6
k	k	k7c3
tmavorůžovému	tmavorůžový	k2eAgInSc3d1
plášti	plášť	k1gInSc3
s	s	k7c7
vyšitou	vyšitý	k2eAgFnSc7d1
hvězdou	hvězda	k1gFnSc7
na	na	k7c6
levé	levý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Dělení	dělení	k1gNnSc1
</s>
<s>
velkokříž	velkokříž	k1gInSc1
(	(	kIx(
<g/>
Knight	Knight	k2eAgMnSc1d1
Grand	grand	k1gMnSc1
Cross	Crossa	k1gFnPc2
GBE	GBE	kA
/	/	kIx~
Dame	Dame	k1gInSc1
Grand	grand	k1gMnSc1
Cross	Crossa	k1gFnPc2
GBE	GBE	kA
<g/>
)	)	kIx)
–	–	k?
velkostuha	velkostuha	k1gFnSc1
<g/>
,	,	kIx,
hvězda	hvězda	k1gFnSc1
<g/>
,	,	kIx,
řetěz	řetěz	k1gInSc1
</s>
<s>
rytíř-komandér	rytíř-komandér	k1gInSc1
(	(	kIx(
<g/>
Knight	Knight	k2eAgInSc1d1
Commander	Commander	k1gInSc1
KBE	KBE	kA
/	/	kIx~
Dame	Dame	k1gFnSc1
Commander	Commander	k1gMnSc1
DBE	DBE	kA
<g/>
)	)	kIx)
–	–	k?
u	u	k7c2
krku	krk	k1gInSc2
<g/>
,	,	kIx,
hvězda	hvězda	k1gFnSc1
</s>
<s>
komandér	komandér	k1gMnSc1
(	(	kIx(
<g/>
Commander	Commander	k1gMnSc1
CBE	CBE	kA
<g/>
)	)	kIx)
–	–	k?
u	u	k7c2
krku	krk	k1gInSc2
</s>
<s>
důstojník	důstojník	k1gMnSc1
(	(	kIx(
<g/>
Officer	Officer	k1gMnSc1
OBE	Ob	k1gInSc5
<g/>
)	)	kIx)
–	–	k?
na	na	k7c6
prsou	prsa	k1gNnPc6
<g/>
,	,	kIx,
nesmaltovaný	smaltovaný	k2eNgInSc4d1
zlacený	zlacený	k2eAgInSc4d1
kříž	kříž	k1gInSc4
</s>
<s>
člen	člen	k1gMnSc1
(	(	kIx(
<g/>
Member	Member	k1gMnSc1
MBE	MBE	kA
<g/>
)	)	kIx)
–	–	k?
na	na	k7c6
prsou	prsa	k1gNnPc6
<g/>
,	,	kIx,
nesmaltovaný	smaltovaný	k2eNgInSc4d1
stříbrný	stříbrný	k2eAgInSc4d1
kříž	kříž	k1gInSc4
</s>
<s>
K	k	k7c3
řádu	řád	k1gInSc3
patří	patřit	k5eAaImIp3nS
také	také	k9
stříbrná	stříbrný	k2eAgFnSc1d1
záslužná	záslužný	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Titulatura	titulatura	k1gFnSc1
</s>
<s>
Pouze	pouze	k6eAd1
držitelé	držitel	k1gMnPc1
dvou	dva	k4xCgFnPc2
nejvyšších	vysoký	k2eAgFnPc2d3
tříd	třída	k1gFnPc2
mají	mít	k5eAaImIp3nP
právo	právo	k1gNnSc4
užívat	užívat	k5eAaImF
titul	titul	k1gInSc4
Sir	sir	k1gMnSc1
nebo	nebo	k8xC
Dame	Dame	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
právo	právo	k1gNnSc1
mají	mít	k5eAaImIp3nP
ovšem	ovšem	k9
jen	jen	k9
občané	občan	k1gMnPc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
a	a	k8xC
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
hlavou	hlava	k1gFnSc7
státu	stát	k1gInSc2
jeho	jeho	k3xOp3gMnSc1
panovník	panovník	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
současnosti	současnost	k1gFnSc6
královna	královna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Panovník	panovník	k1gMnSc1
však	však	k9
může	moct	k5eAaImIp3nS
Řád	řád	k1gInSc1
britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
udělit	udělit	k5eAaPmF
i	i	k8xC
občanům	občan	k1gMnPc3
jiných	jiný	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
;	;	kIx,
tyto	tento	k3xDgFnPc1
osoby	osoba	k1gFnPc1
pak	pak	k6eAd1
obdrží	obdržet	k5eAaPmIp3nP
tzv.	tzv.	kA
čestný	čestný	k2eAgInSc4d1
řád	řád	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
praxi	praxe	k1gFnSc6
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
daná	daný	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
nemůže	moct	k5eNaImIp3nS
používat	používat	k5eAaImF
titul	titul	k1gInSc4
Sir	sir	k1gMnSc1
nebo	nebo	k8xC
Dame	Dame	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
jako	jako	k8xC,k8xS
všichni	všechen	k3xTgMnPc1
členové	člen	k1gMnPc1
řádu	řád	k1gInSc2
může	moct	k5eAaImIp3nS
za	za	k7c7
svým	svůj	k3xOyFgNnSc7
jménem	jméno	k1gNnSc7
užívat	užívat	k5eAaImF
zkratku	zkratka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
uvedena	uvést	k5eAaPmNgFnS
v	v	k7c6
závorce	závorka	k1gFnSc6
u	u	k7c2
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
již	již	k6eAd1
obdržela	obdržet	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
Bill	Bill	k1gMnSc1
Gates	Gates	k1gMnSc1
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yQgMnSc3,k3yRgMnSc3
byl	být	k5eAaImAgInS
čestný	čestný	k2eAgInSc1d1
řád	řád	k1gInSc1
udělen	udělen	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
a	a	k8xC
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
občanem	občan	k1gMnSc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
své	svůj	k3xOyFgMnPc4
jméno	jméno	k1gNnSc1
píše	psát	k5eAaImIp3nS
„	„	k?
<g/>
William	William	k1gInSc1
Henry	Henry	k1gMnSc1
Gates	Gates	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
KBE	KBE	kA
<g/>
“	“	k?
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
pouze	pouze	k6eAd1
„	„	k?
<g/>
Sir	sir	k1gMnSc1
William	William	k1gInSc4
Henry	henry	k1gInSc2
Gates	Gates	k1gMnSc1
III	III	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Vyznamenaní	vyznamenaný	k2eAgMnPc1d1
Češi	Čech	k1gMnPc1
</s>
<s>
Třída	třída	k1gFnSc1
Grand	grand	k1gMnSc1
Cross	Cross	k1gInSc1
(	(	kIx(
<g/>
GBE	GBE	kA
<g/>
)	)	kIx)
</s>
<s>
Edvard	Edvard	k1gMnSc1
Beneš	Beneš	k1gMnSc1
(	(	kIx(
<g/>
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
druhý	druhý	k4xOgMnSc1
prezident	prezident	k1gMnSc1
Československa	Československo	k1gNnSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1923	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třída	třída	k1gFnSc1
Knight	Knight	k1gMnSc1
Commander	Commander	k1gMnSc1
(	(	kIx(
<g/>
KBE	KBE	kA
<g/>
)	)	kIx)
</s>
<s>
Rafael	Rafael	k1gMnSc1
Kubelík	Kubelík	k1gMnSc1
(	(	kIx(
<g/>
dirigent	dirigent	k1gMnSc1
<g/>
,	,	kIx,
houslista	houslista	k1gMnSc1
<g/>
,	,	kIx,
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1996	#num#	k4
</s>
<s>
Libor	Libor	k1gMnSc1
Pešek	Pešek	k1gMnSc1
(	(	kIx(
<g/>
dirigent	dirigent	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1996	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třída	třída	k1gFnSc1
Commander	Commander	k1gMnSc1
(	(	kIx(
<g/>
CBE	CBE	kA
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Berounský	berounský	k2eAgMnSc1d1
(	(	kIx(
<g/>
brigádní	brigádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
<g/>
,	,	kIx,
letec	letec	k1gMnSc1
RAF	raf	k0
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1942	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
Bělohlávek	Bělohlávek	k1gMnSc1
(	(	kIx(
<g/>
dirigent	dirigent	k1gMnSc1
<g/>
,	,	kIx,
hudební	hudební	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
2012	#num#	k4
</s>
<s>
Silvestr	Silvestr	k1gMnSc1
Bláha	Bláha	k1gMnSc1
(	(	kIx(
<g/>
divizní	divizní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1925	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Čihák	Čihák	k1gMnSc1
(	(	kIx(
<g/>
divizní	divizní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1941	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Duda	Duda	k1gMnSc1
(	(	kIx(
<g/>
brigádní	brigádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
<g/>
,	,	kIx,
letec	letec	k1gMnSc1
RAF	raf	k0
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1947	#num#	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Eva	Eva	k1gFnSc1
Jiřičná	Jiřičný	k2eAgFnSc1d1
(	(	kIx(
<g/>
architektka	architektka	k1gFnSc1
a	a	k8xC
designérka	designérka	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1994	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Alois	Alois	k1gMnSc1
Kubita	Kubita	k1gMnSc1
(	(	kIx(
<g/>
brigádní	brigádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
<g/>
,	,	kIx,
letec	letec	k1gMnSc1
RAF	raf	k0
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1943	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Alois	Alois	k1gMnSc1
Liška	Liška	k1gMnSc1
(	(	kIx(
<g/>
armádní	armádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělit	k5eAaPmNgInS
během	během	k7c2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
František	František	k1gMnSc1
Moravec	Moravec	k1gMnSc1
(	(	kIx(
<g/>
brigádní	brigádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
,	,	kIx,
zpravodajský	zpravodajský	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Satorie	satorie	k1gFnSc2
(	(	kIx(
<g/>
brigádní	brigádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Schejbal	Schejbal	k1gMnSc1
(	(	kIx(
<g/>
brigádní	brigádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
<g/>
,	,	kIx,
letec	letec	k1gMnSc1
RAF	raf	k0
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1942	#num#	k4
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třída	třída	k1gFnSc1
Officer	Officra	k1gFnPc2
(	(	kIx(
<g/>
OBE	Ob	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Ján	Ján	k1gMnSc1
Ambruš	Ambruš	k1gMnSc1
(	(	kIx(
<g/>
brigádní	brigádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
<g/>
,	,	kIx,
letec	letec	k1gMnSc1
RAF	raf	k0
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1945	#num#	k4
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bohumil	Bohumil	k1gMnSc1
Boček	Boček	k1gMnSc1
(	(	kIx(
<g/>
armádní	armádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Hlaďo	Hlaďo	k1gMnSc1
(	(	kIx(
<g/>
generálmajor	generálmajor	k1gMnSc1
<g/>
,	,	kIx,
letec	letec	k1gMnSc1
RAF	raf	k0
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1947	#num#	k4
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Charles	Charles	k1gMnSc1
G.	G.	kA
Strasser	Strasser	k1gMnSc1
(	(	kIx(
<g/>
účastník	účastník	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
,	,	kIx,
fotograf	fotograf	k1gMnSc1
<g/>
,	,	kIx,
filantrop	filantrop	k1gMnSc1
českého	český	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
2000	#num#	k4
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Milan	Milan	k1gMnSc1
Vyhnálek	Vyhnálek	k1gMnSc1
(	(	kIx(
<g/>
podnikatel	podnikatel	k1gMnSc1
v	v	k7c6
oboru	obor	k1gInSc6
sýrařství	sýrařství	k1gNnSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1978	#num#	k4
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třída	třída	k1gFnSc1
Member	Member	k1gMnSc1
(	(	kIx(
<g/>
MBE	MBE	kA
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Bergmann	Bergmann	k1gMnSc1
(	(	kIx(
<g/>
major	major	k1gMnSc1
<g/>
,	,	kIx,
lékař	lékař	k1gMnSc1
ve	v	k7c6
službách	služba	k1gFnPc6
RAF	raf	k0
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1946	#num#	k4
</s>
<s>
Josef	Josef	k1gMnSc1
Bryks	Bryks	k1gInSc1
(	(	kIx(
<g/>
brigádní	brigádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
<g/>
,	,	kIx,
pilot	pilot	k1gMnSc1
RAF	raf	k0
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1946	#num#	k4
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
Fantl	Fantl	k1gMnSc1
(	(	kIx(
<g/>
plukovník	plukovník	k1gMnSc1
<g/>
,	,	kIx,
letec	letec	k1gMnSc1
RAF	raf	k0
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1941	#num#	k4
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Martin	Martin	k1gMnSc1
Hilský	Hilský	k1gMnSc1
(	(	kIx(
<g/>
literární	literární	k2eAgMnSc1d1
historik	historik	k1gMnSc1
<g/>
,	,	kIx,
překladatel	překladatel	k1gMnSc1
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
2001	#num#	k4
</s>
<s>
Jan	Jan	k1gMnSc1
Horal	horal	k1gMnSc1
(	(	kIx(
<g/>
plukovník	plukovník	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
,	,	kIx,
manažer	manažer	k1gMnSc1
<g/>
,	,	kIx,
podnikatel	podnikatel	k1gMnSc1
<g/>
,	,	kIx,
hoteliér	hoteliér	k1gMnSc1
<g/>
,	,	kIx,
iniciátor	iniciátor	k1gMnSc1
kulturního	kulturní	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
,	,	kIx,
filantrop	filantrop	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
2004	#num#	k4
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Kafka	Kafka	k1gMnSc1
(	(	kIx(
<g/>
plukovník	plukovník	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
,	,	kIx,
Ing.	ing.	kA
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1945	#num#	k4
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Alois	Alois	k1gMnSc1
Konopický	Konopický	k2eAgMnSc1d1
(	(	kIx(
<g/>
plukovník	plukovník	k1gMnSc1
<g/>
,	,	kIx,
důstojník	důstojník	k1gMnSc1
311	#num#	k4
<g/>
.	.	kIx.
československé	československý	k2eAgFnSc2d1
bombardovací	bombardovací	k2eAgFnSc2d1
perutě	peruť	k1gFnSc2
RAF	raf	k0
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1993	#num#	k4
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Mandl	mandl	k1gInSc1
(	(	kIx(
<g/>
podnikatel	podnikatel	k1gMnSc1
v	v	k7c6
papírenském	papírenský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1991	#num#	k4
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Nedvěd	Nedvěd	k1gMnSc1
(	(	kIx(
<g/>
generálporučík	generálporučík	k1gMnSc1
<g/>
,	,	kIx,
pilot	pilot	k1gMnSc1
RAF	raf	k0
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1941	#num#	k4
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Ondráček	Ondráček	k1gMnSc1
(	(	kIx(
<g/>
brigádní	brigádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1945	#num#	k4
</s>
<s>
Josef	Josef	k1gMnSc1
Otisk	otisk	k1gInSc1
(	(	kIx(
<g/>
podplukovník	podplukovník	k1gMnSc1
<g/>
,	,	kIx,
výsadkář	výsadkář	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1940	#num#	k4
</s>
<s>
František	František	k1gMnSc1
Řežábek	Řežábek	k1gMnSc1
(	(	kIx(
<g/>
generálmajor	generálmajor	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1943	#num#	k4
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Petr	Petr	k1gMnSc1
Torák	Torák	k1gMnSc1
(	(	kIx(
<g/>
britský	britský	k2eAgMnSc1d1
policista	policista	k1gMnSc1
romské	romský	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
2015	#num#	k4
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Alois	Alois	k1gMnSc1
Vicherek	Vicherka	k1gFnPc2
(	(	kIx(
<g/>
sborový	sborový	k2eAgMnSc1d1
generál	generál	k1gMnSc1
<g/>
,	,	kIx,
letec	letec	k1gMnSc1
RAF	raf	k0
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1941	#num#	k4
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Vrdlovec	Vrdlovec	k1gMnSc1
(	(	kIx(
<g/>
generálmajor	generálmajor	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
odboje	odboj	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
udělen	udělen	k2eAgInSc4d1
1945	#num#	k4
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Beneš	Beneš	k1gMnSc1
<g/>
,	,	kIx,
Edvard	Edvard	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
URBANOVÁ	Urbanová	k1gFnSc1
<g/>
,	,	kIx,
Karin	Karina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libor	Libor	k1gMnSc1
Pešek	Pešek	k1gMnSc1
–	–	k?
velký	velký	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
dirigent	dirigent	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
JANÁČKOVA	Janáčkův	k2eAgFnSc1d1
AKADEMIE	akademie	k1gFnSc1
MÚZICKÝCH	múzický	k2eAgNnPc2d1
UMĚNÍ	umění	k1gNnPc2
V	v	k7c6
BRNĚ	Brno	k1gNnSc6
<g/>
,	,	kIx,
Hudební	hudební	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bláha	Bláha	k1gMnSc1
<g/>
,	,	kIx,
Silvestr	Silvestr	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Čihák	Čihák	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
[	[	kIx(
<g/>
1891	#num#	k4
<g/>
-	-	kIx~
<g/>
1944	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Duda	Duda	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
EVA	Eva	k1gFnSc1
JIŘIČNÁ	JIŘIČNÁ	kA
-	-	kIx~
Český	český	k2eAgInSc1d1
dialog	dialog	k1gInSc1
<g/>
.	.	kIx.
www.cesky-dialog.net	www.cesky-dialog.net	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kubita	Kubita	k1gMnSc1
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Liška	Liška	k1gMnSc1
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pomsta	pomsta	k1gFnSc1
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
:	:	kIx,
dvanáct	dvanáct	k4xCc1
let	léto	k1gNnPc2
kriminálu	kriminál	k1gInSc2
pro	pro	k7c4
držitele	držitel	k1gMnSc4
Řádu	řád	k1gInSc2
britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
|	|	kIx~
Lidé	člověk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-06-13	2020-06-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
„	„	k?
<g/>
Špión	špión	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgMnS
mužem	muž	k1gMnSc7
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
místě	místo	k1gNnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
VHU	VHU	kA
Praha	Praha	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Moravec	Moravec	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Satorie	satorie	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Schejbal	Schejbal	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Free	Free	k1gNnSc1
Czechoslovak	Czechoslovak	k1gMnSc1
Air	Air	k1gMnSc2
Force	force	k1gFnSc2
<g/>
.	.	kIx.
fcafa	fcaf	k1gMnSc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
RAJLICH	RAJLICH	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
nebi	nebe	k1gNnSc6
hrdého	hrdý	k2eAgInSc2d1
Albionu	Albion	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
první	první	k4xOgFnSc2
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ares	Ares	k1gMnSc1
s.	s.	k?
r.	r.	kA
o.	o.	k?
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
709	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86158	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
495	#num#	k4
<g/>
-	-	kIx~
<g/>
500	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ambruš	Ambruš	k1gMnSc1
<g/>
,	,	kIx,
Ján	Ján	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Boček	Boček	k1gMnSc1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hlaďo	Hlaďo	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Charles	Charles	k1gMnSc1
G	G	kA
Strasser	Strasser	k1gMnSc1
OBE	Ob	k1gInSc5
|	|	kIx~
Historic	Historic	k1gMnSc1
Flight	Flight	k1gMnSc1
<g/>
.	.	kIx.
www.historicflight.cz	www.historicflight.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SUPPLEMENT	SUPPLEMENT	kA
TO	to	k9
THE	THE	kA
LONDON	London	k1gMnSc1
GAZETTE	GAZETTE	kA
<g/>
,	,	kIx,
30TH	30TH	k4
DECEMBER	DECEMBER	kA
1978	#num#	k4
<g/>
.	.	kIx.
www.thegazette.co.uk	www.thegazette.co.uk	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bryks	Bryks	k1gInSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Život	život	k1gInSc1
Mnichovic	Mnichovice	k1gFnPc2
<g/>
.	.	kIx.
www.mnichovice.cz	www.mnichovice.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jonathan	Jonathan	k1gMnSc1
Rea	Rea	k1gFnSc1
obdržel	obdržet	k5eAaPmAgInS
řád	řád	k1gInSc1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
www.bikeracing.cz	www.bikeracing.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Catalogue	Catalogue	k1gInSc1
description	description	k1gInSc1
</s>
<s>
Recommendation	Recommendation	k1gInSc1
for	forum	k1gNnPc2
Award	Awarda	k1gFnPc2
for	forum	k1gNnPc2
Kafka	Kafka	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
Rank	rank	k1gInSc1
<g/>
:	:	kIx,
2	#num#	k4
<g/>
nd	nd	k?
Major	major	k1gMnSc1
Service	Service	k1gFnPc4
No	no	k9
<g/>
:	:	kIx,
....	....	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Šumavský	šumavský	k2eAgInSc4d1
rozcestník	rozcestník	k1gInSc4
:	:	kIx,
Konopický	Konopický	k2eAgMnSc1d1
Alois	Alois	k1gMnSc1
<g/>
,	,	kIx,
plukovník	plukovník	k1gMnSc1
v.v.	v.v.	k?
<g/>
.	.	kIx.
www.sumava.cz	www.sumava.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Panel	panel	k1gInSc1
RAF04	RAF04	k1gFnPc2
Panel	panel	k1gInSc1
RAF04	RAF04	k1gMnSc1
-	-	kIx~
Letci	letec	k1gMnPc1
Plumlov	Plumlovo	k1gNnPc2
z.	z.	k?
<g/>
s.	s.	k?
<g/>
.	.	kIx.
www.svazletcu.cz	www.svazletcu.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Wing	Wing	k1gMnSc1
Commander	Commander	k1gMnSc1
Vladimir	Vladimir	k1gMnSc1
Nedved	Nedved	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Telegraph	Telegraph	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nedvěd	Nedvěd	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ondráček	Ondráček	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Řežábek	Řežábek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Policista	policista	k1gMnSc1
a	a	k8xC
český	český	k2eAgMnSc1d1
Rom	Rom	k1gMnSc1
Petr	Petr	k1gMnSc1
Torák	Torák	k1gMnSc1
v	v	k7c6
Londýně	Londýn	k1gInSc6
převzal	převzít	k5eAaPmAgInS
Řád	řád	k1gInSc1
britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vicherek	Vicherka	k1gFnPc2
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vrdlovec	Vrdlovec	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
https://www.valka.cz	https://www.valka.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
generálmajor	generálmajor	k1gMnSc1
Karel	Karel	k1gMnSc1
Vrdlovec	Vrdlovec	k1gMnSc1
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Catalogue	Catalogue	k1gInSc1
description	description	k1gInSc1
</s>
<s>
Recommendation	Recommendation	k1gInSc1
for	forum	k1gNnPc2
Award	Awarda	k1gFnPc2
for	forum	k1gNnPc2
Vrdlovec	Vrdlovec	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Rank	rank	k1gInSc1
<g/>
:	:	kIx,
2	#num#	k4
<g/>
nd	nd	k?
Major	major	k1gMnSc1
Service	Service	k1gFnPc4
No	no	k9
<g/>
:	:	kIx,
....	....	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
LOBKOWICZ	Lobkowicz	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
řádů	řád	k1gInPc2
a	a	k8xC
vyznamenání	vyznamenání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
255	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85983	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
britských	britský	k2eAgNnPc2d1
vyznamenání	vyznamenání	k1gNnPc2
</s>
<s>
Nositelé	nositel	k1gMnPc1
Řádu	řád	k1gInSc2
britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kultura	kultura	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4298376-9	4298376-9	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nr	nr	k?
<g/>
90010472	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
157543990	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nr	lccn-nr	k1gInSc1
<g/>
90010472	#num#	k4
</s>
