<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
Magyar	Magyar	k1gMnSc1	Magyar
Köztársaság	Köztársaság	k1gMnSc1	Köztársaság
címere	címrat	k5eAaPmIp3nS	címrat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
symbolech	symbol	k1gInPc6	symbol
MR	MR	kA	MR
oficiálním	oficiální	k2eAgInSc7d1	oficiální
státním	státní	k2eAgInSc7d1	státní
symbolem	symbol	k1gInSc7	symbol
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunismu	komunismus	k1gInSc2	komunismus
byl	být	k5eAaImAgInS	být
současný	současný	k2eAgInSc1d1	současný
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
slavnostně	slavnostně	k6eAd1	slavnostně
přijat	přijat	k2eAgInSc1d1	přijat
Národním	národní	k2eAgNnSc7d1	národní
shromážděním	shromáždění	k1gNnSc7	shromáždění
dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
a	a	k8xC	a
historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
má	mít	k5eAaImIp3nS	mít
polcený	polcený	k2eAgInSc1d1	polcený
štít	štít	k1gInSc1	štít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
poli	pole	k1gFnSc6	pole
je	být	k5eAaImIp3nS	být
sedmkrát	sedmkrát	k6eAd1	sedmkrát
červeno-stříbrně	červenotříbrně	k6eAd1	červeno-stříbrně
dělený	dělený	k2eAgInSc1d1	dělený
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
představuje	představovat	k5eAaImIp3nS	představovat
uherské	uherský	k2eAgFnPc4d1	uherská
řeky	řeka	k1gFnPc4	řeka
–	–	k?	–
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
,	,	kIx,	,
Tisu	Tisa	k1gFnSc4	Tisa
<g/>
,	,	kIx,	,
Drávu	Dráva	k1gFnSc4	Dráva
a	a	k8xC	a
Sávu	Sáva	k1gFnSc4	Sáva
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
znak	znak	k1gInSc4	znak
Arpádské	Arpádský	k2eAgFnSc2d1	Arpádský
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vládla	vládnout	k5eAaImAgFnS	vládnout
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
Uherského	uherský	k2eAgInSc2d1	uherský
státu	stát	k1gInSc2	stát
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1301	[number]	k4	1301
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
poli	pole	k1gNnSc6	pole
je	být	k5eAaImIp3nS	být
zelené	zelený	k2eAgNnSc4d1	zelené
trojvrší	trojvrší	k1gNnSc4	trojvrší
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
korunou	koruna	k1gFnSc7	koruna
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
dvojkříž	dvojkříž	k6eAd1	dvojkříž
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
trojvrší	trojvrší	k2eAgFnSc7d1	trojvrší
interpretací	interpretace	k1gFnSc7	interpretace
uherských	uherský	k2eAgMnPc2d1	uherský
pohoří	pohořet	k5eAaPmIp3nS	pohořet
–	–	k?	–
Tatra	Tatra	k1gFnSc1	Tatra
<g/>
,	,	kIx,	,
Mátra	Mátra	k1gFnSc1	Mátra
a	a	k8xC	a
Fatra	Fatra	k1gFnSc1	Fatra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
znak	znak	k1gInSc1	znak
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
dvojramenný	dvojramenný	k2eAgInSc1d1	dvojramenný
kříž	kříž	k1gInSc1	kříž
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
již	již	k6eAd1	již
na	na	k7c6	na
erbu	erb	k1gInSc6	erb
krále	král	k1gMnSc2	král
Bély	Béla	k1gMnSc2	Béla
III	III	kA	III
<g/>
.	.	kIx.	.
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
štítem	štít	k1gInSc7	štít
je	být	k5eAaImIp3nS	být
koruna	koruna	k1gFnSc1	koruna
sv.	sv.	kA	sv.
Štěpána	Štěpána	k1gFnSc1	Štěpána
I	i	k9	i
<g/>
,	,	kIx,	,
zakladatele	zakladatel	k1gMnSc2	zakladatel
Uherského	uherský	k2eAgNnSc2d1	Uherské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
šikmý	šikmý	k2eAgInSc4d1	šikmý
kříž	kříž	k1gInSc4	kříž
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgFnSc4d1	připomínající
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Maďaři	Maďar	k1gMnPc1	Maďar
raději	rád	k6eAd2	rád
korunu	koruna	k1gFnSc4	koruna
ukryjí	ukrýt	k5eAaPmIp3nP	ukrýt
<g/>
,	,	kIx,	,
nežli	nežli	k8xS	nežli
by	by	kYmCp3nP	by
ji	on	k3xPp3gFnSc4	on
předali	předat	k5eAaPmAgMnP	předat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
nepřátelům	nepřítel	k1gMnPc3	nepřítel
či	či	k8xC	či
samovládcům	samovládce	k1gMnPc3	samovládce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
nad	nad	k7c7	nad
znakem	znak	k1gInSc7	znak
není	být	k5eNaImIp3nS	být
Svatoštěpánská	svatoštěpánský	k2eAgFnSc1d1	Svatoštěpánská
koruna	koruna	k1gFnSc1	koruna
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Kossuth-címer	Kossuthímer	k1gInSc4	Kossuth-címer
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Kossuthův	Kossuthův	k2eAgInSc1d1	Kossuthův
znak	znak	k1gInSc1	znak
pojmenovaný	pojmenovaný	k2eAgInSc1d1	pojmenovaný
podle	podle	k7c2	podle
Lajose	Lajosa	k1gFnSc3	Lajosa
Kossutha	Kossutha	k1gFnSc1	Kossutha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
na	na	k7c6	na
koruně	koruna	k1gFnSc6	koruna
byl	být	k5eAaImAgInS	být
ohnut	ohnout	k5eAaPmNgInS	ohnout
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
Ladislava	Ladislav	k1gMnSc2	Ladislav
Pohrobka	pohrobek	k1gMnSc2	pohrobek
roku	rok	k1gInSc2	rok
1440	[number]	k4	1440
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Alžběta	Alžběta	k1gFnSc1	Alžběta
dala	dát	k5eAaPmAgFnS	dát
příkaz	příkaz	k1gInSc4	příkaz
své	svůj	k3xOyFgFnSc3	svůj
dvorní	dvorní	k2eAgFnSc3d1	dvorní
dámě	dáma	k1gFnSc3	dáma
Heleně	Helena	k1gFnSc3	Helena
Kottannerové	Kottannerová	k1gFnSc3	Kottannerová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
uherskou	uherský	k2eAgFnSc4d1	uherská
korunu	koruna	k1gFnSc4	koruna
ukradla	ukradnout	k5eAaPmAgFnS	ukradnout
polskému	polský	k2eAgMnSc3d1	polský
králi	král	k1gMnSc3	král
Vladislavovi	Vladislav	k1gMnSc3	Vladislav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonskému	jagellonský	k2eAgMnSc3d1	jagellonský
Varenčíkovi	Varenčík	k1gMnSc3	Varenčík
a	a	k8xC	a
přivezla	přivézt	k5eAaPmAgFnS	přivézt
do	do	k7c2	do
Komárna	Komárno	k1gNnSc2	Komárno
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
převozu	převoz	k1gInSc6	převoz
koruna	koruna	k1gFnSc1	koruna
spadla	spadnout	k5eAaPmAgFnS	spadnout
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
kříž	kříž	k1gInSc4	kříž
se	se	k3xPyFc4	se
ohnul	ohnout	k5eAaPmAgMnS	ohnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současná	současný	k2eAgFnSc1d1	současná
verze	verze	k1gFnSc1	verze
znaku	znak	k1gInSc2	znak
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
při	při	k7c6	při
Rakousko-Uherském	rakouskoherský	k2eAgNnSc6d1	rakousko-uherské
vyrovnání	vyrovnání	k1gNnSc6	vyrovnání
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
první	první	k4xOgInSc1	první
polcený	polcený	k2eAgInSc1d1	polcený
štít	štít	k1gInSc1	štít
spojující	spojující	k2eAgInSc1d1	spojující
znak	znak	k1gInSc1	znak
Arpádské	Arpádský	k2eAgFnSc2d1	Arpádský
dynastie	dynastie	k1gFnSc2	dynastie
a	a	k8xC	a
zelené	zelený	k2eAgNnSc1d1	zelené
trojvrší	trojvrší	k1gNnSc1	trojvrší
s	s	k7c7	s
dvojkřížem	dvojkřížem	k6eAd1	dvojkřížem
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
již	již	k6eAd1	již
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
podoba	podoba	k1gFnSc1	podoba
se	se	k3xPyFc4	se
ustálila	ustálit	k5eAaPmAgFnS	ustálit
až	až	k9	až
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1576	[number]	k4	1576
<g/>
-	-	kIx~	-
<g/>
1608	[number]	k4	1608
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oficiální	oficiální	k2eAgInSc4d1	oficiální
popis	popis	k1gInSc4	popis
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc4d1	oficiální
popis	popis	k1gInSc4	popis
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
z	z	k7c2	z
Ústavy	ústava	k1gFnSc2	ústava
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Používání	používání	k1gNnSc1	používání
znaku	znak	k1gInSc2	znak
===	===	k?	===
</s>
</p>
<p>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
používání	používání	k1gNnSc4	používání
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
republiky	republika	k1gFnSc2	republika
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zákon	zákon	k1gInSc1	zákon
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
évi	évi	k?	évi
LXXXIII	LXXXIII	kA	LXXXIII
<g/>
.	.	kIx.	.
törvény	törvén	k1gInPc1	törvén
a	a	k8xC	a
Magyar	Magyar	k1gMnSc1	Magyar
Köztársaság	Köztársaság	k1gMnSc1	Köztársaság
nemzeti	mzet	k5eNaImF	mzet
jelképeinek	jelképeinek	k1gInSc4	jelképeinek
és	és	k?	és
a	a	k8xC	a
Magyar	Magyar	k1gMnSc1	Magyar
Köztársaságra	Köztársaságra	k1gMnSc1	Köztársaságra
utaló	utaló	k?	utaló
elnevezésnek	elnevezésnek	k1gMnSc1	elnevezésnek
a	a	k8xC	a
használatáról	használatáról	k1gMnSc1	használatáról
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
znaku	znak	k1gInSc2	znak
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Maďarské	maďarský	k2eAgNnSc1d1	Maďarské
velkoknížectví	velkoknížectví	k1gNnSc1	velkoknížectví
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Uherské	uherský	k2eAgNnSc1d1	Uherské
království	království	k1gNnSc1	království
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Uhersko	Uhersko	k1gNnSc1	Uhersko
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Rakouského	rakouský	k2eAgNnSc2d1	rakouské
císařství	císařství	k1gNnSc2	císařství
a	a	k8xC	a
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
===	===	k?	===
</s>
</p>
<p>
<s>
Znaky	znak	k1gInPc1	znak
zemí	zem	k1gFnPc2	zem
Uherska	Uhersko	k1gNnSc2	Uhersko
(	(	kIx(	(
<g/>
Zalitavska	Zalitavsko	k1gNnSc2	Zalitavsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Novodobé	novodobý	k2eAgNnSc1d1	novodobé
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Podobnost	podobnost	k1gFnSc1	podobnost
se	se	k3xPyFc4	se
znakem	znak	k1gInSc7	znak
Slovenska	Slovensko	k1gNnSc2	Slovensko
==	==	k?	==
</s>
</p>
<p>
<s>
Podobnost	podobnost	k1gFnSc1	podobnost
se	s	k7c7	s
státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
Slovenska	Slovensko	k1gNnSc2	Slovensko
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
historické	historický	k2eAgFnSc2d1	historická
souvislosti	souvislost	k1gFnSc2	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
se	se	k3xPyFc4	se
slovenští	slovenský	k2eAgMnPc1d1	slovenský
revolucionáři	revolucionář	k1gMnPc1	revolucionář
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Ľudovítem	Ľudovít	k1gMnSc7	Ľudovít
Štúrem	Štúr	k1gMnSc7	Štúr
hlásili	hlásit	k5eAaImAgMnP	hlásit
k	k	k7c3	k
Cyrilo-Metodějskému	Cyrilo-Metodějský	k2eAgInSc3d1	Cyrilo-Metodějský
odkazu	odkaz	k1gInSc3	odkaz
<g/>
,	,	kIx,	,
a	a	k8xC	a
znak	znak	k1gInSc4	znak
z	z	k7c2	z
heraldicky	heraldicky	k6eAd1	heraldicky
levé	levý	k2eAgFnSc2d1	levá
části	část	k1gFnSc2	část
znaku	znak	k1gInSc2	znak
Uherska	Uhersko	k1gNnSc2	Uhersko
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
–	–	k?	–
červený	červený	k2eAgInSc4d1	červený
štít	štít	k1gInSc4	štít
se	s	k7c7	s
zeleným	zelený	k2eAgNnSc7d1	zelené
trovrším	trovrší	k1gNnSc7	trovrší
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yIgInSc2	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
stříbrný	stříbrný	k2eAgMnSc1d1	stříbrný
dvojkříž	dvojkříž	k6eAd1	dvojkříž
používali	používat	k5eAaImAgMnP	používat
po	po	k7c6	po
úpravě	úprava	k1gFnSc6	úprava
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
uherské	uherský	k2eAgFnPc4d1	uherská
koruny	koruna	k1gFnPc4	koruna
<g/>
.	.	kIx.	.
<g/>
Slováci	Slovák	k1gMnPc1	Slovák
jakožto	jakožto	k8xS	jakožto
Slované	Slovan	k1gMnPc1	Slovan
podporující	podporující	k2eAgFnSc4d1	podporující
myšlenku	myšlenka	k1gFnSc4	myšlenka
Panslavismu	panslavismus	k1gInSc2	panslavismus
změnili	změnit	k5eAaPmAgMnP	změnit
barvu	barva	k1gFnSc4	barva
trojvrší	trojvrší	k1gNnSc2	trojvrší
ze	z	k7c2	z
zelené	zelená	k1gFnSc2	zelená
na	na	k7c4	na
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
znaku	znak	k1gInSc2	znak
tak	tak	k8xC	tak
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
slovanským	slovanský	k2eAgFnPc3d1	Slovanská
barvám	barva	k1gFnPc3	barva
(	(	kIx(	(
<g/>
bílá-červená-modrá	bílá-červenáodrý	k2eAgFnSc1d1	bílá-červená-modrý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
upravený	upravený	k2eAgInSc4d1	upravený
znak	znak	k1gInSc4	znak
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Ľudovít	Ľudovít	k1gMnSc1	Ľudovít
Štúr	Štúr	k1gMnSc1	Štúr
znakem	znak	k1gInSc7	znak
Slováků	Slovák	k1gMnPc2	Slovák
a	a	k8xC	a
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
vyryt	vyrýt	k5eAaPmNgInS	vyrýt
do	do	k7c2	do
pečetě	pečeť	k1gFnSc2	pečeť
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
tento	tento	k3xDgInSc1	tento
znak	znak	k1gInSc4	znak
převzala	převzít	k5eAaPmAgFnS	převzít
i	i	k9	i
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
Matica	Matica	k1gFnSc1	Matica
slovenská	slovenský	k2eAgFnSc1d1	slovenská
<g/>
.	.	kIx.	.
</s>
<s>
Slovenský	slovenský	k2eAgInSc1d1	slovenský
znak	znak	k1gInSc1	znak
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
nepatrně	patrně	k6eNd1	patrně
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
malými	malý	k2eAgFnPc7d1	malá
změnami	změna	k1gFnPc7	změna
kresby	kresba	k1gFnSc2	kresba
trojvrší	trojvrší	k1gNnSc2	trojvrší
nebo	nebo	k8xC	nebo
dvojramenného	dvojramenný	k2eAgInSc2d1	dvojramenný
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatné	samostatný	k2eAgFnSc2d1	samostatná
ČSR	ČSR	kA	ČSR
se	se	k3xPyFc4	se
slovenský	slovenský	k2eAgInSc1d1	slovenský
znak	znak	k1gInSc1	znak
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
všech	všecek	k3xTgInPc2	všecek
tří	tři	k4xCgInPc2	tři
československý	československý	k2eAgMnSc1d1	československý
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
slovenský	slovenský	k2eAgInSc1d1	slovenský
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
maďarského	maďarský	k2eAgInSc2d1	maďarský
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
ramena	rameno	k1gNnSc2	rameno
kříže	kříž	k1gInSc2	kříž
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
a	a	k8xC	a
vyhloubené	vyhloubený	k2eAgFnPc1d1	vyhloubená
<g/>
,	,	kIx,	,
vrchy	vrch	k1gInPc1	vrch
oblé	oblý	k2eAgInPc1d1	oblý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Magyarország	Magyarország	k1gInSc1	Magyarország
címere	címrat	k5eAaPmIp3nS	címrat
na	na	k7c6	na
maďarské	maďarský	k2eAgFnSc6d1	maďarská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BROŽEK	Brožek	k1gMnSc1	Brožek
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gNnSc1	lexikon
vlajek	vlajka	k1gFnPc2	vlajka
a	a	k8xC	a
znaků	znak	k1gInPc2	znak
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Kartografie	kartografie	k1gFnSc1	kartografie
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
223	[number]	k4	223
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7011	[number]	k4	7011
<g/>
-	-	kIx~	-
<g/>
776	[number]	k4	776
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MUCHA	Mucha	k1gMnSc1	Mucha
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Vlajky	vlajka	k1gFnPc1	vlajka
a	a	k8xC	a
Znaky	znak	k1gInPc1	znak
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
GKP	GKP	kA	GKP
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
215	[number]	k4	215
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
</s>
</p>
<p>
<s>
Uherská	uherský	k2eAgFnSc1d1	uherská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
</s>
</p>
<p>
<s>
Župní	župní	k2eAgInPc1d1	župní
znaky	znak	k1gInPc1	znak
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Coats	Coats	k1gInSc1	Coats
of	of	k?	of
arms	arms	k1gInSc1	arms
of	of	k?	of
Hungary	Hungara	k1gFnSc2	Hungara
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
National	National	k1gFnSc1	National
coats	coats	k1gInSc1	coats
of	of	k?	of
arms	arms	k1gInSc1	arms
of	of	k?	of
Hungary	Hungara	k1gFnSc2	Hungara
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
A	a	k8xC	a
Magyar	Magyar	k1gMnSc1	Magyar
Köztársaság	Köztársaság	k1gMnSc1	Köztársaság
címere	címrat	k5eAaPmIp3nS	címrat
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
Magyar	Magyar	k1gInSc1	Magyar
Nemezti	Nemezti	k1gNnSc1	Nemezti
Lap	lap	k1gInSc1	lap
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Historicaltextarchive	Historicaltextarchiev	k1gFnPc1	Historicaltextarchiev
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
THE	THE	kA	THE
HUNGARIAN	HUNGARIAN	kA	HUNGARIAN
COAT	COAT	kA	COAT
OF	OF	kA	OF
ARMS	ARMS	kA	ARMS
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Flags	Flagsa	k1gFnPc2	Flagsa
of	of	k?	of
The	The	k1gMnSc1	The
World	World	k1gMnSc1	World
-	-	kIx~	-
Hungary	Hungar	k1gInPc1	Hungar
-	-	kIx~	-
Coat	Coat	k2eAgInSc1d1	Coat
of	of	k?	of
Arms	Arms	k1gInSc1	Arms
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
Inf.	Inf.	k1gFnSc1	Inf.
<g/>
u-szeged	uzeged	k1gMnSc1	u-szeged
<g/>
.	.	kIx.	.
<g/>
hu	hu	k0	hu
-	-	kIx~	-
A	a	k9	a
magyar	magyar	k1gMnSc1	magyar
címer	címer	k1gMnSc1	címer
története	történout	k5eAaPmIp2nP	történout
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
A	a	k8xC	a
Magyar	Magyar	k1gMnSc1	Magyar
Köztársaság	Köztársaság	k1gMnSc1	Köztársaság
címere	címrat	k5eAaPmIp3nS	címrat
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
Címerek	Címerka	k1gFnPc2	Címerka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rumunsky	rumunsky	k6eAd1	rumunsky
<g/>
)	)	kIx)	)
Magyarország	Magyarország	k1gInSc4	Magyarország
nemzeti	mzet	k5eNaImF	mzet
jelképei	jelképei	k6eAd1	jelképei
</s>
</p>
