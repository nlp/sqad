<p>
<s>
Pascal	pascal	k1gInSc1	pascal
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
velká	velký	k2eAgFnSc1d1	velká
síla	síla	k1gFnSc1	síla
(	(	kIx(	(
<g/>
v	v	k7c6	v
newtonech	newton	k1gInPc6	newton
<g/>
)	)	kIx)	)
působí	působit	k5eAaImIp3nP	působit
na	na	k7c4	na
jednotkovou	jednotkový	k2eAgFnSc4d1	jednotková
plochu	plocha	k1gFnSc4	plocha
(	(	kIx(	(
<g/>
1	[number]	k4	1
m2	m2	k4	m2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
je	být	k5eAaImIp3nS	být
ekvivalentní	ekvivalentní	k2eAgNnSc4d1	ekvivalentní
N	N	kA	N
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
francouzském	francouzský	k2eAgInSc6d1	francouzský
matematikovi	matematik	k1gMnSc3	matematik
a	a	k8xC	a
fyzikovi	fyzik	k1gMnSc3	fyzik
Blaise	Blaise	k1gFnSc2	Blaise
Pascalovi	Pascal	k1gMnSc3	Pascal
(	(	kIx(	(
<g/>
1623	[number]	k4	1623
<g/>
–	–	k?	–
<g/>
1662	[number]	k4	1662
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hektopascal	hektopascal	k1gInSc1	hektopascal
(	(	kIx(	(
<g/>
hPa	hPa	k?	hPa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
v	v	k7c6	v
meteorologii	meteorologie	k1gFnSc6	meteorologie
měří	měřit	k5eAaImIp3nS	měřit
tlak	tlak	k1gInSc4	tlak
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
hPa	hPa	k?	hPa
=	=	kIx~	=
100	[number]	k4	100
Pa	Pa	kA	Pa
=	=	kIx~	=
1	[number]	k4	1
mb	mb	k?	mb
(	(	kIx(	(
<g/>
milibar	milibar	k1gInSc1	milibar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1	[number]	k4	1
Pa	Pa	kA	Pa
=	=	kIx~	=
1	[number]	k4	1
N	N	kA	N
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
</s>
</p>
<p>
<s>
1	[number]	k4	1
hPa	hPa	k?	hPa
=	=	kIx~	=
100	[number]	k4	100
Pa	Pa	kA	Pa
</s>
</p>
<p>
<s>
1	[number]	k4	1
kPa	kPa	k?	kPa
=	=	kIx~	=
1000	[number]	k4	1000
Pa	Pa	kA	Pa
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
užívala	užívat	k5eAaImAgFnS	užívat
též	též	k9	též
jednotka	jednotka	k1gFnSc1	jednotka
piè	piè	k?	piè
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
1	[number]	k4	1
kPa	kPa	k?	kPa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Megapascal	Megapascat	k5eAaPmAgInS	Megapascat
(	(	kIx(	(
<g/>
MPa	MPa	k1gFnSc4	MPa
<g/>
)	)	kIx)	)
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
síle	síla	k1gFnSc6	síla
jednoho	jeden	k4xCgInSc2	jeden
newtonu	newton	k1gInSc2	newton
působící	působící	k2eAgFnPc4d1	působící
na	na	k7c4	na
plochu	plocha	k1gFnSc4	plocha
jednoho	jeden	k4xCgInSc2	jeden
čtverečního	čtvereční	k2eAgInSc2d1	čtvereční
milimetru	milimetr	k1gInSc2	milimetr
(	(	kIx(	(
<g/>
1	[number]	k4	1
N	N	kA	N
<g/>
/	/	kIx~	/
<g/>
mm2	mm2	k4	mm2
=	=	kIx~	=
1	[number]	k4	1
MPa	MPa	k1gFnPc2	MPa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
běžný	běžný	k2eAgMnSc1d1	běžný
ve	v	k7c6	v
strojírenství	strojírenství	k1gNnSc6	strojírenství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
milimetr	milimetr	k1gInSc1	milimetr
běžně	běžně	k6eAd1	běžně
používanou	používaný	k2eAgFnSc7d1	používaná
jednotkou	jednotka	k1gFnSc7	jednotka
délky	délka	k1gFnSc2	délka
(	(	kIx(	(
<g/>
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
používajících	používající	k2eAgFnPc6d1	používající
soustavu	soustava	k1gFnSc4	soustava
SI	si	k1gNnSc1	si
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Megapascal	Megapascat	k5eAaPmAgMnS	Megapascat
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
strojírenství	strojírenství	k1gNnSc6	strojírenství
používán	používat	k5eAaImNgInS	používat
nejen	nejen	k6eAd1	nejen
jako	jako	k8xC	jako
jednotka	jednotka	k1gFnSc1	jednotka
tlaku	tlak	k1gInSc2	tlak
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stlačeného	stlačený	k2eAgInSc2d1	stlačený
plynu	plyn	k1gInSc2	plyn
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
jako	jako	k9	jako
jednotka	jednotka	k1gFnSc1	jednotka
napětí	napětí	k1gNnSc2	napětí
pro	pro	k7c4	pro
kontinuum	kontinuum	k1gNnSc4	kontinuum
vystavené	vystavený	k2eAgNnSc4d1	vystavené
působení	působení	k1gNnSc4	působení
vnější	vnější	k2eAgFnSc2d1	vnější
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzorec	vzorec	k1gInSc1	vzorec
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
1	[number]	k4	1
pascalu	pascal	k1gInSc2	pascal
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Pa	pa	k0	pa
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
Pa	Pa	kA	Pa
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
kg	kg	kA	kg
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
cdot	cdot	k1gMnSc1	cdot
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
kg	kg	kA	kg
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}}	}}}}}}	k?	}}}}}}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pascal	pascal	k1gInSc1	pascal
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
