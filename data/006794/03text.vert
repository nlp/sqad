<s>
Stehlík	Stehlík	k1gMnSc1	Stehlík
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Carduelis	Carduelis	k1gInSc1	Carduelis
carduelis	carduelis	k1gInSc1	carduelis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgMnSc1d1	malý
pěvec	pěvec	k1gMnSc1	pěvec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
pěnkavovití	pěnkavovití	k1gMnPc1	pěnkavovití
s	s	k7c7	s
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
areálem	areál	k1gInSc7	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
12	[number]	k4	12
cm	cm	kA	cm
Rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
<g/>
:	:	kIx,	:
23	[number]	k4	23
cm	cm	kA	cm
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
16	[number]	k4	16
g	g	kA	g
Stehlík	Stehlík	k1gMnSc1	Stehlík
obecný	obecný	k2eAgMnSc1d1	obecný
má	mít	k5eAaImIp3nS	mít
štíhlé	štíhlý	k2eAgNnSc1d1	štíhlé
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc1d1	silný
zašpičatělý	zašpičatělý	k2eAgInSc1d1	zašpičatělý
narůžovělý	narůžovělý	k2eAgInSc1d1	narůžovělý
zobák	zobák	k1gInSc1	zobák
a	a	k8xC	a
hnědé	hnědý	k2eAgFnPc1d1	hnědá
končetiny	končetina	k1gFnPc1	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
pestře	pestro	k6eAd1	pestro
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
<g/>
,	,	kIx,	,
svrchu	svrchu	k6eAd1	svrchu
béžově	béžově	k6eAd1	béžově
<g/>
,	,	kIx,	,
křídla	křídlo	k1gNnPc4	křídlo
má	mít	k5eAaImIp3nS	mít
zčásti	zčásti	k6eAd1	zčásti
černá	černý	k2eAgFnSc1d1	černá
s	s	k7c7	s
bílými	bílý	k2eAgFnPc7d1	bílá
skvrnami	skvrna	k1gFnPc7	skvrna
a	a	k8xC	a
jasným	jasný	k2eAgInSc7d1	jasný
žlutým	žlutý	k2eAgInSc7d1	žlutý
pruhem	pruh	k1gInSc7	pruh
<g/>
,	,	kIx,	,
zespodu	zespodu	k6eAd1	zespodu
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
má	mít	k5eAaImIp3nS	mít
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
červenou	červený	k2eAgFnSc4d1	červená
masku	maska	k1gFnSc4	maska
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
hlavy	hlava	k1gFnSc2	hlava
je	být	k5eAaImIp3nS	být
černobílý	černobílý	k2eAgInSc1d1	černobílý
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
stejně	stejně	k6eAd1	stejně
a	a	k9	a
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
obtížné	obtížný	k2eAgNnSc1d1	obtížné
je	být	k5eAaImIp3nS	být
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
větší	veliký	k2eAgFnSc4d2	veliký
a	a	k8xC	a
výraznější	výrazný	k2eAgFnSc4d2	výraznější
červenou	červený	k2eAgFnSc4d1	červená
masku	maska	k1gFnSc4	maska
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
hlavy	hlava	k1gFnSc2	hlava
než	než	k8xS	než
u	u	k7c2	u
samice	samice	k1gFnSc2	samice
a	a	k8xC	a
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
za	za	k7c4	za
oko	oko	k1gNnSc4	oko
<g/>
,	,	kIx,	,
ramena	rameno	k1gNnPc1	rameno
křídel	křídlo	k1gNnPc2	křídlo
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
samce	samec	k1gMnSc2	samec
celá	celý	k2eAgFnSc1d1	celá
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
samice	samice	k1gFnSc2	samice
z	z	k7c2	z
části	část	k1gFnSc2	část
šedivá	šedivý	k2eAgFnSc1d1	šedivá
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
je	být	k5eAaImIp3nS	být
také	také	k9	také
geometrie	geometrie	k1gFnSc1	geometrie
zobáku	zobák	k1gInSc2	zobák
<g/>
,	,	kIx,	,
samec	samec	k1gMnSc1	samec
mívá	mívat	k5eAaImIp3nS	mívat
zobák	zobák	k1gInSc4	zobák
delší	dlouhý	k2eAgInSc4d2	delší
a	a	k8xC	a
užší	úzký	k2eAgInSc4d2	užší
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
pravidlem	pravidlem	k6eAd1	pravidlem
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
postrádají	postrádat	k5eAaImIp3nP	postrádat
červenou	červený	k2eAgFnSc4d1	červená
masku	maska	k1gFnSc4	maska
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
vyjma	vyjma	k7c2	vyjma
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
Skandinávského	skandinávský	k2eAgInSc2d1	skandinávský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
východně	východně	k6eAd1	východně
až	až	k9	až
po	po	k7c4	po
Bajkalské	bajkalský	k2eAgNnSc4d1	Bajkalské
jezero	jezero	k1gNnSc4	jezero
a	a	k8xC	a
jižně	jižně	k6eAd1	jižně
po	po	k7c4	po
Himálaj	Himálaj	k1gFnSc4	Himálaj
a	a	k8xC	a
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
unikli	uniknout	k5eAaPmAgMnP	uniknout
z	z	k7c2	z
umělých	umělý	k2eAgInPc2d1	umělý
odchovů	odchov	k1gInPc2	odchov
<g/>
,	,	kIx,	,
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
stálou	stálý	k2eAgFnSc4d1	stálá
populaci	populace	k1gFnSc4	populace
i	i	k9	i
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
tažný	tažný	k2eAgInSc1d1	tažný
<g/>
,	,	kIx,	,
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
migruje	migrovat	k5eAaImIp3nS	migrovat
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
celoročně	celoročně	k6eAd1	celoročně
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
však	však	k9	však
o	o	k7c4	o
něco	něco	k3yInSc4	něco
vzácnější	vzácný	k2eAgNnSc1d2	vzácnější
<g/>
.	.	kIx.	.
</s>
<s>
Stehlíka	Stehlík	k1gMnSc4	Stehlík
obecného	obecný	k2eAgInSc2d1	obecný
běžně	běžně	k6eAd1	běžně
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
v	v	k7c6	v
otevřených	otevřený	k2eAgInPc6d1	otevřený
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
na	na	k7c6	na
mýtinách	mýtina	k1gFnPc6	mýtina
<g/>
,	,	kIx,	,
v	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
,	,	kIx,	,
ovocných	ovocný	k2eAgInPc6d1	ovocný
sadech	sad	k1gInPc6	sad
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kulturní	kulturní	k2eAgFnSc6d1	kulturní
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
zvonivým	zvonivý	k2eAgNnSc7d1	zvonivé
a	a	k8xC	a
svižným	svižný	k2eAgNnSc7d1	svižné
"	"	kIx"	"
<g/>
tiglitt	tiglitt	k1gInSc1	tiglitt
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
drsným	drsný	k2eAgNnSc7d1	drsné
"	"	kIx"	"
<g/>
čree	čree	k1gNnSc7	čree
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
období	období	k1gNnSc4	období
hnízdění	hnízdění	k1gNnSc2	hnízdění
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
často	často	k6eAd1	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
loukách	louka	k1gFnPc6	louka
nebo	nebo	k8xC	nebo
opuštěných	opuštěný	k2eAgInPc6d1	opuštěný
a	a	k8xC	a
zarostlých	zarostlý	k2eAgInPc6d1	zarostlý
pozemcích	pozemek	k1gInPc6	pozemek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
malá	malý	k2eAgNnPc4d1	malé
semena	semeno	k1gNnPc4	semeno
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
hvězdicovitých	hvězdicovitý	k2eAgFnPc2d1	hvězdicovitá
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
různé	různý	k2eAgInPc1d1	různý
pcháče	pcháč	k1gInPc1	pcháč
<g/>
,	,	kIx,	,
pupalka	pupalka	k1gFnSc1	pupalka
<g/>
,	,	kIx,	,
lociky	locika	k1gFnPc1	locika
<g/>
,	,	kIx,	,
mrkvovité	mrkvovitý	k2eAgFnPc1d1	mrkvovitá
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
bříz	bříza	k1gFnPc2	bříza
nebo	nebo	k8xC	nebo
olší	olše	k1gFnPc2	olše
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
vedle	vedle	k7c2	vedle
nezralých	zralý	k2eNgFnPc2d1	nezralá
semen	semeno	k1gNnPc2	semeno
krmena	krmit	k5eAaImNgFnS	krmit
také	také	k9	také
drobným	drobný	k2eAgInSc7d1	drobný
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
seskupuje	seskupovat	k5eAaImIp3nS	seskupovat
do	do	k7c2	do
početných	početný	k2eAgFnPc2d1	početná
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
40	[number]	k4	40
<g/>
členných	členný	k2eAgNnPc2d1	členné
hejn	hejno	k1gNnPc2	hejno
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
nepříznivého	příznivý	k2eNgNnSc2d1	nepříznivé
zimního	zimní	k2eAgNnSc2d1	zimní
období	období	k1gNnSc2	období
také	také	k9	také
často	často	k6eAd1	často
zalétává	zalétávat	k5eAaImIp3nS	zalétávat
na	na	k7c4	na
krmítka	krmítko	k1gNnPc4	krmítko
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
ročně	ročně	k6eAd1	ročně
mívá	mívat	k5eAaImIp3nS	mívat
1	[number]	k4	1
snůšku	snůška	k1gFnSc4	snůška
(	(	kIx(	(
<g/>
při	při	k7c6	při
zničení	zničení	k1gNnSc6	zničení
snůšky	snůška	k1gFnSc2	snůška
2	[number]	k4	2
-	-	kIx~	-
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
většinou	většinou	k6eAd1	většinou
dobře	dobře	k6eAd1	dobře
ukrytého	ukrytý	k2eAgNnSc2d1	ukryté
hnízda	hnízdo	k1gNnSc2	hnízdo
na	na	k7c6	na
větvích	větev	k1gFnPc6	větev
vysokých	vysoký	k2eAgInPc2d1	vysoký
listnatých	listnatý	k2eAgInPc2d1	listnatý
stromů	strom	k1gInPc2	strom
klade	klást	k5eAaImIp3nS	klást
3	[number]	k4	3
až	až	k9	až
6	[number]	k4	6
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
sedí	sedit	k5eAaImIp3nS	sedit
11	[number]	k4	11
-	-	kIx~	-
14	[number]	k4	14
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
opouštějí	opouštět	k5eAaImIp3nP	opouštět
hnízdo	hnízdo	k1gNnSc4	hnízdo
po	po	k7c6	po
dalších	další	k2eAgNnPc6d1	další
13	[number]	k4	13
-	-	kIx~	-
15	[number]	k4	15
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Nádherně	nádherně	k6eAd1	nádherně
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
stehlík	stehlík	k1gMnSc1	stehlík
obecný	obecný	k2eAgInSc4d1	obecný
poutal	poutat	k5eAaImAgInS	poutat
pozornost	pozornost	k1gFnSc4	pozornost
umělců	umělec	k1gMnPc2	umělec
i	i	k8xC	i
prostých	prostý	k2eAgMnPc2d1	prostý
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vítanou	vítaný	k2eAgFnSc7d1	vítaná
kořistí	kořist	k1gFnSc7	kořist
čižbářů	čižbář	k1gMnPc2	čižbář
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prehistorie	prehistorie	k1gFnSc2	prehistorie
<g/>
,	,	kIx,	,
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
středověk	středověk	k1gInSc4	středověk
a	a	k8xC	a
v	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
pěnkavovitými	pěnkavovitý	k2eAgMnPc7d1	pěnkavovitý
ptáky	pták	k1gMnPc7	pták
běžně	běžně	k6eAd1	běžně
chytán	chytán	k2eAgInSc1d1	chytán
(	(	kIx(	(
<g/>
i	i	k8xC	i
jako	jako	k9	jako
zdroj	zdroj	k1gInSc1	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stehlík	Stehlík	k1gMnSc1	Stehlík
v	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
symbolice	symbolika	k1gFnSc6	symbolika
může	moct	k5eAaImIp3nS	moct
souviset	souviset	k5eAaImF	souviset
s	s	k7c7	s
dětstvím	dětství	k1gNnSc7	dětství
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považován	považován	k2eAgInSc1d1	považován
i	i	k9	i
za	za	k7c4	za
výraz	výraz	k1gInSc4	výraz
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
milujícími	milující	k2eAgFnPc7d1	milující
se	se	k3xPyFc4	se
osobami	osoba	k1gFnPc7	osoba
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
být	být	k5eAaImF	být
přenosný	přenosný	k2eAgInSc1d1	přenosný
i	i	k9	i
na	na	k7c4	na
výše	vysoce	k6eAd2	vysoce
zmíněný	zmíněný	k2eAgInSc4d1	zmíněný
mystický	mystický	k2eAgInSc4d1	mystický
poměr	poměr	k1gInSc4	poměr
Krista	Kristus	k1gMnSc2	Kristus
a	a	k8xC	a
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
středověkých	středověký	k2eAgFnPc2d1	středověká
legend	legenda	k1gFnPc2	legenda
např.	např.	kA	např.
stehlík	stehlík	k1gMnSc1	stehlík
vytrhával	vytrhávat	k5eAaImAgMnS	vytrhávat
trny	trn	k1gInPc4	trn
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
Kristovy	Kristův	k2eAgFnPc1d1	Kristova
<g/>
,	,	kIx,	,
nesoucího	nesoucí	k2eAgInSc2d1	nesoucí
svůj	svůj	k3xOyFgInSc4	svůj
kříž	kříž	k1gInSc4	kříž
na	na	k7c4	na
Golgotu	Golgota	k1gFnSc4	Golgota
a	a	k8xC	a
červeň	červeň	k1gFnSc4	červeň
peříček	peříčko	k1gNnPc2	peříčko
kolem	kolem	k7c2	kolem
jeho	jeho	k3xOp3gInPc2	jeho
zobáku	zobák	k1gInSc2	zobák
bývá	bývat	k5eAaImIp3nS	bývat
pak	pak	k6eAd1	pak
ztotožňována	ztotožňován	k2eAgFnSc1d1	ztotožňována
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
Kristovou	Kristův	k2eAgFnSc7d1	Kristova
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
stehlík	stehlík	k1gMnSc1	stehlík
přitom	přitom	k6eAd1	přitom
potřísnil	potřísnit	k5eAaPmAgMnS	potřísnit
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
atraktivní	atraktivní	k2eAgInSc4d1	atraktivní
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
příjemný	příjemný	k2eAgInSc1d1	příjemný
zpěv	zpěv	k1gInSc1	zpěv
stal	stát	k5eAaPmAgMnS	stát
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
a	a	k8xC	a
ceněným	ceněný	k2eAgMnSc7d1	ceněný
"	"	kIx"	"
<g/>
klecním	klecní	k2eAgMnSc7d1	klecní
<g/>
"	"	kIx"	"
ptákem	pták	k1gMnSc7	pták
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
stehlíků	stehlík	k1gMnPc2	stehlík
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
neztratil	ztratit	k5eNaPmAgMnS	ztratit
nikdy	nikdy	k6eAd1	nikdy
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
popularitě	popularita	k1gFnSc6	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Známí	známý	k1gMnPc1	známý
jsou	být	k5eAaImIp3nP	být
pestře	pestro	k6eAd1	pestro
zbarvení	zbarvený	k2eAgMnPc1d1	zbarvený
kříženci	kříženec	k1gMnPc1	kříženec
stehlíka	stehlík	k1gMnSc2	stehlík
s	s	k7c7	s
kanárem	kanár	k1gMnSc7	kanár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
v	v	k7c6	v
ČR	ČR	kA	ČR
chování	chování	k1gNnSc1	chování
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
rozmnožováni	rozmnožován	k2eAgMnPc1d1	rozmnožován
jedinci	jedinec	k1gMnPc1	jedinec
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
péči	péče	k1gFnSc6	péče
člověka	člověk	k1gMnSc2	člověk
pocházející	pocházející	k2eAgFnSc7d1	pocházející
z	z	k7c2	z
registrovaných	registrovaný	k2eAgInPc2d1	registrovaný
chovů	chov	k1gInPc2	chov
(	(	kIx(	(
<g/>
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
platným	platný	k2eAgInSc7d1	platný
zákonem	zákon	k1gInSc7	zákon
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
chov	chov	k1gInSc1	chov
je	být	k5eAaImIp3nS	být
cenným	cenný	k2eAgInSc7d1	cenný
zdrojem	zdroj	k1gInSc7	zdroj
poučení	poučení	k1gNnSc2	poučení
a	a	k8xC	a
i	i	k9	i
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
bionomii	bionomie	k1gFnSc6	bionomie
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
jsou	být	k5eAaImIp3nP	být
vedle	vedle	k7c2	vedle
většího	veliký	k2eAgInSc2d2	veliký
poddruhu	poddruh	k1gInSc2	poddruh
(	(	kIx(	(
<g/>
C.	C.	kA	C.
carduelis	carduelis	k1gFnSc1	carduelis
major	major	k1gMnSc1	major
<g/>
)	)	kIx)	)
stále	stále	k6eAd1	stále
populárnější	populární	k2eAgFnPc1d2	populárnější
četné	četný	k2eAgFnPc1d1	četná
barevné	barevný	k2eAgFnPc1d1	barevná
mutace	mutace	k1gFnPc1	mutace
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
c.	c.	k?	c.
balcanica	balcanica	k1gFnSc1	balcanica
-	-	kIx~	-
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
c.	c.	k?	c.
brevirostris	brevirostris	k1gInSc1	brevirostris
-	-	kIx~	-
Krym	Krym	k1gInSc1	Krym
a	a	k8xC	a
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
c.	c.	k?	c.
britannica	britannica	k1gFnSc1	britannica
-	-	kIx~	-
Britské	britský	k2eAgInPc1d1	britský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
c.	c.	k?	c.
carduelis	carduelis	k1gInSc1	carduelis
-	-	kIx~	-
většinová	většinový	k2eAgFnSc1d1	většinová
část	část	k1gFnSc1	část
evropského	evropský	k2eAgInSc2d1	evropský
areálu	areál	k1gInSc2	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
c.	c.	k?	c.
loudoni	loudoň	k1gFnSc6	loudoň
-	-	kIx~	-
jižní	jižní	k2eAgInSc1d1	jižní
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
a	a	k8xC	a
Írán	Írán	k1gInSc1	Írán
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
c.	c.	k?	c.
majora	major	k1gMnSc2	major
-	-	kIx~	-
západní	západní	k2eAgFnSc1d1	západní
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
c.	c.	k?	c.
niediecki	niedieck	k1gFnSc2	niedieck
-	-	kIx~	-
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
c.	c.	k?	c.
parva	parva	k?	parva
-	-	kIx~	-
Apeninský	apeninský	k2eAgInSc1d1	apeninský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
,	,	kIx,	,
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
c.	c.	k?	c.
tschusii	tschusie	k1gFnSc6	tschusie
-	-	kIx~	-
Korsika	Korsika	k1gFnSc1	Korsika
<g/>
,	,	kIx,	,
Sardinie	Sardinie	k1gFnSc1	Sardinie
a	a	k8xC	a
Sicílie	Sicílie	k1gFnSc1	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Stehlík	Stehlík	k1gMnSc1	Stehlík
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
European	European	k1gMnSc1	European
Goldfinch	Goldfinch	k1gMnSc1	Goldfinch
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Szczygieł	Szczygieł	k1gFnSc6	Szczygieł
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Biolib	Biolib	k1gMnSc1	Biolib
www.priroda.cz	www.priroda.cz	k1gMnSc1	www.priroda.cz
Bezzel	Bezzel	k1gMnSc1	Bezzel
<g/>
,	,	kIx,	,
E.	E.	kA	E.
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Rebo	Rebo	k1gNnSc1	Rebo
Productions	Productionsa	k1gFnPc2	Productionsa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7234-292-1	[number]	k4	978-80-7234-292-1
</s>
