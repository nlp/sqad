<s>
Káva	káva	k1gFnSc1
</s>
<s>
O	o	k7c6
obci	obec	k1gFnSc6
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Káva	káva	k1gFnSc1
(	(	kIx(
<g/>
Maďarsko	Maďarsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kávová	kávový	k2eAgNnPc4d1
zrna	zrno	k1gNnPc4
po	po	k7c6
upražení	upražení	k1gNnSc6
(	(	kIx(
<g/>
arabika	arabika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Káva	káva	k1gFnSc1
je	být	k5eAaImIp3nS
nápoj	nápoj	k1gInSc4
z	z	k7c2
upražených	upražený	k2eAgNnPc2d1
a	a	k8xC
rozemletých	rozemletý	k2eAgNnPc2d1
semen	semeno	k1gNnPc2
plodů	plod	k1gInPc2
kávovníku	kávovník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označují	označovat	k5eAaImIp3nP
se	se	k3xPyFc4
tak	tak	k9
i	i	k9
samotná	samotný	k2eAgNnPc1d1
semena	semeno	k1gNnPc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
semena	semeno	k1gNnPc1
rozemletá	rozemletý	k2eAgNnPc1d1
na	na	k7c4
prášek	prášek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Káva	káva	k1gFnSc1
je	být	k5eAaImIp3nS
charakteristická	charakteristický	k2eAgFnSc1d1
svou	svůj	k3xOyFgFnSc7
silnou	silný	k2eAgFnSc7d1
vůní	vůně	k1gFnSc7
(	(	kIx(
<g/>
aroma	aroma	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
černou	černý	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
mimo	mimo	k7c4
jiné	jiný	k2eAgInPc4d1
alkaloid	alkaloid	k1gInSc4
kofein	kofein	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
povzbuzuje	povzbuzovat	k5eAaImIp3nS
srdeční	srdeční	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
a	a	k8xC
zvyšuje	zvyšovat	k5eAaImIp3nS
krevní	krevní	k2eAgInSc4d1
tlak	tlak	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Především	především	k6eAd1
se	se	k3xPyFc4
káva	káva	k1gFnSc1
pije	pít	k5eAaImIp3nS
pro	pro	k7c4
své	svůj	k3xOyFgInPc4
povzbuzující	povzbuzující	k2eAgInPc4d1
účinky	účinek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
velmi	velmi	k6eAd1
oblíbeným	oblíbený	k2eAgInSc7d1
nápojem	nápoj	k1gInSc7
při	při	k7c6
setkávání	setkávání	k1gNnSc6
lidí	člověk	k1gMnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
často	často	k6eAd1
podávána	podáván	k2eAgFnSc1d1
po	po	k7c6
jídle	jídlo	k1gNnSc6
(	(	kIx(
<g/>
jako	jako	k9
jakási	jakýsi	k3yIgFnSc1
„	„	k?
<g/>
tečka	tečka	k1gFnSc1
<g/>
“	“	k?
či	či	k8xC
pro	pro	k7c4
lepší	dobrý	k2eAgNnSc4d2
trávení	trávení	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
největším	veliký	k2eAgMnPc3d3
producentům	producent	k1gMnPc3
patří	patřit	k5eAaImIp3nS
Brazílie	Brazílie	k1gFnSc1
<g/>
,	,	kIx,
Vietnam	Vietnam	k1gInSc1
<g/>
,	,	kIx,
Kolumbie	Kolumbie	k1gFnSc1
a	a	k8xC
Indonésie	Indonésie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
se	se	k3xPyFc4
pije	pít	k5eAaImIp3nS
káva	káva	k1gFnSc1
ze	z	k7c2
zrnek	zrnko	k1gNnPc2
druhu	druh	k1gInSc2
arabika	arabik	k1gMnSc2
a	a	k8xC
robusta	robust	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Káva	káva	k1gFnSc1
je	být	k5eAaImIp3nS
druhou	druhý	k4xOgFnSc7
nejprodávanější	prodávaný	k2eAgFnSc7d3
komoditou	komodita	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
po	po	k7c6
ropě	ropa	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kávovník	kávovník	k1gInSc1
</s>
<s>
Největší	veliký	k2eAgMnPc1d3
světoví	světový	k2eAgMnPc1d1
producenti	producent	k1gMnPc1
kávy	káva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tmavě	tmavě	k6eAd1
je	být	k5eAaImIp3nS
vyznačena	vyznačen	k2eAgFnSc1d1
robusta	robusta	k1gFnSc1
a	a	k8xC
světle	světle	k6eAd1
arabika	arabika	k1gFnSc1
</s>
<s>
Kávovník	kávovník	k1gInSc1
robusta	robust	k1gMnSc2
</s>
<s>
Kávová	kávový	k2eAgNnPc4d1
semena	semeno	k1gNnPc4
jsou	být	k5eAaImIp3nP
semena	semeno	k1gNnPc1
plodů	plod	k1gInPc2
rostliny	rostlina	k1gFnPc1
zvané	zvaný	k2eAgFnSc2d1
kávovník	kávovník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
ovoce	ovoce	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plod	plod	k1gInSc1
lze	lze	k6eAd1
přirovnat	přirovnat	k5eAaPmF
k	k	k7c3
tvrdé	tvrdý	k2eAgFnSc3d1
třešni	třešeň	k1gFnSc3
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
tvarem	tvar	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
barvou	barva	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
bývá	bývat	k5eAaImIp3nS
v	v	k7c6
době	doba	k1gFnSc6
sklizně	sklizeň	k1gFnSc2
červená	červený	k2eAgFnSc1d1
nebo	nebo	k8xC
červenofialová	červenofialový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k7c2
plodu	plod	k1gInSc2
jsou	být	k5eAaImIp3nP
ukryta	ukryt	k2eAgNnPc1d1
dvě	dva	k4xCgFnPc4
proti	proti	k7c3
sobě	se	k3xPyFc3
položená	položená	k1gFnSc1
semena	semeno	k1gNnSc2
–	–	k?
zelená	zelenat	k5eAaImIp3nS
kávová	kávový	k2eAgNnPc4d1
zrna	zrno	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
rostlina	rostlina	k1gFnSc1
patří	patřit	k5eAaImIp3nS
do	do	k7c2
čeledi	čeleď	k1gFnSc2
mořenovitých	mořenovitý	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
divoce	divoce	k6eAd1
rostoucí	rostoucí	k2eAgInSc1d1
strom	strom	k1gInSc1
se	se	k3xPyFc4
pěstuje	pěstovat	k5eAaImIp3nS
na	na	k7c6
kávovníkových	kávovníkový	k2eAgFnPc6d1
plantážích	plantáž	k1gFnPc6
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
buď	buď	k8xC
jako	jako	k9
strom	strom	k1gInSc1
nebo	nebo	k8xC
keř	keř	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existující	existující	k2eAgInPc1d1
druhy	druh	k1gInPc1
kávovníků	kávovník	k1gInPc2
se	se	k3xPyFc4
mezi	mezi	k7c7
sebou	se	k3xPyFc7
kříží	křížit	k5eAaImIp3nP
a	a	k8xC
šlechtí	šlechtit	k5eAaImIp3nP
se	se	k3xPyFc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
větší	veliký	k2eAgFnSc7d2
odolnosti	odolnost	k1gFnSc6
proti	proti	k7c3
škůdcům	škůdce	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
pokud	pokud	k8xS
napadnou	napadnout	k5eAaPmIp3nP
kávovníky	kávovník	k1gInPc1
na	na	k7c6
plantážích	plantáž	k1gFnPc6
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
způsobit	způsobit	k5eAaPmF
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
škody	škoda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
cizopasná	cizopasný	k2eAgFnSc1d1
houba	houba	k1gFnSc1
dokáže	dokázat	k5eAaPmIp3nS
totálně	totálně	k6eAd1
zničit	zničit	k5eAaPmF
i	i	k9
celé	celý	k2eAgFnPc4d1
plantáže	plantáž	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
příklad	příklad	k1gInSc1
této	tento	k3xDgFnSc2
zkázy	zkáza	k1gFnSc2
můžeme	moct	k5eAaImIp1nP
uvést	uvést	k5eAaPmF
ostrov	ostrov	k1gInSc4
Cejlon	Cejlon	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
musely	muset	k5eAaImAgFnP
být	být	k5eAaImF
kávovníkové	kávovníkový	k2eAgFnPc1d1
plantáže	plantáž	k1gFnPc1
zcela	zcela	k6eAd1
nahrazeny	nahrazen	k2eAgInPc4d1
čajovníky	čajovník	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Pravlastí	pravlast	k1gFnSc7
kávovníku	kávovník	k1gInSc2
je	být	k5eAaImIp3nS
africký	africký	k2eAgInSc1d1
kontinent	kontinent	k1gInSc1
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
Etiopie	Etiopie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zde	zde	k6eAd1
existuje	existovat	k5eAaImIp3nS
oblast	oblast	k1gFnSc4
Kaffa	Kaff	k1gMnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
káva	káva	k1gFnSc1
dodnes	dodnes	k6eAd1
roste	růst	k5eAaImIp3nS
i	i	k9
divoce	divoce	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
historie	historie	k1gFnSc2
byla	být	k5eAaImAgFnS
káva	káva	k1gFnSc1
v	v	k7c6
období	období	k1gNnSc6
13	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
převezena	převézt	k5eAaPmNgFnS
válečníky	válečník	k1gMnPc7
z	z	k7c2
Etiopie	Etiopie	k1gFnSc2
do	do	k7c2
Jemenu	Jemen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
se	se	k3xPyFc4
káva	káva	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
Arábie	Arábie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
kolébku	kolébka	k1gFnSc4
kávy	káva	k1gFnSc2
–	–	k?
nápoje	nápoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc1
pěstování	pěstování	k1gNnSc1
bylo	být	k5eAaImAgNnS
ještě	ještě	k9
před	před	k7c7
150	#num#	k4
lety	let	k1gInPc7
primární	primární	k2eAgFnSc2d1
výsadou	výsada	k1gFnSc7
Arabského	arabský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
a	a	k8xC
vývoz	vývoz	k1gInSc4
kávových	kávový	k2eAgNnPc2d1
zrn	zrno	k1gNnPc2
byl	být	k5eAaImAgInS
přísně	přísně	k6eAd1
zakázán	zakázat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc4d3
podíl	podíl	k1gInSc4
na	na	k7c4
rozšíření	rozšíření	k1gNnSc4
kávy	káva	k1gFnSc2
do	do	k7c2
Evropy	Evropa	k1gFnSc2
mají	mít	k5eAaImIp3nP
především	především	k9
Holanďané	Holanďan	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
tajně	tajně	k6eAd1
převezli	převézt	k5eAaPmAgMnP
kávová	kávový	k2eAgNnPc4d1
zrna	zrno	k1gNnPc4
z	z	k7c2
Jemenu	Jemen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
ovšem	ovšem	k9
kávovníky	kávovník	k1gInPc1
pěstují	pěstovat	k5eAaImIp3nP
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všude	všude	k6eAd1
v	v	k7c6
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
pro	pro	k7c4
jejich	jejich	k3xOp3gInSc4
růst	růst	k1gInSc4
příznivé	příznivý	k2eAgNnSc4d1
podnebí	podnebí	k1gNnSc4
a	a	k8xC
další	další	k2eAgFnPc4d1
ekologické	ekologický	k2eAgFnPc4d1
a	a	k8xC
ekonomické	ekonomický	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kávovníky	kávovník	k1gInPc4
v	v	k7c6
průběhu	průběh	k1gInSc6
svého	svůj	k3xOyFgInSc2
růstu	růst	k1gInSc2
potřebují	potřebovat	k5eAaImIp3nP
dostatek	dostatek	k1gInSc4
vláhy	vláha	k1gFnSc2
i	i	k8xC
slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
nejlépe	dobře	k6eAd3
daří	dařit	k5eAaImIp3nS
ve	v	k7c6
vlhkém	vlhký	k2eAgNnSc6d1
tropickém	tropický	k2eAgNnSc6d1
pásmu	pásmo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každému	každý	k3xTgMnSc3
druhu	druh	k1gInSc2
kávovníku	kávovník	k1gInSc2
ale	ale	k8xC
vyhovuje	vyhovovat	k5eAaImIp3nS
trochu	trochu	k6eAd1
odlišné	odlišný	k2eAgNnSc4d1
podnebí	podnebí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kávovníkové	kávovníkový	k2eAgInPc1d1
stromy	strom	k1gInPc1
nebo	nebo	k8xC
keře	keř	k1gInPc1
se	se	k3xPyFc4
dožívají	dožívat	k5eAaImIp3nP
poměrně	poměrně	k6eAd1
vysokého	vysoký	k2eAgInSc2d1
věku	věk	k1gInSc2
(	(	kIx(
<g/>
až	až	k9
třiceti	třicet	k4xCc2
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ovšem	ovšem	k9
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
už	už	k6eAd1
poskytují	poskytovat	k5eAaImIp3nP
nižší	nízký	k2eAgFnSc4d2
úrodu	úroda	k1gFnSc4
<g/>
;	;	kIx,
nejvyšší	vysoký	k2eAgInPc1d3
výnosy	výnos	k1gInPc1
dávají	dávat	k5eAaImIp3nP
kávovníky	kávovník	k1gInPc4
po	po	k7c6
pěti	pět	k4xCc6
až	až	k8xS
šesti	šest	k4xCc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k8xS
nejvyšším	vysoký	k2eAgInPc3d3
výnosům	výnos	k1gInPc3
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
pomoci	pomoct	k5eAaPmF
správnou	správný	k2eAgFnSc7d1
péčí	péče	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kávovníky	kávovník	k1gInPc7
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
jiné	jiný	k2eAgNnSc1d1
pro	pro	k7c4
nás	my	k3xPp1nPc4
méně	málo	k6eAd2
exotické	exotický	k2eAgFnSc2d1
rostliny	rostlina	k1gFnSc2
<g/>
,	,	kIx,
pravidelně	pravidelně	k6eAd1
přihnojovat	přihnojovat	k5eAaImF
<g/>
,	,	kIx,
prořezávat	prořezávat	k5eAaImF
a	a	k8xC
chránit	chránit	k5eAaImF
nově	nově	k6eAd1
vzrostlé	vzrostlý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
proti	proti	k7c3
prudkému	prudký	k2eAgNnSc3d1
a	a	k8xC
žhnoucímu	žhnoucí	k2eAgNnSc3d1
slunci	slunce	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jednoho	jeden	k4xCgInSc2
keře	keř	k1gInSc2
lze	lze	k6eAd1
získat	získat	k5eAaPmF
až	až	k9
2,5	2,5	k4
kg	kg	kA
zelené	zelený	k2eAgFnSc2d1
kávy	káva	k1gFnSc2
a	a	k8xC
z	z	k7c2
té	ten	k3xDgFnSc2
0,5	0,5	k4
kg	kg	kA
pražené	pražený	k2eAgFnSc2d1
kávy	káva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Existují	existovat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
základní	základní	k2eAgInPc1d1
druhy	druh	k1gInPc1
kávovníků	kávovník	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
odlišují	odlišovat	k5eAaImIp3nP
růstem	růst	k1gInSc7
<g/>
,	,	kIx,
svými	svůj	k3xOyFgInPc7
nároky	nárok	k1gInPc7
na	na	k7c4
pěstování	pěstování	k1gNnSc4
i	i	k8xC
finální	finální	k2eAgFnSc7d1
chutí	chuť	k1gFnSc7
kávy	káva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
kávovník	kávovník	k1gInSc1
roste	růst	k5eAaImIp3nS
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
rovněž	rovněž	k9
vliv	vliv	k1gInSc4
na	na	k7c4
chuť	chuť	k1gFnSc4
kávy	káva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kávovníky	kávovník	k1gInPc1
pěstované	pěstovaný	k2eAgInPc1d1
ve	v	k7c6
vyšších	vysoký	k2eAgFnPc6d2
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
poskytují	poskytovat	k5eAaImIp3nP
kávová	kávový	k2eAgNnPc4d1
zrna	zrno	k1gNnPc4
jemnější	jemný	k2eAgFnSc2d2
chuti	chuť	k1gFnSc2
a	a	k8xC
s	s	k7c7
nižším	nízký	k2eAgInSc7d2
obsahem	obsah	k1gInSc7
kofeinu	kofein	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
českém	český	k2eAgInSc6d1
trhu	trh	k1gInSc6
se	se	k3xPyFc4
prodávají	prodávat	k5eAaImIp3nP
převážně	převážně	k6eAd1
směsi	směs	k1gFnPc1
kávy	káva	k1gFnSc2
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
více	hodně	k6eAd2
základních	základní	k2eAgInPc2d1
druhů	druh	k1gInPc2
zrn	zrno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
správnou	správný	k2eAgFnSc7d1
kombinací	kombinace	k1gFnSc7
vhodně	vhodně	k6eAd1
zvolených	zvolený	k2eAgInPc2d1
druhů	druh	k1gInPc2
se	se	k3xPyFc4
vytvoří	vytvořit	k5eAaPmIp3nS
směs	směs	k1gFnSc1
s	s	k7c7
harmonickou	harmonický	k2eAgFnSc7d1
a	a	k8xC
ucelenou	ucelený	k2eAgFnSc7d1
chutí	chuť	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tzv.	tzv.	kA
„	„	k?
<g/>
čisté	čistý	k2eAgInPc1d1
druhy	druh	k1gInPc1
<g/>
“	“	k?
jsou	být	k5eAaImIp3nP
k	k	k7c3
dostání	dostání	k1gNnSc3
spíše	spíše	k9
ve	v	k7c6
specializovaných	specializovaný	k2eAgInPc6d1
obchodech	obchod	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směsi	směs	k1gFnSc6
se	se	k3xPyFc4
většinou	většinou	k6eAd1
připravují	připravovat	k5eAaImIp3nP
ze	z	k7c2
zelených	zelená	k1gFnPc2
zrn	zrno	k1gNnPc2
a	a	k8xC
teprve	teprve	k6eAd1
pak	pak	k6eAd1
se	se	k3xPyFc4
dohromady	dohromady	k6eAd1
praží	pražit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
výběru	výběr	k1gInSc6
směsi	směs	k1gFnSc2
jsou	být	k5eAaImIp3nP
rozhodující	rozhodující	k2eAgFnPc4d1
nejen	nejen	k6eAd1
požadované	požadovaný	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
cena	cena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Aby	aby	k9
výsledná	výsledný	k2eAgFnSc1d1
káva	káva	k1gFnSc1
byla	být	k5eAaImAgFnS
co	co	k9
nejlepší	dobrý	k2eAgFnPc4d3
chuti	chuť	k1gFnPc4
<g/>
,	,	kIx,
záleží	záležet	k5eAaImIp3nS
nejen	nejen	k6eAd1
na	na	k7c6
odrůdě	odrůda	k1gFnSc6
pěstovaných	pěstovaný	k2eAgInPc2d1
kávovníků	kávovník	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
především	především	k9
na	na	k7c6
sklizni	sklizeň	k1gFnSc6
plodů	plod	k1gInPc2
<g/>
,	,	kIx,
následném	následný	k2eAgNnSc6d1
zpracování	zpracování	k1gNnSc6
kávových	kávový	k2eAgNnPc2d1
zrn	zrno	k1gNnPc2
<g/>
,	,	kIx,
na	na	k7c6
správném	správný	k2eAgNnSc6d1
pražení	pražení	k1gNnSc6
<g/>
,	,	kIx,
uskladnění	uskladnění	k1gNnSc6
a	a	k8xC
expedování	expedování	k1gNnSc6
kávových	kávový	k2eAgInPc2d1
žoků	žok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
z	z	k7c2
těchto	tento	k3xDgFnPc2
dílčích	dílčí	k2eAgFnPc2d1
prací	práce	k1gFnPc2
má	mít	k5eAaImIp3nS
pevná	pevný	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
a	a	k8xC
jejich	jejich	k3xOp3gNnPc2
nedodržení	nedodržení	k1gNnPc2
se	se	k3xPyFc4
vždy	vždy	k6eAd1
projeví	projevit	k5eAaPmIp3nS
negativně	negativně	k6eAd1
na	na	k7c6
výsledné	výsledný	k2eAgFnSc6d1
chuti	chuť	k1gFnSc6
kávy	káva	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odrůdy	odrůda	k1gFnPc1
kávovníků	kávovník	k1gInPc2
</s>
<s>
Plody	plod	k1gInPc1
kávovníku	kávovník	k1gInSc2
arabika	arabika	k1gFnSc1
</s>
<s>
Kávová	kávový	k2eAgNnPc4d1
zrna	zrno	k1gNnPc4
robusty	robusta	k1gFnSc2
po	po	k7c6
upražení	upražení	k1gNnSc6
</s>
<s>
Káva	káva	k1gFnSc1
se	se	k3xPyFc4
často	často	k6eAd1
připravuje	připravovat	k5eAaImIp3nS
jako	jako	k9
směs	směs	k1gFnSc1
z	z	k7c2
různých	různý	k2eAgInPc2d1
druhů	druh	k1gInPc2
kávovníku	kávovník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
se	se	k3xPyFc4
setkáváme	setkávat	k5eAaImIp1nP
s	s	k7c7
druhem	druh	k1gInSc7
kávovník	kávovník	k1gInSc1
robusta	robusta	k1gFnSc1
(	(	kIx(
<g/>
Canephora	Canephora	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
kávovník	kávovník	k1gInSc1
arabika	arabikum	k1gNnSc2
(	(	kIx(
<g/>
Arabica	Arabica	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
méně	málo	k6eAd2
používané	používaný	k2eAgInPc4d1
kávovník	kávovník	k1gInSc4
liberijský	liberijský	k2eAgInSc4d1
(	(	kIx(
<g/>
Liberica	Liberica	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
kávovník	kávovník	k1gInSc1
šari	šari	k1gNnSc2
(	(	kIx(
<g/>
Excelsa	Excelsa	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
na	na	k7c6
trhu	trh	k1gInSc6
vyskytují	vyskytovat	k5eAaImIp3nP
zřídka	zřídka	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arabika	Arabika	k1gFnSc1
má	mít	k5eAaImIp3nS
podíl	podíl	k1gInSc4
na	na	k7c6
světové	světový	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
asi	asi	k9
70	#num#	k4
%	%	kIx~
a	a	k8xC
robusta	robusta	k1gFnSc1
asi	asi	k9
25	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Arabský	arabský	k2eAgInSc1d1
kávovník	kávovník	k1gInSc1
roste	růst	k5eAaImIp3nS
ve	v	k7c6
větších	veliký	k2eAgFnPc6d2
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
(	(	kIx(
<g/>
900	#num#	k4
<g/>
–	–	k?
<g/>
2800	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
často	často	k6eAd1
na	na	k7c6
sopečné	sopečný	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
<g/>
,	,	kIx,
či	či	k8xC
jako	jako	k9
lesní	lesní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
(	(	kIx(
<g/>
např.	např.	kA
etiopská	etiopský	k2eAgFnSc1d1
káva	káva	k1gFnSc1
Harar	Harar	k1gMnSc1
či	či	k8xC
Wild	Wild	k1gMnSc1
Forest	Forest	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kávovníky	kávovník	k1gInPc1
rostoucí	rostoucí	k2eAgInPc1d1
v	v	k7c6
těchto	tento	k3xDgFnPc6
výškách	výška	k1gFnPc6
jsou	být	k5eAaImIp3nP
méně	málo	k6eAd2
odolné	odolný	k2eAgFnPc1d1
proti	proti	k7c3
škůdcům	škůdce	k1gMnPc3
a	a	k8xC
chorobám	choroba	k1gFnPc3
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
také	také	k9
velmi	velmi	k6eAd1
choulostivé	choulostivý	k2eAgNnSc1d1
na	na	k7c4
mrazíky	mrazík	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhovují	vyhovovat	k5eAaImIp3nP
mu	on	k3xPp3gNnSc3
mírnější	mírný	k2eAgFnPc1d2
teploty	teplota	k1gFnPc1
nepřesahující	přesahující	k2eNgFnSc2d1
24	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Zrna	zrno	k1gNnPc4
jsou	být	k5eAaImIp3nP
větší	veliký	k2eAgInPc1d2
než	než	k8xS
u	u	k7c2
robusty	robusta	k1gFnSc2
a	a	k8xC
během	během	k7c2
pražení	pražení	k1gNnSc2
i	i	k9
při	při	k7c6
samotné	samotný	k2eAgFnSc6d1
přípravě	příprava	k1gFnSc6
nápoje	nápoj	k1gInPc1
jsou	být	k5eAaImIp3nP
velice	velice	k6eAd1
citlivá	citlivý	k2eAgNnPc1d1
na	na	k7c4
teplotu	teplota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Káva	káva	k1gFnSc1
z	z	k7c2
tohoto	tento	k3xDgInSc2
druhu	druh	k1gInSc2
má	mít	k5eAaImIp3nS
nižší	nízký	k2eAgInSc4d2
obsah	obsah	k1gInSc4
kofeinu	kofein	k1gInSc2
(	(	kIx(
<g/>
běžně	běžně	k6eAd1
mezi	mezi	k7c7
1	#num#	k4
<g/>
-	-	kIx~
<g/>
1,3	1,3	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
její	její	k3xOp3gFnSc1
chuť	chuť	k1gFnSc1
je	být	k5eAaImIp3nS
jemná	jemný	k2eAgFnSc1d1
a	a	k8xC
nasládle	nasládle	k6eAd1
lahodná	lahodný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
až	až	k9
70	#num#	k4
%	%	kIx~
z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
kávy	káva	k1gFnSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc1
zpracování	zpracování	k1gNnSc1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
náročné	náročný	k2eAgNnSc1d1
<g/>
,	,	kIx,
zrna	zrno	k1gNnPc1
se	se	k3xPyFc4
zpracovávají	zpracovávat	k5eAaImIp3nP
takzvanou	takzvaný	k2eAgFnSc7d1
„	„	k?
<g/>
mokrou	mokrý	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
káva	káva	k1gFnSc1
projde	projít	k5eAaPmIp3nS
procesem	proces	k1gInSc7
fermentace	fermentace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
zpracování	zpracování	k1gNnSc2
je	být	k5eAaImIp3nS
finančně	finančně	k6eAd1
náročnější	náročný	k2eAgNnSc1d2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
také	také	k9
výsledná	výsledný	k2eAgFnSc1d1
cena	cena	k1gFnSc1
této	tento	k3xDgFnSc2
kávy	káva	k1gFnSc2
vyšší	vysoký	k2eAgInPc1d2
než	než	k8xS
u	u	k7c2
ostatních	ostatní	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Druhým	druhý	k4xOgInSc7
významným	významný	k2eAgInSc7d1
kávovníkem	kávovník	k1gInSc7
je	být	k5eAaImIp3nS
robusta	robusta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyznačuje	vyznačovat	k5eAaImIp3nS
se	s	k7c7
tmavší	tmavý	k2eAgFnSc7d2
barvou	barva	k1gFnSc7
<g/>
,	,	kIx,
výraznější	výrazný	k2eAgFnSc1d2
<g/>
,	,	kIx,
zemitější	zemitý	k2eAgFnSc1d2
(	(	kIx(
<g/>
drsnější	drsný	k2eAgFnSc1d2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
méně	málo	k6eAd2
vytříbenou	vytříbený	k2eAgFnSc7d1
chutí	chuť	k1gFnSc7
než	než	k8xS
arabský	arabský	k2eAgInSc1d1
kávovník	kávovník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrnka	zrnko	k1gNnPc1
jsou	být	k5eAaImIp3nP
menší	malý	k2eAgNnPc1d2
<g/>
,	,	kIx,
obsahují	obsahovat	k5eAaImIp3nP
cca	cca	kA
2	#num#	k4
<g/>
x	x	k?
více	hodně	k6eAd2
kofeinu	kofein	k1gInSc2
(	(	kIx(
<g/>
asi	asi	k9
2	#num#	k4
<g/>
-	-	kIx~
<g/>
2,6	2,6	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robusta	Robusta	k1gMnSc1
není	být	k5eNaImIp3nS
tolik	tolik	k6eAd1
náročná	náročný	k2eAgFnSc1d1
na	na	k7c4
pěstování	pěstování	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
odolnější	odolný	k2eAgMnSc1d2
proti	proti	k7c3
škůdcům	škůdce	k1gMnPc3
a	a	k8xC
poskytuje	poskytovat	k5eAaImIp3nS
vyšší	vysoký	k2eAgInPc4d2
výnosy	výnos	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomuto	tento	k3xDgInSc3
kávovníku	kávovník	k1gInSc3
vyhovuje	vyhovovat	k5eAaImIp3nS
stálejší	stálý	k2eAgNnSc1d2
počasí	počasí	k1gNnSc1
s	s	k7c7
vyššími	vysoký	k2eAgFnPc7d2
teplotami	teplota	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlépe	dobře	k6eAd3
snáší	snášet	k5eAaImIp3nS
teploty	teplota	k1gFnPc4
kolem	kolem	k7c2
30	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Kávová	kávový	k2eAgNnPc1d1
zrna	zrno	k1gNnPc1
jsou	být	k5eAaImIp3nP
zpracována	zpracován	k2eAgNnPc1d1
tzv.	tzv.	kA
„	„	k?
<g/>
suchou	suchý	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
není	být	k5eNaImIp3nS
tak	tak	k6eAd1
finančně	finančně	k6eAd1
náročná	náročný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
je	být	k5eAaImIp3nS
cena	cena	k1gFnSc1
výrazně	výrazně	k6eAd1
nižší	nízký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
především	především	k9
do	do	k7c2
směsí	směs	k1gFnPc2
s	s	k7c7
ostatními	ostatní	k2eAgInPc7d1
druhy	druh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
směsi	směs	k1gFnSc6
s	s	k7c7
arabikou	arabika	k1gFnSc7
snižuje	snižovat	k5eAaImIp3nS
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Nejméně	málo	k6eAd3
významným	významný	k2eAgMnSc7d1
druhem	druh	k1gMnSc7
kávovníku	kávovník	k1gInSc2
je	být	k5eAaImIp3nS
Coffea	Coffe	k2eAgFnSc1d1
liberica	liberica	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzrůstem	vzrůst	k1gInSc7
je	být	k5eAaImIp3nS
ze	z	k7c2
všech	všecek	k3xTgInPc2
druhů	druh	k1gInPc2
kávovníku	kávovník	k1gInSc2
nejvyšší	vysoký	k2eAgFnSc4d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dorůstá	dorůstat	k5eAaImIp3nS
až	až	k9
18	#num#	k4
metrů	metr	k1gInPc2
(	(	kIx(
<g/>
arabika	arabika	k1gFnSc1
8	#num#	k4
m	m	kA
<g/>
,	,	kIx,
robusta	robusta	k1gFnSc1
10	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
silný	silný	k2eAgInSc1d1
a	a	k8xC
plody	plod	k1gInPc1
mají	mít	k5eAaImIp3nP
i	i	k9
velká	velký	k2eAgNnPc4d1
zrna	zrno	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
hořké	hořká	k1gFnPc4
chuti	chuť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberica	Liberic	k1gInSc2
sice	sice	k8xC
poskytuje	poskytovat	k5eAaImIp3nS
velké	velký	k2eAgInPc4d1
výnosy	výnos	k1gInPc4
<g/>
,	,	kIx,
ovšem	ovšem	k9
kvalita	kvalita	k1gFnSc1
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
pouze	pouze	k6eAd1
průměrnou	průměrný	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Příprava	příprava	k1gFnSc1
kávy	káva	k1gFnSc2
</s>
<s>
Espresso	Espressa	k1gFnSc5
</s>
<s>
Příprava	příprava	k1gFnSc1
kávy	káva	k1gFnSc2
má	mít	k5eAaImIp3nS
mnoho	mnoho	k4c1
způsobů	způsob	k1gInPc2
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
<g/>
:	:	kIx,
filtrovaná	filtrovaný	k2eAgFnSc1d1
káva	káva	k1gFnSc1
(	(	kIx(
<g/>
drip	drip	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
espresso	espressa	k1gFnSc5
<g/>
,	,	kIx,
vacuum	vacuum	k1gInSc1
pot	pot	k1gInSc1
<g/>
,	,	kIx,
Aeropress	Aeropress	k1gInSc1
<g/>
,	,	kIx,
cafeteria	cafeterium	k1gNnPc1
(	(	kIx(
<g/>
french	french	k1gInSc1
press	press	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mokka	mokka	k1gNnSc1
press	pressa	k1gFnPc2
<g/>
,	,	kIx,
instantní	instantní	k2eAgFnSc1d1
káva	káva	k1gFnSc1
<g/>
,	,	kIx,
turecká	turecký	k2eAgNnPc1d1
mokka	mokka	k1gNnPc1
<g/>
,	,	kIx,
v	v	k7c6
Česku	Česko	k1gNnSc6
a	a	k8xC
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
také	také	k9
tzv.	tzv.	kA
turecká	turecký	k2eAgFnSc1d1
káva	káva	k1gFnSc1
(	(	kIx(
<g/>
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
však	však	k9
liší	lišit	k5eAaImIp3nP
od	od	k7c2
kávy	káva	k1gFnSc2
připravované	připravovaný	k2eAgFnSc2d1
v	v	k7c6
Turecku	Turecko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Espresso	Espressa	k1gFnSc5
</s>
<s>
Espresso	Espressa	k1gFnSc5
je	být	k5eAaImIp3nS
způsob	způsob	k1gInSc1
výroby	výroba	k1gFnSc2
kávy	káva	k1gFnSc2
italského	italský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
je	být	k5eAaImIp3nS
malé	malý	k2eAgNnSc1d1
množství	množství	k1gNnSc1
téměř	téměř	k6eAd1
vroucí	vroucí	k2eAgFnPc4d1
vody	voda	k1gFnPc4
vytlačeno	vytlačen	k2eAgNnSc1d1
pod	pod	k7c7
tlakem	tlak	k1gInSc7
jemně	jemně	k6eAd1
mletými	mletý	k2eAgFnPc7d1
kávovými	kávový	k2eAgNnPc7d1
zrny	zrno	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výraz	výraz	k1gInSc1
espresso	espressa	k1gFnSc5
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
italštiny	italština	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
příčestím	příčestí	k1gNnSc7
inf.	inf.	k?
esprimere	esprimrat	k5eAaPmIp3nS
–	–	k?
„	„	k?
<g/>
vytlačit	vytlačit	k5eAaPmF
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
též	též	k9
„	„	k?
<g/>
vyjádřit	vyjádřit	k5eAaPmF
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Espresso	Espressa	k1gFnSc5
je	být	k5eAaImIp3nS
také	také	k9
základem	základ	k1gInSc7
pro	pro	k7c4
jiné	jiný	k2eAgInPc4d1
druhy	druh	k1gInPc4
kávy	káva	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
caffè	caffè	k?
latte	latte	k5eAaPmIp2nP
<g/>
,	,	kIx,
cappuccino	cappuccina	k1gFnSc5
<g/>
,	,	kIx,
lungo	lunga	k1gFnSc5
<g/>
,	,	kIx,
americano	americana	k1gFnSc5
nebo	nebo	k8xC
flat	flat	k1gInSc4
white	white	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Existují	existovat	k5eAaImIp3nP
i	i	k9
alternativy	alternativa	k1gFnPc1
k	k	k7c3
espressu	espress	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
každou	každý	k3xTgFnSc4
jednotlivou	jednotlivý	k2eAgFnSc4d1
přípravu	příprava	k1gFnSc4
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
především	především	k6eAd1
použít	použít	k5eAaPmF
kvalitní	kvalitní	k2eAgFnSc4d1
čerstvě	čerstvě	k6eAd1
praženou	pražený	k2eAgFnSc4d1
kávu	káva	k1gFnSc4
a	a	k8xC
čistou	čistý	k2eAgFnSc4d1
vodu	voda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
důležité	důležitý	k2eAgNnSc1d1
dodržet	dodržet	k5eAaPmF
správný	správný	k2eAgInSc4d1
postup	postup	k1gInSc4
přípravy	příprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
rozhodujících	rozhodující	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
je	být	k5eAaImIp3nS
správné	správný	k2eAgNnSc4d1
mletí	mletí	k1gNnSc4
kávy	káva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
doporučuje	doporučovat	k5eAaImIp3nS
především	především	k9
pro	pro	k7c4
přípravy	příprava	k1gFnPc4
filtrovaných	filtrovaný	k2eAgFnPc2d1
káv	káva	k1gFnPc2
čerstvě	čerstvě	k6eAd1
a	a	k8xC
světle	světle	k6eAd1
praženou	pražený	k2eAgFnSc4d1
kávu	káva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1
káva	káva	k1gFnSc1
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1
káva	káva	k1gFnSc1
<g/>
,	,	kIx,
tzv.	tzv.	kA
„	„	k?
<g/>
turek	turek	k1gInSc1
<g/>
“	“	k?
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
slitím	slití	k1gNnSc7
namleté	namletý	k2eAgFnSc2d1
kávy	káva	k1gFnSc2
v	v	k7c6
šálku	šálek	k1gInSc6
s	s	k7c7
vodou	voda	k1gFnSc7
těsně	těsně	k6eAd1
pod	pod	k7c7
bodem	bod	k1gInSc7
varu	var	k1gInSc2
(	(	kIx(
<g/>
nejlépe	dobře	k6eAd3
98	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
příprava	příprava	k1gFnSc1
kávy	káva	k1gFnSc2
je	být	k5eAaImIp3nS
populární	populární	k2eAgFnSc7d1
a	a	k8xC
tradiční	tradiční	k2eAgFnSc7d1
v	v	k7c6
Česku	Česko	k1gNnSc6
a	a	k8xC
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
přípravy	příprava	k1gFnSc2
kávy	káva	k1gFnSc2
není	být	k5eNaImIp3nS
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
obvyklý	obvyklý	k2eAgInSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
své	svůj	k3xOyFgFnPc4
nevýhody	nevýhoda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvních	první	k4xOgFnPc6
minutách	minuta	k1gFnPc6
louhování	louhování	k1gNnSc1
se	se	k3xPyFc4
do	do	k7c2
nápoje	nápoj	k1gInSc2
uvolňují	uvolňovat	k5eAaImIp3nP
jen	jen	k9
chuťové	chuťový	k2eAgFnPc1d1
a	a	k8xC
aromatické	aromatický	k2eAgFnPc1d1
složky	složka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prudké	Prudké	k2eAgNnSc2d1
spaření	spaření	k1gNnSc2
kávy	káva	k1gFnSc2
a	a	k8xC
delší	dlouhý	k2eAgNnSc4d2
louhování	louhování	k1gNnSc4
je	být	k5eAaImIp3nS
ale	ale	k9
velmi	velmi	k6eAd1
nešetrné	šetrný	k2eNgNnSc1d1
a	a	k8xC
uvolňuje	uvolňovat	k5eAaImIp3nS
třísloviny	tříslovina	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
mají	mít	k5eAaImIp3nP
za	za	k7c4
následek	následek	k1gInSc4
nakyslou	nakyslý	k2eAgFnSc4d1
až	až	k9
řezavou	řezavý	k2eAgFnSc4d1
chuť	chuť	k1gFnSc4
kávy	káva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Turecká	turecký	k2eAgNnPc1d1
mokka	mokka	k1gNnPc1
</s>
<s>
Turecká	turecký	k2eAgNnPc1d1
mokka	mokka	k1gNnPc1
</s>
<s>
Turecká	turecký	k2eAgNnPc4d1
mokka	mokka	k1gNnPc4
je	být	k5eAaImIp3nS
tradiční	tradiční	k2eAgInSc1d1
způsob	způsob	k1gInSc1
pití	pití	k1gNnSc2
kávy	káva	k1gFnSc2
ve	v	k7c6
Středomoří	středomoří	k1gNnSc6
<g/>
,	,	kIx,
především	především	k6eAd1
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
,	,	kIx,
Turecku	Turecko	k1gNnSc6
a	a	k8xC
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
přípravu	příprava	k1gFnSc4
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
kovová	kovový	k2eAgFnSc1d1
konvička	konvička	k1gFnSc1
zvaná	zvaný	k2eAgFnSc1d1
„	„	k?
<g/>
džezva	džezva	k1gFnSc1
<g/>
,	,	kIx,
ibrik	ibrik	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turecká	turecký	k2eAgNnPc4d1
mokka	mokka	k1gNnPc4
je	být	k5eAaImIp3nS
všeobecně	všeobecně	k6eAd1
asi	asi	k9
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejsilnějších	silný	k2eAgFnPc2d3
káv	káva	k1gFnPc2
co	co	k8xS
se	se	k3xPyFc4
obsahu	obsah	k1gInSc2
kofeinu	kofein	k1gInSc2
týče	týkat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
je	být	k5eAaImIp3nS
silnější	silný	k2eAgMnSc1d2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
její	její	k3xOp3gFnSc1
chuť	chuť	k1gFnSc1
kultivovanější	kultivovaný	k2eAgFnSc1d2
než	než	k8xS
u	u	k7c2
„	„	k?
<g/>
českého	český	k2eAgInSc2d1
turka	turek	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Pravá	pravý	k2eAgFnSc1d1
turecká	turecký	k2eAgFnSc1d1
káva	káva	k1gFnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
většině	většina	k1gFnSc6
případů	případ	k1gInPc2
připravována	připravovat	k5eAaImNgFnS
rovnou	rovnou	k6eAd1
s	s	k7c7
cukrem	cukr	k1gInSc7
a	a	k8xC
kořením	koření	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
skořice	skořice	k1gFnSc1
<g/>
,	,	kIx,
vanilka	vanilka	k1gFnSc1
a	a	k8xC
kardamom	kardamom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Káva	káva	k1gFnSc1
bez	bez	k7c2
těchto	tento	k3xDgFnPc2
uvedených	uvedený	k2eAgFnPc2d1
ingrediencí	ingredience	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
cukru	cukr	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
pije	pít	k5eAaImIp3nS
jen	jen	k9
při	při	k7c6
smutečních	smuteční	k2eAgFnPc6d1
hostinách	hostina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
džezvy	džezva	k1gFnSc2
nasypeme	nasypat	k5eAaPmIp1nP
velmi	velmi	k6eAd1
jemně	jemně	k6eAd1
semletou	semletý	k2eAgFnSc4d1
kávu	káva	k1gFnSc4
s	s	k7c7
kořením	koření	k1gNnSc7
a	a	k8xC
cukrem	cukr	k1gInSc7
v	v	k7c6
poměru	poměr	k1gInSc6
cca	cca	kA
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
zalijeme	zalít	k5eAaPmIp1nP
studenou	studený	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
(	(	kIx(
<g/>
jen	jen	k9
ve	v	k7c6
výjimečných	výjimečný	k2eAgInPc6d1
případech	případ	k1gInPc6
mírně	mírně	k6eAd1
předehřátou	předehřátý	k2eAgFnSc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
postavíme	postavit	k5eAaPmIp1nP
na	na	k7c4
tepelný	tepelný	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
a	a	k8xC
čekáme	čekat	k5eAaImIp1nP
kdy	kdy	k6eAd1
káva	káva	k1gFnSc1
poprvé	poprvé	k6eAd1
vzkypí	vzkypět	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
obsah	obsah	k1gInSc1
díky	díky	k7c3
pomalému	pomalý	k2eAgInSc3d1
ohřevu	ohřev	k1gInSc3
uzavře	uzavřít	k5eAaPmIp3nS
zúženou	zúžený	k2eAgFnSc4d1
část	část	k1gFnSc4
<g/>
,	,	kIx,
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
nejdůležitější	důležitý	k2eAgFnSc3d3
fázi	fáze	k1gFnSc3
přípravy	příprava	k1gFnSc2
<g/>
,	,	kIx,
pomalé	pomalý	k2eAgNnSc1d1
propařování	propařování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
káva	káva	k1gFnSc1
vzkypí	vzkypět	k5eAaPmIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
už	už	k6eAd1
by	by	kYmCp3nS
z	z	k7c2
džezvy	džezva	k1gFnSc2
přetekla	přetéct	k5eAaPmAgFnS
<g/>
,	,	kIx,
odstavíme	odstavit	k5eAaPmIp1nP
ji	on	k3xPp3gFnSc4
na	na	k7c4
asi	asi	k9
půl	půl	k6eAd1
minuty	minuta	k1gFnPc4
a	a	k8xC
celý	celý	k2eAgInSc4d1
proces	proces	k1gInSc4
opakujme	opakovat	k5eAaImRp1nP
dle	dle	k7c2
chuti	chuť	k1gFnSc2
a	a	k8xC
potřeby	potřeba	k1gFnSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
celkem	celkem	k6eAd1
čtyřikrát	čtyřikrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
žádném	žádný	k3yNgInSc6
případě	případ	k1gInSc6
nápoj	nápoj	k1gInSc4
nepřivedeme	přivést	k5eNaPmIp1nP
k	k	k7c3
varu	var	k1gInSc3
<g/>
,	,	kIx,
káva	káva	k1gFnSc1
připravená	připravený	k2eAgFnSc1d1
touto	tento	k3xDgFnSc7
cestou	cesta	k1gFnSc7
zdaleka	zdaleka	k6eAd1
nedosahuje	dosahovat	k5eNaImIp3nS
chuťových	chuťový	k2eAgFnPc2d1
kvalit	kvalita	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
sebou	se	k3xPyFc7
už	už	k6eAd1
jako	jako	k8xS,k8xC
surovina	surovina	k1gFnSc1
nese	nést	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
vařící	vařící	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
a	a	k8xC
výlučně	výlučně	k6eAd1
<g/>
,	,	kIx,
pravý	pravý	k2eAgInSc4d1
český	český	k2eAgInSc4d1
turek	turek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Díky	díky	k7c3
velmi	velmi	k6eAd1
jemnému	jemný	k2eAgNnSc3d1
mletí	mletí	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
tento	tento	k3xDgInSc4
způsob	způsob	k1gInSc4
přípravy	příprava	k1gFnSc2
kávy	káva	k1gFnSc2
nutné	nutný	k2eAgFnPc4d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
drží	držet	k5eAaImIp3nS
sedlina	sedlina	k1gFnSc1
na	na	k7c6
dně	dno	k1gNnSc6
džezvy	džezva	k1gFnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
není	být	k5eNaImIp3nS
třeba	třeba	k6eAd1
kávu	káva	k1gFnSc4
filtrovat	filtrovat	k5eAaImF
a	a	k8xC
můžeme	moct	k5eAaImIp1nP
jí	on	k3xPp3gFnSc7
pohodlně	pohodlně	k6eAd1
servírovat	servírovat	k5eAaBmF
přímo	přímo	k6eAd1
do	do	k7c2
šálků	šálek	k1gInPc2
bez	bez	k7c2
plovoucích	plovoucí	k2eAgNnPc2d1
zrnek	zrnko	k1gNnPc2
na	na	k7c6
hladině	hladina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nápoj	nápoj	k1gInSc1
je	být	k5eAaImIp3nS
díky	díky	k7c3
této	tento	k3xDgFnSc3
přípravě	příprava	k1gFnSc3
také	také	k6eAd1
vydatnější	vydatný	k2eAgFnSc1d2
a	a	k8xC
podává	podávat	k5eAaImIp3nS
se	se	k3xPyFc4
bez	bez	k7c2
mléka	mléko	k1gNnSc2
<g/>
,	,	kIx,
jen	jen	k9
se	s	k7c7
sklenicí	sklenice	k1gFnSc7
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInPc1d1
způsoby	způsob	k1gInPc1
</s>
<s>
Může	moct	k5eAaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
filtrovanou	filtrovaný	k2eAgFnSc4d1
kávu	káva	k1gFnSc4
<g/>
,	,	kIx,
označovanou	označovaný	k2eAgFnSc4d1
i	i	k8xC
jako	jako	k9
drip	drip	k1gInSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
o	o	k7c4
přípravu	příprava	k1gFnSc4
kávy	káva	k1gFnSc2
v	v	k7c6
kafetieru	kafetiero	k1gNnSc6
(	(	kIx(
<g/>
cafetier	cafetier	k1gMnSc1
<g/>
,	,	kIx,
french	french	k1gMnSc1
press	press	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aeropress	Aeropressa	k1gFnPc2
je	být	k5eAaImIp3nS
příprava	příprava	k1gFnSc1
kávy	káva	k1gFnSc2
v	v	k7c6
přístroji	přístroj	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
káva	káva	k1gFnSc1
stlačována	stlačovat	k5eAaImNgFnS
vzduchovým	vzduchový	k2eAgInSc7d1
válcem	válec	k1gInSc7
přes	přes	k7c4
filtrační	filtrační	k2eAgInSc4d1
papír	papír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metoda	metoda	k1gFnSc1
a	a	k8xC
přístroj	přístroj	k1gInSc4
byla	být	k5eAaImAgFnS
vynalezena	vynaleznout	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vacuum	Vacuum	k1gInSc1
pot	pot	k1gInSc4
(	(	kIx(
<g/>
kávový	kávový	k2eAgInSc4d1
sifon	sifon	k1gInSc4
<g/>
)	)	kIx)
–	–	k?
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
přípravu	příprava	k1gFnSc4
filtrované	filtrovaný	k2eAgFnSc2d1
kávy	káva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Káva	káva	k1gFnSc1
je	být	k5eAaImIp3nS
díky	díky	k7c3
přetlaku	přetlak	k1gInSc3
vodní	vodní	k2eAgFnSc2d1
páry	pára	k1gFnSc2
vytlačena	vytlačit	k5eAaPmNgFnS
přes	přes	k7c4
filtr	filtr	k1gInSc4
do	do	k7c2
skleněné	skleněný	k2eAgFnSc2d1
nádoby	nádoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Moka	moka	k1gNnSc1
<g/>
,	,	kIx,
moka	moka	k1gNnSc1
press	pressa	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
způsob	způsob	k1gInSc1
přípravy	příprava	k1gFnSc2
v	v	k7c6
kovové	kovový	k2eAgFnSc6d1
konvičce	konvička	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
typy	typ	k1gInPc1
konviček	konvička	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
kombinací	kombinace	k1gFnSc7
kovu	kov	k1gInSc2
a	a	k8xC
porcelánu	porcelán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Principem	princip	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
voda	voda	k1gFnSc1
se	se	k3xPyFc4
zahřívá	zahřívat	k5eAaImIp3nS
až	až	k6eAd1
se	se	k3xPyFc4
změní	změnit	k5eAaPmIp3nS
v	v	k7c4
páru	pára	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
ze	z	k7c2
spodní	spodní	k2eAgFnSc2d1
části	část	k1gFnSc2
probublá	probublat	k5eAaPmIp3nS
přes	přes	k7c4
kovové	kovový	k2eAgNnSc4d1
sítko	sítko	k1gNnSc4
naplněné	naplněný	k2eAgNnSc4d1
kávou	káva	k1gFnSc7
do	do	k7c2
vrchní	vrchní	k2eAgFnSc2d1
nádobky	nádobka	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
přímo	přímo	k6eAd1
do	do	k7c2
šálků	šálek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Existují	existovat	k5eAaImIp3nP
ale	ale	k9
i	i	k9
tradiční	tradiční	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
kávy	káva	k1gFnSc2
často	často	k6eAd1
spojené	spojený	k2eAgNnSc1d1
s	s	k7c7
místní	místní	k2eAgFnSc7d1
kulturou	kultura	k1gFnSc7
jednotlivých	jednotlivý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
příprava	příprava	k1gFnSc1
kávy	káva	k1gFnSc2
ve	v	k7c6
Vietnamu	Vietnam	k1gInSc6
nebo	nebo	k8xC
etiopský	etiopský	k2eAgInSc4d1
kávový	kávový	k2eAgInSc4d1
obřad	obřad	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
káva	káva	k1gFnSc1
sváží	svážit	k5eAaPmIp3nS,k5eAaImIp3nS
s	s	k7c7
keramické	keramický	k2eAgFnSc6d1
konvičce	konvička	k1gFnSc6
tzv.	tzv.	kA
jebena	jeben	k2eAgFnSc1d1
(	(	kIx(
<g/>
džebena	džeben	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Arabský	arabský	k2eAgInSc4d1
rituál	rituál	k1gInSc4
pití	pití	k1gNnSc2
kávy	káva	k1gFnSc2
</s>
<s>
U	u	k7c2
nás	my	k3xPp1nPc2
je	být	k5eAaImIp3nS
znám	znám	k2eAgMnSc1d1
především	především	k6eAd1
italský	italský	k2eAgMnSc1d1
„	„	k?
<g/>
rituál	rituál	k1gInSc1
<g/>
“	“	k?
pití	pití	k1gNnSc2
kávy	káva	k1gFnSc2
<g/>
;	;	kIx,
starší	starý	k2eAgFnPc1d2
a	a	k8xC
trochu	trochu	k6eAd1
složitější	složitý	k2eAgMnSc1d2
je	být	k5eAaImIp3nS
rituál	rituál	k1gInSc1
arabský	arabský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabídnutou	nabídnutý	k2eAgFnSc4d1
kávu	káva	k1gFnSc4
prakticky	prakticky	k6eAd1
nelze	lze	k6eNd1
odmítnout	odmítnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kávu	káva	k1gFnSc4
obvykle	obvykle	k6eAd1
roznáší	roznášet	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
obdobou	obdoba	k1gFnSc7
„	„	k?
<g/>
našeho	náš	k3xOp1gInSc2
<g/>
“	“	k?
číšníka	číšník	k1gMnSc4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
tácek	tácek	k1gInSc4
a	a	k8xC
na	na	k7c6
něm	on	k3xPp3gInSc6
šálky	šálka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabídne	nabídnout	k5eAaPmIp3nS
kávu	káva	k1gFnSc4
<g/>
,	,	kIx,
tu	ten	k3xDgFnSc4
host	host	k1gMnSc1
vypije	vypít	k5eAaPmIp3nS
a	a	k8xC
pokud	pokud	k8xS
chce	chtít	k5eAaImIp3nS
další	další	k2eAgNnSc1d1
<g/>
,	,	kIx,
položí	položit	k5eAaPmIp3nS
šálek	šálek	k1gInSc1
na	na	k7c4
tácek	tácek	k1gInSc4
dnem	dnem	k7c2
dolů	dol	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k9
do	do	k7c2
standardní	standardní	k2eAgFnSc2d1
pozice	pozice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
však	však	k9
už	už	k6eAd1
další	další	k2eAgFnSc4d1
kávu	káva	k1gFnSc4
nechce	chtít	k5eNaImIp3nS
<g/>
,	,	kIx,
položí	položit	k5eAaPmIp3nS
šálek	šálek	k1gInSc4
dnem	dnem	k7c2
vzhůru	vzhůru	k6eAd1
nebo	nebo	k8xC
na	na	k7c4
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
často	často	k6eAd1
pro	pro	k7c4
Evropany	Evropan	k1gMnPc4
i	i	k8xC
Američany	Američan	k1gMnPc4
trochu	trochu	k6eAd1
problém	problém	k1gInSc1
<g/>
,	,	kIx,
stavějí	stavět	k5eAaImIp3nP
šálky	šálek	k1gInPc7
dnem	dnem	k7c2
dolů	dol	k1gInPc2
a	a	k8xC
„	„	k?
<g/>
číšník	číšník	k1gMnSc1
<g/>
“	“	k?
jim	on	k3xPp3gMnPc3
s	s	k7c7
úsměvem	úsměv	k1gInSc7
nalévá	nalévat	k5eAaImIp3nS
další	další	k2eAgFnSc4d1
a	a	k8xC
další	další	k2eAgFnSc4d1
kávu	káva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Zdraví	zdraví	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Vlivy	vliv	k1gInPc4
kávy	káva	k1gFnSc2
na	na	k7c4
zdraví	zdraví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
r.	r.	kA
2017	#num#	k4
byla	být	k5eAaImAgFnS
vypracována	vypracován	k2eAgFnSc1d1
na	na	k7c6
základě	základ	k1gInSc6
201	#num#	k4
metaanalýz	metaanalýza	k1gFnPc2
souhrnná	souhrnný	k2eAgFnSc1d1
studie	studie	k1gFnSc1
kompletující	kompletující	k2eAgFnSc1d1
dostupné	dostupný	k2eAgNnSc4d1
poznání	poznání	k1gNnSc4
(	(	kIx(
<g/>
do	do	k7c2
června	červen	k1gInSc2
2017	#num#	k4
<g/>
)	)	kIx)
týkajících	týkající	k2eAgFnPc2d1
se	se	k3xPyFc4
vlivu	vliv	k1gInSc2
konzumace	konzumace	k1gFnSc2
kávy	káva	k1gFnSc2
na	na	k7c4
lidské	lidský	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
<g/>
;	;	kIx,
podkladem	podklad	k1gInSc7
byly	být	k5eAaImAgFnP
většinou	většinou	k6eAd1
observační	observační	k2eAgFnPc1d1
studie	studie	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
dokládají	dokládat	k5eAaImIp3nP
vztah	vztah	k1gInSc4
(	(	kIx(
<g/>
korelaci	korelace	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
nejsou	být	k5eNaImIp3nP
schopny	schopen	k2eAgInPc1d1
prokázat	prokázat	k5eAaPmF
příčinnou	příčinný	k2eAgFnSc4d1
souvislost	souvislost	k1gFnSc4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
korelace	korelace	k1gFnSc2
neimplikuje	implikovat	k5eNaImIp3nS
kauzalitu	kauzalita	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
populační	populační	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
je	být	k5eAaImIp3nS
pití	pití	k1gNnSc1
kávy	káva	k1gFnSc2
spojeno	spojit	k5eAaPmNgNnS
s	s	k7c7
nižší	nízký	k2eAgFnSc7d2
celkovou	celkový	k2eAgFnSc7d1
úmrtností	úmrtnost	k1gFnSc7
<g/>
,	,	kIx,
s	s	k7c7
nižší	nízký	k2eAgFnSc7d2
úmrtností	úmrtnost	k1gFnSc7
na	na	k7c4
kardiovaskulární	kardiovaskulární	k2eAgNnSc4d1
onemocnění	onemocnění	k1gNnSc4
a	a	k8xC
s	s	k7c7
nižším	nízký	k2eAgInSc7d2
výskytem	výskyt	k1gInSc7
rakoviny	rakovina	k1gFnSc2
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
rakoviny	rakovina	k1gFnSc2
prostaty	prostata	k1gFnSc2
<g/>
,	,	kIx,
dělohy	děloha	k1gFnSc2
<g/>
,	,	kIx,
rakoviny	rakovina	k1gFnSc2
kůže	kůže	k1gFnSc2
(	(	kIx(
<g/>
melanomové	melanomový	k2eAgNnSc1d1
i	i	k8xC
nemelanomové	melanomový	k2eNgNnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
rakoviny	rakovina	k1gFnSc2
jater	játra	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
prokázána	prokázán	k2eAgFnSc1d1
souvislost	souvislost	k1gFnSc1
mezi	mezi	k7c7
konzumací	konzumace	k1gFnSc7
kávy	káva	k1gFnSc2
a	a	k8xC
lepšího	dobrý	k2eAgNnSc2d2
metabolického	metabolický	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
nižšího	nízký	k2eAgInSc2d2
výskytu	výskyt	k1gInSc2
diabetu	diabetes	k1gInSc2
typu	typ	k1gInSc2
2	#num#	k4
<g/>
,	,	kIx,
metabolického	metabolický	k2eAgInSc2d1
syndromu	syndrom	k1gInSc2
<g/>
,	,	kIx,
žlučových	žlučový	k2eAgInPc2d1
kamenů	kámen	k1gInPc2
<g/>
,	,	kIx,
dnou	dna	k1gFnSc7
a	a	k8xC
ledvinovými	ledvinový	k2eAgInPc7d1
kameny	kámen	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláště	zvláště	k6eAd1
statisticky	statisticky	k6eAd1
výrazně	výrazně	k6eAd1
je	být	k5eAaImIp3nS
prokázán	prokázat	k5eAaPmNgInS
příznivý	příznivý	k2eAgInSc1d1
vztah	vztah	k1gInSc1
mezi	mezi	k7c7
pitím	pití	k1gNnSc7
kávy	káva	k1gFnSc2
a	a	k8xC
jaterními	jaterní	k2eAgFnPc7d1
potížemi	potíž	k1gFnPc7
(	(	kIx(
<g/>
snížená	snížený	k2eAgFnSc1d1
pravděpodobnost	pravděpodobnost	k1gFnSc1
jaterní	jaterní	k2eAgFnSc2d1
fibrózy	fibróza	k1gFnSc2
<g/>
,	,	kIx,
jaterní	jaterní	k2eAgFnSc2d1
cirhózy	cirhóza	k1gFnSc2
<g/>
,	,	kIx,
úmrtnosti	úmrtnost	k1gFnSc2
na	na	k7c4
jaterní	jaterní	k2eAgFnSc4d1
cirhózu	cirhóza	k1gFnSc4
a	a	k8xC
chronického	chronický	k2eAgNnSc2d1
onemocnění	onemocnění	k1gNnSc2
jater	játra	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečně	konečně	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
literatuře	literatura	k1gFnSc6
publikován	publikován	k2eAgMnSc1d1
statisticky	statisticky	k6eAd1
významná	významný	k2eAgFnSc1d1
souvislost	souvislost	k1gFnSc1
mezi	mezi	k7c7
pitím	pití	k1gNnSc7
kávy	káva	k1gFnSc2
a	a	k8xC
nižším	nízký	k2eAgInSc7d2
výskytem	výskyt	k1gInSc7
Parkinsonovy	Parkinsonův	k2eAgFnSc2d1
choroby	choroba	k1gFnSc2
<g/>
,	,	kIx,
klinické	klinický	k2eAgFnSc2d1
deprese	deprese	k1gFnSc2
a	a	k8xC
Alzheimerovy	Alzheimerův	k2eAgFnSc2d1
choroby	choroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
statisticky	statisticky	k6eAd1
přesvědčivé	přesvědčivý	k2eAgInPc4d1
důkazy	důkaz	k1gInPc4
negativní	negativní	k2eAgInPc4d1
účinků	účinek	k1gInPc2
pití	pití	k1gNnSc4
kávy	káva	k1gFnSc2
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
vztahují	vztahovat	k5eAaImIp3nP
k	k	k7c3
těhotenství	těhotenství	k1gNnSc3
(	(	kIx(
<g/>
pití	pití	k1gNnSc3
kávy	káva	k1gFnSc2
v	v	k7c6
těhotenství	těhotenství	k1gNnSc6
je	být	k5eAaImIp3nS
spojeno	spojit	k5eAaPmNgNnS
s	s	k7c7
nízkou	nízký	k2eAgFnSc7d1
porodní	porodní	k2eAgFnSc7d1
hmotností	hmotnost	k1gFnSc7
<g/>
,	,	kIx,
předčasnými	předčasný	k2eAgInPc7d1
porody	porod	k1gInPc7
i	i	k8xC
spontánními	spontánní	k2eAgInPc7d1
potraty	potrat	k1gInPc7
a	a	k8xC
také	také	k9
s	s	k7c7
akutní	akutní	k2eAgFnSc7d1
dětskou	dětský	k2eAgFnSc7d1
leukémií	leukémie	k1gFnSc7
<g/>
)	)	kIx)
a	a	k8xC
dále	daleko	k6eAd2
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
negativní	negativní	k2eAgFnSc2d1
souvislosti	souvislost	k1gFnSc2
vysoké	vysoký	k2eAgFnSc2d1
konzumace	konzumace	k1gFnSc2
kávy	káva	k1gFnSc2
a	a	k8xC
zvýšeného	zvýšený	k2eAgNnSc2d1
rizika	riziko	k1gNnSc2
vzniku	vznik	k1gInSc2
zlomenin	zlomenina	k1gFnPc2
u	u	k7c2
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
nejvhodnější	vhodný	k2eAgNnSc4d3
množství	množství	k1gNnSc4
kávy	káva	k1gFnSc2
maximalizující	maximalizující	k2eAgFnSc2d1
předpokládané	předpokládaný	k2eAgFnSc2d1
příznivé	příznivý	k2eAgNnSc1d1
zdravotní	zdravotní	k2eAgInPc1d1
účinky	účinek	k1gInPc1
kávy	káva	k1gFnSc2
byly	být	k5eAaImAgInP
doporučeny	doporučit	k5eAaPmNgInP
3	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
šálky	šálka	k1gFnSc2
kávy	káva	k1gFnSc2
denně	denně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Takovéto	takovýto	k3xDgNnSc1
množství	množství	k1gNnSc1
kávy	káva	k1gFnSc2
navíc	navíc	k6eAd1
neobsahuje	obsahovat	k5eNaImIp3nS
dostatečné	dostatečný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
kofeinu	kofein	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
projevit	projevit	k5eAaPmF
jeho	jeho	k3xOp3gInSc1
močopudný	močopudný	k2eAgInSc1d1
účinek	účinek	k1gInSc1
a	a	k8xC
tudíž	tudíž	k8xC
z	z	k7c2
hlediska	hledisko	k1gNnSc2
příjmu	příjem	k1gInSc2
tekutin	tekutina	k1gFnPc2
je	být	k5eAaImIp3nS
toto	tento	k3xDgNnSc4
množství	množství	k1gNnSc4
kávy	káva	k1gFnSc2
shodné	shodný	k2eAgInPc1d1
s	s	k7c7
pitím	pití	k1gNnSc7
stejného	stejný	k2eAgNnSc2d1
množství	množství	k1gNnSc2
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pití	pití	k1gNnSc1
kávy	káva	k1gFnSc2
a	a	k8xC
rakovina	rakovina	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
Mezinárodní	mezinárodní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
rakoviny	rakovina	k1gFnSc2
(	(	kIx(
<g/>
IARC	IARC	kA
<g/>
)	)	kIx)
konstatovala	konstatovat	k5eAaBmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
pití	pití	k1gNnSc1
kávy	káva	k1gFnSc2
nemůže	moct	k5eNaImIp3nS
klasifikovat	klasifikovat	k5eAaImF
jako	jako	k9
karcinogen	karcinogen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
pití	pití	k1gNnSc1
horkých	horký	k2eAgInPc2d1
nápojů	nápoj	k1gInPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
kávy	káva	k1gFnSc2
<g/>
)	)	kIx)
tato	tento	k3xDgFnSc1
agentura	agentura	k1gFnSc1
klasifikuje	klasifikovat	k5eAaImIp3nS
jako	jako	k9
pravděpodobný	pravděpodobný	k2eAgInSc1d1
karcinogen	karcinogen	k1gInSc1
pro	pro	k7c4
člověka	člověk	k1gMnSc4
(	(	kIx(
<g/>
skupina	skupina	k1gFnSc1
2	#num#	k4
<g/>
A	A	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čerstvost	čerstvost	k1gFnSc1
kávy	káva	k1gFnSc2
</s>
<s>
Káva	káva	k1gFnSc1
vydrží	vydržet	k5eAaPmIp3nS
opravdu	opravdu	k6eAd1
čerstvá	čerstvý	k2eAgFnSc1d1
pouze	pouze	k6eAd1
několik	několik	k4yIc1
dní	den	k1gInPc2
max	max	kA
<g/>
.	.	kIx.
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pražená	pražený	k2eAgFnSc1d1
káva	káva	k1gFnSc1
je	být	k5eAaImIp3nS
tepelně	tepelně	k6eAd1
opracovaný	opracovaný	k2eAgInSc1d1
produkt	produkt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
těkavé	těkavý	k2eAgFnPc4d1
látky	látka	k1gFnPc4
a	a	k8xC
oleje	olej	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
na	na	k7c6
vzduchu	vzduch	k1gInSc6
i	i	k8xC
v	v	k7c6
různých	různý	k2eAgFnPc6d1
atmosférách	atmosféra	k1gFnPc6
žluknou	žluknout	k5eAaBmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
prodávaných	prodávaný	k2eAgFnPc2d1
káv	káva	k1gFnPc2
jsou	být	k5eAaImIp3nP
již	již	k6eAd1
žluklé	žluklý	k2eAgFnPc1d1
<g/>
,	,	kIx,
vyčpělé	vyčpělý	k2eAgFnPc1d1
kávy	káva	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ať	ať	k8xS,k8xC
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
obyčejné	obyčejný	k2eAgFnPc4d1
nejlevnější	levný	k2eAgFnPc4d3
kávy	káva	k1gFnPc4
typu	typ	k1gInSc2
robusta	robusta	k1gFnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k8xC
kávy	káva	k1gFnSc2
renomovaných	renomovaný	k2eAgFnPc2d1
značek	značka	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
lidi	člověk	k1gMnPc4
nenechají	nechat	k5eNaPmIp3nP
dopustit	dopustit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k6eAd1
kávu	káva	k1gFnSc4
správně	správně	k6eAd1
skladovat	skladovat	k5eAaImF
<g/>
?	?	kIx.
</s>
<s>
Káva	káva	k1gFnSc1
je	být	k5eAaImIp3nS
organická	organický	k2eAgFnSc1d1
surovina	surovina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
si	se	k3xPyFc3
žádá	žádat	k5eAaImIp3nS
správné	správný	k2eAgNnSc4d1
skladování	skladování	k1gNnSc4
pro	pro	k7c4
uchování	uchování	k1gNnSc4
své	svůj	k3xOyFgFnSc2
vynikající	vynikající	k2eAgFnSc2d1
chuti	chuť	k1gFnSc2
a	a	k8xC
bohatého	bohatý	k2eAgNnSc2d1
aroma	aroma	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejími	její	k3xOp3gMnPc7
největšími	veliký	k2eAgMnPc7d3
nepřáteli	nepřítel	k1gMnPc7
jsou	být	k5eAaImIp3nP
teplo	teplo	k1gNnSc1
<g/>
,	,	kIx,
vlhkost	vlhkost	k1gFnSc1
<g/>
,	,	kIx,
vzduch	vzduch	k1gInSc1
a	a	k8xC
světlo	světlo	k1gNnSc1
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
doporučeno	doporučen	k2eAgNnSc1d1
ji	on	k3xPp3gFnSc4
od	od	k7c2
upražení	upražení	k1gNnSc2
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
nejdříve	dříve	k6eAd3
spotřebovat	spotřebovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kávu	káva	k1gFnSc4
ukládáme	ukládat	k5eAaImIp1nP
do	do	k7c2
řádně	řádně	k6eAd1
vymyté	vymytý	k2eAgFnSc2d1
neprůsvitné	průsvitný	k2eNgFnSc2d1
nádoby	nádoba	k1gFnSc2
s	s	k7c7
gumovým	gumový	k2eAgNnSc7d1
těsněním	těsnění	k1gNnSc7
<g/>
,	,	kIx,
nejlépe	dobře	k6eAd3
z	z	k7c2
tmavého	tmavý	k2eAgNnSc2d1
skla	sklo	k1gNnSc2
či	či	k8xC
plechu	plech	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
nádobu	nádoba	k1gFnSc4
dobře	dobře	k6eAd1
uzavřeme	uzavřít	k5eAaPmIp1nP
<g/>
,	,	kIx,
zabráníme	zabránit	k5eAaPmIp1nP
přístupu	přístup	k1gInSc2
vzduchu	vzduch	k1gInSc2
ke	k	k7c3
kávě	káva	k1gFnSc3
a	a	k8xC
uložíme	uložit	k5eAaPmIp1nP
ji	on	k3xPp3gFnSc4
do	do	k7c2
chladu	chlad	k1gInSc2
a	a	k8xC
sucha	sucho	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideálním	ideální	k2eAgNnSc7d1
místem	místo	k1gNnSc7
bude	být	k5eAaImBp3nS
kuchyňská	kuchyňský	k2eAgFnSc1d1
skříňka	skříňka	k1gFnSc1
nebo	nebo	k8xC
spíž	spíž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhněte	vyhníst	k5eAaPmIp3nS
se	se	k3xPyFc4
kontaktu	kontakt	k1gInSc2
s	s	k7c7
kořením	koření	k1gNnSc7
<g/>
,	,	kIx,
káva	káva	k1gFnSc1
často	často	k6eAd1
absorbuje	absorbovat	k5eAaBmIp3nS
okolní	okolní	k2eAgInPc4d1
pachy	pach	k1gInPc4
a	a	k8xC
znehodnocuje	znehodnocovat	k5eAaImIp3nS
se	se	k3xPyFc4
tak	tak	k9
její	její	k3xOp3gFnSc1
chuť	chuť	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Mletí	mletí	k1gNnSc1
zrnkové	zrnkový	k2eAgFnSc2d1
kávy	káva	k1gFnSc2
</s>
<s>
Při	při	k7c6
přípravě	příprava	k1gFnSc6
kávy	káva	k1gFnSc2
je	být	k5eAaImIp3nS
nejlépe	dobře	k6eAd3
umlít	umlít	k5eAaPmF
pouze	pouze	k6eAd1
tolik	tolik	k4xDc4,k4yIc4
kávy	káva	k1gFnSc2
<g/>
,	,	kIx,
kolik	kolik	k9
se	se	k3xPyFc4
přímo	přímo	k6eAd1
spotřebuje	spotřebovat	k5eAaPmIp3nS
–	–	k?
mletá	mletý	k2eAgFnSc1d1
káva	káva	k1gFnSc1
si	se	k3xPyFc3
nedokáže	dokázat	k5eNaPmIp3nS
dlouho	dlouho	k6eAd1
držet	držet	k5eAaImF
své	svůj	k3xOyFgNnSc4
aroma	aroma	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
rychle	rychle	k6eAd1
ztrácí	ztrácet	k5eAaImIp3nS
–	–	k?
asi	asi	k9
do	do	k7c2
15	#num#	k4
minut	minuta	k1gFnPc2
po	po	k7c6
umletí	umletí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Orientační	orientační	k2eAgFnSc1d1
hrubost	hrubost	k1gFnSc1
mletí	mletí	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
velmi	velmi	k6eAd1
jemné	jemný	k2eAgNnSc1d1
(	(	kIx(
<g/>
na	na	k7c4
prach	prach	k1gInSc4
<g/>
)	)	kIx)
–	–	k?
turecká	turecký	k2eAgFnSc1d1
káva	káva	k1gFnSc1
(	(	kIx(
<g/>
džezva	džezva	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
jemné	jemný	k2eAgFnPc4d1
–	–	k?
espresso	espressa	k1gFnSc5
</s>
<s>
střední	střední	k2eAgFnSc1d1
–	–	k?
moka	moka	k1gFnSc1
<g/>
,	,	kIx,
překapávaná	překapávaný	k2eAgFnSc1d1
filtrovaná	filtrovaný	k2eAgFnSc1d1
káva	káva	k1gFnSc1
(	(	kIx(
<g/>
aeropress	aeropress	k1gInSc1
<g/>
,	,	kIx,
chemex	chemex	k1gInSc1
a	a	k8xC
drip	drip	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kapsle	kapsle	k1gFnSc1
</s>
<s>
hrubé	hrubý	k2eAgNnSc4d1
mletí	mletí	k1gNnSc4
–	–	k?
french	french	k1gInSc1
press	press	k1gInSc1
<g/>
,	,	kIx,
český	český	k2eAgInSc1d1
turek	turek	k1gInSc1
</s>
<s>
Velmi	velmi	k6eAd1
jemně	jemně	k6eAd1
mletá	mletý	k2eAgFnSc1d1
káva	káva	k1gFnSc1
</s>
<s>
Jemně	jemně	k6eAd1
mletá	mletý	k2eAgFnSc1d1
káva	káva	k1gFnSc1
</s>
<s>
Středně	středně	k6eAd1
mletá	mletý	k2eAgFnSc1d1
káva	káva	k1gFnSc1
</s>
<s>
Hrubě	hrubě	k6eAd1
mletá	mletý	k2eAgFnSc1d1
káva	káva	k1gFnSc1
</s>
<s>
Plantážní	plantážní	k2eAgFnSc1d1
káva	káva	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Výběrová	výběrový	k2eAgFnSc1d1
káva	káva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Tímto	tento	k3xDgNnSc7
pojmenováním	pojmenování	k1gNnSc7
se	se	k3xPyFc4
typicky	typicky	k6eAd1
rozumí	rozumět	k5eAaImIp3nS
jakostní	jakostní	k2eAgFnSc1d1
jednodruhová	jednodruhový	k2eAgFnSc1d1
káva	káva	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
původ	původ	k1gInSc4
v	v	k7c6
nějakém	nějaký	k3yIgNnSc6
konkrétním	konkrétní	k2eAgNnSc6d1
zeměpisném	zeměpisný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
sbírána	sbírán	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
těchto	tento	k3xDgFnPc2
oblastí	oblast	k1gFnPc2
se	se	k3xPyFc4
také	také	k9
odvíjí	odvíjet	k5eAaImIp3nS
konkrétní	konkrétní	k2eAgFnPc4d1
typické	typický	k2eAgFnPc4d1
chuťové	chuťový	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
kávy	káva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
kávy	káva	k1gFnSc2
</s>
<s>
Kávu	káva	k1gFnSc4
lze	lze	k6eAd1
rozdělit	rozdělit	k5eAaPmF
do	do	k7c2
několika	několik	k4yIc2
skupin	skupina	k1gFnPc2
podle	podle	k7c2
způsobu	způsob	k1gInSc2
a	a	k8xC
okolností	okolnost	k1gFnSc7
zpracování	zpracování	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Zrnková	zrnkový	k2eAgFnSc1d1
káva	káva	k1gFnSc1
</s>
<s>
Mletá	mletý	k2eAgFnSc1d1
káva	káva	k1gFnSc1
</s>
<s>
Porcovaná	porcovaný	k2eAgFnSc1d1
káva	káva	k1gFnSc1
</s>
<s>
Fair	fair	k6eAd1
trade	trade	k6eAd1
káva	káva	k1gFnSc1
</s>
<s>
Instantní	instantní	k2eAgFnSc1d1
káva	káva	k1gFnSc1
</s>
<s>
Bio	Bio	k?
káva	káva	k1gFnSc1
</s>
<s>
Bezkofeinová	Bezkofeinová	k1gFnSc1
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Příprava	příprava	k1gFnSc1
a	a	k8xC
pití	pití	k1gNnSc1
kávy	káva	k1gFnSc2
v	v	k7c6
bojových	bojový	k2eAgFnPc6d1
polních	polní	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
</s>
<s>
Pražení	pražení	k1gNnSc1
kávy	káva	k1gFnSc2
v	v	k7c6
čs	čs	kA
<g/>
.	.	kIx.
legiích	legie	k1gFnPc6
na	na	k7c6
ruské	ruský	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Západní	západní	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Místo	místo	k7c2
první	první	k4xOgFnSc2
pomoci	pomoc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člen	člen	k1gInSc1
armádního	armádní	k2eAgInSc2d1
zdravotnického	zdravotnický	k2eAgInSc2d1
sboru	sbor	k1gInSc2
předává	předávat	k5eAaImIp3nS
šálek	šálek	k1gInSc1
kávy	káva	k1gFnSc2
zraněnému	zraněný	k1gMnSc3
vojákovi	voják	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Pití	pití	k1gNnSc1
kávy	káva	k1gFnSc2
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
za	za	k7c4
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
What	What	k1gInSc1
are	ar	k1gInSc5
the	the	k?
Most	most	k1gInSc1
Commonly	Commonla	k1gFnSc2
Traded	Traded	k1gMnSc1
Commodities	Commodities	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
InvestorGuide	InvestorGuid	k1gInSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
vybudovala	vybudovat	k5eAaPmAgFnS
v	v	k7c6
Etiopii	Etiopie	k1gFnSc6
novou	nový	k2eAgFnSc4d1
pražírnu	pražírna	k1gFnSc4
kávy	káva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mzv	Mzv	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2020-03-31	2020-03-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Svět	svět	k1gInSc1
plus	plus	k1gInSc1
-	-	kIx~
příloha	příloha	k1gFnSc1
teplického	teplický	k2eAgInSc2d1
deníku	deník	k1gInSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
denik	denik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
téma	téma	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
35	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
BRZOŇOVÁ	BRZOŇOVÁ	kA
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc1
kávy	káva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sdružení	sdružení	k1gNnSc2
českých	český	k2eAgMnPc2d1
spotřebitelů	spotřebitel	k1gMnPc2
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc4
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Jak	jak	k6eAd1
poznáme	poznat	k5eAaPmIp1nP
kvalitu	kvalita	k1gFnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87719	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
POOLE	POOLE	kA
<g/>
,	,	kIx,
Robin	robin	k2eAgMnSc1d1
<g/>
;	;	kIx,
KENNEDY	KENNEDY	kA
<g/>
,	,	kIx,
Oliver	Oliver	k1gMnSc1
J.	J.	kA
<g/>
;	;	kIx,
RODERICK	RODERICK	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Coffee	Coffe	k1gInSc2
consumption	consumption	k1gInSc1
and	and	k?
health	health	k1gInSc1
<g/>
:	:	kIx,
umbrella	umbrella	k1gFnSc1
review	review	k?
of	of	k?
meta-analyses	meta-analyses	k1gInSc1
of	of	k?
multiple	multipl	k1gInSc5
health	health	k1gMnSc1
outcomes	outcomes	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BMJ	BMJ	kA
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
359	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
j	j	k?
<g/>
5024	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
<g/>
:	:	kIx,
29167102	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
959	#num#	k4
<g/>
-	-	kIx~
<g/>
8138	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.113	10.113	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
bmj	bmj	k?
<g/>
.	.	kIx.
<g/>
j	j	k?
<g/>
5024	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
29167102	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KILLER	KILLER	kA
<g/>
,	,	kIx,
Sophie	Sophie	k1gFnSc1
C.	C.	kA
<g/>
;	;	kIx,
BLANNIN	BLANNIN	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gMnSc1
K.	K.	kA
<g/>
;	;	kIx,
JEUKENDRUP	JEUKENDRUP	kA
<g/>
,	,	kIx,
Asker	Asker	k1gInSc1
E.	E.	kA
No	no	k9
Evidence	evidence	k1gFnSc1
of	of	k?
Dehydration	Dehydration	k1gInSc1
with	with	k1gInSc1
Moderate	Moderat	k1gInSc5
Daily	Dail	k1gInPc7
Coffee	Coffee	k1gNnSc1
Intake	Intake	k1gFnSc1
<g/>
:	:	kIx,
A	a	k9
Counterbalanced	Counterbalanced	k1gInSc1
Cross-Over	Cross-Over	k1gInSc1
Study	stud	k1gInPc1
in	in	k?
a	a	k8xC
Free-Living	Free-Living	k1gInSc1
Population	Population	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PLoS	plos	k1gMnSc1
ONE	ONE	kA
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
<g/>
:	:	kIx,
24416202	#num#	k4
</s>
<s>
PMCID	PMCID	kA
<g/>
:	:	kIx,
PMC	PMC	kA
<g/>
3886980	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1932	#num#	k4
<g/>
-	-	kIx~
<g/>
6203	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.137	10.137	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
journal	journat	k5eAaImAgInS,k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
pone	pone	k1gInSc1
<g/>
.0084154	.0084154	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
24416202	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Does	Does	k1gInSc1
caffeine	caffeinout	k5eAaPmIp3nS
cause	causa	k1gFnSc3
dehydration	dehydration	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
medicalxpress	medicalxpressa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Miroslav	Miroslav	k1gMnSc1
Šuta	šuta	k1gFnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
Šťovíček	Šťovíček	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Způsobuje	způsobovat	k5eAaImIp3nS
káva	káva	k1gFnSc1
rakovinu	rakovina	k1gFnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
Plzeň	Plzeň	k1gFnSc1
<g/>
,	,	kIx,
Zdraví	zdraví	k1gNnSc1
"	"	kIx"
<g/>
v	v	k7c6
cajku	cajk	k1gInSc6
<g/>
"	"	kIx"
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2017	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
AUGUSTÍN	AUGUSTÍN	kA
<g/>
,	,	kIx,
Jozef	Jozef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
kávy	káva	k1gFnSc2
o	o	k7c6
kávě	káva	k1gFnSc6
a	a	k8xC
kávovinách	kávovina	k1gFnPc6
(	(	kIx(
<g/>
původním	původní	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Pri	Pri	k1gFnSc1
káve	kávat	k5eAaPmIp3nS
o	o	k7c6
káve	káve	k1gFnPc6
a	a	k8xC
kávovinách	kávovina	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Katarína	Katarín	k1gInSc2
Kašpárková	kašpárkový	k2eAgFnSc1d1
Koišová	Koišová	k1gFnSc1
<g/>
.	.	kIx.
aktualizované	aktualizovaný	k2eAgFnPc4d1
a	a	k8xC
rozšířené	rozšířený	k2eAgFnPc4d1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
360	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7462	#num#	k4
<g/>
-	-	kIx~
<g/>
850	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ČERNÝ	Černý	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
zámoří	zámoří	k1gNnSc2
do	do	k7c2
Čech	Čechy	k1gFnPc2
<g/>
:	:	kIx,
Čokoláda	čokoláda	k1gFnSc1
<g/>
,	,	kIx,
čaj	čaj	k1gInSc1
a	a	k8xC
káva	káva	k1gFnSc1
v	v	k7c6
raném	raný	k2eAgInSc6d1
novověku	novověk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
3149	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kavárna	kavárna	k1gFnSc1
</s>
<s>
Barista	Barista	k1gMnSc1
</s>
<s>
Mistr	mistr	k1gMnSc1
kávy	káva	k1gFnSc2
</s>
<s>
Latté	Latta	k1gMnPc1
art	art	k?
</s>
<s>
Káva	káva	k1gFnSc1
bez	bez	k7c2
kofeinu	kofein	k1gInSc2
</s>
<s>
Instantní	instantní	k2eAgFnSc1d1
káva	káva	k1gFnSc1
(	(	kIx(
<g/>
rozpustná	rozpustný	k2eAgFnSc1d1
káva	káva	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Easy	Easa	k1gFnPc1
Serving	Serving	k1gInSc1
Espresso	Espressa	k1gFnSc5
</s>
<s>
Kávové	kávový	k2eAgFnPc4d1
odrůdy	odrůda	k1gFnPc4
</s>
<s>
Julius	Julius	k1gMnSc1
Meinl	Meinl	k1gMnSc1
</s>
<s>
Nálev	nálev	k1gInSc1
</s>
<s>
Čaj	čaj	k1gInSc1
</s>
<s>
Maté	maté	k1gNnSc1
</s>
<s>
Druhy	druh	k1gInPc1
přípravy	příprava	k1gFnSc2
kávy	káva	k1gFnSc2
</s>
<s>
Cappuccino	Cappuccino	k1gNnSc1
</s>
<s>
Moccacino	Moccacino	k1gNnSc1
</s>
<s>
Alžírská	alžírský	k2eAgFnSc1d1
káva	káva	k1gFnSc1
</s>
<s>
Holandská	holandský	k2eAgFnSc1d1
káva	káva	k1gFnSc1
</s>
<s>
Irská	irský	k2eAgFnSc1d1
káva	káva	k1gFnSc1
</s>
<s>
Karibská	karibský	k2eAgFnSc1d1
káva	káva	k1gFnSc1
</s>
<s>
Café	café	k1gNnSc1
au	au	k0
lait	laita	k1gFnPc2
</s>
<s>
Caffè	Caffè	k?
crema	crema	k1gFnSc1
</s>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1
káva	káva	k1gFnSc1
</s>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1
melange	melange	k1gFnSc1
</s>
<s>
Káva	káva	k1gFnSc1
dalgona	dalgona	k1gFnSc1
</s>
<s>
Nádoby	nádoba	k1gFnPc4
a	a	k8xC
zařízení	zařízení	k1gNnPc4
na	na	k7c4
přípravu	příprava	k1gFnSc4
kávy	káva	k1gFnSc2
</s>
<s>
Džezva	džezva	k1gFnSc1
</s>
<s>
Dallah	Dallah	k1gMnSc1
</s>
<s>
Kávovar	kávovar	k1gInSc1
</s>
<s>
Presovač	Presovač	k1gMnSc1
</s>
<s>
Moka	moka	k1gFnSc1
konvička	konvička	k1gFnSc1
(	(	kIx(
<g/>
bialettka	bialettka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
káva	káva	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Káva	káva	k1gFnSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
káva	káva	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Téma	téma	k1gNnSc1
Káva	káva	k1gFnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Víte	vědět	k5eAaImIp2nP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
udělat	udělat	k5eAaPmF
espreso	espreso	k1gNnSc4
a	a	k8xC
kapučíno	kapučína	k1gFnSc5
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
ekonomika	ekonomika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Káva	káva	k1gFnSc1
–	–	k?
nápoj	nápoj	k1gInSc4
plný	plný	k2eAgInSc4d1
otáznikov	otáznikov	k1gInSc4
–	–	k?
primar	primar	k1gInSc1
<g/>
.	.	kIx.
<g/>
sme	sme	k?
<g/>
.	.	kIx.
<g/>
sk	sk	k?
</s>
<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
co	co	k9
dělá	dělat	k5eAaImIp3nS
<g/>
:	:	kIx,
Káva	káva	k1gFnSc1
</s>
<s>
Jak	jak	k6eAd1
na	na	k7c4
lepší	dobrý	k2eAgFnSc4d2
kávu	káva	k1gFnSc4
(	(	kIx(
<g/>
hobby	hobby	k1gNnSc4
<g/>
.	.	kIx.
<g/>
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Káva	káva	k1gFnSc1
Druhy	druh	k1gInPc7
</s>
<s>
Coffea	Coffe	k2eAgFnSc1d1
arabica	arabica	k1gFnSc1
<g/>
:	:	kIx,
Moca	Moc	k2eAgFnSc1d1
•	•	k?
Coffea	Coffea	k1gFnSc1
canephora	canephora	k1gFnSc1
(	(	kIx(
<g/>
robusta	robusta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Kopi	Kop	k1gMnPc1
Luwak	Luwak	k1gMnSc1
•	•	k?
Coffea	Coffea	k1gMnSc1
liberica	libericum	k1gNnSc2
•	•	k?
Kávové	kávový	k2eAgFnSc2d1
odrůdy	odrůda	k1gFnSc2
Značky	značka	k1gFnSc2
</s>
<s>
Camiscia	Camiscia	k1gFnSc1
•	•	k?
Dibar	Dibar	k1gInSc1
•	•	k?
Illy	Illa	k1gFnSc2
•	•	k?
Intenso	Intensa	k1gFnSc5
•	•	k?
Izzo	Izzo	k1gNnSc4
•	•	k?
Jihlavanka	Jihlavanka	k1gFnSc1
•	•	k?
Lavazza	Lavazza	k1gFnSc1
•	•	k?
Moncafé	Moncafá	k1gFnSc2
•	•	k?
Nescafé	Nescafá	k1gFnSc2
•	•	k?
Pellini	Pellin	k2eAgMnPc1d1
•	•	k?
Rovi	Rov	k1gFnPc4
•	•	k?
Segafredo	Segafredo	k1gNnSc1
•	•	k?
Starbucks	Starbucksa	k1gFnPc2
•	•	k?
Tchibo	Tchiba	k1gFnSc5
•	•	k?
Universal	Universal	k1gFnSc6
•	•	k?
Vivi	Viv	k1gFnSc2
Různé	různý	k2eAgFnPc1d1
úpravy	úprava	k1gFnPc1
nápoje	nápoj	k1gInPc4
</s>
<s>
Alžírská	alžírský	k2eAgFnSc1d1
káva	káva	k1gFnSc1
•	•	k?
Caffè	Caffè	k1gFnPc2
latte	latte	k5eAaPmIp2nP
•	•	k?
Caffè	Caffè	k1gFnSc4
macchiato	macchiato	k6eAd1
•	•	k?
Cappuccino	Cappuccino	k1gNnSc1
•	•	k?
Frappé	Frappý	k2eAgFnSc2d1
•	•	k?
Espresso	Espressa	k1gFnSc5
(	(	kIx(
<g/>
lungo	lungo	k1gNnSc1
<g/>
,	,	kIx,
ristretto	ristretto	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Flat	Flat	k1gInSc1
White	Whit	k1gInSc5
•	•	k?
Instantní	instantní	k2eAgFnSc1d1
káva	káva	k1gFnSc1
•	•	k?
Irská	irský	k2eAgFnSc1d1
káva	káva	k1gFnSc1
•	•	k?
Latte	Latt	k1gInSc5
macchiato	macchiato	k6eAd1
•	•	k?
Ledová	ledový	k2eAgFnSc1d1
káva	káva	k1gFnSc1
•	•	k?
Long	Long	k1gMnSc1
black	black	k1gMnSc1
•	•	k?
Moccacino	Moccacin	k2eAgNnSc1d1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
mokka	mokka	k1gNnSc7
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
káva	káva	k1gFnSc1
•	•	k?
Vídeňská	vídeňský	k2eAgFnSc1d1
melange	melange	k1gFnSc1
•	•	k?
Vídeňská	vídeňský	k2eAgFnSc1d1
káva	káva	k1gFnSc1
Zpracování	zpracování	k1gNnSc1
a	a	k8xC
pomůcky	pomůcka	k1gFnPc1
přípravy	příprava	k1gFnSc2
</s>
<s>
AeroPress	AeroPress	k6eAd1
•	•	k?
Džezva	džezva	k1gFnSc1
•	•	k?
Chemex	Chemex	k1gInSc1
•	•	k?
Překapávaná	překapávaný	k2eAgFnSc1d1
káva	káva	k1gFnSc1
•	•	k?
Filtr	filtr	k1gInSc4
na	na	k7c4
kávu	káva	k1gFnSc4
•	•	k?
French	French	k1gInSc1
press	press	k1gInSc1
•	•	k?
Karlovarský	karlovarský	k2eAgInSc1d1
příbor	příbor	k1gInSc1
•	•	k?
Kávovar	kávovar	k1gInSc1
•	•	k?
Mlýnek	mlýnek	k1gInSc1
na	na	k7c4
kávu	káva	k1gFnSc4
•	•	k?
Moka	moka	k1gFnSc1
•	•	k?
Vacuum	Vacuum	k1gInSc1
pot	pot	k1gInSc1
Káva	káva	k1gFnSc1
a	a	k8xC
životní	životní	k2eAgInSc1d1
styl	styl	k1gInSc1
</s>
<s>
Barista	Barista	k1gMnSc1
•	•	k?
Latte	Latt	k1gInSc5
art	art	k?
•	•	k?
Kavárna	kavárna	k1gFnSc1
•	•	k?
Pražení	pražení	k1gNnPc2
•	•	k?
Třetí	třetí	k4xOgFnSc1
vlna	vlna	k1gFnSc1
kávy	káva	k1gFnSc2
Kategorie	kategorie	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4029189-3	4029189-3	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
8698	#num#	k4
</s>
