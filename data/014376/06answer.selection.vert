<s>
Espresso	Espressa	k1gFnSc5
je	být	k5eAaImIp3nS
způsob	způsob	k1gInSc1
výroby	výroba	k1gFnSc2
kávy	káva	k1gFnSc2
italského	italský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
je	být	k5eAaImIp3nS
malé	malý	k2eAgNnSc1d1
množství	množství	k1gNnSc1
téměř	téměř	k6eAd1
vroucí	vroucí	k2eAgFnPc4d1
vody	voda	k1gFnPc4
vytlačeno	vytlačen	k2eAgNnSc1d1
pod	pod	k7c7
tlakem	tlak	k1gInSc7
jemně	jemně	k6eAd1
mletými	mletý	k2eAgFnPc7d1
kávovými	kávový	k2eAgNnPc7d1
zrny	zrno	k1gNnPc7
<g/>
.	.	kIx.
</s>