<p>
<s>
Lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
Schöleitenbahn	Schöleitenbahna	k1gFnPc2	Schöleitenbahna
je	být	k5eAaImIp3nS	být
kabinková	kabinkový	k2eAgFnSc1d1	kabinková
lanovka	lanovka	k1gFnSc1	lanovka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stoupá	stoupat	k5eAaImIp3nS	stoupat
z	z	k7c2	z
údolí	údolí	k1gNnSc2	údolí
části	část	k1gFnSc2	část
Saalbachu	Saalbach	k1gInSc2	Saalbach
Vorderchlemm	Vorderchlemmo	k1gNnPc2	Vorderchlemmo
na	na	k7c4	na
svahy	svah	k1gInPc4	svah
vrcholu	vrchol	k1gInSc2	vrchol
v	v	k7c4	v
Wildenkarkogel	Wildenkarkogel	k1gInSc4	Wildenkarkogel
Rakousku	Rakousko	k1gNnSc3	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Lanovka	lanovka	k1gFnSc1	lanovka
má	mít	k5eAaImIp3nS	mít
přestupní	přestupní	k2eAgInSc4d1	přestupní
terminál	terminál	k1gInSc4	terminál
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vrchol	vrchol	k1gInSc1	vrchol
s	s	k7c7	s
lanovkami	lanovka	k1gFnPc7	lanovka
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
skiareálu	skiareál	k1gInSc2	skiareál
Ski	ski	k1gFnPc2	ski
Circus	Circus	k1gMnSc1	Circus
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
lanovkou	lanovka	k1gFnSc7	lanovka
vede	vést	k5eAaImIp3nS	vést
červená	červený	k2eAgFnSc1d1	červená
sjezdovka	sjezdovka	k1gFnSc1	sjezdovka
Schönleiten	Schönleitno	k1gNnPc2	Schönleitno
Talstation	Talstation	k1gInSc4	Talstation
a	a	k8xC	a
z	z	k7c2	z
části	část	k1gFnSc2	část
modrá	modrý	k2eAgFnSc1d1	modrá
sjezdovka	sjezdovka	k1gFnSc1	sjezdovka
Schönleiten	Schönleiten	k2eAgInSc4d1	Schönleiten
Mittelstation	Mittelstation	k1gInSc4	Mittelstation
<g/>
.	.	kIx.	.
</s>
</p>
