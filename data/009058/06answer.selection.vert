<s>
Lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
Schöleitenbahn	Schöleitenbahna	k1gFnPc2	Schöleitenbahna
je	být	k5eAaImIp3nS	být
kabinková	kabinkový	k2eAgFnSc1d1	kabinková
lanovka	lanovka	k1gFnSc1	lanovka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stoupá	stoupat	k5eAaImIp3nS	stoupat
z	z	k7c2	z
údolí	údolí	k1gNnSc2	údolí
části	část	k1gFnSc2	část
Saalbachu	Saalbach	k1gInSc2	Saalbach
Vorderchlemm	Vorderchlemmo	k1gNnPc2	Vorderchlemmo
na	na	k7c4	na
svahy	svah	k1gInPc4	svah
vrcholu	vrchol	k1gInSc2	vrchol
v	v	k7c4	v
Wildenkarkogel	Wildenkarkogel	k1gInSc4	Wildenkarkogel
Rakousku	Rakousko	k1gNnSc3	Rakousko
<g/>
.	.	kIx.	.
</s>
