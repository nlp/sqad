<s>
Římská	římský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
Publiem	Publium	k1gNnSc7
Corneliem	Cornelium	k1gNnSc7
Scipionem	Scipion	k1gInSc7
(	(	kIx(
<g/>
Africanem	African	k1gInSc7
<g/>
)	)	kIx)
porazila	porazit	k5eAaPmAgFnS
kartaginské	kartaginský	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
pod	pod	k7c7
velením	velení	k1gNnSc7
Hannibala	Hannibal	k1gMnSc2
<g/>
.	.	kIx.
</s>