<s>
Woody	Woody	k6eAd1	Woody
Allen	allen	k1gInSc1	allen
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Allen	Allen	k1gMnSc1	Allen
Stewart	Stewart	k1gMnSc1	Stewart
Konigsberg	Konigsberg	k1gMnSc1	Konigsberg
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1935	[number]	k4	1935
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
amatérský	amatérský	k2eAgMnSc1d1	amatérský
klarinetista	klarinetista	k1gMnSc1	klarinetista
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
komik	komik	k1gMnSc1	komik
<g/>
,	,	kIx,	,
čtyřnásobný	čtyřnásobný	k2eAgMnSc1d1	čtyřnásobný
držitel	držitel	k1gMnSc1	držitel
Oscara	Oscar	k1gMnSc2	Oscar
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
uměleckém	umělecký	k2eAgInSc6d1	umělecký
světě	svět	k1gInSc6	svět
působí	působit	k5eAaImIp3nS	působit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
amerických	americký	k2eAgMnPc2d1	americký
filmařů	filmař	k1gMnPc2	filmař
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
dokázali	dokázat	k5eAaPmAgMnP	dokázat
prosadit	prosadit	k5eAaPmF	prosadit
především	především	k6eAd1	především
jako	jako	k8xS	jako
tvůrci	tvůrce	k1gMnPc1	tvůrce
autorských	autorský	k2eAgInPc2d1	autorský
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tak	tak	k9	tak
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
různých	různý	k2eAgInPc2d1	různý
zájmů	zájem	k1gInPc2	zájem
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
talentu	talent	k1gInSc2	talent
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejen	nejen	k6eAd1	nejen
režisérem	režisér	k1gMnSc7	režisér
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
hercem	herec	k1gMnSc7	herec
<g/>
,	,	kIx,	,
spisovatelem	spisovatel	k1gMnSc7	spisovatel
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řade	řad	k1gInSc5	řad
i	i	k9	i
jazzmanem	jazzman	k1gMnSc7	jazzman
a	a	k8xC	a
hráčem	hráč	k1gMnSc7	hráč
na	na	k7c4	na
klarinet	klarinet	k1gInSc4	klarinet
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Newyorské	newyorský	k2eAgFnSc6d1	newyorská
univerzitě	univerzita	k1gFnSc6	univerzita
skončil	skončit	k5eAaPmAgInS	skončit
už	už	k6eAd1	už
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
semestru	semestr	k1gInSc6	semestr
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
hned	hned	k6eAd1	hned
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zaměstnán	zaměstnat	k5eAaPmNgInS	zaměstnat
v	v	k7c6	v
NBC	NBC	kA	NBC
jako	jako	k8xC	jako
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
prosadit	prosadit	k5eAaPmF	prosadit
jako	jako	k9	jako
tvůrce	tvůrce	k1gMnSc1	tvůrce
vtipných	vtipný	k2eAgMnPc2d1	vtipný
gagů	gag	k1gInPc2	gag
pro	pro	k7c4	pro
komiky	komik	k1gMnPc4	komik
a	a	k8xC	a
pro	pro	k7c4	pro
různá	různý	k2eAgNnPc4d1	různé
vystoupení	vystoupení	k1gNnPc4	vystoupení
v	v	k7c6	v
nočních	noční	k2eAgInPc6d1	noční
podnicích	podnik	k1gInPc6	podnik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
se	se	k3xPyFc4	se
ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Harlene	Harlen	k1gInSc5	Harlen
Rosenovou	Rosenová	k1gFnSc7	Rosenová
<g/>
,	,	kIx,	,
studentkou	studentka	k1gFnSc7	studentka
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
se	se	k3xPyFc4	se
zapletl	zaplést	k5eAaPmAgMnS	zaplést
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
z	z	k7c2	z
vlastního	vlastní	k2eAgInSc2d1	vlastní
filmu	film	k1gInSc2	film
Banáni	Banán	k1gMnPc1	Banán
Louise	Louis	k1gMnPc4	Louis
Lasserovou	Lasserová	k1gFnSc4	Lasserová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
rozvedl	rozvést	k5eAaPmAgInS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Potřetí	potřetí	k4xO	potřetí
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
s	s	k7c7	s
adoptivní	adoptivní	k2eAgFnSc7d1	adoptivní
dcerou	dcera	k1gFnSc7	dcera
své	svůj	k3xOyFgFnSc2	svůj
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
partnerky	partnerka	k1gFnSc2	partnerka
Mii	Mii	k1gFnPc2	Mii
Farrowové	Farrowové	k2eAgFnSc2d1	Farrowové
Soon-Yi	Soon-Y	k1gFnSc2	Soon-Y
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
později	pozdě	k6eAd2	pozdě
adoptovali	adoptovat	k5eAaPmAgMnP	adoptovat
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
programu	program	k1gInSc6	program
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Comedian	Comediany	k1gInPc2	Comediany
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Comedian	Comediana	k1gFnPc2	Comediana
<g/>
"	"	kIx"	"
vysílaném	vysílaný	k2eAgInSc6d1	vysílaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
britskou	britský	k2eAgFnSc7d1	britská
televizí	televize	k1gFnSc7	televize
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
první	první	k4xOgFnSc2	první
padesátky	padesátka	k1gFnSc2	padesátka
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
komiků	komik	k1gMnPc2	komik
a	a	k8xC	a
bavičů	bavič	k1gMnPc2	bavič
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
i	i	k9	i
to	ten	k3xDgNnSc1	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
velké	velký	k2eAgFnSc6d1	velká
oblíbenosti	oblíbenost	k1gFnSc6	oblíbenost
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
a	a	k8xC	a
vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Nettie	Nettie	k1gFnSc1	Nettie
Cherrie	Cherrie	k1gFnSc1	Cherrie
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
účetní	účetní	k1gMnPc4	účetní
v	v	k7c6	v
rodinném	rodinný	k2eAgNnSc6d1	rodinné
lahůdkářství	lahůdkářství	k1gNnSc6	lahůdkářství
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
Martin	Martin	k1gMnSc1	Martin
Konigsberg	Konigsberg	k1gMnSc1	Konigsberg
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
rytec	rytec	k1gMnSc1	rytec
drahokamů	drahokam	k1gInPc2	drahokam
a	a	k8xC	a
číšník	číšník	k1gMnSc1	číšník
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
byla	být	k5eAaImAgFnS	být
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
prarodiče	prarodič	k1gMnPc1	prarodič
byli	být	k5eAaImAgMnP	být
imigranti	imigrant	k1gMnPc1	imigrant
mluvící	mluvící	k2eAgMnPc1d1	mluvící
jazykem	jazyk	k1gInSc7	jazyk
jidiš	jidiš	k1gNnPc2	jidiš
a	a	k8xC	a
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
sestru	sestra	k1gFnSc4	sestra
Letty	Letta	k1gFnSc2	Letta
(	(	kIx(	(
<g/>
narozena	narozen	k2eAgFnSc1d1	narozena
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
v	v	k7c6	v
Midwoodu	Midwood	k1gInSc6	Midwood
v	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
a	a	k8xC	a
vyrostli	vyrůst	k5eAaPmAgMnP	vyrůst
na	na	k7c4	na
Lower	Lower	k1gInSc4	Lower
East	East	k2eAgInSc4d1	East
Side	Side	k1gInSc4	Side
v	v	k7c6	v
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dětství	dětství	k1gNnSc1	dětství
nebylo	být	k5eNaImAgNnS	být
nikterak	nikterak	k6eAd1	nikterak
šťastné	šťastný	k2eAgNnSc1d1	šťastné
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
spolu	spolu	k6eAd1	spolu
nevycházeli	vycházet	k5eNaImAgMnP	vycházet
dobře	dobře	k6eAd1	dobře
a	a	k8xC	a
obzvláště	obzvláště	k6eAd1	obzvláště
s	s	k7c7	s
přísnou	přísný	k2eAgFnSc7d1	přísná
temperamentní	temperamentní	k2eAgFnSc7d1	temperamentní
matkou	matka	k1gFnSc7	matka
měl	mít	k5eAaImAgInS	mít
vyhrocený	vyhrocený	k2eAgInSc1d1	vyhrocený
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
mluvil	mluvit	k5eAaImAgMnS	mluvit
jazykem	jazyk	k1gInSc7	jazyk
jidiš	jidiš	k1gNnSc1	jidiš
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
židovskou	židovský	k2eAgFnSc4d1	židovská
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c6	na
Public	publicum	k1gNnPc2	publicum
School	School	k1gInSc4	School
99	[number]	k4	99
a	a	k8xC	a
na	na	k7c4	na
Midwood	Midwood	k1gInSc4	Midwood
High	High	k1gInSc4	High
School	Schoola	k1gFnPc2	Schoola
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgInPc3	svůj
zrzavým	zrzavý	k2eAgInPc3d1	zrzavý
vlasům	vlas	k1gInPc3	vlas
dostal	dostat	k5eAaPmAgMnS	dostat
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Zrzek	zrzek	k1gMnSc1	zrzek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
studenty	student	k1gMnPc4	student
udělal	udělat	k5eAaPmAgInS	udělat
dojem	dojem	k1gInSc1	dojem
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
neobyčejným	obyčejný	k2eNgInSc7d1	neobyčejný
talentem	talent	k1gInSc7	talent
v	v	k7c6	v
kouzelných	kouzelný	k2eAgInPc6d1	kouzelný
a	a	k8xC	a
karetních	karetní	k2eAgInPc6d1	karetní
tricích	trik	k1gInPc6	trik
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
filmech	film	k1gInPc6	film
často	často	k6eAd1	často
stylizuje	stylizovat	k5eAaImIp3nS	stylizovat
jako	jako	k9	jako
nešika	nešika	k1gFnSc1	nešika
a	a	k8xC	a
společensky	společensky	k6eAd1	společensky
neoblíbený	oblíbený	k2eNgMnSc1d1	neoblíbený
tvor	tvor	k1gMnSc1	tvor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
byl	být	k5eAaImAgMnS	být
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
student	student	k1gMnSc1	student
i	i	k8xC	i
obratný	obratný	k2eAgMnSc1d1	obratný
hráč	hráč	k1gMnSc1	hráč
basketbalu	basketbal	k1gInSc2	basketbal
a	a	k8xC	a
baseballu	baseball	k1gInSc2	baseball
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
vydělal	vydělat	k5eAaPmAgMnS	vydělat
nějaké	nějaký	k3yIgInPc4	nějaký
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
gagy	gag	k1gInPc4	gag
pro	pro	k7c4	pro
Davida	David	k1gMnSc4	David
O.	O.	kA	O.
Albera	Alber	k1gMnSc4	Alber
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
prodával	prodávat	k5eAaImAgMnS	prodávat
novinám	novina	k1gFnPc3	novina
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
sám	sám	k3xTgMnSc1	sám
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgInSc1	první
vtip	vtip	k1gInSc1	vtip
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c6	v
společenské	společenský	k2eAgFnSc6d1	společenská
rubrice	rubrika	k1gFnSc6	rubrika
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Woody	Wooda	k1gMnSc2	Wooda
Allen	Allen	k1gMnSc1	Allen
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedl	jíst	k5eAaImAgMnS	jíst
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
NPO	NPO	kA	NPO
ceny	cena	k1gFnSc2	cena
-	-	kIx~	-
nad	nad	k7c4	nad
platy	plat	k1gInPc4	plat
občanů	občan	k1gMnPc2	občan
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
jej	on	k3xPp3gNnSc2	on
objevil	objevit	k5eAaPmAgInS	objevit
Milt	Milt	k1gInSc1	Milt
Kamen	kamna	k1gNnPc2	kamna
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgInS	dát
se	s	k7c7	s
Sidem	Sid	k1gMnSc7	Sid
Caesarem	Caesar	k1gMnSc7	Caesar
první	první	k4xOgFnSc4	první
spisovatelskou	spisovatelský	k2eAgFnSc4d1	spisovatelská
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
si	se	k3xPyFc3	se
říkat	říkat	k5eAaImF	říkat
Woody	Wooda	k1gMnSc2	Wooda
Allen	allen	k1gInSc4	allen
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nadaným	nadaný	k2eAgMnSc7d1	nadaný
bavičem	bavič	k1gMnSc7	bavič
od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
vtipkoval	vtipkovat	k5eAaImAgMnS	vtipkovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
náboženský	náboženský	k2eAgInSc4d1	náboženský
letní	letní	k2eAgInSc4d1	letní
tábor	tábor	k1gInSc4	tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
brutálně	brutálně	k6eAd1	brutálně
bitý	bitý	k2eAgInSc1d1	bitý
dětmi	dítě	k1gFnPc7	dítě
všech	všecek	k3xTgFnPc2	všecek
ras	rasa	k1gFnPc2	rasa
a	a	k8xC	a
vyznání	vyznání	k1gNnPc2	vyznání
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
šel	jít	k5eAaImAgMnS	jít
studovat	studovat	k5eAaImF	studovat
na	na	k7c4	na
Newyorskou	newyorský	k2eAgFnSc4d1	newyorská
univerzitu	univerzita	k1gFnSc4	univerzita
(	(	kIx(	(
<g/>
NYU	NYU	kA	NYU
<g/>
)	)	kIx)	)
komunikaci	komunikace	k1gFnSc4	komunikace
a	a	k8xC	a
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
moc	moc	k6eAd1	moc
snaživým	snaživý	k2eAgMnSc7d1	snaživý
studentem	student	k1gMnSc7	student
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ve	v	k7c6	v
filmovém	filmový	k2eAgInSc6d1	filmový
kurzu	kurz	k1gInSc6	kurz
neuspěl	uspět	k5eNaPmAgMnS	uspět
a	a	k8xC	a
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
byl	být	k5eAaImAgMnS	být
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
také	také	k9	také
krátce	krátce	k6eAd1	krátce
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
City	City	k1gFnSc2	City
College	Colleg	k1gMnSc2	Colleg
of	of	k?	of
New	New	k1gMnSc4	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInPc6	jeho
nezdařených	zdařený	k2eNgInPc6d1	nezdařený
startech	start	k1gInPc6	start
na	na	k7c6	na
NYU	NYU	kA	NYU
a	a	k8xC	a
City	city	k1gNnSc1	city
College	Colleg	k1gInSc2	Colleg
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
spisovatelem	spisovatel	k1gMnSc7	spisovatel
na	na	k7c4	na
plný	plný	k2eAgInSc4d1	plný
úvazek	úvazek	k1gInSc4	úvazek
pro	pro	k7c4	pro
Herba	Herb	k1gMnSc4	Herb
Shrinera	Shriner	k1gMnSc4	Shriner
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
vydělával	vydělávat	k5eAaImAgMnS	vydělávat
$	$	kIx~	$
<g/>
75	[number]	k4	75
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
texty	text	k1gInPc4	text
pro	pro	k7c4	pro
The	The	k1gFnSc4	The
Ed	Ed	k1gMnSc1	Ed
Sullivan	Sullivan	k1gMnSc1	Sullivan
Show	show	k1gFnSc1	show
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Tonight	Tonight	k1gInSc1	Tonight
Show	show	k1gFnSc1	show
<g/>
,	,	kIx,	,
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hour	Hour	k1gInSc1	Hour
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
televizní	televizní	k2eAgInPc1d1	televizní
pořady	pořad	k1gInPc1	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
pro	pro	k7c4	pro
Sida	Sidus	k1gMnSc4	Sidus
Caesara	Caesar	k1gMnSc4	Caesar
vydělával	vydělávat	k5eAaImAgMnS	vydělávat
už	už	k6eAd1	už
$	$	kIx~	$
<g/>
1500	[number]	k4	1500
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Caesarem	Caesar	k1gMnSc7	Caesar
pracoval	pracovat	k5eAaImAgInS	pracovat
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Dannyho	Danny	k1gMnSc2	Danny
Simona	Simon	k1gMnSc2	Simon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
vytvořit	vytvořit	k5eAaPmF	vytvořit
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
spisovatelský	spisovatelský	k2eAgInSc4d1	spisovatelský
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
začal	začít	k5eAaPmAgInS	začít
novou	nový	k2eAgFnSc4d1	nová
kariéru	kariéra	k1gFnSc4	kariéra
jako	jako	k8xS	jako
stand-up	standp	k1gMnSc1	stand-up
komik	komik	k1gMnSc1	komik
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c4	v
Greenwich	Greenwich	k1gInSc4	Greenwich
Village	Villag	k1gFnSc2	Villag
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Duplex	duplex	k1gInSc1	duplex
<g/>
.	.	kIx.	.
</s>
<s>
Ukázky	ukázka	k1gFnPc1	ukázka
jeho	on	k3xPp3gInSc4	on
stand-up	standp	k1gInSc4	stand-up
vystoupení	vystoupení	k1gNnSc2	vystoupení
můžete	moct	k5eAaImIp2nP	moct
slyšet	slyšet	k5eAaImF	slyšet
na	na	k7c6	na
albech	album	k1gNnPc6	album
Standup	Standup	k1gInSc1	Standup
Comic	Comic	k1gMnSc1	Comic
a	a	k8xC	a
Nightclub	Nightclub	k1gMnSc1	Nightclub
Years	Years	k1gInSc4	Years
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
pro	pro	k7c4	pro
populární	populární	k2eAgNnSc4d1	populární
televizní	televizní	k2eAgNnSc4d1	televizní
show	show	k1gNnSc4	show
Skrytá	skrytý	k2eAgFnSc1d1	skrytá
kamera	kamera	k1gFnSc1	kamera
(	(	kIx(	(
<g/>
Candid	Candid	k1gInSc1	Candid
Camera	Camero	k1gNnSc2	Camero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
několika	několik	k4yIc6	několik
dílech	dílo	k1gNnPc6	dílo
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
sám	sám	k3xTgMnSc1	sám
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Allen	Allen	k1gMnSc1	Allen
dokázal	dokázat	k5eAaPmAgMnS	dokázat
změnit	změnit	k5eAaPmF	změnit
svojí	svojit	k5eAaImIp3nP	svojit
slabost	slabost	k1gFnSc4	slabost
v	v	k7c4	v
přednost	přednost	k1gFnSc4	přednost
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
neurotickou	neurotický	k2eAgFnSc4d1	neurotická
<g/>
,	,	kIx,	,
nervózní	nervózní	k2eAgFnSc4d1	nervózní
a	a	k8xC	a
intelektuální	intelektuální	k2eAgFnSc4d1	intelektuální
personu	persona	k1gFnSc4	persona
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
komikem	komik	k1gMnSc7	komik
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
objevoval	objevovat	k5eAaImAgInS	objevovat
v	v	k7c6	v
nočních	noční	k2eAgInPc6d1	noční
podnicích	podnik	k1gInPc6	podnik
a	a	k8xC	a
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
natolik	natolik	k6eAd1	natolik
populární	populární	k2eAgMnSc1d1	populární
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
obálce	obálka	k1gFnSc6	obálka
časopisu	časopis	k1gInSc2	časopis
Life	Lif	k1gInSc2	Lif
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaPmAgInS	věnovat
se	se	k3xPyFc4	se
také	také	k9	také
psaní	psaní	k1gNnSc3	psaní
krátkých	krátký	k2eAgFnPc2d1	krátká
povídek	povídka	k1gFnPc2	povídka
pro	pro	k7c4	pro
časopisy	časopis	k1gInPc4	časopis
(	(	kIx(	(
<g/>
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
The	The	k1gFnPc4	The
New	New	k1gMnSc1	New
Yorker	Yorker	k1gMnSc1	Yorker
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
broadwayským	broadwayský	k2eAgMnSc7d1	broadwayský
dramatikem	dramatik	k1gMnSc7	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
hru	hra	k1gFnSc4	hra
Nepijte	pít	k5eNaImRp2nP	pít
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1966	[number]	k4	1966
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
598	[number]	k4	598
repríz	repríza	k1gFnPc2	repríza
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
hráli	hrát	k5eAaImAgMnP	hrát
Lou	Lou	k1gMnPc1	Lou
Jacobi	Jacobi	k1gNnSc2	Jacobi
<g/>
,	,	kIx,	,
Kay	Kay	k1gMnSc1	Kay
Medford	Medford	k1gMnSc1	Medford
<g/>
,	,	kIx,	,
Anita	Anita	k1gMnSc1	Anita
Gillette	Gillett	k1gInSc5	Gillett
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
budoucí	budoucí	k2eAgMnSc1d1	budoucí
filmový	filmový	k2eAgMnSc1d1	filmový
kolega	kolega	k1gMnSc1	kolega
Anthony	Anthona	k1gFnSc2	Anthona
Roberts	Roberts	k1gInSc1	Roberts
<g/>
.	.	kIx.	.
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
hry	hra	k1gFnSc2	hra
režírovaná	režírovaný	k2eAgFnSc1d1	režírovaná
Howardem	Howard	k1gInSc7	Howard
Morrisem	Morris	k1gInSc7	Morris
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
se	se	k3xPyFc4	se
představil	představit	k5eAaPmAgInS	představit
Jackie	Jackie	k1gFnSc2	Jackie
Gleason	Gleason	k1gInSc1	Gleason
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
režíroval	režírovat	k5eAaImAgInS	režírovat
a	a	k8xC	a
hrál	hrát	k5eAaImAgInS	hrát
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
verzi	verze	k1gFnSc6	verze
pro	pro	k7c4	pro
televizi	televize	k1gFnSc4	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
verzi	verze	k1gFnSc6	verze
se	se	k3xPyFc4	se
také	také	k9	také
objevili	objevit	k5eAaPmAgMnP	objevit
Michael	Michael	k1gMnSc1	Michael
J.	J.	kA	J.
Fox	fox	k1gInSc1	fox
a	a	k8xC	a
Mayim	Mayim	k1gMnSc1	Mayim
Bialik	Bialik	k1gMnSc1	Bialik
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
broadwayský	broadwayský	k2eAgInSc1d1	broadwayský
hit	hit	k1gInSc1	hit
Zahraj	zahrát	k5eAaPmRp2nS	zahrát
to	ten	k3xDgNnSc1	ten
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
Same	Sam	k1gMnSc5	Sam
nejenom	nejenom	k6eAd1	nejenom
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1969	[number]	k4	1969
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
453	[number]	k4	453
představení	představení	k1gNnPc2	představení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
představili	představit	k5eAaPmAgMnP	představit
Diane	Dian	k1gInSc5	Dian
Keaton	Keaton	k1gInSc4	Keaton
a	a	k8xC	a
Anthony	Anthon	k1gInPc4	Anthon
Roberts	Robertsa	k1gFnPc2	Robertsa
<g/>
.	.	kIx.	.
</s>
<s>
Allen	Allen	k1gMnSc1	Allen
<g/>
,	,	kIx,	,
Keaton	Keaton	k1gInSc4	Keaton
a	a	k8xC	a
Roberts	Roberts	k1gInSc4	Roberts
si	se	k3xPyFc3	se
zopakovali	zopakovat	k5eAaPmAgMnP	zopakovat
svoje	svůj	k3xOyFgFnPc4	svůj
role	role	k1gFnPc4	role
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
verzi	verze	k1gFnSc6	verze
hry	hra	k1gFnSc2	hra
režírované	režírovaný	k2eAgFnSc2d1	režírovaná
Herbertem	Herbert	k1gMnSc7	Herbert
Rossem	Ross	k1gMnSc7	Ross
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
také	také	k9	také
o	o	k7c4	o
výborného	výborný	k2eAgMnSc4d1	výborný
autora	autor	k1gMnSc4	autor
krátkých	krátká	k1gFnPc2	krátká
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
publikoval	publikovat	k5eAaBmAgMnS	publikovat
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
knihách	kniha	k1gFnPc6	kniha
-	-	kIx~	-
Vyřídit	vyřídit	k5eAaPmF	vyřídit
si	se	k3xPyFc3	se
účty	účet	k1gInPc4	účet
<g/>
,	,	kIx,	,
Bez	bez	k7c2	bez
peří	peří	k1gNnSc2	peří
<g/>
,	,	kIx,	,
Vedlejší	vedlejší	k2eAgInPc1d1	vedlejší
účinky	účinek	k1gInPc1	účinek
a	a	k8xC	a
Čirá	čirý	k2eAgFnSc1d1	čirá
anarchie	anarchie	k1gFnSc1	anarchie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
rané	raný	k2eAgFnPc1d1	raná
povídky	povídka	k1gFnPc1	povídka
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
ovlivněné	ovlivněný	k2eAgMnPc4d1	ovlivněný
ztřeštěným	ztřeštěný	k2eAgInSc7d1	ztřeštěný
humorem	humor	k1gInSc7	humor
S.	S.	kA	S.
<g/>
J.	J.	kA	J.
Perelmana	Perelman	k1gMnSc2	Perelman
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vášnivým	vášnivý	k2eAgMnSc7d1	vášnivý
fandou	fandou	k?	fandou
jazzu	jazz	k1gInSc3	jazz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
soundtracích	soundtrace	k1gFnPc6	soundtrace
k	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
filmům	film	k1gInPc3	film
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
klarinet	klarinet	k1gInSc4	klarinet
už	už	k6eAd1	už
od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
a	a	k8xC	a
jazz	jazz	k1gInSc1	jazz
ho	on	k3xPp3gMnSc4	on
dokonce	dokonce	k9	dokonce
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
svůj	svůj	k3xOyFgInSc4	svůj
pseudonym	pseudonym	k1gInSc4	pseudonym
podle	podle	k7c2	podle
slavného	slavný	k2eAgMnSc2d1	slavný
klarinetisty	klarinetista	k1gMnSc2	klarinetista
Woodyho	Woody	k1gMnSc2	Woody
Hermana	Herman	k1gMnSc2	Herman
<g/>
.	.	kIx.	.
</s>
<s>
Veřejně	veřejně	k6eAd1	veřejně
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
nejméně	málo	k6eAd3	málo
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
Jazz	jazz	k1gInSc1	jazz
Band	banda	k1gFnPc2	banda
hraje	hrát	k5eAaImIp3nS	hrát
každý	každý	k3xTgInSc1	každý
pondělní	pondělní	k2eAgInSc1d1	pondělní
večer	večer	k1gInSc1	večer
v	v	k7c6	v
manhattanském	manhattanský	k2eAgInSc6d1	manhattanský
hotelu	hotel	k1gInSc6	hotel
Carlyle	Carlyl	k1gInSc5	Carlyl
Hotel	hotel	k1gInSc1	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Hrají	hrát	k5eAaImIp3nP	hrát
klasický	klasický	k2eAgInSc4d1	klasický
neworleanský	worleanský	k2eNgInSc4d1	neworleanský
jazz	jazz	k1gInSc4	jazz
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dokumentární	dokumentární	k2eAgInSc1d1	dokumentární
film	film	k1gInSc1	film
Wild	Wilda	k1gFnPc2	Wilda
Man	mana	k1gFnPc2	mana
Blues	blues	k1gFnSc2	blues
(	(	kIx(	(
<g/>
režírovaný	režírovaný	k2eAgInSc4d1	režírovaný
Barborou	Barbora	k1gFnSc7	Barbora
Koppleovou	Koppleův	k2eAgFnSc7d1	Koppleův
<g/>
)	)	kIx)	)
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
Allenovo	Allenův	k2eAgNnSc4d1	Allenovo
evropské	evropský	k2eAgNnSc4d1	Evropské
turné	turné	k1gNnSc4	turné
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
kapelou	kapela	k1gFnSc7	kapela
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
vydala	vydat	k5eAaPmAgFnS	vydat
dvě	dva	k4xCgFnPc4	dva
CD	CD	kA	CD
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Bunk	Bunk	k1gMnSc1	Bunk
Project	Project	k1gMnSc1	Project
<g/>
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
a	a	k8xC	a
soundrtrack	soundrtrack	k1gInSc1	soundrtrack
k	k	k7c3	k
Wild	Wild	k1gMnSc1	Wild
Man	mana	k1gFnPc2	mana
Blues	blues	k1gFnPc2	blues
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
také	také	k9	také
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
kongresovém	kongresový	k2eAgNnSc6d1	Kongresové
centru	centrum	k1gNnSc6	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Filmografie	filmografie	k1gFnSc2	filmografie
Woodyho	Woody	k1gMnSc2	Woody
Allena	Allen	k1gMnSc2	Allen
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
filmová	filmový	k2eAgFnSc1d1	filmová
tvorba	tvorba	k1gFnSc1	tvorba
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
především	především	k9	především
na	na	k7c4	na
komedie	komedie	k1gFnPc4	komedie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
také	také	k9	také
objevují	objevovat	k5eAaImIp3nP	objevovat
prvky	prvek	k1gInPc1	prvek
parodie	parodie	k1gFnSc2	parodie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
typický	typický	k2eAgInSc1d1	typický
humor	humor	k1gInSc1	humor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
dostává	dostávat	k5eAaImIp3nS	dostávat
až	až	k9	až
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
podbízivosti	podbízivost	k1gFnSc2	podbízivost
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgNnSc7d1	časté
tématem	téma	k1gNnSc7	téma
jeho	jeho	k3xOp3gFnPc2	jeho
parodií	parodie	k1gFnPc2	parodie
je	být	k5eAaImIp3nS	být
komercializace	komercializace	k1gFnSc1	komercializace
sexu	sex	k1gInSc3	sex
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jeho	jeho	k3xOp3gInPc6	jeho
filmech	film	k1gInPc6	film
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
autobiografické	autobiografický	k2eAgInPc4d1	autobiografický
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
hrál	hrát	k5eAaImAgMnS	hrát
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
scenáristou	scenárista	k1gMnSc7	scenárista
filmu	film	k1gInSc2	film
Co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
nového	nový	k2eAgMnSc4d1	nový
<g/>
,	,	kIx,	,
kočičko	kočička	k1gFnSc5	kočička
<g/>
?	?	kIx.	?
</s>
<s>
Jako	jako	k8xC	jako
režisér	režisér	k1gMnSc1	režisér
debutoval	debutovat	k5eAaBmAgMnS	debutovat
nízkorozpočtovým	nízkorozpočtový	k2eAgMnSc7d1	nízkorozpočtový
japonským	japonský	k2eAgInSc7d1	japonský
špionážním	špionážní	k2eAgInSc7d1	špionážní
thrillerem	thriller	k1gInSc7	thriller
What	Whata	k1gFnPc2	Whata
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Up	Up	k1gFnSc7	Up
<g/>
,	,	kIx,	,
Tiger	Tigero	k1gNnPc2	Tigero
Lily	lít	k5eAaImAgInP	lít
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
natočený	natočený	k2eAgInSc1d1	natočený
s	s	k7c7	s
anglickými	anglický	k2eAgInPc7d1	anglický
dialogy	dialog	k1gInPc7	dialog
a	a	k8xC	a
předabovaný	předabovaný	k2eAgInSc4d1	předabovaný
americkými	americký	k2eAgMnPc7d1	americký
herci	herec	k1gMnPc7	herec
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
práce	práce	k1gFnSc1	práce
pro	pro	k7c4	pro
televizi	televize	k1gFnSc4	televize
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
napsal	napsat	k5eAaPmAgMnS	napsat
a	a	k8xC	a
zrežíroval	zrežírovat	k5eAaPmAgMnS	zrežírovat
film	film	k1gInSc4	film
Seber	sebrat	k5eAaPmRp2nS	sebrat
prachy	prach	k1gInPc4	prach
a	a	k8xC	a
vypadni	vypadnout	k5eAaPmRp2nS	vypadnout
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
i	i	k8xC	i
sám	sám	k3xTgInSc4	sám
zahrál	zahrát	k5eAaPmAgInS	zahrát
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
jakousi	jakýsi	k3yIgFnSc7	jakýsi
parodií	parodie	k1gFnSc7	parodie
na	na	k7c4	na
gangsterské	gangsterský	k2eAgInPc4d1	gangsterský
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Banáni	Banán	k1gMnPc1	Banán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
jednu	jeden	k4xCgFnSc4	jeden
malou	malý	k2eAgFnSc4d1	malá
roli	role	k1gFnSc4	role
zahrál	zahrát	k5eAaPmAgMnS	zahrát
i	i	k9	i
Sylvester	Sylvester	k1gMnSc1	Sylvester
Stallone	Stallon	k1gInSc5	Stallon
<g/>
,	,	kIx,	,
Woody	Woodo	k1gNnPc7	Woodo
trefně	trefně	k6eAd1	trefně
využívá	využívat	k5eAaPmIp3nS	využívat
prvky	prvek	k1gInPc4	prvek
politické	politický	k2eAgFnSc2d1	politická
satiry	satira	k1gFnSc2	satira
a	a	k8xC	a
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
bere	brát	k5eAaImIp3nS	brát
na	na	k7c4	na
mušku	muška	k1gFnSc4	muška
"	"	kIx"	"
<g/>
sexuální	sexuální	k2eAgFnSc4d1	sexuální
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
"	"	kIx"	"
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
název	název	k1gInSc1	název
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
téměř	téměř	k6eAd1	téměř
vše	všechen	k3xTgNnSc4	všechen
-	-	kIx~	-
Všechno	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
jste	být	k5eAaImIp2nP	být
kdy	kdy	k6eAd1	kdy
chtěli	chtít	k5eAaImAgMnP	chtít
vědět	vědět	k5eAaImF	vědět
o	o	k7c6	o
sexu	sex	k1gInSc6	sex
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
báli	bát	k5eAaImAgMnP	bát
jste	být	k5eAaImIp2nP	být
se	se	k3xPyFc4	se
zeptat	zeptat	k5eAaPmF	zeptat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
povídkový	povídkový	k2eAgInSc1d1	povídkový
snímek	snímek	k1gInSc1	snímek
je	být	k5eAaImIp3nS	být
stavěný	stavěný	k2eAgInSc1d1	stavěný
především	především	k9	především
na	na	k7c6	na
dobrém	dobrý	k2eAgInSc6d1	dobrý
situačním	situační	k2eAgInSc6d1	situační
humoru	humor	k1gInSc6	humor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bychom	by	kYmCp1nP	by
mohli	moct	k5eAaImAgMnP	moct
odvážně	odvážně	k6eAd1	odvážně
nazvat	nazvat	k5eAaBmF	nazvat
i	i	k9	i
obdobím	období	k1gNnSc7	období
filmů	film	k1gInPc2	film
s	s	k7c7	s
Diane	Dian	k1gInSc5	Dian
Keaton	Keaton	k1gInSc4	Keaton
–	–	k?	–
Woody	Wooda	k1gMnSc2	Wooda
natočil	natočit	k5eAaBmAgMnS	natočit
filmy	film	k1gInPc7	film
Spáč	spáč	k1gMnSc1	spáč
a	a	k8xC	a
Láska	láska	k1gFnSc1	láska
a	a	k8xC	a
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obzvlášť	obzvlášť	k6eAd1	obzvlášť
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
jeho	jeho	k3xOp3gFnPc4	jeho
vysoké	vysoký	k2eAgFnPc4d1	vysoká
ambice	ambice	k1gFnPc4	ambice
a	a	k8xC	a
touhu	touha	k1gFnSc4	touha
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
seriózního	seriózní	k2eAgMnSc4d1	seriózní
filmového	filmový	k2eAgMnSc4d1	filmový
tvůrce	tvůrce	k1gMnSc4	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Annie	Annie	k1gFnSc2	Annie
Hallová	Hallová	k1gFnSc1	Hallová
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
mnoha	mnoho	k4c3	mnoho
kritiky	kritik	k1gMnPc4	kritik
i	i	k9	i
diváky	divák	k1gMnPc4	divák
označovaný	označovaný	k2eAgInSc1d1	označovaný
za	za	k7c4	za
Allenův	Allenův	k2eAgInSc4d1	Allenův
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
čtyři	čtyři	k4xCgMnPc4	čtyři
Oscary	Oscar	k1gMnPc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
Woody	Wooda	k1gFnPc4	Wooda
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgInS	moct
převzít	převzít	k5eAaPmF	převzít
dvě	dva	k4xCgFnPc1	dva
sošky	soška	k1gFnPc1	soška
-	-	kIx~	-
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
a	a	k8xC	a
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
napsal	napsat	k5eAaPmAgInS	napsat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Marshallem	Marshall	k1gMnSc7	Marshall
Brickmanem	Brickman	k1gMnSc7	Brickman
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
mohl	moct	k5eAaImAgMnS	moct
těšit	těšit	k5eAaImF	těšit
z	z	k7c2	z
nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
slavnosti	slavnost	k1gFnSc6	slavnost
se	se	k3xPyFc4	se
však	však	k9	však
neobjevil	objevit	k5eNaPmAgInS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Tematicky	tematicky	k6eAd1	tematicky
navázal	navázat	k5eAaPmAgMnS	navázat
Woody	Wooda	k1gFnPc4	Wooda
na	na	k7c4	na
Annie	Annie	k1gFnPc4	Annie
Hallovou	Hallový	k2eAgFnSc4d1	Hallová
černobílým	černobílý	k2eAgInSc7d1	černobílý
filmem	film	k1gInSc7	film
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
atmosféru	atmosféra	k1gFnSc4	atmosféra
skvěle	skvěle	k6eAd1	skvěle
dotváří	dotvářet	k5eAaImIp3nS	dotvářet
Gerswinova	Gerswinův	k2eAgFnSc1d1	Gerswinův
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
scenáristicky	scenáristicky	k6eAd1	scenáristicky
pracoval	pracovat	k5eAaImAgMnS	pracovat
opět	opět	k6eAd1	opět
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
s	s	k7c7	s
Marshallem	Marshall	k1gMnSc7	Marshall
Brickmanem	Brickman	k1gMnSc7	Brickman
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
osobním	osobní	k2eAgNnSc7d1	osobní
vyznáním	vyznání	k1gNnSc7	vyznání
lásky	láska	k1gFnSc2	láska
Woody	Wooda	k1gFnSc2	Wooda
Allena	Allen	k1gMnSc2	Allen
městu	město	k1gNnSc3	město
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
už	už	k6eAd1	už
"	"	kIx"	"
<g/>
jen	jen	k9	jen
<g/>
"	"	kIx"	"
dvě	dva	k4xCgFnPc1	dva
oscarové	oscarový	k2eAgFnPc1d1	oscarová
nominace	nominace	k1gFnPc1	nominace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
kvalitě	kvalita	k1gFnSc6	kvalita
určitě	určitě	k6eAd1	určitě
neubírá	ubírat	k5eNaImIp3nS	ubírat
<g/>
.	.	kIx.	.
</s>
<s>
Osmdesátá	osmdesátý	k4xOgNnPc4	osmdesátý
léta	léto	k1gNnPc4	léto
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
byla	být	k5eAaImAgFnS	být
plodným	plodný	k2eAgNnSc7d1	plodné
filmovým	filmový	k2eAgNnSc7d1	filmové
obdobím	období	k1gNnSc7	období
-	-	kIx~	-
co	co	k9	co
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
nový	nový	k2eAgInSc4d1	nový
film	film	k1gInSc4	film
-	-	kIx~	-
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
filmů	film	k1gInPc2	film
obsazoval	obsazovat	k5eAaImAgMnS	obsazovat
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
především	především	k9	především
herečku	herečka	k1gFnSc4	herečka
Miu	Miu	k1gMnSc2	Miu
Farrow	Farrow	k1gMnSc2	Farrow
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
navázal	navázat	k5eAaPmAgMnS	navázat
bližší	blízký	k2eAgInSc4d2	bližší
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
trval	trvat	k5eAaImAgInS	trvat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
neskončil	skončit	k5eNaPmAgMnS	skončit
manželstvím	manželství	k1gNnSc7	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
ale	ale	k8xC	ale
adoptoval	adoptovat	k5eAaPmAgMnS	adoptovat
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
i	i	k8xC	i
jedno	jeden	k4xCgNnSc1	jeden
vlastní	vlastní	k2eAgNnSc1d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
točil	točit	k5eAaImAgMnS	točit
komedie	komedie	k1gFnPc4	komedie
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
si	se	k3xPyFc3	se
ale	ale	k9	ale
zachovávaly	zachovávat	k5eAaImAgFnP	zachovávat
i	i	k9	i
filozofický	filozofický	k2eAgInSc4d1	filozofický
podtext	podtext	k1gInSc4	podtext
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
na	na	k7c4	na
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
prach	prach	k1gInSc4	prach
a	a	k8xC	a
September	September	k1gInSc4	September
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
hlavně	hlavně	k9	hlavně
tvorbou	tvorba	k1gFnSc7	tvorba
evropských	evropský	k2eAgMnPc2d1	evropský
filmových	filmový	k2eAgMnPc2d1	filmový
režisérů	režisér	k1gMnPc2	režisér
jako	jako	k8xS	jako
Federico	Federico	k6eAd1	Federico
Fellini	Fellin	k2eAgMnPc1d1	Fellin
a	a	k8xC	a
Ingmar	Ingmar	k1gMnSc1	Ingmar
Bergman	Bergman	k1gMnSc1	Bergman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
natočil	natočit	k5eAaBmAgInS	natočit
film	film	k1gInSc4	film
Stíny	stín	k1gInPc4	stín
a	a	k8xC	a
mlha	mlha	k1gFnSc1	mlha
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
poctou	pocta	k1gFnSc7	pocta
Fritzovi	Fritz	k1gMnSc3	Fritz
Langovi	Lang	k1gMnSc3	Lang
<g/>
,	,	kIx,	,
G.W.	G.W.	k1gMnSc3	G.W.
Pabstovi	Pabsta	k1gMnSc3	Pabsta
a	a	k8xC	a
F.	F.	kA	F.
<g/>
W.	W.	kA	W.
Murnauovi	Murnauův	k2eAgMnPc1d1	Murnauův
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
hold	hold	k1gInSc4	hold
celému	celý	k2eAgInSc3d1	celý
německému	německý	k2eAgInSc3d1	německý
expresionismu	expresionismus	k1gInSc3	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Woody	Wooda	k1gFnSc2	Wooda
věnuje	věnovat	k5eAaPmIp3nS	věnovat
lehčímu	lehký	k2eAgNnSc3d2	lehčí
a	a	k8xC	a
méně	málo	k6eAd2	málo
vážnému	vážný	k2eAgInSc3d1	vážný
žánru	žánr	k1gInSc3	žánr
jako	jako	k8xC	jako
například	například	k6eAd1	například
Všichni	všechen	k3xTgMnPc1	všechen
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
:	:	kIx,	:
Miluju	milovat	k5eAaImIp1nS	milovat
tě	ty	k3xPp2nSc4	ty
nebo	nebo	k8xC	nebo
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
Mocná	mocný	k2eAgFnSc1d1	mocná
Afrodité	Afroditý	k2eAgFnSc3d1	Afroditý
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
představitelka	představitelka	k1gFnSc1	představitelka
Mira	Mira	k1gFnSc1	Mira
Sorvino	Sorvin	k2eAgNnSc1d1	Sorvino
za	za	k7c4	za
vedlejší	vedlejší	k2eAgFnSc4d1	vedlejší
roli	role	k1gFnSc4	role
získala	získat	k5eAaPmAgFnS	získat
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
století	století	k1gNnSc6	století
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
dlužen	dlužen	k2eAgMnSc1d1	dlužen
své	svůj	k3xOyFgFnSc3	svůj
pověsti	pověst	k1gFnSc3	pověst
a	a	k8xC	a
rok	rok	k1gInSc4	rok
co	co	k8xS	co
rok	rok	k1gInSc4	rok
natáčí	natáčet	k5eAaImIp3nP	natáčet
nové	nový	k2eAgInPc1d1	nový
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
dostává	dostávat	k5eAaImIp3nS	dostávat
šanci	šance	k1gFnSc4	šance
i	i	k8xC	i
mladší	mladý	k2eAgFnPc4d2	mladší
generace	generace	k1gFnPc4	generace
herců	herc	k1gInPc2	herc
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Jason	Jason	k1gNnSc1	Jason
Biggs	Biggs	k1gInSc1	Biggs
<g/>
,	,	kIx,	,
Christina	Christina	k1gFnSc1	Christina
Ricci	Ricce	k1gFnSc4	Ricce
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Cokoliv	cokoliv	k3yInSc1	cokoliv
nebo	nebo	k8xC	nebo
Scarlett	Scarlett	k1gMnSc1	Scarlett
Johanssonová	Johanssonová	k1gFnSc1	Johanssonová
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Match	Matcha	k1gFnPc2	Matcha
Point	pointa	k1gFnPc2	pointa
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgMnS	natáčet
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
premiéru	premiéra	k1gFnSc4	premiéra
film	film	k1gInSc4	film
Vicky	Vicka	k1gFnSc2	Vicka
Cristina	Cristina	k1gFnSc1	Cristina
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
dramatu	drama	k1gNnSc6	drama
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
Javier	Javier	k1gMnSc1	Javier
Bardem	bard	k1gMnSc7	bard
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Johanssonové	Johanssonová	k1gFnSc2	Johanssonová
a	a	k8xC	a
Penélope	Penélop	k1gInSc5	Penélop
Cruzové	Cruzová	k1gFnSc3	Cruzová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
za	za	k7c4	za
roli	role	k1gFnSc4	role
Maríe	Maríe	k1gNnSc2	Maríe
Eleny	Elena	k1gFnSc2	Elena
obdržela	obdržet	k5eAaPmAgFnS	obdržet
Oscara	Oscar	k1gMnSc4	Oscar
a	a	k8xC	a
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
herečku	herečka	k1gFnSc4	herečka
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
1971	[number]	k4	1971
Vyřídit	vyřídit	k5eAaPmF	vyřídit
si	se	k3xPyFc3	se
účty	účet	k1gInPc4	účet
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
1975	[number]	k4	1975
Bez	bez	k7c2	bez
peří	peří	k1gNnSc2	peří
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
1980	[number]	k4	1980
Vedlejší	vedlejší	k2eAgInPc1d1	vedlejší
příznaky	příznak	k1gInPc1	příznak
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2003	[number]	k4	2003
2007	[number]	k4	2007
Čirá	čirý	k2eAgFnSc1d1	čirá
anarchie	anarchie	k1gFnSc1	anarchie
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
1966	[number]	k4	1966
Nepijte	pít	k5eNaImRp2nP	pít
vodu	voda	k1gFnSc4	voda
1969	[number]	k4	1969
Zahraj	zahrát	k5eAaPmRp2nS	zahrát
to	ten	k3xDgNnSc1	ten
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
Same	Sam	k1gMnSc5	Sam
1975	[number]	k4	1975
Bůh	bůh	k1gMnSc1	bůh
–	–	k?	–
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
1975	[number]	k4	1975
Smrt	smrt	k1gFnSc1	smrt
–	–	k?	–
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
1982	[number]	k4	1982
The	The	k1gMnPc2	The
Floating	Floating	k1gInSc4	Floating
Light	Lighta	k1gFnPc2	Lighta
Bulb	Bulba	k1gFnPc2	Bulba
1982	[number]	k4	1982
Central	Central	k1gFnPc2	Central
Park	park	k1gInSc1	park
West	West	k1gMnSc1	West
–	–	k?	–
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
1995	[number]	k4	1995
Old	Olda	k1gFnPc2	Olda
Sybrook	Sybrook	k1gInSc1	Sybrook
–	–	k?	–
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
1995	[number]	k4	1995
Riverside	Riversid	k1gInSc5	Riversid
Drive	drive	k1gInSc1	drive
–	–	k?	–
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
2004	[number]	k4	2004
A	A	kA	A
Second	Seconda	k1gFnPc2	Seconda
Hand	Handa	k1gFnPc2	Handa
Memory	Memora	k1gFnSc2	Memora
</s>
