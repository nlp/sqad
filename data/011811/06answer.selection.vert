<s>
Maďarština	maďarština	k1gFnSc1	maďarština
je	být	k5eAaImIp3nS	být
ugrofinský	ugrofinský	k2eAgInSc4d1	ugrofinský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
částech	část	k1gFnPc6	část
srbské	srbský	k2eAgFnSc2d1	Srbská
Vojvodiny	Vojvodina	k1gFnSc2	Vojvodina
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
Zakarpatské	zakarpatský	k2eAgFnSc2d1	Zakarpatská
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
částech	část	k1gFnPc6	část
rakouské	rakouský	k2eAgFnSc2d1	rakouská
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Burgenlandu	Burgenland	k1gInSc2	Burgenland
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
a	a	k8xC	a
v	v	k7c6	v
částech	část	k1gFnPc6	část
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
