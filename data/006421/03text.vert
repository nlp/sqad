<s>
Pavel	Pavel	k1gMnSc1	Pavel
Vondruška	Vondruška	k1gMnSc1	Vondruška
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1925	[number]	k4	1925
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
člen	člen	k1gInSc1	člen
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
též	též	k9	též
v	v	k7c6	v
několika	několik	k4yIc6	několik
desítkách	desítka	k1gFnPc6	desítka
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Ovládal	ovládat	k5eAaImAgMnS	ovládat
několik	několik	k4yIc4	několik
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
,	,	kIx,	,
italštinu	italština	k1gFnSc4	italština
<g/>
,	,	kIx,	,
španělštinu	španělština	k1gFnSc4	španělština
<g/>
,	,	kIx,	,
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
ruštinu	ruština	k1gFnSc4	ruština
<g/>
,	,	kIx,	,
němčinu	němčina	k1gFnSc4	němčina
<g/>
,	,	kIx,	,
srbochorvatštinu	srbochorvatština	k1gFnSc4	srbochorvatština
<g/>
,	,	kIx,	,
latinu	latina	k1gFnSc4	latina
a	a	k8xC	a
esperanto	esperanto	k1gNnSc4	esperanto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
a	a	k8xC	a
následně	následně	k6eAd1	následně
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
studiem	studio	k1gNnSc7	studio
dirigování	dirigování	k1gNnPc2	dirigování
<g/>
,	,	kIx,	,
operní	operní	k2eAgFnSc1d1	operní
režie	režie	k1gFnSc1	režie
a	a	k8xC	a
dramaturgie	dramaturgie	k1gFnSc1	dramaturgie
na	na	k7c6	na
Hudební	hudební	k2eAgFnSc6d1	hudební
akademii	akademie	k1gFnSc6	akademie
AMU	AMU	kA	AMU
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
–	–	k?	–
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
vyučovali	vyučovat	k5eAaImAgMnP	vyučovat
například	například	k6eAd1	například
Karel	Karel	k1gMnSc1	Karel
Ančerl	Ančerl	k1gMnSc1	Ančerl
či	či	k8xC	či
Václav	Václav	k1gMnSc1	Václav
Talich	Talich	k1gMnSc1	Talich
<g/>
.	.	kIx.	.
</s>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
u	u	k7c2	u
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Pujmana	Pujman	k1gMnSc2	Pujman
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
studiích	studio	k1gNnPc6	studio
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
korepetitor	korepetitor	k1gMnSc1	korepetitor
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
a	a	k8xC	a
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
Pražskou	pražský	k2eAgFnSc4d1	Pražská
zpěvohru	zpěvohra	k1gFnSc4	zpěvohra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1951	[number]	k4	1951
až	až	k9	až
1977	[number]	k4	1977
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
dirigentem	dirigent	k1gMnSc7	dirigent
<g/>
:	:	kIx,	:
symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
AUSu	AUSa	k1gFnSc4	AUSa
Moravské	moravský	k2eAgFnSc2d1	Moravská
filharmonie	filharmonie	k1gFnSc2	filharmonie
Olomouc	Olomouc	k1gFnSc1	Olomouc
opery	opera	k1gFnSc2	opera
Státního	státní	k2eAgNnSc2d1	státní
divadla	divadlo	k1gNnSc2	divadlo
Ostrava	Ostrava	k1gFnSc1	Ostrava
opery	opera	k1gFnSc2	opera
Hudebního	hudební	k2eAgNnSc2d1	hudební
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
(	(	kIx(	(
<g/>
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
souběžně	souběžně	k6eAd1	souběžně
i	i	k8xC	i
korepetitor	korepetitor	k1gMnSc1	korepetitor
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
)	)	kIx)	)
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1977	[number]	k4	1977
a	a	k8xC	a
2009	[number]	k4	2009
působil	působit	k5eAaImAgInS	působit
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
orchestr	orchestr	k1gInSc4	orchestr
během	během	k7c2	během
představení	představení	k1gNnSc2	představení
činohry	činohra	k1gFnSc2	činohra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1977	[number]	k4	1977
až	až	k9	až
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
i	i	k9	i
vedoucím	vedoucí	k1gMnSc7	vedoucí
tohoto	tento	k3xDgInSc2	tento
divadelního	divadelní	k2eAgInSc2d1	divadelní
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
též	též	k9	též
jako	jako	k9	jako
autor	autor	k1gMnSc1	autor
a	a	k8xC	a
upravovatel	upravovatel	k1gMnSc1	upravovatel
scénické	scénický	k2eAgFnSc2d1	scénická
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
několika	několik	k4yIc3	několik
inscenacím	inscenace	k1gFnPc3	inscenace
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
k	k	k7c3	k
Paličově	paličův	k2eAgFnSc3d1	Paličova
dceři	dcera	k1gFnSc3	dcera
Josefa	Josef	k1gMnSc2	Josef
Kajetána	Kajetán	k1gMnSc2	Kajetán
Tyla	Tyl	k1gMnSc2	Tyl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
na	na	k7c6	na
prknech	prkno	k1gNnPc6	prkno
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
jako	jako	k9	jako
herec	herec	k1gMnSc1	herec
v	v	k7c6	v
epizodních	epizodní	k2eAgFnPc6d1	epizodní
rolích	role	k1gFnPc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
působení	působení	k1gNnSc4	působení
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
ukončil	ukončit	k5eAaPmAgInS	ukončit
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc3	červenec
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
souboru	soubor	k1gInSc2	soubor
divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
nedobrovolně	dobrovolně	k6eNd1	dobrovolně
odešel	odejít	k5eAaPmAgMnS	odejít
kvůli	kvůli	k7c3	kvůli
zhoršující	zhoršující	k2eAgFnSc3d1	zhoršující
se	se	k3xPyFc4	se
paměti	paměť	k1gFnSc3	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Hostoval	hostovat	k5eAaImAgMnS	hostovat
také	také	k9	také
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Na	na	k7c6	na
Jezerce	Jezerka	k1gFnSc6	Jezerka
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
se	se	k3xPyFc4	se
vážně	vážně	k6eAd1	vážně
zranil	zranit	k5eAaPmAgMnS	zranit
při	při	k7c6	při
pádu	pád	k1gInSc6	pád
do	do	k7c2	do
orchestřiště	orchestřiště	k1gNnSc2	orchestřiště
Stavovského	stavovský	k2eAgNnSc2d1	Stavovské
divadla	divadlo	k1gNnSc2	divadlo
při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
hospitalizován	hospitalizovat	k5eAaBmNgInS	hospitalizovat
v	v	k7c6	v
Motolské	motolský	k2eAgFnSc6d1	motolská
nemocnici	nemocnice	k1gFnSc6	nemocnice
a	a	k8xC	a
udržován	udržovat	k5eAaImNgInS	udržovat
v	v	k7c6	v
umělém	umělý	k2eAgInSc6d1	umělý
spánku	spánek	k1gInSc6	spánek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
večer	večer	k6eAd1	večer
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
nemocnici	nemocnice	k1gFnSc6	nemocnice
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
