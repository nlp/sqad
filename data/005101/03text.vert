<s>
Glukóza	glukóza	k1gFnSc1	glukóza
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
běžně	běžně	k6eAd1	běžně
glukosa	glukosa	k1gFnSc1	glukosa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
γ	γ	k?	γ
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
glykys	glykys	k1gInSc1	glykys
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
sladký	sladký	k2eAgInSc4d1	sladký
nebo	nebo	k8xC	nebo
γ	γ	k?	γ
<g/>
,	,	kIx,	,
gleukos	gleukos	k1gInSc1	gleukos
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
mošt	mošt	k1gInSc4	mošt
<g/>
,	,	kIx,	,
sladké	sladký	k2eAgNnSc4d1	sladké
víno	víno	k1gNnSc4	víno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
řeči	řeč	k1gFnSc6	řeč
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
hroznový	hroznový	k2eAgInSc4d1	hroznový
cukr	cukr	k1gInSc4	cukr
nebo	nebo	k8xC	nebo
krevní	krevní	k2eAgInSc4d1	krevní
cukr	cukr	k1gInSc4	cukr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
monosacharidů	monosacharid	k1gInPc2	monosacharid
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
aldohexóz	aldohexóza	k1gFnPc2	aldohexóza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemických	chemický	k2eAgInPc6d1	chemický
vzorcích	vzorec	k1gInPc6	vzorec
oligosacharidů	oligosacharid	k1gInPc2	oligosacharid
a	a	k8xC	a
polysacharidů	polysacharid	k1gInPc2	polysacharid
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
symbolem	symbol	k1gInSc7	symbol
Glc	Glc	k1gFnSc2	Glc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
glukosa	glukosa	k1gFnSc1	glukosa
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
sladké	sladký	k2eAgFnSc2d1	sladká
chuti	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
běžná	běžný	k2eAgFnSc1d1	běžná
konformace	konformace	k1gFnSc1	konformace
glukózy	glukóza	k1gFnSc2	glukóza
je	být	k5eAaImIp3nS	být
D-glukóza	Dlukóza	k1gFnSc1	D-glukóza
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stáčí	stáčet	k5eAaImIp3nS	stáčet
polarizované	polarizovaný	k2eAgNnSc4d1	polarizované
světlo	světlo	k1gNnSc4	světlo
doprava	doprava	k6eAd1	doprava
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
její	její	k3xOp3gNnSc1	její
staré	starý	k2eAgNnSc1d1	staré
označení	označení	k1gNnSc1	označení
dextróza	dextróza	k1gFnSc1	dextróza
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
doposud	doposud	k6eAd1	doposud
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
lékařské	lékařský	k2eAgFnSc6d1	lékařská
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
D-glukóza	Dlukóza	k1gFnSc1	D-glukóza
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
připravit	připravit	k5eAaPmF	připravit
krystalizací	krystalizace	k1gFnSc7	krystalizace
z	z	k7c2	z
rostlinných	rostlinný	k2eAgFnPc2d1	rostlinná
šťáv	šťáva	k1gFnPc2	šťáva
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
vinné	vinný	k2eAgFnSc2d1	vinná
révy	réva	k1gFnSc2	réva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavní	hlavní	k2eAgFnSc7d1	hlavní
metodou	metoda	k1gFnSc7	metoda
její	její	k3xOp3gFnSc2	její
výroby	výroba	k1gFnSc2	výroba
je	být	k5eAaImIp3nS	být
kyselá	kyselý	k2eAgFnSc1d1	kyselá
nebo	nebo	k8xC	nebo
enzymatická	enzymatický	k2eAgFnSc1d1	enzymatická
hydrolýza	hydrolýza	k1gFnSc1	hydrolýza
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
škrobu	škrob	k1gInSc2	škrob
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
především	především	k9	především
bramborového	bramborový	k2eAgNnSc2d1	bramborové
<g/>
.	.	kIx.	.
</s>
<s>
Glukóza	glukóza	k1gFnSc1	glukóza
existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
enantiomerech	enantiomer	k1gInPc6	enantiomer
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
D-glukóza	Dlukóza	k1gFnSc1	D-glukóza
(	(	kIx(	(
<g/>
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
též	též	k9	též
dextróza	dextróza	k1gFnSc1	dextróza
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
zrcadlový	zrcadlový	k2eAgInSc1d1	zrcadlový
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
L-glukóza	Llukóza	k1gFnSc1	L-glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
chemickými	chemický	k2eAgInPc7d1	chemický
a	a	k8xC	a
fyzikálními	fyzikální	k2eAgFnPc7d1	fyzikální
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
stáčí	stáčet	k5eAaImIp3nS	stáčet
rovinu	rovina	k1gFnSc4	rovina
polarizovaného	polarizovaný	k2eAgNnSc2d1	polarizované
světla	světlo	k1gNnSc2	světlo
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
živých	živý	k2eAgInPc6d1	živý
organismech	organismus	k1gInPc6	organismus
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
nezaměnitelné	zaměnitelný	k2eNgFnPc1d1	nezaměnitelná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
L-glukóza	Llukóza	k1gFnSc1	L-glukóza
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
fosforylována	fosforylován	k2eAgFnSc1d1	fosforylován
hexokinázou	hexokináza	k1gFnSc7	hexokináza
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
tedy	tedy	k9	tedy
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
metabolismu	metabolismus	k1gInSc2	metabolismus
cukrů	cukr	k1gInPc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
bakterie	bakterie	k1gFnPc1	bakterie
ovšem	ovšem	k9	ovšem
mají	mít	k5eAaImIp3nP	mít
enzymy	enzym	k1gInPc1	enzym
schopné	schopný	k2eAgInPc1d1	schopný
L-glukózu	Llukóza	k1gFnSc4	L-glukóza
zpracovat	zpracovat	k5eAaPmF	zpracovat
<g/>
.	.	kIx.	.
</s>
<s>
L-glukóza	Llukóza	k1gFnSc1	L-glukóza
chutná	chutnat	k5eAaImIp3nS	chutnat
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
D-glukóza	Dlukóza	k1gFnSc1	D-glukóza
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
jako	jako	k8xC	jako
umělé	umělý	k2eAgNnSc1d1	umělé
sladidlo	sladidlo	k1gNnSc1	sladidlo
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
vysokým	vysoký	k2eAgInPc3d1	vysoký
nákladům	náklad	k1gInPc3	náklad
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
ovšem	ovšem	k9	ovšem
nebyla	být	k5eNaImAgFnS	být
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
přítomnosti	přítomnost	k1gFnSc3	přítomnost
aldehydické	aldehydický	k2eAgFnSc2d1	aldehydická
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nS	patřit
glukóza	glukóza	k1gFnSc1	glukóza
k	k	k7c3	k
redukujícím	redukující	k2eAgInPc3d1	redukující
sacharidům	sacharid	k1gInPc3	sacharid
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
např.	např.	kA	např.
redukuje	redukovat	k5eAaBmIp3nS	redukovat
soli	sůl	k1gFnPc4	sůl
dvojmocné	dvojmocný	k2eAgFnSc2d1	dvojmocná
mědi	měď	k1gFnSc2	měď
na	na	k7c4	na
jednomocné	jednomocný	k2eAgNnSc4d1	jednomocný
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
užívalo	užívat	k5eAaImAgNnS	užívat
ke	k	k7c3	k
kvalitativnímu	kvalitativní	k2eAgNnSc3d1	kvalitativní
i	i	k8xC	i
kvantitativnímu	kvantitativní	k2eAgNnSc3d1	kvantitativní
stanovení	stanovení	k1gNnSc3	stanovení
cukru	cukr	k1gInSc2	cukr
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
moči	moč	k1gFnSc6	moč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
aldehydická	aldehydický	k2eAgFnSc1d1	aldehydická
skupina	skupina	k1gFnSc1	skupina
D-glukózy	Dlukóza	k1gFnSc2	D-glukóza
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
karboxylovou	karboxylový	k2eAgFnSc4d1	karboxylová
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
kyselina	kyselina	k1gFnSc1	kyselina
D-glukonová	Dlukonová	k1gFnSc1	D-glukonová
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
silnějších	silný	k2eAgFnPc2d2	silnější
oxidačních	oxidační	k2eAgFnPc2d1	oxidační
činidel	činidlo	k1gNnPc2	činidlo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
kyseliny	kyselina	k1gFnPc4	kyselina
dusičné	dusičný	k2eAgFnPc4d1	dusičná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
rovněž	rovněž	k9	rovněž
primární	primární	k2eAgFnSc1d1	primární
alkoholická	alkoholický	k2eAgFnSc1d1	alkoholická
skupina	skupina	k1gFnSc1	skupina
na	na	k7c6	na
opačném	opačný	k2eAgInSc6d1	opačný
konci	konec	k1gInSc6	konec
molekuly	molekula	k1gFnSc2	molekula
též	též	k9	též
na	na	k7c4	na
karboxylovou	karboxylový	k2eAgFnSc4d1	karboxylová
skupinu	skupina	k1gFnSc4	skupina
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
kyseliny	kyselina	k1gFnSc2	kyselina
D-glukarové	Dlukarová	k1gFnSc2	D-glukarová
<g/>
.	.	kIx.	.
</s>
<s>
Zoxiduje	zoxidovat	k5eAaPmIp3nS	zoxidovat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
primární	primární	k2eAgFnSc1d1	primární
alkoholická	alkoholický	k2eAgFnSc1d1	alkoholická
skupina	skupina	k1gFnSc1	skupina
na	na	k7c6	na
posledním	poslední	k2eAgInSc6d1	poslední
uhlíku	uhlík	k1gInSc6	uhlík
(	(	kIx(	(
<g/>
s	s	k7c7	s
lokačním	lokační	k2eAgNnSc7d1	lokační
číslem	číslo	k1gNnSc7	číslo
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
kyselina	kyselina	k1gFnSc1	kyselina
D-glukuronová	Dlukuronová	k1gFnSc1	D-glukuronová
<g/>
.	.	kIx.	.
</s>
<s>
Redukcí	redukce	k1gFnSc7	redukce
glukosy	glukosa	k1gFnSc2	glukosa
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hydrogenací	hydrogenace	k1gFnSc7	hydrogenace
vodíkem	vodík	k1gInSc7	vodík
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
redukuje	redukovat	k5eAaBmIp3nS	redukovat
aldehydická	aldehydický	k2eAgFnSc1d1	aldehydická
skupina	skupina	k1gFnSc1	skupina
na	na	k7c4	na
primární	primární	k2eAgFnSc4d1	primární
alkoholickou	alkoholický	k2eAgFnSc4d1	alkoholická
skupinu	skupina	k1gFnSc4	skupina
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
D-glucitol	Dlucitol	k1gInSc1	D-glucitol
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnSc1d2	známější
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
sorbitol	sorbitol	k1gInSc1	sorbitol
(	(	kIx(	(
<g/>
sorbit	sorbit	k1gInSc1	sorbit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc4d1	používaný
jako	jako	k8xS	jako
umělé	umělý	k2eAgNnSc4d1	umělé
sladidlo	sladidlo	k1gNnSc4	sladidlo
pro	pro	k7c4	pro
diabetiky	diabetik	k1gMnPc4	diabetik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
přechází	přecházet	k5eAaImIp3nS	přecházet
glukóza	glukóza	k1gFnSc1	glukóza
do	do	k7c2	do
cyklické	cyklický	k2eAgFnSc2d1	cyklická
hemiacetalové	hemiacetal	k1gMnPc1	hemiacetal
formy	forma	k1gFnSc2	forma
s	s	k7c7	s
šestičlenným	šestičlenný	k2eAgInSc7d1	šestičlenný
kruhem	kruh	k1gInSc7	kruh
(	(	kIx(	(
<g/>
pyranóza	pyranóza	k1gFnSc1	pyranóza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
rovnovážném	rovnovážný	k2eAgInSc6d1	rovnovážný
stavu	stav	k1gInSc6	stav
za	za	k7c2	za
laboratorní	laboratorní	k2eAgFnSc2d1	laboratorní
teploty	teplota	k1gFnSc2	teplota
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dva	dva	k4xCgMnPc4	dva
anomery	anomer	k1gMnPc4	anomer
<g/>
,	,	kIx,	,
lišící	lišící	k2eAgFnPc1d1	lišící
se	se	k3xPyFc4	se
orientací	orientace	k1gFnSc7	orientace
hemiacetalového	hemiacetalový	k2eAgInSc2d1	hemiacetalový
hydroxylu	hydroxyl	k1gInSc2	hydroxyl
<g/>
:	:	kIx,	:
36	[number]	k4	36
%	%	kIx~	%
α	α	k?	α
a	a	k8xC	a
64	[number]	k4	64
%	%	kIx~	%
β	β	k?	β
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
anomery	anomer	k1gInPc1	anomer
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dynamické	dynamický	k2eAgFnSc6d1	dynamická
rovnováze	rovnováha	k1gFnSc6	rovnováha
a	a	k8xC	a
procesem	proces	k1gInSc7	proces
mutarotace	mutarotace	k1gFnSc2	mutarotace
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
předchozí	předchozí	k2eAgFnSc4d1	předchozí
reakční	reakční	k2eAgNnSc4d1	reakční
schema	schema	k1gNnSc4	schema
<g/>
)	)	kIx)	)
přecházejí	přecházet	k5eAaImIp3nP	přecházet
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
řádu	řád	k1gInSc2	řád
hodin	hodina	k1gFnPc2	hodina
jeden	jeden	k4xCgInSc1	jeden
v	v	k7c6	v
druhý	druhý	k4xOgInSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
prostorové	prostorový	k2eAgNnSc4d1	prostorové
uspořádání	uspořádání	k1gNnSc4	uspořádání
šestičlenného	šestičlenný	k2eAgInSc2d1	šestičlenný
heterocyklu	heterocykl	k1gInSc2	heterocykl
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
židličkové	židličkové	k2eAgFnSc4d1	židličkové
konformaci	konformace	k1gFnSc4	konformace
(	(	kIx(	(
<g/>
Z-forma	Zorma	k1gFnSc1	Z-forma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čisté	čistý	k2eAgFnPc1d1	čistá
anomery	anomera	k1gFnPc1	anomera
v	v	k7c6	v
pevné	pevný	k2eAgFnSc6d1	pevná
fázi	fáze	k1gFnSc6	fáze
můžeme	moct	k5eAaImIp1nP	moct
získat	získat	k5eAaPmF	získat
krystalizaci	krystalizace	k1gFnSc4	krystalizace
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
krystalizaci	krystalizace	k1gFnSc6	krystalizace
z	z	k7c2	z
methanolu	methanol	k1gInSc2	methanol
se	se	k3xPyFc4	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
krystaly	krystal	k1gInPc1	krystal
α	α	k?	α
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
při	při	k7c6	při
krystalizaci	krystalizace	k1gFnSc6	krystalizace
z	z	k7c2	z
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgInPc1d1	octový
krystaly	krystal	k1gInPc1	krystal
β	β	k?	β
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
krystalizaci	krystalizace	k1gFnSc6	krystalizace
z	z	k7c2	z
vodného	vodné	k1gNnSc2	vodné
roztoku	roztok	k1gInSc2	roztok
obvykle	obvykle	k6eAd1	obvykle
dostáváme	dostávat	k5eAaImIp1nP	dostávat
monohydrát	monohydrát	k1gInSc4	monohydrát
D-glukózy	Dlukóza	k1gFnSc2	D-glukóza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zelených	zelený	k2eAgFnPc6d1	zelená
rostlinách	rostlina	k1gFnPc6	rostlina
vzniká	vznikat	k5eAaImIp3nS	vznikat
procesem	proces	k1gInSc7	proces
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
vody	voda	k1gFnPc1	voda
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
a	a	k8xC	a
dodávky	dodávka	k1gFnSc2	dodávka
energie	energie	k1gFnSc2	energie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
světelných	světelný	k2eAgNnPc2d1	světelné
kvant	kvantum	k1gNnPc2	kvantum
podle	podle	k7c2	podle
sumární	sumární	k2eAgFnSc2d1	sumární
rovnice	rovnice	k1gFnSc2	rovnice
6	[number]	k4	6
CO2	CO2	k1gFnPc2	CO2
+	+	kIx~	+
12	[number]	k4	12
H2O	H2O	k1gFnPc2	H2O
+	+	kIx~	+
hν	hν	k?	hν
→	→	k?	→
C6H12O6	C6H12O6	k1gFnSc2	C6H12O6
+	+	kIx~	+
6	[number]	k4	6
O2	O2	k1gFnPc2	O2
+	+	kIx~	+
6	[number]	k4	6
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vzniká	vznikat	k5eAaImIp3nS	vznikat
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
opačným	opačný	k2eAgInSc7d1	opačný
procesem	proces	k1gInSc7	proces
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
organismy	organismus	k1gInPc4	organismus
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
odbourávají	odbourávat	k5eAaImIp3nP	odbourávat
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
spalují	spalovat	k5eAaImIp3nP	spalovat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
D-glukózu	Dlukóza	k1gFnSc4	D-glukóza
na	na	k7c4	na
původní	původní	k2eAgFnPc4d1	původní
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
získávají	získávat	k5eAaImIp3nP	získávat
tak	tak	k6eAd1	tak
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
o	o	k7c6	o
chemických	chemický	k2eAgFnPc6d1	chemická
vlastnostech	vlastnost	k1gFnPc6	vlastnost
a	a	k8xC	a
reakcích	reakce	k1gFnPc6	reakce
viz	vidět	k5eAaImRp2nS	vidět
též	též	k6eAd1	též
heslo	heslo	k1gNnSc4	heslo
aldóza	aldóza	k1gFnSc1	aldóza
<g/>
.	.	kIx.	.
</s>
<s>
Čistá	čistý	k2eAgFnSc1d1	čistá
D-glukóza	Dlukóza	k1gFnSc1	D-glukóza
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
rostlinách	rostlina	k1gFnPc6	rostlina
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
produktů	produkt	k1gInPc2	produkt
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
pro	pro	k7c4	pro
rostliny	rostlina	k1gFnPc4	rostlina
zásobu	zásoba	k1gFnSc4	zásoba
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Hromadí	hromadit	k5eAaImIp3nS	hromadit
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
plodech	plod	k1gInPc6	plod
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgMnSc2	ten
je	být	k5eAaImIp3nS	být
podjednotkou	podjednotka	k1gFnSc7	podjednotka
řady	řada	k1gFnSc2	řada
přírodních	přírodní	k2eAgInPc2d1	přírodní
oligosacharidů	oligosacharid	k1gInPc2	oligosacharid
<g/>
,	,	kIx,	,
např.	např.	kA	např.
maltózy	maltóza	k1gFnPc1	maltóza
<g/>
,	,	kIx,	,
sacharózy	sacharóza	k1gFnPc1	sacharóza
<g/>
,	,	kIx,	,
laktózy	laktóza	k1gFnPc1	laktóza
aj.	aj.	kA	aj.
a	a	k8xC	a
polysacharidů	polysacharid	k1gInPc2	polysacharid
<g/>
,	,	kIx,	,
např.	např.	kA	např.
škrobu	škrob	k1gInSc2	škrob
nebo	nebo	k8xC	nebo
glykogenu	glykogen	k1gInSc2	glykogen
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
glukózové	glukózový	k2eAgInPc1d1	glukózový
sacharidy	sacharid	k1gInPc1	sacharid
nazývají	nazývat	k5eAaImIp3nP	nazývat
glukany	glukana	k1gFnPc4	glukana
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
mnoha	mnoho	k4c2	mnoho
heteroglykosidů	heteroglykosid	k1gMnPc2	heteroglykosid
<g/>
,	,	kIx,	,
vyskytujících	vyskytující	k2eAgMnPc2d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
rostlinách	rostlina	k1gFnPc6	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
přítomna	přítomen	k2eAgFnSc1d1	přítomna
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
živočišných	živočišný	k2eAgInPc6d1	živočišný
produktech	produkt	k1gInPc6	produkt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
medu	med	k1gInSc6	med
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
s	s	k7c7	s
cukrovkou	cukrovka	k1gFnSc7	cukrovka
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
moči	moč	k1gFnSc6	moč
<g/>
.	.	kIx.	.
</s>
<s>
D-Glukóza	D-Glukóza	k1gFnSc1	D-Glukóza
je	být	k5eAaImIp3nS	být
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
pro	pro	k7c4	pro
fungování	fungování	k1gNnSc4	fungování
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
a	a	k8xC	a
nejrychlejším	rychlý	k2eAgInSc7d3	nejrychlejší
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
tělesné	tělesný	k2eAgFnPc4d1	tělesná
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
lidské	lidský	k2eAgFnPc4d1	lidská
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
buňky	buňka	k1gFnPc4	buňka
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
červené	červený	k2eAgFnSc2d1	červená
krvinky	krvinka	k1gFnSc2	krvinka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
glukóza	glukóza	k1gFnSc1	glukóza
jediným	jediný	k2eAgInSc7d1	jediný
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
neobejdou	obejít	k5eNaPmIp3nP	obejít
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
tkáně	tkáň	k1gFnPc1	tkáň
spotřebují	spotřebovat	k5eAaPmIp3nP	spotřebovat
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
g	g	kA	g
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
příjem	příjem	k1gInSc1	příjem
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
nižší	nízký	k2eAgMnSc1d2	nižší
nebo	nebo	k8xC	nebo
nerovnoměrný	rovnoměrný	k2eNgMnSc1d1	nerovnoměrný
<g/>
,	,	kIx,	,
chybějící	chybějící	k2eAgInSc1d1	chybějící
glukózu	glukóza	k1gFnSc4	glukóza
získává	získávat	k5eAaImIp3nS	získávat
organizmus	organizmus	k1gInSc4	organizmus
štěpením	štěpení	k1gNnSc7	štěpení
zásobního	zásobní	k2eAgInSc2d1	zásobní
glykogenu	glykogen	k1gInSc2	glykogen
procesem	proces	k1gInSc7	proces
glykogenolýzy	glykogenolýza	k1gFnPc4	glykogenolýza
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poklesu	pokles	k1gInSc6	pokles
hladiny	hladina	k1gFnSc2	hladina
glukózy	glukóza	k1gFnSc2	glukóza
se	se	k3xPyFc4	se
ze	z	k7c2	z
žláz	žláza	k1gFnPc2	žláza
s	s	k7c7	s
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
sekrecí	sekrece	k1gFnSc7	sekrece
(	(	kIx(	(
<g/>
z	z	k7c2	z
pankreatu	pankreas	k1gInSc2	pankreas
a	a	k8xC	a
z	z	k7c2	z
nadledvin	nadledvina	k1gFnPc2	nadledvina
<g/>
)	)	kIx)	)
začnou	začít	k5eAaPmIp3nP	začít
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
kontraregulační	kontraregulační	k2eAgInPc1d1	kontraregulační
hormony	hormon	k1gInPc1	hormon
(	(	kIx(	(
<g/>
glukagon	glukagon	k1gInSc1	glukagon
a	a	k8xC	a
adrenalin	adrenalin	k1gInSc1	adrenalin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mobilizují	mobilizovat	k5eAaBmIp3nP	mobilizovat
její	její	k3xOp3gFnSc4	její
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Glykogen	glykogen	k1gInSc1	glykogen
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
především	především	k9	především
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
svalstvu	svalstvo	k1gNnSc6	svalstvo
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
od	od	k7c2	od
150	[number]	k4	150
do	do	k7c2	do
400	[number]	k4	400
g	g	kA	g
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
organizmus	organizmus	k1gInSc1	organizmus
nalézá	nalézat	k5eAaImIp3nS	nalézat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
zásoby	zásoba	k1gFnPc1	zásoba
glykogenu	glykogen	k1gInSc2	glykogen
vyčerpány	vyčerpat	k5eAaPmNgFnP	vyčerpat
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
si	se	k3xPyFc3	se
organizmus	organizmus	k1gInSc4	organizmus
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
množství	množství	k1gNnSc1	množství
glukózy	glukóza	k1gFnSc2	glukóza
přeměnou	přeměna	k1gFnSc7	přeměna
tělesného	tělesný	k2eAgInSc2d1	tělesný
tuku	tuk	k1gInSc2	tuk
pomocí	pomocí	k7c2	pomocí
glukoneogeneze	glukoneogeneze	k1gFnSc2	glukoneogeneze
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stresu	stres	k1gInSc6	stres
a	a	k8xC	a
při	při	k7c6	při
delším	dlouhý	k2eAgNnSc6d2	delší
hladovění	hladovění	k1gNnSc6	hladovění
se	se	k3xPyFc4	se
jako	jako	k9	jako
zdroj	zdroj	k1gInSc1	zdroj
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
glukózy	glukóza	k1gFnSc2	glukóza
využívá	využívat	k5eAaImIp3nS	využívat
také	také	k9	také
bílkovin	bílkovina	k1gFnPc2	bílkovina
ze	z	k7c2	z
svalů	sval	k1gInPc2	sval
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
oslabování	oslabování	k1gNnSc2	oslabování
jejich	jejich	k3xOp3gFnSc2	jejich
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Využitelná	využitelný	k2eAgFnSc1d1	využitelná
energie	energie	k1gFnSc1	energie
na	na	k7c4	na
100	[number]	k4	100
g	g	kA	g
glukózy	glukóza	k1gFnSc2	glukóza
je	být	k5eAaImIp3nS	být
1670	[number]	k4	1670
kJ	kJ	k?	kJ
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
(	(	kIx(	(
<g/>
hladina	hladina	k1gFnSc1	hladina
<g/>
)	)	kIx)	)
D-glukózy	Dlukóza	k1gFnPc1	D-glukóza
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
glykemie	glykemie	k1gFnSc1	glykemie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
biochemického	biochemický	k2eAgNnSc2d1	biochemické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgFnSc7d1	aktivní
formou	forma	k1gFnSc7	forma
D-glukózy	Dlukóza	k1gFnSc2	D-glukóza
glukóza-	glukóza-	k?	glukóza-
<g/>
6	[number]	k4	6
<g/>
-fosfát	osfát	k1gInSc1	-fosfát
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgInSc1d1	vznikající
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
působením	působení	k1gNnSc7	působení
adenosin	adenosina	k1gFnPc2	adenosina
trifosfátu	trifosfát	k1gInSc2	trifosfát
(	(	kIx(	(
<g/>
ATP	atp	kA	atp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
již	již	k6eAd1	již
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
použití	použití	k1gNnSc2	použití
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
je	být	k5eAaImIp3nS	být
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
fermentační	fermentační	k2eAgFnSc4d1	fermentační
výrobu	výroba	k1gFnSc4	výroba
ethanolu	ethanol	k1gInSc2	ethanol
a	a	k8xC	a
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
(	(	kIx(	(
<g/>
pivo	pivo	k1gNnSc1	pivo
<g/>
,	,	kIx,	,
víno	víno	k1gNnSc1	víno
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
alkoholovým	alkoholový	k2eAgNnSc7d1	alkoholové
kvašením	kvašení	k1gNnSc7	kvašení
působením	působení	k1gNnSc7	působení
kvasinek	kvasinka	k1gFnPc2	kvasinka
rodu	rod	k1gInSc2	rod
Saccharomyces	Saccharomycesa	k1gFnPc2	Saccharomycesa
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
jiných	jiný	k2eAgInPc2d1	jiný
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
lze	lze	k6eAd1	lze
vyrábět	vyrábět	k5eAaImF	vyrábět
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
důležité	důležitý	k2eAgFnPc1d1	důležitá
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
butan-	butan-	k?	butan-
<g/>
1	[number]	k4	1
<g/>
-ol	l	k?	-ol
,	,	kIx,	,
glycerol	glycerol	k1gInSc1	glycerol
<g/>
,	,	kIx,	,
aceton	aceton	k1gInSc1	aceton
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
mléčnou	mléčný	k2eAgFnSc4d1	mléčná
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
citronovou	citronový	k2eAgFnSc4d1	citronová
aj.	aj.	kA	aj.
Enzymaticky	enzymaticky	k6eAd1	enzymaticky
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyrábí	vyrábět	k5eAaImIp3nP	vyrábět
mnohem	mnohem	k6eAd1	mnohem
sladší	sladký	k2eAgInSc4d2	sladší
monosacharid	monosacharid	k1gInSc4	monosacharid
fruktóza	fruktóza	k1gFnSc1	fruktóza
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
potravinářském	potravinářský	k2eAgInSc6d1	potravinářský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
