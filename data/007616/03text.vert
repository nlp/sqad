<s>
Dějiny	dějiny	k1gFnPc1	dějiny
umění	umění	k1gNnSc2	umění
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
zpravidla	zpravidla	k6eAd1	zpravidla
používaný	používaný	k2eAgInSc1d1	používaný
pro	pro	k7c4	pro
dějiny	dějiny	k1gFnPc4	dějiny
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
kterými	který	k3yQgNnPc7	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
(	(	kIx(	(
<g/>
též	též	k9	též
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
kunsthistorie	kunsthistorie	k1gFnPc4	kunsthistorie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
oborům	obor	k1gInPc3	obor
tohoto	tento	k3xDgNnSc2	tento
umění	umění	k1gNnSc1	umění
patří	patřit	k5eAaImIp3nS	patřit
tradiční	tradiční	k2eAgNnSc1d1	tradiční
malířství	malířství	k1gNnSc1	malířství
<g/>
,	,	kIx,	,
sochařství	sochařství	k1gNnSc1	sochařství
<g/>
,	,	kIx,	,
architektura	architektura	k1gFnSc1	architektura
a	a	k8xC	a
umělecké	umělecký	k2eAgNnSc1d1	umělecké
řemeslo	řemeslo	k1gNnSc1	řemeslo
<g/>
,	,	kIx,	,
nové	nový	k2eAgFnPc1d1	nová
formy	forma	k1gFnPc1	forma
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
zdůrazňující	zdůrazňující	k2eAgInSc4d1	zdůrazňující
kontext	kontext	k1gInSc4	kontext
a	a	k8xC	a
tvůrčí	tvůrčí	k2eAgInSc4d1	tvůrčí
proces	proces	k1gInSc4	proces
rámec	rámec	k1gInSc4	rámec
čistě	čistě	k6eAd1	čistě
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
často	často	k6eAd1	často
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
<g/>
.	.	kIx.	.
</s>
<s>
Ostatními	ostatní	k2eAgInPc7d1	ostatní
uměleckými	umělecký	k2eAgInPc7d1	umělecký
obory	obor	k1gInPc7	obor
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
dějiny	dějiny	k1gFnPc1	dějiny
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
filmu	film	k1gInSc2	film
ap.	ap.	kA	ap.
Periodizace	periodizace	k1gFnSc1	periodizace
dějin	dějiny	k1gFnPc2	dějiny
umění	umění	k1gNnSc2	umění
podle	podle	k7c2	podle
století	století	k1gNnSc2	století
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
normálnímu	normální	k2eAgNnSc3d1	normální
dělení	dělení	k1gNnSc3	dělení
na	na	k7c6	na
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
stylu	styl	k1gInSc3	styl
vždy	vždy	k6eAd1	vždy
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
trvá	trvat	k5eAaImIp3nS	trvat
než	než	k8xS	než
se	se	k3xPyFc4	se
prosadí	prosadit	k5eAaPmIp3nS	prosadit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
svým	svůj	k3xOyFgInSc7	svůj
významem	význam	k1gInSc7	význam
nezapadá	zapadat	k5eNaPmIp3nS	zapadat
do	do	k7c2	do
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
formálně	formálně	k6eAd1	formálně
vznikala	vznikat	k5eAaImAgNnP	vznikat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
konzervativního	konzervativní	k2eAgNnSc2d1	konzervativní
pojetí	pojetí	k1gNnSc2	pojetí
začíná	začínat	k5eAaImIp3nS	začínat
modernismus	modernismus	k1gInSc4	modernismus
klasicismem	klasicismus	k1gInSc7	klasicismus
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
převažujícího	převažující	k2eAgInSc2d1	převažující
až	až	k8xS	až
impresionismem	impresionismus	k1gInSc7	impresionismus
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
postimpresionismem	postimpresionismus	k1gInSc7	postimpresionismus
a	a	k8xC	a
symbolismem	symbolismus	k1gInSc7	symbolismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
umění	umění	k1gNnSc6	umění
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
střídají	střídat	k5eAaImIp3nP	střídat
dynamické	dynamický	k2eAgInPc1d1	dynamický
a	a	k8xC	a
statické	statický	k2eAgInPc1d1	statický
styly	styl	k1gInPc1	styl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dynamickém	dynamický	k2eAgNnSc6d1	dynamické
baroku	baroko	k1gNnSc6	baroko
následoval	následovat	k5eAaImAgInS	následovat
statický	statický	k2eAgInSc4d1	statický
klasicismus	klasicismus	k1gInSc4	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
členění	členění	k1gNnSc6	členění
slohu	sloh	k1gInSc2	sloh
je	být	k5eAaImIp3nS	být
začátek	začátek	k1gInSc4	začátek
a	a	k8xC	a
vrchol	vrchol	k1gInSc4	vrchol
statický	statický	k2eAgInSc4d1	statický
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
konec	konec	k1gInSc1	konec
je	být	k5eAaImIp3nS	být
dynamický	dynamický	k2eAgInSc1d1	dynamický
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
pojetí	pojetí	k1gNnSc1	pojetí
historiků	historik	k1gMnPc2	historik
umění	umění	k1gNnSc2	umění
dávalo	dávat	k5eAaImAgNnS	dávat
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
osobnosti	osobnost	k1gFnPc4	osobnost
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejímž	jejíž	k3xOyRp3gMnPc3	jejíž
hlavním	hlavní	k2eAgMnPc3d1	hlavní
zakladatelům	zakladatel	k1gMnPc3	zakladatel
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
Johann	Johann	k1gMnSc1	Johann
Joachim	Joachim	k1gMnSc1	Joachim
Winckelmann	Winckelmann	k1gMnSc1	Winckelmann
<g/>
,	,	kIx,	,
sleduje	sledovat	k5eAaImIp3nS	sledovat
širší	široký	k2eAgInPc4d2	širší
stylové	stylový	k2eAgInPc4d1	stylový
proudy	proud	k1gInPc4	proud
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
ke	k	k7c3	k
kulturní	kulturní	k2eAgFnSc3d1	kulturní
a	a	k8xC	a
společenského	společenský	k2eAgNnSc2d1	společenské
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
dějiny	dějiny	k1gFnPc4	dějiny
umění	umění	k1gNnPc2	umění
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
přímo	přímo	k6eAd1	přímo
pro	pro	k7c4	pro
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
historie	historie	k1gFnSc2	historie
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
všech	všecek	k3xTgInPc2	všecek
oborů	obor	k1gInPc2	obor
umění	umění	k1gNnSc2	umění
<g/>
:	:	kIx,	:
výtvarného	výtvarný	k2eAgMnSc2d1	výtvarný
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
zobrazovacího	zobrazovací	k2eAgInSc2d1	zobrazovací
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
mimetického	mimetický	k2eAgNnSc2d1	mimetické
<g/>
"	"	kIx"	"
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
sochařství	sochařství	k1gNnSc1	sochařství
<g/>
,	,	kIx,	,
malířství	malířství	k1gNnSc1	malířství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
<g />
.	.	kIx.	.
</s>
<s>
také	také	k9	také
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
časových	časový	k2eAgInPc2d1	časový
<g/>
"	"	kIx"	"
umění	umění	k1gNnSc1	umění
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
hranice	hranice	k1gFnPc4	hranice
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
obory	obor	k1gInPc7	obor
poměrně	poměrně	k6eAd1	poměrně
výrazně	výrazně	k6eAd1	výrazně
narušována	narušovat	k5eAaImNgNnP	narušovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
performance	performance	k1gFnSc2	performance
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
instalace	instalace	k1gFnSc1	instalace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
multimediální	multimediální	k2eAgNnPc1d1	multimediální
umění	umění	k1gNnPc1	umění
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
obory	obor	k1gInPc4	obor
umění	umění	k1gNnSc2	umění
spolu	spolu	k6eAd1	spolu
tvořísloh	tvořísloha	k1gFnPc2	tvořísloha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
podmínkou	podmínka	k1gFnSc7	podmínka
pro	pro	k7c4	pro
chápání	chápání	k1gNnSc4	chápání
určitého	určitý	k2eAgInSc2d1	určitý
stylu	styl	k1gInSc2	styl
jako	jako	k8xS	jako
uměleckého	umělecký	k2eAgInSc2d1	umělecký
slohu	sloh	k1gInSc2	sloh
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
uměleckých	umělecký	k2eAgInPc6d1	umělecký
oborech	obor	k1gInPc6	obor
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
dějiny	dějiny	k1gFnPc4	dějiny
umění	umění	k1gNnSc2	umění
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
užívá	užívat	k5eAaImIp3nS	užívat
také	také	k9	také
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
vědeckého	vědecký	k2eAgInSc2d1	vědecký
oboru	obor	k1gInSc2	obor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zkoumáteorii	zkoumáteorie	k1gFnSc4	zkoumáteorie
a	a	k8xC	a
dějiny	dějiny	k1gFnPc4	dějiny
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
:	:	kIx,	:
Příčiny	příčina	k1gFnPc4	příčina
<g/>
,	,	kIx,	,
průběh	průběh	k1gInSc4	průběh
a	a	k8xC	a
důsledky	důsledek	k1gInPc4	důsledek
vzniku	vznik	k1gInSc2	vznik
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
výtvarných	výtvarný	k2eAgFnPc2d1	výtvarná
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
kulturní	kulturní	k2eAgInSc1d1	kulturní
kontext	kontext	k1gInSc1	kontext
i	i	k8xC	i
dějiny	dějiny	k1gFnPc1	dějiny
uměleckých	umělecký	k2eAgInPc2d1	umělecký
slohů	sloh	k1gInPc2	sloh
jako	jako	k8xC	jako
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
<g/>
typologii	typologie	k1gFnSc4	typologie
a	a	k8xC	a
stylové	stylový	k2eAgFnPc4d1	stylová
proměny	proměna	k1gFnPc4	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Činí	činit	k5eAaImIp3nS	činit
tak	tak	k9	tak
pomocí	pomocí	k7c2	pomocí
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
stylová	stylový	k2eAgFnSc1d1	stylová
analýza	analýza	k1gFnSc1	analýza
<g/>
,	,	kIx,	,
<g/>
stavebně-historický	stavebněistorický	k2eAgInSc1d1	stavebně-historický
průzkum	průzkum	k1gInSc1	průzkum
či	či	k8xC	či
ikonografie	ikonografie	k1gFnSc1	ikonografie
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
přitom	přitom	k6eAd1	přitom
také	také	k9	také
řadu	řada	k1gFnSc4	řada
informací	informace	k1gFnPc2	informace
získaných	získaný	k2eAgFnPc2d1	získaná
jinými	jiný	k2eAgInPc7d1	jiný
obory	obor	k1gInPc7	obor
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
obecná	obecný	k2eAgFnSc1d1	obecná
historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
archivnictví	archivnictví	k1gNnSc1	archivnictví
<g/>
,	,	kIx,	,
restaurátorství	restaurátorství	k1gNnSc1	restaurátorství
<g/>
,	,	kIx,	,
sociologie	sociologie	k1gFnSc1	sociologie
<g/>
,	,	kIx,	,
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Výsledkem	výsledek	k1gInSc7	výsledek
tohoto	tento	k3xDgNnSc2	tento
zkoumání	zkoumání	k1gNnSc2	zkoumání
je	být	k5eAaImIp3nS	být
napříkladdatace	napříkladdatace	k1gFnSc1	napříkladdatace
uměleckých	umělecký	k2eAgInPc2d1	umělecký
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
výklad	výklad	k1gInSc1	výklad
<g/>
,	,	kIx,	,
autorství	autorství	k1gNnSc1	autorství
<g/>
,	,	kIx,	,
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
objednavatelé	objednavatel	k1gMnPc1	objednavatel
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
Ostatními	ostatní	k2eAgInPc7d1	ostatní
uměleckými	umělecký	k2eAgInPc7d1	umělecký
obory	obor	k1gInPc7	obor
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
například	například	k6eAd1	například
dějiny	dějiny	k1gFnPc1	dějiny
hudby	hudba	k1gFnSc2	hudba
neboli	neboli	k8xC	neboli
hudební	hudební	k2eAgFnSc1d1	hudební
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
dějiny	dějiny	k1gFnPc4	dějiny
divadla	divadlo	k1gNnSc2	divadlo
neboliteatrologie	neboliteatrologie	k1gFnSc2	neboliteatrologie
<g/>
,	,	kIx,	,
dějiny	dějiny	k1gFnPc4	dějiny
literatury	literatura	k1gFnSc2	literatura
neboli	neboli	k8xC	neboli
literární	literární	k2eAgFnSc2d1	literární
historie	historie	k1gFnSc2	historie
(	(	kIx(	(
<g/>
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
podle	podle	k7c2	podle
jazyka	jazyk	k1gInSc2	jazyk
bohemistika	bohemistika	k1gFnSc1	bohemistika
<g/>
,	,	kIx,	,
anglistika	anglistika	k1gFnSc1	anglistika
<g/>
,	,	kIx,	,
<g/>
sinologie	sinologie	k1gFnSc1	sinologie
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dějiny	dějiny	k1gFnPc4	dějiny
filmu	film	k1gInSc2	film
neboli	neboli	k8xC	neboli
filmová	filmový	k2eAgFnSc1d1	filmová
věda	věda	k1gFnSc1	věda
atd.	atd.	kA	atd.
Následující	následující	k2eAgInSc4d1	následující
přehled	přehled	k1gInSc4	přehled
uměleckých	umělecký	k2eAgInPc2d1	umělecký
slohů	sloh	k1gInPc2	sloh
a	a	k8xC	a
stylů	styl	k1gInPc2	styl
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
práce	práce	k1gFnPc4	práce
těchto	tento	k3xDgInPc2	tento
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
vědeckých	vědecký	k2eAgInPc2d1	vědecký
oborů	obor	k1gInPc2	obor
<g/>
:	:	kIx,	:
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Umění	umění	k1gNnSc2	umění
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
pravěku	pravěk	k1gInSc2	pravěk
představuje	představovat	k5eAaImIp3nS	představovat
nejdelší	dlouhý	k2eAgNnSc4d3	nejdelší
období	období	k1gNnSc4	období
vývoje	vývoj	k1gInSc2	vývoj
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
estetických	estetický	k2eAgInPc2d1	estetický
projevů	projev	k1gInPc2	projev
předchůdců	předchůdce	k1gMnPc2	předchůdce
dnešního	dnešní	k2eAgMnSc2d1	dnešní
člověka	člověk	k1gMnSc2	člověk
po	po	k7c4	po
vznik	vznik	k1gInSc4	vznik
civilizací	civilizace	k1gFnPc2	civilizace
používajících	používající	k2eAgFnPc2d1	používající
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Vyspělejší	vyspělý	k2eAgFnSc2d2	vyspělejší
formy	forma	k1gFnSc2	forma
získává	získávat	k5eAaImIp3nS	získávat
umění	umění	k1gNnSc1	umění
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
kromaňonců	kromaňonec	k1gMnPc2	kromaňonec
<g/>
)	)	kIx)	)
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
mladém	mladý	k2eAgInSc6d1	mladý
paleolitu	paleolit	k1gInSc6	paleolit
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
zvláště	zvláště	k9	zvláště
jeskynní	jeskynní	k2eAgNnSc1d1	jeskynní
malířství	malířství	k1gNnSc1	malířství
a	a	k8xC	a
sošky	soška	k1gFnPc1	soška
zvířat	zvíře	k1gNnPc2	zvíře
i	i	k8xC	i
lidských	lidský	k2eAgFnPc2d1	lidská
postav	postava	k1gFnPc2	postava
(	(	kIx(	(
<g/>
venuše	venuše	k1gFnSc1	venuše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
impuls	impuls	k1gInSc1	impuls
získává	získávat	k5eAaImIp3nS	získávat
umění	umění	k1gNnSc4	umění
v	v	k7c6	v
neolitu	neolit	k1gInSc6	neolit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
usazováním	usazování	k1gNnSc7	usazování
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
přechodem	přechod	k1gInSc7	přechod
k	k	k7c3	k
zemědělství	zemědělství	k1gNnSc3	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
rozvoj	rozvoj	k1gInSc1	rozvoj
keramických	keramický	k2eAgFnPc2d1	keramická
nádob	nádoba	k1gFnPc2	nádoba
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
svými	svůj	k3xOyFgInPc7	svůj
tvary	tvar	k1gInPc7	tvar
a	a	k8xC	a
výzdobou	výzdoba	k1gFnSc7	výzdoba
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
vymezovat	vymezovat	k5eAaImF	vymezovat
archeologické	archeologický	k2eAgFnSc2d1	archeologická
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
novým	nový	k2eAgFnPc3d1	nová
formám	forma	k1gFnPc3	forma
umění	umění	k1gNnSc2	umění
patří	patřit	k5eAaImIp3nP	patřit
z	z	k7c2	z
velkých	velký	k2eAgInPc2d1	velký
kamenů	kámen	k1gInPc2	kámen
budované	budovaný	k2eAgFnSc2d1	budovaná
megalitické	megalitický	k2eAgFnSc2d1	megalitická
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
účel	účel	k1gInSc4	účel
nebyl	být	k5eNaImAgInS	být
praktický	praktický	k2eAgInSc1d1	praktický
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
pravěku	pravěk	k1gInSc2	pravěk
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
vznik	vznik	k1gInSc1	vznik
vyspělých	vyspělý	k2eAgFnPc2d1	vyspělá
starověkých	starověký	k2eAgFnPc2d1	starověká
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ovlivňovaly	ovlivňovat	k5eAaImAgFnP	ovlivňovat
i	i	k9	i
sousední	sousední	k2eAgFnPc1d1	sousední
oblasti	oblast	k1gFnPc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Umění	umění	k1gNnSc2	umění
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgInPc1d1	vysoký
civilizační	civilizační	k2eAgMnSc1d1	civilizační
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
umělecké	umělecký	k2eAgFnSc2d1	umělecká
úrovně	úroveň	k1gFnSc2	úroveň
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
lidstvo	lidstvo	k1gNnSc1	lidstvo
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
velké	velký	k2eAgFnPc1d1	velká
kultury	kultura	k1gFnPc1	kultura
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
a	a	k8xC	a
Egyptě	Egypt	k1gInSc6	Egypt
zhruba	zhruba	k6eAd1	zhruba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
3	[number]	k4	3
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Umění	umění	k1gNnSc4	umění
těchto	tento	k3xDgFnPc2	tento
říší	říš	k1gFnPc2	říš
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
despotickými	despotický	k2eAgMnPc7d1	despotický
vládci	vládce	k1gMnPc7	vládce
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc7	jejich
mocenským	mocenský	k2eAgInSc7d1	mocenský
aparátem	aparát	k1gInSc7	aparát
a	a	k8xC	a
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Projevovalo	projevovat	k5eAaImAgNnS	projevovat
se	se	k3xPyFc4	se
monumentálními	monumentální	k2eAgFnPc7d1	monumentální
stavbami	stavba	k1gFnPc7	stavba
náboženskými	náboženský	k2eAgFnPc7d1	náboženská
<g/>
,	,	kIx,	,
palácovými	palácový	k2eAgInPc7d1	palácový
i	i	k8xC	i
pohřebními	pohřební	k2eAgInPc7d1	pohřební
<g/>
,	,	kIx,	,
sochařským	sochařský	k2eAgInSc7d1	sochařský
zpodobnění	zpodobnění	k1gNnSc4	zpodobnění
vládců	vládce	k1gMnPc2	vládce
<g/>
,	,	kIx,	,
nízkými	nízký	k2eAgInPc7d1	nízký
reliéfy	reliéf	k1gInPc7	reliéf
a	a	k8xC	a
malbami	malba	k1gFnPc7	malba
oslavujícími	oslavující	k2eAgInPc7d1	oslavující
jejich	jejich	k3xOp3gInPc7	jejich
činy	čin	k1gInPc7	čin
nebo	nebo	k8xC	nebo
doplňujícími	doplňující	k2eAgInPc7d1	doplňující
účel	účel	k1gInSc4	účel
zmiňovaných	zmiňovaný	k2eAgFnPc2d1	zmiňovaná
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Klasický	klasický	k2eAgInSc1d1	klasický
starověk	starověk	k1gInSc1	starověk
(	(	kIx(	(
<g/>
antika	antika	k1gFnSc1	antika
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Jiných	jiný	k2eAgFnPc2d1	jiná
kvalit	kvalita	k1gFnPc2	kvalita
později	pozdě	k6eAd2	pozdě
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
umění	umění	k1gNnSc1	umění
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
hledalo	hledat	k5eAaImAgNnS	hledat
harmonii	harmonie	k1gFnSc4	harmonie
<g/>
,	,	kIx,	,
řád	řád	k1gInSc4	řád
a	a	k8xC	a
obracelo	obracet	k5eAaImAgNnS	obracet
pozornost	pozornost	k1gFnSc4	pozornost
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
individuálního	individuální	k2eAgMnSc4d1	individuální
člena	člen	k1gMnSc4	člen
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
chrámy	chrám	k1gInPc1	chrám
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
závazných	závazný	k2eAgInPc2d1	závazný
řádů	řád	k1gInPc2	řád
rytmizující	rytmizující	k2eAgFnSc2d1	rytmizující
stavby	stavba	k1gFnSc2	stavba
sloupy	sloup	k1gInPc1	sloup
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
sochařství	sochařství	k1gNnSc1	sochařství
je	být	k5eAaImIp3nS	být
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
na	na	k7c4	na
zobrazení	zobrazení	k1gNnSc4	zobrazení
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
idealizované	idealizovaný	k2eAgFnSc2d1	idealizovaná
krásy	krása	k1gFnSc2	krása
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
postavy	postava	k1gFnPc4	postava
a	a	k8xC	a
příběhy	příběh	k1gInPc4	příběh
je	být	k5eAaImIp3nS	být
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
výzdoba	výzdoba	k1gFnSc1	výzdoba
keramiky	keramika	k1gFnSc2	keramika
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
starého	starý	k2eAgInSc2d1	starý
Říma	Řím	k1gInSc2	Řím
navazuje	navazovat	k5eAaImIp3nS	navazovat
a	a	k8xC	a
čerpá	čerpat	k5eAaImIp3nS	čerpat
z	z	k7c2	z
umění	umění	k1gNnSc2	umění
řeckého	řecký	k2eAgNnSc2d1	řecké
<g/>
,	,	kIx,	,
prozrazuje	prozrazovat	k5eAaImIp3nS	prozrazovat
však	však	k9	však
odlišnost	odlišnost	k1gFnSc1	odlišnost
společnosti	společnost	k1gFnSc2	společnost
budující	budující	k2eAgFnSc1d1	budující
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
silou	síla	k1gFnSc7	síla
mocnou	mocný	k2eAgFnSc4d1	mocná
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
soch	socha	k1gFnPc2	socha
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
a	a	k8xC	a
staveb	stavba	k1gFnPc2	stavba
oslavujícím	oslavující	k2eAgFnPc3d1	oslavující
jejich	jejich	k3xOp3gInPc1	jejich
činy	čin	k1gInPc4	čin
však	však	k9	však
Římané	Říman	k1gMnPc1	Říman
umění	umění	k1gNnSc2	umění
obohacují	obohacovat	k5eAaImIp3nP	obohacovat
např.	např.	kA	např.
o	o	k7c4	o
portrétní	portrétní	k2eAgNnSc4d1	portrétní
sochařství	sochařství	k1gNnSc4	sochařství
či	či	k8xC	či
klenbu	klenba	k1gFnSc4	klenba
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Antické	antický	k2eAgNnSc1d1	antické
umění	umění	k1gNnSc1	umění
vytyčením	vytyčení	k1gNnSc7	vytyčení
univerzálních	univerzální	k2eAgInPc2d1	univerzální
ideálů	ideál	k1gInPc2	ideál
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
systematickou	systematický	k2eAgFnSc7d1	systematická
realizací	realizace	k1gFnSc7	realizace
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
kvalit	kvalita	k1gFnPc2	kvalita
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
opakovaně	opakovaně	k6eAd1	opakovaně
vracet	vracet	k5eAaImF	vracet
umělci	umělec	k1gMnSc3	umělec
všech	všecek	k3xTgNnPc2	všecek
následujících	následující	k2eAgNnPc2d1	následující
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
vysoké	vysoká	k1gFnPc1	vysoká
umělecké	umělecký	k2eAgFnSc2d1	umělecká
úrovně	úroveň	k1gFnSc2	úroveň
i	i	k8xC	i
civilizace	civilizace	k1gFnSc2	civilizace
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Indie	Indie	k1gFnSc2	Indie
(	(	kIx(	(
<g/>
od	od	k7c2	od
protoindické	protoindický	k2eAgFnSc2d1	protoindická
po	po	k7c4	po
Guptovu	Guptův	k2eAgFnSc4d1	Guptův
říši	říše	k1gFnSc4	říše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Velká	velký	k2eAgFnSc1d1	velká
čínská	čínský	k2eAgFnSc1d1	čínská
zeď	zeď	k1gFnSc1	zeď
či	či	k8xC	či
terakotová	terakotový	k2eAgFnSc1d1	Terakotová
armáda	armáda	k1gFnSc1	armáda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
klasická	klasický	k2eAgFnSc1d1	klasická
mayská	mayský	k2eAgFnSc1d1	mayská
civilizace	civilizace	k1gFnSc1	civilizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Umění	umění	k1gNnSc2	umění
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Románský	románský	k2eAgInSc1d1	románský
sloh	sloh	k1gInSc1	sloh
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1075	[number]	k4	1075
<g/>
–	–	k?	–
<g/>
1125	[number]	k4	1125
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
důsledek	důsledek	k1gInSc1	důsledek
rozmachu	rozmach	k1gInSc2	rozmach
klášterů	klášter	k1gInPc2	klášter
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
zažívala	zažívat	k5eAaImAgFnS	zažívat
konečně	konečně	k6eAd1	konečně
po	po	k7c6	po
temných	temný	k2eAgNnPc6d1	temné
stoletích	století	k1gNnPc6	století
období	období	k1gNnSc2	období
stability	stabilita	k1gFnSc2	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
ve	v	k7c6	v
specifické	specifický	k2eAgFnSc6d1	specifická
architektuře	architektura	k1gFnSc6	architektura
sakrálních	sakrální	k2eAgFnPc2d1	sakrální
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
600	[number]	k4	600
letech	léto	k1gNnPc6	léto
spánku	spánek	k1gInSc2	spánek
oživlo	oživnout	k5eAaPmAgNnS	oživnout
monumentální	monumentální	k2eAgNnSc4d1	monumentální
sochařství	sochařství	k1gNnSc4	sochařství
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
gotický	gotický	k2eAgInSc4d1	gotický
sloh	sloh	k1gInSc4	sloh
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
italských	italský	k2eAgMnPc2d1	italský
renesančních	renesanční	k2eAgMnPc2d1	renesanční
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
předchozí	předchozí	k2eAgInSc4d1	předchozí
barbarský	barbarský	k2eAgInSc4d1	barbarský
sloh	sloh	k1gInSc4	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nemá	mít	k5eNaImIp3nS	mít
s	s	k7c7	s
Góty	Gót	k1gMnPc7	Gót
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgMnSc4d1	společný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
typický	typický	k2eAgInSc1d1	typický
sloh	sloh	k1gInSc1	sloh
vyspělého	vyspělý	k2eAgInSc2d1	vyspělý
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Znamenal	znamenat	k5eAaImAgMnS	znamenat
obnovení	obnovení	k1gNnSc4	obnovení
městského	městský	k2eAgInSc2d1	městský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Mocným	mocný	k2eAgInSc7d1	mocný
impulsem	impuls	k1gInSc7	impuls
byly	být	k5eAaImAgFnP	být
křížové	křížový	k2eAgFnPc1d1	křížová
výpravy	výprava	k1gFnPc1	výprava
do	do	k7c2	do
Levanty	Levanta	k1gFnSc2	Levanta
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
a	a	k8xC	a
nejoriginálnější	originální	k2eAgFnSc1d3	nejoriginálnější
je	být	k5eAaImIp3nS	být
gotická	gotický	k2eAgFnSc1d1	gotická
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1120	[number]	k4	1120
v	v	k7c6	v
Île-de-France	Îlee-France	k1gFnSc2	Île-de-France
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
investicemi	investice	k1gFnPc7	investice
bohatého	bohatý	k2eAgNnSc2d1	bohaté
měšťanstva	měšťanstvo	k1gNnSc2	měšťanstvo
<g/>
.	.	kIx.	.
</s>
<s>
Odlehčené	odlehčený	k2eAgInPc1d1	odlehčený
oblouky	oblouk	k1gInPc1	oblouk
umožnily	umožnit	k5eAaPmAgInP	umožnit
větší	veliký	k2eAgFnPc4d2	veliký
a	a	k8xC	a
vyšší	vysoký	k2eAgFnPc4d2	vyšší
budovy	budova	k1gFnPc4	budova
než	než	k8xS	než
v	v	k7c6	v
románském	románský	k2eAgInSc6d1	románský
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
gotickou	gotický	k2eAgFnSc7d1	gotická
památkou	památka	k1gFnSc7	památka
je	být	k5eAaImIp3nS	být
klášter	klášter	k1gInSc1	klášter
Saint-Denis	Saint-Denis	k1gFnSc2	Saint-Denis
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
stavba	stavba	k1gFnSc1	stavba
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1140	[number]	k4	1140
<g/>
.	.	kIx.	.
</s>
<s>
Sochařství	sochařství	k1gNnSc1	sochařství
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
doplněk	doplněk	k1gInSc4	doplněk
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
rozvoj	rozvoj	k1gInSc1	rozvoj
začíná	začínat	k5eAaImIp3nS	začínat
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
sochy	socha	k1gFnPc1	socha
stávají	stávat	k5eAaImIp3nP	stávat
přirozenější	přirozený	k2eAgFnPc1d2	přirozenější
<g/>
,	,	kIx,	,
s	s	k7c7	s
individualizovanými	individualizovaný	k2eAgFnPc7d1	individualizovaná
tvářemi	tvář	k1gFnPc7	tvář
a	a	k8xC	a
postavami	postava	k1gFnPc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Malířství	malířství	k1gNnSc1	malířství
začíná	začínat	k5eAaImIp3nS	začínat
až	až	k9	až
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Filozofie	filozofie	k1gFnSc1	filozofie
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
scholastikou	scholastika	k1gFnSc7	scholastika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
gotice	gotika	k1gFnSc6	gotika
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
stavěly	stavět	k5eAaImAgFnP	stavět
světské	světský	k2eAgFnPc4d1	světská
stavby	stavba	k1gFnPc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
gotika	gotika	k1gFnSc1	gotika
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
14	[number]	k4	14
<g/>
.	.	kIx.	.
stol	stol	k1gInSc1	stol
se	se	k3xPyFc4	se
gotická	gotický	k2eAgFnSc1d1	gotická
kultura	kultura	k1gFnSc1	kultura
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
i	i	k9	i
díky	díky	k7c3	díky
Karlovi	Karel	k1gMnSc3	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
sem	sem	k6eAd1	sem
přinesl	přinést	k5eAaPmAgInS	přinést
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Umění	umění	k1gNnSc2	umění
novověku	novověk	k1gInSc2	novověk
<g/>
.	.	kIx.	.
</s>
<s>
Renesance	renesance	k1gFnSc1	renesance
znamená	znamenat	k5eAaImIp3nS	znamenat
znovuzrození	znovuzrození	k1gNnSc4	znovuzrození
<g/>
,	,	kIx,	,
rozumí	rozumět	k5eAaImIp3nS	rozumět
se	se	k3xPyFc4	se
klasického	klasický	k2eAgInSc2d1	klasický
slohu	sloh	k1gInSc2	sloh
antiky	antika	k1gFnSc2	antika
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
přechodný	přechodný	k2eAgInSc4d1	přechodný
sloh	sloh	k1gInSc4	sloh
mezi	mezi	k7c7	mezi
středověkem	středověk	k1gInSc7	středověk
a	a	k8xC	a
novověkem	novověk	k1gInSc7	novověk
<g/>
.	.	kIx.	.
</s>
<s>
Znamenala	znamenat	k5eAaImAgFnS	znamenat
prohloubení	prohloubení	k1gNnSc4	prohloubení
oživení	oživení	k1gNnSc4	oživení
městského	městský	k2eAgInSc2d1	městský
života	život	k1gInSc2	život
a	a	k8xC	a
větší	veliký	k2eAgInSc4d2	veliký
podíl	podíl	k1gInSc4	podíl
laiků	laik	k1gMnPc2	laik
na	na	k7c6	na
náboženském	náboženský	k2eAgInSc6d1	náboženský
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
předznamenána	předznamenat	k5eAaPmNgFnS	předznamenat
selháním	selhání	k1gNnSc7	selhání
univerzalismu	univerzalismus	k1gInSc2	univerzalismus
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
(	(	kIx(	(
<g/>
avignonské	avignonský	k2eAgNnSc1d1	avignonské
zajetí	zajetí	k1gNnSc1	zajetí
a	a	k8xC	a
velké	velký	k2eAgNnSc1d1	velké
schisma	schisma	k1gNnSc1	schisma
<g/>
)	)	kIx)	)
a	a	k8xC	a
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
král	král	k1gMnSc1	král
/	/	kIx~	/
císař	císař	k1gMnSc1	císař
neměl	mít	k5eNaImAgMnS	mít
reálnou	reálný	k2eAgFnSc4d1	reálná
moc	moc	k1gFnSc4	moc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Latina	latina	k1gFnSc1	latina
přestala	přestat	k5eAaPmAgFnS	přestat
být	být	k5eAaImF	být
jedinou	jediný	k2eAgFnSc7d1	jediná
řečí	řeč	k1gFnSc7	řeč
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
<g/>
.	.	kIx.	.
</s>
<s>
Renesance	renesance	k1gFnSc1	renesance
se	se	k3xPyFc4	se
zrodila	zrodit	k5eAaPmAgFnS	zrodit
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
Florencie	Florencie	k1gFnPc1	Florencie
<g/>
,	,	kIx,	,
Benátky	Benátky	k1gFnPc1	Benátky
a	a	k8xC	a
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
bratrské	bratrský	k2eAgNnSc1d1	bratrské
hnutí	hnutí	k1gNnSc1	hnutí
humanistů	humanista	k1gMnPc2	humanista
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byli	být	k5eAaImAgMnP	být
laici	laik	k1gMnPc1	laik
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dřívějších	dřívější	k2eAgMnPc2d1	dřívější
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
–	–	k?	–
kleriků	klerik	k1gMnPc2	klerik
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
intelektuálním	intelektuální	k2eAgInSc7d1	intelektuální
světem	svět	k1gInSc7	svět
byl	být	k5eAaImAgInS	být
synkretismus	synkretismus	k1gInSc1	synkretismus
<g/>
.	.	kIx.	.
</s>
<s>
Renesance	renesance	k1gFnSc1	renesance
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
na	na	k7c6	na
konci	konec	k1gInSc6	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
inspirována	inspirován	k2eAgFnSc1d1	inspirována
sv.	sv.	kA	sv.
Františkem	František	k1gMnSc7	František
z	z	k7c2	z
Assisi	Assis	k1gMnSc6	Assis
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
obrátil	obrátit	k5eAaPmAgMnS	obrátit
pozornost	pozornost	k1gFnSc4	pozornost
od	od	k7c2	od
duchovních	duchovní	k2eAgFnPc2d1	duchovní
věcí	věc	k1gFnPc2	věc
k	k	k7c3	k
světu	svět	k1gInSc3	svět
kolem	kolem	k7c2	kolem
nás	my	k3xPp1nPc2	my
–	–	k?	–
ke	k	k7c3	k
kráse	krása	k1gFnSc3	krása
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
umělcem	umělec	k1gMnSc7	umělec
proto-renesance	protoenesance	k1gFnSc2	proto-renesance
je	být	k5eAaImIp3nS	být
Giotto	Giotto	k1gNnSc1	Giotto
(	(	kIx(	(
<g/>
1266	[number]	k4	1266
<g/>
–	–	k?	–
<g/>
1337	[number]	k4	1337
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Danteho	Danteze	k6eAd1	Danteze
Božská	božský	k2eAgFnSc1d1	božská
komedie	komedie	k1gFnSc1	komedie
je	být	k5eAaImIp3nS	být
gotická	gotický	k2eAgFnSc1d1	gotická
svým	svůj	k3xOyFgInSc7	svůj
rozvrhem	rozvrh	k1gInSc7	rozvrh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
renesanční	renesanční	k2eAgMnSc1d1	renesanční
svou	svůj	k3xOyFgFnSc7	svůj
náplní	náplň	k1gFnSc7	náplň
<g/>
.	.	kIx.	.
</s>
<s>
Protorenesance	Protorenesance	k1gFnSc1	Protorenesance
končí	končit	k5eAaImIp3nS	končit
černou	černý	k2eAgFnSc7d1	černá
smrtí	smrt	k1gFnSc7	smrt
(	(	kIx(	(
<g/>
1348	[number]	k4	1348
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
následné	následný	k2eAgFnSc2d1	následná
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
ukončily	ukončit	k5eAaPmAgInP	ukončit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
vyšší	vysoký	k2eAgFnPc4d2	vyšší
věci	věc	k1gFnPc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
představiteli	představitel	k1gMnPc7	představitel
renesance	renesance	k1gFnSc2	renesance
jsou	být	k5eAaImIp3nP	být
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
mj.	mj.	kA	mj.
autorem	autor	k1gMnSc7	autor
obrazu	obraz	k1gInSc2	obraz
Mona	Mona	k1gFnSc1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Michelangelo	Michelangela	k1gFnSc5	Michelangela
Buonarroti	Buonarrot	k1gMnPc1	Buonarrot
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
např.	např.	kA	např.
sochu	socha	k1gFnSc4	socha
Davida	David	k1gMnSc2	David
nebo	nebo	k8xC	nebo
Raffael	Raffael	k1gInSc4	Raffael
Santi	Sanť	k1gFnSc2	Sanť
-	-	kIx~	-
autor	autor	k1gMnSc1	autor
Sixtinské	sixtinský	k2eAgFnSc2d1	Sixtinská
madony	madona	k1gFnSc2	madona
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
renesance	renesance	k1gFnSc1	renesance
se	se	k3xPyFc4	se
zrodila	zrodit	k5eAaPmAgFnS	zrodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1401	[number]	k4	1401
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
soutěž	soutěž	k1gFnSc4	soutěž
o	o	k7c4	o
zakázku	zakázka	k1gFnSc4	zakázka
na	na	k7c4	na
reliéf	reliéf	k1gInSc4	reliéf
bronzových	bronzový	k2eAgFnPc2d1	bronzová
dveří	dveře	k1gFnPc2	dveře
baptistéria	baptistérium	k1gNnSc2	baptistérium
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
přiměla	přimět	k5eAaPmAgFnS	přimět
několik	několik	k4yIc4	několik
umělců	umělec	k1gMnPc2	umělec
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
antického	antický	k2eAgNnSc2d1	antické
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
renesančního	renesanční	k2eAgNnSc2d1	renesanční
malířství	malířství	k1gNnSc2	malířství
byl	být	k5eAaImAgInS	být
Tommaso	Tommasa	k1gFnSc5	Tommasa
di	di	k?	di
Giovanni	Giovanň	k1gMnSc3	Giovanň
di	di	k?	di
Simone	Simon	k1gMnSc5	Simon
Guidi	Guid	k1gMnPc1	Guid
–	–	k?	–
Masaccio	Masaccio	k6eAd1	Masaccio
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1401	[number]	k4	1401
<g/>
–	–	k?	–
<g/>
1428	[number]	k4	1428
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
začal	začít	k5eAaPmAgMnS	začít
tvořit	tvořit	k5eAaImF	tvořit
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
Filippo	Filippa	k1gFnSc5	Filippa
Brunelleschi	Brunellesch	k1gMnSc6	Brunellesch
(	(	kIx(	(
<g/>
1377	[number]	k4	1377
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1446	[number]	k4	1446
<g/>
)	)	kIx)	)
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
perspektivu	perspektiva	k1gFnSc4	perspektiva
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
dóm	dóm	k1gInSc4	dóm
florentské	florentský	k2eAgFnSc2d1	florentská
katedrály	katedrála	k1gFnSc2	katedrála
je	být	k5eAaImIp3nS	být
manifestem	manifest	k1gInSc7	manifest
renesanční	renesanční	k2eAgFnSc2d1	renesanční
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Mocným	mocný	k2eAgInSc7d1	mocný
impulsem	impuls	k1gInSc7	impuls
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
nového	nový	k2eAgInSc2d1	nový
slohu	sloh	k1gInSc2	sloh
byl	být	k5eAaImAgInS	být
vynález	vynález	k1gInSc1	vynález
knihtisku	knihtisk	k1gInSc2	knihtisk
(	(	kIx(	(
<g/>
1444	[number]	k4	1444
<g/>
)	)	kIx)	)
a	a	k8xC	a
pád	pád	k1gInSc1	pád
Cařihradu	Cařihrad	k1gInSc2	Cařihrad
(	(	kIx(	(
<g/>
1453	[number]	k4	1453
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mnoho	mnoho	k4c1	mnoho
východních	východní	k2eAgMnPc2d1	východní
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
odešlo	odejít	k5eAaPmAgNnS	odejít
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Scholastika	scholastika	k1gFnSc1	scholastika
byla	být	k5eAaImAgFnS	být
opuštěna	opustit	k5eAaPmNgFnS	opustit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svobodné	svobodný	k2eAgFnSc2d1	svobodná
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Vypálením	vypálení	k1gNnSc7	vypálení
Říma	Řím	k1gInSc2	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1527	[number]	k4	1527
začala	začít	k5eAaPmAgFnS	začít
renesance	renesance	k1gFnSc1	renesance
upadat	upadat	k5eAaPmF	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Svár	svár	k1gInSc1	svár
mezi	mezi	k7c7	mezi
vírou	víra	k1gFnSc7	víra
a	a	k8xC	a
humanismem	humanismus	k1gInSc7	humanismus
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
manýrismu	manýrismus	k1gInSc3	manýrismus
(	(	kIx(	(
<g/>
Mannerism	Mannerism	k1gInSc1	Mannerism
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vynález	vynález	k1gInSc1	vynález
olejomalby	olejomalba	k1gFnSc2	olejomalba
umožnil	umožnit	k5eAaPmAgInS	umožnit
malířství	malířství	k1gNnSc3	malířství
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pro	pro	k7c4	pro
klimatické	klimatický	k2eAgFnPc4d1	klimatická
podmínky	podmínka	k1gFnPc4	podmínka
nebyly	být	k5eNaImAgInP	být
fresky	freska	k1gFnPc4	freska
vhodné	vhodný	k2eAgNnSc4d1	vhodné
–	–	k?	–
např.	např.	kA	např.
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgInSc4d1	barokní
sloh	sloh	k1gInSc4	sloh
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
započal	započnout	k5eAaPmAgInS	započnout
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
barocco	barocco	k1gNnSc1	barocco
či	či	k8xC	či
barroco	barroco	k1gNnSc1	barroco
znamenalo	znamenat	k5eAaImAgNnS	znamenat
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
nepravidelný	pravidelný	k2eNgInSc4d1	nepravidelný
<g/>
,	,	kIx,	,
bizarní	bizarní	k2eAgInSc4d1	bizarní
styl	styl	k1gInSc4	styl
porušující	porušující	k2eAgFnSc2d1	porušující
klasické	klasický	k2eAgFnSc2d1	klasická
konvence	konvence	k1gFnSc2	konvence
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgMnPc1d2	pozdější
klasikové	klasik	k1gMnPc1	klasik
pro	pro	k7c4	pro
baroko	baroko	k1gNnSc4	baroko
neměli	mít	k5eNaImAgMnP	mít
pochopení	pochopení	k1gNnSc4	pochopení
a	a	k8xC	a
označovali	označovat	k5eAaImAgMnP	označovat
jej	on	k3xPp3gMnSc4	on
za	za	k7c4	za
úpadkový	úpadkový	k2eAgInSc4d1	úpadkový
sloh	sloh	k1gInSc4	sloh
až	až	k9	až
do	do	k7c2	do
Wölfflinovy	Wölfflinův	k2eAgFnSc2d1	Wölfflinův
studie	studie	k1gFnSc2	studie
Renaissance	Renaissance	k1gFnSc2	Renaissance
und	und	k?	und
Barock	Barock	k1gMnSc1	Barock
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Baroko	baroko	k1gNnSc1	baroko
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
protireformace	protireformace	k1gFnSc2	protireformace
(	(	kIx(	(
<g/>
katolické	katolický	k2eAgFnSc2d1	katolická
reformace	reformace	k1gFnSc2	reformace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zakladateli	zakladatel	k1gMnPc7	zakladatel
baroka	baroko	k1gNnSc2	baroko
jsou	být	k5eAaImIp3nP	být
Annibale	Annibal	k1gInSc6	Annibal
Carracci	Carracec	k1gInSc6	Carracec
a	a	k8xC	a
Caravaggio	Caravaggio	k1gMnSc1	Caravaggio
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
šerosvitu	šerosvit	k1gInSc2	šerosvit
<g/>
.	.	kIx.	.
</s>
<s>
Odlišný	odlišný	k2eAgInSc1d1	odlišný
charakter	charakter	k1gInSc1	charakter
má	mít	k5eAaImIp3nS	mít
baroko	baroko	k1gNnSc4	baroko
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
církevním	církevní	k2eAgInSc7d1	církevní
galikalismem	galikalismus	k1gInSc7	galikalismus
<g/>
.	.	kIx.	.
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
baroko	baroko	k1gNnSc1	baroko
tak	tak	k6eAd1	tak
má	mít	k5eAaImIp3nS	mít
klasicizující	klasicizující	k2eAgInSc4d1	klasicizující
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
v	v	k7c6	v
protestantských	protestantský	k2eAgFnPc6d1	protestantská
zemích	zem	k1gFnPc6	zem
má	mít	k5eAaImIp3nS	mít
baroko	baroko	k1gNnSc1	baroko
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
životním	životní	k2eAgInSc6d1	životní
stylu	styl	k1gInSc6	styl
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc7	jeho
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
nošení	nošení	k1gNnSc2	nošení
vysokých	vysoký	k2eAgFnPc2d1	vysoká
paruk	paruka	k1gFnPc2	paruka
přirozených	přirozený	k2eAgFnPc2d1	přirozená
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Rokokový	rokokový	k2eAgInSc1d1	rokokový
sloh	sloh	k1gInSc1	sloh
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
sloh	sloh	k1gInSc1	sloh
uznáván	uznáván	k2eAgInSc1d1	uznáván
zejména	zejména	k9	zejména
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
jako	jako	k8xS	jako
reakce	reakce	k1gFnPc4	reakce
na	na	k7c4	na
vkus	vkus	k1gInSc4	vkus
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Velikého	veliký	k2eAgMnSc2d1	veliký
a	a	k8xC	a
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Benátek	Benátky	k1gFnPc2	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
jako	jako	k9	jako
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
styl	styl	k1gInSc4	styl
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
interiérů	interiér	k1gInPc2	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Rozpoznávacím	rozpoznávací	k2eAgInSc7d1	rozpoznávací
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
asymetrie	asymetrie	k1gFnSc1	asymetrie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
životním	životní	k2eAgInSc6d1	životní
stylu	styl	k1gInSc6	styl
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc7	jeho
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
nošení	nošení	k1gNnSc2	nošení
napudrovaných	napudrovaný	k2eAgFnPc2d1	napudrovaná
(	(	kIx(	(
<g/>
bílých	bílý	k2eAgFnPc2d1	bílá
<g/>
)	)	kIx)	)
paruk	paruka	k1gFnPc2	paruka
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
vynálezem	vynález	k1gInSc7	vynález
porcelánu	porcelán	k1gInSc2	porcelán
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgFnPc7d1	typická
barvami	barva	k1gFnPc7	barva
byla	být	k5eAaImAgFnS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
zlatá	zlatý	k2eAgFnSc1d1	zlatá
a	a	k8xC	a
pastelové	pastelový	k2eAgFnPc1d1	pastelová
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
odráželo	odrážet	k5eAaImAgNnS	odrážet
frivolitu	frivolita	k1gFnSc4	frivolita
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
však	však	k9	však
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
pozdní	pozdní	k2eAgNnSc1d1	pozdní
baroko	baroko	k1gNnSc1	baroko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
podmínkách	podmínka	k1gFnPc6	podmínka
nemá	mít	k5eNaImIp3nS	mít
charakter	charakter	k1gInSc4	charakter
samostatného	samostatný	k2eAgInSc2d1	samostatný
slohu	sloh	k1gInSc2	sloh
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Klasicismus	klasicismus	k1gInSc1	klasicismus
neboli	neboli	k8xC	neboli
neoklasicismus	neoklasicismus	k1gInSc1	neoklasicismus
se	se	k3xPyFc4	se
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
oživení	oživení	k1gNnPc2	oživení
antiky	antika	k1gFnSc2	antika
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
místo	místo	k1gNnSc1	místo
Říma	Řím	k1gInSc2	Řím
je	být	k5eAaImIp3nS	být
vzorem	vzor	k1gInSc7	vzor
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenkově	myšlenkově	k6eAd1	myšlenkově
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
racionalismus	racionalismus	k1gInSc4	racionalismus
a	a	k8xC	a
ctnost	ctnost	k1gFnSc4	ctnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Umění	umění	k1gNnSc1	umění
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
období	období	k1gNnSc4	období
přechodu	přechod	k1gInSc2	přechod
od	od	k7c2	od
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
epoch	epocha	k1gFnPc2	epocha
historických	historický	k2eAgInPc2d1	historický
slohů	sloh	k1gInPc2	sloh
k	k	k7c3	k
modernímu	moderní	k2eAgNnSc3d1	moderní
umění	umění	k1gNnSc3	umění
různě	různě	k6eAd1	různě
se	se	k3xPyFc4	se
uplatňujících	uplatňující	k2eAgFnPc2d1	uplatňující
a	a	k8xC	a
mísících	mísící	k2eAgInPc2d1	mísící
uměleckých	umělecký	k2eAgInPc2d1	umělecký
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
nacházelo	nacházet	k5eAaImAgNnS	nacházet
ideál	ideál	k1gInSc4	ideál
zprvu	zprvu	k6eAd1	zprvu
v	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
harmonii	harmonie	k1gFnSc6	harmonie
a	a	k8xC	a
lidských	lidský	k2eAgInPc6d1	lidský
citech	cit	k1gInPc6	cit
(	(	kIx(	(
<g/>
romantismus	romantismus	k1gInSc1	romantismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
idealizované	idealizovaný	k2eAgFnSc6d1	idealizovaná
minulosti	minulost	k1gFnSc6	minulost
(	(	kIx(	(
<g/>
historismus	historismus	k1gInSc1	historismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yQgFnSc4	který
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
zájem	zájem	k1gInSc4	zájem
i	i	k8xC	i
probuzené	probuzený	k2eAgNnSc4d1	probuzené
národního	národní	k2eAgNnSc2d1	národní
cítění	cítění	k1gNnSc2	cítění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
umění	umění	k1gNnSc6	umění
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
měl	mít	k5eAaImAgMnS	mít
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
vývoj	vývoj	k1gInSc4	vývoj
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
od	od	k7c2	od
nových	nový	k2eAgFnPc2d1	nová
konstrukcí	konstrukce	k1gFnPc2	konstrukce
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
přes	přes	k7c4	přes
vynález	vynález	k1gInSc4	vynález
fotografie	fotografia	k1gFnSc2	fotografia
na	na	k7c4	na
malířství	malířství	k1gNnSc4	malířství
až	až	k9	až
po	po	k7c4	po
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výrobu	výroba	k1gFnSc4	výroba
měnící	měnící	k2eAgFnSc4d1	měnící
podobu	podoba	k1gFnSc4	podoba
užitého	užitý	k2eAgNnSc2d1	užité
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Preromantismus	preromantismus	k1gInSc1	preromantismus
je	být	k5eAaImIp3nS	být
umělecký	umělecký	k2eAgInSc4d1	umělecký
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xC	jako
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
klasicismus	klasicismus	k1gInSc4	klasicismus
a	a	k8xC	a
osvícenství	osvícenství	k1gNnSc4	osvícenství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
výrazem	výraz	k1gInSc7	výraz
nového	nový	k2eAgInSc2d1	nový
životního	životní	k2eAgInSc2d1	životní
postoje	postoj	k1gInSc2	postoj
a	a	k8xC	a
uměleckého	umělecký	k2eAgInSc2d1	umělecký
stylu	styl	k1gInSc2	styl
měšťanstva	měšťanstvo	k1gNnSc2	měšťanstvo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nastupující	nastupující	k2eAgFnSc2d1	nastupující
společenské	společenský	k2eAgFnSc2d1	společenská
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
zdůrazňováním	zdůrazňování	k1gNnSc7	zdůrazňování
citových	citový	k2eAgFnPc2d1	citová
hodnot	hodnota	k1gFnPc2	hodnota
prostého	prostý	k2eAgMnSc2d1	prostý
<g/>
,	,	kIx,	,
nezkaženého	zkažený	k2eNgMnSc2d1	nezkažený
člověka	člověk	k1gMnSc2	člověk
svůj	svůj	k3xOyFgInSc4	svůj
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
nespravedlnosti	nespravedlnost	k1gFnSc3	nespravedlnost
a	a	k8xC	a
poutům	pout	k1gInPc3	pout
feudalismu	feudalismus	k1gInSc2	feudalismus
a	a	k8xC	a
proti	proti	k7c3	proti
chladnému	chladný	k2eAgNnSc3d1	chladné
rozumářství	rozumářství	k1gNnSc3	rozumářství
klasicismu	klasicismus	k1gInSc2	klasicismus
jako	jako	k8xC	jako
uměleckého	umělecký	k2eAgInSc2d1	umělecký
směru	směr	k1gInSc2	směr
aristokracie	aristokracie	k1gFnSc2	aristokracie
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
myšlenkové	myšlenkový	k2eAgNnSc1d1	myšlenkové
hnutí	hnutí	k1gNnSc1	hnutí
často	často	k6eAd1	často
nazýváno	nazývat	k5eAaImNgNnS	nazývat
sentimentalismem	sentimentalismus	k1gInSc7	sentimentalismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Význačným	význačný	k2eAgMnSc7d1	význačný
preromantikem	preromantik	k1gMnSc7	preromantik
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
Jean-Jacques	Jean-Jacques	k1gMnSc1	Jean-Jacques
Rousseau	Rousseau	k1gMnSc1	Rousseau
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
byli	být	k5eAaImAgMnP	být
výrazem	výraz	k1gInSc7	výraz
preromantismu	preromantismus	k1gInSc2	preromantismus
tzv.	tzv.	kA	tzv.
jezerní	jezerní	k2eAgMnPc1d1	jezerní
básníci	básník	k1gMnPc1	básník
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
hnutí	hnutí	k1gNnSc2	hnutí
Bouře	bouř	k1gFnSc2	bouř
a	a	k8xC	a
vzdor	vzdor	k1gInSc1	vzdor
(	(	kIx(	(
<g/>
1773	[number]	k4	1773
<g/>
–	–	k?	–
<g/>
1781	[number]	k4	1781
<g/>
,	,	kIx,	,
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Drang	k1gInSc1	Drang
<g/>
)	)	kIx)	)
–	–	k?	–
Goethea	Goethea	k1gMnSc1	Goethea
Schiller	Schiller	k1gMnSc1	Schiller
<g/>
.	.	kIx.	.
</s>
<s>
Romantismus	romantismus	k1gInSc1	romantismus
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgInS	zrodit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
nebyl	být	k5eNaImAgInS	být
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
jen	jen	k9	jen
avantgardu	avantgarda	k1gFnSc4	avantgarda
–	–	k?	–
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Romantici	romantik	k1gMnPc1	romantik
proto	proto	k8xC	proto
pěstovali	pěstovat	k5eAaImAgMnP	pěstovat
kult	kult	k1gInSc4	kult
výlučnosti	výlučnost	k1gFnSc2	výlučnost
génia	génius	k1gMnSc2	génius
<g/>
.	.	kIx.	.
</s>
<s>
Romantici	romantik	k1gMnPc1	romantik
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
klid	klid	k1gInSc4	klid
<g/>
,	,	kIx,	,
harmonii	harmonie	k1gFnSc4	harmonie
<g/>
,	,	kIx,	,
vyrovnanost	vyrovnanost	k1gFnSc4	vyrovnanost
<g/>
,	,	kIx,	,
idealizaci	idealizace	k1gFnSc4	idealizace
a	a	k8xC	a
racionalitu	racionalita	k1gFnSc4	racionalita
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Romantismus	romantismus	k1gInSc1	romantismus
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
individualitu	individualita	k1gFnSc4	individualita
<g/>
,	,	kIx,	,
subjektivnost	subjektivnost	k1gFnSc4	subjektivnost
<g/>
,	,	kIx,	,
<g/>
iracionalitu	iracionalita	k1gFnSc4	iracionalita
<g/>
,	,	kIx,	,
imaginaci	imaginace	k1gFnSc4	imaginace
<g/>
,	,	kIx,	,
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
,	,	kIx,	,
spontánnost	spontánnost	k1gFnSc4	spontánnost
<g/>
,	,	kIx,	,
emotivnost	emotivnost	k1gFnSc4	emotivnost
<g/>
,	,	kIx,	,
vizionářství	vizionářství	k1gNnSc4	vizionářství
a	a	k8xC	a
transcendentalitu	transcendentalita	k1gFnSc4	transcendentalita
<g/>
.	.	kIx.	.
</s>
<s>
Romantismus	romantismus	k1gInSc1	romantismus
znamenal	znamenat	k5eAaImAgInS	znamenat
zrod	zrod	k1gInSc4	zrod
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
a	a	k8xC	a
folklóru	folklór	k1gInSc2	folklór
<g/>
;	;	kIx,	;
obdiv	obdiv	k1gInSc1	obdiv
ke	k	k7c3	k
středověku	středověk	k1gInSc3	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
romantismus	romantismus	k1gInSc1	romantismus
přežíval	přežívat	k5eAaImAgInS	přežívat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Historismus	historismus	k1gInSc1	historismus
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc4d1	souhrnné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
tzv	tzv	kA	tzv
"	"	kIx"	"
<g/>
neoslohů	neosloh	k1gMnPc2	neosloh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
romantismus	romantismus	k1gInSc4	romantismus
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
užíván	užívat	k5eAaImNgInS	užívat
například	například	k6eAd1	například
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
dobových	dobový	k2eAgNnPc2d1	dobové
šlechtických	šlechtický	k2eAgNnPc2d1	šlechtické
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
také	také	k9	také
při	při	k7c6	při
dekorování	dekorování	k1gNnSc6	dekorování
štukových	štukový	k2eAgFnPc2d1	štuková
fasád	fasáda	k1gFnPc2	fasáda
činžovních	činžovní	k2eAgInPc2d1	činžovní
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Historismus	historismus	k1gInSc1	historismus
čerpá	čerpat	k5eAaImIp3nS	čerpat
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
gotického	gotický	k2eAgInSc2d1	gotický
<g/>
,	,	kIx,	,
renesančního	renesanční	k2eAgInSc2d1	renesanční
a	a	k8xC	a
barokního	barokní	k2eAgInSc2d1	barokní
slohu	sloh	k1gInSc2	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Pozdní	pozdní	k2eAgFnSc1d1	pozdní
fáze	fáze	k1gFnSc1	fáze
historismu	historismus	k1gInSc2	historismus
začíná	začínat	k5eAaImIp3nS	začínat
synteticky	synteticky	k6eAd1	synteticky
směšovat	směšovat	k5eAaImF	směšovat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
používané	používaný	k2eAgInPc4d1	používaný
stavební	stavební	k2eAgInPc4d1	stavební
slohy	sloh	k1gInPc4	sloh
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
také	také	k6eAd1	také
někdy	někdy	k6eAd1	někdy
eklektismem	eklektismus	k1gInSc7	eklektismus
<g/>
.	.	kIx.	.
</s>
<s>
Realismus	realismus	k1gInSc1	realismus
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
umělecký	umělecký	k2eAgInSc1d1	umělecký
program	program	k1gInSc1	program
přijat	přijmout	k5eAaPmNgInS	přijmout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1850	[number]	k4	1850
až	až	k9	až
1880	[number]	k4	1880
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
protagonisté	protagonista	k1gMnPc1	protagonista
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
umělost	umělost	k1gFnSc4	umělost
neoklasicismu	neoklasicismus	k1gInSc2	neoklasicismus
i	i	k8xC	i
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
jejich	jejich	k3xOp3gFnSc2	jejich
pozornosti	pozornost	k1gFnSc2	pozornost
byl	být	k5eAaImAgInS	být
život	život	k1gInSc1	život
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
nižší	nízký	k2eAgFnSc2d2	nižší
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Filozofickým	filozofický	k2eAgInSc7d1	filozofický
programem	program	k1gInSc7	program
byl	být	k5eAaImAgInS	být
pozitivismus	pozitivismus	k1gInSc1	pozitivismus
<g/>
.	.	kIx.	.
</s>
<s>
Realismus	realismus	k1gInSc1	realismus
byl	být	k5eAaImAgInS	být
povzbuzen	povzbudit	k5eAaPmNgInS	povzbudit
vynálezem	vynález	k1gInSc7	vynález
fotografie	fotografia	k1gFnSc2	fotografia
a	a	k8xC	a
masovým	masový	k2eAgNnSc7d1	masové
rozšířením	rozšíření	k1gNnSc7	rozšíření
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Realistickým	realistický	k2eAgMnSc7d1	realistický
malířem	malíř	k1gMnSc7	malíř
byl	být	k5eAaImAgMnS	být
Gustave	Gustav	k1gMnSc5	Gustav
Courbet	Courbet	k1gMnSc1	Courbet
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
však	však	k9	však
mainstream	mainstream	k6eAd1	mainstream
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
těsným	těsný	k2eAgMnPc3d1	těsný
předchůdcům	předchůdce	k1gMnPc3	předchůdce
patří	patřit	k5eAaImIp3nS	patřit
Barbizonská	Barbizonský	k2eAgFnSc1d1	Barbizonská
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
–	–	k?	–
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
venkovské	venkovský	k2eAgInPc4d1	venkovský
náměty	námět	k1gInPc4	námět
<g/>
.	.	kIx.	.
</s>
<s>
Honoré	Honorý	k2eAgInPc1d1	Honorý
Daumier	Daumier	k1gInSc1	Daumier
byl	být	k5eAaImAgInS	být
spíše	spíše	k9	spíše
kreslíř	kreslíř	k1gMnSc1	kreslíř
<g/>
.	.	kIx.	.
</s>
<s>
Naturalismus	naturalismus	k1gInSc1	naturalismus
je	být	k5eAaImIp3nS	být
realismus	realismus	k1gInSc4	realismus
do	do	k7c2	do
důsledků	důsledek	k1gInPc2	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgInSc1	první
výraz	výraz	k1gInSc1	výraz
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc1d1	poslední
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
aplikací	aplikace	k1gFnSc7	aplikace
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
darwinismu	darwinismus	k1gInSc2	darwinismus
<g/>
)	)	kIx)	)
do	do	k7c2	do
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Umělecká	umělecký	k2eAgNnPc1d1	umělecké
díla	dílo	k1gNnPc1	dílo
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
determinismem	determinismus	k1gInSc7	determinismus
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
jsou	být	k5eAaImIp3nP	být
hnáni	hnát	k5eAaImNgMnP	hnát
pudy	pud	k1gInPc7	pud
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
pouhý	pouhý	k2eAgMnSc1d1	pouhý
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
snaží	snažit	k5eAaImIp3nS	snažit
zdržet	zdržet	k5eAaPmF	zdržet
komentáře	komentář	k1gInPc4	komentář
<g/>
.	.	kIx.	.
</s>
<s>
Impresionismus	impresionismus	k1gInSc1	impresionismus
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
stylem	styl	k1gInSc7	styl
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
má	mít	k5eAaImIp3nS	mít
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
realismu	realismus	k1gInSc3	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Existoval	existovat	k5eAaImAgMnS	existovat
v	v	k7c6	v
malířství	malířství	k1gNnSc6	malířství
<g/>
,	,	kIx,	,
hudbě	hudba	k1gFnSc6	hudba
a	a	k8xC	a
literatuře	literatura	k1gFnSc6	literatura
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1867	[number]	k4	1867
<g/>
až	až	k9	až
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
veřejně	veřejně	k6eAd1	veřejně
známým	známý	k1gMnPc3	známý
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
předchůdci	předchůdce	k1gMnPc1	předchůdce
byli	být	k5eAaImAgMnP	být
Édouard	Édouard	k1gMnSc1	Édouard
Manet	Manet	k1gMnSc1	Manet
<g/>
,	,	kIx,	,
Eugene	Eugen	k1gInSc5	Eugen
Boudin	Boudin	k2eAgInSc1d1	Boudin
a	a	k8xC	a
J.	J.	kA	J.
B.	B.	kA	B.
Jongkind	Jongkind	k1gMnSc1	Jongkind
<g/>
.	.	kIx.	.
</s>
<s>
Symbolismus	symbolismus	k1gInSc1	symbolismus
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgInS	zrodit
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
básnictví	básnictví	k1gNnSc6	básnictví
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jako	jako	k8xC	jako
revolta	revolta	k1gFnSc1	revolta
proti	proti	k7c3	proti
Parnasu	Parnas	k1gInSc3	Parnas
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
měl	mít	k5eAaImAgMnS	mít
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
předchůdců	předchůdce	k1gMnPc2	předchůdce
(	(	kIx(	(
<g/>
Baudelaire	Baudelair	k1gMnSc5	Baudelair
<g/>
,	,	kIx,	,
Verlaine	Verlain	k1gMnSc5	Verlain
<g/>
,	,	kIx,	,
Rimbaud	Rimbaudo	k1gNnPc2	Rimbaudo
a	a	k8xC	a
Mallarmé	Mallarmý	k2eAgNnSc1d1	Mallarmé
–	–	k?	–
potomní	potomní	k2eAgMnSc1d1	potomní
vůdce	vůdce	k1gMnSc1	vůdce
symbolistů	symbolista	k1gMnPc2	symbolista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Básníci	básník	k1gMnPc1	básník
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
zachytit	zachytit	k5eAaPmF	zachytit
mystérium	mystérium	k1gNnSc1	mystérium
hlubšího	hluboký	k2eAgInSc2d2	hlubší
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
byl	být	k5eAaImAgInS	být
symbolismus	symbolismus	k1gInSc1	symbolismus
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
dekadence	dekadence	k1gFnSc1	dekadence
(	(	kIx(	(
<g/>
=	=	kIx~	=
úpadek	úpadek	k1gInSc1	úpadek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
znakem	znak	k1gInSc7	znak
byl	být	k5eAaImAgInS	být
volný	volný	k2eAgInSc1d1	volný
verš	verš	k1gInSc1	verš
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
hnutí	hnutí	k1gNnSc1	hnutí
upadalo	upadat	k5eAaPmAgNnS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Secesní	secesní	k2eAgInSc1d1	secesní
sloh	sloh	k1gInSc1	sloh
kvetl	kvést	k5eAaImAgInS	kvést
zejména	zejména	k9	zejména
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1890	[number]	k4	1890
a	a	k8xC	a
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Odklání	odklánět	k5eAaImIp3nS	odklánět
se	se	k3xPyFc4	se
od	od	k7c2	od
klasických	klasický	k2eAgInPc2d1	klasický
historických	historický	k2eAgInPc2d1	historický
slohů	sloh	k1gInPc2	sloh
a	a	k8xC	a
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	s	k7c7	s
plošnými	plošný	k2eAgInPc7d1	plošný
ornamenty	ornament	k1gInPc7	ornament
se	s	k7c7	s
stylizovanými	stylizovaný	k2eAgInPc7d1	stylizovaný
přírodními	přírodní	k2eAgInPc7d1	přírodní
tvary	tvar	k1gInPc7	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
poslední	poslední	k2eAgInSc4d1	poslední
univerzální	univerzální	k2eAgInSc4d1	univerzální
evropský	evropský	k2eAgInSc4d1	evropský
sloh	sloh	k1gInSc4	sloh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
prosadil	prosadit	k5eAaPmAgInS	prosadit
jednotný	jednotný	k2eAgInSc1d1	jednotný
estetický	estetický	k2eAgInSc1d1	estetický
styl	styl	k1gInSc1	styl
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
oblastí	oblast	k1gFnPc2	oblast
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
i	i	k9	i
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Moderní	moderní	k2eAgInSc1d1	moderní
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
Umění	umění	k1gNnSc1	umění
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Art	Art	k?	Art
Deco	Deco	k1gMnSc1	Deco
Art	Art	k1gMnSc1	Art
Deco	Deco	k1gMnSc1	Deco
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
omezil	omezit	k5eAaPmAgInS	omezit
na	na	k7c4	na
užité	užitý	k2eAgNnSc4d1	užité
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
architekturu	architektura	k1gFnSc4	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Purismus	purismus	k1gInSc1	purismus
Purismus	purismus	k1gInSc1	purismus
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
pozdní	pozdní	k2eAgFnSc2d1	pozdní
secese	secese	k1gFnSc2	secese
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
významné	významný	k2eAgMnPc4d1	významný
představitele	představitel	k1gMnPc4	představitel
patří	patřit	k5eAaImIp3nS	patřit
Adolf	Adolf	k1gMnSc1	Adolf
Loos	Loos	k1gInSc1	Loos
<g/>
.	.	kIx.	.
</s>
<s>
Purismus	purismus	k1gInSc1	purismus
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
plynule	plynule	k6eAd1	plynule
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
Funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
jeho	jeho	k3xOp3gFnSc7	jeho
systematizovanou	systematizovaný	k2eAgFnSc7d1	systematizovaná
podobou	podoba	k1gFnSc7	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Dadaismus	dadaismus	k1gInSc1	dadaismus
Dadaismus	dadaismus	k1gInSc1	dadaismus
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
projevoval	projevovat	k5eAaImAgInS	projevovat
se	se	k3xPyFc4	se
snahou	snaha	k1gFnSc7	snaha
vyvolat	vyvolat	k5eAaPmF	vyvolat
a	a	k8xC	a
postihnout	postihnout	k5eAaPmF	postihnout
absurdno	absurdno	k1gNnSc4	absurdno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
nesmyslnost	nesmyslnost	k1gFnSc4	nesmyslnost
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Dadaismu	dadaismus	k1gInSc2	dadaismus
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rodí	rodit	k5eAaImIp3nP	rodit
Surrealismus	surrealismus	k1gInSc4	surrealismus
<g/>
.	.	kIx.	.
</s>
<s>
Konstruktivismus	konstruktivismus	k1gInSc1	konstruktivismus
Konstruktivismus	konstruktivismus	k1gInSc1	konstruktivismus
byl	být	k5eAaImAgInS	být
pěstován	pěstovat	k5eAaImNgInS	pěstovat
především	především	k9	především
v	v	k7c6	v
Sovětském	sovětský	k2eAgNnSc6d1	sovětské
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
konstruktivistické	konstruktivistický	k2eAgFnPc1d1	konstruktivistická
tendence	tendence	k1gFnPc1	tendence
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
<g/>
.	.	kIx.	.
</s>
<s>
Klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
viditelnou	viditelný	k2eAgFnSc4d1	viditelná
<g/>
,	,	kIx,	,
nezdobenou	zdobený	k2eNgFnSc4d1	nezdobená
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
Funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
je	být	k5eAaImIp3nS	být
sloh	sloh	k1gInSc4	sloh
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
hlavním	hlavní	k2eAgMnSc7d1	hlavní
zakladatelem	zakladatel	k1gMnSc7	zakladatel
je	být	k5eAaImIp3nS	být
architekt	architekt	k1gMnSc1	architekt
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
<g/>
.	.	kIx.	.
</s>
<s>
Klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
funkčnost	funkčnost	k1gFnSc4	funkčnost
a	a	k8xC	a
účelnost	účelnost	k1gFnSc4	účelnost
<g/>
.	.	kIx.	.
</s>
<s>
Surrealismus	surrealismus	k1gInSc1	surrealismus
Surrealismus	surrealismus	k1gInSc1	surrealismus
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgInS	zrodit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c6	na
podvědomí	podvědomí	k1gNnSc6	podvědomí
<g/>
,	,	kIx,	,
představy	představa	k1gFnPc4	představa
<g/>
,	,	kIx,	,
sny	sen	k1gInPc4	sen
<g/>
,	,	kIx,	,
zdánlivou	zdánlivý	k2eAgFnSc4d1	zdánlivá
náhodnost	náhodnost	k1gFnSc4	náhodnost
a	a	k8xC	a
nesmyslnost	nesmyslnost	k1gFnSc4	nesmyslnost
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
projevem	projev	k1gInSc7	projev
surrealismu	surrealismus	k1gInSc2	surrealismus
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
tzv.	tzv.	kA	tzv.
automatická	automatický	k2eAgFnSc1d1	automatická
poesie	poesie	k1gFnSc1	poesie
<g/>
.	.	kIx.	.
</s>
