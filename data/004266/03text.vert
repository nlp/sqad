<s>
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
vlaky	vlak	k1gInPc1	vlak
je	být	k5eAaImIp3nS	být
československý	československý	k2eAgInSc1d1	československý
film	film	k1gInSc1	film
natočený	natočený	k2eAgInSc1d1	natočený
režisérem	režisér	k1gMnSc7	režisér
Jiřím	Jiří	k1gMnSc7	Jiří
Menzelem	Menzel	k1gMnSc7	Menzel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
novely	novela	k1gFnSc2	novela
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hrabala	Hrabal	k1gMnSc2	Hrabal
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
období	období	k1gNnSc6	období
protektorátu	protektorát	k1gInSc2	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
Předlohou	předloha	k1gFnSc7	předloha
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
skutečná	skutečný	k2eAgFnSc1d1	skutečná
událost	událost	k1gFnSc1	událost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
výbuch	výbuch	k1gInSc1	výbuch
německého	německý	k2eAgInSc2d1	německý
muničního	muniční	k2eAgInSc2d1	muniční
vlaku	vlak	k1gInSc2	vlak
<g/>
,	,	kIx,	,
odpáleného	odpálený	k2eAgInSc2d1	odpálený
časovým	časový	k2eAgInSc7d1	časový
spínačem	spínač	k1gInSc7	spínač
podskupinou	podskupina	k1gFnSc7	podskupina
partyzánské	partyzánský	k2eAgFnSc2d1	Partyzánská
skupiny	skupina	k1gFnSc2	skupina
Podřipsko	Podřipsko	k1gNnSc1	Podřipsko
z	z	k7c2	z
Lysé	Lysá	k1gFnSc2	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1945	[number]	k4	1945
nedaleko	nedaleko	k7c2	nedaleko
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
Stratov	Stratov	k1gInSc4	Stratov
a	a	k8xC	a
zážitky	zážitek	k1gInPc4	zážitek
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hrabala	Hrabal	k1gMnSc2	Hrabal
z	z	k7c2	z
nádraží	nádraží	k1gNnSc2	nádraží
v	v	k7c6	v
Kostomlatech	Kostomle	k1gNnPc6	Kostomle
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
zastával	zastávat	k5eAaImAgInS	zastávat
funkci	funkce	k1gFnSc4	funkce
výpravčího	výpravčí	k1gMnSc2	výpravčí
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
postavy	postava	k1gFnPc1	postava
-	-	kIx~	-
přednosta	přednosta	k1gMnSc1	přednosta
Němeček	Němeček	k1gMnSc1	Němeček
<g/>
,	,	kIx,	,
výpravčí	výpravčí	k1gMnSc1	výpravčí
Hubička	hubička	k1gFnSc1	hubička
<g/>
...	...	k?	...
jsou	být	k5eAaImIp3nP	být
původně	původně	k6eAd1	původně
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Dobrovice	Dobrovice	k1gFnSc2	Dobrovice
<g/>
.	.	kIx.	.
</s>
<s>
Nevinný	vinný	k2eNgMnSc1d1	nevinný
mladíček	mladíček	k1gMnSc1	mladíček
Miloš	Miloš	k1gMnSc1	Miloš
Hrma	hrma	k1gFnSc1	hrma
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Neckář	Neckář	k1gMnSc1	Neckář
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zaučuje	zaučovat	k5eAaImIp3nS	zaučovat
coby	coby	k?	coby
novopečený	novopečený	k2eAgMnSc1d1	novopečený
železničář	železničář	k1gMnSc1	železničář
na	na	k7c6	na
malé	malý	k2eAgFnSc6d1	malá
železniční	železniční	k2eAgFnSc6d1	železniční
stanici	stanice	k1gFnSc6	stanice
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
prožívá	prožívat	k5eAaImIp3nS	prožívat
nelehké	lehký	k2eNgNnSc4d1	nelehké
období	období	k1gNnSc4	období
svého	svůj	k3xOyFgNnSc2	svůj
dospívání	dospívání	k1gNnSc2	dospívání
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
kvůli	kvůli	k7c3	kvůli
milostným	milostný	k2eAgInPc3d1	milostný
neúspěchům	neúspěch	k1gInPc3	neúspěch
se	se	k3xPyFc4	se
však	však	k9	však
zaučí	zaučit	k5eAaPmIp3nS	zaučit
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
intimním	intimní	k2eAgInSc6d1	intimní
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
ženě	žena	k1gFnSc3	žena
(	(	kIx(	(
<g/>
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
zachová	zachovat	k5eAaPmIp3nS	zachovat
jako	jako	k9	jako
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
vyhodit	vyhodit	k5eAaPmF	vyhodit
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
muniční	muniční	k2eAgInSc4d1	muniční
vlak	vlak	k1gInSc4	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
amatérské	amatérský	k2eAgFnSc6d1	amatérská
diverzní	diverzní	k2eAgFnSc6d1	diverzní
akci	akce	k1gFnSc6	akce
však	však	k9	však
zahyne	zahynout	k5eAaPmIp3nS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
v	v	k7c6	v
železniční	železniční	k2eAgFnSc6d1	železniční
stanici	stanice	k1gFnSc6	stanice
Loděnice	loděnice	k1gFnSc2	loděnice
u	u	k7c2	u
Berouna	Beroun	k1gInSc2	Beroun
na	na	k7c6	na
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Beroun	Beroun	k1gInSc1	Beroun
přes	přes	k7c4	přes
Rudnou	rudný	k2eAgFnSc4d1	Rudná
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
filmu	film	k1gInSc2	film
Jiří	Jiří	k1gMnSc1	Jiří
Menzel	Menzel	k1gMnSc1	Menzel
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
také	také	k9	také
zahrál	zahrát	k5eAaPmAgInS	zahrát
malou	malý	k2eAgFnSc4d1	malá
epizodní	epizodní	k2eAgFnSc4d1	epizodní
roli	role	k1gFnSc4	role
lékaře	lékař	k1gMnSc2	lékař
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
natočení	natočení	k1gNnSc2	natočení
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
v	v	k7c6	v
Loděnici	loděnice	k1gFnSc6	loděnice
sešli	sejít	k5eAaPmAgMnP	sejít
jeho	jeho	k3xOp3gMnPc1	jeho
tvůrci	tvůrce	k1gMnPc1	tvůrce
a	a	k8xC	a
na	na	k7c6	na
výpravní	výpravní	k2eAgFnSc6d1	výpravní
budově	budova	k1gFnSc6	budova
byla	být	k5eAaImAgFnS	být
odhalena	odhalit	k5eAaPmNgFnS	odhalit
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
připomínající	připomínající	k2eAgNnSc1d1	připomínající
natáčení	natáčení	k1gNnSc4	natáčení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
získal	získat	k5eAaPmAgInS	získat
film	film	k1gInSc1	film
Cenu	cena	k1gFnSc4	cena
Americké	americký	k2eAgFnSc2d1	americká
akademie	akademie	k1gFnSc2	akademie
filmových	filmový	k2eAgNnPc2d1	filmové
umění	umění	k1gNnPc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
-	-	kIx~	-
Oscara	Oscara	k1gFnSc1	Oscara
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
PSOTOVÁ	Psotová	k1gFnSc1	Psotová
<g/>
,	,	kIx,	,
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
.	.	kIx.	.
</s>
<s>
Vybrané	vybraná	k1gFnPc1	vybraná
filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
tvorby	tvorba	k1gFnSc2	tvorba
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hrabala	Hrabal	k1gMnSc2	Hrabal
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
TEPLANOVÁ	TEPLANOVÁ	kA	TEPLANOVÁ
<g/>
,	,	kIx,	,
Lenka	Lenka	k1gFnSc1	Lenka
<g/>
.	.	kIx.	.
</s>
<s>
Srovnání	srovnání	k1gNnSc1	srovnání
textu	text	k1gInSc2	text
Hrabalovy	Hrabalův	k2eAgFnSc2d1	Hrabalova
novely	novela	k1gFnSc2	novela
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
vlaky	vlak	k1gInPc1	vlak
a	a	k8xC	a
scénáře	scénář	k1gInPc1	scénář
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Ivo	Ivo	k1gMnSc1	Ivo
Martinec	Martinec	k1gMnSc1	Martinec
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
vlaky	vlak	k1gInPc1	vlak
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
vlaky	vlak	k1gInPc1	vlak
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
