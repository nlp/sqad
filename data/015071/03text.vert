<s>
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
na	na	k7c6
Zimních	zimní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2010	#num#	k4
–	–	k?
turnaj	turnaj	k1gInSc1
ženy	žena	k1gFnSc2
</s>
<s>
Lední	lední	k2eAgInSc1d1
hokejna	hokejna	k6eAd1
Zimních	zimní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2010	#num#	k4
<g/>
Turnaj	turnaj	k1gInSc1
žen	žena	k1gFnPc2
</s>
<s>
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Pořadatel	pořadatel	k1gMnSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
ledního	lední	k2eAgMnSc2d1
hokejeMezinárodní	hokejeMezinárodní	k2eAgInSc4d1
olympijský	olympijský	k2eAgInSc4d1
výbor	výbor	k1gInSc4
Dějiště	dějiště	k1gNnSc2
</s>
<s>
Canada	Canada	k1gFnSc1
Hockey	Hockea	k1gFnSc2
Place	plac	k1gInSc6
<g/>
,	,	kIx,
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
Datum	datum	k1gNnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
Startující	startující	k2eAgFnSc4d1
</s>
<s>
8	#num#	k4
týmů	tým	k1gInPc2
Medailisté	medailista	k1gMnPc5
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
CAN	CAN	kA
<g/>
)	)	kIx)
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
Finsko	Finsko	k1gNnSc1
(	(	kIx(
<g/>
FIN	Fina	k1gFnPc2
<g/>
)	)	kIx)
Soutěže	soutěž	k1gFnPc1
</s>
<s>
turnajmužiženy	turnajmužižen	k2eAgFnPc1d1
</s>
<s>
soupiskamužiženy	soupiskamužižen	k2eAgFnPc1d1
</s>
<s>
kvalifikacemužiženy	kvalifikacemužižen	k2eAgFnPc1d1
</s>
<s>
<<	<<	k?
2006	#num#	k4
<g/>
Seznam	seznam	k1gInSc4
medailistů	medailista	k1gMnPc2
Seznam	seznam	k1gInSc4
medailistek	medailistka	k1gFnPc2
<g/>
2014	#num#	k4
>>	>>	k?
</s>
<s>
Ženský	ženský	k2eAgInSc1d1
turnaj	turnaj	k1gInSc1
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
na	na	k7c6
zimních	zimní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2010	#num#	k4
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
ve	v	k7c6
Vancouveru	Vancouver	k1gInSc6
v	v	k7c6
Britské	britský	k2eAgFnSc6d1
Kolumbii	Kolumbie	k1gFnSc6
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
ve	v	k7c6
dnech	den	k1gInPc6
13	#num#	k4
<g/>
.	.	kIx.
až	až	k9
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soutěžilo	soutěžit	k5eAaImAgNnS
osm	osm	k4xCc1
týmů	tým	k1gInPc2
rozdělených	rozdělený	k2eAgInPc2d1
do	do	k7c2
dvou	dva	k4xCgFnPc2
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kanada	Kanada	k1gFnSc1
zvítězila	zvítězit	k5eAaPmAgFnS
ve	v	k7c6
finále	finále	k1gNnSc6
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
nad	nad	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
získaly	získat	k5eAaPmAgInP
stříbro	stříbro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zápas	zápas	k1gInSc1
o	o	k7c4
bronz	bronz	k1gInSc4
vyhrálo	vyhrát	k5eAaPmAgNnS
Finsko	Finsko	k1gNnSc1
vítězstvím	vítězství	k1gNnSc7
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
nad	nad	k7c7
Švédskem	Švédsko	k1gNnSc7
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Kvalifikace	kvalifikace	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Lední	lední	k2eAgInSc4d1
hokej	hokej	k1gInSc4
na	na	k7c6
Zimních	zimní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2010	#num#	k4
–	–	k?
kvalifikace	kvalifikace	k1gFnSc2
ženy	žena	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
turnaje	turnaj	k1gInSc2
se	se	k3xPyFc4
přímo	přímo	k6eAd1
kvalifikovalo	kvalifikovat	k5eAaBmAgNnS
6	#num#	k4
nejlepších	dobrý	k2eAgInPc2d3
celků	celek	k1gInPc2
podle	podle	k7c2
ženského	ženský	k2eAgInSc2d1
žebříčku	žebříček	k1gInSc2
IIHF	IIHF	kA
z	z	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týmy	tým	k1gInPc7
od	od	k7c2
7	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
18	#num#	k4
<g/>
.	.	kIx.
místa	místo	k1gNnPc4
v	v	k7c6
tomto	tento	k3xDgInSc6
žebříčku	žebříček	k1gInSc6
sehrály	sehrát	k5eAaPmAgFnP
olympijskou	olympijský	k2eAgFnSc4d1
kvalifikaci	kvalifikace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
celky	celek	k1gInPc1
na	na	k7c4
7	#num#	k4
<g/>
.	.	kIx.
až	až	k9
12	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
byly	být	k5eAaImAgFnP
nasazeny	nasadit	k5eAaPmNgFnP
přímo	přímo	k6eAd1
do	do	k7c2
hlavní	hlavní	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
kvalifikace	kvalifikace	k1gFnSc2
<g/>
,	,	kIx,
zbylé	zbylý	k2eAgInPc1d1
celky	celek	k1gInPc1
hrály	hrát	k5eAaImAgInP
předkvalifikaci	předkvalifikace	k1gFnSc4
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yQgFnSc2,k3yIgFnSc2,k3yRgFnSc2
postoupily	postoupit	k5eAaPmAgFnP
nejlepší	dobrý	k2eAgInPc4d3
2	#num#	k4
celky	celek	k1gInPc4
do	do	k7c2
hlavní	hlavní	k2eAgFnSc2d1
kvalifikace	kvalifikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hlavní	hlavní	k2eAgFnSc2d1
kvalifikace	kvalifikace	k1gFnSc2
vedla	vést	k5eAaImAgFnS
cesta	cesta	k1gFnSc1
pro	pro	k7c4
2	#num#	k4
nejlepší	dobrý	k2eAgInPc4d3
celky	celek	k1gInPc4
na	na	k7c4
olympijský	olympijský	k2eAgInSc4d1
turnaj	turnaj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Předkvalifikace	předkvalifikace	k1gFnSc1
se	se	k3xPyFc4
hrála	hrát	k5eAaImAgFnS
v	v	k7c6
září	září	k1gNnSc6
2008	#num#	k4
a	a	k8xC
hlavní	hlavní	k2eAgFnSc2d1
kvalifikace	kvalifikace	k1gFnSc2
v	v	k7c6
listopadu	listopad	k1gInSc6
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Týmy	tým	k1gInPc1
přímo	přímo	k6eAd1
kvalifikované	kvalifikovaný	k2eAgInPc1d1
na	na	k7c4
olympijský	olympijský	k2eAgInSc4d1
turnaj	turnaj	k1gInSc4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Účastníci	účastník	k1gMnPc1
hlavní	hlavní	k2eAgFnSc2d1
kvalifikace	kvalifikace	k1gFnSc2
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Čína	Čína	k1gFnSc1
–	–	k?
postup	postup	k1gInSc4
na	na	k7c4
olympijský	olympijský	k2eAgInSc4d1
turnaj	turnaj	k1gInSc4
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Účastníci	účastník	k1gMnPc1
předkvalifikace	předkvalifikace	k1gFnSc2
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Norsko	Norsko	k1gNnSc4
–	–	k?
postup	postup	k1gInSc1
do	do	k7c2
hlavní	hlavní	k2eAgFnSc2d1
kvalifikace	kvalifikace	k1gFnSc2
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc4
–	–	k?
postup	postup	k1gInSc1
do	do	k7c2
hlavní	hlavní	k2eAgFnSc2d1
kvalifikace	kvalifikace	k1gFnSc2
–	–	k?
tam	tam	k6eAd1
úspěch	úspěch	k1gInSc1
⇒	⇒	k?
postup	postup	k1gInSc4
na	na	k7c4
olympijský	olympijský	k2eAgInSc4d1
turnaj	turnaj	k1gInSc4
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Systém	systém	k1gInSc1
turnaje	turnaj	k1gInSc2
</s>
<s>
Týmy	tým	k1gInPc1
budou	být	k5eAaImBp3nP
rozděleny	rozdělit	k5eAaPmNgInP
do	do	k7c2
dvou	dva	k4xCgFnPc2
skupin	skupina	k1gFnPc2
po	po	k7c6
4	#num#	k4
týmech	tým	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
každé	každý	k3xTgFnSc6
skupině	skupina	k1gFnSc6
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
hrát	hrát	k5eAaImF
jednokolově	jednokolově	k6eAd1
každý	každý	k3xTgMnSc1
s	s	k7c7
každým	každý	k3xTgMnSc7
<g/>
.	.	kIx.
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
2	#num#	k4
týmy	tým	k1gInPc7
z	z	k7c2
každé	každý	k3xTgFnSc2
skupiny	skupina	k1gFnSc2
postoupí	postoupit	k5eAaPmIp3nP
do	do	k7c2
semifinále	semifinále	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězové	vítěz	k1gMnPc1
semifinále	semifinále	k1gNnSc2
se	se	k3xPyFc4
utkají	utkat	k5eAaPmIp3nP
o	o	k7c4
zlato	zlato	k1gNnSc4
ve	v	k7c6
finále	finále	k1gNnSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
poražení	poražený	k2eAgMnPc1d1
semifinalisté	semifinalista	k1gMnPc1
budou	být	k5eAaImBp3nP
hrát	hrát	k5eAaImF
o	o	k7c4
bronz	bronz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týmy	tým	k1gInPc1
vyřazené	vyřazený	k2eAgInPc1d1
ve	v	k7c6
skupinách	skupina	k1gFnPc6
budou	být	k5eAaImBp3nP
hrát	hrát	k5eAaImF
o	o	k7c4
umístění	umístění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
VP	VP	kA
</s>
<s>
PP	PP	kA
</s>
<s>
P	P	kA
</s>
<s>
GV	GV	kA
</s>
<s>
GI	GI	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
−	−	k?
</s>
<s>
B	B	kA
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
33000412399	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
320011015	#num#	k4
<g/>
−	−	k?
<g/>
56	#num#	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
31002615	#num#	k4
<g/>
−	−	k?
<g/>
93	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
30003429	#num#	k4
<g/>
−	−	k?
<g/>
250	#num#	k4
</s>
<s>
Zápasy	zápas	k1gInPc1
</s>
<s>
Všechny	všechen	k3xTgInPc1
časy	čas	k1gInPc1
jsou	být	k5eAaImIp3nP
místní	místní	k2eAgInPc1d1
(	(	kIx(
<g/>
UTC	UTC	kA
<g/>
−	−	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tj.	tj.	kA
SEČ	SEČ	kA
<g/>
−	−	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
3	#num#	k4
–	–	k?
0	#num#	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
1	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
-	-	kIx~
0	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
Rundqvistová	Rundqvistová	k1gFnSc1
13	#num#	k4
<g/>
'	'	kIx"
<g/>
Enströmová	Enströmová	k1gFnSc1
32	#num#	k4
<g/>
'	'	kIx"
<g/>
Uden-Johanssonová	Uden-Johanssonová	k1gFnSc1
42	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2009	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
18	#num#	k4
–	–	k?
0	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
7	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
-	-	kIx~
0	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
IRWIN	IRWIN	kA
Haley	Halea	k1gFnSc2
1	#num#	k4
<g/>
:	:	kIx,
<g/>
39	#num#	k4
<g/>
'	'	kIx"
<g/>
BONHOMME	bonhomme	k1gMnSc1
Tessa	Tess	k1gMnSc2
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
'	'	kIx"
<g/>
AGOSTA	AGOSTA	kA
Meghan	Meghan	k1gInSc4
5	#num#	k4
<g/>
:	:	kIx,
<g/>
38	#num#	k4
<g/>
'	'	kIx"
<g/>
MACLEOD	MACLEOD	kA
Carla	Carla	k1gFnSc1
8	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
'	'	kIx"
<g/>
AGOSTA	AGOSTA	kA
Meghan	Meghan	k1gInSc4
11	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
<g/>
'	'	kIx"
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
KINGSBURY	KINGSBURY	kA
Gina	Gin	k1gInSc2
15	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
'	'	kIx"
<g/>
SOSTORICS	SOSTORICS	kA
Colleen	Colleen	k1gInSc4
16	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
'	'	kIx"
<g/>
VAILLANCOURT	VAILLANCOURT	kA
Sarah	Sarah	k1gFnSc1
23	#num#	k4
<g/>
:	:	kIx,
<g/>
42	#num#	k4
<g/>
'	'	kIx"
<g/>
POULIN	POULIN	kA
Marie-Philip	Marie-Philip	k1gInSc4
27	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
'	'	kIx"
<g/>
AGOSTA	AGOSTA	kA
Meghan	Meghan	k1gInSc4
30	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
'	'	kIx"
<g/>
HEFFORD	HEFFORD	kA
Jayna	Jayna	k1gFnSc1
32	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
'	'	kIx"
<g/>
OUELLETTE	OUELLETTE	kA
Caroline	Carolin	k1gInSc5
32	#num#	k4
<g/>
:	:	kIx,
<g/>
44	#num#	k4
<g/>
'	'	kIx"
<g/>
MACLEOD	MACLEOD	kA
Carla	Carla	k1gFnSc1
36	#num#	k4
<g/>
:	:	kIx,
<g/>
42	#num#	k4
<g/>
'	'	kIx"
<g/>
HEFFORD	HEFFORD	kA
Jayna	Jayna	k1gFnSc1
44	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
'	'	kIx"
<g/>
IRWIN	IRWIN	kA
Haley	Halea	k1gFnSc2
44	#num#	k4
<g/>
:	:	kIx,
<g/>
37	#num#	k4
<g/>
'	'	kIx"
<g/>
PIPER	PIPER	kA
Cherie	Cherie	k1gFnSc1
46	#num#	k4
<g/>
:	:	kIx,
<g/>
54	#num#	k4
<g/>
'	'	kIx"
<g/>
HEFFORD	HEFFORD	kA
Jayna	Jayna	k1gFnSc1
51	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
'	'	kIx"
<g/>
KINGSBURY	KINGSBURY	kA
Gina	Gina	k1gFnSc1
52	#num#	k4
<g/>
:	:	kIx,
<g/>
52	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
6	#num#	k4
–	–	k?
2	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
3	#num#	k4
-	-	kIx~
2	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
-	-	kIx~
0	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
WINBERG	WINBERG	kA
Pernilla	Pernilla	k1gFnSc1
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
'	'	kIx"
<g/>
WINBERG	WINBERG	kA
Pernilla	Pernilla	k1gFnSc1
12	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
'	'	kIx"
<g/>
ASSERHOLT	ASSERHOLT	kA
Jenni	Jeneň	k1gFnSc6
16	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
'	'	kIx"
<g/>
WINBERG	WINBERG	kA
Pernilla	Pernilla	k1gFnSc1
29	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
'	'	kIx"
<g/>
HOLMLOV	HOLMLOV	kA
Elin	Elin	k2eAgInSc1d1
46	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
'	'	kIx"
<g/>
WINBERG	WINBERG	kA
Pernilla	Pernilla	k1gFnSc1
53	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
DZURNAKOVA	DZURNAKOVA	kA
Anna	Anna	k1gFnSc1
9	#num#	k4
<g/>
:	:	kIx,
<g/>
52	#num#	k4
<g/>
'	'	kIx"
<g/>
CUPKOVA	CUPKOVA	kA
Nicol	Nicol	k1gInSc4
13	#num#	k4
<g/>
:	:	kIx,
<g/>
29	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
1	#num#	k4
–	–	k?
10	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
0	#num#	k4
-	-	kIx~
2	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
-	-	kIx~
3	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
-	-	kIx~
5	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
LEIMGRUBER	LEIMGRUBER	kA
Darcia	Darcia	k1gFnSc1
29	#num#	k4
<g/>
:	:	kIx,
<g/>
46	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
APPS	APPS	kA
Gillian	Gillian	k1gInSc4
6	#num#	k4
<g/>
:	:	kIx,
<g/>
27	#num#	k4
<g/>
'	'	kIx"
<g/>
VAILLANCOURT	VAILLANCOURT	kA
Sarah	Sarah	k1gFnSc1
14	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
'	'	kIx"
<g/>
PIPER	PIPER	kA
Cherie	Cherie	k1gFnSc1
22	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
<g/>
'	'	kIx"
<g/>
AGOSTA	AGOSTA	kA
Meghan	Meghan	k1gInSc4
28	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
'	'	kIx"
<g/>
AGOSTA	AGOSTA	kA
Meghan	Meghan	k1gInSc4
31	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
'	'	kIx"
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
HEFFORD	HEFFORD	kA
Jayna	Jayn	k1gInSc2
40	#num#	k4
<g/>
:	:	kIx,
<g/>
54	#num#	k4
<g/>
'	'	kIx"
<g/>
WARD	WARD	kA
Catherine	Catherin	k1gInSc5
49	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
'	'	kIx"
<g/>
POULIN	POULIN	kA
Marie-Philip	Marie-Philip	k1gInSc4
49	#num#	k4
<g/>
:	:	kIx,
<g/>
27	#num#	k4
<g/>
'	'	kIx"
<g/>
JOHNSTON	JOHNSTON	kA
Rebecca	Rebecca	k1gFnSc1
50	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
<g/>
'	'	kIx"
<g/>
WICKENHEISER	WICKENHEISER	kA
Hayley	Haylea	k1gFnSc2
51	#num#	k4
<g/>
:	:	kIx,
<g/>
55	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
2	#num#	k4
–	–	k?
5	#num#	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
1	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
-	-	kIx~
1	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
-	-	kIx~
4	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
CULIKOVA	CULIKOVA	kA
Janka	Janka	k1gFnSc1
7	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
'	'	kIx"
<g/>
CULIKOVA	CULIKOVA	kA
Janka	Janka	k1gFnSc1
40	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
MARTY	Marta	k1gFnPc1
Stefanie	Stefanie	k1gFnSc1
35	#num#	k4
<g/>
:	:	kIx,
<g/>
39	#num#	k4
<g/>
'	'	kIx"
<g/>
MARTY	Marta	k1gFnSc2
Stefanie	Stefanie	k1gFnSc2
50	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
<g/>
'	'	kIx"
<g/>
BENZ	BENZ	kA
Sara	Sara	k1gFnSc1
51	#num#	k4
<g/>
:	:	kIx,
<g/>
18	#num#	k4
<g/>
'	'	kIx"
<g/>
LEHMANN	LEHMANN	kA
Kathrin	Kathrin	k1gInSc4
56	#num#	k4
<g/>
:	:	kIx,
<g/>
28	#num#	k4
<g/>
'	'	kIx"
<g/>
MARTY	Marta	k1gFnSc2
Stefanie	Stefanie	k1gFnSc2
58	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
13	#num#	k4
–	–	k?
1	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
5	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
-	-	kIx~
1	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
AGOSTA	AGOSTA	kA
Meghan	Meghan	k1gInSc4
6	#num#	k4
<g/>
:	:	kIx,
<g/>
58	#num#	k4
<g/>
'	'	kIx"
<g/>
POULIN	POULIN	kA
Marie-Philip	Marie-Philip	k1gInSc4
9	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
'	'	kIx"
<g/>
PIPER	PIPER	kA
Cherie	Cherie	k1gFnSc1
13	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
'	'	kIx"
<g/>
VAILLANCOURT	VAILLANCOURT	kA
Sarah	Sarah	k1gFnSc1
15	#num#	k4
<g/>
:	:	kIx,
<g/>
27	#num#	k4
<g/>
'	'	kIx"
<g/>
BONHOMME	bonhomme	k1gMnSc1
Tessa	Tess	k1gMnSc2
15	#num#	k4
<g/>
:	:	kIx,
<g/>
57	#num#	k4
<g/>
'	'	kIx"
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
AGOSTA	AGOSTA	kA
Meghan	Meghan	k1gInSc4
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
'	'	kIx"
<g/>
HEFFORD	HEFFORD	kA
Jayna	Jayna	k1gFnSc1
25	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
'	'	kIx"
<g/>
WICKENHEISER	WICKENHEISER	kA
Hayley	Haylea	k1gFnSc2
25	#num#	k4
<g/>
:	:	kIx,
<g/>
36	#num#	k4
<g/>
'	'	kIx"
<g/>
APPS	APPS	kA
Gillian	Gillian	k1gInSc4
26	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
'	'	kIx"
<g/>
AGOSTA	AGOSTA	kA
Meghan	Meghan	k1gInSc4
27	#num#	k4
<g/>
:	:	kIx,
<g/>
59	#num#	k4
<g/>
'	'	kIx"
<g/>
PIPER	PIPER	kA
Cherie	Cherie	k1gFnSc1
29	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
<g/>
'	'	kIx"
<g/>
IRWIN	IRWIN	kA
Haley	Halea	k1gFnSc2
31	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
<g/>
'	'	kIx"
<g/>
APPS	APPS	kA
Gillian	Gillian	k1gInSc4
47	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
TIMGLAS	TIMGLAS	kA
Katarina	Katarina	k1gFnSc1
52	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
VP	VP	kA
</s>
<s>
PP	PP	kA
</s>
<s>
P	P	kA
</s>
<s>
GV	GV	kA
</s>
<s>
GI	GI	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
−	−	k?
</s>
<s>
B	B	kA
</s>
<s>
USA	USA	kA
</s>
<s>
33000311309	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
3200178	#num#	k4
<g/>
−	−	k?
<g/>
16	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
31002319	#num#	k4
<g/>
−	−	k?
<g/>
163	#num#	k4
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
30003316	#num#	k4
<g/>
−	−	k?
<g/>
130	#num#	k4
</s>
<s>
Zápasy	zápas	k1gInPc1
</s>
<s>
Všechny	všechen	k3xTgInPc1
časy	čas	k1gInPc1
jsou	být	k5eAaImIp3nP
místní	místní	k2eAgInPc1d1
(	(	kIx(
<g/>
UTC	UTC	kA
<g/>
−	−	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tj.	tj.	kA
SEČ	SEČ	kA
<g/>
−	−	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
5	#num#	k4
–	–	k?
1	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
1	#num#	k4
-	-	kIx~
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
-	-	kIx~
0	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
SIRVIO	SIRVIO	kA
Saija	Saija	k1gFnSc1
9	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
'	'	kIx"
<g/>
VOUTILAINEN	VOUTILAINEN	kA
Marjo	Marjo	k6eAd1
23	#num#	k4
<g/>
:	:	kIx,
<g/>
59	#num#	k4
<g/>
'	'	kIx"
<g/>
HOVI	HOVI	kA
Venla	Venla	k1gFnSc1
27	#num#	k4
<g/>
:	:	kIx,
<g/>
36	#num#	k4
<g/>
'	'	kIx"
<g/>
TIKKINEN	TIKKINEN	kA
Nina	Nina	k1gFnSc1
42	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
'	'	kIx"
<g/>
TIKKINEN	TIKKINEN	kA
Nina	Nina	k1gFnSc1
49	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
VAFINA	VAFINA	kA
Aleksandra	Aleksandra	k1gFnSc1
6	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
USA	USA	kA
</s>
<s>
12	#num#	k4
–	–	k?
1	#num#	k4
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
5	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
-	-	kIx~
1	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
RUGGIERO	RUGGIERO	kA
Angela	Angela	k1gFnSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
<g/>
'	'	kIx"
<g/>
STACK	STACK	kA
Kelli	Kelle	k1gFnSc4
9	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
<g/>
'	'	kIx"
<g/>
POTTER	POTTER	kA
Jenny	Jenna	k1gFnSc2
14	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
'	'	kIx"
<g/>
DUGGAN	DUGGAN	kA
Meghan	Meghan	k1gInSc4
17	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
<g/>
'	'	kIx"
<g/>
POTTER	POTTER	kA
Jenny	Jenna	k1gFnSc2
18	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
'	'	kIx"
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
POTTER	POTTER	kA
Jenny	Jenna	k1gFnSc2
21	#num#	k4
<g/>
:	:	kIx,
<g/>
18	#num#	k4
<g/>
'	'	kIx"
<g/>
CHESSON	CHESSON	kA
Lisa	Lisa	k1gFnSc1
23	#num#	k4
<g/>
:	:	kIx,
<g/>
46	#num#	k4
<g/>
'	'	kIx"
<g/>
LAMOUREUX	LAMOUREUX	kA
Jocelyne	Jocelyn	k1gInSc5
39	#num#	k4
<g/>
:	:	kIx,
<g/>
39	#num#	k4
<g/>
'	'	kIx"
<g/>
DUGGAN	DUGGAN	kA
Meghan	Meghan	k1gInSc4
34	#num#	k4
<g/>
:	:	kIx,
<g/>
59	#num#	k4
<g/>
'	'	kIx"
<g/>
ENGSTROM	ENGSTROM	kA
Molly	Molla	k1gFnSc2
50	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
<g/>
'	'	kIx"
<g/>
DARWITZ	DARWITZ	kA
Natalie	Natalie	k1gFnSc1
54	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
<g/>
'	'	kIx"
<g/>
CHU	CHU	kA
Julie	Julie	k1gFnSc1
59	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
JIN	JIN	kA
Fengling	Fengling	k1gInSc4
57	#num#	k4
<g/>
:	:	kIx,
<g/>
39	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
2	#num#	k4
–	–	k?
1	#num#	k4
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
0	#num#	k4
-	-	kIx~
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
-	-	kIx~
0	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
RANTAMAKI	RANTAMAKI	kA
Karoliina	Karoliin	k2eAgFnSc1d1
29	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
'	'	kIx"
<g/>
HOVI	HOVI	kA
Venla	Venla	k1gFnSc1
34	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
WANG	WANG	kA
Linuo	Linuo	k6eAd1
18	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
0	#num#	k4
–	–	k?
13	#num#	k4
</s>
<s>
USA	USA	kA
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
0	#num#	k4
-	-	kIx~
5	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
-	-	kIx~
7	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
-	-	kIx~
1	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
LAMOUREUX	LAMOUREUX	kA
Monique	Monique	k1gFnSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
<g/>
'	'	kIx"
<g/>
POTTER	POTTER	kA
Jenny	Jenna	k1gFnSc2
5	#num#	k4
<g/>
:	:	kIx,
<g/>
48	#num#	k4
<g/>
'	'	kIx"
<g/>
THATCHER	THATCHER	kA
Karen	Karen	k1gInSc4
9	#num#	k4
<g/>
:	:	kIx,
<g/>
54	#num#	k4
<g/>
'	'	kIx"
<g/>
CAHOW	CAHOW	kA
Caitlin	Caitlin	k1gInSc4
12	#num#	k4
<g/>
:	:	kIx,
<g/>
57	#num#	k4
<g/>
'	'	kIx"
<g/>
POTTER	POTTER	kA
Jenny	Jenna	k1gFnSc2
15	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
<g/>
'	'	kIx"
<g/>
RUGGIERO	RUGGIERO	kA
<g />
.	.	kIx.
</s>
<s hack="1">
Angela	Angela	k1gFnSc1
20	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
<g/>
'	'	kIx"
<g/>
STACK	STACK	kA
Kelli	Kelle	k1gFnSc4
23	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
'	'	kIx"
<g/>
LAMOUREUX	LAMOUREUX	kA
Jocelyne	Jocelyn	k1gInSc5
26	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
'	'	kIx"
<g/>
DARWITZ	DARWITZ	kA
Natalie	Natalie	k1gFnSc1
27	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
<g/>
'	'	kIx"
<g/>
DARWITZ	DARWITZ	kA
Natalie	Natalie	k1gFnSc1
31	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
'	'	kIx"
<g/>
POTTER	POTTER	kA
Jenny	Jenna	k1gFnSc2
31	#num#	k4
<g/>
:	:	kIx,
<g/>
46	#num#	k4
<g/>
'	'	kIx"
<g/>
ENGSTROM	ENGSTROM	kA
Molly	Molla	k1gFnSc2
33	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
'	'	kIx"
<g/>
CHESSON	CHESSON	kA
Lisa	Lisa	k1gFnSc1
41	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
1	#num#	k4
–	–	k?
2	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
0	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
-	-	kIx~
2	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
-	-	kIx~
0	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
JIN	JIN	kA
Fengling	Fengling	k1gInSc4
37	#num#	k4
<g/>
:	:	kIx,
<g/>
38	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
SMOLENTSEVA	SMOLENTSEVA	kA
Ekaterina	Ekaterina	k1gFnSc1
24	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
'	'	kIx"
<g/>
SERGINA	SERGINA	kA
Marina	Marina	k1gFnSc1
36	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
USA	USA	kA
</s>
<s>
6	#num#	k4
–	–	k?
0	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
4	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
-	-	kIx~
0	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
HU	hu	k0
Julie	Julie	k1gFnSc1
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
'	'	kIx"
<g/>
ENGSTROM	ENGSTROM	kA
Molly	Molla	k1gFnSc2
10	#num#	k4
<g/>
:	:	kIx,
<g/>
47	#num#	k4
<g/>
'	'	kIx"
<g/>
DUGGAN	DUGGAN	kA
Meghan	Meghan	k1gInSc4
11	#num#	k4
<g/>
:	:	kIx,
<g/>
29	#num#	k4
<g/>
'	'	kIx"
<g/>
DARWITZ	DARWITZ	kA
Natalie	Natalie	k1gFnSc1
18	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
'	'	kIx"
<g/>
KNIGHT	KNIGHT	kA
Hilary	Hilara	k1gFnSc2
31	#num#	k4
<g/>
:	:	kIx,
<g/>
48	#num#	k4
<g/>
'	'	kIx"
<g/>
THATCHER	THATCHER	kA
Karen	Karen	k1gInSc4
58	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
O	o	k7c6
5	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
6	#num#	k4
–	–	k?
0	#num#	k4
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
2	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
-	-	kIx~
0	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
LEHMANN	LEHMANN	kA
Kathrin	Kathrin	k1gInSc4
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
'	'	kIx"
<g/>
MARTY	Marta	k1gFnSc2
Stefanie	Stefanie	k1gFnSc2
7	#num#	k4
<g/>
:	:	kIx,
<g/>
53	#num#	k4
<g/>
'	'	kIx"
<g/>
NUSSBAUM	NUSSBAUM	kA
Lucrece	Lucrece	k1gFnSc1
37	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
'	'	kIx"
<g/>
MARTY	Marta	k1gFnSc2
Stefanie	Stefanie	k1gFnSc2
39	#num#	k4
<g/>
:	:	kIx,
<g/>
44	#num#	k4
<g/>
'	'	kIx"
<g/>
MARTY	Marta	k1gFnSc2
Stefanie	Stefanie	k1gFnSc2
54	#num#	k4
<g/>
:	:	kIx,
<g/>
54	#num#	k4
<g/>
'	'	kIx"
<g/>
MARTY	Marta	k1gFnSc2
Stefanie	Stefanie	k1gFnSc2
58	#num#	k4
<g/>
:	:	kIx,
<g/>
48	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
4	#num#	k4
–	–	k?
2	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
-	-	kIx~
1	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
-	-	kIx~
1	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
SOTNIKOVA	SOTNIKOVA	kA
Tatiana	Tatiana	k1gFnSc1
7	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
<g/>
'	'	kIx"
<g/>
GAVRILOVA	GAVRILOVA	kA
Iya	Iya	k1gFnSc1
18	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
'	'	kIx"
<g/>
TERENTEVA	TERENTEVA	kA
Svetlana	Svetlana	k1gFnSc1
33	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
<g/>
'	'	kIx"
<g/>
GAVRILOVA	GAVRILOVA	kA
Iya	Iya	k1gFnSc1
50	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
PRAVLIKOVA	PRAVLIKOVA	kA
Petra	Petra	k1gFnSc1
23	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
'	'	kIx"
<g/>
KAPUSTOVA	Kapustův	k2eAgFnSc1d1
Jana	Jana	k1gFnSc1
40	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
O	o	k7c6
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
3	#num#	k4
–	–	k?
1	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
0	#num#	k4
-	-	kIx~
1	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
-	-	kIx~
0	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
WANG	WANG	kA
Linuo	Linuo	k6eAd1
37	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
'	'	kIx"
<g/>
SUN	Sun	kA
Rui	Rui	k1gFnSc1
45	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
'	'	kIx"
<g/>
WANG	WANG	kA
Linuo	Linuo	k6eAd1
51	#num#	k4
<g/>
:	:	kIx,
<g/>
44	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
PRAVLIKOVA	PRAVLIKOVA	kA
Petra	Petra	k1gFnSc1
10	#num#	k4
<g/>
:	:	kIx,
<g/>
37	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
O	o	k7c6
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
2	#num#	k4
–	–	k?
1	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
UBC	UBC	kA
Winter	Winter	k1gMnSc1
Sports	Sports	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
0	#num#	k4
:	:	kIx,
1	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
:	:	kIx,
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
:	:	kIx,
0	#num#	k4
-	-	kIx~
na	na	k7c6
SN	SN	kA
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
MARTY	Marta	k1gFnPc1
Stefanie	Stefanie	k1gFnSc1
34	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
'	'	kIx"
<g/>
MARTY	Marta	k1gFnSc2
Stefanie	Stefanie	k1gFnSc2
(	(	kIx(
<g/>
SN	SN	kA
<g/>
)	)	kIx)
</s>
<s>
BURINA	BURINA	kA
Tatiana	Tatiana	k1gFnSc1
9	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Play	play	k0
off	off	k?
</s>
<s>
Semifinále	semifinále	k1gNnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
5	#num#	k4
–	–	k?
0	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
General	Generat	k5eAaImAgInS,k5eAaPmAgInS
Motors	Motors	k1gInSc1
Place	plac	k1gInSc6
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
2	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
-	-	kIx~
0	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
PIPER	PIPER	kA
Cherie	Cherie	k1gFnSc1
5	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
'	'	kIx"
<g/>
IRWIN	IRWIN	kA
Haley	Halea	k1gFnSc2
14	#num#	k4
<g/>
:	:	kIx,
<g/>
36	#num#	k4
<g/>
'	'	kIx"
<g/>
AGOSTA	AGOSTA	kA
Meghan	Meghan	k1gInSc4
36	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
'	'	kIx"
<g/>
IRWIN	IRWIN	kA
Haley	Halea	k1gFnSc2
44	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
'	'	kIx"
<g/>
OUELLETTE	OUELLETTE	kA
Caroline	Carolin	k1gInSc5
58	#num#	k4
<g/>
:	:	kIx,
<g/>
57	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
USA	USA	kA
</s>
<s>
9	#num#	k4
–	–	k?
1	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
General	Generat	k5eAaPmAgInS,k5eAaImAgInS
Motors	Motors	k1gInSc1
Place	plac	k1gInSc6
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
-	-	kIx~
0	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
-	-	kIx~
1	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
-	-	kIx~
0	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
LAMOUREUX	LAMOUREUX	kA
Monique	Monique	k1gFnSc1
7	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
'	'	kIx"
<g/>
DUGGAN	DUGGAN	kA
Meghan	Meghan	k1gInSc4
8	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
'	'	kIx"
<g/>
RUGGIERO	RUGGIERO	kA
Angela	Angela	k1gFnSc1
23	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
'	'	kIx"
<g/>
CAHOW	CAHOW	kA
Caitlin	Caitlin	k1gInSc4
25	#num#	k4
<g/>
:	:	kIx,
<g/>
58	#num#	k4
<g/>
'	'	kIx"
<g/>
THATCHER	THATCHER	kA
Karen	Karen	k1gInSc4
33	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
<g/>
'	'	kIx"
<g/>
LAMOUREUX	LAMOUREUX	kA
Monique	Monique	k1gFnSc1
45	#num#	k4
<g/>
:	:	kIx,
<g/>
59	#num#	k4
<g/>
'	'	kIx"
<g/>
WEILAND	WEILAND	kA
Kerry	Kerra	k1gFnSc2
47	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
'	'	kIx"
<g/>
STACK	STACK	kA
Kelli	Kelle	k1gFnSc4
55	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
'	'	kIx"
<g/>
LAMOUREUX	LAMOUREUX	kA
Monique	Monique	k1gFnSc1
57	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
WINBERG	WINBERG	kA
Pernilla	Pernilla	k1gFnSc1
29	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
O	o	k7c6
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
3	#num#	k4
–	–	k?
2	#num#	k4
PP	PP	kA
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
General	Generat	k5eAaImAgInS,k5eAaPmAgInS
Motors	Motors	k1gInSc1
Place	plac	k1gInSc6
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
0	#num#	k4
:	:	kIx,
0	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
:	:	kIx,
1	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
:	:	kIx,
1	#num#	k4
+	+	kIx~
1	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
PELTTARI	PELTTARI	kA
Heidi	Heid	k1gMnPc1
24	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
'	'	kIx"
<g/>
KARVINEN	KARVINEN	kA
Michelle	Michelle	k1gFnSc1
36	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
'	'	kIx"
<g/>
RANTAMAKI	RANTAMAKI	kA
Karoliina	Karoliin	k2eAgFnSc1d1
62	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
ROOTH	ROOTH	kA
Maria	Maria	k1gFnSc1
32	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
'	'	kIx"
<g/>
RUNDQVIST	RUNDQVIST	kA
Danijela	Danijela	k1gFnSc1
45	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
2	#num#	k4
–	–	k?
0	#num#	k4
</s>
<s>
USA	USA	kA
</s>
<s>
General	Generat	k5eAaImAgInS,k5eAaPmAgInS
Motors	Motors	k1gInSc1
Place	plac	k1gInSc6
<g/>
,	,	kIx,
Vancouver	Vancouver	k1gInSc1
</s>
<s>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
2	#num#	k4
:	:	kIx,
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
:	:	kIx,
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Souhrn	souhrn	k1gInSc1
zápasu	zápas	k1gInSc2
</s>
<s>
POULIN	POULIN	kA
Marie-Philip	Marie-Philip	k1gInSc4
13	#num#	k4
<g/>
:	:	kIx,
<g/>
55	#num#	k4
<g/>
'	'	kIx"
<g/>
POULIN	POULIN	kA
Marie-Philip	Marie-Philip	k1gInSc4
16	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Konečné	Konečné	k2eAgNnSc1d1
umístění	umístění	k1gNnSc1
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko	Rusko	k1gNnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Lední	lední	k2eAgInSc4d1
hokej	hokej	k1gInSc4
na	na	k7c6
Zimních	zimní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2010	#num#	k4
–	–	k?
turnaj	turnaj	k1gInSc1
ženy	žena	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
na	na	k7c4
ZOH	ZOH	kA
2010	#num#	k4
<g/>
,	,	kIx,
vancouver	vancouver	k1gInSc1
<g/>
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Olympijské	olympijský	k2eAgFnPc1d1
medailistky	medailistka	k1gFnPc1
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
–	–	k?
turnaj	turnaj	k1gInSc1
2010	#num#	k4
ve	v	k7c6
Vancouveru	Vancouver	k1gInSc6
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
CAN	CAN	kA
<g/>
)	)	kIx)
</s>
<s>
Meghan	Meghan	k1gInSc1
Agostaová	Agostaová	k1gFnSc1
•	•	k?
Gillian	Gilliana	k1gFnPc2
Appsová	Appsový	k2eAgFnSc1d1
•	•	k?
Tessa	Tessa	k1gFnSc1
Bonhommeová	Bonhommeová	k1gFnSc1
•	•	k?
Jennifer	Jennifra	k1gFnPc2
Botterillová	Botterillový	k2eAgFnSc1d1
•	•	k?
Jayna	Jayna	k1gFnSc1
Heffordová	Heffordová	k1gFnSc1
"	"	kIx"
<g/>
A	A	kA
<g/>
"	"	kIx"
•	•	k?
Haley	Halea	k1gFnSc2
Irwinová	Irwinová	k1gFnSc1
•	•	k?
Rebecca	Rebecca	k1gFnSc1
Johnstonová	Johnstonový	k2eAgFnSc1d1
•	•	k?
Becky	Beck	k1gInPc4
Kellarová	Kellarová	k1gFnSc1
•	•	k?
Gina	Gin	k2eAgFnSc1d1
Kingsburyová	Kingsburyová	k1gFnSc1
•	•	k?
Charline	Charlin	k1gInSc5
Labontéová	Labontéový	k2eAgFnSc1d1
•	•	k?
Carla	Carla	k1gFnSc1
MacLeodová	MacLeodový	k2eAgFnSc1d1
•	•	k?
Meaghan	Meaghan	k1gInSc1
Mikkelsonová	Mikkelsonová	k1gFnSc1
•	•	k?
Caroline	Carolin	k1gInSc5
Ouelletteová	Ouelletteová	k1gFnSc1
"	"	kIx"
<g/>
A	A	kA
<g/>
"	"	kIx"
•	•	k?
Cherie	Cherie	k1gFnSc1
Piperová	Piperová	k1gFnSc1
•	•	k?
Marie-Philip	Marie-Philip	k1gInSc1
Poulinová	Poulinový	k2eAgFnSc1d1
•	•	k?
Colleen	Collena	k1gFnPc2
Sostoricsová	Sostoricsový	k2eAgFnSc1d1
•	•	k?
Kim	Kim	k1gFnSc1
St-Pierreová	St-Pierreová	k1gFnSc1
•	•	k?
Shannon	Shannona	k1gFnPc2
Szabadosová	Szabadosový	k2eAgFnSc1d1
•	•	k?
Sarah	Sarah	k1gFnSc1
Vaillancourtová	Vaillancourtová	k1gFnSc1
•	•	k?
Catherine	Catherin	k1gInSc5
Wardová	Wardová	k1gFnSc1
•	•	k?
Hayley	Hayley	k1gInPc1
Wickenheiserová	Wickenheiserová	k1gFnSc1
"	"	kIx"
<g/>
C	C	kA
<g/>
"	"	kIx"
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
Kacey	Kace	k2eAgFnPc1d1
Bellamyová	Bellamyová	k1gFnSc1
•	•	k?
Caitlin	Caitlina	k1gFnPc2
Cahowová	Cahowový	k2eAgFnSc1d1
•	•	k?
Natalie	Natalie	k1gFnSc1
Darwitzová	Darwitzová	k1gFnSc1
"	"	kIx"
<g/>
C	C	kA
<g/>
"	"	kIx"
•	•	k?
Meghan	Meghan	k1gInSc1
Dugganová	Dugganová	k1gFnSc1
•	•	k?
Molly	Molla	k1gMnSc2
Engstromová	Engstromový	k2eAgFnSc1d1
•	•	k?
Lisa	Lisa	k1gFnSc1
Chessonová	Chessonový	k2eAgFnSc1d1
•	•	k?
Julie	Julie	k1gFnSc1
Chuová	Chuová	k1gFnSc1
"	"	kIx"
<g/>
A	A	kA
<g/>
"	"	kIx"
•	•	k?
Hilary	Hilara	k1gFnSc2
Knightová	Knightová	k1gFnSc1
•	•	k?
Jocelyne	Jocelyn	k1gInSc5
Lamoureuxová	Lamoureuxový	k2eAgFnSc1d1
•	•	k?
Monique	Monique	k1gFnSc1
Lamoureuxová	Lamoureuxový	k2eAgFnSc1d1
•	•	k?
Erika	Erika	k1gFnSc1
Lawlerová	Lawlerový	k2eAgFnSc1d1
•	•	k?
Gisele	Gisela	k1gFnSc3
Marvinová	Marvinová	k1gFnSc1
•	•	k?
Brianne	Briann	k1gInSc5
McLaughlinová	McLaughlinový	k2eAgFnSc1d1
•	•	k?
Jenny	Jenn	k1gInPc1
Potterová	Potterová	k1gFnSc1
"	"	kIx"
<g/>
A	A	kA
<g/>
"	"	kIx"
•	•	k?
Angela	Angela	k1gFnSc1
Ruggieroová	Ruggieroová	k1gFnSc1
"	"	kIx"
<g/>
A	A	kA
<g/>
"	"	kIx"
•	•	k?
Molly	Molla	k1gMnSc2
Schausová	Schausový	k2eAgFnSc1d1
•	•	k?
Kelli	Kelle	k1gFnSc3
Stacková	Stacková	k1gFnSc1
•	•	k?
Karen	Karen	k1gInSc1
Thatcherová	Thatcherová	k1gFnSc1
•	•	k?
Jessie	Jessie	k1gFnSc1
Vetterová	Vetterová	k1gFnSc1
•	•	k?
Kerry	Kerra	k1gMnSc2
Weilandová	Weilandový	k2eAgFnSc1d1
•	•	k?
Jinelle	Jinelle	k1gFnSc1
Zaugg-Siergiejová	Zaugg-Siergiejový	k2eAgFnSc1d1
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Finsko	Finsko	k1gNnSc1
(	(	kIx(
<g/>
FIN	Fin	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Jenni	Jenn	k1gMnPc1
Hiirikoskiová	Hiirikoskiový	k2eAgNnPc1d1
"	"	kIx"
<g/>
A	a	k9
<g/>
"	"	kIx"
•	•	k?
Anne	Anne	k1gFnSc1
Helinová	Helinový	k2eAgFnSc1d1
•	•	k?
Venla	Venla	k1gFnSc1
Hoviová	Hoviový	k2eAgFnSc1d1
•	•	k?
Michelle	Michelle	k1gFnSc1
Karvinenová	Karvinenový	k2eAgFnSc1d1
•	•	k?
Mira	Mira	k1gFnSc1
Kuismaová	Kuismaový	k2eAgFnSc1d1
•	•	k?
Emma	Emma	k1gFnSc1
Laaksonenová	Laaksonenová	k1gFnSc1
"	"	kIx"
<g/>
C	C	kA
<g/>
"	"	kIx"
•	•	k?
Rosa	Rosa	k1gFnSc1
Lindstedtová	Lindstedtová	k1gFnSc1
•	•	k?
Terhi	Terh	k1gFnSc2
Mertanenová	Mertanenová	k1gFnSc1
•	•	k?
Heidi	Heid	k1gMnPc1
Pelttariová	Pelttariový	k2eAgFnSc1d1
•	•	k?
Mariia	Mariia	k1gFnSc1
Posaová	Posaový	k2eAgFnSc1d1
•	•	k?
Annina	Annin	k2eAgFnSc1d1
Rajahuhtová	Rajahuhtová	k1gFnSc1
•	•	k?
Karoliina	Karoliin	k2eAgFnSc1d1
Rantamäkiová	Rantamäkiový	k2eAgFnSc1d1
•	•	k?
Noora	Noora	k1gFnSc1
Rätyová	Rätyová	k1gFnSc1
•	•	k?
Mari	Mar	k1gFnSc2
Saarinenová	Saarinenová	k1gFnSc1
•	•	k?
Saija	Saija	k1gFnSc1
Sirviöová	Sirviöový	k2eAgFnSc1d1
•	•	k?
Nina	Nina	k1gFnSc1
Tikkinenová	Tikkinenová	k1gFnSc1
•	•	k?
Minnamari	Minnamar	k1gFnSc2
Tuominenová	Tuominenová	k1gFnSc1
•	•	k?
Saara	Saara	k1gFnSc1
Tuominenová	Tuominenová	k1gFnSc1
"	"	kIx"
<g/>
A	A	kA
<g/>
"	"	kIx"
•	•	k?
Anna	Anna	k1gFnSc1
Vanhataloová	Vanhataloový	k2eAgFnSc1d1
•	•	k?
Linda	Linda	k1gFnSc1
Välimäkiová	Välimäkiový	k2eAgFnSc1d1
•	•	k?
Marjo	Marjo	k1gNnSc1
Voutilainenová	Voutilainenová	k1gFnSc1
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Trenéři	trenér	k1gMnPc1
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
uvedeni	uveden	k2eAgMnPc1d1
pouze	pouze	k6eAd1
pro	pro	k7c4
úplnost	úplnost	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
olympijské	olympijský	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
nedostávají	dostávat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
Hlavní	hlavní	k2eAgInSc1d1
článek	článek	k1gInSc1
</s>
<s>
Antverpy	Antverpy	k1gFnPc1
1920	#num#	k4
(	(	kIx(
<g/>
na	na	k7c6
LOH	LOH	kA
<g/>
)	)	kIx)
•	•	k?
Chamonix	Chamonix	k1gNnSc1
1924	#num#	k4
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Mořic	Mořic	k1gMnSc1
1928	#num#	k4
•	•	k?
Lake	Lak	k1gInPc1
Placid	Placid	k1gInSc1
1932	#num#	k4
•	•	k?
Garmisch-Partenkirchen	Garmisch-Partenkirchen	k1gInSc1
1936	#num#	k4
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Mořic	Mořic	k1gMnSc1
1948	#num#	k4
•	•	k?
Oslo	Oslo	k1gNnSc1
1952	#num#	k4
•	•	k?
Cortina	Cortina	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Ampezzo	Ampezza	k1gFnSc5
1956	#num#	k4
•	•	k?
Squaw	squaw	k1gFnSc1
Valley	Vallea	k1gFnSc2
1960	#num#	k4
•	•	k?
Innsbruck	Innsbruck	k1gInSc1
1964	#num#	k4
•	•	k?
Grenoble	Grenoble	k1gInSc1
1968	#num#	k4
•	•	k?
Sapporo	Sappora	k1gFnSc5
1972	#num#	k4
•	•	k?
Innsbruck	Innsbruck	k1gInSc1
1976	#num#	k4
•	•	k?
Lake	Lak	k1gInPc1
Placid	Placid	k1gInSc1
1980	#num#	k4
•	•	k?
Sarajevo	Sarajevo	k1gNnSc1
1984	#num#	k4
•	•	k?
Calgary	Calgary	k1gNnSc1
1988	#num#	k4
•	•	k?
Albertville	Albertville	k1gNnSc1
1992	#num#	k4
•	•	k?
Lillehammer	Lillehammer	k1gInSc1
1994	#num#	k4
•	•	k?
Nagano	Nagano	k1gNnSc1
1998	#num#	k4
•	•	k?
Salt	salto	k1gNnPc2
Lake	Lake	k1gInSc1
City	city	k1gNnSc1
2002	#num#	k4
•	•	k?
Turín	Turín	k1gInSc1
2006	#num#	k4
•	•	k?
Vancouver	Vancouver	k1gInSc1
2010	#num#	k4
•	•	k?
Soči	Soči	k1gNnSc1
2014	#num#	k4
•	•	k?
Pchjongčchang	Pchjongčchang	k1gInSc1
2018	#num#	k4
•	•	k?
Peking	Peking	k1gInSc1
2022	#num#	k4
Hlavní	hlavní	k2eAgInSc1d1
turnaj	turnaj	k1gInSc1
–	–	k?
muži	muž	k1gMnSc6
</s>
<s>
1998	#num#	k4
(	(	kIx(
<g/>
soupisky	soupiska	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
2002	#num#	k4
(	(	kIx(
<g/>
soupisky	soupiska	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
2006	#num#	k4
(	(	kIx(
<g/>
soupisky	soupiska	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
2010	#num#	k4
(	(	kIx(
<g/>
soupisky	soupiska	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
2014	#num#	k4
(	(	kIx(
<g/>
soupisky	soupiska	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
2018	#num#	k4
(	(	kIx(
<g/>
soupisky	soupiska	k1gFnSc2
<g/>
)	)	kIx)
Hlavní	hlavní	k2eAgInSc1d1
turnaj	turnaj	k1gInSc1
–	–	k?
ženy	žena	k1gFnPc1
</s>
<s>
1998	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2018	#num#	k4
Kvalifikace	kvalifikace	k1gFnSc1
na	na	k7c6
ZOH	ZOH	kA
–	–	k?
muži	muž	k1gMnSc6
</s>
<s>
1956	#num#	k4
•	•	k?
1960	#num#	k4
•	•	k?
1964	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2022	#num#	k4
Kvalifikace	kvalifikace	k1gFnSc1
na	na	k7c6
ZOH	ZOH	kA
–	–	k?
ženy	žena	k1gFnPc4
</s>
<s>
2002	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2018	#num#	k4
Vítězové	vítěz	k1gMnPc1
individuálních	individuální	k2eAgFnPc2d1
statistik	statistika	k1gFnPc2
</s>
<s>
Nejproduktivnější	produktivní	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgMnSc1d3
střelec	střelec	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgMnSc1d3
nahrávač	nahrávač	k1gMnSc1
•	•	k?
Nejtrestanější	trestaný	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgFnSc1d3
statistika	statistika	k1gFnSc1
+	+	kIx~
<g/>
/	/	kIx~
<g/>
−	−	k?
•	•	k?
Nejvyšší	vysoký	k2eAgFnSc4d3
úspěšnost	úspěšnost	k1gFnSc4
zákroků	zákrok	k1gInPc2
Ocenění	ocenění	k1gNnSc4
</s>
<s>
Nejlepší	dobrý	k2eAgMnPc1d3
hráči	hráč	k1gMnPc1
na	na	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
postech	post	k1gInPc6
Seznam	seznam	k1gInSc1
československých	československý	k2eAgMnPc2d1
ledních	lední	k2eAgMnPc2d1
hokejistů	hokejista	k1gMnPc2
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
•	•	k?
Seznam	seznam	k1gInSc1
českých	český	k2eAgMnPc2d1
ledních	lední	k2eAgMnPc2d1
hokejistů	hokejista	k1gMnPc2
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kanada	Kanada	k1gFnSc1
|	|	kIx~
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
</s>
