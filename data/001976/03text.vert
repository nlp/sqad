<s>
Saeima	Saeima	k1gFnSc1	Saeima
je	být	k5eAaImIp3nS	být
lotyšský	lotyšský	k2eAgInSc4d1	lotyšský
jednokomorový	jednokomorový	k2eAgInSc4d1	jednokomorový
parlament	parlament	k1gInSc4	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
100	[number]	k4	100
poslanci	poslanec	k1gMnPc7	poslanec
volenými	volený	k2eAgMnPc7d1	volený
na	na	k7c6	na
dobu	doba	k1gFnSc4	doba
čtyř	čtyři	k4xCgMnPc2	čtyři
let	léto	k1gNnPc2	léto
poměrným	poměrný	k2eAgInSc7d1	poměrný
volebním	volební	k2eAgInSc7d1	volební
systémem	systém	k1gInSc7	systém
s	s	k7c7	s
5	[number]	k4	5
<g/>
%	%	kIx~	%
uzavírací	uzavírací	k2eAgFnSc7d1	uzavírací
klauzulí	klauzule	k1gFnSc7	klauzule
<g/>
.	.	kIx.	.
</s>
<s>
Seimas	Seimas	k1gInSc1	Seimas
Sejm	Sejm	k1gInSc1	Sejm
Parlament	parlament	k1gInSc4	parlament
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Saeima	Saeimum	k1gNnSc2	Saeimum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
lotyšského	lotyšský	k2eAgInSc2d1	lotyšský
parlamentu	parlament	k1gInSc2	parlament
</s>
