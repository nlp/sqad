<p>
<s>
Čečehov	Čečehov	k1gInSc1	Čečehov
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Michalovce	Michalovce	k1gInPc1	Michalovce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Východoslovenské	východoslovenský	k2eAgFnPc1d1	Východoslovenská
nížiny	nížina	k1gFnPc1	nížina
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
nánosovém	nánosový	k2eAgInSc6d1	nánosový
valu	val	k1gInSc6	val
řeky	řeka	k1gFnSc2	řeka
Laborec	Laborec	k1gInSc1	Laborec
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
okolo	okolo	k7c2	okolo
106	[number]	k4	106
m.	m.	k?	m.
</s>
</p>
<p>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
341	[number]	k4	341
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc4	obec
založili	založit	k5eAaPmAgMnP	založit
osadníci	osadník	k1gMnPc1	osadník
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
písemný	písemný	k2eAgInSc4d1	písemný
doklad	doklad	k1gInSc4	doklad
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1410	[number]	k4	1410
<g/>
.	.	kIx.	.
</s>
<s>
Feudálními	feudální	k2eAgMnPc7d1	feudální
pány	pan	k1gMnPc7	pan
Čečehova	Čečehova	k1gFnSc1	Čečehova
byli	být	k5eAaImAgMnP	být
šlechtici	šlechtic	k1gMnPc1	šlechtic
z	z	k7c2	z
Michalovců	Michalovce	k1gInPc2	Michalovce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1427	[number]	k4	1427
hospodařilo	hospodařit	k5eAaImAgNnS	hospodařit
v	v	k7c6	v
Čečehově	Čečehova	k1gFnSc6	Čečehova
asi	asi	k9	asi
12	[number]	k4	12
poddanských	poddanský	k2eAgFnPc2d1	poddanská
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rozvoji	rozvoj	k1gInSc3	rozvoj
obce	obec	k1gFnSc2	obec
tu	tu	k6eAd1	tu
roku	rok	k1gInSc2	rok
1599	[number]	k4	1599
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
28	[number]	k4	28
poddanských	poddanský	k2eAgInPc2d1	poddanský
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1715	[number]	k4	1715
hospodařily	hospodařit	k5eAaImAgInP	hospodařit
jen	jen	k6eAd1	jen
4	[number]	k4	4
poddanské	poddanský	k2eAgFnSc2d1	poddanská
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1828	[number]	k4	1828
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
napočítáno	napočítán	k2eAgNnSc1d1	napočítáno
68	[number]	k4	68
domů	dům	k1gInPc2	dům
a	a	k8xC	a
584	[number]	k4	584
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
pamětihodností	pamětihodnost	k1gFnPc2	pamětihodnost
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
kostel	kostel	k1gInSc4	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
růžencové	růžencový	k2eAgFnPc1d1	Růžencová
a	a	k8xC	a
Kristova	Kristův	k2eAgFnSc1d1	Kristova
socha	socha	k1gFnSc1	socha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
a	a	k8xC	a
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
==	==	k?	==
</s>
</p>
<p>
<s>
Zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
půdu	půda	k1gFnSc4	půda
obhospodařuje	obhospodařovat	k5eAaImIp3nS	obhospodařovat
soukromá	soukromý	k2eAgFnSc1d1	soukromá
firma	firma	k1gFnSc1	firma
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Čečehově	Čečehova	k1gFnSc6	Čečehova
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc4	ječmen
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
plodiny	plodina	k1gFnPc4	plodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
je	být	k5eAaImIp3nS	být
poštovní	poštovní	k2eAgInSc1d1	poštovní
úřad	úřad	k1gInSc1	úřad
a	a	k8xC	a
prodejna	prodejna	k1gFnSc1	prodejna
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Dopravu	doprava	k1gFnSc4	doprava
zabezpečují	zabezpečovat	k5eAaImIp3nP	zabezpečovat
autobusy	autobus	k1gInPc1	autobus
SAD	sada	k1gFnPc2	sada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejbližší	blízký	k2eAgFnSc1d3	nejbližší
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
10	[number]	k4	10
km	km	kA	km
vzdálených	vzdálený	k2eAgInPc6d1	vzdálený
Michalovcích	Michalovce	k1gInPc6	Michalovce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc4d1	další
zajímavosti	zajímavost	k1gFnPc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
působí	působit	k5eAaImIp3nS	působit
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
a	a	k8xC	a
Svaz	svaz	k1gInSc1	svaz
zdravotně	zdravotně	k6eAd1	zdravotně
postižených	postižený	k1gMnPc2	postižený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Čečehov	Čečehov	k1gInSc1	Čečehov
na	na	k7c6	na
webu	web	k1gInSc6	web
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Čečehov	Čečehov	k1gInSc1	Čečehov
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
