<s>
Čečehov	Čečehov	k1gInSc1	Čečehov
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Michalovce	Michalovce	k1gInPc1	Michalovce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Východoslovenské	východoslovenský	k2eAgFnPc1d1	Východoslovenská
nížiny	nížina	k1gFnPc1	nížina
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
nánosovém	nánosový	k2eAgInSc6d1	nánosový
valu	val	k1gInSc6	val
řeky	řeka	k1gFnSc2	řeka
Laborec	Laborec	k1gInSc1	Laborec
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
okolo	okolo	k7c2	okolo
106	[number]	k4	106
m.	m.	k?	m.
</s>
