<s>
Velbloudovití	velbloudovitý	k2eAgMnPc1d1	velbloudovitý
jsou	být	k5eAaImIp3nP	být
jedinou	jediný	k2eAgFnSc7d1	jediná
čeledí	čeleď	k1gFnSc7	čeleď
podřádu	podřád	k1gInSc2	podřád
mozolnatci	mozolnatec	k1gMnPc7	mozolnatec
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
velbloudy	velbloud	k1gMnPc4	velbloud
<g/>
,	,	kIx,	,
lamy	lama	k1gMnPc4	lama
a	a	k8xC	a
vikuně	vikuně	k6eAd1	vikuně
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velbloudovité	velbloudovitý	k2eAgNnSc4d1	velbloudovitý
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
jejich	jejich	k3xOp3gFnSc1	jejich
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
chůze	chůze	k1gFnSc1	chůze
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
mimochod	mimochod	k1gInSc1	mimochod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
současně	současně	k6eAd1	současně
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
přední	přední	k2eAgMnSc1d1	přední
i	i	k8xC	i
zadní	zadní	k2eAgFnSc1d1	zadní
končetina	končetina	k1gFnSc1	končetina
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
chůze	chůze	k1gFnSc1	chůze
kymácivá	kymácivý	k2eAgFnSc1d1	kymácivý
<g/>
.	.	kIx.	.
</s>
<s>
Podobě	podoba	k1gFnSc3	podoba
jako	jako	k9	jako
u	u	k7c2	u
mnohých	mnohý	k2eAgNnPc2d1	mnohé
jiných	jiný	k2eAgNnPc2d1	jiné
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
názory	názor	k1gInPc1	názor
na	na	k7c4	na
systém	systém	k1gInSc4	systém
velbloudovitých	velbloudovitý	k2eAgFnPc2d1	velbloudovitý
liší	lišit	k5eAaImIp3nS	lišit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
genetických	genetický	k2eAgInPc2d1	genetický
průzkumů	průzkum	k1gInPc2	průzkum
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
vypadat	vypadat	k5eAaImF	vypadat
nejspíše	nejspíše	k9	nejspíše
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
velbloud	velbloud	k1gMnSc1	velbloud
(	(	kIx(	(
<g/>
Camelus	Camelus	k1gMnSc1	Camelus
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
velbloud	velbloud	k1gMnSc1	velbloud
jednohrbý	jednohrbý	k2eAgMnSc1d1	jednohrbý
(	(	kIx(	(
<g/>
C.	C.	kA	C.
dromedarius	dromedarius	k1gMnSc1	dromedarius
<g/>
)	)	kIx)	)
-	-	kIx~	-
také	také	k9	také
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
dromedár	dromedár	k1gMnSc1	dromedár
<g/>
"	"	kIx"	"
velbloud	velbloud	k1gMnSc1	velbloud
dvouhrbý	dvouhrbý	k2eAgMnSc1d1	dvouhrbý
(	(	kIx(	(
<g/>
C.	C.	kA	C.
ferus	ferus	k1gInSc1	ferus
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
C.	C.	kA	C.
bactrianus	bactrianus	k1gInSc4	bactrianus
<g/>
)	)	kIx)	)
-	-	kIx~	-
také	také	k9	také
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
drabař	drabař	k1gMnSc1	drabař
<g/>
"	"	kIx"	"
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
lama	lama	k1gFnSc1	lama
(	(	kIx(	(
<g/>
Lama	lama	k1gFnSc1	lama
<g/>
)	)	kIx)	)
guanako	guanako	k1gNnSc1	guanako
(	(	kIx(	(
<g/>
Lama	lama	k1gFnSc1	lama
guanicoe	guanicoe	k1gFnSc1	guanicoe
<g/>
)	)	kIx)	)
lama	lama	k1gFnSc1	lama
krotká	krotký	k2eAgFnSc1d1	krotká
(	(	kIx(	(
<g/>
Lama	lama	k1gFnSc1	lama
guanicoe	guanico	k1gMnSc2	guanico
f.	f.	k?	f.
glama	glam	k1gMnSc2	glam
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
zdomácnělá	zdomácnělý	k2eAgFnSc1d1	zdomácnělá
a	a	k8xC	a
následně	následně	k6eAd1	následně
opět	opět	k6eAd1	opět
zdivočelá	zdivočelý	k2eAgFnSc1d1	Zdivočelá
forma	forma	k1gFnSc1	forma
guanaka	guanak	k1gMnSc2	guanak
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
vikuna	vikuna	k1gFnSc1	vikuna
(	(	kIx(	(
<g/>
Vicugna	Vicugna	k1gFnSc1	Vicugna
<g/>
)	)	kIx)	)
vikuňa	vikuňa	k1gFnSc1	vikuňa
(	(	kIx(	(
<g/>
Vicugna	Vicugen	k2eAgFnSc1d1	Vicugna
vicigna	vicigna	k1gFnSc1	vicigna
<g/>
)	)	kIx)	)
alpaka	alpaka	k1gFnSc1	alpaka
(	(	kIx(	(
<g/>
Vicugna	Vicugna	k1gFnSc1	Vicugna
pacos	pacos	k1gMnSc1	pacos
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdomácnělá	zdomácnělý	k2eAgFnSc1d1	zdomácnělá
forma	forma	k1gFnSc1	forma
vikuně	vikuně	k6eAd1	vikuně
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Lama	lama	k1gFnSc1	lama
pacos	pacosa	k1gFnPc2	pacosa
Lamy	lama	k1gFnSc2	lama
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
řadí	řadit	k5eAaImIp3nS	řadit
i	i	k9	i
vikuně	vikuně	k6eAd1	vikuně
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Velbloudy	velbloud	k1gMnPc4	velbloud
můžeme	moct	k5eAaImIp1nP	moct
potkat	potkat	k5eAaPmF	potkat
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
velbloudi	velbloud	k1gMnPc1	velbloud
jednohrbí	jednohrbý	k2eAgMnPc1d1	jednohrbý
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
i	i	k9	i
ještě	ještě	k9	ještě
divocí	divoký	k2eAgMnPc1d1	divoký
velbloudi	velbloud	k1gMnPc1	velbloud
dvouhrbí	dvouhrbý	k2eAgMnPc1d1	dvouhrbý
<g/>
.	.	kIx.	.
</s>
<s>
Divoká	divoký	k2eAgFnSc1d1	divoká
forma	forma	k1gFnSc1	forma
velblouda	velbloud	k1gMnSc2	velbloud
jednohrbého	jednohrbý	k2eAgInSc2d1	jednohrbý
již	již	k6eAd1	již
vymřela	vymřít	k5eAaPmAgNnP	vymřít
<g/>
.	.	kIx.	.
</s>
<s>
Velbloudy	velbloud	k1gMnPc4	velbloud
jednohrbé	jednohrbý	k2eAgMnPc4d1	jednohrbý
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
i	i	k9	i
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byli	být	k5eAaImAgMnP	být
uměle	uměle	k6eAd1	uměle
vysazeni	vysazen	k2eAgMnPc1d1	vysazen
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velbloudovité	velbloudovitý	k2eAgFnPc4d1	velbloudovitý
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgFnPc1d1	typická
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
štíhlé	štíhlý	k2eAgFnPc1d1	štíhlá
nohy	noha	k1gFnPc1	noha
a	a	k8xC	a
krk	krk	k1gInSc1	krk
a	a	k8xC	a
malá	malý	k2eAgFnSc1d1	malá
hlava	hlava	k1gFnSc1	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Hustý	hustý	k2eAgInSc1d1	hustý
kožich	kožich	k1gInSc1	kožich
je	být	k5eAaImIp3nS	být
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
sluncem	slunce	k1gNnSc7	slunce
i	i	k8xC	i
zimou	zima	k1gFnSc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
sudokopytníků	sudokopytník	k1gMnPc2	sudokopytník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
končetinách	končetina	k1gFnPc6	končetina
ještě	ještě	k9	ještě
i	i	k9	i
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
prst	prst	k1gInSc4	prst
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
paspárků	paspárek	k1gInPc2	paspárek
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
velbloudovití	velbloudovitý	k2eAgMnPc1d1	velbloudovitý
zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
jen	jen	k9	jen
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
prst	prst	k1gInSc1	prst
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
prsty	prst	k1gInPc1	prst
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
drobných	drobný	k2eAgFnPc2d1	drobná
kůstek	kůstka	k1gFnPc2	kůstka
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
kopytníci	kopytník	k1gMnPc1	kopytník
prstochodci	prstochodec	k1gMnPc1	prstochodec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
našlapují	našlapovat	k5eAaImIp3nP	našlapovat
na	na	k7c4	na
poslední	poslední	k2eAgInPc4d1	poslední
tři	tři	k4xCgInPc4	tři
články	článek	k1gInPc4	článek
prstů	prst	k1gInPc2	prst
kryté	krytý	k2eAgFnSc2d1	krytá
mozolem	mozol	k1gInSc7	mozol
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
rozpáleným	rozpálený	k2eAgInSc7d1	rozpálený
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yRgInSc6	který
chodí	chodit	k5eAaImIp3nP	chodit
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
chodit	chodit	k5eAaImF	chodit
po	po	k7c6	po
písku	písek	k1gInSc6	písek
<g/>
.	.	kIx.	.
</s>
<s>
Velbloudovití	velbloudovitý	k2eAgMnPc1d1	velbloudovitý
mají	mít	k5eAaImIp3nP	mít
třídílný	třídílný	k2eAgInSc4d1	třídílný
žaludek	žaludek	k1gInSc4	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
předžaludky	předžaludek	k1gInPc1	předžaludek
-	-	kIx~	-
bachor	bachor	k1gInSc1	bachor
a	a	k8xC	a
čepec	čepec	k1gInSc1	čepec
a	a	k8xC	a
vlastní	vlastní	k2eAgInSc1d1	vlastní
žaludek	žaludek	k1gInSc1	žaludek
-	-	kIx~	-
slez	slez	k1gInSc1	slez
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
speciálně	speciálně	k6eAd1	speciálně
upravené	upravený	k2eAgInPc1d1	upravený
přední	přední	k2eAgInPc1d1	přední
zuby	zub	k1gInPc1	zub
a	a	k8xC	a
krátké	krátký	k2eAgNnSc1d1	krátké
slepé	slepý	k2eAgNnSc1d1	slepé
střevo	střevo	k1gNnSc1	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
odlišností	odlišnost	k1gFnSc7	odlišnost
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
savců	savec	k1gMnPc2	savec
jsou	být	k5eAaImIp3nP	být
oválné	oválný	k2eAgFnPc1d1	oválná
červené	červený	k2eAgFnPc1d1	červená
krvinky	krvinka	k1gFnPc1	krvinka
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jim	on	k3xPp3gMnPc3	on
tak	tak	k9	tak
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
lépe	dobře	k6eAd2	dobře
hospodařit	hospodařit	k5eAaImF	hospodařit
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Velbloudi	velbloud	k1gMnPc1	velbloud
a	a	k8xC	a
lamy	lama	k1gFnPc1	lama
žijí	žít	k5eAaImIp3nP	žít
většinou	většina	k1gFnSc7	většina
ve	v	k7c6	v
stádech	stádo	k1gNnPc6	stádo
vedených	vedený	k2eAgFnPc2d1	vedená
dominantním	dominantní	k2eAgInSc7d1	dominantní
samcem	samec	k1gInSc7	samec
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
samci	samec	k1gMnPc1	samec
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
mládenecké	mládenecký	k2eAgFnPc4d1	mládenecká
skupiny	skupina	k1gFnPc4	skupina
nebo	nebo	k8xC	nebo
žijí	žít	k5eAaImIp3nP	žít
samotářsky	samotářsky	k6eAd1	samotářsky
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tato	tento	k3xDgNnPc4	tento
zvířata	zvíře	k1gNnPc4	zvíře
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
plivání	plivání	k1gNnSc1	plivání
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
obranných	obranný	k2eAgFnPc2d1	obranná
taktik	taktika	k1gFnPc2	taktika
<g/>
.	.	kIx.	.
</s>
<s>
Využívají	využívat	k5eAaPmIp3nP	využívat
několik	několik	k4yIc4	několik
typů	typ	k1gInPc2	typ
plivání	plivání	k1gNnSc2	plivání
<g/>
,	,	kIx,	,
od	od	k7c2	od
výhrůžného	výhrůžný	k2eAgNnSc2d1	výhrůžné
plivání	plivání	k1gNnSc2	plivání
pouze	pouze	k6eAd1	pouze
slin	slina	k1gFnPc2	slina
až	až	k6eAd1	až
po	po	k7c6	po
vyprázdnění	vyprázdnění	k1gNnSc6	vyprázdnění
natráveného	natrávený	k2eAgInSc2d1	natrávený
obsahu	obsah	k1gInSc2	obsah
předžaludků	předžaludek	k1gInPc2	předžaludek
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
oblastem	oblast	k1gFnPc3	oblast
jejich	jejich	k3xOp3gInPc2	jejich
výskytu	výskyt	k1gInSc2	výskyt
nejsou	být	k5eNaImIp3nP	být
zástupci	zástupce	k1gMnPc1	zástupce
této	tento	k3xDgFnSc2	tento
čeledi	čeleď	k1gFnSc2	čeleď
nároční	náročný	k2eAgMnPc1d1	náročný
na	na	k7c4	na
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
si	se	k3xPyFc3	se
vystačí	vystačit	k5eAaBmIp3nS	vystačit
pouze	pouze	k6eAd1	pouze
se	s	k7c7	s
suchou	suchý	k2eAgFnSc7d1	suchá
trávou	tráva	k1gFnSc7	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Velbloudi	velbloud	k1gMnPc1	velbloud
vydrží	vydržet	k5eAaPmIp3nP	vydržet
až	až	k9	až
40	[number]	k4	40
dní	den	k1gInPc2	den
bez	bez	k7c2	bez
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
lam	lama	k1gFnPc2	lama
také	také	k6eAd1	také
mnoho	mnoho	k4c4	mnoho
dní	den	k1gInPc2	den
bez	bez	k7c2	bez
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
rodí	rodit	k5eAaImIp3nP	rodit
přibližně	přibližně	k6eAd1	přibližně
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
rok	rok	k1gInSc4	rok
jedno	jeden	k4xCgNnSc4	jeden
mládě	mládě	k1gNnSc4	mládě
<g/>
.	.	kIx.	.
</s>
