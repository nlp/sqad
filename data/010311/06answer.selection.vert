<s>
Asymetrická	asymetrický	k2eAgFnSc1d1	asymetrická
válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
konflikt	konflikt	k1gInSc4	konflikt
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
válčícími	válčící	k2eAgFnPc7d1	válčící
stranami	strana	k1gFnPc7	strana
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
vojenská	vojenský	k2eAgFnSc1d1	vojenská
síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
strategie	strategie	k1gFnSc1	strategie
nebo	nebo	k8xC	nebo
taktika	taktika	k1gFnSc1	taktika
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
