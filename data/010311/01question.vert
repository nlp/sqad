<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
válčícími	válčící	k2eAgFnPc7d1	válčící
stranami	strana	k1gFnPc7	strana
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
vojenská	vojenský	k2eAgFnSc1d1	vojenská
síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
?	?	kIx.	?
</s>
