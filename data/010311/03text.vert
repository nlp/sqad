<p>
<s>
Asymetrická	asymetrický	k2eAgFnSc1d1	asymetrická
válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
konflikt	konflikt	k1gInSc4	konflikt
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
válčícími	válčící	k2eAgFnPc7d1	válčící
stranami	strana	k1gFnPc7	strana
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
vojenská	vojenský	k2eAgFnSc1d1	vojenská
síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
strategie	strategie	k1gFnSc1	strategie
nebo	nebo	k8xC	nebo
taktika	taktika	k1gFnSc1	taktika
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Asymetrickou	asymetrický	k2eAgFnSc4d1	asymetrická
válku	válka	k1gFnSc4	válka
definoval	definovat	k5eAaBmAgMnS	definovat
Steven	Steven	k2eAgMnSc1d1	Steven
Metz	Metz	k1gMnSc1	Metz
a	a	k8xC	a
Douglas	Douglas	k1gMnSc1	Douglas
Johnson	Johnson	k1gMnSc1	Johnson
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
vojenské	vojenský	k2eAgFnSc2d1	vojenská
univerzity	univerzita	k1gFnSc2	univerzita
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vojenství	vojenství	k1gNnSc2	vojenství
a	a	k8xC	a
státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
asymetrie	asymetrie	k1gFnSc2	asymetrie
je	být	k5eAaImIp3nS	být
jednání	jednání	k1gNnSc1	jednání
a	a	k8xC	a
uvažování	uvažování	k1gNnSc1	uvažování
odlišné	odlišný	k2eAgNnSc1d1	odlišné
od	od	k7c2	od
protivníka	protivník	k1gMnSc2	protivník
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
maximalizovat	maximalizovat	k5eAaBmF	maximalizovat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
výhody	výhoda	k1gFnPc4	výhoda
<g/>
,	,	kIx,	,
využít	využít	k5eAaPmF	využít
slabých	slabý	k2eAgNnPc2d1	slabé
míst	místo	k1gNnPc2	místo
protivníka	protivník	k1gMnSc2	protivník
<g/>
,	,	kIx,	,
získat	získat	k5eAaPmF	získat
iniciativu	iniciativa	k1gFnSc4	iniciativa
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
větší	veliký	k2eAgFnSc1d2	veliký
volnost	volnost	k1gFnSc1	volnost
jednání	jednání	k1gNnSc2	jednání
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velkým	velký	k2eAgFnPc3d1	velká
válkám	válka	k1gFnPc3	válka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
charakter	charakter	k1gInSc4	charakter
mezistátních	mezistátní	k2eAgInPc2d1	mezistátní
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc1	ten
války	válka	k1gFnPc1	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
(	(	kIx(	(
<g/>
operace	operace	k1gFnSc1	operace
Pouštní	pouštní	k2eAgFnSc2d1	pouštní
bouře	bouř	k1gFnSc2	bouř
1991	[number]	k4	1991
a	a	k8xC	a
Irácká	irácký	k2eAgFnSc1d1	irácká
Svoboda	svoboda	k1gFnSc1	svoboda
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
proti	proti	k7c3	proti
Jugoslávii	Jugoslávie	k1gFnSc3	Jugoslávie
(	(	kIx(	(
<g/>
operace	operace	k1gFnSc2	operace
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
síla	síla	k1gFnSc1	síla
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
(	(	kIx(	(
<g/>
Trvalá	trvalý	k2eAgFnSc1d1	trvalá
svoboda	svoboda	k1gFnSc1	svoboda
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
a	a	k8xC	a
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Zásah	zásah	k1gInSc1	zásah
USA	USA	kA	USA
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc2d1	ostatní
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dostala	dostat	k5eAaPmAgFnS	dostat
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
humanitární	humanitární	k2eAgNnSc4d1	humanitární
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
společných	společný	k2eAgInPc2d1	společný
jmenovatelů	jmenovatel	k1gInPc2	jmenovatel
těchto	tento	k3xDgFnPc2	tento
válek	válka	k1gFnPc2	válka
bylo	být	k5eAaImAgNnS	být
kladení	kladení	k1gNnSc1	kladení
důrazu	důraz	k1gInSc2	důraz
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
společenství	společenství	k1gNnSc2	společenství
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
a	a	k8xC	a
prosazena	prosadit	k5eAaPmNgFnS	prosadit
mimo	mimo	k7c4	mimo
hranice	hranice	k1gFnPc4	hranice
národní	národní	k2eAgFnSc2d1	národní
suverenity	suverenita	k1gFnSc2	suverenita
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
uvnitř	uvnitř	k7c2	uvnitř
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
a	a	k8xC	a
možná	možná	k9	možná
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gFnSc3	jejich
vůli	vůle	k1gFnSc3	vůle
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
válkách	válka	k1gFnPc6	válka
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgInS	zrodit
"	"	kIx"	"
<g/>
militární	militární	k2eAgInSc1d1	militární
humanismus	humanismus	k1gInSc1	humanismus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
mj.	mj.	kA	mj.
<g/>
na	na	k7c6	na
asymetrii	asymetrie	k1gFnSc6	asymetrie
síly	síla	k1gFnSc2	síla
-	-	kIx~	-
čím	co	k3yQnSc7	co
slabší	slabý	k2eAgFnSc1d2	slabší
je	být	k5eAaImIp3nS	být
protivník	protivník	k1gMnSc1	protivník
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
je	být	k5eAaImIp3nS	být
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
napaden	napadnout	k5eAaPmNgMnS	napadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyzbrojování	vyzbrojování	k1gNnSc1	vyzbrojování
vojáků	voják	k1gMnPc2	voják
afrických	africký	k2eAgFnPc2d1	africká
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
zemí	zem	k1gFnPc2	zem
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
každá	každý	k3xTgFnSc1	každý
koloniální	koloniální	k2eAgFnSc1d1	koloniální
mocnost	mocnost	k1gFnSc1	mocnost
prováděla	provádět	k5eAaImAgFnS	provádět
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
své	svůj	k3xOyFgFnSc2	svůj
nadvlády	nadvláda	k1gFnSc2	nadvláda
výcvik	výcvik	k1gInSc1	výcvik
asijského	asijský	k2eAgNnSc2d1	asijské
a	a	k8xC	a
afrického	africký	k2eAgNnSc2d1	africké
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
stylu	styl	k1gInSc6	styl
vedení	vedení	k1gNnSc2	vedení
války	válka	k1gFnSc2	válka
a	a	k8xC	a
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
zámořských	zámořský	k2eAgFnPc6d1	zámořská
državách	država	k1gFnPc6	država
udržovala	udržovat	k5eAaImAgFnS	udržovat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
bílé	bílý	k2eAgInPc1d1	bílý
oddíly	oddíl	k1gInPc1	oddíl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
upevnění	upevnění	k1gNnSc3	upevnění
koloniální	koloniální	k2eAgFnSc2d1	koloniální
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
spolupráce	spolupráce	k1gFnSc2	spolupráce
místních	místní	k2eAgFnPc2d1	místní
etnik	etnikum	k1gNnPc2	etnikum
vznikaly	vznikat	k5eAaImAgInP	vznikat
i	i	k9	i
domorodé	domorodý	k2eAgInPc1d1	domorodý
oddíly	oddíl	k1gInPc1	oddíl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
tvořili	tvořit	k5eAaImAgMnP	tvořit
černošští	černošský	k2eAgMnPc1d1	černošský
vojáci	voják	k1gMnPc1	voják
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
také	také	k9	také
včleněni	včleněn	k2eAgMnPc1d1	včleněn
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
oddíly	oddíl	k1gInPc1	oddíl
bílé	bílý	k2eAgFnSc2d1	bílá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolonizované	kolonizovaný	k2eAgInPc1d1	kolonizovaný
národy	národ	k1gInPc1	národ
a	a	k8xC	a
státy	stát	k1gInPc1	stát
byly	být	k5eAaImAgInP	být
zasvěceny	zasvětit	k5eAaPmNgInP	zasvětit
do	do	k7c2	do
moderního	moderní	k2eAgNnSc2d1	moderní
válečného	válečný	k2eAgNnSc2d1	válečné
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
války	válka	k1gFnSc2	válka
bojovali	bojovat	k5eAaImAgMnP	bojovat
afričtí	africký	k2eAgMnPc1d1	africký
vojáci	voják	k1gMnPc1	voják
i	i	k9	i
mimo	mimo	k7c4	mimo
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
svých	svůj	k3xOyFgFnPc2	svůj
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
mocností	mocnost	k1gFnPc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
Afričané	Afričan	k1gMnPc1	Afričan
i	i	k8xC	i
Asiaté	Asiat	k1gMnPc1	Asiat
naučili	naučit	k5eAaPmAgMnP	naučit
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
vlastně	vlastně	k9	vlastně
jsou	být	k5eAaImIp3nP	být
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
taktiky	taktika	k1gFnPc4	taktika
a	a	k8xC	a
západní	západní	k2eAgInPc4d1	západní
vojenské	vojenský	k2eAgInPc4d1	vojenský
postupy	postup	k1gInPc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
dekolonizace	dekolonizace	k1gFnSc2	dekolonizace
bojovali	bojovat	k5eAaImAgMnP	bojovat
Afričané	Afričan	k1gMnPc1	Afričan
i	i	k8xC	i
Asiaté	Asiat	k1gMnPc1	Asiat
v	v	k7c6	v
oddílech	oddíl	k1gInPc6	oddíl
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
chtěly	chtít	k5eAaImAgInP	chtít
osvobodit	osvobodit	k5eAaPmF	osvobodit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
zemi	zem	k1gFnSc4	zem
od	od	k7c2	od
kolonizační	kolonizační	k2eAgFnSc2d1	kolonizační
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
"	"	kIx"	"
<g/>
třetího	třetí	k4xOgMnSc2	třetí
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
také	také	k6eAd1	také
využívaly	využívat	k5eAaPmAgFnP	využívat
všech	všecek	k3xTgFnPc2	všecek
možností	možnost	k1gFnPc2	možnost
k	k	k7c3	k
financování	financování	k1gNnSc3	financování
aktuálních	aktuální	k2eAgInPc2d1	aktuální
konfliktů	konflikt	k1gInPc2	konflikt
-	-	kIx~	-
od	od	k7c2	od
přímé	přímý	k2eAgFnSc2d1	přímá
pomoci	pomoc	k1gFnSc2	pomoc
supervelmocí	supervelmoc	k1gFnPc2	supervelmoc
přes	přes	k7c4	přes
ilegální	ilegální	k2eAgInSc4d1	ilegální
obchod	obchod	k1gInSc4	obchod
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
až	až	k9	až
po	po	k7c4	po
využívání	využívání	k1gNnSc4	využívání
mezinárodních	mezinárodní	k2eAgMnPc2d1	mezinárodní
žoldnéřů	žoldnéř	k1gMnPc2	žoldnéř
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
původní	původní	k2eAgInPc1d1	původní
útvary	útvar	k1gInPc1	útvar
odporu	odpor	k1gInSc2	odpor
uplatnily	uplatnit	k5eAaPmAgFnP	uplatnit
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
skutečných	skutečný	k2eAgInPc2d1	skutečný
vojenských	vojenský	k2eAgInPc2d1	vojenský
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
zaměřily	zaměřit	k5eAaPmAgInP	zaměřit
na	na	k7c4	na
postupné	postupný	k2eAgNnSc4d1	postupné
osvobozování	osvobozování	k1gNnSc4	osvobozování
území	území	k1gNnSc2	území
vlastního	vlastní	k2eAgInSc2d1	vlastní
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
byla	být	k5eAaImAgFnS	být
nezávislost	nezávislost	k1gFnSc1	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Povinná	povinný	k2eAgFnSc1d1	povinná
vojenská	vojenský	k2eAgFnSc1d1	vojenská
služba	služba	k1gFnSc1	služba
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
armáda	armáda	k1gFnSc1	armáda
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
vysoké	vysoký	k2eAgInPc4d1	vysoký
náklady	náklad	k1gInPc4	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vytvořená	vytvořený	k2eAgNnPc1d1	vytvořené
vojska	vojsko	k1gNnPc1	vojsko
snadno	snadno	k6eAd1	snadno
ustupovala	ustupovat	k5eAaImAgNnP	ustupovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
přinucena	přinucen	k2eAgFnSc1d1	přinucena
utkat	utkat	k5eAaPmF	utkat
se	se	k3xPyFc4	se
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
s	s	k7c7	s
americkými	americký	k2eAgInPc7d1	americký
nebo	nebo	k8xC	nebo
západními	západní	k2eAgInPc7d1	západní
vojenskými	vojenský	k2eAgInPc7d1	vojenský
oddíly	oddíl	k1gInPc7	oddíl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
uskutečňovaly	uskutečňovat	k5eAaImAgInP	uskutečňovat
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
operace	operace	k1gFnSc2	operace
mimo	mimo	k7c4	mimo
jejich	jejich	k3xOp3gNnSc4	jejich
vlastní	vlastní	k2eAgNnSc4d1	vlastní
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
bojům	boj	k1gInPc3	boj
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
bylo	být	k5eAaImAgNnS	být
moderní	moderní	k2eAgNnSc1d1	moderní
technologické	technologický	k2eAgNnSc1d1	Technologické
vojsko	vojsko	k1gNnSc1	vojsko
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
stvořil	stvořit	k5eAaPmAgInS	stvořit
rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Operace	operace	k1gFnSc1	operace
Pouštní	pouštní	k2eAgFnSc2d1	pouštní
bouře	bouř	k1gFnSc2	bouř
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Příkladem	příklad	k1gInSc7	příklad
zhroucení	zhroucení	k1gNnSc2	zhroucení
ambicí	ambice	k1gFnPc2	ambice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
neprozíravě	prozíravě	k6eNd1	prozíravě
opíraly	opírat	k5eAaImAgFnP	opírat
o	o	k7c4	o
sílu	síla	k1gFnSc4	síla
vlastních	vlastní	k2eAgInPc2d1	vlastní
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rychlý	rychlý	k2eAgInSc1d1	rychlý
konec	konec	k1gInSc1	konec
iráckého	irácký	k2eAgInSc2d1	irácký
snu	sen	k1gInSc2	sen
Saddáma	Saddám	k1gMnSc4	Saddám
Husajna	Husajn	k1gMnSc4	Husajn
o	o	k7c4	o
dobytí	dobytí	k1gNnSc4	dobytí
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
-	-	kIx~	-
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
Saddám	Saddám	k1gMnSc1	Saddám
Hussajn	Hussajn	k1gMnSc1	Hussajn
odhodlal	odhodlat	k5eAaPmAgMnS	odhodlat
k	k	k7c3	k
vojenské	vojenský	k2eAgFnSc3d1	vojenská
akci	akce	k1gFnSc3	akce
proti	proti	k7c3	proti
Kuvajtu	Kuvajt	k1gInSc3	Kuvajt
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
George	George	k1gNnSc2	George
H.	H.	kA	H.
W.	W.	kA	W.
Bushem	Bush	k1gMnSc7	Bush
zahájily	zahájit	k5eAaPmAgInP	zahájit
kampaň	kampaň	k1gFnSc4	kampaň
za	za	k7c4	za
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
intervenci	intervence	k1gFnSc4	intervence
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
asi	asi	k9	asi
třiceti	třicet	k4xCc2	třicet
vlád	vláda	k1gFnPc2	vláda
a	a	k8xC	a
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
silná	silný	k2eAgFnSc1d1	silná
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
koalice	koalice	k1gFnSc1	koalice
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgMnPc4	který
měly	mít	k5eAaImAgInP	mít
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
jasnou	jasný	k2eAgFnSc4d1	jasná
technologickou	technologický	k2eAgFnSc4d1	technologická
převahu	převaha	k1gFnSc4	převaha
nad	nad	k7c7	nad
svým	svůj	k3xOyFgMnSc7	svůj
spojencem	spojenec	k1gMnSc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
koalice	koalice	k1gFnSc1	koalice
podporovaná	podporovaný	k2eAgFnSc1d1	podporovaná
OSN	OSN	kA	OSN
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
válku	válka	k1gFnSc4	válka
Iráku	Irák	k1gInSc2	Irák
jako	jako	k8xC	jako
trest	trest	k1gInSc4	trest
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
i	i	k9	i
souhlasu	souhlas	k1gInSc2	souhlas
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
svého	svůj	k3xOyFgMnSc2	svůj
protivníka	protivník	k1gMnSc2	protivník
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
Čína	Čína	k1gFnSc1	Čína
nebyla	být	k5eNaImAgFnS	být
proti	proti	k7c3	proti
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
Pouštní	pouštní	k2eAgFnSc1d1	pouštní
bouře	bouře	k1gFnSc1	bouře
začala	začít	k5eAaPmAgFnS	začít
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
nálety	nálet	k1gInPc1	nálet
amerických	americký	k2eAgFnPc2d1	americká
a	a	k8xC	a
britských	britský	k2eAgFnPc2d1	britská
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
zapojeno	zapojit	k5eAaPmNgNnS	zapojit
37	[number]	k4	37
států	stát	k1gInPc2	stát
včetně	včetně	k7c2	včetně
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
protichemickou	protichemický	k2eAgFnSc4d1	protichemická
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bombardování	bombardování	k1gNnSc1	bombardování
trvalo	trvat	k5eAaImAgNnS	trvat
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
a	a	k8xC	a
skončilo	skončit	k5eAaPmAgNnS	skončit
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
způsobila	způsobit	k5eAaPmAgFnS	způsobit
i	i	k9	i
civilní	civilní	k2eAgFnPc4d1	civilní
oběti	oběť	k1gFnPc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Koalice	koalice	k1gFnSc1	koalice
útočila	útočit	k5eAaImAgFnS	útočit
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
vojenská	vojenský	k2eAgNnPc4d1	vojenské
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
mosty	most	k1gInPc4	most
<g/>
,	,	kIx,	,
elektrárny	elektrárna	k1gFnPc4	elektrárna
a	a	k8xC	a
televizní	televizní	k2eAgInPc4d1	televizní
vysílače	vysílač	k1gInPc4	vysílač
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
sloužily	sloužit	k5eAaImAgInP	sloužit
také	také	k6eAd1	také
civilnímu	civilní	k2eAgNnSc3d1	civilní
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
věhlasu	věhlas	k1gInSc2	věhlas
letectva	letectvo	k1gNnSc2	letectvo
a	a	k8xC	a
západních	západní	k2eAgFnPc2d1	západní
"	"	kIx"	"
<g/>
asymetrických	asymetrický	k2eAgFnPc2d1	asymetrická
válek	válka	k1gFnPc2	válka
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
vyžadováno	vyžadovat	k5eAaImNgNnS	vyžadovat
vítězství	vítězství	k1gNnSc2	vítězství
"	"	kIx"	"
<g/>
beze	beze	k7c2	beze
ztrát	ztráta	k1gFnPc2	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byl	být	k5eAaImAgInS	být
prezentován	prezentovat	k5eAaBmNgInS	prezentovat
jako	jako	k8xS	jako
špičková	špičkový	k2eAgFnSc1d1	špičková
technologická	technologický	k2eAgFnSc1d1	technologická
a	a	k8xC	a
bezchybná	bezchybný	k2eAgFnSc1d1	bezchybná
operace	operace	k1gFnSc1	operace
vedená	vedený	k2eAgFnSc1d1	vedená
dokonalými	dokonalý	k2eAgFnPc7d1	dokonalá
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Koalice	koalice	k1gFnSc1	koalice
ztratila	ztratit	k5eAaPmAgFnS	ztratit
necelých	celý	k2eNgNnPc2d1	necelé
pět	pět	k4xCc1	pět
set	sto	k4xCgNnPc2	sto
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
veteránů	veterán	k1gMnPc2	veterán
z	z	k7c2	z
války	válka	k1gFnSc2	válka
trpí	trpět	k5eAaImIp3nS	trpět
syndromem	syndrom	k1gInSc7	syndrom
"	"	kIx"	"
<g/>
Války	válka	k1gFnSc2	válka
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Iráčanů	Iráčan	k1gMnPc2	Iráčan
se	se	k3xPyFc4	se
ztráty	ztráta	k1gFnPc1	ztráta
počítají	počítat	k5eAaImIp3nP	počítat
na	na	k7c4	na
20	[number]	k4	20
000	[number]	k4	000
až	až	k9	až
200	[number]	k4	200
000	[number]	k4	000
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
na	na	k7c4	na
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
civilisté	civilista	k1gMnPc1	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
operaci	operace	k1gFnSc6	operace
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
nasazeny	nasadit	k5eAaPmNgFnP	nasadit
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
bojové	bojový	k2eAgFnSc6d1	bojová
akci	akce	k1gFnSc6	akce
americké	americký	k2eAgInPc1d1	americký
tanky	tank	k1gInPc1	tank
M1	M1	k1gFnPc2	M1
Abrams	Abramsa	k1gFnPc2	Abramsa
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgNnSc2	druhý
většího	veliký	k2eAgNnSc2d2	veliký
bojového	bojový	k2eAgNnSc2d1	bojové
nasazení	nasazení	k1gNnSc2	nasazení
se	se	k3xPyFc4	se
tankům	tank	k1gInPc3	tank
Abrahms	Abrahms	k1gInSc4	Abrahms
dostalo	dostat	k5eAaPmAgNnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
při	při	k7c6	při
operaci	operace	k1gFnSc6	operace
Irácká	irácký	k2eAgFnSc1d1	irácká
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
byly	být	k5eAaImAgFnP	být
nasazeny	nasadit	k5eAaPmNgFnP	nasadit
také	také	k9	také
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
pancéřování	pancéřování	k1gNnSc6	pancéřování
opodstatnily	opodstatnit	k5eAaPmAgInP	opodstatnit
pláty	plát	k1gInPc1	plát
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
(	(	kIx(	(
<g/>
při	při	k7c6	při
probití	probití	k1gNnSc6	probití
čelního	čelní	k2eAgInSc2d1	čelní
pancíře	pancíř	k1gInSc2	pancíř
se	se	k3xPyFc4	se
ochuzený	ochuzený	k2eAgInSc1d1	ochuzený
uran	uran	k1gInSc1	uran
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
používá	používat	k5eAaImIp3nS	používat
téměř	téměř	k6eAd1	téměř
devět	devět	k4xCc4	devět
tisíc	tisíc	k4xCgInSc4	tisíc
tanků	tank	k1gInPc2	tank
M1	M1	k1gFnSc2	M1
Abrams	Abramsa	k1gFnPc2	Abramsa
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tanky	tank	k1gInPc1	tank
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
také	také	k9	také
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
armádách	armáda	k1gFnPc6	armáda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
také	také	k9	také
však	však	k9	však
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
:	:	kIx,	:
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
a	a	k8xC	a
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Operace	operace	k1gFnSc1	operace
Pouštní	pouštní	k2eAgFnSc2d1	pouštní
bouře	bouř	k1gFnSc2	bouř
je	být	k5eAaImIp3nS	být
specifická	specifický	k2eAgFnSc1d1	specifická
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
roli	role	k1gFnSc4	role
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
hrála	hrát	k5eAaImAgFnS	hrát
média	médium	k1gNnSc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
z	z	k7c2	z
boje	boj	k1gInSc2	boj
byly	být	k5eAaImAgFnP	být
rozšiřovány	rozšiřovat	k5eAaImNgFnP	rozšiřovat
do	do	k7c2	do
televizních	televizní	k2eAgFnPc2d1	televizní
sítí	síť	k1gFnPc2	síť
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
zpráv	zpráva	k1gFnPc2	zpráva
pocházelo	pocházet	k5eAaImAgNnS	pocházet
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
pořízených	pořízený	k2eAgInPc2d1	pořízený
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
od	od	k7c2	od
novinářů	novinář	k1gMnPc2	novinář
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
diváci	divák	k1gMnPc1	divák
nemohli	moct	k5eNaImAgMnP	moct
spatřit	spatřit	k5eAaPmF	spatřit
umírající	umírající	k2eAgMnPc4d1	umírající
vojáky	voják	k1gMnPc4	voják
nebo	nebo	k8xC	nebo
civilisty	civilista	k1gMnPc4	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
invaze	invaze	k1gFnSc1	invaze
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
odstranila	odstranit	k5eAaPmAgFnS	odstranit
problém	problém	k1gInSc4	problém
vlády	vláda	k1gFnSc2	vláda
jednoho	jeden	k4xCgMnSc2	jeden
diktátora	diktátor	k1gMnSc2	diktátor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Irák	Irák	k1gInSc1	Irák
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zmítán	zmítat	k5eAaImNgInS	zmítat
náboženským	náboženský	k2eAgNnSc7d1	náboženské
i	i	k8xC	i
politickým	politický	k2eAgNnSc7d1	politické
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
kurdští	kurdský	k2eAgMnPc1d1	kurdský
povstalci	povstalec	k1gMnPc1	povstalec
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
území	území	k1gNnSc4	území
před	před	k7c7	před
protiofenzivou	protiofenziva	k1gFnSc7	protiofenziva
irácké	irácký	k2eAgFnSc2d1	irácká
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
uvrhla	uvrhnout	k5eAaPmAgFnS	uvrhnout
na	na	k7c4	na
cesty	cesta	k1gFnPc4	cesta
tisíce	tisíc	k4xCgInSc2	tisíc
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
povstali	povstat	k5eAaPmAgMnP	povstat
šíité	šíita	k1gMnPc1	šíita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
některé	některý	k3yIgFnSc2	některý
další	další	k2eAgFnSc2d1	další
země	zem	k1gFnSc2	zem
koalice	koalice	k1gFnSc2	koalice
zasahovaly	zasahovat	k5eAaImAgFnP	zasahovat
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
přijala	přijmout	k5eAaPmAgFnS	přijmout
dvě	dva	k4xCgFnPc4	dva
rezoluce	rezoluce	k1gFnPc4	rezoluce
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
byla	být	k5eAaImAgFnS	být
humanitární	humanitární	k2eAgFnSc4d1	humanitární
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
nastolení	nastolení	k1gNnSc4	nastolení
pořádku	pořádek	k1gInSc2	pořádek
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
rezoluce	rezoluce	k1gFnSc1	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
přijatá	přijatý	k2eAgFnSc1d1	přijatá
jednomyslně	jednomyslně	k6eAd1	jednomyslně
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1992	[number]	k4	1992
schválila	schválit	k5eAaPmAgFnS	schválit
vytvoření	vytvoření	k1gNnSc4	vytvoření
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
mírových	mírový	k2eAgFnPc2d1	mírová
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvořily	tvořit	k5eAaImAgFnP	tvořit
vojenské	vojenský	k2eAgInPc4d1	vojenský
oddíly	oddíl	k1gInPc4	oddíl
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
misí	mise	k1gFnPc2	mise
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
operace	operace	k1gFnSc2	operace
"	"	kIx"	"
<g/>
Obnovená	obnovený	k2eAgFnSc1d1	obnovená
naděje	naděje	k1gFnSc1	naděje
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
síly	síla	k1gFnPc1	síla
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
asi	asi	k9	asi
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
tisíc	tisíc	k4xCgInSc4	tisíc
mužů	muž	k1gMnPc2	muž
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zajistit	zajistit	k5eAaPmF	zajistit
dodávky	dodávka	k1gFnPc4	dodávka
humanitární	humanitární	k2eAgFnSc2d1	humanitární
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
mírový	mírový	k2eAgInSc1d1	mírový
zásah	zásah	k1gInSc1	zásah
<g/>
"	"	kIx"	"
proměnil	proměnit	k5eAaPmAgInS	proměnit
ve	v	k7c4	v
válečnou	válečný	k2eAgFnSc4d1	válečná
akci	akce	k1gFnSc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
přecházet	přecházet	k5eAaImF	přecházet
na	na	k7c4	na
koncept	koncept	k1gInSc4	koncept
"	"	kIx"	"
<g/>
humanitární	humanitární	k2eAgFnSc2d1	humanitární
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
narůstajících	narůstající	k2eAgFnPc2d1	narůstající
ztrát	ztráta	k1gFnPc2	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
ze	z	k7c2	z
Somálska	Somálsko	k1gNnSc2	Somálsko
stáhly	stáhnout	k5eAaPmAgInP	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Prokázalo	prokázat	k5eAaPmAgNnS	prokázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgInSc1	sám
koncept	koncept	k1gInSc1	koncept
"	"	kIx"	"
<g/>
asymetrické	asymetrický	k2eAgFnSc2d1	asymetrická
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
nevede	vést	k5eNaImIp3nS	vést
vždy	vždy	k6eAd1	vždy
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
válce	válka	k1gFnSc6	válka
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
politiků	politik	k1gMnPc2	politik
média	médium	k1gNnSc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Odchod	odchod	k1gInSc1	odchod
mírových	mírový	k2eAgFnPc2d1	mírová
sil	síla	k1gFnPc2	síla
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
k	k	k7c3	k
3	[number]	k4	3
<g/>
.	.	kIx.	.
březnu	březen	k1gInSc3	březen
1995	[number]	k4	1995
a	a	k8xC	a
Somálsko	Somálsko	k1gNnSc1	Somálsko
bylo	být	k5eAaImAgNnS	být
ponecháno	ponechat	k5eAaPmNgNnS	ponechat
napospas	napospas	k6eAd1	napospas
svému	svůj	k3xOyFgInSc3	svůj
osudu	osud	k1gInSc3	osud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Operace	operace	k1gFnSc1	operace
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
síla	síla	k1gFnSc1	síla
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Válkou	válka	k1gFnSc7	válka
proti	proti	k7c3	proti
Jugoslávii	Jugoslávie	k1gFnSc3	Jugoslávie
se	se	k3xPyFc4	se
válečné	válečný	k2eAgInPc1d1	válečný
konflikty	konflikt	k1gInPc1	konflikt
přenesly	přenést	k5eAaPmAgInP	přenést
na	na	k7c6	na
území	území	k1gNnSc6	území
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pádem	Pád	k1gInSc7	Pád
komunismu	komunismus	k1gInSc2	komunismus
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
půlstoletí	půlstoletí	k1gNnSc1	půlstoletí
dušeného	dušený	k2eAgNnSc2d1	dušené
národnostního	národnostní	k2eAgNnSc2d1	národnostní
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nikdy	nikdy	k6eAd1	nikdy
nezaniklo	zaniknout	k5eNaPmAgNnS	zaniknout
<g/>
,	,	kIx,	,
a	a	k8xC	a
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
intenzitou	intenzita	k1gFnSc7	intenzita
a	a	k8xC	a
brutalitou	brutalita	k1gFnSc7	brutalita
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
probuzení	probuzení	k1gNnSc3	probuzení
národnostní	národnostní	k2eAgFnSc2d1	národnostní
identity	identita	k1gFnSc2	identita
a	a	k8xC	a
rozhoření	rozhoření	k1gNnSc2	rozhoření
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
začala	začít	k5eAaPmAgFnS	začít
vojenským	vojenský	k2eAgInSc7d1	vojenský
konfliktem	konflikt	k1gInSc7	konflikt
mezi	mezi	k7c7	mezi
Jugoslávií	Jugoslávie	k1gFnSc7	Jugoslávie
a	a	k8xC	a
Slovinskem	Slovinsko	k1gNnSc7	Slovinsko
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
války	válka	k1gFnPc1	válka
mezi	mezi	k7c7	mezi
Jugoslávií	Jugoslávie	k1gFnSc7	Jugoslávie
a	a	k8xC	a
Chorvatskem	Chorvatsko	k1gNnSc7	Chorvatsko
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
koflikty	koflikt	k1gInPc4	koflikt
mezi	mezi	k7c7	mezi
Srby	Srb	k1gMnPc7	Srb
a	a	k8xC	a
Albánci	Albánec	k1gMnPc7	Albánec
na	na	k7c4	na
území	území	k1gNnSc4	území
Kosova	Kosův	k2eAgNnSc2d1	Kosovo
<g/>
.	.	kIx.	.
</s>
<s>
Západ	západ	k1gInSc1	západ
se	se	k3xPyFc4	se
marně	marně	k6eAd1	marně
snažil	snažit	k5eAaImAgMnS	snažit
usmířit	usmířit	k5eAaPmF	usmířit
znepřátelené	znepřátelený	k2eAgFnPc4d1	znepřátelená
politické	politický	k2eAgFnPc4d1	politická
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
další	další	k2eAgNnSc1d1	další
násilí	násilí	k1gNnSc1	násilí
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
dotlačil	dotlačit	k5eAaPmAgInS	dotlačit
k	k	k7c3	k
akci	akce	k1gFnSc3	akce
část	část	k1gFnSc4	část
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
provedení	provedení	k1gNnSc6	provedení
mise	mise	k1gFnSc2	mise
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k8xS	jako
humanitární	humanitární	k2eAgMnSc1d1	humanitární
<g/>
.	.	kIx.	.
</s>
<s>
Leteckou	letecký	k2eAgFnSc4d1	letecká
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
Srbsku	Srbsko	k1gNnSc3	Srbsko
schválila	schválit	k5eAaPmAgFnS	schválit
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
situaci	situace	k1gFnSc4	situace
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
mír	mír	k1gInSc4	mír
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
v	v	k7c6	v
tamní	tamní	k2eAgFnSc6d1	tamní
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
veta	veto	k1gNnSc2	veto
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
nebyla	být	k5eNaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
další	další	k2eAgFnSc1d1	další
rezoluce	rezoluce	k1gFnSc1	rezoluce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
dala	dát	k5eAaPmAgFnS	dát
zmocnění	zmocnění	k1gNnSc4	zmocnění
užít	užít	k5eAaPmF	užít
všech	všecek	k3xTgInPc2	všecek
nezbytných	nezbytný	k2eAgInPc2d1	nezbytný
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
válku	válka	k1gFnSc4	válka
nelegální	legální	k2eNgMnSc1d1	nelegální
<g/>
.	.	kIx.	.
<g/>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Bill	Bill	k1gMnSc1	Bill
Clinton	Clinton	k1gMnSc1	Clinton
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
postavit	postavit	k5eAaPmF	postavit
se	se	k3xPyFc4	se
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
trpících	trpící	k2eAgFnPc2d1	trpící
obětí	oběť	k1gFnPc2	oběť
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zajistit	zajistit	k5eAaPmF	zajistit
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
stabilitu	stabilita	k1gFnSc4	stabilita
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
zahájila	zahájit	k5eAaPmAgFnS	zahájit
vojska	vojsko	k1gNnPc1	vojsko
NATO	NATO	kA	NATO
vzdušné	vzdušný	k2eAgInPc4d1	vzdušný
údery	úder	k1gInPc4	úder
proti	proti	k7c3	proti
Srbsku	Srbsko	k1gNnSc3	Srbsko
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dostaly	dostat	k5eAaPmAgFnP	dostat
název	název	k1gInSc4	název
Operace	operace	k1gFnSc2	operace
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
velkou	velký	k2eAgFnSc7d1	velká
manifestací	manifestace	k1gFnSc7	manifestace
vojenských	vojenský	k2eAgFnPc2d1	vojenská
schopností	schopnost	k1gFnPc2	schopnost
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
došlo	dojít	k5eAaPmAgNnS	dojít
poprvé	poprvé	k6eAd1	poprvé
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
vojenských	vojenský	k2eAgFnPc2d1	vojenská
sil	síla	k1gFnPc2	síla
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
podílelo	podílet	k5eAaImAgNnS	podílet
svými	svůj	k3xOyFgFnPc7	svůj
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
kapacitami	kapacita	k1gFnPc7	kapacita
třináct	třináct	k4xCc4	třináct
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vzdušné	vzdušný	k2eAgInPc1d1	vzdušný
údery	úder	k1gInPc1	úder
trvaly	trvat	k5eAaImAgInP	trvat
tři	tři	k4xCgInPc1	tři
měsíce	měsíc	k1gInPc1	měsíc
a	a	k8xC	a
skončily	skončit	k5eAaPmAgFnP	skončit
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
použitým	použitý	k2eAgInPc3d1	použitý
zbrojním	zbrojní	k2eAgInPc3d1	zbrojní
systémům	systém	k1gInPc3	systém
neměly	mít	k5eNaImAgFnP	mít
jakkoliv	jakkoliv	k6eAd1	jakkoliv
zkušené	zkušený	k2eAgFnPc1d1	zkušená
srbské	srbský	k2eAgFnPc1d1	Srbská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
žádnou	žádný	k3yNgFnSc4	žádný
šanci	šance	k1gFnSc4	šance
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
asymetrickou	asymetrický	k2eAgFnSc4d1	asymetrická
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
výsledek	výsledek	k1gInSc1	výsledek
byl	být	k5eAaImAgInS	být
předem	předem	k6eAd1	předem
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
útokům	útok	k1gInPc3	útok
proti	proti	k7c3	proti
ryze	ryze	k6eAd1	ryze
nevojenským	vojenský	k2eNgInPc3d1	nevojenský
cílům	cíl	k1gInPc3	cíl
(	(	kIx(	(
<g/>
kolona	kolona	k1gFnSc1	kolona
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
,	,	kIx,	,
nemocnice	nemocnice	k1gFnPc1	nemocnice
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
srbské	srbský	k2eAgFnSc2d1	Srbská
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgInSc1d1	osobní
vlak	vlak	k1gInSc1	vlak
<g/>
,	,	kIx,	,
autobus	autobus	k1gInSc1	autobus
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konečná	konečný	k2eAgFnSc1d1	konečná
bilance	bilance	k1gFnSc1	bilance
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
zaplacená	zaplacený	k2eAgFnSc1d1	zaplacená
za	za	k7c4	za
nacionalismus	nacionalismus	k1gInSc4	nacionalismus
a	a	k8xC	a
taktiku	taktika	k1gFnSc4	taktika
"	"	kIx"	"
<g/>
etnických	etnický	k2eAgFnPc2d1	etnická
čistek	čistka	k1gFnPc2	čistka
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
tragická	tragický	k2eAgFnSc1d1	tragická
<g/>
:	:	kIx,	:
asi	asi	k9	asi
200	[number]	k4	200
000	[number]	k4	000
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
stejné	stejný	k2eAgNnSc4d1	stejné
číslo	číslo	k1gNnSc4	číslo
zraněných	zraněný	k1gMnPc2	zraněný
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
intenzivní	intenzivní	k2eAgFnSc4d1	intenzivní
snahu	snaha	k1gFnSc4	snaha
G.	G.	kA	G.
W.	W.	kA	W.
Bushe	Bush	k1gMnSc2	Bush
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dodatečně	dodatečně	k6eAd1	dodatečně
neprokázaly	prokázat	k5eNaPmAgInP	prokázat
důvody	důvod	k1gInPc1	důvod
pro	pro	k7c4	pro
vedení	vedení	k1gNnSc4	vedení
války	válka	k1gFnSc2	válka
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
válku	válka	k1gFnSc4	válka
nelegitimní	legitimní	k2eNgFnSc4d1	nelegitimní
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
USA	USA	kA	USA
a	a	k8xC	a
EU	EU	kA	EU
jednostranně	jednostranně	k6eAd1	jednostranně
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Srbsku	Srbsko	k1gNnSc6	Srbsko
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Srbsko	Srbsko	k1gNnSc1	Srbsko
odmítá	odmítat	k5eAaImIp3nS	odmítat
uznat	uznat	k5eAaPmF	uznat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Operace	operace	k1gFnSc1	operace
Trvalá	trvalá	k1gFnSc1	trvalá
svoboda	svoboda	k1gFnSc1	svoboda
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
nových	nový	k2eAgFnPc2d1	nová
forem	forma	k1gFnPc2	forma
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
terorismu	terorismus	k1gInSc2	terorismus
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
nový	nový	k2eAgInSc4d1	nový
směr	směr	k1gInSc4	směr
americké	americký	k2eAgFnSc2d1	americká
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
G.	G.	kA	G.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
totální	totální	k2eAgNnSc4d1	totální
"	"	kIx"	"
<g/>
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
<g/>
"	"	kIx"	"
a	a	k8xC	a
všechny	všechen	k3xTgInPc4	všechen
státy	stát	k1gInPc4	stát
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
především	především	k9	především
ty	ten	k3xDgFnPc1	ten
západní	západní	k2eAgFnPc1d1	západní
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vyzvány	vyzván	k2eAgFnPc1d1	vyzvána
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
k	k	k7c3	k
USA	USA	kA	USA
připojily	připojit	k5eAaPmAgInP	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Odpovědí	odpověď	k1gFnSc7	odpověď
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
na	na	k7c4	na
teroristické	teroristický	k2eAgInPc4d1	teroristický
útoky	útok	k1gInPc4	útok
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
provedení	provedení	k1gNnSc1	provedení
vojenských	vojenský	k2eAgFnPc2d1	vojenská
operací	operace	k1gFnPc2	operace
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
a	a	k8xC	a
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
proti	proti	k7c3	proti
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
sérií	série	k1gFnSc7	série
leteckých	letecký	k2eAgInPc2d1	letecký
útoků	útok	k1gInPc2	útok
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
provedly	provést	k5eAaPmAgInP	provést
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
vlády	vláda	k1gFnSc2	vláda
stál	stát	k5eAaImAgMnS	stát
britský	britský	k2eAgMnSc1d1	britský
premiér	premiér	k1gMnSc1	premiér
Tony	Tony	k1gMnSc1	Tony
Blair	Blair	k1gMnSc1	Blair
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2001	[number]	k4	2001
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
Operace	operace	k1gFnSc1	operace
Trvalá	trvalá	k1gFnSc1	trvalá
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
nebyl	být	k5eNaImAgInS	být
potrestán	potrestat	k5eAaPmNgInS	potrestat
za	za	k7c4	za
agresi	agrese	k1gFnSc4	agrese
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dovolil	dovolit	k5eAaPmAgInS	dovolit
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
organizaci	organizace	k1gFnSc4	organizace
al-Kaída	al-Kaída	k1gFnSc1	al-Kaída
naplánovat	naplánovat	k5eAaBmF	naplánovat
teroristické	teroristický	k2eAgInPc4d1	teroristický
útoky	útok	k1gInPc4	útok
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
zvolili	zvolit	k5eAaPmAgMnP	zvolit
cestu	cesta	k1gFnSc4	cesta
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
zničit	zničit	k5eAaPmF	zničit
teroristickou	teroristický	k2eAgFnSc4d1	teroristická
organizaci	organizace	k1gFnSc4	organizace
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
potrestat	potrestat	k5eAaPmF	potrestat
hostitelský	hostitelský	k2eAgInSc4d1	hostitelský
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
jako	jako	k9	jako
odveta	odveta	k1gFnSc1	odveta
za	za	k7c4	za
teroristické	teroristický	k2eAgInPc4d1	teroristický
útoky	útok	k1gInPc4	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
přijala	přijmout	k5eAaPmAgFnS	přijmout
první	první	k4xOgFnSc4	první
rezoluci	rezoluce	k1gFnSc4	rezoluce
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
dala	dát	k5eAaPmAgFnS	dát
souhlas	souhlas	k1gInSc4	souhlas
se	s	k7c7	s
zahájením	zahájení	k1gNnSc7	zahájení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
netrvalo	trvat	k5eNaImAgNnS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
RB	RB	kA	RB
OSN	OSN	kA	OSN
jednala	jednat	k5eAaImAgFnS	jednat
také	také	k9	také
o	o	k7c6	o
druhé	druhý	k4xOgFnSc6	druhý
rezoluci	rezoluce	k1gFnSc6	rezoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
větu	věta	k1gFnSc4	věta
o	o	k7c4	o
zmocnění	zmocnění	k1gNnSc4	zmocnění
užít	užít	k5eAaPmF	užít
všech	všecek	k3xTgInPc2	všecek
nezbytných	nezbytný	k2eAgInPc2d1	nezbytný
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
válka	válka	k1gFnSc1	válka
legitimní	legitimní	k2eAgFnSc1d1	legitimní
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
důvody	důvod	k1gInPc1	důvod
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterým	který	k3yIgInPc3	který
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
operaci	operace	k1gFnSc6	operace
se	se	k3xPyFc4	se
podílelo	podílet	k5eAaImAgNnS	podílet
celkem	celkem	k6eAd1	celkem
51	[number]	k4	51
zemí	zem	k1gFnPc2	zem
včetně	včetně	k7c2	včetně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
rozhodly	rozhodnout	k5eAaPmAgInP	rozhodnout
soustředit	soustředit	k5eAaPmF	soustředit
své	svůj	k3xOyFgNnSc4	svůj
úsilí	úsilí	k1gNnSc4	úsilí
na	na	k7c4	na
Irák	Irák	k1gInSc4	Irák
<g/>
,	,	kIx,	,
vyzvaly	vyzvat	k5eAaPmAgInP	vyzvat
Severoatlantickou	severoatlantický	k2eAgFnSc4d1	Severoatlantická
alianci	aliance	k1gFnSc4	aliance
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zajistila	zajistit	k5eAaPmAgFnS	zajistit
velení	velení	k1gNnSc4	velení
jednotek	jednotka	k1gFnPc2	jednotka
umístěných	umístěný	k2eAgFnPc2d1	umístěná
v	v	k7c6	v
Afhánistánu	Afhánistán	k1gInSc6	Afhánistán
<g/>
:	:	kIx,	:
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
hrají	hrát	k5eAaImIp3nP	hrát
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
roli	role	k1gFnSc4	role
při	při	k7c6	při
poskytování	poskytování	k1gNnSc6	poskytování
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Trvalo	trvat	k5eAaImAgNnS	trvat
však	však	k9	však
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
než	než	k8xS	než
zde	zde	k6eAd1	zde
rozvojová	rozvojový	k2eAgFnSc1d1	rozvojová
pomoc	pomoc	k1gFnSc1	pomoc
dostala	dostat	k5eAaPmAgFnS	dostat
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
efekty	efekt	k1gInPc1	efekt
této	tento	k3xDgFnSc2	tento
pomoci	pomoc	k1gFnSc2	pomoc
se	se	k3xPyFc4	se
dodatečně	dodatečně	k6eAd1	dodatečně
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
jako	jako	k9	jako
nedostatečné	dostatečný	k2eNgNnSc4d1	nedostatečné
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
spojenci	spojenec	k1gMnPc1	spojenec
za	za	k7c7	za
sebou	se	k3xPyFc7	se
zanechali	zanechat	k5eAaPmAgMnP	zanechat
zemi	zem	k1gFnSc3	zem
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
rozdělenou	rozdělený	k2eAgFnSc7d1	rozdělená
než	než	k8xS	než
předtím	předtím	k6eAd1	předtím
a	a	k8xC	a
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
zničenou	zničený	k2eAgFnSc7d1	zničená
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
civilních	civilní	k2eAgFnPc2d1	civilní
obětí	oběť	k1gFnPc2	oběť
násilností	násilnost	k1gFnPc2	násilnost
a	a	k8xC	a
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
počtu	počet	k1gInSc2	počet
3545	[number]	k4	3545
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Operace	operace	k1gFnSc1	operace
Irácká	irácký	k2eAgFnSc1d1	irácká
svoboda	svoboda	k1gFnSc1	svoboda
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Irácká	irácký	k2eAgFnSc1d1	irácká
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
George	George	k1gNnSc2	George
Walker	Walker	k1gMnSc1	Walker
Bush	Bush	k1gMnSc1	Bush
byl	být	k5eAaImAgMnS	být
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
nejbližšími	blízký	k2eAgMnPc7d3	nejbližší
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
zvlášť	zvlášť	k6eAd1	zvlášť
angažoval	angažovat	k5eAaBmAgMnS	angažovat
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
Donald	Donald	k1gMnSc1	Donald
Rumsfeld	Rumsfeld	k1gMnSc1	Rumsfeld
<g/>
,	,	kIx,	,
odhodlán	odhodlán	k2eAgMnSc1d1	odhodlán
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
svrhnout	svrhnout	k5eAaPmF	svrhnout
Saddáma	Saddám	k1gMnSc4	Saddám
Husajna	Husajn	k1gMnSc4	Husajn
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
režim	režim	k1gInSc4	režim
nahradit	nahradit	k5eAaPmF	nahradit
novou	nový	k2eAgFnSc7d1	nová
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
RB	RB	kA	RB
OSN	OSN	kA	OSN
nedala	dát	k5eNaPmAgFnS	dát
k	k	k7c3	k
válečnému	válečný	k2eAgInSc3d1	válečný
tažení	tažení	k1gNnSc1	tažení
mandát	mandát	k1gInSc1	mandát
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
a	a	k8xC	a
Paříž	Paříž	k1gFnSc1	Paříž
se	se	k3xPyFc4	se
otevřeně	otevřeně	k6eAd1	otevřeně
distancovaly	distancovat	k5eAaBmAgFnP	distancovat
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
nenabídla	nabídnout	k5eNaPmAgFnS	nabídnout
souhlas	souhlas	k1gInSc4	souhlas
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
pochyboval	pochybovat	k5eAaImAgInS	pochybovat
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
bylo	být	k5eAaImAgNnS	být
proti	proti	k7c3	proti
zásahu	zásah	k1gInSc3	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
početné	početný	k2eAgFnPc4d1	početná
námitky	námitka	k1gFnPc4	námitka
Američané	Američan	k1gMnPc1	Američan
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
pozměnit	pozměnit	k5eAaPmF	pozměnit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
trvali	trvat	k5eAaImAgMnP	trvat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
jednostranném	jednostranný	k2eAgNnSc6d1	jednostranné
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
podpořily	podpořit	k5eAaPmAgFnP	podpořit
pouze	pouze	k6eAd1	pouze
Londýn	Londýn	k1gInSc4	Londýn
a	a	k8xC	a
Canberra	Canberra	k1gFnSc1	Canberra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
vedení	vedení	k1gNnSc4	vedení
užší	úzký	k2eAgFnSc2d2	užší
koalice	koalice	k1gFnSc2	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vojenské	vojenský	k2eAgFnSc3d1	vojenská
a	a	k8xC	a
technologické	technologický	k2eAgFnSc3d1	technologická
převaze	převaha	k1gFnSc3	převaha
a	a	k8xC	a
přímému	přímý	k2eAgInSc3d1	přímý
postupu	postup	k1gInSc3	postup
bylo	být	k5eAaImAgNnS	být
dosaženo	dosažen	k2eAgNnSc1d1	dosaženo
cíle	cíl	k1gInPc4	cíl
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Přípravná	přípravný	k2eAgFnSc1d1	přípravná
letecká	letecký	k2eAgFnSc1d1	letecká
operace	operace	k1gFnSc1	operace
užívala	užívat	k5eAaImAgFnS	užívat
velice	velice	k6eAd1	velice
silné	silný	k2eAgFnPc4d1	silná
bomby	bomba	k1gFnPc4	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zničení	zničení	k1gNnSc6	zničení
slabé	slabý	k2eAgFnSc2d1	slabá
protivzdušné	protivzdušný	k2eAgFnSc2d1	protivzdušná
obrany	obrana	k1gFnSc2	obrana
přelétávala	přelétávat	k5eAaImAgFnS	přelétávat
neustále	neustále	k6eAd1	neustále
letadla	letadlo	k1gNnPc1	letadlo
bez	bez	k7c2	bez
lidské	lidský	k2eAgFnSc2d1	lidská
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
využilo	využít	k5eAaPmAgNnS	využít
kompletní	kompletní	k2eAgInSc4d1	kompletní
systém	systém	k1gInSc4	systém
nově	nově	k6eAd1	nově
vyprojektovaných	vyprojektovaný	k2eAgFnPc2d1	vyprojektovaná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
prováděli	provádět	k5eAaImAgMnP	provádět
na	na	k7c4	na
irácké	irácký	k2eAgFnPc4d1	irácká
síly	síla	k1gFnPc4	síla
nepřetržitý	přetržitý	k2eNgInSc1d1	nepřetržitý
<g/>
,	,	kIx,	,
denní	denní	k2eAgInSc1d1	denní
a	a	k8xC	a
noční	noční	k2eAgInSc1d1	noční
útok	útok	k1gInSc1	útok
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2003	[number]	k4	2003
mohl	moct	k5eAaImAgMnS	moct
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
G.	G.	kA	G.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
oznámit	oznámit	k5eAaPmF	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mise	mise	k1gFnSc1	mise
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
koalice	koalice	k1gFnSc2	koalice
sestavené	sestavený	k2eAgFnSc2d1	sestavená
pro	pro	k7c4	pro
válku	válka	k1gFnSc4	válka
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
počtu	počet	k1gInSc2	počet
ke	k	k7c3	k
třiceti	třicet	k4xCc2	třicet
zemím	zem	k1gFnPc3	zem
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
účast	účast	k1gFnSc1	účast
některých	některý	k3yIgMnPc2	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
symbolická	symbolický	k2eAgFnSc1d1	symbolická
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledávaným	vyhledávaný	k2eAgInSc7d1	vyhledávaný
cílem	cíl	k1gInSc7	cíl
byly	být	k5eAaImAgInP	být
také	také	k9	také
komunikační	komunikační	k2eAgFnPc1d1	komunikační
sítě	síť	k1gFnPc1	síť
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
vodovodní	vodovodní	k2eAgNnPc4d1	vodovodní
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgInPc4d1	elektrický
rozvody	rozvod	k1gInPc4	rozvod
aj.	aj.	kA	aj.
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sloužily	sloužit	k5eAaImAgFnP	sloužit
i	i	k8xC	i
civilistům	civilista	k1gMnPc3	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
padlo	padnout	k5eAaPmAgNnS	padnout
ke	k	k7c3	k
13	[number]	k4	13
000	[number]	k4	000
Iráčanů	Iráčan	k1gMnPc2	Iráčan
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
bylo	být	k5eAaImAgNnS	být
kolem	kolem	k6eAd1	kolem
4	[number]	k4	4
000	[number]	k4	000
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
mrtví	mrtvý	k1gMnPc1	mrtvý
však	však	k9	však
denně	denně	k6eAd1	denně
přibývají	přibývat	k5eAaImIp3nP	přibývat
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
dodatečně	dodatečně	k6eAd1	dodatečně
neprokázalo	prokázat	k5eNaPmAgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
válka	válka	k1gFnSc1	válka
naplnila	naplnit	k5eAaPmAgFnS	naplnit
důvody	důvod	k1gInPc4	důvod
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgMnPc4	který
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
obvinění	obvinění	k1gNnSc4	obvinění
Iráku	Irák	k1gInSc2	Irák
z	z	k7c2	z
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
z	z	k7c2	z
kontaktů	kontakt	k1gInPc2	kontakt
s	s	k7c7	s
organizací	organizace	k1gFnSc7	organizace
al-Káida	al-Káid	k1gMnSc2	al-Káid
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
válku	válka	k1gFnSc4	válka
nelegitimní	legitimní	k2eNgMnSc1d1	nelegitimní
<g/>
.	.	kIx.	.
<g/>
Se	s	k7c7	s
skončením	skončení	k1gNnSc7	skončení
vojenské	vojenský	k2eAgFnSc2d1	vojenská
mise	mise	k1gFnSc2	mise
však	však	k9	však
neskončily	skončit	k5eNaPmAgInP	skončit
místní	místní	k2eAgInPc4d1	místní
problémy	problém	k1gInPc4	problém
<g/>
:	:	kIx,	:
došlo	dojít	k5eAaPmAgNnS	dojít
sice	sice	k8xC	sice
ke	k	k7c3	k
svržení	svržení	k1gNnSc3	svržení
Saddáma	Saddám	k1gMnSc2	Saddám
Husajna	Husajn	k1gMnSc2	Husajn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
vypluly	vyplout	k5eAaPmAgInP	vyplout
rozdíly	rozdíl	k1gInPc1	rozdíl
uvnitř	uvnitř	k7c2	uvnitř
místní	místní	k2eAgFnSc2d1	místní
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
<g/>
,	,	kIx,	,
Američany	Američan	k1gMnPc7	Američan
nastolená	nastolený	k2eAgFnSc1d1	nastolená
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
iráckým	irácký	k2eAgNnSc7d1	irácké
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
místních	místní	k2eAgFnPc2d1	místní
vojenských	vojenský	k2eAgFnPc2d1	vojenská
frakcí	frakce	k1gFnPc2	frakce
se	se	k3xPyFc4	se
spojilo	spojit	k5eAaPmAgNnS	spojit
v	v	k7c6	v
ozbrojeném	ozbrojený	k2eAgInSc6d1	ozbrojený
odporu	odpor	k1gInSc6	odpor
proti	proti	k7c3	proti
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dál	daleko	k6eAd2	daleko
šířit	šířit	k5eAaImF	šířit
"	"	kIx"	"
<g/>
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
prvním	první	k4xOgNnSc7	první
ani	ani	k8xC	ani
posledním	poslední	k2eAgInSc7d1	poslední
projevem	projev	k1gInSc7	projev
islamistického	islamistický	k2eAgInSc2d1	islamistický
teroru	teror	k1gInSc2	teror
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
němu	on	k3xPp3gInSc3	on
začalo	začít	k5eAaPmAgNnS	začít
docházet	docházet	k5eAaImF	docházet
již	již	k6eAd1	již
střetem	střet	k1gInSc7	střet
islámu	islám	k1gInSc2	islám
s	s	k7c7	s
koloniálními	koloniální	k2eAgFnPc7d1	koloniální
mocnostmi	mocnost	k1gFnPc7	mocnost
a	a	k8xC	a
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
také	také	k9	také
střetem	střet	k1gInSc7	střet
s	s	k7c7	s
novými	nový	k2eAgMnPc7d1	nový
představiteli	představitel	k1gMnPc7	představitel
mladých	mladý	k2eAgInPc2d1	mladý
afrických	africký	k2eAgInPc2d1	africký
a	a	k8xC	a
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Operace	operace	k1gFnSc1	operace
Písečná	písečný	k2eAgFnSc1d1	písečná
bouře	bouře	k1gFnSc1	bouře
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Operace	operace	k1gFnSc1	operace
Písečná	písečný	k2eAgFnSc1d1	písečná
bouře	bouře	k1gFnSc1	bouře
(	(	kIx(	(
<g/>
Odyssea	Odyssea	k1gFnSc1	Odyssea
Dawn	Dawn	k1gMnSc1	Dawn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vojenská	vojenský	k2eAgFnSc1d1	vojenská
intervence	intervence	k1gFnSc1	intervence
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vojska	vojsko	k1gNnSc2	vojsko
Aliance	aliance	k1gFnSc2	aliance
zahájila	zahájit	k5eAaPmAgFnS	zahájit
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Libyjský	libyjský	k2eAgMnSc1d1	libyjský
představitel	představitel	k1gMnSc1	představitel
Muammar	Muammar	k1gMnSc1	Muammar
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
byl	být	k5eAaImAgMnS	být
odstraněn	odstranit	k5eAaPmNgMnS	odstranit
událostmi	událost	k1gFnPc7	událost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
přineslo	přinést	k5eAaPmAgNnS	přinést
Arabské	arabský	k2eAgNnSc1d1	arabské
jaro	jaro	k1gNnSc1	jaro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
země	země	k1gFnSc1	země
upadla	upadnout	k5eAaPmAgFnS	upadnout
do	do	k7c2	do
trvalého	trvalý	k2eAgInSc2d1	trvalý
zmatku	zmatek	k1gInSc2	zmatek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nepokojích	nepokoj	k1gInPc6	nepokoj
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
padlo	padnout	k5eAaImAgNnS	padnout
město	město	k1gNnSc1	město
Benghází	Bengháze	k1gFnPc2	Bengháze
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
povstalců	povstalec	k1gMnPc2	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
moc	moc	k1gFnSc1	moc
zahájila	zahájit	k5eAaPmAgFnS	zahájit
protiofenzívu	protiofenzíva	k1gFnSc4	protiofenzíva
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
byl	být	k5eAaImAgMnS	být
Nicolas	Nicolas	k1gInSc4	Nicolas
Sarkozy	Sarkoz	k1gInPc7	Sarkoz
<g/>
,	,	kIx,	,
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vynutily	vynutit	k5eAaPmAgFnP	vynutit
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
hlasování	hlasování	k1gNnSc2	hlasování
RB	RB	kA	RB
OSN	OSN	kA	OSN
o	o	k7c4	o
rezoluci	rezoluce	k1gFnSc4	rezoluce
č.	č.	k?	č.
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
se	se	k3xPyFc4	se
hlasování	hlasování	k1gNnSc4	hlasování
zdržely	zdržet	k5eAaPmAgFnP	zdržet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
závazku	závazek	k1gInSc2	závazek
ochrany	ochrana	k1gFnSc2	ochrana
libyjských	libyjský	k2eAgMnPc2d1	libyjský
civilistů	civilista	k1gMnPc2	civilista
text	text	k1gInSc1	text
stanovil	stanovit	k5eAaPmAgInS	stanovit
bezletovou	bezletový	k2eAgFnSc4d1	bezletová
zónu	zóna	k1gFnSc4	zóna
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
zákaz	zákaz	k1gInSc1	zákaz
bombardování	bombardování	k1gNnSc2	bombardování
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
byl	být	k5eAaImAgInS	být
vyloučen	vyloučen	k2eAgInSc1d1	vyloučen
pozemní	pozemní	k2eAgInSc1d1	pozemní
zásah	zásah	k1gInSc1	zásah
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
události	událost	k1gFnPc1	událost
zásah	zásah	k1gInSc1	zásah
provedený	provedený	k2eAgInSc1d1	provedený
francouzskými	francouzský	k2eAgInPc7d1	francouzský
letouny	letoun	k1gInPc7	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nálet	nálet	k1gInSc1	nálet
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
předehrou	předehra	k1gFnSc7	předehra
pro	pro	k7c4	pro
intenzivní	intenzivní	k2eAgInPc4d1	intenzivní
údery	úder	k1gInPc4	úder
vedené	vedený	k2eAgInPc4d1	vedený
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
operace	operace	k1gFnSc2	operace
Písečná	písečný	k2eAgFnSc1d1	písečná
bouře	bouře	k1gFnSc1	bouře
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
operace	operace	k1gFnSc1	operace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
prováděna	provádět	k5eAaImNgFnS	provádět
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
oblohy	obloha	k1gFnSc2	obloha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
19	[number]	k4	19
zemí	zem	k1gFnPc2	zem
podporovaných	podporovaný	k2eAgFnPc2d1	podporovaná
leteckou	letecký	k2eAgFnSc7d1	letecká
armádou	armáda	k1gFnSc7	armáda
s	s	k7c7	s
americkými	americký	k2eAgInPc7d1	americký
bombardéry	bombardér	k1gInPc7	bombardér
a	a	k8xC	a
bombardéry	bombardér	k1gInPc7	bombardér
kanadskými	kanadský	k2eAgInPc7d1	kanadský
se	s	k7c7	s
státními	státní	k2eAgInPc7d1	státní
znaky	znak	k1gInPc7	znak
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Kataru	katar	k1gInSc2	katar
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
arabských	arabský	k2eAgInPc2d1	arabský
emirátů	emirát	k1gInPc2	emirát
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
operace	operace	k1gFnSc2	operace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
nad	nad	k7c7	nad
územím	území	k1gNnSc7	území
jiného	jiný	k2eAgInSc2d1	jiný
státu	stát	k1gInSc2	stát
se	s	k7c7	s
zřetelnou	zřetelný	k2eAgFnSc7d1	zřetelná
převahou	převaha	k1gFnSc7	převaha
vojenských	vojenský	k2eAgFnPc2d1	vojenská
sil	síla	k1gFnPc2	síla
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
útočníka	útočník	k1gMnSc2	útočník
<g/>
,	,	kIx,	,
připadlo	připadnout	k5eAaPmAgNnS	připadnout
NATO	nato	k6eAd1	nato
a	a	k8xC	a
manévry	manévr	k1gInPc1	manévr
řídily	řídit	k5eAaImAgInP	řídit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
prezident	prezident	k1gMnSc1	prezident
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
útočníka	útočník	k1gMnSc2	útočník
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
minimálním	minimální	k2eAgFnPc3d1	minimální
ztrátám	ztráta	k1gFnPc3	ztráta
na	na	k7c6	na
lidských	lidský	k2eAgInPc6d1	lidský
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byl	být	k5eAaImAgMnS	být
Muammar	Muammar	k1gMnSc1	Muammar
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
místními	místní	k2eAgMnPc7d1	místní
povstalci	povstalec	k1gMnPc7	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
libyjská	libyjský	k2eAgFnSc1d1	Libyjská
společnost	společnost	k1gFnSc1	společnost
neunikla	uniknout	k5eNaPmAgFnS	uniknout
nadvládě	nadvláda	k1gFnSc3	nadvláda
násilí	násilí	k1gNnSc2	násilí
a	a	k8xC	a
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
trvá	trvat	k5eAaImIp3nS	trvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Sesazení	sesazení	k1gNnSc1	sesazení
tyrana	tyran	k1gMnSc2	tyran
nezaručuje	zaručovat	k5eNaImIp3nS	zaručovat
přechod	přechod	k1gInSc4	přechod
k	k	k7c3	k
demokracii	demokracie	k1gFnSc3	demokracie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nárůst	nárůst	k1gInSc1	nárůst
počtu	počet	k1gInSc2	počet
asymetrických	asymetrický	k2eAgFnPc2d1	asymetrická
válek	válka	k1gFnPc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Revolucí	revoluce	k1gFnSc7	revoluce
ve	v	k7c6	v
vojenské	vojenský	k2eAgFnSc6d1	vojenská
technice	technika	k1gFnSc6	technika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
informatika	informatika	k1gFnSc1	informatika
a	a	k8xC	a
telematika	telematika	k1gFnSc1	telematika
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
součástí	součást	k1gFnSc7	součást
bojové	bojový	k2eAgFnPc1d1	bojová
činnosti	činnost	k1gFnPc1	činnost
<g/>
,	,	kIx,	,
způsobem	způsob	k1gInSc7	způsob
zpracování	zpracování	k1gNnSc2	zpracování
bojových	bojový	k2eAgNnPc2d1	bojové
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
použitím	použití	k1gNnSc7	použití
vyvinutých	vyvinutý	k2eAgFnPc2d1	vyvinutá
zbraní	zbraň	k1gFnPc2	zbraň
umožňujícím	umožňující	k2eAgInSc7d1	umožňující
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
nepředstavitelnou	představitelný	k2eNgFnSc7d1	nepředstavitelná
přesností	přesnost	k1gFnSc7	přesnost
i	i	k9	i
v	v	k7c6	v
nepříznivých	příznivý	k2eNgFnPc6d1	nepříznivá
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
např	např	kA	např
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
nebo	nebo	k8xC	nebo
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
,	,	kIx,	,
aplikací	aplikace	k1gFnPc2	aplikace
informatiky	informatika	k1gFnSc2	informatika
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
zbraní	zbraň	k1gFnPc2	zbraň
aj.	aj.	kA	aj.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dospělo	dochvít	k5eAaPmAgNnS	dochvít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
války	válka	k1gFnPc1	válka
stávají	stávat	k5eAaImIp3nP	stávat
stále	stále	k6eAd1	stále
asymetričtějšími	asymetrický	k2eAgInPc7d2	asymetrický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Asymmetric	Asymmetrice	k1gFnPc2	Asymmetrice
warfare	warfar	k1gMnSc5	warfar
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Huntington	Huntington	k1gInSc1	Huntington
Samuel	Samuel	k1gMnSc1	Samuel
P.	P.	kA	P.
<g/>
,	,	kIx,	,
Střet	střet	k1gInSc1	střet
civilizací	civilizace	k1gFnPc2	civilizace
/	/	kIx~	/
<g/>
Boj	boj	k1gInSc1	boj
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
proměna	proměna	k1gFnSc1	proměna
světového	světový	k2eAgInSc2d1	světový
řádu	řád	k1gInSc2	řád
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rybka	rybka	k1gFnSc1	rybka
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
447	[number]	k4	447
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86182	[number]	k4	86182
<g/>
-	-	kIx~	-
<g/>
49	[number]	k4	49
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
85	[number]	k4	85
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
</s>
</p>
