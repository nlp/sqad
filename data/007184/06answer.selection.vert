<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
největší	veliký	k2eAgFnSc1d3	veliký
terestrická	terestrický	k2eAgFnSc1d1	terestrická
planeta	planeta	k1gFnSc1	planeta
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
a	a	k8xC	a
jediné	jediný	k2eAgNnSc1d1	jediné
planetární	planetární	k2eAgNnSc1d1	planetární
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
současných	současný	k2eAgInPc2d1	současný
vědeckých	vědecký	k2eAgInPc2d1	vědecký
poznatků	poznatek	k1gInPc2	poznatek
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
