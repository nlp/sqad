<s>
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
(	(	kIx(	(
<g/>
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
tetum	tetum	k1gNnSc1	tetum
Timór	Timór	k1gInSc1	Timór
Lorosa	Lorosa	k1gFnSc1	Lorosa
<g/>
'	'	kIx"	'
<g/>
e	e	k0	e
<g/>
,	,	kIx,	,
portugalsky	portugalsky	k6eAd1	portugalsky
Timor-Leste	Timor-Lest	k1gInSc5	Timor-Lest
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
stát	stát	k1gInSc1	stát
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ostrova	ostrov	k1gInSc2	ostrov
Timor	Timora	k1gFnPc2	Timora
v	v	k7c6	v
souostroví	souostroví	k1gNnSc6	souostroví
Malé	Malé	k2eAgInPc1d1	Malé
Sundy	sund	k1gInPc1	sund
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
milion	milion	k4xCgInSc4	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejmladších	mladý	k2eAgInPc2d3	nejmladší
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
-	-	kIx~	-
samostatnost	samostatnost	k1gFnSc4	samostatnost
sice	sice	k8xC	sice
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naprostou	naprostý	k2eAgFnSc7d1	naprostá
většinou	většina	k1gFnSc7	většina
zemí	zem	k1gFnPc2	zem
byl	být	k5eAaImAgInS	být
uznán	uznat	k5eAaPmNgInS	uznat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
samostatnost	samostatnost	k1gFnSc4	samostatnost
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
referendum	referendum	k1gNnSc1	referendum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
hlediska	hledisko	k1gNnSc2	hledisko
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rozvojovou	rozvojový	k2eAgFnSc4d1	rozvojová
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
zemi	zem	k1gFnSc4	zem
s	s	k7c7	s
pěstováním	pěstování	k1gNnSc7	pěstování
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
kokosových	kokosový	k2eAgInPc2d1	kokosový
ořechů	ořech	k1gInPc2	ořech
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
tropických	tropický	k2eAgFnPc2d1	tropická
plodin	plodina	k1gFnPc2	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
však	však	k9	však
i	i	k9	i
významná	významný	k2eAgNnPc4d1	významné
ložiska	ložisko	k1gNnPc4	ložisko
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
-	-	kIx~	-
především	především	k9	především
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
produkty	produkt	k1gInPc1	produkt
tvoří	tvořit	k5eAaImIp3nP	tvořit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
část	část	k1gFnSc4	část
exportu	export	k1gInSc2	export
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Východního	východní	k2eAgInSc2d1	východní
Timoru	Timor	k1gInSc2	Timor
je	být	k5eAaImIp3nS	být
Dili	Dile	k1gFnSc4	Dile
<g/>
.	.	kIx.	.
</s>
<s>
Úředními	úřední	k2eAgInPc7d1	úřední
jazyky	jazyk	k1gInPc7	jazyk
jsou	být	k5eAaImIp3nP	být
tetum	tetum	k1gInSc4	tetum
(	(	kIx(	(
<g/>
malajsko-polynéský	malajskoolynéský	k2eAgInSc4d1	malajsko-polynéský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c4	mnoho
prvků	prvek	k1gInPc2	prvek
z	z	k7c2	z
indonéštiny	indonéština	k1gFnSc2	indonéština
a	a	k8xC	a
portugalštiny	portugalština	k1gFnSc2	portugalština
<g/>
)	)	kIx)	)
a	a	k8xC	a
portugalština	portugalština	k1gFnSc1	portugalština
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
věřících	věřící	k1gMnPc2	věřící
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
ke	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
je	být	k5eAaImIp3nS	být
rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
stát	stát	k1gInSc1	stát
s	s	k7c7	s
významnými	významný	k2eAgNnPc7d1	významné
ložisky	ložisko	k1gNnPc7	ložisko
ropy	ropa	k1gFnSc2	ropa
(	(	kIx(	(
<g/>
těžba	těžba	k1gFnSc1	těžba
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
20	[number]	k4	20
<g/>
.	.	kIx.	.
místa	místo	k1gNnPc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
maniok	maniok	k1gInSc1	maniok
<g/>
,	,	kIx,	,
batáty	batáty	k1gInPc1	batáty
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
<g/>
,	,	kIx,	,
kokosová	kokosový	k2eAgFnSc1d1	kokosová
palma	palma	k1gFnSc1	palma
a	a	k8xC	a
kaučukovník	kaučukovník	k1gInSc1	kaučukovník
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
vzácného	vzácný	k2eAgInSc2d1	vzácný
santalu	santal	k1gMnSc3	santal
<g/>
.	.	kIx.	.
</s>
<s>
Chovají	chovat	k5eAaImIp3nP	chovat
se	se	k3xPyFc4	se
buvoli	buvol	k1gMnPc1	buvol
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
kozy	koza	k1gFnPc1	koza
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnPc1	ovce
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc1	prase
a	a	k8xC	a
koně	kůň	k1gMnPc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
je	být	k5eAaImIp3nS	být
rybolov	rybolov	k1gInSc1	rybolov
i	i	k8xC	i
řemeslná	řemeslný	k2eAgFnSc1d1	řemeslná
výroba	výroba	k1gFnSc1	výroba
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
tkání	tkáň	k1gFnSc7	tkáň
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
košíkářství	košíkářství	k1gNnPc2	košíkářství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Východním	východní	k2eAgInSc6d1	východní
Timoru	Timor	k1gInSc6	Timor
existuje	existovat	k5eAaImIp3nS	existovat
kolem	kolem	k7c2	kolem
30	[number]	k4	30
dialektů	dialekt	k1gInPc2	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
na	na	k7c6	na
tak	tak	k6eAd1	tak
malém	malý	k2eAgNnSc6d1	malé
území	území	k1gNnSc6	území
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
hornatým	hornatý	k2eAgInSc7d1	hornatý
terénem	terén	k1gInSc7	terén
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
znesnadňoval	znesnadňovat	k5eAaImAgInS	znesnadňovat
mísení	mísení	k1gNnSc4	mísení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Číst	číst	k5eAaImF	číst
a	a	k8xC	a
psát	psát	k5eAaImF	psát
umí	umět	k5eAaImIp3nS	umět
přibližně	přibližně	k6eAd1	přibližně
polovina	polovina	k1gFnSc1	polovina
populace	populace	k1gFnSc1	populace
<g/>
,	,	kIx,	,
negramotnost	negramotnost	k1gFnSc1	negramotnost
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
60,7	[number]	k4	60,7
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nedostupnost	nedostupnost	k1gFnSc4	nedostupnost
nezávadné	závadný	k2eNgFnSc2d1	nezávadná
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Timor	Timor	k1gInSc1	Timor
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
ostrovní	ostrovní	k2eAgFnSc6d1	ostrovní
části	část	k1gFnSc6	část
Jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
400	[number]	k4	400
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
severního	severní	k2eAgNnSc2d1	severní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c4	mezi
8	[number]	k4	8
<g/>
°	°	k?	°
a	a	k8xC	a
10	[number]	k4	10
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
124	[number]	k4	124
<g/>
°	°	k?	°
a	a	k8xC	a
128	[number]	k4	128
°	°	k?	°
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Malých	Malých	k2eAgInPc6d1	Malých
Sundách	Sundách	k?	Sundách
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
ostrova	ostrov	k1gInSc2	ostrov
jsou	být	k5eAaImIp3nP	být
Ombaiův	Ombaiův	k2eAgInSc4d1	Ombaiův
průliv	průliv	k1gInSc4	průliv
<g/>
,	,	kIx,	,
Wetarské	Wetarský	k2eAgFnPc4d1	Wetarský
úžiny	úžina	k1gFnPc4	úžina
a	a	k8xC	a
větší	veliký	k2eAgNnSc4d2	veliký
Bandské	Bandský	k2eAgNnSc4d1	Bandský
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Timorské	Timorský	k2eAgNnSc1d1	Timorské
moře	moře	k1gNnSc1	moře
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
ostrov	ostrov	k1gInSc4	ostrov
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
od	od	k7c2	od
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
státu	stát	k1gInSc2	stát
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
hornatý	hornatý	k2eAgInSc1d1	hornatý
<g/>
,	,	kIx,	,
střed	střed	k1gInSc1	střed
země	zem	k1gFnSc2	zem
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
hornatina	hornatina	k1gFnSc1	hornatina
<g/>
,	,	kIx,	,
přesahující	přesahující	k2eAgFnSc1d1	přesahující
2000	[number]	k4	2000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Nejvýše	vysoce	k6eAd3	vysoce
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
masiv	masiv	k1gInSc1	masiv
Ramelau	Ramelaus	k1gInSc2	Ramelaus
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
ostrova	ostrov	k1gInSc2	ostrov
Tatamailau	Tatamailaus	k1gInSc2	Tatamailaus
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
2963	[number]	k4	2963
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
Řeky	Řek	k1gMnPc4	Řek
jako	jako	k8xS	jako
je	on	k3xPp3gMnPc4	on
Lacio	Lacio	k6eAd1	Lacio
jsou	být	k5eAaImIp3nP	být
krátké	krátká	k1gFnPc1	krátká
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
vodné	vodné	k1gNnSc1	vodné
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
teploty	teplota	k1gFnPc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
nížině	nížina	k1gFnSc6	nížina
mezi	mezi	k7c7	mezi
26	[number]	k4	26
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
27	[number]	k4	27
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
500	[number]	k4	500
až	až	k9	až
1500	[number]	k4	1500
mm	mm	kA	mm
<g/>
,	,	kIx,	,
na	na	k7c6	na
návětrných	návětrný	k2eAgInPc6d1	návětrný
svazích	svah	k1gInPc6	svah
hor	hora	k1gFnPc2	hora
vyšší	vysoký	k2eAgFnPc4d2	vyšší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
2000	[number]	k4	2000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Převažuje	převažovat	k5eAaImIp3nS	převažovat
zde	zde	k6eAd1	zde
přirozeně	přirozeně	k6eAd1	přirozeně
vlhké	vlhký	k2eAgNnSc4d1	vlhké
<g/>
,	,	kIx,	,
tropické	tropický	k2eAgNnSc4d1	tropické
podnebí	podnebí	k1gNnSc4	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
suché	suchý	k2eAgNnSc1d1	suché
a	a	k8xC	a
deštivé	deštivý	k2eAgNnSc1d1	deštivé
období	období	k1gNnSc1	období
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
vypalování	vypalování	k1gNnSc2	vypalování
lesů	les	k1gInPc2	les
zde	zde	k6eAd1	zde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
odlesňování	odlesňování	k1gNnSc3	odlesňování
a	a	k8xC	a
erozi	eroze	k1gFnSc3	eroze
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejvýchodnější	východní	k2eAgFnSc6d3	nejvýchodnější
oblasti	oblast	k1gFnSc6	oblast
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
pohoří	pohoří	k1gNnSc1	pohoří
Paitchau	Paitchaus	k1gInSc2	Paitchaus
a	a	k8xC	a
jezero	jezero	k1gNnSc1	jezero
Ira	Ir	k1gMnSc2	Ir
Lalaro	Lalara	k1gFnSc5	Lalara
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Nino	Nina	k1gFnSc5	Nina
Konis	Konis	k1gFnPc7	Konis
Santana	Santan	k1gMnSc2	Santan
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc2	první
místní	místní	k2eAgFnSc2d1	místní
chráněné	chráněný	k2eAgFnSc2d1	chráněná
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
národní	národní	k2eAgInSc1d1	národní
pak	pak	k8xC	pak
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
poslední	poslední	k2eAgFnSc2d1	poslední
zbývající	zbývající	k2eAgFnSc2d1	zbývající
tropické	tropický	k2eAgFnSc2d1	tropická
suché	suchý	k2eAgFnSc2d1	suchá
zalesněné	zalesněný	k2eAgFnSc2d1	zalesněná
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hostí	hostit	k5eAaImIp3nS	hostit
řadu	řada	k1gFnSc4	řada
jedinečných	jedinečný	k2eAgInPc2d1	jedinečný
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
a	a	k8xC	a
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
řídce	řídce	k6eAd1	řídce
obydlená	obydlený	k2eAgFnSc1d1	obydlená
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
leží	ležet	k5eAaImIp3nS	ležet
řada	řada	k1gFnSc1	řada
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
korálových	korálový	k2eAgInPc2d1	korálový
útesů	útes	k1gInPc2	útes
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Timor	Timor	k1gInSc1	Timor
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
obydlen	obydlet	k5eAaPmNgInS	obydlet
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pravěkých	pravěký	k2eAgFnPc2d1	pravěká
migrací	migrace	k1gFnPc2	migrace
v	v	k7c6	v
australasijského	australasijský	k2eAgInSc2d1	australasijský
regionu	region	k1gInSc2	region
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Nové	Nové	k2eAgNnSc1d1	Nové
Guinei	Guinei	k1gNnSc1	Guinei
před	před	k7c7	před
40	[number]	k4	40
000	[number]	k4	000
roky	rok	k1gInPc7	rok
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
3000	[number]	k4	3000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
na	na	k7c4	na
Timor	Timor	k1gInSc4	Timor
migrovaly	migrovat	k5eAaImAgInP	migrovat
austronéské	austronéský	k2eAgInPc1d1	austronéský
kmeny	kmen	k1gInPc1	kmen
a	a	k8xC	a
rozvinuly	rozvinout	k5eAaPmAgFnP	rozvinout
zde	zde	k6eAd1	zde
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
prakticky	prakticky	k6eAd1	prakticky
až	až	k9	až
do	do	k7c2	do
nedávné	dávný	k2eNgFnSc2d1	nedávná
minulosti	minulost	k1gFnSc2	minulost
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
tamějším	tamější	k2eAgMnPc3d1	tamější
obyvatelům	obyvatel	k1gMnPc3	obyvatel
soběstačnost	soběstačnost	k1gFnSc4	soběstačnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
Timor	Timor	k1gInSc1	Timor
integroval	integrovat	k5eAaBmAgInS	integrovat
migraci	migrace	k1gFnSc4	migrace
Malajců	Malajec	k1gMnPc2	Malajec
z	z	k7c2	z
Indočíny	Indočína	k1gFnSc2	Indočína
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ostrov	ostrov	k1gInSc1	ostrov
stal	stát	k5eAaPmAgInS	stát
zastávkou	zastávka	k1gFnSc7	zastávka
čínských	čínský	k2eAgMnPc2d1	čínský
a	a	k8xC	a
indických	indický	k2eAgMnPc2d1	indický
obchodníků	obchodník	k1gMnPc2	obchodník
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
,	,	kIx,	,
santalovým	santalový	k2eAgNnSc7d1	santalové
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
,	,	kIx,	,
medem	med	k1gInSc7	med
a	a	k8xC	a
voskem	vosk	k1gInSc7	vosk
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
písemný	písemný	k2eAgInSc1d1	písemný
záznam	záznam	k1gInSc1	záznam
o	o	k7c6	o
Timoru	Timor	k1gInSc6	Timor
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c4	o
součásti	součást	k1gFnPc4	součást
jávské	jávský	k2eAgFnSc2d1	jávská
říše	říš	k1gFnSc2	říš
Majapahit	Majapahita	k1gFnPc2	Majapahita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jej	on	k3xPp3gMnSc4	on
objevili	objevit	k5eAaPmAgMnP	objevit
i	i	k9	i
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
;	;	kIx,	;
tehdy	tehdy	k6eAd1	tehdy
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
objevili	objevit	k5eAaPmAgMnP	objevit
mnoho	mnoho	k4c4	mnoho
oblastních	oblastní	k2eAgNnPc2d1	oblastní
panství	panství	k1gNnPc2	panství
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
největší	veliký	k2eAgFnSc7d3	veliký
bylo	být	k5eAaImAgNnS	být
Wehali	Wehali	k1gMnPc4	Wehali
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Laran	Larany	k1gInPc2	Larany
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
portugalskou	portugalský	k2eAgFnSc7d1	portugalská
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
éry	éra	k1gFnSc2	éra
španělských	španělský	k2eAgMnPc2d1	španělský
Habsburků	Habsburk	k1gMnPc2	Habsburk
na	na	k7c6	na
portugalském	portugalský	k2eAgInSc6d1	portugalský
trůnu	trůn	k1gInSc6	trůn
(	(	kIx(	(
<g/>
1580	[number]	k4	1580
<g/>
-	-	kIx~	-
<g/>
1640	[number]	k4	1640
<g/>
)	)	kIx)	)
Timor	Timor	k1gInSc1	Timor
padl	padnout	k5eAaImAgInS	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Nizozemců	Nizozemec	k1gMnPc2	Nizozemec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
získali	získat	k5eAaPmAgMnP	získat
jistý	jistý	k2eAgInSc4d1	jistý
vliv	vliv	k1gInSc4	vliv
právě	právě	k6eAd1	právě
nad	nad	k7c7	nad
východní	východní	k2eAgFnSc7d1	východní
částí	část	k1gFnSc7	část
tohoto	tento	k3xDgInSc2	tento
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
dostal	dostat	k5eAaPmAgMnS	dostat
zpět	zpět	k6eAd1	zpět
pod	pod	k7c4	pod
portugalskou	portugalský	k2eAgFnSc4d1	portugalská
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
západní	západní	k2eAgFnSc7d1	západní
(	(	kIx(	(
<g/>
indonéskou	indonéský	k2eAgFnSc7d1	Indonéská
<g/>
)	)	kIx)	)
a	a	k8xC	a
východní	východní	k2eAgFnSc7d1	východní
(	(	kIx(	(
<g/>
portugalskou	portugalský	k2eAgFnSc7d1	portugalská
<g/>
)	)	kIx)	)
oblastí	oblast	k1gFnSc7	oblast
ostrova	ostrov	k1gInSc2	ostrov
byla	být	k5eAaImAgFnS	být
přesně	přesně	k6eAd1	přesně
určena	určit	k5eAaPmNgFnS	určit
až	až	k9	až
za	za	k7c2	za
I.	I.	kA	I.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
celého	celý	k2eAgNnSc2d1	celé
období	období	k1gNnSc2	období
kolonizace	kolonizace	k1gFnSc1	kolonizace
zůstával	zůstávat	k5eAaImAgInS	zůstávat
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
relativně	relativně	k6eAd1	relativně
nerozvinutý	rozvinutý	k2eNgInSc1d1	nerozvinutý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
obyvatelé	obyvatel	k1gMnPc1	obyvatel
vázáni	vázat	k5eAaImNgMnP	vázat
k	k	k7c3	k
zemědělství	zemědělství	k1gNnSc3	zemědělství
a	a	k8xC	a
skromnému	skromný	k2eAgInSc3d1	skromný
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
založeném	založený	k2eAgInSc6d1	založený
na	na	k7c6	na
tradicích	tradice	k1gFnPc6	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
jednoduchých	jednoduchý	k2eAgNnPc6d1	jednoduché
obydlích	obydlí	k1gNnPc6	obydlí
v	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
<g/>
,	,	kIx,	,
oddělených	oddělený	k2eAgFnPc6d1	oddělená
vesnických	vesnický	k2eAgFnPc6d1	vesnická
komunitách	komunita	k1gFnPc6	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
jej	on	k3xPp3gMnSc4	on
od	od	k7c2	od
února	únor	k1gInSc2	únor
1942	[number]	k4	1942
do	do	k7c2	do
února	únor	k1gInSc2	únor
1943	[number]	k4	1943
okupovala	okupovat	k5eAaBmAgNnP	okupovat
japonská	japonský	k2eAgNnPc1d1	Japonské
vojska	vojsko	k1gNnPc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Odehrála	odehrát	k5eAaPmAgFnS	odehrát
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Timor	Timor	k1gInSc4	Timor
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
spojenců	spojenec	k1gMnPc2	spojenec
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
především	především	k6eAd1	především
australské	australský	k2eAgFnPc1d1	australská
a	a	k8xC	a
britské	britský	k2eAgFnPc1d1	britská
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
začal	začít	k5eAaPmAgMnS	začít
na	na	k7c6	na
Východním	východní	k2eAgInSc6d1	východní
Timoru	Timor	k1gInSc6	Timor
proces	proces	k1gInSc1	proces
dekolonizace	dekolonizace	k1gFnSc2	dekolonizace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
konci	konec	k1gInSc6	konec
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1975	[number]	k4	1975
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Východní	východní	k2eAgMnSc1d1	východní
Timor	Timor	k1gMnSc1	Timor
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
(	(	kIx(	(
<g/>
2	[number]	k4	2
dny	den	k1gInPc4	den
po	po	k7c6	po
návštěvě	návštěva	k1gFnSc6	návštěva
prezidenta	prezident	k1gMnSc2	prezident
Forda	ford	k1gMnSc2	ford
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
Indonésií	Indonésie	k1gFnSc7	Indonésie
provedena	proveden	k2eAgFnSc1d1	provedena
mohutná	mohutný	k2eAgFnSc1d1	mohutná
invaze	invaze	k1gFnSc1	invaze
na	na	k7c4	na
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
ostrova	ostrov	k1gInSc2	ostrov
(	(	kIx(	(
<g/>
s	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
záběrem	záběr	k1gInSc7	záběr
na	na	k7c4	na
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Dili	Dil	k1gFnSc2	Dil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Indonésii	Indonésie	k1gFnSc4	Indonésie
sice	sice	k8xC	sice
patří	patřit	k5eAaImIp3nS	patřit
západní	západní	k2eAgFnSc1d1	západní
polovina	polovina	k1gFnSc1	polovina
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
ale	ale	k9	ale
sama	sám	k3xTgFnSc1	sám
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
území	území	k1gNnSc6	území
Východního	východní	k2eAgInSc2d1	východní
Timoru	Timor	k1gInSc2	Timor
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
historický	historický	k2eAgInSc4d1	historický
nárok	nárok	k1gInSc4	nárok
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Rozbuškou	rozbuška	k1gFnSc7	rozbuška
<g/>
"	"	kIx"	"
konfliktu	konflikt	k1gInSc2	konflikt
zřejmě	zřejmě	k6eAd1	zřejmě
byla	být	k5eAaImAgFnS	být
tamější	tamější	k2eAgFnSc1d1	tamější
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
FRETLIN	FRETLIN	kA	FRETLIN
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
těšila	těšit	k5eAaImAgFnS	těšit
slovní	slovní	k2eAgFnSc1d1	slovní
podpoře	podpora	k1gFnSc3	podpora
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
komunistické	komunistický	k2eAgFnSc2d1	komunistická
garnitury	garnitura	k1gFnSc2	garnitura
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
pramenily	pramenit	k5eAaImAgFnP	pramenit
obavy	obava	k1gFnPc1	obava
Indonésie	Indonésie	k1gFnSc2	Indonésie
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
na	na	k7c4	na
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc4	Austrálie
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
země	zem	k1gFnPc4	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
oblast	oblast	k1gFnSc1	oblast
mohla	moct	k5eAaImAgFnS	moct
spadnout	spadnout	k5eAaPmF	spadnout
do	do	k7c2	do
komunistické	komunistický	k2eAgFnSc2d1	komunistická
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
Indonésie	Indonésie	k1gFnPc4	Indonésie
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
poli	pole	k1gNnSc6	pole
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc4	stát
omlouvána	omlouván	k2eAgFnSc1d1	omlouvána
a	a	k8xC	a
veta	veta	k6eAd1	veta
Washingtonu	Washington	k1gInSc2	Washington
otupovala	otupovat	k5eAaImAgFnS	otupovat
resoluce	resoluce	k1gFnSc1	resoluce
OSN	OSN	kA	OSN
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
partyzánská	partyzánský	k2eAgFnSc1d1	Partyzánská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
při	pře	k1gFnSc4	pře
níž	jenž	k3xRgFnSc2	jenž
indonéské	indonéský	k2eAgFnSc2d1	Indonéská
síly	síla	k1gFnSc2	síla
postupovaly	postupovat	k5eAaImAgFnP	postupovat
i	i	k9	i
proti	proti	k7c3	proti
civilistům	civilista	k1gMnPc3	civilista
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
krutostí	krutost	k1gFnSc7	krutost
-	-	kIx~	-
z	z	k7c2	z
populace	populace	k1gFnSc2	populace
700	[number]	k4	700
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
nejméně	málo	k6eAd3	málo
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
civilní	civilní	k2eAgNnSc1d1	civilní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
nenašlo	najít	k5eNaPmAgNnS	najít
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
konfliktu	konflikt	k1gInSc6	konflikt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
při	při	k7c6	při
následném	následný	k2eAgInSc6d1	následný
poválečném	poválečný	k2eAgInSc6d1	poválečný
chaosu	chaos	k1gInSc6	chaos
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
-	-	kIx~	-
podle	podle	k7c2	podle
detailní	detailní	k2eAgFnSc2d1	detailní
statistiky	statistika	k1gFnSc2	statistika
Komise	komise	k1gFnSc2	komise
za	za	k7c4	za
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
uznání	uznání	k1gNnSc4	uznání
a	a	k8xC	a
umírnění	umírnění	k1gNnSc4	umírnění
situace	situace	k1gFnSc2	situace
na	na	k7c6	na
Východním	východní	k2eAgInSc6d1	východní
Timoru	Timor	k1gInSc6	Timor
je	být	k5eAaImIp3nS	být
minimálně	minimálně	k6eAd1	minimálně
102	[number]	k4	102
800	[number]	k4	800
případů	případ	k1gInPc2	případ
přímo	přímo	k6eAd1	přímo
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
indonéskou	indonéský	k2eAgFnSc7d1	Indonéská
invazí	invaze	k1gFnSc7	invaze
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
84	[number]	k4	84
200	[number]	k4	200
následkem	následkem	k7c2	následkem
hladu	hlad	k1gInSc2	hlad
a	a	k8xC	a
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Političtí	politický	k2eAgMnPc1d1	politický
zástupci	zástupce	k1gMnPc1	zástupce
Východního	východní	k2eAgInSc2d1	východní
Timoru	Timor	k1gInSc2	Timor
<g/>
,	,	kIx,	,
např.	např.	kA	např.
José	José	k1gNnSc1	José
Ramos-Horta	Ramos-Horta	k1gFnSc1	Ramos-Horta
<g/>
,	,	kIx,	,
u	u	k7c2	u
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
požadovali	požadovat	k5eAaImAgMnP	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
konfliktem	konflikt	k1gInSc7	konflikt
více	hodně	k6eAd2	hodně
zabývala	zabývat	k5eAaImAgFnS	zabývat
a	a	k8xC	a
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
urovnání	urovnání	k1gNnSc3	urovnání
této	tento	k3xDgFnSc2	tento
kritické	kritický	k2eAgFnSc2d1	kritická
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
ani	ani	k8xC	ani
žádné	žádný	k3yNgFnPc4	žádný
významné	významný	k2eAgFnPc4d1	významná
rezoluce	rezoluce	k1gFnPc4	rezoluce
<g/>
,	,	kIx,	,
a	a	k8xC	a
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
ani	ani	k8xC	ani
povědomí	povědomí	k1gNnSc2	povědomí
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
o	o	k7c6	o
probíhající	probíhající	k2eAgFnSc6d1	probíhající
katastrofě	katastrofa	k1gFnSc6	katastrofa
Timořanů	Timořan	k1gMnPc2	Timořan
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc2	první
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
mise	mise	k1gFnSc2	mise
na	na	k7c4	na
postižené	postižený	k1gMnPc4	postižený
území	území	k1gNnSc2	území
dorazily	dorazit	k5eAaPmAgInP	dorazit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
invaze	invaze	k1gFnSc2	invaze
naopak	naopak	k6eAd1	naopak
Indonésie	Indonésie	k1gFnSc1	Indonésie
nadále	nadále	k6eAd1	nadále
přijímala	přijímat	k5eAaImAgFnS	přijímat
dodávky	dodávka	k1gFnPc4	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
mířící	mířící	k2eAgInPc4d1	mířící
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
ze	z	k7c2	z
Západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
zesílení	zesílení	k1gNnSc4	zesílení
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
tlaku	tlak	k1gInSc2	tlak
(	(	kIx(	(
<g/>
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
omezoval	omezovat	k5eAaImAgMnS	omezovat
na	na	k7c4	na
formální	formální	k2eAgInPc4d1	formální
protesty	protest	k1gInPc4	protest
s	s	k7c7	s
předchozím	předchozí	k2eAgNnSc7d1	předchozí
tichým	tichý	k2eAgNnSc7d1	tiché
schválením	schválení	k1gNnSc7	schválení
invaze	invaze	k1gFnSc2	invaze
<g/>
)	)	kIx)	)
a	a	k8xC	a
politické	politický	k2eAgFnPc1d1	politická
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
způsobily	způsobit	k5eAaPmAgFnP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
indonéská	indonéský	k2eAgNnPc1d1	indonéské
vojska	vojsko	k1gNnPc1	vojsko
stáhla	stáhnout	k5eAaPmAgNnP	stáhnout
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2002	[number]	k4	2002
se	se	k3xPyFc4	se
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
stal	stát	k5eAaPmAgInS	stát
nezávislou	závislý	k2eNgFnSc7d1	nezávislá
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Xanana	Xanan	k1gMnSc4	Xanan
Gusmao	Gusmao	k6eAd1	Gusmao
<g/>
,	,	kIx,	,
respektovaný	respektovaný	k2eAgMnSc1d1	respektovaný
bojovník	bojovník	k1gMnSc1	bojovník
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1999	[number]	k4	1999
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
Indonésie	Indonésie	k1gFnPc1	Indonésie
a	a	k8xC	a
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
dohodu	dohoda	k1gFnSc4	dohoda
zprostředkovanou	zprostředkovaný	k2eAgFnSc7d1	zprostředkovaná
Organizací	organizace	k1gFnSc7	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obyvatelům	obyvatel	k1gMnPc3	obyvatel
východního	východní	k2eAgInSc2d1	východní
Timoru	Timor	k1gInSc2	Timor
umožnila	umožnit	k5eAaPmAgFnS	umožnit
hlasovat	hlasovat	k5eAaImF	hlasovat
o	o	k7c6	o
případné	případný	k2eAgFnSc6d1	případná
nezávislosti	nezávislost	k1gFnSc6	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
organizovaném	organizovaný	k2eAgInSc6d1	organizovaný
OSN	OSN	kA	OSN
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1999	[number]	k4	1999
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
pro	pro	k7c4	pro
nezávislost	nezávislost	k1gFnSc4	nezávislost
Východního	východní	k2eAgInSc2d1	východní
Timoru	Timor	k1gInSc2	Timor
na	na	k7c6	na
Indonésii	Indonésie	k1gFnSc6	Indonésie
78,5	[number]	k4	78,5
%	%	kIx~	%
účastníků	účastník	k1gMnPc2	účastník
referenda	referendum	k1gNnSc2	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
nepokojů	nepokoj	k1gInPc2	nepokoj
a	a	k8xC	a
násilností	násilnost	k1gFnPc2	násilnost
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
ozbrojenců	ozbrojenec	k1gMnPc2	ozbrojenec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
odmítali	odmítat	k5eAaImAgMnP	odmítat
nezávislost	nezávislost	k1gFnSc4	nezávislost
Východního	východní	k2eAgInSc2d1	východní
Timoru	Timor	k1gInSc2	Timor
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
1000	[number]	k4	1000
-	-	kIx~	-
2000	[number]	k4	2000
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Indonéské	indonéský	k2eAgFnPc1d1	Indonéská
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
tlakem	tlak	k1gInSc7	tlak
stáhly	stáhnout	k5eAaPmAgFnP	stáhnout
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
ale	ale	k8xC	ale
samy	sám	k3xTgFnPc1	sám
nebo	nebo	k8xC	nebo
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
timorských	timorský	k2eAgFnPc2d1	timorský
milicí	milice	k1gFnPc2	milice
zničily	zničit	k5eAaPmAgFnP	zničit
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
mezinárodním	mezinárodní	k2eAgFnPc3d1	mezinárodní
misím	mise	k1gFnPc3	mise
a	a	k8xC	a
misím	mise	k1gFnPc3	mise
OSN	OSN	kA	OSN
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
postupně	postupně	k6eAd1	postupně
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stažení	stažení	k1gNnSc6	stažení
mírových	mírový	k2eAgFnPc2d1	mírová
jednotek	jednotka	k1gFnPc2	jednotka
OSN	OSN	kA	OSN
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
však	však	k8xC	však
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2006	[number]	k4	2006
propukla	propuknout	k5eAaPmAgFnS	propuknout
další	další	k2eAgFnSc1d1	další
vlna	vlna	k1gFnSc1	vlna
nepokojů	nepokoj	k1gInPc2	nepokoj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
takové	takový	k3xDgFnPc4	takový
míry	míra	k1gFnPc4	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
evakuovat	evakuovat	k5eAaBmF	evakuovat
cizí	cizí	k2eAgMnPc4d1	cizí
státní	státní	k2eAgMnPc4d1	státní
příslušníky	příslušník	k1gMnPc4	příslušník
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
OSN	OSN	kA	OSN
byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
následně	následně	k6eAd1	následně
opět	opět	k6eAd1	opět
posílena	posílit	k5eAaPmNgFnS	posílit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
akce	akce	k1gFnSc1	akce
ozbrojenců	ozbrojenec	k1gMnPc2	ozbrojenec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
proti	proti	k7c3	proti
nezávislosti	nezávislost	k1gFnSc3	nezávislost
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
povstalci	povstalec	k1gMnPc7	povstalec
těžce	těžce	k6eAd1	těžce
postřelili	postřelit	k5eAaPmAgMnP	postřelit
prezidenta	prezident	k1gMnSc4	prezident
José	Josá	k1gFnSc2	Josá
Ramos-Hortu	Ramos-Hort	k1gInSc2	Ramos-Hort
<g/>
,	,	kIx,	,
držitele	držitel	k1gMnSc4	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
kritickém	kritický	k2eAgInSc6d1	kritický
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
útok	útok	k1gInSc4	útok
přežil	přežít	k5eAaPmAgMnS	přežít
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Východním	východní	k2eAgInSc6d1	východní
Timoru	Timor	k1gInSc6	Timor
se	se	k3xPyFc4	se
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
pět	pět	k4xCc1	pět
různých	různý	k2eAgFnPc2d1	různá
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
misí	mise	k1gFnPc2	mise
<g/>
:	:	kIx,	:
UNAMET	UNAMET	kA	UNAMET
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
1999	[number]	k4	1999
až	až	k9	až
říjen	říjen	k1gInSc1	říjen
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
organizování	organizování	k1gNnSc6	organizování
referenda	referendum	k1gNnSc2	referendum
<g/>
)	)	kIx)	)
INTERFET	INTERFET	kA	INTERFET
(	(	kIx(	(
<g/>
září	září	k1gNnSc4	září
1999	[number]	k4	1999
až	až	k9	až
únor	únor	k1gInSc1	únor
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
obnova	obnova	k1gFnSc1	obnova
míru	mír	k1gInSc2	mír
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
)	)	kIx)	)
UNTAET	UNTAET	kA	UNTAET
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
1999	[number]	k4	1999
až	až	k9	až
květen	květen	k1gInSc1	květen
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
správa	správa	k1gFnSc1	správa
území	území	k1gNnSc1	území
a	a	k8xC	a
příprava	příprava	k1gFnSc1	příprava
na	na	k7c4	na
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
)	)	kIx)	)
UNMISET	UNMISET	kA	UNMISET
(	(	kIx(	(
<g/>
květen	květen	k1gInSc4	květen
2002	[number]	k4	2002
až	až	k9	až
květen	květen	k1gInSc1	květen
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
poskytování	poskytování	k1gNnSc4	poskytování
pomoci	pomoc	k1gFnSc2	pomoc
během	během	k7c2	během
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
správy	správa	k1gFnSc2	správa
Východního	východní	k2eAgInSc2d1	východní
Timoru	Timor	k1gInSc2	Timor
<g/>
)	)	kIx)	)
UNOTIL	UNOTIL	kA	UNOTIL
(	(	kIx(	(
<g/>
od	od	k7c2	od
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
politická	politický	k2eAgFnSc1d1	politická
mise	mise	k1gFnSc1	mise
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
mírně	mírně	k6eAd1	mírně
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
<g/>
:	:	kIx,	:
hlavním	hlavní	k2eAgInSc7d1	hlavní
směrem	směr	k1gInSc7	směr
premiéra	premiér	k1gMnSc2	premiér
Xanana	Xanan	k1gMnSc2	Xanan
Gusmay	Gusmaa	k1gMnSc2	Gusmaa
je	být	k5eAaImIp3nS	být
Strategický	strategický	k2eAgInSc1d1	strategický
plán	plán	k1gInSc1	plán
rozvoje	rozvoj	k1gInSc2	rozvoj
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
let	léto	k1gNnPc2	léto
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
2030	[number]	k4	2030
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
využití	využití	k1gNnSc4	využití
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
ropy	ropa	k1gFnSc2	ropa
(	(	kIx(	(
<g/>
Petroleum	Petroleum	k1gInSc1	Petroleum
Fund	fund	k1gInSc1	fund
<g/>
)	)	kIx)	)
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
a	a	k8xC	a
veřejných	veřejný	k2eAgFnPc2d1	veřejná
služeb	služba	k1gFnPc2	služba
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Indonésií	Indonésie	k1gFnSc7	Indonésie
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
zlepšily	zlepšit	k5eAaPmAgFnP	zlepšit
<g/>
;	;	kIx,	;
problém	problém	k1gInSc1	problém
však	však	k9	však
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
s	s	k7c7	s
vymezením	vymezení	k1gNnSc7	vymezení
přesné	přesný	k2eAgFnSc2d1	přesná
hranice	hranice	k1gFnSc2	hranice
obou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
5	[number]	k4	5
000	[number]	k4	000
lokalit	lokalita	k1gFnPc2	lokalita
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
jen	jen	k9	jen
900	[number]	k4	900
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
od	od	k7c2	od
10	[number]	k4	10
let	léto	k1gNnPc2	léto
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
členem	člen	k1gInSc7	člen
Sdružení	sdružení	k1gNnSc1	sdružení
národů	národ	k1gInPc2	národ
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
ASEAN	ASEAN	kA	ASEAN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Členské	členský	k2eAgInPc4d1	členský
státy	stát	k1gInPc4	stát
ASEAN	ASEAN	kA	ASEAN
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nepovažují	považovat	k5eNaImIp3nP	považovat
Východní	východní	k2eAgInSc4d1	východní
Timor	Timor	k1gInSc4	Timor
za	za	k7c4	za
plně	plně	k6eAd1	plně
suverénní	suverénní	k2eAgFnSc4d1	suverénní
zemi	zem	k1gFnSc4	zem
s	s	k7c7	s
plným	plný	k2eAgInSc7d1	plný
vlivem	vliv	k1gInSc7	vliv
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
fungující	fungující	k2eAgFnSc7d1	fungující
ekonomikou	ekonomika	k1gFnSc7	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
zatím	zatím	k6eAd1	zatím
pouze	pouze	k6eAd1	pouze
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
vláda	vláda	k1gFnSc1	vláda
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vypořádat	vypořádat	k5eAaPmF	vypořádat
se	s	k7c7	s
samosprávou	samospráva	k1gFnSc7	samospráva
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Dili	Dil	k1gFnSc2	Dil
nebyla	být	k5eNaImAgFnS	být
standardní	standardní	k2eAgFnSc1d1	standardní
<g/>
:	:	kIx,	:
docházelo	docházet	k5eAaImAgNnS	docházet
zde	zde	k6eAd1	zde
ke	k	k7c3	k
každodenním	každodenní	k2eAgFnPc3d1	každodenní
násilnostem	násilnost	k1gFnPc3	násilnost
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
vypalovány	vypalován	k2eAgInPc1d1	vypalován
domy	dům	k1gInPc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
tohoto	tento	k3xDgNnSc2	tento
násilí	násilí	k1gNnSc2	násilí
byla	být	k5eAaImAgFnS	být
páchána	páchat	k5eAaImNgFnS	páchat
pouličními	pouliční	k2eAgInPc7d1	pouliční
gangy	gang	k1gInPc7	gang
nebo	nebo	k8xC	nebo
členy	člen	k1gMnPc7	člen
náboženských	náboženský	k2eAgFnPc2d1	náboženská
sekt	sekta	k1gFnPc2	sekta
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
tyto	tento	k3xDgFnPc4	tento
akce	akce	k1gFnPc4	akce
sice	sice	k8xC	sice
přímo	přímo	k6eAd1	přímo
nepodporují	podporovat	k5eNaImIp3nP	podporovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
situace	situace	k1gFnSc2	situace
využívají	využívat	k5eAaPmIp3nP	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
všechny	všechen	k3xTgFnPc1	všechen
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
sily	sít	k5eAaImAgFnP	sít
odejdou	odejít	k5eAaPmIp3nP	odejít
z	z	k7c2	z
Východního	východní	k2eAgInSc2d1	východní
Timoru	Timor	k1gInSc2	Timor
a	a	k8xC	a
východotimorská	východotimorský	k2eAgFnSc1d1	východotimorský
vláda	vláda	k1gFnSc1	vláda
bude	být	k5eAaImBp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
zajistit	zajistit	k5eAaPmF	zajistit
vnější	vnější	k2eAgNnSc4d1	vnější
i	i	k8xC	i
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
i	i	k8xC	i
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
klid	klid	k1gInSc4	klid
a	a	k8xC	a
pořádek	pořádek	k1gInSc4	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2016	[number]	k4	2016
zde	zde	k6eAd1	zde
zasedá	zasedat	k5eAaImIp3nS	zasedat
ASEAN	ASEAN	kA	ASEAN
<g/>
.	.	kIx.	.
</s>
<s>
Tradičními	tradiční	k2eAgMnPc7d1	tradiční
spojenci	spojenec	k1gMnPc7	spojenec
Východního	východní	k2eAgInSc2d1	východní
Timoru	Timor	k1gInSc2	Timor
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
místních	místní	k2eAgFnPc2d1	místní
zemí	zem	k1gFnPc2	zem
Malajsie	Malajsie	k1gFnSc2	Malajsie
a	a	k8xC	a
Singapur	Singapur	k1gInSc4	Singapur
<g/>
.	.	kIx.	.
</s>
<s>
Dobré	dobrý	k2eAgInPc1d1	dobrý
vztahy	vztah	k1gInPc1	vztah
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
se	s	k7c7	s
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jí	on	k3xPp3gFnSc3	on
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
:	:	kIx,	:
především	především	k6eAd1	především
s	s	k7c7	s
Austrálií	Austrálie	k1gFnSc7	Austrálie
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
i	i	k8xC	i
s	s	k7c7	s
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
,	,	kIx,	,
Japonskem	Japonsko	k1gNnSc7	Japonsko
a	a	k8xC	a
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
nemá	mít	k5eNaImIp3nS	mít
ve	v	k7c6	v
Východním	východní	k2eAgInSc6d1	východní
Timoru	Timor	k1gInSc6	Timor
zastupitelský	zastupitelský	k2eAgInSc4d1	zastupitelský
úřad	úřad	k1gInSc4	úřad
ani	ani	k8xC	ani
jiné	jiný	k2eAgNnSc4d1	jiné
oficiální	oficiální	k2eAgNnSc4d1	oficiální
zastoupení	zastoupení	k1gNnSc4	zastoupení
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgMnPc4d1	český
zájmy	zájem	k1gInPc4	zájem
zde	zde	k6eAd1	zde
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
v	v	k7c6	v
Jakartě	Jakarta	k1gFnSc6	Jakarta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
zde	zde	k6eAd1	zde
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
zastoupení	zastoupení	k1gNnPc1	zastoupení
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
Carlos	Carlos	k1gMnSc1	Carlos
Rey	Rea	k1gFnSc2	Rea
Salgado	Salgada	k1gFnSc5	Salgada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
postavila	postavit	k5eAaPmAgFnS	postavit
pro	pro	k7c4	pro
své	své	k1gNnSc4	své
politické	politický	k2eAgFnSc2d1	politická
a	a	k8xC	a
obchodní	obchodní	k2eAgFnSc2d1	obchodní
aktivity	aktivita	k1gFnSc2	aktivita
Dům	dům	k1gInSc1	dům
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Východním	východní	k2eAgInSc6d1	východní
Timoru	Timor	k1gInSc6	Timor
1	[number]	k4	1
143	[number]	k4	143
667	[number]	k4	667
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
hustota	hustota	k1gFnSc1	hustota
osídlení	osídlení	k1gNnSc2	osídlení
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
okolo	okolo	k7c2	okolo
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
tohoto	tento	k3xDgInSc2	tento
státu	stát	k1gInSc2	stát
-	-	kIx~	-
Dili	Dil	k1gFnSc2	Dil
<g/>
.	.	kIx.	.
</s>
<s>
Gramotnost	gramotnost	k1gFnSc1	gramotnost
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
50	[number]	k4	50
%	%	kIx~	%
<g/>
;	;	kIx,	;
negramotnost	negramotnost	k1gFnSc1	negramotnost
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
60,7	[number]	k4	60,7
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
ve	v	k7c6	v
Východním	východní	k2eAgInSc6d1	východní
Timoru	Timor	k1gInSc6	Timor
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
50	[number]	k4	50
%	%	kIx~	%
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
a	a	k8xC	a
inflace	inflace	k1gFnSc1	inflace
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
1,9	[number]	k4	1,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnanost	zaměstnanost	k1gFnSc1	zaměstnanost
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
-	-	kIx~	-
10	[number]	k4	10
<g/>
%	%	kIx~	%
služby	služba	k1gFnPc1	služba
a	a	k8xC	a
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
90	[number]	k4	90
<g/>
%	%	kIx~	%
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
11	[number]	k4	11
nemocnic	nemocnice	k1gFnPc2	nemocnice
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
330	[number]	k4	330
zdravotnických	zdravotnický	k2eAgNnPc2d1	zdravotnické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgInPc1d1	vládní
výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c6	na
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
činily	činit	k5eAaImAgInP	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
150	[number]	k4	150
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
porodnosti	porodnost	k1gFnSc2	porodnost
je	být	k5eAaImIp3nS	být
šest	šest	k4xCc4	šest
porodů	porod	k1gInPc2	porod
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Náboženských	náboženský	k2eAgFnPc2d1	náboženská
skupin	skupina	k1gFnPc2	skupina
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naprosto	naprosto	k6eAd1	naprosto
s	s	k7c7	s
96,9	[number]	k4	96,9
%	%	kIx~	%
převažují	převažovat	k5eAaImIp3nP	převažovat
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Protestantů	protestant	k1gMnPc2	protestant
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
2,2	[number]	k4	2,2
%	%	kIx~	%
<g/>
,	,	kIx,	,
muslimů	muslim	k1gMnPc2	muslim
0,3	[number]	k4	0,3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
úřední	úřední	k2eAgInPc1d1	úřední
jazyky	jazyk	k1gInPc1	jazyk
-	-	kIx~	-
tetum	tetum	k1gInSc1	tetum
a	a	k8xC	a
portugalština	portugalština	k1gFnSc1	portugalština
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
lidí	člověk	k1gMnPc2	člověk
hovoří	hovořit	k5eAaImIp3nS	hovořit
právě	právě	k9	právě
jazykem	jazyk	k1gInSc7	jazyk
tetum	tetum	k1gInSc1	tetum
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
ostatními	ostatní	k2eAgInPc7d1	ostatní
domorodými	domorodý	k2eAgInPc7d1	domorodý
jazyky	jazyk	k1gInPc7	jazyk
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
mambai	mamba	k1gFnPc1	mamba
<g/>
,	,	kIx,	,
makasai	makasai	k1gNnSc1	makasai
<g/>
,	,	kIx,	,
tetum	tetum	k1gNnSc1	tetum
terik	terika	k1gFnPc2	terika
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
ostatní	ostatní	k2eAgInPc1d1	ostatní
jazyky	jazyk	k1gInPc1	jazyk
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
téměř	téměř	k6eAd1	téměř
40	[number]	k4	40
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
ve	v	k7c6	v
Východním	východní	k2eAgInSc6d1	východní
Timoru	Timor	k1gInSc6	Timor
mluví	mluvit	k5eAaImIp3nS	mluvit
30	[number]	k4	30
dialekty	dialekt	k1gInPc7	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
vysoce	vysoce	k6eAd1	vysoce
hornatým	hornatý	k2eAgInSc7d1	hornatý
terénem	terén	k1gInSc7	terén
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
znesnadňoval	znesnadňovat	k5eAaImAgInS	znesnadňovat
mísení	mísení	k1gNnSc4	mísení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
13	[number]	k4	13
administrativních	administrativní	k2eAgInPc2d1	administrativní
distriktů	distrikt	k1gInPc2	distrikt
<g/>
:	:	kIx,	:
Lautém	Lautém	k1gInSc4	Lautém
Baucau	Bauca	k2eAgFnSc4d1	Bauca
Viqueque	Viqueque	k1gFnSc4	Viqueque
Manatuto	Manatut	k2eAgNnSc1d1	Manatut
Dili	Dili	k1gNnSc1	Dili
Aileu	Aileus	k1gInSc2	Aileus
Manufahi	Manufah	k1gFnSc2	Manufah
Liquiçá	Liquiçá	k1gFnSc1	Liquiçá
Ermera	Ermera	k1gFnSc1	Ermera
Ainaro	Ainara	k1gFnSc5	Ainara
Bobonaro	Bobonara	k1gFnSc5	Bobonara
Cova	Covum	k1gNnPc4	Covum
Lima	limo	k1gNnSc2	limo
Oecusse	Oecusse	k1gFnSc2	Oecusse
Ambeno	Amben	k2eAgNnSc1d1	Amben
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Dili	Dil	k1gFnSc2	Dil
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
distriktu	distrikt	k1gInSc6	distrikt
Dili	Dil	k1gFnSc2	Dil
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
distrikty	distrikt	k1gInPc1	distrikt
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
65	[number]	k4	65
podokresů	podokres	k1gInPc2	podokres
(	(	kIx(	(
<g/>
subdistrikts	subdistriktsit	k5eAaPmRp2nS	subdistriktsit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
443	[number]	k4	443
sucos	sucosa	k1gFnPc2	sucosa
a	a	k8xC	a
2336	[number]	k4	2336
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Východním	východní	k2eAgInSc6d1	východní
Timoru	Timor	k1gInSc6	Timor
zpívá	zpívat	k5eAaImIp3nS	zpívat
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Shakira	Shakir	k1gInSc2	Shakir
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Timor	Timora	k1gFnPc2	Timora
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
albu	album	k1gNnSc6	album
Oral	orat	k5eAaImAgInS	orat
Fixation	Fixation	k1gInSc4	Fixation
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
