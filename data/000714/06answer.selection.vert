<s>
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
(	(	kIx(	(
<g/>
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
tetum	tetum	k1gNnSc1	tetum
Timór	Timór	k1gInSc1	Timór
Lorosa	Lorosa	k1gFnSc1	Lorosa
<g/>
'	'	kIx"	'
<g/>
e	e	k0	e
<g/>
,	,	kIx,	,
portugalsky	portugalsky	k6eAd1	portugalsky
Timor-Leste	Timor-Lest	k1gInSc5	Timor-Lest
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
stát	stát	k1gInSc1	stát
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ostrova	ostrov	k1gInSc2	ostrov
Timor	Timora	k1gFnPc2	Timora
v	v	k7c6	v
souostroví	souostroví	k1gNnSc6	souostroví
Malé	Malé	k2eAgInPc1d1	Malé
Sundy	sund	k1gInPc1	sund
<g/>
.	.	kIx.	.
</s>
