<s>
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
japonském	japonský	k2eAgNnSc6d1	Japonské
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
z	z	k7c2	z
kandidátských	kandidátský	k2eAgNnPc2d1	kandidátské
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgInPc3	který
patřily	patřit	k5eAaImAgInP	patřit
ještě	ještě	k6eAd1	ještě
Detroit	Detroit	k1gInSc4	Detroit
<g/>
,	,	kIx,	,
Buenos	Buenos	k1gInSc4	Buenos
Aires	Airesa	k1gFnPc2	Airesa
a	a	k8xC	a
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Tokio	Tokio	k1gNnSc1	Tokio
mělo	mít	k5eAaImAgNnS	mít
pořádat	pořádat	k5eAaImF	pořádat
již	již	k6eAd1	již
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
co	co	k3yInSc4	co
Japonsko	Japonsko	k1gNnSc1	Japonsko
napadlo	napadnout	k5eAaPmAgNnS	napadnout
Čínu	Čína	k1gFnSc4	Čína
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
pořadatelství	pořadatelství	k1gNnSc1	pořadatelství
přiřknuto	přiřknout	k5eAaPmNgNnS	přiřknout
finským	finský	k2eAgFnPc3d1	finská
Helsinkám	Helsinky	k1gFnPc3	Helsinky
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Československo	Československo	k1gNnSc4	Československo
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
1964	[number]	k4	1964
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
www.olympic.org	www.olympic.org	k1gInSc4	www.olympic.org
</s>
