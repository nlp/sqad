<s>
Departement	departement	k1gInSc1	departement
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
département	département	k1gInSc1	département
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
départ	départ	k1gInSc1	départ
<g/>
́	́	k?	́
<g/>
mañ	mañ	k?	mañ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
administrativního	administrativní	k2eAgNnSc2d1	administrativní
a	a	k8xC	a
samosprávného	samosprávný	k2eAgNnSc2d1	samosprávné
členění	členění	k1gNnSc2	členění
území	území	k1gNnSc2	území
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Nadřazenou	nadřazený	k2eAgFnSc7d1	nadřazená
jednotkou	jednotka	k1gFnSc7	jednotka
jsou	být	k5eAaImIp3nP	být
regiony	region	k1gInPc1	region
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
101	[number]	k4	101
departementů	departement	k1gInPc2	departement
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
5	[number]	k4	5
jsou	být	k5eAaImIp3nP	být
zámořské	zámořský	k2eAgInPc1d1	zámořský
(	(	kIx(	(
<g/>
Département	Département	k1gInSc1	Département
d	d	k?	d
<g/>
́	́	k?	́
<g/>
outre-mer	outreer	k1gInSc1	outre-mer
<g/>
,	,	kIx,	,
DOM	DOM	k?	DOM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
departement	departement	k1gInSc1	departement
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
téměř	téměř	k6eAd1	téměř
6	[number]	k4	6
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
něco	něco	k3yInSc4	něco
přes	přes	k7c4	přes
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohou	rozloha	k1gFnSc7	rozloha
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
blíží	blížit	k5eAaImIp3nS	blížit
našemu	náš	k3xOp1gInSc3	náš
kraji	kraj	k1gInSc3	kraj
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
o	o	k7c4	o
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
obyvatel	obyvatel	k1gMnSc1	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
téměř	téměř	k6eAd1	téměř
třikrát	třikrát	k6eAd1	třikrát
větší	veliký	k2eAgNnPc4d2	veliký
než	než	k8xS	než
tradiční	tradiční	k2eAgNnPc4d1	tradiční
britská	britský	k2eAgNnPc4d1	Britské
hrabství	hrabství	k1gNnPc4	hrabství
(	(	kIx(	(
<g/>
county	count	k1gInPc1	count
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
rovněž	rovněž	k9	rovněž
o	o	k7c4	o
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
obyvatel	obyvatel	k1gMnSc1	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Departementy	departement	k1gInPc1	departement
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
administrativní	administrativní	k2eAgInPc4d1	administrativní
arrondisementy	arrondisement	k1gInPc4	arrondisement
(	(	kIx(	(
<g/>
arrondissement	arrondissement	k1gMnSc1	arrondissement
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
kantony	kanton	k1gInPc4	kanton
(	(	kIx(	(
<g/>
canton	canton	k1gInSc4	canton
<g/>
)	)	kIx)	)
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
obce	obec	k1gFnSc2	obec
(	(	kIx(	(
<g/>
commune	commun	k1gInSc5	commun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Departement	departement	k1gInSc1	departement
je	být	k5eAaImIp3nS	být
jednotkou	jednotka	k1gFnSc7	jednotka
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
i	i	k8xC	i
občanské	občanský	k2eAgFnSc2d1	občanská
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
Departement	departement	k1gInSc1	departement
jako	jako	k8xS	jako
správní	správní	k2eAgFnSc1d1	správní
jednotka	jednotka	k1gFnSc1	jednotka
veřejného	veřejný	k2eAgNnSc2d1	veřejné
práva	právo	k1gNnSc2	právo
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
také	také	k9	také
prefektura	prefektura	k1gFnSc1	prefektura
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
řízena	řízen	k2eAgFnSc1d1	řízena
prefektem	prefekt	k1gMnSc7	prefekt
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
<g/>
préfet	préfet	k1gInSc1	préfet
du	du	k?	du
département	département	k1gInSc1	département
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jmenovaným	jmenovaný	k1gMnPc3	jmenovaný
přímo	přímo	k6eAd1	přímo
presidentem	president	k1gMnSc7	president
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
podprefektury	podprefektura	k1gFnPc4	podprefektura
(	(	kIx(	(
<g/>
sous-préfecture	sousréfectur	k1gMnSc5	sous-préfectur
<g/>
)	)	kIx)	)
vedené	vedený	k2eAgMnPc4d1	vedený
podprefekty	podprefekt	k1gMnPc4	podprefekt
(	(	kIx(	(
<g/>
sous-préfet	sousréfet	k1gInSc1	sous-préfet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
departementy	departement	k1gInPc4	departement
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
dělí	dělit	k5eAaImIp3nS	dělit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
VIII	VIII	kA	VIII
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
(	(	kIx(	(
<g/>
1799	[number]	k4	1799
<g/>
-	-	kIx~	-
<g/>
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgInPc2	tento
departementů	departement	k1gInPc2	departement
je	být	k5eAaImIp3nS	být
spravována	spravován	k2eAgFnSc1d1	spravována
řada	řada	k1gFnSc1	řada
státních	státní	k2eAgFnPc2d1	státní
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
Rada	rada	k1gFnSc1	rada
departmentu	department	k1gInSc6	department
pro	pro	k7c4	pro
výstroj	výstroj	k1gFnSc4	výstroj
(	(	kIx(	(
<g/>
Direction	Direction	k1gInSc4	Direction
départementale	départemental	k1gMnSc5	départemental
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
équipement	équipement	k1gMnSc1	équipement
,	,	kIx,	,
DDE	DDE	kA	DDE
<g/>
)	)	kIx)	)
či	či	k8xC	či
Rada	rada	k1gFnSc1	rada
departementu	departement	k1gInSc2	departement
pro	pro	k7c4	pro
záležitosti	záležitost	k1gFnPc4	záležitost
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
a	a	k8xC	a
sociální	sociální	k2eAgNnSc4d1	sociální
(	(	kIx(	(
<g/>
Direction	Direction	k1gInSc4	Direction
départementale	départemental	k1gMnSc5	départemental
des	des	k1gNnSc2	des
affaires	affaires	k1gMnSc1	affaires
sanitaires	sanitaires	k1gMnSc1	sanitaires
et	et	k?	et
sociales	sociales	k1gMnSc1	sociales
<g/>
,	,	kIx,	,
DDASS	DDASS	kA	DDASS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Departement	departement	k1gInSc1	departement
je	být	k5eAaImIp3nS	být
též	též	k9	též
samosprávné	samosprávný	k2eAgNnSc1d1	samosprávné
čili	čili	k8xC	čili
decentralizované	decentralizovaný	k2eAgNnSc1d1	decentralizované
místní	místní	k2eAgNnSc1d1	místní
společenství	společenství	k1gNnSc1	společenství
<g/>
,	,	kIx,	,
řízené	řízený	k2eAgNnSc1d1	řízené
generální	generální	k2eAgFnSc7d1	generální
radou	rada	k1gFnSc7	rada
(	(	kIx(	(
<g/>
krajskou	krajský	k2eAgFnSc7d1	krajská
radou	rada	k1gFnSc7	rada
<g/>
,	,	kIx,	,
fr.	fr.	k?	fr.
conseil	conseil	k1gInSc1	conseil
général	générat	k5eAaImAgInS	générat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
volí	volit	k5eAaImIp3nS	volit
všeobecným	všeobecný	k2eAgNnSc7d1	všeobecné
hlasováním	hlasování	k1gNnSc7	hlasování
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
každé	každý	k3xTgInPc4	každý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
obměnila	obměnit	k5eAaPmAgFnS	obměnit
polovina	polovina	k1gFnSc1	polovina
zastupitelů	zastupitel	k1gMnPc2	zastupitel
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgInSc1d1	volební
mandát	mandát	k1gInSc1	mandát
zastupitele	zastupitel	k1gMnSc2	zastupitel
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
na	na	k7c4	na
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
generální	generální	k2eAgFnSc2d1	generální
rady	rada	k1gFnSc2	rada
stojí	stát	k5eAaImIp3nS	stát
její	její	k3xOp3gMnSc1	její
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
země	zem	k1gFnSc2	zem
na	na	k7c4	na
departementy	departement	k1gInPc4	departement
předložil	předložit	k5eAaPmAgInS	předložit
králi	král	k1gMnSc3	král
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1665	[number]	k4	1665
Marc-René	Marc-Rená	k1gFnSc2	Marc-Rená
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Argenson	Argenson	k1gInSc4	Argenson
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
rozložila	rozložit	k5eAaPmAgNnP	rozložit
daňová	daňový	k2eAgNnPc1d1	daňové
břemena	břemeno	k1gNnPc1	břemeno
i	i	k8xC	i
státní	státní	k2eAgFnSc1d1	státní
správa	správa	k1gFnSc1	správa
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
mostů	most	k1gInPc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Departementy	departement	k1gInPc4	departement
zavedl	zavést	k5eAaPmAgInS	zavést
francouzský	francouzský	k2eAgInSc1d1	francouzský
parlament	parlament	k1gInSc1	parlament
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1790	[number]	k4	1790
hlavně	hlavně	k6eAd1	hlavně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
oslabily	oslabit	k5eAaPmAgFnP	oslabit
lokální	lokální	k2eAgFnPc1d1	lokální
solidarity	solidarita	k1gFnPc1	solidarita
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
historických	historický	k2eAgInPc2d1	historický
a	a	k8xC	a
feudálních	feudální	k2eAgInPc2d1	feudální
regionů	region	k1gInPc2	region
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
posílila	posílit	k5eAaPmAgFnS	posílit
loajalita	loajalita	k1gFnSc1	loajalita
k	k	k7c3	k
centrální	centrální	k2eAgFnSc3d1	centrální
vládě	vláda	k1gFnSc3	vláda
a	a	k8xC	a
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Departement	departement	k1gInSc1	departement
byl	být	k5eAaImAgInS	být
vymezen	vymezit	k5eAaPmNgInS	vymezit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každý	každý	k3xTgMnSc1	každý
občan	občan	k1gMnSc1	občan
mohl	moct	k5eAaImAgMnS	moct
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
dojet	dojet	k5eAaPmF	dojet
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Departementy	departement	k1gInPc1	departement
dostaly	dostat	k5eAaPmAgInP	dostat
nové	nový	k2eAgInPc4d1	nový
názvy	název	k1gInPc4	název
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
podle	podle	k7c2	podle
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
oslabily	oslabit	k5eAaPmAgFnP	oslabit
historické	historický	k2eAgFnPc1d1	historická
souvislosti	souvislost	k1gFnPc1	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
seřazeny	seřadit	k5eAaPmNgFnP	seřadit
podle	podle	k7c2	podle
abecedy	abeceda	k1gFnSc2	abeceda
a	a	k8xC	a
očíslovány	očíslovat	k5eAaPmNgFnP	očíslovat
od	od	k7c2	od
1	[number]	k4	1
(	(	kIx(	(
<g/>
Ain	Ain	k1gMnPc2	Ain
<g/>
)	)	kIx)	)
do	do	k7c2	do
83	[number]	k4	83
(	(	kIx(	(
<g/>
Yonne	Yonn	k1gInSc5	Yonn
<g/>
)	)	kIx)	)
a	a	k8xC	a
tato	tento	k3xDgNnPc1	tento
čísla	číslo	k1gNnPc1	číslo
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
hojně	hojně	k6eAd1	hojně
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
poštovním	poštovní	k2eAgInPc3d1	poštovní
a	a	k8xC	a
administrativním	administrativní	k2eAgInPc3d1	administrativní
účelům	účel	k1gInPc3	účel
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
na	na	k7c6	na
číslech	číslo	k1gNnPc6	číslo
aut	auto	k1gNnPc2	auto
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
byly	být	k5eAaImAgFnP	být
zřízeny	zřídit	k5eAaPmNgFnP	zřídit
větší	veliký	k2eAgFnPc1d2	veliký
administrativní	administrativní	k2eAgFnPc1d1	administrativní
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
regiony	region	k1gInPc1	region
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
patrně	patrně	k6eAd1	patrně
roste	růst	k5eAaImIp3nS	růst
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
dokonce	dokonce	k9	dokonce
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
departementů	departement	k1gInPc2	departement
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
(	(	kIx(	(
<g/>
1790	[number]	k4	1790
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
departementy	departement	k1gInPc1	departement
očíslovány	očíslován	k2eAgInPc1d1	očíslován
(	(	kIx(	(
<g/>
čísly	čísnout	k5eAaPmAgFnP	čísnout
01	[number]	k4	01
-	-	kIx~	-
83	[number]	k4	83
<g/>
)	)	kIx)	)
jen	jen	k9	jen
pro	pro	k7c4	pro
poštovní	poštovní	k2eAgInPc4d1	poštovní
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každou	každý	k3xTgFnSc4	každý
zásilku	zásilka	k1gFnSc4	zásilka
byla	být	k5eAaImAgFnS	být
umísťována	umísťován	k2eAgFnSc1d1	umísťována
pečeť	pečeť	k1gFnSc1	pečeť
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
výchozího	výchozí	k2eAgInSc2d1	výchozí
departementu	departement	k1gInSc2	departement
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
poštovní	poštovní	k2eAgInSc1d1	poštovní
systém	systém	k1gInSc1	systém
fungoval	fungovat	k5eAaImAgInS	fungovat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacátém	dvacátý	k4xOgNnSc6	dvacátý
století	století	k1gNnSc6	století
byl	být	k5eAaImAgInS	být
přidáním	přidání	k1gNnSc7	přidání
5	[number]	k4	5
dalších	další	k2eAgInPc2d1	další
departementů	departement	k1gInPc2	departement
(	(	kIx(	(
<g/>
Alpes-Maritimes	Alpes-Maritimes	k1gMnSc1	Alpes-Maritimes
<g/>
,	,	kIx,	,
Loire	Loir	k1gMnSc5	Loir
<g/>
,	,	kIx,	,
Savojsko	Savojsko	k1gNnSc1	Savojsko
<g/>
,	,	kIx,	,
Horní	horní	k2eAgNnSc1d1	horní
Savojsko	Savojsko	k1gNnSc1	Savojsko
a	a	k8xC	a
Tarn-et-Garonne	Tarnt-Garonn	k1gInSc5	Tarn-et-Garonn
<g/>
)	)	kIx)	)
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
nový	nový	k2eAgInSc1d1	nový
seznam	seznam	k1gInSc1	seznam
departementů	departement	k1gInPc2	departement
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
seřazen	seřadit	k5eAaPmNgInS	seřadit
podle	podle	k7c2	podle
abecedy	abeceda	k1gFnSc2	abeceda
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
změnám	změna	k1gFnPc3	změna
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
modifikován	modifikovat	k5eAaBmNgMnS	modifikovat
na	na	k7c4	na
počet	počet	k1gInSc4	počet
89	[number]	k4	89
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
měl	mít	k5eAaImAgMnS	mít
département	département	k1gMnSc1	département
Ain	Ain	k1gMnSc1	Ain
<g/>
,	,	kIx,	,
89	[number]	k4	89
Yonne	Yonn	k1gInSc5	Yonn
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
nato	nato	k6eAd1	nato
z	z	k7c2	z
území	území	k1gNnSc2	území
kolem	kolem	k7c2	kolem
Belfortu	Belfort	k1gInSc2	Belfort
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
departement	departement	k1gInSc1	departement
Territoire	Territoir	k1gInSc5	Territoir
de	de	k?	de
Belfort	Belfort	k1gInSc1	Belfort
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
90	[number]	k4	90
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
administrativního	administrativní	k2eAgNnSc2d1	administrativní
dělení	dělení	k1gNnSc3	dělení
v	v	k7c6	v
regionu	region	k1gInSc6	region
Île-de-France	Îlee-France	k1gFnSc2	Île-de-France
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc6	vytvoření
departementů	departement	k1gInPc2	departement
Paris	Paris	k1gMnSc1	Paris
(	(	kIx(	(
<g/>
jenž	jenž	k3xRgMnSc1	jenž
získal	získat	k5eAaPmAgInS	získat
číslo	číslo	k1gNnSc4	číslo
75	[number]	k4	75
patřící	patřící	k2eAgInSc1d1	patřící
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
departementu	departement	k1gInSc2	departement
Seine	Sein	k1gMnSc5	Sein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Yvelines	Yvelines	k1gMnSc1	Yvelines
(	(	kIx(	(
<g/>
přejímající	přejímající	k2eAgMnSc1d1	přejímající
č.	č.	k?	č.
78	[number]	k4	78
místo	místo	k1gNnSc4	místo
Seine-et-Oise	Seinet-Oise	k1gFnSc2	Seine-et-Oise
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Hauts-de-Seine	Hautse-Sein	k1gInSc5	Hauts-de-Sein
<g/>
,	,	kIx,	,
Seine-Saint-Denis	Seine-Saint-Denis	k1gFnSc5	Seine-Saint-Denis
<g/>
,	,	kIx,	,
Val-de-Marne	Vale-Marn	k1gInSc5	Val-de-Marn
a	a	k8xC	a
Val-d	Val	k1gInSc1	Val-d
<g/>
'	'	kIx"	'
<g/>
Oise	Ois	k1gInPc1	Ois
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
na	na	k7c4	na
konec	konec	k1gInSc4	konec
seznamu	seznam	k1gInSc2	seznam
a	a	k8xC	a
získaly	získat	k5eAaPmAgFnP	získat
tak	tak	k9	tak
čísla	číslo	k1gNnSc2	číslo
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
95	[number]	k4	95
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
byla	být	k5eAaImAgFnS	být
Korsika	Korsika	k1gFnSc1	Korsika
(	(	kIx(	(
<g/>
departement	departement	k1gInSc1	departement
La	la	k1gNnSc2	la
Corse	Corse	k1gFnSc2	Corse
<g/>
,	,	kIx,	,
č.	č.	k?	č.
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
departementy	departement	k1gInPc4	departement
<g/>
,	,	kIx,	,
Corse-du-Sud	Corseu-Sud	k1gMnSc1	Corse-du-Sud
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
a	a	k8xC	a
Haute-Corse	Haute-Corse	k1gFnSc1	Haute-Corse
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zámořským	zámořský	k2eAgInPc3d1	zámořský
departementům	departement	k1gInPc3	departement
byla	být	k5eAaImAgFnS	být
přidána	přidán	k2eAgNnPc1d1	Přidáno
čísla	číslo	k1gNnPc4	číslo
971	[number]	k4	971
<g/>
-	-	kIx~	-
<g/>
974	[number]	k4	974
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
číslo	číslo	k1gNnSc4	číslo
96	[number]	k4	96
bylo	být	k5eAaImAgNnS	být
vynecháno	vynechán	k2eAgNnSc1d1	vynecháno
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnPc1	číslo
departementů	departement	k1gInPc2	departement
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
Francouzi	Francouz	k1gMnPc1	Francouz
hojně	hojně	k6eAd1	hojně
používána	používán	k2eAgFnSc1d1	používána
i	i	k9	i
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
kupříkladu	kupříkladu	k6eAd1	kupříkladu
na	na	k7c6	na
registračních	registrační	k2eAgFnPc6d1	registrační
značkách	značka	k1gFnPc6	značka
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
všech	všecek	k3xTgMnPc2	všecek
poštovních	poštovní	k1gMnPc2	poštovní
směrovacích	směrovací	k2eAgNnPc2d1	směrovací
čísel	číslo	k1gNnPc2	číslo
či	či	k8xC	či
v	v	k7c6	v
kódech	kód	k1gInPc6	kód
INSEE	INSEE	kA	INSEE
(	(	kIx(	(
<g/>
obdoba	obdoba	k1gFnSc1	obdoba
českých	český	k2eAgNnPc2d1	české
rodných	rodný	k2eAgNnPc2d1	rodné
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc1d1	ostatní
francouzská	francouzský	k2eAgNnPc1d1	francouzské
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nemají	mít	k5eNaImIp3nP	mít
statut	statut	k1gInSc4	statut
departementu	departement	k1gInSc2	departement
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
čísla	číslo	k1gNnPc4	číslo
vytvořená	vytvořený	k2eAgNnPc4d1	vytvořené
obdobným	obdobný	k2eAgInSc7d1	obdobný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Zámořská	zámořský	k2eAgNnPc1d1	zámořské
společenství	společenství	k1gNnPc1	společenství
(	(	kIx(	(
<g/>
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
statut	statut	k1gInSc1	statut
je	být	k5eAaImIp3nS	být
departementu	departement	k1gInSc3	departement
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
)	)	kIx)	)
Saint-Pierre	Saint-Pierr	k1gInSc5	Saint-Pierr
a	a	k8xC	a
Miquelon	Miquelon	k1gInSc4	Miquelon
a	a	k8xC	a
Mayotte	Mayott	k1gInSc5	Mayott
mají	mít	k5eAaImIp3nP	mít
čísla	číslo	k1gNnPc4	číslo
975	[number]	k4	975
a	a	k8xC	a
976	[number]	k4	976
<g/>
,	,	kIx,	,
Wallis	Wallis	k1gFnSc1	Wallis
a	a	k8xC	a
Futuna	Futuna	k1gFnSc1	Futuna
<g/>
,	,	kIx,	,
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Polynésie	Polynésie	k1gFnSc1	Polynésie
a	a	k8xC	a
Nová	nový	k2eAgFnSc1d1	nová
Kaledonie	Kaledonie	k1gFnSc1	Kaledonie
mají	mít	k5eAaImIp3nP	mít
čísla	číslo	k1gNnPc1	číslo
986	[number]	k4	986
<g/>
,	,	kIx,	,
987	[number]	k4	987
a	a	k8xC	a
988	[number]	k4	988
<g/>
.	.	kIx.	.
</s>
<s>
Monako	Monako	k1gNnSc1	Monako
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nezávislý	závislý	k2eNgInSc4d1	nezávislý
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
poštovní	poštovní	k2eAgInPc4d1	poštovní
směrovací	směrovací	k2eAgInPc4d1	směrovací
kódy	kód	k1gInPc4	kód
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
jako	jako	k9	jako
začáteční	začáteční	k2eAgNnSc4d1	začáteční
číslo	číslo	k1gNnSc4	číslo
98	[number]	k4	98
přidělené	přidělený	k2eAgInPc1d1	přidělený
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
poštou	pošta	k1gFnSc7	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
jako	jako	k8xS	jako
další	další	k2eAgInSc1d1	další
nezávislý	závislý	k2eNgInSc1d1	nezávislý
stát	stát	k1gInSc1	stát
ležící	ležící	k2eAgInSc1d1	ležící
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Francie	Francie	k1gFnSc2	Francie
Regiony	region	k1gInPc1	region
Francie	Francie	k1gFnSc2	Francie
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
departement	departement	k1gInSc1	departement
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
francouzské	francouzský	k2eAgInPc1d1	francouzský
departementy	departement	k1gInPc1	departement
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
