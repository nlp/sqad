<s>
Havárie	havárie	k1gFnSc1	havárie
japonské	japonský	k2eAgFnSc2d1	japonská
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Fukušima	Fukušimum	k1gNnSc2	Fukušimum
I	i	k8xC	i
společnosti	společnost	k1gFnSc2	společnost
TEPCO	TEPCO	kA	TEPCO
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
nejhorší	zlý	k2eAgFnSc1d3	nejhorší
jaderná	jaderný	k2eAgFnSc1d1	jaderná
havárie	havárie	k1gFnSc1	havárie
od	od	k7c2	od
Černobylu	Černobyl	k1gInSc2	Černobyl
1986	[number]	k4	1986
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
jediná	jediný	k2eAgFnSc1d1	jediná
další	další	k2eAgFnSc1d1	další
havárie	havárie	k1gFnSc1	havárie
označená	označený	k2eAgFnSc1d1	označená
na	na	k7c6	na
stupnici	stupnice	k1gFnSc6	stupnice
INES	INES	kA	INES
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
stupněm	stupeň	k1gInSc7	stupeň
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Katastrofa	katastrofa	k1gFnSc1	katastrofa
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
následkem	následkem	k7c2	následkem
zatopení	zatopení	k1gNnSc2	zatopení
elektrárny	elektrárna	k1gFnSc2	elektrárna
ničivou	ničivý	k2eAgFnSc7d1	ničivá
vlnou	vlna	k1gFnSc7	vlna
tsunami	tsunami	k1gNnSc1	tsunami
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
mimořádně	mimořádně	k6eAd1	mimořádně
silným	silný	k2eAgNnSc7d1	silné
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Tóhoku	Tóhok	k1gInSc2	Tóhok
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
havárii	havárie	k1gFnSc6	havárie
došlo	dojít	k5eAaPmAgNnS	dojít
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
k	k	k7c3	k
závažnému	závažný	k2eAgNnSc3d1	závažné
poškození	poškození	k1gNnSc3	poškození
tří	tři	k4xCgInPc2	tři
tlakových	tlakový	k2eAgFnPc2d1	tlaková
nádob	nádoba	k1gFnPc2	nádoba
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obnažení	obnažení	k1gNnSc6	obnažení
paliva	palivo	k1gNnSc2	palivo
v	v	k7c6	v
reaktoru	reaktor	k1gInSc6	reaktor
vznikal	vznikat	k5eAaImAgInS	vznikat
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
příčinou	příčina	k1gFnSc7	příčina
tří	tři	k4xCgInPc2	tři
mohutných	mohutný	k2eAgFnPc2d1	mohutná
explozí	exploze	k1gFnPc2	exploze
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
exploze	exploze	k1gFnPc1	exploze
zásadně	zásadně	k6eAd1	zásadně
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
a	a	k8xC	a
rozptýlení	rozptýlení	k1gNnSc3	rozptýlení
štěpných	štěpný	k2eAgInPc2d1	štěpný
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dočasně	dočasně	k6eAd1	dočasně
způsobily	způsobit	k5eAaPmAgFnP	způsobit
okolí	okolí	k1gNnSc4	okolí
elektrárny	elektrárna	k1gFnSc2	elektrárna
neobyvatelným	obyvatelný	k2eNgInPc3d1	neobyvatelný
a	a	k8xC	a
také	také	k9	také
dočasně	dočasně	k6eAd1	dočasně
ekonomicky	ekonomicky	k6eAd1	ekonomicky
znehodnotily	znehodnotit	k5eAaPmAgFnP	znehodnotit
široké	široký	k2eAgFnPc1d1	široká
oblasti	oblast	k1gFnPc1	oblast
jinak	jinak	k6eAd1	jinak
velmi	velmi	k6eAd1	velmi
úrodné	úrodný	k2eAgFnSc2d1	úrodná
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc4	příčina
havárie	havárie	k1gFnSc2	havárie
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
hledat	hledat	k5eAaImF	hledat
již	již	k6eAd1	již
v	v	k7c6	v
čase	čas	k1gInSc6	čas
před	před	k7c7	před
samotnou	samotný	k2eAgFnSc7d1	samotná
havárií	havárie	k1gFnSc7	havárie
–	–	k?	–
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
připravenost	připravenost	k1gFnSc1	připravenost
personálu	personál	k1gInSc2	personál
na	na	k7c4	na
možné	možný	k2eAgInPc4d1	možný
havarijní	havarijní	k2eAgInPc4d1	havarijní
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
zanedbávání	zanedbávání	k1gNnSc1	zanedbávání
připomínek	připomínka	k1gFnPc2	připomínka
regulačních	regulační	k2eAgInPc2d1	regulační
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
chyby	chyba	k1gFnPc1	chyba
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
legislativě	legislativa	k1gFnSc6	legislativa
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
regulačních	regulační	k2eAgInPc2d1	regulační
úřadů	úřad	k1gInPc2	úřad
a	a	k8xC	a
k	k	k7c3	k
havárii	havárie	k1gFnSc3	havárie
svojí	svůj	k3xOyFgFnSc7	svůj
měrou	míra	k1gFnSc7wR	míra
přispěly	přispět	k5eAaPmAgInP	přispět
i	i	k9	i
kulturní	kulturní	k2eAgInPc1d1	kulturní
předpoklady	předpoklad	k1gInPc1	předpoklad
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
japonská	japonský	k2eAgFnSc1d1	japonská
hierarchie	hierarchie	k1gFnSc1	hierarchie
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
150	[number]	k4	150
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
evakuováno	evakuovat	k5eAaBmNgNnS	evakuovat
z	z	k7c2	z
potenciálně	potenciálně	k6eAd1	potenciálně
nebezpečného	bezpečný	k2eNgNnSc2d1	nebezpečné
okolí	okolí	k1gNnSc2	okolí
elektrárny	elektrárna	k1gFnSc2	elektrárna
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
šíření	šíření	k1gNnSc2	šíření
štěpných	štěpný	k2eAgInPc2d1	štěpný
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prozatím	prozatím	k6eAd1	prozatím
nebyla	být	k5eNaImAgFnS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
žádná	žádný	k3yNgNnPc4	žádný
úmrtí	úmrtí	k1gNnPc4	úmrtí
nebo	nebo	k8xC	nebo
nemoci	nemoc	k1gFnPc4	nemoc
způsobené	způsobený	k2eAgFnPc4d1	způsobená
ozářením	ozáření	k1gNnSc7	ozáření
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
evakuovaných	evakuovaný	k2eAgMnPc2d1	evakuovaný
obyvatel	obyvatel	k1gMnPc2	obyvatel
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
samotné	samotný	k2eAgFnSc2d1	samotná
evakuace	evakuace	k1gFnSc2	evakuace
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
již	již	k9	již
kvůli	kvůli	k7c3	kvůli
pokročilému	pokročilý	k2eAgInSc3d1	pokročilý
věku	věk	k1gInSc3	věk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
chronických	chronický	k2eAgNnPc2d1	chronické
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Ničivá	ničivý	k2eAgFnSc1d1	ničivá
katastrofa	katastrofa	k1gFnSc1	katastrofa
tedy	tedy	k9	tedy
odhalila	odhalit	k5eAaPmAgFnS	odhalit
spoustu	spousta	k1gFnSc4	spousta
chyb	chyba	k1gFnPc2	chyba
v	v	k7c6	v
přístupu	přístup	k1gInSc6	přístup
společnosti	společnost	k1gFnSc2	společnost
TEPCO	TEPCO	kA	TEPCO
k	k	k7c3	k
jaderné	jaderný	k2eAgFnSc3d1	jaderná
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ukázala	ukázat	k5eAaPmAgFnS	ukázat
na	na	k7c4	na
roztříštěnost	roztříštěnost	k1gFnSc4	roztříštěnost
vedení	vedení	k1gNnSc2	vedení
po	po	k7c6	po
katastrofě	katastrofa	k1gFnSc6	katastrofa
a	a	k8xC	a
odhalila	odhalit	k5eAaPmAgFnS	odhalit
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
laxnost	laxnost	k1gFnSc4	laxnost
kontrolních	kontrolní	k2eAgInPc2d1	kontrolní
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
faktory	faktor	k1gInPc1	faktor
vyústily	vyústit	k5eAaPmAgInP	vyústit
do	do	k7c2	do
havárie	havárie	k1gFnSc2	havárie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
podle	podle	k7c2	podle
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
založené	založený	k2eAgNnSc1d1	založené
japonským	japonský	k2eAgInSc7d1	japonský
parlamentem	parlament	k1gInSc7	parlament
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
man-made	manást	k5eAaPmIp3nS	man-mást
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
zapříčiněna	zapříčinit	k5eAaPmNgFnS	zapříčinit
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Člověkem	člověk	k1gMnSc7	člověk
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nemyslí	myslet	k5eNaImIp3nS	myslet
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
celý	celý	k2eAgInSc4d1	celý
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
zanedbáváno	zanedbáván	k2eAgNnSc1d1	zanedbáváno
informování	informování	k1gNnSc1	informování
veřejnosti	veřejnost	k1gFnSc2	veřejnost
ať	ať	k8xC	ať
již	již	k6eAd1	již
před	před	k7c7	před
havárií	havárie	k1gFnSc7	havárie
nebo	nebo	k8xC	nebo
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
samotné	samotný	k2eAgFnSc2d1	samotná
havárie	havárie	k1gFnSc2	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
také	také	k6eAd1	také
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
různé	různý	k2eAgFnPc4d1	různá
občanské	občanský	k2eAgFnPc4d1	občanská
iniciativy	iniciativa	k1gFnPc4	iniciativa
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
získat	získat	k5eAaPmF	získat
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
organizace	organizace	k1gFnSc1	organizace
Safecast	Safecast	k1gFnSc1	Safecast
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
posléze	posléze	k6eAd1	posléze
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
i	i	k9	i
vlastní	vlastní	k2eAgInPc4d1	vlastní
přístroje	přístroj	k1gInPc4	přístroj
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
organizovat	organizovat	k5eAaBmF	organizovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
radiační	radiační	k2eAgInSc4d1	radiační
monitoring	monitoring	k1gInSc4	monitoring
zajišťovaný	zajišťovaný	k2eAgInSc4d1	zajišťovaný
dobrovolníky	dobrovolník	k1gMnPc4	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
takto	takto	k6eAd1	takto
získaná	získaný	k2eAgNnPc1d1	získané
data	datum	k1gNnPc1	datum
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgNnP	být
zveřejňována	zveřejňovat	k5eAaImNgNnP	zveřejňovat
na	na	k7c6	na
jejich	jejich	k3xOp3gFnPc6	jejich
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
na	na	k7c6	na
interaktivní	interaktivní	k2eAgFnSc6d1	interaktivní
mapě	mapa	k1gFnSc6	mapa
-	-	kIx~	-
Safecast	Safecast	k1gInSc1	Safecast
Tile	til	k1gInSc5	til
Map	mapa	k1gFnPc2	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
kvůli	kvůli	k7c3	kvůli
nedostatečné	dostatečný	k2eNgFnSc3d1	nedostatečná
informovanosti	informovanost	k1gFnSc3	informovanost
japonské	japonský	k2eAgFnSc2d1	japonská
veřejnosti	veřejnost	k1gFnSc2	veřejnost
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
velmi	velmi	k6eAd1	velmi
prudká	prudký	k2eAgFnSc1d1	prudká
reakce	reakce	k1gFnSc1	reakce
vůči	vůči	k7c3	vůči
pokračování	pokračování	k1gNnSc3	pokračování
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energetiky	energetika	k1gFnSc2	energetika
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
vedle	vedle	k7c2	vedle
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
další	další	k2eAgInSc4d1	další
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
k	k	k7c3	k
odstavování	odstavování	k1gNnSc3	odstavování
zbylých	zbylý	k2eAgFnPc2d1	zbylá
japonských	japonský	k2eAgFnPc2d1	japonská
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Havárie	havárie	k1gFnSc1	havárie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
podnětem	podnět	k1gInSc7	podnět
k	k	k7c3	k
politickým	politický	k2eAgFnPc3d1	politická
debatám	debata	k1gFnPc3	debata
o	o	k7c6	o
jaderné	jaderný	k2eAgFnSc6d1	jaderná
energetice	energetika	k1gFnSc6	energetika
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
i	i	k9	i
k	k	k7c3	k
odborným	odborný	k2eAgFnPc3d1	odborná
debatám	debata	k1gFnPc3	debata
ohledně	ohledně	k7c2	ohledně
jaderné	jaderný	k2eAgFnSc2d1	jaderná
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nového	nový	k2eAgInSc2d1	nový
jaderného	jaderný	k2eAgInSc2d1	jaderný
regulačního	regulační	k2eAgInSc2d1	regulační
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
oproti	oproti	k7c3	oproti
minulému	minulý	k2eAgNnSc3d1	Minulé
nespadá	spadat	k5eNaImIp3nS	spadat
pod	pod	k7c4	pod
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pod	pod	k7c4	pod
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Fukušima	Fukušima	k1gFnSc1	Fukušima
I.	I.	kA	I.
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Fukušima	Fukušimum	k1gNnSc2	Fukušimum
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
prefektuře	prefektura	k1gFnSc6	prefektura
Fukušima	Fukušimum	k1gNnSc2	Fukušimum
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Okuma	Okumum	k1gNnSc2	Okumum
a	a	k8xC	a
Futaba	Futaba	k1gFnSc1	Futaba
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
zabírá	zabírat	k5eAaImIp3nS	zabírat
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
plochu	plocha	k1gFnSc4	plocha
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
zhruba	zhruba	k6eAd1	zhruba
3,5	[number]	k4	3,5
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vlastněna	vlastnit	k5eAaImNgFnS	vlastnit
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
největší	veliký	k2eAgFnSc1d3	veliký
japonskou	japonský	k2eAgFnSc7d1	japonská
elektrárenskou	elektrárenský	k2eAgFnSc7d1	elektrárenská
společností	společnost	k1gFnSc7	společnost
TEPCO	TEPCO	kA	TEPCO
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
spuštěním	spuštění	k1gNnSc7	spuštění
bloku	blok	k1gInSc2	blok
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
bloky	blok	k1gInPc1	blok
byly	být	k5eAaImAgInP	být
uváděny	uvádět	k5eAaImNgInP	uvádět
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
celkového	celkový	k2eAgInSc2d1	celkový
instalovaného	instalovaný	k2eAgInSc2d1	instalovaný
výkonu	výkon	k1gInSc2	výkon
4,696	[number]	k4	4,696
GWe	GWe	k1gFnPc2	GWe
<g/>
.	.	kIx.	.
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Fukušima	Fukušima	k1gFnSc1	Fukušima
I	i	k9	i
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
šesti	šest	k4xCc2	šest
bloků	blok	k1gInPc2	blok
s	s	k7c7	s
varnými	varný	k2eAgInPc7d1	varný
reaktory	reaktor	k1gInPc7	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgInSc3	každý
bloku	blok	k1gInSc3	blok
náleží	náležet	k5eAaImIp3nS	náležet
řada	řada	k1gFnSc1	řada
budov	budova	k1gFnPc2	budova
<g/>
:	:	kIx,	:
aktivní	aktivní	k2eAgFnSc1d1	aktivní
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
strojovna	strojovna	k1gFnSc1	strojovna
<g/>
,	,	kIx,	,
sklad	sklad	k1gInSc1	sklad
vyhořelého	vyhořelý	k2eAgNnSc2d1	vyhořelé
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
pro	pro	k7c4	pro
nakládání	nakládání	k1gNnSc4	nakládání
s	s	k7c7	s
odpady	odpad	k1gInPc7	odpad
z	z	k7c2	z
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
bazén	bazén	k1gInSc1	bazén
vyhořelého	vyhořelý	k2eAgNnSc2d1	vyhořelé
paliva	palivo	k1gNnSc2	palivo
v	v	k7c6	v
aktivní	aktivní	k2eAgFnSc6d1	aktivní
zóně	zóna	k1gFnSc6	zóna
<g/>
,	,	kIx,	,
společný	společný	k2eAgInSc4d1	společný
bazén	bazén	k1gInSc4	bazén
vyhořelého	vyhořelý	k2eAgNnSc2d1	vyhořelé
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
čerpací	čerpací	k2eAgFnSc2d1	čerpací
stanice	stanice	k1gFnSc2	stanice
chladící	chladící	k2eAgFnSc2d1	chladící
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Bloky	blok	k1gInPc1	blok
1-4	[number]	k4	1-4
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
plošině	plošina	k1gFnSc6	plošina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
10	[number]	k4	10
m	m	kA	m
nad	nad	k7c7	nad
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Bloky	blok	k1gInPc1	blok
5	[number]	k4	5
a	a	k8xC	a
6	[number]	k4	6
jsou	být	k5eAaImIp3nP	být
položeny	položen	k2eAgInPc1d1	položen
o	o	k7c4	o
něco	něco	k3yInSc4	něco
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	s	k7c7	s
13	[number]	k4	13
m	m	kA	m
nad	nad	k7c7	nad
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
bez	bez	k7c2	bez
poškození	poškození	k1gNnSc2	poškození
při	při	k7c6	při
vlnách	vlna	k1gFnPc6	vlna
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
5,4	[number]	k4	5,4
m	m	kA	m
až	až	k9	až
5,7	[number]	k4	5,7
m.	m.	k?	m.
Bezpečné	bezpečný	k2eAgFnSc2d1	bezpečná
hodnoty	hodnota	k1gFnSc2	hodnota
pro	pro	k7c4	pro
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
byly	být	k5eAaImAgFnP	být
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
podle	podle	k7c2	podle
stupnice	stupnice	k1gFnSc2	stupnice
PGA	PGA	kA	PGA
(	(	kIx(	(
<g/>
Peak	Peak	k1gMnSc1	Peak
Ground	Ground	k1gMnSc1	Ground
Acceleration	Acceleration	k1gInSc1	Acceleration
<g/>
)	)	kIx)	)
kolem	kolem	k7c2	kolem
0,45	[number]	k4	0,45
g	g	kA	g
=	=	kIx~	=
4,415	[number]	k4	4,415
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s2	s2	k4	s2
-	-	kIx~	-
hodnoty	hodnota	k1gFnPc1	hodnota
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
i	i	k9	i
podle	podle	k7c2	podle
směru	směr	k1gInSc2	směr
otřesů	otřes	k1gInPc2	otřes
(	(	kIx(	(
<g/>
horizontální	horizontální	k2eAgFnSc1d1	horizontální
<g/>
,	,	kIx,	,
vertikální	vertikální	k2eAgFnSc1d1	vertikální
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgInPc1	všechen
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
zhruba	zhruba	k6eAd1	zhruba
kolem	kolem	k7c2	kolem
uvedené	uvedený	k2eAgFnSc2d1	uvedená
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
byly	být	k5eAaImAgFnP	být
bloky	blok	k1gInPc4	blok
1-3	[number]	k4	1-3
v	v	k7c6	v
normálním	normální	k2eAgInSc6d1	normální
provozu	provoz	k1gInSc6	provoz
a	a	k8xC	a
bloky	blok	k1gInPc1	blok
4-6	[number]	k4	4-6
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
odstávky	odstávka	k1gFnSc2	odstávka
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
první	první	k4xOgFnSc6	první
známce	známka	k1gFnSc6	známka
seizmické	seizmický	k2eAgFnSc2d1	seizmická
aktivity	aktivita	k1gFnSc2	aktivita
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
okamžitému	okamžitý	k2eAgNnSc3d1	okamžité
automatickému	automatický	k2eAgNnSc3d1	automatické
zastavení	zastavení	k1gNnSc3	zastavení
štěpné	štěpný	k2eAgFnSc2d1	štěpná
reakce	reakce	k1gFnSc2	reakce
(	(	kIx(	(
<g/>
SCRAM	SCRAM	kA	SCRAM
<g/>
)	)	kIx)	)
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
blocích	blok	k1gInPc6	blok
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
poškodilo	poškodit	k5eAaPmAgNnS	poškodit
rozvodné	rozvodný	k2eAgFnPc4d1	rozvodná
elektrické	elektrický	k2eAgFnPc4d1	elektrická
sítě	síť	k1gFnPc4	síť
spojující	spojující	k2eAgFnSc4d1	spojující
elektrárnu	elektrárna	k1gFnSc4	elektrárna
s	s	k7c7	s
rozvodnami	rozvodna	k1gFnPc7	rozvodna
<g/>
,	,	kIx,	,
následkem	následkem	k7c2	následkem
čehož	což	k3yRnSc2	což
byla	být	k5eAaImAgFnS	být
elektrárna	elektrárna	k1gFnSc1	elektrárna
bez	bez	k7c2	bez
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
vnějšího	vnější	k2eAgInSc2d1	vnější
zdroje	zdroj	k1gInSc2	zdroj
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
automatickému	automatický	k2eAgNnSc3d1	automatické
spuštění	spuštění	k1gNnSc3	spuštění
záložních	záložní	k2eAgMnPc2d1	záložní
diesel	diesel	k1gInSc4	diesel
generátorů	generátor	k1gInPc2	generátor
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yQgFnPc3	který
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
spolehlivě	spolehlivě	k6eAd1	spolehlivě
odvádět	odvádět	k5eAaImF	odvádět
zbytkové	zbytkový	k2eAgNnSc4d1	zbytkové
teplo	teplo	k1gNnSc4	teplo
z	z	k7c2	z
reaktoru	reaktor	k1gInSc2	reaktor
pomocí	pomocí	k7c2	pomocí
systémů	systém	k1gInPc2	systém
pro	pro	k7c4	pro
odvod	odvod	k1gInSc4	odvod
zbytkového	zbytkový	k2eAgNnSc2d1	zbytkové
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
překročení	překročení	k1gNnSc3	překročení
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
byla	být	k5eAaImAgFnS	být
elektrárna	elektrárna	k1gFnSc1	elektrárna
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prozatím	prozatím	k6eAd1	prozatím
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
závažnému	závažný	k2eAgNnSc3d1	závažné
poškození	poškození	k1gNnSc3	poškození
elektrárny	elektrárna	k1gFnSc2	elektrárna
před	před	k7c7	před
zaplavením	zaplavení	k1gNnSc7	zaplavení
vlnou	vlna	k1gFnSc7	vlna
Tsunami	tsunami	k1gNnSc4	tsunami
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vyšetřovacích	vyšetřovací	k2eAgFnPc2d1	vyšetřovací
zpráv	zpráva	k1gFnPc2	zpráva
se	se	k3xPyFc4	se
přiklání	přiklánět	k5eAaImIp3nS	přiklánět
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
vlivem	vlivem	k7c2	vlivem
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
byla	být	k5eAaImAgFnS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
dokud	dokud	k6eAd1	dokud
vlny	vlna	k1gFnSc2	vlna
Tsunami	tsunami	k1gNnSc1	tsunami
vyvolané	vyvolaný	k2eAgNnSc1d1	vyvolané
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
nezaplavily	zaplavit	k5eNaPmAgFnP	zaplavit
a	a	k8xC	a
nezničily	zničit	k5eNaPmAgFnP	zničit
záložní	záložní	k2eAgInSc4d1	záložní
diesel	diesel	k1gInSc4	diesel
generátory	generátor	k1gInPc7	generátor
<g/>
,	,	kIx,	,
čerpadla	čerpadlo	k1gNnPc4	čerpadlo
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgNnSc1d1	elektrické
vedení	vedení	k1gNnSc1	vedení
uvnitř	uvnitř	k7c2	uvnitř
elektrárny	elektrárna	k1gFnSc2	elektrárna
a	a	k8xC	a
zdroje	zdroj	k1gInSc2	zdroj
stejnosměrného	stejnosměrný	k2eAgNnSc2d1	stejnosměrné
napájení	napájení	k1gNnSc2	napájení
(	(	kIx(	(
<g/>
baterie	baterie	k1gFnPc1	baterie
na	na	k7c6	na
blocích	blok	k1gInPc6	blok
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
a	a	k8xC	a
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
bloku	blok	k1gInSc2	blok
6	[number]	k4	6
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgMnS	zůstat
provozu	provoz	k1gInSc2	provoz
schopný	schopný	k2eAgMnSc1d1	schopný
vzduchem	vzduch	k1gInSc7	vzduch
chlazený	chlazený	k2eAgInSc1d1	chlazený
diesel	diesel	k1gInSc1	diesel
generátor	generátor	k1gInSc4	generátor
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
bloky	blok	k1gInPc1	blok
bez	bez	k7c2	bez
zdroje	zdroj	k1gInSc2	zdroj
střídavého	střídavý	k2eAgNnSc2d1	střídavé
napájení	napájení	k1gNnSc2	napájení
a	a	k8xC	a
bloky	blok	k1gInPc1	blok
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
a	a	k8xC	a
4	[number]	k4	4
byly	být	k5eAaImAgFnP	být
bez	bez	k7c2	bez
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
zdroje	zdroj	k1gInSc2	zdroj
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
použití	použití	k1gNnSc4	použití
měřících	měřící	k2eAgInPc2d1	měřící
přístrojů	přístroj	k1gInPc2	přístroj
a	a	k8xC	a
značně	značně	k6eAd1	značně
omezila	omezit	k5eAaPmAgFnS	omezit
funkce	funkce	k1gFnSc1	funkce
blokové	blokový	k2eAgFnSc2d1	bloková
dozorny	dozorna	k1gFnSc2	dozorna
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zjistit	zjistit	k5eAaPmF	zjistit
hodnoty	hodnota	k1gFnPc4	hodnota
hladiny	hladina	k1gFnSc2	hladina
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
reaktoru	reaktor	k1gInSc2	reaktor
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
klíčové	klíčový	k2eAgInPc4d1	klíčový
parametry	parametr	k1gInPc4	parametr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
znamenalo	znamenat	k5eAaImAgNnS	znamenat
zbytečné	zbytečný	k2eAgInPc4d1	zbytečný
prostoje	prostoj	k1gInPc4	prostoj
a	a	k8xC	a
nedostatečně	dostatečně	k6eNd1	dostatečně
rychlé	rychlý	k2eAgFnPc4d1	rychlá
reakce	reakce	k1gFnPc4	reakce
na	na	k7c6	na
rapidně	rapidně	k6eAd1	rapidně
se	se	k3xPyFc4	se
zhoršující	zhoršující	k2eAgInSc1d1	zhoršující
stav	stav	k1gInSc1	stav
havárie	havárie	k1gFnSc2	havárie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ztráty	ztráta	k1gFnSc2	ztráta
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
nedostatečné	dostatečný	k2eNgFnSc6d1	nedostatečná
připravenosti	připravenost	k1gFnSc6	připravenost
provozovatelů	provozovatel	k1gMnPc2	provozovatel
na	na	k7c4	na
takovou	takový	k3xDgFnSc4	takový
situaci	situace	k1gFnSc4	situace
docházelo	docházet	k5eAaImAgNnS	docházet
postupně	postupně	k6eAd1	postupně
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
záložních	záložní	k2eAgInPc2d1	záložní
systémů	systém	k1gInPc2	systém
chlazení	chlazení	k1gNnSc2	chlazení
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
blocích	blok	k1gInPc6	blok
<g/>
.	.	kIx.	.
</s>
<s>
Nejhorší	zlý	k2eAgFnSc1d3	nejhorší
situace	situace	k1gFnSc1	situace
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
na	na	k7c6	na
bloku	blok	k1gInSc6	blok
1	[number]	k4	1
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
předpokládat	předpokládat	k5eAaImF	předpokládat
první	první	k4xOgNnPc1	první
poškození	poškození	k1gNnPc1	poškození
aktivní	aktivní	k2eAgFnSc2d1	aktivní
zóny	zóna	k1gFnSc2	zóna
okolo	okolo	k7c2	okolo
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
v	v	k7c4	v
den	den	k1gInSc4	den
havárie	havárie	k1gFnSc2	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obnažení	obnažení	k1gNnSc6	obnažení
paliva	palivo	k1gNnSc2	palivo
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
teploty	teplota	k1gFnSc2	teplota
palivových	palivový	k2eAgInPc2d1	palivový
proutků	proutek	k1gInPc2	proutek
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
ze	z	k7c2	z
slitiny	slitina	k1gFnSc2	slitina
zirkonia	zirkonium	k1gNnSc2	zirkonium
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
palivových	palivový	k2eAgInPc2d1	palivový
proutků	proutek	k1gInPc2	proutek
překročí	překročit	k5eAaPmIp3nS	překročit
900	[number]	k4	900
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
množství	množství	k1gNnSc1	množství
exotermické	exotermický	k2eAgNnSc1d1	exotermické
reakci	reakce	k1gFnSc4	reakce
zirkonia	zirkonium	k1gNnSc2	zirkonium
s	s	k7c7	s
vodní	vodní	k2eAgFnSc7d1	vodní
párou	pára	k1gFnSc7	pára
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
vzniká	vznikat	k5eAaImIp3nS	vznikat
oxid	oxid	k1gInSc1	oxid
zirkoničitý	zirkoničitý	k2eAgInSc1d1	zirkoničitý
a	a	k8xC	a
vodík	vodík	k1gInSc1	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
unikal	unikat	k5eAaImAgInS	unikat
z	z	k7c2	z
tlakové	tlakový	k2eAgFnSc2d1	tlaková
nádoby	nádoba	k1gFnSc2	nádoba
reaktoru	reaktor	k1gInSc2	reaktor
a	a	k8xC	a
hromadil	hromadit	k5eAaImAgMnS	hromadit
se	se	k3xPyFc4	se
ve	v	k7c6	v
vrchním	vrchní	k2eAgNnSc6d1	vrchní
patře	patro	k1gNnSc6	patro
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
výbušné	výbušný	k2eAgFnSc2d1	výbušná
koncentrace	koncentrace	k1gFnSc2	koncentrace
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
explozi	exploze	k1gFnSc3	exploze
a	a	k8xC	a
hoření	hoření	k1gNnSc6	hoření
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
na	na	k7c6	na
bloku	blok	k1gInSc6	blok
1	[number]	k4	1
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
36	[number]	k4	36
den	den	k1gInSc4	den
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Výbuch	výbuch	k1gInSc1	výbuch
vážně	vážně	k6eAd1	vážně
poničil	poničit	k5eAaPmAgInS	poničit
vrchní	vrchní	k2eAgNnSc4d1	vrchní
patro	patro	k1gNnSc4	patro
a	a	k8xC	a
rozmetal	rozmetat	k5eAaImAgMnS	rozmetat
trosky	troska	k1gFnPc4	troska
do	do	k7c2	do
širokého	široký	k2eAgNnSc2d1	široké
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dále	daleko	k6eAd2	daleko
zkomplikovalo	zkomplikovat	k5eAaPmAgNnS	zkomplikovat
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
zvládnutí	zvládnutí	k1gNnSc4	zvládnutí
havárie	havárie	k1gFnSc2	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Výbuch	výbuch	k1gInSc1	výbuch
také	také	k9	také
narušil	narušit	k5eAaPmAgInS	narušit
těsnost	těsnost	k1gFnSc4	těsnost
vrchního	vrchní	k2eAgNnSc2d1	vrchní
patra	patro	k1gNnSc2	patro
na	na	k7c6	na
bloku	blok	k1gInSc6	blok
2	[number]	k4	2
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
hromadění	hromadění	k1gNnSc3	hromadění
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
následnému	následný	k2eAgInSc3d1	následný
výbuchu	výbuch	k1gInSc3	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obnažování	obnažování	k1gNnSc3	obnažování
a	a	k8xC	a
tavení	tavení	k1gNnSc3	tavení
paliva	palivo	k1gNnSc2	palivo
i	i	k8xC	i
na	na	k7c6	na
bloku	blok	k1gInSc6	blok
2	[number]	k4	2
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
na	na	k7c6	na
bloku	blok	k1gInSc6	blok
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bloku	blok	k1gInSc6	blok
4	[number]	k4	4
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
aktivní	aktivní	k2eAgFnSc2d1	aktivní
zóny	zóna	k1gFnSc2	zóna
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nebylo	být	k5eNaImAgNnS	být
žádné	žádný	k3yNgNnSc1	žádný
palivo	palivo	k1gNnSc1	palivo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výbuchu	výbuch	k1gInSc3	výbuch
na	na	k7c6	na
bloku	blok	k1gInSc6	blok
3	[number]	k4	3
došlo	dojít	k5eAaPmAgNnS	dojít
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xS	jako
na	na	k7c6	na
bloku	blok	k1gInSc6	blok
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
na	na	k7c6	na
bloku	blok	k1gInSc6	blok
4	[number]	k4	4
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
explozi	exploze	k1gFnSc3	exploze
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
dostal	dostat	k5eAaPmAgMnS	dostat
z	z	k7c2	z
bloku	blok	k1gInSc2	blok
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
s	s	k7c7	s
blokem	blok	k1gInSc7	blok
4	[number]	k4	4
sdílí	sdílet	k5eAaImIp3nS	sdílet
odvětrávací	odvětrávací	k2eAgInSc1d1	odvětrávací
system	syst	k1gInSc7	syst
SGTS	SGTS	kA	SGTS
(	(	kIx(	(
<g/>
Stand-by	Standa	k1gMnSc2	Stand-ba
Gas	Gas	k1gMnSc1	Gas
Treatment	Treatment	k1gMnSc1	Treatment
System	Syst	k1gMnSc7	Syst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
všech	všecek	k3xTgInPc2	všecek
dostupných	dostupný	k2eAgInPc2d1	dostupný
systémů	systém	k1gInPc2	systém
chlazení	chlazení	k1gNnPc2	chlazení
se	se	k3xPyFc4	se
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
blocích	blok	k1gInPc6	blok
započalo	započnout	k5eAaPmAgNnS	započnout
se	s	k7c7	s
vstřikováním	vstřikování	k1gNnSc7	vstřikování
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
tlakové	tlakový	k2eAgFnSc2d1	tlaková
nádoby	nádoba	k1gFnSc2	nádoba
reaktoru	reaktor	k1gInSc2	reaktor
pomocí	pomocí	k7c2	pomocí
požárních	požární	k2eAgFnPc2d1	požární
cisteren	cisterna	k1gFnPc2	cisterna
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
tohoto	tento	k3xDgNnSc2	tento
vstřikování	vstřikování	k1gNnSc2	vstřikování
docílit	docílit	k5eAaPmF	docílit
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
operátoři	operátor	k1gMnPc1	operátor
složitě	složitě	k6eAd1	složitě
odtlakovat	odtlakovat	k5eAaImF	odtlakovat
reaktor	reaktor	k1gInSc4	reaktor
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zvyšovalo	zvyšovat	k5eAaImAgNnS	zvyšovat
množství	množství	k1gNnSc1	množství
uniklých	uniklý	k2eAgInPc2d1	uniklý
štěpných	štěpný	k2eAgInPc2d1	štěpný
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
do	do	k7c2	do
reaktoru	reaktor	k1gInSc2	reaktor
dodávala	dodávat	k5eAaImAgFnS	dodávat
slaná	slaný	k2eAgFnSc1d1	slaná
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
první	první	k4xOgFnSc6	první
příležitosti	příležitost	k1gFnSc6	příležitost
nahradila	nahradit	k5eAaPmAgFnS	nahradit
sladkovodní	sladkovodní	k2eAgFnSc7d1	sladkovodní
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
chlazení	chlazení	k1gNnSc2	chlazení
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
elektrické	elektrický	k2eAgNnSc1d1	elektrické
napájení	napájení	k1gNnSc1	napájení
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
došlo	dojít	k5eAaPmAgNnS	dojít
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2011	[number]	k4	2011
došlo	dojít	k5eAaPmAgNnS	dojít
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
reaktorů	reaktor	k1gInPc2	reaktor
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
tlakové	tlakový	k2eAgFnSc6d1	tlaková
nádobě	nádoba	k1gFnSc6	nádoba
reaktoru	reaktor	k1gInSc2	reaktor
pod	pod	k7c7	pod
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
únik	únik	k1gInSc1	únik
štěpných	štěpný	k2eAgInPc2d1	štěpný
produktů	produkt	k1gInPc2	produkt
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
minimální	minimální	k2eAgInSc1d1	minimální
<g/>
.	.	kIx.	.
</s>
<s>
Takovýto	takovýto	k3xDgInSc1	takovýto
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
studená	studený	k2eAgFnSc1d1	studená
odstávka	odstávka	k1gFnSc1	odstávka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
cold	cold	k1gMnSc1	cold
shut-down	shutown	k1gMnSc1	shut-down
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bloky	blok	k1gInPc1	blok
5	[number]	k4	5
a	a	k8xC	a
6	[number]	k4	6
byly	být	k5eAaImAgFnP	být
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
studené	studený	k2eAgFnSc2d1	studená
odstávky	odstávka	k1gFnSc2	odstávka
a	a	k8xC	a
havárii	havárie	k1gFnSc4	havárie
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
přes	přes	k7c4	přes
počáteční	počáteční	k2eAgInPc4d1	počáteční
problémy	problém	k1gInPc4	problém
podařilo	podařit	k5eAaPmAgNnS	podařit
zvládnout	zvládnout	k5eAaPmF	zvládnout
bez	bez	k7c2	bez
poškození	poškození	k1gNnSc2	poškození
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
jímek	jímka	k1gFnPc2	jímka
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
radioaktivní	radioaktivní	k2eAgFnSc1d1	radioaktivní
voda	voda	k1gFnSc1	voda
unikající	unikající	k2eAgFnSc1d1	unikající
z	z	k7c2	z
reaktoru	reaktor	k1gInSc2	reaktor
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
se	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
objevila	objevit	k5eAaPmAgFnS	objevit
trhlina	trhlina	k1gFnSc1	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
neúspěšné	úspěšný	k2eNgInPc1d1	neúspěšný
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
ucpání	ucpání	k1gNnSc4	ucpání
praskliny	prasklina	k1gFnSc2	prasklina
betonem	beton	k1gInSc7	beton
a	a	k8xC	a
polymery	polymer	k1gInPc1	polymer
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
úspěšně	úspěšně	k6eAd1	úspěšně
použito	použít	k5eAaPmNgNnS	použít
až	až	k9	až
tzv.	tzv.	kA	tzv.
tekuté	tekutý	k2eAgNnSc1d1	tekuté
sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zastavit	zastavit	k5eAaPmF	zastavit
unikání	unikání	k1gNnSc1	unikání
vysoce	vysoce	k6eAd1	vysoce
radioaktivní	radioaktivní	k2eAgFnSc2d1	radioaktivní
vody	voda	k1gFnSc2	voda
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
oceánu	oceán	k1gInSc2	oceán
řízeně	řízeně	k6eAd1	řízeně
vypuštěno	vypustit	k5eAaPmNgNnS	vypustit
11	[number]	k4	11
500	[number]	k4	500
tun	tuna	k1gFnPc2	tuna
mírně	mírně	k6eAd1	mírně
radioaktivní	radioaktivní	k2eAgFnSc2d1	radioaktivní
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
uvolnily	uvolnit	k5eAaPmAgFnP	uvolnit
prostory	prostora	k1gFnPc1	prostora
pro	pro	k7c4	pro
skladování	skladování	k1gNnSc4	skladování
vysoce	vysoce	k6eAd1	vysoce
radioaktivní	radioaktivní	k2eAgFnSc2d1	radioaktivní
vody	voda	k1gFnSc2	voda
unikající	unikající	k2eAgInSc4d1	unikající
z	z	k7c2	z
reaktorů	reaktor	k1gInPc2	reaktor
do	do	k7c2	do
okolních	okolní	k2eAgFnPc2d1	okolní
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
chlazení	chlazení	k1gNnSc6	chlazení
přehřátých	přehřátý	k2eAgInPc2d1	přehřátý
reaktorů	reaktor	k1gInPc2	reaktor
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
chladicích	chladicí	k2eAgInPc2d1	chladicí
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
několika	několik	k4yIc6	několik
reaktorech	reaktor	k1gInPc6	reaktor
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
roztavení	roztavení	k1gNnSc3	roztavení
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Japonský	japonský	k2eAgMnSc1d1	japonský
premiér	premiér	k1gMnSc1	premiér
Naoto	Naoto	k1gNnSc4	Naoto
Kan	Kan	k1gMnSc2	Kan
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektrárnu	elektrárna	k1gFnSc4	elektrárna
bude	být	k5eAaImBp3nS	být
potřeba	potřeba	k6eAd1	potřeba
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
již	již	k6eAd1	již
požádalo	požádat	k5eAaPmAgNnS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
s	s	k7c7	s
odstraňováním	odstraňování	k1gNnSc7	odstraňování
následků	následek	k1gInPc2	následek
havárie	havárie	k1gFnSc2	havárie
francouzské	francouzský	k2eAgFnSc2d1	francouzská
jaderné	jaderný	k2eAgFnSc2d1	jaderná
korporace	korporace	k1gFnSc2	korporace
Areva	Arevo	k1gNnSc2	Arevo
a	a	k8xC	a
EDF	EDF	kA	EDF
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
prací	práce	k1gFnPc2	práce
budou	být	k5eAaImBp3nP	být
zapojeni	zapojit	k5eAaPmNgMnP	zapojit
i	i	k9	i
odborníci	odborník	k1gMnPc1	odborník
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Poradce	poradce	k1gMnSc4	poradce
premiéra	premiér	k1gMnSc4	premiér
Keniči	Kenič	k1gMnPc1	Kenič
Macumoto	Macumota	k1gFnSc5	Macumota
sdělil	sdělit	k5eAaPmAgMnS	sdělit
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
médiím	médium	k1gNnPc3	médium
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
bude	být	k5eAaImBp3nS	být
20	[number]	k4	20
let	léto	k1gNnPc2	léto
nejbližší	blízký	k2eAgNnSc4d3	nejbližší
okolí	okolí	k1gNnSc4	okolí
elektrárny	elektrárna	k1gFnSc2	elektrárna
neobyvatelné	obyvatelný	k2eNgFnSc2d1	neobyvatelná
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
vláda	vláda	k1gFnSc1	vláda
však	však	k9	však
toto	tento	k3xDgNnSc4	tento
prohlášení	prohlášení	k1gNnSc4	prohlášení
záhy	záhy	k6eAd1	záhy
dementovala	dementovat	k5eAaBmAgFnS	dementovat
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
k	k	k7c3	k
odstraňování	odstraňování	k1gNnSc3	odstraňování
palivových	palivový	k2eAgInPc2d1	palivový
souborů	soubor	k1gInPc2	soubor
z	z	k7c2	z
bazénu	bazén	k1gInSc2	bazén
pro	pro	k7c4	pro
vyhořelé	vyhořelý	k2eAgNnSc4d1	vyhořelé
palivo	palivo	k1gNnSc4	palivo
na	na	k7c6	na
bloku	blok	k1gInSc6	blok
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgInSc1d1	aktuální
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
sledovat	sledovat	k5eAaImF	sledovat
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
TEPCO	TEPCO	kA	TEPCO
(	(	kIx(	(
<g/>
http://www.tepco.co.jp/en/decommision/index-e.html	[url]	k1gMnSc1	http://www.tepco.co.jp/en/decommision/index-e.html
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
probíhají	probíhat	k5eAaImIp3nP	probíhat
snahy	snaha	k1gFnPc4	snaha
o	o	k7c6	o
zastavení	zastavení	k1gNnSc6	zastavení
úniků	únik	k1gInPc2	únik
podzemní	podzemní	k2eAgFnSc2d1	podzemní
radioaktivní	radioaktivní	k2eAgFnSc2d1	radioaktivní
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
havárii	havárie	k1gFnSc6	havárie
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
zdraví	zdraví	k1gNnSc2	zdraví
škodlivých	škodlivý	k2eAgInPc2d1	škodlivý
štěpných	štěpný	k2eAgInPc2d1	štěpný
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc1d1	přesné
množství	množství	k1gNnSc1	množství
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
stanovit	stanovit	k5eAaPmF	stanovit
a	a	k8xC	a
odhady	odhad	k1gInPc1	odhad
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
různí	různit	k5eAaImIp3nP	různit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
studie	studie	k1gFnSc2	studie
došlo	dojít	k5eAaPmAgNnS	dojít
např.	např.	kA	např.
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
až	až	k9	až
20	[number]	k4	20
500	[number]	k4	500
TBq	TBq	k1gFnSc1	TBq
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
césia-	césia-	k?	césia-
<g/>
137	[number]	k4	137
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nepočítáme	počítat	k5eNaImIp1nP	počítat
stále	stále	k6eAd1	stále
probíhající	probíhající	k2eAgInPc1d1	probíhající
úniky	únik	k1gInPc1	únik
do	do	k7c2	do
podzemních	podzemní	k2eAgFnPc2d1	podzemní
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
do	do	k7c2	do
konce	konec	k1gInSc2	konec
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
99	[number]	k4	99
<g/>
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgNnSc2d1	celkové
množství	množství	k1gNnSc2	množství
uniklých	uniklý	k2eAgInPc2d1	uniklý
štěpných	štěpný	k2eAgInPc2d1	štěpný
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
300	[number]	k4	300
m3	m3	k4	m3
radioaktivní	radioaktivní	k2eAgFnSc2d1	radioaktivní
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
stupnici	stupnice	k1gFnSc4	stupnice
INES	INES	kA	INES
ohodnoceno	ohodnotit	k5eAaPmNgNnS	ohodnotit
stupněm	stupeň	k1gInSc7	stupeň
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
japonského	japonský	k2eAgNnSc2d1	Japonské
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
naměřit	naměřit	k5eAaBmF	naměřit
roční	roční	k2eAgFnSc4d1	roční
dávku	dávka	k1gFnSc4	dávka
radiačního	radiační	k2eAgNnSc2d1	radiační
záření	záření	k1gNnSc2	záření
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
5	[number]	k4	5
mSv	mSv	k?	mSv
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
zhruba	zhruba	k6eAd1	zhruba
1800	[number]	k4	1800
km2	km2	k4	km2
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
havárie	havárie	k1gFnSc2	havárie
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
znehodnocení	znehodnocení	k1gNnSc3	znehodnocení
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
okolí	okolí	k1gNnSc6	okolí
elektrárny	elektrárna	k1gFnSc2	elektrárna
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
césiem-	césiem-	k?	césiem-
<g/>
137	[number]	k4	137
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Znehodnocení	znehodnocení	k1gNnSc1	znehodnocení
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
zapříčiněno	zapříčinit	k5eAaPmNgNnS	zapříčinit
odporem	odpor	k1gInSc7	odpor
obyvatel	obyvatel	k1gMnPc2	obyvatel
k	k	k7c3	k
produktům	produkt	k1gInPc3	produkt
z	z	k7c2	z
širokého	široký	k2eAgNnSc2d1	široké
okolí	okolí	k1gNnSc2	okolí
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
omezit	omezit	k5eAaPmF	omezit
rybolov	rybolov	k1gInSc4	rybolov
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
chycené	chycený	k2eAgFnPc1d1	chycená
ryby	ryba	k1gFnPc1	ryba
vykazovaly	vykazovat	k5eAaImAgFnP	vykazovat
zvýšené	zvýšený	k2eAgFnPc1d1	zvýšená
úrovně	úroveň	k1gFnPc1	úroveň
radiace	radiace	k1gFnPc1	radiace
–	–	k?	–
příkladem	příklad	k1gInSc7	příklad
budiž	budiž	k9	budiž
ryba	ryba	k1gFnSc1	ryba
chycená	chycený	k2eAgFnSc1d1	chycená
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
740	[number]	k4	740
000	[number]	k4	000
Bq	Bq	k1gFnPc2	Bq
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
radioaktivního	radioaktivní	k2eAgNnSc2d1	radioaktivní
césia	césium	k1gNnSc2	césium
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
překračující	překračující	k2eAgFnSc2d1	překračující
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
limity	limita	k1gFnSc2	limita
7400	[number]	k4	7400
–	–	k?	–
krát	krát	k6eAd1	krát
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
hodnoty	hodnota	k1gFnSc2	hodnota
radiace	radiace	k1gFnPc4	radiace
naměřené	naměřený	k2eAgFnPc4d1	naměřená
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
k	k	k7c3	k
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
v	v	k7c6	v
zemědělských	zemědělský	k2eAgInPc6d1	zemědělský
produktech	produkt	k1gInPc6	produkt
z	z	k7c2	z
prefektury	prefektura	k1gFnSc2	prefektura
Fukušima	Fukušimum	k1gNnSc2	Fukušimum
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
bezpečně	bezpečně	k6eAd1	bezpečně
pod	pod	k7c4	pod
zdraví	zdraví	k1gNnSc6	zdraví
nebezpečnými	bezpečný	k2eNgFnPc7d1	nebezpečná
hodnotami	hodnota	k1gFnPc7	hodnota
<g/>
,	,	kIx,	,
strach	strach	k1gInSc1	strach
stále	stále	k6eAd1	stále
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
zhoršující	zhoršující	k2eAgInSc4d1	zhoršující
se	se	k3xPyFc4	se
stav	stav	k1gInSc4	stav
havárie	havárie	k1gFnSc2	havárie
vydal	vydat	k5eAaPmAgMnS	vydat
japonský	japonský	k2eAgMnSc1d1	japonský
premiér	premiér	k1gMnSc1	premiér
v	v	k7c4	v
den	den	k1gInSc4	den
havárie	havárie	k1gFnSc1	havárie
příkaz	příkaz	k1gInSc1	příkaz
k	k	k7c3	k
evakuaci	evakuace	k1gFnSc3	evakuace
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
3	[number]	k4	3
km	km	kA	km
od	od	k7c2	od
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Okruh	okruh	k1gInSc1	okruh
byl	být	k5eAaImAgInS	být
dále	daleko	k6eAd2	daleko
navýšen	navýšit	k5eAaPmNgInS	navýšit
na	na	k7c4	na
10	[number]	k4	10
km	km	kA	km
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
na	na	k7c4	na
20	[number]	k4	20
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pokaždé	pokaždé	k6eAd1	pokaždé
<g/>
,	,	kIx,	,
když	když	k8xS	když
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
evakuačního	evakuační	k2eAgNnSc2d1	evakuační
pásma	pásmo	k1gNnSc2	pásmo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
obyvatelé	obyvatel	k1gMnPc1	obyvatel
znovu	znovu	k6eAd1	znovu
přemisťovat	přemisťovat	k5eAaImF	přemisťovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
vyčerpání	vyčerpání	k1gNnSc3	vyčerpání
a	a	k8xC	a
následně	následně	k6eAd1	následně
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
jež	jenž	k3xRgMnPc4	jenž
trpěli	trpět	k5eAaImAgMnP	trpět
různými	různý	k2eAgFnPc7d1	různá
chronickými	chronický	k2eAgFnPc7d1	chronická
nemocemi	nemoc	k1gFnPc7	nemoc
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
již	již	k6eAd1	již
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
pokročilém	pokročilý	k2eAgInSc6d1	pokročilý
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
nařízení	nařízení	k1gNnSc1	nařízení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
obyvatelé	obyvatel	k1gMnPc1	obyvatel
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
20-30	[number]	k4	20-30
km	km	kA	km
od	od	k7c2	od
elektrárny	elektrárna	k1gFnSc2	elektrárna
zůstali	zůstat	k5eAaPmAgMnP	zůstat
doma	doma	k6eAd1	doma
a	a	k8xC	a
nevycházeli	vycházet	k5eNaImAgMnP	vycházet
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nařízení	nařízení	k1gNnSc1	nařízení
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
až	až	k9	až
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
následkem	následkem	k7c2	následkem
čehož	což	k3yQnSc2	což
byli	být	k5eAaImAgMnP	být
lidé	člověk	k1gMnPc1	člověk
úplně	úplně	k6eAd1	úplně
odříznuti	odříznout	k5eAaPmNgMnP	odříznout
od	od	k7c2	od
okolí	okolí	k1gNnSc2	okolí
takřka	takřka	k6eAd1	takřka
bez	bez	k7c2	bez
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
pomoci	pomoc	k1gFnSc2	pomoc
zvenčí	zvenčí	k6eAd1	zvenčí
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
vydala	vydat	k5eAaPmAgFnS	vydat
vláda	vláda	k1gFnSc1	vláda
doporučení	doporučení	k1gNnSc4	doporučení
k	k	k7c3	k
dobrovolné	dobrovolný	k2eAgFnSc3d1	dobrovolná
evakuaci	evakuace	k1gFnSc3	evakuace
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
opět	opět	k6eAd1	opět
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
dalších	další	k2eAgFnPc2d1	další
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
konečné	konečný	k2eAgNnSc1d1	konečné
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
úsudku	úsudek	k1gInSc6	úsudek
každého	každý	k3xTgMnSc2	každý
obyvatele	obyvatel	k1gMnSc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgInPc4d1	přesný
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
dopady	dopad	k1gInPc4	dopad
na	na	k7c4	na
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
zatím	zatím	k6eAd1	zatím
nejsou	být	k5eNaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
předběžných	předběžný	k2eAgInPc2d1	předběžný
průzkumů	průzkum	k1gInPc2	průzkum
bylo	být	k5eAaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
záření	záření	k1gNnSc1	záření
vyššímu	vysoký	k2eAgInSc3d2	vyšší
jak	jak	k8xC	jak
10	[number]	k4	10
mSv	mSv	k?	mSv
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
z	z	k7c2	z
časopisu	časopis	k1gInSc2	časopis
National	National	k1gMnSc1	National
Geographic	Geographic	k1gMnSc1	Geographic
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
výskyt	výskyt	k1gInSc4	výskyt
rakoviny	rakovina	k1gFnSc2	rakovina
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
–	–	k?	–
33	[number]	k4	33
výskytů	výskyt	k1gInPc2	výskyt
rakoviny	rakovina	k1gFnSc2	rakovina
u	u	k7c2	u
zhruba	zhruba	k6eAd1	zhruba
350	[number]	k4	350
000	[number]	k4	000
testovaných	testovaný	k2eAgFnPc2d1	testovaná
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
typický	typický	k2eAgInSc1d1	typický
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
1-2	[number]	k4	1-2
nemoci	nemoc	k1gFnSc2	nemoc
na	na	k7c4	na
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
dětí	dítě	k1gFnPc2	dítě
–	–	k?	–
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
nutně	nutně	k6eAd1	nutně
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
havárií	havárie	k1gFnSc7	havárie
na	na	k7c6	na
Fukušimě	Fukušima	k1gFnSc6	Fukušima
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
důsledkem	důsledek	k1gInSc7	důsledek
přísných	přísný	k2eAgFnPc2d1	přísná
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
prohlídek	prohlídka	k1gFnPc2	prohlídka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
prováděny	provádět	k5eAaImNgInP	provádět
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
havárii	havárie	k1gFnSc6	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dostupných	dostupný	k2eAgFnPc2d1	dostupná
informací	informace	k1gFnPc2	informace
trápí	trápit	k5eAaImIp3nP	trápit
evakuované	evakuovaný	k2eAgMnPc4d1	evakuovaný
obyvatele	obyvatel	k1gMnPc4	obyvatel
spíše	spíše	k9	spíše
psychické	psychický	k2eAgFnSc2d1	psychická
než	než	k8xS	než
fyzické	fyzický	k2eAgInPc4d1	fyzický
problémy	problém	k1gInPc4	problém
–	–	k?	–
například	například	k6eAd1	například
v	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
Hirono	Hirona	k1gFnSc5	Hirona
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
leží	ležet	k5eAaImIp3nP	ležet
nedaleko	nedaleko	k7c2	nedaleko
Fukušimy	Fukušima	k1gFnSc2	Fukušima
<g/>
,	,	kIx,	,
trpí	trpět	k5eAaImIp3nS	trpět
až	až	k9	až
polovina	polovina	k1gFnSc1	polovina
obyvatel	obyvatel	k1gMnSc1	obyvatel
posttraumatickým	posttraumatický	k2eAgInSc7d1	posttraumatický
stresovým	stresový	k2eAgInSc7d1	stresový
syndromem	syndrom	k1gInSc7	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
říjnu	říjen	k1gInSc6	říjen
2014	[number]	k4	2014
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
rozloha	rozloha	k1gFnSc1	rozloha
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
zóny	zóna	k1gFnSc2	zóna
mírně	mírně	k6eAd1	mírně
omezena	omezen	k2eAgFnSc1d1	omezena
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
dovolila	dovolit	k5eAaPmAgFnS	dovolit
obyvatelům	obyvatel	k1gMnPc3	obyvatel
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
domovů	domov	k1gInPc2	domov
<g/>
.	.	kIx.	.
</s>
<s>
Vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
však	však	k9	však
hodlá	hodlat	k5eAaImIp3nS	hodlat
méně	málo	k6eAd2	málo
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
evakuovaných	evakuovaný	k2eAgNnPc2d1	evakuované
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
březnu	březen	k1gInSc3	březen
2015	[number]	k4	2015
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
téměř	téměř	k6eAd1	téměř
120	[number]	k4	120
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
evakuovaných	evakuovaný	k2eAgFnPc2d1	evakuovaná
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
téměř	téměř	k6eAd1	téměř
50	[number]	k4	50
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
již	již	k6eAd1	již
žije	žít	k5eAaImIp3nS	žít
mimo	mimo	k7c4	mimo
Prefekturu	prefektura	k1gFnSc4	prefektura
Fukušima	Fukušimum	k1gNnSc2	Fukušimum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
byla	být	k5eAaImAgFnS	být
před	před	k7c4	před
havárii	havárie	k1gFnSc4	havárie
jaderná	jaderný	k2eAgFnSc1d1	jaderná
energetika	energetika	k1gFnSc1	energetika
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xC	jako
nutná	nutný	k2eAgFnSc1d1	nutná
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
plány	plán	k1gInPc1	plán
počítaly	počítat	k5eAaImAgInP	počítat
se	s	k7c7	s
zvýšením	zvýšení	k1gNnSc7	zvýšení
jejího	její	k3xOp3gInSc2	její
podílu	podíl	k1gInSc2	podíl
v	v	k7c6	v
celkovém	celkový	k2eAgInSc6d1	celkový
energetickém	energetický	k2eAgInSc6d1	energetický
mixu	mix	k1gInSc6	mix
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
tvořila	tvořit	k5eAaImAgFnS	tvořit
elektřina	elektřina	k1gFnSc1	elektřina
generovaná	generovaný	k2eAgFnSc1d1	generovaná
v	v	k7c6	v
jaderných	jaderný	k2eAgFnPc6d1	jaderná
elektrárnách	elektrárna	k1gFnPc6	elektrárna
30	[number]	k4	30
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgNnSc2d1	celkové
množství	množství	k1gNnSc2	množství
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
plány	plán	k1gInPc1	plán
počítaly	počítat	k5eAaImAgInP	počítat
s	s	k7c7	s
navýšením	navýšení	k1gNnSc7	navýšení
tohoto	tento	k3xDgInSc2	tento
podílu	podíl	k1gInSc2	podíl
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2030	[number]	k4	2030
na	na	k7c4	na
50	[number]	k4	50
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
chtělo	chtít	k5eAaImAgNnS	chtít
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
zbavit	zbavit	k5eAaPmF	zbavit
závislosti	závislost	k1gFnSc3	závislost
na	na	k7c6	na
dovážených	dovážený	k2eAgNnPc6d1	dovážené
fosilních	fosilní	k2eAgNnPc6d1	fosilní
palivech	palivo	k1gNnPc6	palivo
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zvýšit	zvýšit	k5eAaPmF	zvýšit
svou	svůj	k3xOyFgFnSc4	svůj
energetickou	energetický	k2eAgFnSc4d1	energetická
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
jaderná	jaderný	k2eAgFnSc1d1	jaderná
energetika	energetika	k1gFnSc1	energetika
jen	jen	k9	jen
10	[number]	k4	10
%	%	kIx~	%
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
spotřebované	spotřebovaný	k2eAgFnSc2d1	spotřebovaná
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
energie	energie	k1gFnSc1	energie
z	z	k7c2	z
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
85	[number]	k4	85
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
započalo	započnout	k5eAaPmAgNnS	započnout
postupné	postupný	k2eAgNnSc4d1	postupné
odstavování	odstavování	k1gNnSc4	odstavování
všech	všecek	k3xTgInPc2	všecek
54	[number]	k4	54
japonských	japonský	k2eAgInPc2d1	japonský
jaderných	jaderný	k2eAgInPc2d1	jaderný
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
reaktory	reaktor	k1gInPc1	reaktor
odstaveny	odstaven	k2eAgInPc1d1	odstaven
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odstávce	odstávka	k1gFnSc3	odstávka
všech	všecek	k3xTgInPc2	všecek
reaktorů	reaktor	k1gInPc2	reaktor
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byly	být	k5eAaImAgFnP	být
obavy	obava	k1gFnPc1	obava
o	o	k7c4	o
nedostatečnou	dostatečný	k2eNgFnSc4d1	nedostatečná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
elektráren	elektrárna	k1gFnPc2	elektrárna
a	a	k8xC	a
protesty	protest	k1gInPc4	protest
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
vůči	vůči	k7c3	vůči
jaderné	jaderný	k2eAgFnSc3d1	jaderná
energii	energie	k1gFnSc3	energie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odstavení	odstavení	k1gNnSc6	odstavení
reaktorů	reaktor	k1gInPc2	reaktor
muselo	muset	k5eAaImAgNnS	muset
Japonsko	Japonsko	k1gNnSc1	Japonsko
nahradit	nahradit	k5eAaPmF	nahradit
tento	tento	k3xDgInSc4	tento
ztracený	ztracený	k2eAgInSc4d1	ztracený
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc1	energie
zvýšením	zvýšení	k1gNnSc7	zvýšení
množství	množství	k1gNnSc1	množství
importovaných	importovaný	k2eAgNnPc2d1	importované
paliv	palivo	k1gNnPc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
zaplatilo	zaplatit	k5eAaPmAgNnS	zaplatit
Japonsko	Japonsko	k1gNnSc1	Japonsko
za	za	k7c4	za
dovoz	dovoz	k1gInSc4	dovoz
paliv	palivo	k1gNnPc2	palivo
250	[number]	k4	250
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
(	(	kIx(	(
<g/>
5,5	[number]	k4	5,5
bilionu	bilion	k4xCgInSc2	bilion
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
povolení	povolení	k1gNnSc1	povolení
ke	k	k7c3	k
znovu	znovu	k6eAd1	znovu
spuštění	spuštění	k1gNnSc1	spuštění
některých	některý	k3yIgInPc2	některý
bloků	blok	k1gInPc2	blok
JE	být	k5eAaImIp3nS	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Japonský	japonský	k2eAgInSc1d1	japonský
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
institut	institut	k1gInSc1	institut
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
11	[number]	k4	11
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podporuje	podporovat	k5eAaImIp3nS	podporovat
i	i	k9	i
změna	změna	k1gFnSc1	změna
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
<g/>
.	.	kIx.	.
</s>
<s>
Havárie	havárie	k1gFnSc1	havárie
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
elektrárně	elektrárna	k1gFnSc6	elektrárna
Fukušima	Fukušimum	k1gNnSc2	Fukušimum
I	i	k9	i
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
politickou	politický	k2eAgFnSc4d1	politická
diskuzi	diskuze	k1gFnSc4	diskuze
o	o	k7c6	o
dalším	další	k2eAgNnSc6d1	další
využívání	využívání	k1gNnSc6	využívání
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
tato	tento	k3xDgFnSc1	tento
havárie	havárie	k1gFnSc1	havárie
způsobila	způsobit	k5eAaPmAgFnS	způsobit
obrat	obrat	k1gInSc4	obrat
v	v	k7c6	v
energetické	energetický	k2eAgFnSc6d1	energetická
koncepci	koncepce	k1gFnSc6	koncepce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vláda	vláda	k1gFnSc1	vláda
neprodloužila	prodloužit	k5eNaPmAgFnS	prodloužit
jaderným	jaderný	k2eAgFnPc3d1	jaderná
elektrárnám	elektrárna	k1gFnPc3	elektrárna
provoz	provoz	k1gInSc4	provoz
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
po	po	k7c6	po
bezpečnostních	bezpečnostní	k2eAgFnPc6d1	bezpečnostní
prověrkách	prověrka	k1gFnPc6	prověrka
bylo	být	k5eAaImAgNnS	být
7	[number]	k4	7
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
dočasně	dočasně	k6eAd1	dočasně
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
způsobil	způsobit	k5eAaPmAgInS	způsobit
růst	růst	k1gInSc4	růst
cen	cena	k1gFnPc2	cena
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
na	na	k7c6	na
německém	německý	k2eAgInSc6d1	německý
trhu	trh	k1gInSc6	trh
až	až	k9	až
o	o	k7c4	o
18	[number]	k4	18
%	%	kIx~	%
již	již	k6eAd1	již
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Bavorský	bavorský	k2eAgInSc4d1	bavorský
odpor	odpor	k1gInSc4	odpor
vůči	vůči	k7c3	vůči
atomu	atom	k1gInSc3	atom
obnovil	obnovit	k5eAaPmAgMnS	obnovit
i	i	k9	i
snahy	snaha	k1gFnPc4	snaha
o	o	k7c6	o
odstavení	odstavení	k1gNnSc6	odstavení
JE	být	k5eAaImIp3nS	být
Temelín	Temelín	k1gInSc1	Temelín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
možném	možný	k2eAgInSc6d1	možný
a	a	k8xC	a
možná	možná	k9	možná
i	i	k8xC	i
nutném	nutný	k2eAgNnSc6d1	nutné
prodloužení	prodloužení	k1gNnSc6	prodloužení
provozu	provoz	k1gInSc2	provoz
některých	některý	k3yIgFnPc2	některý
jejich	jejich	k3xOp3gFnPc2	jejich
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2022	[number]	k4	2022
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
Japonsku	Japonsko	k1gNnSc6	Japonsko
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
některých	některý	k3yIgInPc2	některý
projektů	projekt	k1gInPc2	projekt
nových	nový	k2eAgInPc2d1	nový
bloků	blok	k1gInPc2	blok
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
a	a	k8xC	a
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
bude	být	k5eAaImBp3nS	být
ukončen	ukončen	k2eAgInSc1d1	ukončen
i	i	k9	i
japonský	japonský	k2eAgInSc1d1	japonský
projekt	projekt	k1gInSc1	projekt
rychlého	rychlý	k2eAgInSc2d1	rychlý
reaktoru	reaktor	k1gInSc2	reaktor
Mondžú	Mondžú	k1gFnSc2	Mondžú
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Curuga	Curuga	k1gFnSc1	Curuga
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
již	již	k6eAd1	již
dlouho	dlouho	k6eAd1	dlouho
provázejí	provázet	k5eAaImIp3nP	provázet
nehody	nehoda	k1gFnPc1	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Reaktor	reaktor	k1gInSc1	reaktor
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
dohromady	dohromady	k6eAd1	dohromady
asi	asi	k9	asi
jen	jen	k9	jen
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Zrušení	zrušení	k1gNnSc1	zrušení
projektu	projekt	k1gInSc2	projekt
ale	ale	k8xC	ale
bude	být	k5eAaImBp3nS	být
znamenat	znamenat	k5eAaImF	znamenat
velkou	velký	k2eAgFnSc4d1	velká
ránu	rána	k1gFnSc4	rána
pro	pro	k7c4	pro
japonskou	japonský	k2eAgFnSc4d1	japonská
energetickou	energetický	k2eAgFnSc4d1	energetická
koncepci	koncepce	k1gFnSc4	koncepce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
s	s	k7c7	s
přechodem	přechod	k1gInSc7	přechod
na	na	k7c4	na
rychlé	rychlý	k2eAgInPc4d1	rychlý
reaktory	reaktor	k1gInPc4	reaktor
se	se	k3xPyFc4	se
počítalo	počítat	k5eAaImAgNnS	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
japonské	japonský	k2eAgFnSc2d1	japonská
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
katastrofu	katastrofa	k1gFnSc4	katastrofa
nezavinila	zavinit	k5eNaPmAgFnS	zavinit
přílivová	přílivový	k2eAgFnSc1d1	přílivová
vlna	vlna	k1gFnSc1	vlna
tsunami	tsunami	k1gNnSc2	tsunami
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
havárii	havárie	k1gFnSc4	havárie
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
ji	on	k3xPp3gFnSc4	on
možné	možný	k2eAgMnPc4d1	možný
předvídat	předvídat	k5eAaImF	předvídat
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
předvídána	předvídat	k5eAaImNgFnS	předvídat
<g/>
.	.	kIx.	.
</s>
<s>
Dalo	dát	k5eAaPmAgNnS	dát
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
zabránit	zabránit	k5eAaPmF	zabránit
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
uvádí	uvádět	k5eAaImIp3nS	uvádět
komise	komise	k1gFnSc1	komise
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
nekvalitní	kvalitní	k2eNgInPc4d1	nekvalitní
zákony	zákon	k1gInPc4	zákon
regulující	regulující	k2eAgInSc4d1	regulující
jadernou	jaderný	k2eAgFnSc4d1	jaderná
energetiku	energetika	k1gFnSc4	energetika
<g/>
,	,	kIx,	,
nedostatečnost	nedostatečnost	k1gFnSc4	nedostatečnost
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
chyby	chyba	k1gFnPc4	chyba
vedení	vedení	k1gNnSc2	vedení
elektrárny	elektrárna	k1gFnSc2	elektrárna
a	a	k8xC	a
státního	státní	k2eAgInSc2d1	státní
dozoru	dozor	k1gInSc2	dozor
i	i	k8xC	i
špatné	špatný	k2eAgFnSc2d1	špatná
reakce	reakce	k1gFnSc2	reakce
po	po	k7c6	po
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
proto	proto	k8xC	proto
doporučila	doporučit	k5eAaPmAgFnS	doporučit
posílit	posílit	k5eAaPmF	posílit
dozor	dozor	k1gInSc4	dozor
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energetiky	energetika	k1gFnSc2	energetika
<g/>
.	.	kIx.	.
</s>
