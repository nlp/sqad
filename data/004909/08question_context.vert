<s>
Havárie	havárie	k1gFnSc1
japonské	japonský	k2eAgFnSc2d1
jaderné	jaderný	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
Fukušima	Fukušima	k1gNnSc2
I	i	k8xC
společnosti	společnost	k1gFnSc2
TEPCO	TEPCO	kA
v	v	k7c6
roce	rok	k1gInSc6
2011	[number]	k4
byla	být	k5eAaImAgFnS
nejhorší	zlý	k2eAgFnSc1d3
jaderná	jaderný	k2eAgFnSc1d1
havárie	havárie	k1gFnSc1
od	od	k7c2
Černobylu	Černobyl	k1gInSc2
1986	[number]	k4
a	a	k8xC
po	po	k7c6
ní	on	k3xPp3gFnSc6
jediná	jediný	k2eAgFnSc1d1
další	další	k2eAgFnSc1d1
havárie	havárie	k1gFnSc1
označená	označený	k2eAgFnSc1d1
na	na	k7c6
stupnici	stupnice	k1gFnSc6
INES	INES	kA
nejvyšším	vysoký	k2eAgInSc7d3
stupněm	stupeň	k1gInSc7
7	[number]	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Jaderná	jaderný	k2eAgFnSc1d1
elektrárna	elektrárna	k1gFnSc1
Fukušima	Fukušima	k1gFnSc1
I.	I.	kA
Jaderná	jaderný	k2eAgFnSc1d1
elektrárna	elektrárna	k1gFnSc1
Fukušima	Fukušima	k1gNnSc2
leží	ležet	k5eAaImIp3nS
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>