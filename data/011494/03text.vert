<p>
<s>
Brooklynský	brooklynský	k2eAgInSc1d1	brooklynský
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
and	and	k?	and
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
Bridge	Bridge	k1gInSc1	Bridge
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
visutých	visutý	k2eAgInPc2d1	visutý
mostů	most	k1gInPc2	most
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
486,3	[number]	k4	486,3
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
East	East	k2eAgInSc4d1	East
River	River	k1gInSc4	River
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
-	-	kIx~	-
Manhattan	Manhattan	k1gInSc1	Manhattan
a	a	k8xC	a
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	on	k3xPp3gNnSc2	on
otevření	otevření	k1gNnSc2	otevření
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
visutý	visutý	k2eAgInSc4d1	visutý
most	most	k1gInSc4	most
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
první	první	k4xOgInSc4	první
visutý	visutý	k2eAgInSc4d1	visutý
most	most	k1gInSc4	most
na	na	k7c6	na
ocelových	ocelový	k2eAgNnPc6d1	ocelové
lanech	lano	k1gNnPc6	lano
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
mostní	mostní	k2eAgFnPc1d1	mostní
věže	věž	k1gFnPc1	věž
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nejvyššími	vysoký	k2eAgFnPc7d3	nejvyšší
stavbami	stavba	k1gFnPc7	stavba
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
začala	začít	k5eAaPmAgFnS	začít
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
po	po	k7c6	po
13	[number]	k4	13
letech	let	k1gInPc6	let
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1883	[number]	k4	1883
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
otevření	otevření	k1gNnSc2	otevření
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
symbolem	symbol	k1gInSc7	symbol
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
národní	národní	k2eAgFnPc4d1	národní
historické	historický	k2eAgFnPc4d1	historická
památky	památka	k1gFnPc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
most	most	k1gInSc4	most
také	také	k9	také
vede	vést	k5eAaImIp3nS	vést
trasa	trasa	k1gFnSc1	trasa
Newyorského	newyorský	k2eAgInSc2d1	newyorský
maratonu	maraton	k1gInSc2	maraton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Výstavba	výstavba	k1gFnSc1	výstavba
===	===	k?	===
</s>
</p>
<p>
<s>
Most	most	k1gInSc1	most
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vybudovat	vybudovat	k5eAaPmF	vybudovat
John	John	k1gMnSc1	John
Augustus	Augustus	k1gMnSc1	Augustus
Roebling	Roebling	k1gInSc4	Roebling
<g/>
.	.	kIx.	.
</s>
<s>
Snil	snít	k5eAaImAgMnS	snít
o	o	k7c6	o
Brooklynském	brooklynský	k2eAgInSc6d1	brooklynský
mostě	most	k1gInSc6	most
jako	jako	k8xC	jako
svém	svůj	k3xOyFgNnSc6	svůj
největším	veliký	k2eAgNnSc6d3	veliký
díle	dílo	k1gNnSc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
strašné	strašný	k2eAgFnSc6d1	strašná
zimě	zima	k1gFnSc6	zima
1866	[number]	k4	1866
-	-	kIx~	-
1867	[number]	k4	1867
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zkolabovala	zkolabovat	k5eAaPmAgFnS	zkolabovat
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
všechny	všechen	k3xTgFnPc1	všechen
lodě	loď	k1gFnPc1	loď
zamrzly	zamrznout	k5eAaPmAgFnP	zamrznout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
postaví	postavit	k5eAaPmIp3nS	postavit
most	most	k1gInSc1	most
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
East	East	k2eAgInSc4d1	East
River	River	k1gInSc4	River
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
schválena	schválen	k2eAgFnSc1d1	schválena
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
začali	začít	k5eAaPmAgMnP	začít
vybírat	vybírat	k5eAaImF	vybírat
místo	místo	k7c2	místo
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
při	při	k7c6	při
poslední	poslední	k2eAgFnSc6d1	poslední
kontrole	kontrola	k1gFnSc6	kontrola
místa	místo	k1gNnSc2	místo
si	se	k3xPyFc3	se
Roebling	Roebling	k1gInSc1	Roebling
nevšiml	všimnout	k5eNaPmAgInS	všimnout
připlouvající	připlouvající	k2eAgFnPc4d1	připlouvající
lodi	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gNnSc3	on
při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
do	do	k7c2	do
mola	molo	k1gNnSc2	molo
rozdrtila	rozdrtit	k5eAaPmAgFnS	rozdrtit
nohu	noha	k1gFnSc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Museli	muset	k5eAaImAgMnP	muset
mu	on	k3xPp3gMnSc3	on
amputovat	amputovat	k5eAaBmF	amputovat
prsty	prst	k1gInPc4	prst
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
infekci	infekce	k1gFnSc6	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Washington	Washington	k1gInSc1	Washington
Roebling	Roebling	k1gInSc1	Roebling
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
vybudování	vybudování	k1gNnSc4	vybudování
základů	základ	k1gInPc2	základ
věží	věž	k1gFnPc2	věž
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
hladiny	hladina	k1gFnSc2	hladina
vody	voda	k1gFnSc2	voda
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
kesony	keson	k1gInPc1	keson
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
zvon	zvon	k1gInSc1	zvon
se	se	k3xPyFc4	se
spustil	spustit	k5eAaPmAgInS	spustit
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
spodní	spodní	k2eAgFnSc1d1	spodní
hrana	hrana	k1gFnSc1	hrana
zařízla	zaříznout	k5eAaPmAgFnS	zaříznout
do	do	k7c2	do
dna	dno	k1gNnSc2	dno
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
vrchní	vrchní	k2eAgFnSc1d1	vrchní
část	část	k1gFnSc1	část
vyčnívala	vyčnívat	k5eAaImAgFnS	vyčnívat
nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
odčerpala	odčerpat	k5eAaPmAgFnS	odčerpat
voda	voda	k1gFnSc1	voda
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vtlačen	vtlačit	k5eAaPmNgInS	vtlačit
do	do	k7c2	do
zvonu	zvon	k1gInSc2	zvon
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
vytlačil	vytlačit	k5eAaPmAgMnS	vytlačit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Věže	věž	k1gFnPc1	věž
jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgInP	postavit
z	z	k7c2	z
vápence	vápenec	k1gInSc2	vápenec
<g/>
,	,	kIx,	,
žuly	žula	k1gFnSc2	žula
a	a	k8xC	a
cementu	cement	k1gInSc2	cement
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgInP	postavit
v	v	k7c6	v
novogotickém	novogotický	k2eAgInSc6d1	novogotický
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
s	s	k7c7	s
charakteristickými	charakteristický	k2eAgInPc7d1	charakteristický
lomenými	lomený	k2eAgInPc7d1	lomený
oblouky	oblouk	k1gInPc7	oblouk
ve	v	k7c6	v
věžích	věž	k1gFnPc6	věž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
výstavby	výstavba	k1gFnSc2	výstavba
celého	celý	k2eAgInSc2d1	celý
mostu	most	k1gInSc2	most
byla	být	k5eAaImAgFnS	být
vyčíslena	vyčíslit	k5eAaPmNgFnS	vyčíslit
na	na	k7c6	na
tehdy	tehdy	k6eAd1	tehdy
neuvěřitelných	uvěřitelný	k2eNgInPc6d1	neuvěřitelný
15,5	[number]	k4	15,5
miliónu	milión	k4xCgInSc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
moc	moc	k6eAd1	moc
nehledělo	hledět	k5eNaImAgNnS	hledět
na	na	k7c4	na
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
zdraví	zdraví	k1gNnSc4	zdraví
dělníků	dělník	k1gMnPc2	dělník
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
také	také	k9	také
nebyl	být	k5eNaImAgInS	být
dostatečně	dostatečně	k6eAd1	dostatečně
znám	znám	k2eAgInSc4d1	znám
princip	princip	k1gInSc4	princip
a	a	k8xC	a
možnosti	možnost	k1gFnPc4	možnost
kompenzace	kompenzace	k1gFnSc2	kompenzace
kesonové	kesonový	k2eAgFnPc4d1	kesonová
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
27	[number]	k4	27
lidí	člověk	k1gMnPc2	člověk
během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
otevření	otevření	k1gNnSc1	otevření
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1883	[number]	k4	1883
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
13	[number]	k4	13
letech	léto	k1gNnPc6	léto
stavby	stavba	k1gFnSc2	stavba
most	most	k1gInSc1	most
dokončen	dokončit	k5eAaPmNgInS	dokončit
a	a	k8xC	a
otevřen	otevřít	k5eAaPmNgInS	otevřít
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
u	u	k7c2	u
mostu	most	k1gInSc2	most
sešly	sejít	k5eAaPmAgInP	sejít
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
přivítal	přivítat	k5eAaPmAgMnS	přivítat
newyorský	newyorský	k2eAgMnSc1d1	newyorský
starosta	starosta	k1gMnSc1	starosta
Franklin	Franklin	k2eAgInSc4d1	Franklin
Edson	Edson	k1gInSc4	Edson
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
21	[number]	k4	21
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Chester	Chester	k1gMnSc1	Chester
Arthur	Arthur	k1gMnSc1	Arthur
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
přecházeli	přecházet	k5eAaImAgMnP	přecházet
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
s	s	k7c7	s
brooklynským	brooklynský	k2eAgMnSc7d1	brooklynský
starostou	starosta	k1gMnSc7	starosta
Sethem	Seth	k1gInSc7	Seth
Lowem	Low	k1gInSc7	Low
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
čekal	čekat	k5eAaImAgMnS	čekat
pod	pod	k7c4	pod
mostní	mostní	k2eAgFnPc4d1	mostní
věží	věžit	k5eAaImIp3nP	věžit
na	na	k7c6	na
brooklynském	brooklynský	k2eAgInSc6d1	brooklynský
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Washington	Washington	k1gInSc1	Washington
Roebling	Roebling	k1gInSc1	Roebling
se	se	k3xPyFc4	se
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
otevření	otevření	k1gNnSc2	otevření
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
konal	konat	k5eAaImAgInS	konat
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
doma	doma	k6eAd1	doma
večírek	večírek	k1gInSc4	večírek
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
otevření	otevření	k1gNnSc2	otevření
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
most	most	k1gInSc1	most
využilo	využít	k5eAaPmAgNnS	využít
asi	asi	k9	asi
1	[number]	k4	1
800	[number]	k4	800
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
150	[number]	k4	150
300	[number]	k4	300
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Planý	planý	k2eAgInSc4d1	planý
poplach	poplach	k1gInSc4	poplach
===	===	k?	===
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1883	[number]	k4	1883
se	se	k3xPyFc4	se
na	na	k7c6	na
mostě	most	k1gInSc6	most
rozkřiklo	rozkřiknout	k5eAaPmAgNnS	rozkřiknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
most	most	k1gInSc1	most
nezvládá	zvládat	k5eNaImIp3nS	zvládat
nápor	nápor	k1gInSc4	nápor
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
zřítit	zřítit	k5eAaPmF	zřítit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
na	na	k7c6	na
mostě	most	k1gInSc6	most
zavládla	zavládnout	k5eAaPmAgFnS	zavládnout
panika	panika	k1gFnSc1	panika
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
mostě	most	k1gInSc6	most
snažili	snažit	k5eAaImAgMnP	snažit
co	co	k9	co
nejrychleji	rychle	k6eAd3	rychle
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
bylo	být	k5eAaImAgNnS	být
ušlapáno	ušlapat	k5eAaPmNgNnS	ušlapat
12	[number]	k4	12
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
pochybnosti	pochybnost	k1gFnPc1	pochybnost
o	o	k7c6	o
stabilitě	stabilita	k1gFnSc6	stabilita
mostu	most	k1gInSc2	most
vyvráceny	vyvrácen	k2eAgInPc1d1	vyvrácen
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
mostě	most	k1gInSc6	most
vydalo	vydat	k5eAaPmAgNnS	vydat
21	[number]	k4	21
slonů	slon	k1gMnPc2	slon
z	z	k7c2	z
nedalekého	daleký	k2eNgInSc2d1	nedaleký
cirkusu	cirkus	k1gInSc2	cirkus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgMnSc1	první
sebevrah	sebevrah	k1gMnSc1	sebevrah
===	===	k?	===
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1885	[number]	k4	1885
skočil	skočit	k5eAaPmAgMnS	skočit
z	z	k7c2	z
mostu	most	k1gInSc2	most
první	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
který	který	k3yRgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
utrpěl	utrpět	k5eAaPmAgInS	utrpět
vážná	vážný	k2eAgNnPc4d1	vážné
vnitřní	vnitřní	k2eAgNnPc4d1	vnitřní
zranění	zranění	k1gNnPc4	zranění
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Osvětlení	osvětlení	k1gNnSc1	osvětlení
mostu	most	k1gInSc2	most
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
most	most	k1gInSc1	most
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
dominant	dominanta	k1gFnPc2	dominanta
nočního	noční	k2eAgMnSc2d1	noční
New	New	k1gMnSc2	New
Yorku	York	k1gInSc2	York
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byly	být	k5eAaImAgFnP	být
jeho	jeho	k3xOp3gFnPc1	jeho
věže	věž	k1gFnPc1	věž
osvětleny	osvětlit	k5eAaPmNgFnP	osvětlit
pomocí	pomocí	k7c2	pomocí
reflektorů	reflektor	k1gInPc2	reflektor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Střelba	střelba	k1gFnSc1	střelba
na	na	k7c6	na
mostě	most	k1gInSc6	most
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1994	[number]	k4	1994
se	se	k3xPyFc4	se
na	na	k7c6	na
mostě	most	k1gInSc6	most
střílelo	střílet	k5eAaImAgNnS	střílet
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
jménem	jméno	k1gNnSc7	jméno
Rashid	Rashid	k1gInSc4	Rashid
Baz	Baz	k1gMnSc1	Baz
zahájil	zahájit	k5eAaPmAgMnS	zahájit
palbu	palba	k1gFnSc4	palba
na	na	k7c4	na
skupinu	skupina	k1gFnSc4	skupina
ortodoxních	ortodoxní	k2eAgMnPc2d1	ortodoxní
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
člověka	člověk	k1gMnSc2	člověk
smrtelně	smrtelně	k6eAd1	smrtelně
postřelil	postřelit	k5eAaPmAgMnS	postřelit
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
tři	tři	k4xCgMnPc1	tři
zranil	zranit	k5eAaPmAgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
pomstu	pomsta	k1gFnSc4	pomsta
za	za	k7c4	za
útok	útok	k1gInSc4	útok
na	na	k7c4	na
muslimy	muslim	k1gMnPc4	muslim
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
<g/>
,	,	kIx,	,
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Baz	Baz	k?	Baz
byl	být	k5eAaImAgInS	být
usvědčen	usvědčit	k5eAaPmNgInS	usvědčit
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
to	ten	k3xDgNnSc1	ten
označilo	označit	k5eAaPmAgNnS	označit
jako	jako	k9	jako
teroristický	teroristický	k2eAgInSc4d1	teroristický
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
zabil	zabít	k5eAaPmAgMnS	zabít
pouze	pouze	k6eAd1	pouze
jednoho	jeden	k4xCgMnSc4	jeden
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
na	na	k7c4	na
141	[number]	k4	141
let	léto	k1gNnPc2	léto
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zmařený	zmařený	k2eAgInSc1d1	zmařený
teroristický	teroristický	k2eAgInSc1d1	teroristický
útok	útok	k1gInSc1	útok
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
podporoval	podporovat	k5eAaImAgMnS	podporovat
teroristické	teroristický	k2eAgNnSc4d1	teroristické
hnutí	hnutí	k1gNnSc4	hnutí
Al-Káida	Al-Káid	k1gMnSc2	Al-Káid
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
na	na	k7c6	na
20	[number]	k4	20
let	léto	k1gNnPc2	léto
vězení	vězení	k1gNnSc2	vězení
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
připravoval	připravovat	k5eAaImAgMnS	připravovat
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
most	most	k1gInSc1	most
zničen	zničit	k5eAaPmNgInS	zničit
tím	ten	k3xDgInSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
přeřezala	přeřezat	k5eAaPmAgNnP	přeřezat
mostní	mostní	k2eAgNnPc1d1	mostní
lana	lano	k1gNnPc1	lano
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
neunesl	unést	k5eNaPmAgMnS	unést
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
tíhu	tíha	k1gFnSc4	tíha
a	a	k8xC	a
následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zřítil	zřítit	k5eAaPmAgInS	zřítit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nález	nález	k1gInSc1	nález
zásob	zásoba	k1gFnPc2	zásoba
v	v	k7c6	v
bunkru	bunkr	k1gInSc6	bunkr
===	===	k?	===
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
r.	r.	kA	r.
2006	[number]	k4	2006
při	při	k7c6	při
opravě	oprava	k1gFnSc6	oprava
mostu	most	k1gInSc2	most
obnažili	obnažit	k5eAaPmAgMnP	obnažit
dělníci	dělník	k1gMnPc1	dělník
v	v	k7c6	v
pilíři	pilíř	k1gInSc6	pilíř
tajnou	tajný	k2eAgFnSc4d1	tajná
skrýš	skrýš	k1gFnSc4	skrýš
<g/>
,	,	kIx,	,
vybudovanou	vybudovaný	k2eAgFnSc4d1	vybudovaná
r.	r.	kA	r.
1950	[number]	k4	1950
v	v	k7c6	v
době	doba	k1gFnSc6	doba
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Vchod	vchod	k1gInSc1	vchod
byl	být	k5eAaImAgInS	být
zamaskován	zamaskovat	k5eAaPmNgInS	zamaskovat
ve	v	k7c6	v
stěně	stěna	k1gFnSc6	stěna
od	od	k7c2	od
strany	strana	k1gFnSc2	strana
Manhattanu	Manhattan	k1gInSc2	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tajné	tajný	k2eAgFnSc6d1	tajná
schránce	schránka	k1gFnSc6	schránka
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
značné	značný	k2eAgFnPc1d1	značná
zásoby	zásoba	k1gFnPc1	zásoba
-	-	kIx~	-
350	[number]	k4	350
000	[number]	k4	000
kovových	kovový	k2eAgFnPc2d1	kovová
dóz	dóza	k1gFnPc2	dóza
se	se	k3xPyFc4	se
suchary	suchar	k1gInPc1	suchar
<g/>
,	,	kIx,	,
zařízení	zařízení	k1gNnPc1	zařízení
na	na	k7c4	na
čištění	čištění	k1gNnSc4	čištění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
přikrývky	přikrývka	k1gFnPc1	přikrývka
<g/>
,	,	kIx,	,
lékařské	lékařský	k2eAgInPc1d1	lékařský
přístroje	přístroj	k1gInPc1	přístroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Provoz	provoz	k1gInSc1	provoz
==	==	k?	==
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
mostě	most	k1gInSc6	most
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
dráha	dráha	k1gFnSc1	dráha
a	a	k8xC	a
dráha	dráha	k1gFnSc1	dráha
pro	pro	k7c4	pro
tramvaje	tramvaj	k1gFnPc4	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
nadzemní	nadzemní	k2eAgFnSc2d1	nadzemní
dráhy	dráha	k1gFnSc2	dráha
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
i	i	k9	i
tramvajové	tramvajový	k2eAgFnPc1d1	tramvajová
koleje	kolej	k1gFnPc1	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
most	most	k1gInSc1	most
přebudován	přebudován	k2eAgInSc1d1	přebudován
na	na	k7c4	na
šest	šest	k4xCc4	šest
jízdních	jízdní	k2eAgInPc2d1	jízdní
pruhů	pruh	k1gInPc2	pruh
pro	pro	k7c4	pro
automobily	automobil	k1gInPc4	automobil
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
směr	směr	k1gInSc1	směr
tři	tři	k4xCgInPc4	tři
pruhy	pruh	k1gInPc4	pruh
na	na	k7c6	na
krajích	kraj	k1gInPc6	kraj
mostu	most	k1gInSc2	most
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
uprostřed	uprostřed	k6eAd1	uprostřed
vede	vést	k5eAaImIp3nS	vést
pás	pás	k1gInSc1	pás
pro	pro	k7c4	pro
chodce	chodec	k1gMnPc4	chodec
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Brooklynský	brooklynský	k2eAgInSc4d1	brooklynský
most	most	k1gInSc4	most
druhý	druhý	k4xOgInSc1	druhý
nejfrekventovanější	frekventovaný	k2eAgInSc1d3	nejfrekventovanější
most	most	k1gInSc1	most
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rychle	rychle	k6eAd1	rychle
narůstajícímu	narůstající	k2eAgInSc3d1	narůstající
počtu	počet	k1gInSc3	počet
automobilů	automobil	k1gInPc2	automobil
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojenou	spojený	k2eAgFnSc7d1	spojená
houstnoucí	houstnoucí	k2eAgFnSc7d1	houstnoucí
dopravou	doprava	k1gFnSc7	doprava
museli	muset	k5eAaImAgMnP	muset
na	na	k7c6	na
mostě	most	k1gInSc6	most
zavést	zavést	k5eAaPmF	zavést
zákaz	zákaz	k1gInSc4	zákaz
pro	pro	k7c4	pro
automobily	automobil	k1gInPc4	automobil
těžší	těžký	k2eAgInPc4d2	těžší
než	než	k8xS	než
2,7	[number]	k4	2,7
tuny	tuna	k1gFnPc4	tuna
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
přes	přes	k7c4	přes
most	most	k1gInSc4	most
dnes	dnes	k6eAd1	dnes
nejezdí	jezdit	k5eNaImIp3nP	jezdit
ani	ani	k8xC	ani
autobusy	autobus	k1gInPc1	autobus
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
projede	projet	k5eAaPmIp3nS	projet
přes	přes	k7c4	přes
most	most	k1gInSc4	most
necelých	celý	k2eNgInPc2d1	necelý
150	[number]	k4	150
000	[number]	k4	000
aut	auto	k1gNnPc2	auto
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
most	most	k1gInSc1	most
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
výstavba	výstavba	k1gFnSc1	výstavba
je	být	k5eAaImIp3nS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
romatickém	romatický	k2eAgInSc6d1	romatický
hraném	hraný	k2eAgInSc6d1	hraný
filmu	film	k1gInSc6	film
Kate	kat	k1gInSc5	kat
a	a	k8xC	a
Leopold	Leopolda	k1gFnPc2	Leopolda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Statistika	statistika	k1gFnSc1	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
výška	výška	k1gFnSc1	výška
84	[number]	k4	84
m	m	kA	m
</s>
</p>
<p>
<s>
šířka	šířka	k1gFnSc1	šířka
26	[number]	k4	26
m	m	kA	m
</s>
</p>
<p>
<s>
3	[number]	k4	3
mostová	mostový	k2eAgFnSc1d1	mostová
pole	pole	k1gFnSc1	pole
jsou	být	k5eAaImIp3nP	být
spojena	spojit	k5eAaPmNgFnS	spojit
2	[number]	k4	2
gotickými	gotický	k2eAgInPc7d1	gotický
pilíři	pilíř	k1gInPc7	pilíř
</s>
</p>
<p>
<s>
střední	střední	k2eAgNnSc1d1	střední
pole	pole	k1gNnSc1	pole
měří	měřit	k5eAaImIp3nS	měřit
486,3	[number]	k4	486,3
m	m	kA	m
</s>
</p>
<p>
<s>
stál	stát	k5eAaImAgMnS	stát
15,5	[number]	k4	15,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
dolarů	dolar	k1gInPc2	dolar
</s>
</p>
<p>
<s>
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
27	[number]	k4	27
osob	osoba	k1gFnPc2	osoba
</s>
</p>
<p>
<s>
==	==	k?	==
Panorama	panorama	k1gNnSc1	panorama
mostu	most	k1gInSc2	most
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Brooklynský	brooklynský	k2eAgInSc4d1	brooklynský
most	most	k1gInSc4	most
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
,	,	kIx,	,
Б	Б	k?	Б
м	м	k?	м
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Most	most	k1gInSc1	most
Brookliński	Brooklińsk	k1gFnSc2	Brooklińsk
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Brooklynský	brooklynský	k2eAgInSc4d1	brooklynský
most	most	k1gInSc4	most
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Mosty	most	k1gInPc1	most
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
</s>
</p>
