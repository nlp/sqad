<s>
Kanton	Kanton	k1gInSc1
Seltz	Seltza	k1gFnPc2
</s>
<s>
Kanton	Kanton	k1gInSc1
Seltz	Seltz	k1gInSc1
Kanton	Kanton	k1gInSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
arrondissementu	arrondissement	k1gInSc2
Wissembourg	Wissembourg	k1gInSc4
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
Region	region	k1gInSc1
</s>
<s>
Alsasko	Alsasko	k1gNnSc1
Departement	departement	k1gInSc1
</s>
<s>
Bas-Rhin	Bas-Rhin	k2eAgInSc1d1
Arrondissement	Arrondissement	k1gInSc1
</s>
<s>
Wissembourg	Wissembourg	k1gInSc1
Počet	počet	k1gInSc1
obcí	obec	k1gFnPc2
</s>
<s>
14	#num#	k4
Sídlo	sídlo	k1gNnSc4
správy	správa	k1gFnSc2
</s>
<s>
Seltz	Seltz	k1gInSc1
Rozloha	rozloha	k1gFnSc1
</s>
<s>
106,49	106,49	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
13	#num#	k4
063	#num#	k4
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
122,7	122,7	k4
ob.	ob.	k?
<g/>
/	/	kIx~
<g/>
km²	km²	k?
</s>
<s>
Kanton	Kanton	k1gInSc1
Seltz	Seltz	k1gInSc1
(	(	kIx(
<g/>
fr.	fr.	k?
Canton	Canton	k1gInSc1
de	de	k?
Seltz	Seltz	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
francouzský	francouzský	k2eAgInSc1d1
kanton	kanton	k1gInSc1
v	v	k7c6
departementu	departement	k1gInSc6
Bas-Rhin	Bas-Rhina	k1gFnPc2
v	v	k7c6
regionu	region	k1gInSc6
Alsasko	Alsasko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvořilo	tvořit	k5eAaImAgNnS
ho	on	k3xPp3gNnSc2
14	#num#	k4
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrušen	zrušen	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
po	po	k7c6
reformě	reforma	k1gFnSc6
kantonů	kanton	k1gInPc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obce	obec	k1gFnPc1
kantonu	kanton	k1gInSc2
</s>
<s>
Beinheim	Beinheim	k6eAd1
</s>
<s>
Buhl	Buhl	k1gMnSc1
</s>
<s>
Crœ	Crœ	k1gMnSc1
</s>
<s>
Eberbach-Seltz	Eberbach-Seltz	k1gMnSc1
</s>
<s>
Kesseldorf	Kesseldorf	k1gMnSc1
</s>
<s>
Mothern	Mothern	k1gMnSc1
</s>
<s>
Munchhausen	Munchhausen	k1gInSc1
</s>
<s>
Niederrœ	Niederrœ	k1gMnSc1
</s>
<s>
Oberlauterbach	Oberlauterbach	k1gMnSc1
</s>
<s>
Schaffhouse-prè	Schaffhouse-prè	k1gMnSc1
</s>
<s>
Seltz	Seltz	k1gMnSc1
</s>
<s>
Siegen	Siegen	k1gInSc1
</s>
<s>
Trimbach	Trimbach	k1gMnSc1
</s>
<s>
Wintzenbach	Wintzenbach	k1gMnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kantony	Kanton	k1gInPc1
v	v	k7c6
departementu	departement	k1gInSc6
Bas-Rhin	Bas-Rhina	k1gFnPc2
po	po	k7c6
reformě	reforma	k1gFnSc6
2015	#num#	k4
(	(	kIx(
<g/>
23	#num#	k4
kantonů	kanton	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Bischwiller	Bischwiller	k1gMnSc1
•	•	k?
Bouxwiller	Bouxwiller	k1gMnSc1
•	•	k?
Brumath	Brumath	k1gMnSc1
•	•	k?
Erstein	Erstein	k1gInSc1
•	•	k?
Haguenau	Haguenaus	k1gInSc2
•	•	k?
Hœ	Hœ	k1gInSc1
•	•	k?
Illkirch-Graffenstaden	Illkirch-Graffenstaden	k2eAgInSc1d1
•	•	k?
Ingwiller	Ingwiller	k1gInSc1
•	•	k?
Lingolsheim	Lingolsheim	k1gInSc1
•	•	k?
Molsheim	Molsheim	k1gInSc1
•	•	k?
Mutzig	Mutzig	k1gInSc1
•	•	k?
Obernai	Oberna	k1gFnSc2
•	•	k?
Reichshoffen	Reichshoffen	k1gInSc1
•	•	k?
Saverne	Savern	k1gInSc5
•	•	k?
Schiltigheim	Schiltigheim	k1gInSc1
•	•	k?
Sélestat	Sélestat	k1gInSc1
•	•	k?
Štrasburk-	Štrasburk-	k1gFnSc1
<g/>
1	#num#	k4
•	•	k?
Štrasburk-	Štrasburk-	k1gFnPc2
<g/>
2	#num#	k4
•	•	k?
Štrasburk-	Štrasburk-	k1gFnPc2
<g/>
3	#num#	k4
•	•	k?
Štrasburk-	Štrasburk-	k1gFnPc2
<g/>
4	#num#	k4
•	•	k?
Štrasburk-	Štrasburk-	k1gFnPc2
<g/>
5	#num#	k4
•	•	k?
Štrasburk-	Štrasburk-	k1gFnPc2
<g/>
6	#num#	k4
•	•	k?
Wissembourg	Wissembourg	k1gMnSc1
před	před	k7c7
reformou	reforma	k1gFnSc7
2015	#num#	k4
(	(	kIx(
<g/>
44	#num#	k4
kantonů	kanton	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Barr	Barr	k1gMnSc1
•	•	k?
Benfeld	Benfeld	k1gMnSc1
•	•	k?
Bischheim	Bischheim	k1gMnSc1
•	•	k?
Bischwiller	Bischwiller	k1gMnSc1
•	•	k?
Bouxwiller	Bouxwiller	k1gMnSc1
•	•	k?
Brumath	Brumath	k1gMnSc1
•	•	k?
Drulingen	Drulingen	k1gInSc1
•	•	k?
Erstein	Erstein	k1gInSc1
•	•	k?
Geispolsheim	Geispolsheim	k1gInSc1
•	•	k?
Haguenau	Haguenaus	k1gInSc2
•	•	k?
Hochfelden	Hochfeldna	k1gFnPc2
•	•	k?
Illkirch-Graffenstaden	Illkirch-Graffenstaden	k2eAgInSc1d1
•	•	k?
Lauterbourg	Lauterbourg	k1gInSc1
•	•	k?
Marckolsheim	Marckolsheim	k1gMnSc1
•	•	k?
Marmoutier	Marmoutier	k1gMnSc1
•	•	k?
Molsheim	Molsheim	k1gMnSc1
•	•	k?
Mundolsheim	Mundolsheim	k1gMnSc1
•	•	k?
Niederbronn-les-Bains	Niederbronn-les-Bains	k1gInSc1
•	•	k?
Obernai	Oberna	k1gFnSc2
•	•	k?
La	la	k1gNnSc1
Petite-Pierre	Petite-Pierr	k1gInSc5
•	•	k?
Rosheim	Rosheim	k1gMnSc1
•	•	k?
Saales	Saales	k1gMnSc1
•	•	k?
Sarre-Union	Sarre-Union	k1gInSc1
•	•	k?
Saverne	Savern	k1gInSc5
•	•	k?
Schiltigheim	Schiltigheim	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Schirmeck	Schirmeck	k1gInSc1
•	•	k?
Sélestat	Sélestat	k1gInSc1
•	•	k?
Seltz	Seltz	k1gInSc1
•	•	k?
Soultz-sous-Forê	Soultz-sous-Forê	k1gInSc1
•	•	k?
Štrasburk-	Štrasburk-	k1gFnSc1
<g/>
1	#num#	k4
•	•	k?
Štrasburk-	Štrasburk-	k1gFnPc2
<g/>
2	#num#	k4
•	•	k?
Štrasburk-	Štrasburk-	k1gFnPc2
<g/>
3	#num#	k4
•	•	k?
Štrasburk-	Štrasburk-	k1gFnPc2
<g/>
4	#num#	k4
•	•	k?
Štrasburk-	Štrasburk-	k1gFnPc2
<g/>
5	#num#	k4
•	•	k?
Štrasburk-	Štrasburk-	k1gFnPc2
<g/>
6	#num#	k4
•	•	k?
Štrasburk-	Štrasburk-	k1gFnPc2
<g/>
7	#num#	k4
•	•	k?
Štrasburk-	Štrasburk-	k1gFnPc2
<g/>
8	#num#	k4
•	•	k?
Štrasburk-	Štrasburk-	k1gFnPc2
<g/>
9	#num#	k4
•	•	k?
Štrasburk-	Štrasburk-	k1gFnPc2
<g/>
10	#num#	k4
•	•	k?
Truchtersheim	Truchtersheim	k1gMnSc1
•	•	k?
Villé	Villý	k2eAgNnSc1d1
•	•	k?
Wasselonne	Wasselonn	k1gInSc5
•	•	k?
Wissembourg	Wissembourg	k1gMnSc1
•	•	k?
Wœ	Wœ	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
</s>
