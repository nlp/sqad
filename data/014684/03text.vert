<s>
Egyptská	egyptský	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
starověké	starověký	k2eAgFnSc6d1
egyptské	egyptský	k2eAgFnSc6d1
Knize	kniha	k1gFnSc6
mrtvých	mrtvý	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
islámské	islámský	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
mrtvých	mrtvý	k1gMnPc2
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Islámská	islámský	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1
z	z	k7c2
knihy	kniha	k1gFnSc2
</s>
<s>
Egyptská	egyptský	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
mrtvýchv	mrtvýchva	k1gFnPc2
hieroglyfickém	hieroglyfický	k2eAgMnSc6d1
zápisu	zápis	k1gInSc3
</s>
<s>
Egyptská	egyptský	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
je	být	k5eAaImIp3nS
mimořádně	mimořádně	k6eAd1
důležité	důležitý	k2eAgNnSc1d1
a	a	k8xC
nejznámější	známý	k2eAgNnSc1d3
dílo	dílo	k1gNnSc1
egyptské	egyptský	k2eAgFnSc2d1
duchovní	duchovní	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Umělý	umělý	k2eAgInSc1d1
název	název	k1gInSc1
Egyptská	egyptský	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
zavedl	zavést	k5eAaPmAgMnS
J.	J.	kA
F.	F.	kA
Champollion	Champollion	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
arabský	arabský	k2eAgInSc4d1
název	název	k1gInSc4
Kniha	kniha	k1gFnSc1
mrtvého	mrtvý	k1gMnSc2
muže	muž	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
ji	on	k3xPp3gFnSc4
poprvé	poprvé	k6eAd1
přeložil	přeložit	k5eAaPmAgMnS
do	do	k7c2
moderního	moderní	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
se	se	k3xPyFc4
však	však	k9
ztratil	ztratit	k5eAaPmAgMnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
jeho	jeho	k3xOp3gMnSc1
žák	žák	k1gMnSc1
se	se	k3xPyFc4
zmocnil	zmocnit	k5eAaPmAgMnS
všech	všecek	k3xTgInPc2
jeho	jeho	k3xOp3gInPc2
rukopisů	rukopis	k1gInPc2
a	a	k8xC
chtěl	chtít	k5eAaImAgMnS
je	být	k5eAaImIp3nS
vydávat	vydávat	k5eAaPmF,k5eAaImF
za	za	k7c4
svá	svůj	k3xOyFgNnPc4
díla	dílo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratru	bratru	k9
Champolliona	Champollion	k1gMnSc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
větší	veliký	k2eAgFnSc4d2
část	část	k1gFnSc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
zpět	zpět	k6eAd1
a	a	k8xC
vydat	vydat	k5eAaPmF
je	on	k3xPp3gNnSc4
pod	pod	k7c7
jménem	jméno	k1gNnSc7
skutečného	skutečný	k2eAgMnSc2d1
autora	autor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
zachoval	zachovat	k5eAaPmAgInS
slovník	slovník	k1gInSc1
a	a	k8xC
gramatika	gramatika	k1gFnSc1
rozluštitele	rozluštitel	k1gMnSc2
egyptštiny	egyptština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sami	sám	k3xTgMnPc1
starověcí	starověký	k2eAgMnPc1d1
Egypťané	Egypťan	k1gMnPc1
ji	on	k3xPp3gFnSc4
však	však	k9
označovali	označovat	k5eAaImAgMnP
názvem	název	k1gInSc7
Kapitoly	kapitola	k1gFnSc2
o	o	k7c4
vycházení	vycházení	k1gNnSc4
z	z	k7c2
hmotného	hmotný	k2eAgInSc2d1
světa	svět	k1gInSc2
do	do	k7c2
Bezbřehé	bezbřehý	k2eAgFnSc2d1
záře	zář	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
do	do	k7c2
190	#num#	k4
kapitol	kapitola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemusely	muset	k5eNaImAgInP
to	ten	k3xDgNnSc1
být	být	k5eAaImF
nutně	nutně	k6eAd1
texty	text	k1gInPc4
pro	pro	k7c4
přípravu	příprava	k1gFnSc4
na	na	k7c4
posmrtný	posmrtný	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
současně	současně	k6eAd1
popis	popis	k1gInSc4
božského	božský	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
životem	život	k1gInSc7
pozemským	pozemský	k2eAgInSc7d1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
například	například	k6eAd1
přinášení	přinášení	k1gNnSc4
obětin	obětina	k1gFnPc2
zemřelým	zemřelý	k2eAgInPc3d1
<g/>
)	)	kIx)
A	a	k9
toto	tento	k3xDgNnSc1
myšlení	myšlení	k1gNnSc1
se	se	k3xPyFc4
odráží	odrážet	k5eAaImIp3nS
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Knize	kniha	k1gFnSc6
mrtvých	mrtvý	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Kozák	Kozák	k1gMnSc1
<g/>
:	:	kIx,
Egyptská	egyptský	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
svazky	svazek	k1gInPc1
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Kozák	Kozák	k1gMnSc1
<g/>
:	:	kIx,
Aniho	Ani	k1gMnSc2
papyrus	papyrus	k1gInSc1
-	-	kIx~
Nejkrásnější	krásný	k2eAgFnSc1d3
Egyptská	egyptský	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2006	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kniha	kniha	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
</s>
<s>
Aniho	Anize	k6eAd1
papyrus	papyrus	k1gInSc1
-	-	kIx~
nejkrásnější	krásný	k2eAgInSc1d3
exemplář	exemplář	k1gInSc1
Egyptské	egyptský	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
mrtvých	mrtvý	k1gMnPc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
v	v	k7c6
Britském	britský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Starověký	starověký	k2eAgInSc1d1
Egypt	Egypt	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
177494324	#num#	k4
</s>
