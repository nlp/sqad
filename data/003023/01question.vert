<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
živočichové	živočich	k1gMnPc1	živočich
přečkávají	přečkávat	k5eAaImIp3nP	přečkávat
nepříznivé	příznivý	k2eNgFnPc4d1	nepříznivá
zimní	zimní	k2eAgFnPc4d1	zimní
období	období	k1gNnPc4	období
<g/>
?	?	kIx.	?
</s>
