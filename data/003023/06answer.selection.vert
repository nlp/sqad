<s>
Zimoviště	zimoviště	k1gNnSc1	zimoviště
<g/>
,	,	kIx,	,
též	též	k9	též
zvané	zvaný	k2eAgNnSc1d1	zvané
jako	jako	k8xC	jako
hibernakulum	hibernakulum	k1gNnSc1	hibernakulum
<g/>
,	,	kIx,	,
je	on	k3xPp3gNnSc4	on
místo	místo	k1gNnSc4	místo
nebo	nebo	k8xC	nebo
úkryt	úkryt	k1gInSc4	úkryt
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
živočichové	živočich	k1gMnPc1	živočich
přečkávají	přečkávat	k5eAaImIp3nP	přečkávat
nepříznivé	příznivý	k2eNgFnPc4d1	nepříznivá
zimní	zimní	k2eAgFnPc4d1	zimní
období	období	k1gNnPc4	období
<g/>
.	.	kIx.	.
</s>
