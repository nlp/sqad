<s>
Zimoviště	zimoviště	k1gNnSc1	zimoviště
<g/>
,	,	kIx,	,
též	též	k9	též
zvané	zvaný	k2eAgNnSc1d1	zvané
jako	jako	k8xS	jako
hibernakulum	hibernakulum	k1gNnSc1	hibernakulum
<g/>
,	,	kIx,	,
je	on	k3xPp3gNnSc4	on
místo	místo	k1gNnSc4	místo
nebo	nebo	k8xC	nebo
úkryt	úkryt	k1gInSc4	úkryt
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
živočichové	živočich	k1gMnPc1	živočich
přečkávají	přečkávat	k5eAaImIp3nP	přečkávat
nepříznivé	příznivý	k2eNgFnPc4d1	nepříznivá
zimní	zimní	k2eAgFnPc4d1	zimní
období	období	k1gNnPc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Zimovišti	zimoviště	k1gNnSc3	zimoviště
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
chráněná	chráněný	k2eAgNnPc1d1	chráněné
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
jeskyně	jeskyně	k1gFnPc1	jeskyně
<g/>
,	,	kIx,	,
úkryty	úkryt	k1gInPc1	úkryt
<g/>
,	,	kIx,	,
zimní	zimní	k2eAgNnPc1d1	zimní
doupata	doupě	k1gNnPc1	doupě
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
společná	společný	k2eAgNnPc1d1	společné
zimní	zimní	k2eAgNnPc1d1	zimní
hnízda	hnízdo	k1gNnPc1	hnízdo
hrabošů	hraboš	k1gMnPc2	hraboš
<g/>
,	,	kIx,	,
zimní	zimní	k2eAgFnPc4d1	zimní
listová	listový	k2eAgNnPc4d1	listové
hnízda	hnízdo	k1gNnPc4	hnízdo
housenek	housenka	k1gFnPc2	housenka
apod.	apod.	kA	apod.
Nestěhovavé	stěhovavý	k2eNgNnSc1d1	stěhovavý
vodní	vodní	k2eAgNnSc1d1	vodní
ptactvo	ptactvo	k1gNnSc1	ptactvo
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
svá	svůj	k3xOyFgNnPc4	svůj
zimoviště	zimoviště	k1gNnPc4	zimoviště
odlehlá	odlehlý	k2eAgNnPc4d1	odlehlé
ramena	rameno	k1gNnPc4	rameno
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
rybníky	rybník	k1gInPc1	rybník
<g/>
,	,	kIx,	,
mlýnské	mlýnský	k2eAgInPc1d1	mlýnský
náhony	náhon	k1gInPc1	náhon
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
pak	pak	k6eAd1	pak
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
úplnému	úplný	k2eAgNnSc3d1	úplné
zamrznutí	zamrznutí	k1gNnSc3	zamrznutí
vodní	vodní	k2eAgFnSc2d1	vodní
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Hmyzí	hmyzí	k2eAgInSc1d1	hmyzí
hotel	hotel	k1gInSc1	hotel
Broučí	broučí	k2eAgInSc4d1	broučí
násep	násep	k1gInSc4	násep
</s>
