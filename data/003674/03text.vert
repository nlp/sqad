<s>
Život	život	k1gInSc1	život
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
řeči	řeč	k1gFnSc6	řeč
období	období	k1gNnSc2	období
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
člověka	člověk	k1gMnSc2	člověk
nebo	nebo	k8xC	nebo
živočicha	živočich	k1gMnSc2	živočich
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
za	za	k7c4	za
jeho	jeho	k3xOp3gInSc4	jeho
počátek	počátek	k1gInSc4	počátek
pokládá	pokládat	k5eAaImIp3nS	pokládat
už	už	k6eAd1	už
oplodnění	oplodnění	k1gNnSc1	oplodnění
vajíčka	vajíčko	k1gNnSc2	vajíčko
(	(	kIx(	(
<g/>
početí	početí	k1gNnSc2	početí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
může	moct	k5eAaImIp3nS	moct
život	život	k1gInSc1	život
označovat	označovat	k5eAaImF	označovat
také	také	k9	také
souborný	souborný	k2eAgInSc4d1	souborný
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
tvořený	tvořený	k2eAgInSc4d1	tvořený
všemi	všecek	k3xTgInPc7	všecek
živými	živý	k2eAgInPc7d1	živý
organismy	organismus	k1gInPc7	organismus
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
charakterizovaný	charakterizovaný	k2eAgInSc4d1	charakterizovaný
zejména	zejména	k9	zejména
evolucí	evoluce	k1gFnSc7	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
část	část	k1gFnSc1	část
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
živé	živý	k2eAgInPc1d1	živý
organismy	organismus	k1gInPc1	organismus
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
biosféra	biosféra	k1gFnSc1	biosféra
<g/>
.	.	kIx.	.
</s>
<s>
Vědeckému	vědecký	k2eAgNnSc3d1	vědecké
zkoumání	zkoumání	k1gNnSc3	zkoumání
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
biologie	biologie	k1gFnSc1	biologie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
život	život	k1gInSc4	život
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
soubor	soubor	k1gInSc1	soubor
signálních	signální	k2eAgInPc2d1	signální
a	a	k8xC	a
sebeudržovacích	sebeudržovací	k2eAgInPc2d1	sebeudržovací
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
určitého	určitý	k2eAgInSc2d1	určitý
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
například	například	k6eAd1	například
látkovou	látkový	k2eAgFnSc4d1	látková
výměnu	výměna	k1gFnSc4	výměna
<g/>
,	,	kIx,	,
dráždivost	dráždivost	k1gFnSc4	dráždivost
nebo	nebo	k8xC	nebo
reprodukci	reprodukce	k1gFnSc4	reprodukce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
slovem	slovem	k6eAd1	slovem
život	život	k1gInSc4	život
označováno	označovat	k5eAaImNgNnS	označovat
cokoliv	cokoliv	k3yInSc4	cokoliv
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
žije	žít	k5eAaImIp3nS	žít
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
živé	živý	k2eAgNnSc1d1	živé
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ekologického	ekologický	k2eAgInSc2d1	ekologický
pohledu	pohled	k1gInSc2	pohled
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
živou	živý	k2eAgFnSc4d1	živá
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
společenském	společenský	k2eAgInSc6d1	společenský
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
pak	pak	k8xC	pak
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
nějakou	nějaký	k3yIgFnSc4	nějaký
činnost	činnost	k1gFnSc4	činnost
(	(	kIx(	(
<g/>
společenský	společenský	k2eAgInSc1d1	společenský
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Definice	definice	k1gFnSc2	definice
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
prof.	prof.	kA	prof.
Rosypala	Rosypal	k1gMnSc2	Rosypal
jsou	být	k5eAaImIp3nP	být
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc1	organismus
tvořeny	tvořit	k5eAaImNgInP	tvořit
buňkami	buňka	k1gFnPc7	buňka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
jednobuněčné	jednobuněčný	k2eAgNnSc1d1	jednobuněčné
a	a	k8xC	a
mnohobuněčné	mnohobuněčný	k2eAgNnSc1d1	mnohobuněčné
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jistými	jistý	k2eAgFnPc7d1	jistá
výhradami	výhrada	k1gFnPc7	výhrada
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
nebuněčné	buněčný	k2eNgInPc4d1	nebuněčný
organismy	organismus	k1gInPc4	organismus
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
viry	vir	k1gInPc1	vir
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
společné	společný	k2eAgFnPc1d1	společná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Přítomnost	přítomnost	k1gFnSc1	přítomnost
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
míra	míra	k1gFnSc1	míra
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
strukturní	strukturní	k2eAgFnSc2d1	strukturní
složitosti	složitost	k1gFnSc2	složitost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
hierarchii	hierarchie	k1gFnSc6	hierarchie
různých	různý	k2eAgFnPc2d1	různá
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Organismy	organismus	k1gInPc1	organismus
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
vydělené	vydělený	k2eAgNnSc1d1	vydělené
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otevřené	otevřený	k2eAgFnPc1d1	otevřená
<g/>
:	:	kIx,	:
udržují	udržovat	k5eAaImIp3nP	udržovat
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
okolím	okolí	k1gNnSc7	okolí
tok	toka	k1gFnPc2	toka
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mohou	moct	k5eAaImIp3nP	moct
udržovat	udržovat	k5eAaImF	udržovat
ustálený	ustálený	k2eAgInSc4d1	ustálený
stav	stav	k1gInSc4	stav
své	svůj	k3xOyFgFnSc2	svůj
struktury	struktura	k1gFnSc2	struktura
a	a	k8xC	a
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
samoregulace	samoregulace	k1gFnPc1	samoregulace
(	(	kIx(	(
<g/>
autoregulace	autoregulace	k1gFnPc1	autoregulace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pomocí	pomocí	k7c2	pomocí
zpětných	zpětný	k2eAgFnPc2d1	zpětná
vazeb	vazba	k1gFnPc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
metabolismem	metabolismus	k1gInSc7	metabolismus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
syntézou	syntéza	k1gFnSc7	syntéza
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
samostatné	samostatný	k2eAgFnSc2d1	samostatná
reprodukce	reprodukce	k1gFnSc2	reprodukce
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
lze	lze	k6eAd1	lze
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jako	jako	k8xC	jako
strukturálně	strukturálně	k6eAd1	strukturálně
vysoce	vysoce	k6eAd1	vysoce
složité	složitý	k2eAgNnSc1d1	složité
<g/>
,	,	kIx,	,
hierarchicky	hierarchicky	k6eAd1	hierarchicky
uspořádané	uspořádaný	k2eAgFnPc1d1	uspořádaná
<g/>
,	,	kIx,	,
termodynamicky	termodynamicky	k6eAd1	termodynamicky
otevřené	otevřený	k2eAgFnPc1d1	otevřená
a	a	k8xC	a
autoregulující	autoregulující	k2eAgFnPc1d1	autoregulující
se	se	k3xPyFc4	se
nukleoproteinové	nukleoproteinová	k1gFnSc2	nukleoproteinová
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc7	jejichž
podstatnými	podstatný	k2eAgFnPc7d1	podstatná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
jsou	být	k5eAaImIp3nP	být
metabolismus	metabolismus	k1gInSc4	metabolismus
<g/>
,	,	kIx,	,
autoreprodukce	autoreprodukce	k1gFnPc4	autoreprodukce
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vznik	vznik	k1gInSc1	vznik
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
vyjasněná	vyjasněný	k2eAgFnSc1d1	vyjasněná
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
z	z	k7c2	z
neživé	živý	k2eNgFnSc2d1	neživá
hmoty	hmota	k1gFnSc2	hmota
stala	stát	k5eAaPmAgFnS	stát
hmota	hmota	k1gFnSc1	hmota
živá	živý	k2eAgFnSc1d1	živá
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
<g/>
,	,	kIx,	,
přeměňovat	přeměňovat	k5eAaImF	přeměňovat
svoje	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
a	a	k8xC	a
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
předpokládájící	předpokládájící	k1gFnSc1	předpokládájící
<g/>
,	,	kIx,	,
že	že	k8xS	že
vývoj	vývoj	k1gInSc1	vývoj
života	život	k1gInSc2	život
probíhal	probíhat	k5eAaImAgInS	probíhat
postupně	postupně	k6eAd1	postupně
hromaděním	hromadění	k1gNnSc7	hromadění
nahodilých	nahodilý	k2eAgFnPc2d1	nahodilá
změn	změna	k1gFnPc2	změna
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
selekcí	selekce	k1gFnPc2	selekce
(	(	kIx(	(
<g/>
tj	tj	kA	tj
evolucí	evoluce	k1gFnSc7	evoluce
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
evoluční	evoluční	k2eAgFnSc1d1	evoluční
teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
život	život	k1gInSc1	život
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nedá	dát	k5eNaPmIp3nS	dát
se	se	k3xPyFc4	se
však	však	k9	však
vyloučit	vyloučit	k5eAaPmF	vyloučit
výskyt	výskyt	k1gInSc4	výskyt
života	život	k1gInSc2	život
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
dnešní	dnešní	k2eAgFnSc2d1	dnešní
vědy	věda	k1gFnSc2	věda
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
definování	definování	k1gNnSc4	definování
tvora	tvor	k1gMnSc2	tvor
jako	jako	k8xC	jako
živého	živý	k2eAgNnSc2d1	živé
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
splňoval	splňovat	k5eAaImAgMnS	splňovat
tyto	tento	k3xDgFnPc4	tento
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
:	:	kIx,	:
schopnost	schopnost	k1gFnSc4	schopnost
samostatně	samostatně	k6eAd1	samostatně
se	se	k3xPyFc4	se
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc4	schopnost
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
příjmu	příjem	k1gInSc2	příjem
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
tyto	tento	k3xDgFnPc1	tento
podmínky	podmínka	k1gFnPc1	podmínka
občas	občas	k6eAd1	občas
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
splněny	splnit	k5eAaPmNgInP	splnit
vždy	vždy	k6eAd1	vždy
všechny	všechen	k3xTgMnPc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
živého	živé	k1gNnSc2	živé
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
neživý	živý	k2eNgMnSc1d1	neživý
neboli	neboli	k8xC	neboli
abiotický	abiotický	k2eAgMnSc1d1	abiotický
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
vědci	vědec	k1gMnPc7	vědec
nepanuje	panovat	k5eNaImIp3nS	panovat
shoda	shoda	k1gFnSc1	shoda
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
za	za	k7c4	za
život	život	k1gInSc4	život
považovat	považovat	k5eAaImF	považovat
některé	některý	k3yIgInPc4	některý
nebuněčné	buněčný	k2eNgInPc4d1	nebuněčný
organismy	organismus	k1gInPc4	organismus
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
viry	vir	k1gInPc1	vir
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
přiklánějí	přiklánět	k5eAaImIp3nP	přiklánět
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jakési	jakýsi	k3yIgInPc4	jakýsi
neživé	živý	k2eNgInPc4d1	neživý
fragmenty	fragment	k1gInPc4	fragment
DNA	dno	k1gNnSc2	dno
někdejších	někdejší	k2eAgMnPc2d1	někdejší
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
definování	definování	k1gNnSc4	definování
živého	živý	k2eAgMnSc2d1	živý
tvora	tvor	k1gMnSc2	tvor
jsou	být	k5eAaImIp3nP	být
problematické	problematický	k2eAgFnPc1d1	problematická
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
splněna	splnit	k5eAaPmNgFnS	splnit
jak	jak	k8xC	jak
živým	živý	k2eAgMnSc7d1	živý
tvorem	tvor	k1gMnSc7	tvor
<g/>
,	,	kIx,	,
tak	tak	k9	tak
neživým	živý	k2eNgInSc7d1	neživý
objektem	objekt	k1gInSc7	objekt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
krystaly	krystal	k1gInPc1	krystal
rostou	růst	k5eAaImIp3nP	růst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
také	také	k9	také
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
nějaký	nějaký	k3yIgMnSc1	nějaký
živý	živý	k1gMnSc1	živý
tvor	tvor	k1gMnSc1	tvor
nemusí	muset	k5eNaImIp3nS	muset
splňovat	splňovat	k5eAaImF	splňovat
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
život	život	k1gInSc1	život
by	by	kYmCp3nS	by
snad	snad	k9	snad
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
lépe	dobře	k6eAd2	dobře
vymezit	vymezit	k5eAaPmF	vymezit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
porovnání	porovnání	k1gNnSc4	porovnání
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
považujeme	považovat	k5eAaImIp1nP	považovat
za	za	k7c4	za
živý	živý	k2eAgInSc4d1	živý
s	s	k7c7	s
objektem	objekt	k1gInSc7	objekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
za	za	k7c4	za
živý	živý	k2eAgInSc4d1	živý
nepovažujeme	považovat	k5eNaImIp1nP	považovat
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
od	od	k7c2	od
prvního	první	k4xOgNnSc2	první
neliší	lišit	k5eNaImIp3nP	lišit
ničím	nic	k3yNnSc7	nic
jiným	jiný	k2eAgNnSc7d1	jiné
než	než	k8xS	než
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
živý	živý	k2eAgMnSc1d1	živý
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
porovnat	porovnat	k5eAaPmF	porovnat
například	například	k6eAd1	například
živou	živý	k2eAgFnSc4d1	živá
buňku	buňka	k1gFnSc4	buňka
a	a	k8xC	a
buňku	buňka	k1gFnSc4	buňka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
právě	právě	k6eAd1	právě
ztratila	ztratit	k5eAaPmAgFnS	ztratit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Hmota	hmota	k1gFnSc1	hmota
buňky	buňka	k1gFnSc2	buňka
se	se	k3xPyFc4	se
ani	ani	k9	ani
neztratila	ztratit	k5eNaPmAgFnS	ztratit
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
nepřibyla	přibýt	k5eNaPmAgFnS	přibýt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
okamžiku	okamžik	k1gInSc2	okamžik
ztráty	ztráta	k1gFnSc2	ztráta
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
její	její	k3xOp3gFnSc1	její
struktura	struktura	k1gFnSc1	struktura
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
hlavní	hlavní	k2eAgFnSc7d1	hlavní
větou	věta	k1gFnSc7	věta
termodynamickou	termodynamický	k2eAgFnSc7d1	termodynamická
<g/>
.	.	kIx.	.
</s>
<s>
Nastal	nastat	k5eAaPmAgInS	nastat
tedy	tedy	k9	tedy
jiný	jiný	k2eAgInSc1d1	jiný
směr	směr	k1gInSc1	směr
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
směru	směr	k1gInSc2	směr
reakcí	reakce	k1gFnSc7	reakce
živé	živý	k2eAgFnSc2d1	živá
buňky	buňka	k1gFnSc2	buňka
liší	lišit	k5eAaImIp3nP	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
řízený	řízený	k2eAgInSc1d1	řízený
<g/>
.	.	kIx.	.
</s>
<s>
Řízení	řízení	k1gNnSc1	řízení
procesů	proces	k1gInPc2	proces
tak	tak	k8xS	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
jednak	jednak	k8xC	jednak
udržena	udržen	k2eAgFnSc1d1	udržena
integrita	integrita	k1gFnSc1	integrita
a	a	k8xC	a
funkčnost	funkčnost	k1gFnSc1	funkčnost
objektu	objekt	k1gInSc2	objekt
navzdor	navzdor	k7c3	navzdor
nahodilým	nahodilý	k2eAgInPc3d1	nahodilý
destruktivním	destruktivní	k2eAgInPc3d1	destruktivní
procesům	proces	k1gInPc3	proces
zvenku	zvenku	k6eAd1	zvenku
<g/>
,	,	kIx,	,
a	a	k8xC	a
případně	případně	k6eAd1	případně
vykonávání	vykonávání	k1gNnSc1	vykonávání
nějaké	nějaký	k3yIgFnSc2	nějaký
další	další	k2eAgFnSc2d1	další
funkce	funkce	k1gFnSc2	funkce
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
(	(	kIx(	(
<g/>
řízení	řízení	k1gNnSc1	řízení
procesů	proces	k1gInPc2	proces
<g/>
)	)	kIx)	)
hlavní	hlavní	k2eAgFnSc7d1	hlavní
nutnou	nutný	k2eAgFnSc7d1	nutná
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
však	však	k9	však
stále	stále	k6eAd1	stále
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
postačující	postačující	k2eAgMnSc1d1	postačující
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
by	by	kYmCp3nS	by
totiž	totiž	k9	totiž
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
atribut	atribut	k1gInSc4	atribut
života	život	k1gInSc2	život
přiznat	přiznat	k5eAaPmF	přiznat
i	i	k8xC	i
jakémukoli	jakýkoli	k3yIgNnSc3	jakýkoli
technickému	technický	k2eAgNnSc3d1	technické
zařízení	zařízení	k1gNnSc3	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
tuto	tento	k3xDgFnSc4	tento
podmínku	podmínka	k1gFnSc4	podmínka
splní	splnit	k5eAaPmIp3nP	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
laboratorně	laboratorně	k6eAd1	laboratorně
z	z	k7c2	z
neživých	živý	k2eNgFnPc2d1	neživá
látek	látka	k1gFnPc2	látka
vytvořit	vytvořit	k5eAaPmF	vytvořit
objekt	objekt	k1gInSc4	objekt
nerozlišitelný	rozlišitelný	k2eNgInSc4d1	nerozlišitelný
od	od	k7c2	od
živé	živý	k2eAgFnSc2d1	živá
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
vhodné	vhodný	k2eAgNnSc1d1	vhodné
mu	on	k3xPp3gMnSc3	on
atribut	atribut	k1gInSc4	atribut
života	život	k1gInSc2	život
také	také	k9	také
přiznat	přiznat	k5eAaPmF	přiznat
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nS	by
k	k	k7c3	k
nutným	nutný	k2eAgFnPc3d1	nutná
podmínkám	podmínka	k1gFnPc3	podmínka
přibyla	přibýt	k5eAaPmAgFnS	přibýt
i	i	k9	i
podmínka	podmínka	k1gFnSc1	podmínka
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
testovatelná	testovatelný	k2eAgFnSc1d1	testovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Mimozemský	mimozemský	k2eAgInSc4d1	mimozemský
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Mimozemský	mimozemský	k2eAgInSc1d1	mimozemský
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
souhrn	souhrn	k1gInSc4	souhrn
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
mimo	mimo	k7c4	mimo
planetu	planeta	k1gFnSc4	planeta
Zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
tyto	tento	k3xDgInPc1	tento
organismy	organismus	k1gInPc1	organismus
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
nazývány	nazývat	k5eAaImNgFnP	nazývat
mimozemské	mimozemský	k2eAgFnPc1d1	mimozemská
biologické	biologický	k2eAgFnPc1d1	biologická
entity	entita	k1gFnPc1	entita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hypotetický	hypotetický	k2eAgInSc4d1	hypotetický
termín	termín	k1gInSc4	termín
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
není	být	k5eNaImIp3nS	být
život	život	k1gInSc1	život
znám	znám	k2eAgInSc1d1	znám
nikde	nikde	k6eAd1	nikde
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
než	než	k8xS	než
právě	právě	k9	právě
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
