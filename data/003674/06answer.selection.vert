<s>
Vědeckému	vědecký	k2eAgNnSc3d1	vědecké
zkoumání	zkoumání	k1gNnSc3	zkoumání
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
biologie	biologie	k1gFnSc1	biologie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
život	život	k1gInSc4	život
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
soubor	soubor	k1gInSc1	soubor
signálních	signální	k2eAgInPc2d1	signální
a	a	k8xC	a
sebeudržovacích	sebeudržovací	k2eAgInPc2d1	sebeudržovací
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
určitého	určitý	k2eAgInSc2d1	určitý
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
například	například	k6eAd1	například
látkovou	látkový	k2eAgFnSc4d1	látková
výměnu	výměna	k1gFnSc4	výměna
<g/>
,	,	kIx,	,
dráždivost	dráždivost	k1gFnSc4	dráždivost
nebo	nebo	k8xC	nebo
reprodukci	reprodukce	k1gFnSc4	reprodukce
<g/>
.	.	kIx.	.
</s>
