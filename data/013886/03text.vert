<s>
Seznam	seznam	k1gInSc1
minerálů	minerál	k1gInPc2
</s>
<s>
Není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
zde	zde	k6eAd1
prezentovat	prezentovat	k5eAaBmF
úplný	úplný	k2eAgInSc4d1
seznam	seznam	k1gInSc4
minerálů	minerál	k1gInPc2
<g/>
,	,	kIx,
protože	protože	k8xS
Komise	komise	k1gFnSc1
pro	pro	k7c4
nové	nový	k2eAgInPc4d1
minerály	minerál	k1gInPc4
a	a	k8xC
názvy	název	k1gInPc4
minerálů	minerál	k1gInPc2
(	(	kIx(
<g/>
CNMMN	CNMMN	kA
<g/>
)	)	kIx)
při	při	k7c6
Mezinárodní	mezinárodní	k2eAgFnSc6d1
mineralogické	mineralogický	k2eAgFnSc6d1
asociaci	asociace	k1gFnSc6
(	(	kIx(
<g/>
IMA	IMA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
nové	nový	k2eAgInPc4d1
minerály	minerál	k1gInPc4
a	a	k8xC
změny	změna	k1gFnPc4
v	v	k7c6
názvosloví	názvosloví	k1gNnSc6
schvaluje	schvalovat	k5eAaImIp3nS
<g/>
,	,	kIx,
každý	každý	k3xTgInSc1
rok	rok	k1gInSc1
přidává	přidávat	k5eAaImIp3nS
nové	nový	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
názvů	název	k1gInPc2
minerálů	minerál	k1gInPc2
jsou	být	k5eAaImIp3nP
doplněny	doplnit	k5eAaPmNgFnP
značky	značka	k1gFnPc1
pro	pro	k7c4
status	status	k1gInSc4
názvu	název	k1gInSc2
(	(	kIx(
<g/>
platný	platný	k2eAgMnSc1d1
–	–	k?
neplatný	platný	k2eNgInSc1d1
nerost	nerost	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
důvodu	důvod	k1gInSc2
rozsáhlosti	rozsáhlost	k1gFnSc2
je	být	k5eAaImIp3nS
seznam	seznam	k1gInSc1
rozdělen	rozdělen	k2eAgInSc1d1
na	na	k7c4
tyto	tento	k3xDgInPc4
seznamy	seznam	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Seznam	seznam	k1gInSc1
minerálů	minerál	k1gInPc2
A-B	A-B	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
minerálů	minerál	k1gInPc2
C-F	C-F	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
minerálů	minerál	k1gInPc2
G-J	G-J	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
minerálů	minerál	k1gInPc2
K-M	K-M	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
minerálů	minerál	k1gInPc2
N-R	N-R	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
minerálů	minerál	k1gInPc2
S-U	S-U	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
minerálů	minerál	k1gInPc2
V-Ž	V-Ž	k1gFnSc2
</s>
<s>
Vysvětlivky	vysvětlivka	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
označení	označení	k1gNnSc1
u	u	k7c2
názvu	název	k1gInSc2
minerálu	minerál	k1gInSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
A	A	kA
–	–	k?
schválený	schválený	k2eAgInSc4d1
(	(	kIx(
<g/>
approved	approved	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
D	D	kA
–	–	k?
zrušený	zrušený	k2eAgInSc4d1
(	(	kIx(
<g/>
discredited	discredited	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
G	G	kA
–	–	k?
uznané	uznaný	k2eAgInPc4d1
nerosty	nerost	k1gInPc4
popsané	popsaný	k2eAgInPc4d1
před	před	k7c7
založením	založení	k1gNnSc7
komise	komise	k1gFnSc2
(	(	kIx(
<g/>
grandfathered	grandfathered	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
H	H	kA
–	–	k?
hypotetický	hypotetický	k2eAgInSc4d1
(	(	kIx(
<g/>
hypothetical	hypotheticat	k5eAaPmAgMnS
<g/>
)	)	kIx)
</s>
<s>
I	I	kA
–	–	k?
přechodný	přechodný	k2eAgInSc4d1
člen	člen	k1gInSc4
(	(	kIx(
<g/>
intermediate	intermediat	k1gMnSc5
member	membero	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
N	N	kA
–	–	k?
neschvalovaný	schvalovaný	k2eNgMnSc1d1
komisí	komise	k1gFnSc7
pro	pro	k7c4
nové	nový	k2eAgInPc4d1
minerály	minerál	k1gInPc4
a	a	k8xC
názvy	název	k1gInPc4
minerálů	minerál	k1gInPc2
</s>
<s>
Q	Q	kA
–	–	k?
sporný	sporný	k2eAgInSc4d1
(	(	kIx(
<g/>
questionable	questionable	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Rd	Rd	k?
–	–	k?
redefinovaný	redefinovaný	k2eAgMnSc1d1
se	s	k7c7
schválením	schválení	k1gNnSc7
komise	komise	k1gFnSc2
(	(	kIx(
<g/>
redefined	redefined	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Rn	Rn	k?
–	–	k?
přejmenovaný	přejmenovaný	k2eAgMnSc1d1
se	s	k7c7
schválením	schválení	k1gNnSc7
komise	komise	k1gFnSc2
(	(	kIx(
<g/>
renamed	renamed	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Úplný	úplný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
minerálů	minerál	k1gInPc2
z	z	k7c2
listopadu	listopad	k1gInSc2
2019	#num#	k4
</s>
<s>
Aktuální	aktuální	k2eAgInSc1d1
úplný	úplný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
minerálů	minerál	k1gInPc2
uznaných	uznaný	k2eAgInPc2d1
IMA	IMA	kA
-	-	kIx~
pod	pod	k7c7
záložkou	záložka	k1gFnSc7
"	"	kIx"
<g/>
IMA	IMA	kA
list	list	k1gInSc1
of	of	k?
minerals	minerals	k1gInSc1
<g/>
"	"	kIx"
</s>
