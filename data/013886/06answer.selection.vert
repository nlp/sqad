<s>
Není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
zde	zde	k6eAd1
prezentovat	prezentovat	k5eAaBmF
úplný	úplný	k2eAgInSc4d1
seznam	seznam	k1gInSc4
minerálů	minerál	k1gInPc2
<g/>
,	,	kIx,
protože	protože	k8xS
Komise	komise	k1gFnSc1
pro	pro	k7c4
nové	nový	k2eAgInPc4d1
minerály	minerál	k1gInPc4
a	a	k8xC
názvy	název	k1gInPc4
minerálů	minerál	k1gInPc2
(	(	kIx(
<g/>
CNMMN	CNMMN	kA
<g/>
)	)	kIx)
při	při	k7c6
Mezinárodní	mezinárodní	k2eAgFnSc6d1
mineralogické	mineralogický	k2eAgFnSc6d1
asociaci	asociace	k1gFnSc6
(	(	kIx(
<g/>
IMA	IMA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
nové	nový	k2eAgInPc4d1
minerály	minerál	k1gInPc4
a	a	k8xC
změny	změna	k1gFnPc4
v	v	k7c6
názvosloví	názvosloví	k1gNnSc6
schvaluje	schvalovat	k5eAaImIp3nS
<g/>
,	,	kIx,
každý	každý	k3xTgInSc1
rok	rok	k1gInSc1
přidává	přidávat	k5eAaImIp3nS
nové	nový	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>