<s>
Niccolò	Niccolò	k?
Machiavelli	Machiavelli	k1gMnSc1
</s>
<s>
Niccolò	Niccolò	k?
Machiavelli	Machiavelli	k1gMnSc1
Santi	Sanť	k1gFnSc2
di	di	k?
Tito	tento	k3xDgMnPc1
<g/>
,	,	kIx,
portrét	portrét	k1gInSc4
Machiavelliho	Machiavelli	k1gMnSc2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
pol.	pol.	k?
16	#num#	k4
<g/>
.	.	kIx.
st.	st.	kA
(	(	kIx(
<g/>
detail	detail	k1gInSc1
<g/>
)	)	kIx)
Narození	narození	k1gNnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1469	#num#	k4
Florencie	Florencie	k1gFnSc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1527	#num#	k4
(	(	kIx(
<g/>
58	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Florencie	Florencie	k1gFnSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
peritonitida	peritonitida	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Santa	Santa	k1gMnSc1
Croce	Croce	k1gMnSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
filozof	filozof	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
Italská	italský	k2eAgFnSc1d1
Stát	stát	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Období	období	k1gNnSc2
</s>
<s>
1499	#num#	k4
-	-	kIx~
1527	#num#	k4
Významná	významný	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
VladařRozpravy	VladařRozprava	k1gFnPc1
nad	nad	k7c7
prvními	první	k4xOgInPc7
deseti	deset	k4xCc7
knihami	kniha	k1gFnPc7
Tita	Tita	k1gMnSc1
LiviaFlorentské	LiviaFlorentský	k2eAgFnSc2d1
letopisyMandragora	letopisyMandragor	k1gMnSc2
(	(	kIx(
<g/>
komedie	komedie	k1gFnSc1
<g/>
)	)	kIx)
Rodiče	rodič	k1gMnPc1
</s>
<s>
Bernardo	Bernardo	k1gNnSc1
di	di	k?
Niccolò	Niccolò	k1gMnSc1
Machiavelli	Machiavelli	k1gMnSc1
Podpis	podpis	k1gInSc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
galerie	galerie	k1gFnSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
citáty	citát	k1gInPc1
na	na	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Niccolò	Niccolò	k?
Machiavelli	Machiavelli	k1gMnSc1
[	[	kIx(
<g/>
makjaveli	makjavet	k5eAaBmAgMnP,k5eAaImAgMnP,k5eAaPmAgMnP
<g/>
]	]	kIx)
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1469	#num#	k4
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1527	#num#	k4
<g/>
,	,	kIx,
Florencie	Florencie	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
italský	italský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
filozof	filozof	k1gMnSc1
<g/>
,	,	kIx,
diplomat	diplomat	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
historik	historik	k1gMnSc1
<g/>
,	,	kIx,
vojenský	vojenský	k2eAgMnSc1d1
teoretik	teoretik	k1gMnSc1
a	a	k8xC
polyhistor	polyhistor	k1gMnSc1
žijící	žijící	k2eAgMnSc1d1
v	v	k7c6
období	období	k1gNnSc6
renesance	renesance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
je	být	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
za	za	k7c2
zakladatele	zakladatel	k1gMnSc2
politologie	politologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svých	svůj	k3xOyFgInPc6
spisech	spis	k1gInPc6
formuloval	formulovat	k5eAaImAgInS
politicko-mocenský	politicko-mocenský	k2eAgInSc4d1
ideál	ideál	k1gInSc4
a	a	k8xC
prostředky	prostředek	k1gInPc4
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
silného	silný	k2eAgMnSc2d1
<g/>
,	,	kIx,
jednotného	jednotný	k2eAgInSc2d1
státu	stát	k1gInSc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
osvíceným	osvícený	k2eAgMnSc7d1
vladařem	vladař	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
notářské	notářský	k2eAgFnSc2d1
<g/>
,	,	kIx,
ne	ne	k9
příliš	příliš	k6eAd1
zámožné	zámožný	k2eAgFnSc2d1
florentské	florentský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
otec	otec	k1gMnSc1
Bernardo	Bernardo	k1gNnSc4
byl	být	k5eAaImAgMnS
právníkem	právník	k1gMnSc7
<g/>
,	,	kIx,
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
ženou	žena	k1gFnSc7
Bartolomeou	Bartolomea	k1gFnSc7
měl	mít	k5eAaImAgMnS
kromě	kromě	k7c2
Niccola	Niccola	k1gFnSc1
ještě	ještě	k6eAd1
syna	syn	k1gMnSc2
Totta	Tott	k1gMnSc2
a	a	k8xC
dcery	dcera	k1gFnSc2
Primaveru	Primaver	k1gInSc2
a	a	k8xC
Ginevru	Ginevr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Niccolovi	Niccol	k1gMnSc3
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
humanitního	humanitní	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
myšlení	myšlení	k1gNnSc4
formovala	formovat	k5eAaImAgFnS
díla	dílo	k1gNnPc4
antických	antický	k2eAgMnPc2d1
klasiků	klasik	k1gMnPc2
<g/>
,	,	kIx,
především	především	k9
Aristotela	Aristoteles	k1gMnSc4
<g/>
,	,	kIx,
Cicerona	Cicero	k1gMnSc4
a	a	k8xC
Tita	Titus	k1gMnSc4
Livia	Livius	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1498	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
pádu	pád	k1gInSc6
Savonarolova	Savonarolův	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Machiavelli	Machiavelli	k1gMnSc1
19	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
jmenován	jmenovat	k5eAaBmNgInS,k5eAaImNgInS
do	do	k7c2
funkce	funkce	k1gFnSc2
kancléře	kancléř	k1gMnSc2
florentské	florentský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalších	další	k2eAgNnPc2d1
třináct	třináct	k4xCc4
let	léto	k1gNnPc2
vedl	vést	k5eAaImAgInS
druhou	druhý	k4xOgFnSc4
kancelář	kancelář	k1gFnSc4
sekretariátu	sekretariát	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zabývala	zabývat	k5eAaImAgFnS
správou	správa	k1gFnSc7
zahraničněpolitických	zahraničněpolitický	k2eAgFnPc2d1
a	a	k8xC
vojenských	vojenský	k2eAgFnPc2d1
záležitostí	záležitost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
skvělý	skvělý	k2eAgMnSc1d1
diplomat	diplomat	k1gMnSc1
a	a	k8xC
schopný	schopný	k2eAgMnSc1d1
úředník	úředník	k1gMnSc1
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
mnoha	mnoho	k4c2
jednání	jednání	k1gNnPc2
i	i	k8xC
zahraničních	zahraniční	k2eAgFnPc2d1
misí	mise	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Machiavelli	Machiavelli	k1gMnSc1
byl	být	k5eAaImAgMnS
čtyřikrát	čtyřikrát	k6eAd1
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
a	a	k8xC
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
plnil	plnit	k5eAaImAgInS
poslání	poslání	k1gNnSc4
i	i	k8xC
k	k	k7c3
papežskému	papežský	k2eAgInSc3d1
dvoru	dvůr	k1gInSc3
Julia	Julius	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
komisařem	komisař	k1gMnSc7
pro	pro	k7c4
válku	válka	k1gFnSc4
proti	proti	k7c3
Pise	Pisa	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
těchto	tento	k3xDgFnPc6
příležitostech	příležitost	k1gFnPc6
pořizoval	pořizovat	k5eAaImAgInS
podrobné	podrobný	k2eAgInPc4d1
záznamy	záznam	k1gInPc4
z	z	k7c2
jednání	jednání	k1gNnSc2
a	a	k8xC
zpracoval	zpracovat	k5eAaPmAgInS
řadu	řada	k1gFnSc4
dokumentů	dokument	k1gInPc2
obsahujících	obsahující	k2eAgInPc2d1
analýzy	analýza	k1gFnPc4
politické	politický	k2eAgFnSc2d1
situace	situace	k1gFnSc2
v	v	k7c6
příslušných	příslušný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
jako	jako	k8xS,k8xC
Zpráva	zpráva	k1gFnSc1
o	o	k7c6
německých	německý	k2eAgFnPc6d1
záležitostech	záležitost	k1gFnPc6
(	(	kIx(
<g/>
Rapporto	Rapporta	k1gFnSc5
delle	delle	k1gNnPc4
cose	cos	k1gMnSc2
della	dell	k1gMnSc2
Magna	Magn	k1gMnSc2
<g/>
,	,	kIx,
1508	#num#	k4
<g/>
)	)	kIx)
nebo	nebo	k8xC
Popis	popis	k1gInSc4
německých	německý	k2eAgFnPc2d1
záležitostí	záležitost	k1gFnPc2
(	(	kIx(
<g/>
Ritratto	Ritratt	k2eAgNnSc1d1
delle	delle	k1gNnSc1
cose	cos	k1gMnSc2
della	dell	k1gMnSc2
Magna	Magn	k1gMnSc2
<g/>
,	,	kIx,
1508	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Byly	být	k5eAaImAgFnP
také	také	k9
podkladem	podklad	k1gInSc7
pro	pro	k7c4
jeho	jeho	k3xOp3gFnPc4
pozdější	pozdní	k2eAgFnPc4d2
díla	dílo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1500	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
poprvé	poprvé	k6eAd1
otcem	otec	k1gMnSc7
a	a	k8xC
rok	rok	k1gInSc4
poté	poté	k6eAd1
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Mariettou	Marietta	k1gFnSc7
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
Bartolomea	Bartolomeus	k1gMnSc2
Corsiniho	Corsini	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
Niccolovi	Niccolův	k2eAgMnPc1d1
porodila	porodit	k5eAaPmAgFnS
nakonec	nakonec	k6eAd1
šest	šest	k4xCc4
dětí	dítě	k1gFnPc2
a	a	k8xC
podle	podle	k7c2
manželových	manželův	k2eAgInPc2d1
dopisů	dopis	k1gInPc2
k	k	k7c3
ní	on	k3xPp3gFnSc3
Niccolò	Niccolò	k1gFnSc3
nikdy	nikdy	k6eAd1
neztratil	ztratit	k5eNaPmAgMnS
náklonnost	náklonnost	k1gFnSc4
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
jí	on	k3xPp3gFnSc3
byl	být	k5eAaImAgInS
mnohokrát	mnohokrát	k6eAd1
nevěrný	věrný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
politickém	politický	k2eAgInSc6d1
převratu	převrat	k1gInSc6
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1512	#num#	k4
do	do	k7c2
Florencie	Florencie	k1gFnSc2
vrátili	vrátit	k5eAaPmAgMnP
Medicejové	Medicejové	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
v	v	k7c6
roce	rok	k1gInSc6
1513	#num#	k4
dali	dát	k5eAaPmAgMnP
Machiavelliho	Machiavelli	k1gMnSc4
kvůli	kvůli	k7c3
podezření	podezření	k1gNnSc3
ze	z	k7c2
spiknutí	spiknutí	k1gNnSc2
proti	proti	k7c3
nim	on	k3xPp3gMnPc3
zatknout	zatknout	k5eAaPmF
<g/>
,	,	kIx,
vyslýchat	vyslýchat	k5eAaImF
a	a	k8xC
mučit	mučit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
zbaven	zbavit	k5eAaPmNgMnS
všech	všecek	k3xTgFnPc2
funkcí	funkce	k1gFnPc2
a	a	k8xC
odsouzen	odsouzet	k5eAaImNgInS,k5eAaPmNgInS
k	k	k7c3
vyhnanství	vyhnanství	k1gNnSc3
za	za	k7c2
hradby	hradba	k1gFnSc2
města	město	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
nesmí	smět	k5eNaImIp3nS
opustit	opustit	k5eAaPmF
území	území	k1gNnSc4
Florencie	Florencie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Machiavelli	Machiavelli	k1gMnSc1
žil	žít	k5eAaImAgMnS
s	s	k7c7
rodinou	rodina	k1gFnSc7
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
vile	vila	k1gFnSc6
v	v	k7c4
Albergaccio	Albergaccio	k1gNnSc4
u	u	k7c2
San	San	k1gFnSc2
Cascania	Cascanium	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
studiu	studio	k1gNnSc3
literatury	literatura	k1gFnSc2
a	a	k8xC
vlastní	vlastní	k2eAgFnSc2d1
literární	literární	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
období	období	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
žil	žít	k5eAaImAgMnS
v	v	k7c6
politické	politický	k2eAgFnSc6d1
nemilosti	nemilost	k1gFnSc6
<g/>
,	,	kIx,
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
svá	svůj	k3xOyFgNnPc4
nejvýznamnější	významný	k2eAgNnPc4d3
díla	dílo	k1gNnPc4
<g/>
:	:	kIx,
Vladař	vladařit	k5eAaImRp2nS
(	(	kIx(
<g/>
Il	Il	k1gMnSc1
Principe	princip	k1gInSc5
<g/>
)	)	kIx)
a	a	k8xC
Rozpravy	rozprava	k1gFnPc1
na	na	k7c6
prvními	první	k4xOgMnPc7
deseti	deset	k4xCc7
knihami	kniha	k1gFnPc7
Tita	Titus	k1gMnSc2
Livia	Livius	k1gMnSc2
(	(	kIx(
<g/>
I	i	k9
Discorsi	Discorse	k1gFnSc4
sopra	sopro	k1gNnSc2
la	la	k1gNnSc2
prima	prima	k2eAgNnSc2d1
deca	decum	k1gNnSc2
di	di	k?
Tito	tento	k3xDgMnPc1
Livio	Livio	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občas	občas	k6eAd1
zajížděl	zajíždět	k5eAaImAgMnS
do	do	k7c2
Florencie	Florencie	k1gFnSc2
na	na	k7c4
schůzky	schůzka	k1gFnPc4
s	s	k7c7
mladými	mladý	k2eAgMnPc7d1
politiky	politik	k1gMnPc7
a	a	k8xC
literáty	literát	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
také	také	k6eAd1
komedii	komedie	k1gFnSc4
Mandragora	mandragora	k1gFnSc1
(	(	kIx(
<g/>
Mandragola	Mandragola	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
novely	novela	k1gFnSc2
Arciďábel	arciďábel	k1gMnSc1
Belfagor	Belfagor	k1gMnSc1
(	(	kIx(
<g/>
Belfagor	Belfagor	k1gMnSc1
Arcidiavolo	Arcidiavola	k1gFnSc5
<g/>
)	)	kIx)
a	a	k8xC
Zlatý	zlatý	k2eAgMnSc1d1
osel	osel	k1gMnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
’	’	k?
<g/>
asino	asino	k6eAd1
d	d	k?
<g/>
’	’	k?
<g/>
oro	oro	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Machiavelli	Machiavelli	k1gMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
získat	získat	k5eAaPmF
přízeň	přízeň	k1gFnSc4
Medicejských	Medicejský	k2eAgInPc2d1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1520	#num#	k4
byl	být	k5eAaImAgMnS
skutečně	skutečně	k6eAd1
omilostněn	omilostnit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
Florencie	Florencie	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
jejich	jejich	k3xOp3gMnSc7
oficiálním	oficiální	k2eAgMnSc7d1
dějepiscem	dějepisec	k1gMnSc7
a	a	k8xC
byl	být	k5eAaImAgMnS
pověřen	pověřit	k5eAaPmNgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
Florentské	florentský	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
(	(	kIx(
<g/>
Istorie	Istorie	k1gFnPc4
Fiorentine	Fiorentin	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
osmidílném	osmidílný	k2eAgNnSc6d1
vědeckém	vědecký	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
1520	#num#	k4
až	až	k9
1525	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1521	#num#	k4
dokončil	dokončit	k5eAaPmAgInS
spis	spis	k1gInSc4
Rozhovory	rozhovor	k1gInPc4
o	o	k7c6
umění	umění	k1gNnSc6
válečném	válečný	k2eAgNnSc6d1
(	(	kIx(
<g/>
Dialoghi	Dialoghi	k1gNnSc6
dell	delnout	k5eAaPmAgMnS
<g/>
’	’	k?
<g/>
arte	art	k1gMnSc4
della	dell	k1gMnSc4
guerra	guerr	k1gMnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
sklonku	sklonek	k1gInSc6
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
života	život	k1gInSc2
nabídli	nabídnout	k5eAaPmAgMnP
Medicejští	Medicejský	k2eAgMnPc1d1
Machiavellimu	Machiavelli	k1gMnSc3
roku	rok	k1gInSc2
1526	#num#	k4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
řídil	řídit	k5eAaImAgMnS
novou	nový	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
městského	městský	k2eAgNnSc2d1
opevnění	opevnění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1527	#num#	k4
však	však	k9
vláda	vláda	k1gFnSc1
Medicejských	Medicejský	k2eAgMnPc2d1
padla	padnout	k5eAaImAgFnS,k5eAaPmAgFnS
a	a	k8xC
nová	nový	k2eAgFnSc1d1
republikánská	republikánský	k2eAgFnSc1d1
garnitura	garnitura	k1gFnSc1
zbavila	zbavit	k5eAaPmAgFnS
Machiavelliho	Machiavelli	k1gMnSc4
úřadu	úřada	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Machiavelli	Machiavelli	k1gMnSc1
se	se	k3xPyFc4
ještě	ještě	k6eAd1
pokusil	pokusit	k5eAaPmAgMnS
znovu	znovu	k6eAd1
získat	získat	k5eAaPmF
místo	místo	k1gNnSc4
sekretáře	sekretář	k1gMnSc2
<g/>
,	,	kIx,
neuspěl	uspět	k5eNaPmAgMnS
však	však	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tomto	tento	k3xDgNnSc6
odmítnutí	odmítnutí	k1gNnSc6
těžce	těžce	k6eAd1
onemocněl	onemocnět	k5eAaPmAgInS
<g/>
,	,	kIx,
patrně	patrně	k6eAd1
na	na	k7c4
žaludeční	žaludeční	k2eAgInPc4d1
vředy	vřed	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1527	#num#	k4
duševně	duševně	k6eAd1
zlomen	zlomen	k2eAgMnSc1d1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohřben	pohřbít	k5eAaPmNgInS
je	být	k5eAaImIp3nS
ve	v	k7c6
florentském	florentský	k2eAgInSc6d1
chrámu	chrám	k1gInSc6
Santa	Santa	k1gMnSc1
Croce	Croce	k1gMnSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
například	například	k6eAd1
Galileo	Galilea	k1gFnSc5
Galilei	Galilei	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Niccolò	Niccolò	k?
Machiavelli	Machiavelli	k1gMnSc1
</s>
<s>
Politická	politický	k2eAgFnSc1d1
filozofie	filozofie	k1gFnSc1
a	a	k8xC
pojetí	pojetí	k1gNnSc1
člověka	člověk	k1gMnSc2
</s>
<s>
K	k	k7c3
novému	nový	k2eAgNnSc3d1
originálnímu	originální	k2eAgNnSc3d1
zkoumání	zkoumání	k1gNnSc3
politického	politický	k2eAgInSc2d1
fenoménu	fenomén	k1gInSc2
<g/>
,	,	kIx,
k	k	k7c3
pokusu	pokus	k1gInSc3
o	o	k7c4
novou	nový	k2eAgFnSc4d1
politickou	politický	k2eAgFnSc4d1
filozofii	filozofie	k1gFnSc4
<g/>
,	,	kIx,
vedlo	vést	k5eAaImAgNnS
Machiavelliho	Machiavelli	k1gMnSc4
právě	právě	k6eAd1
jeho	jeho	k3xOp3gNnSc4
vyřazení	vyřazení	k1gNnSc4
z	z	k7c2
politického	politický	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdyby	kdyby	k9
zůstal	zůstat	k5eAaPmAgInS
v	v	k7c6
politickém	politický	k2eAgNnSc6d1
dění	dění	k1gNnSc6
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
by	by	kYmCp3nS
nebyl	být	k5eNaImAgMnS
měl	mít	k5eAaImAgMnS
odvahu	odvaha	k1gFnSc4
být	být	k5eAaImF
tak	tak	k9
bezohledně	bezohledně	k6eAd1
kritický	kritický	k2eAgMnSc1d1
k	k	k7c3
politice	politika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Machiavelliho	Machiavelli	k1gMnSc2
je	být	k5eAaImIp3nS
politika	politika	k1gFnSc1
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
zacházet	zacházet	k5eAaImF
s	s	k7c7
mocí	moc	k1gFnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
dosáhlo	dosáhnout	k5eAaPmAgNnS
úspěchu	úspěch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moc	moc	k6eAd1
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
nemá	mít	k5eNaImIp3nS
žádné	žádný	k3yNgNnSc1
teologické	teologický	k2eAgNnSc1d1
ani	ani	k8xC
etické	etický	k2eAgNnSc1d1
zdůvodnění	zdůvodnění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nP
vládnoucí	vládnoucí	k2eAgMnPc1d1
zvítězili	zvítězit	k5eAaPmAgMnP
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
bezohlední	bezohledný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politická	politický	k2eAgFnSc1d1
věda	věda	k1gFnSc1
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
tedy	tedy	k9
zabývat	zabývat	k5eAaImF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
nikoli	nikoli	k9
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
nová	nový	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
filozofie	filozofie	k1gFnSc1
<g/>
,	,	kIx,
zcela	zcela	k6eAd1
oddělující	oddělující	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
od	od	k7c2
etiky	etika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
pro	pro	k7c4
Machiavelliho	Machiavelli	k1gMnSc4
úvahy	úvaha	k1gFnSc2
typické	typický	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
přesvědčení	přesvědčení	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
když	když	k8xS
se	se	k3xPyFc4
časy	čas	k1gInPc1
mění	měnit	k5eAaImIp3nP
<g/>
,	,	kIx,
lidé	člověk	k1gMnPc1
zůstávají	zůstávat	k5eAaImIp3nP
v	v	k7c6
podstatě	podstata	k1gFnSc6
stejní	stejný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepřijímá	přijímat	k5eNaImIp3nS
tedy	tedy	k9
myšlenku	myšlenka	k1gFnSc4
o	o	k7c4
zdokonalování	zdokonalování	k1gNnSc4
lidstva	lidstvo	k1gNnSc2
a	a	k8xC
člověka	člověk	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
lidí	člověk	k1gMnPc2
se	se	k3xPyFc4
podle	podle	k7c2
Machiavelliho	Machiavelli	k1gMnSc2
dá	dát	k5eAaPmIp3nS
očekávat	očekávat	k5eAaImF
spíš	spíš	k9
zlo	zlo	k1gNnSc1
<g/>
;	;	kIx,
k	k	k7c3
dobrému	dobré	k1gNnSc3
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
vedeni	vést	k5eAaImNgMnP
nebo	nebo	k8xC
i	i	k8xC
donucováni	donucován	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
dílu	díl	k1gInSc6
Vladař	vladař	k1gMnSc1
vkládá	vkládat	k5eAaImIp3nS
do	do	k7c2
lidí	člověk	k1gMnPc2
jistou	jistý	k2eAgFnSc4d1
důvěru	důvěra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Učení	učení	k1gNnSc1
o	o	k7c6
ctnosti	ctnost	k1gFnSc6
</s>
<s>
S	s	k7c7
pojmem	pojem	k1gInSc7
ctnost	ctnost	k1gFnSc1
(	(	kIx(
<g/>
virtù	virtù	k?
<g/>
)	)	kIx)
se	se	k3xPyFc4
pracovalo	pracovat	k5eAaImAgNnS
už	už	k6eAd1
v	v	k7c6
antice	antika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Machiavelli	Machiavelli	k1gMnSc1
však	však	k9
její	její	k3xOp3gNnSc4
pojetí	pojetí	k1gNnSc4
zcela	zcela	k6eAd1
mění	měnit	k5eAaImIp3nS
–	–	k?
souhlasí	souhlasit	k5eAaImIp3nP
sice	sice	k8xC
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
soubor	soubor	k1gInSc4
vlastností	vlastnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
vladaři	vladař	k1gMnSc3
získat	získat	k5eAaPmF
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
Štěstěnu	Štěstěna	k1gFnSc4
a	a	k8xC
tím	ten	k3xDgNnSc7
získat	získat	k5eAaPmF
slávu	sláva	k1gFnSc4
<g/>
,	,	kIx,
nespojuje	spojovat	k5eNaImIp3nS
ji	on	k3xPp3gFnSc4
ale	ale	k9
s	s	k7c7
konkrétními	konkrétní	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
určujícím	určující	k2eAgInSc7d1
rysem	rys	k1gInSc7
je	být	k5eAaImIp3nS
ochota	ochota	k1gFnSc1
udělat	udělat	k5eAaPmF
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
diktuje	diktovat	k5eAaImIp3nS
nutnost	nutnost	k1gFnSc1
–	–	k?
ať	ať	k9
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
čin	čin	k1gInSc1
ničemný	ničemný	k2eAgInSc1d1
nebo	nebo	k8xC
ctnostný	ctnostný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Virtù	Virtù	k1gMnSc1
tedy	tedy	k9
označuje	označovat	k5eAaImIp3nS
požadovanou	požadovaný	k2eAgFnSc4d1
vlastnost	vlastnost	k1gFnSc4
mravní	mravní	k2eAgFnSc2d1
pružnosti	pružnost	k1gFnSc2
vladaře	vladař	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mít	mít	k5eAaImF
virtù	virtù	k?
tedy	tedy	k9
znamená	znamenat	k5eAaImIp3nS
být	být	k5eAaImF
ochoten	ochoten	k2eAgMnSc1d1
udělat	udělat	k5eAaPmF
všechno	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
pro	pro	k7c4
dosažení	dosažení	k1gNnSc4
slávy	sláva	k1gFnSc2
a	a	k8xC
velikosti	velikost	k1gFnSc2
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
činy	čin	k1gInPc1
samy	sám	k3xTgInPc1
o	o	k7c6
sobě	sebe	k3xPyFc6
dobré	dobré	k1gNnSc4
nebo	nebo	k8xC
zlé	zlá	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chce	chtít	k5eAaImIp3nS
<g/>
-li	-li	k?
vládce	vládce	k1gMnSc1
dosáhnout	dosáhnout	k5eAaPmF
nejvyšších	vysoký	k2eAgInPc2d3
cílů	cíl	k1gInPc2
<g/>
,	,	kIx,
nemá	mít	k5eNaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
pokoušet	pokoušet	k5eAaImF
jednat	jednat	k5eAaImF
v	v	k7c6
první	první	k4xOgFnSc6
řadě	řada	k1gFnSc6
mravně	mravně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Virtù	Virtù	k1gMnSc1
má	mít	k5eAaImIp3nS
původ	původ	k1gInSc4
v	v	k7c6
dobré	dobrý	k2eAgFnSc6d1
výchově	výchova	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
zase	zase	k9
původ	původ	k1gInSc4
v	v	k7c6
dobrých	dobrý	k2eAgInPc6d1
zákonech	zákon	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
s	s	k7c7
virtù	virtù	k?
se	se	k3xPyFc4
neslučuje	slučovat	k5eNaImIp3nS
např.	např.	kA
bohatství	bohatství	k1gNnSc1
<g/>
,	,	kIx,
rozvoj	rozvoj	k1gInSc1
technických	technický	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
(	(	kIx(
<g/>
občané	občan	k1gMnPc1
mají	mít	k5eAaImIp3nP
mít	mít	k5eAaImF
vlastní	vlastní	k2eAgFnSc3d1
branné	branný	k2eAgFnSc3d1
schopnosti	schopnost	k1gFnSc3
<g/>
)	)	kIx)
a	a	k8xC
rozvoji	rozvoj	k1gInSc3
vědy	věda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
rozvoj	rozvoj	k1gInSc4
vědy	věda	k1gFnSc2
má	mít	k5eAaImIp3nS
bránit	bránit	k5eAaImF
lidstvu	lidstvo	k1gNnSc3
vrátit	vrátit	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
svého	svůj	k3xOyFgInSc2
původního	původní	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
měl	mít	k5eAaImAgInS
znamenat	znamenat	k5eAaImF
návrat	návrat	k1gInSc1
k	k	k7c3
ctnosti	ctnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
návratem	návrat	k1gInSc7
Machiavelli	Machiavelli	k1gMnSc1
chápal	chápat	k5eAaImAgMnS
především	především	k9
návrat	návrat	k1gInSc4
k	k	k7c3
původnímu	původní	k2eAgInSc3d1
městskému	městský	k2eAgInSc3d1
státu	stát	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Vztah	vztah	k1gInSc1
ke	k	k7c3
křesťanství	křesťanství	k1gNnSc3
</s>
<s>
Machiavelliho	Machiavelli	k1gMnSc4
názor	názor	k1gInSc4
na	na	k7c6
víru	vír	k1gInSc6
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
hodnocení	hodnocení	k1gNnSc6
náboženství	náboženství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Negativní	negativní	k2eAgInSc1d1
přístup	přístup	k1gInSc1
má	mít	k5eAaImIp3nS
ke	k	k7c3
křesťanství	křesťanství	k1gNnSc3
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
těch	ten	k3xDgFnPc2
jeho	jeho	k3xOp3gFnPc2
forem	forma	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
hlásají	hlásat	k5eAaImIp3nP
návrat	návrat	k1gInSc4
k	k	k7c3
chudobě	chudoba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křesťanství	křesťanství	k1gNnSc1
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
oslabilo	oslabit	k5eAaPmAgNnS
antickou	antický	k2eAgFnSc4d1
představu	představa	k1gFnSc4
o	o	k7c6
státu	stát	k1gInSc6
<g/>
,	,	kIx,
zničilo	zničit	k5eAaPmAgNnS
víru	víra	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
vyvinula	vyvinout	k5eAaPmAgFnS
především	především	k9
v	v	k7c6
době	doba	k1gFnSc6
republikánského	republikánský	k2eAgInSc2d1
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
víru	víra	k1gFnSc4
ve	v	k7c4
velikost	velikost	k1gFnSc4
ducha	duch	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
změkčilé	změkčilý	k2eAgNnSc1d1
náboženství	náboženství	k1gNnSc1
odnaučilo	odnaučit	k5eAaPmAgNnS
lidi	člověk	k1gMnPc4
statečnosti	statečnost	k1gFnSc2
a	a	k8xC
ctnosti	ctnost	k1gFnSc2
<g/>
,	,	kIx,
úmyslně	úmyslně	k6eAd1
odvádělo	odvádět	k5eAaImAgNnS
pozornost	pozornost	k1gFnSc4
lidí	člověk	k1gMnPc2
od	od	k7c2
záležitostí	záležitost	k1gFnPc2
tohoto	tento	k3xDgInSc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Instituci	instituce	k1gFnSc6
církve	církev	k1gFnSc2
podroboval	podrobovat	k5eAaImAgMnS
přísné	přísný	k2eAgFnSc3d1
kritice	kritika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
způsobila	způsobit	k5eAaPmAgFnS
morální	morální	k2eAgFnSc4d1
zkázu	zkáza	k1gFnSc4
Itálie	Itálie	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gMnPc1
hodnostáři	hodnostář	k1gMnPc1
upřednostňují	upřednostňovat	k5eAaImIp3nP
své	svůj	k3xOyFgInPc4
osobní	osobní	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
na	na	k7c4
úkor	úkor	k1gInSc4
svých	svůj	k3xOyFgFnPc2
základních	základní	k2eAgFnPc2d1
povinností	povinnost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Místo	místo	k7c2
politické	politický	k2eAgFnSc2d1
morálky	morálka	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
hlásala	hlásat	k5eAaImAgFnS
statečnost	statečnost	k1gFnSc4
<g/>
,	,	kIx,
zavedli	zavést	k5eAaPmAgMnP
morálku	morálka	k1gFnSc4
útrpnosti	útrpnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
lidé	člověk	k1gMnPc1
přestali	přestat	k5eAaPmAgMnP
mít	mít	k5eAaImF
zájem	zájem	k1gInSc4
o	o	k7c4
politiku	politika	k1gFnSc4
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zavedení	zavedení	k1gNnSc3
zločineckých	zločinecký	k2eAgFnPc2d1
forem	forma	k1gFnPc2
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
</s>
<s>
Discorso	Discorsa	k1gFnSc5
fatto	fatta	k1gFnSc5
al	ala	k1gFnPc2
magistrato	magistrato	k6eAd1
de	de	k?
<g/>
'	'	kIx"
Dieci	Diece	k1gFnSc3
sopra	sopro	k1gNnSc2
le	le	k?
cose	cosat	k5eAaPmIp3nS
di	di	k?
Pisa	Pisa	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1499	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Parole	parole	k1gFnSc1
da	da	k?
dirle	dirle	k1gFnSc1
sopra	sopr	k1gInSc2
la	la	k1gNnSc2
provvisione	provvision	k1gInSc5
del	del	k?
danaio	danaio	k1gNnSc4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1503	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Descrizione	Descrizion	k1gInSc5
del	del	k?
modo	modo	k1gMnSc1
tenuto	tenuto	k6eAd1
dal	dát	k5eAaPmAgMnS
Duca	Duca	k1gMnSc1
Valentino	Valentina	k1gFnSc5
nello	lnout	k5eNaImAgNnS,k5eNaPmAgNnS
ammazzare	ammazzar	k1gMnSc5
Vitellozzo	Vitellozza	k1gFnSc5
Vitelli	Vitell	k1gMnSc6
<g/>
,	,	kIx,
Oliverotto	Oliverott	k2eAgNnSc1d1
da	da	k?
Fermo	Ferma	k1gFnSc5
<g/>
,	,	kIx,
il	il	k?
Signor	signor	k1gMnSc1
Pagolo	Pagola	k1gFnSc5
e	e	k0
il	il	k?
duca	duc	k2eAgFnSc1d1
di	di	k?
Gravina	Gravina	k1gFnSc1
Orsini	Orsin	k2eAgMnPc1d1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1503	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ritratto	Ritratt	k2eAgNnSc1d1
delle	delle	k1gNnSc1
cose	cose	k1gNnSc2
di	di	k?
Francia	francium	k1gNnSc2
(	(	kIx(
<g/>
1510	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ritratto	Ritratt	k2eAgNnSc1d1
delle	delle	k1gNnSc1
cose	cos	k1gMnSc2
della	dell	k1gMnSc2
Magna	Magn	k1gMnSc2
(	(	kIx(
<g/>
1512	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Il	Il	k?
Principe	princip	k1gInSc5
(	(	kIx(
<g/>
1513	#num#	k4
<g/>
,	,	kIx,
vydáno	vydat	k5eAaPmNgNnS
1532	#num#	k4
<g/>
)	)	kIx)
=	=	kIx~
Vladař	vladař	k1gMnSc1
</s>
<s>
Discorsi	Discorse	k1gFnSc4
sopra	sopro	k1gNnSc2
la	la	k1gNnSc2
prima	prima	k2eAgNnSc2d1
deca	decum	k1gNnSc2
di	di	k?
Tito	tento	k3xDgMnPc1
Livio	Livio	k6eAd1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1513	#num#	k4
<g/>
–	–	k?
<g/>
1519	#num#	k4
<g/>
)	)	kIx)
=	=	kIx~
Rozpravy	rozprava	k1gFnPc1
nad	nad	k7c7
prvními	první	k4xOgInPc7
deseti	deset	k4xCc7
knihami	kniha	k1gFnPc7
Tita	Titus	k1gMnSc2
Livia	Livius	k1gMnSc2
</s>
<s>
Dell	Dell	kA
<g/>
'	'	kIx"
<g/>
arte	artat	k5eAaPmIp3nS
della	della	k1gMnSc1
guerra	guerra	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1516	#num#	k4
–	–	k?
1520	#num#	k4
<g/>
)	)	kIx)
=	=	kIx~
O	o	k7c6
umění	umění	k1gNnSc6
válečném	válečný	k2eAgNnSc6d1
</s>
<s>
La	la	k1gNnPc1
vita	vit	k2eAgNnPc1d1
di	di	k?
Castruccio	Castruccio	k1gMnSc1
Castracani	Castracan	k1gMnPc1
da	da	k?
Lucca	Lucc	k1gInSc2
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1520	#num#	k4
<g/>
)	)	kIx)
=	=	kIx~
Život	život	k1gInSc1
Castruccia	Castruccius	k1gMnSc2
Castracaniho	Castracani	k1gMnSc2
z	z	k7c2
Lukky	Lukka	k1gFnSc2
</s>
<s>
Istorie	Istorie	k1gFnSc1
Fiorentine	Fiorentin	k1gInSc5
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1520	#num#	k4
–	–	k?
1525	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vydáno	vydat	k5eAaPmNgNnS
1532	#num#	k4
<g/>
)	)	kIx)
=	=	kIx~
Florentské	florentský	k2eAgInPc1d1
letopisy	letopis	k1gInPc1
</s>
<s>
Decennali	Decennat	k5eAaImAgMnP,k5eAaPmAgMnP
</s>
<s>
komedie	komedie	k1gFnSc1
La	la	k1gNnSc2
mandragola	mandragola	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1513	#num#	k4
<g/>
)	)	kIx)
=	=	kIx~
Mandragora	mandragora	k1gFnSc1
(	(	kIx(
<g/>
1520	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
novela	novela	k1gFnSc1
Belfagor	Belfagora	k1gFnPc2
arcidiavolo	arcidiavola	k1gFnSc5
(	(	kIx(
<g/>
1519	#num#	k4
<g/>
)	)	kIx)
=	=	kIx~
Belfagor	Belfagor	k1gInSc1
</s>
<s>
Epistolario	Epistolario	k1gNnSc1
(	(	kIx(
<g/>
1497	#num#	k4
–	–	k?
1527	#num#	k4
<g/>
)	)	kIx)
–	–	k?
korespondence	korespondence	k1gFnSc2
(	(	kIx(
<g/>
poprvé	poprvé	k6eAd1
vydáno	vydat	k5eAaPmNgNnS
až	až	k9
1883	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
L	L	kA
<g/>
'	'	kIx"
<g/>
asino	asino	k6eAd1
</s>
<s>
Andria	Andrium	k1gNnPc1
<g/>
,	,	kIx,
přepracování	přepracování	k1gNnSc1
překladu	překlad	k1gInSc2
Terenciovy	Terenciův	k2eAgFnSc2d1
Andrie	Andrie	k1gFnSc2
</s>
<s>
Clizia	Clizia	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1525	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vladař	vladař	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
útlý	útlý	k2eAgInSc1d1
spis	spis	k1gInSc1
obsahující	obsahující	k2eAgInSc1d1
26	#num#	k4
krátkých	krátká	k1gFnPc2
kapitol	kapitola	k1gFnPc2
vyšel	vyjít	k5eAaPmAgInS
poprvé	poprvé	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1532	#num#	k4
až	až	k6eAd1
po	po	k7c6
smrti	smrt	k1gFnSc6
Machiavelliho	Machiavelli	k1gMnSc2
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
jej	on	k3xPp3gMnSc4
začal	začít	k5eAaPmAgMnS
psát	psát	k5eAaImF
už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1513	#num#	k4
po	po	k7c6
vykázání	vykázání	k1gNnSc6
z	z	k7c2
Florencie	Florencie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
dílo	dílo	k1gNnSc4
v	v	k7c6
úvodu	úvod	k1gInSc6
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
Lorenzovi	Lorenz	k1gMnSc3
Medicejskému	Medicejský	k2eAgMnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
klíčové	klíčový	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
politické	politický	k2eAgFnSc2d1
úvahy	úvaha	k1gFnSc2
odhalující	odhalující	k2eAgInPc1d1
a	a	k8xC
popisující	popisující	k2eAgInPc1d1
mechanismy	mechanismus	k1gInPc1
fungování	fungování	k1gNnSc2
moci	moc	k1gFnSc2
a	a	k8xC
definují	definovat	k5eAaBmIp3nP
zásady	zásada	k1gFnPc1
Pragmatické	pragmatický	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
zaměřené	zaměřený	k2eAgFnSc2d1
na	na	k7c4
řešení	řešení	k1gNnSc4
konkrétních	konkrétní	k2eAgInPc2d1
problémů	problém	k1gInPc2
doby	doba	k1gFnSc2
a	a	k8xC
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
touží	toužit	k5eAaImIp3nS
po	po	k7c6
jednotném	jednotný	k2eAgInSc6d1
státu	stát	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
by	by	kYmCp3nS
byl	být	k5eAaImAgMnS
spravován	spravovat	k5eAaImNgMnS
moudrým	moudrý	k2eAgMnSc7d1
vládcem	vládce	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
by	by	kYmCp3nS
neměl	mít	k5eNaImAgMnS
váhat	váhat	k5eAaImF
použít	použít	k5eAaPmF
jakékoli	jakýkoli	k3yIgInPc4
prostředky	prostředek	k1gInPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zajistil	zajistit	k5eAaPmAgMnS
blaho	blaho	k1gNnSc4
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
myšlenky	myšlenka	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
člověk	člověk	k1gMnSc1
měl	mít	k5eAaImAgMnS
spoléhat	spoléhat	k5eAaImF
pouze	pouze	k6eAd1
na	na	k7c4
své	svůj	k3xOyFgFnPc4
síly	síla	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Recepce	recepce	k1gFnSc1
Machiavelliho	Machiavelli	k1gMnSc2
a	a	k8xC
úsudky	úsudek	k1gInPc7
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
politické	politický	k2eAgFnSc6d1
filozofii	filozofie	k1gFnSc6
byly	být	k5eAaImAgFnP
po	po	k7c4
vydání	vydání	k1gNnSc4
Vladaře	vladař	k1gMnSc2
velmi	velmi	k6eAd1
protichůdné	protichůdný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1532	#num#	k4
povolil	povolit	k5eAaPmAgMnS
vydat	vydat	k5eAaPmF
Machiavelliho	Machiavelli	k1gMnSc2
spisy	spis	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
dvacet	dvacet	k4xCc4
sedm	sedm	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
se	se	k3xPyFc4
Vladař	vladař	k1gMnSc1
ocitl	ocitnout	k5eAaPmAgMnS
na	na	k7c6
Indexu	index	k1gInSc6
zakázaných	zakázaný	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Jezuité	jezuita	k1gMnPc1
i	i	k8xC
protestanti	protestant	k1gMnPc1
psali	psát	k5eAaImAgMnP
proti	proti	k7c3
Machiavellimu	Machiavelli	k1gMnSc3
a	a	k8xC
snažili	snažit	k5eAaImAgMnP
se	se	k3xPyFc4
vyvrátit	vyvrátit	k5eAaPmF
jeho	jeho	k3xOp3gFnSc4
politickou	politický	k2eAgFnSc4d1
filozofii	filozofie	k1gFnSc4
v	v	k7c6
základě	základ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vycházeli	vycházet	k5eAaImAgMnP
z	z	k7c2
něho	on	k3xPp3gMnSc2
však	však	k9
první	první	k4xOgMnPc1
teoretikové	teoretik	k1gMnPc1
absolutismu	absolutismus	k1gInSc2
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
pak	pak	k6eAd1
Thomas	Thomas	k1gMnSc1
Hobbes	Hobbes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
osvícenci	osvícenec	k1gMnPc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
v	v	k7c6
Niccolovi	Niccol	k1gMnSc6
spatřovali	spatřovat	k5eAaImAgMnP
republikána	republikán	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charles	Charles	k1gMnSc1
Montesquieu	Montesquiea	k1gFnSc4
byl	být	k5eAaImAgMnS
dokonce	dokonce	k9
podezříván	podezřívat	k5eAaImNgMnS
<g/>
,	,	kIx,
že	že	k8xS
od	od	k7c2
něho	on	k3xPp3gMnSc2
opsal	opsat	k5eAaPmAgMnS
řadu	řada	k1gFnSc4
myšlenek	myšlenka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Vladaři	vladař	k1gMnSc6
Machiavelli	Machiavelli	k1gMnSc1
vůbec	vůbec	k9
poprvé	poprvé	k6eAd1
použil	použít	k5eAaPmAgInS
pojem	pojem	k1gInSc1
stát	stát	k1gInSc1
(	(	kIx(
<g/>
stato	stato	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
neužívalo	užívat	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říkalo	říkat	k5eAaImAgNnS
se	se	k3xPyFc4
politeia	politeia	k1gFnSc1
<g/>
,	,	kIx,
res	res	k?
publica	publicum	k1gNnSc2
<g/>
,	,	kIx,
civitas	civitas	k1gInSc1
<g/>
,	,	kIx,
regnum	regnum	k1gNnSc1
<g/>
,	,	kIx,
imperium	imperium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Dělení	dělení	k1gNnSc1
panství	panství	k1gNnSc2
</s>
<s>
V	v	k7c6
úvodní	úvodní	k2eAgFnSc6d1
kapitole	kapitola	k1gFnSc6
začíná	začínat	k5eAaImIp3nS
vyčleněním	vyčlenění	k1gNnSc7
pojmu	pojem	k1gInSc2
„	„	k?
<g/>
panství	panství	k1gNnSc1
<g/>
“	“	k?
a	a	k8xC
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
všechna	všechen	k3xTgNnPc1
panství	panství	k1gNnPc1
jsou	být	k5eAaImIp3nP
buď	buď	k8xC
republiky	republika	k1gFnPc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
suverénní	suverénní	k2eAgNnSc1d1
vladařství	vladařství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
předkládá	předkládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
všechna	všechen	k3xTgNnPc1
vladařství	vladařství	k1gNnPc1
jsou	být	k5eAaImIp3nP
buď	buď	k8xC
dědičná	dědičný	k2eAgFnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
nová	nový	k2eAgFnSc1d1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yRgInPc6,k3yQgInPc6
rozlišuje	rozlišovat	k5eAaImIp3nS
„	„	k?
<g/>
celá	celý	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
<g/>
“	“	k?
od	od	k7c2
těch	ten	k3xDgFnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
jsou	být	k5eAaImIp3nP
přičleněna	přičleněn	k2eAgNnPc1d1
k	k	k7c3
dědičnému	dědičný	k2eAgInSc3d1
státu	stát	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oněm	onen	k3xDgFnPc3
„	„	k?
<g/>
celým	celý	k2eAgFnPc3d1
novým	nový	k2eAgInSc7d1
<g/>
“	“	k?
se	se	k3xPyFc4
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
i	i	k9
v	v	k7c6
6	#num#	k4
<g/>
.	.	kIx.
kapitole	kapitola	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
místě	místo	k1gNnSc6
zároveň	zároveň	k6eAd1
uvede	uvést	k5eAaPmIp3nS
snad	snad	k9
nejdůležitější	důležitý	k2eAgFnSc4d3
antitezi	antiteze	k1gFnSc4
v	v	k7c6
celé	celá	k1gFnSc6
své	svůj	k3xOyFgFnSc6
politické	politický	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
–	–	k?
prohlašuje	prohlašovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nová	nový	k2eAgNnPc1d1
vladařství	vladařství	k1gNnPc1
se	se	k3xPyFc4
získávají	získávat	k5eAaImIp3nP
vlastními	vlastní	k2eAgFnPc7d1
zbraněmi	zbraň	k1gFnPc7
a	a	k8xC
schopnostmi	schopnost	k1gFnPc7
(	(	kIx(
<g/>
virtù	virtù	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
cizími	cizí	k2eAgFnPc7d1
zbraněmi	zbraň	k1gFnPc7
a	a	k8xC
štěstím	štěstí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc4
případ	případ	k1gInSc4
je	být	k5eAaImIp3nS
častější	častý	k2eAgNnSc1d2
<g/>
,	,	kIx,
a	a	k8xC
panovník	panovník	k1gMnSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
neobejde	obejde	k6eNd1
bez	bez	k7c2
odborného	odborný	k2eAgMnSc2d1
poradce	poradce	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Ideální	ideální	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
</s>
<s>
Základem	základ	k1gInSc7
státu	stát	k1gInSc2
jsou	být	k5eAaImIp3nP
podle	podle	k7c2
Machiavelliho	Machiavelli	k1gMnSc2
dobré	dobrý	k2eAgInPc1d1
zákony	zákon	k1gInPc1
a	a	k8xC
dobré	dobrý	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
(	(	kIx(
<g/>
přičemž	přičemž	k6eAd1
vojsko	vojsko	k1gNnSc1
zákony	zákon	k1gInPc7
svou	svůj	k3xOyFgFnSc7
důležitostí	důležitost	k1gFnSc7
převyšuje	převyšovat	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojska	vojsko	k1gNnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
základu	základ	k1gInSc6
dvojího	dvojí	k4xRgInSc2
typu	typ	k1gInSc2
<g/>
:	:	kIx,
námezdní	námezdní	k2eAgMnPc1d1
žoldnéři	žoldnér	k1gMnPc1
a	a	k8xC
branné	branný	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
z	z	k7c2
vlastních	vlastní	k2eAgFnPc2d1
poddaných	poddaná	k1gFnPc2
a	a	k8xC
vojsko	vojsko	k1gNnSc1
smíšené	smíšený	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Itálii	Itálie	k1gFnSc6
se	se	k3xPyFc4
téměř	téměř	k6eAd1
všeobecně	všeobecně	k6eAd1
používal	používat	k5eAaImAgInS
žoldnéřský	žoldnéřský	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
Machiavelli	Machiavelli	k1gMnSc1
kritizuje	kritizovat	k5eAaImIp3nS
<g/>
;	;	kIx,
takové	takový	k3xDgNnSc1
vojsko	vojsko	k1gNnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
něj	on	k3xPp3gNnSc2
nákladné	nákladný	k2eAgNnSc1d1
<g/>
,	,	kIx,
nespolehlivé	spolehlivý	k2eNgNnSc1d1
a	a	k8xC
podplatitelné	podplatitelný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použití	použití	k1gNnSc1
žoldnéřů	žoldnéř	k1gMnPc2
přímo	přímo	k6eAd1
nezavrhuje	zavrhovat	k5eNaImIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
v	v	k7c6
dané	daný	k2eAgFnSc6d1
chvíli	chvíle	k1gFnSc6
nutné	nutný	k2eAgNnSc1d1
jej	on	k3xPp3gMnSc4
použít	použít	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
z	z	k7c2
dlouhodobého	dlouhodobý	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
výhodnější	výhodný	k2eAgFnPc4d2
a	a	k8xC
rozumnější	rozumný	k2eAgNnSc1d2
budovat	budovat	k5eAaImF
vojsko	vojsko	k1gNnSc4
z	z	k7c2
právě	právě	k6eAd1
z	z	k7c2
vlastních	vlastní	k2eAgMnPc2d1
poddaných	poddaný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
vladaře	vladař	k1gMnSc2
</s>
<s>
Úspěšný	úspěšný	k2eAgMnSc1d1
vladař	vladař	k1gMnSc1
musí	muset	k5eAaImIp3nS
mít	mít	k5eAaImF
nejen	nejen	k6eAd1
spolehlivou	spolehlivý	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
pěstovat	pěstovat	k5eAaImF
správné	správný	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
vladaře	vladař	k1gMnSc2
a	a	k8xC
vůdce	vůdce	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sem	sem	k6eAd1
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
určitá	určitý	k2eAgFnSc1d1
míra	míra	k1gFnSc1
štěstí	štěstí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Panovníkova	panovníkův	k2eAgFnSc1d1
virtù	virtù	k?
pomáhá	pomáhat	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Štěstěna	Štěstěna	k1gFnSc1
stála	stát	k5eAaImAgFnS
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
tedy	tedy	k9
podmínky	podmínka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
vedou	vést	k5eAaImIp3nP
k	k	k7c3
vladařově	vladařův	k2eAgFnSc3d1
slávě	sláva	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Nutnosti	nutnost	k1gFnPc1
státu	stát	k1gInSc2
</s>
<s>
Svobodu	Svoboda	k1gMnSc4
považuje	považovat	k5eAaImIp3nS
Machiavelli	Machiavelli	k1gMnSc1
za	za	k7c4
základní	základní	k2eAgInSc4d1
předpoklad	předpoklad	k1gInSc4
velikosti	velikost	k1gFnSc2
státu	stát	k1gInSc2
(	(	kIx(
<g/>
tento	tento	k3xDgInSc1
názor	názor	k1gInSc1
vyslovuje	vyslovovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
Rozpravách	rozprava	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
se	se	k3xPyFc4
stát	stát	k1gInSc1
mohl	moct	k5eAaImAgInS
hospodářsky	hospodářsky	k6eAd1
rozvíjet	rozvíjet	k5eAaImF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
oproti	oproti	k7c3
tomu	ten	k3xDgNnSc3
nutné	nutný	k2eAgNnSc1d1
zajistit	zajistit	k5eAaPmF
právní	právní	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
–	–	k?
když	když	k8xS
si	se	k3xPyFc3
lidé	člověk	k1gMnPc1
budou	být	k5eAaImBp3nP
moc	moc	k6eAd1
být	být	k5eAaImF
svým	svůj	k3xOyFgInSc7
majetkem	majetek	k1gInSc7
jisti	jist	k2eAgMnPc1d1
<g/>
,	,	kIx,
rádi	rád	k2eAgMnPc1d1
ho	on	k3xPp3gMnSc4
budou	být	k5eAaImBp3nP
znásobovat	znásobovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Panovník	panovník	k1gMnSc1
musí	muset	k5eAaImIp3nS
zajistit	zajistit	k5eAaPmF
ochranu	ochrana	k1gFnSc4
majetku	majetek	k1gInSc2
a	a	k8xC
žen	žena	k1gFnPc2
poddaných	poddaný	k1gMnPc2
nejen	nejen	k6eAd1
před	před	k7c7
jinými	jiný	k2eAgMnPc7d1
poddanými	poddaný	k1gMnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k8xC
sám	sám	k3xTgMnSc1
si	se	k3xPyFc3
na	na	k7c4
ně	on	k3xPp3gMnPc4
nesmí	smět	k5eNaImIp3nS
činit	činit	k5eAaImF
nárok	nárok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Rozpravy	rozprava	k1gFnPc1
nad	nad	k7c7
prvními	první	k4xOgInPc7
deseti	deset	k4xCc7
knihami	kniha	k1gFnPc7
Tita	Titus	k1gMnSc2
Livia	Livius	k1gMnSc2
</s>
<s>
Rozpravy	rozprava	k1gFnPc1
jsou	být	k5eAaImIp3nP
oproti	oproti	k7c3
Vladaři	vladař	k1gMnSc3
obsáhlé	obsáhlý	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
o	o	k7c6
státech	stát	k1gInPc6
a	a	k8xC
vládách	vláda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevznikly	vzniknout	k5eNaPmAgFnP
jako	jako	k9
dílo	dílo	k1gNnSc4
určené	určený	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
jako	jako	k9
spis	spis	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
Machiavelli	Machiavelli	k1gMnSc1
sám	sám	k3xTgMnSc1
vyrovnává	vyrovnávat	k5eAaImIp3nS
s	s	k7c7
mnoha	mnoho	k4c7
historickopolitickými	historickopolitický	k2eAgInPc7d1
problémy	problém	k1gInPc7
<g/>
,	,	kIx,
tedy	tedy	k9
jako	jako	k9
dílo	dílo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
určeno	určit	k5eAaPmNgNnS
především	především	k9
pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
osobní	osobní	k2eAgFnSc4d1
potřebu	potřeba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vojenství	vojenství	k1gNnSc1
a	a	k8xC
politika	politika	k1gFnSc1
</s>
<s>
Machiavelli	Machiavelli	k1gMnSc1
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
vztah	vztah	k1gInSc4
vojenských	vojenský	k2eAgInPc2d1
řádů	řád	k1gInPc2
k	k	k7c3
politice	politika	k1gFnSc3
(	(	kIx(
<g/>
tj.	tj.	kA
závislost	závislost	k1gFnSc4
těchto	tento	k3xDgInPc2
řádů	řád	k1gInPc2
na	na	k7c6
politice	politika	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
si	se	k3xPyFc3
stát	stát	k1gInSc1
uchoval	uchovat	k5eAaPmAgInS
politickou	politický	k2eAgFnSc4d1
vůli	vůle	k1gFnSc4
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
v	v	k7c6
něm	on	k3xPp3gMnSc6
fungovat	fungovat	k5eAaImF
řádný	řádný	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jeho	jeho	k3xOp3gInSc6
ustavení	ustavení	k1gNnSc6
zase	zase	k9
slouží	sloužit	k5eAaImIp3nS
ctnost	ctnost	k1gFnSc1
(	(	kIx(
<g/>
virtù	virtù	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válečné	válečná	k1gFnSc2
umění	umění	k1gNnSc2
však	však	k9
nemá	mít	k5eNaImIp3nS
být	být	k5eAaImF
dědičné	dědičný	k2eAgInPc4d1
<g/>
;	;	kIx,
i	i	k8xC
řemeslníci	řemeslník	k1gMnPc1
by	by	kYmCp3nP
měli	mít	k5eAaImAgMnP
ve	v	k7c6
válce	válka	k1gFnSc6
bojovat	bojovat	k5eAaImF
<g/>
,	,	kIx,
po	po	k7c6
jejím	její	k3xOp3gNnSc6
skončení	skončení	k1gNnSc6
by	by	kYmCp3nP
se	se	k3xPyFc4
však	však	k9
všichni	všechen	k3xTgMnPc1
měli	mít	k5eAaImAgMnP
navrátit	navrátit	k5eAaPmF
ke	k	k7c3
svému	svůj	k3xOyFgNnSc3
původnímu	původní	k2eAgNnSc3d1
povolání	povolání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Ideál	ideál	k1gInSc1
vládní	vládní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
</s>
<s>
Machiavelli	Machiavelli	k1gMnSc1
odsuzuje	odsuzovat	k5eAaImIp3nS
diktaturu	diktatura	k1gFnSc4
a	a	k8xC
staví	stavit	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
se	se	k3xPyFc4
proti	proti	k7c3
neomezené	omezený	k2eNgFnSc3d1
královské	královský	k2eAgFnSc3d1
moci	moc	k1gFnSc3
(	(	kIx(
<g/>
ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
přípustná	přípustný	k2eAgFnSc1d1
jen	jen	k9
ve	v	k7c6
válečném	válečný	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
formu	forma	k1gFnSc4
považuje	považovat	k5eAaImIp3nS
republiku	republika	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
nikdo	nikdo	k3yNnSc1
nezíská	získat	k5eNaPmIp3nS
přílišnou	přílišný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgInPc1
„	„	k?
<g/>
čisté	čistá	k1gFnSc2
<g/>
“	“	k?
ústavní	ústavní	k2eAgFnSc2d1
formy	forma	k1gFnSc2
–	–	k?
monarchie	monarchie	k1gFnSc2
<g/>
,	,	kIx,
aristokracie	aristokracie	k1gFnPc1
<g/>
,	,	kIx,
demokracie	demokracie	k1gFnPc1
–	–	k?
jsou	být	k5eAaImIp3nP
podle	podle	k7c2
něj	on	k3xPp3gInSc2
bytostně	bytostně	k6eAd1
nestálé	stálý	k2eNgFnPc1d1
a	a	k8xC
cyklicky	cyklicky	k6eAd1
směřují	směřovat	k5eAaImIp3nP
ke	k	k7c3
zkáze	zkáza	k1gFnSc3
a	a	k8xC
rozkladu	rozklad	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nestálé	stálý	k2eNgInPc1d1
rysy	rys	k1gInPc1
čistých	čistý	k2eAgFnPc2d1
forem	forma	k1gFnPc2
se	se	k3xPyFc4
tedy	tedy	k9
musí	muset	k5eAaImIp3nS
korigovat	korigovat	k5eAaBmF
a	a	k8xC
silné	silný	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
spojit	spojit	k5eAaPmF
pomocí	pomocí	k7c2
smíšené	smíšený	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Typologie	typologie	k1gFnSc1
městských	městský	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
Machiavelli	Machiavelli	k1gMnSc1
rozeznává	rozeznávat	k5eAaImIp3nS
tři	tři	k4xCgInPc4
druhy	druh	k1gInPc4
států	stát	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
za	za	k7c4
nejlepší	dobrý	k2eAgNnSc4d3
považuje	považovat	k5eAaImIp3nS
onen	onen	k3xDgMnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
byla	být	k5eAaImAgFnS
zachována	zachován	k2eAgFnSc1d1
vnitřní	vnitřní	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
nedošlo	dojít	k5eNaPmAgNnS
k	k	k7c3
zásadnímu	zásadní	k2eAgInSc3d1
rozporu	rozpor	k1gInSc3
mezi	mezi	k7c7
bohatými	bohatý	k2eAgFnPc7d1
a	a	k8xC
chudými	chudý	k2eAgFnPc7d1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
hegemonii	hegemonie	k1gFnSc4
podržely	podržet	k5eAaPmAgFnP
střední	střední	k2eAgFnPc1d1
vrstvy	vrstva	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
především	především	k6eAd1
tohoto	tento	k3xDgNnSc2
kritéria	kritérion	k1gNnSc2
rozeznává	rozeznávat	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
a	a	k8xC
<g/>
)	)	kIx)
města	město	k1gNnSc2
německá	německý	k2eAgFnSc1d1
<g/>
,	,	kIx,
b	b	k?
<g/>
)	)	kIx)
města	město	k1gNnPc4
toskánská	toskánský	k2eAgNnPc4d1
<g/>
,	,	kIx,
c	c	k0
<g/>
)	)	kIx)
Benátky	Benátky	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
a	a	k8xC
<g/>
)	)	kIx)
Města	město	k1gNnPc1
německá	německý	k2eAgFnSc1d1
<g/>
:	:	kIx,
Machiavelli	Machiavelli	k1gMnSc1
je	on	k3xPp3gInPc4
hodnotí	hodnotit	k5eAaImIp3nS
velice	velice	k6eAd1
pozitivně	pozitivně	k6eAd1
<g/>
,	,	kIx,
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
prostotu	prostota	k1gFnSc4
a	a	k8xC
nezkaženost	nezkaženost	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
staví	stavit	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
do	do	k7c2
protikladu	protiklad	k1gInSc2
s	s	k7c7
rozmařilostí	rozmařilost	k1gFnSc7
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vidí	vidět	k5eAaImIp3nS
v	v	k7c6
nich	on	k3xPp3gFnPc6
ztělesněnu	ztělesněn	k2eAgFnSc4d1
především	především	k9
vládu	vláda	k1gFnSc4
středního	střední	k2eAgNnSc2d1
měšťanstva	měšťanstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
b	b	k?
<g/>
)	)	kIx)
Města	město	k1gNnSc2
toskánská	toskánský	k2eAgFnSc1d1
<g/>
:	:	kIx,
Spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
sem	sem	k6eAd1
i	i	k9
Florencie	Florencie	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
Machiavelli	Machiavelli	k1gMnSc1
hodnotí	hodnotit	k5eAaImIp3nS
různě	různě	k6eAd1
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
různých	různý	k2eAgNnPc6d1
historických	historický	k2eAgNnPc6d1
obdobích	období	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
považuje	považovat	k5eAaImIp3nS
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
ve	v	k7c6
Florencii	Florencie	k1gFnSc6
plně	plně	k6eAd1
dostala	dostat	k5eAaPmAgFnS
k	k	k7c3
moci	moc	k1gFnSc3
vláda	vláda	k1gFnSc1
popolo	popola	k1gFnSc5
(	(	kIx(
<g/>
řemeslnicko-obchodnická	řemeslnicko-obchodnický	k2eAgFnSc1d1
složka	složka	k1gFnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
ta	ten	k3xDgFnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
schopná	schopný	k2eAgFnSc1d1
zaručit	zaručit	k5eAaPmF
zákony	zákon	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
c	c	k0
<g/>
)	)	kIx)
Benátky	Benátky	k1gFnPc1
<g/>
:	:	kIx,
Machiavelli	Machiavelli	k1gMnSc1
kritizoval	kritizovat	k5eAaImAgMnS
její	její	k3xOp3gFnSc4
ústavu	ústava	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
spatřoval	spatřovat	k5eAaImAgInS
prostředek	prostředek	k1gInSc1
zaměřený	zaměřený	k2eAgInSc1d1
proti	proti	k7c3
široké	široký	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
lidu	lid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
vytýká	vytýkat	k5eAaImIp3nS
benátské	benátský	k2eAgFnSc3d1
ústavě	ústava	k1gFnSc3
též	též	k9
neschopnost	neschopnost	k1gFnSc4
plně	plně	k6eAd1
rozvinout	rozvinout	k5eAaPmF
vojenské	vojenský	k2eAgFnPc4d1
akce	akce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
benátské	benátský	k2eAgFnSc6d1
ústavě	ústava	k1gFnSc6
viděl	vidět	k5eAaImAgMnS
především	především	k9
typ	typ	k1gInSc4
ústavy	ústava	k1gFnSc2
městského	městský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
nikoli	nikoli	k9
tedy	tedy	k9
ústavu	ústava	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
přibližovala	přibližovat	k5eAaImAgFnS
feudálnímu	feudální	k2eAgInSc3d1
vzoru	vzor	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
LOVÍŠEK	LOVÍŠEK	kA
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Niccolo	Niccola	k1gFnSc5
Machiavelli	Machiavelli	k1gMnSc1
-	-	kIx~
Virtù	Virtù	k1gMnSc1
a	a	k8xC
politika	politika	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
-	-	kIx~
Fakulta	fakulta	k1gFnSc1
humanitních	humanitní	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
BPTX_	BPTX_	k1gFnSc1
<g/>
2011	#num#	k4
<g/>
_	_	kIx~
<g/>
2	#num#	k4
<g/>
__	__	k?
<g/>
0	#num#	k4
<g/>
_	_	kIx~
<g/>
264012	#num#	k4
<g/>
_	_	kIx~
<g/>
0	#num#	k4
<g/>
_	_	kIx~
<g/>
120993	#num#	k4
<g/>
.	.	kIx.
<g/>
pdf	pdf	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
CODR	CODR	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
;	;	kIx,
MARKOVIČOVÁ	MARKOVIČOVÁ	kA
<g/>
,	,	kIx,
Blanka	Blanka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přemožitelé	přemožitel	k1gMnPc1
času	čas	k1gInSc2
sv.	sv.	kA
7	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
novinářů	novinář	k1gMnPc2
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Niccolo	Niccola	k1gFnSc5
Machiavelli	Machiavelli	k1gMnSc5
<g/>
,	,	kIx,
s.	s.	k?
23	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
BOUDOVÁ	Boudová	k1gFnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Machiavell	Machiavella	k1gFnPc2
<g/>
,	,	kIx,
Niccolo	Niccola	k1gFnSc5
<g/>
.	.	kIx.
www.iliteratura.cz	www.iliteratura.cz	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2000-03-07	2000-03-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
„	„	k?
<g/>
Machiavelliho	Machiavelli	k1gMnSc2
dílo	dílo	k1gNnSc4
se	se	k3xPyFc4
roku	rok	k1gInSc2
1559	#num#	k4
ocitá	ocitat	k5eAaImIp3nS
na	na	k7c6
indexu	index	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
vydrželo	vydržet	k5eAaPmAgNnS
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
→	→	k?
ZNOJ	znoj	k1gInSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
,	,	kIx,
BÍBA	BÍBA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Machiavelli	Machiavelli	k1gMnSc1
mezi	mezi	k7c7
republikanismem	republikanismus	k1gInSc7
a	a	k8xC
demokracií	demokracie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Filosofia	Filosofia	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
453	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7007	#num#	k4
<g/>
-	-	kIx~
<g/>
360	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
str	str	kA
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Machiavelli	Machiavelli	k1gMnSc1
je	být	k5eAaImIp3nS
uveden	uvést	k5eAaPmNgMnS
např.	např.	kA
ještě	ještě	k9
v	v	k7c6
Indexu	index	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
1892	#num#	k4
→	→	k?
Index	index	k1gInSc1
librorum	librorum	k1gNnSc1
prohibitorum	prohibitorum	k1gInSc1
sanctissimi	sanctissi	k1gFnPc7
Domini	Domin	k1gMnPc1
Nostri	Nostr	k1gMnPc1
Leonis	Leonis	k1gFnSc2
XIII	XIII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pont	Pont	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Max	Max	k1gMnSc1
<g/>
.	.	kIx.
jussu	juss	k1gInSc2
editus	editus	k1gInSc1
<g/>
:	:	kIx,
Editio	Editio	k1gNnSc1
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taurnensis	Taurnensis	k1gFnSc1
cum	cum	k?
appendice	appendice	k1gFnSc1
usque	usquat	k5eAaPmIp3nS
ad	ad	k7c4
1892	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taurini	Taurin	k1gMnPc1
:	:	kIx,
Typ	typ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pontificia	Pontificia	k1gFnSc1
et	et	k?
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Eq	Eq	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petrus	Petrus	k1gInSc1
Marietti	Marietť	k1gFnSc2
<g/>
,	,	kIx,
1892	#num#	k4
<g/>
.	.	kIx.
444	#num#	k4
s.	s.	k?
[	[	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
str	str	kA
<g/>
.	.	kIx.
245	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Vydání	vydání	k1gNnSc1
Indexu	index	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
už	už	k6eAd1
jeho	jeho	k3xOp3gNnSc1
jméno	jméno	k1gNnSc1
nezmiňuje	zmiňovat	k5eNaImIp3nS
→	→	k?
Index	index	k1gInSc1
Librorum	Librorum	k1gInSc1
Prohibitorum	Prohibitorum	k1gInSc4
Ssmi	Ssm	k1gFnSc2
D.	D.	kA
<g/>
N.	N.	kA
Leonis	Leonis	k1gInSc4
XIII	XIII	kA
Jussu	Juss	k1gInSc2
et	et	k?
auctoritate	auctoritat	k1gInSc5
recognitus	recognitus	k1gMnSc1
et	et	k?
editus	editus	k1gMnSc1
<g/>
:	:	kIx,
praemittuntur	praemittuntura	k1gFnPc2
constitutiones	constitutiones	k1gInSc1
apostolicae	apostolica	k1gInSc2
de	de	k?
examine	examin	k1gInSc5
et	et	k?
prohibitione	prohibition	k1gInSc5
librorum	librorum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Romae	Romae	k1gNnSc1
<g/>
:	:	kIx,
Typis	Typis	k1gFnSc1
Vaticanis	Vaticanis	k1gFnSc1
<g/>
,	,	kIx,
1900	#num#	k4
<g/>
.	.	kIx.
xxiii	xxiie	k1gFnSc6
<g/>
,	,	kIx,
316	#num#	k4
s.	s.	k?
[	[	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
str	str	kA
<g/>
.	.	kIx.
198	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KUDRNA	Kudrna	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Machiavelli	Machiavelli	k1gMnSc1
a	a	k8xC
Guicciardini	Guicciardin	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
typologii	typologie	k1gFnSc3
historickopolitického	historickopolitický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
pozdní	pozdní	k2eAgFnSc2d1
italské	italský	k2eAgFnSc2d1
renesance	renesance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
:	:	kIx,
UJEP	UJEP	kA
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MACHIAVELLI	Machiavelli	k1gMnSc1
<g/>
,	,	kIx,
Niccolò	Niccolò	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vladař	vladař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MACHIAVELLI	Machiavelli	k1gMnSc1
<g/>
,	,	kIx,
Niccolò	Niccolò	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpravy	rozprava	k1gFnSc2
o	o	k7c6
prvních	první	k4xOgInPc6
deseti	deset	k4xCc6
knihách	kniha	k1gFnPc6
Tita	Titum	k1gNnSc2
Livia	Livium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MÉNISSIER	MÉNISSIER	kA
<g/>
,	,	kIx,
Thierry	Thierr	k1gInPc1
–	–	k?
ZARKA	ZARKA	kA
<g/>
,	,	kIx,
Yves	Yves	k1gInSc4
Charles	Charles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Machiavelliho	Machiavelli	k1gMnSc4
Vladař	vladař	k1gMnSc1
<g/>
:	:	kIx,
nové	nový	k2eAgNnSc1d1
umění	umění	k1gNnSc1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
OIKOYMENH	OIKOYMENH	kA
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PREZZOLINI	PREZZOLINI	kA
<g/>
,	,	kIx,
Giuseppe	Giusepp	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Nicola	Nicola	k1gFnSc1
Machiavelliho	Machiavelli	k1gMnSc2
<g/>
,	,	kIx,
učitele	učitel	k1gMnSc2
vladařů	vladař	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SKINNER	SKINNER	kA
<g/>
,	,	kIx,
Quentin	Quentin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Machiavelli	Machiavelli	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SYLLABA	SYLLABA	kA
<g/>
,	,	kIx,
Theodor	Theodor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
politických	politický	k2eAgFnPc2d1
filozofií	filozofie	k1gFnPc2
(	(	kIx(
<g/>
Politické	politický	k2eAgFnPc1d1
filozofie	filozofie	k1gFnPc1
od	od	k7c2
antiky	antika	k1gFnSc2
do	do	k7c2
konce	konec	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
:	:	kIx,
Gaudeamus	Gaudeamus	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WHITE	WHITE	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Machiavelli	Machiavelli	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepochopený	pochopený	k2eNgMnSc1d1
muž	muž	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
BB	BB	kA
<g/>
/	/	kIx~
<g/>
art	art	k?
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ZNOJ	znoj	k1gFnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
,	,	kIx,
BÍBA	BÍBA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Machiavelli	Machiavelli	k1gMnSc1
mezi	mezi	k7c7
republikanismem	republikanismus	k1gInSc7
a	a	k8xC
demokracií	demokracie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Filosofia	Filosofia	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
453	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7007	#num#	k4
<g/>
-	-	kIx~
<g/>
360	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Machiavellismus	Machiavellismus	k1gInSc1
</s>
<s>
Seznam	seznam	k1gInSc1
italských	italský	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Niccolò	Niccolò	k1gFnSc2
Machiavelli	Machiavelli	k1gMnPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Niccolò	Niccolò	k1gMnSc1
Machiavelli	Machiavelli	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
„	„	k?
<g/>
Old	Olda	k1gFnPc2
Nick	Nick	k1gMnSc1
<g/>
“	“	k?
<g/>
:	:	kIx,
Ďábelský	ďábelský	k2eAgMnSc1d1
kazatel	kazatel	k1gMnSc1
nebo	nebo	k8xC
první	první	k4xOgMnSc1
hodnotově	hodnotově	k6eAd1
neutrální	neutrální	k2eAgMnSc1d1
politický	politický	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
na	na	k7c6
Mises	Misesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Il	Il	k?
Principe	princip	k1gInSc5
na	na	k7c4
MetaLibri	MetaLibr	k1gFnPc4
Digital	Digital	kA
Library	Librara	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990005296	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118575775	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2144	#num#	k4
1233	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
78096105	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500208005	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
95151646	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
78096105	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Filosofie	filosofie	k1gFnSc1
|	|	kIx~
Historie	historie	k1gFnSc1
|	|	kIx~
Itálie	Itálie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
</s>
<s>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
</s>
