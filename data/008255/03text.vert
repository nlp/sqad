<p>
<s>
Okres	okres	k1gInSc1	okres
Stará	starý	k2eAgFnSc1d1	stará
Ľubovňa	Ľubovňa	k1gFnSc1	Ľubovňa
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
okresů	okres	k1gInPc2	okres
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Prešovském	prešovský	k2eAgInSc6d1	prešovský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
okresy	okres	k1gInPc7	okres
Kežmarok	Kežmarok	k1gInSc1	Kežmarok
<g/>
,	,	kIx,	,
Sabinov	Sabinov	k1gInSc1	Sabinov
a	a	k8xC	a
Bardejov	Bardejov	k1gInSc1	Bardejov
<g/>
.	.	kIx.	.
</s>
</p>
