<p>
<s>
Trojka	trojka	k1gFnSc1	trojka
byla	být	k5eAaImAgFnS	být
třetí	třetí	k4xOgInSc4	třetí
slovenský	slovenský	k2eAgInSc4d1	slovenský
kanál	kanál	k1gInSc4	kanál
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
začal	začít	k5eAaPmAgInS	začít
vysílat	vysílat	k5eAaImF	vysílat
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
provoz	provoz	k1gInSc4	provoz
ukončil	ukončit	k5eAaPmAgMnS	ukončit
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Televizní	televizní	k2eAgInSc1d1	televizní
kanál	kanál	k1gInSc1	kanál
oficiálně	oficiálně	k6eAd1	oficiálně
zahájil	zahájit	k5eAaPmAgInS	zahájit
vysílání	vysílání	k1gNnSc4	vysílání
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
v	v	k7c4	v
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
začátkem	začátkem	k7c2	začátkem
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
zahájení	zahájení	k1gNnSc2	zahájení
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
z	z	k7c2	z
Pekingu	Peking	k1gInSc2	Peking
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgInSc1d1	televizní
kanál	kanál	k1gInSc1	kanál
vysílá	vysílat	k5eAaImIp3nS	vysílat
technologií	technologie	k1gFnSc7	technologie
DVB-T	DVB-T	k1gFnSc2	DVB-T
a	a	k8xC	a
také	také	k9	také
byl	být	k5eAaImAgInS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
přes	přes	k7c4	přes
kabelovou	kabelový	k2eAgFnSc4d1	kabelová
televizi	televize	k1gFnSc4	televize
a	a	k8xC	a
satelit	satelit	k1gInSc4	satelit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Rozhlasu	rozhlas	k1gInSc2	rozhlas
a	a	k8xC	a
televize	televize	k1gFnSc2	televize
Slovenska	Slovensko	k1gNnSc2	Slovensko
ukončeno	ukončen	k2eAgNnSc4d1	ukončeno
vysílání	vysílání	k1gNnSc4	vysílání
na	na	k7c6	na
kanále	kanál	k1gInSc6	kanál
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatku	nedostatek	k1gInSc2	nedostatek
financí	finance	k1gFnPc2	finance
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
<g/>
Aktuálně	aktuálně	k6eAd1	aktuálně
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
třetí	třetí	k4xOgInSc1	třetí
kanál	kanál	k1gInSc1	kanál
mohl	moct	k5eAaImAgInS	moct
spustit	spustit	k5eAaPmF	spustit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
znamenalo	znamenat	k5eAaImAgNnS	znamenat
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
zvýšení	zvýšení	k1gNnSc1	zvýšení
koncesionářských	koncesionářský	k2eAgInPc2d1	koncesionářský
poplatků	poplatek	k1gInPc2	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Trojka	trojka	k1gFnSc1	trojka
ale	ale	k8xC	ale
už	už	k6eAd1	už
nebude	být	k5eNaImBp3nS	být
jen	jen	k6eAd1	jen
sportovní	sportovní	k2eAgFnSc1d1	sportovní
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sportovně-zpravodajská	sportovněpravodajský	k2eAgFnSc1d1	sportovně-zpravodajský
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Vysílané	vysílaný	k2eAgInPc1d1	vysílaný
pořady	pořad	k1gInPc1	pořad
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Slovenské	slovenský	k2eAgInPc1d1	slovenský
pořady	pořad	k1gInPc1	pořad
===	===	k?	===
</s>
</p>
<p>
<s>
Aréna	aréna	k1gFnSc1	aréna
</s>
</p>
<p>
<s>
Góly	gól	k1gInPc1	gól
body	bod	k1gInPc4	bod
sekundy	sekunda	k1gFnPc4	sekunda
</s>
</p>
<p>
<s>
Góóól	Góóól	k1gMnSc1	Góóól
</s>
</p>
<p>
<s>
Odpískané	odpískaný	k2eAgNnSc1d1	odpískané
</s>
</p>
<p>
<s>
Šport	Šport	k1gInSc1	Šport
vo	vo	k?	vo
svete	svet	k1gMnSc5	svet
</s>
</p>
<p>
<s>
Športové	Športový	k2eAgFnPc1d1	Športový
ozveny	ozvena	k1gFnPc1	ozvena
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
pořady	pořad	k1gInPc1	pořad
===	===	k?	===
</s>
</p>
<p>
<s>
Magazín	magazín	k1gInSc1	magazín
NHL	NHL	kA	NHL
</s>
</p>
<p>
<s>
Magazín	magazín	k1gInSc1	magazín
Ligy	liga	k1gFnSc2	liga
majstrov	majstrovo	k1gNnPc2	majstrovo
</s>
</p>
<p>
<s>
Týždeň	Týždeň	k1gFnSc1	Týždeň
v	v	k7c6	v
KHL	KHL	kA	KHL
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
