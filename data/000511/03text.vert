<s>
Bohunice	Bohunice	k1gFnPc1	Bohunice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Bohonitz	Bohonitz	k1gInSc1	Bohonitz
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
historická	historický	k2eAgFnSc1d1	historická
obec	obec	k1gFnSc1	obec
<g/>
,	,	kIx,	,
městská	městský	k2eAgFnSc1d1	městská
čtvrť	čtvrť	k1gFnSc1	čtvrť
a	a	k8xC	a
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Brno-Bohunice	Brno-Bohunice	k1gFnSc2	Brno-Bohunice
také	také	k9	také
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
okraji	okraj	k1gInSc6	okraj
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
301,71	[number]	k4	301,71
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
evidováno	evidovat	k5eAaImNgNnS	evidovat
47	[number]	k4	47
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
1	[number]	k4	1
132	[number]	k4	132
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
senátních	senátní	k2eAgFnPc2d1	senátní
voleb	volba	k1gFnPc2	volba
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc1	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Bohunice	Brno-Bohunice	k1gFnSc2	Brno-Bohunice
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
volebního	volební	k2eAgInSc2d1	volební
obvodu	obvod	k1gInSc2	obvod
číslo	číslo	k1gNnSc1	číslo
58	[number]	k4	58
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Bohunice	Brno-Bohunice	k1gFnSc2	Brno-Bohunice
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Brno-Starý	Brno-Starý	k2eAgInSc1d1	Brno-Starý
Lískovec	Lískovec	k1gInSc1	Lískovec
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
s	s	k7c7	s
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Brno-Nový	Brno-Nový	k2eAgInSc1d1	Brno-Nový
Lískovec	Lískovec	k1gInSc1	Lískovec
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
východě	východ	k1gInSc6	východ
s	s	k7c7	s
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Brno-jih	Brnoiha	k1gFnPc2	Brno-jiha
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
obcí	obec	k1gFnPc2	obec
Moravany	Moravan	k1gMnPc4	Moravan
<g/>
.	.	kIx.	.
</s>
<s>
Bohunice	Bohunice	k1gFnPc1	Bohunice
mají	mít	k5eAaImIp3nP	mít
spíše	spíše	k9	spíše
městský	městský	k2eAgInSc4d1	městský
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
zvlněnou	zvlněný	k2eAgFnSc4d1	zvlněná
krajinu	krajina	k1gFnSc4	krajina
se	s	k7c7	s
znatelnými	znatelný	k2eAgInPc7d1	znatelný
výškovými	výškový	k2eAgInPc7d1	výškový
rozdíly	rozdíl	k1gInPc7	rozdíl
v	v	k7c6	v
zástavbě	zástavba	k1gFnSc6	zástavba
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
je	on	k3xPp3gNnSc4	on
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
panelové	panelový	k2eAgNnSc4d1	panelové
sídliště	sídliště	k1gNnSc4	sídliště
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
středu	střed	k1gInSc6	střed
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zástavba	zástavba	k1gFnSc1	zástavba
zbytku	zbytek	k1gInSc2	zbytek
původní	původní	k2eAgFnSc2d1	původní
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
původní	původní	k2eAgFnSc2d1	původní
zástavby	zástavba	k1gFnSc2	zástavba
protéká	protékat	k5eAaImIp3nS	protékat
říčka	říčka	k1gFnSc1	říčka
Leskava	Leskava	k1gFnSc1	Leskava
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
sídliště	sídliště	k1gNnSc2	sídliště
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
zdejší	zdejší	k2eAgFnPc4d1	zdejší
ulice	ulice	k1gFnPc4	ulice
pojmenovány	pojmenován	k2eAgFnPc4d1	pojmenována
podle	podle	k7c2	podle
svazových	svazový	k2eAgFnPc2d1	svazová
republik	republika	k1gFnPc2	republika
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
sídliště	sídliště	k1gNnSc2	sídliště
se	se	k3xPyFc4	se
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
Jihlavské	jihlavský	k2eAgFnSc2d1	Jihlavská
ulice	ulice	k1gFnSc2	ulice
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
přilehlý	přilehlý	k2eAgInSc1d1	přilehlý
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
areál	areál	k1gInSc1	areál
zdejší	zdejší	k2eAgFnSc2d1	zdejší
fakultní	fakultní	k2eAgFnSc2d1	fakultní
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
sousední	sousední	k2eAgFnSc2d1	sousední
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Starý	Brno-Starý	k2eAgInSc1d1	Brno-Starý
Lískovec	Lískovec	k1gInSc1	Lískovec
<g/>
,	,	kIx,	,
a	a	k8xC	a
sousední	sousední	k2eAgFnSc7d1	sousední
vysokou	vysoký	k2eAgFnSc7d1	vysoká
zdí	zeď	k1gFnSc7	zeď
obehnaný	obehnaný	k2eAgInSc1d1	obehnaný
areál	areál	k1gInSc1	areál
zdejší	zdejší	k2eAgFnSc2d1	zdejší
vazební	vazební	k2eAgFnSc2d1	vazební
věznice	věznice	k1gFnSc2	věznice
<g/>
,	,	kIx,	,
od	od	k7c2	od
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
nachází	nacházet	k5eAaImIp3nS	nacházet
budova	budova	k1gFnSc1	budova
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
nákupního	nákupní	k2eAgNnSc2d1	nákupní
centra	centrum	k1gNnSc2	centrum
Kaufland	Kauflanda	k1gFnPc2	Kauflanda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejzápadnější	západní	k2eAgFnSc6d3	nejzápadnější
části	část	k1gFnSc6	část
katastru	katastr	k1gInSc2	katastr
Bohunic	Bohunice	k1gFnPc2	Bohunice
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
severní	severní	k2eAgFnSc1d1	severní
polovina	polovina	k1gFnSc1	polovina
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
Campus	Campus	k1gMnSc1	Campus
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
jižní	jižní	k2eAgFnSc1d1	jižní
polovina	polovina	k1gFnSc1	polovina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
již	již	k6eAd1	již
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
sousední	sousední	k2eAgFnSc2d1	sousední
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Starý	Brno-Starý	k2eAgInSc1d1	Brno-Starý
Lískovec	Lískovec	k1gInSc1	Lískovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bohunické	bohunický	k2eAgFnSc6d1	Bohunická
části	část	k1gFnSc6	část
tohoto	tento	k3xDgNnSc2	tento
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
například	například	k6eAd1	například
hypermarket	hypermarket	k1gInSc4	hypermarket
společnosti	společnost	k1gFnSc2	společnost
Tesco	Tesco	k1gNnSc4	Tesco
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jižním	jižní	k2eAgInSc7d1	jižní
okrajem	okraj	k1gInSc7	okraj
katastru	katastr	k1gInSc2	katastr
Bohunic	Bohunice	k1gFnPc2	Bohunice
prochází	procházet	k5eAaImIp3nS	procházet
dálnice	dálnice	k1gFnSc1	dálnice
D1	D1	k1gFnSc1	D1
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
žádné	žádný	k3yNgNnSc4	žádný
přímé	přímý	k2eAgNnSc4d1	přímé
napojení	napojení	k1gNnSc4	napojení
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
a	a	k8xC	a
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Leskavy	Leskavy	k?	Leskavy
zde	zde	k6eAd1	zde
prochází	procházet	k5eAaImIp3nS	procházet
také	také	k9	také
železniční	železniční	k2eAgFnSc1d1	železniční
trasa	trasa	k1gFnSc1	trasa
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
do	do	k7c2	do
Střelic	Střelice	k1gFnPc2	Střelice
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
se	s	k7c7	s
středem	střed	k1gInSc7	střed
města	město	k1gNnSc2	město
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
linek	linka	k1gFnPc2	linka
číslo	číslo	k1gNnSc1	číslo
6	[number]	k4	6
a	a	k8xC	a
8	[number]	k4	8
(	(	kIx(	(
<g/>
jedou	jet	k5eAaImIp3nP	jet
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
do	do	k7c2	do
Králova	Králův	k2eAgNnSc2d1	Královo
Pole	pole	k1gNnSc2	pole
/	/	kIx~	/
Líšně	Líšeň	k1gFnSc2	Líšeň
<g/>
,	,	kIx,	,
či	či	k8xC	či
naopak	naopak	k6eAd1	naopak
<g/>
)	)	kIx)	)
a	a	k8xC	a
několika	několik	k4yIc2	několik
autobusových	autobusový	k2eAgFnPc2d1	autobusová
a	a	k8xC	a
trolejbusových	trolejbusový	k2eAgFnPc2d1	trolejbusová
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
se	s	k7c7	s
St.	st.	kA	st.
Lískovcem	Lískovec	k1gInSc7	Lískovec
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
tramvajové	tramvajový	k2eAgFnPc4d1	tramvajová
linky	linka	k1gFnPc4	linka
č.	č.	k?	č.
6	[number]	k4	6
<g/>
,	,	kIx,	,
8	[number]	k4	8
a	a	k8xC	a
10	[number]	k4	10
<g/>
,	,	kIx,	,
trolejbusové	trolejbusový	k2eAgFnSc2d1	trolejbusová
linky	linka	k1gFnSc2	linka
č.	č.	k?	č.
25	[number]	k4	25
<g/>
,	,	kIx,	,
37	[number]	k4	37
a	a	k8xC	a
autobusové	autobusový	k2eAgFnSc2d1	autobusová
linky	linka	k1gFnSc2	linka
č.	č.	k?	č.
50	[number]	k4	50
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
56	[number]	k4	56
<g/>
,	,	kIx,	,
69	[number]	k4	69
a	a	k8xC	a
82	[number]	k4	82
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
dopravu	doprava	k1gFnSc4	doprava
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
noční	noční	k2eAgFnPc1d1	noční
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
N	N	kA	N
<g/>
90	[number]	k4	90
<g/>
,	,	kIx,	,
N91	N91	k1gFnSc1	N91
a	a	k8xC	a
N	N	kA	N
<g/>
96	[number]	k4	96
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Bohunicích	Bohunice	k1gFnPc6	Bohunice
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
září	září	k1gNnSc2	září
1237	[number]	k4	1237
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
okrajová	okrajový	k2eAgFnSc1d1	okrajová
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
pozemky	pozemek	k1gInPc1	pozemek
patřící	patřící	k2eAgFnSc2d1	patřící
původně	původně	k6eAd1	původně
ke	k	k7c3	k
Starému	starý	k2eAgNnSc3d1	staré
Brnu	Brno	k1gNnSc3	Brno
<g/>
)	)	kIx)	)
moderního	moderní	k2eAgInSc2d1	moderní
katastru	katastr	k1gInSc2	katastr
Bohunic	Bohunice	k1gFnPc2	Bohunice
byla	být	k5eAaImAgFnS	být
připojena	připojit	k5eAaPmNgFnS	připojit
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
už	už	k9	už
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
katastr	katastr	k1gInSc1	katastr
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
obce	obec	k1gFnSc2	obec
Bohunic	Bohunice	k1gFnPc2	Bohunice
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
katastrální	katastrální	k2eAgFnSc7d1	katastrální
reformou	reforma	k1gFnSc7	reforma
Brna	Brno	k1gNnSc2	Brno
ze	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
ke	k	k7c3	k
znatelným	znatelný	k2eAgFnPc3d1	znatelná
úpravám	úprava	k1gFnPc3	úprava
katastrálních	katastrální	k2eAgFnPc2d1	katastrální
hranic	hranice	k1gFnPc2	hranice
Bohunic	Bohunice	k1gFnPc2	Bohunice
<g/>
:	:	kIx,	:
celá	celý	k2eAgFnSc1d1	celá
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
katastru	katastr	k1gInSc2	katastr
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
částí	část	k1gFnSc7	část
areálu	areál	k1gInSc2	areál
Kohnovy	Kohnův	k2eAgFnSc2d1	Kohnova
cihelny	cihelna	k1gFnSc2	cihelna
a	a	k8xC	a
pozemků	pozemek	k1gInPc2	pozemek
kolem	kolem	k7c2	kolem
Brněnského	brněnský	k2eAgNnSc2d1	brněnské
krematoria	krematorium	k1gNnSc2	krematorium
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
o	o	k7c4	o
svoji	svůj	k3xOyFgFnSc4	svůj
část	část	k1gFnSc4	část
Nového	Nového	k2eAgInSc2d1	Nového
Lískovce	Lískovec	k1gInSc2	Lískovec
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
zalesněné	zalesněný	k2eAgNnSc4d1	zalesněné
území	území	k1gNnSc4	území
východně	východně	k6eAd1	východně
od	od	k7c2	od
něho	on	k3xPp3gNnSc2	on
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
získaly	získat	k5eAaPmAgFnP	získat
část	část	k1gFnSc4	část
katastru	katastr	k1gInSc2	katastr
Horních	horní	k2eAgFnPc2d1	horní
Heršpic	Heršpice	k1gFnPc2	Heršpice
a	a	k8xC	a
od	od	k7c2	od
Lískovce	Lískovec	k1gInSc2	Lískovec
zástavbu	zástavba	k1gFnSc4	zástavba
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
Humenné	Humenné	k1gNnSc1	Humenné
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
vybudovat	vybudovat	k5eAaPmF	vybudovat
v	v	k7c6	v
nevyužitých	využitý	k2eNgFnPc6d1	nevyužitá
částech	část	k1gFnPc6	část
Bohunic	Bohunice	k1gFnPc2	Bohunice
a	a	k8xC	a
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
sídliště	sídliště	k1gNnSc2	sídliště
pro	pro	k7c4	pro
30	[number]	k4	30
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
výstavba	výstavba	k1gFnSc1	výstavba
začala	začít	k5eAaPmAgFnS	začít
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
na	na	k7c6	na
východě	východ	k1gInSc6	východ
bohunického	bohunický	k2eAgInSc2d1	bohunický
katastru	katastr	k1gInSc2	katastr
mezi	mezi	k7c7	mezi
areálem	areál	k1gInSc7	areál
ústředního	ústřední	k2eAgInSc2d1	ústřední
hřbitova	hřbitov	k1gInSc2	hřbitov
a	a	k8xC	a
zástavbou	zástavba	k1gFnSc7	zástavba
původní	původní	k2eAgFnSc2d1	původní
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
byly	být	k5eAaImAgFnP	být
nakonec	nakonec	k6eAd1	nakonec
zbořeny	zbořen	k2eAgFnPc1d1	zbořena
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
výstavba	výstavba	k1gFnSc1	výstavba
sídliště	sídliště	k1gNnSc2	sídliště
na	na	k7c6	na
katastrech	katastr	k1gInPc6	katastr
Bohunic	Bohunice	k1gFnPc2	Bohunice
a	a	k8xC	a
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
byla	být	k5eAaImAgFnS	být
též	též	k9	též
důvodem	důvod	k1gInSc7	důvod
návrhu	návrh	k1gInSc2	návrh
na	na	k7c4	na
výraznou	výrazný	k2eAgFnSc4d1	výrazná
změnu	změna	k1gFnSc4	změna
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
katastry	katastr	k1gInPc7	katastr
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
projednána	projednat	k5eAaPmNgFnS	projednat
s	s	k7c7	s
odborem	odbor	k1gInSc7	odbor
Vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
věcí	věc	k1gFnPc2	věc
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nová	nový	k2eAgFnSc1d1	nová
hranice	hranice	k1gFnSc1	hranice
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc3	duben
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Bohunicím	Bohunice	k1gFnPc3	Bohunice
ze	z	k7c2	z
Starého	staré	k1gNnSc2	staré
Lískovce	Lískovec	k1gInSc2	Lískovec
přešlo	přejít	k5eAaPmAgNnS	přejít
necelých	celý	k2eNgInPc2d1	necelý
14	[number]	k4	14
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
většinu	většina	k1gFnSc4	většina
nové	nový	k2eAgFnSc2d1	nová
hranice	hranice	k1gFnSc2	hranice
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
ulice	ulice	k1gFnSc1	ulice
Osová	osový	k2eAgFnSc1d1	Osová
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
východní	východní	k2eAgInSc4d1	východní
okraj	okraj	k1gInSc4	okraj
silnice	silnice	k1gFnSc2	silnice
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
Bohunic	Bohunice	k1gFnPc2	Bohunice
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
z	z	k7c2	z
279,6	[number]	k4	279,6
hektarů	hektar	k1gInPc2	hektar
na	na	k7c4	na
293,4	[number]	k4	293,4
hektarů	hektar	k1gInPc2	hektar
a	a	k8xC	a
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
katastry	katastr	k1gInPc7	katastr
se	se	k3xPyFc4	se
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
z	z	k7c2	z
1108	[number]	k4	1108
metrů	metr	k1gInPc2	metr
na	na	k7c4	na
1326	[number]	k4	1326
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
17	[number]	k4	17
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc3	únor
2011	[number]	k4	2011
pak	pak	k9	pak
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
další	další	k2eAgFnSc1d1	další
změna	změna	k1gFnSc1	změna
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
Starým	starý	k2eAgInSc7d1	starý
Lískovcem	Lískovec	k1gInSc7	Lískovec
a	a	k8xC	a
Bohunicemi	Bohunice	k1gFnPc7	Bohunice
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
ulic	ulice	k1gFnPc2	ulice
Osová	osový	k2eAgFnSc1d1	Osová
a	a	k8xC	a
Jihlavská	jihlavský	k2eAgFnSc1d1	Jihlavská
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
přešlo	přejít	k5eAaPmAgNnS	přejít
k	k	k7c3	k
Bohunicím	Bohunice	k1gFnPc3	Bohunice
4221	[number]	k4	4221
m2	m2	k4	m2
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
z	z	k7c2	z
Bohunic	Bohunice	k1gFnPc2	Bohunice
ke	k	k7c3	k
Starému	starý	k2eAgInSc3d1	starý
Lískovci	Lískovec	k1gInSc3	Lískovec
jen	jen	k9	jen
3609	[number]	k4	3609
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
12	[number]	k4	12
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
nabyla	nabýt	k5eAaPmAgFnS	nabýt
účinnosti	účinnost	k1gFnPc4	účinnost
menší	malý	k2eAgFnSc1d2	menší
změna	změna	k1gFnSc1	změna
hranice	hranice	k1gFnSc2	hranice
Bohunic	Bohunice	k1gFnPc2	Bohunice
a	a	k8xC	a
Pisárek	Pisárka	k1gFnPc2	Pisárka
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zároveň	zároveň	k6eAd1	zároveň
i	i	k8xC	i
změna	změna	k1gFnSc1	změna
hranice	hranice	k1gFnSc2	hranice
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
Brno-Bohunice	Brno-Bohunice	k1gFnSc2	Brno-Bohunice
a	a	k8xC	a
Brno-střed	Brnotřed	k1gInSc4	Brno-střed
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Vinohrady	Vinohrady	k1gInPc4	Vinohrady
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
Bohunic	Bohunice	k1gFnPc2	Bohunice
přešly	přejít	k5eAaPmAgFnP	přejít
k	k	k7c3	k
Pisárkám	Pisárka	k1gFnPc3	Pisárka
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
k	k	k7c3	k
městské	městský	k2eAgFnSc3d1	městská
části	část	k1gFnSc3	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
<g/>
)	)	kIx)	)
parcely	parcela	k1gFnPc1	parcela
s	s	k7c7	s
novými	nový	k2eAgNnPc7d1	nové
čísly	číslo	k1gNnPc7	číslo
2334	[number]	k4	2334
<g/>
,	,	kIx,	,
2335	[number]	k4	2335
<g/>
,	,	kIx,	,
2336	[number]	k4	2336
a	a	k8xC	a
2337	[number]	k4	2337
<g/>
,	,	kIx,	,
mající	mající	k2eAgInSc4d1	mající
dohromady	dohromady	k6eAd1	dohromady
rozlohu	rozloha	k1gFnSc4	rozloha
42	[number]	k4	42
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tyt	k2eAgNnSc1d1	tyto
k	k	k7c3	k
Pisárkám	Pisárka	k1gFnPc3	Pisárka
nově	nově	k6eAd1	nově
připojené	připojený	k2eAgInPc1d1	připojený
pozemky	pozemek	k1gInPc1	pozemek
náležely	náležet	k5eAaImAgInP	náležet
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ke	k	k7c3	k
k.	k.	k?	k.
ú.	ú.	k?	ú.
Staré	Staré	k2eAgNnSc1d1	Staré
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Vídeňka	Vídeňka	k1gFnSc1	Vídeňka
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1946	[number]	k4	1946
-	-	kIx~	-
území	území	k1gNnSc6	území
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
pozemky	pozemek	k1gInPc4	pozemek
tehdy	tehdy	k6eAd1	tehdy
patřící	patřící	k2eAgInPc4d1	patřící
do	do	k7c2	do
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g />
.	.	kIx.	.
</s>
<s>
Bohunice	Bohunice	k1gFnPc1	Bohunice
<g/>
,	,	kIx,	,
spadaly	spadat	k5eAaImAgInP	spadat
pod	pod	k7c4	pod
brněnský	brněnský	k2eAgInSc4d1	brněnský
MNV	MNV	kA	MNV
Bohunice	Bohunice	k1gFnPc1	Bohunice
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
lískovecká	lískovecký	k2eAgFnSc1d1	lískovecký
část	část	k1gFnSc1	část
moderního	moderní	k2eAgInSc2d1	moderní
katastru	katastr	k1gInSc2	katastr
Bohunic	Bohunice	k1gFnPc2	Bohunice
spadala	spadat	k5eAaPmAgFnS	spadat
pod	pod	k7c4	pod
MNV	MNV	kA	MNV
Starý	starý	k2eAgInSc4d1	starý
Lískovec	Lískovec	k1gInSc4	Lískovec
<g/>
,	,	kIx,	,
okrajové	okrajový	k2eAgFnPc4d1	okrajová
části	část	k1gFnPc4	část
moderního	moderní	k2eAgInSc2d1	moderní
katastru	katastr	k1gInSc2	katastr
Bohunic	Bohunice	k1gFnPc2	Bohunice
pak	pak	k6eAd1	pak
spadaly	spadat	k5eAaPmAgInP	spadat
pod	pod	k7c4	pod
MNV	MNV	kA	MNV
Staré	Staré	k2eAgNnSc1d1	Staré
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
MNV	MNV	kA	MNV
Horní	horní	k2eAgFnPc4d1	horní
Heršpice	Heršpice	k1gFnPc4	Heršpice
1947	[number]	k4	1947
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
-	-	kIx~	-
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Bohunice	Brno-Bohunice	k1gFnSc2	Brno-Bohunice
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
městského	městský	k2eAgInSc2d1	městský
obvodu	obvod	k1gInSc2	obvod
Brno	Brno	k1gNnSc4	Brno
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
hornoheršpické	hornoheršpický	k2eAgInPc1d1	hornoheršpický
pozemky	pozemek	k1gInPc1	pozemek
byly	být	k5eAaImAgInP	být
součástí	součást	k1gFnSc7	součást
městského	městský	k2eAgInSc2d1	městský
obvodu	obvod	k1gInSc2	obvod
Brno	Brno	k1gNnSc4	Brno
VIII	VIII	kA	VIII
<g/>
..	..	k?	..
1949	[number]	k4	1949
<g/>
-	-	kIx~	-
<g/>
1954	[number]	k4	1954
-	-	kIx~	-
správní	správní	k2eAgFnSc2d1	správní
hranice	hranice	k1gFnSc2	hranice
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
území	území	k1gNnSc2	území
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Bohunice	Brno-Bohunice	k1gFnSc2	Brno-Bohunice
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
nezměnily	změnit	k5eNaPmAgFnP	změnit
<g/>
,	,	kIx,	,
obvody	obvod	k1gInPc1	obvod
byly	být	k5eAaImAgInP	být
pouze	pouze	k6eAd1	pouze
přečíslovány	přečíslován	k2eAgInPc1d1	přečíslován
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
Bohunic	Bohunice	k1gFnPc2	Bohunice
nyní	nyní	k6eAd1	nyní
spadala	spadat	k5eAaImAgNnP	spadat
do	do	k7c2	do
městského	městský	k2eAgInSc2d1	městský
obvodu	obvod	k1gInSc2	obvod
Brno	Brno	k1gNnSc4	Brno
V	V	kA	V
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
byl	být	k5eAaImAgInS	být
dále	daleko	k6eAd2	daleko
například	například	k6eAd1	například
Starý	starý	k2eAgInSc1d1	starý
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
Lískovec	Lískovec	k1gInSc1	Lískovec
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
hornoheršpické	hornoheršpický	k2eAgInPc4d1	hornoheršpický
pozemky	pozemek	k1gInPc4	pozemek
do	do	k7c2	do
městského	městský	k2eAgInSc2d1	městský
obvodu	obvod	k1gInSc2	obvod
Brno	Brno	k1gNnSc4	Brno
XI	XI	kA	XI
<g/>
.	.	kIx.	.
1954	[number]	k4	1954
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
-	-	kIx~	-
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
zůstávaly	zůstávat	k5eAaImAgFnP	zůstávat
správní	správní	k2eAgFnPc1d1	správní
hranice	hranice	k1gFnPc1	hranice
na	na	k7c6	na
území	území	k1gNnSc6	území
Bohunic	Bohunice	k1gFnPc2	Bohunice
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
správnímu	správní	k2eAgNnSc3d1	správní
rozdělení	rozdělení	k1gNnSc3	rozdělení
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
katastru	katastr	k1gInSc2	katastr
bohunic	bohunice	k1gFnPc2	bohunice
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
hranici	hranice	k1gFnSc4	hranice
tvořila	tvořit	k5eAaImAgFnS	tvořit
polní	polní	k2eAgFnSc1d1	polní
cesta	cesta	k1gFnSc1	cesta
vedoucí	vedoucí	k1gFnSc2	vedoucí
přes	přes	k7c4	přes
Netroufalky	Netroufalka	k1gFnPc4	Netroufalka
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
hranice	hranice	k1gFnSc2	hranice
spadalo	spadat	k5eAaPmAgNnS	spadat
do	do	k7c2	do
městského	městský	k2eAgInSc2d1	městský
obvodu	obvod	k1gInSc2	obvod
Brno	Brno	k1gNnSc1	Brno
VIII-Bohunice	VIII-Bohunice	k1gFnSc2	VIII-Bohunice
(	(	kIx(	(
<g/>
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1957	[number]	k4	1957
nazýván	nazýván	k2eAgInSc4d1	nazýván
Brno	Brno	k1gNnSc1	Brno
VIII-Bohunice	VIII-Bohunice	k1gFnSc1	VIII-Bohunice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
k	k	k7c3	k
městskému	městský	k2eAgInSc3d1	městský
obvodu	obvod	k1gInSc3	obvod
Brno	Brno	k1gNnSc4	Brno
I	I	kA	I
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
hornoheršpické	hornoheršpický	k2eAgInPc1d1	hornoheršpický
pozemky	pozemek	k1gInPc1	pozemek
spadaly	spadat	k5eAaImAgInP	spadat
do	do	k7c2	do
městského	městský	k2eAgInSc2d1	městský
obvodu	obvod	k1gInSc2	obvod
Brno	Brno	k1gNnSc4	Brno
IX	IX	kA	IX
(	(	kIx(	(
<g/>
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1957	[number]	k4	1957
nazýván	nazýván	k2eAgInSc4d1	nazýván
Brno	Brno	k1gNnSc1	Brno
IX-Horní	IX-Horní	k2eAgFnPc1d1	IX-Horní
Heršpice	Heršpice	k1gFnPc1	Heršpice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
se	se	k3xPyFc4	se
bohunický	bohunický	k2eAgInSc1d1	bohunický
obvod	obvod	k1gInSc1	obvod
nazýval	nazývat	k5eAaImAgInS	nazývat
Bohunice	Bohunice	k1gFnPc4	Bohunice
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hornoheršpický	hornoheršpický	k2eAgInSc4d1	hornoheršpický
Horní	horní	k2eAgFnSc4d1	horní
Heršpice	Heršpice	k1gFnPc4	Heršpice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
stály	stát	k5eAaImAgFnP	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
bohunického	bohunický	k2eAgInSc2d1	bohunický
a	a	k8xC	a
hornoheršpického	hornoheršpický	k2eAgInSc2d1	hornoheršpický
obvodu	obvod	k1gInSc2	obvod
místní	místní	k2eAgInPc4d1	místní
národní	národní	k2eAgInSc4d1	národní
výbory	výbor	k1gInPc4	výbor
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
obvody	obvod	k1gInPc1	obvod
se	se	k3xPyFc4	se
označovaly	označovat	k5eAaImAgInP	označovat
jako	jako	k9	jako
městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
bohunického	bohunický	k2eAgInSc2d1	bohunický
obvodu	obvod	k1gInSc2	obvod
byl	být	k5eAaImAgInS	být
i	i	k9	i
Starý	starý	k2eAgInSc1d1	starý
Lískovec	Lískovec	k1gInSc1	Lískovec
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1975	[number]	k4	1975
-	-	kIx~	-
celé	celý	k2eAgNnSc4d1	celé
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Bohunice	Bohunice	k1gFnPc4	Bohunice
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
tehdejší	tehdejší	k2eAgNnSc4d1	tehdejší
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Starý	starý	k2eAgInSc1d1	starý
Lískovec	Lískovec	k1gInSc1	Lískovec
(	(	kIx(	(
<g/>
k	k	k7c3	k
němuž	jenž	k3xRgMnSc3	jenž
tehdy	tehdy	k6eAd1	tehdy
patřily	patřit	k5eAaImAgInP	patřit
i	i	k9	i
pozemky	pozemek	k1gInPc1	pozemek
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
později	pozdě	k6eAd2	pozdě
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
sídliště	sídliště	k1gNnSc4	sídliště
Kamenný	kamenný	k2eAgMnSc1d1	kamenný
<g />
.	.	kIx.	.
</s>
<s>
Vrch	vrch	k1gInSc1	vrch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvořilo	tvořit	k5eAaImAgNnS	tvořit
městskou	městský	k2eAgFnSc4d1	městská
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
nazývala	nazývat	k5eAaImAgFnS	nazývat
Brno	Brno	k1gNnSc4	Brno
VIII-Bohunice	VIII-Bohunice	k1gFnSc2	VIII-Bohunice
<g/>
,	,	kIx,	,
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1972	[number]	k4	1972
již	již	k6eAd1	již
jen	jen	k9	jen
Brno-Bohunice	Brno-Bohunice	k1gFnSc1	Brno-Bohunice
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1976	[number]	k4	1976
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
-	-	kIx~	-
Bohunice	Bohunice	k1gFnPc1	Bohunice
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
městského	městský	k2eAgInSc2d1	městský
obvodu	obvod	k1gInSc2	obvod
Brno	Brno	k1gNnSc4	Brno
I.	I.	kA	I.
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
-	-	kIx~	-
moderní	moderní	k2eAgFnSc1d1	moderní
samosprávná	samosprávný	k2eAgFnSc1d1	samosprávná
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Brno-Bohunice	Brno-Bohunice	k1gFnSc2	Brno-Bohunice
<g/>
.	.	kIx.	.
</s>
