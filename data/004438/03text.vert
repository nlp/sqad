<s>
Předmostí	předmostí	k1gNnSc1	předmostí
(	(	kIx(	(
<g/>
Přerov	Přerov	k1gInSc1	Přerov
II-Předmostí	II-Předmost	k1gFnPc2	II-Předmost
<g/>
;	;	kIx,	;
německy	německy	k6eAd1	německy
Przedmost	Przedmost	k1gFnSc1	Przedmost
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
místní	místní	k2eAgFnSc4d1	místní
část	část	k1gFnSc4	část
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Přerova	Přerov	k1gInSc2	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
tu	tu	k6eAd1	tu
žilo	žít	k5eAaImAgNnS	žít
5	[number]	k4	5
088	[number]	k4	088
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
břehu	břeh	k1gInSc6	břeh
Bečvy	Bečva	k1gFnSc2	Bečva
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
Moravské	moravský	k2eAgFnSc2d1	Moravská
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
na	na	k7c6	na
nejjižnějších	jižní	k2eAgInPc6d3	nejjižnější
výběžcích	výběžek	k1gInPc6	výběžek
Oderských	oderský	k2eAgInPc2d1	oderský
vrchů	vrch	k1gInPc2	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnSc7d1	přírodní
dominantou	dominanta	k1gFnSc7	dominanta
bývala	bývat	k5eAaImAgFnS	bývat
dvě	dva	k4xCgNnPc4	dva
návrší	návrší	k1gNnPc2	návrší
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
sprašovými	sprašový	k2eAgFnPc7d1	sprašová
nánosy	nános	k1gInPc1	nános
(	(	kIx(	(
<g/>
Hradisko	hradisko	k1gNnSc1	hradisko
a	a	k8xC	a
Skalka	skalka	k1gFnSc1	skalka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
zaniklá	zaniklý	k2eAgNnPc1d1	zaniklé
díky	díky	k7c3	díky
průmyslové	průmyslový	k2eAgFnSc3d1	průmyslová
těžbě	těžba	k1gFnSc3	těžba
<g/>
.	.	kIx.	.
</s>
<s>
Předmostím	předmostí	k1gNnSc7	předmostí
tekla	téct	k5eAaImAgFnS	téct
zemitá	zemitý	k2eAgFnSc1d1	zemitá
kyselka	kyselka	k1gFnSc1	kyselka
<g/>
,	,	kIx,	,
využívaná	využívaný	k2eAgFnSc1d1	využívaná
obyvateli	obyvatel	k1gMnPc7	obyvatel
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Předmostí	předmostí	k1gNnSc2	předmostí
(	(	kIx(	(
<g/>
archeologická	archeologický	k2eAgFnSc1d1	archeologická
lokalita	lokalita	k1gFnSc1	lokalita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předmostí	předmostí	k1gNnSc1	předmostí
je	být	k5eAaImIp3nS	být
významné	významný	k2eAgNnSc1d1	významné
paleolitické	paleolitický	k2eAgNnSc1d1	paleolitické
sídliště	sídliště	k1gNnSc1	sídliště
lovců	lovec	k1gMnPc2	lovec
mamutů	mamut	k1gMnPc2	mamut
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zde	zde	k6eAd1	zde
žili	žít	k5eAaImAgMnP	žít
asi	asi	k9	asi
před	před	k7c7	před
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
před	před	k7c7	před
nimi	on	k3xPp3gInPc7	on
tu	tu	k6eAd1	tu
ale	ale	k9	ale
pobývali	pobývat	k5eAaImAgMnP	pobývat
neandrtálci	neandrtálec	k1gMnPc1	neandrtálec
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
100	[number]	k4	100
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
těm	ten	k3xDgMnPc3	ten
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
podařilo	podařit	k5eAaPmAgNnS	podařit
nalézt	nalézt	k5eAaPmF	nalézt
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Souvislé	souvislý	k2eAgNnSc1d1	souvislé
osídlení	osídlení	k1gNnSc1	osídlení
(	(	kIx(	(
<g/>
s	s	k7c7	s
určitými	určitý	k2eAgFnPc7d1	určitá
mezerami	mezera	k1gFnPc7	mezera
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
prakticky	prakticky	k6eAd1	prakticky
od	od	k7c2	od
mladého	mladý	k2eAgInSc2d1	mladý
paleolitu	paleolit	k1gInSc2	paleolit
(	(	kIx(	(
<g/>
kultura	kultura	k1gFnSc1	kultura
s	s	k7c7	s
lineární	lineární	k2eAgFnSc7d1	lineární
keramikou	keramika	k1gFnSc7	keramika
<g/>
,	,	kIx,	,
5000	[number]	k4	5000
př.n.l.	př.n.l.	k?	př.n.l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
sídlili	sídlit	k5eAaImAgMnP	sídlit
první	první	k4xOgMnPc1	první
zemědělci	zemědělec	k1gMnPc1	zemědělec
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
slovanského	slovanský	k2eAgNnSc2d1	slovanské
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
době	doba	k1gFnSc6	doba
hradištní	hradištní	k2eAgFnSc2d1	hradištní
zde	zde	k6eAd1	zde
stávalo	stávat	k5eAaImAgNnS	stávat
velkomoravské	velkomoravský	k2eAgNnSc1d1	velkomoravské
hradiště	hradiště	k1gNnSc1	hradiště
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
tu	tu	k6eAd1	tu
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
kostrové	kostrový	k2eAgNnSc4d1	kostrové
pohřebiště	pohřebiště	k1gNnSc4	pohřebiště
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Předmostí	předmostí	k1gNnSc2	předmostí
v	v	k7c6	v
mladém	mladý	k2eAgInSc6d1	mladý
paleolitu	paleolit	k1gInSc6	paleolit
zařadil	zařadit	k5eAaPmAgMnS	zařadit
svůj	svůj	k3xOyFgInSc4	svůj
slavný	slavný	k2eAgInSc4d1	slavný
román	román	k1gInSc4	román
Lovci	lovec	k1gMnPc1	lovec
mamutů	mamut	k1gMnPc2	mamut
Eduard	Eduard	k1gMnSc1	Eduard
Štorch	Štorch	k1gMnSc1	Štorch
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
jména	jméno	k1gNnSc2	jméno
Předmostí	předmostí	k1gNnSc2	předmostí
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Místo	místo	k1gNnSc1	místo
před	před	k7c7	před
mostem	most	k1gInSc7	most
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
starobylost	starobylost	k1gFnSc4	starobylost
osídlení	osídlení	k1gNnSc2	osídlení
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
je	být	k5eAaImIp3nS	být
datována	datován	k2eAgFnSc1d1	datována
k	k	k7c3	k
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1362	[number]	k4	1362
<g/>
.	.	kIx.	.
</s>
<s>
Existoval	existovat	k5eAaImAgInS	existovat
tu	tu	k6eAd1	tu
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
farářem	farář	k1gMnSc7	farář
byl	být	k5eAaImAgMnS	být
kněz	kněz	k1gMnSc1	kněz
Macůrek	Macůrka	k1gFnPc2	Macůrka
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
významnější	významný	k2eAgFnPc1d2	významnější
zmínky	zmínka	k1gFnPc1	zmínka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
že	že	k8xS	že
měštka	měštka	k1gFnSc1	měštka
Střešna	Střešna	k1gFnSc1	Střešna
z	z	k7c2	z
Předmostí	předmostí	k1gNnSc2	předmostí
prodala	prodat	k5eAaPmAgFnS	prodat
1368	[number]	k4	1368
dvůr	dvůr	k1gInSc4	dvůr
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
Matějovi	Matěj	k1gMnSc3	Matěj
z	z	k7c2	z
Předmostí	předmostí	k1gNnSc2	předmostí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1374	[number]	k4	1374
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
zmiňován	zmiňován	k2eAgMnSc1d1	zmiňován
půhončů	půhonč	k1gInPc2	půhonč
Vach	Vach	k?	Vach
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
půhončí	půhončí	k1gMnSc1	půhončí
Adam	Adam	k1gMnSc1	Adam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1384	[number]	k4	1384
markrabě	markrabě	k1gMnSc1	markrabě
Prokop	Prokop	k1gMnSc1	Prokop
daroval	darovat	k5eAaPmAgMnS	darovat
Ješkovi	Ješek	k1gMnSc3	Ješek
z	z	k7c2	z
Třebišova	Třebišův	k2eAgMnSc2d1	Třebišův
manský	manský	k2eAgInSc4d1	manský
dvůr	dvůr	k1gInSc4	dvůr
v	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
užívali	užívat	k5eAaImAgMnP	užívat
k	k	k7c3	k
obživě	obživa	k1gFnSc3	obživa
kasteláni	kastelán	k1gMnPc1	kastelán
přerovského	přerovský	k2eAgInSc2d1	přerovský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
a	a	k8xC	a
markrabě	markrabě	k1gMnSc1	markrabě
Jošt	Jošt	k1gMnSc1	Jošt
Moravský	moravský	k2eAgMnSc1d1	moravský
zase	zase	k9	zase
roku	rok	k1gInSc2	rok
1406	[number]	k4	1406
za	za	k7c2	za
věrné	věrný	k2eAgFnSc2d1	věrná
služby	služba	k1gFnSc2	služba
dal	dát	k5eAaPmAgMnS	dát
Sulíkovi	Sulík	k1gMnSc3	Sulík
z	z	k7c2	z
Konice	Konice	k1gFnSc2	Konice
zboží	zboží	k1gNnSc2	zboží
patřící	patřící	k2eAgFnSc1d1	patřící
k	k	k7c3	k
markrabskému	markrabský	k2eAgNnSc3d1	markrabský
šenkovství	šenkovství	k1gNnSc3	šenkovství
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
náležely	náležet	k5eAaImAgFnP	náležet
i	i	k9	i
lány	lán	k1gInPc4	lán
rolí	role	k1gFnPc2	role
v	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1410	[number]	k4	1410
je	být	k5eAaImIp3nS	být
zmiňován	zmiňován	k2eAgInSc1d1	zmiňován
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
s	s	k7c7	s
farářem	farář	k1gMnSc7	farář
Ličkem	Liček	k1gMnSc7	Liček
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
1412	[number]	k4	1412
existují	existovat	k5eAaImIp3nP	existovat
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
soudních	soudní	k2eAgInPc6d1	soudní
sporech	spor	k1gInPc6	spor
mezi	mezi	k7c7	mezi
Mabkou	Mabka	k1gMnSc7	Mabka
a	a	k8xC	a
Petrem	Petr	k1gMnSc7	Petr
z	z	k7c2	z
Předmostí	předmostí	k1gNnSc2	předmostí
ohledně	ohledně	k7c2	ohledně
předmostského	předmostský	k2eAgInSc2d1	předmostský
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
prodaného	prodaný	k2eAgInSc2d1	prodaný
Mabkou	Mabka	k1gFnSc7	Mabka
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
1416	[number]	k4	1416
zápis	zápis	k1gInSc4	zápis
v	v	k7c6	v
moravských	moravský	k2eAgFnPc6d1	Moravská
zemských	zemský	k2eAgFnPc6d1	zemská
deskách	deska	k1gFnPc6	deska
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Rokytnice	Rokytnice	k1gFnSc2	Rokytnice
prodává	prodávat	k5eAaImIp3nS	prodávat
Milotovi	Milot	k1gMnSc3	Milot
z	z	k7c2	z
Tvorkova	tvorkův	k2eAgMnSc2d1	tvorkův
a	a	k8xC	a
dědicům	dědic	k1gMnPc3	dědic
alodiální	alodiální	k2eAgInSc4d1	alodiální
dvorec	dvorec	k1gInSc4	dvorec
s	s	k7c7	s
pozemky	pozemek	k1gInPc7	pozemek
<g/>
,	,	kIx,	,
chlumem	chlum	k1gInSc7	chlum
zvaným	zvaný	k2eAgInSc7d1	zvaný
Skalka	skalka	k1gFnSc1	skalka
a	a	k8xC	a
zahradou	zahrada	k1gFnSc7	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
1447	[number]	k4	1447
prodal	prodat	k5eAaPmAgInS	prodat
markraběcí	markraběcí	k2eAgInSc1d1	markraběcí
dvůr	dvůr	k1gInSc1	dvůr
Martinovi	Martin	k1gMnSc3	Martin
a	a	k8xC	a
Matuškovi	Matuška	k1gMnSc3	Matuška
z	z	k7c2	z
Předmostí	předmostí	k1gNnSc2	předmostí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1487	[number]	k4	1487
<g/>
,	,	kIx,	,
když	když	k8xS	když
král	král	k1gMnSc1	král
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
předal	předat	k5eAaPmAgMnS	předat
k	k	k7c3	k
dědičnému	dědičný	k2eAgNnSc3d1	dědičné
užívání	užívání	k1gNnSc3	užívání
Vilémovi	Vilémův	k2eAgMnPc1d1	Vilémův
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
přerovské	přerovský	k2eAgNnSc1d1	přerovské
panství	panství	k1gNnSc1	panství
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
Předmostí	předmostí	k1gNnSc2	předmostí
<g/>
,	,	kIx,	,
bránili	bránit	k5eAaImAgMnP	bránit
se	se	k3xPyFc4	se
tomu	ten	k3xDgMnSc3	ten
zdejší	zdejší	k2eAgMnPc1d1	zdejší
majitelé	majitel	k1gMnPc1	majitel
svobodných	svobodný	k2eAgInPc2d1	svobodný
dvorů	dvůr	k1gInPc2	dvůr
(	(	kIx(	(
<g/>
Stašek	Stašek	k1gMnSc1	Stašek
ze	z	k7c2	z
Závad	závada	k1gFnPc2	závada
<g/>
,	,	kIx,	,
Ondra	Ondra	k1gMnSc1	Ondra
Korytník	korytník	k1gMnSc1	korytník
<g/>
,	,	kIx,	,
Matěj	Matěj	k1gMnSc1	Matěj
Bacák	Bacák	k1gMnSc1	Bacák
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Vaněk	Vaněk	k1gMnSc1	Vaněk
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Mukař	Mukař	k1gMnSc1	Mukař
z	z	k7c2	z
Kokor	Kokora	k1gFnPc2	Kokora
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tu	tu	k6eAd1	tu
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgFnPc4	tři
poddané	poddaná	k1gFnPc4	poddaná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pernštejnové	Pernštejn	k1gMnPc1	Pernštejn
však	však	k9	však
s	s	k7c7	s
Předmostím	předmostí	k1gNnSc7	předmostí
přece	přece	k9	přece
jen	jen	k9	jen
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
disponovali	disponovat	k5eAaBmAgMnP	disponovat
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
prodal	prodat	k5eAaPmAgMnS	prodat
Vilém	Vilém	k1gMnSc1	Vilém
roku	rok	k1gInSc2	rok
1507	[number]	k4	1507
koupil	koupit	k5eAaPmAgInS	koupit
svobodný	svobodný	k2eAgInSc1d1	svobodný
dvůr	dvůr	k1gInSc1	dvůr
v	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
za	za	k7c7	za
kostelem	kostel	k1gInSc7	kostel
od	od	k7c2	od
zemana	zeman	k1gMnSc2	zeman
Jiřího	Jiří	k1gMnSc2	Jiří
Leščanského	Leščanský	k2eAgInSc2d1	Leščanský
za	za	k7c4	za
180	[number]	k4	180
hřiven	hřivna	k1gFnPc2	hřivna
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1519	[number]	k4	1519
povolil	povolit	k5eAaPmAgMnS	povolit
jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
zřízení	zřízení	k1gNnSc1	zřízení
pastviska	pastvisko	k1gNnSc2	pastvisko
v	v	k7c6	v
lese	les	k1gInSc6	les
"	"	kIx"	"
<g/>
na	na	k7c4	na
Lipovej	Lipovej	k?	Lipovej
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgMnSc1	týž
osvobodil	osvobodit	k5eAaPmAgInS	osvobodit
Předmostské	Předmostský	k2eAgFnPc4d1	Předmostský
od	od	k7c2	od
některých	některý	k3yIgFnPc2	některý
robot	robota	k1gFnPc2	robota
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
změněny	změnit	k5eAaPmNgFnP	změnit
na	na	k7c4	na
peněžitý	peněžitý	k2eAgInSc4d1	peněžitý
plat	plat	k1gInSc4	plat
(	(	kIx(	(
<g/>
1519	[number]	k4	1519
<g/>
,	,	kIx,	,
netýkalo	týkat	k5eNaImAgNnS	týkat
se	se	k3xPyFc4	se
dovážení	dovážení	k1gNnSc1	dovážení
dříví	dříví	k1gNnSc2	dříví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
jim	on	k3xPp3gMnPc3	on
dal	dát	k5eAaPmAgMnS	dát
právo	právo	k1gNnSc4	právo
čepovat	čepovat	k5eAaImF	čepovat
přerovské	přerovský	k2eAgNnSc4d1	přerovské
pivo	pivo	k1gNnSc4	pivo
(	(	kIx(	(
<g/>
privilegium	privilegium	k1gNnSc1	privilegium
trvalo	trvat	k5eAaImAgNnS	trvat
ale	ale	k9	ale
jen	jen	k9	jen
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1564	[number]	k4	1564
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
přenechal	přenechat	k5eAaPmAgInS	přenechat
Přerovským	přerovský	k2eAgMnSc7d1	přerovský
mýto	mýto	k1gNnSc4	mýto
u	u	k7c2	u
Předmostí	předmostí	k1gNnSc2	předmostí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1585	[number]	k4	1585
prodal	prodat	k5eAaPmAgMnS	prodat
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
panství	panství	k1gNnSc2	panství
Janu	Jan	k1gMnSc3	Jan
Manriquovi	Manriqu	k1gMnSc3	Manriqu
de	de	k?	de
Lara	Lara	k1gMnSc1	Lara
<g/>
,	,	kIx,	,
o	o	k7c4	o
9	[number]	k4	9
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
musel	muset	k5eAaImAgMnS	muset
převzít	převzít	k5eAaPmF	převzít
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1596	[number]	k4	1596
se	se	k3xPyFc4	se
novými	nový	k2eAgMnPc7d1	nový
majiteli	majitel	k1gMnPc7	majitel
Předmostí	předmostí	k1gNnSc2	předmostí
stali	stát	k5eAaPmAgMnP	stát
Žerotínové	Žerotín	k1gMnPc1	Žerotín
<g/>
.	.	kIx.	.
</s>
<s>
Předmostí	předmostí	k1gNnSc1	předmostí
pak	pak	k6eAd1	pak
spadalo	spadat	k5eAaImAgNnS	spadat
vlastně	vlastně	k9	vlastně
pod	pod	k7c4	pod
panství	panství	k1gNnPc4	panství
přerovské	přerovský	k2eAgFnPc1d1	přerovská
<g/>
.	.	kIx.	.
</s>
<s>
Žerotínové	Žerotín	k1gMnPc1	Žerotín
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
solný	solný	k2eAgInSc4d1	solný
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
vinopalnu	vinopalna	k1gFnSc4	vinopalna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1516	[number]	k4	1516
odvádělo	odvádět	k5eAaImAgNnS	odvádět
berni	berni	k?	berni
z	z	k7c2	z
Předmostí	předmostí	k1gNnSc2	předmostí
16	[number]	k4	16
usedlých	usedlý	k2eAgFnPc2d1	usedlá
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
Hradisku	hradisko	k1gNnSc6	hradisko
lámal	lámat	k5eAaImAgInS	lámat
devonský	devonský	k2eAgInSc1d1	devonský
vápenec	vápenec	k1gInSc1	vápenec
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1536	[number]	k4	1536
jej	on	k3xPp3gInSc4	on
zde	zde	k6eAd1	zde
dobývali	dobývat	k5eAaImAgMnP	dobývat
Přerované	Přerovan	k1gMnPc1	Přerovan
pro	pro	k7c4	pro
stavební	stavební	k2eAgInPc4d1	stavební
účely	účel	k1gInPc4	účel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
ale	ale	k9	ale
řadila	řadit	k5eAaImAgFnS	řadit
k	k	k7c3	k
bohatšímu	bohatý	k2eAgNnSc3d2	bohatší
rolnictvu	rolnictvo	k1gNnSc3	rolnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
bratři	bratr	k1gMnPc1	bratr
se	se	k3xPyFc4	se
usazovali	usazovat	k5eAaImAgMnP	usazovat
v	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
již	již	k9	již
za	za	k7c4	za
Viléma	Vilém	k1gMnSc4	Vilém
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
,	,	kIx,	,
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	on	k3xPp3gInPc4	on
ale	ale	k9	ale
přečíslili	přečíslit	k5eAaPmAgMnP	přečíslit
luteráni	luterán	k1gMnPc1	luterán
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Maří	mařit	k5eAaImIp3nS	mařit
Magdalény	Magdaléna	k1gFnPc4	Magdaléna
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
luteránský	luteránský	k2eAgMnSc1d1	luteránský
<g/>
.	.	kIx.	.
</s>
<s>
Evangelický	evangelický	k2eAgMnSc1d1	evangelický
předmostský	předmostský	k2eAgMnSc1d1	předmostský
farář	farář	k1gMnSc1	farář
Adolf	Adolf	k1gMnSc1	Adolf
rozpoutal	rozpoutat	k5eAaPmAgMnS	rozpoutat
roku	rok	k1gInSc2	rok
1571	[number]	k4	1571
spor	spor	k1gInSc1	spor
s	s	k7c7	s
městskou	městský	k2eAgFnSc7d1	městská
radou	rada	k1gFnSc7	rada
přerovskou	přerovský	k2eAgFnSc4d1	přerovská
o	o	k7c4	o
pohřby	pohřeb	k1gInPc4	pohřeb
českých	český	k2eAgMnPc2d1	český
bratří	bratr	k1gMnPc2	bratr
na	na	k7c6	na
šířavském	šířavský	k2eAgInSc6d1	šířavský
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
náboženské	náboženský	k2eAgFnPc1d1	náboženská
spory	spora	k1gFnPc1	spora
nastaly	nastat	k5eAaPmAgFnP	nastat
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
předmostští	předmostský	k2eAgMnPc1d1	předmostský
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
faráře	farář	k1gMnSc4	farář
<g/>
,	,	kIx,	,
dosazeného	dosazený	k2eAgInSc2d1	dosazený
biskupem	biskup	k1gMnSc7	biskup
Stanislavem	Stanislav	k1gMnSc7	Stanislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Pavlovským	pavlovský	k2eAgInSc7d1	pavlovský
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgFnSc1d1	následná
třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
svou	svůj	k3xOyFgFnSc4	svůj
daň	daň	k1gFnSc4	daň
i	i	k9	i
v	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
<g/>
.	.	kIx.	.
</s>
<s>
Nekatolíci	nekatolík	k1gMnPc1	nekatolík
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
sama	sám	k3xTgFnSc1	sám
byla	být	k5eAaImAgFnS	být
1642	[number]	k4	1642
vypálena	vypálen	k2eAgFnSc1d1	vypálena
Švédy	švéda	k1gFnPc1	švéda
<g/>
.	.	kIx.	.
</s>
<s>
Katolický	katolický	k2eAgMnSc1d1	katolický
farář	farář	k1gMnSc1	farář
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
dosazen	dosazen	k2eAgMnSc1d1	dosazen
až	až	k8xS	až
mnoho	mnoho	k4c1	mnoho
let	léto	k1gNnPc2	léto
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1657	[number]	k4	1657
jej	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgMnS	mít
spravovat	spravovat	k5eAaImF	spravovat
přerovský	přerovský	k2eAgMnSc1d1	přerovský
farář	farář	k1gMnSc1	farář
Ausswitzer	Ausswitzer	k1gMnSc1	Ausswitzer
<g/>
.	.	kIx.	.
</s>
<s>
Samostatný	samostatný	k2eAgMnSc1d1	samostatný
farář	farář	k1gMnSc1	farář
byl	být	k5eAaImAgMnS	být
ustaven	ustavit	k5eAaPmNgMnS	ustavit
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1661	[number]	k4	1661
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
stal	stát	k5eAaPmAgMnS	stát
Jan	Jan	k1gMnSc1	Jan
Alois	Alois	k1gMnSc1	Alois
Kornelius	Kornelius	k1gMnSc1	Kornelius
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
propukly	propuknout	k5eAaPmAgInP	propuknout
sváry	svár	k1gInPc1	svár
předmostských	předmostský	k2eAgMnPc2d1	předmostský
s	s	k7c7	s
přerovskými	přerovský	k2eAgMnPc7d1	přerovský
měšťany	měšťan	k1gMnPc7	měšťan
<g/>
.	.	kIx.	.
</s>
<s>
Předmostští	Předmostský	k2eAgMnPc1d1	Předmostský
jim	on	k3xPp3gMnPc3	on
totiž	totiž	k9	totiž
uloupili	uloupit	k5eAaPmAgMnP	uloupit
proso	proso	k1gNnSc4	proso
zaseté	zasetý	k2eAgNnSc4d1	zaseté
na	na	k7c6	na
rybníku	rybník	k1gInSc6	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
se	se	k3xPyFc4	se
táhl	táhnout	k5eAaImAgInS	táhnout
20	[number]	k4	20
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1716	[number]	k4	1716
<g/>
–	–	k?	–
<g/>
1736	[number]	k4	1736
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
rybníky	rybník	k1gInPc1	rybník
připadnou	připadnout	k5eAaPmIp3nP	připadnout
městu	město	k1gNnSc3	město
a	a	k8xC	a
Předmostští	Předmostský	k2eAgMnPc1d1	Předmostský
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
pást	pást	k5eAaImF	pást
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
dobytek	dobytek	k1gInSc4	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
válek	válka	k1gFnPc2	válka
o	o	k7c4	o
dědictví	dědictví	k1gNnSc4	dědictví
rakouské	rakouský	k2eAgNnSc4d1	rakouské
protáhli	protáhnout	k5eAaPmAgMnP	protáhnout
obcí	obec	k1gFnPc2	obec
Prusové	Prus	k1gMnPc1	Prus
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
1741	[number]	k4	1741
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrátili	vrátit	k5eAaPmAgMnP	vrátit
se	se	k3xPyFc4	se
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1742	[number]	k4	1742
a	a	k8xC	a
tentokrát	tentokrát	k6eAd1	tentokrát
vyloupili	vyloupit	k5eAaPmAgMnP	vyloupit
farní	farní	k2eAgInSc4d1	farní
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
obec	obec	k1gFnSc1	obec
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Rakušané	Rakušan	k1gMnPc1	Rakušan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ovšem	ovšem	k9	ovšem
měli	mít	k5eAaImAgMnP	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
disciplínou	disciplína	k1gFnSc7	disciplína
–	–	k?	–
část	část	k1gFnSc1	část
uherského	uherský	k2eAgInSc2d1	uherský
pluku	pluk	k1gInSc2	pluk
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
vzbouřila	vzbouřit	k5eAaPmAgFnS	vzbouřit
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
dominanta	dominanta	k1gFnSc1	dominanta
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Maří	mařit	k5eAaImIp3nS	mařit
Magdalény	Magdaléna	k1gFnPc4	Magdaléna
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
přestavěné	přestavěný	k2eAgFnSc6d1	přestavěná
podobě	podoba	k1gFnSc6	podoba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1772	[number]	k4	1772
<g/>
.	.	kIx.	.
</s>
<s>
Sochy	socha	k1gFnPc1	socha
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc4	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnPc4	Metoděj
před	před	k7c7	před
vchodem	vchod	k1gInSc7	vchod
byly	být	k5eAaImAgFnP	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
k	k	k7c3	k
tisíciletému	tisíciletý	k2eAgNnSc3d1	tisícileté
výročí	výročí	k1gNnSc3	výročí
jejich	jejich	k3xOp3gFnSc2	jejich
mise	mise	k1gFnSc2	mise
1863	[number]	k4	1863
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zadní	zadní	k2eAgFnSc6d1	zadní
stěně	stěna	k1gFnSc6	stěna
před	před	k7c7	před
oltářem	oltář	k1gInSc7	oltář
je	být	k5eAaImIp3nS	být
náhrobek	náhrobek	k1gInSc1	náhrobek
Kateřiny	Kateřina	k1gFnSc2	Kateřina
<g/>
,	,	kIx,	,
manželky	manželka	k1gFnPc1	manželka
Kuneše	Kuneš	k1gMnSc2	Kuneš
z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
a	a	k8xC	a
na	na	k7c6	na
Čekyni	Čekyně	k1gFnSc6	Čekyně
(	(	kIx(	(
<g/>
†	†	k?	†
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fara	fara	k1gFnSc1	fara
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1780	[number]	k4	1780
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
stála	stát	k5eAaImAgFnS	stát
ovšem	ovšem	k9	ovšem
fara	fara	k1gFnSc1	fara
starší	starý	k2eAgFnSc1d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Josefinismu	josefinismus	k1gInSc2	josefinismus
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několika	několik	k4yIc3	několik
pokrokovým	pokrokový	k2eAgFnPc3d1	pokroková
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1784	[number]	k4	1784
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
jednotřídní	jednotřídní	k2eAgFnSc1d1	jednotřídní
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvojnásobném	dvojnásobný	k2eAgNnSc6d1	dvojnásobné
vyhoření	vyhoření	k1gNnSc6	vyhoření
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
za	za	k7c2	za
katastrofálního	katastrofální	k2eAgInSc2d1	katastrofální
požáru	požár	k1gInSc2	požár
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
školní	školní	k2eAgFnSc1d1	školní
budova	budova	k1gFnSc1	budova
za	za	k7c7	za
kostelem	kostel	k1gInSc7	kostel
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
1867	[number]	k4	1867
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
na	na	k7c4	na
dvojtřídku	dvojtřídka	k1gFnSc4	dvojtřídka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1807	[number]	k4	1807
založen	založen	k2eAgInSc1d1	založen
nový	nový	k2eAgInSc1d1	nový
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ležící	ležící	k2eAgNnPc1d1	ležící
mimo	mimo	k7c4	mimo
obec	obec	k1gFnSc4	obec
<g/>
,	,	kIx,	,
na	na	k7c6	na
starém	starý	k2eAgInSc6d1	starý
hřbitově	hřbitov	k1gInSc6	hřbitov
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
pohřbívat	pohřbívat	k5eAaImF	pohřbívat
přestalo	přestat	k5eAaPmAgNnS	přestat
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
hřbitov	hřbitov	k1gInSc1	hřbitov
byl	být	k5eAaImAgInS	být
však	však	k9	však
zrušen	zrušit	k5eAaPmNgInS	zrušit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Předmostí	předmostí	k1gNnSc3	předmostí
byly	být	k5eAaImAgFnP	být
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přifařeny	přifařen	k2eAgFnPc1d1	přifařen
Dluhonice	Dluhonice	k1gFnPc1	Dluhonice
<g/>
,	,	kIx,	,
Čekyně	Čekyně	k1gFnPc1	Čekyně
<g/>
,	,	kIx,	,
Vinary	Vinar	k1gInPc1	Vinar
<g/>
,	,	kIx,	,
Popovice	Popovice	k1gFnPc1	Popovice
<g/>
,	,	kIx,	,
Lýsky	Lýsk	k1gInPc1	Lýsk
<g/>
.	.	kIx.	.
</s>
<s>
Farář	farář	k1gMnSc1	farář
dostával	dostávat	k5eAaImAgMnS	dostávat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
desátek	desátka	k1gFnPc2	desátka
v	v	k7c6	v
naturáliích	naturálie	k1gFnPc6	naturálie
i	i	k8xC	i
penězích	peníze	k1gInPc6	peníze
(	(	kIx(	(
<g/>
108	[number]	k4	108
zl	zl	k?	zl
<g/>
.	.	kIx.	.
6	[number]	k4	6
krejcarů	krejcar	k1gInPc2	krejcar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dorazila	dorazit	k5eAaPmAgFnS	dorazit
do	do	k7c2	do
Předmostí	předmostí	k1gNnSc2	předmostí
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Spraše	spraš	k1gFnPc1	spraš
byly	být	k5eAaImAgFnP	být
využívány	využívat	k5eAaPmNgFnP	využívat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
místních	místní	k2eAgFnPc6d1	místní
cihelnách	cihelna	k1gFnPc6	cihelna
–	–	k?	–
Fryčkově	Fryčkův	k2eAgFnSc3d1	Fryčkův
(	(	kIx(	(
<g/>
od	od	k7c2	od
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mandlově	mandlově	k6eAd1	mandlově
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
Společenské	společenský	k2eAgFnPc4d1	společenská
<g/>
,	,	kIx,	,
kopec	kopec	k1gInSc1	kopec
Hradisko	hradisko	k1gNnSc4	hradisko
byl	být	k5eAaImAgInS	být
díky	díky	k7c3	díky
těžbě	těžba	k1gFnSc3	těžba
vápence	vápenec	k1gInSc2	vápenec
(	(	kIx(	(
<g/>
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
též	též	k9	též
díky	díky	k7c3	díky
archeologickým	archeologický	k2eAgFnPc3d1	archeologická
vykopávkám	vykopávka	k1gFnPc3	vykopávka
<g/>
)	)	kIx)	)
zcela	zcela	k6eAd1	zcela
srovnán	srovnán	k2eAgInSc1d1	srovnán
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
ze	z	k7c2	z
Skalky	skalka	k1gFnSc2	skalka
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
jen	jen	k9	jen
torzo	torzo	k1gNnSc1	torzo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
křižovatka	křižovatka	k1gFnSc1	křižovatka
železniční	železniční	k2eAgFnSc2d1	železniční
tratě	trať	k1gFnSc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
zpracovávány	zpracováván	k2eAgFnPc1d1	zpracovávána
nebyly	být	k5eNaImAgFnP	být
jen	jen	k9	jen
hlíny	hlína	k1gFnPc1	hlína
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
i	i	k9	i
četné	četný	k2eAgFnPc1d1	četná
mamutí	mamutí	k2eAgFnPc1d1	mamutí
kosti	kost	k1gFnPc1	kost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
se	se	k3xPyFc4	se
vyvážely	vyvážet	k5eAaImAgFnP	vyvážet
do	do	k7c2	do
přerovského	přerovský	k2eAgInSc2d1	přerovský
cukrovaru	cukrovar	k1gInSc2	cukrovar
(	(	kIx(	(
<g/>
využívaly	využívat	k5eAaImAgFnP	využívat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
spodia	spodium	k1gNnSc2	spodium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rolníci	rolník	k1gMnPc1	rolník
používali	používat	k5eAaImAgMnP	používat
kosti	kost	k1gFnPc4	kost
jako	jako	k8xC	jako
hnojivo	hnojivo	k1gNnSc4	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Plundrování	plundrování	k1gNnSc1	plundrování
přírodního	přírodní	k2eAgNnSc2d1	přírodní
bohatství	bohatství	k1gNnSc2	bohatství
bylo	být	k5eAaImAgNnS	být
alarmující	alarmující	k2eAgNnSc1d1	alarmující
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgInPc4d1	archeologický
výzkumy	výzkum	k1gInPc4	výzkum
prováděli	provádět	k5eAaImAgMnP	provádět
nekoordinovaně	koordinovaně	k6eNd1	koordinovaně
hlavně	hlavně	k6eAd1	hlavně
soukromí	soukromý	k2eAgMnPc1d1	soukromý
badatelé	badatel	k1gMnPc1	badatel
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
velkým	velký	k2eAgFnPc3d1	velká
ztrátám	ztráta	k1gFnPc3	ztráta
vědeckého	vědecký	k2eAgInSc2d1	vědecký
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
se	se	k3xPyFc4	se
vykopávek	vykopávka	k1gFnPc2	vykopávka
na	na	k7c6	na
profesionální	profesionální	k2eAgFnSc6d1	profesionální
úrovni	úroveň	k1gFnSc6	úroveň
ujímá	ujímat	k5eAaImIp3nS	ujímat
Jindřich	Jindřich	k1gMnSc1	Jindřich
Wankel	Wankel	k1gMnSc1	Wankel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
tu	tu	k6eAd1	tu
kopali	kopat	k5eAaImAgMnP	kopat
Karel	Karel	k1gMnSc1	Karel
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Maška	Mašek	k1gMnSc2	Mašek
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Kříž	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Matiegka	Matiegka	k1gMnSc1	Matiegka
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějšími	úspěšný	k2eAgMnPc7d3	nejúspěšnější
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
léta	léto	k1gNnSc2	léto
1894	[number]	k4	1894
<g/>
-	-	kIx~	-
<g/>
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Objeven	objevit	k5eAaPmNgInS	objevit
byl	být	k5eAaImAgInS	být
hrob	hrob	k1gInSc1	hrob
20	[number]	k4	20
lidí	člověk	k1gMnPc2	člověk
archaického	archaický	k2eAgInSc2d1	archaický
typu	typ	k1gInSc2	typ
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
sapiens	sapiens	k6eAd1	sapiens
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc1d1	známá
sošky	soška	k1gFnPc1	soška
(	(	kIx(	(
<g/>
několik	několik	k4yIc4	několik
sedících	sedící	k2eAgFnPc2d1	sedící
venuší	venuše	k1gFnPc2	venuše
<g/>
,	,	kIx,	,
soška	soška	k1gFnSc1	soška
mamuta	mamut	k1gMnSc2	mamut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rytina	rytina	k1gFnSc1	rytina
geometrické	geometrický	k2eAgFnSc2d1	geometrická
venuše	venuše	k1gFnSc2	venuše
v	v	k7c6	v
mamutím	mamutí	k2eAgInSc6d1	mamutí
klu	kel	k1gInSc6	kel
apod.	apod.	kA	apod.
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
nálezy	nález	k1gInPc1	nález
byly	být	k5eAaImAgInP	být
datovány	datovat	k5eAaImNgInP	datovat
do	do	k7c2	do
mladšího	mladý	k2eAgInSc2d2	mladší
paleolitu	paleolit	k1gInSc2	paleolit
(	(	kIx(	(
<g/>
stáří	stáří	k1gNnSc2	stáří
asi	asi	k9	asi
25	[number]	k4	25
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předmostí	předmostí	k1gNnSc1	předmostí
vešlo	vejít	k5eAaPmAgNnS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
navštívily	navštívit	k5eAaPmAgFnP	navštívit
jej	on	k3xPp3gMnSc4	on
i	i	k9	i
mezinárodně	mezinárodně	k6eAd1	mezinárodně
proslulí	proslulý	k2eAgMnPc1d1	proslulý
archeologové	archeolog	k1gMnPc1	archeolog
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Kříž	Kříž	k1gMnSc1	Kříž
ukončil	ukončit	k5eAaPmAgMnS	ukončit
výzkumy	výzkum	k1gInPc4	výzkum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
novým	nový	k2eAgInPc3d1	nový
výkopům	výkop	k1gInPc3	výkop
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
dvoutřídní	dvoutřídní	k2eAgFnSc1d1	dvoutřídní
školní	školní	k2eAgFnSc1d1	školní
budova	budova	k1gFnSc1	budova
(	(	kIx(	(
<g/>
sloužila	sloužit	k5eAaImAgFnS	sloužit
samostatně	samostatně	k6eAd1	samostatně
devadesát	devadesát	k4xCc4	devadesát
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1905	[number]	k4	1905
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
Čekyňským	Čekyňský	k2eAgInSc7d1	Čekyňský
kopcem	kopec	k1gInSc7	kopec
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
lomu	lom	k1gInSc3	lom
vojenská	vojenský	k2eAgFnSc1d1	vojenská
střelnice	střelnice	k1gFnSc1	střelnice
<g/>
.	.	kIx.	.
</s>
<s>
Rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
se	se	k3xPyFc4	se
spolkový	spolkový	k2eAgInSc1d1	spolkový
život	život	k1gInSc1	život
–	–	k?	–
prvním	první	k4xOgMnSc6	první
spolkem	spolek	k1gInSc7	spolek
byl	být	k5eAaImAgInS	být
čtenářský	čtenářský	k2eAgInSc1d1	čtenářský
a	a	k8xC	a
pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
spolek	spolek	k1gInSc1	spolek
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
následován	následován	k2eAgMnSc1d1	následován
pobočkou	pobočka	k1gFnSc7	pobočka
Matice	matice	k1gFnSc2	matice
školské	školská	k1gFnSc2	školská
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
katolickým	katolický	k2eAgInSc7d1	katolický
spolkem	spolek	k1gInSc7	spolek
1902	[number]	k4	1902
a	a	k8xC	a
Národní	národní	k2eAgFnSc7d1	národní
jednotou	jednota	k1gFnSc7	jednota
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Spolkový	spolkový	k2eAgInSc1d1	spolkový
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
výhradně	výhradně	k6eAd1	výhradně
český	český	k2eAgInSc1d1	český
<g/>
,	,	kIx,	,
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
Židů	Žid	k1gMnPc2	Žid
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
jen	jen	k9	jen
pár	pár	k4xCyI	pár
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
se	se	k3xPyFc4	se
vyskytla	vyskytnout	k5eAaPmAgFnS	vyskytnout
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
obec	obec	k1gFnSc1	obec
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
těsně	těsně	k6eAd1	těsně
přiléhající	přiléhající	k2eAgFnSc1d1	přiléhající
k	k	k7c3	k
Přerovu	Přerov	k1gInSc3	Přerov
<g/>
,	,	kIx,	,
ponechat	ponechat	k5eAaPmF	ponechat
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
učinit	učinit	k5eAaImF	učinit
součást	součást	k1gFnSc4	součást
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
můžeme	moct	k5eAaImIp1nP	moct
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
spojení	spojení	k1gNnSc4	spojení
se	se	k3xPyFc4	se
vyslovovala	vyslovovat	k5eAaImAgFnS	vyslovovat
radnice	radnice	k1gFnSc1	radnice
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
pak	pak	k6eAd1	pak
zastupitelé	zastupitel	k1gMnPc1	zastupitel
Předmostí	předmostí	k1gNnSc2	předmostí
<g/>
.	.	kIx.	.
</s>
<s>
Motivace	motivace	k1gFnSc1	motivace
Přerovanů	Přerovan	k1gMnPc2	Přerovan
vycházela	vycházet	k5eAaImAgFnS	vycházet
ze	z	k7c2	z
snahy	snaha	k1gFnSc2	snaha
docílit	docílit	k5eAaPmF	docílit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Přerov	Přerov	k1gInSc1	Přerov
měl	mít	k5eAaImAgInS	mít
25	[number]	k4	25
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgMnS	být
přeřazen	přeřadit	k5eAaPmNgMnS	přeřadit
do	do	k7c2	do
lepší	dobrý	k2eAgFnSc2d2	lepší
daňové	daňový	k2eAgFnSc2d1	daňová
skupiny	skupina	k1gFnSc2	skupina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Předmostští	Předmostský	k2eAgMnPc1d1	Předmostský
pak	pak	k6eAd1	pak
nejevili	jevit	k5eNaImAgMnP	jevit
příliš	příliš	k6eAd1	příliš
nadšení	nadšený	k2eAgMnPc1d1	nadšený
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
místní	místní	k2eAgFnSc1d1	místní
obecní	obecní	k2eAgFnSc1d1	obecní
finance	finance	k1gFnSc1	finance
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
podstatně	podstatně	k6eAd1	podstatně
lepším	dobrý	k2eAgInSc6d2	lepší
stavu	stav	k1gInSc6	stav
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Přerova	Přerov	k1gInSc2	Přerov
a	a	k8xC	a
nešlo	jít	k5eNaImAgNnS	jít
tedy	tedy	k9	tedy
očekávat	očekávat	k5eAaImF	očekávat
nějaký	nějaký	k3yIgInSc4	nějaký
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
zisk	zisk	k1gInSc4	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
samostatnost	samostatnost	k1gFnSc4	samostatnost
Předmostí	předmostí	k1gNnSc2	předmostí
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
stavěl	stavět	k5eAaImAgMnS	stavět
také	také	k9	také
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
národní	národní	k2eAgMnSc1d1	národní
socialista	socialista	k1gMnSc1	socialista
Josef	Josef	k1gMnSc1	Josef
Knejzlík	Knejzlík	k1gMnSc1	Knejzlík
(	(	kIx(	(
<g/>
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
obecního	obecní	k2eAgNnSc2d1	obecní
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
dopadly	dopadnout	k5eAaPmAgFnP	dopadnout
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Předmostí	předmostí	k1gNnSc1	předmostí
se	se	k3xPyFc4	se
tak	tak	k9	tak
prakticky	prakticky	k6eAd1	prakticky
rozdělilo	rozdělit	k5eAaPmAgNnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
táborů	tábor	k1gInPc2	tábor
–	–	k?	–
lidoveckého	lidovecký	k2eAgNnSc2d1	lidovecké
a	a	k8xC	a
národně	národně	k6eAd1	národně
socialistického	socialistický	k2eAgNnSc2d1	socialistické
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
v	v	k7c6	v
obecních	obecní	k2eAgFnPc6d1	obecní
volbách	volba	k1gFnPc6	volba
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
<g/>
,	,	kIx,	,
prosadila	prosadit	k5eAaPmAgFnS	prosadit
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
polarizace	polarizace	k1gFnSc1	polarizace
přinesla	přinést	k5eAaPmAgFnS	přinést
mnohé	mnohý	k2eAgInPc4d1	mnohý
vyhraněné	vyhraněný	k2eAgInPc4d1	vyhraněný
projevy	projev	k1gInPc4	projev
dobového	dobový	k2eAgInSc2d1	dobový
politického	politický	k2eAgInSc2d1	politický
folklóru	folklór	k1gInSc2	folklór
<g/>
.	.	kIx.	.
</s>
<s>
Okázalé	okázalý	k2eAgFnPc1d1	okázalá
náboženské	náboženský	k2eAgFnPc1d1	náboženská
slavnosti	slavnost	k1gFnPc1	slavnost
následovaly	následovat	k5eAaImAgFnP	následovat
antiklerikální	antiklerikální	k2eAgFnPc1d1	antiklerikální
trucakce	trucakce	k1gFnPc1	trucakce
(	(	kIx(	(
<g/>
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lidovci	lidovec	k1gMnPc1	lidovec
si	se	k3xPyFc3	se
stěžovali	stěžovat	k5eAaImAgMnP	stěžovat
na	na	k7c4	na
duchovní	duchovní	k2eAgInSc4d1	duchovní
útisk	útisk	k1gInSc4	útisk
<g/>
,	,	kIx,	,
národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
deklarovali	deklarovat	k5eAaBmAgMnP	deklarovat
boj	boj	k1gInSc4	boj
za	za	k7c4	za
kulturní	kulturní	k2eAgInSc4d1	kulturní
pokrok	pokrok	k1gInSc4	pokrok
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
starosta	starosta	k1gMnSc1	starosta
Josef	Josef	k1gMnSc1	Josef
Knejzlík	Knejzlík	k1gMnSc1	Knejzlík
byl	být	k5eAaImAgMnS	být
velice	velice	k6eAd1	velice
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
protivném	protivný	k2eAgInSc6d1	protivný
táboře	tábor	k1gInSc6	tábor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
častým	častý	k2eAgInSc7d1	častý
terčem	terč	k1gInSc7	terč
útoků	útok	k1gInPc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Nescházely	scházet	k5eNaImAgInP	scházet
ani	ani	k9	ani
žaloby	žaloba	k1gFnSc2	žaloba
za	za	k7c4	za
urážky	urážka	k1gFnPc4	urážka
na	na	k7c6	na
cti	čest	k1gFnSc6	čest
apod.	apod.	kA	apod.
I	i	k8xC	i
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
vykopávky	vykopávka	k1gFnPc1	vykopávka
(	(	kIx(	(
<g/>
A.	A.	kA	A.
Telička	telička	k1gFnSc1	telička
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Absolon	Absolon	k1gMnSc1	Absolon
<g/>
,	,	kIx,	,
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
Karel	Karel	k1gMnSc1	Karel
Žebera	Žeber	k1gMnSc2	Žeber
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
bylo	být	k5eAaImAgNnS	být
schváleno	schválit	k5eAaPmNgNnS	schválit
otevření	otevření	k1gNnSc1	otevření
měšťanky	měšťanka	k1gFnSc2	měšťanka
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
ale	ale	k9	ale
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
dobudována	dobudován	k2eAgFnSc1d1	dobudována
přístavba	přístavba	k1gFnSc1	přístavba
ke	k	k7c3	k
školní	školní	k2eAgFnSc3d1	školní
budově	budova	k1gFnSc3	budova
<g/>
.	.	kIx.	.
</s>
<s>
Měšťanka	měšťanka	k1gFnSc1	měšťanka
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
tři	tři	k4xCgFnPc4	tři
třídy	třída	k1gFnPc1	třída
se	s	k7c7	s
119	[number]	k4	119
žáky	žák	k1gMnPc7	žák
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
průtazích	průtah	k1gInPc6	průtah
zprovozněn	zprovozněn	k2eAgInSc4d1	zprovozněn
podjezd	podjezd	k1gInSc4	podjezd
mezi	mezi	k7c7	mezi
Předmostím	předmostí	k1gNnSc7	předmostí
a	a	k8xC	a
Přerovem	Přerov	k1gInSc7	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
podnikem	podnik	k1gInSc7	podnik
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
byly	být	k5eAaImAgFnP	být
Společenské	společenský	k2eAgFnPc4d1	společenská
cihelny	cihelna	k1gFnPc4	cihelna
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
roku	rok	k1gInSc3	rok
1935	[number]	k4	1935
převzal	převzít	k5eAaPmAgMnS	převzít
L.	L.	kA	L.
Šilhavý	šilhavý	k2eAgMnSc1d1	šilhavý
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
pivovaru	pivovar	k1gInSc2	pivovar
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
cihelna	cihelna	k1gFnSc1	cihelna
<g/>
,	,	kIx,	,
Přikrylova	Přikrylův	k2eAgFnSc1d1	Přikrylova
<g/>
,	,	kIx,	,
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
konjunktury	konjunktura	k1gFnSc2	konjunktura
120	[number]	k4	120
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
pivní	pivní	k2eAgInPc1d1	pivní
tlakostroje	tlakostroj	k1gInPc1	tlakostroj
a	a	k8xC	a
lihoviny	lihovina	k1gFnPc1	lihovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
byla	být	k5eAaImAgFnS	být
dále	daleko	k6eAd2	daleko
pošta	pošta	k1gFnSc1	pošta
a	a	k8xC	a
četnická	četnický	k2eAgFnSc1d1	četnická
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
spolků	spolek	k1gInPc2	spolek
<g/>
,	,	kIx,	,
především	především	k9	především
Sokol	Sokol	k1gMnSc1	Sokol
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgInSc1d1	divadelní
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
chovatelský	chovatelský	k2eAgInSc1d1	chovatelský
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
spolků	spolek	k1gInPc2	spolek
fungoval	fungovat	k5eAaImAgMnS	fungovat
Orel	Orel	k1gMnSc1	Orel
<g/>
,	,	kIx,	,
DTJ	DTJ	kA	DTJ
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
též	též	k9	též
FDTJ	FDTJ	kA	FDTJ
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
založili	založit	k5eAaPmAgMnP	založit
též	též	k9	též
konzum	konzum	k1gInSc4	konzum
a	a	k8xC	a
organizaci	organizace	k1gFnSc4	organizace
své	svůj	k3xOyFgFnSc2	svůj
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
lidovci	lidovec	k1gMnPc1	lidovec
družstvo	družstvo	k1gNnSc1	družstvo
Lidového	lidový	k2eAgInSc2d1	lidový
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Dělníci	dělník	k1gMnPc1	dělník
byli	být	k5eAaImAgMnP	být
členy	člen	k1gMnPc4	člen
několika	několik	k4yIc2	několik
odborových	odborový	k2eAgFnPc2d1	odborová
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
národně	národně	k6eAd1	národně
socialistických	socialistický	k2eAgInPc2d1	socialistický
a	a	k8xC	a
komunistických	komunistický	k2eAgInPc2d1	komunistický
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
odbočka	odbočka	k1gFnSc1	odbočka
Národního	národní	k2eAgNnSc2d1	národní
všeodborového	všeodborový	k2eAgNnSc2d1	Všeodborové
sdružení	sdružení	k1gNnSc2	sdružení
(	(	kIx(	(
<g/>
napojeného	napojený	k2eAgMnSc4d1	napojený
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
demokracii	demokracie	k1gFnSc6	demokracie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
československé	československý	k2eAgFnSc2d1	Československá
církve	církev	k1gFnSc2	církev
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Spolek	spolek	k1gInSc1	spolek
proletářských	proletářský	k2eAgMnPc2d1	proletářský
bezvěrců	bezvěrec	k1gMnPc2	bezvěrec
<g/>
.	.	kIx.	.
</s>
<s>
Katolíků	katolík	k1gMnPc2	katolík
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
relativně	relativně	k6eAd1	relativně
ubývalo	ubývat	k5eAaImAgNnS	ubývat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
církvi	církev	k1gFnSc3	církev
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
vzniku	vznik	k1gInSc6	vznik
<g/>
,	,	kIx,	,
81	[number]	k4	81
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
o	o	k7c4	o
9	[number]	k4	9
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
již	již	k6eAd1	již
173	[number]	k4	173
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
církvím	církev	k1gFnPc3	církev
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
(	(	kIx(	(
<g/>
evangelíkům	evangelík	k1gMnPc3	evangelík
<g/>
,	,	kIx,	,
Židům	Žid	k1gMnPc3	Žid
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
jen	jen	k9	jen
pár	pár	k4xCyI	pár
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
,	,	kIx,	,
bezvěrců	bezvěrec	k1gMnPc2	bezvěrec
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
97	[number]	k4	97
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
působila	působit	k5eAaImAgFnS	působit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
populární	populární	k2eAgFnSc1d1	populární
dechovka	dechovka	k1gFnSc1	dechovka
Hanácká	hanácký	k2eAgFnSc1d1	Hanácká
hudba	hudba	k1gFnSc1	hudba
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
kapelníkem	kapelník	k1gMnSc7	kapelník
Josefem	Josef	k1gMnSc7	Josef
Janíčkem	Janíček	k1gMnSc7	Janíček
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Knejzlík	Knejzlík	k1gMnSc1	Knejzlík
vedl	vést	k5eAaImAgInS	vést
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
časopis	časopis	k1gInSc4	časopis
Rádce	rádce	k1gMnSc1	rádce
z	z	k7c2	z
Předmostí	předmostí	k1gNnSc2	předmostí
(	(	kIx(	(
<g/>
do	do	k7c2	do
1924	[number]	k4	1924
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Český	český	k2eAgMnSc1d1	český
Rolník	rolník	k1gMnSc1	rolník
<g/>
,	,	kIx,	,
od	od	k7c2	od
1951	[number]	k4	1951
Rádce	rádce	k1gMnSc1	rádce
zahrádkářů	zahrádkář	k1gMnPc2	zahrádkář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vycházel	vycházet	k5eAaImAgMnS	vycházet
v	v	k7c6	v
letech	let	k1gInPc6	let
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgInSc2	ten
vycházel	vycházet	k5eAaImAgMnS	vycházet
ještě	ještě	k9	ještě
v	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
Farní	farní	k2eAgInSc4d1	farní
věstník	věstník	k1gInSc4	věstník
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Předmostí	předmostí	k1gNnSc4	předmostí
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Přerovem	Přerov	k1gInSc7	Přerov
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
pochopitelně	pochopitelně	k6eAd1	pochopitelně
nepřivítaly	přivítat	k5eNaPmAgFnP	přivítat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
se	se	k3xPyFc4	se
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
objevily	objevit	k5eAaPmAgInP	objevit
protiněmecké	protiněmecký	k2eAgInPc1d1	protiněmecký
letáky	leták	k1gInPc1	leták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
přebrala	přebrat	k5eAaPmAgFnS	přebrat
otěže	otěž	k1gFnPc4	otěž
starostování	starostování	k?	starostování
dřívější	dřívější	k2eAgMnSc1d1	dřívější
lidovec	lidovec	k1gMnSc1	lidovec
a	a	k8xC	a
pracovník	pracovník	k1gMnSc1	pracovník
ve	v	k7c6	v
spolku	spolek	k1gInSc6	spolek
Orel	Orel	k1gMnSc1	Orel
Bedřich	Bedřich	k1gMnSc1	Bedřich
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Knejzlík	Knejzlík	k1gMnSc1	Knejzlík
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Blížící	blížící	k2eAgMnSc1d1	blížící
se	se	k3xPyFc4	se
konec	konec	k1gInSc1	konec
války	válka	k1gFnSc2	válka
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
i	i	k9	i
do	do	k7c2	do
života	život	k1gInSc2	život
Předmostských	Předmostský	k2eAgMnPc2d1	Předmostský
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
školní	školní	k2eAgFnSc1d1	školní
budova	budova	k1gFnSc1	budova
přidělena	přidělen	k2eAgFnSc1d1	přidělena
Vlasovcům	vlasovec	k1gMnPc3	vlasovec
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
nastěhovala	nastěhovat	k5eAaPmAgFnS	nastěhovat
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
obce	obec	k1gFnSc2	obec
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
krátce	krátce	k6eAd1	krátce
(	(	kIx(	(
<g/>
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
politické	politický	k2eAgInPc1d1	politický
poměry	poměr	k1gInPc1	poměr
změnily	změnit	k5eAaPmAgInP	změnit
<g/>
,	,	kIx,	,
národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
ztratili	ztratit	k5eAaPmAgMnP	ztratit
svou	svůj	k3xOyFgFnSc4	svůj
majoritu	majorita	k1gFnSc4	majorita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
1946	[number]	k4	1946
dali	dát	k5eAaPmAgMnP	dát
občané	občan	k1gMnPc1	občan
z	z	k7c2	z
Předmostí	předmostí	k1gNnSc2	předmostí
nejvíce	nejvíce	k6eAd1	nejvíce
hlasů	hlas	k1gInPc2	hlas
KSČ	KSČ	kA	KSČ
(	(	kIx(	(
<g/>
454	[number]	k4	454
<g/>
)	)	kIx)	)
před	před	k7c7	před
ČSNS	ČSNS	kA	ČSNS
(	(	kIx(	(
<g/>
309	[number]	k4	309
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ČSL	ČSL	kA	ČSL
(	(	kIx(	(
<g/>
238	[number]	k4	238
<g/>
)	)	kIx)	)
a	a	k8xC	a
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
sociální	sociální	k2eAgFnSc7d1	sociální
demokracií	demokracie	k1gFnSc7	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přetřes	přetřes	k1gInSc4	přetřes
znovu	znovu	k6eAd1	znovu
přišla	přijít	k5eAaPmAgFnS	přijít
otázka	otázka	k1gFnSc1	otázka
spojení	spojení	k1gNnSc2	spojení
obce	obec	k1gFnSc2	obec
s	s	k7c7	s
Přerovem	Přerov	k1gInSc7	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
k	k	k7c3	k
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
společné	společný	k2eAgNnSc1d1	společné
MNV	MNV	kA	MNV
pro	pro	k7c4	pro
Přerov	Přerov	k1gInSc4	Přerov
a	a	k8xC	a
připojené	připojený	k2eAgFnSc2d1	připojená
obce	obec	k1gFnSc2	obec
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
včetně	včetně	k7c2	včetně
Přerova	Přerov	k1gInSc2	Přerov
II	II	kA	II
-	-	kIx~	-
Předmostí	předmostí	k1gNnSc1	předmostí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Předmostí	předmostí	k1gNnSc1	předmostí
znovu	znovu	k6eAd1	znovu
osamostatnilo	osamostatnit	k5eAaPmAgNnS	osamostatnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
JZD	JZD	kA	JZD
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1960	[number]	k4	1960
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Předmostí	předmostí	k1gNnSc1	předmostí
definitivně	definitivně	k6eAd1	definitivně
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
Přerova	Přerov	k1gInSc2	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
reorganizaci	reorganizace	k1gFnSc3	reorganizace
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
zázemí	zázemí	k1gNnSc2	zázemí
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
cihelna	cihelna	k1gFnSc1	cihelna
připadla	připadnout	k5eAaPmAgFnS	připadnout
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
nově	nově	k6eAd1	nově
vytvořeného	vytvořený	k2eAgInSc2d1	vytvořený
podniku	podnik	k1gInSc2	podnik
Severomoravské	severomoravský	k2eAgFnSc2d1	Severomoravská
cihelny	cihelna	k1gFnSc2	cihelna
(	(	kIx(	(
<g/>
podnik	podnik	k1gInSc1	podnik
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
sloučením	sloučení	k1gNnSc7	sloučení
Hanáckých	hanácký	k2eAgFnPc2d1	Hanácká
ciehelen	ciehelna	k1gFnPc2	ciehelna
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
Hlučínských	hlučínský	k2eAgFnPc2d1	Hlučínská
Slezských	slezský	k2eAgFnPc2d1	Slezská
Cihelen	cihelna	k1gFnPc2	cihelna
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
JZD	JZD	kA	JZD
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
sloučeno	sloučen	k2eAgNnSc4d1	sloučeno
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
dalšími	další	k2eAgFnPc7d1	další
jinými	jiná	k1gFnPc7	jiná
do	do	k7c2	do
většího	veliký	k2eAgInSc2d2	veliký
celku	celek	k1gInSc2	celek
Rudý	rudý	k2eAgInSc1d1	rudý
Říjen	říjen	k1gInSc1	říjen
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Brodku	brodek	k1gInSc6	brodek
u	u	k7c2	u
Přerova	Přerov	k1gInSc2	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
událostem	událost	k1gFnPc3	událost
patřilo	patřit	k5eAaImAgNnS	patřit
otevření	otevření	k1gNnSc3	otevření
kina	kino	k1gNnSc2	kino
Panorama	panorama	k1gNnSc4	panorama
v	v	k7c6	v
září	září	k1gNnSc6	září
1964	[number]	k4	1964
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Orlovny	orlovna	k1gFnSc2	orlovna
a	a	k8xC	a
první	první	k4xOgInSc4	první
autokrosový	autokrosový	k2eAgInSc4d1	autokrosový
závod	závod	k1gInSc4	závod
na	na	k7c4	na
území	území	k1gNnSc4	území
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
cihelny	cihelna	k1gFnSc2	cihelna
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgNnSc1d1	sportovní
vyžití	vyžití	k1gNnSc1	vyžití
zajišťoval	zajišťovat	k5eAaImAgInS	zajišťovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
TJ	tj	kA	tj
OSP	OSP	kA	OSP
<g/>
.	.	kIx.	.
</s>
<s>
Převratné	převratný	k2eAgFnPc1d1	převratná
změny	změna	k1gFnPc1	změna
však	však	k9	však
čekaly	čekat	k5eAaImAgFnP	čekat
Předmostí	předmostí	k1gNnSc4	předmostí
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
výstavba	výstavba	k1gFnSc1	výstavba
panelového	panelový	k2eAgNnSc2d1	panelové
sídliště	sídliště	k1gNnSc2	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Dokončena	dokončen	k2eAgFnSc1d1	dokončena
byla	být	k5eAaImAgFnS	být
prakticky	prakticky	k6eAd1	prakticky
až	až	k6eAd1	až
koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
vesnický	vesnický	k2eAgInSc1d1	vesnický
ráz	ráz	k1gInSc1	ráz
Předmostí	předmostí	k1gNnSc2	předmostí
byl	být	k5eAaImAgInS	být
zcela	zcela	k6eAd1	zcela
setřen	setřít	k5eAaPmNgInS	setřít
a	a	k8xC	a
změněn	změnit	k5eAaPmNgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Předmostí	předmostí	k1gNnSc1	předmostí
nyní	nyní	k6eAd1	nyní
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jako	jako	k9	jako
běžné	běžný	k2eAgNnSc1d1	běžné
panelové	panelový	k2eAgNnSc1d1	panelové
sídliště	sídliště	k1gNnSc1	sídliště
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
kterémkoliv	kterýkoliv	k3yIgNnSc6	kterýkoliv
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
ale	ale	k9	ale
pominout	pominout	k5eAaPmF	pominout
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
nových	nový	k2eAgInPc2d1	nový
bytů	byt	k1gInPc2	byt
značně	značně	k6eAd1	značně
odlehčil	odlehčit	k5eAaPmAgMnS	odlehčit
tísnivé	tísnivý	k2eAgFnSc3d1	tísnivá
bytové	bytový	k2eAgFnSc3d1	bytová
krizi	krize	k1gFnSc3	krize
<g/>
.	.	kIx.	.
</s>
<s>
Pamatováno	pamatován	k2eAgNnSc1d1	pamatováno
bylo	být	k5eAaImAgNnS	být
též	též	k6eAd1	též
na	na	k7c4	na
sociální	sociální	k2eAgNnSc4d1	sociální
a	a	k8xC	a
kulturní	kulturní	k2eAgNnSc4d1	kulturní
zázemí	zázemí	k1gNnSc4	zázemí
Předmostí	předmostí	k1gNnSc2	předmostí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
o	o	k7c4	o
4000	[number]	k4	4000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Obytné	obytný	k2eAgFnSc2d1	obytná
budovy	budova	k1gFnSc2	budova
doplnilo	doplnit	k5eAaPmAgNnS	doplnit
nákupní	nákupní	k2eAgNnSc1d1	nákupní
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
samoobsluha	samoobsluha	k1gFnSc1	samoobsluha
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgNnSc1d1	zdravotní
středisko	středisko	k1gNnSc1	středisko
<g/>
,	,	kIx,	,
hospoda	hospoda	k?	hospoda
s	s	k7c7	s
bufetem	bufet	k1gInSc7	bufet
<g/>
,	,	kIx,	,
mateřská	mateřský	k2eAgFnSc1d1	mateřská
a	a	k8xC	a
především	především	k6eAd1	především
velká	velký	k2eAgFnSc1d1	velká
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
budova	budova	k1gFnSc1	budova
školy	škola	k1gFnSc2	škola
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
nevyhovující	vyhovující	k2eNgFnSc1d1	nevyhovující
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1985	[number]	k4	1985
se	se	k3xPyFc4	se
zřítila	zřítit	k5eAaPmAgFnS	zřítit
část	část	k1gFnSc1	část
její	její	k3xOp3gFnSc2	její
zadní	zadní	k2eAgFnSc2d1	zadní
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
ZŠ	ZŠ	kA	ZŠ
J.	J.	kA	J.
A.	A.	kA	A.
Komenského	Komenský	k1gMnSc2	Komenský
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
tělocvična	tělocvična	k1gFnSc1	tělocvična
<g/>
,	,	kIx,	,
moderně	moderně	k6eAd1	moderně
vybavené	vybavený	k2eAgFnPc4d1	vybavená
učebny	učebna	k1gFnPc4	učebna
<g/>
,	,	kIx,	,
dílny	dílna	k1gFnPc4	dílna
<g/>
,	,	kIx,	,
pěstitelské	pěstitelský	k2eAgInPc4d1	pěstitelský
pozemky	pozemek	k1gInPc4	pozemek
apod.	apod.	kA	apod.
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
školka	školka	k1gFnSc1	školka
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Výstavbou	výstavba	k1gFnSc7	výstavba
sídliště	sídliště	k1gNnSc2	sídliště
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
rovněž	rovněž	k9	rovněž
tři	tři	k4xCgFnPc1	tři
nové	nový	k2eAgFnPc1d1	nová
ulice	ulice	k1gFnPc1	ulice
-	-	kIx~	-
Michajlovská	Michajlovský	k2eAgFnSc1d1	Michajlovská
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
M.	M.	kA	M.
Horákové	Horáková	k1gFnSc2	Horáková
<g/>
,	,	kIx,	,
U	u	k7c2	u
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
Pod	pod	k7c7	pod
Skalkou	skalka	k1gFnSc7	skalka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
18	[number]	k4	18
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Vybudována	vybudován	k2eAgFnSc1d1	vybudována
byla	být	k5eAaImAgFnS	být
komunikace	komunikace	k1gFnSc1	komunikace
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
18	[number]	k4	18
(	(	kIx(	(
<g/>
obchvat	obchvat	k1gInSc1	obchvat
Předmostí	předmostí	k1gNnSc2	předmostí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
bylo	být	k5eAaImAgNnS	být
zprovozněno	zprovoznit	k5eAaPmNgNnS	zprovoznit
venkovní	venkovní	k2eAgNnSc1d1	venkovní
koupaliště	koupaliště	k1gNnSc1	koupaliště
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ale	ale	k9	ale
fungovalo	fungovat	k5eAaImAgNnS	fungovat
relativně	relativně	k6eAd1	relativně
krátce	krátce	k6eAd1	krátce
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
dobudovaný	dobudovaný	k2eAgInSc1d1	dobudovaný
tobogan	tobogan	k1gInSc1	tobogan
místo	místo	k7c2	místo
adrenalinového	adrenalinový	k2eAgInSc2d1	adrenalinový
požitku	požitek	k1gInSc2	požitek
nabízel	nabízet	k5eAaImAgInS	nabízet
pouze	pouze	k6eAd1	pouze
pomalé	pomalý	k2eAgNnSc1d1	pomalé
sklouzávání	sklouzávání	k1gNnSc1	sklouzávání
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
nízkému	nízký	k2eAgInSc3d1	nízký
tlaku	tlak	k1gInSc3	tlak
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
a	a	k8xC	a
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
sedření	sedření	k1gNnSc2	sedření
plavek	plavka	k1gFnPc2	plavka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
koupaliště	koupaliště	k1gNnSc1	koupaliště
i	i	k9	i
s	s	k7c7	s
restaurací	restaurace	k1gFnSc7	restaurace
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatkem	nedostatek	k1gInSc7	nedostatek
sídliště	sídliště	k1gNnSc2	sídliště
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
podcenění	podcenění	k1gNnSc1	podcenění
potřeby	potřeba	k1gFnSc2	potřeba
parkovacích	parkovací	k2eAgFnPc2d1	parkovací
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
kterých	který	k3yIgFnPc2	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
nedostatek	nedostatek	k1gInSc4	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametu	samet	k1gInSc6	samet
1989	[number]	k4	1989
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
kino	kino	k1gNnSc1	kino
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
TJ	tj	kA	tj
OSP	OSP	kA	OSP
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c6	na
TJ	tj	kA	tj
Pozemní	pozemní	k2eAgFnPc4d1	pozemní
stavby	stavba	k1gFnPc4	stavba
a	a	k8xC	a
JZD	JZD	kA	JZD
bylo	být	k5eAaImAgNnS	být
reorganizováno	reorganizován	k2eAgNnSc1d1	reorganizováno
a	a	k8xC	a
přiřazeno	přiřazen	k2eAgNnSc1d1	přiřazeno
k	k	k7c3	k
ZD	ZD	kA	ZD
Kokory	Kokor	k1gInPc7	Kokor
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozmachu	rozmach	k1gInSc3	rozmach
podnikatelských	podnikatelský	k2eAgFnPc2d1	podnikatelská
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
podniky	podnik	k1gInPc4	podnik
a	a	k8xC	a
obchody	obchod	k1gInPc4	obchod
ale	ale	k8xC	ale
vydržely	vydržet	k5eAaPmAgFnP	vydržet
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
událostí	událost	k1gFnSc7	událost
bylo	být	k5eAaImAgNnS	být
otevření	otevření	k1gNnSc1	otevření
servisního	servisní	k2eAgNnSc2d1	servisní
střediska	středisko	k1gNnSc2	středisko
firmy	firma	k1gFnSc2	firma
Olympus	Olympus	k1gMnSc1	Olympus
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
našlo	najít	k5eAaPmAgNnS	najít
práci	práce	k1gFnSc4	práce
120	[number]	k4	120
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1996	[number]	k4	1996
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
předmostská	předmostský	k2eAgFnSc1d1	předmostský
teplárna	teplárna	k1gFnSc1	teplárna
<g/>
,	,	kIx,	,
Předmostí	předmostí	k1gNnSc1	předmostí
bylo	být	k5eAaImAgNnS	být
napojeno	napojit	k5eAaPmNgNnS	napojit
na	na	k7c4	na
Přerov	Přerov	k1gInSc4	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
nového	nový	k2eAgNnSc2d1	nové
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
opravily	opravit	k5eAaPmAgFnP	opravit
komunikace	komunikace	k1gFnPc1	komunikace
<g/>
,	,	kIx,	,
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
pod	pod	k7c7	pod
nákupním	nákupní	k2eAgNnSc7d1	nákupní
střediskem	středisko	k1gNnSc7	středisko
bylo	být	k5eAaImAgNnS	být
zřízeno	zřízen	k2eAgNnSc1d1	zřízeno
náměstíčko	náměstíčko	k1gNnSc1	náměstíčko
s	s	k7c7	s
hodinami	hodina	k1gFnPc7	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
revitalizaci	revitalizace	k1gFnSc3	revitalizace
sídliště	sídliště	k1gNnSc2	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2005	[number]	k4	2005
úřední	úřední	k2eAgInSc4d1	úřední
odhad	odhad	k1gInSc4	odhad
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
stabilně	stabilně	k6eAd1	stabilně
vzrůstal	vzrůstat	k5eAaImAgInS	vzrůstat
až	až	k6eAd1	až
do	do	k7c2	do
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1	výrazný
pokles	pokles	k1gInSc1	pokles
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
demolicí	demolice	k1gFnSc7	demolice
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
prudký	prudký	k2eAgInSc1d1	prudký
nárůst	nárůst	k1gInSc1	nárůst
naopak	naopak	k6eAd1	naopak
s	s	k7c7	s
dokončením	dokončení	k1gNnSc7	dokončení
panelového	panelový	k2eAgNnSc2d1	panelové
sídliště	sídliště	k1gNnSc2	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
stárnutím	stárnutí	k1gNnSc7	stárnutí
populace	populace	k1gFnSc2	populace
(	(	kIx(	(
<g/>
na	na	k7c4	na
sídliště	sídliště	k1gNnSc4	sídliště
se	se	k3xPyFc4	se
nastěhovaly	nastěhovat	k5eAaPmAgFnP	nastěhovat
zejména	zejména	k9	zejména
mladé	mladý	k2eAgFnPc1d1	mladá
rodiny	rodina	k1gFnPc1	rodina
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
opět	opět	k6eAd1	opět
k	k	k7c3	k
mírnému	mírný	k2eAgInSc3d1	mírný
poklesu	pokles	k1gInSc3	pokles
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
dnes	dnes	k6eAd1	dnes
sídlí	sídlet	k5eAaImIp3nS	sídlet
Policie	policie	k1gFnSc1	policie
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pobočka	pobočka	k1gFnSc1	pobočka
pošty	pošta	k1gFnSc2	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
a	a	k8xC	a
údržba	údržba	k1gFnSc1	údržba
silnic	silnice	k1gFnPc2	silnice
tu	tu	k6eAd1	tu
má	mít	k5eAaImIp3nS	mít
středisko	středisko	k1gNnSc1	středisko
zimní	zimní	k2eAgFnSc2d1	zimní
údržby	údržba	k1gFnSc2	údržba
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotní	zdravotní	k2eAgNnSc1d1	zdravotní
středisko	středisko	k1gNnSc1	středisko
má	mít	k5eAaImIp3nS	mít
dětské	dětský	k2eAgNnSc1d1	dětské
i	i	k8xC	i
dospělé	dospělý	k2eAgNnSc1d1	dospělé
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
,	,	kIx,	,
v	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
též	též	k9	též
fungují	fungovat	k5eAaImIp3nP	fungovat
dva	dva	k4xCgMnPc1	dva
zubní	zubní	k2eAgMnPc1d1	zubní
lékaři	lékař	k1gMnPc1	lékař
a	a	k8xC	a
Sociální	sociální	k2eAgNnSc1d1	sociální
středisko	středisko	k1gNnSc1	středisko
(	(	kIx(	(
<g/>
Domov	domov	k1gInSc1	domov
důchodců	důchodce	k1gMnPc2	důchodce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
pobočka	pobočka	k1gFnSc1	pobočka
městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Školy	škola	k1gFnPc1	škola
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
dvě	dva	k4xCgFnPc1	dva
<g/>
:	:	kIx,	:
ZŠ	ZŠ	kA	ZŠ
J.	J.	kA	J.
A.	A.	kA	A.
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
zřídili	zřídit	k5eAaPmAgMnP	zřídit
žáci	žák	k1gMnPc1	žák
a	a	k8xC	a
učitelé	učitel	k1gMnPc1	učitel
malé	malý	k2eAgFnPc1d1	malá
muzeum	muzeum	k1gNnSc4	muzeum
pravěkého	pravěký	k2eAgMnSc2d1	pravěký
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
sídlilo	sídlit	k5eAaImAgNnS	sídlit
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
školní	školní	k2eAgFnSc6d1	školní
budově	budova	k1gFnSc6	budova
<g/>
,	,	kIx,	,
od	od	k7c2	od
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
protiatomovém	protiatomový	k2eAgInSc6d1	protiatomový
krytu	kryt	k1gInSc6	kryt
pod	pod	k7c7	pod
školou	škola	k1gFnSc7	škola
<g/>
.	.	kIx.	.
</s>
<s>
ZŠ	ZŠ	kA	ZŠ
a	a	k8xC	a
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
<g/>
.	.	kIx.	.
</s>
<s>
Inkluzivní	Inkluzivní	k2eAgFnSc1d1	Inkluzivní
škola	škola	k1gFnSc1	škola
rodinného	rodinný	k2eAgInSc2d1	rodinný
typu	typ	k1gInSc2	typ
s	s	k7c7	s
unikátním	unikátní	k2eAgInSc7d1	unikátní
individuálním	individuální	k2eAgInSc7d1	individuální
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
žákům	žák	k1gMnPc3	žák
s	s	k7c7	s
dvacetiletou	dvacetiletý	k2eAgFnSc7d1	dvacetiletá
tradicí	tradice	k1gFnSc7	tradice
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
nadšení	nadšení	k1gNnSc2	nadšení
učitelů	učitel	k1gMnPc2	učitel
a	a	k8xC	a
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
přerovští	přerovský	k2eAgMnPc1d1	přerovský
radní	radní	k1gMnPc1	radní
obě	dva	k4xCgFnPc4	dva
školy	škola	k1gFnPc4	škola
"	"	kIx"	"
<g/>
sloučit	sloučit	k5eAaPmF	sloučit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
provedli	provést	k5eAaPmAgMnP	provést
bez	bez	k7c2	bez
veřejné	veřejný	k2eAgFnSc2d1	veřejná
diskuze	diskuze	k1gFnSc2	diskuze
formálním	formální	k2eAgNnSc7d1	formální
zrušením	zrušení	k1gNnSc7	zrušení
ZŠ	ZŠ	kA	ZŠ
Mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
<g/>
.	.	kIx.	.
</s>
<s>
Vzbudili	vzbudit	k5eAaPmAgMnP	vzbudit
tím	ten	k3xDgNnSc7	ten
bouři	bouře	k1gFnSc4	bouře
občanské	občanský	k2eAgFnSc2d1	občanská
nevole	nevole	k1gFnSc2	nevole
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jedinou	jediný	k2eAgFnSc4d1	jediná
školu	škola	k1gFnSc4	škola
s	s	k7c7	s
alternativním	alternativní	k2eAgInSc7d1	alternativní
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
žákům	žák	k1gMnPc3	žák
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
konflikt	konflikt	k1gInSc1	konflikt
dosud	dosud	k6eAd1	dosud
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
není	být	k5eNaImIp3nS	být
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
funguje	fungovat	k5eAaImIp3nS	fungovat
několik	několik	k4yIc4	několik
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
obchodů	obchod	k1gInPc2	obchod
<g/>
,	,	kIx,	,
jmenujme	jmenovat	k5eAaBmRp1nP	jmenovat
hlavně	hlavně	k9	hlavně
dvě	dva	k4xCgFnPc4	dva
samoobsluhy	samoobsluha	k1gFnPc4	samoobsluha
(	(	kIx(	(
<g/>
doplněné	doplněný	k2eAgFnPc4d1	doplněná
několika	několik	k4yIc7	několik
menšími	malý	k2eAgInPc7d2	menší
obchody	obchod	k1gInPc7	obchod
s	s	k7c7	s
potravinami	potravina	k1gFnPc7	potravina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nechybí	chybět	k5eNaImIp3nS	chybět
ani	ani	k9	ani
několik	několik	k4yIc4	několik
hospod	hospod	k?	hospod
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
tradici	tradice	k1gFnSc4	tradice
má	mít	k5eAaImIp3nS	mít
Hospoda	Hospoda	k?	Hospoda
u	u	k7c2	u
Mamuta	mamut	k1gMnSc2	mamut
<g/>
,	,	kIx,	,
zajímavým	zajímavý	k2eAgNnSc7d1	zajímavé
je	on	k3xPp3gFnPc4	on
hostinec	hostinec	k1gInSc1	hostinec
BoRa	BoR	k1gInSc2	BoR
<g/>
,	,	kIx,	,
otevřený	otevřený	k2eAgInSc1d1	otevřený
v	v	k7c6	v
renovované	renovovaný	k2eAgFnSc6d1	renovovaná
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
hasičské	hasičský	k2eAgFnSc2d1	hasičská
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
není	být	k5eNaImIp3nS	být
nabídka	nabídka	k1gFnSc1	nabídka
služeb	služba	k1gFnPc2	služba
příliš	příliš	k6eAd1	příliš
bohatá	bohatý	k2eAgFnSc1d1	bohatá
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
blízkosti	blízkost	k1gFnSc3	blízkost
Přerova	Přerov	k1gInSc2	Přerov
a	a	k8xC	a
snadným	snadný	k2eAgNnSc7d1	snadné
dopravním	dopravní	k2eAgNnSc7d1	dopravní
spojením	spojení	k1gNnSc7	spojení
(	(	kIx(	(
<g/>
linka	linka	k1gFnSc1	linka
2	[number]	k4	2
v	v	k7c4	v
pracovní	pracovní	k2eAgInPc4d1	pracovní
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
linka	linka	k1gFnSc1	linka
6	[number]	k4	6
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
a	a	k8xC	a
linka	linka	k1gFnSc1	linka
1	[number]	k4	1
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
týden	týden	k1gInSc4	týden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
doprava	doprava	k6eAd1	doprava
příměstskými	příměstský	k2eAgInPc7d1	příměstský
autobusy	autobus	k1gInPc7	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgFnSc2d1	Staré
architektury	architektura	k1gFnSc2	architektura
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
poskrovnu	poskrovnu	k6eAd1	poskrovnu
<g/>
,	,	kIx,	,
starou	starý	k2eAgFnSc4d1	stará
zástavbu	zástavba	k1gFnSc4	zástavba
pohltilo	pohltit	k5eAaPmAgNnS	pohltit
sídliště	sídliště	k1gNnSc1	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
budov	budova	k1gFnPc2	budova
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
prakticky	prakticky	k6eAd1	prakticky
tři	tři	k4xCgInPc4	tři
<g/>
:	:	kIx,	:
rokokový	rokokový	k2eAgInSc1d1	rokokový
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Máří	Máří	k?	Máří
Magdalény	Magdaléna	k1gFnSc2	Magdaléna
fara	fara	k1gFnSc1	fara
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
budova	budova	k1gFnSc1	budova
staré	starý	k2eAgFnSc2d1	stará
školy	škola	k1gFnSc2	škola
starobylý	starobylý	k2eAgInSc1d1	starobylý
hřbitov	hřbitov	k1gInSc1	hřbitov
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Další	další	k2eAgFnSc2d1	další
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
:	:	kIx,	:
socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1728	[number]	k4	1728
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
bronzový	bronzový	k2eAgInSc4d1	bronzový
památník	památník	k1gInSc4	památník
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
za	za	k7c2	za
<g />
.	.	kIx.	.
</s>
<s>
kostelem	kostel	k1gInSc7	kostel
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
J.	J.	kA	J.
Pelikána	Pelikán	k1gMnSc2	Pelikán
sonda	sonda	k1gFnSc1	sonda
do	do	k7c2	do
pravěku	pravěk	k1gInSc2	pravěk
<g/>
,	,	kIx,	,
památník	památník	k1gInSc1	památník
se	s	k7c7	s
stylizovanou	stylizovaný	k2eAgFnSc7d1	stylizovaná
geometrickou	geometrický	k2eAgFnSc7d1	geometrická
venuší	venuše	k1gFnSc7	venuše
a	a	k8xC	a
reliéfními	reliéfní	k2eAgFnPc7d1	reliéfní
plastikami	plastika	k1gFnPc7	plastika
mamutích	mamutí	k2eAgFnPc2d1	mamutí
kostí	kost	k1gFnPc2	kost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
pamětní	pamětní	k2eAgFnSc2d1	pamětní
desky	deska	k1gFnSc2	deska
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
staré	starý	k2eAgFnSc2d1	stará
školy	škola	k1gFnSc2	škola
upomínají	upomínat	k5eAaImIp3nP	upomínat
na	na	k7c6	na
oběti	oběť	k1gFnSc6	oběť
první	první	k4xOgFnSc2	první
i	i	k8xC	i
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
na	na	k7c6	na
nalezišti	naleziště	k1gNnSc6	naleziště
<g/>
,	,	kIx,	,
odhalená	odhalený	k2eAgFnSc1d1	odhalená
1971	[number]	k4	1971
bludný	bludný	k2eAgInSc4d1	bludný
balvan	balvan	k1gInSc4	balvan
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
<g />
.	.	kIx.	.
</s>
<s>
MŠ	MŠ	kA	MŠ
<g/>
,	,	kIx,	,
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Chromečkovy	Chromečkův	k2eAgFnSc2d1	Chromečkův
zahrady	zahrada	k1gFnSc2	zahrada
s	s	k7c7	s
pamětní	pamětní	k2eAgFnSc7d1	pamětní
deskou	deska	k1gFnSc7	deska
model	model	k1gInSc1	model
mamuta	mamut	k1gMnSc2	mamut
na	na	k7c6	na
Čekyňském	Čekyňský	k2eAgInSc6d1	Čekyňský
kopci	kopec	k1gInSc6	kopec
v	v	k7c6	v
podživotní	podživotní	k2eAgFnSc6d1	podživotní
velikosti	velikost	k1gFnSc6	velikost
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2006	[number]	k4	2006
byly	být	k5eAaImAgFnP	být
zahájeny	zahájit	k5eAaPmNgFnP	zahájit
práce	práce	k1gFnPc1	práce
na	na	k7c6	na
naučné	naučný	k2eAgFnSc6d1	naučná
stezce	stezka	k1gFnSc6	stezka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
také	také	k9	také
památník	památník	k1gInSc1	památník
lovců	lovec	k1gMnPc2	lovec
mamutů	mamut	k1gMnPc2	mamut
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
konzervovány	konzervován	k2eAgInPc1d1	konzervován
nálezy	nález	k1gInPc1	nález
kostí	kost	k1gFnPc2	kost
pravěkých	pravěký	k2eAgNnPc2d1	pravěké
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
autentické	autentický	k2eAgFnSc6d1	autentická
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Stezka	stezka	k1gFnSc1	stezka
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovoznit	k5eAaPmNgFnS	zprovoznit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nP	měřit
něco	něco	k3yInSc4	něco
přes	přes	k7c4	přes
8	[number]	k4	8
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Knejzlík	Knejzlík	k1gMnSc1	Knejzlík
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1878	[number]	k4	1878
Třebíč	Třebíč	k1gFnSc1	Třebíč
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1952	[number]	k4	1952
Předmostí	předmostí	k1gNnPc2	předmostí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
parlamentu	parlament	k1gInSc2	parlament
ČSR	ČSR	kA	ČSR
Fráňa	Fráňa	k1gMnSc1	Fráňa
Kopeček	Kopeček	k1gMnSc1	Kopeček
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1881	[number]	k4	1881
Předmostí	předmostí	k1gNnSc6	předmostí
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1946	[number]	k4	1946
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgMnSc1d1	kulturní
pracovník	pracovník	k1gMnSc1	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Vrba	Vrba	k1gMnSc1	Vrba
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1911	[number]	k4	1911
Předmostí	předmostí	k1gNnSc6	předmostí
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1971	[number]	k4	1971
Přerov	Přerov	k1gInSc1	Přerov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
primář	primář	k1gMnSc1	primář
gynekologicko-porodnického	gynekologickoorodnický	k2eAgNnSc2d1	gynekologicko-porodnické
oddělení	oddělení	k1gNnSc2	oddělení
přerovské	přerovský	k2eAgFnSc2d1	přerovská
nemocnice	nemocnice	k1gFnSc2	nemocnice
Vlatimil	Vlatimil	k1gMnSc1	Vlatimil
Knejzlík	Knejzlík	k1gMnSc1	Knejzlík
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1910	[number]	k4	1910
Předmostí	předmostí	k1gNnSc6	předmostí
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1997	[number]	k4	1997
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
J.	J.	kA	J.
Knejzlíka	Knejzlík	k1gMnSc4	Knejzlík
<g/>
,	,	kIx,	,
redaktor	redaktor	k1gMnSc1	redaktor
a	a	k8xC	a
vydavatel	vydavatel	k1gMnSc1	vydavatel
časopisu	časopis	k1gInSc2	časopis
Rádce	rádce	k1gMnSc1	rádce
z	z	k7c2	z
Předmostí	předmostí	k1gNnSc2	předmostí
Historický	historický	k2eAgInSc4d1	historický
místopis	místopis	k1gInSc4	místopis
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1848	[number]	k4	1848
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
6	[number]	k4	6
<g/>
,	,	kIx,	,
Okresy	okres	k1gInPc1	okres
<g/>
:	:	kIx,	:
Přerov	Přerov	k1gInSc1	Přerov
<g/>
,	,	kIx,	,
Hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Drechsler	Drechsler	k1gMnSc1	Drechsler
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
Fišmistrová	Fišmistrová	k1gFnSc1	Fišmistrová
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
a	a	k8xC	a
Lapáček	Lapáček	k1gMnSc1	Lapáček
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
města	město	k1gNnSc2	město
Přerova	Přerov	k1gInSc2	Přerov
v	v	k7c6	v
datech	datum	k1gNnPc6	datum
<g/>
.	.	kIx.	.
</s>
<s>
Přerov	Přerov	k1gInSc1	Přerov
<g/>
:	:	kIx,	:
Město	město	k1gNnSc1	město
Přerov	Přerov	k1gInSc1	Přerov
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
191	[number]	k4	191
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
7202	[number]	k4	7202
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kreutz	Kreutz	k1gMnSc1	Kreutz
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Vlastivěda	vlastivěda	k1gFnSc1	vlastivěda
moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
:	:	kIx,	:
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Místopis	místopis	k1gInSc1	místopis
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
53	[number]	k4	53
<g/>
,	,	kIx,	,
Přerovský	přerovský	k2eAgInSc1d1	přerovský
okres	okres	k1gInSc1	okres
<g/>
.	.	kIx.	.
</s>
<s>
Jičínský	jičínský	k2eAgInSc1d1	jičínský
kraj	kraj	k1gInSc1	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
446	[number]	k4	446
s.	s.	k?	s.
<g/>
,	,	kIx,	,
1	[number]	k4	1
mp	mp	k?	mp
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Přerov	Přerov	k1gInSc1	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Přerov-Předmostí	Přerov-Předmost	k1gFnSc7	Přerov-Předmost
Stránka	stránka	k1gFnSc1	stránka
o	o	k7c6	o
Předmostí	předmostí	k1gNnSc6	předmostí
Stránky	stránka	k1gFnSc2	stránka
místní	místní	k2eAgFnSc2d1	místní
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
</s>
