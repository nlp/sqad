<s>
Akademii	akademie	k1gFnSc4	akademie
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ředitel	ředitel	k1gMnSc1	ředitel
studia	studio	k1gNnSc2	studio
MGM	MGM	kA	MGM
Louis	Louis	k1gMnSc1	Louis
B.	B.	kA	B.
Mayer	Mayer	k1gMnSc1	Mayer
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
pětatřiceti	pětatřicet	k4xCc3	pětatřicet
osobnostmi	osobnost	k1gFnPc7	osobnost
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
