<s>
Holki	Holki	k6eAd1	Holki
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
konkurzem	konkurz	k1gInSc7	konkurz
uspořádaným	uspořádaný	k2eAgMnSc7d1	uspořádaný
Petrem	Petr	k1gMnSc7	Petr
Fiderem	Fider	k1gMnSc7	Fider
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
Holki	Holki	k1gNnSc2	Holki
ukončily	ukončit	k5eAaPmAgInP	ukončit
smlouvu	smlouva	k1gFnSc4	smlouva
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
EMI	EMI	kA	EMI
<g/>
,	,	kIx,	,
Nikola	Nikola	k1gFnSc1	Nikola
Šobichová	Šobichová	k1gFnSc1	Šobichová
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
nepokračovat	pokračovat	k5eNaImF	pokračovat
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
skupina	skupina	k1gFnSc1	skupina
jen	jen	k6eAd1	jen
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
členkách	členka	k1gFnPc6	členka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
však	však	k9	však
skupina	skupina	k1gFnSc1	skupina
vrací	vracet	k5eAaImIp3nS	vracet
na	na	k7c4	na
hudební	hudební	k2eAgFnSc4d1	hudební
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jezdí	jezdit	k5eAaImIp3nP	jezdit
po	po	k7c6	po
městech	město	k1gNnPc6	město
dělat	dělat	k5eAaImF	dělat
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Vystupují	vystupovat	k5eAaImIp3nP	vystupovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
Kláry	Klára	k1gFnSc2	Klára
Kolomazníkové	Kolomazníková	k1gFnSc2	Kolomazníková
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
už	už	k6eAd1	už
k	k	k7c3	k
projektu	projekt	k1gInSc3	projekt
Holki	Holk	k1gFnSc2	Holk
nechce	chtít	k5eNaImIp3nS	chtít
vracet	vracet	k5eAaImF	vracet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ji	on	k3xPp3gFnSc4	on
dostatečně	dostatečně	k6eAd1	dostatečně
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
její	její	k3xOp3gFnSc1	její
vlastní	vlastní	k2eAgFnSc1d1	vlastní
kariéra	kariéra	k1gFnSc1	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Radana	Radana	k1gFnSc1	Radana
Labajová	Labajový	k2eAgFnSc1d1	Labajová
Kateřina	Kateřina	k1gFnSc1	Kateřina
Brzobohatá	Brzobohatá	k1gFnSc1	Brzobohatá
Nikola	Nikola	k1gFnSc1	Nikola
Šobichová	Šobichová	k1gFnSc1	Šobichová
Klára	Klára	k1gFnSc1	Klára
Kolomazníková	Kolomazníková	k1gFnSc1	Kolomazníková
(	(	kIx(	(
<g/>
do	do	k7c2	do
r.	r.	kA	r.
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
S	s	k7c7	s
láskou	láska	k1gFnSc7	láska
–	–	k?	–
1999	[number]	k4	1999
Pro	pro	k7c4	pro
tebe	ty	k3xPp2nSc2	ty
–	–	k?	–
2000	[number]	k4	2000
Spolu	spolu	k6eAd1	spolu
–	–	k?	–
2001	[number]	k4	2001
Pro	pro	k7c4	pro
tebe	ty	k3xPp2nSc4	ty
/	/	kIx~	/
Dvojplatinové	dvojplatinový	k2eAgNnSc4d1	dvojplatinový
album	album	k1gNnSc4	album
–	–	k?	–
2001	[number]	k4	2001
Vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
–	–	k?	–
2002	[number]	k4	2002
Ať	ať	k8xS	ať
to	ten	k3xDgNnSc1	ten
neskončí	skončit	k5eNaPmIp3nS	skončit
–	–	k?	–
2003	[number]	k4	2003
Pohádkový	pohádkový	k2eAgInSc4d1	pohádkový
příběh	příběh	k1gInSc4	příběh
–	–	k?	–
2004	[number]	k4	2004
Zkus	zkusit	k5eAaPmRp2nS	zkusit
zapomenout	zapomenout	k5eAaPmF	zapomenout
–	–	k?	–
2009	[number]	k4	2009
</s>
