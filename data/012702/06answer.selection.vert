<s>
Zebra	zebra	k1gFnSc1	zebra
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
podrodů	podrod	k1gInPc2	podrod
kopytníků	kopytník	k1gInPc2	kopytník
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Equus	Equus	k1gInSc1	Equus
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
srst	srst	k1gFnSc4	srst
je	být	k5eAaImIp3nS	být
charakteristicky	charakteristicky	k6eAd1	charakteristicky
bílo-černě	bílo-černě	k6eAd1	bílo-černě
pruhovaná	pruhovaný	k2eAgFnSc1d1	pruhovaná
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zebrovaná	zebrovaný	k2eAgFnSc1d1	zebrovaná
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
