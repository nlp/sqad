<p>
<s>
Zebra	zebra	k1gFnSc1	zebra
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
podrodů	podrod	k1gInPc2	podrod
kopytníků	kopytník	k1gInPc2	kopytník
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Equus	Equus	k1gInSc1	Equus
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
srst	srst	k1gFnSc4	srst
je	být	k5eAaImIp3nS	být
charakteristicky	charakteristicky	k6eAd1	charakteristicky
bílo-černě	bílo-černě	k6eAd1	bílo-černě
pruhovaná	pruhovaný	k2eAgFnSc1d1	pruhovaná
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zebrovaná	zebrovaný	k2eAgFnSc1d1	zebrovaná
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
zástupci	zástupce	k1gMnPc1	zástupce
skupiny	skupina	k1gFnSc2	skupina
koňovitých	koňovitý	k2eAgMnPc2d1	koňovitý
lichokopytníků	lichokopytník	k1gMnPc2	lichokopytník
žijí	žít	k5eAaImIp3nP	žít
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
rodinných	rodinný	k2eAgFnPc6d1	rodinná
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
jedním	jeden	k4xCgMnSc7	jeden
hřebcem	hřebec	k1gMnSc7	hřebec
<g/>
,	,	kIx,	,
doprovázeným	doprovázený	k2eAgMnSc7d1	doprovázený
klisnami	klisna	k1gFnPc7	klisna
a	a	k8xC	a
mláďaty	mládě	k1gNnPc7	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Zebra	zebra	k1gFnSc1	zebra
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
30	[number]	k4	30
až	až	k9	až
40	[number]	k4	40
let	léto	k1gNnPc2	léto
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
==	==	k?	==
</s>
</p>
<p>
<s>
Stavbou	stavba	k1gFnSc7	stavba
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
zebry	zebra	k1gFnSc2	zebra
podobají	podobat	k5eAaImIp3nP	podobat
primitivním	primitivní	k2eAgInPc3d1	primitivní
druhům	druh	k1gInPc3	druh
oslů	osel	k1gMnPc2	osel
a	a	k8xC	a
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
kratší	krátký	k2eAgFnPc1d2	kratší
nohy	noha	k1gFnPc1	noha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
větší	veliký	k2eAgFnSc4d2	veliký
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
dobrými	dobrý	k2eAgMnPc7d1	dobrý
sprintery	sprinter	k1gMnPc7	sprinter
jako	jako	k8xS	jako
ušlechtilá	ušlechtilý	k2eAgNnPc1d1	ušlechtilé
plemena	plemeno	k1gNnPc1	plemeno
domácích	domácí	k2eAgMnPc2d1	domácí
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
běžet	běžet	k5eAaImF	běžet
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
65	[number]	k4	65
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vytrvalejšími	vytrvalý	k2eAgMnPc7d2	vytrvalejší
běžci	běžec	k1gMnPc7	běžec
než	než	k8xS	než
kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Pruhy	pruh	k1gInPc1	pruh
nemusejí	muset	k5eNaImIp3nP	muset
znamenat	znamenat	k5eAaImF	znamenat
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c4	před
predátory	predátor	k1gMnPc4	predátor
–	–	k?	–
ti	ten	k3xDgMnPc1	ten
totiž	totiž	k9	totiž
spatří	spatřit	k5eAaPmIp3nP	spatřit
obrys	obrys	k1gInSc4	obrys
zebry	zebra	k1gFnSc2	zebra
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
pruhování	pruhování	k1gNnPc2	pruhování
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
termoregulační	termoregulační	k2eAgFnSc4d1	termoregulační
funkci	funkce	k1gFnSc4	funkce
(	(	kIx(	(
<g/>
vzduch	vzduch	k1gInSc4	vzduch
nad	nad	k7c7	nad
černými	černý	k2eAgInPc7d1	černý
pruhy	pruh	k1gInPc7	pruh
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
proudění	proudění	k1gNnSc1	proudění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k8xS	jako
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
bodavému	bodavý	k2eAgInSc3d1	bodavý
hmyzu	hmyz	k1gInSc3	hmyz
(	(	kIx(	(
<g/>
světlo	světlo	k1gNnSc1	světlo
odražené	odražený	k2eAgNnSc1d1	odražené
od	od	k7c2	od
pruhované	pruhovaný	k2eAgFnSc2d1	pruhovaná
srsti	srst	k1gFnSc2	srst
hmyz	hmyz	k1gInSc1	hmyz
odrazuje	odrazovat	k5eAaImIp3nS	odrazovat
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
polarizaci	polarizace	k1gFnSc3	polarizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
zeber	zebra	k1gFnPc2	zebra
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
zebrami	zebra	k1gFnPc7	zebra
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
individuální	individuální	k2eAgInPc1d1	individuální
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
poddruhů	poddruh	k1gInPc2	poddruh
a	a	k8xC	a
místních	místní	k2eAgFnPc2d1	místní
variant	varianta	k1gFnPc2	varianta
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
i	i	k9	i
počet	počet	k1gInSc1	počet
popsaných	popsaný	k2eAgInPc2d1	popsaný
druhů	druh	k1gInPc2	druh
prošel	projít	k5eAaPmAgMnS	projít
revizí	revize	k1gFnSc7	revize
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
popisovaly	popisovat	k5eAaImAgInP	popisovat
čtyři	čtyři	k4xCgInPc1	čtyři
druhy	druh	k1gInPc1	druh
<g/>
:	:	kIx,	:
zebra	zebra	k1gFnSc1	zebra
stepní	stepní	k2eAgFnSc1d1	stepní
(	(	kIx(	(
<g/>
E.	E.	kA	E.
burchellii	burchellie	k1gFnSc6	burchellie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zebra	zebra	k1gFnSc1	zebra
horská	horský	k2eAgFnSc1d1	horská
(	(	kIx(	(
<g/>
E.	E.	kA	E.
zebra	zebra	k1gFnSc1	zebra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zebra	zebra	k1gFnSc1	zebra
Grévyho	Grévy	k1gMnSc2	Grévy
(	(	kIx(	(
<g/>
E.	E.	kA	E.
grevyi	grevy	k1gFnSc2	grevy
<g/>
)	)	kIx)	)
a	a	k8xC	a
zebra	zebra	k1gFnSc1	zebra
kvaga	kvaga	k1gFnSc1	kvaga
(	(	kIx(	(
<g/>
E.	E.	kA	E.
quagga	quagga	k1gFnSc1	quagga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zebra	zebra	k1gFnSc1	zebra
kvaga	kvaga	k1gFnSc1	kvaga
byla	být	k5eAaImAgFnS	být
vyhubena	vyhuben	k2eAgFnSc1d1	vyhubena
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
uhynul	uhynout	k5eAaPmAgMnS	uhynout
poslední	poslední	k2eAgMnSc1d1	poslední
jedinec	jedinec	k1gMnSc1	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
ale	ale	k9	ale
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zebra	zebra	k1gFnSc1	zebra
kvaga	kvaga	k1gFnSc1	kvaga
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
poddruhem	poddruh	k1gInSc7	poddruh
zebry	zebra	k1gFnSc2	zebra
stepní	stepní	k2eAgFnSc2d1	stepní
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
zebra	zebra	k1gFnSc1	zebra
kvaga	kvaga	k1gFnSc1	kvaga
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
pravidla	pravidlo	k1gNnSc2	pravidlo
priority	priorita	k1gFnSc2	priorita
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
zebra	zebra	k1gFnSc1	zebra
stepní	stepní	k2eAgFnSc1d1	stepní
správně	správně	k6eAd1	správně
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Equus	Equus	k1gInSc4	Equus
quagga	quagga	k1gFnSc1	quagga
<g/>
,	,	kIx,	,
vyhynulá	vyhynulý	k2eAgFnSc1d1	vyhynulá
zebra	zebra	k1gFnSc1	zebra
kvaga	kvaga	k1gFnSc1	kvaga
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
poddruhem	poddruh	k1gInSc7	poddruh
E.	E.	kA	E.
quagga	quagg	k1gMnSc4	quagg
quagga	quagg	k1gMnSc4	quagg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zebra	zebra	k1gFnSc1	zebra
Chapmanova	Chapmanův	k2eAgFnSc1d1	Chapmanova
má	mít	k5eAaImIp3nS	mít
mezi	mezi	k7c7	mezi
širokými	široký	k2eAgInPc7d1	široký
černými	černý	k2eAgInPc7d1	černý
pruhy	pruh	k1gInPc7	pruh
vyznačeny	vyznačen	k2eAgInPc4d1	vyznačen
ještě	ještě	k9	ještě
pruhy	pruh	k1gInPc4	pruh
tmavohnědé	tmavohnědý	k2eAgInPc4d1	tmavohnědý
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
čtyř	čtyři	k4xCgInPc2	čtyři
poddruhů	poddruh	k1gInPc2	poddruh
zeber	zebra	k1gFnPc2	zebra
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Žijí	žít	k5eAaImIp3nP	žít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
savanách	savana	k1gFnPc6	savana
nebo	nebo	k8xC	nebo
v	v	k7c6	v
horských	horský	k2eAgInPc6d1	horský
biotopech	biotop	k1gInPc6	biotop
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
různými	různý	k2eAgFnPc7d1	různá
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Pasou	pást	k5eAaImIp3nP	pást
se	se	k3xPyFc4	se
na	na	k7c6	na
travnatých	travnatý	k2eAgInPc6d1	travnatý
území	území	k1gNnSc6	území
blízko	blízko	k7c2	blízko
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
páření	páření	k1gNnSc6	páření
nastává	nastávat	k5eAaImIp3nS	nastávat
březost	březost	k1gFnSc1	březost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
jedno	jeden	k4xCgNnSc1	jeden
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nP	narodit
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
nejsou	být	k5eNaImIp3nP	být
životaschopná	životaschopný	k2eAgNnPc1d1	životaschopné
obě	dva	k4xCgFnPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
narození	narození	k1gNnSc6	narození
mládě	mládě	k1gNnSc1	mládě
měří	měřit	k5eAaImIp3nS	měřit
kolem	kolem	k7c2	kolem
84	[number]	k4	84
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
průměrně	průměrně	k6eAd1	průměrně
35	[number]	k4	35
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgNnPc4d1	schopno
páření	páření	k1gNnPc4	páření
téměř	téměř	k6eAd1	téměř
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
jenom	jenom	k9	jenom
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
mívá	mívat	k5eAaImIp3nS	mívat
samice	samice	k1gFnSc1	samice
mládě	mládě	k1gNnSc1	mládě
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jezdectví	jezdectví	k1gNnSc2	jezdectví
==	==	k?	==
</s>
</p>
<p>
<s>
Podobnost	podobnost	k1gFnSc1	podobnost
zebry	zebra	k1gFnSc2	zebra
s	s	k7c7	s
koněm	kůň	k1gMnSc7	kůň
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
automaticky	automaticky	k6eAd1	automaticky
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
<g/>
-li	i	k?	-li
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
jezdit	jezdit	k5eAaImF	jezdit
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
ano	ano	k9	ano
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
obtížnější	obtížný	k2eAgNnSc1d2	obtížnější
ji	on	k3xPp3gFnSc4	on
ochočit	ochočit	k5eAaPmF	ochočit
a	a	k8xC	a
vycvičit	vycvičit	k5eAaPmF	vycvičit
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Zebra	zebra	k1gFnSc1	zebra
Burchellova	Burchellův	k2eAgFnSc1d1	Burchellova
</s>
</p>
<p>
<s>
Zebra	zebra	k1gFnSc1	zebra
Grévyho	Grévy	k1gMnSc2	Grévy
</s>
</p>
<p>
<s>
Zebra	zebra	k1gFnSc1	zebra
horská	horský	k2eAgFnSc1d1	horská
</s>
</p>
<p>
<s>
Zebra	zebra	k1gFnSc1	zebra
kvaga	kvaga	k1gFnSc1	kvaga
</s>
</p>
<p>
<s>
Zebra	zebra	k1gFnSc1	zebra
stepní	stepní	k2eAgFnSc2d1	stepní
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zebra	zebra	k1gFnSc1	zebra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
zebra	zebra	k1gFnSc1	zebra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zebra	zebra	k1gFnSc1	zebra
jako	jako	k8xS	jako
dopravní	dopravní	k2eAgNnSc1d1	dopravní
zvíře	zvíře	k1gNnSc1	zvíře
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
Sbírka	sbírka	k1gFnSc1	sbírka
odkazů	odkaz	k1gInPc2	odkaz
o	o	k7c6	o
zebře	zebra	k1gFnSc6	zebra
</s>
</p>
