<p>
<s>
Mikrofon	mikrofon	k1gInSc1	mikrofon
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnPc4	zařízení
pro	pro	k7c4	pro
přeměnu	přeměna	k1gFnSc4	přeměna
akustického	akustický	k2eAgInSc2d1	akustický
(	(	kIx(	(
<g/>
zvukového	zvukový	k2eAgInSc2d1	zvukový
<g/>
)	)	kIx)	)
signálu	signál	k1gInSc2	signál
na	na	k7c4	na
signál	signál	k1gInSc4	signál
elektrický	elektrický	k2eAgInSc4d1	elektrický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
mikrofon	mikrofon	k1gInSc4	mikrofon
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
tvůrce	tvůrce	k1gMnSc1	tvůrce
gramofonu	gramofon	k1gInSc2	gramofon
Emile	Emil	k1gMnSc5	Emil
Berliner	Berlinero	k1gNnPc2	Berlinero
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Principy	princip	k1gInPc1	princip
mikrofonů	mikrofon	k1gInPc2	mikrofon
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kondenzátorový	kondenzátorový	k2eAgInSc4d1	kondenzátorový
mikrofon	mikrofon	k1gInSc4	mikrofon
===	===	k?	===
</s>
</p>
<p>
<s>
Kondenzátorový	kondenzátorový	k2eAgInSc1d1	kondenzátorový
mikrofon	mikrofon	k1gInSc1	mikrofon
pracuje	pracovat	k5eAaImIp3nS	pracovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
akustické	akustický	k2eAgInPc1d1	akustický
kmity	kmit	k1gInPc1	kmit
rozechvívají	rozechvívat	k5eAaImIp3nP	rozechvívat
membránu	membrána	k1gFnSc4	membrána
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
elektrod	elektroda	k1gFnPc2	elektroda
kondenzátoru	kondenzátor	k1gInSc2	kondenzátor
<g/>
,	,	kIx,	,
připojeného	připojený	k2eAgInSc2d1	připojený
do	do	k7c2	do
elektrického	elektrický	k2eAgInSc2d1	elektrický
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rytmu	rytmus	k1gInSc6	rytmus
změny	změna	k1gFnSc2	změna
polohy	poloha	k1gFnSc2	poloha
membrány	membrána	k1gFnSc2	membrána
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
kapacita	kapacita	k1gFnSc1	kapacita
kondenzátoru	kondenzátor	k1gInSc2	kondenzátor
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
převádí	převádět	k5eAaImIp3nS	převádět
na	na	k7c4	na
elektrický	elektrický	k2eAgInSc4d1	elektrický
signál	signál	k1gInSc4	signál
buď	buď	k8xC	buď
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vlastní	vlastní	k2eAgFnSc1d1	vlastní
mikrofonní	mikrofonní	k2eAgFnSc1d1	mikrofonní
vložka	vložka	k1gFnSc1	vložka
napájena	napájen	k2eAgFnSc1d1	napájena
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
měkkého	měkký	k2eAgInSc2d1	měkký
zdroje	zdroj	k1gInSc2	zdroj
polarizačního	polarizační	k2eAgNnSc2d1	polarizační
napětí	napětí	k1gNnSc2	napětí
a	a	k8xC	a
napětí	napětí	k1gNnSc2	napětí
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
snímáno	snímat	k5eAaImNgNnS	snímat
předzesilovačem	předzesilovač	k1gInSc7	předzesilovač
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
vstupní	vstupní	k2eAgFnSc7d1	vstupní
impedancí	impedance	k1gFnSc7	impedance
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
kapacita	kapacita	k1gFnSc1	kapacita
vložky	vložka	k1gFnSc2	vložka
použita	použít	k5eAaPmNgFnS	použít
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
vysokofrekvenčního	vysokofrekvenční	k2eAgInSc2d1	vysokofrekvenční
oscilátoru	oscilátor	k1gInSc2	oscilátor
<g/>
,	,	kIx,	,
rozlaďovaného	rozlaďovaný	k2eAgInSc2d1	rozlaďovaný
změnou	změna	k1gFnSc7	změna
kapacity	kapacita	k1gFnPc1	kapacita
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
obvodech	obvod	k1gInPc6	obvod
je	být	k5eAaImIp3nS	být
demodulován	demodulován	k2eAgInSc4d1	demodulován
nízkofrekvenční	nízkofrekvenční	k2eAgInSc4d1	nízkofrekvenční
signál	signál	k1gInSc4	signál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kondenzátorové	kondenzátorový	k2eAgInPc1d1	kondenzátorový
mikrofony	mikrofon	k1gInPc1	mikrofon
obou	dva	k4xCgNnPc2	dva
provedení	provedení	k1gNnPc2	provedení
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
napájení	napájený	k2eAgMnPc1d1	napájený
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vhodné	vhodný	k2eAgFnSc6d1	vhodná
konstrukci	konstrukce	k1gFnSc6	konstrukce
mikrofonní	mikrofonní	k2eAgFnSc2d1	mikrofonní
vložky	vložka	k1gFnSc2	vložka
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
polarizačním	polarizační	k2eAgNnSc7d1	polarizační
napětím	napětí	k1gNnSc7	napětí
měnit	měnit	k5eAaImF	měnit
směrové	směrový	k2eAgFnPc4d1	směrová
charakteristiky	charakteristika	k1gFnPc4	charakteristika
mikrofonu	mikrofon	k1gInSc2	mikrofon
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
některé	některý	k3yIgInPc4	některý
studiové	studiový	k2eAgInPc4d1	studiový
mikrofony	mikrofon	k1gInPc4	mikrofon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kondenzátorové	kondenzátorový	k2eAgInPc1d1	kondenzátorový
mikrofony	mikrofon	k1gInPc1	mikrofon
jsou	být	k5eAaImIp3nP	být
pokládány	pokládat	k5eAaImNgInP	pokládat
za	za	k7c4	za
nejkvalitnější	kvalitní	k2eAgInSc4d3	nejkvalitnější
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
pro	pro	k7c4	pro
profesionální	profesionální	k2eAgInSc4d1	profesionální
záznam	záznam	k1gInSc4	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
pro	pro	k7c4	pro
měřící	měřící	k2eAgInPc4d1	měřící
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Elektretový	Elektretový	k2eAgInSc4d1	Elektretový
mikrofon	mikrofon	k1gInSc4	mikrofon
===	===	k?	===
</s>
</p>
<p>
<s>
Elektretový	Elektretový	k2eAgInSc1d1	Elektretový
mikrofon	mikrofon	k1gInSc1	mikrofon
je	být	k5eAaImIp3nS	být
typem	typ	k1gInSc7	typ
kondenzátorového	kondenzátorový	k2eAgInSc2d1	kondenzátorový
mikrofonu	mikrofon	k1gInSc2	mikrofon
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgNnSc2	jenž
je	být	k5eAaImIp3nS	být
elektrické	elektrický	k2eAgNnSc1d1	elektrické
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
pro	pro	k7c4	pro
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
vytvářeno	vytvářit	k5eAaPmNgNnS	vytvářit
elektretem	elektret	k1gInSc7	elektret
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nevodivou	vodivý	k2eNgFnSc7d1	nevodivá
hmotou	hmota	k1gFnSc7	hmota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
permanentně	permanentně	k6eAd1	permanentně
elektricky	elektricky	k6eAd1	elektricky
nabitá	nabitý	k2eAgFnSc1d1	nabitá
(	(	kIx(	(
<g/>
srovnej	srovnat	k5eAaPmRp2nS	srovnat
s	s	k7c7	s
"	"	kIx"	"
<g/>
permanentním	permanentní	k2eAgInSc7d1	permanentní
magnetem	magnet	k1gInSc7	magnet
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rytmu	rytmus	k1gInSc6	rytmus
pohybu	pohyb	k1gInSc2	pohyb
membrány	membrána	k1gFnSc2	membrána
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
mění	měnit	k5eAaImIp3nS	měnit
kapacita	kapacita	k1gFnSc1	kapacita
kondenzátoru	kondenzátor	k1gInSc2	kondenzátor
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
deskami	deska	k1gFnPc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
napětí	napětí	k1gNnSc2	napětí
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
zpracovávány	zpracovávat	k5eAaImNgInP	zpracovávat
předzesilovačem	předzesilovač	k1gInSc7	předzesilovač
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
vstupní	vstupní	k2eAgFnSc7d1	vstupní
impedancí	impedance	k1gFnSc7	impedance
(	(	kIx(	(
<g/>
zpr	zpr	k?	zpr
<g/>
.	.	kIx.	.
s	s	k7c7	s
tranzistory	tranzistor	k1gInPc7	tranzistor
FET	FET	kA	FET
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
součástí	součást	k1gFnSc7	součást
mikrofonu	mikrofon	k1gInSc2	mikrofon
<g/>
.	.	kIx.	.
</s>
<s>
Elektretové	Elektretový	k2eAgInPc1d1	Elektretový
mikrofony	mikrofon	k1gInPc1	mikrofon
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
napájení	napájení	k1gNnSc4	napájení
pro	pro	k7c4	pro
vestavěný	vestavěný	k2eAgInSc4d1	vestavěný
předzesilovač	předzesilovač	k1gInSc4	předzesilovač
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
nejnáročnější	náročný	k2eAgInPc4d3	nejnáročnější
profesionální	profesionální	k2eAgInSc4d1	profesionální
účely	účel	k1gInPc4	účel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
měření	měření	k1gNnSc4	měření
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
nenáročné	náročný	k2eNgFnPc4d1	nenáročná
aplikace	aplikace	k1gFnPc4	aplikace
–	–	k?	–
u	u	k7c2	u
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
v	v	k7c6	v
telefonech	telefon	k1gInPc6	telefon
<g/>
,	,	kIx,	,
diktafonech	diktafon	k1gInPc6	diktafon
apod.	apod.	kA	apod.
Poměrně	poměrně	k6eAd1	poměrně
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
konstrukce	konstrukce	k1gFnSc1	konstrukce
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
snadno	snadno	k6eAd1	snadno
miniaturizovat	miniaturizovat	k5eAaImF	miniaturizovat
rozměry	rozměr	k1gInPc4	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Citlivost	citlivost	k1gFnSc1	citlivost
na	na	k7c4	na
1	[number]	k4	1
<g/>
kHz	khz	kA	khz
nebo	nebo	k8xC	nebo
napětí	napětí	k1gNnSc1	napětí
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
-	-	kIx~	-
10	[number]	k4	10
mV	mV	k?	mV
<g/>
/	/	kIx~	/
<g/>
Pa	Pa	kA	Pa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dynamický	dynamický	k2eAgInSc4d1	dynamický
<g/>
/	/	kIx~	/
<g/>
Membránový	membránový	k2eAgInSc4d1	membránový
mikrofon	mikrofon	k1gInSc4	mikrofon
===	===	k?	===
</s>
</p>
<p>
<s>
Membrána	membrána	k1gFnSc1	membrána
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
cívkou	cívka	k1gFnSc7	cívka
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
vytvořeném	vytvořený	k2eAgInSc6d1	vytvořený
permanentním	permanentní	k2eAgInSc7d1	permanentní
magnetem	magnet	k1gInSc7	magnet
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
je	být	k5eAaImIp3nS	být
vytvářen	vytvářen	k2eAgInSc4d1	vytvářen
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Zákon	zákon	k1gInSc1	zákon
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
indukce	indukce	k1gFnSc2	indukce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dynamické	dynamický	k2eAgInPc1d1	dynamický
mikrofony	mikrofon	k1gInPc1	mikrofon
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
citlivé	citlivý	k2eAgInPc1d1	citlivý
než	než	k8xS	než
kondenzátorové	kondenzátorový	k2eAgInPc1d1	kondenzátorový
mikrofony	mikrofon	k1gInPc1	mikrofon
<g/>
,	,	kIx,	,
lépe	dobře	k6eAd2	dobře
proto	proto	k8xC	proto
zpracují	zpracovat	k5eAaPmIp3nP	zpracovat
například	například	k6eAd1	například
hlasitý	hlasitý	k2eAgInSc4d1	hlasitý
zpěv	zpěv	k1gInSc4	zpěv
při	při	k7c6	při
živých	živý	k2eAgInPc6d1	živý
vystoupeních	vystoupení	k1gNnPc6	vystoupení
<g/>
,	,	kIx,	,
ozvučení	ozvučení	k1gNnSc1	ozvučení
veřejných	veřejný	k2eAgFnPc2d1	veřejná
shromáždění	shromáždění	k1gNnSc1	shromáždění
apod.	apod.	kA	apod.
Bývají	bývat	k5eAaImIp3nP	bývat
poměrně	poměrně	k6eAd1	poměrně
odolné	odolný	k2eAgInPc1d1	odolný
proti	proti	k7c3	proti
mechanickému	mechanický	k2eAgNnSc3d1	mechanické
poškození	poškození	k1gNnSc3	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Nevyžadují	vyžadovat	k5eNaImIp3nP	vyžadovat
napájení	napájený	k2eAgMnPc1d1	napájený
<g/>
.	.	kIx.	.
</s>
<s>
Citlivost	citlivost	k1gFnSc1	citlivost
na	na	k7c4	na
1	[number]	k4	1
kHz	khz	kA	khz
nebo	nebo	k8xC	nebo
napětí	napětí	k1gNnSc1	napětí
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Dynamického	dynamický	k2eAgMnSc2d1	dynamický
středoohmového	středoohmový	k2eAgMnSc2d1	středoohmový
3	[number]	k4	3
-	-	kIx~	-
5	[number]	k4	5
mV	mV	k?	mV
<g/>
/	/	kIx~	/
<g/>
Pa	Pa	kA	Pa
a	a	k8xC	a
u	u	k7c2	u
Dynamického	dynamický	k2eAgInSc2d1	dynamický
vysokoohmového	vysokoohmový	k2eAgInSc2d1	vysokoohmový
je	on	k3xPp3gNnSc4	on
10	[number]	k4	10
mV	mV	k?	mV
<g/>
/	/	kIx~	/
<g/>
Pa	Pa	kA	Pa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Páskový	páskový	k2eAgInSc4d1	páskový
mikrofon	mikrofon	k1gInSc4	mikrofon
===	===	k?	===
</s>
</p>
<p>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
dynamického	dynamický	k2eAgInSc2d1	dynamický
mikrofonu	mikrofon	k1gInSc2	mikrofon
je	být	k5eAaImIp3nS	být
mikrofon	mikrofon	k1gInSc1	mikrofon
páskový	páskový	k2eAgInSc1d1	páskový
<g/>
.	.	kIx.	.
</s>
<s>
Membránou	membrána	k1gFnSc7	membrána
je	být	k5eAaImIp3nS	být
kovový	kovový	k2eAgInSc1d1	kovový
pásek	pásek	k1gInSc1	pásek
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
proužek	proužek	k1gInSc1	proužek
tenké	tenký	k2eAgFnSc2d1	tenká
hliníkové	hliníkový	k2eAgFnSc2d1	hliníková
fólie	fólie	k1gFnSc2	fólie
<g/>
,	,	kIx,	,
umístěný	umístěný	k2eAgInSc1d1	umístěný
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
mechanické	mechanický	k2eAgFnSc3d1	mechanická
konstrukci	konstrukce	k1gFnSc3	konstrukce
je	být	k5eAaImIp3nS	být
náchylný	náchylný	k2eAgInSc1d1	náchylný
k	k	k7c3	k
mechanickému	mechanický	k2eAgNnSc3d1	mechanické
poškození	poškození	k1gNnSc3	poškození
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
používán	používat	k5eAaImNgInS	používat
výhradně	výhradně	k6eAd1	výhradně
ve	v	k7c6	v
studiových	studiový	k2eAgFnPc6d1	studiová
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
malému	malý	k2eAgNnSc3d1	malé
napětí	napětí	k1gNnSc3	napětí
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
vybaven	vybavit	k5eAaPmNgInS	vybavit
převodním	převodní	k2eAgInSc7d1	převodní
transformátorem	transformátor	k1gInSc7	transformátor
<g/>
.	.	kIx.	.
</s>
<s>
Citlivost	citlivost	k1gFnSc1	citlivost
na	na	k7c4	na
1	[number]	k4	1
kHz	khz	kA	khz
nebo	nebo	k8xC	nebo
napětí	napětí	k1gNnSc1	napětí
je	být	k5eAaImIp3nS	být
0,1	[number]	k4	0,1
mV	mV	k?	mV
<g/>
/	/	kIx~	/
<g/>
Pa	Pa	kA	Pa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Uhlíkový	uhlíkový	k2eAgInSc4d1	uhlíkový
mikrofon	mikrofon	k1gInSc4	mikrofon
===	===	k?	===
</s>
</p>
<p>
<s>
Uhlíkový	uhlíkový	k2eAgInSc1d1	uhlíkový
mikrofon	mikrofon	k1gInSc1	mikrofon
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgNnSc7	první
prakticky	prakticky	k6eAd1	prakticky
použitelným	použitelný	k2eAgInSc7d1	použitelný
mikrofonem	mikrofon	k1gInSc7	mikrofon
<g/>
,	,	kIx,	,
hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
využíval	využívat	k5eAaPmAgInS	využívat
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
–	–	k?	–
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
telefonech	telefon	k1gInPc6	telefon
až	až	k9	až
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Membrána	membrána	k1gFnSc1	membrána
stlačuje	stlačovat	k5eAaImIp3nS	stlačovat
uhlíková	uhlíkový	k2eAgNnPc4d1	uhlíkové
zrnka	zrnko	k1gNnPc4	zrnko
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
mění	měnit	k5eAaImIp3nS	měnit
jejich	jejich	k3xOp3gNnSc1	jejich
odpor	odpor	k1gInSc1	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Mikrofon	mikrofon	k1gInSc1	mikrofon
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přímo	přímo	k6eAd1	přímo
modulovat	modulovat	k5eAaImF	modulovat
procházející	procházející	k2eAgInSc4d1	procházející
signál	signál	k1gInSc4	signál
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
využívalo	využívat	k5eAaImAgNnS	využívat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ještě	ještě	k6eAd1	ještě
nebyly	být	k5eNaImAgFnP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
zesilovací	zesilovací	k2eAgInPc4d1	zesilovací
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
začátcích	začátek	k1gInPc6	začátek
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
vysílání	vysílání	k1gNnSc2	vysílání
uhlíkovými	uhlíkový	k2eAgInPc7d1	uhlíkový
mikrofony	mikrofon	k1gInPc7	mikrofon
přímo	přímo	k6eAd1	přímo
procházel	procházet	k5eAaImAgInS	procházet
signál	signál	k1gInSc1	signál
vysílače	vysílač	k1gInSc2	vysílač
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
výkonem	výkon	k1gInSc7	výkon
<g/>
,	,	kIx,	,
mikrofony	mikrofon	k1gInPc7	mikrofon
proto	proto	k8xC	proto
vyžadovaly	vyžadovat	k5eAaImAgInP	vyžadovat
chlazení	chlazení	k1gNnSc4	chlazení
<g/>
.	.	kIx.	.
</s>
<s>
Uhlíkové	Uhlíkové	k2eAgInPc1d1	Uhlíkové
mikrofony	mikrofon	k1gInPc1	mikrofon
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nekvalitní	kvalitní	k2eNgFnPc1d1	nekvalitní
<g/>
,	,	kIx,	,
při	při	k7c6	při
nežádoucím	žádoucí	k2eNgInSc6d1	nežádoucí
pohybu	pohyb	k1gInSc6	pohyb
vydávají	vydávat	k5eAaPmIp3nP	vydávat
intenzivní	intenzivní	k2eAgInPc4d1	intenzivní
chrastivé	chrastivý	k2eAgInPc4d1	chrastivý
zvuky	zvuk	k1gInPc4	zvuk
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byly	být	k5eAaImAgFnP	být
umísťovány	umísťovat	k5eAaImNgFnP	umísťovat
do	do	k7c2	do
těžkých	těžký	k2eAgNnPc2d1	těžké
pouzder	pouzdro	k1gNnPc2	pouzdro
upevněných	upevněný	k2eAgNnPc2d1	upevněné
do	do	k7c2	do
soustavy	soustava	k1gFnSc2	soustava
pružin	pružina	k1gFnPc2	pružina
<g/>
.	.	kIx.	.
</s>
<s>
Uhlíkové	Uhlíkové	k2eAgFnPc1d1	Uhlíkové
mikrofonní	mikrofonní	k2eAgFnPc1d1	mikrofonní
vložky	vložka	k1gFnPc1	vložka
se	se	k3xPyFc4	se
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
léta	léto	k1gNnPc1	léto
používaly	používat	k5eAaImAgFnP	používat
v	v	k7c6	v
telefonních	telefonní	k2eAgInPc6d1	telefonní
přístrojích	přístroj	k1gInPc6	přístroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Piezoelektrický	piezoelektrický	k2eAgInSc4d1	piezoelektrický
mikrofon	mikrofon	k1gInSc4	mikrofon
===	===	k?	===
</s>
</p>
<p>
<s>
Piezoelektrický	piezoelektrický	k2eAgInSc1d1	piezoelektrický
mikrofon	mikrofon	k1gInSc1	mikrofon
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
piezoelektrického	piezoelektrický	k2eAgInSc2d1	piezoelektrický
jevu	jev	k1gInSc2	jev
<g/>
:	:	kIx,	:
stlačováním	stlačování	k1gNnSc7	stlačování
či	či	k8xC	či
ohybem	ohyb	k1gInSc7	ohyb
některých	některý	k3yIgInPc2	některý
materiálů	materiál	k1gInPc2	materiál
(	(	kIx(	(
<g/>
solí	sůl	k1gFnPc2	sůl
některých	některý	k3yIgInPc2	některý
minerálů	minerál	k1gInPc2	minerál
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
elektrické	elektrický	k2eAgNnSc4d1	elektrické
napětí	napětí	k1gNnSc4	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
mikrofony	mikrofon	k1gInPc1	mikrofon
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nebyly	být	k5eNaImAgInP	být
příliš	příliš	k6eAd1	příliš
kvalitní	kvalitní	k2eAgInPc1d1	kvalitní
<g/>
.	.	kIx.	.
</s>
<s>
Používaly	používat	k5eAaImAgFnP	používat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
systémech	systém	k1gInPc6	systém
veřejného	veřejný	k2eAgNnSc2d1	veřejné
ozvučení	ozvučení	k1gNnSc2	ozvučení
a	a	k8xC	a
i	i	k9	i
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
od	od	k7c2	od
jejich	jejich	k3xOp3gNnSc2	jejich
užívání	užívání	k1gNnSc2	užívání
záhy	záhy	k6eAd1	záhy
upustilo	upustit	k5eAaPmAgNnS	upustit
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
dynamického	dynamický	k2eAgInSc2d1	dynamický
mikrofonu	mikrofon	k1gInSc2	mikrofon
<g/>
.	.	kIx.	.
</s>
<s>
Piezoelektrického	piezoelektrický	k2eAgInSc2d1	piezoelektrický
jevu	jev	k1gInSc2	jev
se	se	k3xPyFc4	se
však	však	k9	však
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
využívalo	využívat	k5eAaPmAgNnS	využívat
v	v	k7c6	v
zařízení	zařízení	k1gNnSc2	zařízení
podobnému	podobný	k2eAgInSc3d1	podobný
mikrofonu	mikrofon	k1gInSc3	mikrofon
–	–	k?	–
gramofonové	gramofonový	k2eAgFnSc6d1	gramofonová
přenosce	přenoska	k1gFnSc6	přenoska
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
u	u	k7c2	u
levných	levný	k2eAgInPc2d1	levný
výrobků	výrobek	k1gInPc2	výrobek
využívá	využívat	k5eAaImIp3nS	využívat
doposud	doposud	k6eAd1	doposud
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
typech	typ	k1gInPc6	typ
snímačů	snímač	k1gInPc2	snímač
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
nebo	nebo	k8xC	nebo
kontaktních	kontaktní	k2eAgInPc2d1	kontaktní
snímačů	snímač	k1gInPc2	snímač
chvění	chvění	k1gNnSc2	chvění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc4	vlastnost
mikrofonů	mikrofon	k1gInPc2	mikrofon
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Směrové	směrový	k2eAgFnPc1d1	směrová
charakteristiky	charakteristika	k1gFnPc1	charakteristika
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
a	a	k8xC	a
konstrukci	konstrukce	k1gFnSc6	konstrukce
pouzdra	pouzdro	k1gNnSc2	pouzdro
mikrofonu	mikrofon	k1gInSc2	mikrofon
může	moct	k5eAaImIp3nS	moct
tento	tento	k3xDgMnSc1	tento
přijímat	přijímat	k5eAaImF	přijímat
zvuk	zvuk	k1gInSc1	zvuk
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
směrů	směr	k1gInPc2	směr
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
intenzitě	intenzita	k1gFnSc6	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Konstruktéři	konstruktér	k1gMnPc1	konstruktér
mikrofony	mikrofon	k1gInPc1	mikrofon
záměrně	záměrně	k6eAd1	záměrně
navrhují	navrhovat	k5eAaImIp3nP	navrhovat
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
charakteristikami	charakteristika	k1gFnPc7	charakteristika
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
předpokládaném	předpokládaný	k2eAgNnSc6d1	předpokládané
použití	použití	k1gNnSc6	použití
<g/>
.	.	kIx.	.
</s>
<s>
Směrová	směrový	k2eAgFnSc1d1	směrová
charakteristika	charakteristika	k1gFnSc1	charakteristika
je	být	k5eAaImIp3nS	být
frekvenčně	frekvenčně	k6eAd1	frekvenčně
závislá	závislý	k2eAgFnSc1d1	závislá
–	–	k?	–
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
u	u	k7c2	u
vysokých	vysoký	k2eAgInPc2d1	vysoký
tónů	tón	k1gInPc2	tón
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hluboké	hluboký	k2eAgFnPc1d1	hluboká
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
nepoznamenány	poznamenán	k2eNgFnPc1d1	nepoznamenána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všesměrová	všesměrový	k2eAgFnSc1d1	všesměrová
neboli	neboli	k8xC	neboli
omnidirekcionální	omnidirekcionální	k2eAgFnSc1d1	omnidirekcionální
neboli	neboli	k8xC	neboli
kulová	kulový	k2eAgFnSc1d1	kulová
charakteristika	charakteristika	k1gFnSc1	charakteristika
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc3	který
mikrofon	mikrofon	k1gInSc1	mikrofon
přijímá	přijímat	k5eAaImIp3nS	přijímat
zvuk	zvuk	k1gInSc4	zvuk
stejně	stejně	k6eAd1	stejně
kvalitně	kvalitně	k6eAd1	kvalitně
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dosahována	dosahovat	k5eAaImNgFnS	dosahovat
nejjednodušeji	jednoduše	k6eAd3	jednoduše
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
levné	levný	k2eAgInPc4d1	levný
elektretové	elektretový	k2eAgInPc4d1	elektretový
mikrofony	mikrofon	k1gInPc4	mikrofon
<g/>
,	,	kIx,	,
velké	velká	k1gFnPc4	velká
jen	jen	k9	jen
několik	několik	k4yIc4	několik
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
.	.	kIx.	.
<g/>
Kardioidní	Kardioidní	k2eAgFnSc1d1	Kardioidní
neboli	neboli	k8xC	neboli
ledvinová	ledvinový	k2eAgFnSc1d1	ledvinová
charakteristika	charakteristika	k1gFnSc1	charakteristika
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
příjem	příjem	k1gInSc4	příjem
zvuku	zvuk	k1gInSc2	zvuk
"	"	kIx"	"
<g/>
zezadu	zezadu	k6eAd1	zezadu
<g/>
"	"	kIx"	"
mikrofonu	mikrofon	k1gInSc3	mikrofon
<g/>
.	.	kIx.	.
</s>
<s>
Diagram	diagram	k1gInSc1	diagram
připomíná	připomínat	k5eAaImIp3nS	připomínat
Němcům	Němec	k1gMnPc3	Němec
a	a	k8xC	a
Čechům	Čech	k1gMnPc3	Čech
tvar	tvar	k1gInSc4	tvar
ledviny	ledvina	k1gFnSc2	ledvina
(	(	kIx(	(
<g/>
německé	německý	k2eAgNnSc1d1	německé
označení	označení	k1gNnSc1	označení
je	být	k5eAaImIp3nS	být
Niere	Nier	k1gMnSc5	Nier
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anglosasům	Anglosas	k1gMnPc3	Anglosas
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
typickou	typický	k2eAgFnSc4d1	typická
charakteristiku	charakteristika	k1gFnSc4	charakteristika
dynamických	dynamický	k2eAgInPc2d1	dynamický
mikrofonů	mikrofon	k1gInPc2	mikrofon
pro	pro	k7c4	pro
zpěváky	zpěvák	k1gMnPc4	zpěvák
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
vazbu	vazba	k1gFnSc4	vazba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Superkardioidní	Superkardioidní	k2eAgFnSc1d1	Superkardioidní
charakteristika	charakteristika	k1gFnSc1	charakteristika
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
směrová	směrový	k2eAgFnSc1d1	směrová
než	než	k8xS	než
charakteristika	charakteristika	k1gFnSc1	charakteristika
kardioidní	kardioidní	k2eAgFnSc1d1	kardioidní
<g/>
.	.	kIx.	.
</s>
<s>
Mikrofon	mikrofon	k1gInSc1	mikrofon
přijímá	přijímat	k5eAaImIp3nS	přijímat
zvuk	zvuk	k1gInSc4	zvuk
částečně	částečně	k6eAd1	částečně
i	i	k9	i
zezadu	zezadu	k6eAd1	zezadu
<g/>
.	.	kIx.	.
<g/>
Hyperkardioidní	Hyperkardioidní	k2eAgNnSc1d1	Hyperkardioidní
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
hyperledvinová	hyperledvinový	k2eAgFnSc1d1	hyperledvinový
<g/>
)	)	kIx)	)
charakteristika	charakteristika	k1gFnSc1	charakteristika
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
směrová	směrový	k2eAgFnSc1d1	směrová
než	než	k8xS	než
charakteristika	charakteristika	k1gFnSc1	charakteristika
superkardioidní	superkardioidní	k2eAgFnSc1d1	superkardioidní
<g/>
,	,	kIx,	,
příjem	příjem	k1gInSc1	příjem
zvuku	zvuk	k1gInSc2	zvuk
zezadu	zezadu	k6eAd1	zezadu
mikrofonu	mikrofon	k1gInSc2	mikrofon
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
<g/>
Osmičková	osmičkový	k2eAgFnSc1d1	osmičková
neboli	neboli	k8xC	neboli
bidirekcionální	bidirekcionální	k2eAgFnSc1d1	bidirekcionální
charakteristika	charakteristika	k1gFnSc1	charakteristika
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc3	který
mikrofon	mikrofon	k1gInSc1	mikrofon
přijímá	přijímat	k5eAaImIp3nS	přijímat
zvuk	zvuk	k1gInSc4	zvuk
pouze	pouze	k6eAd1	pouze
zepředu	zepředu	k6eAd1	zepředu
a	a	k8xC	a
zezadu	zezadu	k6eAd1	zezadu
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
především	především	k9	především
při	při	k7c6	při
některých	některý	k3yIgFnPc6	některý
metodách	metoda	k1gFnPc6	metoda
snímání	snímání	k1gNnSc2	snímání
stereofonního	stereofonní	k2eAgInSc2d1	stereofonní
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
<g/>
Úzce	úzko	k6eAd1	úzko
směrová	směrový	k2eAgFnSc1d1	směrová
charakteristika	charakteristika	k1gFnSc1	charakteristika
má	mít	k5eAaImIp3nS	mít
výrazně	výrazně	k6eAd1	výrazně
oslaben	oslabit	k5eAaPmNgInS	oslabit
příjem	příjem	k1gInSc1	příjem
zvuku	zvuk	k1gInSc2	zvuk
zezadu	zezadu	k6eAd1	zezadu
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
bývá	bývat	k5eAaImIp3nS	bývat
z	z	k7c2	z
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
důvodů	důvod	k1gInPc2	důvod
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
délky	délka	k1gFnSc2	délka
mikrofonu	mikrofon	k1gInSc2	mikrofon
až	až	k9	až
1	[number]	k4	1
metr	metr	k1gInSc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
speciální	speciální	k2eAgFnPc4d1	speciální
aplikace	aplikace	k1gFnPc4	aplikace
(	(	kIx(	(
<g/>
příjem	příjem	k1gInSc1	příjem
pomocného	pomocný	k2eAgInSc2d1	pomocný
zvuku	zvuk	k1gInSc2	zvuk
při	při	k7c6	při
filmování	filmování	k1gNnSc6	filmování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úzce	úzko	k6eAd1	úzko
směrová	směrový	k2eAgFnSc1d1	směrová
charakteristika	charakteristika	k1gFnSc1	charakteristika
mikrofonu	mikrofon	k1gInSc2	mikrofon
je	být	k5eAaImIp3nS	být
dosahována	dosahovat	k5eAaImNgFnS	dosahovat
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
zřetelného	zřetelný	k2eAgNnSc2d1	zřetelné
zhoršení	zhoršení	k1gNnSc2	zhoršení
frekvenční	frekvenční	k2eAgFnSc2d1	frekvenční
charakteristiky	charakteristika	k1gFnSc2	charakteristika
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
v	v	k7c6	v
části	část	k1gFnSc6	část
akustického	akustický	k2eAgNnSc2d1	akustické
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Frekvenční	frekvenční	k2eAgFnPc1d1	frekvenční
charakteristiky	charakteristika	k1gFnPc1	charakteristika
===	===	k?	===
</s>
</p>
<p>
<s>
Fyzikálním	fyzikální	k2eAgInSc7d1	fyzikální
ideálem	ideál	k1gInSc7	ideál
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
mikrofon	mikrofon	k1gInSc1	mikrofon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
akustický	akustický	k2eAgInSc1d1	akustický
podnět	podnět	k1gInSc1	podnět
přeměnil	přeměnit	k5eAaPmAgInS	přeměnit
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
elektrický	elektrický	k2eAgInSc4d1	elektrický
signál	signál	k1gInSc4	signál
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
není	být	k5eNaImIp3nS	být
a	a	k8xC	a
nejen	nejen	k6eAd1	nejen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
takový	takový	k3xDgInSc1	takový
mikrofon	mikrofon	k1gInSc1	mikrofon
byl	být	k5eAaImAgInS	být
mj.	mj.	kA	mj.
současně	současně	k6eAd1	současně
i	i	k9	i
barometrem	barometr	k1gInSc7	barometr
<g/>
.	.	kIx.	.
</s>
<s>
Dosažení	dosažení	k1gNnSc1	dosažení
vyrovnané	vyrovnaný	k2eAgFnSc2d1	vyrovnaná
charakteristiky	charakteristika	k1gFnSc2	charakteristika
alespoň	alespoň	k9	alespoň
ve	v	k7c6	v
slyšitelné	slyšitelný	k2eAgFnSc6d1	slyšitelná
oblasti	oblast	k1gFnSc6	oblast
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
nákladná	nákladný	k2eAgNnPc4d1	nákladné
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
např.	např.	kA	např.
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgInPc4d1	malý
rozměry	rozměr	k1gInPc4	rozměr
mikrofonu	mikrofon	k1gInSc2	mikrofon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
nedostatku	nedostatek	k1gInSc2	nedostatek
se	se	k3xPyFc4	se
však	však	k9	však
časem	časem	k6eAd1	časem
stala	stát	k5eAaPmAgFnS	stát
ctnost	ctnost	k1gFnSc1	ctnost
a	a	k8xC	a
frekvenční	frekvenční	k2eAgFnPc1d1	frekvenční
nevyrovnanosti	nevyrovnanost	k1gFnPc1	nevyrovnanost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
výrobků	výrobek	k1gInPc2	výrobek
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
využívány	využívat	k5eAaPmNgInP	využívat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pomohly	pomoct	k5eAaPmAgFnP	pomoct
vyzdvihnout	vyzdvihnout	k5eAaPmF	vyzdvihnout
či	či	k8xC	či
potlačit	potlačit	k5eAaPmF	potlačit
některé	některý	k3yIgInPc4	některý
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
zvukové	zvukový	k2eAgInPc4d1	zvukový
odstíny	odstín	k1gInPc4	odstín
snímaných	snímaný	k2eAgInPc2d1	snímaný
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
všechny	všechen	k3xTgInPc4	všechen
mikrofony	mikrofon	k1gInPc4	mikrofon
kromě	kromě	k7c2	kromě
kulových	kulový	k2eAgMnPc2d1	kulový
pracují	pracovat	k5eAaImIp3nP	pracovat
jako	jako	k9	jako
převodníky	převodník	k1gInPc1	převodník
gradientu	gradient	k1gInSc2	gradient
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
s	s	k7c7	s
přibližováním	přibližování	k1gNnSc7	přibližování
ke	k	k7c3	k
zdroji	zdroj	k1gInSc3	zdroj
signálu	signál	k1gInSc2	signál
zesilují	zesilovat	k5eAaImIp3nP	zesilovat
hluboké	hluboký	k2eAgInPc1d1	hluboký
kmitočty	kmitočet	k1gInPc1	kmitočet
–	–	k?	–
tzv.	tzv.	kA	tzv.
proximity	proximita	k1gFnSc2	proximita
efekt	efekt	k1gInSc1	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
využívají	využívat	k5eAaPmIp3nP	využívat
někteří	některý	k3yIgMnPc1	některý
zpěváci	zpěvák	k1gMnPc1	zpěvák
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
teplé	teplý	k2eAgFnSc2d1	teplá
barvy	barva	k1gFnSc2	barva
hlasu	hlas	k1gInSc2	hlas
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
pasážích	pasáž	k1gFnPc6	pasáž
zpěvu	zpěv	k1gInSc2	zpěv
přibližováním	přibližování	k1gNnSc7	přibližování
a	a	k8xC	a
oddalováním	oddalování	k1gNnSc7	oddalování
mikrofonu	mikrofon	k1gInSc2	mikrofon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
speciálnější	speciální	k2eAgInPc4d2	speciálnější
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
mikrofony	mikrofon	k1gInPc1	mikrofon
s	s	k7c7	s
potlačenou	potlačený	k2eAgFnSc7d1	potlačená
částí	část	k1gFnSc7	část
kmitočtové	kmitočtový	k2eAgFnSc2d1	kmitočtová
charakteristiky	charakteristika	k1gFnSc2	charakteristika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pro	pro	k7c4	pro
reportážní	reportážní	k2eAgNnSc4d1	reportážní
snímání	snímání	k1gNnSc4	snímání
řeči	řeč	k1gFnSc2	řeč
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
komunikační	komunikační	k2eAgNnPc4d1	komunikační
zařízení	zařízení	k1gNnPc4	zařízení
v	v	k7c6	v
hlučném	hlučný	k2eAgNnSc6d1	hlučné
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
jsou	být	k5eAaImIp3nP	být
vybaveny	vybaven	k2eAgInPc1d1	vybaven
i	i	k9	i
přepínatelnými	přepínatelný	k2eAgFnPc7d1	přepínatelná
korekcemi	korekce	k1gFnPc7	korekce
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
vlastním	vlastní	k2eAgNnSc6d1	vlastní
tělese	těleso	k1gNnSc6	těleso
mikrofonu	mikrofon	k1gInSc2	mikrofon
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mají	mít	k5eAaImIp3nP	mít
zdůrazněnu	zdůrazněn	k2eAgFnSc4d1	zdůrazněna
určitou	určitý	k2eAgFnSc4d1	určitá
část	část	k1gFnSc4	část
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
třeba	třeba	k6eAd1	třeba
ke	k	k7c3	k
snímání	snímání	k1gNnSc3	snímání
určitých	určitý	k2eAgInPc2d1	určitý
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
a	a	k8xC	a
modely	model	k1gInPc1	model
mikrofonů	mikrofon	k1gInPc2	mikrofon
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mikrofon	mikrofon	k1gInSc4	mikrofon
určený	určený	k2eAgInSc4d1	určený
na	na	k7c4	na
stojan	stojan	k1gInSc4	stojan
===	===	k?	===
</s>
</p>
<p>
<s>
Držák	držák	k1gInSc1	držák
nemá	mít	k5eNaImIp3nS	mít
obecně	obecně	k6eAd1	obecně
žargónovité	žargónovitý	k2eAgNnSc1d1	žargónovitý
označení	označení	k1gNnSc1	označení
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
stačí	stačit	k5eAaBmIp3nS	stačit
pouze	pouze	k6eAd1	pouze
říct	říct	k5eAaPmF	říct
jeho	jeho	k3xOp3gInSc4	jeho
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
odborník	odborník	k1gMnSc1	odborník
vybavil	vybavit	k5eAaPmAgMnS	vybavit
<g/>
,	,	kIx,	,
o	o	k7c4	o
jaký	jaký	k3yIgInSc4	jaký
přístroj	přístroj	k1gInSc4	přístroj
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
evropští	evropský	k2eAgMnPc1d1	evropský
státníci	státník	k1gMnPc1	státník
minulí	minulý	k2eAgMnPc1d1	minulý
i	i	k8xC	i
současní	současný	k2eAgMnPc1d1	současný
nejčastěji	často	k6eAd3	často
využívají	využívat	k5eAaImIp3nP	využívat
Sennheiser	Sennheiser	kA	Sennheiser
MD	MD	kA	MD
<g/>
441	[number]	k4	441
<g/>
.	.	kIx.	.
</s>
<s>
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
jich	on	k3xPp3gMnPc2	on
na	na	k7c6	na
sjezdech	sjezd	k1gInPc6	sjezd
KSČ	KSČ	kA	KSČ
míval	mívat	k5eAaImAgMnS	mívat
na	na	k7c6	na
stojáncích	stojánek	k1gInPc6	stojánek
současně	současně	k6eAd1	současně
osm	osm	k4xCc1	osm
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgInP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
v	v	k7c6	v
NSR	NSR	kA	NSR
<g/>
)	)	kIx)	)
a	a	k8xC	a
posměšně	posměšně	k6eAd1	posměšně
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
dýchací	dýchací	k2eAgInSc1d1	dýchací
přístroj	přístroj	k1gInSc1	přístroj
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
řečnických	řečnický	k2eAgInPc2d1	řečnický
pultů	pult	k1gInPc2	pult
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgMnPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dynamický	dynamický	k2eAgInSc4d1	dynamický
mikrofon	mikrofon	k1gInSc4	mikrofon
s	s	k7c7	s
ledvinovou	ledvinový	k2eAgFnSc7d1	ledvinová
charakteristikou	charakteristika	k1gFnSc7	charakteristika
a	a	k8xC	a
se	s	k7c7	s
zdvihem	zdvih	k1gInSc7	zdvih
vysokých	vysoký	k2eAgFnPc2d1	vysoká
frekvencí	frekvence	k1gFnPc2	frekvence
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
srozumitelnost	srozumitelnost	k1gFnSc1	srozumitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
nečekaně	nečekaně	k6eAd1	nečekaně
jej	on	k3xPp3gNnSc4	on
použila	použít	k5eAaPmAgFnS	použít
i	i	k9	i
skupina	skupina	k1gFnSc1	skupina
Sugababes	Sugababesa	k1gFnPc2	Sugababesa
ve	v	k7c6	v
videoklipu	videoklip	k1gInSc6	videoklip
Round	round	k1gInSc1	round
Round	round	k1gInSc1	round
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
Neumann	Neumann	k1gMnSc1	Neumann
U47	U47	k1gMnSc1	U47
s	s	k7c7	s
tlustým	tlustý	k2eAgNnSc7d1	tlusté
válcovitým	válcovitý	k2eAgNnSc7d1	válcovité
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
rakouský	rakouský	k2eAgInSc1d1	rakouský
AKG	AKG	kA	AKG
C12	C12	k1gMnSc7	C12
jsou	být	k5eAaImIp3nP	být
příklady	příklad	k1gInPc4	příklad
mikrofonů	mikrofon	k1gInPc2	mikrofon
<g/>
,	,	kIx,	,
pokládaných	pokládaný	k2eAgInPc2d1	pokládaný
za	za	k7c4	za
vrchol	vrchol	k1gInSc4	vrchol
pro	pro	k7c4	pro
studiový	studiový	k2eAgInSc4d1	studiový
záznam	záznam	k1gInSc4	záznam
hlasu	hlas	k1gInSc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Technickým	technický	k2eAgInSc7d1	technický
vrcholem	vrchol	k1gInSc7	vrchol
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
mikrofony	mikrofon	k1gInPc1	mikrofon
s	s	k7c7	s
vyrovnanou	vyrovnaný	k2eAgFnSc7d1	vyrovnaná
frekvenční	frekvenční	k2eAgFnSc7d1	frekvenční
charakteristikou	charakteristika	k1gFnSc7	charakteristika
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
slyšitelném	slyšitelný	k2eAgNnSc6d1	slyšitelné
pásmu	pásmo	k1gNnSc6	pásmo
od	od	k7c2	od
dánské	dánský	k2eAgFnSc2d1	dánská
firmy	firma	k1gFnSc2	firma
na	na	k7c4	na
laboratorní	laboratorní	k2eAgInPc4d1	laboratorní
přístroje	přístroj	k1gInPc4	přístroj
Brüel	Brüela	k1gFnPc2	Brüela
&	&	k?	&
Kjæ	Kjæ	k1gFnPc2	Kjæ
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
prodávány	prodávat	k5eAaImNgInP	prodávat
zvukařům	zvukař	k1gMnPc3	zvukař
divizí	divize	k1gFnPc2	divize
této	tento	k3xDgFnSc2	tento
firmy	firma	k1gFnSc2	firma
DPA	DPA	kA	DPA
<g/>
.	.	kIx.	.
</s>
<s>
Soukromá	soukromý	k2eAgNnPc1d1	soukromé
rádia	rádio	k1gNnPc1	rádio
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
moderátory	moderátor	k1gMnPc4	moderátor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zároveň	zároveň	k6eAd1	zároveň
obsluhují	obsluhovat	k5eAaImIp3nP	obsluhovat
odbavovací	odbavovací	k2eAgNnSc4d1	odbavovací
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
americký	americký	k2eAgInSc1d1	americký
dynamický	dynamický	k2eAgInSc1d1	dynamický
kardioidní	kardioidní	k2eAgInSc1d1	kardioidní
mikrofon	mikrofon	k1gInSc1	mikrofon
Electrovoice	Electrovoice	k1gFnSc2	Electrovoice
RE	re	k9	re
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgMnSc2	který
lze	lze	k6eAd1	lze
mluvit	mluvit	k5eAaImF	mluvit
zblízka	zblízka	k6eAd1	zblízka
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
posluchače	posluchač	k1gMnPc4	posluchač
relativně	relativně	k6eAd1	relativně
zeslabí	zeslabit	k5eAaPmIp3nP	zeslabit
hluky	hluk	k1gInPc1	hluk
ovládání	ovládání	k1gNnSc2	ovládání
zařízení	zařízení	k1gNnSc2	zařízení
a	a	k8xC	a
akustika	akustika	k1gFnSc1	akustika
vysílacího	vysílací	k2eAgNnSc2d1	vysílací
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
zdůrazňovaly	zdůrazňovat	k5eAaImAgFnP	zdůrazňovat
basy	basa	k1gFnPc1	basa
a	a	k8xC	a
retnice	retnice	k1gFnPc1	retnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klopák	Klopák	k1gInSc4	Klopák
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Klopák	Klopák	k1gInSc4	Klopák
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
české	český	k2eAgNnSc1d1	české
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
mikrofon	mikrofon	k1gInSc4	mikrofon
velikosti	velikost	k1gFnSc2	velikost
cca	cca	kA	cca
1	[number]	k4	1
cm	cm	kA	cm
spojený	spojený	k2eAgInSc1d1	spojený
se	se	k3xPyFc4	se
sponkou	sponka	k1gFnSc7	sponka
nebo	nebo	k8xC	nebo
magnetem	magnet	k1gInSc7	magnet
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
připevňuje	připevňovat	k5eAaImIp3nS	připevňovat
na	na	k7c4	na
klopu	klopa	k1gFnSc4	klopa
saka	sako	k1gNnSc2	sako
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
ovšem	ovšem	k9	ovšem
o	o	k7c4	o
nesprávné	správný	k2eNgNnSc4d1	nesprávné
umisťování	umisťování	k1gNnSc4	umisťování
<g/>
,	,	kIx,	,
výrobci	výrobce	k1gMnPc1	výrobce
zpravidla	zpravidla	k6eAd1	zpravidla
udávají	udávat	k5eAaImIp3nP	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mikrofon	mikrofon	k1gInSc1	mikrofon
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
níže	nízce	k6eAd2	nízce
a	a	k8xC	a
uprostřed	uprostřed	k6eAd1	uprostřed
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
na	na	k7c6	na
kravatě	kravata	k1gFnSc6	kravata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
kondenzátorových	kondenzátorový	k2eAgFnPc2d1	kondenzátorová
(	(	kIx(	(
<g/>
levnější	levný	k2eAgInPc1d2	levnější
z	z	k7c2	z
elektretových	elektretový	k2eAgInPc2d1	elektretový
<g/>
)	)	kIx)	)
mikrofonů	mikrofon	k1gInPc2	mikrofon
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgNnSc1d1	standardní
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
označení	označení	k1gNnSc1	označení
takového	takový	k3xDgInSc2	takový
mikrofonu	mikrofon	k1gInSc2	mikrofon
je	být	k5eAaImIp3nS	být
lavalier	lavalier	k1gInSc1	lavalier
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
klopákem	klopák	k1gInSc7	klopák
využívaným	využívaný	k2eAgInSc7d1	využívaný
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
všemi	všecek	k3xTgMnPc7	všecek
českými	český	k2eAgMnPc7d1	český
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
evropskými	evropský	k2eAgFnPc7d1	Evropská
televizními	televizní	k2eAgFnPc7d1	televizní
stanicemi	stanice	k1gFnPc7	stanice
ve	v	k7c6	v
zpravodajských	zpravodajský	k2eAgInPc6d1	zpravodajský
pořadech	pořad	k1gInPc6	pořad
je	být	k5eAaImIp3nS	být
německý	německý	k2eAgInSc4d1	německý
Sennheiser	Sennheiser	kA	Sennheiser
MKE	MKE	kA	MKE
40	[number]	k4	40
s	s	k7c7	s
ledvinovou	ledvinový	k2eAgFnSc7d1	ledvinová
směrovou	směrový	k2eAgFnSc7d1	směrová
charakteristikou	charakteristika	k1gFnSc7	charakteristika
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
používán	používat	k5eAaImNgInS	používat
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
černým	černý	k2eAgMnSc7d1	černý
nebo	nebo	k8xC	nebo
bílým	bílý	k2eAgInSc7d1	bílý
protivětrným	protivětrný	k2eAgInSc7d1	protivětrný
krytem	kryt	k1gInSc7	kryt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
dodává	dodávat	k5eAaImIp3nS	dodávat
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
válcový	válcový	k2eAgInSc1d1	válcový
tvar	tvar	k1gInSc1	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
klopák	klopák	k1gInSc1	klopák
použit	použít	k5eAaPmNgInS	použít
neviditelným	viditelný	k2eNgInSc7d1	neviditelný
způsobem	způsob	k1gInSc7	způsob
k	k	k7c3	k
zamikrofonování	zamikrofonování	k1gNnSc3	zamikrofonování
herců	herc	k1gInPc2	herc
ve	v	k7c6	v
filmu	film	k1gInSc6	film
nebo	nebo	k8xC	nebo
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
používán	používat	k5eAaImNgInS	používat
japonský	japonský	k2eAgInSc1d1	japonský
mikrofon	mikrofon	k1gInSc1	mikrofon
Sanken	Sankna	k1gFnPc2	Sankna
Cos	cos	kA	cos
<g/>
,	,	kIx,	,
dánský	dánský	k2eAgInSc4d1	dánský
DPA	DPA	kA	DPA
4061	[number]	k4	4061
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
americký	americký	k2eAgInSc1d1	americký
TRAM	tram	k1gInSc1	tram
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
rozměry	rozměr	k1gInPc4	rozměr
s	s	k7c7	s
kulovou	kulový	k2eAgFnSc7d1	kulová
směrovou	směrový	k2eAgFnSc7d1	směrová
charakteristikou	charakteristika	k1gFnSc7	charakteristika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
takovým	takový	k3xDgInPc3	takový
mikrofonům	mikrofon	k1gInPc3	mikrofon
jsou	být	k5eAaImIp3nP	být
přidávány	přidáván	k2eAgInPc1d1	přidáván
různé	různý	k2eAgInPc1d1	různý
kryty	kryt	k1gInPc1	kryt
<g/>
,	,	kIx,	,
obaly	obal	k1gInPc1	obal
<g/>
,	,	kIx,	,
držátka	držátko	k1gNnPc1	držátko
<g/>
,	,	kIx,	,
tmely	tmel	k1gInPc1	tmel
<g/>
,	,	kIx,	,
lepicí	lepicí	k2eAgInPc1d1	lepicí
pásky	pásek	k1gInPc1	pásek
<g/>
,	,	kIx,	,
oboustranně	oboustranně	k6eAd1	oboustranně
lepicí	lepicí	k2eAgInPc1d1	lepicí
štítky	štítek	k1gInPc1	štítek
a	a	k8xC	a
jiná	jiný	k2eAgNnPc1d1	jiné
vylepšení	vylepšení	k1gNnPc1	vylepšení
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
lze	lze	k6eAd1	lze
mikrofon	mikrofon	k1gInSc1	mikrofon
pevně	pevně	k6eAd1	pevně
přilepit	přilepit	k5eAaPmF	přilepit
k	k	k7c3	k
oděvu	oděv	k1gInSc3	oděv
nebo	nebo	k8xC	nebo
tělu	tělo	k1gNnSc3	tělo
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
slyšet	slyšet	k5eAaImF	slyšet
šustění	šustění	k1gNnSc1	šustění
textilu	textil	k1gInSc2	textil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
mikrofonů	mikrofon	k1gInPc2	mikrofon
===	===	k?	===
</s>
</p>
<p>
<s>
Handka	Handka	k1gFnSc1	Handka
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
mikrofon	mikrofon	k1gInSc4	mikrofon
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
ho	on	k3xPp3gNnSc4	on
zpěváci	zpěvák	k1gMnPc1	zpěvák
<g/>
,	,	kIx,	,
řečníci	řečník	k1gMnPc1	řečník
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgInSc7d3	nejslavnější
modelem	model	k1gInSc7	model
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
mikrofon	mikrofon	k1gInSc1	mikrofon
Shure	Shur	k1gInSc5	Shur
SM	SM	kA	SM
<g/>
58	[number]	k4	58
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
zpěv	zpěv	k1gInSc4	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
nejčastěji	často	k6eAd3	často
dynamické	dynamický	k2eAgInPc1d1	dynamický
<g/>
.	.	kIx.	.
<g/>
Polopuška	Polopuška	k1gFnSc1	Polopuška
neboli	neboli	k8xC	neboli
short	short	k1gInSc1	short
shotgun	shotguna	k1gFnPc2	shotguna
je	být	k5eAaImIp3nS	být
mikrofon	mikrofon	k1gInSc1	mikrofon
s	s	k7c7	s
hyperkardioidní	hyperkardioidní	k2eAgFnSc7d1	hyperkardioidní
charakteristikou	charakteristika	k1gFnSc7	charakteristika
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
snímání	snímání	k1gNnSc3	snímání
zvuku	zvuk	k1gInSc2	zvuk
při	při	k7c6	při
filmování	filmování	k1gNnSc6	filmování
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Sennheiser	Sennheiser	kA	Sennheiser
MKH	MKH	kA	MKH
416	[number]	k4	416
<g/>
.	.	kIx.	.
<g/>
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
puška	puška	k1gFnSc1	puška
neboli	neboli	k8xC	neboli
shotgun	shotgun	k1gInSc1	shotgun
je	být	k5eAaImIp3nS	být
mikrofon	mikrofon	k1gInSc4	mikrofon
s	s	k7c7	s
úzce	úzko	k6eAd1	úzko
směrovou	směrový	k2eAgFnSc7d1	směrová
charakteristikou	charakteristika	k1gFnSc7	charakteristika
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Sennheiser	Sennheiser	kA	Sennheiser
MKH	MKH	kA	MKH
816	[number]	k4	816
<g/>
.	.	kIx.	.
<g/>
PZM	PZM	kA	PZM
neboli	neboli	k8xC	neboli
boundary	boundara	k1gFnSc2	boundara
mic	mic	k?	mic
–	–	k?	–
mikrofon	mikrofon	k1gInSc1	mikrofon
je	být	k5eAaImIp3nS	být
zapuštěn	zapustit	k5eAaPmNgInS	zapustit
v	v	k7c6	v
destičce	destička	k1gFnSc6	destička
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
položí	položit	k5eAaPmIp3nS	položit
na	na	k7c4	na
stůl	stůl	k1gInSc4	stůl
nebo	nebo	k8xC	nebo
na	na	k7c4	na
podlahu	podlaha	k1gFnSc4	podlaha
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
vynalezeno	vynaleznout	k5eAaPmNgNnS	vynaleznout
pro	pro	k7c4	pro
snímání	snímání	k1gNnSc4	snímání
violoncella	violoncello	k1gNnSc2	violoncello
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nevhodně	vhodně	k6eNd1	vhodně
využíváno	využívat	k5eAaPmNgNnS	využívat
pro	pro	k7c4	pro
snímání	snímání	k1gNnSc4	snímání
televizních	televizní	k2eAgFnPc2d1	televizní
diskusí	diskuse	k1gFnPc2	diskuse
<g/>
.	.	kIx.	.
<g/>
Laserový	laserový	k2eAgInSc1d1	laserový
mikrofon	mikrofon	k1gInSc1	mikrofon
–	–	k?	–
speciální	speciální	k2eAgNnSc4d1	speciální
využití	využití	k1gNnSc4	využití
laserového	laserový	k2eAgInSc2d1	laserový
paprsku	paprsek	k1gInSc2	paprsek
jako	jako	k8xC	jako
mikrofonu	mikrofon	k1gInSc2	mikrofon
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
špionážní	špionážní	k2eAgInPc4d1	špionážní
účely	účel	k1gInPc4	účel
</s>
</p>
<p>
<s>
==	==	k?	==
Příslušenství	příslušenství	k1gNnSc1	příslušenství
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
větru	vítr	k1gInSc3	vítr
===	===	k?	===
</s>
</p>
<p>
<s>
Naráží	narážet	k5eAaImIp3nS	narážet
<g/>
-li	i	k?	-li
na	na	k7c4	na
membránu	membrána	k1gFnSc4	membrána
mikrofonu	mikrofon	k1gInSc2	mikrofon
proud	proud	k1gInSc1	proud
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
mikrofon	mikrofon	k1gInSc4	mikrofon
používán	používat	k5eAaImNgInS	používat
venku	venku	k6eAd1	venku
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stačí	stačit	k5eAaBmIp3nS	stačit
i	i	k9	i
proud	proud	k1gInSc1	proud
vzduchu	vzduch	k1gInSc2	vzduch
při	při	k7c6	při
vyslovování	vyslovování	k1gNnSc6	vyslovování
retnic	retnice	k1gFnPc2	retnice
(	(	kIx(	(
<g/>
hlásky	hlásek	k1gInPc1	hlásek
"	"	kIx"	"
<g/>
p	p	k?	p
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
t	t	k?	t
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
b	b	k?	b
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
vznikají	vznikat	k5eAaImIp3nP	vznikat
nepříjemné	příjemný	k2eNgInPc1d1	nepříjemný
silné	silný	k2eAgInPc1d1	silný
hluboké	hluboký	k2eAgInPc1d1	hluboký
pazvuky	pazvuk	k1gInPc1	pazvuk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zpravidla	zpravidla	k6eAd1	zpravidla
zcela	zcela	k6eAd1	zcela
znehodnotí	znehodnotit	k5eAaPmIp3nS	znehodnotit
sejmutý	sejmutý	k2eAgInSc4d1	sejmutý
záznam	záznam	k1gInSc4	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Mikrofon	mikrofon	k1gInSc1	mikrofon
lze	lze	k6eAd1	lze
proto	proto	k8xC	proto
často	často	k6eAd1	často
umístit	umístit	k5eAaPmF	umístit
do	do	k7c2	do
dodatečného	dodatečný	k2eAgInSc2d1	dodatečný
obalu	obal	k1gInSc2	obal
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
zhoršení	zhoršení	k1gNnSc2	zhoršení
přenosu	přenos	k1gInSc2	přenos
vysokých	vysoký	k2eAgFnPc2d1	vysoká
frekvencí	frekvence	k1gFnPc2	frekvence
soustředěný	soustředěný	k2eAgInSc1d1	soustředěný
proud	proud	k1gInSc1	proud
vzduchu	vzduch	k1gInSc2	vzduch
láme	lámat	k5eAaImIp3nS	lámat
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
ochran	ochrana	k1gFnPc2	ochrana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejzákladnější	základní	k2eAgMnPc1d3	nejzákladnější
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
koš	koš	k1gInSc1	koš
(	(	kIx(	(
<g/>
zřídka	zřídka	k6eAd1	zřídka
též	též	k9	též
pucka	pucka	k1gFnSc1	pucka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
windjammer	windjammer	k1gInSc1	windjammer
–	–	k?	–
několik	několik	k4yIc4	několik
centimetrů	centimetr	k1gInPc2	centimetr
velký	velký	k2eAgInSc1d1	velký
ovál	ovál	k1gInSc1	ovál
nasazený	nasazený	k2eAgInSc1d1	nasazený
na	na	k7c6	na
handku	handek	k1gInSc6	handek
</s>
</p>
<p>
<s>
cepelín	cepelín	k1gInSc1	cepelín
(	(	kIx(	(
<g/>
srov.	srov.	kA	srov.
s	s	k7c7	s
tvarem	tvar	k1gInSc7	tvar
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
Zeppelin	Zeppelin	k2eAgMnSc1d1	Zeppelin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
windshield	windshield	k1gInSc1	windshield
–	–	k?	–
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
centimetrů	centimetr	k1gInPc2	centimetr
velký	velký	k2eAgInSc4d1	velký
kryt	kryt	k2eAgInSc4d1	kryt
pokrývající	pokrývající	k2eAgInSc4d1	pokrývající
celý	celý	k2eAgInSc4d1	celý
mikrofon	mikrofon	k1gInSc4	mikrofon
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
ceně	cena	k1gFnSc6	cena
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
kryty	kryt	k1gInPc1	kryt
konstruovány	konstruován	k2eAgInPc1d1	konstruován
z	z	k7c2	z
levných	levný	k2eAgFnPc2d1	levná
umělých	umělý	k2eAgFnPc2d1	umělá
hmot	hmota	k1gFnPc2	hmota
nebo	nebo	k8xC	nebo
z	z	k7c2	z
pravé	pravý	k2eAgFnSc2d1	pravá
kožešiny	kožešina	k1gFnSc2	kožešina
sibiřské	sibiřský	k2eAgFnSc2d1	sibiřská
lišky	liška	k1gFnSc2	liška
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
(	(	kIx(	(
<g/>
nejdražším	drahý	k2eAgInSc7d3	nejdražší
<g/>
)	)	kIx)	)
výrobcem	výrobce	k1gMnSc7	výrobce
profesionálních	profesionální	k2eAgInPc2d1	profesionální
krytů	kryt	k1gInPc2	kryt
je	být	k5eAaImIp3nS	být
firma	firma	k1gFnSc1	firma
Rycote	Rycot	k1gInSc5	Rycot
<g/>
.	.	kIx.	.
</s>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
výrobci	výrobce	k1gMnPc1	výrobce
mikrofonů	mikrofon	k1gInPc2	mikrofon
rovněž	rovněž	k9	rovněž
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
protivětrné	protivětrný	k2eAgInPc4d1	protivětrný
kryty	kryt	k1gInPc4	kryt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pop	pop	k1gInSc1	pop
filtr	filtr	k1gInSc1	filtr
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
ve	v	k7c6	v
studiích	studie	k1gFnPc6	studie
se	se	k3xPyFc4	se
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
mikrofonů	mikrofon	k1gInPc2	mikrofon
používají	používat	k5eAaImIp3nP	používat
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
POP	pop	k1gInSc4	pop
filtry	filtr	k1gInPc1	filtr
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
extrémních	extrémní	k2eAgFnPc2d1	extrémní
změn	změna	k1gFnPc2	změna
akustických	akustický	k2eAgInPc2d1	akustický
tlaků	tlak	k1gInPc2	tlak
při	při	k7c6	při
explozivních	explozivní	k2eAgFnPc6d1	explozivní
hláskách	hláska	k1gFnPc6	hláska
a	a	k8xC	a
slabikách	slabika	k1gFnPc6	slabika
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
p	p	k?	p
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
b	b	k?	b
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
t	t	k?	t
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ř	ř	k?	ř
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
sykavky	sykavka	k1gFnPc4	sykavka
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
využívají	využívat	k5eAaImIp3nP	využívat
principu	princip	k1gInSc3	princip
rozptýlení	rozptýlení	k1gNnSc2	rozptýlení
akustické	akustický	k2eAgFnSc2d1	akustická
energie	energie	k1gFnSc2	energie
mimo	mimo	k7c4	mimo
hlavní	hlavní	k2eAgInSc4d1	hlavní
směr	směr	k1gInSc4	směr
šíření	šíření	k1gNnSc2	šíření
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
rámečky	rámeček	k1gInPc1	rámeček
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
napnuta	napnut	k2eAgFnSc1d1	napnuta
různá	různý	k2eAgFnSc1d1	různá
průzvučná	průzvučný	k2eAgFnSc1d1	průzvučná
tkanina	tkanina	k1gFnSc1	tkanina
<g/>
,	,	kIx,	,
síťka	síťka	k1gFnSc1	síťka
a	a	k8xC	a
podobné	podobný	k2eAgInPc1d1	podobný
materiály	materiál	k1gInPc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Upevňují	upevňovat	k5eAaImIp3nP	upevňovat
se	se	k3xPyFc4	se
různými	různý	k2eAgMnPc7d1	různý
držáky	držák	k1gInPc1	držák
ke	k	k7c3	k
stojanům	stojan	k1gInPc3	stojan
mikrofonů	mikrofon	k1gInPc2	mikrofon
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
mikrofon	mikrofon	k1gInSc4	mikrofon
a	a	k8xC	a
zpěváka	zpěvák	k1gMnSc2	zpěvák
či	či	k8xC	či
řečníka	řečník	k1gMnSc2	řečník
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
odstranění	odstranění	k1gNnSc2	odstranění
problémů	problém	k1gInPc2	problém
při	při	k7c6	při
snímání	snímání	k1gNnSc6	snímání
retnic	retnice	k1gFnPc2	retnice
i	i	k8xC	i
ochrana	ochrana	k1gFnSc1	ochrana
mikrofonu	mikrofon	k1gInSc2	mikrofon
před	před	k7c7	před
dechem	dech	k1gInSc7	dech
a	a	k8xC	a
ev.	ev.	k?	ev.
i	i	k8xC	i
slinami	slina	k1gFnPc7	slina
<g/>
.	.	kIx.	.
</s>
<s>
Vlhko	vlhko	k1gNnSc1	vlhko
dechu	dech	k1gInSc2	dech
některým	některý	k3yIgInPc3	některý
mikrofonům	mikrofon	k1gInPc3	mikrofon
značně	značně	k6eAd1	značně
škodí	škodit	k5eAaImIp3nS	škodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Upevnění	upevnění	k1gNnSc2	upevnění
===	===	k?	===
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
mikrofon	mikrofon	k1gInSc4	mikrofon
držen	držen	k2eAgInSc4d1	držen
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
nebo	nebo	k8xC	nebo
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
provedení	provedení	k1gNnSc6	provedení
PZM	PZM	kA	PZM
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
upevněn	upevněn	k2eAgInSc1d1	upevněn
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
držáku	držák	k1gInSc6	držák
<g/>
.	.	kIx.	.
</s>
<s>
Držák	držák	k1gInSc1	držák
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jako	jako	k9	jako
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
objímka	objímka	k1gFnSc1	objímka
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
tělo	tělo	k1gNnSc1	tělo
mikrofonu	mikrofon	k1gInSc2	mikrofon
zasune	zasunout	k5eAaPmIp3nS	zasunout
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
odpružovací	odpružovací	k2eAgInSc1d1	odpružovací
mechanismus	mechanismus	k1gInSc1	mechanismus
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
pavouk	pavouk	k1gMnSc1	pavouk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Držák	držák	k1gInSc1	držák
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
některým	některý	k3yIgInSc7	některý
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
stojan	stojan	k1gInSc1	stojan
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
stativ	stativ	k1gInSc1	stativ
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
slovo	slovo	k1gNnSc1	slovo
využívané	využívaný	k2eAgNnSc1d1	využívané
fotografy	fotograf	k1gMnPc7	fotograf
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
stojánek	stojánek	k1gInSc1	stojánek
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
destička	destička	k1gFnSc1	destička
nebo	nebo	k8xC	nebo
trojnožka	trojnožka	k1gFnSc1	trojnožka
s	s	k7c7	s
vyčnívajícím	vyčnívající	k2eAgInSc7d1	vyčnívající
šroubkem	šroubek	k1gInSc7	šroubek
pro	pro	k7c4	pro
položení	položení	k1gNnSc4	položení
na	na	k7c4	na
stůl	stůl	k1gInSc4	stůl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
šroubek	šroubek	k1gInSc4	šroubek
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
přišroubuje	přišroubovat	k5eAaPmIp3nS	přišroubovat
držák	držák	k1gInSc1	držák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
husí	husí	k2eAgInSc1d1	husí
krk	krk	k1gInSc1	krk
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
gooseneck	gooseneck	k1gInSc1	gooseneck
<g/>
)	)	kIx)	)
–	–	k?	–
mikrofon	mikrofon	k1gInSc1	mikrofon
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
centimetrů	centimetr	k1gInPc2	centimetr
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
tenkou	tenký	k2eAgFnSc7d1	tenká
pružnou	pružný	k2eAgFnSc7d1	pružná
trubkou	trubka	k1gFnSc7	trubka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
lze	lze	k6eAd1	lze
zkroutit	zkroutit	k5eAaPmF	zkroutit
do	do	k7c2	do
téměř	téměř	k6eAd1	téměř
libovolné	libovolný	k2eAgFnSc2d1	libovolná
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
konec	konec	k1gInSc1	konec
trubky	trubka	k1gFnSc2	trubka
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
zapuštěn	zapustit	k5eAaPmNgInS	zapustit
do	do	k7c2	do
stolu	stol	k1gInSc2	stol
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
rádiích	rádio	k1gNnPc6	rádio
<g/>
,	,	kIx,	,
v	v	k7c6	v
konferenčních	konferenční	k2eAgFnPc6d1	konferenční
místnostech	místnost	k1gFnPc6	místnost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
dorozumívací	dorozumívací	k2eAgInPc4d1	dorozumívací
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
u	u	k7c2	u
řečnických	řečnický	k2eAgInPc2d1	řečnický
stolů	stol	k1gInPc2	stol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tyčka	tyčka	k1gFnSc1	tyčka
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
boom	boom	k1gInSc1	boom
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
několikametrová	několikametrový	k2eAgFnSc1d1	několikametrová
zpravidla	zpravidla	k6eAd1	zpravidla
teleskopická	teleskopický	k2eAgFnSc1d1	teleskopická
tyč	tyč	k1gFnSc1	tyč
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
lehkého	lehký	k2eAgInSc2d1	lehký
moderního	moderní	k2eAgInSc2d1	moderní
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kevlar	kevlar	k1gInSc1	kevlar
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
filmování	filmování	k1gNnSc6	filmování
hraných	hraný	k2eAgInPc2d1	hraný
filmů	film	k1gInPc2	film
ji	on	k3xPp3gFnSc4	on
drží	držet	k5eAaImIp3nS	držet
mikrofonista	mikrofonista	k1gMnSc1	mikrofonista
(	(	kIx(	(
<g/>
také	také	k9	také
"	"	kIx"	"
<g/>
asistent	asistent	k1gMnSc1	asistent
zvuku	zvuk	k1gInSc2	zvuk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
snímala	snímat	k5eAaImAgFnS	snímat
zvuk	zvuk	k1gInSc4	zvuk
z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
herců	herc	k1gInPc2	herc
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
záběru	záběr	k1gInSc6	záběr
kamery	kamera	k1gFnSc2	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Tyčce	tyčka	k1gFnSc3	tyčka
se	se	k3xPyFc4	se
také	také	k9	také
běžně	běžně	k6eAd1	běžně
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
tágo	tágo	k1gNnSc1	tágo
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
bambus	bambus	k1gInSc1	bambus
<g/>
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
kdysi	kdysi	k6eAd1	kdysi
se	se	k3xPyFc4	se
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
totiž	totiž	k9	totiž
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
účelům	účel	k1gInPc3	účel
užívalo	užívat	k5eAaImAgNnS	užívat
bambusových	bambusový	k2eAgFnPc2d1	bambusová
tyčí	tyč	k1gFnPc2	tyč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
šibenice	šibenice	k1gFnSc1	šibenice
je	být	k5eAaImIp3nS	být
tyčka	tyčka	k1gFnSc1	tyčka
upevněná	upevněný	k2eAgFnSc1d1	upevněná
na	na	k7c6	na
teleskopickém	teleskopický	k2eAgInSc6d1	teleskopický
stojanu	stojan	k1gInSc6	stojan
<g/>
,	,	kIx,	,
obojím	obé	k1gNnSc7	obé
lze	lze	k6eAd1	lze
libovolně	libovolně	k6eAd1	libovolně
pohybovat	pohybovat	k5eAaImF	pohybovat
zpravidla	zpravidla	k6eAd1	zpravidla
soustavou	soustava	k1gFnSc7	soustava
klik	klika	k1gFnPc2	klika
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
soustava	soustava	k1gFnSc1	soustava
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
židličkou	židlička	k1gFnSc7	židlička
na	na	k7c6	na
vozíčku	vozíček	k1gInSc6	vozíček
<g/>
.	.	kIx.	.
</s>
<s>
Používala	používat	k5eAaImAgFnS	používat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
v	v	k7c6	v
začátcích	začátek	k1gInPc6	začátek
filmu	film	k1gInSc2	film
a	a	k8xC	a
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
při	při	k7c6	při
snímání	snímání	k1gNnSc6	snímání
delších	dlouhý	k2eAgFnPc2d2	delší
scén	scéna	k1gFnPc2	scéna
(	(	kIx(	(
<g/>
např.	např.	kA	např.
diskusních	diskusní	k2eAgInPc2d1	diskusní
pořadů	pořad	k1gInPc2	pořad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozměrům	rozměr	k1gInPc3	rozměr
a	a	k8xC	a
cenám	cena	k1gFnPc3	cena
je	být	k5eAaImIp3nS	být
vytlačována	vytlačovat	k5eAaImNgFnS	vytlačovat
používáním	používání	k1gNnSc7	používání
mikroportů	mikroport	k1gInPc2	mikroport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
mikrofon	mikrofon	k1gInSc1	mikrofon
může	moct	k5eAaImIp3nS	moct
viset	viset	k5eAaImF	viset
i	i	k9	i
od	od	k7c2	od
stropu	strop	k1gInSc2	strop
místnosti	místnost	k1gFnSc2	místnost
na	na	k7c6	na
vhodném	vhodný	k2eAgNnSc6d1	vhodné
zařízení	zařízení	k1gNnSc6	zařízení
(	(	kIx(	(
<g/>
tah	tah	k1gInSc1	tah
<g/>
,	,	kIx,	,
rošt	rošt	k1gInSc1	rošt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
nad	nad	k7c7	nad
scénou	scéna	k1gFnSc7	scéna
</s>
</p>
<p>
<s>
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
úchytů	úchyt	k1gInPc2	úchyt
pro	pro	k7c4	pro
klopové	klopový	k2eAgInPc4d1	klopový
mikrofony	mikrofon	k1gInPc4	mikrofon
byly	být	k5eAaImAgFnP	být
zmíněny	zmínit	k5eAaPmNgInP	zmínit
již	již	k9	již
výše	vysoce	k6eAd2	vysoce
</s>
</p>
<p>
<s>
==	==	k?	==
Připojení	připojení	k1gNnSc1	připojení
mikrofonů	mikrofon	k1gInPc2	mikrofon
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Elektrické	elektrický	k2eAgNnSc1d1	elektrické
zapojení	zapojení	k1gNnSc1	zapojení
===	===	k?	===
</s>
</p>
<p>
<s>
Levnější	levný	k2eAgInPc1d2	levnější
mikrofony	mikrofon	k1gInPc1	mikrofon
bývají	bývat	k5eAaImIp3nP	bývat
zapojeny	zapojit	k5eAaPmNgInP	zapojit
nesymetricky	symetricky	k6eNd1	symetricky
pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgInPc2	dva
vodičů	vodič	k1gInPc2	vodič
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jedním	jeden	k4xCgInSc7	jeden
je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgInS	vést
signál	signál	k1gInSc1	signál
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
zemní	zemní	k2eAgInSc1d1	zemní
vodič	vodič	k1gInSc1	vodič
a	a	k8xC	a
stínění	stínění	k1gNnSc1	stínění
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitnější	kvalitní	k2eAgInPc1d2	kvalitnější
mikrofony	mikrofon	k1gInPc1	mikrofon
používají	používat	k5eAaImIp3nP	používat
symetrického	symetrický	k2eAgNnSc2d1	symetrické
zapojení	zapojení	k1gNnSc2	zapojení
<g/>
.	.	kIx.	.
</s>
<s>
Signál	signál	k1gInSc1	signál
je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgInS	vést
dvěma	dva	k4xCgInPc7	dva
vodiči	vodič	k1gInPc7	vodič
v	v	k7c6	v
protifázi	protifáze	k1gFnSc6	protifáze
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc1	třetí
vodič	vodič	k1gInSc1	vodič
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
stínění	stínění	k1gNnSc1	stínění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Napájení	napájení	k1gNnSc4	napájení
===	===	k?	===
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
typy	typ	k1gInPc1	typ
mikrofonů	mikrofon	k1gInPc2	mikrofon
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
napájení	napájení	k1gNnPc4	napájení
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
standardů	standard	k1gInPc2	standard
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
podporovány	podporovat	k5eAaImNgInP	podporovat
výrobci	výrobce	k1gMnPc1	výrobce
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yIgNnPc2	který
se	se	k3xPyFc4	se
mikrofony	mikrofon	k1gInPc7	mikrofon
připojují	připojovat	k5eAaImIp3nP	připojovat
(	(	kIx(	(
<g/>
mixážní	mixážní	k2eAgInSc4d1	mixážní
pult	pult	k1gInSc4	pult
<g/>
,	,	kIx,	,
videokamera	videokamera	k1gFnSc1	videokamera
<g/>
,	,	kIx,	,
počítač	počítač	k1gInSc1	počítač
atd.	atd.	kA	atd.
–	–	k?	–
dále	daleko	k6eAd2	daleko
"	"	kIx"	"
<g/>
spotřebič	spotřebič	k1gInSc1	spotřebič
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
při	při	k7c6	při
správné	správný	k2eAgFnSc6d1	správná
kombinaci	kombinace	k1gFnSc6	kombinace
mikrofonu	mikrofon	k1gInSc2	mikrofon
a	a	k8xC	a
zařízení	zařízení	k1gNnSc2	zařízení
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
k	k	k7c3	k
mikrofonu	mikrofon	k1gInSc3	mikrofon
připojovat	připojovat	k5eAaImF	připojovat
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
napáječ	napáječ	k1gInSc4	napáječ
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zařízení	zařízení	k1gNnSc1	zařízení
samo	sám	k3xTgNnSc1	sám
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
zdroj	zdroj	k1gInSc1	zdroj
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tonaderspeisung	Tonaderspeisung	k1gInSc1	Tonaderspeisung
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používá	používat	k5eAaImIp3nS	používat
německý	německý	k2eAgInSc1d1	německý
název	název	k1gInSc1	název
<g/>
;	;	kIx,	;
označení	označení	k1gNnSc1	označení
jako	jako	k8xC	jako
AB	AB	kA	AB
napájení	napájení	k1gNnSc1	napájení
<g/>
,	,	kIx,	,
T-power	Tower	k1gInSc1	T-power
nebo	nebo	k8xC	nebo
paralelní	paralelní	k2eAgNnPc1d1	paralelní
napájení	napájení	k1gNnPc1	napájení
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
neuchytila	uchytit	k5eNaPmAgFnS	uchytit
<g/>
)	)	kIx)	)
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
normou	norma	k1gFnSc7	norma
DIN	din	k1gInSc1	din
45595	[number]	k4	45595
–	–	k?	–
napájení	napájení	k1gNnSc1	napájení
12	[number]	k4	12
V	V	kA	V
je	být	k5eAaImIp3nS	být
vedeno	vést	k5eAaImNgNnS	vést
přímo	přímo	k6eAd1	přímo
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
shodné	shodný	k2eAgInPc4d1	shodný
odpory	odpor	k1gInPc4	odpor
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
pólech	pól	k1gInPc6	pól
<g/>
)	)	kIx)	)
symetrickým	symetrický	k2eAgNnSc7d1	symetrické
vedením	vedení	k1gNnSc7	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Mikrofon	mikrofon	k1gInSc1	mikrofon
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
střídavé	střídavý	k2eAgNnSc4d1	střídavé
napětí	napětí	k1gNnSc4	napětí
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
spotřebiče	spotřebič	k1gInSc2	spotřebič
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
stejnosměrné	stejnosměrný	k2eAgNnSc1d1	stejnosměrné
napájecí	napájecí	k2eAgNnSc1d1	napájecí
napětí	napětí	k1gNnSc1	napětí
filtrováno	filtrován	k2eAgNnSc1d1	filtrováno
<g/>
.	.	kIx.	.
<g/>
Fantomové	Fantomový	k2eAgNnSc1d1	Fantomové
napájení	napájení	k1gNnSc1	napájení
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Phantom	Phantom	k1gInSc1	Phantom
Supply	Supply	k1gFnSc1	Supply
<g/>
/	/	kIx~	/
<g/>
Powering	Powering	k1gInSc1	Powering
<g/>
)	)	kIx)	)
norma	norma	k1gFnSc1	norma
Audio	audio	k2eAgFnSc1d1	audio
Engineering	Engineering	k1gInSc4	Engineering
Society	societa	k1gFnSc2	societa
<g/>
,	,	kIx,	,
DIN	din	k1gInSc1	din
45596	[number]	k4	45596
–	–	k?	–
mikrofon	mikrofon	k1gInSc1	mikrofon
je	být	k5eAaImIp3nS	být
spojen	spojen	k2eAgInSc1d1	spojen
symetrickým	symetrický	k2eAgNnSc7d1	symetrické
vedením	vedení	k1gNnSc7	vedení
se	s	k7c7	s
spotřebičem	spotřebič	k1gInSc7	spotřebič
pomocí	pomocí	k7c2	pomocí
konektoru	konektor	k1gInSc2	konektor
XLR-3	XLR-3	k1gFnSc2	XLR-3
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Cannon	Cannon	k1gMnSc1	Cannon
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
původního	původní	k2eAgMnSc2d1	původní
výrobce	výrobce	k1gMnSc2	výrobce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
vývody	vývod	k1gInPc7	vývod
1,2	[number]	k4	1,2
resp.	resp.	kA	resp.
mezi	mezi	k7c7	mezi
vývody	vývod	k1gInPc7	vývod
1,3	[number]	k4	1,3
lze	lze	k6eAd1	lze
naměřit	naměřit	k5eAaBmF	naměřit
napětí	napětí	k1gNnSc4	napětí
9	[number]	k4	9
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
,	,	kIx,	,
48	[number]	k4	48
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
52	[number]	k4	52
V.	V.	kA	V.
Mezi	mezi	k7c7	mezi
vývody	vývod	k1gInPc7	vývod
2,3	[number]	k4	2,3
nelze	lze	k6eNd1	lze
naměřit	naměřit	k5eAaBmF	naměřit
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Signál	signál	k1gInSc1	signál
se	se	k3xPyFc4	se
z	z	k7c2	z
mikrofonu	mikrofon	k1gInSc2	mikrofon
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
spotřebiče	spotřebič	k1gInSc2	spotřebič
právě	právě	k9	právě
po	po	k7c6	po
vodičích	vodič	k1gInPc6	vodič
2,3	[number]	k4	2,3
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
připojen	připojit	k5eAaPmNgInS	připojit
mikrofon	mikrofon	k1gInSc1	mikrofon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
napájení	napájený	k2eAgMnPc1d1	napájený
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dynamický	dynamický	k2eAgInSc1d1	dynamický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neděje	dít	k5eNaImIp3nS	dít
se	se	k3xPyFc4	se
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
připojit	připojit	k5eAaPmF	připojit
dynamický	dynamický	k2eAgInSc4d1	dynamický
mikrofon	mikrofon	k1gInSc4	mikrofon
bez	bez	k7c2	bez
výstupního	výstupní	k2eAgInSc2d1	výstupní
transformátoru	transformátor	k1gInSc2	transformátor
-	-	kIx~	-
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
fantomové	fantomový	k2eAgNnSc1d1	fantomové
napájení	napájení	k1gNnSc1	napájení
spálilo	spálit	k5eAaPmAgNnS	spálit
vinutí	vinutí	k1gNnSc4	vinutí
cívky	cívka	k1gFnSc2	cívka
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
standardu	standard	k1gInSc2	standard
PC99	PC99	k1gFnSc2	PC99
je	být	k5eAaImIp3nS	být
mikrofon	mikrofon	k1gInSc4	mikrofon
připojený	připojený	k2eAgInSc4d1	připojený
nesymetricky	symetricky	k6eNd1	symetricky
k	k	k7c3	k
počítači	počítač	k1gInSc3	počítač
stereofonním	stereofonní	k2eAgInSc7d1	stereofonní
3,5	[number]	k4	3,5
<g/>
mm	mm	kA	mm
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
konektorem	konektor	k1gInSc7	konektor
"	"	kIx"	"
<g/>
jack	jack	k1gInSc1	jack
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
konektoru	konektor	k1gInSc2	konektor
(	(	kIx(	(
<g/>
sleeve	sleevat	k5eAaPmIp3nS	sleevat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
na	na	k7c6	na
kroužku	kroužek	k1gInSc6	kroužek
(	(	kIx(	(
<g/>
ring	ring	k1gInSc4	ring
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
napájení	napájení	k1gNnSc1	napájení
+5	+5	k4	+5
V	V	kA	V
pro	pro	k7c4	pro
mikrofonní	mikrofonní	k2eAgInSc4d1	mikrofonní
předzesilovač	předzesilovač	k1gInSc4	předzesilovač
<g/>
,	,	kIx,	,
na	na	k7c6	na
špičce	špička	k1gFnSc6	špička
(	(	kIx(	(
<g/>
tip	tip	k1gInSc1	tip
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
signál	signál	k1gInSc4	signál
z	z	k7c2	z
mikrofonu	mikrofon	k1gInSc2	mikrofon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Siegfried	Siegfried	k1gInSc1	Siegfried
Wirsum	Wirsum	k1gInSc1	Wirsum
<g/>
:	:	kIx,	:
Abeceda	abeceda	k1gFnSc1	abeceda
NF	NF	kA	NF
techniky	technika	k1gFnPc1	technika
<g/>
,	,	kIx,	,
BEN	Ben	k1gInSc1	Ben
-	-	kIx~	-
technická	technický	k2eAgFnSc1d1	technická
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86056	[number]	k4	86056
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
12	[number]	k4	12
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mikrofon	mikrofon	k1gInSc1	mikrofon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
mikrofon	mikrofon	k1gInSc1	mikrofon
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Historické	historický	k2eAgInPc4d1	historický
mikrofony	mikrofon	k1gInPc4	mikrofon
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
fotogalerie	fotogalerie	k1gFnSc1	fotogalerie
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
popisem	popis	k1gInSc7	popis
</s>
</p>
<p>
<s>
Mikrofony	mikrofon	k1gInPc1	mikrofon
Technický	technický	k2eAgInSc1d1	technický
popis	popis	k1gInSc4	popis
<g/>
,	,	kIx,	,
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
fotogalerie	fotogalerie	k1gFnPc4	fotogalerie
historických	historický	k2eAgInPc2d1	historický
mikrofonů	mikrofon	k1gInPc2	mikrofon
</s>
</p>
<p>
<s>
Audiozone	Audiozon	k1gMnSc5	Audiozon
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
diskuzní	diskuzní	k2eAgNnSc1d1	diskuzní
fórum	fórum	k1gNnSc1	fórum
o	o	k7c6	o
mikrofonech	mikrofon	k1gInPc6	mikrofon
</s>
</p>
<p>
<s>
Zvukařina	Zvukařina	k1gFnSc1	Zvukařina
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
diskuzní	diskuzní	k2eAgNnSc1d1	diskuzní
fórum	fórum	k1gNnSc1	fórum
o	o	k7c6	o
mikrofonech	mikrofon	k1gInPc6	mikrofon
</s>
</p>
