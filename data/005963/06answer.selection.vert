<s>
Řecké	řecký	k2eAgNnSc1d1	řecké
písmo	písmo	k1gNnSc1	písmo
(	(	kIx(	(
<g/>
též	též	k9	též
řecká	řecký	k2eAgFnSc1d1	řecká
abeceda	abeceda	k1gFnSc1	abeceda
nebo	nebo	k8xC	nebo
alfabeta	alfabeta	k1gFnSc1	alfabeta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgNnSc1d3	nejstarší
dosud	dosud	k6eAd1	dosud
používané	používaný	k2eAgNnSc1d1	používané
písmo	písmo	k1gNnSc1	písmo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
