<s>
Řecké	řecký	k2eAgNnSc1d1	řecké
písmo	písmo	k1gNnSc1	písmo
(	(	kIx(	(
<g/>
též	též	k9	též
řecká	řecký	k2eAgFnSc1d1	řecká
abeceda	abeceda	k1gFnSc1	abeceda
nebo	nebo	k8xC	nebo
alfabeta	alfabeta	k1gFnSc1	alfabeta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgNnSc1d3	nejstarší
dosud	dosud	k6eAd1	dosud
používané	používaný	k2eAgNnSc1d1	používané
písmo	písmo	k1gNnSc1	písmo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
řečtiny	řečtina	k1gFnSc2	řečtina
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
koptštiny	koptština	k1gFnSc2	koptština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k9	i
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
však	však	k9	však
pronikají	pronikat	k5eAaImIp3nP	pronikat
řecká	řecký	k2eAgNnPc1d1	řecké
písmena	písmeno	k1gNnPc1	písmeno
např.	např.	kA	např.
jako	jako	k8xS	jako
matematické	matematický	k2eAgInPc4d1	matematický
symboly	symbol	k1gInPc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Písmo	písmo	k1gNnSc1	písmo
bylo	být	k5eAaImAgNnS	být
vyvinuto	vyvinout	k5eAaPmNgNnS	vyvinout
v	v	k7c6	v
geometrickém	geometrický	k2eAgNnSc6d1	geometrické
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c6	na
základě	základ	k1gInSc6	základ
fénického	fénický	k2eAgNnSc2d1	fénické
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
několika	několik	k4yIc7	několik
změnami	změna	k1gFnPc7	změna
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
alfabety	alfabeta	k1gFnSc2	alfabeta
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
vychází	vycházet	k5eAaImIp3nS	vycházet
i	i	k9	i
slovanské	slovanský	k2eAgNnSc1d1	slovanské
písmo	písmo	k1gNnSc1	písmo
cyrilice	cyrilice	k1gFnSc2	cyrilice
a	a	k8xC	a
nezpochybnitelnou	zpochybnitelný	k2eNgFnSc4d1	nezpochybnitelná
příbuznost	příbuznost	k1gFnSc4	příbuznost
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
i	i	k8xC	i
námi	my	k3xPp1nPc7	my
používaná	používaný	k2eAgFnSc1d1	používaná
latinka	latinka	k1gFnSc1	latinka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kódování	kódování	k1gNnSc6	kódování
Unicode	Unicod	k1gInSc5	Unicod
jsou	být	k5eAaImIp3nP	být
znaky	znak	k1gInPc1	znak
řeckého	řecký	k2eAgNnSc2d1	řecké
písma	písmo	k1gNnSc2	písmo
umístěny	umístěn	k2eAgFnPc1d1	umístěna
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
0	[number]	k4	0
<g/>
370	[number]	k4	370
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
FF	ff	kA	ff
(	(	kIx(	(
<g/>
hexadecimálně	hexadecimálně	k6eAd1	hexadecimálně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Pro	pro	k7c4	pro
velká	velký	k2eAgNnPc4d1	velké
písmena	písmeno	k1gNnPc4	písmeno
řecké	řecký	k2eAgFnSc2d1	řecká
abecedy	abeceda	k1gFnSc2	abeceda
v	v	k7c6	v
HTML	HTML	kA	HTML
napište	napsat	k5eAaBmRp2nP	napsat
první	první	k4xOgNnSc4	první
písmeno	písmeno	k1gNnSc4	písmeno
za	za	k7c4	za
&	&	k?	&
velké	velká	k1gFnPc4	velká
<g/>
.	.	kIx.	.
</s>
