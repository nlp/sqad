<p>
<s>
Karbalá	Karbalý	k2eAgFnSc1d1	Karbalá
je	být	k5eAaImIp3nS	být
irácké	irácký	k2eAgNnSc4d1	irácké
město	město	k1gNnSc4	město
asi	asi	k9	asi
100	[number]	k4	100
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Bagdádu	Bagdád	k1gInSc2	Bagdád
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
guvernorátu	guvernorát	k1gInSc2	guvernorát
Karbalá	Karbalý	k2eAgFnSc1d1	Karbalá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
1	[number]	k4	1
066	[number]	k4	066
600	[number]	k4	600
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
680	[number]	k4	680
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
odehrála	odehrát	k5eAaPmAgFnS	odehrát
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Karbalá	Karbalý	k2eAgFnSc1d1	Karbalá
mezi	mezi	k7c7	mezi
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
šíítské	šíítský	k2eAgMnPc4d1	šíítský
muslimy	muslim	k1gMnPc4	muslim
<g/>
)	)	kIx)	)
třetím	třetí	k4xOgMnSc6	třetí
imámem	imám	k1gMnSc7	imám
Husajnem	Husajn	k1gMnSc7	Husajn
ibn	ibn	k?	ibn
Alím	Alím	k1gInSc1	Alím
a	a	k8xC	a
Jazídem	Jazíd	k1gInSc7	Jazíd
I.	I.	kA	I.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
Husajn	Husajn	k1gMnSc1	Husajn
i	i	k9	i
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
padl	padnout	k5eAaPmAgMnS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
Karbalá	Karbalý	k2eAgFnSc1d1	Karbalá
pro	pro	k7c4	pro
šíity	šíita	k1gMnPc4	šíita
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejsvětějších	nejsvětější	k2eAgNnPc2d1	nejsvětější
měst	město	k1gNnPc2	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
mimo	mimo	k7c4	mimo
Mekku	Mekka	k1gFnSc4	Mekka
<g/>
,	,	kIx,	,
Medínu	Medína	k1gFnSc4	Medína
a	a	k8xC	a
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Svatyně	svatyně	k1gFnPc1	svatyně
imáma	imám	k1gMnSc2	imám
Husajna	Husajn	k1gMnSc2	Husajn
<g/>
.	.	kIx.	.
</s>
</p>
