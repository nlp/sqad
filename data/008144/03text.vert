<s>
Sjednocení	sjednocení	k1gNnSc1	sjednocení
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
politickému	politický	k2eAgNnSc3d1	politické
a	a	k8xC	a
administrativnímu	administrativní	k2eAgNnSc3d1	administrativní
sjednocení	sjednocení	k1gNnSc3	sjednocení
německých	německý	k2eAgInPc2d1	německý
států	stát	k1gInPc2	stát
do	do	k7c2	do
národního	národní	k2eAgInSc2d1	národní
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1871	[number]	k4	1871
v	v	k7c6	v
zrcadlové	zrcadlový	k2eAgFnSc6d1	zrcadlová
síni	síň	k1gFnSc6	síň
paláce	palác	k1gInSc2	palác
Versailles	Versailles	k1gFnSc2	Versailles
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
německá	německý	k2eAgNnPc4d1	německé
knížata	kníže	k1gNnPc4	kníže
slavnostně	slavnostně	k6eAd1	slavnostně
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
Viléma	Vilém	k1gMnSc4	Vilém
I.	I.	kA	I.
Pruského	pruský	k2eAgMnSc4d1	pruský
německým	německý	k2eAgMnSc7d1	německý
císařem	císař	k1gMnSc7	císař
po	po	k7c6	po
kapitulaci	kapitulace	k1gFnSc6	kapitulace
Francie	Francie	k1gFnSc2	Francie
v	v	k7c6	v
prusko-francouzské	pruskorancouzský	k2eAgFnSc6d1	prusko-francouzská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Sjednocení	sjednocení	k1gNnSc1	sjednocení
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
většina	většina	k1gFnSc1	většina
německy	německy	k6eAd1	německy
mluvícího	mluvící	k2eAgNnSc2d1	mluvící
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
spolkové	spolkový	k2eAgFnSc2d1	spolková
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
období	období	k1gNnSc6	období
jednoho	jeden	k4xCgInSc2	jeden
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
po	po	k7c6	po
Prešpurském	prešpurský	k2eAgInSc6d1	prešpurský
míru	mír	k1gInSc6	mír
a	a	k8xC	a
abdikaci	abdikace	k1gFnSc6	abdikace
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
II	II	kA	II
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
během	během	k7c2	během
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
Rýnským	rýnský	k2eAgInSc7d1	rýnský
spolkem	spolek	k1gInSc7	spolek
pod	pod	k7c7	pod
protektorátem	protektorát	k1gInSc7	protektorát
Napoleona	Napoleon	k1gMnSc2	Napoleon
I.	I.	kA	I.
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Napoleona	Napoleon	k1gMnSc2	Napoleon
I.	I.	kA	I.
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
a	a	k8xC	a
1815	[number]	k4	1815
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
kongresu	kongres	k1gInSc6	kongres
založen	založit	k5eAaPmNgInS	založit
velmi	velmi	k6eAd1	velmi
volný	volný	k2eAgInSc1d1	volný
Německý	německý	k2eAgInSc1d1	německý
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
sněmy	sněm	k1gInPc1	sněm
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
pod	pod	k7c7	pod
předsednictvím	předsednictví	k1gNnSc7	předsednictví
rakouského	rakouský	k2eAgMnSc2d1	rakouský
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Model	modla	k1gFnPc2	modla
"	"	kIx"	"
<g/>
sfér	sféra	k1gFnPc2	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
"	"	kIx"	"
a	a	k8xC	a
rovnováhy	rovnováha	k1gFnSc2	rovnováha
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
na	na	k7c6	na
Vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1814	[number]	k4	1814
a	a	k8xC	a
1815	[number]	k4	1815
po	po	k7c6	po
napoleonských	napoleonský	k2eAgFnPc6d1	napoleonská
válkách	válka	k1gFnPc6	válka
<g/>
,	,	kIx,	,
učinil	učinit	k5eAaPmAgMnS	učinit
Rakouské	rakouský	k2eAgNnSc4d1	rakouské
císařství	císařství	k1gNnSc4	císařství
dominantní	dominantní	k2eAgFnSc7d1	dominantní
mocností	mocnost	k1gFnSc7	mocnost
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
však	však	k9	však
nebrala	brát	k5eNaImAgFnS	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
narůstající	narůstající	k2eAgFnSc4d1	narůstající
sílu	síla	k1gFnSc4	síla
Pruska	Prusko	k1gNnSc2	Prusko
a	a	k8xC	a
neuvědomila	uvědomit	k5eNaPmAgFnS	uvědomit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prusko	Prusko	k1gNnSc1	Prusko
může	moct	k5eAaImIp3nS	moct
konkurovat	konkurovat	k5eAaImF	konkurovat
Rakousku	Rakousko	k1gNnSc3	Rakousko
jako	jako	k8xC	jako
vůdčí	vůdčí	k2eAgFnSc4d1	vůdčí
mocnost	mocnost	k1gFnSc4	mocnost
mezi	mezi	k7c7	mezi
německými	německý	k2eAgInPc7d1	německý
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
německý	německý	k2eAgInSc1d1	německý
dualismus	dualismus	k1gInSc1	dualismus
nabízel	nabízet	k5eAaImAgMnS	nabízet
dvojí	dvojí	k4xRgNnSc4	dvojí
řešení	řešení	k1gNnSc4	řešení
národního	národní	k2eAgNnSc2d1	národní
sjednocení	sjednocení	k1gNnSc2	sjednocení
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
variantu	varianta	k1gFnSc4	varianta
vylučující	vylučující	k2eAgFnSc4d1	vylučující
jak	jak	k8xS	jak
Rakousko	Rakousko	k1gNnSc1	Rakousko
tak	tak	k8xC	tak
Prusko	Prusko	k1gNnSc1	Prusko
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Maloněmecká	maloněmecký	k2eAgFnSc1d1	maloněmecká
koncepce	koncepce	k1gFnSc1	koncepce
(	(	kIx(	(
<g/>
Kleindeutsche	Kleindeutsche	k1gFnSc1	Kleindeutsche
Lösung	Lösung	k1gMnSc1	Lösung
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
malé	malý	k2eAgNnSc1d1	malé
Německo	Německo	k1gNnSc1	Německo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nezahrnovalo	zahrnovat	k5eNaImAgNnS	zahrnovat
rakouské	rakouský	k2eAgFnPc4d1	rakouská
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
Velkoněmecká	velkoněmecký	k2eAgFnSc1d1	Velkoněmecká
koncepce	koncepce	k1gFnSc1	koncepce
(	(	kIx(	(
<g/>
Großdeutsche	Großdeutsche	k1gFnSc1	Großdeutsche
Lösung	Lösung	k1gMnSc1	Lösung
<g/>
)	)	kIx)	)
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
velké	velký	k2eAgNnSc1d1	velké
Německo	Německo	k1gNnSc1	Německo
<g/>
"	"	kIx"	"
se	s	k7c7	s
zahrnutím	zahrnutí	k1gNnSc7	zahrnutí
rakouských	rakouský	k2eAgFnPc2d1	rakouská
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k9	i
českých	český	k2eAgFnPc2d1	Česká
<g/>
)	)	kIx)	)
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
koncepce	koncepce	k1gFnSc1	koncepce
třetího	třetí	k4xOgNnSc2	třetí
Německa	Německo	k1gNnSc2	Německo
tj.	tj.	kA	tj.
Německa	Německo	k1gNnSc2	Německo
bez	bez	k7c2	bez
Pruska	Prusko	k1gNnSc2	Prusko
i	i	k8xC	i
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
se	se	k3xPyFc4	se
však	však	k9	však
téměř	téměř	k6eAd1	téměř
neujala	ujmout	k5eNaPmAgFnS	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
německé	německý	k2eAgFnPc1d1	německá
země	zem	k1gFnPc1	zem
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnPc1d1	římská
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
politických	politický	k2eAgFnPc2d1	politická
entit	entita	k1gFnPc2	entita
<g/>
,	,	kIx,	,
od	od	k7c2	od
říšských	říšský	k2eAgNnPc2d1	říšské
měst	město	k1gNnPc2	město
a	a	k8xC	a
drobných	drobný	k2eAgNnPc2d1	drobné
panství	panství	k1gNnPc2	panství
až	až	k9	až
po	po	k7c4	po
velká	velký	k2eAgNnPc4d1	velké
království	království	k1gNnPc4	království
jako	jako	k8xS	jako
Bavorsko	Bavorsko	k1gNnSc4	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Organizovány	organizován	k2eAgInPc4d1	organizován
do	do	k7c2	do
"	"	kIx"	"
<g/>
říšských	říšský	k2eAgInPc2d1	říšský
krajů	kraj	k1gInPc2	kraj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Reichskreise	Reichskreise	k1gFnSc1	Reichskreise
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skupiny	skupina	k1gFnSc2	skupina
států	stát	k1gInPc2	stát
koordinovaly	koordinovat	k5eAaBmAgFnP	koordinovat
regionální	regionální	k2eAgFnPc1d1	regionální
a	a	k8xC	a
organizační	organizační	k2eAgInPc1d1	organizační
zájmy	zájem	k1gInPc1	zájem
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
spolupráce	spolupráce	k1gFnSc2	spolupráce
a	a	k8xC	a
vojenské	vojenský	k2eAgFnSc2d1	vojenská
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
kurfiřti	kurfiřt	k1gMnPc1	kurfiřt
volili	volit	k5eAaImAgMnP	volit
-	-	kIx~	-
s	s	k7c7	s
několika	několik	k4yIc7	několik
výjimkami	výjimka	k1gFnPc7	výjimka
–	–	k?	–
za	za	k7c2	za
císaře	císař	k1gMnSc2	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
hlavy	hlava	k1gFnSc2	hlava
dynastie	dynastie	k1gFnSc2	dynastie
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
druhé	druhý	k4xOgFnSc2	druhý
koalice	koalice	k1gFnSc2	koalice
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1799	[number]	k4	1799
až	až	k8xS	až
1802	[number]	k4	1802
skončila	skončit	k5eAaPmAgFnS	skončit
porážkou	porážka	k1gFnSc7	porážka
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
vojsk	vojsko	k1gNnPc2	vojsko
Napoleonem	Napoleon	k1gMnSc7	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
a	a	k8xC	a
smlouvami	smlouva	k1gFnPc7	smlouva
z	z	k7c2	z
Luneville	Luneville	k1gFnSc2	Luneville
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
a	a	k8xC	a
smlouvou	smlouva	k1gFnSc7	smlouva
z	z	k7c2	z
Amiens	Amiens	k1gInSc1	Amiens
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1806	[number]	k4	1806
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Pruska	Prusko	k1gNnSc2	Prusko
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Jeny	jen	k1gInPc1	jen
a	a	k8xC	a
u	u	k7c2	u
Auerstedtu	Auerstedt	k1gInSc2	Auerstedt
Napoleon	Napoleon	k1gMnSc1	Napoleon
diktoval	diktovat	k5eAaImAgMnS	diktovat
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
kterým	který	k3yIgInPc3	který
byla	být	k5eAaImAgFnS	být
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
rostl	růst	k5eAaImAgInS	růst
také	také	k9	také
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
zemích	zem	k1gFnPc6	zem
nacionalismus	nacionalismus	k1gInSc4	nacionalismus
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
jako	jako	k9	jako
hnutí	hnutí	k1gNnSc1	hnutí
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
francouzské	francouzský	k2eAgFnSc3d1	francouzská
nadvládě	nadvláda	k1gFnSc3	nadvláda
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
hnutí	hnutí	k1gNnSc4	hnutí
za	za	k7c4	za
moderní	moderní	k2eAgInSc4d1	moderní
a	a	k8xC	a
jednotný	jednotný	k2eAgInSc4d1	jednotný
německý	německý	k2eAgInSc4d1	německý
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
studentská	studentský	k2eAgFnSc1d1	studentská
manifestace	manifestace	k1gFnSc1	manifestace
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Wartburgu	Wartburg	k1gInSc2	Wartburg
<g/>
.	.	kIx.	.
</s>
<s>
Rakouský	rakouský	k2eAgMnSc1d1	rakouský
kancléř	kancléř	k1gMnSc1	kancléř
kníže	kníže	k1gMnSc1	kníže
Metternich	Metternich	k1gMnSc1	Metternich
sice	sice	k8xC	sice
nacionalistické	nacionalistický	k2eAgInPc1d1	nacionalistický
studentské	studentský	k2eAgInPc1d1	studentský
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
spolky	spolek	k1gInPc1	spolek
Karlovarskými	karlovarský	k2eAgInPc7d1	karlovarský
dekrety	dekret	k1gInPc7	dekret
(	(	kIx(	(
<g/>
1819	[number]	k4	1819
<g/>
)	)	kIx)	)
zakázal	zakázat	k5eAaPmAgInS	zakázat
a	a	k8xC	a
utužil	utužit	k5eAaPmAgInS	utužit
cenzuru	cenzura	k1gFnSc4	cenzura
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
"	"	kIx"	"
<g/>
předbřeznová	předbřeznový	k2eAgFnSc1d1	předbřeznová
doba	doba	k1gFnSc1	doba
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Vormärz	Vormärz	k1gInSc1	Vormärz
<g/>
)	)	kIx)	)
však	však	k9	však
neměla	mít	k5eNaImAgFnS	mít
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
trvání	trvání	k1gNnSc4	trvání
<g/>
.	.	kIx.	.
</s>
<s>
Nacionálně-liberální	Nacionálněiberální	k2eAgFnSc1d1	Nacionálně-liberální
revoluce	revoluce	k1gFnSc1	revoluce
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
stoupence	stoupenka	k1gFnSc6	stoupenka
sjednocení	sjednocení	k1gNnSc1	sjednocení
a	a	k8xC	a
modernizace	modernizace	k1gFnSc1	modernizace
povzbudila	povzbudit	k5eAaPmAgFnS	povzbudit
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
velká	velký	k2eAgFnSc1d1	velká
národní	národní	k2eAgFnSc1d1	národní
manifestace	manifestace	k1gFnSc1	manifestace
v	v	k7c6	v
bavorském	bavorský	k2eAgInSc6d1	bavorský
Hambachu	Hambach	k1gInSc6	Hambach
a	a	k8xC	a
revoluce	revoluce	k1gFnSc1	revoluce
1848	[number]	k4	1848
způsobila	způsobit	k5eAaPmAgFnS	způsobit
Metternichův	Metternichův	k2eAgInSc4d1	Metternichův
pád	pád	k1gInSc4	pád
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
politickým	politický	k2eAgInSc7d1	politický
cílem	cíl	k1gInSc7	cíl
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
silné	silný	k2eAgFnSc2d1	silná
zejména	zejména	k9	zejména
v	v	k7c6	v
Porýní	Porýní	k1gNnSc6	Porýní
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
společná	společný	k2eAgFnSc1d1	společná
německá	německý	k2eAgFnSc1d1	německá
ústava	ústava	k1gFnSc1	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
byl	být	k5eAaImAgInS	být
svolán	svolat	k5eAaPmNgInS	svolat
do	do	k7c2	do
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
"	"	kIx"	"
<g/>
výbor	výbor	k1gInSc1	výbor
padesáti	padesát	k4xCc7	padesát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
připravit	připravit	k5eAaPmF	připravit
ústavu	ústav	k1gInSc2	ústav
a	a	k8xC	a
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Pozván	pozvat	k5eAaPmNgMnS	pozvat
byl	být	k5eAaImAgMnS	být
také	také	k9	také
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
však	však	k9	však
pozvání	pozvánět	k5eAaImIp3nS	pozvánět
známým	známý	k2eAgInSc7d1	známý
Listem	list	k1gInSc7	list
do	do	k7c2	do
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vypsat	vypsat	k5eAaPmF	vypsat
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jejich	jejich	k3xOp3gFnSc4	jejich
podobu	podoba	k1gFnSc4	podoba
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgMnPc6d1	jednotlivý
panovnících	panovník	k1gMnPc6	panovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
česky	česky	k6eAd1	česky
mluvících	mluvící	k2eAgInPc6d1	mluvící
krajích	kraj	k1gInPc6	kraj
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
se	se	k3xPyFc4	se
nekonaly	konat	k5eNaImAgInP	konat
<g/>
.	.	kIx.	.
</s>
<s>
Zvolený	zvolený	k2eAgInSc4d1	zvolený
parlament	parlament	k1gInSc4	parlament
se	se	k3xPyFc4	se
sešel	sejít	k5eAaPmAgMnS	sejít
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
opět	opět	k6eAd1	opět
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
dědičný	dědičný	k2eAgInSc4d1	dědičný
císařský	císařský	k2eAgInSc4d1	císařský
titul	titul	k1gInSc4	titul
pruskému	pruský	k2eAgMnSc3d1	pruský
králi	král	k1gMnSc3	král
Fridrichu	Fridrich	k1gMnSc3	Fridrich
Vilémovi	Vilém	k1gMnSc3	Vilém
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
nabídku	nabídka	k1gFnSc4	nabídka
z	z	k7c2	z
obav	obava	k1gFnPc2	obava
před	před	k7c7	před
knížaty	kníže	k1gNnPc7	kníže
(	(	kIx(	(
<g/>
kteří	který	k3yRgMnPc1	který
většinou	většinou	k6eAd1	většinou
liberální	liberální	k2eAgFnSc4d1	liberální
ústavu	ústava	k1gFnSc4	ústava
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Nemohl	moct	k5eNaImAgMnS	moct
také	také	k9	také
přijmout	přijmout	k5eAaPmF	přijmout
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
císařem	císař	k1gMnSc7	císař
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
když	když	k8xS	když
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
vládl	vládnout	k5eAaImAgMnS	vládnout
jako	jako	k9	jako
panovník	panovník	k1gMnSc1	panovník
z	z	k7c2	z
Boží	boží	k2eAgFnSc2d1	boží
milosti	milost	k1gFnSc2	milost
<g/>
.	.	kIx.	.
</s>
<s>
Sněm	sněm	k1gInSc1	sněm
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
maloněmeckou	maloněmecký	k2eAgFnSc4d1	maloněmecká
koncepci	koncepce	k1gFnSc4	koncepce
<g/>
"	"	kIx"	"
s	s	k7c7	s
vyloučením	vyloučení	k1gNnSc7	vyloučení
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
však	však	k9	však
dále	daleko	k6eAd2	daleko
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
dohodnout	dohodnout	k5eAaPmF	dohodnout
<g/>
,	,	kIx,	,
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
opět	opět	k6eAd1	opět
revoluce	revoluce	k1gFnSc1	revoluce
a	a	k8xC	a
sněm	sněm	k1gInSc1	sněm
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
rozehnán	rozehnat	k5eAaPmNgInS	rozehnat
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
diplomatických	diplomatický	k2eAgNnPc6d1	diplomatické
jednáních	jednání	k1gNnPc6	jednání
se	se	k3xPyFc4	se
Prusku	Prusko	k1gNnSc6	Prusko
nepodařilo	podařit	k5eNaPmAgNnS	podařit
problém	problém	k1gInSc4	problém
sjednocení	sjednocení	k1gNnSc2	sjednocení
vyřešit	vyřešit	k5eAaPmF	vyřešit
a	a	k8xC	a
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
muselo	muset	k5eAaImAgNnS	muset
myšlenky	myšlenka	k1gFnPc1	myšlenka
sjednocení	sjednocení	k1gNnSc4	sjednocení
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Prudký	prudký	k2eAgInSc1d1	prudký
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
a	a	k8xC	a
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
rozvoj	rozvoj	k1gInSc1	rozvoj
německých	německý	k2eAgFnPc2d1	německá
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ovšem	ovšem	k9	ovšem
vznik	vznik	k1gInSc4	vznik
takového	takový	k3xDgInSc2	takový
moderního	moderní	k2eAgInSc2d1	moderní
státu	stát	k1gInSc2	stát
nutně	nutně	k6eAd1	nutně
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
i	i	k8xC	i
podporoval	podporovat	k5eAaImAgInS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Pruská	pruský	k2eAgFnSc1d1	pruská
celní	celní	k2eAgFnSc1d1	celní
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
Zollverein	Zollverein	k1gInSc1	Zollverein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spojila	spojit	k5eAaPmAgFnS	spojit
všechny	všechen	k3xTgFnPc4	všechen
Hohenzollernské	hohenzollernský	k2eAgFnPc4d1	hohenzollernský
země	zem	k1gFnPc4	zem
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
i	i	k8xC	i
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
silnic	silnice	k1gFnPc2	silnice
se	s	k7c7	s
zpevněným	zpevněný	k2eAgInSc7d1	zpevněný
povrchem	povrch	k1gInSc7	povrch
činila	činit	k5eAaImAgFnS	činit
roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
3	[number]	k4	3
800	[number]	k4	800
km	km	kA	km
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1852	[number]	k4	1852
16	[number]	k4	16
600	[number]	k4	600
km	km	kA	km
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
vícevrstvé	vícevrstvý	k2eAgFnSc6d1	vícevrstvá
technologii	technologie	k1gFnSc6	technologie
(	(	kIx(	(
<g/>
macadam	macadam	k1gInSc1	macadam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
síť	síť	k1gFnSc4	síť
kanálů	kanál	k1gInPc2	kanál
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
po	po	k7c6	po
nich	on	k3xPp3gInPc2	on
a	a	k8xC	a
po	po	k7c6	po
řekách	řeka	k1gFnPc6	řeka
plulo	plout	k5eAaImAgNnS	plout
180	[number]	k4	180
parníků	parník	k1gInPc2	parník
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
otevřena	otevřít	k5eAaPmNgFnS	otevřít
první	první	k4xOgFnSc1	první
železnice	železnice	k1gFnSc1	železnice
(	(	kIx(	(
<g/>
Norimberk	Norimberk	k1gInSc1	Norimberk
–	–	k?	–
Fürth	Fürth	k1gInSc1	Fürth
<g/>
)	)	kIx)	)
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
6	[number]	k4	6
km	km	kA	km
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
měly	mít	k5eAaImAgFnP	mít
německé	německý	k2eAgFnPc1d1	německá
železnice	železnice	k1gFnPc1	železnice
462	[number]	k4	462
km	km	kA	km
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1860	[number]	k4	1860
11	[number]	k4	11
157	[number]	k4	157
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
už	už	k9	už
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
německých	německý	k2eAgNnPc2d1	německé
měst	město	k1gNnPc2	město
napojena	napojen	k2eAgFnSc1d1	napojena
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgInPc1d1	vojenský
úspěchy	úspěch	k1gInPc1	úspěch
v	v	k7c6	v
Prusko-dánské	pruskoánský	k2eAgFnSc6d1	prusko-dánský
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Prusko-rakouské	pruskoakouský	k2eAgFnSc6d1	prusko-rakouská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
a	a	k8xC	a
zejména	zejména	k9	zejména
porážka	porážka	k1gFnSc1	porážka
Francie	Francie	k1gFnSc2	Francie
v	v	k7c6	v
Prusko-francouzské	pruskorancouzský	k2eAgFnSc6d1	prusko-francouzská
válce	válka	k1gFnSc6	válka
1870	[number]	k4	1870
připravily	připravit	k5eAaPmAgFnP	připravit
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
sjednocení	sjednocení	k1gNnSc4	sjednocení
<g/>
.	.	kIx.	.
</s>
<s>
Vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
nadšení	nadšení	k1gNnSc4	nadšení
a	a	k8xC	a
národní	národní	k2eAgFnSc4d1	národní
hrdost	hrdost	k1gFnSc4	hrdost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mohli	moct	k5eAaImAgMnP	moct
politici	politik	k1gMnPc1	politik
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
politickému	politický	k2eAgNnSc3d1	politické
sjednocení	sjednocení	k1gNnSc3	sjednocení
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
úlohy	úloha	k1gFnSc2	úloha
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
pruský	pruský	k2eAgMnSc1d1	pruský
kancléř	kancléř	k1gMnSc1	kancléř
Otto	Otto	k1gMnSc1	Otto
von	von	k1gInSc4	von
Bismarck	Bismarck	k1gMnSc1	Bismarck
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
říše	říše	k1gFnSc1	říše
jako	jako	k8xC	jako
jednotné	jednotný	k2eAgNnSc1d1	jednotné
císařství	císařství	k1gNnSc1	císařství
s	s	k7c7	s
Vilémem	Vilém	k1gMnSc7	Vilém
I.	I.	kA	I.
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
nevznikla	vzniknout	k5eNaPmAgFnS	vzniknout
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
voleného	volený	k2eAgInSc2d1	volený
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
slavnostním	slavnostní	k2eAgNnSc7d1	slavnostní
provoláním	provolání	k1gNnSc7	provolání
německých	německý	k2eAgMnPc2d1	německý
knížat	kníže	k1gMnPc2wR	kníže
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jí	on	k3xPp3gFnSc3	on
dalo	dát	k5eAaPmAgNnS	dát
její	její	k3xOp3gInSc4	její
konzervativní	konzervativní	k2eAgInSc4d1	konzervativní
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
v	v	k7c6	v
někdejším	někdejší	k2eAgNnSc6d1	někdejší
sídle	sídlo	k1gNnSc6	sídlo
francouzských	francouzský	k2eAgMnPc2d1	francouzský
králů	král	k1gMnPc2	král
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
,	,	kIx,	,
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
k	k	k7c3	k
budoucímu	budoucí	k2eAgNnSc3d1	budoucí
německo-francouzskému	německorancouzský	k2eAgNnSc3d1	německo-francouzské
napětí	napětí	k1gNnSc3	napětí
a	a	k8xC	a
rivalitě	rivalita	k1gFnSc3	rivalita
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
"	"	kIx"	"
<g/>
maloněmecké	maloněmecký	k2eAgFnSc3d1	maloněmecká
<g/>
"	"	kIx"	"
sjednocení	sjednocení	k1gNnSc1	sjednocení
Německa	Německo	k1gNnSc2	Německo
bez	bez	k7c2	bez
Rakouska	Rakousko	k1gNnSc2	Rakousko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
alespoň	alespoň	k9	alespoň
dočasně	dočasně	k6eAd1	dočasně
vyřešilo	vyřešit	k5eAaPmAgNnS	vyřešit
otázku	otázka	k1gFnSc4	otázka
dualismu	dualismus	k1gInSc2	dualismus
a	a	k8xC	a
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
neobyčejně	obyčejně	k6eNd1	obyčejně
mocný	mocný	k2eAgInSc4d1	mocný
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Důsledky	důsledek	k1gInPc1	důsledek
tohoto	tento	k3xDgInSc2	tento
kroku	krok	k1gInSc2	krok
pak	pak	k6eAd1	pak
silně	silně	k6eAd1	silně
poznamenaly	poznamenat	k5eAaPmAgFnP	poznamenat
evropské	evropský	k2eAgFnPc1d1	Evropská
dějiny	dějiny	k1gFnPc1	dějiny
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
