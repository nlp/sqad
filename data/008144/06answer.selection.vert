<s>
Sjednocení	sjednocení	k1gNnSc1	sjednocení
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
politickému	politický	k2eAgNnSc3d1	politické
a	a	k8xC	a
administrativnímu	administrativní	k2eAgNnSc3d1	administrativní
sjednocení	sjednocení	k1gNnSc3	sjednocení
německých	německý	k2eAgInPc2d1	německý
států	stát	k1gInPc2	stát
do	do	k7c2	do
národního	národní	k2eAgInSc2d1	národní
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1871	[number]	k4	1871
v	v	k7c6	v
zrcadlové	zrcadlový	k2eAgFnSc6d1	zrcadlová
síni	síň	k1gFnSc6	síň
paláce	palác	k1gInSc2	palác
Versailles	Versailles	k1gFnSc2	Versailles
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
německá	německý	k2eAgNnPc4d1	německé
knížata	kníže	k1gNnPc4	kníže
slavnostně	slavnostně	k6eAd1	slavnostně
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
Viléma	Vilém	k1gMnSc4	Vilém
I.	I.	kA	I.
Pruského	pruský	k2eAgMnSc4d1	pruský
německým	německý	k2eAgMnSc7d1	německý
císařem	císař	k1gMnSc7	císař
po	po	k7c6	po
kapitulaci	kapitulace	k1gFnSc6	kapitulace
Francie	Francie	k1gFnSc2	Francie
v	v	k7c6	v
prusko-francouzské	pruskorancouzský	k2eAgFnSc6d1	prusko-francouzská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
