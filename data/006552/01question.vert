<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
molekula	molekula	k1gFnSc1	molekula
DNA	DNA	kA	DNA
má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgInSc1	jeden
řetězec	řetězec	k1gInSc1	řetězec
z	z	k7c2	z
molekuly	molekula	k1gFnSc2	molekula
původní	původní	k2eAgInSc4d1	původní
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
nový	nový	k2eAgInSc4d1	nový
<g/>
?	?	kIx.	?
</s>
