<s>
Celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
semikonzervativní	semikonzervativní	k2eAgMnSc1d1	semikonzervativní
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
každá	každý	k3xTgFnSc1	každý
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
molekula	molekula	k1gFnSc1	molekula
DNA	DNA	kA	DNA
má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgInSc1	jeden
řetězec	řetězec	k1gInSc1	řetězec
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
molekuly	molekula	k1gFnSc2	molekula
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
nový	nový	k2eAgMnSc1d1	nový
<g/>
,	,	kIx,	,
syntetizovaný	syntetizovaný	k2eAgInSc1d1	syntetizovaný
<g/>
.	.	kIx.	.
</s>
