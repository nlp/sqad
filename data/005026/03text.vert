<s>
Cyklon	cyklon	k1gInSc1	cyklon
B	B	kA	B
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Zyklon	Zyklon	k1gInSc1	Zyklon
B	B	kA	B
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obchodní	obchodní	k2eAgInSc4d1	obchodní
název	název	k1gInSc4	název
insekticidu	insekticid	k1gInSc2	insekticid
německé	německý	k2eAgFnSc2d1	německá
firmy	firma	k1gFnSc2	firma
IG	IG	kA	IG
Farben	Farbna	k1gFnPc2	Farbna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
granulovaná	granulovaný	k2eAgFnSc1d1	granulovaná
křemelina	křemelina	k1gFnSc1	křemelina
nasycená	nasycený	k2eAgFnSc1d1	nasycená
kyanovodíkem	kyanovodík	k1gInSc7	kyanovodík
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
obalu	obal	k1gInSc2	obal
začal	začít	k5eAaPmAgInS	začít
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
plynný	plynný	k2eAgInSc1d1	plynný
kyanovodík	kyanovodík	k1gInSc1	kyanovodík
(	(	kIx(	(
<g/>
HCN	HCN	kA	HCN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
původní	původní	k2eAgNnSc1d1	původní
zamýšlené	zamýšlený	k2eAgNnSc1d1	zamýšlené
použití	použití	k1gNnSc1	použití
spočívalo	spočívat	k5eAaImAgNnS	spočívat
v	v	k7c6	v
dezinfekci	dezinfekce	k1gFnSc6	dezinfekce
a	a	k8xC	a
dezinsekci	dezinsekce	k1gFnSc6	dezinsekce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
nástroj	nástroj	k1gInSc1	nástroj
genocidy	genocida	k1gFnSc2	genocida
v	v	k7c6	v
plynových	plynový	k2eAgFnPc6d1	plynová
komorách	komora	k1gFnPc6	komora
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
táborech	tábor	k1gInPc6	tábor
Auschwitz-Birkenau	Auschwitz-Birkenaus	k1gInSc2	Auschwitz-Birkenaus
a	a	k8xC	a
Majdanek	Majdanka	k1gFnPc2	Majdanka
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovým	klíčový	k2eAgMnSc7d1	klíčový
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
optimalizací	optimalizace	k1gFnSc7	optimalizace
zabíjení	zabíjení	k1gNnSc2	zabíjení
Cyklonem	cyklon	k1gInSc7	cyklon
B	B	kA	B
ve	v	k7c6	v
vyhlazovacích	vyhlazovací	k2eAgInPc6d1	vyhlazovací
táborech	tábor	k1gInPc6	tábor
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
důstojník	důstojník	k1gMnSc1	důstojník
SS3	SS3	k1gMnSc1	SS3
Neo	Neo	k1gMnSc1	Neo
Kurt	Kurt	k1gMnSc1	Kurt
Gerstein	Gerstein	k1gMnSc1	Gerstein
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
měl	mít	k5eAaImAgInS	mít
zvýšit	zvýšit	k5eAaPmF	zvýšit
jeho	jeho	k3xOp3gFnSc4	jeho
smrtící	smrtící	k2eAgFnSc4d1	smrtící
efektivitu	efektivita	k1gFnSc4	efektivita
a	a	k8xC	a
zkrátit	zkrátit	k5eAaPmF	zkrátit
jak	jak	k8xC	jak
dobu	doba	k1gFnSc4	doba
působení	působení	k1gNnSc2	působení
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
čištění	čištění	k1gNnSc2	čištění
"	"	kIx"	"
<g/>
po	po	k7c6	po
použití	použití	k1gNnSc6	použití
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Gerstein	Gerstein	k1gMnSc1	Gerstein
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
staral	starat	k5eAaImAgMnS	starat
o	o	k7c4	o
dezinfekci	dezinfekce	k1gFnSc4	dezinfekce
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
s	s	k7c7	s
hromadným	hromadný	k2eAgNnSc7d1	hromadné
vyvražďováním	vyvražďování	k1gNnSc7	vyvražďování
nesouhlasil	souhlasit	k5eNaImAgInS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
ale	ale	k9	ale
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
tuto	tento	k3xDgFnSc4	tento
informaci	informace	k1gFnSc4	informace
vyvést	vyvést	k5eAaPmF	vyvést
z	z	k7c2	z
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
do	do	k7c2	do
Vatikánu	Vatikán	k1gInSc2	Vatikán
k	k	k7c3	k
papeži	papež	k1gMnSc3	papež
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
francouzské	francouzský	k2eAgFnSc3d1	francouzská
armádě	armáda	k1gFnSc3	armáda
a	a	k8xC	a
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
sepsal	sepsat	k5eAaPmAgMnS	sepsat
tzv.	tzv.	kA	tzv.
Gersteinovu	Gersteinův	k2eAgFnSc4d1	Gersteinův
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podrobně	podrobně	k6eAd1	podrobně
popisuje	popisovat	k5eAaImIp3nS	popisovat
použití	použití	k1gNnSc1	použití
Cyklonu	cyklon	k1gInSc2	cyklon
B.	B.	kA	B.
Producentem	producent	k1gMnSc7	producent
Cyklonu	cyklon	k1gInSc2	cyklon
B	B	kA	B
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
Deutsche	Deutsch	k1gFnSc2	Deutsch
Gesellschaft	Gesellschaft	k1gMnSc1	Gesellschaft
für	für	k?	für
Schädlingsbekämpfung	Schädlingsbekämpfung	k1gMnSc1	Schädlingsbekämpfung
GmbH	GmbH	k1gMnSc1	GmbH
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
Degesch	Degescha	k1gFnPc2	Degescha
<g/>
)	)	kIx)	)
z	z	k7c2	z
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
koncernu	koncern	k1gInSc2	koncern
IG	IG	kA	IG
Farben	Farben	k2eAgInSc4d1	Farben
AG	AG	kA	AG
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
poslán	poslat	k5eAaPmNgInS	poslat
do	do	k7c2	do
likvidace	likvidace	k1gFnSc2	likvidace
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
začala	začít	k5eAaPmAgFnS	začít
poprvé	poprvé	k6eAd1	poprvé
Cyklon	cyklon	k1gInSc4	cyklon
B	B	kA	B
vyrábět	vyrábět	k5eAaImF	vyrábět
továrna	továrna	k1gFnSc1	továrna
Dessauer	Dessauer	k1gInSc4	Dessauer
Werke	Werke	k1gInSc1	Werke
für	für	k?	für
Zucker	Zucker	k1gInSc1	Zucker
und	und	k?	und
chemische	chemische	k1gInSc1	chemische
Industrie	industrie	k1gFnSc1	industrie
(	(	kIx(	(
<g/>
DZR	DZR	kA	DZR
<g/>
)	)	kIx)	)
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
/	/	kIx~	/
<g/>
25	[number]	k4	25
<g/>
;	;	kIx,	;
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
továrna	továrna	k1gFnSc1	továrna
jediným	jediný	k2eAgMnSc7d1	jediný
výrobcem	výrobce	k1gMnSc7	výrobce
tohoto	tento	k3xDgInSc2	tento
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Cyklon	cyklon	k1gInSc1	cyklon
B	B	kA	B
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
i	i	k9	i
její	její	k3xOp3gFnSc1	její
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
pobočka	pobočka	k1gFnSc1	pobočka
Draslovka	draslovka	k1gFnSc1	draslovka
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
byla	být	k5eAaImAgFnS	být
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
tohoto	tento	k3xDgInSc2	tento
plynu	plyn	k1gInSc2	plyn
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
v	v	k7c6	v
Desavě	Desava	k1gFnSc6	Desava
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
399,2	[number]	k4	399,2
tun	tuna	k1gFnPc2	tuna
v	v	k7c6	v
Desavě	Desava	k1gFnSc6	Desava
oproti	oproti	k7c3	oproti
58,4	[number]	k4	58,4
tunám	tuna	k1gFnPc3	tuna
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
údaje	údaj	k1gInPc4	údaj
pro	pro	k7c4	pro
léta	léto	k1gNnPc4	léto
1944	[number]	k4	1944
a	a	k8xC	a
1945	[number]	k4	1945
neznáme	znát	k5eNaImIp1nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
Cyklonu	cyklon	k1gInSc2	cyklon
B	B	kA	B
v	v	k7c6	v
Lučebních	lučební	k2eAgInPc6d1	lučební
závodech	závod	k1gInPc6	závod
Draslovka	draslovka	k1gFnSc1	draslovka
ve	v	k7c6	v
středočeském	středočeský	k2eAgInSc6d1	středočeský
Kolíně	Kolín	k1gInSc6	Kolín
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
pod	pod	k7c7	pod
změněným	změněný	k2eAgInSc7d1	změněný
obchodním	obchodní	k2eAgInSc7d1	obchodní
názvem	název	k1gInSc7	název
Uragan	Uragana	k1gFnPc2	Uragana
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
dezinsekční	dezinsekční	k2eAgInSc1d1	dezinsekční
a	a	k8xC	a
deratizační	deratizační	k2eAgInSc1d1	deratizační
prostředek	prostředek	k1gInSc1	prostředek
při	při	k7c6	při
plynování	plynování	k1gNnSc6	plynování
(	(	kIx(	(
<g/>
fumigaci	fumigace	k1gFnSc6	fumigace
<g/>
)	)	kIx)	)
např.	např.	kA	např.
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
URAGAN	URAGAN	kA	URAGAN
D2	D2	k1gMnSc4	D2
je	být	k5eAaImIp3nS	být
stabilizovaný	stabilizovaný	k2eAgInSc1d1	stabilizovaný
kapalný	kapalný	k2eAgInSc1d1	kapalný
kyanovodík	kyanovodík	k1gInSc1	kyanovodík
(	(	kIx(	(
<g/>
min	min	kA	min
<g/>
.	.	kIx.	.
97,6	[number]	k4	97,6
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
nasáklý	nasáklý	k2eAgMnSc1d1	nasáklý
do	do	k7c2	do
porézní	porézní	k2eAgFnSc2d1	porézní
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
plynotěsně	plynotěsně	k6eAd1	plynotěsně
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
v	v	k7c6	v
plechovkách	plechovka	k1gFnPc6	plechovka
<g/>
.	.	kIx.	.
</s>
<s>
Stabilizace	stabilizace	k1gFnSc1	stabilizace
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
kyselinou	kyselina	k1gFnSc7	kyselina
fosforečnou	fosforečný	k2eAgFnSc7d1	fosforečná
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
0,1	[number]	k4	0,1
%	%	kIx~	%
a	a	k8xC	a
oxidem	oxid	k1gInSc7	oxid
siřičitým	siřičitý	k2eAgInSc7d1	siřičitý
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
0,9	[number]	k4	0,9
–	–	k?	–
1,1	[number]	k4	1,1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
URAGAN	URAGAN	kA	URAGAN
D2	D2	k1gFnSc1	D2
(	(	kIx(	(
<g/>
HCN	HCN	kA	HCN
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
vysoce	vysoce	k6eAd1	vysoce
toxických	toxický	k2eAgFnPc2d1	toxická
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
extrémně	extrémně	k6eAd1	extrémně
hořlavých	hořlavý	k2eAgInPc2d1	hořlavý
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
platných	platný	k2eAgInPc2d1	platný
předpisů	předpis	k1gInPc2	předpis
o	o	k7c6	o
nebezpečných	bezpečný	k2eNgFnPc6d1	nebezpečná
látkách	látka	k1gFnPc6	látka
zdraví	zdraví	k1gNnSc2	zdraví
škodlivých	škodlivý	k2eAgFnPc2d1	škodlivá
<g/>
.	.	kIx.	.
</s>
<s>
Kyanovodík	kyanovodík	k1gInSc1	kyanovodík
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
jed	jed	k1gInSc1	jed
<g/>
.	.	kIx.	.
</s>
<s>
Toxický	toxický	k2eAgInSc1d1	toxický
účinek	účinek	k1gInSc1	účinek
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
blokování	blokování	k1gNnSc6	blokování
enzymů	enzym	k1gInPc2	enzym
tkáňového	tkáňový	k2eAgNnSc2d1	tkáňové
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Transport	transport	k1gInSc1	transport
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
je	být	k5eAaImIp3nS	být
zachován	zachovat	k5eAaPmNgInS	zachovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nastává	nastávat	k5eAaImIp3nS	nastávat
tkáňová	tkáňový	k2eAgFnSc1d1	tkáňová
hypoxie	hypoxie	k1gFnSc1	hypoxie
<g/>
.	.	kIx.	.
</s>
