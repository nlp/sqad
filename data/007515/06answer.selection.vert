<s>
Lodě	loď	k1gFnPc1	loď
jezdí	jezdit	k5eAaImIp3nP	jezdit
z	z	k7c2	z
přístaviště	přístaviště	k1gNnSc2	přístaviště
v	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
do	do	k7c2	do
Veverské	Veverský	k2eAgFnSc2d1	Veverská
Bítýšky	Bítýška	k1gFnSc2	Bítýška
<g/>
,	,	kIx,	,
obce	obec	k1gFnSc2	obec
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
již	již	k6eAd1	již
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozšířené	rozšířený	k2eAgFnSc6d1	rozšířená
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
(	(	kIx(	(
<g/>
od	od	k7c2	od
konce	konec	k1gInSc2	konec
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
