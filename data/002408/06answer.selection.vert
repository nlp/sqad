<s>
Podle	podle	k7c2	podle
původu	původ	k1gInSc2	původ
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c6	na
bacilární	bacilární	k2eAgFnSc6d1	bacilární
úplavici	úplavice	k1gFnSc6	úplavice
<g/>
,	,	kIx,	,
způsobovanou	způsobovaný	k2eAgFnSc7d1	způsobovaná
bakterií	bakterie	k1gFnSc7	bakterie
<g/>
,	,	kIx,	,
a	a	k8xC	a
amébní	amébnit	k5eAaPmIp3nS	amébnit
úplavici	úplavice	k1gFnSc4	úplavice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS	zapříčiňovat
měňavka	měňavka	k1gFnSc1	měňavka
(	(	kIx(	(
<g/>
prvok	prvok	k1gMnSc1	prvok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
