<s>
Úplavice	úplavice	k1gFnSc1	úplavice
je	být	k5eAaImIp3nS	být
akutní	akutní	k2eAgFnSc1d1	akutní
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
infekční	infekční	k2eAgNnSc4d1	infekční
průjmové	průjmový	k2eAgNnSc4d1	průjmové
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
původu	původ	k1gInSc2	původ
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c6	na
bacilární	bacilární	k2eAgFnSc6d1	bacilární
úplavici	úplavice	k1gFnSc6	úplavice
<g/>
,	,	kIx,	,
způsobovanou	způsobovaný	k2eAgFnSc7d1	způsobovaná
bakterií	bakterie	k1gFnSc7	bakterie
<g/>
,	,	kIx,	,
a	a	k8xC	a
amébní	amébnit	k5eAaPmIp3nS	amébnit
úplavici	úplavice	k1gFnSc4	úplavice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS	zapříčiňovat
měňavka	měňavka	k1gFnSc1	měňavka
(	(	kIx(	(
<g/>
prvok	prvok	k1gMnSc1	prvok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přenáší	přenášet	k5eAaImIp3nS	přenášet
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
z	z	k7c2	z
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Léčí	léčit	k5eAaImIp3nS	léčit
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
a	a	k8xC	a
pro	pro	k7c4	pro
pacienta	pacient	k1gMnSc4	pacient
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
dieta	dieta	k1gFnSc1	dieta
a	a	k8xC	a
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
přísun	přísun	k1gInSc1	přísun
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Alternativními	alternativní	k2eAgInPc7d1	alternativní
názvy	název	k1gInPc7	název
této	tento	k3xDgFnSc2	tento
formy	forma	k1gFnSc2	forma
nemoci	nemoc	k1gFnSc2	nemoc
jsou	být	k5eAaImIp3nP	být
shigelóza	shigelóza	k1gFnSc1	shigelóza
nebo	nebo	k8xC	nebo
dyzentérie	dyzentérie	k1gFnSc1	dyzentérie
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
původcem	původce	k1gMnSc7	původce
jsou	být	k5eAaImIp3nP	být
gramnegativní	gramnegativní	k2eAgFnSc2d1	gramnegativní
tyčinkovité	tyčinkovitý	k2eAgFnSc2d1	tyčinkovitá
bakterie	bakterie	k1gFnSc2	bakterie
rodu	rod	k1gInSc2	rod
Shigella	Shigello	k1gNnSc2	Shigello
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejnebezpečnější	bezpečný	k2eNgFnSc1d3	nejnebezpečnější
je	být	k5eAaImIp3nS	být
Shigella	Shigella	k1gFnSc1	Shigella
dysenteriae	dysenteriae	k1gFnSc1	dysenteriae
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
u	u	k7c2	u
cholery	cholera	k1gFnSc2	cholera
o	o	k7c4	o
výlučně	výlučně	k6eAd1	výlučně
lidské	lidský	k2eAgNnSc4d1	lidské
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
zdrojem	zdroj	k1gInSc7	zdroj
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jiný	jiný	k2eAgMnSc1d1	jiný
člověk	člověk	k1gMnSc1	člověk
-	-	kIx~	-
nemocný	nemocný	k1gMnSc1	nemocný
nebo	nebo	k8xC	nebo
přenašeč	přenašeč	k1gMnSc1	přenašeč
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
dochází	docházet	k5eAaImIp3nS	docházet
alimentární	alimentární	k2eAgNnSc1d1	alimentární
cestou	cesta	k1gFnSc7	cesta
přes	přes	k7c4	přes
potraviny	potravina	k1gFnPc4	potravina
nebo	nebo	k8xC	nebo
pitnou	pitný	k2eAgFnSc4d1	pitná
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mouchy	moucha	k1gFnPc1	moucha
mohou	moct	k5eAaImIp3nP	moct
přenést	přenést	k5eAaPmF	přenést
infekční	infekční	k2eAgInSc4d1	infekční
materiál	materiál	k1gInSc4	materiál
na	na	k7c6	na
krátké	krátký	k2eAgFnSc6d1	krátká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
extrémně	extrémně	k6eAd1	extrémně
nakažlivou	nakažlivý	k2eAgFnSc4d1	nakažlivá
chorobu	choroba	k1gFnSc4	choroba
<g/>
,	,	kIx,	,
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
stačí	stačit	k5eAaBmIp3nS	stačit
dávka	dávka	k1gFnSc1	dávka
okolo	okolo	k7c2	okolo
200	[number]	k4	200
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Prevence	prevence	k1gFnSc1	prevence
<g/>
:	:	kIx,	:
izolace	izolace	k1gFnSc1	izolace
nemocných	nemocný	k1gMnPc2	nemocný
<g/>
,	,	kIx,	,
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
kontroly	kontrola	k1gFnPc4	kontrola
pracovníků	pracovník	k1gMnPc2	pracovník
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
<g/>
,	,	kIx,	,
mytí	mytí	k1gNnSc1	mytí
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
tepelná	tepelný	k2eAgFnSc1d1	tepelná
úprava	úprava	k1gFnSc1	úprava
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
jídla	jídlo	k1gNnSc2	jídlo
před	před	k7c7	před
mouchami	moucha	k1gFnPc7	moucha
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
činí	činit	k5eAaImIp3nS	činit
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
napadají	napadat	k5eAaPmIp3nP	napadat
tlusté	tlustý	k2eAgNnSc4d1	tlusté
střevo	střevo	k1gNnSc4	střevo
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
toxiny	toxin	k1gInPc1	toxin
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
příznaky	příznak	k1gInPc1	příznak
jsou	být	k5eAaImIp3nP	být
svíravé	svíravý	k2eAgFnPc4d1	svíravá
bolesti	bolest	k1gFnPc4	bolest
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
křeče	křeč	k1gFnPc4	křeč
a	a	k8xC	a
vodnaté	vodnatý	k2eAgInPc4d1	vodnatý
průjmy	průjem	k1gInPc4	průjem
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
hlenu	hlen	k1gInSc2	hlen
<g/>
.	.	kIx.	.
</s>
<s>
Hrozí	hrozit	k5eAaImIp3nS	hrozit
silná	silný	k2eAgFnSc1d1	silná
dehydratace	dehydratace	k1gFnSc1	dehydratace
a	a	k8xC	a
u	u	k7c2	u
extrémně	extrémně	k6eAd1	extrémně
těžkých	těžký	k2eAgInPc2d1	těžký
případů	případ	k1gInPc2	případ
protržení	protržení	k1gNnSc2	protržení
střevní	střevní	k2eAgFnSc2d1	střevní
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
onemocnění	onemocnění	k1gNnSc2	onemocnění
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
.	.	kIx.	.
</s>
<s>
Diagnostikuje	diagnostikovat	k5eAaBmIp3nS	diagnostikovat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
kultivace	kultivace	k1gFnSc2	kultivace
původce	původce	k1gMnSc2	původce
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
pomocí	pomocí	k7c2	pomocí
Endiaronu	Endiaron	k1gInSc2	Endiaron
(	(	kIx(	(
<g/>
u	u	k7c2	u
lehčích	lehký	k2eAgInPc2d2	lehčí
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
dieta	dieta	k1gFnSc1	dieta
a	a	k8xC	a
nahrazování	nahrazování	k1gNnSc1	nahrazování
ztracených	ztracený	k2eAgFnPc2d1	ztracená
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Amébní	Amébnit	k5eAaPmIp3nS	Amébnit
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
entamoební	entamoební	k2eAgFnSc1d1	entamoební
úplavice	úplavice	k1gFnSc1	úplavice
je	být	k5eAaImIp3nS	být
způsobována	způsobován	k2eAgFnSc1d1	způsobována
prvokem	prvok	k1gMnSc7	prvok
Entamoeba	Entamoeb	k1gMnSc2	Entamoeb
histolytica	histolyticus	k1gMnSc2	histolyticus
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
měňavka	měňavka	k1gFnSc1	měňavka
úplavičná	úplavičný	k2eAgFnSc1d1	úplavičná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
ve	v	k7c6	v
vysoce	vysoce	k6eAd1	vysoce
znečištěných	znečištěný	k2eAgFnPc6d1	znečištěná
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
kanále	kanál	k1gInSc6	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
jako	jako	k8xC	jako
u	u	k7c2	u
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
<g/>
.	.	kIx.	.
</s>
<s>
Měňavka	měňavka	k1gFnSc1	měňavka
úplavičná	úplavičný	k2eAgFnSc1d1	úplavičná
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
tlustém	tlustý	k2eAgNnSc6d1	tlusté
střevu	střevo	k1gNnSc6	střevo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
formách	forma	k1gFnPc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
menší	malý	k2eAgNnSc1d2	menší
(	(	kIx(	(
<g/>
minuta	minuta	k1gFnSc1	minuta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
a	a	k8xC	a
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
organismus	organismus	k1gInSc4	organismus
prakticky	prakticky	k6eAd1	prakticky
neškodná	škodný	k2eNgFnSc1d1	neškodná
a	a	k8xC	a
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
větší	veliký	k2eAgFnSc6d2	veliký
(	(	kIx(	(
<g/>
magna	magna	k6eAd1	magna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
začne	začít	k5eAaPmIp3nS	začít
v	v	k7c6	v
tlustém	tlustý	k2eAgNnSc6d1	tlusté
střevě	střevo	k1gNnSc6	střevo
tvořit	tvořit	k5eAaImF	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
napadá	napadat	k5eAaBmIp3nS	napadat
střevní	střevní	k2eAgInSc4d1	střevní
epitel	epitel	k1gInSc4	epitel
a	a	k8xC	a
proniká	pronikat	k5eAaImIp3nS	pronikat
do	do	k7c2	do
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
může	moct	k5eAaImIp3nS	moct
roznést	roznést	k5eAaPmF	roznést
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
podobnými	podobný	k2eAgInPc7d1	podobný
příznaky	příznak	k1gInPc7	příznak
jako	jako	k8xS	jako
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
úplavice	úplavice	k1gFnSc1	úplavice
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
jaterní	jaterní	k2eAgInPc4d1	jaterní
abscesy	absces	k1gInPc4	absces
<g/>
.	.	kIx.	.
</s>
<s>
Léčí	léčit	k5eAaImIp3nP	léčit
se	se	k3xPyFc4	se
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
<g/>
,	,	kIx,	,
jaterní	jaterní	k2eAgInPc4d1	jaterní
abscesy	absces	k1gInPc4	absces
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
chirurgicky	chirurgicky	k6eAd1	chirurgicky
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
dietu	dieta	k1gFnSc4	dieta
a	a	k8xC	a
doplnění	doplnění	k1gNnSc4	doplnění
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Prognóza	prognóza	k1gFnSc1	prognóza
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
dosažitelnosti	dosažitelnost	k1gFnSc6	dosažitelnost
lékařské	lékařský	k2eAgFnSc2d1	lékařská
pomoci	pomoc	k1gFnSc2	pomoc
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
formě	forma	k1gFnSc6	forma
nemoci	nemoc	k1gFnSc2	nemoc
a	a	k8xC	a
stavu	stav	k1gInSc2	stav
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
O.	O.	kA	O.
Uplavici	Uplavice	k1gFnSc6	Uplavice
-	-	kIx~	-
pseudonym	pseudonym	k1gInSc1	pseudonym
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nímž	jenž	k3xRgMnSc7	jenž
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
omylem	omylem	k6eAd1	omylem
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
článek	článek	k1gInSc1	článek
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Hlavy	Hlava	k1gMnSc2	Hlava
Disanteria	Disanterium	k1gNnSc2	Disanterium
</s>
