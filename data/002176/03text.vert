<s>
Trenton	Trenton	k1gInSc1	Trenton
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
a	a	k8xC	a
okresní	okresní	k2eAgNnSc1d1	okresní
město	město	k1gNnSc1	město
Mercer	Mercra	k1gFnPc2	Mercra
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
měl	mít	k5eAaImAgMnS	mít
Trenton	Trenton	k1gInSc4	Trenton
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
82	[number]	k4	82
804	[number]	k4	804
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
21,1	[number]	k4	21,1
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
19,8	[number]	k4	19,8
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
pevnina	pevnina	k1gFnSc1	pevnina
a	a	k8xC	a
1,3	[number]	k4	1,3
km2	km2	k4	km2
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
84	[number]	k4	84
913	[number]	k4	913
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlilo	sídlit	k5eAaImAgNnS	sídlit
85	[number]	k4	85
403	[number]	k4	403
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
29	[number]	k4	29
437	[number]	k4	437
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
18	[number]	k4	18
692	[number]	k4	692
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
byla	být	k5eAaImAgFnS	být
4	[number]	k4	4
304,7	[number]	k4	304,7
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
26,6	[number]	k4	26,6
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
52,0	[number]	k4	52,0
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
1,2	[number]	k4	1,2
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
15,3	[number]	k4	15,3
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
4,1	[number]	k4	4,1
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
33,7	[number]	k4	33,7
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Hispánci	Hispánek	k1gMnPc1	Hispánek
-	-	kIx~	-
21,53	[number]	k4	21,53
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
-	-	kIx~	-
52,1	[number]	k4	52,1
%	%	kIx~	%
Portoričané	Portoričan	k1gMnPc1	Portoričan
-	-	kIx~	-
10,5	[number]	k4	10,5
%	%	kIx~	%
Italové	Ital	k1gMnPc1	Ital
-	-	kIx~	-
7,3	[number]	k4	7,3
%	%	kIx~	%
Irové	Ir	k1gMnPc1	Ir
-	-	kIx~	-
4,5	[number]	k4	4,5
%	%	kIx~	%
Poláci	Polák	k1gMnPc1	Polák
-	-	kIx~	-
3,8	[number]	k4	3,8
%	%	kIx~	%
Guatemalané	Guatemalaný	k2eAgFnSc2d1	Guatemalaný
-	-	kIx~	-
3,1	[number]	k4	3,1
%	%	kIx~	%
Angličané	Angličan	k1gMnPc1	Angličan
-	-	kIx~	-
2,0	[number]	k4	2,0
%	%	kIx~	%
Jamajčané	Jamajčan	k1gMnPc1	Jamajčan
-	-	kIx~	-
1,3	[number]	k4	1,3
%	%	kIx~	%
Maďaři	Maďar	k1gMnPc1	Maďar
-	-	kIx~	-
1,1	[number]	k4	1,1
%	%	kIx~	%
Mexičané	Mexičan	k1gMnPc1	Mexičan
-	-	kIx~	-
1,1	[number]	k4	1,1
%	%	kIx~	%
<	<	kIx(	<
<g/>
18	[number]	k4	18
let	léto	k1gNnPc2	léto
-	-	kIx~	-
27,7	[number]	k4	27,7
<g />
.	.	kIx.	.
</s>
<s>
%	%	kIx~	%
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
let	léto	k1gNnPc2	léto
-	-	kIx~	-
10,1	[number]	k4	10,1
%	%	kIx~	%
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
let	léto	k1gNnPc2	léto
-	-	kIx~	-
31,9	[number]	k4	31,9
%	%	kIx~	%
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
-	-	kIx~	-
18,9	[number]	k4	18,9
%	%	kIx~	%
>	>	kIx)	>
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
-	-	kIx~	-
11,4	[number]	k4	11,4
%	%	kIx~	%
průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
-	-	kIx~	-
32	[number]	k4	32
let	léto	k1gNnPc2	léto
Roxanne	Roxann	k1gInSc5	Roxann
Hart	Hart	k2eAgMnSc1d1	Hart
(	(	kIx(	(
<g/>
*	*	kIx~	*
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
