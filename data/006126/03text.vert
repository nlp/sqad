<s>
Ruby	rub	k1gInPc4	rub
je	být	k5eAaImIp3nS	být
interpretovaný	interpretovaný	k2eAgInSc1d1	interpretovaný
skriptovací	skriptovací	k2eAgInSc1d1	skriptovací
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
jednoduché	jednoduchý	k2eAgFnSc3d1	jednoduchá
syntaxi	syntax	k1gFnSc3	syntax
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
snadný	snadný	k2eAgMnSc1d1	snadný
k	k	k7c3	k
naučení	naučení	k1gNnSc3	naučení
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
dostatečně	dostatečně	k6eAd1	dostatečně
výkonný	výkonný	k2eAgMnSc1d1	výkonný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokázal	dokázat	k5eAaPmAgMnS	dokázat
konkurovat	konkurovat	k5eAaImF	konkurovat
známějším	známý	k2eAgMnPc3d2	známější
jazykům	jazyk	k1gMnPc3	jazyk
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Python	Python	k1gMnSc1	Python
a	a	k8xC	a
Perl	perl	k1gInSc1	perl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
objektově	objektově	k6eAd1	objektově
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
–	–	k?	–
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c4	v
Ruby	rub	k1gInPc4	rub
je	být	k5eAaImIp3nS	být
objekt	objekt	k1gInSc1	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrcem	tvůrce	k1gMnSc7	tvůrce
Ruby	rub	k1gInPc7	rub
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
člověk	člověk	k1gMnSc1	člověk
<g/>
:	:	kIx,	:
Jukihiro	Jukihiro	k1gNnSc1	Jukihiro
Macumoto	Macumota	k1gFnSc5	Macumota
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
také	také	k9	také
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Matz	Matza	k1gFnPc2	Matza
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
jako	jako	k9	jako
zastánce	zastánce	k1gMnPc4	zastánce
objektově	objektově	k6eAd1	objektově
orientovaného	orientovaný	k2eAgNnSc2d1	orientované
programování	programování	k1gNnSc2	programování
hledal	hledat	k5eAaImAgInS	hledat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
skriptovací	skriptovací	k2eAgInSc1d1	skriptovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
vyhovoval	vyhovovat	k5eAaImAgMnS	vyhovovat
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Perl	perl	k1gInSc4	perl
mu	on	k3xPp3gMnSc3	on
připadal	připadat	k5eAaPmAgMnS	připadat
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
málo	málo	k6eAd1	málo
výkonný	výkonný	k2eAgMnSc1d1	výkonný
a	a	k8xC	a
Python	Python	k1gMnSc1	Python
zase	zase	k9	zase
nebyl	být	k5eNaImAgMnS	být
natolik	natolik	k6eAd1	natolik
objektový	objektový	k2eAgMnSc1d1	objektový
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
vlastní	vlastní	k2eAgInSc4d1	vlastní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
započaly	započnout	k5eAaPmAgFnP	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
uveřejněna	uveřejnit	k5eAaPmNgFnS	uveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Matz	Matz	k1gMnSc1	Matz
svůj	svůj	k3xOyFgInSc4	svůj
jazyk	jazyk	k1gInSc4	jazyk
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
po	po	k7c6	po
rubínu	rubín	k1gInSc6	rubín
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
Ruby	rub	k1gInPc1	rub
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Ruby	rub	k1gInPc4	rub
asi	asi	k9	asi
nejšířeji	šíro	k6eAd3	šíro
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
svého	svůj	k3xOyFgInSc2	svůj
původu	původ	k1gInSc2	původ
–	–	k?	–
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
do	do	k7c2	do
světa	svět	k1gInSc2	svět
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
bránila	bránit	k5eAaImAgFnS	bránit
absence	absence	k1gFnSc1	absence
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
dokumentace	dokumentace	k1gFnSc2	dokumentace
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
však	však	k9	však
anglických	anglický	k2eAgInPc2d1	anglický
materiálů	materiál	k1gInPc2	materiál
celkem	celkem	k6eAd1	celkem
dost	dost	k6eAd1	dost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
Ruby	rub	k1gInPc4	rub
prudce	prudko	k6eAd1	prudko
roste	růst	k5eAaImIp3nS	růst
díky	díky	k7c3	díky
úspěchu	úspěch	k1gInSc3	úspěch
webového	webový	k2eAgInSc2d1	webový
frameworku	frameworek	k1gInSc2	frameworek
Ruby	rub	k1gInPc1	rub
on	on	k3xPp3gMnSc1	on
Rails	Railsa	k1gFnPc2	Railsa
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jiné	jiný	k2eAgInPc4d1	jiný
skriptovací	skriptovací	k2eAgInPc4d1	skriptovací
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
použití	použití	k1gNnSc4	použití
Ruby	rub	k1gInPc4	rub
poměrně	poměrně	k6eAd1	poměrně
široká	široký	k2eAgFnSc1d1	široká
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejčastější	častý	k2eAgNnSc1d3	nejčastější
využití	využití	k1gNnSc1	využití
najde	najít	k5eAaPmIp3nS	najít
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
Linux	Linux	kA	Linux
(	(	kIx(	(
<g/>
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
systémech	systém	k1gInPc6	systém
typu	typ	k1gInSc2	typ
Unix	Unix	k1gInSc1	Unix
<g/>
)	)	kIx)	)
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
skriptů	skript	k1gInPc2	skript
pro	pro	k7c4	pro
usnadnění	usnadnění	k1gNnSc4	usnadnění
každodenní	každodenní	k2eAgFnSc2d1	každodenní
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
přehledné	přehledný	k2eAgFnSc3d1	přehledná
syntaxi	syntax	k1gFnSc3	syntax
a	a	k8xC	a
striktní	striktní	k2eAgFnSc3d1	striktní
objektovosti	objektovost	k1gFnSc3	objektovost
se	se	k3xPyFc4	se
hodí	hodit	k5eAaPmIp3nS	hodit
i	i	k9	i
k	k	k7c3	k
psaní	psaní	k1gNnSc3	psaní
velkých	velký	k2eAgInPc2d1	velký
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
CGI	CGI	kA	CGI
skriptů	skript	k1gInPc2	skript
<g/>
,	,	kIx,	,
ba	ba	k9	ba
dokonce	dokonce	k9	dokonce
GUI	GUI	kA	GUI
aplikací	aplikace	k1gFnPc2	aplikace
(	(	kIx(	(
<g/>
GTK	GTK	kA	GTK
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
TK	TK	kA	TK
<g/>
,	,	kIx,	,
Qt	Qt	k1gFnPc1	Qt
<g/>
,	,	kIx,	,
fxRuby	fxRuba	k1gFnPc1	fxRuba
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
podporuje	podporovat	k5eAaImIp3nS	podporovat
regulární	regulární	k2eAgInPc4d1	regulární
výrazy	výraz	k1gInPc4	výraz
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
Ruby	rub	k1gInPc4	rub
vhodným	vhodný	k2eAgInSc7d1	vhodný
jazykem	jazyk	k1gInSc7	jazyk
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
textových	textový	k2eAgInPc2d1	textový
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Výhody	výhoda	k1gFnPc1	výhoda
<g/>
:	:	kIx,	:
interpretovaný	interpretovaný	k2eAgInSc4d1	interpretovaný
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plynoucí	plynoucí	k2eAgInPc1d1	plynoucí
výhody	výhod	k1gInPc1	výhod
(	(	kIx(	(
<g/>
odpadá	odpadat	k5eAaImIp3nS	odpadat
proces	proces	k1gInSc4	proces
kompilace	kompilace	k1gFnSc2	kompilace
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
ve	v	k7c6	v
zdrojovém	zdrojový	k2eAgInSc6d1	zdrojový
kódu	kód	k1gInSc6	kód
jsou	být	k5eAaImIp3nP	být
ihned	ihned	k6eAd1	ihned
vidět	vidět	k5eAaImF	vidět
<g/>
)	)	kIx)	)
přenositelnost	přenositelnost	k1gFnSc4	přenositelnost
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
kódu	kód	k1gInSc2	kód
mezi	mezi	k7c7	mezi
platformami	platforma	k1gFnPc7	platforma
(	(	kIx(	(
<g/>
MS	MS	kA	MS
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
GNU	gnu	k1gNnSc1	gnu
<g/>
/	/	kIx~	/
<g/>
Linux	linux	k1gInSc1	linux
<g/>
,	,	kIx,	,
Solaris	Solaris	k1gInSc1	Solaris
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
možnost	možnost	k1gFnSc1	možnost
spuštění	spuštění	k1gNnSc2	spuštění
v	v	k7c6	v
interaktivním	interaktivní	k2eAgInSc6d1	interaktivní
režimu	režim	k1gInSc6	režim
(	(	kIx(	(
<g/>
program	program	k1gInSc1	program
irb	irb	k?	irb
příkazy	příkaz	k1gInPc1	příkaz
jsou	být	k5eAaImIp3nP	být
prováděny	provádět	k5eAaImNgInP	provádět
okamžitě	okamžitě	k6eAd1	okamžitě
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
zápisu	zápis	k1gInSc6	zápis
<g/>
,	,	kIx,	,
podporuje	podporovat	k5eAaImIp3nS	podporovat
doplňování	doplňování	k1gNnSc1	doplňování
jmen	jméno	k1gNnPc2	jméno
<g/>
)	)	kIx)	)
jednoduchá	jednoduchý	k2eAgNnPc1d1	jednoduché
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
naučitelná	naučitelný	k2eAgFnSc1d1	naučitelná
syntaxe	syntaxe	k1gFnSc1	syntaxe
plná	plný	k2eAgFnSc1d1	plná
podpora	podpora	k1gFnSc1	podpora
objektově	objektově	k6eAd1	objektově
orientovaného	orientovaný	k2eAgNnSc2d1	orientované
programování	programování	k1gNnSc2	programování
–	–	k?	–
vše	všechen	k3xTgNnSc4	všechen
je	být	k5eAaImIp3nS	být
objekt	objekt	k1gInSc1	objekt
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
však	však	k9	však
psát	psát	k5eAaImF	psát
i	i	k9	i
klasické	klasický	k2eAgInPc4d1	klasický
strukturované	strukturovaný	k2eAgInPc4d1	strukturovaný
programy	program	k1gInPc4	program
<g/>
)	)	kIx)	)
dynamické	dynamický	k2eAgInPc1d1	dynamický
datové	datový	k2eAgInPc1d1	datový
<g />
.	.	kIx.	.
</s>
<s>
typy	typ	k1gInPc1	typ
regulární	regulární	k2eAgInPc1d1	regulární
výrazy	výraz	k1gInPc1	výraz
modularita	modularita	k1gFnSc1	modularita
zdrojových	zdrojový	k2eAgInPc2d1	zdrojový
kódů	kód	k1gInPc2	kód
možnost	možnost	k1gFnSc4	možnost
psát	psát	k5eAaImF	psát
GUI	GUI	kA	GUI
nebo	nebo	k8xC	nebo
webové	webový	k2eAgFnSc2d1	webová
aplikace	aplikace	k1gFnSc2	aplikace
Nevýhody	nevýhoda	k1gFnSc2	nevýhoda
<g/>
:	:	kIx,	:
nižší	nízký	k2eAgFnSc1d2	nižší
rychlost	rychlost	k1gFnSc1	rychlost
oproti	oproti	k7c3	oproti
kompilovaným	kompilovaný	k2eAgInPc3d1	kompilovaný
jazykům	jazyk	k1gInPc3	jazyk
(	(	kIx(	(
<g/>
interpretovaný	interpretovaný	k2eAgInSc1d1	interpretovaný
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
překládán	překládat	k5eAaImNgInS	překládat
až	až	k6eAd1	až
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
knihoven	knihovna	k1gFnPc2	knihovna
je	být	k5eAaImIp3nS	být
napsána	napsat	k5eAaBmNgFnS	napsat
rovněž	rovněž	k9	rovněž
v	v	k7c4	v
Ruby	rub	k1gInPc4	rub
<g/>
)	)	kIx)	)
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
interpretu	interpret	k1gMnSc6	interpret
nedostatek	nedostatek	k1gInSc4	nedostatek
české	český	k2eAgFnSc2d1	Česká
dokumentace	dokumentace	k1gFnSc2	dokumentace
Pro	pro	k7c4	pro
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Ruby	rub	k1gInPc4	rub
vypadá	vypadat	k5eAaImIp3nS	vypadat
<g/>
,	,	kIx,	,
ukažme	ukázat	k5eAaPmRp1nP	ukázat
si	se	k3xPyFc3	se
několik	několik	k4yIc4	několik
příkladů	příklad	k1gInPc2	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
začátek	začátek	k1gInSc4	začátek
tradiční	tradiční	k2eAgInSc1d1	tradiční
program	program	k1gInSc1	program
hello	hello	k1gNnSc1	hello
world	world	k1gInSc1	world
<g/>
:	:	kIx,	:
Tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používán	používat	k5eAaImNgInS	používat
na	na	k7c4	na
důkaz	důkaz	k1gInSc4	důkaz
toho	ten	k3xDgNnSc2	ten
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
kód	kód	k1gInSc4	kód
v	v	k7c4	v
Ruby	rub	k1gInPc4	rub
čitelný	čitelný	k2eAgMnSc1d1	čitelný
<g/>
:	:	kIx,	:
Tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
pětkrát	pětkrát	k6eAd1	pětkrát
vypíše	vypsat	k5eAaPmIp3nS	vypsat
"	"	kIx"	"
<g/>
ahoj	ahoj	k0	ahoj
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Doslovně	doslovně	k6eAd1	doslovně
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
brát	brát	k5eAaImF	brát
5	[number]	k4	5
krát	krát	k6eAd1	krát
(	(	kIx(	(
<g/>
times	times	k1gInSc1	times
znamená	znamenat	k5eAaImIp3nS	znamenat
česky	česky	k6eAd1	česky
krát	krát	k6eAd1	krát
<g/>
)	)	kIx)	)
vypiš	vypsat	k5eAaPmRp2nS	vypsat
ahoj	ahoj	k0	ahoj
(	(	kIx(	(
<g/>
puts	puts	k6eAd1	puts
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
z	z	k7c2	z
put	puta	k1gFnPc2	puta
string	string	k1gInSc4	string
–	–	k?	–
"	"	kIx"	"
<g/>
dej	dát	k5eAaPmRp2nS	dát
řetězec	řetězec	k1gInSc1	řetězec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
na	na	k7c4	na
standardní	standardní	k2eAgInSc4d1	standardní
výstup	výstup	k1gInSc4	výstup
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
příklad	příklad	k1gInSc1	příklad
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
pořád	pořád	k6eAd1	pořád
tak	tak	k6eAd1	tak
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
,	,	kIx,	,
že	že	k8xS	že
nám	my	k3xPp1nPc3	my
toho	ten	k3xDgNnSc2	ten
o	o	k7c6	o
struktuře	struktura	k1gFnSc6	struktura
nic	nic	k3yNnSc1	nic
moc	moc	k6eAd1	moc
nepoví	povědět	k5eNaPmIp3nS	povědět
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
příklad	příklad	k1gInSc1	příklad
prozradí	prozradit	k5eAaPmIp3nS	prozradit
víc	hodně	k6eAd2	hodně
<g/>
:	:	kIx,	:
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
definice	definice	k1gFnSc1	definice
metody	metoda	k1gFnSc2	metoda
fib	fib	k?	fib
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vypíše	vypsat	k5eAaPmIp3nS	vypsat
začátek	začátek	k1gInSc4	začátek
Fibonacciho	Fibonacci	k1gMnSc2	Fibonacci
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
posledním	poslední	k2eAgInSc6d1	poslední
řádku	řádek	k1gInSc6	řádek
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
volána	volán	k2eAgFnSc1d1	volána
s	s	k7c7	s
parametrem	parametr	k1gInSc7	parametr
100	[number]	k4	100
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
chceme	chtít	k5eAaImIp1nP	chtít
vypsat	vypsat	k5eAaPmF	vypsat
posloupnost	posloupnost	k1gFnSc4	posloupnost
všech	všecek	k3xTgNnPc2	všecek
čísel	číslo	k1gNnPc2	číslo
Fibonacciho	Fibonacci	k1gMnSc2	Fibonacci
posloupnosti	posloupnost	k1gFnSc2	posloupnost
menších	malý	k2eAgMnPc2d2	menší
než	než	k8xS	než
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
programu	program	k1gInSc2	program
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgFnSc1d1	následující
<g/>
:	:	kIx,	:
1	[number]	k4	1
1	[number]	k4	1
2	[number]	k4	2
3	[number]	k4	3
5	[number]	k4	5
8	[number]	k4	8
13	[number]	k4	13
21	[number]	k4	21
34	[number]	k4	34
55	[number]	k4	55
89	[number]	k4	89
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
Ruby	rub	k1gInPc4	rub
plně	plně	k6eAd1	plně
objektový	objektový	k2eAgInSc1d1	objektový
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
ukažme	ukázat	k5eAaPmRp1nP	ukázat
i	i	k9	i
objekty	objekt	k1gInPc4	objekt
<g/>
:	:	kIx,	:
Po	po	k7c6	po
spuštění	spuštění	k1gNnSc6	spuštění
bude	být	k5eAaImBp3nS	být
na	na	k7c6	na
výstupu	výstup	k1gInSc6	výstup
<g/>
:	:	kIx,	:
Jmeno	Jmeno	k1gNnSc1	Jmeno
<g/>
:	:	kIx,	:
Tereza	Tereza	k1gFnSc1	Tereza
Vek	veka	k1gFnPc2	veka
<g/>
:	:	kIx,	:
18	[number]	k4	18
Tereza	Tereza	k1gFnSc1	Tereza
18	[number]	k4	18
V	v	k7c4	v
Ruby	rub	k1gInPc4	rub
je	být	k5eAaImIp3nS	být
úplně	úplně	k6eAd1	úplně
vše	všechen	k3xTgNnSc4	všechen
objekt	objekt	k1gInSc1	objekt
(	(	kIx(	(
<g/>
i	i	k8xC	i
třídy	třída	k1gFnPc1	třída
jsou	být	k5eAaImIp3nP	být
objekty	objekt	k1gInPc4	objekt
typu	typ	k1gInSc2	typ
Class	Class	k1gInSc1	Class
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
zavolání	zavolání	k1gNnSc4	zavolání
metody	metoda	k1gFnSc2	metoda
+	+	kIx~	+
objektu	objekt	k1gInSc2	objekt
1	[number]	k4	1
s	s	k7c7	s
argumentem	argument	k1gInSc7	argument
2	[number]	k4	2
<g/>
:	:	kIx,	:
Naše	náš	k3xOp1gInPc1	náš
dva	dva	k4xCgInPc1	dva
objekty	objekt	k1gInPc1	objekt
1	[number]	k4	1
i	i	k8xC	i
2	[number]	k4	2
jsou	být	k5eAaImIp3nP	být
objekty	objekt	k1gInPc1	objekt
třídy	třída	k1gFnSc2	třída
Fixnum	Fixnum	k1gInSc1	Fixnum
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c4	v
Ruby	rub	k1gInPc4	rub
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
uchovávání	uchovávání	k1gNnSc3	uchovávání
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
celá	celý	k2eAgNnPc1d1	celé
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
objekty	objekt	k1gInPc4	objekt
třídy	třída	k1gFnSc2	třída
Bignum	Bignum	k1gInSc1	Bignum
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
teoretická	teoretický	k2eAgFnSc1d1	teoretická
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
velikost	velikost	k1gFnSc4	velikost
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ruby	rub	k1gInPc7	rub
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
http://ruby-lang.org	[url]	k1gInSc4	http://ruby-lang.org
–	–	k?	–
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
jazyka	jazyk	k1gInSc2	jazyk
http://www.ruby-doc.org/	[url]	k?	http://www.ruby-doc.org/
–	–	k?	–
Ruby	rub	k1gInPc4	rub
dokumentační	dokumentační	k2eAgInSc4d1	dokumentační
projekt	projekt	k1gInSc4	projekt
http://rubyforge.org/	[url]	k?	http://rubyforge.org/
–	–	k?	–
stránky	stránka	k1gFnSc2	stránka
projektů	projekt	k1gInPc2	projekt
v	v	k7c4	v
Ruby	rub	k1gInPc4	rub
http://www.rubycentral.com/book/	[url]	k?	http://www.rubycentral.com/book/
–	–	k?	–
online	onlinout	k5eAaPmIp3nS	onlinout
kniha	kniha	k1gFnSc1	kniha
Programming	Programming	k1gInSc1	Programming
Ruby	rub	k1gInPc1	rub
Full	Full	k1gInSc1	Full
Ruby	rub	k1gInPc1	rub
on	on	k3xPp3gMnSc1	on
Rails	Rails	k1gInSc4	Rails
Tutorial	Tutorial	k1gInSc4	Tutorial
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Euruko	Euruko	k1gNnSc1	Euruko
2008	[number]	k4	2008
–	–	k?	–
videozáznamy	videozáznam	k1gInPc7	videozáznam
přednášek	přednáška	k1gFnPc2	přednáška
z	z	k7c2	z
European	Europeana	k1gFnPc2	Europeana
Ruby	rub	k1gInPc1	rub
Conference	Conference	k1gFnSc2	Conference
2008	[number]	k4	2008
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c4	na
avc-cvut	avcvut	k1gInSc4	avc-cvut
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
