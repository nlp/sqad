<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Penangu	Penang	k1gInSc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Penangu	Penang	k1gInSc2
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
SMS	SMS	kA
Emden	Emden	k1gInSc1
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1914	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
U	u	k7c2
ostrova	ostrov	k1gInSc2
Penang	Penanga	k1gFnPc2
<g/>
,	,	kIx,
Malajsie	Malajsie	k1gFnSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Německé	německý	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
Ruské	ruský	k2eAgFnSc2d1
impériumFrancie	impériumFrancie	k1gFnSc2
Francie	Francie	k1gFnSc2
</s>
<s>
Německé	německý	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Německé	německý	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
</s>
<s>
Karl	Karl	k1gInSc1
von	von	k1gInSc1
Müller	Müller	k1gInSc4
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
chráněný	chráněný	k2eAgInSc1d1
křižník	křižník	k1gInSc1
a	a	k8xC
torpédoborec	torpédoborec	k1gInSc1
</s>
<s>
lehký	lehký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
chráněný	chráněný	k2eAgInSc1d1
křižník	křižník	k1gInSc1
a	a	k8xC
torpédoborec	torpédoborec	k1gInSc1
<g/>
,	,	kIx,
89	#num#	k4
<g/>
+	+	kIx~
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
143	#num#	k4
<g/>
+	+	kIx~
zraněných	zraněný	k1gMnPc2
</s>
<s>
žádné	žádný	k3yNgInPc1
?	?	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Penangu	Penang	k1gInSc2
byla	být	k5eAaImAgFnS
námořní	námořní	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uskutečnila	uskutečnit	k5eAaPmAgNnP
se	se	k3xPyFc4
dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1914	#num#	k4
v	v	k7c6
Malackém	malacký	k2eAgInSc6d1
průlivu	průliv	k1gInSc6
a	a	k8xC
německý	německý	k2eAgInSc1d1
lehký	lehký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
SMS	SMS	kA
Emden	Emden	k1gInSc1
v	v	k7c6
ní	on	k3xPp3gFnSc6
potopil	potopit	k5eAaPmAgInS
dvě	dva	k4xCgFnPc4
válečné	válečný	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
Dohody	dohoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ostrov	ostrov	k1gInSc1
Penang	Penang	k1gInSc1
byl	být	k5eAaImAgInS
součástí	součást	k1gFnSc7
britské	britský	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
a	a	k8xC
nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
poblíž	poblíž	k7c2
západního	západní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
Malajsie	Malajsie	k1gFnSc2
Největší	veliký	k2eAgNnSc1d3
město	město	k1gNnSc1
Penangu	Penang	k1gInSc2
je	být	k5eAaImIp3nS
přístav	přístav	k1gInSc1
George	Georg	k1gFnSc2
Town	Towna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
přístav	přístav	k1gInSc1
často	často	k6eAd1
používán	používat	k5eAaImNgInS
spojeneckými	spojenecký	k2eAgFnPc7d1
válečnými	válečná	k1gFnPc7
a	a	k8xC
obchodními	obchodní	k2eAgFnPc7d1
loděmi	loď	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
války	válka	k1gFnSc2
Německá	německý	k2eAgFnSc1d1
východoasijská	východoasijský	k2eAgFnSc1d1
eskadra	eskadra	k1gFnSc1
opustila	opustit	k5eAaPmAgFnS
přístav	přístav	k1gInSc4
Čching-tao	Čching-tao	k6eAd1
v	v	k7c6
Číně	Čína	k1gFnSc6
a	a	k8xC
vypravila	vypravit	k5eAaPmAgFnS
se	se	k3xPyFc4
na	na	k7c4
strastiplnou	strastiplný	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
do	do	k7c2
německých	německý	k2eAgInPc2d1
přístavů	přístav	k1gInPc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
žádná	žádný	k3yNgFnSc1
z	z	k7c2
nich	on	k3xPp3gInPc2
nedoplula	doplout	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
cesty	cesta	k1gFnSc2
zvítězila	zvítězit	k5eAaPmAgFnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Coronelu	Coronel	k1gInSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
poražena	porazit	k5eAaPmNgFnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Falklandských	Falklandský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
plavby	plavba	k1gFnSc2
se	se	k3xPyFc4
od	od	k7c2
squadrony	squadron	k1gMnPc4
oddělil	oddělit	k5eAaPmAgInS
křižník	křižník	k1gInSc1
Emden	Emden	k2eAgInSc1d1
<g/>
,	,	kIx,
kterému	který	k3yRgMnSc3,k3yIgMnSc3,k3yQgMnSc3
velel	velet	k5eAaImAgInS
kapitán	kapitán	k1gMnSc1
Karl	Karla	k1gFnPc2
von	von	k1gInSc4
Müller	Müller	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úkolem	úkol	k1gInSc7
Emdenu	Emden	k1gInSc2
bylo	být	k5eAaImAgNnS
přepadat	přepadat	k5eAaImF
britské	britský	k2eAgFnPc4d1
obchodní	obchodní	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1914	#num#	k4
připlul	připlout	k5eAaPmAgInS
Emden	Emden	k1gInSc1
k	k	k7c3
městu	město	k1gNnSc3
George	George	k1gInPc2
Town	Towna	k1gNnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
napadl	napadnout	k5eAaPmAgInS
přístav	přístav	k1gInSc1
a	a	k8xC
lodě	loď	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gNnSc6
nacházely	nacházet	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Müller	Müller	k1gMnSc1
nechal	nechat	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
falešný	falešný	k2eAgInSc4d1
komín	komín	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
loď	loď	k1gFnSc1
podobala	podobat	k5eAaImAgFnS
britským	britský	k2eAgInPc3d1
křižníkům	křižník	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
vplul	vplout	k5eAaPmAgMnS
do	do	k7c2
přístavu	přístav	k1gInSc2
<g/>
,	,	kIx,
odhalil	odhalit	k5eAaPmAgMnS
svoji	svůj	k3xOyFgFnSc4
pravou	pravý	k2eAgFnSc4d1
identitu	identita	k1gFnSc4
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
vztyčit	vztyčit	k5eAaPmF
německou	německý	k2eAgFnSc4d1
námořní	námořní	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Než	než	k8xS
se	se	k3xPyFc4
dohodové	dohodový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
v	v	k7c6
přístavu	přístav	k1gInSc6
vzpamatovaly	vzpamatovat	k5eAaPmAgFnP
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
torpédován	torpédován	k2eAgInSc4d1
a	a	k8xC
potopen	potopen	k2eAgInSc4d1
ruský	ruský	k2eAgInSc4d1
křižník	křižník	k1gInSc4
Žemčug	Žemčuga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
palubě	paluba	k1gFnSc6
bylo	být	k5eAaImAgNnS
89	#num#	k4
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
143	#num#	k4
zraněných	zraněný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzský	francouzský	k2eAgInSc1d1
torpédoborec	torpédoborec	k1gInSc1
Mosquet	Mosquet	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgInS
Emden	Emden	k1gInSc1
pronásledovat	pronásledovat	k5eAaImF
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
německou	německý	k2eAgFnSc7d1
dělostřelbou	dělostřelba	k1gFnSc7
rychle	rychle	k6eAd1
potopen	potopen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Křižník	křižník	k1gInSc1
Emden	Emden	k2eAgInSc1d1
ještě	ještě	k9
několik	několik	k4yIc4
týdnů	týden	k1gInPc2
podnikal	podnikat	k5eAaImAgMnS
útoky	útok	k1gInPc4
na	na	k7c4
spojenecké	spojenecký	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
,	,	kIx,
kterých	který	k3yIgInPc2,k3yRgInPc2,k3yQgInPc2
potopil	potopit	k5eAaPmAgMnS
celkem	celek	k1gInSc7
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
října	říjen	k1gInSc2
ho	on	k3xPp3gMnSc4
hledalo	hledat	k5eAaImAgNnS
kolem	kolem	k6eAd1
60	#num#	k4
válečných	válečný	k2eAgNnPc2d1
plavidel	plavidlo	k1gNnPc2
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
byl	být	k5eAaImAgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Kokosových	kokosový	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
potopen	potopen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Kapitán	kapitán	k1gMnSc1
ruského	ruský	k2eAgInSc2d1
křižníku	křižník	k1gInSc2
Žemčug	Žemčug	k1gMnSc1
baron	baron	k1gMnSc1
Čerkasov	Čerkasov	k1gInSc4
<g/>
,	,	kIx,
nebyl	být	k5eNaImAgMnS
během	během	k7c2
útoku	útok	k1gInSc2
na	na	k7c6
palubě	paluba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
u	u	k7c2
své	svůj	k3xOyFgFnSc2
přítelkyně	přítelkyně	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
to	ten	k3xDgNnSc4
byl	být	k5eAaImAgInS
pro	pro	k7c4
nedbalost	nedbalost	k1gFnSc4
odsouzen	odsoudit	k5eAaPmNgMnS,k5eAaImNgMnS
k	k	k7c3
3,5	3,5	k4
rokům	rok	k1gInPc3
vězení	vězení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HRBEK	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c6
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc1
1914	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85983	#num#	k4
<g/>
-	-	kIx~
<g/>
84	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
258	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Námořní	námořní	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Penangu	Penang	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Kokosových	kokosový	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Coronelu	Coronel	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Falklandských	Falklandský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
•	•	k?
První	první	k4xOgFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
Helgolandské	helgolandský	k2eAgFnSc2d1
zátoky	zátoka	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Dogger	Dogger	k1gInSc4
Banku	banka	k1gFnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Jutska	Jutsko	k1gNnSc2
<g/>
/	/	kIx~
<g/>
Skagerraku	Skagerrak	k1gInSc6
•	•	k?
Druhá	druhý	k4xOgFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
Helgolandské	helgolandský	k2eAgFnSc2d1
zátoky	zátoka	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Loďstvo	loďstvo	k1gNnSc1
|	|	kIx~
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
