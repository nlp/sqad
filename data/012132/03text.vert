<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Bolesław	Bolesław	k1gFnPc4	Bolesław
I	i	k8xC	i
Chrobry	Chrobr	k1gInPc4	Chrobr
<g/>
,	,	kIx,	,
967	[number]	k4	967
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1025	[number]	k4	1025
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
polským	polský	k2eAgNnSc7d1	polské
knížetem	kníže	k1gNnSc7wR	kníže
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
992	[number]	k4	992
až	až	k9	až
1025	[number]	k4	1025
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
se	se	k3xPyFc4	se
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
papeže	papež	k1gMnSc2	papež
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Dočasně	dočasně	k6eAd1	dočasně
byl	být	k5eAaImAgInS	být
také	také	k9	také
vládcem	vládce	k1gMnSc7	vládce
Čech	Čechy	k1gFnPc2	Čechy
(	(	kIx(	(
<g/>
1003	[number]	k4	1003
<g/>
–	–	k?	–
<g/>
1004	[number]	k4	1004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
nejstaršího	starý	k2eAgInSc2d3	nejstarší
polského	polský	k2eAgInSc2d1	polský
panovnického	panovnický	k2eAgInSc2d1	panovnický
rodu	rod	k1gInSc2	rod
Piastovců	Piastovec	k1gMnPc2	Piastovec
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
prvního	první	k4xOgNnSc2	první
historicky	historicky	k6eAd1	historicky
doloženého	doložený	k2eAgNnSc2d1	doložené
knížete	kníže	k1gNnSc2wR	kníže
Měška	Měško	k1gNnSc2	Měško
I.	I.	kA	I.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
první	první	k4xOgFnPc1	první
manželky	manželka	k1gFnPc1	manželka
Přemyslovny	Přemyslovna	k1gFnSc2	Přemyslovna
Doubravky	Doubravka	k1gFnSc2	Doubravka
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
českého	český	k2eAgMnSc4d1	český
knížete	kníže	k1gMnSc4	kníže
Boleslava	Boleslav	k1gMnSc4	Boleslav
I.	I.	kA	I.
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
Měšek	Měšek	k1gMnSc1	Měšek
znovu	znovu	k6eAd1	znovu
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Odou	Odoum	k1gNnSc3	Odoum
z	z	k7c2	z
Haldenslebenu	Haldensleben	k2eAgFnSc4d1	Haldensleben
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
mladší	mladý	k2eAgInPc1d2	mladší
syny	syn	k1gMnPc4	syn
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
manželství	manželství	k1gNnSc2	manželství
upřednostnil	upřednostnit	k5eAaPmAgInS	upřednostnit
v	v	k7c6	v
nástupnických	nástupnický	k2eAgNnPc6d1	nástupnické
právech	právo	k1gNnPc6	právo
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
prvorozeného	prvorozený	k2eAgMnSc2d1	prvorozený
Boleslava	Boleslav	k1gMnSc2	Boleslav
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
vydělil	vydělit	k5eAaPmAgInS	vydělit
pouze	pouze	k6eAd1	pouze
úděl	úděl	k1gInSc1	úděl
v	v	k7c6	v
Malopolsku	Malopolsko	k1gNnSc6	Malopolsko
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
téměř	téměř	k6eAd1	téměř
okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
992	[number]	k4	992
<g/>
)	)	kIx)	)
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
bratry	bratr	k1gMnPc7	bratr
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
i	i	k9	i
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Velmože	velmož	k1gMnPc4	velmož
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
krutě	krutě	k6eAd1	krutě
potrestat	potrestat	k5eAaPmF	potrestat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přátelství	přátelství	k1gNnSc4	přátelství
s	s	k7c7	s
Otou	Ota	k1gMnSc7	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
zřízení	zřízení	k1gNnSc1	zřízení
hnězdenského	hnězdenský	k2eAgNnSc2d1	hnězdenský
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
==	==	k?	==
</s>
</p>
<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
ambiciózním	ambiciózní	k2eAgMnSc7d1	ambiciózní
a	a	k8xC	a
energickým	energický	k2eAgMnSc7d1	energický
panovníkem	panovník	k1gMnSc7	panovník
i	i	k8xC	i
schopným	schopný	k2eAgMnSc7d1	schopný
vojevůdcem	vojevůdce	k1gMnSc7	vojevůdce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
upevnění	upevnění	k1gNnSc4	upevnění
knížecí	knížecí	k2eAgFnSc2d1	knížecí
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc4	rozšíření
své	svůj	k3xOyFgFnSc2	svůj
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
povznesení	povznesení	k1gNnSc4	povznesení
její	její	k3xOp3gFnSc2	její
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
prestiže	prestiž	k1gFnSc2	prestiž
zřízením	zřízení	k1gNnSc7	zřízení
samostatné	samostatný	k2eAgFnSc2d1	samostatná
polské	polský	k2eAgFnSc2d1	polská
církevní	církevní	k2eAgFnSc2d1	církevní
organizace	organizace	k1gFnSc2	organizace
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
a	a	k8xC	a
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
královského	královský	k2eAgInSc2d1	královský
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
využít	využít	k5eAaPmF	využít
dobrých	dobrý	k2eAgInPc2d1	dobrý
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Otou	Ota	k1gMnSc7	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
papežskou	papežský	k2eAgFnSc7d1	Papežská
kurií	kurie	k1gFnSc7	kurie
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
tomu	ten	k3xDgNnSc3	ten
napomohla	napomoct	k5eAaPmAgFnS	napomoct
také	také	k9	také
mučednická	mučednický	k2eAgFnSc1d1	mučednická
smrt	smrt	k1gFnSc1	smrt
Slavníkovce	Slavníkovec	k1gMnSc2	Slavníkovec
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
,	,	kIx,	,
císařova	císařův	k2eAgMnSc2d1	císařův
přítele	přítel	k1gMnSc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyvraždění	vyvraždění	k1gNnSc6	vyvraždění
Slavníkovců	Slavníkovec	k1gInPc2	Slavníkovec
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
totiž	totiž	k9	totiž
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
<g/>
,	,	kIx,	,
využívaje	využívat	k5eAaImSgMnS	využívat
mocenských	mocenský	k2eAgInPc2d1	mocenský
rozporů	rozpor	k1gInPc2	rozpor
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
útočiště	útočiště	k1gNnSc4	útočiště
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
dvoře	dvůr	k1gInSc6	dvůr
těm	ten	k3xDgMnPc3	ten
členům	člen	k1gMnPc3	člen
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vraždění	vražděný	k2eAgMnPc1d1	vražděný
unikli	uniknout	k5eAaPmAgMnP	uniknout
<g/>
,	,	kIx,	,
pražskému	pražský	k2eAgMnSc3d1	pražský
biskupu	biskup	k1gMnSc3	biskup
Vojtěchovi	Vojtěch	k1gMnSc3	Vojtěch
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc3	jeho
nejstaršímu	starý	k2eAgMnSc3d3	nejstarší
bratru	bratr	k1gMnSc3	bratr
Soběborovi	Soběbor	k1gMnSc3	Soběbor
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc3	jejich
nevlastnímu	vlastní	k2eNgMnSc3d1	nevlastní
bratru	bratr	k1gMnSc3	bratr
Radimovi	Radim	k1gMnSc3	Radim
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
však	však	k9	však
chtěl	chtít	k5eAaImAgMnS	chtít
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
misii	misie	k1gFnSc6	misie
k	k	k7c3	k
pohanským	pohanský	k2eAgMnPc3d1	pohanský
Prusům	Prus	k1gMnPc3	Prus
<g/>
,	,	kIx,	,
a	a	k8xC	a
Boleslav	Boleslav	k1gMnSc1	Boleslav
jeho	jeho	k3xOp3gInPc4	jeho
plány	plán	k1gInPc4	plán
podpořil	podpořit	k5eAaPmAgMnS	podpořit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
nalezl	nalézt	k5eAaBmAgMnS	nalézt
roku	rok	k1gInSc2	rok
997	[number]	k4	997
v	v	k7c6	v
Prusích	Prusích	k?	Prusích
svoji	svůj	k3xOyFgFnSc4	svůj
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
vykoupil	vykoupit	k5eAaPmAgMnS	vykoupit
kníže	kníže	k1gMnSc1	kníže
jeho	jeho	k3xOp3gInPc4	jeho
ostatky	ostatek	k1gInPc4	ostatek
(	(	kIx(	(
<g/>
vyvážil	vyvážit	k5eAaPmAgMnS	vyvážit
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
zlatem	zlato	k1gNnSc7	zlato
<g/>
)	)	kIx)	)
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
je	být	k5eAaImIp3nS	být
důstojně	důstojně	k6eAd1	důstojně
uložit	uložit	k5eAaPmF	uložit
v	v	k7c6	v
hnězdenském	hnězdenský	k2eAgInSc6d1	hnězdenský
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
podnikli	podniknout	k5eAaPmAgMnP	podniknout
společně	společně	k6eAd1	společně
s	s	k7c7	s
Otou	Ota	k1gMnSc7	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
k	k	k7c3	k
Vojtěchovu	Vojtěchův	k2eAgInSc3d1	Vojtěchův
hrobu	hrob	k1gInSc3	hrob
pouť	pouť	k1gFnSc4	pouť
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
slavnostně	slavnostně	k6eAd1	slavnostně
uctili	uctít	k5eAaPmAgMnP	uctít
památku	památka	k1gFnSc4	památka
nového	nový	k2eAgMnSc2d1	nový
světce	světec	k1gMnSc2	světec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
císař	císař	k1gMnSc1	císař
zřídil	zřídit	k5eAaPmAgMnS	zřídit
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
papeže	papež	k1gMnSc2	papež
Silvestra	Silvestr	k1gMnSc2	Silvestr
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
sídelním	sídelní	k2eAgNnSc6d1	sídelní
Hnězdně	Hnězdno	k1gNnSc6	Hnězdno
arcidiecézi	arcidiecéze	k1gFnSc4	arcidiecéze
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc2	její
správy	správa	k1gFnSc2	správa
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
hnězdenský	hnězdenský	k2eAgMnSc1d1	hnězdenský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
ujal	ujmout	k5eAaPmAgMnS	ujmout
Slavníkovec	Slavníkovec	k1gMnSc1	Slavníkovec
Radim	Radim	k1gMnSc1	Radim
<g/>
)	)	kIx)	)
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
jí	on	k3xPp3gFnSc3	on
podřídil	podřídit	k5eAaPmAgMnS	podřídit
současně	současně	k6eAd1	současně
založená	založený	k2eAgNnPc1d1	založené
biskupství	biskupství	k1gNnPc1	biskupství
se	s	k7c7	s
sídly	sídlo	k1gNnPc7	sídlo
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
,	,	kIx,	,
Vratislavi	Vratislav	k1gFnSc6	Vratislav
a	a	k8xC	a
Kolobřehu	Kolobřeh	k1gInSc6	Kolobřeh
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
již	již	k6eAd1	již
existující	existující	k2eAgFnSc4d1	existující
diecézi	diecéze	k1gFnSc4	diecéze
poznaňskou	poznaňský	k2eAgFnSc4d1	Poznaňská
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
polského	polský	k2eAgMnSc2d1	polský
kronikáře	kronikář	k1gMnSc2	kronikář
Galla	Gall	k1gMnSc2	Gall
Anonyma	anonym	k1gMnSc2	anonym
sňal	snít	k5eAaPmAgInS	snít
prý	prý	k9	prý
Ota	Ota	k1gMnSc1	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
sezení	sezení	k1gNnSc1	sezení
"	"	kIx"	"
<g/>
svůj	svůj	k3xOyFgInSc4	svůj
diadém	diadém	k1gInSc4	diadém
<g/>
"	"	kIx"	"
a	a	k8xC	a
vložil	vložit	k5eAaPmAgMnS	vložit
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
skráně	skráň	k1gFnSc2	skráň
Boleslava	Boleslav	k1gMnSc2	Boleslav
Chrabrého	chrabrý	k2eAgMnSc2d1	chrabrý
<g/>
,	,	kIx,	,
nazývaje	nazývat	k5eAaImSgMnS	nazývat
ho	on	k3xPp3gMnSc4	on
"	"	kIx"	"
<g/>
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
spojencem	spojenec	k1gMnSc7	spojenec
římského	římský	k2eAgInSc2d1	římský
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
říše	říš	k1gFnSc2	říš
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Symbolicky	symbolicky	k6eAd1	symbolicky
tak	tak	k6eAd1	tak
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
polský	polský	k2eAgMnSc1d1	polský
panovník	panovník	k1gMnSc1	panovník
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
programu	program	k1gInSc6	program
obnovy	obnova	k1gFnSc2	obnova
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
jakýmsi	jakýsi	k3yIgMnSc7	jakýsi
představitelem	představitel	k1gMnSc7	představitel
"	"	kIx"	"
<g/>
Sclavinie	Sclavinie	k1gFnSc1	Sclavinie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
rámci	rámec	k1gInSc6	rámec
<g/>
.	.	kIx.	.
</s>
<s>
Povýšení	povýšení	k1gNnSc1	povýšení
na	na	k7c4	na
krále	král	k1gMnSc4	král
to	ten	k3xDgNnSc1	ten
však	však	k9	však
Boleslavu	Boleslav	k1gMnSc3	Boleslav
Chrabrému	chrabrý	k2eAgInSc3d1	chrabrý
nevyneslo	vynést	k5eNaPmAgNnS	vynést
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
a	a	k8xC	a
český	český	k2eAgInSc1d1	český
stát	stát	k1gInSc1	stát
==	==	k?	==
</s>
</p>
<p>
<s>
Oslabení	oslabení	k1gNnSc1	oslabení
českého	český	k2eAgNnSc2d1	české
knížectví	knížectví	k1gNnSc2	knížectví
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
999	[number]	k4	999
<g/>
)	)	kIx)	)
využil	využít	k5eAaPmAgMnS	využít
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
Chrabrý	chrabrý	k2eAgInSc1d1	chrabrý
k	k	k7c3	k
přímým	přímý	k2eAgInPc3d1	přímý
vojenským	vojenský	k2eAgInPc3d1	vojenský
zásahům	zásah	k1gInPc3	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
vzápětí	vzápětí	k6eAd1	vzápětí
přepadl	přepadnout	k5eAaPmAgMnS	přepadnout
českou	český	k2eAgFnSc4d1	Česká
posádku	posádka	k1gFnSc4	posádka
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
,	,	kIx,	,
pobil	pobít	k5eAaPmAgMnS	pobít
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
připojil	připojit	k5eAaPmAgInS	připojit
Malopolsko	Malopolsko	k1gNnSc4	Malopolsko
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
osud	osud	k1gInSc1	osud
potkal	potkat	k5eAaPmAgInS	potkat
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
i	i	k9	i
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
část	část	k1gFnSc4	část
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc4	jenž
toužili	toužit	k5eAaImAgMnP	toužit
získat	získat	k5eAaPmF	získat
uherští	uherský	k2eAgMnPc1d1	uherský
Arpádovci	Arpádovec	k1gMnPc1	Arpádovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc1d1	další
příležitost	příležitost	k1gFnSc1	příležitost
mu	on	k3xPp3gMnSc3	on
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
nesváry	nesvár	k1gInPc4	nesvár
uvnitř	uvnitř	k7c2	uvnitř
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1002	[number]	k4	1002
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
vyhnán	vyhnán	k2eAgMnSc1d1	vyhnán
neschopný	schopný	k2eNgMnSc1d1	neschopný
a	a	k8xC	a
krutý	krutý	k2eAgMnSc1d1	krutý
vládce	vládce	k1gMnSc1	vládce
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
obsadil	obsadit	k5eAaPmAgMnS	obsadit
polský	polský	k2eAgMnSc1d1	polský
kníže	kníže	k1gMnSc1	kníže
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
na	na	k7c4	na
pražský	pražský	k2eAgInSc4d1	pražský
knížecí	knížecí	k2eAgInSc4d1	knížecí
stolec	stolec	k1gInSc4	stolec
dosadil	dosadit	k5eAaPmAgMnS	dosadit
jakéhosi	jakýsi	k3yIgInSc2	jakýsi
Vladivoje	Vladivoj	k1gInSc2	Vladivoj
<g/>
,	,	kIx,	,
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
snad	snad	k9	snad
přemyslovského	přemyslovský	k2eAgInSc2d1	přemyslovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
záhy	záhy	k6eAd1	záhy
nato	nato	k6eAd1	nato
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
již	již	k9	již
také	také	k9	také
s	s	k7c7	s
polskou	polský	k2eAgFnSc7d1	polská
pomocí	pomoc	k1gFnSc7	pomoc
<g/>
,	,	kIx,	,
vyhnaný	vyhnaný	k2eAgMnSc1d1	vyhnaný
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
slíbil	slíbit	k5eAaPmAgMnS	slíbit
milost	milost	k1gFnSc4	milost
všem	všecek	k3xTgMnPc3	všecek
svým	svůj	k3xOyFgMnPc3	svůj
protivníkům	protivník	k1gMnPc3	protivník
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
vzápětí	vzápětí	k6eAd1	vzápětí
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
vyvraždit	vyvraždit	k5eAaPmF	vyvraždit
příslušníky	příslušník	k1gMnPc4	příslušník
rodu	rod	k1gInSc2	rod
Vršovců	Vršovec	k1gMnPc2	Vršovec
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
pocházel	pocházet	k5eAaImAgMnS	pocházet
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
zeť	zeť	k1gMnSc1	zeť
<g/>
.	.	kIx.	.
</s>
<s>
Čechové	Čechové	k2eAgNnSc1d1	Čechové
rozezlení	rozezlení	k1gNnSc1	rozezlení
takovým	takový	k3xDgInSc7	takový
bezectným	bezectný	k2eAgInSc7d1	bezectný
činem	čin	k1gInSc7	čin
se	se	k3xPyFc4	se
obrátili	obrátit	k5eAaPmAgMnP	obrátit
s	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
na	na	k7c4	na
hnězdenský	hnězdenský	k2eAgInSc4d1	hnězdenský
dvůr	dvůr	k1gInSc4	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
povolal	povolat	k5eAaPmAgMnS	povolat
Boleslava	Boleslav	k1gMnSc4	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgInS	nechat
pobít	pobít	k5eAaPmF	pobít
jeho	jeho	k3xOp3gInSc1	jeho
doprovod	doprovod	k1gInSc1	doprovod
<g/>
,	,	kIx,	,
knížete	kníže	k1gNnSc4wR	kníže
pak	pak	k6eAd1	pak
oslepit	oslepit	k5eAaPmF	oslepit
a	a	k8xC	a
uvěznit	uvěznit	k5eAaPmF	uvěznit
(	(	kIx(	(
<g/>
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
v	v	k7c6	v
žaláři	žalář	k1gInSc6	žalář
zůstal	zůstat	k5eAaPmAgMnS	zůstat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1037	[number]	k4	1037
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgInSc1d1	chrabrý
ujal	ujmout	k5eAaPmAgInS	ujmout
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
knížectví	knížectví	k1gNnSc6	knížectví
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
ostatně	ostatně	k6eAd1	ostatně
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
z	z	k7c2	z
přemyslovského	přemyslovský	k2eAgInSc2d1	přemyslovský
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
bratrancem	bratranec	k1gMnSc7	bratranec
sesazeného	sesazený	k2eAgNnSc2d1	sesazené
knížete	kníže	k1gNnSc2wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
historiografie	historiografie	k1gFnSc1	historiografie
razila	razit	k5eAaImAgFnS	razit
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtěl	chtít	k5eAaImAgMnS	chtít
vytvořit	vytvořit	k5eAaPmF	vytvořit
pod	pod	k7c7	pod
svojí	svůj	k3xOyFgFnSc7	svůj
vládou	vláda	k1gFnSc7	vláda
mohutnou	mohutný	k2eAgFnSc4d1	mohutná
západoslovanskou	západoslovanský	k2eAgFnSc4d1	západoslovanská
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
Boleslava	Boleslav	k1gMnSc2	Boleslav
Chrabrého	chrabrý	k2eAgInSc2d1	chrabrý
zpočátku	zpočátku	k6eAd1	zpočátku
vítali	vítat	k5eAaImAgMnP	vítat
jako	jako	k8xC	jako
osvoboditele	osvoboditel	k1gMnSc4	osvoboditel
od	od	k7c2	od
Boleslavovy	Boleslavův	k2eAgFnSc2d1	Boleslavova
krutovlády	krutovláda	k1gFnSc2	krutovláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
uznání	uznání	k1gNnSc4	uznání
si	se	k3xPyFc3	se
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
problém	problém	k1gInSc1	problém
však	však	k9	však
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
představovala	představovat	k5eAaImAgFnS	představovat
změna	změna	k1gFnSc1	změna
na	na	k7c6	na
říšském	říšský	k2eAgInSc6d1	říšský
trůně	trůn	k1gInSc6	trůn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Oty	Ota	k1gMnSc2	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1002	[number]	k4	1002
<g/>
)	)	kIx)	)
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Jindřich	Jindřich	k1gMnSc1	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nepřiznal	přiznat	k5eNaPmAgMnS	přiznat
Boleslavu	Boleslav	k1gMnSc3	Boleslav
Chrabrému	chrabrý	k2eAgNnSc3d1	chrabré
postavení	postavení	k1gNnSc3	postavení
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
nabyl	nabýt	k5eAaPmAgInS	nabýt
za	za	k7c2	za
jeho	on	k3xPp3gMnSc2	on
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
mu	on	k3xPp3gMnSc3	on
sice	sice	k8xC	sice
udělení	udělení	k1gNnSc3	udělení
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
<g/>
,	,	kIx,	,
když	když	k8xS	když
však	však	k9	však
ctižádostivý	ctižádostivý	k2eAgMnSc1d1	ctižádostivý
kníže	kníže	k1gMnSc1	kníže
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
z	z	k7c2	z
knížectví	knížectví	k1gNnSc2	knížectví
vypudit	vypudit	k5eAaPmF	vypudit
a	a	k8xC	a
nastolit	nastolit	k5eAaPmF	nastolit
právoplatného	právoplatný	k2eAgMnSc4d1	právoplatný
dědice	dědic	k1gMnSc4	dědic
českého	český	k2eAgInSc2d1	český
trůnu	trůn	k1gInSc2	trůn
Přemyslovce	Přemyslovec	k1gMnSc4	Přemyslovec
Jaromíra	Jaromír	k1gMnSc4	Jaromír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rychlý	rychlý	k2eAgInSc1d1	rychlý
útok	útok	k1gInSc1	útok
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Poláky	Polák	k1gMnPc4	Polák
překvapil	překvapit	k5eAaPmAgInS	překvapit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
tak	tak	k6eAd1	tak
řečený	řečený	k2eAgMnSc1d1	řečený
Dalimil	Dalimil	k1gMnSc1	Dalimil
jistě	jistě	k9	jistě
přehání	přehánět	k5eAaImIp3nS	přehánět
<g/>
,	,	kIx,	,
když	když	k8xS	když
píše	psát	k5eAaImIp3nS	psát
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
"	"	kIx"	"
<g/>
polské	polský	k2eAgNnSc1d1	polské
vojsko	vojsko	k1gNnSc1	vojsko
prchá	prchat	k5eAaImIp3nS	prchat
v	v	k7c6	v
děsu	děs	k1gInSc6	děs
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Polané	polaný	k2eAgNnSc1d1	Polané
se	se	k3xPyFc4	se
v	v	k7c6	v
zmatku	zmatek	k1gInSc6	zmatek
plazí	plazit	k5eAaImIp3nS	plazit
přes	přes	k7c4	přes
valy	val	k1gInPc4	val
a	a	k8xC	a
srázy	sráz	k1gInPc4	sráz
nazí	nahý	k2eAgMnPc1d1	nahý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
Čechy	Čechy	k1gFnPc4	Čechy
urychleně	urychleně	k6eAd1	urychleně
opustil	opustit	k5eAaPmAgMnS	opustit
(	(	kIx(	(
<g/>
1004	[number]	k4	1004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
posledními	poslední	k2eAgMnPc7d1	poslední
polskými	polský	k2eAgMnPc7d1	polský
bojovníky	bojovník	k1gMnPc7	bojovník
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
poslední	poslední	k2eAgMnSc1d1	poslední
Slavníkovec	Slavníkovec	k1gMnSc1	Slavníkovec
Soběslav	Soběslav	k1gMnSc1	Soběslav
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
vzápětí	vzápětí	k6eAd1	vzápětí
nato	nato	k6eAd1	nato
zabit	zabít	k5eAaPmNgMnS	zabít
na	na	k7c6	na
mostě	most	k1gInSc6	most
přes	přes	k7c4	přes
hradní	hradní	k2eAgInSc4d1	hradní
příkop	příkop	k1gInSc4	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
však	však	k9	však
dále	daleko	k6eAd2	daleko
ovládal	ovládat	k5eAaImAgInS	ovládat
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
postupně	postupně	k6eAd1	postupně
dobyl	dobýt	k5eAaPmAgInS	dobýt
až	až	k9	až
kníže	kníže	k1gMnSc1	kníže
Oldřich	Oldřich	k1gMnSc1	Oldřich
kolem	kolem	k7c2	kolem
r.	r.	kA	r.
1019	[number]	k4	1019
(	(	kIx(	(
<g/>
možná	možná	k9	možná
i	i	k9	i
1029	[number]	k4	1029
–	–	k?	–
přímý	přímý	k2eAgInSc4d1	přímý
záznam	záznam	k1gInSc4	záznam
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
datace	datace	k1gFnSc1	datace
je	být	k5eAaImIp3nS	být
nejistá	jistý	k2eNgFnSc1d1	nejistá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konflikt	konflikt	k1gInSc4	konflikt
s	s	k7c7	s
říší	říš	k1gFnSc7	říš
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
expanze	expanze	k1gFnSc1	expanze
==	==	k?	==
</s>
</p>
<p>
<s>
Vypuzení	vypuzení	k1gNnSc1	vypuzení
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
ale	ale	k9	ale
neznamenalo	znamenat	k5eNaImAgNnS	znamenat
konec	konec	k1gInSc4	konec
Boleslavova	Boleslavův	k2eAgInSc2d1	Boleslavův
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jeho	jeho	k3xOp3gInSc4	jeho
začátek	začátek	k1gInSc4	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1002	[number]	k4	1002
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
Boleslav	Boleslav	k1gMnSc1	Boleslav
do	do	k7c2	do
Lužice	Lužice	k1gFnSc2	Lužice
a	a	k8xC	a
obsadil	obsadit	k5eAaPmAgMnS	obsadit
město	město	k1gNnSc4	město
Budyšín	Budyšín	k1gInSc1	Budyšín
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výpad	výpad	k1gInSc1	výpad
proti	proti	k7c3	proti
Míšni	Míšeň	k1gFnSc3	Míšeň
byl	být	k5eAaImAgInS	být
však	však	k9	však
neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
přijmout	přijmout	k5eAaPmF	přijmout
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
mu	on	k3xPp3gMnSc3	on
pak	pak	k9	pak
Lužici	Lužice	k1gFnSc4	Lužice
udělil	udělit	k5eAaPmAgInS	udělit
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
<g/>
.	.	kIx.	.
</s>
<s>
Války	válka	k1gFnPc1	válka
s	s	k7c7	s
říší	říš	k1gFnSc7	říš
vedlo	vést	k5eAaImAgNnS	vést
Polsko	Polsko	k1gNnSc1	Polsko
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1003	[number]	k4	1003
celých	celý	k2eAgNnPc2d1	celé
patnáct	patnáct	k4xCc4	patnáct
let	léto	k1gNnPc2	léto
a	a	k8xC	a
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vyšel	vyjít	k5eAaPmAgInS	vyjít
jako	jako	k9	jako
vítěz	vítěz	k1gMnSc1	vítěz
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
se	se	k3xPyFc4	se
Jindřich	Jindřich	k1gMnSc1	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
všemožně	všemožně	k6eAd1	všemožně
snažil	snažit	k5eAaImAgMnS	snažit
omezit	omezit	k5eAaPmF	omezit
samostatné	samostatný	k2eAgNnSc4d1	samostatné
postavení	postavení	k1gNnSc4	postavení
polského	polský	k2eAgInSc2d1	polský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
přinutit	přinutit	k5eAaPmF	přinutit
Boleslava	Boleslav	k1gMnSc4	Boleslav
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
vazalem	vazal	k1gMnSc7	vazal
<g/>
.	.	kIx.	.
</s>
<s>
Budyšínský	budyšínský	k2eAgInSc1d1	budyšínský
mír	mír	k1gInSc1	mír
(	(	kIx(	(
<g/>
1018	[number]	k4	1018
<g/>
)	)	kIx)	)
mu	on	k3xPp3gMnSc3	on
zajistil	zajistit	k5eAaPmAgMnS	zajistit
držbu	držba	k1gFnSc4	držba
říšských	říšský	k2eAgNnPc2d1	říšské
lén	léno	k1gNnPc2	léno
Milska	Milsk	k1gInSc2	Milsk
a	a	k8xC	a
Lužice	Lužice	k1gFnSc2	Lužice
a	a	k8xC	a
také	také	k9	také
Moravy	Morava	k1gFnSc2	Morava
(	(	kIx(	(
<g/>
kterou	který	k3yRgFnSc4	který
však	však	k9	však
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
ztratil	ztratit	k5eAaPmAgMnS	ztratit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mírová	mírový	k2eAgNnPc1d1	Mírové
ujednání	ujednání	k1gNnPc1	ujednání
byla	být	k5eAaImAgNnP	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
Boleslavovým	Boleslavův	k2eAgInSc7d1	Boleslavův
sňatkem	sňatek	k1gInSc7	sňatek
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
míšeňského	míšeňský	k2eAgMnSc4d1	míšeňský
markraběte	markrabě	k1gMnSc4	markrabě
Odou	Odoa	k1gMnSc4	Odoa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Expanzívní	expanzívní	k2eAgFnPc1d1	expanzívní
snahy	snaha	k1gFnPc1	snaha
Boleslava	Boleslav	k1gMnSc2	Boleslav
I.	I.	kA	I.
nesměřovaly	směřovat	k5eNaImAgInP	směřovat
jen	jen	k9	jen
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
Rusi	Rus	k1gFnSc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
roku	rok	k1gInSc3	rok
1018	[number]	k4	1018
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
nazpět	nazpět	k6eAd1	nazpět
Červoňské	Červoňský	k2eAgInPc4d1	Červoňský
hrady	hrad	k1gInPc4	hrad
a	a	k8xC	a
dosadit	dosadit	k5eAaPmF	dosadit
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
jen	jen	k9	jen
dočasně	dočasně	k6eAd1	dočasně
<g/>
,	,	kIx,	,
na	na	k7c4	na
kyjevský	kyjevský	k2eAgInSc4d1	kyjevský
trůn	trůn	k1gInSc4	trůn
právoplatného	právoplatný	k2eAgMnSc2d1	právoplatný
dědice	dědic	k1gMnSc2	dědic
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgMnSc4	svůj
zetě	zeť	k1gMnSc4	zeť
Svjatopolka	Svjatopolka	k1gFnSc1	Svjatopolka
<g/>
.	.	kIx.	.
</s>
<s>
Mírový	mírový	k2eAgInSc1d1	mírový
stav	stav	k1gInSc1	stav
mezi	mezi	k7c7	mezi
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
říší	říš	k1gFnSc7	říš
trval	trvat	k5eAaImAgMnS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
císaře	císař	k1gMnSc2	císař
Jindřicha	Jindřich	k1gMnSc2	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1024	[number]	k4	1024
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
i	i	k9	i
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nebyl	být	k5eNaImAgInS	být
Polsku	Polska	k1gFnSc4	Polska
příznivě	příznivě	k6eAd1	příznivě
nakloněn	nakloněn	k2eAgMnSc1d1	nakloněn
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgMnS	začít
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
energicky	energicky	k6eAd1	energicky
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
s	s	k7c7	s
papežskou	papežský	k2eAgFnSc7d1	Papežská
kurií	kurie	k1gFnSc7	kurie
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
královské	královský	k2eAgFnSc6d1	královská
korunovaci	korunovace	k1gFnSc6	korunovace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
by	by	kYmCp3nP	by
oba	dva	k4xCgMnPc1	dva
zesnulí	zesnulý	k1gMnPc1	zesnulý
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
připustili	připustit	k5eAaPmAgMnP	připustit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1025	[number]	k4	1025
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
Hnězdně	Hnězdno	k1gNnSc6	Hnězdno
korunovat	korunovat	k5eAaBmF	korunovat
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
z	z	k7c2	z
příkazu	příkaz	k1gInSc2	příkaz
nového	nový	k2eAgMnSc2d1	nový
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
však	však	k9	však
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
Jindřichova	Jindřichův	k2eAgMnSc2d1	Jindřichův
nástupce	nástupce	k1gMnSc2	nástupce
na	na	k7c6	na
císařském	císařský	k2eAgInSc6d1	císařský
trůně	trůn	k1gInSc6	trůn
Konráda	Konrád	k1gMnSc2	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
papežské	papežský	k2eAgNnSc4d1	papežské
léno	léno	k1gNnSc4	léno
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
nato	nato	k6eAd1	nato
Boleslav	Boleslav	k1gMnSc1	Boleslav
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
polský	polský	k2eAgInSc1d1	polský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
mezi	mezi	k7c4	mezi
přední	přední	k2eAgFnPc4d1	přední
středoevropské	středoevropský	k2eAgFnPc4d1	středoevropská
mocnosti	mocnost	k1gFnPc4	mocnost
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgInS	zahájit
pozvolný	pozvolný	k2eAgInSc1d1	pozvolný
sestup	sestup	k1gInSc1	sestup
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
významného	významný	k2eAgNnSc2d1	významné
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Genealogie	genealogie	k1gFnSc2	genealogie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Grabski	Grabski	k1gNnSc1	Grabski
A.	A.	kA	A.
F.	F.	kA	F.
Bolesław	Bolesław	k1gMnSc2	Bolesław
Chrobry	Chrobra	k1gMnSc2	Chrobra
<g/>
.	.	kIx.	.
</s>
<s>
Zarys	Zarys	k6eAd1	Zarys
dziejów	dziejów	k?	dziejów
politycznych	politycznycha	k1gFnPc2	politycznycha
i	i	k8xC	i
wojskowych	wojskowycha	k1gFnPc2	wojskowycha
<g/>
.	.	kIx.	.
</s>
<s>
Warszawa	Warszawa	k1gFnSc1	Warszawa
:	:	kIx,	:
Wydawnictwo	Wydawnictwo	k1gNnSc1	Wydawnictwo
Ministerstwa	Ministerstw	k1gInSc2	Ministerstw
Obrony	Obrona	k1gFnSc2	Obrona
Narodowej	Narodowej	k1gInSc1	Narodowej
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
321	[number]	k4	321
s.	s.	k?	s.
</s>
</p>
<p>
<s>
DVORNÍK	dvorník	k1gMnSc1	dvorník
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Zrod	zrod	k1gInSc1	zrod
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Byzancí	Byzanc	k1gFnSc7	Byzanc
a	a	k8xC	a
Římem	Řím	k1gInSc7	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
528	[number]	k4	528
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7260	[number]	k4	7260
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gallus	Gallus	k1gMnSc1	Gallus
Anonymus	Anonymus	k?	Anonymus
<g/>
.	.	kIx.	.
</s>
<s>
Kroniky	kronika	k1gFnPc1	kronika
a	a	k8xC	a
činy	čin	k1gInPc1	čin
polských	polský	k2eAgMnPc2d1	polský
knížat	kníže	k1gMnPc2wR	kníže
a	a	k8xC	a
vládců	vládce	k1gMnPc2	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
162	[number]	k4	162
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
206	[number]	k4	206
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
MERSEBURKU	MERSEBURKU	kA	MERSEBURKU
<g/>
,	,	kIx,	,
Dětmar	Dětmar	k1gInSc1	Dětmar
<g/>
.	.	kIx.	.
</s>
<s>
Kronika	kronika	k1gFnSc1	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
342	[number]	k4	342
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
88	[number]	k4	88
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
Od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
knížete	kníže	k1gMnSc2	kníže
Oldřicha	Oldřich	k1gMnSc2	Oldřich
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
782	[number]	k4	782
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
61	[number]	k4	61
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
6	[number]	k4	6
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Boh	Boh	k1gFnSc1	Boh
<g/>
–	–	k?	–
<g/>
Bož	Boža	k1gFnPc2	Boža
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
109	[number]	k4	109
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
34	[number]	k4	34
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strzelczyk	Strzelczyk	k1gInSc1	Strzelczyk
<g/>
,	,	kIx,	,
Jerzy	Jerz	k1gInPc1	Jerz
<g/>
:	:	kIx,	:
Bolesław	Bolesław	k1gFnSc1	Bolesław
Chrobry	Chrobra	k1gFnSc2	Chrobra
<g/>
.	.	kIx.	.
</s>
<s>
Poznań	Poznań	k?	Poznań
:	:	kIx,	:
WBPiCAK	WBPiCAK	k1gMnSc1	WBPiCAK
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
296	[number]	k4	296
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
85811	[number]	k4	85811
<g/>
-	-	kIx~	-
<g/>
88	[number]	k4	88
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strzelczyk	Strzelczyk	k1gInSc1	Strzelczyk
<g/>
,	,	kIx,	,
Jerzy	Jerz	k1gInPc1	Jerz
<g/>
:	:	kIx,	:
Zjazd	Zjazd	k1gInSc1	Zjazd
gnieźnieński	gnieźnieńsk	k1gFnSc2	gnieźnieńsk
<g/>
.	.	kIx.	.
</s>
<s>
Poznań	Poznań	k?	Poznań
:	:	kIx,	:
Wydaw	Wydaw	k1gMnSc1	Wydaw
<g/>
.	.	kIx.	.
</s>
<s>
WBP	WBP	kA	WBP
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
147	[number]	k4	147
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
85811	[number]	k4	85811
<g/>
-	-	kIx~	-
<g/>
73	[number]	k4	73
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Balzer	Balzer	k1gMnSc1	Balzer
<g/>
,	,	kIx,	,
Oswald	Oswald	k1gMnSc1	Oswald
<g/>
:	:	kIx,	:
Genealogia	Genealogia	k1gFnSc1	Genealogia
Piastów	Piastów	k1gFnSc1	Piastów
<g/>
.	.	kIx.	.
</s>
<s>
Kraków	Kraków	k?	Kraków
:	:	kIx,	:
Wydawnictwo	Wydawnictwo	k6eAd1	Wydawnictwo
Avalon	Avalon	k1gInSc1	Avalon
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
1020	[number]	k4	1020
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
918497	[number]	k4	918497
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jasiński	Jasińsk	k1gMnPc1	Jasińsk
<g/>
,	,	kIx,	,
Kazimierz	Kazimierz	k1gMnSc1	Kazimierz
<g/>
:	:	kIx,	:
Rodowód	Rodowód	k1gMnSc1	Rodowód
pierwszych	pierwszych	k1gMnSc1	pierwszych
Piastów	Piastów	k1gMnSc1	Piastów
<g/>
.	.	kIx.	.
</s>
<s>
Poznań	Poznań	k?	Poznań
:	:	kIx,	:
Wydaw	Wydaw	k1gMnSc1	Wydaw
<g/>
.	.	kIx.	.
</s>
<s>
PTPN	PTPN	kA	PTPN
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
306	[number]	k4	306
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
7063	[number]	k4	7063
<g/>
-	-	kIx~	-
<g/>
409	[number]	k4	409
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Burislav	Burislav	k1gMnSc1	Burislav
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
KDO	kdo	k3yInSc1	kdo
BYL	být	k5eAaImAgMnS	být
KDO	kdo	k3yInSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
</s>
</p>
<p>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Boleslava	Boleslav	k1gMnSc2	Boleslav
Chrabrého	chrabrý	k2eAgMnSc2d1	chrabrý
</s>
</p>
