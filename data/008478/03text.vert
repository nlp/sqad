<p>
<s>
Fuente	Fuent	k1gMnSc5	Fuent
de	de	k?	de
Cantos	Cantos	k1gInSc4	Cantos
je	být	k5eAaImIp3nS	být
španělské	španělský	k2eAgNnSc1d1	španělské
město	město	k1gNnSc1	město
situované	situovaný	k2eAgNnSc1d1	situované
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Badajoz	Badajoz	k1gInSc1	Badajoz
(	(	kIx(	(
<g/>
autonomní	autonomní	k2eAgNnSc1d1	autonomní
společenství	společenství	k1gNnSc1	společenství
Extremadura	Extremadura	k1gFnSc1	Extremadura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
vzdálena	vzdálen	k2eAgFnSc1d1	vzdálena
80	[number]	k4	80
km	km	kA	km
od	od	k7c2	od
Méridy	Mérida	k1gFnSc2	Mérida
a	a	k8xC	a
100	[number]	k4	100
km	km	kA	km
od	od	k7c2	od
města	město	k1gNnSc2	město
Badajoz	Badajoza	k1gFnPc2	Badajoza
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
okresu	okres	k1gInSc2	okres
Tentudía	Tentudí	k1gInSc2	Tentudí
a	a	k8xC	a
soudního	soudní	k2eAgInSc2d1	soudní
okresu	okres	k1gInSc2	okres
Zafra	Zafr	k1gInSc2	Zafr
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
římské	římský	k2eAgFnSc6d1	římská
cestě	cesta	k1gFnSc6	cesta
Vía	Vía	k1gFnPc2	Vía
de	de	k?	de
la	la	k1gNnSc4	la
Platě	platit	k5eAaImSgInS	platit
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc3d1	dnešní
národní	národní	k2eAgFnSc3d1	národní
silnici	silnice	k1gFnSc3	silnice
N-	N-	k1gFnSc1	N-
<g/>
630	[number]	k4	630
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
dálnici	dálnice	k1gFnSc4	dálnice
A-	A-	k1gFnSc2	A-
<g/>
66	[number]	k4	66
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
čítala	čítat	k5eAaImAgFnS	čítat
obec	obec	k1gFnSc1	obec
1	[number]	k4	1
178	[number]	k4	178
usedlostí	usedlost	k1gFnPc2	usedlost
a	a	k8xC	a
4	[number]	k4	4
500	[number]	k4	500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
