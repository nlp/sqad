<p>
<s>
Camembert	camembert	k1gInSc1	camembert
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgInSc1d1	měkký
sýr	sýr	k1gInSc1	sýr
krémovité	krémovitý	k2eAgFnSc2d1	krémovitá
konzistence	konzistence	k1gFnSc2	konzistence
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
francouzské	francouzský	k2eAgFnSc6d1	francouzská
vesnici	vesnice	k1gFnSc6	vesnice
Camembert	camembert	k1gInSc4	camembert
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Orne	Orn	k1gFnSc2	Orn
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Normandský	normandský	k2eAgInSc1d1	normandský
camembert	camembert	k1gInSc1	camembert
získal	získat	k5eAaPmAgInS	získat
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
označení	označení	k1gNnSc2	označení
Apellation	Apellation	k1gInSc1	Apellation
d	d	k?	d
́	́	k?	́
<g/>
Origine	Origin	k1gMnSc5	Origin
Contrôlée	Contrôléus	k1gMnSc5	Contrôléus
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
povolena	povolit	k5eAaPmNgFnS	povolit
v	v	k7c6	v
departementech	departement	k1gInPc6	departement
Seine-Maritime	Seine-Maritim	k1gInSc5	Seine-Maritim
<g/>
,	,	kIx,	,
Calvados	Calvadosa	k1gFnPc2	Calvadosa
<g/>
,	,	kIx,	,
Eure	Eure	k1gFnPc2	Eure
<g/>
,	,	kIx,	,
Manche	Manche	k1gFnPc2	Manche
a	a	k8xC	a
Orne	Orne	k1gFnPc2	Orne
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
původu	původ	k1gInSc6	původ
sýru	sýr	k1gInSc2	sýr
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
plísní	plíseň	k1gFnSc7	plíseň
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
francouzského	francouzský	k2eAgInSc2d1	francouzský
Camembertu	camembert	k1gInSc2	camembert
<g/>
,	,	kIx,	,
panují	panovat	k5eAaImIp3nP	panovat
různé	různý	k2eAgFnPc1d1	různá
legendy	legenda	k1gFnPc1	legenda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jistá	jistý	k2eAgFnSc1d1	jistá
selka	selka	k1gFnSc1	selka
zapomněla	zapomnět	k5eAaImAgFnS	zapomnět
sýr	sýr	k1gInSc4	sýr
ve	v	k7c6	v
sklepě	sklep	k1gInSc6	sklep
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ho	on	k3xPp3gInSc4	on
po	po	k7c6	po
čase	čas	k1gInSc6	čas
našla	najít	k5eAaPmAgFnS	najít
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
obrostlý	obrostlý	k2eAgInSc1d1	obrostlý
bílou	bílý	k2eAgFnSc7d1	bílá
plísní	plíseň	k1gFnSc7	plíseň
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
ho	on	k3xPp3gNnSc4	on
chtěla	chtít	k5eAaImAgFnS	chtít
vyhodit	vyhodit	k5eAaPmF	vyhodit
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
ochutnala	ochutnat	k5eAaPmAgFnS	ochutnat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
překvapena	překvapen	k2eAgFnSc1d1	překvapena
jeho	jeho	k3xOp3gFnSc7	jeho
báječnou	báječný	k2eAgFnSc7d1	báječná
chutí	chuť	k1gFnSc7	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
písemné	písemný	k2eAgFnPc1d1	písemná
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
typu	typ	k1gInSc6	typ
sýru	sýr	k1gInSc2	sýr
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1554	[number]	k4	1554
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
tento	tento	k3xDgInSc4	tento
sýr	sýr	k1gInSc4	sýr
skutečně	skutečně	k6eAd1	skutečně
proslavil	proslavit	k5eAaPmAgMnS	proslavit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Marie	Marie	k1gFnSc1	Marie
la	la	k1gNnSc2	la
Fontaine	Fontain	k1gInSc5	Fontain
Harel	Harlo	k1gNnPc2	Harlo
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
Roiville	Roiville	k1gFnSc2	Roiville
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
dcera	dcera	k1gFnSc1	dcera
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
tohoto	tento	k3xDgInSc2	tento
sýru	sýr	k1gInSc2	sýr
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
ona	onen	k3xDgFnSc1	onen
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
jej	on	k3xPp3gMnSc4	on
nazvala	nazvat	k5eAaPmAgFnS	nazvat
podle	podle	k7c2	podle
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
města	město	k1gNnSc2	město
Camembert	camembert	k1gInSc1	camembert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Camembert	camembert	k1gInSc1	camembert
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
nepasterovaného	pasterovaný	k2eNgNnSc2d1	nepasterované
kravského	kravský	k2eAgNnSc2d1	kravské
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
zraje	zrát	k5eAaImIp3nS	zrát
ve	v	k7c6	v
formách	forma	k1gFnPc6	forma
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
plísní	plísnit	k5eAaImIp3nS	plísnit
Penicillium	Penicillium	k1gNnSc1	Penicillium
camemberti	camembert	k1gMnPc1	camembert
(	(	kIx(	(
<g/>
synonymum	synonymum	k1gNnSc1	synonymum
Penicillium	Penicillium	k1gNnSc1	Penicillium
candidum	candidum	k1gInSc1	candidum
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
v	v	k7c6	v
malých	malý	k2eAgInPc6d1	malý
nízkých	nízký	k2eAgInPc6d1	nízký
válečcích	váleček	k1gInPc6	váleček
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
125	[number]	k4	125
gramů	gram	k1gInPc2	gram
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
baleny	balit	k5eAaImNgInP	balit
do	do	k7c2	do
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
poté	poté	k6eAd1	poté
do	do	k7c2	do
krabiček	krabička	k1gFnPc2	krabička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
chuť	chuť	k1gFnSc1	chuť
sýra	sýr	k1gInSc2	sýr
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
stárnutím	stárnutí	k1gNnSc7	stárnutí
<g/>
,	,	kIx,	,
od	od	k7c2	od
jemné	jemný	k2eAgFnSc2d1	jemná
až	až	k9	až
po	po	k7c4	po
palčivou	palčivý	k2eAgFnSc4d1	palčivá
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
výtečným	výtečný	k2eAgInPc3d1	výtečný
dezertním	dezertní	k2eAgInPc3d1	dezertní
sýrům	sýr	k1gInPc3	sýr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Varianty	variant	k1gInPc4	variant
==	==	k?	==
</s>
</p>
<p>
<s>
Sýr	sýr	k1gInSc1	sýr
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
a	a	k8xC	a
na	na	k7c4	na
trh	trh	k1gInSc4	trh
dodává	dodávat	k5eAaImIp3nS	dodávat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
variantách	varianta	k1gFnPc6	varianta
a	a	k8xC	a
baleních	balení	k1gNnPc6	balení
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
odlišných	odlišný	k2eAgMnPc2d1	odlišný
i	i	k8xC	i
dle	dle	k7c2	dle
místa	místo	k1gNnSc2	místo
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Ligueil	Ligueil	k1gInSc1	Ligueil
je	být	k5eAaImIp3nS	být
sýr	sýr	k1gInSc4	sýr
typu	typ	k1gInSc2	typ
camembert	camembert	k1gInSc1	camembert
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Poitiers	Poitiers	k1gInSc1	Poitiers
z	z	k7c2	z
kravského	kravský	k2eAgNnSc2d1	kravské
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Hermelín	hermelín	k1gMnSc1	hermelín
</s>
</p>
<p>
<s>
Le	Le	k?	Le
Rustique	Rustique	k1gInSc1	Rustique
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Camembert	camembert	k1gInSc1	camembert
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Harel	Harela	k1gFnPc2	Harela
and	and	k?	and
the	the	k?	the
camembert	camembert	k1gInSc1	camembert
of	of	k?	of
Normandii	Normandie	k1gFnSc4	Normandie
</s>
</p>
