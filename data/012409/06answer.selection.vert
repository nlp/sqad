<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
<g/>
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
DPMB	DPMB	kA	DPMB
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
dopravcem	dopravce	k1gMnSc7	dopravce
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
hromadné	hromadný	k2eAgFnSc6d1	hromadná
dopravě	doprava	k1gFnSc6	doprava
na	na	k7c6	na
území	území	k1gNnSc6	území
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
