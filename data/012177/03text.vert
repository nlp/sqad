<p>
<s>
Polní	polní	k2eAgFnSc1d1	polní
cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
pravidlech	pravidlo	k1gNnPc6	pravidlo
silničního	silniční	k2eAgInSc2d1	silniční
provozu	provoz	k1gInSc2	provoz
zmíněna	zmínit	k5eAaPmNgFnS	zmínit
jako	jako	k8xC	jako
druh	druh	k1gInSc1	druh
účelové	účelový	k2eAgFnSc2d1	účelová
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
ČSN	ČSN	kA	ČSN
73	[number]	k4	73
6109	[number]	k4	6109
ji	on	k3xPp3gFnSc4	on
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k9	jako
účelovou	účelový	k2eAgFnSc4d1	účelová
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
zejména	zejména	k9	zejména
zemědělské	zemědělský	k2eAgFnSc3d1	zemědělská
dopravě	doprava	k1gFnSc3	doprava
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
plnit	plnit	k5eAaImF	plnit
i	i	k9	i
jinou	jiný	k2eAgFnSc4d1	jiná
dopravní	dopravní	k2eAgFnSc4d1	dopravní
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
cyklistická	cyklistický	k2eAgFnSc1d1	cyklistická
stezka	stezka	k1gFnSc1	stezka
nebo	nebo	k8xC	nebo
stezka	stezka	k1gFnSc1	stezka
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
<g/>
,	,	kIx,	,
a	a	k8xC	a
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
ji	on	k3xPp3gFnSc4	on
jako	jako	k9	jako
směrově	směrově	k6eAd1	směrově
nerozdělenou	rozdělený	k2eNgFnSc4d1	nerozdělená
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
tak	tak	k9	tak
nejen	nejen	k6eAd1	nejen
cesty	cesta	k1gFnPc4	cesta
mezi	mezi	k7c7	mezi
poli	pole	k1gNnPc7	pole
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jinde	jinde	k6eAd1	jinde
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
krajině	krajina	k1gFnSc6	krajina
(	(	kIx(	(
<g/>
nejde	jít	k5eNaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
lesní	lesní	k2eAgFnSc4d1	lesní
cestu	cesta	k1gFnSc4	cesta
<g/>
)	)	kIx)	)
–	–	k?	–
přímou	přímý	k2eAgFnSc4d1	přímá
definici	definice	k1gFnSc4	definice
polní	polní	k2eAgFnSc2d1	polní
cesty	cesta	k1gFnSc2	cesta
však	však	k9	však
české	český	k2eAgInPc1d1	český
zákony	zákon	k1gInPc1	zákon
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jako	jako	k9	jako
polní	polní	k2eAgFnSc1d1	polní
cesta	cesta	k1gFnSc1	cesta
označují	označovat	k5eAaImIp3nP	označovat
zpevněné	zpevněný	k2eAgFnPc4d1	zpevněná
i	i	k8xC	i
nezpevněné	zpevněný	k2eNgFnPc4d1	nezpevněná
komunikace	komunikace	k1gFnPc4	komunikace
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
evidovány	evidovat	k5eAaImNgFnP	evidovat
jako	jako	k9	jako
silnice	silnice	k1gFnPc1	silnice
ani	ani	k8xC	ani
jako	jako	k9	jako
místní	místní	k2eAgFnPc4d1	místní
komunikace	komunikace	k1gFnPc4	komunikace
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
provoz	provoz	k1gInSc4	provoz
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
<g/>
,	,	kIx,	,
označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
stezka	stezka	k1gFnSc1	stezka
nebo	nebo	k8xC	nebo
pěšina	pěšina	k1gFnSc1	pěšina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
polních	polní	k2eAgFnPc2d1	polní
cest	cesta	k1gFnPc2	cesta
==	==	k?	==
</s>
</p>
<p>
<s>
Polní	polní	k2eAgFnPc1d1	polní
cesty	cesta	k1gFnPc1	cesta
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
zpřístupnění	zpřístupnění	k1gNnSc3	zpřístupnění
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
pozemků	pozemek	k1gInPc2	pozemek
vzdálených	vzdálený	k2eAgInPc2d1	vzdálený
od	od	k7c2	od
silnic	silnice	k1gFnPc2	silnice
či	či	k8xC	či
místních	místní	k2eAgFnPc2d1	místní
komunikací	komunikace	k1gFnPc2	komunikace
vlastníkům	vlastník	k1gMnPc3	vlastník
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
napojení	napojení	k1gNnSc2	napojení
na	na	k7c4	na
síť	síť	k1gFnSc4	síť
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
místních	místní	k2eAgFnPc2d1	místní
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
,	,	kIx,	,
lesních	lesní	k2eAgFnPc2d1	lesní
cest	cesta	k1gFnPc2	cesta
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
účelových	účelový	k2eAgFnPc2d1	účelová
komunikací	komunikace	k1gFnPc2	komunikace
a	a	k8xC	a
ke	k	k7c3	k
zpřístupnění	zpřístupnění	k1gNnSc3	zpřístupnění
krajiny	krajina	k1gFnSc2	krajina
a	a	k8xC	a
propojení	propojení	k1gNnSc2	propojení
důležitých	důležitý	k2eAgInPc2d1	důležitý
bodů	bod	k1gInPc2	bod
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
krajině	krajina	k1gFnSc6	krajina
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
vedení	vedení	k1gNnSc4	vedení
turistických	turistický	k2eAgFnPc2d1	turistická
tras	trasa	k1gFnPc2	trasa
(	(	kIx(	(
<g/>
pěších	pěší	k2eAgFnPc2d1	pěší
<g/>
,	,	kIx,	,
cyklistických	cyklistický	k2eAgFnPc2d1	cyklistická
<g/>
,	,	kIx,	,
jezdeckých	jezdecký	k2eAgFnPc2d1	jezdecká
<g/>
,	,	kIx,	,
lyžařských	lyžařský	k2eAgFnPc2d1	lyžařská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
polní	polní	k2eAgFnSc2d1	polní
cesty	cesta	k1gFnSc2	cesta
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
vyloučení	vyloučení	k1gNnSc4	vyloučení
účelové	účelový	k2eAgFnSc2d1	účelová
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
dopravy	doprava	k1gFnSc2	doprava
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polní	polní	k2eAgFnPc1d1	polní
cesty	cesta	k1gFnPc1	cesta
a	a	k8xC	a
vegetace	vegetace	k1gFnSc1	vegetace
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gMnPc2	on
dotváří	dotvářet	k5eAaImIp3nS	dotvářet
ráz	ráz	k1gInSc4	ráz
krajiny	krajina	k1gFnSc2	krajina
a	a	k8xC	a
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
její	její	k3xOp3gFnSc4	její
biodiverzitu	biodiverzita	k1gFnSc4	biodiverzita
<g/>
,	,	kIx,	,
trvalým	trvalý	k2eAgInSc7d1	trvalý
a	a	k8xC	a
výrazným	výrazný	k2eAgInSc7d1	výrazný
způsobem	způsob	k1gInSc7	způsob
ohraničují	ohraničovat	k5eAaImIp3nP	ohraničovat
pozemky	pozemek	k1gInPc4	pozemek
a	a	k8xC	a
katastrální	katastrální	k2eAgNnPc4d1	katastrální
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Polní	polní	k2eAgFnPc1d1	polní
cesty	cesta	k1gFnPc1	cesta
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
význam	význam	k1gInSc4	význam
v	v	k7c6	v
protierozní	protierozní	k2eAgFnSc6d1	protierozní
ochraně	ochrana	k1gFnSc6	ochrana
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
vodohospodářství	vodohospodářství	k1gNnSc2	vodohospodářství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Veřejně	veřejně	k6eAd1	veřejně
přístupné	přístupný	k2eAgFnPc1d1	přístupná
účelové	účelový	k2eAgFnPc1d1	účelová
komunikace	komunikace	k1gFnPc1	komunikace
<g/>
,	,	kIx,	,
stezky	stezka	k1gFnPc1	stezka
i	i	k8xC	i
pěšiny	pěšina	k1gFnPc1	pěšina
mimo	mimo	k7c4	mimo
zastavěné	zastavěný	k2eAgNnSc4d1	zastavěné
území	území	k1gNnSc4	území
podléhají	podléhat	k5eAaImIp3nP	podléhat
podle	podle	k7c2	podle
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
povinné	povinný	k2eAgFnSc2d1	povinná
evidenci	evidence	k1gFnSc3	evidence
u	u	k7c2	u
obecních	obecní	k2eAgInPc2d1	obecní
úřadů	úřad	k1gInPc2	úřad
a	a	k8xC	a
nesmějí	smát	k5eNaImIp3nP	smát
být	být	k5eAaImF	být
rušeny	rušen	k2eAgFnPc1d1	rušena
ani	ani	k8xC	ani
zřizovány	zřizován	k2eAgFnPc1d1	zřizována
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
pověřeného	pověřený	k2eAgInSc2d1	pověřený
obecního	obecní	k2eAgInSc2d1	obecní
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kategorizace	kategorizace	k1gFnSc1	kategorizace
==	==	k?	==
</s>
</p>
<p>
<s>
ČSN	ČSN	kA	ČSN
73	[number]	k4	73
6109	[number]	k4	6109
dělí	dělit	k5eAaImIp3nP	dělit
polní	polní	k2eAgFnSc2d1	polní
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
třech	tři	k4xCgInPc2	tři
hlavních	hlavní	k2eAgFnPc2d1	hlavní
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Návrhové	návrhový	k2eAgInPc1d1	návrhový
parametry	parametr	k1gInPc1	parametr
se	se	k3xPyFc4	se
uvádějí	uvádět	k5eAaImIp3nP	uvádět
písmenem	písmeno	k1gNnSc7	písmeno
P	P	kA	P
a	a	k8xC	a
údajem	údaj	k1gInSc7	údaj
o	o	k7c6	o
návrhové	návrhový	k2eAgFnSc6d1	návrhová
šířce	šířka	k1gFnSc6	šířka
a	a	k8xC	a
za	za	k7c7	za
lomítkem	lomítko	k1gNnSc7	lomítko
návrhové	návrhový	k2eAgFnSc2d1	návrhová
rychlosti	rychlost	k1gFnSc2	rychlost
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
P	P	kA	P
<g/>
7,0	[number]	k4	7,0
<g/>
/	/	kIx~	/
<g/>
50	[number]	k4	50
nebo	nebo	k8xC	nebo
P	P	kA	P
<g/>
3,0	[number]	k4	3,0
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
hlavní	hlavní	k2eAgFnPc1d1	hlavní
polní	polní	k2eAgFnPc1d1	polní
cesty	cesta	k1gFnPc1	cesta
<g/>
:	:	kIx,	:
návrhová	návrhový	k2eAgFnSc1d1	návrhová
šířka	šířka	k1gFnSc1	šířka
7	[number]	k4	7
až	až	k9	až
4	[number]	k4	4
metry	metr	k1gInPc1	metr
<g/>
,	,	kIx,	,
návrhová	návrhový	k2eAgFnSc1d1	návrhová
rychlost	rychlost	k1gFnSc1	rychlost
50	[number]	k4	50
až	až	k9	až
30	[number]	k4	30
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
jedno-	jedno-	k?	jedno-
i	i	k8xC	i
dvoupruhové	dvoupruhový	k2eAgNnSc1d1	dvoupruhové
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
svoz	svoz	k1gInSc4	svoz
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
50	[number]	k4	50
až	až	k9	až
500	[number]	k4	500
ha	ha	kA	ha
</s>
</p>
<p>
<s>
vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
polní	polní	k2eAgFnPc1d1	polní
cesty	cesta	k1gFnPc1	cesta
<g/>
:	:	kIx,	:
návrhová	návrhový	k2eAgFnSc1d1	návrhová
šířka	šířka	k1gFnSc1	šířka
4,5	[number]	k4	4,5
až	až	k9	až
3,5	[number]	k4	3,5
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
návrhová	návrhový	k2eAgFnSc1d1	návrhová
rychlost	rychlost	k1gFnSc1	rychlost
30	[number]	k4	30
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
jednopruhové	jednopruhový	k2eAgFnSc2d1	jednopruhová
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
svoz	svoz	k1gInSc4	svoz
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
50	[number]	k4	50
až	až	k9	až
200	[number]	k4	200
ha	ha	kA	ha
</s>
</p>
<p>
<s>
doplňkové	doplňkový	k2eAgFnPc1d1	doplňková
polní	polní	k2eAgFnPc1d1	polní
cesty	cesta	k1gFnPc1	cesta
<g/>
:	:	kIx,	:
návrhová	návrhový	k2eAgFnSc1d1	návrhová
šířka	šířka	k1gFnSc1	šířka
3,5	[number]	k4	3,5
až	až	k9	až
3	[number]	k4	3
metry	metr	k1gInPc1	metr
<g/>
,	,	kIx,	,
návrhová	návrhový	k2eAgFnSc1d1	návrhová
rychlost	rychlost	k1gFnSc1	rychlost
30	[number]	k4	30
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
jednopruhové	jednopruhový	k2eAgFnSc6d1	jednopruhová
<g/>
,	,	kIx,	,
nezpevněné	zpevněný	k2eNgFnSc6d1	nezpevněná
</s>
</p>
<p>
<s>
==	==	k?	==
Užívání	užívání	k1gNnSc1	užívání
polních	polní	k2eAgFnPc2d1	polní
cest	cesta	k1gFnPc2	cesta
==	==	k?	==
</s>
</p>
<p>
<s>
Polní	polní	k2eAgFnPc1d1	polní
cesty	cesta	k1gFnPc1	cesta
jakožto	jakožto	k8xS	jakožto
účelové	účelový	k2eAgFnPc1d1	účelová
komunikace	komunikace	k1gFnPc1	komunikace
obecně	obecně	k6eAd1	obecně
spadají	spadat	k5eAaPmIp3nP	spadat
do	do	k7c2	do
práva	právo	k1gNnSc2	právo
bezplatného	bezplatný	k2eAgNnSc2d1	bezplatné
obecného	obecný	k2eAgNnSc2d1	obecné
užívání	užívání	k1gNnSc2	užívání
obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
k	k	k7c3	k
obvyklým	obvyklý	k2eAgInPc3d1	obvyklý
účelům	účel	k1gInPc3	účel
podle	podle	k7c2	podle
§	§	k?	§
19	[number]	k4	19
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
Zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zákon	zákon	k1gInSc1	zákon
nestanoví	stanovit	k5eNaPmIp3nS	stanovit
pro	pro	k7c4	pro
speciální	speciální	k2eAgInSc4d1	speciální
případ	případ	k1gInSc4	případ
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
vlastníka	vlastník	k1gMnSc2	vlastník
a	a	k8xC	a
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
jeho	jeho	k3xOp3gInPc2	jeho
oprávněných	oprávněný	k2eAgInPc2d1	oprávněný
zájmů	zájem	k1gInPc2	zájem
může	moct	k5eAaImIp3nS	moct
silniční	silniční	k2eAgInSc1d1	silniční
správní	správní	k2eAgInSc1d1	správní
úřad	úřad	k1gInSc1	úřad
veřejný	veřejný	k2eAgInSc4d1	veřejný
přístup	přístup	k1gInSc4	přístup
upravit	upravit	k5eAaPmF	upravit
nebo	nebo	k8xC	nebo
omezit	omezit	k5eAaPmF	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
vstupu	vstup	k1gInSc2	vstup
je	být	k5eAaImIp3nS	být
zaručeno	zaručit	k5eAaPmNgNnS	zaručit
i	i	k9	i
§	§	k?	§
63	[number]	k4	63
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
(	(	kIx(	(
<g/>
institut	institut	k1gInSc1	institut
veřejné	veřejný	k2eAgFnSc2d1	veřejná
přístupnosti	přístupnost	k1gFnSc2	přístupnost
krajiny	krajina	k1gFnSc2	krajina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zákony	zákon	k1gInPc4	zákon
vymezují	vymezovat	k5eAaImIp3nP	vymezovat
možnosti	možnost	k1gFnPc1	možnost
omezení	omezení	k1gNnSc4	omezení
tohoto	tento	k3xDgNnSc2	tento
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
neplatí	platit	k5eNaImIp3nS	platit
pro	pro	k7c4	pro
polní	polní	k2eAgFnPc4d1	polní
cesty	cesta	k1gFnPc4	cesta
žádné	žádný	k3yNgNnSc4	žádný
speciální	speciální	k2eAgNnSc4d1	speciální
omezení	omezení	k1gNnSc4	omezení
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
motorová	motorový	k2eAgNnPc4d1	motorové
vozidla	vozidlo	k1gNnPc4	vozidlo
tedy	tedy	k9	tedy
platí	platit	k5eAaImIp3nS	platit
obecné	obecný	k2eAgNnSc4d1	obecné
omezení	omezení	k1gNnSc4	omezení
rychlosti	rychlost	k1gFnSc2	rychlost
na	na	k7c4	na
90	[number]	k4	90
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
80	[number]	k4	80
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
nemotorová	motorový	k2eNgNnPc4d1	nemotorové
vozidla	vozidlo	k1gNnPc4	vozidlo
není	být	k5eNaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
konkrétní	konkrétní	k2eAgFnSc7d1	konkrétní
hodnotou	hodnota	k1gFnSc7	hodnota
omezena	omezit	k5eAaPmNgFnS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Zastavěná	zastavěný	k2eAgFnSc1d1	zastavěná
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
na	na	k7c6	na
polních	polní	k2eAgFnPc6d1	polní
cestách	cesta	k1gFnPc6	cesta
nevyznačuje	vyznačovat	k5eNaImIp3nS	vyznačovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
neplatí	platit	k5eNaImIp3nS	platit
omezení	omezení	k1gNnSc4	omezení
rychlosti	rychlost	k1gFnSc2	rychlost
na	na	k7c4	na
50	[number]	k4	50
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Typicky	typicky	k6eAd1	typicky
však	však	k9	však
polní	polní	k2eAgFnSc2d1	polní
cesty	cesta	k1gFnSc2	cesta
zastavěným	zastavěný	k2eAgNnSc7d1	zastavěné
územím	území	k1gNnSc7	území
nevedou	vést	k5eNaImIp3nP	vést
<g/>
.	.	kIx.	.
</s>
<s>
Fakticky	fakticky	k6eAd1	fakticky
se	se	k3xPyFc4	se
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
jízdy	jízda	k1gFnSc2	jízda
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
především	především	k9	především
obecná	obecný	k2eAgFnSc1d1	obecná
povinnost	povinnost	k1gFnSc1	povinnost
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
rychlost	rychlost	k1gFnSc4	rychlost
jízdy	jízda	k1gFnSc2	jízda
schopnostem	schopnost	k1gFnPc3	schopnost
řidiče	řidič	k1gInSc2	řidič
<g/>
,	,	kIx,	,
vlastnostem	vlastnost	k1gFnPc3	vlastnost
vozidla	vozidlo	k1gNnSc2	vozidlo
a	a	k8xC	a
nákladu	náklad	k1gInSc2	náklad
<g/>
,	,	kIx,	,
stavu	stav	k1gInSc2	stav
pozemní	pozemní	k2eAgFnSc2d1	pozemní
komunikace	komunikace	k1gFnSc2	komunikace
a	a	k8xC	a
dalším	další	k2eAgFnPc3d1	další
okolnostem	okolnost	k1gFnPc3	okolnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyústění	vyústění	k1gNnSc1	vyústění
polní	polní	k2eAgFnSc2d1	polní
cesty	cesta	k1gFnSc2	cesta
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
účelové	účelový	k2eAgFnSc2d1	účelová
komunikace	komunikace	k1gFnSc2	komunikace
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
pozemní	pozemní	k2eAgFnSc4d1	pozemní
komunikaci	komunikace	k1gFnSc4	komunikace
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
křižovatky	křižovatka	k1gFnSc2	křižovatka
v	v	k7c6	v
§	§	k?	§
2	[number]	k4	2
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
361	[number]	k4	361
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
nepovažuje	považovat	k5eNaImIp3nS	považovat
za	za	k7c4	za
křižovatku	křižovatka	k1gFnSc4	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vjíždění	vjíždění	k1gNnSc4	vjíždění
z	z	k7c2	z
účelové	účelový	k2eAgFnSc2d1	účelová
komunikace	komunikace	k1gFnSc2	komunikace
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
pozemní	pozemní	k2eAgFnSc4d1	pozemní
komunikaci	komunikace	k1gFnSc4	komunikace
platí	platit	k5eAaImIp3nS	platit
podle	podle	k7c2	podle
§	§	k?	§
23	[number]	k4	23
stejná	stejná	k1gFnSc1	stejná
pravidla	pravidlo	k1gNnSc2	pravidlo
jako	jako	k8xC	jako
při	při	k7c6	při
vjíždění	vjíždění	k1gNnSc6	vjíždění
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
ležícího	ležící	k2eAgNnSc2d1	ležící
mimo	mimo	k7c4	mimo
pozemní	pozemní	k2eAgFnSc4d1	pozemní
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
křížení	křížení	k1gNnSc2	křížení
či	či	k8xC	či
jiného	jiný	k2eAgInSc2d1	jiný
styku	styk	k1gInSc2	styk
dvou	dva	k4xCgNnPc2	dva
či	či	k8xC	či
více	hodně	k6eAd2	hodně
účelových	účelový	k2eAgFnPc2d1	účelová
komunikací	komunikace	k1gFnPc2	komunikace
nejsou	být	k5eNaImIp3nP	být
blíže	blízce	k6eAd2	blízce
stanovena	stanovit	k5eAaPmNgNnP	stanovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Standardizace	standardizace	k1gFnSc2	standardizace
==	==	k?	==
</s>
</p>
<p>
<s>
Standardizace	standardizace	k1gFnSc1	standardizace
polních	polní	k2eAgFnPc2d1	polní
cest	cesta	k1gFnPc2	cesta
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
započala	započnout	k5eAaPmAgFnS	započnout
vydáním	vydání	k1gNnSc7	vydání
technických	technický	k2eAgFnPc2d1	technická
podmínek	podmínka	k1gFnPc2	podmínka
Katalog	katalog	k1gInSc1	katalog
vozovek	vozovka	k1gFnPc2	vozovka
polních	polní	k2eAgFnPc2d1	polní
cest	cesta	k1gFnPc2	cesta
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2004	[number]	k4	2004
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
technických	technický	k2eAgFnPc2d1	technická
podmínek	podmínka	k1gFnPc2	podmínka
TP	TP	kA	TP
170	[number]	k4	170
(	(	kIx(	(
<g/>
schvalovaných	schvalovaný	k2eAgFnPc2d1	schvalovaná
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
navrhování	navrhování	k1gNnSc4	navrhování
vozovek	vozovka	k1gFnPc2	vozovka
pozemních	pozemní	k2eAgFnPc2d1	pozemní
komunikací	komunikace	k1gFnPc2	komunikace
nově	nově	k6eAd1	nově
zahrnuty	zahrnut	k2eAgFnPc4d1	zahrnuta
i	i	k8xC	i
nezpevněné	zpevněný	k2eNgFnPc4d1	nezpevněná
vozovky	vozovka	k1gFnPc4	vozovka
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2004	[number]	k4	2004
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
ČSN	ČSN	kA	ČSN
73	[number]	k4	73
6101	[number]	k4	6101
Projektování	projektování	k1gNnPc2	projektování
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
dálnic	dálnice	k1gFnPc2	dálnice
nově	nově	k6eAd1	nově
zahrnuty	zahrnout	k5eAaPmNgInP	zahrnout
i	i	k9	i
jednopruhové	jednopruhový	k2eAgFnPc1d1	jednopruhová
silnice	silnice	k1gFnPc1	silnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
takto	takto	k6eAd1	takto
rozšířenou	rozšířený	k2eAgFnSc4d1	rozšířená
standardizaci	standardizace	k1gFnSc4	standardizace
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
byla	být	k5eAaImAgFnS	být
zpracována	zpracován	k2eAgFnSc1d1	zpracována
i	i	k8xC	i
nová	nový	k2eAgFnSc1d1	nová
ČSN	ČSN	kA	ČSN
73	[number]	k4	73
6109	[number]	k4	6109
Projektování	projektování	k1gNnPc2	projektování
polních	polní	k2eAgFnPc2d1	polní
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
vyšla	vyjít	k5eAaPmAgFnS	vyjít
změna	změna	k1gFnSc1	změna
č.	č.	k?	č.
1	[number]	k4	1
k	k	k7c3	k
TP	TP	kA	TP
Katalog	katalog	k1gInSc1	katalog
vozovek	vozovka	k1gFnPc2	vozovka
polních	polní	k2eAgFnPc2d1	polní
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
například	například	k6eAd1	například
nově	nově	k6eAd1	nově
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
vedle	vedle	k7c2	vedle
vozovek	vozovka	k1gFnPc2	vozovka
tuhých	tuhý	k2eAgFnPc2d1	tuhá
(	(	kIx(	(
<g/>
s	s	k7c7	s
krytem	kryt	k1gInSc7	kryt
cemento-betonovým	cementoetonový	k2eAgInSc7d1	cemento-betonový
či	či	k8xC	či
asfaltovým	asfaltový	k2eAgMnSc7d1	asfaltový
<g/>
)	)	kIx)	)
a	a	k8xC	a
netuhých	tuhý	k2eNgFnPc2d1	netuhá
vozovek	vozovka	k1gFnPc2	vozovka
i	i	k8xC	i
vozovky	vozovka	k1gFnSc2	vozovka
dlážděné	dlážděný	k2eAgInPc1d1	dlážděný
nebo	nebo	k8xC	nebo
s	s	k7c7	s
krytem	kryt	k1gInSc7	kryt
z	z	k7c2	z
betonových	betonový	k2eAgInPc2d1	betonový
dílců	dílec	k1gInPc2	dílec
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k6eAd1	též
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vozovky	vozovka	k1gFnPc4	vozovka
s	s	k7c7	s
krytem	kryt	k1gInSc7	kryt
stabilizovaným	stabilizovaný	k2eAgInSc7d1	stabilizovaný
(	(	kIx(	(
<g/>
ČSN	ČSN	kA	ČSN
73	[number]	k4	73
6125	[number]	k4	6125
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krytem	kryt	k1gInSc7	kryt
z	z	k7c2	z
nestmelených	stmelený	k2eNgInPc2d1	stmelený
materiálů	materiál	k1gInPc2	materiál
(	(	kIx(	(
<g/>
ČSN	ČSN	kA	ČSN
73	[number]	k4	73
6126	[number]	k4	6126
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
R-materiálů	Rateriál	k1gInPc2	R-materiál
(	(	kIx(	(
<g/>
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
13108-8	[number]	k4	13108-8
část	část	k1gFnSc1	část
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Průhon	průhon	k1gInSc1	průhon
</s>
</p>
<p>
<s>
Úvozová	úvozový	k2eAgFnSc1d1	úvozová
cesta	cesta	k1gFnSc1	cesta
</s>
</p>
<p>
<s>
Lesní	lesní	k2eAgFnSc1d1	lesní
cesta	cesta	k1gFnSc1	cesta
</s>
</p>
<p>
<s>
Pozemkové	pozemkový	k2eAgFnPc1d1	pozemková
úpravy	úprava	k1gFnPc1	úprava
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
ČSN	ČSN	kA	ČSN
73	[number]	k4	73
6109	[number]	k4	6109
Projektování	projektování	k1gNnPc2	projektování
polních	polní	k2eAgFnPc2d1	polní
cest	cesta	k1gFnPc2	cesta
(	(	kIx(	(
<g/>
duben	duben	k1gInSc1	duben
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
technické	technický	k2eAgFnPc4d1	technická
podmínky	podmínka	k1gFnPc4	podmínka
Katalog	katalog	k1gInSc1	katalog
vozovek	vozovka	k1gFnPc2	vozovka
polních	polní	k2eAgFnPc2d1	polní
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
MZ	MZ	kA	MZ
ČR	ČR	kA	ČR
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
+	+	kIx~	+
Změna	změna	k1gFnSc1	změna
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
účinná	účinný	k2eAgFnSc1d1	účinná
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
Roadconsult	Roadconsult	k1gInSc1	Roadconsult
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
schválena	schválit	k5eAaPmNgFnS	schválit
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
ČR	ČR	kA	ČR
–	–	k?	–
Ústředním	ústřední	k2eAgInSc7d1	ústřední
pozemkovým	pozemkový	k2eAgInSc7d1	pozemkový
úřadem	úřad	k1gInSc7	úřad
č.	č.	k?	č.
j.	j.	k?	j.
26206	[number]	k4	26206
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
17170	[number]	k4	17170
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
ze	z	k7c2	z
semináře	seminář	k1gInSc2	seminář
o	o	k7c6	o
problematice	problematika	k1gFnSc6	problematika
pravidel	pravidlo	k1gNnPc2	pravidlo
provozu	provoz	k1gInSc2	provoz
na	na	k7c6	na
lesní	lesní	k2eAgFnSc6d1	lesní
a	a	k8xC	a
polní	polní	k2eAgFnSc6d1	polní
dopravní	dopravní	k2eAgFnSc6d1	dopravní
síti	síť	k1gFnSc6	síť
<g/>
,	,	kIx,	,
Regiontour	Regiontour	k1gMnSc1	Regiontour
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
návrh	návrh	k1gInSc1	návrh
"	"	kIx"	"
<g/>
Pravidel	pravidlo	k1gNnPc2	pravidlo
pohybu	pohyb	k1gInSc2	pohyb
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
"	"	kIx"	"
předložený	předložený	k2eAgInSc4d1	předložený
Klubem	klub	k1gInSc7	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
</s>
</p>
<p>
<s>
P.	P.	kA	P.
Málek	Málek	k1gMnSc1	Málek
<g/>
,	,	kIx,	,
I.	I.	kA	I.
Celjak	Celjak	k1gInSc1	Celjak
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
katalog	katalog	k1gInSc1	katalog
vozovek	vozovka	k1gFnPc2	vozovka
polních	polní	k2eAgFnPc2d1	polní
cest	cesta	k1gFnPc2	cesta
–	–	k?	–
technické	technický	k2eAgFnSc2d1	technická
podmínky	podmínka	k1gFnSc2	podmínka
jako	jako	k8xS	jako
změna	změna	k1gFnSc1	změna
č.	č.	k?	č.
1	[number]	k4	1
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
<g/>
,	,	kIx,	,
Jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
nedatováno	datován	k2eNgNnSc1d1	nedatováno
</s>
</p>
