<s>
Rada	rada	k1gFnSc1
vzájemné	vzájemný	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
</s>
<s>
Rada	rada	k1gFnSc1
vzájemné	vzájemný	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
pomociС	pomociС	k?
Э	Э	k?
В	В	k?
Vlajka	vlajka	k1gFnSc1
RVHP	RVHP	kA
</s>
<s>
Členství	členství	k1gNnSc1
v	v	k7c6
RVHP	RVHP	kA
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
<g/>
,	,	kIx,
členové	člen	k1gMnPc1
tmavě	tmavě	k6eAd1
červeně	červeně	k6eAd1
<g/>
,	,	kIx,
členové	člen	k1gMnPc1
nespolupracující	spolupracující	k2eNgMnPc1d1
fialově	fialově	k6eAd1
<g/>
,	,	kIx,
přidružení	přidružení	k1gNnSc1
světle	světle	k6eAd1
červeně	červeně	k6eAd1
<g/>
,	,	kIx,
pozorovatelé	pozorovatel	k1gMnPc1
žlutě	žlutě	k6eAd1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
RVHP	RVHP	kA
Vznik	vznik	k1gInSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1949	#num#	k4
Zánik	zánik	k1gInSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1991	#num#	k4
Účel	účel	k1gInSc1
</s>
<s>
ekonomická	ekonomický	k2eAgFnSc1d1
pomoc	pomoc	k1gFnSc1
mezi	mezi	k7c7
členskými	členský	k2eAgInPc7d1
státy	stát	k1gInPc7
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
SSSR	SSSR	kA
Členové	člen	k1gMnPc1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Bulharsko	Bulharsko	k1gNnSc1
Československo	Československo	k1gNnSc1
Maďarsko	Maďarsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
Rumunsko	Rumunsko	k1gNnSc1
Albánie	Albánie	k1gFnSc2
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
MongolskoKuba	MongolskoKuba	k1gFnSc1
KubaVietnam	KubaVietnam	k1gInSc4
Vietnam	Vietnam	k1gInSc1
<g/>
(	(	kIx(
<g/>
podrobněji	podrobně	k6eAd2
níže	nízce	k6eAd2
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
RVHP	RVHP	kA
neboli	neboli	k8xC
Rada	rada	k1gFnSc1
vzájemné	vzájemný	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
:	:	kIx,
С	С	k?
<g/>
,	,	kIx,
С	С	k?
Э	Э	k?
В	В	k?
<g/>
,	,	kIx,
Sovět	Sovět	k1gMnSc1
ekonomičeskoj	ekonomičeskoj	k1gInSc1
vzaimopomošči	vzaimopomošč	k1gFnSc3
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Comecon	Comecon	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnPc1
Council	Councila	k1gFnPc2
for	forum	k1gNnPc2
Mutual	Mutual	k1gMnSc1
Economic	Economic	k1gMnSc1
Assistance	Assistance	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
obchodní	obchodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
sdružující	sdružující	k2eAgFnSc1d1
v	v	k7c6
době	doba	k1gFnSc6
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
socialistické	socialistický	k2eAgInPc4d1
státy	stát	k1gInPc4
sovětského	sovětský	k2eAgInSc2d1
bloku	blok	k1gInSc2
<g/>
,	,	kIx,
založena	založit	k5eAaPmNgFnS
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
na	na	k7c4
popud	popud	k1gInSc4
sovětského	sovětský	k2eAgMnSc2d1
vůdce	vůdce	k1gMnSc2
Josifa	Josif	k1gMnSc2
Visarionoviče	Visarionovič	k1gMnSc2
Stalina	Stalin	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
podstatě	podstata	k1gFnSc6
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
mocenský	mocenský	k2eAgInSc4d1
nástroj	nástroj	k1gInSc4
pro	pro	k7c4
centrální	centrální	k2eAgNnSc4d1
ovládání	ovládání	k1gNnSc4
ekonomiky	ekonomika	k1gFnSc2
socialistických	socialistický	k2eAgInPc2d1
států	stát	k1gInPc2
Sovětským	sovětský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
<g/>
,	,	kIx,
o	o	k7c4
sovětský	sovětský	k2eAgInSc4d1
protipól	protipól	k1gInSc4
Marshallova	Marshallův	k2eAgInSc2d1
plánu	plán	k1gInSc2
a	a	k8xC
později	pozdě	k6eAd2
EHS	EHS	kA
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vznik	vznik	k1gInSc1
RVHP	RVHP	kA
</s>
<s>
Zakládajícími	zakládající	k2eAgMnPc7d1
členy	člen	k1gMnPc7
RVHP	RVHP	kA
byly	být	k5eAaImAgFnP
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1949	#num#	k4
Bulharsko	Bulharsko	k1gNnSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
,	,	kIx,
Rumunsko	Rumunsko	k1gNnSc1
a	a	k8xC
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
přistoupily	přistoupit	k5eAaPmAgFnP
Albánie	Albánie	k1gFnPc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
NDR	NDR	kA
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mongolsko	Mongolsko	k1gNnSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kuba	Kuba	k1gFnSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Vietnam	Vietnam	k1gInSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Albánie	Albánie	k1gFnSc1
pod	pod	k7c7
čínským	čínský	k2eAgInSc7d1
vlivem	vliv	k1gInSc7
roku	rok	k1gInSc2
1961	#num#	k4
od	od	k7c2
účasti	účast	k1gFnSc2
na	na	k7c4
činnosti	činnost	k1gFnPc4
RVHP	RVHP	kA
upustila	upustit	k5eAaPmAgFnS
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
vystoupila	vystoupit	k5eAaPmAgFnS
úplně	úplně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přidruženým	přidružený	k2eAgInSc7d1
státem	stát	k1gInSc7
RVHP	RVHP	kA
byla	být	k5eAaImAgFnS
od	od	k7c2
roku	rok	k1gInSc2
1964	#num#	k4
Jugoslávie	Jugoslávie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
RVHP	RVHP	kA
částečně	částečně	k6eAd1
spolupracovaly	spolupracovat	k5eAaImAgInP
nebo	nebo	k8xC
měly	mít	k5eAaImAgInP
status	status	k1gInSc4
pozorovatele	pozorovatel	k1gMnSc2
i	i	k8xC
některé	některý	k3yIgFnSc2
nesocialistické	socialistický	k2eNgFnSc2d1
či	či	k8xC
rozvojové	rozvojový	k2eAgFnSc2d1
země	zem	k1gFnSc2
(	(	kIx(
<g/>
Finsko	Finsko	k1gNnSc1
<g/>
,	,	kIx,
Irák	Irák	k1gInSc1
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
<g/>
,	,	kIx,
Nikaragua	Nikaragua	k1gFnSc1
<g/>
,	,	kIx,
Etiopie	Etiopie	k1gFnSc1
<g/>
,	,	kIx,
Laos	Laos	k1gInSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgInSc1d1
Jemen	Jemen	k1gInSc1
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sídlo	sídlo	k1gNnSc1
RVHP	RVHP	kA
bylo	být	k5eAaImAgNnS
vždy	vždy	k6eAd1
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
RVHP	RVHP	kA
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
:	:	kIx,
з	з	k?
С	С	k?
<g/>
,	,	kIx,
zdanije	zdanít	k5eAaPmIp3nS
sev	sev	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
30	#num#	k4
<g/>
poschoďová	poschoďový	k2eAgFnSc1d1
výšková	výškový	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
v	v	k7c6
ulici	ulice	k1gFnSc6
Nový	nový	k2eAgInSc1d1
Arbat	Arbat	k1gInSc1
36	#num#	k4
<g/>
,	,	kIx,
vybudovaná	vybudovaný	k2eAgFnSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1963	#num#	k4
až	až	k9
1970	#num#	k4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc4
otevřené	otevřený	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
v	v	k7c6
budově	budova	k1gFnSc6
sídlí	sídlet	k5eAaImIp3nS
magistrát	magistrát	k1gInSc1
města	město	k1gNnSc2
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zánik	zánik	k1gInSc1
RVHP	RVHP	kA
</s>
<s>
Koncem	koncem	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
spolupráce	spolupráce	k1gFnSc1
zemí	zem	k1gFnPc2
RVHP	RVHP	kA
orientovala	orientovat	k5eAaBmAgFnS
na	na	k7c4
užší	úzký	k2eAgFnSc4d2
ekonomickou	ekonomický	k2eAgFnSc4d1
integraci	integrace	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c4
vytváření	vytváření	k1gNnSc4
vazeb	vazba	k1gFnPc2
v	v	k7c6
kooperacích	kooperace	k1gFnPc6
a	a	k8xC
na	na	k7c6
specializaci	specializace	k1gFnSc6
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
výrobních	výrobní	k2eAgInPc6d1
oborech	obor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozvíjela	rozvíjet	k5eAaImAgFnS
se	se	k3xPyFc4
široká	široký	k2eAgFnSc1d1
mezinárodní	mezinárodní	k2eAgFnSc1d1
spolupráce	spolupráce	k1gFnSc1
a	a	k8xC
výměna	výměna	k1gFnSc1
výsledků	výsledek	k1gInPc2
vědeckotechnického	vědeckotechnický	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1988-1989	1988-1989	k4
se	se	k3xPyFc4
však	však	k9
dostavily	dostavit	k5eAaPmAgInP
strukturální	strukturální	k2eAgInPc1d1
problémy	problém	k1gInPc1
v	v	k7c6
plánování	plánování	k1gNnSc6
<g/>
,	,	kIx,
nefungovala	fungovat	k5eNaImAgFnS
dobře	dobře	k6eAd1
obchodní	obchodní	k2eAgFnSc1d1
výměna	výměna	k1gFnSc1
a	a	k8xC
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
zjevným	zjevný	k2eAgFnPc3d1
poruchám	porucha	k1gFnPc3
při	při	k7c6
převozu	převoz	k1gInSc6
zboží	zboží	k1gNnSc2
přes	přes	k7c4
hranice	hranice	k1gFnPc4
<g/>
,	,	kIx,
především	především	k9
ve	v	k7c6
východoevropských	východoevropský	k2eAgFnPc6d1
členských	členský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
(	(	kIx(
<g/>
řešeno	řešit	k5eAaImNgNnS
tzv.	tzv.	kA
celními	celní	k2eAgNnPc7d1
opatřeními	opatření	k1gNnPc7
na	na	k7c4
limitování	limitování	k1gNnSc4
dovozu	dovoz	k1gInSc2
a	a	k8xC
vývozu	vývoz	k1gInSc2
zboží	zboží	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
po	po	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
většina	většina	k1gFnSc1
východoevropských	východoevropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
začala	začít	k5eAaPmAgFnS
orientovat	orientovat	k5eAaBmF
svou	svůj	k3xOyFgFnSc4
ekonomiku	ekonomika	k1gFnSc4
na	na	k7c4
tržní	tržní	k2eAgFnSc4d1
a	a	k8xC
pro	pro	k7c4
obchod	obchod	k1gInSc4
se	s	k7c7
Západem	západ	k1gInSc7
<g/>
,	,	kIx,
nastaly	nastat	k5eAaPmAgFnP
potíže	potíž	k1gFnPc1
v	v	k7c6
plánování	plánování	k1gNnSc6
a	a	k8xC
zúčtování	zúčtování	k1gNnSc6
hospodářských	hospodářský	k2eAgInPc2d1
závazků	závazek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1
zasedání	zasedání	k1gNnSc1
RVHP	RVHP	kA
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1991	#num#	k4
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
dohodě	dohoda	k1gFnSc3
rozpustit	rozpustit	k5eAaPmF
RVHP	RVHP	kA
do	do	k7c2
90	#num#	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
sklonku	sklonek	k1gInSc6
komunistické	komunistický	k2eAgFnSc2d1
éry	éra	k1gFnSc2
měla	mít	k5eAaImAgFnS
RVHP	RVHP	kA
deset	deset	k4xCc4
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1991	#num#	k4
na	na	k7c4
návrh	návrh	k1gInSc4
československé	československý	k2eAgFnSc2d1
strany	strana	k1gFnSc2
a	a	k8xC
jejich	jejich	k3xOp3gMnPc2
představitelů	představitel	k1gMnPc2
(	(	kIx(
<g/>
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
<g/>
,	,	kIx,
ministr	ministr	k1gMnSc1
financí	finance	k1gFnPc2
ČSFR	ČSFR	kA
a	a	k8xC
Vladimír	Vladimír	k1gMnSc1
Dlouhý	Dlouhý	k1gMnSc1
<g/>
,	,	kIx,
ministr	ministr	k1gMnSc1
hospodářství	hospodářství	k1gNnSc2
ČSFR	ČSFR	kA
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
přijato	přijmout	k5eAaPmNgNnS
ve	v	k7c6
Federálním	federální	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
ČSFR	ČSFR	kA
usnesení	usnesení	k1gNnSc4
o	o	k7c6
sjednání	sjednání	k1gNnSc6
protokolu	protokol	k1gInSc2
o	o	k7c4
zrušení	zrušení	k1gNnSc4
Rady	rada	k1gFnSc2
vzájemné	vzájemný	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
a	a	k8xC
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1991	#num#	k4
schválilo	schválit	k5eAaPmAgNnS
znění	znění	k1gNnSc1
protokolu	protokol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Postupným	postupný	k2eAgInSc7d1
odklonem	odklon	k1gInSc7
od	od	k7c2
zásad	zásada	k1gFnPc2
centrálně	centrálně	k6eAd1
plánované	plánovaný	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
a	a	k8xC
politickou	politický	k2eAgFnSc7d1
transformací	transformace	k1gFnSc7
zemí	zem	k1gFnPc2
se	s	k7c7
RVHP	RVHP	kA
rozpadla	rozpadnout	k5eAaPmAgFnS
a	a	k8xC
nebyla	být	k5eNaImAgFnS
nahrazena	nahrazen	k2eAgFnSc1d1
žádnou	žádný	k3yNgFnSc7
podobnou	podobný	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Výstavba	výstavba	k1gFnSc1
budovy	budova	k1gFnSc2
RVHP	RVHP	kA
(	(	kIx(
<g/>
fotografie	fotografie	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Noční	noční	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
budovu	budova	k1gFnSc4
bývalé	bývalý	k2eAgFnSc2d1
RVHP	RVHP	kA
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Zasedání	zasedání	k1gNnSc1
výkonné	výkonný	k2eAgFnSc2d1
rady	rada	k1gFnSc2
v	v	k7c6
Polsku	Polsko	k1gNnSc6
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Obdobné	obdobný	k2eAgNnSc1d1
zasedání	zasedání	k1gNnSc1
v	v	k7c6
Rumunsku	Rumunsko	k1gNnSc6
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1
poštovní	poštovní	k2eAgFnSc1d1
známka	známka	k1gFnSc1
ke	k	k7c3
30	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
RVHP	RVHP	kA
(	(	kIx(
<g/>
červen	červen	k1gInSc1
1979	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Mapa	mapa	k1gFnSc1
států	stát	k1gInPc2
RVHP	RVHP	kA
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Členský	členský	k2eAgInSc1d1
stát	stát	k1gInSc1
nespolupracující	spolupracující	k2eNgFnSc2d1
</s>
<s>
Přídružený	Přídružený	k2eAgMnSc1d1
člen	člen	k1gMnSc1
</s>
<s>
Pozorovatelské	pozorovatelský	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Leden	leden	k1gInSc1
1949	#num#	k4
</s>
<s>
Bulharská	bulharský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Československá	československý	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
ČSSR	ČSSR	kA
<g/>
)	)	kIx)
</s>
<s>
Maďarská	maďarský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Polská	polský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Rumunská	rumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Svaz	svaz	k1gInSc1
sovětských	sovětský	k2eAgFnPc2d1
socialistických	socialistický	k2eAgFnPc2d1
republik	republika	k1gFnPc2
</s>
<s>
Únor	únor	k1gInSc1
1949	#num#	k4
</s>
<s>
Albánská	albánský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
1950	#num#	k4
</s>
<s>
Německá	německý	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
1962	#num#	k4
</s>
<s>
Mongolská	mongolský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
1972	#num#	k4
</s>
<s>
Kuba	Kuba	k1gFnSc1
Kuba	Kuba	k1gFnSc1
</s>
<s>
1978	#num#	k4
</s>
<s>
Vietnam	Vietnam	k1gInSc1
Vietnam	Vietnam	k1gInSc1
</s>
<s>
Přidružený	přidružený	k2eAgMnSc1d1
člen	člen	k1gMnSc1
</s>
<s>
1964	#num#	k4
</s>
<s>
Socialistická	socialistický	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Jugoslávie	Jugoslávie	k1gFnSc2
</s>
<s>
Pozorovatelské	pozorovatelský	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
1950	#num#	k4
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
1956	#num#	k4
</s>
<s>
Korejská	korejský	k2eAgFnSc1d1
lidově	lidově	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
1973	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
Finsko	Finsko	k1gNnSc1
</s>
<s>
1975	#num#	k4
</s>
<s>
Irák	Irák	k1gInSc1
Irák	Irák	k1gInSc1
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
1976	#num#	k4
</s>
<s>
Angola	Angola	k1gFnSc1
Angola	Angola	k1gFnSc1
</s>
<s>
1984	#num#	k4
</s>
<s>
Nikaragua	Nikaragua	k1gFnSc1
Nikaragua	Nikaragua	k1gFnSc1
</s>
<s>
1985	#num#	k4
</s>
<s>
Mosambik	Mosambik	k1gInSc1
Mosambik	Mosambik	k1gInSc1
</s>
<s>
1986	#num#	k4
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
Afghánistán	Afghánistán	k1gInSc1
</s>
<s>
Etiopie	Etiopie	k1gFnSc1
Etiopie	Etiopie	k1gFnSc1
</s>
<s>
Laos	Laos	k1gInSc1
Laos	Laos	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgInSc1d1
Jemen	Jemen	k1gInSc1
Jižní	jižní	k2eAgInSc1d1
Jemen	Jemen	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Na	na	k7c6
RVHP	RVHP	kA
nejvíc	hodně	k6eAd3,k6eAd1
doplácely	doplácet	k5eAaImAgFnP
rozvinuté	rozvinutý	k2eAgFnPc1d1
země	zem	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
třeba	třeba	k6eAd1
Československo	Československo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Kalendárium	kalendárium	k1gNnSc4
<g/>
:	:	kIx,
Vznikla	vzniknout	k5eAaPmAgFnS
Rada	rada	k1gFnSc1
vzájemné	vzájemný	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
<g/>
.	.	kIx.
www.moderni-dejiny.cz	www.moderni-dejiny.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BARÁK	Barák	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
RVHP	RVHP	kA
-	-	kIx~
42	#num#	k4
let	léto	k1gNnPc2
činnosti	činnost	k1gFnSc2
ukončila	ukončit	k5eAaPmAgFnS
schůze	schůze	k1gFnSc1
netrvající	trvající	k2eNgFnSc2d1
ani	ani	k8xC
hodinu	hodina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bideleux	Bideleux	k1gInSc1
and	and	k?
Jeffries	Jeffries	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
582	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Kolektiv	kolektiv	k1gInSc1
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
československá	československý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
V.	V.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
230,231	230,231	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Východní	východní	k2eAgInSc1d1
blok	blok	k1gInSc1
</s>
<s>
Varšavská	varšavský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rada	rada	k1gFnSc1
vzájemné	vzájemný	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Rada	rada	k1gFnSc1
vzájemné	vzájemný	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Rada	rada	k1gFnSc1
vzájemné	vzájemný	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010711373	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1016341-4	1016341-4	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2122	#num#	k4
7536	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79106441	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
51145066485666591390	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79106441	#num#	k4
</s>
