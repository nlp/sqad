<s>
Koruna	koruna	k1gFnSc1	koruna
česká	český	k2eAgFnSc1d1	Česká
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
od	od	k7c2	od
měnové	měnový	k2eAgFnSc2d1	měnová
odluky	odluka	k1gFnSc2	odluka
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
měnová	měnový	k2eAgFnSc1d1	měnová
jednotka	jednotka	k1gFnSc1	jednotka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
se	s	k7c7	s
zkratkou	zkratka	k1gFnSc7	zkratka
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
mezinárodně	mezinárodně	k6eAd1	mezinárodně
v	v	k7c6	v
ISO	ISO	kA	ISO
4217	[number]	k4	4217
CZK	CZK	kA	CZK
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
haléřů	haléř	k1gInPc2	haléř
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
pro	pro	k7c4	pro
nízkou	nízký	k2eAgFnSc4d1	nízká
hodnotu	hodnota	k1gFnSc4	hodnota
už	už	k6eAd1	už
používají	používat	k5eAaImIp3nP	používat
jen	jen	k9	jen
v	v	k7c6	v
bezhotovostním	bezhotovostní	k2eAgInSc6d1	bezhotovostní
styku	styk	k1gInSc6	styk
<g/>
.	.	kIx.	.
</s>
