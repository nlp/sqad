<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
česká	český	k2eAgFnSc1d1	Česká
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
od	od	k7c2	od
měnové	měnový	k2eAgFnSc2d1	měnová
odluky	odluka	k1gFnSc2	odluka
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
měnová	měnový	k2eAgFnSc1d1	měnová
jednotka	jednotka	k1gFnSc1	jednotka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
se	s	k7c7	s
zkratkou	zkratka	k1gFnSc7	zkratka
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
mezinárodně	mezinárodně	k6eAd1	mezinárodně
v	v	k7c6	v
ISO	ISO	kA	ISO
4217	[number]	k4	4217
CZK	CZK	kA	CZK
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
haléřů	haléř	k1gInPc2	haléř
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
pro	pro	k7c4	pro
nízkou	nízký	k2eAgFnSc4d1	nízká
hodnotu	hodnota	k1gFnSc4	hodnota
už	už	k6eAd1	už
používají	používat	k5eAaImIp3nP	používat
jen	jen	k9	jen
v	v	k7c6	v
bezhotovostním	bezhotovostní	k2eAgInSc6d1	bezhotovostní
styku	styk	k1gInSc6	styk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
rozdělení	rozdělení	k1gNnSc2	rozdělení
Československa	Československo	k1gNnSc2	Československo
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
existovala	existovat	k5eAaImAgFnS	existovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
dohody	dohoda	k1gFnSc2	dohoda
měnová	měnový	k2eAgFnSc1d1	měnová
unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
měnou	měna	k1gFnSc7	měna
obou	dva	k4xCgInPc2	dva
nástupnických	nástupnický	k2eAgInPc2d1	nástupnický
států	stát	k1gInPc2	stát
nadále	nadále	k6eAd1	nadále
koruna	koruna	k1gFnSc1	koruna
československá	československý	k2eAgFnSc1d1	Československá
(	(	kIx(	(
<g/>
se	s	k7c7	s
zkratkou	zkratka	k1gFnSc7	zkratka
Kčs	Kčs	kA	Kčs
<g/>
,	,	kIx,	,
mezinárodně	mezinárodně	k6eAd1	mezinárodně
CSK	CSK	kA	CSK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
národní	národní	k2eAgFnSc6d1	národní
bance	banka	k1gFnSc6	banka
(	(	kIx(	(
<g/>
ČNB	ČNB	kA	ČNB
<g/>
)	)	kIx)	)
č.	č.	k?	č.
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
v	v	k7c6	v
§	§	k?	§
16	[number]	k4	16
platné	platný	k2eAgFnPc4d1	platná
bankovky	bankovka	k1gFnPc4	bankovka
a	a	k8xC	a
mince	mince	k1gFnPc4	mince
vydané	vydaný	k2eAgFnPc4d1	vydaná
ČNB	ČNB	kA	ČNB
za	za	k7c4	za
zákonné	zákonný	k2eAgInPc4d1	zákonný
peníze	peníz	k1gInPc4	peníz
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
při	při	k7c6	při
všech	všecek	k3xTgFnPc6	všecek
platbách	platba	k1gFnPc6	platba
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
úpravu	úprava	k1gFnSc4	úprava
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
oběhu	oběh	k1gInSc6	oběh
bankovek	bankovka	k1gFnPc2	bankovka
a	a	k8xC	a
mincí	mince	k1gFnPc2	mince
č.	č.	k?	č.
136	[number]	k4	136
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
a	a	k8xC	a
prováděcí	prováděcí	k2eAgInPc1d1	prováděcí
předpisy	předpis	k1gInPc1	předpis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Měnová	měnový	k2eAgFnSc1d1	měnová
odluka	odluka	k1gFnSc1	odluka
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
nové	nový	k2eAgFnSc2d1	nová
české	český	k2eAgFnSc2d1	Česká
měny	měna	k1gFnSc2	měna
stanovil	stanovit	k5eAaPmAgInS	stanovit
§	§	k?	§
13	[number]	k4	13
zákona	zákon	k1gInSc2	zákon
ČNR	ČNR	kA	ČNR
č.	č.	k?	č.
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
národní	národní	k2eAgFnSc6d1	národní
bance	banka	k1gFnSc6	banka
<g/>
.	.	kIx.	.
§	§	k?	§
56	[number]	k4	56
dále	daleko	k6eAd2	daleko
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosavadní	dosavadní	k2eAgFnPc4d1	dosavadní
platné	platný	k2eAgFnPc4d1	platná
bankovky	bankovka	k1gFnPc4	bankovka
a	a	k8xC	a
mince	mince	k1gFnPc4	mince
Státní	státní	k2eAgFnSc2d1	státní
banky	banka	k1gFnSc2	banka
Československé	československý	k2eAgFnSc2d1	Československá
(	(	kIx(	(
<g/>
SBČS	SBČS	kA	SBČS
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
zákonné	zákonný	k2eAgInPc4d1	zákonný
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
zákon	zákon	k1gInSc1	zákon
nabyl	nabýt	k5eAaPmAgInS	nabýt
účinnosti	účinnost	k1gFnSc2	účinnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
to	ten	k3xDgNnSc4	ten
chápat	chápat	k5eAaImF	chápat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k9	již
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
datu	datum	k1gNnSc3	datum
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
koruna	koruna	k1gFnSc1	koruna
česká	český	k2eAgFnSc1d1	Česká
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
zákonnou	zákonný	k2eAgFnSc7d1	zákonná
jednotkou	jednotka	k1gFnSc7	jednotka
–	–	k?	–
nebylo	být	k5eNaImAgNnS	být
dosud	dosud	k6eAd1	dosud
vydáno	vydat	k5eAaPmNgNnS	vydat
žádné	žádný	k3yNgNnSc1	žádný
prováděcí	prováděcí	k2eAgNnSc1d1	prováděcí
nařízení	nařízení	k1gNnSc1	nařízení
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ČNB	ČNB	kA	ČNB
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
skutečně	skutečně	k6eAd1	skutečně
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1993	[number]	k4	1993
tedy	tedy	k9	tedy
existovaly	existovat	k5eAaImAgInP	existovat
dva	dva	k4xCgInPc1	dva
suverénní	suverénní	k2eAgInPc1d1	suverénní
státy	stát	k1gInPc1	stát
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
nezávislými	závislý	k2eNgFnPc7d1	nezávislá
centrálními	centrální	k2eAgFnPc7d1	centrální
bankami	banka	k1gFnPc7	banka
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
společnou	společný	k2eAgFnSc7d1	společná
měnou	měna	k1gFnSc7	měna
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
byla	být	k5eAaImAgFnS	být
nadále	nadále	k6eAd1	nadále
koruna	koruna	k1gFnSc1	koruna
československá	československý	k2eAgFnSc1d1	Československá
(	(	kIx(	(
<g/>
s	s	k7c7	s
ročníkem	ročník	k1gInSc7	ročník
ražby	ražba	k1gFnSc2	ražba
1993	[number]	k4	1993
dokonce	dokonce	k9	dokonce
vyšly	vyjít	k5eAaPmAgInP	vyjít
ještě	ještě	k9	ještě
dvě	dva	k4xCgFnPc4	dva
československé	československý	k2eAgFnPc4d1	Československá
oběžné	oběžný	k2eAgFnPc4d1	oběžná
desetikoruny	desetikoruna	k1gFnPc4	desetikoruna
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
pamětní	pamětní	k2eAgFnPc4d1	pamětní
mince	mince	k1gFnPc4	mince
této	tento	k3xDgFnSc2	tento
měny	měna	k1gFnSc2	měna
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
byly	být	k5eAaImAgFnP	být
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
jistotu	jistota	k1gFnSc4	jistota
<g/>
"	"	kIx"	"
uvedeny	uveden	k2eAgInPc1d1	uveden
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
již	již	k6eAd1	již
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
60	[number]	k4	60
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1993	[number]	k4	1993
o	o	k7c6	o
oddělení	oddělení	k1gNnSc6	oddělení
měny	měna	k1gFnSc2	měna
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nařízením	nařízení	k1gNnSc7	nařízení
vlády	vláda	k1gFnSc2	vláda
č.	č.	k?	č.
61	[number]	k4	61
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
oddělení	oddělení	k1gNnSc4	oddělení
měny	měna	k1gFnSc2	měna
a	a	k8xC	a
vyhláškou	vyhláška	k1gFnSc7	vyhláška
ČNB	ČNB	kA	ČNB
č.	č.	k?	č.
62	[number]	k4	62
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
oddělení	oddělení	k1gNnSc4	oddělení
měny	měna	k1gFnSc2	měna
<g/>
,	,	kIx,	,
z	z	k7c2	z
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
byla	být	k5eAaImAgFnS	být
měnová	měnový	k2eAgFnSc1d1	měnová
unie	unie	k1gFnSc1	unie
ukončena	ukončen	k2eAgFnSc1d1	ukončena
ke	k	k7c3	k
dni	den	k1gInSc3	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1993	[number]	k4	1993
–	–	k?	–
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
tak	tak	k6eAd1	tak
koruna	koruna	k1gFnSc1	koruna
československá	československý	k2eAgFnSc1d1	Československá
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
samostatná	samostatný	k2eAgFnSc1d1	samostatná
česká	český	k2eAgFnSc1d1	Česká
koruna	koruna	k1gFnSc1	koruna
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
slovenská	slovenský	k2eAgFnSc1d1	slovenská
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
měny	měna	k1gFnPc1	měna
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
předešlé	předešlý	k2eAgFnSc2d1	předešlá
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
Kčs	Kčs	kA	Kčs
=	=	kIx~	=
1	[number]	k4	1
Kč	Kč	kA	Kč
=	=	kIx~	=
1	[number]	k4	1
Sk	Sk	kA	Sk
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
slovenská	slovenský	k2eAgFnSc1d1	slovenská
koruna	koruna	k1gFnSc1	koruna
rychle	rychle	k6eAd1	rychle
oslabila	oslabit	k5eAaPmAgFnS	oslabit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc4	první
bankovky	bankovka	k1gFnPc4	bankovka
samostatné	samostatný	k2eAgFnSc2d1	samostatná
české	český	k2eAgFnSc2d1	Česká
měny	měna	k1gFnSc2	měna
(	(	kIx(	(
<g/>
hodnota	hodnota	k1gFnSc1	hodnota
200	[number]	k4	200
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
připravené	připravený	k2eAgInPc4d1	připravený
před	před	k7c7	před
odlukou	odluka	k1gFnSc7	odluka
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
postupně	postupně	k6eAd1	postupně
uváděny	uvádět	k5eAaImNgInP	uvádět
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Bankovky	bankovka	k1gFnPc1	bankovka
(	(	kIx(	(
<g/>
vyšší	vysoký	k2eAgFnPc1d2	vyšší
hodnoty	hodnota	k1gFnPc1	hodnota
okolkované	okolkovaný	k2eAgFnPc1d1	okolkovaná
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgFnPc1d2	nižší
hodnoty	hodnota	k1gFnPc1	hodnota
neokolkované	okolkovaný	k2eNgFnPc4d1	okolkovaný
<g/>
)	)	kIx)	)
a	a	k8xC	a
mince	mince	k1gFnSc1	mince
zrušené	zrušený	k2eAgFnSc2d1	zrušená
československé	československý	k2eAgFnSc2d1	Československá
měny	měna	k1gFnSc2	měna
však	však	k9	však
nadále	nadále	k6eAd1	nadále
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
v	v	k7c6	v
určeném	určený	k2eAgInSc6d1	určený
poměru	poměr	k1gInSc6	poměr
jako	jako	k8xC	jako
platidla	platidlo	k1gNnSc2	platidlo
nové	nový	k2eAgFnSc2d1	nová
měny	měna	k1gFnSc2	měna
po	po	k7c4	po
několik	několik	k4yIc4	několik
následujících	následující	k2eAgInPc2d1	následující
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
nejdéle	dlouho	k6eAd3	dlouho
v	v	k7c6	v
ČR	ČR	kA	ČR
mince	mince	k1gFnSc2	mince
v	v	k7c6	v
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
2	[number]	k4	2
Kčs	Kčs	kA	Kčs
a	a	k8xC	a
5	[number]	k4	5
Kčs	Kčs	kA	Kčs
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
okolkované	okolkovaný	k2eAgFnPc4d1	okolkovaná
bankovky	bankovka	k1gFnPc4	bankovka
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
zůstaly	zůstat	k5eAaPmAgInP	zůstat
oficiálně	oficiálně	k6eAd1	oficiálně
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
všechny	všechen	k3xTgFnPc4	všechen
československé	československý	k2eAgFnPc4d1	Československá
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
mince	mince	k1gFnPc4	mince
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
letech	let	k1gInPc6	let
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
SBČS	SBČS	kA	SBČS
(	(	kIx(	(
<g/>
až	až	k9	až
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
vyměnit	vyměnit	k5eAaPmF	vyměnit
je	on	k3xPp3gFnPc4	on
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mince	mince	k1gFnPc4	mince
a	a	k8xC	a
bankovky	bankovka	k1gFnPc4	bankovka
české	český	k2eAgFnSc2d1	Česká
koruny	koruna	k1gFnSc2	koruna
==	==	k?	==
</s>
</p>
<p>
<s>
Součet	součet	k1gInSc4	součet
hodnot	hodnota	k1gFnPc2	hodnota
všech	všecek	k3xTgFnPc2	všecek
českých	český	k2eAgFnPc2d1	Česká
platných	platný	k2eAgFnPc2d1	platná
mincí	mince	k1gFnPc2	mince
a	a	k8xC	a
bankovek	bankovka	k1gFnPc2	bankovka
činí	činit	k5eAaImIp3nS	činit
8888	[number]	k4	8888
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
10	[number]	k4	10
<g/>
+	+	kIx~	+
<g/>
20	[number]	k4	20
<g/>
+	+	kIx~	+
<g/>
50	[number]	k4	50
<g/>
+	+	kIx~	+
<g/>
100	[number]	k4	100
<g/>
+	+	kIx~	+
<g/>
200	[number]	k4	200
<g/>
+	+	kIx~	+
<g/>
500	[number]	k4	500
<g/>
+	+	kIx~	+
<g/>
1000	[number]	k4	1000
<g/>
+	+	kIx~	+
<g/>
2000	[number]	k4	2000
<g/>
+	+	kIx~	+
<g/>
5000	[number]	k4	5000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
ukončena	ukončen	k2eAgFnSc1d1	ukončena
platnost	platnost	k1gFnSc1	platnost
mincí	mince	k1gFnPc2	mince
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
10	[number]	k4	10
a	a	k8xC	a
20	[number]	k4	20
haléřů	haléř	k1gInPc2	haléř
<g/>
,	,	kIx,	,
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
ukončena	ukončen	k2eAgFnSc1d1	ukončena
platnost	platnost	k1gFnSc1	platnost
mincí	mince	k1gFnPc2	mince
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
50	[number]	k4	50
h	h	k?	h
a	a	k8xC	a
bankovky	bankovka	k1gFnPc1	bankovka
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
20	[number]	k4	20
Kč	Kč	kA	Kč
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
platnost	platnost	k1gFnSc1	platnost
bankovky	bankovka	k1gFnSc2	bankovka
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
50	[number]	k4	50
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
vyšla	vyjít	k5eAaPmAgFnS	vyjít
pamětní	pamětní	k2eAgFnSc1d1	pamětní
stokorunová	stokorunový	k2eAgFnSc1d1	stokorunová
bankovka	bankovka	k1gFnSc1	bankovka
v	v	k7c6	v
zlatožluté	zlatožlutý	k2eAgFnSc6d1	zlatožlutá
barvě	barva	k1gFnSc6	barva
s	s	k7c7	s
portrétem	portrét	k1gInSc7	portrét
Aloise	Alois	k1gMnSc2	Alois
Rašína	Rašín	k1gMnSc2	Rašín
<g/>
,	,	kIx,	,
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
měnové	měnový	k2eAgFnSc2d1	měnová
odluky	odluka	k1gFnSc2	odluka
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
spekulacím	spekulace	k1gFnPc3	spekulace
nenahradí	nahradit	k5eNaPmIp3nS	nahradit
klasický	klasický	k2eAgInSc4d1	klasický
motiv	motiv	k1gInSc4	motiv
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mince	Minka	k1gFnSc6	Minka
===	===	k?	===
</s>
</p>
<p>
<s>
Mince	mince	k1gFnSc1	mince
50	[number]	k4	50
Kč	Kč	kA	Kč
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
World	World	k1gInSc4	World
Coins	Coins	k1gInSc4	Coins
Mincí	mince	k1gFnPc2	mince
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
oběžných	oběžný	k2eAgFnPc2d1	oběžná
mincí	mince	k1gFnPc2	mince
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
mince	mince	k1gFnPc1	mince
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
10	[number]	k4	10
a	a	k8xC	a
20	[number]	k4	20
korun	koruna	k1gFnPc2	koruna
se	s	k7c7	s
speciálním	speciální	k2eAgInSc7d1	speciální
(	(	kIx(	(
<g/>
jubilejním	jubilejní	k2eAgInSc7d1	jubilejní
<g/>
)	)	kIx)	)
vzhledem	vzhled	k1gInSc7	vzhled
–	–	k?	–
desetikoruna	desetikoruna	k1gFnSc1	desetikoruna
s	s	k7c7	s
ozubenými	ozubený	k2eAgInPc7d1	ozubený
kolečky	koleček	k1gInPc7	koleček
hodinového	hodinový	k2eAgInSc2d1	hodinový
strojku	strojek	k1gInSc2	strojek
a	a	k8xC	a
dvacetikoruna	dvacetikoruna	k1gFnSc1	dvacetikoruna
s	s	k7c7	s
astrolábem	astroláb	k1gInSc7	astroláb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
vzniku	vznik	k1gInSc2	vznik
Československa	Československo	k1gNnSc2	Československo
vydána	vydán	k2eAgFnSc1d1	vydána
emise	emise	k1gFnSc1	emise
oběžných	oběžný	k2eAgFnPc2d1	oběžná
mincí	mince	k1gFnPc2	mince
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
20	[number]	k4	20
korun	koruna	k1gFnPc2	koruna
s	s	k7c7	s
vyobrazeními	vyobrazení	k1gNnPc7	vyobrazení
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
Milana	Milan	k1gMnSc2	Milan
Rastislava	Rastislav	k1gMnSc2	Rastislav
Štefánika	Štefánik	k1gMnSc2	Štefánik
a	a	k8xC	a
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
emise	emise	k1gFnSc2	emise
bylo	být	k5eAaImAgNnS	být
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2019	[number]	k4	2019
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
emise	emise	k1gFnSc1	emise
taktéž	taktéž	k?	taktéž
dvacetikorunových	dvacetikorunový	k2eAgFnPc2d1	dvacetikorunová
mincí	mince	k1gFnPc2	mince
s	s	k7c7	s
vyobrazeními	vyobrazení	k1gNnPc7	vyobrazení
Aloise	Alois	k1gMnSc2	Alois
Rašína	Rašín	k1gMnSc2	Rašín
<g/>
,	,	kIx,	,
Viléma	Vilém	k1gMnSc2	Vilém
Pospíšila	Pospíšil	k1gMnSc2	Pospíšil
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Engliše	Engliš	k1gMnSc2	Engliš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
mince	mince	k1gFnSc1	mince
====	====	k?	====
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
oběhových	oběhový	k2eAgFnPc2d1	oběhová
mincí	mince	k1gFnPc2	mince
vydává	vydávat	k5eAaPmIp3nS	vydávat
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
též	též	k9	též
pamětní	pamětní	k2eAgFnPc4d1	pamětní
mince	mince	k1gFnPc4	mince
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
ve	v	k7c6	v
Sbírce	sbírka	k1gFnSc6	sbírka
zákonů	zákon	k1gInPc2	zákon
<g/>
:	:	kIx,	:
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
v	v	k7c6	v
hodnotách	hodnota	k1gFnPc6	hodnota
200	[number]	k4	200
Kč	Kč	kA	Kč
a	a	k8xC	a
500	[number]	k4	500
Kč	Kč	kA	Kč
a	a	k8xC	a
zlaté	zlatý	k2eAgFnPc4d1	zlatá
v	v	k7c6	v
hodnotách	hodnota	k1gFnPc6	hodnota
1000	[number]	k4	1000
Kč	Kč	kA	Kč
2500	[number]	k4	2500
Kč	Kč	kA	Kč
5000	[number]	k4	5000
Kč	Kč	kA	Kč
a	a	k8xC	a
10000	[number]	k4	10000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Zlaté	zlatý	k2eAgFnPc1d1	zlatá
mince	mince	k1gFnPc1	mince
jsou	být	k5eAaImIp3nP	být
vydávány	vydávat	k5eAaImNgFnP	vydávat
v	v	k7c6	v
tematických	tematický	k2eAgFnPc6d1	tematická
řadách	řada	k1gFnPc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
stříbrná	stříbrná	k1gFnSc1	stříbrná
mince	mince	k1gFnSc2	mince
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
vložkou	vložka	k1gFnSc7	vložka
a	a	k8xC	a
hologramem	hologram	k1gInSc7	hologram
v	v	k7c6	v
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
2000	[number]	k4	2000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Výrobcem	výrobce	k1gMnSc7	výrobce
jak	jak	k8xS	jak
oběžných	oběžný	k2eAgInPc6d1	oběžný
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pamětních	pamětní	k2eAgFnPc2d1	pamětní
mincí	mince	k1gFnPc2	mince
je	být	k5eAaImIp3nS	být
Česká	český	k2eAgFnSc1d1	Česká
mincovna	mincovna	k1gFnSc1	mincovna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
smluvním	smluvní	k2eAgMnSc7d1	smluvní
partnerem	partner	k1gMnSc7	partner
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
mincovně	mincovna	k1gFnSc6	mincovna
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2018	[number]	k4	2018
realizována	realizován	k2eAgFnSc1d1	realizována
vysokohmotnostní	vysokohmotnostní	k2eAgFnSc1d1	vysokohmotnostní
mince	mince	k1gFnSc1	mince
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
sto	sto	k4xCgNnSc4	sto
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
přes	přes	k7c4	přes
půl	půl	k1xP	půl
metru	metr	k1gInSc2	metr
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
česká	český	k2eAgFnSc1d1	Česká
mince	mince	k1gFnSc1	mince
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgInSc4d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Českou	český	k2eAgFnSc4d1	Česká
vysokohmotnostní	vysokohmotnostní	k2eAgFnSc4d1	vysokohmotnostní
minci	mince	k1gFnSc4	mince
představila	představit	k5eAaPmAgFnS	představit
ČNB	ČNB	kA	ČNB
veřejnosti	veřejnost	k1gFnSc2	veřejnost
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2019	[number]	k4	2019
na	na	k7c6	na
slavnostní	slavnostní	k2eAgFnSc6d1	slavnostní
vernisáži	vernisáž	k1gFnSc6	vernisáž
výstavy	výstava	k1gFnSc2	výstava
věnované	věnovaný	k2eAgNnSc1d1	věnované
výročí	výročí	k1gNnSc1	výročí
100	[number]	k4	100
let	léto	k1gNnPc2	léto
československé	československý	k2eAgFnSc2d1	Československá
měny	měna	k1gFnSc2	měna
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bankovky	bankovka	k1gFnPc1	bankovka
===	===	k?	===
</s>
</p>
<p>
<s>
Autorem	autor	k1gMnSc7	autor
podoby	podoba	k1gFnSc2	podoba
všech	všecek	k3xTgFnPc2	všecek
bankovek	bankovka	k1gFnPc2	bankovka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
Oldřich	Oldřich	k1gMnSc1	Oldřich
Kulhánek	Kulhánek	k1gMnSc1	Kulhánek
<g/>
.	.	kIx.	.
</s>
<s>
Výrobní	výrobní	k2eAgFnSc1d1	výrobní
cena	cena	k1gFnSc1	cena
bankovek	bankovka	k1gFnPc2	bankovka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
1,30	[number]	k4	1,30
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
za	za	k7c4	za
50	[number]	k4	50
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
do	do	k7c2	do
2,30	[number]	k4	2,30
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
za	za	k7c4	za
5000	[number]	k4	5000
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
ČNB	ČNB	kA	ČNB
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Bankovky	bankovka	k1gFnPc1	bankovka
váží	vážit	k5eAaImIp3nP	vážit
cca	cca	kA	cca
1	[number]	k4	1
g.	g.	k?	g.
</s>
</p>
<p>
<s>
Tisícikorunová	tisícikorunový	k2eAgFnSc1d1	tisícikorunová
bankovka	bankovka	k1gFnSc1	bankovka
s	s	k7c7	s
novými	nový	k2eAgInPc7d1	nový
ochrannými	ochranný	k2eAgInPc7d1	ochranný
prvky	prvek	k1gInPc7	prvek
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vybrána	vybrat	k5eAaPmNgFnS	vybrat
za	za	k7c4	za
světovou	světový	k2eAgFnSc4d1	světová
bankovku	bankovka	k1gFnSc4	bankovka
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Výroční	výroční	k2eAgFnPc4d1	výroční
a	a	k8xC	a
pamětní	pamětní	k2eAgFnPc4d1	pamětní
bankovky	bankovka	k1gFnPc4	bankovka
====	====	k?	====
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
vydala	vydat	k5eAaPmAgFnS	vydat
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2019	[number]	k4	2019
klasickou	klasický	k2eAgFnSc4d1	klasická
oběžní	oběžní	k2eAgFnSc4d1	oběžní
stokorunovou	stokorunový	k2eAgFnSc4d1	stokorunová
bankovku	bankovka	k1gFnSc4	bankovka
s	s	k7c7	s
přetiskem	přetisk	k1gInSc7	přetisk
ke	k	k7c3	k
stému	stý	k4xOgNnSc3	stý
výročí	výročí	k1gNnSc3	výročí
měnové	měnový	k2eAgFnSc2d1	měnová
odluky	odluka	k1gFnSc2	odluka
od	od	k7c2	od
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Přetisk	přetisk	k1gInSc1	přetisk
na	na	k7c6	na
bankovce	bankovka	k1gFnSc6	bankovka
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
výročním	výroční	k2eAgInSc7d1	výroční
logem	log	k1gInSc7	log
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
text	text	k1gInSc1	text
a	a	k8xC	a
letopočty	letopočet	k1gInPc1	letopočet
"	"	kIx"	"
<g/>
ČNB	ČNB	kA	ČNB
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
1919	[number]	k4	1919
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
100	[number]	k4	100
let	léto	k1gNnPc2	léto
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
Kč	Kč	kA	Kč
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
2019	[number]	k4	2019
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
vydala	vydat	k5eAaPmAgFnS	vydat
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2019	[number]	k4	2019
první	první	k4xOgFnSc4	první
pamětní	pamětní	k2eAgFnSc4d1	pamětní
bankovku	bankovka	k1gFnSc4	bankovka
v	v	k7c6	v
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
100	[number]	k4	100
Kč	Kč	kA	Kč
ke	k	k7c3	k
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
budování	budování	k1gNnSc2	budování
československé	československý	k2eAgFnSc2d1	Československá
měny	měna	k1gFnSc2	měna
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pamětní	pamětní	k2eAgFnSc1d1	pamětní
zlatožlutá	zlatožlutý	k2eAgFnSc1d1	zlatožlutá
stokoruna	stokoruna	k1gFnSc1	stokoruna
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
194	[number]	k4	194
x	x	k?	x
84	[number]	k4	84
mm	mm	kA	mm
s	s	k7c7	s
portrétem	portrét	k1gInSc7	portrét
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
Aloise	Alois	k1gMnSc2	Alois
Rašína	Rašín	k1gMnSc2	Rašín
byla	být	k5eAaImAgFnS	být
zhotovena	zhotovit	k5eAaPmNgFnS	zhotovit
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
akademické	akademický	k2eAgFnSc2d1	akademická
malířky	malířka	k1gFnSc2	malířka
Evy	Eva	k1gFnSc2	Eva
Hoškové	Hošková	k1gFnSc2	Hošková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výroční	výroční	k2eAgInSc4d1	výroční
a	a	k8xC	a
pamětní	pamětní	k2eAgFnPc4d1	pamětní
stokoruny	stokoruna	k1gFnPc4	stokoruna
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Směnný	směnný	k2eAgInSc1d1	směnný
kurz	kurz	k1gInSc1	kurz
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
koruna	koruna	k1gFnSc1	koruna
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
nejvíce	hodně	k6eAd3	hodně
posilující	posilující	k2eAgFnSc1d1	posilující
a	a	k8xC	a
poté	poté	k6eAd1	poté
druhou	druhý	k4xOgFnSc7	druhý
nejvíce	hodně	k6eAd3	hodně
oslabující	oslabující	k2eAgFnSc7d1	oslabující
měnou	měna	k1gFnSc7	měna
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
o	o	k7c6	o
intervencích	intervence	k1gFnPc6	intervence
na	na	k7c6	na
devizovém	devizový	k2eAgInSc6d1	devizový
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
držet	držet	k5eAaImF	držet
kurz	kurz	k1gInSc4	kurz
koruny	koruna	k1gFnSc2	koruna
vůči	vůči	k7c3	vůči
euru	euro	k1gNnSc3	euro
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
27	[number]	k4	27
CZK	CZK	kA	CZK
<g/>
/	/	kIx~	/
<g/>
EUR	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
intervence	intervence	k1gFnPc1	intervence
skončily	skončit	k5eAaPmAgFnP	skončit
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
československá	československý	k2eAgFnSc1d1	Československá
(	(	kIx(	(
<g/>
bankovky	bankovka	k1gFnSc2	bankovka
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mince	mince	k1gFnSc1	mince
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
</s>
</p>
<p>
<s>
Rakousko-uherská	rakouskoherský	k2eAgFnSc1d1	rakousko-uherská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
měn	měna	k1gFnPc2	měna
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
měn	měna	k1gFnPc2	měna
Evropy	Evropa	k1gFnSc2	Evropa
</s>
</p>
<p>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
eura	euro	k1gNnSc2	euro
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
koruna	koruna	k1gFnSc1	koruna
česká	český	k2eAgFnSc1d1	Česká
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Padesátníky	padesátník	k1gInPc1	padesátník
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
končí	končit	k5eAaImIp3nS	končit
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Koruna	koruna	k1gFnSc1	koruna
česká	český	k2eAgFnSc1d1	Česká
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Rekordní	rekordní	k2eAgNnSc1d1	rekordní
posílení	posílení	k1gNnSc1	posílení
české	český	k2eAgFnSc2d1	Česká
i	i	k8xC	i
slovenské	slovenský	k2eAgFnSc2d1	slovenská
koruny	koruna	k1gFnSc2	koruna
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
ČNB	ČNB	kA	ČNB
–	–	k?	–
české	český	k2eAgFnSc2d1	Česká
bankovky	bankovka	k1gFnSc2	bankovka
</s>
</p>
<p>
<s>
ČNB	ČNB	kA	ČNB
–	–	k?	–
české	český	k2eAgFnSc2d1	Česká
mince	mince	k1gFnSc2	mince
</s>
</p>
<p>
<s>
ČNB	ČNB	kA	ČNB
–	–	k?	–
mobilní	mobilní	k2eAgFnSc2d1	mobilní
aplikace	aplikace	k1gFnSc2	aplikace
české	český	k2eAgInPc4d1	český
peníze	peníz	k1gInPc4	peníz
</s>
</p>
<p>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
Surga	Surg	k1gMnSc2	Surg
<g/>
:	:	kIx,	:
České	český	k2eAgFnPc1d1	Česká
bankovky	bankovka	k1gFnPc1	bankovka
a	a	k8xC	a
mince	mince	k1gFnSc1	mince
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
ČNB	ČNB	kA	ČNB
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
o	o	k7c6	o
českých	český	k2eAgFnPc6d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc6d1	slovenská
bankovkách	bankovka	k1gFnPc6	bankovka
a	a	k8xC	a
papírových	papírový	k2eAgNnPc6d1	papírové
platidlech	platidlo	k1gNnPc6	platidlo
(	(	kIx(	(
<g/>
katalog	katalog	k1gInSc1	katalog
<g/>
,	,	kIx,	,
galerie	galerie	k1gFnSc1	galerie
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
informace	informace	k1gFnPc1	informace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kurzovní	kurzovní	k2eAgInPc1d1	kurzovní
lístky	lístek	k1gInPc1	lístek
a	a	k8xC	a
grafy	graf	k1gInPc1	graf
vývoje	vývoj	k1gInSc2	vývoj
kurzu	kurz	k1gInSc2	kurz
české	český	k2eAgFnSc2d1	Česká
koruny	koruna	k1gFnSc2	koruna
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
českého	český	k2eAgNnSc2d1	české
mincovnictví	mincovnictví	k1gNnSc2	mincovnictví
</s>
</p>
<p>
<s>
České	český	k2eAgFnSc2d1	Česká
mince	mince	k1gFnSc2	mince
(	(	kIx(	(
<g/>
katalog	katalog	k1gInSc1	katalog
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
národní	národní	k2eAgFnSc6d1	národní
bance	banka	k1gFnSc6	banka
<g/>
,	,	kIx,	,
Část	část	k1gFnSc1	část
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
<g/>
:	:	kIx,	:
Emise	emise	k1gFnSc1	emise
bankovek	bankovka	k1gFnPc2	bankovka
a	a	k8xC	a
mincí	mince	k1gFnPc2	mince
(	(	kIx(	(
<g/>
§	§	k?	§
12	[number]	k4	12
až	až	k9	až
22	[number]	k4	22
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
20	[number]	k4	20
let	let	k1gInSc4	let
české	český	k2eAgFnSc2d1	Česká
koruny	koruna	k1gFnSc2	koruna
<g/>
:	:	kIx,	:
Kdo	kdo	k3yQnSc1	kdo
zachránil	zachránit	k5eAaPmAgInS	zachránit
českou	český	k2eAgFnSc4d1	Česká
měnu	měna	k1gFnSc4	měna
</s>
</p>
<p>
<s>
===	===	k?	===
Oznámení	oznámení	k1gNnSc1	oznámení
ČNB	ČNB	kA	ČNB
o	o	k7c6	o
ukončení	ukončení	k1gNnSc6	ukončení
platnosti	platnost	k1gFnSc2	platnost
haléřových	haléřový	k2eAgFnPc2d1	haléřová
mincí	mince	k1gFnPc2	mince
===	===	k?	===
</s>
</p>
<p>
<s>
Racionalizace	racionalizace	k1gFnPc1	racionalizace
soustavy	soustava	k1gFnSc2	soustava
drobných	drobný	k2eAgFnPc2d1	drobná
mincí	mince	k1gFnPc2	mince
v	v	k7c6	v
ČR	ČR	kA	ČR
na	na	k7c6	na
cnb	cnb	k?	cnb
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Ukončení	ukončení	k1gNnSc1	ukončení
platnosti	platnost	k1gFnSc2	platnost
drobných	drobný	k2eAgFnPc2d1	drobná
mincí	mince	k1gFnPc2	mince
po	po	k7c6	po
10	[number]	k4	10
a	a	k8xC	a
20	[number]	k4	20
h	h	k?	h
na	na	k7c4	na
cnb	cnb	k?	cnb
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
ČNB	ČNB	kA	ČNB
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
ukončení	ukončení	k1gNnSc6	ukončení
platnosti	platnost	k1gFnSc2	platnost
padesátihaléřových	padesátihaléřový	k2eAgFnPc2d1	padesátihaléřový
mincí	mince	k1gFnPc2	mince
na	na	k7c6	na
cnb	cnb	k?	cnb
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
