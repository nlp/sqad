<s>
Buire	Buir	k1gMnSc5
</s>
<s>
Buire	Buir	k1gMnSc5
radnice	radnice	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
10	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
4	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
23	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
159	#num#	k4
<g/>
–	–	k?
<g/>
203	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
region	region	k1gInSc1
</s>
<s>
Hauts-de-France	Hauts-de-France	k1gFnSc1
departement	departement	k1gInSc1
</s>
<s>
Aisne	Aisnout	k5eAaPmIp3nS,k5eAaImIp3nS
arrondissement	arrondissement	k1gInSc1
</s>
<s>
Vervins	Vervins	k6eAd1
kanton	kanton	k1gInSc1
</s>
<s>
Hirson	Hirson	k1gMnSc1
</s>
<s>
Buire	Buir	k1gMnSc5
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
4,13	4,13	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
882	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
214	#num#	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
communedebuire	communedebuir	k1gMnSc5
<g/>
.	.	kIx.
<g/>
fr	fr	k0
PSČ	PSČ	kA
</s>
<s>
02500	#num#	k4
INSEE	INSEE	kA
</s>
<s>
02134	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Buire	Buir	k1gMnSc5
je	být	k5eAaImIp3nS
francouzská	francouzský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
v	v	k7c6
departementu	departement	k1gInSc6
Aisne	Aisn	k1gInSc5
v	v	k7c6
regionu	region	k1gInSc6
Hauts-de-France	Hauts-de-France	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
882	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sousední	sousední	k2eAgFnPc1d1
obce	obec	k1gFnPc1
</s>
<s>
Éparcy	Éparcy	k1gInPc1
<g/>
,	,	kIx,
La	la	k1gNnPc1
Hérie	Hérie	k1gFnSc1
<g/>
,	,	kIx,
Hirson	Hirson	k1gNnSc1
<g/>
,	,	kIx,
Neuve-Maison	Neuve-Maison	k1gNnSc1
<g/>
,	,	kIx,
Origny-en-Thiérache	Origny-en-Thiérache	k1gNnSc1
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
departementu	departement	k1gInSc6
Aisne	Aisn	k1gInSc5
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INSEE	INSEE	kA
<g/>
↑	↑	k?
INSEE	INSEE	kA
(	(	kIx(
<g/>
ZIP	zip	k1gInSc1
2,2	2,2	k4
MB	MB	kA
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Buire	Buir	k1gInSc5
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Obce	obec	k1gFnPc1
kantonu	kanton	k1gInSc2
Hirson	Hirson	k1gInSc1
</s>
<s>
Any-Martin-Rieux	Any-Martin-Rieux	k1gInSc1
•	•	k?
Aubenton	Aubenton	k1gInSc4
•	•	k?
Beaumé	Beaumý	k2eAgFnSc2d1
•	•	k?
Besmont	Besmont	k1gMnSc1
•	•	k?
Bucilly	Bucilla	k1gFnSc2
•	•	k?
Buire	Buir	k1gInSc5
•	•	k?
Coingt	Coingtum	k1gNnPc2
•	•	k?
Effry	Effra	k1gMnSc2
•	•	k?
Éparcy	Éparca	k1gMnSc2
•	•	k?
La	la	k1gNnSc1
Hérie	Hérie	k1gFnSc2
•	•	k?
Hirson	Hirson	k1gInSc1
•	•	k?
Iviers	Iviers	k1gInSc1
•	•	k?
Jeantes	Jeantes	k1gInSc1
•	•	k?
Landouzy-la-Ville	Landouzy-la-Ville	k1gInSc1
•	•	k?
Leuze	Leuze	k1gFnSc2
•	•	k?
Logny-lè	Logny-lè	k1gInSc1
•	•	k?
Martigny	Martigna	k1gFnSc2
•	•	k?
Mondrepuis	Mondrepuis	k1gFnSc2
•	•	k?
Mont-Saint-Jean	Mont-Saint-Jean	k1gMnSc1
•	•	k?
Neuve-Maison	Neuve-Maison	k1gMnSc1
•	•	k?
Ohis	Ohis	k1gInSc1
•	•	k?
Origny-en-Thiérache	Origny-en-Thiérach	k1gFnSc2
•	•	k?
Saint-Clément	Saint-Clément	k1gMnSc1
•	•	k?
Saint-Michel	Saint-Michel	k1gMnSc1
•	•	k?
Watigny	Watigna	k1gFnSc2
•	•	k?
Wimy	Wima	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
</s>
