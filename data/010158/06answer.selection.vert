<s>
Digitální	digitální	k2eAgInSc1d1	digitální
negativ	negativ	k1gInSc1	negativ
(	(	kIx(	(
<g/>
DNG	DNG	kA	DNG
<g/>
,	,	kIx,	,
Digital	Digital	kA	Digital
Negative	negativ	k1gInSc5	negativ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
otevřený	otevřený	k2eAgInSc4d1	otevřený
formát	formát	k1gInSc4	formát
bezeztrátového	bezeztrátový	k2eAgInSc2d1	bezeztrátový
zápisu	zápis	k1gInSc2	zápis
digitalizovaného	digitalizovaný	k2eAgInSc2d1	digitalizovaný
obrázku	obrázek	k1gInSc2	obrázek
originálně	originálně	k6eAd1	originálně
pořízeného	pořízený	k2eAgInSc2d1	pořízený
fotografického	fotografický	k2eAgInSc2d1	fotografický
nebo	nebo	k8xC	nebo
skenového	skenový	k2eAgInSc2d1	skenový
snímku	snímek	k1gInSc2	snímek
<g/>
.	.	kIx.	.
</s>
