<p>
<s>
Digitální	digitální	k2eAgInSc1d1	digitální
negativ	negativ	k1gInSc1	negativ
(	(	kIx(	(
<g/>
DNG	DNG	kA	DNG
<g/>
,	,	kIx,	,
Digital	Digital	kA	Digital
Negative	negativ	k1gInSc5	negativ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
otevřený	otevřený	k2eAgInSc4d1	otevřený
formát	formát	k1gInSc4	formát
bezeztrátového	bezeztrátový	k2eAgInSc2d1	bezeztrátový
zápisu	zápis	k1gInSc2	zápis
digitalizovaného	digitalizovaný	k2eAgInSc2d1	digitalizovaný
obrázku	obrázek	k1gInSc2	obrázek
originálně	originálně	k6eAd1	originálně
pořízeného	pořízený	k2eAgInSc2d1	pořízený
fotografického	fotografický	k2eAgInSc2d1	fotografický
nebo	nebo	k8xC	nebo
skenového	skenový	k2eAgInSc2d1	skenový
snímku	snímek	k1gInSc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
formát	formát	k1gInSc4	formát
zavedla	zavést	k5eAaPmAgFnS	zavést
firma	firma	k1gFnSc1	firma
Adobe	Adobe	kA	Adobe
pro	pro	k7c4	pro
digitální	digitální	k2eAgFnSc4d1	digitální
fotografii	fotografia	k1gFnSc4	fotografia
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
uvedení	uvedení	k1gNnSc1	uvedení
mělo	mít	k5eAaImAgNnS	mít
podobu	podoba	k1gFnSc4	podoba
první	první	k4xOgFnSc2	první
verze	verze	k1gFnSc2	verze
specifikace	specifikace	k1gFnSc2	specifikace
DNG	DNG	kA	DNG
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
produkty	produkt	k1gInPc7	produkt
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
bezplatné	bezplatný	k2eAgFnSc2d1	bezplatná
utility	utilita	k1gFnSc2	utilita
pro	pro	k7c4	pro
převod	převod	k1gInSc4	převod
do	do	k7c2	do
DNG	DNG	kA	DNG
a	a	k8xC	a
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Veškerý	veškerý	k3xTgInSc1	veškerý
software	software	k1gInSc1	software
Adobe	Adobe	kA	Adobe
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Adobe	Adobe	kA	Adobe
Photoshop	Photoshop	k1gInSc1	Photoshop
a	a	k8xC	a
Adobe	Adobe	kA	Adobe
Lightroom	Lightroom	k1gInSc1	Lightroom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
tento	tento	k3xDgInSc4	tento
formát	formát	k1gInSc4	formát
podporuje	podporovat	k5eAaImIp3nS	podporovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
doby	doba	k1gFnSc2	doba
uvedení	uvedení	k1gNnSc2	uvedení
DNG	DNG	kA	DNG
formátu	formát	k1gInSc2	formát
se	se	k3xPyFc4	se
za	za	k7c4	za
digitální	digitální	k2eAgInSc4d1	digitální
negativ	negativ	k1gInSc4	negativ
pokládal	pokládat	k5eAaImAgMnS	pokládat
formát	formát	k1gInSc4	formát
RAW	RAW	kA	RAW
<g/>
,	,	kIx,	,
nenormalizovaný	normalizovaný	k2eNgInSc1d1	nenormalizovaný
a	a	k8xC	a
velice	velice	k6eAd1	velice
nejednotný	jednotný	k2eNgInSc1d1	nejednotný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Norma	Norma	k1gFnSc1	Norma
==	==	k?	==
</s>
</p>
<p>
<s>
DNG	DNG	kA	DNG
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
standardního	standardní	k2eAgInSc2d1	standardní
otevřeného	otevřený	k2eAgInSc2d1	otevřený
formátu	formát	k1gInSc2	formát
TIFF	TIFF	kA	TIFF
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
široké	široký	k2eAgNnSc1d1	široké
užití	užití	k1gNnSc1	užití
průvodních	průvodní	k2eAgFnPc2d1	průvodní
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
obrázku	obrázek	k1gInSc6	obrázek
(	(	kIx(	(
<g/>
metadat	metadat	k5eAaImF	metadat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
používání	používání	k1gNnSc1	používání
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
volné	volný	k2eAgFnSc2d1	volná
–	–	k?	–
Adobe	Adobe	kA	Adobe
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
licenci	licence	k1gFnSc4	licence
a	a	k8xC	a
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistují	existovat	k5eNaImIp3nP	existovat
žádná	žádný	k3yNgNnPc1	žádný
omezení	omezení	k1gNnPc1	omezení
užití	užití	k1gNnSc3	užití
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
či	či	k8xC	či
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Adobe	Adobe	kA	Adobe
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
shoda	shoda	k1gFnSc1	shoda
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
tato	tento	k3xDgFnSc1	tento
norma	norma	k1gFnSc1	norma
spravována	spravovat	k5eAaImNgFnS	spravovat
normalizační	normalizační	k2eAgFnSc7d1	normalizační
organizací	organizace	k1gFnSc7	organizace
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
otevřeni	otevřen	k2eAgMnPc1d1	otevřen
této	tento	k3xDgFnSc3	tento
myšlence	myšlenka	k1gFnSc3	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Adobe	Adobe	kA	Adobe
předložil	předložit	k5eAaPmAgInS	předložit
normu	norma	k1gFnSc4	norma
DNG	DNG	kA	DNG
organizaci	organizace	k1gFnSc4	organizace
ISO	ISO	kA	ISO
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
zařadila	zařadit	k5eAaPmAgFnS	zařadit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
revize	revize	k1gFnSc2	revize
normy	norma	k1gFnSc2	norma
TIFF	TIFF	kA	TIFF
<g/>
/	/	kIx~	/
<g/>
EP	EP	kA	EP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přijetí	přijetí	k1gNnSc1	přijetí
formátu	formát	k1gInSc2	formát
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
různosti	různost	k1gFnSc3	různost
podob	podoba	k1gFnPc2	podoba
surových	surový	k2eAgNnPc2d1	surové
dat	datum	k1gNnPc2	datum
snímků	snímek	k1gInPc2	snímek
je	být	k5eAaImIp3nS	být
převod	převod	k1gInSc1	převod
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
standard	standard	k1gInSc4	standard
někdy	někdy	k6eAd1	někdy
nejistý	jistý	k2eNgInSc1d1	nejistý
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
zejména	zejména	k9	zejména
u	u	k7c2	u
surových	surový	k2eAgNnPc2d1	surové
dat	datum	k1gNnPc2	datum
pocházejících	pocházející	k2eAgMnPc2d1	pocházející
z	z	k7c2	z
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
nepředpokládají	předpokládat	k5eNaImIp3nP	předpokládat
ukládání	ukládání	k1gNnSc3	ukládání
snímků	snímek	k1gInPc2	snímek
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
RAW	RAW	kA	RAW
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
v	v	k7c6	v
JPEG	JPEG	kA	JPEG
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
upravených	upravený	k2eAgInPc2d1	upravený
pro	pro	k7c4	pro
export	export	k1gInSc4	export
snímků	snímek	k1gInPc2	snímek
RAW	RAW	kA	RAW
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jindy	jindy	k6eAd1	jindy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
výrobci	výrobce	k1gMnPc1	výrobce
fotopřístrojů	fotopřístroj	k1gInPc2	fotopřístroj
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
k	k	k7c3	k
exportu	export	k1gInSc3	export
snímků	snímek	k1gInPc2	snímek
v	v	k7c6	v
DNG	DNG	kA	DNG
(	(	kIx(	(
<g/>
Pentax	Pentax	k1gInSc1	Pentax
<g/>
,	,	kIx,	,
Casio	Casio	k1gMnSc1	Casio
<g/>
,	,	kIx,	,
Leica	Leica	k1gMnSc1	Leica
<g/>
,	,	kIx,	,
Sinar	Sinar	k1gMnSc1	Sinar
<g/>
,	,	kIx,	,
Ricoh	Ricoh	k1gMnSc1	Ricoh
<g/>
,	,	kIx,	,
Samsung	Samsung	kA	Samsung
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
dosud	dosud	k6eAd1	dosud
nikoliv	nikoliv	k9	nikoliv
(	(	kIx(	(
<g/>
Canon	Canon	kA	Canon
<g/>
,	,	kIx,	,
Nikon	Nikon	k1gNnSc1	Nikon
<g/>
,	,	kIx,	,
Sony	Sony	kA	Sony
<g/>
,	,	kIx,	,
Panasonic	Panasonic	kA	Panasonic
<g/>
,	,	kIx,	,
Olympus	Olympus	k1gMnSc1	Olympus
<g/>
,	,	kIx,	,
Fuji	Fuji	kA	Fuji
<g/>
,	,	kIx,	,
Sigma	sigma	k1gNnSc1	sigma
<g/>
)	)	kIx)	)
a	a	k8xC	a
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
verze	verze	k1gFnSc2	verze
RAWu	RAWus	k1gInSc2	RAWus
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýkonnější	výkonný	k2eAgInPc1d3	nejvýkonnější
mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
již	již	k6eAd1	již
také	také	k6eAd1	také
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
své	svůj	k3xOyFgInPc4	svůj
snímky	snímek	k1gInPc4	snímek
v	v	k7c6	v
surové	surový	k2eAgFnSc6d1	surová
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
obecně	obecně	k6eAd1	obecně
právě	právě	k9	právě
v	v	k7c6	v
DNG	DNG	kA	DNG
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Digital	Digital	kA	Digital
Negative	negativ	k1gInSc5	negativ
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
