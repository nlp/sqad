<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pštross	Pštrossa	k1gFnPc2	Pštrossa
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1885	[number]	k4	1885
v	v	k7c6	v
Berouně	Beroun	k1gInSc6	Beroun
-	-	kIx~	-
1950	[number]	k4	1950
?	?	kIx.	?
</s>
<s>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
profesor	profesor	k1gMnSc1	profesor
antické	antický	k2eAgFnSc2d1	antická
architektury	architektura	k1gFnSc2	architektura
na	na	k7c6	na
Českém	český	k2eAgNnSc6d1	české
vysokém	vysoký	k2eAgNnSc6d1	vysoké
učení	učení	k1gNnSc6	učení
technickém	technický	k2eAgNnSc6d1	technické
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stručný	stručný	k2eAgInSc4d1	stručný
životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pštross	Pštross	k1gInSc4	Pštross
byl	být	k5eAaImAgMnS	být
absolventem	absolvent	k1gMnSc7	absolvent
fakulty	fakulta	k1gFnSc2	fakulta
architektury	architektura	k1gFnSc2	architektura
na	na	k7c4	na
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1903	[number]	k4	1903
až	až	k9	až
1908	[number]	k4	1908
<g/>
/	/	kIx~	/
<g/>
1909	[number]	k4	1909
byl	být	k5eAaImAgMnS	být
žákem	žák	k1gMnSc7	žák
významného	významný	k2eAgMnSc2d1	významný
českého	český	k2eAgMnSc2d1	český
architekta	architekt	k1gMnSc2	architekt
<g/>
,	,	kIx,	,
pedagoga	pedagog	k1gMnSc2	pedagog
<g/>
,	,	kIx,	,
designéra	designér	k1gMnSc2	designér
a	a	k8xC	a
restaurátora	restaurátor	k1gMnSc2	restaurátor
Josefa	Josef	k1gMnSc2	Josef
Schulze	Schulz	k1gMnSc2	Schulz
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
ČVUT	ČVUT	kA	ČVUT
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
studijní	studijní	k2eAgFnPc4d1	studijní
cesty	cesta	k1gFnPc4	cesta
po	po	k7c6	po
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
-	-	kIx~	-
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
Turecku	Turecko	k1gNnSc6	Turecko
a	a	k8xC	a
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Docentem	docent	k1gMnSc7	docent
antické	antický	k2eAgFnSc2d1	antická
architektury	architektura	k1gFnSc2	architektura
na	na	k7c4	na
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pštross	Pštross	k1gInSc1	Pštross
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Františka	František	k1gMnSc2	František
Pštrossová	Pštrossová	k1gFnSc1	Pštrossová
nebyli	být	k5eNaImAgMnP	být
židovského	židovský	k2eAgMnSc2d1	židovský
původu	původ	k1gInSc6	původ
jejich	jejich	k3xOp3gFnSc1	jejich
stopa	stopa	k1gFnSc1	stopa
končí	končit	k5eAaImIp3nS	končit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
-	-	kIx~	-
během	během	k7c2	během
válečných	válečný	k2eAgNnPc2d1	válečné
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odcestovali	odcestovat	k5eAaPmAgMnP	odcestovat
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Charakter	charakter	k1gInSc1	charakter
odborného	odborný	k2eAgNnSc2d1	odborné
působení	působení	k1gNnSc2	působení
==	==	k?	==
</s>
</p>
<p>
<s>
Realizace	realizace	k1gFnSc1	realizace
veřejných	veřejný	k2eAgFnPc2d1	veřejná
a	a	k8xC	a
obytných	obytný	k2eAgFnPc2d1	obytná
budov	budova	k1gFnPc2	budova
</s>
</p>
<p>
<s>
Navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
rodinné	rodinný	k2eAgInPc4d1	rodinný
domy	dům	k1gInPc4	dům
<g/>
,	,	kIx,	,
kancelářské	kancelářský	k2eAgFnSc2d1	kancelářská
<g/>
,	,	kIx,	,
občanské	občanský	k2eAgFnSc2d1	občanská
a	a	k8xC	a
dopravní	dopravní	k2eAgFnSc2d1	dopravní
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Interiérová	interiérový	k2eAgFnSc1d1	interiérová
tvorba	tvorba	k1gFnSc1	tvorba
</s>
</p>
<p>
<s>
Nositel	nositel	k1gMnSc1	nositel
řady	řada	k1gFnSc2	řada
ocenění	ocenění	k1gNnPc2	ocenění
v	v	k7c6	v
architektonických	architektonický	k2eAgFnPc6d1	architektonická
soutěžích	soutěž	k1gFnPc6	soutěž
(	(	kIx(	(
<g/>
na	na	k7c4	na
regulaci	regulace	k1gFnSc4	regulace
měst	město	k1gNnPc2	město
Berouna	Beroun	k1gInSc2	Beroun
a	a	k8xC	a
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
,	,	kIx,	,
na	na	k7c4	na
budovu	budova	k1gFnSc4	budova
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgFnSc1d1	obchodní
akademie	akademie	k1gFnSc1	akademie
v	v	k7c6	v
Berouně	Beroun	k1gInSc6	Beroun
a	a	k8xC	a
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Antické	antický	k2eAgFnSc3d1	antická
architektuře	architektura	k1gFnSc3	architektura
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
teoreticky	teoreticky	k6eAd1	teoreticky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
odborné	odborný	k2eAgFnSc2d1	odborná
studie	studie	k1gFnSc2	studie
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Chronologický	chronologický	k2eAgInSc1d1	chronologický
přehled	přehled	k1gInSc1	přehled
realizací	realizace	k1gFnPc2	realizace
==	==	k?	==
</s>
</p>
<p>
<s>
1918	[number]	k4	1918
-	-	kIx~	-
Waldesovo	Waldesův	k2eAgNnSc1d1	Waldesovo
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
Fantou	Fanta	k1gMnSc7	Fanta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moskevská	moskevský	k2eAgFnSc1d1	Moskevská
čp.	čp.	k?	čp.
262	[number]	k4	262
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Vršovice	Vršovice	k1gFnPc5	Vršovice
</s>
</p>
<p>
<s>
1920	[number]	k4	1920
-	-	kIx~	-
1922	[number]	k4	1922
-	-	kIx~	-
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
Beroun	Beroun	k1gInSc1	Beroun
</s>
</p>
<p>
<s>
1924	[number]	k4	1924
-	-	kIx~	-
Ředitelská	ředitelský	k2eAgFnSc1d1	ředitelská
vila	vila	k1gFnSc1	vila
<g/>
,	,	kIx,	,
Beroun	Beroun	k1gInSc1	Beroun
</s>
</p>
<p>
<s>
1926	[number]	k4	1926
-	-	kIx~	-
České	český	k2eAgNnSc1d1	české
státní	státní	k2eAgNnSc1d1	státní
reálné	reálný	k2eAgNnSc1d1	reálné
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc2d1	Česká
mládeže	mládež	k1gFnSc2	mládež
čp.	čp.	k?	čp.
360	[number]	k4	360
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
–	–	k?	–
Klíše	Klíše	k1gFnSc2	Klíše
</s>
</p>
<p>
<s>
1928	[number]	k4	1928
-	-	kIx~	-
1932	[number]	k4	1932
-	-	kIx~	-
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
Okresní	okresní	k2eAgInSc1d1	okresní
úřad	úřad	k1gInSc1	úřad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Blatná	blatný	k2eAgFnSc1d1	Blatná
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
2015	[number]	k4	2015
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
T.G.	T.G.	k1gMnSc2	T.G.
<g/>
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1929	[number]	k4	1929
-	-	kIx~	-
1931	[number]	k4	1931
-	-	kIx~	-
Nádraží	nádraží	k1gNnSc1	nádraží
<g/>
,	,	kIx,	,
Rokycany	Rokycany	k1gInPc1	Rokycany
</s>
</p>
<p>
<s>
1934	[number]	k4	1934
-	-	kIx~	-
Velitelská	velitelský	k2eAgFnSc1d1	velitelská
vila	vila	k1gFnSc1	vila
<g/>
,	,	kIx,	,
Strašice	Strašice	k1gFnPc1	Strašice
</s>
</p>
<p>
<s>
1936	[number]	k4	1936
-	-	kIx~	-
Hotel	hotel	k1gInSc1	hotel
Tlustý	tlustý	k2eAgInSc1d1	tlustý
<g/>
,	,	kIx,	,
Strašice	Strašice	k1gFnPc4	Strašice
</s>
</p>
<p>
<s>
1938	[number]	k4	1938
-	-	kIx~	-
1939	[number]	k4	1939
-	-	kIx~	-
Kancelářský	kancelářský	k2eAgInSc1d1	kancelářský
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
Nádražní	nádražní	k2eAgFnSc1d1	nádražní
ul	ul	kA	ul
<g/>
.	.	kIx.	.
č.	č.	k?	č.
<g/>
p.	p.	k?	p.
274	[number]	k4	274
<g/>
/	/	kIx~	/
<g/>
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Smíchov	Smíchov	k1gInSc1	Smíchov
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
-	-	kIx~	-
Rodinný	rodinný	k2eAgInSc1d1	rodinný
dům	dům	k1gInSc1	dům
Františky	Františka	k1gFnSc2	Františka
Pštrossové	Pštrossové	k2eAgInSc2d1	Pštrossové
<g/>
,	,	kIx,	,
Valtrova	Valtrův	k2eAgInSc2d1	Valtrův
č.	č.	k?	č.
<g/>
p.	p.	k?	p.
33	[number]	k4	33
<g/>
,	,	kIx,	,
391	[number]	k4	391
65	[number]	k4	65
Bechyně	Bechyně	k1gMnSc1	Bechyně
Některé	některý	k3yIgFnPc4	některý
stavby	stavba	k1gFnPc4	stavba
Z.	Z.	kA	Z.
Pštrosse	Pštrosse	k1gFnSc1	Pštrosse
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Z.	Z.	kA	Z.
Pštross	Pštross	k1gInSc1	Pštross
v	v	k7c6	v
encyklopediích	encyklopedie	k1gFnPc6	encyklopedie
a	a	k8xC	a
slovnících	slovník	k1gInPc6	slovník
==	==	k?	==
</s>
</p>
<p>
<s>
1936	[number]	k4	1936
-	-	kIx~	-
Kulturní	kulturní	k2eAgInSc1d1	kulturní
adresář	adresář	k1gInSc1	adresář
ČSR	ČSR	kA	ČSR
(	(	kIx(	(
<g/>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
žijících	žijící	k2eAgMnPc2d1	žijící
kulturních	kulturní	k2eAgMnPc2d1	kulturní
pracovníků	pracovník	k1gMnPc2	pracovník
a	a	k8xC	a
pracovnic	pracovnice	k1gFnPc2	pracovnice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1950	[number]	k4	1950
-	-	kIx~	-
TOMAN	Toman	k1gMnSc1	Toman
<g/>
,	,	kIx,	,
Prokop	Prokop	k1gMnSc1	Prokop
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
slovník	slovník	k1gInSc1	slovník
československých	československý	k2eAgMnPc2d1	československý
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
s.	s.	k?	s.
327	[number]	k4	327
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
-	-	kIx~	-
Nový	nový	k2eAgInSc1d1	nový
slovník	slovník	k1gInSc1	slovník
československých	československý	k2eAgMnPc2d1	československý
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
(	(	kIx(	(
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
;	;	kIx,	;
L	L	kA	L
-	-	kIx~	-
Ž	Ž	kA	Ž
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
-	-	kIx~	-
Slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
slovenských	slovenský	k2eAgMnPc2d1	slovenský
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
XII	XII	kA	XII
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
Por	Por	k1gMnSc1	Por
-	-	kIx~	-
Rj	Rj	k1gMnSc1	Rj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
centrum	centrum	k1gNnSc1	centrum
Chagall	Chagalla	k1gFnPc2	Chagalla
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
s.	s.	k?	s.
183	[number]	k4	183
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
-	-	kIx~	-
VLČEK	Vlček	k1gMnSc1	Vlček
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
architektů	architekt	k1gMnPc2	architekt
<g/>
,	,	kIx,	,
stavitelů	stavitel	k1gMnPc2	stavitel
<g/>
,	,	kIx,	,
zedníků	zedník	k1gMnPc2	zedník
a	a	k8xC	a
kameníků	kameník	k1gMnPc2	kameník
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
s.	s.	k?	s.
533	[number]	k4	533
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SELLNEROVÁ	SELLNEROVÁ	kA	SELLNEROVÁ
<g/>
,	,	kIx,	,
Alena	Alena	k1gFnSc1	Alena
<g/>
;	;	kIx,	;
PISTORIUS	PISTORIUS	kA	PISTORIUS
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Architekt	architekt	k1gMnSc1	architekt
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
Pštross	Pštross	k1gInSc1	Pštross
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
a	a	k8xC	a
tvorba	tvorba	k1gFnSc1	tvorba
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
J.E.	J.E.	k1gFnSc1	J.E.
Purkyně	Purkyně	k1gFnSc1	Purkyně
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Architekti	architekt	k1gMnPc1	architekt
<g/>
;	;	kIx,	;
moderní	moderní	k2eAgFnSc1d1	moderní
architektura	architektura	k1gFnSc1	architektura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
92	[number]	k4	92
stran	strana	k1gFnPc2	strana
<g/>
:	:	kIx,	:
ilustrace	ilustrace	k1gFnPc1	ilustrace
(	(	kIx(	(
<g/>
některé	některý	k3yIgNnSc1	některý
barevné	barevný	k2eAgNnSc1d1	barevné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
půdorysy	půdorys	k1gInPc1	půdorys
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
památkový	památkový	k2eAgInSc1d1	památkový
ústav	ústav	k1gInSc1	ústav
-	-	kIx~	-
online	onlinout	k5eAaPmIp3nS	onlinout
katalog	katalog	k1gInSc4	katalog
<g/>
:	:	kIx,	:
Odborné	odborný	k2eAgFnPc1d1	odborná
knihovny	knihovna	k1gFnPc1	knihovna
a	a	k8xC	a
dokumentační	dokumentační	k2eAgInPc1d1	dokumentační
fondy	fond	k1gInPc1	fond
<g/>
,	,	kIx,	,
Studentská	studentský	k2eAgFnSc1d1	studentská
práce	práce	k1gFnSc1	práce
(	(	kIx(	(
<g/>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Signatury	signatura	k1gFnSc2	signatura
svazku	svazek	k1gInSc2	svazek
<g/>
:	:	kIx,	:
C	C	kA	C
<g/>
3378	[number]	k4	3378
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
Pštross	Pštrossa	k1gFnPc2	Pštrossa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Záznam	záznam	k1gInSc1	záznam
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
národních	národní	k2eAgFnPc2d1	národní
autorit	autorita	k1gFnPc2	autorita
-	-	kIx~	-
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
Pštross	Pštross	k1gInSc1	Pštross
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
ČR	ČR	kA	ČR
-	-	kIx~	-
Pštross	Pštross	k1gInSc1	Pštross
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Antický	antický	k2eAgInSc4d1	antický
dům	dům	k1gInSc4	dům
obytný	obytný	k2eAgInSc4d1	obytný
<g/>
:	:	kIx,	:
příspěvek	příspěvek	k1gInSc4	příspěvek
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
rodinného	rodinný	k2eAgInSc2d1	rodinný
domu	dům	k1gInSc2	dům
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hellenistické	hellenistický	k2eAgFnSc6d1	hellenistický
a	a	k8xC	a
římské	římský	k2eAgFnSc6d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
řecký	řecký	k2eAgInSc1d1	řecký
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
1915	[number]	k4	1915
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
104	[number]	k4	104
s.	s.	k?	s.
</s>
</p>
