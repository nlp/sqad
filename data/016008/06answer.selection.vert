<s>
Stůl	stůl	k1gInSc1
je	být	k5eAaImIp3nS
druh	druh	k1gInSc4
nábytku	nábytek	k1gInSc2
tvořený	tvořený	k2eAgInSc4d1
deskou	deska	k1gFnSc7
a	a	k8xC
nohou	noha	k1gFnSc7
či	či	k8xC
nohami	noha	k1gFnPc7
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
má	mít	k5eAaImIp3nS
desku	deska	k1gFnSc4
podloženou	podložený	k2eAgFnSc4d1
lubem	lub	k1gInSc7
či	či	k8xC
doplněnou	doplněný	k2eAgFnSc7d1
zásuvkou	zásuvka	k1gFnSc7
nebo	nebo	k8xC
zásuvkami	zásuvka	k1gFnPc7
<g/>
.	.	kIx.
</s>