<s>
Pyl	pyl	k1gInSc1	pyl
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
drobných	drobný	k2eAgFnPc2d1	drobná
samčích	samčí	k2eAgFnPc2d1	samčí
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
buněk	buňka	k1gFnPc2	buňka
semenných	semenný	k2eAgFnPc2d1	semenná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
samčí	samčí	k2eAgFnSc1d1	samčí
buňka	buňka	k1gFnSc1	buňka
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pylové	pylový	k2eAgNnSc1d1	pylové
zrno	zrno	k1gNnSc1	zrno
(	(	kIx(	(
<g/>
granum	granum	k1gNnSc1	granum
pollinis	pollinis	k1gFnSc2	pollinis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
pylu	pyl	k1gInSc2	pyl
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
žlutá	žlutý	k2eAgFnSc1d1	žlutá
nebo	nebo	k8xC	nebo
nažloutlá	nažloutlý	k2eAgFnSc1d1	nažloutlá
<g/>
.	.	kIx.	.
</s>
<s>
Pyly	pyl	k1gInPc1	pyl
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
palynologie	palynologie	k1gFnSc2	palynologie
<g/>
.	.	kIx.	.
</s>
<s>
Pyl	pyl	k1gInSc1	pyl
tvořený	tvořený	k2eAgInSc1d1	tvořený
pylovými	pylový	k2eAgNnPc7d1	pylové
zrny	zrno	k1gNnPc7	zrno
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
v	v	k7c6	v
prašníku	prašník	k1gInSc6	prašník
z	z	k7c2	z
parenchymatickými	parenchymatický	k2eAgFnPc7d1	parenchymatická
stěnami	stěna	k1gFnPc7	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Prašník	prašník	k1gInSc1	prašník
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
samčího	samčí	k2eAgInSc2d1	samčí
orgánu	orgán	k1gInSc2	orgán
tyčinky	tyčinka	k1gFnSc2	tyčinka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgInPc7	dva
prašnými	prašný	k2eAgInPc7d1	prašný
váčky	váček	k1gInPc7	váček
(	(	kIx(	(
<g/>
theca	theca	k1gFnSc1	theca
<g/>
)	)	kIx)	)
obsahující	obsahující	k2eAgFnSc1d1	obsahující
každý	každý	k3xTgMnSc1	každý
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
prašných	prašný	k2eAgNnPc6d1	prašné
pouzdrech	pouzdro	k1gNnPc6	pouzdro
(	(	kIx(	(
<g/>
loculamentum	loculamentum	k1gNnSc4	loculamentum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
pylová	pylový	k2eAgNnPc4d1	pylové
zrna	zrno	k1gNnPc4	zrno
z	z	k7c2	z
pylotvorného	pylotvorný	k2eAgNnSc2d1	pylotvorný
pletiva	pletivo	k1gNnSc2	pletivo
(	(	kIx(	(
<g/>
archesporium	archesporium	k1gNnSc1	archesporium
<g/>
)	)	kIx)	)
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zraním	zranit	k5eAaPmIp1nS	zranit
pylových	pylový	k2eAgNnPc2d1	pylové
zrn	zrno	k1gNnPc2	zrno
se	se	k3xPyFc4	se
v	v	k7c6	v
prašných	prašný	k2eAgNnPc6d1	prašné
pouzdrech	pouzdro	k1gNnPc6	pouzdro
diferencuje	diferencovat	k5eAaImIp3nS	diferencovat
výstelka	výstelka	k1gFnSc1	výstelka
(	(	kIx(	(
<g/>
tapetum	tapetum	k1gNnSc1	tapetum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
pylová	pylový	k2eAgNnPc1d1	pylové
zrna	zrno	k1gNnPc1	zrno
vyživuje	vyživovat	k5eAaImIp3nS	vyživovat
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
vzniká	vznikat	k5eAaImIp3nS	vznikat
vláknitá	vláknitý	k2eAgFnSc1d1	vláknitá
vrstva	vrstva	k1gFnSc1	vrstva
(	(	kIx(	(
<g/>
endothecium	endothecium	k1gNnSc1	endothecium
<g/>
)	)	kIx)	)
napomáhající	napomáhající	k2eAgNnSc1d1	napomáhající
otevření	otevření	k1gNnSc1	otevření
prašného	prašný	k2eAgNnSc2d1	prašné
pouzdra	pouzdro	k1gNnSc2	pouzdro
po	po	k7c6	po
dozrání	dozrání	k1gNnSc6	dozrání
pylu	pyl	k1gInSc2	pyl
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prvotních	prvotní	k2eAgFnPc2d1	prvotní
sporogenních	sporogenní	k2eAgFnPc2d1	sporogenní
buněk	buňka	k1gFnPc2	buňka
vznikají	vznikat	k5eAaImIp3nP	vznikat
mitotickým	mitotický	k2eAgNnSc7d1	mitotické
dělením	dělení	k1gNnSc7	dělení
diploidní	diploidní	k2eAgFnSc2d1	diploidní
mateřské	mateřský	k2eAgFnSc2d1	mateřská
buňky	buňka	k1gFnSc2	buňka
pylových	pylový	k2eAgNnPc2d1	pylové
zrn	zrno	k1gNnPc2	zrno
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
redukčním	redukční	k2eAgNnSc7d1	redukční
dělením	dělení	k1gNnSc7	dělení
(	(	kIx(	(
<g/>
meiózou	meióza	k1gFnSc7	meióza
<g/>
)	)	kIx)	)
dále	daleko	k6eAd2	daleko
vznikají	vznikat	k5eAaImIp3nP	vznikat
čtveřice	čtveřice	k1gFnPc1	čtveřice
(	(	kIx(	(
<g/>
tetrády	tetráda	k1gFnPc1	tetráda
<g/>
)	)	kIx)	)
haploidních	haploidní	k2eAgFnPc2d1	haploidní
pylových	pylový	k2eAgFnPc2d1	pylová
zrn	zrno	k1gNnPc2	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Pylová	pylový	k2eAgNnPc4d1	pylové
zrna	zrno	k1gNnPc4	zrno
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
tetrádách	tetráda	k1gFnPc6	tetráda
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
různě	různě	k6eAd1	různě
<g/>
,	,	kIx,	,
např.	např.	kA	např.
lineárně	lineárně	k6eAd1	lineárně
<g/>
,	,	kIx,	,
příčné	příčný	k2eAgFnPc1d1	příčná
<g/>
,	,	kIx,	,
tetraedricky	tetraedricky	k6eAd1	tetraedricky
<g/>
,	,	kIx,	,
izobilaterálně	izobilaterálně	k6eAd1	izobilaterálně
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
jsou	být	k5eAaImIp3nP	být
spojena	spojen	k2eAgFnSc1d1	spojena
vlákny	vlákna	k1gFnPc1	vlákna
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
postupně	postupně	k6eAd1	postupně
enzymaticky	enzymaticky	k6eAd1	enzymaticky
rozrušena	rozrušen	k2eAgFnSc1d1	rozrušena
a	a	k8xC	a
tetrády	tetráda	k1gFnPc1	tetráda
se	se	k3xPyFc4	se
rozpadají	rozpadat	k5eAaPmIp3nP	rozpadat
<g/>
,	,	kIx,	,
pylová	pylový	k2eAgNnPc1d1	pylové
zrna	zrno	k1gNnPc1	zrno
se	se	k3xPyFc4	se
osamostatňují	osamostatňovat	k5eAaImIp3nP	osamostatňovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
rostlin	rostlina	k1gFnPc2	rostlina
ale	ale	k8xC	ale
tetrády	tetráda	k1gFnSc2	tetráda
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
zachovány	zachován	k2eAgInPc1d1	zachován
vřesy	vřes	k1gInPc1	vřes
<g/>
,	,	kIx,	,
sítiny	sítina	k1gFnPc1	sítina
<g/>
,	,	kIx,	,
rosnatky	rosnatka	k1gFnPc1	rosnatka
<g/>
,	,	kIx,	,
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
do	do	k7c2	do
dvojic	dvojice	k1gFnPc2	dvojice
nebo	nebo	k8xC	nebo
do	do	k7c2	do
početnějších	početní	k2eAgInPc2d2	početnější
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
čeledi	čeleď	k1gFnSc2	čeleď
vstavačovitých	vstavačovitý	k2eAgInPc2d1	vstavačovitý
a	a	k8xC	a
klejichovitých	klejichovitý	k2eAgInPc2d1	klejichovitý
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
spolu	spolu	k6eAd1	spolu
slepena	slepen	k2eAgNnPc1d1	slepeno
všechna	všechen	k3xTgNnPc1	všechen
pylová	pylový	k2eAgNnPc1d1	pylové
zrna	zrno	k1gNnPc1	zrno
prašného	prašný	k2eAgNnSc2d1	prašné
pouzdra	pouzdro	k1gNnSc2	pouzdro
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
hrudku	hrudka	k1gFnSc4	hrudka
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
brylku	brylek	k1gInSc2	brylek
(	(	kIx(	(
<g/>
pollinarium	pollinarium	k1gNnSc1	pollinarium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uchycující	uchycující	k2eAgInSc4d1	uchycující
se	se	k3xPyFc4	se
lepivým	lepivý	k2eAgInSc7d1	lepivý
štítkem	štítek	k1gInSc7	štítek
na	na	k7c6	na
opylovači	opylovač	k1gInSc6	opylovač
<g/>
.	.	kIx.	.
</s>
<s>
Zraním	zranit	k5eAaPmIp1nS	zranit
se	se	k3xPyFc4	se
každé	každý	k3xTgNnSc1	každý
pylové	pylový	k2eAgNnSc1d1	pylové
zrno	zrno	k1gNnSc1	zrno
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
buňku	buňka	k1gFnSc4	buňka
vegetativní	vegetativní	k2eAgFnSc4d1	vegetativní
s	s	k7c7	s
laločným	laločný	k2eAgNnSc7d1	laločný
jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
menší	malý	k2eAgFnSc1d2	menší
generativní	generativní	k2eAgFnSc1d1	generativní
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
vegetativní	vegetativní	k2eAgFnSc2d1	vegetativní
buňky	buňka	k1gFnSc2	buňka
zanořená	zanořený	k2eAgFnSc1d1	zanořená
<g/>
.	.	kIx.	.
</s>
<s>
Pylové	pylový	k2eAgNnSc1d1	pylové
zrno	zrno	k1gNnSc1	zrno
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
dvoubuněčné	dvoubuněčný	k2eAgNnSc1d1	dvoubuněčný
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
diploidní	diploidní	k2eAgFnPc1d1	diploidní
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
generativní	generativní	k2eAgFnSc2d1	generativní
buňky	buňka	k1gFnSc2	buňka
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
buňky	buňka	k1gFnPc4	buňka
spermatické	spermatický	k2eAgFnSc2d1	spermatická
(	(	kIx(	(
<g/>
gamety	gameta	k1gFnSc2	gameta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
děje	dít	k5eAaImIp3nS	dít
až	až	k9	až
v	v	k7c6	v
klíčící	klíčící	k2eAgFnSc6d1	klíčící
pylové	pylový	k2eAgFnSc6d1	pylová
láčce	láčka	k1gFnSc6	láčka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
brukvovitých	brukvovitý	k2eAgInPc6d1	brukvovitý
<g/>
,	,	kIx,	,
hvězdnicovitých	hvězdnicovitý	k2eAgInPc6d1	hvězdnicovitý
a	a	k8xC	a
lipnicovitých	lipnicovitý	k2eAgInPc6d1	lipnicovitý
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dělení	dělení	k1gNnSc3	dělení
již	již	k6eAd1	již
v	v	k7c6	v
prašném	prašný	k2eAgNnSc6d1	prašné
pouzdru	pouzdro	k1gNnSc6	pouzdro
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
je	být	k5eAaImIp3nS	být
pylové	pylový	k2eAgNnSc1d1	pylové
zrno	zrno	k1gNnSc1	zrno
trojbuněčné	trojbuněčný	k2eAgNnSc1d1	trojbuněčný
<g/>
.	.	kIx.	.
</s>
<s>
Pylová	pylový	k2eAgNnPc1d1	pylové
zrna	zrno	k1gNnPc1	zrno
jsou	být	k5eAaImIp3nP	být
různě	různě	k6eAd1	různě
velká	velký	k2eAgNnPc1d1	velké
<g/>
,	,	kIx,	,
od	od	k7c2	od
5	[number]	k4	5
μ	μ	k?	μ
u	u	k7c2	u
pomněnky	pomněnka	k1gFnSc2	pomněnka
po	po	k7c6	po
250	[number]	k4	250
μ	μ	k?	μ
u	u	k7c2	u
tykve	tykev	k1gFnSc2	tykev
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
pylových	pylový	k2eAgNnPc2d1	pylové
zrn	zrno	k1gNnPc2	zrno
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
v	v	k7c6	v
korelaci	korelace	k1gFnSc6	korelace
se	s	k7c7	s
stupněm	stupeň	k1gInSc7	stupeň
ploidie	ploidie	k1gFnSc2	ploidie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
evoluce	evoluce	k1gFnSc2	evoluce
se	se	k3xPyFc4	se
pylová	pylový	k2eAgNnPc1d1	pylové
zrna	zrno	k1gNnPc1	zrno
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
zmenšují	zmenšovat	k5eAaImIp3nP	zmenšovat
<g/>
.	.	kIx.	.
</s>
<s>
Tvary	tvar	k1gInPc1	tvar
mívají	mívat	k5eAaImIp3nP	mívat
nejčastěji	často	k6eAd3	často
kolovité	kolovitý	k2eAgInPc1d1	kolovitý
nebo	nebo	k8xC	nebo
elipsovité	elipsovitý	k2eAgInPc1d1	elipsovitý
<g/>
,	,	kIx,	,
u	u	k7c2	u
vodních	vodní	k2eAgFnPc2d1	vodní
rostlin	rostlina	k1gFnPc2	rostlina
nitkovité	nitkovitý	k2eAgFnPc1d1	nitkovitá
<g/>
.	.	kIx.	.
</s>
<s>
Pylové	pylový	k2eAgNnSc1d1	pylové
zrno	zrno	k1gNnSc1	zrno
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
přenosu	přenos	k1gInSc2	přenos
z	z	k7c2	z
prašníku	prašník	k1gInSc2	prašník
na	na	k7c4	na
bliznu	blizna	k1gFnSc4	blizna
opatřeno	opatřen	k2eAgNnSc1d1	opatřeno
dvěma	dva	k4xCgFnPc7	dva
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
zajistit	zajistit	k5eAaPmF	zajistit
jeho	jeho	k3xOp3gNnSc4	jeho
nepoškození	nepoškození	k1gNnSc4	nepoškození
<g/>
:	:	kIx,	:
intina	intina	k1gFnSc1	intina
–	–	k?	–
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
obal	obal	k1gInSc4	obal
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
tenký	tenký	k2eAgInSc4d1	tenký
<g/>
,	,	kIx,	,
pružný	pružný	k2eAgInSc4d1	pružný
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
odolný	odolný	k2eAgInSc1d1	odolný
<g/>
,	,	kIx,	,
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
celulózy	celulóza	k1gFnSc2	celulóza
a	a	k8xC	a
pektinu	pektin	k1gInSc2	pektin
<g/>
,	,	kIx,	,
exina	exina	k6eAd1	exina
–	–	k?	–
vnější	vnější	k2eAgInSc4d1	vnější
obal	obal	k1gInSc4	obal
<g/>
,	,	kIx,	,
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc4d1	obsahující
kromě	kromě	k7c2	kromě
celulózy	celulóza	k1gFnSc2	celulóza
a	a	k8xC	a
pektinu	pektin	k1gInSc2	pektin
i	i	k9	i
kutin	kutin	k1gInSc1	kutin
<g/>
,	,	kIx,	,
sporopoleniny	sporopolenin	k2eAgInPc1d1	sporopolenin
a	a	k8xC	a
pevné	pevný	k2eAgInPc1d1	pevný
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
odolné	odolný	k2eAgInPc1d1	odolný
vůči	vůči	k7c3	vůči
působení	působení	k1gNnSc3	působení
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
zásad	zásada	k1gFnPc2	zásada
<g/>
.	.	kIx.	.
</s>
<s>
Exina	Exina	k6eAd1	Exina
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
krytosemenných	krytosemenný	k2eAgFnPc2d1	krytosemenná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nahosemenných	nahosemenný	k2eAgFnPc2d1	nahosemenná
<g/>
,	,	kIx,	,
skládat	skládat	k5eAaImF	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
pylového	pylový	k2eAgNnSc2d1	pylové
zrna	zrno	k1gNnSc2	zrno
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
entomogamních	entomogamní	k2eAgFnPc2d1	entomogamní
rostlin	rostlina	k1gFnPc2	rostlina
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
druhy	druh	k1gInPc4	druh
charakteristický	charakteristický	k2eAgMnSc1d1	charakteristický
svou	svůj	k3xOyFgFnSc7	svůj
drsností	drsnost	k1gFnSc7	drsnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
velikosti	velikost	k1gFnSc2	velikost
zrna	zrno	k1gNnSc2	zrno
důležitým	důležitý	k2eAgInSc7d1	důležitý
rozeznávacím	rozeznávací	k2eAgInSc7d1	rozeznávací
znakem	znak	k1gInSc7	znak
pro	pro	k7c4	pro
palynologií	palynologie	k1gFnSc7	palynologie
při	při	k7c6	při
určování	určování	k1gNnSc6	určování
rostlin	rostlina	k1gFnPc2	rostlina
podle	podle	k7c2	podle
pylových	pylový	k2eAgNnPc2d1	pylové
zrn	zrno	k1gNnPc2	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
povrchu	povrch	k1gInSc2	povrch
má	mít	k5eAaImIp3nS	mít
podstatný	podstatný	k2eAgInSc4d1	podstatný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
uchycení	uchycení	k1gNnSc4	uchycení
zrna	zrno	k1gNnSc2	zrno
na	na	k7c6	na
blizně	blizna	k1gFnSc6	blizna
i	i	k8xC	i
na	na	k7c6	na
opylovači	opylovač	k1gInSc6	opylovač
<g/>
,	,	kIx,	,
tomu	ten	k3xDgNnSc3	ten
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
i	i	k9	i
lepkavá	lepkavý	k2eAgFnSc1d1	lepkavá
tekutina	tekutina	k1gFnSc1	tekutina
někdy	někdy	k6eAd1	někdy
vylučována	vylučovat	k5eAaImNgFnS	vylučovat
exinou	exina	k1gFnSc7	exina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
anemogamních	anemogamní	k2eAgFnPc2d1	anemogamní
rostlin	rostlina	k1gFnPc2	rostlina
bývá	bývat	k5eAaImIp3nS	bývat
povrch	povrch	k1gInSc4	povrch
hladký	hladký	k2eAgInSc4d1	hladký
a	a	k8xC	a
nelepkavý	lepkavý	k2eNgInSc4d1	nelepkavý
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
jsou	být	k5eAaImIp3nP	být
tato	tento	k3xDgNnPc4	tento
pylová	pylový	k2eAgNnPc4d1	pylové
zrna	zrno	k1gNnPc4	zrno
opatřena	opatřit	k5eAaPmNgFnS	opatřit
přídavnými	přídavný	k2eAgInPc7d1	přídavný
vzdušnými	vzdušný	k2eAgInPc7d1	vzdušný
vaky	vak	k1gInPc7	vak
pro	pro	k7c4	pro
snazší	snadný	k2eAgInSc4d2	snadnější
přenos	přenos	k1gInSc4	přenos
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
hydrogamních	hydrogamní	k2eAgFnPc2d1	hydrogamní
rostlin	rostlina	k1gFnPc2	rostlina
chybí	chybět	k5eAaImIp3nS	chybět
exina	exina	k6eAd1	exina
<g/>
,	,	kIx,	,
nehrozí	hrozit	k5eNaImIp3nS	hrozit
vyschnutí	vyschnutí	k1gNnSc1	vyschnutí
pylu	pyl	k1gInSc2	pyl
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
styku	styk	k1gInSc2	styk
pylového	pylový	k2eAgNnSc2d1	pylové
zrna	zrno	k1gNnSc2	zrno
s	s	k7c7	s
bliznou	blizna	k1gFnSc7	blizna
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
opylení	opylení	k1gNnSc1	opylení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
opylení	opylení	k1gNnSc6	opylení
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
blizny	blizna	k1gFnSc2	blizna
a	a	k8xC	a
pylového	pylový	k2eAgNnSc2d1	pylové
zrna	zrno	k1gNnSc2	zrno
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
pyl	pyl	k1gInSc1	pyl
bliznou	blizna	k1gFnSc7	blizna
přijat	přijmout	k5eAaPmNgMnS	přijmout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
poskytnuta	poskytnut	k2eAgFnSc1d1	poskytnuta
voda	voda	k1gFnSc1	voda
pro	pro	k7c4	pro
hydrataci	hydratace	k1gFnSc4	hydratace
<g/>
.	.	kIx.	.
</s>
<s>
Hydratovaný	hydratovaný	k2eAgInSc1d1	hydratovaný
pyl	pyl	k1gInSc1	pyl
roste	růst	k5eAaImIp3nS	růst
apikálním	apikální	k2eAgInSc7d1	apikální
růstem	růst	k1gInSc7	růst
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
pylovou	pylový	k2eAgFnSc4d1	pylová
láčku	láčka	k1gFnSc4	láčka
s	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
buňkami	buňka	k1gFnPc7	buňka
(	(	kIx(	(
<g/>
buňka	buňka	k1gFnSc1	buňka
vegetativní	vegetativní	k2eAgFnSc1d1	vegetativní
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
spermatické	spermatický	k2eAgFnPc4d1	spermatická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Láčka	láčka	k1gFnSc1	láčka
proniká	pronikat	k5eAaImIp3nS	pronikat
vodícím	vodící	k2eAgNnSc7d1	vodící
pletivem	pletivo	k1gNnSc7	pletivo
čnělky	čnělka	k1gFnSc2	čnělka
do	do	k7c2	do
semeníku	semeník	k1gInSc2	semeník
a	a	k8xC	a
přenáší	přenášet	k5eAaImIp3nS	přenášet
tam	tam	k6eAd1	tam
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
obsahu	obsah	k1gInSc2	obsah
pylového	pylový	k2eAgNnSc2d1	pylové
zrna	zrno	k1gNnSc2	zrno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semeníku	semeník	k1gInSc6	semeník
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oplodnění	oplodnění	k1gNnSc3	oplodnění
oosféry	oosféra	k1gFnSc2	oosféra
v	v	k7c6	v
megagametofytu	megagametofyt	k1gInSc6	megagametofyt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
oplodnění	oplodnění	k1gNnSc3	oplodnění
nedojde	dojít	k5eNaPmIp3nS	dojít
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
<g/>
:	:	kIx,	:
sporofytická	sporofytický	k2eAgFnSc1d1	sporofytický
inkompatibilita	inkompatibilita	k1gFnSc1	inkompatibilita
–	–	k?	–
rozpoznání	rozpoznání	k1gNnSc1	rozpoznání
pylu	pyl	k1gInSc2	pyl
na	na	k7c6	na
blizně	blizna	k1gFnSc6	blizna
jako	jako	k8xC	jako
příliš	příliš	k6eAd1	příliš
příbuzného	příbuzný	k2eAgNnSc2d1	příbuzné
a	a	k8xC	a
neposkytnutí	neposkytnutí	k1gNnSc2	neposkytnutí
mu	on	k3xPp3gMnSc3	on
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
hydrataci	hydratace	k1gFnSc4	hydratace
<g/>
,	,	kIx,	,
gametofytická	gametofytický	k2eAgFnSc1d1	gametofytický
inkompatibilita	inkompatibilita	k1gFnSc1	inkompatibilita
–	–	k?	–
rozpoznání	rozpoznání	k1gNnSc1	rozpoznání
pylu	pyl	k1gInSc2	pyl
ve	v	k7c6	v
čnělce	čnělka	k1gFnSc6	čnělka
jako	jako	k8xC	jako
příliš	příliš	k6eAd1	příliš
příbuzného	příbuzný	k1gMnSc4	příbuzný
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
zničení	zničení	k1gNnSc4	zničení
samičí	samičí	k2eAgFnSc7d1	samičí
rostlinou	rostlina	k1gFnSc7	rostlina
<g/>
,	,	kIx,	,
samčí	samčí	k2eAgFnSc1d1	samčí
cytoplazmatická	cytoplazmatický	k2eAgFnSc1d1	cytoplazmatická
sterilita	sterilita	k1gFnSc1	sterilita
–	–	k?	–
neschopnost	neschopnost	k1gFnSc1	neschopnost
pylu	pyl	k1gInSc2	pyl
vyklíčit	vyklíčit	k5eAaPmF	vyklíčit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
defektní	defektní	k2eAgFnSc7d1	defektní
funkcí	funkce	k1gFnSc7	funkce
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
Sporofytická	Sporofytický	k2eAgFnSc1d1	Sporofytický
a	a	k8xC	a
gametofytická	gametofytický	k2eAgFnSc1d1	gametofytický
inkompatibilita	inkompatibilita	k1gFnSc1	inkompatibilita
jsou	být	k5eAaImIp3nP	být
způsoby	způsob	k1gInPc1	způsob
zábrany	zábrana	k1gFnSc2	zábrana
samoopylení	samoopylení	k1gNnSc2	samoopylení
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
pylová	pylový	k2eAgFnSc1d1	pylová
láčka	láčka	k1gFnSc1	láčka
proniknout	proniknout	k5eAaPmF	proniknout
přes	přes	k7c4	přes
exinu	exina	k1gFnSc4	exina
ven	ven	k6eAd1	ven
z	z	k7c2	z
pylového	pylový	k2eAgNnSc2d1	pylové
zrna	zrno	k1gNnSc2	zrno
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
exině	exino	k1gNnSc6	exino
zeslabená	zeslabený	k2eAgNnPc1d1	zeslabené
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
apertury	apertura	k1gFnSc2	apertura
<g/>
.	.	kIx.	.
</s>
<s>
Láčka	láčka	k1gFnSc1	láčka
většinou	většinou	k6eAd1	většinou
klíčí	klíčit	k5eAaImIp3nS	klíčit
jednou	jeden	k4xCgFnSc7	jeden
aperturou	apertura	k1gFnSc7	apertura
(	(	kIx(	(
<g/>
klíčení	klíčení	k1gNnSc4	klíčení
monosyfonické	monosyfonický	k2eAgFnSc2d1	monosyfonický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jen	jen	k9	jen
občas	občas	k6eAd1	občas
i	i	k9	i
více	hodně	k6eAd2	hodně
aperturami	apertura	k1gFnPc7	apertura
(	(	kIx(	(
<g/>
klíčení	klíčení	k1gNnSc4	klíčení
polysyfonické	polysyfonický	k2eAgFnSc2d1	polysyfonický
<g/>
)	)	kIx)	)
např.	např.	kA	např.
u	u	k7c2	u
lýkovce	lýkovec	k1gInSc2	lýkovec
<g/>
.	.	kIx.	.
</s>
<s>
Klíčení	klíčení	k1gNnSc1	klíčení
pylu	pyl	k1gInSc2	pyl
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rychlé	rychlý	k2eAgNnSc1d1	rychlé
a	a	k8xC	a
energeticky	energeticky	k6eAd1	energeticky
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
pyl	pyl	k1gInSc1	pyl
připravuje	připravovat	k5eAaImIp3nS	připravovat
ještě	ještě	k9	ještě
před	před	k7c7	před
opuštěním	opuštění	k1gNnSc7	opuštění
prašníku	prašník	k1gInSc2	prašník
"	"	kIx"	"
<g/>
polotovary	polotovar	k1gInPc4	polotovar
<g/>
"	"	kIx"	"
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gMnPc4	on
mohl	moct	k5eAaImAgInS	moct
při	při	k7c6	při
klíčení	klíčení	k1gNnSc6	klíčení
rychleji	rychle	k6eAd2	rychle
vyrábět	vyrábět	k5eAaImF	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
translace	translace	k1gFnSc1	translace
je	být	k5eAaImIp3nS	být
blokována	blokován	k2eAgFnSc1d1	blokována
navázáním	navázání	k1gNnSc7	navázání
rRNA	rRNA	k?	rRNA
a	a	k8xC	a
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
komplexy	komplex	k1gInPc1	komplex
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
veliké	veliký	k2eAgFnPc1d1	veliká
a	a	k8xC	a
nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
ribonukleové	ribonukleový	k2eAgFnSc2d1	ribonukleová
částečky	částečka	k1gFnSc2	částečka
RNP	RNP	kA	RNP
<g/>
,	,	kIx,	,
blokují	blokovat	k5eAaImIp3nP	blokovat
nejen	nejen	k6eAd1	nejen
translaci	translace	k1gFnSc4	translace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
chrání	chránit	k5eAaImIp3nS	chránit
mRNA	mRNA	k?	mRNA
před	před	k7c7	před
degradací	degradace	k1gFnSc7	degradace
proteasami	proteasa	k1gFnPc7	proteasa
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nahosemenných	nahosemenný	k2eAgFnPc2d1	nahosemenná
rostlin	rostlina	k1gFnPc2	rostlina
jsou	být	k5eAaImIp3nP	být
proti	proti	k7c3	proti
krytosemenným	krytosemenný	k2eAgFnPc3d1	krytosemenná
některé	některý	k3yIgFnPc4	některý
odlišnosti	odlišnost	k1gFnPc4	odlišnost
<g/>
.	.	kIx.	.
</s>
<s>
Mikrosporofyl	Mikrosporofyl	k1gInSc1	Mikrosporofyl
<g/>
,	,	kIx,	,
obdoba	obdoba	k1gFnSc1	obdoba
tyčinky	tyčinka	k1gFnSc2	tyčinka
v	v	k7c6	v
květu	květ	k1gInSc6	květ
krytosemenných	krytosemenný	k2eAgFnPc2d1	krytosemenná
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
rozlišen	rozlišit	k5eAaPmNgMnS	rozlišit
na	na	k7c4	na
nitku	nitka	k1gFnSc4	nitka
a	a	k8xC	a
prašník	prašník	k1gInSc4	prašník
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
prašné	prašný	k2eAgInPc4d1	prašný
váčky	váček	k1gInPc4	váček
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mikrosporofylu	mikrosporofyl	k1gInSc6	mikrosporofyl
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
navzájem	navzájem	k6eAd1	navzájem
nesrostlých	srostlý	k2eNgFnPc2d1	nesrostlá
mikrosporangií	mikrosporangie	k1gFnPc2	mikrosporangie
<g/>
,	,	kIx,	,
obdoby	obdoba	k1gFnSc2	obdoba
prašných	prašný	k2eAgFnPc2d1	prašná
pouzder	pouzdro	k1gNnPc2	pouzdro
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
pylová	pylový	k2eAgNnPc4d1	pylové
zrna	zrno	k1gNnPc4	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Pylové	pylový	k2eAgNnSc4d1	pylové
zrno	zrno	k1gNnSc4	zrno
při	při	k7c6	při
opylování	opylování	k1gNnSc6	opylování
nepřisedá	přisedat	k5eNaImIp3nS	přisedat
na	na	k7c4	na
bliznu	blizna	k1gFnSc4	blizna
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
není	být	k5eNaImIp3nS	být
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
polinační	polinační	k2eAgFnSc4d1	polinační
kapku	kapka	k1gFnSc4	kapka
vajíčka	vajíčko	k1gNnSc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
klový	klový	k2eAgInSc4d1	klový
otvor	otvor	k1gInSc4	otvor
do	do	k7c2	do
vajíčka	vajíčko	k1gNnSc2	vajíčko
vroste	vrůst	k5eAaPmIp3nS	vrůst
z	z	k7c2	z
pylového	pylový	k2eAgNnSc2d1	pylové
zrna	zrno	k1gNnSc2	zrno
pylová	pylový	k2eAgFnSc1d1	pylová
láčka	láčka	k1gFnSc1	láčka
která	který	k3yIgFnSc1	který
není	být	k5eNaImIp3nS	být
trojbuněčná	trojbuněčný	k2eAgFnSc1d1	trojbuněčný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pětibuněčná	pětibuněčný	k2eAgFnSc1d1	pětibuněčný
(	(	kIx(	(
<g/>
buňka	buňka	k1gFnSc1	buňka
vegetativní	vegetativní	k2eAgFnSc1d1	vegetativní
<g/>
,	,	kIx,	,
2	[number]	k4	2
spermatické	spermatický	k2eAgInPc1d1	spermatický
<g/>
,	,	kIx,	,
proklová	proklovat	k5eAaPmIp3nS	proklovat
a	a	k8xC	a
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
pylu	pyl	k1gInSc2	pyl
pro	pro	k7c4	pro
pohlavní	pohlavní	k2eAgNnSc4d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc4	rozmnožování
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
zásadní	zásadní	k2eAgInSc1d1	zásadní
<g/>
.	.	kIx.	.
</s>
<s>
Pyl	pyl	k1gInSc1	pyl
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
využitím	využití	k1gNnSc7	využití
vrozených	vrozený	k2eAgInPc2d1	vrozený
instinktů	instinkt	k1gInPc2	instinkt
včel	včela	k1gFnPc2	včela
zdrojem	zdroj	k1gInSc7	zdroj
medu	med	k1gInSc2	med
<g/>
,	,	kIx,	,
propolisu	propolis	k1gInSc2	propolis
<g/>
,	,	kIx,	,
mateří	mateří	k2eAgFnSc2d1	mateří
kašičky	kašička	k1gFnSc2	kašička
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
lidskou	lidský	k2eAgFnSc4d1	lidská
výživu	výživa	k1gFnSc4	výživa
<g/>
,	,	kIx,	,
medicínu	medicína	k1gFnSc4	medicína
i	i	k8xC	i
kosmetiku	kosmetika	k1gFnSc4	kosmetika
potřebných	potřebný	k2eAgInPc2d1	potřebný
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
pyl	pyl	k1gInSc4	pyl
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgMnPc4d1	mnohý
nepříjemným	příjemný	k2eNgInSc7d1	nepříjemný
alergenem	alergen	k1gInSc7	alergen
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
značně	značně	k6eAd1	značně
znepříjemnit	znepříjemnit	k5eAaPmF	znepříjemnit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
množství	množství	k1gNnSc6	množství
pylu	pyl	k1gInSc2	pyl
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
svědčí	svědčit	k5eAaImIp3nS	svědčit
příklad	příklad	k1gInSc1	příklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
bříza	bříza	k1gFnSc1	bříza
vyprodukuje	vyprodukovat	k5eAaPmIp3nS	vyprodukovat
až	až	k9	až
5,5	[number]	k4	5,5
milionu	milion	k4xCgInSc2	milion
pylových	pylový	k2eAgNnPc2d1	pylové
zrn	zrno	k1gNnPc2	zrno
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
jehnědě	jehněda	k1gFnSc6	jehněda
<g/>
.	.	kIx.	.
</s>
<s>
Znát	znát	k5eAaImF	znát
typ	typ	k1gInSc4	typ
pylových	pylový	k2eAgNnPc2d1	pylové
zrn	zrno	k1gNnPc2	zrno
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
určování	určování	k1gNnSc4	určování
taxonů	taxon	k1gInPc2	taxon
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
archeologií	archeologie	k1gFnSc7	archeologie
nebo	nebo	k8xC	nebo
alergologií	alergologie	k1gFnSc7	alergologie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
zkoumání	zkoumání	k1gNnSc4	zkoumání
pravděpodobného	pravděpodobný	k2eAgInSc2d1	pravděpodobný
historického	historický	k2eAgInSc2d1	historický
vývoje	vývoj	k1gInSc2	vývoj
daného	daný	k2eAgInSc2d1	daný
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pylových	pylový	k2eAgNnPc6d1	pylové
zrnech	zrno	k1gNnPc6	zrno
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
tvary	tvar	k1gInPc1	tvar
a	a	k8xC	a
pak	pak	k6eAd1	pak
umístění	umístění	k1gNnSc4	umístění
a	a	k8xC	a
počty	počet	k1gInPc4	počet
apertur	apertura	k1gFnPc2	apertura
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
směru	směr	k1gInSc6	směr
ze	z	k7c2	z
středu	střed	k1gInSc2	střed
tetrády	tetráda	k1gFnSc2	tetráda
rozlišíme	rozlišit	k5eAaPmIp1nP	rozlišit
na	na	k7c6	na
pylovém	pylový	k2eAgNnSc6d1	pylové
zrnu	zrno	k1gNnSc6	zrno
dva	dva	k4xCgInPc4	dva
póly	pól	k1gInPc4	pól
<g/>
:	:	kIx,	:
proximální	proximální	k2eAgFnPc4d1	proximální
–	–	k?	–
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
centru	centrum	k1gNnSc3	centrum
tetrády	tetráda	k1gFnSc2	tetráda
<g/>
,	,	kIx,	,
distální	distální	k2eAgFnSc2d1	distální
–	–	k?	–
směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
z	z	k7c2	z
tetrády	tetráda	k1gFnSc2	tetráda
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc4	dva
póly	pól	k1gInPc4	pól
spojuje	spojovat	k5eAaImIp3nS	spojovat
pólová	pólový	k2eAgFnSc1d1	pólová
osa	osa	k1gFnSc1	osa
a	a	k8xC	a
kolmo	kolmo	k6eAd1	kolmo
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
leží	ležet	k5eAaImIp3nS	ležet
ekvatoriální	ekvatoriální	k2eAgFnSc1d1	ekvatoriální
rovina	rovina	k1gFnSc1	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Apertury	apertura	k1gFnPc1	apertura
jsou	být	k5eAaImIp3nP	být
dvojího	dvojí	k4xRgMnSc2	dvojí
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
kolpus	kolpus	k1gInSc1	kolpus
–	–	k?	–
oválná	oválný	k2eAgFnSc1d1	oválná
až	až	k9	až
úzce	úzko	k6eAd1	úzko
štěrbinatá	štěrbinatý	k2eAgFnSc1d1	štěrbinatý
<g/>
,	,	kIx,	,
vývojově	vývojově	k6eAd1	vývojově
původnější	původní	k2eAgFnSc1d2	původnější
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
kolpus	kolpus	k1gMnSc1	kolpus
v	v	k7c6	v
distální	distální	k2eAgFnSc6d1	distální
části	část	k1gFnSc6	část
zrna	zrno	k1gNnSc2	zrno
bývá	bývat	k5eAaImIp3nS	bývat
nazýván	nazývat	k5eAaImNgInS	nazývat
sulkus	sulkus	k1gInSc1	sulkus
<g/>
,	,	kIx,	,
porus	porus	k1gInSc1	porus
–	–	k?	–
okrouhlá	okrouhlý	k2eAgFnSc1d1	okrouhlá
až	až	k9	až
mírně	mírně	k6eAd1	mírně
oválná	oválný	k2eAgFnSc1d1	oválná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
porus	porus	k1gMnSc1	porus
v	v	k7c6	v
distální	distální	k2eAgFnSc6d1	distální
části	část	k1gFnSc6	část
zrna	zrno	k1gNnSc2	zrno
bývá	bývat	k5eAaImIp3nS	bývat
nazýván	nazývat	k5eAaImNgInS	nazývat
ulkus	ulkus	k1gInSc1	ulkus
<g/>
.	.	kIx.	.
</s>
<s>
Umístění	umístění	k1gNnSc1	umístění
apertury	apertura	k1gFnSc2	apertura
<g/>
:	:	kIx,	:
panto	panto	k1gNnSc1	panto
–	–	k?	–
apertury	apertura	k1gFnSc2	apertura
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
zono	zono	k6eAd1	zono
–	–	k?	–
apertury	apertura	k1gFnSc2	apertura
v	v	k7c6	v
ekvatoriální	ekvatoriální	k2eAgFnSc6d1	ekvatoriální
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Apertura	apertura	k1gFnSc1	apertura
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
nebo	nebo	k8xC	nebo
i	i	k9	i
jich	on	k3xPp3gMnPc2	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
na	na	k7c6	na
pylovém	pylový	k2eAgNnSc6d1	pylové
zrnu	zrno	k1gNnSc6	zrno
více	hodně	k6eAd2	hodně
<g/>
:	:	kIx,	:
mono	mono	k6eAd1	mono
(	(	kIx(	(
<g/>
uni	uni	k?	uni
<g/>
)	)	kIx)	)
až	až	k9	až
...	...	k?	...
<g/>
poly	pola	k1gFnSc2	pola
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgInPc2d1	uvedený
počtů	počet	k1gInPc2	počet
<g/>
,	,	kIx,	,
umístění	umístění	k1gNnSc2	umístění
a	a	k8xC	a
typů	typ	k1gInPc2	typ
apertur	apertura	k1gFnPc2	apertura
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenován	k2eAgInPc1d1	pojmenován
pyly	pyl	k1gInPc1	pyl
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
trizonoporátní	trizonoporátní	k2eAgNnPc1d1	trizonoporátní
<g/>
,	,	kIx,	,
trizonokolpátní	trizonokolpátní	k2eAgNnPc1d1	trizonokolpátní
<g/>
,	,	kIx,	,
pentazonoporátní	pentazonoporátní	k2eAgNnPc1d1	pentazonoporátní
<g/>
,	,	kIx,	,
hexapantokolpátní	hexapantokolpátní	k2eAgNnPc1d1	hexapantokolpátní
<g/>
,	,	kIx,	,
polypantoporátní	polypantoporátní	k2eAgNnPc1d1	polypantoporátní
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
vyšších	vysoký	k2eAgFnPc2d2	vyšší
dvouděložných	dvouděložný	k2eAgFnPc2d1	dvouděložná
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
pyl	pyl	k1gInSc1	pyl
trikolpátní	trikolpátní	k2eAgInSc1d1	trikolpátní
<g/>
.	.	kIx.	.
</s>
