<p>
<s>
Normané	Norman	k1gMnPc1	Norman
jsou	být	k5eAaImIp3nP	být
potomci	potomek	k1gMnPc1	potomek
části	část	k1gFnSc2	část
Vikingů	Viking	k1gMnPc2	Viking
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
obsadili	obsadit	k5eAaPmAgMnP	obsadit
severní	severní	k2eAgFnSc3d1	severní
Francii	Francie	k1gFnSc3	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
911	[number]	k4	911
západofranský	západofranský	k2eAgMnSc1d1	západofranský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Prosťáček	prosťáček	k1gMnSc1	prosťáček
udělil	udělit	k5eAaPmAgMnS	udělit
jejich	jejich	k3xOp3gMnSc3	jejich
náčelníkovi	náčelník	k1gMnSc3	náčelník
Rollovi	Roll	k1gMnSc3	Roll
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
hrabství	hrabství	k1gNnSc2	hrabství
Rouen	Rouna	k1gFnPc2	Rouna
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Normandské	normandský	k2eAgNnSc4d1	normandské
vévodství	vévodství	k1gNnSc4	vévodství
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
pokusil	pokusit	k5eAaPmAgInS	pokusit
řešit	řešit	k5eAaImF	řešit
problém	problém	k1gInSc4	problém
rozpínavosti	rozpínavost	k1gFnSc2	rozpínavost
a	a	k8xC	a
dravosti	dravost	k1gFnSc2	dravost
Normanů	Norman	k1gMnPc2	Norman
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
mu	on	k3xPp3gMnSc3	on
přísahali	přísahat	k5eAaImAgMnP	přísahat
věrnost	věrnost	k1gFnSc4	věrnost
<g/>
,	,	kIx,	,
přijali	přijmout	k5eAaPmAgMnP	přijmout
křesťanství	křesťanství	k1gNnSc2	křesťanství
a	a	k8xC	a
také	také	k9	také
galorománský	galorománský	k2eAgInSc4d1	galorománský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc4d1	další
výboje	výboj	k1gInPc4	výboj
směřovali	směřovat	k5eAaImAgMnP	směřovat
Normané	Norman	k1gMnPc1	Norman
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
roku	rok	k1gInSc2	rok
1066	[number]	k4	1066
v	v	k7c6	v
Bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Hastingsu	Hastings	k1gInSc2	Hastings
podmanil	podmanit	k5eAaPmAgMnS	podmanit
normanský	normanský	k2eAgMnSc1d1	normanský
vévoda	vévoda	k1gMnSc1	vévoda
Vilém	Vilém	k1gMnSc1	Vilém
Dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
anglickým	anglický	k2eAgMnSc7d1	anglický
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1072	[number]	k4	1072
napadl	napadnout	k5eAaPmAgMnS	napadnout
Vilém	Vilém	k1gMnSc1	Vilém
Dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
Skotsko	Skotsko	k1gNnSc4	Skotsko
a	a	k8xC	a
král	král	k1gMnSc1	král
Malcolm	Malcolm	k1gMnSc1	Malcolm
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
podrobit	podrobit	k5eAaPmF	podrobit
<g/>
.	.	kIx.	.
</s>
<s>
Normané	Norman	k1gMnPc1	Norman
též	též	k9	též
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
a	a	k8xC	a
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Normané	Norman	k1gMnPc1	Norman
výrazně	výrazně	k6eAd1	výrazně
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
také	také	k9	také
dějiny	dějiny	k1gFnPc4	dějiny
Středozemí	středozemí	k1gNnSc2	středozemí
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
se	se	k3xPyFc4	se
řady	řada	k1gFnSc2	řada
bojů	boj	k1gInPc2	boj
s	s	k7c7	s
Araby	Arab	k1gMnPc7	Arab
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
statečnost	statečnost	k1gFnSc4	statečnost
hojně	hojně	k6eAd1	hojně
odměňováni	odměňovat	k5eAaImNgMnP	odměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Rainulf	Rainulf	k1gMnSc1	Rainulf
Drengot	Drengot	k1gMnSc1	Drengot
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
roku	rok	k1gInSc2	rok
1030	[number]	k4	1030
od	od	k7c2	od
Sergia	Sergius	k1gMnSc2	Sergius
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Neapolského	neapolský	k2eAgNnSc2d1	Neapolské
hrabství	hrabství	k1gNnSc2	hrabství
Aversa	Aversa	k1gFnSc1	Aversa
a	a	k8xC	a
příslušník	příslušník	k1gMnSc1	příslušník
rodiny	rodina	k1gFnSc2	rodina
Hauteville	Hauteville	k1gFnSc1	Hauteville
Guaimar	Guaimar	k1gMnSc1	Guaimar
IV	IV	kA	IV
<g/>
.	.	kIx.	.
ze	z	k7c2	z
Salerna	Salerna	k1gFnSc1	Salerna
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgInS	získat
dokonce	dokonce	k9	dokonce
Apulii	Apulie	k1gFnSc4	Apulie
a	a	k8xC	a
Kalábrii	Kalábrie	k1gFnSc4	Kalábrie
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Apulie	Apulie	k1gFnSc2	Apulie
a	a	k8xC	a
Calabrie	Calabrie	k1gFnSc2	Calabrie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Guaimar	Guaimar	k1gInSc1	Guaimar
okamžitě	okamžitě	k6eAd1	okamžitě
odměnil	odměnit	k5eAaPmAgInS	odměnit
svého	svůj	k3xOyFgMnSc2	svůj
prvního	první	k4xOgMnSc2	první
velitele	velitel	k1gMnSc2	velitel
Viléma	Vilém	k1gMnSc2	Vilém
zvaného	zvaný	k2eAgInSc2d1	zvaný
Železná	železný	k2eAgFnSc1d1	železná
paže	paže	k1gFnSc1	paže
knížectvím	knížectví	k1gNnSc7	knížectví
Melfi	Melfi	k1gNnSc1	Melfi
<g/>
.	.	kIx.	.
</s>
<s>
Drengotův	Drengotův	k2eAgMnSc1d1	Drengotův
syn	syn	k1gMnSc1	syn
pak	pak	k6eAd1	pak
získal	získat	k5eAaPmAgMnS	získat
ještě	ještě	k9	ještě
knížectví	knížectví	k1gNnSc4	knížectví
Capuu	Capuus	k1gInSc2	Capuus
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
územní	územní	k2eAgInPc1d1	územní
zisky	zisk	k1gInPc1	zisk
byly	být	k5eAaImAgInP	být
završeny	završit	k5eAaPmNgInP	završit
dobytím	dobytí	k1gNnSc7	dobytí
Sicílie	Sicílie	k1gFnSc2	Sicílie
a	a	k8xC	a
Malty	Malta	k1gFnSc2	Malta
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
Arabů	Arab	k1gMnPc2	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1130	[number]	k4	1130
pak	pak	k6eAd1	pak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Sicilské	sicilský	k2eAgNnSc1d1	sicilské
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristika	k1gFnSc1	charakteristika
Normanů	Norman	k1gMnPc2	Norman
==	==	k?	==
</s>
</p>
<p>
<s>
Normané	Norman	k1gMnPc1	Norman
by	by	kYmCp3nP	by
neměli	mít	k5eNaImAgMnP	mít
být	být	k5eAaImF	být
zaměňováni	zaměňovat	k5eAaImNgMnP	zaměňovat
s	s	k7c7	s
Vikingy	Viking	k1gMnPc7	Viking
–	–	k?	–
skandinávskými	skandinávský	k2eAgMnPc7d1	skandinávský
nájezdníky	nájezdník	k1gMnPc7	nájezdník
<g/>
,	,	kIx,	,
pleniteli	plenitel	k1gMnPc7	plenitel
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
ruských	ruský	k2eAgNnPc2d1	ruské
knížectví	knížectví	k1gNnPc2	knížectví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Geoffrey	Geoffre	k1gMnPc7	Geoffre
Malaterra	Malaterr	k1gInSc2	Malaterr
Normany	Norman	k1gMnPc7	Norman
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
jako	jako	k9	jako
vyznačující	vyznačující	k2eAgFnSc1d1	vyznačující
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
prohnaností	prohnanost	k1gFnSc7	prohnanost
<g/>
,	,	kIx,	,
pohrdající	pohrdající	k2eAgInPc4d1	pohrdající
odkazy	odkaz	k1gInPc4	odkaz
svých	svůj	k3xOyFgMnPc2	svůj
předků	předek	k1gMnPc2	předek
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
na	na	k7c4	na
další	další	k2eAgNnPc4d1	další
veliká	veliký	k2eAgNnPc4d1	veliké
vítězství	vítězství	k1gNnPc4	vítězství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Normané	Norman	k1gMnPc1	Norman
a	a	k8xC	a
Normandie	Normandie	k1gFnPc1	Normandie
==	==	k?	==
</s>
</p>
<p>
<s>
Zeměpisně	zeměpisně	k6eAd1	zeměpisně
se	se	k3xPyFc4	se
Normandie	Normandie	k1gFnSc1	Normandie
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
týchž	týž	k3xTgNnPc6	týž
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
stará	starat	k5eAaImIp3nS	starat
církevní	církevní	k2eAgFnPc4d1	církevní
provincie	provincie	k1gFnPc4	provincie
Rouen	Rouna	k1gFnPc2	Rouna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
Brittanica	Brittanic	k2eAgFnSc1d1	Brittanic
Nova	nova	k1gFnSc1	nova
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
západní	západní	k2eAgInPc4d1	západní
Flandry	Flandry	k1gInPc4	Flandry
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
nemělo	mít	k5eNaImAgNnS	mít
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
předtím	předtím	k6eAd1	předtím
tvořilo	tvořit	k5eAaImAgNnS	tvořit
administrativní	administrativní	k2eAgInSc4d1	administrativní
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
bylo	být	k5eAaImAgNnS	být
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
převážně	převážně	k6eAd1	převážně
francouzské	francouzský	k2eAgNnSc1d1	francouzské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
také	také	k9	také
natrvalo	natrvalo	k6eAd1	natrvalo
usadili	usadit	k5eAaPmAgMnP	usadit
vikinští	vikinský	k2eAgMnPc1d1	vikinský
osadníci	osadník	k1gMnPc1	osadník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
oblast	oblast	k1gFnSc4	oblast
osidlovat	osidlovat	k5eAaImF	osidlovat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
880	[number]	k4	880
a	a	k8xC	a
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
mezi	mezi	k7c4	mezi
malou	malý	k2eAgFnSc4d1	malá
kolonii	kolonie	k1gFnSc4	kolonie
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Normandii	Normandie	k1gFnSc6	Normandie
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgFnSc1d2	veliký
usedlost	usedlost	k1gFnSc1	usedlost
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
množit	množit	k5eAaImF	množit
případy	případ	k1gInPc4	případ
zkázonosných	zkázonosný	k2eAgInPc2d1	zkázonosný
nájezdů	nájezd	k1gInPc2	nájezd
Vikingů	Viking	k1gMnPc2	Viking
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
pronikajících	pronikající	k2eAgInPc2d1	pronikající
po	po	k7c6	po
řekách	řeka	k1gFnPc6	řeka
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
Galie	Galie	k1gFnSc2	Galie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
stávat	stávat	k5eAaImF	stávat
jejich	jejich	k3xOp3gNnSc7	jejich
častým	častý	k2eAgNnSc7d1	časté
tábořištěm	tábořiště	k1gNnSc7	tábořiště
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
sebou	se	k3xPyFc7	se
si	se	k3xPyFc3	se
Vikingové	Viking	k1gMnPc1	Viking
brali	brát	k5eAaImAgMnP	brát
i	i	k9	i
své	svůj	k3xOyFgFnPc4	svůj
ženy	žena	k1gFnPc4	žena
i	i	k8xC	i
osobní	osobní	k2eAgInSc4d1	osobní
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
Vikingů	Viking	k1gMnPc2	Viking
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
trvale	trvale	k6eAd1	trvale
usadila	usadit	k5eAaPmAgFnS	usadit
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc7	jejich
pohanské	pohanský	k2eAgInPc4d1	pohanský
zvyky	zvyk	k1gInPc4	zvyk
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
a	a	k8xC	a
Vikingové	Viking	k1gMnPc1	Viking
též	též	k9	též
přijali	přijmout	k5eAaPmAgMnP	přijmout
také	také	k9	také
galorománský	galorománský	k2eAgInSc4d1	galorománský
jazyk	jazyk	k1gInSc4	jazyk
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
či	či	k8xC	či
dvou	dva	k4xCgFnPc6	dva
generacích	generace	k1gFnPc6	generace
se	se	k3xPyFc4	se
již	již	k6eAd1	již
potomci	potomek	k1gMnPc1	potomek
vikinských	vikinský	k2eAgFnPc2d1	vikinská
nájezdníků	nájezdník	k1gMnPc2	nájezdník
ničím	ničí	k3xOyNgFnPc3	ničí
nelišili	lišit	k5eNaImAgMnP	lišit
od	od	k7c2	od
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
francouzského	francouzský	k2eAgNnSc2d1	francouzské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Normané	Norman	k1gMnPc1	Norman
si	se	k3xPyFc3	se
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
osvojili	osvojit	k5eAaPmAgMnP	osvojit
sílící	sílící	k2eAgInSc4d1	sílící
feudální	feudální	k2eAgInSc4d1	feudální
systém	systém	k1gInSc4	systém
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Normané	Norman	k1gMnPc1	Norman
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
také	také	k9	také
Normané	Norman	k1gMnPc1	Norman
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Francii	Francie	k1gFnSc6	Francie
zaujali	zaujmout	k5eAaPmAgMnP	zaujmout
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
společenském	společenský	k2eAgInSc6d1	společenský
žebříčku	žebříček	k1gInSc6	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Normanští	normanský	k2eAgMnPc1d1	normanský
válečníci	válečník	k1gMnPc1	válečník
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
lišili	lišit	k5eAaImAgMnP	lišit
od	od	k7c2	od
místní	místní	k2eAgFnSc2d1	místní
francouzské	francouzský	k2eAgFnSc2d1	francouzská
aristokracie	aristokracie	k1gFnSc2	aristokracie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
leckdy	leckdy	k6eAd1	leckdy
vysledovat	vysledovat	k5eAaImF	vysledovat
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
až	až	k9	až
do	do	k7c2	do
karolinské	karolinský	k2eAgFnSc2d1	Karolinská
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Normané	Norman	k1gMnPc1	Norman
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
dokázali	dokázat	k5eAaPmAgMnP	dokázat
vystopovat	vystopovat	k5eAaPmF	vystopovat
své	svůj	k3xOyFgInPc4	svůj
předky	předek	k1gInPc4	předek
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
před	před	k7c4	před
počátek	počátek	k1gInSc4	počátek
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
rytířů	rytíř	k1gMnPc2	rytíř
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
chudá	chudý	k2eAgFnSc1d1	chudá
a	a	k8xC	a
lačná	lačný	k2eAgFnSc1d1	lačná
po	po	k7c6	po
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Rytířstvo	rytířstvo	k1gNnSc1	rytířstvo
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
před	před	k7c7	před
křížovými	křížový	k2eAgFnPc7d1	křížová
výpravami	výprava	k1gFnPc7	výprava
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
nízké	nízký	k2eAgFnSc3d1	nízká
společenské	společenský	k2eAgFnSc3d1	společenská
třídě	třída	k1gFnSc3	třída
a	a	k8xC	a
rytíř	rytíř	k1gMnSc1	rytíř
býval	bývat	k5eAaImAgMnS	bývat
často	často	k6eAd1	často
jen	jen	k9	jen
profesionální	profesionální	k2eAgMnSc1d1	profesionální
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
jediným	jediný	k2eAgInSc7d1	jediný
jeho	jeho	k3xOp3gInSc7	jeho
majetkem	majetek	k1gInSc7	majetek
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
anglických	anglický	k2eAgMnPc2d1	anglický
a	a	k8xC	a
francouzských	francouzský	k2eAgMnPc2d1	francouzský
Normanů	Norman	k1gMnPc2	Norman
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
horlivě	horlivě	k6eAd1	horlivě
připojilo	připojit	k5eAaPmAgNnS	připojit
k	k	k7c3	k
myšlence	myšlenka	k1gFnSc3	myšlenka
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Normanské	normanský	k2eAgInPc1d1	normanský
výboje	výboj	k1gInPc1	výboj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Normané	Norman	k1gMnPc1	Norman
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
===	===	k?	===
</s>
</p>
<p>
<s>
Normané	Norman	k1gMnPc1	Norman
měli	mít	k5eAaImAgMnP	mít
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
již	již	k6eAd1	již
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
raných	raný	k2eAgFnPc6d1	raná
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Vikingové	Viking	k1gMnPc1	Viking
nejen	nejen	k6eAd1	nejen
pustošili	pustošit	k5eAaImAgMnP	pustošit
anglické	anglický	k2eAgInPc4d1	anglický
břehy	břeh	k1gInPc4	břeh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsadili	obsadit	k5eAaPmAgMnP	obsadit
většinu	většina	k1gFnSc4	většina
důležitých	důležitý	k2eAgInPc2d1	důležitý
přístavů	přístav	k1gInPc2	přístav
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
Lamanšského	lamanšský	k2eAgInSc2d1	lamanšský
kanálu	kanál	k1gInSc2	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vztah	vztah	k1gInSc1	vztah
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
stvrzen	stvrdit	k5eAaPmNgInS	stvrdit
i	i	k9	i
krví	krev	k1gFnSc7	krev
-	-	kIx~	-
svatbou	svatba	k1gFnSc7	svatba
sestry	sestra	k1gFnSc2	sestra
vévody	vévoda	k1gMnSc2	vévoda
Richarda	Richard	k1gMnSc2	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Normandského	normandský	k2eAgMnSc2d1	normandský
s	s	k7c7	s
anglickým	anglický	k2eAgMnSc7d1	anglický
králem	král	k1gMnSc7	král
Ethelredem	Ethelred	k1gMnSc7	Ethelred
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1013	[number]	k4	1013
byl	být	k5eAaImAgInS	být
Ethelred	Ethelred	k1gInSc1	Ethelred
II	II	kA	II
<g/>
.	.	kIx.	.
nucen	nutit	k5eAaImNgMnS	nutit
uprchnout	uprchnout	k5eAaPmF	uprchnout
do	do	k7c2	do
Normandie	Normandie	k1gFnSc2	Normandie
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
švagrovi	švagr	k1gMnSc3	švagr
Richardovi	Richard	k1gMnSc3	Richard
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ho	on	k3xPp3gMnSc4	on
vyhnaly	vyhnat	k5eAaPmAgFnP	vyhnat
vikinské	vikinský	k2eAgFnPc4d1	vikinská
výpravy	výprava	k1gFnPc4	výprava
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
vedl	vést	k5eAaImAgMnS	vést
dánský	dánský	k2eAgMnSc1d1	dánský
král	král	k1gMnSc1	král
Sweyn	Sweyn	k1gMnSc1	Sweyn
Forkbeard	Forkbeard	k1gMnSc1	Forkbeard
a	a	k8xC	a
Ethelred	Ethelred	k1gMnSc1	Ethelred
zůstal	zůstat	k5eAaPmAgMnS	zůstat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Vyznavač	vyznavač	k1gMnSc1	vyznavač
roku	rok	k1gInSc2	rok
1041	[number]	k4	1041
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
otcova	otcův	k2eAgInSc2d1	otcův
exilu	exil	k1gInSc2	exil
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
svého	svůj	k3xOyFgMnSc2	svůj
nevlastního	vlastní	k2eNgMnSc2d1	nevlastní
bratra	bratr	k1gMnSc2	bratr
Hardiknuta	Hardiknut	k2eAgMnSc4d1	Hardiknut
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
přinesl	přinést	k5eAaPmAgInS	přinést
s	s	k7c7	s
sebou	se	k3xPyFc7	se
také	také	k6eAd1	také
vysokou	vysoký	k2eAgFnSc4d1	vysoká
normanskou	normanský	k2eAgFnSc4d1	normanská
vzdělanost	vzdělanost	k1gFnSc4	vzdělanost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Eduardem	Eduard	k1gMnSc7	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Vyznavačem	vyznavač	k1gMnSc7	vyznavač
přišlo	přijít	k5eAaPmAgNnS	přijít
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
normanských	normanský	k2eAgMnPc2d1	normanský
úředníků	úředník	k1gMnPc2	úředník
a	a	k8xC	a
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
najal	najmout	k5eAaPmAgMnS	najmout
menší	malý	k2eAgInSc4d2	menší
počet	počet	k1gInSc4	počet
Normanů	Norman	k1gMnPc2	Norman
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
postavit	postavit	k5eAaPmF	postavit
anglické	anglický	k2eAgNnSc1d1	anglické
jezdectvo	jezdectvo	k1gNnSc1	jezdectvo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
koncepce	koncepce	k1gFnSc1	koncepce
nikdy	nikdy	k6eAd1	nikdy
úplně	úplně	k6eAd1	úplně
nezakořenila	zakořenit	k5eNaPmAgFnS	zakořenit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
kroků	krok	k1gInPc2	krok
Eduardovy	Eduardův	k2eAgFnSc2d1	Eduardova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Roberta	Robert	k1gMnSc2	Robert
z	z	k7c2	z
Jumiè	Jumiè	k1gFnSc2	Jumiè
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
v	v	k7c4	v
Canterbury	Canterbur	k1gInPc4	Canterbur
a	a	k8xC	a
Ralfa	Ralf	k1gMnSc4	Ralf
Bázlivého	bázlivý	k2eAgInSc2d1	bázlivý
hrabětem	hrabě	k1gMnSc7	hrabě
z	z	k7c2	z
Herefordu	Hereford	k1gInSc2	Hereford
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
pozval	pozvat	k5eAaPmAgMnS	pozvat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1051	[number]	k4	1051
svého	svůj	k3xOyFgMnSc2	svůj
švagra	švagr	k1gMnSc2	švagr
Eustacha	Eustach	k1gMnSc2	Eustach
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Boulogne	Boulogn	k1gInSc5	Boulogn
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
dvůr	dvůr	k1gInSc4	dvůr
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
dosud	dosud	k6eAd1	dosud
největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
raných	raný	k2eAgInPc2d1	raný
konfliktů	konflikt	k1gInPc2	konflikt
mezi	mezi	k7c7	mezi
Sasy	Sas	k1gMnPc7	Sas
a	a	k8xC	a
Normany	Norman	k1gMnPc7	Norman
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
skončil	skončit	k5eAaPmAgMnS	skončit
exilem	exil	k1gInSc7	exil
hraběte	hrabě	k1gMnSc2	hrabě
Godwina	Godwin	k1gMnSc2	Godwin
z	z	k7c2	z
Wessexu	Wessex	k1gInSc2	Wessex
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1066	[number]	k4	1066
nejslavnější	slavný	k2eAgMnSc1d3	nejslavnější
normanský	normanský	k2eAgMnSc1d1	normanský
vévoda	vévoda	k1gMnSc1	vévoda
Vilém	Vilém	k1gMnSc1	Vilém
I.	I.	kA	I.
Dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Přišedší	přišedší	k2eAgMnPc1d1	přišedší
Normané	Norman	k1gMnPc1	Norman
pak	pak	k6eAd1	pak
nahradili	nahradit	k5eAaPmAgMnP	nahradit
Anglosasy	Anglosas	k1gMnPc4	Anglosas
jako	jako	k8xS	jako
vládnoucí	vládnoucí	k2eAgFnSc4d1	vládnoucí
vrstvu	vrstva	k1gFnSc4	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odeznění	odeznění	k1gNnSc6	odeznění
počáteční	počáteční	k2eAgFnSc2d1	počáteční
vlny	vlna	k1gFnSc2	vlna
odporu	odpor	k1gInSc2	odpor
vůči	vůči	k7c3	vůči
Normanům	Norman	k1gMnPc3	Norman
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
populace	populace	k1gFnPc1	populace
začaly	začít	k5eAaPmAgFnP	začít
vzájemně	vzájemně	k6eAd1	vzájemně
mísit	mísit	k5eAaImF	mísit
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
kombinovat	kombinovat	k5eAaImF	kombinovat
své	svůj	k3xOyFgInPc4	svůj
zvyky	zvyk	k1gInPc4	zvyk
a	a	k8xC	a
tradice	tradice	k1gFnPc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Normané	Norman	k1gMnPc1	Norman
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
ztotožnili	ztotožnit	k5eAaPmAgMnP	ztotožnit
jako	jako	k8xC	jako
Anglo-Normané	Anglo-Normaný	k2eAgInPc1d1	Anglo-Normaný
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
rozdíly	rozdíl	k1gInPc1	rozdíl
definitivně	definitivně	k6eAd1	definitivně
vymizely	vymizet	k5eAaPmAgInP	vymizet
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Anglo-Normanská	Anglo-Normanský	k2eAgFnSc1d1	Anglo-Normanský
aristokracie	aristokracie	k1gFnSc1	aristokracie
identifikovala	identifikovat	k5eAaBmAgFnS	identifikovat
jako	jako	k9	jako
anglická	anglický	k2eAgFnSc1d1	anglická
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
Anglo-Normanů	Anglo-Norman	k1gMnPc2	Anglo-Norman
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Jazyky	jazyk	k1gInPc1	jazyk
Anglosasů	Anglosas	k1gMnPc2	Anglosas
a	a	k8xC	a
Anglo-Normanů	Anglo-Norman	k1gMnPc2	Anglo-Norman
se	se	k3xPyFc4	se
promíchaly	promíchat	k5eAaPmAgInP	promíchat
a	a	k8xC	a
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
středověkou	středověký	k2eAgFnSc4d1	středověká
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
anglickou	anglický	k2eAgFnSc7d1	anglická
korunou	koruna	k1gFnSc7	koruna
ztraceno	ztracen	k2eAgNnSc1d1	ztraceno
Normandské	normandský	k2eAgNnSc1d1	normandské
vévodství	vévodství	k1gNnSc1	vévodství
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
ostrovy	ostrov	k1gInPc1	ostrov
v	v	k7c6	v
kanálu	kanál	k1gInSc6	kanál
La	la	k1gNnSc2	la
Manche	Manch	k1gFnSc2	Manch
Anglie	Anglie	k1gFnSc2	Anglie
udržela	udržet	k5eAaPmAgFnS	udržet
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
dynastie	dynastie	k1gFnSc1	dynastie
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
dlouho	dlouho	k6eAd1	dlouho
udržováno	udržován	k2eAgNnSc1d1	udržováno
<g/>
.	.	kIx.	.
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
šlechta	šlechta	k1gFnSc1	šlechta
byla	být	k5eAaImAgFnS	být
částí	část	k1gFnSc7	část
samostatné	samostatný	k2eAgFnSc2d1	samostatná
francouzsky	francouzsky	k6eAd1	francouzsky
mluvící	mluvící	k2eAgFnSc2d1	mluvící
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
měli	mít	k5eAaImAgMnP	mít
svá	svůj	k3xOyFgNnPc4	svůj
území	území	k1gNnPc4	území
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
straně	strana	k1gFnSc6	strana
kanálu	kanál	k1gInSc2	kanál
La	la	k1gNnSc2	la
Manche	Manch	k1gFnSc2	Manch
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
byli	být	k5eAaImAgMnP	být
leníky	leník	k1gMnPc4	leník
králů	král	k1gMnPc2	král
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Angličtí	anglický	k2eAgMnPc1d1	anglický
králové	král	k1gMnPc1	král
ovládali	ovládat	k5eAaImAgMnP	ovládat
části	část	k1gFnPc4	část
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
francouzského	francouzský	k2eAgNnSc2d1	francouzské
území	území	k1gNnSc2	území
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
středověcí	středověký	k2eAgMnPc1d1	středověký
angličtí	anglický	k2eAgMnPc1d1	anglický
králové	král	k1gMnPc1	král
nezapomínali	zapomínat	k5eNaImAgMnP	zapomínat
na	na	k7c6	na
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
Normandie	Normandie	k1gFnSc2	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Richard	Richard	k1gMnSc1	Richard
I.	I.	kA	I.
Lví	lví	k2eAgNnSc1d1	lví
srdce	srdce	k1gNnSc1	srdce
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
ztělesnění	ztělesnění	k1gNnSc4	ztělesnění
středověkého	středověký	k2eAgMnSc2d1	středověký
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
trávil	trávit	k5eAaImAgMnS	trávit
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
v	v	k7c6	v
Akvitánii	Akvitánie	k1gFnSc6	Akvitánie
nebo	nebo	k8xC	nebo
na	na	k7c6	na
křížové	křížový	k2eAgFnSc6d1	křížová
výpravě	výprava	k1gFnSc6	výprava
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Lví	lví	k2eAgNnSc4d1	lví
srdce	srdce	k1gNnSc4	srdce
ani	ani	k8xC	ani
nebyl	být	k5eNaImAgMnS	být
vychován	vychovat	k5eAaPmNgMnS	vychovat
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
žádný	žádný	k3yNgMnSc1	žádný
jiný	jiný	k2eAgMnSc1d1	jiný
král	král	k1gMnSc1	král
až	až	k9	až
do	do	k7c2	do
Richarda	Richard	k1gMnSc2	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
nebyl	být	k5eNaImAgMnS	být
anglickým	anglický	k2eAgMnSc7d1	anglický
rodilým	rodilý	k2eAgMnSc7d1	rodilý
mluvčím	mluvčí	k1gMnSc7	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
středověkých	středověký	k2eAgMnPc2d1	středověký
anglických	anglický	k2eAgMnPc2d1	anglický
králů	král	k1gMnPc2	král
si	se	k3xPyFc3	se
dělala	dělat	k5eAaImAgFnS	dělat
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Normané	Norman	k1gMnPc1	Norman
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
===	===	k?	===
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
ucházečů	ucházeč	k1gInPc2	ucházeč
o	o	k7c4	o
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
rival	rival	k1gMnSc1	rival
Viléma	Vilém	k1gMnSc2	Vilém
I.	I.	kA	I.
Dobyvatele	dobyvatel	k1gMnSc2	dobyvatel
<g/>
,	,	kIx,	,
Edgar	Edgar	k1gMnSc1	Edgar
Atheling	Atheling	k1gInSc1	Atheling
když	když	k8xS	když
anglickou	anglický	k2eAgFnSc4d1	anglická
korunu	koruna	k1gFnSc4	koruna
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
,	,	kIx,	,
odplul	odplout	k5eAaPmAgMnS	odplout
nakonec	nakonec	k6eAd1	nakonec
do	do	k7c2	do
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Skotský	skotský	k2eAgMnSc1d1	skotský
král	král	k1gMnSc1	král
Malcolm	Malcolm	k1gMnSc1	Malcolm
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Edgarovou	Edgarův	k2eAgFnSc7d1	Edgarova
sestrou	sestra	k1gFnSc7	sestra
Margaret	Margareta	k1gFnPc2	Margareta
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Vilémovým	Vilémův	k2eAgMnSc7d1	Vilémův
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
už	už	k6eAd1	už
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
Malcolmem	Malcolm	k1gInSc7	Malcolm
přít	přít	k5eAaImF	přít
o	o	k7c4	o
jižní	jižní	k2eAgFnPc4d1	jižní
hranice	hranice	k1gFnPc4	hranice
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1072	[number]	k4	1072
se	se	k3xPyFc4	se
vojsko	vojsko	k1gNnSc1	vojsko
Viléma	Vilém	k1gMnSc2	Vilém
Dobyvatele	dobyvatel	k1gMnSc2	dobyvatel
vylodilo	vylodit	k5eAaPmAgNnS	vylodit
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
a	a	k8xC	a
Vilém	Vilém	k1gMnSc1	Vilém
postupoval	postupovat	k5eAaImAgMnS	postupovat
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
až	až	k9	až
k	k	k7c3	k
Abernethy	Abernetha	k1gMnSc2	Abernetha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
loďstvem	loďstvo	k1gNnSc7	loďstvo
<g/>
.	.	kIx.	.
</s>
<s>
Malcolm	Malcolm	k1gMnSc1	Malcolm
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
Vilémovi	Vilém	k1gMnSc3	Vilém
podrobil	podrobit	k5eAaPmAgMnS	podrobit
<g/>
,	,	kIx,	,
slíbil	slíbit	k5eAaPmAgMnS	slíbit
mu	on	k3xPp3gMnSc3	on
věrnost	věrnost	k1gFnSc4	věrnost
a	a	k8xC	a
vzal	vzít	k5eAaPmAgMnS	vzít
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Duncana	Duncan	k1gMnSc4	Duncan
jako	jako	k9	jako
rukojmí	rukojmí	k1gMnPc4	rukojmí
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
rozpoutal	rozpoutat	k5eAaPmAgMnS	rozpoutat
spory	spor	k1gInPc4	spor
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
je	být	k5eAaImIp3nS	být
skotská	skotský	k2eAgFnSc1d1	skotská
koruna	koruna	k1gFnSc1	koruna
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Normané	Norman	k1gMnPc1	Norman
dále	daleko	k6eAd2	daleko
přicházeli	přicházet	k5eAaImAgMnP	přicházet
do	do	k7c2	do
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
budovali	budovat	k5eAaImAgMnP	budovat
zde	zde	k6eAd1	zde
své	svůj	k3xOyFgInPc4	svůj
hrady	hrad	k1gInPc4	hrad
a	a	k8xC	a
zakládali	zakládat	k5eAaImAgMnP	zakládat
šlechtické	šlechtický	k2eAgInPc4d1	šlechtický
rody	rod	k1gInPc4	rod
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgMnPc2	který
vzešli	vzejít	k5eAaPmAgMnP	vzejít
budoucí	budoucí	k2eAgMnPc1d1	budoucí
králové	král	k1gMnPc1	král
jako	jako	k9	jako
například	například	k6eAd1	například
Robert	Robert	k1gMnSc1	Robert
Bruce	Bruce	k1gMnSc1	Bruce
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
David	David	k1gMnSc1	David
I.	I.	kA	I.
byl	být	k5eAaImAgMnS	být
svolný	svolný	k2eAgMnSc1d1	svolný
k	k	k7c3	k
uvedení	uvedení	k1gNnSc3	uvedení
Normanů	Norman	k1gMnPc2	Norman
a	a	k8xC	a
normanské	normanský	k2eAgFnSc2d1	normanská
kultury	kultura	k1gFnSc2	kultura
do	do	k7c2	do
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
tomuto	tento	k3xDgInSc3	tento
procesu	proces	k1gInSc3	proces
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
Davidovská	Davidovský	k2eAgFnSc1d1	Davidovská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sestra	sestra	k1gFnSc1	sestra
Davida	David	k1gMnSc2	David
I.	I.	kA	I.
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
I.	I.	kA	I.
a	a	k8xC	a
David	David	k1gMnSc1	David
I.	I.	kA	I.
trávil	trávit	k5eAaImAgMnS	trávit
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
svého	svůj	k3xOyFgMnSc2	svůj
švagra	švagr	k1gMnSc2	švagr
hodně	hodně	k6eAd1	hodně
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
jeho	jeho	k3xOp3gFnSc4	jeho
podporu	podpora	k1gFnSc4	podpora
proti	proti	k7c3	proti
svému	svůj	k3xOyFgMnSc3	svůj
nevlastnímu	vlastní	k2eNgMnSc3d1	nevlastní
bratrovi	bratr	k1gMnSc3	bratr
Máelu	Máel	k1gMnSc3	Máel
Coluimovi	Coluim	k1gMnSc3	Coluim
mac	mac	k?	mac
Alaxandairovi	Alaxandair	k1gMnSc3	Alaxandair
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
Jindřicha	Jindřich	k1gMnSc4	Jindřich
podaroval	podarovat	k5eAaPmAgInS	podarovat
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
územím	území	k1gNnSc7	území
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
"	"	kIx"	"
<g/>
Davidovská	Davidovský	k2eAgFnSc1d1	Davidovská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
"	"	kIx"	"
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Davidových	Davidových	k2eAgMnPc2d1	Davidových
nástupců	nástupce	k1gMnPc2	nástupce
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejrozsáhleji	rozsáhle	k6eAd3	rozsáhle
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Viléma	Viléma	k1gFnSc1	Viléma
I.	I.	kA	I.
Normanská	normanský	k2eAgFnSc1d1	normanská
forma	forma	k1gFnSc1	forma
feudalismu	feudalismus	k1gInSc2	feudalismus
se	se	k3xPyFc4	se
místně	místně	k6eAd1	místně
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
či	či	k8xC	či
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
ujala	ujmout	k5eAaPmAgFnS	ujmout
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Normané	Norman	k1gMnPc1	Norman
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
===	===	k?	===
</s>
</p>
<p>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
si	se	k3xPyFc3	se
Normané	Norman	k1gMnPc1	Norman
podrobili	podrobit	k5eAaPmAgMnP	podrobit
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
navázali	navázat	k5eAaPmAgMnP	navázat
kontakty	kontakt	k1gInPc4	kontakt
také	také	k9	také
s	s	k7c7	s
Walesem	Wales	k1gInSc7	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Vyznavač	vyznavač	k1gMnSc1	vyznavač
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
výše	vysoce	k6eAd2	vysoce
jmenovaného	jmenovaný	k2eAgMnSc4d1	jmenovaný
Ralfa	Ralf	k1gMnSc4	Ralf
Bázlivého	bázlivý	k2eAgInSc2d1	bázlivý
hrabětem	hrabě	k1gMnSc7	hrabě
z	z	k7c2	z
Herefordu	Hereford	k1gInSc2	Hereford
a	a	k8xC	a
pověřil	pověřit	k5eAaPmAgMnS	pověřit
ho	on	k3xPp3gMnSc4	on
bojem	boj	k1gInSc7	boj
s	s	k7c7	s
Velšany	Velšan	k1gMnPc7	Velšan
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
tím	ten	k3xDgNnSc7	ten
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
veškerá	veškerý	k3xTgFnSc1	veškerý
normanská	normanský	k2eAgFnSc1d1	normanská
snaha	snaha	k1gFnSc1	snaha
na	na	k7c4	na
dobytí	dobytí	k1gNnSc4	dobytí
Walesu	Wales	k1gInSc2	Wales
nijak	nijak	k6eAd1	nijak
nepostoupila	postoupit	k5eNaPmAgFnS	postoupit
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k8xS	takže
se	se	k3xPyFc4	se
Normané	Norman	k1gMnPc1	Norman
pustili	pustit	k5eAaPmAgMnP	pustit
do	do	k7c2	do
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
a	a	k8xC	a
pomalého	pomalý	k2eAgNnSc2d1	pomalé
dobývání	dobývání	k1gNnSc2	dobývání
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yRgInSc2	který
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc1d1	celý
Wales	Wales	k1gInSc1	Wales
stal	stát	k5eAaPmAgInS	stát
předmětem	předmět	k1gInSc7	předmět
Normanských	normanský	k2eAgInPc2d1	normanský
zásahů	zásah	k1gInPc2	zásah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Jihoitalští	jihoitalský	k2eAgMnPc1d1	jihoitalský
Normané	Norman	k1gMnPc1	Norman
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
národy	národ	k1gInPc7	národ
a	a	k8xC	a
kulturami	kultura	k1gFnPc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
současnost	současnost	k1gFnSc1	současnost
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
,	,	kIx,	,
s.	s.	k?	s.
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
418	[number]	k4	418
<g/>
-	-	kIx~	-
<g/>
5129	[number]	k4	5129
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Normané	Norman	k1gMnPc1	Norman
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Norman	Norman	k1gMnSc1	Norman
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
