<p>
<s>
Malba	malba	k1gFnSc1	malba
je	být	k5eAaImIp3nS	být
technika	technika	k1gFnSc1	technika
nanášení	nanášení	k1gNnSc2	nanášení
barev	barva	k1gFnPc2	barva
v	v	k7c6	v
souvislé	souvislý	k2eAgFnSc6d1	souvislá
vrstvě	vrstva	k1gFnSc6	vrstva
na	na	k7c4	na
podklad	podklad	k1gInSc4	podklad
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
pomocí	pomocí	k7c2	pomocí
štětce	štětec	k1gInSc2	štětec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
znamená	znamenat	k5eAaImIp3nS	znamenat
také	také	k9	také
produkt	produkt	k1gInSc4	produkt
této	tento	k3xDgFnSc2	tento
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
kresby	kresba	k1gFnSc2	kresba
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
použitým	použitý	k2eAgInSc7d1	použitý
nástrojem	nástroj	k1gInSc7	nástroj
(	(	kIx(	(
<g/>
štětec	štětec	k1gInSc1	štětec
<g/>
)	)	kIx)	)
a	a	k8xC	a
plošností	plošnost	k1gFnSc7	plošnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
nanášena	nanášen	k2eAgFnSc1d1	nanášena
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neuměleckém	umělecký	k2eNgInSc6d1	neumělecký
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
malba	malba	k1gFnSc1	malba
synonymem	synonymum	k1gNnSc7	synonymum
pro	pro	k7c4	pro
nátěr	nátěr	k1gInSc4	nátěr
nebo	nebo	k8xC	nebo
natírání	natírání	k1gNnSc4	natírání
<g/>
.	.	kIx.	.
</s>
<s>
Osoba	osoba	k1gFnSc1	osoba
tvořící	tvořící	k2eAgFnSc2d1	tvořící
malby	malba	k1gFnSc2	malba
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
malíř	malíř	k1gMnSc1	malíř
či	či	k8xC	či
malířka	malířka	k1gFnSc1	malířka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základ	základ	k1gInSc1	základ
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
malba	malba	k1gFnSc1	malba
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
malovat	malovat	k5eAaImF	malovat
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
malen	malena	k1gFnPc2	malena
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
starohornoněmeckého	starohornoněmecký	k2eAgInSc2d1	starohornoněmecký
mal	málit	k5eAaImRp2nS	málit
=	=	kIx~	=
"	"	kIx"	"
<g/>
skvrna	skvrna	k1gFnSc1	skvrna
<g/>
,	,	kIx,	,
znamení	znamení	k1gNnSc1	znamení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
původ	původ	k1gInSc1	původ
je	být	k5eAaImIp3nS	být
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
indoevropským	indoevropský	k2eAgInSc7d1	indoevropský
kmenem	kmen	k1gInSc7	kmen
mel-	mel-	k?	mel-
znamenajícím	znamenající	k2eAgInSc6d1	znamenající
"	"	kIx"	"
<g/>
tmavý	tmavý	k2eAgInSc1d1	tmavý
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc1d1	černý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejdůležitější	důležitý	k2eAgFnSc4d3	nejdůležitější
techniku	technika	k1gFnSc4	technika
malíře	malíř	k1gMnSc2	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
charakteru	charakter	k1gInSc2	charakter
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
použitých	použitý	k2eAgInPc2d1	použitý
materiálů	materiál	k1gInPc2	materiál
či	či	k8xC	či
podkladu	podklad	k1gInSc2	podklad
se	se	k3xPyFc4	se
malba	malba	k1gFnSc1	malba
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
základních	základní	k2eAgFnPc2d1	základní
malířských	malířský	k2eAgFnPc2d1	malířská
technik	technika	k1gFnPc2	technika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
malbu	malba	k1gFnSc4	malba
obrazů	obraz	k1gInPc2	obraz
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
používána	používán	k2eAgFnSc1d1	používána
olejomalba	olejomalba	k1gFnSc1	olejomalba
<g/>
,	,	kIx,	,
před	před	k7c7	před
jejím	její	k3xOp3gNnSc7	její
rozšířením	rozšíření	k1gNnSc7	rozšíření
byla	být	k5eAaImAgFnS	být
používána	používat	k5eAaImNgFnS	používat
například	například	k6eAd1	například
vaječná	vaječný	k2eAgFnSc1d1	vaječná
tempera	tempera	k1gFnSc1	tempera
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
hojné	hojný	k2eAgFnSc6d1	hojná
míře	míra	k1gFnSc6	míra
také	také	k9	také
akryl	akryl	k1gInSc1	akryl
<g/>
.	.	kIx.	.
</s>
<s>
Podkladem	podklad	k1gInSc7	podklad
takové	takový	k3xDgFnSc2	takový
malby	malba	k1gFnSc2	malba
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
plátno	plátno	k1gNnSc1	plátno
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
častá	častý	k2eAgFnSc1d1	častá
i	i	k8xC	i
malba	malba	k1gFnSc1	malba
na	na	k7c4	na
dřevěnou	dřevěný	k2eAgFnSc4d1	dřevěná
podložku	podložka	k1gFnSc4	podložka
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hojné	hojný	k2eAgFnSc6d1	hojná
míře	míra	k1gFnSc6	míra
využíván	využívat	k5eAaImNgInS	využívat
jako	jako	k9	jako
podklad	podklad	k1gInSc1	podklad
například	například	k6eAd1	například
sololit	sololit	k1gInSc4	sololit
<g/>
.	.	kIx.	.
</s>
<s>
Malba	malba	k1gFnSc1	malba
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
prováděna	provádět	k5eAaImNgFnS	provádět
krycími	krycí	k2eAgFnPc7d1	krycí
barvami	barva	k1gFnPc7	barva
tzv.	tzv.	kA	tzv.
vrstvenou	vrstvený	k2eAgFnSc7d1	vrstvená
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
použití	použití	k1gNnSc4	použití
různých	různý	k2eAgInPc2d1	různý
efektů	efekt	k1gInPc2	efekt
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
lazury	lazura	k1gFnSc2	lazura
<g/>
.	.	kIx.	.
</s>
<s>
Podložka	podložka	k1gFnSc1	podložka
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
malováno	malován	k2eAgNnSc1d1	malováno
bývá	bývat	k5eAaImIp3nS	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
upravena	upravit	k5eAaPmNgFnS	upravit
pomocí	pomoc	k1gFnSc7	pomoc
podkladových	podkladový	k2eAgFnPc2d1	podkladová
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
podkladové	podkladový	k2eAgFnSc2d1	podkladová
vrstvy	vrstva	k1gFnSc2	vrstva
bývá	bývat	k5eAaImIp3nS	bývat
bílá	bílý	k2eAgFnSc1d1	bílá
či	či	k8xC	či
světlá	světlý	k2eAgFnSc1d1	světlá
<g/>
,	,	kIx,	,
v	v	k7c6	v
baroku	baroko	k1gNnSc6	baroko
ovšem	ovšem	k9	ovšem
byla	být	k5eAaImAgFnS	být
podkladová	podkladový	k2eAgFnSc1d1	podkladová
vrstva	vrstva	k1gFnSc1	vrstva
zpravidla	zpravidla	k6eAd1	zpravidla
tmavá	tmavý	k2eAgFnSc1d1	tmavá
(	(	kIx(	(
<g/>
bolus	bolus	k1gInSc1	bolus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
malba	malba	k1gFnSc1	malba
vznikala	vznikat	k5eAaImAgFnS	vznikat
tzv.	tzv.	kA	tzv.
vysvětlováním	vysvětlování	k1gNnSc7	vysvětlování
malovaného	malovaný	k2eAgInSc2d1	malovaný
předmětu	předmět	k1gInSc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
podkladové	podkladový	k2eAgFnSc2d1	podkladová
vrstvy	vrstva	k1gFnSc2	vrstva
má	mít	k5eAaImIp3nS	mít
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
použitá	použitý	k2eAgFnSc1d1	použitá
technika	technika	k1gFnSc1	technika
a	a	k8xC	a
podklad	podklad	k1gInSc1	podklad
<g/>
)	)	kIx)	)
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
pro	pro	k7c4	pro
celkové	celkový	k2eAgNnSc4d1	celkové
barevné	barevný	k2eAgNnSc4d1	barevné
vyznění	vyznění	k1gNnSc4	vyznění
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
provedením	provedení	k1gNnSc7	provedení
vlastního	vlastní	k2eAgNnSc2d1	vlastní
díla	dílo	k1gNnSc2	dílo
bývá	bývat	k5eAaImIp3nS	bývat
obraz	obraz	k1gInSc1	obraz
zpravidla	zpravidla	k6eAd1	zpravidla
rozvržen	rozvrhnout	k5eAaPmNgInS	rozvrhnout
pomocí	pomocí	k7c2	pomocí
podkresby	podkresba	k1gFnSc2	podkresba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
je	být	k5eAaImIp3nS	být
také	také	k9	také
malba	malba	k1gFnSc1	malba
na	na	k7c4	na
papír	papír	k1gInSc4	papír
pomocí	pomoc	k1gFnPc2	pomoc
vodou	voda	k1gFnSc7	voda
ředitelných	ředitelný	k2eAgFnPc2d1	ředitelná
<g/>
,	,	kIx,	,
nekrycích	krycí	k2eNgFnPc2d1	krycí
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
akvarel	akvarel	k1gInSc1	akvarel
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kombinován	kombinován	k2eAgMnSc1d1	kombinován
také	také	k9	také
s	s	k7c7	s
kresbou	kresba	k1gFnSc7	kresba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nástěnné	nástěnný	k2eAgNnSc1d1	nástěnné
malířství	malířství	k1gNnSc1	malířství
využívá	využívat	k5eAaImIp3nS	využívat
tři	tři	k4xCgFnPc4	tři
základní	základní	k2eAgFnPc4d1	základní
techniky	technika	k1gFnPc4	technika
<g/>
:	:	kIx,	:
Malbu	malba	k1gFnSc4	malba
do	do	k7c2	do
vlhké	vlhký	k2eAgFnSc2d1	vlhká
omítky	omítka	k1gFnSc2	omítka
–	–	k?	–
al	ala	k1gFnPc2	ala
fresco	fresco	k1gMnSc1	fresco
(	(	kIx(	(
<g/>
freska	freska	k1gFnSc1	freska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malbu	malba	k1gFnSc4	malba
na	na	k7c4	na
suchou	suchý	k2eAgFnSc4d1	suchá
omítku	omítka	k1gFnSc4	omítka
–	–	k?	–
al	ala	k1gFnPc2	ala
secco	secco	k6eAd1	secco
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
kombinaci	kombinace	k1gFnSc4	kombinace
–	–	k?	–
fresco-secco	frescoecco	k1gMnSc1	fresco-secco
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současné	současný	k2eAgNnSc1d1	současné
malířství	malířství	k1gNnSc1	malířství
často	často	k6eAd1	často
překračuje	překračovat	k5eAaImIp3nS	překračovat
historickou	historický	k2eAgFnSc7d1	historická
praxí	praxe	k1gFnSc7	praxe
vymezené	vymezený	k2eAgInPc1d1	vymezený
názvy	název	k1gInPc1	název
<g/>
,	,	kIx,	,
techniky	technika	k1gFnPc1	technika
i	i	k8xC	i
technologie	technologie	k1gFnPc1	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
vymezení	vymezení	k1gNnSc4	vymezení
současných	současný	k2eAgFnPc2d1	současná
malířských	malířský	k2eAgFnPc2d1	malířská
technik	technika	k1gFnPc2	technika
složitější	složitý	k2eAgMnSc1d2	složitější
a	a	k8xC	a
často	často	k6eAd1	často
vábnější	vábný	k2eAgFnSc1d2	vábnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
malby	malba	k1gFnSc2	malba
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnPc1	jaký
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgFnP	použít
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc7	jaký
technikou	technika	k1gFnSc7	technika
jsou	být	k5eAaImIp3nP	být
barvy	barva	k1gFnPc1	barva
nanášeny	nanášen	k2eAgFnPc1d1	nanášena
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
jaký	jaký	k3yQgInSc4	jaký
povrch	povrch	k1gInSc4	povrch
se	se	k3xPyFc4	se
barva	barva	k1gFnSc1	barva
nanáší	nanášet	k5eAaImIp3nS	nanášet
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
malbu	malba	k1gFnSc4	malba
rozdělit	rozdělit	k5eAaPmF	rozdělit
například	například	k6eAd1	například
na	na	k7c4	na
</s>
</p>
<p>
<s>
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
malba	malba	k1gFnSc1	malba
(	(	kIx(	(
<g/>
freska	freska	k1gFnSc1	freska
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
malba	malba	k1gFnSc1	malba
bytu	byt	k1gInSc2	byt
</s>
</p>
<p>
<s>
malba	malba	k1gFnSc1	malba
na	na	k7c6	na
dřevě	dřevo	k1gNnSc6	dřevo
</s>
</p>
<p>
<s>
malba	malba	k1gFnSc1	malba
na	na	k7c6	na
skle	sklo	k1gNnSc6	sklo
</s>
</p>
<p>
<s>
malba	malba	k1gFnSc1	malba
pod	pod	k7c7	pod
polevou	poleva	k1gFnSc7	poleva
</s>
</p>
<p>
<s>
malba	malba	k1gFnSc1	malba
porcelánu	porcelán	k1gInSc2	porcelán
</s>
</p>
<p>
<s>
malba	malba	k1gFnSc1	malba
prstem	prst	k1gInSc7	prst
</s>
</p>
<p>
<s>
malba	malba	k1gFnSc1	malba
sprejem	sprej	k1gInSc7	sprej
na	na	k7c6	na
zeď	zeď	k1gFnSc1	zeď
(	(	kIx(	(
<g/>
graffiti	graffiti	k1gNnSc1	graffiti
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
malba	malba	k1gFnSc1	malba
na	na	k7c4	na
omítku	omítka	k1gFnSc4	omítka
(	(	kIx(	(
<g/>
sgrafito	sgrafito	k1gNnSc4	sgrafito
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
malba	malba	k1gFnSc1	malba
temperou	tempera	k1gFnSc7	tempera
</s>
</p>
<p>
<s>
dekorativní	dekorativní	k2eAgFnSc1d1	dekorativní
malba	malba	k1gFnSc1	malba
</s>
</p>
<p>
<s>
emailová	emailový	k2eAgFnSc1d1	emailová
malba	malba	k1gFnSc1	malba
</s>
</p>
<p>
<s>
grisaille	grisaille	k1gFnSc1	grisaille
</s>
</p>
<p>
<s>
iluzivní	iluzivní	k2eAgFnSc1d1	iluzivní
malba	malba	k1gFnSc1	malba
</s>
</p>
<p>
<s>
keramická	keramický	k2eAgFnSc1d1	keramická
malba	malba	k1gFnSc1	malba
</s>
</p>
<p>
<s>
pastózní	pastózní	k2eAgFnSc1d1	pastózní
malba	malba	k1gFnSc1	malba
</s>
</p>
<p>
<s>
písková	pískový	k2eAgFnSc1d1	písková
malba	malba	k1gFnSc1	malba
</s>
</p>
<p>
<s>
ruční	ruční	k2eAgFnSc1d1	ruční
malba	malba	k1gFnSc1	malba
</s>
</p>
<p>
<s>
smaltovaná	smaltovaný	k2eAgFnSc1d1	smaltovaná
malba	malba	k1gFnSc1	malba
</s>
</p>
<p>
<s>
sépiová	sépiový	k2eAgFnSc1d1	sépiová
malba	malba	k1gFnSc1	malba
</s>
</p>
<p>
<s>
vodová	vodový	k2eAgFnSc1d1	vodová
malba	malba	k1gFnSc1	malba
(	(	kIx(	(
<g/>
akvarel	akvarel	k1gInSc1	akvarel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vosková	voskový	k2eAgFnSc1d1	vosková
malba	malba	k1gFnSc1	malba
</s>
</p>
<p>
<s>
kvaš	kvaš	k1gFnSc1	kvaš
</s>
</p>
<p>
<s>
olejomalba	olejomalba	k1gFnSc1	olejomalba
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Malířství	malířství	k1gNnSc1	malířství
</s>
</p>
<p>
<s>
Kresba	kresba	k1gFnSc1	kresba
</s>
</p>
<p>
<s>
Grafika	grafika	k1gFnSc1	grafika
</s>
</p>
<p>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
</s>
</p>
<p>
<s>
Koláž	koláž	k1gFnSc1	koláž
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
malířů	malíř	k1gMnPc2	malíř
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
malba	malba	k1gFnSc1	malba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
