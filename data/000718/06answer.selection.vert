<s>
Na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgNnPc4	tři
roční	roční	k2eAgNnPc4d1	roční
období	období	k1gNnPc4	období
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tag-init	Tagnit	k1gFnSc1	Tag-init
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Tag-araw	Tagraw	k1gFnSc1	Tag-araw
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
teplé	teplý	k2eAgNnSc4d1	teplé
období	období	k1gNnSc4	období
nebo	nebo	k8xC	nebo
léto	léto	k1gNnSc4	léto
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Tag-ulan	Taglan	k1gMnSc1	Tag-ulan
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
období	období	k1gNnSc1	období
dešťů	dešť	k1gInPc2	dešť
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Tag-lamig	Tagamig	k1gInSc1	Tag-lamig
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
chladné	chladný	k2eAgNnSc1d1	chladné
období	období	k1gNnSc1	období
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
