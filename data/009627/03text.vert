<p>
<s>
Football	Football	k1gInSc1	Football
Club	club	k1gInSc1	club
Lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
Jerevan	Jerevan	k1gMnSc1	Jerevan
(	(	kIx(	(
<g/>
arménsky	arménsky	k6eAd1	arménsky
<g/>
:	:	kIx,	:
Ֆ	Ֆ	k?	Ֆ
Ա	Ա	k?	Ա
"	"	kIx"	"
<g/>
Լ	Լ	k?	Լ
<g/>
"	"	kIx"	"
Ե	Ե	k?	Ե
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
arménský	arménský	k2eAgInSc1d1	arménský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
sídlící	sídlící	k2eAgInSc1d1	sídlící
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Jerevan	Jerevan	k1gMnSc1	Jerevan
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
sezonách	sezona	k1gFnPc6	sezona
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
<g/>
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
Z	z	k7c2	z
–	–	k?	–
zápasy	zápas	k1gInPc4	zápas
<g/>
,	,	kIx,	,
V	v	k7c6	v
–	–	k?	–
výhry	výhra	k1gFnPc1	výhra
<g/>
,	,	kIx,	,
R	R	kA	R
–	–	k?	–
remízy	remíza	k1gFnPc4	remíza
<g/>
,	,	kIx,	,
P	P	kA	P
–	–	k?	–
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
VG	VG	kA	VG
–	–	k?	–
vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
góly	gól	k1gInPc1	gól
<g/>
,	,	kIx,	,
OG	OG	kA	OG
–	–	k?	–
obdržené	obdržený	k2eAgInPc4d1	obdržený
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
–	–	k?	–
rozdíl	rozdíl	k1gInSc1	rozdíl
skóre	skóre	k1gNnSc2	skóre
<g/>
,	,	kIx,	,
B	B	kA	B
–	–	k?	–
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc4d1	červené
podbarvení	podbarvení	k1gNnSc4	podbarvení
–	–	k?	–
sestup	sestup	k1gInSc1	sestup
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc4d1	zelené
podbarvení	podbarvení	k1gNnSc4	podbarvení
–	–	k?	–
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgNnSc1d1	fialové
podbarvení	podbarvení	k1gNnSc1	podbarvení
–	–	k?	–
reorganizace	reorganizace	k1gFnSc1	reorganizace
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
skupiny	skupina	k1gFnSc2	skupina
či	či	k8xC	či
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
FC	FC	kA	FC
Lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
Yerevan	Yerevan	k1gMnSc1	Yerevan
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
