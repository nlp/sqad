<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Hastingsu	Hastings	k1gInSc2	Hastings
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1066	[number]	k4	1066
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
rozhodným	rozhodný	k2eAgNnSc7d1	rozhodné
vítězstvím	vítězství	k1gNnSc7	vítězství
normanského	normanský	k2eAgNnSc2d1	normanské
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
základem	základ	k1gInSc7	základ
ovládnutí	ovládnutí	k1gNnSc2	ovládnutí
Anglie	Anglie	k1gFnSc2	Anglie
Normany	Norman	k1gMnPc4	Norman
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
střetnutí	střetnutí	k1gNnSc6	střetnutí
armády	armáda	k1gFnSc2	armáda
vedené	vedený	k2eAgNnSc1d1	vedené
Vilémem	Vilém	k1gMnSc7	Vilém
<g/>
,	,	kIx,	,
vévodou	vévoda	k1gMnSc7	vévoda
z	z	k7c2	z
Normandie	Normandie	k1gFnSc2	Normandie
a	a	k8xC	a
anglosaského	anglosaský	k2eAgNnSc2d1	anglosaské
vojska	vojsko	k1gNnSc2	vojsko
vedeného	vedený	k2eAgInSc2d1	vedený
anglickým	anglický	k2eAgMnSc7d1	anglický
králem	král	k1gMnSc7	král
Haroldem	Harold	k1gMnSc7	Harold
Godwinsonem	Godwinson	k1gMnSc7	Godwinson
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
na	na	k7c6	na
Senlackém	Senlacký	k2eAgInSc6d1	Senlacký
vrchu	vrch	k1gInSc6	vrch
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
km	km	kA	km
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
od	od	k7c2	od
Hastingsu	Hastings	k1gInSc2	Hastings
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
obec	obec	k1gFnSc1	obec
Battle	Battle	k1gFnSc2	Battle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
skončila	skončit	k5eAaPmAgFnS	skončit
vítězstvím	vítězství	k1gNnSc7	vítězství
Normanů	Norman	k1gMnPc2	Norman
<g/>
,	,	kIx,	,
Harold	Harold	k1gMnSc1	Harold
Godwinson	Godwinson	k1gMnSc1	Godwinson
byl	být	k5eAaImAgMnS	být
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
šípem	šíp	k1gInSc7	šíp
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ho	on	k3xPp3gInSc4	on
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
objevil	objevit	k5eAaPmAgInS	objevit
anglický	anglický	k2eAgInSc1d1	anglický
odpor	odpor	k1gInSc1	odpor
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
bitva	bitva	k1gFnSc1	bitva
byla	být	k5eAaImAgFnS	být
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
momentem	moment	k1gInSc7	moment
v	v	k7c6	v
získání	získání	k1gNnSc6	získání
Vilémovy	Vilémův	k2eAgFnSc2d1	Vilémova
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnPc1	událost
předcházející	předcházející	k2eAgFnPc1d1	předcházející
této	tento	k3xDgFnSc3	tento
bitvě	bitva	k1gFnSc3	bitva
i	i	k8xC	i
průběh	průběh	k1gInSc4	průběh
střetu	střet	k1gInSc2	střet
samotného	samotný	k2eAgInSc2d1	samotný
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
zachycen	zachytit	k5eAaPmNgInS	zachytit
na	na	k7c6	na
proslulé	proslulý	k2eAgFnSc6d1	proslulá
tapisérii	tapisérie	k1gFnSc6	tapisérie
z	z	k7c2	z
Bayeux	Bayeux	k1gInSc1	Bayeux
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bitvy	bitva	k1gFnSc2	bitva
bylo	být	k5eAaImAgNnS	být
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
postaveno	postaven	k2eAgNnSc4d1	postaveno
opatství	opatství	k1gNnSc4	opatství
Batlle	Batlle	k1gFnSc2	Batlle
Abbey	Abbea	k1gFnSc2	Abbea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozadí	pozadí	k1gNnSc1	pozadí
konfliktu	konflikt	k1gInSc2	konflikt
==	==	k?	==
</s>
</p>
<p>
<s>
Harold	Harold	k1gMnSc1	Harold
Godwinson	Godwinson	k1gMnSc1	Godwinson
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
králi	král	k1gMnSc6	král
nejmocnějším	mocný	k2eAgMnSc7d3	nejmocnější
mužem	muž	k1gMnSc7	muž
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Eduarda	Eduard	k1gMnSc2	Eduard
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1066	[number]	k4	1066
si	se	k3xPyFc3	se
činil	činit	k5eAaImAgInS	činit
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
nárok	nárok	k1gInSc4	nárok
si	se	k3xPyFc3	se
zajistil	zajistit	k5eAaPmAgMnS	zajistit
podporu	podpora	k1gFnSc4	podpora
Witenagemotu	Witenagemot	k1gInSc2	Witenagemot
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Eduard	Eduard	k1gMnSc1	Eduard
původně	původně	k6eAd1	původně
doporučil	doporučit	k5eAaPmAgMnS	doporučit
jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc2	svůj
nástupce	nástupce	k1gMnSc2	nástupce
Viléma	Vilém	k1gMnSc2	Vilém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
smrtelném	smrtelný	k2eAgNnSc6d1	smrtelné
loži	lože	k1gNnSc6	lože
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
změnil	změnit	k5eAaPmAgMnS	změnit
a	a	k8xC	a
podpořil	podpořit	k5eAaPmAgMnS	podpořit
Harolda	Harolda	k1gMnSc1	Harolda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
si	se	k3xPyFc3	se
činil	činit	k5eAaImAgInS	činit
také	také	k9	také
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
.	.	kIx.	.
</s>
<s>
Haroldovu	Haroldův	k2eAgFnSc4d1	Haroldova
korunovaci	korunovace	k1gFnSc4	korunovace
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
budoval	budovat	k5eAaImAgMnS	budovat
15	[number]	k4	15
let	let	k1gInSc4	let
a	a	k8xC	a
nebyl	být	k5eNaImAgMnS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
se	se	k3xPyFc4	se
lehce	lehko	k6eAd1	lehko
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Normanská	normanský	k2eAgFnSc1d1	normanská
armáda	armáda	k1gFnSc1	armáda
ale	ale	k9	ale
nebyla	být	k5eNaImAgFnS	být
dostatečně	dostatečně	k6eAd1	dostatečně
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
tak	tak	k6eAd1	tak
byli	být	k5eAaImAgMnP	být
na	na	k7c4	na
jednání	jednání	k1gNnSc4	jednání
do	do	k7c2	do
Caen	Caena	k1gFnPc2	Caena
pozvání	pozvánět	k5eAaImIp3nP	pozvánět
i	i	k9	i
šlechtici	šlechtic	k1gMnPc1	šlechtic
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
Vilém	Vilém	k1gMnSc1	Vilém
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
půdu	půda	k1gFnSc4	půda
a	a	k8xC	a
tituly	titul	k1gInPc1	titul
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
podpoří	podpořit	k5eAaPmIp3nP	podpořit
jeho	jeho	k3xOp3gFnSc4	jeho
výpravu	výprava	k1gFnSc4	výprava
a	a	k8xC	a
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
zajistit	zajistit	k5eAaPmF	zajistit
podporu	podpora	k1gFnSc4	podpora
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
pro	pro	k7c4	pro
invazi	invaze	k1gFnSc4	invaze
700	[number]	k4	700
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1066	[number]	k4	1066
se	se	k3xPyFc4	se
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
,	,	kIx,	,
po	po	k7c4	po
zdržení	zdržení	k1gNnSc4	zdržení
v	v	k7c6	v
bouři	bouř	k1gFnSc6	bouř
<g/>
,	,	kIx,	,
vylodil	vylodit	k5eAaPmAgMnS	vylodit
na	na	k7c6	na
anglickém	anglický	k2eAgNnSc6d1	anglické
pobřeží	pobřeží	k1gNnSc6	pobřeží
nedaleko	nedaleko	k7c2	nedaleko
města	město	k1gNnSc2	město
Hastingsu	Hastings	k1gInSc2	Hastings
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
vlastní	vlastní	k2eAgFnSc1d1	vlastní
bitva	bitva	k1gFnSc1	bitva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
se	se	k3xPyFc4	se
dověděl	dovědět	k5eAaPmAgMnS	dovědět
o	o	k7c6	o
vylodění	vylodění	k1gNnSc6	vylodění
normanské	normanský	k2eAgFnSc2d1	normanská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Harold	Harold	k1gMnSc1	Harold
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
právě	právě	k6eAd1	právě
odrazil	odrazit	k5eAaPmAgMnS	odrazit
invazi	invaze	k1gFnSc4	invaze
vikingského	vikingský	k2eAgNnSc2d1	vikingské
vojska	vojsko	k1gNnSc2	vojsko
vedeného	vedený	k2eAgInSc2d1	vedený
Haraldem	Harald	k1gInSc7	Harald
Hardradou	Hardrada	k1gFnSc7	Hardrada
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
bratrem	bratr	k1gMnSc7	bratr
Tostigem	Tostig	k1gMnSc7	Tostig
Godwinsonem	Godwinson	k1gMnSc7	Godwinson
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
nedaleko	nedaleko	k7c2	nedaleko
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Stamford	Stamforda	k1gFnPc2	Stamforda
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odrazil	odrazit	k5eAaPmAgInS	odrazit
další	další	k2eAgFnSc3d1	další
invazi	invaze	k1gFnSc3	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Gyrth	Gyrth	k1gMnSc1	Gyrth
doporučoval	doporučovat	k5eAaImAgMnS	doporučovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyčkalo	vyčkat	k5eAaPmAgNnS	vyčkat
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
shromáždí	shromáždět	k5eAaImIp3nS	shromáždět
větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Harold	Harold	k1gMnSc1	Harold
chtěl	chtít	k5eAaImAgMnS	chtít
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
ochránit	ochránit	k5eAaPmF	ochránit
své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
království	království	k1gNnSc4	království
proti	proti	k7c3	proti
komukoli	kdokoli	k3yInSc3	kdokoli
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
dorazil	dorazit	k5eAaPmAgInS	dorazit
ráno	ráno	k6eAd1	ráno
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
sehnat	sehnat	k5eAaPmF	sehnat
za	za	k7c4	za
tak	tak	k6eAd1	tak
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
k	k	k7c3	k
Senlackému	Senlacký	k2eAgInSc3d1	Senlacký
vrchu	vrch	k1gInSc3	vrch
dorazil	dorazit	k5eAaPmAgMnS	dorazit
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Harold	Harold	k1gMnSc1	Harold
rozmístil	rozmístit	k5eAaPmAgMnS	rozmístit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
kolem	kolem	k7c2	kolem
silnice	silnice	k1gFnSc2	silnice
z	z	k7c2	z
Hastingsu	Hastings	k1gInSc2	Hastings
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
asi	asi	k9	asi
10	[number]	k4	10
km	km	kA	km
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
od	od	k7c2	od
Hastingsu	Hastings	k1gInSc2	Hastings
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
hluboký	hluboký	k2eAgInSc4d1	hluboký
les	les	k1gInSc4	les
a	a	k8xC	a
před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
se	se	k3xPyFc4	se
půda	půda	k1gFnSc1	půda
pozvolna	pozvolna	k6eAd1	pozvolna
skláněla	sklánět	k5eAaImAgFnS	sklánět
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Anglosaské	anglosaský	k2eAgNnSc1d1	anglosaské
vojsko	vojsko	k1gNnSc1	vojsko
==	==	k?	==
</s>
</p>
<p>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
anglosaské	anglosaský	k2eAgNnSc1d1	anglosaské
vojsko	vojsko	k1gNnSc1	vojsko
čítalo	čítat	k5eAaImAgNnS	čítat
asi	asi	k9	asi
7	[number]	k4	7
500	[number]	k4	500
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc4	ten
pouze	pouze	k6eAd1	pouze
kopiníci	kopiník	k1gMnPc1	kopiník
a	a	k8xC	a
jezdectvo	jezdectvo	k1gNnSc1	jezdectvo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
dopravili	dopravit	k5eAaPmAgMnP	dopravit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bitvy	bitva	k1gFnSc2	bitva
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
jako	jako	k9	jako
pěšáci	pěšák	k1gMnPc1	pěšák
<g/>
.	.	kIx.	.
</s>
<s>
Jádrem	jádro	k1gNnSc7	jádro
anglosaské	anglosaský	k2eAgFnSc2d1	anglosaská
armády	armáda	k1gFnSc2	armáda
byli	být	k5eAaImAgMnP	být
profesionální	profesionální	k2eAgMnPc1d1	profesionální
žoldáci	žoldák	k1gMnPc1	žoldák
nazývaní	nazývaný	k2eAgMnPc1d1	nazývaný
Housecarls	Housecarls	k1gInSc4	Housecarls
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
krále	král	k1gMnSc2	král
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
bojovat	bojovat	k5eAaImF	bojovat
do	do	k7c2	do
posledního	poslední	k2eAgMnSc2d1	poslední
muže	muž	k1gMnSc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
zbroj	zbroj	k1gFnSc1	zbroj
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
kuželovitou	kuželovitý	k2eAgFnSc4d1	kuželovitá
helmici	helmice	k1gFnSc4	helmice
<g/>
,	,	kIx,	,
kroužkovité	kroužkovitý	k2eAgNnSc4d1	kroužkovitý
brnění	brnění	k1gNnSc4	brnění
a	a	k8xC	a
kruhový	kruhový	k2eAgInSc4d1	kruhový
štít	štít	k1gInSc4	štít
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
hlavní	hlavní	k2eAgFnSc7d1	hlavní
zbraní	zbraň	k1gFnSc7	zbraň
byla	být	k5eAaImAgFnS	být
vikingská	vikingský	k2eAgFnSc1d1	vikingská
sekera	sekera	k1gFnSc1	sekera
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ovládali	ovládat	k5eAaImAgMnP	ovládat
obouruč	obouruč	k6eAd1	obouruč
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
muž	muž	k1gMnSc1	muž
měl	mít	k5eAaImAgMnS	mít
i	i	k9	i
meč	meč	k1gInSc4	meč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejpočetnější	početní	k2eAgFnSc4d3	nejpočetnější
část	část	k1gFnSc4	část
vojska	vojsko	k1gNnSc2	vojsko
tvořili	tvořit	k5eAaImAgMnP	tvořit
vojáci	voják	k1gMnPc1	voják
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
malých	malý	k2eAgMnPc2d1	malý
farmářů	farmář	k1gMnPc2	farmář
a	a	k8xC	a
v	v	k7c6	v
anglosaském	anglosaský	k2eAgNnSc6d1	anglosaské
prostředí	prostředí	k1gNnSc6	prostředí
byli	být	k5eAaImAgMnP	být
označováni	označovat	k5eAaImNgMnP	označovat
pojmem	pojem	k1gInSc7	pojem
fyrd	fyrda	k1gFnPc2	fyrda
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc7	jejich
veliteli	velitel	k1gMnPc7	velitel
(	(	kIx(	(
<g/>
thegn	thegno	k1gNnPc2	thegno
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
příslušníci	příslušník	k1gMnPc1	příslušník
velkostatkářské	velkostatkářský	k2eAgFnSc2d1	velkostatkářská
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
povinni	povinen	k2eAgMnPc1d1	povinen
sloužit	sloužit	k5eAaImF	sloužit
králi	král	k1gMnSc3	král
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
a	a	k8xC	a
být	být	k5eAaImF	být
vybaveni	vybavit	k5eAaPmNgMnP	vybavit
vlastním	vlastní	k2eAgNnSc7d1	vlastní
brněním	brnění	k1gNnSc7	brnění
a	a	k8xC	a
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejhrozivější	hrozivý	k2eAgFnSc7d3	nejhrozivější
anglosaskou	anglosaský	k2eAgFnSc7d1	anglosaská
obranou	obrana	k1gFnSc7	obrana
byla	být	k5eAaImAgFnS	být
štítová	štítový	k2eAgFnSc1d1	štítová
hradba	hradba	k1gFnSc1	hradba
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgInPc4	který
všichni	všechen	k3xTgMnPc1	všechen
muži	muž	k1gMnPc1	muž
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
spojili	spojit	k5eAaPmAgMnP	spojit
své	svůj	k3xOyFgInPc4	svůj
štíty	štít	k1gInPc4	štít
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvotním	prvotní	k2eAgNnSc6d1	prvotní
stádiu	stádium	k1gNnSc6	stádium
bitvy	bitva	k1gFnSc2	bitva
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
val	val	k1gInSc1	val
velmi	velmi	k6eAd1	velmi
efektivní	efektivní	k2eAgFnSc4d1	efektivní
obranou	obrana	k1gFnSc7	obrana
proti	proti	k7c3	proti
šípům	šíp	k1gInPc3	šíp
vystřelovaným	vystřelovaný	k2eAgInPc3d1	vystřelovaný
Normany	Norman	k1gMnPc7	Norman
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
vojska	vojsko	k1gNnSc2	vojsko
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
za	za	k7c7	za
první	první	k4xOgFnSc7	první
řadou	řada	k1gFnSc7	řada
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
případní	případný	k2eAgMnPc1d1	případný
padlí	padlý	k1gMnPc1	padlý
byli	být	k5eAaImAgMnP	být
nahrazeni	nahradit	k5eAaPmNgMnP	nahradit
ve	v	k7c6	v
štítové	štítový	k2eAgFnSc6d1	štítová
hradbě	hradba	k1gFnSc6	hradba
dalšími	další	k2eAgMnPc7d1	další
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Normanské	normanský	k2eAgNnSc1d1	normanské
vojsko	vojsko	k1gNnSc1	vojsko
==	==	k?	==
</s>
</p>
<p>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
normanské	normanský	k2eAgNnSc1d1	normanské
vojsko	vojsko	k1gNnSc1	vojsko
čítalo	čítat	k5eAaImAgNnS	čítat
asi	asi	k9	asi
8	[number]	k4	8
400	[number]	k4	400
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2	[number]	k4	2
200	[number]	k4	200
byli	být	k5eAaImAgMnP	být
jezdci	jezdec	k1gMnPc1	jezdec
<g/>
,	,	kIx,	,
4	[number]	k4	4
500	[number]	k4	500
pěchoty	pěchota	k1gFnPc1	pěchota
a	a	k8xC	a
1	[number]	k4	1
700	[number]	k4	700
lukostřelců	lukostřelec	k1gMnPc2	lukostřelec
<g/>
.	.	kIx.	.
</s>
<s>
Vilémova	Vilémův	k2eAgFnSc1d1	Vilémova
strategie	strategie	k1gFnSc1	strategie
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
lukostřelcích	lukostřelec	k1gMnPc6	lukostřelec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
narušit	narušit	k5eAaPmF	narušit
anglosaské	anglosaský	k2eAgFnPc4d1	anglosaská
řady	řada	k1gFnPc4	řada
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
měla	mít	k5eAaImAgFnS	mít
vyrazit	vyrazit	k5eAaPmF	vyrazit
pěchota	pěchota	k1gFnSc1	pěchota
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
kavalérie	kavalérie	k1gFnSc1	kavalérie
<g/>
.	.	kIx.	.
</s>
<s>
Normanské	normanský	k2eAgNnSc1d1	normanské
vojsko	vojsko	k1gNnSc1	vojsko
bylo	být	k5eAaImAgNnS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
šlechtici	šlechtic	k1gMnPc7	šlechtic
a	a	k8xC	a
vojáky	voják	k1gMnPc7	voják
od	od	k7c2	od
Francie	Francie	k1gFnSc2	Francie
až	až	k9	až
po	po	k7c4	po
jih	jih	k1gInSc4	jih
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Síla	síla	k1gFnSc1	síla
normanské	normanský	k2eAgFnSc2d1	normanská
armády	armáda	k1gFnSc2	armáda
vycházela	vycházet	k5eAaImAgNnP	vycházet
z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
jezdectva	jezdectvo	k1gNnSc2	jezdectvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
nejlepším	dobrý	k2eAgFnPc3d3	nejlepší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jezdci	jezdec	k1gMnPc1	jezdec
byli	být	k5eAaImAgMnP	být
vybaveni	vybavit	k5eAaPmNgMnP	vybavit
těžkým	těžký	k2eAgNnSc7d1	těžké
brněním	brnění	k1gNnSc7	brnění
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
byli	být	k5eAaImAgMnP	být
vyzbrojeni	vyzbrojit	k5eAaPmNgMnP	vyzbrojit
kopím	kopí	k1gNnSc7	kopí
a	a	k8xC	a
mečem	meč	k1gInSc7	meč
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
veškeré	veškerý	k3xTgNnSc4	veškerý
jezdectvo	jezdectvo	k1gNnSc1	jezdectvo
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
toto	tento	k3xDgNnSc1	tento
nejefektivnější	efektivní	k2eAgNnSc1d3	nejefektivnější
proti	proti	k7c3	proti
protivníkovi	protivník	k1gMnSc3	protivník
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
řady	řada	k1gFnPc1	řada
začaly	začít	k5eAaPmAgFnP	začít
kolísat	kolísat	k5eAaImF	kolísat
a	a	k8xC	a
v	v	k7c6	v
jehož	jehož	k3xOyRp3gNnSc6	jehož
obranném	obranný	k2eAgNnSc6d1	obranné
postavení	postavení	k1gNnSc6	postavení
se	se	k3xPyFc4	se
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
mezery	mezera	k1gFnPc1	mezera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
lukostřelců	lukostřelec	k1gMnPc2	lukostřelec
byli	být	k5eAaImAgMnP	být
ostatní	ostatní	k2eAgMnPc1d1	ostatní
normanští	normanský	k2eAgMnPc1d1	normanský
vojáci	voják	k1gMnPc1	voják
chráněni	chránit	k5eAaImNgMnP	chránit
kroužkovým	kroužkový	k2eAgNnPc3d1	kroužkové
brněním	brnění	k1gNnPc3	brnění
a	a	k8xC	a
vyzbrojeni	vyzbrojen	k2eAgMnPc1d1	vyzbrojen
kopím	kopí	k1gNnSc7	kopí
<g/>
,	,	kIx,	,
mečem	meč	k1gInSc7	meč
a	a	k8xC	a
štítem	štít	k1gInSc7	štít
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jejich	jejich	k3xOp3gMnPc1	jejich
angličtí	anglický	k2eAgMnPc1d1	anglický
protivníci	protivník	k1gMnPc1	protivník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
lukostřelců	lukostřelec	k1gMnPc2	lukostřelec
byl	být	k5eAaImAgMnS	být
odrazem	odraz	k1gInSc7	odraz
trendů	trend	k1gInPc2	trend
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
evropských	evropský	k2eAgFnPc6d1	Evropská
armádách	armáda	k1gFnPc6	armáda
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Luk	luk	k1gInSc1	luk
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
krátkou	krátký	k2eAgFnSc7d1	krátká
zbraní	zbraň	k1gFnSc7	zbraň
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
tětivou	tětiva	k1gFnSc7	tětiva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
efektivní	efektivní	k2eAgFnSc7d1	efektivní
zbraní	zbraň	k1gFnSc7	zbraň
na	na	k7c6	na
bojištích	bojiště	k1gNnPc6	bojiště
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Hastings	Hastings	k1gInSc1	Hastings
byl	být	k5eAaImAgInS	být
také	také	k9	také
místem	místem	k6eAd1	místem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
poprvé	poprvé	k6eAd1	poprvé
použita	použit	k2eAgFnSc1d1	použita
kuše	kuše	k1gFnSc1	kuše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bitva	bitva	k1gFnSc1	bitva
==	==	k?	==
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
použít	použít	k5eAaPmF	použít
strategii	strategie	k1gFnSc4	strategie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
by	by	kYmCp3nP	by
lučištníci	lučištník	k1gMnPc1	lučištník
způsobili	způsobit	k5eAaPmAgMnP	způsobit
počáteční	počáteční	k2eAgInSc4d1	počáteční
zmatek	zmatek	k1gInSc4	zmatek
a	a	k8xC	a
ztráty	ztráta	k1gFnPc4	ztráta
mezi	mezi	k7c7	mezi
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
měla	mít	k5eAaImAgFnS	mít
nastoupit	nastoupit	k5eAaPmF	nastoupit
pěchota	pěchota	k1gFnSc1	pěchota
a	a	k8xC	a
v	v	k7c6	v
boji	boj	k1gInSc6	boj
zblízka	zblízka	k6eAd1	zblízka
vytvořit	vytvořit	k5eAaPmF	vytvořit
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
uplatnění	uplatnění	k1gNnSc4	uplatnění
jezdectva	jezdectvo	k1gNnSc2	jezdectvo
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
bitvy	bitva	k1gFnSc2	bitva
tato	tento	k3xDgFnSc1	tento
taktika	taktika	k1gFnSc1	taktika
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
účinná	účinný	k2eAgFnSc1d1	účinná
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
představoval	představovat	k5eAaImAgMnS	představovat
<g/>
.	.	kIx.	.
</s>
<s>
Vilémova	Vilémův	k2eAgFnSc1d1	Vilémova
armáda	armáda	k1gFnSc1	armáda
napadla	napadnout	k5eAaPmAgFnS	napadnout
již	již	k6eAd1	již
zformované	zformovaný	k2eAgNnSc4d1	zformované
anglosaské	anglosaský	k2eAgNnSc4d1	anglosaské
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Normanští	normanský	k2eAgMnPc1d1	normanský
lučištníci	lučištník	k1gMnPc1	lučištník
vystřelili	vystřelit	k5eAaPmAgMnP	vystřelit
své	svůj	k3xOyFgInPc4	svůj
šípy	šíp	k1gInPc4	šíp
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohé	mnohý	k2eAgFnPc1d1	mnohá
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
skončily	skončit	k5eAaPmAgFnP	skončit
na	na	k7c6	na
štítové	štítový	k2eAgFnSc6d1	štítová
hradbě	hradba	k1gFnSc6	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
anglosaské	anglosaský	k2eAgNnSc1d1	anglosaské
vojsko	vojsko	k1gNnSc1	vojsko
je	být	k5eAaImIp3nS	být
rozkolísané	rozkolísaný	k2eAgNnSc1d1	rozkolísané
a	a	k8xC	a
tak	tak	k6eAd1	tak
zavelel	zavelet	k5eAaPmAgMnS	zavelet
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
Normané	Norman	k1gMnPc1	Norman
šplhali	šplhat	k5eAaImAgMnP	šplhat
do	do	k7c2	do
kopce	kopec	k1gInSc2	kopec
<g/>
,	,	kIx,	,
Anglosasové	Anglosas	k1gMnPc1	Anglosas
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
házeli	házet	k5eAaImAgMnP	házet
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
měli	mít	k5eAaImAgMnP	mít
po	po	k7c6	po
ruce	ruka	k1gFnSc6	ruka
–	–	k?	–
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
oštěpy	oštěp	k1gInPc7	oštěp
a	a	k8xC	a
palcáty	palcát	k1gInPc7	palcát
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
předměty	předmět	k1gInPc1	předmět
způsobily	způsobit	k5eAaPmAgInP	způsobit
Normanům	Norman	k1gMnPc3	Norman
velké	velký	k2eAgFnPc4d1	velká
ztráty	ztráta	k1gFnPc4	ztráta
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
řady	řada	k1gFnPc1	řada
byly	být	k5eAaImAgFnP	být
porušeny	porušit	k5eAaPmNgFnP	porušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Normanská	normanský	k2eAgFnSc1d1	normanská
pěchota	pěchota	k1gFnSc1	pěchota
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
anglosaské	anglosaský	k2eAgFnPc4d1	anglosaská
linie	linie	k1gFnPc4	linie
a	a	k8xC	a
tam	tam	k6eAd1	tam
nastal	nastat	k5eAaPmAgInS	nastat
těžký	těžký	k2eAgInSc1d1	těžký
boj	boj	k1gInSc1	boj
muže	muž	k1gMnSc2	muž
proti	proti	k7c3	proti
muži	muž	k1gMnSc3	muž
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
očekával	očekávat	k5eAaImAgMnS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Anglosasové	Anglosas	k1gMnPc1	Anglosas
budou	být	k5eAaImBp3nP	být
rozkolísáni	rozkolísat	k5eAaPmNgMnP	rozkolísat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
střelba	střelba	k1gFnSc1	střelba
lukostřelců	lukostřelec	k1gMnPc2	lukostřelec
neměla	mít	k5eNaImAgFnS	mít
téměř	téměř	k6eAd1	téměř
žádný	žádný	k3yNgInSc4	žádný
efekt	efekt	k1gInSc4	efekt
a	a	k8xC	a
Anglosasové	Anglosas	k1gMnPc1	Anglosas
drželi	držet	k5eAaImAgMnP	držet
své	svůj	k3xOyFgFnPc4	svůj
pozice	pozice	k1gFnPc4	pozice
a	a	k8xC	a
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
štítový	štítový	k2eAgInSc4d1	štítový
val	val	k1gInSc4	val
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
zavelel	zavelet	k5eAaPmAgMnS	zavelet
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
kavalerie	kavalerie	k1gFnSc2	kavalerie
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
dokonalému	dokonalý	k2eAgInSc3d1	dokonalý
výcviku	výcvik	k1gInSc3	výcvik
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
koní	kůň	k1gMnPc2	kůň
proti	proti	k7c3	proti
sevřenému	sevřený	k2eAgInSc3d1	sevřený
valu	val	k1gInSc3	val
štítů	štít	k1gInPc2	štít
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
trčely	trčet	k5eAaImAgFnP	trčet
kopí	kopit	k5eAaImIp3nP	kopit
a	a	k8xC	a
meče	meč	k1gInPc1	meč
<g/>
,	,	kIx,	,
zaváhalo	zaváhat	k5eAaPmAgNnS	zaváhat
a	a	k8xC	a
uskočilo	uskočit	k5eAaPmAgNnS	uskočit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hodině	hodina	k1gFnSc6	hodina
bitvy	bitva	k1gFnSc2	bitva
bretaňská	bretaňský	k2eAgFnSc1d1	bretaňská
divize	divize	k1gFnSc1	divize
na	na	k7c6	na
levém	levý	k2eAgNnSc6d1	levé
Vilémově	Vilémův	k2eAgNnSc6d1	Vilémovo
křídle	křídlo	k1gNnSc6	křídlo
zaváhala	zaváhat	k5eAaPmAgFnS	zaváhat
a	a	k8xC	a
prchala	prchat	k5eAaImAgFnS	prchat
z	z	k7c2	z
kopce	kopec	k1gInSc2	kopec
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
velkých	velký	k2eAgFnPc6d1	velká
ztrátách	ztráta	k1gFnPc6	ztráta
se	se	k3xPyFc4	se
normanská	normanský	k2eAgFnSc1d1	normanská
a	a	k8xC	a
vlámská	vlámský	k2eAgFnSc1d1	vlámská
divize	divize	k1gFnSc1	divize
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
bretaňské	bretaňský	k2eAgFnSc3d1	bretaňská
a	a	k8xC	a
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Anglosasů	Anglosas	k1gMnPc2	Anglosas
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Haroldových	Haroldův	k2eAgMnPc2d1	Haroldův
bratrů	bratr	k1gMnPc2	bratr
Leofwyna	Leofwyn	k1gMnSc2	Leofwyn
a	a	k8xC	a
Gyrtha	Gyrth	k1gMnSc2	Gyrth
<g/>
,	,	kIx,	,
nemohlo	moct	k5eNaImAgNnS	moct
odolat	odolat	k5eAaPmF	odolat
pokušení	pokušení	k1gNnSc4	pokušení
a	a	k8xC	a
vysunuli	vysunout	k5eAaPmAgMnP	vysunout
se	se	k3xPyFc4	se
z	z	k7c2	z
obranných	obranný	k2eAgFnPc2d1	obranná
linií	linie	k1gFnPc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
zmatené	zmatený	k2eAgFnSc6d1	zmatená
bitvě	bitva	k1gFnSc6	bitva
byl	být	k5eAaImAgMnS	být
zabit	zabit	k2eAgMnSc1d1	zabit
Vilémův	Vilémův	k2eAgMnSc1d1	Vilémův
kůň	kůň	k1gMnSc1	kůň
a	a	k8xC	a
Normané	Norman	k1gMnPc1	Norman
se	se	k3xPyFc4	se
po	po	k7c6	po
domnělé	domnělý	k2eAgFnSc6d1	domnělá
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc4	svůj
vůdce	vůdce	k1gMnSc4	vůdce
dali	dát	k5eAaPmAgMnP	dát
na	na	k7c4	na
ústup	ústup	k1gInSc4	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
ale	ale	k8xC	ale
ukázal	ukázat	k5eAaPmAgMnS	ukázat
svou	svůj	k3xOyFgFnSc4	svůj
helmu	helma	k1gFnSc4	helma
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
organizovat	organizovat	k5eAaBmF	organizovat
své	svůj	k3xOyFgNnSc4	svůj
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
jeho	jeho	k3xOp3gMnPc2	jeho
rytířů	rytíř	k1gMnPc2	rytíř
napadli	napadnout	k5eAaPmAgMnP	napadnout
Anglosasy	Anglosas	k1gMnPc7	Anglosas
roztroušené	roztroušený	k2eAgNnSc1d1	roztroušené
za	za	k7c4	za
obrannou	obranný	k2eAgFnSc4d1	obranná
linií	linie	k1gFnSc7	linie
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
ti	ten	k3xDgMnPc1	ten
už	už	k6eAd1	už
nebyli	být	k5eNaImAgMnP	být
chráněni	chránit	k5eAaImNgMnP	chránit
obranným	obranný	k2eAgInSc7d1	obranný
valem	val	k1gInSc7	val
<g/>
,	,	kIx,	,
stali	stát	k5eAaPmAgMnP	stát
se	s	k7c7	s
snadnou	snadný	k2eAgFnSc7d1	snadná
kořistí	kořist	k1gFnSc7	kořist
Normanů	Norman	k1gMnPc2	Norman
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nerozpoznali	rozpoznat	k5eNaPmAgMnP	rozpoznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Normané	Norman	k1gMnPc1	Norman
zahájili	zahájit	k5eAaPmAgMnP	zahájit
protiútok	protiútok	k1gInSc4	protiútok
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebylo	být	k5eNaImAgNnS	být
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgMnPc3	některý
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
podařilo	podařit	k5eAaPmAgNnS	podařit
ustoupit	ustoupit	k5eAaPmF	ustoupit
za	za	k7c4	za
obrannou	obranný	k2eAgFnSc4d1	obranná
linii	linie	k1gFnSc4	linie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Haroldovi	Haroldův	k2eAgMnPc1d1	Haroldův
bratři	bratr	k1gMnPc1	bratr
padli	padnout	k5eAaImAgMnP	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
armády	armáda	k1gFnPc1	armáda
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
zformovaly	zformovat	k5eAaPmAgFnP	zformovat
a	a	k8xC	a
nastal	nastat	k5eAaPmAgMnS	nastat
relativní	relativní	k2eAgInSc4d1	relativní
klid	klid	k1gInSc4	klid
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
toho	ten	k3xDgMnSc4	ten
využil	využít	k5eAaPmAgMnS	využít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
nové	nový	k2eAgFnSc2d1	nová
taktiky	taktika	k1gFnSc2	taktika
<g/>
.	.	kIx.	.
</s>
<s>
Přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
Anglosasové	Anglosas	k1gMnPc1	Anglosas
za	za	k7c7	za
obranným	obranný	k2eAgInSc7d1	obranný
valem	val	k1gInSc7	val
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
je	být	k5eAaImIp3nS	být
překonat	překonat	k5eAaPmF	překonat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roztroušení	roztroušený	k2eAgMnPc1d1	roztroušený
anglosaští	anglosaský	k2eAgMnPc1d1	anglosaský
bojovníci	bojovník	k1gMnPc1	bojovník
byli	být	k5eAaImAgMnP	být
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gMnPc4	jeho
vojáky	voják	k1gMnPc4	voják
snadným	snadný	k2eAgInSc7d1	snadný
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
zformovaným	zformovaný	k2eAgNnSc7d1	zformované
vojskem	vojsko	k1gNnSc7	vojsko
znovu	znovu	k6eAd1	znovu
zaútočil	zaútočit	k5eAaPmAgInS	zaútočit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Normané	Norman	k1gMnPc1	Norman
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
velké	velký	k2eAgFnPc4d1	velká
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
anglosaských	anglosaský	k2eAgMnPc2d1	anglosaský
důstojníků	důstojník	k1gMnPc2	důstojník
zabito	zabít	k5eAaPmNgNnS	zabít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
linii	linie	k1gFnSc4	linie
obranného	obranný	k2eAgInSc2d1	obranný
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
valu	val	k1gInSc2	val
tak	tak	k6eAd1	tak
tvořilo	tvořit	k5eAaImAgNnS	tvořit
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
řadových	řadový	k2eAgMnPc2d1	řadový
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
jejich	jejich	k3xOp3gMnPc1	jejich
důstojníci	důstojník	k1gMnPc1	důstojník
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
dobře	dobře	k6eAd1	dobře
organizovat	organizovat	k5eAaBmF	organizovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
obranný	obranný	k2eAgInSc1d1	obranný
val	val	k1gInSc1	val
začal	začít	k5eAaPmAgInS	začít
mít	mít	k5eAaImF	mít
mezery	mezera	k1gFnPc4	mezera
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
normanští	normanský	k2eAgMnPc1d1	normanský
lučištníci	lučištník	k1gMnPc1	lučištník
stříleli	střílet	k5eAaImAgMnP	střílet
na	na	k7c6	na
krajní	krajní	k2eAgFnSc6d1	krajní
linii	linie	k1gFnSc6	linie
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInPc1	jejich
šípy	šíp	k1gInPc1	šíp
se	se	k3xPyFc4	se
odrážely	odrážet	k5eAaImAgInP	odrážet
od	od	k7c2	od
štítového	štítový	k2eAgInSc2d1	štítový
valu	val	k1gInSc2	val
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
vojáků	voják	k1gMnPc2	voják
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
linii	linie	k1gFnSc6	linie
mělo	mít	k5eAaImAgNnS	mít
své	svůj	k3xOyFgInPc4	svůj
štíty	štít	k1gInPc4	štít
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
nařídil	nařídit	k5eAaPmAgMnS	nařídit
zamířit	zamířit	k5eAaPmF	zamířit
střelbu	střelba	k1gFnSc4	střelba
za	za	k7c2	za
přední	přední	k2eAgFnSc2d1	přední
linie	linie	k1gFnSc2	linie
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
šípy	šíp	k1gInPc1	šíp
tak	tak	k6eAd1	tak
zasáhly	zasáhnout	k5eAaPmAgInP	zasáhnout
týlovou	týlový	k2eAgFnSc4d1	týlová
část	část	k1gFnSc4	část
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
zasažen	zasáhnout	k5eAaPmNgInS	zasáhnout
šípem	šíp	k1gInSc7	šíp
i	i	k8xC	i
Harold	Harold	k1gInSc4	Harold
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
spekulaci	spekulace	k1gFnSc4	spekulace
odvozenou	odvozený	k2eAgFnSc4d1	odvozená
z	z	k7c2	z
tapisérie	tapisérie	k1gFnSc2	tapisérie
z	z	k7c2	z
Bayeux	Bayeux	k1gInSc4	Bayeux
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Anglosasů	Anglosas	k1gMnPc2	Anglosas
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
vyčerpáno	vyčerpat	k5eAaPmNgNnS	vyčerpat
a	a	k8xC	a
disciplína	disciplína	k1gFnSc1	disciplína
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
obranného	obranný	k2eAgInSc2d1	obranný
valu	val	k1gInSc2	val
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
Vilémova	Vilémův	k2eAgFnSc1d1	Vilémova
armáda	armáda	k1gFnSc1	armáda
znovu	znovu	k6eAd1	znovu
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
a	a	k8xC	a
dokázala	dokázat	k5eAaPmAgFnS	dokázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
malé	malý	k2eAgFnPc4d1	malá
mezery	mezera	k1gFnPc4	mezera
v	v	k7c6	v
nepřátelské	přátelský	k2eNgFnSc6d1	nepřátelská
obraně	obrana	k1gFnSc6	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Normané	Norman	k1gMnPc1	Norman
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
těchto	tento	k3xDgFnPc2	tento
mezer	mezera	k1gFnPc2	mezera
využít	využít	k5eAaPmF	využít
a	a	k8xC	a
rozdělit	rozdělit	k5eAaPmF	rozdělit
anglosaskou	anglosaský	k2eAgFnSc4d1	anglosaská
armádu	armáda	k1gFnSc4	armáda
na	na	k7c6	na
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
rytíři	rytíř	k1gMnPc1	rytíř
překonali	překonat	k5eAaPmAgMnP	překonat
obranný	obranný	k2eAgInSc4d1	obranný
val	val	k1gInSc4	val
a	a	k8xC	a
zabili	zabít	k5eAaPmAgMnP	zabít
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
svého	svůj	k3xOyFgMnSc2	svůj
vůdce	vůdce	k1gMnSc2	vůdce
a	a	k8xC	a
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
zabitými	zabitý	k2eAgMnPc7d1	zabitý
důstojníky	důstojník	k1gMnPc7	důstojník
se	se	k3xPyFc4	se
stovky	stovka	k1gFnPc1	stovka
řadových	řadový	k2eAgMnPc2d1	řadový
vojáků	voják	k1gMnPc2	voják
daly	dát	k5eAaPmAgFnP	dát
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Důstojníci	důstojník	k1gMnPc1	důstojník
věrni	věren	k2eAgMnPc1d1	věren
své	svůj	k3xOyFgFnSc6	svůj
přísaze	přísaha	k1gFnSc6	přísaha
králi	král	k1gMnSc3	král
se	se	k3xPyFc4	se
hrdinně	hrdinně	k6eAd1	hrdinně
bili	bít	k5eAaImAgMnP	bít
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nepadli	padnout	k5eNaPmAgMnP	padnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Válečné	válečný	k2eAgNnSc1d1	válečné
pole	pole	k1gNnSc1	pole
bylo	být	k5eAaImAgNnS	být
uklizeno	uklidit	k5eAaPmNgNnS	uklidit
od	od	k7c2	od
mrtvol	mrtvola	k1gFnPc2	mrtvola
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
Vilémův	Vilémův	k2eAgInSc1d1	Vilémův
stan	stan	k1gInSc1	stan
a	a	k8xC	a
pořádána	pořádán	k2eAgFnSc1d1	pořádána
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
večeře	večeře	k1gFnSc1	večeře
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
těžko	těžko	k6eAd1	těžko
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
padlo	padnout	k5eAaPmAgNnS	padnout
asi	asi	k9	asi
5	[number]	k4	5
000	[number]	k4	000
Anglosasů	Anglosas	k1gMnPc2	Anglosas
a	a	k8xC	a
3	[number]	k4	3
000	[number]	k4	000
Normanů	Norman	k1gMnPc2	Norman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důsledky	důsledek	k1gInPc1	důsledek
==	==	k?	==
</s>
</p>
<p>
<s>
Pouze	pouze	k6eAd1	pouze
zbytky	zbytek	k1gInPc1	zbytek
obránců	obránce	k1gMnPc2	obránce
se	se	k3xPyFc4	se
zachránily	zachránit	k5eAaPmAgFnP	zachránit
v	v	k7c6	v
lese	les	k1gInSc6	les
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
bitevního	bitevní	k2eAgNnSc2d1	bitevní
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
Normané	Norman	k1gMnPc1	Norman
je	on	k3xPp3gFnPc4	on
pronásledovali	pronásledovat	k5eAaImAgMnP	pronásledovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
pobiti	pobit	k2eAgMnPc1d1	pobit
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
nechal	nechat	k5eAaPmAgMnS	nechat
své	svůj	k3xOyFgNnSc4	svůj
vojsko	vojsko	k1gNnSc4	vojsko
odpočívat	odpočívat	k5eAaImF	odpočívat
u	u	k7c2	u
Hastingsu	Hastings	k1gInSc6	Hastings
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
a	a	k8xC	a
čekal	čekat	k5eAaImAgMnS	čekat
na	na	k7c4	na
anglosaské	anglosaský	k2eAgMnPc4d1	anglosaský
šlechtice	šlechtic	k1gMnPc4	šlechtic
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přišli	přijít	k5eAaPmAgMnP	přijít
uznat	uznat	k5eAaPmF	uznat
jeho	jeho	k3xOp3gFnSc4	jeho
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
naděje	naděje	k1gFnPc1	naděje
jsou	být	k5eAaImIp3nP	být
marné	marný	k2eAgInPc1d1	marný
<g/>
,	,	kIx,	,
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
na	na	k7c4	na
Londýn	Londýn	k1gInSc4	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Vilémovo	Vilémův	k2eAgNnSc1d1	Vilémovo
vojsko	vojsko	k1gNnSc1	vojsko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
zasaženo	zasáhnout	k5eAaPmNgNnS	zasáhnout
úplavicí	úplavice	k1gFnSc7	úplavice
a	a	k8xC	a
i	i	k9	i
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
vážně	vážně	k6eAd1	vážně
nemocen	nemocen	k2eAgMnSc1d1	nemocen
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
byl	být	k5eAaImAgInS	být
posílen	posílit	k5eAaPmNgInS	posílit
čerstvými	čerstvý	k2eAgFnPc7d1	čerstvá
silami	síla	k1gFnPc7	síla
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dorazily	dorazit	k5eAaPmAgInP	dorazit
přes	přes	k7c4	přes
Lamanšský	lamanšský	k2eAgInSc4d1	lamanšský
průliv	průliv	k1gInSc4	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
neuspěl	uspět	k5eNaPmAgMnS	uspět
s	s	k7c7	s
útokem	útok	k1gInSc7	útok
přes	přes	k7c4	přes
London	London	k1gMnSc1	London
Bridge	Bridg	k1gFnPc4	Bridg
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
města	město	k1gNnPc4	město
oklikou	oklika	k1gFnSc7	oklika
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
překročil	překročit	k5eAaPmAgMnS	překročit
Temži	Temže	k1gFnSc4	Temže
u	u	k7c2	u
Wallingfordu	Wallingford	k1gInSc2	Wallingford
a	a	k8xC	a
postupoval	postupovat	k5eAaImAgInS	postupovat
na	na	k7c4	na
Londýn	Londýn	k1gInSc4	Londýn
ze	z	k7c2	z
severozápadu	severozápad	k1gInSc2	severozápad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrabata	hrabě	k1gNnPc1	hrabě
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
Edwin	Edwin	k1gMnSc1	Edwin
a	a	k8xC	a
Morcar	Morcar	k1gMnSc1	Morcar
<g/>
,	,	kIx,	,
Essegar	Essegar	k1gMnSc1	Essegar
<g/>
,	,	kIx,	,
londýnský	londýnský	k2eAgMnSc1d1	londýnský
šerif	šerif	k1gMnSc1	šerif
a	a	k8xC	a
Edgar	Edgar	k1gMnSc1	Edgar
Atheling	Atheling	k1gInSc1	Atheling
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
po	po	k7c6	po
Haroldově	Haroldův	k2eAgFnSc6d1	Haroldova
smrti	smrt	k1gFnSc6	smrt
králem	král	k1gMnSc7	král
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgMnS	být
korunován	korunovat	k5eAaBmNgMnS	korunovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dostavili	dostavit	k5eAaPmAgMnP	dostavit
k	k	k7c3	k
Vilémovi	Vilém	k1gMnSc3	Vilém
ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
města	město	k1gNnSc2	město
a	a	k8xC	a
podrobili	podrobit	k5eAaPmAgMnP	podrobit
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1066	[number]	k4	1066
byl	být	k5eAaImAgMnS	být
Vilém	Vilém	k1gMnSc1	Vilém
korunován	korunovat	k5eAaBmNgMnS	korunovat
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
jako	jako	k8xS	jako
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Battle	Battle	k1gFnSc1	Battle
of	of	k?	of
Hastings	Hastingsa	k1gFnPc2	Hastingsa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
GRAVETT	GRAVETT	kA	GRAVETT
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
<g/>
.	.	kIx.	.
</s>
<s>
Hastings	Hastings	k1gInSc1	Hastings
1066	[number]	k4	1066
:	:	kIx,	:
pád	pád	k1gInSc1	pád
anglosaské	anglosaský	k2eAgFnSc2d1	anglosaská
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
96	[number]	k4	96
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
2377	[number]	k4	2377
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KODETOVÁ	KODETOVÁ	kA	KODETOVÁ
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
bitvy	bitva	k1gFnPc1	bitva
světových	světový	k2eAgFnPc2d1	světová
dějin	dějiny	k1gFnPc2	dějiny
<g/>
:	:	kIx,	:
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Hastingsu	Hastings	k1gInSc2	Hastings
<g/>
,	,	kIx,	,
1066	[number]	k4	1066
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
:	:	kIx,	:
časopis	časopis	k1gInSc1	časopis
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
dějepisu	dějepis	k1gInSc2	dějepis
a	a	k8xC	a
popularizaci	popularizace	k1gFnSc4	popularizace
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
23	[number]	k4	23
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
185	[number]	k4	185
<g/>
-	-	kIx~	-
<g/>
187	[number]	k4	187
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
