<s>
Erb	erb	k1gInSc1	erb
(	(	kIx(	(
<g/>
z	z	k7c2	z
něm.	něm.	k?	něm.
der	drát	k5eAaImRp2nS	drát
Erbe	erb	k1gInSc5	erb
-	-	kIx~	-
dědic	dědic	k1gMnSc1	dědic
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
das	das	k?	das
Erbe	erb	k1gInSc5	erb
-	-	kIx~	-
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
barevné	barevný	k2eAgNnSc1d1	barevné
grafické	grafický	k2eAgNnSc1d1	grafické
znamení	znamení	k1gNnSc1	znamení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
zejména	zejména	k9	zejména
znakem	znak	k1gInSc7	znak
umístěným	umístěný	k2eAgInSc7d1	umístěný
na	na	k7c6	na
štítě	štít	k1gInSc6	štít
<g/>
.	.	kIx.	.
</s>
<s>
Štít	štít	k1gInSc1	štít
se	se	k3xPyFc4	se
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
povinná	povinný	k2eAgFnSc1d1	povinná
součást	součást	k1gFnSc1	součást
erbu	erb	k1gInSc2	erb
<g/>
.	.	kIx.	.
</s>
<s>
Vlastností	vlastnost	k1gFnSc7	vlastnost
erbu	erb	k1gInSc2	erb
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
dědičnost	dědičnost	k1gFnSc1	dědičnost
a	a	k8xC	a
stálost	stálost	k1gFnSc1	stálost
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
erbů	erb	k1gInPc2	erb
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
heraldika	heraldika	k1gFnSc1	heraldika
<g/>
.	.	kIx.	.
</s>
<s>
Heraldika	heraldika	k1gFnSc1	heraldika
také	také	k9	také
popisuje	popisovat	k5eAaImIp3nS	popisovat
způsob	způsob	k1gInSc1	způsob
vytváření	vytváření	k1gNnSc2	vytváření
<g/>
,	,	kIx,	,
význam	význam	k1gInSc1	význam
a	a	k8xC	a
užití	užití	k1gNnSc1	užití
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Erb	erb	k1gInSc1	erb
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
zejména	zejména	k9	zejména
u	u	k7c2	u
středověkých	středověký	k2eAgMnPc2d1	středověký
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
označovali	označovat	k5eAaImAgMnP	označovat
své	svůj	k3xOyFgNnSc4	svůj
brnění	brnění	k1gNnSc4	brnění
a	a	k8xC	a
přilbu	přilba	k1gFnSc4	přilba
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
stále	stále	k6eAd1	stále
dokonalejší	dokonalý	k2eAgFnSc7d2	dokonalejší
výzbrojí	výzbroj	k1gFnSc7	výzbroj
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pokrývala	pokrývat	k5eAaImAgFnS	pokrývat
celé	celý	k2eAgNnSc4d1	celé
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
obtížnější	obtížný	k2eAgNnSc1d2	obtížnější
rychle	rychle	k6eAd1	rychle
rozeznat	rozeznat	k5eAaPmF	rozeznat
spojence	spojenec	k1gMnPc4	spojenec
od	od	k7c2	od
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
zavedeny	zaveden	k2eAgInPc1d1	zaveden
barevné	barevný	k2eAgInPc1d1	barevný
symboly	symbol	k1gInPc1	symbol
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
umisťovaly	umisťovat	k5eAaImAgInP	umisťovat
na	na	k7c6	na
brnění	brnění	k1gNnSc6	brnění
<g/>
,	,	kIx,	,
štíty	štít	k1gInPc4	štít
a	a	k8xC	a
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Používaly	používat	k5eAaImAgInP	používat
se	se	k3xPyFc4	se
jednoznačné	jednoznačný	k2eAgInPc1d1	jednoznačný
a	a	k8xC	a
kontrastní	kontrastní	k2eAgInPc1d1	kontrastní
barvy	barva	k1gFnPc4	barva
a	a	k8xC	a
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
dobře	dobře	k6eAd1	dobře
rozlišitelné	rozlišitelný	k2eAgInPc1d1	rozlišitelný
i	i	k9	i
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
a	a	k8xC	a
umožňovaly	umožňovat	k5eAaImAgInP	umožňovat
rychlou	rychlý	k2eAgFnSc4d1	rychlá
orientaci	orientace	k1gFnSc4	orientace
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
a	a	k8xC	a
pomáhaly	pomáhat	k5eAaImAgFnP	pomáhat
rychle	rychle	k6eAd1	rychle
rozlišit	rozlišit	k5eAaPmF	rozlišit
spojence	spojenec	k1gMnSc4	spojenec
a	a	k8xC	a
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Erby	erb	k1gInPc1	erb
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
objevily	objevit	k5eAaPmAgFnP	objevit
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
době	doba	k1gFnSc6	doba
křižáckých	křižácký	k2eAgFnPc2d1	křižácká
výprav	výprava	k1gFnPc2	výprava
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
určité	určitý	k2eAgNnSc1d1	určité
dědičné	dědičný	k2eAgNnSc1d1	dědičné
rozlišování	rozlišování	k1gNnSc1	rozlišování
pomocí	pomocí	k7c2	pomocí
štítů	štít	k1gInPc2	štít
známe	znát	k5eAaImIp1nP	znát
již	již	k9	již
ze	z	k7c2	z
starověkých	starověký	k2eAgFnPc2d1	starověká
Atén	Atény	k1gFnPc2	Atény
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
standardizované	standardizovaný	k2eAgFnPc1d1	standardizovaná
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
vlastní	vlastní	k2eAgFnSc4d1	vlastní
terminologii	terminologie	k1gFnSc4	terminologie
<g/>
.	.	kIx.	.
</s>
<s>
Slovní	slovní	k2eAgInSc4d1	slovní
popis	popis	k1gInSc4	popis
erbu	erb	k1gInSc2	erb
sestavený	sestavený	k2eAgInSc4d1	sestavený
podle	podle	k7c2	podle
heraldických	heraldický	k2eAgNnPc2d1	heraldické
pravidel	pravidlo	k1gNnPc2	pravidlo
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
blason	blason	k1gInSc1	blason
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
existuje	existovat	k5eAaImIp3nS	existovat
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
erb	erb	k1gInSc4	erb
i	i	k8xC	i
nějaký	nějaký	k3yIgInSc4	nějaký
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
význam	význam	k1gInSc4	význam
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
erbu	erb	k1gInSc2	erb
použita	použit	k2eAgNnPc1d1	použito
zvířata	zvíře	k1gNnPc1	zvíře
a	a	k8xC	a
symboly	symbol	k1gInPc1	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
legendy	legenda	k1gFnPc1	legenda
byly	být	k5eAaImAgFnP	být
obvykle	obvykle	k6eAd1	obvykle
přidány	přidat	k5eAaPmNgInP	přidat
až	až	k9	až
dodatečně	dodatečně	k6eAd1	dodatečně
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
erb	erb	k1gInSc1	erb
"	"	kIx"	"
<g/>
přečíst	přečíst	k5eAaPmF	přečíst
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
barvy	barva	k1gFnPc1	barva
a	a	k8xC	a
symboly	symbol	k1gInPc1	symbol
napoví	napovědět	k5eAaPmIp3nS	napovědět
jméno	jméno	k1gNnSc4	jméno
jeho	on	k3xPp3gMnSc2	on
nositele	nositel	k1gMnSc2	nositel
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
erb	erb	k1gInSc1	erb
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
mluvící	mluvící	k2eAgInSc1d1	mluvící
erb	erb	k1gInSc1	erb
<g/>
.	.	kIx.	.
</s>
<s>
Erb	erb	k1gInSc1	erb
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
částí	část	k1gFnPc2	část
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
heraldika	heraldika	k1gFnSc1	heraldika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Štít	štít	k1gInSc1	štít
je	být	k5eAaImIp3nS	být
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
součástí	součást	k1gFnSc7	součást
erbu	erb	k1gInSc2	erb
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jedinou	jediný	k2eAgFnSc4d1	jediná
povinnou	povinný	k2eAgFnSc4d1	povinná
součást	součást	k1gFnSc4	součást
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
kreslen	kreslen	k2eAgMnSc1d1	kreslen
buď	buď	k8xC	buď
kolmo	kolmo	k6eAd1	kolmo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mírně	mírně	k6eAd1	mírně
šikmo	šikmo	k6eAd1	šikmo
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
správnější	správní	k2eAgNnSc4d2	správnější
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
to	ten	k3xDgNnSc1	ten
více	hodně	k6eAd2	hodně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
nošení	nošení	k1gNnSc4	nošení
štítů	štít	k1gInPc2	štít
rytíři	rytíř	k1gMnPc1	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
štítu	štít	k1gInSc2	štít
se	se	k3xPyFc4	se
v	v	k7c6	v
blasonu	blason	k1gInSc6	blason
nepopisuje	popisovat	k5eNaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
provedení	provedení	k1gNnSc4	provedení
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
závislé	závislý	k2eAgFnSc2d1	závislá
na	na	k7c6	na
kreslíři	kreslíř	k1gMnSc6	kreslíř
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
respektovat	respektovat	k5eAaImF	respektovat
určitá	určitý	k2eAgNnPc4d1	určité
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
respektovat	respektovat	k5eAaImF	respektovat
umělecký	umělecký	k2eAgInSc4d1	umělecký
sloh	sloh	k1gInSc4	sloh
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
též	též	k9	též
provenienci	provenience	k1gFnSc6	provenience
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
ženské	ženský	k2eAgInPc4d1	ženský
erby	erb	k1gInPc4	erb
vyhrazeny	vyhrazen	k2eAgInPc4d1	vyhrazen
odlišné	odlišný	k2eAgInPc4d1	odlišný
tvary	tvar	k1gInPc4	tvar
štítů	štít	k1gInPc2	štít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zdůraznila	zdůraznit	k5eAaPmAgFnS	zdůraznit
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neúčastnily	účastnit	k5eNaImAgFnP	účastnit
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
štít	štít	k1gInSc4	štít
routový	routový	k2eAgInSc1d1	routový
a	a	k8xC	a
oválný	oválný	k2eAgInSc1d1	oválný
(	(	kIx(	(
<g/>
Belgie	Belgie	k1gFnSc1	Belgie
a	a	k8xC	a
severní	severní	k2eAgFnSc1d1	severní
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
zemích	zem	k1gFnPc6	zem
nebylo	být	k5eNaImAgNnS	být
takovéto	takovýto	k3xDgNnSc1	takovýto
odlišování	odlišování	k1gNnSc1	odlišování
používáno	používán	k2eAgNnSc1d1	používáno
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Přilba	přilba	k1gFnSc1	přilba
(	(	kIx(	(
<g/>
heraldika	heraldika	k1gFnSc1	heraldika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Přikryvadla	Přikryvadlo	k1gNnSc2	Přikryvadlo
<g/>
.	.	kIx.	.
</s>
<s>
Křížové	Kříž	k1gMnPc1	Kříž
výpravy	výprava	k1gFnSc2	výprava
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
přinesly	přinést	k5eAaPmAgFnP	přinést
doplněk	doplněk	k1gInSc4	doplněk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
též	též	k9	též
stal	stát	k5eAaPmAgMnS	stát
součástí	součást	k1gFnSc7	součást
erbu	erb	k1gInSc2	erb
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
přikryvadla	přikryvadlo	k1gNnPc1	přikryvadlo
(	(	kIx(	(
<g/>
též	též	k9	též
staročesky	staročesky	k6eAd1	staročesky
fafrnochy	fafrnoch	k1gInPc1	fafrnoch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kryla	krýt	k5eAaImAgFnS	krýt
zadní	zadní	k2eAgFnSc4d1	zadní
část	část	k1gFnSc4	část
přilby	přilba	k1gFnSc2	přilba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nepřehřívala	přehřívat	k5eNaImAgFnS	přehřívat
od	od	k7c2	od
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Přikrývadla	přikrývadlo	k1gNnPc1	přikrývadlo
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
barvách	barva	k1gFnPc6	barva
erbu	erb	k1gInSc2	erb
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc2	jejich
kraje	kraj	k1gInSc2	kraj
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
dobách	doba	k1gFnPc6	doba
ozdobně	ozdobně	k6eAd1	ozdobně
rozstříhány	rozstříhán	k2eAgMnPc4d1	rozstříhán
do	do	k7c2	do
cípů	cíp	k1gInPc2	cíp
(	(	kIx(	(
<g/>
četné	četný	k2eAgFnPc1d1	četná
nepřítelovy	nepřítelův	k2eAgFnPc1d1	nepřítelova
rány	rána	k1gFnPc1	rána
a	a	k8xC	a
údery	úder	k1gInPc1	úder
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přikrývadla	přikrývadlo	k1gNnSc2	přikrývadlo
pochází	pocházet	k5eAaImIp3nS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
od	od	k7c2	od
Saracénů	Saracén	k1gMnPc2	Saracén
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Točenice	točenice	k1gFnSc2	točenice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
heraldika	heraldika	k1gFnSc1	heraldika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Točenice	točenice	k1gFnPc1	točenice
a	a	k8xC	a
helmovní	helmovní	k2eAgFnPc1d1	helmovní
koruny	koruna	k1gFnPc1	koruna
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
zakrytí	zakrytí	k1gNnSc3	zakrytí
upevnění	upevnění	k1gNnSc2	upevnění
klenotu	klenot	k1gInSc2	klenot
k	k	k7c3	k
přilbě	přilba	k1gFnSc3	přilba
(	(	kIx(	(
<g/>
helmě	helma	k1gFnSc3	helma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
používána	používat	k5eAaImNgFnS	používat
jen	jen	k9	jen
na	na	k7c6	na
královských	královský	k2eAgInPc6d1	královský
erbech	erb	k1gInPc6	erb
<g/>
,	,	kIx,	,
až	až	k9	až
později	pozdě	k6eAd2	pozdě
i	i	k9	i
na	na	k7c6	na
šlechtických	šlechtický	k2eAgFnPc6d1	šlechtická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
přestává	přestávat	k5eAaImIp3nS	přestávat
používat	používat	k5eAaImF	používat
točenice	točenice	k1gFnSc1	točenice
a	a	k8xC	a
erby	erb	k1gInPc1	erb
jsou	být	k5eAaImIp3nP	být
udělovány	udělovat	k5eAaImNgInP	udělovat
již	již	k6eAd1	již
výhradně	výhradně	k6eAd1	výhradně
s	s	k7c7	s
korunou	koruna	k1gFnSc7	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
hodnostní	hodnostní	k2eAgFnSc2d1	hodnostní
koruny	koruna	k1gFnSc2	koruna
tedy	tedy	k9	tedy
nevyjadřují	vyjadřovat	k5eNaImIp3nP	vyjadřovat
žádnou	žádný	k3yNgFnSc4	žádný
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Klenot	klenot	k1gInSc1	klenot
(	(	kIx(	(
<g/>
heraldika	heraldika	k1gFnSc1	heraldika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klenot	klenot	k1gInSc1	klenot
je	být	k5eAaImIp3nS	být
ozdoba	ozdoba	k1gFnSc1	ozdoba
na	na	k7c6	na
přilbě	přilba	k1gFnSc6	přilba
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
opakuje	opakovat	k5eAaImIp3nS	opakovat
některou	některý	k3yIgFnSc4	některý
z	z	k7c2	z
figur	figura	k1gFnPc2	figura
štítu	štít	k1gInSc2	štít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
části	část	k1gFnPc1	část
erbu	erb	k1gInSc2	erb
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nebyly	být	k5eNaImAgInP	být
v	v	k7c6	v
boji	boj	k1gInSc6	boj
funkční	funkční	k2eAgFnSc2d1	funkční
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
přidány	přidat	k5eAaPmNgInP	přidat
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
hodnost	hodnost	k1gFnSc4	hodnost
a	a	k8xC	a
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Erbovní	erbovní	k2eAgInSc4d1	erbovní
plášť	plášť	k1gInSc4	plášť
a	a	k8xC	a
stan	stan	k1gInSc4	stan
<g/>
.	.	kIx.	.
</s>
<s>
Plášť	plášť	k1gInSc1	plášť
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
hodnostní	hodnostní	k2eAgFnSc2d1	hodnostní
koruny	koruna	k1gFnSc2	koruna
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
zbytek	zbytek	k1gInSc1	zbytek
erbu	erb	k1gInSc2	erb
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
plášti	plášť	k1gInSc6	plášť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozích	roh	k1gInPc6	roh
byl	být	k5eAaImAgInS	být
převázán	převázat	k5eAaPmNgInS	převázat
šňůrkami	šňůrka	k1gFnPc7	šňůrka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
počet	počet	k1gInSc1	počet
symbolizoval	symbolizovat	k5eAaImAgInS	symbolizovat
postavení	postavení	k1gNnSc4	postavení
držitele	držitel	k1gMnSc2	držitel
erbu	erb	k1gInSc2	erb
(	(	kIx(	(
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
<g/>
,	,	kIx,	,
2	[number]	k4	2
a	a	k8xC	a
půl	půl	k1xP	půl
náleželo	náležet	k5eAaImAgNnS	náležet
císaři	císař	k1gMnSc6	císař
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stranách	strana	k1gFnPc6	strana
pláště	plášť	k1gInSc2	plášť
byly	být	k5eAaImAgInP	být
obvykle	obvykle	k6eAd1	obvykle
záhyby	záhyba	k1gFnPc4	záhyba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
také	také	k9	také
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
hodnost	hodnost	k1gFnSc4	hodnost
majitele	majitel	k1gMnSc2	majitel
(	(	kIx(	(
<g/>
tři	tři	k4xCgFnPc1	tři
náležely	náležet	k5eAaImAgFnP	náležet
císaři	císař	k1gMnSc5	císař
<g/>
/	/	kIx~	/
králi	král	k1gMnSc6	král
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plášť	plášť	k1gInSc4	plášť
obvykle	obvykle	k6eAd1	obvykle
používala	používat	k5eAaImAgFnS	používat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Stan	stan	k1gInSc1	stan
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
podobný	podobný	k2eAgInSc4d1	podobný
plášti	plášť	k1gInSc3	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Erb	erb	k1gInSc1	erb
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
svou	svůj	k3xOyFgFnSc7	svůj
ozdobnou	ozdobný	k2eAgFnSc7d1	ozdobná
kopulí	kopule	k1gFnSc7	kopule
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
usazovala	usazovat	k5eAaImAgFnS	usazovat
hodnostní	hodnostní	k2eAgFnSc1d1	hodnostní
nebo	nebo	k8xC	nebo
panovníkova	panovníkův	k2eAgFnSc1d1	panovníkova
korunka	korunka	k1gFnSc1	korunka
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
ho	on	k3xPp3gMnSc4	on
používaly	používat	k5eAaImAgInP	používat
jenom	jenom	k9	jenom
svrchovaní	svrchovaný	k2eAgMnPc1d1	svrchovaný
panovníci	panovník	k1gMnPc1	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Štítonoš	štítonoš	k1gMnSc1	štítonoš
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
figury	figura	k1gFnPc4	figura
nesoucí	nesoucí	k2eAgInSc1d1	nesoucí
štít	štít	k1gInSc1	štít
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
bývají	bývat	k5eAaImIp3nP	bývat
dvě	dva	k4xCgNnPc4	dva
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
štítu	štít	k1gInSc2	štít
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
jedna	jeden	k4xCgFnSc1	jeden
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Korouhve	korouhev	k1gFnSc2	korouhev
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Motto	motto	k1gNnSc1	motto
<g/>
.	.	kIx.	.
</s>
<s>
Heslo	heslo	k1gNnSc1	heslo
(	(	kIx(	(
<g/>
motto	motto	k1gNnSc1	motto
<g/>
,	,	kIx,	,
devíza	devíza	k1gFnSc1	devíza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovní	slovní	k2eAgNnSc4d1	slovní
spojení	spojení	k1gNnSc4	spojení
na	na	k7c6	na
stuze	stuha	k1gFnSc6	stuha
umisťované	umisťovaný	k2eAgFnSc6d1	umisťovaná
pod	pod	k7c7	pod
štítem	štít	k1gInSc7	štít
<g/>
.	.	kIx.	.
</s>
<s>
Vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
rodovou	rodový	k2eAgFnSc4d1	rodová
tradici	tradice	k1gFnSc4	tradice
nebo	nebo	k8xC	nebo
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
panovníku	panovník	k1gMnSc3	panovník
či	či	k8xC	či
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Bojový	bojový	k2eAgInSc1d1	bojový
pokřik	pokřik	k1gInSc1	pokřik
je	být	k5eAaImIp3nS	být
slovní	slovní	k2eAgNnSc4d1	slovní
spojení	spojení	k1gNnSc4	spojení
na	na	k7c6	na
stuze	stuha	k1gFnSc6	stuha
umisťované	umisťovaný	k2eAgFnSc6d1	umisťovaná
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
erbu	erb	k1gInSc2	erb
<g/>
.	.	kIx.	.
</s>
<s>
Půda	půda	k1gFnSc1	půda
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
porostlá	porostlý	k2eAgFnSc1d1	porostlá
zeleným	zelený	k2eAgInSc7d1	zelený
pažitem	pažit	k1gInSc7	pažit
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
období	období	k1gNnSc2	období
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
umisťována	umisťovat	k5eAaImNgFnS	umisťovat
pod	pod	k7c7	pod
štítem	štít	k1gInSc7	štít
<g/>
.	.	kIx.	.
</s>
<s>
Trofeje	trofej	k1gFnPc1	trofej
jsou	být	k5eAaImIp3nP	být
nahromaděné	nahromaděný	k2eAgInPc4d1	nahromaděný
válečné	válečný	k2eAgInPc4d1	válečný
předměty	předmět	k1gInPc4	předmět
naznačující	naznačující	k2eAgInPc4d1	naznačující
válečné	válečný	k2eAgInPc4d1	válečný
úspěchy	úspěch	k1gInPc4	úspěch
erbovníka	erbovník	k1gMnSc2	erbovník
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
děla	dělo	k1gNnPc4	dělo
<g/>
,	,	kIx,	,
hmoždíře	hmoždíř	k1gInPc4	hmoždíř
<g/>
,	,	kIx,	,
hromady	hromada	k1gFnPc4	hromada
kulí	kule	k1gFnPc2	kule
<g/>
,	,	kIx,	,
bubny	buben	k1gInPc4	buben
<g/>
,	,	kIx,	,
trubky	trubka	k1gFnPc4	trubka
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
prapory	prapor	k1gInPc4	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Řádové	řádový	k2eAgFnSc2d1	řádová
dekorace	dekorace	k1gFnSc2	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
Řetězy	řetěz	k1gInPc1	řetěz
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
majitel	majitel	k1gMnSc1	majitel
erbu	erb	k1gInSc2	erb
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
získal	získat	k5eAaPmAgInS	získat
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
nechat	nechat	k5eAaPmF	nechat
obtáčet	obtáčet	k5eAaImF	obtáčet
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
erbu	erb	k1gInSc2	erb
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejdůležitější	důležitý	k2eAgInSc4d3	nejdůležitější
řád	řád	k1gInSc4	řád
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
nejblíže	blízce	k6eAd3	blízce
štítu	štít	k1gInSc2	štít
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
neděditelnosti	neděditelnost	k1gFnSc3	neděditelnost
většiny	většina	k1gFnSc2	většina
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc4	tento
úpravy	úprava	k1gFnPc4	úprava
erbu	erb	k1gInSc2	erb
<g/>
/	/	kIx~	/
<g/>
znaku	znak	k1gInSc2	znak
byly	být	k5eAaImAgFnP	být
také	také	k9	také
neděditelné	děditelný	k2eNgFnPc1d1	děditelný
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hodnostní	hodnostní	k2eAgInPc4d1	hodnostní
odznaky	odznak	k1gInPc4	odznak
<g/>
.	.	kIx.	.
</s>
<s>
Hodnostní	hodnostní	k2eAgInPc4d1	hodnostní
odznaky	odznak	k1gInPc4	odznak
neboli	neboli	k8xC	neboli
odznaky	odznak	k1gInPc4	odznak
úřadu	úřad	k1gInSc2	úřad
používá	používat	k5eAaImIp3nS	používat
výhradně	výhradně	k6eAd1	výhradně
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
daný	daný	k2eAgInSc4d1	daný
úřad	úřad	k1gInSc4	úřad
zastává	zastávat	k5eAaImIp3nS	zastávat
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
tedy	tedy	k9	tedy
dědičné	dědičný	k2eAgNnSc4d1	dědičné
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hodnostní	hodnostní	k2eAgFnSc2d1	hodnostní
koruny	koruna	k1gFnSc2	koruna
a	a	k8xC	a
klobouky	klobouk	k1gInPc4	klobouk
<g/>
.	.	kIx.	.
</s>
<s>
Klobouky	Klobouky	k1gInPc1	Klobouky
umístěné	umístěný	k2eAgInPc1d1	umístěný
nad	nad	k7c4	nad
štít	štít	k1gInSc4	štít
používali	používat	k5eAaImAgMnP	používat
církevní	církevní	k2eAgMnPc1d1	církevní
hodnostáři	hodnostář	k1gMnPc1	hodnostář
<g/>
.	.	kIx.	.
</s>
<s>
Hodnost	hodnost	k1gFnSc1	hodnost
majitele	majitel	k1gMnPc4	majitel
určuje	určovat	k5eAaImIp3nS	určovat
barva	barva	k1gFnSc1	barva
klobouku	klobouk	k1gInSc2	klobouk
a	a	k8xC	a
počet	počet	k1gInSc1	počet
střapců	střapec	k1gInPc2	střapec
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
štítu	štít	k1gInSc2	štít
<g/>
.	.	kIx.	.
</s>
<s>
Kardinál	kardinál	k1gMnSc1	kardinál
měl	mít	k5eAaImAgMnS	mít
červený	červený	k2eAgInSc4d1	červený
klobouk	klobouk	k1gInSc4	klobouk
a	a	k8xC	a
patnáct	patnáct	k4xCc4	patnáct
střapců	střapec	k1gInPc2	střapec
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
štítu	štít	k1gInSc2	štít
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
měl	mít	k5eAaImAgInS	mít
klobouk	klobouk	k1gInSc4	klobouk
zelený	zelený	k2eAgInSc1d1	zelený
a	a	k8xC	a
deset	deset	k4xCc1	deset
střapců	střapec	k1gInPc2	střapec
<g/>
.	.	kIx.	.
</s>
<s>
Hodnostní	hodnostní	k2eAgFnPc1d1	hodnostní
koruny	koruna	k1gFnPc1	koruna
se	se	k3xPyFc4	se
kladou	klást	k5eAaImIp3nP	klást
bezprostředně	bezprostředně	k6eAd1	bezprostředně
na	na	k7c4	na
horní	horní	k2eAgFnSc4d1	horní
hranu	hrana	k1gFnSc4	hrana
štítu	štít	k1gInSc2	štít
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
nebo	nebo	k8xC	nebo
až	až	k6eAd1	až
nahoru	nahoru	k6eAd1	nahoru
na	na	k7c4	na
plášť	plášť	k1gInSc4	plášť
či	či	k8xC	či
stan	stan	k1gInSc4	stan
<g/>
.	.	kIx.	.
</s>
<s>
Heraldika	heraldika	k1gFnSc1	heraldika
Genealogie	genealogie	k1gFnSc2	genealogie
Rodokmen	rodokmen	k1gInSc1	rodokmen
</s>
