<p>
<s>
Sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
je	být	k5eAaImIp3nS	být
proud	proud	k1gInSc4	proud
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
rychlost	rychlost	k1gFnSc4	rychlost
asi	asi	k9	asi
450	[number]	k4	450
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
‰	‰	k?	‰
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
-li	i	k?	-li
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
hvězd	hvězda	k1gFnPc2	hvězda
než	než	k8xS	než
z	z	k7c2	z
našeho	náš	k3xOp1gNnSc2	náš
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
hvězdný	hvězdný	k2eAgInSc1d1	hvězdný
vítr	vítr	k1gInSc1	vítr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
je	být	k5eAaImIp3nS	být
sluneční	sluneční	k2eAgFnSc1d1	sluneční
korona	korona	k1gFnSc1	korona
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
že	že	k8xS	že
zdejší	zdejší	k2eAgFnPc1d1	zdejší
částice	částice	k1gFnPc1	částice
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
sluneční	sluneční	k2eAgFnSc2d1	sluneční
gravitace	gravitace	k1gFnSc2	gravitace
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
udržet	udržet	k5eAaPmF	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
však	však	k9	však
není	být	k5eNaImIp3nS	být
objasněn	objasněn	k2eAgInSc1d1	objasněn
mechanismus	mechanismus	k1gInSc1	mechanismus
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
částice	částice	k1gFnPc1	částice
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
urychlovány	urychlovat	k5eAaImNgFnP	urychlovat
na	na	k7c6	na
tak	tak	k6eAd1	tak
vysokou	vysoký	k2eAgFnSc4d1	vysoká
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc1	složení
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
==	==	k?	==
</s>
</p>
<p>
<s>
Sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
protony	proton	k1gInPc1	proton
</s>
</p>
<p>
<s>
částice	částice	k1gFnSc1	částice
alfa	alfa	k1gNnSc2	alfa
(	(	kIx(	(
<g/>
jádra	jádro	k1gNnSc2	jádro
hélia	hélium	k1gNnSc2	hélium
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
elektrony	elektron	k1gInPc1	elektron
</s>
</p>
<p>
<s>
==	==	k?	==
Vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Intenzita	intenzita	k1gFnSc1	intenzita
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
po	po	k7c6	po
velkých	velký	k2eAgFnPc6d1	velká
slunečních	sluneční	k2eAgFnPc6d1	sluneční
erupcích	erupce	k1gFnPc6	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
interaguje	interagovat	k5eAaBmIp3nS	interagovat
s	s	k7c7	s
magnetickými	magnetický	k2eAgNnPc7d1	magnetické
poli	pole	k1gNnPc7	pole
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
komet	kometa	k1gFnPc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
ionizaci	ionizace	k1gFnSc4	ionizace
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
výskytem	výskyt	k1gInSc7	výskyt
polární	polární	k2eAgFnSc2d1	polární
záře	zář	k1gFnSc2	zář
<g/>
,	,	kIx,	,
poruchou	porucha	k1gFnSc7	porucha
příjmu	příjem	k1gInSc2	příjem
na	na	k7c6	na
krátkých	krátký	k2eAgFnPc6d1	krátká
rádiových	rádiový	k2eAgFnPc6d1	rádiová
vlnách	vlna	k1gFnPc6	vlna
(	(	kIx(	(
<g/>
Dellingerův	Dellingerův	k2eAgInSc1d1	Dellingerův
efekt	efekt	k1gInSc1	efekt
<g/>
)	)	kIx)	)
či	či	k8xC	či
kolísáním	kolísání	k1gNnSc7	kolísání
a	a	k8xC	a
výpadky	výpadek	k1gInPc7	výpadek
v	v	k7c6	v
elektrické	elektrický	k2eAgFnSc6d1	elektrická
síti	síť	k1gFnSc6	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Planeta	planeta	k1gFnSc1	planeta
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
částečně	částečně	k6eAd1	částečně
chráněna	chránit	k5eAaImNgFnS	chránit
svým	svůj	k3xOyFgNnSc7	svůj
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
(	(	kIx(	(
<g/>
popis	popis	k1gInSc1	popis
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
detektoru	detektor	k1gInSc2	detektor
částic	částice	k1gFnPc2	částice
na	na	k7c6	na
sondě	sonda	k1gFnSc6	sonda
NASA	NASA	kA	NASA
Polar	Polar	k1gInSc1	Polar
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k9	rovněž
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
únik	únik	k1gInSc4	únik
plynů	plyn	k1gInPc2	plyn
ze	z	k7c2	z
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
pro	pro	k7c4	pro
kosmické	kosmický	k2eAgNnSc4d1	kosmické
cestování	cestování	k1gNnSc4	cestování
==	==	k?	==
</s>
</p>
<p>
<s>
Sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
nehraje	hrát	k5eNaImIp3nS	hrát
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
žádnou	žádný	k3yNgFnSc4	žádný
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
připravované	připravovaný	k2eAgFnPc4d1	připravovaná
sluneční	sluneční	k2eAgFnPc4d1	sluneční
plachetnice	plachetnice	k1gFnPc4	plachetnice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gInSc1	jeho
tlak	tlak	k1gInSc1	tlak
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgMnSc1d1	malý
oproti	oproti	k7c3	oproti
tlaku	tlak	k1gInSc3	tlak
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
asi	asi	k9	asi
10	[number]	k4	10
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
krát	krát	k6eAd1	krát
menší	malý	k2eAgInPc1d2	menší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výzkumné	výzkumný	k2eAgFnPc4d1	výzkumná
družice	družice	k1gFnPc4	družice
==	==	k?	==
</s>
</p>
<p>
<s>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
bylo	být	k5eAaImAgNnS	být
prováděno	provádět	k5eAaImNgNnS	provádět
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
obsáhlejšího	obsáhlý	k2eAgInSc2d2	obsáhlejší
vědeckého	vědecký	k2eAgInSc2d1	vědecký
programu	program	k1gInSc2	program
na	na	k7c6	na
družicích	družice	k1gFnPc6	družice
HEOS	HEOS	kA	HEOS
(	(	kIx(	(
<g/>
ESRO	ESRO	kA	ESRO
<g/>
)	)	kIx)	)
a	a	k8xC	a
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
družic	družice	k1gFnPc2	družice
USA	USA	kA	USA
Explorer	Explorer	k1gInSc1	Explorer
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
probíhal	probíhat	k5eAaImAgInS	probíhat
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
Apollo	Apollo	k1gMnSc1	Apollo
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Seznam	seznam	k1gInSc1	seznam
družic	družice	k1gFnPc2	družice
monitorujících	monitorující	k2eAgFnPc2d1	monitorující
sluneční	sluneční	k2eAgFnSc4d1	sluneční
aktivitu	aktivita	k1gFnSc4	aktivita
===	===	k?	===
</s>
</p>
<p>
<s>
Aktivní	aktivní	k2eAgFnSc1d1	aktivní
družice	družice	k1gFnSc1	družice
</s>
</p>
<p>
<s>
SOHO	SOHO	kA	SOHO
(	(	kIx(	(
<g/>
Solar	Solar	k1gMnSc1	Solar
and	and	k?	and
Heliospheric	Heliospheric	k1gMnSc1	Heliospheric
Observatory	Observator	k1gInPc7	Observator
<g/>
)	)	kIx)	)
–	–	k?	–
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
ACE	ACE	kA	ACE
(	(	kIx(	(
<g/>
Advanced	Advanced	k1gInSc1	Advanced
Composition	Composition	k1gInSc1	Composition
Explorer	Explorer	k1gInSc4	Explorer
<g/>
)	)	kIx)	)
–	–	k?	–
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
STEREO	stereo	k6eAd1	stereo
A	a	k9	a
<g/>
,	,	kIx,	,
STEREO	stereo	k6eAd1	stereo
B	B	kA	B
(	(	kIx(	(
<g/>
Solar	Solar	k1gMnSc1	Solar
Terrestrial	Terrestrial	k1gMnSc1	Terrestrial
Relations	Relations	k1gInSc1	Relations
Observatory	Observator	k1gInPc1	Observator
<g/>
)	)	kIx)	)
–	–	k?	–
od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
SDO	SDO	kA	SDO
(	(	kIx(	(
<g/>
Solar	Solar	k1gInSc1	Solar
Dynamics	Dynamics	k1gInSc1	Dynamics
Observatory	Observator	k1gInPc1	Observator
<g/>
)	)	kIx)	)
–	–	k?	–
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
GOES	GOES	kA	GOES
(	(	kIx(	(
<g/>
Geostationary	Geostationara	k1gFnSc2	Geostationara
Operational	Operational	k1gMnSc1	Operational
Environmental	Environmental	k1gMnSc1	Environmental
Satellites	Satellites	k1gMnSc1	Satellites
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WIND	WIND	kA	WIND
–	–	k?	–
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Parker	Parker	k1gMnSc1	Parker
Solar	Solar	k1gMnSc1	Solar
Probe	Prob	k1gInSc5	Prob
-	-	kIx~	-
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2018	[number]	k4	2018
(	(	kIx(	(
<g/>
start	start	k1gInSc1	start
<g/>
)	)	kIx)	)
<g/>
Neaktivní	aktivní	k2eNgFnSc1d1	neaktivní
družice	družice	k1gFnSc1	družice
</s>
</p>
<p>
<s>
Ulysses	Ulysses	k1gMnSc1	Ulysses
–	–	k?	–
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
Genesis	Genesis	k1gFnSc1	Genesis
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2001	[number]	k4	2001
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
TRACE	TRACE	kA	TRACE
(	(	kIx(	(
<g/>
Transition	Transition	k1gInSc1	Transition
Region	region	k1gInSc1	region
and	and	k?	and
Coronal	Coronal	k1gMnSc1	Coronal
Explorer	Explorer	k1gMnSc1	Explorer
<g/>
)	)	kIx)	)
–	–	k?	–
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1998	[number]	k4	1998
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
LÁLA	Lála	k1gMnSc1	Lála
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
VÍTEK	Vítek	k1gMnSc1	Vítek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Slunce	slunce	k1gNnSc1	slunce
</s>
</p>
<p>
<s>
Wolf-Rayetova	Wolf-Rayetův	k2eAgFnSc1d1	Wolf-Rayetova
hvězda	hvězda	k1gFnSc1	hvězda
</s>
</p>
<p>
<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
plachetnice	plachetnice	k1gFnSc1	plachetnice
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sluneční	sluneční	k2eAgInSc4d1	sluneční
vítr	vítr	k1gInSc4	vítr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
NASA	NASA	kA	NASA
-	-	kIx~	-
sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
</s>
</p>
<p>
<s>
Námořní	námořní	k2eAgNnSc1d1	námořní
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
-	-	kIx~	-
sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
zpráva	zpráva	k1gFnSc1	zpráva
NASA	NASA	kA	NASA
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
zprávy	zpráva	k1gFnSc2	zpráva
NASA	NASA	kA	NASA
</s>
</p>
