<s>
Kostel	kostel	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
Engrácie	Engrácie	k1gFnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
Engrácie	Engrácie	k1gFnSc2
/	/	kIx~
Igreja	Igreja	k1gMnSc1
de	de	k?
Santa	Santa	k1gMnSc1
Engrácia	Engrácium	k1gNnSc2
Kostel	kostel	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
EngrácieMísto	EngrácieMísta	k1gMnSc5
Stát	stát	k1gInSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
Portugalsko	Portugalsko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
38	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
54	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
9	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Církev	církev	k1gFnSc1
</s>
<s>
římskokatolická	římskokatolický	k2eAgFnSc1d1
Status	status	k1gInSc1
</s>
<s>
Kostel	kostel	k1gInSc1
Architektonický	architektonický	k2eAgInSc4d1
popis	popis	k1gInSc4
Stavební	stavební	k2eAgInSc1d1
sloh	sloh	k1gInSc1
</s>
<s>
baroko	baroko	k1gNnSc1
Typ	typa	k1gFnPc2
stavby	stavba	k1gFnSc2
</s>
<s>
kostel	kostel	k1gInSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1682-1966	1682-1966	k4
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
Engrácie	Engrácie	k1gFnSc2
<g/>
,	,	kIx,
portugalsky	portugalsky	k6eAd1
Igreja	Igrej	k2eAgFnSc1d1
de	de	k?
Santa	Santa	k1gFnSc1
Engrácia	Engrácia	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
barokní	barokní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
v	v	k7c6
portugalském	portugalský	k2eAgNnSc6d1
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Lisabonu	Lisabon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
má	mít	k5eAaImIp3nS
impozantní	impozantní	k2eAgFnSc4d1
kupoli	kupole	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
patrná	patrný	k2eAgFnSc1d1
až	až	k9
z	z	k7c2
okrajových	okrajový	k2eAgFnPc2d1
částí	část	k1gFnPc2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
kostela	kostel	k1gInSc2
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
dokončena	dokončen	k2eAgFnSc1d1
až	až	k9
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1916	#num#	k4
byl	být	k5eAaImAgInS
kostel	kostel	k1gInSc1
vyhlášen	vyhlásit	k5eAaPmNgInS
Národním	národní	k2eAgInSc7d1
panteonem	panteon	k1gInSc7
(	(	kIx(
<g/>
portugalsky	portugalsky	k6eAd1
Panteã	Panteã	k1gMnSc1
Nacional	Nacional	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
Santa	Sant	k1gInSc2
Engrácia	Engrácium	k1gNnSc2
a	a	k8xC
okolní	okolní	k2eAgFnSc1d1
městská	městský	k2eAgFnSc1d1
zástavba	zástavba	k1gFnSc1
na	na	k7c6
leteckém	letecký	k2eAgInSc6d1
snímku	snímek	k1gInSc6
</s>
<s>
Varhany	varhany	k1gFnPc1
v	v	k7c6
kostele	kostel	k1gInSc6
</s>
<s>
Původní	původní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgInS
na	na	k7c6
místě	místo	k1gNnSc6
dnešního	dnešní	k2eAgInSc2d1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
zničen	zničen	k2eAgInSc1d1
roku	rok	k1gInSc2
1681	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Práce	práce	k1gFnSc1
na	na	k7c6
novém	nový	k2eAgInSc6d1
kostele	kostel	k1gInSc6
v	v	k7c6
barokním	barokní	k2eAgInSc6d1
slohu	sloh	k1gInSc6
započaly	započnout	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1682	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
přinášela	přinášet	k5eAaImAgFnS
pochybnosti	pochybnost	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
kostel	kostel	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
Engrácie	Engrácie	k1gFnSc2
bude	být	k5eAaImBp3nS
kdy	kdy	k6eAd1
dokončen	dokončit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedokončená	dokončený	k2eNgFnSc1d1
stavba	stavba	k1gFnSc1
skutečně	skutečně	k6eAd1
po	po	k7c4
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
sloužila	sloužit	k5eAaImAgFnS
mimo	mimo	k7c4
jiné	jiný	k2eAgInPc4d1
i	i	k9
pro	pro	k7c4
vojenské	vojenský	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pochybnosti	pochybnost	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
ukázal	ukázat	k5eAaPmAgInS
být	být	k5eAaImF
nesprávnými	správný	k2eNgInPc7d1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
byla	být	k5eAaImAgFnS
stavba	stavba	k1gFnSc1
kostela	kostel	k1gInSc2
dokončena	dokončit	k5eAaPmNgFnS
<g/>
.	.	kIx.
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
o	o	k7c4
284	#num#	k4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Interiér	interiér	k1gInSc1
kostela	kostel	k1gInSc2
je	být	k5eAaImIp3nS
vyveden	vyvést	k5eAaPmNgInS
v	v	k7c6
barevném	barevný	k2eAgInSc6d1
mramoru	mramor	k1gInSc6
a	a	k8xC
celé	celý	k2eAgFnSc6d1
stavbě	stavba	k1gFnSc6
dominuje	dominovat	k5eAaImIp3nS
mohutná	mohutný	k2eAgFnSc1d1
kupole	kupole	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k7c2
budovy	budova	k1gFnSc2
kostela	kostel	k1gInSc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
také	také	k9
kenotafy	kenotaf	k1gInPc1
národních	národní	k2eAgMnPc2d1
hrdinů	hrdina	k1gMnPc2
portugalských	portugalský	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
Vasco	Vasco	k1gNnSc1
da	da	k?
Gama	gama	k1gNnPc2
<g/>
,	,	kIx,
Afonso	Afonsa	k1gFnSc5
de	de	k?
Albuquerque	Albuquerque	k1gNnPc2
<g/>
,	,	kIx,
místokrál	místokrál	k1gMnSc1
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
či	či	k8xC
král	král	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
Mořeplavec	mořeplavec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
novější	nový	k2eAgInSc4d2
potom	potom	k8xC
patří	patřit	k5eAaImIp3nS
hrobka	hrobka	k1gFnSc1
fadistky	fadistka	k1gFnSc2
Amálie	Amálie	k1gFnSc2
Rodrigues	Rodriguesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
kupoli	kupole	k1gFnSc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
nechat	nechat	k5eAaPmF
se	se	k3xPyFc4
vyvézt	vyvézt	k5eAaPmF
výtahem	výtah	k1gInSc7
<g/>
,	,	kIx,
z	z	k7c2
ní	on	k3xPp3gFnSc2
je	být	k5eAaImIp3nS
potom	potom	k8xC
výborný	výborný	k2eAgInSc4d1
panoramatický	panoramatický	k2eAgInSc4d1
výhled	výhled	k1gInSc4
na	na	k7c4
celé	celý	k2eAgNnSc4d1
město	město	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Susie	Susie	k1gFnSc1
Boulton	Boulton	k1gInSc1
<g/>
,	,	kIx,
City	city	k1gNnSc1
Book	Book	k1gInSc1
Lisbona	Lisbona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mondadori	Mondadori	k1gNnSc1
<g/>
,	,	kIx,
Milano	Milana	k1gFnSc5
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788837058104	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kostel	kostel	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
Engrácie	Engrácie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Chiesa	Chiesa	k1gFnSc1
di	di	k?
Santa	Sant	k1gMnSc2
Engrácia	Engrácius	k1gMnSc2
na	na	k7c6
italské	italský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Portugalsko	Portugalsko	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2013058914	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
156504037	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2013058914	#num#	k4
</s>
