<s>
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
(	(	kIx(
<g/>
zastarale	zastarale	k6eAd1
též	též	k9
Otomanská	otomanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
,	,	kIx,
možno	možno	k6eAd1
psát	psát	k5eAaImF
i	i	k9
malé	malý	k2eAgNnSc4d1
písmeno	písmeno	k1gNnSc4
<g/>
,	,	kIx,
turecky	turecky	k6eAd1
Osmanlı	Osmanlı	k1gMnSc4
İ	İ	k1gMnSc4
nebo	nebo	k8xC
Osmanlı	Osmanlı	k1gFnSc4
Devleti	Devle	k1gNnSc3
<g/>
,	,	kIx,
v	v	k7c6
Západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
také	také	k9
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Turecká	turecký	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
jednoduše	jednoduše	k6eAd1
Turecko	Turecko	k1gNnSc4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
největších	veliký	k2eAgFnPc2d3
a	a	k8xC
nejmocnějších	mocný	k2eAgFnPc2d3
říší	říš	k1gFnPc2
v	v	k7c6
prostoru	prostor	k1gInSc6
při	při	k7c6
Středozemním	středozemní	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>