<p>
<s>
Biljana	Biljana	k1gFnSc1	Biljana
Golubović	Golubović	k1gFnSc2	Golubović
(	(	kIx(	(
<g/>
srbsky	srbsky	k6eAd1	srbsky
Б	Б	k?	Б
Г	Г	k?	Г
<g/>
;	;	kIx,	;
narozena	narozen	k2eAgFnSc1d1	narozena
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
srbská	srbský	k2eAgFnSc1d1	Srbská
lingvistka	lingvistka	k1gFnSc1	lingvistka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Biljana	Biljana	k1gFnSc1	Biljana
Sikimić	Sikimić	k1gFnSc1	Sikimić
<g/>
,	,	kIx,	,
Petko	Petko	k1gNnSc1	Petko
Hristov	Hristovo	k1gNnPc2	Hristovo
a	a	k8xC	a
Biljana	Biljana	k1gFnSc1	Biljana
Golubović	Golubović	k1gFnSc1	Golubović
<g/>
.	.	kIx.	.
</s>
<s>
Labour	Labour	k1gMnSc1	Labour
migrations	migrationsa	k1gFnPc2	migrationsa
in	in	k?	in
the	the	k?	the
Balkans	Balkans	k1gInSc1	Balkans
<g/>
.	.	kIx.	.
</s>
<s>
München	Münchna	k1gFnPc2	Münchna
;	;	kIx,	;
Berlin	berlina	k1gFnPc2	berlina
<g/>
:	:	kIx,	:
Sagner	Sagner	k1gMnSc1	Sagner
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
86688	[number]	k4	86688
<g/>
-	-	kIx~	-
<g/>
199	[number]	k4	199
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Christiana	Christian	k1gMnSc4	Christian
Voß	Voß	k1gMnSc4	Voß
a	a	k8xC	a
Biljana	Biljan	k1gMnSc4	Biljan
Golubović	Golubović	k1gMnSc4	Golubović
<g/>
.	.	kIx.	.
</s>
<s>
Srpska	Srpska	k1gFnSc1	Srpska
lingvistika	lingvistika	k1gFnSc1	lingvistika
:	:	kIx,	:
Serbische	Serbische	k1gNnSc1	Serbische
Linguistik	Linguistika	k1gFnPc2	Linguistika
:	:	kIx,	:
eine	einus	k1gMnSc5	einus
Bestandsaufnahme	Bestandsaufnahm	k1gMnSc5	Bestandsaufnahm
<g/>
.	.	kIx.	.
</s>
<s>
München	Münchna	k1gFnPc2	Münchna
:	:	kIx,	:
Berlin	berlina	k1gFnPc2	berlina
<g/>
:	:	kIx,	:
Verlag	Verlag	k1gMnSc1	Verlag
Otto	Otto	k1gMnSc1	Otto
Sagner	Sagner	k1gMnSc1	Sagner
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
86688	[number]	k4	86688
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
90	[number]	k4	90
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
sr	sr	k?	sr
<g/>
,	,	kIx,	,
de	de	k?	de
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
režie	režie	k1gFnSc1	režie
==	==	k?	==
</s>
</p>
<p>
<s>
Hans	Hans	k1gMnSc1	Hans
Christian	Christian	k1gMnSc1	Christian
Andersen	Andersen	k1gMnSc1	Andersen
<g/>
,	,	kIx,	,
Biljana	Biljana	k1gFnSc1	Biljana
Golubović	Golubović	k1gFnSc1	Golubović
<g/>
:	:	kIx,	:
Sněhová	sněhový	k2eAgFnSc1d1	sněhová
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
Dejvické	dejvický	k2eAgNnSc1d1	Dejvické
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Biljana	Biljana	k1gFnSc1	Biljana
Golubović	Golubović	k1gFnSc1	Golubović
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
derniéra	derniéra	k1gFnSc1	derniéra
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
Přeneseno	přenesen	k2eAgNnSc1d1	přeneseno
do	do	k7c2	do
Divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Dlouhé	Dlouhé	k2eAgFnSc6d1	Dlouhé
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Biljana	Biljana	k1gFnSc1	Biljana
Golubović	Golubović	k1gFnSc1	Golubović
</s>
</p>
