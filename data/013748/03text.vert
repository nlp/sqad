<s>
Hradiště	Hradiště	k1gNnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
původním	původní	k2eAgInSc6d1
významu	význam	k1gInSc6
slova	slovo	k1gNnSc2
hradiště	hradiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Hradiště	Hradiště	k1gNnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hradiště	Hradiště	k1gNnSc1
(	(	kIx(
<g/>
též	též	k9
hradisko	hradisko	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
opevněné	opevněný	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
z	z	k7c2
období	období	k1gNnSc2
neolitu	neolit	k1gInSc2
až	až	k8xS
raného	raný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
,	,	kIx,
předchůdce	předchůdce	k1gMnSc1
středověkých	středověký	k2eAgMnPc2d1
hradů	hrad	k1gInPc2
nebo	nebo	k8xC
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výraz	výraz	k1gInSc1
hradiště	hradiště	k1gNnSc2
označuje	označovat	k5eAaImIp3nS
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
stál	stát	k5eAaImAgInS
nebo	nebo	k8xC
stojí	stát	k5eAaImIp3nS
hrad	hrad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
běžné	běžný	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc1
hradiště	hradiště	k1gNnSc2
používán	používán	k2eAgInSc1d1
pro	pro	k7c4
neolitická	neolitický	k2eAgNnPc4d1
až	až	k8xS
raně	raně	k6eAd1
středověká	středověký	k2eAgNnPc4d1
opevněná	opevněný	k2eAgNnPc4d1
sídla	sídlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typickými	typický	k2eAgFnPc7d1
polohami	poloha	k1gFnPc7
vhodnými	vhodný	k2eAgFnPc7d1
pro	pro	k7c4
stavbu	stavba	k1gFnSc4
hradišť	hradiště	k1gNnPc2
byly	být	k5eAaImAgInP
různé	různý	k2eAgInPc1d1
výšiny	výšin	k1gInPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
svahy	svah	k1gInPc1
poskytovaly	poskytovat	k5eAaImAgInP
přirozenou	přirozený	k2eAgFnSc4d1
ochranu	ochrana	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
nebylo	být	k5eNaImAgNnS
nutné	nutný	k2eAgNnSc1d1
výrazněji	výrazně	k6eAd2
je	být	k5eAaImIp3nS
ohrazovat	ohrazovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiným	jiný	k2eAgInSc7d1
častým	častý	k2eAgInSc7d1
typem	typ	k1gInSc7
jsou	být	k5eAaImIp3nP
hradiště	hradiště	k1gNnPc1
blatná	blatný	k2eAgNnPc1d1
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
byla	být	k5eAaImAgNnP
zakládána	zakládat	k5eAaImNgNnP
v	v	k7c6
rovinatém	rovinatý	k2eAgInSc6d1
terénu	terén	k1gInSc6
a	a	k8xC
využívala	využívat	k5eAaPmAgFnS,k5eAaImAgFnS
vodní	vodní	k2eAgFnSc1d1
ochrany	ochrana	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Velikost	velikost	k1gFnSc1
hradišť	hradiště	k1gNnPc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
velmi	velmi	k6eAd1
lišit	lišit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
hradiště	hradiště	k1gNnSc4
s	s	k7c7
rozlohou	rozloha	k1gFnSc7
menší	malý	k2eAgFnSc7d2
než	než	k8xS
jeden	jeden	k4xCgInSc4
hektar	hektar	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
rozsáhlé	rozsáhlý	k2eAgInPc4d1
areály	areál	k1gInPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
opevněná	opevněný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
stovek	stovka	k1gFnPc2
hektarů	hektar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradiště	Hradiště	k1gNnPc1
mohla	moct	k5eAaImAgNnP
plnit	plnit	k5eAaImF
více	hodně	k6eAd2
funkcí	funkce	k1gFnPc2
a	a	k8xC
často	často	k6eAd1
je	být	k5eAaImIp3nS
kombinovala	kombinovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
předpokládané	předpokládaný	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
vojensko-strategické	vojensko-strategický	k2eAgFnSc2d1
mohla	moct	k5eAaImAgFnS
hradiště	hradiště	k1gNnSc4
sloužit	sloužit	k5eAaImF
jako	jako	k8xC,k8xS
útočiště	útočiště	k1gNnSc4
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
shromaždiště	shromaždiště	k1gNnSc1
ke	k	k7c3
kultovním	kultovní	k2eAgInPc3d1
či	či	k8xC
společenským	společenský	k2eAgInPc3d1
rituálům	rituál	k1gInPc3
i	i	k8xC
jako	jako	k9
střediska	středisko	k1gNnSc2
obchodu	obchod	k1gInSc2
včetně	včetně	k7c2
funkce	funkce	k1gFnSc2
opěrných	opěrný	k2eAgInPc2d1
bodů	bod	k1gInPc2
na	na	k7c6
dálkových	dálkový	k2eAgFnPc6d1
komunikacích	komunikace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepochybně	pochybně	k6eNd1
sloužila	sloužit	k5eAaImAgFnS
i	i	k8xC
jako	jako	k8xS,k8xC
sídla	sídlo	k1gNnSc2
společenských	společenský	k2eAgFnPc2d1
elit	elita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Praslovanské	praslovanský	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
pro	pro	k7c4
hradiště	hradiště	k1gNnSc4
je	být	k5eAaImIp3nS
*	*	kIx~
<g/>
gordъ	gordъ	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
různých	různý	k2eAgInPc6d1
slovanských	slovanský	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
se	se	k3xPyFc4
vyvíjelo	vyvíjet	k5eAaImAgNnS
různě	různě	k6eAd1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vysvětluje	vysvětlovat	k5eAaImIp3nS
původ	původ	k1gInSc4
českého	český	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
hrad	hrad	k1gInSc1
a	a	k8xC
ruského	ruský	k2eAgMnSc2d1
г	г	k?
(	(	kIx(
<g/>
gorod	gorod	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hradiště	Hradiště	k1gNnSc1
podle	podle	k7c2
zemí	zem	k1gFnPc2
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Hradiště	Hradiště	k1gNnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Oppidum	oppidum	k1gNnSc1
Stradonice	Stradonice	k1gFnSc2
</s>
<s>
Česká	český	k2eAgNnPc1d1
hradiště	hradiště	k1gNnPc1
byla	být	k5eAaImAgNnP
budována	budovat	k5eAaImNgNnP
od	od	k7c2
neolitu	neolit	k1gInSc2
do	do	k7c2
raného	raný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Raně	raně	k6eAd1
středověká	středověký	k2eAgNnPc1d1
hradiště	hradiště	k1gNnPc1
byla	být	k5eAaImAgNnP
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
době	doba	k1gFnSc6
označována	označován	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
hrady	hrad	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
správným	správný	k2eAgNnSc7d1
označením	označení	k1gNnSc7
těchto	tento	k3xDgFnPc2
staveb	stavba	k1gFnPc2
je	být	k5eAaImIp3nS
termín	termín	k1gInSc1
raně	raně	k6eAd1
středověké	středověký	k2eAgInPc4d1
hrady	hrad	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc7
podobou	podoba	k1gFnSc7
se	se	k3xPyFc4
však	však	k9
hradiště	hradiště	k1gNnSc1
od	od	k7c2
vrcholně	vrcholně	k6eAd1
středověkých	středověký	k2eAgInPc2d1
hradů	hrad	k1gInPc2
velmi	velmi	k6eAd1
lišila	lišit	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3
objekty	objekt	k1gInPc1
považované	považovaný	k2eAgInPc1d1
za	za	k7c4
hradiště	hradiště	k1gNnPc4
či	či	k8xC
opevněná	opevněný	k2eAgNnPc4d1
výšinná	výšinný	k2eAgNnPc4d1
sídliště	sídliště	k1gNnPc4
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
závěrečné	závěrečný	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
neolitu	neolit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezpochybnitelná	zpochybnitelný	k2eNgNnPc4d1
hradiště	hradiště	k1gNnPc4
pochází	pocházet	k5eAaImIp3nS
až	až	k9
z	z	k7c2
eneolitu	eneolit	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
objekty	objekt	k1gInPc1
opevněné	opevněný	k2eAgInPc1d1
příkopy	příkop	k1gInPc1
<g/>
,	,	kIx,
palisádami	palisáda	k1gFnPc7
a	a	k8xC
různými	různý	k2eAgInPc7d1
typy	typ	k1gInPc7
hradeb	hradba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
příchodem	příchod	k1gInSc7
doby	doba	k1gFnSc2
bronzové	bronzový	k2eAgNnSc4d1
hradiště	hradiště	k1gNnSc4
mizí	mizet	k5eAaImIp3nS
a	a	k8xC
v	v	k7c6
malém	malý	k2eAgNnSc6d1
množství	množství	k1gNnSc6
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
až	až	k9
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
době	doba	k1gFnSc6
bronzové	bronzový	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mladší	mladý	k2eAgFnSc6d2
a	a	k8xC
pozdní	pozdní	k2eAgFnSc6d1
době	doba	k1gFnSc6
bronzové	bronzový	k2eAgFnSc6d1
se	se	k3xPyFc4
počet	počet	k1gInSc1
hradišť	hradiště	k1gNnPc2
výrazně	výrazně	k6eAd1
zvýšil	zvýšit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opevněná	opevněný	k2eAgFnSc1d1
sídla	sídlo	k1gNnSc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
byla	být	k5eAaImAgFnS
středisky	středisko	k1gNnPc7
řemeslné	řemeslný	k2eAgMnPc4d1
výroby	výroba	k1gFnSc2
a	a	k8xC
snad	snad	k9
i	i	k8xC
ekonomicko-správními	ekonomicko-správní	k2eAgNnPc7d1
centry	centrum	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
dalším	další	k2eAgNnSc6d1
období	období	k1gNnSc6
je	být	k5eAaImIp3nS
stavba	stavba	k1gFnSc1
hradišť	hradiště	k1gNnPc2
doložena	doložen	k2eAgFnSc1d1
až	až	k8xS
od	od	k7c2
počátku	počátek	k1gInSc2
šestého	šestý	k4xOgNnSc2
století	století	k1gNnPc2
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
v	v	k7c6
pozdní	pozdní	k2eAgFnSc6d1
době	doba	k1gFnSc6
halštatské	halštatský	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Halštatská	halštatský	k2eAgFnSc1d1
opevněná	opevněný	k2eAgFnSc1d1
sídliště	sídliště	k1gNnSc1
v	v	k7c6
maximální	maximální	k2eAgFnSc6d1
míře	míra	k1gFnSc6
využívala	využívat	k5eAaPmAgFnS,k5eAaImAgFnS
kombinaci	kombinace	k1gFnSc4
přírodních	přírodní	k2eAgInPc2d1
obranných	obranný	k2eAgInPc2d1
prvků	prvek	k1gInPc2
a	a	k8xC
umělých	umělý	k2eAgNnPc2d1
opevnění	opevnění	k1gNnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
rozloha	rozloha	k1gFnSc1
se	se	k3xPyFc4
pohybovala	pohybovat	k5eAaImAgFnS
od	od	k7c2
stovek	stovka	k1gFnPc2
metrů	metr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
do	do	k7c2
desítek	desítka	k1gFnPc2
hektarů	hektar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
laténské	laténský	k2eAgFnSc6d1
se	se	k3xPyFc4
hradiště	hradiště	k1gNnPc1
objevují	objevovat	k5eAaImIp3nP
již	již	k6eAd1
v	v	k7c6
její	její	k3xOp3gFnSc6
časné	časný	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
dobře	dobře	k6eAd1
poznané	poznaný	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
až	až	k9
objekty	objekt	k1gInPc1
z	z	k7c2
mladšího	mladý	k2eAgNnSc2d2
období	období	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
počátku	počátek	k1gInSc6
druhého	druhý	k4xOgNnSc2
století	století	k1gNnSc2
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
vnikala	vnikat	k5eAaImAgFnS
hradiště	hradiště	k1gNnSc4
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
oppida	oppidum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
výstavba	výstavba	k1gFnSc1
souvisela	souviset	k5eAaImAgFnS
pravděpodobně	pravděpodobně	k6eAd1
s	s	k7c7
příchodem	příchod	k1gInSc7
keltských	keltský	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
z	z	k7c2
předalpské	předalpský	k2eAgFnSc2d1
Galie	Galie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
si	se	k3xPyFc3
s	s	k7c7
sebou	se	k3xPyFc7
přinesly	přinést	k5eAaPmAgFnP
pokročilé	pokročilý	k2eAgFnPc4d1
znalosti	znalost	k1gFnPc4
stavebního	stavební	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
od	od	k7c2
přelomu	přelom	k1gInSc2
letopočtu	letopočet	k1gInSc2
do	do	k7c2
sedmého	sedmý	k4xOgNnSc2
století	století	k1gNnSc2
byla	být	k5eAaImAgFnS
na	na	k7c6
území	území	k1gNnSc6
Čech	Čechy	k1gFnPc2
budována	budovat	k5eAaImNgFnS
jen	jen	k6eAd1
lehká	lehký	k2eAgFnSc1d1
a	a	k8xC
nejspíše	nejspíše	k9
dočasná	dočasný	k2eAgNnPc4d1
opevnění	opevnění	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
raně	raně	k6eAd1
středověká	středověký	k2eAgNnPc1d1
hradiště	hradiště	k1gNnPc1
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgNnP
až	až	k9
v	v	k7c6
osmém	osmý	k4xOgInSc6
století	století	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
jejich	jejich	k3xOp3gNnPc4
stavebníci	stavebník	k1gMnPc1
využívali	využívat	k5eAaImAgMnP,k5eAaPmAgMnP
k	k	k7c3
opevnění	opevnění	k1gNnSc3
pouze	pouze	k6eAd1
mělké	mělký	k2eAgInPc1d1
příkopy	příkop	k1gInPc1
a	a	k8xC
jednoduché	jednoduchý	k2eAgFnPc1d1
dřevěné	dřevěný	k2eAgFnPc1d1
hradby	hradba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgFnPc1
byly	být	k5eAaImAgFnP
často	často	k6eAd1
v	v	k7c6
průběhu	průběh	k1gInSc6
devátého	devátý	k4xOgNnSc2
století	století	k1gNnSc2
nahrazeny	nahradit	k5eAaPmNgInP
mohutnějšími	mohutný	k2eAgFnPc7d2
hradbami	hradba	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
konstrukcích	konstrukce	k1gFnPc6
kombinovaly	kombinovat	k5eAaImAgFnP
dřevo	dřevo	k1gNnSc4
<g/>
,	,	kIx,
hlínu	hlína	k1gFnSc4
a	a	k8xC
především	především	k9
kámen	kámen	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
desátého	desátý	k4xOgNnSc2
století	století	k1gNnSc2
se	se	k3xPyFc4
některá	některý	k3yIgNnPc1
hradiště	hradiště	k1gNnPc1
stala	stát	k5eAaPmAgNnP
základními	základní	k2eAgFnPc7d1
mocenskými	mocenský	k2eAgFnPc7d1
oporami	opora	k1gFnPc7
přemyslovského	přemyslovský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Centra	centrum	k1gNnPc1
provincií	provincie	k1gFnPc2
tvořila	tvořit	k5eAaImAgNnP
tzv.	tzv.	kA
hradskou	hradský	k2eAgFnSc4d1
soustavu	soustava	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
přetrvala	přetrvat	k5eAaPmAgFnS
až	až	k9
do	do	k7c2
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
třináctého	třináctý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
Hradiště	Hradiště	k1gNnSc1
Daubarių	Daubarių	k1gFnSc1
piliakalnis	piliakalnis	k1gFnSc1
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Mažeikiai	Mažeikia	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Litevský	litevský	k2eAgInSc1d1
piliakalnis	piliakalnis	k1gInSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
lotyšsky	lotyšsky	k6eAd1
pilskalns	pilskalns	k6eAd1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
litevský	litevský	k2eAgInSc1d1
protějšek	protějšek	k1gInSc1
hradiště	hradiště	k1gNnSc2
s	s	k7c7
tím	ten	k3xDgInSc7
rozdílem	rozdíl	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nevyskytují	vyskytovat	k5eNaImIp3nP
hradiště	hradiště	k1gNnPc4
tzv.	tzv.	kA
blatná	blatný	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
litevská	litevský	k2eAgNnPc1d1
hradiště	hradiště	k1gNnPc1
jsou	být	k5eAaImIp3nP
neolitická	neolitický	k2eAgNnPc1d1
(	(	kIx(
<g/>
první	první	k4xOgFnSc3
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
již	již	k6eAd1
1500	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
a	a	k8xC
jsou	být	k5eAaImIp3nP
nejdůležitějšími	důležitý	k2eAgInPc7d3
kulturními	kulturní	k2eAgInPc7d1
památníky	památník	k1gInPc7
pohanských	pohanský	k2eAgInPc2d1
Baltů	Balt	k1gInPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
častěji	často	k6eAd2
raně	raně	k6eAd1
středověká	středověký	k2eAgFnSc1d1
<g/>
,	,	kIx,
výjimečně	výjimečně	k6eAd1
budovaná	budovaný	k2eAgFnSc1d1
též	též	k9
v	v	k7c6
současnosti	současnost	k1gFnSc6
a	a	k8xC
jsou	být	k5eAaImIp3nP
původu	původ	k1gMnSc3
převážně	převážně	k6eAd1
žemaitského	žemaitský	k2eAgNnSc2d1
nebo	nebo	k8xC
litevského	litevský	k2eAgMnSc2d1
<g/>
,	,	kIx,
méně	málo	k6eAd2
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
nachází	nacházet	k5eAaImIp3nS
ještě	ještě	k9
také	také	k9
na	na	k7c6
území	území	k1gNnSc6
Lotyšska	Lotyšsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
ČTVERÁK	čtverák	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
LUTOVSKÝ	LUTOVSKÝ	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
;	;	kIx,
SLABINA	slabina	k1gFnSc1
<g/>
,	,	kIx,
Miloslav	Miloslav	k1gMnSc1
<g/>
;	;	kIx,
SMEJTEK	SMEJTEK	kA
<g/>
,	,	kIx,
Lubor	Lubor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
hradišť	hradiště	k1gNnPc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
432	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
173	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Encyklopedie	encyklopedie	k1gFnPc4
českých	český	k2eAgNnPc2d1
hradišť	hradiště	k1gNnPc2
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Encyklopedie	encyklopedie	k1gFnSc1
českých	český	k2eAgNnPc2d1
hradišť	hradiště	k1gNnPc2
<g/>
,	,	kIx,
s.	s.	k?
191	#num#	k4
2	#num#	k4
DURDÍK	DURDÍK	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustrovaná	ilustrovaný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
českých	český	k2eAgInPc2d1
hradů	hrad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
736	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Nástin	nástin	k1gInSc1
vývoje	vývoj	k1gInSc2
českých	český	k2eAgInPc2d1
hradů	hrad	k1gInPc2
<g/>
,	,	kIx,
s.	s.	k?
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Encyklopedie	encyklopedie	k1gFnSc2
českých	český	k2eAgNnPc2d1
hradišť	hradiště	k1gNnPc2
<g/>
,	,	kIx,
s.	s.	k?
15	#num#	k4
<g/>
↑	↑	k?
Encyklopedie	encyklopedie	k1gFnSc2
českých	český	k2eAgNnPc2d1
hradišť	hradiště	k1gNnPc2
<g/>
,	,	kIx,
s.	s.	k?
16	#num#	k4
<g/>
↑	↑	k?
ČIŽMÁŘ	čižmář	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
hradišť	hradiště	k1gNnPc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
304	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
174	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
65	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Encyklopedie	encyklopedie	k1gFnSc2
českých	český	k2eAgNnPc2d1
hradišť	hradiště	k1gNnPc2
<g/>
,	,	kIx,
s.	s.	k?
17	#num#	k4
<g/>
↑	↑	k?
Encyklopedie	encyklopedie	k1gFnSc2
českých	český	k2eAgNnPc2d1
hradišť	hradiště	k1gNnPc2
<g/>
,	,	kIx,
s.	s.	k?
18	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ČTVERÁK	čtverák	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
LUTOVSKÝ	LUTOVSKÝ	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
;	;	kIx,
SLABINA	slabina	k1gFnSc1
<g/>
,	,	kIx,
Miloslav	Miloslav	k1gMnSc1
<g/>
;	;	kIx,
SMEJTEK	SMEJTEK	kA
<g/>
,	,	kIx,
Lubor	Lubor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
hradišť	hradiště	k1gNnPc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
432	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
173	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ČIŽMÁŘ	čižmář	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
hradišť	hradiště	k1gNnPc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
304	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
174	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
hradišť	hradiště	k1gNnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
hradiště	hradiště	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
hradiště	hradiště	k1gNnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Databáze	databáze	k1gFnSc1
českých	český	k2eAgNnPc2d1
hradišť	hradiště	k1gNnPc2
na	na	k7c4
www.stredovek.com	www.stredovek.com	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
litevských	litevský	k2eAgNnPc2d1
hradišť	hradiště	k1gNnPc2
(	(	kIx(
<g/>
litevsky	litevsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4189008-5	4189008-5	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
163	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Pravěk	pravěk	k1gInSc1
|	|	kIx~
Středověk	středověk	k1gInSc1
</s>
