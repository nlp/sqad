<p>
<s>
Mandelík	mandelík	k1gMnSc1	mandelík
hajní	hajní	k2eAgMnSc1d1	hajní
(	(	kIx(	(
<g/>
Coracias	Coracias	k1gMnSc1	Coracias
garrulus	garrulus	k1gMnSc1	garrulus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
pestře	pestro	k6eAd1	pestro
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
mandelíkovitých	mandelíkovitý	k2eAgMnPc2d1	mandelíkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
blízce	blízce	k6eAd1	blízce
příbuzný	příbuzný	k2eAgMnSc1d1	příbuzný
s	s	k7c7	s
vlhou	vlha	k1gFnSc7	vlha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Mandelík	mandelík	k1gInSc1	mandelík
hajní	hajní	k2eAgInSc1d1	hajní
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
asi	asi	k9	asi
jako	jako	k9	jako
kavka	kavka	k1gFnSc1	kavka
<g/>
,	,	kIx,	,
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
přibližně	přibližně	k6eAd1	přibližně
29	[number]	k4	29
<g/>
–	–	k?	–
<g/>
32	[number]	k4	32
cm	cm	kA	cm
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
kolem	kolem	k7c2	kolem
180	[number]	k4	180
g	g	kA	g
a	a	k8xC	a
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měří	měřit	k5eAaImIp3nS	měřit
52	[number]	k4	52
<g/>
–	–	k?	–
<g/>
58	[number]	k4	58
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nádherně	nádherně	k6eAd1	nádherně
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
<g/>
,	,	kIx,	,
azurově	azurově	k6eAd1	azurově
modrý	modrý	k2eAgInSc1d1	modrý
s	s	k7c7	s
červenohnědým	červenohnědý	k2eAgInSc7d1	červenohnědý
hřbetem	hřbet	k1gInSc7	hřbet
<g/>
,	,	kIx,	,
pravoúhle	pravoúhle	k6eAd1	pravoúhle
zastřiženým	zastřižený	k2eAgInSc7d1	zastřižený
ocasem	ocas	k1gInSc7	ocas
<g/>
,	,	kIx,	,
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
silným	silný	k2eAgInSc7d1	silný
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
krátkýma	krátký	k2eAgFnPc7d1	krátká
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
krk	krk	k1gInSc1	krk
a	a	k8xC	a
spodina	spodina	k1gFnSc1	spodina
těla	tělo	k1gNnSc2	tělo
jsou	být	k5eAaImIp3nP	být
světle	světle	k6eAd1	světle
modré	modrý	k2eAgFnSc2d1	modrá
se	s	k7c7	s
zelenavým	zelenavý	k2eAgInSc7d1	zelenavý
nádechem	nádech	k1gInSc7	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Hřbet	hřbet	k1gInSc1	hřbet
<g/>
,	,	kIx,	,
lopatky	lopatka	k1gFnPc1	lopatka
a	a	k8xC	a
raménko	raménko	k1gNnSc1	raménko
tmavohnědé	tmavohnědý	k2eAgNnSc1d1	tmavohnědé
<g/>
.	.	kIx.	.
</s>
<s>
Svrchní	svrchní	k2eAgFnPc1d1	svrchní
křídelní	křídelní	k2eAgFnPc1d1	křídelní
krovky	krovka	k1gFnPc1	krovka
fialovomodré	fialovomodrý	k2eAgFnPc1d1	fialovomodrá
<g/>
,	,	kIx,	,
letky	letek	k1gInPc1	letek
černohnědé	černohnědý	k2eAgInPc1d1	černohnědý
s	s	k7c7	s
modravým	modravý	k2eAgInSc7d1	modravý
nádechem	nádech	k1gInSc7	nádech
na	na	k7c6	na
vnějších	vnější	k2eAgInPc6d1	vnější
praporech	prapor	k1gInPc6	prapor
a	a	k8xC	a
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
světle	světle	k6eAd1	světle
modré	modrý	k2eAgFnPc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
tmavomodrý	tmavomodrý	k2eAgInSc1d1	tmavomodrý
<g/>
,	,	kIx,	,
před	před	k7c7	před
tmavými	tmavý	k2eAgFnPc7d1	tmavá
špičkami	špička	k1gFnPc7	špička
světlejší	světlý	k2eAgFnSc7d2	světlejší
<g/>
,	,	kIx,	,
prostřední	prostřednět	k5eAaImIp3nS	prostřednět
pár	pár	k4xCyI	pár
špinavě	špinavě	k6eAd1	špinavě
zelený	zelený	k2eAgInSc4d1	zelený
<g/>
,	,	kIx,	,
vnější	vnější	k2eAgInSc4d1	vnější
pár	pár	k4xCyI	pár
rýdovacích	rýdovací	k2eAgNnPc2d1	rýdovací
per	pero	k1gNnPc2	pero
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
černé	černý	k2eAgFnPc4d1	černá
špičky	špička	k1gFnPc4	špička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
pohlaví	pohlaví	k1gNnPc4	pohlaví
se	s	k7c7	s
zbarvením	zbarvení	k1gNnSc7	zbarvení
neliší	lišit	k5eNaImIp3nS	lišit
<g/>
;	;	kIx,	;
mladí	mladý	k2eAgMnPc1d1	mladý
jedinci	jedinec	k1gMnPc1	jedinec
mají	mít	k5eAaImIp3nP	mít
bledší	bledý	k2eAgFnPc4d2	bledší
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
krk	krk	k1gInSc4	krk
a	a	k8xC	a
prsa	prsa	k1gNnPc4	prsa
nahnědlé	nahnědlý	k2eAgFnPc4d1	nahnědlá
<g/>
,	,	kIx,	,
krajní	krajní	k2eAgFnPc4d1	krajní
rýdovací	rýdovací	k2eAgNnPc4d1	rýdovací
pera	pero	k1gNnPc4	pero
bez	bez	k7c2	bez
černých	černý	k2eAgFnPc2d1	černá
špiček	špička	k1gFnPc2	špička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
zpěvu	zpěv	k1gInSc6	zpěv
vydává	vydávat	k5eAaPmIp3nS	vydávat
drsné	drsný	k2eAgInPc1d1	drsný
zvuky	zvuk	k1gInPc1	zvuk
"	"	kIx"	"
<g/>
krak	krak	k?	krak
<g/>
,	,	kIx,	,
krak	krak	k?	krak
<g/>
,	,	kIx,	,
krak	krak	k?	krak
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
až	až	k9	až
září	zářit	k5eAaImIp3nS	zářit
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
migrují	migrovat	k5eAaImIp3nP	migrovat
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
začínají	začínat	k5eAaImIp3nP	začínat
vracet	vracet	k5eAaImF	vracet
obvykle	obvykle	k6eAd1	obvykle
během	během	k7c2	během
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mandelík	mandelík	k1gInSc1	mandelík
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
hojně	hojně	k6eAd1	hojně
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
třiceti	třicet	k4xCc6	třicet
letech	léto	k1gNnPc6	léto
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jeho	jeho	k3xOp3gFnSc2	jeho
populace	populace	k1gFnSc2	populace
dosti	dosti	k6eAd1	dosti
rychle	rychle	k6eAd1	rychle
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
hnízdil	hnízdit	k5eAaImAgMnS	hnízdit
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
,	,	kIx,	,
i	i	k8xC	i
dost	dost	k6eAd1	dost
početně	početně	k6eAd1	početně
(	(	kIx(	(
<g/>
i	i	k9	i
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zcela	zcela	k6eAd1	zcela
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
max	max	kA	max
<g/>
.	.	kIx.	.
tři	tři	k4xCgInPc4	tři
páry	pár	k1gInPc4	pár
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
hnízdění	hnízdění	k1gNnSc1	hnízdění
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Brna	Brno	k1gNnSc2	Brno
<g/>
;	;	kIx,	;
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
území	území	k1gNnSc4	území
ČR	ČR	kA	ČR
zcela	zcela	k6eAd1	zcela
vymizelý	vymizelý	k2eAgInSc1d1	vymizelý
<g/>
..	..	k?	..
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
pozorováni	pozorován	k2eAgMnPc1d1	pozorován
jen	jen	k9	jen
ojediněle	ojediněle	k6eAd1	ojediněle
ptáci	pták	k1gMnPc1	pták
na	na	k7c6	na
tahu	tah	k1gInSc6	tah
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
naše	náš	k3xOp1gNnSc4	náš
území	území	k1gNnSc4	území
přilétal	přilétat	k5eAaImAgMnS	přilétat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
až	až	k6eAd1	až
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
a	a	k8xC	a
odlétal	odlétat	k5eAaPmAgMnS	odlétat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
až	až	k8xS	až
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Tažný	tažný	k2eAgMnSc1d1	tažný
pták	pták	k1gMnSc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zpravidla	zpravidla	k6eAd1	zpravidla
osaměle	osaměle	k6eAd1	osaměle
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
také	také	k9	také
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
otevřené	otevřený	k2eAgFnSc3d1	otevřená
krajině	krajina	k1gFnSc3	krajina
v	v	k7c6	v
rovinách	rovina	k1gFnPc6	rovina
a	a	k8xC	a
pahorkatinách	pahorkatina	k1gFnPc6	pahorkatina
s	s	k7c7	s
roztroušenou	roztroušený	k2eAgFnSc7d1	roztroušená
zelení	zeleň	k1gFnSc7	zeleň
–	–	k?	–
menšími	malý	k2eAgInPc7d2	menší
lesíky	lesík	k1gInPc7	lesík
<g/>
,	,	kIx,	,
většími	veliký	k2eAgInPc7d2	veliký
parky	park	k1gInPc7	park
<g/>
,	,	kIx,	,
ovocnými	ovocný	k2eAgInPc7d1	ovocný
sady	sad	k1gInPc7	sad
<g/>
,	,	kIx,	,
alejemi	alej	k1gFnPc7	alej
starých	starý	k2eAgMnPc2d1	starý
vykotlaných	vykotlaný	k2eAgMnPc2d1	vykotlaný
stromů	strom	k1gInPc2	strom
atp.	atp.	kA	atp.
Usazuje	usazovat	k5eAaImIp3nS	usazovat
se	se	k3xPyFc4	se
i	i	k9	i
ve	v	k7c6	v
větších	veliký	k2eAgInPc6d2	veliký
lesních	lesní	k2eAgInPc6d1	lesní
komplexech	komplex	k1gInPc6	komplex
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
rozsáhlejší	rozsáhlý	k2eAgFnSc1d2	rozsáhlejší
paseky	paseka	k1gFnPc1	paseka
a	a	k8xC	a
duté	dutý	k2eAgInPc1d1	dutý
stromy	strom	k1gInPc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mimohnízdní	mimohnízdní	k2eAgFnSc6d1	mimohnízdní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
na	na	k7c6	na
zcela	zcela	k6eAd1	zcela
bezlesých	bezlesý	k2eAgNnPc6d1	bezlesé
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
<g/>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jednotlivě	jednotlivě	k6eAd1	jednotlivě
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
během	během	k7c2	během
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vhodných	vhodný	k2eAgNnPc6d1	vhodné
hnízdištích	hnízdiště	k1gNnPc6	hnízdiště
(	(	kIx(	(
<g/>
hlinité	hlinitý	k2eAgFnPc1d1	hlinitá
stěny	stěna	k1gFnPc1	stěna
<g/>
,	,	kIx,	,
doupné	doupný	k2eAgInPc1d1	doupný
stromy	strom	k1gInPc1	strom
<g/>
)	)	kIx)	)
i	i	k9	i
více	hodně	k6eAd2	hodně
párů	pár	k1gInPc2	pár
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
kavky	kavka	k1gFnPc1	kavka
<g/>
,	,	kIx,	,
vlhy	vlha	k1gFnPc1	vlha
<g/>
,	,	kIx,	,
poštolky	poštolka	k1gFnPc1	poštolka
aj.	aj.	kA	aj.
(	(	kIx(	(
<g/>
Dokud	dokud	k8xS	dokud
hnízdil	hnízdit	k5eAaImAgMnS	hnízdit
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
přiletoval	přiletovat	k5eAaImAgMnS	přiletovat
již	již	k6eAd1	již
v	v	k7c6	v
párech	pár	k1gInPc6	pár
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Samec	samec	k1gMnSc1	samec
létá	létat	k5eAaImIp3nS	létat
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c7	nad
hnízdištěm	hnízdiště	k1gNnSc7	hnízdiště
a	a	k8xC	a
opakovaně	opakovaně	k6eAd1	opakovaně
se	se	k3xPyFc4	se
spouší	spoušit	k5eAaPmIp3nS	spoušit
střemhlav	střemhlav	k6eAd1	střemhlav
dolů	dolů	k6eAd1	dolů
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
hlasitě	hlasitě	k6eAd1	hlasitě
ozývá	ozývat	k5eAaImIp3nS	ozývat
"	"	kIx"	"
<g/>
ra	ra	k0	ra
kra	kra	k1gFnSc1	kra
kra	kra	k1gFnSc1	kra
kra	kra	k1gFnSc1	kra
krááá	krááá	k1gFnSc1	krááá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
hnízda	hnízdo	k1gNnSc2	hnízdo
volí	volit	k5eAaImIp3nS	volit
samec	samec	k1gInSc1	samec
<g/>
;	;	kIx,	;
zaletuje	zaletovat	k5eAaImIp3nS	zaletovat
ke	k	k7c3	k
vchodu	vchod	k1gInSc2	vchod
a	a	k8xC	a
"	"	kIx"	"
<g/>
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
"	"	kIx"	"
hnízdo	hnízdo	k1gNnSc4	hnízdo
samici	samice	k1gFnSc3	samice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
stromových	stromový	k2eAgInPc2d1	stromový
(	(	kIx(	(
<g/>
např.	např.	kA	např.
po	po	k7c6	po
datlech	datel	k1gMnPc6	datel
nebo	nebo	k8xC	nebo
žlunách	žluna	k1gFnPc6	žluna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
děrách	děra	k1gFnPc6	děra
hlinitých	hlinitý	k2eAgFnPc2d1	hlinitá
stěn	stěna	k1gFnPc2	stěna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
štěrbinách	štěrbina	k1gFnPc6	štěrbina
zdiva	zdivo	k1gNnSc2	zdivo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
v	v	k7c6	v
budkách	budka	k1gFnPc6	budka
<g/>
.	.	kIx.	.
</s>
<s>
Dutiny	dutina	k1gFnPc4	dutina
ani	ani	k8xC	ani
budky	budka	k1gFnPc4	budka
nevystýlá	vystýlat	k5eNaImIp3nS	vystýlat
<g/>
.	.	kIx.	.
</s>
<s>
Snáší	snášet	k5eAaImIp3nS	snášet
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
čistě	čistě	k6eAd1	čistě
bílých	bílý	k2eAgNnPc2d1	bílé
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
sedí	sedit	k5eAaImIp3nP	sedit
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
(	(	kIx(	(
<g/>
samička	samička	k1gFnSc1	samička
déle	dlouho	k6eAd2	dlouho
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
krmí	krmit	k5eAaImIp3nP	krmit
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
potravu	potrava	k1gFnSc4	potrava
podává	podávat	k5eAaImIp3nS	podávat
pouze	pouze	k6eAd1	pouze
samička	samička	k1gFnSc1	samička
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
hnízdo	hnízdo	k1gNnSc4	hnízdo
opouštějí	opouštět	k5eAaImIp3nP	opouštět
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
dní	den	k1gInPc2	den
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
krmena	krmit	k5eAaImNgNnP	krmit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
dosažený	dosažený	k2eAgInSc1d1	dosažený
věk	věk	k1gInSc1	věk
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
9	[number]	k4	9
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
živočišnou	živočišný	k2eAgFnSc7d1	živočišná
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
většími	veliký	k2eAgMnPc7d2	veliký
členovci	členovec	k1gMnPc7	členovec
<g/>
,	,	kIx,	,
především	především	k9	především
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ojediněle	ojediněle	k6eAd1	ojediněle
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
zaznamenáni	zaznamenán	k2eAgMnPc1d1	zaznamenán
drobní	drobný	k2eAgMnPc1d1	drobný
savci	savec	k1gMnPc1	savec
(	(	kIx(	(
<g/>
rejsci	rejsek	k1gMnPc1	rejsek
<g/>
,	,	kIx,	,
hraboši	hraboš	k1gMnPc1	hraboš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drobné	drobný	k2eAgFnPc4d1	drobná
žáby	žába	k1gFnPc4	žába
či	či	k8xC	či
ještěrky	ještěrka	k1gFnPc4	ještěrka
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
Nestravitelné	stravitelný	k2eNgFnSc2d1	nestravitelná
části	část	k1gFnSc2	část
vyvrhuje	vyvrhovat	k5eAaImIp3nS	vyvrhovat
<g/>
,	,	kIx,	,
vývržky	vývržek	k1gInPc4	vývržek
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
mm	mm	kA	mm
×	×	k?	×
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
mm	mm	kA	mm
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozpadají	rozpadat	k5eAaPmIp3nP	rozpadat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kořist	kořist	k1gFnSc1	kořist
vyhlíží	vyhlížet	k5eAaImIp3nS	vyhlížet
z	z	k7c2	z
vyvýšeného	vyvýšený	k2eAgNnSc2d1	vyvýšené
místa	místo	k1gNnSc2	místo
(	(	kIx(	(
<g/>
větev	větev	k1gFnSc1	větev
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
kůl	kůl	k1gInSc1	kůl
<g/>
;	;	kIx,	;
sloup	sloup	k1gInSc1	sloup
či	či	k8xC	či
dráty	drát	k1gInPc1	drát
elektrického	elektrický	k2eAgNnSc2d1	elektrické
vedení	vedení	k1gNnSc2	vedení
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
a	a	k8xC	a
sletuje	sletovat	k5eAaPmIp3nS	sletovat
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
nechodí	chodit	k5eNaImIp3nS	chodit
<g/>
,	,	kIx,	,
i	i	k8xC	i
na	na	k7c6	na
krátké	krátký	k2eAgFnSc6d1	krátká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přeletuje	přeletovat	k5eAaImIp3nS	přeletovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letu	let	k1gInSc6	let
loví	lovit	k5eAaImIp3nP	lovit
potravu	potrava	k1gFnSc4	potrava
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
uvádny	uvádna	k1gFnPc4	uvádna
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Coracias	Coracias	k1gMnSc1	Coracias
garrulus	garrulus	k1gMnSc1	garrulus
garrulus	garrulus	k1gMnSc1	garrulus
Linnaeus	Linnaeus	k1gMnSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
–	–	k?	–
mandelík	mandelík	k1gMnSc1	mandelík
hajní	hajní	k2eAgInPc4d1	hajní
evropský	evropský	k2eAgInSc4d1	evropský
(	(	kIx(	(
<g/>
sever	sever	k1gInSc4	sever
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc1d1	východní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
po	po	k7c4	po
Irák	Irák	k1gInSc4	Irák
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Coracias	Coracias	k1gMnSc1	Coracias
garrulus	garrulus	k1gMnSc1	garrulus
semenowi	semenow	k1gFnSc2	semenow
Loudon	Loudon	k1gMnSc1	Loudon
&	&	k?	&
Tschusi	Tschuse	k1gFnSc4	Tschuse
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
–	–	k?	–
mandelík	mandelík	k1gInSc1	mandelík
hajní	hajní	k2eAgInSc1d1	hajní
asijský	asijský	k2eAgInSc1d1	asijský
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnSc1d1	střední
Asie	Asie	k1gFnSc1	Asie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
České	český	k2eAgNnSc1d1	české
pojmenování	pojmenování	k1gNnSc1	pojmenování
==	==	k?	==
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
jméno	jméno	k1gNnSc1	jméno
mandelík	mandelík	k1gInSc1	mandelík
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
pták	pták	k1gMnSc1	pták
vyhlíží	vyhlížet	k5eAaImIp3nS	vyhlížet
kořist	kořist	k1gFnSc4	kořist
z	z	k7c2	z
vyvýšených	vyvýšený	k2eAgNnPc2d1	vyvýšené
míst	místo	k1gNnPc2	místo
–	–	k?	–
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
lidé	člověk	k1gMnPc1	člověk
nejčastěji	často	k6eAd3	často
všímali	všímat	k5eAaImAgMnP	všímat
<g/>
,	,	kIx,	,
když	když	k8xS	když
seděl	sedět	k5eAaImAgMnS	sedět
na	na	k7c6	na
seskupených	seskupený	k2eAgInPc6d1	seskupený
snopech	snop	k1gInPc6	snop
obilí	obilí	k1gNnSc4	obilí
čili	čili	k8xC	čili
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
panácích	panák	k1gMnPc6	panák
neboli	neboli	k8xC	neboli
mandelech	mandel	k1gMnPc6	mandel
(	(	kIx(	(
<g/>
mandelích	mandel	k1gInPc6	mandel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Domnívali	domnívat	k5eAaImAgMnP	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pták	pták	k1gMnSc1	pták
na	na	k7c6	na
mandelech	mandel	k1gInPc6	mandel
polyká	polykat	k5eAaImIp3nS	polykat
klasy	klasa	k1gFnPc4	klasa
<g/>
.	.	kIx.	.
</s>
<s>
Zdrobnělina	zdrobnělina	k1gFnSc1	zdrobnělina
slova	slovo	k1gNnSc2	slovo
mandel	mandel	k1gInSc4	mandel
je	být	k5eAaImIp3nS	být
mj.	mj.	kA	mj.
mandelík	mandelík	k1gInSc1	mandelík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
mandelík	mandelík	k1gInSc1	mandelík
hájní	hájní	k2eAgInSc1d1	hájní
či	či	k8xC	či
mandelík	mandelík	k1gInSc1	mandelík
obecný	obecný	k2eAgInSc1d1	obecný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
onomaziologických	onomaziologický	k2eAgFnPc2d1	onomaziologická
motivací	motivace	k1gFnPc2	motivace
je	být	k5eAaImIp3nS	být
pták	pták	k1gMnSc1	pták
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
podle	podle	k7c2	podle
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
doby	doba	k1gFnSc2	doba
vzniku	vznik	k1gInSc2	vznik
se	se	k3xPyFc4	se
jméno	jméno	k1gNnSc1	jméno
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
česká	český	k2eAgNnPc4d1	české
předobrozenecká	předobrozenecký	k2eAgNnPc4d1	předobrozenecký
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Video	video	k1gNnSc1	video
==	==	k?	==
</s>
</p>
<p>
<s>
Jarmila	Jarmila	k1gFnSc1	Jarmila
Kačírková	Kačírková	k1gFnSc1	Kačírková
<g/>
:	:	kIx,	:
Mandelík	mandelík	k1gInSc1	mandelík
hajní	hajní	k2eAgInSc1d1	hajní
(	(	kIx(	(
<g/>
Coracias	Coracias	k1gInSc1	Coracias
garrulus	garrulus	k1gInSc1	garrulus
<g/>
)	)	kIx)	)
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
2.5	[number]	k4	2.5
<g/>
.2019	.2019	k4	.2019
</s>
</p>
<p>
<s>
https://youtu.be/UdJ_l7y1FvY	[url]	k4	https://youtu.be/UdJ_l7y1FvY
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ČERNÝ	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Walter	Walter	k1gMnSc1	Walter
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
830	[number]	k4	830
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
146	[number]	k4	146
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FIALOVÁ	Fialová	k1gFnSc1	Fialová
KARPENKOVÁ	KARPENKOVÁ	kA	KARPENKOVÁ
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
názvy	název	k1gInPc1	název
ptáků	pták	k1gMnPc2	pták
z	z	k7c2	z
etymologického	etymologický	k2eAgNnSc2d1	etymologické
a	a	k8xC	a
slovotvorného	slovotvorný	k2eAgNnSc2d1	slovotvorné
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
131	[number]	k4	131
s.	s.	k?	s.
Diplomová	diplomový	k2eAgFnSc1d1	Diplomová
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Ved	Ved	k?	Ved
<g/>
.	.	kIx.	.
práce	práce	k1gFnSc2	práce
PhDr.	PhDr.	kA	PhDr.
Jiří	Jiří	k1gMnSc1	Jiří
Rejzek	Rejzek	k1gInSc1	Rejzek
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
FFUK	FFUK	kA	FFUK
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
teorie	teorie	k1gFnSc2	teorie
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GOSLER	GOSLER	kA	GOSLER
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gFnSc1	Andrew
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
ptáků	pták	k1gMnPc2	pták
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Príroda	Príroda	k1gFnSc1	Príroda
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7151	[number]	k4	7151
<g/>
-	-	kIx~	-
<g/>
258	[number]	k4	258
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
33	[number]	k4	33
<g/>
,	,	kIx,	,
166	[number]	k4	166
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HUDEC	Hudec	k1gMnSc1	Hudec
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Fauna	fauna	k1gFnSc1	fauna
ČSSR	ČSSR	kA	ČSSR
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
23	[number]	k4	23
<g/>
,	,	kIx,	,
Ptáci	pták	k1gMnPc1	pták
–	–	k?	–
Aves	Avesa	k1gFnPc2	Avesa
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
3	[number]	k4	3
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
s.	s.	k?	s.
159	[number]	k4	159
<g/>
–	–	k?	–
<g/>
166	[number]	k4	166
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mandelík	mandelík	k1gMnSc1	mandelík
hajní	hajní	k2eAgMnSc1d1	hajní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
mandelík	mandelík	k1gMnSc1	mandelík
hajní	hajní	k2eAgMnSc1d1	hajní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Coracias	Coracias	k1gInSc1	Coracias
garrulus	garrulus	k1gInSc1	garrulus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
mandelík	mandelík	k1gInSc1	mandelík
hajní	hajní	k2eAgInSc1d1	hajní
(	(	kIx(	(
<g/>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
)	)	kIx)	)
</s>
</p>
