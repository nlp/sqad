<s>
Jaké	jaký	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
nebo	nebo	k8xC
pseudonym	pseudonym	k1gInSc1
osoby	osoba	k1gFnSc2
nebo	nebo	k8xC
skupiny	skupina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
navrhla	navrhnout	k5eAaPmAgFnS
a	a	k8xC
vytvořila	vytvořit	k5eAaPmAgFnS
protokol	protokol	k1gInSc4
pro	pro	k7c4
Bitcoin	Bitcoin	k1gInSc4
<g/>
?	?	kIx.
</s>