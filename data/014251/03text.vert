<s>
Satoshi	Satosh	k1gFnSc5
Nakamoto	Nakamota	k1gFnSc5
</s>
<s>
Satoshi	Satosh	k1gFnSc5
Nakamoto	Nakamota	k1gFnSc5
Povolání	povolání	k1gNnPc1
</s>
<s>
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
kryptograf	kryptograf	k1gMnSc1
<g/>
,	,	kIx,
programátor	programátor	k1gMnSc1
a	a	k8xC
revolucionář	revolucionář	k1gMnSc1
Znám	znát	k5eAaImIp1nS
jako	jako	k9
</s>
<s>
vynálezce	vynálezce	k1gMnSc1
Bitcoinu	Bitcoin	k1gInSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Satoshi	Satoshi	k1gMnSc1
Nakamoto	Nakamoto	k1gMnSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
též	též	k9
Satoši	Satoš	k1gMnPc1
Nakamoto	Nakamota	k1gFnSc5
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
nebo	nebo	k8xC
pseudonym	pseudonym	k1gInSc4
osoby	osoba	k1gFnSc2
nebo	nebo	k8xC
skupiny	skupina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
navrhla	navrhnout	k5eAaPmAgFnS
a	a	k8xC
vytvořila	vytvořit	k5eAaPmAgFnS
protokol	protokol	k1gInSc4
pro	pro	k7c4
Bitcoin	Bitcoin	k2eAgInSc4d1
a	a	k8xC
potřebný	potřebný	k2eAgInSc4d1
software	software	k1gInSc4
<g/>
,	,	kIx,
Bitcoin-Qt	Bitcoin-Qt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
na	na	k7c6
e-mailové	e-mailové	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
metzdowd	metzdowd	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
zveřejnil	zveřejnit	k5eAaPmAgMnS
popis	popis	k1gInSc4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
digitální	digitální	k2eAgFnSc2d1
měny	měna	k1gFnSc2
Bitcoin	Bitcoina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2009	#num#	k4
vydal	vydat	k5eAaPmAgMnS
první	první	k4xOgInSc4
software	software	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
zahájil	zahájit	k5eAaPmAgInS
provoz	provoz	k1gInSc4
celé	celý	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
a	a	k8xC
prvních	první	k4xOgFnPc2
jednotek	jednotka	k1gFnPc2
Bitcoin	Bitcoina	k1gFnPc2
měny	měna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
skutečná	skutečný	k2eAgFnSc1d1
identita	identita	k1gFnSc1
není	být	k5eNaImIp3nS
známa	znám	k2eAgFnSc1d1
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
teorií	teorie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Nakamoto	Nakamota	k1gFnSc5
dále	daleko	k6eAd2
přispíval	přispívat	k5eAaImAgInS
do	do	k7c2
dalších	další	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
Bitcoin	Bitcoin	k1gInSc1
softwaru	software	k1gInSc2
společně	společně	k6eAd1
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
vývojáři	vývojář	k1gMnPc7
<g/>
,	,	kIx,
dokud	dokud	k8xS
se	se	k3xPyFc4
v	v	k7c6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
2010	#num#	k4
postupně	postupně	k6eAd1
nevytratil	vytratit	k5eNaPmAgInS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
kontakt	kontakt	k1gInSc4
s	s	k7c7
vývojářským	vývojářský	k2eAgInSc7d1
týmem	tým	k1gInSc7
a	a	k8xC
celou	celý	k2eAgFnSc7d1
komunitou	komunita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhruba	zhruba	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
předal	předat	k5eAaPmAgMnS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
úložištěm	úložiště	k1gNnSc7
zdrojového	zdrojový	k2eAgInSc2d1
kódu	kód	k1gInSc2
a	a	k8xC
klíčových	klíčový	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
softwaru	software	k1gInSc2
Gavinu	Gavin	k1gMnSc3
Andresenovi	Andresen	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
předal	předat	k5eAaPmAgMnS
doménu	doména	k1gFnSc4
Bitcoin	Bitcoina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
org	org	k?
a	a	k8xC
několik	několik	k4yIc4
dalších	další	k2eAgFnPc2d1
domén	doména	k1gFnPc2
do	do	k7c2
rukou	ruka	k1gFnPc2
předních	přední	k2eAgInPc2d1
členů	člen	k1gInPc2
komunity	komunita	k1gFnSc2
Bitcoinu	Bitcoin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
domněnek	domněnka	k1gFnPc2
by	by	kYmCp3nP
Nakamoto	Nakamota	k1gFnSc5
mohl	moct	k5eAaImAgInS
mít	mít	k5eAaImF
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
zhruba	zhruba	k6eAd1
jeden	jeden	k4xCgInSc1
milión	milión	k4xCgInSc1
bitcoinů	bitcoin	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
předpoklad	předpoklad	k1gInSc1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c4
existenci	existence	k1gFnSc4
bitcoinových	bitcoinův	k2eAgFnPc2d1
peněženek	peněženka	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
Nakamoto	Nakamota	k1gFnSc5
převáděl	převádět	k5eAaImAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
2009-2010	2009-2010	k4
vytěžené	vytěžený	k2eAgFnPc1d1
bitcoiny	bitcoina	k1gFnPc1
a	a	k8xC
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
jsou	být	k5eAaImIp3nP
bez	bez	k7c2
pohybu	pohyb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
roku	rok	k1gInSc2
2017	#num#	k4
by	by	kYmCp3nP
to	ten	k3xDgNnSc1
odpovídalo	odpovídat	k5eAaImAgNnS
asi	asi	k9
19	#num#	k4
miliardám	miliarda	k4xCgFnPc3
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
asi	asi	k9
390	#num#	k4
miliard	miliarda	k4xCgFnPc2
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
jisté	jistý	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
Nakamoto	Nakamota	k1gFnSc5
<g/>
,	,	kIx,
či	či	k8xC
někdo	někdo	k3yInSc1
jiný	jiný	k2eAgMnSc1d1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
privátní	privátní	k2eAgInPc4d1
klíče	klíč	k1gInPc4
k	k	k7c3
daným	daný	k2eAgFnPc3d1
peněženkám	peněženka	k1gFnPc3
<g/>
,	,	kIx,
nebo	nebo	k8xC
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgInPc1
bitcoiny	bitcoin	k1gInPc1
navždy	navždy	k6eAd1
zamknuté	zamknutý	k2eAgInPc1d1
(	(	kIx(
<g/>
tj.	tj.	kA
ztracené	ztracený	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Identita	identita	k1gFnSc1
</s>
<s>
Nejsou	být	k5eNaImIp3nP
žádné	žádný	k3yNgInPc4
záznamy	záznam	k1gInPc4
o	o	k7c4
existenci	existence	k1gFnSc4
identity	identita	k1gFnSc2
Nakamoto	Nakamota	k1gFnSc5
před	před	k7c7
vytvořením	vytvoření	k1gNnSc7
Bitcoinu	Bitcoin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Satoshi	Satoshi	k1gNnPc4
je	být	k5eAaImIp3nS
mužské	mužský	k2eAgNnSc1d1
japonské	japonský	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
význam	význam	k1gInSc1
má	mít	k5eAaImIp3nS
různé	různý	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
jako	jako	k8xS,k8xC
„	„	k?
<g/>
moudrý	moudrý	k2eAgInSc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
jasné	jasný	k2eAgNnSc1d1
myšlení	myšlení	k1gNnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
bystrý	bystrý	k2eAgInSc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
nebo	nebo	k8xC
„	„	k?
<g/>
osoba	osoba	k1gFnSc1
s	s	k7c7
inteligentními	inteligentní	k2eAgInPc7d1
předky	předek	k1gInPc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakamoto	Nakamota	k1gFnSc5
je	být	k5eAaImIp3nS
japonské	japonský	k2eAgNnSc4d1
příjmení	příjmení	k1gNnSc4
(	(	kIx(
<g/>
jméno	jméno	k1gNnSc4
rodiny	rodina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
svém	svůj	k3xOyFgInSc6
profilu	profil	k1gInSc6
u	u	k7c2
nadace	nadace	k1gFnSc2
P2P	P2P	k1gFnPc1
Foundation	Foundation	k1gInSc4
Nakamoto	Nakamota	k1gFnSc5
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
muž	muž	k1gMnSc1
ve	v	k7c6
věku	věk	k1gInSc6
37	#num#	k4
let	léto	k1gNnPc2
s	s	k7c7
japonskými	japonský	k2eAgInPc7d1
kořeny	kořen	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
ale	ale	k8xC
podnítilo	podnítit	k5eAaPmAgNnS
řadu	řada	k1gFnSc4
pochybností	pochybnost	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
používá	používat	k5eAaImIp3nS
jako	jako	k8xS,k8xC
jazyk	jazyk	k1gInSc1
angličtinu	angličtina	k1gFnSc4
a	a	k8xC
software	software	k1gInSc4
pro	pro	k7c4
Bitcoin	Bitcoin	k1gInSc4
nebyl	být	k5eNaImAgInS
zdokumentován	zdokumentovat	k5eAaPmNgInS
ani	ani	k8xC
vydán	vydat	k5eAaPmNgInS
v	v	k7c6
japonštině	japonština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Spekuluje	spekulovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
první	první	k4xOgNnSc4
vydání	vydání	k1gNnSc4
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
původního	původní	k2eAgInSc2d1
Bitcoinového	Bitcoinový	k2eAgInSc2d1
softwaru	software	k1gInSc2
je	být	k5eAaImIp3nS
dílo	dílo	k1gNnSc4
skupiny	skupina	k1gFnSc2
spolupracovníků	spolupracovník	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
někoho	někdo	k3yInSc4
vede	vést	k5eAaImIp3nS
k	k	k7c3
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
Satoshi	Satoshi	k1gNnSc1
Nakamoto	Nakamota	k1gFnSc5
byl	být	k5eAaImAgInS
kolektivní	kolektivní	k2eAgInSc4d1
pseudonym	pseudonym	k1gInSc4
skupiny	skupina	k1gFnSc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k6eAd1
v	v	k7c6
komentářích	komentář	k1gInPc6
zdrojového	zdrojový	k2eAgInSc2d1
kódu	kód	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
v	v	k7c6
příspěvcích	příspěvek	k1gInPc6
na	na	k7c6
fórech	fór	k1gInPc6
používá	používat	k5eAaImIp3nS
britskou	britský	k2eAgFnSc4d1
angličtinu	angličtina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
naznačuje	naznačovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
alespoň	alespoň	k9
jedna	jeden	k4xCgFnSc1
osoba	osoba	k1gFnSc1
ze	z	k7c2
sdružení	sdružení	k1gNnSc2
vydávající	vydávající	k2eAgMnSc1d1
se	se	k3xPyFc4
za	za	k7c4
něj	on	k3xPp3gInSc4
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
mít	mít	k5eAaImF
původ	původ	k1gInSc4
ze	z	k7c2
zemí	zem	k1gFnPc2
Commonwealthu	Commonwealth	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
jeho	jeho	k3xOp3gInPc2
příspěvků	příspěvek	k1gInPc2
obsahuje	obsahovat	k5eAaImIp3nS
anglický	anglický	k2eAgInSc1d1
výraz	výraz	k1gInSc1
„	„	k?
<g/>
Bloody	Blooda	k1gFnSc2
hard	hard	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
„	„	k?
<g/>
Bloody	Blooda	k1gFnSc2
<g/>
“	“	k?
je	být	k5eAaImIp3nS
slovo	slovo	k1gNnSc4
používané	používaný	k2eAgNnSc4d1
pro	pro	k7c4
klení	klení	k1gNnSc4
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
Irsku	Irsko	k1gNnSc6
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
Austrálii	Austrálie	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
Novém	nový	k2eAgInSc6d1
Zélandu	Zéland	k1gInSc6
a	a	k8xC
těmi	ten	k3xDgMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
mluví	mluvit	k5eAaImIp3nS
indickou	indický	k2eAgFnSc7d1
angličtinou	angličtina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
je	být	k5eAaImIp3nS
málokdy	málokdy	k6eAd1
slyšet	slyšet	k5eAaImF
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
mimo	mimo	k7c4
Newfoundland	Newfoundland	k1gInSc4
a	a	k8xC
Labrador	Labrador	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Stefan	Stefan	k1gMnSc1
Thomas	Thomas	k1gMnSc1
<g/>
,	,	kIx,
švýcarský	švýcarský	k2eAgInSc1d1
kodér	kodér	k1gInSc1
a	a	k8xC
aktivní	aktivní	k2eAgMnSc1d1
člen	člen	k1gMnSc1
komunity	komunita	k1gFnSc2
<g/>
,	,	kIx,
vytvořil	vytvořit	k5eAaPmAgMnS
graf	graf	k1gInSc4
s	s	k7c7
časy	čas	k1gInPc7
příspěvků	příspěvek	k1gInPc2
od	od	k7c2
Nakamota	Nakamot	k1gMnSc2
na	na	k7c6
bitcoinovém	bitcoinový	k2eAgNnSc6d1
fóru	fórum	k1gNnSc6
(	(	kIx(
<g/>
více	hodně	k6eAd2
než	než	k8xS
500	#num#	k4
příspěvků	příspěvek	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledek	výsledek	k1gInSc1
ukázal	ukázat	k5eAaPmAgInS
prudký	prudký	k2eAgInSc1d1
pokles	pokles	k1gInSc1
aktivity	aktivita	k1gFnSc2
<g/>
,	,	kIx,
až	až	k9
na	na	k7c4
téměř	téměř	k6eAd1
žádné	žádný	k3yNgInPc1
příspěvky	příspěvek	k1gInPc1
<g/>
,	,	kIx,
mezi	mezi	k7c7
5	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
11	#num#	k4
<g/>
.	.	kIx.
hodinou	hodina	k1gFnSc7
Greenwichského	greenwichský	k2eAgInSc2d1
středního	střední	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
Nakamoto	Nakamota	k1gFnSc5
jedinec	jedinec	k1gMnSc1
s	s	k7c7
běžnými	běžný	k2eAgInPc7d1
zvyky	zvyk	k1gInPc7
spaní	spaní	k1gNnSc2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
by	by	kYmCp3nS
se	se	k3xPyFc4
nacházet	nacházet	k5eAaImF
v	v	k7c6
časovém	časový	k2eAgNnSc6d1
pásmu	pásmo	k1gNnSc6
UTC	UTC	kA
<g/>
−	−	k?
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
nebo	nebo	k8xC
UTC	UTC	kA
<g/>
−	−	k?
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
odpovídá	odpovídat	k5eAaImIp3nS
východní	východní	k2eAgFnSc2d1
části	část	k1gFnSc2
severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
části	část	k1gFnSc2
Střední	střední	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
Karibiku	Karibik	k1gInSc2
a	a	k8xC
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2015	#num#	k4
vydal	vydat	k5eAaPmAgInS
časopis	časopis	k1gInSc1
Wired	Wired	k1gMnSc1
článek	článek	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
by	by	kYmCp3nS
Nakamotem	Nakamot	k1gMnSc7
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
australský	australský	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
a	a	k8xC
bývalý	bývalý	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
Craig	Craig	k1gMnSc1
Steven	Steven	k2eAgInSc4d1
Wright	Wright	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
k	k	k7c3
tomu	ten	k3xDgNnSc3
původně	původně	k6eAd1
nevyjadřoval	vyjadřovat	k5eNaImAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
květnu	květen	k1gInSc6
2016	#num#	k4
se	se	k3xPyFc4
za	za	k7c2
Satoshiho	Satoshi	k1gMnSc2
Nakamota	Nakamot	k1gMnSc2
veřejně	veřejně	k6eAd1
prohlásil	prohlásit	k5eAaPmAgInS
a	a	k8xC
před	před	k7c7
novináři	novinář	k1gMnPc7
provedl	provést	k5eAaPmAgInS
operace	operace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
podle	podle	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
tvrzení	tvrzení	k1gNnSc2
tuto	tento	k3xDgFnSc4
identitu	identita	k1gFnSc4
prokazují	prokazovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
prohlášení	prohlášení	k1gNnSc1
však	však	k9
rozporovali	rozporovat	k5eAaBmAgMnP,k5eAaImAgMnP,k5eAaPmAgMnP
zástupci	zástupce	k1gMnPc7
komunity	komunita	k1gFnSc2
Bitcoin	Bitcoin	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
Dan	Dan	k1gMnSc1
Kaminsky	Kaminsky	k1gMnSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wright	Wright	k1gInSc1
nikdy	nikdy	k6eAd1
nepředložil	předložit	k5eNaPmAgInS
žádný	žádný	k3yNgInSc4
věrohodný	věrohodný	k2eAgInSc4d1
důkaz	důkaz	k1gInSc4
a	a	k8xC
pro	pro	k7c4
své	svůj	k3xOyFgNnSc4
obecně	obecně	k6eAd1
jednání	jednání	k1gNnSc1
(	(	kIx(
<g/>
právní	právní	k2eAgInPc1d1
spory	spor	k1gInPc1
<g/>
,	,	kIx,
snaha	snaha	k1gFnSc1
o	o	k7c4
patenty	patent	k1gInPc4
<g/>
,	,	kIx,
neprůkazná	průkazný	k2eNgNnPc4d1
prohlášení	prohlášení	k1gNnPc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
bitcoinovou	bitcoinový	k2eAgFnSc4d1
komunitou	komunita	k1gFnSc7
přezdíváno	přezdíván	k2eAgNnSc4d1
"	"	kIx"
<g/>
Faketoshi	Faketoshi	k1gNnSc4
<g/>
"	"	kIx"
a	a	k8xC
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
jedny	jeden	k4xCgFnPc4
z	z	k7c2
nejméně	málo	k6eAd3
oblíbených	oblíbený	k2eAgInPc2d1
členů	člen	k1gInPc2
komunity	komunita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Satoshi	Satosh	k1gFnSc2
Nakamoto	Nakamota	k1gFnSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Bitcoin	Bitcoin	k1gInSc1
oslaví	oslavit	k5eAaPmIp3nS
kulaté	kulatý	k2eAgNnSc4d1
výročí	výročí	k1gNnSc4
aneb	aneb	k?
Za	za	k7c4
deset	deset	k4xCc4
let	léto	k1gNnPc2
od	od	k7c2
nápadu	nápad	k1gInSc2
ke	k	k7c3
stamiliardovému	stamiliardový	k2eAgInSc3d1
byznysu	byznys	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Investičníweb	Investičníwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-11-16	2018-11-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
překlad	překlad	k1gInSc4
originálního	originální	k2eAgInSc2d1
textu	text	k1gInSc2
"	"	kIx"
<g/>
Bitcoin	Bitcoin	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Peer-to-Peer	Peer-to-Peer	k1gInSc1
Electronic	Electronice	k1gFnPc2
Cash	cash	k1gFnPc2
System	Syst	k1gInSc7
<g/>
"	"	kIx"
jehož	jehož	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
Satoshi	Satoshi	k1gNnPc4
Nakamoto	Nakamota	k1gFnSc5
<g/>
,	,	kIx,
vynálezce	vynálezce	k1gMnSc1
Bitcoinu	Bitcoin	k1gInSc2
<g/>
↑	↑	k?
Tweet	Tweet	k1gMnSc1
„	„	k?
<g/>
Bitcoin	Bitcoin	k1gMnSc1
Core	Cor	k1gFnSc2
Project	Project	k1gMnSc1
<g/>
“	“	k?
na	na	k7c6
Twitteru	Twitter	k1gInSc6
<g/>
↑	↑	k?
Pearson	Pearson	k1gMnSc1
<g/>
,	,	kIx,
Jordan	Jordan	k1gMnSc1
<g/>
;	;	kIx,
Franceschi-Bicchierai	Franceschi-Bicchierai	k1gNnSc1
<g/>
,	,	kIx,
Lorenzo	Lorenza	k1gFnSc5
<g/>
:	:	kIx,
„	„	k?
<g/>
Craig	Craig	k1gMnSc1
Wright	Wright	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
New	New	k1gFnSc7
Evidence	evidence	k1gFnSc1
That	That	k1gMnSc1
He	he	k0
Is	Is	k1gMnSc1
Satoshi	Satosh	k1gMnPc1
Nakamoto	Nakamota	k1gFnSc5
Is	Is	k1gFnPc7
Worthless	Worthless	k1gInSc1
<g/>
,	,	kIx,
Motherboard	motherboard	k1gInSc1
<g/>
,	,	kIx,
2016-05-02	2016-05-02	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
vse	vse	k?
<g/>
2015890534	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
121911801X	121911801X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nb	nb	k?
<g/>
2018014025	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
12149365879685600396	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nb	lccn-nb	k1gInSc1
<g/>
2018014025	#num#	k4
</s>
