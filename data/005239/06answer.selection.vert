<s>
Informatik	informatik	k1gMnSc1	informatik
je	být	k5eAaImIp3nS	být
vědec	vědec	k1gMnSc1	vědec
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
informatikou	informatika	k1gFnSc7	informatika
<g/>
,	,	kIx,	,
teoretickou	teoretický	k2eAgFnSc7d1	teoretická
vědou	věda	k1gFnSc7	věda
o	o	k7c6	o
informacích	informace	k1gFnPc6	informace
a	a	k8xC	a
práci	práce	k1gFnSc3	práce
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
