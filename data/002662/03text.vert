<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Slovenska	Slovensko	k1gNnSc2	Slovensko
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Preßburg	Preßburg	k1gInSc1	Preßburg
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Pozsony	Pozsona	k1gFnSc2	Pozsona
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Posonium	Posonium	k1gNnSc1	Posonium
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
slovensky	slovensky	k6eAd1	slovensky
Prešporok	Prešporok	k1gInSc1	Prešporok
<g/>
/	/	kIx~	/
<g/>
Prešporek	Prešporek	k1gInSc1	Prešporek
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
česky	česky	k6eAd1	česky
Prešpurk	Prešpurk	k?	Prešpurk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
Bratislavského	bratislavský	k2eAgInSc2d1	bratislavský
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
historická	historický	k2eAgFnSc1d1	historická
metropole	metropole	k1gFnSc1	metropole
někdejších	někdejší	k2eAgFnPc2d1	někdejší
žup	župa	k1gFnPc2	župa
Prešpurské	prešpurský	k2eAgFnSc2d1	prešpurský
a	a	k8xC	a
Bratislavské	bratislavský	k2eAgFnSc2d1	Bratislavská
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
,	,	kIx,	,
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
při	při	k7c6	při
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
417389	[number]	k4	417389
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
centrály	centrála	k1gFnSc2	centrála
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
finanční	finanční	k2eAgFnSc2d1	finanční
korporace	korporace	k1gFnSc2	korporace
(	(	kIx(	(
<g/>
IFC	IFC	kA	IFC
<g/>
)	)	kIx)	)
-	-	kIx~	-
organizace	organizace	k1gFnSc1	organizace
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
zde	zde	k6eAd1	zde
sídli	sídlet	k5eAaImRp2nS	sídlet
Rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
program	program	k1gInSc1	program
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
Společenství	společenství	k1gNnSc4	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
UNDP	UNDP	kA	UNDP
Europe	Europ	k1gInSc5	Europ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
v	v	k7c6	v
domě	dům	k1gInSc6	dům
OSN	OSN	kA	OSN
na	na	k7c6	na
Grösslingově	Grösslingově	k1gFnSc6	Grösslingově
ulici	ulice	k1gFnSc6	ulice
sídli	sídlet	k5eAaImRp2nS	sídlet
také	také	k9	také
Populační	populační	k2eAgInSc1d1	populační
fond	fond	k1gInSc1	fond
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
západní	západní	k2eAgFnSc4d1	západní
Asii	Asie	k1gFnSc4	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
Bratislavy	Bratislava	k1gFnSc2	Bratislava
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazen	k2eAgMnSc1d1	vyobrazen
i	i	k8xC	i
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
logu	log	k1gInSc6	log
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Bratislavský	bratislavský	k2eAgInSc1d1	bratislavský
hrad	hrad	k1gInSc1	hrad
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
věžemi	věž	k1gFnPc7	věž
v	v	k7c6	v
rozích	roh	k1gInPc6	roh
a	a	k8xC	a
jedním	jeden	k4xCgNnSc7	jeden
nádvořím	nádvoří	k1gNnSc7	nádvoří
uprostřed	uprostřed	k7c2	uprostřed
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
architektonicky	architektonicky	k6eAd1	architektonicky
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
Most	most	k1gInSc1	most
SNP	SNP	kA	SNP
(	(	kIx(	(
<g/>
postavený	postavený	k2eAgInSc1d1	postavený
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klenoucí	klenoucí	k2eAgFnSc4d1	klenoucí
se	se	k3xPyFc4	se
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
mostů	most	k1gInPc2	most
přes	přes	k7c4	přes
Bratislavou	Bratislava	k1gFnSc7	Bratislava
protékající	protékající	k2eAgInSc1d1	protékající
Dunaj	Dunaj	k1gInSc1	Dunaj
nedaleko	nedaleko	k7c2	nedaleko
historického	historický	k2eAgNnSc2d1	historické
jádra	jádro	k1gNnSc2	jádro
metropole	metropol	k1gFnSc2	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc4	seznam
historických	historický	k2eAgInPc2d1	historický
názvů	název	k1gInPc2	název
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
známý	známý	k2eAgInSc1d1	známý
název	název	k1gInSc1	název
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Braslavespurch	Braslavespurch	k1gInSc1	Braslavespurch
(	(	kIx(	(
<g/>
907	[number]	k4	907
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
zachovaná	zachovaný	k2eAgNnPc1d1	zachované
pojmenování	pojmenování	k1gNnPc1	pojmenování
jsou	být	k5eAaImIp3nP	být
Brezalauspurch	Brezalauspurch	k1gInSc4	Brezalauspurch
<g/>
,	,	kIx,	,
Poson	Poson	k1gNnSc4	Poson
(	(	kIx(	(
<g/>
1002	[number]	k4	1002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brezesburg	Brezesburg	k1gMnSc1	Brezesburg
(	(	kIx(	(
<g/>
1042	[number]	k4	1042
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bosenburg	Bosenburg	k1gMnSc1	Bosenburg
(	(	kIx(	(
<g/>
1045	[number]	k4	1045
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brecesburg	Brecesburg	k1gMnSc1	Brecesburg
(	(	kIx(	(
<g/>
1048	[number]	k4	1048
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bresburc	Bresburc	k1gInSc1	Bresburc
<g/>
,	,	kIx,	,
Brezisburg	Brezisburg	k1gMnSc1	Brezisburg
<g/>
,	,	kIx,	,
Preslawaspurch	Preslawaspurch	k1gMnSc1	Preslawaspurch
(	(	kIx(	(
<g/>
1052	[number]	k4	1052
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
latinsky	latinsky	k6eAd1	latinsky
Posonium	Posonium	k1gNnSc1	Posonium
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Istropolis	Istropolis	k1gFnSc1	Istropolis
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Pozsony	Pozsona	k1gFnSc2	Pozsona
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Preßburg	Preßburg	k1gInSc1	Preßburg
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
布	布	k?	布
;	;	kIx,	;
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pojmenování	pojmenování	k1gNnSc4	pojmenování
Prešpurk	Prešpurk	k?	Prešpurk
či	či	k8xC	či
Prešporok	Prešporok	k1gInSc1	Prešporok
<g/>
.	.	kIx.	.
</s>
<s>
Slangově	slangově	k6eAd1	slangově
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
Blava	Blava	k1gFnSc1	Blava
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
Podunajské	podunajský	k2eAgFnSc2d1	Podunajská
nížiny	nížina	k1gFnSc2	nížina
<g/>
,	,	kIx,	,
v	v	k7c6	v
předhůří	předhůří	k1gNnSc6	předhůří
Malých	Malých	k2eAgInPc2d1	Malých
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
soutoku	soutok	k1gInSc2	soutok
Moravy	Morava	k1gFnSc2	Morava
s	s	k7c7	s
Dunajem	Dunaj	k1gInSc7	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Malé	Malé	k2eAgInPc1d1	Malé
Karpaty	Karpaty	k1gInPc1	Karpaty
představují	představovat	k5eAaImIp3nP	představovat
část	část	k1gFnSc4	část
jen	jen	k9	jen
málo	málo	k1gNnSc4	málo
dotčené	dotčený	k2eAgFnSc2d1	dotčená
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
do	do	k7c2	do
metropole	metropol	k1gFnSc2	metropol
zasahující	zasahující	k2eAgInSc4d1	zasahující
jako	jako	k8xS	jako
klín	klín	k1gInSc4	klín
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
upravené	upravený	k2eAgFnPc1d1	upravená
jako	jako	k8xC	jako
velký	velký	k2eAgInSc1d1	velký
lesopark	lesopark	k1gInSc1	lesopark
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
Dunaje	Dunaj	k1gInSc2	Dunaj
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
slepých	slepý	k2eAgNnPc2d1	slepé
ramen	rameno	k1gNnPc2	rameno
s	s	k7c7	s
lužními	lužní	k2eAgInPc7d1	lužní
lesy	les	k1gInPc7	les
a	a	k8xC	a
nivami	niva	k1gFnPc7	niva
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byly	být	k5eAaImAgFnP	být
rekultivovány	rekultivován	k2eAgInPc1d1	rekultivován
a	a	k8xC	a
přebudovány	přebudován	k2eAgInPc1d1	přebudován
jako	jako	k8xS	jako
parky	park	k1gInPc1	park
<g/>
.	.	kIx.	.
46,8	[number]	k4	46,8
km2	km2	k4	km2
z	z	k7c2	z
rozlohy	rozloha	k1gFnSc2	rozloha
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
110	[number]	k4	110
m2	m2	k4	m2
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
právě	právě	k9	právě
příroda	příroda	k1gFnSc1	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Rovinatý	rovinatý	k2eAgInSc1d1	rovinatý
charakter	charakter	k1gInSc1	charakter
krajiny	krajina	k1gFnSc2	krajina
umožnil	umožnit	k5eAaPmAgInS	umožnit
rozšíření	rozšíření	k1gNnSc4	rozšíření
Bratislavy	Bratislava	k1gFnSc2	Bratislava
ze	z	k7c2	z
středověkého	středověký	k2eAgNnSc2d1	středověké
města	město	k1gNnSc2	město
na	na	k7c6	na
moderní	moderní	k2eAgFnSc6d1	moderní
metropoli	metropol	k1gFnSc6	metropol
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
veletoku	veletok	k1gInSc2	veletok
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
,	,	kIx,	,
se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
rozlohou	rozloha	k1gFnSc7	rozloha
367	[number]	k4	367
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Omezení	omezení	k1gNnSc1	omezení
růstu	růst	k1gInSc2	růst
jsou	být	k5eAaImIp3nP	být
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
spíše	spíše	k9	spíše
politického	politický	k2eAgInSc2d1	politický
charakteru	charakter	k1gInSc2	charakter
(	(	kIx(	(
<g/>
státní	státní	k2eAgFnSc1d1	státní
hranice	hranice	k1gFnSc1	hranice
<g/>
)	)	kIx)	)
-	-	kIx~	-
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
Rakousko	Rakousko	k1gNnSc1	Rakousko
a	a	k8xC	a
jižním	jižní	k2eAgInSc7d1	jižní
pak	pak	k6eAd1	pak
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
jediné	jediný	k2eAgNnSc1d1	jediné
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
hraničí	hraničit	k5eAaImIp3nP	hraničit
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Poloha	poloha	k1gFnSc1	poloha
Bratislavy	Bratislava	k1gFnSc2	Bratislava
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
Podunajské	podunajský	k2eAgFnSc2d1	Podunajská
nížiny	nížina	k1gFnSc2	nížina
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
činí	činit	k5eAaImIp3nS	činit
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
měst	město	k1gNnPc2	město
s	s	k7c7	s
nejteplejším	teplý	k2eAgNnSc7d3	nejteplejší
podnebím	podnebí	k1gNnSc7	podnebí
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
zemědělsky	zemědělsky	k6eAd1	zemědělsky
intenzivně	intenzivně	k6eAd1	intenzivně
využíváno	využívat	k5eAaImNgNnS	využívat
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejúrodnějším	úrodný	k2eAgFnPc3d3	nejúrodnější
částem	část	k1gFnPc3	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
okolo	okolo	k7c2	okolo
9,9	[number]	k4	9,9
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
21	[number]	k4	21
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
pak	pak	k6eAd1	pak
-	-	kIx~	-
°	°	k?	°
<g/>
C.	C.	kA	C.
Sluneční	sluneční	k2eAgInSc1d1	sluneční
svit	svit	k1gInSc1	svit
zde	zde	k6eAd1	zde
činí	činit	k5eAaImIp3nS	činit
průměrně	průměrně	k6eAd1	průměrně
2447	[number]	k4	2447
hodin	hodina	k1gFnPc2	hodina
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
pak	pak	k6eAd1	pak
401	[number]	k4	401
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
Bratislavy	Bratislava	k1gFnSc2	Bratislava
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
již	již	k9	již
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
neolitu	neolit	k1gInSc2	neolit
<g/>
,	,	kIx,	,
souvislé	souvislý	k2eAgNnSc1d1	souvislé
osídlení	osídlení	k1gNnSc1	osídlení
je	být	k5eAaImIp3nS	být
však	však	k9	však
datováno	datovat	k5eAaImNgNnS	datovat
až	až	k6eAd1	až
z	z	k7c2	z
období	období	k1gNnSc2	období
let	let	k1gInSc4	let
400	[number]	k4	400
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
až	až	k9	až
50	[number]	k4	50
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
tu	tu	k6eAd1	tu
žili	žít	k5eAaImAgMnP	žít
Keltové	Kelt	k1gMnPc1	Kelt
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
zde	zde	k6eAd1	zde
dokonce	dokonce	k9	dokonce
oppidum	oppidum	k1gNnSc1	oppidum
(	(	kIx(	(
<g/>
opevněné	opevněný	k2eAgNnSc1d1	opevněné
sídliště	sídliště	k1gNnSc1	sídliště
<g/>
)	)	kIx)	)
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
mincovnou	mincovna	k1gFnSc7	mincovna
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
nahradili	nahradit	k5eAaPmAgMnP	nahradit
později	pozdě	k6eAd2	pozdě
Germáni	Germán	k1gMnPc1	Germán
<g/>
;	;	kIx,	;
středem	středem	k7c2	středem
řeky	řeka	k1gFnSc2	řeka
poté	poté	k6eAd1	poté
procházela	procházet	k5eAaImAgFnS	procházet
hranice	hranice	k1gFnSc1	hranice
jejich	jejich	k3xOp3gFnSc2	jejich
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Říma	Řím	k1gInSc2	Řím
následovalo	následovat	k5eAaImAgNnS	následovat
stěhování	stěhování	k1gNnSc1	stěhování
národů	národ	k1gInPc2	národ
a	a	k8xC	a
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Bratislavy	Bratislava	k1gFnSc2	Bratislava
přišli	přijít	k5eAaPmAgMnP	přijít
Slované	Slovan	k1gMnPc1	Slovan
a	a	k8xC	a
také	také	k6eAd1	také
Avaři	Avar	k1gMnPc1	Avar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Sámovy	Sámův	k2eAgFnSc2d1	Sámova
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Nitranského	nitranský	k2eAgNnSc2d1	Nitranské
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
říši	říš	k1gFnSc3	říš
Velkomoravské	velkomoravský	k2eAgFnSc3d1	Velkomoravská
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
na	na	k7c4	na
tisíc	tisíc	k4xCgInSc4	tisíc
let	let	k1gInSc4	let
součástí	součást	k1gFnSc7	součást
uherského	uherský	k2eAgInSc2d1	uherský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
prvního	první	k4xOgMnSc2	první
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Štěpána	Štěpán	k1gMnSc2	Štěpán
I.	I.	kA	I.
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
náboženskému	náboženský	k2eAgMnSc3d1	náboženský
<g/>
;	;	kIx,	;
budovaly	budovat	k5eAaImAgInP	budovat
se	se	k3xPyFc4	se
nové	nový	k2eAgInPc1d1	nový
kostely	kostel	k1gInPc1	kostel
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
i	i	k9	i
kamenný	kamenný	k2eAgInSc1d1	kamenný
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
opevněných	opevněný	k2eAgInPc2d1	opevněný
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Uhersku	Uhersko	k1gNnSc6	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
přetrvával	přetrvávat	k5eAaImAgInS	přetrvávat
význam	význam	k1gInSc1	význam
města	město	k1gNnSc2	město
z	z	k7c2	z
časů	čas	k1gInPc2	čas
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
se	se	k3xPyFc4	se
jako	jako	k9	jako
Poson	Poson	k1gMnSc1	Poson
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
Pozsony	Pozsona	k1gFnSc2	Pozsona
následně	následně	k6eAd1	následně
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
jako	jako	k9	jako
provinční	provinční	k2eAgNnSc1d1	provinční
uherské	uherský	k2eAgNnSc1d1	Uherské
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1241	[number]	k4	1241
a	a	k8xC	a
1242	[number]	k4	1242
na	na	k7c4	na
Bratislavu	Bratislava	k1gFnSc4	Bratislava
během	během	k7c2	během
svých	svůj	k3xOyFgInPc2	svůj
nájezdů	nájezd	k1gInPc2	nájezd
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
Mongolové	Mongol	k1gMnPc1	Mongol
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
město	město	k1gNnSc1	město
ani	ani	k8xC	ani
hrad	hrad	k1gInSc4	hrad
nedobyli	dobýt	k5eNaPmAgMnP	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Zpustošení	zpustošení	k1gNnSc1	zpustošení
okolí	okolí	k1gNnSc2	okolí
města	město	k1gNnSc2	město
však	však	k8xC	však
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
přísunu	přísun	k1gInSc3	přísun
německých	německý	k2eAgMnPc2d1	německý
kolonistů	kolonista	k1gMnPc2	kolonista
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k9	i
k	k	k7c3	k
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
1291	[number]	k4	1291
<g/>
)	)	kIx)	)
udělil	udělit	k5eAaPmAgMnS	udělit
král	král	k1gMnSc1	král
Ondřej	Ondřej	k1gMnSc1	Ondřej
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Bratislavě	Bratislava	k1gFnSc6	Bratislava
městská	městský	k2eAgNnPc1d1	Městské
práva	právo	k1gNnPc1	právo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1436	[number]	k4	1436
udělil	udělit	k5eAaPmAgMnS	udělit
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
městu	město	k1gNnSc3	město
vlastní	vlastní	k2eAgInSc4d1	vlastní
erb	erb	k1gInSc4	erb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1465	[number]	k4	1465
založil	založit	k5eAaPmAgMnS	založit
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
Academii	academia	k1gFnSc4	academia
Istropolitanu	Istropolitan	k1gInSc2	Istropolitan
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
univerzitu	univerzita	k1gFnSc4	univerzita
na	na	k7c6	na
území	území	k1gNnSc6	území
Uherska	Uhersko	k1gNnSc2	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
takto	takto	k6eAd1	takto
byla	být	k5eAaImAgFnS	být
podpořena	podpořen	k2eAgFnSc1d1	podpořena
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
předtím	předtím	k6eAd1	předtím
zdecimovaly	zdecimovat	k5eAaPmAgInP	zdecimovat
vojenské	vojenský	k2eAgInPc1d1	vojenský
boje	boj	k1gInPc1	boj
a	a	k8xC	a
povodně	povodeň	k1gFnPc1	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
nárůst	nárůst	k1gInSc1	nárůst
jejího	její	k3xOp3gInSc2	její
významu	význam	k1gInSc2	význam
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
Uherské	uherský	k2eAgNnSc4d1	Uherské
království	království	k1gNnSc4	království
však	však	k9	však
přišel	přijít	k5eAaPmAgMnS	přijít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vpádem	vpád	k1gInSc7	vpád
Osmanů	Osman	k1gMnPc2	Osman
do	do	k7c2	do
Uherska	Uhersko	k1gNnSc2	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Budín	Budín	k1gInSc1	Budín
totiž	totiž	k9	totiž
padl	padnout	k5eAaImAgInS	padnout
a	a	k8xC	a
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
jej	on	k3xPp3gMnSc4	on
zabrala	zabrat	k5eAaPmAgFnS	zabrat
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgFnPc1d1	důležitá
instituce	instituce	k1gFnPc1	instituce
tak	tak	k9	tak
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
přesunuty	přesunout	k5eAaPmNgFnP	přesunout
právě	právě	k6eAd1	právě
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1536	[number]	k4	1536
<g/>
-	-	kIx~	-
<g/>
1784	[number]	k4	1784
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Korunováno	korunován	k2eAgNnSc1d1	korunováno
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
králů	král	k1gMnPc2	král
a	a	k8xC	a
sídlil	sídlit	k5eAaImAgInS	sídlit
tu	tu	k6eAd1	tu
také	také	k9	také
ostřihomský	ostřihomský	k2eAgMnSc1d1	ostřihomský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
morové	morový	k2eAgFnSc6d1	morová
epidemii	epidemie	k1gFnSc6	epidemie
roku	rok	k1gInSc2	rok
1711	[number]	k4	1711
tu	tu	k6eAd1	tu
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
3860	[number]	k4	3860
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
počest	počest	k1gFnSc4	počest
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
sloup	sloup	k1gInSc1	sloup
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
období	období	k1gNnSc1	období
všestranného	všestranný	k2eAgInSc2d1	všestranný
rozvoje	rozvoj	k1gInSc2	rozvoj
a	a	k8xC	a
rozkvětu	rozkvět	k1gInSc2	rozkvět
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pozici	pozice	k1gFnSc3	pozice
metropole	metropol	k1gFnSc2	metropol
Uherska	Uhersko	k1gNnSc2	Uhersko
se	se	k3xPyFc4	se
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
budují	budovat	k5eAaImIp3nP	budovat
nové	nový	k2eAgFnPc4d1	nová
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
daří	dařit	k5eAaImIp3nS	dařit
se	se	k3xPyFc4	se
obchodu	obchod	k1gInSc2	obchod
i	i	k8xC	i
řemeslům	řemeslo	k1gNnPc3	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
osvícenské	osvícenský	k2eAgFnSc2d1	osvícenská
vlády	vláda	k1gFnSc2	vláda
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
tak	tak	k6eAd1	tak
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
ke	k	k7c3	k
zbourání	zbourání	k1gNnSc3	zbourání
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
první	první	k4xOgInPc1	první
parky	park	k1gInPc1	park
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dalších	další	k2eAgNnPc2d1	další
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
přikázal	přikázat	k5eAaPmAgInS	přikázat
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Uherska	Uhersko	k1gNnSc2	Uhersko
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Budína	Budín	k1gInSc2	Budín
a	a	k8xC	a
korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
celé	celý	k2eAgFnSc2d1	celá
Bratislavy	Bratislava	k1gFnSc2	Bratislava
tak	tak	k9	tak
velmi	velmi	k6eAd1	velmi
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
napoleonské	napoleonský	k2eAgFnPc1d1	napoleonská
války	válka	k1gFnPc1	válka
zanechaly	zanechat	k5eAaPmAgFnP	zanechat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
svoji	svůj	k3xOyFgFnSc4	svůj
stopu	stopa	k1gFnSc4	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
přes	přes	k7c4	přes
Bratislavu	Bratislava	k1gFnSc4	Bratislava
táhla	táhlo	k1gNnSc2	táhlo
vojska	vojsko	k1gNnSc2	vojsko
ruská	ruský	k2eAgFnSc1d1	ruská
(	(	kIx(	(
<g/>
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
i	i	k8xC	i
síly	síla	k1gFnPc4	síla
francouzského	francouzský	k2eAgNnSc2d1	francouzské
císařství	císařství	k1gNnSc2	císařství
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
podepsání	podepsání	k1gNnSc2	podepsání
Prešpurského	prešpurský	k2eAgInSc2d1	prešpurský
míru	mír	k1gInSc2	mír
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1805	[number]	k4	1805
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
však	však	k9	však
město	město	k1gNnSc4	město
Napoleonova	Napoleonův	k2eAgNnSc2d1	Napoleonovo
vojska	vojsko	k1gNnSc2	vojsko
opět	opět	k6eAd1	opět
obléhala	obléhat	k5eAaImAgFnS	obléhat
i	i	k8xC	i
bombardovala	bombardovat	k5eAaImAgFnS	bombardovat
<g/>
;	;	kIx,	;
následně	následně	k6eAd1	následně
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
podepsáno	podepsat	k5eAaPmNgNnS	podepsat
příměří	příměří	k1gNnSc1	příměří
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
i	i	k9	i
hrad	hrad	k1gInSc1	hrad
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
<g/>
;	;	kIx,	;
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházela	nacházet	k5eAaImAgNnP	nacházet
vojenská	vojenský	k2eAgNnPc1d1	vojenské
kasárna	kasárna	k1gNnPc1	kasárna
a	a	k8xC	a
díky	dík	k1gInPc1	dík
neopatrnosti	neopatrnost	k1gFnSc2	neopatrnost
vojáků	voják	k1gMnPc2	voják
při	při	k7c6	při
zacházení	zacházení	k1gNnSc6	zacházení
s	s	k7c7	s
ohněm	oheň	k1gInSc7	oheň
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
požár	požár	k1gInSc1	požár
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
přebudován	přebudovat	k5eAaPmNgInS	přebudovat
až	až	k9	až
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
však	však	k9	však
i	i	k9	i
sem	sem	k6eAd1	sem
dorazil	dorazit	k5eAaPmAgMnS	dorazit
rozvoj	rozvoj	k1gInSc4	rozvoj
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
se	s	k7c7	s
Svätým	Svätý	k2eAgMnSc7d1	Svätý
Jurem	Jurem	k?	Jurem
spojené	spojený	k2eAgFnPc1d1	spojená
koněspřežkou	koněspřežka	k1gFnSc7	koněspřežka
<g/>
.	.	kIx.	.
</s>
<s>
Rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
se	se	k3xPyFc4	se
také	také	k9	také
buditelské	buditelský	k2eAgFnPc1d1	buditelská
aktivity	aktivita	k1gFnPc1	aktivita
mezi	mezi	k7c4	mezi
Slováky	Slovák	k1gMnPc4	Slovák
a	a	k8xC	a
město	město	k1gNnSc4	město
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
centrem	centrum	k1gNnSc7	centrum
slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
revolučním	revoluční	k2eAgInSc6d1	revoluční
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
sehrála	sehrát	k5eAaPmAgFnS	sehrát
Bratislava	Bratislava	k1gFnSc1	Bratislava
svoji	svůj	k3xOyFgFnSc4	svůj
ne	ne	k9	ne
zrovna	zrovna	k6eAd1	zrovna
nevýznamnou	významný	k2eNgFnSc4d1	nevýznamná
úlohu	úloha	k1gFnSc4	úloha
<g/>
;	;	kIx,	;
byly	být	k5eAaImAgFnP	být
tu	tu	k6eAd1	tu
přijaty	přijmout	k5eAaPmNgFnP	přijmout
tzv.	tzv.	kA	tzv.
březnové	březnový	k2eAgInPc4d1	březnový
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zrušily	zrušit	k5eAaPmAgFnP	zrušit
poddanství	poddanství	k1gNnSc4	poddanství
v	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
je	být	k5eAaImIp3nS	být
podepsal	podepsat	k5eAaPmAgMnS	podepsat
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
císař	císař	k1gMnSc1	císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
V.	V.	kA	V.
Dobrotivý	dobrotivý	k2eAgInSc1d1	dobrotivý
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
Primaciálním	primaciální	k2eAgInSc6d1	primaciální
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
samotné	samotný	k2eAgFnSc6d1	samotná
revoluci	revoluce	k1gFnSc6	revoluce
se	se	k3xPyFc4	se
Bratislava	Bratislava	k1gFnSc1	Bratislava
postavila	postavit	k5eAaPmAgFnS	postavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Uhrů	Uher	k1gMnPc2	Uher
a	a	k8xC	a
proti	proti	k7c3	proti
Vídni	Vídeň	k1gFnSc3	Vídeň
<g/>
;	;	kIx,	;
následně	následně	k6eAd1	následně
však	však	k9	však
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
byla	být	k5eAaImAgFnS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
vojsky	vojsky	k6eAd1	vojsky
Windischgrätze	Windischgrätze	k1gFnSc1	Windischgrätze
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
i	i	k8xC	i
ruskými	ruský	k2eAgFnPc7d1	ruská
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
konal	konat	k5eAaImAgInS	konat
soud	soud	k1gInSc4	soud
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
účastníky	účastník	k1gMnPc7	účastník
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
;	;	kIx,	;
ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
buď	buď	k8xC	buď
odsouzeni	odsoudit	k5eAaPmNgMnP	odsoudit
k	k	k7c3	k
vězení	vězení	k1gNnSc3	vězení
(	(	kIx(	(
<g/>
stovky	stovka	k1gFnPc1	stovka
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
popraveni	popraven	k2eAgMnPc1d1	popraven
(	(	kIx(	(
<g/>
13	[number]	k4	13
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
měnilo	měnit	k5eAaImAgNnS	měnit
i	i	k9	i
postavení	postavení	k1gNnSc4	postavení
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
byla	být	k5eAaImAgFnS	být
sídlem	sídlo	k1gNnSc7	sídlo
tzv.	tzv.	kA	tzv.
bratislavského	bratislavský	k2eAgInSc2d1	bratislavský
distriktu	distrikt	k1gInSc2	distrikt
(	(	kIx(	(
<g/>
nejdříve	dříve	k6eAd3	dříve
vojenského	vojenský	k2eAgNnSc2d1	vojenské
a	a	k8xC	a
potom	potom	k6eAd1	potom
civilního	civilní	k2eAgNnSc2d1	civilní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
město	město	k1gNnSc1	město
soudní	soudní	k2eAgFnSc2d1	soudní
pravomoci	pravomoc	k1gFnSc2	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
stala	stát	k5eAaPmAgFnS	stát
Bratislava	Bratislava	k1gFnSc1	Bratislava
tzv.	tzv.	kA	tzv.
municipálním	municipální	k2eAgNnSc7d1	municipální
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
nový	nový	k2eAgInSc4d1	nový
statut	statut	k1gInSc4	statut
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
měst	město	k1gNnPc2	město
královských	královský	k2eAgNnPc2d1	královské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
města	město	k1gNnSc2	město
tak	tak	k6eAd1	tak
nyní	nyní	k6eAd1	nyní
stál	stát	k5eAaImAgMnS	stát
župan	župan	k1gMnSc1	župan
<g/>
.	.	kIx.	.
</s>
<s>
Technický	technický	k2eAgInSc1d1	technický
pokrok	pokrok	k1gInSc1	pokrok
postupoval	postupovat	k5eAaImAgInS	postupovat
mílovými	mílový	k2eAgInPc7d1	mílový
kroky	krok	k1gInPc7	krok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1848	[number]	k4	1848
<g/>
-	-	kIx~	-
<g/>
1850	[number]	k4	1850
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
železnice	železnice	k1gFnSc1	železnice
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
plynofikace	plynofikace	k1gFnSc1	plynofikace
osvětlení	osvětlení	k1gNnSc2	osvětlení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
<g/>
,	,	kIx,	,
co	co	k9	co
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
druhá	druhý	k4xOgFnSc1	druhý
plynárna	plynárna	k1gFnSc1	plynárna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
přibylo	přibýt	k5eAaPmAgNnS	přibýt
ve	v	k7c6	v
městě	město	k1gNnSc6	město
další	další	k2eAgFnSc4d1	další
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
železniční	železniční	k2eAgNnSc4d1	železniční
spojení	spojení	k1gNnSc4	spojení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
do	do	k7c2	do
Trnavy	Trnava	k1gFnSc2	Trnava
a	a	k8xC	a
zbytku	zbytek	k1gInSc2	zbytek
Slovenska	Slovensko	k1gNnSc2	Slovensko
-	-	kIx~	-
původní	původní	k2eAgFnSc1d1	původní
koňská	koňský	k2eAgFnSc1d1	koňská
železnice	železnice	k1gFnSc1	železnice
byla	být	k5eAaImAgFnS	být
přebudována	přebudovat	k5eAaPmNgFnS	přebudovat
na	na	k7c4	na
parní	parní	k2eAgNnSc4d1	parní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
pak	pak	k6eAd1	pak
přinesla	přinést	k5eAaPmAgFnS	přinést
elektrifikaci	elektrifikace	k1gFnSc4	elektrifikace
veřejného	veřejný	k2eAgNnSc2d1	veřejné
osvětlení	osvětlení	k1gNnSc2	osvětlení
<g/>
,	,	kIx,	,
zavedení	zavedení	k1gNnSc1	zavedení
vodovodu	vodovod	k1gInSc2	vodovod
a	a	k8xC	a
otevření	otevření	k1gNnSc2	otevření
Městského	městský	k2eAgNnSc2d1	Městské
divadla	divadlo	k1gNnSc2	divadlo
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
budov	budova	k1gFnPc2	budova
SND	SND	kA	SND
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
pak	pak	k6eAd1	pak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
první	první	k4xOgInSc1	první
most	most	k1gInSc1	most
přes	přes	k7c4	přes
Dunaj	Dunaj	k1gInSc4	Dunaj
(	(	kIx(	(
<g/>
Starý	starý	k2eAgInSc4d1	starý
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
most	most	k1gInSc1	most
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
také	také	k9	také
i	i	k9	i
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1	elektrifikovaná
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
síť	síť	k1gFnSc1	síť
o	o	k7c6	o
rozchodu	rozchod	k1gInSc6	rozchod
1000	[number]	k4	1000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
takových	takový	k3xDgInPc2	takový
provozů	provoz	k1gInPc2	provoz
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtrnáct	čtrnáct	k4xCc4	čtrnáct
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
následoval	následovat	k5eAaImAgInS	následovat
trolejbusový	trolejbusový	k2eAgInSc4d1	trolejbusový
provoz	provoz	k1gInSc4	provoz
<g/>
;	;	kIx,	;
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
měst	město	k1gNnPc2	město
v	v	k7c6	v
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc6	Rakousko-Uhersek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
tu	tu	k6eAd1	tu
žilo	žít	k5eAaImAgNnS	žít
65	[number]	k4	65
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1918	[number]	k4	1918
vzniká	vznikat	k5eAaImIp3nS	vznikat
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
právě	právě	k9	právě
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
a	a	k8xC	a
působností	působnost	k1gFnSc7	působnost
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
Martinskou	martinský	k2eAgFnSc7d1	Martinská
deklarací	deklarace	k1gFnSc7	deklarace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgFnP	připojit
Horní	horní	k2eAgFnPc1d1	horní
Uhry	Uhry	k1gFnPc1	Uhry
k	k	k7c3	k
Čechám	Čechy	k1gFnPc3	Čechy
<g/>
,	,	kIx,	,
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
Slezsku	Slezsko	k1gNnSc6	Slezsko
a	a	k8xC	a
zrodilo	zrodit	k5eAaPmAgNnS	zrodit
se	se	k3xPyFc4	se
Československo	Československo	k1gNnSc1	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc4d1	současný
název	název	k1gInSc4	název
Bratislava	Bratislava	k1gFnSc1	Bratislava
byl	být	k5eAaImAgInS	být
přijat	přijat	k2eAgInSc1d1	přijat
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
;	;	kIx,	;
existovalo	existovat	k5eAaImAgNnS	existovat
však	však	k9	však
také	také	k9	také
spoustu	spousta	k1gFnSc4	spousta
jiných	jiný	k2eAgInPc2d1	jiný
návrhů	návrh	k1gInPc2	návrh
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Wilsonovo	Wilsonův	k2eAgNnSc1d1	Wilsonovo
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
vznikalo	vznikat	k5eAaImAgNnS	vznikat
mnoho	mnoho	k4c1	mnoho
nových	nový	k2eAgFnPc2d1	nová
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
přestěhovaly	přestěhovat	k5eAaPmAgFnP	přestěhovat
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
některé	některý	k3yIgInPc1	některý
úřady	úřad	k1gInPc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
tu	tu	k6eAd1	tu
žilo	žít	k5eAaImAgNnS	žít
již	již	k6eAd1	již
128	[number]	k4	128
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
byla	být	k5eAaImAgFnS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
velkoměst	velkoměsto	k1gNnPc2	velkoměsto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
také	také	k9	také
i	i	k9	i
městem	město	k1gNnSc7	město
zemským	zemský	k2eAgNnSc7d1	zemské
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Krajiny	Krajina	k1gFnSc2	Krajina
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
do	do	k7c2	do
tváře	tvář	k1gFnSc2	tvář
města	město	k1gNnSc2	město
zapsali	zapsat	k5eAaPmAgMnP	zapsat
také	také	k9	také
čeští	český	k2eAgMnPc1d1	český
stavebníci	stavebník	k1gMnPc1	stavebník
<g/>
,	,	kIx,	,
stavitelé	stavitel	k1gMnPc1	stavitel
<g/>
,	,	kIx,	,
firmy	firma	k1gFnPc1	firma
ale	ale	k8xC	ale
i	i	k9	i
avantgardní	avantgardní	k2eAgMnPc1d1	avantgardní
architekti	architekt	k1gMnPc1	architekt
<g/>
:	:	kIx,	:
Arnošt	Arnošt	k1gMnSc1	Arnošt
Wiesner	Wiesner	k1gMnSc1	Wiesner
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Vaněk	Vaněk	k1gMnSc1	Vaněk
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Karfík	Karfík	k1gMnSc1	Karfík
ale	ale	k8xC	ale
především	především	k6eAd1	především
proslulý	proslulý	k2eAgMnSc1d1	proslulý
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
legendárním	legendární	k2eAgInSc7d1	legendární
obytným	obytný	k2eAgInSc7d1	obytný
domem	dům	k1gInSc7	dům
na	na	k7c6	na
Hviezdoslavově	Hviezdoslavův	k2eAgNnSc6d1	Hviezdoslavovo
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ale	ale	k9	ale
i	i	k9	i
dále	daleko	k6eAd2	daleko
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
pouhý	pouhý	k2eAgInSc4d1	pouhý
rok	rok	k1gInSc4	rok
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
rozšíření	rozšíření	k1gNnSc3	rozšíření
o	o	k7c6	o
tehdy	tehdy	k6eAd1	tehdy
samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
obce	obec	k1gFnSc2	obec
Devín	Devína	k1gFnPc2	Devína
<g/>
,	,	kIx,	,
Dúbravka	Dúbravka	k1gFnSc1	Dúbravka
<g/>
,	,	kIx,	,
Lamač	lamač	k1gInSc1	lamač
<g/>
,	,	kIx,	,
Petržalka	Petržalka	k1gFnSc1	Petržalka
<g/>
,	,	kIx,	,
Prievoz	Prievoz	k1gInSc1	Prievoz
<g/>
,	,	kIx,	,
Rača	Račum	k1gNnPc1	Račum
a	a	k8xC	a
Vajnory	Vajnora	k1gFnPc1	Vajnora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
byla	být	k5eAaImAgFnS	být
Bratislava	Bratislava	k1gFnSc1	Bratislava
ustanovena	ustanovit	k5eAaPmNgFnS	ustanovit
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Slovenska	Slovensko	k1gNnSc2	Slovensko
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
socialistické	socialistický	k2eAgFnSc2d1	socialistická
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
k	k	k7c3	k
rychlé	rychlý	k2eAgFnSc3d1	rychlá
výstavbě	výstavba	k1gFnSc3	výstavba
sídlišť	sídliště	k1gNnPc2	sídliště
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Petržalka	Petržalka	k1gFnSc1	Petržalka
<g/>
,	,	kIx,	,
Ružinov	Ružinov	k1gInSc1	Ružinov
<g/>
,	,	kIx,	,
Karlova	Karlův	k2eAgFnSc1d1	Karlova
Ves	ves	k1gFnSc1	ves
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
,	,	kIx,	,
postavily	postavit	k5eAaPmAgFnP	postavit
se	se	k3xPyFc4	se
moderní	moderní	k2eAgFnPc1d1	moderní
budovy	budova	k1gFnPc1	budova
(	(	kIx(	(
<g/>
Slovenský	slovenský	k2eAgInSc1d1	slovenský
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
se	se	k3xPyFc4	se
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
hranice	hranice	k1gFnSc2	hranice
města	město	k1gNnSc2	město
opět	opět	k6eAd1	opět
posunuly	posunout	k5eAaPmAgFnP	posunout
a	a	k8xC	a
připojily	připojit	k5eAaPmAgFnP	připojit
se	se	k3xPyFc4	se
obce	obec	k1gFnPc1	obec
Čunovo	Čunovo	k1gNnSc1	Čunovo
<g/>
,	,	kIx,	,
Devínska	Devínsko	k1gNnSc2	Devínsko
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
<g/>
,	,	kIx,	,
Jarovce	Jarovec	k1gInPc1	Jarovec
<g/>
,	,	kIx,	,
Podunajské	podunajský	k2eAgInPc1d1	podunajský
Biskupice	Biskupice	k1gInPc1	Biskupice
<g/>
,	,	kIx,	,
Rusovce	Rusovec	k1gInPc1	Rusovec
<g/>
,	,	kIx,	,
Vrakuňa	Vrakuňum	k1gNnPc1	Vrakuňum
a	a	k8xC	a
Záhorská	záhorský	k2eAgFnSc1d1	Záhorská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
městem	město	k1gNnSc7	město
ČSSR	ČSSR	kA	ČSSR
a	a	k8xC	a
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Československa	Československo	k1gNnSc2	Československo
pak	pak	k6eAd1	pak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
památek	památka	k1gFnPc2	památka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
je	být	k5eAaImIp3nS	být
komplex	komplex	k1gInSc4	komplex
budov	budova	k1gFnPc2	budova
vybudovaných	vybudovaný	k2eAgFnPc2d1	vybudovaná
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
městského	městský	k2eAgNnSc2d1	Městské
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Michalská	Michalský	k2eAgFnSc1d1	Michalská
brána	brána	k1gFnSc1	brána
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
brána	brána	k1gFnSc1	brána
z	z	k7c2	z
původních	původní	k2eAgNnPc2d1	původní
středověkých	středověký	k2eAgNnPc2d1	středověké
městských	městský	k2eAgNnPc2d1	Městské
opevnění	opevnění	k1gNnPc2	opevnění
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
budov	budova	k1gFnPc2	budova
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Univerzitní	univerzitní	k2eAgFnSc1d1	univerzitní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1756	[number]	k4	1756
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
používaná	používaný	k2eAgNnPc4d1	používané
uherským	uherský	k2eAgInSc7d1	uherský
sněmem	sněm	k1gInSc7	sněm
(	(	kIx(	(
<g/>
parlamentem	parlament	k1gInSc7	parlament
<g/>
)	)	kIx)	)
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1802	[number]	k4	1802
<g/>
-	-	kIx~	-
<g/>
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
centrum	centrum	k1gNnSc1	centrum
je	být	k5eAaImIp3nS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
mnohými	mnohý	k2eAgInPc7d1	mnohý
paláci	palác	k1gInPc7	palác
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Grasalkovičův	Grasalkovičův	k2eAgInSc1d1	Grasalkovičův
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1760	[number]	k4	1760
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
sídlem	sídlo	k1gNnSc7	sídlo
slovenského	slovenský	k2eAgMnSc2d1	slovenský
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
slovenská	slovenský	k2eAgFnSc1d1	slovenská
vláda	vláda	k1gFnSc1	vláda
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
arcibiskupském	arcibiskupský	k2eAgInSc6d1	arcibiskupský
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Primaciálném	Primaciálný	k2eAgInSc6d1	Primaciálný
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
dnešním	dnešní	k2eAgNnSc7d1	dnešní
sídlem	sídlo	k1gNnSc7	sídlo
primátora	primátor	k1gMnSc2	primátor
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1805	[number]	k4	1805
podepsán	podepsán	k2eAgInSc4d1	podepsán
prešpurský	prešpurský	k2eAgInSc4d1	prešpurský
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
známým	známý	k2eAgFnPc3d1	známá
sakrálním	sakrální	k2eAgFnPc3d1	sakrální
stavbám	stavba	k1gFnPc3	stavba
patří	patřit	k5eAaImIp3nS	patřit
Konkatedrála	Konkatedrála	k1gFnSc1	Konkatedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Martina	Martin	k1gMnSc2	Martin
<g/>
,	,	kIx,	,
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1563	[number]	k4	1563
<g/>
-	-	kIx~	-
<g/>
1830	[number]	k4	1830
korunovačním	korunovační	k2eAgInSc7d1	korunovační
kostelem	kostel	k1gInSc7	kostel
uherských	uherský	k2eAgMnPc2d1	uherský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Františkánský	františkánský	k2eAgInSc1d1	františkánský
kostel	kostel	k1gInSc1	kostel
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
zachovalý	zachovalý	k2eAgInSc1d1	zachovalý
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
místem	místo	k1gNnSc7	místo
rytířských	rytířský	k2eAgFnPc2d1	rytířská
ceremonií	ceremonie	k1gFnPc2	ceremonie
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
Modrý	modrý	k2eAgInSc1d1	modrý
kostelík	kostelík	k1gInSc1	kostelík
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
v	v	k7c6	v
secesním	secesní	k2eAgInSc6d1	secesní
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Kuriozitou	kuriozita	k1gFnSc7	kuriozita
je	být	k5eAaImIp3nS	být
podzemní	podzemní	k2eAgNnSc1d1	podzemní
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
nadzemní	nadzemní	k2eAgFnPc1d1	nadzemní
<g/>
)	)	kIx)	)
židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
při	při	k7c6	při
vjezdu	vjezd	k1gInSc6	vjezd
do	do	k7c2	do
tramvajového	tramvajový	k2eAgInSc2d1	tramvajový
tunelu	tunel	k1gInSc2	tunel
na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
i	i	k9	i
pochován	pochován	k2eAgMnSc1d1	pochován
rabín	rabín	k1gMnSc1	rabín
Chatam	Chatam	k1gInSc4	Chatam
Sofer	Sofra	k1gFnPc2	Sofra
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
vojenských	vojenský	k2eAgInPc2d1	vojenský
hřbitovů	hřbitov	k1gInPc2	hřbitov
je	být	k5eAaImIp3nS	být
Slavín	Slavín	k1gInSc1	Slavín
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
vojáků	voják	k1gMnPc2	voják
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
padli	padnout	k5eAaImAgMnP	padnout
při	při	k7c6	při
osvobozování	osvobozování	k1gNnSc6	osvobozování
Bratislavy	Bratislava	k1gFnSc2	Bratislava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bratislavský	bratislavský	k2eAgInSc4d1	bratislavský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Bratislavský	bratislavský	k2eAgInSc4d1	bratislavský
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
tyčící	tyčící	k2eAgInSc4d1	tyčící
se	se	k3xPyFc4	se
na	na	k7c6	na
plošině	plošina	k1gFnSc6	plošina
85	[number]	k4	85
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
Dunajem	Dunaj	k1gInSc7	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Hradní	hradní	k2eAgInSc1d1	hradní
vrch	vrch	k1gInSc1	vrch
byl	být	k5eAaImAgInS	být
obýván	obývat	k5eAaImNgInS	obývat
od	od	k7c2	od
eneolitu	eneolit	k1gInSc2	eneolit
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
keltské	keltský	k2eAgNnSc4d1	keltské
oppidum	oppidum	k1gNnSc4	oppidum
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
starověké	starověký	k2eAgFnSc2d1	starověká
římské	římský	k2eAgFnSc2d1	římská
hranice	hranice	k1gFnSc2	hranice
<g/>
,	,	kIx,	,
zpevněné	zpevněný	k2eAgNnSc1d1	zpevněné
sídlo	sídlo	k1gNnSc1	sídlo
Slovanů	Slovan	k1gInPc2	Slovan
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnPc4d1	politická
a	a	k8xC	a
vojenské	vojenský	k2eAgNnSc1d1	vojenské
středisko	středisko	k1gNnSc1	středisko
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c4	na
gotický	gotický	k2eAgInSc4d1	gotický
<g/>
,	,	kIx,	,
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c4	na
renesanční	renesanční	k2eAgInSc4d1	renesanční
a	a	k8xC	a
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c4	na
barokní	barokní	k2eAgInSc4d1	barokní
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
vládu	vláda	k1gFnSc4	vláda
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc4	hrad
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
uherské	uherský	k2eAgFnSc6d1	uherská
části	část	k1gFnSc6	část
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1811	[number]	k4	1811
hrad	hrad	k1gInSc1	hrad
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
do	do	k7c2	do
základů	základ	k1gInPc2	základ
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
ruinách	ruina	k1gFnPc6	ruina
až	až	k6eAd1	až
do	do	k7c2	do
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
rekonstruován	rekonstruovat	k5eAaBmNgMnS	rekonstruovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953	[number]	k4	1953
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
stylu	styl	k1gInSc6	styl
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
D.	D.	kA	D.
Martinčeka	Martinčeka	k1gFnSc1	Martinčeka
a	a	k8xC	a
A.	A.	kA	A.
Piffla	Piffla	k1gFnSc1	Piffla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
stavba	stavba	k1gFnSc1	stavba
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Československou	československý	k2eAgFnSc7d1	Československá
národní	národní	k2eAgFnSc7d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
památkám	památka	k1gFnPc3	památka
pouze	pouze	k6eAd1	pouze
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ruiny	ruina	k1gFnPc4	ruina
hradu	hrad	k1gInSc2	hrad
Devín	Devína	k1gFnPc2	Devína
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Devín	Devín	k1gInSc4	Devín
<g/>
,	,	kIx,	,
na	na	k7c6	na
skále	skála	k1gFnSc6	skála
nad	nad	k7c7	nad
soutokem	soutok	k1gInSc7	soutok
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
s	s	k7c7	s
Dunajem	Dunaj	k1gInSc7	Dunaj
<g/>
,	,	kIx,	,
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Devínu	Devín	k1gInSc6	Devín
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
864	[number]	k4	864
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
hradiště	hradiště	k1gNnSc1	hradiště
poprvé	poprvé	k6eAd1	poprvé
připomínáno	připomínat	k5eAaImNgNnS	připomínat
jako	jako	k8xS	jako
Dowina	Dowina	k1gFnSc1	Dowina
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
především	především	k9	především
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
knížete	kníže	k1gMnSc2	kníže
Rastislava	Rastislav	k1gMnSc2	Rastislav
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tu	ten	k3xDgFnSc4	ten
dal	dát	k5eAaPmAgMnS	dát
vybudovat	vybudovat	k5eAaPmF	vybudovat
mohutnou	mohutný	k2eAgFnSc4d1	mohutná
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
historii	historie	k1gFnSc6	historie
Devína	Devín	k1gInSc2	Devín
se	se	k3xPyFc4	se
podepsali	podepsat	k5eAaPmAgMnP	podepsat
též	též	k9	též
knížata	kníže	k1gMnPc1wR	kníže
Mojmír	Mojmír	k1gMnSc1	Mojmír
a	a	k8xC	a
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
strategické	strategický	k2eAgFnSc3d1	strategická
poloze	poloha	k1gFnSc3	poloha
byl	být	k5eAaImAgInS	být
klíčovým	klíčový	k2eAgInSc7d1	klíčový
hradem	hrad	k1gInSc7	hrad
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
raného	raný	k2eAgInSc2d1	raný
uherského	uherský	k2eAgInSc2d1	uherský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
zničen	zničit	k5eAaPmNgInS	zničit
Napoleonovou	Napoleonův	k2eAgFnSc7d1	Napoleonova
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
slovenské	slovenský	k2eAgFnSc2d1	slovenská
a	a	k8xC	a
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
5	[number]	k4	5
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
okresy	okres	k1gInPc4	okres
řazené	řazený	k2eAgInPc4d1	řazený
mezi	mezi	k7c4	mezi
celostátní	celostátní	k2eAgNnSc4d1	celostátní
<g/>
)	)	kIx)	)
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
17	[number]	k4	17
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Bratislavským	bratislavský	k2eAgInSc7d1	bratislavský
krajem	kraj	k1gInSc7	kraj
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nejbohatší	bohatý	k2eAgFnPc4d3	nejbohatší
oblasti	oblast	k1gFnPc4	oblast
celého	celý	k2eAgNnSc2d1	celé
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
;	;	kIx,	;
průměrný	průměrný	k2eAgInSc1d1	průměrný
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
zde	zde	k6eAd1	zde
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
176	[number]	k4	176
%	%	kIx~	%
průměru	průměr	k1gInSc6	průměr
EU	EU	kA	EU
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejbohatší	bohatý	k2eAgInSc1d3	nejbohatší
region	region	k1gInSc1	region
zemí	zem	k1gFnPc2	zem
EU	EU	kA	EU
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
přijaty	přijmout	k5eAaPmNgInP	přijmout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
je	být	k5eAaImIp3nS	být
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
ze	z	k7c2	z
zbytku	zbytek	k1gInSc2	zbytek
země	zem	k1gFnSc2	zem
sem	sem	k6eAd1	sem
cestuje	cestovat	k5eAaImIp3nS	cestovat
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
tu	tu	k6eAd1	tu
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
významných	významný	k2eAgFnPc2d1	významná
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
od	od	k7c2	od
telekomunikačních	telekomunikační	k2eAgInPc2d1	telekomunikační
operátorů	operátor	k1gInPc2	operátor
až	až	k9	až
po	po	k7c4	po
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
podniky	podnik	k1gInPc4	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rafinerie	rafinerie	k1gFnSc1	rafinerie
Slovnaftu	Slovnaft	k1gInSc2	Slovnaft
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
Slovensko	Slovensko	k1gNnSc4	Slovensko
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
se	se	k3xPyFc4	se
zase	zase	k9	zase
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
závody	závod	k1gInPc1	závod
Volkswagen	volkswagen	k1gInSc1	volkswagen
Slovakia	Slovakia	k1gFnSc1	Slovakia
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
metropole	metropol	k1gFnSc2	metropol
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tzv.	tzv.	kA	tzv.
business	business	k1gInSc4	business
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
koncentrují	koncentrovat	k5eAaBmIp3nP	koncentrovat
například	například	k6eAd1	například
softwarové	softwarový	k2eAgFnPc4d1	softwarová
společnosti	společnost	k1gFnPc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
sídli	sídlet	k5eAaImRp2nS	sídlet
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
visegrádský	visegrádský	k2eAgInSc1d1	visegrádský
fond	fond	k1gInSc1	fond
(	(	kIx(	(
<g/>
International	International	k1gFnSc1	International
visegrad	visegrad	k1gInSc1	visegrad
fund	fund	k1gInSc1	fund
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
provozovny	provozovna	k1gFnPc1	provozovna
firem	firma	k1gFnPc2	firma
koncentrují	koncentrovat	k5eAaBmIp3nP	koncentrovat
v	v	k7c6	v
nových	nový	k2eAgInPc6d1	nový
administrativních	administrativní	k2eAgInPc6d1	administrativní
a	a	k8xC	a
obchodních	obchodní	k2eAgInPc6d1	obchodní
komplexech	komplex	k1gInPc6	komplex
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Aupark	Aupark	k1gInSc1	Aupark
či	či	k8xC	či
Polus	Polus	k1gMnSc1	Polus
City	City	k1gFnSc2	City
Center	centrum	k1gNnPc2	centrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přibývají	přibývat	k5eAaImIp3nP	přibývat
zde	zde	k6eAd1	zde
rovněž	rovněž	k9	rovněž
i	i	k9	i
kancelářské	kancelářský	k2eAgFnPc4d1	kancelářská
plochy	plocha	k1gFnPc4	plocha
a	a	k8xC	a
staví	stavit	k5eAaPmIp3nP	stavit
se	se	k3xPyFc4	se
výškové	výškový	k2eAgFnSc2d1	výšková
budovy	budova	k1gFnSc2	budova
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Tower	Tower	k1gInSc1	Tower
115	[number]	k4	115
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
budova	budova	k1gFnSc1	budova
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
plánují	plánovat	k5eAaImIp3nP	plánovat
se	se	k3xPyFc4	se
ale	ale	k9	ale
i	i	k9	i
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soustředěn	soustředěn	k2eAgMnSc1d1	soustředěn
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
i	i	k9	i
bankovní	bankovní	k2eAgInSc1d1	bankovní
sektor	sektor	k1gInSc1	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
právě	právě	k6eAd1	právě
vznikají	vznikat	k5eAaImIp3nP	vznikat
kancelářské	kancelářský	k2eAgFnPc4d1	kancelářská
prostory	prostora	k1gFnPc4	prostora
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nábřeží	nábřeží	k1gNnSc4	nábřeží
Dunaje	Dunaj	k1gInSc2	Dunaj
nedaleko	nedaleko	k7c2	nedaleko
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
projekt	projekt	k1gInSc1	projekt
Eurovea	Eurove	k1gInSc2	Eurove
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
trochu	trochu	k6eAd1	trochu
vzdálenější	vzdálený	k2eAgFnPc4d2	vzdálenější
Mlynské	Mlynský	k2eAgFnPc4d1	Mlynská
nivy	niva	k1gFnPc4	niva
poblíž	poblíž	k6eAd1	poblíž
Ružinova	Ružinův	k2eAgFnSc1d1	Ružinův
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
nových	nový	k2eAgFnPc2d1	nová
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
budov	budova	k1gFnPc2	budova
koresponduje	korespondovat	k5eAaImIp3nS	korespondovat
s	s	k7c7	s
nárůstem	nárůst	k1gInSc7	nárůst
poptávky	poptávka	k1gFnSc2	poptávka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
s	s	k7c7	s
hospodářským	hospodářský	k2eAgInSc7d1	hospodářský
růstem	růst	k1gInSc7	růst
Slovenska	Slovensko	k1gNnSc2	Slovensko
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
desetiletí	desetiletí	k1gNnSc6	desetiletí
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
dni	den	k1gInSc3	den
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
výškových	výškový	k2eAgFnPc2d1	výšková
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
jsou	být	k5eAaImIp3nP	být
buďto	buďto	k8xC	buďto
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
plánují	plánovat	k5eAaImIp3nP	plánovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalých	bývalý	k2eAgFnPc2d1	bývalá
Vodních	vodní	k2eAgFnPc2d1	vodní
kasáren	kasárny	k1gFnPc2	kasárny
sídlí	sídlet	k5eAaImIp3nS	sídlet
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
vrcholná	vrcholný	k2eAgFnSc1d1	vrcholná
slovenská	slovenský	k2eAgFnSc1d1	slovenská
instituce	instituce	k1gFnSc1	instituce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
,	,	kIx,	,
existující	existující	k2eAgFnPc1d1	existující
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Kontroverzní	kontroverzní	k2eAgFnSc1d1	kontroverzní
dostavba	dostavba	k1gFnSc1	dostavba
budovy	budova	k1gFnSc2	budova
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
V.	V.	kA	V.
Dědečka	dědeček	k1gMnSc2	dědeček
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
plánována	plánovat	k5eAaImNgFnS	plánovat
celková	celkový	k2eAgFnSc1d1	celková
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
Významné	významný	k2eAgFnSc2d1	významná
sbírky	sbírka	k1gFnSc2	sbírka
vlastní	vlastní	k2eAgFnSc2d1	vlastní
Galérie	galérie	k1gFnSc2	galérie
mesta	mesto	k1gNnSc2	mesto
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Mirbachově	Mirbachův	k2eAgInSc6d1	Mirbachův
paláci	palác	k1gInSc6	palác
(	(	kIx(	(
<g/>
středoevropské	středoevropský	k2eAgNnSc1d1	středoevropské
umění	umění	k1gNnSc1	umění
baroka	baroko	k1gNnSc2	baroko
a	a	k8xC	a
grafický	grafický	k2eAgInSc1d1	grafický
kabinet	kabinet	k1gInSc1	kabinet
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Pálffyho	Pálffy	k1gMnSc2	Pálffy
paláci	palác	k1gInSc6	palác
(	(	kIx(	(
<g/>
gotické	gotický	k2eAgNnSc1d1	gotické
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
středoevropské	středoevropský	k2eAgNnSc1d1	středoevropské
umění	umění	k1gNnSc1	umění
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgNnSc1d1	moderní
slovenské	slovenský	k2eAgNnSc1d1	slovenské
umění	umění	k1gNnSc1	umění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgFnPc7d3	nejvýznamnější
bratislavskými	bratislavský	k2eAgFnPc7d1	Bratislavská
galeriemi	galerie	k1gFnPc7	galerie
moderního	moderní	k2eAgInSc2d1	moderní
umění	umění	k1gNnSc4	umění
jsou	být	k5eAaImIp3nP	být
Galerie	galerie	k1gFnSc1	galerie
Nedbalka	Nedbalka	k1gFnSc1	Nedbalka
<g/>
,	,	kIx,	,
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
slovenské	slovenský	k2eAgNnSc4d1	slovenské
umění	umění	k1gNnSc4	umění
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
a	a	k8xC	a
připomínající	připomínající	k2eAgMnSc1d1	připomínající
svou	svůj	k3xOyFgFnSc7	svůj
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
dispozicí	dispozice	k1gFnSc7	dispozice
Guggenheimovo	Guggenheimův	k2eAgNnSc1d1	Guggenheimovo
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
a	a	k8xC	a
největší	veliký	k2eAgFnSc1d3	veliký
slovenská	slovenský	k2eAgFnSc1d1	slovenská
galerie	galerie	k1gFnSc1	galerie
světového	světový	k2eAgNnSc2d1	světové
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
Danubiana	Danubiana	k1gFnSc1	Danubiana
Meulensteen	Meulensteno	k1gNnPc2	Meulensteno
Art	Art	k1gFnSc2	Art
Museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
na	na	k7c6	na
dunajském	dunajský	k2eAgInSc6d1	dunajský
poloostrově	poloostrov	k1gInSc6	poloostrov
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vodního	vodní	k2eAgNnSc2d1	vodní
díla	dílo	k1gNnSc2	dílo
Čunovo	Čunovo	k1gNnSc1	Čunovo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Bratislava	Bratislava	k1gFnSc1	Bratislava
domovem	domov	k1gInSc7	domov
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc2d1	hlavní
scény	scéna	k1gFnSc2	scéna
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
budovách	budova	k1gFnPc6	budova
<g/>
;	;	kIx,	;
hlavní	hlavní	k2eAgFnSc1d1	hlavní
historická	historický	k2eAgFnSc1d1	historická
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Hviezdoslavově	Hviezdoslavův	k2eAgNnSc6d1	Hviezdoslavovo
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
opera	opera	k1gFnSc1	opera
a	a	k8xC	a
balet	balet	k1gInSc1	balet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
se	se	k3xPyFc4	se
však	však	k9	však
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
objektu	objekt	k1gInSc2	objekt
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
národního	národní	k2eAgInSc2d1	národní
však	však	k9	však
hlavní	hlavní	k2eAgFnSc1d1	hlavní
slovenská	slovenský	k2eAgFnSc1d1	slovenská
metropole	metropole	k1gFnSc1	metropole
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
jiné	jiný	k2eAgFnSc2d1	jiná
divadelní	divadelní	k2eAgFnSc2d1	divadelní
scény	scéna	k1gFnSc2	scéna
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
divadla	divadlo	k1gNnPc4	divadlo
Astorka	Astorka	k1gFnSc1	Astorka
Korzo	korzo	k1gNnSc4	korzo
'	'	kIx"	'
<g/>
90	[number]	k4	90
či	či	k8xC	či
divadlo	divadlo	k1gNnSc4	divadlo
Aréna	aréna	k1gFnSc1	aréna
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
Nového	Nového	k2eAgInSc2d1	Nového
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
na	na	k7c6	na
Vajanského	Vajanský	k2eAgNnSc2d1	Vajanský
nábřeží	nábřeží	k1gNnSc2	nábřeží
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
dějinami	dějiny	k1gFnPc7	dějiny
celého	celý	k2eAgInSc2d1	celý
národa	národ	k1gInSc2	národ
<g/>
;	;	kIx,	;
na	na	k7c6	na
historii	historie	k1gFnSc6	historie
Bratislavy	Bratislava	k1gFnSc2	Bratislava
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
Mestské	Mestský	k2eAgNnSc1d1	Mestské
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
(	(	kIx(	(
<g/>
založené	založený	k2eAgInPc1d1	založený
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgNnPc1d1	další
muzea	muzeum	k1gNnPc1	muzeum
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zasvěcena	zasvěcen	k2eAgNnPc1d1	zasvěceno
různým	různý	k2eAgInPc3d1	různý
dalším	další	k2eAgInPc3d1	další
oborům	obor	k1gInPc3	obor
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
u	u	k7c2	u
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
muzeum	muzeum	k1gNnSc1	muzeum
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
metropole	metropol	k1gFnSc2	metropol
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
ale	ale	k8xC	ale
i	i	k9	i
muzeum	muzeum	k1gNnSc1	muzeum
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
zbraní	zbraň	k1gFnPc2	zbraň
či	či	k8xC	či
farmaceutické	farmaceutický	k2eAgNnSc4d1	farmaceutické
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
muzea	muzeum	k1gNnPc1	muzeum
mají	mít	k5eAaImIp3nP	mít
expozice	expozice	k1gFnPc1	expozice
o	o	k7c6	o
osobnostech	osobnost	k1gFnPc6	osobnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Ján	Ján	k1gMnSc1	Ján
Jesenský	Jesenský	k1gMnSc1	Jesenský
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
Fleischmann	Fleischmann	k1gMnSc1	Fleischmann
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
Hummel	Hummel	k1gMnSc1	Hummel
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
Dobeš	Dobeš	k1gMnSc1	Dobeš
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zdejší	zdejší	k2eAgFnSc4d1	zdejší
židovskou	židovský	k2eAgFnSc4d1	židovská
komunitu	komunita	k1gFnSc4	komunita
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc1	význam
Muzeum	muzeum	k1gNnSc1	muzeum
židovské	židovská	k1gFnSc2	židovská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
umístěné	umístěný	k2eAgInPc1d1	umístěný
v	v	k7c6	v
Židovské	židovský	k2eAgFnSc6d1	židovská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
událostmi	událost	k1gFnPc7	událost
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
městech	město	k1gNnPc6	město
festivaly	festival	k1gInPc1	festival
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c6	o
Bratislavské	bratislavský	k2eAgFnSc6d1	Bratislavská
hudobné	hudobný	k2eAgFnSc6d1	hudobná
slávnosti	slávnost	k1gFnSc6	slávnost
<g/>
,	,	kIx,	,
festival	festival	k1gInSc1	festival
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
či	či	k8xC	či
Bratislavské	bratislavský	k2eAgInPc4d1	bratislavský
jazzové	jazzový	k2eAgInPc4d1	jazzový
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
jazzový	jazzový	k2eAgInSc1d1	jazzový
festival	festival	k1gInSc1	festival
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Reduty	reduta	k1gFnSc2	reduta
sídlí	sídlet	k5eAaImIp3nS	sídlet
také	také	k9	také
i	i	k9	i
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
filharmonie	filharmonie	k1gFnSc1	filharmonie
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
Bratislavy	Bratislava	k1gFnSc2	Bratislava
též	též	k9	též
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
kina	kino	k1gNnSc2	kino
<g/>
;	;	kIx,	;
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
tzv.	tzv.	kA	tzv.
multiplexy	multiplex	k1gInPc1	multiplex
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
velkokina	velkokina	k1gFnSc1	velkokina
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
sály	sál	k1gInPc7	sál
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
součástí	součást	k1gFnPc2	součást
obchodních	obchodní	k2eAgNnPc2d1	obchodní
a	a	k8xC	a
kulturních	kulturní	k2eAgNnPc2d1	kulturní
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
takové	takový	k3xDgInPc4	takový
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
například	například	k6eAd1	například
multikino	multikino	k1gNnSc4	multikino
v	v	k7c4	v
Polus	Polus	k1gInSc4	Polus
City	city	k1gNnSc1	city
Centru	centr	k1gInSc2	centr
ve	v	k7c6	v
Vajnorské	Vajnorský	k2eAgFnSc6d1	Vajnorská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
například	například	k6eAd1	například
Palace	Palace	k1gFnSc1	Palace
Cinemas	Cinemasa	k1gFnPc2	Cinemasa
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Aupark	Aupark	k1gInSc4	Aupark
a	a	k8xC	a
též	též	k9	též
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Eurovea	Eurove	k1gInSc2	Eurove
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
starším	starý	k2eAgNnPc3d2	starší
kinům	kino	k1gNnPc3	kino
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Hviezda	Hviezda	k1gFnSc1	Hviezda
<g/>
,	,	kIx,	,
Tatra	Tatra	k1gFnSc1	Tatra
<g/>
,	,	kIx,	,
FK	FK	kA	FK
Mladosť	Mladosť	k1gFnPc1	Mladosť
<g/>
,	,	kIx,	,
Kino	kino	k1gNnSc1	kino
Café	café	k1gNnSc1	café
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
pak	pak	k6eAd1	pak
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
Slovenska	Slovensko	k1gNnSc2	Slovensko
do	do	k7c2	do
EU	EU	kA	EU
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
Bratislava	Bratislava	k1gFnSc1	Bratislava
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
turisty	turist	k1gMnPc7	turist
navštěvovaných	navštěvovaný	k2eAgNnPc2d1	navštěvované
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nemůže	moct	k5eNaImIp3nS	moct
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
velikostí	velikost	k1gFnSc7	velikost
i	i	k8xC	i
významem	význam	k1gInSc7	význam
měřit	měřit	k5eAaImF	měřit
sice	sice	k8xC	sice
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Vídeň	Vídeň	k1gFnSc1	Vídeň
či	či	k8xC	či
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnPc4	svůj
jedinečné	jedinečný	k2eAgFnPc4d1	jedinečná
pamětihodnosti	pamětihodnost	k1gFnPc4	pamětihodnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
náležitě	náležitě	k6eAd1	náležitě
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
svou	svůj	k3xOyFgFnSc7	svůj
prezentací	prezentace	k1gFnSc7	prezentace
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
Bratislavu	Bratislava	k1gFnSc4	Bratislava
686	[number]	k4	686
000	[number]	k4	000
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
tak	tak	k6eAd1	tak
metropoli	metropole	k1gFnSc4	metropole
činí	činit	k5eAaImIp3nS	činit
nejnavštěvovanějším	navštěvovaný	k2eAgNnSc7d3	nejnavštěvovanější
místem	místo	k1gNnSc7	místo
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
Bratislavy	Bratislava	k1gFnSc2	Bratislava
jsou	být	k5eAaImIp3nP	být
kromě	kromě	k7c2	kromě
domácích	domácí	k2eAgMnPc2d1	domácí
Slováků	Slovák	k1gMnPc2	Slovák
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
třetina	třetina	k1gFnSc1	třetina
všech	všecek	k3xTgMnPc2	všecek
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
232	[number]	k4	232
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
též	též	k9	též
i	i	k9	i
Britové	Brit	k1gMnPc1	Brit
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
,	,	kIx,	,
Američani	Američan	k1gMnPc1	Američan
<g/>
,	,	kIx,	,
Rakušané	Rakušan	k1gMnPc1	Rakušan
<g/>
,	,	kIx,	,
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
Maďaři	Maďar	k1gMnPc1	Maďar
<g/>
,	,	kIx,	,
Izraelci	Izraelec	k1gMnPc1	Izraelec
a	a	k8xC	a
Češi	Čech	k1gMnPc1	Čech
(	(	kIx(	(
<g/>
z	z	k7c2	z
kterých	který	k3yIgFnPc2	který
právě	právě	k6eAd1	právě
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Britové	Brit	k1gMnPc1	Brit
tvoří	tvořit	k5eAaImIp3nP	tvořit
největší	veliký	k2eAgInSc4d3	veliký
počet	počet	k1gInSc4	počet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
z	z	k7c2	z
Bratislavy	Bratislava	k1gFnSc2	Bratislava
ještě	ještě	k9	ještě
do	do	k7c2	do
dalších	další	k2eAgNnPc2d1	další
měst	město	k1gNnPc2	město
a	a	k8xC	a
turistických	turistický	k2eAgFnPc2d1	turistická
destinací	destinace	k1gFnPc2	destinace
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
také	také	k9	také
na	na	k7c4	na
silvestrovské	silvestrovský	k2eAgFnPc4d1	silvestrovská
oslavy	oslava	k1gFnPc4	oslava
na	na	k7c6	na
konci	konec	k1gInSc6	konec
každého	každý	k3xTgInSc2	každý
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
cestovní	cestovní	k2eAgFnSc2d1	cestovní
kanceláře	kancelář	k1gFnSc2	kancelář
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
pořádají	pořádat	k5eAaImIp3nP	pořádat
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
zájezdy	zájezd	k1gInPc4	zájezd
právě	právě	k9	právě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
přelomu	přelom	k1gInSc2	přelom
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
turistice	turistika	k1gFnSc3	turistika
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
nejen	nejen	k6eAd1	nejen
centru	centrum	k1gNnSc3	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
míry	míra	k1gFnSc2	míra
závislé	závislý	k2eAgFnSc2d1	závislá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pohostinství	pohostinství	k1gNnSc2	pohostinství
a	a	k8xC	a
bratislavským	bratislavský	k2eAgInPc3d1	bratislavský
hotelům	hotel	k1gInPc3	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
však	však	k9	však
ale	ale	k9	ale
na	na	k7c4	na
stále	stále	k6eAd1	stále
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
nápor	nápor	k1gInSc4	nápor
nejsou	být	k5eNaImIp3nP	být
připraveny	připravit	k5eAaPmNgInP	připravit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
jejich	jejich	k3xOp3gFnPc1	jejich
kapacity	kapacita	k1gFnPc1	kapacita
nepostačují	postačovat	k5eNaImIp3nP	postačovat
<g/>
.	.	kIx.	.
</s>
<s>
Budují	budovat	k5eAaImIp3nP	budovat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
hotely	hotel	k1gInPc1	hotel
nové	nový	k2eAgInPc1d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
turismu	turismus	k1gInSc2	turismus
používá	používat	k5eAaImIp3nS	používat
speciální	speciální	k2eAgNnSc4d1	speciální
turistické	turistický	k2eAgNnSc4d1	turistické
logo	logo	k1gNnSc4	logo
se	s	k7c7	s
sloganem	slogan	k1gInSc7	slogan
"	"	kIx"	"
<g/>
Little	Little	k1gFnSc1	Little
Big	Big	k1gFnSc2	Big
City	city	k1gNnSc1	city
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Malé	Malé	k2eAgNnSc4d1	Malé
velké	velký	k2eAgNnSc4d1	velké
město	město	k1gNnSc4	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
využíváno	využívat	k5eAaImNgNnS	využívat
na	na	k7c6	na
propagačních	propagační	k2eAgInPc6d1	propagační
materiálech	materiál	k1gInPc6	materiál
<g/>
,	,	kIx,	,
či	či	k8xC	či
na	na	k7c6	na
nátěru	nátěr	k1gInSc6	nátěr
některých	některý	k3yIgFnPc2	některý
vozidel	vozidlo	k1gNnPc2	vozidlo
MHD	MHD	kA	MHD
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
Bratislavy	Bratislava	k1gFnSc2	Bratislava
jsou	být	k5eAaImIp3nP	být
římskokatoličtí	římskokatolický	k2eAgMnPc1d1	římskokatolický
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
také	také	k9	také
byly	být	k5eAaImAgInP	být
budovány	budován	k2eAgInPc1d1	budován
ve	v	k7c6	v
městě	město	k1gNnSc6	město
mnohé	mnohý	k2eAgInPc1d1	mnohý
kostely	kostel	k1gInPc1	kostel
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
pak	pak	k6eAd1	pak
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
samozřejmě	samozřejmě	k6eAd1	samozřejmě
ostře	ostro	k6eAd1	ostro
kontrastuje	kontrastovat	k5eAaImIp3nS	kontrastovat
se	se	k3xPyFc4	se
století	století	k1gNnSc2	století
dvacátým	dvacátý	k4xOgMnPc3	dvacátý
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
-	-	kIx~	-
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
-	-	kIx~	-
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
potlačování	potlačování	k1gNnSc2	potlačování
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
náboženské	náboženský	k2eAgFnPc1d1	náboženská
stavby	stavba	k1gFnPc1	stavba
nové	nový	k2eAgFnPc1d1	nová
<g/>
,	,	kIx,	,
paradoxně	paradoxně	k6eAd1	paradoxně
v	v	k7c6	v
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
na	na	k7c6	na
sídlištích	sídliště	k1gNnPc6	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Bratislavu	Bratislava	k1gFnSc4	Bratislava
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
přišlo	přijít	k5eAaPmAgNnS	přijít
až	až	k6eAd1	až
200	[number]	k4	200
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgInPc4d3	nejstarší
z	z	k7c2	z
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
též	též	k9	též
i	i	k9	i
bratislavských	bratislavský	k2eAgFnPc2d1	Bratislavská
památek	památka	k1gFnPc2	památka
patří	patřit	k5eAaImIp3nS	patřit
Františkánský	františkánský	k2eAgInSc1d1	františkánský
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
gotická	gotický	k2eAgFnSc1d1	gotická
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
mnohem	mnohem	k6eAd1	mnohem
významnější	významný	k2eAgNnSc1d2	významnější
je	být	k5eAaImIp3nS	být
již	již	k9	již
Katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Martina	Martin	k1gMnSc2	Martin
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bývali	bývat	k5eAaImAgMnP	bývat
korunováni	korunován	k2eAgMnPc1d1	korunován
uherští	uherský	k2eAgMnPc1d1	uherský
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
i	i	k9	i
menšina	menšina	k1gFnSc1	menšina
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
stavbu	stavba	k1gFnSc4	stavba
s	s	k7c7	s
orientálními	orientální	k2eAgInPc7d1	orientální
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
synagog	synagoga	k1gFnPc2	synagoga
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
typické	typický	k2eAgNnSc1d1	typické
<g/>
.	.	kIx.	.
</s>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
byla	být	k5eAaImAgFnS	být
zdemolována	zdemolovat	k5eAaPmNgFnS	zdemolovat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
Nového	Nového	k2eAgInSc2d1	Nového
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
veřejných	veřejný	k2eAgFnPc2d1	veřejná
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
Univerzita	univerzita	k1gFnSc1	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
STU	sto	k4xCgNnSc3	sto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
zázemí	zázemí	k1gNnSc1	zázemí
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
především	především	k9	především
tamní	tamní	k2eAgFnPc1d1	tamní
univerzity	univerzita	k1gFnPc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
některé	některý	k3yIgInPc1	některý
bratislavské	bratislavský	k2eAgInPc1d1	bratislavský
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
podniky	podnik	k1gInPc1	podnik
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgNnPc4d1	vlastní
výzkumná	výzkumný	k2eAgNnPc4d1	výzkumné
střediska	středisko	k1gNnPc4	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
evropských	evropský	k2eAgFnPc2d1	Evropská
metropolí	metropol	k1gFnPc2	metropol
nemá	mít	k5eNaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc1d1	vlastní
observatoř	observatoř	k1gFnSc1	observatoř
či	či	k8xC	či
planetárium	planetárium	k1gNnSc1	planetárium
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
severu	sever	k1gInSc6	sever
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
dobré	dobrý	k2eAgFnPc1d1	dobrá
podmínky	podmínka	k1gFnPc1	podmínka
na	na	k7c4	na
pozorování	pozorování	k1gNnSc4	pozorování
noční	noční	k2eAgFnSc2d1	noční
oblohy	obloha	k1gFnSc2	obloha
<g/>
;	;	kIx,	;
nejbližší	blízký	k2eAgFnPc1d3	nejbližší
stavby	stavba	k1gFnPc1	stavba
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
desítky	desítka	k1gFnPc4	desítka
kilometrů	kilometr	k1gInPc2	kilometr
daleko	daleko	k6eAd1	daleko
-	-	kIx~	-
observatoř	observatoř	k1gFnSc1	observatoř
v	v	k7c4	v
Modře	modř	k1gFnPc4	modř
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
km	km	kA	km
a	a	k8xC	a
planetárium	planetárium	k1gNnSc1	planetárium
v	v	k7c6	v
Hlohovci	Hlohovec	k1gInSc6	Hlohovec
zhruba	zhruba	k6eAd1	zhruba
70	[number]	k4	70
km	km	kA	km
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgNnSc7d1	jediné
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
současným	současný	k2eAgMnPc3d1	současný
bratislavským	bratislavský	k2eAgMnPc3d1	bratislavský
astronomům	astronom	k1gMnPc3	astronom
sloužit	sloužit	k5eAaImF	sloužit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
astronomický	astronomický	k2eAgInSc1d1	astronomický
úsek	úsek	k1gInSc1	úsek
PKO	PKO	kA	PKO
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
různé	různý	k2eAgFnPc4d1	různá
přednášky	přednáška	k1gFnPc4	přednáška
o	o	k7c4	o
astronomii	astronomie	k1gFnSc4	astronomie
a	a	k8xC	a
sledování	sledování	k1gNnSc4	sledování
noční	noční	k2eAgFnSc2d1	noční
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
dopravní	dopravní	k2eAgFnSc7d1	dopravní
křižovatkou	křižovatka	k1gFnSc7	křižovatka
<g/>
;	;	kIx,	;
vedou	vést	k5eAaImIp3nP	vést
sem	sem	k6eAd1	sem
čtyři	čtyři	k4xCgFnPc4	čtyři
dálnice	dálnice	k1gFnPc4	dálnice
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
směr	směr	k1gInSc1	směr
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
D2	D2	k1gFnSc1	D2
(	(	kIx(	(
<g/>
směr	směr	k1gInSc1	směr
Brno	Brno	k1gNnSc1	Brno
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgInPc1d1	mnohý
železniční	železniční	k2eAgInPc1d1	železniční
spoje	spoj	k1gInPc1	spoj
<g/>
,	,	kIx,	,
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
letišti	letiště	k1gNnSc6	letiště
M.	M.	kA	M.
R.	R.	kA	R.
Štefánika	Štefánik	k1gMnSc2	Štefánik
přistávají	přistávat	k5eAaImIp3nP	přistávat
letadla	letadlo	k1gNnPc1	letadlo
z	z	k7c2	z
mnohých	mnohý	k2eAgFnPc2d1	mnohá
zemí	zem	k1gFnPc2	zem
i	i	k9	i
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Dunaj	Dunaj	k1gInSc1	Dunaj
existuje	existovat	k5eAaImIp3nS	existovat
čilá	čilý	k2eAgFnSc1d1	čilá
říční	říční	k2eAgFnSc1d1	říční
plavba	plavba	k1gFnSc1	plavba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
osobní	osobní	k2eAgNnSc1d1	osobní
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
nákladní	nákladní	k2eAgFnSc1d1	nákladní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
je	být	k5eAaImIp3nS	být
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
síť	síť	k1gFnSc1	síť
(	(	kIx(	(
<g/>
rozchod	rozchod	k1gInSc1	rozchod
1000	[number]	k4	1000
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
železniční	železniční	k2eAgFnSc1d1	železniční
dráha	dráha	k1gFnSc1	dráha
S	s	k7c7	s
(	(	kIx(	(
<g/>
11	[number]	k4	11
stanic	stanice	k1gFnPc2	stanice
mimo	mimo	k7c4	mimo
centra	centr	k1gMnSc4	centr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
i	i	k8xC	i
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c6	o
dostavbě	dostavba	k1gFnSc6	dostavba
rozestavěného	rozestavěný	k2eAgNnSc2d1	rozestavěné
podzemního	podzemní	k2eAgNnSc2d1	podzemní
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
ale	ale	k9	ale
připravuje	připravovat	k5eAaImIp3nS	připravovat
stavba	stavba	k1gFnSc1	stavba
tzv.	tzv.	kA	tzv.
nosného	nosný	k2eAgInSc2d1	nosný
systému	systém	k1gInSc2	systém
MHD	MHD	kA	MHD
-	-	kIx~	-
rychlodrážní	rychlodrážní	k2eAgFnSc2d1	rychlodrážní
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Letiště	letiště	k1gNnSc1	letiště
M.	M.	kA	M.
R.	R.	kA	R.
Štefánika	Štefánik	k1gMnSc2	Štefánik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
odbavilo	odbavit	k5eAaPmAgNnS	odbavit
přibližně	přibližně	k6eAd1	přibližně
2,02	[number]	k4	2,02
milionu	milion	k4xCgInSc2	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sport	sport	k1gInSc1	sport
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
se	se	k3xPyFc4	se
již	již	k6eAd1	již
tradičně	tradičně	k6eAd1	tradičně
hrají	hrát	k5eAaImIp3nP	hrát
různé	různý	k2eAgInPc4d1	různý
sporty	sport	k1gInPc4	sport
<g/>
.	.	kIx.	.
</s>
<s>
Konají	konat	k5eAaImIp3nP	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
významné	významný	k2eAgFnPc1d1	významná
sportovní	sportovní	k2eAgFnPc1d1	sportovní
akce	akce	k1gFnPc1	akce
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
finále	finále	k1gNnSc2	finále
Davisova	Davisův	k2eAgInSc2d1	Davisův
poháru	pohár	k1gInSc2	pohár
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konalo	konat	k5eAaImAgNnS	konat
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Košicemi	Košice	k1gInPc7	Košice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
byl	být	k5eAaImAgInS	být
rekonstruován	rekonstruován	k2eAgInSc1d1	rekonstruován
zimní	zimní	k2eAgInSc1d1	zimní
stadion	stadion	k1gInSc1	stadion
Ondreje	Ondrej	k1gInSc2	Ondrej
Nepely	Nepela	k1gFnSc2	Nepela
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejrozšířenějším	rozšířený	k2eAgInPc3d3	nejrozšířenější
sportům	sport	k1gInPc3	sport
patří	patřit	k5eAaImIp3nS	patřit
ale	ale	k8xC	ale
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
zde	zde	k6eAd1	zde
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
hned	hned	k6eAd1	hned
čtyři	čtyři	k4xCgInPc4	čtyři
týmy	tým	k1gInPc4	tým
z	z	k7c2	z
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
slovenské	slovenský	k2eAgFnSc2d1	slovenská
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
Corgoň	Corgoň	k1gFnSc2	Corgoň
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
týmem	tým	k1gInSc7	tým
je	být	k5eAaImIp3nS	být
ŠK	ŠK	kA	ŠK
Slovan	Slovan	k1gInSc1	Slovan
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
PVP	PVP	kA	PVP
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
stadion	stadion	k1gInSc1	stadion
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Tehelném	Tehelný	k2eAgNnSc6d1	Tehelný
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
i	i	k9	i
na	na	k7c4	na
za	za	k7c2	za
začátku	začátek	k1gInSc2	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
konaly	konat	k5eAaImAgInP	konat
federální	federální	k2eAgInPc1d1	federální
duely	duel	k1gInPc1	duel
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Bratislavou	Bratislava	k1gFnSc7	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
extraligové	extraligový	k2eAgInPc4d1	extraligový
kluby	klub	k1gInPc4	klub
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
řadí	řadit	k5eAaImIp3nS	řadit
FK	FK	kA	FK
Inter	Inter	k1gInSc1	Inter
Bratislava	Bratislava	k1gFnSc1	Bratislava
založený	založený	k2eAgInSc1d1	založený
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
a	a	k8xC	a
FC	FC	kA	FC
Artmedia	Artmedium	k1gNnPc1	Artmedium
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
klubem	klub	k1gInSc7	klub
zcela	zcela	k6eAd1	zcela
nejstarším	starý	k2eAgInSc7d3	nejstarší
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
domácí	domácí	k2eAgInSc1d1	domácí
stadion	stadion	k1gInSc1	stadion
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Petržalce	Petržalka	k1gFnSc6	Petržalka
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
ale	ale	k9	ale
také	také	k9	také
i	i	k9	i
basketbal	basketbal	k1gInSc4	basketbal
<g/>
,	,	kIx,	,
házená	házená	k1gFnSc1	házená
a	a	k8xC	a
volejbal	volejbal	k1gInSc1	volejbal
<g/>
;	;	kIx,	;
jak	jak	k6eAd1	jak
mužský	mužský	k2eAgInSc1d1	mužský
(	(	kIx(	(
<g/>
Inter	Inter	k1gInSc1	Inter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
ženský	ženský	k2eAgInSc1d1	ženský
(	(	kIx(	(
<g/>
Slovan	Slovan	k1gInSc1	Slovan
<g/>
)	)	kIx)	)
basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
tým	tým	k1gInSc1	tým
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
slovenské	slovenský	k2eAgFnSc2d1	slovenská
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
arénou	aréna	k1gFnSc7	aréna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
zázemí	zázemí	k1gNnSc4	zázemí
těmto	tento	k3xDgInPc3	tento
týmům	tým	k1gInPc3	tým
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Inter	Inter	k1gMnSc1	Inter
Arena	Aren	k1gInSc2	Aren
Pasienky	Pasienka	k1gFnSc2	Pasienka
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgFnPc4d1	vhodná
podmínky	podmínka	k1gFnPc4	podmínka
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
kanoistika	kanoistika	k1gFnSc1	kanoistika
<g/>
,	,	kIx,	,
nalézá	nalézat	k5eAaImIp3nS	nalézat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
množství	množství	k1gNnSc1	množství
klubů	klub	k1gInPc2	klub
zaměřených	zaměřený	k2eAgInPc2d1	zaměřený
na	na	k7c4	na
vodní	vodní	k2eAgInPc4d1	vodní
sporty	sport	k1gInPc4	sport
<g/>
.	.	kIx.	.
</s>
