<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Slovenska	Slovensko	k1gNnSc2	Slovensko
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Preßburg	Preßburg	k1gInSc1	Preßburg
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Pozsony	Pozsona	k1gFnSc2	Pozsona
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Posonium	Posonium	k1gNnSc1	Posonium
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
slovensky	slovensky	k6eAd1	slovensky
Prešporok	Prešporok	k1gInSc1	Prešporok
<g/>
/	/	kIx~	/
<g/>
Prešporek	Prešporek	k1gInSc1	Prešporek
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
česky	česky	k6eAd1	česky
Prešpurk	Prešpurk	k?	Prešpurk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
Bratislavského	bratislavský	k2eAgInSc2d1	bratislavský
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
historická	historický	k2eAgFnSc1d1	historická
metropole	metropole	k1gFnSc1	metropole
někdejších	někdejší	k2eAgFnPc2d1	někdejší
žup	župa	k1gFnPc2	župa
Prešpurské	prešpurský	k2eAgFnSc2d1	prešpurský
a	a	k8xC	a
Bratislavské	bratislavský	k2eAgFnSc2d1	Bratislavská
<g/>
.	.	kIx.	.
</s>
