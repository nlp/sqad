<p>
<s>
František	František	k1gMnSc1	František
Gellner	Gellner	k1gMnSc1	Gellner
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1881	[number]	k4	1881
<g/>
,	,	kIx,	,
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
–	–	k?	–
asi	asi	k9	asi
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
prohlášen	prohlášen	k2eAgMnSc1d1	prohlášen
za	za	k7c4	za
nezvěstného	zvěstný	k2eNgMnSc4d1	nezvěstný
na	na	k7c6	na
haličské	haličský	k2eAgFnSc6d1	Haličská
frontě	fronta	k1gFnSc6	fronta
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
nadaný	nadaný	k2eAgMnSc1d1	nadaný
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
kreslíř	kreslíř	k1gMnSc1	kreslíř
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
karikaturista	karikaturista	k1gMnSc1	karikaturista
a	a	k8xC	a
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
tzv.	tzv.	kA	tzv.
generace	generace	k1gFnSc1	generace
anarchistických	anarchistický	k2eAgMnPc2d1	anarchistický
buřičů	buřič	k1gMnPc2	buřič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Gellner	Gellner	k1gMnSc1	Gellner
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
nepříliš	příliš	k6eNd1	příliš
zámožného	zámožný	k2eAgMnSc2d1	zámožný
židovského	židovský	k2eAgMnSc2d1	židovský
obchodníka	obchodník	k1gMnSc2	obchodník
<g/>
,	,	kIx,	,
cestoval	cestovat	k5eAaImAgMnS	cestovat
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
jako	jako	k8xS	jako
kramář	kramář	k1gMnSc1	kramář
na	na	k7c6	na
pouti	pouť	k1gFnSc6	pouť
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
poznával	poznávat	k5eAaImAgMnS	poznávat
pouťovou	pouťový	k2eAgFnSc4d1	pouťová
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
nalézal	nalézat	k5eAaImAgMnS	nalézat
zde	zde	k6eAd1	zde
inspiraci	inspirace	k1gFnSc4	inspirace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
studoval	studovat	k5eAaImAgMnS	studovat
techniku	technika	k1gFnSc4	technika
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
báňské	báňský	k2eAgFnSc6d1	báňská
akademii	akademie	k1gFnSc6	akademie
v	v	k7c6	v
Příbrami	Příbram	k1gFnSc6	Příbram
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
odjel	odjet	k5eAaPmAgMnS	odjet
studovat	studovat	k5eAaImF	studovat
malířství	malířství	k1gNnSc4	malířství
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
a	a	k8xC	a
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
zahájil	zahájit	k5eAaPmAgMnS	zahájit
své	svůj	k3xOyFgNnSc4	svůj
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
malířské	malířský	k2eAgFnSc6d1	malířská
akademii	akademie	k1gFnSc6	akademie
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
však	však	k9	však
znovu	znovu	k6eAd1	znovu
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
kreslíř	kreslíř	k1gMnSc1	kreslíř
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
mnoha	mnoho	k4c2	mnoho
karikatur	karikatura	k1gFnPc2	karikatura
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
novinářů	novinář	k1gMnPc2	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
proslul	proslout	k5eAaPmAgMnS	proslout
také	také	k9	také
jako	jako	k9	jako
fejetonista	fejetonista	k1gMnSc1	fejetonista
a	a	k8xC	a
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1914	[number]	k4	1914
narukoval	narukovat	k5eAaPmAgMnS	narukovat
do	do	k7c2	do
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Mladé	mladý	k2eAgFnSc2d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
byl	být	k5eAaImAgInS	být
odvelen	odvelen	k2eAgInSc1d1	odvelen
na	na	k7c4	na
haličskou	haličský	k2eAgFnSc4d1	Haličská
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Pochod	pochod	k1gInSc1	pochod
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
vyčerpal	vyčerpat	k5eAaPmAgInS	vyčerpat
a	a	k8xC	a
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nP	zářit
si	se	k3xPyFc3	se
nakonec	nakonec	k6eAd1	nakonec
lehl	lehnout	k5eAaPmAgMnS	lehnout
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čekal	čekat	k5eAaImAgMnS	čekat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
osud	osud	k1gInSc4	osud
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
nezvěstného	zvěstný	k2eNgMnSc4d1	nezvěstný
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
datum	datum	k1gNnSc4	datum
a	a	k8xC	a
místo	místo	k7c2	místo
jeho	jeho	k3xOp3gNnPc2	jeho
úmrtí	úmrtí	k1gNnPc2	úmrtí
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
ulice	ulice	k1gFnSc1	ulice
Gellnerova	Gellnerův	k2eAgFnSc1d1	Gellnerova
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Brno-Jundrov	Brno-Jundrov	k1gInSc4	Brno-Jundrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Básně	báseň	k1gFnSc2	báseň
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Nechci	chtít	k5eNaImIp1nS	chtít
se	se	k3xPyFc4	se
vyvyšovat	vyvyšovat	k5eAaImF	vyvyšovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
průkopníci	průkopník	k1gMnPc1	průkopník
nových	nový	k2eAgInPc2d1	nový
směrů	směr	k1gInPc2	směr
uznají	uznat	k5eAaPmIp3nP	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
já	já	k3xPp1nSc1	já
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
zavedl	zavést	k5eAaPmAgInS	zavést
do	do	k7c2	do
českého	český	k2eAgNnSc2d1	české
básnictví	básnictví	k1gNnSc2	básnictví
moderní	moderní	k2eAgFnSc2d1	moderní
vymoženosti	vymoženost	k1gFnSc2	vymoženost
jako	jako	k8xS	jako
pivo	pivo	k1gNnSc1	pivo
<g/>
,	,	kIx,	,
viržinka	viržinka	k1gFnSc1	viržinka
atd.	atd.	kA	atd.
Volný	volný	k2eAgInSc4d1	volný
verš	verš	k1gInSc4	verš
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
neoblíbil	oblíbit	k5eNaPmAgMnS	oblíbit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
málo	málo	k6eAd1	málo
citu	cit	k1gInSc2	cit
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
upřílišněným	upřílišněný	k2eAgInPc3d1	upřílišněný
požadavkům	požadavek	k1gInPc3	požadavek
srdce	srdce	k1gNnSc2	srdce
je	být	k5eAaImIp3nS	být
marno	marno	k1gNnSc4	marno
bojovat	bojovat	k5eAaImF	bojovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
současníků	současník	k1gMnPc2	současník
se	se	k3xPyFc4	se
odlišoval	odlišovat	k5eAaImAgMnS	odlišovat
antiliterárností	antiliterárnost	k1gFnSc7	antiliterárnost
<g/>
:	:	kIx,	:
Básničky	básnička	k1gFnPc1	básnička
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
písničkám	písnička	k1gFnPc3	písnička
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
krátké	krátký	k2eAgInPc4d1	krátký
verše	verš	k1gInPc4	verš
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
i	i	k9	i
krátké	krátký	k2eAgFnSc2d1	krátká
strofy	strofa	k1gFnSc2	strofa
<g/>
,	,	kIx,	,
některá	některý	k3yIgFnSc1	některý
strofa	strofa	k1gFnSc1	strofa
se	se	k3xPyFc4	se
i	i	k9	i
opakuje	opakovat	k5eAaImIp3nS	opakovat
jako	jako	k9	jako
refrén	refrén	k1gInSc1	refrén
<g/>
,	,	kIx,	,
nepoužívá	používat	k5eNaImIp3nS	používat
obrazná	obrazný	k2eAgNnPc4d1	obrazné
pojmenování	pojmenování	k1gNnPc4	pojmenování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pojmenovává	pojmenovávat	k5eAaImIp3nS	pojmenovávat
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
básně	báseň	k1gFnPc1	báseň
mají	mít	k5eAaImIp3nP	mít
písňovou	písňový	k2eAgFnSc4d1	písňová
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gellnerovy	Gellnerův	k2eAgInPc1d1	Gellnerův
verše	verš	k1gInPc1	verš
jsou	být	k5eAaImIp3nP	být
prosté	prostý	k2eAgInPc1d1	prostý
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
verš	verš	k1gInSc4	verš
i	i	k8xC	i
rým	rým	k1gInSc4	rým
a	a	k8xC	a
blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
prozaickému	prozaický	k2eAgNnSc3d1	prozaické
sdělení	sdělení	k1gNnSc3	sdělení
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
napsány	napsán	k2eAgFnPc1d1	napsána
formou	forma	k1gFnSc7	forma
popěvku	popěvek	k1gInSc2	popěvek
nebo	nebo	k8xC	nebo
písně	píseň	k1gFnSc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Gellner	Gellner	k1gMnSc1	Gellner
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
básnické	básnický	k2eAgFnSc6d1	básnická
tvorbě	tvorba	k1gFnSc6	tvorba
hojně	hojně	k6eAd1	hojně
využíval	využívat	k5eAaImAgMnS	využívat
dobových	dobový	k2eAgFnPc2d1	dobová
forem	forma	k1gFnPc2	forma
lidové	lidový	k2eAgFnSc2d1	lidová
zábavy	zábava	k1gFnSc2	zábava
(	(	kIx(	(
<g/>
kuplety	kuplet	k1gInPc1	kuplet
<g/>
,	,	kIx,	,
šansony	šanson	k1gInPc1	šanson
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otevřenost	otevřenost	k1gFnSc1	otevřenost
sdělení	sdělení	k1gNnSc2	sdělení
je	být	k5eAaImIp3nS	být
až	až	k6eAd1	až
šokující	šokující	k2eAgMnSc1d1	šokující
<g/>
.	.	kIx.	.
</s>
<s>
Zdánlivě	zdánlivě	k6eAd1	zdánlivě
autor	autor	k1gMnSc1	autor
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
čtenářů	čtenář	k1gMnPc2	čtenář
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
o	o	k7c4	o
sobě	se	k3xPyFc3	se
kriticky	kriticky	k6eAd1	kriticky
a	a	k8xC	a
s	s	k7c7	s
cynismem	cynismus	k1gInSc7	cynismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cynikem	cynik	k1gMnSc7	cynik
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
moralizovat	moralizovat	k5eAaImF	moralizovat
<g/>
.	.	kIx.	.
</s>
<s>
Zastírá	zastírat	k5eAaImIp3nS	zastírat
přitom	přitom	k6eAd1	přitom
touhou	touha	k1gFnSc7	touha
po	po	k7c6	po
skutečném	skutečný	k2eAgInSc6d1	skutečný
citu	cit	k1gInSc6	cit
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
bolest	bolest	k1gFnSc4	bolest
a	a	k8xC	a
zklamání	zklamání	k1gNnSc4	zklamání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
výrazně	výrazně	k6eAd1	výrazně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
anarchismem	anarchismus	k1gInSc7	anarchismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
je	být	k5eAaImIp3nS	být
též	též	k9	též
ideálem	ideál	k1gInSc7	ideál
básníka	básník	k1gMnSc2	básník
podle	podle	k7c2	podle
F.	F.	kA	F.
X.	X.	kA	X.
Šaldy	Šalda	k1gMnSc2	Šalda
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
oprošťuje	oprošťovat	k5eAaImIp3nS	oprošťovat
od	od	k7c2	od
"	"	kIx"	"
<g/>
veškerých	veškerý	k3xTgInPc2	veškerý
zbytečných	zbytečný	k2eAgInPc2d1	zbytečný
poetismů	poetismus	k1gInPc2	poetismus
<g/>
"	"	kIx"	"
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
poezii	poezie	k1gFnSc4	poezie
velice	velice	k6eAd1	velice
přímou	přímý	k2eAgFnSc4d1	přímá
s	s	k7c7	s
minimem	minimum	k1gNnSc7	minimum
složitých	složitý	k2eAgInPc2d1	složitý
obrazů	obraz	k1gInPc2	obraz
známých	známý	k2eAgInPc2d1	známý
například	například	k6eAd1	například
od	od	k7c2	od
tzv.	tzv.	kA	tzv.
francouzských	francouzský	k2eAgMnPc2d1	francouzský
prokletých	prokletý	k2eAgMnPc2d1	prokletý
básníků	básník	k1gMnPc2	básník
(	(	kIx(	(
<g/>
lartpourlartistů	lartpourlartista	k1gMnPc2	lartpourlartista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
styl	styl	k1gInSc1	styl
dal	dát	k5eAaPmAgInS	dát
nazvat	nazvat	k5eAaBmF	nazvat
"	"	kIx"	"
<g/>
expresionismem	expresionismus	k1gInSc7	expresionismus
v	v	k7c4	v
poesii	poesie	k1gFnSc4	poesie
<g/>
"	"	kIx"	"
–	–	k?	–
důležité	důležitý	k2eAgNnSc1d1	důležité
je	on	k3xPp3gInPc4	on
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
a	a	k8xC	a
nezkresleně	zkresleně	k6eNd1	zkresleně
<g/>
,	,	kIx,	,
opisy	opis	k1gInPc1	opis
jsou	být	k5eAaImIp3nP	být
zbytečné	zbytečný	k2eAgMnPc4d1	zbytečný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Umělecké	umělecký	k2eAgInPc4d1	umělecký
prostředky	prostředek	k1gInPc4	prostředek
====	====	k?	====
</s>
</p>
<p>
<s>
Gellner	Gellner	k1gInSc1	Gellner
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
básních	báseň	k1gFnPc6	báseň
často	často	k6eAd1	často
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
rytmus	rytmus	k1gInSc1	rytmus
lidovému	lidový	k2eAgInSc3d1	lidový
popěvku	popěvek	k1gInSc3	popěvek
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
banální	banální	k2eAgFnPc4d1	banální
rýmy	rýma	k1gFnPc4	rýma
<g/>
,	,	kIx,	,
nepoetická	poetický	k2eNgNnPc4d1	nepoetické
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
častá	častý	k2eAgNnPc4d1	časté
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
i	i	k9	i
nemetaforičnost	nemetaforičnost	k1gFnSc4	nemetaforičnost
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
symbolistů	symbolista	k1gMnPc2	symbolista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
verši	verš	k1gInPc7	verš
chce	chtít	k5eAaImIp3nS	chtít
vědomě	vědomě	k6eAd1	vědomě
šokovat	šokovat	k5eAaBmF	šokovat
poklidné	poklidný	k2eAgNnSc4d1	poklidné
měšťanstvo	měšťanstvo	k1gNnSc4	měšťanstvo
<g/>
,	,	kIx,	,
užívá	užívat	k5eAaImIp3nS	užívat
vulgarismy	vulgarismus	k1gInPc1	vulgarismus
<g/>
.	.	kIx.	.
</s>
<s>
Verš	verš	k1gInSc1	verš
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
<g/>
,	,	kIx,	,
forma	forma	k1gFnSc1	forma
popěvku	popěvek	k1gInSc2	popěvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sbírky	sbírka	k1gFnSc2	sbírka
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c4	po
nás	my	k3xPp1nPc4	my
ať	ať	k9	ať
přijde	přijít	k5eAaPmIp3nS	přijít
potopa	potopa	k1gFnSc1	potopa
Dostupné	dostupný	k2eAgFnSc2d1	dostupná
jako	jako	k8xS	jako
e-kniha	enih	k1gMnSc2	e-knih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
subjektivní	subjektivní	k2eAgFnSc1d1	subjektivní
lyrika	lyrika	k1gFnSc1	lyrika
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgFnSc1d1	osobní
cynická	cynický	k2eAgFnSc1d1	cynická
zpověď	zpověď	k1gFnSc1	zpověď
<g/>
,	,	kIx,	,
prvky	prvek	k1gInPc1	prvek
anarchismu	anarchismus	k1gInSc2	anarchismus
</s>
</p>
<p>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
zklamání	zklamání	k1gNnSc1	zklamání
nad	nad	k7c7	nad
malostí	malost	k1gFnSc7	malost
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odpor	odpor	k1gInSc1	odpor
k	k	k7c3	k
měšťáckému	měšťácký	k2eAgInSc3d1	měšťácký
životu	život	k1gInSc3	život
<g/>
,	,	kIx,	,
skeptický	skeptický	k2eAgInSc1d1	skeptický
postoj	postoj	k1gInSc1	postoj
ke	k	k7c3	k
společnosti	společnost	k1gFnSc3	společnost
<g/>
,	,	kIx,	,
hořkost	hořkost	k1gFnSc1	hořkost
nad	nad	k7c7	nad
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
zklamání	zklamání	k1gNnSc4	zklamání
nad	nad	k7c7	nad
dobou	doba	k1gFnSc7	doba
a	a	k8xC	a
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
hrubá	hrubý	k2eAgNnPc1d1	hrubé
slova	slovo	k1gNnPc1	slovo
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
</s>
</p>
<p>
<s>
Přetékající	přetékající	k2eAgInSc1d1	přetékající
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
<g/>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
plném	plný	k2eAgInSc6d1	plný
poháru	pohár	k1gInSc6	pohár
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
co	co	k3yInSc1	co
vše	všechen	k3xTgNnSc1	všechen
udělá	udělat	k5eAaPmIp3nS	udělat
vypití	vypití	k1gNnSc4	vypití
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
opojné	opojný	k2eAgNnSc1d1	opojné
<g/>
/	/	kIx~	/
<g/>
alkoholické	alkoholický	k2eAgInPc1d1	alkoholický
<g/>
,	,	kIx,	,
zažene	zahnat	k5eAaPmIp3nS	zahnat
žízen	žízen	k2eAgMnSc1d1	žízen
a	a	k8xC	a
posilní	posilnit	k5eAaPmIp3nS	posilnit
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Radosti	radost	k1gFnSc3	radost
života	život	k1gInSc2	život
Dostupné	dostupný	k2eAgFnSc2d1	dostupná
jako	jako	k8xC	jako
e-kniha	enih	k1gMnSc2	e-knih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
35	[number]	k4	35
básní	báseň	k1gFnPc2	báseň
různé	různý	k2eAgFnSc2d1	různá
tematiky	tematika	k1gFnSc2	tematika
a	a	k8xC	a
žánru	žánr	k1gInSc2	žánr
</s>
</p>
<p>
<s>
Reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
falešnou	falešný	k2eAgFnSc4d1	falešná
dobovou	dobový	k2eAgFnSc4d1	dobová
morálku	morálka	k1gFnSc4	morálka
<g/>
,	,	kIx,	,
na	na	k7c4	na
dobové	dobový	k2eAgNnSc4d1	dobové
pokrytecké	pokrytecký	k2eAgNnSc4d1	pokrytecké
maloměšťáctví	maloměšťáctví	k1gNnSc4	maloměšťáctví
<g/>
.	.	kIx.	.
</s>
<s>
Gellner	Gellner	k1gInSc1	Gellner
záměrně	záměrně	k6eAd1	záměrně
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
protiliterárnost	protiliterárnost	k1gFnSc4	protiliterárnost
a	a	k8xC	a
protidekadenci	protidekadence	k1gFnSc4	protidekadence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ironie	ironie	k1gFnPc1	ironie
<g/>
,	,	kIx,	,
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
na	na	k7c4	na
mládí	mládí	k1gNnSc4	mládí
<g/>
,	,	kIx,	,
popis	popis	k1gInSc1	popis
bohémského	bohémský	k2eAgInSc2d1	bohémský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Báseň	báseň	k1gFnSc1	báseň
Všichni	všechen	k3xTgMnPc1	všechen
mi	já	k3xPp1nSc3	já
lhali	lhát	k5eAaImAgMnP	lhát
(	(	kIx(	(
<g/>
Pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
slepě	slepě	k6eAd1	slepě
věřil	věřit	k5eAaImAgInS	věřit
všemu	všecek	k3xTgMnSc3	všecek
a	a	k8xC	a
že	že	k8xS	že
svět	svět	k1gInSc1	svět
bude	být	k5eAaImBp3nS	být
lepší	dobrý	k2eAgInSc1d2	lepší
<g/>
.	.	kIx.	.
</s>
<s>
Nechce	chtít	k5eNaImIp3nS	chtít
být	být	k5eAaImF	být
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pokrytecká	pokrytecký	k2eAgFnSc1d1	pokrytecká
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
se	se	k3xPyFc4	se
něčím	něčí	k3xOyIgMnPc3	něčí
přetvařujeme	přetvařovat	k5eAaImIp1nP	přetvařovat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
verše	verš	k1gInPc1	verš
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
vydaný	vydaný	k2eAgInSc1d1	vydaný
soubor	soubor	k1gInSc1	soubor
básní	báseň	k1gFnPc2	báseň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jakési	jakýsi	k3yIgNnSc4	jakýsi
zklidnění	zklidnění	k1gNnSc4	zklidnění
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
jako	jako	k8xC	jako
e-kniha	eniha	k1gFnSc1	e-kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
(	(	kIx(	(
<g/>
časopisecky	časopisecky	k6eAd1	časopisecky
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
knižně	knižně	k6eAd1	knižně
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
ve	v	k7c6	v
verších	verš	k1gInPc6	verš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
jako	jako	k8xS	jako
e-kniha	eniha	k1gFnSc1	e-kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potulný	potulný	k2eAgInSc1d1	potulný
národ	národ	k1gInSc1	národ
</s>
</p>
<p>
<s>
povídka	povídka	k1gFnSc1	povídka
s	s	k7c7	s
povídkou	povídka	k1gFnSc7	povídka
Veselý	Veselý	k1gMnSc1	Veselý
koník	koník	k1gMnSc1	koník
<g/>
,	,	kIx,	,
knižně	knižně	k6eAd1	knižně
1926	[number]	k4	1926
v	v	k7c6	v
sebraných	sebraný	k2eAgInPc6d1	sebraný
spisech	spis	k1gInPc6	spis
</s>
</p>
<p>
<s>
Torzo	torzo	k1gNnSc1	torzo
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nedopsal	dopsat	k5eNaPmAgMnS	dopsat
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
bohémská	bohémský	k2eAgFnSc1d1	bohémská
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Ukázky	ukázka	k1gFnPc1	ukázka
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
====	====	k?	====
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
držím	držet	k5eAaImIp1nS	držet
pohár	pohár	k1gInSc4	pohár
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dlani	dlaň	k1gFnSc6	dlaň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
zpěněný	zpěněný	k2eAgMnSc1d1	zpěněný
a	a	k8xC	a
přetéká	přetékat	k5eAaImIp3nS	přetékat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Já	já	k3xPp1nSc1	já
držím	držet	k5eAaImIp1nS	držet
pohár	pohár	k1gInSc4	pohár
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dlani	dlaň	k1gFnSc6	dlaň
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
jenž	jenž	k3xRgMnSc1	jenž
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
rty	ret	k1gInPc4	ret
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
<g/>
Jenž	jenž	k3xRgMnSc1	jenž
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
víno	víno	k1gNnSc1	víno
jeho	jeho	k3xOp3gFnSc2	jeho
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
do	do	k7c2	do
brázd	brázda	k1gFnPc2	brázda
vyschlých	vyschlý	k2eAgFnPc2d1	vyschlá
rozleje	rozlít	k5eAaPmIp3nS	rozlít
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
na	na	k7c6	na
snivých	snivý	k2eAgInPc6d1	snivý
květech	květ	k1gInPc6	květ
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
světech	svět	k1gInPc6	svět
</s>
</p>
<p>
<s>
zda	zda	k8xS	zda
zavěsí	zavěsit	k5eAaPmIp3nS	zavěsit
své	svůj	k3xOyFgFnSc2	svůj
krůpěje	krůpěj	k1gFnSc2	krůpěj
<g/>
.	.	kIx.	.
<g/>
Jenž	jenž	k3xRgMnSc1	jenž
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
sehnou	sehnout	k5eAaPmIp3nP	sehnout
květy	květ	k1gInPc7	květ
</s>
</p>
<p>
<s>
pod	pod	k7c7	pod
onou	onen	k3xDgFnSc7	onen
tíží	tíž	k1gFnSc7	tíž
ku	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jenž	jenž	k3xRgMnSc1	jenž
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
jiné	jiný	k2eAgInPc1d1	jiný
světy	svět	k1gInPc1	svět
</s>
</p>
<p>
<s>
rozzáří	rozzářit	k5eAaPmIp3nS	rozzářit
svými	svůj	k3xOyFgFnPc7	svůj
vůněmi	vůně	k1gFnPc7	vůně
<g/>
.	.	kIx.	.
<g/>
Já	já	k3xPp1nSc1	já
držím	držet	k5eAaImIp1nS	držet
pohár	pohár	k1gInSc4	pohár
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dlani	dlaň	k1gFnSc6	dlaň
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
jenž	jenž	k3xRgMnSc1	jenž
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
rty	ret	k1gInPc4	ret
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Já	já	k3xPp1nSc1	já
držím	držet	k5eAaImIp1nS	držet
pohár	pohár	k1gInSc4	pohár
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dlani	dlaň	k1gFnSc6	dlaň
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
své	svůj	k3xOyFgNnSc4	svůj
srdce	srdce	k1gNnSc4	srdce
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
přetéká	přetékat	k5eAaImIp3nS	přetékat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
báseň	báseň	k1gFnSc4	báseň
Přetékající	přetékající	k2eAgInSc4d1	přetékající
pohár	pohár	k1gInSc4	pohár
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Po	po	k7c4	po
nás	my	k3xPp1nPc4	my
ať	ať	k9	ať
přijde	přijít	k5eAaPmIp3nS	přijít
potopa	potopa	k1gFnSc1	potopa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Má	můj	k3xOp1gFnSc1	můj
milá	milý	k2eAgFnSc1d1	Milá
rozmilá	rozmilá	k1gFnSc1	rozmilá
<g/>
,	,	kIx,	,
neplakej	plakat	k5eNaImRp2nS	plakat
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
jinakej	jinakej	k?	jinakej
<g/>
.	.	kIx.	.
<g/>
Dnes	dnes	k6eAd1	dnes
buďme	budit	k5eAaImRp1nP	budit
ještě	ještě	k9	ještě
veselí	veselí	k1gNnSc4	veselí
</s>
</p>
<p>
<s>
na	na	k7c6	na
naší	náš	k3xOp1gFnSc6	náš
bílé	bílý	k2eAgFnSc6d1	bílá
posteli	postel	k1gFnSc6	postel
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
Zejtra	Zejtra	k?	Zejtra
<g/>
,	,	kIx,	,
co	co	k9	co
zejtra	zejtra	k?	zejtra
<g/>
?	?	kIx.	?
</s>
<s>
Kdožpak	Kdožpak	k6eAd1	Kdožpak
ví	vědět	k5eAaImIp3nS	vědět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zejtra	Zejtra	k?	Zejtra
si	se	k3xPyFc3	se
lehneme	lehnout	k5eAaPmIp1nP	lehnout
do	do	k7c2	do
rakví	rakev	k1gFnPc2	rakev
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
báseň	báseň	k1gFnSc1	báseň
Perspektiva	perspektiva	k1gFnSc1	perspektiva
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Po	po	k7c4	po
nás	my	k3xPp1nPc4	my
ať	ať	k9	ať
přijde	přijít	k5eAaPmIp3nS	přijít
potopa	potopa	k1gFnSc1	potopa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Má	můj	k3xOp1gFnSc1	můj
řeč	řeč	k1gFnSc1	řeč
je	být	k5eAaImIp3nS	být
hrubá	hrubý	k2eAgFnSc1d1	hrubá
jako	jako	k8xS	jako
můj	můj	k3xOp1gInSc1	můj
smích	smích	k1gInSc1	smích
</s>
</p>
<p>
<s>
a	a	k8xC	a
jako	jako	k9	jako
moji	můj	k3xOp1gMnPc1	můj
známí	známý	k2eAgMnPc1d1	známý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k9	a
alkohol	alkohol	k1gInSc1	alkohol
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc4	ten
myslil	myslit	k5eAaImAgMnS	myslit
bych	by	kYmCp1nS	by
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jemnosti	jemnost	k1gFnPc4	jemnost
nepřidá	přidat	k5eNaPmIp3nS	přidat
mi	já	k3xPp1nSc3	já
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
z	z	k7c2	z
básně	báseň	k1gFnSc2	báseň
Což	což	k9	což
<g/>
,	,	kIx,	,
páni	pan	k1gMnPc1	pan
spisovatelé	spisovatel	k1gMnPc1	spisovatel
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Radosti	radost	k1gFnSc2	radost
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
mi	já	k3xPp1nSc3	já
lhali	lhát	k5eAaImAgMnP	lhát
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
mi	já	k3xPp1nSc3	já
lhali	lhát	k5eAaImAgMnP	lhát
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
blázna	blázen	k1gMnSc4	blázen
si	se	k3xPyFc3	se
ze	z	k7c2	z
mne	já	k3xPp1nSc4	já
dělali	dělat	k5eAaImAgMnP	dělat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přede	před	k7c7	před
mnou	já	k3xPp1nSc7	já
citem	cit	k1gInSc7	cit
se	se	k3xPyFc4	se
rozplývali	rozplývat	k5eAaImAgMnP	rozplývat
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
za	za	k7c7	za
zády	záda	k1gNnPc7	záda
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
vysmáli	vysmát	k5eAaPmAgMnP	vysmát
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Srdce	srdce	k1gNnSc1	srdce
mé	můj	k3xOp1gFnSc2	můj
stále	stále	k6eAd1	stále
po	po	k7c6	po
lásce	láska	k1gFnSc6	láska
prahne	prahnout	k5eAaImIp3nS	prahnout
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
nikomu	nikdo	k3yNnSc3	nikdo
však	však	k9	však
již	již	k6eAd1	již
nevěřím	věřit	k5eNaImIp1nS	věřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
někdo	někdo	k3yInSc1	někdo
ke	k	k7c3	k
mně	já	k3xPp1nSc6	já
ruce	ruka	k1gFnSc6	ruka
své	svůj	k3xOyFgNnSc4	svůj
vztáhne	vztáhnout	k5eAaPmIp3nS	vztáhnout
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ustoupím	ustoupit	k5eAaPmIp1nS	ustoupit
bojácně	bojácně	k6eAd1	bojácně
ke	k	k7c3	k
dveřím	dveře	k1gFnPc3	dveře
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
z	z	k7c2	z
básně	báseň	k1gFnSc2	báseň
Všichni	všechen	k3xTgMnPc1	všechen
mi	já	k3xPp1nSc3	já
lhali	lhát	k5eAaImAgMnP	lhát
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Radosti	radost	k1gFnSc2	radost
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Nezemru	Nezemru	k?	Nezemru
já	já	k3xPp1nSc1	já
od	od	k7c2	od
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
nezahynu	zahynout	k5eNaPmIp1nS	zahynout
bídou	bída	k1gFnSc7	bída
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
nezalknu	zalknout	k5eNaPmIp1nS	zalknout
se	se	k3xPyFc4	se
v	v	k7c6	v
oprátce	oprátka	k1gFnSc6	oprátka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
skončím	skončit	k5eAaPmIp1nS	skončit
sifilitidou	sifilitida	k1gFnSc7	sifilitida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
z	z	k7c2	z
básně	báseň	k1gFnSc2	báseň
Píseň	píseň	k1gFnSc1	píseň
zhýralého	zhýralý	k2eAgMnSc2d1	zhýralý
jinocha	jinoch	k1gMnSc2	jinoch
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Básně	báseň	k1gFnSc2	báseň
z	z	k7c2	z
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Karikatury	karikatura	k1gFnSc2	karikatura
===	===	k?	===
</s>
</p>
<p>
<s>
Karikatury	karikatura	k1gFnPc1	karikatura
Františka	František	k1gMnSc2	František
Gellnera	Gellner	k1gMnSc2	Gellner
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
František	František	k1gMnSc1	František
Gellner	Gellner	k1gMnSc1	Gellner
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
hudbě	hudba	k1gFnSc6	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
básně	báseň	k1gFnPc1	báseň
byly	být	k5eAaImAgFnP	být
zhudebněny	zhudebnit	k5eAaPmNgInP	zhudebnit
a	a	k8xC	a
hrány	hrát	k5eAaImNgInP	hrát
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
vydávány	vydávat	k5eAaPmNgInP	vydávat
na	na	k7c4	na
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Juraje	Juraj	k1gInSc2	Juraj
Herze	Herze	k1gFnSc2	Herze
Petrolejové	petrolejový	k2eAgFnSc2d1	petrolejová
lampy	lampa	k1gFnSc2	lampa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
skladatel	skladatel	k1gMnSc1	skladatel
Luboš	Luboš	k1gMnSc1	Luboš
Fišer	Fišer	k1gMnSc1	Fišer
zhudebnil	zhudebnit	k5eAaPmAgMnS	zhudebnit
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
originálu	originál	k1gMnSc6	originál
velmi	velmi	k6eAd1	velmi
věrně	věrně	k6eAd1	věrně
Gellnerovy	Gellnerův	k2eAgFnPc4d1	Gellnerova
básně	báseň	k1gFnPc4	báseň
jako	jako	k8xS	jako
kuplety	kuplet	k1gInPc4	kuplet
a	a	k8xC	a
šansony	šanson	k1gInPc4	šanson
přelomu	přelom	k1gInSc2	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
hlavně	hlavně	k9	hlavně
Josef	Josef	k1gMnSc1	Josef
Laufer	Laufer	k1gMnSc1	Laufer
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Karel	Karel	k1gMnSc1	Karel
Černoch	Černoch	k1gMnSc1	Černoch
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Rosůlková	Rosůlková	k1gFnSc1	Rosůlková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
televizní	televizní	k2eAgFnSc6d1	televizní
inscenaci	inscenace	k1gFnSc6	inscenace
Evžena	Evžen	k1gMnSc2	Evžen
Sokolovského	sokolovský	k2eAgInSc2d1	sokolovský
Radosti	radost	k1gFnPc4	radost
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Kundery	Kundera	k1gFnSc2	Kundera
Jiří	Jiří	k1gMnSc1	Jiří
Štěpnička	Štěpnička	k1gMnSc1	Štěpnička
v	v	k7c6	v
roli	role	k1gFnSc6	role
Františka	František	k1gMnSc2	František
Gellnera	Gellner	k1gMnSc2	Gellner
zpívá	zpívat	k5eAaImIp3nS	zpívat
mnoho	mnoho	k6eAd1	mnoho
svých	svůj	k3xOyFgFnPc2	svůj
básní	báseň	k1gFnPc2	báseň
jako	jako	k8xC	jako
kuplety	kuplet	k1gInPc4	kuplet
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
snímku	snímek	k1gInSc3	snímek
napsal	napsat	k5eAaPmAgMnS	napsat
Miloš	Miloš	k1gMnSc1	Miloš
Štědroň	Štědroň	k1gMnSc1	Štědroň
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
hrával	hrávat	k5eAaImAgMnS	hrávat
Radosti	radost	k1gFnPc4	radost
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Punk-rocková	Punkockový	k2eAgFnSc1d1	Punk-rocková
skupina	skupina	k1gFnSc1	skupina
Visací	visací	k2eAgFnSc1d1	visací
zámek	zámek	k1gInSc4	zámek
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
repertoáru	repertoár	k1gInSc6	repertoár
postupně	postupně	k6eAd1	postupně
tři	tři	k4xCgFnPc1	tři
básně	báseň	k1gFnPc1	báseň
Františka	František	k1gMnSc2	František
Gellnera	Gellner	k1gMnSc2	Gellner
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
mi	já	k3xPp1nSc3	já
lhali	lhát	k5eAaImAgMnP	lhát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Perspektiva	perspektiva	k1gFnSc1	perspektiva
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Buď	budit	k5eAaImRp2nS	budit
matko	matka	k1gFnSc5	matka
Boží	boží	k2eAgMnSc1d1	boží
pomocna	pomocn	k1gInSc2	pomocn
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Báseň	báseň	k1gFnSc1	báseň
"	"	kIx"	"
<g/>
Perspektiva	perspektiva	k1gFnSc1	perspektiva
<g/>
"	"	kIx"	"
nahrála	nahrát	k5eAaBmAgFnS	nahrát
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
i	i	k8xC	i
skupina	skupina	k1gFnSc1	skupina
3045	[number]	k4	3045
mám	mít	k5eAaImIp1nS	mít
Tě	ty	k3xPp2nSc4	ty
rád	rád	k6eAd1	rád
Maruško	Maruška	k1gFnSc5	Maruška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozpadlá	rozpadlý	k2eAgFnSc1d1	rozpadlá
punková	punkový	k2eAgFnSc1d1	punková
skupina	skupina	k1gFnSc1	skupina
Malomocnost	malomocnost	k1gFnSc1	malomocnost
prázdnoty	prázdnota	k1gFnSc2	prázdnota
hrála	hrát	k5eAaImAgFnS	hrát
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Nečekám	čekat	k5eNaImIp1nS	čekat
nic	nic	k3yNnSc4	nic
od	od	k7c2	od
reforem	reforma	k1gFnPc2	reforma
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
cyklu	cyklus	k1gInSc2	cyklus
Radosti	radost	k1gFnSc2	radost
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pepa	Pepa	k1gMnSc1	Pepa
Nos	nos	k1gInSc4	nos
zhudebnil	zhudebnit	k5eAaPmAgMnS	zhudebnit
básně	báseň	k1gFnPc4	báseň
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
teď	teď	k6eAd1	teď
celá	celý	k2eAgFnSc1d1	celá
moudrost	moudrost	k1gFnSc1	moudrost
moje	můj	k3xOp1gFnSc1	můj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
mi	já	k3xPp1nSc3	já
lhali	lhát	k5eAaImAgMnP	lhát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Perspektiva	perspektiva	k1gFnSc1	perspektiva
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Babička	babička	k1gFnSc1	babička
Málková	Málková	k1gFnSc1	Málková
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Noc	noc	k1gFnSc1	noc
byla	být	k5eAaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Usnout	usnout	k5eAaPmF	usnout
nemoh	nemoh	k?	nemoh
<g/>
'	'	kIx"	'
jsem	být	k5eAaImIp1nS	být
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Rum	rum	k1gInSc4	rum
smutně	smutně	k6eAd1	smutně
pil	pít	k5eAaImAgMnS	pít
pan	pan	k1gMnSc1	pan
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Báseň	báseň	k1gFnSc1	báseň
Václava	Václav	k1gMnSc2	Václav
Hraběte	Hrabě	k1gMnSc2	Hrabě
"	"	kIx"	"
<g/>
Jam	jam	k1gInSc1	jam
Session	Session	k1gInSc1	Session
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Gellnerem	Gellner	k1gMnSc7	Gellner
<g/>
"	"	kIx"	"
zhudebnil	zhudebnit	k5eAaPmAgInS	zhudebnit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mišík	Mišík	k1gMnSc1	Mišík
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgMnSc1d1	stejný
interpret	interpret	k1gMnSc1	interpret
společně	společně	k6eAd1	společně
s	s	k7c7	s
houslistou	houslista	k1gMnSc7	houslista
Janem	Jan	k1gMnSc7	Jan
Hrubým	Hrubý	k1gMnSc7	Hrubý
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
i	i	k9	i
báseň	báseň	k1gFnSc4	báseň
"	"	kIx"	"
<g/>
Pomalu	pomalu	k6eAd1	pomalu
v	v	k7c4	v
revolver	revolver	k1gInSc4	revolver
se	se	k3xPyFc4	se
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
víra	víra	k1gFnSc1	víra
<g/>
"	"	kIx"	"
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Radosti	radost	k1gFnSc2	radost
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
první	první	k4xOgFnSc6	první
dlouhohrající	dlouhohrající	k2eAgFnSc6d1	dlouhohrající
desce	deska	k1gFnSc6	deska
skupiny	skupina	k1gFnSc2	skupina
Katapult	katapult	k1gInSc1	katapult
je	být	k5eAaImIp3nS	být
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Miluju	milovat	k5eAaImIp1nS	milovat
severní	severní	k2eAgNnSc1d1	severní
nebe	nebe	k1gNnSc1	nebe
<g/>
"	"	kIx"	"
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Po	po	k7c4	po
nás	my	k3xPp1nPc4	my
ať	ať	k9	ať
přijde	přijít	k5eAaPmIp3nS	přijít
potopa	potopa	k1gFnSc1	potopa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
CD	CD	kA	CD
Radosti	radost	k1gFnSc2	radost
života	život	k1gInSc2	život
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
skladba	skladba	k1gFnSc1	skladba
zařazen	zařadit	k5eAaPmNgInS	zařadit
"	"	kIx"	"
<g/>
Přetékající	přetékající	k2eAgInSc1d1	přetékající
pohár	pohár	k1gInSc1	pohár
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
V	v	k7c6	v
albu	album	k1gNnSc6	album
Kladivo	kladivo	k1gNnSc1	kladivo
na	na	k7c4	na
život	život	k1gInSc4	život
je	být	k5eAaImIp3nS	být
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Osobní	osobní	k2eAgInSc1d1	osobní
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
na	na	k7c4	na
CD	CD	kA	CD
Pozor	pozor	k1gInSc4	pozor
<g/>
,	,	kIx,	,
rock	rock	k1gInSc4	rock
<g/>
!	!	kIx.	!
</s>
<s>
live	live	k1gInSc1	live
1988	[number]	k4	1988
je	být	k5eAaImIp3nS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
mi	já	k3xPp1nSc3	já
lhali	lhát	k5eAaImAgMnP	lhát
<g/>
"	"	kIx"	"
Ostatně	ostatně	k6eAd1	ostatně
Oldřich	Oldřich	k1gMnSc1	Oldřich
Říha	Říha	k1gMnSc1	Říha
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
sahá	sahat	k5eAaImIp3nS	sahat
po	po	k7c6	po
tvorbě	tvorba	k1gFnSc6	tvorba
F.	F.	kA	F.
Gellnera	Gellner	k1gMnSc2	Gellner
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Pardubická	pardubický	k2eAgFnSc1d1	pardubická
punková	punkový	k2eAgFnSc1d1	punková
skupina	skupina	k1gFnSc1	skupina
Volant	volant	k1gInSc4	volant
zhudebnila	zhudebnit	k5eAaPmAgFnS	zhudebnit
báseň	báseň	k1gFnSc1	báseň
"	"	kIx"	"
<g/>
Ožeň	ohnat	k5eAaPmRp2nS	ohnat
se	se	k3xPyFc4	se
bratře	bratr	k1gMnSc5	bratr
<g/>
,	,	kIx,	,
ožeň	ohnat	k5eAaPmRp2nS	ohnat
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Ožeň	ohnat	k5eAaPmRp2nS	ohnat
se	se	k3xPyFc4	se
Honzo	Honza	k1gMnSc5	Honza
<g/>
,	,	kIx,	,
ožeň	ohnat	k5eAaPmRp2nS	ohnat
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
"	"	kIx"	"
<g/>
Neděle	neděle	k1gFnSc1	neděle
<g/>
"	"	kIx"	"
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Má	mít	k5eAaImIp3nS	mít
milá	milá	k1gFnSc1	milá
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Bláznění	bláznění	k1gNnSc1	bláznění
vjelo	vjet	k5eAaPmAgNnS	vjet
do	do	k7c2	do
párů	pár	k1gInPc2	pár
<g/>
"	"	kIx"	"
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Už	už	k9	už
tě	ty	k3xPp2nSc4	ty
nemám	mít	k5eNaImIp1nS	mít
rád	rád	k2eAgMnSc1d1	rád
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
bigbítová	bigbítový	k2eAgFnSc1d1	bigbítová
kapela	kapela	k1gFnSc1	kapela
Předkožky	předkožka	k1gFnSc2	předkožka
zhudebnila	zhudebnit	k5eAaPmAgFnS	zhudebnit
báseň	báseň	k1gFnSc1	báseň
"	"	kIx"	"
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
mi	já	k3xPp1nSc3	já
lhali	lhát	k5eAaImAgMnP	lhát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
táborská	táborský	k2eAgFnSc1d1	táborská
undergroundová	undergroundový	k2eAgFnSc1d1	undergroundová
skupina	skupina	k1gFnSc1	skupina
Vo	Vo	k?	Vo
100	[number]	k4	100
sex	sex	k1gInSc1	sex
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
repertoáru	repertoár	k1gInSc6	repertoár
zhudebněnou	zhudebněný	k2eAgFnSc4d1	zhudebněná
Gellnerovu	Gellnerův	k2eAgFnSc4d1	Gellnerova
báseň	báseň	k1gFnSc4	báseň
"	"	kIx"	"
<g/>
Od	od	k7c2	od
rána	ráno	k1gNnSc2	ráno
dřepěl	dřepět	k5eAaImAgMnS	dřepět
jsem	být	k5eAaImIp1nS	být
vesele	vesele	k6eAd1	vesele
<g/>
"	"	kIx"	"
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Po	po	k7c4	po
nás	my	k3xPp1nPc4	my
ať	ať	k9	ať
přijde	přijít	k5eAaPmIp3nS	přijít
potopa	potopa	k1gFnSc1	potopa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chebská	chebský	k2eAgFnSc1d1	Chebská
punk	punk	k1gInSc1	punk
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
rollová	rollový	k2eAgFnSc1d1	rollová
skupina	skupina	k1gFnSc1	skupina
Esgmeq	Esgmeq	k1gFnSc2	Esgmeq
zhudebnila	zhudebnit	k5eAaPmAgFnS	zhudebnit
báseň	báseň	k1gFnSc1	báseň
"	"	kIx"	"
<g/>
Ožeň	ohnat	k5eAaPmRp2nS	ohnat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
bratře	bratr	k1gMnSc5	bratr
<g/>
"	"	kIx"	"
z	z	k7c2	z
Radostí	radost	k1gFnPc2	radost
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
allskapones	allskaponesa	k1gFnPc2	allskaponesa
z	z	k7c2	z
Karviné	Karviná	k1gFnSc2	Karviná
zhudebnila	zhudebnit	k5eAaPmAgFnS	zhudebnit
celé	celý	k2eAgNnSc4d1	celé
pásmo	pásmo	k1gNnSc4	pásmo
Gellnerových	Gellnerův	k2eAgFnPc2d1	Gellnerova
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
udělala	udělat	k5eAaPmAgFnS	udělat
hudebně-literární	hudebněiterární	k2eAgInSc4d1	hudebně-literární
projekt	projekt	k1gInSc4	projekt
"	"	kIx"	"
<g/>
František	František	k1gMnSc1	František
Gellner	Gellner	k1gMnSc1	Gellner
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
oživuje	oživovat	k5eAaImIp3nS	oživovat
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
a	a	k8xC	a
zasazuje	zasazovat	k5eAaImIp3nS	zasazovat
jej	on	k3xPp3gNnSc4	on
do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
kontextu	kontext	k1gInSc2	kontext
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Gellnerovské	Gellnerovský	k2eAgFnSc2d1	Gellnerovský
<g/>
"	"	kIx"	"
skladby	skladba	k1gFnSc2	skladba
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
Vzdušné	vzdušný	k2eAgFnSc6d1	vzdušná
mé	můj	k3xOp1gMnPc4	můj
vidiny	vidina	k1gFnSc2	vidina
nádherná	nádherný	k2eAgNnPc1d1	nádherné
těla	tělo	k1gNnPc1	tělo
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
Hm	Hm	k?	Hm
<g/>
...	...	k?	...
zhudebnila	zhudebnit	k5eAaPmAgFnS	zhudebnit
Gellnerovu	Gellnerův	k2eAgFnSc4d1	Gellnerova
báseň	báseň	k1gFnSc4	báseň
"	"	kIx"	"
<g/>
Kuplet	kuplet	k1gInSc4	kuplet
o	o	k7c6	o
ženské	ženský	k2eAgFnSc6d1	ženská
emancipaci	emancipace	k1gFnSc6	emancipace
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Radosti	radost	k1gFnSc2	radost
života	život	k1gInSc2	život
VI	VI	kA	VI
(	(	kIx(	(
<g/>
A	a	k8xC	a
nový	nový	k2eAgInSc4d1	nový
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
hlásil	hlásit	k5eAaImAgMnS	hlásit
kuropěním	kuropění	k1gNnSc7	kuropění
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
desce	deska	k1gFnSc6	deska
Plán	plán	k1gInSc1	plán
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Punková	punkový	k2eAgFnSc1d1	punková
skupina	skupina	k1gFnSc1	skupina
Houba	houba	k1gFnSc1	houba
zhudebnila	zhudebnit	k5eAaPmAgFnS	zhudebnit
báseň	báseň	k1gFnSc4	báseň
"	"	kIx"	"
<g/>
Drobky	drobek	k1gInPc1	drobek
pod	pod	k7c4	pod
stůl	stůl	k1gInSc4	stůl
hází	házet	k5eAaImIp3nS	házet
nám	my	k3xPp1nPc3	my
osud	osud	k1gInSc1	osud
<g/>
"	"	kIx"	"
na	na	k7c6	na
albu	album	k1gNnSc6	album
Hnus	hnus	k1gInSc4	hnus
fialovej	fialovej	k?	fialovej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Punková	punkový	k2eAgFnSc1d1	punková
plzeňská	plzeňský	k2eAgFnSc1d1	Plzeňská
skupina	skupina	k1gFnSc1	skupina
Indiánská	indiánský	k2eAgFnSc1d1	indiánská
rezervace	rezervace	k1gFnSc1	rezervace
zhudebnila	zhudebnit	k5eAaPmAgFnS	zhudebnit
Gellnerovu	Gellnerův	k2eAgFnSc4d1	Gellnerova
báseň	báseň	k1gFnSc4	báseň
"	"	kIx"	"
<g/>
Ředitel	ředitel	k1gMnSc1	ředitel
<g/>
"	"	kIx"	"
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Radosti	radost	k1gFnSc2	radost
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
hudebně-recitační	hudebněecitační	k2eAgFnSc1d1	hudebně-recitační
skupina	skupina	k1gFnSc1	skupina
Sbor	sbor	k1gInSc1	sbor
brežanských	brežanský	k2eAgMnPc2d1	brežanský
kastrátů	kastrát	k1gMnPc2	kastrát
pravidelně	pravidelně	k6eAd1	pravidelně
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
Gellnerovy	Gellnerův	k2eAgFnPc4d1	Gellnerova
básně	báseň	k1gFnPc4	báseň
jako	jako	k8xC	jako
Radosti	radost	k1gFnPc4	radost
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Pomalu	pomalu	k6eAd1	pomalu
v	v	k7c4	v
revolver	revolver	k1gInSc4	revolver
se	se	k3xPyFc4	se
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
víra	víra	k1gFnSc1	víra
<g/>
,	,	kIx,	,
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
<g/>
,	,	kIx,	,
Ožeň	ohnat	k5eAaPmRp2nS	ohnat
se	se	k3xPyFc4	se
bratře	bratr	k1gMnSc5	bratr
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
Mňága	Mňága	k1gFnSc1	Mňága
a	a	k8xC	a
Žďorp	Žďorp	k1gInSc4	Žďorp
zhudebnila	zhudebnit	k5eAaPmAgFnS	zhudebnit
báseň	báseň	k1gFnSc1	báseň
"	"	kIx"	"
<g/>
Pod	pod	k7c7	pod
mrazivým	mrazivý	k2eAgNnSc7d1	mrazivé
nebem	nebe	k1gNnSc7	nebe
<g/>
"	"	kIx"	"
na	na	k7c6	na
albu	album	k1gNnSc6	album
Jen	jen	k9	jen
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgFnSc4d1	vlastní
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
skupina	skupina	k1gFnSc1	skupina
Hubertus	hubertus	k1gInSc4	hubertus
zhudebnila	zhudebnit	k5eAaPmAgFnS	zhudebnit
báseň	báseň	k1gFnSc1	báseň
"	"	kIx"	"
<g/>
Neděle	neděle	k1gFnSc1	neděle
<g/>
"	"	kIx"	"
na	na	k7c6	na
albu	album	k1gNnSc6	album
Sebrané	sebraný	k2eAgFnSc2d1	sebraná
kdysi	kdysi	k6eAd1	kdysi
</s>
</p>
<p>
<s>
zhudebňování	zhudebňování	k1gNnSc1	zhudebňování
textů	text	k1gInPc2	text
Františka	František	k1gMnSc4	František
Gellnera	Gellner	k1gMnSc4	Gellner
se	se	k3xPyFc4	se
systematicky	systematicky	k6eAd1	systematicky
a	a	k8xC	a
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
věnuje	věnovat	k5eAaPmIp3nS	věnovat
vlašimská	vlašimský	k2eAgFnSc1d1	Vlašimská
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
Nazdárci	Nazdárek	k1gMnPc1	Nazdárek
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Referecnce	Referecnka	k1gFnSc6	Referecnka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
GELLNER	GELLNER	kA	GELLNER
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Jiří	Jiří	k1gMnSc1	Jiří
Flaišman	Flaišman	k1gMnSc1	Flaišman
et	et	k?	et
<g/>
.	.	kIx.	.
al	ala	k1gFnPc2	ala
<g/>
..	..	k?	..
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
literaturu	literatura	k1gFnSc4	literatura
AV	AV	kA	AV
ČR	ČR	kA	ČR
:	:	kIx,	:
Akropolis	Akropolis	k1gFnSc1	Akropolis
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
2	[number]	k4	2
svazky	svazek	k1gInPc4	svazek
(	(	kIx(	(
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Kritická	kritický	k2eAgFnSc1d1	kritická
hybridní	hybridní	k2eAgFnSc1d1	hybridní
edice	edice	k1gFnSc1	edice
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85778	[number]	k4	85778
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7470	[number]	k4	7470
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
68	[number]	k4	68
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FORST	FORST	kA	FORST
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
A-	A-	k?	A-
<g/>
G.	G.	kA	G.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
900	[number]	k4	900
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
797	[number]	k4	797
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Fryš	Fryš	k1gMnSc1	Fryš
<g/>
:	:	kIx,	:
Dvanáct	dvanáct	k4xCc1	dvanáct
osudů	osud	k1gInPc2	osud
dvou	dva	k4xCgNnPc2	dva
staletí	staletí	k1gNnPc2	staletí
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
<g/>
,	,	kIx,	,
Příbram	Příbram	k1gFnSc1	Příbram
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
–	–	k?	–
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
osobností	osobnost	k1gFnPc2	osobnost
je	být	k5eAaImIp3nS	být
František	František	k1gMnSc1	František
Gellner	Gellner	k1gMnSc1	Gellner
</s>
</p>
<p>
<s>
GEJGUŠOVÁ	GEJGUŠOVÁ	kA	GEJGUŠOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Gellner	Gellner	k1gMnSc1	Gellner
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
Ostravské	ostravský	k2eAgFnSc2d1	Ostravská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
94	[number]	k4	94
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7368	[number]	k4	7368
<g/>
-	-	kIx~	-
<g/>
487	[number]	k4	487
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MACH	Mach	k1gMnSc1	Mach
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Kreslířské	kreslířský	k2eAgInPc1d1	kreslířský
počátky	počátek	k1gInPc1	počátek
Františka	František	k1gMnSc2	František
Gellnera	Gellner	k1gMnSc2	Gellner
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgFnPc1d1	literární
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
53	[number]	k4	53
<g/>
–	–	k?	–
<g/>
54	[number]	k4	54
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠTECH	ŠTECH	kA	ŠTECH
<g/>
,	,	kIx,	,
V.	V.	kA	V.
V.	V.	kA	V.
V	v	k7c6	v
zamlženém	zamlžený	k2eAgNnSc6d1	zamlžené
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
115	[number]	k4	115
<g/>
,	,	kIx,	,
117	[number]	k4	117
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Anarchističtí	anarchistický	k2eAgMnPc1d1	anarchistický
buřiči	buřič	k1gMnPc1	buřič
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
letech	let	k1gInPc6	let
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
František	František	k1gMnSc1	František
Gellner	Gellnra	k1gFnPc2	Gellnra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
František	František	k1gMnSc1	František
Gellner	Gellner	k1gMnSc1	Gellner
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
František	František	k1gMnSc1	František
Gellner	Gellner	k1gMnSc1	Gellner
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
abART	abART	k?	abART
–	–	k?	–
osoba	osoba	k1gFnSc1	osoba
<g/>
:	:	kIx,	:
<g/>
Gellner	Gellner	k1gMnSc1	Gellner
František	František	k1gMnSc1	František
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Gellner	Gellner	k1gInSc1	Gellner
–	–	k?	–
fanouškovský	fanouškovský	k2eAgInSc1d1	fanouškovský
web	web	k1gInSc1	web
</s>
</p>
