<p>
<s>
Koberce	koberec	k1gInPc1	koberec
jsou	být	k5eAaImIp3nP	být
textilie	textilie	k1gFnPc4	textilie
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
se	se	k3xPyFc4	se
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
podlahy	podlaha	k1gFnPc1	podlaha
nebo	nebo	k8xC	nebo
stěny	stěna	k1gFnPc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
formě	forma	k1gFnSc6	forma
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgInP	být
(	(	kIx(	(
<g/>
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
<g/>
)	)	kIx)	)
výrobky	výrobek	k1gInPc1	výrobek
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
k	k	k7c3	k
předem	předem	k6eAd1	předem
určeným	určený	k2eAgInPc3d1	určený
rozměrům	rozměr	k1gInPc3	rozměr
každého	každý	k3xTgInSc2	každý
kusu	kus	k1gInSc2	kus
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
vzorování	vzorování	k1gNnSc1	vzorování
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
konstrukce	konstrukce	k1gFnSc1	konstrukce
koberce	koberec	k1gInSc2	koberec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
také	také	k9	také
koberce	koberec	k1gInPc4	koberec
jako	jako	k8xC	jako
celoplošné	celoplošný	k2eAgFnSc2d1	celoplošná
podlahové	podlahový	k2eAgFnSc2d1	podlahová
textilie	textilie	k1gFnSc2	textilie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
libovolně	libovolně	k6eAd1	libovolně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
pás	pás	k1gInSc1	pás
v	v	k7c6	v
šířkách	šířka	k1gFnPc6	šířka
až	až	k6eAd1	až
do	do	k7c2	do
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stříhá	stříhat	k5eAaImIp3nS	stříhat
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
koberce	koberec	k1gInPc1	koberec
se	se	k3xPyFc4	se
zhotovovaly	zhotovovat	k5eAaImAgInP	zhotovovat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
před	před	k7c7	před
4-5	[number]	k4	4-5
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
nejstarší	starý	k2eAgInSc1d3	nejstarší
nález	nález	k1gInSc1	nález
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
Pazyryku	Pazyryk	k1gInSc2	Pazyryk
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
údolí	údolí	k1gNnPc2	údolí
Altaje	Altaj	k1gInSc2	Altaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kobercové	kobercový	k2eAgFnSc2d1	kobercová
příze	příz	k1gFnSc2	příz
==	==	k?	==
</s>
</p>
<p>
<s>
Koberce	koberec	k1gInSc2	koberec
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
téměř	téměř	k6eAd1	téměř
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
textilních	textilní	k2eAgNnPc2d1	textilní
vláken	vlákno	k1gNnPc2	vlákno
a	a	k8xC	a
přízí	příz	k1gFnPc2	příz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příze	příze	k1gFnSc1	příze
na	na	k7c4	na
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
tkané	tkaný	k2eAgInPc4d1	tkaný
koberce	koberec	k1gInPc4	koberec
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
juty	juta	k1gFnSc2	juta
nebo	nebo	k8xC	nebo
kokosu	kokos	k1gInSc2	kokos
(	(	kIx(	(
<g/>
pevnost	pevnost	k1gFnSc1	pevnost
v	v	k7c6	v
tahu	tah	k1gInSc6	tah
a	a	k8xC	a
v	v	k7c6	v
oděru	oděr	k1gInSc6	oděr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bavlněné	bavlněný	k2eAgFnPc1d1	bavlněná
a	a	k8xC	a
jutové	jutový	k2eAgFnPc1d1	jutová
příze	příz	k1gFnPc1	příz
jsou	být	k5eAaImIp3nP	být
nejvhodnější	vhodný	k2eAgInSc4d3	nejvhodnější
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
podkladové	podkladový	k2eAgFnPc4d1	podkladová
kobercové	kobercový	k2eAgFnPc4d1	kobercová
tkaniny	tkanina	k1gFnPc4	tkanina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlna	vlna	k1gFnSc1	vlna
a	a	k8xC	a
směsi	směs	k1gFnPc1	směs
s	s	k7c7	s
vlnou	vlna	k1gFnSc7	vlna
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
na	na	k7c4	na
smyčkové	smyčkový	k2eAgInPc4d1	smyčkový
a	a	k8xC	a
vlasové	vlasový	k2eAgInPc4d1	vlasový
koberce	koberec	k1gInPc4	koberec
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
pružnost	pružnost	k1gFnSc4	pružnost
a	a	k8xC	a
rychlé	rychlý	k2eAgNnSc4d1	rychlé
zotavení	zotavení	k1gNnSc4	zotavení
po	po	k7c6	po
působení	působení	k1gNnSc6	působení
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Skoro	skoro	k6eAd1	skoro
všechny	všechen	k3xTgInPc4	všechen
ručně	ručně	k6eAd1	ručně
vázané	vázaný	k2eAgInPc4d1	vázaný
koberce	koberec	k1gInPc4	koberec
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
mykaných	mykaný	k2eAgFnPc2d1	mykaná
vlněných	vlněný	k2eAgFnPc2d1	vlněná
přízí	příz	k1gFnPc2	příz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
přírodního	přírodní	k2eAgNnSc2d1	přírodní
hedvábí	hedvábí	k1gNnSc2	hedvábí
se	se	k3xPyFc4	se
vážou	vázat	k5eAaImIp3nP	vázat
velmi	velmi	k6eAd1	velmi
jemné	jemný	k2eAgInPc4d1	jemný
gobelíny	gobelín	k1gInPc4	gobelín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
koberce	koberec	k1gInPc1	koberec
pro	pro	k7c4	pro
domácnost	domácnost	k1gFnSc4	domácnost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
veřejné	veřejný	k2eAgFnPc4d1	veřejná
budovy	budova	k1gFnPc4	budova
a	a	k8xC	a
pro	pro	k7c4	pro
venkovní	venkovní	k2eAgNnSc4d1	venkovní
použití	použití	k1gNnSc4	použití
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
z	z	k7c2	z
umělých	umělý	k2eAgNnPc2d1	umělé
vláken	vlákno	k1gNnPc2	vlákno
(	(	kIx(	(
<g/>
PA	Pa	kA	Pa
<g/>
,	,	kIx,	,
PP	PP	kA	PP
<g/>
,	,	kIx,	,
PAC	pac	k1gFnSc1	pac
<g/>
,	,	kIx,	,
VI	VI	kA	VI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
buďto	buďto	k8xC	buďto
staplové	staplový	k2eAgFnPc1d1	staplová
mykané	mykaný	k2eAgFnPc1d1	mykaná
příze	příz	k1gFnPc1	příz
nebo	nebo	k8xC	nebo
tvarované	tvarovaný	k2eAgInPc1d1	tvarovaný
filamenty	filament	k1gInPc1	filament
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
USA	USA	kA	USA
sestávají	sestávat	k5eAaImIp3nP	sestávat
koberce	koberec	k1gInPc1	koberec
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
%	%	kIx~	%
z	z	k7c2	z
polyamidů	polyamid	k1gInPc2	polyamid
<g/>
,	,	kIx,	,
30	[number]	k4	30
%	%	kIx~	%
z	z	k7c2	z
polypropylenových	polypropylenový	k2eAgInPc2d1	polypropylenový
filamentů	filament	k1gInPc2	filament
a	a	k8xC	a
jen	jen	k9	jen
asi	asi	k9	asi
1	[number]	k4	1
%	%	kIx~	%
z	z	k7c2	z
vlněných	vlněný	k2eAgNnPc2d1	vlněné
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Staplové	Staplový	k2eAgFnPc1d1	Staplový
příze	příz	k1gFnPc1	příz
jsou	být	k5eAaImIp3nP	být
výhodnější	výhodný	k2eAgFnPc1d2	výhodnější
pro	pro	k7c4	pro
kobercové	kobercový	k2eAgInPc4d1	kobercový
vzory	vzor	k1gInPc4	vzor
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
hedvábí	hedvábí	k1gNnSc2	hedvábí
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
vyloučeno	vyloučen	k2eAgNnSc1d1	vyloučeno
žmolkování	žmolkování	k1gNnSc1	žmolkování
hotového	hotový	k2eAgNnSc2d1	hotové
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
na	na	k7c4	na
staplové	staplový	k2eAgFnPc4d1	staplová
příze	příz	k1gFnPc4	příz
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
barví	barvit	k5eAaImIp3nP	barvit
ve	v	k7c6	v
vločce	vločka	k1gFnSc6	vločka
nebo	nebo	k8xC	nebo
během	během	k7c2	během
přádního	přádní	k2eAgInSc2d1	přádní
procesu	proces	k1gInSc2	proces
(	(	kIx(	(
<g/>
vlna	vlna	k1gFnSc1	vlna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umělá	umělý	k2eAgNnPc1d1	umělé
vlákna	vlákno	k1gNnPc1	vlákno
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
barví	barvit	k5eAaImIp3nS	barvit
ve	v	k7c6	v
hmotě	hmota	k1gFnSc6	hmota
(	(	kIx(	(
<g/>
barva	barva	k1gFnSc1	barva
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
suroviny	surovina	k1gFnSc2	surovina
před	před	k7c7	před
zvlákňováním	zvlákňování	k1gNnSc7	zvlákňování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákrut	zákrut	k1gInSc1	zákrut
kobercových	kobercový	k2eAgFnPc2d1	kobercová
přízí	příz	k1gFnPc2	příz
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
fixuje	fixovat	k5eAaImIp3nS	fixovat
pařením	paření	k1gNnSc7	paření
(	(	kIx(	(
<g/>
heatsetting	heatsetting	k1gInSc1	heatsetting
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
příze	příze	k1gFnSc1	příze
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
nesmyčkují	smyčkovat	k5eNaImIp3nP	smyčkovat
a	a	k8xC	a
zejména	zejména	k9	zejména
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
konce	konec	k1gInSc2	konec
vlasových	vlasový	k2eAgFnPc2d1	vlasová
nití	nit	k1gFnPc2	nit
na	na	k7c6	na
kobercích	koberec	k1gInPc6	koberec
se	se	k3xPyFc4	se
nerozevírají	rozevírat	k5eNaImIp3nP	rozevírat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
užitná	užitný	k2eAgFnSc1d1	užitná
hodnota	hodnota	k1gFnSc1	hodnota
podlahové	podlahový	k2eAgFnSc2d1	podlahová
krytiny	krytina	k1gFnSc2	krytina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
dalším	další	k2eAgNnSc7d1	další
zpracováním	zpracování	k1gNnSc7	zpracování
se	s	k7c7	s
2-6	[number]	k4	2-6
(	(	kIx(	(
<g/>
staplových	staplův	k2eAgFnPc2d1	staplův
i	i	k8xC	i
hedvábných	hedvábný	k2eAgFnPc2d1	hedvábná
<g/>
)	)	kIx)	)
nití	nit	k1gFnPc2	nit
druží	družit	k5eAaImIp3nS	družit
a	a	k8xC	a
seskává	seskávat	k5eAaImIp3nS	seskávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
koberců	koberec	k1gInPc2	koberec
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Podle	podle	k7c2	podle
výrobních	výrobní	k2eAgFnPc2d1	výrobní
technik	technika	k1gFnPc2	technika
se	se	k3xPyFc4	se
koberce	koberec	k1gInPc1	koberec
a	a	k8xC	a
podlahové	podlahový	k2eAgFnPc1d1	podlahová
krytiny	krytina	k1gFnPc1	krytina
často	často	k6eAd1	často
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
Vázané	vázaný	k2eAgFnSc2d1	vázaná
<g/>
,	,	kIx,	,
tkané	tkaný	k2eAgFnSc2d1	tkaná
<g/>
,	,	kIx,	,
všívané	všívaný	k2eAgFnSc2d1	všívaná
<g/>
,	,	kIx,	,
vpichované	vpichovaný	k2eAgFnSc2d1	vpichovaná
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
pojené	pojený	k2eAgFnPc1d1	pojená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
každé	každý	k3xTgFnSc2	každý
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nS	patřit
několik	několik	k4yIc1	několik
výrobních	výrobní	k2eAgInPc2d1	výrobní
postupů	postup	k1gInPc2	postup
a	a	k8xC	a
vzorovacích	vzorovací	k2eAgFnPc2d1	vzorovací
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
k	k	k7c3	k
běžným	běžný	k2eAgInPc3d1	běžný
pojmům	pojem	k1gInPc3	pojem
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Ručně	ručně	k6eAd1	ručně
vázané	vázaný	k2eAgInPc4d1	vázaný
koberce	koberec	k1gInPc4	koberec
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
osnovní	osnovní	k2eAgFnPc4d1	osnovní
nitě	nit	k1gFnPc4	nit
se	se	k3xPyFc4	se
navazují	navazovat	k5eAaImIp3nP	navazovat
uzlíky	uzlík	k1gInPc1	uzlík
různého	různý	k2eAgInSc2d1	různý
tvaru	tvar	k1gInSc2	tvar
(	(	kIx(	(
<g/>
perský	perský	k2eAgMnSc1d1	perský
<g/>
,	,	kIx,	,
smyrenský	smyrenský	k2eAgMnSc1d1	smyrenský
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
předem	předem	k6eAd1	předem
určeného	určený	k2eAgInSc2d1	určený
barevného	barevný	k2eAgInSc2d1	barevný
vzoru	vzor	k1gInSc2	vzor
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
řada	řada	k1gFnSc1	řada
uzlíků	uzlík	k1gInPc2	uzlík
se	se	k3xPyFc4	se
protkává	protkávat	k5eAaImIp3nS	protkávat
dvěma	dva	k4xCgInPc7	dva
útky	útek	k1gInPc7	útek
v	v	k7c6	v
plátnové	plátnový	k2eAgFnSc6d1	plátnová
vazbě	vazba	k1gFnSc6	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Vlněné	vlněný	k2eAgInPc1d1	vlněný
koberce	koberec	k1gInPc1	koberec
mívají	mívat	k5eAaImIp3nP	mívat
8	[number]	k4	8
uzlů	uzel	k1gInPc2	uzel
<g/>
,	,	kIx,	,
hedvábné	hedvábný	k2eAgFnSc2d1	hedvábná
60-100	[number]	k4	60-100
na	na	k7c4	na
cm2	cm2	k4	cm2
<g/>
.	.	kIx.	.
</s>
<s>
Špičky	špička	k1gFnPc1	špička
uzlů	uzel	k1gInPc2	uzel
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
postřihují	postřihovat	k5eAaImIp3nP	postřihovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
asijských	asijský	k2eAgFnPc6d1	asijská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středoasijských	středoasijský	k2eAgFnPc6d1	středoasijská
republikách	republika	k1gFnPc6	republika
a	a	k8xC	a
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ručně	ručně	k6eAd1	ručně
vázané	vázaný	k2eAgInPc4d1	vázaný
koberce	koberec	k1gInPc4	koberec
se	s	k7c7	s
specifickým	specifický	k2eAgNnSc7d1	specifické
vzorováním	vzorování	k1gNnSc7	vzorování
a	a	k8xC	a
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
výrobní	výrobní	k2eAgFnSc7d1	výrobní
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
řada	řada	k1gFnSc1	řada
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc4	jejichž
původ	původ	k1gInSc4	původ
a	a	k8xC	a
hodnotu	hodnota	k1gFnSc4	hodnota
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kusů	kus	k1gInPc2	kus
umí	umět	k5eAaImIp3nS	umět
posoudit	posoudit	k5eAaPmF	posoudit
jen	jen	k9	jen
úzce	úzko	k6eAd1	úzko
specializovaní	specializovaný	k2eAgMnPc1d1	specializovaný
odborníci	odborník	k1gMnPc1	odborník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
druhy	druh	k1gMnPc4	druh
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Kelim	kelim	k1gInSc1	kelim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
poměrně	poměrně	k6eAd1	poměrně
dost	dost	k6eAd1	dost
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
amatérské	amatérský	k2eAgNnSc1d1	amatérské
zhotovování	zhotovování	k1gNnSc1	zhotovování
menších	malý	k2eAgInPc2d2	menší
ručně	ručně	k6eAd1	ručně
vázaných	vázaný	k2eAgInPc2d1	vázaný
koberců	koberec	k1gInPc2	koberec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
účelu	účel	k1gInSc3	účel
se	se	k3xPyFc4	se
prodávaly	prodávat	k5eAaImAgFnP	prodávat
soupravy	souprava	k1gFnPc1	souprava
přízí	příz	k1gFnPc2	příz
(	(	kIx(	(
<g/>
nastříhaných	nastříhaný	k2eAgFnPc2d1	nastříhaná
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
cca	cca	kA	cca
5	[number]	k4	5
cm	cm	kA	cm
<g/>
)	)	kIx)	)
s	s	k7c7	s
podkladovou	podkladový	k2eAgFnSc7d1	podkladová
tkaninou	tkanina	k1gFnSc7	tkanina
a	a	k8xC	a
patřičným	patřičný	k2eAgInSc7d1	patřičný
návodem	návod	k1gInSc7	návod
k	k	k7c3	k
určitému	určitý	k2eAgInSc3d1	určitý
vzoru	vzor	k1gInSc3	vzor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
vpravo	vpravo	k6eAd1	vpravo
je	být	k5eAaImIp3nS	být
příklad	příklad	k1gInSc4	příklad
amatérsky	amatérsky	k6eAd1	amatérsky
vázané	vázaný	k2eAgFnPc1d1	vázaná
předložky	předložka	k1gFnPc1	předložka
s	s	k7c7	s
ukázkou	ukázka	k1gFnSc7	ukázka
vzorků	vzorek	k1gInPc2	vzorek
přízí	příz	k1gFnPc2	příz
<g/>
,	,	kIx,	,
jehly	jehla	k1gFnSc2	jehla
a	a	k8xC	a
podkladové	podkladový	k2eAgFnSc2d1	podkladová
tkaniny	tkanina	k1gFnSc2	tkanina
na	na	k7c6	na
rubní	rubní	k2eAgFnSc6d1	rubní
straně	strana	k1gFnSc6	strana
koberce	koberec	k1gInSc2	koberec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Údajně	údajně	k6eAd1	údajně
největší	veliký	k2eAgInSc1d3	veliký
ručně	ručně	k6eAd1	ručně
vázaný	vázaný	k2eAgInSc1d1	vázaný
koberec	koberec	k1gInSc1	koberec
na	na	k7c6	na
světě	svět	k1gInSc6	svět
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
a	a	k8xC	a
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
podlahu	podlaha	k1gFnSc4	podlaha
Velké	velký	k2eAgFnSc2d1	velká
mešity	mešita	k1gFnSc2	mešita
v	v	k7c6	v
Maskatu	Maskat	k1gInSc6	Maskat
(	(	kIx(	(
<g/>
Omán	Omán	k1gInSc1	Omán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
4	[number]	k4	4
343	[number]	k4	343
m2	m2	k4	m2
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
22	[number]	k4	22
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Příze	příze	k1gFnSc1	příze
z	z	k7c2	z
čisté	čistý	k2eAgFnSc2d1	čistá
vlny	vlna	k1gFnSc2	vlna
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
vázána	vázán	k2eAgFnSc1d1	vázána
do	do	k7c2	do
základní	základní	k2eAgFnSc2d1	základní
bavlněné	bavlněný	k2eAgFnSc2d1	bavlněná
tkaniny	tkanina	k1gFnSc2	tkanina
</s>
</p>
<p>
<s>
===	===	k?	===
Strojně	strojně	k6eAd1	strojně
vázané	vázaný	k2eAgInPc4d1	vázaný
koberce	koberec	k1gInPc4	koberec
===	===	k?	===
</s>
</p>
<p>
<s>
Tkací	tkací	k2eAgInPc1d1	tkací
stroje	stroj	k1gInPc1	stroj
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
ruční	ruční	k2eAgNnSc4d1	ruční
vázání	vázání	k1gNnSc4	vázání
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgMnPc4d3	nejznámější
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
gripper	gripper	k1gInSc4	gripper
axminsterské	axminsterský	k2eAgInPc4d1	axminsterský
koberce	koberec	k1gInPc4	koberec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postup	postup	k1gInSc1	postup
výroby	výroba	k1gFnSc2	výroba
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Skřipec	skřipec	k1gInSc1	skřipec
vytáhne	vytáhnout	k5eAaPmIp3nS	vytáhnout
z	z	k7c2	z
podavače	podavač	k1gInSc2	podavač
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
délku	délka	k1gFnSc4	délka
vlasové	vlasový	k2eAgFnSc2d1	vlasová
příze	příz	k1gFnSc2	příz
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
odřízne	odříznout	k5eAaPmIp3nS	odříznout
<g/>
.	.	kIx.	.
</s>
<s>
Skřipec	skřipec	k1gInSc1	skřipec
zavede	zavést	k5eAaPmIp3nS	zavést
vlasovou	vlasový	k2eAgFnSc4d1	vlasová
osnovu	osnova	k1gFnSc4	osnova
dospod	dospod	k6eAd1	dospod
základní	základní	k2eAgFnSc2d1	základní
tkaniny	tkanina	k1gFnSc2	tkanina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
upevní	upevnit	k5eAaPmIp3nS	upevnit
dvěma	dva	k4xCgInPc7	dva
útky	útek	k1gInPc7	útek
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
protáhne	protáhnout	k5eAaPmIp3nS	protáhnout
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Vlas	vlas	k1gInSc1	vlas
se	se	k3xPyFc4	se
upevní	upevnit	k5eAaPmIp3nS	upevnit
dalšími	další	k2eAgInPc7d1	další
útky	útek	k1gInPc7	útek
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
proces	proces	k1gInSc4	proces
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
tkané	tkaný	k2eAgInPc4d1	tkaný
koberce	koberec	k1gInPc4	koberec
===	===	k?	===
</s>
</p>
<p>
<s>
Tkají	tkát	k5eAaImIp3nP	tkát
se	se	k3xPyFc4	se
v	v	k7c6	v
rypsové	rypsový	k2eAgFnSc6d1	rypsový
nebo	nebo	k8xC	nebo
keprové	keprový	k2eAgFnSc6d1	keprová
vazbě	vazba	k1gFnSc6	vazba
a	a	k8xC	a
označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
jsou	být	k5eAaImIp3nP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jutové	jutový	k2eAgInPc1d1	jutový
<g/>
,	,	kIx,	,
sisalové	sisalový	k2eAgInPc1d1	sisalový
<g/>
,	,	kIx,	,
kokosové	kokosový	k2eAgInPc1d1	kokosový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčastější	častý	k2eAgNnSc1d3	nejčastější
použití	použití	k1gNnSc1	použití
<g/>
:	:	kIx,	:
jako	jako	k8xS	jako
běhouny	běhoun	k1gInPc1	běhoun
</s>
</p>
<p>
<s>
Tkaní	tkaní	k1gNnSc4	tkaní
koberců	koberec	k1gInPc2	koberec
prutovou	prutový	k2eAgFnSc7d1	prutová
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
tkaní	tkaní	k1gNnSc1	tkaní
dvojplyšů	dvojplyš	k1gInPc2	dvojplyš
a	a	k8xC	a
gobelínů	gobelín	k1gInPc2	gobelín
je	být	k5eAaImIp3nS	být
složité	složitý	k2eAgNnSc1d1	složité
a	a	k8xC	a
nákladné	nákladný	k2eAgNnSc1d1	nákladné
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
jen	jen	k9	jen
v	v	k7c6	v
omezeném	omezený	k2eAgNnSc6d1	omezené
množství	množství	k1gNnSc6	množství
a	a	k8xC	a
pro	pro	k7c4	pro
speciální	speciální	k2eAgInPc4d1	speciální
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Všívané	všívaný	k2eAgFnSc2d1	všívaná
podlahové	podlahový	k2eAgFnSc2d1	podlahová
krytiny	krytina	k1gFnSc2	krytina
===	===	k?	===
</s>
</p>
<p>
<s>
Všívané	všívaný	k2eAgInPc1d1	všívaný
koberce	koberec	k1gInPc1	koberec
jsou	být	k5eAaImIp3nP	být
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
druhem	druh	k1gInSc7	druh
podlahových	podlahový	k2eAgFnPc2d1	podlahová
krytin	krytina	k1gFnPc2	krytina
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Podle	podle	k7c2	podle
amerických	americký	k2eAgInPc2d1	americký
pramenů	pramen	k1gInPc2	pramen
až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
koberců	koberec	k1gInPc2	koberec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Technologie	technologie	k1gFnSc1	technologie
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
tufting	tufting	k1gInSc1	tufting
<g/>
.	.	kIx.	.
</s>
<s>
Vlas	vlas	k1gInSc1	vlas
se	se	k3xPyFc4	se
všívá	všívat	k5eAaImIp3nS	všívat
do	do	k7c2	do
základní	základní	k2eAgFnSc2d1	základní
tkaniny	tkanina	k1gFnSc2	tkanina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zhotovena	zhotovit	k5eAaPmNgFnS	zhotovit
z	z	k7c2	z
juty	juta	k1gFnSc2	juta
<g/>
,	,	kIx,	,
z	z	k7c2	z
polypropylénových	polypropylénový	k2eAgInPc2d1	polypropylénový
pásků	pásek	k1gInPc2	pásek
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Napnutá	napnutý	k2eAgFnSc1d1	napnutá
tkanina	tkanina	k1gFnSc1	tkanina
se	se	k3xPyFc4	se
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
všívacímu	všívací	k2eAgNnSc3d1	všívací
zařízení	zařízení	k1gNnSc3	zařízení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jehlovém	jehlový	k2eAgInSc6d1	jehlový
rámu	rám	k1gInSc6	rám
uloženy	uložen	k2eAgFnPc1d1	uložena
jehly	jehla	k1gFnPc1	jehla
s	s	k7c7	s
navlečenými	navlečený	k2eAgFnPc7d1	navlečená
vlasovými	vlasový	k2eAgFnPc7d1	vlasová
nitěmi	nit	k1gFnPc7	nit
<g/>
.	.	kIx.	.
</s>
<s>
Jehlový	jehlový	k2eAgInSc1d1	jehlový
systém	systém	k1gInSc1	systém
propichuje	propichovat	k5eAaImIp3nS	propichovat
tlakem	tlak	k1gInSc7	tlak
jehly	jehla	k1gFnSc2	jehla
nosnou	nosný	k2eAgFnSc4d1	nosná
textilii	textilie	k1gFnSc4	textilie
na	na	k7c6	na
opěrném	opěrný	k2eAgInSc6d1	opěrný
stole	stol	k1gInSc6	stol
<g/>
.	.	kIx.	.
</s>
<s>
Vlasové	vlasový	k2eAgFnPc1d1	vlasová
nitě	nit	k1gFnPc1	nit
jsou	být	k5eAaImIp3nP	být
zachyceny	zachytit	k5eAaPmNgFnP	zachytit
háčkem	háček	k1gInSc7	háček
pod	pod	k7c7	pod
jehlou	jehla	k1gFnSc7	jehla
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
se	se	k3xPyFc4	se
tak	tak	k9	tak
smyčka	smyčka	k1gFnSc1	smyčka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vytažení	vytažení	k1gNnSc6	vytažení
jehly	jehla	k1gFnSc2	jehla
z	z	k7c2	z
nosné	nosný	k2eAgFnSc2d1	nosná
textilie	textilie	k1gFnSc2	textilie
se	se	k3xPyFc4	se
textilie	textilie	k1gFnSc1	textilie
posune	posunout	k5eAaPmIp3nS	posunout
o	o	k7c4	o
určitou	určitý	k2eAgFnSc4d1	určitá
délku	délka	k1gFnSc4	délka
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
proces	proces	k1gInSc4	proces
všívání	všívání	k1gNnSc2	všívání
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
výroby	výroba	k1gFnSc2	výroba
koberců	koberec	k1gInPc2	koberec
s	s	k7c7	s
řezaným	řezaný	k2eAgInSc7d1	řezaný
vlasem	vlas	k1gInSc7	vlas
jsou	být	k5eAaImIp3nP	být
háčky	háček	k1gInPc4	háček
pod	pod	k7c7	pod
jehlami	jehla	k1gFnPc7	jehla
opatřeny	opatřen	k2eAgMnPc4d1	opatřen
řezným	řezný	k2eAgInSc7d1	řezný
nožem	nůž	k1gInSc7	nůž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všívané	všívaný	k2eAgInPc1d1	všívaný
koberce	koberec	k1gInPc1	koberec
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
jemné	jemný	k2eAgFnPc1d1	jemná
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
tkanina	tkanina	k1gFnSc1	tkanina
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
všíván	všíván	k2eAgInSc1d1	všíván
vlas	vlas	k1gInSc1	vlas
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
prostupná	prostupný	k2eAgNnPc1d1	prostupné
pro	pro	k7c4	pro
jehlu	jehla	k1gFnSc4	jehla
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
k	k	k7c3	k
rubu	rub	k1gInSc3	rub
koberce	koberec	k1gInSc2	koberec
přidávají	přidávat	k5eAaImIp3nP	přidávat
sekundární	sekundární	k2eAgFnPc1d1	sekundární
vrstvy	vrstva	k1gFnPc1	vrstva
ze	z	k7c2	z
tkaných	tkaný	k2eAgFnPc2d1	tkaná
nebo	nebo	k8xC	nebo
netkaných	tkaný	k2eNgFnPc2d1	netkaná
textilií	textilie	k1gFnPc2	textilie
nebo	nebo	k8xC	nebo
zátěrů	zátěr	k1gInPc2	zátěr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
zafixování	zafixování	k1gNnSc3	zafixování
smyček	smyčka	k1gFnPc2	smyčka
a	a	k8xC	a
k	k	k7c3	k
vyztužení	vyztužení	k1gNnSc3	vyztužení
koberce	koberec	k1gInSc2	koberec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vpichované	vpichovaný	k2eAgInPc4d1	vpichovaný
koberce	koberec	k1gInPc4	koberec
===	===	k?	===
</s>
</p>
<p>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejlevnějším	levný	k2eAgFnPc3d3	nejlevnější
podlahovým	podlahový	k2eAgFnPc3d1	podlahová
textilním	textilní	k2eAgFnPc3d1	textilní
krytinám	krytina	k1gFnPc3	krytina
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
jednou	jeden	k4xCgFnSc7	jeden
operací	operace	k1gFnSc7	operace
na	na	k7c6	na
jehelném	jehelné	k1gNnSc6	jehelné
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
výroby	výroba	k1gFnSc2	výroba
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
vzájemně	vzájemně	k6eAd1	vzájemně
propletených	propletený	k2eAgNnPc6d1	propletené
vláknech	vlákno	k1gNnPc6	vlákno
rouna	rouno	k1gNnSc2	rouno
pomocí	pomocí	k7c2	pomocí
speciálních	speciální	k2eAgFnPc2d1	speciální
vpichovacích	vpichovací	k2eAgFnPc2d1	vpichovací
jehel	jehla	k1gFnPc2	jehla
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
spodního	spodní	k2eAgNnSc2d1	spodní
podkladového	podkladový	k2eAgNnSc2d1	podkladové
rouna	rouno	k1gNnSc2	rouno
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgInPc1d1	používán
odpadové	odpadový	k2eAgInPc1d1	odpadový
textilní	textilní	k2eAgInPc1d1	textilní
materiály	materiál	k1gInPc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgNnSc1d1	vrchní
rouno	rouno	k1gNnSc1	rouno
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnPc1	jehož
vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
vpichována	vpichován	k2eAgNnPc1d1	vpichován
do	do	k7c2	do
podkladového	podkladový	k2eAgNnSc2d1	podkladové
rouna	rouno	k1gNnSc2	rouno
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
kvalitnějších	kvalitní	k2eAgInPc2d2	kvalitnější
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rubové	rubový	k2eAgFnSc2d1	rubová
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
zatírají	zatírat	k5eAaImIp3nP	zatírat
a	a	k8xC	a
tuží	tužit	k5eAaImIp3nP	tužit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Popsaným	popsaný	k2eAgInSc7d1	popsaný
způsobem	způsob	k1gInSc7	způsob
vzniká	vznikat	k5eAaImIp3nS	vznikat
textilie	textilie	k1gFnSc1	textilie
s	s	k7c7	s
hladkým	hladký	k2eAgInSc7d1	hladký
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
tzv.	tzv.	kA	tzv.
vidličkových	vidličkový	k2eAgFnPc2d1	Vidličková
jehel	jehla	k1gFnPc2	jehla
se	se	k3xPyFc4	se
na	na	k7c6	na
stejných	stejný	k2eAgInPc6d1	stejný
strojích	stroj	k1gInPc6	stroj
mohou	moct	k5eAaImIp3nP	moct
vyrábět	vyrábět	k5eAaImF	vyrábět
smyčkové	smyčkový	k2eAgFnPc4d1	Smyčková
podlahové	podlahový	k2eAgFnPc4d1	podlahová
krytiny	krytina	k1gFnPc4	krytina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chemicky	chemicky	k6eAd1	chemicky
pojené	pojený	k2eAgFnSc2d1	pojená
podlahové	podlahový	k2eAgFnSc2d1	podlahová
textilie	textilie	k1gFnSc2	textilie
===	===	k?	===
</s>
</p>
<p>
<s>
Lepené	lepený	k2eAgFnPc4d1	lepená
podlahové	podlahový	k2eAgFnPc4d1	podlahová
krytiny	krytina	k1gFnPc4	krytina
vznikají	vznikat	k5eAaImIp3nP	vznikat
lepením	lepení	k1gNnSc7	lepení
různých	různý	k2eAgInPc2d1	různý
vlákenných	vlákenný	k2eAgInPc2d1	vlákenný
materiálů	materiál	k1gInPc2	materiál
na	na	k7c4	na
podkladovou	podkladový	k2eAgFnSc4d1	podkladová
textilii	textilie	k1gFnSc4	textilie
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
pojené	pojený	k2eAgInPc1d1	pojený
koberce	koberec	k1gInPc1	koberec
s	s	k7c7	s
řezaným	řezaný	k2eAgInSc7d1	řezaný
vlasem	vlas	k1gInSc7	vlas
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
dvojplyš	dvojplyš	k1gInSc1	dvojplyš
<g/>
,	,	kIx,	,
vlasová	vlasový	k2eAgFnSc1d1	vlasová
osnova	osnova	k1gFnSc1	osnova
se	se	k3xPyFc4	se
však	však	k9	však
nezatkává	zatkávat	k5eNaImIp3nS	zatkávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lepí	lepit	k5eAaImIp3nS	lepit
na	na	k7c4	na
podkladovou	podkladový	k2eAgFnSc4d1	podkladová
textilii	textilie	k1gFnSc4	textilie
a	a	k8xC	a
po	po	k7c6	po
vytvrzení	vytvrzení	k1gNnSc6	vytvrzení
pojiva	pojivo	k1gNnSc2	pojivo
se	se	k3xPyFc4	se
řeže	řezat	k5eAaImIp3nS	řezat
–	–	k?	–
vzniká	vznikat	k5eAaImIp3nS	vznikat
lepený	lepený	k2eAgInSc4d1	lepený
koberec	koberec	k1gInSc4	koberec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
účelem	účel	k1gInSc7	účel
koberce	koberec	k1gInSc2	koberec
je	být	k5eAaImIp3nS	být
tepelně	tepelně	k6eAd1	tepelně
izolovat	izolovat	k5eAaBmF	izolovat
podlahu	podlaha	k1gFnSc4	podlaha
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
snížit	snížit	k5eAaPmF	snížit
tepelné	tepelný	k2eAgFnPc4d1	tepelná
ztráty	ztráta	k1gFnPc4	ztráta
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
změkčení	změkčení	k1gNnSc1	změkčení
podlahy	podlaha	k1gFnSc2	podlaha
a	a	k8xC	a
ozdobná	ozdobný	k2eAgFnSc1d1	ozdobná
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohádkách	pohádka	k1gFnPc6	pohádka
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
arabských	arabský	k2eAgInPc2d1	arabský
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
létání	létání	k1gNnSc3	létání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
domácnost	domácnost	k1gFnSc4	domácnost
nachází	nacházet	k5eAaImIp3nS	nacházet
podlahové	podlahový	k2eAgFnPc4d1	podlahová
krytiny	krytina	k1gFnPc4	krytina
široké	široký	k2eAgNnSc4d1	široké
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
speciálních	speciální	k2eAgNnPc6d1	speciální
provedeních	provedení	k1gNnPc6	provedení
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
</s>
</p>
<p>
<s>
ve	v	k7c6	v
veřejných	veřejný	k2eAgFnPc6d1	veřejná
prostorách	prostora	k1gFnPc6	prostora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
vysoká	vysoký	k2eAgFnSc1d1	vysoká
zátěž	zátěž	k1gFnSc1	zátěž
zejména	zejména	k9	zejména
u	u	k7c2	u
schodových	schodový	k2eAgFnPc2d1	Schodová
kobercův	kobercův	k2eAgInSc1d1	kobercův
čisticích	čisticí	k2eAgFnPc6d1	čisticí
zónách	zóna	k1gFnPc6	zóna
u	u	k7c2	u
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
veřejných	veřejný	k2eAgFnPc2d1	veřejná
prostorjako	prostorjako	k6eAd1	prostorjako
umělý	umělý	k2eAgInSc4d1	umělý
trávník	trávník	k1gInSc4	trávník
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
variant	varianta	k1gFnPc2	varianta
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
použití	použití	k1gNnSc2	použití
<g/>
,	,	kIx,	,
druhu	druh	k1gInSc2	druh
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yIgInSc4	který
je	být	k5eAaImIp3nS	být
určen	určen	k2eAgInSc1d1	určen
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
ve	v	k7c6	v
druzích	druh	k1gInPc6	druh
použitých	použitý	k2eAgFnPc2d1	použitá
textilních	textilní	k2eAgFnPc2d1	textilní
vláken	vlákna	k1gFnPc2	vlákna
<g/>
,	,	kIx,	,
délce	délka	k1gFnSc6	délka
a	a	k8xC	a
hustotě	hustota	k1gFnSc6	hustota
smyček	smyčka	k1gFnPc2	smyčka
a	a	k8xC	a
vlasu	vlas	k1gInSc2	vlas
<g/>
,	,	kIx,	,
druhu	druh	k1gInSc2	druh
písku	písek	k1gInSc2	písek
nebo	nebo	k8xC	nebo
granulátu	granulát	k1gInSc2	granulát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
sype	sypat	k5eAaImIp3nS	sypat
mezi	mezi	k7c4	mezi
smyčky	smyčka	k1gFnPc4	smyčka
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
Na	na	k7c6	na
nákresu	nákres	k1gInSc6	nákres
vpravo	vpravo	k6eAd1	vpravo
je	být	k5eAaImIp3nS	být
příklad	příklad	k1gInSc1	příklad
složení	složení	k1gNnSc2	složení
umělého	umělý	k2eAgInSc2d1	umělý
trávníku	trávník	k1gInSc2	trávník
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
vlákna	vlákno	k1gNnPc1	vlákno
povrstvená	povrstvený	k2eAgFnSc1d1	povrstvená
silikonem	silikon	k1gInSc7	silikon
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
2	[number]	k4	2
pryžový	pryžový	k2eAgInSc1d1	pryžový
granulát	granulát	k1gInSc1	granulát
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
3	[number]	k4	3
spirálovitě	spirálovitě	k6eAd1	spirálovitě
zkadeřená	zkadeřený	k2eAgNnPc1d1	zkadeřené
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
4	[number]	k4	4
podklad	podklad	k1gInSc1	podklad
z	z	k7c2	z
polypropylénu	polypropylén	k1gInSc2	polypropylén
</s>
</p>
<p>
<s>
Odhad	odhad	k1gInSc1	odhad
odborníků	odborník	k1gMnPc2	odborník
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
udával	udávat	k5eAaImAgInS	udávat
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
spotřebu	spotřeba	k1gFnSc4	spotřeba
koberců	koberec	k1gInPc2	koberec
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
umělého	umělý	k2eAgInSc2d1	umělý
trávníku	trávník	k1gInSc2	trávník
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2021	[number]	k4	2021
na	na	k7c4	na
5	[number]	k4	5
miliard	miliarda	k4xCgFnPc2	miliarda
m	m	kA	m
<g/>
2	[number]	k4	2
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
45	[number]	k4	45
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
Gewebetechnik	Gewebetechnik	k1gMnSc1	Gewebetechnik
<g/>
,	,	kIx,	,
Fachbuchverlag	Fachbuchverlag	k1gMnSc1	Fachbuchverlag
Leipzig	Leipzig	k1gMnSc1	Leipzig
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
378-419	[number]	k4	378-419
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Skaní	skaní	k1gNnSc1	skaní
</s>
</p>
<p>
<s>
Samety	samet	k1gInPc1	samet
a	a	k8xC	a
plyše	plyš	k1gInPc1	plyš
</s>
</p>
<p>
<s>
Netkané	tkaný	k2eNgFnPc1d1	netkaná
textilie	textilie	k1gFnPc1	textilie
</s>
</p>
<p>
<s>
Plst	plst	k1gFnSc1	plst
</s>
</p>
<p>
<s>
Vázání	vázání	k1gNnSc1	vázání
koberců	koberec	k1gInPc2	koberec
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
koberec	koberec	k1gInSc1	koberec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
koberec	koberec	k1gInSc1	koberec
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
