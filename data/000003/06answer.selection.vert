<s>
Čokoláda	čokoláda	k1gFnSc1	čokoláda
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
kvašených	kvašený	k2eAgNnPc2d1	kvašené
<g/>
,	,	kIx,	,
pražených	pražený	k2eAgNnPc2d1	pražené
a	a	k8xC	a
mletých	mletý	k2eAgNnPc2d1	mleté
zrnek	zrnko	k1gNnPc2	zrnko
tropického	tropický	k2eAgInSc2d1	tropický
kakaového	kakaový	k2eAgInSc2d1	kakaový
stromu	strom	k1gInSc2	strom
Theobroma	Theobromum	k1gNnSc2	Theobromum
cacao	cacao	k1gNnSc1	cacao
<g/>
.	.	kIx.	.
</s>
