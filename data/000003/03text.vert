<s>
Čokoláda	čokoláda	k1gFnSc1	čokoláda
je	být	k5eAaImIp3nS	být
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
součást	součást	k1gFnSc1	součást
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
druhů	druh	k1gInPc2	druh
sladkostí	sladkost	k1gFnPc2	sladkost
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
bezesporu	bezesporu	k9	bezesporu
k	k	k7c3	k
nejpopulárnějším	populární	k2eAgFnPc3d3	nejpopulárnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
čokoláda	čokoláda	k1gFnSc1	čokoláda
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
aztéckém	aztécký	k2eAgInSc6d1	aztécký
výrazu	výraz	k1gInSc6	výraz
xocolā	xocolā	k?	xocolā
(	(	kIx(	(
<g/>
čteno	čten	k2eAgNnSc4d1	čteno
[	[	kIx(	[
<g/>
šokolatl	šokolatnout	k5eAaPmAgMnS	šokolatnout
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
hořké	hořký	k2eAgNnSc1d1	hořké
pití	pití	k1gNnSc1	pití
<g/>
.	.	kIx.	.
</s>
<s>
Čokoláda	čokoláda	k1gFnSc1	čokoláda
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
kvašených	kvašený	k2eAgNnPc2d1	kvašené
<g/>
,	,	kIx,	,
pražených	pražený	k2eAgNnPc2d1	pražené
a	a	k8xC	a
mletých	mletý	k2eAgNnPc2d1	mleté
zrnek	zrnko	k1gNnPc2	zrnko
tropického	tropický	k2eAgInSc2d1	tropický
kakaového	kakaový	k2eAgInSc2d1	kakaový
stromu	strom	k1gInSc2	strom
Theobroma	Theobromum	k1gNnSc2	Theobromum
cacao	cacao	k1gNnSc1	cacao
<g/>
.	.	kIx.	.
</s>
<s>
Zrna	zrno	k1gNnPc1	zrno
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
kakaových	kakaový	k2eAgFnPc2d1	kakaová
tobolek	tobolka	k1gFnPc2	tobolka
-	-	kIx~	-
bobů	bob	k1gInPc2	bob
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
produkt	produkt	k1gInSc1	produkt
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
jako	jako	k9	jako
"	"	kIx"	"
<g/>
čokoláda	čokoláda	k1gFnSc1	čokoláda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
intenzivně	intenzivně	k6eAd1	intenzivně
ochucená	ochucený	k2eAgFnSc1d1	ochucená
hořká	hořký	k2eAgFnSc1d1	hořká
potravina	potravina	k1gFnSc1	potravina
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
definice	definice	k1gFnSc1	definice
čokolády	čokoláda	k1gFnSc2	čokoláda
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
slovnících	slovník	k1gInPc6	slovník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
produkt	produkt	k1gInSc1	produkt
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
jako	jako	k8xC	jako
kakao	kakao	k1gNnSc4	kakao
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
čokoládovém	čokoládový	k2eAgInSc6d1	čokoládový
průmyslu	průmysl	k1gInSc6	průmysl
je	být	k5eAaImIp3nS	být
kakao	kakao	k1gNnSc1	kakao
definováno	definovat	k5eAaBmNgNnS	definovat
jako	jako	k8xC	jako
sušina	sušina	k1gFnSc1	sušina
kakaových	kakaový	k2eAgNnPc2d1	kakaové
zrn	zrno	k1gNnPc2	zrno
<g/>
,	,	kIx,	,
kakaové	kakaový	k2eAgNnSc1d1	kakaové
máslo	máslo	k1gNnSc1	máslo
je	být	k5eAaImIp3nS	být
definováno	definovat	k5eAaBmNgNnS	definovat
jako	jako	k8xS	jako
tuková	tukový	k2eAgFnSc1d1	tuková
složka	složka	k1gFnSc1	složka
<g/>
,	,	kIx,	,
a	a	k8xC	a
čokoláda	čokoláda	k1gFnSc1	čokoláda
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
sušiny	sušina	k1gFnSc2	sušina
a	a	k8xC	a
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
směs	směs	k1gFnSc1	směs
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
oslazena	osladit	k5eAaPmNgFnS	osladit
cukrem	cukr	k1gInSc7	cukr
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
přísadami	přísada	k1gFnPc7	přísada
a	a	k8xC	a
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
do	do	k7c2	do
čokoládových	čokoládový	k2eAgFnPc2d1	čokoládová
tabulek	tabulka	k1gFnPc2	tabulka
(	(	kIx(	(
<g/>
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
též	též	k9	též
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
čokoláda	čokoláda	k1gFnSc1	čokoláda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
do	do	k7c2	do
nápoje	nápoj	k1gInSc2	nápoj
(	(	kIx(	(
<g/>
zvaného	zvaný	k2eAgInSc2d1	zvaný
kakao	kakao	k1gNnSc1	kakao
nebo	nebo	k8xC	nebo
horká	horký	k2eAgFnSc1d1	horká
čokoláda	čokoláda	k1gFnSc1	čokoláda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
typy	typ	k1gInPc1	typ
kakaových	kakaový	k2eAgNnPc2d1	kakaové
zrn	zrno	k1gNnPc2	zrno
užívaných	užívaný	k2eAgMnPc2d1	užívaný
v	v	k7c6	v
čokoládách	čokoláda	k1gFnPc6	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
ceněná	ceněný	k2eAgFnSc1d1	ceněná
<g/>
,	,	kIx,	,
vzácná	vzácný	k2eAgFnSc1d1	vzácná
a	a	k8xC	a
drahá	drahá	k1gFnSc1	drahá
jsou	být	k5eAaImIp3nP	být
zrna	zrno	k1gNnPc1	zrno
Criollo	Criollo	k1gNnSc1	Criollo
z	z	k7c2	z
mayské	mayský	k2eAgFnSc2d1	mayská
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
Mexiko	Mexiko	k1gNnSc1	Mexiko
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zrna	zrno	k1gNnPc1	zrno
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
hořká	hořký	k2eAgFnSc1d1	hořká
a	a	k8xC	a
aromatičtější	aromatický	k2eAgFnPc1d2	aromatičtější
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Dělá	dělat	k5eAaImIp3nS	dělat
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jen	jen	k9	jen
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
čokolády	čokoláda	k1gFnSc2	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Kakaová	kakaový	k2eAgNnPc4d1	kakaové
zrna	zrno	k1gNnPc4	zrno
v	v	k7c6	v
80	[number]	k4	80
%	%	kIx~	%
čokolády	čokoláda	k1gFnPc1	čokoláda
jsou	být	k5eAaImIp3nP	být
typu	typa	k1gFnSc4	typa
Forastero	Forastero	k1gNnSc1	Forastero
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
Forastero	Forastero	k1gNnSc1	Forastero
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
robustnější	robustní	k2eAgInPc1d2	robustnější
než	než	k8xS	než
stromy	strom	k1gInPc1	strom
Criollo	Criollo	k1gNnSc4	Criollo
a	a	k8xC	a
kakaová	kakaový	k2eAgNnPc4d1	kakaové
zrna	zrno	k1gNnPc4	zrno
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
levnější	levný	k2eAgInPc1d2	levnější
<g/>
.	.	kIx.	.
</s>
<s>
Trinitario	Trinitario	k6eAd1	Trinitario
<g/>
,	,	kIx,	,
hybrid	hybrid	k1gInSc4	hybrid
zrn	zrno	k1gNnPc2	zrno
Criollo	Criollo	k1gNnSc1	Criollo
a	a	k8xC	a
Forastero	Forastero	k1gNnSc1	Forastero
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
asi	asi	k9	asi
v	v	k7c6	v
10	[number]	k4	10
%	%	kIx~	%
čokolády	čokoláda	k1gFnSc2	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
přichází	přicházet	k5eAaImIp3nS	přicházet
čokoláda	čokoláda	k1gFnSc1	čokoláda
ponejvíce	ponejvíce	k6eAd1	ponejvíce
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tabulek	tabulka	k1gFnPc2	tabulka
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
geometrických	geometrický	k2eAgInPc2d1	geometrický
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
též	též	k9	též
tvarována	tvarován	k2eAgFnSc1d1	tvarována
do	do	k7c2	do
malých	malý	k2eAgFnPc2d1	malá
figurek	figurka	k1gFnPc2	figurka
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
lidí	člověk	k1gMnPc2	člověk
nebo	nebo	k8xC	nebo
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
tvary	tvar	k1gInPc1	tvar
přímo	přímo	k6eAd1	přímo
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
k	k	k7c3	k
určitým	určitý	k2eAgInPc3d1	určitý
svátkům	svátek	k1gInPc3	svátek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
velikonoční	velikonoční	k2eAgMnPc1d1	velikonoční
zajíci	zajíc	k1gMnPc1	zajíc
nebo	nebo	k8xC	nebo
vejce	vejce	k1gNnSc1	vejce
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
vánoční	vánoční	k2eAgFnPc1d1	vánoční
či	či	k8xC	či
mikulášské	mikulášský	k2eAgFnPc1d1	Mikulášská
figurky	figurka	k1gFnPc1	figurka
anebo	anebo	k8xC	anebo
tvary	tvar	k1gInPc1	tvar
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
den	den	k1gInSc4	den
svatého	svatý	k2eAgMnSc2d1	svatý
Valentina	Valentin	k1gMnSc2	Valentin
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
populárním	populární	k2eAgInSc7d1	populární
tvarem	tvar	k1gInSc7	tvar
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
čokoládové	čokoládový	k2eAgInPc1d1	čokoládový
"	"	kIx"	"
<g/>
pusy	pusa	k1gFnPc4	pusa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
čokoláda	čokoláda	k1gFnSc1	čokoláda
často	často	k6eAd1	často
tvoří	tvořit	k5eAaImIp3nS	tvořit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
složku	složka	k1gFnSc4	složka
nebo	nebo	k8xC	nebo
přísadu	přísada	k1gFnSc4	přísada
v	v	k7c6	v
potravinách	potravina	k1gFnPc6	potravina
jako	jako	k8xC	jako
zmrzlina	zmrzlina	k1gFnSc1	zmrzlina
<g/>
,	,	kIx,	,
koláče	koláč	k1gInPc1	koláč
<g/>
,	,	kIx,	,
sušenky	sušenka	k1gFnPc1	sušenka
<g/>
,	,	kIx,	,
buchty	buchta	k1gFnPc1	buchta
a	a	k8xC	a
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
dezertech	dezert	k1gInPc6	dezert
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
čokoládu	čokoláda	k1gFnSc4	čokoláda
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
dávno	dávno	k6eAd1	dávno
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
Mayové	May	k1gMnPc1	May
a	a	k8xC	a
Aztékové	Azték	k1gMnPc1	Azték
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
kakaové	kakaový	k2eAgFnPc4d1	kakaová
placky	placka	k1gFnPc4	placka
nebo	nebo	k8xC	nebo
připravovali	připravovat	k5eAaImAgMnP	připravovat
kakaové	kakaový	k2eAgInPc4d1	kakaový
nápoje	nápoj	k1gInPc4	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Kakaové	kakaový	k2eAgInPc1d1	kakaový
boby	bob	k1gInPc1	bob
přivezl	přivézt	k5eAaPmAgMnS	přivézt
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
poprvé	poprvé	k6eAd1	poprvé
Fernando	Fernanda	k1gFnSc5	Fernanda
Cortéz	Cortéza	k1gFnPc2	Cortéza
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
čokoládou	čokoláda	k1gFnSc7	čokoláda
setkal	setkat	k5eAaPmAgMnS	setkat
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
aztéckého	aztécký	k2eAgMnSc2d1	aztécký
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Španělské	španělský	k2eAgFnPc1d1	španělská
kroniky	kronika	k1gFnPc1	kronika
dobývání	dobývání	k1gNnSc2	dobývání
Mexika	Mexiko	k1gNnSc2	Mexiko
Hernánem	Hernán	k1gMnSc7	Hernán
Cortézem	Cortéz	k1gMnSc7	Cortéz
udávají	udávat	k5eAaImIp3nP	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
slavnostní	slavnostní	k2eAgFnSc6d1	slavnostní
tabuli	tabule	k1gFnSc6	tabule
Montezumy	Montezum	k1gInPc1	Montezum
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnSc1	vládce
Aztéků	Azték	k1gMnPc2	Azték
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
jiných	jiný	k2eAgInPc2d1	jiný
nápojů	nápoj	k1gInPc2	nápoj
než	než	k8xS	než
čokolády	čokoláda	k1gFnPc1	čokoláda
<g/>
,	,	kIx,	,
podávané	podávaný	k2eAgInPc1d1	podávaný
ve	v	k7c6	v
zlatých	zlatý	k2eAgInPc6d1	zlatý
pohárech	pohár	k1gInPc6	pohár
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
lžičkou	lžička	k1gFnSc7	lžička
<g/>
.	.	kIx.	.
</s>
<s>
Dochucována	Dochucován	k2eAgFnSc1d1	Dochucován
vanilkou	vanilka	k1gFnSc7	vanilka
a	a	k8xC	a
kořením	koření	k1gNnSc7	koření
<g/>
,	,	kIx,	,
podávaná	podávaný	k2eAgFnSc1d1	podávaná
čokoláda	čokoláda	k1gFnSc1	čokoláda
byla	být	k5eAaImAgFnS	být
našlehána	našlehat	k5eAaPmNgFnS	našlehat
do	do	k7c2	do
lahodné	lahodný	k2eAgFnSc2d1	lahodná
pěny	pěna	k1gFnSc2	pěna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
rozpouštěla	rozpouštět	k5eAaImAgFnS	rozpouštět
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
připraveno	připravit	k5eAaPmNgNnS	připravit
alespoň	alespoň	k9	alespoň
50	[number]	k4	50
šálků	šálek	k1gInPc2	šálek
čokolády	čokoláda	k1gFnSc2	čokoláda
pro	pro	k7c4	pro
krále	král	k1gMnSc4	král
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
2000	[number]	k4	2000
pro	pro	k7c4	pro
šlechtice	šlechtic	k1gMnPc4	šlechtic
jeho	on	k3xPp3gInSc2	on
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
čokoládu	čokoláda	k1gFnSc4	čokoláda
přivezli	přivézt	k5eAaPmAgMnP	přivézt
Španělé	Španěl	k1gMnPc1	Španěl
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
stala	stát	k5eAaPmAgFnS	stát
velmi	velmi	k6eAd1	velmi
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
nápojem	nápoj	k1gInSc7	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
také	také	k9	také
dovezli	dovézt	k5eAaPmAgMnP	dovézt
kakaovníky	kakaovník	k1gInPc4	kakaovník
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Zadní	zadní	k2eAgFnSc2d1	zadní
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Španělské	španělský	k2eAgFnSc2d1	španělská
východní	východní	k2eAgFnSc2d1	východní
Indie	Indie	k1gFnSc2	Indie
(	(	kIx(	(
<g/>
především	především	k9	především
na	na	k7c4	na
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
i	i	k9	i
tuhá	tuhý	k2eAgFnSc1d1	tuhá
čokoláda	čokoláda	k1gFnSc1	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Čokoláda	čokoláda	k1gFnSc1	čokoláda
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc1d1	populární
pochutina	pochutina	k1gFnSc1	pochutina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
typech	typ	k1gInPc6	typ
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
formy	forma	k1gFnPc1	forma
a	a	k8xC	a
chutě	chuť	k1gFnPc1	chuť
čokolády	čokoláda	k1gFnSc2	čokoláda
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
obměňováním	obměňování	k1gNnSc7	obměňování
množství	množství	k1gNnSc2	množství
přísad	přísada	k1gFnPc2	přísada
<g/>
.	.	kIx.	.
</s>
<s>
Neslazená	slazený	k2eNgFnSc1d1	neslazená
čokoláda	čokoláda	k1gFnSc1	čokoláda
Čistý	čistý	k2eAgInSc1d1	čistý
čokoládový	čokoládový	k2eAgInSc4d1	čokoládový
nápoj	nápoj	k1gInSc4	nápoj
<g/>
,	,	kIx,	,
též	též	k9	též
znám	znát	k5eAaImIp1nS	znát
jako	jako	k9	jako
hořká	hořká	k1gFnSc1	hořká
nebo	nebo	k8xC	nebo
čokoláda	čokoláda	k1gFnSc1	čokoláda
na	na	k7c6	na
vaření	vaření	k1gNnSc6	vaření
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nesmíšená	smíšený	k2eNgFnSc1d1	nesmíšená
čokoláda	čokoláda	k1gFnSc1	čokoláda
<g/>
:	:	kIx,	:
mletá	mletý	k2eAgFnSc1d1	mletá
pražená	pražený	k2eAgNnPc4d1	pražené
čokoládová	čokoládový	k2eAgNnPc4d1	čokoládové
zrna	zrno	k1gNnPc4	zrno
bez	bez	k7c2	bez
přísad	přísada	k1gFnPc2	přísada
dávají	dávat	k5eAaImIp3nP	dávat
silnou	silný	k2eAgFnSc4d1	silná
<g/>
,	,	kIx,	,
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
čokoládovou	čokoládový	k2eAgFnSc4d1	čokoládová
chuť	chuť	k1gFnSc4	chuť
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
sladkostí	sladkost	k1gFnPc2	sladkost
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgNnPc2	který
jsou	být	k5eAaImIp3nP	být
přidána	přidán	k2eAgNnPc1d1	Přidáno
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
s	s	k7c7	s
přidaným	přidaný	k2eAgInSc7d1	přidaný
cukrem	cukr	k1gInSc7	cukr
jsou	být	k5eAaImIp3nP	být
používané	používaný	k2eAgInPc1d1	používaný
jako	jako	k8xC	jako
základ	základ	k1gInSc1	základ
vrstvových	vrstvový	k2eAgInPc2d1	vrstvový
dortů	dort	k1gInPc2	dort
<g/>
,	,	kIx,	,
koláčků	koláček	k1gInPc2	koláček
<g/>
,	,	kIx,	,
cukroví	cukroví	k1gNnSc1	cukroví
<g/>
,	,	kIx,	,
keksů	keks	k1gInPc2	keks
atd.	atd.	kA	atd.
Tmavá	tmavý	k2eAgFnSc1d1	tmavá
čokoláda	čokoláda	k1gFnSc1	čokoláda
Čokoláda	čokoláda	k1gFnSc1	čokoláda
bez	bez	k7c2	bez
mléka	mléko	k1gNnSc2	mléko
jako	jako	k8xS	jako
přísady	přísada	k1gFnSc2	přísada
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
zvaná	zvaný	k2eAgFnSc1d1	zvaná
jako	jako	k8xC	jako
prostá	prostý	k2eAgFnSc1d1	prostá
čokoláda	čokoláda	k1gFnSc1	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
sladká	sladký	k2eAgFnSc1d1	sladká
čokoláda	čokoláda	k1gFnSc1	čokoláda
<g/>
"	"	kIx"	"
a	a	k8xC	a
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
15	[number]	k4	15
<g/>
%	%	kIx~	%
koncentraci	koncentrace	k1gFnSc4	koncentrace
čokoládového	čokoládový	k2eAgInSc2d1	čokoládový
moku	mok	k1gInSc2	mok
<g/>
.	.	kIx.	.
</s>
<s>
Předpisy	předpis	k1gInPc1	předpis
EU	EU	kA	EU
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
v	v	k7c6	v
mléčné	mléčný	k2eAgFnSc6d1	mléčná
čokoládě	čokoláda	k1gFnSc6	čokoláda
zastoupení	zastoupení	k1gNnSc2	zastoupení
nejméně	málo	k6eAd3	málo
35	[number]	k4	35
%	%	kIx~	%
pevných	pevný	k2eAgFnPc2d1	pevná
složek	složka	k1gFnPc2	složka
kakaa	kakao	k1gNnSc2	kakao
<g/>
.	.	kIx.	.
</s>
<s>
Couverture	Couvertur	k1gMnSc5	Couvertur
Termín	termín	k1gInSc1	termín
používaný	používaný	k2eAgInSc1d1	používaný
pro	pro	k7c4	pro
čokolády	čokoláda	k1gFnPc4	čokoláda
obsahující	obsahující	k2eAgFnPc4d1	obsahující
mnoho	mnoho	k6eAd1	mnoho
kakaového	kakaový	k2eAgNnSc2d1	kakaové
másla	máslo	k1gNnSc2	máslo
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgInSc1d1	populární
druh	druh	k1gInSc1	druh
čokolády	čokoláda	k1gFnSc2	čokoláda
Couverture	Couvertur	k1gMnSc5	Couvertur
používaný	používaný	k2eAgInSc4d1	používaný
profesionálními	profesionální	k2eAgMnPc7d1	profesionální
cukráři	cukrář	k1gMnPc7	cukrář
a	a	k8xC	a
často	často	k6eAd1	často
prodáván	prodávat	k5eAaImNgInS	prodávat
v	v	k7c6	v
gurmánských	gurmánský	k2eAgInPc6d1	gurmánský
a	a	k8xC	a
speciálních	speciální	k2eAgInPc6d1	speciální
potravinových	potravinový	k2eAgInPc6d1	potravinový
obchodech	obchod	k1gInPc6	obchod
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ingredienty	ingredienta	k1gFnPc4	ingredienta
<g/>
:	:	kIx,	:
Valrhona	Valrhona	k1gFnSc1	Valrhona
<g/>
,	,	kIx,	,
Lindt	Lindt	k1gMnSc1	Lindt
<g/>
,	,	kIx,	,
Coco	Coco	k1gMnSc1	Coco
Barry	Barra	k1gFnSc2	Barra
a	a	k8xC	a
Esprit	esprit	k1gInSc1	esprit
des	des	k1gNnSc2	des
Alpes	Alpesa	k1gFnPc2	Alpesa
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
čokolády	čokoláda	k1gFnPc1	čokoláda
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vysoké	vysoký	k2eAgNnSc4d1	vysoké
procento	procento	k1gNnSc4	procento
čokoládového	čokoládový	k2eAgInSc2d1	čokoládový
moku	mok	k1gInSc2	mok
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
%	%	kIx~	%
<g/>
)	)	kIx)	)
právě	právě	k6eAd1	právě
jako	jako	k8xC	jako
kakaové	kakaový	k2eAgNnSc1d1	kakaové
máslo	máslo	k1gNnSc1	máslo
<g/>
,	,	kIx,	,
aspoň	aspoň	k9	aspoň
32	[number]	k4	32
<g/>
-	-	kIx~	-
<g/>
39	[number]	k4	39
%	%	kIx~	%
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
tekuté	tekutý	k2eAgInPc1d1	tekutý
po	po	k7c4	po
rozpuštění	rozpuštění	k1gNnSc4	rozpuštění
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
výbornou	výborný	k2eAgFnSc4d1	výborná
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
čokoláda	čokoláda	k1gFnSc1	čokoláda
Čokoláda	čokoláda	k1gFnSc1	čokoláda
s	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
kondenzovaného	kondenzovaný	k2eAgNnSc2d1	kondenzované
nebo	nebo	k8xC	nebo
sušeného	sušený	k2eAgNnSc2d1	sušené
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgFnPc1d1	Evropská
normy	norma	k1gFnPc1	norma
specifikují	specifikovat	k5eAaBmIp3nP	specifikovat
minimálně	minimálně	k6eAd1	minimálně
25	[number]	k4	25
%	%	kIx~	%
kakaové	kakaový	k2eAgFnSc2d1	kakaová
sušiny	sušina	k1gFnSc2	sušina
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
čokoláda	čokoláda	k1gFnSc1	čokoláda
Cukrářský	cukrářský	k2eAgInSc1d1	cukrářský
výrobek	výrobek	k1gInSc4	výrobek
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
kakaovém	kakaový	k2eAgNnSc6d1	kakaové
másle	máslo	k1gNnSc6	máslo
bez	bez	k7c2	bez
pevných	pevný	k2eAgFnPc2d1	pevná
kakaových	kakaový	k2eAgFnPc2d1	kakaová
složek	složka	k1gFnPc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Ledová	ledový	k2eAgFnSc1d1	ledová
čokoláda	čokoláda	k1gFnSc1	čokoláda
Čokoláda	čokoláda	k1gFnSc1	čokoláda
obsahující	obsahující	k2eAgInSc1d1	obsahující
nerafinovaný	rafinovaný	k2eNgInSc1d1	nerafinovaný
kokosový	kokosový	k2eAgInSc1d1	kokosový
tuk	tuk	k1gInSc1	tuk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
již	již	k6eAd1	již
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
chladivý	chladivý	k2eAgInSc4d1	chladivý
efekt	efekt	k1gInSc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Ledová	ledový	k2eAgFnSc1d1	ledová
čokoláda	čokoláda	k1gFnSc1	čokoláda
by	by	kYmCp3nS	by
neměla	mít	k5eNaImAgFnS	mít
být	být	k5eAaImF	být
uchovávána	uchovávat	k5eAaImNgFnS	uchovávat
v	v	k7c6	v
ledničce	lednička	k1gFnSc6	lednička
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
chladivý	chladivý	k2eAgInSc1d1	chladivý
efekt	efekt	k1gInSc1	efekt
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
nedostaví	dostavit	k5eNaPmIp3nP	dostavit
<g/>
.	.	kIx.	.
</s>
<s>
Bublinková	bublinkový	k2eAgFnSc1d1	bublinková
čokoláda	čokoláda	k1gFnSc1	čokoláda
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Schaumschokolade	Schaumschokolad	k1gInSc5	Schaumschokolad
<g/>
,	,	kIx,	,
Luftschokolade	Luftschokolad	k1gInSc5	Luftschokolad
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
aerated	aerated	k1gInSc1	aerated
chocolate	chocolat	k1gInSc5	chocolat
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Čokoláda	čokoláda	k1gFnSc1	čokoláda
<g/>
,	,	kIx,	,
při	při	k7c6	při
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
výrobě	výroba	k1gFnSc6	výroba
se	se	k3xPyFc4	se
do	do	k7c2	do
čokoládové	čokoládový	k2eAgFnSc2d1	čokoládová
směsi	směs	k1gFnSc2	směs
vhání	vhánět	k5eAaImIp3nS	vhánět
vzduch	vzduch	k1gInSc1	vzduch
a	a	k8xC	a
chlazení	chlazení	k1gNnSc1	chlazení
se	se	k3xPyFc4	se
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
se	s	k7c7	s
sníženým	snížený	k2eAgInSc7d1	snížený
tlakem	tlak	k1gInSc7	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vynalezena	vynaleznout	k5eAaPmNgFnS	vynaleznout
firmou	firma	k1gFnSc7	firma
Rowntree	Rowntre	k1gFnSc2	Rowntre
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
čokolády	čokoláda	k1gFnSc2	čokoláda
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
surovinách	surovina	k1gFnPc6	surovina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
kakao	kakao	k1gNnSc4	kakao
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
kakaový	kakaový	k2eAgInSc1d1	kakaový
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
kakaová	kakaový	k2eAgFnSc1d1	kakaová
hmota	hmota	k1gFnSc1	hmota
a	a	k8xC	a
kakaová	kakaový	k2eAgFnSc1d1	kakaová
pokrutina	pokrutina	k1gFnSc1	pokrutina
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tuk	tuk	k1gInSc1	tuk
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc1	cukr
nebo	nebo	k8xC	nebo
sladidla	sladidlo	k1gNnPc1	sladidlo
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
přísady	přísada	k1gFnPc1	přísada
(	(	kIx(	(
<g/>
např.	např.	kA	např.
palmový	palmový	k2eAgInSc1d1	palmový
olej	olej	k1gInSc1	olej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
přísady	přísada	k1gFnPc1	přísada
se	se	k3xPyFc4	se
pečlivě	pečlivě	k6eAd1	pečlivě
promíchají	promíchat	k5eAaPmIp3nP	promíchat
v	v	k7c6	v
míchacím	míchací	k2eAgInSc6d1	míchací
stroji	stroj	k1gInSc6	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Válcovací	válcovací	k2eAgFnSc1d1	válcovací
stolice	stolice	k1gFnSc1	stolice
Konšování	Konšování	k1gNnSc2	Konšování
Temperace	Temperace	k1gFnSc2	Temperace
Chlazení	chlazení	k1gNnSc2	chlazení
Válcovací	válcovací	k2eAgFnSc2d1	válcovací
stolice	stolice	k1gFnSc2	stolice
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
zjemnění	zjemnění	k1gNnSc3	zjemnění
chuti	chuť	k1gFnSc2	chuť
čokolády	čokoláda	k1gFnSc2	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Čokoládová	čokoládový	k2eAgFnSc1d1	čokoládová
hmota	hmota	k1gFnSc1	hmota
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
lije	lít	k5eAaImIp3nS	lít
a	a	k8xC	a
roztírá	roztírat	k5eAaImIp3nS	roztírat
<g/>
.	.	kIx.	.
</s>
<s>
Konšování	Konšování	k1gNnSc1	Konšování
-	-	kIx~	-
tento	tento	k3xDgInSc4	tento
pojem	pojem	k1gInSc4	pojem
znamená	znamenat	k5eAaImIp3nS	znamenat
chuť	chuť	k1gFnSc4	chuť
čokolády	čokoláda	k1gFnSc2	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
čokoláda	čokoláda	k1gFnSc1	čokoláda
konšuje	konšovat	k5eAaPmIp3nS	konšovat
<g/>
,	,	kIx,	,
přidáváme	přidávat	k5eAaImIp1nP	přidávat
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
dochucovací	dochucovací	k2eAgFnSc1d1	dochucovací
prostředky	prostředek	k1gInPc7	prostředek
a	a	k8xC	a
emulgátory	emulgátor	k1gInPc7	emulgátor
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
celá	celý	k2eAgFnSc1d1	celá
hmota	hmota	k1gFnSc1	hmota
se	se	k3xPyFc4	se
míchá	míchat	k5eAaImIp3nS	míchat
<g/>
,	,	kIx,	,
šlehá	šlehat	k5eAaImIp3nS	šlehat
a	a	k8xC	a
provzdušňuje	provzdušňovat	k5eAaImIp3nS	provzdušňovat
<g/>
.	.	kIx.	.
</s>
<s>
Provzdušnění	provzdušnění	k1gNnSc2	provzdušnění
čokoládové	čokoládový	k2eAgFnSc6d1	čokoládová
hmotě	hmota	k1gFnSc6	hmota
dodá	dodat	k5eAaPmIp3nS	dodat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
čokolády	čokoláda	k1gFnSc2	čokoláda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
určíme	určit	k5eAaPmIp1nP	určit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dlouho	dlouho	k6eAd1	dlouho
čokoládu	čokoláda	k1gFnSc4	čokoláda
budeme	být	k5eAaImBp1nP	být
tzv.	tzv.	kA	tzv.
konšovat	konšovat	k5eAaImF	konšovat
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
to	ten	k3xDgNnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
48	[number]	k4	48
až	až	k9	až
96	[number]	k4	96
hodin	hodina	k1gFnPc2	hodina
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
50	[number]	k4	50
až	až	k9	až
80	[number]	k4	80
°	°	k?	°
<g/>
C.	C.	kA	C.
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
čokoládová	čokoládový	k2eAgFnSc1d1	čokoládová
hmota	hmota	k1gFnSc1	hmota
putuje	putovat	k5eAaImIp3nS	putovat
do	do	k7c2	do
zásobníku	zásobník	k1gInSc2	zásobník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
homogenizuje	homogenizovat	k5eAaBmIp3nS	homogenizovat
při	při	k7c6	při
následné	následný	k2eAgFnSc6d1	následná
teplotě	teplota	k1gFnSc6	teplota
50	[number]	k4	50
až	až	k9	až
60	[number]	k4	60
°	°	k?	°
<g/>
C.	C.	kA	C.
Dále	daleko	k6eAd2	daleko
následuje	následovat	k5eAaImIp3nS	následovat
tzv.	tzv.	kA	tzv.
temperace	temperace	k1gFnSc1	temperace
a	a	k8xC	a
chlazení	chlazení	k1gNnSc1	chlazení
<g/>
.	.	kIx.	.
</s>
<s>
Temperace	Temperace	k1gFnSc1	Temperace
probíhá	probíhat	k5eAaImIp3nS	probíhat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
poté	poté	k6eAd1	poté
následuje	následovat	k5eAaImIp3nS	následovat
plnění	plnění	k1gNnSc1	plnění
do	do	k7c2	do
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Temperace	Temperace	k1gFnSc1	Temperace
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
odolnosti	odolnost	k1gFnSc3	odolnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tuk	tuk	k1gInSc1	tuk
neodděloval	oddělovat	k5eNaImAgInS	oddělovat
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
nezpůsoboval	způsobovat	k5eNaImAgMnS	způsobovat
výkvět	výkvět	k1gInSc4	výkvět
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
k	k	k7c3	k
jemné	jemný	k2eAgFnSc3d1	jemná
struktuře	struktura	k1gFnSc3	struktura
a	a	k8xC	a
lesklosti	lesklost	k1gFnSc3	lesklost
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
chlazení	chlazení	k1gNnSc1	chlazení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
chladicích	chladicí	k2eAgFnPc6d1	chladicí
skříních	skříň	k1gFnPc6	skříň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čokoláda	čokoláda	k1gFnSc1	čokoláda
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ztuhnutí	ztuhnutí	k1gNnSc6	ztuhnutí
se	se	k3xPyFc4	se
čokoláda	čokoláda	k1gFnSc1	čokoláda
z	z	k7c2	z
forem	forma	k1gFnPc2	forma
vyklepne	vyklepnout	k5eAaPmIp3nS	vyklepnout
a	a	k8xC	a
balí	balit	k5eAaImIp3nS	balit
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
čokoládě	čokoláda	k1gFnSc6	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pultech	pult	k1gInPc6	pult
českých	český	k2eAgInPc2d1	český
obchodů	obchod	k1gInPc2	obchod
lze	lze	k6eAd1	lze
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
spatřit	spatřit	k5eAaPmF	spatřit
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
vypadají	vypadat	k5eAaImIp3nP	vypadat
jako	jako	k9	jako
čokoládové	čokoládový	k2eAgFnPc1d1	čokoládová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
různé	různý	k2eAgFnPc4d1	různá
náhražky	náhražka	k1gFnPc4	náhražka
čokolád	čokoláda	k1gFnPc2	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
náhražek	náhražka	k1gFnPc2	náhražka
bývají	bývat	k5eAaImIp3nP	bývat
většinou	většinou	k6eAd1	většinou
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
i	i	k9	i
různé	různý	k2eAgFnPc1d1	různá
vánoční	vánoční	k2eAgFnPc1d1	vánoční
i	i	k8xC	i
velikonoční	velikonoční	k2eAgFnPc1d1	velikonoční
figurky	figurka	k1gFnPc1	figurka
<g/>
.	.	kIx.	.
</s>
<s>
Spotřebitel	spotřebitel	k1gMnSc1	spotřebitel
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgInSc3	tento
sledovat	sledovat	k5eAaImF	sledovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
skutečný	skutečný	k2eAgInSc4d1	skutečný
výrobek	výrobek	k1gInSc4	výrobek
z	z	k7c2	z
čokolády	čokoláda	k1gFnSc2	čokoláda
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
psy	pes	k1gMnPc4	pes
je	být	k5eAaImIp3nS	být
čokoláda	čokoláda	k1gFnSc1	čokoláda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jako	jako	k8xC	jako
pamlsek	pamlsek	k1gInSc1	pamlsek
<g/>
)	)	kIx)	)
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gNnSc4	jejich
tělo	tělo	k1gNnSc4	tělo
ji	on	k3xPp3gFnSc4	on
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
strávit	strávit	k5eAaPmF	strávit
<g/>
.	.	kIx.	.
</s>
<s>
Čokoláda	čokoláda	k1gFnSc1	čokoláda
totiž	totiž	k9	totiž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
některé	některý	k3yIgInPc4	některý
alkaloidy	alkaloid	k1gInPc4	alkaloid
jako	jako	k8xS	jako
theofylin	theofylin	k1gInSc1	theofylin
a	a	k8xC	a
kofein	kofein	k1gInSc1	kofein
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
psí	psí	k2eAgInSc4d1	psí
organismus	organismus	k1gInSc4	organismus
problémy	problém	k1gInPc7	problém
zpracovat	zpracovat	k5eAaPmF	zpracovat
<g/>
;	;	kIx,	;
zejména	zejména	k9	zejména
ale	ale	k9	ale
theobromin	theobromin	k1gInSc1	theobromin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
jed	jed	k1gInSc1	jed
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
lidský	lidský	k2eAgInSc1d1	lidský
organismus	organismus	k1gInSc1	organismus
má	mít	k5eAaImIp3nS	mít
enzym	enzym	k1gInSc4	enzym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
theobromin	theobromin	k1gInSc1	theobromin
odbourává	odbourávat	k5eAaImIp3nS	odbourávat
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
jej	on	k3xPp3gMnSc4	on
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Theobromin	theobromin	k1gInSc1	theobromin
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
těžký	těžký	k2eAgInSc4d1	těžký
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
a	a	k8xC	a
srdce	srdce	k1gNnSc4	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
malé	malý	k2eAgFnSc6d1	malá
dávce	dávka	k1gFnSc6	dávka
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
hyperaktivitou	hyperaktivita	k1gFnSc7	hyperaktivita
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
při	při	k7c6	při
vyšší	vysoký	k2eAgFnSc6d2	vyšší
jej	on	k3xPp3gMnSc4	on
může	moct	k5eAaImIp3nS	moct
usmrtit	usmrtit	k5eAaPmF	usmrtit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
obsahu	obsah	k1gInSc3	obsah
flavonoidů	flavonoid	k1gInPc2	flavonoid
některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
a	a	k8xC	a
kardiovaskulární	kardiovaskulární	k2eAgInSc4d1	kardiovaskulární
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
běžných	běžný	k2eAgInPc2d1	běžný
čokoládových	čokoládový	k2eAgInPc2d1	čokoládový
pamlsků	pamlsek	k1gInPc2	pamlsek
však	však	k9	však
převažuje	převažovat	k5eAaImIp3nS	převažovat
negativní	negativní	k2eAgInSc4d1	negativní
efekt	efekt	k1gInSc4	efekt
obsažených	obsažený	k2eAgInPc2d1	obsažený
cukrů	cukr	k1gInPc2	cukr
a	a	k8xC	a
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
afrodiziakum	afrodiziakum	k1gNnSc4	afrodiziakum
a	a	k8xC	a
povzbuzující	povzbuzující	k2eAgInSc4d1	povzbuzující
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Obsažené	obsažený	k2eAgInPc1d1	obsažený
flavonoidy	flavonoid	k1gInPc1	flavonoid
kakaových	kakaový	k2eAgInPc2d1	kakaový
bobů	bob	k1gInPc2	bob
mají	mít	k5eAaImIp3nP	mít
antioxidační	antioxidační	k2eAgInSc4d1	antioxidační
účinek	účinek	k1gInSc4	účinek
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
organizmus	organizmus	k1gInSc1	organizmus
před	před	k7c7	před
účinkem	účinek	k1gInSc7	účinek
volných	volný	k2eAgMnPc2d1	volný
radikálů	radikál	k1gMnPc2	radikál
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
