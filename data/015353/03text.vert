<s>
Issai	Issai	k1gNnSc1
Schur	Schura	k1gFnPc2
</s>
<s>
Issai	Issai	k1gNnSc1
Schur	Schura	k1gFnPc2
Narození	narození	k1gNnSc2
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1875	#num#	k4
<g/>
jul	jul	k?
<g/>
.	.	kIx.
/	/	kIx~
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1876	#num#	k4
<g/>
greg	grega	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohylev	Mohylev	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1941	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
65	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Tel	tel	kA
Aviv	Aviv	k1gInSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Trumpeldorův	Trumpeldorův	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
(	(	kIx(
<g/>
32	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
32	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
34	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
<g/>
12	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
)	)	kIx)
Povolání	povolání	k1gNnSc1
</s>
<s>
matematik	matematik	k1gMnSc1
a	a	k8xC
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Humboldtova	Humboldtův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Témata	téma	k1gNnPc4
</s>
<s>
kombinatorika	kombinatorika	k1gFnSc1
a	a	k8xC
teorie	teorie	k1gFnSc1
grup	grupa	k1gFnPc2
Významná	významný	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
Schur	Schur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
inequalitySchur	inequalitySchura	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
lemmaSchur	lemmaSchura	k1gFnPc2
complementSchurův	complementSchurův	k2eAgMnSc1d1
rozkladSchur	rozkladSchur	k1gMnSc1
<g/>
–	–	k?
<g/>
Horn	Horn	k1gMnSc1
theorem	theor	k1gMnSc7
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Issai	Issai	k1gNnSc1
Schur	Schur	k1gMnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1875	#num#	k4
Mogilev	Mogilev	k1gFnPc2
<g/>
,	,	kIx,
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1941	#num#	k4
Tel	tel	kA
Aviv	Aviva	k1gFnPc2
<g/>
,	,	kIx,
Britský	britský	k2eAgInSc1d1
mandát	mandát	k1gInSc1
Palestina	Palestina	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
židovského	židovský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xC,k8xS
student	student	k1gMnSc1
Ferdinanda	Ferdinand	k1gMnSc2
Georga	Georg	k1gMnSc2
Frobenia	Frobenium	k1gNnSc2
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgInS,k5eAaImAgInS
reprezentacím	reprezentace	k1gFnPc3
grup	grupa	k1gFnPc2
<g/>
,	,	kIx,
pracoval	pracovat	k5eAaImAgInS
také	také	k9
v	v	k7c6
oblastech	oblast	k1gFnPc6
kombinatoriky	kombinatorika	k1gFnSc2
<g/>
,	,	kIx,
lineární	lineární	k2eAgFnSc2d1
algebry	algebra	k1gFnSc2
<g/>
,	,	kIx,
teorie	teorie	k1gFnSc2
čísel	číslo	k1gNnPc2
či	či	k8xC
teoretické	teoretický	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
po	po	k7c6
něm	on	k3xPp3gNnSc6
pojmenováno	pojmenovat	k5eAaPmNgNnS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
matematických	matematický	k2eAgFnPc2d1
vět	věta	k1gFnPc2
a	a	k8xC
konceptů	koncept	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Issai	Issa	k1gFnSc2
Schur	Schura	k1gFnPc2
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
mzk	mzk	k?
<g/>
2004236849	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118795627	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0881	#num#	k4
8294	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
84801612	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
27132789	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
84801612	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Matematika	matematika	k1gFnSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
