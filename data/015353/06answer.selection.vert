<s>
Jako	jako	k8xC,k8xS
student	student	k1gMnSc1
Ferdinanda	Ferdinand	k1gMnSc2
Georga	Georg	k1gMnSc2
Frobenia	Frobenium	k1gNnSc2
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgInS,k5eAaImAgInS
reprezentacím	reprezentace	k1gFnPc3
grup	grupa	k1gFnPc2
<g/>
,	,	kIx,
pracoval	pracovat	k5eAaImAgInS
také	také	k9
v	v	k7c6
oblastech	oblast	k1gFnPc6
kombinatoriky	kombinatorika	k1gFnSc2
<g/>
,	,	kIx,
lineární	lineární	k2eAgFnSc2d1
algebry	algebra	k1gFnSc2
<g/>
,	,	kIx,
teorie	teorie	k1gFnSc2
čísel	číslo	k1gNnPc2
či	či	k8xC
teoretické	teoretický	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
<g/>
.	.	kIx.
</s>