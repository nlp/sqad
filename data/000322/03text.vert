<s>
Vltava	Vltava	k1gFnSc1	Vltava
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Moldau	Moldaus	k1gInSc3	Moldaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
430,2	[number]	k4	430,2
km	km	kA	km
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Černý	Černý	k1gMnSc1	Černý
Kříž	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
soutokem	soutok	k1gInSc7	soutok
Teplé	Teplé	k2eAgFnSc2d1	Teplé
Vltavy	Vltava	k1gFnSc2	Vltava
a	a	k8xC	a
Studené	Studené	k2eAgFnSc2d1	Studené
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
Českým	český	k2eAgInSc7d1	český
Krumlovem	Krumlov	k1gInSc7	Krumlov
<g/>
,	,	kIx,	,
Českými	český	k2eAgInPc7d1	český
Budějovicemi	Budějovice	k1gInPc7	Budějovice
a	a	k8xC	a
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
zleva	zleva	k6eAd1	zleva
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
v	v	k7c6	v
Mělníku	Mělník	k1gInSc6	Mělník
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
Vltavy	Vltava	k1gFnSc2	Vltava
s	s	k7c7	s
přítoky	přítok	k1gInPc7	přítok
Malší	Malší	k1gNnSc2	Malší
<g/>
,	,	kIx,	,
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
,	,	kIx,	,
Otavou	Otava	k1gFnSc7	Otava
<g/>
,	,	kIx,	,
Sázavou	Sázava	k1gFnSc7	Sázava
a	a	k8xC	a
Berounkou	Berounka	k1gFnSc7	Berounka
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
přítokem	přítok	k1gInSc7	přítok
Vltava	Vltava	k1gFnSc1	Vltava
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
systém	systém	k1gInSc1	systém
odvodňující	odvodňující	k2eAgInSc1d1	odvodňující
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnPc4d1	celá
Čechy	Čechy	k1gFnPc4	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
28	[number]	k4	28
090	[number]	k4	090
km2	km2	k4	km2
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
27	[number]	k4	27
0	[number]	k4	0
<g/>
47,59	[number]	k4	47,59
km2	km2	k4	km2
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
"	"	kIx"	"
řeka	řeka	k1gFnSc1	řeka
dostala	dostat	k5eAaPmAgFnS	dostat
od	od	k7c2	od
starých	starý	k2eAgMnPc2d1	starý
Germánů	Germán	k1gMnPc2	Germán
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ji	on	k3xPp3gFnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
Wilt-ahwa	Wilthwa	k1gFnSc1	Wilt-ahwa
-	-	kIx~	-
divoká	divoký	k2eAgFnSc1d1	divoká
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Teplá	teplý	k2eAgFnSc1d1	teplá
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
hlavních	hlavní	k2eAgInPc2d1	hlavní
zdrojů	zdroj	k1gInPc2	zdroj
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
,	,	kIx,	,
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
svahu	svah	k1gInSc6	svah
Černé	Černé	k2eAgFnSc2d1	Černé
hory	hora	k1gFnSc2	hora
(	(	kIx(	(
<g/>
1	[number]	k4	1
315	[number]	k4	315
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
1	[number]	k4	1
172	[number]	k4	172
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
jako	jako	k8xC	jako
Černý	černý	k2eAgInSc1d1	černý
potok	potok	k1gInSc1	potok
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pramen	pramen	k1gInSc1	pramen
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
chráněné	chráněný	k2eAgNnSc1d1	chráněné
území	území	k1gNnSc1	území
Pramen	pramen	k1gInSc4	pramen
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
5	[number]	k4	5
km	km	kA	km
teče	téct	k5eAaImIp3nS	téct
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
od	od	k7c2	od
soutoku	soutok	k1gInSc2	soutok
s	s	k7c7	s
Kvildským	Kvildský	k2eAgInSc7d1	Kvildský
potokem	potok	k1gInSc7	potok
v	v	k7c6	v
Kvildě	Kvilda	k1gFnSc6	Kvilda
obrací	obracet	k5eAaImIp3nS	obracet
nadlouho	nadlouho	k6eAd1	nadlouho
svůj	svůj	k3xOyFgInSc4	svůj
tok	tok	k1gInSc4	tok
k	k	k7c3	k
jihovýchodu	jihovýchod	k1gInSc3	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
Vydřího	vydří	k2eAgInSc2d1	vydří
potoka	potok	k1gInSc2	potok
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Borová	borový	k2eAgFnSc1d1	Borová
Lada	Lada	k1gFnSc1	Lada
(	(	kIx(	(
<g/>
890	[number]	k4	890
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
nese	nést	k5eAaImIp3nS	nést
říčka	říčka	k1gFnSc1	říčka
název	název	k1gInSc4	název
Teplá	teplat	k5eAaImIp3nS	teplat
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
přibírá	přibírat	k5eAaImIp3nS	přibírat
Vltavský	vltavský	k2eAgInSc1d1	vltavský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
též	též	k9	též
jako	jako	k8xS	jako
Malá	malý	k2eAgFnSc1d1	malá
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Pláňském	Pláňský	k2eAgNnSc6d1	Pláňský
polesí	polesí	k1gNnSc6	polesí
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
1	[number]	k4	1
158	[number]	k4	158
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Protéká	protékat	k5eAaImIp3nS	protékat
Horní	horní	k2eAgFnSc7d1	horní
Vltavicí	Vltavice	k1gFnSc7	Vltavice
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
Lenoře	Lenora	k1gFnSc6	Lenora
(	(	kIx(	(
<g/>
756	[number]	k4	756
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
získává	získávat	k5eAaImIp3nS	získávat
vody	voda	k1gFnPc4	voda
Řasnice	Řasnice	k1gFnSc2	Řasnice
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnSc2d1	zvaná
také	také	k9	také
Travnatá	travnatý	k2eAgFnSc1d1	travnatá
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Lenorou	Lenora	k1gFnSc7	Lenora
má	mít	k5eAaImIp3nS	mít
řeka	řeka	k1gFnSc1	řeka
jen	jen	k9	jen
nepatrný	nepatrný	k2eAgInSc4d1	nepatrný
spád	spád	k1gInSc4	spád
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
v	v	k7c6	v
ploché	plochý	k2eAgFnSc6d1	plochá
krajině	krajina	k1gFnSc6	krajina
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
mokřady	mokřad	k1gInPc4	mokřad
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
meandry	meandr	k1gInPc7	meandr
<g/>
,	,	kIx,	,
souhrnně	souhrnně	k6eAd1	souhrnně
zvané	zvaný	k2eAgFnSc2d1	zvaná
Vltavský	vltavský	k2eAgInSc4d1	vltavský
luh	luh	k1gInSc4	luh
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
uprostřed	uprostřed	k7c2	uprostřed
1	[number]	k4	1
<g/>
.	.	kIx.	.
zóny	zóna	k1gFnSc2	zóna
NP	NP	kA	NP
Šumava	Šumava	k1gFnSc1	Šumava
Mrtvý	mrtvý	k1gMnSc1	mrtvý
luh	luh	k1gInSc1	luh
se	se	k3xPyFc4	se
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
731	[number]	k4	731
m	m	kA	m
u	u	k7c2	u
osady	osada	k1gFnSc2	osada
Chlum	chlum	k1gInSc1	chlum
stékají	stékat	k5eAaImIp3nP	stékat
Teplá	teplý	k2eAgFnSc1d1	teplá
a	a	k8xC	a
Studená	studený	k2eAgFnSc1d1	studená
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
hranic	hranice	k1gFnPc2	hranice
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
západně	západně	k6eAd1	západně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Haidmühle	Haidmühle	k1gFnSc2	Haidmühle
pod	pod	k7c7	pod
německým	německý	k2eAgInSc7d1	německý
názvem	název	k1gInSc7	název
Altwasser	Altwassra	k1gFnPc2	Altwassra
nebo	nebo	k8xC	nebo
Kalte	kalit	k5eAaImRp2nP	kalit
Moldau	Moldaa	k1gFnSc4	Moldaa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
soutoku	soutok	k1gInSc2	soutok
obou	dva	k4xCgInPc2	dva
hlavních	hlavní	k2eAgInPc2d1	hlavní
pramenných	pramenný	k2eAgInPc2d1	pramenný
toků	tok	k1gInPc2	tok
Teplé	Teplé	k2eAgFnSc2d1	Teplé
a	a	k8xC	a
Studené	Studené	k2eAgFnSc2d1	Studené
Vltavy	Vltava	k1gFnSc2	Vltava
pak	pak	k6eAd1	pak
řeka	řeka	k1gFnSc1	řeka
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
své	svůj	k3xOyFgFnSc2	svůj
cesty	cesta	k1gFnSc2	cesta
má	mít	k5eAaImIp3nS	mít
jméno	jméno	k1gNnSc1	jméno
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Nové	Nové	k2eAgFnSc2d1	Nové
Pece	Pec	k1gFnSc2	Pec
se	se	k3xPyFc4	se
Vltava	Vltava	k1gFnSc1	Vltava
rozlévá	rozlévat	k5eAaImIp3nS	rozlévat
do	do	k7c2	do
širokého	široký	k2eAgNnSc2d1	široké
a	a	k8xC	a
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
přehradního	přehradní	k2eAgNnSc2d1	přehradní
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
přehrazením	přehrazení	k1gNnSc7	přehrazení
jejího	její	k3xOp3gInSc2	její
toku	tok	k1gInSc2	tok
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Lipno	Lipno	k1gNnSc4	Lipno
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Lipnem	Lipno	k1gNnSc7	Lipno
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
romantickým	romantický	k2eAgNnSc7d1	romantické
skalnatým	skalnatý	k2eAgNnSc7d1	skalnaté
údolím	údolí	k1gNnSc7	údolí
pod	pod	k7c7	pod
Čertovou	čertový	k2eAgFnSc7d1	Čertová
stěnou	stěna	k1gFnSc7	stěna
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
její	její	k3xOp3gInSc1	její
úsek	úsek	k1gInSc1	úsek
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Čertovy	čertův	k2eAgInPc1d1	čertův
proudy	proud	k1gInPc1	proud
<g/>
)	)	kIx)	)
a	a	k8xC	a
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
vyrovnávací	vyrovnávací	k2eAgFnSc3d1	vyrovnávací
nádrži	nádrž	k1gFnSc3	nádrž
Lipno	Lipno	k1gNnSc4	Lipno
II	II	kA	II
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
Vyšším	vysoký	k2eAgInSc7d2	vyšší
Brodem	Brod	k1gInSc7	Brod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
mezi	mezi	k7c7	mezi
přehradní	přehradní	k2eAgFnSc7d1	přehradní
hrází	hráz	k1gFnSc7	hráz
Lipno	Lipno	k1gNnSc4	Lipno
a	a	k8xC	a
Vyšším	vysoký	k2eAgInSc7d2	vyšší
Brodem	Brod	k1gInSc7	Brod
je	být	k5eAaImIp3nS	být
koryto	koryto	k1gNnSc1	koryto
řeky	řeka	k1gFnSc2	řeka
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
přehrada	přehrada	k1gFnSc1	přehrada
je	být	k5eAaImIp3nS	být
povinna	povinen	k2eAgFnSc1d1	povinna
udržovat	udržovat	k5eAaImF	udržovat
minimální	minimální	k2eAgInSc4d1	minimální
průtok	průtok	k1gInSc4	průtok
2	[number]	k4	2
krychlové	krychlový	k2eAgInPc1d1	krychlový
metry	metr	k1gInPc1	metr
vody	voda	k1gFnSc2	voda
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
vod	voda	k1gFnPc2	voda
z	z	k7c2	z
Lipenského	lipenský	k2eAgNnSc2d1	Lipenské
jezera	jezero	k1gNnSc2	jezero
je	být	k5eAaImIp3nS	být
odváděna	odvádět	k5eAaImNgFnS	odvádět
kanálem	kanál	k1gInSc7	kanál
od	od	k7c2	od
podzemní	podzemní	k2eAgFnSc2d1	podzemní
elektrárny	elektrárna	k1gFnSc2	elektrárna
do	do	k7c2	do
vyrovnávací	vyrovnávací	k2eAgFnSc2d1	vyrovnávací
nádrže	nádrž	k1gFnSc2	nádrž
Lipno	Lipno	k1gNnSc1	Lipno
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
průtok	průtok	k1gInSc1	průtok
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
-	-	kIx~	-
buďto	buďto	k8xC	buďto
za	za	k7c2	za
přebytku	přebytek	k1gInSc2	přebytek
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
nádrži	nádrž	k1gFnSc6	nádrž
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
vodáckých	vodácký	k2eAgInPc2d1	vodácký
závodů	závod	k1gInPc2	závod
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
úsek	úsek	k1gInSc1	úsek
Čertových	čertový	k2eAgInPc2d1	čertový
proudů	proud	k1gInPc2	proud
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejtěžších	těžký	k2eAgFnPc2d3	nejtěžší
vodáckých	vodácký	k2eAgFnPc2d1	vodácká
a	a	k8xC	a
slalomových	slalomový	k2eAgFnPc2d1	slalomová
tras	trasa	k1gFnPc2	trasa
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
akce	akce	k1gFnPc1	akce
ovšem	ovšem	k9	ovšem
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
průtok	průtok	k1gInSc1	průtok
20	[number]	k4	20
-	-	kIx~	-
30	[number]	k4	30
m	m	kA	m
<g/>
3	[number]	k4	3
vody	voda	k1gFnSc2	voda
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
,	,	kIx,	,
vodácký	vodácký	k2eAgInSc1d1	vodácký
úsek	úsek	k1gInSc1	úsek
Vyšší	vysoký	k2eAgInSc1d2	vyšší
Brod	Brod	k1gInSc1	Brod
-	-	kIx~	-
Boršov	Boršov	k1gInSc1	Boršov
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Vyšším	vysoký	k2eAgInSc7d2	vyšší
Brodem	Brod	k1gInSc7	Brod
protéká	protékat	k5eAaImIp3nS	protékat
Vltava	Vltava	k1gFnSc1	Vltava
otevřenější	otevřený	k2eAgFnSc1d2	otevřenější
krajinou	krajina	k1gFnSc7	krajina
a	a	k8xC	a
stáčí	stáčet	k5eAaImIp3nP	stáčet
svůj	svůj	k3xOyFgInSc4	svůj
tok	tok	k1gInSc4	tok
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
.	.	kIx.	.
</s>
<s>
Četnými	četný	k2eAgInPc7d1	četný
zákruty	zákrut	k1gInPc7	zákrut
protéká	protékat	k5eAaImIp3nS	protékat
turisticky	turisticky	k6eAd1	turisticky
atraktivní	atraktivní	k2eAgFnSc7d1	atraktivní
oblastí	oblast	k1gFnSc7	oblast
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
kulturně	kulturně	k6eAd1	kulturně
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
tok	tok	k1gInSc1	tok
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
hradu	hrad	k1gInSc3	hrad
Rožmberk	Rožmberk	k1gInSc1	Rožmberk
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
městečkem	městečko	k1gNnSc7	městečko
Větřní	Větřní	k2eAgFnSc2d1	Větřní
a	a	k8xC	a
skalnatým	skalnatý	k2eAgNnSc7d1	skalnaté
údolím	údolí	k1gNnSc7	údolí
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
Českého	český	k2eAgInSc2d1	český
Krumlova	Krumlov	k1gInSc2	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
Vltavy	Vltava	k1gFnSc2	Vltava
směřuje	směřovat	k5eAaImIp3nS	směřovat
odtud	odtud	k6eAd1	odtud
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
kolem	kolem	k7c2	kolem
kláštera	klášter	k1gInSc2	klášter
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Koruna	koruna	k1gFnSc1	koruna
<g/>
,	,	kIx,	,
míjí	míjet	k5eAaImIp3nS	míjet
bývalé	bývalý	k2eAgNnSc1d1	bývalé
keltské	keltský	k2eAgNnSc1d1	keltské
oppidum	oppidum	k1gNnSc1	oppidum
Třísov	Třísovo	k1gNnPc2	Třísovo
a	a	k8xC	a
obtéká	obtékat	k5eAaImIp3nS	obtékat
skalnatý	skalnatý	k2eAgInSc4d1	skalnatý
ostroh	ostroh	k1gInSc4	ostroh
se	s	k7c7	s
zříceninou	zřícenina	k1gFnSc7	zřícenina
hradu	hrad	k1gInSc2	hrad
Dívčí	dívčí	k2eAgFnSc1d1	dívčí
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
již	již	k6eAd1	již
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
rovinatou	rovinatý	k2eAgFnSc7d1	rovinatá
Českobudějovickou	českobudějovický	k2eAgFnSc7d1	českobudějovická
pánví	pánev	k1gFnSc7	pánev
k	k	k7c3	k
Českým	český	k2eAgInPc3d1	český
Budějovicím	Budějovice	k1gInPc3	Budějovice
(	(	kIx(	(
<g/>
řkm	řkm	k?	řkm
239,6	[number]	k4	239,6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
přibírá	přibírat	k5eAaImIp3nS	přibírat
vody	voda	k1gFnSc2	voda
řeky	řeka	k1gFnSc2	řeka
Malše	Malše	k1gFnSc2	Malše
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
městem	město	k1gNnSc7	město
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
tok	tok	k1gInSc1	tok
Vltavy	Vltava	k1gFnSc2	Vltava
kolem	kolem	k7c2	kolem
obce	obec	k1gFnSc2	obec
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
a	a	k8xC	a
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
Hněvkovické	Hněvkovický	k2eAgFnSc3d1	Hněvkovická
přehradě	přehrada	k1gFnSc3	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
vodu	voda	k1gFnSc4	voda
pro	pro	k7c4	pro
Jadernou	jaderný	k2eAgFnSc4d1	jaderná
elektrárnu	elektrárna	k1gFnSc4	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
až	až	k9	až
za	za	k7c4	za
Prahu	Praha	k1gFnSc4	Praha
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
řeka	řeka	k1gFnSc1	řeka
hluboké	hluboký	k2eAgFnSc2d1	hluboká
a	a	k8xC	a
úzké	úzký	k2eAgNnSc4d1	úzké
údolí	údolí	k1gNnSc4	údolí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
jen	jen	k9	jen
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
Týnem	Týn	k1gInSc7	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
a	a	k8xC	a
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
přibírá	přibírat	k5eAaImIp3nS	přibírat
vody	voda	k1gFnSc2	voda
řeky	řeka	k1gFnSc2	řeka
Lužnice	Lužnice	k1gFnSc2	Lužnice
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
k	k	k7c3	k
Týnu	Týn	k1gInSc3	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
však	však	k9	však
již	již	k6eAd1	již
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
hladina	hladina	k1gFnSc1	hladina
dalšího	další	k2eAgInSc2d1	další
stupně	stupeň	k1gInSc2	stupeň
Vltavské	vltavský	k2eAgFnSc2d1	Vltavská
kaskády	kaskáda	k1gFnSc2	kaskáda
<g/>
,	,	kIx,	,
Orlické	orlický	k2eAgFnSc2d1	Orlická
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgFnSc6d1	pojmenovaná
podle	podle	k7c2	podle
známého	známý	k2eAgInSc2d1	známý
zámku	zámek	k1gInSc2	zámek
Orlík	orlík	k1gMnSc1	orlík
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
pohltilo	pohltit	k5eAaPmAgNnS	pohltit
i	i	k9	i
soutok	soutok	k1gInSc4	soutok
Vltavy	Vltava	k1gFnSc2	Vltava
s	s	k7c7	s
Otavou	Otava	k1gFnSc7	Otava
u	u	k7c2	u
hradu	hrad	k1gInSc2	hrad
Zvíkova	Zvíkov	k1gInSc2	Zvíkov
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
hrází	hráz	k1gFnSc7	hráz
Orlické	orlický	k2eAgFnSc2d1	Orlická
přehrady	přehrada	k1gFnSc2	přehrada
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
tok	tok	k1gInSc1	tok
Vltavy	Vltava	k1gFnSc2	Vltava
dále	daleko	k6eAd2	daleko
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
před	před	k7c7	před
Prahou	Praha	k1gFnSc7	Praha
zadržují	zadržovat	k5eAaImIp3nP	zadržovat
vody	voda	k1gFnPc1	voda
Vltavy	Vltava	k1gFnSc2	Vltava
ještě	ještě	k6eAd1	ještě
další	další	k2eAgFnPc4d1	další
čtyři	čtyři	k4xCgFnPc4	čtyři
přehrady	přehrada	k1gFnPc4	přehrada
Vltavské	vltavský	k2eAgFnSc2d1	Vltavská
kaskády	kaskáda	k1gFnSc2	kaskáda
-	-	kIx~	-
Kamýcká	kamýcký	k2eAgFnSc1d1	Kamýcká
<g/>
,	,	kIx,	,
Slapská	slapský	k2eAgFnSc1d1	Slapská
<g/>
,	,	kIx,	,
Štěchovická	štěchovický	k2eAgFnSc1d1	Štěchovická
a	a	k8xC	a
Vranská	vranský	k2eAgFnSc1d1	Vranská
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Davle	Davl	k1gMnSc2	Davl
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Ostrov	ostrov	k1gInSc1	ostrov
sv.	sv.	kA	sv.
Kiliána	Kilián	k1gMnSc2	Kilián
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1419	[number]	k4	1419
stával	stávat	k5eAaImAgInS	stávat
Ostrovský	ostrovský	k2eAgInSc1d1	ostrovský
klášter	klášter	k1gInSc1	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
se	se	k3xPyFc4	se
zprava	zprava	k6eAd1	zprava
vlévá	vlévat	k5eAaImIp3nS	vlévat
řeka	řeka	k1gFnSc1	řeka
Sázava	Sázava	k1gFnSc1	Sázava
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Prahy	Praha	k1gFnSc2	Praha
za	za	k7c7	za
Zbraslaví	Zbraslav	k1gFnSc7	Zbraslav
se	se	k3xPyFc4	se
zleva	zleva	k6eAd1	zleva
připojuje	připojovat	k5eAaImIp3nS	připojovat
řeka	řeka	k1gFnSc1	řeka
Berounka	Berounka	k1gFnSc1	Berounka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
průtoku	průtok	k1gInSc6	průtok
hlavním	hlavní	k2eAgInSc6d1	hlavní
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
Vltava	Vltava	k1gFnSc1	Vltava
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
Kralupech	Kralupy	k1gInPc6	Kralupy
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
vymaňuje	vymaňovat	k5eAaImIp3nS	vymaňovat
z	z	k7c2	z
úzkého	úzký	k2eAgNnSc2d1	úzké
údolí	údolí	k1gNnSc2	údolí
do	do	k7c2	do
roviny	rovina	k1gFnSc2	rovina
a	a	k8xC	a
u	u	k7c2	u
Mělníka	Mělník	k1gInSc2	Mělník
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
156	[number]	k4	156
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
Vltava	Vltava	k1gFnSc1	Vltava
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
soutoku	soutok	k1gInSc2	soutok
delší	dlouhý	k2eAgFnSc3d2	delší
a	a	k8xC	a
vodnější	vodný	k2eAgFnSc3d2	vodný
<g/>
,	,	kIx,	,
nese	nést	k5eAaImIp3nS	nést
tok	tok	k1gInSc1	tok
dále	daleko	k6eAd2	daleko
oproti	oproti	k7c3	oproti
zvyklostem	zvyklost	k1gFnPc3	zvyklost
jméno	jméno	k1gNnSc4	jméno
Labe	Labe	k1gNnSc4	Labe
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
protéká	protékat	k5eAaImIp3nS	protékat
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
několika	několik	k4yIc7	několik
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
odtékala	odtékat	k5eAaImAgFnS	odtékat
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
Vltavy	Vltava	k1gFnSc2	Vltava
do	do	k7c2	do
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
levý	levý	k2eAgInSc4d1	levý
/	/	kIx~	/
pravý	pravý	k2eAgInSc4d1	pravý
<g/>
,	,	kIx,	,
říční	říční	k2eAgInSc4d1	říční
kilometr	kilometr	k1gInSc4	kilometr
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
<g/>
)	)	kIx)	)
Kvildský	Kvildský	k2eAgInSc1d1	Kvildský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
424,1	[number]	k4	424,1
<g/>
)	)	kIx)	)
Bučina	bučina	k1gFnSc1	bučina
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
422,6	[number]	k4	422,6
<g/>
)	)	kIx)	)
Vydří	vydří	k2eAgInSc1d1	vydří
potok	potok	k1gInSc1	potok
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
416,8	[number]	k4	416,8
<g/>
)	)	kIx)	)
Vltavský	vltavský	k2eAgInSc1d1	vltavský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
Malá	malý	k2eAgFnSc1d1	malá
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
416,0	[number]	k4	416,0
<g/>
)	)	kIx)	)
Račí	račí	k2eAgInSc1d1	račí
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
404,5	[number]	k4	404,5
<g/>
)	)	kIx)	)
Kubohuťský	Kubohuťský	k2eAgInSc1d1	Kubohuťský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
402,8	[number]	k4	402,8
<g/>
)	)	kIx)	)
Kaplický	Kaplický	k2eAgInSc1d1	Kaplický
potok	potok	k1gInSc1	potok
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
396,5	[number]	k4	396,5
<g/>
)	)	kIx)	)
Řasnice	Řasnice	k1gFnSc1	Řasnice
(	(	kIx(	(
<g/>
Travnatá	travnatý	k2eAgFnSc1d1	travnatá
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
394,3	[number]	k4	394,3
<g/>
)	)	kIx)	)
Volarský	Volarský	k2eAgInSc1d1	Volarský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
378,9	[number]	k4	378,9
<g/>
)	)	kIx)	)
Studená	studený	k2eAgFnSc1d1	studená
Vltava	Vltava	k1gFnSc1	Vltava
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
376,7	[number]	k4	376,7
<g/>
)	)	kIx)	)
Jezerní	jezerní	k2eAgInSc1d1	jezerní
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Olšina	olšina	k1gFnSc1	olšina
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
Větší	veliký	k2eAgFnSc1d2	veliký
Vltavice	Vltavice	k1gFnSc1	Vltavice
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
314,5	[number]	k4	314,5
<g/>
)	)	kIx)	)
Branná	branný	k2eAgFnSc1d1	Branná
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
297,9	[number]	k4	297,9
<g/>
)	)	kIx)	)
Polečnice	Polečnice	k1gFnSc2	Polečnice
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
281,3	[number]	k4	281,3
<g/>
)	)	kIx)	)
Jílecký	Jílecký	k2eAgInSc1d1	Jílecký
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Křemžský	Křemžský	k2eAgInSc1d1	Křemžský
potok	potok	k1gInSc1	potok
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Křemže	Křemž	k1gFnPc4	Křemž
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
258,6	[number]	k4	258,6
<g/>
)	)	kIx)	)
Malše	Malš	k1gInSc2	Malš
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
240,0	[number]	k4	240,0
<g/>
)	)	kIx)	)
Dehtářský	Dehtářský	k2eAgInSc1d1	Dehtářský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
231,9	[number]	k4	231,9
<g/>
)	)	kIx)	)
Bezdrevský	bezdrevský	k2eAgInSc1d1	bezdrevský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
231,0	[number]	k4	231,0
<g/>
)	)	kIx)	)
Lužnice	Lužnice	k1gFnSc1	Lužnice
(	(	kIx(	(
<g/>
P	P	kA	P
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
202,2	[number]	k4	202,2
<g/>
)	)	kIx)	)
Hrejkovický	Hrejkovický	k2eAgInSc1d1	Hrejkovický
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Otava	Otava	k1gFnSc1	Otava
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
169,1	[number]	k4	169,1
<g/>
)	)	kIx)	)
Brzina	Brzina	k1gMnSc1	Brzina
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
126,9	[number]	k4	126,9
<g/>
)	)	kIx)	)
Musík	Musík	k1gMnSc1	Musík
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
106,8	[number]	k4	106,8
<g/>
)	)	kIx)	)
Mastník	Mastník	k1gMnSc1	Mastník
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
<g />
.	.	kIx.	.
</s>
<s>
104,6	[number]	k4	104,6
<g/>
)	)	kIx)	)
Kocába	kocába	k1gFnSc1	kocába
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
82,8	[number]	k4	82,8
<g/>
)	)	kIx)	)
Sázava	Sázava	k1gFnSc1	Sázava
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
78,3	[number]	k4	78,3
<g/>
)	)	kIx)	)
Bojovský	Bojovský	k2eAgInSc1d1	Bojovský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
75,2	[number]	k4	75,2
<g/>
)	)	kIx)	)
Berounka	Berounka	k1gFnSc1	Berounka
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
63,4	[number]	k4	63,4
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Seznam	seznam	k1gInSc4	seznam
řek	řeka	k1gFnPc2	řeka
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
potoků	potok	k1gInPc2	potok
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
Botič	Botič	k1gInSc1	Botič
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
55,2	[number]	k4	55,2
<g/>
)	)	kIx)	)
Rokytka	Rokytka	k1gMnSc1	Rokytka
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
47,4	[number]	k4	47,4
<g/>
)	)	kIx)	)
Litovický	Litovický	k2eAgInSc1d1	Litovický
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
Šárecký	šárecký	k2eAgInSc1d1	šárecký
potok	potok	k1gInSc1	potok
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
řkm	řkm	k?	řkm
42,7	[number]	k4	42,7
<g/>
)	)	kIx)	)
Zákolanský	Zákolanský	k2eAgInSc1d1	Zákolanský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Bakovský	Bakovský	k2eAgInSc1d1	Bakovský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
Vybrané	vybraný	k2eAgInPc4d1	vybraný
hlásné	hlásný	k2eAgInPc4d1	hlásný
profily	profil	k1gInPc4	profil
<g/>
:	:	kIx,	:
řkm	řkm	k?	řkm
329,54	[number]	k4	329,54
–	–	k?	–
Přehrada	přehrada	k1gFnSc1	přehrada
Lipno	Lipno	k1gNnSc1	Lipno
I	i	k8xC	i
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plocha	plocha	k1gFnSc1	plocha
nádrže	nádrž	k1gFnSc2	nádrž
48,7	[number]	k4	48,7
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
725,6	[number]	k4	725,6
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
řkm	řkm	k?	řkm
319,12	[number]	k4	319,12
–	–	k?	–
Přehrada	přehrada	k1gFnSc1	přehrada
Lipno	Lipno	k1gNnSc1	Lipno
II	II	kA	II
řkm	řkm	k?	řkm
233,00	[number]	k4	233,00
–	–	k?	–
jez	jez	k1gInSc1	jez
České	český	k2eAgFnSc2d1	Česká
Vrbné	Vrbná	k1gFnSc2	Vrbná
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
postavena	postaven	k2eAgFnSc1d1	postavena
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
vorové	vorový	k2eAgFnSc2d1	vorová
propusti	propust	k1gFnSc2	propust
postavena	postaven	k2eAgFnSc1d1	postavena
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
nová	nový	k2eAgFnSc1d1	nová
slalomová	slalomový	k2eAgFnSc1d1	slalomová
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
řkm	řkm	k?	řkm
228,80	[number]	k4	228,80
–	–	k?	–
jez	jez	k1gInSc1	jez
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
řkm	řkm	k?	řkm
210,39	[number]	k4	210,39
–	–	k?	–
Přehrada	přehrada	k1gFnSc1	přehrada
Hněvkovice	Hněvkovice	k1gFnSc1	Hněvkovice
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
a	a	k8xC	a
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
plocha	plocha	k1gFnSc1	plocha
nádrže	nádrž	k1gFnSc2	nádrž
2,68	[number]	k4	2,68
–	–	k?	–
3,21	[number]	k4	3,21
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
řkm	řkm	k?	řkm
208,90	[number]	k4	208,90
–	–	k?	–
starý	starý	k2eAgInSc4d1	starý
jez	jez	k1gInSc4	jez
Hněvkovice	Hněvkovice	k1gFnSc2	Hněvkovice
<g/>
,	,	kIx,	,
354,79	[number]	k4	354,79
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
<g/>
,	,	kIx,	,
provedena	proveden	k2eAgFnSc1d1	provedena
modernizace	modernizace	k1gFnSc1	modernizace
jezu	jez	k1gInSc2	jez
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
plavební	plavební	k2eAgFnSc2d1	plavební
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Uvedeno	uvést	k5eAaPmNgNnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
řkm	řkm	k?	řkm
200,41	[number]	k4	200,41
–	–	k?	–
Přehrada	přehrada	k1gFnSc1	přehrada
Kořensko	Kořensko	k1gNnSc4	Kořensko
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
dokončena	dokončen	k2eAgFnSc1d1	dokončena
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2016-2017	[number]	k4	2016-2017
komora	komora	k1gFnSc1	komora
modernizována	modernizován	k2eAgFnSc1d1	modernizována
<g/>
.	.	kIx.	.
řkm	řkm	k?	řkm
159,90	[number]	k4	159,90
–	–	k?	–
Žďákovský	Žďákovský	k2eAgInSc4d1	Žďákovský
most	most	k1gInSc4	most
řkm	řkm	k?	řkm
144,70	[number]	k4	144,70
–	–	k?	–
Přehrada	přehrada	k1gFnSc1	přehrada
Orlík	orlík	k1gMnSc1	orlík
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plavební	plavební	k2eAgNnSc1d1	plavební
zařízení	zařízení	k1gNnSc1	zařízení
pro	pro	k7c4	pro
velká	velký	k2eAgNnPc4d1	velké
plavidla	plavidlo	k1gNnPc4	plavidlo
(	(	kIx(	(
<g/>
šikmé	šikmý	k2eAgNnSc4d1	šikmé
kolejové	kolejový	k2eAgNnSc4d1	kolejové
zdvihadlo	zdvihadlo	k1gNnSc4	zdvihadlo
doplněné	doplněná	k1gFnSc2	doplněná
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
plavební	plavební	k2eAgFnSc7d1	plavební
komorou	komora	k1gFnSc7	komora
<g/>
)	)	kIx)	)
nedokončeno	dokončen	k2eNgNnSc1d1	nedokončeno
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
šikmé	šikmý	k2eAgNnSc1d1	šikmé
kolejové	kolejový	k2eAgNnSc1d1	kolejové
zdvihadlo	zdvihadlo	k1gNnSc1	zdvihadlo
pro	pro	k7c4	pro
sportovní	sportovní	k2eAgNnPc4d1	sportovní
plavidla	plavidlo	k1gNnPc4	plavidlo
do	do	k7c2	do
3,5	[number]	k4	3,5
t	t	k?	t
<g/>
;	;	kIx,	;
plocha	plocha	k1gFnSc1	plocha
nádrže	nádrž	k1gFnSc2	nádrž
27,3	[number]	k4	27,3
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
hladina	hladina	k1gFnSc1	hladina
max	max	kA	max
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
354	[number]	k4	354
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
min	min	kA	min
<g/>
.	.	kIx.	.
347,6	[number]	k4	347,6
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
řkm	řkm	k?	řkm
134,73	[number]	k4	134,73
–	–	k?	–
Přehrada	přehrada	k1gFnSc1	přehrada
Kamýk	Kamýk	k1gInSc1	Kamýk
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
plavební	plavební	k2eAgFnSc7d1	plavební
komorou	komora	k1gFnSc7	komora
řkm	řkm	k?	řkm
91,69	[number]	k4	91,69
–	–	k?	–
Přehrada	přehrada	k1gFnSc1	přehrada
Slapy	slap	k1gInPc4	slap
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výtah	výtah	k1gInSc4	výtah
pro	pro	k7c4	pro
<g />
.	.	kIx.	.
</s>
<s>
lodě	loď	k1gFnPc1	loď
(	(	kIx(	(
<g/>
svislé	svislý	k2eAgNnSc1d1	svislé
nebo	nebo	k8xC	nebo
šikmé	šikmý	k2eAgNnSc1d1	šikmé
zdvihadlo	zdvihadlo	k1gNnSc1	zdvihadlo
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
dokončen	dokončen	k2eAgMnSc1d1	dokončen
<g/>
,	,	kIx,	,
plocha	plocha	k1gFnSc1	plocha
nádrže	nádrž	k1gFnSc2	nádrž
13,92	[number]	k4	13,92
km	km	kA	km
<g/>
2	[number]	k4	2
řkm	řkm	k?	řkm
84,44	[number]	k4	84,44
–	–	k?	–
Přehrada	přehrada	k1gFnSc1	přehrada
Štěchovice	Štěchovice	k1gFnPc1	Štěchovice
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
plavební	plavební	k2eAgFnSc7d1	plavební
komorou	komora	k1gFnSc7	komora
délkově	délkově	k6eAd1	délkově
přizpůsobenou	přizpůsobený	k2eAgFnSc7d1	přizpůsobená
pro	pro	k7c4	pro
plavení	plavení	k1gNnSc4	plavení
vorů	vor	k1gInPc2	vor
<g/>
,	,	kIx,	,
plocha	plocha	k1gFnSc1	plocha
nádrže	nádrž	k1gFnSc2	nádrž
1,14	[number]	k4	1,14
km	km	kA	km
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
řkm	řkm	k?	řkm
71,33	[number]	k4	71,33
–	–	k?	–
Přehrada	přehrada	k1gFnSc1	přehrada
Vrané	vraný	k2eAgFnPc1d1	Vraná
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
s	s	k7c7	s
plavební	plavební	k2eAgFnSc7d1	plavební
komorou	komora	k1gFnSc7	komora
<g/>
,	,	kIx,	,
plocha	plocha	k1gFnSc1	plocha
nádrže	nádrž	k1gFnSc2	nádrž
2,51	[number]	k4	2,51
km	km	kA	km
<g/>
2	[number]	k4	2
řkm	řkm	k?	řkm
62,90	[number]	k4	62,90
–	–	k?	–
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
Modřany	Modřany	k1gInPc1	Modřany
(	(	kIx(	(
<g/>
jez	jez	k1gInSc1	jez
Modřany	Modřany	k1gInPc1	Modřany
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
53,70	[number]	k4	53,70
–	–	k?	–
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
Smíchov	Smíchov	k1gInSc1	Smíchov
(	(	kIx(	(
<g/>
oddělený	oddělený	k2eAgInSc4d1	oddělený
kanál	kanál	k1gInSc4	kanál
<g />
.	.	kIx.	.
</s>
<s>
od	od	k7c2	od
Šítkovského	Šítkovský	k2eAgInSc2d1	Šítkovský
jezu	jez	k1gInSc2	jez
k	k	k7c3	k
jezu	jez	k1gInSc3	jez
u	u	k7c2	u
Sovových	Sovových	k2eAgInPc2d1	Sovových
mlýnů	mlýn	k1gInPc2	mlýn
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
50,50	[number]	k4	50,50
–	–	k?	–
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
Štvanice	Štvanice	k1gFnSc1	Štvanice
(	(	kIx(	(
<g/>
jez	jez	k1gInSc1	jez
u	u	k7c2	u
začátku	začátek	k1gInSc2	začátek
ostrova	ostrov	k1gInSc2	ostrov
Štvanice	Štvanice	k1gFnSc2	Štvanice
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
43,50	[number]	k4	43,50
–	–	k?	–
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
Podbaba	Podbaba	k1gFnSc1	Podbaba
<g/>
,	,	kIx,	,
plavební	plavební	k2eAgInSc1d1	plavební
kanál	kanál	k1gInSc1	kanál
Podbaba	Podbaba	k1gFnSc1	Podbaba
–	–	k?	–
Troja	Troja	k1gFnSc1	Troja
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
3,5	[number]	k4	3,5
km	km	kA	km
(	(	kIx(	(
<g/>
Trojský	trojský	k2eAgInSc4d1	trojský
jez	jez	k1gInSc4	jez
a	a	k8xC	a
sportovní	sportovní	k2eAgInSc4d1	sportovní
kanál	kanál	k1gInSc4	kanál
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
35,90	[number]	k4	35,90
–	–	k?	–
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
Roztoky	roztoka	k1gFnSc2	roztoka
<g/>
,	,	kIx,	,
plavební	plavební	k2eAgInSc1d1	plavební
kanál	kanál	k1gInSc1	kanál
Roztoky	roztoka	k1gFnSc2	roztoka
–	–	k?	–
Klecany	Klecana	k1gFnSc2	Klecana
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
1,4	[number]	k4	1,4
km	km	kA	km
(	(	kIx(	(
<g/>
jez	jez	k1gInSc1	jez
Klecany	Klecana	k1gFnSc2	Klecana
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
26,70	[number]	k4	26,70
–	–	k?	–
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
Dolánky	Dolánka	k1gFnSc2	Dolánka
(	(	kIx(	(
<g/>
jez	jez	k1gInSc1	jez
Dolany	Dolany	k1gInPc1	Dolany
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
17,90	[number]	k4	17,90
–	–	k?	–
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
Miřejovice	Miřejovice	k1gFnSc1	Miřejovice
(	(	kIx(	(
<g/>
jez	jez	k1gInSc1	jez
Nelahozeves	Nelahozeves	k1gInSc1	Nelahozeves
–	–	k?	–
Veltrusy	Veltrusy	k1gInPc4	Veltrusy
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
0	[number]	k4	0
<g/>
0,90	[number]	k4	0,90
–	–	k?	–
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
Hořín	Hořín	k1gInSc1	Hořín
<g/>
,	,	kIx,	,
plavební	plavební	k2eAgInSc1d1	plavební
kanál	kanál	k1gInSc1	kanál
Mělník	Mělník	k1gInSc1	Mělník
–	–	k?	–
Vraňany	Vraňan	k1gMnPc7	Vraňan
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
10,1	[number]	k4	10,1
km	km	kA	km
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
,	,	kIx,	,
vodácký	vodácký	k2eAgInSc1d1	vodácký
úsek	úsek	k1gInSc1	úsek
Vyšší	vysoký	k2eAgInSc1d2	vyšší
Brod	Brod	k1gInSc1	Brod
-	-	kIx~	-
Boršov	Boršov	k1gInSc1	Boršov
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Vodácky	vodácky	k6eAd1	vodácky
nejvyužívanější	využívaný	k2eAgFnSc1d3	nejvyužívanější
je	být	k5eAaImIp3nS	být
Vltava	Vltava	k1gFnSc1	Vltava
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
z	z	k7c2	z
Vyššího	vysoký	k2eAgInSc2d2	vyšší
Brodu	Brod	k1gInSc2	Brod
do	do	k7c2	do
Boršova	Boršův	k2eAgNnSc2d1	Boršův
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
řeky	řeka	k1gFnSc2	řeka
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
starogermánského	starogermánský	k2eAgNnSc2d1	starogermánské
slovního	slovní	k2eAgNnSc2d1	slovní
spojení	spojení	k1gNnSc2	spojení
"	"	kIx"	"
<g/>
Wilth	Wilth	k1gInSc1	Wilth
<g/>
"	"	kIx"	"
<g/>
-	-	kIx~	-
<g/>
"	"	kIx"	"
<g/>
ahwa	ahwa	k6eAd1	ahwa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
divoká	divoký	k2eAgFnSc1d1	divoká
<g/>
,	,	kIx,	,
dravá	dravý	k2eAgFnSc1d1	dravá
voda	voda	k1gFnSc1	voda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Fuldských	Fuldský	k2eAgInPc6d1	Fuldský
análech	anály	k1gInPc6	anály
z	z	k7c2	z
roku	rok	k1gInSc2	rok
872	[number]	k4	872
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
doložen	doložit	k5eAaPmNgInS	doložit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Fuldaha	Fuldaha	k1gFnSc1	Fuldaha
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
srov.	srov.	kA	srov.
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Fulda	Fulda	k1gMnSc1	Fulda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1113	[number]	k4	1113
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Wultha	Wultha	k1gFnSc1	Wultha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
kronice	kronika	k1gFnSc6	kronika
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1125	[number]	k4	1125
poprvé	poprvé	k6eAd1	poprvé
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
počeštěné	počeštěný	k2eAgFnSc6d1	počeštěná
podobě	podoba	k1gFnSc6	podoba
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Wlitaua	Wlitaua	k1gMnSc1	Wlitaua
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
