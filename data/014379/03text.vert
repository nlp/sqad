<s>
Qubit	Qubit	k1gMnSc1
</s>
<s>
Kvantový	kvantový	k2eAgInSc1d1
bit	bit	k1gInSc1
neboli	neboli	k8xC
qubit	qubit	k5eAaImF,k5eAaPmF,k5eAaBmF
(	(	kIx(
<g/>
čti	číst	k5eAaImRp2nS
kjúbit	kjúbit	k5eAaImF,k5eAaPmF
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
kvantové	kvantový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
odvozená	odvozený	k2eAgFnSc1d1
od	od	k7c2
klasického	klasický	k2eAgInSc2d1
bitu	bit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jeden	jeden	k4xCgInSc4
ze	z	k7c2
stěžejních	stěžejní	k2eAgInPc2d1
pojmů	pojem	k1gInPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
kvantových	kvantový	k2eAgMnPc2d1
počítačů	počítač	k1gMnPc2
a	a	k8xC
kvantově	kvantově	k6eAd1
informačních	informační	k2eAgInPc2d1
protokolů	protokol	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Qubit	Qubit	k1gInSc1
je	být	k5eAaImIp3nS
implementován	implementovat	k5eAaImNgInS
kvantovým	kvantový	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
může	moct	k5eAaImIp3nS
nabývat	nabývat	k5eAaImF
dvou	dva	k4xCgFnPc2
stavů	stav	k1gInPc2
<g/>
:	:	kIx,
například	například	k6eAd1
foton	foton	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
polarizovaný	polarizovaný	k2eAgInSc1d1
vodorovně	vodorovně	k6eAd1
či	či	k8xC
svisle	svisle	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bit	bit	k1gInSc1
v	v	k7c6
klasickém	klasický	k2eAgInSc6d1
systému	systém	k1gInSc6
může	moct	k5eAaImIp3nS
nabývat	nabývat	k5eAaImF
jedné	jeden	k4xCgFnSc2
ze	z	k7c2
dvou	dva	k4xCgFnPc2
hodnot	hodnota	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
kvantová	kvantový	k2eAgFnSc1d1
mechanika	mechanika	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
stav	stav	k1gInSc1
qubitu	qubita	k1gMnSc4
byl	být	k5eAaImAgInS
jistou	jistý	k2eAgFnSc7d1
kombinací	kombinace	k1gFnSc7
obou	dva	k4xCgFnPc2
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
qubit	qubit	k1gInSc4
zavedl	zavést	k5eAaPmAgInS
B.	B.	kA
Schumacher	Schumachra	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Motivace	motivace	k1gFnSc1
</s>
<s>
V	v	k7c6
klasické	klasický	k2eAgFnSc6d1
počítačové	počítačový	k2eAgFnSc6d1
vědě	věda	k1gFnSc6
se	se	k3xPyFc4
pracuje	pracovat	k5eAaImIp3nS
s	s	k7c7
jednotkou	jednotka	k1gFnSc7
informace	informace	k1gFnSc2
bitem	bit	k1gInSc7
jako	jako	k8xC,k8xS
s	s	k7c7
abstraktním	abstraktní	k2eAgInSc7d1
pojmem	pojem	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhlíží	odhlížet	k5eAaImIp3nS
se	se	k3xPyFc4
přitom	přitom	k6eAd1
od	od	k7c2
jeho	jeho	k3xOp3gFnSc2
fyzikální	fyzikální	k2eAgFnSc2d1
implementace	implementace	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
zvolit	zvolit	k5eAaPmF
při	při	k7c6
konkrétní	konkrétní	k2eAgFnSc6d1
realizaci	realizace	k1gFnSc6
počítače	počítač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
abstraktní	abstraktní	k2eAgInSc1d1
přístup	přístup	k1gInSc1
nám	my	k3xPp1nPc3
umožňuje	umožňovat	k5eAaImIp3nS
zaměřit	zaměřit	k5eAaPmF
se	se	k3xPyFc4
čistě	čistě	k6eAd1
na	na	k7c4
vlastnosti	vlastnost	k1gFnPc4
informace	informace	k1gFnPc4
a	a	k8xC
rozvíjet	rozvíjet	k5eAaImF
koncept	koncept	k1gInSc4
počítání	počítání	k1gNnSc2
v	v	k7c6
dostatečné	dostatečný	k2eAgFnSc6d1
obecnosti	obecnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
třeba	třeba	k6eAd1
v	v	k7c6
žádném	žádný	k3yNgInSc6
případě	případ	k1gInSc6
uvažovat	uvažovat	k5eAaImF
fyziku	fyzika	k1gFnSc4
výpočetního	výpočetní	k2eAgInSc2d1
procesu	proces	k1gInSc2
či	či	k8xC
způsobu	způsob	k1gInSc2
uložení	uložení	k1gNnSc2
informace	informace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejme	podívat	k5eAaImRp1nP,k5eAaPmRp1nP
se	se	k3xPyFc4
ale	ale	k8xC
na	na	k7c4
problém	problém	k1gInSc4
z	z	k7c2
fyzikálního	fyzikální	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bity	bit	k1gInPc7
jsou	být	k5eAaImIp3nP
vždy	vždy	k6eAd1
reprezentovány	reprezentován	k2eAgFnPc1d1
stavem	stav	k1gInSc7
nějakého	nějaký	k3yIgInSc2
fyzikálního	fyzikální	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
už	už	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
pevný	pevný	k2eAgInSc4d1
disk	disk	k1gInSc4
počítače	počítač	k1gInSc2
<g/>
,	,	kIx,
různá	různý	k2eAgNnPc4d1
záznamová	záznamový	k2eAgNnPc4d1
media	medium	k1gNnPc4
či	či	k8xC
zpracovávací	zpracovávací	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
práci	práce	k1gFnSc6
s	s	k7c7
bity	bit	k1gInPc7
například	například	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
uvolňování	uvolňování	k1gNnSc3
tepla	teplo	k1gNnSc2
jednotlivými	jednotlivý	k2eAgFnPc7d1
součástkami	součástka	k1gFnPc7
či	či	k8xC
dochází	docházet	k5eAaImIp3nS
též	též	k9
k	k	k7c3
narušování	narušování	k1gNnSc3
přenosu	přenos	k1gInSc2
informace	informace	k1gFnSc2
vlivem	vlivem	k7c2
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bity	bit	k1gInPc7
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
implementovány	implementován	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
různě	různě	k6eAd1
zmagnetované	zmagnetovaný	k2eAgFnSc6d1
části	část	k1gFnSc6
pevného	pevný	k2eAgInSc2d1
disku	disk	k1gInSc2
nebo	nebo	k8xC
různě	různě	k6eAd1
velké	velký	k2eAgFnPc4d1
prohlubně	prohlubeň	k1gFnPc4
v	v	k7c6
povrchu	povrch	k1gInSc6
kompaktních	kompaktní	k2eAgInPc2d1
disků	disk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
z	z	k7c2
mikroskopického	mikroskopický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
o	o	k7c4
poměrně	poměrně	k6eAd1
velký	velký	k2eAgInSc4d1
počet	počet	k1gInSc4
atomů	atom	k1gInPc2
či	či	k8xC
molekul	molekula	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
reprezentují	reprezentovat	k5eAaImIp3nP
jeden	jeden	k4xCgInSc4
bit	bit	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Zkusme	zkusit	k5eAaPmRp1nP
nyní	nyní	k6eAd1
tuto	tento	k3xDgFnSc4
situaci	situace	k1gFnSc4
přenést	přenést	k5eAaPmF
do	do	k7c2
mikroskopického	mikroskopický	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
bychom	by	kYmCp1nP
jeden	jeden	k4xCgInSc4
bit	bit	k1gInSc4
reprezentovali	reprezentovat	k5eAaImAgMnP
jediným	jediný	k2eAgInSc7d1
atomem	atom	k1gInSc7
či	či	k8xC
fotonem	foton	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
bitu	bit	k1gInSc2
požadujeme	požadovat	k5eAaImIp1nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nabýval	nabývat	k5eAaImAgMnS
dvou	dva	k4xCgFnPc2
hodnot	hodnota	k1gFnPc2
–	–	k?
0	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
fotonu	foton	k1gInSc2
lze	lze	k6eAd1
tuto	tento	k3xDgFnSc4
vlastnost	vlastnost	k1gFnSc4
nasimulovat	nasimulovat	k5eAaPmF
například	například	k6eAd1
odlišnou	odlišný	k2eAgFnSc7d1
polarizací	polarizace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foton	foton	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
polarizován	polarizovat	k5eAaImNgMnS
vodorovně	vodorovně	k6eAd1
či	či	k8xC
svisle	svisle	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
dva	dva	k4xCgInPc4
stavy	stav	k1gInPc4
lze	lze	k6eAd1
po	po	k7c6
řadě	řada	k1gFnSc6
přirovnat	přirovnat	k5eAaPmF
k	k	k7c3
nule	nula	k1gFnSc3
či	či	k8xC
jedničce	jednička	k1gFnSc3
bitu	bit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
zahájením	zahájení	k1gNnSc7
výpočtu	výpočet	k1gInSc2
si	se	k3xPyFc3
můžeme	moct	k5eAaImIp1nP
připravit	připravit	k5eAaPmF
dostatek	dostatek	k1gInSc4
fotonů	foton	k1gInPc2
polarizovaných	polarizovaný	k2eAgInPc2d1
<g/>
,	,	kIx,
řekněme	říct	k5eAaPmRp1nP
<g/>
,	,	kIx,
vodorovně	vodorovně	k6eAd1
a	a	k8xC
tyto	tento	k3xDgInPc4
fotony	foton	k1gInPc4
pak	pak	k6eAd1
vysílat	vysílat	k5eAaImF
do	do	k7c2
procesoru	procesor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Procesor	procesor	k1gInSc1
provede	provést	k5eAaPmIp3nS
s	s	k7c7
fotony	foton	k1gInPc7
potřebné	potřebný	k2eAgFnSc2d1
operace	operace	k1gFnSc2
a	a	k8xC
nakonec	nakonec	k6eAd1
vrátí	vrátit	k5eAaPmIp3nP
fotony	foton	k1gInPc1
upravené	upravený	k2eAgInPc1d1
tak	tak	k6eAd1
<g/>
,	,	kIx,
abychom	aby	kYmCp1nP
z	z	k7c2
jejich	jejich	k3xOp3gFnSc2
polarizace	polarizace	k1gFnSc2
byli	být	k5eAaImAgMnP
schopni	schopen	k2eAgMnPc1d1
vyčíst	vyčíst	k5eAaPmF
výsledek	výsledek	k1gInSc1
výpočtu	výpočet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
zjistíme	zjistit	k5eAaPmIp1nP
změřením	změření	k1gNnSc7
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
každý	každý	k3xTgInSc1
foton	foton	k1gInSc1
polarizován	polarizován	k2eAgInSc1d1
<g/>
,	,	kIx,
buď	buď	k8xC
vodorovně	vodorovně	k6eAd1
<g/>
,	,	kIx,
či	či	k8xC
svisle	svisle	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
polarizace	polarizace	k1gFnSc2
každého	každý	k3xTgInSc2
fotonu	foton	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
opustil	opustit	k5eAaPmAgInS
procesor	procesor	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
zrekonstruujeme	zrekonstruovat	k5eAaPmIp1nP
řetězec	řetězec	k1gInSc4
bitů	bit	k1gInPc2
nesoucích	nesoucí	k2eAgFnPc2d1
cílovou	cílový	k2eAgFnSc4d1
informaci	informace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
právě	právě	k6eAd1
popsaném	popsaný	k2eAgInSc6d1
mikroskopickém	mikroskopický	k2eAgInSc6d1
modelu	model	k1gInSc6
počítání	počítání	k1gNnSc2
však	však	k9
přichází	přicházet	k5eAaImIp3nS
do	do	k7c2
hry	hra	k1gFnSc2
nový	nový	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jím	on	k3xPp3gNnSc7
kvantová	kvantový	k2eAgFnSc1d1
mechanika	mechanika	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
zákony	zákon	k1gInPc4
je	být	k5eAaImIp3nS
v	v	k7c6
mikrosvětě	mikrosvět	k1gInSc6
nutno	nutno	k6eAd1
brát	brát	k5eAaImF
v	v	k7c4
úvahu	úvaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
zákony	zákon	k1gInPc7
nám	my	k3xPp1nPc3
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
říkají	říkat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
ačkoli	ačkoli	k8xS
byly	být	k5eAaImAgInP
fotony	foton	k1gInPc1
na	na	k7c6
počátku	počátek	k1gInSc6
připraveny	připraven	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
vodorovně	vodorovně	k6eAd1
polarizované	polarizovaný	k2eAgNnSc1d1
<g/>
,	,	kIx,
z	z	k7c2
procesoru	procesor	k1gInSc2
používajícího	používající	k2eAgInSc2d1
kvantové	kvantový	k2eAgFnSc2d1
brány	brána	k1gFnSc2
mohou	moct	k5eAaImIp3nP
vylétat	vylétat	k5eAaPmF,k5eAaImF
fotony	foton	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
nejsou	být	k5eNaImIp3nP
polarizovány	polarizován	k2eAgInPc1d1
ani	ani	k8xC
vodorovně	vodorovně	k6eAd1
ani	ani	k8xC
svisle	svisle	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gInSc1
stav	stav	k1gInSc1
je	být	k5eAaImIp3nS
kombinací	kombinace	k1gFnPc2
obou	dva	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestliže	jestliže	k8xS
změříme	změřit	k5eAaPmIp1nP
polarizaci	polarizace	k1gFnSc4
takových	takový	k3xDgMnPc2
fotonů	foton	k1gInPc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
dostaneme	dostat	k5eAaPmIp1nP
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
foton	foton	k1gInSc1
polarizován	polarizován	k2eAgInSc1d1
svisle	svisla	k1gFnSc3
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jindy	jindy	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
polarizován	polarizovat	k5eAaImNgInS
vodorovně	vodorovně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
nejednoznačné	jednoznačný	k2eNgInPc1d1
výsledky	výsledek	k1gInPc1
přitom	přitom	k6eAd1
budeme	být	k5eAaImBp1nP
dostávat	dostávat	k5eAaImF
i	i	k9
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
mnohokrát	mnohokrát	k6eAd1
provedeme	provést	k5eAaPmIp1nP
tutéž	týž	k3xTgFnSc4
úlohu	úloha	k1gFnSc4
na	na	k7c6
témže	týž	k3xTgInSc6
procesoru	procesor	k1gInSc6
se	s	k7c7
vstupními	vstupní	k2eAgInPc7d1
fotony	foton	k1gInPc7
připravenými	připravený	k2eAgInPc7d1
týmtéž	týmtéž	k6eAd1
způsobem	způsob	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohlo	moct	k5eAaImAgNnS
by	by	kYmCp3nS
se	se	k3xPyFc4
zdát	zdát	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgInPc1
výsledky	výsledek	k1gInPc1
můžeme	moct	k5eAaImIp1nP
vysvětlit	vysvětlit	k5eAaPmF
za	za	k7c2
pomoci	pomoc	k1gFnSc2
teorie	teorie	k1gFnSc2
pravděpodobnosti	pravděpodobnost	k1gFnSc2
<g/>
:	:	kIx,
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
bychom	by	kYmCp1nP
bit	bit	k2eAgMnSc1d1
modelovali	modelovat	k5eAaImAgMnP
jako	jako	k8xS,k8xC
lineární	lineární	k2eAgFnSc4d1
kombinaci	kombinace	k1gFnSc4
obou	dva	k4xCgFnPc2
hodnot	hodnota	k1gFnPc2
0	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
koeficienty	koeficient	k1gInPc4
této	tento	k3xDgFnSc2
kombinace	kombinace	k1gFnSc2
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
dvě	dva	k4xCgNnPc4
nezáporná	záporný	k2eNgNnPc4d1
čísla	číslo	k1gNnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnPc4
součet	součet	k1gInSc1
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koeficienty	koeficient	k1gInPc1
by	by	kYmCp3nP
tak	tak	k6eAd1
odpovídaly	odpovídat	k5eAaImAgFnP
pravděpodobnostem	pravděpodobnost	k1gFnPc3
dané	daný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
kvantové	kvantový	k2eAgFnSc2d1
mechaniky	mechanika	k1gFnSc2
nicméně	nicméně	k8xC
stav	stav	k1gInSc1
kvantových	kvantový	k2eAgInPc2d1
systémů	systém	k1gInPc2
není	být	k5eNaImIp3nS
popsán	popsán	k2eAgInSc1d1
přímo	přímo	k6eAd1
pomocí	pomoc	k1gFnSc7
pravděpodobností	pravděpodobnost	k1gFnSc7
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
pomocí	pomocí	k7c2
tzv.	tzv.	kA
amplitud	amplituda	k1gFnPc2
pravděpodobnosti	pravděpodobnost	k1gFnSc2
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
amplitud	amplituda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čísel	číslo	k1gNnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
pravděpodobností	pravděpodobnost	k1gFnPc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
i	i	k9
záporná	záporný	k2eAgNnPc4d1
a	a	k8xC
dokonce	dokonce	k9
komplexní	komplexní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abychom	aby	kYmCp1nP
tak	tak	k9
mohli	moct	k5eAaImAgMnP
mluvit	mluvit	k5eAaImF
o	o	k7c4
informaci	informace	k1gFnSc4
na	na	k7c6
mikroskopické	mikroskopický	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
,	,	kIx,
zavádíme	zavádět	k5eAaImIp1nP
nový	nový	k2eAgInSc4d1
pojem	pojem	k1gInSc4
–	–	k?
kvantový	kvantový	k2eAgInSc1d1
bit	bit	k1gInSc1
<g/>
,	,	kIx,
čili	čili	k8xC
qubit	qubit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
abstraktní	abstraktní	k2eAgInSc4d1
model	model	k1gInSc4
fyzikálního	fyzikální	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
může	moct	k5eAaImIp3nS
po	po	k7c6
změření	změření	k1gNnSc6
nabývat	nabývat	k5eAaImF
dvou	dva	k4xCgFnPc6
různých	různý	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
a	a	k8xC
jehož	jehož	k3xOyRp3gInSc1
stav	stav	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c4
tuto	tento	k3xDgFnSc4
chvíli	chvíle	k1gFnSc4
popsán	popsat	k5eAaPmNgMnS
pomocí	pomoc	k1gFnSc7
amplitud	amplituda	k1gFnPc2
jako	jako	k8xC,k8xS
kombinace	kombinace	k1gFnSc1
těchto	tento	k3xDgFnPc2
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
jsou	být	k5eAaImIp3nP
zmíněné	zmíněný	k2eAgInPc1d1
fotony	foton	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
mají	mít	k5eAaImIp3nP
po	po	k7c6
změření	změření	k1gNnSc6
polarizaci	polarizace	k1gFnSc4
buď	buď	k8xC
vodorovně	vodorovně	k6eAd1
<g/>
,	,	kIx,
nebo	nebo	k8xC
svisle	svisle	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
výpočtu	výpočet	k1gInSc2
je	být	k5eAaImIp3nS
však	však	k9
jejich	jejich	k3xOp3gInSc1
stav	stav	k1gInSc1
popsán	popsat	k5eAaPmNgInS
pouze	pouze	k6eAd1
jako	jako	k8xS,k8xC
kombinace	kombinace	k1gFnSc1
těchto	tento	k3xDgInPc2
dvou	dva	k4xCgInPc2
stavů	stav	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Formální	formální	k2eAgFnSc1d1
definice	definice	k1gFnSc1
</s>
<s>
Formálně	formálně	k6eAd1
je	být	k5eAaImIp3nS
qubit	qubit	k5eAaImF,k5eAaBmF,k5eAaPmF
definován	definovat	k5eAaBmNgInS
jako	jako	k8xS,k8xC
normalizovaný	normalizovaný	k2eAgInSc1d1
vektor	vektor	k1gInSc1
ve	v	k7c6
dvourozměrném	dvourozměrný	k2eAgInSc6d1
Hilbertově	Hilbertův	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
stavů	stav	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takovýto	takovýto	k3xDgInSc1
popis	popis	k1gInSc4
odpovídá	odpovídat	k5eAaImIp3nS
běžnému	běžný	k2eAgInSc3d1
způsobu	způsob	k1gInSc3
popisu	popis	k1gInSc2
stavů	stav	k1gInPc2
v	v	k7c6
kvantové	kvantový	k2eAgFnSc6d1
mechanice	mechanika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
dvourozměrný	dvourozměrný	k2eAgInSc4d1
Hilbertův	Hilbertův	k2eAgInSc4d1
prostor	prostor	k1gInSc4
značíme	značit	k5eAaImIp1nP
</s>
<s>
H	H	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathcal	mathcat	k5eAaPmAgInS
{	{	kIx(
<g/>
H	H	kA
<g/>
}}}	}}}	k?
</s>
<s>
a	a	k8xC
zafixujeme	zafixovat	k5eAaPmIp1nP
jeho	jeho	k3xOp3gFnSc7
ortonormální	ortonormální	k2eAgFnSc7d1
bází	báze	k1gFnSc7
tvořenou	tvořený	k2eAgFnSc7d1
vektory	vektor	k1gInPc7
označenými	označený	k2eAgFnPc7d1
jako	jako	k8xC,k8xS
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
,	,	kIx,
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc2
,	,	kIx,
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
braketová	braketový	k2eAgFnSc1d1
notace	notace	k1gFnSc1
pro	pro	k7c4
označování	označování	k1gNnSc4
vektorů	vektor	k1gInPc2
v	v	k7c6
kvantové	kvantový	k2eAgFnSc6d1
mechanice	mechanika	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
prvek	prvek	k1gInSc1
</s>
<s>
|	|	kIx~
</s>
<s>
ψ	ψ	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
∈	∈	k?
</s>
<s>
H	H	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
psi	pes	k1gMnPc1
\	\	kIx~
<g/>
rangle	rangle	k1gInSc1
\	\	kIx~
<g/>
in	in	k?
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathcal	mathcat	k5eAaPmAgInS
{	{	kIx(
<g/>
H	H	kA
<g/>
}}}	}}}	k?
</s>
<s>
tohoto	tento	k3xDgInSc2
prostoru	prostor	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
jednotkovou	jednotkový	k2eAgFnSc4d1
velikost	velikost	k1gFnSc4
<g/>
,	,	kIx,
nazveme	nazvat	k5eAaBmIp1nP,k5eAaPmIp1nP
qubitem	qubit	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Explicitně	explicitně	k6eAd1
je	být	k5eAaImIp3nS
tedy	tedy	k9
qubit	qubit	k1gInSc1
popsán	popsán	k2eAgInSc1d1
jako	jako	k8xS,k8xC
vektor	vektor	k1gInSc1
</s>
<s>
|	|	kIx~
</s>
<s>
ψ	ψ	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
=	=	kIx~
</s>
<s>
α	α	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
+	+	kIx~
</s>
<s>
β	β	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
,	,	kIx,
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
psi	pes	k1gMnPc1
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
=	=	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gMnSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
+	+	kIx~
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc2
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
α	α	k?
</s>
<s>
,	,	kIx,
</s>
<s>
β	β	k?
</s>
<s>
∈	∈	k?
</s>
<s>
C	C	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
alpha	alpha	k1gMnSc1
,	,	kIx,
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
\	\	kIx~
<g/>
in	in	k?
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
jsou	být	k5eAaImIp3nP
komplexní	komplexní	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
vektoru	vektor	k1gInSc2
v	v	k7c6
bázi	báze	k1gFnSc6
</s>
<s>
{	{	kIx(
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
,	,	kIx,
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
}	}	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
{	{	kIx(
<g/>
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc2
,	,	kIx,
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
\	\	kIx~
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
též	tenž	k3xDgFnSc2
nazývané	nazývaný	k2eAgFnSc2d1
amplitudy	amplituda	k1gFnSc2
pravděpodobnosti	pravděpodobnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
musí	muset	k5eAaImIp3nP
splňovat	splňovat	k5eAaImF
normalizační	normalizační	k2eAgFnSc4d1
podmínku	podmínka	k1gFnSc4
</s>
<s>
|	|	kIx~
</s>
<s>
α	α	k?
</s>
<s>
|	|	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
|	|	kIx~
</s>
<s>
β	β	k?
</s>
<s>
|	|	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
|	|	kIx~
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
|	|	kIx~
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
|	|	kIx~
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
Ta	ten	k3xDgFnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
čísla	číslo	k1gNnPc1
</s>
<s>
|	|	kIx~
</s>
<s>
α	α	k?
</s>
<s>
|	|	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
|	|	kIx~
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
resp.	resp.	kA
</s>
<s>
|	|	kIx~
</s>
<s>
β	β	k?
</s>
<s>
|	|	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
|	|	kIx~
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
bude	být	k5eAaImBp3nS
po	po	k7c6
měření	měření	k1gNnSc6
možno	možno	k6eAd1
interpretovat	interpretovat	k5eAaBmF
jako	jako	k9
pravděpodobnost	pravděpodobnost	k1gFnSc1
nalezení	nalezení	k1gNnSc2
stavu	stav	k1gInSc2
</s>
<s>
|	|	kIx~
</s>
<s>
ψ	ψ	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
psi	pes	k1gMnPc1
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
ve	v	k7c6
stavu	stav	k1gInSc6
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
,	,	kIx,
resp.	resp.	kA
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
Ortonormální	Ortonormální	k2eAgFnSc1d1
báze	báze	k1gFnSc1
tvořená	tvořený	k2eAgFnSc1d1
vektory	vektor	k1gInPc1
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
,	,	kIx,
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc2
,	,	kIx,
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
se	se	k3xPyFc4
občas	občas	k6eAd1
nazývá	nazývat	k5eAaImIp3nS
výpočetní	výpočetní	k2eAgFnSc1d1
báze	báze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Právě	právě	k9
uvedená	uvedený	k2eAgFnSc1d1
definice	definice	k1gFnSc1
qubitu	qubit	k1gInSc2
pracuje	pracovat	k5eAaImIp3nS
s	s	k7c7
takzvanými	takzvaný	k2eAgInPc7d1
čistými	čistý	k2eAgInPc7d1
stavy	stav	k1gInPc7
<g/>
,	,	kIx,
tedy	tedy	k9
stavy	stav	k1gInPc7
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
lze	lze	k6eAd1
popsat	popsat	k5eAaPmF
jako	jako	k8xC,k8xS
prvky	prvek	k1gInPc4
Hilbertova	Hilbertův	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
ale	ale	k8xC
stav	stav	k1gInSc4
kvantového	kvantový	k2eAgInSc2d1
systému	systém	k1gInSc2
nelze	lze	k6eNd1
popsat	popsat	k5eAaPmF
pomocí	pomocí	k7c2
vektoru	vektor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
přizvat	přizvat	k5eAaPmF
obecnější	obecní	k2eAgInSc4d2
formalismus	formalismus	k1gInSc4
užívající	užívající	k2eAgFnSc2d1
matic	matice	k1gFnPc2
hustoty	hustota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
formalizmu	formalizmus	k1gInSc6
je	být	k5eAaImIp3nS
qubit	qubit	k5eAaImF,k5eAaPmF,k5eAaBmF
jakýkoli	jakýkoli	k3yIgInSc4
pozitivní	pozitivní	k2eAgInSc4d1
operátor	operátor	k1gInSc4
</s>
<s>
ρ	ρ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
rho	rho	k?
}	}	kIx)
</s>
<s>
působící	působící	k2eAgMnSc1d1
na	na	k7c6
dvourozměrném	dvourozměrný	k2eAgInSc6d1
Hilbertově	Hilbertův	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
</s>
<s>
H	H	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathcal	mathcat	k5eAaPmAgInS
{	{	kIx(
<g/>
H	H	kA
<g/>
}}}	}}}	k?
</s>
<s>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
splňuje	splňovat	k5eAaImIp3nS
podmínku	podmínka	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc1
stopa	stopa	k1gFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
jedné	jeden	k4xCgFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neboli	neboli	k8xC
</s>
<s>
ρ	ρ	k?
</s>
<s>
∈	∈	k?
</s>
<s>
C	C	kA
</s>
<s>
2	#num#	k4
</s>
<s>
×	×	k?
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
in	in	k?
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
times	times	k1gInSc1
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
matice	matice	k1gFnSc1
rozměru	rozměr	k1gInSc2
</s>
<s>
2	#num#	k4
</s>
<s>
×	×	k?
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
2	#num#	k4
<g/>
\	\	kIx~
<g/>
times	times	k1gInSc1
2	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
pro	pro	k7c4
níž	jenž	k3xRgFnSc2
</s>
<s>
T	T	kA
</s>
<s>
r	r	kA
</s>
<s>
(	(	kIx(
</s>
<s>
ρ	ρ	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
a	a	k8xC
</s>
<s>
⟨	⟨	k?
</s>
<s>
ψ	ψ	k?
</s>
<s>
|	|	kIx~
</s>
<s>
ρ	ρ	k?
</s>
<s>
|	|	kIx~
</s>
<s>
ψ	ψ	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
≥	≥	k?
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
∀	∀	k?
</s>
<s>
|	|	kIx~
</s>
<s>
ψ	ψ	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
∈	∈	k?
</s>
<s>
H	H	kA
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
Tr	Tr	k1gFnSc1
<g/>
}	}	kIx)
(	(	kIx(
<g/>
\	\	kIx~
<g/>
rho	rho	k?
)	)	kIx)
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
qquad	qquad	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
text	text	k1gInSc1
<g/>
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
qquad	qquad	k1gInSc1
\	\	kIx~
<g/>
langle	langle	k1gInSc1
\	\	kIx~
<g/>
psi	pes	k1gMnPc1
|	|	kIx~
<g/>
\	\	kIx~
<g/>
rho	rho	k?
|	|	kIx~
<g/>
\	\	kIx~
<g/>
psi	pes	k1gMnPc1
\	\	kIx~
<g/>
rangle	rangle	k1gInSc1
\	\	kIx~
<g/>
geq	geq	k?
0	#num#	k4
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
forall	forall	k1gMnSc1
|	|	kIx~
<g/>
\	\	kIx~
<g/>
psi	pes	k1gMnPc1
\	\	kIx~
<g/>
rangle	rangle	k1gInSc1
\	\	kIx~
<g/>
in	in	k?
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathcal	mathcat	k5eAaPmAgInS
{	{	kIx(
<g/>
H	H	kA
<g/>
}}	}}	k?
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Vícequbitové	Vícequbitový	k2eAgInPc1d1
systémy	systém	k1gInPc1
</s>
<s>
Ve	v	k7c6
výpočtech	výpočet	k1gInPc6
na	na	k7c6
kvantovém	kvantový	k2eAgInSc6d1
počítači	počítač	k1gInSc6
si	se	k3xPyFc3
s	s	k7c7
jediným	jediný	k2eAgInSc7d1
qubitem	qubit	k1gInSc7
nevystačíme	vystačit	k5eNaBmIp1nP
a	a	k8xC
musíme	muset	k5eAaImIp1nP
tedy	tedy	k9
uvažovat	uvažovat	k5eAaImF
i	i	k9
stavy	stav	k1gInPc1
tvořené	tvořený	k2eAgInPc1d1
více	hodně	k6eAd2
qubity	qubit	k2eAgInPc1d1
<g/>
,	,	kIx,
také	také	k9
zvané	zvaný	k2eAgInPc4d1
kvantové	kvantový	k2eAgInPc4d1
registry	registr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Složený	složený	k2eAgInSc1d1
stav	stav	k1gInSc1
</s>
<s>
N	N	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
N	N	kA
<g/>
}	}	kIx)
</s>
<s>
qubitů	qubit	k1gInPc2
lze	lze	k6eAd1
matematicky	matematicky	k6eAd1
popsat	popsat	k5eAaPmF
jako	jako	k9
prvek	prvek	k1gInSc4
Hilbertova	Hilbertův	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
</s>
<s>
H	H	kA
</s>
<s>
N	N	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathcal	mathcat	k5eAaPmAgInS
{	{	kIx(
<g/>
H	H	kA
<g/>
}}	}}	k?
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
N	N	kA
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
vznikne	vzniknout	k5eAaPmIp3nS
jako	jako	k9
tenzorový	tenzorový	k2eAgInSc1d1
součin	součin	k1gInSc1
</s>
<s>
N	N	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
N	N	kA
<g/>
}	}	kIx)
</s>
<s>
Hilbertových	Hilbertův	k2eAgInPc2d1
prostorů	prostor	k1gInPc2
</s>
<s>
H	H	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathcal	mathcat	k5eAaPmAgInS
{	{	kIx(
<g/>
H	H	kA
<g/>
}}}	}}}	k?
</s>
<s>
pro	pro	k7c4
jednoqubitové	jednoqubitový	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
</s>
<s>
H	H	kA
</s>
<s>
N	N	kA
</s>
<s>
=	=	kIx~
</s>
<s>
H	H	kA
</s>
<s>
⊗	⊗	k?
</s>
<s>
H	H	kA
</s>
<s>
⊗	⊗	k?
</s>
<s>
…	…	k?
</s>
<s>
⊗	⊗	k?
</s>
<s>
H	H	kA
</s>
<s>
⏟	⏟	k?
</s>
<s>
N	N	kA
</s>
<s>
=	=	kIx~
</s>
<s>
⨂	⨂	k?
</s>
<s>
i	i	k9
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
N	N	kA
</s>
<s>
H	H	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathcal	mathcat	k5eAaPmAgInS
{	{	kIx(
<g/>
H	H	kA
<g/>
}}	}}	k?
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
N	N	kA
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
underbrace	underbrace	k1gFnSc1
{{	{{	k?
<g/>
\	\	kIx~
<g/>
mathcal	mathcat	k5eAaPmAgInS
{	{	kIx(
<g/>
H	H	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathcal	mathcat	k5eAaPmAgInS
{	{	kIx(
<g/>
H	H	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
\	\	kIx~
<g/>
ldots	ldots	k1gInSc1
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathcal	mathcat	k5eAaPmAgInS
{	{	kIx(
<g/>
H	H	kA
<g/>
}}}	}}}	k?
_	_	kIx~
<g/>
{	{	kIx(
<g/>
N	N	kA
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
bigotimes	bigotimes	k1gInSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
N	N	kA
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathcal	mathcat	k5eAaPmAgInS
{	{	kIx(
<g/>
H	H	kA
<g/>
}}}	}}}	k?
</s>
<s>
Takový	takový	k3xDgInSc1
systém	systém	k1gInSc1
má	mít	k5eAaImIp3nS
dimenzi	dimenze	k1gFnSc4
rovnou	rovnou	k6eAd1
</s>
<s>
dim	dim	k?
</s>
<s>
H	H	kA
</s>
<s>
N	N	kA
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
N	N	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
dim	dim	k?
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathcal	mathcat	k5eAaPmAgInS
{	{	kIx(
<g/>
H	H	kA
<g/>
}}	}}	k?
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
N	N	kA
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
N	N	kA
<g/>
}}	}}	k?
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
ortonormální	ortonormální	k2eAgFnSc1d1
báze	báze	k1gFnSc1
vznikne	vzniknout	k5eAaPmIp3nS
například	například	k6eAd1
jako	jako	k9
množina	množina	k1gFnSc1
tenzorových	tenzorový	k2eAgInPc2d1
součinů	součin	k1gInPc2
bazických	bazický	k2eAgInPc2d1
vektorů	vektor	k1gInPc2
jednoqubitových	jednoqubitový	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
vektory	vektor	k1gInPc4
tvaru	tvar	k1gInSc2
</s>
<s>
|	|	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
1	#num#	k4
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
…	…	k?
</s>
<s>
a	a	k8xC
</s>
<s>
N	N	kA
</s>
<s>
⟩	⟩	k?
</s>
<s>
=	=	kIx~
</s>
<s>
|	|	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
⊗	⊗	k?
</s>
<s>
…	…	k?
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
N	N	kA
</s>
<s>
⟩	⟩	k?
</s>
<s>
,	,	kIx,
</s>
<s>
(	(	kIx(
</s>
<s>
∀	∀	k?
</s>
<s>
i	i	k9
</s>
<s>
∈	∈	k?
</s>
<s>
{	{	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
…	…	k?
</s>
<s>
,	,	kIx,
</s>
<s>
N	N	kA
</s>
<s>
}	}	kIx)
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
a	a	k8xC
</s>
<s>
i	i	k9
</s>
<s>
∈	∈	k?
</s>
<s>
{	{	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
}	}	kIx)
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
|	|	kIx~
<g/>
a_	a_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
a_	a_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
ldots	ldotsit	k5eAaPmRp2nS
a_	a_	k?
<g/>
{	{	kIx(
<g/>
N	N	kA
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
=	=	kIx~
<g/>
|	|	kIx~
<g/>
a_	a_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
rangle	rangle	k1gInSc1
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
a_	a_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gInSc1
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
\	\	kIx~
<g/>
ldots	ldots	k1gInSc1
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
a_	a_	k?
<g/>
{	{	kIx(
<g/>
N	N	kA
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gInSc1
,	,	kIx,
<g/>
\	\	kIx~
<g/>
quad	quad	k6eAd1
(	(	kIx(
<g/>
\	\	kIx~
<g/>
forall	forallit	k5eAaPmRp2nS
i	i	k9
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
\	\	kIx~
<g/>
in	in	k?
\	\	kIx~
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
ldots	ldots	k1gInSc1
,	,	kIx,
<g/>
N	N	kA
<g/>
\	\	kIx~
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
a_	a_	k?
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
in	in	k?
\	\	kIx~
<g/>
{	{	kIx(
<g/>
0,1	0,1	k4
<g/>
\	\	kIx~
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
Pro	pro	k7c4
dvouqubitové	dvouqubitový	k2eAgInPc4d1
systémy	systém	k1gInPc4
dostáváme	dostávat	k5eAaImIp1nP
konkrétně	konkrétně	k6eAd1
bázi	báze	k1gFnSc4
</s>
<s>
{	{	kIx(
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
,	,	kIx,
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
,	,	kIx,
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
,	,	kIx,
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
}	}	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
{	{	kIx(
<g/>
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc2
,	,	kIx,
<g/>
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc2
,	,	kIx,
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc2
,	,	kIx,
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
\	\	kIx~
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
však	však	k9
i	i	k9
jiných	jiný	k2eAgFnPc2d1
bází	báze	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
Bellovy	Bellův	k2eAgFnSc2d1
báze	báze	k1gFnSc2
tvořené	tvořený	k2eAgFnPc1d1
maximálně	maximálně	k6eAd1
provázanými	provázaný	k2eAgInPc7d1
stavy	stav	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnSc1d1
interpretace	interpretace	k1gFnSc1
qubitu	qubit	k1gInSc2
</s>
<s>
Qubit	Qubit	k1gInSc1
je	být	k5eAaImIp3nS
kvantovým	kvantový	k2eAgNnSc7d1
zobecněním	zobecnění	k1gNnSc7
klasického	klasický	k2eAgInSc2d1
bitu	bit	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
kromě	kromě	k7c2
hodnot	hodnota	k1gFnPc2
0	#num#	k4
a	a	k8xC
1	#num#	k4
může	moct	k5eAaImIp3nS
nabývat	nabývat	k5eAaImF
i	i	k8xC
všech	všecek	k3xTgFnPc2
hodnot	hodnota	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
kombinacemi	kombinace	k1gFnPc7
obou	dva	k4xCgFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vyplývá	vyplývat	k5eAaImIp3nS
z	z	k7c2
následující	následující	k2eAgFnSc2d1
obecné	obecný	k2eAgFnSc2d1
vlastnosti	vlastnost	k1gFnSc2
stavů	stav	k1gInPc2
kvantových	kvantový	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
:	:	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
systém	systém	k1gInSc1
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
stavu	stav	k1gInSc6
</s>
<s>
|	|	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
a	a	k8xC
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
}	}	kIx)
</s>
<s>
nebo	nebo	k8xC
</s>
<s>
|	|	kIx~
</s>
<s>
b	b	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
b	b	k?
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
}	}	kIx)
</s>
<s>
,	,	kIx,
můžeme	moct	k5eAaImIp1nP
připravit	připravit	k5eAaPmF
kvantovou	kvantový	k2eAgFnSc4d1
superpozici	superpozice	k1gFnSc4
těchto	tento	k3xDgInPc2
stavů	stav	k1gInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
systém	systém	k1gInSc4
nacházející	nacházející	k2eAgInSc4d1
se	se	k3xPyFc4
ve	v	k7c6
stavu	stav	k1gInSc6
</s>
<s>
α	α	k?
</s>
<s>
|	|	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
⟩	⟩	k?
</s>
<s>
+	+	kIx~
</s>
<s>
β	β	k?
</s>
<s>
|	|	kIx~
</s>
<s>
b	b	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
alpha	alph	k1gMnSc4
|	|	kIx~
<g/>
a	a	k8xC
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
+	+	kIx~
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
|	|	kIx~
<g/>
b	b	k?
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
}	}	kIx)
</s>
<s>
pro	pro	k7c4
nějaká	nějaký	k3yIgNnPc4
komplexní	komplexní	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
</s>
<s>
α	α	k?
</s>
<s>
,	,	kIx,
</s>
<s>
β	β	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
alpha	alpha	k1gMnSc1
,	,	kIx,
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
}	}	kIx)
</s>
<s>
Identifikujeme	identifikovat	k5eAaBmIp1nP
<g/>
-li	-li	k?
hodnotu	hodnota	k1gFnSc4
0	#num#	k4
klasického	klasický	k2eAgInSc2d1
bitu	bit	k1gInSc2
s	s	k7c7
bazickým	bazický	k2eAgInSc7d1
vektorem	vektor	k1gInSc7
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
a	a	k8xC
podobně	podobně	k6eAd1
hodnotu	hodnota	k1gFnSc4
1	#num#	k4
s	s	k7c7
bazickým	bazický	k2eAgInSc7d1
vektorem	vektor	k1gInSc7
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
,	,	kIx,
pak	pak	k6eAd1
můžeme	moct	k5eAaImIp1nP
superpozici	superpozice	k1gFnSc6
těchto	tento	k3xDgInPc2
dvou	dva	k4xCgInPc2
stavů	stav	k1gInPc2
zhruba	zhruba	k6eAd1
interpretovat	interpretovat	k5eAaBmF
jako	jako	k8xC,k8xS
"	"	kIx"
<g/>
klasickou	klasický	k2eAgFnSc4d1
<g/>
"	"	kIx"
hodnotu	hodnota	k1gFnSc4
bitu	bit	k1gInSc2
z	z	k7c2
intervalu	interval	k1gInSc2
mezi	mezi	k7c7
0	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Takovýto	takovýto	k3xDgInSc1
pohled	pohled	k1gInSc1
je	být	k5eAaImIp3nS
ale	ale	k9
zavádějící	zavádějící	k2eAgMnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
qubity	qubita	k1gFnPc1
vykazují	vykazovat	k5eAaImIp3nP
mnoho	mnoho	k4c4
zajímavých	zajímavý	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
v	v	k7c6
klasickém	klasický	k2eAgInSc6d1
případě	případ	k1gInSc6
nevyskytují	vyskytovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Blochova	Blochův	k2eAgFnSc1d1
sféra	sféra	k1gFnSc1
</s>
<s>
Jak	jak	k6eAd1
vyplývá	vyplývat	k5eAaImIp3nS
z	z	k7c2
obecné	obecný	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
pro	pro	k7c4
popis	popis	k1gInSc4
stavů	stav	k1gInPc2
systémů	systém	k1gInPc2
v	v	k7c6
kvantové	kvantový	k2eAgFnSc6d1
mechanice	mechanika	k1gFnSc6
<g/>
,	,	kIx,
můžeme	moct	k5eAaImIp1nP
qubit	qubit	k5eAaImF,k5eAaPmF,k5eAaBmF
alternativně	alternativně	k6eAd1
vyjádřit	vyjádřit	k5eAaPmF
pomocí	pomocí	k7c2
matice	matice	k1gFnSc2
hustoty	hustota	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
pozitivní	pozitivní	k2eAgInSc4d1
operátor	operátor	k1gInSc4
s	s	k7c7
jednotkovou	jednotkový	k2eAgFnSc7d1
stopou	stopa	k1gFnSc7
přiřazený	přiřazený	k2eAgInSc4d1
danému	daný	k2eAgInSc3d1
kvantovému	kvantový	k2eAgInSc3d1
systému	systém	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
qubity	qubit	k1gInPc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
systémy	systém	k1gInPc1
s	s	k7c7
dvourozměrným	dvourozměrný	k2eAgInSc7d1
stavovým	stavový	k2eAgInSc7d1
prostorem	prostor	k1gInSc7
<g/>
,	,	kIx,
tak	tak	k6eAd1
dostáváme	dostávat	k5eAaImIp1nP
matice	matice	k1gFnPc1
</s>
<s>
2	#num#	k4
</s>
<s>
×	×	k?
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
2	#num#	k4
<g/>
\	\	kIx~
<g/>
times	times	k1gInSc1
2	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
ve	v	k7c6
tvaru	tvar	k1gInSc6
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
b	b	k?
</s>
<s>
−	−	k?
</s>
<s>
i	i	k9
</s>
<s>
c	c	k0
</s>
<s>
b	b	k?
</s>
<s>
+	+	kIx~
</s>
<s>
i	i	k9
</s>
<s>
c	c	k0
</s>
<s>
1	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k2eAgInSc4d1
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
a	a	k8xC
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
&	&	k?
<g/>
b-ic	b-ic	k1gInSc1
<g/>
\\	\\	k?
<g/>
b	b	k?
<g/>
+	+	kIx~
<g/>
ic	ic	k?
<g/>
&	&	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
-a	-a	k?
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}}}	}}}	k?
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
a	a	k8xC
</s>
<s>
,	,	kIx,
</s>
<s>
b	b	k?
</s>
<s>
,	,	kIx,
</s>
<s>
c	c	k0
</s>
<s>
∈	∈	k?
</s>
<s>
R	R	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
a	a	k8xC
<g/>
,	,	kIx,
<g/>
b	b	k?
<g/>
,	,	kIx,
<g/>
c	c	k0
<g/>
\	\	kIx~
<g/>
in	in	k?
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
jsou	být	k5eAaImIp3nP
reálná	reálný	k2eAgNnPc1d1
libovolná	libovolný	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
parametry	parametr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
matice	matice	k1gFnSc1
tohoto	tento	k3xDgInSc2
tvaru	tvar	k1gInSc2
splňuje	splňovat	k5eAaImIp3nS
pro	pro	k7c4
libovolné	libovolný	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
parametrů	parametr	k1gInPc2
dvě	dva	k4xCgFnPc4
výše	výše	k1gFnPc4,k1gFnPc4wB
zmíněné	zmíněný	k2eAgFnSc2d1
podmínky	podmínka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
těžké	těžký	k2eAgNnSc1d1
nahlédnout	nahlédnout	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
lze	lze	k6eAd1
takovouto	takovýto	k3xDgFnSc4
matici	matice	k1gFnSc4
vyjádřit	vyjádřit	k5eAaPmF
v	v	k7c6
kompaktní	kompaktní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
s	s	k7c7
využitím	využití	k1gNnSc7
Pauliho	Pauli	k1gMnSc2
matic	matice	k1gFnPc2
ve	v	k7c6
tvaru	tvar	k1gInSc6
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
+	+	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
+	+	kIx~
</s>
<s>
b	b	k?
</s>
<s>
(	(	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
+	+	kIx~
</s>
<s>
c	c	k0
</s>
<s>
(	(	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
i	i	k9
</s>
<s>
i	i	k9
</s>
<s>
0	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
I	i	k9
</s>
<s>
+	+	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
σ	σ	k?
</s>
<s>
z	z	k7c2
</s>
<s>
+	+	kIx~
</s>
<s>
b	b	k?
</s>
<s>
σ	σ	k?
</s>
<s>
x	x	k?
</s>
<s>
+	+	kIx~
</s>
<s>
c	c	k0
</s>
<s>
σ	σ	k?
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
I	i	k9
</s>
<s>
+	+	kIx~
</s>
<s>
r	r	kA
</s>
<s>
→	→	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
σ	σ	k?
</s>
<s>
→	→	k?
</s>
<s>
2	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k1gInSc1
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}	}	kIx)
<g/>
1	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
\\	\\	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}}	}}	k?
<g/>
\	\	kIx~
+	+	kIx~
<g/>
\	\	kIx~
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k1gInSc1
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}	}	kIx)
<g/>
1	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
&	&	k?
<g/>
0	#num#	k4
<g/>
\\	\\	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}}	}}	k?
<g/>
\	\	kIx~
+	+	kIx~
<g/>
\	\	kIx~
b	b	k?
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k1gInSc1
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}	}	kIx)
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1	#num#	k4
<g/>
\\	\\	k?
<g/>
1	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}}	}}	k?
<g/>
\	\	kIx~
+	+	kIx~
<g/>
\	\	kIx~
c	c	k0
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k1gInSc1
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}	}	kIx)
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
-i	-i	k?
<g/>
\\	\\	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
i	i	k8xC
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
I	i	k9
<g/>
\	\	kIx~
+	+	kIx~
<g/>
\	\	kIx~
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
\	\	kIx~
<g/>
sigma	sigma	k1gNnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
z	z	k7c2
<g/>
}	}	kIx)
<g/>
\	\	kIx~
+	+	kIx~
<g/>
\	\	kIx~
b	b	k?
<g/>
\	\	kIx~
\	\	kIx~
<g/>
sigma	sigma	k1gNnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
x	x	k?
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
c	c	k0
<g/>
\	\	kIx~
\	\	kIx~
<g/>
sigma	sigma	k1gNnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
y	y	k?
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
I	i	k9
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
vec	vec	k?
{	{	kIx(
<g/>
r	r	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
vec	vec	k?
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sigma	sigma	k1gNnSc1
}}}	}}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
σ	σ	k?
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
σ	σ	k?
</s>
<s>
y	y	k?
</s>
<s>
,	,	kIx,
</s>
<s>
σ	σ	k?
</s>
<s>
z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
sigma	sigma	k1gNnSc3
_	_	kIx~
<g/>
{	{	kIx(
<g/>
x	x	k?
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
sigma	sigma	k1gNnSc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
y	y	k?
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
sigma	sigma	k1gNnSc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
z	z	k7c2
<g/>
}}	}}	k?
</s>
<s>
jsou	být	k5eAaImIp3nP
Pauliho	Pauli	k1gMnSc4
matice	matice	k1gFnSc2
a	a	k8xC
</s>
<s>
r	r	kA
</s>
<s>
→	→	k?
</s>
<s>
≡	≡	k?
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
b	b	k?
</s>
<s>
,	,	kIx,
</s>
<s>
2	#num#	k4
</s>
<s>
c	c	k0
</s>
<s>
,	,	kIx,
</s>
<s>
a	a	k8xC
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
vec	vec	k?
{	{	kIx(
<g/>
r	r	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
equiv	equiv	k6eAd1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
b	b	k?
<g/>
,2	,2	k4
<g/>
c	c	k0
<g/>
,	,	kIx,
<g/>
a	a	k8xC
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
Blochův	Blochův	k2eAgInSc1d1
vektor	vektor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledním	poslední	k2eAgInSc6d1
výrazu	výraz	k1gInSc6
výše	výše	k1gFnSc2,k1gFnSc2wB
je	být	k5eAaImIp3nS
formální	formální	k2eAgInSc1d1
"	"	kIx"
<g/>
skalární	skalární	k2eAgInSc1d1
součin	součin	k1gInSc1
<g/>
"	"	kIx"
Blochova	Blochův	k2eAgInSc2d1
vektoru	vektor	k1gInSc2
a	a	k8xC
vektoru	vektor	k1gInSc2
Pauliho	Pauli	k1gMnSc2
matic	matice	k1gFnPc2
definován	definovat	k5eAaBmNgInS
prostě	prostě	k9
jen	jen	k6eAd1
jako	jako	k8xS,k8xC
</s>
<s>
r	r	kA
</s>
<s>
→	→	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
σ	σ	k?
</s>
<s>
→	→	k?
</s>
<s>
≡	≡	k?
</s>
<s>
r	r	kA
</s>
<s>
1	#num#	k4
</s>
<s>
σ	σ	k?
</s>
<s>
x	x	k?
</s>
<s>
+	+	kIx~
</s>
<s>
r	r	kA
</s>
<s>
2	#num#	k4
</s>
<s>
σ	σ	k?
</s>
<s>
y	y	k?
</s>
<s>
+	+	kIx~
</s>
<s>
r	r	kA
</s>
<s>
3	#num#	k4
</s>
<s>
σ	σ	k?
</s>
<s>
z	z	k7c2
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
vec	vec	k?
{	{	kIx(
<g/>
r	r	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
vec	vec	k?
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sigma	sigma	k1gNnSc1
}}	}}	k?
<g/>
\	\	kIx~
<g/>
equiv	equivit	k5eAaPmRp2nS
r_	r_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
sigma	sigma	k1gNnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
x	x	k?
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
r_	r_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
sigma	sigma	k1gNnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
y	y	k?
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
r_	r_	k?
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
sigma	sigma	k1gNnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
z	z	k7c2
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
jsme	být	k5eAaImIp1nP
tak	tak	k6eAd1
dostali	dostat	k5eAaPmAgMnP
přiřazení	přiřazení	k1gNnSc4
matice	matice	k1gFnSc2
hustoty	hustota	k1gFnSc2
qubitu	qubit	k1gInSc2
s	s	k7c7
jistým	jistý	k2eAgInSc7d1
(	(	kIx(
<g/>
Blochovým	Blochův	k2eAgInSc7d1
<g/>
)	)	kIx)
vektorem	vektor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velikost	velikost	k1gFnSc1
Blochova	Blochův	k2eAgInSc2d1
vektoru	vektor	k1gInSc2
lze	lze	k6eAd1
vždy	vždy	k6eAd1
vzít	vzít	k5eAaPmF
menší	malý	k2eAgFnPc1d2
nebo	nebo	k8xC
rovnou	rovnou	k6eAd1
jedné	jeden	k4xCgFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
tyto	tento	k3xDgInPc4
vektory	vektor	k1gInPc4
tedy	tedy	k9
zjevně	zjevně	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
jednotkovou	jednotkový	k2eAgFnSc4d1
kouli	koule	k1gFnSc4
ve	v	k7c6
třírozměrném	třírozměrný	k2eAgInSc6d1
Euklidově	Euklidův	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
není	být	k5eNaImIp3nS
těžké	těžký	k2eAgNnSc1d1
to	ten	k3xDgNnSc4
nahlédnout	nahlédnout	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
Blochův	Blochův	k2eAgInSc1d1
vektor	vektor	k1gInSc1
přiřazený	přiřazený	k2eAgInSc1d1
jistému	jistý	k2eAgInSc3d1
kvantovému	kvantový	k2eAgInSc3d1
systému	systém	k1gInSc3
je	být	k5eAaImIp3nS
jednotkový	jednotkový	k2eAgMnSc1d1
<g/>
,	,	kIx,
právě	právě	k9
když	když	k8xS
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
v	v	k7c6
čistém	čistý	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc4
jednotkové	jednotkový	k2eAgInPc1d1
Blochovy	Blochův	k2eAgInPc1d1
vektory	vektor	k1gInPc1
tvoří	tvořit	k5eAaImIp3nP
sféru	sféra	k1gFnSc4
zvanou	zvaný	k2eAgFnSc4d1
Blochova	Blochův	k2eAgFnSc1d1
sféra	sféra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blochovy	Blochův	k2eAgInPc1d1
vektory	vektor	k1gInPc1
nám	my	k3xPp1nPc3
umožňují	umožňovat	k5eAaImIp3nP
graficky	graficky	k6eAd1
vyjadřovat	vyjadřovat	k5eAaImF
působení	působení	k1gNnSc4
různých	různý	k2eAgFnPc2d1
operací	operace	k1gFnPc2
na	na	k7c4
daný	daný	k2eAgInSc4d1
qubit	qubit	k1gInSc4
a	a	k8xC
napomáhají	napomáhat	k5eAaBmIp3nP,k5eAaImIp3nP
nám	my	k3xPp1nPc3
si	se	k3xPyFc3
tak	tak	k9
představit	představit	k5eAaPmF
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
qubit	qubit	k1gMnSc1
při	při	k7c6
dané	daný	k2eAgFnSc6d1
transformaci	transformace	k1gFnSc6
mění	měnit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Realizace	realizace	k1gFnSc1
qubitů	qubita	k1gMnPc2
</s>
<s>
Kterýkoliv	kterýkoliv	k3yIgInSc1
dvouúrovňový	dvouúrovňový	k2eAgInSc1d1
kvantově	kvantově	k6eAd1
mechanický	mechanický	k2eAgInSc1d1
systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
považovat	považovat	k5eAaImF
za	za	k7c4
fyzickou	fyzický	k2eAgFnSc4d1
reprezentaci	reprezentace	k1gFnSc4
qubitu	qubit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
elektron	elektron	k1gInSc1
v	v	k7c6
obalu	obal	k1gInSc6
atomu	atom	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
buď	buď	k8xC
na	na	k7c6
základní	základní	k2eAgFnSc6d1
energetické	energetický	k2eAgFnSc6d1
hladině	hladina	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
excitován	excitován	k2eAgMnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
implementací	implementace	k1gFnSc7
qubitu	qubit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musíme	muset	k5eAaImIp1nP
ovšem	ovšem	k9
předpokládat	předpokládat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
lze	lze	k6eAd1
zanedbat	zanedbat	k5eAaPmF
excitaci	excitace	k1gFnSc4
elektronu	elektron	k1gInSc2
do	do	k7c2
vyšších	vysoký	k2eAgFnPc2d2
energetických	energetický	k2eAgFnPc2d1
hladin	hladina	k1gFnPc2
a	a	k8xC
můžeme	moct	k5eAaImIp1nP
tak	tak	k6eAd1
uvažovat	uvažovat	k5eAaImF
jen	jen	k9
základní	základní	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
první	první	k4xOgInSc4
excitovaný	excitovaný	k2eAgInSc4d1
stav	stav	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvantová	kvantový	k2eAgFnSc1d1
mechanika	mechanika	k1gFnSc1
předepisuje	předepisovat	k5eAaImIp3nS
pro	pro	k7c4
mikrosvět	mikrosvět	k1gInSc4
pravidla	pravidlo	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
lze	lze	k6eAd1
(	(	kIx(
<g/>
kupříkladu	kupříkladu	k6eAd1
přesně	přesně	k6eAd1
vyladěným	vyladěný	k2eAgInSc7d1
laserem	laser	k1gInSc7
<g/>
)	)	kIx)
uvést	uvést	k5eAaPmF
atom	atom	k1gInSc4
do	do	k7c2
stavu	stav	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
superpozicí	superpozice	k1gFnSc7
základního	základní	k2eAgInSc2d1
a	a	k8xC
excitovaného	excitovaný	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
může	moct	k5eAaImIp3nS
atom	atom	k1gInSc4
uchovávat	uchovávat	k5eAaImF
kvantovou	kvantový	k2eAgFnSc4d1
informaci	informace	k1gFnSc4
a	a	k8xC
reprezentovat	reprezentovat	k5eAaImF
jeden	jeden	k4xCgInSc4
qubit	qubit	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
je	být	k5eAaImIp3nS
kódování	kódování	k1gNnSc1
qubitů	qubit	k1gInPc2
do	do	k7c2
polarizace	polarizace	k1gFnSc2
fotonů	foton	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k9
částic	částice	k1gFnPc2
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můžeme	moct	k5eAaImIp1nP
provádět	provádět	k5eAaImF
měření	měření	k1gNnSc4
roviny	rovina	k1gFnSc2
polarizace	polarizace	k1gFnSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
postavíme	postavit	k5eAaPmIp1nP
fotonu	foton	k1gInSc3
do	do	k7c2
cesty	cesta	k1gFnSc2
filtr	filtr	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
se	se	k3xPyFc4
100	#num#	k4
<g/>
%	%	kIx~
pravděpodobností	pravděpodobnost	k1gFnSc7
projdou	projít	k5eAaPmIp3nP
jen	jen	k9
fotony	foton	k1gInPc4
s	s	k7c7
určitou	určitý	k2eAgFnSc7d1
rovinou	rovina	k1gFnSc7
polarizace	polarizace	k1gFnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc4
stav	stav	k1gInSc4
označíme	označit	k5eAaPmIp1nP
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
Fotony	foton	k1gInPc1
polarizované	polarizovaný	k2eAgInPc1d1
kolmo	kolmo	k6eAd1
na	na	k7c4
rovinu	rovina	k1gFnSc4
filtru	filtr	k1gInSc2
neprojdou	projít	k5eNaPmIp3nP
vůbec	vůbec	k9
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
označíme	označit	k5eAaPmIp1nP
jako	jako	k8xS,k8xC
stav	stav	k1gInSc4
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
Fotony	foton	k1gInPc1
polarizované	polarizovaný	k2eAgInPc1d1
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
rovinách	rovina	k1gFnPc6
se	se	k3xPyFc4
budou	být	k5eAaImBp3nP
chovat	chovat	k5eAaImF
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
bychom	by	kYmCp1nP
měřili	měřit	k5eAaImAgMnP
qubit	qubit	k5eAaImF,k5eAaBmF,k5eAaPmF
v	v	k7c6
superponovaném	superponovaný	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jmenovitě	jmenovitě	k6eAd1
tedy	tedy	k9
<g/>
,	,	kIx,
mějme	mít	k5eAaImRp1nP
foton	foton	k1gInSc4
polarizovaný	polarizovaný	k2eAgInSc4d1
v	v	k7c6
rovině	rovina	k1gFnSc6
svírající	svírající	k2eAgInSc4d1
úhel	úhel	k1gInSc4
</s>
<s>
φ	φ	k?
</s>
<s>
∈	∈	k?
</s>
<s>
⟨	⟨	k?
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
π	π	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varphi	k1gNnSc7
\	\	kIx~
<g/>
in	in	k?
\	\	kIx~
<g/>
langle	langle	k1gInSc1
0	#num#	k4
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
pi	pi	k0
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
s	s	k7c7
rovinou	rovina	k1gFnSc7
odpovídající	odpovídající	k2eAgFnSc2d1
stavu	stav	k1gInSc3
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
Stav	stav	k1gInSc1
fotonu	foton	k1gInSc2
</s>
<s>
|	|	kIx~
</s>
<s>
φ	φ	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
varphi	varphi	k6eAd1
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
můžeme	moct	k5eAaImIp1nP
popsat	popsat	k5eAaPmF
jako	jako	k9
lineární	lineární	k2eAgFnSc4d1
kombinaci	kombinace	k1gFnSc4
</s>
<s>
|	|	kIx~
</s>
<s>
φ	φ	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
=	=	kIx~
</s>
<s>
α	α	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
+	+	kIx~
</s>
<s>
β	β	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
,	,	kIx,
</s>
<s>
a	a	k8xC
</s>
<s>
=	=	kIx~
</s>
<s>
sin	sin	kA
</s>
<s>
φ	φ	k?
</s>
<s>
,	,	kIx,
</s>
<s>
b	b	k?
</s>
<s>
=	=	kIx~
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
φ	φ	k?
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
varphi	varphi	k6eAd1
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
=	=	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gMnSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
+	+	kIx~
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc2
,	,	kIx,
<g/>
\	\	kIx~
<g/>
quad	quad	k6eAd1
a	a	k8xC
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sin	sin	kA
\	\	kIx~
<g/>
varphi	varph	k1gMnSc3
,	,	kIx,
<g/>
\	\	kIx~
b	b	k?
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
cos	cos	kA
\	\	kIx~
<g/>
varphi	varphi	k6eAd1
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Podle	podle	k7c2
úhlu	úhel	k1gInSc2
polarizace	polarizace	k1gFnSc2
</s>
<s>
φ	φ	k?
</s>
<s>
∈	∈	k?
</s>
<s>
⟨	⟨	k?
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
π	π	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varphi	k1gNnSc7
\	\	kIx~
<g/>
in	in	k?
\	\	kIx~
<g/>
langle	langle	k1gInSc1
0	#num#	k4
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
pi	pi	k0
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
amplituda	amplituda	k1gFnSc1
pravděpodobnosti	pravděpodobnost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
foton	foton	k1gInSc1
projde	projít	k5eAaPmIp3nS
či	či	k8xC
neprojde	projít	k5eNaPmIp3nS
filtrem	filtr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc2
techniky	technika	k1gFnSc2
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
kvantová	kvantový	k2eAgFnSc1d1
kryptografie	kryptografie	k1gFnSc1
k	k	k7c3
bezpečnému	bezpečný	k2eAgInSc3d1
přenosu	přenos	k1gInSc3
informace	informace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Operace	operace	k1gFnSc1
s	s	k7c7
qubity	qubit	k1gInPc7
</s>
<s>
Zatímco	zatímco	k8xS
pro	pro	k7c4
operace	operace	k1gFnPc4
s	s	k7c7
klasickým	klasický	k2eAgInSc7d1
bity	bit	k1gInPc7
používáme	používat	k5eAaImIp1nP
logické	logický	k2eAgInPc1d1
operace	operace	k1gFnPc4
jako	jako	k8xS,k8xC
NOT	nota	k1gFnPc2
<g/>
,	,	kIx,
AND	Anda	k1gFnPc2
či	či	k8xC
OR	OR	kA
<g/>
,	,	kIx,
operace	operace	k1gFnPc4
prováděné	prováděný	k2eAgFnPc4d1
nad	nad	k7c4
qubity	qubita	k1gMnPc4
lze	lze	k6eAd1
popisovat	popisovat	k5eAaImF
pomocí	pomocí	k7c2
tzv.	tzv.	kA
unitárních	unitární	k2eAgFnPc2d1
operací	operace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
aplikaci	aplikace	k1gFnSc4
si	se	k3xPyFc3
tak	tak	k9
lze	lze	k6eAd1
obvykle	obvykle	k6eAd1
představovat	představovat	k5eAaImF
jako	jako	k9
rotace	rotace	k1gFnSc1
v	v	k7c6
odpovídajícím	odpovídající	k2eAgInSc6d1
Hilbertově	Hilbertův	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protějšek	protějšek	k1gInSc1
k	k	k7c3
těmto	tento	k3xDgFnPc3
operacím	operace	k1gFnPc3
pak	pak	k6eAd1
tvoří	tvořit	k5eAaImIp3nS
kvantové	kvantový	k2eAgNnSc1d1
měření	měření	k1gNnSc1
qubitů	qubita	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Měření	měření	k1gNnSc1
qubitů	qubit	k1gInPc2
</s>
<s>
Hodnotu	hodnota	k1gFnSc4
qubitu	qubit	k1gInSc2
můžeme	moct	k5eAaImIp1nP
zjišťovat	zjišťovat	k5eAaImF
měřením	měření	k1gNnSc7
<g/>
,	,	kIx,
kterým	který	k3yRgNnSc7,k3yIgNnSc7,k3yQgNnSc7
vždy	vždy	k6eAd1
dostaneme	dostat	k5eAaPmIp1nP
buď	buď	k8xC
hodnotu	hodnota	k1gFnSc4
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
,	,	kIx,
nebo	nebo	k8xC
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
Žádným	žádný	k3yNgNnSc7
měřením	měření	k1gNnSc7
není	být	k5eNaImIp3nS
možno	možno	k6eAd1
zjistit	zjistit	k5eAaPmF
čísla	číslo	k1gNnPc4
</s>
<s>
α	α	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
alpha	alpha	k1gMnSc1
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
β	β	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
}	}	kIx)
</s>
<s>
ve	v	k7c6
výrazu	výraz	k1gInSc6
</s>
<s>
α	α	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
+	+	kIx~
</s>
<s>
β	β	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
alpha	alpha	k1gMnSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
+	+	kIx~
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
qubit	qubit	k5eAaImF,k5eAaBmF,k5eAaPmF
přesně	přesně	k6eAd1
v	v	k7c6
jednom	jeden	k4xCgInSc6
ze	z	k7c2
základních	základní	k2eAgInPc2d1
stavů	stav	k1gInPc2
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
či	či	k8xC
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
,	,	kIx,
vybere	vybrat	k5eAaPmIp3nS
se	se	k3xPyFc4
výsledná	výsledný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
náhodně	náhodně	k6eAd1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
pravděpodobnost	pravděpodobnost	k1gFnSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
výsledků	výsledek	k1gInPc2
určují	určovat	k5eAaImIp3nP
amplitudy	amplituda	k1gFnPc1
</s>
<s>
α	α	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
alpha	alpha	k1gMnSc1
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
β	β	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
}	}	kIx)
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1
naměření	naměření	k1gNnSc2
stavu	stav	k1gInSc2
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
</s>
<s>
|	|	kIx~
</s>
<s>
α	α	k?
</s>
<s>
|	|	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
|	|	kIx~
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
pravděpodobnost	pravděpodobnost	k1gFnSc1
naměření	naměření	k1gNnSc2
stavu	stav	k1gInSc2
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
</s>
<s>
|	|	kIx~
</s>
<s>
β	β	k?
</s>
<s>
|	|	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
|	|	kIx~
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
Součet	součet	k1gInSc1
těchto	tento	k3xDgFnPc2
dvou	dva	k4xCgFnPc2
pravděpodobností	pravděpodobnost	k1gFnPc2
musí	muset	k5eAaImIp3nS
dávat	dávat	k5eAaImF
1	#num#	k4
<g/>
,	,	kIx,
neboť	neboť	k8xC
jde	jít	k5eAaImIp3nS
o	o	k7c4
pravděpodobnost	pravděpodobnost	k1gFnSc4
jistého	jistý	k2eAgInSc2d1
jevu	jev	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
<g/>
-li	-li	k?
obě	dva	k4xCgFnPc1
amplitudy	amplituda	k1gFnPc1
stejně	stejně	k6eAd1
velké	velký	k2eAgFnPc1d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
</s>
<s>
|	|	kIx~
</s>
<s>
α	α	k?
</s>
<s>
|	|	kIx~
</s>
<s>
=	=	kIx~
</s>
<s>
|	|	kIx~
</s>
<s>
β	β	k?
</s>
<s>
|	|	kIx~
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
|	|	kIx~
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
|	|	kIx~
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
pak	pak	k6eAd1
je	být	k5eAaImIp3nS
pravděpodobnost	pravděpodobnost	k1gFnSc1
nalezení	nalezení	k1gNnSc4
kteréhokoli	kterýkoli	k3yIgInSc2
ze	z	k7c2
stavů	stav	k1gInPc2
rovna	roven	k2eAgFnSc1d1
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
a	a	k8xC
výsledek	výsledek	k1gInSc1
měření	měření	k1gNnSc2
je	být	k5eAaImIp3nS
tedy	tedy	k9
zcela	zcela	k6eAd1
náhodný	náhodný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Podstatným	podstatný	k2eAgInSc7d1
důsledkem	důsledek	k1gInSc7
kvantové	kvantový	k2eAgFnSc2d1
mechaniky	mechanika	k1gFnSc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
stav	stav	k1gInSc1
qubitu	qubit	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
provedeme	provést	k5eAaPmIp1nP
měření	měření	k1gNnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
zákonitě	zákonitě	k6eAd1
změní	změnit	k5eAaPmIp3nS
na	na	k7c4
ten	ten	k3xDgInSc4
stav	stav	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
výsledkem	výsledek	k1gInSc7
měření	měření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinými	jiný	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
<g/>
,	,	kIx,
měření	měření	k1gNnSc1
ničí	ničit	k5eAaImIp3nS
stav	stav	k1gInSc4
superpozice	superpozice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
případ	případ	k1gInSc4
obecnějšího	obecní	k2eAgInSc2d2
jevu	jev	k1gInSc2
<g/>
,	,	kIx,
kterému	který	k3yQgInSc3,k3yRgInSc3,k3yIgInSc3
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
kolaps	kolaps	k1gInSc1
vlnové	vlnový	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolaps	kolaps	k1gInSc1
vlnové	vlnový	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
při	při	k7c6
kvantovém	kvantový	k2eAgNnSc6d1
měření	měření	k1gNnSc6
odpovídá	odpovídat	k5eAaImIp3nS
projekci	projekce	k1gFnSc4
na	na	k7c4
osu	osa	k1gFnSc4
odpovídající	odpovídající	k2eAgFnSc4d1
některému	některý	k3yIgInSc3
z	z	k7c2
bázových	bázový	k2eAgInPc2d1
stavů	stav	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
obtížné	obtížný	k2eAgNnSc1d1
si	se	k3xPyFc3
tento	tento	k3xDgInSc4
specifický	specifický	k2eAgInSc4d1
pojem	pojem	k1gInSc4
z	z	k7c2
kvantové	kvantový	k2eAgFnSc2d1
mechaniky	mechanika	k1gFnSc2
přirozeně	přirozeně	k6eAd1
představit	představit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
změřením	změření	k1gNnSc7
je	být	k5eAaImIp3nS
kvantový	kvantový	k2eAgInSc1d1
systém	systém	k1gInSc1
implementující	implementující	k2eAgInSc4d1
qubit	qubit	k1gInSc4
ve	v	k7c6
stavu	stav	k1gInSc6
superpozice	superpozice	k1gFnSc2
stavů	stav	k1gInPc2
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
,	,	kIx,
není	být	k5eNaImIp3nS
ale	ale	k8xC
ani	ani	k8xC
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analogií	analogie	k1gFnPc2
ke	k	k7c3
qubitu	qubit	k1gInSc3
by	by	kYmCp3nP
v	v	k7c6
klasickém	klasický	k2eAgInSc6d1
počítači	počítač	k1gInSc6
byl	být	k5eAaImAgInS
pravděpodobnostní	pravděpodobnostní	k2eAgInSc1d1
bit	bit	k1gInSc1
-	-	kIx~
bit	bit	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
s	s	k7c7
určitou	určitý	k2eAgFnSc7d1
pravděpodobností	pravděpodobnost	k1gFnSc7
ve	v	k7c6
stavu	stav	k1gInSc6
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
a	a	k8xC
jinak	jinak	k6eAd1
je	být	k5eAaImIp3nS
ve	v	k7c6
stavu	stav	k1gInSc6
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
Podle	podle	k7c2
kvantové	kvantový	k2eAgFnSc2d1
mechaniky	mechanika	k1gFnSc2
však	však	k9
stav	stav	k1gInSc1
bitu	bit	k1gInSc2
není	být	k5eNaImIp3nS
popsán	popsat	k5eAaPmNgInS
přímo	přímo	k6eAd1
pravděpodobnostmi	pravděpodobnost	k1gFnPc7
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
zmíněnými	zmíněný	k2eAgFnPc7d1
amplitudami	amplituda	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
amplitudy	amplituda	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
i	i	k9
záporné	záporný	k2eAgFnPc4d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
pravděpodobnosti	pravděpodobnost	k1gFnPc4
nikoli	nikoli	k9
<g/>
,	,	kIx,
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
v	v	k7c6
kvantově	kvantově	k6eAd1
mechanických	mechanický	k2eAgInPc6d1
algoritmech	algoritmus	k1gInPc6
a	a	k8xC
umožňuje	umožňovat	k5eAaImIp3nS
jim	on	k3xPp3gFnPc3
rychle	rychle	k6eAd1
řešit	řešit	k5eAaImF
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
časově	časově	k6eAd1
efektivní	efektivní	k2eAgNnPc4d1
řešení	řešení	k1gNnPc4
na	na	k7c6
klasických	klasický	k2eAgInPc6d1
počítačích	počítač	k1gInPc6
používajících	používající	k2eAgInPc6d1
pouze	pouze	k6eAd1
náhodné	náhodný	k2eAgInPc4d1
bity	bit	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
algoritmus	algoritmus	k1gInSc1
pro	pro	k7c4
tzv.	tzv.	kA
Fourierovu	Fourierův	k2eAgFnSc4d1
transformaci	transformace	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gInSc6
základě	základ	k1gInSc6
funguje	fungovat	k5eAaImIp3nS
kvantový	kvantový	k2eAgInSc1d1
Shorův	Shorův	k2eAgInSc1d1
algoritmus	algoritmus	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
rychle	rychle	k6eAd1
rozložit	rozložit	k5eAaPmF
dané	daný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
na	na	k7c4
prvočísla	prvočíslo	k1gNnPc4
(	(	kIx(
<g/>
např.	např.	kA
</s>
<s>
15	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
×	×	k?
</s>
<s>
5	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
15	#num#	k4
<g/>
=	=	kIx~
<g/>
3	#num#	k4
<g/>
\	\	kIx~
<g/>
times	times	k1gInSc1
5	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kvantová	kvantový	k2eAgFnSc1d1
brána	brána	k1gFnSc1
CNOT	CNOT	kA
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP
systém	systém	k1gInSc4
dvou	dva	k4xCgInPc2
qubitů	qubit	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpovídající	odpovídající	k2eAgFnSc4d1
ortonormální	ortonormální	k2eAgFnSc4d1
bázi	báze	k1gFnSc4
volíme	volit	k5eAaImIp1nP
tvaru	tvar	k1gInSc3
vyobrazeného	vyobrazený	k2eAgNnSc2d1
výše	výše	k1gFnSc2,k1gFnSc2wB
</s>
<s>
{	{	kIx(
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
,	,	kIx,
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
,	,	kIx,
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
,	,	kIx,
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
}	}	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
{	{	kIx(
<g/>
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
indexy	index	k1gInPc4
</s>
<s>
K	k	k7c3
</s>
<s>
,	,	kIx,
</s>
<s>
C	C	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
K	k	k7c3
<g/>
,	,	kIx,
<g/>
C	C	kA
<g/>
}	}	kIx)
</s>
<s>
zdůrazněno	zdůrazněn	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
první	první	k4xOgFnSc1
složka	složka	k1gFnSc1
složeného	složený	k2eAgInSc2d1
vektoru	vektor	k1gInSc2
působí	působit	k5eAaImIp3nS
na	na	k7c6
systému	systém	k1gInSc6
prvního	první	k4xOgInSc2
qubitu	qubit	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
si	se	k3xPyFc3
označíme	označit	k5eAaPmIp1nP
K	k	k7c3
<g/>
,	,	kIx,
a	a	k8xC
druhá	druhý	k4xOgFnSc1
složka	složka	k1gFnSc1
působí	působit	k5eAaImIp3nS
na	na	k7c6
systému	systém	k1gInSc6
qubitu	qubit	k1gInSc2
druhého	druhý	k4xOgNnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
si	se	k3xPyFc3
podobně	podobně	k6eAd1
označíme	označit	k5eAaPmIp1nP
jako	jako	k9
C.	C.	kA
</s>
<s>
Jednou	jeden	k4xCgFnSc7
z	z	k7c2
velmi	velmi	k6eAd1
často	často	k6eAd1
užívanou	užívaný	k2eAgFnSc7d1
logickou	logický	k2eAgFnSc7d1
operací	operace	k1gFnSc7
nad	nad	k7c7
dvěma	dva	k4xCgMnPc7
qubity	qubita	k1gMnPc7
je	být	k5eAaImIp3nS
operace	operace	k1gFnPc4
CNOT	CNOT	kA
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
controlled	controlled	k1gInSc1
NOT	nota	k1gFnPc2
-	-	kIx~
kontrolované	kontrolovaný	k2eAgFnSc2d1
NOT	nota	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gMnSc1
funkci	funkce	k1gFnSc4
lze	lze	k6eAd1
popsat	popsat	k5eAaPmF
následovně	následovně	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
→	→	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
→	→	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
→	→	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
→	→	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k1gMnSc1
<g/>
{	{	kIx(
<g/>
aligned	aligned	k1gMnSc1
<g/>
}	}	kIx)
<g/>
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
&	&	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
\	\	kIx~
<g/>
quad	quad	k6eAd1
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
\	\	kIx~
<g/>
quad	quad	k1gInSc1
&	&	k?
<g/>
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
\\	\\	k?
<g/>
|	|	kIx~
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
&	&	k?
<g/>
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
\	\	kIx~
<g/>
quad	quad	k1gInSc1
&	&	k?
<g/>
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
\\	\\	k?
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
&	&	k?
<g/>
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
\	\	kIx~
<g/>
quad	quad	k1gInSc1
&	&	k?
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gNnSc2
_	_	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
\\	\\	k?
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
&	&	k?
<g/>
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
\	\	kIx~
<g/>
quad	quad	k1gInSc1
&	&	k?
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
aligned	aligned	k1gMnSc1
<g/>
}}}	}}}	k?
</s>
<s>
Qubit	Qubit	k1gInSc1
pod	pod	k7c7
označením	označení	k1gNnSc7
K	K	kA
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
kontrolní	kontrolní	k2eAgFnSc1d1
(	(	kIx(
<g/>
angl.	angl.	k?
control	control	k1gInSc1
qubit	qubit	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
qubit	qubit	k1gInSc1
označený	označený	k2eAgInSc1d1
jako	jako	k8xS,k8xC
C	C	kA
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
cílový	cílový	k2eAgInSc1d1
(	(	kIx(
<g/>
angl.	angl.	k?
target	target	k1gMnSc1
qubit	qubit	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovy	slovo	k1gNnPc7
lze	lze	k6eAd1
tedy	tedy	k9
působení	působení	k1gNnSc1
CNOT	CNOT	kA
operace	operace	k1gFnSc2
formulovat	formulovat	k5eAaImF
takto	takto	k6eAd1
<g/>
:	:	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
kontrolní	kontrolní	k2eAgInSc4d1
qubit	qubit	k1gInSc4
ve	v	k7c6
stavu	stav	k1gInSc6
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
,	,	kIx,
tak	tak	k6eAd1
proveď	provést	k5eAaPmRp2nS
u	u	k7c2
cílového	cílový	k2eAgInSc2d1
qubitu	qubit	k1gInSc2
záměnu	záměna	k1gFnSc4
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
↔	↔	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gNnSc2
\	\	kIx~
<g/>
leftrightarrow	leftrightarrow	k?
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
,	,	kIx,
jinak	jinak	k6eAd1
nedělej	dělat	k5eNaImRp2nS
nic	nic	k3yNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formálně	formálně	k6eAd1
je	být	k5eAaImIp3nS
tedy	tedy	k8xC
CNOT	CNOT	kA
totožné	totožný	k2eAgInPc4d1
s	s	k7c7
operací	operace	k1gFnSc7
XOR	XOR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
ji	on	k3xPp3gFnSc4
kompaktně	kompaktně	k6eAd1
popsat	popsat	k5eAaPmF
jako	jako	k8xC,k8xS
</s>
<s>
|	|	kIx~
</s>
<s>
A	a	k9
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
B	B	kA
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
→	→	k?
</s>
<s>
|	|	kIx~
</s>
<s>
A	a	k9
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
A	a	k9
</s>
<s>
⊕	⊕	k?
</s>
<s>
B	B	kA
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
,	,	kIx,
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
|	|	kIx~
<g/>
A	a	k9
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gMnSc1
|	|	kIx~
<g/>
B	B	kA
<g/>
\	\	kIx~
<g/>
rangle	rangl	k1gMnSc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
quad	quad	k6eAd1
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
\	\	kIx~
<g/>
quad	quad	k6eAd1
|	|	kIx~
<g/>
A	a	k9
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gMnSc1
|	|	kIx~
<g/>
A	A	kA
<g/>
\	\	kIx~
<g/>
oplus	oplus	k1gMnSc1
B	B	kA
<g/>
\	\	kIx~
<g/>
rangle	rangl	k1gMnSc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
⊕	⊕	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
oplus	oplus	k1gInSc4
}	}	kIx)
</s>
<s>
značí	značit	k5eAaImIp3nS
sčítání	sčítání	k1gNnSc1
modulo	modula	k1gFnSc5
dvěma	dva	k4xCgFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působení	působení	k1gNnSc1
CNOT	CNOT	kA
na	na	k7c4
libovolný	libovolný	k2eAgInSc4d1
pár	pár	k4xCyI
qubitů	qubit	k1gInPc2
pak	pak	k6eAd1
vyplývá	vyplývat	k5eAaImIp3nS
z	z	k7c2
bilinearity	bilinearita	k1gFnSc2
tohoto	tento	k3xDgNnSc2
zobrazení	zobrazení	k1gNnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
</s>
<s>
|	|	kIx~
</s>
<s>
ψ	ψ	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
=	=	kIx~
</s>
<s>
α	α	k?
</s>
<s>
00	#num#	k4
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
+	+	kIx~
</s>
<s>
α	α	k?
</s>
<s>
01	#num#	k4
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
+	+	kIx~
</s>
<s>
α	α	k?
</s>
<s>
10	#num#	k4
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
+	+	kIx~
</s>
<s>
α	α	k?
</s>
<s>
11	#num#	k4
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
→	→	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
psi	pes	k1gMnPc1
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
=	=	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
rangle	rangle	k1gNnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
10	#num#	k4
<g/>
}	}	kIx)
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
C	C	kA
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
11	#num#	k4
<g/>
}	}	kIx)
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
quad	quad	k6eAd1
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
}	}	kIx)
</s>
<s>
→	→	k?
</s>
<s>
|	|	kIx~
</s>
<s>
ψ	ψ	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
CNOT	CNOT	kA
</s>
<s>
=	=	kIx~
</s>
<s>
α	α	k?
</s>
<s>
00	#num#	k4
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
+	+	kIx~
</s>
<s>
α	α	k?
</s>
<s>
01	#num#	k4
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
+	+	kIx~
</s>
<s>
α	α	k?
</s>
<s>
10	#num#	k4
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
+	+	kIx~
</s>
<s>
α	α	k?
</s>
<s>
11	#num#	k4
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
K	k	k7c3
</s>
<s>
⊗	⊗	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
C	C	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
\	\	kIx~
<g/>
quad	quad	k6eAd1
|	|	kIx~
<g/>
\	\	kIx~
<g/>
psi	pes	k1gMnPc1
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
text	text	k1gInSc1
<g/>
{	{	kIx(
<g/>
CNOT	CNOT	kA
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
|	|	kIx~
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
rangle	rangle	k1gNnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
10	#num#	k4
<g/>
}	}	kIx)
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gNnSc2
_	_	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
11	#num#	k4
<g/>
}	}	kIx)
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
otimes	otimes	k1gInSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
C	C	kA
<g/>
}}	}}	k?
</s>
<s>
V	v	k7c6
maticové	maticový	k2eAgFnSc6d1
reprezentaci	reprezentace	k1gFnSc6
v	v	k7c4
námi	my	k3xPp1nPc7
zvolené	zvolený	k2eAgNnSc4d1
bázi	báze	k1gFnSc6
můžeme	moct	k5eAaImIp1nP
CNOT	CNOT	kA
operaci	operace	k1gFnSc4
zapsat	zapsat	k5eAaPmF
jako	jako	k9
unitární	unitární	k2eAgFnSc4d1
matici	matice	k1gFnSc4
</s>
<s>
U	u	k7c2
</s>
<s>
CNOT	CNOT	kA
</s>
<s>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
U_	U_	k1gMnPc6
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
text	text	k1gInSc1
<g/>
{	{	kIx(
<g/>
CNOT	CNOT	kA
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k1gInSc1
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}	}	kIx)
<g/>
1	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
\\	\\	k?
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
&	&	k?
<g/>
1	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
\\	\\	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
1	#num#	k4
<g/>
\\	\\	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
1	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
\\\	\\\	k?
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}}}	}}}	k?
</s>
<s>
Její	její	k3xOp3gNnSc1
působení	působení	k1gNnSc1
tak	tak	k9
vypadá	vypadat	k5eAaPmIp3nS,k5eAaImIp3nS
následovně	následovně	k6eAd1
</s>
<s>
|	|	kIx~
</s>
<s>
ψ	ψ	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
α	α	k?
</s>
<s>
00	#num#	k4
</s>
<s>
α	α	k?
</s>
<s>
01	#num#	k4
</s>
<s>
α	α	k?
</s>
<s>
10	#num#	k4
</s>
<s>
α	α	k?
</s>
<s>
11	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
→	→	k?
</s>
<s>
U	u	k7c2
</s>
<s>
CNOT	CNOT	kA
</s>
<s>
|	|	kIx~
</s>
<s>
ψ	ψ	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
α	α	k?
</s>
<s>
00	#num#	k4
</s>
<s>
α	α	k?
</s>
<s>
01	#num#	k4
</s>
<s>
α	α	k?
</s>
<s>
10	#num#	k4
</s>
<s>
α	α	k?
</s>
<s>
11	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
α	α	k?
</s>
<s>
00	#num#	k4
</s>
<s>
α	α	k?
</s>
<s>
01	#num#	k4
</s>
<s>
α	α	k?
</s>
<s>
11	#num#	k4
</s>
<s>
α	α	k?
</s>
<s>
10	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
psi	pes	k1gMnPc1
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k2eAgInSc4d1
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
\\\	\\\	k?
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
}	}	kIx)
<g/>
\\\	\\\	k?
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
10	#num#	k4
<g/>
}	}	kIx)
<g/>
\\\	\\\	k?
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
11	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
quad	quad	k6eAd1
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
\	\	kIx~
<g/>
quad	quad	k1gInSc1
U_	U_	k1gFnSc2
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
text	text	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
{	{	kIx(
<g/>
CNOT	CNOT	kA
<g/>
}}	}}	k?
<g/>
|	|	kIx~
<g/>
\	\	kIx~
<g/>
psi	pes	k1gMnPc1
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k1gInSc1
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}	}	kIx)
<g/>
1	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
\\	\\	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
\\	\\	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
1	#num#	k4
<g/>
\\	\\	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
1	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
\\\	\\\	k?
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}}	}}	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k2eAgInSc4d1
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
\\\	\\\	k?
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
\\\	\\\	k?
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
10	#num#	k4
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
\\\	\\\	k?
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
11	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k2eAgInSc4d1
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
\\\	\\\	k?
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
\\\	\\\	k?
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
11	#num#	k4
<g/>
}	}	kIx)
<g/>
\\\	\\\	k?
<g/>
alpha	alpha	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
10	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
pmatrix	pmatrix	k1gInSc1
<g/>
}}}	}}}	k?
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k9
všechny	všechen	k3xTgFnPc4
unitární	unitární	k2eAgFnPc4d1
operace	operace	k1gFnPc4
je	být	k5eAaImIp3nS
i	i	k9
CNOT	CNOT	kA
reverzibilní	reverzibilní	k2eAgNnSc1d1
logické	logický	k2eAgNnSc1d1
hradlo	hradlo	k1gNnSc1
<g/>
,	,	kIx,
tzn.	tzn.	kA
z	z	k7c2
výsledku	výsledek	k1gInSc2
jsme	být	k5eAaImIp1nP
schopni	schopen	k2eAgMnPc1d1
jednoznačně	jednoznačně	k6eAd1
rekonstruovat	rekonstruovat	k5eAaBmF
vstupní	vstupní	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
qubitů	qubita	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
obecně	obecně	k6eAd1
u	u	k7c2
klasických	klasický	k2eAgNnPc2d1
hradel	hradlo	k1gNnPc2
neplatí	platit	k5eNaImIp3nS
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
např.	např.	kA
AND	Anda	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
část	část	k1gFnSc1
informace	informace	k1gFnPc4
o	o	k7c6
vstupech	vstup	k1gInPc6
ztrácí	ztrácet	k5eAaImIp3nS
<g/>
,	,	kIx,
neboť	neboť	k8xC
jednomu	jeden	k4xCgMnSc3
výstupu	výstup	k1gInSc3
obecně	obecně	k6eAd1
odpovídá	odpovídat	k5eAaImIp3nS
více	hodně	k6eAd2
vstupních	vstupní	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přímé	přímý	k2eAgNnSc1d1
kvantové	kvantový	k2eAgNnSc1d1
zobecnění	zobecnění	k1gNnSc1
podobných	podobný	k2eAgNnPc2d1
ireverzibilních	ireverzibilní	k2eAgNnPc2d1
hradel	hradlo	k1gNnPc2
tak	tak	k6eAd1
není	být	k5eNaImIp3nS
možné	možný	k2eAgInPc4d1
a	a	k8xC
kvantové	kvantový	k2eAgInPc4d1
počítače	počítač	k1gInPc4
tak	tak	k6eAd1
vždy	vždy	k6eAd1
používají	používat	k5eAaImIp3nP
reverzibilní	reverzibilní	k2eAgFnPc1d1
varianty	varianta	k1gFnPc1
klasických	klasický	k2eAgNnPc2d1
hradel	hradlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Význam	význam	k1gInSc1
CNOT	CNOT	kA
operace	operace	k1gFnSc2
tkví	tkvět	k5eAaImIp3nS
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
důležitém	důležitý	k2eAgInSc6d1
faktu	fakt	k1gInSc6
<g/>
:	:	kIx,
</s>
<s>
Každé	každý	k3xTgNnSc1
vícequbitové	vícequbitový	k2eAgNnSc1d1
logické	logický	k2eAgNnSc1d1
hradlo	hradlo	k1gNnSc1
lze	lze	k6eAd1
vyjádřit	vyjádřit	k5eAaPmF
pomocí	pomocí	k7c2
CNOT	CNOT	kA
hradla	hradlo	k1gNnSc2
a	a	k8xC
jednoqubitových	jednoqubitový	k2eAgNnPc2d1
hradel	hradlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
do	do	k7c2
více	hodně	k6eAd2
dimenzí	dimenze	k1gFnPc2
</s>
<s>
Qubit	Qubit	k1gInSc1
je	být	k5eAaImIp3nS
kvantový	kvantový	k2eAgInSc1d1
systém	systém	k1gInSc1
s	s	k7c7
dvourozměrným	dvourozměrný	k2eAgInSc7d1
Hilbertovým	Hilbertův	k2eAgInSc7d1
stavovým	stavový	k2eAgInSc7d1
prostorem	prostor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvažujeme	uvažovat	k5eAaImIp1nP
<g/>
-li	-li	k?
vícerozměrné	vícerozměrný	k2eAgFnPc4d1
stavové	stavový	k2eAgFnPc4d1
prostory	prostora	k1gFnPc4
<g/>
,	,	kIx,
hovoříme	hovořit	k5eAaImIp1nP
o	o	k7c4
quditu	qudita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
i	i	k9
konkrétní	konkrétní	k2eAgInPc1d1
názvy	název	k1gInPc1
pro	pro	k7c4
konkrétní	konkrétní	k2eAgFnSc4d1
dimenzi	dimenze	k1gFnSc4
<g/>
,	,	kIx,
např.	např.	kA
pro	pro	k7c4
dimenzi	dimenze	k1gFnSc4
rovnou	rovnou	k6eAd1
třem	tři	k4xCgFnPc3
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
termínu	termín	k1gInSc2
qutrit	qutrita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
uvažujeme	uvažovat	k5eAaImIp1nP
Hilbertův	Hilbertův	k2eAgInSc4d1
prostor	prostor	k1gInSc4
</s>
<s>
H	H	kA
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathcal	mathcat	k5eAaPmAgInS
{	{	kIx(
<g/>
H	H	kA
<g/>
}}	}}	k?
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
s	s	k7c7
</s>
<s>
dim	dim	k?
</s>
<s>
H	H	kA
</s>
<s>
3	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
dim	dim	k?
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathcal	mathcat	k5eAaPmAgInS
{	{	kIx(
<g/>
H	H	kA
<g/>
}}	}}	k?
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
3	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
ortonormální	ortonormální	k2eAgFnSc7d1
bází	báze	k1gFnSc7
</s>
<s>
{	{	kIx(
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
,	,	kIx,
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
,	,	kIx,
</s>
<s>
|	|	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
}	}	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
{	{	kIx(
<g/>
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc2
,	,	kIx,
<g/>
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc2
,	,	kIx,
<g/>
|	|	kIx~
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
\	\	kIx~
<g/>
}}	}}	k?
</s>
<s>
Qutrit	Qutrit	k1gInSc1
</s>
<s>
|	|	kIx~
</s>
<s>
ψ	ψ	k?
</s>
<s>
3	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
∈	∈	k?
</s>
<s>
H	H	kA
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
psi	pes	k1gMnPc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
\	\	kIx~
<g/>
in	in	k?
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathcal	mathcat	k5eAaPmAgInS
{	{	kIx(
<g/>
H	H	kA
<g/>
}}	}}	k?
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
má	mít	k5eAaImIp3nS
tedy	tedy	k9
tvar	tvar	k1gInSc4
</s>
<s>
|	|	kIx~
</s>
<s>
ψ	ψ	k?
</s>
<s>
3	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
=	=	kIx~
</s>
<s>
α	α	k?
</s>
<s>
|	|	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
+	+	kIx~
</s>
<s>
β	β	k?
</s>
<s>
|	|	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
+	+	kIx~
</s>
<s>
γ	γ	k?
</s>
<s>
|	|	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
⟩	⟩	k?
</s>
<s>
,	,	kIx,
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
psi	pes	k1gMnPc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
=	=	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gMnSc1
|	|	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
+	+	kIx~
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
|	|	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
+	+	kIx~
<g/>
\	\	kIx~
<g/>
gamma	gamma	k1gFnSc1
|	|	kIx~
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc2
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
α	α	k?
</s>
<s>
,	,	kIx,
</s>
<s>
β	β	k?
</s>
<s>
,	,	kIx,
</s>
<s>
γ	γ	k?
</s>
<s>
∈	∈	k?
</s>
<s>
C	C	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
\	\	kIx~
<g/>
alpha	alpha	k1gMnSc1
,	,	kIx,
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
,	,	kIx,
<g/>
\	\	kIx~
<g/>
gamma	gamma	k1gFnSc1
\	\	kIx~
<g/>
in	in	k?
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
splňují	splňovat	k5eAaImIp3nP
normalizační	normalizační	k2eAgFnSc4d1
podmínku	podmínka	k1gFnSc4
</s>
<s>
|	|	kIx~
</s>
<s>
α	α	k?
</s>
<s>
|	|	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
|	|	kIx~
</s>
<s>
β	β	k?
</s>
<s>
|	|	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
|	|	kIx~
</s>
<s>
γ	γ	k?
</s>
<s>
|	|	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
|	|	kIx~
<g/>
\	\	kIx~
<g/>
alpha	alpha	k1gFnSc1
|	|	kIx~
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
|	|	kIx~
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
|	|	kIx~
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
|	|	kIx~
<g/>
\	\	kIx~
<g/>
gamma	gamma	k1gFnSc1
|	|	kIx~
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
Daleko	daleko	k6eAd1
nejpoužívanějším	používaný	k2eAgInSc7d3
objektem	objekt	k1gInSc7
v	v	k7c6
teorii	teorie	k1gFnSc6
kvantové	kvantový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
však	však	k9
zatím	zatím	k6eAd1
stále	stále	k6eAd1
zůstává	zůstávat	k5eAaImIp3nS
qubit	qubit	k5eAaImF,k5eAaPmF,k5eAaBmF
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SCHUMACHER	SCHUMACHER	kA
<g/>
,	,	kIx,
Benjamin	Benjamin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Quantum	Quantum	k1gNnSc1
coding	coding	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Physical	Physical	k1gFnSc1
Review	Review	k1gFnSc2
A.	A.	kA
1995	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
51	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2738	#num#	k4
<g/>
–	–	k?
<g/>
2747	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
PhysRevA	PhysRevA	k1gFnSc2
<g/>
.51	.51	k4
<g/>
.2738	.2738	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Kupča	Kupča	k1gMnSc1
<g/>
:	:	kIx,
Teorie	teorie	k1gFnSc1
a	a	k8xC
perspektiva	perspektiva	k1gFnSc1
kvantových	kvantový	k2eAgMnPc2d1
počítačů	počítač	k1gMnPc2
<g/>
,	,	kIx,
diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
na	na	k7c4
FEL	FEL	kA
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
2001	#num#	k4
</s>
<s>
NIELSEN	NIELSEN	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
A.	A.	kA
<g/>
;	;	kIx,
CHUANG	CHUANG	kA
<g/>
,	,	kIx,
Isaac	Isaac	k1gFnSc1
L.	L.	kA
Quantum	Quantum	k1gNnSc1
Computation	Computation	k1gInSc1
and	and	k?
Quantum	Quantum	k1gNnSc1
Information	Information	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
52163235	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MERMIN	MERMIN	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
N.	N.	kA
Quantum	Quantum	k1gNnSc1
Computer	computer	k1gInSc1
Science	Science	k1gFnSc1
<g/>
,	,	kIx,
An	An	k1gFnSc1
Introduction	Introduction	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
87658	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Qubit	Qubit	k1gInSc1
v	v	k7c6
encyklopedii	encyklopedie	k1gFnSc6
MathWorld	MathWorlda	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
