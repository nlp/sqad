<s>
Kvantový	kvantový	k2eAgInSc1d1
bit	bit	k1gInSc1
neboli	neboli	k8xC
qubit	qubit	k5eAaImF,k5eAaPmF,k5eAaBmF
(	(	kIx(
<g/>
čti	číst	k5eAaImRp2nS
kjúbit	kjúbit	k5eAaImF,k5eAaPmF
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
kvantové	kvantový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
odvozená	odvozený	k2eAgFnSc1d1
od	od	k7c2
klasického	klasický	k2eAgInSc2d1
bitu	bit	k1gInSc2
<g/>
.	.	kIx.
</s>