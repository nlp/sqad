<s>
Termín	termín	k1gInSc1	termín
supernova	supernova	k1gFnSc1	supernova
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
několika	několik	k4yIc3	několik
typům	typ	k1gInPc3	typ
hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
explozí	exploze	k1gFnPc2	exploze
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
extrémně	extrémně	k6eAd1	extrémně
jasné	jasný	k2eAgInPc1d1	jasný
objekty	objekt	k1gInPc1	objekt
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
plazmatu	plazma	k1gNnSc2	plazma
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
jasnost	jasnost	k1gFnSc1	jasnost
posléze	posléze	k6eAd1	posléze
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
týdnů	týden	k1gInPc2	týden
či	či	k8xC	či
měsíců	měsíc	k1gInPc2	měsíc
opět	opět	k6eAd1	opět
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
řádů	řád	k1gInPc2	řád
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
konci	konec	k1gInSc3	konec
vedou	vést	k5eAaImIp3nP	vést
dvě	dva	k4xCgFnPc4	dva
možné	možný	k2eAgFnPc4d1	možná
cesty	cesta	k1gFnPc4	cesta
<g/>
:	:	kIx,	:
buďto	buďto	k8xC	buďto
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
masívní	masívní	k2eAgFnSc4d1	masívní
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
jádře	jádro	k1gNnSc6	jádro
vyčerpala	vyčerpat	k5eAaPmAgFnS	vyčerpat
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
zásob	zásoba	k1gFnPc2	zásoba
paliva	palivo	k1gNnSc2	palivo
pro	pro	k7c4	pro
fúzi	fúze	k1gFnSc4	fúze
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
tvořit	tvořit	k5eAaImF	tvořit
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
hroutit	hroutit	k5eAaImF	hroutit
pod	pod	k7c7	pod
silou	síla	k1gFnSc7	síla
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
,	,	kIx,	,
vlivem	vliv	k1gInSc7	vliv
útlumu	útlum	k1gInSc2	útlum
termonuklearní	termonuklearní	k2eAgFnSc2d1	termonuklearní
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
bílého	bílý	k2eAgMnSc4d1	bílý
trpaslíka	trpaslík	k1gMnSc4	trpaslík
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nahromadil	nahromadit	k5eAaPmAgInS	nahromadit
materiál	materiál	k1gInSc4	materiál
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
hvězdného	hvězdný	k2eAgMnSc2d1	hvězdný
průvodce	průvodce	k1gMnSc2	průvodce
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Chandrasekharovy	Chandrasekharův	k2eAgFnPc4d1	Chandrasekharova
meze	mez	k1gFnPc4	mez
a	a	k8xC	a
prodělal	prodělat	k5eAaPmAgMnS	prodělat
termonukleární	termonukleární	k2eAgFnSc4d1	termonukleární
explozi	exploze	k1gFnSc4	exploze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
výsledná	výsledný	k2eAgFnSc1d1	výsledná
exploze	exploze	k1gFnSc1	exploze
supernovy	supernova	k1gFnSc2	supernova
rozmetá	rozmetat	k5eAaPmIp3nS	rozmetat
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
silou	síla	k1gFnSc7	síla
většinu	většina	k1gFnSc4	většina
nebo	nebo	k8xC	nebo
všechnu	všechen	k3xTgFnSc4	všechen
hmotu	hmota	k1gFnSc4	hmota
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Exploze	exploze	k1gFnSc1	exploze
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
rázovou	rázový	k2eAgFnSc4d1	rázová
vlnu	vlna	k1gFnSc4	vlna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
interaguje	interagovat	k5eAaPmIp3nS	interagovat
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
supernovy	supernova	k1gFnSc2	supernova
a	a	k8xC	a
mezihvězdnou	mezihvězdný	k2eAgFnSc7d1	mezihvězdná
hmotou	hmota	k1gFnSc7	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
příkladem	příklad	k1gInSc7	příklad
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
jsou	být	k5eAaImIp3nP	být
zbytky	zbytek	k1gInPc1	zbytek
SN	SN	kA	SN
1604	[number]	k4	1604
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
můžete	moct	k5eAaImIp2nP	moct
vidět	vidět	k5eAaImF	vidět
vpravo	vpravo	k6eAd1	vpravo
<g/>
.	.	kIx.	.
</s>
<s>
Exploze	exploze	k1gFnSc1	exploze
supernov	supernova	k1gFnPc2	supernova
jsou	být	k5eAaImIp3nP	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
všech	všecek	k3xTgInPc2	všecek
prvků	prvek	k1gInPc2	prvek
těžších	těžký	k2eAgNnPc2d2	těžší
než	než	k8xS	než
železo	železo	k1gNnSc4	železo
a	a	k8xC	a
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
důležitých	důležitý	k2eAgInPc2d1	důležitý
prvků	prvek	k1gInPc2	prvek
zdrojem	zdroj	k1gInSc7	zdroj
jediným	jediný	k2eAgInSc7d1	jediný
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
všechen	všechen	k3xTgInSc1	všechen
vápník	vápník	k1gInSc1	vápník
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
kostech	kost	k1gFnPc6	kost
a	a	k8xC	a
všechno	všechen	k3xTgNnSc1	všechen
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
uran	uran	k1gInSc1	uran
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
byly	být	k5eAaImAgFnP	být
syntetizovány	syntetizován	k2eAgFnPc1d1	syntetizována
při	při	k7c6	při
explozi	exploze	k1gFnSc6	exploze
supernov	supernova	k1gFnPc2	supernova
před	před	k7c7	před
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Supernovy	supernova	k1gFnPc1	supernova
vnášejí	vnášet	k5eAaImIp3nP	vnášet
do	do	k7c2	do
mezihvězdné	mezihvězdný	k2eAgFnSc2d1	mezihvězdná
hmoty	hmota	k1gFnSc2	hmota
těžké	těžký	k2eAgInPc4d1	těžký
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
obohacují	obohacovat	k5eAaImIp3nP	obohacovat
tak	tak	k6eAd1	tak
molekulární	molekulární	k2eAgNnPc4d1	molekulární
mračna	mračno	k1gNnPc4	mračno
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
dějištěm	dějiště	k1gNnSc7	dějiště
tvorby	tvorba	k1gFnSc2	tvorba
nových	nový	k2eAgFnPc2d1	nová
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
supernov	supernova	k1gFnPc2	supernova
významně	významně	k6eAd1	významně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
složení	složení	k1gNnSc3	složení
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
umožnila	umožnit	k5eAaPmAgFnS	umožnit
tak	tak	k9	tak
nakonec	nakonec	k6eAd1	nakonec
chemii	chemie	k1gFnSc4	chemie
života	život	k1gInSc2	život
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gMnSc4	on
známe	znát	k5eAaImIp1nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Výbuch	výbuch	k1gInSc1	výbuch
supernovy	supernova	k1gFnSc2	supernova
je	být	k5eAaImIp3nS	být
provázen	provázet	k5eAaImNgInS	provázet
obrovskými	obrovský	k2eAgFnPc7d1	obrovská
teplotami	teplota	k1gFnPc7	teplota
a	a	k8xC	a
za	za	k7c2	za
jistých	jistý	k2eAgFnPc2d1	jistá
podmínek	podmínka	k1gFnPc2	podmínka
mohou	moct	k5eAaImIp3nP	moct
fúzní	fúzní	k2eAgFnPc1d1	fúzní
reakce	reakce	k1gFnPc1	reakce
během	během	k7c2	během
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
fáze	fáze	k1gFnSc2	fáze
vyprodukovat	vyprodukovat	k5eAaPmF	vyprodukovat
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
nejtěžších	těžký	k2eAgInPc2d3	nejtěžší
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
kalifornium	kalifornium	k1gNnSc1	kalifornium
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nova	nova	k1gFnSc1	nova
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
latinsky	latinsky	k6eAd1	latinsky
"	"	kIx"	"
<g/>
nový	nový	k2eAgMnSc1d1	nový
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
jasná	jasný	k2eAgFnSc1d1	jasná
nová	nový	k2eAgFnSc1d1	nová
hvězda	hvězda	k1gFnSc1	hvězda
na	na	k7c6	na
nebeské	nebeský	k2eAgFnSc6d1	nebeská
sféře	sféra	k1gFnSc6	sféra
<g/>
;	;	kIx,	;
prefix	prefix	k1gInSc1	prefix
"	"	kIx"	"
<g/>
super	super	k6eAd1	super
<g/>
"	"	kIx"	"
ji	on	k3xPp3gFnSc4	on
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
obyčejné	obyčejný	k2eAgFnSc2d1	obyčejná
novy	nova	k1gFnSc2	nova
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
míněna	míněn	k2eAgFnSc1d1	míněna
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
svou	svůj	k3xOyFgFnSc4	svůj
jasnost	jasnost	k1gFnSc4	jasnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
menším	malý	k2eAgInSc6d2	menší
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
odlišným	odlišný	k2eAgInSc7d1	odlišný
mechanismem	mechanismus	k1gInSc7	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
však	však	k9	však
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
považovat	považovat	k5eAaImF	považovat
supernovu	supernova	k1gFnSc4	supernova
za	za	k7c4	za
novou	nový	k2eAgFnSc4d1	nová
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zánik	zánik	k1gInSc4	zánik
hvězdy	hvězda	k1gFnSc2	hvězda
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
přinejmenším	přinejmenším	k6eAd1	přinejmenším
její	její	k3xOp3gFnSc4	její
radikální	radikální	k2eAgFnSc4d1	radikální
transformaci	transformace	k1gFnSc4	transformace
v	v	k7c4	v
něco	něco	k3yInSc4	něco
odlišného	odlišný	k2eAgNnSc2d1	odlišné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
astronomové	astronom	k1gMnPc1	astronom
snažili	snažit	k5eAaImAgMnP	snažit
porozumět	porozumět	k5eAaPmF	porozumět
explozím	exploze	k1gFnPc3	exploze
supernov	supernova	k1gFnPc2	supernova
<g/>
,	,	kIx,	,
klasifikovali	klasifikovat	k5eAaImAgMnP	klasifikovat
je	on	k3xPp3gFnPc4	on
podle	podle	k7c2	podle
čar	čára	k1gFnPc2	čára
různých	různý	k2eAgInPc2d1	různý
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
objevujících	objevující	k2eAgFnPc2d1	objevující
se	se	k3xPyFc4	se
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
spektru	spektrum	k1gNnSc6	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
popis	popis	k1gInSc1	popis
těchto	tento	k3xDgFnPc2	tento
tříd	třída	k1gFnPc2	třída
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
anglická	anglický	k2eAgFnSc1d1	anglická
publikace	publikace	k1gFnSc1	publikace
"	"	kIx"	"
<g/>
Optická	optický	k2eAgNnPc4d1	optické
spektra	spektrum	k1gNnPc4	spektrum
supernov	supernova	k1gFnPc2	supernova
<g/>
"	"	kIx"	"
od	od	k7c2	od
Filipenka	Filipenka	k1gFnSc1	Filipenka
(	(	kIx(	(
<g/>
Annual	Annual	k1gInSc1	Annual
Review	Review	k1gFnSc2	Review
of	of	k?	of
Astronomy	astronom	k1gMnPc4	astronom
and	and	k?	and
Astrophysics	Astrophysicsa	k1gFnPc2	Astrophysicsa
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
35	[number]	k4	35
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
309	[number]	k4	309
<g/>
-	-	kIx~	-
<g/>
355	[number]	k4	355
<g/>
)	)	kIx)	)
Základním	základní	k2eAgInSc7d1	základní
prvkem	prvek	k1gInSc7	prvek
rozdělení	rozdělení	k1gNnSc2	rozdělení
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
nebo	nebo	k8xC	nebo
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
čáry	čára	k1gFnSc2	čára
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
spektrum	spektrum	k1gNnSc1	spektrum
supernovy	supernova	k1gFnSc2	supernova
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
čáru	čára	k1gFnSc4	čára
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
klasifikována	klasifikován	k2eAgFnSc1d1	klasifikována
jako	jako	k8xS	jako
typ	typ	k1gInSc1	typ
II	II	kA	II
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
typ	typ	k1gInSc4	typ
I.	I.	kA	I.
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
skupin	skupina	k1gFnPc2	skupina
existují	existovat	k5eAaImIp3nP	existovat
podrobnější	podrobný	k2eAgNnSc4d2	podrobnější
dělení	dělení	k1gNnSc4	dělení
podle	podle	k7c2	podle
přítomnosti	přítomnost	k1gFnSc2	přítomnost
jiných	jiný	k2eAgFnPc2d1	jiná
čar	čára	k1gFnPc2	čára
nebo	nebo	k8xC	nebo
tvaru	tvar	k1gInSc2	tvar
světelné	světelný	k2eAgFnSc2d1	světelná
křivky	křivka	k1gFnSc2	křivka
<g/>
.	.	kIx.	.
</s>
<s>
Typ	typ	k1gInSc1	typ
I	i	k9	i
Žádné	žádný	k3yNgFnPc1	žádný
Balmerovy	Balmerův	k2eAgFnPc1d1	Balmerova
čáry	čára	k1gFnPc1	čára
vodíku	vodík	k1gInSc2	vodík
Typ	typ	k1gInSc1	typ
Ia	ia	k0	ia
Čára	čára	k1gFnSc1	čára
Si	se	k3xPyFc3	se
II	II	kA	II
na	na	k7c4	na
615,0	[number]	k4	615,0
nm	nm	k?	nm
Typ	typ	k1gInSc1	typ
Ib	Ib	k1gMnSc1	Ib
Čára	čára	k1gFnSc1	čára
He	he	k0	he
I	i	k9	i
na	na	k7c4	na
587,6	[number]	k4	587,6
nm	nm	k?	nm
Typ	typ	k1gInSc1	typ
Ic	Ic	k1gFnSc2	Ic
Slabé	Slabá	k1gFnSc2	Slabá
nebo	nebo	k8xC	nebo
žádné	žádný	k3yNgFnPc1	žádný
čáry	čára	k1gFnPc1	čára
hélia	hélium	k1gNnSc2	hélium
Typ	typa	k1gFnPc2	typa
II	II	kA	II
Má	mít	k5eAaImIp3nS	mít
Balmerovy	Balmerův	k2eAgFnPc4d1	Balmerova
čáry	čára	k1gFnPc4	čára
vodíku	vodík	k1gInSc2	vodík
Typ	typ	k1gInSc1	typ
II-P	II-P	k1gFnSc2	II-P
Plochá	plochý	k2eAgFnSc1d1	plochá
světelná	světelný	k2eAgFnSc1d1	světelná
křivka	křivka	k1gFnSc1	křivka
Typ	typ	k1gInSc1	typ
II-L	II-L	k1gFnSc4	II-L
Lineární	lineární	k2eAgInSc1d1	lineární
pokles	pokles	k1gInSc1	pokles
světelné	světelný	k2eAgFnSc2d1	světelná
křivky	křivka	k1gFnSc2	křivka
(	(	kIx(	(
<g/>
závislost	závislost	k1gFnSc1	závislost
magnitudy	magnituda	k1gFnSc2	magnituda
na	na	k7c6	na
čase	čas	k1gInSc6	čas
<g/>
)	)	kIx)	)
Supernovy	supernova	k1gFnSc2	supernova
typu	typ	k1gInSc2	typ
Ia	ia	k0	ia
postrádají	postrádat	k5eAaImIp3nP	postrádat
hélium	hélium	k1gNnSc1	hélium
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
spektru	spektrum	k1gNnSc6	spektrum
absorpční	absorpční	k2eAgFnSc4d1	absorpční
čáru	čára	k1gFnSc4	čára
křemíku	křemík	k1gInSc2	křemík
poblíž	poblíž	k7c2	poblíž
světelného	světelný	k2eAgInSc2d1	světelný
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nejvíce	nejvíce	k6eAd1	nejvíce
akceptované	akceptovaný	k2eAgFnSc2d1	akceptovaná
teorie	teorie	k1gFnSc2	teorie
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
supernov	supernova	k1gFnPc2	supernova
výsledkem	výsledek	k1gInSc7	výsledek
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgMnSc6	jenž
uhlíko-kyslíkový	uhlíkoyslíkový	k2eAgMnSc1d1	uhlíko-kyslíkový
bílý	bílý	k2eAgMnSc1d1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
hmotu	hmota	k1gFnSc4	hmota
z	z	k7c2	z
blízkého	blízký	k2eAgMnSc2d1	blízký
hvězdného	hvězdný	k2eAgMnSc2d1	hvězdný
průvodce	průvodce	k1gMnSc2	průvodce
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
rudého	rudý	k2eAgMnSc2d1	rudý
obra	obr	k1gMnSc2	obr
<g/>
,	,	kIx,	,
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
Chandrasekharovy	Chandrasekharův	k2eAgFnPc4d1	Chandrasekharova
meze	mez	k1gFnPc4	mez
<g/>
.	.	kIx.	.
</s>
<s>
Nárůst	nárůst	k1gInSc1	nárůst
tlaku	tlak	k1gInSc2	tlak
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
teplotu	teplota	k1gFnSc4	teplota
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
perioda	perioda	k1gFnSc1	perioda
konvekce	konvekce	k1gFnSc1	konvekce
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
asi	asi	k9	asi
100	[number]	k4	100
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
bodě	bod	k1gInSc6	bod
této	tento	k3xDgFnSc2	tento
fáze	fáze	k1gFnSc2	fáze
slabého	slabý	k2eAgNnSc2d1	slabé
vření	vření	k1gNnSc2	vření
se	se	k3xPyFc4	se
zažehne	zažehnout	k5eAaPmIp3nS	zažehnout
deflagrační	deflagrační	k2eAgInSc1d1	deflagrační
plamen	plamen	k1gInSc1	plamen
živený	živený	k2eAgInSc1d1	živený
termojadernou	termojaderný	k2eAgFnSc7d1	termojaderná
fúzí	fúze	k1gFnSc7	fúze
<g/>
.	.	kIx.	.
</s>
<s>
Detaily	detail	k1gInPc1	detail
jeho	on	k3xPp3gInSc2	on
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
umístění	umístění	k1gNnSc4	umístění
a	a	k8xC	a
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
započne	započnout	k5eAaPmIp3nS	započnout
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
neznámé	známý	k2eNgFnPc1d1	neznámá
<g/>
.	.	kIx.	.
</s>
<s>
Dramatickému	dramatický	k2eAgNnSc3d1	dramatické
zrychlování	zrychlování	k1gNnSc3	zrychlování
šíření	šíření	k1gNnSc2	šíření
plamenu	plamen	k1gInSc2	plamen
napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
Rayleighova-Taylorova	Rayleighova-Taylorův	k2eAgFnSc1d1	Rayleighova-Taylorův
nestabilita	nestabilita	k1gFnSc1	nestabilita
a	a	k8xC	a
interakce	interakce	k1gFnSc1	interakce
s	s	k7c7	s
turbulentními	turbulentní	k2eAgInPc7d1	turbulentní
proudy	proud	k1gInPc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
velkých	velký	k2eAgFnPc2d1	velká
debat	debata	k1gFnPc2	debata
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
otázka	otázka	k1gFnSc1	otázka
přeměny	přeměna	k1gFnSc2	přeměna
podzvukového	podzvukový	k2eAgNnSc2d1	podzvukové
šíření	šíření	k1gNnSc2	šíření
plamenu	plamen	k1gInSc2	plamen
(	(	kIx(	(
<g/>
deflagrace	deflagrace	k1gFnSc1	deflagrace
<g/>
)	)	kIx)	)
do	do	k7c2	do
nadzvukové	nadzvukový	k2eAgFnSc2d1	nadzvuková
detonace	detonace	k1gFnSc2	detonace
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
termonukleárním	termonukleární	k2eAgInSc7d1	termonukleární
zážehem	zážeh	k1gInSc7	zážeh
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
1044	[number]	k4	1044
J	J	kA	J
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
způsobí	způsobit	k5eAaPmIp3nS	způsobit
prudkou	prudký	k2eAgFnSc4d1	prudká
explozi	exploze	k1gFnSc4	exploze
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
vznik	vznik	k1gInSc1	vznik
rázové	rázový	k2eAgFnSc2d1	rázová
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrhovaná	vyvrhovaný	k2eAgFnSc1d1	vyvrhovaná
hmota	hmota	k1gFnSc1	hmota
je	být	k5eAaImIp3nS	být
urychlována	urychlovat	k5eAaImNgFnS	urychlovat
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
10	[number]	k4	10
000	[number]	k4	000
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Energie	energie	k1gFnSc1	energie
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
při	při	k7c6	při
explozi	exploze	k1gFnSc6	exploze
způsobí	způsobit	k5eAaPmIp3nS	způsobit
také	také	k9	také
extrémní	extrémní	k2eAgNnSc1d1	extrémní
zvýšení	zvýšení	k1gNnSc1	zvýšení
jasnosti	jasnost	k1gFnSc2	jasnost
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
typem	typ	k1gInSc7	typ
supernov	supernova	k1gFnPc2	supernova
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
teorii	teorie	k1gFnSc3	teorie
nov	nov	k1gInSc1	nov
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
bílý	bílý	k1gMnSc1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
nabírá	nabírat	k5eAaImIp3nS	nabírat
hmotu	hmota	k1gFnSc4	hmota
mnohem	mnohem	k6eAd1	mnohem
pomaleji	pomale	k6eAd2	pomale
a	a	k8xC	a
nedosáhne	dosáhnout	k5eNaPmIp3nS	dosáhnout
Chandrasekharovy	Chandrasekharův	k2eAgFnPc4d1	Chandrasekharova
meze	mez	k1gFnPc4	mez
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
novy	nova	k1gFnSc2	nova
zapříčiní	zapříčinit	k5eAaPmIp3nS	zapříčinit
dopadající	dopadající	k2eAgFnSc1d1	dopadající
hmota	hmota	k1gFnSc1	hmota
fúzní	fúzní	k2eAgFnSc4d1	fúzní
reakci	reakce	k1gFnSc4	reakce
materiálu	materiál	k1gInSc2	materiál
poblíž	poblíž	k7c2	poblíž
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
nezpůsobí	způsobit	k5eNaPmIp3nS	způsobit
však	však	k9	však
kolaps	kolaps	k1gInSc4	kolaps
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Supernovy	supernova	k1gFnPc1	supernova
typu	typ	k1gInSc2	typ
Ia	ia	k0	ia
mají	mít	k5eAaImIp3nP	mít
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
světelnou	světelný	k2eAgFnSc4d1	světelná
křivku	křivka	k1gFnSc4	křivka
(	(	kIx(	(
<g/>
graf	graf	k1gInSc1	graf
jasnosti	jasnost	k1gFnSc2	jasnost
po	po	k7c6	po
explozi	exploze	k1gFnSc6	exploze
jako	jako	k8xC	jako
funkce	funkce	k1gFnSc2	funkce
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
maximální	maximální	k2eAgFnSc2d1	maximální
jasnosti	jasnost	k1gFnSc2	jasnost
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
spektrum	spektrum	k1gNnSc4	spektrum
čáry	čára	k1gFnSc2	čára
středně	středně	k6eAd1	středně
těžkých	těžký	k2eAgInPc2d1	těžký
prvků	prvek	k1gInPc2	prvek
od	od	k7c2	od
kyslíku	kyslík	k1gInSc2	kyslík
po	po	k7c4	po
vápník	vápník	k1gInSc4	vápník
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
hlavní	hlavní	k2eAgInPc1d1	hlavní
produkty	produkt	k1gInPc1	produkt
fúze	fúze	k1gFnSc2	fúze
ve	v	k7c6	v
vnějších	vnější	k2eAgFnPc6d1	vnější
vrstvách	vrstva	k1gFnPc6	vrstva
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Měsíce	měsíc	k1gInPc1	měsíc
po	po	k7c4	po
explozi	exploze	k1gFnSc4	exploze
<g/>
,	,	kIx,	,
když	když	k8xS	když
vnější	vnější	k2eAgFnPc1d1	vnější
vrstvy	vrstva	k1gFnPc1	vrstva
expandují	expandovat	k5eAaImIp3nP	expandovat
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
průhlednými	průhledný	k2eAgFnPc7d1	průhledná
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
dominovat	dominovat	k5eAaImF	dominovat
světlo	světlo	k1gNnSc1	světlo
emitované	emitovaný	k2eAgNnSc1d1	emitované
materiálem	materiál	k1gInSc7	materiál
poblíž	poblíž	k7c2	poblíž
jádra	jádro	k1gNnSc2	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
:	:	kIx,	:
těžké	těžký	k2eAgInPc1d1	těžký
prvky	prvek	k1gInPc1	prvek
syntetizované	syntetizovaný	k2eAgInPc1d1	syntetizovaný
při	při	k7c6	při
explozi	exploze	k1gFnSc6	exploze
<g/>
,	,	kIx,	,
nejvýznamnějšími	významný	k2eAgInPc7d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
prvky	prvek	k1gInPc1	prvek
skupiny	skupina	k1gFnSc2	skupina
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
rozpad	rozpad	k1gInSc1	rozpad
56	[number]	k4	56
<g/>
Ni	on	k3xPp3gFnSc4	on
přes	přes	k7c4	přes
56	[number]	k4	56
<g/>
Co	co	k9	co
na	na	k7c4	na
56	[number]	k4	56
<g/>
Fe	Fe	k1gMnPc2	Fe
produkuje	produkovat	k5eAaImIp3nS	produkovat
vysokoenergetické	vysokoenergetický	k2eAgInPc4d1	vysokoenergetický
fotony	foton	k1gInPc4	foton
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
dominují	dominovat	k5eAaImIp3nP	dominovat
energetickému	energetický	k2eAgInSc3d1	energetický
výstupu	výstup	k1gInSc3	výstup
vyvržené	vyvržený	k2eAgFnSc2d1	vyvržená
hmoty	hmota	k1gFnSc2	hmota
ve	v	k7c6	v
střednědobém	střednědobý	k2eAgInSc6d1	střednědobý
i	i	k8xC	i
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
horizontu	horizont	k1gInSc6	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Typ	typ	k1gInSc1	typ
supernov	supernova	k1gFnPc2	supernova
Ia	ia	k0	ia
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
největší	veliký	k2eAgNnSc4d3	veliký
množství	množství	k1gNnSc4	množství
energie	energie	k1gFnSc2	energie
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
ostatními	ostatní	k2eAgFnPc7d1	ostatní
známými	známý	k2eAgFnPc7d1	známá
třídami	třída	k1gFnPc7	třída
supernov	supernova	k1gFnPc2	supernova
<g/>
.	.	kIx.	.
</s>
<s>
Nejvzdálenější	vzdálený	k2eAgInSc1d3	nejvzdálenější
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
objekt	objekt	k1gInSc1	objekt
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
detekován	detekován	k2eAgMnSc1d1	detekován
(	(	kIx(	(
<g/>
galaxie	galaxie	k1gFnPc1	galaxie
a	a	k8xC	a
kulové	kulový	k2eAgFnPc1d1	kulová
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
se	se	k3xPyFc4	se
nepočítají	počítat	k5eNaImIp3nP	počítat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
právě	právě	k9	právě
supernova	supernova	k1gFnSc1	supernova
SN	SN	kA	SN
1997	[number]	k4	1997
<g/>
ff	ff	kA	ff
typu	typ	k1gInSc2	typ
Ia	ia	k0	ia
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
více	hodně	k6eAd2	hodně
než	než	k8xS	než
11	[number]	k4	11
miliard	miliarda	k4xCgFnPc2	miliarda
světelných	světelný	k2eAgInPc2d1	světelný
let	let	k1gInSc4	let
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
100	[number]	k4	100
yottametrů	yottametr	k1gInPc2	yottametr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
typů	typ	k1gInPc2	typ
supernov	supernova	k1gFnPc2	supernova
<g/>
,	,	kIx,	,
supernovy	supernova	k1gFnSc2	supernova
typu	typ	k1gInSc2	typ
Ia	ia	k0	ia
lze	lze	k6eAd1	lze
zpravidla	zpravidla	k6eAd1	zpravidla
najít	najít	k5eAaPmF	najít
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
typech	typ	k1gInPc6	typ
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
eliptických	eliptický	k2eAgFnPc2d1	eliptická
<g/>
.	.	kIx.	.
</s>
<s>
Nezdá	zdát	k5eNaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
upřednostňovaly	upřednostňovat	k5eAaImAgFnP	upřednostňovat
nějakou	nějaký	k3yIgFnSc4	nějaký
oblast	oblast	k1gFnSc4	oblast
dnešních	dnešní	k2eAgFnPc2d1	dnešní
hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
formací	formace	k1gFnPc2	formace
<g/>
.	.	kIx.	.
</s>
<s>
Podobnost	podobnost	k1gFnSc1	podobnost
tvarů	tvar	k1gInPc2	tvar
profilů	profil	k1gInPc2	profil
jasnosti	jasnost	k1gFnSc2	jasnost
všech	všecek	k3xTgFnPc2	všecek
známých	známý	k2eAgFnPc2d1	známá
supernov	supernova	k1gFnPc2	supernova
typu	typ	k1gInSc2	typ
Ia	ia	k0	ia
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
užívání	užívání	k1gNnSc3	užívání
jako	jako	k8xC	jako
standardních	standardní	k2eAgFnPc2d1	standardní
svíček	svíčka	k1gFnPc2	svíčka
v	v	k7c6	v
extragalaktické	extragalaktický	k2eAgFnSc6d1	extragalaktická
astronomii	astronomie	k1gFnSc6	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
prakticky	prakticky	k6eAd1	prakticky
jediným	jediný	k2eAgInSc7d1	jediný
nástrojem	nástroj	k1gInSc7	nástroj
umožňujícím	umožňující	k2eAgInSc7d1	umožňující
měření	měření	k1gNnSc4	měření
velkých	velká	k1gFnPc2	velká
intergalaktických	intergalaktický	k2eAgFnPc2d1	intergalaktická
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
dala	dát	k5eAaPmAgFnS	dát
pozorování	pozorování	k1gNnSc4	pozorování
supernov	supernova	k1gFnPc2	supernova
typu	typ	k1gInSc2	typ
Ia	ia	k0	ia
neočekávaný	očekávaný	k2eNgInSc1d1	neočekávaný
výsledek	výsledek	k1gInSc1	výsledek
–	–	k?	–
vypadá	vypadat	k5eAaImIp3nS	vypadat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
prodělává	prodělávat	k5eAaImIp3nS	prodělávat
zrychlující	zrychlující	k2eAgFnSc3d1	zrychlující
se	se	k3xPyFc4	se
expanzi	expanze	k1gFnSc3	expanze
<g/>
.	.	kIx.	.
</s>
<s>
Raná	raný	k2eAgNnPc4d1	rané
spektra	spektrum	k1gNnPc4	spektrum
typů	typ	k1gInPc2	typ
Ib	Ib	k1gMnPc2	Ib
a	a	k8xC	a
Ic	Ic	k1gMnPc2	Ic
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
čáry	čára	k1gFnPc1	čára
vodíku	vodík	k1gInSc2	vodík
ani	ani	k8xC	ani
výraznou	výrazný	k2eAgFnSc4d1	výrazná
křemíkovou	křemíkový	k2eAgFnSc4d1	křemíková
absorpci	absorpce	k1gFnSc4	absorpce
poblíž	poblíž	k6eAd1	poblíž
615	[number]	k4	615
nanometrů	nanometr	k1gInPc2	nanometr
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
událostmi	událost	k1gFnPc7	událost
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
supernovy	supernova	k1gFnPc1	supernova
typu	typ	k1gInSc2	typ
II	II	kA	II
stojí	stát	k5eAaImIp3nP	stát
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
masívní	masívní	k2eAgFnPc1d1	masívní
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyčerpaly	vyčerpat	k5eAaPmAgFnP	vyčerpat
palivo	palivo	k1gNnSc4	palivo
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
centrech	centrum	k1gNnPc6	centrum
<g/>
;	;	kIx,	;
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
původci	původce	k1gMnPc7	původce
typů	typ	k1gInPc2	typ
Ib	Ib	k1gFnSc2	Ib
a	a	k8xC	a
Ic	Ic	k1gFnSc2	Ic
ztratily	ztratit	k5eAaPmAgFnP	ztratit
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgFnPc2	svůj
obálek	obálka	k1gFnPc2	obálka
následkem	následkem	k7c2	následkem
silných	silný	k2eAgInPc2d1	silný
hvězdných	hvězdný	k2eAgInPc2d1	hvězdný
větrů	vítr	k1gInPc2	vítr
popřípadě	popřípadě	k6eAd1	popřípadě
interakcí	interakce	k1gFnSc7	interakce
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
průvodcem	průvodce	k1gMnSc7	průvodce
<g/>
.	.	kIx.	.
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
supernovy	supernova	k1gFnPc1	supernova
typu	typ	k1gInSc2	typ
Ib	Ib	k1gFnPc1	Ib
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
zhroucení	zhroucení	k1gNnSc2	zhroucení
Wolf-Rayetových	Wolf-Rayetový	k2eAgFnPc2d1	Wolf-Rayetový
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
jisté	jistý	k2eAgInPc4d1	jistý
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
supernovy	supernova	k1gFnPc1	supernova
typu	typ	k1gInSc2	typ
Ic	Ic	k1gFnPc1	Ic
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
původci	původce	k1gMnPc1	původce
některých	některý	k3yIgInPc2	některý
typů	typ	k1gInPc2	typ
gama	gama	k1gNnSc7	gama
záblesků	záblesk	k1gInPc2	záblesk
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gFnPc4	on
může	moct	k5eAaImIp3nS	moct
druhotně	druhotně	k6eAd1	druhotně
způsobit	způsobit	k5eAaPmF	způsobit
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
supernova	supernova	k1gFnSc1	supernova
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
geometrii	geometrie	k1gFnSc6	geometrie
exploze	exploze	k1gFnSc2	exploze
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
mnohem	mnohem	k6eAd1	mnohem
hmotnější	hmotný	k2eAgFnPc1d2	hmotnější
než	než	k8xS	než
naše	náš	k3xOp1gNnPc1	náš
Slunce	slunce	k1gNnPc1	slunce
se	se	k3xPyFc4	se
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
o	o	k7c4	o
dost	dost	k6eAd1	dost
složitějšími	složitý	k2eAgInPc7d2	složitější
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
našeho	náš	k3xOp1gNnSc2	náš
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
každou	každý	k3xTgFnSc4	každý
sekundu	sekunda	k1gFnSc4	sekunda
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
589	[number]	k4	589
miliónů	milión	k4xCgInPc2	milión
tun	tuna	k1gFnPc2	tuna
vodíku	vodík	k1gInSc2	vodík
na	na	k7c4	na
584	[number]	k4	584
miliónů	milión	k4xCgInPc2	milión
tun	tuna	k1gFnPc2	tuna
hélia	hélium	k1gNnSc2	hélium
<g/>
,	,	kIx,	,
rozdíl	rozdíl	k1gInSc1	rozdíl
hmotnosti	hmotnost	k1gFnSc2	hmotnost
4,3	[number]	k4	4,3
miliónů	milión	k4xCgInPc2	milión
tun	tuna	k1gFnPc2	tuna
je	být	k5eAaImIp3nS	být
přeměněn	přeměnit	k5eAaPmNgMnS	přeměnit
v	v	k7c4	v
čistou	čistý	k2eAgFnSc4d1	čistá
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
vyzářena	vyzářit	k5eAaPmNgFnS	vyzářit
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Hélium	hélium	k1gNnSc1	hélium
vyprodukované	vyprodukovaný	k2eAgNnSc1d1	vyprodukované
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hromadí	hromadit	k5eAaImIp3nP	hromadit
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
teploty	teplota	k1gFnPc1	teplota
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
nezvýší	zvýšit	k5eNaPmIp3nS	zvýšit
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dovolí	dovolit	k5eAaPmIp3nS	dovolit
fúzi	fúze	k1gFnSc4	fúze
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
vodík	vodík	k1gInSc1	vodík
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
přeměnou	přeměna	k1gFnSc7	přeměna
na	na	k7c4	na
hélium	hélium	k1gNnSc4	hélium
a	a	k8xC	a
postupným	postupný	k2eAgNnSc7d1	postupné
rozředěním	rozředění	k1gNnSc7	rozředění
vznikajícím	vznikající	k2eAgNnSc7d1	vznikající
héliovým	héliový	k2eAgInSc7d1	héliový
"	"	kIx"	"
<g/>
popelem	popel	k1gInSc7	popel
<g/>
"	"	kIx"	"
vyčerpá	vyčerpat	k5eAaPmIp3nS	vyčerpat
<g/>
,	,	kIx,	,
fúze	fúze	k1gFnSc1	fúze
se	se	k3xPyFc4	se
zpomalí	zpomalet	k5eAaPmIp3nS	zpomalet
<g/>
,	,	kIx,	,
gravitace	gravitace	k1gFnSc1	gravitace
nabude	nabýt	k5eAaPmIp3nS	nabýt
převahu	převaha	k1gFnSc4	převaha
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
jádro	jádro	k1gNnSc1	jádro
stlačovat	stlačovat	k5eAaImF	stlačovat
<g/>
.	.	kIx.	.
</s>
<s>
Smršťování	smršťování	k1gNnSc1	smršťování
jádra	jádro	k1gNnSc2	jádro
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
teplotu	teplota	k1gFnSc4	teplota
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zahájí	zahájit	k5eAaPmIp3nS	zahájit
kratší	krátký	k2eAgFnSc1d2	kratší
fáze	fáze	k1gFnSc1	fáze
fúze	fúze	k1gFnSc2	fúze
hélia	hélium	k1gNnSc2	hélium
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bude	být	k5eAaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
roli	role	k1gFnSc4	role
po	po	k7c6	po
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
%	%	kIx~	%
života	život	k1gInSc2	život
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hvězdách	hvězda	k1gFnPc6	hvězda
menších	malý	k2eAgFnPc2d2	menší
než	než	k8xS	než
10	[number]	k4	10
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
uhlík	uhlík	k1gInSc1	uhlík
produkovaný	produkovaný	k2eAgInSc1d1	produkovaný
fúzí	fúze	k1gFnSc7	fúze
hélia	hélium	k1gNnSc2	hélium
dále	daleko	k6eAd2	daleko
nespaluje	spalovat	k5eNaImIp3nS	spalovat
a	a	k8xC	a
hvězda	hvězda	k1gFnSc1	hvězda
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
degenerovaný	degenerovaný	k2eAgInSc1d1	degenerovaný
elektronový	elektronový	k2eAgInSc1d1	elektronový
plyn	plyn	k1gInSc1	plyn
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
bílý	bílý	k1gMnSc1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Bílí	bílý	k2eAgMnPc1d1	bílý
trpaslíci	trpaslík	k1gMnPc1	trpaslík
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
později	pozdě	k6eAd2	pozdě
stát	stát	k5eAaPmF	stát
supernovou	supernový	k2eAgFnSc4d1	supernová
typu	typa	k1gFnSc4	typa
I	i	k9	i
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
výše	vysoce	k6eAd2	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnPc1d2	veliký
hvězdy	hvězda	k1gFnPc1	hvězda
mají	mít	k5eAaImIp3nP	mít
gravitaci	gravitace	k1gFnSc4	gravitace
dostatečně	dostatečně	k6eAd1	dostatečně
silnou	silný	k2eAgFnSc4d1	silná
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
teplot	teplota	k1gFnPc2	teplota
a	a	k8xC	a
tlaků	tlak	k1gInPc2	tlak
umožňujících	umožňující	k2eAgInPc2d1	umožňující
fúzi	fúze	k1gFnSc4	fúze
uhlíku	uhlík	k1gInSc2	uhlík
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
smršťovat	smršťovat	k5eAaImF	smršťovat
<g/>
.	.	kIx.	.
</s>
<s>
Jádra	jádro	k1gNnPc4	jádro
těchto	tento	k3xDgFnPc2	tento
masívních	masívní	k2eAgFnPc2d1	masívní
hvězd	hvězda	k1gFnPc2	hvězda
nabývají	nabývat	k5eAaImIp3nP	nabývat
vrstevnaté	vrstevnatý	k2eAgFnPc4d1	vrstevnatá
struktury	struktura	k1gFnPc4	struktura
podobné	podobný	k2eAgFnPc4d1	podobná
cibuli	cibule	k1gFnSc4	cibule
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
vytvářena	vytvářen	k2eAgFnSc1d1	vytvářena
těžší	těžký	k2eAgFnSc1d2	těžší
a	a	k8xC	a
těžší	těžký	k2eAgNnPc1d2	těžší
atomová	atomový	k2eAgNnPc1d1	atomové
jádra	jádro	k1gNnPc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
vrstva	vrstva	k1gFnSc1	vrstva
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vodíkový	vodíkový	k2eAgInSc4d1	vodíkový
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
noříme	nořit	k5eAaImIp1nP	nořit
dolů	dolů	k6eAd1	dolů
<g/>
,	,	kIx,	,
míjíme	míjet	k5eAaImIp1nP	míjet
vrstvu	vrstva	k1gFnSc4	vrstva
vodíku	vodík	k1gInSc2	vodík
spojujícího	spojující	k2eAgInSc2d1	spojující
se	se	k3xPyFc4	se
fúzí	fúze	k1gFnPc2	fúze
v	v	k7c4	v
hélium	hélium	k1gNnSc4	hélium
<g/>
,	,	kIx,	,
vrstvu	vrstva	k1gFnSc4	vrstva
hélia	hélium	k1gNnSc2	hélium
<g/>
,	,	kIx,	,
vrstvu	vrstva	k1gFnSc4	vrstva
hélia	hélium	k1gNnSc2	hélium
spojujícího	spojující	k2eAgInSc2d1	spojující
se	se	k3xPyFc4	se
fúzí	fúze	k1gFnPc2	fúze
v	v	k7c4	v
uhlík	uhlík	k1gInSc4	uhlík
<g/>
,	,	kIx,	,
vrstvu	vrstva	k1gFnSc4	vrstva
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
vrstvu	vrstva	k1gFnSc4	vrstva
uhlíku	uhlík	k1gInSc2	uhlík
měnícího	měnící	k2eAgInSc2d1	měnící
se	se	k3xPyFc4	se
fúzí	fúze	k1gFnPc2	fúze
v	v	k7c4	v
těžší	těžký	k2eAgInPc4d2	těžší
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
hvězdy	hvězda	k1gFnPc1	hvězda
procházejí	procházet	k5eAaImIp3nP	procházet
postupnými	postupný	k2eAgNnPc7d1	postupné
stadii	stadion	k1gNnPc7	stadion
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
jádro	jádro	k1gNnSc1	jádro
smršťuje	smršťovat	k5eAaImIp3nS	smršťovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
začne	začít	k5eAaPmIp3nS	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
atomová	atomový	k2eAgNnPc1d1	atomové
jádra	jádro	k1gNnPc1	jádro
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
fúze	fúze	k1gFnSc1	fúze
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
nemožná	možný	k2eNgFnSc1d1	nemožná
<g/>
,	,	kIx,	,
a	a	k8xC	a
nově	nově	k6eAd1	nově
uvolňovaná	uvolňovaný	k2eAgFnSc1d1	uvolňovaná
energie	energie	k1gFnSc1	energie
opět	opět	k6eAd1	opět
nastolí	nastolit	k5eAaPmIp3nS	nastolit
rovnováhu	rovnováha	k1gFnSc4	rovnováha
mezi	mezi	k7c7	mezi
tlakem	tlak	k1gInSc7	tlak
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jednoho	jeden	k4xCgNnSc2	jeden
stadia	stadion	k1gNnSc2	stadion
se	se	k3xPyFc4	se
jasnost	jasnost	k1gFnSc1	jasnost
hvězdy	hvězda	k1gFnSc2	hvězda
nepravidelně	pravidelně	k6eNd1	pravidelně
mění	měnit	k5eAaImIp3nS	měnit
–	–	k?	–
každý	každý	k3xTgInSc4	každý
nový	nový	k2eAgInSc4d1	nový
zážeh	zážeh	k1gInSc4	zážeh
fúze	fúze	k1gFnSc1	fúze
vytlačuje	vytlačovat	k5eAaImIp3nS	vytlačovat
prvky	prvek	k1gInPc4	prvek
z	z	k7c2	z
fúzujícího	fúzující	k2eAgNnSc2d1	fúzující
jádra	jádro	k1gNnSc2	jádro
do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
nazýváme	nazývat	k5eAaImIp1nP	nazývat
"	"	kIx"	"
<g/>
hvězdnou	hvězdný	k2eAgFnSc7d1	hvězdná
obálkou	obálka	k1gFnSc7	obálka
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
reakce	reakce	k1gFnSc1	reakce
se	se	k3xPyFc4	se
ztlumí	ztlumit	k5eAaPmIp3nS	ztlumit
<g/>
,	,	kIx,	,
dovolí	dovolit	k5eAaPmIp3nS	dovolit
gravitaci	gravitace	k1gFnSc4	gravitace
vmáčknout	vmáčknout	k5eAaPmF	vmáčknout
hmotu	hmota	k1gFnSc4	hmota
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
aktivního	aktivní	k2eAgNnSc2d1	aktivní
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
začít	začít	k5eAaPmF	začít
tak	tak	k6eAd1	tak
nový	nový	k2eAgInSc4d1	nový
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Limitujícím	limitující	k2eAgInSc7d1	limitující
faktorem	faktor	k1gInSc7	faktor
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
uvolněné	uvolněný	k2eAgNnSc1d1	uvolněné
fúzí	fúze	k1gFnSc7	fúze
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
závisí	záviset	k5eAaImIp3nP	záviset
na	na	k7c6	na
vazebné	vazebný	k2eAgFnSc6d1	vazebná
energii	energie	k1gFnSc6	energie
v	v	k7c6	v
atomových	atomový	k2eAgNnPc6d1	atomové
jádrech	jádro	k1gNnPc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
následný	následný	k2eAgInSc1d1	následný
krok	krok	k1gInSc1	krok
produkuje	produkovat	k5eAaImIp3nS	produkovat
postupně	postupně	k6eAd1	postupně
těžší	těžký	k2eAgInPc4d2	těžší
a	a	k8xC	a
těžší	těžký	k2eAgInPc4d2	těžší
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
těsněji	těsně	k6eAd2	těsně
svázány	svázán	k2eAgInPc1d1	svázán
silnou	silný	k2eAgFnSc7d1	silná
interakcí	interakce	k1gFnSc7	interakce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
při	při	k7c6	při
fúzi	fúze	k1gFnSc6	fúze
méně	málo	k6eAd2	málo
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
uvolňovala	uvolňovat	k5eAaImAgNnP	uvolňovat
lehčí	lehký	k2eAgNnPc1d2	lehčí
jádra	jádro	k1gNnPc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Nejtěsnější	těsný	k2eAgFnPc1d3	nejtěsnější
vazby	vazba	k1gFnPc1	vazba
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
atomovém	atomový	k2eAgNnSc6d1	atomové
jádře	jádro	k1gNnSc6	jádro
má	mít	k5eAaImIp3nS	mít
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
chemickým	chemický	k2eAgInSc7d1	chemický
symbolem	symbol	k1gInSc7	symbol
Fe	Fe	k1gFnSc2	Fe
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
"	"	kIx"	"
<g/>
dno	dno	k1gNnSc1	dno
údolí	údolí	k1gNnSc2	údolí
nuklidů	nuklid	k1gInPc2	nuklid
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
lehčí	lehký	k2eAgInPc4d2	lehčí
prvky	prvek	k1gInPc4	prvek
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
energii	energie	k1gFnSc4	energie
při	při	k7c6	při
termojaderné	termojaderný	k2eAgFnSc6d1	termojaderná
fúzi	fúze	k1gFnSc6	fúze
a	a	k8xC	a
těžší	těžký	k2eAgFnSc3d2	těžší
při	pře	k1gFnSc3	pře
štěpení	štěpení	k1gNnSc2	štěpení
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
při	při	k7c6	při
štěpné	štěpný	k2eAgFnSc6d1	štěpná
reakci	reakce	k1gFnSc6	reakce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
začne	začít	k5eAaPmIp3nS	začít
hromadit	hromadit	k5eAaImF	hromadit
železný	železný	k2eAgInSc1d1	železný
"	"	kIx"	"
<g/>
popel	popel	k1gInSc1	popel
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
gravitace	gravitace	k1gFnSc1	gravitace
do	do	k7c2	do
aktivní	aktivní	k2eAgFnSc2d1	aktivní
oblasti	oblast	k1gFnSc2	oblast
tlačí	tlačit	k5eAaImIp3nS	tlačit
více	hodně	k6eAd2	hodně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
postupně	postupně	k6eAd1	postupně
projde	projít	k5eAaPmIp3nS	projít
všemi	všecek	k3xTgFnPc7	všecek
stupni	stupeň	k1gInSc6	stupeň
fúze	fúze	k1gFnSc1	fúze
<g/>
:	:	kIx,	:
vodík	vodík	k1gInSc1	vodík
na	na	k7c4	na
hélium	hélium	k1gNnSc1	hélium
proton-protonovým	protonrotonový	k2eAgInSc7d1	proton-protonový
cyklem	cyklus	k1gInSc7	cyklus
<g/>
,	,	kIx,	,
hélium	hélium	k1gNnSc4	hélium
na	na	k7c4	na
uhlík	uhlík	k1gInSc4	uhlík
3	[number]	k4	3
<g/>
-alfa	lf	k1gMnSc2	-alf
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
uhlík	uhlík	k1gInSc4	uhlík
s	s	k7c7	s
héliem	hélium	k1gNnSc7	hélium
na	na	k7c4	na
kyslík	kyslík	k1gInSc4	kyslík
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc4	kyslík
na	na	k7c4	na
neon	neon	k1gInSc4	neon
<g/>
,	,	kIx,	,
neon	neon	k1gInSc4	neon
na	na	k7c4	na
hořčík	hořčík	k1gInSc4	hořčík
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc4	hořčík
na	na	k7c4	na
křemík	křemík	k1gInSc4	křemík
a	a	k8xC	a
křemík	křemík	k1gInSc4	křemík
na	na	k7c4	na
železo	železo	k1gNnSc4	železo
<g/>
.	.	kIx.	.
</s>
<s>
Železné	železný	k2eAgNnSc1d1	železné
(	(	kIx(	(
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
)	)	kIx)	)
jádro	jádro	k1gNnSc1	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
obrovským	obrovský	k2eAgInSc7d1	obrovský
gravitačním	gravitační	k2eAgInSc7d1	gravitační
tlakem	tlak	k1gInSc7	tlak
a	a	k8xC	a
protože	protože	k8xS	protože
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
další	další	k2eAgFnPc4d1	další
fúze	fúze	k1gFnPc4	fúze
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
tlakem	tlak	k1gInSc7	tlak
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
,	,	kIx,	,
a	a	k8xC	a
místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
tlak	tlak	k1gInSc1	tlak
elektronové	elektronový	k2eAgFnSc2d1	elektronová
degenerace	degenerace	k1gFnSc2	degenerace
–	–	k?	–
odpor	odpor	k1gInSc1	odpor
elektronů	elektron	k1gInPc2	elektron
proti	proti	k7c3	proti
stlačování	stlačování	k1gNnSc3	stlačování
k	k	k7c3	k
jiným	jiný	k2eAgMnPc3d1	jiný
elektronům	elektron	k1gInPc3	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
Chandrasekharovy	Chandrasekharův	k2eAgFnPc4d1	Chandrasekharova
meze	mez	k1gFnPc4	mez
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
přesáhne	přesáhnout	k5eAaPmIp3nS	přesáhnout
degenerační	degenerační	k2eAgInSc1d1	degenerační
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
železné	železný	k2eAgNnSc1d1	železné
jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
hroutit	hroutit	k5eAaImF	hroutit
<g/>
.	.	kIx.	.
</s>
<s>
Hroutící	hroutící	k2eAgNnSc1d1	hroutící
se	se	k3xPyFc4	se
jádro	jádro	k1gNnSc1	jádro
produkuje	produkovat	k5eAaImIp3nS	produkovat
vysoce	vysoce	k6eAd1	vysoce
enegetické	enegetický	k2eAgNnSc1d1	enegetický
gama	gama	k1gNnSc1	gama
paprsky	paprsek	k1gInPc7	paprsek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
rozbíjejí	rozbíjet	k5eAaImIp3nP	rozbíjet
některá	některý	k3yIgNnPc1	některý
železná	železný	k2eAgNnPc1d1	železné
jádra	jádro	k1gNnPc1	jádro
na	na	k7c4	na
13	[number]	k4	13
He	he	k0	he
a	a	k8xC	a
4	[number]	k4	4
neutrony	neutron	k1gInPc1	neutron
<g/>
,	,	kIx,	,
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
známém	známý	k2eAgInSc6d1	známý
jako	jako	k8xC	jako
fotodisociace	fotodisociace	k1gFnSc2	fotodisociace
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
jaderná	jaderný	k2eAgFnSc1d1	jaderná
reakce	reakce	k1gFnSc1	reakce
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
železa	železo	k1gNnSc2	železo
však	však	k9	však
nemůže	moct	k5eNaImIp3nS	moct
uvolnit	uvolnit	k5eAaPmF	uvolnit
energii	energie	k1gFnSc4	energie
<g/>
;	;	kIx,	;
může	moct	k5eAaImIp3nS	moct
ji	on	k3xPp3gFnSc4	on
jen	jen	k9	jen
absorbovat	absorbovat	k5eAaBmF	absorbovat
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
reakce	reakce	k1gFnSc1	reakce
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
po	po	k7c4	po
milióny	milión	k4xCgInPc4	milión
let	let	k1gInSc4	let
vyzařovaly	vyzařovat	k5eAaImAgInP	vyzařovat
energii	energie	k1gFnSc4	energie
ven	ven	k6eAd1	ven
a	a	k8xC	a
udržovaly	udržovat	k5eAaImAgFnP	udržovat
hvězdu	hvězda	k1gFnSc4	hvězda
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
proti	proti	k7c3	proti
gravitaci	gravitace	k1gFnSc3	gravitace
<g/>
,	,	kIx,	,
náhle	náhle	k6eAd1	náhle
začínají	začínat	k5eAaImIp3nP	začínat
naopak	naopak	k6eAd1	naopak
energii	energie	k1gFnSc4	energie
pohlcovat	pohlcovat	k5eAaImF	pohlcovat
<g/>
,	,	kIx,	,
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
gravitaci	gravitace	k1gFnSc4	gravitace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
masívní	masívní	k2eAgFnSc1d1	masívní
struktura	struktura	k1gFnSc1	struktura
velikosti	velikost	k1gFnSc2	velikost
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
zhroutí	zhroutit	k5eAaPmIp3nS	zhroutit
ve	v	k7c6	v
zlomku	zlomek	k1gInSc6	zlomek
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
hustota	hustota	k1gFnSc1	hustota
hroutícího	hroutící	k2eAgMnSc2d1	hroutící
se	se	k3xPyFc4	se
jádra	jádro	k1gNnSc2	jádro
prudce	prudko	k6eAd1	prudko
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
,	,	kIx,	,
elektrony	elektron	k1gInPc1	elektron
a	a	k8xC	a
protony	proton	k1gInPc1	proton
jsou	být	k5eAaImIp3nP	být
tlačeny	tlačit	k5eAaImNgInP	tlačit
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
jejich	jejich	k3xOp3gNnSc1	jejich
elektrické	elektrický	k2eAgNnSc1d1	elektrické
přitahování	přitahování	k1gNnSc1	přitahování
nepřekoná	překonat	k5eNaPmIp3nS	překonat
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
jaderné	jaderný	k2eAgNnSc4d1	jaderné
odpuzování	odpuzování	k1gNnSc4	odpuzování
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
reakci	reakce	k1gFnSc6	reakce
<g/>
,	,	kIx,	,
obráceném	obrácený	k2eAgInSc6d1	obrácený
beta-rozpadu	betaozpad	k1gInSc6	beta-rozpad
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
elektron	elektron	k1gInSc1	elektron
vtlačen	vtlačen	k2eAgInSc1d1	vtlačen
do	do	k7c2	do
protonu	proton	k1gInSc2	proton
<g/>
,	,	kIx,	,
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
se	se	k3xPyFc4	se
neutrino	neutrino	k1gNnSc1	neutrino
a	a	k8xC	a
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
neutron	neutron	k1gInSc4	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Únik	únik	k1gInSc1	únik
neutrina	neutrino	k1gNnSc2	neutrino
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
odčerpávání	odčerpávání	k1gNnSc2	odčerpávání
energie	energie	k1gFnSc2	energie
dále	daleko	k6eAd2	daleko
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
kolaps	kolaps	k1gInSc1	kolaps
<g/>
,	,	kIx,	,
následkem	následkem	k7c2	následkem
čehož	což	k3yRnSc2	což
oddělení	oddělení	k1gNnSc3	oddělení
hvězdného	hvězdný	k2eAgNnSc2d1	Hvězdné
jádra	jádro	k1gNnSc2	jádro
od	od	k7c2	od
vnějších	vnější	k2eAgFnPc2d1	vnější
vrstev	vrstva	k1gFnPc2	vrstva
a	a	k8xC	a
dosažení	dosažení	k1gNnSc4	dosažení
hustoty	hustota	k1gFnSc2	hustota
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
trvá	trvat	k5eAaImIp3nS	trvat
pouhé	pouhý	k2eAgFnPc4d1	pouhá
milisekundy	milisekunda	k1gFnPc4	milisekunda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
hustotě	hustota	k1gFnSc6	hustota
brání	bránit	k5eAaImIp3nS	bránit
dalšímu	další	k2eAgNnSc3d1	další
stlačování	stlačování	k1gNnSc3	stlačování
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
odpor	odpor	k1gInSc4	odpor
neutronů	neutron	k1gInPc2	neutron
způsobený	způsobený	k2eAgInSc4d1	způsobený
jejich	jejich	k3xOp3gFnPc7	jejich
kvantovými	kvantový	k2eAgFnPc7d1	kvantová
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
fermiony	fermion	k1gInPc4	fermion
podléhající	podléhající	k2eAgInSc1d1	podléhající
vylučovacímu	vylučovací	k2eAgInSc3d1	vylučovací
principu	princip	k1gInSc3	princip
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
okamžiku	okamžik	k1gInSc6	okamžik
je	být	k5eAaImIp3nS	být
neutronový	neutronový	k2eAgInSc4d1	neutronový
degenerační	degenerační	k2eAgInSc4d1	degenerační
tlak	tlak	k1gInSc4	tlak
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
k	k	k7c3	k
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
;	;	kIx,	;
jádro	jádro	k1gNnSc1	jádro
však	však	k9	však
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
přesáhne	přesáhnout	k5eAaPmIp3nS	přesáhnout
bod	bod	k1gInSc4	bod
rovnováhy	rovnováha	k1gFnSc2	rovnováha
a	a	k8xC	a
podléhá	podléhat	k5eAaImIp3nS	podléhat
nepatrnému	patrný	k2eNgNnSc3d1	nepatrné
pružení	pružení	k1gNnSc3	pružení
<g/>
,	,	kIx,	,
vytvářejíce	vytvářet	k5eAaImSgFnP	vytvářet
rázové	rázový	k2eAgInPc4d1	rázový
vlny	vlna	k1gFnPc1	vlna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
narážejí	narážet	k5eAaImIp3nP	narážet
do	do	k7c2	do
kolabujích	kolabuj	k1gInPc6	kolabuj
vnějších	vnější	k2eAgFnPc2d1	vnější
vrstev	vrstva	k1gFnPc2	vrstva
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zárodek	zárodek	k1gInSc1	zárodek
neutronové	neutronový	k2eAgFnSc2d1	neutronová
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
zformoval	zformovat	k5eAaPmAgMnS	zformovat
<g/>
,	,	kIx,	,
dostatečně	dostatečně	k6eAd1	dostatečně
masívní	masívní	k2eAgFnSc1d1	masívní
<g/>
,	,	kIx,	,
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
kolapsu	kolaps	k1gInSc6	kolaps
a	a	k8xC	a
skončí	skončit	k5eAaPmIp3nS	skončit
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
jako	jako	k9	jako
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
hmotnosti	hmotnost	k1gFnSc6	hmotnost
kolaps	kolaps	k1gInSc1	kolaps
zastaví	zastavit	k5eAaPmIp3nS	zastavit
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
předpovězených	předpovězený	k2eAgInPc2d1	předpovězený
stabilních	stabilní	k2eAgInPc2d1	stabilní
mezistavů	mezistav	k1gInPc2	mezistav
<g/>
.	.	kIx.	.
</s>
<s>
Takovým	takový	k3xDgInSc7	takový
přechodem	přechod	k1gInSc7	přechod
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hypotetická	hypotetický	k2eAgFnSc1d1	hypotetická
hyperonová	hyperonový	k2eAgFnSc1d1	hyperonový
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
neutronový	neutronový	k2eAgInSc4d1	neutronový
plyn	plyn	k1gInSc4	plyn
byl	být	k5eAaImAgMnS	být
stlačením	stlačení	k1gNnSc7	stlačení
dále	daleko	k6eAd2	daleko
degenerován	degenerován	k2eAgMnSc1d1	degenerován
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
vybuzeny	vybuzen	k2eAgInPc1d1	vybuzen
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
hyperonů	hyperon	k1gInPc2	hyperon
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ani	ani	k8xC	ani
degenerační	degenerační	k2eAgInSc1d1	degenerační
tlak	tlak	k1gInSc1	tlak
hyperonového	hyperonový	k2eAgNnSc2d1	hyperonový
plazmatu	plazma	k1gNnSc2	plazma
není	být	k5eNaImIp3nS	být
s	s	k7c7	s
to	ten	k3xDgNnSc4	ten
odolat	odolat	k5eAaPmF	odolat
gravitaci	gravitace	k1gFnSc4	gravitace
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
kolaps	kolaps	k1gInSc4	kolaps
zastavit	zastavit	k5eAaPmF	zastavit
ještě	ještě	k9	ještě
ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
kvarkové	kvarkový	k2eAgFnSc2d1	kvarková
hvězdy	hvězda	k1gFnSc2	hvězda
skládající	skládající	k2eAgFnSc2d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
kvark-gluonového	kvarkluonový	k2eAgNnSc2d1	kvark-gluonový
plazmatu	plazma	k1gNnSc2	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Kvarky	kvark	k1gInPc1	kvark
jsou	být	k5eAaImIp3nP	být
opět	opět	k6eAd1	opět
fermiony	fermion	k1gInPc1	fermion
a	a	k8xC	a
díky	dík	k1gInPc1	dík
Pauliho	Pauli	k1gMnSc2	Pauli
vylučovacímu	vylučovací	k2eAgInSc3d1	vylučovací
principu	princip	k1gInSc3	princip
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
schopné	schopný	k2eAgFnPc1d1	schopná
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
gravitační	gravitační	k2eAgInSc4d1	gravitační
tlak	tlak	k1gInSc4	tlak
vytvořením	vytvoření	k1gNnSc7	vytvoření
degenerovaného	degenerovaný	k2eAgInSc2d1	degenerovaný
plynu	plyn	k1gInSc2	plyn
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
elektrony	elektron	k1gInPc1	elektron
v	v	k7c6	v
případě	případ	k1gInSc6	případ
bílých	bílý	k2eAgMnPc2d1	bílý
trpaslíků	trpaslík	k1gMnPc2	trpaslík
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
v	v	k7c6	v
neutronových	neutronový	k2eAgFnPc6d1	neutronová
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
kvarkových	kvarkův	k2eAgFnPc2d1	kvarkův
hvězd	hvězda	k1gFnPc2	hvězda
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
nebyla	být	k5eNaImAgFnS	být
dostatečně	dostatečně	k6eAd1	dostatečně
podložena	podložit	k5eAaPmNgFnS	podložit
pozorováním	pozorování	k1gNnSc7	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
fáze	fáze	k1gFnSc1	fáze
kolapsu	kolaps	k1gInSc2	kolaps
jádra	jádro	k1gNnSc2	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
rychlá	rychlý	k2eAgFnSc1d1	rychlá
a	a	k8xC	a
energetická	energetický	k2eAgFnSc1d1	energetická
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
neutrina	neutrino	k1gNnPc1	neutrino
jsou	být	k5eAaImIp3nP	být
schopna	schopen	k2eAgNnPc1d1	schopno
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
gravitační	gravitační	k2eAgFnSc2d1	gravitační
potenciální	potenciální	k2eAgFnSc2d1	potenciální
energie	energie	k1gFnSc2	energie
kolapsu	kolaps	k1gInSc2	kolaps
je	být	k5eAaImIp3nS	být
přeměněna	přeměnit	k5eAaPmNgFnS	přeměnit
na	na	k7c4	na
10	[number]	k4	10
sekundový	sekundový	k2eAgInSc4d1	sekundový
záblesk	záblesk	k1gInSc4	záblesk
neutrin	neutrino	k1gNnPc2	neutrino
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
1046	[number]	k4	1046
J.	J.	kA	J.
Část	část	k1gFnSc1	část
této	tento	k3xDgFnSc2	tento
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1044	[number]	k4	1044
J	J	kA	J
je	být	k5eAaImIp3nS	být
reabsorbována	reabsorbován	k2eAgFnSc1d1	reabsorbován
explodující	explodující	k2eAgFnSc7d1	explodující
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
připadající	připadající	k2eAgFnSc1d1	připadající
na	na	k7c6	na
částici	částice	k1gFnSc6	částice
v	v	k7c6	v
supernově	supernova	k1gFnSc6	supernova
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
desítky	desítka	k1gFnPc4	desítka
až	až	k8xS	až
stovky	stovka	k1gFnPc4	stovka
MeV	MeV	k1gFnSc2	MeV
(	(	kIx(	(
<g/>
1	[number]	k4	1
až	až	k9	až
150	[number]	k4	150
pJ	pJ	k?	pJ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neutrina	neutrino	k1gNnPc4	neutrino
produkovaná	produkovaný	k2eAgFnSc1d1	produkovaná
supernovou	supernova	k1gFnSc7	supernova
byla	být	k5eAaImAgFnS	být
skutečně	skutečně	k6eAd1	skutečně
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
supernovy	supernova	k1gFnSc2	supernova
1987A	[number]	k4	1987A
a	a	k8xC	a
ubezpečila	ubezpečit	k5eAaPmAgFnS	ubezpečit
astronomy	astronom	k1gMnPc4	astronom
<g/>
,	,	kIx,	,
že	že	k8xS	že
základní	základní	k2eAgInSc1d1	základní
obraz	obraz	k1gInSc1	obraz
kolapsu	kolaps	k1gInSc2	kolaps
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
principu	princip	k1gInSc6	princip
správný	správný	k2eAgInSc1d1	správný
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
souběžně	souběžně	k6eAd1	souběžně
pracujících	pracující	k2eAgInPc2d1	pracující
detektorů	detektor	k1gInPc2	detektor
neutrin	neutrino	k1gNnPc2	neutrino
založilo	založit	k5eAaPmAgNnS	založit
SNEWS	SNEWS	kA	SNEWS
<g/>
,	,	kIx,	,
systém	systém	k1gInSc4	systém
varování	varování	k1gNnSc4	varování
před	před	k7c7	před
supernovami	supernova	k1gFnPc7	supernova
(	(	kIx(	(
<g/>
Supernova	supernova	k1gFnSc1	supernova
Early	earl	k1gMnPc4	earl
Warning	Warning	k1gInSc1	Warning
System	Syst	k1gMnSc7	Syst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
včasné	včasný	k2eAgNnSc4d1	včasné
upozornění	upozornění	k1gNnSc4	upozornění
komunity	komunita	k1gFnSc2	komunita
astronomů	astronom	k1gMnPc2	astronom
na	na	k7c6	na
přicházející	přicházející	k2eAgFnSc6d1	přicházející
explozi	exploze	k1gFnSc6	exploze
supernovy	supernova	k1gFnSc2	supernova
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
standardní	standardní	k2eAgInSc1d1	standardní
model	model	k1gInSc1	model
částicové	částicový	k2eAgFnSc2d1	částicová
fyziky	fyzika	k1gFnSc2	fyzika
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnPc1d2	vyšší
hustoty	hustota	k1gFnPc1	hustota
si	se	k3xPyFc3	se
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
vynutit	vynutit	k5eAaPmF	vynutit
jeho	jeho	k3xOp3gFnPc4	jeho
korekce	korekce	k1gFnPc4	korekce
<g/>
.	.	kIx.	.
</s>
<s>
Pozemské	pozemský	k2eAgInPc1d1	pozemský
akcelerátory	akcelerátor	k1gInPc1	akcelerátor
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
vytvořit	vytvořit	k5eAaPmF	vytvořit
interakce	interakce	k1gFnPc1	interakce
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
u	u	k7c2	u
supernov	supernova	k1gFnPc2	supernova
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
experimenty	experiment	k1gInPc1	experiment
však	však	k9	však
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
pouze	pouze	k6eAd1	pouze
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
částice	částice	k1gFnPc1	částice
interagující	interagující	k2eAgFnPc1d1	interagující
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
částicemi	částice	k1gFnPc7	částice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
hustot	hustota	k1gFnPc2	hustota
uvnitř	uvnitř	k7c2	uvnitř
supernovy	supernova	k1gFnSc2	supernova
vznikají	vznikat	k5eAaImIp3nP	vznikat
neočekávané	očekávaný	k2eNgInPc1d1	neočekávaný
efekty	efekt	k1gInPc1	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Interakce	interakce	k1gFnSc1	interakce
mezi	mezi	k7c7	mezi
neutriny	neutrino	k1gNnPc7	neutrino
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
částicemi	částice	k1gFnPc7	částice
uvnitř	uvnitř	k7c2	uvnitř
supernovy	supernova	k1gFnSc2	supernova
jsou	být	k5eAaImIp3nP	být
určovány	určovat	k5eAaImNgFnP	určovat
slabou	slabý	k2eAgFnSc7d1	slabá
interakcí	interakce	k1gFnSc7	interakce
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
modelování	modelování	k1gNnSc4	modelování
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
zvládnuto	zvládnut	k2eAgNnSc1d1	zvládnuto
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
interakce	interakce	k1gFnSc1	interakce
mezi	mezi	k7c7	mezi
protony	proton	k1gInPc7	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
jsou	být	k5eAaImIp3nP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
především	především	k9	především
silnou	silný	k2eAgFnSc7d1	silná
interakcí	interakce	k1gFnSc7	interakce
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yIgFnSc2	který
jsou	být	k5eAaImIp3nP	být
výpočetní	výpočetní	k2eAgInPc4d1	výpočetní
modely	model	k1gInPc4	model
mnohem	mnohem	k6eAd1	mnohem
složitější	složitý	k2eAgInPc4d2	složitější
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
nevyřešeným	vyřešený	k2eNgInSc7d1	nevyřešený
problémem	problém	k1gInSc7	problém
supernov	supernova	k1gFnPc2	supernova
typu	typ	k1gInSc2	typ
II	II	kA	II
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nerozumíme	rozumět	k5eNaImIp1nP	rozumět
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
záblesky	záblesk	k1gInPc4	záblesk
neutrin	neutrino	k1gNnPc2	neutrino
přenášejí	přenášet	k5eAaImIp3nP	přenášet
energii	energie	k1gFnSc4	energie
na	na	k7c4	na
zbytek	zbytek	k1gInSc4	zbytek
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
rázovou	rázový	k2eAgFnSc4d1	rázová
vlnu	vlna	k1gFnSc4	vlna
<g/>
,	,	kIx,	,
způsobující	způsobující	k2eAgFnSc4d1	způsobující
její	její	k3xOp3gFnSc3	její
explozi	exploze	k1gFnSc3	exploze
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
předchozího	předchozí	k2eAgNnSc2d1	předchozí
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
exploze	exploze	k1gFnSc2	exploze
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
pouhé	pouhý	k2eAgNnSc1d1	pouhé
jedno	jeden	k4xCgNnSc1	jeden
procento	procento	k1gNnSc1	procento
vyzářené	vyzářený	k2eAgFnSc2d1	vyzářená
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
objasnit	objasnit	k5eAaPmF	objasnit
jeho	jeho	k3xOp3gNnSc4	jeho
získání	získání	k1gNnSc4	získání
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
obtížným	obtížný	k2eAgMnPc3d1	obtížný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
modelů	model	k1gInPc2	model
vysvětloval	vysvětlovat	k5eAaImAgInS	vysvětlovat
tento	tento	k3xDgInSc4	tento
fakt	fakt	k1gInSc4	fakt
mj.	mj.	kA	mj.
mechanismem	mechanismus	k1gInSc7	mechanismus
zvrácení	zvrácení	k1gNnSc2	zvrácení
konvekce	konvekce	k1gFnSc2	konvekce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
konvekce	konvekce	k1gFnSc1	konvekce
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
neutrin	neutrino	k1gNnPc2	neutrino
zevnitř	zevnitř	k6eAd1	zevnitř
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
padající	padající	k2eAgFnSc2d1	padající
hmoty	hmota	k1gFnSc2	hmota
shora	shora	k6eAd1	shora
<g/>
,	,	kIx,	,
dokončí	dokončit	k5eAaPmIp3nS	dokončit
proces	proces	k1gInSc1	proces
destrukce	destrukce	k1gFnSc2	destrukce
původní	původní	k2eAgFnSc2d1	původní
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
exploze	exploze	k1gFnSc2	exploze
jsou	být	k5eAaImIp3nP	být
zachycováním	zachycování	k1gNnSc7	zachycování
neutronů	neutron	k1gInPc2	neutron
vytvářeny	vytvářen	k2eAgInPc4d1	vytvářen
prvky	prvek	k1gInPc4	prvek
těžší	těžký	k2eAgFnSc1d2	těžší
než	než	k8xS	než
železo	železo	k1gNnSc1	železo
a	a	k8xC	a
díky	díky	k7c3	díky
tlaku	tlak	k1gInSc3	tlak
neutrin	neutrino	k1gNnPc2	neutrino
na	na	k7c4	na
okraje	okraj	k1gInPc4	okraj
"	"	kIx"	"
<g/>
neutrinosféry	neutrinosféra	k1gFnPc4	neutrinosféra
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
okolní	okolní	k2eAgInSc4d1	okolní
prostor	prostor	k1gInSc4	prostor
obohacen	obohacen	k2eAgInSc4d1	obohacen
oblaky	oblak	k1gInPc4	oblak
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
prachu	prach	k1gInSc2	prach
bohatšími	bohatý	k2eAgInPc7d2	bohatší
na	na	k7c4	na
těžší	těžký	k2eAgInPc4d2	těžší
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
původně	původně	k6eAd1	původně
pocházely	pocházet	k5eAaImAgFnP	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
Neutrinová	Neutrinový	k2eAgFnSc1d1	Neutrinová
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
standardním	standardní	k2eAgInSc6d1	standardní
modelu	model	k1gInSc6	model
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
pochopení	pochopení	k1gNnSc3	pochopení
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
klíčová	klíčový	k2eAgNnPc1d1	klíčové
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
oblastí	oblast	k1gFnSc7	oblast
výzkumů	výzkum	k1gInPc2	výzkum
je	být	k5eAaImIp3nS	být
magnetohydrodynamika	magnetohydrodynamika	k1gFnSc1	magnetohydrodynamika
plazmatu	plazma	k1gNnSc2	plazma
(	(	kIx(	(
<g/>
MHD	MHD	kA	MHD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
umírající	umírající	k2eAgFnSc1d1	umírající
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
chování	chování	k1gNnSc1	chování
během	během	k7c2	během
hroucení	hroucení	k1gNnSc2	hroucení
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
"	"	kIx"	"
<g/>
rázová	rázový	k2eAgFnSc1d1	rázová
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
kdy	kdy	k6eAd1	kdy
a	a	k8xC	a
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
zastaví	zastavit	k5eAaPmIp3nP	zastavit
<g/>
"	"	kIx"	"
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
načerpá	načerpat	k5eAaPmIp3nS	načerpat
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Počítačové	počítačový	k2eAgInPc1d1	počítačový
modely	model	k1gInPc1	model
jsou	být	k5eAaImIp3nP	být
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
ve	v	k7c6	v
výpočtech	výpočet	k1gInPc6	výpočet
chování	chování	k1gNnSc2	chování
supernov	supernova	k1gFnPc2	supernova
typu	typ	k1gInSc2	typ
II	II	kA	II
pouze	pouze	k6eAd1	pouze
od	od	k7c2	od
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
rázová	rázový	k2eAgFnSc1d1	rázová
vlna	vlna	k1gFnSc1	vlna
již	již	k6eAd1	již
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ignorujeme	ignorovat	k5eAaImIp1nP	ignorovat
první	první	k4xOgFnSc4	první
sekundu	sekunda	k1gFnSc4	sekunda
exploze	exploze	k1gFnSc2	exploze
a	a	k8xC	a
předpokládáme	předpokládat	k5eAaImIp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
exploze	exploze	k1gFnSc1	exploze
již	již	k6eAd1	již
začala	začít	k5eAaPmAgFnS	začít
<g/>
,	,	kIx,	,
astrofyzikové	astrofyzik	k1gMnPc1	astrofyzik
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
detailně	detailně	k6eAd1	detailně
předpovědět	předpovědět	k5eAaPmF	předpovědět
prvky	prvek	k1gInPc1	prvek
produkované	produkovaný	k2eAgInPc1d1	produkovaný
supernovou	supernova	k1gFnSc7	supernova
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
očekávanou	očekávaný	k2eAgFnSc4d1	očekávaná
světelnou	světelný	k2eAgFnSc4d1	světelná
křivku	křivka	k1gFnSc4	křivka
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgNnSc1d1	zbývající
jádro	jádro	k1gNnSc1	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
hmotnosti	hmotnost	k1gFnSc6	hmotnost
stát	stát	k5eAaPmF	stát
buď	buď	k8xC	buď
neutronovou	neutronový	k2eAgFnSc7d1	neutronová
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
černou	černý	k2eAgFnSc7d1	černá
dírou	díra	k1gFnSc7	díra
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
dosud	dosud	k6eAd1	dosud
pouze	pouze	k6eAd1	pouze
hypotetickou	hypotetický	k2eAgFnSc7d1	hypotetická
hyperonovou	hyperonův	k2eAgFnSc7d1	hyperonův
nebo	nebo	k8xC	nebo
kvarkovou	kvarkův	k2eAgFnSc7d1	kvarkův
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
však	však	k8xC	však
mechanismu	mechanismus	k1gInSc3	mechanismus
kolapsu	kolaps	k1gInSc2	kolaps
supernovy	supernova	k1gFnSc2	supernova
málo	málo	k6eAd1	málo
rozumíme	rozumět	k5eAaImIp1nP	rozumět
<g/>
,	,	kIx,	,
hraniční	hraniční	k2eAgFnPc1d1	hraniční
hmotnosti	hmotnost	k1gFnPc1	hmotnost
neznáme	znát	k5eNaImIp1nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Supernovy	supernova	k1gFnPc1	supernova
typu	typ	k1gInSc2	typ
II	II	kA	II
lze	lze	k6eAd1	lze
ještě	ještě	k6eAd1	ještě
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
jejich	jejich	k3xOp3gFnPc2	jejich
světelných	světelný	k2eAgFnPc2d1	světelná
křivek	křivka	k1gFnPc2	křivka
na	na	k7c4	na
typy	typ	k1gInPc4	typ
II-P	II-P	k1gMnPc2	II-P
a	a	k8xC	a
II-	II-	k1gMnPc2	II-
<g/>
L.	L.	kA	L.
Typ	typ	k1gInSc4	typ
II-P	II-P	k1gFnSc7	II-P
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
světelné	světelný	k2eAgFnSc6d1	světelná
křivce	křivka	k1gFnSc6	křivka
"	"	kIx"	"
<g/>
plošinu	plošina	k1gFnSc4	plošina
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
plateau	plateau	k6eAd1	plateau
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
II-L	II-L	k1gFnSc1	II-L
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
má	mít	k5eAaImIp3nS	mít
"	"	kIx"	"
<g/>
lineární	lineární	k2eAgInSc1d1	lineární
<g/>
"	"	kIx"	"
pokles	pokles	k1gInSc1	pokles
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
linear	linear	k1gInSc1	linear
<g/>
,	,	kIx,	,
lineární	lineární	k2eAgInSc1d1	lineární
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
magnitudy	magnituda	k1gFnSc2	magnituda
na	na	k7c6	na
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
exponenciální	exponenciální	k2eAgFnSc6d1	exponenciální
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
jasnosti	jasnost	k1gFnSc2	jasnost
na	na	k7c6	na
čase	čas	k1gInSc6	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
rozdílné	rozdílný	k2eAgNnSc1d1	rozdílné
chování	chování	k1gNnSc1	chování
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
obálce	obálka	k1gFnSc6	obálka
těchto	tento	k3xDgFnPc2	tento
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Supernovy	supernova	k1gFnPc1	supernova
Typu	typ	k1gInSc2	typ
II-P	II-P	k1gFnPc1	II-P
mají	mít	k5eAaImIp3nP	mít
velkou	velký	k2eAgFnSc4d1	velká
vodíkovou	vodíkový	k2eAgFnSc4d1	vodíková
obálku	obálka	k1gFnSc4	obálka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zachytí	zachytit	k5eAaPmIp3nS	zachytit
energii	energie	k1gFnSc4	energie
vyslanou	vyslaný	k2eAgFnSc4d1	vyslaná
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
gama	gama	k1gNnSc2	gama
paprsků	paprsek	k1gInPc2	paprsek
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
ji	on	k3xPp3gFnSc4	on
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
typu	typ	k1gInSc2	typ
II-L	II-L	k1gFnSc2	II-L
se	se	k3xPyFc4	se
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnPc1d2	menší
obálky	obálka	k1gFnPc1	obálka
přeměňující	přeměňující	k2eAgFnPc1d1	přeměňující
méně	málo	k6eAd2	málo
gama	gama	k1gNnSc1	gama
záření	záření	k1gNnSc2	záření
do	do	k7c2	do
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Supernovy	supernova	k1gFnPc1	supernova
typu	typ	k1gInSc2	typ
II	II	kA	II
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
také	také	k9	také
dále	daleko	k6eAd2	daleko
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gNnSc2	jejich
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
většina	většina	k1gFnSc1	většina
supernov	supernova	k1gFnPc2	supernova
typu	typ	k1gInSc2	typ
II	II	kA	II
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
velmi	velmi	k6eAd1	velmi
širokými	široký	k2eAgFnPc7d1	široká
emisními	emisní	k2eAgFnPc7d1	emisní
čarami	čára	k1gFnPc7	čára
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
expanzní	expanzní	k2eAgFnSc2d1	expanzní
rychlosti	rychlost	k1gFnSc2	rychlost
mnoha	mnoho	k4c2	mnoho
tisíc	tisíc	k4xCgInPc2	tisíc
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
mají	mít	k5eAaImIp3nP	mít
relativně	relativně	k6eAd1	relativně
úzké	úzký	k2eAgInPc4d1	úzký
rysy	rys	k1gInPc4	rys
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
interakcí	interakce	k1gFnSc7	interakce
obálky	obálka	k1gFnSc2	obálka
s	s	k7c7	s
mezihvězdným	mezihvězdný	k2eAgInSc7d1	mezihvězdný
materiálem	materiál	k1gInSc7	materiál
<g/>
;	;	kIx,	;
nazýváme	nazývat	k5eAaImIp1nP	nazývat
je	on	k3xPp3gInPc4	on
typ	typ	k1gInSc4	typ
IIn	IIn	k1gFnSc3	IIn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
"	"	kIx"	"
<g/>
n	n	k0	n
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
úzký	úzký	k2eAgInSc1d1	úzký
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
narrow	narrow	k?	narrow
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
několika	několik	k4yIc2	několik
supernov	supernova	k1gFnPc2	supernova
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
SN	SN	kA	SN
1987K	[number]	k4	1987K
a	a	k8xC	a
1993	[number]	k4	1993
<g/>
J	J	kA	J
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
změnily	změnit	k5eAaPmAgInP	změnit
typ	typ	k1gInSc4	typ
<g/>
:	:	kIx,	:
zpočátku	zpočátku	k6eAd1	zpočátku
vykazovaly	vykazovat	k5eAaImAgFnP	vykazovat
čáry	čára	k1gFnPc1	čára
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
však	však	k9	však
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
týdnů	týden	k1gInPc2	týden
či	či	k8xC	či
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
dominovat	dominovat	k5eAaImF	dominovat
čáry	čára	k1gFnPc4	čára
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
této	tento	k3xDgFnSc2	tento
kombinace	kombinace	k1gFnSc2	kombinace
rysů	rys	k1gMnPc2	rys
typů	typ	k1gInPc2	typ
II	II	kA	II
a	a	k8xC	a
Ib	Ib	k1gFnSc1	Ib
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
typ	typ	k1gInSc1	typ
IIb	IIb	k1gFnSc2	IIb
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
nejspíš	nejspíš	k9	nejspíš
o	o	k7c4	o
masívní	masívní	k2eAgFnPc4d1	masívní
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ztratily	ztratit	k5eAaPmAgFnP	ztratit
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
všechen	všechen	k3xTgInSc4	všechen
vodíkový	vodíkový	k2eAgInSc4d1	vodíkový
obal	obal	k1gInSc4	obal
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
zbytky	zbytek	k1gInPc1	zbytek
supernovy	supernova	k1gFnSc2	supernova
expandují	expandovat	k5eAaImIp3nP	expandovat
<g/>
,	,	kIx,	,
vodíková	vodíkový	k2eAgFnSc1d1	vodíková
vrstva	vrstva	k1gFnSc1	vrstva
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
stane	stanout	k5eAaPmIp3nS	stanout
opticky	opticky	k6eAd1	opticky
průsvitnou	průsvitný	k2eAgFnSc4d1	průsvitná
a	a	k8xC	a
odhalí	odhalit	k5eAaPmIp3nP	odhalit
hlubší	hluboký	k2eAgFnPc1d2	hlubší
vrstvy	vrstva	k1gFnPc1	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
výjimečně	výjimečně	k6eAd1	výjimečně
velké	velký	k2eAgFnPc1d1	velká
hvězdy	hvězda	k1gFnPc1	hvězda
mohou	moct	k5eAaImIp3nP	moct
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
zánikem	zánik	k1gInSc7	zánik
vytvořit	vytvořit	k5eAaPmF	vytvořit
"	"	kIx"	"
<g/>
hypernovu	hypernův	k2eAgFnSc4d1	hypernova
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
navrženém	navržený	k2eAgInSc6d1	navržený
mechanismu	mechanismus	k1gInSc6	mechanismus
hypernovy	hypernov	k1gInPc7	hypernov
se	se	k3xPyFc4	se
jádro	jádro	k1gNnSc1	jádro
extrémně	extrémně	k6eAd1	extrémně
masívní	masívní	k2eAgFnSc2d1	masívní
hvězdy	hvězda	k1gFnSc2	hvězda
hroutí	hroutit	k5eAaImIp3nS	hroutit
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
extrémně	extrémně	k6eAd1	extrémně
energetické	energetický	k2eAgInPc1d1	energetický
výtrysky	výtrysk	k1gInPc1	výtrysk
plazmatu	plazma	k1gNnSc2	plazma
jsou	být	k5eAaImIp3nP	být
vymrštěny	vymrštit	k5eAaPmNgFnP	vymrštit
takřka	takřka	k6eAd1	takřka
světelnou	světelný	k2eAgFnSc7d1	světelná
rychlostí	rychlost	k1gFnSc7	rychlost
z	z	k7c2	z
pólů	pól	k1gInPc2	pól
její	její	k3xOp3gFnSc2	její
rotace	rotace	k1gFnSc2	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výtrysky	výtrysk	k1gInPc1	výtrysk
emitují	emitovat	k5eAaBmIp3nP	emitovat
intenzívní	intenzívní	k2eAgNnSc4d1	intenzívní
gama	gama	k1gNnSc4	gama
paprsky	paprsek	k1gInPc4	paprsek
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
několik	několik	k4yIc4	několik
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
gama	gama	k1gNnSc2	gama
záblesků	záblesk	k1gInPc2	záblesk
<g/>
.	.	kIx.	.
</s>
<s>
Objevy	objev	k1gInPc1	objev
supernov	supernova	k1gFnPc2	supernova
jsou	být	k5eAaImIp3nP	být
oznamovány	oznamovat	k5eAaImNgInP	oznamovat
na	na	k7c4	na
Centrálu	centrála	k1gFnSc4	centrála
astronomických	astronomický	k2eAgInPc2d1	astronomický
telegramů	telegram	k1gInPc2	telegram
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
astronomické	astronomický	k2eAgFnSc2d1	astronomická
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vydá	vydat	k5eAaPmIp3nS	vydat
oběžník	oběžník	k1gInSc4	oběžník
s	s	k7c7	s
přiděleným	přidělený	k2eAgInSc7d1	přidělený
názvem	název	k1gInSc7	název
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
roku	rok	k1gInSc2	rok
objevu	objev	k1gInSc2	objev
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
nebo	nebo	k8xC	nebo
dvoupísmenného	dvoupísmenný	k2eAgNnSc2d1	dvoupísmenné
označení	označení	k1gNnSc2	označení
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
26	[number]	k4	26
supernov	supernova	k1gFnPc2	supernova
v	v	k7c6	v
roce	rok	k1gInSc6	rok
dostává	dostávat	k5eAaImIp3nS	dostávat
písmena	písmeno	k1gNnPc4	písmeno
od	od	k7c2	od
A	A	kA	A
do	do	k7c2	do
Z.	Z.	kA	Z.
Po	po	k7c6	po
Z	Z	kA	Z
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
aa	aa	k?	aa
<g/>
,	,	kIx,	,
ab	ab	k?	ab
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
letopočty	letopočet	k1gInPc1	letopočet
zde	zde	k6eAd1	zde
uvedené	uvedený	k2eAgInPc1d1	uvedený
představují	představovat	k5eAaImIp3nP	představovat
okamžik	okamžik	k1gInSc4	okamžik
prvního	první	k4xOgNnSc2	první
pozorování	pozorování	k1gNnSc2	pozorování
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Událost	událost	k1gFnSc1	událost
samotná	samotný	k2eAgFnSc1d1	samotná
nastala	nastat	k5eAaPmAgFnS	nastat
ve	v	k7c6	v
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
stovek	stovka	k1gFnPc2	stovka
nebo	nebo	k8xC	nebo
tisíců	tisíc	k4xCgInPc2	tisíc
světelných	světelný	k2eAgInPc2d1	světelný
let	let	k1gInSc4	let
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
tomu	ten	k3xDgNnSc3	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
světlu	světlo	k1gNnSc3	světlo
překonání	překonání	k1gNnSc3	překonání
této	tento	k3xDgFnSc2	tento
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
muselo	muset	k5eAaImAgNnS	muset
zabrat	zabrat	k5eAaPmF	zabrat
<g/>
.	.	kIx.	.
1006	[number]	k4	1006
–	–	k?	–
SN	SN	kA	SN
1006	[number]	k4	1006
–	–	k?	–
Nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
supernova	supernova	k1gFnSc1	supernova
s	s	k7c7	s
hvězdnou	hvězdný	k2eAgFnSc7d1	hvězdná
velikostí	velikost	k1gFnSc7	velikost
−	−	k?	−
<g/>
±	±	k?	±
<g/>
0,5	[number]	k4	0,5
mag	mag	k?	mag
<g/>
;	;	kIx,	;
záznamy	záznam	k1gInPc1	záznam
dokládají	dokládat	k5eAaImIp3nP	dokládat
pozorování	pozorování	k1gNnSc4	pozorování
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
Sýrii	Sýrie	k1gFnSc4	Sýrie
<g/>
.	.	kIx.	.
1054	[number]	k4	1054
–	–	k?	–
SN	SN	kA	SN
1054	[number]	k4	1054
–	–	k?	–
počátek	počátek	k1gInSc1	počátek
formování	formování	k1gNnSc2	formování
Krabí	krabí	k2eAgFnSc2d1	krabí
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
,	,	kIx,	,
zaznamenaný	zaznamenaný	k2eAgMnSc1d1	zaznamenaný
čínskými	čínský	k2eAgInPc7d1	čínský
astronomy	astronom	k1gMnPc4	astronom
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
původními	původní	k2eAgMnPc7d1	původní
obyvateli	obyvatel	k1gMnPc7	obyvatel
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
1181	[number]	k4	1181
–	–	k?	–
SN	SN	kA	SN
1181	[number]	k4	1181
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
čínskými	čínský	k2eAgMnPc7d1	čínský
a	a	k8xC	a
japonskými	japonský	k2eAgMnPc7d1	japonský
astronomy	astronom	k1gMnPc7	astronom
<g/>
,	,	kIx,	,
supernova	supernova	k1gFnSc1	supernova
v	v	k7c6	v
Kasiopei	Kasiope	k1gInSc6	Kasiope
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
podivná	podivný	k2eAgFnSc1d1	podivná
hvězda	hvězda	k1gFnSc1	hvězda
3C	[number]	k4	3C
58	[number]	k4	58
<g/>
.	.	kIx.	.
1572	[number]	k4	1572
–	–	k?	–
SN	SN	kA	SN
1572	[number]	k4	1572
–	–	k?	–
supernova	supernova	k1gFnSc1	supernova
v	v	k7c6	v
Kasiopei	Kasiope	k1gFnSc6	Kasiope
<g/>
,	,	kIx,	,
pozorovaná	pozorovaný	k2eAgFnSc1d1	pozorovaná
Tycho	Tycha	k1gFnSc5	Tycha
Brahem	Brah	k1gInSc7	Brah
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
kniha	kniha	k1gFnSc1	kniha
De	De	k?	De
Nova	nova	k1gFnSc1	nova
Stella	Stella	k1gFnSc1	Stella
dala	dát	k5eAaPmAgFnS	dát
podobným	podobný	k2eAgInPc3d1	podobný
objektům	objekt	k1gInPc3	objekt
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
nova	nova	k1gFnSc1	nova
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1604	[number]	k4	1604
–	–	k?	–
SN	SN	kA	SN
1604	[number]	k4	1604
–	–	k?	–
supernova	supernova	k1gFnSc1	supernova
v	v	k7c6	v
Hadonoši	hadonoš	k1gMnSc6	hadonoš
<g/>
,	,	kIx,	,
pozorovaná	pozorovaný	k2eAgFnSc1d1	pozorovaná
Johannem	Johann	k1gMnSc7	Johann
Keplerem	Kepler	k1gMnSc7	Kepler
<g/>
;	;	kIx,	;
poslední	poslední	k2eAgFnSc1d1	poslední
supernova	supernova	k1gFnSc1	supernova
pozorovaná	pozorovaný	k2eAgFnSc1d1	pozorovaná
v	v	k7c6	v
Mléčné	mléčný	k2eAgFnSc6d1	mléčná
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
1885	[number]	k4	1885
–	–	k?	–
S	s	k7c7	s
Andromeda	Andromeda	k1gMnSc1	Andromeda
v	v	k7c6	v
galaxii	galaxie	k1gFnSc6	galaxie
Andromeda	Andromed	k1gMnSc2	Andromed
<g/>
,	,	kIx,	,
objevená	objevený	k2eAgFnSc1d1	objevená
Ernstem	Ernst	k1gMnSc7	Ernst
Hartwigem	Hartwig	k1gMnSc7	Hartwig
<g/>
.	.	kIx.	.
1987	[number]	k4	1987
–	–	k?	–
SN	SN	kA	SN
1987A	[number]	k4	1987A
ve	v	k7c6	v
Velkém	velký	k2eAgNnSc6d1	velké
Magellanově	Magellanův	k2eAgNnSc6d1	Magellanovo
mračnu	mračno	k1gNnSc6	mračno
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
hodinu	hodina	k1gFnSc4	hodina
po	po	k7c6	po
hodině	hodina	k1gFnSc6	hodina
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
počátku	počátek	k1gInSc2	počátek
<g/>
,	,	kIx,	,
představovala	představovat	k5eAaImAgFnS	představovat
první	první	k4xOgFnSc1	první
možnost	možnost	k1gFnSc1	možnost
otestování	otestování	k1gNnSc2	otestování
moderních	moderní	k2eAgFnPc2d1	moderní
teorií	teorie	k1gFnPc2	teorie
formování	formování	k1gNnSc2	formování
supernov	supernova	k1gFnPc2	supernova
<g/>
.	.	kIx.	.
–	–	k?	–
Kasiopea	Kasiope	k1gInSc2	Kasiope
A	a	k9	a
–	–	k?	–
supernova	supernova	k1gFnSc1	supernova
v	v	k7c6	v
Kasiopei	Kasiope	k1gFnSc6	Kasiope
<g/>
,	,	kIx,	,
nepozorovaná	pozorovaný	k2eNgFnSc1d1	nepozorovaná
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
~	~	kIx~	~
<g/>
300	[number]	k4	300
let	léto	k1gNnPc2	léto
stará	starý	k2eAgFnSc1d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejjasnější	jasný	k2eAgInSc1d3	nejjasnější
zbytek	zbytek	k1gInSc1	zbytek
supernovy	supernova	k1gFnSc2	supernova
v	v	k7c6	v
rádiovém	rádiový	k2eAgInSc6d1	rádiový
oboru	obor	k1gInSc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Supernovu	supernova	k1gFnSc4	supernova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1604	[number]	k4	1604
použil	použít	k5eAaPmAgMnS	použít
italský	italský	k2eAgMnSc1d1	italský
učenec	učenec	k1gMnSc1	učenec
a	a	k8xC	a
astronom	astronom	k1gMnSc1	astronom
Galileo	Galilea	k1gFnSc5	Galilea
jako	jako	k8xC	jako
důkaz	důkaz	k1gInSc4	důkaz
neplatnosti	neplatnost	k1gFnSc2	neplatnost
aristotelovského	aristotelovský	k2eAgNnSc2d1	aristotelovské
dogmatu	dogma	k1gNnSc2	dogma
o	o	k7c6	o
naprosté	naprostý	k2eAgFnSc6d1	naprostá
neměnnosti	neměnnost	k1gFnSc6	neměnnost
nebes	nebesa	k1gNnPc2	nebesa
<g/>
.	.	kIx.	.
</s>
<s>
Supernovy	supernova	k1gFnPc4	supernova
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
často	často	k6eAd1	často
zanechávají	zanechávat	k5eAaImIp3nP	zanechávat
zbytky	zbytek	k1gInPc1	zbytek
<g/>
;	;	kIx,	;
studiem	studio	k1gNnSc7	studio
těchto	tento	k3xDgInPc2	tento
objektů	objekt	k1gInPc2	objekt
o	o	k7c6	o
nich	on	k3xPp3gNnPc6	on
získáváme	získávat	k5eAaImIp1nP	získávat
další	další	k2eAgFnPc4d1	další
cenné	cenný	k2eAgFnPc4d1	cenná
vědomosti	vědomost	k1gFnPc4	vědomost
<g/>
.	.	kIx.	.
</s>
<s>
Supernovy	supernova	k1gFnPc1	supernova
z	z	k7c2	z
dávných	dávný	k2eAgFnPc2d1	dávná
dob	doba	k1gFnPc2	doba
bývají	bývat	k5eAaImIp3nP	bývat
také	také	k9	také
někdy	někdy	k6eAd1	někdy
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
možnou	možný	k2eAgFnSc4d1	možná
příčinu	příčina	k1gFnSc4	příčina
velkých	velký	k2eAgNnPc2d1	velké
hromadných	hromadný	k2eAgNnPc2d1	hromadné
vymírání	vymírání	k1gNnPc2	vymírání
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vymírání	vymírání	k1gNnSc1	vymírání
K-T	K-T	k1gFnSc2	K-T
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgNnSc6	který
byli	být	k5eAaImAgMnP	být
před	před	k7c7	před
66	[number]	k4	66
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
vyhubeni	vyhuben	k2eAgMnPc1d1	vyhuben
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
žádnou	žádný	k3yNgFnSc4	žádný
takovou	takový	k3xDgFnSc4	takový
událost	událost	k1gFnSc4	událost
dostatek	dostatek	k1gInSc4	dostatek
důkazů	důkaz	k1gInPc2	důkaz
<g/>
.	.	kIx.	.
</s>
