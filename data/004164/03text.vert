<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Vrané	vraný	k2eAgFnSc2d1	Vraná
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	let	k1gInPc6	let
1930	[number]	k4	1930
–	–	k?	–
1936	[number]	k4	1936
jako	jako	k8xS	jako
první	první	k4xOgFnSc4	první
část	část	k1gFnSc4	část
vltavské	vltavský	k2eAgFnSc2d1	Vltavská
kaskády	kaskáda	k1gFnSc2	kaskáda
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
hlavním	hlavní	k2eAgInSc7d1	hlavní
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
regulace	regulace	k1gFnSc1	regulace
odtoku	odtok	k1gInSc2	odtok
vltavské	vltavský	k2eAgFnSc2d1	Vltavská
kaskády	kaskáda	k1gFnSc2	kaskáda
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
maximální	maximální	k2eAgFnSc1d1	maximální
hloubka	hloubka	k1gFnSc1	hloubka
je	být	k5eAaImIp3nS	být
10,5	[number]	k4	10,5
m.	m.	k?	m.
U	u	k7c2	u
levého	levý	k2eAgInSc2d1	levý
břehu	břeh	k1gInSc2	břeh
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jez	jez	k1gInSc1	jez
a	a	k8xC	a
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgNnPc4	čtyři
přelivná	přelivný	k2eAgNnPc4d1	přelivný
pole	pole	k1gNnPc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Hráz	hráz	k1gFnSc1	hráz
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hradící	hradící	k2eAgFnSc2d1	hradící
výšky	výška	k1gFnSc2	výška
9,7	[number]	k4	9,7
m	m	kA	m
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
71,325	[number]	k4	71,325
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
dvěma	dva	k4xCgFnPc7	dva
Kaplanovými	Kaplanův	k2eAgFnPc7d1	Kaplanova
turbínami	turbína	k1gFnPc7	turbína
o	o	k7c6	o
celkovém	celkový	k2eAgInSc6d1	celkový
instalovaném	instalovaný	k2eAgInSc6d1	instalovaný
výkonu	výkon	k1gInSc6	výkon
2	[number]	k4	2
x	x	k?	x
6,94	[number]	k4	6,94
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
provoz	provoz	k1gInSc1	provoz
je	být	k5eAaImIp3nS	být
dálkově	dálkově	k6eAd1	dálkově
řízen	řídit	k5eAaImNgInS	řídit
ze	z	k7c2	z
Štěchovic	Štěchovice	k1gFnPc2	Štěchovice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
centrální	centrální	k2eAgInSc1d1	centrální
dispečink	dispečink	k1gInSc1	dispečink
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
jsou	být	k5eAaImIp3nP	být
řízeny	řídit	k5eAaImNgFnP	řídit
všechny	všechen	k3xTgFnPc1	všechen
elektrárny	elektrárna	k1gFnPc1	elektrárna
patřící	patřící	k2eAgFnPc1d1	patřící
do	do	k7c2	do
vltavské	vltavský	k2eAgFnSc2d1	Vltavská
kaskády	kaskáda	k1gFnSc2	kaskáda
<g/>
.	.	kIx.	.
</s>
<s>
Plavební	plavební	k2eAgFnPc1d1	plavební
komory	komora	k1gFnPc1	komora
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
velkým	velký	k2eAgFnPc3d1	velká
lodím	loď	k1gFnPc3	loď
překonat	překonat	k5eAaPmF	překonat
hráz	hráz	k1gFnSc1	hráz
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
a	a	k8xC	a
prodlužují	prodlužovat	k5eAaImIp3nP	prodlužovat
vltavskou	vltavský	k2eAgFnSc4d1	Vltavská
vodní	vodní	k2eAgFnSc4d1	vodní
cestu	cesta	k1gFnSc4	cesta
až	až	k9	až
k	k	k7c3	k
hrázi	hráz	k1gFnSc3	hráz
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Štěchovice	Štěchovice	k1gFnPc1	Štěchovice
<g/>
.	.	kIx.	.
</s>
<s>
Komory	komora	k1gFnPc1	komora
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
<g/>
,	,	kIx,	,
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
u	u	k7c2	u
levého	levý	k2eAgInSc2d1	levý
břehu	břeh	k1gInSc2	břeh
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vysunuty	vysunout	k5eAaPmNgFnP	vysunout
do	do	k7c2	do
horní	horní	k2eAgFnSc2d1	horní
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
blíže	blízce	k6eAd2	blízce
ke	k	k7c3	k
středu	střed	k1gInSc3	střed
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
i	i	k8xC	i
dolním	dolní	k2eAgInSc6d1	dolní
ohlaví	ohlaví	k1gNnSc2	ohlaví
osazena	osazen	k2eAgNnPc4d1	osazeno
vzpěrná	vzpěrný	k2eAgNnPc4d1	vzpěrný
vrata	vrata	k1gNnPc4	vrata
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
užitná	užitný	k2eAgFnSc1d1	užitná
šířka	šířka	k1gFnSc1	šířka
je	být	k5eAaImIp3nS	být
12	[number]	k4	12
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
užitná	užitný	k2eAgFnSc1d1	užitná
délka	délka	k1gFnSc1	délka
85	[number]	k4	85
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
blíže	blízce	k6eAd2	blízce
ke	k	k7c3	k
břehu	břeh	k1gInSc3	břeh
<g/>
,	,	kIx,	,
v	v	k7c6	v
dolním	dolní	k2eAgNnSc6d1	dolní
ohlaví	ohlaví	k1gNnSc6	ohlaví
má	mít	k5eAaImIp3nS	mít
osazena	osazen	k2eAgNnPc4d1	osazeno
vzpěrná	vzpěrný	k2eAgNnPc4d1	vzpěrný
vrata	vrata	k1gNnPc4	vrata
a	a	k8xC	a
v	v	k7c6	v
horním	horní	k2eAgNnSc6d1	horní
ohlaví	ohlaví	k1gNnSc6	ohlaví
tabulový	tabulový	k2eAgInSc1d1	tabulový
uzávěr	uzávěr	k1gInSc1	uzávěr
podobný	podobný	k2eAgInSc1d1	podobný
jako	jako	k9	jako
v	v	k7c6	v
jezovém	jezový	k2eAgNnSc6d1	jezové
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
převádění	převádění	k1gNnSc4	převádění
velkých	velký	k2eAgFnPc2d1	velká
vod	voda	k1gFnPc2	voda
plavební	plavební	k2eAgFnSc7d1	plavební
komorou	komora	k1gFnSc7	komora
<g/>
.	.	kIx.	.
</s>
<s>
Užitná	užitný	k2eAgFnSc1d1	užitná
šířka	šířka	k1gFnSc1	šířka
plavební	plavební	k2eAgFnSc2d1	plavební
komory	komora	k1gFnSc2	komora
je	být	k5eAaImIp3nS	být
12	[number]	k4	12
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
celková	celkový	k2eAgFnSc1d1	celková
užitná	užitný	k2eAgFnSc1d1	užitná
délka	délka	k1gFnSc1	délka
134	[number]	k4	134
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
středními	střední	k2eAgInPc7d1	střední
vzpěrnými	vzpěrný	k2eAgInPc7d1	vzpěrný
vraty	vrat	k1gInPc7	vrat
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
menší	malý	k2eAgFnPc4d2	menší
plavební	plavební	k2eAgFnPc4d1	plavební
komory	komora	k1gFnPc4	komora
délek	délka	k1gFnPc2	délka
(	(	kIx(	(
<g/>
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
<g/>
)	)	kIx)	)
43	[number]	k4	43
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
85	[number]	k4	85
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Vrané	vraný	k2eAgNnSc1d1	Vrané
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Povodí	povodí	k1gNnSc2	povodí
Vltavy	Vltava	k1gFnSc2	Vltava
–	–	k?	–
VD	VD	kA	VD
Vrané	vraný	k2eAgNnSc1d1	Vrané
VD	VD	kA	VD
Vrané	vraný	k2eAgNnSc1d1	Vrané
–	–	k?	–
aktuální	aktuální	k2eAgInSc4d1	aktuální
vodní	vodní	k2eAgInSc4d1	vodní
stav	stav	k1gInSc4	stav
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Povodí	povodí	k1gNnSc2	povodí
Vltavy	Vltava	k1gFnSc2	Vltava
</s>
