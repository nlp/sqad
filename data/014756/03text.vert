<s>
Let	let	k1gInSc1
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
610	#num#	k4
</s>
<s>
Let	let	k1gInSc1
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
610	#num#	k4
Zřícené	zřícený	k2eAgInPc1d1
letadlo	letadlo	k1gNnSc4
v	v	k7c6
září	září	k1gNnSc6
2018	#num#	k4
<g/>
Nehoda	nehoda	k1gFnSc1
Datum	datum	k1gNnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc4
2018	#num#	k4
Čas	čas	k1gInSc1
</s>
<s>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Fáze	fáze	k1gFnSc1
letu	let	k1gInSc2
</s>
<s>
ENR	ENR	kA
Hlavní	hlavní	k2eAgFnSc1d1
příčina	příčina	k1gFnSc1
</s>
<s>
Chybný	chybný	k2eAgInSc1d1
software	software	k1gInSc1
na	na	k7c6
letadle	letadlo	k1gNnSc6
typu	typ	k1gInSc2
Boeing	boeing	k1gInSc1
737	#num#	k4
max	max	kA
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tzn.	tzn.	kA
Chyba	Chyba	k1gMnSc1
výrobce	výrobce	k1gMnSc1
Místo	místo	k7c2
</s>
<s>
Jávské	jávský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
5	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
107	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
16	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Let	léto	k1gNnPc2
</s>
<s>
JT610	JT610	k4
Počátek	počátek	k1gInSc1
letu	let	k1gInSc2
</s>
<s>
letiště	letiště	k1gNnSc4
Sukarno-Hatta	Sukarno-Hatt	k1gInSc2
Cíl	cíl	k1gInSc4
letu	let	k1gInSc3
</s>
<s>
letiště	letiště	k1gNnPc1
Depati	Depat	k1gMnPc1
Amir	Amir	k1gMnSc1
Letoun	letoun	k1gMnSc1
Model	model	k1gInSc4
</s>
<s>
Boeing	boeing	k1gInSc1
737	#num#	k4
MAX	max	kA
8	#num#	k4
Dopravce	dopravce	k1gMnSc4
</s>
<s>
Lion	Lion	k1gMnSc1
Air	Air	k1gFnSc2
Registrace	registrace	k1gFnSc1
</s>
<s>
PK-LQP	PK-LQP	k?
Stáří	stáří	k1gNnSc1
</s>
<s>
2	#num#	k4
měsíce	měsíc	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Následky	následek	k1gInPc1
Na	na	k7c6
palubě	paluba	k1gFnSc6
osob	osoba	k1gFnPc2
</s>
<s>
189	#num#	k4
Cestující	cestující	k1gFnSc1
</s>
<s>
181	#num#	k4
Posádka	posádka	k1gFnSc1
</s>
<s>
8	#num#	k4
Mrtvých	mrtvý	k2eAgMnPc2d1
</s>
<s>
189	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Letoun	letoun	k1gInSc1
</s>
<s>
zničen	zničen	k2eAgInSc1d1
</s>
<s>
Let	let	k1gInSc1
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
610	#num#	k4
byl	být	k5eAaImAgInS
pravidelný	pravidelný	k2eAgInSc1d1
let	let	k1gInSc1
indonéské	indonéský	k2eAgFnSc2d1
nízkonákladové	nízkonákladový	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
z	z	k7c2
Jakarty	Jakarta	k1gFnSc2
do	do	k7c2
Pangkalpinangu	Pangkalpinang	k1gInSc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Bangka	Bangko	k1gNnSc2
u	u	k7c2
Sumatry	Sumatra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gMnSc1
Boeing	boeing	k1gInSc4
737	#num#	k4
MAX	max	kA
8	#num#	k4
se	se	k3xPyFc4
zřítil	zřítit	k5eAaPmAgInS
do	do	k7c2
moře	moře	k1gNnSc2
29	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
v	v	k7c4
6	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
místního	místní	k2eAgInSc2d1
času	čas	k1gInSc2
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
v	v	k7c4
0	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
středoevropského	středoevropský	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
)	)	kIx)
se	s	k7c7
189	#num#	k4
lidmi	člověk	k1gMnPc7
na	na	k7c6
palubě	paluba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třináct	třináct	k4xCc1
minut	minuta	k1gFnPc2
po	po	k7c6
startu	start	k1gInSc6
požádal	požádat	k5eAaPmAgMnS
pilot	pilot	k1gMnSc1
o	o	k7c4
návrat	návrat	k1gInSc4
<g/>
,	,	kIx,
poté	poté	k6eAd1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
ztrátě	ztráta	k1gFnSc3
kontaktu	kontakt	k1gInSc2
a	a	k8xC
letadlo	letadlo	k1gNnSc1
se	se	k3xPyFc4
zřítilo	zřítit	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
informací	informace	k1gFnPc2
z	z	k7c2
Flightradar	Flightradara	k1gFnPc2
<g/>
24	#num#	k4
se	se	k3xPyFc4
letadlo	letadlo	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
otáčet	otáčet	k5eAaImF
a	a	k8xC
na	na	k7c4
hladinu	hladina	k1gFnSc4
dopadlo	dopadnout	k5eAaPmAgNnS
ve	v	k7c6
vysoké	vysoký	k2eAgFnSc6d1
rychlosti	rychlost	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
způsobilo	způsobit	k5eAaPmAgNnS
jeho	jeho	k3xOp3gFnSc4
roztrhání	roztrhání	k1gNnSc1
na	na	k7c4
kusy	kus	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
první	první	k4xOgInSc4
ze	z	k7c2
dvou	dva	k4xCgInPc2
nehod	nehoda	k1gFnPc2
typu	typ	k1gInSc2
Boeing	boeing	k1gInSc4
737	#num#	k4
MAX	Max	k1gMnSc1
od	od	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
provozu	provoz	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pátrání	pátrání	k1gNnSc1
</s>
<s>
Do	do	k7c2
pátrací	pátrací	k2eAgFnSc2d1
operace	operace	k1gFnSc2
byla	být	k5eAaImAgFnS
zapojena	zapojen	k2eAgFnSc1d1
indonéská	indonéský	k2eAgFnSc1d1
pátrací	pátrací	k2eAgFnSc1d1
a	a	k8xC
záchranná	záchranný	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
(	(	kIx(
<g/>
Badan	Badan	k1gMnSc1
Nasional	Nasional	k1gMnSc1
Pencarian	Pencarian	k1gMnSc1
dan	dan	k?
Pertolongan	Pertolongan	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
indonéské	indonéský	k2eAgNnSc1d1
vojenské	vojenský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
a	a	k8xC
indonéské	indonéský	k2eAgNnSc1d1
a	a	k8xC
singapurské	singapurský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
akce	akce	k1gFnSc2
byli	být	k5eAaImAgMnP
nasazeni	nasadit	k5eAaPmNgMnP
potápěči	potápěč	k1gMnPc1
<g/>
,	,	kIx,
sonar	sonar	k1gInSc1
a	a	k8xC
podvodní	podvodní	k2eAgInSc1d1
dron	dron	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Ke	k	k7c3
30	#num#	k4
<g/>
.	.	kIx.
říjnu	říjen	k1gInSc6
byly	být	k5eAaImAgInP
posbírány	posbírán	k2eAgInPc1d1
všechny	všechen	k3xTgInPc1
objekty	objekt	k1gInPc1
plovoucí	plovoucí	k2eAgInPc1d1
na	na	k7c6
hladině	hladina	k1gFnSc6
poté	poté	k6eAd1
se	se	k3xPyFc4
také	také	k9
po	po	k7c6
rozšíření	rozšíření	k1gNnSc6
zóny	zóna	k1gFnSc2
pátrání	pátrání	k1gNnSc2
z	z	k7c2
25	#num#	k4
na	na	k7c4
50	#num#	k4
námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
objevila	objevit	k5eAaPmAgFnS
informace	informace	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
nalezen	nalezen	k2eAgMnSc1d1
pod	pod	k7c7
vodou	voda	k1gFnSc7
velký	velký	k2eAgInSc1d1
objekt	objekt	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
trupem	trup	k1gInSc7
letadla	letadlo	k1gNnSc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
zároveň	zároveň	k6eAd1
byl	být	k5eAaImAgInS
zachycen	zachycen	k2eAgInSc1d1
signál	signál	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
může	moct	k5eAaImIp3nS
pocházet	pocházet	k5eAaImF
z	z	k7c2
černých	černý	k2eAgFnPc2d1
skříněk	skříňka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
také	také	k9
potvrdilo	potvrdit	k5eAaPmAgNnS
a	a	k8xC
ve	v	k7c4
čtvrtek	čtvrtek	k1gInSc4
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
přišla	přijít	k5eAaPmAgFnS
zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgMnS
vyzdvižen	vyzdvižen	k2eAgInSc4d1
zapisovač	zapisovač	k1gInSc4
letových	letový	k2eAgInPc2d1
údajů	údaj	k1gInPc2
(	(	kIx(
<g/>
flight	flight	k1gMnSc1
data	datum	k1gNnSc2
recorder	recorder	k1gMnSc1
–	–	k?
FDR	FDR	kA
<g/>
)	)	kIx)
zaznamenávající	zaznamenávající	k2eAgFnPc1d1
informace	informace	k1gFnPc1
o	o	k7c6
letu	let	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zapisovač	zapisovač	k1gInSc1
hlasu	hlas	k1gInSc2
v	v	k7c6
pilotní	pilotní	k2eAgFnSc6d1
kabině	kabina	k1gFnSc6
(	(	kIx(
<g/>
cockpit	cockpit	k5eAaPmF
voice	voiko	k6eAd1
recorder	recorder	k1gInSc1
–	–	k?
CVR	CVR	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
nalezen	naleznout	k5eAaPmNgInS,k5eAaBmNgInS
až	až	k9
14.1	14.1	k4
<g/>
.2019	.2019	k4
<g/>
,	,	kIx,
tedy	tedy	k9
více	hodně	k6eAd2
než	než	k8xS
2	#num#	k4
měsíce	měsíc	k1gInSc2
po	po	k7c6
nehodě	nehoda	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
přibližně	přibližně	k6eAd1
v	v	k7c6
místě	místo	k1gNnSc6
dopadu	dopad	k1gInSc2
letadla	letadlo	k1gNnSc2
30	#num#	k4
m	m	kA
pod	pod	k7c7
hladinou	hladina	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
pod	pod	k7c4
8	#num#	k4
m	m	kA
vrstvou	vrstva	k1gFnSc7
bahna	bahno	k1gNnSc2
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
zveřejnění	zveřejnění	k1gNnSc2
předběžné	předběžný	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
nebyla	být	k5eNaImAgFnS
ještě	ještě	k9
CVR	CVR	kA
nalezena	nalézt	k5eAaBmNgNnP,k5eAaPmNgNnP
<g/>
,	,	kIx,
takže	takže	k8xS
údaje	údaj	k1gInPc1
z	z	k7c2
ní	on	k3xPp3gFnSc2
do	do	k7c2
zprávy	zpráva	k1gFnSc2
nemohly	moct	k5eNaImAgInP
být	být	k5eAaImF
zahrnuty	zahrnout	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s>
Množství	množství	k1gNnSc1
a	a	k8xC
poloha	poloha	k1gFnSc1
trosek	troska	k1gFnPc2
ukazuje	ukazovat	k5eAaImIp3nS
na	na	k7c4
náraz	náraz	k1gInSc4
o	o	k7c6
velké	velký	k2eAgFnSc6d1
energii	energie	k1gFnSc6
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
pasažéry	pasažér	k1gMnPc7
bylo	být	k5eAaImAgNnS
mnoho	mnoho	k4c1
vládních	vládní	k2eAgMnPc2d1
úředníků	úředník	k1gMnPc2
i	i	k8xC
dva	dva	k4xCgMnPc1
cizinci	cizinec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Okolnosti	okolnost	k1gFnPc1
nehody	nehoda	k1gFnSc2
</s>
<s>
Dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2018	#num#	k4
byla	být	k5eAaImAgFnS
zveřejněna	zveřejnit	k5eAaPmNgFnS
předběžná	předběžný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
z	z	k7c2
vyšetřování	vyšetřování	k1gNnSc2
letecké	letecký	k2eAgFnSc2d1
nehody	nehoda	k1gFnSc2
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
z	z	k7c2
které	který	k3yQgFnSc2,k3yIgFnSc2,k3yRgFnSc2
vyplývá	vyplývat	k5eAaImIp3nS
následující	následující	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
den	den	k1gInSc4
před	před	k7c7
nehodou	nehoda	k1gFnSc7
<g/>
,	,	kIx,
letělo	letět	k5eAaImAgNnS
předmětné	předmětný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
na	na	k7c6
pravidelném	pravidelný	k2eAgInSc6d1
letu	let	k1gInSc6
z	z	k7c2
Denpasar	Denpasara	k1gFnPc2
do	do	k7c2
Jakarta	Jakarta	k1gFnSc1
jako	jako	k8xC,k8xS
let	let	k1gInSc1
JT	JT	kA
<g/>
43	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
předletové	předletový	k2eAgFnSc2d1
kontroly	kontrola	k1gFnSc2
diskutoval	diskutovat	k5eAaImAgMnS
kapitán	kapitán	k1gMnSc1
JT43	JT43	k1gMnSc1
s	s	k7c7
technikem	technik	k1gMnSc7
údřžbu	údřžb	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
byla	být	k5eAaImAgFnS
před	před	k7c7
letem	let	k1gInSc7
na	na	k7c6
letadle	letadlo	k1gNnSc6
provedená	provedený	k2eAgNnPc1d1
<g/>
,	,	kIx,
včetně	včetně	k7c2
výměny	výměna	k1gFnSc2
indikátoru	indikátor	k1gInSc2
úhlu	úhel	k1gInSc2
náběhu	náběh	k1gInSc2
(	(	kIx(
<g/>
AoA	AoA	k1gFnSc1
–	–	k?
Angle	Angl	k1gMnSc5
of	of	k?
Attack	Attacko	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
bylo	být	k5eAaImAgNnS
mu	on	k3xPp3gMnSc3
sděleno	sdělen	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
výměně	výměna	k1gFnSc6
proběhla	proběhnout	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
úspěšná	úspěšný	k2eAgFnSc1d1
zkouška	zkouška	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
vzletu	vzlet	k1gInSc2
JT43	JT43	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
po	po	k7c6
zvednutí	zvednutí	k1gNnSc6
příďového	příďový	k2eAgInSc2d1
podvozku	podvozek	k1gInSc2
k	k	k7c3
aktivaci	aktivace	k1gFnSc3
tzv.	tzv.	kA
stick	stick	k1gInSc4
shakeru	shaker	k1gInSc2
(	(	kIx(
<g/>
zařízení	zařízení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
upozorňuje	upozorňovat	k5eAaImIp3nS
pilota	pilota	k1gFnSc1
na	na	k7c4
příliš	příliš	k6eAd1
vysoký	vysoký	k2eAgInSc4d1
úhel	úhel	k1gInSc4
náběhu	náběh	k1gInSc2
a	a	k8xC
blížící	blížící	k2eAgMnSc1d1
se	se	k3xPyFc4
pád	pád	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
zůstal	zůstat	k5eAaPmAgMnS
aktivní	aktivní	k2eAgInPc4d1
po	po	k7c4
celý	celý	k2eAgInSc4d1
zbytek	zbytek	k1gInSc4
letu	let	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
výšce	výška	k1gFnSc6
zhruba	zhruba	k6eAd1
400	#num#	k4
stop	stopa	k1gFnPc2
nad	nad	k7c7
terénem	terén	k1gInSc7
se	se	k3xPyFc4
na	na	k7c6
primárním	primární	k2eAgInSc6d1
letovém	letový	k2eAgInSc6d1
displayi	display	k1gInSc6
objevilo	objevit	k5eAaPmAgNnS
varování	varování	k1gNnSc1
IAS	IAS	kA
DISAGREE	DISAGREE	kA
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
údaj	údaj	k1gInSc1
o	o	k7c6
indikované	indikovaný	k2eAgFnSc6d1
vzdušné	vzdušný	k2eAgFnSc6d1
rychlosti	rychlost	k1gFnSc6
(	(	kIx(
<g/>
IAS	IAS	kA
<g/>
)	)	kIx)
z	z	k7c2
jedné	jeden	k4xCgFnSc2
strany	strana	k1gFnSc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
od	od	k7c2
údaje	údaj	k1gInSc2
z	z	k7c2
druhé	druhý	k4xOgFnSc2
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
čidla	čidlo	k1gNnPc1
jsou	být	k5eAaImIp3nP
zdvojená	zdvojený	k2eAgNnPc1d1
<g/>
)	)	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
tudíž	tudíž	k8xC
nespolehlivý	spolehlivý	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
předal	předat	k5eAaPmAgMnS
kapitán	kapitán	k1gMnSc1
řízení	řízení	k1gNnSc2
prvnímu	první	k4xOgNnSc3
důstojníkovi	důstojník	k1gMnSc3
a	a	k8xC
zkontroloval	zkontrolovat	k5eAaPmAgInS
údaje	údaj	k1gInPc4
o	o	k7c4
indikované	indikovaný	k2eAgFnPc4d1
vzdušné	vzdušný	k2eAgFnPc4d1
rychlosti	rychlost	k1gFnPc4
z	z	k7c2
levé	levý	k2eAgFnSc2d1
a	a	k8xC
pravé	pravý	k2eAgFnSc2d1
strany	strana	k1gFnSc2
a	a	k8xC
porovnal	porovnat	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
se	se	k3xPyFc4
záložním	záložní	k2eAgInSc7d1
indikátorem	indikátor	k1gInSc7
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
údaj	údaj	k1gInSc1
na	na	k7c6
levém	levý	k2eAgInSc6d1
primárním	primární	k2eAgInSc6d1
letovém	letový	k2eAgInSc6d1
displeji	displej	k1gInSc6
je	být	k5eAaImIp3nS
nesprávný	správný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tu	ten	k3xDgFnSc4
chvíli	chvíle	k1gFnSc4
si	se	k3xPyFc3
kapitán	kapitán	k1gMnSc1
také	také	k6eAd1
všiml	všimnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
letadlo	letadlo	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
automaticky	automaticky	k6eAd1
vyvažovat	vyvažovat	k5eAaImF
ve	v	k7c6
smyslu	smysl	k1gInSc6
těžký	těžký	k2eAgMnSc1d1
na	na	k7c4
hlavu	hlava	k1gFnSc4
(	(	kIx(
<g/>
AND	Anda	k1gFnPc2
–	–	k?
Aircraft	Aircraft	k1gMnSc1
Nose	nos	k1gInSc6
Down	Down	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
načež	načež	k6eAd1
přepnul	přepnout	k5eAaPmAgInS
přepínače	přepínač	k1gInPc4
elektrického	elektrický	k2eAgInSc2d1
pohonu	pohon	k1gInSc2
stabilizátoru	stabilizátor	k1gInSc2
(	(	kIx(
<g/>
STAB	STAB	kA
TRIM	TRIM	kA
<g/>
)	)	kIx)
do	do	k7c2
polohy	poloha	k1gFnSc2
vypnuto	vypnut	k2eAgNnSc1d1
(	(	kIx(
<g/>
CUT	CUT	kA
OUT	OUT	kA
<g/>
)	)	kIx)
a	a	k8xC
první	první	k4xOgMnSc1
důstojník	důstojník	k1gMnSc1
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
letu	let	k1gInSc6
za	za	k7c4
použití	použití	k1gNnSc4
pouze	pouze	k6eAd1
ručního	ruční	k2eAgNnSc2d1
vyvažování	vyvažování	k1gNnSc2
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
přistání	přistání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinak	jinak	k6eAd1
proběhl	proběhnout	k5eAaPmAgMnS
let	let	k1gInSc4
bez	bez	k7c2
dalších	další	k2eAgFnPc2d1
mimořádností	mimořádnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
přistání	přistání	k1gNnSc6
v	v	k7c6
Jakartě	Jakarta	k1gFnSc6
oznámil	oznámit	k5eAaPmAgMnS
kapitán	kapitán	k1gMnSc1
inženýrovi	inženýr	k1gMnSc3
údržby	údržba	k1gFnSc2
výskyt	výskyt	k1gInSc4
varování	varování	k1gNnSc1
o	o	k7c6
nesprávné	správný	k2eNgFnSc6d1
vzdušné	vzdušný	k2eAgFnSc6d1
rychlosti	rychlost	k1gFnSc6
(	(	kIx(
<g/>
IAS	IAS	kA
DISAGREE	DISAGREE	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nesprávné	správný	k2eNgFnSc3d1
nadmořské	nadmořský	k2eAgFnSc3d1
výšce	výška	k1gFnSc3
(	(	kIx(
<g/>
ALT	alt	k1gInSc1
DISAGREE	DISAGREE	kA
<g/>
)	)	kIx)
a	a	k8xC
rozdílu	rozdíl	k1gInSc2
tlaku	tlak	k1gInSc2
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
hydraulickými	hydraulický	k2eAgInPc7d1
okruhy	okruh	k1gInPc7
zpětné	zpětný	k2eAgFnSc2d1
vazby	vazba	k1gFnSc2
do	do	k7c2
řízení	řízení	k1gNnSc2
(	(	kIx(
<g/>
FEEL	FEEL	kA
DIFF	DIFF	kA
PRESS	PRESS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technici	technik	k1gMnPc1
provedli	provést	k5eAaPmAgMnP
úržbu	úržba	k1gFnSc4
dotyčných	dotyčný	k2eAgInPc2d1
systémů	systém	k1gInPc2
a	a	k8xC
po	po	k7c6
jejich	jejich	k3xOp3gInPc6
úspěšných	úspěšný	k2eAgInPc6d1
testech	test	k1gInPc6
na	na	k7c6
zemi	zem	k1gFnSc6
konstatovali	konstatovat	k5eAaBmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
problémy	problém	k1gInPc1
byly	být	k5eAaImAgInP
vyřešeny	vyřešit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s>
Druhý	druhý	k4xOgInSc4
den	den	k1gInSc4
ráno	ráno	k6eAd1
<g/>
,	,	kIx,
29.10	29.10	k4
<g/>
.2018	.2018	k4
<g/>
,	,	kIx,
odletělo	odletět	k5eAaPmAgNnS
letadlo	letadlo	k1gNnSc1
z	z	k7c2
Jakarty	Jakarta	k1gFnSc2
s	s	k7c7
cílem	cíl	k1gInSc7
v	v	k7c6
Pangkal	Pangkal	k1gMnSc1
Pinang	Pinang	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zapisovač	zapisovač	k1gInSc1
letových	letový	k2eAgNnPc2d1
dat	datum	k1gNnPc2
(	(	kIx(
<g/>
část	část	k1gFnSc1
tzv.	tzv.	kA
černé	černý	k2eAgFnSc2d1
skříňky	skříňka	k1gFnSc2
<g/>
)	)	kIx)
zaznamenal	zaznamenat	k5eAaPmAgMnS
rozdíl	rozdíl	k1gInSc4
mezi	mezi	k7c7
údaji	údaj	k1gInPc7
z	z	k7c2
pravého	pravý	k2eAgInSc2d1
a	a	k8xC
levého	levý	k2eAgInSc2d1
indikátoru	indikátor	k1gInSc2
úhlu	úhel	k1gInSc2
náběhu	náběh	k1gInSc2
(	(	kIx(
<g/>
Angle	Angl	k1gMnSc5
of	of	k?
Attack	Attack	k1gMnSc1
–	–	k?
AoA	AoA	k1gMnSc1
<g/>
)	)	kIx)
o	o	k7c6
zhruba	zhruba	k6eAd1
20	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
trval	trvat	k5eAaImAgInS
až	až	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
záznamu	záznam	k1gInSc2
(	(	kIx(
<g/>
tedy	tedy	k9
do	do	k7c2
okamžiku	okamžik	k1gInSc2
havárie	havárie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
zvednutí	zvednutí	k1gNnSc2
příďového	příďový	k2eAgInSc2d1
podvozku	podvozek	k1gInSc2
při	při	k7c6
vzletu	vzlet	k1gInSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
aktivaci	aktivace	k1gFnSc3
stick	stick	k6eAd1
shakeru	shakrat	k5eAaPmIp1nS
na	na	k7c6
levé	levý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stick	Stick	k1gMnSc1
shaker	shaker	k1gMnSc1
zůstal	zůstat	k5eAaPmAgMnS
aktivní	aktivní	k2eAgInPc4d1
po	po	k7c4
většinu	většina	k1gFnSc4
letu	let	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgMnSc1
důstojník	důstojník	k1gMnSc1
během	během	k7c2
letu	let	k1gInSc2
žádal	žádat	k5eAaImAgMnS
řídícího	řídící	k2eAgInSc2d1
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
o	o	k7c4
údaje	údaj	k1gInPc4
o	o	k7c6
výšce	výška	k1gFnSc6
a	a	k8xC
rychlosti	rychlost	k1gFnSc6
a	a	k8xC
rovněž	rovněž	k9
mu	on	k3xPp3gMnSc3
hlásil	hlásit	k5eAaImAgMnS
problém	problém	k1gInSc4
s	s	k7c7
řízením	řízení	k1gNnSc7
letadla	letadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
zatažení	zatažení	k1gNnSc6
vztakových	vztakový	k2eAgFnPc2d1
klapek	klapka	k1gFnPc2
po	po	k7c6
vzletu	vzlet	k1gInSc6
zaznamenal	zaznamenat	k5eAaPmAgInS
zapisovač	zapisovač	k1gInSc1
letových	letový	k2eAgNnPc2d1
dat	datum	k1gNnPc2
aktivaci	aktivace	k1gFnSc6
automatického	automatický	k2eAgNnSc2d1
vyvažování	vyvažování	k1gNnSc2
ve	v	k7c6
smyslu	smysl	k1gInSc6
těžký	těžký	k2eAgMnSc1d1
na	na	k7c4
hlavu	hlava	k1gFnSc4
(	(	kIx(
<g/>
AND	Anda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c4
což	což	k3yQnSc4,k3yRnSc4
posádka	posádka	k1gFnSc1
reagovala	reagovat	k5eAaBmAgFnS
vyvažováním	vyvažování	k1gNnSc7
v	v	k7c6
opačném	opačný	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
<g/>
,	,	kIx,
tedy	tedy	k8xC
těžký	těžký	k2eAgInSc1d1
na	na	k7c4
ocas	ocas	k1gInSc4
(	(	kIx(
<g/>
ANU	ANU	kA
<g/>
)	)	kIx)
a	a	k8xC
opětovným	opětovný	k2eAgNnSc7d1
vytažením	vytažení	k1gNnSc7
vztlakových	vztlakový	k2eAgFnPc2d1
klapek	klapka	k1gFnPc2
do	do	k7c2
polohy	poloha	k1gFnSc2
pro	pro	k7c4
vzlet	vzlet	k1gInSc4
<g/>
,	,	kIx,
po	po	k7c6
čemž	což	k3yRnSc6,k3yQnSc6
automatické	automatický	k2eAgNnSc1d1
vyvažování	vyvažování	k1gNnSc1
AND	Anda	k1gFnPc2
ustalo	ustat	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
posádka	posádka	k1gFnSc1
vztlakové	vztlakový	k2eAgFnSc2d1
klapky	klapka	k1gFnSc2
definitivně	definitivně	k6eAd1
zatáhla	zatáhnout	k5eAaPmAgFnS
načež	načež	k6eAd1
začalo	začít	k5eAaPmAgNnS
opětovné	opětovný	k2eAgNnSc1d1
přetahování	přetahování	k1gNnSc1
se	se	k3xPyFc4
mezi	mezi	k7c7
automatickým	automatický	k2eAgInSc7d1
vyvažování	vyvažování	k1gNnSc1
(	(	kIx(
<g/>
AND	Anda	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
vyvažováním	vyvažování	k1gNnSc7
posádky	posádka	k1gFnSc2
(	(	kIx(
<g/>
ANU	ANU	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
trvalo	trvat	k5eAaImAgNnS
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
záznamu	záznam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Piloti	pilot	k1gMnPc1
</s>
<s>
Kapitán	kapitán	k1gMnSc1
byl	být	k5eAaImAgMnS
indické	indický	k2eAgFnPc4d1
národnosti	národnost	k1gFnPc4
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
celkový	celkový	k2eAgInSc1d1
nálet	nálet	k1gInSc1
byl	být	k5eAaImAgInS
6028	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
5176	#num#	k4
hodin	hodina	k1gFnPc2
na	na	k7c4
typu	typa	k1gFnSc4
Boeing	boeing	k1gInSc1
737	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgMnSc1
důstojník	důstojník	k1gMnSc1
byl	být	k5eAaImAgMnS
indonéské	indonéský	k2eAgFnPc4d1
národnosti	národnost	k1gFnPc4
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
celkový	celkový	k2eAgInSc1d1
nálet	nálet	k1gInSc1
byl	být	k5eAaImAgInS
5174	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
4286	#num#	k4
hodin	hodina	k1gFnPc2
na	na	k7c4
typu	typa	k1gFnSc4
Boeing	boeing	k1gInSc1
737	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Letadlo	letadlo	k1gNnSc1
</s>
<s>
Boeing	boeing	k1gInSc1
737	#num#	k4
MAX	max	kA
8	#num#	k4
registrační	registrační	k2eAgFnSc2d1
značky	značka	k1gFnSc2
PK-LQP	PK-LQP	k1gFnSc2
<g/>
,	,	kIx,
rok	rok	k1gInSc4
výroby	výroba	k1gFnSc2
2018	#num#	k4
<g/>
,	,	kIx,
celkový	celkový	k2eAgInSc1d1
nálet	nálet	k1gInSc1
895	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
/	/	kIx~
<g/>
443	#num#	k4
cyklů	cyklus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Počasí	počasí	k1gNnSc1
</s>
<s>
V	v	k7c6
místě	místo	k1gNnSc6
odletu	odlet	k1gInSc2
panovalo	panovat	k5eAaImAgNnS
od	od	k7c2
času	čas	k1gInSc2
vzletu	vzlet	k1gInSc2
až	až	k6eAd1
do	do	k7c2
času	čas	k1gInSc2
nehody	nehoda	k1gFnSc2
dobré	dobrý	k2eAgFnSc2d1
počasí	počasí	k1gNnSc4
pro	pro	k7c4
bezpečné	bezpečný	k2eAgNnSc4d1
provedení	provedení	k1gNnSc4
letu	let	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teplota	teplota	k1gFnSc1
se	se	k3xPyFc4
pohybovala	pohybovat	k5eAaImAgFnS
mezi	mezi	k7c7
26	#num#	k4
a	a	k8xC
27	#num#	k4
stupni	stupeň	k1gInPc7
Celsia	Celsius	k1gMnSc2
<g/>
,	,	kIx,
vál	vát	k5eAaImAgInS
slabý	slabý	k2eAgInSc1d1
vítr	vítr	k1gInSc1
a	a	k8xC
bylo	být	k5eAaImAgNnS
oblačno	oblačno	k1gNnSc1
se	s	k7c7
základnou	základna	k1gFnSc7
mraků	mrak	k1gInPc2
kolem	kolem	k7c2
2000	#num#	k4
stop	stopa	k1gFnPc2
nad	nad	k7c7
letištěm	letiště	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moře	moře	k1gNnSc1
mělo	mít	k5eAaImAgNnS
v	v	k7c6
místě	místo	k1gNnSc6
a	a	k8xC
čase	čas	k1gInSc6
dopadu	dopad	k1gInSc2
teplotu	teplota	k1gFnSc4
0	#num#	k4
–	–	k?
8	#num#	k4
<g/>
°	°	k?
<g/>
C.	C.	kA
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Palubní	palubní	k2eAgInSc1d1
deník	deník	k1gInSc1
</s>
<s>
Od	od	k7c2
26	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
bylo	být	k5eAaImAgNnS
do	do	k7c2
palubního	palubní	k2eAgInSc2d1
deníku	deník	k1gInSc2
letadla	letadlo	k1gNnSc2
zapsáno	zapsat	k5eAaPmNgNnS
několik	několik	k4yIc1
záznamů	záznam	k1gInPc2
o	o	k7c6
chybách	chyba	k1gFnPc6
různých	různý	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
výpadky	výpadek	k1gInPc4
indikace	indikace	k1gFnSc2
vzdušné	vzdušný	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
a	a	k8xC
výšky	výška	k1gFnSc2
na	na	k7c6
levém	levý	k2eAgInSc6d1
primárnám	primárna	k1gFnPc3
letovém	letový	k2eAgNnSc6d1
displayi	displayi	k1gNnSc1
<g/>
,	,	kIx,
indikace	indikace	k1gFnSc1
poruchy	porucha	k1gFnSc2
automatických	automatický	k2eAgInPc2d1
vyvažovacích	vyvažovací	k2eAgInPc2d1
systémů	systém	k1gInPc2
(	(	kIx(
<g/>
Speed	Speed	k1gMnSc1
trim	trim	k1gMnSc1
a	a	k8xC
Mach	Mach	k1gMnSc1
trim	trim	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
odpojení	odpojení	k1gNnSc1
systému	systém	k1gInSc2
automatického	automatický	k2eAgNnSc2d1
ovládání	ovládání	k1gNnSc2
tahu	tah	k1gInSc2
motorů	motor	k1gInPc2
(	(	kIx(
<g/>
auto-throttle	auto-throttle	k6eAd1
arm	arm	k?
disconnect	disconnect	k1gInSc1
<g/>
)	)	kIx)
během	během	k7c2
vzletu	vzlet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
každém	každý	k3xTgInSc6
letu	let	k1gInSc6
byly	být	k5eAaImAgInP
systémy	systém	k1gInPc1
prověřeny	prověřit	k5eAaPmNgInP
a	a	k8xC
byly	být	k5eAaImAgInP
provedeny	provést	k5eAaPmNgInP
předepsané	předepsaný	k2eAgInPc1d1
úkony	úkon	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
jelikož	jelikož	k8xS
se	se	k3xPyFc4
problémy	problém	k1gInPc1
stále	stále	k6eAd1
opakovaly	opakovat	k5eAaImAgInP
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
preventivně	preventivně	k6eAd1
vyměněn	vyměnit	k5eAaPmNgInS
indikátor	indikátor	k1gInSc1
úhlu	úhel	k1gInSc2
náběhu	náběh	k1gInSc2
(	(	kIx(
<g/>
vyměněný	vyměněný	k2eAgInSc1d1
kus	kus	k1gInSc1
byl	být	k5eAaImAgInS
po	po	k7c6
nehodě	nehoda	k1gFnSc6
zajištěn	zajištěn	k2eAgInSc4d1
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
předmětem	předmět	k1gInSc7
zkoumání	zkoumání	k1gNnSc2
vyšetřovatelů	vyšetřovatel	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
28.10	28.10	k4
<g/>
.	.	kIx.
se	se	k3xPyFc4
problémy	problém	k1gInPc1
vyskytly	vyskytnout	k5eAaPmAgInP
znovu	znovu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
let	let	k1gInSc4
JT43	JT43	k1gFnSc2
den	den	k1gInSc4
před	před	k7c7
nehodou	nehoda	k1gFnSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnPc1
potíže	potíž	k1gFnPc1
byly	být	k5eAaImAgFnP
popsány	popsat	k5eAaPmNgFnP
výše	vysoce	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
letu	let	k1gInSc6
JT43	JT43	k1gFnSc2
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
byly	být	k5eAaImAgInP
propláchnuty	propláchnout	k5eAaPmNgInP
levé	levý	k2eAgInPc1d1
pitot-statické	pitot-statický	k2eAgInPc1d1
systémy	systém	k1gInPc1
(	(	kIx(
<g/>
systémy	systém	k1gInPc1
zodpovědné	zodpovědný	k2eAgInPc1d1
za	za	k7c4
indikaci	indikace	k1gFnSc4
vzdušné	vzdušný	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
a	a	k8xC
výšky	výška	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
vyčištěn	vyčištěn	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
konektor	konektor	k1gInSc4
počítače	počítač	k1gInSc2
systému	systém	k1gInSc2
zpětné	zpětný	k2eAgFnSc2d1
vazby	vazba	k1gFnSc2
podélného	podélný	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
(	(	kIx(
<g/>
elevator	elevator	k1gMnSc1
feel	feenout	k5eAaPmAgMnS
computer	computer	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgNnSc4d1
doporučení	doporučení	k1gNnSc4
</s>
<s>
Indonéská	indonéský	k2eAgFnSc1d1
Národní	národní	k2eAgFnSc1d1
komise	komise	k1gFnSc1
pro	pro	k7c4
bezpečnost	bezpečnost	k1gFnSc4
v	v	k7c6
dopravě	doprava	k1gFnSc6
(	(	kIx(
<g/>
KNKT	KNKT	kA
<g/>
)	)	kIx)
vydala	vydat	k5eAaPmAgFnS
po	po	k7c6
nehodě	nehoda	k1gFnSc6
pro	pro	k7c4
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
dvě	dva	k4xCgFnPc1
bezpečností	bezpečnost	k1gFnPc2
doporučení	doporučení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
konstatuje	konstatovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
let	let	k1gInSc4
JT43	JT43	k1gFnSc2
(	(	kIx(
<g/>
tj.	tj.	kA
let	let	k1gInSc4
přecházející	přecházející	k2eAgInSc1d1
havarovanému	havarovaný	k2eAgInSc3d1
letu	let	k1gInSc3
<g/>
)	)	kIx)
neměl	mít	k5eNaImAgInS
být	být	k5eAaImF
proveden	provést	k5eAaPmNgInS
<g/>
,	,	kIx,
protože	protože	k8xS
letadlo	letadlo	k1gNnSc1
bylo	být	k5eAaImAgNnS
neletuschopné	letuschopný	k2eNgNnSc1d1
(	(	kIx(
<g/>
aktivní	aktivní	k2eAgMnSc1d1
stick	stick	k1gMnSc1
shaker	shaker	k1gMnSc1
po	po	k7c4
téměř	téměř	k6eAd1
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
letu	let	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
kapitán	kapitán	k1gMnSc1
měl	mít	k5eAaImAgMnS
rozhodnout	rozhodnout	k5eAaPmF
o	o	k7c6
návratu	návrat	k1gInSc6
na	na	k7c4
výchozí	výchozí	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
(	(	kIx(
<g/>
Denpasar	Denpasar	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
doporučení	doporučení	k1gNnSc2
by	by	kYmCp3nS
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
měl	mít	k5eAaImAgMnS
zlepšit	zlepšit	k5eAaPmF
povědomí	povědomí	k1gNnSc4
svých	svůj	k3xOyFgMnPc2
pilotů	pilot	k1gMnPc2
o	o	k7c6
neletuschopnosti	neletuschopnost	k1gFnSc6
letadla	letadlo	k1gNnSc2
a	a	k8xC
zlepšit	zlepšit	k5eAaPmF
tím	ten	k3xDgNnSc7
kuturu	kutura	k1gFnSc4
bezpečnosti	bezpečnost	k1gFnSc2
letecké	letecký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhé	druhý	k4xOgFnSc2
doporučení	doporučení	k1gNnPc2
vytýká	vytýkat	k5eAaImIp3nS
Lion	Lion	k1gMnSc1
Air	Air	k1gFnPc2
nesoulad	nesoulad	k1gInSc4
mezi	mezi	k7c7
počtem	počet	k1gInSc7
palubních	palubní	k2eAgFnPc2d1
průvodčích	průvodčí	k1gFnPc2
ve	v	k7c6
dvou	dva	k4xCgInPc6
dokumentech	dokument	k1gInPc6
letu	let	k1gInSc2
JT	JT	kA
<g/>
610	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
jeden	jeden	k4xCgInSc1
uvádí	uvádět	k5eAaImIp3nS
5	#num#	k4
a	a	k8xC
druhý	druhý	k4xOgInSc1
6	#num#	k4
a	a	k8xC
vyzývá	vyzývat	k5eAaImIp3nS
k	k	k7c3
zajištění	zajištění	k1gNnSc3
správného	správný	k2eAgNnSc2d1
vyplňování	vyplňování	k1gNnSc2
letové	letový	k2eAgFnSc2d1
dokumentace	dokumentace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reakce	reakce	k1gFnSc1
Boeingu	boeing	k1gInSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2018	#num#	k4
vydal	vydat	k5eAaPmAgInS
Boeing	boeing	k1gInSc1
oběžník	oběžník	k1gInSc1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
letovým	letový	k2eAgFnPc3d1
posádkám	posádka	k1gFnPc3
Boeingu	boeing	k1gInSc2
737	#num#	k4
MAX	max	kA
8	#num#	k4
a	a	k8xC
9	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
popisuje	popisovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
postupovat	postupovat	k5eAaImF
v	v	k7c6
případě	případ	k1gInSc6
nevyžádaného	vyžádaný	k2eNgInSc2d1
automatického	automatický	k2eAgInSc2d1
zásahu	zásah	k1gInSc2
do	do	k7c2
řízení	řízení	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
důsledku	důsledek	k1gInSc6
chybných	chybný	k2eAgInPc2d1
údajů	údaj	k1gInPc2
ze	z	k7c2
senzoru	senzor	k1gInSc2
úhlu	úhel	k1gInSc2
náběhu	náběh	k1gInSc2
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
potlačení	potlačení	k1gNnSc3
letadla	letadlo	k1gNnSc2
nosem	nos	k1gInSc7
dolů	dol	k1gInPc2
během	během	k7c2
manuálního	manuální	k2eAgInSc2d1
letu	let	k1gInSc2
(	(	kIx(
<g/>
bez	bez	k7c2
použití	použití	k1gNnSc2
autopilota	autopilot	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
důvod	důvod	k1gInSc1
vydání	vydání	k1gNnSc2
Boeing	boeing	k1gInSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
chtějí	chtít	k5eAaImIp3nP
"	"	kIx"
<g/>
zdůraznit	zdůraznit	k5eAaPmF
postup	postup	k1gInSc4
při	při	k7c6
poruše	porucha	k1gFnSc6
vyvažování	vyvažování	k1gNnSc1
poskytnutý	poskytnutý	k2eAgMnSc1d1
v	v	k7c6
dokumentaci	dokumentace	k1gFnSc6
jako	jako	k8xS,k8xC
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nestandardních	standardní	k2eNgMnPc2d1
checklistů	checklista	k1gMnPc2
(	(	kIx(
<g/>
Runaway	Runawaa	k1gMnSc2
Stabilizer	Stabilizer	k1gMnSc1
NonNormal	NonNormal	k1gMnSc1
Checklist	Checklist	k1gMnSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2018	#num#	k4
poslal	poslat	k5eAaPmAgInS
Boeing	boeing	k1gInSc1
zprávu	zpráva	k1gFnSc4
všem	všecek	k3xTgMnPc3
provozovatelům	provozovatel	k1gMnPc3
(	(	kIx(
<g/>
tzv.	tzv.	kA
MOM	MOM	kA
–	–	k?
Multi-operator	Multi-operator	k1gInSc1
Message	Message	k1gFnSc1
<g/>
)	)	kIx)
typu	typ	k1gInSc2
737	#num#	k4
NG	NG	kA
a	a	k8xC
737	#num#	k4
MAX	max	kA
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
popisuje	popisovat	k5eAaImIp3nS
fungování	fungování	k1gNnSc1
systému	systém	k1gInSc2
MCAS	MCAS	kA
(	(	kIx(
<g/>
Maneuvering	Maneuvering	k1gInSc1
Characteristics	Characteristics	k1gInSc1
Augmentation	Augmentation	k1gInSc1
System	Syst	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
nesprávná	správný	k2eNgFnSc1d1
funkce	funkce	k1gFnSc1
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
příčinou	příčina	k1gFnSc7
havárie	havárie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
zprávy	zpráva	k1gFnSc2
je	být	k5eAaImIp3nS
i	i	k9
popis	popis	k1gInSc4
nouzového	nouzový	k2eAgInSc2d1
postup	postup	k1gInSc4
pro	pro	k7c4
případ	případ	k1gInSc4
nesprávné	správný	k2eNgFnSc2d1
funkce	funkce	k1gFnSc2
systému	systém	k1gInSc2
MCAS	MCAS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
(	(	kIx(
<g/>
tedy	tedy	k9
2	#num#	k4
dny	den	k1gInPc4
po	po	k7c6
nehodě	nehoda	k1gFnSc6
letu	let	k1gInSc2
Ethiopian	Ethiopian	k1gInSc4
Airlines	Airlines	k1gInSc1
302	#num#	k4
)	)	kIx)
vydal	vydat	k5eAaPmAgInS
Boeing	boeing	k1gInSc4
prohlášení	prohlášení	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
posledních	poslední	k2eAgInPc6d1
několika	několik	k4yIc6
měsících	měsíc	k1gInPc6
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
nehodu	nehoda	k1gFnSc4
Lion	Lion	k1gMnSc1
Air	Air	k1gFnSc2
610	#num#	k4
na	na	k7c4
vylepšení	vylepšení	k1gNnSc4
softwaru	software	k1gInSc2
řízení	řízení	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tak	tak	k6eAd1
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
"	"	kIx"
<g/>
bezpečné	bezpečný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
ještě	ještě	k6eAd1
bezpečnějším	bezpečný	k2eAgInPc3d2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vylepšení	vylepšení	k1gNnSc1
má	mít	k5eAaImIp3nS
spočívat	spočívat	k5eAaImF
v	v	k7c6
úpravě	úprava	k1gFnSc6
MCAS	MCAS	kA
<g/>
,	,	kIx,
změnu	změna	k1gFnSc4
v	v	k7c6
zobrazovaných	zobrazovaný	k2eAgFnPc6d1
informacích	informace	k1gFnPc6
na	na	k7c6
primárním	primární	k2eAgInSc6d1
letovém	letový	k2eAgInSc6d1
displayi	display	k1gInSc6
<g/>
,	,	kIx,
změny	změna	k1gFnPc4
v	v	k7c6
dokumentaci	dokumentace	k1gFnSc6
a	a	k8xC
ve	v	k7c6
výcviku	výcvik	k1gInSc6
posádek	posádka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
MCAS	MCAS	kA
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
touto	tento	k3xDgFnSc7
úpravou	úprava	k1gFnSc7
učiněn	učinit	k5eAaImNgMnS,k5eAaPmNgMnS
odolnější	odolný	k2eAgMnSc1d2
vůči	vůči	k7c3
chybnému	chybný	k2eAgInSc3d1
vztupu	vztup	k1gInSc3
ze	z	k7c2
senzorů	senzor	k1gInPc2
a	a	k8xC
také	také	k9
by	by	kYmCp3nS
měl	mít	k5eAaImAgMnS
mít	mít	k5eAaImF
omezenější	omezený	k2eAgInSc4d2
rozsah	rozsah	k1gInSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
za	za	k7c2
každých	každý	k3xTgFnPc2
okolností	okolnost	k1gFnPc2
zachovával	zachovávat	k5eAaImAgMnS
autoritu	autorita	k1gFnSc4
výškového	výškový	k2eAgNnSc2d1
kormidla	kormidlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc4
typů	typ	k1gInPc2
Boeing	boeing	k1gInSc1
737	#num#	k4
MAX	max	kA
</s>
<s>
Nehoda	nehoda	k1gFnSc1
již	již	k6eAd1
také	také	k9
rozvířila	rozvířit	k5eAaPmAgFnS
diskuzi	diskuze	k1gFnSc4
<g/>
,	,	kIx,
jestli	jestli	k8xS
tento	tento	k3xDgInSc1
nejnovější	nový	k2eAgInSc1d3
model	model	k1gInSc1
od	od	k7c2
společnosti	společnost	k1gFnSc2
Boeing	boeing	k1gInSc1
je	být	k5eAaImIp3nS
bezpečný	bezpečný	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Letecké	letecký	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
zatím	zatím	k6eAd1
objednaly	objednat	k5eAaPmAgFnP
minimálně	minimálně	k6eAd1
4	#num#	k4
700	#num#	k4
kusů	kus	k1gInPc2
tohoto	tento	k3xDgInSc2
modelu	model	k1gInSc2
a	a	k8xC
219	#num#	k4
z	z	k7c2
nich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
již	již	k6eAd1
používáno	používán	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
tím	ten	k3xDgNnSc7
již	již	k6eAd1
indonéská	indonéský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
nařídila	nařídit	k5eAaPmAgFnS
inspekci	inspekce	k1gFnSc4
všech	všecek	k3xTgInPc2
strojů	stroj	k1gInPc2
stejného	stejný	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
nehodě	nehoda	k1gFnSc6
letu	let	k1gInSc2
Ethiopian	Ethiopian	k1gInSc4
airlines	airlines	k1gInSc1
302	#num#	k4
dne	den	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
byla	být	k5eAaImAgFnS
všechna	všechen	k3xTgNnPc4
letadla	letadlo	k1gNnPc4
typu	typ	k1gInSc2
Boeing	boeing	k1gInSc4
737	#num#	k4
MAX	max	kA
celosvětově	celosvětově	k6eAd1
uzeměna	uzeměn	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Accident	Accident	k1gMnSc1
description	description	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aviation	Aviation	k1gInSc4
Safety	Safeta	k1gFnSc2
Network	network	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-04-11	2018-04-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HASHIM	HASHIM	kA
<g/>
,	,	kIx,
Firdaus	Firdaus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
737	#num#	k4
Max	max	kA
8	#num#	k4
crash	crash	k1gMnSc1
confirmed	confirmed	k1gMnSc1
<g/>
,	,	kIx,
189	#num#	k4
dead	deada	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flightglobal	Flightglobal	k1gFnPc2
<g/>
,	,	kIx,
2018-10-29	2018-10-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
První	první	k4xOgFnSc1
nehoda	nehoda	k1gFnSc1
MAX	max	kA
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
starý	starý	k2eAgInSc4d1
stroj	stroj	k1gInSc4
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
se	se	k3xPyFc4
zřítil	zřítit	k5eAaPmAgMnS
do	do	k7c2
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdopravy	Zdoprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CTV	CTV	kA
NEWS	NEWS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
plane	planout	k5eAaImIp3nS
hit	hit	k1gInSc4
water	watra	k1gFnPc2
'	'	kIx"
<g/>
at	at	k?
a	a	k8xC
high	high	k1gMnSc1
rate	rat	k1gFnSc2
of	of	k?
speed	speed	k1gMnSc1
<g/>
'	'	kIx"
<g/>
:	:	kIx,
aviation	aviation	k1gInSc1
expert	expert	k1gMnSc1
on	on	k3xPp3gMnSc1
Indonesia	Indonesius	k1gMnSc4
plane	planout	k5eAaImIp3nS
crash	crash	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ABC	ABC	kA
NEWS	NEWS	kA
(	(	kIx(
<g/>
AUSTRALIA	AUSTRALIA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
crash	crash	k1gMnSc1
plane	planout	k5eAaImIp3nS
was	was	k?
brand	brand	k1gInSc1
new	new	k?
<g/>
,	,	kIx,
but	but	k?
had	had	k1gMnSc1
a	a	k8xC
'	'	kIx"
<g/>
technical	technicat	k5eAaPmAgInS
issue	issue	k1gFnSc4
<g/>
'	'	kIx"
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
THE	THE	kA
SUN	Sun	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Search	Search	k1gInSc1
crews	crews	k6eAd1
think	think	k1gInSc1
they	thea	k1gFnSc2
found	found	k1gMnSc1
crashed	crashed	k1gMnSc1
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
jet	jet	k5eAaImNgMnS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
CNN	CNN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pings	Pings	k1gInSc1
detected	detected	k1gInSc4
in	in	k?
search	searcha	k1gFnPc2
for	forum	k1gNnPc2
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
plane	planout	k5eAaImIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Indonesia	Indonesium	k1gNnSc2
Finds	Findsa	k1gFnPc2
Cockpit	Cockpit	k1gMnSc1
Voice	Voice	k1gMnSc1
Recorder	Recorder	k1gMnSc1
of	of	k?
Crashed	Crashed	k1gMnSc1
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
Jet	jet	k2eAgMnSc1d1
<g/>
.	.	kIx.
www.bloomberg.com	www.bloomberg.com	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Preliminary	Preliminar	k1gInPc7
Aircraft	Aircraft	k1gMnSc1
Accident	Accident	k1gMnSc1
Investigation	Investigation	k1gInSc4
Report	report	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-11-28	2018-11-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Flight	Flight	k1gInSc1
Crew	Crew	k1gFnPc2
Operations	Operations	k1gInSc1
Manual	Manual	k1gMnSc1
Bulletin	bulletin	k1gInSc1
for	forum	k1gNnPc2
The	The	k1gFnSc2
Boeing	boeing	k1gInSc4
Company	Compana	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Boeing	boeing	k1gInSc1
Correspondence	Correspondence	k1gFnSc1
<g/>
:	:	kIx,
Multi	Mult	k1gMnPc1
Operator	Operator	k1gMnSc1
Messages	Messages	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
CBC	CBC	kA
NEWS	NEWS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Focus	Focus	k1gInSc1
turns	turns	k6eAd1
to	ten	k3xDgNnSc1
new	new	k?
Boeing	boeing	k1gInSc1
737	#num#	k4
Max	max	kA
8	#num#	k4
after	aftero	k1gNnPc2
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
JT160	JT160	k1gMnSc1
crash	crash	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
CBS	CBS	kA
THIS	THIS	kA
MORNING	MORNING	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Passengers	Passengers	k1gInSc4
on	on	k3xPp3gMnSc1
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
jet	jet	k2eAgInSc4d1
night	night	k1gInSc4
before	befor	k1gInSc5
crash	crash	k1gMnSc1
described	described	k1gMnSc1
"	"	kIx"
<g/>
roller	roller	k1gMnSc1
coaster	coaster	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ABC	ABC	kA
NEWS	NEWS	kA
(	(	kIx(
<g/>
AUSTRALIA	AUSTRALIA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
crash	crash	k1gMnSc1
site	site	k6eAd1
may	may	k?
have	have	k1gInSc1
been	been	k1gMnSc1
found	found	k1gMnSc1
<g/>
,	,	kIx,
says	says	k6eAd1
Indonesian	Indonesian	k1gMnSc1
military	militara	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnSc1d1
</s>
<s>
Let	let	k1gInSc1
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
583	#num#	k4
</s>
<s>
Let	let	k1gInSc1
Lion	Lion	k1gMnSc1
Air	Air	k1gMnSc1
904	#num#	k4
</s>
<s>
Let	let	k1gInSc1
Ethiopian	Ethiopiany	k1gInPc2
Airlines	Airlines	k1gInSc1
302	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
