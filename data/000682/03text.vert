<s>
Filadelfie	Filadelfie	k1gFnSc1	Filadelfie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Pensylvánie	Pensylvánie	k1gFnSc2	Pensylvánie
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řeka	k1gFnPc2	řeka
Delaware	Delawar	k1gMnSc5	Delawar
a	a	k8xC	a
Schuylkill	Schuylkillum	k1gNnPc2	Schuylkillum
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Atlantického	atlantický	k2eAgInSc2d1	atlantický
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
1	[number]	k4	1
526000	[number]	k4	526000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
pak	pak	k6eAd1	pak
přes	přes	k7c4	přes
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Filadelfie	Filadelfie	k1gFnSc1	Filadelfie
je	být	k5eAaImIp3nS	být
sídelním	sídelní	k2eAgNnSc7d1	sídelní
městem	město	k1gNnSc7	město
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
filadelfské	filadelfský	k2eAgFnSc2d1	Filadelfská
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
metropolitou	metropolita	k1gMnSc7	metropolita
Provincie	provincie	k1gFnSc2	provincie
Pensylvánie	Pensylvánie	k1gFnSc1	Pensylvánie
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgNnSc1d1	významné
postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
zde	zde	k6eAd1	zde
sepsány	sepsán	k2eAgFnPc1d1	sepsána
Deklarace	deklarace	k1gFnPc1	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
a	a	k8xC	a
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1790	[number]	k4	1790
<g/>
-	-	kIx~	-
<g/>
1800	[number]	k4	1800
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
univerzit	univerzita	k1gFnPc2	univerzita
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
i	i	k9	i
University	universita	k1gFnPc4	universita
of	of	k?	of
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
bratrská	bratrský	k2eAgFnSc1d1	bratrská
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
filos	filos	k1gInSc1	filos
-	-	kIx~	-
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
adelfos	adelfos	k1gMnSc1	adelfos
-	-	kIx~	-
bratr	bratr	k1gMnSc1	bratr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původním	původní	k2eAgNnSc7d1	původní
místem	místo	k1gNnSc7	místo
osídlení	osídlení	k1gNnSc2	osídlení
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
jsou	být	k5eAaImIp3nP	být
čtvrti	čtvrt	k1gFnPc1	čtvrt
Old	Olda	k1gFnPc2	Olda
Town	Towna	k1gFnPc2	Towna
a	a	k8xC	a
Society	societa	k1gFnSc2	societa
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Delaware	Delawar	k1gMnSc5	Delawar
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
je	on	k3xPp3gInPc4	on
převážně	převážně	k6eAd1	převážně
cihlové	cihlový	k2eAgInPc4d1	cihlový
domy	dům	k1gInPc4	dům
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
st.	st.	kA	st.
Velmi	velmi	k6eAd1	velmi
významnou	významný	k2eAgFnSc4d1	významná
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
historie	historie	k1gFnSc2	historie
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Independence	Independence	k1gFnSc1	Independence
National	National	k1gFnSc2	National
Historic	Historice	k1gFnPc2	Historice
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
park	park	k1gInSc1	park
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
historických	historický	k2eAgFnPc2d1	historická
budov	budova	k1gFnPc2	budova
západně	západně	k6eAd1	západně
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
čtvrť	čtvrť	k1gFnSc4	čtvrť
Old	Olda	k1gFnPc2	Olda
Town	Towno	k1gNnPc2	Towno
<g/>
.	.	kIx.	.
</s>
<s>
Independence	Independence	k1gFnSc1	Independence
National	National	k1gFnSc2	National
Historic	Historice	k1gFnPc2	Historice
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
Národní	národní	k2eAgInSc1d1	národní
historický	historický	k2eAgInSc1d1	historický
park	park	k1gInSc1	park
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
park	park	k1gInSc1	park
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
historických	historický	k2eAgFnPc2d1	historická
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
<g/>
,	,	kIx,	,
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
ha	ha	kA	ha
(	(	kIx(	(
<g/>
0,2	[number]	k4	0,2
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
památkám	památka	k1gFnPc3	památka
náleží	náležet	k5eAaImIp3nS	náležet
budova	budova	k1gFnSc1	budova
Independence	Independence	k1gFnSc2	Independence
Hall	Halla	k1gFnPc2	Halla
vystavěná	vystavěný	k2eAgFnSc1d1	vystavěná
v	v	k7c6	v
georgiánském	georgiánský	k2eAgInSc6d1	georgiánský
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	s	k7c7	s
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1776	[number]	k4	1776
sešli	sejít	k5eAaPmAgMnP	sejít
zástupci	zástupce	k1gMnPc1	zástupce
třinácti	třináct	k4xCc2	třináct
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
sestavili	sestavit	k5eAaPmAgMnP	sestavit
Deklaraci	deklarace	k1gFnSc4	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
budovou	budova	k1gFnSc7	budova
leží	ležet	k5eAaImIp3nS	ležet
náměstí	náměstí	k1gNnSc1	náměstí
Independence	Independence	k1gFnSc2	Independence
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
Deklarace	deklarace	k1gFnSc1	deklarace
poprvé	poprvé	k6eAd1	poprvé
veřejně	veřejně	k6eAd1	veřejně
přečtena	přečten	k2eAgFnSc1d1	přečtena
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
další	další	k2eAgFnSc1d1	další
významná	významný	k2eAgFnSc1d1	významná
památka	památka	k1gFnSc1	památka
zvon	zvon	k1gInSc1	zvon
Liberty	Libert	k1gInPc4	Libert
Bell	bell	k1gInSc1	bell
(	(	kIx(	(
<g/>
Zvon	zvon	k1gInSc1	zvon
svobody	svoboda	k1gFnSc2	svoboda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
samostatně	samostatně	k6eAd1	samostatně
v	v	k7c4	v
Liberty	Libert	k1gMnPc4	Libert
Bell	bell	k1gInSc1	bell
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Zvon	zvon	k1gInSc1	zvon
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1752	[number]	k4	1752
a	a	k8xC	a
používal	používat	k5eAaImAgInS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
významných	významný	k2eAgFnPc6d1	významná
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
významným	významný	k2eAgFnPc3d1	významná
budovám	budova	k1gFnPc3	budova
v	v	k7c6	v
parku	park	k1gInSc6	park
náleží	náležet	k5eAaImIp3nS	náležet
Congress	Congress	k1gInSc1	Congress
Hall	Halla	k1gFnPc2	Halla
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
zasedání	zasedání	k1gNnSc1	zasedání
kongresu	kongres	k1gInSc2	kongres
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
Filadelfie	Filadelfie	k1gFnSc1	Filadelfie
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
)	)	kIx)	)
a	a	k8xC	a
Old	Olda	k1gFnPc2	Olda
City	city	k1gNnSc1	city
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
<g/>
,	,	kIx,	,
původní	původní	k2eAgNnSc4d1	původní
místo	místo	k1gNnSc4	místo
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Library	Librara	k1gFnPc1	Librara
Hall	Halla	k1gFnPc2	Halla
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
a	a	k8xC	a
Carpenter	Carpenter	k1gMnSc1	Carpenter
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hall	Hall	k1gInSc1	Hall
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInSc1d3	nejstarší
cech	cech	k1gInSc1	cech
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
založen	založit	k5eAaPmNgInS	založit
1724	[number]	k4	1724
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
Independence	Independence	k1gFnSc2	Independence
Hall	Hall	k1gInSc1	Hall
leží	ležet	k5eAaImIp3nS	ležet
Philosophical	Philosophical	k1gFnSc4	Philosophical
Hall	Halla	k1gFnPc2	Halla
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc4	sídlo
americké	americký	k2eAgFnSc2d1	americká
filosofické	filosofický	k2eAgFnSc2d1	filosofická
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
založené	založený	k2eAgInPc1d1	založený
roku	rok	k1gInSc2	rok
1743	[number]	k4	1743
B.	B.	kA	B.
Franklinem	Franklin	k1gInSc7	Franklin
<g/>
)	)	kIx)	)
a	a	k8xC	a
Second	Second	k1gMnSc1	Second
Bank	banka	k1gFnPc2	banka
of	of	k?	of
the	the	k?	the
US	US	kA	US
<g/>
,	,	kIx,	,
mramorová	mramorový	k2eAgFnSc1d1	mramorová
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
dle	dle	k7c2	dle
vzoru	vzor	k1gInSc2	vzor
řeckého	řecký	k2eAgInSc2d1	řecký
Parthenonu	Parthenon	k1gInSc2	Parthenon
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
sídlo	sídlo	k1gNnSc1	sídlo
federální	federální	k2eAgFnSc2d1	federální
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
National	National	k1gFnSc1	National
Portrait	Portraita	k1gFnPc2	Portraita
Gallery	Galler	k1gMnPc4	Galler
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
Independence	Independence	k1gFnSc2	Independence
Nat	Nat	k1gFnSc2	Nat
<g/>
.	.	kIx.	.
</s>
<s>
Historic	Historic	k1gMnSc1	Historic
Park	park	k1gInSc4	park
leží	ležet	k5eAaImIp3nS	ležet
Old	Olda	k1gFnPc2	Olda
City	City	k1gFnSc2	City
(	(	kIx(	(
<g/>
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
prošla	projít	k5eAaPmAgFnS	projít
čtvrť	čtvrť	k1gFnSc1	čtvrť
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
,	,	kIx,	,
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
obytných	obytný	k2eAgInPc2d1	obytný
domů	dům	k1gInPc2	dům
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
knihkupectví	knihkupectví	k1gNnPc1	knihkupectví
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
místní	místní	k2eAgInPc1d1	místní
malé	malý	k2eAgInPc1d1	malý
obchody	obchod	k1gInPc1	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Elfreth	Elfreth	k1gInSc1	Elfreth
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Alley	Alle	k1gMnPc7	Alle
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejstarší	starý	k2eAgFnSc4d3	nejstarší
obývanou	obývaný	k2eAgFnSc4d1	obývaná
ulici	ulice	k1gFnSc4	ulice
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrť	čtvrť	k1gFnSc1	čtvrť
Society	societa	k1gFnSc2	societa
Hill	Hilla	k1gFnPc2	Hilla
tvoří	tvořit	k5eAaImIp3nP	tvořit
především	především	k9	především
historické	historický	k2eAgFnPc1d1	historická
dvou	dva	k4xCgFnPc2	dva
až	až	k8xS	až
trojpodlažní	trojpodlažní	k2eAgInPc1d1	trojpodlažní
cihlové	cihlový	k2eAgInPc1d1	cihlový
řadové	řadový	k2eAgInPc1d1	řadový
rodinné	rodinný	k2eAgInPc1d1	rodinný
domy	dům	k1gInPc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristické	charakteristický	k2eAgFnPc1d1	charakteristická
jsou	být	k5eAaImIp3nP	být
barevné	barevný	k2eAgFnPc1d1	barevná
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
okenice	okenice	k1gFnPc1	okenice
a	a	k8xC	a
dveře	dveře	k1gFnPc1	dveře
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Místy	místy	k6eAd1	místy
malé	malý	k2eAgFnPc4d1	malá
tabulky	tabulka	k1gFnPc4	tabulka
na	na	k7c6	na
domech	dům	k1gInPc6	dům
informují	informovat	k5eAaBmIp3nP	informovat
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
domu	dům	k1gInSc2	dům
a	a	k8xC	a
prvních	první	k4xOgFnPc2	první
nájemnících	nájemník	k1gMnPc6	nájemník
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
čtvrti	čtvrt	k1gFnSc2	čtvrt
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Washington	Washington	k1gInSc1	Washington
Square	square	k1gInSc1	square
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
historické	historický	k2eAgFnSc2d1	historická
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Old	Olda	k1gFnPc2	Olda
City	City	k1gFnSc2	City
a	a	k8xC	a
Society	societa	k1gFnSc2	societa
Hill	Hill	k1gMnSc1	Hill
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
střed	střed	k1gInSc1	střed
Center	centrum	k1gNnPc2	centrum
City	City	k1gFnSc2	City
(	(	kIx(	(
<g/>
Center	centrum	k1gNnPc2	centrum
City	City	k1gFnPc2	City
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
dominant	dominanta	k1gFnPc2	dominanta
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
<g/>
,	,	kIx,	,
budovou	budova	k1gFnSc7	budova
radnice	radnice	k1gFnSc2	radnice
(	(	kIx(	(
<g/>
City	city	k1gNnSc1	city
Hall	Halla	k1gFnPc2	Halla
<g/>
,	,	kIx,	,
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Penn	Penna	k1gFnPc2	Penna
Square	square	k1gInSc1	square
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
dokončená	dokončený	k2eAgFnSc1d1	dokončená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
a	a	k8xC	a
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
167	[number]	k4	167
m	m	kA	m
patřila	patřit	k5eAaImAgFnS	patřit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
k	k	k7c3	k
nejvyšším	vysoký	k2eAgFnPc3d3	nejvyšší
stavbám	stavba	k1gFnPc3	stavba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1894	[number]	k4	1894
-	-	kIx~	-
1908	[number]	k4	1908
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
obytnou	obytný	k2eAgFnSc7d1	obytná
nesakrální	sakrální	k2eNgFnSc7d1	sakrální
stavbou	stavba	k1gFnSc7	stavba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
radnici	radnice	k1gFnSc3	radnice
stojí	stát	k5eAaImIp3nS	stát
rozlehlý	rozlehlý	k2eAgInSc1d1	rozlehlý
zednářský	zednářský	k2eAgInSc1d1	zednářský
chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
1868	[number]	k4	1868
-	-	kIx~	-
73	[number]	k4	73
v	v	k7c6	v
normanském	normanský	k2eAgInSc6d1	normanský
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
radnice	radnice	k1gFnSc2	radnice
a	a	k8xC	a
Penn	Penn	k1gMnSc1	Penn
Square	square	k1gInSc4	square
leží	ležet	k5eAaImIp3nS	ležet
ulice	ulice	k1gFnSc1	ulice
JFK	JFK	kA	JFK
Boulevard	Boulevarda	k1gFnPc2	Boulevarda
(	(	kIx(	(
<g/>
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Logan	Logana	k1gFnPc2	Logana
Square	square	k1gInSc1	square
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
výškové	výškový	k2eAgFnSc2d1	výšková
budovy	budova	k1gFnSc2	budova
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
finanční	finanční	k2eAgFnSc7d1	finanční
čtvrtí	čtvrt	k1gFnSc7	čtvrt
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
velké	velký	k2eAgInPc1d1	velký
hotely	hotel	k1gInPc1	hotel
<g/>
,	,	kIx,	,
muzea	muzeum	k1gNnPc1	muzeum
<g/>
,	,	kIx,	,
koncertní	koncertní	k2eAgInPc1d1	koncertní
sály	sál	k1gInPc1	sál
<g/>
,	,	kIx,	,
obchody	obchod	k1gInPc1	obchod
a	a	k8xC	a
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
budovou	budova	k1gFnSc7	budova
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
Comcast	Comcast	k1gFnSc1	Comcast
Center	centrum	k1gNnPc2	centrum
s	s	k7c7	s
297	[number]	k4	297
m	m	kA	m
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2005	[number]	k4	2005
-	-	kIx~	-
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
známější	známý	k2eAgFnSc1d2	známější
je	být	k5eAaImIp3nS	být
design	design	k1gInSc4	design
druhé	druhý	k4xOgFnSc2	druhý
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
budovy	budova	k1gFnSc2	budova
ve	v	k7c6	v
městě	město	k1gNnSc6	město
One	One	k1gFnSc2	One
Liberty	Libert	k1gInPc1	Libert
Place	plac	k1gInSc6	plac
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
288	[number]	k4	288
m.	m.	k?	m.
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
třídy	třída	k1gFnSc2	třída
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklina	k1gFnPc2	Franklina
Parkway	Parkwaa	k1gFnSc2	Parkwaa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tzv.	tzv.	kA	tzv.
Museum	museum	k1gNnSc1	museum
District	District	k1gInSc1	District
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
náleží	náležet	k5eAaImIp3nS	náležet
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gFnPc2	Art
a	a	k8xC	a
Franklin	Franklina	k1gFnPc2	Franklina
Institute	institut	k1gInSc5	institut
Science	Science	k1gFnSc1	Science
Museum	museum	k1gNnSc4	museum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Logan	Logan	k1gInSc4	Logan
Square	square	k1gInSc1	square
navazuje	navazovat	k5eAaImIp3nS	navazovat
jižně	jižně	k6eAd1	jižně
čtvrť	čtvrť	k1gFnSc1	čtvrť
Rittenhouse	Rittenhouse	k1gFnSc2	Rittenhouse
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
také	také	k9	také
s	s	k7c7	s
výškovými	výškový	k2eAgFnPc7d1	výšková
budovami	budova	k1gFnPc7	budova
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
s	s	k7c7	s
historickými	historický	k2eAgFnPc7d1	historická
výškovými	výškový	k2eAgFnPc7d1	výšková
budovami	budova	k1gFnPc7	budova
a	a	k8xC	a
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
cihlovou	cihlový	k2eAgFnSc7d1	cihlová
zástavbou	zástavba	k1gFnSc7	zástavba
místy	místy	k6eAd1	místy
nahrazenou	nahrazený	k2eAgFnSc7d1	nahrazená
novostavbami	novostavba	k1gFnPc7	novostavba
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
čtvrti	čtvrt	k1gFnSc2	čtvrt
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgNnSc6d1	stejnojmenné
náměstí	náměstí	k1gNnSc6	náměstí
Rittenhouse	Rittenhouse	k1gFnSc2	Rittenhouse
Square	square	k1gInSc4	square
<g/>
,	,	kIx,	,
plném	plný	k2eAgNnSc6d1	plné
zeleně	zeleně	k6eAd1	zeleně
se	s	k7c7	s
sochami	socha	k1gFnPc7	socha
a	a	k8xC	a
stánky	stánka	k1gFnSc2	stánka
s	s	k7c7	s
uměním	umění	k1gNnSc7	umění
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
Rittenhouse	Rittenhouse	k1gFnSc2	Rittenhouse
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
za	za	k7c4	za
druhou	druhý	k4xOgFnSc4	druhý
z	z	k7c2	z
filadelfských	filadelfský	k2eAgFnPc2d1	Filadelfská
řek	řeka	k1gFnPc2	řeka
Schuylkill	Schuylkilla	k1gFnPc2	Schuylkilla
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
čtvrť	čtvrť	k1gFnSc1	čtvrť
University	universita	k1gFnSc2	universita
City	city	k1gNnSc1	city
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
univerzita	univerzita	k1gFnSc1	univerzita
University	universita	k1gFnSc2	universita
of	of	k?	of
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1740	[number]	k4	1740
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
kampusu	kampus	k1gInSc2	kampus
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
i	i	k9	i
dvě	dva	k4xCgNnPc1	dva
muzea	muzeum	k1gNnPc1	muzeum
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
archeologie	archeologie	k1gFnSc2	archeologie
a	a	k8xC	a
antropologie	antropologie	k1gFnSc2	antropologie
a	a	k8xC	a
Institute	institut	k1gInSc5	institut
of	of	k?	of
Contemporary	Contemporara	k1gFnPc4	Contemporara
Art	Art	k1gFnSc2	Art
s	s	k7c7	s
díly	díl	k1gInPc7	díl
současných	současný	k2eAgMnPc2d1	současný
umělců	umělec	k1gMnPc2	umělec
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Rittenhouse	Rittenhouse	k1gFnSc2	Rittenhouse
Square	square	k1gInSc4	square
leží	ležet	k5eAaImIp3nS	ležet
South	South	k1gMnSc1	South
Street	Street	k1gMnSc1	Street
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
obchodů	obchod	k1gInPc2	obchod
s	s	k7c7	s
levným	levný	k2eAgNnSc7d1	levné
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
hudebními	hudební	k2eAgFnPc7d1	hudební
nahrávkami	nahrávka	k1gFnPc7	nahrávka
apod.	apod.	kA	apod.
Ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Bella	Bello	k1gNnSc2	Bello
Vista	vista	k2eAgNnSc2d1	vista
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
Italian	Italian	k1gInSc1	Italian
Market	market	k1gInSc4	market
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
venkovních	venkovní	k2eAgNnPc2d1	venkovní
tržišť	tržiště	k1gNnPc2	tržiště
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zboží	zboží	k1gNnSc1	zboží
řemeslníků	řemeslník	k1gMnPc2	řemeslník
<g/>
,	,	kIx,	,
domácí	domácí	k2eAgInPc1d1	domácí
sýry	sýr	k1gInPc1	sýr
<g/>
,	,	kIx,	,
těstoviny	těstovina	k1gFnPc1	těstovina
<g/>
,	,	kIx,	,
čerstvé	čerstvý	k2eAgFnPc1d1	čerstvá
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
maso	maso	k1gNnSc1	maso
<g/>
.	.	kIx.	.
</s>
<s>
Fairmount	Fairmount	k1gInSc1	Fairmount
Park	park	k1gInSc1	park
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
37	[number]	k4	37
km	km	kA	km
<g/>
2	[number]	k4	2
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgInPc3d3	veliký
městským	městský	k2eAgInPc3d1	městský
parkům	park	k1gInPc3	park
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Středem	středem	k7c2	středem
parku	park	k1gInSc2	park
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Schuylkill	Schuylkilla	k1gFnPc2	Schuylkilla
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
vedou	vést	k5eAaImIp3nP	vést
trasy	trasa	k1gFnPc1	trasa
pro	pro	k7c4	pro
běžce	běžec	k1gMnPc4	běžec
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
<g/>
,	,	kIx,	,
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
veslařský	veslařský	k2eAgInSc1d1	veslařský
klub	klub	k1gInSc1	klub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
je	být	k5eAaImIp3nS	být
také	také	k9	také
několik	několik	k4yIc4	několik
historických	historický	k2eAgInPc2d1	historický
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnSc1d3	nejstarší
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
st.	st.	kA	st.
Rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
umístěna	umístit	k5eAaPmNgFnS	umístit
filadelfská	filadelfský	k2eAgFnSc1d1	Filadelfská
zoo	zoo	k1gFnSc1	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
parku	park	k1gInSc2	park
leží	ležet	k5eAaImIp3nS	ležet
čtvrť	čtvrť	k1gFnSc1	čtvrť
Germantown	Germantown	k1gInSc1	Germantown
s	s	k7c7	s
několika	několik	k4yIc7	několik
historicky	historicky	k6eAd1	historicky
cennými	cenný	k2eAgFnPc7d1	cenná
budovami	budova	k1gFnPc7	budova
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
st.	st.	kA	st.
Severně	severně	k6eAd1	severně
od	od	k7c2	od
Germantownu	Germantown	k1gInSc2	Germantown
leží	ležet	k5eAaImIp3nP	ležet
čtvrť	čtvrť	k1gFnSc4	čtvrť
Chestnut	Chestnut	k2eAgInSc4d1	Chestnut
Hill	Hill	k1gInSc4	Hill
s	s	k7c7	s
historickými	historický	k2eAgInPc7d1	historický
obytnými	obytný	k2eAgInPc7d1	obytný
domy	dům	k1gInPc7	dům
a	a	k8xC	a
panskými	panský	k2eAgNnPc7d1	panské
sídly	sídlo	k1gNnPc7	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrť	čtvrť	k1gFnSc1	čtvrť
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
136	[number]	k4	136
m.	m.	k?	m.
Filadelfie	Filadelfie	k1gFnSc1	Filadelfie
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
12	[number]	k4	12
oblastí	oblast	k1gFnPc2	oblast
(	(	kIx(	(
<g/>
sections	sections	k1gInSc1	sections
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Center	centrum	k1gNnPc2	centrum
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
South	South	k1gMnSc1	South
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
,	,	kIx,	,
Southwest	Southwest	k1gFnSc1	Southwest
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
,	,	kIx,	,
West	West	k1gInSc1	West
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
,	,	kIx,	,
Lower	Lower	k1gInSc1	Lower
North	North	k1gInSc1	North
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
,	,	kIx,	,
Upper	Upper	k1gInSc1	Upper
North	North	k1gInSc1	North
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
,	,	kIx,	,
Bridesburg-Kensington-Richmond	Bridesburg-Kensington-Richmond	k1gMnSc1	Bridesburg-Kensington-Richmond
<g/>
,	,	kIx,	,
Roxborough-Manayunk	Roxborough-Manayunk	k1gMnSc1	Roxborough-Manayunk
<g/>
,	,	kIx,	,
Germantown-Chestnut	Germantown-Chestnut	k2eAgMnSc1d1	Germantown-Chestnut
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
Olney-Oak	Olney-Oak	k1gMnSc1	Olney-Oak
Lane	Lan	k1gFnSc2	Lan
<g/>
,	,	kIx,	,
Near	Near	k1gMnSc1	Near
Northeast	Northeast	k1gMnSc1	Northeast
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
,	,	kIx,	,
and	and	k?	and
Far	fara	k1gFnPc2	fara
Northeast	Northeast	k1gFnSc1	Northeast
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
oblastí	oblast	k1gFnPc2	oblast
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nP	tvořit
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
čtvrtě	čtvrt	k1gFnPc4	čtvrt
(	(	kIx(	(
<g/>
neighborhoods	ighborhoods	k6eNd1	ighborhoods
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
oblast	oblast	k1gFnSc1	oblast
Center	centrum	k1gNnPc2	centrum
City	City	k1gFnSc2	City
tvoří	tvořit	k5eAaImIp3nP	tvořit
čtvrti	čtvrt	k1gFnPc1	čtvrt
<g/>
:	:	kIx,	:
Avenue	avenue	k1gFnSc1	avenue
of	of	k?	of
the	the	k?	the
Arts	Arts	k1gInSc1	Arts
<g/>
,	,	kIx,	,
Callowhill	Callowhill	k1gMnSc1	Callowhill
<g/>
,	,	kIx,	,
Chinatown	Chinatown	k1gMnSc1	Chinatown
<g/>
,	,	kIx,	,
Elfreth	Elfreth	k1gMnSc1	Elfreth
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Alley	Alley	k1gInPc7	Alley
<g/>
,	,	kIx,	,
Fitler	Fitler	k1gInSc1	Fitler
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
Franklintown	Franklintown	k1gInSc1	Franklintown
<g/>
,	,	kIx,	,
French	French	k1gInSc1	French
Quarter	quarter	k1gInSc1	quarter
<g/>
,	,	kIx,	,
Jewelers	Jewelers	k1gInSc1	Jewelers
<g/>
'	'	kIx"	'
Row	Row	k1gMnSc1	Row
<g/>
,	,	kIx,	,
Logan	Logan	k1gMnSc1	Logan
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
Market	market	k1gInSc1	market
East	East	k1gInSc1	East
<g/>
,	,	kIx,	,
Museum	museum	k1gNnSc1	museum
District	Districta	k1gFnPc2	Districta
<g/>
,	,	kIx,	,
Naval	navalit	k5eAaPmRp2nS	navalit
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
Old	Olda	k1gFnPc2	Olda
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
Penn	Penn	k1gNnSc1	Penn
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Penn	Penn	k1gInSc1	Penn
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Landing	Landing	k1gInSc1	Landing
<g/>
,	,	kIx,	,
Rittenhouse	Rittenhouse	k1gFnSc1	Rittenhouse
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
Society	societa	k1gFnPc1	societa
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
South	South	k1gMnSc1	South
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
Square	square	k1gInSc1	square
West	West	k2eAgInSc1d1	West
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
1	[number]	k4	1
526	[number]	k4	526
006	[number]	k4	006
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
41,0	[number]	k4	41,0
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
43,4	[number]	k4	43,4
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,5	[number]	k4	0,5
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
6,3	[number]	k4	6,3
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
5,9	[number]	k4	5,9
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,8	[number]	k4	2,8
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
12,3	[number]	k4	12,3
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
síť	síť	k1gFnSc4	síť
veřejné	veřejný	k2eAgFnSc2d1	veřejná
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
starost	starost	k1gFnSc4	starost
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
SEPTA	septum	k1gNnSc2	septum
(	(	kIx(	(
<g/>
Southeastern	Southeastern	k1gNnSc1	Southeastern
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
Transportation	Transportation	k1gInSc4	Transportation
Authority	Authorita	k1gFnSc2	Authorita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Páteř	páteř	k1gFnSc1	páteř
dopravy	doprava	k1gFnSc2	doprava
tvoří	tvořit	k5eAaImIp3nS	tvořit
dvě	dva	k4xCgFnPc4	dva
linky	linka	k1gFnPc4	linka
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
<g/>
.	.	kIx.	.
</s>
<s>
Filadelfie	Filadelfie	k1gFnSc1	Filadelfie
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
měst	město	k1gNnPc2	město
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
ponechalo	ponechat	k5eAaPmAgNnS	ponechat
tramvajové	tramvajový	k2eAgFnPc4d1	tramvajová
linky	linka	k1gFnPc4	linka
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
jen	jen	k9	jen
osm	osm	k4xCc1	osm
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
jsou	být	k5eAaImIp3nP	být
rychlodrážního	rychlodrážní	k2eAgInSc2d1	rychlodrážní
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
jezdí	jezdit	k5eAaImIp3nP	jezdit
tramvaje	tramvaj	k1gFnPc1	tramvaj
v	v	k7c6	v
tunelu	tunel	k1gInSc6	tunel
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nebránily	bránit	k5eNaImAgFnP	bránit
provozu	provoz	k1gInSc3	provoz
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
linek	linka	k1gFnPc2	linka
vede	vést	k5eAaImIp3nS	vést
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgFnSc4d2	starší
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
síť	síť	k1gFnSc1	síť
byla	být	k5eAaImAgFnS	být
kdysi	kdysi	k6eAd1	kdysi
rozsáhlejší	rozsáhlý	k2eAgFnSc1d2	rozsáhlejší
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
většina	většina	k1gFnSc1	většina
tratí	trať	k1gFnPc2	trať
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
autobusy	autobus	k1gInPc4	autobus
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
kolejové	kolejový	k2eAgFnSc2d1	kolejová
dopravy	doprava	k1gFnSc2	doprava
může	moct	k5eAaImIp3nS	moct
SEPTA	septum	k1gNnPc1	septum
občanům	občan	k1gMnPc3	občan
a	a	k8xC	a
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
města	město	k1gNnPc1	město
nabídnout	nabídnout	k5eAaPmF	nabídnout
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
<g/>
,	,	kIx,	,
trolejbusovou	trolejbusový	k2eAgFnSc4d1	trolejbusová
a	a	k8xC	a
vlakovou	vlakový	k2eAgFnSc4d1	vlaková
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Filadelfie	Filadelfie	k1gFnSc1	Filadelfie
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
důležitým	důležitý	k2eAgInSc7d1	důležitý
železničním	železniční	k2eAgInSc7d1	železniční
uzlem	uzel	k1gInSc7	uzel
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
příměstských	příměstský	k2eAgInPc2d1	příměstský
vlaků	vlak	k1gInPc2	vlak
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
důležitá	důležitý	k2eAgFnSc1d1	důležitá
stanice	stanice	k1gFnSc1	stanice
Amtraku	Amtrak	k1gInSc2	Amtrak
s	s	k7c7	s
výpravnou	výpravný	k2eAgFnSc7d1	výpravná
nádražní	nádražní	k2eAgFnSc7d1	nádražní
budovou	budova	k1gFnSc7	budova
v	v	k7c6	v
klasicistním	klasicistní	k2eAgInSc6d1	klasicistní
stylu	styl	k1gInSc6	styl
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Greek	Greek	k1gInSc1	Greek
Revival	revival	k1gInSc1	revival
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Academy	Academa	k1gFnPc1	Academa
of	of	k?	of
Natural	Natural	k?	Natural
Sciences	Sciences	k1gMnSc1	Sciences
Museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
přírodovědné	přírodovědný	k2eAgNnSc1d1	Přírodovědné
muzeum	muzeum	k1gNnSc1	muzeum
American	American	k1gInSc1	American
Swedish	Swedish	k1gMnSc1	Swedish
Historical	Historical	k1gMnSc1	Historical
Museum	museum	k1gNnSc1	museum
Civil	civil	k1gMnSc1	civil
War	War	k1gMnSc1	War
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Franklin	Franklina	k1gFnPc2	Franklina
Institute	institut	k1gInSc5	institut
Science	Science	k1gFnSc1	Science
Museum	museum	k1gNnSc4	museum
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
dotýkat	dotýkat	k5eAaImF	dotýkat
se	se	k3xPyFc4	se
exponátů	exponát	k1gInPc2	exponát
Museum	museum	k1gNnSc1	museum
of	of	k?	of
the	the	k?	the
American	American	k1gMnSc1	American
Revolution	Revolution	k1gInSc4	Revolution
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
the	the	k?	the
Fine	Fin	k1gMnSc5	Fin
Arts	Arts	k1gInSc1	Arts
<g/>
,	,	kIx,	,
muzeum	muzeum	k1gNnSc1	muzeum
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
díly	díl	k1gInPc1	díl
amerických	americký	k2eAgMnPc2d1	americký
malířů	malíř	k1gMnPc2	malíř
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
muzeí	muzeum	k1gNnPc2	muzeum
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
;	;	kIx,	;
sbírka	sbírka	k1gFnSc1	sbírka
asijského	asijský	k2eAgNnSc2d1	asijské
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgNnSc1d1	renesanční
malířství	malířství	k1gNnSc1	malířství
<g/>
,	,	kIx,	,
postimpresionismus	postimpresionismus	k1gInSc1	postimpresionismus
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
Picassa	Picassa	k1gFnSc1	Picassa
<g/>
,	,	kIx,	,
Duchampa	Duchampa	k1gFnSc1	Duchampa
<g/>
,	,	kIx,	,
Matisse	Matisse	k1gFnSc1	Matisse
<g/>
;	;	kIx,	;
schodiště	schodiště	k1gNnSc1	schodiště
u	u	k7c2	u
hlavního	hlavní	k2eAgInSc2d1	hlavní
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
galerie	galerie	k1gFnSc2	galerie
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
z	z	k7c2	z
filmu	film	k1gInSc2	film
Rocky	rock	k1gInPc4	rock
Woodmere	Woodmer	k1gInSc5	Woodmer
Art	Art	k1gMnPc3	Art
Museum	museum	k1gNnSc4	museum
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
osobností	osobnost	k1gFnPc2	osobnost
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
<g/>
.	.	kIx.	.
</s>
<s>
Aix-en-Provence	Aixn-Provence	k1gFnSc1	Aix-en-Provence
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Atény	Atény	k1gFnPc1	Atény
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
Douala	Douala	k1gFnSc1	Douala
<g/>
,	,	kIx,	,
Kamerun	Kamerun	k1gInSc1	Kamerun
Florencie	Florencie	k1gFnSc1	Florencie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Incheon	Incheon	k1gInSc1	Incheon
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
Kóbe	Kóbe	k1gNnSc2	Kóbe
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Lyon	Lyon	k1gInSc1	Lyon
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Mosul	Mosul	k1gInSc1	Mosul
<g/>
,	,	kIx,	,
Irák	Irák	k1gInSc1	Irák
Nižnij	Nižnij	k1gFnPc2	Nižnij
Novgorod	Novgorod	k1gInSc1	Novgorod
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Tel	tel	kA	tel
Aviv	Aviv	k1gInSc1	Aviv
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
Tchien-ťin	Tchien-ťin	k1gInSc1	Tchien-ťin
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Toruń	Toruń	k1gFnSc1	Toruń
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
