<p>
<s>
Ruslana	Ruslana	k1gFnSc1	Ruslana
Stepanivna	Stepanivna	k1gFnSc1	Stepanivna
Lyžyčková	Lyžyčková	k1gFnSc1	Lyžyčková
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
Р	Р	k?	Р
С	С	k?	С
Л	Л	k?	Л
<g/>
;	;	kIx,	;
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
Lvov	Lvov	k1gInSc1	Lvov
<g/>
,	,	kIx,	,
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
popová	popový	k2eAgFnSc1d1	popová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
pianistka	pianistka	k1gFnSc1	pianistka
<g/>
,	,	kIx,	,
skladatelka	skladatelka	k1gFnSc1	skladatelka
a	a	k8xC	a
producentka	producentka	k1gFnSc1	producentka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vítězkou	vítězka	k1gFnSc7	vítězka
Eurovision	Eurovision	k1gInSc4	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gFnSc1	Contest
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
World	World	k1gInSc1	World
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
a	a	k8xC	a
držitelkou	držitelka	k1gFnSc7	držitelka
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgNnPc2d1	další
významných	významný	k2eAgNnPc2d1	významné
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
útlého	útlý	k2eAgNnSc2d1	útlé
dětství	dětství	k1gNnSc2	dětství
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaImAgFnS	věnovat
zpěvu	zpěv	k1gInSc3	zpěv
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
souborech	soubor	k1gInPc6	soubor
a	a	k8xC	a
skupinách	skupina	k1gFnPc6	skupina
a	a	k8xC	a
vzdělávala	vzdělávat	k5eAaImAgFnS	vzdělávat
se	se	k3xPyFc4	se
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
diplom	diplom	k1gInSc4	diplom
z	z	k7c2	z
hraní	hraní	k1gNnSc2	hraní
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
z	z	k7c2	z
dirigování	dirigování	k1gNnSc2	dirigování
<g/>
.	.	kIx.	.
</s>
<s>
Umí	umět	k5eAaImIp3nS	umět
dirigovat	dirigovat	k5eAaImF	dirigovat
120	[number]	k4	120
<g/>
členný	členný	k2eAgInSc4d1	členný
orchestr	orchestr	k1gInSc4	orchestr
a	a	k8xC	a
již	již	k6eAd1	již
dirigovala	dirigovat	k5eAaImAgFnS	dirigovat
například	například	k6eAd1	například
ve	v	k7c6	v
Lvovském	lvovský	k2eAgNnSc6d1	lvovské
divadle	divadlo	k1gNnSc6	divadlo
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
baletu	balet	k1gInSc2	balet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jejím	její	k3xOp3gMnSc7	její
manželem	manžel	k1gMnSc7	manžel
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
manažerem	manažer	k1gInSc7	manažer
je	být	k5eAaImIp3nS	být
Aleksandr	Aleksandr	k1gInSc1	Aleksandr
Ksenofontov	Ksenofontovo	k1gNnPc2	Ksenofontovo
<g/>
,	,	kIx,	,
za	za	k7c2	za
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
společně	společně	k6eAd1	společně
provozují	provozovat	k5eAaImIp3nP	provozovat
firmu	firma	k1gFnSc4	firma
Luxen	Luxna	k1gFnPc2	Luxna
Studio	studio	k1gNnSc1	studio
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	se	k3xPyFc4	se
tvorbou	tvorba	k1gFnSc7	tvorba
rádiových	rádiový	k2eAgMnPc2d1	rádiový
a	a	k8xC	a
filmových	filmový	k2eAgMnPc2d1	filmový
teaserů	teaser	k1gMnPc2	teaser
<g/>
.	.	kIx.	.
<g/>
Kariéru	kariéra	k1gFnSc4	kariéra
započala	započnout	k5eAaPmAgFnS	započnout
vítězstvím	vítězství	k1gNnSc7	vítězství
na	na	k7c6	na
soutěži	soutěž	k1gFnSc6	soutěž
Slavjanskyj	Slavjanskyj	k1gMnSc1	Slavjanskyj
Bazar	bazar	k1gInSc4	bazar
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pracovala	pracovat	k5eAaImAgFnS	pracovat
na	na	k7c6	na
televizním	televizní	k2eAgInSc6d1	televizní
projektu	projekt	k1gInSc6	projekt
Vánoce	Vánoce	k1gFnPc1	Vánoce
s	s	k7c7	s
Ruslanou	Ruslaný	k2eAgFnSc7d1	Ruslaný
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
Myť	mýtit	k5eAaImRp2nS	mýtit
vesny	vesna	k1gFnPc1	vesna
<g/>
.	.	kIx.	.
</s>
<s>
Dzvinkyj	Dzvinkyj	k1gFnSc1	Dzvinkyj
viter	vitra	k1gFnPc2	vitra
live	livat	k5eAaPmIp3nS	livat
vydané	vydaný	k2eAgNnSc1d1	vydané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
velmi	velmi	k6eAd1	velmi
pozitivní	pozitivní	k2eAgNnSc4d1	pozitivní
hodnocení	hodnocení	k1gNnSc4	hodnocení
od	od	k7c2	od
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
další	další	k2eAgFnSc6d1	další
práci	práce	k1gFnSc6	práce
se	se	k3xPyFc4	se
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
Huculský	huculský	k2eAgInSc4d1	huculský
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
neodmyslitelně	odmyslitelně	k6eNd1	odmyslitelně
spjat	spjat	k2eAgMnSc1d1	spjat
s	s	k7c7	s
pohořím	pohoří	k1gNnSc7	pohoří
Karpat	Karpaty	k1gInPc2	Karpaty
a	a	k8xC	a
ukrajinskými	ukrajinský	k2eAgFnPc7d1	ukrajinská
tradicemi	tradice	k1gFnPc7	tradice
<g/>
.	.	kIx.	.
<g/>
Slávu	Sláva	k1gFnSc4	Sláva
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
vydáním	vydání	k1gNnSc7	vydání
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Wild	Wild	k1gMnSc1	Wild
Dances	Dances	k1gMnSc1	Dances
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
Dyki	Dyki	k1gNnSc2	Dyki
tanci	tanec	k1gInSc6	tanec
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
Д	Д	k?	Д
т	т	k?	т
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Wild	Wild	k1gMnSc1	Wild
Dances	Dances	k1gMnSc1	Dances
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Divoké	divoký	k2eAgInPc4d1	divoký
tance	tanec	k1gInPc4	tanec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
jen	jen	k9	jen
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
na	na	k7c4	na
500	[number]	k4	500
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
domovině	domovina	k1gFnSc6	domovina
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
obdržela	obdržet	k5eAaPmAgFnS	obdržet
pět	pět	k4xCc4	pět
platinových	platinový	k2eAgFnPc2d1	platinová
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Přineslo	přinést	k5eAaPmAgNnS	přinést
jí	on	k3xPp3gFnSc3	on
také	také	k9	také
vítězství	vítězství	k1gNnSc3	vítězství
na	na	k7c6	na
prestižní	prestižní	k2eAgFnSc6d1	prestižní
soutěži	soutěž	k1gFnSc6	soutěž
Eurovision	Eurovision	k1gInSc4	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gFnSc1	Contest
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
též	též	k6eAd1	též
hlavní	hlavní	k2eAgFnSc7d1	hlavní
hvězdou	hvězda	k1gFnSc7	hvězda
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
ankety	anketa	k1gFnSc2	anketa
Český	český	k2eAgMnSc1d1	český
slavík	slavík	k1gMnSc1	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
písně	píseň	k1gFnPc4	píseň
a	a	k8xC	a
videoklipy	videoklip	k1gInPc4	videoklip
si	se	k3xPyFc3	se
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
komponuje	komponovat	k5eAaImIp3nS	komponovat
i	i	k8xC	i
produkuje	produkovat	k5eAaImIp3nS	produkovat
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
nazývána	nazývat	k5eAaImNgFnS	nazývat
jako	jako	k9	jako
Shakira	Shakira	k1gFnSc1	Shakira
z	z	k7c2	z
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
Kulturák	kulturák	k1gInSc1	kulturák
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c6	o
divoké	divoký	k2eAgFnSc6d1	divoká
krásce	kráska	k1gFnSc6	kráska
z	z	k7c2	z
východu	východ	k1gInSc2	východ
či	či	k8xC	či
zvířecí	zvířecí	k2eAgFnSc7d1	zvířecí
energií	energie	k1gFnSc7	energie
nabité	nabitý	k2eAgFnSc3d1	nabitá
zpěvačce	zpěvačka	k1gFnSc3	zpěvačka
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc3	rok
2008	[number]	k4	2008
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
Wild	Wilda	k1gFnPc2	Wilda
Energy	Energ	k1gMnPc4	Energ
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
duety	duet	k1gInPc4	duet
s	s	k7c7	s
americkými	americký	k2eAgMnPc7d1	americký
zpěváky	zpěvák	k1gMnPc7	zpěvák
T-Painem	T-Pain	k1gMnSc7	T-Pain
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Moon	Moon	k1gInSc1	Moon
of	of	k?	of
Dreams	Dreams	k1gInSc1	Dreams
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Missy	Miss	k1gInPc1	Miss
Elliott	Elliotta	k1gFnPc2	Elliotta
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Girl	girl	k1gFnSc2	girl
That	That	k1gMnSc1	That
Rules	Rules	k1gMnSc1	Rules
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
založené	založený	k2eAgNnSc1d1	založené
na	na	k7c6	na
knize	kniha	k1gFnSc6	kniha
Wild	Wilda	k1gFnPc2	Wilda
Energy	Energ	k1gMnPc4	Energ
<g/>
.	.	kIx.	.
</s>
<s>
Lana	lano	k1gNnPc1	lano
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
show	show	k1gNnSc4	show
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
spojené	spojený	k2eAgFnSc2d1	spojená
poprvé	poprvé	k6eAd1	poprvé
představila	představit	k5eAaPmAgFnS	představit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
na	na	k7c6	na
národním	národní	k2eAgNnSc6d1	národní
kole	kolo	k1gNnSc6	kolo
Eurovize	Eurovize	k1gFnSc2	Eurovize
v	v	k7c6	v
ázerbájdžánském	ázerbájdžánský	k2eAgNnSc6d1	ázerbájdžánské
Baku	Baku	k1gNnSc6	Baku
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
verze	verze	k1gFnSc1	verze
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Amazonka	Amazonka	k1gFnSc1	Amazonka
<g/>
.	.	kIx.	.
<g/>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byla	být	k5eAaImAgFnS	být
pozvána	pozvat	k5eAaPmNgFnS	pozvat
jako	jako	k8xC	jako
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
host	host	k1gMnSc1	host
<g/>
,	,	kIx,	,
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Ruslana	Ruslana	k1gFnSc1	Ruslana
r.	r.	kA	r.
2009	[number]	k4	2009
na	na	k7c6	na
Asijském	asijský	k2eAgInSc6d1	asijský
hudebním	hudební	k2eAgInSc6d1	hudební
festivalu	festival	k1gInSc6	festival
dvě	dva	k4xCgNnPc4	dva
speciální	speciální	k2eAgNnPc4d1	speciální
ocenění	ocenění	k1gNnPc4	ocenění
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
poslankyní	poslankyně	k1gFnSc7	poslankyně
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
<g/>
UNICEF	UNICEF	kA	UNICEF
ji	on	k3xPp3gFnSc4	on
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
Velvyslankyní	velvyslankyně	k1gFnSc7	velvyslankyně
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Aktivně	aktivně	k6eAd1	aktivně
bojuje	bojovat	k5eAaImIp3nS	bojovat
proti	proti	k7c3	proti
obchodování	obchodování	k1gNnSc3	obchodování
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
i	i	k9	i
mnoha	mnoho	k4c2	mnoho
jiným	jiný	k2eAgMnPc3d1	jiný
charitativním	charitativní	k2eAgMnPc3d1	charitativní
projektům	projekt	k1gInPc3	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
také	také	k9	také
iniciativy	iniciativa	k1gFnSc2	iniciativa
na	na	k7c4	na
potírání	potírání	k1gNnSc4	potírání
hudebního	hudební	k2eAgNnSc2d1	hudební
a	a	k8xC	a
softwarového	softwarový	k2eAgNnSc2d1	softwarové
pirátství	pirátství	k1gNnSc2	pirátství
<g/>
.	.	kIx.	.
<g/>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
plynně	plynně	k6eAd1	plynně
rusky	rusky	k6eAd1	rusky
a	a	k8xC	a
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
.	.	kIx.	.
</s>
<s>
Umí	umět	k5eAaImIp3nS	umět
též	též	k9	též
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
učí	učit	k5eAaImIp3nP	učit
se	se	k3xPyFc4	se
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
koníčkem	koníček	k1gInSc7	koníček
je	být	k5eAaImIp3nS	být
extrémní	extrémní	k2eAgInSc1d1	extrémní
sport	sport	k1gInSc1	sport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dětství	dětství	k1gNnSc4	dětství
==	==	k?	==
</s>
</p>
<p>
<s>
Ruslana	Ruslana	k1gFnSc1	Ruslana
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1973	[number]	k4	1973
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
SSR	SSR	kA	SSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
její	její	k3xOp3gMnPc1	její
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
Nina	Nina	k1gFnSc1	Nina
a	a	k8xC	a
Stepanan	Stepanan	k1gInSc1	Stepanan
Lyžyčko	Lyžyčko	k6eAd1	Lyžyčko
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
hudební	hudební	k2eAgFnSc4d1	hudební
kariéru	kariéra	k1gFnSc4	kariéra
určitý	určitý	k2eAgInSc4d1	určitý
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
sama	sám	k3xTgFnSc1	sám
profesionální	profesionální	k2eAgInSc4d1	profesionální
muzikantkou	muzikantka	k1gFnSc7	muzikantka
v	v	k7c6	v
hudebním	hudební	k2eAgNnSc6d1	hudební
tělese	těleso	k1gNnSc6	těleso
Horizon	Horizona	k1gFnPc2	Horizona
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ruslana	Ruslana	k1gFnSc1	Ruslana
začala	začít	k5eAaPmAgFnS	začít
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
experimentální	experimentální	k2eAgFnSc4d1	experimentální
hudební	hudební	k2eAgFnSc4d1	hudební
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
zpívat	zpívat	k5eAaImF	zpívat
v	v	k7c6	v
kapelách	kapela	k1gFnPc6	kapela
a	a	k8xC	a
souborech	soubor	k1gInPc6	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Těmi	ten	k3xDgInPc7	ten
byly	být	k5eAaImAgInP	být
například	například	k6eAd1	například
již	již	k6eAd1	již
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
Horizon	Horizon	k1gInSc1	Horizon
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Orion	orion	k1gInSc1	orion
nebo	nebo	k8xC	nebo
Smile	smil	k1gInSc5	smil
vedený	vedený	k2eAgInSc1d1	vedený
Arkady	Arkad	k1gInPc4	Arkad
Kouzinem	Kouzino	k1gNnSc7	Kouzino
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
tehdy	tehdy	k6eAd1	tehdy
si	se	k3xPyFc3	se
hudbu	hudba	k1gFnSc4	hudba
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
nejoblíbenější	oblíbený	k2eAgFnSc7d3	nejoblíbenější
hračkou	hračka	k1gFnSc7	hračka
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
brzy	brzy	k6eAd1	brzy
stal	stát	k5eAaPmAgInS	stát
mikrofon	mikrofon	k1gInSc1	mikrofon
<g/>
.	.	kIx.	.
</s>
<s>
Domnívá	domnívat	k5eAaImIp3nS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gNnSc4	její
první	první	k4xOgNnSc4	první
vystoupení	vystoupení	k1gNnSc4	vystoupení
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Zlatý	zlatý	k2eAgInSc4d1	zlatý
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
si	se	k3xPyFc3	se
odnesla	odnést	k5eAaPmAgFnS	odnést
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
cenu	cena	k1gFnSc4	cena
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
absolventkou	absolventka	k1gFnSc7	absolventka
Lvovské	lvovský	k2eAgFnSc2d1	Lvovská
národní	národní	k2eAgFnSc2d1	národní
hudební	hudební	k2eAgFnSc2d1	hudební
akademie	akademie	k1gFnSc2	akademie
Mykoly	Mykola	k1gFnSc2	Mykola
Lysenka	lysenka	k1gFnSc1	lysenka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
po	po	k7c6	po
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
matematického	matematický	k2eAgNnSc2d1	matematické
zaměření	zaměření	k1gNnSc2	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
diplomovanou	diplomovaný	k2eAgFnSc7d1	diplomovaná
pianistkou	pianistka	k1gFnSc7	pianistka
a	a	k8xC	a
dirigentkou	dirigentka	k1gFnSc7	dirigentka
symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
zapojí	zapojit	k5eAaPmIp3nP	zapojit
prvky	prvek	k1gInPc1	prvek
moderní	moderní	k2eAgFnSc2d1	moderní
i	i	k8xC	i
symfonické	symfonický	k2eAgFnSc2d1	symfonická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
otce	otec	k1gMnSc2	otec
hrál	hrát	k5eAaImAgMnS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
jeho	jeho	k3xOp3gInSc4	jeho
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
ukrajinských	ukrajinský	k2eAgInPc2d1	ukrajinský
Karpat	Karpaty	k1gInPc2	Karpaty
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
říkají	říkat	k5eAaImIp3nP	říkat
Huculové	Hucul	k1gMnPc1	Hucul
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
starobylá	starobylý	k2eAgFnSc1d1	starobylá
kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
tradice	tradice	k1gFnSc1	tradice
přinesla	přinést	k5eAaPmAgFnS	přinést
Ruslaně	Ruslaně	k1gFnSc4	Ruslaně
mnoho	mnoho	k6eAd1	mnoho
inspirace	inspirace	k1gFnSc2	inspirace
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgInPc4d1	budoucí
hudební	hudební	k2eAgInPc4d1	hudební
projekty	projekt	k1gInPc4	projekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgFnSc1d1	hudební
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Projekt	projekt	k1gInSc1	projekt
Wild	Wild	k1gMnSc1	Wild
Dances	Dances	k1gMnSc1	Dances
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Projekt	projekt	k1gInSc1	projekt
Wild	Wild	k1gInSc1	Wild
Energy	Energ	k1gInPc1	Energ
===	===	k?	===
</s>
</p>
<p>
<s>
Projekt	projekt	k1gInSc1	projekt
s	s	k7c7	s
názvem	název	k1gInSc7	název
Wild	Wild	k1gInSc4	Wild
Energy	Energ	k1gInPc7	Energ
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
vědeckofantastické	vědeckofantastický	k2eAgFnSc6d1	vědeckofantastická
novele	novela	k1gFnSc6	novela
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Maryny	Maryna	k1gFnSc2	Maryna
a	a	k8xC	a
Sergije	Sergije	k1gFnSc2	Sergije
Diačenkových	Diačenkový	k2eAgFnPc2d1	Diačenkový
Wild	Wilda	k1gFnPc2	Wilda
Energy	Energ	k1gMnPc4	Energ
<g/>
.	.	kIx.	.
</s>
<s>
Lana	lano	k1gNnPc1	lano
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zažívá	zažívat	k5eAaImIp3nS	zažívat
globální	globální	k2eAgFnSc4d1	globální
energetickou	energetický	k2eAgFnSc4d1	energetická
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
lidem	lid	k1gInSc7	lid
nedostává	dostávat	k5eNaImIp3nS	dostávat
tzv.	tzv.	kA	tzv.
energie	energie	k1gFnSc2	energie
srdce	srdce	k1gNnSc4	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Lana	lano	k1gNnPc1	lano
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
umělých	umělý	k2eAgMnPc2d1	umělý
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
hledat	hledat	k5eAaImF	hledat
záhadný	záhadný	k2eAgInSc4d1	záhadný
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
dobrodružstvích	dobrodružství	k1gNnPc6	dobrodružství
objevuje	objevovat	k5eAaImIp3nS	objevovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
divoká	divoký	k2eAgFnSc1d1	divoká
energie	energie	k1gFnSc1	energie
přichází	přicházet	k5eAaImIp3nS	přicházet
z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
vlastního	vlastní	k2eAgNnSc2d1	vlastní
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wild	Wild	k6eAd1	Wild
Energy	Energ	k1gMnPc4	Energ
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
umění	umění	k1gNnSc1	umění
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
videoprodukce	videoprodukce	k1gFnSc2	videoprodukce
<g/>
,	,	kIx,	,
literatury	literatura	k1gFnSc2	literatura
i	i	k8xC	i
lidských	lidský	k2eAgInPc2d1	lidský
aspektů	aspekt	k1gInPc2	aspekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
Ruslana	Ruslana	k1gFnSc1	Ruslana
prezentovala	prezentovat	k5eAaBmAgFnS	prezentovat
nový	nový	k2eAgInSc4d1	nový
single	singl	k1gInSc5	singl
a	a	k8xC	a
video	video	k1gNnSc4	video
"	"	kIx"	"
<g/>
Dyka	Dyk	k2eAgFnSc1d1	Dyka
Enerhija	Enerhija	k1gFnSc1	Enerhija
<g/>
"	"	kIx"	"
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
fantasy	fantas	k1gInPc1	fantas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
videoklipu	videoklip	k1gInSc6	videoklip
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
ze	z	k7c2	z
syntetické	syntetický	k2eAgFnSc2d1	syntetická
blonďaté	blonďatý	k2eAgFnSc2d1	blonďatá
dívky	dívka	k1gFnSc2	dívka
jménem	jméno	k1gNnSc7	jméno
Lana	lano	k1gNnPc1	lano
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
divoké	divoký	k2eAgFnSc2d1	divoká
či	či	k8xC	či
ryzí	ryzí	k2eAgFnSc2d1	ryzí
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
vydáno	vydat	k5eAaPmNgNnS	vydat
ukrajinské	ukrajinský	k2eAgNnSc1d1	ukrajinské
album	album	k1gNnSc1	album
Amazonka	Amazonka	k1gFnSc1	Amazonka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
následovala	následovat	k5eAaImAgFnS	následovat
anglická	anglický	k2eAgFnSc1d1	anglická
verze	verze	k1gFnSc1	verze
s	s	k7c7	s
názvem	název	k1gInSc7	název
Wild	Wilda	k1gFnPc2	Wilda
Energy	Energ	k1gMnPc4	Energ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
pokračování	pokračování	k1gNnSc6	pokračování
počítačové	počítačový	k2eAgFnSc2d1	počítačová
hry	hra	k1gFnSc2	hra
Grand	grand	k1gMnSc1	grand
Theft	Theft	k1gInSc1	Theft
Auto	auto	k1gNnSc4	auto
IV	IV	kA	IV
Ruslana	Ruslan	k1gMnSc2	Ruslan
hostuje	hostovat	k5eAaImIp3nS	hostovat
ve	v	k7c6	v
virtuálním	virtuální	k2eAgInSc6d1	virtuální
rádiu	rádius	k1gInSc6	rádius
Vladivostok	Vladivostok	k1gInSc4	Vladivostok
FM	FM	kA	FM
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Wild	Wild	k1gMnSc1	Wild
Dances	Dances	k1gMnSc1	Dances
<g/>
"	"	kIx"	"
figuruje	figurovat	k5eAaImIp3nS	figurovat
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
jeho	on	k3xPp3gInSc2	on
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgFnPc1d1	politická
aktivity	aktivita	k1gFnPc1	aktivita
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2004	[number]	k4	2004
Ruslana	Ruslana	k1gFnSc1	Ruslana
aktivně	aktivně	k6eAd1	aktivně
podporovala	podporovat	k5eAaImAgFnS	podporovat
demokratické	demokratický	k2eAgFnPc4d1	demokratická
snahy	snaha	k1gFnPc4	snaha
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xC	jako
Oranžová	oranžový	k2eAgFnSc1d1	oranžová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
věnovala	věnovat	k5eAaPmAgFnS	věnovat
svou	svůj	k3xOyFgFnSc4	svůj
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Dance	Danka	k1gFnSc6	Danka
with	witha	k1gFnPc2	witha
the	the	k?	the
Wolves	Wolves	k1gMnSc1	Wolves
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Během	během	k7c2	během
sporných	sporný	k2eAgFnPc2d1	sporná
ukrajinských	ukrajinský	k2eAgFnPc2d1	ukrajinská
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
svou	svůj	k3xOyFgFnSc4	svůj
podporu	podpora	k1gFnSc4	podpora
Viktoru	Viktor	k1gMnSc3	Viktor
Juščenkovi	Juščenek	k1gMnSc3	Juščenek
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
představitelů	představitel	k1gMnPc2	představitel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
oslovovali	oslovovat	k5eAaImAgMnP	oslovovat
davy	dav	k1gInPc4	dav
shromážděné	shromážděný	k2eAgInPc4d1	shromážděný
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
Juščenkova	Juščenkův	k2eAgNnSc2d1	Juščenkovo
tvrzení	tvrzení	k1gNnSc2	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
prohra	prohra	k1gFnSc1	prohra
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
byla	být	k5eAaImAgFnS	být
výsledkem	výsledek	k1gInSc7	výsledek
manipulace	manipulace	k1gFnPc4	manipulace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
2006	[number]	k4	2006
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
poslankyní	poslankyně	k1gFnSc7	poslankyně
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
parlamentu	parlament	k1gInSc2	parlament
za	za	k7c4	za
stranu	strana	k1gFnSc4	strana
Naše	náš	k3xOp1gFnSc1	náš
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sociální	sociální	k2eAgFnSc1d1	sociální
aktivity	aktivita	k1gFnPc1	aktivita
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Projekt	projekt	k1gInSc1	projekt
Not	nota	k1gFnPc2	nota
For	forum	k1gNnPc2	forum
Sale	Sal	k1gFnSc2	Sal
proti	proti	k7c3	proti
obchodování	obchodování	k1gNnSc3	obchodování
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
===	===	k?	===
</s>
</p>
<p>
<s>
Ruslana	Ruslana	k1gFnSc1	Ruslana
byla	být	k5eAaImAgFnS	být
organizací	organizace	k1gFnSc7	organizace
UNICEF	UNICEF	kA	UNICEF
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
Velvyslankyní	velvyslankyně	k1gFnSc7	velvyslankyně
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Aktivně	aktivně	k6eAd1	aktivně
se	se	k3xPyFc4	se
zapojuje	zapojovat	k5eAaImIp3nS	zapojovat
do	do	k7c2	do
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
obchodování	obchodování	k1gNnSc3	obchodování
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2008	[number]	k4	2008
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
před	před	k7c7	před
117	[number]	k4	117
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
delegacemi	delegace	k1gFnPc7	delegace
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
proti	proti	k7c3	proti
obchodování	obchodování	k1gNnSc3	obchodování
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
pořádané	pořádaný	k2eAgFnSc2d1	pořádaná
UN	UN	kA	UN
<g/>
.	.	kIx.	.
<g/>
GIFT	GIFT	kA	GIFT
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gMnSc1	The
United	United	k1gMnSc1	United
Nations	Nations	k1gInSc4	Nations
Global	globat	k5eAaImAgMnS	globat
Initiative	Initiativ	k1gInSc5	Initiativ
to	ten	k3xDgNnSc1	ten
Fight	Fight	k2eAgInSc1d1	Fight
Human	Human	k1gInSc1	Human
Trafficking	Trafficking	k1gInSc1	Trafficking
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Not	nota	k1gFnPc2	nota
For	forum	k1gNnPc2	forum
Sale	Sale	k1gFnSc1	Sale
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
Není	být	k5eNaImIp3nS	být
na	na	k7c4	na
prodej	prodej	k1gInSc4	prodej
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
Ruslana	Ruslana	k1gFnSc1	Ruslana
<g/>
,	,	kIx,	,
UN	UN	kA	UN
<g/>
.	.	kIx.	.
<g/>
GIFT	GIFT	kA	GIFT
<g/>
,	,	kIx,	,
Vital	Vital	k1gMnSc1	Vital
Voices	Voices	k1gMnSc1	Voices
a	a	k8xC	a
Evropská	evropský	k2eAgFnSc1d1	Evropská
vysílací	vysílací	k2eAgFnSc1d1	vysílací
unie	unie	k1gFnSc1	unie
spojili	spojit	k5eAaPmAgMnP	spojit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
za	za	k7c4	za
podpory	podpora	k1gFnPc4	podpora
německé	německý	k2eAgFnSc2d1	německá
ambasády	ambasáda	k1gFnSc2	ambasáda
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
natočili	natočit	k5eAaBmAgMnP	natočit
nové	nový	k2eAgNnSc4d1	nové
video	video	k1gNnSc4	video
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
varovat	varovat	k5eAaImF	varovat
všechny	všechen	k3xTgFnPc4	všechen
potenciální	potenciální	k2eAgFnPc4d1	potenciální
oběti	oběť	k1gFnPc4	oběť
obchodování	obchodování	k1gNnSc2	obchodování
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc1	video
je	být	k5eAaImIp3nS	být
dostupné	dostupný	k2eAgFnPc4d1	dostupná
např.	např.	kA	např.
na	na	k7c6	na
oficiální	oficiální	k2eAgFnSc6d1	oficiální
stránce	stránka	k1gFnSc6	stránka
Ruslany	Ruslana	k1gFnSc2	Ruslana
<g/>
.	.	kIx.	.
</s>
<s>
Přibylo	přibýt	k5eAaPmAgNnS	přibýt
tak	tak	k9	tak
ke	k	k7c3	k
dvěma	dva	k4xCgMnPc3	dva
jiným	jiný	k1gMnPc3	jiný
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
vydala	vydat	k5eAaPmAgFnS	vydat
již	již	k9	již
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
sociální	sociální	k2eAgFnPc1d1	sociální
aktivity	aktivita	k1gFnPc1	aktivita
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
konání	konání	k1gNnSc2	konání
Eurovision	Eurovision	k1gInSc4	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gFnSc1	Contest
2005	[number]	k4	2005
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
pořádala	pořádat	k5eAaImAgFnS	pořádat
Ruslana	Ruslana	k1gFnSc1	Ruslana
charitativní	charitativní	k2eAgFnSc1d1	charitativní
koncert	koncert	k1gInSc4	koncert
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
trpící	trpící	k2eAgInSc1d1	trpící
následky	následek	k1gInPc7	následek
černobylské	černobylský	k2eAgFnSc2d1	Černobylská
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
charitativní	charitativní	k2eAgInSc4d1	charitativní
projekt	projekt	k1gInSc4	projekt
spojila	spojit	k5eAaPmAgFnS	spojit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
rockovou	rockový	k2eAgFnSc7d1	rocková
hvězdou	hvězda	k1gFnSc7	hvězda
jménem	jméno	k1gNnSc7	jméno
Peter	Petra	k1gFnPc2	Petra
Maffay	Maffaa	k1gFnSc2	Maffaa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
až	až	k8xS	až
květnu	květen	k1gInSc6	květen
2007	[number]	k4	2007
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
společně	společně	k6eAd1	společně
s	s	k7c7	s
umělci	umělec	k1gMnPc7	umělec
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
14	[number]	k4	14
zemí	zem	k1gFnPc2	zem
čtyřtýdenní	čtyřtýdenní	k2eAgNnSc4d1	čtyřtýdenní
turné	turné	k1gNnSc4	turné
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Výtěžek	výtěžek	k1gInSc1	výtěžek
z	z	k7c2	z
akce	akce	k1gFnSc2	akce
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
věnován	věnován	k2eAgMnSc1d1	věnován
potřebným	potřebný	k2eAgFnPc3d1	potřebná
dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zinscenovala	zinscenovat	k5eAaPmAgFnS	zinscenovat
též	též	k9	též
četné	četný	k2eAgInPc4d1	četný
charitativní	charitativní	k2eAgInPc4d1	charitativní
koncerty	koncert	k1gInPc4	koncert
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
dětských	dětský	k2eAgFnPc2d1	dětská
nemocnic	nemocnice	k1gFnPc2	nemocnice
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
<g/>
,	,	kIx,	,
Lvově	Lvov	k1gInSc6	Lvov
a	a	k8xC	a
Dněpropetrovsku	Dněpropetrovsko	k1gNnSc6	Dněpropetrovsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
projektem	projekt	k1gInSc7	projekt
Wild	Wild	k1gMnSc1	Wild
Energy	Energ	k1gInPc4	Energ
podporuje	podporovat	k5eAaImIp3nS	podporovat
využívání	využívání	k1gNnSc1	využívání
obnovitelných	obnovitelný	k2eAgInPc2d1	obnovitelný
zdrojů	zdroj	k1gInPc2	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Propaguje	propagovat	k5eAaImIp3nS	propagovat
energii	energie	k1gFnSc4	energie
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
větru	vítr	k1gInSc2	vítr
jako	jako	k8xC	jako
druh	druh	k1gInSc1	druh
energetické	energetický	k2eAgFnSc2d1	energetická
soběstačnosti	soběstačnost	k1gFnSc2	soběstačnost
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
plynoucí	plynoucí	k2eAgNnSc4d1	plynoucí
z	z	k7c2	z
globálních	globální	k2eAgFnPc2d1	globální
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byly	být	k5eAaImAgFnP	být
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
oblasti	oblast	k1gFnPc1	oblast
západní	západní	k2eAgFnSc2d1	západní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
zasaženy	zasažen	k2eAgInPc4d1	zasažen
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
povodněmi	povodeň	k1gFnPc7	povodeň
<g/>
,	,	kIx,	,
založila	založit	k5eAaPmAgFnS	založit
Ruslana	Ruslana	k1gFnSc1	Ruslana
koordinační	koordinační	k2eAgFnSc1d1	koordinační
a	a	k8xC	a
humanitární	humanitární	k2eAgNnSc1d1	humanitární
centrum	centrum	k1gNnSc1	centrum
Karpaty	Karpaty	k1gInPc7	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Povodně	povodeň	k1gFnPc1	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
SOS	sos	k1gInSc1	sos
<g/>
!	!	kIx.	!
</s>
<s>
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
seznam	seznam	k1gInSc4	seznam
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
nouzi	nouze	k1gFnSc6	nouze
<g/>
,	,	kIx,	,
zajistit	zajistit	k5eAaPmF	zajistit
naléhavou	naléhavý	k2eAgFnSc4d1	naléhavá
humanitární	humanitární	k2eAgFnSc4d1	humanitární
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
a	a	k8xC	a
předávat	předávat	k5eAaImF	předávat
obětem	oběť	k1gFnPc3	oběť
povodní	povodeň	k1gFnPc2	povodeň
dary	dar	k1gInPc1	dar
od	od	k7c2	od
veřejnosti	veřejnost	k1gFnSc2	veřejnost
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Komerční	komerční	k2eAgFnSc2d1	komerční
aktivity	aktivita	k1gFnSc2	aktivita
==	==	k?	==
</s>
</p>
<p>
<s>
Nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
byla	být	k5eAaImAgFnS	být
Ruslana	Ruslana	k1gFnSc1	Ruslana
tváří	tvář	k1gFnPc2	tvář
společnosti	společnost	k1gFnSc2	společnost
Garnier	Garnira	k1gFnPc2	Garnira
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2008	[number]	k4	2008
jí	on	k3xPp3gFnSc3	on
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
spolupráci	spolupráce	k1gFnSc4	spolupráce
firma	firma	k1gFnSc1	firma
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Oréal	Oréal	k1gMnSc1	Oréal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Ruslana	Ruslana	k1gFnSc1	Ruslana
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
Blíženců	blíženec	k1gMnPc2	blíženec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vysoká	vysoká	k1gFnSc1	vysoká
164	[number]	k4	164
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
49	[number]	k4	49
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
hnědé	hnědý	k2eAgNnSc1d1	hnědé
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
kaštanové	kaštanový	k2eAgInPc4d1	kaštanový
vlasy	vlas	k1gInPc4	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
oficiální	oficiální	k2eAgFnSc6d1	oficiální
stránce	stránka	k1gFnSc6	stránka
se	se	k3xPyFc4	se
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
jako	jako	k8xC	jako
charakterem	charakter	k1gInSc7	charakter
divoká	divoký	k2eAgFnSc1d1	divoká
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
ráda	rád	k2eAgFnSc1d1	ráda
ultramarínově	ultramarínově	k6eAd1	ultramarínově
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
Karpaty	Karpaty	k1gInPc4	Karpaty
a	a	k8xC	a
extrémní	extrémní	k2eAgInSc1d1	extrémní
sport	sport	k1gInSc1	sport
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
její	její	k3xOp3gFnSc4	její
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
známé	známý	k2eAgFnSc6d1	známá
soutěži	soutěž	k1gFnSc6	soutěž
družstev	družstvo	k1gNnPc2	družstvo
Klíče	klíč	k1gInPc1	klíč
od	od	k7c2	od
pevnosti	pevnost	k1gFnSc2	pevnost
Boyard	Boyarda	k1gFnPc2	Boyarda
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc2d1	vodní
pevnosti	pevnost	k1gFnSc2	pevnost
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
soutěžící	soutěžící	k1gMnPc1	soutěžící
plní	plnit	k5eAaImIp3nP	plnit
množství	množství	k1gNnSc4	množství
rozmanitých	rozmanitý	k2eAgInPc2d1	rozmanitý
originálních	originální	k2eAgInPc2d1	originální
úkolů	úkol	k1gInPc2	úkol
<g/>
,	,	kIx,	,
či	či	k8xC	či
na	na	k7c6	na
klání	klání	k1gNnSc6	klání
Bitva	bitva	k1gFnSc1	bitva
ukrajinských	ukrajinský	k2eAgNnPc2d1	ukrajinské
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
Б	Б	k?	Б
У	У	k?	У
М	М	k?	М
<g/>
)	)	kIx)	)
v	v	k7c6	v
Argentinském	argentinský	k2eAgInSc6d1	argentinský
Buenos	Buenos	k1gInSc1	Buenos
Aires	Aires	k1gInSc1	Aires
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
projekt	projekt	k1gInSc4	projekt
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
Inter	Intero	k1gNnPc2	Intero
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
soutěže	soutěž	k1gFnSc2	soutěž
Wipeout	Wipeout	k1gMnSc1	Wipeout
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
změřily	změřit	k5eAaPmAgInP	změřit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
skupiny	skupina	k1gFnSc2	skupina
zastupující	zastupující	k2eAgFnSc1d1	zastupující
různá	různý	k2eAgFnSc1d1	různá
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
města	město	k1gNnSc2	město
složené	složený	k2eAgNnSc4d1	složené
vždy	vždy	k6eAd1	vždy
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
celebrity	celebrita	k1gFnSc2	celebrita
(	(	kIx(	(
<g/>
kapitána	kapitán	k1gMnSc2	kapitán
<g/>
)	)	kIx)	)
a	a	k8xC	a
čtyř	čtyři	k4xCgMnPc2	čtyři
dalších	další	k2eAgMnPc2d1	další
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
roztodivných	roztodivný	k2eAgFnPc6d1	roztodivná
překážkových	překážkový	k2eAgFnPc6d1	překážková
drahách	draha	k1gFnPc6	draha
<g/>
.	.	kIx.	.
</s>
<s>
Ruslana	Ruslana	k1gFnSc1	Ruslana
vedla	vést	k5eAaImAgFnS	vést
tým	tým	k1gInSc4	tým
ze	z	k7c2	z
Lvova	Lvov	k1gInSc2	Lvov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
a	a	k8xC	a
nominace	nominace	k1gFnSc1	nominace
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Alba	album	k1gNnSc2	album
===	===	k?	===
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
Myť	mýtit	k5eAaImRp2nS	mýtit
vesny	vesna	k1gFnPc5	vesna
<g/>
.	.	kIx.	.
</s>
<s>
Dzvinkyj	Dzvinkyj	k1gMnSc1	Dzvinkyj
viter	viter	k1gMnSc1	viter
live	livat	k5eAaPmIp3nS	livat
(	(	kIx(	(
<g/>
М	М	k?	М
в	в	k?	в
<g/>
.	.	kIx.	.
Д	Д	k?	Д
в	в	k?	в
live	live	k1gInSc1	live
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
Ostanně	Ostanně	k1gFnPc2	Ostanně
Rizdvo	Rizdvo	k1gNnSc1	Rizdvo
90	[number]	k4	90
<g/>
-ch	h	k?	-ch
(	(	kIx(	(
<g/>
О	О	k?	О
Р	Р	k?	Р
90	[number]	k4	90
<g/>
-x	-x	k?	-x
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
Najkrašče	Najkrašč	k1gInSc2	Najkrašč
(	(	kIx(	(
<g/>
Н	Н	k?	Н
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
Dobryj	Dobryj	k1gMnSc1	Dobryj
večir	večir	k1gMnSc1	večir
tobi	tobi	k6eAd1	tobi
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
Д	Д	k?	Д
в	в	k?	в
т	т	k?	т
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
Dyki	Dyki	k1gNnSc2	Dyki
tanci	tanec	k1gInSc6	tanec
(	(	kIx(	(
<g/>
Д	Д	k?	Д
т	т	k?	т
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Dyki	Dyki	k1gNnSc2	Dyki
tanci	tanec	k1gInSc6	tanec
+	+	kIx~	+
Jevrobonus	Jevrobonus	k1gInSc1	Jevrobonus
(	(	kIx(	(
<g/>
Д	Д	k?	Д
т	т	k?	т
+	+	kIx~	+
Є	Є	k?	Є
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Wild	Wild	k1gMnSc1	Wild
Dances	Dances	k1gMnSc1	Dances
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
Club	club	k1gInSc1	club
<g/>
'	'	kIx"	'
<g/>
in	in	k?	in
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
Amazonka	Amazonka	k1gFnSc1	Amazonka
(	(	kIx(	(
<g/>
А	А	k?	А
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
Wild	Wild	k1gMnSc1	Wild
Energy	Energ	k1gMnPc4	Energ
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
Euphoria	Euphorium	k1gNnSc2	Euphorium
(	(	kIx(	(
<g/>
EY-fori-YA	EYori-YA	k1gFnSc1	EY-fori-YA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Singly	singl	k1gInPc4	singl
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
CD	CD	kA	CD
====	====	k?	====
</s>
</p>
<p>
<s>
Singlová	singlový	k2eAgNnPc1d1	singlové
alba	album	k1gNnPc1	album
vydaná	vydaný	k2eAgNnPc1d1	vydané
oficiálně	oficiálně	k6eAd1	oficiálně
na	na	k7c6	na
samostatných	samostatný	k2eAgNnPc2d1	samostatné
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rádiové	rádiový	k2eAgInPc1d1	rádiový
singly	singl	k1gInPc1	singl
====	====	k?	====
</s>
</p>
<p>
<s>
Singly	singl	k1gInPc1	singl
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
pojetí	pojetí	k1gNnSc6	pojetí
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
rádiové	rádiový	k2eAgFnPc1d1	rádiová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Wild	Wild	k1gMnSc1	Wild
Dances	Dances	k1gMnSc1	Dances
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Dance	Danka	k1gFnSc3	Danka
with	witha	k1gFnPc2	witha
the	the	k?	the
Wolves	Wolvesa	k1gFnPc2	Wolvesa
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
The	The	k1gMnSc5	The
Same	Sam	k1gMnSc5	Sam
Star	Star	kA	Star
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Ring	ring	k1gInSc4	ring
Dance	Danka	k1gFnSc3	Danka
with	with	k1gInSc1	with
the	the	k?	the
Wolves	Wolves	k1gInSc1	Wolves
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Moon	Moon	k1gInSc1	Moon
of	of	k?	of
Dreams	Dreams	k1gInSc1	Dreams
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Ukrajinské	ukrajinský	k2eAgInPc1d1	ukrajinský
singly	singl	k1gInPc1	singl
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Znaju	Znaju	k1gFnSc1	Znaju
ja	ja	k?	ja
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Kolomyjka	kolomyjka	k1gFnSc1	kolomyjka
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Oj	oj	k1gInSc4	oj
<g/>
,	,	kIx,	,
zahraj	zahrát	k5eAaPmRp2nS	zahrát
my	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
muzyčeňku	muzyčeněk	k1gInSc3	muzyčeněk
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Skažy	Skaž	k2eAgFnPc1d1	Skaž
meni	men	k1gFnPc1	men
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
The	The	k1gMnSc5	The
Same	Sam	k1gMnSc5	Sam
Star	Star	kA	Star
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
"	"	kIx"	"
<g/>
V	v	k7c6	v
rytme	rytmus	k1gInSc5	rytmus
serdca	serdc	k2eAgNnPc1d1	serdc
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Dyka	Dyk	k2eAgFnSc1d1	Dyka
Enerhija	Enerhija	k1gFnSc1	Enerhija
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Vidlunňa	Vidlunň	k2eAgFnSc1d1	Vidlunň
mrij	mrij	k1gFnSc1	mrij
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Vohoň	Vohoň	k1gFnSc1	Vohoň
čy	čy	k?	čy
lid	lid	k1gInSc1	lid
(	(	kIx(	(
<g/>
Vse	Vse	k1gMnSc1	Vse
ne	ne	k9	ne
te	te	k?	te
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Dykyj	Dykyj	k1gMnSc1	Dykyj
anhel	anhel	k1gMnSc1	anhel
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Ja	Ja	k?	Ja
jdu	jít	k5eAaImIp1nS	jít
za	za	k7c4	za
toboju	toboju	k5eAaPmIp1nS	toboju
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
DVD	DVD	kA	DVD
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Videoklipy	videoklip	k1gInPc4	videoklip
==	==	k?	==
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
"	"	kIx"	"
<g/>
Ty	ty	k3xPp2nSc1	ty
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Т	Т	k?	Т
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
"	"	kIx"	"
<g/>
Myť	mýtit	k5eAaImRp2nS	mýtit
Vesny	Vesna	k1gFnSc2	Vesna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
М	М	k?	М
в	в	k?	в
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
"	"	kIx"	"
<g/>
Svitanok	Svitanok	k1gInSc1	Svitanok
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
С	С	k?	С
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
"	"	kIx"	"
<g/>
Balada	balada	k1gFnSc1	balada
pro	pro	k7c4	pro
pryncesu	pryncesa	k1gFnSc4	pryncesa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Б	Б	k?	Б
п	п	k?	п
п	п	k?	п
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
"	"	kIx"	"
<g/>
Kolyskova	Kolyskův	k2eAgFnSc1d1	Kolyskův
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
К	К	k?	К
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
"	"	kIx"	"
<g/>
Znaju	Znaju	k1gMnSc1	Znaju
ja	ja	k?	ja
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
З	З	k?	З
я	я	k?	я
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
"	"	kIx"	"
<g/>
Proščanňa	Proščanňa	k1gMnSc1	Proščanňa
z	z	k7c2	z
disko	disko	k1gNnSc2	disko
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
П	П	k?	П
з	з	k?	з
д	д	k?	д
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
"	"	kIx"	"
<g/>
Dobryj	Dobryj	k1gMnSc1	Dobryj
večir	večir	k1gMnSc1	večir
<g/>
,	,	kIx,	,
tobi	tobi	k6eAd1	tobi
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Д	Д	k?	Д
в	в	k?	в
<g/>
,	,	kIx,	,
т	т	k?	т
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
"	"	kIx"	"
<g/>
Kolomyjka	kolomyjka	k1gFnSc1	kolomyjka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
К	К	k?	К
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
"	"	kIx"	"
<g/>
Oj	oj	k1gInSc4	oj
<g/>
,	,	kIx,	,
zahraj	zahrát	k5eAaPmRp2nS	zahrát
my	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
muzyčenku	muzyčenka	k1gFnSc4	muzyčenka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
О	О	k?	О
<g/>
,	,	kIx,	,
з	з	k?	з
<g/>
,	,	kIx,	,
м	м	k?	м
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
"	"	kIx"	"
<g/>
Wild	Wild	k1gMnSc1	Wild
Dances	Dances	k1gMnSc1	Dances
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
"	"	kIx"	"
<g/>
Dance	Danka	k1gFnSc3	Danka
with	witha	k1gFnPc2	witha
the	the	k?	the
Wolves	Wolves	k1gMnSc1	Wolves
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
"	"	kIx"	"
<g/>
Ring	ring	k1gInSc4	ring
Dance	Danka	k1gFnSc3	Danka
with	with	k1gInSc1	with
the	the	k?	the
Wolves	Wolves	k1gInSc1	Wolves
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
"	"	kIx"	"
<g/>
The	The	k1gMnSc5	The
Same	Sam	k1gMnSc5	Sam
Star	Star	kA	Star
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
"	"	kIx"	"
<g/>
V	v	k7c6	v
rytmi	ryt	k1gFnPc7	ryt
sercja	sercj	k1gInSc2	sercj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
В	В	k?	В
р	р	k?	р
с	с	k?	с
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
"	"	kIx"	"
<g/>
Dyka	Dyka	k1gMnSc1	Dyka
Enerhija	Enerhija	k1gMnSc1	Enerhija
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Д	Д	k?	Д
е	е	k?	е
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
"	"	kIx"	"
<g/>
Vidlunňa	Vidlunňa	k1gMnSc1	Vidlunňa
mrij	mrij	k1gMnSc1	mrij
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
В	В	k?	В
м	м	k?	м
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
verze	verze	k1gFnSc1	verze
videoklipu	videoklip	k1gInSc2	videoklip
"	"	kIx"	"
<g/>
Moon	Moon	k1gInSc1	Moon
of	of	k?	of
Dreams	Dreams	k1gInSc1	Dreams
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
"	"	kIx"	"
<g/>
Moon	Moon	k1gMnSc1	Moon
of	of	k?	of
Dreams	Dreams	k1gInSc1	Dreams
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
T-Pain	T-Pain	k1gInSc1	T-Pain
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
"	"	kIx"	"
<g/>
Vohoň	Vohoň	k1gMnSc1	Vohoň
čy	čy	k?	čy
lid	lid	k1gInSc1	lid
(	(	kIx(	(
<g/>
Vse	Vse	k1gMnSc1	Vse
ne	ne	k9	ne
te	te	k?	te
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
В	В	k?	В
ч	ч	k?	ч
л	л	k?	л
(	(	kIx(	(
<g/>
В	В	k?	В
н	н	k?	н
т	т	k?	т
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
"	"	kIx"	"
<g/>
Dykyj	Dykyj	k1gMnSc1	Dykyj
anhel	anhel	k1gMnSc1	anhel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Д	Д	k?	Д
а	а	k?	а
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
verze	verze	k1gFnSc1	verze
videoklipu	videoklip	k1gInSc2	videoklip
"	"	kIx"	"
<g/>
Silent	Silent	k1gMnSc1	Silent
Angel	angel	k1gMnSc1	angel
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
"	"	kIx"	"
<g/>
Silent	Silent	k1gMnSc1	Silent
Angel	angel	k1gMnSc1	angel
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
-	-	kIx~	-
"	"	kIx"	"
<g/>
WOW	WOW	kA	WOW
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
-	-	kIx~	-
"	"	kIx"	"
<g/>
Sha-la-la	Shaaa	k1gFnSc1	Sha-la-la
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
-	-	kIx~	-
"	"	kIx"	"
<g/>
Davaj	Davaj	k1gMnSc1	Davaj
<g/>
,	,	kIx,	,
graj	graj	k1gMnSc1	graj
(	(	kIx(	(
<g/>
Ogo	Ogo	k1gMnSc1	Ogo
<g/>
,	,	kIx,	,
Ogo	Ogo	k1gMnSc1	Ogo
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
-	-	kIx~	-
"	"	kIx"	"
<g/>
Muj	Muj	k?	Muj
brat	brat	k1gMnSc1	brat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
-	-	kIx~	-
"	"	kIx"	"
<g/>
EY-fori-JA	EYori-JA	k1gFnSc1	EY-fori-JA
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
-	-	kIx~	-
"	"	kIx"	"
<g/>
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
-	-	kIx~	-
"	"	kIx"	"
<g/>
WOW	WOW	kA	WOW
<g/>
"	"	kIx"	"
,	,	kIx,	,
"	"	kIx"	"
<g/>
Sha-la-la	Shaaa	k1gFnSc1	Sha-la-la
<g/>
"	"	kIx"	"
,	,	kIx,	,
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Boo	boa	k1gFnSc5	boa
<g/>
"	"	kIx"	"
,	,	kIx,	,
"	"	kIx"	"
<g/>
This	This	k1gInSc1	This
Is	Is	k1gFnSc2	Is
Euphoria	Euphorium	k1gNnSc2	Euphorium
<g/>
"	"	kIx"	"
a	a	k8xC	a
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
verze	verze	k1gFnSc1	verze
klipů	klip	k1gInPc2	klip
songů	song	k1gInPc2	song
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
text	text	k1gInSc1	text
uvolněný	uvolněný	k2eAgInSc1d1	uvolněný
pod	pod	k7c7	pod
licencí	licence	k1gFnSc7	licence
GFDL	GFDL	kA	GFDL
ze	z	k7c2	z
stránky	stránka	k1gFnSc2	stránka
na	na	k7c6	na
webu	web	k1gInSc6	web
Kulturák	kulturák	k1gInSc1	kulturák
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Ruslana	Ruslana	k1gFnSc1	Ruslana
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Ruslana	Ruslana	k1gFnSc1	Ruslana
discography	discographa	k1gFnSc2	discographa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Wild	Wild	k6eAd1	Wild
Energy	Energ	k1gInPc1	Energ
<g/>
.	.	kIx.	.
</s>
<s>
Lana	lano	k1gNnPc1	lano
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ruslana	Ruslan	k1gMnSc2	Ruslan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Ruslana	Ruslan	k1gMnSc2	Ruslan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Ruslana	Ruslana	k1gFnSc1	Ruslana
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
korejsky	korejsky	k6eAd1	korejsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
Ruslany	Ruslana	k1gFnSc2	Ruslana
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
fanklub	fanklub	k1gInSc1	fanklub
Ruslany	Ruslana	k1gFnSc2	Ruslana
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
fórum	fórum	k1gNnSc1	fórum
Ruslany	Ruslana	k1gFnSc2	Ruslana
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
Ruslana	Ruslana	k1gFnSc1	Ruslana
TV	TV	kA	TV
–	–	k?	–
oficiální	oficiální	k2eAgInSc4d1	oficiální
internetový	internetový	k2eAgInSc4d1	internetový
video	video	k1gNnSc4	video
kanál	kanál	k1gInSc4	kanál
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
RuslanaTube	RuslanaTub	k1gInSc5	RuslanaTub
–	–	k?	–
Youtube	Youtub	k1gInSc5	Youtub
kanál	kanál	k1gInSc4	kanál
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
