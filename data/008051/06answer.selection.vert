<s>
Oceány	oceán	k1gInPc1	oceán
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgMnPc4d1	známý
nejlépe	dobře	k6eAd3	dobře
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
5	[number]	k4	5
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
