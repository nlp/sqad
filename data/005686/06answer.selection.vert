<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
Valpurga	Valpurga	k1gFnSc1	Valpurga
Amálie	Amálie	k1gFnSc1	Amálie
Kristýna	Kristýna	k1gFnSc1	Kristýna
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Maria	Mario	k1gMnSc4	Mario
Theresia	Theresius	k1gMnSc4	Theresius
Walburga	Walburg	k1gMnSc4	Walburg
Amalia	Amalius	k1gMnSc4	Amalius
Christiana	Christian	k1gMnSc4	Christian
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1717	[number]	k4	1717
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1780	[number]	k4	1780
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
rodu	rod	k1gInSc6	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
byla	být	k5eAaImAgFnS	být
arcivévodkyní	arcivévodkyně	k1gFnSc7	arcivévodkyně
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
<g/>
,	,	kIx,	,
královnou	královna	k1gFnSc7	královna
uherskou	uherský	k2eAgFnSc7d1	uherská
(	(	kIx(	(
<g/>
1741	[number]	k4	1741
<g/>
–	–	k?	–
<g/>
1780	[number]	k4	1780
<g/>
)	)	kIx)	)
a	a	k8xC	a
českou	český	k2eAgFnSc7d1	Česká
(	(	kIx(	(
<g/>
1743	[number]	k4	1743
<g/>
–	–	k?	–
<g/>
1780	[number]	k4	1780
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
markraběnkou	markraběnka	k1gFnSc7	markraběnka
moravskou	moravský	k2eAgFnSc7d1	Moravská
atd.	atd.	kA	atd.
</s>
