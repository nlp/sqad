arcivévodkyní	arcivévodkyně	k1gFnSc7	arcivévodkyně
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
<g/>
,	,	kIx,	,
královnou	královna	k1gFnSc7	královna
uherskou	uherský	k2eAgFnSc7d1	uherská
(	(	kIx(	(
<g/>
1741	[number]	k4	1741
<g/>
–	–	k?	–
<g/>
1780	[number]	k4	1780
<g/>
)	)	kIx)	)
a	a	k8xC	a
českou	český	k2eAgFnSc7d1	Česká
(	(	kIx(	(
<g/>
1743	[number]	k4	1743
<g/>
–	–	k?	–
<g/>
1780	[number]	k4	1780
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
markraběnkou	markraběnka	k1gFnSc7	markraběnka
moravskou	moravský	k2eAgFnSc7d1	Moravská
