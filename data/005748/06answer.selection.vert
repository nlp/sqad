<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2 700	[number]	k4	2 700
lety	let	k1gInPc7	let
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
římské	římský	k2eAgFnSc2d1	římská
tradice	tradice	k1gFnSc2	tradice
roku	rok	k1gInSc2	rok
753	[number]	k4	753
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgNnPc3d3	nejstarší
městům	město	k1gNnPc3	město
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
