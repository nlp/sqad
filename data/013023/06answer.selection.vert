<s>
Protože	protože	k8xS	protože
úhlový	úhlový	k2eAgInSc1d1	úhlový
průměr	průměr	k1gInSc1	průměr
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
půl	půl	k1xP	půl
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gMnSc2	on
Venuše	Venuše	k1gFnSc1	Venuše
během	během	k7c2	během
běžné	běžný	k2eAgFnSc2d1	běžná
konjunkce	konjunkce	k1gFnSc2	konjunkce
procházet	procházet	k5eAaImF	procházet
i	i	k9	i
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
18	[number]	k4	18
průměrů	průměr	k1gInPc2	průměr
slunečního	sluneční	k2eAgInSc2d1	sluneční
kotouče	kotouč	k1gInSc2	kotouč
<g/>
.	.	kIx.	.
</s>
