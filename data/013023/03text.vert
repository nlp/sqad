<p>
<s>
Přechod	přechod	k1gInSc1	přechod
Venuše	Venuše	k1gFnSc2	Venuše
(	(	kIx(	(
<g/>
též	též	k9	též
tranzit	tranzit	k1gInSc1	tranzit
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
sluneční	sluneční	k2eAgInSc4d1	sluneční
disk	disk	k1gInSc4	disk
je	být	k5eAaImIp3nS	být
astronomická	astronomický	k2eAgFnSc1d1	astronomická
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
planeta	planeta	k1gFnSc1	planeta
Venuše	Venuše	k1gFnSc2	Venuše
projde	projít	k5eAaPmIp3nS	projít
přímo	přímo	k6eAd1	přímo
mezi	mezi	k7c7	mezi
Sluncem	slunce	k1gNnSc7	slunce
a	a	k8xC	a
Zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
zakryje	zakrýt	k5eAaPmIp3nS	zakrýt
tak	tak	k6eAd1	tak
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
slunečního	sluneční	k2eAgInSc2d1	sluneční
kotouče	kotouč	k1gInSc2	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
během	během	k7c2	během
přechodu	přechod	k1gInSc2	přechod
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
jako	jako	k8xS	jako
malý	malý	k2eAgInSc1d1	malý
černý	černý	k2eAgInSc1d1	černý
disk	disk	k1gInSc1	disk
pohybující	pohybující	k2eAgFnSc2d1	pohybující
se	se	k3xPyFc4	se
napříč	napříč	k7c7	napříč
slunečním	sluneční	k2eAgInSc7d1	sluneční
kotoučem	kotouč	k1gInSc7	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
Přechody	přechod	k1gInPc1	přechod
Venuše	Venuše	k1gFnSc2	Venuše
obvykle	obvykle	k6eAd1	obvykle
trvají	trvat	k5eAaImIp3nP	trvat
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
přechod	přechod	k1gInSc1	přechod
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
trval	trvat	k5eAaImAgInS	trvat
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
zatmění	zatmění	k1gNnSc2	zatmění
Slunce	slunce	k1gNnSc1	slunce
Měsícem	měsíc	k1gInSc7	měsíc
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Venuše	Venuše	k1gFnSc1	Venuše
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
jeví	jevit	k5eAaImIp3nS	jevit
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgNnSc1d2	menší
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc1	její
průměr	průměr	k1gInSc1	průměr
téměř	téměř	k6eAd1	téměř
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
průměr	průměr	k1gInSc1	průměr
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
astronomie	astronomie	k1gFnSc2	astronomie
se	se	k3xPyFc4	se
přechod	přechod	k1gInSc1	přechod
Venuše	Venuše	k1gFnSc2	Venuše
využíval	využívat	k5eAaImAgInS	využívat
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
sluneční	sluneční	k2eAgFnSc2d1	sluneční
paralaxy	paralaxa	k1gFnSc2	paralaxa
a	a	k8xC	a
následnému	následný	k2eAgInSc3d1	následný
výpočtu	výpočet	k1gInSc3	výpočet
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
Země	zem	k1gFnSc2	zem
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
<g/>
Přechody	přechod	k1gInPc1	přechod
Venuše	Venuše	k1gFnSc2	Venuše
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejvzácnější	vzácný	k2eAgInPc4d3	nejvzácnější
předpověditelné	předpověditelný	k2eAgInPc4d1	předpověditelný
astronomické	astronomický	k2eAgInPc4d1	astronomický
úkazy	úkaz	k1gInPc4	úkaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
lze	lze	k6eAd1	lze
každých	každý	k3xTgNnPc2	každý
243	[number]	k4	243
let	léto	k1gNnPc2	léto
pozorovat	pozorovat	k5eAaImF	pozorovat
dvě	dva	k4xCgFnPc4	dva
dvojice	dvojice	k1gFnPc4	dvojice
přechodů	přechod	k1gInPc2	přechod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dva	dva	k4xCgInPc1	dva
přechody	přechod	k1gInPc1	přechod
od	od	k7c2	od
sebe	se	k3xPyFc2	se
dělí	dělit	k5eAaImIp3nP	dělit
8	[number]	k4	8
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
po	po	k7c6	po
nichž	jenž	k3xRgNnPc6	jenž
následují	následovat	k5eAaImIp3nP	následovat
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
pauzy	pauza	k1gFnPc4	pauza
se	s	k7c7	s
střídavou	střídavý	k2eAgFnSc7d1	střídavá
délkou	délka	k1gFnSc7	délka
121,5	[number]	k4	121,5
a	a	k8xC	a
105,5	[number]	k4	105,5
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
dvojice	dvojice	k1gFnSc1	dvojice
přechodů	přechod	k1gInPc2	přechod
byla	být	k5eAaImAgFnS	být
pozorována	pozorován	k2eAgFnSc1d1	pozorována
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgFnSc1d3	nejbližší
další	další	k2eAgFnSc1d1	další
dvojice	dvojice	k1gFnSc1	dvojice
přechodů	přechod	k1gInPc2	přechod
se	se	k3xPyFc4	se
odehraje	odehrát	k5eAaPmIp3nS	odehrát
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2117	[number]	k4	2117
a	a	k8xC	a
prosinci	prosinec	k1gInSc6	prosinec
2125	[number]	k4	2125
<g/>
.	.	kIx.	.
<g/>
Přechod	přechod	k1gInSc1	přechod
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
bezpečně	bezpečně	k6eAd1	bezpečně
sledovat	sledovat	k5eAaImF	sledovat
za	za	k7c4	za
dodržení	dodržení	k1gNnSc4	dodržení
stejných	stejný	k2eAgNnPc2d1	stejné
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
částečného	částečný	k2eAgNnSc2d1	částečné
zatmění	zatmění	k1gNnSc2	zatmění
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
zářivého	zářivý	k2eAgInSc2d1	zářivý
povrchu	povrch	k1gInSc2	povrch
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
fotosféry	fotosféra	k1gFnSc2	fotosféra
<g/>
)	)	kIx)	)
nechráněným	chráněný	k2eNgInSc7d1	nechráněný
zrakem	zrak	k1gInSc7	zrak
může	moct	k5eAaImIp3nS	moct
rychle	rychle	k6eAd1	rychle
vyústit	vyústit	k5eAaPmF	vyústit
ve	v	k7c4	v
vážné	vážný	k2eAgFnPc4d1	vážná
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
trvalé	trvalý	k2eAgNnSc4d1	trvalé
poškození	poškození	k1gNnSc4	poškození
zraku	zrak	k1gInSc2	zrak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konjunkce	konjunkce	k1gFnSc2	konjunkce
==	==	k?	==
</s>
</p>
<p>
<s>
Rovina	rovina	k1gFnSc1	rovina
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Venuše	Venuše	k1gFnSc2	Venuše
svírá	svírat	k5eAaImIp3nS	svírat
s	s	k7c7	s
rovinou	rovina	k1gFnSc7	rovina
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Země	zem	k1gFnSc2	zem
úhel	úhel	k1gInSc1	úhel
3,4	[number]	k4	3,4
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
dolních	dolní	k2eAgFnPc2d1	dolní
konjunkcí	konjunkce	k1gFnPc2	konjunkce
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
planety	planeta	k1gFnPc1	planeta
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
téže	tenže	k3xDgFnSc6	tenže
straně	strana	k1gFnSc6	strana
Slunce	slunce	k1gNnSc1	slunce
<g/>
)	)	kIx)	)
Venuše	Venuše	k1gFnSc1	Venuše
obvykle	obvykle	k6eAd1	obvykle
projde	projít	k5eAaPmIp3nS	projít
nad	nad	k7c7	nad
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
slunečním	sluneční	k2eAgInSc7d1	sluneční
kotoučem	kotouč	k1gInSc7	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
planety	planeta	k1gFnSc2	planeta
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
9,6	[number]	k4	9,6
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
úhlový	úhlový	k2eAgInSc1d1	úhlový
průměr	průměr	k1gInSc1	průměr
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
půl	půl	k1xP	půl
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gMnSc2	on
Venuše	Venuše	k1gFnSc1	Venuše
během	během	k7c2	během
běžné	běžný	k2eAgFnSc2d1	běžná
konjunkce	konjunkce	k1gFnSc2	konjunkce
procházet	procházet	k5eAaImF	procházet
i	i	k9	i
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
18	[number]	k4	18
průměrů	průměr	k1gInPc2	průměr
slunečního	sluneční	k2eAgInSc2d1	sluneční
kotouče	kotouč	k1gInSc2	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
viditelný	viditelný	k2eAgInSc4d1	viditelný
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
konjunkce	konjunkce	k1gFnSc1	konjunkce
Venuše	Venuše	k1gFnSc2	Venuše
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
nastane	nastat	k5eAaPmIp3nS	nastat
poblíž	poblíž	k7c2	poblíž
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
uzlů	uzel	k1gInPc2	uzel
její	její	k3xOp3gFnSc2	její
dráhy	dráha	k1gFnSc2	dráha
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dráha	dráha	k1gFnSc1	dráha
Venuše	Venuše	k1gFnSc2	Venuše
protíná	protínat	k5eAaImIp3nS	protínat
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
rovinu	rovina	k1gFnSc4	rovina
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
ekliptiku	ekliptika	k1gFnSc4	ekliptika
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
v	v	k7c4	v
něm.	něm.	k?	něm.
<g/>
Přechody	přechod	k1gInPc4	přechod
Venuše	Venuše	k1gFnSc2	Venuše
sledují	sledovat	k5eAaImIp3nP	sledovat
243	[number]	k4	243
<g/>
letý	letý	k2eAgInSc4d1	letý
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
něj	on	k3xPp3gNnSc2	on
lze	lze	k6eAd1	lze
obvykle	obvykle	k6eAd1	obvykle
pozorovat	pozorovat	k5eAaImF	pozorovat
dvě	dva	k4xCgFnPc4	dva
dvojice	dvojice	k1gFnPc4	dvojice
přechodů	přechod	k1gInPc2	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
dvojice	dvojice	k1gFnSc1	dvojice
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
oddělená	oddělený	k2eAgFnSc1d1	oddělená
8	[number]	k4	8
lety	léto	k1gNnPc7	léto
a	a	k8xC	a
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
dvojicemi	dvojice	k1gFnPc7	dvojice
uběhne	uběhnout	k5eAaPmIp3nS	uběhnout
vždy	vždy	k6eAd1	vždy
střídavě	střídavě	k6eAd1	střídavě
121,5	[number]	k4	121,5
a	a	k8xC	a
105,5	[number]	k4	105,5
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
243	[number]	k4	243
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
243	[number]	k4	243
pozemských	pozemský	k2eAgNnPc2d1	pozemské
siderických	siderický	k2eAgNnPc2d1	siderické
let	léto	k1gNnPc2	léto
má	mít	k5eAaImIp3nS	mít
88	[number]	k4	88
757,3	[number]	k4	757,3
dní	den	k1gInPc2	den
a	a	k8xC	a
současně	současně	k6eAd1	současně
395	[number]	k4	395
siderických	siderický	k2eAgNnPc2d1	siderické
let	léto	k1gNnPc2	léto
Venuše	Venuše	k1gFnSc2	Venuše
má	mít	k5eAaImIp3nS	mít
88	[number]	k4	88
756,9	[number]	k4	756,9
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
periodách	perioda	k1gFnPc6	perioda
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
obě	dva	k4xCgFnPc1	dva
planety	planeta	k1gFnPc1	planeta
vrací	vracet	k5eAaImIp3nP	vracet
do	do	k7c2	do
přibližně	přibližně	k6eAd1	přibližně
stejné	stejný	k2eAgFnSc2d1	stejná
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
doba	doba	k1gFnSc1	doba
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
také	také	k9	také
152	[number]	k4	152
synodickým	synodický	k2eAgFnPc3d1	synodická
periodám	perioda	k1gFnPc3	perioda
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
<g/>
Současné	současný	k2eAgFnSc2d1	současná
periody	perioda	k1gFnSc2	perioda
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
105,5	[number]	k4	105,5
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
121,5	[number]	k4	121,5
a	a	k8xC	a
8	[number]	k4	8
let	léto	k1gNnPc2	léto
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
243	[number]	k4	243
<g/>
letého	letý	k2eAgInSc2d1	letý
cyklu	cyklus	k1gInSc2	cyklus
jsou	být	k5eAaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
malým	malý	k2eAgInSc7d1	malý
rozdílem	rozdíl	k1gInSc7	rozdíl
v	v	k7c6	v
délkách	délka	k1gFnPc6	délka
siderických	siderický	k2eAgFnPc6d1	siderická
let	let	k1gInSc4	let
obou	dva	k4xCgFnPc2	dva
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1518	[number]	k4	1518
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
cyklu	cyklus	k1gInSc2	cyklus
střídaly	střídat	k5eAaImAgFnP	střídat
tři	tři	k4xCgFnPc1	tři
periody	perioda	k1gFnPc1	perioda
o	o	k7c6	o
délkách	délka	k1gFnPc6	délka
8	[number]	k4	8
<g/>
,	,	kIx,	,
113,5	[number]	k4	113,5
a	a	k8xC	a
121,5	[number]	k4	121,5
let	léto	k1gNnPc2	léto
a	a	k8xC	a
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
546	[number]	k4	546
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
8	[number]	k4	8
přechodů	přechod	k1gInPc2	přechod
za	za	k7c7	za
sebou	se	k3xPyFc7	se
vždy	vždy	k6eAd1	vždy
po	po	k7c6	po
121,5	[number]	k4	121,5
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
cyklu	cyklus	k1gInSc2	cyklus
skončí	skončit	k5eAaPmIp3nS	skončit
roku	rok	k1gInSc2	rok
2846	[number]	k4	2846
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
period	perioda	k1gFnPc2	perioda
opět	opět	k6eAd1	opět
sníží	snížit	k5eAaPmIp3nS	snížit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
délky	délka	k1gFnSc2	délka
budou	být	k5eAaImBp3nP	být
105,5	[number]	k4	105,5
<g/>
,	,	kIx,	,
129,5	[number]	k4	129,5
a	a	k8xC	a
8	[number]	k4	8
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
243	[number]	k4	243
<g/>
letý	letý	k2eAgInSc4d1	letý
cyklus	cyklus	k1gInSc4	cyklus
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
stabilní	stabilní	k2eAgFnSc1d1	stabilní
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
počet	počet	k1gInSc1	počet
přechodů	přechod	k1gInPc2	přechod
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nastávají	nastávat	k5eAaImIp3nP	nastávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kontakty	kontakt	k1gInPc4	kontakt
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
Venuše	Venuše	k1gFnSc2	Venuše
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
čtyři	čtyři	k4xCgMnPc4	čtyři
tzv.	tzv.	kA	tzv.
kontakty	kontakt	k1gInPc1	kontakt
planety	planeta	k1gFnSc2	planeta
se	s	k7c7	s
slunečním	sluneční	k2eAgInSc7d1	sluneční
kotoučem	kotouč	k1gInSc7	kotouč
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
kontakt	kontakt	k1gInSc1	kontakt
<g/>
:	:	kIx,	:
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
zcela	zcela	k6eAd1	zcela
mimo	mimo	k7c4	mimo
sluneční	sluneční	k2eAgInSc4d1	sluneční
disk	disk	k1gInSc4	disk
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
poprvé	poprvé	k6eAd1	poprvé
dotkla	dotknout	k5eAaPmAgFnS	dotknout
<g/>
,	,	kIx,	,
a	a	k8xC	a
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
dovnitř	dovnitř	k6eAd1	dovnitř
</s>
</p>
<p>
<s>
Druhý	druhý	k4xOgInSc1	druhý
kontakt	kontakt	k1gInSc1	kontakt
<g/>
:	:	kIx,	:
Venuše	Venuše	k1gFnSc1	Venuše
se	se	k3xPyFc4	se
ocitá	ocitat	k5eAaImIp3nS	ocitat
zcela	zcela	k6eAd1	zcela
uvnitř	uvnitř	k7c2	uvnitř
slunečního	sluneční	k2eAgInSc2d1	sluneční
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
přestává	přestávat	k5eAaImIp3nS	přestávat
se	se	k3xPyFc4	se
dotýkat	dotýkat	k5eAaImF	dotýkat
jeho	jeho	k3xOp3gInPc4	jeho
okraje	okraj	k1gInPc4	okraj
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
směrem	směr	k1gInSc7	směr
dovnitř	dovnitř	k6eAd1	dovnitř
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgInSc1	třetí
kontakt	kontakt	k1gInSc1	kontakt
<g/>
:	:	kIx,	:
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
zcela	zcela	k6eAd1	zcela
uvnitř	uvnitř	k7c2	uvnitř
slunečního	sluneční	k2eAgInSc2d1	sluneční
disku	disk	k1gInSc2	disk
a	a	k8xC	a
právě	právě	k6eAd1	právě
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
dotkla	dotknout	k5eAaPmAgFnS	dotknout
jeho	jeho	k3xOp3gInPc4	jeho
okraje	okraj	k1gInPc4	okraj
<g/>
,	,	kIx,	,
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
</s>
</p>
<p>
<s>
Čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
kontakt	kontakt	k1gInSc1	kontakt
<g/>
:	:	kIx,	:
Venuše	Venuše	k1gFnSc1	Venuše
zcela	zcela	k6eAd1	zcela
opouští	opouštět	k5eAaImIp3nS	opouštět
sluneční	sluneční	k2eAgNnSc1d1	sluneční
diskDalším	diskDalší	k2eAgInSc7d1	diskDalší
význačným	význačný	k2eAgInSc7d1	význačný
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
střed	střed	k1gInSc1	střed
či	či	k8xC	či
maximální	maximální	k2eAgFnSc1d1	maximální
fáze	fáze	k1gFnSc1	fáze
přechodu	přechod	k1gInSc2	přechod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Venuše	Venuše	k1gFnSc1	Venuše
ocitá	ocitat	k5eAaImIp3nS	ocitat
uprostřed	uprostřed	k7c2	uprostřed
své	svůj	k3xOyFgFnSc2	svůj
cesty	cesta	k1gFnSc2	cesta
napříč	napříč	k7c7	napříč
slunečním	sluneční	k2eAgInSc7d1	sluneční
kotoučem	kotouč	k1gInSc7	kotouč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Starověká	starověký	k2eAgNnPc4d1	starověké
a	a	k8xC	a
středověká	středověký	k2eAgNnPc4d1	středověké
pozorování	pozorování	k1gNnPc4	pozorování
==	==	k?	==
</s>
</p>
<p>
<s>
Pohyby	pohyb	k1gInPc1	pohyb
planety	planeta	k1gFnSc2	planeta
Venuše	Venuše	k1gFnSc2	Venuše
zaznamenávali	zaznamenávat	k5eAaImAgMnP	zaznamenávat
již	již	k6eAd1	již
starověcí	starověký	k2eAgMnPc1d1	starověký
řečtí	řecký	k2eAgMnPc1d1	řecký
<g/>
,	,	kIx,	,
egyptští	egyptský	k2eAgMnPc1d1	egyptský
<g/>
,	,	kIx,	,
babylonští	babylonský	k2eAgMnPc1d1	babylonský
i	i	k8xC	i
čínští	čínský	k2eAgMnPc1d1	čínský
hvězdáři	hvězdář	k1gMnPc1	hvězdář
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Řekové	Řek	k1gMnPc1	Řek
považovali	považovat	k5eAaImAgMnP	považovat
večerní	večerní	k2eAgInSc4d1	večerní
a	a	k8xC	a
ranní	ranní	k2eAgInPc4d1	ranní
východy	východ	k1gInPc4	východ
Venuše	Venuše	k1gFnSc2	Venuše
za	za	k7c4	za
různá	různý	k2eAgNnPc4d1	různé
tělesa	těleso	k1gNnPc4	těleso
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Večernici	večernice	k1gFnSc4	večernice
nazývali	nazývat	k5eAaImAgMnP	nazývat
Hesperos	Hesperosa	k1gFnPc2	Hesperosa
a	a	k8xC	a
Jitřenku	Jitřenka	k1gFnSc4	Jitřenka
Fosforos	Fosforosa	k1gFnPc2	Fosforosa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
prvního	první	k4xOgMnSc2	první
Řeka	Řek	k1gMnSc2	Řek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgMnSc1d1	považován
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
<g/>
.	.	kIx.	.
</s>
<s>
Hérakleidés	Hérakleidés	k6eAd1	Hérakleidés
z	z	k7c2	z
Pontu	Pont	k1gInSc2	Pont
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
prochází	procházet	k5eAaImIp3nS	procházet
někdy	někdy	k6eAd1	někdy
nad	nad	k7c7	nad
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
pod	pod	k7c7	pod
slunečním	sluneční	k2eAgInSc7d1	sluneční
kotoučem	kotouč	k1gInSc7	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
historikové	historik	k1gMnPc1	historik
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyvozují	vyvozovat	k5eAaImIp3nP	vyvozovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
obíhá	obíhat	k5eAaImIp3nS	obíhat
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
okolo	okolo	k7c2	okolo
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
však	však	k9	však
nenasvědčuje	nasvědčovat	k5eNaImIp3nS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
kultur	kultura	k1gFnPc2	kultura
věděla	vědět	k5eAaImAgFnS	vědět
o	o	k7c6	o
přechodech	přechod	k1gInPc6	přechod
Venuše	Venuše	k1gFnSc2	Venuše
přes	přes	k7c4	přes
sluneční	sluneční	k2eAgInSc4d1	sluneční
disk	disk	k1gInSc4	disk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
byla	být	k5eAaImAgFnS	být
známá	známý	k2eAgFnSc1d1	známá
též	též	k6eAd1	též
americkým	americký	k2eAgFnPc3d1	americká
předkolumbovským	předkolumbovský	k2eAgFnPc3d1	předkolumbovská
civilizacím	civilizace	k1gFnPc3	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Mayové	Mayová	k1gFnSc2	Mayová
ji	on	k3xPp3gFnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
Noh	noh	k1gMnSc1	noh
Ek	Ek	k1gMnSc1	Ek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
také	také	k9	také
Xux	Xux	k1gMnSc1	Xux
Ek	Ek	k1gMnSc1	Ek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vosí	vosí	k2eAgFnSc1d1	vosí
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
ztotožňovali	ztotožňovat	k5eAaImAgMnP	ztotožňovat
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
Kukulkánem	Kukulkán	k1gMnSc7	Kukulkán
(	(	kIx(	(
<g/>
Aztékům	Azték	k1gMnPc3	Azték
pak	pak	k6eAd1	pak
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Quetzalcoatl	Quetzalcoatl	k1gMnSc1	Quetzalcoatl
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
Opeřený	opeřený	k2eAgMnSc1d1	opeřený
had	had	k1gMnSc1	had
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tzv.	tzv.	kA	tzv.
Drážďanském	drážďanský	k2eAgNnSc6d1	drážďanské
kodexu	kodex	k1gInSc6	kodex
je	být	k5eAaImIp3nS	být
zmapován	zmapovat	k5eAaPmNgInS	zmapovat
celý	celý	k2eAgInSc1d1	celý
cyklus	cyklus	k1gInSc1	cyklus
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
ovšem	ovšem	k9	ovšem
nenasvědčuje	nasvědčovat	k5eNaImIp3nS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
Mayové	May	k1gMnPc1	May
měli	mít	k5eAaImAgMnP	mít
také	také	k9	také
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c6	o
přechodech	přechod	k1gInPc6	přechod
planety	planeta	k1gFnSc2	planeta
přes	přes	k7c4	přes
sluneční	sluneční	k2eAgInSc4d1	sluneční
disk	disk	k1gInSc4	disk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
potvrzené	potvrzený	k2eAgFnPc1d1	potvrzená
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
pozorování	pozorování	k1gNnSc6	pozorování
přechodu	přechod	k1gInSc2	přechod
Venuše	Venuše	k1gFnSc2	Venuše
máme	mít	k5eAaImIp1nP	mít
od	od	k7c2	od
středověkého	středověký	k2eAgMnSc2d1	středověký
perského	perský	k2eAgMnSc2d1	perský
učence	učenec	k1gMnSc2	učenec
Ibn	Ibn	k1gFnSc1	Ibn
Síná	Síná	k1gFnSc1	Síná
<g/>
,	,	kIx,	,
známějšího	známý	k2eAgInSc2d2	známější
pod	pod	k7c7	pod
polatinštěným	polatinštěný	k2eAgNnSc7d1	polatinštěný
jménem	jméno	k1gNnSc7	jméno
Avicenna	Avicenno	k1gNnSc2	Avicenno
(	(	kIx(	(
<g/>
980	[number]	k4	980
<g/>
–	–	k?	–
<g/>
1037	[number]	k4	1037
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
údajně	údajně	k6eAd1	údajně
Venuši	Venuše	k1gFnSc4	Venuše
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
jako	jako	k8xC	jako
skvrnu	skvrna	k1gFnSc4	skvrna
na	na	k7c6	na
slunečním	sluneční	k2eAgInSc6d1	sluneční
disku	disk	k1gInSc6	disk
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
údajném	údajný	k2eAgNnSc6d1	údajné
pozorování	pozorování	k1gNnSc6	pozorování
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
interpretaci	interpretace	k1gFnSc4	interpretace
panují	panovat	k5eAaImIp3nP	panovat
pochybnosti	pochybnost	k1gFnPc1	pochybnost
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
vyloučit	vyloučit	k5eAaPmF	vyloučit
to	ten	k3xDgNnSc4	ten
nelze	lze	k6eNd1	lze
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
skutečně	skutečně	k6eAd1	skutečně
potvrzená	potvrzený	k2eAgNnPc4d1	potvrzené
pozorování	pozorování	k1gNnPc4	pozorování
jevu	jev	k1gInSc2	jev
však	však	k9	však
pocházejí	pocházet	k5eAaImIp3nP	pocházet
až	až	k9	až
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Novověká	novověký	k2eAgNnPc1d1	novověké
pozorování	pozorování	k1gNnPc1	pozorování
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
zájem	zájem	k1gInSc1	zájem
vědců	vědec	k1gMnPc2	vědec
o	o	k7c4	o
přechod	přechod	k1gInSc4	přechod
Venuše	Venuše	k1gFnSc2	Venuše
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
vzácnosti	vzácnost	k1gFnSc2	vzácnost
jevu	jev	k1gInSc2	jev
<g/>
,	,	kIx,	,
motivován	motivován	k2eAgInSc1d1	motivován
především	především	k9	především
snahou	snaha	k1gFnSc7	snaha
vypočítat	vypočítat	k5eAaPmF	vypočítat
velikost	velikost	k1gFnSc1	velikost
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
astronomové	astronom	k1gMnPc1	astronom
už	už	k6eAd1	už
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
uměli	umět	k5eAaImAgMnP	umět
vypočítat	vypočítat	k5eAaPmF	vypočítat
poměrnou	poměrný	k2eAgFnSc4d1	poměrná
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
planet	planeta	k1gFnPc2	planeta
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
astronomických	astronomický	k2eAgFnPc6d1	astronomická
jednotkách	jednotka	k1gFnPc6	jednotka
<g/>
,	,	kIx,	,
absolutní	absolutní	k2eAgFnSc1d1	absolutní
hodnota	hodnota	k1gFnSc1	hodnota
této	tento	k3xDgFnSc2	tento
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
ještě	ještě	k6eAd1	ještě
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1631	[number]	k4	1631
a	a	k8xC	a
1639	[number]	k4	1639
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
přechod	přechod	k1gInSc4	přechod
Venuše	Venuše	k1gFnSc2	Venuše
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
údajů	údaj	k1gInPc2	údaj
o	o	k7c6	o
oběžných	oběžný	k2eAgFnPc6d1	oběžná
dráhách	dráha	k1gFnPc6	dráha
planet	planeta	k1gFnPc2	planeta
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
Rudolfinských	rudolfinský	k2eAgFnPc6d1	Rudolfinská
tabulkách	tabulka	k1gFnPc6	tabulka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1627	[number]	k4	1627
vyplývalo	vyplývat	k5eAaImAgNnS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
planeta	planeta	k1gFnSc1	planeta
přejde	přejít	k5eAaPmIp3nS	přejít
přes	přes	k7c4	přes
sluneční	sluneční	k2eAgInSc4d1	sluneční
kotouč	kotouč	k1gInSc4	kotouč
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1631	[number]	k4	1631
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
také	také	k9	také
přechod	přechod	k1gInSc4	přechod
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
odehrát	odehrát	k5eAaPmF	odehrát
přesně	přesně	k6eAd1	přesně
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1631	[number]	k4	1631
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yRgMnSc1	který
úspěšně	úspěšně	k6eAd1	úspěšně
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
astronom	astronom	k1gMnSc1	astronom
Pierre	Pierr	k1gInSc5	Pierr
Gassendi	Gassend	k1gMnPc1	Gassend
<g/>
.	.	kIx.	.
</s>
<s>
Keplerovy	Keplerův	k2eAgInPc1d1	Keplerův
výpočty	výpočet	k1gInPc1	výpočet
však	však	k9	však
nebyly	být	k5eNaImAgInP	být
dostatečně	dostatečně	k6eAd1	dostatečně
přesné	přesný	k2eAgFnPc1d1	přesná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přechod	přechod	k1gInSc1	přechod
Venuše	Venuše	k1gFnSc2	Venuše
nebude	být	k5eNaImBp3nS	být
viditelný	viditelný	k2eAgMnSc1d1	viditelný
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
oblastí	oblast	k1gFnPc2	oblast
žádný	žádný	k1gMnSc1	žádný
astronom	astronom	k1gMnSc1	astronom
nevypravil	vypravit	k5eNaPmAgMnS	vypravit
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Kepler	Kepler	k1gMnSc1	Kepler
se	se	k3xPyFc4	se
tohoto	tento	k3xDgNnSc2	tento
data	datum	k1gNnSc2	datum
nedožil	dožít	k5eNaPmAgInS	dožít
–	–	k?	–
zemřel	zemřít	k5eAaPmAgInS	zemřít
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1630	[number]	k4	1630
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
před	před	k7c7	před
očekávanou	očekávaný	k2eAgFnSc7d1	očekávaná
událostí	událost	k1gFnSc7	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgNnPc4	první
vědecká	vědecký	k2eAgNnPc4d1	vědecké
pozorování	pozorování	k1gNnPc4	pozorování
přechodu	přechod	k1gInSc2	přechod
Venuše	Venuše	k1gFnSc2	Venuše
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1639	[number]	k4	1639
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
Much	moucha	k1gFnPc2	moucha
Hoole	Hoole	k1gFnSc2	Hoole
poblíž	poblíž	k7c2	poblíž
Prestonu	Preston	k1gInSc2	Preston
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Anglii	Anglie	k1gFnSc6	Anglie
sledoval	sledovat	k5eAaImAgMnS	sledovat
Jeremiah	Jeremiah	k1gInSc4	Jeremiah
Horrocks	Horrocksa	k1gFnPc2	Horrocksa
a	a	k8xC	a
nedaleko	daleko	k6eNd1	daleko
odtud	odtud	k6eAd1	odtud
ze	z	k7c2	z
Salfordu	Salford	k1gInSc2	Salford
poblíž	poblíž	k7c2	poblíž
Manchesteru	Manchester	k1gInSc2	Manchester
další	další	k2eAgMnSc1d1	další
anglický	anglický	k2eAgMnSc1d1	anglický
astronom	astronom	k1gMnSc1	astronom
William	William	k1gInSc4	William
Crabtree	Crabtre	k1gInSc2	Crabtre
<g/>
.	.	kIx.	.
</s>
<s>
Kepler	Kepler	k1gMnSc1	Kepler
přitom	přitom	k6eAd1	přitom
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
přechody	přechod	k1gInPc4	přechod
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1631	[number]	k4	1631
a	a	k8xC	a
1761	[number]	k4	1761
<g/>
;	;	kIx,	;
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1639	[number]	k4	1639
předpovídal	předpovídat	k5eAaImAgMnS	předpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc2	Venuše
sluneční	sluneční	k2eAgInSc1d1	sluneční
kotouč	kotouč	k1gInSc1	kotouč
těsně	těsně	k6eAd1	těsně
mine	minout	k5eAaImIp3nS	minout
<g/>
.	.	kIx.	.
</s>
<s>
Horrocks	Horrocks	k6eAd1	Horrocks
však	však	k9	však
Keplerovy	Keplerův	k2eAgInPc4d1	Keplerův
výpočty	výpočet	k1gInPc4	výpočet
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Venuše	Venuše	k1gFnSc2	Venuše
opravil	opravit	k5eAaPmAgInS	opravit
a	a	k8xC	a
uvědomil	uvědomit	k5eAaPmAgInS	uvědomit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
přechody	přechod	k1gInPc1	přechod
této	tento	k3xDgFnSc2	tento
planety	planeta	k1gFnSc2	planeta
přes	přes	k7c4	přes
sluneční	sluneční	k2eAgInSc4d1	sluneční
disk	disk	k1gInSc4	disk
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
ve	v	k7c6	v
dvojicích	dvojice	k1gFnPc6	dvojice
<g/>
,	,	kIx,	,
navzájem	navzájem	k6eAd1	navzájem
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
vzdálených	vzdálený	k2eAgNnPc2d1	vzdálené
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
Horrocks	Horrocks	k1gInSc1	Horrocks
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
Crabtree	Crabtre	k1gFnSc2	Crabtre
stali	stát	k5eAaPmAgMnP	stát
jedinými	jediný	k2eAgMnPc7d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
událost	událost	k1gFnSc4	událost
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
si	se	k3xPyFc3	se
Horrocks	Horrocks	k1gInSc1	Horrocks
nebyl	být	k5eNaImAgInS	být
zcela	zcela	k6eAd1	zcela
jist	jist	k2eAgMnSc1d1	jist
přesným	přesný	k2eAgInSc7d1	přesný
časem	čas	k1gInSc7	čas
<g/>
,	,	kIx,	,
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
přechod	přechod	k1gInSc1	přechod
začne	začít	k5eAaPmIp3nS	začít
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
bez	bez	k7c2	bez
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
poškození	poškození	k1gNnSc2	poškození
zraku	zrak	k1gInSc2	zrak
<g/>
,	,	kIx,	,
zaostřil	zaostřit	k5eAaPmAgMnS	zaostřit
pomocí	pomocí	k7c2	pomocí
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
dalekohledu	dalekohled	k1gInSc2	dalekohled
obraz	obraz	k1gInSc1	obraz
Slunce	slunce	k1gNnSc2	slunce
na	na	k7c4	na
malý	malý	k2eAgInSc4d1	malý
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
bylo	být	k5eAaImAgNnS	být
zataženo	zatažen	k2eAgNnSc1d1	zataženo
<g/>
,	,	kIx,	,
strávil	strávit	k5eAaPmAgMnS	strávit
u	u	k7c2	u
dalekohledu	dalekohled	k1gInSc2	dalekohled
většinu	většina	k1gFnSc4	většina
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
mu	on	k3xPp3gMnSc3	on
přálo	přát	k5eAaImAgNnS	přát
štěstí	štěstí	k1gNnSc4	štěstí
a	a	k8xC	a
v	v	k7c6	v
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
před	před	k7c7	před
západem	západ	k1gInSc7	západ
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc1	slunce
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
mezi	mezi	k7c7	mezi
mraky	mrak	k1gInPc7	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Crabtreemu	Crabtreema	k1gFnSc4	Crabtreema
počasí	počasí	k1gNnSc2	počasí
a	a	k8xC	a
snad	snad	k9	snad
také	také	k9	také
radost	radost	k1gFnSc4	radost
ze	z	k7c2	z
zážitku	zážitek	k1gInSc2	zážitek
neumožnily	umožnit	k5eNaPmAgFnP	umožnit
provést	provést	k5eAaPmF	provést
žádná	žádný	k3yNgNnPc1	žádný
měření	měření	k1gNnPc1	měření
<g/>
,	,	kIx,	,
Horrocks	Horrocks	k1gInSc1	Horrocks
dokázal	dokázat	k5eAaPmAgInS	dokázat
změřit	změřit	k5eAaPmF	změřit
úhlovou	úhlový	k2eAgFnSc4d1	úhlová
velikost	velikost	k1gFnSc4	velikost
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
mylné	mylný	k2eAgFnSc2d1	mylná
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
velikost	velikost	k1gFnSc1	velikost
planet	planeta	k1gFnPc2	planeta
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrná	k1gFnSc1	úměrná
jejich	jejich	k3xOp3gFnSc2	jejich
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
byly	být	k5eAaImAgInP	být
jejich	jejich	k3xOp3gFnPc4	jejich
úhlové	úhlový	k2eAgFnPc4d1	úhlová
velikosti	velikost	k1gFnPc4	velikost
stejné	stejný	k2eAgFnPc4d1	stejná
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
odhadl	odhadnout	k5eAaPmAgMnS	odhadnout
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
Země	zem	k1gFnSc2	zem
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
na	na	k7c4	na
59,4	[number]	k4	59,4
milionu	milion	k4xCgInSc2	milion
mil	míle	k1gFnPc2	míle
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
95,6	[number]	k4	95,6
milionu	milion	k4xCgInSc2	milion
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
údaj	údaj	k1gInSc4	údaj
mnohem	mnohem	k6eAd1	mnohem
přesnější	přesný	k2eAgInSc4d2	přesnější
než	než	k8xS	než
veškeré	veškerý	k3xTgInPc4	veškerý
předchozí	předchozí	k2eAgInPc4d1	předchozí
odhady	odhad	k1gInPc4	odhad
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
od	od	k7c2	od
správné	správný	k2eAgFnSc2d1	správná
hodnoty	hodnota	k1gFnSc2	hodnota
149,6	[number]	k4	149,6
milionu	milion	k4xCgInSc2	milion
km	km	kA	km
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
významně	významně	k6eAd1	významně
lišil	lišit	k5eAaImAgMnS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Horrocksova	Horrocksův	k2eAgNnPc1d1	Horrocksův
pozorování	pozorování	k1gNnPc1	pozorování
byla	být	k5eAaImAgNnP	být
zveřejněna	zveřejnit	k5eAaPmNgNnP	zveřejnit
ve	v	k7c6	v
spise	spis	k1gInSc6	spis
Venus	Venus	k1gMnSc1	Venus
in	in	k?	in
Sole	sol	k1gInSc5	sol
Visa	viso	k1gNnSc2	viso
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Venuše	Venuše	k1gFnSc2	Venuše
před	před	k7c7	před
Sluncem	slunce	k1gNnSc7	slunce
spatřená	spatřený	k2eAgNnPc4d1	spatřené
<g/>
)	)	kIx)	)
až	až	k8xS	až
roku	rok	k1gInSc2	rok
1662	[number]	k4	1662
<g/>
,	,	kIx,	,
dlouho	dlouho	k6eAd1	dlouho
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1761	[number]	k4	1761
a	a	k8xC	a
1769	[number]	k4	1769
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1761	[number]	k4	1761
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
přechod	přechod	k1gInSc4	přechod
Venuše	Venuše	k1gFnSc2	Venuše
v	v	k7c4	v
Sankt	Sankt	k1gInSc4	Sankt
Petěrburgu	Petěrburg	k1gInSc2	Petěrburg
Michail	Michail	k1gMnSc1	Michail
Lomonosov	Lomonosovo	k1gNnPc2	Lomonosovo
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
lomu	lom	k1gInSc2	lom
slunečních	sluneční	k2eAgInPc2d1	sluneční
paprsků	paprsek	k1gInPc2	paprsek
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
přechodu	přechod	k1gInSc2	přechod
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
kolem	kolem	k7c2	kolem
Venuše	Venuše	k1gFnSc2	Venuše
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
ještě	ještě	k6eAd1	ještě
nepřišla	přijít	k5eNaPmAgFnS	přijít
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
se	s	k7c7	s
slunečním	sluneční	k2eAgInSc7d1	sluneční
kotoučem	kotouč	k1gInSc7	kotouč
<g/>
,	,	kIx,	,
světlý	světlý	k2eAgInSc1d1	světlý
prstenec	prstenec	k1gInSc1	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
prstenec	prstenec	k1gInSc1	prstenec
pak	pak	k6eAd1	pak
pozoroval	pozorovat	k5eAaImAgInS	pozorovat
i	i	k9	i
po	po	k7c6	po
třetím	třetí	k4xOgInSc6	třetí
kontaktu	kontakt	k1gInSc6	kontakt
na	na	k7c6	na
konci	konec	k1gInSc6	konec
přechodu	přechod	k1gInSc2	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Lomonosov	Lomonosov	k1gInSc1	Lomonosov
správně	správně	k6eAd1	správně
usoudil	usoudit	k5eAaPmAgInS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prstenec	prstenec	k1gInSc1	prstenec
lze	lze	k6eAd1	lze
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
pouze	pouze	k6eAd1	pouze
existencí	existence	k1gFnSc7	existence
atmosféry	atmosféra	k1gFnSc2	atmosféra
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvojici	dvojice	k1gFnSc4	dvojice
přechodů	přechod	k1gInPc2	přechod
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1761	[number]	k4	1761
a	a	k8xC	a
1769	[number]	k4	1769
využili	využít	k5eAaPmAgMnP	využít
astronomové	astronom	k1gMnPc1	astronom
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
přesnější	přesný	k2eAgFnSc2d2	přesnější
hodnoty	hodnota	k1gFnSc2	hodnota
astronomické	astronomický	k2eAgFnSc2d1	astronomická
jednotky	jednotka	k1gFnSc2	jednotka
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
sluneční	sluneční	k2eAgFnSc2d1	sluneční
paralaxy	paralaxa	k1gFnSc2	paralaxa
a	a	k8xC	a
Keplerova	Keplerův	k2eAgInSc2d1	Keplerův
třetího	třetí	k4xOgInSc2	třetí
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
metodu	metoda	k1gFnSc4	metoda
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgMnS	popsat
skotský	skotský	k2eAgMnSc1d1	skotský
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
astronom	astronom	k1gMnSc1	astronom
James	James	k1gMnSc1	James
Gregory	Gregor	k1gMnPc7	Gregor
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Optica	Optic	k2eAgFnSc1d1	Optic
Promota	Promota	k1gFnSc1	Promota
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1663	[number]	k4	1663
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
přesném	přesný	k2eAgNnSc6d1	přesné
změření	změření	k1gNnSc6	změření
malého	malý	k2eAgInSc2d1	malý
časového	časový	k2eAgInSc2d1	časový
rozdílu	rozdíl	k1gInSc2	rozdíl
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
nebo	nebo	k8xC	nebo
koncích	konec	k1gInPc6	konec
přechodu	přechod	k1gInSc2	přechod
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
z	z	k7c2	z
navzájem	navzájem	k6eAd1	navzájem
velmi	velmi	k6eAd1	velmi
vzdálených	vzdálený	k2eAgNnPc2d1	vzdálené
stanovišť	stanoviště	k1gNnPc2	stanoviště
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
stanovišti	stanoviště	k1gNnPc7	stanoviště
potom	potom	k8xC	potom
posloužila	posloužit	k5eAaPmAgFnS	posloužit
jako	jako	k9	jako
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
trigonometrický	trigonometrický	k2eAgInSc4d1	trigonometrický
výpočet	výpočet	k1gInSc4	výpočet
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
Země	zem	k1gFnSc2	zem
od	od	k7c2	od
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
expedic	expedice	k1gFnPc2	expedice
do	do	k7c2	do
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
oblastí	oblast	k1gFnPc2	oblast
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přechod	přechod	k1gInSc4	přechod
pozorovat	pozorovat	k5eAaImF	pozorovat
a	a	k8xC	a
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
porovnáním	porovnání	k1gNnSc7	porovnání
údajů	údaj	k1gInPc2	údaj
pak	pak	k6eAd1	pak
vypočítat	vypočítat	k5eAaPmF	vypočítat
sluneční	sluneční	k2eAgFnSc4d1	sluneční
paralaxu	paralaxa	k1gFnSc4	paralaxa
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgMnS	přijít
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1716	[number]	k4	1716
Edmund	Edmund	k1gMnSc1	Edmund
Halley	Halley	k1gMnSc1	Halley
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
téměř	téměř	k6eAd1	téměř
dvacet	dvacet	k4xCc4	dvacet
let	let	k1gInSc4	let
před	před	k7c7	před
jejím	její	k3xOp3gNnSc7	její
uskutečněním	uskutečnění	k1gNnSc7	uskutečnění
<g/>
.	.	kIx.	.
</s>
<s>
Početné	početný	k2eAgFnPc1d1	početná
výpravy	výprava	k1gFnPc1	výprava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
za	za	k7c7	za
pozorováním	pozorování	k1gNnSc7	pozorování
přechodu	přechod	k1gInSc2	přechod
roku	rok	k1gInSc2	rok
1761	[number]	k4	1761
vyrazily	vyrazit	k5eAaPmAgInP	vyrazit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
případů	případ	k1gInPc2	případ
široké	široký	k2eAgFnSc2d1	široká
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
vědecké	vědecký	k2eAgFnSc2d1	vědecká
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prvním	první	k4xOgMnSc6	první
z	z	k7c2	z
dvojice	dvojice	k1gFnSc2	dvojice
přechodů	přechod	k1gInPc2	přechod
se	se	k3xPyFc4	se
do	do	k7c2	do
různých	různý	k2eAgNnPc2d1	různé
míst	místo	k1gNnPc2	místo
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
,	,	kIx,	,
Newfoundlandu	Newfoundland	k1gInSc2	Newfoundland
a	a	k8xC	a
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
<g/>
,	,	kIx,	,
vydali	vydat	k5eAaPmAgMnP	vydat
vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Příležitost	příležitost	k1gFnSc4	příležitost
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
přinejmenším	přinejmenším	k6eAd1	přinejmenším
části	část	k1gFnSc2	část
přechodu	přechod	k1gInSc2	přechod
měla	mít	k5eAaImAgFnS	mít
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
např.	např.	kA	např.
Jeremiah	Jeremiah	k1gMnSc1	Jeremiah
Dixon	Dixon	k1gMnSc1	Dixon
a	a	k8xC	a
Charles	Charles	k1gMnSc1	Charles
Mason	mason	k1gMnSc1	mason
na	na	k7c6	na
mysu	mys	k1gInSc6	mys
Dobré	dobrý	k2eAgFnSc2d1	dobrá
naděje	naděje	k1gFnSc2	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1769	[number]	k4	1769
astronomové	astronom	k1gMnPc1	astronom
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
zase	zase	k9	zase
do	do	k7c2	do
Hudsonova	Hudsonův	k2eAgInSc2d1	Hudsonův
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
na	na	k7c4	na
Kalifornský	kalifornský	k2eAgInSc4d1	kalifornský
poloostrov	poloostrov	k1gInSc4	poloostrov
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
pod	pod	k7c7	pod
španělskou	španělský	k2eAgFnSc7d1	španělská
nadvládou	nadvláda	k1gFnSc7	nadvláda
<g/>
)	)	kIx)	)
a	a	k8xC	a
opět	opět	k6eAd1	opět
do	do	k7c2	do
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
též	též	k9	též
z	z	k7c2	z
Tahiti	Tahiti	k1gNnSc2	Tahiti
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
první	první	k4xOgFnSc2	první
průzkumné	průzkumný	k2eAgFnSc2d1	průzkumná
cesty	cesta	k1gFnSc2	cesta
kapitána	kapitán	k1gMnSc2	kapitán
Cooka	Cooek	k1gMnSc2	Cooek
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
Point	pointa	k1gFnPc2	pointa
Venus	Venus	k1gInSc1	Venus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
astronom	astronom	k1gMnSc1	astronom
Christian	Christian	k1gMnSc1	Christian
Mayer	Mayer	k1gMnSc1	Mayer
sledoval	sledovat	k5eAaImAgMnS	sledovat
přechod	přechod	k1gInSc4	přechod
ze	z	k7c2	z
Sankt	Sankt	k1gInSc1	Sankt
Petěrburgu	Petěrburg	k1gMnSc3	Petěrburg
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ho	on	k3xPp3gNnSc4	on
pozvala	pozvat	k5eAaPmAgFnS	pozvat
carevna	carevna	k1gFnSc1	carevna
Kateřina	Kateřina	k1gFnSc1	Kateřina
Veliká	veliký	k2eAgFnSc1d1	veliká
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gNnSc1	jeho
pozorování	pozorování	k1gNnSc1	pozorování
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
zmařila	zmařit	k5eAaPmAgFnS	zmařit
zatažená	zatažený	k2eAgFnSc1d1	zatažená
obloha	obloha	k1gFnSc1	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Smůla	smůla	k1gFnSc1	smůla
provázela	provázet	k5eAaImAgFnS	provázet
zejména	zejména	k9	zejména
francouzského	francouzský	k2eAgMnSc4d1	francouzský
astronoma	astronom	k1gMnSc4	astronom
Guillauma	Guillaum	k1gMnSc4	Guillaum
Le	Le	k1gMnSc4	Le
Gentil	Gentil	k1gMnSc4	Gentil
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
snaze	snaha	k1gFnSc6	snaha
spatřit	spatřit	k5eAaPmF	spatřit
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
přechodů	přechod	k1gInPc2	přechod
strávil	strávit	k5eAaPmAgInS	strávit
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
11	[number]	k4	11
let	léto	k1gNnPc2	léto
a	a	k8xC	a
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgMnPc2	jenž
ztratil	ztratit	k5eAaPmAgMnS	ztratit
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
i	i	k8xC	i
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
jeho	jeho	k3xOp3gInPc1	jeho
osudy	osud	k1gInPc1	osud
posloužily	posloužit	k5eAaPmAgInP	posloužit
jako	jako	k9	jako
předloha	předloha	k1gFnSc1	předloha
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Transit	transit	k1gInSc1	transit
of	of	k?	of
Venus	Venus	k1gInSc4	Venus
kanadské	kanadský	k2eAgFnSc2d1	kanadská
autorky	autorka	k1gFnSc2	autorka
Maureen	Maureen	k2eAgInSc1d1	Maureen
Hunter	Hunter	k1gInSc1	Hunter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
tzv.	tzv.	kA	tzv.
efektu	efekt	k1gInSc3	efekt
černé	černý	k2eAgFnSc2d1	černá
kapky	kapka	k1gFnSc2	kapka
(	(	kIx(	(
<g/>
též	též	k9	též
známému	známý	k1gMnSc3	známý
jako	jako	k8xS	jako
Bailyho	Baily	k1gMnSc2	Baily
kapka	kapka	k1gFnSc1	kapka
<g/>
)	)	kIx)	)
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
změřit	změřit	k5eAaPmF	změřit
počátek	počátek	k1gInSc4	počátek
a	a	k8xC	a
konec	konec	k1gInSc4	konec
přechodu	přechod	k1gInSc2	přechod
s	s	k7c7	s
dostatečnou	dostatečný	k2eAgFnSc7d1	dostatečná
přesností	přesnost	k1gFnSc7	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
Efekt	efekt	k1gInSc1	efekt
černé	černý	k2eAgFnSc2d1	černá
kapky	kapka	k1gFnSc2	kapka
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
připisován	připisován	k2eAgInSc1d1	připisován
husté	hustý	k2eAgFnSc3d1	hustá
atmosféře	atmosféra	k1gFnSc3	atmosféra
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
důkaz	důkaz	k1gInSc4	důkaz
její	její	k3xOp3gFnSc2	její
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc1d1	současná
studie	studie	k1gFnPc1	studie
však	však	k9	však
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
jen	jen	k9	jen
o	o	k7c4	o
optický	optický	k2eAgInSc4d1	optický
efekt	efekt	k1gInSc4	efekt
způsobený	způsobený	k2eAgInSc1d1	způsobený
rozmazáním	rozmazání	k1gNnPc3	rozmazání
obrazu	obraz	k1gInSc3	obraz
planety	planeta	k1gFnPc4	planeta
turbulencemi	turbulence	k1gFnPc7	turbulence
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
nebo	nebo	k8xC	nebo
vadami	vada	k1gFnPc7	vada
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1771	[number]	k4	1771
francouzský	francouzský	k2eAgInSc1d1	francouzský
astronom	astronom	k1gMnSc1	astronom
Jérôme	Jérôm	k1gInSc5	Jérôm
Lalande	Laland	k1gInSc5	Laland
využil	využít	k5eAaPmAgMnS	využít
údajů	údaj	k1gInPc2	údaj
získaných	získaný	k2eAgInPc2d1	získaný
během	během	k7c2	během
přechodů	přechod	k1gInPc2	přechod
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1761	[number]	k4	1761
a	a	k8xC	a
1769	[number]	k4	1769
a	a	k8xC	a
vypočítal	vypočítat	k5eAaPmAgInS	vypočítat
hodnotu	hodnota	k1gFnSc4	hodnota
astronomické	astronomický	k2eAgFnSc2d1	astronomická
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
153	[number]	k4	153
milionů	milion	k4xCgInPc2	milion
kilometrů	kilometr	k1gInPc2	kilometr
(	(	kIx(	(
<g/>
±	±	k?	±
<g/>
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
přesnost	přesnost	k1gFnSc1	přesnost
stále	stále	k6eAd1	stále
nebyla	být	k5eNaImAgFnS	být
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
efekt	efekt	k1gInSc4	efekt
černé	černý	k2eAgFnSc2d1	černá
kapky	kapka	k1gFnSc2	kapka
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
Horrocksovými	Horrocksův	k2eAgInPc7d1	Horrocksův
výpočty	výpočet	k1gInPc7	výpočet
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
znatelný	znatelný	k2eAgInSc4d1	znatelný
pokrok	pokrok	k1gInSc4	pokrok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1874	[number]	k4	1874
a	a	k8xC	a
1882	[number]	k4	1882
===	===	k?	===
</s>
</p>
<p>
<s>
Lalandem	Laland	k1gInSc7	Laland
vypočtenou	vypočtený	k2eAgFnSc4d1	vypočtená
hodnotu	hodnota	k1gFnSc4	hodnota
astronomové	astronom	k1gMnPc1	astronom
dále	daleko	k6eAd2	daleko
zpřesňovali	zpřesňovat	k5eAaImAgMnP	zpřesňovat
během	během	k7c2	během
přechodů	přechod	k1gInPc2	přechod
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1874	[number]	k4	1874
a	a	k8xC	a
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
expedic	expedice	k1gFnPc2	expedice
vydalo	vydat	k5eAaPmAgNnS	vydat
na	na	k7c4	na
Kerguelenské	Kerguelenský	k2eAgInPc4d1	Kerguelenský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
astronomové	astronom	k1gMnPc1	astronom
mohli	moct	k5eAaImAgMnP	moct
využít	využít	k5eAaPmF	využít
výhod	výhod	k1gInSc4	výhod
vynálezu	vynález	k1gInSc2	vynález
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jim	on	k3xPp3gMnPc3	on
umožnil	umožnit	k5eAaPmAgInS	umožnit
mikrometrická	mikrometrický	k2eAgNnPc4d1	mikrometrický
měření	měření	k1gNnPc4	měření
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
Venuše	Venuše	k1gFnSc2	Venuše
od	od	k7c2	od
okraje	okraj	k1gInSc2	okraj
slunečního	sluneční	k2eAgInSc2d1	sluneční
kotouče	kotouč	k1gInSc2	kotouč
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
již	již	k6eAd1	již
nebyli	být	k5eNaImAgMnP	být
odkázáni	odkázat	k5eAaPmNgMnP	odkázat
na	na	k7c4	na
prchavé	prchavý	k2eAgInPc4d1	prchavý
okamžiky	okamžik	k1gInPc4	okamžik
kontaktů	kontakt	k1gInPc2	kontakt
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
a	a	k8xC	a
konci	konec	k1gInSc6	konec
přechodu	přechod	k1gInSc2	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
našly	najít	k5eAaPmAgFnP	najít
také	také	k6eAd1	také
tzv.	tzv.	kA	tzv.
heliometry	heliometr	k1gInPc1	heliometr
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
určené	určený	k2eAgInPc1d1	určený
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
slunečního	sluneční	k2eAgInSc2d1	sluneční
průměru	průměr	k1gInSc2	průměr
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tato	tento	k3xDgNnPc1	tento
měření	měření	k1gNnPc1	měření
však	však	k9	však
byla	být	k5eAaImAgNnP	být
stále	stále	k6eAd1	stále
zatížena	zatížit	k5eAaPmNgFnS	zatížit
velkou	velký	k2eAgFnSc7d1	velká
chybou	chyba	k1gFnSc7	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
astronom	astronom	k1gMnSc1	astronom
Simon	Simon	k1gMnSc1	Simon
Newcomb	Newcomb	k1gMnSc1	Newcomb
zkombinoval	zkombinovat	k5eAaPmAgMnS	zkombinovat
data	datum	k1gNnSc2	datum
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
posledních	poslední	k2eAgInPc2d1	poslední
přechodů	přechod	k1gInPc2	přechod
a	a	k8xC	a
odvodil	odvodit	k5eAaPmAgMnS	odvodit
hodnotu	hodnota	k1gFnSc4	hodnota
astronomické	astronomický	k2eAgFnSc2d1	astronomická
jednotky	jednotka	k1gFnSc2	jednotka
na	na	k7c4	na
149,59	[number]	k4	149,59
milionu	milion	k4xCgInSc2	milion
kilometrů	kilometr	k1gInPc2	kilometr
(	(	kIx(	(
<g/>
±	±	k?	±
<g/>
0,31	[number]	k4	0,31
milionu	milion	k4xCgInSc2	milion
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2004	[number]	k4	2004
a	a	k8xC	a
2012	[number]	k4	2012
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
již	již	k9	již
výpočty	výpočet	k1gInPc1	výpočet
pomocí	pomocí	k7c2	pomocí
paralaxy	paralaxa	k1gFnSc2	paralaxa
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
hodnoty	hodnota	k1gFnSc2	hodnota
astronomické	astronomický	k2eAgFnSc2d1	astronomická
jednotky	jednotka	k1gFnSc2	jednotka
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
moderní	moderní	k2eAgFnPc1d1	moderní
metody	metoda	k1gFnPc1	metoda
využívající	využívající	k2eAgFnPc4d1	využívající
vesmírných	vesmírný	k2eAgFnPc2d1	vesmírná
sond	sonda	k1gFnPc2	sonda
či	či	k8xC	či
radarových	radarový	k2eAgNnPc2d1	radarové
pozorování	pozorování	k1gNnPc2	pozorování
těles	těleso	k1gNnPc2	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
umožnily	umožnit	k5eAaPmAgFnP	umožnit
určit	určit	k5eAaPmF	určit
její	její	k3xOp3gFnSc4	její
hodnotu	hodnota	k1gFnSc4	hodnota
s	s	k7c7	s
přesností	přesnost	k1gFnSc7	přesnost
±	±	k?	±
<g/>
30	[number]	k4	30
m.	m.	k?	m.
Přesto	přesto	k8xC	přesto
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
přechod	přechod	k1gInSc4	přechod
Venuše	Venuše	k1gFnSc2	Venuše
opět	opět	k6eAd1	opět
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
zájem	zájem	k1gInSc4	zájem
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
událost	událost	k1gFnSc4	událost
využili	využít	k5eAaPmAgMnP	využít
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
míry	míra	k1gFnSc2	míra
poklesu	pokles	k1gInSc2	pokles
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
během	během	k7c2	během
zákrytu	zákryt	k1gInSc2	zákryt
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
zpřesnit	zpřesnit	k5eAaPmF	zpřesnit
metody	metoda	k1gFnPc4	metoda
hledání	hledání	k1gNnSc2	hledání
extrasolárních	extrasolární	k2eAgFnPc2d1	extrasolární
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
planety	planeta	k1gFnPc1	planeta
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
hvězd	hvězda	k1gFnPc2	hvězda
hledají	hledat	k5eAaImIp3nP	hledat
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
je	být	k5eAaImIp3nS	být
měření	měření	k1gNnPc4	měření
změn	změna	k1gFnPc2	změna
ve	v	k7c6	v
vlastním	vlastní	k2eAgInSc6d1	vlastní
pohybu	pohyb	k1gInSc6	pohyb
hvězdy	hvězda	k1gFnSc2	hvězda
nebo	nebo	k8xC	nebo
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
Dopplerově	Dopplerův	k2eAgInSc6d1	Dopplerův
posuvu	posuv	k1gInSc6	posuv
<g/>
,	,	kIx,	,
způsobených	způsobený	k2eAgInPc2d1	způsobený
změnou	změna	k1gFnSc7	změna
radiální	radiální	k2eAgFnSc3d1	radiální
rychlosti	rychlost	k1gFnSc3	rychlost
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
lze	lze	k6eAd1	lze
detekovat	detekovat	k5eAaImF	detekovat
velké	velký	k2eAgInPc4d1	velký
exoplanety	exoplanet	k1gInPc4	exoplanet
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
Jupiteru	Jupiter	k1gInSc2	Jupiter
či	či	k8xC	či
Neptunu	Neptun	k1gInSc2	Neptun
obíhající	obíhající	k2eAgFnPc1d1	obíhající
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
svou	svůj	k3xOyFgFnSc7	svůj
gravitací	gravitace	k1gFnSc7	gravitace
dokáží	dokázat	k5eAaPmIp3nP	dokázat
tuto	tento	k3xDgFnSc4	tento
hvězdu	hvězda	k1gFnSc4	hvězda
nepatrně	patrně	k6eNd1	patrně
vychýlit	vychýlit	k5eAaPmF	vychýlit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
metodou	metoda	k1gFnSc7	metoda
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc1	využití
gravitační	gravitační	k2eAgFnSc2d1	gravitační
mikročočky	mikročočka	k1gFnSc2	mikročočka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
planeta	planeta	k1gFnSc1	planeta
přecházející	přecházející	k2eAgFnSc1d1	přecházející
před	před	k7c7	před
hvězdou	hvězda	k1gFnSc7	hvězda
způsobí	způsobit	k5eAaPmIp3nS	způsobit
efekt	efekt	k1gInSc1	efekt
podobný	podobný	k2eAgInSc1d1	podobný
tzv.	tzv.	kA	tzv.
Einsteinovu	Einsteinův	k2eAgInSc3d1	Einsteinův
prstenu	prsten	k1gInSc3	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
poklesu	pokles	k1gInSc2	pokles
intenzity	intenzita	k1gFnSc2	intenzita
světla	světlo	k1gNnSc2	světlo
během	během	k7c2	během
přechodu	přechod	k1gInSc2	přechod
planety	planeta	k1gFnSc2	planeta
přes	přes	k7c4	přes
disk	disk	k1gInSc4	disk
hvězdy	hvězda	k1gFnSc2	hvězda
by	by	kYmCp3nS	by
však	však	k9	však
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
mnohem	mnohem	k6eAd1	mnohem
citlivější	citlivý	k2eAgNnSc1d2	citlivější
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nP	by
umožnit	umožnit	k5eAaPmF	umožnit
hledat	hledat	k5eAaImF	hledat
i	i	k9	i
malé	malý	k2eAgFnPc4d1	malá
planety	planeta	k1gFnPc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
velmi	velmi	k6eAd1	velmi
náročná	náročný	k2eAgFnSc1d1	náročná
na	na	k7c4	na
přesnost	přesnost	k1gFnSc4	přesnost
měření	měření	k1gNnSc2	měření
<g/>
:	:	kIx,	:
např.	např.	kA	např.
přechod	přechod	k1gInSc4	přechod
Venuše	Venuše	k1gFnSc2	Venuše
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jasnost	jasnost	k1gFnSc1	jasnost
Slunce	slunce	k1gNnSc2	slunce
poklesne	poklesnout	k5eAaPmIp3nS	poklesnout
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
hodnotu	hodnota	k1gFnSc4	hodnota
0,001	[number]	k4	0,001
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokles	pokles	k1gInSc1	pokles
jasnosti	jasnost	k1gFnSc2	jasnost
hvězdy	hvězda	k1gFnSc2	hvězda
způsobený	způsobený	k2eAgInSc1d1	způsobený
přechodem	přechod	k1gInSc7	přechod
malé	malý	k2eAgInPc4d1	malý
exoplanety	exoplanet	k1gInPc4	exoplanet
by	by	kYmCp3nP	by
byl	být	k5eAaImAgInS	být
podobně	podobně	k6eAd1	podobně
nepatrný	nepatrný	k2eAgInSc1d1	nepatrný
<g/>
.	.	kIx.	.
<g/>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgInSc1d1	poslední
přechod	přechod	k1gInSc1	přechod
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
viditelný	viditelný	k2eAgMnSc1d1	viditelný
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
Tichomoří	Tichomoří	k1gNnSc2	Tichomoří
<g/>
,	,	kIx,	,
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
v	v	k7c6	v
časných	časný	k2eAgFnPc6d1	časná
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
jen	jen	k9	jen
jeho	jeho	k3xOp3gFnSc4	jeho
poslední	poslední	k2eAgFnSc4d1	poslední
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přechody	přechod	k1gInPc7	přechod
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
a	a	k8xC	a
budoucnosti	budoucnost	k1gFnSc6	budoucnost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	s	k7c7	s
přechody	přechod	k1gInPc7	přechod
Venuše	Venuše	k1gFnSc2	Venuše
přes	přes	k7c4	přes
sluneční	sluneční	k2eAgInSc4d1	sluneční
disk	disk	k1gInSc4	disk
mohou	moct	k5eAaImIp3nP	moct
odehrávat	odehrávat	k5eAaImF	odehrávat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
nebo	nebo	k8xC	nebo
prosinci	prosinec	k1gInSc6	prosinec
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
tabulku	tabulka	k1gFnSc4	tabulka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
kalendářní	kalendářní	k2eAgNnPc1d1	kalendářní
data	datum	k1gNnPc1	datum
se	se	k3xPyFc4	se
však	však	k9	však
pomalu	pomalu	k6eAd1	pomalu
zpožďují	zpožďovat	k5eAaImIp3nP	zpožďovat
<g/>
;	;	kIx,	;
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1631	[number]	k4	1631
připadaly	připadat	k5eAaImAgFnP	připadat
na	na	k7c4	na
přechody	přechod	k1gInPc4	přechod
měsíce	měsíc	k1gInSc2	měsíc
květen	květen	k1gInSc1	květen
a	a	k8xC	a
listopad	listopad	k1gInSc1	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Přechody	přechod	k1gInPc1	přechod
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
ve	v	k7c6	v
dvojicích	dvojice	k1gFnPc6	dvojice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
druhý	druhý	k4xOgMnSc1	druhý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nastává	nastávat	k5eAaImIp3nS	nastávat
po	po	k7c6	po
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
takřka	takřka	k6eAd1	takřka
ve	v	k7c4	v
stejné	stejný	k2eAgNnSc4d1	stejné
datum	datum	k1gNnSc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
8	[number]	k4	8
pozemských	pozemský	k2eAgNnPc2d1	pozemské
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
stejně	stejně	k6eAd1	stejně
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
jako	jako	k8xS	jako
13	[number]	k4	13
oběhů	oběh	k1gInPc2	oběh
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
každých	každý	k3xTgNnPc2	každý
osm	osm	k4xCc1	osm
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
planety	planeta	k1gFnPc1	planeta
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
konjunkce	konjunkce	k1gFnSc1	konjunkce
však	však	k9	však
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
přesná	přesný	k2eAgFnSc1d1	přesná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
opakovat	opakovat	k5eAaImF	opakovat
tři	tři	k4xCgInPc4	tři
nebo	nebo	k8xC	nebo
i	i	k9	i
více	hodně	k6eAd2	hodně
přechodů	přechod	k1gInPc2	přechod
po	po	k7c6	po
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Venuše	Venuše	k1gFnSc1	Venuše
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
pokaždé	pokaždé	k6eAd1	pokaždé
dorazí	dorazit	k5eAaPmIp3nS	dorazit
o	o	k7c4	o
22	[number]	k4	22
hodin	hodina	k1gFnPc2	hodina
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
při	při	k7c6	při
předchozí	předchozí	k2eAgFnSc6d1	předchozí
události	událost	k1gFnSc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
přechod	přechod	k1gInSc1	přechod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
neproběhl	proběhnout	k5eNaPmAgInS	proběhnout
ve	v	k7c6	v
dvojici	dvojice	k1gFnSc6	dvojice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgMnS	odehrát
roku	rok	k1gInSc2	rok
1396	[number]	k4	1396
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
tak	tak	k9	tak
stane	stanout	k5eAaPmIp3nS	stanout
roku	rok	k1gInSc2	rok
3089	[number]	k4	3089
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2854	[number]	k4	2854
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
proběhnout	proběhnout	k5eAaPmF	proběhnout
druhý	druhý	k4xOgInSc1	druhý
přechod	přechod	k1gInSc1	přechod
z	z	k7c2	z
dvojice	dvojice	k1gFnSc2	dvojice
2846	[number]	k4	2846
<g/>
/	/	kIx~	/
<g/>
2854	[number]	k4	2854
<g/>
)	)	kIx)	)
Venuše	Venuše	k1gFnSc2	Venuše
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
pozemského	pozemský	k2eAgInSc2d1	pozemský
rovníku	rovník	k1gInSc2	rovník
sluneční	sluneční	k2eAgInSc1d1	sluneční
kotouč	kotouč	k1gInSc1	kotouč
těsně	těsně	k6eAd1	těsně
mine	minout	k5eAaImIp3nS	minout
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
jižní	jižní	k2eAgFnSc2d1	jižní
polokoule	polokoule	k1gFnSc2	polokoule
bude	být	k5eAaImBp3nS	být
viditelný	viditelný	k2eAgInSc1d1	viditelný
alespoň	alespoň	k9	alespoň
částečný	částečný	k2eAgInSc1d1	částečný
přechod	přechod	k1gInSc1	přechod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzácné	vzácný	k2eAgInPc4d1	vzácný
úkazy	úkaz	k1gInPc4	úkaz
doprovázející	doprovázející	k2eAgInPc4d1	doprovázející
přechod	přechod	k1gInSc4	přechod
Venuše	Venuše	k1gFnSc2	Venuše
==	==	k?	==
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
Venuše	Venuše	k1gFnSc1	Venuše
přejde	přejít	k5eAaPmIp3nS	přejít
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
okraji	okraj	k1gInSc6	okraj
slunečního	sluneční	k2eAgInSc2d1	sluneční
kotouče	kotouč	k1gInSc2	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
viditelný	viditelný	k2eAgInSc1d1	viditelný
pouze	pouze	k6eAd1	pouze
částečný	částečný	k2eAgInSc1d1	částečný
přechod	přechod	k1gInSc1	přechod
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
bez	bez	k7c2	bez
druhého	druhý	k4xOgInSc2	druhý
a	a	k8xC	a
třetího	třetí	k4xOgInSc2	třetí
kontaktu	kontakt	k1gInSc2	kontakt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
přechod	přechod	k1gInSc1	přechod
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1631	[number]	k4	1631
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
nastane	nastat	k5eAaPmIp3nS	nastat
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2611	[number]	k4	2611
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
viditelný	viditelný	k2eAgInSc1d1	viditelný
částečný	částečný	k2eAgInSc1d1	částečný
přechod	přechod	k1gInSc1	přechod
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
Země	zem	k1gFnSc2	zem
Venuše	Venuše	k1gFnSc2	Venuše
zcela	zcela	k6eAd1	zcela
Slunce	slunce	k1gNnSc1	slunce
mine	minout	k5eAaImIp3nS	minout
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
přechod	přechod	k1gInSc1	přechod
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
naposledy	naposledy	k6eAd1	naposledy
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
541	[number]	k4	541
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stane	stanout	k5eAaPmIp3nS	stanout
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2854	[number]	k4	2854
<g/>
.	.	kIx.	.
<g/>
Extrémně	extrémně	k6eAd1	extrémně
vzácnou	vzácný	k2eAgFnSc7d1	vzácná
událostí	událost	k1gFnSc7	událost
je	být	k5eAaImIp3nS	být
současný	současný	k2eAgInSc1d1	současný
přechod	přechod	k1gInSc1	přechod
planet	planeta	k1gFnPc2	planeta
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
takový	takový	k3xDgInSc1	takový
případ	případ	k1gInSc1	případ
nastal	nastat	k5eAaPmAgInS	nastat
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
373	[number]	k4	373
173	[number]	k4	173
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
a	a	k8xC	a
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stane	stanout	k5eAaPmIp3nS	stanout
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
69	[number]	k4	69
163	[number]	k4	163
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
simultánní	simultánní	k2eAgInSc1d1	simultánní
přechod	přechod	k1gInSc1	přechod
bude	být	k5eAaImBp3nS	být
následovat	následovat	k5eAaImF	následovat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
224	[number]	k4	224
508	[number]	k4	508
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
vzácným	vzácný	k2eAgInSc7d1	vzácný
úkazem	úkaz	k1gInSc7	úkaz
je	být	k5eAaImIp3nS	být
i	i	k9	i
přechod	přechod	k1gInSc1	přechod
Venuše	Venuše	k1gFnSc2	Venuše
probíhající	probíhající	k2eAgInSc4d1	probíhající
současně	současně	k6eAd1	současně
se	s	k7c7	s
zatměním	zatmění	k1gNnSc7	zatmění
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sluneční	sluneční	k2eAgInSc1d1	sluneční
disk	disk	k1gInSc1	disk
zakryje	zakrýt	k5eAaPmIp3nS	zakrýt
i	i	k9	i
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
při	při	k7c6	při
prstencovém	prstencový	k2eAgNnSc6d1	prstencové
zatmění	zatmění	k1gNnSc6	zatmění
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
15	[number]	k4	15
607	[number]	k4	607
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
tato	tento	k3xDgFnSc1	tento
konstelace	konstelace	k1gFnSc1	konstelace
nastane	nastat	k5eAaPmIp3nS	nastat
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
kombinovaném	kombinovaný	k2eAgNnSc6d1	kombinované
zatmění	zatmění	k1gNnSc4	zatmění
Slunce	slunce	k1gNnSc1	slunce
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
15	[number]	k4	15
232	[number]	k4	232
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1769	[number]	k4	1769
byli	být	k5eAaImAgMnP	být
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
úplné	úplný	k2eAgNnSc1d1	úplné
zatmění	zatmění	k1gNnSc1	zatmění
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
viditelné	viditelný	k2eAgFnSc2d1	viditelná
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
nastalo	nastat	k5eAaPmAgNnS	nastat
pouhý	pouhý	k2eAgInSc4d1	pouhý
den	den	k1gInSc4	den
po	po	k7c6	po
přechodu	přechod	k1gInSc6	přechod
Venuše	Venuše	k1gFnSc2	Venuše
přes	přes	k7c4	přes
sluneční	sluneční	k2eAgInSc4d1	sluneční
disk	disk	k1gInSc4	disk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
pozorování	pozorování	k1gNnSc2	pozorování
==	==	k?	==
</s>
</p>
<p>
<s>
Nejbezpečnější	bezpečný	k2eAgInSc1d3	nejbezpečnější
způsob	způsob	k1gInSc1	způsob
pozorování	pozorování	k1gNnSc2	pozorování
přechodu	přechod	k1gInSc2	přechod
je	být	k5eAaImIp3nS	být
promítání	promítání	k1gNnSc4	promítání
obrazu	obraz	k1gInSc2	obraz
Slunce	slunce	k1gNnSc2	slunce
pomocí	pomocí	k7c2	pomocí
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
triedru	triedr	k1gInSc2	triedr
nebo	nebo	k8xC	nebo
dírkové	dírkový	k2eAgFnSc2d1	dírková
komory	komora	k1gFnSc2	komora
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
událost	událost	k1gFnSc4	událost
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
sledovat	sledovat	k5eAaImF	sledovat
i	i	k9	i
přímo	přímo	k6eAd1	přímo
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
speciálních	speciální	k2eAgInPc2d1	speciální
filtrů	filtr	k1gInPc2	filtr
(	(	kIx(	(
<g/>
např.	např.	kA	např.
astronomický	astronomický	k2eAgInSc1d1	astronomický
sluneční	sluneční	k2eAgInSc1d1	sluneční
filtr	filtr	k1gInSc1	filtr
pokrytý	pokrytý	k2eAgInSc1d1	pokrytý
tenkou	tenký	k2eAgFnSc7d1	tenká
vrstvou	vrstva	k1gFnSc7	vrstva
chrómu	chróm	k1gInSc2	chróm
či	či	k8xC	či
hliníku	hliník	k1gInSc2	hliník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brýlí	brýle	k1gFnPc2	brýle
na	na	k7c4	na
pozorování	pozorování	k1gNnPc4	pozorování
Slunce	slunce	k1gNnSc2	slunce
nebo	nebo	k8xC	nebo
ochranného	ochranný	k2eAgNnSc2d1	ochranné
svářečského	svářečský	k2eAgNnSc2d1	svářečské
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
využívané	využívaný	k2eAgFnPc1d1	využívaná
ochranné	ochranný	k2eAgFnPc1d1	ochranná
pomůcky	pomůcka	k1gFnPc1	pomůcka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
osvícený	osvícený	k2eAgInSc1d1	osvícený
černobílý	černobílý	k2eAgInSc1d1	černobílý
fotografický	fotografický	k2eAgInSc1d1	fotografický
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nepovažují	považovat	k5eNaImIp3nP	považovat
za	za	k7c4	za
bezpečné	bezpečný	k2eAgFnPc4d1	bezpečná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
malé	malý	k2eAgFnPc1d1	malá
vady	vada	k1gFnPc1	vada
filmu	film	k1gInSc2	film
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
škodlivé	škodlivý	k2eAgNnSc4d1	škodlivé
ultrafialové	ultrafialový	k2eAgNnSc4d1	ultrafialové
záření	záření	k1gNnSc4	záření
tímto	tento	k3xDgInSc7	tento
filtrem	filtr	k1gInSc7	filtr
projde	projít	k5eAaPmIp3nS	projít
<g/>
.	.	kIx.	.
</s>
<s>
Vyvolaný	vyvolaný	k2eAgInSc1d1	vyvolaný
barevný	barevný	k2eAgInSc1d1	barevný
film	film	k1gInSc1	film
zase	zase	k9	zase
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
černobílého	černobílý	k2eAgMnSc2d1	černobílý
<g/>
)	)	kIx)	)
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
průhledný	průhledný	k2eAgInSc1d1	průhledný
pro	pro	k7c4	pro
infračervené	infračervený	k2eAgNnSc4d1	infračervené
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
popáleninám	popálenina	k1gFnPc3	popálenina
na	na	k7c6	na
sítnici	sítnice	k1gFnSc6	sítnice
<g/>
.	.	kIx.	.
</s>
<s>
Přímé	přímý	k2eAgNnSc1d1	přímé
pozorování	pozorování	k1gNnSc1	pozorování
Slunce	slunce	k1gNnSc2	slunce
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
filtru	filtr	k1gInSc2	filtr
nebo	nebo	k8xC	nebo
s	s	k7c7	s
nevhodným	vhodný	k2eNgInSc7d1	nevhodný
filtrem	filtr	k1gInSc7	filtr
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
poškození	poškození	k1gNnSc3	poškození
buněk	buňka	k1gFnPc2	buňka
sítnice	sítnice	k1gFnSc2	sítnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
nebo	nebo	k8xC	nebo
i	i	k9	i
trvalou	trvalý	k2eAgFnSc4d1	trvalá
ztrátu	ztráta	k1gFnSc4	ztráta
zraku	zrak	k1gInSc2	zrak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Transit	transit	k1gInSc1	transit
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MAOR	MAOR	kA	MAOR
<g/>
,	,	kIx,	,
Eli	Eli	k1gFnSc1	Eli
<g/>
.	.	kIx.	.
</s>
<s>
Venus	Venus	k1gInSc1	Venus
in	in	k?	in
Transit	transit	k1gInSc1	transit
<g/>
.	.	kIx.	.
</s>
<s>
Princeton	Princeton	k1gInSc1	Princeton
<g/>
:	:	kIx,	:
Princeton	Princeton	k1gInSc1	Princeton
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
691	[number]	k4	691
<g/>
-	-	kIx~	-
<g/>
11589	[number]	k4	11589
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SHEEHAN	SHEEHAN	kA	SHEEHAN
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
<g/>
;	;	kIx,	;
WESTFALL	WESTFALL	kA	WESTFALL
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Transits	Transits	k1gInSc1	Transits
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
<g/>
.	.	kIx.	.
</s>
<s>
Amherst	Amherst	k1gMnSc1	Amherst
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Prometheus	Prometheus	k1gMnSc1	Prometheus
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
59102	[number]	k4	59102
<g/>
-	-	kIx~	-
<g/>
175	[number]	k4	175
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GRUSS	GRUSS	kA	GRUSS
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
říše	říš	k1gFnSc2	říš
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Astronomie	astronomie	k1gFnSc1	astronomie
pro	pro	k7c4	pro
širší	široký	k2eAgInPc4d2	širší
kruhy	kruh	k1gInPc4	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Bursík	Bursík	k1gMnSc1	Bursík
&	&	k?	&
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
</s>
<s>
Parallaxa	Parallaxa	k1gMnSc1	Parallaxa
<g/>
,	,	kIx,	,
s.	s.	k?	s.
238	[number]	k4	238
<g/>
–	–	k?	–
<g/>
257	[number]	k4	257
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠARONOV	ŠARONOV	kA	ŠARONOV
<g/>
,	,	kIx,	,
Vsevolod	Vsevolod	k1gInSc1	Vsevolod
Vasil	Vasil	k1gInSc1	Vasil
<g/>
'	'	kIx"	'
<g/>
jevič	jevič	k1gInSc1	jevič
<g/>
.	.	kIx.	.
П	П	k?	П
В	В	k?	В
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
:	:	kIx,	:
И	И	k?	И
"	"	kIx"	"
<g/>
Н	Н	k?	Н
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Astropis	Astropis	k1gInSc1	Astropis
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
</s>
<s>
XI	XI	kA	XI
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1211	[number]	k4	1211
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
485	[number]	k4	485
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
</s>
</p>
<p>
<s>
Přechod	přechod	k1gInSc1	přechod
(	(	kIx(	(
<g/>
astronomie	astronomie	k1gFnSc1	astronomie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přechod	přechod	k1gInSc1	přechod
Merkuru	Merkur	k1gInSc2	Merkur
</s>
</p>
<p>
<s>
Efekt	efekt	k1gInSc1	efekt
černé	černý	k2eAgFnSc2d1	černá
kapky	kapka	k1gFnSc2	kapka
</s>
</p>
<p>
<s>
Zatmění	zatmění	k1gNnSc1	zatmění
Slunce	slunce	k1gNnSc2	slunce
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
přechod	přechod	k1gInSc1	přechod
Venuše	Venuše	k1gFnSc2	Venuše
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Transit	transit	k1gInSc1	transit
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
Transits	Transits	k1gInSc1	Transits
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Venuše	Venuše	k1gFnSc2	Venuše
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c4	v
Wikizdrojíchčesky	Wikizdrojíchčesk	k1gInPc4	Wikizdrojíchčesk
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Přechod	přechod	k1gInSc1	přechod
Venuše	Venuše	k1gFnSc2	Venuše
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
Přechod	přechod	k1gInSc1	přechod
Venuše	Venuše	k1gFnSc2	Venuše
před	před	k7c7	před
Sluncem	slunce	k1gNnSc7	slunce
–	–	k?	–
Instantní	instantní	k2eAgFnPc4d1	instantní
astronomické	astronomický	k2eAgFnPc4d1	astronomická
noviny	novina	k1gFnPc4	novina
</s>
</p>
<p>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
přechodu	přechod	k1gInSc2	přechod
Venuše	Venuše	k1gFnSc2	Venuše
přes	přes	k7c4	přes
Slunce	slunce	k1gNnSc4	slunce
8	[number]	k4	8
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
June	jun	k1gMnSc5	jun
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Venus	Venus	k1gInSc1	Venus
Transit	transit	k1gInSc1	transit
</s>
</p>
<p>
<s>
Transit	transit	k1gInSc1	transit
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
–	–	k?	–
Základní	základní	k2eAgFnPc4d1	základní
informace	informace	k1gFnPc4	informace
</s>
</p>
<p>
<s>
Transits	Transits	k1gInSc1	Transits
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
–	–	k?	–
Katalog	katalog	k1gInSc1	katalog
přechodů	přechod	k1gInPc2	přechod
Venuše	Venuše	k1gFnSc2	Venuše
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
5	[number]	k4	5
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
až	až	k9	až
10	[number]	k4	10
000	[number]	k4	000
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
Chasing	Chasing	k1gInSc1	Chasing
Venus	Venus	k1gInSc1	Venus
<g/>
,	,	kIx,	,
Observing	Observing	k1gInSc1	Observing
the	the	k?	the
Transits	Transits	k1gInSc1	Transits
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
–	–	k?	–
Smithsonian	Smithsonian	k1gInSc1	Smithsonian
Institution	Institution	k1gInSc1	Institution
Libraries	Libraries	k1gInSc4	Libraries
<g/>
:	:	kIx,	:
Doprovodné	doprovodný	k2eAgFnPc4d1	doprovodná
stránky	stránka	k1gFnPc4	stránka
k	k	k7c3	k
výstavě	výstava	k1gFnSc3	výstava
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
přechodu	přechod	k1gInSc2	přechod
Venuše	Venuše	k1gFnSc2	Venuše
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
Predictions	Predictions	k6eAd1	Predictions
for	forum	k1gNnPc2	forum
the	the	k?	the
2012	[number]	k4	2012
transit	transit	k1gInSc1	transit
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
–	–	k?	–
Interaktivní	interaktivní	k2eAgFnSc1d1	interaktivní
stránka	stránka	k1gFnSc1	stránka
zobrazující	zobrazující	k2eAgFnSc1d1	zobrazující
údaje	údaj	k1gInSc2	údaj
o	o	k7c6	o
přechodu	přechod	k1gInSc6	přechod
Venuše	Venuše	k1gFnSc2	Venuše
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
podle	podle	k7c2	podle
zadaných	zadaný	k2eAgFnPc2d1	zadaná
zeměpisných	zeměpisný	k2eAgFnPc2d1	zeměpisná
souřadnic	souřadnice	k1gFnPc2	souřadnice
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
</s>
</p>
<p>
<s>
The	The	k?	The
2004	[number]	k4	2004
Transit	transit	k1gInSc1	transit
of	of	k?	of
Venus	Venus	k1gInSc4	Venus
–	–	k?	–
Časové	časový	k2eAgInPc4d1	časový
údaje	údaj	k1gInPc4	údaj
kontaktů	kontakt	k1gInPc2	kontakt
a	a	k8xC	a
maximální	maximální	k2eAgFnSc2d1	maximální
fáze	fáze	k1gFnSc2	fáze
</s>
</p>
<p>
<s>
Reanimating	Reanimating	k1gInSc1	Reanimating
the	the	k?	the
1882	[number]	k4	1882
Transit	transit	k1gInSc1	transit
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
–	–	k?	–
Článek	článek	k1gInSc1	článek
v	v	k7c6	v
online	onlinout	k5eAaPmIp3nS	onlinout
verzi	verze	k1gFnSc4	verze
časopisu	časopis	k1gInSc2	časopis
Sky	Sky	k1gFnSc3	Sky
and	and	k?	and
Telescope	Telescop	k1gInSc5	Telescop
</s>
</p>
<p>
<s>
Transit	transit	k1gInSc1	transit
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
–	–	k?	–
Historická	historický	k2eAgNnPc4d1	historické
pozorování	pozorování	k1gNnPc4	pozorování
přechodu	přechod	k1gInSc2	přechod
Venuše	Venuše	k1gFnSc2	Venuše
</s>
</p>
<p>
<s>
The	The	k?	The
transit	transit	k1gInSc1	transit
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
across	across	k1gInSc1	across
the	the	k?	the
Sun	Sun	kA	Sun
–	–	k?	–
Dokument	dokument	k1gInSc1	dokument
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
a	a	k8xC	a
významu	význam	k1gInSc6	význam
pozorování	pozorování	k1gNnSc2	pozorování
přechodu	přechod	k1gInSc2	přechod
Venuše	Venuše	k1gFnSc2	Venuše
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
transit	transit	k1gInSc1	transit
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
<g/>
:	:	kIx,	:
a	a	k8xC	a
stroke	stroke	k6eAd1	stroke
of	of	k?	of
luck	luck	k1gMnSc1	luck
for	forum	k1gNnPc2	forum
teachers	teachers	k6eAd1	teachers
–	–	k?	–
Dokument	dokument	k1gInSc1	dokument
o	o	k7c4	o
využití	využití	k1gNnSc4	využití
přechodu	přechod	k1gInSc2	přechod
Venuše	Venuše	k1gFnSc2	Venuše
ve	v	k7c6	v
školní	školní	k2eAgFnSc6d1	školní
výuce	výuka	k1gFnSc6	výuka
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
venus	venus	k1gInSc1	venus
<g/>
2004	[number]	k4	2004
–	–	k?	–
Stránky	stránka	k1gFnSc2	stránka
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
přechodu	přechod	k1gInSc3	přechod
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
samotné	samotný	k2eAgFnSc6d1	samotná
planetě	planeta	k1gFnSc6	planeta
Venuši	Venuše	k1gFnSc6	Venuše
a	a	k8xC	a
sondě	sonda	k1gFnSc6	sonda
Venus	Venus	k1gInSc1	Venus
Express	express	k1gInSc1	express
(	(	kIx(	(
<g/>
též	též	k9	též
francouzsky	francouzsky	k6eAd1	francouzsky
a	a	k8xC	a
portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Two	Two	k?	Two
exoplanets	exoplanets	k1gInSc1	exoplanets
discovered	discovered	k1gInSc1	discovered
by	by	kYmCp3nS	by
transit	transit	k1gInSc4	transit
light	light	k5eAaPmF	light
dimming	dimming	k1gInSc4	dimming
method	method	k1gInSc1	method
–	–	k?	–
Přechod	přechod	k1gInSc1	přechod
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
přes	přes	k7c4	přes
disk	disk	k1gInSc4	disk
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
ESO	eso	k1gNnSc1	eso
Press	Press	k1gInSc1	Press
Release	Releas	k1gInSc6	Releas
11	[number]	k4	11
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
</s>
</p>
<p>
<s>
Simulátor	simulátor	k1gInSc1	simulátor
Prechodu	Prechod	k1gInSc2	Prechod
Venuše	Venuše	k1gFnSc2	Venuše
2012	[number]	k4	2012
</s>
</p>
