<s>
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Radecký	Radecký	k1gMnSc1	Radecký
z	z	k7c2	z
Radče	Radč	k1gFnSc2	Radč
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1766	[number]	k4	1766
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Třebnice	Třebnice	k1gFnSc2	Třebnice
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1858	[number]	k4	1858
<g/>
,	,	kIx,	,
Milán	Milán	k1gInSc1	Milán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Antonín	Antonín	k1gMnSc1	Antonín
František	František	k1gMnSc1	František
Karel	Karel	k1gMnSc1	Karel
hrabě	hrabě	k1gMnSc1	hrabě
Radecký	Radecký	k1gMnSc1	Radecký
z	z	k7c2	z
Radče	Radč	k1gFnSc2	Radč
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
uváděn	uvádět	k5eAaImNgMnS	uvádět
též	též	k9	též
nesprávně	správně	k6eNd1	správně
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
šlechtic	šlechtic	k1gMnSc1	šlechtic
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Radeckých	Radeckých	k2eAgInSc2d1	Radeckých
z	z	k7c2	z
Radče	Radč	k1gFnSc2	Radč
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
rakouský	rakouský	k2eAgMnSc1d1	rakouský
vojenský	vojenský	k2eAgMnSc1d1	vojenský
velitel	velitel	k1gMnSc1	velitel
<g/>
,	,	kIx,	,
stratég	stratég	k1gMnSc1	stratég
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
vojenský	vojenský	k2eAgMnSc1d1	vojenský
reformátor	reformátor	k1gMnSc1	reformátor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
Evropy	Evropa	k1gFnSc2	Evropa
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
