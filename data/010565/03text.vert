<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Radecký	Radecký	k1gMnSc1	Radecký
z	z	k7c2	z
Radče	Radč	k1gFnSc2	Radč
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1766	[number]	k4	1766
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Třebnice	Třebnice	k1gFnSc2	Třebnice
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1858	[number]	k4	1858
<g/>
,	,	kIx,	,
Milán	Milán	k1gInSc1	Milán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Antonín	Antonín	k1gMnSc1	Antonín
František	František	k1gMnSc1	František
Karel	Karel	k1gMnSc1	Karel
hrabě	hrabě	k1gMnSc1	hrabě
Radecký	Radecký	k1gMnSc1	Radecký
z	z	k7c2	z
Radče	Radč	k1gFnSc2	Radč
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
uváděn	uvádět	k5eAaImNgMnS	uvádět
též	též	k9	též
nesprávně	správně	k6eNd1	správně
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
šlechtic	šlechtic	k1gMnSc1	šlechtic
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Radeckých	Radeckých	k2eAgInSc2d1	Radeckých
z	z	k7c2	z
Radče	Radč	k1gFnSc2	Radč
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
rakouský	rakouský	k2eAgMnSc1d1	rakouský
vojenský	vojenský	k2eAgMnSc1d1	vojenský
velitel	velitel	k1gMnSc1	velitel
<g/>
,	,	kIx,	,
stratég	stratég	k1gMnSc1	stratég
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
vojenský	vojenský	k2eAgMnSc1d1	vojenský
reformátor	reformátor	k1gMnSc1	reformátor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
Evropy	Evropa	k1gFnSc2	Evropa
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
vojenské	vojenský	k2eAgFnSc2d1	vojenská
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
porážce	porážka	k1gFnSc6	porážka
napoleonské	napoleonský	k2eAgFnSc2d1	napoleonská
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
reformě	reforma	k1gFnSc3	reforma
rakouské	rakouský	k2eAgFnSc2d1	rakouská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
své	svůj	k3xOyFgFnSc2	svůj
zásluhy	zásluha	k1gFnSc2	zásluha
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
rakouských	rakouský	k2eAgFnPc2d1	rakouská
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
rakouským	rakouský	k2eAgMnSc7d1	rakouský
polním	polní	k2eAgMnSc7d1	polní
maršálem	maršál	k1gMnSc7	maršál
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
82	[number]	k4	82
let	léto	k1gNnPc2	léto
následně	následně	k6eAd1	následně
jednoznačně	jednoznačně	k6eAd1	jednoznačně
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
italské	italský	k2eAgFnSc6d1	italská
válce	válka	k1gFnSc6	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
nad	nad	k7c7	nad
spojenými	spojený	k2eAgFnPc7d1	spojená
armádami	armáda	k1gFnPc7	armáda
piemontského	piemontský	k2eAgMnSc2d1	piemontský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
Alberta	Albert	k1gMnSc2	Albert
a	a	k8xC	a
zajistil	zajistit	k5eAaPmAgMnS	zajistit
reinstalaci	reinstalace	k1gFnSc4	reinstalace
rakouské	rakouský	k2eAgFnSc2d1	rakouská
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
pro	pro	k7c4	pro
Království	království	k1gNnSc4	království
lombardsko-benátské	lombardskoenátský	k2eAgNnSc1d1	lombardsko-benátské
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
zůstal	zůstat	k5eAaPmAgMnS	zůstat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
z	z	k7c2	z
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
hraběte	hrabě	k1gMnSc2	hrabě
Petra	Petr	k1gMnSc2	Petr
Eusebia	Eusebius	k1gMnSc2	Eusebius
Radeckého	Radecký	k1gMnSc2	Radecký
a	a	k8xC	a
Marie	Maria	k1gFnSc2	Maria
Venantie	Venantie	k1gFnSc2	Venantie
rozené	rozený	k2eAgFnSc2d1	rozená
Bechyňové	Bechyňová	k1gFnSc2	Bechyňová
z	z	k7c2	z
Lažan	Lažana	k1gFnPc2	Lažana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
rodičů	rodič	k1gMnPc2	rodič
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
výchovu	výchova	k1gFnSc4	výchova
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
dědovi	děd	k1gMnSc3	děd
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
bývalému	bývalý	k2eAgMnSc3d1	bývalý
důstojníkovi	důstojník	k1gMnSc3	důstojník
<g/>
,	,	kIx,	,
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
gymnasiu	gymnasion	k1gNnSc6	gymnasion
u	u	k7c2	u
piaristů	piarista	k1gMnPc2	piarista
v	v	k7c6	v
Panské	panský	k2eAgFnSc6d1	Panská
ulici	ulice	k1gFnSc6	ulice
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
studiem	studio	k1gNnSc7	studio
na	na	k7c6	na
rytířské	rytířský	k2eAgFnSc6d1	rytířská
akademii	akademie	k1gFnSc6	akademie
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
na	na	k7c6	na
Theresianu	Theresian	k1gInSc6	Theresian
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
jako	jako	k9	jako
kadet	kadet	k1gMnSc1	kadet
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
kyrysnického	kyrysnický	k2eAgInSc2d1	kyrysnický
pluku	pluk	k1gInSc2	pluk
c.	c.	k?	c.
a	a	k8xC	a
k.	k.	k?	k.
rakouské	rakouský	k2eAgFnSc2d1	rakouská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
ležením	ležení	k1gNnSc7	ležení
v	v	k7c6	v
Gyöngyösi	Gyöngyöse	k1gFnSc6	Gyöngyöse
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
poručíkem	poručík	k1gMnSc7	poručík
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
nadporučíkem	nadporučík	k1gMnSc7	nadporučík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1788	[number]	k4	1788
-	-	kIx~	-
1789	[number]	k4	1789
se	se	k3xPyFc4	se
vyznamenal	vyznamenat	k5eAaPmAgMnS	vyznamenat
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
s	s	k7c7	s
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
během	během	k7c2	během
rakousko-turecké	rakouskourecký	k2eAgFnSc2d1	rakousko-turecká
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
místo	místo	k6eAd1	místo
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
pobočníků	pobočník	k1gMnPc2	pobočník
maršála	maršál	k1gMnSc4	maršál
Laudona	Laudon	k1gMnSc4	Laudon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
bojích	boj	k1gInPc6	boj
proti	proti	k7c3	proti
revoluční	revoluční	k2eAgFnSc3d1	revoluční
Francii	Francie	k1gFnSc3	Francie
v	v	k7c6	v
První	první	k4xOgFnSc6	první
koaliční	koaliční	k2eAgFnSc6d1	koaliční
válce	válka	k1gFnSc6	válka
bojoval	bojovat	k5eAaImAgMnS	bojovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1794	[number]	k4	1794
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
rytmistra	rytmistr	k1gMnSc2	rytmistr
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1796	[number]	k4	1796
převelen	převelet	k5eAaPmNgInS	převelet
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
štábní	štábní	k2eAgMnSc1d1	štábní
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
z	z	k7c2	z
hodnosti	hodnost	k1gFnSc2	hodnost
majora	major	k1gMnSc2	major
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
velitele	velitel	k1gMnSc4	velitel
pluku	pluk	k1gInSc2	pluk
pionýrského	pionýrský	k2eAgMnSc2d1	pionýrský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
campoformijského	campoformijský	k2eAgInSc2d1	campoformijský
míru	mír	k1gInSc2	mír
byl	být	k5eAaImAgInS	být
přesunut	přesunout	k5eAaPmNgInS	přesunout
k	k	k7c3	k
ženijním	ženijní	k2eAgFnPc3d1	ženijní
jednotkám	jednotka	k1gFnPc3	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
Druhé	druhý	k4xOgFnSc2	druhý
koaliční	koaliční	k2eAgFnSc2d1	koaliční
války	válka	k1gFnSc2	válka
sloužil	sloužit	k5eAaImAgInS	sloužit
opět	opět	k6eAd1	opět
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
adjutant	adjutant	k1gMnSc1	adjutant
ve	v	k7c6	v
štábu	štáb	k1gInSc6	štáb
generála	generál	k1gMnSc2	generál
Melase	melasa	k1gFnSc6	melasa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
plukovníkem	plukovník	k1gMnSc7	plukovník
a	a	k8xC	a
velitelem	velitel	k1gMnSc7	velitel
kyrysnického	kyrysnický	k2eAgInSc2d1	kyrysnický
pluku	pluk	k1gInSc2	pluk
na	na	k7c6	na
Rýně	Rýn	k1gInSc6	Rýn
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
se	se	k3xPyFc4	se
vyznamenal	vyznamenat	k5eAaPmAgMnS	vyznamenat
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Hohenlindenu	Hohenlinden	k1gInSc2	Hohenlinden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
generálmajorem	generálmajor	k1gMnSc7	generálmajor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
opětovné	opětovný	k2eAgFnSc6d1	opětovná
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Válka	válka	k1gFnSc1	válka
třetí	třetí	k4xOgFnSc2	třetí
koalice	koalice	k1gFnSc2	koalice
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velitelem	velitel	k1gMnSc7	velitel
brigády	brigáda	k1gFnSc2	brigáda
znovu	znovu	k6eAd1	znovu
na	na	k7c6	na
severoitalském	severoitalský	k2eAgNnSc6d1	severoitalské
bojišti	bojiště	k1gNnSc6	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
brigádou	brigáda	k1gFnSc7	brigáda
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
vítězné	vítězný	k2eAgFnSc2d1	vítězná
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Caldiera	Caldiero	k1gNnSc2	Caldiero
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
uděleno	udělit	k5eAaPmNgNnS	udělit
velení	velení	k1gNnSc1	velení
nad	nad	k7c7	nad
samostatným	samostatný	k2eAgInSc7d1	samostatný
oddílem	oddíl	k1gInSc7	oddíl
ve	v	k7c6	v
Štýrsku	Štýrsko	k1gNnSc6	Štýrsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
porazit	porazit	k5eAaPmF	porazit
francouzské	francouzský	k2eAgFnPc4d1	francouzská
jednotky	jednotka	k1gFnPc4	jednotka
v	v	k7c6	v
šarvátce	šarvátka	k1gFnSc6	šarvátka
u	u	k7c2	u
Ehrenhausenu	Ehrenhausen	k2eAgFnSc4d1	Ehrenhausen
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Rakouské	rakouský	k2eAgNnSc1d1	rakouské
císařství	císařství	k1gNnSc1	císařství
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
Prešpurský	prešpurský	k2eAgInSc4d1	prešpurský
mír	mír	k1gInSc4	mír
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
z	z	k7c2	z
protinapoleonské	protinapoleonský	k2eAgFnSc2d1	protinapoleonská
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Radecký	Radecký	k1gMnSc1	Radecký
převelen	převelet	k5eAaPmNgMnS	převelet
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velitelem	velitel	k1gMnSc7	velitel
brigády	brigáda	k1gFnSc2	brigáda
a	a	k8xC	a
pobočníkem	pobočník	k1gMnSc7	pobočník
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Karla	Karel	k1gMnSc2	Karel
<g/>
,	,	kIx,	,
známým	známý	k2eAgMnSc7d1	známý
reformátorem	reformátor	k1gMnSc7	reformátor
rakouské	rakouský	k2eAgFnSc2d1	rakouská
císařské	císařský	k2eAgFnSc2d1	císařská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
již	již	k6eAd1	již
Páté	pátá	k1gFnSc6	pátá
koaliční	koaliční	k2eAgFnSc6d1	koaliční
válce	válka	k1gFnSc6	válka
vedl	vést	k5eAaImAgInS	vést
svou	svůj	k3xOyFgFnSc4	svůj
brigádu	brigáda	k1gFnSc4	brigáda
do	do	k7c2	do
boje	boj	k1gInSc2	boj
u	u	k7c2	u
Welsu	Wels	k1gInSc2	Wels
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
tato	tento	k3xDgFnSc1	tento
válka	válka	k1gFnSc1	válka
však	však	k9	však
neskončila	skončit	k5eNaPmAgFnS	skončit
úspěchem	úspěch	k1gInSc7	úspěch
rakouských	rakouský	k2eAgFnPc2d1	rakouská
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opětovné	opětovný	k2eAgFnSc6d1	opětovná
porážce	porážka	k1gFnSc6	porážka
Rakouska	Rakousko	k1gNnSc2	Rakousko
se	se	k3xPyFc4	se
Radecký	Radecký	k1gMnSc1	Radecký
stal	stát	k5eAaPmAgMnS	stát
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
náčelníkem	náčelník	k1gInSc7	náčelník
stálého	stálý	k2eAgInSc2d1	stálý
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
na	na	k7c6	na
reorganizaci	reorganizace	k1gFnSc6	reorganizace
rakouské	rakouský	k2eAgFnSc2d1	rakouská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gInPc2	jeho
návrhů	návrh	k1gInPc2	návrh
reformy	reforma	k1gFnSc2	reforma
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
jako	jako	k8xS	jako
příliš	příliš	k6eAd1	příliš
nákladná	nákladný	k2eAgNnPc1d1	nákladné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1813	[number]	k4	1813
jej	on	k3xPp3gMnSc4	on
generalissimus	generalissimus	k1gMnSc1	generalissimus
rakouských	rakouský	k2eAgFnPc2d1	rakouská
vojsk	vojsko	k1gNnPc2	vojsko
kníže	kníže	k1gMnSc1	kníže
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
Filip	Filip	k1gMnSc1	Filip
ze	z	k7c2	z
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
náčelníkem	náčelník	k1gMnSc7	náčelník
svého	svůj	k3xOyFgInSc2	svůj
štábu	štáb	k1gInSc2	štáb
<g/>
,	,	kIx,	,
kteroužto	kteroužto	k?	kteroužto
hodnost	hodnost	k1gFnSc1	hodnost
si	se	k3xPyFc3	se
podržel	podržet	k5eAaPmAgMnS	podržet
během	během	k7c2	během
celého	celý	k2eAgNnSc2d1	celé
tažení	tažení	k1gNnSc2	tažení
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
autorem	autor	k1gMnSc7	autor
spojeneckých	spojenecký	k2eAgInPc2d1	spojenecký
plánů	plán	k1gInPc2	plán
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
protinapoleonském	protinapoleonský	k2eAgNnSc6d1	protinapoleonský
tažení	tažení	k1gNnSc6	tažení
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
různé	různý	k2eAgFnPc4d1	různá
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prosazováním	prosazování	k1gNnSc7	prosazování
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
velitelů	velitel	k1gMnPc2	velitel
nepopulárních	populární	k2eNgFnPc2d1	nepopulární
vojenských	vojenský	k2eAgFnPc2d1	vojenská
reforem	reforma	k1gFnPc2	reforma
si	se	k3xPyFc3	se
udělal	udělat	k5eAaPmAgInS	udělat
mnoho	mnoho	k4c4	mnoho
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jej	on	k3xPp3gMnSc4	on
nakonec	nakonec	k6eAd1	nakonec
roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
na	na	k7c4	na
čas	čas	k1gInSc4	čas
odklidili	odklidit	k5eAaPmAgMnP	odklidit
na	na	k7c4	na
post	post	k1gInSc4	post
velitele	velitel	k1gMnSc2	velitel
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
pevnosti	pevnost	k1gFnSc2	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1829	[number]	k4	1829
<g/>
–	–	k?	–
<g/>
1831	[number]	k4	1831
přebýval	přebývat	k5eAaImAgMnS	přebývat
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
sídlil	sídlit	k5eAaImAgInS	sídlit
zde	zde	k6eAd1	zde
v	v	k7c6	v
Edelmannově	Edelmannův	k2eAgInSc6d1	Edelmannův
paláci	palác	k1gInSc6	palác
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
tam	tam	k6eAd1	tam
má	mít	k5eAaImIp3nS	mít
pamětní	pamětní	k2eAgFnSc4d1	pamětní
desku	deska	k1gFnSc4	deska
<g/>
)	)	kIx)	)
na	na	k7c6	na
Horním	horní	k2eAgNnSc6d1	horní
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
sanitární	sanitární	k2eAgFnPc4d1	sanitární
podmínky	podmínka	k1gFnPc4	podmínka
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
vysušoval	vysušovat	k5eAaImAgInS	vysušovat
okolní	okolní	k2eAgInPc4d1	okolní
mokřady	mokřad	k1gInPc4	mokřad
a	a	k8xC	a
vysazoval	vysazovat	k5eAaImAgMnS	vysazovat
stromy	strom	k1gInPc7	strom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1831	[number]	k4	1831
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
rakouských	rakouský	k2eAgNnPc2d1	rakouské
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
a	a	k8xC	a
provedl	provést	k5eAaPmAgInS	provést
jejich	jejich	k3xOp3gFnSc4	jejich
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
reorganizaci	reorganizace	k1gFnSc4	reorganizace
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
sedmdesátým	sedmdesátý	k4xOgInPc3	sedmdesátý
narozeninám	narozeniny	k1gFnPc3	narozeniny
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
obdržel	obdržet	k5eAaPmAgInS	obdržet
hodnost	hodnost	k1gFnSc4	hodnost
polního	polní	k2eAgMnSc2d1	polní
maršálka	maršálek	k1gMnSc2	maršálek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
vpadla	vpadnout	k5eAaPmAgFnS	vpadnout
do	do	k7c2	do
Lombardie	Lombardie	k1gFnSc2	Lombardie
sardinská	sardinský	k2eAgFnSc1d1	sardinská
armáda	armáda	k1gFnSc1	armáda
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
sjednotit	sjednotit	k5eAaPmF	sjednotit
Itálii	Itálie	k1gFnSc4	Itálie
pod	pod	k7c4	pod
nadvládu	nadvláda	k1gFnSc4	nadvláda
Savojských	savojský	k2eAgMnPc2d1	savojský
a	a	k8xC	a
osvobodit	osvobodit	k5eAaPmF	osvobodit
ji	on	k3xPp3gFnSc4	on
tak	tak	k6eAd1	tak
z	z	k7c2	z
cizí	cizí	k2eAgFnSc2d1	cizí
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
mužů	muž	k1gMnPc2	muž
byl	být	k5eAaImAgMnS	být
Radecký	Radecký	k1gMnSc1	Radecký
nucen	nutit	k5eAaImNgMnS	nutit
vyklidit	vyklidit	k5eAaPmF	vyklidit
většinu	většina	k1gFnSc4	většina
rakouských	rakouský	k2eAgNnPc2d1	rakouské
území	území	k1gNnPc2	území
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
udržel	udržet	k5eAaPmAgInS	udržet
však	však	k9	však
proti	proti	k7c3	proti
značné	značný	k2eAgFnSc3d1	značná
přesile	přesila	k1gFnSc3	přesila
nepřítele	nepřítel	k1gMnSc2	nepřítel
pevnost	pevnost	k1gFnSc4	pevnost
Veronu	Verona	k1gFnSc4	Verona
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
konečně	konečně	k6eAd1	konečně
dostal	dostat	k5eAaPmAgMnS	dostat
posily	posila	k1gFnPc4	posila
<g/>
,	,	kIx,	,
porazil	porazit	k5eAaPmAgMnS	porazit
podstatně	podstatně	k6eAd1	podstatně
početnější	početní	k2eAgNnPc4d2	početnější
spojená	spojený	k2eAgNnPc4d1	spojené
vojska	vojsko	k1gNnPc4	vojsko
italských	italský	k2eAgMnPc2d1	italský
revolucionářů	revolucionář	k1gMnPc2	revolucionář
a	a	k8xC	a
sardinského	sardinský	k2eAgNnSc2d1	Sardinské
království	království	k1gNnSc2	království
v	v	k7c6	v
bitvách	bitva	k1gFnPc6	bitva
u	u	k7c2	u
Custozy	Custoz	k1gInPc1	Custoz
a	a	k8xC	a
u	u	k7c2	u
Novary	Novara	k1gFnSc2	Novara
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
rezignaci	rezignace	k1gFnSc3	rezignace
sardinského	sardinský	k2eAgMnSc2d1	sardinský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
Alberta	Albert	k1gMnSc2	Albert
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
těchto	tento	k3xDgFnPc2	tento
jeho	jeho	k3xOp3gFnPc2	jeho
vítězství	vítězství	k1gNnSc2	vítězství
složil	složit	k5eAaPmAgMnS	složit
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
rakouský	rakouský	k2eAgMnSc1d1	rakouský
skladatel	skladatel	k1gMnSc1	skladatel
Johann	Johanna	k1gFnPc2	Johanna
Strauss	Straussa	k1gFnPc2	Straussa
starší	starý	k2eAgInSc1d2	starší
slavný	slavný	k2eAgInSc1d1	slavný
Radeckého	Radeckého	k2eAgInSc1d1	Radeckého
pochod	pochod	k1gInSc1	pochod
a	a	k8xC	a
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
ho	on	k3xPp3gMnSc4	on
ocenil	ocenit	k5eAaPmAgMnS	ocenit
titulem	titul	k1gInSc7	titul
ruského	ruský	k2eAgMnSc4d1	ruský
generála	generál	k1gMnSc4	generál
polního	polní	k2eAgMnSc4d1	polní
maršálka	maršálek	k1gMnSc4	maršálek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
napětím	napětí	k1gNnSc7	napětí
mezi	mezi	k7c7	mezi
Pruskem	Prusko	k1gNnSc7	Prusko
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
poslán	poslat	k5eAaPmNgMnS	poslat
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
dvou	dva	k4xCgInPc2	dva
armádních	armádní	k2eAgMnPc2d1	armádní
sborů	sbor	k1gInPc2	sbor
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
asi	asi	k9	asi
14	[number]	k4	14
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
do	do	k7c2	do
Olomouce	Olomouc	k1gFnSc2	Olomouc
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
manifestace	manifestace	k1gFnSc1	manifestace
síly	síla	k1gFnSc2	síla
nakonec	nakonec	k6eAd1	nakonec
významně	významně	k6eAd1	významně
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Olomoucké	olomoucký	k2eAgFnPc1d1	olomoucká
punktace	punktace	k1gFnPc1	punktace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1849	[number]	k4	1849
–	–	k?	–
1857	[number]	k4	1857
byl	být	k5eAaImAgMnS	být
Radecký	Radecký	k1gMnSc1	Radecký
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
Lombardsko-benátského	lombardskoenátský	k2eAgNnSc2d1	lombardsko-benátské
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k8xS	jako
5	[number]	k4	5
<g/>
.	.	kIx.	.
vicekrál	vicekrál	k1gMnSc1	vicekrál
<g/>
)	)	kIx)	)
a	a	k8xC	a
velitelem	velitel	k1gMnSc7	velitel
II	II	kA	II
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgFnSc4d1	aktivní
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
kariéru	kariéra	k1gFnSc4	kariéra
ukončil	ukončit	k5eAaPmAgMnS	ukončit
po	po	k7c6	po
72	[number]	k4	72
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rodinný	rodinný	k2eAgInSc1d1	rodinný
život	život	k1gInSc1	život
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1798	[number]	k4	1798
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Františkou	Františka	k1gFnSc7	Františka
<g/>
,	,	kIx,	,
hraběnkou	hraběnka	k1gFnSc7	hraběnka
Strassoldo-Grafenberg	Strassoldo-Grafenberg	k1gInSc1	Strassoldo-Grafenberg
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zemřela	zemřít	k5eAaPmAgFnS	zemřít
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
manželství	manželství	k1gNnSc2	manželství
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
pět	pět	k4xCc1	pět
synů	syn	k1gMnPc2	syn
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
otce	otec	k1gMnSc4	otec
přežili	přežít	k5eAaPmAgMnP	přežít
pouze	pouze	k6eAd1	pouze
syn	syn	k1gMnSc1	syn
Theodor	Theodor	k1gMnSc1	Theodor
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
hraběnka	hraběnka	k1gFnSc1	hraběnka
Wenkheimová	Wenkheimová	k1gFnSc1	Wenkheimová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
penzionování	penzionování	k1gNnSc6	penzionování
Radecký	Radecký	k1gMnSc1	Radecký
dožíval	dožívat	k5eAaImAgMnS	dožívat
ještě	ještě	k9	ještě
dvě	dva	k4xCgNnPc1	dva
desetiletí	desetiletí	k1gNnPc1	desetiletí
<g/>
;	;	kIx,	;
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
stále	stále	k6eAd1	stále
vedli	vést	k5eAaImAgMnP	vést
nákladný	nákladný	k2eAgInSc4d1	nákladný
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
zadlužili	zadlužit	k5eAaPmAgMnP	zadlužit
<g/>
.	.	kIx.	.
</s>
<s>
Radeckého	Radeckého	k2eAgInPc1d1	Radeckého
dluhy	dluh	k1gInPc1	dluh
platil	platit	k5eAaImAgMnS	platit
mecenáš	mecenáš	k1gMnSc1	mecenáš
Joseph	Joseph	k1gMnSc1	Joseph
Gottfried	Gottfried	k1gMnSc1	Gottfried
Pargfrieder	Pargfrieder	k1gMnSc1	Pargfrieder
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
určil	určit	k5eAaPmAgMnS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Radecký	Radecký	k1gMnSc1	Radecký
bude	být	k5eAaImBp3nS	být
pohřben	pohřben	k2eAgMnSc1d1	pohřben
ve	v	k7c6	v
Wetzdorfu	Wetzdorf	k1gInSc6	Wetzdorf
v	v	k7c6	v
památníku	památník	k1gInSc6	památník
rakouské	rakouský	k2eAgFnSc2d1	rakouská
armády	armáda	k1gFnSc2	armáda
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Heldenberg	Heldenberg	k1gInSc1	Heldenberg
v	v	k7c6	v
dolním	dolní	k2eAgNnSc6d1	dolní
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
v	v	k7c6	v
samostatné	samostatný	k2eAgFnSc6d1	samostatná
kryptě	krypta	k1gFnSc6	krypta
<g/>
,	,	kIx,	,
kryté	krytý	k2eAgFnSc6d1	krytá
obeliskem	obelisk	k1gInSc7	obelisk
<g/>
,	,	kIx,	,
po	po	k7c6	po
boku	bok	k1gInSc6	bok
generála	generál	k1gMnSc2	generál
Wimpfena	Wimpfen	k1gMnSc2	Wimpfen
<g/>
.	.	kIx.	.
</s>
<s>
Přáním	přání	k1gNnSc7	přání
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
bylo	být	k5eAaImAgNnS	být
pohřbít	pohřbít	k5eAaPmF	pohřbít
Radeckého	Radecký	k1gMnSc4	Radecký
v	v	k7c6	v
Kapucínské	kapucínský	k2eAgFnSc6d1	Kapucínská
kryptě	krypta	k1gFnSc6	krypta
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
pohřbem	pohřeb	k1gInSc7	pohřeb
bylo	být	k5eAaImAgNnS	být
tělo	tělo	k1gNnSc1	tělo
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
poctami	pocta	k1gFnPc7	pocta
ve	v	k7c6	v
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
Arsenalu	Arsenal	k1gInSc6	Arsenal
a	a	k8xC	a
ve	v	k7c6	v
Svatoštěpánském	svatoštěpánský	k2eAgInSc6d1	svatoštěpánský
dómu	dóm	k1gInSc6	dóm
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
smuteční	smuteční	k2eAgFnSc1d1	smuteční
slavnost	slavnost	k1gFnSc1	slavnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Legendární	legendární	k2eAgFnPc4d1	legendární
epizody	epizoda	k1gFnPc4	epizoda
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
legendy	legenda	k1gFnSc2	legenda
si	se	k3xPyFc3	se
Radecký	Radecký	k1gMnSc1	Radecký
v	v	k7c6	v
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
jako	jako	k9	jako
čerstvý	čerstvý	k2eAgMnSc1d1	čerstvý
kadet	kadet	k1gMnSc1	kadet
kyrysnického	kyrysnický	k2eAgInSc2d1	kyrysnický
pluku	pluk	k1gInSc2	pluk
dal	dát	k5eAaPmAgMnS	dát
věštit	věštit	k5eAaImF	věštit
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
životní	životní	k2eAgInPc4d1	životní
osudy	osud	k1gInPc4	osud
od	od	k7c2	od
staré	starý	k2eAgFnSc2d1	stará
cikánky	cikánka	k1gFnSc2	cikánka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
údajně	údajně	k6eAd1	údajně
z	z	k7c2	z
čar	čára	k1gFnPc2	čára
na	na	k7c6	na
dlani	dlaň	k1gFnSc6	dlaň
předpověděla	předpovědět	k5eAaPmAgFnS	předpovědět
skvělou	skvělý	k2eAgFnSc4d1	skvělá
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sejde	sejít	k5eAaPmIp3nS	sejít
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
jinou	jiný	k2eAgFnSc7d1	jiná
než	než	k8xS	než
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
předpověď	předpověď	k1gFnSc1	předpověď
se	se	k3xPyFc4	se
splnila	splnit	k5eAaPmAgFnS	splnit
<g/>
:	:	kIx,	:
Radecký	Radecký	k1gMnSc1	Radecký
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jako	jako	k9	jako
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
legendou	legenda	k1gFnSc7	legenda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezemřel	zemřít	k5eNaPmAgMnS	zemřít
na	na	k7c6	na
válečném	válečný	k2eAgNnSc6d1	válečné
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Uklouzl	uklouznout	k5eAaPmAgMnS	uklouznout
na	na	k7c6	na
navoskované	navoskovaný	k2eAgFnSc6d1	navoskovaná
podlaze	podlaha	k1gFnSc6	podlaha
v	v	k7c6	v
pokoji	pokoj	k1gInSc6	pokoj
své	svůj	k3xOyFgFnSc2	svůj
rezidence	rezidence	k1gFnSc2	rezidence
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Pádem	Pád	k1gInSc7	Pád
si	se	k3xPyFc3	se
zlomil	zlomit	k5eAaPmAgMnS	zlomit
krček	krček	k1gInSc4	krček
stehenní	stehenní	k2eAgFnSc2d1	stehenní
kosti	kost	k1gFnSc2	kost
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
zranění	zranění	k1gNnPc2	zranění
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
jednadevadesáti	jednadevadesát	k4xCc2	jednadevadesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
gastronomické	gastronomický	k2eAgFnSc2d1	gastronomická
tradice	tradice	k1gFnSc2	tradice
si	se	k3xPyFc3	se
Josef	Josef	k1gMnSc1	Josef
Radecký	Radecký	k1gMnSc1	Radecký
již	již	k6eAd1	již
v	v	k7c4	v
mládí	mládí	k1gNnSc4	mládí
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
milánskou	milánský	k2eAgFnSc4d1	Milánská
specialitu	specialita	k1gFnSc4	specialita
<g/>
,	,	kIx,	,
telecí	telecí	k2eAgInSc4d1	telecí
řízek	řízek	k1gInSc4	řízek
<g/>
,	,	kIx,	,
smažený	smažený	k2eAgInSc4d1	smažený
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
strouhanky	strouhanka	k1gFnSc2	strouhanka
a	a	k8xC	a
sýra	sýr	k1gInSc2	sýr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
osobní	osobní	k2eAgMnSc1d1	osobní
kuchař	kuchař	k1gMnSc1	kuchař
tuto	tento	k3xDgFnSc4	tento
pochoutku	pochoutka	k1gFnSc4	pochoutka
přivezl	přivézt	k5eAaPmAgMnS	přivézt
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
vynechal	vynechat	k5eAaPmAgInS	vynechat
strouhaný	strouhaný	k2eAgInSc1d1	strouhaný
sýr	sýr	k1gInSc1	sýr
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
klasický	klasický	k2eAgInSc4d1	klasický
vídeňský	vídeňský	k2eAgInSc4d1	vídeňský
řízek	řízek	k1gInSc4	řízek
v	v	k7c6	v
trojobalu	trojobal	k1gInSc6	trojobal
<g/>
.	.	kIx.	.
</s>
<s>
Radecký	Radecký	k1gMnSc1	Radecký
byl	být	k5eAaImAgMnS	být
proslulý	proslulý	k2eAgMnSc1d1	proslulý
gourmet	gourmet	k1gMnSc1	gourmet
a	a	k8xC	a
zadlužil	zadlužit	k5eAaPmAgMnS	zadlužit
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
proslulým	proslulý	k2eAgFnPc3d1	proslulá
hostinám	hostina	k1gFnPc3	hostina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
pořádal	pořádat	k5eAaImAgMnS	pořádat
až	až	k6eAd1	až
do	do	k7c2	do
pozdního	pozdní	k2eAgInSc2d1	pozdní
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Radeckého	Radeckého	k2eAgFnSc4d1	Radeckého
pochoutku	pochoutka	k1gFnSc4	pochoutka
brzy	brzy	k6eAd1	brzy
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gMnSc1	jeho
obdivovatel	obdivovatel	k1gMnSc1	obdivovatel
<g/>
,	,	kIx,	,
mladý	mladý	k2eAgMnSc1d1	mladý
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
</s>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
a	a	k8xC	a
současnost	současnost	k1gFnSc1	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
muzeum	muzeum	k1gNnSc1	muzeum
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
(	(	kIx(	(
<g/>
Heeresgeschichtliches	Heeresgeschichtliches	k1gMnSc1	Heeresgeschichtliches
Museum	museum	k1gNnSc4	museum
<g/>
,	,	kIx,	,
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
)	)	kIx)	)
věnovalo	věnovat	k5eAaPmAgNnS	věnovat
Radeckého	Radeckého	k2eAgFnSc3d1	Radeckého
vojenské	vojenský	k2eAgFnSc3d1	vojenská
kariéře	kariéra	k1gFnSc3	kariéra
jednu	jeden	k4xCgFnSc4	jeden
pamětní	pamětní	k2eAgFnSc4d1	pamětní
síň	síň	k1gFnSc4	síň
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc2	jenž
jsou	být	k5eAaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
jeho	jeho	k3xOp3gInPc1	jeho
osobní	osobní	k2eAgInPc1d1	osobní
předměty	předmět	k1gInPc1	předmět
<g/>
,	,	kIx,	,
jako	jako	k9	jako
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
jezdecké	jezdecký	k2eAgNnSc4d1	jezdecké
sedlo	sedlo	k1gNnSc4	sedlo
<g/>
,	,	kIx,	,
šavle	šavle	k1gFnPc4	šavle
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc4	některý
ukořistěné	ukořistěný	k2eAgInPc1d1	ukořistěný
prapory	prapor	k1gInPc1	prapor
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
vojenské	vojenský	k2eAgInPc1d1	vojenský
řády	řád	k1gInPc1	řád
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
<g/>
,	,	kIx,	,
posmrtná	posmrtný	k2eAgFnSc1d1	posmrtná
maska	maska	k1gFnSc1	maska
a	a	k8xC	a
dopis	dopis	k1gInSc1	dopis
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
císaři	císař	k1gMnSc3	císař
Františku	František	k1gMnSc3	František
Josefovi	Josef	k1gMnSc3	Josef
I.	I.	kA	I.
Expozici	expozice	k1gFnSc4	expozice
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
olejomalby	olejomalba	k1gFnPc1	olejomalba
vítězných	vítězný	k2eAgFnPc2d1	vítězná
bitev	bitva	k1gFnPc2	bitva
<g/>
,	,	kIx,	,
bronzová	bronzový	k2eAgFnSc1d1	bronzová
portrétní	portrétní	k2eAgFnSc1d1	portrétní
busta	busta	k1gFnSc1	busta
a	a	k8xC	a
model	model	k1gInSc1	model
sochy	socha	k1gFnSc2	socha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pomník	pomník	k1gInSc4	pomník
malostranský	malostranský	k2eAgInSc4d1	malostranský
===	===	k?	===
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
pomníků	pomník	k1gInPc2	pomník
Radeckého	Radecký	k1gMnSc2	Radecký
byl	být	k5eAaImAgInS	být
iniciován	iniciovat	k5eAaBmNgInS	iniciovat
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
u	u	k7c2	u
Novarry	Novarra	k1gFnSc2	Novarra
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byl	být	k5eAaImAgInS	být
naskicován	naskicovat	k5eAaPmNgInS	naskicovat
na	na	k7c4	na
zeď	zeď	k1gFnSc4	zeď
Akademie	akademie	k1gFnSc2	akademie
malířem	malíř	k1gMnSc7	malíř
prof.	prof.	kA	prof.
Christianem	Christian	k1gMnSc7	Christian
Rubenem	Ruben	k1gMnSc7	Ruben
<g/>
.	.	kIx.	.
</s>
<s>
Sochařskou	sochařský	k2eAgFnSc4d1	sochařská
podobu	podoba	k1gFnSc4	podoba
mu	on	k3xPp3gMnSc3	on
vtiskli	vtisknout	k5eAaPmAgMnP	vtisknout
sochaři	sochař	k1gMnPc1	sochař
bratři	bratr	k1gMnPc1	bratr
Josef	Josef	k1gMnSc1	Josef
Max	Max	k1gMnSc1	Max
(	(	kIx(	(
<g/>
sochy	socha	k1gFnPc1	socha
vojáků	voják	k1gMnPc2	voják
<g/>
)	)	kIx)	)
a	a	k8xC	a
Emanuel	Emanuel	k1gMnSc1	Emanuel
Max	Max	k1gMnSc1	Max
(	(	kIx(	(
<g/>
socha	socha	k1gFnSc1	socha
maršálka	maršálka	k1gFnSc1	maršálka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kovu	kov	k1gInSc2	kov
ukořistěných	ukořistěný	k2eAgNnPc2d1	ukořistěné
děl	dělo	k1gNnPc2	dělo
italské	italský	k2eAgFnSc2d1	italská
armády	armáda	k1gFnSc2	armáda
na	na	k7c6	na
Sardinii	Sardinie	k1gFnSc6	Sardinie
jej	on	k3xPp3gMnSc4	on
odlili	odlít	k5eAaPmAgMnP	odlít
Jakob	Jakob	k1gMnSc1	Jakob
Daniel	Daniel	k1gMnSc1	Daniel
Burgschmiet	Burgschmiet	k1gMnSc1	Burgschmiet
a	a	k8xC	a
Christoph	Christoph	k1gMnSc1	Christoph
Lenz	Lenz	k1gMnSc1	Lenz
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
byl	být	k5eAaImAgInS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
až	až	k9	až
po	po	k7c6	po
maršálkově	maršálkův	k2eAgFnSc6d1	maršálkův
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
<g/>
.	.	kIx.	.
</s>
<s>
Stál	stát	k5eAaImAgMnS	stát
v	v	k7c6	v
letech	let	k1gInPc6	let
1858	[number]	k4	1858
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
na	na	k7c6	na
dolním	dolní	k2eAgNnSc6d1	dolní
Malostranském	malostranský	k2eAgNnSc6d1	Malostranské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
tramvajového	tramvajový	k2eAgInSc2d1	tramvajový
ostrůvku	ostrůvek	k1gInSc2	ostrůvek
proti	proti	k7c3	proti
tehdejší	tehdejší	k2eAgFnSc3d1	tehdejší
kavárně	kavárna	k1gFnSc3	kavárna
Caffé	Caffý	k2eAgFnSc2d1	Caffý
Radetzky	Radetzka	k1gFnSc2	Radetzka
(	(	kIx(	(
<g/>
Grömlingovský	Grömlingovský	k2eAgInSc1d1	Grömlingovský
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
po	po	k7c6	po
zboření	zboření	k1gNnSc6	zboření
brány	brána	k1gFnSc2	brána
v	v	k7c6	v
Karmelitské	karmelitský	k2eAgFnSc6d1	Karmelitská
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
zavedení	zavedení	k1gNnSc6	zavedení
koňské	koňský	k2eAgFnSc2d1	koňská
dráhy	dráha	k1gFnSc2	dráha
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
asi	asi	k9	asi
o	o	k7c4	o
pět	pět	k4xCc4	pět
metrů	metr	k1gInPc2	metr
přesunout	přesunout	k5eAaPmF	přesunout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
osamostatnění	osamostatnění	k1gNnSc6	osamostatnění
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
Italské	italský	k2eAgInPc1d1	italský
velvyslanectví	velvyslanectví	k1gNnSc4	velvyslanectví
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
usídlilo	usídlit	k5eAaPmAgNnS	usídlit
v	v	k7c6	v
nedalekém	daleký	k2eNgInSc6d1	nedaleký
Thunovském	Thunovský	k2eAgInSc6d1	Thunovský
(	(	kIx(	(
<g/>
Kolovratském	Kolovratský	k2eAgInSc6d1	Kolovratský
<g/>
)	)	kIx)	)
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Nerudově	Nerudův	k2eAgFnSc6d1	Nerudova
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
protestovalo	protestovat	k5eAaBmAgNnS	protestovat
proti	proti	k7c3	proti
Radeckému	Radecký	k1gMnSc3	Radecký
jako	jako	k8xS	jako
nepříteli	nepřítel	k1gMnSc3	nepřítel
sjednocení	sjednocení	k1gNnSc3	sjednocení
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
pomník	pomník	k1gInSc4	pomník
v	v	k7c6	v
letech	let	k1gInPc6	let
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
rozebrán	rozebrán	k2eAgInSc1d1	rozebrán
<g/>
.	.	kIx.	.
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
podstavec	podstavec	k1gInSc1	podstavec
z	z	k7c2	z
leštěné	leštěný	k2eAgFnSc2d1	leštěná
žuly	žula	k1gFnSc2	žula
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
bronzovými	bronzový	k2eAgFnPc7d1	bronzová
tabulkami	tabulka	k1gFnPc7	tabulka
se	se	k3xPyFc4	se
jmény	jméno	k1gNnPc7	jméno
bitev	bitva	k1gFnPc2	bitva
(	(	kIx(	(
<g/>
Verona	Verona	k1gFnSc1	Verona
<g/>
,	,	kIx,	,
Vicenza	Vicenza	k1gFnSc1	Vicenza
<g/>
,	,	kIx,	,
Custozza	Custozza	k1gFnSc1	Custozza
<g/>
,	,	kIx,	,
Novara	Novara	k1gFnSc1	Novara
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
<g/>
,	,	kIx,	,
bronzová	bronzový	k2eAgFnSc1d1	bronzová
sochařská	sochařský	k2eAgFnSc1d1	sochařská
část	část	k1gFnSc1	část
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
676	[number]	k4	676
centimetrů	centimetr	k1gInPc2	centimetr
a	a	k8xC	a
váze	váha	k1gFnSc6	váha
přes	přes	k7c4	přes
10	[number]	k4	10
tun	tuna	k1gFnPc2	tuna
byla	být	k5eAaImAgFnS	být
rozebrána	rozebrat	k5eAaPmNgFnS	rozebrat
a	a	k8xC	a
přenesena	přenést	k5eAaPmNgFnS	přenést
do	do	k7c2	do
Lapidária	lapidárium	k1gNnSc2	lapidárium
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
7	[number]	k4	7
na	na	k7c6	na
Pražském	pražský	k2eAgNnSc6d1	Pražské
výstavišti	výstaviště	k1gNnSc6	výstaviště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
dodnes	dodnes	k6eAd1	dodnes
společně	společně	k6eAd1	společně
s	s	k7c7	s
modelem	model	k1gInSc7	model
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
z	z	k7c2	z
papírmašé	papírmašá	k1gFnSc2	papírmašá
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
představuje	představovat	k5eAaImIp3nS	představovat
prostovlasého	prostovlasý	k2eAgMnSc4d1	prostovlasý
vojevůdce	vojevůdce	k1gMnSc4	vojevůdce
<g/>
,	,	kIx,	,
rozkročeného	rozkročený	k2eAgMnSc4d1	rozkročený
jako	jako	k8xC	jako
vítěze	vítěz	k1gMnSc4	vítěz
na	na	k7c6	na
kruhovém	kruhový	k2eAgInSc6d1	kruhový
vypuklém	vypuklý	k2eAgInSc6d1	vypuklý
štítu	štít	k1gInSc6	štít
<g/>
,	,	kIx,	,
s	s	k7c7	s
maršálskou	maršálský	k2eAgFnSc7d1	maršálská
holí	hole	k1gFnSc7	hole
v	v	k7c6	v
pravici	pravice	k1gFnSc6	pravice
a	a	k8xC	a
s	s	k7c7	s
vítězným	vítězný	k2eAgInSc7d1	vítězný
praporem	prapor	k1gInSc7	prapor
z	z	k7c2	z
válek	válka	k1gFnPc2	válka
o	o	k7c6	o
Lombardii	Lombardie	k1gFnSc6	Lombardie
<g/>
.	.	kIx.	.
</s>
<s>
Kruhový	kruhový	k2eAgInSc1d1	kruhový
štít	štít	k1gInSc1	štít
po	po	k7c4	po
způsobu	způsoba	k1gFnSc4	způsoba
vojáků	voják	k1gMnPc2	voják
antického	antický	k2eAgInSc2d1	antický
Říma	Řím	k1gInSc2	Řím
nese	nést	k5eAaImIp3nS	nést
sedm	sedm	k4xCc1	sedm
představitelů	představitel	k1gMnPc2	představitel
vojsk	vojsko	k1gNnPc2	vojsko
rakouské	rakouský	k2eAgFnSc2d1	rakouská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
:	:	kIx,	:
myslivec	myslivec	k1gMnSc1	myslivec
<g/>
,	,	kIx,	,
dělostřelec	dělostřelec	k1gMnSc1	dělostřelec
(	(	kIx(	(
<g/>
ztotožňovaný	ztotožňovaný	k2eAgMnSc1d1	ztotožňovaný
s	s	k7c7	s
Čechem	Čech	k1gMnSc7	Čech
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
z	z	k7c2	z
Tyrol	Tyroly	k1gInPc2	Tyroly
<g/>
,	,	kIx,	,
husar	husar	k1gMnSc1	husar
<g/>
,	,	kIx,	,
námořník	námořník	k1gMnSc1	námořník
<g/>
,	,	kIx,	,
hulán	hulán	k1gMnSc1	hulán
a	a	k8xC	a
jihoslovanský	jihoslovanský	k2eAgMnSc1d1	jihoslovanský
serežan	serežan	k1gMnSc1	serežan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
předpokládané	předpokládaný	k2eAgFnSc2d1	předpokládaná
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
Malostranského	malostranský	k2eAgNnSc2d1	Malostranské
náměstí	náměstí	k1gNnSc2	náměstí
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c4	o
zhotovení	zhotovení	k1gNnSc4	zhotovení
kopie	kopie	k1gFnSc2	kopie
pomníku	pomník	k1gInSc2	pomník
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
umístění	umístění	k1gNnSc2	umístění
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
původního	původní	k2eAgNnSc2d1	původní
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Shromáždění	shromáždění	k1gNnSc1	shromáždění
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
obnovy	obnova	k1gFnSc2	obnova
pomníku	pomník	k1gInSc2	pomník
maršála	maršál	k1gMnSc2	maršál
Radeckého	Radecký	k1gMnSc2	Radecký
na	na	k7c6	na
Malostranském	malostranský	k2eAgNnSc6d1	Malostranské
náměstí	náměstí	k1gNnSc6	náměstí
pořádá	pořádat	k5eAaImIp3nS	pořádat
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
10	[number]	k4	10
letech	léto	k1gNnPc6	léto
každoročně	každoročně	k6eAd1	každoročně
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
Koruna	koruna	k1gFnSc1	koruna
Česká	český	k2eAgFnSc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
sdružení	sdružení	k1gNnSc1	sdružení
"	"	kIx"	"
<g/>
Spolek	spolek	k1gInSc1	spolek
Radecký	Radecký	k1gMnSc1	Radecký
Praha	Praha	k1gFnSc1	Praha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
obnova	obnova	k1gFnSc1	obnova
pomníku	pomník	k1gInSc2	pomník
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
získat	získat	k5eAaPmF	získat
podporu	podpora	k1gFnSc4	podpora
mnoha	mnoho	k4c2	mnoho
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
kulturní	kulturní	k2eAgFnPc4d1	kulturní
<g/>
,	,	kIx,	,
církevní	církevní	k2eAgFnPc4d1	církevní
i	i	k8xC	i
akademické	akademický	k2eAgFnPc4d1	akademická
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
kardinála	kardinál	k1gMnSc2	kardinál
Dominika	Dominik	k1gMnSc2	Dominik
Duky	Duka	k1gMnSc2	Duka
<g/>
,	,	kIx,	,
malíře	malíř	k1gMnSc2	malíř
Adolfa	Adolf	k1gMnSc2	Adolf
Borna	Born	k1gMnSc2	Born
či	či	k8xC	či
historika	historik	k1gMnSc2	historik
Jiřího	Jiří	k1gMnSc2	Jiří
Raka	rak	k1gMnSc2	rak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Lapidáriu	lapidárium	k1gNnSc6	lapidárium
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
Výstavišti	výstaviště	k1gNnSc6	výstaviště
v	v	k7c6	v
Holešovicích	Holešovice	k1gFnPc6	Holešovice
se	se	k3xPyFc4	se
u	u	k7c2	u
Radeckého	Radeckého	k2eAgInSc2d1	Radeckého
pomníku	pomník	k1gInSc2	pomník
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
pořádá	pořádat	k5eAaImIp3nS	pořádat
každoročně	každoročně	k6eAd1	každoročně
ve	v	k7c4	v
výroční	výroční	k2eAgInSc4d1	výroční
den	den	k1gInSc4	den
odhalení	odhalení	k1gNnSc2	odhalení
Radeckého	Radeckého	k2eAgInSc2d1	Radeckého
pomníku	pomník	k1gInSc2	pomník
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
pietní	pietní	k2eAgFnSc1d1	pietní
slavnost	slavnost	k1gFnSc1	slavnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
začala	začít	k5eAaPmAgFnS	začít
probíhat	probíhat	k5eAaImF	probíhat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Rakouským	rakouský	k2eAgNnSc7d1	rakouské
kulturním	kulturní	k2eAgNnSc7d1	kulturní
fórem	fórum	k1gNnSc7	fórum
jako	jako	k8xS	jako
společná	společný	k2eAgFnSc1d1	společná
česko-rakouská	českoakouský	k2eAgFnSc1d1	česko-rakouská
slavnost	slavnost	k1gFnSc1	slavnost
<g/>
..	..	k?	..
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
jejím	její	k3xOp3gMnSc7	její
pořadatelem	pořadatel	k1gMnSc7	pořadatel
je	být	k5eAaImIp3nS	být
Spolek	spolek	k1gInSc1	spolek
Radecký	Radecký	k1gMnSc1	Radecký
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgInPc4d1	ostatní
pomníky	pomník	k1gInPc4	pomník
===	===	k?	===
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc1d1	další
Radeckého	Radeckého	k2eAgInSc1d1	Radeckého
jezdecký	jezdecký	k2eAgInSc1d1	jezdecký
pomník	pomník	k1gInSc1	pomník
byl	být	k5eAaImAgInS	být
vztyčen	vztyčit	k5eAaPmNgInS	vztyčit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
před	před	k7c7	před
císařským	císařský	k2eAgInSc7d1	císařský
dvorním	dvorní	k2eAgInSc7d1	dvorní
kostelem	kostel	k1gInSc7	kostel
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Am	Am	k1gMnSc2	Am
Hof	Hof	k1gMnSc2	Hof
<g/>
,	,	kIx,	,
od	od	k7c2	od
rakouského	rakouský	k2eAgMnSc2d1	rakouský
dvorního	dvorní	k2eAgMnSc2d1	dvorní
sochaře	sochař	k1gMnSc2	sochař
Caspara	Caspara	k1gFnSc1	Caspara
von	von	k1gInSc1	von
Zumbusch	Zumbusch	k1gInSc1	Zumbusch
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
Okružní	okružní	k2eAgFnSc6d1	okružní
třídě	třída	k1gFnSc6	třída
(	(	kIx(	(
<g/>
Ringstrasse	Ringstrass	k1gInSc6	Ringstrass
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
model	model	k1gInSc1	model
ve	v	k7c6	v
stříbře	stříbro	k1gNnSc6	stříbro
jako	jako	k8xC	jako
čestný	čestný	k2eAgInSc4d1	čestný
dar	dar	k1gInSc4	dar
provedla	provést	k5eAaPmAgFnS	provést
vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
firma	firma	k1gFnSc1	firma
J.	J.	kA	J.
C.	C.	kA	C.
Klinkosch	Klinkoscha	k1gFnPc2	Klinkoscha
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
stojící	stojící	k2eAgFnSc2d1	stojící
pěší	pěší	k2eAgFnSc2d1	pěší
figury	figura	k1gFnSc2	figura
z	z	k7c2	z
bílého	bílý	k2eAgInSc2d1	bílý
mramoru	mramor	k1gInSc2	mramor
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
v	v	k7c6	v
galerii	galerie	k1gFnSc6	galerie
slavných	slavný	k2eAgMnPc2d1	slavný
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
ve	v	k7c6	v
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
Arsenalu	Arsenal	k1gInSc6	Arsenal
<g/>
,	,	kIx,	,
týž	týž	k3xTgInSc4	týž
v	v	k7c6	v
bronzovém	bronzový	k2eAgInSc6d1	bronzový
odlitku	odlitek	k1gInSc6	odlitek
stál	stát	k5eAaImAgInS	stát
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Lublani	Lublaň	k1gFnSc6	Lublaň
<g/>
,	,	kIx,	,
tamže	tamže	k?	tamže
v	v	k7c6	v
sadech	sad	k1gInPc6	sad
stávala	stávat	k5eAaImAgFnS	stávat
Radeckého	Radeckého	k2eAgFnSc1d1	Radeckého
portrétní	portrétní	k2eAgFnSc1d1	portrétní
busta	busta	k1gFnSc1	busta
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
býval	bývat	k5eAaImAgInS	bývat
pomník	pomník	k1gInSc1	pomník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
české	český	k2eAgInPc1d1	český
návrhy	návrh	k1gInPc1	návrh
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
pomníky	pomník	k1gInPc4	pomník
Radeckého	Radeckého	k2eAgInPc4d1	Radeckého
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgInP	dochovat
v	v	k7c6	v
kresbách	kresba	k1gFnPc6	kresba
<g/>
,	,	kIx,	,
litografiích	litografie	k1gFnPc6	litografie
<g/>
,	,	kIx,	,
či	či	k8xC	či
v	v	k7c6	v
sériově	sériově	k6eAd1	sériově
vyráběných	vyráběný	k2eAgFnPc6d1	vyráběná
miniaturách	miniatura	k1gFnPc6	miniatura
v	v	k7c6	v
porcelánu	porcelán	k1gInSc6	porcelán
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
skle	sklo	k1gNnSc6	sklo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnPc1d1	další
památky	památka	k1gFnPc1	památka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
Radeckého	Radeckého	k2eAgFnSc2d1	Radeckého
pobytů	pobyt	k1gInPc2	pobyt
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
pamětní	pamětní	k2eAgFnPc1d1	pamětní
desky	deska	k1gFnPc1	deska
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Třebnici	Třebnice	k1gFnSc6	Třebnice
<g/>
,	,	kIx,	,
na	na	k7c6	na
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Duchcově	Duchcův	k2eAgInSc6d1	Duchcův
v	v	k7c6	v
Husově	Husův	k2eAgFnSc6d1	Husova
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
na	na	k7c6	na
kasárnách	kasárny	k1gFnPc6	kasárny
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgInP	dochovat
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
desítky	desítka	k1gFnPc4	desítka
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
pamětní	pamětní	k2eAgFnPc4d1	pamětní
medaile	medaile	k1gFnPc4	medaile
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
také	také	k9	také
Moravské	moravský	k2eAgNnSc1d1	Moravské
zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Památku	památka	k1gFnSc4	památka
svého	svůj	k3xOyFgMnSc2	svůj
rodáka	rodák	k1gMnSc2	rodák
připomíná	připomínat	k5eAaImIp3nS	připomínat
v	v	k7c6	v
expozici	expozice	k1gFnSc6	expozice
regionální	regionální	k2eAgNnSc4d1	regionální
muzeum	muzeum	k1gNnSc4	muzeum
v	v	k7c6	v
Sedlčanech	Sedlčany	k1gInPc6	Sedlčany
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Olomouc	Olomouc	k1gFnSc1	Olomouc
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
pořádá	pořádat	k5eAaImIp3nS	pořádat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
září	září	k1gNnSc2	září
každoročně	každoročně	k6eAd1	každoročně
"	"	kIx"	"
<g/>
Oslavy	oslava	k1gFnSc2	oslava
maršála	maršál	k1gMnSc4	maršál
Radeckého	Radecký	k1gMnSc4	Radecký
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
památníku	památník	k1gInSc6	památník
Walhalla	Walhallo	k1gNnSc2	Walhallo
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Donaustauf	Donaustauf	k1gInSc4	Donaustauf
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
,	,	kIx,	,
asi	asi	k9	asi
10	[number]	k4	10
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Řezna	Řezno	k1gNnSc2	Řezno
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
busta	busta	k1gFnSc1	busta
maršála	maršál	k1gMnSc2	maršál
Josefa	Josef	k1gMnSc2	Josef
Radeckého	Radecký	k1gMnSc2	Radecký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
byly	být	k5eAaImAgFnP	být
také	také	k9	také
pojmenovány	pojmenován	k2eAgFnPc1d1	pojmenována
rakousko-uherské	rakouskoherský	k2eAgFnPc1d1	rakousko-uherská
lodě	loď	k1gFnPc1	loď
<g/>
:	:	kIx,	:
šroubová	šroubový	k2eAgFnSc1d1	šroubová
fregata	fregata	k1gFnSc1	fregata
SMS	SMS	kA	SMS
Radetzky	Radetzka	k1gFnSc2	Radetzka
a	a	k8xC	a
predreadnought	predreadnought	k1gInSc1	predreadnought
SMS	SMS	kA	SMS
Radetzky	Radetzek	k1gInPc1	Radetzek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
a	a	k8xC	a
divadlo	divadlo	k1gNnSc1	divadlo
==	==	k?	==
</s>
</p>
<p>
<s>
Rakouský	rakouský	k2eAgMnSc1d1	rakouský
básník	básník	k1gMnSc1	básník
doby	doba	k1gFnSc2	doba
romantismu	romantismus	k1gInSc2	romantismus
Franz	Franz	k1gMnSc1	Franz
Grillparzer	Grillparzer	k1gMnSc1	Grillparzer
jej	on	k3xPp3gMnSc4	on
oslavil	oslavit	k5eAaPmAgMnS	oslavit
hned	hned	k6eAd1	hned
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
divadelní	divadelní	k2eAgFnSc7d1	divadelní
hrou	hra	k1gFnSc7	hra
Feldmarschall	Feldmarschall	k1gMnSc1	Feldmarschall
Radetzky	Radetzek	k1gInPc4	Radetzek
<g/>
,	,	kIx,	,
uvedenou	uvedený	k2eAgFnSc4d1	uvedená
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
hrdiny	hrdina	k1gMnSc2	hrdina
</s>
</p>
<p>
<s>
epickou	epický	k2eAgFnSc7d1	epická
básní	básnit	k5eAaImIp3nS	básnit
Óda	óda	k1gFnSc1	óda
na	na	k7c4	na
Radeckého	Radecký	k1gMnSc4	Radecký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Česko-rakouský	českoakouský	k2eAgMnSc1d1	česko-rakouský
básník	básník	k1gMnSc1	básník
Johann	Johann	k1gMnSc1	Johann
Carl	Carl	k1gMnSc1	Carl
Hickel	Hickel	k1gMnSc1	Hickel
napsal	napsat	k5eAaBmAgMnS	napsat
epickou	epický	k2eAgFnSc4d1	epická
báseň	báseň	k1gFnSc4	báseň
Radetzky-Feier	Radetzky-Feira	k1gFnPc2	Radetzky-Feira
<g/>
,	,	kIx,	,
vyšla	vyjít	k5eAaPmAgFnS	vyjít
u	u	k7c2	u
J.	J.	kA	J.
Kalveho	Kalve	k1gMnSc2	Kalve
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rakouský	rakouský	k2eAgMnSc1d1	rakouský
skladatel	skladatel	k1gMnSc1	skladatel
Johann	Johann	k1gMnSc1	Johann
Strauss	Straussa	k1gFnPc2	Straussa
starší	starší	k1gMnSc1	starší
složil	složit	k5eAaPmAgMnS	složit
Radeckého	Radeckého	k2eAgInSc4d1	Radeckého
pochod	pochod	k1gInSc4	pochod
(	(	kIx(	(
<g/>
Radetzky	Radetzek	k1gInPc4	Radetzek
Marsch	Marsch	k1gInSc1	Marsch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pražský	pražský	k2eAgMnSc1d1	pražský
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Josef	Josef	k1gMnSc1	Josef
Jiří	Jiří	k1gMnSc1	Jiří
Kolár	Kolár	k1gMnSc1	Kolár
napsal	napsat	k5eAaBmAgMnS	napsat
hru	hra	k1gFnSc4	hra
Vysloužilci	vysloužilec	k1gMnSc3	vysloužilec
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oslavující	oslavující	k2eAgInSc1d1	oslavující
vojáky	voják	k1gMnPc7	voják
Radeckého	Radeckého	k2eAgNnSc2d1	Radeckého
vojska	vojsko	k1gNnSc2	vojsko
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
postava	postava	k1gFnSc1	postava
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
divadelní	divadelní	k2eAgFnSc6d1	divadelní
hře	hra	k1gFnSc6	hra
České	český	k2eAgNnSc1d1	české
nebe	nebe	k1gNnSc1	nebe
uváděné	uváděný	k2eAgNnSc1d1	uváděné
Divadlem	divadlo	k1gNnSc7	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Epizodní	epizodní	k2eAgInSc4d1	epizodní
výstup	výstup	k1gInSc4	výstup
má	mít	k5eAaImIp3nS	mít
kníže	kníže	k1gMnSc1	kníže
maršál	maršál	k1gMnSc1	maršál
Radecký	Radecký	k1gMnSc1	Radecký
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
filmu	film	k1gInSc6	film
Sissi	Sisse	k1gFnSc4	Sisse
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
postava	postava	k1gFnSc1	postava
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
dílu	díl	k1gInSc6	díl
televizního	televizní	k2eAgInSc2d1	televizní
filmu	film	k1gInSc2	film
Císařovna	císařovna	k1gFnSc1	císařovna
Sissi	Sisse	k1gFnSc4	Sisse
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
==	==	k?	==
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
zlatého	zlatý	k2eAgNnSc2d1	Zlaté
rouna	rouno	k1gNnSc2	rouno
</s>
</p>
<p>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
řád	řád	k1gInSc1	řád
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
<g/>
,	,	kIx,	,
komturský	komturský	k2eAgInSc1d1	komturský
kříž	kříž	k1gInSc1	kříž
i	i	k9	i
rytířský	rytířský	k2eAgInSc4d1	rytířský
kříž	kříž	k1gInSc4	kříž
</s>
</p>
<p>
<s>
Královský	královský	k2eAgInSc1d1	královský
uherský	uherský	k2eAgInSc1d1	uherský
řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Štěpána	Štěpán	k1gMnSc2	Štěpán
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
a	a	k8xC	a
komturský	komturský	k2eAgInSc1d1	komturský
kříž	kříž	k1gInSc1	kříž
</s>
</p>
<p>
<s>
Císařský	císařský	k2eAgInSc1d1	císařský
rakouský	rakouský	k2eAgInSc1d1	rakouský
řád	řád	k1gInSc1	řád
Leopoldův	Leopoldův	k2eAgInSc1d1	Leopoldův
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
železné	železný	k2eAgFnSc2d1	železná
koruny	koruna	k1gFnSc2	koruna
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
ruský	ruský	k2eAgInSc1d1	ruský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Ondřeje	Ondřej	k1gMnSc2	Ondřej
–	–	k?	–
obdržel	obdržet	k5eAaPmAgMnS	obdržet
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
s	s	k7c7	s
brilianty	briliant	k1gInPc7	briliant
</s>
</p>
<p>
<s>
ruský	ruský	k2eAgInSc1d1	ruský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
</s>
</p>
<p>
<s>
ruský	ruský	k2eAgInSc1d1	ruský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Alexandra	Alexandr	k1gMnSc2	Alexandr
Něvského	něvský	k2eAgMnSc2d1	něvský
</s>
</p>
<p>
<s>
ruský	ruský	k2eAgInSc1d1	ruský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Vladimíra	Vladimíra	k1gFnSc1	Vladimíra
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
</s>
</p>
<p>
<s>
ruský	ruský	k2eAgInSc1d1	ruský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
</s>
</p>
<p>
<s>
ruský	ruský	k2eAgInSc1d1	ruský
Řád	řád	k1gInSc1	řád
bílé	bílý	k2eAgFnSc2d1	bílá
orlice	orlice	k1gFnSc2	orlice
</s>
</p>
<p>
<s>
bádenský	bádenský	k2eAgInSc1d1	bádenský
Řád	řád	k1gInSc1	řád
zähringenského	zähringenský	k2eAgMnSc2d1	zähringenský
lva	lev	k1gMnSc2	lev
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
bavorský	bavorský	k2eAgInSc1d1	bavorský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Huberta	Hubert	k1gMnSc2	Hubert
</s>
</p>
<p>
<s>
bavorský	bavorský	k2eAgInSc1d1	bavorský
Vojenský	vojenský	k2eAgInSc1d1	vojenský
řád	řád	k1gInSc1	řád
Maxe	Max	k1gMnSc2	Max
Josefa	Josef	k1gMnSc2	Josef
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
dánský	dánský	k2eAgInSc1d1	dánský
Řád	řád	k1gInSc1	řád
slona	slon	k1gMnSc2	slon
</s>
</p>
<p>
<s>
francouzský	francouzský	k2eAgInSc1d1	francouzský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Ludvíka	Ludvík	k1gMnSc2	Ludvík
</s>
</p>
<p>
<s>
hannoverský	hannoverský	k2eAgInSc1d1	hannoverský
Řád	řád	k1gInSc1	řád
Guelfů	guelf	k1gMnPc2	guelf
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
hannoverský	hannoverský	k2eAgInSc1d1	hannoverský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
</s>
</p>
<p>
<s>
hesenský	hesenský	k2eAgInSc1d1	hesenský
Řád	řád	k1gInSc1	řád
zlatého	zlatý	k2eAgInSc2d1	zlatý
lva	lev	k1gInSc2	lev
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
hesenský	hesenský	k2eAgInSc1d1	hesenský
Řád	řád	k1gInSc1	řád
Ludvíkův	Ludvíkův	k2eAgInSc1d1	Ludvíkův
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
lukský	lukský	k2eAgInSc1d1	lukský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
</s>
</p>
<p>
<s>
modenský	modenský	k2eAgInSc1d1	modenský
Řád	řád	k1gInSc1	řád
Estenské	Estenský	k2eAgFnSc2d1	Estenský
orlice	orlice	k1gFnSc2	orlice
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
papežský	papežský	k2eAgInSc1d1	papežský
Řád	řád	k1gInSc1	řád
Pia	Pius	k1gMnSc2	Pius
IX	IX	kA	IX
<g/>
.	.	kIx.	.
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
papežský	papežský	k2eAgInSc1d1	papežský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Řehoře	Řehoř	k1gMnSc2	Řehoř
Velkého	velký	k2eAgNnSc2d1	velké
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
parmský	parmský	k2eAgInSc1d1	parmský
Řád	řád	k1gInSc1	řád
konstantiniánských	konstantiniánský	k2eAgMnPc2d1	konstantiniánský
rytířů	rytíř	k1gMnPc2	rytíř
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
parmský	parmský	k2eAgInSc1d1	parmský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
pruský	pruský	k2eAgInSc1d1	pruský
Řád	řád	k1gInSc1	řád
černé	černý	k2eAgFnSc2d1	černá
orlice	orlice	k1gFnSc2	orlice
</s>
</p>
<p>
<s>
pruský	pruský	k2eAgInSc1d1	pruský
Řád	řád	k1gInSc1	řád
červené	červený	k2eAgFnSc2d1	červená
orlice	orlice	k1gFnSc2	orlice
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
</s>
</p>
<p>
<s>
řecký	řecký	k2eAgInSc1d1	řecký
Řád	řád	k1gInSc1	řád
Spasitele	spasitel	k1gMnSc2	spasitel
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
sardinský	sardinský	k2eAgInSc1d1	sardinský
Řád	řád	k1gInSc1	řád
zvěstování	zvěstování	k1gNnSc2	zvěstování
</s>
</p>
<p>
<s>
saský	saský	k2eAgInSc1d1	saský
Řád	řád	k1gInSc1	řád
routové	routový	k2eAgFnSc2d1	Routová
koruny	koruna	k1gFnSc2	koruna
</s>
</p>
<p>
<s>
sicilský	sicilský	k2eAgInSc1d1	sicilský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
a	a	k8xC	a
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
toskánský	toskánský	k2eAgInSc1d1	toskánský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Štěpána	Štěpán	k1gMnSc2	Štěpán
<g/>
,	,	kIx,	,
papeže	papež	k1gMnSc2	papež
a	a	k8xC	a
mučedníka	mučedník	k1gMnSc2	mučedník
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
toskánský	toskánský	k2eAgInSc1d1	toskánský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc2	Josef
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
toskánský	toskánský	k2eAgInSc1d1	toskánský
Vojenský	vojenský	k2eAgInSc1d1	vojenský
záslužný	záslužný	k2eAgInSc1d1	záslužný
řád	řád	k1gInSc1	řád
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
</s>
</p>
<p>
<s>
württemberský	württemberský	k2eAgInSc1d1	württemberský
Řád	řád	k1gInSc1	řád
württemberské	württemberský	k2eAgFnSc2d1	württemberská
koruny	koruna	k1gFnSc2	koruna
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
württemberský	württemberský	k2eAgInSc1d1	württemberský
Vojenský	vojenský	k2eAgInSc1d1	vojenský
záslužný	záslužný	k2eAgInSc1d1	záslužný
řád	řád	k1gInSc1	řád
–	–	k?	–
velkokříž	velkokříž	k1gInSc1	velkokříž
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Radecký	Radecký	k1gMnSc1	Radecký
mrtev	mrtev	k2eAgMnSc1d1	mrtev
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
nekrolog	nekrolog	k1gInSc4	nekrolog
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Světozor	světozor	k1gInSc1	světozor
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1858	[number]	k4	1858
<g/>
,	,	kIx,	,
strana	strana	k1gFnSc1	strana
1	[number]	k4	1
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HERRE	HERRE	kA	HERRE
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
<g/>
.	.	kIx.	.
</s>
<s>
Radecký	Radecký	k1gMnSc1	Radecký
<g/>
:	:	kIx,	:
nejoblíbenější	oblíbený	k2eAgMnSc1d3	nejoblíbenější
polní	polní	k2eAgMnSc1d1	polní
maršálek	maršálek	k1gMnSc1	maršálek
rakouské	rakouský	k2eAgFnSc2d1	rakouská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
pohnutá	pohnutý	k2eAgFnSc1d1	pohnutá
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Bonus	bonus	k1gInSc1	bonus
A	A	kA	A
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
239	[number]	k4	239
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85914	[number]	k4	85914
<g/>
-	-	kIx~	-
<g/>
78	[number]	k4	78
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HAVEL	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
ROMAŇÁK	ROMAŇÁK	kA	ROMAŇÁK
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
<g/>
.	.	kIx.	.
</s>
<s>
Maršál	maršál	k1gMnSc1	maršál
Radecký	Radecký	k1gMnSc1	Radecký
<g/>
.	.	kIx.	.
</s>
<s>
Vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
pěti	pět	k4xCc2	pět
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
368	[number]	k4	368
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
297	[number]	k4	297
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
VÁLKA	Válka	k1gMnSc1	Válka
<g/>
,	,	kIx,	,
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
<g/>
.	.	kIx.	.
</s>
<s>
Maršál	maršál	k1gMnSc1	maršál
Radecký	Radecký	k1gMnSc1	Radecký
a	a	k8xC	a
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Agentura	agentura	k1gFnSc1	agentura
Galia	galium	k1gNnSc2	galium
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
157	[number]	k4	157
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
2102	[number]	k4	2102
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PERNES	PERNES	kA	PERNES
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
FUČÍK	Fučík	k1gMnSc1	Fučík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
;	;	kIx,	;
HAVEL	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
císařským	císařský	k2eAgInSc7d1	císařský
praporem	prapor	k1gInSc7	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
habsburské	habsburský	k2eAgFnSc2d1	habsburská
armády	armáda	k1gFnSc2	armáda
1526	[number]	k4	1526
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Elka	Elka	k1gFnSc1	Elka
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
555	[number]	k4	555
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902745	[number]	k4	902745
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FIDLER	FIDLER	kA	FIDLER
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnSc4	vládce
a	a	k8xC	a
vlast	vlast	k1gFnSc4	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Ruští	ruský	k2eAgMnPc1d1	ruský
a	a	k8xC	a
sovětští	sovětský	k2eAgMnPc1d1	sovětský
maršálové	maršál	k1gMnPc1	maršál
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
JOTA	jota	k1gNnSc1	jota
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
290	[number]	k4	290
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7217	[number]	k4	7217
<g/>
-	-	kIx~	-
<g/>
354	[number]	k4	354
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
–	–	k?	–
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
579	[number]	k4	579
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maršál	maršál	k1gMnSc1	maršál
Radecký	Radecký	k1gMnSc1	Radecký
<g/>
,	,	kIx,	,
od	od	k7c2	od
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
k	k	k7c3	k
pomníku	pomník	k1gInSc3	pomník
<g/>
,	,	kIx,	,
jubilejní	jubilejní	k2eAgInSc1d1	jubilejní
sborník	sborník	k1gInSc1	sborník
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
250	[number]	k4	250
let	léto	k1gNnPc2	léto
narození	narození	k1gNnSc2	narození
Jana	Jan	k1gMnSc2	Jan
Josefa	Josef	k1gMnSc2	Josef
Václava	Václav	k1gMnSc2	Václav
Radeckého	Radecký	k1gMnSc2	Radecký
z	z	k7c2	z
Radče	Radč	k1gFnSc2	Radč
(	(	kIx(	(
<g/>
1766	[number]	k4	1766
<g/>
-	-	kIx~	-
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
editoři	editor	k1gMnPc1	editor
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hojda	Hojda	k1gMnSc1	Hojda
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
Štogrová	Štogrová	k1gFnSc1	Štogrová
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
;	;	kIx,	;
autoři	autor	k1gMnPc1	autor
Emanuel	Emanuel	k1gMnSc1	Emanuel
Bechinie-Lažan	Bechinie-Lažan	k1gMnSc1	Bechinie-Lažan
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Rak	rak	k1gMnSc1	rak
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Koťátko	koťátko	k1gNnSc1	koťátko
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Buben	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
Giorgio	Giorgio	k1gMnSc1	Giorgio
Ferrari	Ferrari	k1gMnSc1	Ferrari
<g/>
,	,	kIx,	,
Božidar	Božidar	k1gMnSc1	Božidar
Jezernik	Jezernik	k1gMnSc1	Jezernik
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hojda	Hojda	k1gMnSc1	Hojda
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
Hnojil	hnojit	k5eAaImAgMnS	hnojit
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
Stehlíková	Stehlíková	k1gFnSc1	Stehlíková
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
Bečková	Bečková	k1gFnSc1	Bečková
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Pavelka	Pavelka	k1gMnSc1	Pavelka
<g/>
.	.	kIx.	.
</s>
<s>
Bondy	bond	k1gInPc1	bond
Praha	Praha	k1gFnSc1	Praha
2016	[number]	k4	2016
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Radecký	Radecký	k1gMnSc1	Radecký
z	z	k7c2	z
Radče	Radč	k1gInSc2	Radč
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Radecký	Radecký	k1gMnSc1	Radecký
z	z	k7c2	z
Radče	Radč	k1gFnSc2	Radč
</s>
</p>
<p>
<s>
Záznam	záznam	k1gInSc1	záznam
o	o	k7c6	o
křtu	křest	k1gInSc6	křest
v	v	k7c6	v
matrice	matrika	k1gFnSc6	matrika
</s>
</p>
<p>
<s>
Občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
Radecký	Radecký	k1gMnSc1	Radecký
1766-2016	[number]	k4	1766-2016
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
o	o	k7c6	o
Radeckém	Radecký	k1gMnSc6	Radecký
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
k	k	k7c3	k
zamyšlení	zamyšlení	k1gNnSc3	zamyšlení
o	o	k7c6	o
současném	současný	k2eAgNnSc6d1	současné
vnímání	vnímání	k1gNnSc6	vnímání
maršála	maršál	k1gMnSc2	maršál
Radeckého	Radecký	k1gMnSc2	Radecký
</s>
</p>
<p>
<s>
Maršál	maršál	k1gMnSc1	maršál
Radecký	Radecký	k1gMnSc1	Radecký
a	a	k8xC	a
spor	spor	k1gInSc1	spor
o	o	k7c4	o
jeho	on	k3xPp3gInSc4	on
pomník	pomník	k1gInSc4	pomník
–	–	k?	–
video	video	k1gNnSc4	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Radecký	Radecký	k1gMnSc1	Radecký
z	z	k7c2	z
Radče	Radč	k1gInSc2	Radč
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
voják	voják	k1gMnSc1	voják
brání	bránit	k5eAaImIp3nS	bránit
trůn	trůn	k1gInSc4	trůn
-	-	kIx~	-
monografie	monografie	k1gFnPc1	monografie
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
-	-	kIx~	-
rodokmen	rodokmen	k1gInSc1	rodokmen
maršála	maršál	k1gMnSc2	maršál
Radeckého	Radecký	k1gMnSc2	Radecký
</s>
</p>
