<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
výše	vysoce	k6eAd2	vysoce
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
NiMH	NiMH	k1gMnSc1	NiMH
akumulátor	akumulátor	k1gInSc1	akumulátor
oproti	oproti	k7c3	oproti
starším	starý	k2eAgFnPc3d2	starší
NiCd	NiCd	k1gInSc1	NiCd
akumulátorům	akumulátor	k1gInPc3	akumulátor
až	až	k6eAd1	až
2,5	[number]	k4	2,5
<g/>
×	×	k?	×
větší	veliký	k2eAgFnSc4d2	veliký
kapacitu	kapacita	k1gFnSc4	kapacita
<g/>
.	.	kIx.	.
</s>
