<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
kynurenová	kynurenová	k1gFnSc1	kynurenová
(	(	kIx(	(
<g/>
KYNA	kynout	k5eAaImSgInS	kynout
nebo	nebo	k8xC	nebo
KYN	kyn	k1gInSc1wB	kyn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
produktem	produkt	k1gInSc7	produkt
běžného	běžný	k2eAgInSc2d1	běžný
metabolismu	metabolismus	k1gInSc2	metabolismus
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
L-tryptofan	Lryptofana	k1gFnPc2	L-tryptofana
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
neuroaktivní	uroaktivní	k2eNgInSc4d1	uroaktivní
účinek	účinek	k1gInSc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
antiexcitotoxicky	antiexcitotoxicky	k6eAd1	antiexcitotoxicky
a	a	k8xC	a
jako	jako	k9	jako
antikonvulzivum	antikonvulzivum	k1gInSc1	antikonvulzivum
<g/>
;	;	kIx,	;
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
antagonista	antagonista	k1gMnSc1	antagonista
na	na	k7c6	na
excitačních	excitační	k2eAgInPc6d1	excitační
receptorech	receptor	k1gInPc6	receptor
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
především	především	k9	především
jako	jako	k9	jako
endogenní	endogenní	k2eAgMnSc1d1	endogenní
antagonista	antagonista	k1gMnSc1	antagonista
všech	všecek	k3xTgInPc2	všecek
ionotropních	ionotropní	k2eAgInPc2d1	ionotropní
glutamátových	glutamátův	k2eAgInPc2d1	glutamátův
receptorů	receptor	k1gInPc2	receptor
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
aktivitou	aktivita	k1gFnSc7	aktivita
může	moct	k5eAaImIp3nS	moct
kyselina	kyselina	k1gFnSc1	kyselina
kynurenová	kynurenový	k2eAgFnSc1d1	kynurenový
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
důležité	důležitý	k2eAgInPc4d1	důležitý
neurofyziologické	neurofyziologický	k2eAgInPc4d1	neurofyziologický
a	a	k8xC	a
neuropatologické	neuropatologický	k2eAgInPc4d1	neuropatologický
procesy	proces	k1gInPc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zvažováno	zvažován	k2eAgNnSc1d1	zvažováno
její	její	k3xOp3gNnSc4	její
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
terapii	terapie	k1gFnSc6	terapie
některých	některý	k3yIgFnPc2	některý
neurobiologických	urobiologický	k2eNgFnPc2d1	neurobiologická
poruch	porucha	k1gFnPc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
zvýšené	zvýšený	k2eAgFnPc1d1	zvýšená
hladiny	hladina	k1gFnPc1	hladina
kyseliny	kyselina	k1gFnSc2	kyselina
kynurenové	kynurenová	k1gFnSc2	kynurenová
jsou	být	k5eAaImIp3nP	být
spojovány	spojovat	k5eAaImNgInP	spojovat
s	s	k7c7	s
určitými	určitý	k2eAgInPc7d1	určitý
patologickými	patologický	k2eAgInPc7d1	patologický
stavy	stav	k1gInPc7	stav
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
klíšťovou	klíšťový	k2eAgFnSc7d1	klíšťová
encefalitidou	encefalitida	k1gFnSc7	encefalitida
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
zvýšené	zvýšený	k2eAgNnSc1d1	zvýšené
množství	množství	k1gNnSc1	množství
kyseliny	kyselina	k1gFnSc2	kyselina
kynurenové	kynurenový	k2eAgFnSc2d1	kynurenový
v	v	k7c6	v
mozkomíšním	mozkomíšní	k2eAgInSc6d1	mozkomíšní
moku	mok	k1gInSc6	mok
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
možná	možný	k2eAgFnSc1d1	možná
role	role	k1gFnSc1	role
v	v	k7c6	v
patogenezi	patogeneze	k1gFnSc6	patogeneze
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyselinu	kyselina	k1gFnSc4	kyselina
kynurenovou	kynurenová	k1gFnSc4	kynurenová
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
německý	německý	k2eAgMnSc1d1	německý
chemik	chemik	k1gMnSc1	chemik
Justus	Justus	k1gMnSc1	Justus
von	von	k1gInSc4	von
Liebig	Liebiga	k1gFnPc2	Liebiga
v	v	k7c6	v
psí	psí	k2eAgFnSc6d1	psí
moči	moč	k1gFnSc6	moč
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
zřejmě	zřejmě	k6eAd1	zřejmě
pochází	pocházet	k5eAaImIp3nS	pocházet
její	její	k3xOp3gInSc1	její
název	název	k1gInSc1	název
<g/>
.	.	kIx.	.
<g/>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
L-kynureninu	Lynurenina	k1gFnSc4	L-kynurenina
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
katalyzované	katalyzovaný	k2eAgFnSc6d1	katalyzovaná
enzymem	enzym	k1gInSc7	enzym
kynurenin-oxoglutarát	kynureninxoglutarát	k1gInSc1	kynurenin-oxoglutarát
transaminázou	transamináza	k1gFnSc7	transamináza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kynurenic	Kynurenice	k1gFnPc2	Kynurenice
acid	acido	k1gNnPc2	acido
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
