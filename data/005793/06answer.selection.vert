<s>
Ze	z	k7c2	z
středního	střední	k2eAgNnSc2d1	střední
ucha	ucho	k1gNnSc2	ucho
do	do	k7c2	do
nosohltanu	nosohltan	k1gInSc2	nosohltan
vyúsťuje	vyúsťovat	k5eAaImIp3nS	vyúsťovat
Eustachova	Eustachův	k2eAgFnSc1d1	Eustachova
trubice	trubice	k1gFnSc1	trubice
(	(	kIx(	(
<g/>
tuba	tuba	k1gFnSc1	tuba
Eustachi	Eustachi	k1gMnSc1	Eustachi
<g/>
,	,	kIx,	,
tuba	tuba	k1gFnSc1	tuba
auditiva	auditiva	k1gFnSc1	auditiva
<g/>
,	,	kIx,	,
sluchová	sluchový	k2eAgFnSc1d1	sluchová
středoušní	středoušní	k2eAgFnSc1d1	středoušní
trubice	trubice	k1gFnSc1	trubice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
tlak	tlak	k1gInSc4	tlak
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
uchu	ucho	k1gNnSc6	ucho
s	s	k7c7	s
tlakem	tlak	k1gInSc7	tlak
v	v	k7c6	v
okolním	okolní	k2eAgNnSc6d1	okolní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
