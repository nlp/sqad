<s>
Grisaille	grisaille	k1gFnSc1	grisaille
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
"	"	kIx"	"
<g/>
en	en	k?	en
grisaille	grisaille	k1gFnSc1	grisaille
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
též	též	k9	též
"	"	kIx"	"
<g/>
monochromní	monochromní	k2eAgFnSc1d1	monochromní
malba	malba	k1gFnSc1	malba
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
z	z	k7c2	z
francouzského	francouzský	k2eAgNnSc2d1	francouzské
gris	gris	k6eAd1	gris
<g/>
,	,	kIx,	,
šedý	šedý	k2eAgInSc1d1	šedý
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
malbu	malba	k1gFnSc4	malba
vytvořenou	vytvořený	k2eAgFnSc4d1	vytvořená
převážně	převážně	k6eAd1	převážně
jednobarevně	jednobarevně	k6eAd1	jednobarevně
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
odstínech	odstín	k1gInPc6	odstín
šedi	šeď	k1gFnSc2	šeď
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
však	však	k9	však
také	také	k9	také
v	v	k7c6	v
tónech	tón	k1gInPc6	tón
hnědi	hněď	k1gFnSc2	hněď
<g/>
.	.	kIx.	.
</s>
<s>
Malby	malba	k1gFnPc1	malba
en	en	k?	en
grisaille	grisaille	k1gFnPc1	grisaille
byly	být	k5eAaImAgFnP	být
oblíbeny	oblíbit	k5eAaPmNgFnP	oblíbit
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
renesanci	renesance	k1gFnSc6	renesance
a	a	k8xC	a
v	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
gotice	gotika	k1gFnSc6	gotika
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgNnSc1d1	časté
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
užití	užití	k1gNnSc1	užití
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
oltářních	oltářní	k2eAgNnPc2d1	oltářní
křídel	křídlo	k1gNnPc2	křídlo
nebo	nebo	k8xC	nebo
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
oltářní	oltářní	k2eAgFnSc2d1	oltářní
skříně	skříň	k1gFnSc2	skříň
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
používá	používat	k5eAaImIp3nS	používat
malbu	malba	k1gFnSc4	malba
en	en	k?	en
grisaille	grisaille	k1gFnSc1	grisaille
i	i	k8xC	i
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
Bosch	Bosch	kA	Bosch
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
křídel	křídlo	k1gNnPc2	křídlo
svých	svůj	k3xOyFgMnPc2	svůj
triptychů	triptych	k1gInPc2	triptych
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgNnSc1d1	časté
je	být	k5eAaImIp3nS	být
též	též	k9	též
užití	užití	k1gNnSc1	užití
maleb	malba	k1gFnPc2	malba
en	en	k?	en
grisaille	grisaille	k1gFnSc2	grisaille
na	na	k7c6	na
malovaných	malovaný	k2eAgFnPc6d1	malovaná
fasádách	fasáda	k1gFnPc6	fasáda
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takového	takový	k3xDgNnSc2	takový
užití	užití	k1gNnSc2	užití
je	být	k5eAaImIp3nS	být
tzv	tzv	kA	tzv
buchalterie	buchalterie	k1gFnSc1	buchalterie
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
nádvoří	nádvoří	k1gNnSc6	nádvoří
zámku	zámek	k1gInSc2	zámek
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
užil	užít	k5eAaPmAgMnS	užít
techniku	technika	k1gFnSc4	technika
monochromní	monochromní	k2eAgFnSc2d1	monochromní
malby	malba	k1gFnSc2	malba
i	i	k8xC	i
Andrea	Andrea	k1gFnSc1	Andrea
del	del	k?	del
Sarto	Sarto	k1gNnSc1	Sarto
v	v	k7c4	v
Chiostro	Chiostro	k1gNnSc4	Chiostro
dello	delnout	k5eAaPmAgNnS	delnout
Scalzo	Scalza	k1gFnSc5	Scalza
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgNnSc1d1	časté
je	být	k5eAaImIp3nS	být
také	také	k9	také
použití	použití	k1gNnSc1	použití
maleb	malba	k1gFnPc2	malba
en	en	k?	en
grisaille	grisaille	k1gFnSc2	grisaille
v	v	k7c6	v
celku	celek	k1gInSc6	celek
barevně	barevně	k6eAd1	barevně
malované	malovaný	k2eAgFnPc1d1	malovaná
renesanční	renesanční	k2eAgFnPc1d1	renesanční
fasády	fasáda	k1gFnPc1	fasáda
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
použití	použití	k1gNnSc2	použití
této	tento	k3xDgFnSc2	tento
techniky	technika	k1gFnSc2	technika
je	být	k5eAaImIp3nS	být
především	především	k9	především
snaha	snaha	k1gFnSc1	snaha
napodobit	napodobit	k5eAaPmF	napodobit
nízký	nízký	k2eAgInSc4d1	nízký
reliéf	reliéf	k1gInSc4	reliéf
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
štuku	štuk	k1gInSc2	štuk
<g/>
,	,	kIx,	,
dřeva	dřevo	k1gNnSc2	dřevo
atp.	atp.	kA	atp.
Druhým	druhý	k4xOgInSc7	druhý
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
výrazové	výrazový	k2eAgNnSc4d1	výrazové
ztišení	ztišení	k1gNnSc4	ztišení
<g/>
,	,	kIx,	,
ztlumení	ztlumení	k1gNnSc4	ztlumení
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
většího	veliký	k2eAgInSc2d2	veliký
celku	celek	k1gInSc2	celek
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
oltářní	oltářní	k2eAgFnPc4d1	oltářní
skříně	skříň	k1gFnPc4	skříň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Grisaille	grisaille	k1gFnSc1	grisaille
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
určena	určen	k2eAgFnSc1d1	určena
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
výzdobě	výzdoba	k1gFnSc3	výzdoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c6	o
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
olejomalby	olejomalba	k1gFnSc2	olejomalba
nebo	nebo	k8xC	nebo
o	o	k7c4	o
ryteckou	rytecký	k2eAgFnSc4d1	rytecká
předlohu	předloha	k1gFnSc4	předloha
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
též	též	k9	též
pro	pro	k7c4	pro
jednobarevnou	jednobarevný	k2eAgFnSc4d1	jednobarevná
malbu	malba	k1gFnSc4	malba
na	na	k7c6	na
emailu	email	k1gInSc6	email
nebo	nebo	k8xC	nebo
na	na	k7c6	na
skle	sklo	k1gNnSc6	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
okno	okno	k1gNnSc1	okno
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k9	jako
Pět	pět	k4xCc1	pět
sester	sestra	k1gFnPc2	sestra
v	v	k7c6	v
koncové	koncový	k2eAgFnSc6d1	koncová
části	část	k1gFnSc6	část
severní	severní	k2eAgFnSc2d1	severní
chrámové	chrámový	k2eAgFnSc2d1	chrámová
lodi	loď	k1gFnSc2	loď
katedrály	katedrála	k1gFnSc2	katedrála
v	v	k7c6	v
Yorku	York	k1gInSc6	York
<g/>
.	.	kIx.	.
</s>
