<s>
Grisaille	grisaille	k1gFnSc1	grisaille
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
"	"	kIx"	"
<g/>
en	en	k?	en
grisaille	grisaille	k1gFnSc1	grisaille
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
též	též	k9	též
"	"	kIx"	"
<g/>
monochromní	monochromní	k2eAgFnSc1d1	monochromní
malba	malba	k1gFnSc1	malba
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
z	z	k7c2	z
francouzského	francouzský	k2eAgNnSc2d1	francouzské
gris	gris	k6eAd1	gris
<g/>
,	,	kIx,	,
šedý	šedý	k2eAgInSc1d1	šedý
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
malbu	malba	k1gFnSc4	malba
vytvořenou	vytvořený	k2eAgFnSc4d1	vytvořená
převážně	převážně	k6eAd1	převážně
jednobarevně	jednobarevně	k6eAd1	jednobarevně
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
odstínech	odstín	k1gInPc6	odstín
šedi	šeď	k1gFnSc2	šeď
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
však	však	k9	však
také	také	k9	také
v	v	k7c6	v
tónech	tón	k1gInPc6	tón
hnědi	hněď	k1gFnSc2	hněď
<g/>
.	.	kIx.	.
</s>
