<s>
Jaký	jaký	k3yIgInSc1	jaký
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
malbu	malba	k1gFnSc4	malba
vytvořenou	vytvořený	k2eAgFnSc4d1	vytvořená
převážně	převážně	k6eAd1	převážně
jednobarevně	jednobarevně	k6eAd1	jednobarevně
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
odstínech	odstín	k1gInPc6	odstín
šedi	šeď	k1gFnSc2	šeď
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
však	však	k9	však
také	také	k9	také
v	v	k7c6	v
tónech	tón	k1gInPc6	tón
hnědi	hněď	k1gFnSc2	hněď
<g/>
?	?	kIx.	?
</s>
