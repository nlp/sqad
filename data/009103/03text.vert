<p>
<s>
Sinhálština	sinhálština	k1gFnSc1	sinhálština
(	(	kIx(	(
<g/>
sinhálsky	sinhálsky	k6eAd1	sinhálsky
"	"	kIx"	"
<g/>
sinhala	sinhat	k5eAaBmAgFnS	sinhat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jazykem	jazyk	k1gInSc7	jazyk
Sinhálců	Sinhálec	k1gMnPc2	Sinhálec
<g/>
,	,	kIx,	,
největšího	veliký	k2eAgNnSc2d3	veliký
etnika	etnikum	k1gNnSc2	etnikum
na	na	k7c6	na
Srí	Srí	k1gFnSc6	Srí
Lance	lance	k1gNnSc2	lance
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
do	do	k7c2	do
indoevropské	indoevropský	k2eAgFnSc2d1	indoevropská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
do	do	k7c2	do
jižních	jižní	k2eAgInPc2d1	jižní
indoárijských	indoárijský	k2eAgInPc2d1	indoárijský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
19	[number]	k4	19
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
16	[number]	k4	16
milionů	milion	k4xCgInPc2	milion
tvoří	tvořit	k5eAaImIp3nS	tvořit
mluvčí	mluvčí	k1gMnPc1	mluvčí
rodilí	rodilý	k2eAgMnPc1d1	rodilý
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
tamilštinou	tamilština	k1gFnSc7	tamilština
je	být	k5eAaImIp3nS	být
uznávána	uznáván	k2eAgFnSc1d1	uznávána
jako	jako	k9	jako
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
Srí	Srí	k1gFnSc2	Srí
Lanky	lanko	k1gNnPc7	lanko
<g/>
.	.	kIx.	.
</s>
<s>
Sinhálština	sinhálština	k1gFnSc1	sinhálština
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
používá	používat	k5eAaImIp3nS	používat
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
z	z	k7c2	z
písma	písmo	k1gNnSc2	písmo
bráhmí	bráhmit	k5eAaPmIp3nS	bráhmit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
sedmém	sedmý	k4xOgNnSc6	sedmý
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
nápisy	nápis	k1gInPc1	nápis
v	v	k7c6	v
sinhálštině	sinhálština	k1gFnSc6	sinhálština
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnPc1d3	nejstarší
literární	literární	k2eAgFnPc1d1	literární
památky	památka	k1gFnPc1	památka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
datují	datovat	k5eAaImIp3nP	datovat
do	do	k7c2	do
devátého	devátý	k4xOgNnSc2	devátý
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližším	blízký	k2eAgMnPc3d3	nejbližší
příbuzným	příbuzný	k1gMnPc3	příbuzný
sinhálštiny	sinhálština	k1gFnSc2	sinhálština
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc1	jazyk
divehi	divehi	k6eAd1	divehi
používaný	používaný	k2eAgInSc1d1	používaný
na	na	k7c6	na
Maledivách	Maledivy	k1gFnPc6	Maledivy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
názvu	název	k1gInSc2	název
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
sinhala	sinhal	k1gMnSc2	sinhal
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
sanskrtu	sanskrt	k1gInSc2	sanskrt
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
částí	část	k1gFnSc7	část
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
siṃ	siṃ	k?	siṃ
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
lev	lev	k1gInSc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
Sinhabahu	Sinhabaha	k1gFnSc4	Sinhabaha
(	(	kIx(	(
<g/>
lev	lev	k1gMnSc1	lev
<g/>
–	–	k?	–
<g/>
paže	paže	k1gFnSc1	paže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
lva	lev	k1gInSc2	lev
a	a	k8xC	a
princezny	princezna	k1gFnSc2	princezna
z	z	k7c2	z
Vangy	Vanga	k1gFnSc2	Vanga
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
zabil	zabít	k5eAaPmAgMnS	zabít
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
králem	král	k1gMnSc7	král
Vangy	Vanga	k1gFnSc2	Vanga
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Vijaya	Vijaya	k1gMnSc1	Vijaya
pak	pak	k6eAd1	pak
odešel	odejít	k5eAaPmAgMnS	odejít
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
usadil	usadit	k5eAaPmAgMnS	usadit
se	se	k3xPyFc4	se
na	na	k7c4	na
Srí	Srí	k1gFnSc4	Srí
Lance	lance	k1gNnSc2	lance
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
praotcem	praotec	k1gMnSc7	praotec
všech	všecek	k3xTgMnPc2	všecek
Sinhálců	Sinhálec	k1gMnPc2	Sinhálec
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
la	la	k1gNnSc2	la
je	být	k5eAaImIp3nS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
buď	buď	k8xC	buď
se	s	k7c7	s
sanskrtským	sanskrtský	k2eAgInSc7d1	sanskrtský
kořenem	kořen	k1gInSc7	kořen
lā	lā	k?	lā
s	s	k7c7	s
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
uchvátit	uchvátit	k5eAaPmF	uchvátit
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
zabít	zabít	k5eAaPmF	zabít
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
slovem	slovo	k1gNnSc7	slovo
loha	loh	k1gInSc2	loh
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
krev	krev	k1gFnSc1	krev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
lingvistického	lingvistický	k2eAgNnSc2d1	lingvistické
hlediska	hledisko	k1gNnSc2	hledisko
není	být	k5eNaImIp3nS	být
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
interpretací	interpretace	k1gFnPc2	interpretace
dosti	dosti	k6eAd1	dosti
přesvědčující	přesvědčující	k2eAgInSc1d1	přesvědčující
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
slovo	slovo	k1gNnSc1	slovo
sinhala	sinhal	k1gMnSc2	sinhal
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
"	"	kIx"	"
<g/>
lev	lev	k1gMnSc1	lev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dorazili	dorazit	k5eAaPmAgMnP	dorazit
na	na	k7c4	na
Srí	Srí	k1gFnSc4	Srí
Lanku	lanko	k1gNnSc3	lanko
osadníci	osadník	k1gMnPc1	osadník
ze	z	k7c2	z
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
přinesli	přinést	k5eAaPmAgMnP	přinést
s	s	k7c7	s
sebou	se	k3xPyFc7	se
indoárijské	indoárijský	k2eAgInPc1d1	indoárijský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
stoletích	století	k1gNnPc6	století
přicházeli	přicházet	k5eAaImAgMnP	přicházet
další	další	k2eAgMnPc1d1	další
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
ze	z	k7c2	z
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
Indie	Indie	k1gFnSc2	Indie
(	(	kIx(	(
<g/>
Kalinga	Kalinga	k1gFnSc1	Kalinga
<g/>
,	,	kIx,	,
Magadha	Magadha	k1gFnSc1	Magadha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
mísení	mísení	k1gNnSc3	mísení
s	s	k7c7	s
východními	východní	k2eAgInPc7d1	východní
prákrty	prákrt	k1gInPc7	prákrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Etapy	etapa	k1gFnSc2	etapa
vývoje	vývoj	k1gInSc2	vývoj
sinhálštiny	sinhálština	k1gFnSc2	sinhálština
===	===	k?	===
</s>
</p>
<p>
<s>
sinhálský	sinhálský	k2eAgInSc1d1	sinhálský
prákrt	prákrt	k1gInSc1	prákrt
(	(	kIx(	(
<g/>
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
protosinhálština	protosinhálština	k1gFnSc1	protosinhálština
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
středověká	středověký	k2eAgFnSc1d1	středověká
sinhálština	sinhálština	k1gFnSc1	sinhálština
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
moderní	moderní	k2eAgFnSc1d1	moderní
sinhálština	sinhálština	k1gFnSc1	sinhálština
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
současnost	současnost	k1gFnSc1	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
výslovnosti	výslovnost	k1gFnSc2	výslovnost
===	===	k?	===
</s>
</p>
<p>
<s>
splynutí	splynutí	k1gNnSc1	splynutí
ploziv	plozit	k5eAaPmDgInS	plozit
s	s	k7c7	s
přídechem	přídech	k1gInSc7	přídech
s	s	k7c7	s
plozivami	ploziva	k1gFnPc7	ploziva
bez	bez	k7c2	bez
přídechu	přídech	k1gInSc2	přídech
</s>
</p>
<p>
<s>
zkracování	zkracování	k1gNnSc1	zkracování
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
samohlásek	samohláska	k1gFnPc2	samohláska
</s>
</p>
<p>
<s>
zjednodušování	zjednodušování	k1gNnSc1	zjednodušování
skupin	skupina	k1gFnPc2	skupina
různých	různý	k2eAgFnPc2d1	různá
souhlásek	souhláska	k1gFnPc2	souhláska
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
stejných	stejný	k2eAgFnPc2d1	stejná
souhlásek	souhláska	k1gFnPc2	souhláska
nebo	nebo	k8xC	nebo
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
souhlásky	souhláska	k1gFnSc2	souhláska
</s>
</p>
<p>
<s>
přeměna	přeměna	k1gFnSc1	přeměna
/	/	kIx~	/
<g/>
j	j	k?	j
<g/>
/	/	kIx~	/
v	v	k7c6	v
/	/	kIx~	/
<g/>
d	d	k?	d
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
==	==	k?	==
Vliv	vliv	k1gInSc1	vliv
okolních	okolní	k2eAgInPc2d1	okolní
jazyků	jazyk	k1gInPc2	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Drávidské	drávidský	k2eAgInPc1d1	drávidský
jazyky	jazyk	k1gInPc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
přejatých	přejatý	k2eAgNnPc2d1	přejaté
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
tamilštiny	tamilština	k1gFnSc2	tamilština
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
společné	společný	k2eAgNnSc4d1	společné
soužití	soužití	k1gNnSc4	soužití
sinhálštiny	sinhálština	k1gFnSc2	sinhálština
a	a	k8xC	a
drávidských	drávidský	k2eAgInPc2d1	drávidský
jazyků	jazyk	k1gInPc2	jazyk
i	i	k8xC	i
některé	některý	k3yIgInPc4	některý
prvky	prvek	k1gInPc4	prvek
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
a	a	k8xC	a
gramatice	gramatika	k1gFnSc6	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
rozlišování	rozlišování	k1gNnSc1	rozlišování
mezi	mezi	k7c7	mezi
krátkým	krátký	k2eAgInSc7d1	krátký
e	e	k0	e
<g/>
,	,	kIx,	,
o	o	k0	o
a	a	k8xC	a
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
ē	ē	k?	ē
<g/>
,	,	kIx,	,
ō	ō	k?	ō
</s>
</p>
<p>
<s>
ztráta	ztráta	k1gFnSc1	ztráta
aspirace	aspirace	k1gFnSc2	aspirace
souhlásek	souhláska	k1gFnPc2	souhláska
</s>
</p>
<p>
<s>
skladba	skladba	k1gFnSc1	skladba
upřednostňující	upřednostňující	k2eAgNnSc1d1	upřednostňující
vkládání	vkládání	k1gNnSc1	vkládání
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
daný	daný	k2eAgInSc1d1	daný
větný	větný	k2eAgInSc1d1	větný
člen	člen	k1gInSc1	člen
<g/>
,	,	kIx,	,
nalevo	nalevo	k6eAd1	nalevo
(	(	kIx(	(
<g/>
nový	nový	k2eAgInSc4d1	nový
dům	dům	k1gInSc4	dům
místo	místo	k1gNnSc4	místo
dům	dům	k1gInSc4	dům
nový	nový	k2eAgInSc4d1	nový
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
užití	užití	k1gNnSc1	užití
slovesného	slovesný	k2eAgNnSc2d1	slovesné
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
slovesa	sloveso	k1gNnSc2	sloveso
kiyanavā	kiyanavā	k?	kiyanavā
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
říkat	říkat	k5eAaImF	říkat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
podřadicí	podřadicí	k2eAgFnSc2d1	podřadicí
spojky	spojka	k1gFnSc2	spojka
"	"	kIx"	"
<g/>
že	že	k8xS	že
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
jestli	jestli	k8xS	jestli
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nové	nový	k2eAgNnSc1d1	nové
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nové	nový	k2eAgNnSc1d1	nové
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Evropské	evropský	k2eAgInPc1d1	evropský
jazyky	jazyk	k1gInPc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
staletím	staletí	k1gNnSc7	staletí
koloniální	koloniální	k2eAgFnSc2d1	koloniální
nadvlády	nadvláda	k1gFnSc2	nadvláda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
současná	současný	k2eAgFnSc1d1	současná
sinhálština	sinhálština	k1gFnSc1	sinhálština
mnoho	mnoho	k6eAd1	mnoho
přejatých	přejatý	k2eAgNnPc2d1	přejaté
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
portugalštiny	portugalština	k1gFnSc2	portugalština
<g/>
,	,	kIx,	,
nizozemštiny	nizozemština	k1gFnSc2	nizozemština
a	a	k8xC	a
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Číslovky	číslovka	k1gFnSc2	číslovka
==	==	k?	==
</s>
</p>
<p>
<s>
Sinhálština	sinhálština	k1gFnSc1	sinhálština
sdílí	sdílet	k5eAaImIp3nS	sdílet
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
indoevropskými	indoevropský	k2eAgInPc7d1	indoevropský
jazyky	jazyk	k1gInPc7	jazyk
mnoho	mnoho	k6eAd1	mnoho
společných	společný	k2eAgInPc2d1	společný
rysů	rys	k1gInPc2	rys
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
patrné	patrný	k2eAgNnSc1d1	patrné
na	na	k7c6	na
číslovkách	číslovka	k1gFnPc6	číslovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dialekty	dialekt	k1gInPc4	dialekt
==	==	k?	==
</s>
</p>
<p>
<s>
Sinhálština	sinhálština	k1gFnSc1	sinhálština
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
provincii	provincie	k1gFnSc6	provincie
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
sinhálštiny	sinhálština	k1gFnSc2	sinhálština
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
mluví	mluvit	k5eAaImIp3nP	mluvit
obyvatelé	obyvatel	k1gMnPc1	obyvatel
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
části	část	k1gFnSc2	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
cizinci	cizinec	k1gMnPc1	cizinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nP	učit
západnímu	západní	k2eAgInSc3d1	západní
dialektu	dialekt	k1gInSc3	dialekt
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
standard	standard	k1gInSc4	standard
sinhálštiny	sinhálština	k1gFnSc2	sinhálština
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
dorozumět	dorozumět	k5eAaPmF	dorozumět
se	se	k3xPyFc4	se
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rodilé	rodilý	k2eAgMnPc4d1	rodilý
mluvčí	mluvčí	k1gMnPc4	mluvčí
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgInPc4	dva
dialekty	dialekt	k1gInPc4	dialekt
srozumitelné	srozumitelný	k2eAgInPc4d1	srozumitelný
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
si	se	k3xPyFc3	se
ani	ani	k9	ani
neuvědomuje	uvědomovat	k5eNaImIp3nS	uvědomovat
jejich	jejich	k3xOp3gFnSc4	jejich
rozdílnost	rozdílnost	k1gFnSc4	rozdílnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Veddština	Veddština	k1gFnSc1	Veddština
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc1	jazyk
srílanských	srílanský	k2eAgMnPc2d1	srílanský
domorodců	domorodec	k1gMnPc2	domorodec
(	(	kIx(	(
<g/>
známí	známit	k5eAaImIp3nS	známit
také	také	k9	také
jako	jako	k9	jako
"	"	kIx"	"
<g/>
lesní	lesní	k2eAgMnPc1d1	lesní
lidé	člověk	k1gMnPc1	člověk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sinhálštině	sinhálština	k1gFnSc3	sinhálština
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
rysů	rys	k1gInPc2	rys
podobá	podobat	k5eAaImIp3nS	podobat
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nepocházejí	pocházet	k5eNaImIp3nP	pocházet
z	z	k7c2	z
žádného	žádný	k3yNgInSc2	žádný
jiného	jiný	k2eAgInSc2d1	jiný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diglosie	Diglosie	k1gFnSc2	Diglosie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
sinhálštině	sinhálština	k1gFnSc6	sinhálština
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
jev	jev	k1gInSc1	jev
zvaný	zvaný	k2eAgInSc1d1	zvaný
diglosie	diglosie	k1gFnSc1	diglosie
<g/>
,	,	kIx,	,
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
jazyků	jazyk	k1gInPc2	jazyk
jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
spisovná	spisovný	k2eAgFnSc1d1	spisovná
a	a	k8xC	a
hovorová	hovorový	k2eAgFnSc1d1	hovorová
sinhálština	sinhálština	k1gFnSc1	sinhálština
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
do	do	k7c2	do
takové	takový	k3xDgFnSc2	takový
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
navenek	navenek	k6eAd1	navenek
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
téměř	téměř	k6eAd1	téměř
jako	jako	k8xC	jako
dva	dva	k4xCgInPc4	dva
odlišné	odlišný	k2eAgInPc4d1	odlišný
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Spisovná	spisovný	k2eAgFnSc1d1	spisovná
sinhálština	sinhálština	k1gFnSc1	sinhálština
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nS	mluvit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
pak	pak	k6eAd1	pak
ve	v	k7c6	v
formálních	formální	k2eAgFnPc6d1	formální
situacích	situace	k1gFnPc6	situace
(	(	kIx(	(
<g/>
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
na	na	k7c6	na
úřadech	úřad	k1gInPc6	úřad
<g/>
,	,	kIx,	,
při	při	k7c6	při
veřejných	veřejný	k2eAgInPc6d1	veřejný
proslovech	proslov	k1gInPc6	proslov
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
hovorová	hovorový	k2eAgFnSc1d1	hovorová
sinhálština	sinhálština	k1gFnSc1	sinhálština
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Spisovná	spisovný	k2eAgFnSc1d1	spisovná
forma	forma	k1gFnSc1	forma
jazyka	jazyk	k1gInSc2	jazyk
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
slov	slovo	k1gNnPc2	slovo
pocházejících	pocházející	k2eAgMnPc2d1	pocházející
ze	z	k7c2	z
sanskrtu	sanskrt	k1gInSc2	sanskrt
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
rozdílem	rozdíl	k1gInSc7	rozdíl
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
vrstvami	vrstva	k1gFnPc7	vrstva
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
sinhálštině	sinhálština	k1gFnSc6	sinhálština
se	se	k3xPyFc4	se
slovesa	sloveso	k1gNnSc2	sloveso
nijak	nijak	k6eAd1	nijak
nečasují	časovat	k5eNaBmIp3nP	časovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Písmo	písmo	k1gNnSc1	písmo
==	==	k?	==
</s>
</p>
<p>
<s>
Sinhálské	sinhálský	k2eAgNnSc4d1	sinhálské
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
Sinhala	Sinhal	k1gMnSc2	Sinhal
Hodiya	Hodiyus	k1gMnSc2	Hodiyus
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
písma	písmo	k1gNnPc4	písmo
ostatních	ostatní	k2eAgInPc2d1	ostatní
indoárijských	indoárijský	k2eAgInPc2d1	indoárijský
jazyků	jazyk	k1gInPc2	jazyk
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
z	z	k7c2	z
písma	písmo	k1gNnSc2	písmo
bráhmí	bráhmí	k1gNnSc2	bráhmí
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
písmo	písmo	k1gNnSc4	písmo
poloslabičné	poloslabičný	k2eAgNnSc4d1	poloslabičný
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedno	jeden	k4xCgNnSc1	jeden
písmeno	písmeno	k1gNnSc1	písmeno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ක	ක	k?	ක
<g/>
)	)	kIx)	)
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
slabiku	slabika	k1gFnSc4	slabika
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
souhláskou	souhláska	k1gFnSc7	souhláska
a	a	k8xC	a
výchozí	výchozí	k2eAgFnSc7d1	výchozí
samohláskou	samohláska	k1gFnSc7	samohláska
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
ka	ka	k?	ka
[	[	kIx(	[
<g/>
kə	kə	k?	kə
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
vytvořit	vytvořit	k5eAaPmF	vytvořit
slabiku	slabika	k1gFnSc4	slabika
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
než	než	k8xS	než
výchozí	výchozí	k2eAgFnSc7d1	výchozí
samohláskou	samohláska	k1gFnSc7	samohláska
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
přidat	přidat	k5eAaPmF	přidat
k	k	k7c3	k
písmenu	písmeno	k1gNnSc3	písmeno
tzv.	tzv.	kA	tzv.
pilla	pillo	k1gNnPc4	pillo
<g/>
,	,	kIx,	,
diakritická	diakritický	k2eAgNnPc4d1	diakritické
znaménka	znaménko	k1gNnPc4	znaménko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
slouží	sloužit	k5eAaImIp3nP	sloužit
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
ostatních	ostatní	k2eAgFnPc2d1	ostatní
samohlásek	samohláska	k1gFnPc2	samohláska
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
výslovnost	výslovnost	k1gFnSc1	výslovnost
daného	daný	k2eAgNnSc2d1	dané
písmena	písmeno	k1gNnSc2	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
znaky	znak	k1gInPc1	znak
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
nad	nad	k7c7	nad
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
nebo	nebo	k8xC	nebo
vedle	vedle	k6eAd1	vedle
písmena	písmeno	k1gNnPc4	písmeno
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
svůj	svůj	k3xOyFgInSc4	svůj
tvar	tvar	k1gInSc4	tvar
podle	podle	k7c2	podle
povahy	povaha	k1gFnSc2	povaha
písmena	písmeno	k1gNnSc2	písmeno
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgMnSc3	který
jsou	být	k5eAaImIp3nP	být
připojovány	připojován	k2eAgFnPc1d1	připojována
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
můžeme	moct	k5eAaImIp1nP	moct
z	z	k7c2	z
písmena	písmeno	k1gNnSc2	písmeno
ක	ක	k?	ක
ka	ka	k?	ka
vytvořit	vytvořit	k5eAaPmF	vytvořit
písmena	písmeno	k1gNnPc4	písmeno
ක	ක	k?	ක
<g/>
ා	ා	k?	ා
kā	kā	k?	kā
<g/>
,	,	kIx,	,
ක	ක	k?	ක
<g/>
ැ	ැ	k?	ැ
kä	kä	k?	kä
<g/>
,	,	kIx,	,
ක	ක	k?	ක
<g/>
ෑ	ෑ	k?	ෑ
kǟ	kǟ	k?	kǟ
<g/>
,	,	kIx,	,
ක	ක	k?	ක
<g/>
ි	ි	k?	ි
ki	ki	k?	ki
<g/>
,	,	kIx,	,
ක	ක	k?	ක
<g/>
ී	ී	k?	ී
kī	kī	k?	kī
<g/>
,	,	kIx,	,
ක	ක	k?	ක
<g/>
ු	ු	k?	ු
ku	k	k7c3	k
<g/>
,	,	kIx,	,
ක	ක	k?	ක
<g/>
ූ	ූ	k?	ූ
kū	kū	k?	kū
<g/>
,	,	kIx,	,
ක	ක	k?	ක
<g/>
ෙ	ෙ	k?	ෙ
ke	k	k7c3	k
<g/>
,	,	kIx,	,
ක	ක	k?	ක
<g/>
ේ	ේ	k?	ේ
kē	kē	k?	kē
<g/>
,	,	kIx,	,
ක	ක	k?	ක
<g/>
ො	ො	k?	ො
ko	ko	k?	ko
a	a	k8xC	a
'	'	kIx"	'
<g/>
ක	ක	k?	ක
<g/>
ෝ	ෝ	k?	ෝ
kō	kō	k?	kō
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zapsání	zapsání	k1gNnSc4	zapsání
absence	absence	k1gFnSc2	absence
samohlásky	samohláska	k1gFnSc2	samohláska
ve	v	k7c6	v
slabice	slabika	k1gFnSc6	slabika
sinhálština	sinhálština	k1gFnSc1	sinhálština
používá	používat	k5eAaImIp3nS	používat
speciální	speciální	k2eAgInSc4d1	speciální
znak	znak	k1gInSc4	znak
zvaný	zvaný	k2eAgInSc4d1	zvaný
hal	hala	k1gFnPc2	hala
kirī	kirī	k?	kirī
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kompletní	kompletní	k2eAgFnSc4d1	kompletní
sinhálskou	sinhálský	k2eAgFnSc4d1	sinhálská
abecedu	abeceda	k1gFnSc4	abeceda
<g/>
,	,	kIx,	,
Elu	Ela	k1gFnSc4	Ela
Hodiya	Hodiy	k1gInSc2	Hodiy
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
54	[number]	k4	54
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
18	[number]	k4	18
samohlásek	samohláska	k1gFnPc2	samohláska
a	a	k8xC	a
36	[number]	k4	36
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
původních	původní	k2eAgNnPc2d1	původní
sinhálských	sinhálský	k2eAgNnPc2d1	sinhálské
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
Suddha	Suddha	k1gFnSc1	Suddha
Sinhala	Sinhal	k1gMnSc2	Sinhal
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
však	však	k9	však
potřeba	potřeba	k6eAd1	potřeba
pouze	pouze	k6eAd1	pouze
36	[number]	k4	36
písmen	písmeno	k1gNnPc2	písmeno
(	(	kIx(	(
<g/>
12	[number]	k4	12
samohlásek	samohláska	k1gFnPc2	samohláska
a	a	k8xC	a
24	[number]	k4	24
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
písmena	písmeno	k1gNnPc4	písmeno
z	z	k7c2	z
Elu	Ela	k1gFnSc4	Ela
Hodiya	Hodiy	k1gInSc2	Hodiy
(	(	kIx(	(
<g/>
např.	např.	kA	např.
aspiranty	aspirant	k1gMnPc4	aspirant
<g/>
)	)	kIx)	)
se	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
výslovnostních	výslovnostní	k2eAgFnPc2d1	výslovnostní
změn	změna	k1gFnPc2	změna
u	u	k7c2	u
sinhálských	sinhálský	k2eAgNnPc2d1	sinhálské
slov	slovo	k1gNnPc2	slovo
přestala	přestat	k5eAaPmAgFnS	přestat
používat	používat	k5eAaImF	používat
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	on	k3xPp3gInPc4	on
najdeme	najít	k5eAaPmIp1nP	najít
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
přejatých	přejatý	k2eAgNnPc6d1	přejaté
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
sanskrt	sanskrt	k1gInSc4	sanskrt
nebo	nebo	k8xC	nebo
páli	pále	k1gFnSc4	pále
<g/>
.	.	kIx.	.
</s>
<s>
Sinhálské	sinhálský	k2eAgNnSc1d1	sinhálské
písmo	písmo	k1gNnSc1	písmo
je	být	k5eAaImIp3nS	být
psáno	psát	k5eAaImNgNnS	psát
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
a	a	k8xC	a
sinhálština	sinhálština	k1gFnSc1	sinhálština
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
jej	on	k3xPp3gMnSc4	on
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
řazení	řazení	k1gNnSc1	řazení
písmen	písmeno	k1gNnPc2	písmeno
za	za	k7c7	za
sebou	se	k3xPyFc7	se
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
odlišný	odlišný	k2eAgMnSc1d1	odlišný
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
evropského	evropský	k2eAgNnSc2d1	Evropské
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
ā	ā	k?	ā
ä	ä	k?	ä
<g/>
/	/	kIx~	/
<g/>
ǟ	ǟ	k?	ǟ
i	i	k8xC	i
<g/>
/	/	kIx~	/
<g/>
ī	ī	k?	ī
u	u	k7c2	u
<g/>
/	/	kIx~	/
<g/>
ū	ū	k?	ū
[	[	kIx(	[
<g/>
ŗ	ŗ	k?	ŗ
<g/>
]	]	kIx)	]
e	e	k0	e
<g/>
/	/	kIx~	/
<g/>
ē	ē	k?	ē
[	[	kIx(	[
<g/>
ai	ai	k?	ai
<g/>
]	]	kIx)	]
o	o	k7c4	o
<g/>
/	/	kIx~	/
<g/>
ō	ō	k?	ō
[	[	kIx(	[
<g/>
au	au	k0	au
<g/>
]	]	kIx)	]
k	k	k7c3	k
[	[	kIx(	[
<g/>
kh	kh	k0	kh
<g/>
]	]	kIx)	]
<g />
.	.	kIx.	.
</s>
<s>
g	g	kA	g
[	[	kIx(	[
<g/>
g	g	kA	g
<g/>
]	]	kIx)	]
ṅ	ṅ	k?	ṅ
c	c	k0	c
[	[	kIx(	[
<g/>
ch	ch	k0	ch
<g/>
]	]	kIx)	]
j	j	k?	j
[	[	kIx(	[
<g/>
jh	jh	k?	jh
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
ñ	ñ	k?	ñ
<g/>
]	]	kIx)	]
ṭ	ṭ	k?	ṭ
[	[	kIx(	[
<g/>
ṭ	ṭ	k?	ṭ
<g/>
]	]	kIx)	]
ṭ	ṭ	k?	ṭ
[	[	kIx(	[
<g/>
ṭ	ṭ	k?	ṭ
<g/>
]	]	kIx)	]
ḍ	ḍ	k?	ḍ
[	[	kIx(	[
<g/>
ḍ	ḍ	k?	ḍ
<g/>
]	]	kIx)	]
ṇ	ṇ	k?	ṇ
t	t	k?	t
[	[	kIx(	[
<g/>
th	th	k?	th
<g/>
]	]	kIx)	]
d	d	k?	d
[	[	kIx(	[
<g/>
dh	dh	k?	dh
<g/>
]	]	kIx)	]
n	n	k0	n
p	p	k?	p
[	[	kIx(	[
<g/>
ph	ph	kA	ph
<g/>
]	]	kIx)	]
b	b	k?	b
[	[	kIx(	[
<g/>
bh	bh	k?	bh
<g/>
]	]	kIx)	]
m	m	kA	m
y	y	k?	y
r	r	kA	r
l	l	kA	l
v	v	k7c6	v
[	[	kIx(	[
<g/>
ś	ś	k?	ś
ṣ	ṣ	k?	ṣ
<g/>
]	]	kIx)	]
s	s	k7c7	s
h	h	k?	h
ḷ	ḷ	k?	ḷ
f	f	k?	f
</s>
</p>
<p>
<s>
==	==	k?	==
Fonologie	fonologie	k1gFnSc2	fonologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Souhlásky	souhláska	k1gFnSc2	souhláska
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Samohlásky	samohláska	k1gFnSc2	samohláska
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Morfologie	morfologie	k1gFnSc2	morfologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Skloňování	skloňování	k1gNnSc2	skloňování
===	===	k?	===
</s>
</p>
<p>
<s>
Sinhálština	sinhálština	k1gFnSc1	sinhálština
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
tyto	tento	k3xDgFnPc4	tento
mluvnické	mluvnický	k2eAgFnPc4d1	mluvnická
kategorie	kategorie	k1gFnPc4	kategorie
<g/>
:	:	kIx,	:
pád	pád	k1gInSc1	pád
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
životnost	životnost	k1gFnSc1	životnost
a	a	k8xC	a
určitost	určitost	k1gFnSc1	určitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Pád	Pád	k1gInSc1	Pád
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
sinhálštině	sinhálština	k1gFnSc6	sinhálština
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
pádů	pád	k1gInPc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
těch	ten	k3xDgFnPc2	ten
běžných	běžný	k2eAgFnPc2d1	běžná
(	(	kIx(	(
<g/>
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
,	,	kIx,	,
dativ	dativ	k1gInSc1	dativ
<g/>
,	,	kIx,	,
genitiv	genitiv	k1gInSc1	genitiv
<g/>
,	,	kIx,	,
ablativ	ablativ	k1gInSc1	ablativ
<g/>
)	)	kIx)	)
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
méně	málo	k6eAd2	málo
časté	častý	k2eAgInPc1d1	častý
pády	pád	k1gInPc1	pád
<g/>
,	,	kIx,	,
např.	např.	kA	např.
instrumentál	instrumentál	k1gInSc1	instrumentál
nebo	nebo	k8xC	nebo
vokativ	vokativ	k1gInSc1	vokativ
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
najdeme	najít	k5eAaPmIp1nP	najít
i	i	k8xC	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
počet	počet	k1gInSc1	počet
pádů	pád	k1gInPc2	pád
v	v	k7c6	v
sinhálštině	sinhálština	k1gFnSc6	sinhálština
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
samotné	samotný	k2eAgFnPc4d1	samotná
definici	definice	k1gFnSc4	definice
pádu	pád	k1gInSc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
Sinhálský	sinhálský	k2eAgInSc1d1	sinhálský
lokativ	lokativ	k1gInSc1	lokativ
a	a	k8xC	a
instrumentál	instrumentál	k1gInSc1	instrumentál
(	(	kIx(	(
<g/>
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
českému	český	k2eAgMnSc3d1	český
šestému	šestý	k4xOgInSc3	šestý
a	a	k8xC	a
sedmému	sedmý	k4xOgInSc3	sedmý
pádu	pád	k1gInSc3	pád
<g/>
)	)	kIx)	)
totiž	totiž	k9	totiž
někdy	někdy	k6eAd1	někdy
nejsou	být	k5eNaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
opravdové	opravdový	k2eAgInPc4d1	opravdový
pády	pád	k1gInPc4	pád
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jejich	jejich	k3xOp3gFnPc1	jejich
životné	životný	k2eAgFnPc1d1	životná
koncovky	koncovka	k1gFnPc1	koncovka
<g/>
,	,	kIx,	,
laŋ	laŋ	k?	laŋ
a	a	k8xC	a
atiŋ	atiŋ	k?	atiŋ
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaPmF	stát
jako	jako	k9	jako
samostatná	samostatný	k2eAgNnPc4d1	samostatné
slova	slovo	k1gNnPc4	slovo
s	s	k7c7	s
významy	význam	k1gInPc7	význam
"	"	kIx"	"
<g/>
blízko	blízko	k6eAd1	blízko
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Závorky	závorka	k1gFnPc1	závorka
označují	označovat	k5eAaImIp3nP	označovat
volitelné	volitelný	k2eAgNnSc4d1	volitelné
zkracování	zkracování	k1gNnSc4	zkracování
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
samohlásek	samohláska	k1gFnPc2	samohláska
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
nepřízvučných	přízvučný	k2eNgFnPc6d1	nepřízvučná
slabikách	slabika	k1gFnPc6	slabika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Číslo	číslo	k1gNnSc4	číslo
====	====	k?	====
</s>
</p>
<p>
<s>
U	u	k7c2	u
životných	životný	k2eAgNnPc2d1	životné
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
se	se	k3xPyFc4	se
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
tvoří	tvořit	k5eAaImIp3nS	tvořit
pomocí	pomocí	k7c2	pomocí
koncovek	koncovka	k1gFnPc2	koncovka
-o	-o	k?	-o
<g/>
(	(	kIx(	(
<g/>
:	:	kIx,	:
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zdvojená	zdvojený	k2eAgFnSc1d1	zdvojená
souhláska	souhláska	k1gFnSc1	souhláska
+	+	kIx~	+
-u	-u	k?	-u
nebo	nebo	k8xC	nebo
-la	-la	k?	-la
<g/>
(	(	kIx(	(
<g/>
:	:	kIx,	:
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
neživotných	životný	k2eNgNnPc2d1	neživotné
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
tvoří	tvořit	k5eAaImIp3nS	tvořit
plurál	plurál	k1gInSc1	plurál
vypuštěním	vypuštění	k1gNnSc7	vypuštění
části	část	k1gFnSc2	část
tvaru	tvar	k1gInSc2	tvar
singuláru	singulár	k1gInSc2	singulár
<g/>
.	.	kIx.	.
</s>
<s>
Jednotné	jednotný	k2eAgNnSc1d1	jednotné
číslo	číslo	k1gNnSc1	číslo
slov	slovo	k1gNnPc2	slovo
přejatých	přejatý	k2eAgFnPc2d1	přejatá
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
je	být	k5eAaImIp3nS	být
značeno	značit	k5eAaImNgNnS	značit
slovem	slovo	k1gNnSc7	slovo
ekə	ekə	k?	ekə
a	a	k8xC	a
v	v	k7c6	v
plurálu	plurál	k1gInSc6	plurál
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
slova	slovo	k1gNnPc1	slovo
nijak	nijak	k6eAd1	nijak
nemění	měnit	k5eNaImIp3nP	měnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
části	část	k1gFnSc6	část
tabulky	tabulka	k1gFnSc2	tabulka
jsou	být	k5eAaImIp3nP	být
tvary	tvar	k1gInPc1	tvar
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
delší	dlouhý	k2eAgMnSc1d2	delší
než	než	k8xS	než
tvary	tvar	k1gInPc1	tvar
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
vpravo	vpravo	k6eAd1	vpravo
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přesně	přesně	k6eAd1	přesně
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Určitý	určitý	k2eAgMnSc1d1	určitý
a	a	k8xC	a
neurčitý	určitý	k2eNgMnSc1d1	neurčitý
člen	člen	k1gMnSc1	člen
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
čísle	číslo	k1gNnSc6	číslo
je	být	k5eAaImIp3nS	být
neurčitý	určitý	k2eNgInSc1d1	neurčitý
člen	člen	k1gInSc1	člen
vyjádřen	vyjádřen	k2eAgInSc1d1	vyjádřen
koncovkami	koncovka	k1gFnPc7	koncovka
-ek	-ek	k?	-ek
pro	pro	k7c4	pro
životná	životný	k2eAgNnPc4d1	životné
a	a	k8xC	a
-ak	-ak	k?	-ak
pro	pro	k7c4	pro
neživotná	životný	k2eNgNnPc4d1	neživotné
podstatná	podstatný	k2eAgNnPc4d1	podstatné
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Absence	absence	k1gFnSc1	absence
neurčitého	určitý	k2eNgInSc2d1	neurčitý
členu	člen	k1gInSc2	člen
značí	značit	k5eAaImIp3nS	značit
určitost	určitost	k1gFnSc1	určitost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
se	s	k7c7	s
členy	člen	k1gMnPc7	člen
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Časování	časování	k1gNnSc2	časování
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
sinhálštině	sinhálština	k1gFnSc6	sinhálština
jsou	být	k5eAaImIp3nP	být
slovesa	sloveso	k1gNnSc2	sloveso
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
třid	třida	k1gFnPc2	třida
<g/>
.	.	kIx.	.
</s>
<s>
Mluvená	mluvený	k2eAgFnSc1d1	mluvená
sinhálština	sinhálština	k1gFnSc1	sinhálština
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
nijak	nijak	k6eAd1	nijak
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
osoby	osoba	k1gFnPc4	osoba
ani	ani	k8xC	ani
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
neprobíhá	probíhat	k5eNaImIp3nS	probíhat
tedy	tedy	k9	tedy
mluvnická	mluvnický	k2eAgFnSc1d1	mluvnická
shoda	shoda	k1gFnSc1	shoda
mezi	mezi	k7c7	mezi
přísudkem	přísudek	k1gInSc7	přísudek
a	a	k8xC	a
podmětem	podmět	k1gInSc7	podmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skladba	skladba	k1gFnSc1	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
slovosled	slovosled	k1gInSc1	slovosled
typu	typ	k1gInSc2	typ
SOV	sova	k1gFnPc2	sova
(	(	kIx(	(
<g/>
podmět	podmět	k1gInSc1	podmět
předmět	předmět	k1gInSc1	předmět
sloveso	sloveso	k1gNnSc1	sloveso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
se	se	k3xPyFc4	se
levé	levý	k2eAgNnSc1d1	levé
větvení	větvení	k1gNnSc1	větvení
<g/>
,	,	kIx,	,
rozvíjející	rozvíjející	k2eAgInPc1d1	rozvíjející
větné	větný	k2eAgInPc1d1	větný
členy	člen	k1gInPc1	člen
jsou	být	k5eAaImIp3nP	být
umisťovány	umisťovat	k5eAaImNgInP	umisťovat
před	před	k7c4	před
rozvíjené	rozvíjený	k2eAgInPc4d1	rozvíjený
větné	větný	k2eAgInPc4d1	větný
členy	člen	k1gInPc4	člen
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
píše	psát	k5eAaImIp3nS	psát
knihy	kniha	k1gFnSc2	kniha
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
přeloží	přeložit	k5eAaPmIp3nS	přeložit
jako	jako	k9	jako
pot	pot	k1gInSc4	pot
<g/>
̪	̪	k?	̪
liə	liə	k?	liə
miniha	miniha	k1gFnSc1	miniha
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
knihy	kniha	k1gFnSc2	kniha
píšící	píšící	k2eAgMnSc1d1	píšící
muž	muž	k1gMnSc1	muž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neužívá	užívat	k5eNaImIp3nS	užívat
se	se	k3xPyFc4	se
spojek	spojka	k1gFnPc2	spojka
že	že	k8xS	že
nebo	nebo	k8xC	nebo
jestli	jestli	k8xS	jestli
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nahrazovány	nahrazovat	k5eAaImNgInP	nahrazovat
slovesnými	slovesný	k2eAgInPc7d1	slovesný
přídavnými	přídavný	k2eAgNnPc7d1	přídavné
jmény	jméno	k1gNnPc7	jméno
(	(	kIx(	(
<g/>
příklad	příklad	k1gInSc1	příklad
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
a	a	k8xC	a
příčestími	příčestí	k1gNnPc7	příčestí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předložky	předložka	k1gFnPc1	předložka
jsou	být	k5eAaImIp3nP	být
vkládány	vkládat	k5eAaImNgFnP	vkládat
za	za	k7c4	za
slovní	slovní	k2eAgNnSc4d1	slovní
spojení	spojení	k1gNnSc4	spojení
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
patří	patřit	k5eAaImIp3nS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pod	pod	k7c7	pod
knihou	kniha	k1gFnSc7	kniha
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
přeloží	přeložit	k5eAaPmIp3nS	přeložit
jako	jako	k9	jako
pot	pot	k1gInSc4	pot
<g/>
̪	̪	k?	̪
<g/>
ə	ə	k?	ə
yaʈ	yaʈ	k?	yaʈ
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
knihou	kniha	k1gFnSc7	kniha
pod	pod	k7c7	pod
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
přísudková	přísudkový	k2eAgFnSc1d1	přísudková
spona	spona	k1gFnSc1	spona
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
bohatý	bohatý	k2eAgMnSc1d1	bohatý
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
překládá	překládat	k5eAaImIp3nS	překládat
jako	jako	k9	jako
mamə	mamə	k?	mamə
poː	poː	k?	poː
<g/>
̪	̪	k?	̪
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
já	já	k3xPp1nSc1	já
bohatý	bohatý	k2eAgInSc4d1	bohatý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sémantika	sémantika	k1gFnSc1	sémantika
==	==	k?	==
</s>
</p>
<p>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
sinhálštiny	sinhálština	k1gFnSc2	sinhálština
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
aktuální	aktuální	k2eAgFnSc1d1	aktuální
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
mluvčího	mluvčí	k1gMnSc2	mluvčí
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mění	měnit	k5eAaImIp3nS	měnit
mluvčího	mluvčí	k1gMnSc2	mluvčí
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
věci	věc	k1gFnSc3	věc
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
čtyři	čtyři	k4xCgNnPc1	čtyři
ukazovací	ukazovací	k2eAgNnPc1d1	ukazovací
zájmena	zájmeno	k1gNnPc1	zájmeno
<g/>
.	.	kIx.	.
</s>
<s>
Meː	Meː	k?	Meː
"	"	kIx"	"
<g/>
tady	tady	k6eAd1	tady
<g/>
,	,	kIx,	,
blízko	blízko	k6eAd1	blízko
mluvčího	mluvčí	k1gMnSc2	mluvčí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
oː	oː	k?	oː
"	"	kIx"	"
<g/>
"	"	kIx"	"
<g/>
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
blízko	blízko	k6eAd1	blízko
adresáta	adresát	k1gMnSc4	adresát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
arə	arə	k?	arə
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
třetí	třetí	k4xOgFnSc2	třetí
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
"	"	kIx"	"
a	a	k8xC	a
eː	eː	k?	eː
"	"	kIx"	"
<g/>
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
třetí	třetí	k4xOgFnSc2	třetí
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
věty	věta	k1gFnSc2	věta
naprosto	naprosto	k6eAd1	naprosto
jasná	jasný	k2eAgFnSc1d1	jasná
nějaká	nějaký	k3yIgFnSc1	nějaký
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
podmět	podmět	k1gInSc1	podmět
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
v	v	k7c6	v
sinhálštině	sinhálština	k1gFnSc6	sinhálština
prostě	prostě	k6eAd1	prostě
vypustit	vypustit	k5eAaPmF	vypustit
<g/>
.	.	kIx.	.
</s>
<s>
Stejná	stejný	k2eAgFnSc1d1	stejná
praxe	praxe	k1gFnSc1	praxe
funguje	fungovat	k5eAaImIp3nS	fungovat
i	i	k9	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc3d2	menší
míře	míra	k1gFnSc3	míra
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Věta	věta	k1gFnSc1	věta
[	[	kIx(	[
<g/>
koɦ	koɦ	k?	koɦ
<g/>
̪	̪	k?	̪
<g/>
ə	ə	k?	ə
ɡ	ɡ	k?	ɡ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
kam	kam	k6eAd1	kam
šel	jít	k5eAaImAgMnS	jít
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
"	"	kIx"	"
<g/>
kam	kam	k6eAd1	kam
jsem	být	k5eAaImIp1nS	být
<g/>
/	/	kIx~	/
<g/>
jsi	být	k5eAaImIp2nS	být
<g/>
/	/	kIx~	/
<g/>
jsme	být	k5eAaImIp1nP	být
<g/>
/	/	kIx~	/
<g/>
jste	být	k5eAaImIp2nP	být
šel	jít	k5eAaImAgInS	jít
<g/>
/	/	kIx~	/
<g/>
šli	šle	k1gFnSc4	šle
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
