<s>
Právní	právní	k2eAgNnSc1d1
vědomí	vědomí	k1gNnSc1
označuje	označovat	k5eAaImIp3nS
všeobecné	všeobecný	k2eAgFnPc4d1
znalosti	znalost	k1gFnPc4
o	o	k7c6
právu	právo	k1gNnSc6
<g/>
,	,	kIx,
představy	představa	k1gFnPc4
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
platnosti	platnost	k1gFnSc6
a	a	k8xC
oprávněnosti	oprávněnost	k1gFnSc6
<g/>
.	.	kIx.
</s>