<s>
Právní	právní	k2eAgNnSc1d1
vědomí	vědomí	k1gNnSc1
</s>
<s>
Právní	právní	k2eAgNnSc1d1
vědomí	vědomí	k1gNnSc1
označuje	označovat	k5eAaImIp3nS
všeobecné	všeobecný	k2eAgFnPc4d1
znalosti	znalost	k1gFnPc4
o	o	k7c6
právu	právo	k1gNnSc6
<g/>
,	,	kIx,
představy	představa	k1gFnPc4
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
platnosti	platnost	k1gFnSc6
a	a	k8xC
oprávněnosti	oprávněnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
základem	základ	k1gInSc7
je	být	k5eAaImIp3nS
idea	idea	k1gFnSc1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
představa	představa	k1gFnSc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
je	být	k5eAaImIp3nS
„	„	k?
<g/>
po	po	k7c6
právu	právo	k1gNnSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
však	však	k9
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
od	od	k7c2
platného	platný	k2eAgNnSc2d1
práva	právo	k1gNnSc2
i	i	k9
značně	značně	k6eAd1
odlišná	odlišný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blíží	blížit	k5eAaImIp3nS
se	se	k3xPyFc4
proto	proto	k8xC
více	hodně	k6eAd2
ideji	idea	k1gFnSc3
spravedlnosti	spravedlnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Může	moct	k5eAaImIp3nS
být	být	k5eAaImF
celospolečenské	celospolečenský	k2eAgFnSc3d1
<g/>
,	,	kIx,
skupinové	skupinový	k2eAgFnSc3d1
i	i	k8xC
individuální	individuální	k2eAgFnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vždy	vždy	k6eAd1
jde	jít	k5eAaImIp3nS
o	o	k7c4
vědomí	vědomí	k1gNnSc4
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
společnosti	společnost	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
a	a	k8xC
funguje	fungovat	k5eAaImIp3nS
nějaké	nějaký	k3yIgNnSc4
právo	právo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
určitým	určitý	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
hodnoceno	hodnocen	k2eAgNnSc4d1
nebo	nebo	k8xC
jde	jít	k5eAaImIp3nS
o	o	k7c4
představy	představa	k1gFnPc4
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jaké	jaký	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
(	(	kIx(
<g/>
vědomí	vědomí	k1gNnSc4
o	o	k7c6
právu	právo	k1gNnSc6
de	de	k?
lege	leg	k1gMnSc2
ferenda	ferend	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yInSc7,k3yRnSc7,k3yQnSc7
více	hodně	k6eAd2
se	se	k3xPyFc4
takové	takový	k3xDgNnSc1
vědomí	vědomí	k1gNnSc1
s	s	k7c7
platným	platný	k2eAgNnSc7d1
právem	právo	k1gNnSc7
shoduje	shodovat	k5eAaImIp3nS
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
více	hodně	k6eAd2
je	být	k5eAaImIp3nS
právo	právo	k1gNnSc1
dodržováno	dodržován	k2eAgNnSc1d1
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nP
ho	on	k3xPp3gNnSc4
lidé	člověk	k1gMnPc1
prakticky	prakticky	k6eAd1
museli	muset	k5eAaImAgMnP
znát	znát	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
právní	právní	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
se	se	k3xPyFc4
právě	právě	k9
znalost	znalost	k1gFnSc1
skutečně	skutečně	k6eAd1
platného	platný	k2eAgNnSc2d1
práva	právo	k1gNnSc2
(	(	kIx(
<g/>
vědomí	vědomí	k1gNnSc1
o	o	k7c6
právu	právo	k1gNnSc6
de	de	k?
lege	legat	k5eAaPmIp3nS
lata	lata	k1gFnSc1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc1
právního	právní	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
zpochybňuje	zpochybňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
je	být	k5eAaImIp3nS
takto	takto	k6eAd1
obecně	obecně	k6eAd1
přijímáno	přijímat	k5eAaImNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Např.	např.	kA
Viktor	Viktor	k1gMnSc1
Knapp	Knapp	k1gMnSc1
to	ten	k3xDgNnSc4
ale	ale	k8xC
s	s	k7c7
poukazem	poukaz	k1gInSc7
na	na	k7c4
absurdnost	absurdnost	k1gFnSc4
závěru	závěr	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
vyšší	vysoký	k2eAgFnSc4d2
právní	právní	k2eAgNnSc1d1
vědomí	vědomí	k1gNnSc1
muselo	muset	k5eAaImAgNnS
být	být	k5eAaImF
u	u	k7c2
podvodníků	podvodník	k1gMnPc2
znajících	znající	k2eAgMnPc2d1
většinou	většina	k1gFnSc7
platné	platný	k2eAgNnSc1d1
právo	právo	k1gNnSc1
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
u	u	k7c2
poctivých	poctivý	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
s	s	k7c7
jeho	jeho	k3xOp3gFnPc7
sankcemi	sankce	k1gFnPc7
do	do	k7c2
styku	styk	k1gInSc2
nepřicházejí	přicházet	k5eNaImIp3nP
<g/>
,	,	kIx,
odmítá	odmítat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Viktor	Viktor	k1gMnSc1
Knapp	Knapp	k1gMnSc1
<g/>
:	:	kIx,
Teorie	teorie	k1gFnSc1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
C.	C.	kA
H.	H.	kA
Beck	Beck	k1gMnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7179	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
28	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
82	#num#	k4
<g/>
–	–	k?
<g/>
83	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jaromír	Jaromír	k1gMnSc1
Harvánek	Harvánek	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Teorie	teorie	k1gFnSc1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aleš	Aleš	k1gMnSc1
Čeněk	Čeněk	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7380	#num#	k4
<g/>
-	-	kIx~
<g/>
104	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
299	#num#	k4
<g/>
–	–	k?
<g/>
301	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Aleš	Aleš	k1gMnSc1
Gerloch	Gerloch	k1gMnSc1
<g/>
:	:	kIx,
Teorie	teorie	k1gFnSc1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aleš	Aleš	k1gMnSc1
Čeněk	Čeněk	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7380	#num#	k4
<g/>
-	-	kIx~
<g/>
233	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
23	#num#	k4
<g/>
–	–	k?
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
8829	#num#	k4
</s>
