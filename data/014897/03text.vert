<s>
Kamejka	kamejka	k1gFnSc1
modronachová	modronachová	k1gFnSc1
</s>
<s>
Kamejka	kamejka	k1gFnSc1
modronachová	modronachová	k1gFnSc1
Kamejka	kamejka	k1gFnSc1
modronachová	modronachový	k2eAgFnSc1d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
vyšší	vysoký	k2eAgFnPc1d2
dvouděložné	dvouděložná	k1gFnPc1
(	(	kIx(
<g/>
Rosopsida	Rosopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
brutnákotvaré	brutnákotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Boraginales	Boraginales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
brutnákovité	brutnákovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Boraginaceae	Boraginacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
kamejka	kamejka	k1gFnSc1
(	(	kIx(
<g/>
Aegonychon	Aegonychon	k1gNnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Aegonychon	Aegonychon	k1gInSc1
purpurocaeruleum	purpurocaeruleum	k1gInSc1
<g/>
(	(	kIx(
<g/>
L.	L.	kA
<g/>
)	)	kIx)
Holub	Holub	k1gMnSc1
<g/>
,	,	kIx,
1973	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kamejka	kamejka	k1gFnSc1
modronachová	modronachová	k1gFnSc1
(	(	kIx(
<g/>
Aegonychon	Aegonychon	k1gInSc1
purpurocaeruleum	purpurocaeruleum	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
syn	syn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lithospermum	Lithospermum	k1gInSc1
purpurocaeruleum	purpurocaeruleum	k1gInSc4
<g/>
,	,	kIx,
Buglossoides	Buglossoides	k1gInSc4
purpurocaerulea	purpurocaerule	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vytrvalá	vytrvalý	k2eAgFnSc1d1
bylina	bylina	k1gFnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
brutnákovitých	brutnákovitý	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
německou	německý	k2eAgFnSc7d1
Rostlinou	rostlina	k1gFnSc7
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Má	mít	k5eAaImIp3nS
kopinaté	kopinatý	k2eAgNnSc1d1
<g/>
,	,	kIx,
střídavé	střídavý	k2eAgNnSc1d1
<g/>
,	,	kIx,
drsně	drsně	k6eAd1
srstnaté	srstnatý	k2eAgInPc4d1
listy	list	k1gInPc4
a	a	k8xC
nejčastěji	často	k6eAd3
jednoduché	jednoduchý	k2eAgFnPc4d1
lodyhy	lodyha	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oboupohlavné	oboupohlavný	k2eAgInPc1d1
květy	květ	k1gInPc1
jsou	být	k5eAaImIp3nP
uspořádány	uspořádat	k5eAaPmNgInP
ve	v	k7c6
vijanech	vijan	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc2
koruny	koruna	k1gFnSc2
jsou	být	k5eAaImIp3nP
nálevkovité	nálevkovitý	k2eAgFnPc1d1
<g/>
,	,	kIx,
pěticípé	pěticípý	k2eAgFnPc1d1
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
delší	dlouhý	k2eAgInSc4d2
kalich	kalich	k1gInSc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
nachové	nachový	k2eAgFnPc1d1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
blankytně	blankytně	k6eAd1
modré	modrý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rostlina	rostlina	k1gFnSc1
kvete	kvést	k5eAaImIp3nS
v	v	k7c6
květnu	květen	k1gInSc6
a	a	k8xC
v	v	k7c6
červnu	červen	k1gInSc6
<g/>
,	,	kIx,
opylována	opylovat	k5eAaImNgFnS
je	být	k5eAaImIp3nS
hmyzem	hmyz	k1gInSc7
<g/>
,	,	kIx,
plody	plod	k1gInPc1
jsou	být	k5eAaImIp3nP
tvrdky	tvrdka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
rozmnožuje	rozmnožovat	k5eAaImIp3nS
též	též	k9
vegetativně	vegetativně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
rostlina	rostlina	k1gFnSc1
preferující	preferující	k2eAgFnSc1d1
teplé	teplý	k2eAgNnSc4d1
<g/>
,	,	kIx,
spíše	spíše	k9
polostinné	polostinný	k2eAgFnSc2d1
polohy	poloha	k1gFnSc2
se	s	k7c7
suššími	suchý	k2eAgFnPc7d2
půdami	půda	k1gFnPc7
bohatými	bohatý	k2eAgFnPc7d1
na	na	k7c4
vápník	vápník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roste	růst	k5eAaImIp3nS
převážně	převážně	k6eAd1
v	v	k7c6
teplomilných	teplomilný	k2eAgInPc6d1
lesních	lesní	k2eAgInPc6d1
lemech	lem	k1gInPc6
<g/>
,	,	kIx,
v	v	k7c6
lesostepních	lesostepní	k2eAgFnPc6d1
mozaikách	mozaika	k1gFnPc6
křovin	křovina	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
rozvolněných	rozvolněný	k2eAgFnPc6d1
teplomilných	teplomilný	k2eAgFnPc6d1
doubravách	doubrava	k1gFnPc6
nebo	nebo	k8xC
na	na	k7c6
lesních	lesní	k2eAgFnPc6d1
světlinách	světlina	k1gFnPc6
dubohabřin	dubohabřina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
hlavně	hlavně	k9
v	v	k7c6
teplých	teplý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
středních	střední	k2eAgFnPc2d1
a	a	k8xC
severozápadních	severozápadní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
a	a	k8xC
v	v	k7c6
Bílých	bílý	k2eAgInPc6d1
Karpatech	Karpaty	k1gInPc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
patří	patřit	k5eAaImIp3nS
k	k	k7c3
vzácnějším	vzácný	k2eAgMnPc3d2
druhům	druh	k1gMnPc3
vyžadujícím	vyžadující	k2eAgMnPc3d1
pozornost	pozornost	k1gFnSc4
(	(	kIx(
<g/>
kategorie	kategorie	k1gFnSc2
C	C	kA
<g/>
4	#num#	k4
<g/>
a	a	k8xC
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Kamejka	kamejka	k1gFnSc1
modronachová	modronachová	k1gFnSc1
-	-	kIx~
Lithospermum	Lithospermum	k1gInSc1
purpureo-coeruleum	purpureo-coeruleum	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Aegonychon	Aegonychon	k1gInSc1
purpurocaeruleum	purpurocaeruleum	k1gInSc1
L.	L.	kA
–	–	k?
kamejka	kamejka	k1gFnSc1
modronachová	modronachová	k1gFnSc1
/	/	kIx~
kamienkovec	kamienkovec	k1gInSc1
modropurpurový	modropurpurový	k2eAgInSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kamejka	kamejka	k1gFnSc1
modronachová	modronachový	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Aegonychon	Aegonychon	k1gInSc1
purpurocaeruleum	purpurocaeruleum	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
</s>
