<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
nejblíže	blízce	k6eAd3	blízce
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
oběh	oběh	k1gInSc4	oběh
trvá	trvat	k5eAaImIp3nS	trvat
pouze	pouze	k6eAd1	pouze
87,969	[number]	k4	87,969
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
