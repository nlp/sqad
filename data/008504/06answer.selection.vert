<s>
Koloseum	Koloseum	k1gNnSc1	Koloseum
nebo	nebo	k8xC	nebo
Římské	římský	k2eAgNnSc1d1	římské
koloseum	koloseum	k1gNnSc1	koloseum
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Flaviovský	Flaviovský	k2eAgInSc4d1	Flaviovský
amfiteátr	amfiteátr	k1gInSc4	amfiteátr
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Amphitheatrum	Amphitheatrum	k1gNnSc1	Amphitheatrum
Flavium	Flavium	k1gNnSc1	Flavium
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Anfiteatro	Anfiteatro	k1gNnSc4	Anfiteatro
Flavio	Flavio	k6eAd1	Flavio
nebo	nebo	k8xC	nebo
Colosseo	Colosseo	k6eAd1	Colosseo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oválný	oválný	k2eAgInSc1d1	oválný
amfiteátr	amfiteátr	k1gInSc1	amfiteátr
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
Říma	Řím	k1gInSc2	Řím
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
