<p>
<s>
Koloseum	Koloseum	k1gNnSc1	Koloseum
nebo	nebo	k8xC	nebo
Římské	římský	k2eAgNnSc1d1	římské
koloseum	koloseum	k1gNnSc1	koloseum
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Flaviovský	Flaviovský	k2eAgInSc4d1	Flaviovský
amfiteátr	amfiteátr	k1gInSc4	amfiteátr
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Amphitheatrum	Amphitheatrum	k1gNnSc1	Amphitheatrum
Flavium	Flavium	k1gNnSc1	Flavium
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Anfiteatro	Anfiteatro	k1gNnSc4	Anfiteatro
Flavio	Flavio	k6eAd1	Flavio
nebo	nebo	k8xC	nebo
Colosseo	Colosseo	k6eAd1	Colosseo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oválný	oválný	k2eAgInSc1d1	oválný
amfiteátr	amfiteátr	k1gInSc1	amfiteátr
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
Říma	Řím	k1gInSc2	Řím
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
amfiteátr	amfiteátr	k1gInSc4	amfiteátr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
říši	říš	k1gFnSc6	říš
postaven	postavit	k5eAaPmNgInS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
velkých	velký	k2eAgNnPc2d1	velké
děl	dělo	k1gNnPc2	dělo
římské	římský	k2eAgFnSc2d1	římská
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
východně	východně	k6eAd1	východně
od	od	k7c2	od
Fora	forum	k1gNnSc2	forum
Romana	Roman	k1gMnSc2	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
stavba	stavba	k1gFnSc1	stavba
začala	začít	k5eAaPmAgFnS	začít
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
70	[number]	k4	70
a	a	k8xC	a
72	[number]	k4	72
n.	n.	k?	n.
l.	l.	k?	l.
za	za	k7c2	za
římského	římský	k2eAgMnSc2d1	římský
císaře	císař	k1gMnSc2	císař
Vespasiana	Vespasian	k1gMnSc2	Vespasian
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
80	[number]	k4	80
n.	n.	k?	n.
l.	l.	k?	l.
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
Tita	Titus	k1gMnSc4	Titus
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Domitiana	Domitian	k1gMnSc2	Domitian
(	(	kIx(	(
<g/>
81	[number]	k4	81
<g/>
–	–	k?	–
<g/>
96	[number]	k4	96
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
úpravy	úprava	k1gFnPc1	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Amphitheatrum	Amphitheatrum	k1gNnSc1	Amphitheatrum
Flavium	Flavium	k1gNnSc4	Flavium
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
Vespasianova	Vespasianův	k2eAgNnSc2d1	Vespasianovo
a	a	k8xC	a
Titova	Titův	k2eAgNnSc2d1	Titův
rodového	rodový	k2eAgNnSc2d1	rodové
jména	jméno	k1gNnSc2	jméno
Flavius	Flavius	k1gInSc1	Flavius
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
gentilicia	gentilicia	k1gFnSc1	gentilicia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
schopno	schopen	k2eAgNnSc1d1	schopno
pojmout	pojmout	k5eAaPmF	pojmout
50	[number]	k4	50
000	[number]	k4	000
sedících	sedící	k2eAgMnPc2d1	sedící
diváků	divák	k1gMnPc2	divák
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
užíváno	užívat	k5eAaImNgNnS	užívat
ke	k	k7c3	k
gladiátorským	gladiátorský	k2eAgInPc3d1	gladiátorský
zápasům	zápas	k1gInPc3	zápas
a	a	k8xC	a
veřejným	veřejný	k2eAgFnPc3d1	veřejná
podívaným	podívaná	k1gFnPc3	podívaná
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
gladiátorských	gladiátorský	k2eAgFnPc2d1	gladiátorská
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pořádaly	pořádat	k5eAaImAgFnP	pořádat
i	i	k9	i
naumachie	naumachie	k1gFnPc1	naumachie
(	(	kIx(	(
<g/>
stylizované	stylizovaný	k2eAgFnPc1d1	stylizovaná
námořní	námořní	k2eAgFnPc1d1	námořní
bitvy	bitva	k1gFnPc1	bitva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zápasy	zápas	k1gInPc4	zápas
s	s	k7c7	s
divou	divý	k2eAgFnSc7d1	divá
zvěří	zvěř	k1gFnSc7	zvěř
<g/>
,	,	kIx,	,
popravy	poprava	k1gFnPc1	poprava
<g/>
,	,	kIx,	,
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
známých	známý	k2eAgFnPc2d1	známá
bitev	bitva	k1gFnPc2	bitva
a	a	k8xC	a
dramata	drama	k1gNnPc4	drama
založená	založený	k2eAgNnPc4d1	založené
na	na	k7c6	na
klasické	klasický	k2eAgFnSc6d1	klasická
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
přestala	přestat	k5eAaPmAgFnS	přestat
budova	budova	k1gFnSc1	budova
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
zábavě	zábava	k1gFnSc3	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
využívána	využívat	k5eAaPmNgFnS	využívat
k	k	k7c3	k
obývání	obývání	k1gNnSc3	obývání
<g/>
,	,	kIx,	,
sněmování	sněmování	k1gNnSc3	sněmování
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
kamenolom	kamenolom	k1gInSc1	kamenolom
a	a	k8xC	a
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
svatyně	svatyně	k1gFnSc1	svatyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
periodicky	periodicky	k6eAd1	periodicky
se	se	k3xPyFc4	se
opakující	opakující	k2eAgNnPc1d1	opakující
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
a	a	k8xC	a
zloději	zloděj	k1gMnPc1	zloděj
kamenů	kámen	k1gInPc2	kámen
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
učinili	učinit	k5eAaPmAgMnP	učinit
zříceninu	zřícenina	k1gFnSc4	zřícenina
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
stavba	stavba	k1gFnSc1	stavba
stala	stát	k5eAaPmAgFnS	stát
skutečným	skutečný	k2eAgInSc7d1	skutečný
symbolem	symbol	k1gInSc7	symbol
císařského	císařský	k2eAgInSc2d1	císařský
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
turistických	turistický	k2eAgFnPc2d1	turistická
atrakcí	atrakce	k1gFnPc2	atrakce
a	a	k8xC	a
stále	stále	k6eAd1	stále
má	mít	k5eAaImIp3nS	mít
úzké	úzký	k2eAgFnPc4d1	úzká
vazby	vazba	k1gFnPc4	vazba
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
vede	vést	k5eAaImIp3nS	vést
papež	papež	k1gMnSc1	papež
"	"	kIx"	"
<g/>
Křížovou	křížový	k2eAgFnSc4d1	křížová
cestu	cesta	k1gFnSc4	cesta
<g/>
"	"	kIx"	"
do	do	k7c2	do
amfiteátru	amfiteátr	k1gInSc2	amfiteátr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koloseum	Koloseum	k1gNnSc1	Koloseum
je	být	k5eAaImIp3nS	být
také	také	k9	také
znázorněno	znázorněn	k2eAgNnSc1d1	znázorněno
na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
verzi	verze	k1gFnSc6	verze
pěticentové	pěticentový	k2eAgFnSc2d1	pěticentový
mince	mince	k1gFnSc2	mince
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jméno	jméno	k1gNnSc1	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
původní	původní	k2eAgNnSc4d1	původní
latinské	latinský	k2eAgNnSc4d1	latinské
jméno	jméno	k1gNnSc4	jméno
bylo	být	k5eAaImAgNnS	být
Amphitheatrum	Amphitheatrum	k1gNnSc1	Amphitheatrum
Flavium	Flavium	k1gNnSc1	Flavium
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
počešťováno	počešťovat	k5eAaImNgNnS	počešťovat
jako	jako	k8xS	jako
Flaviovský	Flaviovský	k2eAgInSc1d1	Flaviovský
amfiteátr	amfiteátr	k1gInSc1	amfiteátr
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
císařů	císař	k1gMnPc2	císař
flaviovské	flaviovský	k2eAgFnSc2d1	flaviovský
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gMnSc4	on
postavili	postavit	k5eAaPmAgMnP	postavit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
často	často	k6eAd1	často
užíváno	užívat	k5eAaImNgNnS	užívat
ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
všeobecně	všeobecně	k6eAd1	všeobecně
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
Římané	Říman	k1gMnPc1	Říman
Koloseu	Koloseum	k1gNnSc6	Koloseum
neoficiálně	neoficiálně	k6eAd1	neoficiálně
říkali	říkat	k5eAaImAgMnP	říkat
Amphitheatrum	Amphitheatrum	k1gNnSc4	Amphitheatrum
Caesareum	Caesareum	k1gNnSc1	Caesareum
(	(	kIx(	(
<g/>
Císařský	císařský	k2eAgInSc1d1	císařský
amfiteátr	amfiteátr	k1gInSc1	amfiteátr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
by	by	kYmCp3nS	by
však	však	k9	však
mohlo	moct	k5eAaImAgNnS	moct
nést	nést	k5eAaImF	nést
více	hodně	k6eAd2	hodně
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
název	název	k1gInSc4	název
Amphitheatrum	Amphitheatrum	k1gNnSc4	Amphitheatrum
Flavium	Flavium	k1gNnSc1	Flavium
rovněž	rovněž	k9	rovněž
–	–	k?	–
Vespasianus	Vespasianus	k1gMnSc1	Vespasianus
a	a	k8xC	a
Titus	Titus	k1gMnSc1	Titus
<g/>
,	,	kIx,	,
stavitelé	stavitel	k1gMnPc1	stavitel
Kolosea	Koloseum	k1gNnSc2	Koloseum
<g/>
,	,	kIx,	,
postavili	postavit	k5eAaPmAgMnP	postavit
také	také	k9	také
amfiteátr	amfiteátr	k1gInSc4	amfiteátr
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
Puteoli	Puteole	k1gFnSc6	Puteole
(	(	kIx(	(
<g/>
moderní	moderní	k2eAgFnSc4d1	moderní
Pozzuoli	Pozzuole	k1gFnSc4	Pozzuole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historikové	historik	k1gMnPc1	historik
usuzují	usuzovat	k5eAaImIp3nP	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jméno	jméno	k1gNnSc1	jméno
Koloseum	Koloseum	k1gNnSc4	Koloseum
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
kolosální	kolosální	k2eAgFnSc2d1	kolosální
Neronovy	Neronův	k2eAgFnSc2d1	Neronova
sochy	socha	k1gFnSc2	socha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
socha	socha	k1gFnSc1	socha
Neronovými	Neronův	k2eAgFnPc7d1	Neronova
nástupci	nástupce	k1gMnSc3	nástupce
upravena	upravit	k5eAaPmNgFnS	upravit
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
boha	bůh	k1gMnSc2	bůh
Helia	helium	k1gNnSc2	helium
(	(	kIx(	(
<g/>
Sol	sol	k1gNnSc2	sol
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Apollóna	Apollón	k1gMnSc2	Apollón
<g/>
,	,	kIx,	,
boha	bůh	k1gMnSc2	bůh
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
přidáním	přidání	k1gNnSc7	přidání
nezbytné	zbytný	k2eNgFnSc2d1	zbytný
sluneční	sluneční	k2eAgFnSc2d1	sluneční
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
byla	být	k5eAaImAgFnS	být
původní	původní	k2eAgFnSc1d1	původní
hlava	hlava	k1gFnSc1	hlava
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
hlavami	hlava	k1gFnPc7	hlava
dalších	další	k2eAgMnPc2d1	další
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
rozličné	rozličný	k2eAgFnPc4d1	rozličná
katastrofy	katastrofa	k1gFnPc4	katastrofa
zůstala	zůstat	k5eAaPmAgFnS	zůstat
socha	socha	k1gFnSc1	socha
stát	stát	k5eAaPmF	stát
až	až	k9	až
do	do	k7c2	do
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
jí	on	k3xPp3gFnSc3	on
přisuzována	přisuzován	k2eAgFnSc1d1	přisuzována
zázračná	zázračný	k2eAgFnSc1d1	zázračná
moc	moc	k1gFnSc1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
věčnosti	věčnost	k1gFnSc2	věčnost
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
napsal	napsat	k5eAaBmAgMnS	napsat
Beda	Beda	k1gMnSc1	Beda
Ctihodný	ctihodný	k2eAgMnSc1d1	ctihodný
(	(	kIx(	(
<g/>
asi	asi	k9	asi
672	[number]	k4	672
<g/>
–	–	k?	–
<g/>
735	[number]	k4	735
<g/>
)	)	kIx)	)
známý	známý	k2eAgInSc1d1	známý
epigram	epigram	k1gInSc1	epigram
oslavující	oslavující	k2eAgInSc1d1	oslavující
symbolický	symbolický	k2eAgInSc4d1	symbolický
význam	význam	k1gInSc4	význam
sochy	socha	k1gFnSc2	socha
<g/>
:	:	kIx,	:
Quandiu	Quandium	k1gNnSc3	Quandium
stabit	stabit	k2eAgInSc4d1	stabit
coliseus	coliseus	k1gInSc4	coliseus
<g/>
,	,	kIx,	,
stabit	stabit	k2eAgInSc4d1	stabit
et	et	k?	et
Roma	Rom	k1gMnSc2	Rom
<g/>
;	;	kIx,	;
quando	quando	k6eAd1	quando
cadit	cadit	k2eAgInSc1d1	cadit
coliseus	coliseus	k1gInSc1	coliseus
<g/>
,	,	kIx,	,
cadet	cadet	k1gInSc1	cadet
et	et	k?	et
Roma	Rom	k1gMnSc2	Rom
<g/>
;	;	kIx,	;
quando	quando	k6eAd1	quando
cadet	cadet	k5eAaBmF	cadet
Roma	Rom	k1gMnSc4	Rom
<g/>
,	,	kIx,	,
cadet	cadet	k1gMnSc1	cadet
et	et	k?	et
mundus	mundus	k1gMnSc1	mundus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dokud	dokud	k6eAd1	dokud
stojí	stát	k5eAaImIp3nS	stát
Kolos	kolos	k1gInSc1	kolos
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
Řím	Řím	k1gInSc1	Řím
<g/>
;	;	kIx,	;
padne	padnout	k5eAaPmIp3nS	padnout
<g/>
-li	i	k?	-li
Kolos	kolos	k1gInSc1	kolos
<g/>
,	,	kIx,	,
padne	padnout	k5eAaImIp3nS	padnout
i	i	k9	i
Řím	Řím	k1gInSc1	Řím
<g/>
;	;	kIx,	;
padne	padnout	k5eAaImIp3nS	padnout
<g/>
-li	i	k?	-li
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
padne	padnout	k5eAaPmIp3nS	padnout
svět	svět	k1gInSc1	svět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
mylně	mylně	k6eAd1	mylně
chápáno	chápat	k5eAaImNgNnS	chápat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
často	často	k6eAd1	často
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
Koloseu	Koloseum	k1gNnSc6	Koloseum
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
o	o	k7c6	o
Kolosu	kolos	k1gInSc6	kolos
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
např.	např.	kA	např.
v	v	k7c6	v
Byronově	Byronův	k2eAgFnSc6d1	Byronova
poemu	poema	k1gFnSc4	poema
Childe	Child	k1gInSc5	Child
Haroldova	Haroldův	k2eAgFnSc1d1	Haroldova
pouť	pouť	k1gFnSc1	pouť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
psal	psát	k5eAaImAgMnS	psát
Beda	Beda	k1gMnSc1	Beda
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
termín	termín	k1gInSc1	termín
coliseus	coliseus	k1gInSc1	coliseus
užíván	užíván	k2eAgInSc1d1	užíván
spíše	spíše	k9	spíše
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
než	než	k8xS	než
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k8xC	jako
Flaviovský	Flaviovský	k2eAgInSc1d1	Flaviovský
amfiteátr	amfiteátr	k1gInSc1	amfiteátr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolos	kolos	k1gInSc1	kolos
nakonec	nakonec	k6eAd1	nakonec
padl	padnout	k5eAaPmAgInS	padnout
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
byl	být	k5eAaImAgInS	být
stržen	strhnout	k5eAaPmNgInS	strhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
druhotně	druhotně	k6eAd1	druhotně
využit	využít	k5eAaPmNgInS	využít
jeho	on	k3xPp3gInSc4	on
bronz	bronz	k1gInSc4	bronz
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
používáno	používán	k2eAgNnSc1d1	používáno
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
Koloseum	Koloseum	k1gNnSc1	Koloseum
<g/>
"	"	kIx"	"
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
amfiteátrem	amfiteátr	k1gInSc7	amfiteátr
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
samotná	samotný	k2eAgFnSc1d1	samotná
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
zapomenuta	zapomnět	k5eAaImNgFnS	zapomnět
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
jenom	jenom	k9	jenom
jeho	jeho	k3xOp3gInSc1	jeho
podstavec	podstavec	k1gInSc1	podstavec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
mezi	mezi	k7c7	mezi
Koloseem	Koloseum	k1gNnSc7	Koloseum
a	a	k8xC	a
vedle	vedle	k6eAd1	vedle
stojícím	stojící	k2eAgInSc7d1	stojící
Chrámem	chrám	k1gInSc7	chrám
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
středověku	středověk	k1gInSc2	středověk
bylo	být	k5eAaImAgNnS	být
jméno	jméno	k1gNnSc1	jméno
zkomoleno	zkomolit	k5eAaPmNgNnS	zkomolit
na	na	k7c4	na
Koliseum	Koliseum	k1gInSc4	Koliseum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
je	být	k5eAaImIp3nS	být
amfiteátr	amfiteátr	k1gInSc1	amfiteátr
stále	stále	k6eAd1	stále
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xC	jako
il	il	k?	il
Colosseo	Colosseo	k1gNnSc1	Colosseo
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc1d1	ostatní
románské	románský	k2eAgInPc1d1	románský
jazyky	jazyk	k1gInPc1	jazyk
však	však	k9	však
užívají	užívat	k5eAaImIp3nP	užívat
tvary	tvar	k1gInPc1	tvar
le	le	k?	le
Colisée	Colisé	k1gInSc2	Colisé
(	(	kIx(	(
<g/>
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
el	ela	k1gFnPc2	ela
Coliseo	Coliseo	k6eAd1	Coliseo
(	(	kIx(	(
<g/>
španělština	španělština	k1gFnSc1	španělština
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c6	o
Coliseu	Coliseus	k1gInSc6	Coliseus
(	(	kIx(	(
<g/>
portugalština	portugalština	k1gFnSc1	portugalština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Starověk	starověk	k1gInSc4	starověk
===	===	k?	===
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
výstavba	výstavba	k1gFnSc1	výstavba
začala	začít	k5eAaPmAgFnS	začít
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Vespasiana	Vespasian	k1gMnSc2	Vespasian
v	v	k7c6	v
letech	let	k1gInPc6	let
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
72	[number]	k4	72
<g/>
.	.	kIx.	.
</s>
<s>
Vybrána	vybrán	k2eAgFnSc1d1	vybrána
byla	být	k5eAaImAgFnS	být
rovinatá	rovinatý	k2eAgFnSc1d1	rovinatá
plocha	plocha	k1gFnSc1	plocha
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
nízkého	nízký	k2eAgNnSc2d1	nízké
údolí	údolí	k1gNnSc2	údolí
mezi	mezi	k7c7	mezi
kopci	kopec	k1gInSc3	kopec
Caelius	Caelius	k1gInSc1	Caelius
<g/>
,	,	kIx,	,
Esquilinus	Esquilinus	k1gInSc1	Esquilinus
a	a	k8xC	a
Palatinus	Palatinus	k1gInSc1	Palatinus
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
vedl	vést	k5eAaImAgMnS	vést
kanál	kanál	k1gInSc4	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
hustě	hustě	k6eAd1	hustě
obydleno	obydlet	k5eAaPmNgNnS	obydlet
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
velkým	velký	k2eAgInSc7d1	velký
požárem	požár	k1gInSc7	požár
Říma	Řím	k1gInSc2	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
64	[number]	k4	64
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
následkem	následkem	k7c2	následkem
čehož	což	k3yRnSc2	což
Nero	Nero	k1gMnSc1	Nero
zabral	zabrat	k5eAaPmAgInS	zabrat
většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
a	a	k8xC	a
zařadil	zařadit	k5eAaPmAgInS	zařadit
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
osobního	osobní	k2eAgInSc2d1	osobní
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
straně	strana	k1gFnSc6	strana
Kolosea	Koloseum	k1gNnSc2	Koloseum
postavil	postavit	k5eAaPmAgMnS	postavit
grandiózní	grandiózní	k2eAgMnSc1d1	grandiózní
Domus	Domus	k1gMnSc1	Domus
Aurea	Aurea	k1gMnSc1	Aurea
<g/>
,	,	kIx,	,
před	před	k7c7	před
kterým	který	k3yIgNnSc7	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
umělé	umělý	k2eAgNnSc4d1	umělé
jezero	jezero	k1gNnSc4	jezero
obklopené	obklopený	k2eAgInPc4d1	obklopený
pavilony	pavilon	k1gInPc4	pavilon
a	a	k8xC	a
zahradami	zahrada	k1gFnPc7	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Stávající	stávající	k2eAgInSc1d1	stávající
akvadukt	akvadukt	k1gInSc1	akvadukt
Aqua	Aquum	k1gNnSc2	Aquum
Claudia	Claudia	k1gFnSc1	Claudia
byl	být	k5eAaImAgInS	být
rozšířen	rozšířen	k2eAgMnSc1d1	rozšířen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
zásobovat	zásobovat	k5eAaImF	zásobovat
prostor	prostor	k1gInSc4	prostor
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
vedle	vedle	k7c2	vedle
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
Domu	dům	k1gInSc2	dům
Aurea	Aure	k1gInSc2	Aure
byla	být	k5eAaImAgFnS	být
zasazena	zasazen	k2eAgFnSc1d1	zasazena
gigantická	gigantický	k2eAgFnSc1d1	gigantická
bronzová	bronzový	k2eAgFnSc1d1	bronzová
socha	socha	k1gFnSc1	socha
Nerona	Nero	k1gMnSc2	Nero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aréně	aréna	k1gFnSc6	aréna
bojovali	bojovat	k5eAaImAgMnP	bojovat
gladiátoři	gladiátor	k1gMnPc1	gladiátor
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
proto	proto	k8xC	proto
nazýváno	nazývat	k5eAaImNgNnS	nazývat
"	"	kIx"	"
<g/>
římskou	římský	k2eAgFnSc7d1	římská
arénou	aréna	k1gFnSc7	aréna
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
Domu	dům	k1gInSc2	dům
Aurea	Aurea	k1gFnSc1	Aurea
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
,	,	kIx,	,
socha	socha	k1gFnSc1	socha
však	však	k9	však
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
bylo	být	k5eAaImAgNnS	být
vypuštěno	vypustit	k5eAaPmNgNnS	vypustit
a	a	k8xC	a
plocha	plocha	k1gFnSc1	plocha
použita	použít	k5eAaPmNgFnS	použít
jako	jako	k8xC	jako
místo	místo	k7c2	místo
pro	pro	k7c4	pro
nový	nový	k2eAgInSc4d1	nový
Flaviovský	Flaviovský	k2eAgInSc4d1	Flaviovský
amfiteátr	amfiteátr	k1gInSc4	amfiteátr
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
bývalých	bývalý	k2eAgInPc2d1	bývalý
základů	základ	k1gInPc2	základ
Domu	dům	k1gInSc2	dům
Aurea	Aure	k1gInSc2	Aure
vyrostly	vyrůst	k5eAaPmAgFnP	vyrůst
gladiátorské	gladiátorský	k2eAgFnPc1d1	gladiátorská
školy	škola	k1gFnPc1	škola
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
podpůrné	podpůrný	k2eAgFnPc1d1	podpůrná
budovy	budova	k1gFnPc1	budova
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rekonstruovaného	rekonstruovaný	k2eAgInSc2d1	rekonstruovaný
nápisu	nápis	k1gInSc2	nápis
nalezeného	nalezený	k2eAgInSc2d1	nalezený
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
"	"	kIx"	"
<g/>
nařídil	nařídit	k5eAaPmAgMnS	nařídit
císař	císař	k1gMnSc1	císař
Vespasianus	Vespasianus	k1gMnSc1	Vespasianus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
amfiteátr	amfiteátr	k1gInSc1	amfiteátr
vztyčen	vztyčen	k2eAgInSc1d1	vztyčen
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
podílu	podíl	k1gInSc2	podíl
na	na	k7c6	na
válečné	válečný	k2eAgFnSc6d1	válečná
kořisti	kořist	k1gFnSc6	kořist
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
myslí	myslet	k5eAaImIp3nS	myslet
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
pokladu	poklad	k1gInSc2	poklad
získaného	získaný	k2eAgInSc2d1	získaný
roku	rok	k1gInSc2	rok
70	[number]	k4	70
Římany	Říman	k1gMnPc4	Říman
díky	díky	k7c3	díky
vítězství	vítězství	k1gNnSc3	vítězství
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Koloseum	Koloseum	k1gNnSc1	Koloseum
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
velký	velký	k2eAgInSc4d1	velký
triumfální	triumfální	k2eAgInSc4d1	triumfální
památník	památník	k1gInSc4	památník
<g/>
,	,	kIx,	,
odrážející	odrážející	k2eAgFnSc4d1	odrážející
římskou	římský	k2eAgFnSc4d1	římská
tradici	tradice	k1gFnSc4	tradice
oslav	oslava	k1gFnPc2	oslava
velkých	velký	k2eAgFnPc2d1	velká
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Vespasianovo	Vespasianův	k2eAgNnSc1d1	Vespasianovo
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
postavit	postavit	k5eAaPmF	postavit
Koloseum	Koloseum	k1gNnSc4	Koloseum
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Neronova	Neronův	k2eAgNnSc2d1	Neronovo
jezera	jezero	k1gNnSc2	jezero
lze	lze	k6eAd1	lze
také	také	k9	také
chápat	chápat	k5eAaImF	chápat
jako	jako	k8xC	jako
populistické	populistický	k2eAgNnSc4d1	populistické
gesto	gesto	k1gNnSc4	gesto
navrátit	navrátit	k5eAaPmF	navrátit
lidem	člověk	k1gMnPc3	člověk
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
Nero	Nero	k1gMnSc1	Nero
využíval	využívat	k5eAaPmAgMnS	využívat
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
amfiteátrů	amfiteátr	k1gInPc2	amfiteátr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgInP	nacházet
na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Koloseum	Koloseum	k1gNnSc1	Koloseum
postaveno	postavit	k5eAaPmNgNnS	postavit
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
–	–	k?	–
doslova	doslova	k6eAd1	doslova
i	i	k9	i
symbolicky	symbolicky	k6eAd1	symbolicky
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Vespasianovy	Vespasianův	k2eAgFnSc2d1	Vespasianova
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
79	[number]	k4	79
bylo	být	k5eAaImAgNnS	být
Koloseum	Koloseum	k1gNnSc1	Koloseum
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
až	až	k6eAd1	až
po	po	k7c4	po
třetí	třetí	k4xOgNnSc4	třetí
patro	patro	k1gNnSc4	patro
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
úroveň	úroveň	k1gFnSc1	úroveň
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
a	a	k8xC	a
budova	budova	k1gFnSc1	budova
inaugurována	inaugurovat	k5eAaBmNgFnS	inaugurovat
jeho	jeho	k3xOp3gMnSc7	jeho
synem	syn	k1gMnSc7	syn
Titem	Tit	k1gMnSc7	Tit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
80	[number]	k4	80
<g/>
.	.	kIx.	.
</s>
<s>
Cassius	Cassius	k1gMnSc1	Cassius
Dio	Dio	k1gMnSc1	Dio
počítal	počítat	k5eAaImAgMnS	počítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
inauguračních	inaugurační	k2eAgFnPc2d1	inaugurační
her	hra	k1gFnPc2	hra
amfiteátru	amfiteátr	k1gInSc2	amfiteátr
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
9	[number]	k4	9
000	[number]	k4	000
divokých	divoký	k2eAgNnPc2d1	divoké
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Vespasiánova	Vespasiánův	k2eAgMnSc4d1	Vespasiánův
mladšího	mladý	k2eAgMnSc4d2	mladší
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
dosazeného	dosazený	k2eAgMnSc2d1	dosazený
císaře	císař	k1gMnSc2	císař
Domitiana	Domitian	k1gMnSc2	Domitian
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
dále	daleko	k6eAd2	daleko
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
<g/>
.	.	kIx.	.
</s>
<s>
Domitianus	Domitianus	k1gMnSc1	Domitianus
postavil	postavit	k5eAaPmAgMnS	postavit
hypogeum	hypogeum	k1gInSc4	hypogeum
<g/>
,	,	kIx,	,
sérii	série	k1gFnSc4	série
podzemních	podzemní	k2eAgInPc2d1	podzemní
tunelů	tunel	k1gInPc2	tunel
užívaných	užívaný	k2eAgInPc2d1	užívaný
k	k	k7c3	k
ubytování	ubytování	k1gNnSc3	ubytování
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Kolosea	Koloseum	k1gNnSc2	Koloseum
umístit	umístit	k5eAaPmF	umístit
galerii	galerie	k1gFnSc4	galerie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
kapacitu	kapacita	k1gFnSc4	kapacita
míst	místo	k1gNnPc2	místo
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
217	[number]	k4	217
bylo	být	k5eAaImAgNnS	být
Koloseum	Koloseum	k1gNnSc1	Koloseum
zle	zle	k6eAd1	zle
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
velkým	velký	k2eAgInSc7d1	velký
požárem	požár	k1gInSc7	požár
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Dio	Dio	k1gFnSc2	Dio
Cassia	Cassium	k1gNnSc2	Cassium
způsobeného	způsobený	k2eAgInSc2d1	způsobený
osvětlením	osvětlení	k1gNnPc3	osvětlení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zničil	zničit	k5eAaPmAgInS	zničit
dřevěná	dřevěný	k2eAgNnPc4d1	dřevěné
horní	horní	k2eAgNnPc4d1	horní
patra	patro	k1gNnPc4	patro
vnitřku	vnitřek	k1gInSc2	vnitřek
amfiteátru	amfiteátr	k1gInSc2	amfiteátr
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
240	[number]	k4	240
nebylo	být	k5eNaImAgNnS	být
Koloseum	Koloseum	k1gNnSc1	Koloseum
opraveno	opravit	k5eAaPmNgNnS	opravit
úplně	úplně	k6eAd1	úplně
a	a	k8xC	a
prošlo	projít	k5eAaPmAgNnS	projít
dalšími	další	k2eAgFnPc7d1	další
úpravami	úprava	k1gFnPc7	úprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
250	[number]	k4	250
nebo	nebo	k8xC	nebo
252	[number]	k4	252
a	a	k8xC	a
opět	opět	k6eAd1	opět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
320	[number]	k4	320
<g/>
.	.	kIx.	.
</s>
<s>
Zápisy	zápis	k1gInPc1	zápis
popisují	popisovat	k5eAaImIp3nP	popisovat
restaurace	restaurace	k1gFnPc4	restaurace
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
Kolosea	Koloseum	k1gNnSc2	Koloseum
za	za	k7c2	za
Theodosia	Theodosium	k1gNnSc2	Theodosium
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Valentiniana	Valentiniana	k1gFnSc1	Valentiniana
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
vládl	vládnout	k5eAaImAgMnS	vládnout
425	[number]	k4	425
<g/>
–	–	k?	–
<g/>
450	[number]	k4	450
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
opravilo	opravit	k5eAaPmAgNnS	opravit
poškození	poškození	k1gNnSc1	poškození
způsobené	způsobený	k2eAgNnSc1d1	způsobené
hlavním	hlavní	k2eAgNnSc7d1	hlavní
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
443	[number]	k4	443
<g/>
;	;	kIx,	;
více	hodně	k6eAd2	hodně
prací	práce	k1gFnSc7	práce
následovalo	následovat	k5eAaImAgNnS	následovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
484	[number]	k4	484
a	a	k8xC	a
508	[number]	k4	508
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
aréna	aréna	k1gFnSc1	aréna
dále	daleko	k6eAd2	daleko
užívána	užívat	k5eAaImNgFnS	užívat
pro	pro	k7c4	pro
podívané	podívaná	k1gFnPc4	podívaná
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInPc1d1	poslední
gladiátorské	gladiátorský	k2eAgInPc1d1	gladiátorský
boje	boj	k1gInPc1	boj
jsou	být	k5eAaImIp3nP	být
zmiňovány	zmiňovat	k5eAaImNgInP	zmiňovat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
435	[number]	k4	435
<g/>
.	.	kIx.	.
</s>
<s>
Lov	lov	k1gInSc1	lov
zvířat	zvíře	k1gNnPc2	zvíře
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
nejméně	málo	k6eAd3	málo
do	do	k7c2	do
roku	rok	k1gInSc2	rok
523	[number]	k4	523
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
prošlo	projít	k5eAaPmAgNnS	projít
Koloseum	Koloseum	k1gNnSc1	Koloseum
několika	několik	k4yIc7	několik
radikálními	radikální	k2eAgFnPc7d1	radikální
změnami	změna	k1gFnPc7	změna
týkajícími	týkající	k2eAgFnPc7d1	týkající
se	se	k3xPyFc4	se
jeho	on	k3xPp3gNnSc2	on
používání	používání	k1gNnSc2	používání
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
stavby	stavba	k1gFnSc2	stavba
amfiteátru	amfiteátr	k1gInSc2	amfiteátr
postaven	postavit	k5eAaPmNgInS	postavit
malý	malý	k2eAgInSc1d1	malý
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
to	ten	k3xDgNnSc1	ten
nedávalo	dávat	k5eNaImAgNnS	dávat
budovu	budova	k1gFnSc4	budova
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
žádný	žádný	k3yNgInSc4	žádný
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
náboženský	náboženský	k2eAgInSc4d1	náboženský
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Aréna	aréna	k1gFnSc1	aréna
byla	být	k5eAaImAgFnS	být
přeměněna	přeměnit	k5eAaPmNgFnS	přeměnit
na	na	k7c4	na
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Četné	četný	k2eAgFnPc1d1	četná
klenbové	klenbový	k2eAgFnPc1d1	klenbová
prostory	prostora	k1gFnPc1	prostora
v	v	k7c6	v
arkádách	arkáda	k1gFnPc6	arkáda
pod	pod	k7c7	pod
sedadly	sedadlo	k1gNnPc7	sedadlo
byly	být	k5eAaImAgFnP	být
přeměněny	přeměnit	k5eAaPmNgInP	přeměnit
v	v	k7c4	v
ubytovny	ubytovna	k1gFnPc4	ubytovna
a	a	k8xC	a
dílny	dílna	k1gFnPc4	dílna
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
ze	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
o	o	k7c6	o
nich	on	k3xPp3gInPc6	on
dochovány	dochován	k2eAgInPc1d1	dochován
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgInP	být
pronajímány	pronajímat	k5eAaImNgInP	pronajímat
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1200	[number]	k4	1200
převzala	převzít	k5eAaPmAgFnS	převzít
Koloseum	Koloseum	k1gNnSc4	Koloseum
rodina	rodina	k1gFnSc1	rodina
Frangipani	Frangipaň	k1gFnSc3	Frangipaň
a	a	k8xC	a
obehnala	obehnat	k5eAaPmAgFnS	obehnat
ho	on	k3xPp3gMnSc4	on
opevněním	opevnění	k1gNnSc7	opevnění
<g/>
;	;	kIx,	;
očividně	očividně	k6eAd1	očividně
ho	on	k3xPp3gNnSc4	on
využívala	využívat	k5eAaPmAgFnS	využívat
jako	jako	k9	jako
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1349	[number]	k4	1349
zde	zde	k6eAd1	zde
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
jeho	jeho	k3xOp3gNnSc4	jeho
vážné	vážný	k2eAgNnSc4d1	vážné
poškození	poškození	k1gNnSc4	poškození
a	a	k8xC	a
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnější	vnější	k2eAgFnSc1d1	vnější
strana	strana	k1gFnSc1	strana
budovy	budova	k1gFnSc2	budova
se	se	k3xPyFc4	se
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
popadaných	popadaný	k2eAgInPc2d1	popadaný
kamenů	kámen	k1gInPc2	kámen
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
k	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
paláců	palác	k1gInPc2	palác
<g/>
,	,	kIx,	,
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
nemocnic	nemocnice	k1gFnPc2	nemocnice
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
budov	budova	k1gFnPc2	budova
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
severní	severní	k2eAgFnSc2d1	severní
třetiny	třetina	k1gFnSc2	třetina
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
náboženský	náboženský	k2eAgInSc1d1	náboženský
řád	řád	k1gInSc1	řád
a	a	k8xC	a
obýval	obývat	k5eAaImAgInS	obývat
ji	on	k3xPp3gFnSc4	on
až	až	k6eAd1	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
amfiteátru	amfiteátr	k1gInSc2	amfiteátr
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
míře	míra	k1gFnSc6	míra
vydlážděn	vydlážděn	k2eAgInSc4d1	vydlážděn
kameny	kámen	k1gInPc4	kámen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
užit	užít	k5eAaPmNgInS	užít
odněkud	odněkud	k6eAd1	odněkud
jinud	jinud	k6eAd1	jinud
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
mramorové	mramorový	k2eAgFnPc1d1	mramorová
fasády	fasáda	k1gFnPc1	fasáda
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
pálen	pálit	k5eAaImNgInS	pálit
na	na	k7c4	na
nehašené	hašený	k2eNgNnSc4d1	nehašené
vápno	vápno	k1gNnSc4	vápno
<g/>
.	.	kIx.	.
</s>
<s>
Bronzové	bronzový	k2eAgFnPc1d1	bronzová
výztuže	výztuž	k1gFnPc1	výztuž
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
držely	držet	k5eAaImAgFnP	držet
kamennou	kamenný	k2eAgFnSc4d1	kamenná
stavbu	stavba	k1gFnSc4	stavba
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vypáčeny	vypáčen	k2eAgFnPc1d1	vypáčena
nebo	nebo	k8xC	nebo
vyháknuty	vyháknut	k2eAgFnPc1d1	vyháknut
ze	z	k7c2	z
stěn	stěna	k1gFnPc2	stěna
a	a	k8xC	a
zanechaly	zanechat	k5eAaPmAgInP	zanechat
četné	četný	k2eAgInPc1d1	četný
důlky	důlek	k1gInPc1	důlek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
budovu	budova	k1gFnSc4	budova
hyzdí	hyzdit	k5eAaImIp3nS	hyzdit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moderní	moderní	k2eAgFnSc1d1	moderní
doba	doba	k1gFnSc1	doba
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
shledali	shledat	k5eAaPmAgMnP	shledat
představitelé	představitel	k1gMnPc1	představitel
církve	církev	k1gFnSc2	církev
užití	užití	k1gNnSc1	užití
pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
zpustlou	zpustlý	k2eAgFnSc4d1	zpustlá
masu	masa	k1gFnSc4	masa
Kolosea	Koloseum	k1gNnSc2	Koloseum
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Sixtus	Sixtus	k1gMnSc1	Sixtus
V.	V.	kA	V.
(	(	kIx(	(
<g/>
1585	[number]	k4	1585
<g/>
–	–	k?	–
<g/>
1590	[number]	k4	1590
<g/>
)	)	kIx)	)
plánoval	plánovat	k5eAaImAgMnS	plánovat
předělat	předělat	k5eAaPmF	předělat
budovu	budova	k1gFnSc4	budova
na	na	k7c4	na
továrnu	továrna	k1gFnSc4	továrna
na	na	k7c4	na
vlnu	vlna	k1gFnSc4	vlna
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nS	by
zajistil	zajistit	k5eAaPmAgMnS	zajistit
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
římským	římský	k2eAgFnPc3d1	římská
prostitutkám	prostitutka	k1gFnPc3	prostitutka
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
ale	ale	k9	ale
padl	padnout	k5eAaPmAgInS	padnout
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
předčasné	předčasný	k2eAgFnSc3d1	předčasná
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1671	[number]	k4	1671
schválil	schválit	k5eAaPmAgMnS	schválit
kardinál	kardinál	k1gMnSc1	kardinál
Altieri	Altieri	k1gNnSc2	Altieri
využití	využití	k1gNnSc2	využití
Kolosea	Koloseum	k1gNnSc2	Koloseum
k	k	k7c3	k
býčím	býčí	k2eAgInPc3d1	býčí
zápasům	zápas	k1gInPc3	zápas
<g/>
;	;	kIx,	;
veřejný	veřejný	k2eAgInSc4d1	veřejný
poprask	poprask	k1gInSc4	poprask
ale	ale	k8xC	ale
způsobil	způsobit	k5eAaPmAgMnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc2	tento
myšlenky	myšlenka	k1gFnSc2	myšlenka
rychle	rychle	k6eAd1	rychle
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1749	[number]	k4	1749
kvitoval	kvitovat	k5eAaBmAgMnS	kvitovat
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
jako	jako	k8xS	jako
oficiální	oficiální	k2eAgFnSc4d1	oficiální
politiku	politika	k1gFnSc4	politika
církve	církev	k1gFnSc2	církev
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Koloseum	Koloseum	k1gNnSc1	Koloseum
je	být	k5eAaImIp3nS	být
svaté	svatý	k2eAgNnSc4d1	svaté
místo	místo	k1gNnSc4	místo
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
mučeni	mučen	k2eAgMnPc1d1	mučen
raní	raný	k2eAgMnPc1d1	raný
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Zakázal	zakázat	k5eAaPmAgMnS	zakázat
užívání	užívání	k1gNnSc4	užívání
Kolosea	Koloseum	k1gNnSc2	Koloseum
jako	jako	k8xC	jako
kamenolomu	kamenolom	k1gInSc2	kamenolom
a	a	k8xC	a
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
budovu	budova	k1gFnSc4	budova
Kristovým	Kristův	k2eAgFnPc3d1	Kristova
pašijím	pašije	k1gFnPc3	pašije
a	a	k8xC	a
umístil	umístit	k5eAaPmAgMnS	umístit
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
kříže	kříž	k1gInPc4	kříž
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
posvěcené	posvěcený	k2eAgNnSc4d1	posvěcené
krví	krvit	k5eAaImIp3nS	krvit
Kristových	Kristův	k2eAgMnPc2d1	Kristův
mučedníků	mučedník	k1gMnPc2	mučedník
<g/>
,	,	kIx,	,
již	jenž	k3xRgMnPc1	jenž
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
v	v	k7c6	v
Koloseu	Koloseum	k1gNnSc6	Koloseum
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
iniciovali	iniciovat	k5eAaBmAgMnP	iniciovat
papežové	papež	k1gMnPc1	papež
různé	různý	k2eAgFnSc2d1	různá
stabilizační	stabilizační	k2eAgFnSc2d1	stabilizační
a	a	k8xC	a
restaurační	restaurační	k2eAgInPc4d1	restaurační
projekty	projekt	k1gInPc4	projekt
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
vegetace	vegetace	k1gFnSc1	vegetace
přerůstající	přerůstající	k2eAgFnSc1d1	přerůstající
přes	přes	k7c4	přes
budovu	budova	k1gFnSc4	budova
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
ji	on	k3xPp3gFnSc4	on
ohrožující	ohrožující	k2eAgFnSc4d1	ohrožující
<g/>
.	.	kIx.	.
</s>
<s>
Fasáda	fasáda	k1gFnSc1	fasáda
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1821	[number]	k4	1821
a	a	k8xC	a
1827	[number]	k4	1827
zpevněna	zpevnit	k5eAaPmNgFnS	zpevnit
trojúhelníkovými	trojúhelníkový	k2eAgInPc7d1	trojúhelníkový
cihlovými	cihlový	k2eAgInPc7d1	cihlový
klíny	klín	k1gInPc7	klín
a	a	k8xC	a
interiér	interiér	k1gInSc1	interiér
byl	být	k5eAaImAgInS	být
opraven	opravit	k5eAaPmNgInS	opravit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
1846	[number]	k4	1846
a	a	k8xC	a
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
část	část	k1gFnSc1	část
arény	aréna	k1gFnSc2	aréna
byla	být	k5eAaImAgFnS	být
částečně	částečně	k6eAd1	částečně
vyhloubena	vyhloubit	k5eAaPmNgFnS	vyhloubit
v	v	k7c6	v
letech	let	k1gInPc6	let
1810	[number]	k4	1810
<g/>
–	–	k?	–
<g/>
1814	[number]	k4	1814
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
a	a	k8xC	a
za	za	k7c4	za
Mussoliniho	Mussolini	k1gMnSc4	Mussolini
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
odkryta	odkryt	k2eAgFnSc1d1	odkryta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
turistických	turistický	k2eAgFnPc2d1	turistická
atrakcí	atrakce	k1gFnPc2	atrakce
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
sem	sem	k6eAd1	sem
zavítají	zavítat	k5eAaPmIp3nP	zavítat
miliony	milion	k4xCgInPc1	milion
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Následky	následek	k1gInPc1	následek
znečištění	znečištění	k1gNnSc2	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
a	a	k8xC	a
celkové	celkový	k2eAgNnSc4d1	celkové
zhoršení	zhoršení	k1gNnSc4	zhoršení
stavu	stav	k1gInSc2	stav
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
vyzvaly	vyzvat	k5eAaPmAgFnP	vyzvat
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
restauračnímu	restaurační	k2eAgInSc3d1	restaurační
programu	program	k1gInSc3	program
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
akce	akce	k1gFnSc2	akce
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1993	[number]	k4	1993
a	a	k8xC	a
2000	[number]	k4	2000
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
40	[number]	k4	40
miliard	miliarda	k4xCgFnPc2	miliarda
italských	italský	k2eAgFnPc2d1	italská
lir	lira	k1gFnPc2	lira
(	(	kIx(	(
<g/>
19,3	[number]	k4	19,3
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
/	/	kIx~	/
<g/>
20,6	[number]	k4	20,6
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
v	v	k7c6	v
cenách	cena	k1gFnPc6	cena
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Koloseum	Koloseum	k1gNnSc1	Koloseum
symbolem	symbol	k1gInSc7	symbol
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
kampaně	kampaň	k1gFnSc2	kampaň
proti	proti	k7c3	proti
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
zrušen	zrušit	k5eAaPmNgInS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
před	před	k7c7	před
Koloseem	Koloseum	k1gNnSc7	Koloseum
konalo	konat	k5eAaImAgNnS	konat
několik	několik	k4yIc4	několik
demonstrací	demonstrace	k1gFnPc2	demonstrace
proti	proti	k7c3	proti
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
gesto	gesto	k1gNnSc4	gesto
proti	proti	k7c3	proti
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
změnili	změnit	k5eAaPmAgMnP	změnit
představitelé	představitel	k1gMnPc1	představitel
Říma	Řím	k1gInSc2	Řím
barvu	barva	k1gFnSc4	barva
nočního	noční	k2eAgNnSc2d1	noční
osvětlení	osvětlení	k1gNnSc2	osvětlení
Říma	Řím	k1gInSc2	Řím
od	od	k7c2	od
bílé	bílý	k2eAgFnSc2d1	bílá
na	na	k7c4	na
zlatou	zlatá	k1gFnSc4	zlatá
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kdekoliv	kdekoliv	k6eAd1	kdekoliv
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
odsouzená	odsouzený	k2eAgFnSc1d1	odsouzená
tomuto	tento	k3xDgInSc3	tento
trestu	trest	k1gInSc2	trest
osvobozena	osvobozen	k2eAgFnSc1d1	osvobozena
<g/>
,	,	kIx,	,
propuštěna	propuštěn	k2eAgFnSc1d1	propuštěna
nebo	nebo	k8xC	nebo
jí	on	k3xPp3gFnSc3	on
je	být	k5eAaImIp3nS	být
trest	trest	k1gInSc1	trest
zmírněn	zmírnit	k5eAaPmNgInS	zmírnit
<g/>
.	.	kIx.	.
</s>
<s>
Koloseum	Koloseum	k1gNnSc1	Koloseum
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
zlatě	zlatě	k6eAd1	zlatě
nasvíceno	nasvícen	k2eAgNnSc1d1	nasvíceno
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
poškozenému	poškozený	k2eAgInSc3d1	poškozený
stavu	stav	k1gInSc3	stav
interiéru	interiér	k1gInSc2	interiér
je	být	k5eAaImIp3nS	být
nepraktické	praktický	k2eNgNnSc1d1	nepraktické
pořádat	pořádat	k5eAaImF	pořádat
v	v	k7c6	v
Koloseu	Koloseum	k1gNnSc6	Koloseum
velké	velký	k2eAgFnSc2d1	velká
události	událost	k1gFnSc2	událost
<g/>
;	;	kIx,	;
do	do	k7c2	do
současného	současný	k2eAgNnSc2d1	současné
hlediště	hlediště	k1gNnSc2	hlediště
se	se	k3xPyFc4	se
vejde	vejít	k5eAaPmIp3nS	vejít
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInPc1d2	veliký
koncerty	koncert	k1gInPc1	koncert
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
před	před	k7c7	před
Koloseem	Koloseum	k1gNnSc7	Koloseum
a	a	k8xC	a
Koloseum	Koloseum	k1gNnSc1	Koloseum
bylo	být	k5eAaImAgNnS	být
užíváno	užívat	k5eAaImNgNnS	užívat
jako	jako	k9	jako
pozadí	pozadí	k1gNnSc1	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
minulých	minulý	k2eAgNnPc6d1	Minulé
letech	léto	k1gNnPc6	léto
hráli	hrát	k5eAaImAgMnP	hrát
u	u	k7c2	u
Kolosea	Koloseum	k1gNnSc2	Koloseum
<g/>
,	,	kIx,	,
jmenujme	jmenovat	k5eAaBmRp1nP	jmenovat
Raye	Raye	k1gFnSc4	Raye
Charlese	Charles	k1gMnSc2	Charles
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Paula	Paul	k1gMnSc2	Paul
McCartneyho	McCartney	k1gMnSc2	McCartney
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
Eltona	Elton	k1gMnSc4	Elton
Johna	John	k1gMnSc4	John
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
Koloseum	Koloseum	k1gNnSc1	Koloseum
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
Nových	Nových	k2eAgInPc2d1	Nových
sedmi	sedm	k4xCc2	sedm
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyzický	fyzický	k2eAgInSc4d1	fyzický
popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vnitřní	vnitřní	k2eAgNnPc4d1	vnitřní
sedadla	sedadlo	k1gNnPc4	sedadlo
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
kalendáře	kalendář	k1gInSc2	kalendář
z	z	k7c2	z
roku	rok	k1gInSc2	rok
354	[number]	k4	354
mohlo	moct	k5eAaImAgNnS	moct
Koloseum	Koloseum	k1gNnSc1	Koloseum
pojmout	pojmout	k5eAaPmF	pojmout
87	[number]	k4	87
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
moderní	moderní	k2eAgInPc1d1	moderní
odhady	odhad	k1gInPc1	odhad
počítají	počítat	k5eAaImIp3nP	počítat
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
kolem	kolem	k7c2	kolem
50	[number]	k4	50
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Posazeni	posazen	k2eAgMnPc1d1	posazen
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
řadovém	řadový	k2eAgNnSc6d1	řadové
uspořádání	uspořádání	k1gNnSc6	uspořádání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
odráželo	odrážet	k5eAaImAgNnS	odrážet
přísnou	přísný	k2eAgFnSc4d1	přísná
rozvrstvenost	rozvrstvenost	k1gFnSc4	rozvrstvenost
římské	římský	k2eAgFnSc2d1	římská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
a	a	k8xC	a
jižním	jižní	k2eAgInSc6d1	jižní
konci	konec	k1gInSc6	konec
byly	být	k5eAaImAgInP	být
vyhrazeny	vyhrazen	k2eAgInPc1d1	vyhrazen
speciální	speciální	k2eAgInPc1d1	speciální
boxy	box	k1gInPc1	box
pro	pro	k7c4	pro
římského	římský	k2eAgMnSc4d1	římský
císaře	císař	k1gMnSc4	císař
a	a	k8xC	a
vestálky	vestálka	k1gFnPc4	vestálka
a	a	k8xC	a
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výhled	výhled	k1gInSc4	výhled
do	do	k7c2	do
arény	aréna	k1gFnSc2	aréna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
byla	být	k5eAaImAgFnS	být
široká	široký	k2eAgFnSc1d1	široká
platforma	platforma	k1gFnSc1	platforma
nebo	nebo	k8xC	nebo
podium	podium	k1gNnSc1	podium
pro	pro	k7c4	pro
třídu	třída	k1gFnSc4	třída
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
směli	smět	k5eAaImAgMnP	smět
přinést	přinést	k5eAaPmF	přinést
vlastní	vlastní	k2eAgFnPc4d1	vlastní
židle	židle	k1gFnPc4	židle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kamenech	kámen	k1gInPc6	kámen
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
stále	stále	k6eAd1	stále
viděna	viděn	k2eAgNnPc4d1	viděno
vyrytá	vyrytý	k2eAgNnPc4d1	vyryté
jména	jméno	k1gNnPc4	jméno
některých	některý	k3yIgMnPc2	některý
senátorů	senátor	k1gMnPc2	senátor
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
tím	ten	k3xDgNnSc7	ten
zajišťovali	zajišťovat	k5eAaImAgMnP	zajišťovat
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgNnSc4d1	vlastní
užití	užití	k1gNnSc4	užití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řada	řada	k1gFnSc1	řada
nad	nad	k7c4	nad
senátory	senátor	k1gMnPc4	senátor
<g/>
,	,	kIx,	,
známá	známá	k1gFnSc1	známá
jako	jako	k8xC	jako
maenianum	maenianum	k1gNnSc1	maenianum
primum	primum	k1gInSc4	primum
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
pro	pro	k7c4	pro
druhý	druhý	k4xOgInSc4	druhý
aristokratický	aristokratický	k2eAgInSc4d1	aristokratický
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
jezdce	jezdec	k1gInPc4	jezdec
(	(	kIx(	(
<g/>
equites	equites	k1gInSc4	equites
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
další	další	k2eAgFnSc4d1	další
řadu	řada	k1gFnSc4	řada
výše	výše	k1gFnSc2	výše
bylo	být	k5eAaImAgNnS	být
maenianum	maenianum	k1gNnSc1	maenianum
secundum	secundum	k1gInSc1	secundum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
určeno	určit	k5eAaPmNgNnS	určit
řádným	řádný	k2eAgInSc7d1	řádný
římským	římský	k2eAgInSc7d1	římský
občanům	občan	k1gMnPc3	občan
(	(	kIx(	(
<g/>
patricijům	patricij	k1gMnPc3	patricij
<g/>
)	)	kIx)	)
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
sektorů	sektor	k1gInPc2	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
níže	níže	k1gFnSc1	níže
(	(	kIx(	(
<g/>
immum	immum	k1gInSc1	immum
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
bohaté	bohatý	k2eAgMnPc4d1	bohatý
občany	občan	k1gMnPc4	občan
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
výše	vysoce	k6eAd2	vysoce
položená	položený	k2eAgFnSc1d1	položená
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
summum	summum	k?	summum
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
chudé	chudý	k2eAgMnPc4d1	chudý
občany	občan	k1gMnPc4	občan
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
další	další	k1gNnSc4	další
sociální	sociální	k2eAgFnSc2d1	sociální
skupiny	skupina	k1gFnSc2	skupina
byly	být	k5eAaImAgInP	být
vyhrazeny	vyhradit	k5eAaPmNgInP	vyhradit
speciální	speciální	k2eAgInPc1d1	speciální
sektory	sektor	k1gInPc1	sektor
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
chlapce	chlapec	k1gMnSc2	chlapec
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
učiteli	učitel	k1gMnPc7	učitel
<g/>
,	,	kIx,	,
vojáky	voják	k1gMnPc7	voják
na	na	k7c4	na
dovolené	dovolený	k2eAgMnPc4d1	dovolený
<g/>
,	,	kIx,	,
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
hodnostáře	hodnostář	k1gMnPc4	hodnostář
<g/>
,	,	kIx,	,
písaře	písař	k1gMnPc4	písař
<g/>
,	,	kIx,	,
hlasatele	hlasatel	k1gMnPc4	hlasatel
<g/>
,	,	kIx,	,
kněze	kněz	k1gMnPc4	kněz
a	a	k8xC	a
tak	tak	k9	tak
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Kamenná	kamenný	k2eAgNnPc1d1	kamenné
(	(	kIx(	(
<g/>
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
mramorová	mramorový	k2eAgNnPc1d1	mramorové
<g/>
)	)	kIx)	)
sedadla	sedadlo	k1gNnPc1	sedadlo
byla	být	k5eAaImAgNnP	být
vyhrazena	vyhradit	k5eAaPmNgNnP	vyhradit
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
a	a	k8xC	a
šlechtu	šlechta	k1gFnSc4	šlechta
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nosili	nosit	k5eAaImAgMnP	nosit
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
polštáře	polštář	k1gInPc4	polštář
<g/>
.	.	kIx.	.
</s>
<s>
Prostory	prostor	k1gInPc1	prostor
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
skupiny	skupina	k1gFnPc4	skupina
byly	být	k5eAaImAgInP	být
rozeznány	rozeznán	k2eAgInPc1d1	rozeznán
popisky	popisek	k1gInPc1	popisek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc1d1	další
úroveň	úroveň	k1gFnSc1	úroveň
<g/>
,	,	kIx,	,
maenianum	maenianum	k1gInSc1	maenianum
secundum	secundum	k1gInSc1	secundum
in	in	k?	in
legneis	legneis	k1gFnSc2	legneis
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
přidána	přidat	k5eAaPmNgFnS	přidat
na	na	k7c4	na
úplný	úplný	k2eAgInSc4d1	úplný
vrchol	vrchol	k1gInSc4	vrchol
budovy	budova	k1gFnSc2	budova
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
Domitiana	Domitian	k1gMnSc2	Domitian
<g/>
.	.	kIx.	.
</s>
<s>
Skládala	skládat	k5eAaImAgFnS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
galerie	galerie	k1gFnSc2	galerie
pro	pro	k7c4	pro
chudé	chudý	k1gMnPc4	chudý
<g/>
,	,	kIx,	,
otroky	otrok	k1gMnPc4	otrok
a	a	k8xC	a
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
místnost	místnost	k1gFnSc1	místnost
pro	pro	k7c4	pro
stání	stání	k1gNnPc4	stání
nebo	nebo	k8xC	nebo
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
příkré	příkrý	k2eAgFnPc1d1	příkrá
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
lavičky	lavička	k1gFnPc1	lavička
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgMnPc3	některý
skupinám	skupina	k1gFnPc3	skupina
byl	být	k5eAaImAgInS	být
zakázán	zakázán	k2eAgInSc1d1	zakázán
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
Kolosea	Koloseum	k1gNnSc2	Koloseum
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
hrobníkům	hrobník	k1gMnPc3	hrobník
<g/>
,	,	kIx,	,
hercům	herec	k1gMnPc3	herec
a	a	k8xC	a
bývalým	bývalý	k2eAgMnPc3d1	bývalý
gladiátorům	gladiátor	k1gMnPc3	gladiátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
řada	řada	k1gFnSc1	řada
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
sekcí	sekce	k1gFnPc2	sekce
(	(	kIx(	(
<g/>
meaniana	meaniana	k1gFnSc1	meaniana
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
zahnutých	zahnutý	k2eAgFnPc2d1	zahnutá
chodeb	chodba	k1gFnPc2	chodba
a	a	k8xC	a
nízkých	nízký	k2eAgFnPc2d1	nízká
zdí	zeď	k1gFnPc2	zeď
(	(	kIx(	(
<g/>
praecinctiones	praecinctionesa	k1gFnPc2	praecinctionesa
nebo	nebo	k8xC	nebo
baltei	baltei	k1gNnPc2	baltei
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
byla	být	k5eAaImAgNnP	být
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
do	do	k7c2	do
cunei	cunei	k1gNnPc2	cunei
nebo	nebo	k8xC	nebo
výklenků	výklenek	k1gInPc2	výklenek
pomocí	pomocí	k7c2	pomocí
schodů	schod	k1gInPc2	schod
a	a	k8xC	a
uliček	ulička	k1gFnPc2	ulička
mezi	mezi	k7c7	mezi
sedadly	sedadlo	k1gNnPc7	sedadlo
z	z	k7c2	z
vomitoria	vomitorium	k1gNnSc2	vomitorium
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
řada	řada	k1gFnSc1	řada
sedadel	sedadlo	k1gNnPc2	sedadlo
(	(	kIx(	(
<g/>
gradus	gradus	k1gInSc1	gradus
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
očíslována	očíslován	k2eAgFnSc1d1	očíslována
a	a	k8xC	a
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
každé	každý	k3xTgNnSc4	každý
jednotlivé	jednotlivý	k2eAgNnSc4d1	jednotlivé
sedadlo	sedadlo	k1gNnSc4	sedadlo
přesně	přesně	k6eAd1	přesně
rozpoznáno	rozpoznat	k5eAaPmNgNnS	rozpoznat
podle	podle	k7c2	podle
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
sektoru	sektor	k1gInSc2	sektor
a	a	k8xC	a
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Aréna	aréna	k1gFnSc1	aréna
a	a	k8xC	a
hypogeum	hypogeum	k1gInSc1	hypogeum
===	===	k?	===
</s>
</p>
<p>
<s>
Aréna	aréna	k1gFnSc1	aréna
samotná	samotný	k2eAgFnSc1d1	samotná
měla	mít	k5eAaImAgFnS	mít
rozměry	rozměr	k1gInPc7	rozměr
83	[number]	k4	83
×	×	k?	×
48	[number]	k4	48
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
dřevěnou	dřevěný	k2eAgFnSc4d1	dřevěná
podlahu	podlaha	k1gFnSc4	podlaha
pokrytou	pokrytý	k2eAgFnSc4d1	pokrytá
pískem	písek	k1gInSc7	písek
(	(	kIx(	(
<g/>
latinské	latinský	k2eAgNnSc1d1	latinské
slovo	slovo	k1gNnSc1	slovo
pro	pro	k7c4	pro
písek	písek	k1gInSc4	písek
je	být	k5eAaImIp3nS	být
harena	haren	k2eAgFnSc1d1	haren
nebo	nebo	k8xC	nebo
arena	areen	k2eAgFnSc1d1	arena
<g/>
)	)	kIx)	)
a	a	k8xC	a
podlaha	podlaha	k1gFnSc1	podlaha
pokrývala	pokrývat	k5eAaImAgFnS	pokrývat
spletitou	spletitý	k2eAgFnSc4d1	spletitá
podzemní	podzemní	k2eAgFnSc4d1	podzemní
strukturu	struktura	k1gFnSc4	struktura
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
hypogeum	hypogeum	k1gNnSc4	hypogeum
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
podzemí	podzemí	k1gNnSc1	podzemí
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgFnPc1d1	malá
části	část	k1gFnPc1	část
původní	původní	k2eAgFnSc2d1	původní
podlahy	podlaha	k1gFnSc2	podlaha
arény	aréna	k1gFnSc2	aréna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hypogeum	hypogeum	k1gInSc1	hypogeum
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
jasně	jasně	k6eAd1	jasně
viditelné	viditelný	k2eAgFnPc1d1	viditelná
<g/>
.	.	kIx.	.
</s>
<s>
Skládalo	skládat	k5eAaImAgNnS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
dvojstupňové	dvojstupňový	k2eAgFnSc2d1	dvojstupňová
sítě	síť	k1gFnSc2	síť
podzemních	podzemní	k2eAgInPc2d1	podzemní
tunelů	tunel	k1gInPc2	tunel
a	a	k8xC	a
jeskyň	jeskyně	k1gFnPc2	jeskyně
pod	pod	k7c7	pod
arénou	aréna	k1gFnSc7	aréna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
byly	být	k5eAaImAgFnP	být
drženi	držet	k5eAaImNgMnP	držet
gladiátoři	gladiátor	k1gMnPc1	gladiátor
a	a	k8xC	a
zvířata	zvíře	k1gNnPc1	zvíře
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
začaly	začít	k5eAaPmAgInP	začít
zápasy	zápas	k1gInPc1	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Osmdesát	osmdesát	k4xCc4	osmdesát
vertikálních	vertikální	k2eAgFnPc2d1	vertikální
šachet	šachta	k1gFnPc2	šachta
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
stálý	stálý	k2eAgInSc4d1	stálý
přístup	přístup	k1gInSc4	přístup
pro	pro	k7c4	pro
vězněná	vězněný	k2eAgNnPc4d1	vězněné
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
kusy	kus	k1gInPc4	kus
scén	scéna	k1gFnPc2	scéna
ukrytých	ukrytý	k2eAgFnPc2d1	ukrytá
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
<g/>
;	;	kIx,	;
větší	veliký	k2eAgFnSc2d2	veliký
zavěšené	zavěšený	k2eAgFnSc2d1	zavěšená
platformy	platforma	k1gFnSc2	platforma
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgNnSc4d1	nazývané
hegmata	hegma	k1gNnPc4	hegma
<g/>
,	,	kIx,	,
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
přístup	přístup	k1gInSc4	přístup
pro	pro	k7c4	pro
slony	slon	k1gMnPc4	slon
a	a	k8xC	a
podobné	podobný	k2eAgInPc4d1	podobný
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
přebudovány	přebudovat	k5eAaPmNgInP	přebudovat
při	při	k7c6	při
četných	četný	k2eAgFnPc6d1	četná
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
;	;	kIx,	;
vidět	vidět	k5eAaImF	vidět
můžeme	moct	k5eAaImIp1nP	moct
nejméně	málo	k6eAd3	málo
dvanáct	dvanáct	k4xCc4	dvanáct
různých	různý	k2eAgFnPc2d1	různá
fází	fáze	k1gFnPc2	fáze
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hypogeum	Hypogeum	k1gNnSc1	Hypogeum
bylo	být	k5eAaImAgNnS	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
místy	místo	k1gNnPc7	místo
mimo	mimo	k7c4	mimo
Koloseum	Koloseum	k1gNnSc4	Koloseum
pomocí	pomocí	k7c2	pomocí
podzemních	podzemní	k2eAgInPc2d1	podzemní
tunelů	tunel	k1gInPc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
blízkých	blízký	k2eAgFnPc2d1	blízká
stájí	stáj	k1gFnPc2	stáj
byli	být	k5eAaImAgMnP	být
tunelem	tunel	k1gInSc7	tunel
přiváděni	přiváděn	k2eAgMnPc1d1	přiváděn
účinkující	účinkující	k1gMnPc1	účinkující
a	a	k8xC	a
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
kasárny	kasárny	k1gFnPc4	kasárny
gladiátorů	gladiátor	k1gMnPc2	gladiátor
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Kolosea	Koloseum	k1gNnSc2	Koloseum
u	u	k7c2	u
Luda	Lud	k1gInSc2	Lud
Magna	Magn	k1gInSc2	Magn
byly	být	k5eAaImAgFnP	být
spojeny	spojit	k5eAaPmNgInP	spojit
tunely	tunel	k1gInPc1	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Oddělené	oddělený	k2eAgInPc1d1	oddělený
tunely	tunel	k1gInPc1	tunel
sloužily	sloužit	k5eAaImAgInP	sloužit
císaři	císař	k1gMnSc3	císař
a	a	k8xC	a
Vestálkám	vestálka	k1gFnPc3	vestálka
a	a	k8xC	a
dovolovaly	dovolovat	k5eAaImAgFnP	dovolovat
jim	on	k3xPp3gMnPc3	on
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
a	a	k8xC	a
opustit	opustit	k5eAaPmF	opustit
Koloseum	Koloseum	k1gNnSc4	Koloseum
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
museli	muset	k5eAaImAgMnP	muset
projít	projít	k5eAaPmF	projít
davem	dav	k1gInSc7	dav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
hypogeu	hypogeus	k1gInSc6	hypogeus
existovalo	existovat	k5eAaImAgNnS	existovat
také	také	k9	také
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Výtahy	výtah	k1gInPc1	výtah
a	a	k8xC	a
kladky	kladka	k1gFnPc1	kladka
zvyšovaly	zvyšovat	k5eAaImAgFnP	zvyšovat
a	a	k8xC	a
snižovaly	snižovat	k5eAaImAgFnP	snižovat
scénu	scéna	k1gFnSc4	scéna
a	a	k8xC	a
kulisy	kulisa	k1gFnPc4	kulisa
a	a	k8xC	a
také	také	k9	také
vyvážely	vyvážet	k5eAaImAgInP	vyvážet
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
uvězněná	uvězněný	k2eAgNnPc1d1	uvězněné
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Koloseu	Koloseum	k1gNnSc6	Koloseum
jsou	být	k5eAaImIp3nP	být
záznamy	záznam	k1gInPc1	záznam
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
hlavních	hlavní	k2eAgInPc2d1	hlavní
hydraulických	hydraulický	k2eAgInPc2d1	hydraulický
mechanismů	mechanismus	k1gInPc2	mechanismus
a	a	k8xC	a
podle	podle	k7c2	podle
starověkých	starověký	k2eAgInPc2d1	starověký
záznamů	záznam	k1gInPc2	záznam
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zatopit	zatopit	k5eAaPmF	zatopit
arénu	aréna	k1gFnSc4	aréna
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
díky	díky	k7c3	díky
spojení	spojení	k1gNnSc3	spojení
k	k	k7c3	k
blízkému	blízký	k2eAgInSc3d1	blízký
akvaduktu	akvadukt	k1gInSc3	akvadukt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podpůrné	podpůrný	k2eAgFnPc4d1	podpůrná
budovy	budova	k1gFnPc4	budova
===	===	k?	===
</s>
</p>
<p>
<s>
Koloseum	Koloseum	k1gNnSc1	Koloseum
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
aktivity	aktivita	k1gFnPc1	aktivita
podporovaly	podporovat	k5eAaImAgFnP	podporovat
pomáhaly	pomáhat	k5eAaImAgFnP	pomáhat
podstatnému	podstatný	k2eAgNnSc3d1	podstatné
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
samotného	samotný	k2eAgInSc2d1	samotný
amfiteátru	amfiteátr	k1gInSc2	amfiteátr
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
hrami	hra	k1gFnPc7	hra
spojeno	spojit	k5eAaPmNgNnS	spojit
mnoho	mnoho	k4c1	mnoho
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
na	na	k7c4	na
východ	východ	k1gInSc4	východ
stojí	stát	k5eAaImIp3nP	stát
zbytky	zbytek	k1gInPc1	zbytek
Luda	Lud	k1gInSc2	Lud
Magna	Magn	k1gInSc2	Magn
<g/>
,	,	kIx,	,
tréninkové	tréninkový	k2eAgFnPc4d1	tréninková
školy	škola	k1gFnPc4	škola
pro	pro	k7c4	pro
gladiátory	gladiátor	k1gMnPc4	gladiátor
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
Koloseem	Koloseum	k1gNnSc7	Koloseum
propojena	propojit	k5eAaPmNgFnS	propojit
podzemní	podzemní	k2eAgFnSc7d1	podzemní
chodbou	chodba	k1gFnSc7	chodba
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
gladiátorům	gladiátor	k1gMnPc3	gladiátor
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
snadný	snadný	k2eAgInSc4d1	snadný
přístup	přístup	k1gInSc4	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Ludus	Ludus	k1gMnSc1	Ludus
Magnus	Magnus	k1gMnSc1	Magnus
měla	mít	k5eAaImAgFnS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
miniaturní	miniaturní	k2eAgFnSc4d1	miniaturní
arénu	aréna	k1gFnSc4	aréna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
byla	být	k5eAaImAgFnS	být
populární	populární	k2eAgFnSc7d1	populární
atraktivitou	atraktivita	k1gFnSc7	atraktivita
pro	pro	k7c4	pro
římské	římský	k2eAgMnPc4d1	římský
diváky	divák	k1gMnPc4	divák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
samé	samý	k3xTgFnSc6	samý
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
gladiátorské	gladiátorský	k2eAgFnPc1d1	gladiátorská
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Luda	Lud	k1gInSc2	Lud
Matutina	matutinum	k1gNnSc2	matutinum
(	(	kIx(	(
<g/>
Ranní	ranní	k2eAgFnSc1d1	ranní
škola	škola	k1gFnSc1	škola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
trénováni	trénován	k2eAgMnPc1d1	trénován
bojovníci	bojovník	k1gMnPc1	bojovník
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
plus	plus	k6eAd1	plus
Daciánova	Daciánův	k2eAgFnSc1d1	Daciánův
a	a	k8xC	a
Galská	galský	k2eAgFnSc1d1	galská
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
vedle	vedle	k7c2	vedle
Kolosea	Koloseum	k1gNnSc2	Koloseum
bylo	být	k5eAaImAgNnS	být
Armamentarium	Armamentarium	k1gNnSc4	Armamentarium
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInPc4d1	zahrnující
luky	luk	k1gInPc4	luk
<g/>
,	,	kIx,	,
šípy	šíp	k1gInPc4	šíp
a	a	k8xC	a
uchovavající	uchovavající	k2eAgFnPc4d1	uchovavající
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
;	;	kIx,	;
Summum	Summum	k?	Summum
Choragium	Choragium	k1gNnSc1	Choragium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
skladovány	skladován	k2eAgInPc1d1	skladován
stroje	stroj	k1gInPc1	stroj
<g/>
;	;	kIx,	;
Sanitarium	Sanitarium	k1gNnSc1	Sanitarium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vlastnilo	vlastnit	k5eAaImAgNnS	vlastnit
zařízení	zařízení	k1gNnSc1	zařízení
umožňující	umožňující	k2eAgNnSc1d1	umožňující
ošetření	ošetření	k1gNnSc1	ošetření
zraněných	zraněný	k2eAgMnPc2d1	zraněný
gladiátorů	gladiátor	k1gMnPc2	gladiátor
<g/>
;	;	kIx,	;
a	a	k8xC	a
Spoliarium	Spoliarium	k1gNnSc1	Spoliarium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgNnP	být
těla	tělo	k1gNnPc1	tělo
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
gladiátorů	gladiátor	k1gMnPc2	gladiátor
zbavena	zbaven	k2eAgFnSc1d1	zbavena
šípů	šíp	k1gInPc2	šíp
a	a	k8xC	a
rovnána	rovnán	k2eAgFnSc1d1	rovnána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
obvodu	obvod	k1gInSc2	obvod
Kolosea	Koloseum	k1gNnSc2	Koloseum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
18	[number]	k4	18
m	m	kA	m
od	od	k7c2	od
obvodu	obvod	k1gInSc2	obvod
<g/>
,	,	kIx,	,
stála	stát	k5eAaImAgFnS	stát
řada	řada	k1gFnSc1	řada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
kamenných	kamenný	k2eAgFnPc2d1	kamenná
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
pět	pět	k4xCc4	pět
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
přítomnost	přítomnost	k1gFnSc1	přítomnost
je	být	k5eAaImIp3nS	být
vysvětlována	vysvětlovat	k5eAaImNgFnS	vysvětlovat
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
;	;	kIx,	;
mohly	moct	k5eAaImAgInP	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
náboženské	náboženský	k2eAgFnPc4d1	náboženská
hranice	hranice	k1gFnPc4	hranice
nebo	nebo	k8xC	nebo
vnější	vnější	k2eAgFnPc4d1	vnější
hranice	hranice	k1gFnPc4	hranice
pro	pro	k7c4	pro
kontrolovače	kontrolovač	k1gMnPc4	kontrolovač
lístků	lístek	k1gInPc2	lístek
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
kotva	kotva	k1gFnSc1	kotva
pro	pro	k7c4	pro
velarium	velarium	k1gNnSc4	velarium
nebo	nebo	k8xC	nebo
střechu	střecha	k1gFnSc4	střecha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
Kolosea	Koloseum	k1gNnSc2	Koloseum
stojí	stát	k5eAaImIp3nS	stát
rovněž	rovněž	k9	rovněž
Konstantinův	Konstantinův	k2eAgInSc1d1	Konstantinův
oblouk	oblouk	k1gInSc1	oblouk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Využití	využití	k1gNnSc1	využití
===	===	k?	===
</s>
</p>
<p>
<s>
Koloseum	Koloseum	k1gNnSc1	Koloseum
bylo	být	k5eAaImAgNnS	být
užíváno	užívat	k5eAaImNgNnS	užívat
k	k	k7c3	k
pořádání	pořádání	k1gNnSc3	pořádání
gladiátorských	gladiátorský	k2eAgInPc2d1	gladiátorský
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgFnPc2d1	další
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
nazývaných	nazývaný	k2eAgNnPc2d1	nazývané
munera	munero	k1gNnSc2	munero
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vždy	vždy	k6eAd1	vždy
pořádány	pořádán	k2eAgFnPc1d1	pořádána
spíše	spíše	k9	spíše
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
než	než	k8xS	než
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Měly	mít	k5eAaImAgFnP	mít
silný	silný	k2eAgInSc4d1	silný
náboženský	náboženský	k2eAgInSc4d1	náboženský
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgInP	být
rovněž	rovněž	k9	rovněž
demonstrací	demonstrace	k1gFnPc2	demonstrace
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
rodinné	rodinný	k2eAgFnSc2d1	rodinná
prestiže	prestiž	k1gFnSc2	prestiž
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInPc1d1	populární
mezi	mezi	k7c7	mezi
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
populárním	populární	k2eAgInSc7d1	populární
druhem	druh	k1gInSc7	druh
show	show	k1gFnSc2	show
byl	být	k5eAaImAgInS	být
lov	lov	k1gInSc1	lov
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
venatio	venatio	k6eAd1	venatio
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
show	show	k1gFnSc1	show
využívala	využívat	k5eAaImAgFnS	využívat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
divokých	divoký	k2eAgFnPc2d1	divoká
šelem	šelma	k1gFnPc2	šelma
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
dovážených	dovážený	k2eAgInPc2d1	dovážený
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Středního	střední	k2eAgInSc2d1	střední
Východu	východ	k1gInSc2	východ
a	a	k8xC	a
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
tvory	tvor	k1gMnPc4	tvor
jakými	jaký	k3yQgInPc7	jaký
byli	být	k5eAaImAgMnP	být
nosorožci	nosorožec	k1gMnPc1	nosorožec
<g/>
,	,	kIx,	,
hroši	hroch	k1gMnPc1	hroch
<g/>
,	,	kIx,	,
sloni	slon	k1gMnPc1	slon
<g/>
,	,	kIx,	,
žirafy	žirafa	k1gFnPc1	žirafa
<g/>
,	,	kIx,	,
zubři	zubr	k1gMnPc1	zubr
<g/>
,	,	kIx,	,
lvi	lev	k1gMnPc1	lev
<g/>
,	,	kIx,	,
levharti	levhart	k1gMnPc1	levhart
<g/>
,	,	kIx,	,
medvědi	medvěd	k1gMnPc1	medvěd
<g/>
,	,	kIx,	,
kaspičtí	kaspický	k2eAgMnPc1d1	kaspický
tygři	tygr	k1gMnPc1	tygr
<g/>
,	,	kIx,	,
aligátoři	aligátor	k1gMnPc1	aligátor
<g/>
,	,	kIx,	,
krokodýli	krokodýl	k1gMnPc1	krokodýl
a	a	k8xC	a
pštrosi	pštros	k1gMnPc1	pštros
<g/>
.	.	kIx.	.
</s>
<s>
Bitvy	bitva	k1gFnPc1	bitva
a	a	k8xC	a
lovy	lov	k1gInPc1	lov
se	se	k3xPyFc4	se
často	často	k6eAd1	často
odehrávaly	odehrávat	k5eAaImAgInP	odehrávat
uprostřed	uprostřed	k7c2	uprostřed
spletitých	spletitý	k2eAgFnPc2d1	spletitá
sad	sada	k1gFnPc2	sada
pohyblivých	pohyblivý	k2eAgInPc2d1	pohyblivý
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
události	událost	k1gFnPc1	událost
se	se	k3xPyFc4	se
příležitostně	příležitostně	k6eAd1	příležitostně
odehrávaly	odehrávat	k5eAaImAgFnP	odehrávat
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
;	;	kIx,	;
Traianus	Traianus	k1gMnSc1	Traianus
prý	prý	k9	prý
oslavoval	oslavovat	k5eAaImAgMnS	oslavovat
jeho	jeho	k3xOp3gNnSc4	jeho
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
Dácii	Dácie	k1gFnSc6	Dácie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
107	[number]	k4	107
podívanou	podívaná	k1gFnSc4	podívaná
čítající	čítající	k2eAgFnSc4d1	čítající
11	[number]	k4	11
000	[number]	k4	000
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
10	[number]	k4	10
000	[number]	k4	000
gladiátorů	gladiátor	k1gMnPc2	gladiátor
během	během	k7c2	během
123	[number]	k4	123
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Starověcí	starověký	k2eAgMnPc1d1	starověký
písaři	písař	k1gMnPc1	písař
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
raných	raný	k2eAgInPc2d1	raný
dní	den	k1gInPc2	den
Kolosea	Koloseum	k1gNnSc2	Koloseum
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
využívána	využívat	k5eAaPmNgFnS	využívat
pro	pro	k7c4	pro
naumachiae	naumachiae	k1gFnSc4	naumachiae
(	(	kIx(	(
<g/>
běžněji	běžně	k6eAd2	běžně
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
navalia	navalia	k1gFnSc1	navalia
proelia	proelia	k1gFnSc1	proelia
<g/>
)	)	kIx)	)
čili	čili	k8xC	čili
simulované	simulovaný	k2eAgFnSc2d1	simulovaná
námořní	námořní	k2eAgFnSc2d1	námořní
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
inauguračních	inaugurační	k2eAgFnPc6d1	inaugurační
hrách	hra	k1gFnPc6	hra
pořádaných	pořádaný	k2eAgFnPc6d1	pořádaná
Titem	Tit	k1gInSc7	Tit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
80	[number]	k4	80
bylo	být	k5eAaImAgNnS	být
Koloseum	Koloseum	k1gNnSc1	Koloseum
napuštěno	napuštěn	k2eAgNnSc1d1	napuštěno
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
ukázáni	ukázán	k2eAgMnPc1d1	ukázán
speciálně	speciálně	k6eAd1	speciálně
trénovaní	trénovaný	k2eAgMnPc1d1	trénovaný
plavající	plavající	k2eAgMnPc1d1	plavající
koně	kůň	k1gMnPc1	kůň
a	a	k8xC	a
býci	býk	k1gMnPc1	býk
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
záznam	záznam	k1gInSc4	záznam
o	o	k7c4	o
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
slavné	slavný	k2eAgFnSc2d1	slavná
námořní	námořní	k2eAgFnSc2d1	námořní
bitvy	bitva	k1gFnSc2	bitva
mezi	mezi	k7c7	mezi
Korkyreánskými	Korkyreánský	k2eAgInPc7d1	Korkyreánský
(	(	kIx(	(
<g/>
Corfiot	Corfiot	k1gInSc1	Corfiot
<g/>
)	)	kIx)	)
Řeky	Řek	k1gMnPc4	Řek
a	a	k8xC	a
Korinťany	Korinťan	k1gMnPc4	Korinťan
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
předmětem	předmět	k1gInSc7	předmět
diskuse	diskuse	k1gFnSc2	diskuse
mezi	mezi	k7c7	mezi
historiky	historik	k1gMnPc7	historik
<g/>
;	;	kIx,	;
ačkoliv	ačkoliv	k8xS	ačkoliv
zásobení	zásobení	k1gNnSc1	zásobení
vodou	voda	k1gFnSc7	voda
by	by	kYmCp3nP	by
nebyl	být	k5eNaImAgInS	být
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejasné	jasný	k2eNgNnSc1d1	nejasné
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
aréna	aréna	k1gFnSc1	aréna
vodětěsná	vodětěsný	k2eAgFnSc1d1	vodětěsný
ani	ani	k8xC	ani
jak	jak	k6eAd1	jak
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
aréně	aréna	k1gFnSc6	aréna
dostatek	dostatek	k1gInSc4	dostatek
místa	místo	k1gNnSc2	místo
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
válečných	válečný	k2eAgFnPc2d1	válečná
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
záznamy	záznam	k1gInPc1	záznam
udávají	udávat	k5eAaImIp3nP	udávat
špatnou	špatný	k2eAgFnSc4d1	špatná
lokalitu	lokalita	k1gFnSc4	lokalita
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
Koloseum	Koloseum	k1gNnSc1	Koloseum
původně	původně	k6eAd1	původně
mělo	mít	k5eAaImAgNnS	mít
široký	široký	k2eAgInSc4d1	široký
plavební	plavební	k2eAgInSc4d1	plavební
kanál	kanál	k1gInSc4	kanál
pod	pod	k7c4	pod
svoji	svůj	k3xOyFgFnSc4	svůj
centrální	centrální	k2eAgFnSc4d1	centrální
osou	osý	k2eAgFnSc4d1	osá
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
hypogeem	hypogeus	k1gMnSc7	hypogeus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sylvae	Sylvae	k1gNnSc1	Sylvae
neboli	neboli	k8xC	neboli
osvěžení	osvěžení	k1gNnSc1	osvěžení
přírodních	přírodní	k2eAgFnPc2d1	přírodní
scén	scéna	k1gFnPc2	scéna
se	se	k3xPyFc4	se
také	také	k9	také
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
aréně	aréna	k1gFnSc6	aréna
<g/>
.	.	kIx.	.
</s>
<s>
Malíři	malíř	k1gMnPc1	malíř
<g/>
,	,	kIx,	,
technici	technik	k1gMnPc1	technik
a	a	k8xC	a
architekti	architekt	k1gMnPc1	architekt
postavili	postavit	k5eAaPmAgMnP	postavit
podobiznu	podobizna	k1gFnSc4	podobizna
lesa	les	k1gInSc2	les
s	s	k7c7	s
pravými	pravý	k2eAgInPc7d1	pravý
stromy	strom	k1gInPc7	strom
a	a	k8xC	a
keři	keř	k1gInPc7	keř
pěstovanými	pěstovaný	k2eAgInPc7d1	pěstovaný
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
arény	aréna	k1gFnSc2	aréna
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
byla	být	k5eAaImAgNnP	být
představena	představit	k5eAaPmNgNnP	představit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
oživila	oživit	k5eAaPmAgFnS	oživit
scénu	scéna	k1gFnSc4	scéna
pro	pro	k7c4	pro
potěšení	potěšení	k1gNnSc4	potěšení
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Takových	takový	k3xDgFnPc2	takový
scén	scéna	k1gFnPc2	scéna
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
užívat	užívat	k5eAaImF	užívat
jednoduše	jednoduše	k6eAd1	jednoduše
k	k	k7c3	k
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
přirozené	přirozený	k2eAgNnSc4d1	přirozené
prostředí	prostředí	k1gNnSc4	prostředí
pro	pro	k7c4	pro
lidskou	lidský	k2eAgFnSc4d1	lidská
populaci	populace	k1gFnSc4	populace
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
užívalo	užívat	k5eAaImAgNnS	užívat
jako	jako	k9	jako
pozadí	pozadí	k1gNnSc1	pozadí
lovům	lov	k1gInPc3	lov
a	a	k8xC	a
dramatům	drama	k1gNnPc3	drama
popisujícím	popisující	k2eAgFnPc3d1	popisující
epizody	epizoda	k1gFnPc4	epizoda
z	z	k7c2	z
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Příležitostně	příležitostně	k6eAd1	příležitostně
byly	být	k5eAaImAgInP	být
rovněž	rovněž	k9	rovněž
využívány	využívat	k5eAaPmNgInP	využívat
pro	pro	k7c4	pro
popravy	poprava	k1gFnPc4	poprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterých	který	k3yRgNnPc6	který
hrdina	hrdina	k1gMnSc1	hrdina
příběhu	příběh	k1gInSc2	příběh
–	–	k?	–
představován	představován	k2eAgInSc1d1	představován
odsouzenou	odsouzený	k2eAgFnSc7d1	odsouzená
osobou	osoba	k1gFnSc7	osoba
–	–	k?	–
byl	být	k5eAaImAgInS	být
zabit	zabít	k5eAaPmNgInS	zabít
různými	různý	k2eAgInPc7d1	různý
hroznými	hrozný	k2eAgInPc7d1	hrozný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mytologicky	mytologicky	k6eAd1	mytologicky
autentickými	autentický	k2eAgInPc7d1	autentický
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
byl	být	k5eAaImAgMnS	být
sežrán	sežrat	k5eAaPmNgMnS	sežrat
šelmami	šelma	k1gFnPc7	šelma
nebo	nebo	k8xC	nebo
upálen	upálit	k5eAaPmNgInS	upálit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dnes	dnes	k6eAd1	dnes
===	===	k?	===
</s>
</p>
<p>
<s>
Koloseum	Koloseum	k1gNnSc1	Koloseum
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
hlavní	hlavní	k2eAgFnSc7d1	hlavní
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
s	s	k7c7	s
tisícovkami	tisícovka	k1gFnPc7	tisícovka
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
vidět	vidět	k5eAaImF	vidět
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
arénu	aréna	k1gFnSc4	aréna
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
vstup	vstup	k1gInSc1	vstup
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
EU	EU	kA	EU
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
dotován	dotovat	k5eAaBmNgInS	dotovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vstupy	vstup	k1gInPc1	vstup
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
mladší	mladý	k2eAgFnSc4d2	mladší
18	[number]	k4	18
a	a	k8xC	a
starší	starý	k2eAgMnPc1d2	starší
65	[number]	k4	65
let	léto	k1gNnPc2	léto
z	z	k7c2	z
EU	EU	kA	EU
jsou	být	k5eAaImIp3nP	být
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horním	horní	k2eAgNnSc6d1	horní
patře	patro	k1gNnSc6	patro
vnější	vnější	k2eAgFnSc2d1	vnější
zdi	zeď	k1gFnSc2	zeď
budovy	budova	k1gFnSc2	budova
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
muzeum	muzeum	k1gNnSc1	muzeum
věnované	věnovaný	k2eAgNnSc1d1	věnované
Erotovi	Eros	k1gMnSc6	Eros
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
podlahy	podlaha	k1gFnSc2	podlaha
arény	aréna	k1gFnSc2	aréna
byla	být	k5eAaImAgFnS	být
předělána	předělán	k2eAgFnSc1d1	předělána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koloseum	Koloseum	k1gNnSc1	Koloseum
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k6eAd1	rovněž
místem	místo	k1gNnSc7	místo
římskokatolických	římskokatolický	k2eAgFnPc2d1	Římskokatolická
ceremonií	ceremonie	k1gFnPc2	ceremonie
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
předváděl	předvádět	k5eAaImAgMnS	předvádět
v	v	k7c6	v
Koloseu	Koloseum	k1gNnSc6	Koloseum
zastavení	zastavení	k1gNnPc2	zastavení
křížů	kříž	k1gInPc2	kříž
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
Biblická	biblický	k2eAgFnSc1d1	biblická
cesta	cesta	k1gFnSc1	cesta
kříže	kříž	k1gInSc2	kříž
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
k	k	k7c3	k
větší	veliký	k2eAgFnSc3d2	veliký
meditaci	meditace	k1gFnSc3	meditace
<g/>
)	)	kIx)	)
na	na	k7c4	na
Velké	velký	k2eAgInPc4d1	velký
pátky	pátek	k1gInPc4	pátek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Flora	Flora	k1gFnSc1	Flora
==	==	k?	==
</s>
</p>
<p>
<s>
Koloseum	Koloseum	k1gNnSc1	Koloseum
má	mít	k5eAaImIp3nS	mít
bohatou	bohatý	k2eAgFnSc7d1	bohatá
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
dokumentovanou	dokumentovaný	k2eAgFnSc4d1	dokumentovaná
historii	historie	k1gFnSc4	historie
flory	flora	k1gFnSc2	flora
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
Domenico	Domenico	k6eAd1	Domenico
Panaroli	Panarole	k1gFnSc4	Panarole
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1643	[number]	k4	1643
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
první	první	k4xOgInSc4	první
katalog	katalog	k1gInSc4	katalog
zdejších	zdejší	k2eAgFnPc2d1	zdejší
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
rozpoznáno	rozpoznat	k5eAaPmNgNnS	rozpoznat
684	[number]	k4	684
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
(	(	kIx(	(
<g/>
420	[number]	k4	420
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
pokusům	pokus	k1gInPc3	pokus
vymýtit	vymýtit	k5eAaPmF	vymýtit
vegetaci	vegetace	k1gFnSc4	vegetace
kvůli	kvůli	k7c3	kvůli
obavám	obava	k1gFnPc3	obava
z	z	k7c2	z
poničení	poničení	k1gNnSc2	poničení
zdiva	zdivo	k1gNnSc2	zdivo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
pokusů	pokus	k1gInPc2	pokus
neuspěla	uspět	k5eNaPmAgFnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
napočítáno	napočítán	k2eAgNnSc1d1	napočítáno
242	[number]	k4	242
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
z	z	k7c2	z
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgMnS	popsat
Panaroli	Panarole	k1gFnSc4	Panarole
<g/>
,	,	kIx,	,
jich	on	k3xPp3gMnPc2	on
zbylo	zbýt	k5eAaPmAgNnS	zbýt
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Různorodost	různorodost	k1gFnSc1	různorodost
druhů	druh	k1gInPc2	druh
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vysvětlována	vysvětlovat	k5eAaImNgFnS	vysvětlovat
změnou	změna	k1gFnSc7	změna
klimatu	klima	k1gNnSc2	klima
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
během	během	k7c2	během
staletí	staletí	k1gNnPc2	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Migrace	migrace	k1gFnSc1	migrace
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
kvetení	kvetení	k1gNnSc1	kvetení
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
růst	růst	k1gInSc1	růst
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobil	způsobit	k5eAaPmAgInS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Koloseum	Koloseum	k1gNnSc1	Koloseum
vsazeno	vsadit	k5eAaPmNgNnS	vsadit
uprostřed	uprostřed	k7c2	uprostřed
moderního	moderní	k2eAgNnSc2d1	moderní
městského	městský	k2eAgNnSc2d1	Městské
centra	centrum	k1gNnSc2	centrum
spíše	spíše	k9	spíše
než	než	k8xS	než
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
starověkého	starověký	k2eAgNnSc2d1	starověké
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
také	také	k9	také
příčinami	příčina	k1gFnPc7	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
romantický	romantický	k2eAgInSc1d1	romantický
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
uváděný	uváděný	k2eAgInSc4d1	uváděný
důvod	důvod	k1gInSc4	důvod
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
semena	semeno	k1gNnPc1	semeno
rostlin	rostlina	k1gFnPc2	rostlina
jsou	být	k5eAaImIp3nP	být
bezděčně	bezděčně	k6eAd1	bezděčně
přenášena	přenášen	k2eAgNnPc1d1	přenášeno
zvířaty	zvíře	k1gNnPc7	zvíře
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
koutů	kout	k1gInPc2	kout
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Populární	populární	k2eAgFnSc1d1	populární
kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Ikonický	ikonický	k2eAgInSc1d1	ikonický
status	status	k1gInSc1	status
Kolosea	Koloseum	k1gNnSc2	Koloseum
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
filmech	film	k1gInPc6	film
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
odvětvích	odvětví	k1gNnPc6	odvětví
populární	populární	k2eAgFnSc2d1	populární
kultury	kultura	k1gFnSc2	kultura
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
Cola	cola	k1gFnSc1	cola
Portera	Porter	k1gMnSc2	Porter
"	"	kIx"	"
<g/>
You	You	k1gFnSc1	You
<g/>
́	́	k?	́
<g/>
re	re	k9	re
the	the	k?	the
Top	topit	k5eAaImRp2nS	topit
<g/>
"	"	kIx"	"
z	z	k7c2	z
muzikálu	muzikál	k1gInSc2	muzikál
Všechno	všechen	k3xTgNnSc1	všechen
pomíjí	pomíjet	k5eAaImIp3nS	pomíjet
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
řádku	řádek	k1gInSc2	řádek
"	"	kIx"	"
<g/>
You	You	k1gFnSc1	You
<g/>
́	́	k?	́
<g/>
re	re	k9	re
the	the	k?	the
Top	topit	k5eAaImRp2nS	topit
<g/>
,	,	kIx,	,
You	You	k1gFnPc6	You
<g/>
́	́	k?	́
<g/>
re	re	k9	re
the	the	k?	the
Colosseum	Colosseum	k1gInSc1	Colosseum
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
Prázdniny	prázdniny	k1gFnPc4	prázdniny
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
sloužilo	sloužit	k5eAaImAgNnS	sloužit
Koloseum	Koloseum	k1gNnSc4	Koloseum
slavně	slavně	k6eAd1	slavně
jako	jako	k8xS	jako
pozadí	pozadí	k1gNnSc1	pozadí
několika	několik	k4yIc2	několik
scénám	scéna	k1gFnPc3	scéna
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
Demetrius	Demetrius	k1gInSc4	Demetrius
a	a	k8xC	a
gladiátoři	gladiátor	k1gMnPc1	gladiátor
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
císař	císař	k1gMnSc1	císař
Caligula	Caligul	k1gMnSc2	Caligul
křesťana	křesťan	k1gMnSc2	křesťan
Demetria	Demetrium	k1gNnSc2	Demetrium
k	k	k7c3	k
boji	boj	k1gInSc3	boj
v	v	k7c6	v
Koloseu	Koloseum	k1gNnSc6	Koloseum
</s>
</p>
<p>
<s>
Závěr	závěr	k1gInSc1	závěr
filmu	film	k1gInSc2	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
mil	míle	k1gFnPc2	míle
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
20	[number]	k4	20
Million	Million	k1gInSc1	Million
Miles	Miles	k1gInSc4	Miles
to	ten	k3xDgNnSc4	ten
Earth	Earth	k1gMnSc1	Earth
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Koloseu	Koloseum	k1gNnSc6	Koloseum
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
Cesta	cesta	k1gFnSc1	cesta
draka	drak	k1gMnSc4	drak
bojoval	bojovat	k5eAaImAgInS	bojovat
v	v	k7c6	v
Koloseu	Koloseum	k1gNnSc6	Koloseum
Bruce	Bruce	k1gMnSc1	Bruce
Lee	Lea	k1gFnSc3	Lea
s	s	k7c7	s
Chuckem	Chucko	k1gNnSc7	Chucko
Norrisem	Norris	k1gInSc7	Norris
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Ridleyho	Ridley	k1gMnSc2	Ridley
Scotta	Scott	k1gMnSc2	Scott
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
Gladiátor	gladiátor	k1gMnSc1	gladiátor
bylo	být	k5eAaImAgNnS	být
Koloseum	Koloseum	k1gNnSc1	Koloseum
předěláno	předělat	k5eAaPmNgNnS	předělat
pomocí	pomocí	k7c2	pomocí
počítačově	počítačově	k6eAd1	počítačově
upravené	upravený	k2eAgFnSc2d1	upravená
fantazie	fantazie	k1gFnSc2	fantazie
(	(	kIx(	(
<g/>
CGI	CGI	kA	CGI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
"	"	kIx"	"
<g/>
obnovilo	obnovit	k5eAaPmAgNnS	obnovit
<g/>
"	"	kIx"	"
jeho	jeho	k3xOp3gFnSc4	jeho
slávu	sláva	k1gFnSc4	sláva
ze	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc1	popis
budovy	budova	k1gFnSc2	budova
samotné	samotný	k2eAgFnSc2d1	samotná
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
přesný	přesný	k2eAgInSc1d1	přesný
a	a	k8xC	a
podává	podávat	k5eAaImIp3nS	podávat
dobrý	dobrý	k2eAgInSc1d1	dobrý
obrázek	obrázek	k1gInSc1	obrázek
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
podzemní	podzemní	k2eAgInSc4d1	podzemní
hypogeum	hypogeum	k1gInSc4	hypogeum
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
Jumper	jumper	k1gInSc4	jumper
bylo	být	k5eAaImAgNnS	být
Koloseum	Koloseum	k1gNnSc1	Koloseum
použito	použít	k5eAaPmNgNnS	použít
jako	jako	k8xS	jako
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
jednu	jeden	k4xCgFnSc4	jeden
bitvu	bitva	k1gFnSc4	bitva
mezi	mezi	k7c7	mezi
jumpery	jumper	k1gInPc7	jumper
a	a	k8xC	a
paladiny	paladin	k1gMnPc7	paladin
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
science	scienka	k1gFnSc6	scienka
fiction	fiction	k1gInSc4	fiction
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
Jádro	jádro	k1gNnSc4	jádro
je	on	k3xPp3gNnSc4	on
Koloseum	Koloseum	k1gNnSc4	Koloseum
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
Říma	Řím	k1gInSc2	Řím
zničeno	zničen	k2eAgNnSc1d1	zničeno
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
superbouří	superbouřit	k5eAaPmIp3nS	superbouřit
</s>
</p>
<p>
<s>
V	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
hře	hra	k1gFnSc6	hra
Assassin	Assassin	k1gInSc1	Assassin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Creed	Creed	k1gInSc1	Creed
<g/>
:	:	kIx,	:
Brotherhood	Brotherhood	k1gInSc1	Brotherhood
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
koloseem	koloseum	k1gNnSc7	koloseum
nachází	nacházet	k5eAaImIp3nS	nacházet
ukryté	ukrytý	k2eAgNnSc4d1	ukryté
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
technologií	technologie	k1gFnSc7	technologie
prastaré	prastarý	k2eAgFnSc2d1	prastará
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
samotné	samotný	k2eAgFnSc2d1	samotná
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
do	do	k7c2	do
prostor	prostora	k1gFnPc2	prostora
rozbořeného	rozbořený	k2eAgNnSc2d1	rozbořené
Kolosea	Koloseum	k1gNnSc2	Koloseum
umístěna	umístěn	k2eAgFnSc1d1	umístěna
pašiová	pašiový	k2eAgFnSc1d1	pašiový
hraSláva	hraSláva	k1gFnSc1	hraSláva
Kolosea	Koloseum	k1gNnSc2	Koloseum
jako	jako	k8xC	jako
místa	místo	k1gNnSc2	místo
pro	pro	k7c4	pro
zábavu	zábava	k1gFnSc4	zábava
vedla	vést	k5eAaImAgFnS	vést
také	také	k9	také
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
pro	pro	k7c4	pro
moderní	moderní	k2eAgInPc4d1	moderní
zábavní	zábavní	k2eAgInPc4d1	zábavní
podniky	podnik	k1gInPc4	podnik
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
divadla	divadlo	k1gNnPc1	divadlo
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgFnPc1d1	hudební
haly	hala	k1gFnPc1	hala
a	a	k8xC	a
budovy	budova	k1gFnPc1	budova
užívané	užívaný	k2eAgFnPc1d1	užívaná
pro	pro	k7c4	pro
sport	sport	k1gInSc4	sport
či	či	k8xC	či
představení	představení	k1gNnSc4	představení
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Koloseum	Koloseum	k1gNnSc4	Koloseum
nebo	nebo	k8xC	nebo
Koliseum	Koliseum	k1gNnSc4	Koliseum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Program	program	k1gInSc1	program
pro	pro	k7c4	pro
autorizaci	autorizace	k1gFnSc4	autorizace
optických	optický	k2eAgInPc2d1	optický
disků	disk	k1gInPc2	disk
Nero	Nero	k1gMnSc1	Nero
Burning	Burning	k1gInSc1	Burning
ROM	ROM	kA	ROM
užívá	užívat	k5eAaImIp3nS	užívat
obrázek	obrázek	k1gInSc1	obrázek
Kolosea	Koloseum	k1gNnSc2	Koloseum
v	v	k7c6	v
ohni	oheň	k1gInSc6	oheň
jako	jako	k9	jako
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
hlavních	hlavní	k2eAgFnPc2d1	hlavní
ikon	ikona	k1gFnPc2	ikona
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
Říma	Řím	k1gInSc2	Řím
císaře	císař	k1gMnSc2	císař
Nerona	Nero	k1gMnSc2	Nero
(	(	kIx(	(
<g/>
na	na	k7c4	na
který	který	k3yQgInSc4	který
jméno	jméno	k1gNnSc4	jméno
programu	program	k1gInSc2	program
a	a	k8xC	a
ikona	ikona	k1gFnSc1	ikona
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
64	[number]	k4	64
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
Koloseum	Koloseum	k1gNnSc1	Koloseum
postaveno	postavit	k5eAaPmNgNnS	postavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Gladiátor	gladiátor	k1gMnSc1	gladiátor
</s>
</p>
<p>
<s>
Meta	meta	k1gFnSc1	meta
Sudans	Sudansa	k1gFnPc2	Sudansa
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Koloseum	Koloseum	k1gNnSc4	Koloseum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnPc1	galerie
Koloseum	Koloseum	k1gNnSc4	Koloseum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
koloseum	koloseum	k1gNnSc4	koloseum
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Satelitní	satelitní	k2eAgInSc1d1	satelitní
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
Koloseum	Koloseum	k1gNnSc4	Koloseum
</s>
</p>
