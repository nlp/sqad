<s>
Estetika	estetika	k1gFnSc1	estetika
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
aisthetikos	aisthetikos	k1gInSc1	aisthetikos
-	-	kIx~	-
vnímavost	vnímavost	k1gFnSc1	vnímavost
<g/>
,	,	kIx,	,
cit	cit	k1gInSc1	cit
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
krásu	krása	k1gFnSc4	krása
<g/>
))	))	k?	))
je	být	k5eAaImIp3nS	být
filosofická	filosofický	k2eAgFnSc1d1	filosofická
disciplína	disciplína	k1gFnSc1	disciplína
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
krásnem	krásno	k1gNnSc7	krásno
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
působením	působení	k1gNnSc7	působení
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
lidským	lidský	k2eAgNnSc7d1	lidské
vnímáním	vnímání	k1gNnSc7	vnímání
pocitů	pocit	k1gInPc2	pocit
a	a	k8xC	a
dojmů	dojem	k1gInPc2	dojem
z	z	k7c2	z
uměleckých	umělecký	k2eAgInPc2d1	umělecký
i	i	k8xC	i
přírodních	přírodní	k2eAgInPc2d1	přírodní
výtvorů	výtvor	k1gInPc2	výtvor
<g/>
.	.	kIx.	.
</s>
