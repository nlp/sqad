<s>
Estetika	estetika	k1gFnSc1	estetika
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
aisthetikos	aisthetikos	k1gInSc1	aisthetikos
-	-	kIx~	-
vnímavost	vnímavost	k1gFnSc1	vnímavost
<g/>
,	,	kIx,	,
cit	cit	k1gInSc1	cit
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
krásu	krása	k1gFnSc4	krása
<g/>
))	))	k?	))
je	být	k5eAaImIp3nS	být
filosofická	filosofický	k2eAgFnSc1d1	filosofická
disciplína	disciplína	k1gFnSc1	disciplína
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
krásnem	krásno	k1gNnSc7	krásno
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
působením	působení	k1gNnSc7	působení
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
lidským	lidský	k2eAgNnSc7d1	lidské
vnímáním	vnímání	k1gNnSc7	vnímání
pocitů	pocit	k1gInPc2	pocit
a	a	k8xC	a
dojmů	dojem	k1gInPc2	dojem
z	z	k7c2	z
uměleckých	umělecký	k2eAgInPc2d1	umělecký
i	i	k8xC	i
přírodních	přírodní	k2eAgInPc2d1	přírodní
výtvorů	výtvor	k1gInPc2	výtvor
<g/>
.	.	kIx.	.
</s>
<s>
Estetické	estetický	k2eAgFnPc1d1	estetická
úvahy	úvaha	k1gFnPc1	úvaha
provázely	provázet	k5eAaImAgFnP	provázet
filosofii	filosofie	k1gFnSc4	filosofie
již	již	k9	již
od	od	k7c2	od
jejích	její	k3xOp3gInPc2	její
samotných	samotný	k2eAgInPc2d1	samotný
počátků	počátek	k1gInPc2	počátek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
disciplínu	disciplína	k1gFnSc4	disciplína
však	však	k9	však
estetiku	estetika	k1gFnSc4	estetika
vymezil	vymezit	k5eAaPmAgInS	vymezit
až	až	k9	až
roku	rok	k1gInSc2	rok
1750	[number]	k4	1750
A.	A.	kA	A.
G.	G.	kA	G.
Baumgarten	Baumgarten	k2eAgInSc4d1	Baumgarten
<g/>
.	.	kIx.	.
</s>
<s>
Pýthagorás	Pýthagorás	k6eAd1	Pýthagorás
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
následovníci	následovník	k1gMnPc1	následovník
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
pythagorejci	pythagorejec	k1gMnPc1	pythagorejec
<g/>
)	)	kIx)	)
spatřovali	spatřovat	k5eAaImAgMnP	spatřovat
krásu	krása	k1gFnSc4	krása
v	v	k7c6	v
dokonalosti	dokonalost	k1gFnSc6	dokonalost
matematického	matematický	k2eAgInSc2d1	matematický
řádu	řád	k1gInSc2	řád
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
řád	řád	k1gInSc1	řád
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
pythagorejců	pythagorejec	k1gMnPc2	pythagorejec
tvořen	tvořit	k5eAaImNgInS	tvořit
nezávisle	závisle	k6eNd1	závisle
existujícími	existující	k2eAgInPc7d1	existující
přirozenými	přirozený	k2eAgInPc7d1	přirozený
čísly	číslo	k1gNnPc7	číslo
(	(	kIx(	(
<g/>
=	=	kIx~	=
počty	počet	k1gInPc1	počet
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
poměry	poměr	k1gInPc1	poměr
(	(	kIx(	(
<g/>
racionálními	racionální	k2eAgNnPc7d1	racionální
čísly	číslo	k1gNnPc7	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
světě	svět	k1gInSc6	svět
počty	počet	k1gInPc1	počet
a	a	k8xC	a
poměry	poměr	k1gInPc1	poměr
projevovaly	projevovat	k5eAaImAgInP	projevovat
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
mírou	míra	k1gFnSc7	míra
dokonalosti	dokonalost	k1gFnSc2	dokonalost
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dle	dle	k7c2	dle
jejich	jejich	k3xOp3gInSc2	jejich
názoru	názor	k1gInSc2	názor
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
krása	krása	k1gFnSc1	krása
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pythagorejská	pythagorejský	k2eAgFnSc1d1	pythagorejská
teorie	teorie	k1gFnSc1	teorie
byla	být	k5eAaImAgFnS	být
podpořena	podpořit	k5eAaPmNgFnS	podpořit
i	i	k9	i
řadou	řada	k1gFnSc7	řada
jejich	jejich	k3xOp3gInPc2	jejich
výsledků	výsledek	k1gInPc2	výsledek
"	"	kIx"	"
<g/>
experimentálních	experimentální	k2eAgFnPc2d1	experimentální
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
zjištění	zjištění	k1gNnSc1	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
otvorů	otvor	k1gInPc2	otvor
na	na	k7c6	na
flétně	flétna	k1gFnSc6	flétna
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
postupným	postupný	k2eAgNnSc7d1	postupné
odkrýváním	odkrývání	k1gNnSc7	odkrývání
a	a	k8xC	a
zakrýváním	zakrývání	k1gNnSc7	zakrývání
lze	lze	k6eAd1	lze
zahrát	zahrát	k5eAaPmF	zahrát
základní	základní	k2eAgFnSc4d1	základní
stupnici	stupnice	k1gFnSc4	stupnice
(	(	kIx(	(
<g/>
libozvučných	libozvučný	k2eAgFnPc2d1	libozvučná
a	a	k8xC	a
tedy	tedy	k9	tedy
krásných	krásný	k2eAgInPc2d1	krásný
<g/>
)	)	kIx)	)
tónů	tón	k1gInPc2	tón
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
poměry	poměr	k1gInPc4	poměr
dané	daný	k2eAgInPc4d1	daný
nízkými	nízký	k2eAgInPc7d1	nízký
(	(	kIx(	(
<g/>
do	do	k7c2	do
10	[number]	k4	10
<g/>
)	)	kIx)	)
přirozenými	přirozený	k2eAgNnPc7d1	přirozené
čísly	číslo	k1gNnPc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejdokonalejší	dokonalý	k2eAgFnPc4d3	nejdokonalejší
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
poměrů	poměr	k1gInPc2	poměr
považovali	považovat	k5eAaImAgMnP	považovat
pythagorejci	pythagorejec	k1gMnPc1	pythagorejec
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
zlatý	zlatý	k2eAgInSc4d1	zlatý
řez	řez	k1gInSc4	řez
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Platóna	Platón	k1gMnSc2	Platón
je	být	k5eAaImIp3nS	být
krásno	krásno	k1gNnSc4	krásno
sepětím	sepětí	k1gNnSc7	sepětí
dobra	dobro	k1gNnSc2	dobro
<g/>
,	,	kIx,	,
pravdy	pravda	k1gFnSc2	pravda
a	a	k8xC	a
krásy	krása	k1gFnSc2	krása
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
systém	systém	k1gInSc1	systém
pohledu	pohled	k1gInSc2	pohled
na	na	k7c6	na
umění	umění	k1gNnSc6	umění
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
mimetický	mimetický	k2eAgMnSc1d1	mimetický
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
slova	slovo	k1gNnSc2	slovo
mimesis	mimesis	k1gFnSc2	mimesis
=	=	kIx~	=
nápodoba	nápodoba	k1gFnSc1	nápodoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
knize	kniha	k1gFnSc3	kniha
Ústavy	ústava	k1gFnSc2	ústava
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
na	na	k7c6	na
příkladu	příklad	k1gInSc6	příklad
existence	existence	k1gFnSc2	existence
tří	tři	k4xCgInPc2	tři
druhů	druh	k1gInPc2	druh
lavic	lavice	k1gFnPc2	lavice
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
jako	jako	k8xC	jako
idea	idea	k1gFnSc1	idea
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
skutečná	skutečný	k2eAgFnSc1d1	skutečná
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
řemeslný	řemeslný	k2eAgInSc1d1	řemeslný
výrobek	výrobek	k1gInSc1	výrobek
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
namalovaná	namalovaný	k2eAgFnSc1d1	namalovaná
umělcem	umělec	k1gMnSc7	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
poslední	poslední	k2eAgFnSc1d1	poslední
má	můj	k3xOp1gFnSc1	můj
podle	podle	k7c2	podle
Platóna	Platón	k1gMnSc4	Platón
nejmenší	malý	k2eAgFnSc4d3	nejmenší
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
jen	jen	k9	jen
odrazem	odraz	k1gInSc7	odraz
skutečnosti	skutečnost	k1gFnSc2	skutečnost
a	a	k8xC	a
tedy	tedy	k9	tedy
nejdále	daleko	k6eAd3	daleko
od	od	k7c2	od
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
ideje	idea	k1gFnSc2	idea
a	a	k8xC	a
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
dobra	dobro	k1gNnSc2	dobro
<g/>
.	.	kIx.	.
</s>
<s>
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
působením	působení	k1gNnPc3	působení
umění	umění	k1gNnSc2	umění
vzniká	vznikat	k5eAaImIp3nS	vznikat
všechno	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
čeho	co	k3yRnSc2	co
tvar	tvar	k1gInSc4	tvar
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
duši	duše	k1gFnSc6	duše
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tedy	tedy	k9	tedy
působením	působení	k1gNnSc7	působení
umění	umění	k1gNnSc2	umění
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
umělec	umělec	k1gMnSc1	umělec
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
představy	představa	k1gFnSc2	představa
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
jí	on	k3xPp3gFnSc3	on
tvar	tvar	k1gInSc1	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelův	Aristotelův	k2eAgInSc1d1	Aristotelův
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
také	také	k9	také
mimetický	mimetický	k2eAgMnSc1d1	mimetický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
v	v	k7c6	v
negativním	negativní	k2eAgInSc6d1	negativní
smyslu	smysl	k1gInSc6	smysl
jako	jako	k8xS	jako
u	u	k7c2	u
Platóna	Platón	k1gMnSc2	Platón
<g/>
.	.	kIx.	.
</s>
<s>
Záliba	záliba	k1gFnSc1	záliba
v	v	k7c6	v
napodobeninách	napodobenina	k1gFnPc6	napodobenina
je	být	k5eAaImIp3nS	být
člověku	člověk	k1gMnSc3	člověk
přirozená	přirozený	k2eAgFnSc1d1	přirozená
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
poznávání	poznávání	k1gNnSc4	poznávání
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
je	být	k5eAaImIp3nS	být
také	také	k9	také
prostředkem	prostředek	k1gInSc7	prostředek
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
citu	cit	k1gInSc2	cit
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
básnickém	básnický	k2eAgMnSc6d1	básnický
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ve	v	k7c6	v
spisu	spis	k1gInSc6	spis
"	"	kIx"	"
<g/>
Poetika	poetika	k1gFnSc1	poetika
<g/>
"	"	kIx"	"
a	a	k8xC	a
všeobecné	všeobecný	k2eAgInPc4d1	všeobecný
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
a	a	k8xC	a
příčinách	příčina	k1gFnPc6	příčina
věcí	věc	k1gFnPc2	věc
v	v	k7c6	v
"	"	kIx"	"
<g/>
Metafyzice	metafyzika	k1gFnSc6	metafyzika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
filosofii	filosofie	k1gFnSc6	filosofie
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
u	u	k7c2	u
Augustina	Augustin	k1gMnSc2	Augustin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
estetika	estetika	k1gFnSc1	estetika
zcela	zcela	k6eAd1	zcela
v	v	k7c6	v
područí	područí	k1gNnSc6	područí
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
krása	krása	k1gFnSc1	krása
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
veškerá	veškerý	k3xTgFnSc1	veškerý
krása	krása	k1gFnSc1	krása
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
od	od	k7c2	od
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Alexander	Alexandra	k1gFnPc2	Alexandra
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Baumgarten	Baumgartno	k1gNnPc2	Baumgartno
(	(	kIx(	(
<g/>
1714	[number]	k4	1714
<g/>
-	-	kIx~	-
<g/>
1762	[number]	k4	1762
<g/>
)	)	kIx)	)
svým	svůj	k3xOyFgInSc7	svůj
spisem	spis	k1gInSc7	spis
Aesthetica	Aesthetica	k1gMnSc1	Aesthetica
(	(	kIx(	(
<g/>
1750	[number]	k4	1750
<g/>
)	)	kIx)	)
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
estetiku	estetika	k1gFnSc4	estetika
za	za	k7c4	za
speciální	speciální	k2eAgFnSc4d1	speciální
oblast	oblast	k1gFnSc4	oblast
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
jí	jíst	k5eAaImIp3nS	jíst
tak	tak	k9	tak
zároveň	zároveň	k6eAd1	zároveň
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nepoužívané	používaný	k2eNgInPc1d1	nepoužívaný
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vedle	vedle	k7c2	vedle
logiky	logika	k1gFnSc2	logika
rozumu	rozum	k1gInSc2	rozum
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
ještě	ještě	k9	ještě
logiku	logika	k1gFnSc4	logika
obrazotvornosti	obrazotvornost	k1gFnSc2	obrazotvornost
<g/>
.	.	kIx.	.
</s>
<s>
Obrazotvornost	obrazotvornost	k1gFnSc4	obrazotvornost
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
nižší	nízký	k2eAgInSc4d2	nižší
<g/>
,	,	kIx,	,
zmatený	zmatený	k2eAgInSc4d1	zmatený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
důležitý	důležitý	k2eAgInSc4d1	důležitý
stupeň	stupeň	k1gInSc4	stupeň
poznávacího	poznávací	k2eAgInSc2d1	poznávací
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
této	tento	k3xDgFnSc3	tento
oblasti	oblast	k1gFnSc2	oblast
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
věnovat	věnovat	k5eAaPmF	věnovat
nově	nově	k6eAd1	nově
vyhlášený	vyhlášený	k2eAgInSc1d1	vyhlášený
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
-	-	kIx~	-
estetika	estetika	k1gFnSc1	estetika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Baumgartena	Baumgarten	k2eAgFnSc1d1	Baumgartena
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
studovat	studovat	k5eAaImF	studovat
především	především	k9	především
výtvory	výtvor	k1gInPc1	výtvor
krásných	krásný	k2eAgNnPc2d1	krásné
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nižšímu	nízký	k2eAgNnSc3d2	nižší
poznání	poznání	k1gNnSc3	poznání
<g/>
,	,	kIx,	,
k	k	k7c3	k
nejasnému	jasný	k2eNgNnSc3d1	nejasné
chápání	chápání	k1gNnSc3	chápání
<g/>
,	,	kIx,	,
k	k	k7c3	k
zmatené	zmatený	k2eAgFnSc3d1	zmatená
obrazotvornosti	obrazotvornost	k1gFnSc3	obrazotvornost
<g/>
.	.	kIx.	.
</s>
<s>
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
se	se	k3xPyFc4	se
estetikou	estetika	k1gFnSc7	estetika
zabývá	zabývat	k5eAaImIp3nS	zabývat
ve	v	k7c6	v
spise	spis	k1gInSc6	spis
Kritika	kritika	k1gFnSc1	kritika
soudnosti	soudnost	k1gFnSc2	soudnost
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
s	s	k7c7	s
vkusovým	vkusový	k2eAgInSc7d1	vkusový
soudem	soud	k1gInSc7	soud
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
samostatná	samostatný	k2eAgFnSc1d1	samostatná
duševní	duševní	k2eAgFnSc1d1	duševní
potence	potence	k1gFnSc1	potence
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
součást	součást	k1gFnSc4	součást
rozumu	rozum	k1gInSc2	rozum
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
subjektivní	subjektivní	k2eAgMnSc1d1	subjektivní
<g/>
.	.	kIx.	.
</s>
<s>
Vkusový	vkusový	k2eAgInSc1d1	vkusový
soud	soud	k1gInSc1	soud
je	být	k5eAaImIp3nS	být
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
a	a	k8xC	a
nelze	lze	k6eNd1	lze
jej	on	k3xPp3gMnSc4	on
zobecňovat	zobecňovat	k5eAaImF	zobecňovat
<g/>
.	.	kIx.	.
</s>
<s>
Kant	Kant	k1gMnSc1	Kant
je	být	k5eAaImIp3nS	být
formalista	formalista	k1gMnSc1	formalista
<g/>
,	,	kIx,	,
krása	krása	k1gFnSc1	krása
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
obsahu	obsah	k1gInSc6	obsah
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
u	u	k7c2	u
Hegela	Hegel	k1gMnSc2	Hegel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
jeho	jeho	k3xOp3gNnSc1	jeho
averze	averze	k1gFnSc1	averze
vůči	vůči	k7c3	vůči
služebnosti	služebnost	k1gFnSc3	služebnost
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
s	s	k7c7	s
sebou	se	k3xPyFc7	se
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
obsahovost	obsahovost	k1gFnSc4	obsahovost
může	moct	k5eAaImIp3nS	moct
přinášet	přinášet	k5eAaImF	přinášet
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
dělí	dělit	k5eAaImIp3nS	dělit
umění	umění	k1gNnSc1	umění
na	na	k7c4	na
mechanická	mechanický	k2eAgNnPc4d1	mechanické
(	(	kIx(	(
<g/>
řemesla	řemeslo	k1gNnPc4	řemeslo
a	a	k8xC	a
pod	pod	k7c4	pod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
estetická	estetický	k2eAgFnSc1d1	estetická
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
smyslem	smysl	k1gInSc7	smysl
pocit	pocit	k1gInSc1	pocit
libosti	libost	k1gFnSc2	libost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pojetí	pojetí	k1gNnSc2	pojetí
estetiky	estetika	k1gFnSc2	estetika
různí	různit	k5eAaImIp3nP	různit
<g/>
.	.	kIx.	.
</s>
<s>
Estetika	estetika	k1gFnSc1	estetika
je	být	k5eAaImIp3nS	být
vnímána	vnímat	k5eAaImNgFnS	vnímat
a	a	k8xC	a
vykládána	vykládán	k2eAgFnSc1d1	vykládána
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
fenomenologie	fenomenologie	k1gFnSc2	fenomenologie
<g/>
,	,	kIx,	,
existencialismu	existencialismus	k1gInSc2	existencialismus
<g/>
,	,	kIx,	,
intuitivismu	intuitivismus	k1gInSc2	intuitivismus
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgInPc2d1	další
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
hraje	hrát	k5eAaImIp3nS	hrát
estetika	estetika	k1gFnSc1	estetika
v	v	k7c6	v
postmoderní	postmoderní	k2eAgFnSc6d1	postmoderní
filosofii	filosofie	k1gFnSc6	filosofie
a	a	k8xC	a
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
estetika	estetik	k1gMnSc2	estetik
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
