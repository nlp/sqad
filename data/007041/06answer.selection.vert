<s>
Následovaly	následovat	k5eAaImAgInP	následovat
tedy	tedy	k9	tedy
další	další	k2eAgInPc1d1	další
bombové	bombový	k2eAgInPc1d1	bombový
útoky	útok	k1gInPc1	útok
spojenců	spojenec	k1gMnPc2	spojenec
až	až	k6eAd1	až
do	do	k7c2	do
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
okupanti	okupant	k1gMnPc1	okupant
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
provoz	provoz	k1gInSc4	provoz
opustit	opustit	k5eAaPmF	opustit
a	a	k8xC	a
odvézt	odvézt	k5eAaPmF	odvézt
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
50	[number]	k4	50
již	již	k6eAd1	již
hotových	hotový	k2eAgInPc2d1	hotový
sudů	sud	k1gInPc2	sud
těžké	těžký	k2eAgFnSc2d1	těžká
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Její	její	k3xOp3gFnSc1	její
koncentrace	koncentrace	k1gFnSc1	koncentrace
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
1	[number]	k4	1
a	a	k8xC	a
99	[number]	k4	99
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Hydro	hydra	k1gFnSc5	hydra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
převážející	převážející	k2eAgFnPc1d1	převážející
tento	tento	k3xDgInSc4	tento
náklad	náklad	k1gInSc4	náklad
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
představoval	představovat	k5eAaImAgMnS	představovat
zhruba	zhruba	k6eAd1	zhruba
desetinu	desetina	k1gFnSc4	desetina
množství	množství	k1gNnSc2	množství
potřebného	potřebné	k1gNnSc2	potřebné
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
,	,	kIx,	,
spáchán	spáchán	k2eAgInSc4d1	spáchán
atentát	atentát	k1gInSc4	atentát
<g/>
.	.	kIx.	.
</s>
