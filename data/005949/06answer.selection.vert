<s>
Hieroglyfický	hieroglyfický	k2eAgInSc1d1	hieroglyfický
systém	systém	k1gInSc1	systém
písma	písmo	k1gNnSc2	písmo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
komplikovaný	komplikovaný	k2eAgInSc1d1	komplikovaný
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
přes	přes	k7c4	přes
700	[number]	k4	700
různých	různý	k2eAgInPc2d1	různý
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
