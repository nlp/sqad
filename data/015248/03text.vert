<s>
Hrací	hrací	k2eAgFnSc1d1
karta	karta	k1gFnSc1
</s>
<s>
Mariášové	mariášový	k2eAgFnPc1d1
české	český	k2eAgFnPc1d1
jednohlavé	jednohlavý	k2eAgFnPc1d1
karty	karta	k1gFnPc1
</s>
<s>
Hrací	hrací	k2eAgFnSc1d1
karta	karta	k1gFnSc1
je	být	k5eAaImIp3nS
list	list	k1gInSc4
papíru	papír	k1gInSc2
s	s	k7c7
obrázkem	obrázek	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
při	při	k7c6
hře	hra	k1gFnSc6
karetních	karetní	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrací	hrací	k2eAgFnPc4d1
karty	karta	k1gFnPc4
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
v	v	k7c6
sadách	sada	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
karta	karta	k1gFnSc1
v	v	k7c6
sadě	sada	k1gFnSc6
má	mít	k5eAaImIp3nS
svoji	svůj	k3xOyFgFnSc4
barvu	barva	k1gFnSc4
a	a	k8xC
hodnotu	hodnota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
historie	historie	k1gFnSc2
</s>
<s>
Do	do	k7c2
Evropy	Evropa	k1gFnSc2
přes	přes	k7c4
Itálii	Itálie	k1gFnSc4
a	a	k8xC
Španělsko	Španělsko	k1gNnSc4
karetní	karetní	k2eAgFnSc2d1
hry	hra	k1gFnSc2
přinesli	přinést	k5eAaPmAgMnP
zřejmě	zřejmě	k6eAd1
Saracéni	Saracén	k1gMnPc1
pod	pod	k7c7
označením	označení	k1gNnSc7
naibbe	naibbat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Písemný	písemný	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
o	o	k7c6
popularitě	popularita	k1gFnSc6
hry	hra	k1gFnSc2
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
1376	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vyhláškou	vyhláška	k1gFnSc7
23	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
město	město	k1gNnSc1
Florencie	Florencie	k1gFnSc1
hru	hra	k1gFnSc4
zakazuje	zakazovat	k5eAaImIp3nS
pod	pod	k7c7
hrozbou	hrozba	k1gFnSc7
přísného	přísný	k2eAgNnSc2d1
potrestání	potrestání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
hru	hra	k1gFnSc4
v	v	k7c4
karty	karta	k1gFnPc4
přísně	přísně	k6eAd1
odsuzovala	odsuzovat	k5eAaImAgFnS
i	i	k9
katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3
typem	typ	k1gInSc7
karetního	karetní	k2eAgInSc2d1
listu	list	k1gInSc2
na	na	k7c6
evropském	evropský	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
jsou	být	k5eAaImIp3nP
karty	karta	k1gFnPc1
italské	italský	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svého	svůj	k3xOyFgInSc2
času	čas	k1gInSc2
byly	být	k5eAaImAgFnP
rozšířené	rozšířený	k2eAgFnPc1d1
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
postupně	postupně	k6eAd1
své	svůj	k3xOyFgNnSc4
výsadní	výsadní	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
ztrácely	ztrácet	k5eAaImAgInP
a	a	k8xC
dnes	dnes	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
nimi	on	k3xPp3gNnPc7
setkáme	setkat	k5eAaPmIp1nP
jen	jen	k9
v	v	k7c6
Itálii	Itálie	k1gFnSc6
a	a	k8xC
několika	několik	k4yIc7
málo	málo	k6eAd1
přilehlých	přilehlý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
(	(	kIx(
<g/>
jih	jih	k1gInSc4
Rakouska	Rakousko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Významné	významný	k2eAgNnSc1d1
postavení	postavení	k1gNnSc1
na	na	k7c6
českém	český	k2eAgInSc6d1
trhu	trh	k1gInSc6
s	s	k7c7
kartami	karta	k1gFnPc7
měla	mít	k5eAaImAgFnS
tradičně	tradičně	k6eAd1
vídeňská	vídeňský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Piatnik	Piatnik	k1gInSc1
založená	založený	k2eAgNnPc1d1
v	v	k7c6
roce	rok	k1gInSc6
1824	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
koupila	koupit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1898	#num#	k4
též	též	k6eAd1
karetní	karetní	k2eAgFnSc4d1
firmu	firma	k1gFnSc4
Ritter	Rittra	k1gFnPc2
<g/>
&	&	k?
<g/>
Cie	Cie	k1gMnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tzv.	tzv.	kA
český	český	k2eAgMnSc1d1
(	(	kIx(
<g/>
pražský	pražský	k2eAgInSc1d1
<g/>
)	)	kIx)
vzor	vzor	k1gInSc4
karet	kareta	k1gFnPc2
tiskla	tisknout	k5eAaImAgFnS
jak	jak	k6eAd1
firma	firma	k1gFnSc1
Ritter	Rittra	k1gFnPc2
<g/>
&	&	k?
<g/>
Cie	Cie	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k9
také	také	k9
další	další	k2eAgMnPc1d1
výrobci	výrobce	k1gMnPc1
v	v	k7c6
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc6
(	(	kIx(
<g/>
např.	např.	kA
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
a	a	k8xC
v	v	k7c6
Terstu	Terst	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgFnSc1d3
známá	známý	k2eAgFnSc1d1
hra	hra	k1gFnSc1
pražského	pražský	k2eAgInSc2d1
typu	typ	k1gInSc2
pochází	pocházet	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
z	z	k7c2
roku	rok	k1gInSc2
1800	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
střední	střední	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
jsou	být	k5eAaImIp3nP
typické	typický	k2eAgFnPc1d1
karty	karta	k1gFnPc1
německého	německý	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
tvoří	tvořit	k5eAaImIp3nP
mariášovou	mariášový	k2eAgFnSc4d1
sadu	sada	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typickou	typický	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
Vilém	Vilém	k1gMnSc1
Tell	Tell	k1gMnSc1
(	(	kIx(
<g/>
nebo	nebo	k8xC
doppeldeutsche	doppeldeutsche	k6eAd1
<g/>
)	)	kIx)
namaloval	namalovat	k5eAaPmAgMnS
kolem	kolem	k7c2
roku	rok	k1gInSc2
1835	#num#	k4
maďarský	maďarský	k2eAgMnSc1d1
malíř	malíř	k1gMnSc1
József	József	k1gMnSc1
Schneider	Schneider	k1gMnSc1
a	a	k8xC
v	v	k7c6
upravené	upravený	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
je	být	k5eAaImIp3nS
začala	začít	k5eAaPmAgFnS
produkovat	produkovat	k5eAaImF
vídeňská	vídeňský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Ferdinanda	Ferdinand	k1gMnSc2
Piatnika	Piatnik	k1gMnSc2
kolem	kolem	k7c2
roku	rok	k1gInSc2
1848	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
firma	firma	k1gFnSc1
ztratila	ztratit	k5eAaPmAgFnS
znárodňováním	znárodňování	k1gNnSc7
svoji	svůj	k3xOyFgFnSc4
pobočku	pobočka	k1gFnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrací	hrací	k2eAgFnPc4d1
karty	karta	k1gFnPc4
byly	být	k5eAaImAgFnP
poté	poté	k6eAd1
vyráběny	vyrábět	k5eAaImNgFnP
národním	národní	k2eAgInSc7d1
podnikem	podnik	k1gInSc7
Obchodní	obchodní	k2eAgFnSc2d1
Tiskárny	tiskárna	k1gFnSc2
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
založení	založení	k1gNnSc3
dceřiné	dceřiný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
„	„	k?
<g/>
Piatnik	Piatnik	k1gInSc1
Praha	Praha	k1gFnSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
pokračuje	pokračovat	k5eAaImIp3nS
v	v	k7c6
tradiční	tradiční	k2eAgFnSc6d1
vyrobě	vyroba	k1gFnSc6
hracích	hrací	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
výrobě	výroba	k1gFnSc6
karet	kareta	k1gFnPc2
s	s	k7c7
klasickými	klasický	k2eAgInPc7d1
motivy	motiv	k1gInPc7
nyní	nyní	k6eAd1
pokračuje	pokračovat	k5eAaImIp3nS
více	hodně	k6eAd2
společností	společnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Sady	sada	k1gFnPc1
hracích	hrací	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
</s>
<s>
Německé	německý	k2eAgNnSc1d1
</s>
<s>
Srdce	srdce	k1gNnSc1
</s>
<s>
Kule	kule	k1gFnSc1
</s>
<s>
Žaludy	žalud	k1gInPc1
</s>
<s>
Listy	list	k1gInPc1
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1
</s>
<s>
Srdce	srdce	k1gNnSc1
</s>
<s>
Kára	kára	k1gFnSc1
</s>
<s>
Kříže	kříž	k1gInPc1
</s>
<s>
Piky	pik	k1gInPc1
</s>
<s>
Italské	italský	k2eAgInPc1d1
</s>
<s>
Poháry	pohár	k1gInPc4
</s>
<s>
Mince	mince	k1gFnSc1
</s>
<s>
Hole	hole	k6eAd1
</s>
<s>
Meče	meč	k1gInPc1
</s>
<s>
Španělské	španělský	k2eAgNnSc1d1
</s>
<s>
Poháry	pohár	k1gInPc4
</s>
<s>
Mince	mince	k1gFnSc1
</s>
<s>
Hole	hole	k6eAd1
</s>
<s>
Meče	meč	k1gInPc1
</s>
<s>
Švýcarské	švýcarský	k2eAgFnPc1d1
</s>
<s>
Růže	růže	k1gFnSc1
</s>
<s>
Kule	kule	k1gFnSc1
</s>
<s>
Žaludy	žalud	k1gInPc1
</s>
<s>
Štíty	štít	k1gInPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc4
jednotlivých	jednotlivý	k2eAgInPc2d1
typů	typ	k1gInPc2
karetních	karetní	k2eAgInPc2d1
listů	list	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Mariášové	mariášový	k2eAgFnPc1d1
německé	německý	k2eAgFnPc1d1
dvouhlavé	dvouhlavý	k2eAgFnPc1d1
karty	karta	k1gFnPc1
Vilém	Vilém	k1gMnSc1
Tell	Tell	k1gMnSc1
</s>
<s>
Karty	karta	k1gFnPc1
francouzského	francouzský	k2eAgInSc2d1
typu	typ	k1gInSc2
</s>
<s>
Každá	každý	k3xTgFnSc1
karetní	karetní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
vyžaduje	vyžadovat	k5eAaImIp3nS
určitou	určitý	k2eAgFnSc4d1
karetní	karetní	k2eAgFnSc4d1
sadu	sada	k1gFnSc4
<g/>
,	,	kIx,
především	především	k9
pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
počet	počet	k1gInSc4
karet	kareta	k1gFnPc2
v	v	k7c6
sadě	sada	k1gFnSc6
a	a	k8xC
uspořádání	uspořádání	k1gNnSc6
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
na	na	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
existuje	existovat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
karetních	karetní	k2eAgInPc2d1
listů	list	k1gInPc2
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
svázána	svázán	k2eAgFnSc1d1
jen	jen	k9
s	s	k7c7
určitou	určitý	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
či	či	k8xC
lokalitou	lokalita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karetních	karetní	k2eAgFnPc2d1
sad	sada	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
dosáhly	dosáhnout	k5eAaPmAgFnP
celosvětové	celosvětový	k2eAgFnPc4d1
proslulosti	proslulost	k1gFnPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jen	jen	k9
několik	několik	k4yIc1
a	a	k8xC
v	v	k7c6
zásadě	zásada	k1gFnSc6
všechny	všechen	k3xTgInPc4
vycházejí	vycházet	k5eAaImIp3nP
ze	z	k7c2
stejného	stejný	k2eAgNnSc2d1
schématu	schéma	k1gNnSc2
(	(	kIx(
<g/>
4	#num#	k4
barvy	barva	k1gFnSc2
a	a	k8xC
8-14	8-14	k4
hodnot	hodnota	k1gFnPc2
s	s	k7c7
určenou	určený	k2eAgFnSc7d1
hierarchií	hierarchie	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karetní	karetní	k2eAgFnPc4d1
sady	sada	k1gFnPc4
lze	lze	k6eAd1
v	v	k7c6
zásadě	zásada	k1gFnSc6
rozdělit	rozdělit	k5eAaPmF
podle	podle	k7c2
jejich	jejich	k3xOp3gInSc2
původu	původ	k1gInSc2
nebo	nebo	k8xC
podle	podle	k7c2
počtu	počet	k1gInSc2
a	a	k8xC
hierarchie	hierarchie	k1gFnSc1
listů	list	k1gInPc2
v	v	k7c6
sadě	sada	k1gFnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
oba	dva	k4xCgInPc1
způsoby	způsob	k1gInPc1
rozdělení	rozdělení	k1gNnSc2
se	se	k3xPyFc4
překrývají	překrývat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
whistové	whistový	k2eAgFnPc1d1
karty	karta	k1gFnPc1
(	(	kIx(
<g/>
též	též	k9
pokerové	pokerový	k2eAgFnPc1d1
<g/>
)	)	kIx)
-	-	kIx~
sada	sada	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
52	#num#	k4
karet	kareta	k1gFnPc2
francouzského	francouzský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
rozdělených	rozdělený	k2eAgInPc2d1
do	do	k7c2
čtyř	čtyři	k4xCgFnPc2
barev	barva	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
hodnotami	hodnota	k1gFnPc7
2	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
J	J	kA
(	(	kIx(
<g/>
spodek	spodek	k1gInSc1
<g/>
,	,	kIx,
kluk	kluk	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Q	Q	kA
(	(	kIx(
<g/>
královna	královna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
K	k	k7c3
(	(	kIx(
<g/>
král	král	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
A	a	k8xC
(	(	kIx(
<g/>
eso	eso	k1gNnSc1
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
nabývat	nabývat	k5eAaImF
hodnoty	hodnota	k1gFnPc4
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
rummy	rumm	k1gInPc1
karty	karta	k1gFnSc2
(	(	kIx(
<g/>
též	též	k9
bridge	bridge	k6eAd1
<g/>
)	)	kIx)
-	-	kIx~
sada	sada	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
104	#num#	k4
karet	kareta	k1gFnPc2
francouzského	francouzský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
vznikne	vzniknout	k5eAaPmIp3nS
ze	z	k7c2
dvou	dva	k4xCgFnPc2
whistových	whistový	k2eAgFnPc2d1
sad	sada	k1gFnPc2
(	(	kIx(
<g/>
jejichž	jejichž	k3xOyRp3gFnPc4
rubové	rubový	k2eAgFnPc4d1
strany	strana	k1gFnPc4
bývají	bývat	k5eAaImIp3nP
barevně	barevně	k6eAd1
odlišeny	odlišen	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
kanastové	kanastová	k1gFnPc1
(	(	kIx(
<g/>
též	též	k9
žolíkové	žolíkové	k?
<g/>
)	)	kIx)
karty	karta	k1gFnSc2
-	-	kIx~
sada	sada	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
108	#num#	k4
karet	kareta	k1gFnPc2
francouzského	francouzský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
vznikne	vzniknout	k5eAaPmIp3nS
ze	z	k7c2
sady	sada	k1gFnSc2
rummy	rumma	k1gFnSc2
karet	kareta	k1gFnPc2
přidáním	přidání	k1gNnSc7
4	#num#	k4
žolíků	žolík	k1gInPc2
-	-	kIx~
karet	kareta	k1gFnPc2
se	s	k7c7
speciální	speciální	k2eAgFnSc7d1
funkcí	funkce	k1gFnSc7
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
nepatří	patřit	k5eNaImIp3nP
k	k	k7c3
žádné	žádný	k3yNgFnSc3
barvě	barva	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
pasiánsové	pasiánsový	k2eAgFnPc1d1
karty	karta	k1gFnPc1
(	(	kIx(
<g/>
též	též	k9
solitaire	solitair	k1gInSc5
<g/>
)	)	kIx)
-	-	kIx~
odvozené	odvozený	k2eAgInPc1d1
z	z	k7c2
kanastových	kanastův	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
108	#num#	k4
listů	list	k1gInPc2
francouzského	francouzský	k2eAgInSc2d1
typu	typ	k1gInSc2
včetně	včetně	k7c2
4	#num#	k4
jokerů	joker	k1gMnPc2
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yIgFnPc7,k3yRgFnPc7,k3yQgFnPc7
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
nehraje	hrát	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
oproti	oproti	k7c3
rummy	rumm	k1gInPc1
kartám	karta	k1gFnPc3
menší	malý	k2eAgInSc4d2
rozměr	rozměr	k1gInSc4
(	(	kIx(
<g/>
44	#num#	k4
<g/>
x	x	k?
<g/>
67	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
lépe	dobře	k6eAd2
vešly	vejít	k5eAaPmAgFnP
na	na	k7c4
stůl	stůl	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
piketové	piketový	k2eAgFnPc1d1
karty	karta	k1gFnPc1
-	-	kIx~
sada	sada	k1gFnSc1
32	#num#	k4
karet	kareta	k1gFnPc2
francouzského	francouzský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
hodnotami	hodnota	k1gFnPc7
7	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
J	J	kA
<g/>
,	,	kIx,
Q	Q	kA
<g/>
,	,	kIx,
K	K	kA
<g/>
,	,	kIx,
A.	A.	kA
Hodnoty	hodnota	k1gFnPc1
nejsou	být	k5eNaImIp3nP
na	na	k7c6
kartách	karta	k1gFnPc6
uvedeny	uveden	k2eAgFnPc1d1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
vyobrazeny	vyobrazit	k5eAaPmNgFnP
počtem	počet	k1gInSc7
patřičných	patřičný	k2eAgInPc2d1
symbolů	symbol	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
mariášové	mariášový	k2eAgFnPc1d1
karty	karta	k1gFnPc1
-	-	kIx~
sada	sada	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
32	#num#	k4
karet	kareta	k1gFnPc2
německého	německý	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
hodnotami	hodnota	k1gFnPc7
7	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
spodek	spodek	k1gInSc1
<g/>
,	,	kIx,
svršek	svršek	k1gInSc1
<g/>
,	,	kIx,
král	král	k1gMnSc1
<g/>
,	,	kIx,
eso	eso	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikne	vzniknout	k5eAaPmIp3nS
z	z	k7c2
whistové	whistový	k2eAgFnSc2d1
sady	sada	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
jsou	být	k5eAaImIp3nP
odebrány	odebrán	k2eAgFnPc1d1
karty	karta	k1gFnPc1
nízkých	nízký	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
původu	původ	k1gInSc2
jsou	být	k5eAaImIp3nP
nejrozšířenější	rozšířený	k2eAgFnPc1d3
karty	karta	k1gFnPc1
francouzského	francouzský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
se	se	k3xPyFc4
vytvářejí	vytvářet	k5eAaImIp3nP
whistové	whistový	k2eAgFnPc1d1
<g/>
,	,	kIx,
rummy	rumma	k1gFnPc1
a	a	k8xC
kanastové	kanastový	k2eAgFnPc1d1
sady	sada	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žolík	žolík	k1gInSc1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
do	do	k7c2
těchto	tento	k3xDgFnPc2
karet	kareta	k1gFnPc2
přidán	přidán	k2eAgMnSc1d1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
,	,	kIx,
nejstarší	starý	k2eAgFnSc1d3
zmínka	zmínka	k1gFnSc1
je	být	k5eAaImIp3nS
o	o	k7c6
něm	on	k3xPp3gInSc6
z	z	k7c2
roku	rok	k1gInSc2
1875	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhé	druhý	k4xOgNnSc1
v	v	k7c6
pořadí	pořadí	k1gNnSc6
jsou	být	k5eAaImIp3nP
v	v	k7c6
tomto	tento	k3xDgInSc6
směru	směr	k1gInSc6
karty	karta	k1gFnSc2
španělského	španělský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yRgFnPc7,k3yQgFnPc7,k3yIgFnPc7
se	se	k3xPyFc4
dodnes	dodnes	k6eAd1
setkáme	setkat	k5eAaPmIp1nP
v	v	k7c6
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
pod	pod	k7c7
španělským	španělský	k2eAgInSc7d1
či	či	k8xC
portugalským	portugalský	k2eAgInSc7d1
vlivem	vliv	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
francouzských	francouzský	k2eAgInPc2d1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
názvy	název	k1gInPc7
hodnot	hodnota	k1gFnPc2
a	a	k8xC
barev	barva	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
uspořádání	uspořádání	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
zásadě	zásada	k1gFnSc6
stejné	stejný	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s>
Uspořádání	uspořádání	k1gNnSc1
italských	italský	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
je	být	k5eAaImIp3nS
opět	opět	k6eAd1
podobné	podobný	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
u	u	k7c2
francouzského	francouzský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
ovšem	ovšem	k9
týká	týkat	k5eAaImIp3nS
jen	jen	k9
klasických	klasický	k2eAgFnPc2d1
karetních	karetní	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Italská	italský	k2eAgFnSc1d1
symbolika	symbolika	k1gFnSc1
se	se	k3xPyFc4
udržela	udržet	k5eAaPmAgFnS
a	a	k8xC
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
používána	používán	k2eAgFnSc1d1
na	na	k7c6
tarotových	tarotův	k2eAgFnPc6d1
kartách	karta	k1gFnPc6
sloužících	sloužící	k2eAgFnPc6d1
k	k	k7c3
předpovídání	předpovídání	k1gNnSc3
osudu	osud	k1gInSc2
-	-	kIx~
tzv.	tzv.	kA
tarot	tarot	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tarotové	Tarotový	k2eAgFnPc4d1
karty	karta	k1gFnPc4
přibírají	přibírat	k5eAaImIp3nP
do	do	k7c2
sady	sada	k1gFnSc2
několik	několik	k4yIc1
dalších	další	k2eAgFnPc2d1
speciálních	speciální	k2eAgFnPc2d1
figur	figura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
se	se	k3xPyFc4
karty	karta	k1gFnSc2
německého	německý	k2eAgInSc2d1
typu	typ	k1gInSc2
zpravidla	zpravidla	k6eAd1
označují	označovat	k5eAaImIp3nP
jako	jako	k9
mariášky	mariášek	k1gInPc1
nebo	nebo	k8xC
na	na	k7c6
prší	pršet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Setkáme	setkat	k5eAaPmIp1nP
se	se	k3xPyFc4
s	s	k7c7
nimi	on	k3xPp3gMnPc7
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
Rakousku	Rakousko	k1gNnSc6
<g/>
,	,	kIx,
Maďarsku	Maďarsko	k1gNnSc6
<g/>
,	,	kIx,
Polsku	Polsko	k1gNnSc6
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
a	a	k8xC
místně	místně	k6eAd1
i	i	k9
v	v	k7c6
některých	některý	k3yIgFnPc6
dalších	další	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nejtypičtější	typický	k2eAgFnSc6d3
výtvarné	výtvarný	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
Vilém	Vilém	k1gMnSc1
Tell	Tell	k1gMnSc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
esech	eso	k1gNnPc6
vyobrazeny	vyobrazit	k5eAaPmNgFnP
alegorie	alegorie	k1gFnPc1
ročních	roční	k2eAgNnPc2d1
období	období	k1gNnPc2
a	a	k8xC
na	na	k7c6
kartách	karta	k1gFnPc6
svršků	svršek	k1gInPc2
a	a	k8xC
spodků	spodek	k1gInPc2
postavy	postava	k1gFnSc2
z	z	k7c2
dramatu	drama	k1gNnSc2
Friedricha	Friedrich	k1gMnSc2
Schillera	Schiller	k1gMnSc2
Vilém	Vilém	k1gMnSc1
Tell	Tell	k1gMnSc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
Tella	Tello	k1gNnSc2
samotného	samotný	k2eAgInSc2d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
namalován	namalovat	k5eAaPmNgInS
na	na	k7c6
kartě	karta	k1gFnSc6
žaludského	žaludský	k2eAgInSc2d1
svrška	svršek	k1gInSc2
<g/>
)	)	kIx)
-	-	kIx~
v	v	k7c6
původní	původní	k2eAgFnSc6d1
Schneiderově	Schneiderův	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
byly	být	k5eAaImAgInP
pojmenovány	pojmenovat	k5eAaPmNgInP
jako	jako	k8xS,k8xC
postavy	postava	k1gFnPc1
z	z	k7c2
maďarské	maďarský	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
ale	ale	k9
nebyly	být	k5eNaImAgFnP
schváleny	schválit	k5eAaPmNgFnP
z	z	k7c2
důvodů	důvod	k1gInPc2
cenzury	cenzura	k1gFnSc2
a	a	k8xC
proto	proto	k8xC
byly	být	k5eAaImAgFnP
pro	pro	k7c4
distribuci	distribuce	k1gFnSc4
změněny	změněn	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ostatních	ostatní	k2eAgFnPc6d1
kartách	karta	k1gFnPc6
jsou	být	k5eAaImIp3nP
zobrazeny	zobrazit	k5eAaPmNgInP
různé	různý	k2eAgInPc1d1
přírodní	přírodní	k2eAgInPc1d1
a	a	k8xC
historické	historický	k2eAgInPc4d1
motivy	motiv	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgMnSc1d1
(	(	kIx(
<g/>
nebo	nebo	k8xC
pražský	pražský	k2eAgInSc1d1
<g/>
)	)	kIx)
vzor	vzor	k1gInSc1
připomíná	připomínat	k5eAaImIp3nS
německý	německý	k2eAgInSc1d1
jižní	jižní	k2eAgInSc1d1
či	či	k8xC
salcburský	salcburský	k2eAgInSc1d1
typ	typ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
spodci	spodce	k1gMnPc1
a	a	k8xC
svršci	svršek	k1gMnPc1
drží	držet	k5eAaImIp3nP
meče	meč	k1gInPc4
či	či	k8xC
hole	hole	k6eAd1
a	a	k8xC
v	v	k7c6
případě	případ	k1gInSc6
zelené	zelený	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
hudební	hudební	k2eAgFnPc1d1
nástroje	nástroj	k1gInPc4
<g/>
,	,	kIx,
karty	karta	k1gFnPc4
také	také	k9
kromě	kromě	k7c2
desítky	desítka	k1gFnSc2
neobsahují	obsahovat	k5eNaImIp3nP
číselné	číselný	k2eAgInPc1d1
indexy	index	k1gInPc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c6
případě	případ	k1gInSc6
salcburského	salcburský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karty	karta	k1gFnPc1
ovšem	ovšem	k9
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
u	u	k7c2
německého	německý	k2eAgInSc2d1
severního	severní	k2eAgInSc2d1
typu	typ	k1gInSc2
mají	mít	k5eAaImIp3nP
na	na	k7c6
žaludovém	žaludový	k2eAgNnSc6d1
esu	eso	k1gNnSc6
zvíře	zvíře	k1gNnSc1
a	a	k8xC
neobsahují	obsahovat	k5eNaImIp3nP
šestky	šestka	k1gFnPc1
<g/>
,	,	kIx,
skládají	skládat	k5eAaImIp3nP
se	se	k3xPyFc4
tak	tak	k9
pouze	pouze	k6eAd1
ze	z	k7c2
32	#num#	k4
karet	kareta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obrázky	obrázek	k1gInPc7
připomínají	připomínat	k5eAaImIp3nP
jedny	jeden	k4xCgFnPc1
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
německých	německý	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
Heinricha	Heinrich	k1gMnSc2
Hauka	Hauek	k1gMnSc2
ze	z	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
dalších	další	k2eAgMnPc2d1
pozdějších	pozdní	k2eAgMnPc2d2
umělců	umělec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhled	vzhled	k1gInSc1
karty	karta	k1gFnSc2
</s>
<s>
Běžná	běžný	k2eAgFnSc1d1
hrací	hrací	k2eAgFnSc1d1
karta	karta	k1gFnSc1
francouzského	francouzský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
:	:	kIx,
barvu	barva	k1gFnSc4
určují	určovat	k5eAaImIp3nP
symboly	symbol	k1gInPc4
(	(	kIx(
<g/>
srdce	srdce	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hodnotu	hodnota	k1gFnSc4
určuje	určovat	k5eAaImIp3nS
počet	počet	k1gInSc1
symbolů	symbol	k1gInPc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Běžná	běžný	k2eAgFnSc1d1
hrací	hrací	k2eAgFnSc1d1
karta	karta	k1gFnSc1
je	být	k5eAaImIp3nS
nejčastěji	často	k6eAd3
obdélný	obdélný	k2eAgInSc1d1
list	list	k1gInSc1
tuhého	tuhý	k2eAgInSc2d1
papíru	papír	k1gInSc2
s	s	k7c7
oblými	oblý	k2eAgInPc7d1
rohy	roh	k1gInPc7
o	o	k7c6
výšce	výška	k1gFnSc6
asi	asi	k9
10	#num#	k4
cm	cm	kA
a	a	k8xC
zhruba	zhruba	k6eAd1
poloviční	poloviční	k2eAgFnSc6d1
šířce	šířka	k1gFnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
rubová	rubový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
u	u	k7c2
všech	všecek	k3xTgFnPc2
karet	kareta	k1gFnPc2
stejná	stejný	k2eAgFnSc1d1
<g/>
,	,	kIx,
na	na	k7c6
lícní	lícní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
obrázky	obrázek	k1gInPc7
a	a	k8xC
symboly	symbol	k1gInPc7
<g/>
,	,	kIx,
podle	podle	k7c2
nichž	jenž	k3xRgInPc2
se	se	k3xPyFc4
určuje	určovat	k5eAaImIp3nS
barva	barva	k1gFnSc1
a	a	k8xC
hodnota	hodnota	k1gFnSc1
karty	karta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
mnohé	mnohý	k2eAgFnPc1d1
karetní	karetní	k2eAgFnPc1d1
sady	sada	k1gFnPc1
se	se	k3xPyFc4
v	v	k7c6
některých	některý	k3yIgInPc6
ohledech	ohled	k1gInPc6
odlišují	odlišovat	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
materiál	materiál	k1gInSc1
<g/>
:	:	kIx,
kromě	kromě	k7c2
karet	kareta	k1gFnPc2
z	z	k7c2
tuhého	tuhý	k2eAgInSc2d1
papíru	papír	k1gInSc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
karty	karta	k1gFnPc4
např.	např.	kA
dřevěné	dřevěný	k2eAgFnPc4d1
nebo	nebo	k8xC
plastové	plastový	k2eAgFnPc4d1
<g/>
,	,	kIx,
</s>
<s>
tvar	tvar	k1gInSc1
<g/>
:	:	kIx,
karty	karta	k1gFnPc1
nemusí	muset	k5eNaImIp3nP
být	být	k5eAaImF
jen	jen	k9
obdélné	obdélný	k2eAgInPc1d1
<g/>
,	,	kIx,
například	například	k6eAd1
na	na	k7c4
pexeso	pexeso	k1gNnSc4
se	se	k3xPyFc4
častěji	často	k6eAd2
užívají	užívat	k5eAaImIp3nP
karty	karta	k1gFnPc1
čtvercové	čtvercový	k2eAgFnPc1d1
<g/>
,	,	kIx,
</s>
<s>
rozměry	rozměr	k1gInPc1
karet	kareta	k1gFnPc2
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
značně	značně	k6eAd1
lišit	lišit	k5eAaImF
<g/>
,	,	kIx,
</s>
<s>
rub	rub	k1gInSc1
<g/>
:	:	kIx,
rubová	rubový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
může	moct	k5eAaImIp3nS
také	také	k9
sloužit	sloužit	k5eAaImF
jako	jako	k9
rozlišovací	rozlišovací	k2eAgInSc4d1
znak	znak	k1gInSc4
<g/>
,	,	kIx,
takže	takže	k8xS
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
u	u	k7c2
všech	všecek	k3xTgFnPc2
karet	kareta	k1gFnPc2
v	v	k7c6
sadě	sada	k1gFnSc6
stejná	stejná	k1gFnSc1
<g/>
,	,	kIx,
např.	např.	kA
„	„	k?
<g/>
kanastové	kanastová	k1gFnSc6
<g/>
“	“	k?
jsou	být	k5eAaImIp3nP
barvou	barva	k1gFnSc7
rubové	rubový	k2eAgFnPc1d1
strany	strana	k1gFnPc1
rozlišeny	rozlišit	k5eAaPmNgFnP
na	na	k7c4
dvě	dva	k4xCgFnPc4
poloviny	polovina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Karty	karta	k1gFnPc1
bývají	bývat	k5eAaImIp3nP
nejčastěji	často	k6eAd3
rozděleny	rozdělit	k5eAaPmNgInP
do	do	k7c2
4	#num#	k4
barev	barva	k1gFnPc2
a	a	k8xC
8-14	8-14	k4
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barvu	barva	k1gFnSc4
karty	karta	k1gFnSc2
označují	označovat	k5eAaImIp3nP
speciální	speciální	k2eAgInPc4d1
symboly	symbol	k1gInPc4
<g/>
,	,	kIx,
hodnota	hodnota	k1gFnSc1
karty	karta	k1gFnSc2
je	být	k5eAaImIp3nS
vyjádřena	vyjádřit	k5eAaPmNgFnS
vyobrazením	vyobrazení	k1gNnSc7
<g/>
,	,	kIx,
počtem	počet	k1gInSc7
symbolů	symbol	k1gInPc2
a	a	k8xC
číslem	číslo	k1gNnSc7
nebo	nebo	k8xC
písmenem	písmeno	k1gNnSc7
-	-	kIx~
hodnoty	hodnota	k1gFnSc2
se	se	k3xPyFc4
ještě	ještě	k6eAd1
rozdělují	rozdělovat	k5eAaImIp3nP
na	na	k7c4
čísla	číslo	k1gNnPc4
a	a	k8xC
figury	figura	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Speciální	speciální	k2eAgFnPc1d1
hrací	hrací	k2eAgFnPc1d1
karty	karta	k1gFnPc1
</s>
<s>
Kromě	kromě	k7c2
běžných	běžný	k2eAgFnPc2d1
karetních	karetní	k2eAgFnPc2d1
sad	sada	k1gFnPc2
existuje	existovat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
jiných	jiný	k2eAgFnPc2d1
společenských	společenský	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
speciálních	speciální	k2eAgFnPc2d1
karetních	karetní	k2eAgFnPc2d1
sad	sada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
běžných	běžný	k2eAgFnPc2d1
hracích	hrací	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
je	být	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
lze	lze	k6eAd1
využít	využít	k5eAaPmF
pouze	pouze	k6eAd1
k	k	k7c3
jediné	jediný	k2eAgFnSc3d1
konkrétní	konkrétní	k2eAgFnSc3d1
hře	hra	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
takové	takový	k3xDgFnPc1
hry	hra	k1gFnPc1
se	se	k3xPyFc4
hrají	hrát	k5eAaImIp3nP
pouze	pouze	k6eAd1
s	s	k7c7
hracími	hrací	k2eAgFnPc7d1
kartami	karta	k1gFnPc7
<g/>
,	,	kIx,
jiné	jiný	k2eAgFnPc1d1
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
i	i	k9
další	další	k2eAgFnPc4d1
pomůcky	pomůcka	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
hrací	hrací	k2eAgFnSc4d1
desku	deska	k1gFnSc4
<g/>
,	,	kIx,
žetony	žeton	k1gInPc4
<g/>
,	,	kIx,
figurky	figurka	k1gFnPc4
atd.	atd.	kA
</s>
<s>
Vedle	vedle	k7c2
toho	ten	k3xDgNnSc2
existují	existovat	k5eAaImIp3nP
i	i	k9
karty	karta	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
ke	k	k7c3
hraní	hraní	k1gNnSc3
neslouží	sloužit	k5eNaImIp3nP
vůbec	vůbec	k9
-	-	kIx~
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
především	především	k9
věštecké	věštecký	k2eAgFnPc1d1
karty	karta	k1gFnPc1
(	(	kIx(
<g/>
tarot	tarot	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
sběratelské	sběratelský	k2eAgFnPc4d1
karty	karta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
AUTORSKÝ	autorský	k2eAgInSc4d1
KOLEKTIV	kolektiv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sběratelství	sběratelství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Sběratelství	sběratelství	k1gNnSc2
lidové	lidový	k2eAgNnSc1d1
/	/	kIx~
28	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Mariášek	mariášek	k1gInSc1
<g/>
,	,	kIx,
mariáš	mariáš	k1gInSc1
<g/>
:	:	kIx,
Nejslavnější	slavný	k2eAgFnSc1d3
karetní	karetní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
má	mít	k5eAaImIp3nS
své	svůj	k3xOyFgInPc4
kořeny	kořen	k1gInPc4
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
100	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
zahraniční	zahraniční	k2eAgFnSc4d1
zajímavost	zajímavost	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
OMASTA	omasta	k1gFnSc1
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pasiáns	pasiáns	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
:	:	kIx,
AVOMA	AVOMA	kA
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901124	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Pasiánsový	Pasiánsový	k2eAgInSc1d1
slovníček	slovníček	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Bohemian	bohemian	k1gInSc1
Pattern	Pattern	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
of	of	k?
Playing	Playing	k1gInSc1
Cards	Cards	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
typů	typ	k1gInPc2
karetních	karetní	k2eAgInPc2d1
listů	list	k1gInPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
hrací	hrací	k2eAgFnSc1d1
karta	karta	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Karty	karta	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Přehled	přehled	k1gInSc1
karetních	karetní	k2eAgInPc2d1
listů	list	k1gInPc2
Evropa	Evropa	k1gFnSc1
</s>
<s>
Německé	německý	k2eAgFnPc1d1
karty	karta	k1gFnPc1
•	•	k?
Francouzské	francouzský	k2eAgFnPc1d1
karty	karta	k1gFnPc1
•	•	k?
Italské	italský	k2eAgFnPc1d1
karty	karta	k1gFnPc1
•	•	k?
Španělské	španělský	k2eAgFnPc1d1
karty	karta	k1gFnPc1
•	•	k?
Švýcarské	švýcarský	k2eAgFnPc1d1
karty	karta	k1gFnPc1
Asie	Asie	k1gFnSc2
</s>
<s>
Arabské	arabský	k2eAgFnPc1d1
karty	karta	k1gFnPc1
•	•	k?
Čínské	čínský	k2eAgFnPc1d1
karty	karta	k1gFnPc1
•	•	k?
Indické	indický	k2eAgFnPc1d1
karty	karta	k1gFnPc1
•	•	k?
Japonské	japonský	k2eAgFnPc1d1
karty	karta	k1gFnPc1
•	•	k?
Korejské	korejský	k2eAgFnPc1d1
karty	karta	k1gFnPc1
Ostatní	ostatní	k2eAgFnPc1d1
</s>
<s>
Kukaččí	kukaččí	k2eAgFnPc1d1
karty	karta	k1gFnPc1
•	•	k?
Tarokové	tarokový	k2eAgFnPc1d1
karty	karta	k1gFnPc1
•	•	k?
Věštecké	věštecký	k2eAgFnPc1d1
karty	karta	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4056228-1	4056228-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
90004763	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
90004763	#num#	k4
</s>
