<s>
Sluch	sluch	k1gInSc1	sluch
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc4	schopnost
vnímat	vnímat	k5eAaImF	vnímat
zvuky	zvuk	k1gInPc4	zvuk
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
společná	společný	k2eAgFnSc1d1	společná
všem	všecek	k3xTgMnPc3	všecek
vyšším	vysoký	k2eAgMnPc3d2	vyšší
živočichům	živočich	k1gMnPc3	živočich
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
vybaveni	vybavit	k5eAaPmNgMnP	vybavit
speciálním	speciální	k2eAgInSc7d1	speciální
smyslovým	smyslový	k2eAgInSc7d1	smyslový
orgánem	orgán	k1gInSc7	orgán
<g/>
,	,	kIx,	,
uchem	ucho	k1gNnSc7	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
zvukovodem	zvukovod	k1gInSc7	zvukovod
naráží	narážet	k5eAaPmIp3nS	narážet
do	do	k7c2	do
bubínku	bubínek	k1gInSc2	bubínek
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
rozechvěje	rozechvět	k5eAaPmIp3nS	rozechvět
a	a	k8xC	a
vibrace	vibrace	k1gFnSc1	vibrace
přenáší	přenášet	k5eAaImIp3nS	přenášet
přes	přes	k7c4	přes
kladívko	kladívko	k1gNnSc4	kladívko
<g/>
,	,	kIx,	,
kovadlinku	kovadlinka	k1gFnSc4	kovadlinka
a	a	k8xC	a
třmínek	třmínek	k1gInSc4	třmínek
do	do	k7c2	do
hlemýždě	hlemýžď	k1gMnSc2	hlemýžď
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
na	na	k7c4	na
vibrace	vibrace	k1gFnPc4	vibrace
reagují	reagovat	k5eAaBmIp3nP	reagovat
smyslové	smyslový	k2eAgFnPc4d1	smyslová
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
zachyceném	zachycený	k2eAgInSc6d1	zachycený
zvuku	zvuk	k1gInSc6	zvuk
vedou	vést	k5eAaImIp3nP	vést
pomocí	pomocí	k7c2	pomocí
sluchového	sluchový	k2eAgInSc2d1	sluchový
nervu	nerv	k1gInSc2	nerv
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
zpracování	zpracování	k1gNnSc3	zpracování
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Lékařský	lékařský	k2eAgInSc1d1	lékařský
obor	obor	k1gInSc1	obor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
lidský	lidský	k2eAgInSc4d1	lidský
sluch	sluch	k1gInSc4	sluch
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
audiologie	audiologie	k1gFnSc1	audiologie
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
dokáže	dokázat	k5eAaPmIp3nS	dokázat
slyšet	slyšet	k5eAaImF	slyšet
frekvence	frekvence	k1gFnSc1	frekvence
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
20	[number]	k4	20
Hz	Hz	kA	Hz
až	až	k9	až
20	[number]	k4	20
kHz	khz	kA	khz
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
dokážou	dokázat	k5eAaPmIp3nP	dokázat
slyšet	slyšet	k5eAaImF	slyšet
frekvence	frekvence	k1gFnPc4	frekvence
mírně	mírně	k6eAd1	mírně
nad	nad	k7c4	nad
20	[number]	k4	20
kHz	khz	kA	khz
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
s	s	k7c7	s
přibývajícím	přibývající	k2eAgInSc7d1	přibývající
věkem	věk	k1gInSc7	věk
se	se	k3xPyFc4	se
horní	horní	k2eAgFnSc1d1	horní
hranice	hranice	k1gFnSc1	hranice
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
lidský	lidský	k2eAgInSc1d1	lidský
hlas	hlas	k1gInSc1	hlas
má	mít	k5eAaImIp3nS	mít
frekvenci	frekvence	k1gFnSc4	frekvence
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
200	[number]	k4	200
Hz	Hz	kA	Hz
do	do	k7c2	do
800	[number]	k4	800
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
schopnost	schopnost	k1gFnSc1	schopnost
rozeznávat	rozeznávat	k5eAaImF	rozeznávat
zabarvení	zabarvení	k1gNnSc4	zabarvení
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
tóny	tón	k1gInPc1	tón
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
různí	různý	k2eAgMnPc1d1	různý
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
relativní	relativní	k2eAgInSc4d1	relativní
sluch	sluch	k1gInSc4	sluch
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
asi	asi	k9	asi
jeden	jeden	k4xCgMnSc1	jeden
člověk	člověk	k1gMnSc1	člověk
z	z	k7c2	z
10	[number]	k4	10
000	[number]	k4	000
má	mít	k5eAaImIp3nS	mít
absolutní	absolutní	k2eAgInSc4d1	absolutní
sluch	sluch	k1gInSc4	sluch
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dokáže	dokázat	k5eAaPmIp3nS	dokázat
rozlišit	rozlišit	k5eAaPmF	rozlišit
i	i	k9	i
nepatrné	nepatrný	k2eAgInPc4d1	nepatrný
rozdíly	rozdíl	k1gInPc4	rozdíl
v	v	k7c6	v
tónech	tón	k1gInPc6	tón
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
uši	ucho	k1gNnPc4	ucho
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
sluch	sluch	k1gInSc1	sluch
orientaci	orientace	k1gFnSc4	orientace
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
podstatně	podstatně	k6eAd1	podstatně
hůře	zle	k6eAd2	zle
než	než	k8xS	než
zrak	zrak	k1gInSc4	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
původ	původ	k1gInSc4	původ
hlubokých	hluboký	k2eAgInPc2d1	hluboký
nebo	nebo	k8xC	nebo
táhlých	táhlý	k2eAgInPc2d1	táhlý
tónů	tón	k1gInPc2	tón
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
sluchu	sluch	k1gInSc2	sluch
obtížně	obtížně	k6eAd1	obtížně
lokalizuje	lokalizovat	k5eAaBmIp3nS	lokalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
šelmy	šelma	k1gFnPc1	šelma
nebo	nebo	k8xC	nebo
přežvýkavci	přežvýkavec	k1gMnPc1	přežvýkavec
<g/>
,	,	kIx,	,
dokážou	dokázat	k5eAaPmIp3nP	dokázat
natáčet	natáčet	k5eAaImF	natáčet
ušní	ušní	k2eAgInPc4d1	ušní
boltce	boltec	k1gInPc4	boltec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jejich	jejich	k3xOp3gFnSc4	jejich
schopnost	schopnost	k1gFnSc4	schopnost
lokalizace	lokalizace	k1gFnSc2	lokalizace
původu	původ	k1gInSc2	původ
zvuku	zvuk	k1gInSc2	zvuk
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
<g/>
.	.	kIx.	.
</s>
<s>
Omezená	omezený	k2eAgFnSc1d1	omezená
schopnost	schopnost	k1gFnSc1	schopnost
sluchu	sluch	k1gInSc2	sluch
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nedoslýchavost	nedoslýchavost	k1gFnSc1	nedoslýchavost
<g/>
,	,	kIx,	,
úplná	úplný	k2eAgFnSc1d1	úplná
ztráta	ztráta	k1gFnSc1	ztráta
sluchu	sluch	k1gInSc2	sluch
je	být	k5eAaImIp3nS	být
hluchota	hluchota	k1gFnSc1	hluchota
<g/>
.	.	kIx.	.
</s>
<s>
Poškození	poškození	k1gNnSc1	poškození
sluchu	sluch	k1gInSc2	sluch
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vrozené	vrozený	k2eAgInPc4d1	vrozený
nebo	nebo	k8xC	nebo
získané	získaný	k2eAgInPc4d1	získaný
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
operace	operace	k1gFnSc2	operace
<g/>
,	,	kIx,	,
působení	působení	k1gNnSc1	působení
nadměrného	nadměrný	k2eAgInSc2d1	nadměrný
hluku	hluk	k1gInSc2	hluk
atd.	atd.	kA	atd.
K	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
sluchu	sluch	k1gInSc2	sluch
zpravidla	zpravidla	k6eAd1	zpravidla
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
hladina	hladina	k1gFnSc1	hladina
akustického	akustický	k2eAgInSc2d1	akustický
tlaku	tlak	k1gInSc2	tlak
překročí	překročit	k5eAaPmIp3nS	překročit
140	[number]	k4	140
decibelů	decibel	k1gInPc2	decibel
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
trvale	trvale	k6eAd1	trvale
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
hlasitostí	hlasitost	k1gFnSc7	hlasitost
pouze	pouze	k6eAd1	pouze
85	[number]	k4	85
decibelů	decibel	k1gInPc2	decibel
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
si	se	k3xPyFc3	se
rovněž	rovněž	k9	rovněž
sluch	sluch	k1gInSc4	sluch
poškodit	poškodit	k5eAaPmF	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
poměrně	poměrně	k6eAd1	poměrně
nepřesně	přesně	k6eNd1	přesně
lokalizuje	lokalizovat	k5eAaBmIp3nS	lokalizovat
původ	původ	k1gInSc4	původ
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
podléhá	podléhat	k5eAaImIp3nS	podléhat
sluchovým	sluchový	k2eAgFnPc3d1	sluchová
halucinacím	halucinace	k1gFnPc3	halucinace
-	-	kIx~	-
například	například	k6eAd1	například
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdo	někdo	k3yInSc1	někdo
chodí	chodit	k5eAaImIp3nS	chodit
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
bytě	byt	k1gInSc6	byt
<g/>
,	,	kIx,	,
že	že	k8xS	že
slyší	slyšet	k5eAaImIp3nS	slyšet
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
šepotání	šepotání	k1gNnSc2	šepotání
apod.	apod.	kA	apod.
Zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
jevem	jev	k1gInSc7	jev
je	být	k5eAaImIp3nS	být
akuse	akuse	k1gFnSc1	akuse
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
člověk	člověk	k1gMnSc1	člověk
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvnějšku	zvnějšku	k6eAd1	zvnějšku
slyší	slyšet	k5eAaImIp3nP	slyšet
hrát	hrát	k5eAaImF	hrát
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
však	však	k9	však
vědom	vědom	k2eAgMnSc1d1	vědom
přeludu	přelud	k1gInSc2	přelud
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c6	o
pseudohalucinaci	pseudohalucinace	k1gFnSc6	pseudohalucinace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
vzácný	vzácný	k2eAgInSc4d1	vzácný
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
častěji	často	k6eAd2	často
mohou	moct	k5eAaImIp3nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
lidé	člověk	k1gMnPc1	člověk
se	s	k7c7	s
sluchovou	sluchový	k2eAgFnSc7d1	sluchová
vadou	vada	k1gFnSc7	vada
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
také	také	k9	také
s	s	k7c7	s
určitými	určitý	k2eAgFnPc7d1	určitá
hudebními	hudební	k2eAgFnPc7d1	hudební
vlohami	vloha	k1gFnPc7	vloha
<g/>
.	.	kIx.	.
</s>
<s>
Vyvolání	vyvolání	k1gNnSc1	vyvolání
jevu	jev	k1gInSc2	jev
také	také	k9	také
podporuje	podporovat	k5eAaImIp3nS	podporovat
hlučné	hlučný	k2eAgNnSc4d1	hlučné
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
únava	únava	k1gFnSc1	únava
a	a	k8xC	a
stresující	stresující	k2eAgFnSc1d1	stresující
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Akusím	Akusí	k1gNnSc7	Akusí
hudby	hudba	k1gFnSc2	hudba
často	často	k6eAd1	často
podléhal	podléhat	k5eAaImAgMnS	podléhat
například	například	k6eAd1	například
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
člověk	člověk	k1gMnSc1	člověk
slyší	slyšet	k5eAaImIp3nS	slyšet
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
člověku	člověk	k1gMnSc3	člověk
příjemná	příjemný	k2eAgFnSc1d1	příjemná
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
nikoli	nikoli	k9	nikoli
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
schopen	schopen	k2eAgMnSc1d1	schopen
hudbu	hudba	k1gFnSc4	hudba
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
ovládat	ovládat	k5eAaImF	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Jev	jev	k1gInSc1	jev
zpravidla	zpravidla	k6eAd1	zpravidla
nelze	lze	k6eNd1	lze
vyvolat	vyvolat	k5eAaPmF	vyvolat
vůli	vůle	k1gFnSc4	vůle
a	a	k8xC	a
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
sám	sám	k3xTgMnSc1	sám
odezní	odeznět	k5eAaPmIp3nS	odeznět
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
živočichové	živočich	k1gMnPc1	živočich
mají	mít	k5eAaImIp3nP	mít
výrazně	výrazně	k6eAd1	výrazně
lepší	dobrý	k2eAgInSc4d2	lepší
sluch	sluch	k1gInSc4	sluch
než	než	k8xS	než
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
citlivosti	citlivost	k1gFnSc2	citlivost
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
frekvence	frekvence	k1gFnPc4	frekvence
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ucho	ucho	k1gNnSc1	ucho
vnímá	vnímat	k5eAaImIp3nS	vnímat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pes	pes	k1gMnSc1	pes
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vnímat	vnímat	k5eAaImF	vnímat
zvuky	zvuk	k1gInPc4	zvuk
od	od	k7c2	od
30	[number]	k4	30
Hz	Hz	kA	Hz
do	do	k7c2	do
45	[number]	k4	45
kHz	khz	kA	khz
<g/>
,	,	kIx,	,
kočka	kočka	k1gFnSc1	kočka
od	od	k7c2	od
15	[number]	k4	15
Hz	Hz	kA	Hz
do	do	k7c2	do
50	[number]	k4	50
kHz	khz	kA	khz
<g/>
,	,	kIx,	,
netopýr	netopýr	k1gMnSc1	netopýr
až	až	k9	až
100	[number]	k4	100
kHz	khz	kA	khz
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
netopýři	netopýr	k1gMnPc1	netopýr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jiní	jiný	k2eAgMnPc1d1	jiný
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaPmIp3nP	využívat
sluch	sluch	k1gInSc4	sluch
také	také	k9	také
k	k	k7c3	k
orientaci	orientace	k1gFnSc3	orientace
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vysílají	vysílat	k5eAaImIp3nP	vysílat
zvukové	zvukový	k2eAgInPc4d1	zvukový
signály	signál	k1gInPc4	signál
a	a	k8xC	a
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
odrazu	odraz	k1gInSc2	odraz
určí	určit	k5eAaPmIp3nS	určit
polohu	poloha	k1gFnSc4	poloha
a	a	k8xC	a
velikost	velikost	k1gFnSc4	velikost
překážky	překážka	k1gFnSc2	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
echolokace	echolokace	k1gFnSc1	echolokace
<g/>
,	,	kIx,	,
a	a	k8xC	a
kromě	kromě	k7c2	kromě
netopýrů	netopýr	k1gMnPc2	netopýr
jej	on	k3xPp3gMnSc4	on
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
můry	můra	k1gFnPc1	můra
<g/>
,	,	kIx,	,
kytovci	kytovec	k1gMnPc1	kytovec
nebo	nebo	k8xC	nebo
někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Ucho	ucho	k1gNnSc1	ucho
Mechanoreceptor	Mechanoreceptor	k1gInSc1	Mechanoreceptor
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
vnímání	vnímání	k1gNnSc3	vnímání
zvuku	zvuk	k1gInSc2	zvuk
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
.	.	kIx.	.
</s>
<s>
Rostlinná	rostlinný	k2eAgFnSc1d1	rostlinná
bioakustika	bioakustika	k1gFnSc1	bioakustika
také	také	k9	také
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sluch	sluch	k1gInSc4	sluch
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
sluch	sluch	k1gInSc4	sluch
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
