<s desamb="1">
Může	moct	k5eAaImIp3nS
způsobovat	způsobovat	k5eAaImF
nemoc	nemoc	k1gFnSc4
AIDS	AIDS	kA
(	(	kIx(
<g/>
z	z	k7c2
angl.	angl.	k?
Acquired	Acquired	k1gInSc1
Immune	Immun	k1gInSc5
Deficiency	Deficiency	k1gInPc1
Syndrome	syndrom	k1gInSc5
<g/>
,	,	kIx,
též	též	k9
Acquired	acquired	k1gInSc1
Immunodeficiency	immunodeficiency	k1gMnSc2
Syndrome	syndrome	k1gInSc5
<g/>
)	)	kIx)
čili	čili	k8xC
syndrom	syndrom	k1gInSc1
získané	získaný	k2eAgFnSc2d1
imunitní	imunitní	k2eAgFnSc2d1
nedostatečnosti	nedostatečnost	k1gFnSc2
<g/>
,	,	kIx,
též	též	k9
syndrom	syndrom	k1gInSc1
získaného	získaný	k2eAgNnSc2d1
selhání	selhání	k1gNnSc2
imunity	imunita	k1gFnSc2
<g/>
.	.	kIx.
</s>