<s>
V	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
symbolem	symbol	k1gInSc7	symbol
prolité	prolitý	k2eAgFnSc2d1	prolitá
krve	krev	k1gFnSc2	krev
mučedníků	mučedník	k1gMnPc2	mučedník
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
liturgická	liturgický	k2eAgFnSc1d1	liturgická
barva	barva	k1gFnSc1	barva
při	při	k7c6	při
jejich	jejich	k3xOp3gFnPc6	jejich
památkách	památka	k1gFnPc6	památka
<g/>
.	.	kIx.	.
</s>
