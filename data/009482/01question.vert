<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
kosmetickému	kosmetický	k2eAgNnSc3d1	kosmetické
líčidlu	líčidlo	k1gNnSc3	líčidlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
zvýraznění	zvýraznění	k1gNnSc4	zvýraznění
či	či	k8xC	či
změnu	změna	k1gFnSc4	změna
barvy	barva	k1gFnSc2	barva
rtů	ret	k1gInPc2	ret
<g/>
?	?	kIx.	?
</s>
