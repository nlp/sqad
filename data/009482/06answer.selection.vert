<s>
Rtěnka	rtěnka	k1gFnSc1	rtěnka
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
kosmetického	kosmetický	k2eAgNnSc2d1	kosmetické
líčidla	líčidlo	k1gNnSc2	líčidlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
zvýraznění	zvýraznění	k1gNnSc4	zvýraznění
či	či	k8xC	či
změnu	změna	k1gFnSc4	změna
barvy	barva	k1gFnSc2	barva
rtů	ret	k1gInPc2	ret
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejčastěji	často	k6eAd3	často
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
