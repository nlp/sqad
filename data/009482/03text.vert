<p>
<s>
Rtěnka	rtěnka	k1gFnSc1	rtěnka
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
kosmetického	kosmetický	k2eAgNnSc2d1	kosmetické
líčidla	líčidlo	k1gNnSc2	líčidlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
zvýraznění	zvýraznění	k1gNnSc4	zvýraznění
či	či	k8xC	či
změnu	změna	k1gFnSc4	změna
barvy	barva	k1gFnSc2	barva
rtů	ret	k1gInPc2	ret
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejčastěji	často	k6eAd3	často
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druh	druh	k1gInSc4	druh
barevné	barevný	k2eAgFnSc2d1	barevná
směsi	směs	k1gFnSc2	směs
tvořené	tvořený	k2eAgNnSc1d1	tvořené
barvivem	barvivo	k1gNnSc7	barvivo
<g/>
,	,	kIx,	,
olejem	olej	k1gInSc7	olej
<g/>
,	,	kIx,	,
voskem	vosk	k1gInSc7	vosk
a	a	k8xC	a
změkčovadlem	změkčovadlo	k1gNnSc7	změkčovadlo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
otočném	otočný	k2eAgInSc6d1	otočný
<g/>
,	,	kIx,	,
či	či	k8xC	či
otvíracím	otvírací	k2eAgNnSc6d1	otvírací
ochranném	ochranný	k2eAgNnSc6d1	ochranné
pouzdře	pouzdro	k1gNnSc6	pouzdro
<g/>
.	.	kIx.	.
</s>
<s>
Aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
natření	natření	k1gNnSc2	natření
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rtěnkou	rtěnka	k1gFnSc7	rtěnka
natřou	natřít	k5eAaPmIp3nP	natřít
rty	ret	k1gInPc4	ret
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pak	pak	k6eAd1	pak
získají	získat	k5eAaPmIp3nP	získat
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
využívá	využívat	k5eAaPmIp3nS	využívat
papírový	papírový	k2eAgInSc1d1	papírový
ubrousek	ubrousek	k1gInSc1	ubrousek
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
rty	ret	k1gInPc7	ret
otisknou	otisknout	k5eAaPmIp3nP	otisknout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odstranila	odstranit	k5eAaPmAgFnS	odstranit
přebytečná	přebytečný	k2eAgFnSc1d1	přebytečná
rtěnka	rtěnka	k1gFnSc1	rtěnka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
používání	používání	k1gNnSc2	používání
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInPc4	první
doklady	doklad	k1gInPc4	doklad
o	o	k7c4	o
používání	používání	k1gNnSc4	používání
rtěnky	rtěnka	k1gFnSc2	rtěnka
pro	pro	k7c4	pro
zvýraznění	zvýraznění	k1gNnSc4	zvýraznění
rtů	ret	k1gInPc2	ret
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
5	[number]	k4	5
000	[number]	k4	000
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
Velkého	velký	k2eAgInSc2d1	velký
rozvoje	rozvoj	k1gInSc2	rozvoj
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
I.	I.	kA	I.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnSc1	její
moderní	moderní	k2eAgFnSc1d1	moderní
podoba	podoba	k1gFnSc1	podoba
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přesněji	přesně	k6eAd2	přesně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
komerční	komerční	k2eAgInSc1d1	komerční
model	model	k1gInSc1	model
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
jeleního	jelení	k2eAgInSc2d1	jelení
loje	loj	k1gInSc2	loj
<g/>
,	,	kIx,	,
vosku	vosk	k1gInSc2	vosk
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
bergamotového	bergamotový	k2eAgInSc2d1	bergamotový
a	a	k8xC	a
mandlového	mandlový	k2eAgInSc2d1	mandlový
oleje	olej	k1gInSc2	olej
<g/>
.	.	kIx.	.
</s>
<s>
Rtěnka	rtěnka	k1gFnSc1	rtěnka
tak	tak	k9	tak
jak	jak	k8xC	jak
jí	on	k3xPp3gFnSc3	on
známe	znát	k5eAaImIp1nP	znát
z	z	k7c2	z
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
je	být	k5eAaImIp3nS	být
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
rtěnky	rtěnka	k1gFnPc1	rtěnka
hojně	hojně	k6eAd1	hojně
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
a	a	k8xC	a
průzkumy	průzkum	k1gInPc1	průzkum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
83	[number]	k4	83
%	%	kIx~	%
dámské	dámský	k2eAgFnSc2d1	dámská
populace	populace	k1gFnSc2	populace
rtěnku	rtěnka	k1gFnSc4	rtěnka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
využívá	využívat	k5eAaPmIp3nS	využívat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kontroverzní	kontroverzní	k2eAgFnPc1d1	kontroverzní
chemické	chemický	k2eAgFnPc1d1	chemická
látky	látka	k1gFnPc1	látka
ve	v	k7c6	v
rtěnkách	rtěnka	k1gFnPc6	rtěnka
==	==	k?	==
</s>
</p>
<p>
<s>
Velké	velký	k2eAgFnPc1d1	velká
diskuse	diskuse	k1gFnPc1	diskuse
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
o	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
olova	olovo	k1gNnSc2	olovo
ve	v	k7c6	v
rtěnkách	rtěnka	k1gFnPc6	rtěnka
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
problematickými	problematický	k2eAgFnPc7d1	problematická
látkami	látka	k1gFnPc7	látka
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc1	některý
UV	UV	kA	UV
filtry	filtr	k1gInPc4	filtr
nebo	nebo	k8xC	nebo
konzervační	konzervační	k2eAgFnPc4d1	konzervační
látky	látka	k1gFnPc4	látka
parabeny	paraben	k2eAgFnPc4d1	paraben
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
německého	německý	k2eAgInSc2d1	německý
Spolkového	spolkový	k2eAgInSc2d1	spolkový
institutu	institut	k1gInSc2	institut
pro	pro	k7c4	pro
hodnocení	hodnocení	k1gNnSc4	hodnocení
rizik	riziko	k1gNnPc2	riziko
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
neměl	mít	k5eNaImAgInS	mít
vitamin	vitamin	k1gInSc1	vitamin
A	a	k9	a
používat	používat	k5eAaImF	používat
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
kosmetických	kosmetický	k2eAgInPc2d1	kosmetický
přípravků	přípravek	k1gInPc2	přípravek
nanášených	nanášený	k2eAgInPc2d1	nanášený
na	na	k7c6	na
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
rtěnky	rtěnka	k1gFnPc4	rtěnka
nebo	nebo	k8xC	nebo
balzámy	balzám	k1gInPc4	balzám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Balzám	balzám	k1gInSc1	balzám
na	na	k7c4	na
rty	ret	k1gInPc4	ret
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rtěnka	rtěnka	k1gFnSc1	rtěnka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rtěnka	rtěnka	k1gFnSc1	rtěnka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
článek	článek	k1gInSc1	článek
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
rtěnek	rtěnka	k1gFnPc2	rtěnka
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
