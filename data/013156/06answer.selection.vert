<s>
Hrací	hrací	k2eAgFnSc1d1	hrací
kostka	kostka	k1gFnSc1	kostka
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc4d1	malý
mnohostěn	mnohostěn	k1gInSc4	mnohostěn
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
krychle	krychle	k1gFnSc1	krychle
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
generování	generování	k1gNnSc4	generování
sekvence	sekvence	k1gFnSc2	sekvence
náhodných	náhodný	k2eAgNnPc2d1	náhodné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
