<p>
<s>
Hrací	hrací	k2eAgFnSc1d1	hrací
kostka	kostka	k1gFnSc1	kostka
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc4d1	malý
mnohostěn	mnohostěn	k1gInSc4	mnohostěn
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
krychle	krychle	k1gFnSc1	krychle
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
generování	generování	k1gNnSc4	generování
sekvence	sekvence	k1gFnSc2	sekvence
náhodných	náhodný	k2eAgNnPc2d1	náhodné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
důležitá	důležitý	k2eAgFnSc1d1	důležitá
součást	součást	k1gFnSc1	součást
mnoha	mnoho	k4c2	mnoho
hazardních	hazardní	k2eAgMnPc2d1	hazardní
i	i	k8xC	i
společenských	společenský	k2eAgFnPc2d1	společenská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
kostek	kostka	k1gFnPc2	kostka
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
Kostky	kostka	k1gFnPc4	kostka
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgFnPc4d1	stejná
velikosti	velikost	k1gFnPc4	velikost
a	a	k8xC	a
házejí	házet	k5eAaImIp3nP	házet
se	se	k3xPyFc4	se
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
či	či	k8xC	či
kalíšku	kalíšek	k1gInSc2	kalíšek
<g/>
.	.	kIx.	.
</s>
<s>
Mívají	mívat	k5eAaImIp3nP	mívat
tvar	tvar	k1gInSc4	tvar
krychle	krychle	k1gFnSc2	krychle
se	s	k7c7	s
zaoblenými	zaoblený	k2eAgInPc7d1	zaoblený
rohy	roh	k1gInPc7	roh
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
stěna	stěna	k1gFnSc1	stěna
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
označena	označit	k5eAaPmNgFnS	označit
různým	různý	k2eAgInSc7d1	různý
počtem	počet	k1gInSc7	počet
kulatých	kulatý	k2eAgInPc2d1	kulatý
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
oka	oka	k1gFnSc1	oka
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
čísla	číslo	k1gNnPc4	číslo
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Každým	každý	k3xTgInSc7	každý
hodem	hod	k1gInSc7	hod
tak	tak	k6eAd1	tak
hrací	hrací	k2eAgFnSc1d1	hrací
kostka	kostka	k1gFnSc1	kostka
"	"	kIx"	"
<g/>
vygeneruje	vygenerovat	k5eAaPmIp3nS	vygenerovat
<g/>
"	"	kIx"	"
náhodné	náhodný	k2eAgNnSc1d1	náhodné
číslo	číslo	k1gNnSc1	číslo
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
do	do	k7c2	do
šesti	šest	k4xCc2	šest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stěny	stěna	k1gFnPc1	stěna
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ale	ale	k9	ale
označeny	označit	k5eAaPmNgInP	označit
i	i	k8xC	i
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
různými	různý	k2eAgInPc7d1	různý
symboly	symbol	k1gInPc7	symbol
nebo	nebo	k8xC	nebo
barvami	barva	k1gFnPc7	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
artefakty	artefakt	k1gInPc1	artefakt
podobné	podobný	k2eAgInPc1d1	podobný
dnešním	dnešní	k2eAgFnPc3d1	dnešní
kostkám	kostka	k1gFnPc3	kostka
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc4	ten
opracované	opracovaný	k2eAgFnPc1d1	opracovaná
vícehranné	vícehranný	k2eAgFnPc1d1	vícehranná
kosti	kost	k1gFnPc1	kost
se	se	k3xPyFc4	se
symboly	symbol	k1gInPc7	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
hrací	hrací	k2eAgFnPc1d1	hrací
kostky	kostka	k1gFnPc1	kostka
začínají	začínat	k5eAaImIp3nP	začínat
objevovat	objevovat	k5eAaImF	objevovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
již	již	k6eAd1	již
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
krychlové	krychlový	k2eAgFnSc6d1	krychlová
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
se	se	k3xPyFc4	se
kostky	kostka	k1gFnPc1	kostka
dočkaly	dočkat	k5eAaPmAgFnP	dočkat
až	až	k6eAd1	až
v	v	k7c6	v
období	období	k1gNnSc6	období
římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
také	také	k9	také
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
slavný	slavný	k2eAgInSc1d1	slavný
citát	citát	k1gInSc1	citát
"	"	kIx"	"
<g/>
Alea	Ale	k2eAgFnSc1d1	Alea
iacta	iacta	k1gFnSc1	iacta
est	est	k?	est
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
Kostky	kostka	k1gFnPc1	kostka
jsou	být	k5eAaImIp3nP	být
vrženy	vržen	k2eAgFnPc1d1	vržena
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Claudius	Claudius	k1gMnSc1	Claudius
prý	prý	k9	prý
napsal	napsat	k5eAaPmAgMnS	napsat
knihu	kniha	k1gFnSc4	kniha
o	o	k7c4	o
strategii	strategie	k1gFnSc4	strategie
kostek	kostka	k1gFnPc2	kostka
a	a	k8xC	a
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
Commoda	Commod	k1gMnSc4	Commod
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
první	první	k4xOgNnSc4	první
kasino	kasino	k1gNnSc4	kasino
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
kostky	kostka	k1gFnPc1	kostka
staly	stát	k5eAaPmAgFnP	stát
natolik	natolik	k6eAd1	natolik
populární	populární	k2eAgFnSc7d1	populární
hrou	hra	k1gFnSc7	hra
<g/>
,	,	kIx,	,
že	že	k8xS	že
nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
hra	hra	k1gFnSc1	hra
jakožto	jakožto	k8xS	jakožto
hazardní	hazardní	k2eAgFnSc1d1	hazardní
postavena	postaven	k2eAgFnSc1d1	postavena
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
nalezená	nalezený	k2eAgFnSc1d1	nalezená
kostka	kostka	k1gFnSc1	kostka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hlíny	hlína	k1gFnSc2	hlína
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Indu	Indus	k1gInSc2	Indus
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
asi	asi	k9	asi
5	[number]	k4	5
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
je	on	k3xPp3gInPc4	on
prokazatelně	prokazatelně	k6eAd1	prokazatelně
hrávali	hrávat	k5eAaImAgMnP	hrávat
jak	jak	k8xC	jak
otroci	otrok	k1gMnPc1	otrok
<g/>
,	,	kIx,	,
tak	tak	k9	tak
císařové	císař	k1gMnPc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Jan	Jan	k1gMnSc1	Jan
Ámos	Ámos	k1gMnSc1	Ámos
Komenský	Komenský	k1gMnSc1	Komenský
jim	on	k3xPp3gMnPc3	on
věnoval	věnovat	k5eAaImAgMnS	věnovat
celou	celá	k1gFnSc4	celá
jednu	jeden	k4xCgFnSc4	jeden
stranu	strana	k1gFnSc4	strana
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knížce	knížka	k1gFnSc6	knížka
Orbis	orbis	k1gInSc4	orbis
pictus	pictus	k1gInSc4	pictus
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Svět	svět	k1gInSc1	svět
v	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obliba	obliba	k1gFnSc1	obliba
hry	hra	k1gFnSc2	hra
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
opakovaným	opakovaný	k2eAgInPc3d1	opakovaný
zákazům	zákaz	k1gInPc3	zákaz
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
městských	městský	k2eAgFnPc2d1	městská
vyhlášek	vyhláška	k1gFnPc2	vyhláška
i	i	k8xC	i
panovnických	panovnický	k2eAgInPc2d1	panovnický
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vykopávkách	vykopávka	k1gFnPc6	vykopávka
z	z	k7c2	z
Pompejí	Pompeje	k1gFnPc2	Pompeje
i	i	k9	i
odjinud	odjinud	k6eAd1	odjinud
byly	být	k5eAaImAgFnP	být
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
hrací	hrací	k2eAgFnPc1d1	hrací
kostky	kostka	k1gFnPc1	kostka
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
hlíny	hlína	k1gFnSc2	hlína
<g/>
,	,	kIx,	,
alabastru	alabastr	k1gInSc2	alabastr
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
i	i	k8xC	i
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
kostka	kostka	k1gFnSc1	kostka
==	==	k?	==
</s>
</p>
<p>
<s>
Hrací	hrací	k2eAgFnSc1d1	hrací
kostka	kostka	k1gFnSc1	kostka
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
malá	malý	k2eAgFnSc1d1	malá
krychle	krychle	k1gFnSc1	krychle
o	o	k7c6	o
hraně	hrana	k1gFnSc6	hrana
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Stěny	stěn	k1gInPc1	stěn
jsou	být	k5eAaImIp3nP	být
očíslovány	očíslovat	k5eAaPmNgInP	očíslovat
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
do	do	k7c2	do
šesti	šest	k4xCc2	šest
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
pomocí	pomocí	k7c2	pomocí
malých	malý	k2eAgInPc2d1	malý
kulatých	kulatý	k2eAgInPc2d1	kulatý
obarvených	obarvený	k2eAgInPc2d1	obarvený
důlků	důlek	k1gInPc2	důlek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kubických	kubický	k2eAgFnPc6d1	kubická
kostkách	kostka	k1gFnPc6	kostka
jsou	být	k5eAaImIp3nP	být
hodnoty	hodnota	k1gFnPc1	hodnota
rozmístěny	rozmístit	k5eAaPmNgFnP	rozmístit
na	na	k7c6	na
kostce	kostka	k1gFnSc6	kostka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
součet	součet	k1gInSc1	součet
bodů	bod	k1gInPc2	bod
dvou	dva	k4xCgFnPc6	dva
protilehlých	protilehlý	k2eAgFnPc2d1	protilehlá
stěn	stěna	k1gFnPc2	stěna
byl	být	k5eAaImAgInS	být
sedm	sedm	k4xCc4	sedm
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrany	hrana	k1gFnPc1	hrana
stěn	stěna	k1gFnPc2	stěna
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
a	a	k8xC	a
3	[number]	k4	3
se	se	k3xPyFc4	se
sbíhají	sbíhat	k5eAaImIp3nP	sbíhat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
vrcholu	vrchol	k1gInSc2	vrchol
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
stěny	stěna	k1gFnPc1	stěna
seřazeny	seřadit	k5eAaPmNgFnP	seřadit
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
nebo	nebo	k8xC	nebo
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hod	hod	k1gInSc4	hod
hrací	hrací	k2eAgFnSc7d1	hrací
kostkou	kostka	k1gFnSc7	kostka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
ryze	ryze	k6eAd1	ryze
náhodný	náhodný	k2eAgMnSc1d1	náhodný
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
-	-	kIx~	-
aspoň	aspoň	k9	aspoň
teoreticky	teoreticky	k6eAd1	teoreticky
-	-	kIx~	-
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
svých	svůj	k3xOyFgFnPc6	svůj
fázích	fáze	k1gFnPc6	fáze
deterministický	deterministický	k2eAgInSc1d1	deterministický
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
beze	beze	k7c2	beze
zbytku	zbytek	k1gInSc2	zbytek
určen	určit	k5eAaPmNgInS	určit
hozením	hození	k1gNnSc7	hození
kostky	kostka	k1gFnSc2	kostka
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
pohyb	pohyb	k1gInSc1	pohyb
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
hráč	hráč	k1gMnSc1	hráč
kostce	kostka	k1gFnSc6	kostka
udělil	udělit	k5eAaPmAgInS	udělit
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tolik	tolik	k4yIc4	tolik
parametrů	parametr	k1gInPc2	parametr
dráhy	dráha	k1gFnSc2	dráha
i	i	k8xC	i
rotace	rotace	k1gFnSc2	rotace
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
výsledek	výsledek	k1gInSc1	výsledek
nedá	dát	k5eNaPmIp3nS	dát
prakticky	prakticky	k6eAd1	prakticky
předvídat	předvídat	k5eAaImF	předvídat
<g/>
.	.	kIx.	.
</s>
<s>
Kostka	kostka	k1gFnSc1	kostka
tak	tak	k9	tak
při	při	k7c6	při
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
hodech	hod	k1gInPc6	hod
generuje	generovat	k5eAaImIp3nS	generovat
(	(	kIx(	(
<g/>
pseudo	pseudo	k1gNnSc1	pseudo
<g/>
)	)	kIx)	)
<g/>
náhodné	náhodný	k2eAgNnSc1d1	náhodné
číslo	číslo	k1gNnSc1	číslo
a	a	k8xC	a
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
jakýsi	jakýsi	k3yIgInSc1	jakýsi
hardwarový	hardwarový	k2eAgInSc1d1	hardwarový
generátor	generátor	k1gInSc1	generátor
náhodných	náhodný	k2eAgNnPc2d1	náhodné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ale	ale	k9	ale
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
každé	každý	k3xTgFnSc2	každý
kostky	kostka	k1gFnSc2	kostka
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
malým	malý	k2eAgFnPc3d1	malá
odchylkám	odchylka	k1gFnPc3	odchylka
a	a	k8xC	a
ze	z	k7c2	z
stěn	stěna	k1gFnPc2	stěna
z	z	k7c2	z
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
bodů	bod	k1gInPc2	bod
je	být	k5eAaImIp3nS	být
odebráno	odebrat	k5eAaPmNgNnS	odebrat
více	hodně	k6eAd2	hodně
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
všechny	všechen	k3xTgInPc4	všechen
výsledky	výsledek	k1gInPc4	výsledek
úplně	úplně	k6eAd1	úplně
stejnou	stejný	k2eAgFnSc4d1	stejná
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
<g/>
.	.	kIx.	.
</s>
<s>
Hrací	hrací	k2eAgFnPc1d1	hrací
kostky	kostka	k1gFnPc1	kostka
v	v	k7c6	v
kasinu	kasino	k1gNnSc6	kasino
proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
značky	značka	k1gFnPc1	značka
zcela	zcela	k6eAd1	zcela
zarovnané	zarovnaný	k2eAgInPc4d1	zarovnaný
s	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
stěny	stěna	k1gFnSc2	stěna
<g/>
,	,	kIx,	,
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
přesností	přesnost	k1gFnSc7	přesnost
a	a	k8xC	a
spadne	spadnout	k5eAaPmIp3nS	spadnout
<g/>
-li	i	k?	-li
taková	takový	k3xDgFnSc1	takový
kostka	kostka	k1gFnSc1	kostka
ze	z	k7c2	z
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
okamžitě	okamžitě	k6eAd1	okamžitě
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
novou	nový	k2eAgFnSc7d1	nová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostky	kostka	k1gFnPc1	kostka
se	se	k3xPyFc4	se
vrhají	vrhat	k5eAaImIp3nP	vrhat
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
z	z	k7c2	z
kalíšku	kalíšek	k1gInSc2	kalíšek
nebo	nebo	k8xC	nebo
krabičky	krabička	k1gFnSc2	krabička
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
rovný	rovný	k2eAgInSc4d1	rovný
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Hodnotu	hodnota	k1gFnSc4	hodnota
hodu	hod	k1gInSc2	hod
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
stěna	stěna	k1gFnSc1	stěna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
zastavení	zastavení	k1gNnSc6	zastavení
kostky	kostka	k1gFnSc2	kostka
nahoře	nahoře	k6eAd1	nahoře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
jednotlivý	jednotlivý	k2eAgInSc4d1	jednotlivý
hod	hod	k1gInSc4	hod
je	být	k5eAaImIp3nS	být
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
padne	padnout	k5eAaImIp3nS	padnout
nějaká	nějaký	k3yIgFnSc1	nějaký
hodnota	hodnota	k1gFnSc1	hodnota
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
6	[number]	k4	6
přesně	přesně	k6eAd1	přesně
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
hází	házet	k5eAaImIp3nS	házet
dvěma	dva	k4xCgFnPc7	dva
kostkami	kostka	k1gFnPc7	kostka
<g/>
,	,	kIx,	,
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
součtu	součet	k1gInSc2	součet
hodnot	hodnota	k1gFnPc2	hodnota
není	být	k5eNaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
sledujeme	sledovat	k5eAaImIp1nP	sledovat
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
součtu	součet	k1gInSc2	součet
tří	tři	k4xCgNnPc2	tři
a	a	k8xC	a
více	hodně	k6eAd2	hodně
kostek	kostka	k1gFnPc2	kostka
<g/>
,	,	kIx,	,
zjistíme	zjistit	k5eAaPmIp1nP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
křivka	křivka	k1gFnSc1	křivka
rozdělení	rozdělení	k1gNnSc2	rozdělení
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
zvonovité	zvonovitý	k2eAgFnSc3d1	zvonovitá
křivce	křivka	k1gFnSc3	křivka
normálního	normální	k2eAgNnSc2d1	normální
rozdělení	rozdělení	k1gNnSc2	rozdělení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
hodu	hod	k1gInSc6	hod
padne	padnout	k5eAaImIp3nS	padnout
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
kostkách	kostka	k1gFnPc6	kostka
předem	předem	k6eAd1	předem
určené	určený	k2eAgNnSc4d1	určené
stejné	stejný	k2eAgNnSc4d1	stejné
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
s	s	k7c7	s
každou	každý	k3xTgFnSc7	každý
další	další	k2eAgFnSc7d1	další
kostkou	kostka	k1gFnSc7	kostka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jakožto	jakožto	k8xS	jakožto
hmotný	hmotný	k2eAgInSc4d1	hmotný
předmět	předmět	k1gInSc4	předmět
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
kostka	kostka	k1gFnSc1	kostka
úplně	úplně	k6eAd1	úplně
přesná	přesný	k2eAgFnSc1d1	přesná
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
těžiště	těžiště	k1gNnSc1	těžiště
nebude	být	k5eNaImBp3nS	být
přesně	přesně	k6eAd1	přesně
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
hodu	hod	k1gInSc2	hod
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
hodnot	hodnota	k1gFnPc2	hodnota
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
tedy	tedy	k9	tedy
lišit	lišit	k5eAaImF	lišit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rozdílné	rozdílný	k2eAgFnSc2d1	rozdílná
relativní	relativní	k2eAgFnSc2d1	relativní
hmotnosti	hmotnost	k1gFnSc2	hmotnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
stran	strana	k1gFnPc2	strana
kostky	kostka	k1gFnSc2	kostka
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
si	se	k3xPyFc3	se
představit	představit	k5eAaPmF	představit
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
kostku	kostka	k1gFnSc4	kostka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
hodnotu	hodnota	k1gFnSc4	hodnota
6	[number]	k4	6
a	a	k8xC	a
na	na	k7c6	na
protější	protější	k2eAgFnSc6d1	protější
straně	strana	k1gFnSc6	strana
hodnotu	hodnota	k1gFnSc4	hodnota
1	[number]	k4	1
(	(	kIx(	(
<g/>
zbylé	zbylý	k2eAgFnSc2d1	zbylá
strany	strana	k1gFnSc2	strana
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
prázdné	prázdný	k2eAgFnPc1d1	prázdná
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
tuto	tento	k3xDgFnSc4	tento
kostku	kostka	k1gFnSc4	kostka
rozřízli	rozříznout	k5eAaPmAgMnP	rozříznout
v	v	k7c6	v
půli	půle	k1gFnSc6	půle
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
část	část	k1gFnSc1	část
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
"	"	kIx"	"
<g/>
6	[number]	k4	6
<g/>
"	"	kIx"	"
lehčí	lehčit	k5eAaImIp3nS	lehčit
než	než	k8xS	než
část	část	k1gFnSc1	část
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
"	"	kIx"	"
<g/>
1	[number]	k4	1
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
úbytku	úbytek	k1gInSc2	úbytek
materiálu	materiál	k1gInSc2	materiál
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
"	"	kIx"	"
<g/>
ďolíčků	ďolíček	k1gInPc2	ďolíček
<g/>
"	"	kIx"	"
představujících	představující	k2eAgMnPc2d1	představující
hodnotu	hodnota	k1gFnSc4	hodnota
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
hodnoty	hodnota	k1gFnPc4	hodnota
zobrazeny	zobrazen	k2eAgFnPc1d1	zobrazena
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
puntíky	puntík	k1gInPc1	puntík
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
opět	opět	k6eAd1	opět
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
částí	část	k1gFnPc2	část
nepatrně	patrně	k6eNd1	patrně
těžší	těžký	k2eAgInSc4d2	těžší
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
přidání	přidání	k1gNnSc2	přidání
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
hustotu	hustota	k1gFnSc4	hustota
než	než	k8xS	než
tělo	tělo	k1gNnSc4	tělo
kostky	kostka	k1gFnSc2	kostka
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k8xC	i
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
však	však	k9	však
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc4d1	malá
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
na	na	k7c6	na
pravděpodobnosti	pravděpodobnost	k1gFnSc6	pravděpodobnost
hodu	hod	k1gInSc2	hod
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
hodnot	hodnota	k1gFnPc2	hodnota
projevit	projevit	k5eAaPmF	projevit
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
v	v	k7c6	v
ideálních	ideální	k2eAgFnPc6d1	ideální
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kostky	kostka	k1gFnSc2	kostka
s	s	k7c7	s
"	"	kIx"	"
<g/>
ďolíčky	ďolíček	k1gInPc7	ďolíček
<g/>
"	"	kIx"	"
s	s	k7c7	s
hranami	hrana	k1gFnPc7	hrana
1	[number]	k4	1
<g/>
x	x	k?	x
<g/>
1	[number]	k4	1
<g/>
x	x	k?	x
<g/>
1	[number]	k4	1
<g/>
cm	cm	kA	cm
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
posun	posun	k1gInSc1	posun
těžiště	těžiště	k1gNnSc2	těžiště
oproti	oproti	k7c3	oproti
středu	střed	k1gInSc3	střed
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
třech	tři	k4xCgFnPc6	tři
osách	osa	k1gFnPc6	osa
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
0.02	[number]	k4	0.02
-	-	kIx~	-
0.004	[number]	k4	0.004
mm	mm	kA	mm
<g/>
,	,	kIx,	,
u	u	k7c2	u
kostky	kostka	k1gFnSc2	kostka
s	s	k7c7	s
graficky	graficky	k6eAd1	graficky
znázorněnými	znázorněný	k2eAgFnPc7d1	znázorněná
hodnotami	hodnota	k1gFnPc7	hodnota
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
puntíky	puntík	k1gInPc1	puntík
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
posun	posun	k1gInSc1	posun
těžiště	těžiště	k1gNnSc2	těžiště
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
v	v	k7c6	v
hodnotách	hodnota	k1gFnPc6	hodnota
okolo	okolo	k7c2	okolo
0.000001	[number]	k4	0.000001
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cinknutá	cinknutý	k2eAgFnSc1d1	cinknutá
kostka	kostka	k1gFnSc1	kostka
===	===	k?	===
</s>
</p>
<p>
<s>
Cinknutá	cinknutý	k2eAgFnSc1d1	cinknutá
kostka	kostka	k1gFnSc1	kostka
je	být	k5eAaImIp3nS	být
hrací	hrací	k2eAgFnSc1d1	hrací
kostka	kostka	k1gFnSc1	kostka
nedovoleným	dovolený	k2eNgInSc7d1	nedovolený
způsobem	způsob	k1gInSc7	způsob
upravená	upravený	k2eAgFnSc1d1	upravená
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
určitá	určitý	k2eAgFnSc1d1	určitá
hodnota	hodnota	k1gFnSc1	hodnota
padala	padat	k5eAaImAgFnS	padat
častěji	často	k6eAd2	často
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
<g/>
)	)	kIx)	)
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc1d1	ostatní
hodnoty	hodnota	k1gFnPc1	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Metody	metoda	k1gFnPc1	metoda
jak	jak	k6eAd1	jak
toho	ten	k3xDgMnSc4	ten
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
některé	některý	k3yIgFnPc4	některý
hrany	hrana	k1gFnPc4	hrana
udělat	udělat	k5eAaPmF	udělat
ostřejší	ostrý	k2eAgFnSc4d2	ostřejší
nebo	nebo	k8xC	nebo
kulatější	kulatý	k2eAgInPc4d2	kulatější
<g/>
,	,	kIx,	,
kostka	kostka	k1gFnSc1	kostka
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
tvar	tvar	k1gInSc4	tvar
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
krychle	krychle	k1gFnSc2	krychle
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
neznatelně	znatelně	k6eNd1	znatelně
zdeformovaná	zdeformovaný	k2eAgFnSc1d1	zdeformovaná
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
kostka	kostka	k1gFnSc1	kostka
není	být	k5eNaImIp3nS	být
průhledná	průhledný	k2eAgFnSc1d1	průhledná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
přidat	přidat	k5eAaPmF	přidat
závaží	závaží	k1gNnSc4	závaží
a	a	k8xC	a
změnit	změnit	k5eAaPmF	změnit
tak	tak	k6eAd1	tak
těžiště	těžiště	k1gNnSc4	těžiště
kostky	kostka	k1gFnSc2	kostka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
možno	možno	k6eAd1	možno
vytvořit	vytvořit	k5eAaPmF	vytvořit
"	"	kIx"	"
<g/>
vyhrávací	vyhrávací	k2eAgFnPc1d1	vyhrávací
kostky	kostka	k1gFnPc1	kostka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
častěji	často	k6eAd2	často
padá	padat	k5eAaImIp3nS	padat
určitá	určitý	k2eAgFnSc1d1	určitá
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
prohrávací	prohrávací	k2eAgFnPc1d1	prohrávací
kostky	kostka	k1gFnPc1	kostka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
naopak	naopak	k6eAd1	naopak
některá	některý	k3yIgFnSc1	některý
hodnota	hodnota	k1gFnSc1	hodnota
padá	padat	k5eAaImIp3nS	padat
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
i	i	k9	i
důmyslnější	důmyslný	k2eAgFnPc1d2	důmyslnější
metody	metoda	k1gFnPc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Kostka	kostka	k1gFnSc1	kostka
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
v	v	k7c6	v
dutince	dutinka	k1gFnSc6	dutinka
uvnitř	uvnitř	k6eAd1	uvnitř
kapku	kapka	k1gFnSc4	kapka
rtuti	rtuť	k1gFnSc2	rtuť
a	a	k8xC	a
dutinka	dutinka	k1gFnSc1	dutinka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
dutinkou	dutinka	k1gFnSc7	dutinka
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Poklepáním	poklepání	k1gNnSc7	poklepání
kostkou	kostka	k1gFnSc7	kostka
o	o	k7c4	o
stůl	stůl	k1gInSc4	stůl
se	se	k3xPyFc4	se
kapka	kapka	k1gFnSc1	kapka
rtuti	rtuť	k1gFnSc2	rtuť
může	moct	k5eAaImIp3nS	moct
stěhovat	stěhovat	k5eAaImF	stěhovat
sem	sem	k6eAd1	sem
a	a	k8xC	a
tam	tam	k6eAd1	tam
a	a	k8xC	a
tak	tak	k6eAd1	tak
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
hry	hra	k1gFnSc2	hra
měnit	měnit	k5eAaImF	měnit
vlastnosti	vlastnost	k1gFnPc4	vlastnost
kostky	kostka	k1gFnSc2	kostka
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
duté	dutý	k2eAgFnPc1d1	dutá
kostky	kostka	k1gFnPc1	kostka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc1d1	malé
závaží	závaží	k1gNnSc1	závaží
a	a	k8xC	a
polotuhá	polotuhý	k2eAgFnSc1d1	polotuhá
substance	substance	k1gFnSc1	substance
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vosk	vosk	k1gInSc4	vosk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
bod	bod	k1gInSc1	bod
tání	tání	k1gNnSc2	tání
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
teplota	teplota	k1gFnSc1	teplota
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
hráči	hráč	k1gMnSc3	hráč
měnit	měnit	k5eAaImF	měnit
umístění	umístění	k1gNnSc4	umístění
závaží	závaží	k1gNnSc2	závaží
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
například	například	k6eAd1	například
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
kostku	kostka	k1gFnSc4	kostka
dýchá	dýchat	k5eAaImIp3nS	dýchat
nebo	nebo	k8xC	nebo
ji	on	k3xPp3gFnSc4	on
zahřeje	zahřát	k5eAaPmIp3nS	zahřát
v	v	k7c6	v
dlani	dlaň	k1gFnSc6	dlaň
<g/>
.	.	kIx.	.
</s>
<s>
Vosk	vosk	k1gInSc1	vosk
změkne	změknout	k5eAaPmIp3nS	změknout
a	a	k8xC	a
závaží	závaží	k1gNnSc1	závaží
klesne	klesnout	k5eAaPmIp3nS	klesnout
k	k	k7c3	k
spodní	spodní	k2eAgFnSc3d1	spodní
stěně	stěna	k1gFnSc3	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Opačná	opačný	k2eAgFnSc1d1	opačná
stěna	stěna	k1gFnSc1	stěna
pak	pak	k6eAd1	pak
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgFnSc1d2	veliký
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
hodu	hod	k1gInSc6	hod
zůstane	zůstat	k5eAaPmIp3nS	zůstat
nahoře	nahoře	k6eAd1	nahoře
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
metodou	metoda	k1gFnSc7	metoda
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
magnetu	magnet	k1gInSc2	magnet
ukrytého	ukrytý	k2eAgInSc2d1	ukrytý
v	v	k7c6	v
kostce	kostka	k1gFnSc6	kostka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
vedením	vedení	k1gNnSc7	vedení
ukrytým	ukrytý	k2eAgNnSc7d1	ukryté
v	v	k7c6	v
hracím	hrací	k2eAgInSc6d1	hrací
stole	stol	k1gInSc6	stol
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
V	v	k7c6	v
kasinu	kasino	k1gNnSc6	kasino
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
acetátové	acetátový	k2eAgFnPc1d1	acetátová
průhledné	průhledný	k2eAgFnPc1d1	průhledná
kostky	kostka	k1gFnPc1	kostka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
"	"	kIx"	"
<g/>
cinknout	cinknout	k5eAaPmF	cinknout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kostky	kostka	k1gFnPc4	kostka
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
hodnotami	hodnota	k1gFnPc7	hodnota
než	než	k8xS	než
1	[number]	k4	1
až	až	k9	až
6	[number]	k4	6
==	==	k?	==
</s>
</p>
<p>
<s>
Stěny	stěna	k1gFnPc1	stěna
hrací	hrací	k2eAgFnSc2d1	hrací
kostky	kostka	k1gFnSc2	kostka
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
označeny	označen	k2eAgInPc1d1	označen
nepřerušovanou	přerušovaný	k2eNgFnSc7d1	nepřerušovaná
řadou	řada	k1gFnSc7	řada
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
začínající	začínající	k2eAgMnSc1d1	začínající
od	od	k7c2	od
jedničky	jednička	k1gFnSc2	jednička
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
nuly	nula	k1gFnSc2	nula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hodnoty	hodnota	k1gFnPc1	hodnota
jsou	být	k5eAaImIp3nP	být
vyznačeny	vyznačen	k2eAgFnPc1d1	vyznačena
tečkami	tečka	k1gFnPc7	tečka
nebo	nebo	k8xC	nebo
čísly	číslo	k1gNnPc7	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
barevné	barevný	k2eAgFnPc1d1	barevná
kostky	kostka	k1gFnPc1	kostka
–	–	k?	–
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
u	u	k7c2	u
her	hra	k1gFnPc2	hra
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ještě	ještě	k9	ještě
neumějí	umět	k5eNaImIp3nP	umět
počítat	počítat	k5eAaImF	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
postupuje	postupovat	k5eAaImIp3nS	postupovat
na	na	k7c4	na
nejbližší	blízký	k2eAgNnSc4d3	nejbližší
políčko	políčko	k1gNnSc4	políčko
vržené	vržený	k2eAgFnSc2d1	vržená
barvy	barva	k1gFnSc2	barva
(	(	kIx(	(
<g/>
dětské	dětský	k2eAgFnSc2d1	dětská
Člověče	člověk	k1gMnSc5	člověk
<g/>
,	,	kIx,	,
nezlob	zlobit	k5eNaImRp2nS	zlobit
se	se	k3xPyFc4	se
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
pokerové	pokerový	k2eAgFnPc1d1	pokerová
kostky	kostka	k1gFnPc1	kostka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
symboly	symbol	k1gInPc4	symbol
známé	známý	k2eAgInPc4d1	známý
z	z	k7c2	z
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
poker	poker	k1gInSc1	poker
(	(	kIx(	(
<g/>
devítka	devítka	k1gFnSc1	devítka
<g/>
,	,	kIx,	,
desítka	desítka	k1gFnSc1	desítka
<g/>
,	,	kIx,	,
spodek	spodek	k1gInSc1	spodek
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
a	a	k8xC	a
eso	eso	k1gNnSc4	eso
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kostky	kostka	k1gFnPc1	kostka
s	s	k7c7	s
písmeny	písmeno	k1gNnPc7	písmeno
–	–	k?	–
různé	různý	k2eAgFnPc4d1	různá
jazykové	jazykový	k2eAgFnPc4d1	jazyková
hry	hra	k1gFnPc4	hra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Ámos	Ámos	k1gMnSc1	Ámos
<g/>
,	,	kIx,	,
Boggle	Boggle	k1gFnSc1	Boggle
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dvojité	dvojitý	k2eAgFnPc1d1	dvojitá
kostky	kostka	k1gFnPc1	kostka
</s>
</p>
<p>
<s>
náhodné	náhodný	k2eAgInPc4d1	náhodný
směry	směr	k1gInPc4	směr
–	–	k?	–
kostky	kostka	k1gFnSc2	kostka
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
ploše	plocha	k1gFnSc6	plocha
šipku	šipka	k1gFnSc4	šipka
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
hodu	hod	k1gInSc2	hod
je	být	k5eAaImIp3nS	být
náhodný	náhodný	k2eAgInSc1d1	náhodný
směr	směr	k1gInSc1	směr
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Mississippi	Mississippi	k1gNnSc1	Mississippi
Queen	Queen	k1gInSc1	Queen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
různé	různý	k2eAgInPc4d1	různý
symboly	symbol	k1gInPc4	symbol
–	–	k?	–
moderní	moderní	k2eAgFnSc4d1	moderní
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
válečné	válečný	k2eAgInPc1d1	válečný
a	a	k8xC	a
fantasy	fantas	k1gInPc1	fantas
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kostky	kostka	k1gFnSc2	kostka
pro	pro	k7c4	pro
herní	herní	k2eAgInSc4d1	herní
systém	systém	k1gInSc4	systém
Fudge	Fudg	k1gFnSc2	Fudg
se	s	k7c7	s
symboly	symbol	k1gInPc7	symbol
"	"	kIx"	"
<g/>
plus	plus	k1gInSc1	plus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
minus	minus	k1gInSc1	minus
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
prázdná	prázdný	k2eAgFnSc1d1	prázdná
stěna	stěna	k1gFnSc1	stěna
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Kostky	kostka	k1gFnPc4	kostka
jiného	jiný	k2eAgInSc2d1	jiný
tvaru	tvar	k1gInSc2	tvar
než	než	k8xS	než
krychle	krychle	k1gFnSc2	krychle
==	==	k?	==
</s>
</p>
<p>
<s>
Mnohostěnná	mnohostěnný	k2eAgFnSc1d1	mnohostěnný
kostka	kostka	k1gFnSc1	kostka
je	být	k5eAaImIp3nS	být
hrací	hrací	k2eAgFnSc1d1	hrací
kostka	kostka	k1gFnSc1	kostka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
než	než	k8xS	než
šest	šest	k4xCc4	šest
stěn	stěna	k1gFnPc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
kostky	kostka	k1gFnPc1	kostka
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
používány	používat	k5eAaImNgInP	používat
v	v	k7c6	v
RPG	RPG	kA	RPG
a	a	k8xC	a
válečných	válečný	k2eAgFnPc6d1	válečná
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
setkáme	setkat	k5eAaPmIp1nP	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
jako	jako	k9	jako
s	s	k7c7	s
pomůckami	pomůcka	k1gFnPc7	pomůcka
při	při	k7c6	při
předpovídání	předpovídání	k1gNnSc6	předpovídání
budoucnosti	budoucnost	k1gFnSc2	budoucnost
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc6d1	jiná
okultních	okultní	k2eAgFnPc6d1	okultní
praktikách	praktika	k1gFnPc6	praktika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
jsou	být	k5eAaImIp3nP	být
používána	používán	k2eAgNnPc1d1	používáno
platónská	platónský	k2eAgNnPc1d1	platónské
tělesa	těleso	k1gNnPc1	těleso
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
12	[number]	k4	12
a	a	k8xC	a
20	[number]	k4	20
stěn	stěna	k1gFnPc2	stěna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
tvary	tvar	k1gInPc1	tvar
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
3	[number]	k4	3
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
,	,	kIx,	,
34	[number]	k4	34
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
100	[number]	k4	100
stěn	stěna	k1gFnPc2	stěna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hry	hra	k1gFnPc1	hra
s	s	k7c7	s
kostkami	kostka	k1gFnPc7	kostka
==	==	k?	==
</s>
</p>
<p>
<s>
Člověče	člověk	k1gMnSc5	člověk
<g/>
,	,	kIx,	,
nezlob	zlobit	k5eNaImRp2nS	zlobit
se	se	k3xPyFc4	se
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Craps	Craps	k6eAd1	Craps
</s>
</p>
<p>
<s>
Backgammon	Backgammon	k1gMnSc1	Backgammon
</s>
</p>
<p>
<s>
Monopoly	monopol	k1gInPc1	monopol
</s>
</p>
<p>
<s>
Dračí	dračí	k2eAgNnSc1d1	dračí
Doupě	doupě	k1gNnSc1	doupě
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kostka	kostka	k1gFnSc1	kostka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hrací	hrací	k2eAgFnSc1d1	hrací
kostka	kostka	k1gFnSc1	kostka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
