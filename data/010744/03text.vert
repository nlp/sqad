<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Janda	Janda	k1gMnSc1	Janda
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1942	[number]	k4	1942
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
rockový	rockový	k2eAgMnSc1d1	rockový
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
nahrávacího	nahrávací	k2eAgNnSc2d1	nahrávací
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Janda	Janda	k1gMnSc1	Janda
je	být	k5eAaImIp3nS	být
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
frontmanem	frontman	k1gMnSc7	frontman
skupiny	skupina	k1gFnSc2	skupina
Olympic	Olympice	k1gFnPc2	Olympice
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yRgFnSc4	který
složil	složit	k5eAaPmAgMnS	složit
mnoho	mnoho	k4c1	mnoho
hitů	hit	k1gInPc2	hit
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
legend	legenda	k1gFnPc2	legenda
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
české	český	k2eAgFnSc2d1	Česká
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Bratrancem	bratranec	k1gMnSc7	bratranec
Petra	Petr	k1gMnSc2	Petr
Jandy	Janda	k1gMnSc2	Janda
je	být	k5eAaImIp3nS	být
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Luboš	Luboš	k1gMnSc1	Luboš
Andršt	Andršt	k1gMnSc1	Andršt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
jeho	jeho	k3xOp3gFnSc4	jeho
působení	působení	k1gNnSc1	působení
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Sputnici	Sputnice	k1gFnSc6	Sputnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začínal	začínat	k5eAaImAgMnS	začínat
hrát	hrát	k5eAaImF	hrát
s	s	k7c7	s
kytarou	kytara	k1gFnSc7	kytara
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Big	Big	k1gFnSc1	Big
Beat	beat	k1gInSc4	beat
Quintet	Quintet	k1gInSc1	Quintet
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
B.B.Q.	B.B.Q.	k1gMnSc1	B.B.Q.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
jeho	jeho	k3xOp3gMnPc7	jeho
partnery	partner	k1gMnPc7	partner
Pavel	Pavel	k1gMnSc1	Pavel
Chrastina	chrastina	k1gFnSc1	chrastina
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Antonín	Antonín	k1gMnSc1	Antonín
Pacák	Pacák	k1gMnSc1	Pacák
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
Olympic	Olympice	k1gInPc2	Olympice
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
klubu	klub	k1gInSc6	klub
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Spálené	spálený	k2eAgFnSc6d1	spálená
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
účinkoval	účinkovat	k5eAaImAgInS	účinkovat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
Ondráš	Ondráš	k1gFnSc2	Ondráš
podotýká	podotýkat	k5eAaImIp3nS	podotýkat
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Semafor	Semafor	k1gInSc1	Semafor
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
nahrávacích	nahrávací	k2eAgFnPc2d1	nahrávací
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
na	na	k7c4	na
gramofové	gramofový	k2eAgFnPc4d1	gramofový
desky	deska	k1gFnPc4	deska
<g/>
,	,	kIx,	,
do	do	k7c2	do
rádia	rádio	k1gNnSc2	rádio
a	a	k8xC	a
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
časopisem	časopis	k1gInSc7	časopis
Mladý	mladý	k2eAgInSc4d1	mladý
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
možnost	možnost	k1gFnSc1	možnost
vystupovat	vystupovat	k5eAaImF	vystupovat
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
zúčastňovala	zúčastňovat	k5eAaImAgFnS	zúčastňovat
Festivalů	festival	k1gInPc2	festival
politické	politický	k2eAgFnSc2d1	politická
písně	píseň	k1gFnSc2	píseň
Sokolov	Sokolov	k1gInSc1	Sokolov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
sólovou	sólový	k2eAgFnSc4d1	sólová
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
jako	jako	k8xS	jako
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
anketě	anketa	k1gFnSc6	anketa
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
časopisu	časopis	k1gInSc2	časopis
Mladý	mladý	k2eAgInSc1d1	mladý
svět	svět	k1gInSc1	svět
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g />
.	.	kIx.	.
</s>
<s>
<g/>
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
lépe	dobře	k6eAd2	dobře
se	se	k3xPyFc4	se
umisťovala	umisťovat	k5eAaImAgFnS	umisťovat
jím	on	k3xPp3gNnSc7	on
vedená	vedený	k2eAgFnSc1d1	vedená
skupina	skupina	k1gFnSc1	skupina
Olympic	Olympice	k1gInPc2	Olympice
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc6d1	populární
anketě	anketa	k1gFnSc6	anketa
dokonce	dokonce	k9	dokonce
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
trojnásobného	trojnásobný	k2eAgNnSc2d1	trojnásobné
vítězství	vítězství	k1gNnSc3	vítězství
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
tři	tři	k4xCgFnPc4	tři
vítězství	vítězství	k1gNnPc2	vítězství
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
letech	let	k1gInPc6	let
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
v	v	k7c6	v
nástupnické	nástupnický	k2eAgFnSc6d1	nástupnická
soutěži	soutěž	k1gFnSc6	soutěž
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
byl	být	k5eAaImAgMnS	být
Petr	Petr	k1gMnSc1	Petr
Janda	Janda	k1gMnSc1	Janda
předsedou	předseda	k1gMnSc7	předseda
výboru	výbor	k1gInSc2	výbor
Ochranného	ochranný	k2eAgInSc2d1	ochranný
svazu	svaz	k1gInSc2	svaz
autorského	autorský	k2eAgInSc2d1	autorský
(	(	kIx(	(
<g/>
OSA	osa	k1gFnSc1	osa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
v	v	k7c6	v
obvodě	obvod	k1gInSc6	obvod
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
jako	jako	k8xC	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
hnutí	hnutí	k1gNnSc4	hnutí
NEZÁVISLÍ	závislý	k2eNgMnPc1d1	nezávislý
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
byla	být	k5eAaImAgFnS	být
Jana	Jana	k1gFnSc1	Jana
Jandová	Jandová	k1gFnSc1	Jandová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
Martu	Marta	k1gFnSc4	Marta
(	(	kIx(	(
<g/>
*	*	kIx~	*
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
proslavila	proslavit	k5eAaPmAgFnS	proslavit
jako	jako	k9	jako
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
německé	německý	k2eAgFnSc2d1	německá
kapely	kapela	k1gFnSc2	kapela
Die	Die	k1gFnSc2	Die
Happy	Happa	k1gFnSc2	Happa
<g/>
,	,	kIx,	,
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
Petra	Petr	k1gMnSc4	Petr
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prohrál	prohrát	k5eAaPmAgMnS	prohrát
boj	boj	k1gInSc4	boj
s	s	k7c7	s
rakovinou	rakovina	k1gFnSc7	rakovina
o	o	k7c4	o
10	[number]	k4	10
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
druhou	druhý	k4xOgFnSc7	druhý
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
moderátorka	moderátorka	k1gFnSc1	moderátorka
Martina	Martina	k1gFnSc1	Martina
Jandová	Jandová	k1gFnSc1	Jandová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
Elišku	Eliška	k1gFnSc4	Eliška
(	(	kIx(	(
<g/>
*	*	kIx~	*
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
třetí	třetí	k4xOgFnSc7	třetí
<g/>
,	,	kIx,	,
současnou	současný	k2eAgFnSc7d1	současná
manželkou	manželka	k1gFnSc7	manželka
je	být	k5eAaImIp3nS	být
Alice	Alice	k1gFnSc1	Alice
Jandová	Jandová	k1gFnSc1	Jandová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
má	mít	k5eAaImIp3nS	mít
dcery	dcera	k1gFnPc4	dcera
Anežku	Anežka	k1gFnSc4	Anežka
(	(	kIx(	(
<g/>
*	*	kIx~	*
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rozárii	rozárium	k1gNnPc7	rozárium
(	(	kIx(	(
<g/>
*	*	kIx~	*
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
syna	syn	k1gMnSc2	syn
Petra	Petr	k1gMnSc2	Petr
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
vnuky	vnuk	k1gMnPc4	vnuk
Matouše	Matouš	k1gMnPc4	Matouš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
a	a	k8xC	a
Petra	Petra	k1gFnSc1	Petra
(	(	kIx(	(
<g/>
*	*	kIx~	*
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
dcery	dcera	k1gFnSc2	dcera
Marty	Marta	k1gFnSc2	Marta
vnučku	vnučka	k1gFnSc4	vnučka
Marii	Maria	k1gFnSc4	Maria
(	(	kIx(	(
<g/>
*	*	kIx~	*
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kytary	kytara	k1gFnPc4	kytara
a	a	k8xC	a
aparáty	aparát	k1gInPc4	aparát
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
hrál	hrát	k5eAaImAgMnS	hrát
Petr	Petr	k1gMnSc1	Petr
Janda	Janda	k1gMnSc1	Janda
na	na	k7c4	na
české	český	k2eAgFnPc4d1	Česká
kytary	kytara	k1gFnPc4	kytara
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c4	na
model	model	k1gInSc4	model
Jolana	Jolana	k1gFnSc1	Jolana
Grazioso	grazioso	k1gNnSc4	grazioso
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejslavnější	slavný	k2eAgFnSc7d3	nejslavnější
kytarou	kytara	k1gFnSc7	kytara
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
černý	černý	k2eAgInSc1d1	černý
Gibson	Gibson	k1gInSc1	Gibson
Les	les	k1gInSc1	les
Paul	Paula	k1gFnPc2	Paula
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
můžeme	moct	k5eAaImIp1nP	moct
slyšet	slyšet	k5eAaImF	slyšet
na	na	k7c6	na
albech	album	k1gNnPc6	album
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Janda	Janda	k1gMnSc1	Janda
používal	používat	k5eAaImAgMnS	používat
řadu	řada	k1gFnSc4	řada
různých	různý	k2eAgFnPc2d1	různá
kytar	kytara	k1gFnPc2	kytara
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kytary	kytara	k1gFnPc1	kytara
Ibanez	Ibaneza	k1gFnPc2	Ibaneza
a	a	k8xC	a
Fender	Fendra	k1gFnPc2	Fendra
Stratocaster	Stratocastra	k1gFnPc2	Stratocastra
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
aparátů	aparát	k1gInPc2	aparát
týká	týkat	k5eAaImIp3nS	týkat
<g/>
:	:	kIx,	:
většinu	většina	k1gFnSc4	většina
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
používal	používat	k5eAaImAgMnS	používat
lampové	lampový	k2eAgInPc4d1	lampový
zesilovače	zesilovač	k1gInPc4	zesilovač
Marshall	Marshalla	k1gFnPc2	Marshalla
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
přešel	přejít	k5eAaPmAgMnS	přejít
na	na	k7c4	na
lampové	lampový	k2eAgInPc4d1	lampový
aparáty	aparát	k1gInPc4	aparát
Hughes	Hughes	k1gMnSc1	Hughes
&	&	k?	&
Kettner	Kettner	k1gMnSc1	Kettner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
Olympic	Olympice	k1gFnPc2	Olympice
50	[number]	k4	50
let	léto	k1gNnPc2	léto
v	v	k7c6	v
O2	O2	k1gFnSc6	O2
areně	areně	k6eAd1	areně
použil	použít	k5eAaPmAgMnS	použít
kytaru	kytara	k1gFnSc4	kytara
Ibanez	Ibaneza	k1gFnPc2	Ibaneza
JEM	JEM	k?	JEM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Petr	Petr	k1gMnSc1	Petr
Janda	Janda	k1gMnSc1	Janda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Petr	Petr	k1gMnSc1	Petr
Janda	Janda	k1gMnSc1	Janda
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
komnata	komnata	k1gFnSc1	komnata
Petra	Petr	k1gMnSc2	Petr
Jandy	Janda	k1gMnSc2	Janda
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Janda	Janda	k1gMnSc1	Janda
-	-	kIx~	-
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Na	na	k7c6	na
plovárně	plovárna	k1gFnSc6	plovárna
</s>
</p>
