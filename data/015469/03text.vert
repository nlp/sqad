<s>
Kandela	kandela	k1gFnSc1
</s>
<s>
Kandela	kandela	k1gFnSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
cd	cd	kA
<g/>
,	,	kIx,
angl.	angl.	k?
název	název	k1gInSc1
jednotky	jednotka	k1gFnSc2
je	být	k5eAaImIp3nS
candela	candela	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
svítivosti	svítivost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
sedmi	sedm	k4xCc2
základních	základní	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Aktuální	aktuální	k2eAgFnPc1d1
definice	definice	k1gFnPc1
</s>
<s>
1	#num#	k4
kandela	kandela	k1gFnSc1
je	být	k5eAaImIp3nS
svítivost	svítivost	k1gFnSc4
světelného	světelný	k2eAgInSc2d1
zdroje	zdroj	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
v	v	k7c6
daném	daný	k2eAgInSc6d1
směru	směr	k1gInSc6
vyzařuje	vyzařovat	k5eAaImIp3nS
monochromatické	monochromatický	k2eAgNnSc1d1
záření	záření	k1gNnSc1
o	o	k7c4
frekvenci	frekvence	k1gFnSc4
540	#num#	k4
THz	THz	k1gFnPc2
a	a	k8xC
jehož	jehož	k3xOyRp3gFnSc1
zářivost	zářivost	k1gFnSc1
(	(	kIx(
<g/>
zářivá	zářivý	k2eAgFnSc1d1
intenzita	intenzita	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
tomto	tento	k3xDgInSc6
směru	směr	k1gInSc6
činí	činit	k5eAaImIp3nS
1	#num#	k4
<g/>
/	/	kIx~
<g/>
683	#num#	k4
wattů	watt	k1gInPc2
na	na	k7c4
steradián	steradián	k1gInSc4
(	(	kIx(
<g/>
přesně	přesně	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Komentář	komentář	k1gInSc1
definice	definice	k1gFnSc2
</s>
<s>
Křivka	křivka	k1gFnSc1
poměrné	poměrný	k2eAgFnSc2d1
světelné	světelný	k2eAgFnSc2d1
účinnosti	účinnost	k1gFnSc2
</s>
<s>
Svítivost	svítivost	k1gFnSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
fotometrických	fotometrický	k2eAgFnPc2d1
veličin	veličina	k1gFnPc2
<g/>
,	,	kIx,
tedy	tedy	k9
veličin	veličina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
respektují	respektovat	k5eAaImIp3nP
odlišnou	odlišný	k2eAgFnSc4d1
citlivost	citlivost	k1gFnSc4
lidského	lidský	k2eAgNnSc2d1
oka	oko	k1gNnSc2
k	k	k7c3
různým	různý	k2eAgFnPc3d1
vlnovým	vlnový	k2eAgFnPc3d1
délkám	délka	k1gFnPc3
světelného	světelný	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
definici	definice	k1gFnSc6
zvolená	zvolený	k2eAgFnSc1d1
frekvence	frekvence	k1gFnSc1
540	#num#	k4
THz	THz	k1gMnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
uprostřed	uprostřed	k7c2
oblasti	oblast	k1gFnSc2
viditelného	viditelný	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
<g/>
,	,	kIx,
blízká	blízký	k2eAgFnSc1d1
světlu	světlo	k1gNnSc3
zelené	zelený	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
při	při	k7c6
vlnové	vlnový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
555	#num#	k4
nm	nm	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
vlnovou	vlnový	k2eAgFnSc4d1
délku	délka	k1gFnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
je	být	k5eAaImIp3nS
lidské	lidský	k2eAgNnSc1d1
oko	oko	k1gNnSc1
při	při	k7c6
denním	denní	k2eAgNnSc6d1
vidění	vidění	k1gNnSc6
nejcitlivější	citlivý	k2eAgMnSc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definice	definice	k1gFnSc1
nám	my	k3xPp1nPc3
tedy	tedy	k9
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
toto	tento	k3xDgNnSc4
monochromatické	monochromatický	k2eAgNnSc4d1
světlo	světlo	k1gNnSc4
platí	platit	k5eAaImIp3nS
přepočet	přepočet	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
1	#num#	k4
cd	cd	kA
~	~	kIx~
1	#num#	k4
<g/>
/	/	kIx~
<g/>
683	#num#	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
sr	sr	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
jakékoli	jakýkoli	k3yIgFnPc4
jiné	jiný	k2eAgFnPc4d1
vlnové	vlnový	k2eAgFnPc4d1
délky	délka	k1gFnPc4
pak	pak	k6eAd1
tento	tento	k3xDgInSc1
koeficient	koeficient	k1gInSc1
683	#num#	k4
lm	lm	k?
<g/>
/	/	kIx~
<g/>
W	W	kA
musíme	muset	k5eAaImIp1nP
vynásobit	vynásobit	k5eAaPmF
příslušnou	příslušný	k2eAgFnSc7d1
hodnotou	hodnota	k1gFnSc7
podle	podle	k7c2
křivky	křivka	k1gFnSc2
poměrné	poměrný	k2eAgFnSc2d1
světelné	světelný	k2eAgFnSc2d1
účinnosti	účinnost	k1gFnSc2
(	(	kIx(
<g/>
křivky	křivka	k1gFnSc2
spektrální	spektrální	k2eAgFnSc2d1
citlivosti	citlivost	k1gFnSc3
lidského	lidský	k2eAgNnSc2d1
oka	oko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
pohledu	pohled	k1gInSc2
fotometrických	fotometrický	k2eAgInPc2d1
výpočtů	výpočet	k1gInPc2
je	být	k5eAaImIp3nS
tedy	tedy	k9
standardní	standardní	k2eAgFnSc1d1
definice	definice	k1gFnSc1
křivky	křivka	k1gFnSc2
poměrné	poměrný	k2eAgFnSc2d1
světelné	světelný	k2eAgFnSc2d1
účinnosti	účinnost	k1gFnSc2
monochromatického	monochromatický	k2eAgNnSc2d1
záření	záření	k1gNnSc2
stejně	stejně	k6eAd1
důležitá	důležitý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
definiční	definiční	k2eAgInSc1d1
koeficient	koeficient	k1gInSc1
683	#num#	k4
lm	lm	k?
<g/>
/	/	kIx~
<g/>
W.	W.	kA
</s>
<s>
Starší	starý	k2eAgFnSc1d2
definice	definice	k1gFnSc1
</s>
<s>
Nejprve	nejprve	k6eAd1
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
jednotka	jednotka	k1gFnSc1
definována	definovat	k5eAaBmNgFnS
jako	jako	k8xS,k8xC
svítivost	svítivost	k1gFnSc1
svíčky	svíčka	k1gFnSc2
definovaného	definovaný	k2eAgNnSc2d1
složení	složení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typů	typ	k1gInPc2
referenčních	referenční	k2eAgFnPc2d1
svíček	svíčka	k1gFnPc2
však	však	k9
existovalo	existovat	k5eAaImAgNnS
několik	několik	k4yIc4
(	(	kIx(
<g/>
a	a	k8xC
tomu	ten	k3xDgNnSc3
odpovídalo	odpovídat	k5eAaImAgNnS
několik	několik	k4yIc1
mírně	mírně	k6eAd1
různých	různý	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
navíc	navíc	k6eAd1
bylo	být	k5eAaImAgNnS
složité	složitý	k2eAgNnSc1d1
zachovat	zachovat	k5eAaPmF
přesně	přesně	k6eAd1
stejné	stejný	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
hoření	hoření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
proto	proto	k8xC
jednotka	jednotka	k1gFnSc1
předefinována	předefinován	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
svítivost	svítivost	k1gFnSc1
1	#num#	k4
<g/>
/	/	kIx~
<g/>
600	#num#	k4
000	#num#	k4
m²	m²	k?
povrchu	povrch	k1gInSc2
absolutně	absolutně	k6eAd1
černého	černý	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
ve	v	k7c6
směru	směr	k1gInSc6
kolmém	kolmý	k2eAgInSc6d1
k	k	k7c3
tomuto	tento	k3xDgInSc3
povrchu	povrch	k1gInSc3
při	při	k7c6
teplotě	teplota	k1gFnSc6
tuhnutí	tuhnutí	k1gNnSc2
platiny	platina	k1gFnSc2
(	(	kIx(
<g/>
1768	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
při	při	k7c6
normálním	normální	k2eAgInSc6d1
tlaku	tlak	k1gInSc6
(	(	kIx(
<g/>
101	#num#	k4
325	#num#	k4
Pa	Pa	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
definice	definice	k1gFnPc4
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
na	na	k7c4
XIII	XIII	kA
<g/>
.	.	kIx.
generální	generální	k2eAgFnSc4d1
konferenci	konference	k1gFnSc4
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
v	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Současná	současný	k2eAgFnSc1d1
definice	definice	k1gFnSc1
platí	platit	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1979	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Všechny	všechen	k3xTgFnPc1
definice	definice	k1gFnPc1
popisují	popisovat	k5eAaImIp3nP
prakticky	prakticky	k6eAd1
stejnou	stejný	k2eAgFnSc4d1
jednotkovou	jednotkový	k2eAgFnSc4d1
svítivost	svítivost	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
stále	stále	k6eAd1
odpovídá	odpovídat	k5eAaImIp3nS
svítivosti	svítivost	k1gFnSc2
plamene	plamen	k1gInSc2
jedné	jeden	k4xCgFnSc2
běžné	běžný	k2eAgFnSc2d1
svíčky	svíčka	k1gFnSc2
ve	v	k7c6
vodorovném	vodorovný	k2eAgInSc6d1
směru	směr	k1gInSc6
(	(	kIx(
<g/>
plamen	plamen	k1gInSc1
je	být	k5eAaImIp3nS
vertikálně	vertikálně	k6eAd1
protáhlý	protáhlý	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
ve	v	k7c6
svislém	svislý	k2eAgInSc6d1
směru	směr	k1gInSc6
jeho	jeho	k3xOp3gFnSc4
svítivost	svítivost	k1gFnSc4
menší	malý	k2eAgInPc1d2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
porovnání	porovnání	k1gNnSc4
<g/>
:	:	kIx,
obyčejná	obyčejný	k2eAgFnSc1d1
žárovka	žárovka	k1gFnSc1
100	#num#	k4
W	W	kA
má	mít	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
120	#num#	k4
cd	cd	kA
<g/>
;	;	kIx,
běžná	běžný	k2eAgFnSc1d1
indikační	indikační	k2eAgFnSc1d1
světelná	světelný	k2eAgFnSc1d1
(	(	kIx(
<g/>
LED	LED	kA
<g/>
)	)	kIx)
dioda	dioda	k1gFnSc1
jen	jen	k9
asi	asi	k9
0,5	0,5	k4
cd	cd	kA
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
názvu	název	k1gInSc2
</s>
<s>
Název	název	k1gInSc1
jednotky	jednotka	k1gFnSc2
byl	být	k5eAaImAgInS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
latinského	latinský	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
candela	candel	k1gMnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
svíce	svíce	k1gFnSc1
<g/>
,	,	kIx,
svíčka	svíčka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vztah	vztah	k1gInSc1
mezi	mezi	k7c7
lumenem	lumen	k1gInSc7
a	a	k8xC
kandelou	kandela	k1gFnSc7
</s>
<s>
U	u	k7c2
zdrojů	zdroj	k1gInPc2
světla	světlo	k1gNnSc2
s	s	k7c7
usměrněným	usměrněný	k2eAgInSc7d1
svitem	svit	k1gInSc7
(	(	kIx(
<g/>
např.	např.	kA
LED	LED	kA
<g/>
)	)	kIx)
výrobci	výrobce	k1gMnPc1
uvádějí	uvádět	k5eAaImIp3nP
svítivost	svítivost	k1gFnSc4
právě	právě	k9
v	v	k7c6
cd	cd	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
jednotka	jednotka	k1gFnSc1
je	být	k5eAaImIp3nS
však	však	k9
ne	ne	k9
vždy	vždy	k6eAd1
přímo	přímo	k6eAd1
porovnávatelná	porovnávatelný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musíme	muset	k5eAaImIp1nP
k	k	k7c3
ní	on	k3xPp3gFnSc3
znát	znát	k5eAaImF
ještě	ještě	k9
další	další	k2eAgInSc1d1
údaj	údaj	k1gInSc1
–	–	k?
úhel	úhel	k1gInSc4
rotačního	rotační	k2eAgInSc2d1
kužele	kužel	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgNnSc6,k3yQgNnSc6,k3yIgNnSc6
zdroj	zdroj	k1gInSc1
vyzařuje	vyzařovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vztah	vztah	k1gInSc1
lm-cd	lm-cd	k6eAd1
je	být	k5eAaImIp3nS
potom	potom	k6eAd1
dán	dát	k5eAaPmNgInS
poměrem	poměr	k1gInSc7
plochy	plocha	k1gFnSc2
kruhové	kruhový	k2eAgFnSc2d1
úseče	úseč	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
tento	tento	k3xDgInSc4
kužel	kužel	k1gInSc1
vytne	vytnout	k5eAaPmIp3nS
na	na	k7c6
povrchu	povrch	k1gInSc6
koule	koule	k1gFnSc2
a	a	k8xC
plochy	plocha	k1gFnSc2
kruhové	kruhový	k2eAgFnSc2d1
úseče	úseč	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
vytne	vytnout	k5eAaPmIp3nS
prostorový	prostorový	k2eAgInSc4d1
úhel	úhel	k1gInSc4
1	#num#	k4
steradián	steradián	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
můžeme	moct	k5eAaImIp1nP
přepočítat	přepočítat	k5eAaPmF
svítivost	svítivost	k1gFnSc4
[	[	kIx(
<g/>
cd	cd	kA
<g/>
]	]	kIx)
na	na	k7c4
světelný	světelný	k2eAgInSc4d1
tok	tok	k1gInSc4
[	[	kIx(
<g/>
lm	lm	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
již	již	k6eAd1
přímou	přímý	k2eAgFnSc4d1
návaznost	návaznost	k1gFnSc4
na	na	k7c4
výkon	výkon	k1gInSc4
všesměrového	všesměrový	k2eAgMnSc2d1
<g/>
,	,	kIx,
bodového	bodový	k2eAgInSc2d1
zdroje	zdroj	k1gInSc2
záření	záření	k1gNnSc2
a	a	k8xC
lze	lze	k6eAd1
ho	on	k3xPp3gInSc4
porovnávat	porovnávat	k5eAaImF
s	s	k7c7
dalšími	další	k2eAgInPc7d1
zdroji	zdroj	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinými	jiný	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
–	–	k?
když	když	k8xS
mají	mít	k5eAaImIp3nP
2	#num#	k4
LED	LED	kA
stejnou	stejný	k2eAgFnSc4d1
svítivost	svítivost	k1gFnSc4
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
nemusí	muset	k5eNaImIp3nS
mít	mít	k5eAaImF
stejný	stejný	k2eAgInSc4d1
světelný	světelný	k2eAgInSc4d1
tok	tok	k1gInSc4
a	a	k8xC
tím	ten	k3xDgNnSc7
ani	ani	k9
výkon	výkon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vyzařuje	vyzařovat	k5eAaImIp3nS
<g/>
-li	-li	k?
zdroj	zdroj	k1gInSc4
určitou	určitý	k2eAgFnSc7d1
svítivostí	svítivost	k1gFnSc7
Iv	Iva	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
kandelách	kandela	k1gFnPc6
<g/>
)	)	kIx)
v	v	k7c6
definovaném	definovaný	k2eAgInSc6d1
prostorovém	prostorový	k2eAgInSc6d1
úhlu	úhel	k1gInSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
celkový	celkový	k2eAgInSc4d1
světelný	světelný	k2eAgInSc4d1
tok	tok	k1gInSc4
Φ	Φ	k1gFnPc2
v	v	k7c6
lumenech	lumen	k1gInPc6
dán	dát	k5eAaPmNgInS
vztahem	vztah	k1gInSc7
<g/>
:	:	kIx,
</s>
<s>
[	[	kIx(
</s>
<s>
l	l	kA
</s>
<s>
m	m	kA
</s>
<s>
]	]	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
[	[	kIx(
</s>
<s>
c	c	k0
</s>
<s>
d	d	k?
</s>
<s>
]	]	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
(	(	kIx(
</s>
<s>
β	β	k?
</s>
<s>
/	/	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
[	[	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
lm	lm	k?
<g/>
}	}	kIx)
]	]	kIx)
<g/>
=	=	kIx~
<g/>
[	[	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
cd	cd	kA
<g/>
}	}	kIx)
]	]	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
2	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
\	\	kIx~
<g/>
cdot	cdotum	k1gNnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
\	\	kIx~
<g/>
cos	cos	kA
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
/	/	kIx~
<g/>
2	#num#	k4
<g/>
))	))	k?
<g/>
}	}	kIx)
</s>
<s>
kde	kde	k6eAd1
β	β	k?
je	být	k5eAaImIp3nS
úhel	úhel	k1gInSc1
vyzařování	vyzařování	k1gNnSc2
svítidla	svítidlo	k1gNnSc2
—	—	k?
<g/>
celkový	celkový	k2eAgInSc4d1
vrcholový	vrcholový	k2eAgInSc4d1
úhel	úhel	k1gInSc4
světelného	světelný	k2eAgInSc2d1
kužele	kužel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
lampa	lampa	k1gFnSc1
se	s	k7c7
svítivostí	svítivost	k1gFnSc7
590	#num#	k4
cd	cd	kA
a	a	k8xC
vrcholovým	vrcholový	k2eAgInSc7d1
úhlem	úhel	k1gInSc7
40	#num#	k4
<g/>
°	°	k?
vyzařuje	vyzařovat	k5eAaImIp3nS
224	#num#	k4
lumenů	lumen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
zdroj	zdroj	k1gInSc1
vyzařuje	vyzařovat	k5eAaImIp3nS
světlo	světlo	k1gNnSc4
rovnoměrně	rovnoměrně	k6eAd1
ve	v	k7c6
všech	všecek	k3xTgInPc6
směrech	směr	k1gInPc6
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
tok	tok	k1gInSc1
vypočten	vypočíst	k5eAaPmNgInS
násobením	násobení	k1gNnSc7
svítivosti	svítivost	k1gFnSc2
konstantou	konstanta	k1gFnSc7
4	#num#	k4
<g/>
π	π	k?
<g/>
:	:	kIx,
všesměrový	všesměrový	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
1	#num#	k4
kandela	kandela	k1gFnSc1
vyzařuje	vyzařovat	k5eAaImIp3nS
12,6	12,6	k4
lumenů	lumen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
kandela	kandela	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Soustava	soustava	k1gFnSc1
SI	si	k1gNnSc2
základní	základní	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
</s>
<s>
ampér	ampér	k1gInSc1
</s>
<s>
kandela	kandela	k1gFnSc1
</s>
<s>
kelvin	kelvin	k1gInSc1
</s>
<s>
kilogram	kilogram	k1gInSc1
</s>
<s>
metr	metr	k1gInSc1
</s>
<s>
mol	mol	k1gMnSc1
</s>
<s>
sekunda	sekunda	k1gFnSc1
odvozené	odvozený	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
</s>
<s>
becquerel	becquerel	k1gInSc1
</s>
<s>
coulomb	coulomb	k1gInSc1
</s>
<s>
farad	farad	k1gInSc1
</s>
<s>
gray	graa	k1gFnPc1
</s>
<s>
henry	henry	k1gInSc1
</s>
<s>
hertz	hertz	k1gInSc1
</s>
<s>
joule	joule	k1gInSc1
</s>
<s>
katal	katal	k1gMnSc1
</s>
<s>
lumen	lumen	k1gMnSc1
</s>
<s>
lux	lux	k1gInSc1
</s>
<s>
newton	newton	k1gInSc1
</s>
<s>
ohm	ohm	k1gInSc1
</s>
<s>
pascal	pascal	k1gInSc1
</s>
<s>
radián	radián	k1gInSc1
</s>
<s>
siemens	siemens	k1gInSc1
</s>
<s>
sievert	sievert	k1gMnSc1
</s>
<s>
steradián	steradián	k1gInSc1
</s>
<s>
stupeň	stupeň	k1gInSc1
Celsia	Celsius	k1gMnSc2
</s>
<s>
tesla	tesla	k1gFnSc1
</s>
<s>
volt	volt	k1gInSc1
</s>
<s>
watt	watt	k1gInSc1
</s>
<s>
weber	weber	k1gInSc1
další	další	k2eAgFnSc2d1
</s>
<s>
předpony	předpona	k1gFnPc1
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
</s>
<s>
systémy	systém	k1gInPc1
měření	měření	k1gNnSc2
</s>
<s>
převody	převod	k1gInPc1
jednotek	jednotka	k1gFnPc2
</s>
<s>
nové	nový	k2eAgFnPc1d1
definice	definice	k1gFnPc1
SI	se	k3xPyFc3
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fyzika	fyzika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1025833252	#num#	k4
</s>
