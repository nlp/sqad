<s>
Nemoc	nemoc	k1gFnSc1	nemoc
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
bakterie	bakterie	k1gFnSc1	bakterie
Paenibacillus	Paenibacillus	k1gInSc4	Paenibacillus
larvae	larvaat	k5eAaPmIp3nS	larvaat
larvae	larvae	k1gNnSc1	larvae
<g/>
,	,	kIx,	,
synonymum	synonymum	k1gNnSc1	synonymum
Bacillus	Bacillus	k1gInSc1	Bacillus
larvae	larvae	k1gFnSc1	larvae
(	(	kIx(	(
<g/>
White	Whit	k1gMnSc5	Whit
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
