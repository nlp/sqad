<s>
Mor	mor	k1gInSc1	mor
včelího	včelí	k2eAgInSc2d1	včelí
plodu	plod	k1gInSc2	plod
(	(	kIx(	(
<g/>
Histolysis	Histolysis	k1gFnSc1	Histolysis
infectiosa	infectiosa	k1gFnSc1	infectiosa
perniciosa	perniciosa	k1gFnSc1	perniciosa
larvae	larvae	k1gInSc4	larvae
apium	apium	k1gInSc1	apium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
závažné	závažný	k2eAgNnSc1d1	závažné
onemocnění	onemocnění	k1gNnSc1	onemocnění
včelích	včelí	k2eAgFnPc2d1	včelí
larev	larva	k1gFnPc2	larva
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
bakterie	bakterie	k1gFnSc1	bakterie
Paenibacillus	Paenibacillus	k1gInSc4	Paenibacillus
larvae	larvaat	k5eAaPmIp3nS	larvaat
larvae	larvae	k1gNnSc1	larvae
<g/>
,	,	kIx,	,
synonymum	synonymum	k1gNnSc1	synonymum
Bacillus	Bacillus	k1gInSc1	Bacillus
larvae	larvae	k1gFnSc1	larvae
(	(	kIx(	(
<g/>
White	Whit	k1gMnSc5	Whit
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
odolné	odolný	k2eAgInPc1d1	odolný
spory	spor	k1gInPc1	spor
přežívají	přežívat	k5eAaImIp3nP	přežívat
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
a	a	k8xC	a
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
dlouhodobé	dlouhodobý	k2eAgNnSc4d1	dlouhodobé
zamoření	zamoření	k1gNnSc4	zamoření
oblasti	oblast	k1gFnSc2	oblast
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Mor	mor	k1gInSc1	mor
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
nedostatečné	dostatečný	k2eNgFnSc2d1	nedostatečná
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
včelstva	včelstvo	k1gNnPc4	včelstvo
(	(	kIx(	(
<g/>
nezajištění	nezajištění	k1gNnSc3	nezajištění
čistoty	čistota	k1gFnSc2	čistota
úlů	úl	k1gInPc2	úl
<g/>
)	)	kIx)	)
a	a	k8xC	a
u	u	k7c2	u
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgFnPc2d1	žijící
včel	včela	k1gFnPc2	včela
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Mor	mor	k1gInSc1	mor
včelího	včelí	k2eAgInSc2d1	včelí
plodu	plod	k1gInSc2	plod
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
radikálním	radikální	k2eAgInPc3d1	radikální
postupům	postup	k1gInPc3	postup
při	při	k7c6	při
tlumení	tlumení	k1gNnSc6	tlumení
nákazy	nákaza	k1gFnSc2	nákaza
podařilo	podařit	k5eAaPmAgNnS	podařit
výskyt	výskyt	k1gInSc4	výskyt
choroby	choroba	k1gFnSc2	choroba
snížit	snížit	k5eAaPmF	snížit
na	na	k7c4	na
desetiny	desetina	k1gFnPc4	desetina
procenta	procento	k1gNnSc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Mapa	mapa	k1gFnSc1	mapa
situace	situace	k1gFnSc2	situace
tak	tak	k6eAd1	tak
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
monitoruje	monitorovat	k5eAaImIp3nS	monitorovat
SVS	SVS	kA	SVS
-	-	kIx~	-
Státní	státní	k2eAgFnSc1d1	státní
veterinární	veterinární	k2eAgFnSc1d1	veterinární
správa	správa	k1gFnSc1	správa
(	(	kIx(	(
<g/>
MZe	mha	k1gFnSc6	mha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
http://eagri.cz/public/app/svs_pub/mapy_nak/#mapa=MVP.	[url]	k?	http://eagri.cz/public/app/svs_pub/mapy_nak/#mapa=MVP.
Jediným	jediný	k2eAgMnSc7d1	jediný
původcem	původce	k1gMnSc7	původce
moru	mor	k1gInSc2	mor
včelího	včelí	k2eAgInSc2d1	včelí
plodu	plod	k1gInSc2	plod
je	být	k5eAaImIp3nS	být
mikrob	mikrob	k1gInSc1	mikrob
Paenibacillus	Paenibacillus	k1gInSc1	Paenibacillus
larvae	larvaat	k5eAaPmIp3nS	larvaat
larvae	larvae	k1gFnSc4	larvae
tyčinkovitého	tyčinkovitý	k2eAgInSc2d1	tyčinkovitý
tvaru	tvar	k1gInSc2	tvar
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
2,5	[number]	k4	2,5
<g/>
-	-	kIx~	-
<g/>
8,5	[number]	k4	8,5
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc4	pohyb
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
bičíky	bičík	k1gInPc1	bičík
vyrůstající	vyrůstající	k2eAgInPc1d1	vyrůstající
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
povrchu	povrch	k1gInSc6	povrch
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
života	život	k1gInSc2	život
a	a	k8xC	a
dělení	dělení	k1gNnSc1	dělení
<g/>
,	,	kIx,	,
tyčinka	tyčinka	k1gFnSc1	tyčinka
zduří	zduřet	k5eAaPmIp3nS	zduřet
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
se	se	k3xPyFc4	se
vřetenovité	vřetenovitý	k2eAgNnSc1d1	vřetenovité
nebo	nebo	k8xC	nebo
kyjovité	kyjovitý	k2eAgNnSc1d1	kyjovité
sporangium	sporangium	k1gNnSc1	sporangium
a	a	k8xC	a
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
oválná	oválný	k2eAgFnSc1d1	oválná
spora	spora	k1gFnSc1	spora
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
1,2	[number]	k4	1,2
<g/>
-	-	kIx~	-
<g/>
1,9	[number]	k4	1,9
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Včelí	včelí	k2eAgInSc4d1	včelí
plod	plod	k1gInSc4	plod
(	(	kIx(	(
<g/>
larvy	larva	k1gFnSc2	larva
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nakazí	nakazit	k5eAaPmIp3nP	nakazit
sporami	spora	k1gFnPc7	spora
mikroba	mikrob	k1gMnSc2	mikrob
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Spora	spora	k1gFnSc1	spora
v	v	k7c6	v
žaludku	žaludek	k1gInSc6	žaludek
larvy	larva	k1gFnSc2	larva
do	do	k7c2	do
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
vyklíčí	vyklíčit	k5eAaPmIp3nS	vyklíčit
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nP	množit
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
vzrůstajícím	vzrůstající	k2eAgNnSc7d1	vzrůstající
množstvím	množství	k1gNnSc7	množství
patogena	patogena	k1gFnSc1	patogena
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
koncentrace	koncentrace	k1gFnSc1	koncentrace
toxických	toxický	k2eAgInPc2d1	toxický
enzymů	enzym	k1gInPc2	enzym
a	a	k8xC	a
tělní	tělní	k2eAgFnPc1d1	tělní
buňky	buňka	k1gFnPc1	buňka
larvy	larva	k1gFnSc2	larva
se	se	k3xPyFc4	se
doslova	doslova	k6eAd1	doslova
rozpouštějí	rozpouštět	k5eAaImIp3nP	rozpouštět
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládající	rozkládající	k2eAgFnSc1d1	rozkládající
se	se	k3xPyFc4	se
tkáňová	tkáňový	k2eAgFnSc1d1	tkáňová
struktura	struktura	k1gFnSc1	struktura
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
mikrobu	mikrob	k1gInSc2	mikrob
pronikat	pronikat	k5eAaImF	pronikat
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
částí	část	k1gFnPc2	část
těla	tělo	k1gNnSc2	tělo
larvy	larva	k1gFnSc2	larva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavíčkování	zavíčkování	k1gNnSc6	zavíčkování
buňky	buňka	k1gFnSc2	buňka
hyne	hynout	k5eAaImIp3nS	hynout
larva	larva	k1gFnSc1	larva
na	na	k7c4	na
celkovou	celkový	k2eAgFnSc4d1	celková
sepsi	sepse	k1gFnSc4	sepse
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
jejího	její	k3xOp3gNnSc2	její
těla	tělo	k1gNnSc2	tělo
postupně	postupně	k6eAd1	postupně
vysychají	vysychat	k5eAaImIp3nP	vysychat
a	a	k8xC	a
mění	měnit	k5eAaImIp3nP	měnit
se	se	k3xPyFc4	se
v	v	k7c4	v
hnědočerný	hnědočerný	k2eAgInSc4d1	hnědočerný
příškvar	příškvar	k1gInSc4	příškvar
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnPc1	včela
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
příškvar	příškvar	k1gInSc1	příškvar
odstranit	odstranit	k5eAaPmF	odstranit
a	a	k8xC	a
mimoděk	mimoděk	k6eAd1	mimoděk
roznášejí	roznášet	k5eAaImIp3nP	roznášet
infekci	infekce	k1gFnSc4	infekce
po	po	k7c6	po
plástech	plást	k1gInPc6	plást
<g/>
.	.	kIx.	.
</s>
<s>
Dospělé	dospělý	k2eAgFnPc1d1	dospělá
včely	včela	k1gFnPc1	včela
jsou	být	k5eAaImIp3nP	být
vůči	vůči	k7c3	vůči
nákaze	nákaza	k1gFnSc3	nákaza
odolné	odolný	k2eAgFnSc3d1	odolná
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
žaludku	žaludek	k1gInSc6	žaludek
nevyklíčí	vyklíčit	k5eNaPmIp3nP	vyklíčit
<g/>
,	,	kIx,	,
uchovají	uchovat	k5eAaPmIp3nP	uchovat
si	se	k3xPyFc3	se
však	však	k9	však
životaschopnost	životaschopnost	k1gFnSc4	životaschopnost
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
výkaly	výkal	k1gInPc7	výkal
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
mimo	mimo	k7c4	mimo
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
znovu	znovu	k6eAd1	znovu
zdrojem	zdroj	k1gInSc7	zdroj
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Chorobou	choroba	k1gFnSc7	choroba
oslabené	oslabený	k2eAgFnPc1d1	oslabená
a	a	k8xC	a
hynoucí	hynoucí	k2eAgNnSc1d1	hynoucí
včelstvo	včelstvo	k1gNnSc1	včelstvo
provokuje	provokovat	k5eAaImIp3nS	provokovat
loupeživý	loupeživý	k2eAgInSc4d1	loupeživý
nálet	nálet	k1gInSc4	nálet
cizích	cizí	k2eAgFnPc2d1	cizí
včel	včela	k1gFnPc2	včela
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
potenciálním	potenciální	k2eAgInSc7d1	potenciální
zdrojem	zdroj	k1gInSc7	zdroj
šíření	šíření	k1gNnSc2	šíření
nákazy	nákaza	k1gFnSc2	nákaza
do	do	k7c2	do
dalších	další	k2eAgNnPc2d1	další
včelstev	včelstvo	k1gNnPc2	včelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Loupeží	loupež	k1gFnSc7	loupež
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
naráz	naráz	k6eAd1	naráz
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
spor	spor	k1gInSc1	spor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
důležitých	důležitý	k2eAgInPc2d1	důležitý
předpokladů	předpoklad	k1gInPc2	předpoklad
propuknutí	propuknutí	k1gNnSc2	propuknutí
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
nákazy	nákaza	k1gFnSc2	nákaza
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
infikovanými	infikovaný	k2eAgFnPc7d1	infikovaná
zalétnuvšími	zalétnuvší	k2eAgFnPc7d1	zalétnuvší
včelami	včela	k1gFnPc7	včela
či	či	k8xC	či
trubci	trubec	k1gMnPc7	trubec
nebyl	být	k5eNaImAgInS	být
prokázán	prokázán	k2eAgInSc1d1	prokázán
<g/>
.	.	kIx.	.
</s>
<s>
Napadení	napadení	k1gNnSc1	napadení
morem	mor	k1gInSc7	mor
včelího	včelí	k2eAgInSc2d1	včelí
plodu	plod	k1gInSc2	plod
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
až	až	k9	až
po	po	k7c6	po
zavíčkování	zavíčkování	k1gNnSc6	zavíčkování
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Víčka	víčko	k1gNnPc1	víčko
nakažených	nakažený	k2eAgFnPc2d1	nakažená
buněk	buňka	k1gFnPc2	buňka
jsou	být	k5eAaImIp3nP	být
nápadně	nápadně	k6eAd1	nápadně
ztmavlá	ztmavlý	k2eAgNnPc1d1	ztmavlé
<g/>
,	,	kIx,	,
propadlá	propadlý	k2eAgNnPc1d1	propadlé
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
perforovaná	perforovaný	k2eAgNnPc1d1	perforované
<g/>
.	.	kIx.	.
</s>
<s>
Nemocné	mocný	k2eNgFnPc1d1	mocný
larvy	larva	k1gFnPc1	larva
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
perleťový	perleťový	k2eAgInSc4d1	perleťový
lesk	lesk	k1gInSc4	lesk
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
na	na	k7c4	na
šedobílou	šedobílý	k2eAgFnSc4d1	šedobílá
<g/>
,	,	kIx,	,
šedožlutou	šedožlutý	k2eAgFnSc4d1	šedožlutá
až	až	k8xS	až
tmavě	tmavě	k6eAd1	tmavě
hnědou	hnědý	k2eAgFnSc4d1	hnědá
lepkavou	lepkavý	k2eAgFnSc4d1	lepkavá
hmotu	hmota	k1gFnSc4	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Napadený	napadený	k2eAgInSc1d1	napadený
plod	plod	k1gInSc1	plod
páchne	páchnout	k5eAaImIp3nS	páchnout
klihem	klih	k1gInSc7	klih
<g/>
.	.	kIx.	.
</s>
<s>
Tenkým	tenký	k2eAgNnSc7d1	tenké
dřívkem	dřívko	k1gNnSc7	dřívko
lze	lze	k6eAd1	lze
z	z	k7c2	z
kašovité	kašovitý	k2eAgFnSc2d1	kašovitá
hmoty	hmota	k1gFnSc2	hmota
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
až	až	k9	až
několik	několik	k4yIc4	několik
cm	cm	kA	cm
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
lepkavou	lepkavý	k2eAgFnSc4d1	lepkavá
nitku	nitka	k1gFnSc4	nitka
<g/>
.	.	kIx.	.
</s>
<s>
Vyschlé	vyschlý	k2eAgFnPc1d1	vyschlá
napadené	napadený	k2eAgFnPc1d1	napadená
buňky	buňka	k1gFnPc1	buňka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
u	u	k7c2	u
dna	dno	k1gNnSc2	dno
přilepený	přilepený	k2eAgInSc4d1	přilepený
hnědočerný	hnědočerný	k2eAgInSc4d1	hnědočerný
příškvar	příškvar	k1gInSc4	příškvar
<g/>
.	.	kIx.	.
</s>
<s>
Hmota	hmota	k1gFnSc1	hmota
příškvaru	příškvar	k1gInSc2	příškvar
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
spor	spora	k1gFnPc2	spora
P.	P.	kA	P.
l.	l.	k?	l.
larvae	larvae	k1gInSc1	larvae
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
nákazy	nákaza	k1gFnSc2	nákaza
není	být	k5eNaImIp3nS	být
nemoc	nemoc	k1gFnSc1	nemoc
příliš	příliš	k6eAd1	příliš
nápadná	nápadný	k2eAgFnSc1d1	nápadná
<g/>
.	.	kIx.	.
</s>
<s>
Plné	plný	k2eAgNnSc1d1	plné
propuknutí	propuknutí	k1gNnSc1	propuknutí
choroby	choroba	k1gFnSc2	choroba
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
až	až	k9	až
následující	následující	k2eAgFnSc4d1	následující
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Včelstvo	včelstvo	k1gNnSc1	včelstvo
postupně	postupně	k6eAd1	postupně
slábne	slábnout	k5eAaImIp3nS	slábnout
a	a	k8xC	a
po	po	k7c6	po
3-4	[number]	k4	3-4
letech	léto	k1gNnPc6	léto
hyne	hynout	k5eAaImIp3nS	hynout
<g/>
;	;	kIx,	;
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
moru	mor	k1gInSc2	mor
včelího	včelí	k2eAgInSc2d1	včelí
plodu	plod	k1gInSc2	plod
lze	lze	k6eAd1	lze
potvrdit	potvrdit	k5eAaPmF	potvrdit
pouze	pouze	k6eAd1	pouze
profesionální	profesionální	k2eAgFnSc7d1	profesionální
laboratorní	laboratorní	k2eAgFnSc7d1	laboratorní
diagnostikou	diagnostika	k1gFnSc7	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
podezření	podezření	k1gNnSc6	podezření
na	na	k7c4	na
výskyt	výskyt	k1gInSc4	výskyt
nutno	nutno	k6eAd1	nutno
neodkladně	odkladně	k6eNd1	odkladně
zaslat	zaslat	k5eAaPmF	zaslat
vzorek	vzorek	k1gInSc4	vzorek
plodu	plod	k1gInSc2	plod
k	k	k7c3	k
laboratornímu	laboratorní	k2eAgNnSc3d1	laboratorní
vyšetření	vyšetření	k1gNnSc3	vyšetření
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
nákazy	nákaza	k1gFnSc2	nákaza
příslušné	příslušný	k2eAgNnSc1d1	příslušné
pracoviště	pracoviště	k1gNnSc1	pracoviště
Státní	státní	k2eAgFnSc2d1	státní
veterinární	veterinární	k2eAgFnSc2d1	veterinární
správy	správa	k1gFnSc2	správa
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
:	:	kIx,	:
vymezí	vymezit	k5eAaPmIp3nP	vymezit
ohnisko	ohnisko	k1gNnSc4	ohnisko
a	a	k8xC	a
ochranné	ochranný	k2eAgNnSc4d1	ochranné
pásmo	pásmo	k1gNnSc4	pásmo
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
nařídí	nařídit	k5eAaPmIp3nS	nařídit
prohlídku	prohlídka	k1gFnSc4	prohlídka
všech	všecek	k3xTgNnPc2	všecek
včelstev	včelstvo	k1gNnPc2	včelstvo
pověřeným	pověřený	k2eAgMnSc7d1	pověřený
pracovníkem	pracovník	k1gMnSc7	pracovník
StVS	StVS	k1gFnSc2	StVS
nařídí	nařídit	k5eAaPmIp3nS	nařídit
uzávěru	uzávěra	k1gFnSc4	uzávěra
ohniska	ohnisko	k1gNnSc2	ohnisko
a	a	k8xC	a
ochranného	ochranný	k2eAgNnSc2d1	ochranné
pásma	pásmo	k1gNnSc2	pásmo
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
zákaz	zákaz	k1gInSc1	zákaz
veškerých	veškerý	k3xTgInPc2	veškerý
přesunů	přesun	k1gInPc2	přesun
včelstev	včelstvo	k1gNnPc2	včelstvo
až	až	k8xS	až
do	do	k7c2	do
odvolání	odvolání	k1gNnSc2	odvolání
nařídí	nařídit	k5eAaPmIp3nS	nařídit
likvidaci	likvidace	k1gFnSc4	likvidace
nakaženého	nakažený	k2eAgNnSc2d1	nakažené
včelstva	včelstvo	k1gNnSc2	včelstvo
resp.	resp.	kA	resp.
<g />
.	.	kIx.	.
</s>
<s>
včelstev	včelstvo	k1gNnPc2	včelstvo
-	-	kIx~	-
spálit	spálit	k5eAaPmF	spálit
celý	celý	k2eAgInSc4d1	celý
úl	úl	k1gInSc4	úl
včetně	včetně	k7c2	včetně
usmrcených	usmrcený	k2eAgFnPc2d1	usmrcená
včel	včela	k1gFnPc2	včela
a	a	k8xC	a
dalšího	další	k2eAgInSc2d1	další
obsahu	obsah	k1gInSc2	obsah
<g/>
,	,	kIx,	,
veškeré	veškerý	k3xTgNnSc4	veškerý
nářadí	nářadí	k1gNnSc4	nářadí
a	a	k8xC	a
pomůcky	pomůcka	k1gFnPc4	pomůcka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přišly	přijít	k5eAaPmAgFnP	přijít
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
napadeným	napadený	k2eAgNnSc7d1	napadené
včelstvem	včelstvo	k1gNnSc7	včelstvo
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
hořlavého	hořlavý	k2eAgInSc2d1	hořlavý
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
desinfikovat	desinfikovat	k5eAaBmF	desinfikovat
vše	všechen	k3xTgNnSc4	všechen
nehořlavé	hořlavý	k2eNgNnSc4d1	nehořlavé
(	(	kIx(	(
<g/>
kovové	kovový	k2eAgNnSc4d1	kovové
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asanovat	asanovat	k5eAaBmF	asanovat
plochu	plocha	k1gFnSc4	plocha
před	před	k7c7	před
včelínem	včelín	k1gInSc7	včelín
resp.	resp.	kA	resp.
na	na	k7c6	na
včelnici	včelnice	k1gFnSc6	včelnice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
hlášení	hlášení	k1gNnSc2	hlášení
posledního	poslední	k2eAgInSc2d1	poslední
výskytu	výskyt	k1gInSc2	výskyt
se	se	k3xPyFc4	se
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
jednoroční	jednoroční	k2eAgInSc1d1	jednoroční
časový	časový	k2eAgInSc1d1	časový
odstup	odstup	k1gInSc1	odstup
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vhodném	vhodný	k2eAgNnSc6d1	vhodné
období	období	k1gNnSc6	období
následující	následující	k2eAgFnSc2d1	následující
vegetační	vegetační	k2eAgFnSc2d1	vegetační
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
provede	provést	k5eAaPmIp3nS	provést
prohlídka	prohlídka	k1gFnSc1	prohlídka
všech	všecek	k3xTgInPc2	všecek
plástů	plást	k1gInPc2	plást
v	v	k7c6	v
úlech	úl	k1gInPc6	úl
ochranné	ochranný	k2eAgFnSc2d1	ochranná
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Prohlídku	prohlídka	k1gFnSc4	prohlídka
provádí	provádět	k5eAaImIp3nS	provádět
způsobilý	způsobilý	k2eAgMnSc1d1	způsobilý
pracovník	pracovník	k1gMnSc1	pracovník
veterinární	veterinární	k2eAgFnSc2d1	veterinární
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
výsledek	výsledek	k1gInSc1	výsledek
prohlídky	prohlídka	k1gFnSc2	prohlídka
negativní	negativní	k2eAgInSc1d1	negativní
<g/>
,	,	kIx,	,
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
se	se	k3xPyFc4	se
nákaza	nákaza	k1gFnSc1	nákaza
za	za	k7c4	za
zaniklou	zaniklý	k2eAgFnSc4d1	zaniklá
<g/>
.	.	kIx.	.
</s>
