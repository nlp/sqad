<s>
Transcendentální	transcendentální	k2eAgFnSc1d1	transcendentální
meditace	meditace	k1gFnSc1	meditace
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
TM	TM	kA	TM
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
meditační	meditační	k2eAgFnSc1d1	meditační
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
z	z	k7c2	z
véd	véda	k1gFnPc2	véda
obrodil	obrodit	k5eAaPmAgMnS	obrodit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
začal	začít	k5eAaPmAgMnS	začít
vyučovat	vyučovat	k5eAaImF	vyučovat
Mahariši	Mahariše	k1gFnSc4	Mahariše
Maheš	Maheš	k1gFnSc2	Maheš
Jógi	Jóg	k1gFnSc2	Jóg
<g/>
.	.	kIx.	.
</s>
