<s>
Transcendentální	transcendentální	k2eAgFnSc1d1	transcendentální
meditace	meditace	k1gFnSc1	meditace
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
TM	TM	kA	TM
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
meditační	meditační	k2eAgFnSc1d1	meditační
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
z	z	k7c2	z
véd	véda	k1gFnPc2	véda
obrodil	obrodit	k5eAaPmAgMnS	obrodit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
začal	začít	k5eAaPmAgMnS	začít
vyučovat	vyučovat	k5eAaImF	vyučovat
Mahariši	Mahariše	k1gFnSc4	Mahariše
Maheš	Maheš	k1gFnSc2	Maheš
Jógi	Jóg	k1gFnSc2	Jóg
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
přirozenosti	přirozenost	k1gFnSc3	přirozenost
a	a	k8xC	a
jednoduchosti	jednoduchost	k1gFnSc3	jednoduchost
této	tento	k3xDgFnSc2	tento
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
možnosti	možnost	k1gFnPc4	možnost
naučit	naučit	k5eAaPmF	naučit
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
mateřské	mateřský	k2eAgFnSc6d1	mateřská
řeči	řeč	k1gFnSc6	řeč
od	od	k7c2	od
učitelů	učitel	k1gMnPc2	učitel
TM	TM	kA	TM
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
50	[number]	k4	50
let	léto	k1gNnPc2	léto
do	do	k7c2	do
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
180	[number]	k4	180
zemí	zem	k1gFnPc2	zem
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Komplexní	komplexní	k2eAgInSc1d1	komplexní
účinek	účinek	k1gInSc1	účinek
programu	program	k1gInSc2	program
Transcendentální	transcendentální	k2eAgFnSc2d1	transcendentální
meditace	meditace	k1gFnSc2	meditace
je	být	k5eAaImIp3nS	být
dokumentován	dokumentovat	k5eAaBmNgInS	dokumentovat
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
vědecko-výzkumnými	vědeckoýzkumný	k2eAgFnPc7d1	vědecko-výzkumná
studiemi	studie	k1gFnPc7	studie
a	a	k8xC	a
TM-program	TMrogram	k1gInSc1	TM-program
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
systému	systém	k1gInSc2	systém
vzdělání	vzdělání	k1gNnSc2	vzdělání
založeného	založený	k2eAgNnSc2d1	založené
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
vědomí	vědomí	k1gNnSc2	vědomí
(	(	kIx(	(
<g/>
CBE	CBE	kA	CBE
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
komplexní	komplexní	k2eAgNnSc4d1	komplexní
rozvíjení	rozvíjení	k1gNnSc4	rozvíjení
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vědeckých	vědecký	k2eAgInPc2d1	vědecký
výzkumů	výzkum	k1gInPc2	výzkum
uvádí	uvádět	k5eAaImIp3nS	uvádět
příznivé	příznivý	k2eAgInPc4d1	příznivý
účinky	účinek	k1gInPc4	účinek
této	tento	k3xDgFnSc2	tento
meditační	meditační	k2eAgFnSc2d1	meditační
techniky	technika	k1gFnSc2	technika
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
života	život	k1gInSc2	život
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
TM-program	TMrogram	k1gInSc1	TM-program
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgInPc4d1	mnohý
nedostupný	dostupný	k2eNgInSc4d1	nedostupný
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
vysoké	vysoká	k1gFnSc3	vysoká
finanční	finanční	k2eAgFnSc2d1	finanční
nákladnosti	nákladnost	k1gFnSc2	nákladnost
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
zcela	zcela	k6eAd1	zcela
odlišnými	odlišný	k2eAgInPc7d1	odlišný
pojmy	pojem	k1gInPc7	pojem
<g/>
:	:	kIx,	:
Meditační	meditační	k2eAgNnPc1d1	meditační
technika	technikum	k1gNnPc1	technikum
a	a	k8xC	a
TM-hnutím	TMnutí	k1gNnSc7	TM-hnutí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tuto	tento	k3xDgFnSc4	tento
techniku	technika	k1gFnSc4	technika
propaguje	propagovat	k5eAaImIp3nS	propagovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
TM-programech	TMrogram	k1gInPc6	TM-program
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
Transcendentální	transcendentální	k2eAgFnSc2d1	transcendentální
meditace	meditace	k1gFnSc2	meditace
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
využití	využití	k1gNnSc6	využití
schopnosti	schopnost	k1gFnSc2	schopnost
mysli	mysl	k1gFnSc2	mysl
zklidnit	zklidnit	k5eAaPmF	zklidnit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
TM	TM	kA	TM
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
zklidňování	zklidňování	k1gNnSc3	zklidňování
speciálních	speciální	k2eAgInPc2d1	speciální
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
manter	mantra	k1gFnPc2	mantra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
zklidňování	zklidňování	k1gNnSc2	zklidňování
proudu	proud	k1gInSc2	proud
myšlenek	myšlenka	k1gFnPc2	myšlenka
popisuje	popisovat	k5eAaImIp3nS	popisovat
Mahariši	Mahariš	k1gMnSc3	Mahariš
Maheš	Maheš	k1gMnSc3	Maheš
Jógi	Jóg	k1gMnSc3	Jóg
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Věda	věda	k1gFnSc1	věda
o	o	k7c6	o
Bytí	bytí	k1gNnSc6	bytí
a	a	k8xC	a
umění	umění	k1gNnSc6	umění
žití	žití	k1gNnPc2	žití
analogií	analogie	k1gFnPc2	analogie
s	s	k7c7	s
oceánem	oceán	k1gInSc7	oceán
a	a	k8xC	a
bublinou	bublina	k1gFnSc7	bublina
následujícím	následující	k2eAgInSc7d1	následující
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
–	–	k?	–
<g/>
Mahariši	Mahariš	k1gMnPc1	Mahariš
Maheš	Maheš	k1gFnSc2	Maheš
Jógi	Jógi	k1gNnSc1	Jógi
<g/>
,	,	kIx,	,
Věda	věda	k1gFnSc1	věda
o	o	k7c4	o
Bytí	bytí	k1gNnSc4	bytí
a	a	k8xC	a
umění	umění	k1gNnSc4	umění
žití	žití	k1gNnSc2	žití
Mahariši	Mahariš	k1gMnPc1	Mahariš
Maheš	Maheš	k1gFnSc2	Maheš
Jógi	Jóg	k1gFnSc2	Jóg
vyškolil	vyškolit	k5eAaPmAgInS	vyškolit
docenty	docent	k1gMnPc4	docent
TM	TM	kA	TM
v	v	k7c6	v
tradičním	tradiční	k2eAgInSc6d1	tradiční
způsobu	způsob	k1gInSc6	způsob
výuky	výuka	k1gFnSc2	výuka
(	(	kIx(	(
<g/>
ceremonie	ceremonie	k1gFnSc1	ceremonie
díků	dík	k1gInPc2	dík
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
tradice	tradice	k1gFnSc2	tradice
védských	védský	k2eAgMnPc2d1	védský
mistrů	mistr	k1gMnPc2	mistr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
přesné	přesný	k2eAgNnSc4d1	přesné
předání	předání	k1gNnSc4	předání
této	tento	k3xDgFnSc2	tento
techniky	technika	k1gFnSc2	technika
bez	bez	k7c2	bez
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
individuálního	individuální	k2eAgInSc2d1	individuální
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
výzkumnou	výzkumný	k2eAgFnSc4d1	výzkumná
práci	práce	k1gFnSc4	práce
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
Robert	Robert	k1gMnSc1	Robert
Keith	Keith	k1gMnSc1	Keith
Wallace	Wallace	k1gFnSc1	Wallace
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
knih	kniha	k1gFnPc2	kniha
Fyziologie	fyziologie	k1gFnSc2	fyziologie
vědomí	vědomí	k1gNnSc1	vědomí
a	a	k8xC	a
Neurofyziologie	neurofyziologie	k1gFnSc1	neurofyziologie
osvícení	osvícení	k1gNnSc2	osvícení
<g/>
,	,	kIx,	,
a	a	k8xC	a
publikována	publikován	k2eAgFnSc1d1	publikována
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
výzkum	výzkum	k1gInSc1	výzkum
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dalších	další	k2eAgInPc2d1	další
třiceti	třicet	k4xCc7	třicet
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
přes	přes	k7c4	přes
600	[number]	k4	600
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
prací	práce	k1gFnPc2	práce
z	z	k7c2	z
250	[number]	k4	250
universit	universita	k1gFnPc2	universita
a	a	k8xC	a
nezávislých	závislý	k2eNgInPc2d1	nezávislý
institutů	institut	k1gInPc2	institut
ve	v	k7c6	v
33	[number]	k4	33
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
publikovány	publikovat	k5eAaBmNgFnP	publikovat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
vědeckých	vědecký	k2eAgInPc6d1	vědecký
časopisech	časopis	k1gInPc6	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgFnPc1d1	vědecká
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgMnPc1d1	zabývající
se	se	k3xPyFc4	se
účinkem	účinek	k1gInSc7	účinek
programu	program	k1gInSc2	program
TM	TM	kA	TM
na	na	k7c4	na
komplexní	komplexní	k2eAgNnSc4d1	komplexní
zdraví	zdraví	k1gNnSc4	zdraví
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
studie	studie	k1gFnPc1	studie
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
hypertenze	hypertenze	k1gFnSc2	hypertenze
<g/>
,	,	kIx,	,
funkční	funkční	k2eAgFnPc4d1	funkční
kapacity	kapacita	k1gFnPc4	kapacita
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
aterosklerózy	ateroskleróza	k1gFnSc2	ateroskleróza
<g/>
,	,	kIx,	,
metabolického	metabolický	k2eAgInSc2d1	metabolický
syndromu	syndrom	k1gInSc2	syndrom
<g/>
,	,	kIx,	,
prevence	prevence	k1gFnSc1	prevence
kardiovaskulárních	kardiovaskulární	k2eAgFnPc2d1	kardiovaskulární
nemocí	nemoc	k1gFnPc2	nemoc
a	a	k8xC	a
redukce	redukce	k1gFnSc2	redukce
stresu	stres	k1gInSc2	stres
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Hans	Hans	k1gMnSc1	Hans
Selye	Sely	k1gFnSc2	Sely
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ředitel	ředitel	k1gMnSc1	ředitel
experimentální	experimentální	k2eAgFnSc2d1	experimentální
medicíny	medicína	k1gFnSc2	medicína
a	a	k8xC	a
chirurgie	chirurgie	k1gFnSc2	chirurgie
university	universita	k1gFnSc2	universita
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odborník	odborník	k1gMnSc1	odborník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
stresu	stres	k1gInSc2	stres
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
:	:	kIx,	:
–	–	k?	–
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Hans	Hans	k1gMnSc1	Hans
Selye	Selye	k1gFnSc1	Selye
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
meta	meta	k1gFnSc1	meta
analýza	analýza	k1gFnSc1	analýza
výzkumu	výzkum	k1gInSc2	výzkum
na	na	k7c6	na
lékařské	lékařský	k2eAgFnSc6d1	lékařská
universitě	universita	k1gFnSc6	universita
v	v	k7c4	v
Kentucky	Kentuck	k1gInPc4	Kentuck
vedeném	vedený	k2eAgInSc6d1	vedený
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
J.	J.	kA	J.
W.	W.	kA	W.
Andersonem	Anderson	k1gMnSc7	Anderson
a	a	k8xC	a
publikovaném	publikovaný	k2eAgInSc6d1	publikovaný
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2008	[number]	k4	2008
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
American	American	k1gMnSc1	American
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Hypertension	Hypertension	k1gInSc1	Hypertension
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
TM	TM	kA	TM
je	být	k5eAaImIp3nS	být
účinnou	účinný	k2eAgFnSc7d1	účinná
léčbou	léčba	k1gFnSc7	léčba
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
škodlivých	škodlivý	k2eAgInPc2d1	škodlivý
účinků	účinek	k1gInPc2	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
TM	TM	kA	TM
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
základního	základní	k2eAgInSc2d1	základní
a	a	k8xC	a
středního	střední	k2eAgInSc2d1	střední
stupně	stupeň	k1gInSc2	stupeň
jsou	být	k5eAaImIp3nP	být
dlouhodobé	dlouhodobý	k2eAgInPc1d1	dlouhodobý
výsledky	výsledek	k1gInPc1	výsledek
zaznamenány	zaznamenán	k2eAgInPc1d1	zaznamenán
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
ve	v	k7c6	v
Fairfieldu	Fairfield	k1gInSc6	Fairfield
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
-	-	kIx~	-
jak	jak	k8xS	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
školy	škola	k1gFnSc2	škola
-	-	kIx~	-
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
vnitřního	vnitřní	k2eAgMnSc4d1	vnitřní
génia	génius	k1gMnSc4	génius
a	a	k8xC	a
plnou	plný	k2eAgFnSc4d1	plná
tvořivou	tvořivý	k2eAgFnSc4d1	tvořivá
inteligenci	inteligence	k1gFnSc4	inteligence
pro	pro	k7c4	pro
zdravý	zdravý	k2eAgInSc4d1	zdravý
<g/>
,	,	kIx,	,
šťastný	šťastný	k2eAgInSc4d1	šťastný
a	a	k8xC	a
produktivní	produktivní	k2eAgInSc4d1	produktivní
život	život	k1gInSc4	život
u	u	k7c2	u
každého	každý	k3xTgMnSc2	každý
studenta	student	k1gMnSc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
Maharišiho	Mahariši	k1gMnSc2	Mahariši
školy	škola	k1gFnSc2	škola
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
a	a	k8xC	a
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
kniha	kniha	k1gFnSc1	kniha
Výjimečné	výjimečný	k2eAgInPc4d1	výjimečný
výsledky	výsledek	k1gInPc1	výsledek
a	a	k8xC	a
pozoruhodný	pozoruhodný	k2eAgInSc1d1	pozoruhodný
úspěch	úspěch	k1gInSc1	úspěch
školy	škola	k1gFnSc2	škola
od	od	k7c2	od
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Ashley	Ashlea	k1gMnSc2	Ashlea
Deanse	Deans	k1gMnSc2	Deans
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
universitního	universitní	k2eAgNnSc2d1	universitní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
má	mít	k5eAaImIp3nS	mít
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
<g/>
leté	letý	k2eAgFnSc2d1	letá
<g/>
)	)	kIx)	)
zkušenosti	zkušenost	k1gFnPc1	zkušenost
Maharišiho	Mahariši	k1gMnSc2	Mahariši
universita	universita	k1gFnSc1	universita
managementu	management	k1gInSc2	management
<g/>
,	,	kIx,	,
akreditovaná	akreditovaný	k2eAgFnSc1d1	akreditovaná
komisí	komise	k1gFnSc7	komise
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgFnPc4d2	vyšší
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
NSSE	NSSE	kA	NSSE
na	na	k7c4	na
účinné	účinný	k2eAgNnSc4d1	účinné
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
umísťují	umísťovat	k5eAaImIp3nP	umísťovat
zdejší	zdejší	k2eAgMnPc1d1	zdejší
studenti	student	k1gMnPc1	student
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgNnPc7d1	ostatní
613	[number]	k4	613
institucemi	instituce	k1gFnPc7	instituce
nabízejícími	nabízející	k2eAgFnPc7d1	nabízející
bakalářské	bakalářský	k2eAgNnSc4d1	bakalářské
a	a	k8xC	a
magisterské	magisterský	k2eAgNnSc4d1	magisterské
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
léčbě	léčba	k1gFnSc6	léčba
návykových	návykový	k2eAgFnPc2d1	návyková
drog	droga	k1gFnPc2	droga
Uzdravit	uzdravit	k5eAaPmF	uzdravit
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
od	od	k7c2	od
D.	D.	kA	D.
<g/>
F.	F.	kA	F.
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Conella	Conell	k1gMnSc2	Conell
a	a	k8xC	a
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
<g/>
N.	N.	kA	N.
Alexandera	Alexander	k1gMnSc2	Alexander
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
aplikaci	aplikace	k1gFnSc4	aplikace
použití	použití	k1gNnSc2	použití
programu	program	k1gInSc2	program
TM	TM	kA	TM
u	u	k7c2	u
juvenilních	juvenilní	k2eAgMnPc2d1	juvenilní
delikventů	delikvent	k1gMnPc2	delikvent
a	a	k8xC	a
ve	v	k7c6	v
věznicích	věznice	k1gFnPc6	věznice
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sarina	Sarina	k1gMnSc1	Sarina
Grosswald	Grosswald	k1gMnSc1	Grosswald
uvádí	uvádět	k5eAaImIp3nS	uvádět
a	a	k8xC	a
komentuje	komentovat	k5eAaBmIp3nS	komentovat
výsledky	výsledek	k1gInPc4	výsledek
výzkumu	výzkum	k1gInSc2	výzkum
aplikace	aplikace	k1gFnSc2	aplikace
TM	TM	kA	TM
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
s	s	k7c7	s
poruchami	porucha	k1gFnPc7	porucha
učení	učení	k1gNnSc2	učení
a	a	k8xC	a
u	u	k7c2	u
hyperaktivních	hyperaktivní	k2eAgFnPc2d1	hyperaktivní
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Zkušenosti	zkušenost	k1gFnPc1	zkušenost
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
programu	program	k1gInSc2	program
TM	TM	kA	TM
na	na	k7c6	na
zdraví	zdraví	k1gNnSc6	zdraví
žen	žena	k1gFnPc2	žena
uvádí	uvádět	k5eAaImIp3nS	uvádět
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Nancy	Nancy	k1gFnSc1	Nancy
Lonsdorf	Lonsdorf	k1gInSc1	Lonsdorf
<g/>
,	,	kIx,	,
autorka	autorka	k1gFnSc1	autorka
knihy	kniha	k1gFnSc2	kniha
Zralá	zralý	k2eAgFnSc1d1	zralá
žena	žena	k1gFnSc1	žena
a	a	k8xC	a
specialistka	specialistka	k1gFnSc1	specialistka
v	v	k7c6	v
integrační	integrační	k2eAgFnSc6d1	integrační
medicíně	medicína	k1gFnSc6	medicína
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
komentuje	komentovat	k5eAaBmIp3nS	komentovat
významné	významný	k2eAgNnSc4d1	významné
snížení	snížení	k1gNnSc4	snížení
stresu	stres	k1gInSc2	stres
u	u	k7c2	u
pacientek	pacientka	k1gFnPc2	pacientka
na	na	k7c6	na
základě	základ	k1gInSc6	základ
snížení	snížení	k1gNnSc2	snížení
jejich	jejich	k3xOp3gFnSc2	jejich
hladiny	hladina	k1gFnSc2	hladina
kortizolu	kortizol	k1gInSc2	kortizol
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
aplikace	aplikace	k1gFnSc2	aplikace
programu	program	k1gInSc2	program
TM	TM	kA	TM
ve	v	k7c6	v
věznici	věznice	k1gFnSc6	věznice
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zlepšování	zlepšování	k1gNnSc4	zlepšování
profilu	profil	k1gInSc2	profil
osobnosti	osobnost	k1gFnSc2	osobnost
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
kniha	kniha	k1gFnSc1	kniha
George	George	k1gFnSc1	George
Ellise	Ellise	k1gFnSc1	Ellise
Folsomská	Folsomský	k2eAgFnSc1d1	Folsomský
věznice	věznice	k1gFnSc1	věznice
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
Transendentální	Transendentální	k2eAgFnSc2d1	Transendentální
meditace	meditace	k1gFnSc2	meditace
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
především	především	k9	především
na	na	k7c4	na
vědecké	vědecký	k2eAgFnPc4d1	vědecká
metody	metoda	k1gFnPc4	metoda
používané	používaný	k2eAgFnPc4d1	používaná
TM	TM	kA	TM
a	a	k8xC	a
finanční	finanční	k2eAgFnSc4d1	finanční
nákladnost	nákladnost	k1gFnSc4	nákladnost
TM-programů	TMrogram	k1gInPc2	TM-program
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
členové	člen	k1gMnPc1	člen
Beatles	beatles	k1gMnPc2	beatles
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
Maharišiho	Mahariši	k1gMnSc2	Mahariši
učili	učit	k5eAaImAgMnP	učit
a	a	k8xC	a
díky	díky	k7c3	díky
kterým	který	k3yRgMnPc3	který
se	se	k3xPyFc4	se
Mahariši	Mahariše	k1gFnSc4	Mahariše
proslavil	proslavit	k5eAaPmAgMnS	proslavit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
rozešli	rozejít	k5eAaPmAgMnP	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rozvedené	rozvedený	k2eAgFnSc2d1	rozvedená
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
vychován	vychovat	k5eAaPmNgMnS	vychovat
sestrou	sestra	k1gFnSc7	sestra
své	svůj	k3xOyFgFnPc4	svůj
matky	matka	k1gFnPc4	matka
<g/>
,	,	kIx,	,
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
17	[number]	k4	17
let	léto	k1gNnPc2	léto
neměl	mít	k5eNaImAgMnS	mít
žádný	žádný	k3yNgInSc4	žádný
domov	domov	k1gInSc4	domov
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
pořád	pořád	k6eAd1	pořád
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
vstup	vstup	k1gInSc1	vstup
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
vedle	vedle	k7c2	vedle
manželky	manželka	k1gFnSc2	manželka
Cythie	Cythie	k1gFnSc2	Cythie
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
neakceptování	neakceptování	k1gNnSc2	neakceptování
skupinou	skupina	k1gFnSc7	skupina
Beatles	Beatles	k1gFnSc2	Beatles
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgInS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mahariši	Mahariše	k1gFnSc4	Mahariše
vyřeší	vyřešit	k5eAaPmIp3nP	vyřešit
všechny	všechen	k3xTgInPc1	všechen
jeho	jeho	k3xOp3gInPc1	jeho
problémy	problém	k1gInPc1	problém
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
Maharišiho	Mahariši	k1gMnSc4	Mahariši
obvinil	obvinit	k5eAaPmAgMnS	obvinit
z	z	k7c2	z
podvodnictví	podvodnictví	k1gNnSc2	podvodnictví
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Mahariši	Mahariše	k1gFnSc4	Mahariše
udělal	udělat	k5eAaPmAgMnS	udělat
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
blázny	blázen	k1gMnPc4	blázen
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rodné	rodný	k2eAgFnSc6d1	rodná
domovině	domovina	k1gFnSc6	domovina
Mahariši	Mahariše	k1gFnSc3	Mahariše
čelil	čelit	k5eAaImAgInS	čelit
již	již	k6eAd1	již
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
kritice	kritika	k1gFnSc3	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
směřovala	směřovat	k5eAaImAgFnS	směřovat
především	především	k9	především
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mahariši	Mahariše	k1gFnSc4	Mahariše
dával	dávat	k5eAaImAgInS	dávat
přednost	přednost	k1gFnSc4	přednost
bohatým	bohatý	k2eAgMnPc3d1	bohatý
žákům	žák	k1gMnPc3	žák
před	před	k7c7	před
chudými	chudý	k2eAgFnPc7d1	chudá
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
týdenní	týdenní	k2eAgInSc4d1	týdenní
plat	plat	k1gInSc4	plat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
hinduistickou	hinduistický	k2eAgFnSc7d1	hinduistická
tradicí	tradice	k1gFnSc7	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
základní	základní	k2eAgInSc4d1	základní
TM-kurz	TMurz	k1gInSc4	TM-kurz
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
14	[number]	k4	14
400	[number]	k4	400
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Neoficiální	oficiální	k2eNgInPc4d1	neoficiální
zdroje	zdroj	k1gInPc4	zdroj
a	a	k8xC	a
např.	např.	kA	např.
<g/>
:	:	kIx,	:
exorcisté	exorcista	k1gMnPc1	exorcista
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
TM	TM	kA	TM
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
některých	některý	k3yIgFnPc2	některý
svázaností	svázanost	k1gFnPc2	svázanost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
člověk	člověk	k1gMnSc1	člověk
praktikující	praktikující	k2eAgFnSc2d1	praktikující
TM	TM	kA	TM
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
otevírá	otevírat	k5eAaImIp3nS	otevírat
působení	působení	k1gNnSc4	působení
zlých	zlý	k2eAgFnPc2d1	zlá
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
ji	on	k3xPp3gFnSc4	on
mezi	mezi	k7c4	mezi
okultní	okultní	k2eAgMnPc4d1	okultní
praktiky	praktik	k1gMnPc4	praktik
<g/>
.	.	kIx.	.
</s>
