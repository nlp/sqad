<s>
GTK	GTK	kA
</s>
<s>
GTK	GTK	kA
</s>
<s>
Snímek	snímek	k1gInSc1
obrazovky	obrazovka	k1gFnSc2
programu	program	k1gInSc2
GIMP	GIMP	kA
založeného	založený	k2eAgNnSc2d1
na	na	k7c4
GTKVývojář	GTKVývojář	k1gInSc4
</s>
<s>
Nadace	nadace	k1gFnSc1
GNOME	GNOME	kA
První	první	k4xOgInSc4
vydání	vydání	k1gNnSc6
</s>
<s>
duben	duben	k1gInSc1
1998	#num#	k4
Aktuální	aktuální	k2eAgFnPc1d1
verze	verze	k1gFnPc1
</s>
<s>
4.2	4.2	k4
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
GNU	gnu	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
<g/>
,	,	kIx,
Unix-like	Unix-like	k1gInSc1
<g/>
,	,	kIx,
MS	MS	kA
Windows	Windows	kA
<g/>
,	,	kIx,
macOS	macOS	k?
Platforma	platforma	k1gFnSc1
</s>
<s>
multiplatformní	multiplatformní	k2eAgInSc4d1
software	software	k1gInSc4
Vyvíjeno	vyvíjen	k2eAgNnSc1d1
v	v	k7c6
</s>
<s>
C	C	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Typ	typ	k1gInSc1
softwaru	software	k1gInSc2
</s>
<s>
knihovna	knihovna	k1gFnSc1
GUI	GUI	kA
Licence	licence	k1gFnSc1
</s>
<s>
LGPL	LGPL	kA
verze	verze	k1gFnSc1
2.1	2.1	k4
Lokalizace	lokalizace	k1gFnSc1
</s>
<s>
mnohojazyčná	mnohojazyčný	k2eAgFnSc1d1
<g/>
,	,	kIx,
včetně	včetně	k7c2
české	český	k2eAgFnSc2d1
Web	web	k1gInSc1
</s>
<s>
www.gtk.org	www.gtk.org	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
GTK	GTK	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
z	z	k7c2
GIMP	GIMP	kA
Toolkit	Toolkita	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
GTK	GTK	kA
<g/>
+	+	kIx~
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
informačních	informační	k2eAgFnPc6d1
technologiích	technologie	k1gFnPc6
sada	sada	k1gFnSc1
knihoven	knihovna	k1gFnPc2
určených	určený	k2eAgInPc2d1
pro	pro	k7c4
běh	běh	k1gInSc4
programů	program	k1gInPc2
v	v	k7c6
grafickém	grafický	k2eAgNnSc6d1
uživatelském	uživatelský	k2eAgNnSc6d1
rozhraní	rozhraní	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knihovna	knihovna	k1gFnSc1
původně	původně	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
grafického	grafický	k2eAgInSc2d1
rastrového	rastrový	k2eAgInSc2d1
editoru	editor	k1gInSc2
GIMP	GIMP	kA
a	a	k8xC
byla	být	k5eAaImAgFnS
poté	poté	k6eAd1
použita	použít	k5eAaPmNgFnS
pro	pro	k7c4
prostředí	prostředí	k1gNnSc4
GNOME	GNOME	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
se	se	k3xPyFc4
tak	tak	k6eAd1
stala	stát	k5eAaPmAgFnS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
dvou	dva	k4xCgFnPc2
nejpopulárnějších	populární	k2eAgFnPc2d3
knihoven	knihovna	k1gFnPc2
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
knihovnami	knihovna	k1gFnPc7
Qt	Qt	k1gFnPc2
nahradila	nahradit	k5eAaPmAgFnS
dříve	dříve	k6eAd2
používané	používaný	k2eAgFnPc4d1
knihovny	knihovna	k1gFnPc4
Motif	Motif	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
GTK	GTK	kA
je	být	k5eAaImIp3nS
šířeno	šířen	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
open	open	k1gMnSc1
source	source	k1gMnSc1
software	software	k1gInSc4
s	s	k7c7
licencí	licence	k1gFnSc7
LGPL	LGPL	kA
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc1
projektu	projekt	k1gInSc2
GNU	gnu	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
GTK	GTK	kA
vytvořili	vytvořit	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
členové	člen	k1gMnPc1
skupiny	skupina	k1gFnSc2
eXperimental	eXperimentat	k5eAaImAgInS,k5eAaPmAgInS
Computing	Computing	k1gInSc1
Facility	Facilita	k1gFnSc2
(	(	kIx(
<g/>
XCF	XCF	kA
<g/>
)	)	kIx)
Kalifornské	kalifornský	k2eAgFnSc2d1
university	universita	k1gFnSc2
v	v	k7c4
Berkeley	Berkelea	k1gFnPc4
(	(	kIx(
<g/>
Spencer	Spencer	k1gMnSc1
Kimball	Kimball	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Mattis	Mattis	k1gFnSc2
a	a	k8xC
Josh	Josh	k1gMnSc1
MacDonald	Macdonald	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Programovací	programovací	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
</s>
<s>
GTK	GTK	kA
používá	používat	k5eAaImIp3nS
programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
C	C	kA
<g/>
,	,	kIx,
přestože	přestože	k8xS
jeho	jeho	k3xOp3gInSc1
design	design	k1gInSc1
používá	používat	k5eAaImIp3nS
objektový	objektový	k2eAgInSc1d1
systém	systém	k1gInSc1
GObject	GObject	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GNOME	GNOME	kA
platforma	platforma	k1gFnSc1
podporuje	podporovat	k5eAaImIp3nS
programovací	programovací	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
C	C	kA
<g/>
++	++	k?
(	(	kIx(
<g/>
gtkmm	gtkmm	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Perl	perl	k1gInSc1
<g/>
,	,	kIx,
Ruby	rub	k1gInPc1
<g/>
,	,	kIx,
Java	Javum	k1gNnPc1
(	(	kIx(
<g/>
zatím	zatím	k6eAd1
nefunkční	funkční	k2eNgFnSc1d1
na	na	k7c4
Microsoft	Microsoft	kA
Windows	Windows	kA
<g/>
)	)	kIx)
a	a	k8xC
Python	Python	k1gMnSc1
(	(	kIx(
<g/>
PyGTK	PyGTK	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byly	být	k5eAaImAgFnP
napsány	napsán	k2eAgFnPc4d1
vazby	vazba	k1gFnPc4
pro	pro	k7c4
mnoho	mnoho	k4c4
dalších	další	k2eAgInPc2d1
programovacích	programovací	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
Ada	Ada	kA
<g/>
,	,	kIx,
D	D	kA
<g/>
,	,	kIx,
Fortran	Fortran	kA
(	(	kIx(
<g/>
gtk-fortran	gtk-fortran	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Haskell	Haskell	k1gMnSc1
<g/>
,	,	kIx,
Lua	Lua	k1gMnSc1
<g/>
,	,	kIx,
Ocaml	Ocaml	k1gMnSc1
<g/>
,	,	kIx,
Pascal	Pascal	k1gMnSc1
<g/>
,	,	kIx,
PHP	PHP	kA
<g/>
,	,	kIx,
Pike	Pike	k1gFnSc1
<g/>
,	,	kIx,
Hrot	hrot	k1gInSc1
<g/>
,	,	kIx,
JavaScript	JavaScript	k1gInSc1
<g/>
,	,	kIx,
Tcl	Tcl	k1gFnSc1
<g/>
,	,	kIx,
Euphoria	Euphorium	k1gNnSc2
a	a	k8xC
také	také	k9
pro	pro	k7c4
všechny	všechen	k3xTgInPc4
programovací	programovací	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
založené	založený	k2eAgInPc4d1
na	na	k7c6
.	.	kIx.
<g/>
NET	NET	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GTK	GTK	kA
server	server	k1gInSc1
poskytuje	poskytovat	k5eAaImIp3nS
IPC	IPC	kA
rozhraní	rozhraní	k1gNnSc1
založené	založený	k2eAgNnSc1d1
na	na	k7c6
streamu	stream	k1gInSc6
GTK	GTK	kA
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
umožňuje	umožňovat	k5eAaImIp3nS
využití	využití	k1gNnSc1
v	v	k7c6
jakémkoli	jakýkoli	k3yIgInSc6
jazyku	jazyk	k1gInSc6
se	s	k7c7
schopností	schopnost	k1gFnSc7
I	I	kA
<g/>
/	/	kIx~
<g/>
O	O	kA
<g/>
,	,	kIx,
včetně	včetně	k7c2
shell	shell	k1gInSc4
scriptu	scripta	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Vazby	vazba	k1gFnSc2
pro	pro	k7c4
mnoho	mnoho	k4c4
jazyků	jazyk	k1gInPc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
vygenerovány	vygenerovat	k5eAaPmNgInP
automaticky	automaticky	k6eAd1
přes	přes	k7c4
GObject-introspection	GObject-introspection	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
GTK	GTK	kA
dále	daleko	k6eAd2
podporuje	podporovat	k5eAaImIp3nS
jazyky	jazyk	k1gInPc4
napsané	napsaný	k2eAgInPc4d1
pro	pro	k7c4
účely	účel	k1gInPc4
GObjectu	GObject	k1gInSc2
systému	systém	k1gInSc2
Vala	Vala	k1gMnSc1
a	a	k8xC
GOB	GOB	kA
<g/>
.	.	kIx.
</s>
<s>
Podobně	podobně	k6eAd1
jako	jako	k9
Qt	Qt	k1gFnSc1
(	(	kIx(
<g/>
ale	ale	k8xC
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
jiných	jiný	k2eAgFnPc2d1
<g/>
)	)	kIx)
není	být	k5eNaImIp3nS
GTK	GTK	kA
založen	založit	k5eAaPmNgInS
na	na	k7c6
knihovně	knihovna	k1gFnSc6
Xt	Xt	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožňuje	umožňovat	k5eAaImIp3nS
využití	využití	k1gNnSc4
GTK	GTK	kA
na	na	k7c6
platformách	platforma	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
není	být	k5eNaImIp3nS
X	X	kA
Window	Window	k1gFnSc1
System	Syst	k1gInSc7
dostupný	dostupný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
v	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
nemá	mít	k5eNaImIp3nS
GTK	GTK	kA
přístup	přístup	k1gInSc1
do	do	k7c2
databáze	databáze	k1gFnSc2
X	X	kA
resources	resources	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
uživatelské	uživatelský	k2eAgNnSc4d1
přizpůsobení	přizpůsobení	k1gNnSc4
aplikací	aplikace	k1gFnPc2
v	v	k7c6
X	X	kA
Window	Window	k1gFnPc2
System	Syst	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
GTK	GTK	kA
zpočátku	zpočátku	k6eAd1
obsahoval	obsahovat	k5eAaImAgInS
další	další	k2eAgFnPc4d1
knihovní	knihovní	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
ne	ne	k9
příliš	příliš	k6eAd1
striktně	striktně	k6eAd1
vázaly	vázat	k5eAaImAgFnP
ke	k	k7c3
grafice	grafika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
nástroje	nástroj	k1gInPc4
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
datovými	datový	k2eAgFnPc7d1
strukturami	struktura	k1gFnPc7
binární	binární	k2eAgInSc4d1
stromy	strom	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
nástroje	nástroj	k1gInPc4
se	se	k3xPyFc4
spolu	spolu	k6eAd1
s	s	k7c7
objektovým	objektový	k2eAgInSc7d1
systémem	systém	k1gInSc7
nazvaným	nazvaný	k2eAgInSc7d1
GObject	GObjecta	k1gFnPc2
přesunuly	přesunout	k5eAaPmAgInP
do	do	k7c2
separované	separovaný	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
s	s	k7c7
názvem	název	k1gInSc7
GLib	GLiba	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
programátoři	programátor	k1gMnPc1
mohou	moct	k5eAaImIp3nP
využívat	využívat	k5eAaImF,k5eAaPmF
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
kódu	kód	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
nevyžaduje	vyžadovat	k5eNaImIp3nS
grafické	grafický	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Platformy	platforma	k1gFnPc1
</s>
<s>
GTK	GTK	kA
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
naprogramován	naprogramován	k2eAgInSc4d1
pro	pro	k7c4
X	X	kA
Window	Window	k1gMnSc7
System	Syst	k1gMnSc7
<g/>
;	;	kIx,
ten	ten	k3xDgMnSc1
zůstává	zůstávat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc7
primární	primární	k2eAgFnSc7d1
cílovou	cílový	k2eAgFnSc7d1
platformou	platforma	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc1d1
cílené	cílený	k2eAgFnPc1d1
platformy	platforma	k1gFnPc1
jsou	být	k5eAaImIp3nP
Microsoft	Microsoft	kA
Windows	Windows	kA
(	(	kIx(
<g/>
Windows	Windows	kA
2000	#num#	k4
a	a	k8xC
výše	vysoce	k6eAd2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
DirectFB	DirectFB	k1gMnSc1
a	a	k8xC
Quartz	Quartz	k1gMnSc1
(	(	kIx(
<g/>
macOS	macOS	k?
10.4	10.4	k4
a	a	k8xC
následující	následující	k2eAgFnPc1d1
verze	verze	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
neustálém	neustálý	k2eAgInSc6d1
vývoji	vývoj	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Prostředí	prostředí	k1gNnSc1
a	a	k8xC
vzhled	vzhled	k1gInSc4
</s>
<s>
Koncový	koncový	k2eAgMnSc1d1
uživatel	uživatel	k1gMnSc1
si	se	k3xPyFc3
může	moct	k5eAaImIp3nS
v	v	k7c6
prostředí	prostředí	k1gNnSc6
programu	program	k1gInSc2
nakonfigurovat	nakonfigurovat	k5eAaPmF
nástroje	nástroj	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
spojeny	spojit	k5eAaPmNgInP
s	s	k7c7
nabízenými	nabízený	k2eAgInPc7d1
enginy	engin	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Enginy	Engin	k2eAgFnPc4d1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
zde	zde	k6eAd1
jsou	být	k5eAaImIp3nP
<g/>
,	,	kIx,
dokáží	dokázat	k5eAaPmIp3nP
emulovat	emulovat	k5eAaImF
vzhled	vzhled	k1gInSc4
dalších	další	k2eAgInPc2d1
populárních	populární	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
nebo	nebo	k8xC
platforem	platforma	k1gFnPc2
jako	jako	k8xC,k8xS
Windows	Windows	kA
95	#num#	k4
<g/>
,	,	kIx,
Motif	Motif	kA
<g/>
,	,	kIx,
kvart	kvart	k1gInSc1
a	a	k8xC
NEXTSTEP	NEXTSTEP	kA
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
GTK	GTK	kA
bylo	být	k5eAaImAgNnS
původně	původně	k6eAd1
navrženo	navrhnout	k5eAaPmNgNnS
a	a	k8xC
použito	použít	k5eAaPmNgNnS
v	v	k7c6
GIMPu	GIMPus	k1gInSc6
(	(	kIx(
<g/>
GNU	gnu	k1gNnSc6
Image	image	k1gInSc2
Manipulation	Manipulation	k1gInSc1
Program	program	k1gInSc1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
náhrada	náhrada	k1gFnSc1
za	za	k7c4
Motif	Motif	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
bodech	bod	k1gInPc6
byl	být	k5eAaImAgMnS
Peter	Peter	k1gMnSc1
Mattis	Mattis	k1gFnSc2
rozčarován	rozčarovat	k5eAaPmNgMnS
prací	práce	k1gFnSc7
s	s	k7c7
Motifem	Motif	k1gInSc7
a	a	k8xC
začal	začít	k5eAaPmAgMnS
tedy	tedy	k9
psát	psát	k5eAaImF
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
GUI	GUI	kA
nástroj	nástroj	k1gInSc1
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc1d1
GIMP	GIMP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
nakonec	nakonec	k6eAd1
úspěšně	úspěšně	k6eAd1
nahradil	nahradit	k5eAaPmAgMnS
Motif	Motif	kA
verzí	verze	k1gFnSc7
GIMPu	GIMPus	k1gInSc2
0.60	0.60	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
GTK	GTK	kA
bylo	být	k5eAaImAgNnS
přepsáno	přepsán	k2eAgNnSc1d1
na	na	k7c4
objektově	objektově	k6eAd1
orientovaný	orientovaný	k2eAgInSc4d1
grafický	grafický	k2eAgInSc4d1
program	program	k1gInSc4
a	a	k8xC
přejmenováno	přejmenovat	k5eAaPmNgNnS
na	na	k7c4
GTK	GTK	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
použit	použít	k5eAaPmNgInS
jako	jako	k9
GIMP	GIMP	kA
0.99	0.99	k4
<g/>
.	.	kIx.
</s>
<s>
GTK	GTK	kA
2	#num#	k4
předčilo	předčit	k5eAaBmAgNnS,k5eAaPmAgNnS
GTK	GTK	kA
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
nové	nový	k2eAgInPc1d1
rysy	rys	k1gInPc1
zahrnují	zahrnovat	k5eAaImIp3nP
zlepšené	zlepšený	k2eAgNnSc4d1
renderování	renderování	k1gNnSc4
textů	text	k1gInPc2
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
bylo	být	k5eAaImAgNnS
využito	využit	k2eAgNnSc1d1
Pango	Pango	k1gNnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
nový	nový	k2eAgMnSc1d1
engine	enginout	k5eAaPmIp3nS
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vylepšil	vylepšit	k5eAaPmAgInS
přístup	přístup	k1gInSc4
používání	používání	k1gNnSc2
Accessibility	Accessibilita	k1gFnSc2
Toolkitu	Toolkit	k1gInSc2
<g/>
,	,	kIx,
kompletní	kompletní	k2eAgInSc4d1
přechod	přechod	k1gInSc4
k	k	k7c3
Unicode	Unicod	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
UTF-8	UTF-8	k1gFnSc4
řetězců	řetězec	k1gInPc2
a	a	k8xC
má	mít	k5eAaImIp3nS
více	hodně	k6eAd2
flexibilní	flexibilní	k2eAgFnSc4d1
API	API	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ať	ať	k8xS,k8xC
tak	tak	k6eAd1
nebo	nebo	k8xC
onak	onak	k6eAd1
<g/>
,	,	kIx,
verze	verze	k1gFnSc1
GTK	GTK	kA
1	#num#	k4
a	a	k8xC
2	#num#	k4
nejsou	být	k5eNaImIp3nP
kompatibilní	kompatibilní	k2eAgInPc1d1
a	a	k8xC
aplikace	aplikace	k1gFnPc4
proto	proto	k8xC
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
portovány	portován	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
verze	verze	k1gFnSc2
2.8	2.8	k4
běží	běžet	k5eAaImIp3nS
GTK	GTK	kA
2	#num#	k4
na	na	k7c6
knihovně	knihovna	k1gFnSc6
Cairo	Cairo	k1gNnSc4
pro	pro	k7c4
překlad	překlad	k1gInSc4
vektorové	vektorový	k2eAgFnSc2d1
grafiky	grafika	k1gFnSc2
v	v	k7c6
GTK	GTK	kA
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vydané	vydaný	k2eAgFnPc1d1
verze	verze	k1gFnPc1
</s>
<s>
Datum	datum	k1gNnSc1
vydání	vydání	k1gNnSc2
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
vylepšení	vylepšení	k1gNnSc1
</s>
<s>
Pozdější	pozdní	k2eAgNnSc1d2
vylepšení	vylepšení	k1gNnSc1
verze	verze	k1gFnSc2
</s>
<s>
1.0	1.0	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1998	#num#	k4
</s>
<s>
První	první	k4xOgFnSc1
dostupná	dostupný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
</s>
<s>
1.0	1.0	k4
<g/>
.6	.6	k4
</s>
<s>
1.2	1.2	k4
Archivováno	archivován	k2eAgNnSc4d1
20	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
1999	#num#	k4
</s>
<s>
přidány	přidán	k2eAgInPc1d1
nové	nový	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
(	(	kIx(
<g/>
GtkFontSelector	GtkFontSelector	k1gInSc1
<g/>
,	,	kIx,
GtkPacker	GtkPacker	k1gInSc1
<g/>
,	,	kIx,
GtkItemFactory	GtkItemFactor	k1gInPc1
<g/>
,	,	kIx,
GtkCTree	GtkCTree	k1gNnPc1
<g/>
,	,	kIx,
<g/>
GtkInvisible	GtkInvisible	k1gMnSc1
<g/>
,	,	kIx,
GtkCalendar	GtkCalendar	k1gMnSc1
<g/>
,	,	kIx,
GtkLayout	GtkLayout	k1gMnSc1
<g/>
,	,	kIx,
GtkPlug	GtkPlug	k1gMnSc1
<g/>
,	,	kIx,
GtkSocket	GtkSocket	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1.2	1.2	k4
<g/>
.10	.10	k4
</s>
<s>
2.0	2.0	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2002	#num#	k4
</s>
<s>
GObject	GObject	k1gMnSc1
<g/>
,	,	kIx,
Universal	Universal	k1gMnSc5
Unicode	Unicod	k1gMnSc5
UTF-8	UTF-8	k1gMnSc5
</s>
<s>
2.0	2.0	k4
<g/>
.9	.9	k4
</s>
<s>
2.2	2.2	k4
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2002	#num#	k4
</s>
<s>
podpora	podpora	k1gFnSc1
multi-head	multi-head	k6eAd1
</s>
<s>
2.2	2.2	k4
<g/>
.4	.4	k4
</s>
<s>
2.4	2.4	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2004	#num#	k4
</s>
<s>
přidány	přidán	k2eAgInPc1d1
nové	nový	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
(	(	kIx(
<g/>
GtkFileChooser	GtkFileChooser	k1gInSc1
<g/>
,	,	kIx,
GtkComboBox	GtkComboBox	k1gInSc1
<g/>
,	,	kIx,
GtkComboBoxEntry	GtkComboBoxEntr	k1gInPc1
<g/>
,	,	kIx,
<g/>
GtkExpander	GtkExpander	k1gInSc1
<g/>
,	,	kIx,
GtkFontButton	GtkFontButton	k1gInSc1
<g/>
,	,	kIx,
GtkColorButton	GtkColorButton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2.4	2.4	k4
<g/>
.14	.14	k4
</s>
<s>
2.6	2.6	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2004	#num#	k4
</s>
<s>
nové	nový	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
(	(	kIx(
<g/>
GtkIconView	GtkIconView	k1gMnSc1
<g/>
,	,	kIx,
GtkAboutDialog	GtkAboutDialog	k1gMnSc1
<g/>
,	,	kIx,
GtkCellView	GtkCellView	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
Poslední	poslední	k2eAgFnSc1d1
verze	verze	k1gFnSc1
s	s	k7c7
podporou	podpora	k1gFnSc7
Windows	Windows	kA
98	#num#	k4
<g/>
/	/	kIx~
<g/>
ME	ME	kA
<g/>
.	.	kIx.
</s>
<s>
2.6	2.6	k4
<g/>
.10	.10	k4
</s>
<s>
2.8	2.8	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
2005	#num#	k4
</s>
<s>
integrace	integrace	k1gFnSc1
Cairo	Cairo	k1gNnSc1
</s>
<s>
2.8	2.8	k4
<g/>
.20	.20	k4
</s>
<s>
2.10	2.10	k4
Archivováno	archivován	k2eAgNnSc4d1
16	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2006	#num#	k4
</s>
<s>
nové	nový	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
(	(	kIx(
<g/>
GtkStatusIcon	GtkStatusIcon	k1gMnSc1
<g/>
,	,	kIx,
GtkAssistant	GtkAssistant	k1gMnSc1
<g/>
,	,	kIx,
GtkLinkButton	GtkLinkButton	k1gInSc1
<g/>
,	,	kIx,
<g/>
GtkRecentChooser	GtkRecentChooser	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
podpora	podpora	k1gFnSc1
tisku	tisk	k1gInSc2
(	(	kIx(
<g/>
GtkPrintOperation	GtkPrintOperation	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2.10	2.10	k4
<g/>
.14	.14	k4
</s>
<s>
2.12	2.12	k4
Archivováno	archivován	k2eAgNnSc4d1
17	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2007	#num#	k4
</s>
<s>
GtkBuilder	GtkBuilder	k1gMnSc1
</s>
<s>
2.12	2.12	k4
<g/>
.12	.12	k4
</s>
<s>
2.14	2.14	k4
Archivováno	archivován	k2eAgNnSc4d1
22	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2008	#num#	k4
</s>
<s>
JPEG	JPEG	kA
2000	#num#	k4
podpora	podpora	k1gFnSc1
nahrávání	nahrávání	k1gNnSc2
</s>
<s>
2.14	2.14	k4
<g/>
.7	.7	k4
</s>
<s>
2.16	2.16	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2009	#num#	k4
</s>
<s>
nově	nově	k6eAd1
přidán	přidán	k2eAgInSc1d1
GtkOrientable	GtkOrientable	k1gFnSc3
<g/>
,	,	kIx,
varování	varování	k1gNnSc4
zapnutého	zapnutý	k2eAgInSc2d1
Caps	Caps	kA
Locku	Lock	k1gInSc2
při	při	k7c6
zadání	zadání	k1gNnSc6
hesla	heslo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
vylepšení	vylepšení	k1gNnSc4
GtkScale	GtkScala	k1gFnSc3
<g/>
,	,	kIx,
GtkStatusIcon	GtkStatusIcon	k1gInSc1
<g/>
,	,	kIx,
GtkFileChooser	GtkFileChooser	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
2.16	2.16	k4
<g/>
.0	.0	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gMnSc1
GTK	GTK	kA
<g/>
+	+	kIx~
Open	Open	k1gMnSc1
Source	Source	k1gMnSc1
Project	Project	k1gInSc4
on	on	k3xPp3gMnSc1
Ohloh	Ohloh	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ohloh	Ohloh	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Project	Project	k1gInSc1
rename	renam	k1gInSc5
to	ten	k3xDgNnSc1
"	"	kIx"
<g/>
GTK	GTK	kA
<g/>
"	"	kIx"
<g/>
.	.	kIx.
mail	mail	k1gInSc1
<g/>
.	.	kIx.
<g/>
gnome	gnomat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
GTK	GTK	kA
<g/>
+	+	kIx~
bindings	bindingsit	k5eAaPmRp2nS
page	page	k1gInSc1
<g/>
.	.	kIx.
www.gtk.org	www.gtk.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
GNOME	GNOME	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4618256-1	4618256-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2001062971	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
179958458	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2001062971	#num#	k4
</s>
