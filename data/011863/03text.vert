<p>
<s>
Silur	silur	k1gInSc1	silur
je	být	k5eAaImIp3nS	být
prvohorní	prvohorní	k2eAgInSc1d1	prvohorní
útvar	útvar	k1gInSc1	útvar
v	v	k7c6	v
nadloží	nadloží	k1gNnSc6	nadloží
ordoviku	ordovik	k1gInSc2	ordovik
a	a	k8xC	a
v	v	k7c6	v
podloží	podloží	k1gNnSc6	podloží
devonu	devon	k1gInSc2	devon
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vymezen	vymezit	k5eAaPmNgInS	vymezit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
Sirem	sir	k1gMnSc7	sir
Roderickem	Roderick	k1gInSc7	Roderick
Murchisonem	Murchison	k1gInSc7	Murchison
jako	jako	k8xS	jako
sled	sled	k1gInSc1	sled
mezi	mezi	k7c7	mezi
kambriem	kambrium	k1gNnSc7	kambrium
a	a	k8xC	a
devonským	devonský	k2eAgInSc7d1	devonský
old	old	k?	old
redem	red	k1gInSc7	red
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vymezení	vymezení	k1gNnSc1	vymezení
bylo	být	k5eAaImAgNnS	být
časem	časem	k6eAd1	časem
redukováno	redukovat	k5eAaBmNgNnS	redukovat
vydělením	vydělení	k1gNnSc7	vydělení
ordoviku	ordovik	k1gInSc2	ordovik
<g/>
.	.	kIx.	.
</s>
<s>
Silur	silur	k1gInSc1	silur
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
časový	časový	k2eAgInSc4d1	časový
úsek	úsek	k1gInSc4	úsek
od	od	k7c2	od
443	[number]	k4	443
do	do	k7c2	do
416	[number]	k4	416
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
provázena	provázet	k5eAaImNgFnS	provázet
výraznými	výrazný	k2eAgInPc7d1	výrazný
horotvornými	horotvorný	k2eAgInPc7d1	horotvorný
pohyby	pohyb	k1gInPc7	pohyb
(	(	kIx(	(
<g/>
takonská	takonský	k2eAgFnSc1d1	takonský
fáze	fáze	k1gFnSc1	fáze
kaledonského	kaledonský	k2eAgNnSc2d1	kaledonské
vrásnění	vrásnění	k1gNnSc2	vrásnění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
období	období	k1gNnSc2	období
trvala	trvat	k5eAaImAgFnS	trvat
mořská	mořský	k2eAgFnSc1d1	mořská
transgrese	transgrese	k1gFnSc1	transgrese
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
graptolitové	graptolitový	k2eAgFnSc2d1	graptolitový
zóny	zóna	k1gFnSc2	zóna
Glyptograptus	Glyptograptus	k1gMnSc1	Glyptograptus
persculptus	persculptus	k1gMnSc1	persculptus
nebo	nebo	k8xC	nebo
Akidograptus	Akidograptus	k1gMnSc1	Akidograptus
acuminatus	acuminatus	k1gMnSc1	acuminatus
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
A.	A.	kA	A.
ascensus	ascensus	k1gMnSc1	ascensus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
vyhynutí	vyhynutí	k1gNnSc3	vyhynutí
mořských	mořský	k2eAgInPc2d1	mořský
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
až	až	k9	až
60	[number]	k4	60
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
siluru	silur	k1gInSc2	silur
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
mořské	mořský	k2eAgFnSc2d1	mořská
regrese	regrese	k1gFnSc2	regrese
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
ardenskou	ardenský	k2eAgFnSc7d1	Ardenská
fází	fáze	k1gFnSc7	fáze
kaledonského	kaledonský	k2eAgNnSc2d1	kaledonské
vrásnění	vrásnění	k1gNnSc2	vrásnění
<g/>
,	,	kIx,	,
doprovázenou	doprovázený	k2eAgFnSc7d1	doprovázená
často	často	k6eAd1	často
vulkanickou	vulkanický	k2eAgFnSc7d1	vulkanická
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hranice	hranice	k1gFnSc1	hranice
byla	být	k5eAaImAgFnS	být
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
24	[number]	k4	24
<g/>
.	.	kIx.	.
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
geologického	geologický	k2eAgInSc2d1	geologický
kongresu	kongres	k1gInSc2	kongres
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vymezena	vymezit	k5eAaPmNgFnS	vymezit
stratotypem	stratotyp	k1gInSc7	stratotyp
na	na	k7c6	na
Klonku	klonek	k1gInSc6	klonek
u	u	k7c2	u
Suchomast	Suchomast	k1gFnSc4	Suchomast
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
pod	pod	k7c7	pod
bází	báze	k1gFnSc7	báze
graptolitové	graptolitový	k2eAgFnSc2d1	graptolitový
zóny	zóna	k1gFnSc2	zóna
Monograptus	Monograptus	k1gMnSc1	Monograptus
uniforms	uniforms	k1gInSc1	uniforms
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
již	již	k6eAd1	již
devonu	devon	k1gInSc2	devon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
siluru	silur	k1gInSc2	silur
==	==	k?	==
</s>
</p>
<p>
<s>
Dřívější	dřívější	k2eAgNnSc1d1	dřívější
oddělení	oddělení	k1gNnSc1	oddělení
spodní	spodní	k2eAgNnSc1d1	spodní
a	a	k8xC	a
svrchní	svrchní	k2eAgInSc1d1	svrchní
silur	silur	k1gInSc1	silur
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
čtyřmi	čtyři	k4xCgFnPc7	čtyři
někdejšími	někdejší	k2eAgNnPc7d1	někdejší
pododděleními	pododdělení	k1gNnPc7	pododdělení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
přídolí	přídolí	k1gNnSc2	přídolí
<g/>
)	)	kIx)	)
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
stupně	stupeň	k1gInPc4	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgNnSc4d2	podrobnější
členění	členění	k1gNnSc4	členění
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc4d1	založený
na	na	k7c6	na
graptolitech	graptolit	k1gMnPc6	graptolit
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
zkamenělinách	zkamenělina	k1gFnPc6	zkamenělina
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
stupně	stupeň	k1gInPc1	stupeň
</s>
</p>
<p>
<s>
cayugan	cayugan	k1gInSc1	cayugan
(	(	kIx(	(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
svrchnímu	svrchní	k2eAgInSc3d1	svrchní
ludlovu	ludlův	k2eAgInSc3d1	ludlův
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
lockportian	lockportian	k1gMnSc1	lockportian
</s>
</p>
<p>
<s>
tonawandian	tonawandian	k1gInSc1	tonawandian
(	(	kIx(	(
<g/>
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
wenlocku	wenlocka	k1gFnSc4	wenlocka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ontarian	ontarian	k1gMnSc1	ontarian
</s>
</p>
<p>
<s>
alexandrian	alexandrian	k1gInSc1	alexandrian
(	(	kIx(	(
<g/>
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
llandoveru	llandovera	k1gFnSc4	llandovera
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Paleogeografie	paleogeografie	k1gFnSc2	paleogeografie
==	==	k?	==
</s>
</p>
<p>
<s>
Rozložení	rozložení	k1gNnSc1	rozložení
kontinentů	kontinent	k1gInPc2	kontinent
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nelišilo	lišit	k5eNaImAgNnS	lišit
od	od	k7c2	od
ordovického	ordovický	k2eAgInSc2d1	ordovický
<g/>
.	.	kIx.	.
</s>
<s>
Gondwana	Gondwana	k1gFnSc1	Gondwana
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
tak	tak	k9	tak
k	k	k7c3	k
tání	tání	k1gNnSc3	tání
ledovců	ledovec	k1gInPc2	ledovec
a	a	k8xC	a
vzestupu	vzestup	k1gInSc2	vzestup
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
(	(	kIx(	(
<g/>
sedimentace	sedimentace	k1gFnSc1	sedimentace
silurských	silurský	k2eAgFnPc2d1	Silurská
uloženin	uloženina	k1gFnPc2	uloženina
na	na	k7c4	na
erodované	erodovaný	k2eAgFnPc4d1	erodovaná
horniny	hornina	k1gFnPc4	hornina
ordoviku	ordovik	k1gInSc2	ordovik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
se	se	k3xPyFc4	se
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
Grónskem	Grónsko	k1gNnSc7	Grónsko
<g/>
,	,	kIx,	,
sev	sev	k?	sev
<g/>
.	.	kIx.	.
</s>
<s>
Amerikou	Amerika	k1gFnSc7	Amerika
a	a	k8xC	a
Eurasií	Eurasie	k1gFnSc7	Eurasie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
siluru	silur	k1gInSc2	silur
ležela	ležet	k5eAaImAgFnS	ležet
v	v	k7c6	v
rovníkové	rovníkový	k2eAgFnSc6d1	Rovníková
oblasti	oblast	k1gFnSc6	oblast
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Teplou	teplý	k2eAgFnSc4d1	teplá
klimatickou	klimatický	k2eAgFnSc4d1	klimatická
zónu	zóna	k1gFnSc4	zóna
indikují	indikovat	k5eAaBmIp3nP	indikovat
organogenní	organogenní	k2eAgInPc1d1	organogenní
vápence	vápenec	k1gInPc1	vápenec
<g/>
,	,	kIx,	,
útesy	útes	k1gInPc1	útes
<g/>
,	,	kIx,	,
ložiska	ložisko	k1gNnPc1	ložisko
evaporitů	evaporit	k1gInPc2	evaporit
<g/>
.	.	kIx.	.
</s>
<s>
Solná	solný	k2eAgNnPc1d1	solné
ložiska	ložisko	k1gNnPc1	ložisko
a	a	k8xC	a
pískovcové	pískovcový	k2eAgFnPc1d1	pískovcová
facie	facie	k1gFnPc1	facie
svrchního	svrchní	k2eAgInSc2d1	svrchní
siluru	silur	k1gInSc2	silur
pak	pak	k6eAd1	pak
dokládají	dokládat	k5eAaImIp3nP	dokládat
suché	suchý	k2eAgNnSc4d1	suché
aridní	aridní	k2eAgNnSc4d1	aridní
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
<s>
Světového	světový	k2eAgNnSc2d1	světové
rozšíření	rozšíření	k1gNnSc2	rozšíření
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
facie	facie	k1gFnSc1	facie
graptolitových	graptolitový	k2eAgFnPc2d1	graptolitový
břidlic	břidlice	k1gFnPc2	břidlice
(	(	kIx(	(
<g/>
černé	černý	k2eAgInPc1d1	černý
jílovce	jílovec	k1gInPc1	jílovec
<g/>
,	,	kIx,	,
prachovce	prachovka	k1gFnSc6	prachovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pelagických	pelagický	k2eAgInPc2d1	pelagický
sedimentů	sediment	k1gInPc2	sediment
vznikajících	vznikající	k2eAgInPc2d1	vznikající
ve	v	k7c6	v
stabilním	stabilní	k2eAgNnSc6d1	stabilní
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
též	též	k9	též
ortocerové	ortocerové	k2eAgInSc2d1	ortocerové
vápence	vápenec	k1gInSc2	vápenec
<g/>
,	,	kIx,	,
reprezentující	reprezentující	k2eAgMnSc1d1	reprezentující
často	často	k6eAd1	často
přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
čistě	čistě	k6eAd1	čistě
karbonátickou	karbonátický	k2eAgFnSc7d1	karbonátický
facií	facie	k1gFnSc7	facie
a	a	k8xC	a
facií	facie	k1gFnSc7	facie
graptolitových	graptolitový	k2eAgFnPc2d1	graptolitový
břidlic	břidlice	k1gFnPc2	břidlice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
sedimenty	sediment	k1gInPc1	sediment
pak	pak	k6eAd1	pak
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
korelaci	korelace	k1gFnSc4	korelace
silurských	silurský	k2eAgFnPc2d1	Silurská
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tektonika	tektonika	k1gFnSc1	tektonika
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
ordoviku	ordovik	k1gInSc2	ordovik
a	a	k8xC	a
siluru	silur	k1gInSc2	silur
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
takonská	takonský	k2eAgFnSc1d1	takonský
fáze	fáze	k1gFnSc1	fáze
(	(	kIx(	(
<g/>
starokaledonská	starokaledonský	k2eAgFnSc1d1	starokaledonský
<g/>
)	)	kIx)	)
kaledonské	kaledonský	k2eAgFnPc4d1	kaledonská
orogeneze	orogeneze	k1gFnPc4	orogeneze
<g/>
,	,	kIx,	,
nastává	nastávat	k5eAaImIp3nS	nastávat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
mořská	mořský	k2eAgFnSc1d1	mořská
transgrese	transgrese	k1gFnSc1	transgrese
s	s	k7c7	s
vrcholem	vrchol	k1gInSc7	vrchol
ve	v	k7c6	v
wenlocku	wenlocko	k1gNnSc6	wenlocko
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
ke	k	k7c3	k
srážce	srážka	k1gFnSc3	srážka
Evropy	Evropa	k1gFnSc2	Evropa
se	s	k7c7	s
sev	sev	k?	sev
<g/>
.	.	kIx.	.
</s>
<s>
Amerikou	Amerika	k1gFnSc7	Amerika
a	a	k8xC	a
k	k	k7c3	k
vyvrásnění	vyvrásnění	k1gNnSc3	vyvrásnění
nahromaděných	nahromaděný	k2eAgInPc2d1	nahromaděný
sedimentů	sediment	k1gInPc2	sediment
kambricko-ordovického	kambrickordovický	k2eAgNnSc2d1	kambricko-ordovický
stáří	stáří	k1gNnSc2	stáří
v	v	k7c6	v
kaledonské	kaledonský	k2eAgFnSc6d1	kaledonská
geosynklinále	geosynklinála	k1gFnSc6	geosynklinála
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
pohoří	pohořet	k5eAaPmIp3nP	pohořet
Kaledonidy	Kaledonida	k1gFnPc1	Kaledonida
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
přes	přes	k7c4	přes
Skotsko	Skotsko	k1gNnSc4	Skotsko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc4	Norsko
do	do	k7c2	do
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
procesy	proces	k1gInPc1	proces
začínají	začínat	k5eAaImIp3nP	začínat
již	již	k6eAd1	již
v	v	k7c4	v
ludlovu	ludlův	k2eAgFnSc4d1	ludlův
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
erijskou	erijský	k2eAgFnSc7d1	erijský
fází	fáze	k1gFnSc7	fáze
v	v	k7c6	v
přídolu	přídol	k1gInSc6	přídol
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
spojení	spojení	k1gNnSc1	spojení
(	(	kIx(	(
<g/>
kratonizace	kratonizace	k1gFnSc1	kratonizace
<g/>
)	)	kIx)	)
Erie	Erie	k1gInSc1	Erie
(	(	kIx(	(
<g/>
kanadský	kanadský	k2eAgInSc1d1	kanadský
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
Grónsko	Grónsko	k1gNnSc1	Grónsko
<g/>
)	)	kIx)	)
s	s	k7c7	s
baltickým	baltický	k2eAgInSc7d1	baltický
štítem	štít	k1gInSc7	štít
a	a	k8xC	a
východoevropskou	východoevropský	k2eAgFnSc7d1	východoevropská
tabulí	tabule	k1gFnSc7	tabule
do	do	k7c2	do
jediné	jediný	k2eAgFnSc2d1	jediná
pevniny	pevnina	k1gFnSc2	pevnina
-	-	kIx~	-
severoatlantického	severoatlantický	k2eAgInSc2d1	severoatlantický
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svrchním	svrchní	k2eAgInSc6d1	svrchní
siluru	silur	k1gInSc6	silur
moře	moře	k1gNnSc2	moře
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rychlé	rychlý	k2eAgFnSc3d1	rychlá
erozi	eroze	k1gFnSc3	eroze
nových	nový	k2eAgNnPc2d1	nové
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc4d1	severní
polokouli	polokoule	k1gFnSc4	polokoule
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
oceán	oceán	k1gInSc1	oceán
Panthalassa	Panthalass	k1gMnSc2	Panthalass
a	a	k8xC	a
nově	nově	k6eAd1	nově
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
oceán	oceán	k1gInSc1	oceán
Uralský	uralský	k2eAgInSc1d1	uralský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
v	v	k7c6	v
siluru	silur	k1gInSc6	silur
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Fauna	fauna	k1gFnSc1	fauna
===	===	k?	===
</s>
</p>
<p>
<s>
Vůdčími	vůdčí	k2eAgFnPc7d1	vůdčí
zkamenělinami	zkamenělina	k1gFnPc7	zkamenělina
jsou	být	k5eAaImIp3nP	být
graptoliti	graptolit	k1gMnPc1	graptolit
<g/>
,	,	kIx,	,
maximálního	maximální	k2eAgInSc2d1	maximální
rozvoje	rozvoj	k1gInSc2	rozvoj
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
jednořadé	jednořadý	k2eAgFnPc1d1	jednořadá
kolonie	kolonie	k1gFnPc1	kolonie
řádu	řád	k1gInSc2	řád
Monograptus	Monograptus	k1gInSc1	Monograptus
(	(	kIx(	(
<g/>
Monograptus	Monograptus	k1gMnSc1	Monograptus
<g/>
,	,	kIx,	,
Spirograptus	Spirograptus	k1gMnSc1	Spirograptus
<g/>
,	,	kIx,	,
Rastrites	Rastrites	k1gMnSc1	Rastrites
<g/>
,	,	kIx,	,
Cyrtograptus	Cyrtograptus	k1gMnSc1	Cyrtograptus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
spodního	spodní	k2eAgInSc2d1	spodní
siluru	silur	k1gInSc2	silur
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
i	i	k9	i
dvojřadé	dvojřadý	k2eAgInPc1d1	dvojřadý
druhy	druh	k1gInPc1	druh
(	(	kIx(	(
<g/>
Climacograptus	Climacograptus	k1gMnSc1	Climacograptus
<g/>
,	,	kIx,	,
Diplograptus	Diplograptus	k1gMnSc1	Diplograptus
<g/>
,	,	kIx,	,
Petalograptus	Petalograptus	k1gMnSc1	Petalograptus
<g/>
,	,	kIx,	,
Retiolites	Retiolites	k1gMnSc1	Retiolites
<g/>
,	,	kIx,	,
Stromatograptus	Stromatograptus	k1gMnSc1	Stromatograptus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jejich	jejich	k3xOp3gNnSc3	jejich
světovému	světový	k2eAgNnSc3d1	světové
rozšíření	rozšíření	k1gNnSc3	rozšíření
je	být	k5eAaImIp3nS	být
vyčleněno	vyčlenit	k5eAaPmNgNnS	vyčlenit
okolo	okolo	k7c2	okolo
40	[number]	k4	40
zón	zóna	k1gFnPc2	zóna
<g/>
,	,	kIx,	,
umožňujících	umožňující	k2eAgInPc2d1	umožňující
biostratigrafické	biostratigrafický	k2eAgNnSc4d1	biostratigrafický
členění	členění	k1gNnSc4	členění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
členovci	členovec	k1gMnPc7	členovec
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
v	v	k7c6	v
kambriu	kambrium	k1gNnSc6	kambrium
a	a	k8xC	a
ordoviku	ordovik	k1gInSc6	ordovik
rozmanití	rozmanitý	k2eAgMnPc1d1	rozmanitý
trilobiti	trilobit	k1gMnPc1	trilobit
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
čeledi	čeleď	k1gFnPc1	čeleď
(	(	kIx(	(
<g/>
Illaenidae	Illaenida	k1gFnPc1	Illaenida
<g/>
,	,	kIx,	,
Trinucleidae	Trinucleida	k1gFnPc1	Trinucleida
<g/>
)	)	kIx)	)
dokonce	dokonce	k9	dokonce
vymírají	vymírat	k5eAaImIp3nP	vymírat
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
vůdčími	vůdčí	k2eAgFnPc7d1	vůdčí
fosíliemi	fosílie	k1gFnPc7	fosílie
jsou	být	k5eAaImIp3nP	být
ostrakodi	ostrakod	k1gMnPc1	ostrakod
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
anomální	anomální	k2eAgFnSc7d1	anomální
salinitou	salinita	k1gFnSc7	salinita
(	(	kIx(	(
<g/>
Leperditia	Leperditia	k1gFnSc1	Leperditia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
rodu	rod	k1gInSc2	rod
Eurypteridae	Eurypteridae	k1gFnSc1	Eurypteridae
(	(	kIx(	(
<g/>
různorepi	různorepi	k1gNnSc1	různorepi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnPc1d1	dosahující
obrovských	obrovský	k2eAgFnPc6d1	obrovská
<g/>
,	,	kIx,	,
až	až	k9	až
3	[number]	k4	3
m	m	kA	m
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc1	ten
dravci	dravec	k1gMnPc1	dravec
s	s	k7c7	s
mohutnými	mohutný	k2eAgNnPc7d1	mohutné
klepety	klepeto	k1gNnPc7	klepeto
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnPc4d1	obývající
mělkovodní	mělkovodní	k2eAgFnPc4d1	mělkovodní
mořské	mořský	k2eAgFnPc4d1	mořská
(	(	kIx(	(
<g/>
Pterygotus	Pterygotus	k1gInSc4	Pterygotus
<g/>
)	)	kIx)	)
i	i	k9	i
lagunární	lagunární	k2eAgFnSc3d1	lagunární
(	(	kIx(	(
<g/>
brakické	brakický	k2eAgFnSc3d1	brakická
<g/>
)	)	kIx)	)
prostředí	prostředí	k1gNnSc1	prostředí
(	(	kIx(	(
<g/>
Eurypterus	Eurypterus	k1gInSc1	Eurypterus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
četné	četný	k2eAgInPc1d1	četný
nálezy	nález	k1gInPc1	nález
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
morfologie	morfologie	k1gFnSc2	morfologie
pancířů	pancíř	k1gInPc2	pancíř
a	a	k8xC	a
dýchací	dýchací	k2eAgInPc1d1	dýchací
orgány	orgán	k1gInPc1	orgán
různorepů	různorep	k1gMnPc2	různorep
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
možnou	možný	k2eAgFnSc4d1	možná
adaptaci	adaptace	k1gFnSc4	adaptace
na	na	k7c4	na
suchozemské	suchozemský	k2eAgNnSc4d1	suchozemské
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgNnSc4	první
suchozemské	suchozemský	k2eAgNnSc4d1	suchozemské
členovce	členovec	k1gMnSc2	členovec
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
považovány	považován	k2eAgFnPc1d1	považována
primitivní	primitivní	k2eAgFnPc1d1	primitivní
mnohonožky	mnohonožka	k1gFnPc1	mnohonožka
(	(	kIx(	(
<g/>
Archidesmus	Archidesmus	k1gInSc1	Archidesmus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nalezené	nalezený	k2eAgFnSc2d1	nalezená
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
jsou	být	k5eAaImIp3nP	být
nálezy	nález	k1gInPc1	nález
štírů	štír	k1gMnPc2	štír
(	(	kIx(	(
<g/>
Scorpionidae	Scorpionidae	k1gNnSc1	Scorpionidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
se	se	k3xPyFc4	se
láčkovci	láčkovec	k1gMnPc1	láčkovec
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
korálnatci	korálnatec	k1gMnPc1	korálnatec
(	(	kIx(	(
<g/>
Anthozoa	Anthozoa	k1gMnSc1	Anthozoa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zastoupeni	zastoupit	k5eAaPmNgMnP	zastoupit
rugosními	rugosní	k2eAgFnPc7d1	rugosní
(	(	kIx(	(
<g/>
Rugosa	Rugosa	k1gFnSc1	Rugosa
<g/>
)	)	kIx)	)
i	i	k9	i
tabulátními	tabulátní	k2eAgFnPc7d1	tabulátní
(	(	kIx(	(
<g/>
Tabulata	Tabule	k1gNnPc4	Tabule
<g/>
)	)	kIx)	)
koráli	korále	k1gInPc7	korále
<g/>
.	.	kIx.	.
</s>
<s>
Hojné	hojný	k2eAgInPc1d1	hojný
byly	být	k5eAaImAgInP	být
i	i	k9	i
stromatopory	stromatopor	k1gInPc1	stromatopor
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgInPc1d1	tvořený
vápenitými	vápenitý	k2eAgFnPc7d1	vápenitá
kostrami	kostra	k1gFnPc7	kostra
válcovitého	válcovitý	k2eAgInSc2d1	válcovitý
nebo	nebo	k8xC	nebo
bochníkovitého	bochníkovitý	k2eAgInSc2d1	bochníkovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnSc4d1	dosahující
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
i	i	k9	i
přes	přes	k7c4	přes
2	[number]	k4	2
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
dně	dno	k1gNnSc6	dno
mělkého	mělký	k2eAgNnSc2d1	mělké
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
ovlivněného	ovlivněný	k2eAgNnSc2d1	ovlivněné
prouděním	proudění	k1gNnSc7	proudění
<g/>
,	,	kIx,	,
žily	žít	k5eAaImAgFnP	žít
přichyceny	přichycen	k2eAgFnPc1d1	přichycena
lilijice	lilijice	k1gFnPc1	lilijice
(	(	kIx(	(
<g/>
Crinoidea	Crinoide	k2eAgFnSc1d1	Crinoide
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svrchním	svrchní	k2eAgInSc6d1	svrchní
siluru	silur	k1gInSc6	silur
jsou	být	k5eAaImIp3nP	být
celosvětově	celosvětově	k6eAd1	celosvětově
stratigraficky	stratigraficky	k6eAd1	stratigraficky
významné	významný	k2eAgFnPc1d1	významná
planktonické	planktonický	k2eAgFnPc1d1	planktonický
formy	forma	k1gFnPc1	forma
(	(	kIx(	(
<g/>
scyfokrinitový	scyfokrinitový	k2eAgInSc4d1	scyfokrinitový
obzor	obzor	k1gInSc4	obzor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
volný	volný	k2eAgInSc1d1	volný
konec	konec	k1gInSc1	konec
stopky	stopka	k1gFnSc2	stopka
nese	nést	k5eAaImIp3nS	nést
plovoucí	plovoucí	k2eAgInSc1d1	plovoucí
orgán	orgán	k1gInSc1	orgán
(	(	kIx(	(
<g/>
lobolit	lobolit	k1gInSc1	lobolit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
rozvoj	rozvoj	k1gInSc1	rozvoj
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
u	u	k7c2	u
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
četní	četný	k2eAgMnPc1d1	četný
zástupci	zástupce	k1gMnPc1	zástupce
již	již	k6eAd1	již
připomínají	připomínat	k5eAaImIp3nP	připomínat
současné	současný	k2eAgFnPc4d1	současná
formy	forma	k1gFnPc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
hlavonožci	hlavonožec	k1gMnPc7	hlavonožec
loděnky	loděnka	k1gFnSc2	loděnka
(	(	kIx(	(
<g/>
Nautiloidea	Nautiloidea	k1gMnSc1	Nautiloidea
<g/>
)	)	kIx)	)
se	s	k7c7	s
schránkami	schránka	k1gFnPc7	schránka
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
přímého	přímý	k2eAgNnSc2d1	přímé
či	či	k8xC	či
mírně	mírně	k6eAd1	mírně
prohnutého	prohnutý	k2eAgInSc2d1	prohnutý
kužele	kužel	k1gInSc2	kužel
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tvar	tvar	k1gInSc1	tvar
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
ohýbal	ohýbat	k5eAaImAgMnS	ohýbat
během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
typická	typický	k2eAgFnSc1d1	typická
zatočená	zatočený	k2eAgFnSc1d1	zatočená
schránka	schránka	k1gFnSc1	schránka
<g/>
,	,	kIx,	,
plně	plně	k6eAd1	plně
způsobilá	způsobilý	k2eAgFnSc1d1	způsobilá
života	život	k1gInSc2	život
v	v	k7c6	v
hlubinných	hlubinný	k2eAgFnPc6d1	hlubinná
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
dneška	dnešek	k1gInSc2	dnešek
přežívá	přežívat	k5eAaImIp3nS	přežívat
loděnka	loděnka	k1gFnSc1	loděnka
hlubinná	hlubinný	k2eAgFnSc1d1	hlubinná
(	(	kIx(	(
<g/>
Nautilus	Nautilus	k1gMnSc1	Nautilus
pompilius	pompilius	k1gMnSc1	pompilius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovné	rovný	k2eAgFnPc1d1	rovná
schránky	schránka	k1gFnPc1	schránka
ortocerů	ortocer	k1gInPc2	ortocer
mají	mít	k5eAaImIp3nP	mít
horninotvorný	horninotvorný	k2eAgInSc4d1	horninotvorný
význam	význam	k1gInSc4	význam
(	(	kIx(	(
<g/>
tvoří	tvořit	k5eAaImIp3nP	tvořit
ortocerové	ortocerový	k2eAgInPc4d1	ortocerový
vápence	vápenec	k1gInPc4	vápenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
gastropodů	gastropod	k1gInPc2	gastropod
obývají	obývat	k5eAaImIp3nP	obývat
zejména	zejména	k9	zejména
mělkovodní	mělkovodní	k2eAgFnPc1d1	mělkovodní
facie	facie	k1gFnPc1	facie
(	(	kIx(	(
<g/>
Platyceras	Platyceras	k1gInSc1	Platyceras
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mlže	mlž	k1gMnPc4	mlž
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
rozličné	rozličný	k2eAgFnPc1d1	rozličná
formy	forma	k1gFnPc1	forma
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
životních	životní	k2eAgNnPc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Krásné	krásný	k2eAgInPc1d1	krásný
české	český	k2eAgInPc1d1	český
názvy	název	k1gInPc1	název
Mila	Mil	k1gInSc2	Mil
<g/>
,	,	kIx,	,
Panenka	panenka	k1gFnSc1	panenka
<g/>
,	,	kIx,	,
Tetinka	tetinka	k1gFnSc1	tetinka
<g/>
,	,	kIx,	,
Slava	Slava	k1gFnSc1	Slava
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
pocházejí	pocházet	k5eAaImIp3nP	pocházet
od	od	k7c2	od
Joachima	Joachim	k1gMnSc2	Joachim
Barrandeho	Barrande	k1gMnSc2	Barrande
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
poznání	poznání	k1gNnSc4	poznání
vývoje	vývoj	k1gInSc2	vývoj
měkkýšů	měkkýš	k1gMnPc2	měkkýš
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc1d1	velký
přínos	přínos	k1gInSc1	přínos
výlov	výlov	k1gInSc1	výlov
13	[number]	k4	13
zástupců	zástupce	k1gMnPc2	zástupce
třídy	třída	k1gFnSc2	třída
přílipkovci	přílipkovec	k1gMnPc5	přílipkovec
(	(	kIx(	(
<g/>
Monoplacophora	Monoplacophora	k1gFnSc1	Monoplacophora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vylovených	vylovený	k2eAgFnPc2d1	vylovená
roku	rok	k1gInSc3	rok
1952	[number]	k4	1952
dánskou	dánský	k2eAgFnSc7d1	dánská
expediční	expediční	k2eAgFnSc7d1	expediční
lodí	loď	k1gFnSc7	loď
Galathea	Galatheum	k1gNnSc2	Galatheum
pro	pro	k7c4	pro
muzeum	muzeum	k1gNnSc4	muzeum
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
z	z	k7c2	z
hloubky	hloubka	k1gFnSc2	hloubka
3570	[number]	k4	3570
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nazváni	nazván	k2eAgMnPc1d1	nazván
byli	být	k5eAaImAgMnP	být
Neopilina	Neopilin	k2eAgNnPc4d1	Neopilin
galatheae	galatheae	k1gNnPc4	galatheae
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
siluru	silur	k1gInSc2	silur
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
i	i	k8xC	i
nálezy	nález	k1gInPc1	nález
celých	celý	k2eAgFnPc2d1	celá
koster	kostra	k1gFnPc2	kostra
obratlovců	obratlovec	k1gMnPc2	obratlovec
-	-	kIx~	-
bezčelistnatých	bezčelistnatý	k2eAgFnPc2d1	bezčelistnatý
ryb	ryba	k1gFnPc2	ryba
Agnatha	Agnatha	k1gFnSc1	Agnatha
(	(	kIx(	(
<g/>
Tremataspis	Tremataspis	k1gFnSc1	Tremataspis
cyathaspis	cyathaspis	k1gFnSc1	cyathaspis
<g/>
)	)	kIx)	)
s	s	k7c7	s
chrupavčitou	chrupavčitý	k2eAgFnSc7d1	chrupavčitá
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
kostrou	kostra	k1gFnSc7	kostra
a	a	k8xC	a
tělem	tělo	k1gNnSc7	tělo
chráněným	chráněný	k2eAgInSc7d1	chráněný
kostěným	kostěný	k2eAgInSc7d1	kostěný
krunýřem	krunýř	k1gInSc7	krunýř
<g/>
,	,	kIx,	,
trnoploutvé	trnoploutvá	k1gFnPc1	trnoploutvá
(	(	kIx(	(
<g/>
Acanthodii	Acanthodie	k1gFnSc4	Acanthodie
<g/>
)	)	kIx)	)
a	a	k8xC	a
pancéřnaté	pancéřnatý	k2eAgNnSc1d1	pancéřnatý
(	(	kIx(	(
<g/>
Placodermi	Placoder	k1gFnPc7	Placoder
<g/>
)	)	kIx)	)
ryby	ryba	k1gFnPc1	ryba
již	již	k6eAd1	již
mají	mít	k5eAaImIp3nP	mít
vyvinutu	vyvinut	k2eAgFnSc4d1	vyvinuta
spodní	spodní	k2eAgFnSc4d1	spodní
čelist	čelist	k1gFnSc4	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přeměnou	přeměna	k1gFnSc7	přeměna
předních	přední	k2eAgInPc2d1	přední
žaberních	žaberní	k2eAgInPc2d1	žaberní
oblouků	oblouk	k1gInPc2	oblouk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Flóra	Flóra	k1gFnSc1	Flóra
===	===	k?	===
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
velkého	velký	k2eAgInSc2d1	velký
vývoje	vývoj	k1gInSc2	vývoj
řas	řasa	k1gFnPc2	řasa
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
objevují	objevovat	k5eAaImIp3nP	objevovat
vyšší	vysoký	k2eAgFnPc4d2	vyšší
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Rody	rod	k1gInPc1	rod
Cooksonia	Cooksonium	k1gNnSc2	Cooksonium
a	a	k8xC	a
Baragwanathia	Baragwanathium	k1gNnSc2	Baragwanathium
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
již	již	k6eAd1	již
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
suchozemské	suchozemský	k2eAgFnPc1d1	suchozemská
cévnaté	cévnatý	k2eAgFnPc1d1	cévnatá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
se	se	k3xPyFc4	se
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
na	na	k7c4	na
souš	souš	k1gFnSc4	souš
musely	muset	k5eAaImAgFnP	muset
bránit	bránit	k5eAaImF	bránit
před	před	k7c7	před
vysycháním	vysychání	k1gNnSc7	vysychání
silnou	silný	k2eAgFnSc4d1	silná
pokožkou	pokožka	k1gFnSc7	pokožka
<g/>
,	,	kIx,	,
opatřenou	opatřený	k2eAgFnSc7d1	opatřená
průduchy	průduch	k1gInPc7	průduch
<g/>
,	,	kIx,	,
umožňujícími	umožňující	k2eAgInPc7d1	umožňující
transpiraci	transpirace	k1gFnSc4	transpirace
<g/>
.	.	kIx.	.
</s>
<s>
Vodivé	vodivý	k2eAgNnSc1d1	vodivé
pletivo	pletivo	k1gNnSc1	pletivo
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
rozvádět	rozvádět	k5eAaImF	rozvádět
živiny	živina	k1gFnPc4	živina
získané	získaný	k2eAgFnPc4d1	získaná
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
do	do	k7c2	do
horních	horní	k2eAgFnPc2d1	horní
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Psilophyton	Psilophyton	k1gInSc4	Psilophyton
již	již	k6eAd1	již
mají	mít	k5eAaImIp3nP	mít
primitivní	primitivní	k2eAgInSc4d1	primitivní
xylem	xylem	k1gInSc4	xylem
(	(	kIx(	(
<g/>
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
)	)	kIx)	)
a	a	k8xC	a
floem	floem	k1gInSc1	floem
(	(	kIx(	(
<g/>
lýko	lýko	k1gNnSc1	lýko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neměli	mít	k5eNaImAgMnP	mít
však	však	k9	však
ještě	ještě	k6eAd1	ještě
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
kořenový	kořenový	k2eAgInSc1d1	kořenový
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Množili	množit	k5eAaImAgMnP	množit
se	se	k3xPyFc4	se
výtrusy	výtrus	k1gInPc7	výtrus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svrchním	svrchní	k2eAgInSc6d1	svrchní
siluru	silur	k1gInSc6	silur
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
Rhyniophyta	Rhyniophyto	k1gNnPc1	Rhyniophyto
<g/>
,	,	kIx,	,
primitivní	primitivní	k2eAgFnPc1d1	primitivní
plavuně	plavuň	k1gFnPc1	plavuň
a	a	k8xC	a
suchozemské	suchozemský	k2eAgFnPc1d1	suchozemská
houby	houba	k1gFnPc1	houba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Regionální	regionální	k2eAgNnSc1d1	regionální
rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Asie	Asie	k1gFnSc2	Asie
===	===	k?	===
</s>
</p>
<p>
<s>
Zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
vynořeny	vynořen	k2eAgInPc1d1	vynořen
staré	starý	k2eAgInPc1d1	starý
štíty	štít	k1gInPc1	štít
(	(	kIx(	(
<g/>
angarský	angarský	k2eAgInSc1d1	angarský
<g/>
,	,	kIx,	,
anabarský	anabarský	k2eAgInSc1d1	anabarský
<g/>
,	,	kIx,	,
kolymský	kolymský	k2eAgInSc1d1	kolymský
<g/>
,	,	kIx,	,
části	část	k1gFnPc4	část
čínského	čínský	k2eAgMnSc2d1	čínský
a	a	k8xC	a
indického	indický	k2eAgInSc2d1	indický
štítu	štít	k1gInSc2	štít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
převládají	převládat	k5eAaImIp3nP	převládat
mělkovodní	mělkovodní	k2eAgFnPc1d1	mělkovodní
karbonátické	karbonátický	k2eAgFnPc1d1	karbonátický
facie	facie	k1gFnPc1	facie
s	s	k7c7	s
hojnou	hojný	k2eAgFnSc7d1	hojná
bentickou	bentický	k2eAgFnSc7d1	bentická
faunou	fauna	k1gFnSc7	fauna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
geosynklinálních	geosynklinální	k2eAgFnPc6d1	geosynklinální
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
ukládají	ukládat	k5eAaImIp3nP	ukládat
graptolitové	graptolitový	k2eAgFnPc1d1	graptolitový
břidlice	břidlice	k1gFnPc1	břidlice
<g/>
,	,	kIx,	,
peliticko-karbonátické	pelitickoarbonátický	k2eAgInPc1d1	peliticko-karbonátický
i	i	k8xC	i
klastické	klastický	k2eAgInPc1d1	klastický
sedimenty	sediment	k1gInPc1	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východouralské	východouralský	k2eAgFnSc6d1	východouralský
a	a	k8xC	a
kazachstánské	kazachstánský	k2eAgFnSc6d1	kazachstánská
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
doprovázeny	doprovázet	k5eAaImNgInP	doprovázet
výlevy	výlev	k1gInPc1	výlev
láv	láva	k1gFnPc2	láva
bazaltového	bazaltový	k2eAgInSc2d1	bazaltový
a	a	k8xC	a
andezitového	andezitový	k2eAgInSc2d1	andezitový
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
postižena	postihnout	k5eAaPmNgFnS	postihnout
mezi	mezi	k7c7	mezi
ordovikem	ordovik	k1gInSc7	ordovik
a	a	k8xC	a
silurem	silur	k1gInSc7	silur
takonským	takonský	k2eAgNnPc3d1	takonský
vrásněním	vrásnění	k1gNnPc3	vrásnění
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
střední	střední	k2eAgFnSc1d1	střední
Asie	Asie	k1gFnSc1	Asie
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
Čína	Čína	k1gFnSc1	Čína
až	až	k6eAd1	až
mladokaledonskou	mladokaledonský	k2eAgFnSc7d1	mladokaledonský
orogenezí	orogeneze	k1gFnSc7	orogeneze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
===	===	k?	===
</s>
</p>
<p>
<s>
Karbonátická	Karbonátický	k2eAgFnSc1d1	Karbonátický
facie	facie	k1gFnSc1	facie
s	s	k7c7	s
útesy	útes	k1gInPc7	útes
společně	společně	k6eAd1	společně
s	s	k7c7	s
evapority	evaporit	k1gInPc7	evaporit
dokládají	dokládat	k5eAaImIp3nP	dokládat
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
rovníkové	rovníkový	k2eAgFnSc6d1	Rovníková
oblasti	oblast	k1gFnSc6	oblast
s	s	k7c7	s
aridním	aridní	k2eAgNnSc7d1	aridní
klimatem	klima	k1gNnSc7	klima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
appalačské	appalačský	k2eAgFnSc6d1	appalačský
geosynklinále	geosynklinála	k1gFnSc6	geosynklinála
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
mělké	mělký	k2eAgNnSc4d1	mělké
moře	moře	k1gNnSc4	moře
většinu	většina	k1gFnSc4	většina
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
sedimentují	sedimentovat	k5eAaImIp3nP	sedimentovat
písčité	písčitý	k2eAgFnPc1d1	písčitá
a	a	k8xC	a
karbonátické	karbonátický	k2eAgFnPc1d1	karbonátický
uloženiny	uloženina	k1gFnPc1	uloženina
<g/>
,	,	kIx,	,
bohaté	bohatý	k2eAgNnSc1d1	bohaté
na	na	k7c4	na
faunu	fauna	k1gFnSc4	fauna
(	(	kIx(	(
<g/>
brachiopodi	brachiopod	k1gMnPc1	brachiopod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pacifické	pacifický	k2eAgFnPc4d1	Pacifická
(	(	kIx(	(
<g/>
západní	západní	k2eAgInSc4d1	západní
okraj	okraj	k1gInSc4	okraj
<g/>
)	)	kIx)	)
a	a	k8xC	a
franklinské	franklinský	k2eAgNnSc1d1	franklinský
(	(	kIx(	(
<g/>
sev	sev	k?	sev
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Grónsko	Grónsko	k1gNnSc1	Grónsko
<g/>
)	)	kIx)	)
oblasti	oblast	k1gFnPc1	oblast
se	se	k3xPyFc4	se
ukládají	ukládat	k5eAaImIp3nP	ukládat
graptolitové	graptolitový	k2eAgFnPc4d1	graptolitový
břidlice	břidlice	k1gFnPc4	břidlice
a	a	k8xC	a
vápence	vápenec	k1gInPc4	vápenec
s	s	k7c7	s
pelagickou	pelagický	k2eAgFnSc7d1	pelagická
faunou	fauna	k1gFnSc7	fauna
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
korelovatelnou	korelovatelný	k2eAgFnSc4d1	korelovatelný
s	s	k7c7	s
Barrandienem	barrandien	k1gInSc7	barrandien
<g/>
.	.	kIx.	.
</s>
<s>
Silurské	silurský	k2eAgInPc1d1	silurský
sedimenty	sediment	k1gInPc1	sediment
s	s	k7c7	s
ložisky	ložisko	k1gNnPc7	ložisko
hematitových	hematitový	k2eAgFnPc2d1	hematitový
rud	ruda	k1gFnPc2	ruda
<g/>
,	,	kIx,	,
solnými	solný	k2eAgNnPc7d1	solné
ložisky	ložisko	k1gNnPc7	ložisko
(	(	kIx(	(
<g/>
Velká	velká	k1gFnSc1	velká
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
až	až	k9	až
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
mocnost	mocnost	k1gFnSc1	mocnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
roponosné	roponosný	k2eAgFnSc2d1	roponosný
struktury	struktura	k1gFnSc2	struktura
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
mají	mít	k5eAaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
povodí	povodí	k1gNnSc2	povodí
Amazonky	Amazonka	k1gFnSc2	Amazonka
a	a	k8xC	a
z	z	k7c2	z
Bolívie	Bolívie	k1gFnSc2	Bolívie
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
jílovito-písčité	jílovitoísčitý	k2eAgFnPc1d1	jílovito-písčitý
facie	facie	k1gFnPc1	facie
s	s	k7c7	s
graptolity	graptolit	k1gMnPc7	graptolit
<g/>
,	,	kIx,	,
silurského	silurský	k2eAgNnSc2d1	silurské
stáří	stáří	k1gNnSc2	stáří
jsou	být	k5eAaImIp3nP	být
glaciální	glaciální	k2eAgFnPc1d1	glaciální
uloženiny	uloženina	k1gFnPc1	uloženina
z	z	k7c2	z
Argentiny	Argentina	k1gFnSc2	Argentina
a	a	k8xC	a
Bolívie	Bolívie	k1gFnSc2	Bolívie
<g/>
.	.	kIx.	.
</s>
<s>
Mořská	mořský	k2eAgFnSc1d1	mořská
fauna	fauna	k1gFnSc1	fauna
má	mít	k5eAaImIp3nS	mít
endemický	endemický	k2eAgInSc4d1	endemický
ráz	ráz	k1gInSc4	ráz
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
malvinokaffrické	malvinokaffrický	k2eAgFnSc3d1	malvinokaffrický
provincii	provincie	k1gFnSc3	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
charakter	charakter	k1gInSc1	charakter
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
chladné	chladný	k2eAgFnSc6d1	chladná
klimatické	klimatický	k2eAgFnSc6d1	klimatická
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Austrálie	Austrálie	k1gFnSc2	Austrálie
===	===	k?	===
</s>
</p>
<p>
<s>
Sedimentace	sedimentace	k1gFnSc1	sedimentace
se	se	k3xPyFc4	se
koncentruje	koncentrovat	k5eAaBmIp3nS	koncentrovat
do	do	k7c2	do
tasmánské	tasmánský	k2eAgFnSc2d1	tasmánská
oblasti	oblast	k1gFnSc2	oblast
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
dnešního	dnešní	k2eAgInSc2d1	dnešní
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodním	spodní	k2eAgInSc6d1	spodní
siluru	silur	k1gInSc6	silur
byla	být	k5eAaImAgFnS	být
postižena	postižen	k2eAgFnSc1d1	postižena
benambranskou	benambranský	k2eAgFnSc7d1	benambranský
orogenezí	orogeneze	k1gFnSc7	orogeneze
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
oblast	oblast	k1gFnSc4	oblast
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
paralelních	paralelní	k2eAgFnPc2d1	paralelní
pánví	pánev	k1gFnPc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Vrásnění	vrásnění	k1gNnSc1	vrásnění
doprovázel	doprovázet	k5eAaImAgInS	doprovázet
kyselý	kyselý	k2eAgInSc1d1	kyselý
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
<g/>
.	.	kIx.	.
</s>
<s>
Svrchní	svrchní	k2eAgInSc1d1	svrchní
silur	silur	k1gInSc1	silur
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
karbonáty	karbonát	k1gInPc4	karbonát
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
brachiopodovou	brachiopodový	k2eAgFnSc7d1	brachiopodový
<g/>
,	,	kIx,	,
korálovou	korálový	k2eAgFnSc7d1	korálová
a	a	k8xC	a
trilobitovou	trilobitový	k2eAgFnSc7d1	trilobitová
faunou	fauna	k1gFnSc7	fauna
<g/>
,	,	kIx,	,
mající	mající	k2eAgInSc1d1	mající
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
asijské	asijský	k2eAgFnSc3d1	asijská
a	a	k8xC	a
evropské	evropský	k2eAgFnSc3d1	Evropská
fauně	fauna	k1gFnSc3	fauna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Afrika	Afrika	k1gFnSc1	Afrika
===	===	k?	===
</s>
</p>
<p>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
je	být	k5eAaImIp3nS	být
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
přínos	přínos	k1gInSc4	přínos
písčitého	písčitý	k2eAgInSc2d1	písčitý
materiálu	materiál	k1gInSc2	materiál
z	z	k7c2	z
vynořeného	vynořený	k2eAgInSc2d1	vynořený
afrického	africký	k2eAgInSc2d1	africký
štítu	štít	k1gInSc2	štít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
zatopené	zatopený	k2eAgNnSc1d1	zatopené
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
sedimentovaly	sedimentovat	k5eAaImAgFnP	sedimentovat
graptolitové	graptolitový	k2eAgFnPc1d1	graptolitový
břidlice	břidlice	k1gFnPc1	břidlice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svrchním	svrchní	k2eAgInSc6d1	svrchní
siluru	silur	k1gInSc6	silur
karbonátická	karbonátický	k2eAgFnSc1d1	karbonátický
facie	facie	k1gFnSc1	facie
s	s	k7c7	s
faunou	fauna	k1gFnSc7	fauna
velmi	velmi	k6eAd1	velmi
blízkou	blízký	k2eAgFnSc7d1	blízká
Barrandienu	barrandien	k1gInSc6	barrandien
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
plynulém	plynulý	k2eAgInSc6d1	plynulý
přechodu	přechod	k1gInSc6	přechod
do	do	k7c2	do
devonu	devon	k1gInSc2	devon
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
a	a	k8xC	a
Maroku	Maroko	k1gNnSc6	Maroko
dobře	dobře	k6eAd1	dobře
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
scyfokrinitový	scyfokrinitový	k2eAgInSc1d1	scyfokrinitový
horizont	horizont	k1gInSc1	horizont
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Evropa	Evropa	k1gFnSc1	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
Evropa	Evropa	k1gFnSc1	Evropa
====	====	k?	====
</s>
</p>
<p>
<s>
Sedimenty	sediment	k1gInPc1	sediment
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
pánve	pánev	k1gFnSc2	pánev
Protoatlantiku	Protoatlantika	k1gFnSc4	Protoatlantika
<g/>
,	,	kIx,	,
klasickou	klasický	k2eAgFnSc7d1	klasická
oblastí	oblast	k1gFnSc7	oblast
je	být	k5eAaImIp3nS	být
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byl	být	k5eAaImAgInS	být
silur	silur	k1gInSc1	silur
poprvé	poprvé	k6eAd1	poprvé
popsán	popsat	k5eAaPmNgInS	popsat
a	a	k8xC	a
detailně	detailně	k6eAd1	detailně
prozkoumán	prozkoumat	k5eAaPmNgInS	prozkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Sedimentační	sedimentační	k2eAgInSc4d1	sedimentační
prostor	prostor	k1gInSc4	prostor
SV-JZ	SV-JZ	k1gMnSc2	SV-JZ
směru	směr	k1gInSc2	směr
byl	být	k5eAaImAgInS	být
rozčleněn	rozčleněn	k2eAgInSc1d1	rozčleněn
elevacemi	elevace	k1gFnPc7	elevace
<g/>
,	,	kIx,	,
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
s	s	k7c7	s
mělkovodními	mělkovodní	k2eAgFnPc7d1	mělkovodní
písčitými	písčitý	k2eAgFnPc7d1	písčitá
a	a	k8xC	a
karbonátickými	karbonátický	k2eAgFnPc7d1	karbonátický
uloženinami	uloženina	k1gFnPc7	uloženina
(	(	kIx(	(
<g/>
mocnost	mocnost	k1gFnSc4	mocnost
až	až	k9	až
tisíce	tisíc	k4xCgInPc1	tisíc
metrů	metr	k1gMnPc2	metr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
s	s	k7c7	s
graptolitovými	graptolitový	k2eAgFnPc7d1	graptolitový
břidlicemi	břidlice	k1gFnPc7	břidlice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
ludlovu	ludlův	k2eAgFnSc4d1	ludlův
díky	díky	k7c3	díky
regresi	regrese	k1gFnSc3	regrese
moře	moře	k1gNnSc2	moře
nahrazeny	nahrazen	k2eAgInPc4d1	nahrazen
vápenatými	vápenatý	k2eAgInPc7d1	vápenatý
sedimenty	sediment	k1gInPc7	sediment
<g/>
,	,	kIx,	,
bohatými	bohatý	k2eAgInPc7d1	bohatý
bentickou	bentický	k2eAgFnSc7d1	bentická
faunou	fauna	k1gFnSc7	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
ludlovu	ludlovat	k5eAaPmIp1nS	ludlovat
sedimentují	sedimentovat	k5eAaImIp3nP	sedimentovat
klastika	klastik	k1gMnSc4	klastik
old	old	k?	old
redu	reda	k1gMnSc4	reda
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
část	část	k1gFnSc1	část
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
silur-devon	silurevona	k1gFnPc2	silur-devona
postižena	postihnout	k5eAaPmNgFnS	postihnout
kaledonským	kaledonský	k2eAgNnSc7d1	kaledonské
vrásněním	vrásnění	k1gNnSc7	vrásnění
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
srážky	srážka	k1gFnSc2	srážka
baltického	baltický	k2eAgInSc2d1	baltický
štítu	štít	k1gInSc2	štít
s	s	k7c7	s
bloky	blok	k1gInPc7	blok
Erie	Eri	k1gFnSc2	Eri
<g/>
.	.	kIx.	.
</s>
<s>
Kaledonská	Kaledonský	k2eAgFnSc1d1	Kaledonská
metamorfóza	metamorfóza	k1gFnSc1	metamorfóza
vedla	vést	k5eAaImAgFnS	vést
až	až	k9	až
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
migmatitů	migmatit	k1gInPc2	migmatit
a	a	k8xC	a
serpentinitů	serpentinit	k1gInPc2	serpentinit
<g/>
,	,	kIx,	,
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
byla	být	k5eAaImAgFnS	být
výlevy	výlev	k1gInPc1	výlev
ofiolitů	ofiolit	k1gInPc2	ofiolit
(	(	kIx(	(
<g/>
bazické	bazický	k2eAgFnPc4d1	bazická
lávy	láva	k1gFnPc4	láva
<g/>
)	)	kIx)	)
a	a	k8xC	a
výstupy	výstup	k1gInPc1	výstup
granitoidů	granitoid	k1gInPc2	granitoid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Baltická	baltický	k2eAgFnSc1d1	baltická
oblast	oblast	k1gFnSc1	oblast
====	====	k?	====
</s>
</p>
<p>
<s>
ostrov	ostrov	k1gInSc1	ostrov
Gotland	Gotland	k1gInSc1	Gotland
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
hl.	hl.	k?	hl.
město	město	k1gNnSc4	město
Visby	Visba	k1gFnSc2	Visba
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
klasická	klasický	k2eAgFnSc1d1	klasická
oblast	oblast	k1gFnSc1	oblast
mělkovodního	mělkovodní	k2eAgInSc2d1	mělkovodní
slínito-karbonátického	slínitoarbonátický	k2eAgInSc2d1	slínito-karbonátický
vývoje	vývoj	k1gInSc2	vývoj
siluru	silur	k1gInSc2	silur
o	o	k7c6	o
mocnosti	mocnost	k1gFnSc6	mocnost
stovek	stovka	k1gFnPc2	stovka
metrů	metr	k1gMnPc2	metr
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
zachovalými	zachovalý	k2eAgInPc7d1	zachovalý
zbytky	zbytek	k1gInPc7	zbytek
brachiopodů	brachiopod	k1gInPc2	brachiopod
<g/>
,	,	kIx,	,
korálů	korál	k1gInPc2	korál
<g/>
,	,	kIx,	,
stromatoporů	stromatopor	k1gInPc2	stromatopor
<g/>
,	,	kIx,	,
krinoidů	krinoid	k1gInPc2	krinoid
<g/>
,	,	kIx,	,
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
,	,	kIx,	,
trilobitů	trilobit	k1gMnPc2	trilobit
a	a	k8xC	a
ostrakodů	ostrakod	k1gMnPc2	ostrakod
<g/>
.	.	kIx.	.
</s>
<s>
Odkryty	odkryt	k1gInPc4	odkryt
byly	být	k5eAaImAgFnP	být
útesy	útes	k1gInPc4	útes
(	(	kIx(	(
<g/>
biohermy	bioherm	k1gInPc4	bioherm
<g/>
)	)	kIx)	)
stromatoporů	stromatopor	k1gInPc2	stromatopor
<g/>
,	,	kIx,	,
korálů	korál	k1gInPc2	korál
a	a	k8xC	a
vápenitých	vápenitý	k2eAgFnPc2d1	vápenitá
řas	řasa	k1gFnPc2	řasa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
-	-	kIx~	-
silur	silur	k1gInSc1	silur
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
z	z	k7c2	z
vrtů	vrt	k1gInPc2	vrt
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgInSc1d1	spodní
silur	silur	k1gInSc1	silur
má	mít	k5eAaImIp3nS	mít
stabilní	stabilní	k2eAgInSc4d1	stabilní
vývoj	vývoj	k1gInSc4	vývoj
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
graptolitových	graptolitový	k2eAgFnPc2d1	graptolitový
břidlic	břidlice	k1gFnPc2	břidlice
<g/>
,	,	kIx,	,
v	v	k7c4	v
ludlovu	ludlův	k2eAgFnSc4d1	ludlův
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
sedimentace	sedimentace	k1gFnSc2	sedimentace
jen	jen	k9	jen
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
slínité	slínitý	k2eAgInPc1d1	slínitý
<g/>
,	,	kIx,	,
karbonátické	karbonátický	k2eAgInPc1d1	karbonátický
sedimenty	sediment	k1gInPc1	sediment
mělkého	mělký	k2eAgNnSc2d1	mělké
moře	moře	k1gNnSc2	moře
<g/>
)	)	kIx)	)
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
(	(	kIx(	(
<g/>
Svatokřížské	Svatokřížský	k2eAgFnPc1d1	Svatokřížský
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
droby	drob	k1gInPc1	drob
<g/>
,	,	kIx,	,
klastika	klastika	k1gFnSc1	klastika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
-	-	kIx~	-
výskyty	výskyt	k1gInPc1	výskyt
transgresívně	transgresívně	k6eAd1	transgresívně
uloženého	uložený	k2eAgInSc2d1	uložený
siluru	silur	k1gInSc2	silur
na	na	k7c6	na
svrchním	svrchní	k2eAgInSc6d1	svrchní
ordoviku	ordovik	k1gInSc6	ordovik
byly	být	k5eAaImAgFnP	být
odkryty	odkrýt	k5eAaPmNgFnP	odkrýt
v	v	k7c6	v
zářezech	zářez	k1gInPc6	zářez
řeky	řeka	k1gFnSc2	řeka
Dněstr	Dněstr	k1gInSc1	Dněstr
<g/>
.	.	kIx.	.
</s>
<s>
Převládá	převládat	k5eAaImIp3nS	převládat
slínito-karbonátická	slínitoarbonátický	k2eAgFnSc1d1	slínito-karbonátický
facie	facie	k1gFnSc1	facie
s	s	k7c7	s
bentickou	bentický	k2eAgFnSc7d1	bentická
faunou	fauna	k1gFnSc7	fauna
<g/>
,	,	kIx,	,
vzácnější	vzácný	k2eAgMnPc1d2	vzácnější
jsou	být	k5eAaImIp3nP	být
graptoliti	graptolit	k1gMnPc1	graptolit
<g/>
.	.	kIx.	.
</s>
<s>
Sedimentace	sedimentace	k1gFnSc1	sedimentace
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
plynule	plynule	k6eAd1	plynule
až	až	k9	až
do	do	k7c2	do
devonu	devon	k1gInSc2	devon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Alpsko-karpatská	alpskoarpatský	k2eAgFnSc1d1	alpsko-karpatský
oblast	oblast	k1gFnSc1	oblast
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
drobové	drobový	k2eAgFnSc6d1	Drobová
zóně	zóna	k1gFnSc6	zóna
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc2d1	východní
Alpy	alpa	k1gFnSc2	alpa
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
siluru	silur	k1gInSc2	silur
grafitické	grafitický	k2eAgFnSc2d1	grafitická
břidlice	břidlice	k1gFnSc2	břidlice
s	s	k7c7	s
ojedinělými	ojedinělý	k2eAgMnPc7d1	ojedinělý
graptolity	graptolit	k1gMnPc7	graptolit
v	v	k7c6	v
podloží	podloží	k1gNnSc6	podloží
vápenců	vápenec	k1gInPc2	vápenec
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
sedimentovaly	sedimentovat	k5eAaImAgFnP	sedimentovat
i	i	k9	i
v	v	k7c6	v
devonu	devon	k1gInSc6	devon
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
nálezy	nález	k1gInPc1	nález
siluru	silur	k1gInSc2	silur
v	v	k7c6	v
Karnských	Karnský	k2eAgFnPc6d1	Karnský
Alpách	Alpy	k1gFnPc6	Alpy
(	(	kIx(	(
<g/>
hranice	hranice	k1gFnSc1	hranice
Rakousko-Itálie	Rakousko-Itálie	k1gFnSc2	Rakousko-Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
faciálně	faciálně	k6eAd1	faciálně
pestré	pestrý	k2eAgInPc1d1	pestrý
s	s	k7c7	s
faunou	fauna	k1gFnSc7	fauna
českého	český	k2eAgInSc2d1	český
typu	typ	k1gInSc2	typ
a	a	k8xC	a
graptolitovými	graptolitový	k2eAgFnPc7d1	graptolitový
břidlicemi	břidlice	k1gFnPc7	břidlice
(	(	kIx(	(
<g/>
sedimentovaly	sedimentovat	k5eAaImAgInP	sedimentovat
do	do	k7c2	do
spodního	spodní	k2eAgInSc2d1	spodní
devonu	devon	k1gInSc2	devon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Západních	západní	k2eAgInPc6d1	západní
Karpatech	Karpaty	k1gInPc6	Karpaty
se	se	k3xPyFc4	se
nálezy	nález	k1gInPc1	nález
opírají	opírat	k5eAaImIp3nP	opírat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
mikropaleontologické	mikropaleontologický	k2eAgInPc4d1	mikropaleontologický
nálezy	nález	k1gInPc4	nález
ve	v	k7c6	v
vlachovském	vlachovský	k2eAgNnSc6d1	vlachovský
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
fylity	fylit	k1gInPc1	fylit
<g/>
,	,	kIx,	,
lydity	lydit	k1gInPc1	lydit
<g/>
)	)	kIx)	)
a	a	k8xC	a
klastikách	klastika	k1gFnPc6	klastika
nadložního	nadložní	k2eAgNnSc2d1	nadložní
souvrství	souvrství	k1gNnSc2	souvrství
Bystrého	bystrý	k2eAgInSc2d1	bystrý
potoka	potok	k1gInSc2	potok
gelnické	gelnický	k2eAgFnSc2d1	gelnický
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
Čertova	čertův	k2eAgFnSc1d1	Čertova
hoľa	hoľa	k1gFnSc1	hoľa
<g/>
,	,	kIx,	,
Betliar	Betliar	k1gInSc1	Betliar
<g/>
,	,	kIx,	,
Čučma	Čučma	k1gFnSc1	Čučma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Český	český	k2eAgInSc1d1	český
masív	masív	k1gInSc1	masív
====	====	k?	====
</s>
</p>
<p>
<s>
BarrandienPlynulá	BarrandienPlynulý	k2eAgFnSc1d1	BarrandienPlynulý
sedimentace	sedimentace	k1gFnSc1	sedimentace
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
siluru	silur	k1gInSc2	silur
<g/>
,	,	kIx,	,
hojnost	hojnost	k1gFnSc1	hojnost
nálezů	nález	k1gInPc2	nález
zkamenělin	zkamenělina	k1gFnPc2	zkamenělina
a	a	k8xC	a
úroveň	úroveň	k1gFnSc4	úroveň
prozkoumanosti	prozkoumanost	k1gFnSc2	prozkoumanost
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
klasickou	klasický	k2eAgFnSc4d1	klasická
světovou	světový	k2eAgFnSc4d1	světová
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Sedimenty	sediment	k1gInPc1	sediment
siluru	silur	k1gInSc2	silur
leží	ležet	k5eAaImIp3nP	ležet
konkordantně	konkordantně	k6eAd1	konkordantně
na	na	k7c6	na
kosovském	kosovský	k2eAgNnSc6d1	kosovské
souvrství	souvrství	k1gNnSc6	souvrství
svrchního	svrchní	k2eAgInSc2d1	svrchní
ordoviku	ordovik	k1gInSc2	ordovik
<g/>
,	,	kIx,	,
začátek	začátek	k1gInSc1	začátek
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
náhlým	náhlý	k2eAgInSc7d1	náhlý
nástupem	nástup	k1gInSc7	nástup
facie	facie	k1gFnSc2	facie
graptolitových	graptolitový	k2eAgFnPc2d1	graptolitový
břidlic	břidlice	k1gFnPc2	břidlice
zóny	zóna	k1gFnSc2	zóna
Akidograptus	Akidograptus	k1gMnSc1	Akidograptus
ascensus	ascensus	k1gMnSc1	ascensus
<g/>
.	.	kIx.	.
</s>
<s>
Bližší	blízký	k2eAgNnSc1d2	bližší
členění	členění	k1gNnSc1	členění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
liteňské	liteňský	k2eAgNnSc1d1	liteňský
souvrství	souvrství	k1gNnSc1	souvrství
</s>
</p>
<p>
<s>
želkovické	želkovický	k2eAgFnPc1d1	želkovický
vrstvy	vrstva	k1gFnPc1	vrstva
(	(	kIx(	(
<g/>
břidlice	břidlice	k1gFnPc1	břidlice
<g/>
,	,	kIx,	,
silicity	silicit	k1gInPc1	silicit
<g/>
,	,	kIx,	,
vápence	vápenec	k1gInPc1	vápenec
<g/>
,	,	kIx,	,
tufity	tufit	k1gInPc1	tufit
<g/>
)	)	kIx)	)
-	-	kIx~	-
llandovery	llandovera	k1gFnPc1	llandovera
<g/>
,	,	kIx,	,
mocnost	mocnost	k1gFnSc1	mocnost
16-24	[number]	k4	16-24
metrů	metr	k1gInPc2	metr
</s>
</p>
<p>
<s>
litohlavské	litohlavský	k2eAgFnPc1d1	litohlavský
vrstvy	vrstva	k1gFnPc1	vrstva
(	(	kIx(	(
<g/>
llandovery	llandovera	k1gFnPc1	llandovera
<g/>
,	,	kIx,	,
černé	černý	k2eAgFnPc1d1	černá
břidlice	břidlice	k1gFnPc1	břidlice
<g/>
,	,	kIx,	,
jílovce	jílovec	k1gInPc1	jílovec
<g/>
,	,	kIx,	,
21-75	[number]	k4	21-75
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
motolské	motolský	k2eAgFnPc1d1	motolská
vrstvy	vrstva	k1gFnPc1	vrstva
(	(	kIx(	(
<g/>
wenlock	wenlock	k1gInSc1	wenlock
<g/>
)	)	kIx)	)
-	-	kIx~	-
mocnost	mocnost	k1gFnSc1	mocnost
100-200	[number]	k4	100-200
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
vápenaté	vápenatý	k2eAgFnPc1d1	vápenatá
břidlice	břidlice	k1gFnPc1	břidlice
<g/>
,	,	kIx,	,
graptolitové	graptolitový	k2eAgFnPc1d1	graptolitový
břidlice	břidlice	k1gFnPc1	břidlice
(	(	kIx(	(
<g/>
Cyrtograptus	Cyrtograptus	k1gInSc1	Cyrtograptus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tufitické	tufitický	k2eAgFnPc1d1	tufitický
břidlice	břidlice	k1gFnPc1	břidlice
<g/>
,	,	kIx,	,
bazické	bazický	k2eAgFnPc1d1	bazická
vulkanity	vulkanita	k1gFnPc1	vulkanita
oceánického	oceánický	k2eAgInSc2d1	oceánický
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
dolerity	dolerit	k1gInPc4	dolerit
<g/>
,	,	kIx,	,
pikrity	pikrit	k1gInPc4	pikrit
<g/>
)	)	kIx)	)
v	v	k7c6	v
submarinních	submarinní	k2eAgInPc6d1	submarinní
lávových	lávový	k2eAgInPc6d1	lávový
příkrovech	příkrov	k1gInPc6	příkrov
s	s	k7c7	s
mocnými	mocný	k2eAgFnPc7d1	mocná
pyroklastikami	pyroklastika	k1gFnPc7	pyroklastika
o	o	k7c6	o
mocnosti	mocnost	k1gFnSc6	mocnost
až	až	k9	až
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
zapříčinily	zapříčinit	k5eAaPmAgInP	zapříčinit
změlčení	změlčení	k1gNnSc4	změlčení
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
místy	místo	k1gNnPc7	místo
i	i	k9	i
vynoření	vynoření	k1gNnSc1	vynoření
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doprovázené	doprovázený	k2eAgNnSc1d1	doprovázené
karbonátickou	karbonátický	k2eAgFnSc7d1	karbonátický
sedimentací	sedimentace	k1gFnSc7	sedimentace
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
faunou	fauna	k1gFnSc7	fauna
(	(	kIx(	(
<g/>
trilobiti	trilobit	k1gMnPc1	trilobit
<g/>
,	,	kIx,	,
brachipodi	brachipod	k1gMnPc5	brachipod
<g/>
,	,	kIx,	,
krinoidi	krinoid	k1gMnPc5	krinoid
<g/>
,	,	kIx,	,
koráli	korál	k1gMnPc5	korál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kopanické	kopanický	k2eAgNnSc1d1	kopanický
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
ludlov	ludlov	k1gInSc1	ludlov
<g/>
)	)	kIx)	)
-	-	kIx~	-
faciálně	faciálně	k6eAd1	faciálně
velmi	velmi	k6eAd1	velmi
pestré	pestrý	k2eAgNnSc1d1	pestré
<g/>
,	,	kIx,	,
mocnost	mocnost	k1gFnSc1	mocnost
50-250	[number]	k4	50-250
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
vápenaté	vápenatý	k2eAgFnPc1d1	vápenatá
břidlice	břidlice	k1gFnPc1	břidlice
s	s	k7c7	s
graptolity	graptolit	k1gMnPc7	graptolit
<g/>
,	,	kIx,	,
ostrakody	ostrakod	k1gInPc7	ostrakod
<g/>
,	,	kIx,	,
bituminózní	bituminózní	k2eAgInPc4d1	bituminózní
ortocerové	ortocerový	k2eAgInPc4d1	ortocerový
vápence	vápenec	k1gInPc4	vápenec
<g/>
,	,	kIx,	,
organodetritické	organodetritický	k2eAgInPc4d1	organodetritický
vápence	vápenec	k1gInPc4	vápenec
<g/>
,	,	kIx,	,
vulkanity	vulkanit	k1gInPc4	vulkanit
</s>
</p>
<p>
<s>
přídolské	přídolský	k2eAgNnSc1d1	přídolský
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
přídol	přídol	k1gInSc1	přídol
<g/>
)	)	kIx)	)
-	-	kIx~	-
jednodušší	jednoduchý	k2eAgInSc1d2	jednodušší
faciální	faciální	k2eAgInSc1d1	faciální
vývoj	vývoj	k1gInSc1	vývoj
<g/>
,	,	kIx,	,
mocnost	mocnost	k1gFnSc1	mocnost
15-80	[number]	k4	15-80
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
tmavé	tmavý	k2eAgInPc4d1	tmavý
vápence	vápenec	k1gInPc4	vápenec
hlubšího	hluboký	k2eAgNnSc2d2	hlubší
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
prokysličeného	prokysličený	k2eAgNnSc2d1	prokysličený
moře	moře	k1gNnSc2	moře
s	s	k7c7	s
vložkami	vložka	k1gFnPc7	vložka
vápnitých	vápnitý	k2eAgFnPc2d1	vápnitá
břidlic	břidlice	k1gFnPc2	břidlice
(	(	kIx(	(
<g/>
graptoliti	graptolit	k1gMnPc1	graptolit
<g/>
,	,	kIx,	,
nautiloidi	nautiloid	k1gMnPc1	nautiloid
<g/>
)	)	kIx)	)
a	a	k8xC	a
zbytky	zbytek	k1gInPc1	zbytek
splavené	splavený	k2eAgFnSc2d1	splavená
terestické	terestický	k2eAgFnSc2d1	terestický
flóry	flóra	k1gFnSc2	flóra
(	(	kIx(	(
<g/>
Cooksonia	Cooksonium	k1gNnSc2	Cooksonium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
organodetritické	organodetritický	k2eAgInPc4d1	organodetritický
vápence	vápenec	k1gInPc4	vápenec
mělkovodní	mělkovodní	k2eAgFnSc2d1	mělkovodní
facie	facie	k1gFnSc2	facie
s	s	k7c7	s
přisedlými	přisedlý	k2eAgMnPc7d1	přisedlý
bentickými	bentický	k2eAgMnPc7d1	bentický
živočichy	živočich	k1gMnPc7	živočich
(	(	kIx(	(
<g/>
krinoidi	krinoid	k1gMnPc1	krinoid
<g/>
,	,	kIx,	,
brachiopodi	brachiopod	k1gMnPc1	brachiopod
<g/>
,	,	kIx,	,
trilobiti	trilobit	k1gMnPc1	trilobit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Scyphocrinites	Scyphocrinitesa	k1gFnPc2	Scyphocrinitesa
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maximálního	maximální	k2eAgInSc2d1	maximální
rozvoje	rozvoj	k1gInSc2	rozvoj
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
silur-devonrožmitálská	silurevonrožmitálský	k2eAgFnSc1d1	silur-devonrožmitálský
oblast	oblast	k1gFnSc1	oblast
-	-	kIx~	-
siluru	silur	k1gInSc2	silur
patří	patřit	k5eAaImIp3nS	patřit
starorožmitálské	starorožmitálský	k2eAgNnSc1d1	starorožmitálský
souvrství	souvrství	k1gNnSc1	souvrství
s	s	k7c7	s
graptolitovými	graptolitový	k2eAgFnPc7d1	graptolitový
břidlicemi	břidlice	k1gFnPc7	břidlice
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
jak	jak	k8xC	jak
50	[number]	k4	50
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vápence	vápenec	k1gInPc1	vápenec
nejsou	být	k5eNaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
</s>
</p>
<p>
<s>
metamorfované	metamorfovaný	k2eAgInPc1d1	metamorfovaný
ostrovy	ostrov	k1gInPc1	ostrov
-	-	kIx~	-
horniny	hornina	k1gFnPc1	hornina
siluru	silur	k1gInSc2	silur
jsou	být	k5eAaImIp3nP	být
zachovány	zachovat	k5eAaPmNgInP	zachovat
na	na	k7c6	na
sedlčansko-krásnohorském	sedlčanskorásnohorský	k2eAgMnSc6d1	sedlčansko-krásnohorský
<g/>
,	,	kIx,	,
mírovickém	mírovický	k2eAgMnSc6d1	mírovický
a	a	k8xC	a
čerčanském	čerčanský	k2eAgInSc6d1	čerčanský
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
kontaktně	kontaktně	k6eAd1	kontaktně
metamorfovány	metamorfován	k2eAgFnPc1d1	metamorfován
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
s	s	k7c7	s
tmavými	tmavý	k2eAgFnPc7d1	tmavá
grafitickými	grafitický	k2eAgFnPc7d1	grafitická
chiastolickými	chiastolický	k2eAgFnPc7d1	chiastolický
břidlicemi	břidlice	k1gFnPc7	břidlice
s	s	k7c7	s
graptolitovou	graptolitový	k2eAgFnSc7d1	graptolitový
faunou	fauna	k1gFnSc7	fauna
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadloží	nadloží	k1gNnSc6	nadloží
krystalické	krystalický	k2eAgInPc4d1	krystalický
vápence	vápenec	k1gInPc4	vápenec
</s>
</p>
<p>
<s>
Železné	železný	k2eAgFnPc1d1	železná
hory	hora	k1gFnPc1	hora
-	-	kIx~	-
silur	silur	k1gInSc1	silur
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
vápenopodolské	vápenopodolský	k2eAgFnSc2d1	vápenopodolský
synklinály	synklinála	k1gFnSc2	synklinála
ve	v	k7c6	v
facii	facie	k1gFnSc6	facie
tmavých	tmavý	k2eAgFnPc2d1	tmavá
fylitových	fylitový	k2eAgFnPc2d1	fylitový
břidlic	břidlice	k1gFnPc2	břidlice
(	(	kIx(	(
<g/>
s	s	k7c7	s
graptolity	graptolit	k1gMnPc7	graptolit
<g/>
,	,	kIx,	,
stáří	stáří	k1gNnSc3	stáří
llandovery-wenlock	llandoveryenlocka	k1gFnPc2	llandovery-wenlocka
<g/>
)	)	kIx)	)
a	a	k8xC	a
tmavých	tmavý	k2eAgInPc2d1	tmavý
grafitických	grafitický	k2eAgInPc2d1	grafitický
vápenců	vápenec	k1gInPc2	vápenec
s	s	k7c7	s
břidličnatými	břidličnatý	k2eAgFnPc7d1	břidličnatá
vložkami	vložka	k1gFnPc7	vložka
a	a	k8xC	a
pyritem	pyrit	k1gInSc7	pyrit
(	(	kIx(	(
<g/>
přídol	přídol	k1gInSc1	přídol
<g/>
,	,	kIx,	,
scyfokrinitová	scyfokrinitový	k2eAgFnSc1d1	scyfokrinitový
fauna	fauna	k1gFnSc1	fauna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hlinská	hlinský	k2eAgFnSc1d1	Hlinská
zóna	zóna	k1gFnSc1	zóna
-	-	kIx~	-
k	k	k7c3	k
siluru	silur	k1gInSc3	silur
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
mrákotínské	mrákotínský	k2eAgFnPc1d1	mrákotínská
vrstvy	vrstva	k1gFnPc1	vrstva
se	s	k7c7	s
slabě	slabě	k6eAd1	slabě
metamorfovanými	metamorfovaný	k2eAgFnPc7d1	metamorfovaná
graptolitovými	graptolitový	k2eAgFnPc7d1	graptolitový
břidlicemi	břidlice	k1gFnPc7	břidlice
(	(	kIx(	(
<g/>
llandovery-wenlock	llandoveryenlock	k1gInSc1	llandovery-wenlock
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
silurské	silurský	k2eAgFnPc1d1	Silurská
jsou	být	k5eAaImIp3nP	být
grafitické	grafitický	k2eAgFnPc1d1	grafitická
břidlice	břidlice	k1gFnPc1	břidlice
a	a	k8xC	a
lydity	lydit	k1gInPc1	lydit
u	u	k7c2	u
Poličky	Polička	k1gFnSc2	Polička
</s>
</p>
<p>
<s>
Drahanská	Drahanský	k2eAgFnSc1d1	Drahanská
vrchovina	vrchovina	k1gFnSc1	vrchovina
-	-	kIx~	-
při	při	k7c6	při
těžbě	těžba	k1gFnSc6	těžba
železných	železný	k2eAgFnPc2d1	železná
rud	ruda	k1gFnPc2	ruda
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
u	u	k7c2	u
Stínavy	Stínava	k1gFnSc2	Stínava
metamorfované	metamorfovaný	k2eAgFnSc2d1	metamorfovaná
graptolitové	graptolitový	k2eAgFnSc2d1	graptolitový
břidlice	břidlice	k1gFnSc2	břidlice
(	(	kIx(	(
<g/>
llandovery-wenlock	llandoveryenlock	k6eAd1	llandovery-wenlock
<g/>
)	)	kIx)	)
a	a	k8xC	a
šedé	šedý	k2eAgInPc1d1	šedý
vápence	vápenec	k1gInPc1	vápenec
s	s	k7c7	s
faunou	fauna	k1gFnSc7	fauna
ludlovu-přídolu	ludlovuřídol	k1gInSc2	ludlovu-přídol
<g/>
.	.	kIx.	.
</s>
<s>
Kra	kra	k1gFnSc1	kra
silurských	silurský	k2eAgFnPc2d1	Silurská
hornin	hornina	k1gFnPc2	hornina
je	být	k5eAaImIp3nS	být
zaklesnuta	zaklesnout	k5eAaPmNgFnS	zaklesnout
mezi	mezi	k7c4	mezi
devon	devon	k1gInSc4	devon
a	a	k8xC	a
spodní	spodní	k2eAgInSc4d1	spodní
karbon	karbon	k1gInSc4	karbon
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
podloží	podloží	k1gNnSc3	podloží
i	i	k8xC	i
nadloží	nadloží	k1gNnSc3	nadloží
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgMnSc1d1	nejasný
</s>
</p>
<p>
<s>
lužická	lužický	k2eAgFnSc1d1	Lužická
oblast	oblast	k1gFnSc1	oblast
-	-	kIx~	-
grafitické	grafitický	k2eAgFnSc2d1	grafitická
břidlice	břidlice	k1gFnSc2	břidlice
s	s	k7c7	s
vápenci	vápenec	k1gInPc7	vápenec
v	v	k7c6	v
nadloží	nadloží	k1gNnSc6	nadloží
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
krkonošsko-jizerského	krkonošskoizerský	k2eAgNnSc2d1	krkonošsko-jizerské
krystalinika	krystalinikum	k1gNnSc2	krystalinikum
-	-	kIx~	-
ponikelská	ponikelský	k2eAgFnSc1d1	ponikelská
skupina	skupina	k1gFnSc1	skupina
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
regionálně	regionálně	k6eAd1	regionálně
metamorfovány	metamorfován	k2eAgInPc4d1	metamorfován
<g/>
,	,	kIx,	,
paleontologické	paleontologický	k2eAgInPc4d1	paleontologický
doklady	doklad	k1gInPc4	doklad
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
(	(	kIx(	(
<g/>
Železný	železný	k2eAgInSc1d1	železný
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Poniklá	Poniklý	k2eAgFnSc1d1	Poniklá
-	-	kIx~	-
graptoliti	graptolit	k1gMnPc1	graptolit
<g/>
,	,	kIx,	,
Ještěd	Ještěd	k1gInSc1	Ještěd
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sasko-durynská	saskourynský	k2eAgFnSc1d1	sasko-durynský
oblast	oblast	k1gFnSc1	oblast
-	-	kIx~	-
na	na	k7c6	na
ordoviku	ordovik	k1gInSc6	ordovik
leží	ležet	k5eAaImIp3nP	ležet
konkordantně	konkordantně	k6eAd1	konkordantně
spodní	spodní	k2eAgFnPc1d1	spodní
graptolitové	graptolitový	k2eAgFnPc1d1	graptolitový
břidlice	břidlice	k1gFnPc1	břidlice
(	(	kIx(	(
<g/>
llandovery-ludlov	llandoveryudlov	k1gInSc1	llandovery-ludlov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přecházející	přecházející	k2eAgFnSc1d1	přecházející
do	do	k7c2	do
okrového	okrový	k2eAgInSc2d1	okrový
vápence	vápenec	k1gInSc2	vápenec
(	(	kIx(	(
<g/>
přídol	přídol	k1gInSc1	přídol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc1	vývoj
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
durynská	durynský	k2eAgFnSc1d1	Durynská
facie	facie	k1gFnSc1	facie
(	(	kIx(	(
<g/>
malá	malý	k2eAgFnSc1d1	malá
mocnost	mocnost	k1gFnSc1	mocnost
do	do	k7c2	do
50	[number]	k4	50
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
hlubokovodní	hlubokovodní	k2eAgMnSc1d1	hlubokovodní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
ČR	ČR	kA	ČR
nezasahuje	zasahovat	k5eNaImIp3nS	zasahovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
