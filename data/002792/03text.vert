<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Typlt	Typlt	k1gMnSc1	Typlt
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Jilemnice	Jilemnice	k1gFnSc1	Jilemnice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
žije	žít	k5eAaImIp3nS	žít
střídavě	střídavě	k6eAd1	střídavě
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
malířské	malířský	k2eAgNnSc1d1	malířské
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
příznačné	příznačný	k2eAgNnSc1d1	příznačné
svou	svůj	k3xOyFgFnSc7	svůj
formální	formální	k2eAgFnSc7d1	formální
syrovostí	syrovost	k1gFnSc7	syrovost
a	a	k8xC	a
obsahovou	obsahový	k2eAgFnSc7d1	obsahová
surovostí	surovost	k1gFnSc7	surovost
<g/>
.	.	kIx.	.
</s>
<s>
Rozvíjí	rozvíjet	k5eAaImIp3nP	rozvíjet
svůj	svůj	k3xOyFgInSc4	svůj
mýtus	mýtus	k1gInSc4	mýtus
o	o	k7c6	o
lidském	lidský	k2eAgNnSc6d1	lidské
jedinci	jedinec	k1gMnPc1	jedinec
pomocí	pomocí	k7c2	pomocí
malířských	malířský	k2eAgInPc2d1	malířský
prostředků	prostředek	k1gInPc2	prostředek
<g/>
:	:	kIx,	:
vytříbené	vytříbený	k2eAgFnPc1d1	vytříbená
kresebnosti	kresebnost	k1gFnPc1	kresebnost
i	i	k8xC	i
absurdních	absurdní	k2eAgInPc2d1	absurdní
prostorových	prostorový	k2eAgInPc2d1	prostorový
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
používá	používat	k5eAaImIp3nS	používat
techniku	technika	k1gFnSc4	technika
kvaše	kvaš	k1gFnSc2	kvaš
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
propojí	propojit	k5eAaPmIp3nS	propojit
výrazové	výrazový	k2eAgInPc4d1	výrazový
přechody	přechod	k1gInPc4	přechod
mezi	mezi	k7c7	mezi
kresbou	kresba	k1gFnSc7	kresba
a	a	k8xC	a
malbou	malba	k1gFnSc7	malba
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
spontánností	spontánnost	k1gFnSc7	spontánnost
<g/>
,	,	kIx,	,
gestickou	gestický	k2eAgFnSc7d1	gestická
živostí	živost	k1gFnSc7	živost
a	a	k8xC	a
formálních	formální	k2eAgFnPc2d1	formální
vytříbeností	vytříbenost	k1gFnPc2	vytříbenost
<g/>
.	.	kIx.	.
</s>
<s>
Příležitostně	příležitostně	k6eAd1	příležitostně
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
také	také	k9	také
hip	hip	k0	hip
hopové	hopová	k1gFnSc3	hopová
skupině	skupina	k1gFnSc3	skupina
WWW	WWW	kA	WWW
(	(	kIx(	(
<g/>
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
grafika	grafika	k1gFnSc1	grafika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
Výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
škola	škola	k1gFnSc1	škola
Václava	Václav	k1gMnSc2	Václav
Hollara	Hollar	k1gMnSc2	Hollar
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1	uměleckoprůmyslová
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
obor	obora	k1gFnPc2	obora
ilustrace	ilustrace	k1gFnSc2	ilustrace
<g/>
,	,	kIx,	,
prof.	prof.	kA	prof.
Jiří	Jiří	k1gMnSc1	Jiří
Šalamoun	Šalamoun	k1gMnSc1	Šalamoun
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2001	[number]	k4	2001
Fakulta	fakulta	k1gFnSc1	fakulta
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
obor	obor	k1gInSc4	obor
malířství	malířství	k1gNnSc2	malířství
<g/>
,	,	kIx,	,
prof.	prof.	kA	prof.
Jiří	Jiří	k1gMnSc1	Jiří
Načeradský	Načeradský	k2eAgMnSc1d1	Načeradský
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
Kunstakademie	Kunstakademie	k1gFnSc2	Kunstakademie
Düsseldorf	Düsseldorf	k1gMnSc1	Düsseldorf
obor	obor	k1gInSc4	obor
malířství	malířství	k1gNnSc2	malířství
<g/>
,	,	kIx,	,
prof.	prof.	kA	prof.
Markus	Markus	k1gInSc1	Markus
Lüpertz	Lüpertz	k1gInSc1	Lüpertz
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
Kunstakademie	Kunstakademie	k1gFnSc2	Kunstakademie
Düsseldorf	Düsseldorf	k1gMnSc1	Düsseldorf
obor	obor	k1gInSc4	obor
malířství	malířství	k1gNnSc2	malířství
<g/>
,	,	kIx,	,
prof.	prof.	kA	prof.
Gerhard	Gerhard	k1gInSc1	Gerhard
Merz	Merz	k1gInSc1	Merz
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
Kunstakademie	Kunstakademie	k1gFnSc1	Kunstakademie
Düsseldorf	Düsseldorf	k1gInSc4	Düsseldorf
obor	obor	k1gInSc1	obor
<g />
.	.	kIx.	.
</s>
<s>
malířství	malířství	k1gNnSc1	malířství
<g/>
,	,	kIx,	,
prof.	prof.	kA	prof.
A.	A.	kA	A.
R.	R.	kA	R.
Penck	Pencka	k1gFnPc2	Pencka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mistrovským	mistrovský	k2eAgMnSc7d1	mistrovský
žákem	žák	k1gMnSc7	žák
2004	[number]	k4	2004
Meisterschüler	Meisterschüler	k1gInSc1	Meisterschüler
2005	[number]	k4	2005
Akademiebrief	Akademiebrief	k1gInSc1	Akademiebrief
2012	[number]	k4	2012
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
-	-	kIx~	-
Galerie	galerie	k1gFnSc1	galerie
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Tikající	tikající	k2eAgMnSc1d1	tikající
muž	muž	k1gMnSc1	muž
<g/>
"	"	kIx"	"
2011	[number]	k4	2011
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Chodovská	chodovský	k2eAgFnSc1d1	Chodovská
vodní	vodní	k2eAgFnSc1d1	vodní
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
,	,	kIx,	,
,,	,,	k?	,,
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Holky	holka	k1gFnPc1	holka
<g/>
"	"	kIx"	"
2011	[number]	k4	2011
Beroun	Beroun	k1gInSc1	Beroun
<g/>
,	,	kIx,	,
Městská	městský	k2eAgFnSc1d1	městská
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
,,	,,	k?	,,
<g/>
Nepřišel	přijít	k5eNaPmAgMnS	přijít
jsem	být	k5eAaImIp1nS	být
k	k	k7c3	k
vám	vy	k3xPp2nPc3	vy
na	na	k7c4	na
zahradu	zahrada	k1gFnSc4	zahrada
pro	pro	k7c4	pro
kytky	kytka	k1gFnPc4	kytka
<g/>
"	"	kIx"	"
2011	[number]	k4	2011
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc2	umění
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Mobilizace	mobilizace	k1gFnSc1	mobilizace
<g/>
"	"	kIx"	"
2011	[number]	k4	2011
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Oblastní	oblastní	k2eAgFnSc1d1	oblastní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Kdo	kdo	k3yInSc1	kdo
bydlí	bydlet	k5eAaImIp3nS	bydlet
v	v	k7c6	v
srdečních	srdeční	k2eAgFnPc6d1	srdeční
komorách	komora	k1gFnPc6	komora
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
2010	[number]	k4	2010
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Sklenář	Sklenář	k1gMnSc1	Sklenář
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
Typlt	Typlt	k1gMnSc1	Typlt
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc4	ten
Typlt	Typlt	k1gMnSc1	Typlt
<g/>
"	"	kIx"	"
2010	[number]	k4	2010
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Fons	Fons	k1gInSc1	Fons
2010	[number]	k4	2010
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Bárka	bárka	k1gFnSc1	bárka
kafe	kafe	k?	kafe
2010	[number]	k4	2010
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
AM	AM	kA	AM
180	[number]	k4	180
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
L.	L.	kA	L.
Gažiovou	Gažiový	k2eAgFnSc7d1	Gažiová
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
2010	[number]	k4	2010
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
VŠUP	VŠUP	kA	VŠUP
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
207	[number]	k4	207
2009	[number]	k4	2009
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Sokolská	sokolská	k1gFnSc1	sokolská
26	[number]	k4	26
2009	[number]	k4	2009
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Dole	dol	k1gInSc6	dol
2009	[number]	k4	2009
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Vernon	Vernona	k1gFnPc2	Vernona
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Turbína	turbína	k1gFnSc1	turbína
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
"	"	kIx"	"
2009	[number]	k4	2009
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
ARS	ARS	kA	ARS
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
může	moct	k5eAaImIp3nS	moct
smát	smát	k5eAaImF	smát
jenom	jenom	k9	jenom
cizinec	cizinec	k1gMnSc1	cizinec
<g/>
"	"	kIx"	"
2009	[number]	k4	2009
Maastricht	Maastricht	k1gInSc1	Maastricht
(	(	kIx(	(
<g/>
NL	NL	kA	NL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Traders	Traders	k1gInSc1	Traders
Pop	pop	k1gInSc1	pop
Gallery	Galler	k1gMnPc7	Galler
2007	[number]	k4	2007
ARD-Hauptstadtstudio	ARD-Hauptstadtstudio	k1gNnSc1	ARD-Hauptstadtstudio
Berlin	berlina	k1gFnPc2	berlina
2007	[number]	k4	2007
Tease	Teas	k1gMnSc2	Teas
Art	Art	k1gFnSc4	Art
Fair	fair	k2eAgMnSc2d1	fair
-	-	kIx~	-
April	April	k1gInSc1	April
19	[number]	k4	19
-	-	kIx~	-
22	[number]	k4	22
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
in	in	k?	in
Cologne	Cologn	k1gInSc5	Cologn
2006	[number]	k4	2006
Urychlovat	urychlovat	k5eAaImF	urychlovat
nekonečno	nekonečno	k1gNnSc1	nekonečno
<g/>
,	,	kIx,	,
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
radnice	radnice	k1gFnSc1	radnice
Galerie	galerie	k1gFnSc1	galerie
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
<g />
.	.	kIx.	.
</s>
<s>
Traders	Traders	k6eAd1	Traders
Pop	pop	k1gInSc1	pop
Gallery	Galler	k1gInPc1	Galler
Maastricht	Maastricht	k1gInSc1	Maastricht
Cha	cha	k0	cha
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
<g/>
Cha	cha	k0	cha
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
Cha	cha	k0	cha
<g/>
...	...	k?	...
/	/	kIx~	/
<g/>
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
/	/	kIx~	/
2006	[number]	k4	2006
Sucharduv	Sucharduvum	k1gNnPc2	Sucharduvum
dum	duma	k1gFnPc2	duma
<g/>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
Paka	Paka	k1gFnSc1	Paka
(	(	kIx(	(
<g/>
CZ	CZ	kA	CZ
<g/>
)	)	kIx)	)
K.	K.	kA	K.
s	s	k7c7	s
kockou	kocka	k1gFnSc7	kocka
2005	[number]	k4	2005
Galerie	galerie	k1gFnSc1	galerie
ARS	ARS	kA	ARS
Brno	Brno	k1gNnSc1	Brno
Europäischer	Europäischra	k1gFnPc2	Europäischra
Kunsthof	Kunsthof	k1gMnSc1	Kunsthof
Vicht	Vicht	k1gMnSc1	Vicht
2004	[number]	k4	2004
Traders	Traders	k1gInSc1	Traders
Pop	pop	k1gInSc1	pop
Gallery	Galler	k1gInPc1	Galler
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Maastricht	Maastricht	k1gInSc1	Maastricht
<g/>
,	,	kIx,	,
Holandsko	Holandsko	k1gNnSc1	Holandsko
2003	[number]	k4	2003
Europäischer	Europäischra	k1gFnPc2	Europäischra
Kunsthof	Kunsthof	k1gMnSc1	Kunsthof
Vicht	Vicht	k1gMnSc1	Vicht
<g/>
,	,	kIx,	,
Stolberg-Vicht	Stolberg-Vicht	k1gMnSc1	Stolberg-Vicht
2002	[number]	k4	2002
Traders	Traders	k1gInSc1	Traders
Pop	pop	k1gInSc4	pop
Gallery	Galler	k1gInPc7	Galler
<g/>
,	,	kIx,	,
Maastricht	Maastricht	k1gInSc1	Maastricht
<g/>
,	,	kIx,	,
Holandsko	Holandsko	k1gNnSc1	Holandsko
2001	[number]	k4	2001
Galerie	galerie	k1gFnSc1	galerie
Václava	Václav	k1gMnSc2	Václav
Špály	Špála	k1gMnSc2	Špála
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Galerie	galerie	k1gFnSc1	galerie
Pecka	pecka	k1gFnSc1	pecka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc2	umění
města	město	k1gNnSc2	město
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
Českomoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
stavební	stavební	k2eAgFnSc1d1	stavební
spořitelna	spořitelna	k1gFnSc1	spořitelna
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
Europäischer	Europäischra	k1gFnPc2	Europäischra
Kunsthof	Kunsthof	k1gInSc4	Kunsthof
<g />
.	.	kIx.	.
</s>
<s>
Vicht	Vicht	k1gMnSc1	Vicht
Stolberg-Vicht	Stolberg-Vicht	k1gMnSc1	Stolberg-Vicht
<g/>
,	,	kIx,	,
<g/>
NSR	NSR	kA	NSR
1999	[number]	k4	1999
Galerie	galerie	k1gFnSc2	galerie
mladých	mladý	k2eAgFnPc2d1	mladá
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
Hrádek	hrádek	k1gInSc1	hrádek
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
Galerie	galerie	k1gFnSc2	galerie
Aspekt	aspekt	k1gInSc1	aspekt
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1998	[number]	k4	1998
Minikino	minikino	k1gNnSc1	minikino
Kavárna	kavárna	k1gFnSc1	kavárna
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1997	[number]	k4	1997
Městské	městský	k2eAgNnSc1d1	Městské
kulturní	kulturní	k2eAgNnSc1d1	kulturní
středisko	středisko	k1gNnSc1	středisko
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Paka	Paka	k1gFnSc1	Paka
1995	[number]	k4	1995
Junior	junior	k1gMnSc1	junior
Club	club	k1gInSc4	club
Chmelnice	chmelnice	k1gFnSc2	chmelnice
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2011	[number]	k4	2011
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Galéria	Galérium	k1gNnSc2	Galérium
19	[number]	k4	19
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Punctum	punctum	k1gNnSc1	punctum
2011	[number]	k4	2011
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Sklenář	Sklenář	k1gMnSc1	Sklenář
<g/>
,	,	kIx,	,
,,	,,	k?	,,
<g/>
Lüpertz	Lüpertz	k1gMnSc1	Lüpertz
<g/>
,	,	kIx,	,
Penck	Penck	k1gMnSc1	Penck
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc1	jejich
žák	žák	k1gMnSc1	žák
Lubomír	Lubomír	k1gMnSc1	Lubomír
Typlt	Typlt	k1gMnSc1	Typlt
<g/>
"	"	kIx"	"
2011	[number]	k4	2011
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
-	-	kIx~	-
Galerie	galerie	k1gFnSc1	galerie
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Fundamenty	fundament	k1gInPc4	fundament
&	&	k?	&
sedimenty	sediment	k1gInPc4	sediment
<g/>
"	"	kIx"	"
2009	[number]	k4	2009
-	-	kIx~	-
2010	[number]	k4	2010
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Topičův	topičův	k2eAgInSc1d1	topičův
salón	salón	k1gInSc1	salón
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Černobílé	černobílý	k2eAgNnSc4d1	černobílé
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
město	město	k1gNnSc4	město
<g/>
"	"	kIx"	"
2009	[number]	k4	2009
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Dolmen	dolmen	k1gInSc1	dolmen
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Načeradského	Načeradský	k2eAgInSc2d1	Načeradský
jedenáctka	jedenáctka	k1gFnSc1	jedenáctka
<g/>
"	"	kIx"	"
2009	[number]	k4	2009
Berlín	Berlín	k1gInSc1	Berlín
(	(	kIx(	(
<g/>
SRN	SRN	kA	SRN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Berliner	Berliner	k1gInSc1	Berliner
Kunstsalon	Kunstsalon	k1gInSc1	Kunstsalon
<g/>
"	"	kIx"	"
2009	[number]	k4	2009
Edsvik	Edsvika	k1gFnPc2	Edsvika
Konsthall	Konsthalla	k1gFnPc2	Konsthalla
Stockholm	Stockholm	k1gInSc1	Stockholm
-	-	kIx~	-
Švédsko	Švédsko	k1gNnSc1	Švédsko
2008	[number]	k4	2008
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
BERLINER	BERLINER	kA	BERLINER
KUNSTSALON	KUNSTSALON	kA	KUNSTSALON
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
<g/>
,	,	kIx,	,
Germany	German	k1gInPc1	German
(	(	kIx(	(
Art	Art	k1gFnSc1	Art
Fair	fair	k2eAgFnSc1d1	fair
<g/>
/	/	kIx~	/
Kunstmesse	Kunstmesse	k1gFnSc1	Kunstmesse
/	/	kIx~	/
Katalog	katalog	k1gInSc1	katalog
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
Tease	Teas	k1gMnSc2	Teas
Art	Art	k1gMnSc2	Art
Fair	fair	k6eAd1	fair
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
Cologne	Cologn	k1gMnSc5	Cologn
<g/>
,	,	kIx,	,
Germany	German	k1gInPc1	German
(	(	kIx(	(
<g/>
Köln	Köln	k1gInSc1	Köln
/	/	kIx~	/
Art	Art	k1gFnSc1	Art
FairKunstmesse	FairKunstmesse	k1gFnSc1	FairKunstmesse
/	/	kIx~	/
Katalog	katalog	k1gInSc1	katalog
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
BERLINER	BERLINER	kA	BERLINER
KUNSTSALON	KUNSTSALON	kA	KUNSTSALON
28.09	[number]	k4	28.09
<g/>
-	-	kIx~	-
<g/>
2.10	[number]	k4	2.10
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
2007	[number]	k4	2007
Kunst-	Kunst-	k1gMnSc1	Kunst-
und	und	k?	und
Gewerbeverein	Gewerbeverein	k1gMnSc1	Gewerbeverein
Regensburg	Regensburg	k1gMnSc1	Regensburg
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
Vychodočeská	Vychodočeský	k2eAgFnSc1d1	Vychodočeský
galerie	galerie	k1gFnSc1	galerie
-	-	kIx~	-
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
Pardubice	Pardubice	k1gInPc1	Pardubice
(	(	kIx(	(
<g/>
CZ	CZ	kA	CZ
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
Gallerie	Gallerie	k1gFnSc2	Gallerie
Šternberk	Šternberk	k1gInSc1	Šternberk
(	(	kIx(	(
<g/>
CZ	CZ	kA	CZ
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
Galerie	galerie	k1gFnSc1	galerie
Klatovy	Klatovy	k1gInPc1	Klatovy
Klenova	Klenov	k1gInSc2	Klenov
(	(	kIx(	(
<g/>
CZ	CZ	kA	CZ
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
2007	[number]	k4	2007
Kunststation	Kunststation	k1gInSc1	Kunststation
Kleinsassen	Kleinsassen	k1gInSc4	Kleinsassen
<g/>
/	/	kIx~	/
Fulda	Fuldo	k1gNnSc2	Fuldo
-	-	kIx~	-
Contemporary	Contemporara	k1gFnSc2	Contemporara
Czech	Czech	k1gMnSc1	Czech
Art	Art	k1gMnSc1	Art
-	-	kIx~	-
Group	Group	k1gMnSc1	Group
Exhibition	Exhibition	k1gInSc4	Exhibition
2006	[number]	k4	2006
Galerie	galerie	k1gFnSc2	galerie
Michael	Michaela	k1gFnPc2	Michaela
Schultz	Schultz	k1gMnSc1	Schultz
Contemporary	Contemporara	k1gFnSc2	Contemporara
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
Germany	German	k1gInPc1	German
2005	[number]	k4	2005
Galerie	galerie	k1gFnSc2	galerie
Bleibtreu	Bleibtreus	k1gInSc2	Bleibtreus
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
2005	[number]	k4	2005
Lehmann	Lehmanna	k1gFnPc2	Lehmanna
<g/>
,	,	kIx,	,
Petrbok	Petrbok	k1gInSc1	Petrbok
<g/>
,	,	kIx,	,
Typlt	Typlt	k1gInSc1	Typlt
-	-	kIx~	-
Happy	Happa	k1gFnSc2	Happa
Days	Days	k1gInSc1	Days
<g/>
,	,	kIx,	,
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
Nosticův	Nosticův	k2eAgInSc1d1	Nosticův
Palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g />
.	.	kIx.	.
</s>
<s>
2005	[number]	k4	2005
Privání	Privání	k1gNnSc1	Privání
pohled	pohled	k1gInSc1	pohled
(	(	kIx(	(
<g/>
sbírka	sbírka	k1gFnSc1	sbírka
Josefa	Josef	k1gMnSc2	Josef
Chloupka	Chloupek	k1gMnSc2	Chloupek
<g/>
)	)	kIx)	)
Dům	dům	k1gInSc1	dům
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
<	<	kIx(	<
2005	[number]	k4	2005
Stopy	stopa	k1gFnPc4	stopa
zápasu	zápas	k1gInSc2	zápas
-	-	kIx~	-
Městská	městský	k2eAgFnSc1d1	městská
galerie	galerie	k1gFnSc1	galerie
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
2005	[number]	k4	2005
Intercity	Intercit	k1gInPc1	Intercit
Berlin	berlina	k1gFnPc2	berlina
-	-	kIx~	-
Prague	Prague	k1gFnPc2	Prague
<g/>
,	,	kIx,	,
Haus	Hausa	k1gFnPc2	Hausa
am	am	k?	am
Waldsee	Waldsee	k1gFnPc2	Waldsee
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
2004	[number]	k4	2004
Intercity	Intercit	k1gInPc1	Intercit
Berlin	berlina	k1gFnPc2	berlina
-	-	kIx~	-
Prague	Prague	k1gFnSc1	Prague
<g/>
,	,	kIx,	,
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Prague	Pragu	k1gInPc1	Pragu
<g/>
,	,	kIx,	,
Gallery	Galler	k1gInPc1	Galler
Bleibtreu	Bleibtreus	k1gInSc2	Bleibtreus
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
2003-04	[number]	k4	2003-04
FaVU	FaVU	k1gFnPc2	FaVU
-	-	kIx~	-
současná	současný	k2eAgFnSc1d1	současná
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
,	,	kIx,	,
Špálova	Špálův	k2eAgFnSc1d1	Špálova
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Perfect	Perfecta	k1gFnPc2	Perfecta
Tense	tense	k1gFnSc1	tense
Malba	malba	k1gFnSc1	malba
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
Jízdarna	Jízdarna	k1gFnSc1	Jízdarna
Pražského	pražský	k2eAgInSc2d1	pražský
Hradu	hrad	k1gInSc2	hrad
2003	[number]	k4	2003
Objekt	objekt	k1gInSc1	objekt
&	&	k?	&
skulptura	skulptura	k1gFnSc1	skulptura
<g/>
,	,	kIx,	,
Kunsthaus	Kunsthaus	k1gMnSc1	Kunsthaus
Oberkassel	Oberkassel	k1gMnSc1	Oberkassel
<g/>
,	,	kIx,	,
Düsseldorf	Düsseldorf	k1gMnSc1	Düsseldorf
<g/>
,	,	kIx,	,
NSR	NSR	kA	NSR
Objekt	objekt	k1gInSc1	objekt
&	&	k?	&
skulptura	skulptura	k1gFnSc1	skulptura
<g/>
,	,	kIx,	,
Kunstverein	Kunstverein	k1gMnSc1	Kunstverein
Kreis	Kreis	k1gFnSc2	Kreis
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
,	,	kIx,	,
e.V.	e.V.	k?	e.V.
NSR	NSR	kA	NSR
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Zlínský	zlínský	k2eAgInSc4d1	zlínský
salón	salón	k1gInSc4	salón
mladých	mladý	k1gMnPc2	mladý
Zlín	Zlín	k1gInSc1	Zlín
Cz	Cz	k1gFnPc2	Cz
FaVU	FaVU	k1gFnPc2	FaVU
-	-	kIx~	-
současná	současný	k2eAgFnSc1d1	současná
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2000	[number]	k4	2000
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Zlínský	zlínský	k2eAgInSc4d1	zlínský
salón	salón	k1gInSc4	salón
mladých	mladý	k1gMnPc2	mladý
<g/>
,	,	kIx,	,
Zlín	Zlín	k1gInSc1	Zlín
Klasika	klasika	k1gFnSc1	klasika
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
Klasika	klasika	k1gFnSc1	klasika
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Památkový	památkový	k2eAgInSc1d1	památkový
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
Rechtsanwälte	Rechtsanwält	k1gInSc5	Rechtsanwält
Franz	Franz	k1gMnSc1	Franz
Jacobi	Jacob	k1gFnSc3	Jacob
Str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Düsseldorf	Düsseldorf	k1gMnSc1	Düsseldorf
<g/>
,	,	kIx,	,
NSR	NSR	kA	NSR
1999	[number]	k4	1999
FaVU	FaVU	k1gFnPc1	FaVU
-	-	kIx~	-
současná	současný	k2eAgFnSc1d1	současná
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
Studenti	student	k1gMnPc1	student
AVU	AVU	kA	AVU
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
VŠUP	VŠUP	kA	VŠUP
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
FaVU	FaVU	k1gFnSc1	FaVU
Brno	Brno	k1gNnSc1	Brno
1998	[number]	k4	1998
3	[number]	k4	3
<g/>
x	x	k?	x
<g/>
3	[number]	k4	3
Pražákův	Pražákův	k2eAgInSc1d1	Pražákův
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
Banka	banka	k1gFnSc1	banka
Haná	Haná	k1gFnSc1	Haná
<g/>
,	,	kIx,	,
Besední	besední	k2eAgInSc1d1	besední
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
</s>
