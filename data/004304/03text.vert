<s>
Národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Pravčická	Pravčická	k1gFnSc1	Pravčická
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Prebischtor	Prebischtor	k1gInSc1	Prebischtor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skalní	skalní	k2eAgFnSc1d1	skalní
brána	brána	k1gFnSc1	brána
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
v	v	k7c6	v
kvádrových	kvádrový	k2eAgInPc6d1	kvádrový
pískovcích	pískovec	k1gInPc6	pískovec
křídového	křídový	k2eAgNnSc2d1	křídové
stáří	stáří	k1gNnSc2	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
(	(	kIx(	(
<g/>
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
v	v	k7c6	v
první	první	k4xOgFnSc6	první
zóně	zóna	k1gFnSc6	zóna
NP	NP	kA	NP
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Hřensko	Hřensko	k1gNnSc1	Hřensko
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Děčín	Děčín	k1gInSc1	Děčín
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozpětím	rozpětí	k1gNnSc7	rozpětí
26,5	[number]	k4	26,5
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
výškou	výška	k1gFnSc7	výška
16	[number]	k4	16
metrů	metr	k1gInPc2	metr
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
pískovcovou	pískovcový	k2eAgFnSc4d1	pískovcová
skalní	skalní	k2eAgFnSc4d1	skalní
bránu	brána	k1gFnSc4	brána
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejnavštěvovanějších	navštěvovaný	k2eAgNnPc2d3	nejnavštěvovanější
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
symbolem	symbol	k1gInSc7	symbol
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
jehož	jehož	k3xOyRp3gFnSc4	jehož
správu	správa	k1gFnSc4	správa
patří	patřit	k5eAaImIp3nS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Chráněné	chráněný	k2eAgNnSc1d1	chráněné
území	území	k1gNnSc1	území
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
nejen	nejen	k6eAd1	nejen
samotnou	samotný	k2eAgFnSc4d1	samotná
pískovcovou	pískovcový	k2eAgFnSc4d1	pískovcová
bránu	brána	k1gFnSc4	brána
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
okolní	okolní	k2eAgInPc1d1	okolní
skalní	skalní	k2eAgInPc1d1	skalní
masivy	masiv	k1gInPc1	masiv
a	a	k8xC	a
z	z	k7c2	z
části	část	k1gFnSc2	část
zastavěnou	zastavěný	k2eAgFnSc4d1	zastavěná
plochu	plocha	k1gFnSc4	plocha
(	(	kIx(	(
<g/>
zámeček	zámeček	k1gInSc4	zámeček
Sokolí	sokolí	k2eAgNnSc1d1	sokolí
hnízdo	hnízdo	k1gNnSc1	hnízdo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
náleží	náležet	k5eAaImIp3nS	náležet
kromě	kromě	k7c2	kromě
NP	NP	kA	NP
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
také	také	k9	také
k	k	k7c3	k
Ptačí	ptačí	k2eAgFnSc3d1	ptačí
oblasti	oblast	k1gFnSc3	oblast
Labské	labský	k2eAgFnSc6d1	Labská
pískovce	pískovka	k1gFnSc6	pískovka
<g/>
.	.	kIx.	.
</s>
<s>
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
navštěvována	navštěvovat	k5eAaImNgFnS	navštěvovat
již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Četná	četný	k2eAgFnSc1d1	četná
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
způsobovala	způsobovat	k5eAaImAgFnS	způsobovat
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
erozi	eroze	k1gFnSc4	eroze
vrchních	vrchní	k2eAgFnPc2d1	vrchní
partií	partie	k1gFnPc2	partie
a	a	k8xC	a
destrukci	destrukce	k1gFnSc4	destrukce
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zjištění	zjištění	k1gNnSc6	zjištění
několika	několik	k4yIc2	několik
trhlin	trhlina	k1gFnPc2	trhlina
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
vstup	vstup	k1gInSc4	vstup
turistů	turist	k1gMnPc2	turist
na	na	k7c4	na
bránu	brána	k1gFnSc4	brána
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
chráněna	chránit	k5eAaImNgFnS	chránit
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
usnesením	usnesení	k1gNnSc7	usnesení
Okresního	okresní	k2eAgInSc2d1	okresní
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
Děčín	Děčín	k1gInSc1	Děčín
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Chráněným	chráněný	k2eAgInSc7d1	chráněný
přírodním	přírodní	k2eAgInSc7d1	přírodní
výtvorem	výtvor	k1gInSc7	výtvor
(	(	kIx(	(
<g/>
CHPV	CHPV	kA	CHPV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
vyhláškou	vyhláška	k1gFnSc7	vyhláška
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
překlasifikována	překlasifikován	k2eAgFnSc1d1	překlasifikován
na	na	k7c4	na
Národní	národní	k2eAgFnSc4d1	národní
přírodní	přírodní	k2eAgFnSc4d1	přírodní
památku	památka	k1gFnSc4	památka
(	(	kIx(	(
<g/>
NPP	NPP	kA	NPP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
zachování	zachování	k1gNnSc4	zachování
současného	současný	k2eAgInSc2d1	současný
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
přirozeného	přirozený	k2eAgInSc2d1	přirozený
vývoje	vývoj	k1gInSc2	vývoj
pískovcových	pískovcový	k2eAgInPc2d1	pískovcový
skalních	skalní	k2eAgInPc2d1	skalní
masivů	masiv	k1gInPc2	masiv
a	a	k8xC	a
minimalizace	minimalizace	k1gFnSc1	minimalizace
negativního	negativní	k2eAgNnSc2d1	negativní
antropogenního	antropogenní	k2eAgNnSc2d1	antropogenní
působení	působení	k1gNnSc2	působení
<g/>
.	.	kIx.	.
</s>
<s>
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
užšího	úzký	k2eAgNnSc2d2	užší
kola	kolo	k1gNnSc2	kolo
ankety	anketa	k1gFnSc2	anketa
New	New	k1gFnSc2	New
<g/>
7	[number]	k4	7
<g/>
Wonders	Wondersa	k1gFnPc2	Wondersa
o	o	k7c4	o
sedm	sedm	k4xCc4	sedm
přírodních	přírodní	k2eAgInPc2d1	přírodní
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
když	když	k8xS	když
postoupila	postoupit	k5eAaPmAgFnS	postoupit
mezi	mezi	k7c4	mezi
71	[number]	k4	71
semifinalistů	semifinalista	k1gMnPc2	semifinalista
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
užšího	úzký	k2eAgInSc2d2	užší
výběru	výběr	k1gInSc2	výběr
28	[number]	k4	28
finalistů	finalista	k1gMnPc2	finalista
se	se	k3xPyFc4	se
ale	ale	k9	ale
již	již	k6eAd1	již
neprobojovala	probojovat	k5eNaPmAgFnS	probojovat
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
je	být	k5eAaImIp3nS	být
odvozen	odvozen	k2eAgMnSc1d1	odvozen
jejího	její	k3xOp3gInSc2	její
německého	německý	k2eAgInSc2d1	německý
názvu	název	k1gInSc2	název
Prebischtor	Prebischtor	k1gInSc1	Prebischtor
(	(	kIx(	(
<g/>
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
Tor	Tor	k1gMnSc1	Tor
jako	jako	k8xS	jako
brána	brána	k1gFnSc1	brána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
obyvatelé	obyvatel	k1gMnPc1	obyvatel
oblasti	oblast	k1gFnSc2	oblast
nazývali	nazývat	k5eAaImAgMnP	nazývat
bránu	brána	k1gFnSc4	brána
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnPc2	jejich
vlastností	vlastnost	k1gFnPc2	vlastnost
a	a	k8xC	a
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Zažitý	zažitý	k2eAgInSc1d1	zažitý
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
brána	brána	k1gFnSc1	brána
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
"	"	kIx"	"
<g/>
Grosses	Grosses	k1gMnSc1	Grosses
Tor	Tor	k1gMnSc1	Tor
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dokumentováno	dokumentovat	k5eAaBmNgNnS	dokumentovat
i	i	k9	i
v	v	k7c6	v
první	první	k4xOgFnSc6	první
písemné	písemný	k2eAgFnSc6d1	písemná
zmínce	zmínka	k1gFnSc6	zmínka
o	o	k7c6	o
bráně	brána	k1gFnSc6	brána
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1410	[number]	k4	1410
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
seznam	seznam	k1gInSc1	seznam
pomístních	pomístní	k2eAgNnPc2d1	pomístní
jmen	jméno	k1gNnPc2	jméno
obsažený	obsažený	k2eAgInSc4d1	obsažený
v	v	k7c6	v
urbáři	urbář	k1gInSc6	urbář
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1722	[number]	k4	1722
uvádí	uvádět	k5eAaImIp3nS	uvádět
pouze	pouze	k6eAd1	pouze
prosté	prostý	k2eAgNnSc1d1	prosté
"	"	kIx"	"
<g/>
Das	Das	k1gFnSc1	Das
Thor	Thor	k1gMnSc1	Thor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
až	až	k9	až
v	v	k7c6	v
turistické	turistický	k2eAgFnSc6d1	turistická
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
začalo	začít	k5eAaPmAgNnS	začít
objevovat	objevovat	k5eAaImF	objevovat
pojmenování	pojmenování	k1gNnSc1	pojmenování
"	"	kIx"	"
<g/>
Prebischtor	Prebischtor	k1gInSc1	Prebischtor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
převzato	převzít	k5eAaPmNgNnS	převzít
z	z	k7c2	z
názvu	název	k1gInSc2	název
nejbližších	blízký	k2eAgInPc2d3	Nejbližší
terénních	terénní	k2eAgInPc2d1	terénní
útvarů	útvar	k1gInPc2	útvar
"	"	kIx"	"
<g/>
Prebischgrund	Prebischgrund	k1gInSc1	Prebischgrund
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Prebischkegel	Prebischkegel	k1gInSc1	Prebischkegel
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Prebischhorn	Prebischhorn	k1gNnSc1	Prebischhorn
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
uváděných	uváděný	k2eAgFnPc2d1	uváděná
již	již	k6eAd1	již
v	v	k7c6	v
popisech	popis	k1gInPc6	popis
hranic	hranice	k1gFnPc2	hranice
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
ve	v	k7c6	v
smlouvách	smlouva	k1gFnPc6	smlouva
o	o	k7c6	o
dělení	dělení	k1gNnSc6	dělení
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
označení	označení	k1gNnPc1	označení
pocházejí	pocházet	k5eAaImIp3nP	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
obyvatelského	obyvatelský	k2eAgNnSc2d1	obyvatelské
jména	jméno	k1gNnSc2	jméno
Prebisch	Prebischa	k1gFnPc2	Prebischa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
kdysi	kdysi	k6eAd1	kdysi
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
turistiky	turistika	k1gFnSc2	turistika
postupně	postupně	k6eAd1	postupně
přibývali	přibývat	k5eAaImAgMnP	přibývat
i	i	k9	i
čeští	český	k2eAgMnPc1d1	český
návštěvníci	návštěvník	k1gMnPc1	návštěvník
z	z	k7c2	z
česky	česky	k6eAd1	česky
mluvícího	mluvící	k2eAgNnSc2d1	mluvící
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
tvar	tvar	k1gInSc1	tvar
slova	slovo	k1gNnSc2	slovo
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
připodobněn	připodobnit	k5eAaPmNgInS	připodobnit
podobnému	podobný	k2eAgNnSc3d1	podobné
slovu	slovo	k1gNnSc3	slovo
českému	český	k2eAgMnSc3d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Dělo	dít	k5eAaBmAgNnS	dít
se	se	k3xPyFc4	se
tak	tak	k9	tak
zprvu	zprvu	k6eAd1	zprvu
nejednotně	jednotně	k6eNd1	jednotně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
i	i	k9	i
rozličnost	rozličnost	k1gFnSc1	rozličnost
těchto	tento	k3xDgInPc2	tento
názvů	název	k1gInPc2	název
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
Přebišova	Přebišův	k2eAgFnSc1d1	Přebišův
<g/>
,	,	kIx,	,
Přebišská	Přebišský	k2eAgFnSc1d1	Přebišský
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
brána	brána	k1gFnSc1	brána
Převýšovská	Převýšovský	k2eAgFnSc1d1	Převýšovský
a	a	k8xC	a
Převyšovská	Převyšovský	k2eAgFnSc1d1	Převyšovský
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ustálil	ustálit	k5eAaPmAgMnS	ustálit
současný	současný	k2eAgInSc4d1	současný
tvar	tvar	k1gInSc4	tvar
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
však	však	k9	však
nijak	nijak	k6eAd1	nijak
nesouvisí	souviset	k5eNaImIp3nS	souviset
s	s	k7c7	s
názvem	název	k1gInSc7	název
obce	obec	k1gFnSc2	obec
Pravčice	Pravčice	k1gFnSc2	Pravčice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Kroměříž	Kroměříž	k1gFnSc4	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
legend	legenda	k1gFnPc2	legenda
a	a	k8xC	a
příkladů	příklad	k1gInPc2	příklad
lidové	lidový	k2eAgFnSc2d1	lidová
etymologie	etymologie	k1gFnSc2	etymologie
vysvětlujících	vysvětlující	k2eAgFnPc2d1	vysvětlující
původ	původ	k1gInSc4	původ
názvu	název	k1gInSc2	název
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
poustevníkovi	poustevník	k1gMnSc6	poustevník
Prebischovi	Prebisch	k1gMnSc6	Prebisch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tu	ten	k3xDgFnSc4	ten
údajně	údajně	k6eAd1	údajně
sídlil	sídlit	k5eAaImAgInS	sídlit
a	a	k8xC	a
dal	dát	k5eAaPmAgInS	dát
bráně	brána	k1gFnSc6	brána
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
nepravděpodobný	pravděpodobný	k2eNgMnSc1d1	nepravděpodobný
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
i	i	k9	i
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
brána	brána	k1gFnSc1	brána
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
od	od	k7c2	od
slovanského	slovanský	k2eAgInSc2d1	slovanský
výrazu	výraz	k1gInSc2	výraz
pro	pro	k7c4	pro
bránu	brána	k1gFnSc4	brána
či	či	k8xC	či
převis	převis	k1gInSc4	převis
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgNnSc1d1	německé
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Prebischtor	Prebischtor	k1gInSc1	Prebischtor
<g/>
"	"	kIx"	"
by	by	kYmCp3nP	by
tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
českého	český	k2eAgNnSc2d1	české
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
převis	převis	k1gInSc1	převis
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
zóně	zóna	k1gFnSc6	zóna
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Hřensko	Hřensko	k1gNnSc1	Hřensko
a	a	k8xC	a
2	[number]	k4	2
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Mezní	mezní	k2eAgFnSc1d1	mezní
Louka	louka	k1gFnSc1	louka
<g/>
.	.	kIx.	.
</s>
<s>
Geomorfologicky	geomorfologicky	k6eAd1	geomorfologicky
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
Děčínské	děčínský	k2eAgFnSc3d1	Děčínská
vrchovině	vrchovina	k1gFnSc3	vrchovina
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
k	k	k7c3	k
podcelku	podcelek	k1gInSc3	podcelek
Jetřichovické	Jetřichovický	k2eAgFnSc2d1	Jetřichovická
stěny	stěna	k1gFnSc2	stěna
a	a	k8xC	a
nalézá	nalézat	k5eAaImIp3nS	nalézat
se	se	k3xPyFc4	se
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
447	[number]	k4	447
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
skalního	skalní	k2eAgNnSc2d1	skalní
defilé	defilé	k1gNnSc2	defilé
Křídelní	křídelní	k2eAgFnSc2d1	křídelní
stěny	stěna	k1gFnSc2	stěna
a	a	k8xC	a
ze	z	k7c2	z
skalní	skalní	k2eAgFnSc2d1	skalní
plošiny	plošina	k1gFnSc2	plošina
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
šikmo	šikmo	k6eAd1	šikmo
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
SV-JZ	SV-JZ	k1gFnSc2	SV-JZ
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
pískovcovou	pískovcový	k2eAgFnSc4d1	pískovcová
skalní	skalní	k2eAgFnSc4d1	skalní
bránu	brána	k1gFnSc4	brána
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
Pravčické	Pravčický	k2eAgFnSc2d1	Pravčická
brány	brána	k1gFnSc2	brána
je	být	k5eAaImIp3nS	být
26,5	[number]	k4	26,5
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
16	[number]	k4	16
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgFnSc1d3	nejmenší
tloušťka	tloušťka	k1gFnSc1	tloušťka
klenby	klenba	k1gFnSc2	klenba
2,5	[number]	k4	2,5
metru	metr	k1gInSc2	metr
a	a	k8xC	a
nejmenší	malý	k2eAgFnSc1d3	nejmenší
šířka	šířka	k1gFnSc1	šířka
8	[number]	k4	8
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
NPP	NPP	kA	NPP
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
je	být	k5eAaImIp3nS	být
1,11	[number]	k4	1,11
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nepřístupnosti	nepřístupnost	k1gFnSc3	nepřístupnost
terénu	terén	k1gInSc2	terén
<g/>
,	,	kIx,	,
nevhodným	vhodný	k2eNgFnPc3d1	nevhodná
podmínkám	podmínka	k1gFnPc3	podmínka
pro	pro	k7c4	pro
zemědělství	zemědělství	k1gNnSc4	zemědělství
i	i	k8xC	i
lesnictví	lesnictví	k1gNnSc4	lesnictví
a	a	k8xC	a
jinému	jiný	k2eAgInSc3d1	jiný
pohledu	pohled	k1gInSc3	pohled
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
krajinu	krajina	k1gFnSc4	krajina
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
brána	brána	k1gFnSc1	brána
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
využívána	využívat	k5eAaPmNgFnS	využívat
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začali	začít	k5eAaPmAgMnP	začít
bránu	brána	k1gFnSc4	brána
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
především	především	k9	především
umělci	umělec	k1gMnPc1	umělec
ze	z	k7c2	z
sousedního	sousední	k2eAgNnSc2d1	sousední
Saska	Sasko	k1gNnSc2	Sasko
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ji	on	k3xPp3gFnSc4	on
zachycovali	zachycovat	k5eAaImAgMnP	zachycovat
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dílech	dílo	k1gNnPc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1826	[number]	k4	1826
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
paty	pata	k1gFnSc2	pata
Pravčické	Pravčický	k2eAgFnSc2d1	Pravčická
brány	brána	k1gFnSc2	brána
zprovozněno	zprovoznit	k5eAaPmNgNnS	zprovoznit
první	první	k4xOgNnPc1	první
občerstvovací	občerstvovací	k2eAgNnPc1d1	občerstvovací
zařízení	zařízení	k1gNnPc1	zařízení
a	a	k8xC	a
o	o	k7c4	o
11	[number]	k4	11
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
i	i	k9	i
nocležna	nocležna	k1gFnSc1	nocležna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
význačné	význačný	k2eAgFnPc4d1	význačná
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
bránu	brána	k1gFnSc4	brána
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
navštívili	navštívit	k5eAaPmAgMnP	navštívit
patří	patřit	k5eAaImIp3nP	patřit
bezesporu	bezesporu	k9	bezesporu
dánský	dánský	k2eAgMnSc1d1	dánský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Hans	Hans	k1gMnSc1	Hans
Christian	Christian	k1gMnSc1	Christian
Andersen	Andersen	k1gMnSc1	Andersen
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
poprvé	poprvé	k6eAd1	poprvé
zavítal	zavítat	k5eAaPmAgMnS	zavítat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1831	[number]	k4	1831
a	a	k8xC	a
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
zápiscích	zápisek	k1gInPc6	zápisek
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
upravena	upravit	k5eAaPmNgFnS	upravit
stezka	stezka	k1gFnSc1	stezka
přes	přes	k7c4	přes
Dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
důl	důl	k1gInSc4	důl
určená	určený	k2eAgFnSc1d1	určená
především	především	k9	především
pro	pro	k7c4	pro
přístup	přístup	k1gInSc4	přístup
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
její	její	k3xOp3gNnSc4	její
úpravy	úprava	k1gFnPc1	úprava
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
kamenné	kamenný	k2eAgInPc1d1	kamenný
můstky	můstek	k1gInPc1	můstek
usnadňující	usnadňující	k2eAgFnSc4d1	usnadňující
cestu	cesta	k1gFnSc4	cesta
náročným	náročný	k2eAgInSc7d1	náročný
terénem	terén	k1gInSc7	terén
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
majitele	majitel	k1gMnSc2	majitel
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
knížete	kníže	k1gMnSc2	kníže
Edmunda	Edmund	k1gMnSc2	Edmund
Clary-Aldringena	Clary-Aldringen	k1gMnSc2	Clary-Aldringen
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
stavba	stavba	k1gFnSc1	stavba
hostince	hostinec	k1gInSc2	hostinec
Sokolí	sokolí	k2eAgNnSc4d1	sokolí
hnízdo	hnízdo	k1gNnSc4	hnízdo
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
pojmenovaného	pojmenovaný	k2eAgInSc2d1	pojmenovaný
Prebischtor	Prebischtor	k1gInSc1	Prebischtor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dokončena	dokončen	k2eAgFnSc1d1	dokončena
a	a	k8xC	a
otevřena	otevřen	k2eAgFnSc1d1	otevřena
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
na	na	k7c6	na
Pravčické	Pravčický	k2eAgFnSc6d1	Pravčická
Bráně	brána	k1gFnSc6	brána
také	také	k9	také
začalo	začít	k5eAaPmAgNnS	začít
poprvé	poprvé	k6eAd1	poprvé
vybírat	vybírat	k5eAaImF	vybírat
vstupné	vstupné	k1gNnSc4	vstupné
<g/>
.	.	kIx.	.
</s>
<s>
Hotel	hotel	k1gInSc4	hotel
Sokolí	sokolí	k2eAgNnSc1d1	sokolí
hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
v	v	k7c6	v
soukromém	soukromý	k2eAgNnSc6d1	soukromé
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Sedimenty	sediment	k1gInPc1	sediment
na	na	k7c4	na
území	území	k1gNnSc4	území
Českosaského	českosaský	k2eAgNnSc2d1	Českosaské
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
se	se	k3xPyFc4	se
ukládaly	ukládat	k5eAaImAgFnP	ukládat
v	v	k7c6	v
období	období	k1gNnSc6	období
svrchní	svrchní	k2eAgFnSc2d1	svrchní
křídy	křída	k1gFnSc2	křída
v	v	k7c6	v
druhohorách	druhohory	k1gFnPc6	druhohory
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytovalo	vyskytovat	k5eAaImAgNnS	vyskytovat
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
své	svůj	k3xOyFgFnSc6	svůj
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
poměrně	poměrně	k6eAd1	poměrně
mělké	mělký	k2eAgNnSc1d1	mělké
<g/>
,	,	kIx,	,
a	a	k8xC	a
ukládaly	ukládat	k5eAaImAgFnP	ukládat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
především	především	k6eAd1	především
křemenné	křemenný	k2eAgInPc1d1	křemenný
pískovce	pískovec	k1gInPc1	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prohloubení	prohloubení	k1gNnSc3	prohloubení
pánve	pánev	k1gFnSc2	pánev
(	(	kIx(	(
<g/>
zvýšení	zvýšení	k1gNnSc3	zvýšení
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
<g/>
)	)	kIx)	)
a	a	k8xC	a
ukládaly	ukládat	k5eAaImAgFnP	ukládat
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
prachovce	prachovka	k1gFnSc3	prachovka
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
tak	tak	k6eAd1	tak
dnes	dnes	k6eAd1	dnes
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
pískovcové	pískovcový	k2eAgFnPc4d1	pískovcová
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
brána	brána	k1gFnSc1	brána
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
jizerským	jizerský	k2eAgNnSc7d1	Jizerské
souvrstvím	souvrství	k1gNnSc7	souvrství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
ukládalo	ukládat	k5eAaImAgNnS	ukládat
od	od	k7c2	od
středního	střední	k2eAgInSc2d1	střední
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
svrchního	svrchní	k2eAgInSc2d1	svrchní
turonu	turon	k1gInSc2	turon
a	a	k8xC	a
nabývá	nabývat	k5eAaImIp3nS	nabývat
mocnosti	mocnost	k1gFnPc4	mocnost
od	od	k7c2	od
350	[number]	k4	350
do	do	k7c2	do
420	[number]	k4	420
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Šikmé	šikmý	k2eAgNnSc1d1	šikmé
zvrstvení	zvrstvení	k1gNnSc1	zvrstvení
pískovcových	pískovcový	k2eAgNnPc2d1	pískovcové
lamin	lamino	k1gNnPc2	lamino
(	(	kIx(	(
<g/>
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
)	)	kIx)	)
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
převládající	převládající	k2eAgInSc4d1	převládající
jihovýchodní	jihovýchodní	k2eAgInSc4d1	jihovýchodní
směr	směr	k1gInSc4	směr
mořských	mořský	k2eAgInPc2d1	mořský
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
například	například	k6eAd1	například
od	od	k7c2	od
západního	západní	k2eAgNnSc2d1	západní
proudění	proudění	k1gNnSc2	proudění
patrného	patrný	k2eAgNnSc2d1	patrné
na	na	k7c6	na
skalách	skála	k1gFnPc6	skála
bělohorského	bělohorský	k2eAgNnSc2d1	bělohorské
souvrství	souvrství	k1gNnSc2	souvrství
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ve	v	k7c6	v
Hřensku	Hřensko	k1gNnSc6	Hřensko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ústupu	ústup	k1gInSc6	ústup
moře	moře	k1gNnSc2	moře
byly	být	k5eAaImAgInP	být
obnažené	obnažený	k2eAgInPc1d1	obnažený
pískovcové	pískovcový	k2eAgInPc1d1	pískovcový
masivy	masiv	k1gInPc1	masiv
exponovány	exponován	k2eAgInPc1d1	exponován
přírodním	přírodní	k2eAgFnPc3d1	přírodní
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
;	;	kIx,	;
na	na	k7c6	na
erozi	eroze	k1gFnSc6	eroze
a	a	k8xC	a
odnosu	odnos	k1gInSc6	odnos
materiálu	materiál	k1gInSc2	materiál
se	se	k3xPyFc4	se
podílelo	podílet	k5eAaImAgNnS	podílet
více	hodně	k6eAd2	hodně
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
hloubková	hloubkový	k2eAgFnSc1d1	hloubková
říční	říční	k2eAgFnSc1d1	říční
eroze	eroze	k1gFnSc1	eroze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
strmé	strmý	k2eAgInPc4d1	strmý
skalní	skalní	k2eAgInPc4d1	skalní
svahy	svah	k1gInPc4	svah
<g/>
.	.	kIx.	.
</s>
<s>
Horniny	hornina	k1gFnPc1	hornina
byly	být	k5eAaImAgFnP	být
následně	následně	k6eAd1	následně
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
zvětrávání	zvětrávání	k1gNnSc2	zvětrávání
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odnosu	odnos	k1gInSc3	odnos
jejich	jejich	k3xOp3gFnPc2	jejich
méně	málo	k6eAd2	málo
pevných	pevný	k2eAgFnPc2d1	pevná
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
různých	různý	k2eAgInPc2d1	různý
útvarů	útvar	k1gInPc2	útvar
pískovcového	pískovcový	k2eAgInSc2d1	pískovcový
reliéfu	reliéf	k1gInSc2	reliéf
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
formování	formování	k1gNnSc6	formování
skalní	skalní	k2eAgFnSc2d1	skalní
brány	brána	k1gFnSc2	brána
nejprve	nejprve	k6eAd1	nejprve
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
tzv.	tzv.	kA	tzv.
skalní	skalní	k2eAgFnSc6d1	skalní
zdi	zeď	k1gFnSc6	zeď
postupným	postupný	k2eAgNnSc7d1	postupné
opadáváním	opadávání	k1gNnSc7	opadávání
kusů	kus	k1gInPc2	kus
pískovce	pískovec	k1gInSc2	pískovec
a	a	k8xC	a
skalním	skalní	k2eAgNnSc7d1	skalní
řícením	řícení	k1gNnSc7	řícení
podél	podél	k7c2	podél
souběžných	souběžný	k2eAgFnPc2d1	souběžná
puklin	puklina	k1gFnPc2	puklina
v	v	k7c6	v
pískovcovém	pískovcový	k2eAgInSc6d1	pískovcový
masivu	masiv	k1gInSc6	masiv
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
zdi	zeď	k1gFnSc3	zeď
potom	potom	k6eAd1	potom
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
převis	převis	k1gInSc1	převis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
postupně	postupně	k6eAd1	postupně
rozšiřoval	rozšiřovat	k5eAaImAgMnS	rozšiřovat
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úplnému	úplný	k2eAgNnSc3d1	úplné
proděravění	proděravění	k1gNnSc3	proděravění
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Ostré	ostrý	k2eAgInPc1d1	ostrý
hrany	hran	k1gInPc1	hran
zbylé	zbylý	k2eAgInPc1d1	zbylý
po	po	k7c6	po
opadu	opad	k1gInSc6	opad
materiálu	materiál	k1gInSc2	materiál
jsou	být	k5eAaImIp3nP	být
časem	časem	k6eAd1	časem
zahlazovány	zahlazovat	k5eAaImNgInP	zahlazovat
zvětráváním	zvětrávání	k1gNnSc7	zvětrávání
(	(	kIx(	(
<g/>
solným	solný	k2eAgFnPc3d1	solná
či	či	k8xC	či
mrazovým	mrazový	k2eAgFnPc3d1	mrazová
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
bioerozí	bioerozit	k5eAaPmIp3nS	bioerozit
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
ploše	plocha	k1gFnSc3	plocha
NPP	NPP	kA	NPP
náleží	náležet	k5eAaImIp3nS	náležet
i	i	k9	i
okolní	okolní	k2eAgInPc4d1	okolní
svahy	svah	k1gInPc4	svah
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
biotopů	biotop	k1gInPc2	biotop
<g/>
;	;	kIx,	;
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
zjištěny	zjistit	k5eAaPmNgFnP	zjistit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
mapováním	mapování	k1gNnSc7	mapování
přírodních	přírodní	k2eAgNnPc2d1	přírodní
stanovišť	stanoviště	k1gNnPc2	stanoviště
soustavy	soustava	k1gFnSc2	soustava
Natura	Natura	k1gFnSc1	Natura
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pískovcovém	pískovcový	k2eAgInSc6d1	pískovcový
substrátu	substrát	k1gInSc6	substrát
lze	lze	k6eAd1	lze
obecně	obecně	k6eAd1	obecně
nalézt	nalézt	k5eAaPmF	nalézt
spíše	spíše	k9	spíše
na	na	k7c4	na
živiny	živina	k1gFnPc4	živina
nenáročné	náročný	k2eNgFnPc4d1	nenáročná
acidofyty	acidofyt	k1gInPc4	acidofyt
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
kyselomilné	kyselomilný	k2eAgInPc1d1	kyselomilný
druhy	druh	k1gInPc1	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vegetaci	vegetace	k1gFnSc6	vegetace
převládá	převládat	k5eAaImIp3nS	převládat
společenstvo	společenstvo	k1gNnSc1	společenstvo
Boreokontinentálních	Boreokontinentální	k2eAgInPc2d1	Boreokontinentální
borů	bor	k1gInPc2	bor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
vrcholových	vrcholový	k2eAgFnPc6d1	vrcholová
partiích	partie	k1gFnPc6	partie
skal	skála	k1gFnPc2	skála
a	a	k8xC	a
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
46	[number]	k4	46
%	%	kIx~	%
rozlohy	rozloha	k1gFnPc1	rozloha
území	území	k1gNnSc2	území
NPP	NPP	kA	NPP
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
převažují	převažovat	k5eAaImIp3nP	převažovat
borovice	borovice	k1gFnPc1	borovice
lesní	lesní	k2eAgFnPc1d1	lesní
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gInSc1	Pinus
sylvestris	sylvestris	k1gFnSc2	sylvestris
<g/>
)	)	kIx)	)
a	a	k8xC	a
bříza	bříza	k1gFnSc1	bříza
bělokorá	bělokorý	k2eAgFnSc1d1	bělokorá
(	(	kIx(	(
<g/>
Betula	Betula	k1gFnSc1	Betula
pendula	pendula	k1gFnSc1	pendula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
bylin	bylina	k1gFnPc2	bylina
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
metlička	metlička	k1gFnSc1	metlička
křivolaká	křivolaký	k2eAgFnSc1d1	křivolaká
(	(	kIx(	(
<g/>
Avenella	Avenella	k1gFnSc1	Avenella
flexuosa	flexuosa	k1gFnSc1	flexuosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hasivka	hasivka	k1gFnSc1	hasivka
orličí	orličí	k2eAgFnSc1d1	orličí
(	(	kIx(	(
<g/>
Pteridium	Pteridium	k1gNnSc1	Pteridium
aquilinum	aquilinum	k1gNnSc1	aquilinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brusnice	brusnice	k1gFnSc1	brusnice
borůvka	borůvka	k1gFnSc1	borůvka
(	(	kIx(	(
<g/>
Vaccinium	Vaccinium	k1gNnSc1	Vaccinium
myrtillus	myrtillus	k1gInSc1	myrtillus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brusnice	brusnice	k1gFnSc1	brusnice
brusinka	brusinka	k1gFnSc1	brusinka
(	(	kIx(	(
<g/>
Vaccinium	Vaccinium	k1gNnSc1	Vaccinium
vitis-idaea	vitisdae	k1gInSc2	vitis-idae
<g/>
)	)	kIx)	)
či	či	k8xC	či
vřes	vřes	k1gInSc4	vřes
obecný	obecný	k2eAgInSc4d1	obecný
(	(	kIx(	(
<g/>
Calluna	Calluna	k1gFnSc1	Calluna
vulgaris	vulgaris	k1gFnSc2	vulgaris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mechů	mech	k1gInPc2	mech
je	být	k5eAaImIp3nS	být
hojný	hojný	k2eAgMnSc1d1	hojný
například	například	k6eAd1	například
dvouhrotec	dvouhrotec	k1gInSc1	dvouhrotec
chvostnatý	chvostnatý	k2eAgInSc1d1	chvostnatý
(	(	kIx(	(
<g/>
Dicranum	Dicranum	k1gInSc1	Dicranum
scoparium	scoparium	k1gNnSc1	scoparium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
paprutka	paprutka	k1gFnSc1	paprutka
nící	nící	k1gFnSc1	nící
(	(	kIx(	(
<g/>
Pohlia	Pohlia	k1gFnSc1	Pohlia
nutans	nutansa	k1gFnPc2	nutansa
<g/>
)	)	kIx)	)
a	a	k8xC	a
křivonožka	křivonožka	k1gMnSc1	křivonožka
zprohýbaná	zprohýbaný	k2eAgFnSc1d1	zprohýbaná
(	(	kIx(	(
<g/>
Campylopus	Campylopus	k1gMnSc1	Campylopus
flexuosus	flexuosus	k1gMnSc1	flexuosus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
lišejníky	lišejník	k1gInPc7	lišejník
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgInSc7d1	častý
rodem	rod	k1gInSc7	rod
je	být	k5eAaImIp3nS	být
dutohlávka	dutohlávka	k1gFnSc1	dutohlávka
(	(	kIx(	(
<g/>
Cladonia	Cladonium	k1gNnPc1	Cladonium
sp	sp	k?	sp
<g/>
.	.	kIx.	.
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
dutohlávka	dutohlávka	k1gFnSc1	dutohlávka
vyzáblá	vyzáblý	k2eAgFnSc1d1	vyzáblá
(	(	kIx(	(
<g/>
Cladonia	Cladonium	k1gNnSc2	Cladonium
macilenta	macilento	k1gNnSc2	macilento
<g/>
)	)	kIx)	)
rostoucí	rostoucí	k2eAgNnSc1d1	rostoucí
na	na	k7c6	na
malé	malý	k2eAgFnSc6d1	malá
vrstvičce	vrstvička	k1gFnSc6	vrstvička
humusu	humus	k1gInSc2	humus
<g/>
,	,	kIx,	,
dutohlávka	dutohlávka	k1gFnSc1	dutohlávka
prstitá	prstitat	k5eAaPmIp3nS	prstitat
(	(	kIx(	(
<g/>
Cladonia	Cladonium	k1gNnSc2	Cladonium
digitata	digitata	k1gFnSc1	digitata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dutohlávka	dutohlávka	k1gFnSc1	dutohlávka
pohárkatá	pohárkatý	k2eAgFnSc1d1	pohárkatý
(	(	kIx(	(
<g/>
Cladonia	Cladonium	k1gNnSc2	Cladonium
pyxidata	pyxidata	k1gFnSc1	pyxidata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dutohlávka	dutohlávka	k1gFnSc1	dutohlávka
červcová	červcový	k2eAgFnSc1d1	červcový
(	(	kIx(	(
<g/>
Cladonia	Cladonium	k1gNnSc2	Cladonium
coccifera	coccifero	k1gNnSc2	coccifero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dutohlávka	dutohlávka	k1gFnSc1	dutohlávka
parohatá	parohatý	k2eAgFnSc1d1	parohatá
(	(	kIx(	(
<g/>
Cladonia	Cladonium	k1gNnPc1	Cladonium
cervicornis	cervicornis	k1gFnPc2	cervicornis
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pískovcových	pískovcový	k2eAgFnPc6d1	pískovcová
skalách	skála	k1gFnPc6	skála
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
terčovku	terčovka	k1gFnSc4	terčovka
zakřivenou	zakřivený	k2eAgFnSc4d1	zakřivená
(	(	kIx(	(
<g/>
Arctoparmelia	Arctoparmelius	k1gMnSc2	Arctoparmelius
incurva	incurv	k1gMnSc2	incurv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
žlutavé	žlutavý	k2eAgFnPc4d1	žlutavá
stélky	stélka	k1gFnPc4	stélka
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
vzácné	vzácný	k2eAgInPc4d1	vzácný
lišejníky	lišejník	k1gInPc4	lišejník
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
nalézt	nalézt	k5eAaBmF	nalézt
i	i	k9	i
na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
útvaru	útvar	k1gInSc6	útvar
PB	PB	kA	PB
spolu	spolu	k6eAd1	spolu
například	například	k6eAd1	například
s	s	k7c7	s
drobnovýtruskou	drobnovýtruský	k2eAgFnSc7d1	drobnovýtruský
hnědavou	hnědavý	k2eAgFnSc7d1	hnědavá
(	(	kIx(	(
<g/>
Acarospora	Acarospor	k1gMnSc2	Acarospor
fuscata	fuscat	k1gMnSc2	fuscat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šálečkou	šálečka	k1gFnSc7	šálečka
černohnědou	černohnědý	k2eAgFnSc7d1	černohnědá
(	(	kIx(	(
<g/>
Lecidea	Lecide	k2eAgNnPc1d1	Lecide
fuscoatra	fuscoatrum	k1gNnPc1	fuscoatrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
terčovkou	terčovka	k1gFnSc7	terčovka
skalní	skalní	k2eAgInSc4d1	skalní
(	(	kIx(	(
<g/>
Parmelia	Parmelia	k1gFnSc1	Parmelia
saxatilis	saxatilis	k1gFnSc2	saxatilis
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
terčovkou	terčovka	k1gFnSc7	terčovka
střechovitou	střechovitý	k2eAgFnSc7d1	střechovitá
(	(	kIx(	(
<g/>
Parmelia	Parmelium	k1gNnPc1	Parmelium
omphalodes	omphalodesa	k1gFnPc2	omphalodesa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stromech	strom	k1gInPc6	strom
najdeme	najít	k5eAaPmIp1nP	najít
terčovku	terčovka	k1gFnSc4	terčovka
bublinatou	bublinatý	k2eAgFnSc4d1	bublinatá
(	(	kIx(	(
<g/>
Hypogymnia	Hypogymnium	k1gNnSc2	Hypogymnium
physodes	physodes	k1gInSc4	physodes
<g/>
)	)	kIx)	)
či	či	k8xC	či
terčovku	terčovka	k1gFnSc4	terčovka
brázditou	brázditý	k2eAgFnSc4d1	brázditá
(	(	kIx(	(
<g/>
Parmelia	Parmelius	k1gMnSc4	Parmelius
sulcata	sulcat	k1gMnSc4	sulcat
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
provazovku	provazovka	k1gFnSc4	provazovka
(	(	kIx(	(
<g/>
rod	rod	k1gInSc4	rod
Usnea	Usne	k1gInSc2	Usne
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
vzácnou	vzácný	k2eAgFnSc4d1	vzácná
kvůli	kvůli	k7c3	kvůli
znečištění	znečištění	k1gNnSc3	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
štěrbinách	štěrbina	k1gFnPc6	štěrbina
na	na	k7c6	na
Pravčické	Pravčický	k2eAgFnSc6d1	Pravčická
bráně	brána	k1gFnSc6	brána
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
játrovka	játrovka	k1gFnSc1	játrovka
kryjnice	kryjnice	k1gFnSc1	kryjnice
Meylanova	Meylanův	k2eAgFnSc1d1	Meylanův
(	(	kIx(	(
<g/>
Calypogeia	Calypogeia	k1gFnSc1	Calypogeia
integristipula	integristipula	k1gFnSc1	integristipula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mechů	mech	k1gInPc2	mech
zde	zde	k6eAd1	zde
rostou	růst	k5eAaImIp3nP	růst
čtyřzoubek	čtyřzoubek	k1gInSc4	čtyřzoubek
průzračný	průzračný	k2eAgInSc4d1	průzračný
(	(	kIx(	(
<g/>
Tetraphis	Tetraphis	k1gFnSc1	Tetraphis
pellucida	pellucida	k1gFnSc1	pellucida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvouhroteček	dvouhroteček	k1gInSc1	dvouhroteček
různotvárný	různotvárný	k2eAgInSc1d1	různotvárný
(	(	kIx(	(
<g/>
Dicranella	Dicranella	k1gMnSc1	Dicranella
heteromalla	heteromalla	k1gMnSc1	heteromalla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kroucenec	kroucenec	k1gMnSc1	kroucenec
zední	zední	k1gMnSc1	zední
(	(	kIx(	(
<g/>
Tortula	Tortula	k1gFnSc1	Tortula
muralis	muralis	k1gFnSc2	muralis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ploník	ploník	k1gInSc1	ploník
chluponosný	chluponosný	k2eAgInSc1d1	chluponosný
(	(	kIx(	(
<g/>
Polytrichum	Polytrichum	k1gInSc1	Polytrichum
piliferum	piliferum	k1gInSc1	piliferum
<g/>
)	)	kIx)	)
<g/>
nebo	nebo	k8xC	nebo
dřípovičník	dřípovičník	k1gInSc1	dřípovičník
zpeřený	zpeřený	k2eAgInSc1d1	zpeřený
(	(	kIx(	(
<g/>
Schistostega	Schistostega	k1gFnSc1	Schistostega
pennata	pennata	k1gFnSc1	pennata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
prvoklíček	prvoklíček	k1gInSc1	prvoklíček
světélkuje	světélkovat	k5eAaImIp3nS	světélkovat
díky	díky	k7c3	díky
lomu	lom	k1gInSc2	lom
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
vypouklých	vypouklý	k2eAgFnPc6d1	vypouklá
buňkách	buňka	k1gFnPc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rostlin	rostlina	k1gFnPc2	rostlina
tu	tu	k6eAd1	tu
najdeme	najít	k5eAaPmIp1nP	najít
několik	několik	k4yIc4	několik
borovic	borovice	k1gFnPc2	borovice
lesních	lesní	k2eAgFnPc2d1	lesní
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gInSc1	Pinus
sylvestris	sylvestris	k1gFnSc2	sylvestris
<g/>
)	)	kIx)	)
a	a	k8xC	a
břízu	bříza	k1gFnSc4	bříza
bělokorou	bělokorý	k2eAgFnSc4d1	bělokorá
(	(	kIx(	(
<g/>
Betula	Betul	k1gMnSc2	Betul
pendula	pendul	k1gMnSc2	pendul
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vřes	vřes	k1gInSc4	vřes
obecný	obecný	k2eAgInSc4d1	obecný
(	(	kIx(	(
<g/>
Calluna	Calluna	k1gFnSc1	Calluna
vulgaris	vulgaris	k1gFnSc2	vulgaris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brusnici	brusnice	k1gFnSc4	brusnice
borůvku	borůvka	k1gFnSc4	borůvka
(	(	kIx(	(
<g/>
Vaccinium	Vaccinium	k1gNnSc4	Vaccinium
myrtillus	myrtillus	k1gInSc1	myrtillus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brusnici	brusnice	k1gFnSc4	brusnice
brusinku	brusinka	k1gFnSc4	brusinka
(	(	kIx(	(
<g/>
Vaccinium	Vaccinium	k1gNnSc4	Vaccinium
vitis-idaea	vitisdae	k1gInSc2	vitis-idae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jestřábník	jestřábník	k1gInSc1	jestřábník
zední	zední	k1gNnSc2	zední
(	(	kIx(	(
<g/>
Hieracium	Hieracium	k1gNnSc1	Hieracium
murorum	murorum	k1gNnSc1	murorum
<g/>
)	)	kIx)	)
a	a	k8xC	a
metličku	metlička	k1gFnSc4	metlička
křivolakou	křivolaký	k2eAgFnSc4d1	křivolaká
(	(	kIx(	(
<g/>
Avenella	Avenella	k1gMnSc1	Avenella
flexuosa	flexuosa	k1gFnSc1	flexuosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
suchá	suchý	k2eAgNnPc4d1	suché
společenstva	společenstvo	k1gNnPc4	společenstvo
patří	patřit	k5eAaImIp3nS	patřit
Štěrbinová	štěrbinový	k2eAgFnSc1d1	štěrbinová
vegetace	vegetace	k1gFnSc1	vegetace
silikátových	silikátový	k2eAgFnPc2d1	silikátová
skal	skála	k1gFnPc2	skála
a	a	k8xC	a
drolin	drolina	k1gFnPc2	drolina
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gInPc7	jejíž
hojnými	hojný	k2eAgInPc7d1	hojný
druhy	druh	k1gInPc7	druh
jsou	být	k5eAaImIp3nP	být
vřes	vřes	k1gInSc4	vřes
obecný	obecný	k2eAgInSc4d1	obecný
(	(	kIx(	(
<g/>
Calluna	Calluna	k1gFnSc1	Calluna
vulgaris	vulgaris	k1gFnSc2	vulgaris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brusnice	brusnice	k1gFnSc1	brusnice
borůvka	borůvka	k1gFnSc1	borůvka
(	(	kIx(	(
<g/>
Vaccinium	Vaccinium	k1gNnSc1	Vaccinium
myrtillus	myrtillus	k1gInSc1	myrtillus
<g/>
)	)	kIx)	)
a	a	k8xC	a
brusnice	brusnice	k1gFnSc1	brusnice
brusinka	brusinka	k1gFnSc1	brusinka
(	(	kIx(	(
<g/>
Vaccinium	Vaccinium	k1gNnSc1	Vaccinium
vitis-idaea	vitisdae	k1gInSc2	vitis-idae
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
z	z	k7c2	z
mechů	mech	k1gInPc2	mech
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
především	především	k9	především
křivonožka	křivonožka	k1gMnSc1	křivonožka
zprohýbaná	zprohýbaný	k2eAgFnSc1d1	zprohýbaná
(	(	kIx(	(
<g/>
Campylopus	Campylopus	k1gMnSc1	Campylopus
flexuosus	flexuosus	k1gMnSc1	flexuosus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ploník	ploník	k1gInSc1	ploník
chluponosný	chluponosný	k2eAgInSc1d1	chluponosný
(	(	kIx(	(
<g/>
Polytrichum	Polytrichum	k1gInSc1	Polytrichum
piliferum	piliferum	k1gInSc1	piliferum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rohozub	rohozub	k1gMnSc1	rohozub
nachový	nachový	k2eAgMnSc1d1	nachový
(	(	kIx(	(
<g/>
Ceratodon	Ceratodon	k1gMnSc1	Ceratodon
purpureus	purpureus	k1gMnSc1	purpureus
<g/>
)	)	kIx)	)
a	a	k8xC	a
paprutka	paprutka	k1gFnSc1	paprutka
nící	nící	k1gFnSc1	nící
(	(	kIx(	(
<g/>
Pohlia	Pohlia	k1gFnSc1	Pohlia
nutans	nutansa	k1gFnPc2	nutansa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
na	na	k7c6	na
hranách	hrana	k1gFnPc6	hrana
skal	skála	k1gFnPc2	skála
exponovaných	exponovaný	k2eAgFnPc6d1	exponovaná
severně	severně	k6eAd1	severně
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
vlhká	vlhký	k2eAgNnPc1d1	vlhké
společenstva	společenstvo	k1gNnPc1	společenstvo
Brusnicové	brusnicový	k2eAgFnSc2d1	brusnicová
vegetace	vegetace	k1gFnSc2	vegetace
skal	skála	k1gFnPc2	skála
a	a	k8xC	a
drolin	drolina	k1gFnPc2	drolina
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
brusnice	brusnice	k1gFnSc1	brusnice
borůvka	borůvka	k1gFnSc1	borůvka
(	(	kIx(	(
<g/>
Vaccinium	Vaccinium	k1gNnSc1	Vaccinium
myrtillus	myrtillus	k1gInSc1	myrtillus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
rašeliník	rašeliník	k1gInSc1	rašeliník
(	(	kIx(	(
<g/>
Sphagnum	Sphagnum	k1gInSc1	Sphagnum
sp	sp	k?	sp
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
bělomech	bělomech	k1gInSc1	bělomech
skalní	skalní	k2eAgInSc1d1	skalní
(	(	kIx(	(
<g/>
Leucobryum	Leucobryum	k1gInSc1	Leucobryum
juniperoideum	juniperoideum	k1gInSc1	juniperoideum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okolních	okolní	k2eAgInPc6d1	okolní
svazích	svah	k1gInPc6	svah
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
nacházejí	nacházet	k5eAaImIp3nP	nacházet
acidofilní	acidofilní	k2eAgFnPc1d1	acidofilní
(	(	kIx(	(
<g/>
kyselé	kyselý	k2eAgFnPc1d1	kyselá
<g/>
)	)	kIx)	)
bučiny	bučina	k1gFnPc1	bučina
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
společenstvo	společenstvo	k1gNnSc1	společenstvo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
v	v	k7c6	v
Labských	labský	k2eAgInPc6d1	labský
pískovcích	pískovec	k1gInPc6	pískovec
dominantní	dominantní	k2eAgMnSc1d1	dominantní
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
lesním	lesní	k2eAgNnSc7d1	lesní
hospodářstvím	hospodářství	k1gNnSc7	hospodářství
bylo	být	k5eAaImAgNnS	být
nahrazováno	nahrazovat	k5eAaImNgNnS	nahrazovat
smrkovými	smrkový	k2eAgFnPc7d1	smrková
monokulturami	monokultura	k1gFnPc7	monokultura
nebo	nebo	k8xC	nebo
borovicemi	borovice	k1gFnPc7	borovice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
území	území	k1gNnSc4	území
NPP	NPP	kA	NPP
dnes	dnes	k6eAd1	dnes
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
10	[number]	k4	10
<g/>
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
.	.	kIx.	.
</s>
<s>
Stromové	stromový	k2eAgNnSc1d1	stromové
patro	patro	k1gNnSc1	patro
tvoří	tvořit	k5eAaImIp3nS	tvořit
buk	buk	k1gInSc4	buk
lesní	lesní	k2eAgInSc4d1	lesní
(	(	kIx(	(
<g/>
Fagus	Fagus	k1gInSc4	Fagus
sylvatica	sylvatic	k1gInSc2	sylvatic
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
potom	potom	k6eAd1	potom
jedle	jedle	k6eAd1	jedle
bělokorá	bělokorý	k2eAgFnSc1d1	bělokorá
(	(	kIx(	(
<g/>
Abies	Abies	k1gInSc1	Abies
alba	album	k1gNnSc2	album
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
letní	letní	k2eAgInSc1d1	letní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
robur	robura	k1gFnPc2	robura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
zimní	zimní	k2eAgInSc1d1	zimní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
petraea	petrae	k1gInSc2	petrae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
javor	javor	k1gInSc1	javor
klen	klen	k1gInSc1	klen
(	(	kIx(	(
<g/>
Acer	Acer	k1gMnSc1	Acer
pseudoplatanus	pseudoplatanus	k1gMnSc1	pseudoplatanus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jilm	jilm	k1gInSc4	jilm
drsný	drsný	k2eAgInSc4d1	drsný
(	(	kIx(	(
<g/>
Ulmus	Ulmus	k1gInSc4	Ulmus
glabra	glabr	k1gInSc2	glabr
<g/>
)	)	kIx)	)
a	a	k8xC	a
smrk	smrk	k1gInSc1	smrk
ztepilý	ztepilý	k2eAgInSc1d1	ztepilý
(	(	kIx(	(
<g/>
Picea	Picea	k1gMnSc1	Picea
abies	abies	k1gMnSc1	abies
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bylin	bylina	k1gFnPc2	bylina
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházejí	nacházet	k5eAaImIp3nP	nacházet
bika	bika	k1gFnSc1	bika
bělavá	bělavý	k2eAgFnSc1d1	bělavá
(	(	kIx(	(
<g/>
Luzula	Luzula	k1gFnSc1	Luzula
luzuloides	luzuloides	k1gMnSc1	luzuloides
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bika	bika	k1gFnSc1	bika
chlupatá	chlupatý	k2eAgFnSc1d1	chlupatá
(	(	kIx(	(
<g/>
Luzula	Luzula	k1gFnSc1	Luzula
pilosa	pilosa	k1gFnSc1	pilosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
metlička	metlička	k1gFnSc1	metlička
křivolaká	křivolaký	k2eAgFnSc1d1	křivolaká
(	(	kIx(	(
<g/>
Avenella	Avenella	k1gFnSc1	Avenella
flexuosa	flexuosa	k1gFnSc1	flexuosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kapraď	kapraď	k1gFnSc1	kapraď
rozložená	rozložený	k2eAgFnSc1d1	rozložená
(	(	kIx(	(
<g/>
Dryopteris	Dryopteris	k1gFnSc1	Dryopteris
dilatata	dilatata	k1gFnSc1	dilatata
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pstroček	pstroček	k1gInSc1	pstroček
dvoulistý	dvoulistý	k2eAgInSc1d1	dvoulistý
(	(	kIx(	(
<g/>
Maianthemum	Maianthemum	k1gInSc1	Maianthemum
bifolium	bifolium	k1gNnSc1	bifolium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgMnPc7d1	typický
zástupci	zástupce	k1gMnPc7	zástupce
mechů	mech	k1gInPc2	mech
jsou	být	k5eAaImIp3nP	být
bezvláska	bezvláska	k1gFnSc1	bezvláska
vlnkatá	vlnkatý	k2eAgFnSc1d1	vlnkatý
(	(	kIx(	(
<g/>
Atrichum	Atrichum	k1gInSc1	Atrichum
undulatum	undulatum	k1gNnSc1	undulatum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvouhroteček	dvouhroteček	k1gInSc1	dvouhroteček
různotvárný	různotvárný	k2eAgInSc1d1	různotvárný
(	(	kIx(	(
<g/>
Dicranella	Dicranella	k1gMnSc1	Dicranella
heteromalla	heteromalla	k1gMnSc1	heteromalla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
paprutka	paprutka	k1gFnSc1	paprutka
nící	nící	k1gFnSc1	nící
(	(	kIx(	(
<g/>
Pohlia	Pohlia	k1gFnSc1	Pohlia
nutans	nutansa	k1gFnPc2	nutansa
<g/>
)	)	kIx)	)
a	a	k8xC	a
ploník	ploník	k1gInSc1	ploník
ztenčený	ztenčený	k2eAgInSc1d1	ztenčený
(	(	kIx(	(
<g/>
Polytrichastrum	Polytrichastrum	k1gNnSc1	Polytrichastrum
formosum	formosum	k1gNnSc1	formosum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prudkých	prudký	k2eAgInPc6d1	prudký
svazích	svah	k1gInPc6	svah
a	a	k8xC	a
na	na	k7c6	na
skálách	skála	k1gFnPc6	skála
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
společenstvo	společenstvo	k1gNnSc1	společenstvo
Subkontinentálních	Subkontinentální	k2eAgFnPc2d1	Subkontinentální
borových	borový	k2eAgFnPc2d1	Borová
doubrav	doubrava	k1gFnPc2	doubrava
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
světlé	světlý	k2eAgInPc4d1	světlý
lesy	les	k1gInPc4	les
obsahující	obsahující	k2eAgFnSc4d1	obsahující
borovici	borovice	k1gFnSc4	borovice
lesní	lesní	k2eAgMnSc1d1	lesní
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gInSc1	Pinus
sylvestris	sylvestris	k1gFnSc2	sylvestris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
letní	letní	k2eAgInSc1d1	letní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
robur	robura	k1gFnPc2	robura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
zimní	zimní	k2eAgInSc1d1	zimní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
petraea	petrae	k1gInSc2	petrae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
břízu	bříza	k1gFnSc4	bříza
bělokorou	bělokorý	k2eAgFnSc4d1	bělokorá
(	(	kIx(	(
<g/>
Betula	Betul	k1gMnSc4	Betul
pendula	pendul	k1gMnSc4	pendul
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeřáb	jeřáb	k1gInSc4	jeřáb
ptačí	ptačí	k2eAgInSc4d1	ptačí
(	(	kIx(	(
<g/>
Sorbus	Sorbus	k1gInSc4	Sorbus
aucuparia	aucuparium	k1gNnSc2	aucuparium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
podrostu	podrost	k1gInSc6	podrost
s	s	k7c7	s
brusnicí	brusnice	k1gFnSc7	brusnice
brusinkou	brusinka	k1gFnSc7	brusinka
(	(	kIx(	(
<g/>
Vaccinium	Vaccinium	k1gNnSc1	Vaccinium
vitis-idaea	vitisdae	k1gInSc2	vitis-idae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brusnicí	brusnice	k1gFnSc7	brusnice
borůvkou	borůvka	k1gFnSc7	borůvka
(	(	kIx(	(
<g/>
Vaccinium	Vaccinium	k1gNnSc1	Vaccinium
myrtillus	myrtillus	k1gInSc1	myrtillus
<g/>
)	)	kIx)	)
a	a	k8xC	a
vřesem	vřes	k1gInSc7	vřes
obecným	obecný	k2eAgInSc7d1	obecný
(	(	kIx(	(
<g/>
Calluna	Calluna	k1gFnSc1	Calluna
vulgaris	vulgaris	k1gFnSc2	vulgaris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
keřů	keř	k1gInPc2	keř
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
krušina	krušina	k1gFnSc1	krušina
olšová	olšový	k2eAgFnSc1d1	olšová
(	(	kIx(	(
<g/>
Frangula	Frangula	k1gFnSc1	Frangula
alnus	alnus	k1gMnSc1	alnus
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
častou	častý	k2eAgFnSc7d1	častá
kapradinou	kapradina	k1gFnSc7	kapradina
je	být	k5eAaImIp3nS	být
hasivka	hasivka	k1gFnSc1	hasivka
orličí	orličí	k2eAgFnSc1d1	orličí
(	(	kIx(	(
<g/>
Pteridium	Pteridium	k1gNnSc1	Pteridium
aquilinum	aquilinum	k1gNnSc1	aquilinum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mechorostů	mechorost	k1gInPc2	mechorost
zde	zde	k6eAd1	zde
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
křivonožku	křivonožka	k1gMnSc4	křivonožka
zprohýbanou	zprohýbaný	k2eAgFnSc4d1	zprohýbaná
(	(	kIx(	(
<g/>
Campylopus	Campylopus	k1gMnSc1	Campylopus
flexuosus	flexuosus	k1gMnSc1	flexuosus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvouhrotec	dvouhrotec	k1gInSc1	dvouhrotec
chvostnatý	chvostnatý	k2eAgInSc1d1	chvostnatý
(	(	kIx(	(
<g/>
Dicranum	Dicranum	k1gInSc1	Dicranum
scoparium	scoparium	k1gNnSc1	scoparium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvouhrotec	dvouhrotec	k1gInSc1	dvouhrotec
čeřitý	čeřitý	k2eAgInSc1d1	čeřitý
(	(	kIx(	(
<g/>
Dicranum	Dicranum	k1gInSc1	Dicranum
polysetum	polysetum	k1gNnSc1	polysetum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
travník	travník	k1gMnSc1	travník
Schreberův	Schreberův	k2eAgMnSc1d1	Schreberův
(	(	kIx(	(
<g/>
Pleurozium	Pleurozium	k1gNnSc1	Pleurozium
schreberi	schreber	k1gFnSc2	schreber
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
bělomech	bělomech	k1gInSc1	bělomech
skalní	skalní	k2eAgInSc1d1	skalní
(	(	kIx(	(
<g/>
Leucobryum	Leucobryum	k1gInSc1	Leucobryum
juniperoideum	juniperoideum	k1gInSc1	juniperoideum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
bezobratlým	bezobratlý	k2eAgMnSc7d1	bezobratlý
živočichem	živočich	k1gMnSc7	živočich
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
NPP	NPP	kA	NPP
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
roháček	roháček	k1gInSc1	roháček
jedlový	jedlový	k2eAgInSc1d1	jedlový
(	(	kIx(	(
<g/>
Ceruchus	Ceruchus	k1gInSc1	Ceruchus
chrysomelinus	chrysomelinus	k1gInSc1	chrysomelinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
larvy	larva	k1gFnPc1	larva
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nP	vyvíjet
ve	v	k7c6	v
tlejících	tlející	k2eAgInPc6d1	tlející
kmenech	kmen	k1gInPc6	kmen
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
významným	významný	k2eAgMnSc7d1	významný
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
chrobák	chrobák	k1gMnSc1	chrobák
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
Typhaeus	Typhaeus	k1gMnSc1	Typhaeus
typhoeus	typhoeus	k1gMnSc1	typhoeus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
jediným	jediný	k2eAgInSc7d1	jediný
areálem	areál	k1gInSc7	areál
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
okolí	okolí	k1gNnSc1	okolí
Hřenska	Hřensko	k1gNnSc2	Hřensko
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
znovuobjeven	znovuobjevit	k5eAaPmNgInS	znovuobjevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
nebyl	být	k5eNaImAgInS	být
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
40	[number]	k4	40
let	léto	k1gNnPc2	léto
nenalezen	naleznout	k5eNaPmNgInS	naleznout
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
mravkolev	mravkolev	k1gMnSc1	mravkolev
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Myrmeleon	Myrmeleon	k1gMnSc1	Myrmeleon
formicarius	formicarius	k1gMnSc1	formicarius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
ojedinělý	ojedinělý	k2eAgInSc1d1	ojedinělý
je	být	k5eAaImIp3nS	být
nález	nález	k1gInSc1	nález
tesaříka	tesařík	k1gMnSc2	tesařík
Pedostrangalia	Pedostrangalius	k1gMnSc2	Pedostrangalius
revestita	revestit	k1gMnSc2	revestit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
motýlů	motýl	k1gMnPc2	motýl
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
píďalka	píďalka	k1gFnSc1	píďalka
šedokřídlec	šedokřídlec	k1gInSc4	šedokřídlec
pampeliškový	pampeliškový	k2eAgInSc4d1	pampeliškový
(	(	kIx(	(
<g/>
Charissa	Charissa	k1gFnSc1	Charissa
glaucinaria	glaucinarium	k1gNnSc2	glaucinarium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žlutokřídlec	žlutokřídlec	k1gInSc4	žlutokřídlec
skalní	skalní	k2eAgInSc4d1	skalní
(	(	kIx(	(
<g/>
Idaea	Idaea	k1gFnSc1	Idaea
contiguaria	contiguarium	k1gNnSc2	contiguarium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
makadlovka	makadlovka	k1gFnSc1	makadlovka
Monochroa	Monochroa	k1gFnSc1	Monochroa
cytisella	cytisella	k1gFnSc1	cytisella
nebo	nebo	k8xC	nebo
zavíječ	zavíječ	k1gInSc1	zavíječ
Pempelia	Pempelium	k1gNnSc2	Pempelium
formosa	formosa	k1gFnSc1	formosa
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
skupinou	skupina	k1gFnSc7	skupina
obratlovců	obratlovec	k1gMnPc2	obratlovec
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
území	území	k1gNnSc6	území
jsou	být	k5eAaImIp3nP	být
ptáci	pták	k1gMnPc1	pták
<g/>
:	:	kIx,	:
sýkora	sýkora	k1gFnSc1	sýkora
koňadra	koňadra	k1gFnSc1	koňadra
(	(	kIx(	(
<g/>
Parus	Parus	k1gMnSc1	Parus
major	major	k1gMnSc1	major
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sýkora	sýkora	k1gFnSc1	sýkora
uhelníček	uhelníček	k1gMnSc1	uhelníček
(	(	kIx(	(
<g/>
Parus	Parus	k1gMnSc1	Parus
ater	ater	k1gMnSc1	ater
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sýkora	sýkora	k1gFnSc1	sýkora
modřinka	modřinka	k1gFnSc1	modřinka
(	(	kIx(	(
<g/>
Parus	Parus	k1gMnSc1	Parus
caeruleus	caeruleus	k1gMnSc1	caeruleus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kos	kos	k1gMnSc1	kos
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
Turdus	Turdus	k1gMnSc1	Turdus
merula	merul	k1gMnSc2	merul
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
sojka	sojka	k1gFnSc1	sojka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Garrulus	Garrulus	k1gMnSc1	Garrulus
glandarius	glandarius	k1gMnSc1	glandarius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
červenka	červenka	k1gFnSc1	červenka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Erithacus	Erithacus	k1gInSc1	Erithacus
rubecula	rubeculum	k1gNnSc2	rubeculum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rehek	rehek	k1gMnSc1	rehek
zahradní	zahradní	k2eAgMnSc1d1	zahradní
(	(	kIx(	(
<g/>
Phoenicurus	Phoenicurus	k1gMnSc1	Phoenicurus
phoenicurus	phoenicurus	k1gMnSc1	phoenicurus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rehek	rehek	k1gMnSc1	rehek
domácí	domácí	k2eAgMnSc1d1	domácí
(	(	kIx(	(
<g/>
Phoenicurus	Phoenicurus	k1gInSc1	Phoenicurus
ochruros	ochrurosa	k1gFnPc2	ochrurosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konipas	konipas	k1gMnSc1	konipas
bílý	bílý	k1gMnSc1	bílý
(	(	kIx(	(
<g/>
Motacilla	Motacilla	k1gFnSc1	Motacilla
alba	album	k1gNnSc2	album
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kulíšek	kulíšek	k1gMnSc1	kulíšek
nejmenší	malý	k2eAgMnSc1d3	nejmenší
(	(	kIx(	(
<g/>
Glaucidium	Glaucidium	k1gNnSc1	Glaucidium
passerinum	passerinum	k1gNnSc1	passerinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poštolka	poštolka	k1gFnSc1	poštolka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Falco	Falco	k1gMnSc1	Falco
tinnunculus	tinnunculus	k1gMnSc1	tinnunculus
<g/>
)	)	kIx)	)
<g/>
nebo	nebo	k8xC	nebo
krkavec	krkavec	k1gMnSc1	krkavec
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Corvus	Corvus	k1gInSc1	Corvus
corax	corax	k1gInSc1	corax
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skalní	skalní	k2eAgFnPc1d1	skalní
stěny	stěna	k1gFnPc1	stěna
jsou	být	k5eAaImIp3nP	být
příhodným	příhodný	k2eAgNnSc7d1	příhodné
stanovištěm	stanoviště	k1gNnSc7	stanoviště
pro	pro	k7c4	pro
sokola	sokol	k1gMnSc2	sokol
stěhovavého	stěhovavý	k2eAgMnSc2d1	stěhovavý
(	(	kIx(	(
<g/>
Falco	Falco	k1gMnSc1	Falco
peregrinus	peregrinus	k1gMnSc1	peregrinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zde	zde	k6eAd1	zde
pravidelně	pravidelně	k6eAd1	pravidelně
hnízdil	hnízdit	k5eAaImAgInS	hnízdit
asi	asi	k9	asi
do	do	k7c2	do
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
jeho	jeho	k3xOp3gInPc1	jeho
počty	počet	k1gInPc1	počet
začaly	začít	k5eAaPmAgInP	začít
klesat	klesat	k5eAaImF	klesat
<g/>
,	,	kIx,	,
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
používání	používání	k1gNnSc3	používání
nadměrného	nadměrný	k2eAgNnSc2d1	nadměrné
množství	množství	k1gNnSc2	množství
pesticidů	pesticid	k1gInPc2	pesticid
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989-1996	[number]	k4	1989-1996
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
jeho	jeho	k3xOp3gFnSc1	jeho
reintrodukce	reintrodukce	k1gFnSc1	reintrodukce
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Saském	saský	k2eAgNnSc6d1	Saské
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
zpět	zpět	k6eAd1	zpět
i	i	k9	i
do	do	k7c2	do
Českého	český	k2eAgNnSc2d1	české
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
savců	savec	k1gMnPc2	savec
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pravidelně	pravidelně	k6eAd1	pravidelně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
plch	plch	k1gMnSc1	plch
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Myoxus	Myoxus	k1gInSc1	Myoxus
glis	glis	k1gInSc1	glis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
plch	plch	k1gMnSc1	plch
zahradní	zahradní	k2eAgMnSc1d1	zahradní
(	(	kIx(	(
<g/>
Eliomys	Eliomys	k1gInSc1	Eliomys
quercinus	quercinus	k1gInSc1	quercinus
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
kuna	kuna	k1gFnSc1	kuna
skalní	skalní	k2eAgFnSc1d1	skalní
(	(	kIx(	(
<g/>
Martes	Martes	k1gInSc1	Martes
foina	foin	k1gInSc2	foin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veverka	veverka	k1gFnSc1	veverka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Sciurus	Sciurus	k1gInSc1	Sciurus
vulgaris	vulgaris	k1gFnSc2	vulgaris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
netopýr	netopýr	k1gMnSc1	netopýr
rezavý	rezavý	k2eAgMnSc1d1	rezavý
(	(	kIx(	(
<g/>
Nyctalus	Nyctalus	k1gInSc1	Nyctalus
noctula	noctulum	k1gNnSc2	noctulum
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
skalních	skalní	k2eAgFnPc6d1	skalní
štěrbinách	štěrbina	k1gFnPc6	štěrbina
netopýr	netopýr	k1gMnSc1	netopýr
hvízdavý	hvízdavý	k2eAgMnSc1d1	hvízdavý
(	(	kIx(	(
<g/>
Pipistrellus	Pipistrellus	k1gMnSc1	Pipistrellus
pipistrellus	pipistrellus	k1gMnSc1	pipistrellus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
třídy	třída	k1gFnSc2	třída
plazů	plaz	k1gMnPc2	plaz
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vzácně	vzácně	k6eAd1	vzácně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ještěrka	ještěrka	k1gFnSc1	ještěrka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Lacerta	Lacerta	k1gFnSc1	Lacerta
agilis	agilis	k1gFnSc2	agilis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
pískovcová	pískovcový	k2eAgFnSc1d1	pískovcová
skalní	skalní	k2eAgFnSc1d1	skalní
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
chráněny	chráněn	k2eAgInPc1d1	chráněn
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
okolní	okolní	k2eAgInPc4d1	okolní
masivy	masiv	k1gInPc4	masiv
a	a	k8xC	a
přirozená	přirozený	k2eAgFnSc1d1	přirozená
skladba	skladba	k1gFnSc1	skladba
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Rozpad	rozpad	k1gInSc1	rozpad
brány	brána	k1gFnSc2	brána
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
nevyhnutelný	vyhnutelný	k2eNgInSc1d1	nevyhnutelný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
již	již	k6eAd1	již
samotným	samotný	k2eAgInSc7d1	samotný
principem	princip	k1gInSc7	princip
jejího	její	k3xOp3gInSc2	její
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zachování	zachování	k1gNnSc4	zachování
současného	současný	k2eAgInSc2d1	současný
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
přirozeného	přirozený	k2eAgInSc2d1	přirozený
vývoje	vývoj	k1gInSc2	vývoj
pískovcových	pískovcový	k2eAgInPc2d1	pískovcový
skalních	skalní	k2eAgInPc2d1	skalní
masivů	masiv	k1gInPc2	masiv
a	a	k8xC	a
minimalizace	minimalizace	k1gFnSc1	minimalizace
negativního	negativní	k2eAgNnSc2d1	negativní
antropogenního	antropogenní	k2eAgNnSc2d1	antropogenní
působení	působení	k1gNnSc2	působení
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
návštěvníků	návštěvník	k1gMnPc2	návštěvník
způsoboval	způsobovat	k5eAaImAgInS	způsobovat
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
erozi	eroze	k1gFnSc4	eroze
vrchních	vrchní	k2eAgFnPc2d1	vrchní
partií	partie	k1gFnPc2	partie
brány	brána	k1gFnSc2	brána
a	a	k8xC	a
odnos	odnos	k1gInSc1	odnos
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
období	období	k1gNnSc4	období
turistického	turistický	k2eAgNnSc2d1	turistické
využívání	využívání	k1gNnSc2	využívání
brány	brána	k1gFnSc2	brána
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
oblouku	oblouk	k1gInSc2	oblouk
odnesena	odnesen	k2eAgFnSc1d1	odnesena
vrstva	vrstva	k1gFnSc1	vrstva
o	o	k7c6	o
tloušťce	tloušťka	k1gFnSc6	tloušťka
60	[number]	k4	60
až	až	k9	až
80	[number]	k4	80
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
bylo	být	k5eAaImAgNnS	být
navíc	navíc	k6eAd1	navíc
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
objeveno	objevit	k5eAaPmNgNnS	objevit
několik	několik	k4yIc4	několik
prasklin	prasklina	k1gFnPc2	prasklina
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
vstup	vstup	k1gInSc1	vstup
na	na	k7c4	na
Pravčickou	Pravčický	k2eAgFnSc4d1	Pravčická
bránu	brána	k1gFnSc4	brána
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zvětrávání	zvětrávání	k1gNnSc6	zvětrávání
pískovce	pískovec	k1gInSc2	pískovec
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
několik	několik	k4yIc1	několik
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Pískovcový	pískovcový	k2eAgInSc1d1	pískovcový
materiál	materiál	k1gInSc1	materiál
Pravčické	Pravčický	k2eAgFnSc2d1	Pravčická
brány	brána	k1gFnSc2	brána
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
horniny	hornina	k1gFnPc4	hornina
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
pevností	pevnost	k1gFnSc7	pevnost
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
brána	brána	k1gFnSc1	brána
nachází	nacházet	k5eAaImIp3nS	nacházet
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
exponováno	exponován	k2eAgNnSc1d1	exponováno
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
se	se	k3xPyFc4	se
na	na	k7c6	na
zvětrávání	zvětrávání	k1gNnSc6	zvětrávání
podílí	podílet	k5eAaImIp3nS	podílet
klima	klima	k1gNnSc1	klima
-	-	kIx~	-
intenzita	intenzita	k1gFnSc1	intenzita
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
teploty	teplota	k1gFnPc1	teplota
<g/>
,	,	kIx,	,
sluneční	sluneční	k2eAgNnSc1d1	sluneční
záření	záření	k1gNnSc1	záření
nebo	nebo	k8xC	nebo
mrazové	mrazový	k2eAgNnSc1d1	mrazové
zvětrávání	zvětrávání	k1gNnSc1	zvětrávání
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
ho	on	k3xPp3gMnSc4	on
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
činnost	činnost	k1gFnSc4	činnost
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Přímý	přímý	k2eAgInSc1d1	přímý
antropogenní	antropogenní	k2eAgInSc1d1	antropogenní
vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
díky	díky	k7c3	díky
zákazu	zákaz	k1gInSc3	zákaz
vstupu	vstup	k1gInSc2	vstup
minimalizován	minimalizovat	k5eAaBmNgInS	minimalizovat
<g/>
,	,	kIx,	,
nepřímo	přímo	k6eNd1	přímo
je	být	k5eAaImIp3nS	být
hornina	hornina	k1gFnSc1	hornina
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vypouštění	vypouštění	k1gNnSc2	vypouštění
antropogenních	antropogenní	k2eAgFnPc2d1	antropogenní
emisí	emise	k1gFnPc2	emise
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
(	(	kIx(	(
<g/>
urychlují	urychlovat	k5eAaImIp3nP	urychlovat
proces	proces	k1gInSc4	proces
zvětrávání	zvětrávání	k1gNnSc2	zvětrávání
<g/>
)	)	kIx)	)
a	a	k8xC	a
především	především	k9	především
kyselé	kyselý	k2eAgFnSc2d1	kyselá
atmosférické	atmosférický	k2eAgFnSc2d1	atmosférická
depozice	depozice	k1gFnSc2	depozice
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
brány	brána	k1gFnSc2	brána
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
tvorbu	tvorba	k1gFnSc4	tvorba
tzv.	tzv.	kA	tzv.
solných	solný	k2eAgInPc2d1	solný
výkvětů	výkvět	k1gInPc2	výkvět
(	(	kIx(	(
<g/>
krystalizované	krystalizovaný	k2eAgFnPc4d1	krystalizovaná
soli	sůl	k1gFnPc4	sůl
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
neutralizací	neutralizace	k1gFnSc7	neutralizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
horninou	hornina	k1gFnSc7	hornina
drolí	drolit	k5eAaImIp3nP	drolit
a	a	k8xC	a
opadávají	opadávat	k5eAaImIp3nP	opadávat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
také	také	k6eAd1	také
porušují	porušovat	k5eAaImIp3nP	porušovat
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
opálovou	opálový	k2eAgFnSc4d1	opálová
krustu	krusta	k1gFnSc4	krusta
pískovce	pískovec	k1gInSc2	pískovec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pozitivně	pozitivně	k6eAd1	pozitivně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
jeho	jeho	k3xOp3gFnSc4	jeho
soudržnost	soudržnost	k1gFnSc4	soudržnost
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobé	dlouhodobý	k2eAgFnPc1d1	dlouhodobá
antropogenní	antropogenní	k2eAgFnPc1d1	antropogenní
emise	emise	k1gFnPc1	emise
také	také	k9	také
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
zapříčinily	zapříčinit	k5eAaPmAgFnP	zapříčinit
tmavě	tmavě	k6eAd1	tmavě
šedé	šedý	k2eAgFnPc1d1	šedá
až	až	k8xS	až
černé	černý	k2eAgNnSc1d1	černé
zabarvení	zabarvení	k1gNnSc1	zabarvení
pískovce	pískovec	k1gInSc2	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Pravčické	Pravčický	k2eAgFnSc6d1	Pravčická
bráně	brána	k1gFnSc6	brána
je	být	k5eAaImIp3nS	být
pravidelně	pravidelně	k6eAd1	pravidelně
prováděn	prováděn	k2eAgInSc4d1	prováděn
monitoring	monitoring	k1gInSc4	monitoring
zahrnující	zahrnující	k2eAgFnPc4d1	zahrnující
různá	různý	k2eAgNnPc4d1	různé
měření	měření	k1gNnPc4	měření
<g/>
.	.	kIx.	.
</s>
<s>
Sleduje	sledovat	k5eAaImIp3nS	sledovat
se	se	k3xPyFc4	se
vnější	vnější	k2eAgFnSc1d1	vnější
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
náklon	náklon	k1gInSc1	náklon
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc1	způsob
a	a	k8xC	a
míra	míra	k1gFnSc1	míra
porušení	porušení	k1gNnSc2	porušení
pískovce	pískovec	k1gInSc2	pískovec
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
složení	složení	k1gNnSc1	složení
<g/>
,	,	kIx,	,
rozložení	rozložení	k1gNnSc1	rozložení
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
,	,	kIx,	,
případné	případný	k2eAgNnSc4d1	případné
druhotné	druhotný	k2eAgNnSc4d1	druhotné
zpevnění	zpevnění	k1gNnSc4	zpevnění
(	(	kIx(	(
<g/>
proželeznění	proželeznění	k1gNnSc2	proželeznění
či	či	k8xC	či
prokřemenění	prokřemenění	k1gNnSc2	prokřemenění
<g/>
)	)	kIx)	)
horniny	hornina	k1gFnSc2	hornina
nebo	nebo	k8xC	nebo
její	její	k3xOp3gNnSc4	její
vratné	vratný	k2eAgNnSc4d1	vratné
rozpínání	rozpínání	k1gNnSc4	rozpínání
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
změn	změna	k1gFnPc2	změna
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
seizmického	seizmický	k2eAgInSc2d1	seizmický
průzkumu	průzkum	k1gInSc2	průzkum
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
snímači	snímač	k1gInSc3	snímač
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
geofony	geofon	k1gInPc1	geofon
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
geologickými	geologický	k2eAgInPc7d1	geologický
radary	radar	k1gInPc7	radar
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
vytvořeny	vytvořen	k2eAgInPc1d1	vytvořen
řezy	řez	k1gInPc1	řez
tělesem	těleso	k1gNnSc7	těleso
brány	brána	k1gFnPc1	brána
ukazující	ukazující	k2eAgFnSc1d1	ukazující
pevnost	pevnost	k1gFnSc4	pevnost
masivu	masiv	k1gInSc2	masiv
<g/>
,	,	kIx,	,
intenzitu	intenzita	k1gFnSc4	intenzita
zvětrávání	zvětrávání	k1gNnSc2	zvětrávání
nebo	nebo	k8xC	nebo
skryté	skrytý	k2eAgFnSc2d1	skrytá
trhliny	trhlina	k1gFnSc2	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
blízkosti	blízkost	k1gFnSc6	blízkost
brány	brána	k1gFnSc2	brána
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vysazen	vysazen	k2eAgInSc1d1	vysazen
nepůvodní	původní	k2eNgInSc1d1	nepůvodní
kaštanovník	kaštanovník	k1gInSc1	kaštanovník
setý	setý	k2eAgInSc1d1	setý
(	(	kIx(	(
<g/>
Castanea	Castane	k2eAgFnSc1d1	Castanea
sativa	sativa	k1gFnSc1	sativa
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
okrasná	okrasný	k2eAgFnSc1d1	okrasná
dřevina	dřevina	k1gFnSc1	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
bohužel	bohužel	k9	bohužel
dobře	dobře	k6eAd1	dobře
šíří	šířit	k5eAaImIp3nS	šířit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
jeho	jeho	k3xOp3gNnSc4	jeho
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
odstraňování	odstraňování	k1gNnSc4	odstraňování
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
nepůvodní	původní	k2eNgFnSc7d1	nepůvodní
dřevinou	dřevina	k1gFnSc7	dřevina
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
invazní	invazní	k2eAgFnSc1d1	invazní
borovice	borovice	k1gFnSc1	borovice
vejmutovka	vejmutovka	k1gFnSc1	vejmutovka
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gMnSc1	Pinus
strobus	strobus	k1gMnSc1	strobus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vysazovaná	vysazovaný	k2eAgFnSc1d1	vysazovaná
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
lesnickým	lesnický	k2eAgNnSc7d1	lesnické
využíváním	využívání	k1gNnSc7	využívání
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
častější	častý	k2eAgNnSc1d2	častější
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
území	území	k1gNnSc6	území
NPP	NPP	kA	NPP
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nutný	nutný	k2eAgInSc1d1	nutný
i	i	k8xC	i
její	její	k3xOp3gInSc4	její
monitoring	monitoring	k1gInSc4	monitoring
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
odstraňovány	odstraňován	k2eAgFnPc4d1	odstraňována
náletové	náletový	k2eAgFnPc4d1	náletová
dřeviny	dřevina	k1gFnPc4	dřevina
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
způsobit	způsobit	k5eAaPmF	způsobit
její	její	k3xOp3gNnSc4	její
poškození	poškození	k1gNnSc4	poškození
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
přístupových	přístupový	k2eAgFnPc2d1	přístupová
a	a	k8xC	a
vyhlídkových	vyhlídkový	k2eAgFnPc2d1	vyhlídková
cest	cesta	k1gFnPc2	cesta
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
k	k	k7c3	k
Pravčické	Pravčický	k2eAgFnSc3d1	Pravčická
bráně	brána	k1gFnSc3	brána
dostat	dostat	k5eAaPmF	dostat
dvěma	dva	k4xCgFnPc7	dva
přístupovými	přístupový	k2eAgFnPc7d1	přístupová
cestami	cesta	k1gFnPc7	cesta
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
lze	lze	k6eAd1	lze
absolvovat	absolvovat	k5eAaPmF	absolvovat
pouze	pouze	k6eAd1	pouze
pěšky	pěšky	k6eAd1	pěšky
<g/>
.	.	kIx.	.
</s>
<s>
Kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zato	zato	k6eAd1	zato
náročnější	náročný	k2eAgMnSc1d2	náročnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
červené	červený	k2eAgFnSc6d1	červená
turistické	turistický	k2eAgFnSc6d1	turistická
značce	značka	k1gFnSc6	značka
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
Hřensko	Hřensko	k1gNnSc1	Hřensko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
cestě	cesta	k1gFnSc6	cesta
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
u	u	k7c2	u
křižovatky	křižovatka	k1gFnSc2	křižovatka
Tři	tři	k4xCgInPc4	tři
prameny	pramen	k1gInPc4	pramen
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
lesní	lesní	k2eAgFnSc7d1	lesní
cestou	cesta	k1gFnSc7	cesta
2	[number]	k4	2
kilometry	kilometr	k1gInPc4	kilometr
stoupá	stoupat	k5eAaImIp3nS	stoupat
Dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
dolem	dol	k1gInSc7	dol
až	až	k9	až
k	k	k7c3	k
bráně	brána	k1gFnSc3	brána
<g/>
.	.	kIx.	.
</s>
<s>
Delší	dlouhý	k2eAgFnSc1d2	delší
Gabrielina	Gabrielin	k2eAgFnSc1d1	Gabrielina
stezka	stezka	k1gFnSc1	stezka
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c4	v
asi	asi	k9	asi
2	[number]	k4	2
kilometry	kilometr	k1gInPc4	kilometr
vzdálené	vzdálený	k2eAgFnSc3d1	vzdálená
obci	obec	k1gFnSc3	obec
Mezní	mezní	k2eAgFnSc1d1	mezní
Louka	louka	k1gFnSc1	louka
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
úpatím	úpatí	k1gNnSc7	úpatí
Křídelních	křídelní	k2eAgFnPc2d1	křídelní
stěn	stěna	k1gFnPc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
druhá	druhý	k4xOgFnSc1	druhý
varianta	varianta	k1gFnSc1	varianta
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
náročná	náročný	k2eAgFnSc1d1	náročná
díky	díky	k7c3	díky
mírnějšímu	mírný	k2eAgNnSc3d2	mírnější
stoupání	stoupání	k1gNnSc3	stoupání
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc4	její
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
6,5	[number]	k4	6,5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Horolezectví	horolezectví	k1gNnSc1	horolezectví
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Českosaském	českosaský	k2eAgNnSc6d1	Českosaské
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
sahající	sahající	k2eAgMnSc1d1	sahající
do	do	k7c2	do
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
horolezeckém	horolezecký	k2eAgInSc6d1	horolezecký
výstupu	výstup	k1gInSc6	výstup
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
brány	brána	k1gFnSc2	brána
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgInPc1	žádný
doklady	doklad	k1gInPc1	doklad
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
pochopitelně	pochopitelně	k6eAd1	pochopitelně
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
MŽP	MŽP	kA	MŽP
udělilo	udělit	k5eAaPmAgNnS	udělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
Českému	český	k2eAgInSc3d1	český
horolezeckému	horolezecký	k2eAgInSc3d1	horolezecký
svazu	svaz	k1gInSc3	svaz
výjimku	výjimek	k1gInSc2	výjimek
umožňující	umožňující	k2eAgMnSc1d1	umožňující
provozovat	provozovat	k5eAaImF	provozovat
horolezectví	horolezectví	k1gNnSc3	horolezectví
na	na	k7c6	na
čtyřech	čtyři	k4xCgNnPc6	čtyři
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
využívaných	využívaný	k2eAgFnPc6d1	využívaná
okolních	okolní	k2eAgFnPc6d1	okolní
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
Staré	Staré	k2eAgFnSc6d1	Staré
Václavské	václavský	k2eAgFnSc6d1	Václavská
stěně	stěna	k1gFnSc6	stěna
<g/>
,	,	kIx,	,
Malém	malý	k2eAgInSc6d1	malý
Pravčickém	Pravčický	k2eAgInSc6d1	Pravčický
kuželu	kužel	k1gInSc6	kužel
<g/>
,	,	kIx,	,
Václavské	václavský	k2eAgFnSc6d1	Václavská
věži	věž	k1gFnSc6	věž
a	a	k8xC	a
Křížové	Křížové	k2eAgFnSc6d1	Křížové
věži	věž	k1gFnSc6	věž
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
České	český	k2eAgFnSc2d1	Česká
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
Malá	malý	k2eAgFnSc1d1	malá
Pravčická	Pravčická	k1gFnSc1	Pravčická
brána	brán	k2eAgFnSc1d1	brána
Sokolí	sokolí	k2eAgNnSc4d1	sokolí
hnízdo	hnízdo	k1gNnSc4	hnízdo
</s>
