<s>
Dalšími	další	k2eAgInPc7d1	další
znaky	znak	k1gInPc7	znak
netopýrů	netopýr	k1gMnPc2	netopýr
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
je	on	k3xPp3gNnSc4	on
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
kaloňů	kaloň	k1gMnPc2	kaloň
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
echolokace	echolokace	k1gFnSc1	echolokace
–	–	k?	–
schopnost	schopnost	k1gFnSc4	schopnost
navigovat	navigovat	k5eAaImF	navigovat
se	se	k3xPyFc4	se
vlastním	vlastní	k2eAgInSc7d1	vlastní
sluchem	sluch	k1gInSc7	sluch
podle	podle	k7c2	podle
odrazů	odraz	k1gInPc2	odraz
zvuku	zvuk	k1gInSc2	zvuk
u	u	k7c2	u
jejich	jejich	k3xOp3gInSc2	jejich
pískotu	pískot	k1gInSc2	pískot
od	od	k7c2	od
předmětů	předmět	k1gInPc2	předmět
(	(	kIx(	(
<g/>
především	především	k9	především
kořisti	kořist	k1gFnSc2	kořist
<g/>
)	)	kIx)	)
–	–	k?	–
podobně	podobně	k6eAd1	podobně
funguje	fungovat	k5eAaImIp3nS	fungovat
sonar	sonar	k1gInSc1	sonar
<g/>
.	.	kIx.	.
</s>
