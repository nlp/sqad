<s>
Netopýři	netopýr	k1gMnPc1	netopýr
(	(	kIx(	(
<g/>
Microchiroptera	Microchiropter	k1gMnSc2	Microchiropter
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
podřád	podřád	k1gInSc4	podřád
savců	savec	k1gMnPc2	savec
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
letouni	letoun	k1gMnPc1	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
znakem	znak	k1gInSc7	znak
netopýrů	netopýr	k1gMnPc2	netopýr
jsou	být	k5eAaImIp3nP	být
křídla	křídlo	k1gNnPc4	křídlo
vyvinuvší	vyvinuvší	k2eAgFnPc1d1	vyvinuvší
se	se	k3xPyFc4	se
z	z	k7c2	z
předních	přední	k2eAgFnPc2d1	přední
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nim	on	k3xPp3gMnPc3	on
jsou	být	k5eAaImIp3nP	být
netopýři	netopýr	k1gMnPc1	netopýr
(	(	kIx(	(
<g/>
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
podřád	podřád	k1gInSc1	podřád
letounů	letoun	k1gInPc2	letoun
<g/>
,	,	kIx,	,
kaloni	kaloň	k1gMnPc1	kaloň
<g/>
)	)	kIx)	)
jedinými	jediný	k2eAgMnPc7d1	jediný
savci	savec	k1gMnPc7	savec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dokážou	dokázat	k5eAaPmIp3nP	dokázat
létat	létat	k5eAaImF	létat
–	–	k?	–
létající	létající	k2eAgFnPc1d1	létající
veverky	veverka	k1gFnPc1	veverka
<g/>
,	,	kIx,	,
vakoveverky	vakoveverka	k1gFnPc1	vakoveverka
a	a	k8xC	a
plachtící	plachtící	k2eAgMnPc1d1	plachtící
kuskusovití	kuskusovitý	k2eAgMnPc1d1	kuskusovitý
mohou	moct	k5eAaImIp3nP	moct
pouze	pouze	k6eAd1	pouze
klouzat	klouzat	k5eAaImF	klouzat
na	na	k7c4	na
omezenou	omezený	k2eAgFnSc4d1	omezená
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Vynikají	vynikat	k5eAaImIp3nP	vynikat
akrobatickým	akrobatický	k2eAgInSc7d1	akrobatický
letem	let	k1gInSc7	let
o	o	k7c6	o
rychlosti	rychlost	k1gFnSc6	rychlost
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Netopýři	netopýr	k1gMnPc1	netopýr
se	se	k3xPyFc4	se
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
60	[number]	k4	60
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
z	z	k7c2	z
primitivních	primitivní	k2eAgMnPc2d1	primitivní
hmyzožravců	hmyzožravec	k1gMnPc2	hmyzožravec
<g/>
,	,	kIx,	,
předchůdců	předchůdce	k1gMnPc2	předchůdce
dnešních	dnešní	k2eAgMnPc2d1	dnešní
ježků	ježek	k1gMnPc2	ježek
a	a	k8xC	a
rejsků	rejsek	k1gMnPc2	rejsek
<g/>
.	.	kIx.	.
</s>
<s>
Netopýři	netopýr	k1gMnPc1	netopýr
vidí	vidět	k5eAaImIp3nP	vidět
černobíle	černobíle	k6eAd1	černobíle
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
znaky	znak	k1gInPc7	znak
netopýrů	netopýr	k1gMnPc2	netopýr
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
je	on	k3xPp3gNnSc4	on
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
kaloňů	kaloň	k1gMnPc2	kaloň
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
echolokace	echolokace	k1gFnSc1	echolokace
–	–	k?	–
schopnost	schopnost	k1gFnSc4	schopnost
navigovat	navigovat	k5eAaImF	navigovat
se	se	k3xPyFc4	se
vlastním	vlastní	k2eAgInSc7d1	vlastní
sluchem	sluch	k1gInSc7	sluch
podle	podle	k7c2	podle
odrazů	odraz	k1gInPc2	odraz
zvuku	zvuk	k1gInSc2	zvuk
u	u	k7c2	u
jejich	jejich	k3xOp3gInSc2	jejich
pískotu	pískot	k1gInSc2	pískot
od	od	k7c2	od
předmětů	předmět	k1gInPc2	předmět
(	(	kIx(	(
<g/>
především	především	k9	především
kořisti	kořist	k1gFnSc2	kořist
<g/>
)	)	kIx)	)
–	–	k?	–
podobně	podobně	k6eAd1	podobně
funguje	fungovat	k5eAaImIp3nS	fungovat
sonar	sonar	k1gInSc1	sonar
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc4d3	nejnižší
frekvenci	frekvence	k1gFnSc4	frekvence
má	mít	k5eAaImIp3nS	mít
netopýr	netopýr	k1gMnSc1	netopýr
pestrý	pestrý	k2eAgMnSc1d1	pestrý
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
20	[number]	k4	20
kHz	khz	kA	khz
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
frekvenci	frekvence	k1gFnSc4	frekvence
má	mít	k5eAaImIp3nS	mít
vrápenec	vrápenec	k1gMnSc1	vrápenec
malý	malý	k2eAgMnSc1d1	malý
až	až	k6eAd1	až
kolem	kolem	k7c2	kolem
110	[number]	k4	110
kHz	khz	kA	khz
<g/>
.	.	kIx.	.
chybějící	chybějící	k2eAgInSc1d1	chybějící
dráp	dráp	k1gInSc1	dráp
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
prstu	prst	k1gInSc6	prst
přední	přední	k2eAgFnSc2d1	přední
končetiny	končetina	k1gFnSc2	končetina
jejich	jejich	k3xOp3gNnPc1	jejich
uši	ucho	k1gNnPc1	ucho
netvoří	tvořit	k5eNaImIp3nP	tvořit
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
kruh	kruh	k1gInSc4	kruh
mají	mít	k5eAaImIp3nP	mít
ostré	ostrý	k2eAgInPc1d1	ostrý
zuby	zub	k1gInPc1	zub
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
stoličky	stolička	k1gFnPc1	stolička
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
menších	malý	k2eAgInPc2d2	menší
rozměrů	rozměr	k1gInPc2	rozměr
stehenní	stehenní	k2eAgFnSc2d1	stehenní
kosti	kost	k1gFnSc2	kost
netopýra	netopýr	k1gMnSc2	netopýr
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
tenké	tenký	k2eAgInPc1d1	tenký
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
netopýrů	netopýr	k1gMnPc2	netopýr
neumí	umět	k5eNaImIp3nS	umět
chodit	chodit	k5eAaImF	chodit
Netopýři	netopýr	k1gMnPc1	netopýr
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
krví	krev	k1gFnSc7	krev
<g/>
,	,	kIx,	,
malými	malý	k2eAgInPc7d1	malý
savci	savec	k1gMnPc7	savec
<g/>
,	,	kIx,	,
žábami	žába	k1gFnPc7	žába
a	a	k8xC	a
rybami	ryba	k1gFnPc7	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
netopýrů	netopýr	k1gMnPc2	netopýr
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
nebo	nebo	k8xC	nebo
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
malé	malý	k2eAgFnPc1d1	malá
a	a	k8xC	a
špatně	špatně	k6eAd1	špatně
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
netopýři	netopýr	k1gMnPc1	netopýr
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
tzv.	tzv.	kA	tzv.
utajené	utajený	k2eAgFnSc2d1	utajená
březosti	březost	k1gFnSc2	březost
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
pozastavit	pozastavit	k5eAaPmF	pozastavit
vývoj	vývoj	k1gInSc4	vývoj
plodu	plod	k1gInSc2	plod
a	a	k8xC	a
odložit	odložit	k5eAaPmF	odložit
jeho	jeho	k3xOp3gInSc4	jeho
porod	porod	k1gInSc4	porod
i	i	k8xC	i
vývoj	vývoj	k1gInSc4	vývoj
na	na	k7c4	na
příznivější	příznivý	k2eAgNnSc4d2	příznivější
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Netopýří	netopýří	k2eAgInSc1d1	netopýří
radar	radar	k1gInSc1	radar
neboli	neboli	k8xC	neboli
echolokace	echolokace	k1gFnSc1	echolokace
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc4	způsob
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
echolokace	echolokace	k1gFnSc2	echolokace
je	být	k5eAaImIp3nS	být
echo	echo	k1gNnSc1	echo
(	(	kIx(	(
<g/>
ozvěna	ozvěna	k1gFnSc1	ozvěna
<g/>
)	)	kIx)	)
a	a	k8xC	a
lokace	lokace	k1gFnSc1	lokace
(	(	kIx(	(
<g/>
zjišťování	zjišťování	k1gNnSc1	zjišťování
místa	místo	k1gNnSc2	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
radar	radar	k1gInSc1	radar
(	(	kIx(	(
<g/>
sonar	sonar	k1gInSc1	sonar
<g/>
)	)	kIx)	)
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
ultrazvukových	ultrazvukový	k2eAgFnPc2d1	ultrazvuková
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
signál	signál	k1gInSc1	signál
je	být	k5eAaImIp3nS	být
standardizovaným	standardizovaný	k2eAgInSc7d1	standardizovaný
zvukovým	zvukový	k2eAgInSc7d1	zvukový
pulsem	puls	k1gInSc7	puls
frekvence	frekvence	k1gFnSc1	frekvence
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
110	[number]	k4	110
kHz	khz	kA	khz
(	(	kIx(	(
<g/>
kilohertzů	kilohertz	k1gInPc2	kilohertz
<g/>
,	,	kIx,	,
14	[number]	k4	14
000-110	[number]	k4	000-110
000	[number]	k4	000
hertzů	hertz	k1gInPc2	hertz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délky	délka	k1gFnSc2	délka
0,7	[number]	k4	0,7
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
ms	ms	k?	ms
(	(	kIx(	(
<g/>
milisekund	milisekunda	k1gFnPc2	milisekunda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opakovaný	opakovaný	k2eAgInSc4d1	opakovaný
5-150	[number]	k4	5-150
krát	krát	k6eAd1	krát
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Intenzita	intenzita	k1gFnSc1	intenzita
zvuku	zvuk	k1gInSc2	zvuk
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
40-150	[number]	k4	40-150
dB	db	kA	db
(	(	kIx(	(
<g/>
decibelů	decibel	k1gInPc2	decibel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc1	tlak
zvukové	zvukový	k2eAgFnSc2d1	zvuková
vlny	vlna	k1gFnSc2	vlna
0,1	[number]	k4	0,1
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
Pa	Pa	kA	Pa
(	(	kIx(	(
<g/>
pascalů	pascal	k1gInPc2	pascal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
0,6	[number]	k4	0,6
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Frekvenční	frekvenční	k2eAgInSc1d1	frekvenční
rozsah	rozsah	k1gInSc1	rozsah
zvuku	zvuk	k1gInSc2	zvuk
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
kHz	khz	kA	khz
(	(	kIx(	(
<g/>
kilohertzů	kilohertz	k1gInPc2	kilohertz
<g/>
,	,	kIx,	,
0-60	[number]	k4	0-60
000	[number]	k4	000
hertzů	hertz	k1gInPc2	hertz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
charakteristikou	charakteristika	k1gFnSc7	charakteristika
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
harmonických	harmonický	k2eAgInPc2d1	harmonický
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Vydávají	vydávat	k5eAaImIp3nP	vydávat
2-4	[number]	k4	2-4
rovnoměrné	rovnoměrný	k2eAgInPc4d1	rovnoměrný
harmonické	harmonický	k2eAgInPc4d1	harmonický
tóny	tón	k1gInPc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
Signály	signál	k1gInPc1	signál
vydávají	vydávat	k5eAaImIp3nP	vydávat
pootevřenými	pootevřený	k2eAgNnPc7d1	pootevřené
ústy	ústa	k1gNnPc7	ústa
a	a	k8xC	a
z	z	k7c2	z
části	část	k1gFnSc2	část
i	i	k9	i
nosem	nos	k1gInSc7	nos
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
tón	tón	k1gInSc1	tón
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vypustí	vypustit	k5eAaPmIp3nS	vypustit
začíná	začínat	k5eAaImIp3nS	začínat
vysoko	vysoko	k6eAd1	vysoko
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
o	o	k7c4	o
oktávu	oktáva	k1gFnSc4	oktáva
níž	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
rozsah	rozsah	k1gInSc1	rozsah
slyšitelnosti	slyšitelnost	k1gFnSc2	slyšitelnost
je	být	k5eAaImIp3nS	být
0,2	[number]	k4	0,2
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
kHz	khz	kA	khz
(	(	kIx(	(
<g/>
kilohertzů	kilohertz	k1gInPc2	kilohertz
<g/>
,	,	kIx,	,
200-200000	[number]	k4	200-200000
hertzů	hertz	k1gInPc2	hertz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prahová	prahový	k2eAgFnSc1d1	prahová
citlivost	citlivost	k1gFnSc1	citlivost
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
frekvence	frekvence	k1gFnPc4	frekvence
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
zpravidla	zpravidla	k6eAd1	zpravidla
mezi	mezi	k7c7	mezi
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
kHz	khz	kA	khz
(	(	kIx(	(
<g/>
kilohertzů	kilohertz	k1gInPc2	kilohertz
<g/>
,	,	kIx,	,
20000-120000	[number]	k4	20000-120000
hertzů	hertz	k1gInPc2	hertz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Netopýři	netopýr	k1gMnPc1	netopýr
mohou	moct	k5eAaImIp3nP	moct
rozlišit	rozlišit	k5eAaPmF	rozlišit
dva	dva	k4xCgInPc4	dva
stimuly	stimul	k1gInPc4	stimul
časově	časově	k6eAd1	časově
oddělené	oddělený	k2eAgInPc4d1	oddělený
pouhými	pouhý	k2eAgInPc7d1	pouhý
0,3	[number]	k4	0,3
ms	ms	k?	ms
(	(	kIx(	(
<g/>
milisekund	milisekunda	k1gFnPc2	milisekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
našich	náš	k3xOp1gMnPc2	náš
netopýrů	netopýr	k1gMnPc2	netopýr
(	(	kIx(	(
<g/>
a	a	k8xC	a
blízce	blízce	k6eAd1	blízce
příbuzných	příbuzný	k2eAgMnPc2d1	příbuzný
vrápenců	vrápenec	k1gMnPc2	vrápenec
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
chráněné	chráněný	k2eAgInPc4d1	chráněný
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
až	až	k9	až
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
Úbytek	úbytek	k1gInSc1	úbytek
stromů	strom	k1gInPc2	strom
s	s	k7c7	s
dutinami	dutina	k1gFnPc7	dutina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
odpočívat	odpočívat	k5eAaImF	odpočívat
<g/>
,	,	kIx,	,
používání	používání	k1gNnSc1	používání
pesticidů	pesticid	k1gInPc2	pesticid
a	a	k8xC	a
herbicidů	herbicid	k1gInPc2	herbicid
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	on	k3xPp3gFnPc4	on
systematicky	systematicky	k6eAd1	systematicky
otravují	otravovat	k5eAaImIp3nP	otravovat
<g/>
.	.	kIx.	.
</s>
<s>
Vyrušování	vyrušování	k1gNnSc1	vyrušování
při	při	k7c6	při
zimování	zimování	k1gNnSc6	zimování
<g/>
.	.	kIx.	.
</s>
<s>
Netopýrům	netopýr	k1gMnPc3	netopýr
můžeme	moct	k5eAaImIp1nP	moct
pomoci	pomoct	k5eAaPmF	pomoct
i	i	k9	i
na	na	k7c6	na
vlastní	vlastní	k2eAgFnSc6d1	vlastní
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
se	se	k3xPyFc4	se
odvděčí	odvděčit	k5eAaPmIp3nS	odvděčit
likvidací	likvidace	k1gFnSc7	likvidace
přemnoženého	přemnožený	k2eAgInSc2d1	přemnožený
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
Nepoužívat	používat	k5eNaImF	používat
chemikálie	chemikálie	k1gFnPc4	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
letní	letní	k2eAgFnSc1d1	letní
kolonie	kolonie	k1gFnSc1	kolonie
usadí	usadit	k5eAaPmIp3nS	usadit
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
či	či	k8xC	či
ve	v	k7c6	v
stodole	stodola	k1gFnSc6	stodola
<g/>
,	,	kIx,	,
ponechat	ponechat	k5eAaPmF	ponechat
jí	on	k3xPp3gFnSc3	on
stále	stále	k6eAd1	stále
volný	volný	k2eAgInSc1d1	volný
průlet	průlet	k1gInSc1	průlet
ven	ven	k6eAd1	ven
i	i	k9	i
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
Vyvěšovat	vyvěšovat	k5eAaImF	vyvěšovat
netopýří	netopýří	k2eAgFnPc4d1	netopýří
budky	budka	k1gFnPc4	budka
čili	čili	k8xC	čili
netopýrovníky	netopýrovník	k1gInPc4	netopýrovník
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zateplování	zateplování	k1gNnSc6	zateplování
či	či	k8xC	či
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
budovy	budova	k1gFnSc2	budova
netopýrům	netopýr	k1gMnPc3	netopýr
neublížit	ublížit	k5eNaPmF	ublížit
<g/>
.	.	kIx.	.
</s>
<s>
Řešení	řešení	k1gNnSc1	řešení
je	být	k5eAaImIp3nS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
,	,	kIx,	,
návod	návod	k1gInSc1	návod
i	i	k8xC	i
mnohé	mnohý	k2eAgInPc1d1	mnohý
příklady	příklad	k1gInPc1	příklad
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
např.	např.	kA	např.
na	na	k7c6	na
http://www.sousednetopyr.cz/	[url]	k?	http://www.sousednetopyr.cz/
Pokud	pokud	k8xS	pokud
netopýry	netopýr	k1gMnPc7	netopýr
objevíme	objevit	k5eAaPmIp1nP	objevit
při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
pro	pro	k7c4	pro
odbornou	odborný	k2eAgFnSc4d1	odborná
pomoc	pomoc	k1gFnSc4	pomoc
zatelefonovat	zatelefonovat	k5eAaPmF	zatelefonovat
na	na	k7c4	na
netopýří	netopýří	k2eAgFnSc4d1	netopýří
linku	linka	k1gFnSc4	linka
(	(	kIx(	(
<g/>
737	[number]	k4	737
121	[number]	k4	121
672	[number]	k4	672
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
provozuje	provozovat	k5eAaImIp3nS	provozovat
záchrannou	záchranný	k2eAgFnSc7d1	záchranná
sos	sos	k1gInSc1	sos
netopýří	netopýří	k2eAgFnSc4d1	netopýří
linku	linka	k1gFnSc4	linka
ZO	ZO	kA	ZO
ČSOP	ČSOP	kA	ČSOP
Nyctalus	Nyctalus	k1gMnSc1	Nyctalus
(	(	kIx(	(
<g/>
731	[number]	k4	731
523	[number]	k4	523
599	[number]	k4	599
<g/>
,	,	kIx,	,
www.nyctalus.cz	www.nyctalus.cz	k1gInSc1	www.nyctalus.cz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
republice	republika	k1gFnSc6	republika
pomohou	pomoct	k5eAaPmIp3nP	pomoct
i	i	k9	i
záchranné	záchranný	k2eAgFnPc1d1	záchranná
stanice	stanice	k1gFnPc1	stanice
(	(	kIx(	(
<g/>
www.zvirevnouzi.cz	www.zvirevnouzi.cz	k1gInSc1	www.zvirevnouzi.cz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
