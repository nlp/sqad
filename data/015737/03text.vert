<s>
Székesfehérvár	Székesfehérvár	k1gMnSc1
</s>
<s>
Stoličný	stoličný	k1gMnSc1
Bělehrad	Bělehrad	k1gInSc1
Székesfehérvár	Székesfehérvár	k1gInSc1
Letecký	letecký	k2eAgInSc1d1
pohled	pohled	k1gInSc4
na	na	k7c4
centrum	centrum	k1gNnSc4
města	město	k1gNnSc2
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
47	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
22	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
39	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
118	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
+1	+1	k4
Stát	stát	k1gInSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
Maďarsko	Maďarsko	k1gNnSc1
Region	region	k1gInSc1
</s>
<s>
Střední	střední	k2eAgFnSc1d1
Zadunají	Zadunají	k1gFnSc1
Župa	župa	k1gFnSc1
</s>
<s>
Fejér	Fejér	k1gInSc1
Okres	okres	k1gInSc1
</s>
<s>
Székesfehérvár	Székesfehérvár	k1gMnSc1
</s>
<s>
Székesfehérvár	Székesfehérvár	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
170,89	170,89	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
96	#num#	k4
940	#num#	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
567,3	567,3	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Etnické	etnický	k2eAgNnSc4d1
složení	složení	k1gNnSc4
</s>
<s>
Maďaři	Maďar	k1gMnPc1
<g/>
,	,	kIx,
Romové	Rom	k1gMnPc1
<g/>
,	,	kIx,
Němci	Němec	k1gMnPc1
Náboženské	náboženský	k2eAgFnSc2d1
složení	složení	k1gNnSc3
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1
Správa	správa	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
Župní	župní	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
s	s	k7c7
župním	župní	k2eAgNnSc7d1
právem	právo	k1gNnSc7
Starosta	Starosta	k1gMnSc1
</s>
<s>
Tihamér	Tihamér	k1gInSc1
Warvasovszky	Warvasovszka	k1gFnSc2
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.szekesfehervar.hu	www.szekesfehervar.ha	k1gFnSc4
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
+	+	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
22	#num#	k4
PSČ	PSČ	kA
</s>
<s>
8000	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Székesfehérvár	Székesfehérvár	k1gInSc1
[	[	kIx(
<g/>
sékeš-	sékeš-	k?
<g/>
]	]	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Stoličný	stoličný	k1gMnSc1
Bělehrad	Bělehrad	k1gInSc1
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Alba	alba	k1gFnSc1
Regia	Regia	k1gFnSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Stuhlweißenburg	Stuhlweißenburg	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
s	s	k7c7
župním	župní	k2eAgNnSc7d1
právem	právo	k1gNnSc7
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
<g/>
,	,	kIx,
správní	správní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
župy	župa	k1gFnSc2
Fejér	Fejér	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
současně	současně	k6eAd1
i	i	k9
okresním	okresní	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Székesfehérváru	Székesfehérvár	k1gInSc6
žije	žít	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
97	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
170,89	170,89	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
Székesfehérvár	Székesfehérvár	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
maďarském	maďarský	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
složený	složený	k2eAgInSc1d1
ze	z	k7c2
tří	tři	k4xCgNnPc2
části	část	k1gFnSc6
<g/>
:	:	kIx,
Székes-fehér-vár	Székes-fehér-vár	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
znamená	znamenat	k5eAaImIp3nS
královský	královský	k2eAgMnSc1d1
a	a	k8xC
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
význam	význam	k1gInSc4
města	město	k1gNnSc2
jako	jako	k8xS,k8xC
prvního	první	k4xOgMnSc2
sídla	sídlo	k1gNnSc2
uherských	uherský	k2eAgMnPc2d1
králů	král	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovo	slovo	k1gNnSc1
fehér	fehér	k1gMnSc1
označuje	označovat	k5eAaImIp3nS
v	v	k7c6
maďarštině	maďarština	k1gFnSc6
barvu	barva	k1gFnSc4
bílou	bílý	k2eAgFnSc4d1
a	a	k8xC
slovo	slovo	k1gNnSc1
vár	vár	k?
potom	potom	k6eAd1
hrad	hrad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
název	název	k1gInSc1
rozdělen	rozdělit	k5eAaPmNgInS
pouze	pouze	k6eAd1
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
–	–	k?
<g/>
Székes-fehervár	Székes-fehervár	k1gInSc1
<g/>
,	,	kIx,
potom	potom	k6eAd1
byl	být	k5eAaImAgInS
překlad	překlad	k1gInSc1
druhé	druhý	k4xOgFnSc6
zněl	znět	k5eAaImAgInS
doslova	doslova	k6eAd1
Bělehrad	Bělehrad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
pochází	pocházet	k5eAaImIp3nS
i	i	k9
české	český	k2eAgNnSc1d1
toponymum	toponymum	k1gNnSc1
Stoličný	stoličný	k1gMnSc1
Bělehrad	Bělehrad	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
používáno	používat	k5eAaImNgNnS
v	v	k7c6
historických	historický	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
maďarský	maďarský	k2eAgInSc1d1
název	název	k1gInSc1
pro	pro	k7c4
srbskou	srbský	k2eAgFnSc4d1
metropoli	metropole	k1gFnSc4
Bělehrad	Bělehrad	k1gInSc1
zní	znět	k5eAaImIp3nS
Nándorfehérvár	Nándorfehérvár	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Existují	existovat	k5eAaImIp3nP
celkem	celkem	k6eAd1
tři	tři	k4xCgFnPc1
teorie	teorie	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
vysvětlují	vysvětlovat	k5eAaImIp3nP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
město	město	k1gNnSc1
přišlo	přijít	k5eAaPmAgNnS
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
současnému	současný	k2eAgInSc3d1
názvu	název	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílá	bílý	k2eAgFnSc1d1
barva	barva	k1gFnSc1
je	být	k5eAaImIp3nS
doložena	doložit	k5eAaPmNgFnS
v	v	k7c6
kronikách	kronika	k1gFnPc6
krále	král	k1gMnSc2
Ondřeje	Ondřej	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
případně	případně	k6eAd1
může	moct	k5eAaImIp3nS
odkazovat	odkazovat	k5eAaImF
na	na	k7c4
skutečně	skutečně	k6eAd1
použitou	použitý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
kamene	kámen	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgInSc2
byly	být	k5eAaImAgInP
postaveny	postavit	k5eAaPmNgInP
nejstarší	starý	k2eAgInPc1d3
objekty	objekt	k1gInPc1
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
,	,	kIx,
resp.	resp.	kA
barvu	barva	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
byly	být	k5eAaImAgFnP
zdi	zeď	k1gFnPc1
natřeny	natřen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1
název	název	k1gInSc1
města	město	k1gNnSc2
Alba	alba	k1gFnSc1
Regalis	Regalis	k1gFnSc1
odkazuje	odkazovat	k5eAaImIp3nS
rovněž	rovněž	k9
na	na	k7c4
bílou	bílý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
(	(	kIx(
<g/>
Alba	album	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
sídlo	sídlo	k1gNnSc1
panovníka	panovník	k1gMnSc2
(	(	kIx(
<g/>
Regalis	Regalis	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
němčině	němčina	k1gFnSc6
je	být	k5eAaImIp3nS
název	název	k1gInSc4
doslovným	doslovný	k2eAgInSc7d1
překladem	překlad	k1gInSc7
maďarského	maďarský	k2eAgInSc2d1
–	–	k?
jako	jako	k8xC,k8xS
Stuhlweißenburg	Stuhlweißenburg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chorvatština	chorvatština	k1gFnSc1
používá	používat	k5eAaImIp3nS
ikavskou	ikavský	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
(	(	kIx(
<g/>
Stolni	Stolni	k1gFnSc4
Biograd	Biograda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Oblast	oblast	k1gFnSc1
Székesfehérváru	Székesfehérvár	k1gInSc2
byla	být	k5eAaImAgFnS
osídlena	osídlen	k2eAgFnSc1d1
již	již	k9
Kelty	Kelt	k1gMnPc4
a	a	k8xC
malé	malý	k2eAgNnSc4d1
sídliště	sídliště	k1gNnSc4
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
i	i	k9
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
rozprostírala	rozprostírat	k5eAaImAgFnS
římská	římský	k2eAgFnSc1d1
provincie	provincie	k1gFnSc1
Panonie	Panonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontinuální	kontinuální	k2eAgInSc4d1
osídlení	osídlení	k1gNnSc1
je	být	k5eAaImIp3nS
poprvé	poprvé	k6eAd1
doloženo	doložen	k2eAgNnSc1d1
v	v	k7c6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Po	po	k7c6
dobytí	dobytí	k1gNnSc6
Maďary	Maďar	k1gMnPc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
součástí	součást	k1gFnSc7
uherského	uherský	k2eAgInSc2d1
státu	stát	k1gInSc2
a	a	k8xC
král	král	k1gMnSc1
Štěpán	Štěpána	k1gFnPc2
I.	I.	kA
vyhlásil	vyhlásit	k5eAaPmAgInS
Székesfehérvár	Székesfehérvár	k1gInSc1
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
korunováno	korunovat	k5eAaBmNgNnS
38	#num#	k4
a	a	k8xC
pohřbeno	pohřbít	k5eAaPmNgNnS
18	#num#	k4
uherských	uherský	k2eAgMnPc2d1
králů	král	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středověku	středověk	k1gInSc6
zde	zde	k6eAd1
stál	stát	k5eAaImAgInS
značný	značný	k2eAgInSc1d1
počet	počet	k1gInSc1
kostelů	kostel	k1gInPc2
a	a	k8xC
klášterů	klášter	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastavovali	zastavovat	k5eAaImAgMnP
se	se	k3xPyFc4
zde	zde	k6eAd1
poutníci	poutník	k1gMnPc1
cestující	cestující	k1gMnPc1
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
město	město	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1601	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
řádně	řádně	k6eAd1
opevněno	opevněn	k2eAgNnSc1d1
a	a	k8xC
hradby	hradba	k1gFnPc1
odrazily	odrazit	k5eAaPmAgFnP
dokonce	dokonce	k9
i	i	k9
mongolskou	mongolský	k2eAgFnSc4d1
invazi	invaze	k1gFnSc4
ve	v	k7c6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
<g/>
,	,	kIx,
sídlo	sídlo	k1gNnSc1
uherských	uherský	k2eAgMnPc2d1
králů	král	k1gMnPc2
však	však	k9
bylo	být	k5eAaImAgNnS
přesto	přesto	k8xC
přeneseno	přenést	k5eAaPmNgNnS
na	na	k7c4
nově	nově	k6eAd1
vzniklý	vzniklý	k2eAgInSc4d1
budínský	budínský	k2eAgInSc4d1
hrad	hrad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Székesfehérvár	Székesfehérvár	k1gMnSc1
však	však	k9
i	i	k9
nadále	nadále	k6eAd1
zůstal	zůstat	k5eAaPmAgInS
místem	místo	k1gNnSc7
korunovací	korunovace	k1gFnPc2
uherských	uherský	k2eAgMnPc2d1
králů	král	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
i	i	k9
řadu	řada	k1gFnSc4
dalších	další	k2eAgFnPc2d1
výsad	výsada	k1gFnPc2
<g/>
;	;	kIx,
kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
že	že	k9
zde	zde	k6eAd1
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
pořádány	pořádán	k2eAgInPc4d1
trhy	trh	k1gInPc4
se	se	k3xPyFc4
tu	tu	k6eAd1
také	také	k9
nacházel	nacházet	k5eAaImAgMnS
královský	královský	k2eAgInSc4d1
vinný	vinný	k2eAgInSc4d1
sklep	sklep	k1gInSc4
a	a	k8xC
prováděla	provádět	k5eAaImAgFnS
se	se	k3xPyFc4
i	i	k9
ražba	ražba	k1gFnSc1
mincí	mince	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1543	#num#	k4
město	město	k1gNnSc1
neodolalo	odolat	k5eNaPmAgNnS
útoku	útok	k1gInSc2
Turků	turek	k1gInPc2
a	a	k8xC
padlo	padnout	k5eAaPmAgNnS,k5eAaImAgNnS
do	do	k7c2
jejich	jejich	k3xOp3gFnPc2
rukou	ruka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turci	Turek	k1gMnPc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Bega	beg	k1gMnSc2
Ahmeda	Ahmed	k1gMnSc2
vyplenili	vyplenit	k5eAaPmAgMnP
místní	místní	k2eAgFnPc4d1
hrobky	hrobka	k1gFnPc4
i	i	k8xC
kostely	kostel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgMnSc1
z	z	k7c2
mála	málo	k4c2
dokladů	doklad	k1gInPc2
o	o	k7c6
životě	život	k1gInSc6
města	město	k1gNnSc2
za	za	k7c2
turecké	turecký	k2eAgFnSc2d1
nadvlády	nadvláda	k1gFnSc2
přinesl	přinést	k5eAaPmAgMnS
cestovatel	cestovatel	k1gMnSc1
Evlija	Evlijus	k1gMnSc2
Čelebi	Čelebi	k1gNnSc2
(	(	kIx(
<g/>
v	v	k7c6
maďarském	maďarský	k2eAgInSc6d1
přepisu	přepis	k1gInSc6
Evlia	Evlium	k1gNnSc2
Cselebi	Cseleb	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
procestoval	procestovat	k5eAaPmAgMnS
celé	celý	k2eAgNnSc4d1
tehdejší	tehdejší	k2eAgNnSc4d1
Evropské	evropský	k2eAgNnSc4d1
Turecko	Turecko	k1gNnSc4
a	a	k8xC
zaznamenal	zaznamenat	k5eAaPmAgMnS
jeho	jeho	k3xOp3gFnSc4
podobu	podoba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
město	město	k1gNnSc4
dobyla	dobýt	k5eAaPmAgFnS
armáda	armáda	k1gFnSc1
Leopolda	Leopold	k1gMnSc2
I.	I.	kA
Habsburského	habsburský	k2eAgInSc2d1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Evženem	Evžen	k1gMnSc7
Savojským	savojský	k2eAgMnSc7d1
<g/>
,	,	kIx,
našli	najít	k5eAaPmAgMnP
zde	zde	k6eAd1
osvoboditelé	osvoboditel	k1gMnPc1
pouze	pouze	k6eAd1
trosky	troska	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Székesfehérvár	Székesfehérvár	k1gInSc1
byl	být	k5eAaImAgInS
poté	poté	k6eAd1
během	během	k7c2
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
prakticky	prakticky	k6eAd1
znovu	znovu	k6eAd1
postaven	postavit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavební	stavební	k2eAgInSc1d1
boom	boom	k1gInSc1
probíhal	probíhat	k5eAaImAgInS
hlavně	hlavně	k9
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochovalo	dochovat	k5eAaPmAgNnS
se	se	k3xPyFc4
jen	jen	k9
minimum	minimum	k1gNnSc1
objektů	objekt	k1gInPc2
ze	z	k7c2
středověku	středověk	k1gInSc2
<g/>
,	,	kIx,
pozůstatky	pozůstatek	k1gInPc1
turecké	turecký	k2eAgFnSc2d1
okupace	okupace	k1gFnSc2
nejsou	být	k5eNaImIp3nP
v	v	k7c6
podstatě	podstata	k1gFnSc6
žádné	žádný	k3yNgNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytky	zbytek	k1gInPc1
středověkých	středověký	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
dokládá	dokládat	k5eAaImIp3nS
např.	např.	kA
i	i	k8xC
tzv.	tzv.	kA
Zahradě	zahrada	k1gFnSc3
trosek	troska	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
i	i	k9
náhrobek	náhrobek	k1gInSc1
údajně	údajně	k6eAd1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
patřící	patřící	k2eAgFnSc1d1
svatému	svatý	k1gMnSc3
Štěpánovi	Štěpán	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelstvo	obyvatelstvo	k1gNnSc1
města	město	k1gNnSc2
bylo	být	k5eAaImAgNnS
národnostně	národnostně	k6eAd1
smíšené	smíšený	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
Maďarů	Maďar	k1gMnPc2
zde	zde	k6eAd1
žili	žít	k5eAaImAgMnP
také	také	k9
Němci	Němec	k1gMnPc1
<g/>
,	,	kIx,
Židé	Žid	k1gMnPc1
a	a	k8xC
jižní	jižní	k2eAgMnPc1d1
Slované	Slovan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přítomnost	přítomnost	k1gFnSc1
posledních	poslední	k2eAgInPc2d1
uvedených	uvedený	k2eAgInPc2d1
dokládají	dokládat	k5eAaImIp3nP
i	i	k9
některé	některý	k3yIgInPc1
místní	místní	k2eAgInPc1d1
názvy	název	k1gInPc1
(	(	kIx(
<g/>
např.	např.	kA
Rác	Rác	k?
utca	utca	k1gMnSc1
apod.	apod.	kA
<g/>
)	)	kIx)
</s>
<s>
Nová	nový	k2eAgFnSc1d1
barokní	barokní	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Štěpána	Štěpán	k1gMnSc2
byla	být	k5eAaImAgFnS
postavena	postavit	k5eAaPmNgFnS
na	na	k7c6
troskách	troska	k1gFnPc6
středověké	středověký	k2eAgFnSc2d1
baziliky	bazilika	k1gFnSc2
původně	původně	k6eAd1
zasvěcené	zasvěcený	k2eAgFnSc3d1
Panně	Panna	k1gFnSc3
Marii	Maria	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedaleko	daleko	k6eNd1
od	od	k7c2
ní	on	k3xPp3gFnSc2
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
mála	málo	k4c2
dochovaných	dochovaný	k2eAgFnPc2d1
gotických	gotický	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
kaple	kaple	k1gFnSc1
sv.	sv.	kA
Anny	Anna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
intenzivně	intenzivně	k6eAd1
bombardováno	bombardovat	k5eAaImNgNnS
spojeneckým	spojenecký	k2eAgNnSc7d1
letectvem	letectvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poničena	poničen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
domů	dům	k1gInPc2
i	i	k8xC
historických	historický	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
;	;	kIx,
hrad	hrad	k1gInSc1
musel	muset	k5eAaImAgInS
být	být	k5eAaImF
nákladně	nákladně	k6eAd1
rekonstruován	rekonstruován	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
místního	místní	k2eAgNnSc2d1
nádraží	nádraží	k1gNnSc2
k	k	k7c3
železničnímu	železniční	k2eAgNnSc3d1
neštěstí	neštěstí	k1gNnSc3
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yQgNnSc6,k3yRgNnSc6,k3yIgNnSc6
zemřelo	zemřít	k5eAaPmAgNnS
150	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
žilo	žít	k5eAaImAgNnS
v	v	k7c6
Székesfehérváru	Székesfehérvár	k1gInSc6
100	#num#	k4
570	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
činí	činit	k5eAaImIp3nS
cca	cca	kA
1	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
celé	celý	k2eAgFnSc2d1
župy	župa	k1gFnSc2
Fejér	Fejér	k1gMnSc1
<g/>
.	.	kIx.
96	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnSc1
se	se	k3xPyFc4
hlásí	hlásit	k5eAaImIp3nS
k	k	k7c3
maďarské	maďarský	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
a	a	k8xC
pouhé	pouhý	k2eAgInPc1d1
4	#num#	k4
%	%	kIx~
k	k	k7c3
jiným	jiný	k2eAgFnPc3d1
národnostem	národnost	k1gFnPc3
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
k	k	k7c3
německé	německý	k2eAgFnSc3d1
nebo	nebo	k8xC
romské	romský	k2eAgFnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
se	se	k3xPyFc4
rovněž	rovněž	k9
hlásí	hlásit	k5eAaImIp3nS
k	k	k7c3
Římskokatolické	římskokatolický	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastoupená	zastoupený	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
ale	ale	k9
také	také	k9
i	i	k9
luteránská	luteránský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
Székesfehérváru	Székesfehérvár	k1gInSc2
rostl	růst	k5eAaImAgInS
bouřlivým	bouřlivý	k2eAgNnSc7d1
tempem	tempo	k1gNnSc7
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
především	především	k9
díky	díky	k7c3
rychlé	rychlý	k2eAgFnSc3d1
urbanizaci	urbanizace	k1gFnSc3
celého	celý	k2eAgNnSc2d1
Maďarska	Maďarsko	k1gNnSc2
a	a	k8xC
s	s	k7c7
ní	on	k3xPp3gFnSc7
ruku	ruka	k1gFnSc4
v	v	k7c6
ruce	ruka	k1gFnSc6
jdoucí	jdoucí	k2eAgFnSc4d1
industrializaci	industrializace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
ale	ale	k8xC
začal	začít	k5eAaPmAgInS
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
postupně	postupně	k6eAd1
klesal	klesat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měřitelný	měřitelný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
odchod	odchod	k1gInSc4
lidí	člověk	k1gMnPc2
z	z	k7c2
města	město	k1gNnSc2
měla	mít	k5eAaImAgFnS
také	také	k9
suburbanizace	suburbanizace	k1gFnSc1
a	a	k8xC
vzestup	vzestup	k1gInSc1
okolních	okolní	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
(	(	kIx(
<g/>
Úrhida	Úrhida	k1gFnSc1
<g/>
,	,	kIx,
Szabadbattyán	Szabadbattyán	k1gMnSc1
<g/>
,	,	kIx,
Fehérvárcsurgó	Fehérvárcsurgó	k1gMnSc1
<g/>
,	,	kIx,
Agárd	Agárd	k1gMnSc1
<g/>
,	,	kIx,
Gárdony	Gárdona	k1gFnPc1
<g/>
,	,	kIx,
Pákozd	Pákozd	k1gMnSc1
<g/>
,	,	kIx,
Pátka	Pátek	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Industrializace	industrializace	k1gFnSc1
města	město	k1gNnSc2
byla	být	k5eAaImAgFnS
iniciována	iniciovat	k5eAaBmNgFnS
ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiný	k2eAgInPc4d1
se	se	k3xPyFc4
zde	zde	k6eAd1
nachází	nacházet	k5eAaImIp3nS
i	i	k9
zbrojní	zbrojní	k2eAgInPc4d1
závody	závod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
město	město	k1gNnSc1
stalo	stát	k5eAaPmAgNnS
cílem	cíl	k1gInSc7
spojeneckého	spojenecký	k2eAgNnSc2d1
bombardování	bombardování	k1gNnSc2
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
byly	být	k5eAaImAgInP
některé	některý	k3yIgInPc1
místní	místní	k2eAgInPc1d1
průmyslové	průmyslový	k2eAgInPc1d1
podniky	podnik	k1gInPc1
přemístěny	přemístěn	k2eAgInPc1d1
do	do	k7c2
města	město	k1gNnSc2
Dunaújváros	Dunaújvárosa	k1gFnPc2
(	(	kIx(
<g/>
Sztalinváros	Sztalinvárosa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
vlády	vláda	k1gFnSc2
Jánose	Jánosa	k1gFnSc6
Kádára	Kádár	k1gMnSc2
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
proces	proces	k1gInSc1
zastaven	zastavit	k5eAaPmNgInS
a	a	k8xC
město	město	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
další	další	k2eAgFnPc4d1
potřebné	potřebný	k2eAgFnPc4d1
investice	investice	k1gFnPc4
do	do	k7c2
hospodářského	hospodářský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlila	sídlit	k5eAaImAgFnS
zde	zde	k6eAd1
např.	např.	kA
i	i	k9
společnost	společnost	k1gFnSc1
Ikarus	Ikarus	k1gMnSc1
vyrábějící	vyrábějící	k2eAgInPc4d1
autobusy	autobus	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
bylo	být	k5eAaImAgNnS
jedním	jeden	k4xCgInSc7
ze	z	k7c2
symbolů	symbol	k1gInPc2
transformace	transformace	k1gFnSc2
a	a	k8xC
modernizace	modernizace	k1gFnSc2
maďarské	maďarský	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
a	a	k8xC
bylo	být	k5eAaImAgNnS
přezdíváno	přezdíván	k2eAgNnSc4d1
Maďarské	maďarský	k2eAgNnSc4d1
Silicon	Silicon	kA
Valley	Vallea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kultura	kultura	k1gFnSc1
a	a	k8xC
pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Bazilika	bazilika	k1gFnSc1
sv.	sv.	kA
Štěpána	Štěpána	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Historický	historický	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
střed	střed	k1gInSc4
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
obvodního	obvodní	k2eAgInSc2d1
soudu	soud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Székesfehérváru	Székesfehérvár	k1gInSc6
je	být	k5eAaImIp3nS
evidováno	evidován	k2eAgNnSc4d1
139	#num#	k4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochovány	dochován	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
základy	základ	k1gInPc1
středověké	středověký	k2eAgFnSc2d1
baziliky	bazilika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgInSc7d1
chrámem	chrám	k1gInSc7
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
i	i	k9
bazilika	bazilika	k1gFnSc1
sv.	sv.	kA
Štěpána	Štěpána	k1gFnSc1
<g/>
,	,	kIx,
zbudovaná	zbudovaný	k2eAgFnSc1d1
v	v	k7c6
barokním	barokní	k2eAgInSc6d1
stylu	styl	k1gInSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
např.	např.	kA
gotická	gotický	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
sv.	sv.	kA
Anny	Anna	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1470	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
městě	město	k1gNnSc6
stojí	stát	k5eAaImIp3nS
několik	několik	k4yIc4
muzeí	muzeum	k1gNnPc2
<g/>
,	,	kIx,
např.	např.	kA
Muzum	Muzum	k1gInSc1
sv.	sv.	kA
Štěpána	Štěpán	k1gMnSc2
<g/>
,	,	kIx,
muzeum	muzeum	k1gNnSc1
panenek	panenka	k1gFnPc2
<g/>
,	,	kIx,
městské	městský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
nebo	nebo	k8xC
městská	městský	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Székesfehérvár	Székesfehérvár	k1gInSc1
představuje	představovat	k5eAaImIp3nS
v	v	k7c6
maďarské	maďarský	k2eAgFnSc6d1
železniční	železniční	k2eAgFnSc6d1
síti	síť	k1gFnSc6
významnou	významný	k2eAgFnSc4d1
křižovatku	křižovatka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
dopravní	dopravní	k2eAgInSc1d1
tah	tah	k1gInSc1
směřuje	směřovat	k5eAaImIp3nS
z	z	k7c2
metropole	metropol	k1gFnSc2
Budapešti	Budapešť	k1gFnSc2
k	k	k7c3
Balatonu	Balaton	k1gInSc3
(	(	kIx(
<g/>
Blatenskému	blatenský	k2eAgNnSc3d1
jezeru	jezero	k1gNnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
města	město	k1gNnSc2
rovněž	rovněž	k9
vedou	vést	k5eAaImIp3nP
i	i	k9
další	další	k2eAgFnPc4d1
regionální	regionální	k2eAgFnPc4d1
tratě	trať	k1gFnPc4
např.	např.	kA
do	do	k7c2
Komáromu	Komárom	k1gInSc2
apod.	apod.	kA
</s>
<s>
Jižně	jižně	k6eAd1
od	od	k7c2
Székesfehérváru	Székesfehérvár	k1gInSc2
prochází	procházet	k5eAaImIp3nS
i	i	k9
dálnice	dálnice	k1gFnSc1
M	M	kA
<g/>
7	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
Budapešť	Budapešť	k1gFnSc1
spojuje	spojovat	k5eAaImIp3nS
s	s	k7c7
chorvatskou	chorvatský	k2eAgFnSc7d1
metropolí	metropol	k1gFnSc7
Záhřebem	Záhřeb	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Slavní	slavný	k2eAgMnPc1d1
rodáci	rodák	k1gMnPc1
</s>
<s>
Jenő	Jenő	k?
Bory	bor	k1gInPc1
<g/>
,	,	kIx,
sochař	sochař	k1gMnSc1
<g/>
,	,	kIx,
architekt	architekt	k1gMnSc1
</s>
<s>
Nándor	Nándor	k1gInSc1
Fa	fa	k1gNnSc2
<g/>
,	,	kIx,
cestovatel	cestovatel	k1gMnSc1
(	(	kIx(
<g/>
obeplul	obeplout	k5eAaPmAgMnS
zemi	zem	k1gFnSc4
v	v	k7c6
malém	malý	k2eAgInSc6d1
člunu	člun	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Katarina	Katarin	k2eAgFnSc1d1
Ivanovićová	Ivanovićová	k1gFnSc1
<g/>
,	,	kIx,
srbská	srbský	k2eAgFnSc1d1
malířka	malířka	k1gFnSc1
</s>
<s>
Péter	Péter	k1gInSc1
Kuczka	Kuczka	k1gFnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
</s>
<s>
Kornél	Kornél	k1gMnSc1
Lánczos	Lánczos	k1gMnSc1
<g/>
,	,	kIx,
fyzik	fyzik	k1gMnSc1
</s>
<s>
Ignác	Ignác	k1gMnSc1
Goldziher	Goldzihra	k1gFnPc2
<g/>
,	,	kIx,
orientalista	orientalista	k1gMnSc1
</s>
<s>
Viktor	Viktor	k1gMnSc1
Orbán	Orbán	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
maďarské	maďarský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
1998	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
</s>
<s>
Katalin	Katalin	k1gInSc1
Bogyayová	Bogyayová	k1gFnSc1
<g/>
,	,	kIx,
novinářka	novinářka	k1gFnSc1
</s>
<s>
Lajos	Lajos	k1gMnSc1
Terkán	Terkán	k2eAgMnSc1d1
<g/>
,	,	kIx,
astronom	astronom	k1gMnSc1
</s>
<s>
Miklós	Miklós	k6eAd1
Ybl	Ybl	k1gMnSc1
<g/>
,	,	kIx,
architekt	architekt	k1gMnSc1
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Alba	alba	k1gFnSc1
Iulia	Iulius	k1gMnSc2
<g/>
,	,	kIx,
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
Birmingham	Birmingham	k1gInSc1
<g/>
,	,	kIx,
Alabama	Alabama	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Blagoevgrad	Blagoevgrad	k1gInSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Cento	Cento	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
Chorley	Chorlea	k1gFnPc1
(	(	kIx(
<g/>
Lancashire	Lancashir	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kemi	Kemi	k1gNnSc1
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
</s>
<s>
Luhansk	Luhansk	k1gInSc1
<g/>
,	,	kIx,
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Opole	Opole	k1gNnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sariwon	Sariwon	k1gNnSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Schwäbisch	Schwäbisch	k1gMnSc1
Gmünd	Gmünd	k1gMnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zadar	Zadar	k1gInSc1
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Spolupracující	spolupracující	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Magyarország	Magyarországa	k1gFnPc2
közigazgatási	közigazgatáse	k1gFnSc4
helynévkönyve	helynévkönyev	k1gFnSc2
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
január	január	k1gInSc1
1	#num#	k4
<g/>
..	..	k?
14	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Székesfehérvár	Székesfehérvár	k1gInSc1
na	na	k7c6
maďarské	maďarský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
NÉMETH	NÉMETH	kA
<g/>
,	,	kIx,
Gyula	Gyulo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hungary	Hungara	k1gFnPc1
<g/>
:	:	kIx,
A	a	k9
Complete	Comple	k1gNnSc2
Guide	Guid	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budapest	Budapest	k1gInSc1
<g/>
:	:	kIx,
Corvina	Corvina	k1gFnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
963	#num#	k4
13	#num#	k4
3740	#num#	k4
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
120	#num#	k4
<g/>
-	-	kIx~
<g/>
124	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Péter	Péter	k1gMnSc1
Szabó	Szabó	k1gMnSc1
<g/>
:	:	kIx,
Cultural	Cultural	k1gMnSc1
identity	identita	k1gFnSc2
change	change	k1gFnSc2
of	of	k?
a	a	k8xC
royal	royal	k1gMnSc1
Hungarian	Hungarian	k1gMnSc1
town	town	k1gMnSc1
<g/>
,	,	kIx,
Székesfehérvár	Székesfehérvár	k1gMnSc1
<g/>
,	,	kIx,
in	in	k?
the	the	k?
18	#num#	k4
<g/>
th	th	k?
and	and	k?
19	#num#	k4
<g/>
th	th	k?
century	centura	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Fejér	Fejér	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Székesfehérvár	Székesfehérvár	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Bělehrad	Bělehrad	k1gInSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Galerie	galerie	k1gFnSc1
Székesfehérvár	Székesfehérvár	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Székesfehérvár	Székesfehérvár	k1gInSc1
(	(	kIx(
<g/>
Megyei	Megyei	k1gNnSc1
jogú	jogú	k?
város	város	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Okres	okres	k1gInSc1
Székesfehérvár	Székesfehérvár	k1gInSc1
</s>
<s>
Aba	Aba	k?
•	•	k?
Bakonykúti	Bakonykúť	k1gFnSc2
•	•	k?
Csór	Csór	k1gMnSc1
•	•	k?
Csősz	Csősz	k1gMnSc1
•	•	k?
Füle	Füle	k1gFnSc1
•	•	k?
Iszkaszentgyörgy	Iszkaszentgyörg	k1gInPc1
•	•	k?
Jenő	Jenő	k1gFnSc2
•	•	k?
Káloz	Káloz	k1gMnSc1
•	•	k?
Kőszárhegy	Kőszárhega	k1gFnSc2
•	•	k?
Lovasberény	Lovasberéna	k1gFnSc2
•	•	k?
Moha	moct	k5eAaImSgInS
•	•	k?
Nádasdladány	Nádasdladán	k2eAgFnPc1d1
•	•	k?
Pátka	Pátek	k1gMnSc2
•	•	k?
Polgárdi	Polgárd	k1gMnPc1
•	•	k?
Sárkeresztes	Sárkeresztes	k1gMnSc1
•	•	k?
Sárkeszi	Sárkesze	k1gFnSc4
•	•	k?
Sárosd	Sárosd	k1gMnSc1
•	•	k?
Sárszentmihály	Sárszentmihála	k1gFnSc2
•	•	k?
Seregélyes	Seregélyes	k1gMnSc1
•	•	k?
Soponya	Soponya	k1gMnSc1
•	•	k?
Szabadbattyán	Szabadbattyán	k1gMnSc1
•	•	k?
Székesfehérvár	Székesfehérvár	k1gMnSc1
•	•	k?
Tác	tác	k1gInSc1
•	•	k?
Úrhida	Úrhida	k1gFnSc1
•	•	k?
Zámoly	Zámola	k1gFnSc2
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
-	-	kIx~
Magyarország	Magyarország	k1gInSc1
-	-	kIx~
(	(	kIx(
<g/>
H	H	kA
<g/>
)	)	kIx)
Župy	župa	k1gFnSc2
(	(	kIx(
<g/>
Megyék	Megyék	k1gMnSc1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
správní	správní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Bács-Kiskun	Bács-Kiskun	k1gInSc1
(	(	kIx(
<g/>
Kecskemét	Kecskemét	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Baranya	Baranya	k1gFnSc1
(	(	kIx(
<g/>
Pécs	Pécs	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Békés	Békés	k1gInSc1
(	(	kIx(
<g/>
Békéscsaba	Békéscsaba	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Borsod-Abaúj-Zemplén	Borsod-Abaúj-Zemplén	k1gInSc1
(	(	kIx(
<g/>
Miskolc	Miskolc	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Csongrád-Csanád	Csongrád-Csanáda	k1gFnPc2
(	(	kIx(
<g/>
Segedín	Segedín	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Fejér	Fejér	k1gInSc1
(	(	kIx(
<g/>
Székesfehérvár	Székesfehérvár	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Győr-Moson-Sopron	Győr-Moson-Sopron	k1gInSc1
(	(	kIx(
<g/>
Győr	Győr	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Hajdú-Bihar	Hajdú-Bihar	k1gInSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Debrecín	Debrecín	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Heves	Heves	k1gInSc1
(	(	kIx(
<g/>
Eger	Eger	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Jász-Nagykun-Szolnok	Jász-Nagykun-Szolnok	k1gInSc1
(	(	kIx(
<g/>
Szolnok	Szolnok	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Komárom-Esztergom	Komárom-Esztergom	k1gInSc1
(	(	kIx(
<g/>
Tatabánya	Tatabánya	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Nógrád	Nógráda	k1gFnPc2
(	(	kIx(
<g/>
Salgótarján	Salgótarján	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Pest	Pest	k1gInSc1
(	(	kIx(
<g/>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Budapešť	Budapešť	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Somogy	Somoga	k1gFnSc2
(	(	kIx(
<g/>
Kaposvár	Kaposvár	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Szabolcs-Szatmár-Bereg	Szabolcs-Szatmár-Bereg	k1gInSc1
(	(	kIx(
<g/>
Nyíregyháza	Nyíregyháza	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Tolna	Tolna	k1gFnSc1
(	(	kIx(
<g/>
Szekszárd	Szekszárd	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Vas	Vas	k1gFnSc1
(	(	kIx(
<g/>
Szombathely	Szombathel	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Veszprém	Veszprý	k2eAgInSc6d1
(	(	kIx(
<g/>
Veszprém	Veszprý	k2eAgMnSc6d1
<g/>
)	)	kIx)
•	•	k?
Zala	Zala	k1gFnSc1
(	(	kIx(
<g/>
Zalaegerszeg	Zalaegerszeg	k1gInSc1
<g/>
)	)	kIx)
Města	město	k1gNnSc2
s	s	k7c7
župním	župní	k2eAgNnSc7d1
právem	právo	k1gNnSc7
(	(	kIx(
<g/>
Megyei	Megyei	k1gNnSc1
jogú	jogú	k?
város	város	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Békéscsaba	Békéscsaba	k1gFnSc1
•	•	k?
Debrecín	Debrecín	k1gInSc1
•	•	k?
Dunaújváros	Dunaújvárosa	k1gFnPc2
•	•	k?
Eger	Eger	k1gMnSc1
•	•	k?
Érd	Érd	k1gMnSc1
•	•	k?
Győr	Győr	k1gMnSc1
•	•	k?
Hódmezővásárhely	Hódmezővásárhela	k1gFnSc2
•	•	k?
Kaposvár	Kaposvár	k1gMnSc1
•	•	k?
Kecskemét	Kecskemét	k1gMnSc1
•	•	k?
Miskolc	Miskolc	k1gInSc1
•	•	k?
Nagykanizsa	Nagykanizsa	k1gFnSc1
•	•	k?
Nyíregyháza	Nyíregyháza	k1gFnSc1
•	•	k?
Pécs	Pécsa	k1gFnPc2
•	•	k?
Salgótarján	Salgótarján	k1gMnSc1
•	•	k?
Segedín	Segedín	k1gMnSc1
•	•	k?
Szekszárd	Szekszárd	k1gMnSc1
•	•	k?
Székesfehérvár	Székesfehérvár	k1gMnSc1
•	•	k?
Szolnok	Szolnok	k1gInSc1
•	•	k?
Szombathely	Szombathela	k1gFnSc2
•	•	k?
Šoproň	Šoproň	k1gFnSc1
•	•	k?
Tatabánya	Tatabánya	k1gFnSc1
•	•	k?
Veszprém	Veszprý	k2eAgInSc6d1
•	•	k?
Zalaegerszeg	Zalaegerszeg	k1gInSc1
Regiony	region	k1gInPc1
(	(	kIx(
<g/>
Régiók	Régiók	k1gInSc1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
název	název	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Dél-Alföld	Dél-Alföld	k1gInSc1
(	(	kIx(
<g/>
Jižní	jižní	k2eAgInSc1d1
Alföld	Alföld	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Dél-Dunántúl	Dél-Dunántúl	k1gInSc1
(	(	kIx(
<g/>
Jižní	jižní	k2eAgFnSc1d1
Zadunají	Zadunají	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Észak-Alföld	Észak-Alföld	k1gInSc1
(	(	kIx(
<g/>
Severní	severní	k2eAgInSc1d1
Alföld	Alföld	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Észak-Magyarország	Észak-Magyarország	k1gInSc1
(	(	kIx(
<g/>
Severní	severní	k2eAgNnSc1d1
Maďarsko	Maďarsko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Közép-Dunántúl	Közép-Dunántúl	k1gInSc1
(	(	kIx(
<g/>
Střední	střední	k2eAgFnSc1d1
Zadunají	Zadunají	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Közép-Magyarország	Közép-Magyarország	k1gInSc1
(	(	kIx(
<g/>
Střední	střední	k2eAgNnSc1d1
Maďarsko	Maďarsko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Nyugat-Dunántúl	Nyugat-Dunántúl	k1gInSc1
(	(	kIx(
<g/>
Západní	západní	k2eAgMnSc1d1
Zadunají	Zadunají	k1gMnSc1
<g/>
)	)	kIx)
Části	část	k1gFnSc6
státu	stát	k1gInSc2
(	(	kIx(
<g/>
Országrész	Országrész	k1gInSc1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
název	název	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Közép-Magyarország	Közép-Magyarország	k1gInSc1
(	(	kIx(
<g/>
Střední	střední	k2eAgNnSc1d1
Maďarsko	Maďarsko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Nyugat-Magyarország	Nyugat-Magyarország	k1gInSc1
(	(	kIx(
<g/>
Západní	západní	k2eAgNnSc1d1
Maďarsko	Maďarsko	k1gNnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
také	také	k9
Dunántúl	Dunántúl	k1gInSc1
(	(	kIx(
<g/>
Zadunají	Zadunají	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kelet-Magyarország	Kelet-Magyarország	k1gInSc1
(	(	kIx(
<g/>
Východní	východní	k2eAgNnSc1d1
Maďarsko	Maďarsko	k1gNnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
také	také	k9
Alföld	Alföld	k1gMnSc1
és	és	k?
Észak	Észak	k1gMnSc1
(	(	kIx(
<g/>
Alföld	Alföld	k1gInSc1
a	a	k8xC
Sever	sever	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Města	město	k1gNnPc1
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
podle	podle	k7c2
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnSc1
1	#num#	k4
000	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Budapešť	Budapešť	k1gFnSc1
100	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Debrecín	Debrecín	k1gMnSc1
•	•	k?
Segedín	Segedín	k1gMnSc1
•	•	k?
Miskolc	Miskolc	k1gInSc1
•	•	k?
Pécs	Pécs	k1gInSc1
•	•	k?
Győr	Győr	k1gInSc1
•	•	k?
Nyíregyháza	Nyíregyháza	k1gFnSc1
•	•	k?
Kecskemét	Kecskemét	k1gInSc1
•	•	k?
Székesfehérvár	Székesfehérvár	k1gInSc1
50	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Szombathely	Szombathel	k1gInPc1
•	•	k?
Szolnok	Szolnok	k1gInSc1
•	•	k?
Tatabánya	Tatabány	k1gInSc2
•	•	k?
Kaposvár	Kaposvár	k1gMnSc1
•	•	k?
Érd	Érd	k1gMnSc1
•	•	k?
Békéscsaba	Békéscsaba	k1gMnSc1
•	•	k?
Veszprém	Veszprý	k2eAgInSc6d1
•	•	k?
Šoproň	Šoproň	k1gFnSc1
•	•	k?
Zalaegerszeg	Zalaegerszeg	k1gMnSc1
•	•	k?
Eger	Eger	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
207338	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4132096-7	4132096-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80097371	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
153605603	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80097371	#num#	k4
</s>
