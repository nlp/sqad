<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
harmonie	harmonie	k1gFnSc2	harmonie
znamená	znamenat	k5eAaImIp3nS	znamenat
obecně	obecně	k6eAd1	obecně
soulad	soulad	k1gInSc4	soulad
<g/>
,	,	kIx,	,
souzvuk	souzvuk	k1gInSc4	souzvuk
<g/>
,	,	kIx,	,
souznění	souznění	k1gNnSc4	souznění
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
nejčastější	častý	k2eAgNnSc1d3	nejčastější
užití	užití	k1gNnSc1	užití
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
akustice	akustika	k1gFnSc6	akustika
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
současně	současně	k6eAd1	současně
znějících	znějící	k2eAgInPc2d1	znějící
tónů	tón	k1gInPc2	tón
a	a	k8xC	a
akordů	akord	k1gInPc2	akord
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
název	název	k1gInSc4	název
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
zabývající	zabývající	k2eAgFnSc4d1	zabývající
se	se	k3xPyFc4	se
hudební	hudební	k2eAgFnSc7d1	hudební
harmonií	harmonie	k1gFnSc7	harmonie
po	po	k7c6	po
teoretické	teoretický	k2eAgFnSc6d1	teoretická
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ale	ale	k9	ale
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
označení	označení	k1gNnSc4	označení
pouhého	pouhý	k2eAgInSc2d1	pouhý
souzvuku	souzvuk	k1gInSc2	souzvuk
či	či	k8xC	či
souladu	soulad	k1gInSc2	soulad
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
se	se	k3xPyFc4	se
tímto	tento	k3xDgNnSc7	tento
slovem	slovo	k1gNnSc7	slovo
také	také	k9	také
označuje	označovat	k5eAaImIp3nS	označovat
soubor	soubor	k1gInSc1	soubor
dechových	dechový	k2eAgInPc2d1	dechový
nástrojů	nástroj	k1gInPc2	nástroj
–	–	k?	–
dechová	dechový	k2eAgFnSc1d1	dechová
harmonie	harmonie	k1gFnSc1	harmonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opakem	opak	k1gInSc7	opak
harmonie	harmonie	k1gFnSc2	harmonie
je	být	k5eAaImIp3nS	být
disharmonie	disharmonie	k1gFnSc1	disharmonie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
označuje	označovat	k5eAaImIp3nS	označovat
nehezký	hezký	k2eNgInSc1d1	nehezký
či	či	k8xC	či
nepříjemný	příjemný	k2eNgInSc1d1	nepříjemný
souzvuk	souzvuk	k1gInSc1	souzvuk
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
jej	on	k3xPp3gMnSc4	on
ovšem	ovšem	k9	ovšem
obecně	obecně	k6eAd1	obecně
chápat	chápat	k5eAaImF	chápat
jako	jako	k8xC	jako
něco	něco	k3yInSc4	něco
objektivně	objektivně	k6eAd1	objektivně
nepatřičného	patřičný	k2eNgMnSc2d1	nepatřičný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zcela	zcela	k6eAd1	zcela
záměrnou	záměrný	k2eAgFnSc7d1	záměrná
součástí	součást	k1gFnSc7	součást
skladby	skladba	k1gFnSc2	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
harmonie	harmonie	k1gFnSc2	harmonie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
ἁ	ἁ	k?	ἁ
(	(	kIx(	(
<g/>
harmonía	harmonía	k1gMnSc1	harmonía
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
hodit	hodit	k5eAaPmF	hodit
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
nebo	nebo	k8xC	nebo
spojovat	spojovat	k5eAaImF	spojovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Koncept	koncept	k1gInSc1	koncept
harmonie	harmonie	k1gFnSc2	harmonie
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Pythagora	Pythagoras	k1gMnSc2	Pythagoras
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pojem	pojem	k1gInSc1	pojem
==	==	k?	==
</s>
</p>
<p>
<s>
Harmonie	harmonie	k1gFnSc1	harmonie
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
melodií	melodie	k1gFnSc7	melodie
a	a	k8xC	a
rytmem	rytmus	k1gInSc7	rytmus
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
základním	základní	k2eAgInPc3d1	základní
prvkům	prvek	k1gInPc3	prvek
<g/>
,	,	kIx,	,
definujícím	definující	k2eAgInPc3d1	definující
hudební	hudební	k2eAgInSc1d1	hudební
projev	projev	k1gInSc4	projev
klasického	klasický	k2eAgInSc2d1	klasický
evropského	evropský	k2eAgInSc2d1	evropský
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
o	o	k7c4	o
harmonii	harmonie	k1gFnSc4	harmonie
hovoří	hovořit	k5eAaImIp3nS	hovořit
jako	jako	k9	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
vertikálním	vertikální	k2eAgInSc6d1	vertikální
<g/>
"	"	kIx"	"
aspektu	aspekt	k1gInSc6	aspekt
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
"	"	kIx"	"
<g/>
horizontálním	horizontální	k2eAgInSc7d1	horizontální
<g/>
"	"	kIx"	"
aspektem	aspekt	k1gInSc7	aspekt
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
melodie	melodie	k1gFnPc4	melodie
<g/>
.	.	kIx.	.
</s>
<s>
Harmonie	harmonie	k1gFnSc1	harmonie
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
určitý	určitý	k2eAgInSc4d1	určitý
protipól	protipól	k1gInSc4	protipól
polyfonie	polyfonie	k1gFnSc2	polyfonie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
současném	současný	k2eAgInSc6d1	současný
znění	znění	k1gNnSc2	znění
několika	několik	k4yIc2	několik
melodických	melodický	k2eAgFnPc2d1	melodická
linek	linka	k1gFnPc2	linka
nebo	nebo	k8xC	nebo
motivů	motiv	k1gInPc2	motiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Harmonie	harmonie	k1gFnSc1	harmonie
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
výsledkem	výsledek	k1gInSc7	výsledek
současného	současný	k2eAgNnSc2d1	současné
souznění	souznění	k1gNnSc2	souznění
dvou	dva	k4xCgFnPc2	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
tónů	tón	k1gInPc2	tón
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
tvořit	tvořit	k5eAaImF	tvořit
harmonii	harmonie	k1gFnSc4	harmonie
i	i	k9	i
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
pouze	pouze	k6eAd1	pouze
jediné	jediný	k2eAgFnSc2d1	jediná
melodické	melodický	k2eAgFnSc2d1	melodická
linky	linka	k1gFnSc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
skladeb	skladba	k1gFnPc2	skladba
pro	pro	k7c4	pro
sólové	sólový	k2eAgInPc4d1	sólový
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
např.	např.	kA	např.
housle	housle	k1gFnPc4	housle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc4	použití
akordů	akord	k1gInPc2	akord
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgNnSc1d1	vzácné
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
dojem	dojem	k1gInSc4	dojem
plné	plný	k2eAgFnSc2d1	plná
harmonie	harmonie	k1gFnSc2	harmonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kofroň	Kofroň	k1gMnSc1	Kofroň
<g/>
:	:	kIx,	:
Učebnice	učebnice	k1gFnSc1	učebnice
harmonie	harmonie	k1gFnSc2	harmonie
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Konsonance	konsonance	k1gFnSc1	konsonance
</s>
</p>
<p>
<s>
Disonance	disonance	k1gFnSc1	disonance
</s>
</p>
<p>
<s>
Akord	akord	k1gInSc1	akord
</s>
</p>
<p>
<s>
Chromatický	chromatický	k2eAgInSc4d1	chromatický
akord	akord	k1gInSc4	akord
</s>
</p>
<p>
<s>
Rytmus	rytmus	k1gInSc1	rytmus
</s>
</p>
<p>
<s>
Tonalita	tonalita	k1gFnSc1	tonalita
</s>
</p>
<p>
<s>
Vedení	vedení	k1gNnSc1	vedení
hlasu	hlas	k1gInSc2	hlas
</s>
</p>
<p>
<s>
Kvintový	kvintový	k2eAgInSc1d1	kvintový
kruh	kruh	k1gInSc1	kruh
</s>
</p>
<p>
<s>
Melodie	melodie	k1gFnSc1	melodie
</s>
</p>
<p>
<s>
Tón	tón	k1gInSc1	tón
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
kinetika	kinetika	k1gFnSc1	kinetika
</s>
</p>
<p>
<s>
Barva	barva	k1gFnSc1	barva
zvuku	zvuk	k1gInSc2	zvuk
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tektonika	tektonika	k1gFnSc1	tektonika
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Harmonie	harmonie	k1gFnSc2	harmonie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
harmonie	harmonie	k1gFnSc2	harmonie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
HARMONIE	harmonie	k1gFnSc1	harmonie
pro	pro	k7c4	pro
muzikanty	muzikant	k1gMnPc4	muzikant
amatéry	amatér	k1gMnPc4	amatér
</s>
</p>
