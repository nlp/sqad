<s>
Opakem	opak	k1gInSc7	opak
harmonie	harmonie	k1gFnSc2	harmonie
je	být	k5eAaImIp3nS	být
disharmonie	disharmonie	k1gFnSc1	disharmonie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
označuje	označovat	k5eAaImIp3nS	označovat
nehezký	hezký	k2eNgInSc1d1	nehezký
či	či	k8xC	či
nepříjemný	příjemný	k2eNgInSc1d1	nepříjemný
souzvuk	souzvuk	k1gInSc1	souzvuk
<g/>
.	.	kIx.	.
</s>
