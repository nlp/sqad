<s>
André	André	k1gMnSc1	André
Paul	Paul	k1gMnSc1	Paul
Guillaume	Guillaum	k1gInSc5	Guillaum
Gide	Gid	k1gFnPc1	Gid
[	[	kIx(	[
<g/>
andré	andrý	k2eAgFnPc1d1	andrý
pól	pól	k1gInSc4	pól
gijjóm	gijjóma	k1gFnPc2	gijjóma
žíd	žíd	k?	žíd
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
významného	významný	k2eAgMnSc2d1	významný
právníka	právník	k1gMnSc2	právník
a	a	k8xC	a
profesora	profesor	k1gMnSc2	profesor
Paula	Paul	k1gMnSc2	Paul
Gida	Gidus	k1gMnSc2	Gidus
<g/>
.	.	kIx.	.
</s>
<s>
Mládí	mládí	k1gNnSc1	mládí
prožil	prožít	k5eAaPmAgInS	prožít
v	v	k7c6	v
silně	silně	k6eAd1	silně
náboženském	náboženský	k2eAgNnSc6d1	náboženské
rodinném	rodinný	k2eAgNnSc6d1	rodinné
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
původně	původně	k6eAd1	původně
katolička	katolička	k1gFnSc1	katolička
<g/>
,	,	kIx,	,
zavedla	zavést	k5eAaPmAgFnS	zavést
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
přísný	přísný	k2eAgInSc4d1	přísný
režim	režim	k1gInSc4	režim
protestantsky	protestantsky	k6eAd1	protestantsky
puritánských	puritánský	k2eAgInPc2d1	puritánský
mravů	mrav	k1gInPc2	mrav
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
několika	několik	k4yIc6	několik
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
pro	pro	k7c4	pro
špatný	špatný	k2eAgInSc4d1	špatný
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
musel	muset	k5eAaImAgMnS	muset
studia	studio	k1gNnSc2	studio
několikrát	několikrát	k6eAd1	několikrát
přerušit	přerušit	k5eAaPmF	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
student	student	k1gMnSc1	student
Alsaské	alsaský	k2eAgFnSc2d1	alsaská
koleje	kolej	k1gFnSc2	kolej
(	(	kIx(	(
<g/>
École	Écol	k1gMnSc5	Écol
Alsacienne	Alsacienn	k1gMnSc5	Alsacienn
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
živě	živě	k6eAd1	živě
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
přispíval	přispívat	k5eAaImAgInS	přispívat
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
revuí	revue	k1gFnPc2	revue
<g/>
,	,	kIx,	,
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
Stéphanem	Stéphan	k1gInSc7	Stéphan
Mallarmém	Mallarmý	k2eAgInSc6d1	Mallarmý
<g/>
,	,	kIx,	,
Paulem	Paul	k1gMnSc7	Paul
Valérym	Valérym	k1gInSc4	Valérym
<g/>
,	,	kIx,	,
Paulem	Paul	k1gMnSc7	Paul
Claudelem	Claudel	k1gMnSc7	Claudel
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
předními	přední	k2eAgMnPc7d1	přední
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
básníky	básník	k1gMnPc7	básník
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgInS	začít
vydávat	vydávat	k5eAaPmF	vydávat
svá	svůj	k3xOyFgNnPc4	svůj
první	první	k4xOgNnPc4	první
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
poplatná	poplatný	k2eAgFnSc1d1	poplatná
symbolismu	symbolismus	k1gInSc2	symbolismus
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
však	však	k9	však
brzy	brzy	k6eAd1	brzy
odklonil	odklonit	k5eAaPmAgMnS	odklonit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1893-1894	[number]	k4	1893-1894
podnikl	podniknout	k5eAaPmAgMnS	podniknout
ozdravné	ozdravný	k2eAgFnPc4d1	ozdravná
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
blízkým	blízký	k2eAgMnSc7d1	blízký
přítelem	přítel	k1gMnSc7	přítel
Oscara	Oscar	k1gMnSc2	Oscar
Wilda	Wild	k1gMnSc2	Wild
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
v	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
cesty	cesta	k1gFnPc1	cesta
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
palčivými	palčivý	k2eAgInPc7d1	palčivý
národnostními	národnostní	k2eAgInPc7d1	národnostní
a	a	k8xC	a
sociálními	sociální	k2eAgInPc7d1	sociální
problémy	problém	k1gInPc7	problém
a	a	k8xC	a
také	také	k9	také
objevil	objevit	k5eAaPmAgInS	objevit
svět	svět	k1gInSc1	svět
smyslů	smysl	k1gInPc2	smysl
<g/>
,	,	kIx,	,
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
od	od	k7c2	od
výchovou	výchova	k1gFnSc7	výchova
vštěpované	vštěpovaný	k2eAgFnSc2d1	vštěpovaná
přísné	přísný	k2eAgFnSc2d1	přísná
sebekázně	sebekázeň	k1gFnSc2	sebekázeň
k	k	k7c3	k
vyhraněnému	vyhraněný	k2eAgInSc3d1	vyhraněný
individualismu	individualismus	k1gInSc3	individualismus
a	a	k8xC	a
etickému	etický	k2eAgNnSc3d1	etické
<g/>
,	,	kIx,	,
filozofickému	filozofický	k2eAgNnSc3d1	filozofické
i	i	k8xC	i
estetickému	estetický	k2eAgInSc3d1	estetický
relativismu	relativismus	k1gInSc3	relativismus
<g/>
.	.	kIx.	.
</s>
<s>
Výrazem	výraz	k1gInSc7	výraz
tohoto	tento	k3xDgInSc2	tento
přerodu	přerod	k1gInSc2	přerod
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
lyrická	lyrický	k2eAgFnSc1d1	lyrická
próza	próza	k1gFnSc1	próza
Pozemské	pozemský	k2eAgFnSc2d1	pozemská
živiny	živina	k1gFnSc2	živina
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
jakýsi	jakýsi	k3yIgInSc4	jakýsi
Gidův	Gidův	k2eAgInSc4d1	Gidův
manifest	manifest	k1gInSc4	manifest
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
svůj	svůj	k3xOyFgInSc4	svůj
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
osvobození	osvobození	k1gNnSc4	osvobození
vlastního	vlastní	k2eAgNnSc2d1	vlastní
já	já	k3xPp1nSc1	já
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
představuje	představovat	k5eAaImIp3nS	představovat
jakýsi	jakýsi	k3yIgInSc4	jakýsi
asketismus	asketismus	k1gInSc4	asketismus
naruby	naruby	k6eAd1	naruby
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
smysly	smysl	k1gInPc7	smysl
musí	muset	k5eAaImIp3nP	muset
otevřít	otevřít	k5eAaPmF	otevřít
všemu	všecek	k3xTgNnSc3	všecek
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc1d1	přirozené
<g/>
,	,	kIx,	,
lidský	lidský	k2eAgInSc4d1	lidský
život	život	k1gInSc4	život
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
sobě	se	k3xPyFc3	se
dostatečným	dostatečný	k2eAgInSc7d1	dostatečný
cílem	cíl	k1gInSc7	cíl
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgInPc4d1	náboženský
příkazy	příkaz	k1gInPc4	příkaz
a	a	k8xC	a
morální	morální	k2eAgFnPc4d1	morální
konvence	konvence	k1gFnPc4	konvence
nezavazují	zavazovat	k5eNaImIp3nP	zavazovat
<g/>
.	.	kIx.	.
</s>
<s>
Subjektem	subjekt	k1gInSc7	subjekt
i	i	k9	i
objektem	objekt	k1gInSc7	objekt
jeho	jeho	k3xOp3gFnSc2	jeho
analýzy	analýza	k1gFnSc2	analýza
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nechce	chtít	k5eNaImIp3nS	chtít
být	být	k5eAaImF	být
spoután	spoután	k2eAgInSc4d1	spoután
žádnými	žádný	k3yNgInPc7	žádný
principy	princip	k1gInPc7	princip
a	a	k8xC	a
ve	v	k7c6	v
všem	všecek	k3xTgNnSc6	všecek
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
jeho	jeho	k3xOp3gInPc4	jeho
epické	epický	k2eAgInPc4d1	epický
příběhy	příběh	k1gInPc4	příběh
Imoralista	imoralista	k1gMnSc1	imoralista
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Těsná	těsný	k2eAgFnSc1d1	těsná
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vatikánské	vatikánský	k2eAgFnPc1d1	Vatikánská
kobky	kobka	k1gFnPc1	kobka
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
i	i	k8xC	i
Pastorální	pastorální	k2eAgFnPc1d1	pastorální
symfonie	symfonie	k1gFnPc1	symfonie
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
sledují	sledovat	k5eAaImIp3nP	sledovat
a	a	k8xC	a
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
konflikty	konflikt	k1gInPc1	konflikt
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgInPc1d1	vznikající
ze	z	k7c2	z
střetu	střet	k1gInSc2	střet
silného	silný	k2eAgMnSc2d1	silný
jedince	jedinec	k1gMnSc2	jedinec
s	s	k7c7	s
omezujícím	omezující	k2eAgInSc7d1	omezující
vlivem	vliv	k1gInSc7	vliv
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
založil	založit	k5eAaPmAgInS	založit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jacquesem	Jacques	k1gMnSc7	Jacques
Copeauem	Copeau	k1gMnSc7	Copeau
a	a	k8xC	a
Jeanem	Jean	k1gMnSc7	Jean
Schlumbergerem	Schlumberger	k1gMnSc7	Schlumberger
literární	literární	k2eAgInSc4d1	literární
magazín	magazín	k1gInSc4	magazín
La	la	k1gNnSc2	la
Nouvelle	Nouvelle	k1gNnSc7	Nouvelle
Revue	revue	k1gFnSc2	revue
Française	Française	k1gFnSc2	Française
(	(	kIx(	(
<g/>
NRF	NRF	kA	NRF
<g/>
)	)	kIx)	)
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
jej	on	k3xPp3gMnSc4	on
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Magazín	magazín	k1gInSc1	magazín
se	se	k3xPyFc4	se
hlásil	hlásit	k5eAaImAgInS	hlásit
k	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
klasicismu	klasicismus	k1gInSc3	klasicismus
a	a	k8xC	a
hodlal	hodlat	k5eAaImAgMnS	hodlat
uvádět	uvádět	k5eAaImF	uvádět
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
literární	literární	k2eAgNnSc4d1	literární
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
podporoval	podporovat	k5eAaImAgInS	podporovat
nějaké	nějaký	k3yIgFnPc4	nějaký
literární	literární	k2eAgFnPc4d1	literární
doktríny	doktrína	k1gFnPc4	doktrína
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gNnSc3	jeho
doporučení	doporučení	k1gNnSc3	doporučení
tak	tak	k6eAd1	tak
vyšel	vyjít	k5eAaPmAgInS	vyjít
například	například	k6eAd1	například
román	román	k1gInSc1	román
Jean	Jean	k1gMnSc1	Jean
Barois	Barois	k1gFnSc1	Barois
od	od	k7c2	od
Martina	Martin	k1gInSc2	Martin
du	du	k?	du
Garda	garda	k1gFnSc1	garda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
své	svůj	k3xOyFgFnSc2	svůj
popularity	popularita	k1gFnSc2	popularita
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
začal	začít	k5eAaPmAgMnS	začít
pronikat	pronikat	k5eAaImF	pronikat
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
současné	současný	k2eAgInPc4d1	současný
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
jeho	jeho	k3xOp3gInSc4	jeho
román	román	k1gInSc4	román
Penězokazi	penězokaz	k1gMnPc1	penězokaz
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
sobě	se	k3xPyFc3	se
samém	samý	k3xTgInSc6	samý
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
s	s	k7c7	s
naprostou	naprostý	k2eAgFnSc7d1	naprostá
upřímností	upřímnost	k1gFnSc7	upřímnost
v	v	k7c6	v
autobiografickém	autobiografický	k2eAgInSc6d1	autobiografický
románu	román	k1gInSc6	román
Zemři	zemřít	k5eAaPmRp2nS	zemřít
a	a	k8xC	a
živ	živit	k5eAaImRp2nS	živit
budeš	být	k5eAaImBp2nS	být
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
přiznal	přiznat	k5eAaPmAgMnS	přiznat
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
homosexualitě	homosexualita	k1gFnSc3	homosexualita
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
ženat	ženat	k2eAgMnSc1d1	ženat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestřenicí	sestřenice	k1gFnSc7	sestřenice
Madeleine	Madeleine	k1gFnSc7	Madeleine
Rondeauxovou	Rondeauxový	k2eAgFnSc7d1	Rondeauxový
<g/>
)	)	kIx)	)
a	a	k8xC	a
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgInS	začít
projevovat	projevovat	k5eAaImF	projevovat
sympatie	sympatie	k1gFnPc4	sympatie
k	k	k7c3	k
úsilí	úsilí	k1gNnSc3	úsilí
o	o	k7c4	o
sociální	sociální	k2eAgFnPc4d1	sociální
přestavbu	přestavba	k1gFnSc4	přestavba
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
okouzlen	okouzlit	k5eAaPmNgMnS	okouzlit
projektem	projekt	k1gInSc7	projekt
komunistické	komunistický	k2eAgFnSc2d1	komunistická
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ovšem	ovšem	k9	ovšem
chápal	chápat	k5eAaImAgInS	chápat
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
jako	jako	k9	jako
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
moderně	moderně	k6eAd1	moderně
reformovaných	reformovaný	k2eAgInPc2d1	reformovaný
vztahů	vztah	k1gInPc2	vztah
prvotního	prvotní	k2eAgNnSc2d1	prvotní
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
André	André	k1gMnSc7	André
Malrauxem	Malraux	k1gInSc7	Malraux
intervenoval	intervenovat	k5eAaImAgInS	intervenovat
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
uvězněného	uvězněný	k2eAgMnSc2d1	uvězněný
komunistického	komunistický	k2eAgMnSc2d1	komunistický
funkcionáře	funkcionář	k1gMnSc2	funkcionář
Georgi	Georgi	k1gNnSc2	Georgi
Dimitrova	Dimitrův	k2eAgInSc2d1	Dimitrův
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
navštívil	navštívit	k5eAaPmAgInS	navštívit
dokonce	dokonce	k9	dokonce
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Hluboké	hluboký	k2eAgNnSc1d1	hluboké
rozčarování	rozčarování	k1gNnSc1	rozčarování
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
cesty	cesta	k1gFnSc2	cesta
pak	pak	k6eAd1	pak
popsal	popsat	k5eAaPmAgMnS	popsat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
reportáži	reportáž	k1gFnSc6	reportáž
Návrat	návrat	k1gInSc1	návrat
ze	z	k7c2	z
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
ostrou	ostrý	k2eAgFnSc4d1	ostrá
kritiku	kritika	k1gFnSc4	kritika
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
levicových	levicový	k2eAgMnPc2d1	levicový
intelektuálů	intelektuál	k1gMnPc2	intelektuál
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
nemělo	mít	k5eNaImAgNnS	mít
se	se	k3xPyFc4	se
sovětským	sovětský	k2eAgNnSc7d1	sovětské
prostředím	prostředí	k1gNnSc7	prostředí
vlastní	vlastní	k2eAgFnSc2d1	vlastní
zkušenosti	zkušenost	k1gFnSc2	zkušenost
(	(	kIx(	(
<g/>
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
například	například	k6eAd1	například
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
s	s	k7c7	s
polemikou	polemika	k1gFnSc7	polemika
básník	básník	k1gMnSc1	básník
Stanislav	Stanislav	k1gMnSc1	Stanislav
Kostka	Kostka	k1gMnSc1	Kostka
Neumann	Neumann	k1gMnSc1	Neumann
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Anti-Gide	Anti-Gid	k1gInSc5	Anti-Gid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
jeho	jeho	k3xOp3gFnPc4	jeho
obsáhlé	obsáhlý	k2eAgFnPc4d1	obsáhlá
a	a	k8xC	a
umělecky	umělecky	k6eAd1	umělecky
významné	významný	k2eAgNnSc4d1	významné
literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
lidské	lidský	k2eAgInPc4d1	lidský
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
vystihl	vystihnout	k5eAaPmAgMnS	vystihnout
s	s	k7c7	s
neochvějnou	neochvějný	k2eAgFnSc7d1	neochvějná
láskou	láska	k1gFnSc7	láska
k	k	k7c3	k
pravdě	pravda	k1gFnSc3	pravda
a	a	k8xC	a
psychologickou	psychologický	k2eAgFnSc7d1	psychologická
bystrozrakostí	bystrozrakost	k1gFnSc7	bystrozrakost
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
'	'	kIx"	'
<g/>
Cimetiè	Cimetiè	k1gFnSc4	Cimetiè
paroissiale	paroissiala	k1gFnSc3	paroissiala
de	de	k?	de
l	l	kA	l
'	'	kIx"	'
<g/>
eglise	eglise	k1gFnSc1	eglise
Saint	Saint	k1gMnSc1	Saint
Gabriel	Gabriel	k1gMnSc1	Gabriel
<g/>
'	'	kIx"	'
v	v	k7c6	v
Cuverville	Cuvervilla	k1gFnSc6	Cuvervilla
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Seine-Maritime	Seine-Maritim	k1gInSc5	Seine-Maritim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
zařadila	zařadit	k5eAaPmAgFnS	zařadit
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
spisy	spis	k1gInPc4	spis
A.	A.	kA	A.
Gida	Gida	k1gFnSc1	Gida
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Les	les	k1gInSc1	les
Cahiers	Cahiers	k1gInSc1	Cahiers
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
André	André	k1gMnSc1	André
Walter	Walter	k1gMnSc1	Walter
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
Sešity	sešit	k1gInPc1	sešit
Andrého	André	k1gMnSc2	André
Waltera	Walter	k1gMnSc2	Walter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
románový	románový	k2eAgInSc1d1	románový
deník	deník	k1gInSc1	deník
vydaný	vydaný	k2eAgInSc1d1	vydaný
anonymně	anonymně	k6eAd1	anonymně
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc1	Le
Traité	Traitý	k2eAgFnSc2d1	Traitý
du	du	k?	du
Narcisse	Narcisse	k1gFnSc2	Narcisse
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
Narkissos	Narkissos	k1gInSc1	Narkissos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
esej	esej	k1gFnSc1	esej
o	o	k7c6	o
podstatě	podstata	k1gFnSc6	podstata
symbolu	symbol	k1gInSc2	symbol
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Poésies	Poésies	k1gInSc1	Poésies
d	d	k?	d
<g/>
<g />
.	.	kIx.	.
</s>
<s>
'	'	kIx"	'
<g/>
André	André	k1gMnSc1	André
Walter	Walter	k1gMnSc1	Walter
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
Básně	báseň	k1gFnPc1	báseň
Andrého	André	k1gMnSc2	André
Waltera	Walter	k1gMnSc2	Walter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
básní	básnit	k5eAaImIp3nS	básnit
vydaná	vydaný	k2eAgFnSc1d1	vydaná
anonymně	anonymně	k6eAd1	anonymně
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc1	Le
Traité	Traitý	k2eAgFnSc2d1	Traitý
du	du	k?	du
Narcisse	Narcisse	k1gFnSc2	Narcisse
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
Narkissos	Narkissos	k1gInSc1	Narkissos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
esej	esej	k1gInSc1	esej
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Voyage	Voyag	k1gFnSc2	Voyag
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Urien	Urien	k1gInSc1	Urien
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
urianova	urianův	k2eAgFnSc1d1	urianův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fiktivní	fiktivní	k2eAgInSc1d1	fiktivní
cestopis	cestopis	k1gInSc1	cestopis
<g/>
,	,	kIx,	,
Paludes	Paludes	k1gInSc1	Paludes
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
satirický	satirický	k2eAgInSc1d1	satirický
obraz	obraz	k1gInSc1	obraz
pařížského	pařížský	k2eAgNnSc2d1	pařížské
literárního	literární	k2eAgNnSc2d1	literární
"	"	kIx"	"
<g/>
bahnění	bahnění	k1gNnSc2	bahnění
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nenalézajícího	nalézající	k2eNgInSc2d1	nalézající
odvahu	odvaha	k1gFnSc4	odvaha
ke	k	k7c3	k
svobodě	svoboda	k1gFnSc3	svoboda
a	a	k8xC	a
odvážnosti	odvážnost	k1gFnSc3	odvážnost
činu	čin	k1gInSc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Les	les	k1gInSc1	les
nourritures	nourritures	k1gMnSc1	nourritures
terrestres	terrestres	k1gMnSc1	terrestres
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
Pozemské	pozemský	k2eAgFnPc4d1	pozemská
živiny	živina	k1gFnPc4	živina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lyrická	lyrický	k2eAgFnSc1d1	lyrická
próza	próza	k1gFnSc1	próza
<g/>
,	,	kIx,	,
proklamace	proklamace	k1gFnPc1	proklamace
autorova	autorův	k2eAgInSc2d1	autorův
nového	nový	k2eAgInSc2d1	nový
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
usilujícího	usilující	k2eAgMnSc4d1	usilující
o	o	k7c4	o
možnost	možnost	k1gFnSc4	možnost
plného	plný	k2eAgNnSc2d1	plné
rozvinutí	rozvinutí	k1gNnSc2	rozvinutí
a	a	k8xC	a
uplatnění	uplatnění	k1gNnSc2	uplatnění
osbnosti	osbnost	k1gFnSc2	osbnost
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
odkládá	odkládat	k5eAaImIp3nS	odkládat
vnucenou	vnucený	k2eAgFnSc4d1	vnucená
mu	on	k3xPp3gMnSc3	on
morálku	morálka	k1gFnSc4	morálka
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
být	být	k5eAaImF	být
nezávislý	závislý	k2eNgInSc1d1	nezávislý
na	na	k7c6	na
lidském	lidský	k2eAgNnSc6d1	lidské
společenství	společenství	k1gNnSc6	společenství
a	a	k8xC	a
sleduje	sledovat	k5eAaImIp3nS	sledovat
jen	jen	k9	jen
své	svůj	k3xOyFgFnPc4	svůj
individuální	individuální	k2eAgFnPc4d1	individuální
potřeby	potřeba	k1gFnPc4	potřeba
a	a	k8xC	a
cíle	cíl	k1gInPc4	cíl
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
překážky	překážka	k1gFnPc4	překážka
a	a	k8xC	a
i	i	k9	i
na	na	k7c6	na
oběti	oběť	k1gFnSc6	oběť
druhých	druhý	k4xOgMnPc2	druhý
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Le	Le	k?	Le
Prométhée	Prométhée	k1gInSc1	Prométhée
mal	málit	k5eAaImRp2nS	málit
enchaîné	enchaîné	k1gNnSc4	enchaîné
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
Prométheus	Prométheus	k1gMnSc1	Prométheus
špatně	špatně	k6eAd1	špatně
připoutaný	připoutaný	k2eAgMnSc1d1	připoutaný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
s	s	k7c7	s
dramatickými	dramatický	k2eAgInPc7d1	dramatický
dialogy	dialog	k1gInPc7	dialog
<g/>
,	,	kIx,	,
Le	Le	k1gFnPc7	Le
Roi	Roi	k1gMnPc2	Roi
Candaule	Candaule	k1gFnSc2	Candaule
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
Kandaules	Kandaules	k1gMnSc1	Kandaules
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Immoraliste	Immoralist	k1gMnSc5	Immoralist
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
Imoralista	imoralista	k1gMnSc1	imoralista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
egoistický	egoistický	k2eAgMnSc1d1	egoistický
hrdina	hrdina	k1gMnSc1	hrdina
Michel	Michel	k1gMnSc1	Michel
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
těhotnou	těhotný	k2eAgFnSc7d1	těhotná
ženou	žena	k1gFnSc7	žena
Marcelinou	Marcelin	k2eAgFnSc7d1	Marcelina
uzdravit	uzdravit	k5eAaPmF	uzdravit
se	se	k3xPyFc4	se
z	z	k7c2	z
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Michel	Michel	k1gMnSc1	Michel
prožívá	prožívat	k5eAaImIp3nS	prožívat
obrodu	obroda	k1gFnSc4	obroda
smyslového	smyslový	k2eAgInSc2d1	smyslový
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Marcelinu	Marcelin	k2eAgFnSc4d1	Marcelina
horké	horký	k2eAgNnSc1d1	horké
a	a	k8xC	a
suché	suchý	k2eAgNnSc1d1	suché
podnebí	podnebí	k1gNnSc1	podnebí
zabijí	zabít	k5eAaPmIp3nP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Marcelina	Marcelin	k2eAgFnSc1d1	Marcelina
umírá	umírat	k5eAaImIp3nS	umírat
a	a	k8xC	a
Michel	Michel	k1gInSc4	Michel
<g/>
,	,	kIx,	,
uvolněný	uvolněný	k2eAgInSc4d1	uvolněný
z	z	k7c2	z
pout	pouto	k1gNnPc2	pouto
povinnosti	povinnost	k1gFnSc2	povinnost
o	o	k7c4	o
ohledů	ohled	k1gInPc2	ohled
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
životní	životní	k2eAgFnSc3d1	životní
družce	družka	k1gFnSc3	družka
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vychutnávat	vychutnávat	k5eAaImF	vychutnávat
rozkoše	rozkoš	k1gFnPc4	rozkoš
uzdraveného	uzdravený	k2eAgMnSc2d1	uzdravený
a	a	k8xC	a
hledat	hledat	k5eAaImF	hledat
jen	jen	k9	jen
takové	takový	k3xDgNnSc4	takový
štěstí	štěstí	k1gNnSc4	štěstí
a	a	k8xC	a
blaho	blaho	k1gNnSc4	blaho
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
jeho	jeho	k3xOp3gFnSc1	jeho
osobní	osobní	k2eAgFnSc1d1	osobní
představě	představa	k1gFnSc3	představa
<g/>
.	.	kIx.	.
</s>
<s>
Prétextes	Prétextes	k1gInSc1	Prétextes
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Záminky	záminka	k1gFnPc1	záminka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
Saül	Saül	k1gInSc1	Saül
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Saul	Saul	k1gMnSc1	Saul
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc1	Le
Prométhée	Prométhé	k1gFnSc2	Prométhé
mal	málit	k5eAaImRp2nS	málit
enchaîné	enchaîný	k2eAgNnSc4d1	enchaîný
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Prométheus	Prométheus	k1gMnSc1	Prométheus
špatně	špatně	k6eAd1	špatně
připoutaný	připoutaný	k2eAgMnSc1d1	připoutaný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
Dostoievsky	Dostoievsko	k1gNnPc7	Dostoievsko
d	d	k?	d
<g/>
<g />
.	.	kIx.	.
</s>
<s>
'	'	kIx"	'
<g/>
apres	apres	k1gInSc1	apres
sa	sa	k?	sa
correspondance	correspondance	k1gFnSc2	correspondance
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Dostojevskij	Dostojevskij	k1gFnSc1	Dostojevskij
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
korespondence	korespondence	k1gFnSc2	korespondence
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
esej	esej	k1gFnSc1	esej
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Porte	port	k1gInSc5	port
étroite	étroit	k1gInSc5	étroit
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
Těsná	těsný	k2eAgFnSc1d1	těsná
brána	brána	k1gFnSc1	brána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Gide	Gide	k1gFnSc1	Gide
komentuje	komentovat	k5eAaBmIp3nS	komentovat
životní	životní	k2eAgFnSc4d1	životní
cestu	cesta	k1gFnSc4	cesta
dívky	dívka	k1gFnSc2	dívka
Alissy	Alissa	k1gFnSc2	Alissa
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
po	po	k7c6	po
prvním	první	k4xOgNnSc6	první
milostném	milostný	k2eAgNnSc6d1	milostné
vzplanutí	vzplanutí	k1gNnSc6	vzplanutí
stane	stanout	k5eAaPmIp3nS	stanout
staropanenská	staropanenský	k2eAgFnSc1d1	staropanenská
pobožnůstkářka	pobožnůstkářka	k1gFnSc1	pobožnůstkářka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zničí	zničit	k5eAaPmIp3nS	zničit
život	život	k1gInSc4	život
i	i	k8xC	i
svému	svůj	k3xOyFgMnSc3	svůj
milému	milý	k1gMnSc3	milý
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
vyznívá	vyznívat	k5eAaImIp3nS	vyznívat
tragicky	tragicky	k6eAd1	tragicky
a	a	k8xC	a
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
Gideovo	Gideův	k2eAgNnSc1d1	Gideův
odmítnutí	odmítnutí	k1gNnSc1	odmítnutí
zříkání	zříkání	k1gNnSc2	zříkání
se	se	k3xPyFc4	se
pozemského	pozemský	k2eAgInSc2d1	pozemský
života	život	k1gInSc2	život
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
"	"	kIx"	"
<g/>
svatosti	svatost	k1gFnSc2	svatost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
takový	takový	k3xDgInSc1	takový
život	život	k1gInSc1	život
vyústí	vyústit	k5eAaPmIp3nS	vyústit
v	v	k7c6	v
zoufalství	zoufalství	k1gNnSc6	zoufalství
a	a	k8xC	a
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
neužitečnost	neužitečnost	k1gFnSc4	neužitečnost
oběti	oběť	k1gFnSc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Oscar	Oscar	k1gInSc1	Oscar
Wilde	Wild	k1gInSc5	Wild
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
esej	esej	k1gFnSc1	esej
<g/>
,	,	kIx,	,
Charles-Louis	Charles-Louis	k1gFnSc1	Charles-Louis
Philippe	Philipp	k1gInSc5	Philipp
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
esej	esej	k1gInSc1	esej
<g/>
,	,	kIx,	,
Nouveaux	Nouveaux	k1gInSc1	Nouveaux
Prétextes	Prétextes	k1gInSc1	Prétextes
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnSc2d1	Nové
záminky	záminka	k1gFnSc2	záminka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
Isabelle	Isabelle	k1gFnPc1	Isabelle
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
Isabela	Isabela	k1gFnSc1	Isabela
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Retour	Retour	k1gMnSc1	Retour
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Enfant	Enfant	k1gMnSc1	Enfant
prodigue	prodigu	k1gMnSc2	prodigu
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
Návrat	návrat	k1gInSc1	návrat
marnotratného	marnotratný	k2eAgMnSc2d1	marnotratný
syna	syn	k1gMnSc2	syn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Caves	Caves	k1gInSc1	Caves
du	du	k?	du
Vatican	Vatican	k1gInSc1	Vatican
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
Vatikánské	vatikánský	k2eAgFnSc2d1	Vatikánská
kobky	kobka	k1gFnSc2	kobka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
označený	označený	k2eAgInSc1d1	označený
autorem	autor	k1gMnSc7	autor
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
bláznovská	bláznovský	k2eAgFnSc1d1	bláznovská
fraška	fraška	k1gFnSc1	fraška
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
satirou	satira	k1gFnSc7	satira
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
předmětem	předmět	k1gInSc7	předmět
je	být	k5eAaImIp3nS	být
náboženská	náboženský	k2eAgFnSc1d1	náboženská
bigotnost	bigotnost	k1gFnSc1	bigotnost
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
založený	založený	k2eAgMnSc1d1	založený
na	na	k7c6	na
"	"	kIx"	"
<g/>
šeptandě	šeptanda	k1gFnSc6	šeptanda
<g/>
"	"	kIx"	"
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
Lev	Lev	k1gMnSc1	Lev
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
vězněn	věznit	k5eAaImNgMnS	věznit
na	na	k7c6	na
Andělském	andělský	k2eAgInSc6d1	andělský
hradě	hrad	k1gInSc6	hrad
svobodnými	svobodný	k2eAgMnPc7d1	svobodný
zednáři	zednář	k1gMnPc7	zednář
a	a	k8xC	a
že	že	k8xS	že
jimi	on	k3xPp3gFnPc7	on
dosazený	dosazený	k2eAgInSc1d1	dosazený
lžipapež	lžipapež	k6eAd1	lžipapež
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vydal	vydat	k5eAaPmAgInS	vydat
encykliku	encyklika	k1gFnSc4	encyklika
Rerum	Rerum	k1gNnSc4	Rerum
novarum	novarum	k1gInSc1	novarum
<g/>
,	,	kIx,	,
utužuje	utužovat	k5eAaImIp3nS	utužovat
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
vládu	vláda	k1gFnSc4	vláda
ďáblovu	ďáblův	k2eAgFnSc4d1	Ďáblova
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
groteskního	groteskní	k2eAgNnSc2d1	groteskní
pásma	pásmo	k1gNnSc2	pásmo
podvodných	podvodný	k2eAgFnPc2d1	podvodná
machinací	machinace	k1gFnPc2	machinace
zločinecké	zločinecký	k2eAgFnSc2d1	zločinecká
bandy	banda	k1gFnSc2	banda
a	a	k8xC	a
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
bezdůvodnou	bezdůvodný	k2eAgFnSc7d1	bezdůvodná
vraždou	vražda	k1gFnSc7	vražda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pojata	pojat	k2eAgFnSc1d1	pojata
jako	jako	k8xC	jako
volní	volní	k2eAgInSc1d1	volní
akt	akt	k1gInSc1	akt
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
necouvajícího	couvající	k2eNgMnSc2d1	couvající
ani	ani	k8xC	ani
před	před	k7c7	před
zločinem	zločin	k1gInSc7	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
Gidův	Gidův	k2eAgInSc4d1	Gidův
rozchod	rozchod	k1gInSc4	rozchod
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Claudelem	Claudel	k1gMnSc7	Claudel
<g/>
.	.	kIx.	.
</s>
<s>
La	la	k1gNnSc1	la
Symphonie	Symphonie	k1gFnSc2	Symphonie
pastorale	pastorale	k6eAd1	pastorale
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Pastorální	pastorální	k2eAgFnSc1d1	pastorální
symfonie	symfonie	k1gFnSc1	symfonie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavní	hlavní	k2eAgFnSc7d1	hlavní
hrdinkou	hrdinka	k1gFnSc7	hrdinka
je	být	k5eAaImIp3nS	být
slepá	slepý	k2eAgFnSc1d1	slepá
Gertruda	Gertruda	k1gFnSc1	Gertruda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nenabyla	nabýt	k5eNaPmAgFnS	nabýt
operací	operace	k1gFnSc7	operace
zraku	zrak	k1gInSc2	zrak
<g/>
,	,	kIx,	,
neznala	neznat	k5eAaImAgFnS	neznat
klamy	klam	k1gInPc4	klam
a	a	k8xC	a
bludy	blud	k1gInPc4	blud
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
láska	láska	k1gFnSc1	láska
jí	on	k3xPp3gFnSc3	on
splývala	splývat	k5eAaImAgFnS	splývat
s	s	k7c7	s
představou	představa	k1gFnSc7	představa
citu	cit	k1gInSc2	cit
lásky	láska	k1gFnSc2	láska
ke	k	k7c3	k
všem	všecek	k3xTgMnPc3	všecek
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
se	s	k7c7	s
soucitem	soucit	k1gInSc7	soucit
a	a	k8xC	a
obětavostí	obětavost	k1gFnSc7	obětavost
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
s	s	k7c7	s
vráceným	vrácený	k2eAgInSc7d1	vrácený
zrakem	zrak	k1gInSc7	zrak
prohlédá	prohlédat	k5eAaImIp3nS	prohlédat
a	a	k8xC	a
přesvědčuje	přesvědčovat	k5eAaImIp3nS	přesvědčovat
se	se	k3xPyFc4	se
i	i	k9	i
o	o	k7c6	o
sobeckých	sobecký	k2eAgFnPc6d1	sobecká
a	a	k8xC	a
nízkých	nízký	k2eAgFnPc6d1	nízká
pohnutkách	pohnutka	k1gFnPc6	pohnutka
jednání	jednání	k1gNnSc2	jednání
svého	svůj	k3xOyFgMnSc2	svůj
pastorského	pastorský	k2eAgMnSc2d1	pastorský
ochránce	ochránce	k1gMnSc2	ochránce
a	a	k8xC	a
vychovatele	vychovatel	k1gMnSc2	vychovatel
<g/>
.	.	kIx.	.
</s>
<s>
Tragická	tragický	k2eAgFnSc1d1	tragická
deziluze	deziluze	k1gFnSc1	deziluze
pak	pak	k6eAd1	pak
uspíší	uspíšit	k5eAaPmIp3nS	uspíšit
její	její	k3xOp3gFnSc4	její
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
když	když	k8xS	když
poznání	poznání	k1gNnSc1	poznání
skutečnosti	skutečnost	k1gFnSc2	skutečnost
rozbilo	rozbít	k5eAaPmAgNnS	rozbít
její	její	k3xOp3gInSc4	její
sen	sen	k1gInSc4	sen
o	o	k7c6	o
člověku	člověk	k1gMnSc6	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ušlechtilosti	ušlechtilost	k1gFnSc2	ušlechtilost
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
zfilmoval	zfilmovat	k5eAaPmAgMnS	zfilmovat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
režisér	režisér	k1gMnSc1	režisér
Jean	Jean	k1gMnSc1	Jean
Delannoy	Delannoa	k1gFnSc2	Delannoa
<g/>
.	.	kIx.	.
</s>
<s>
Dostoievsky	Dostoievsky	k6eAd1	Dostoievsky
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
Dostojevskij	Dostojevskij	k1gFnSc1	Dostojevskij
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
esej	esej	k1gInSc1	esej
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Faux-monnayeurs	Fauxonnayeurs	k1gInSc1	Faux-monnayeurs
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Penězokazi	penězokaz	k1gMnPc1	penězokaz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
psán	psán	k2eAgInSc1d1	psán
inovativní	inovativní	k2eAgFnSc7d1	inovativní
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spoluvytvářela	spoluvytvářet	k5eAaImAgFnS	spoluvytvářet
literární	literární	k2eAgFnSc4d1	literární
modernu	moderna	k1gFnSc4	moderna
(	(	kIx(	(
<g/>
James	James	k1gMnSc1	James
Joyce	Joyce	k1gMnSc1	Joyce
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Bělyj	Bělyj	k1gMnSc1	Bělyj
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
předznamenala	předznamenat	k5eAaPmAgFnS	předznamenat
i	i	k9	i
estetiku	estetika	k1gFnSc4	estetika
francouzského	francouzský	k2eAgInSc2d1	francouzský
"	"	kIx"	"
<g/>
Nového	Nového	k2eAgInSc2d1	Nového
románu	román	k1gInSc2	román
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Syžet	syžet	k1gInSc1	syžet
románu	román	k1gInSc2	román
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
popis	popis	k1gInSc1	popis
pařížského	pařížský	k2eAgNnSc2d1	pařížské
prostředí	prostředí	k1gNnSc2	prostředí
měšťanských	měšťanský	k2eAgFnPc2d1	měšťanská
vrstev	vrstva	k1gFnPc2	vrstva
před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
falšováním	falšování	k1gNnSc7	falšování
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
mezemi	mez	k1gFnPc7	mez
volního	volní	k2eAgNnSc2d1	volní
jednání	jednání	k1gNnSc2	jednání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
v	v	k7c6	v
okamžicích	okamžik	k1gInPc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
může	moct	k5eAaImIp3nS	moct
druhé	druhý	k4xOgNnSc4	druhý
svou	svůj	k3xOyFgFnSc7	svůj
bezohledností	bezohlednost	k1gFnSc7	bezohlednost
i	i	k9	i
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
křehkostí	křehkost	k1gFnSc7	křehkost
dospívajících	dospívající	k2eAgMnPc2d1	dospívající
<g/>
,	,	kIx,	,
tématem	téma	k1gNnSc7	téma
šikany	šikana	k1gFnSc2	šikana
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
otevřeně	otevřeně	k6eAd1	otevřeně
traktována	traktován	k2eAgFnSc1d1	traktována
homosexualita	homosexualita	k1gFnSc1	homosexualita
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
lze	lze	k6eAd1	lze
pro	pro	k7c4	pro
Gideovu	Gideův	k2eAgFnSc4d1	Gideova
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
minoritě	minorita	k1gFnSc3	minorita
vykládat	vykládat	k5eAaImF	vykládat
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
i	i	k9	i
jako	jako	k8xC	jako
autobiografický	autobiografický	k2eAgInSc4d1	autobiografický
záznam	záznam	k1gInSc4	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Le	Le	k?	Le
Journal	Journal	k1gMnPc1	Journal
des	des	k1gNnSc2	des
Faux-Monnayeurs	Faux-Monnayeursa	k1gFnPc2	Faux-Monnayeursa
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Deník	deník	k1gInSc1	deník
Penězokazů	penězokaz	k1gMnPc2	penězokaz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
esej	esej	k1gInSc1	esej
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
Gide	Gide	k1gFnSc6	Gide
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
své	svůj	k3xOyFgInPc4	svůj
tvůrčí	tvůrčí	k2eAgInPc4d1	tvůrčí
postupy	postup	k1gInPc4	postup
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
románu	román	k1gInSc2	román
Penězokazi	penězokaz	k1gMnPc1	penězokaz
<g/>
.	.	kIx.	.
</s>
<s>
Si	se	k3xPyFc3	se
le	le	k?	le
grain	grain	k1gInSc1	grain
ne	ne	k9	ne
meurt	meurt	k1gInSc1	meurt
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Zemři	zemřít	k5eAaPmRp2nS	zemřít
a	a	k8xC	a
živ	živit	k5eAaImRp2nS	živit
budeš	být	k5eAaImBp2nS	být
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autobiografický	autobiografický	k2eAgInSc1d1	autobiografický
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
Voyage	Voyage	k1gInSc1	Voyage
au	au	k0	au
Congo	Congo	k1gMnSc1	Congo
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cestopis	cestopis	k1gInSc1	cestopis
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Retour	Retour	k1gMnSc1	Retour
du	du	k?	du
Tchad	Tchad	k1gInSc1	Tchad
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
Návrat	návrat	k1gInSc1	návrat
z	z	k7c2	z
Čadu	Čad	k1gInSc2	Čad
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cestopis	cestopis	k1gInSc1	cestopis
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
École	École	k1gInSc1	École
des	des	k1gNnSc1	des
femmes	femmes	k1gInSc1	femmes
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
žen	žena	k1gFnPc2	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
triptychu	triptych	k1gInSc2	triptych
vydaného	vydaný	k2eAgInSc2d1	vydaný
česky	česky	k6eAd1	česky
souhrnně	souhrnně	k6eAd1	souhrnně
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Nedokončené	dokončený	k2eNgNnSc4d1	nedokončené
vyznání	vyznání	k1gNnSc4	vyznání
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
triptychu	triptych	k1gInSc2	triptych
Nedokončené	dokončený	k2eNgNnSc4d1	nedokončené
vyznání	vyznání	k1gNnSc4	vyznání
<g/>
,	,	kIx,	,
Odipe	Odip	k1gMnSc5	Odip
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
Oidipús	Oidipús	k1gInSc1	Oidipús
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
Perséphone	Perséphon	k1gMnSc5	Perséphon
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Persefona	Persefona	k1gFnSc1	Persefona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Nouvelles	Nouvelles	k1gMnSc1	Nouvelles
Nourritures	Nourritures	k1gMnSc1	Nourritures
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnPc1d1	Nové
živiny	živina	k1gFnPc1	živina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
esej	esej	k1gFnSc4	esej
navazující	navazující	k2eAgFnSc4d1	navazující
na	na	k7c4	na
autorovy	autorův	k2eAgFnPc4d1	autorova
Pozemské	pozemský	k2eAgFnPc4d1	pozemská
živiny	živina	k1gFnPc4	živina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Genevieve	Genevieev	k1gFnPc1	Genevieev
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
Jenovefa	Jenovef	k1gMnSc2	Jenovef
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
<g/>
,	,	kIx,	,
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
díl	díl	k1gInSc4	díl
triptychu	triptych	k1gInSc2	triptych
Nedokončené	dokončený	k2eNgNnSc4d1	nedokončené
vyznání	vyznání	k1gNnSc4	vyznání
<g/>
,	,	kIx,	,
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
třech	tři	k4xCgInPc6	tři
románech	román	k1gInPc6	román
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
kladou	klást	k5eAaImIp3nP	klást
otázky	otázka	k1gFnPc4	otázka
po	po	k7c6	po
vratkosti	vratkost	k1gFnSc6	vratkost
rovnováhy	rovnováha	k1gFnSc2	rovnováha
mezi	mezi	k7c7	mezi
štěstím	štěstí	k1gNnSc7	štěstí
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
štěstím	štěstí	k1gNnSc7	štěstí
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
zájmy	zájem	k1gInPc4	zájem
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
hrdinka	hrdinka	k1gFnSc1	hrdinka
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
nešťastného	šťastný	k2eNgNnSc2d1	nešťastné
manželství	manželství	k1gNnSc2	manželství
a	a	k8xC	a
až	až	k9	až
její	její	k3xOp3gFnSc1	její
dcera	dcera	k1gFnSc1	dcera
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
zpětně	zpětně	k6eAd1	zpětně
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
její	její	k3xOp3gMnPc4	její
opravdové	opravdový	k2eAgMnPc4d1	opravdový
city	cit	k1gInPc4	cit
a	a	k8xC	a
sexuální	sexuální	k2eAgNnSc4d1	sexuální
zaměření	zaměření	k1gNnSc4	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Retour	Retour	k1gMnSc1	Retour
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
<g/>
S.	S.	kA	S.
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
Návrat	návrat	k1gInSc1	návrat
ze	z	k7c2	z
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
)	)	kIx)	)
a	a	k8xC	a
Retouches	Retouches	k1gMnSc1	Retouches
a	a	k8xC	a
mon	mon	k?	mon
Retour	Retour	k1gMnSc1	Retour
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
<g/>
S.	S.	kA	S.
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
Retuše	retuš	k1gFnPc1	retuš
k	k	k7c3	k
mému	můj	k3xOp1gInSc3	můj
Návratu	návrat	k1gInSc3	návrat
ze	z	k7c2	z
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
reportáže	reportáž	k1gFnPc4	reportáž
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgNnPc6	který
autor	autor	k1gMnSc1	autor
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
negativních	negativní	k2eAgInPc2d1	negativní
jevů	jev	k1gInPc2	jev
ve	v	k7c6	v
stalinském	stalinský	k2eAgNnSc6d1	stalinské
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
většina	většina	k1gFnSc1	většina
levicových	levicový	k2eAgMnPc2d1	levicový
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
navštívili	navštívit	k5eAaPmAgMnP	navštívit
<g/>
,	,	kIx,	,
odmítala	odmítat	k5eAaImAgFnS	odmítat
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
knihy	kniha	k1gFnPc1	kniha
vzbudily	vzbudit	k5eAaPmAgFnP	vzbudit
velký	velký	k2eAgInSc4d1	velký
rozruch	rozruch	k1gInSc4	rozruch
a	a	k8xC	a
vyvolaly	vyvolat	k5eAaPmAgInP	vyvolat
ostrou	ostrý	k2eAgFnSc4d1	ostrá
kritiku	kritika	k1gFnSc4	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Gide	Gide	k6eAd1	Gide
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
reportážích	reportáž	k1gFnPc6	reportáž
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
socialismus	socialismus	k1gInSc1	socialismus
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
horší	zlý	k2eAgInSc1d2	horší
systém	systém	k1gInSc1	systém
než	než	k8xS	než
kapitalismus	kapitalismus	k1gInSc1	kapitalismus
<g/>
.	.	kIx.	.
</s>
<s>
Levicoví	levicový	k2eAgMnPc1d1	levicový
intelektuálové	intelektuál	k1gMnPc1	intelektuál
knihy	kniha	k1gFnSc2	kniha
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
jako	jako	k8xS	jako
protisovětský	protisovětský	k2eAgInSc4d1	protisovětský
pamflet	pamflet	k1gInSc4	pamflet
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
nemělo	mít	k5eNaImAgNnS	mít
se	se	k3xPyFc4	se
sovětským	sovětský	k2eAgNnSc7d1	sovětské
prostředím	prostředí	k1gNnSc7	prostředí
vlastní	vlastní	k2eAgFnSc2d1	vlastní
zkušenosti	zkušenost	k1gFnSc2	zkušenost
(	(	kIx(	(
<g/>
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
například	například	k6eAd1	například
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
s	s	k7c7	s
polemikou	polemika	k1gFnSc7	polemika
básník	básník	k1gMnSc1	básník
Stanislav	Stanislav	k1gMnSc1	Stanislav
Kostka	Kostka	k1gMnSc1	Kostka
Neumann	Neumann	k1gMnSc1	Neumann
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Anti-Gide	Anti-Gid	k1gInSc5	Anti-Gid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Journal	Journat	k5eAaPmAgMnS	Journat
1889-1939	[number]	k4	1889-1939
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Deník	deník	k1gInSc1	deník
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gidovys	Gidovys	k1gInSc4	Gidovys
deníky	deník	k1gInPc7	deník
<g/>
,	,	kIx,	,
souhrn	souhrn	k1gInSc1	souhrn
autorových	autorův	k2eAgInPc2d1	autorův
životopisných	životopisný	k2eAgInPc2d1	životopisný
záznamů	záznam	k1gInPc2	záznam
<g/>
,	,	kIx,	,
glos	glosa	k1gFnPc2	glosa
a	a	k8xC	a
zápisků	zápisek	k1gInPc2	zápisek
<g/>
,	,	kIx,	,
Interviews	Interviewsa	k1gFnPc2	Interviewsa
imaginaires	imaginairesa	k1gFnPc2	imaginairesa
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Pomyslné	pomyslný	k2eAgFnSc2d1	pomyslná
rozmluvy	rozmluva	k1gFnSc2	rozmluva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
Thésée	Thésé	k1gInPc1	Thésé
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
Théseus	Théseus	k1gInSc1	Théseus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
Journal	Journal	k1gFnSc1	Journal
1939-1942	[number]	k4	1939-1942
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
Deník	deník	k1gInSc1	deník
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
Gidových	Gidův	k2eAgInPc2d1	Gidův
deníků	deník	k1gInPc2	deník
<g/>
,	,	kIx,	,
Poétique	Poétique	k1gFnSc1	Poétique
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Poetika	poetika	k1gFnSc1	poetika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
esej	esej	k1gInSc1	esej
<g/>
,	,	kIx,	,
Feuillets	Feuillets	k1gInSc1	Feuillets
<g />
.	.	kIx.	.
</s>
<s>
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
automne	automnout	k5eAaPmIp3nS	automnout
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
Podzimní	podzimní	k2eAgInPc1d1	podzimní
listy	list	k1gInPc1	list
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
Journal	Journal	k1gFnSc1	Journal
1942-1949	[number]	k4	1942-1949
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
Deník	deník	k1gInSc1	deník
1942	[number]	k4	1942
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
část	část	k1gFnSc1	část
Gidových	Gidův	k2eAgInPc2d1	Gidův
deníků	deník	k1gInPc2	deník
<g/>
,	,	kIx,	,
Journal	Journal	k1gMnSc5	Journal
intime	intimus	k1gMnSc5	intimus
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
Intimní	intimní	k2eAgInSc1d1	intimní
denik	denik	k1gInSc1	denik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Filoktétes	Filoktétes	k1gMnSc1	Filoktétes
<g/>
,	,	kIx,	,
Chrysanthem	Chrysanth	k1gInSc7	Chrysanth
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Arnošt	Arnošt	k1gMnSc1	Arnošt
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Prométheus	Prométheus	k1gMnSc1	Prométheus
špatně	špatně	k6eAd1	špatně
připoutaný	připoutaný	k2eAgMnSc1d1	připoutaný
<g/>
,	,	kIx,	,
Moderní	moderní	k2eAgFnPc1d1	moderní
revue	revue	k1gFnPc1	revue
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Arnošt	Arnošt	k1gMnSc1	Arnošt
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Těsná	těsný	k2eAgFnSc1d1	těsná
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
Kamilla	Kamilla	k1gFnSc1	Kamilla
Neumannová	Neumannová	k1gFnSc1	Neumannová
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Arnošt	Arnošt	k1gMnSc1	Arnošt
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Narkissos	Narkissos	k1gMnSc1	Narkissos
<g/>
,	,	kIx,	,
Filoktétes	Filoktétes	k1gMnSc1	Filoktétes
a	a	k8xC	a
Prométheus	Prométheus	k1gMnSc1	Prométheus
<g/>
,	,	kIx,	,
Kamilla	Kamilla	k1gMnSc1	Kamilla
Neumannová	Neumannová	k1gFnSc1	Neumannová
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Arnošt	Arnošt	k1gMnSc1	Arnošt
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Immoralista	Immoralista	k1gMnSc1	Immoralista
<g/>
,	,	kIx,	,
Kamilla	Kamilla	k1gMnSc1	Kamilla
Neumannová	Neumannová	k1gFnSc1	Neumannová
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jindřich	Jindřich	k1gMnSc1	Jindřich
Fleischner	Fleischner	k1gMnSc1	Fleischner
<g/>
,	,	kIx,	,
Betsabé	Betsabý	k2eAgFnPc1d1	Betsabý
<g/>
,	,	kIx,	,
Moderní	moderní	k2eAgFnSc1d1	moderní
revue	revue	k1gFnSc1	revue
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Arnošt	Arnošt	k1gMnSc1	Arnošt
<g />
.	.	kIx.	.
</s>
<s>
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
Kandaules	Kandaules	k1gMnSc1	Kandaules
<g/>
,	,	kIx,	,
Kamilla	Kamilla	k1gMnSc1	Kamilla
Neumannová	Neumannová	k1gFnSc1	Neumannová
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Arnošt	Arnošt	k1gMnSc1	Arnošt
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Saul	Saul	k1gMnSc1	Saul
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Ludvík	Ludvík	k1gMnSc1	Ludvík
Stříž	stříž	k1gFnSc1	stříž
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
Říše	říše	k1gFnSc1	říše
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Marek	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
Oscar	Oscar	k1gMnSc1	Oscar
Wilde	Wild	k1gMnSc5	Wild
<g/>
:	:	kIx,	:
In	In	k1gMnSc6	In
memoriam	memoriam	k1gInSc4	memoriam
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Otakar	Otakar	k1gMnSc1	Otakar
Levý	levý	k2eAgMnSc1d1	levý
<g/>
,	,	kIx,	,
Isabella	Isabella	k1gFnSc1	Isabella
<g/>
,	,	kIx,	,
Kamilla	Kamilla	k1gFnSc1	Kamilla
Neumannová	Neumannová	k1gFnSc1	Neumannová
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Arnošt	Arnošt	k1gMnSc1	Arnošt
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Návrat	návrat	k1gInSc1	návrat
ztraceného	ztracený	k2eAgMnSc2d1	ztracený
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Otakar	Otakar	k1gMnSc1	Otakar
Levý	levý	k2eAgMnSc1d1	levý
<g/>
,	,	kIx,	,
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Prorok	prorok	k1gMnSc1	prorok
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g />
.	.	kIx.	.
</s>
<s>
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Antonín	Antonín	k1gMnSc1	Antonín
Horský	Horský	k1gMnSc1	Horský
<g/>
,	,	kIx,	,
Návrat	návrat	k1gInSc1	návrat
marnotratného	marnotratný	k2eAgMnSc2d1	marnotratný
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
Vincenc	Vincenc	k1gMnSc1	Vincenc
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
Štorch-Marien	Štorch-Marien	k1gInSc1	Štorch-Marien
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Jirka	Jirka	k1gMnSc1	Jirka
Malá	Malá	k1gFnSc1	Malá
a	a	k8xC	a
O.	O.	kA	O.
Rádl	Rádl	k1gInSc1	Rádl
<g/>
,	,	kIx,	,
Vatikánské	vatikánský	k2eAgFnPc1d1	Vatikánská
kobky	kobka	k1gFnPc1	kobka
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Topič	topič	k1gInSc1	topič
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jiří	Jiří	k1gMnSc1	Jiří
Ježek	Ježek	k1gMnSc1	Ježek
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Penězokazi	penězokaz	k1gMnPc1	penězokaz
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnPc1d1	družstevní
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Heyduk	Heyduk	k1gMnSc1	Heyduk
<g/>
,	,	kIx,	,
Paludes	Paludes	k1gMnSc1	Paludes
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Synek	Synek	k1gMnSc1	Synek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Barták	Barták	k1gMnSc1	Barták
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Zemři	zemřít	k5eAaPmRp2nS	zemřít
a	a	k8xC	a
živ	živit	k5eAaImRp2nS	živit
budeš	být	k5eAaImBp2nS	být
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Janda	Janda	k1gMnSc1	Janda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
<g />
.	.	kIx.	.
</s>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
Winter	Winter	k1gMnSc1	Winter
<g/>
,	,	kIx,	,
Nedokončené	dokončený	k2eNgNnSc1d1	nedokončené
vyznání	vyznání	k1gNnSc1	vyznání
<g/>
,	,	kIx,	,
Evropský	evropský	k2eAgInSc1d1	evropský
literární	literární	k2eAgInSc1d1	literární
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Miloš	Miloš	k1gMnSc1	Miloš
Hlávka	Hlávka	k1gMnSc1	Hlávka
a	a	k8xC	a
Jindřich	Jindřich	k1gMnSc1	Jindřich
Hořejší	Hořejší	k1gMnSc1	Hořejší
<g/>
,	,	kIx,	,
Návrat	návrat	k1gInSc1	návrat
ze	z	k7c2	z
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohumil	Bohumil	k1gMnSc1	Bohumil
Mathesius	Mathesius	k1gMnSc1	Mathesius
<g/>
,	,	kIx,	,
šest	šest	k4xCc1	šest
vydání	vydání	k1gNnPc2	vydání
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
dvě	dva	k4xCgFnPc4	dva
vydání	vydání	k1gNnPc2	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Retuše	retuš	k1gFnPc1	retuš
k	k	k7c3	k
mému	můj	k3xOp1gInSc3	můj
Návratu	návrat	k1gInSc3	návrat
ze	z	k7c2	z
Sovětského	sovětský	k2eAgInSc2d1	sovětský
Svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
Julius	Julius	k1gMnSc1	Julius
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohumil	Bohumil	k1gMnSc1	Bohumil
Mathesius	Mathesius	k1gMnSc1	Mathesius
<g/>
,	,	kIx,	,
Pastorální	pastorální	k2eAgFnSc1d1	pastorální
symfonie	symfonie	k1gFnSc1	symfonie
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Šnajdr	Šnajdr	k1gMnSc1	Šnajdr
<g/>
,	,	kIx,	,
Kladno	Kladno	k1gNnSc1	Kladno
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Jindra	Jindra	k1gFnSc1	Jindra
Kvapilová	Kvapilová	k1gFnSc1	Kvapilová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Vilém	Vilém	k1gMnSc1	Vilém
Šmidt	Šmidt	k1gMnSc1	Šmidt
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Nedokončené	dokončený	k2eNgNnSc1d1	nedokončené
vyznání	vyznání	k1gNnSc1	vyznání
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Podroužek	Podroužek	k1gMnSc1	Podroužek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jindřich	Jindřich	k1gMnSc1	Jindřich
Hořejší	Hořejší	k1gMnSc1	Hořejší
<g/>
,	,	kIx,	,
Vatikánské	vatikánský	k2eAgFnPc1d1	Vatikánská
kobky	kobka	k1gFnPc1	kobka
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Penězokazi	penězokaz	k1gMnPc1	penězokaz
<g/>
,	,	kIx,	,
Deník	deník	k1gInSc1	deník
penězokazů	penězokaz	k1gMnPc2	penězokaz
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Heyduk	Heyduk	k1gMnSc1	Heyduk
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
francouzských	francouzský	k2eAgMnPc2d1	francouzský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
literatura	literatura	k1gFnSc1	literatura
Existencialismus	existencialismus	k1gInSc1	existencialismus
Galerie	galerie	k1gFnSc1	galerie
André	André	k1gMnSc2	André
Gide	Gid	k1gMnSc2	Gid
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
André	André	k1gMnSc2	André
Gide	Gid	k1gMnSc2	Gid
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Autor	autor	k1gMnSc1	autor
André	André	k1gMnSc7	André
Gide	Gid	k1gInSc2	Gid
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
André	André	k1gMnSc1	André
Gide	Gid	k1gFnSc2	Gid
Plné	plný	k2eAgInPc4d1	plný
texty	text	k1gInPc4	text
děl	dělo	k1gNnPc2	dělo
autora	autor	k1gMnSc2	autor
André	André	k1gMnSc2	André
Gide	Gid	k1gMnSc2	Gid
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
Osoba	osoba	k1gFnSc1	osoba
André	André	k1gMnSc2	André
Gide	Gid	k1gMnSc2	Gid
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Nobel	Nobel	k1gMnSc1	Nobel
Prize	Prize	k1gFnSc2	Prize
bio	bio	k?	bio
<g/>
,	,	kIx,	,
André	André	k1gMnSc2	André
Gide	Gid	k1gMnSc2	Gid
-	-	kIx~	-
Biography	Biographa	k1gMnSc2	Biographa
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
http://kirjasto.sci.fi/agide.htm	[url]	k6eAd1	http://kirjasto.sci.fi/agide.htm
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
http://www.gidiana.net	[url]	k5eAaPmF	http://www.gidiana.net
-	-	kIx~	-
francouzsky	francouzsky	k6eAd1	francouzsky
Gide	Gid	k1gMnSc2	Gid
<g/>
,	,	kIx,	,
André	André	k1gMnSc2	André
<g/>
.	.	kIx.	.
</s>
<s>
Cimetiè	Cimetiè	k?	Cimetiè
(	(	kIx(	(
<g/>
Cuverville	Cuverville	k1gFnSc2	Cuverville
<g/>
)	)	kIx)	)
Cimetiè	Cimetiè	k1gFnSc2	Cimetiè
paroissiale	paroissiala	k1gFnSc3	paroissiala
de	de	k?	de
l	l	kA	l
'	'	kIx"	'
<g/>
eglise	eglise	k1gFnSc1	eglise
Saint	Saint	k1gMnSc1	Saint
Gabriel	Gabriel	k1gMnSc1	Gabriel
(	(	kIx(	(
<g/>
André	André	k1gMnSc1	André
Gide	Gid	k1gFnSc2	Gid
<g/>
)	)	kIx)	)
</s>
