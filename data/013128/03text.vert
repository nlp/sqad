<p>
<s>
Přátelství	přátelství	k1gNnSc1	přátelství
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
on-line	onin	k1gInSc5	on-lin
Sociologické	sociologický	k2eAgFnPc1d1	sociologická
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
Sociologického	sociologický	k2eAgInSc2d1	sociologický
ústavu	ústav	k1gInSc2	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
reciproký	reciproký	k2eAgInSc4d1	reciproký
vztah	vztah	k1gInSc4	vztah
spojující	spojující	k2eAgFnSc2d1	spojující
dvě	dva	k4xCgNnPc1	dva
anebo	anebo	k8xC	anebo
více	hodně	k6eAd2	hodně
osob	osoba	k1gFnPc2	osoba
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
sympatií	sympatie	k1gFnPc2	sympatie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
sexuální	sexuální	k2eAgInSc4d1	sexuální
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
mezilidský	mezilidský	k2eAgInSc4d1	mezilidský
vztah	vztah	k1gInSc4	vztah
vyznačující	vyznačující	k2eAgInSc4d1	vyznačující
se	se	k3xPyFc4	se
vzájemnou	vzájemný	k2eAgFnSc7d1	vzájemná
náklonností	náklonnost	k1gFnSc7	náklonnost
a	a	k8xC	a
porozuměním	porozumění	k1gNnSc7	porozumění
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
přátelském	přátelský	k2eAgInSc6d1	přátelský
vztahu	vztah	k1gInSc6	vztah
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
svým	svůj	k3xOyFgMnPc3	svůj
protějškům	protějšek	k1gInPc3	protějšek
chovají	chovat	k5eAaImIp3nP	chovat
ohleduplně	ohleduplně	k6eAd1	ohleduplně
a	a	k8xC	a
chtějí	chtít	k5eAaImIp3nP	chtít
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
to	ten	k3xDgNnSc1	ten
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
také	také	k9	také
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
upřímnost	upřímnost	k1gFnSc1	upřímnost
<g/>
,	,	kIx,	,
důvěra	důvěra	k1gFnSc1	důvěra
a	a	k8xC	a
schopnost	schopnost	k1gFnSc1	schopnost
empatie	empatie	k1gFnSc1	empatie
<g/>
;	;	kIx,	;
přátelé	přítel	k1gMnPc1	přítel
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInPc3	svůj
protějškům	protějšek	k1gInPc3	protějšek
mohou	moct	k5eAaImIp3nP	moct
svěřit	svěřit	k5eAaPmF	svěřit
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
pocity	pocit	k1gInPc7	pocit
a	a	k8xC	a
názory	názor	k1gInPc7	názor
bez	bez	k7c2	bez
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
byli	být	k5eAaImAgMnP	být
odsuzováni	odsuzován	k2eAgMnPc1d1	odsuzován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přátelství	přátelství	k1gNnSc6	přátelství
je	být	k5eAaImIp3nS	být
také	také	k9	také
běžná	běžný	k2eAgFnSc1d1	běžná
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
nezištná	zištný	k2eNgFnSc1d1	nezištná
pomoc	pomoc	k1gFnSc1	pomoc
a	a	k8xC	a
touha	touha	k1gFnSc1	touha
trávit	trávit	k5eAaImF	trávit
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
protějškem	protějšek	k1gInSc7	protějšek
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přátelství	přátelství	k1gNnSc1	přátelství
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
charakteristikách	charakteristika	k1gFnPc6	charakteristika
shodné	shodný	k2eAgFnSc2d1	shodná
s	s	k7c7	s
láskou	láska	k1gFnSc7	láska
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
lásky	láska	k1gFnSc2	láska
ale	ale	k8xC	ale
přátelé	přítel	k1gMnPc1	přítel
nejsou	být	k5eNaImIp3nP	být
fyzicky	fyzicky	k6eAd1	fyzicky
(	(	kIx(	(
<g/>
sexuálně	sexuálně	k6eAd1	sexuálně
<g/>
)	)	kIx)	)
přitahováni	přitahován	k2eAgMnPc1d1	přitahován
svými	svůj	k3xOyFgInPc7	svůj
protějšky	protějšek	k1gInPc7	protějšek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
při	při	k7c6	při
krátkodobém	krátkodobý	k2eAgNnSc6d1	krátkodobé
odloučení	odloučení	k1gNnSc6	odloučení
zamilovaný	zamilovaný	k1gMnSc1	zamilovaný
člověk	člověk	k1gMnSc1	člověk
trpí	trpět	k5eAaImIp3nS	trpět
pocity	pocit	k1gInPc4	pocit
samoty	samota	k1gFnSc2	samota
a	a	k8xC	a
prázdnoty	prázdnota	k1gFnSc2	prázdnota
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
o	o	k7c4	o
nezdravou	zdravý	k2eNgFnSc4d1	nezdravá
formu	forma	k1gFnSc4	forma
závislosti	závislost	k1gFnSc2	závislost
než	než	k8xS	než
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
lásku	láska	k1gFnSc4	láska
bez	bez	k7c2	bez
sexuálních	sexuální	k2eAgInPc2d1	sexuální
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
vztah	vztah	k1gInSc4	vztah
platonická	platonický	k2eAgFnSc1d1	platonická
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
silný	silný	k2eAgInSc1d1	silný
citový	citový	k2eAgInSc1d1	citový
vztah	vztah	k1gInSc1	vztah
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
partnerů	partner	k1gMnPc2	partner
netouží	toužit	k5eNaImIp3nS	toužit
po	po	k7c6	po
sexuálním	sexuální	k2eAgInSc6d1	sexuální
styku	styk	k1gInSc6	styk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Forma	forma	k1gFnSc1	forma
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
člověk	člověk	k1gMnSc1	člověk
cítí	cítit	k5eAaImIp3nS	cítit
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
protějšku	protějšek	k1gInSc3	protějšek
hluboký	hluboký	k2eAgInSc1d1	hluboký
obdiv	obdiv	k1gInSc1	obdiv
<g/>
,	,	kIx,	,
trvale	trvale	k6eAd1	trvale
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
myslí	myslet	k5eAaImIp3nS	myslet
a	a	k8xC	a
při	při	k7c6	při
odloučení	odloučení	k1gNnSc6	odloučení
trpí	trpět	k5eAaImIp3nP	trpět
pocity	pocit	k1gInPc1	pocit
prázdnoty	prázdnota	k1gFnSc2	prázdnota
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
narcistický	narcistický	k2eAgInSc1d1	narcistický
obdiv	obdiv	k1gInSc1	obdiv
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
cit	cit	k1gInSc1	cit
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
každý	každý	k3xTgInSc4	každý
cit	cit	k1gInSc4	cit
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
jednostranný	jednostranný	k2eAgMnSc1d1	jednostranný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opakem	opak	k1gInSc7	opak
přátelství	přátelství	k1gNnSc2	přátelství
je	být	k5eAaImIp3nS	být
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
chce	chtít	k5eAaImIp3nS	chtít
člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
nepřátelském	přátelský	k2eNgInSc6d1	nepřátelský
vztahu	vztah	k1gInSc6	vztah
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
nepřítele	nepřítel	k1gMnSc4	nepřítel
to	ten	k3xDgNnSc4	ten
nejhorší	zlý	k2eAgNnSc1d3	nejhorší
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgMnSc3	ten
u	u	k7c2	u
falešného	falešný	k2eAgNnSc2d1	falešné
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
domnělých	domnělý	k2eAgMnPc2d1	domnělý
přátel	přítel	k1gMnPc2	přítel
za	za	k7c4	za
přátele	přítel	k1gMnPc4	přítel
pouze	pouze	k6eAd1	pouze
vydává	vydávat	k5eAaImIp3nS	vydávat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
tváří	tvářet	k5eAaImIp3nS	tvářet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zvláště	zvláště	k6eAd1	zvláště
<g/>
,	,	kIx,	,
když	když	k8xS	když
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
tuhého	tuhý	k2eAgInSc2d1	tuhý
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
jako	jako	k9	jako
přítel	přítel	k1gMnSc1	přítel
nechová	chovat	k5eNaImIp3nS	chovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
hlubinné	hlubinný	k2eAgFnSc2d1	hlubinná
psychologie	psychologie	k1gFnSc2	psychologie
je	být	k5eAaImIp3nS	být
přátelství	přátelství	k1gNnSc1	přátelství
definováno	definovat	k5eAaBmNgNnS	definovat
například	například	k6eAd1	například
teorií	teorie	k1gFnSc7	teorie
sociální	sociální	k2eAgFnSc2d1	sociální
směny	směna	k1gFnSc2	směna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Charakteristiky	charakteristika	k1gFnPc1	charakteristika
přátelství	přátelství	k1gNnSc2	přátelství
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
ontogeneze	ontogeneze	k1gFnSc2	ontogeneze
(	(	kIx(	(
<g/>
vývoje	vývoj	k1gInSc2	vývoj
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
podle	podle	k7c2	podle
věku	věk	k1gInSc2	věk
==	==	k?	==
</s>
</p>
<p>
<s>
Předškolní	předškolní	k2eAgInSc1d1	předškolní
věk	věk	k1gInSc1	věk
a	a	k8xC	a
raný	raný	k2eAgInSc1d1	raný
školní	školní	k2eAgInSc1d1	školní
věk	věk	k1gInSc1	věk
–	–	k?	–
typickými	typický	k2eAgInPc7d1	typický
znaky	znak	k1gInPc7	znak
dětských	dětský	k2eAgNnPc2d1	dětské
přátelství	přátelství	k1gNnSc2	přátelství
je	být	k5eAaImIp3nS	být
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
společnou	společný	k2eAgFnSc4d1	společná
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
uspokojování	uspokojování	k1gNnSc4	uspokojování
vlastních	vlastní	k2eAgFnPc2d1	vlastní
potřeb	potřeba	k1gFnPc2	potřeba
a	a	k8xC	a
posílení	posílení	k1gNnSc4	posílení
vlastního	vlastní	k2eAgInSc2d1	vlastní
statusu	status	k1gInSc2	status
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
krátkodobé	krátkodobý	k2eAgInPc1d1	krátkodobý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Starší	starý	k2eAgFnPc1d2	starší
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
adolescenti	adolescent	k1gMnPc1	adolescent
a	a	k8xC	a
dospělí	dospělí	k1gMnPc1	dospělí
–	–	k?	–
ty	ten	k3xDgInPc4	ten
naopak	naopak	k6eAd1	naopak
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
sdílení	sdílení	k1gNnSc4	sdílení
hlubších	hluboký	k2eAgInPc2d2	hlubší
zájmů	zájem	k1gInPc2	zájem
i	i	k8xC	i
citů	cit	k1gInPc2	cit
<g/>
,	,	kIx,	,
péče	péče	k1gFnSc1	péče
o	o	k7c4	o
druhého	druhý	k4xOgMnSc4	druhý
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
trvalé	trvalý	k2eAgFnPc1d1	trvalá
<g/>
;	;	kIx,	;
snesou	snést	k5eAaPmIp3nP	snést
i	i	k9	i
krize	krize	k1gFnPc1	krize
a	a	k8xC	a
rozpory	rozpor	k1gInPc1	rozpor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
nepřítel	nepřítel	k1gMnSc1	nepřítel
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
zvířat	zvíře	k1gNnPc2	zvíře
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
zvířecí	zvířecí	k2eAgFnSc6d1	zvířecí
říši	říš	k1gFnSc6	říš
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
přátelství	přátelství	k1gNnSc1	přátelství
často	často	k6eAd1	často
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jako	jako	k9	jako
přítel	přítel	k1gMnSc1	přítel
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
nepřátelství	nepřátelství	k1gNnSc2	nepřátelství
lze	lze	k6eAd1	lze
ve	v	k7c6	v
zvířecí	zvířecí	k2eAgFnSc6d1	zvířecí
říši	říš	k1gFnSc6	říš
naopak	naopak	k6eAd1	naopak
považovat	považovat	k5eAaImF	považovat
hada	had	k1gMnSc4	had
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výkladu	výklad	k1gInSc2	výklad
snů	sen	k1gInPc2	sen
had	had	k1gMnSc1	had
(	(	kIx(	(
<g/>
též	též	k9	též
zmije	zmije	k1gFnSc1	zmije
<g/>
)	)	kIx)	)
značí	značit	k5eAaImIp3nS	značit
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
jím	on	k3xPp3gMnSc7	on
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
napaden	napadnout	k5eAaPmNgMnS	napadnout
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc4	ten
varování	varování	k1gNnSc4	varování
dávat	dávat	k5eAaImF	dávat
si	se	k3xPyFc3	se
pozor	pozor	k1gInSc4	pozor
na	na	k7c4	na
nové	nový	k2eAgMnPc4d1	nový
známé	známý	k1gMnPc4	známý
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
ukázat	ukázat	k5eAaPmF	ukázat
jako	jako	k8xC	jako
nepřátelé	nepřítel	k1gMnPc1	nepřítel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
nepřátelství	nepřátelství	k1gNnPc2	nepřátelství
ve	v	k7c6	v
zvířecí	zvířecí	k2eAgFnSc6d1	zvířecí
říši	říš	k1gFnSc6	říš
zobrazován	zobrazovat	k5eAaImNgMnS	zobrazovat
také	také	k9	také
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
byl	být	k5eAaImAgMnS	být
zpupný	zpupný	k2eAgMnSc1d1	zpupný
král	král	k1gMnSc1	král
Lykáón	Lykáón	k1gMnSc1	Lykáón
proměněný	proměněný	k2eAgMnSc1d1	proměněný
ve	v	k7c4	v
vlka	vlk	k1gMnSc4	vlk
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
špatné	špatný	k2eAgFnSc3d1	špatná
povaze	povaha	k1gFnSc3	povaha
a	a	k8xC	a
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vzepřel	vzepřít	k5eAaPmAgInS	vzepřít
bohům	bůh	k1gMnPc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Chápání	chápání	k1gNnSc1	chápání
vlka	vlk	k1gMnSc2	vlk
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
nepřátelství	nepřátelství	k1gNnSc2	nepřátelství
lze	lze	k6eAd1	lze
spatřovat	spatřovat	k5eAaImF	spatřovat
i	i	k9	i
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
člověku	člověk	k1gMnSc3	člověk
vlkem	vlk	k1gMnSc7	vlk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Thomase	Thomas	k1gMnSc2	Thomas
Hobbese	Hobbese	k1gFnSc2	Hobbese
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
knihy	kniha	k1gFnSc2	kniha
Leviathan	Leviathan	k1gInSc1	Leviathan
odrazem	odraz	k1gInSc7	odraz
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
přirozeném	přirozený	k2eAgInSc6d1	přirozený
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nikdo	nikdo	k3yNnSc1	nikdo
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
tak	tak	k6eAd1	tak
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
tak	tak	k6eAd1	tak
chytrý	chytrý	k2eAgInSc1d1	chytrý
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nemusel	muset	k5eNaImAgMnS	muset
obávat	obávat	k5eAaImF	obávat
násilné	násilný	k2eAgFnSc2d1	násilná
smrti	smrt	k1gFnSc2	smrt
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
druhých	druhý	k4xOgMnPc2	druhý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přátelství	přátelství	k1gNnSc3	přátelství
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Úryvek	úryvek	k1gInSc1	úryvek
z	z	k7c2	z
LÚKIANOS	LÚKIANOS	kA	LÚKIANOS
<g/>
.	.	kIx.	.
</s>
<s>
Toxaris	Toxaris	k1gFnSc1	Toxaris
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
O	o	k7c6	o
přátelství	přátelství	k1gNnSc6	přátelství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přeložil	přeložit	k5eAaPmAgMnS	přeložit
Antonín	Antonín	k1gMnSc1	Antonín
SALAČ	Salač	k1gMnSc1	Salač
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
F.	F.	kA	F.
Topič	topič	k1gMnSc1	topič
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Osení	osení	k1gNnSc1	osení
<g/>
,	,	kIx,	,
knihy	kniha	k1gFnPc1	kniha
mladých	mladý	k2eAgMnPc2d1	mladý
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
,	,	kIx,	,
s.	s.	k?	s.
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
46	[number]	k4	46
<g/>
:	:	kIx,	:
<g/>
Toxaris	Toxaris	k1gFnSc1	Toxaris
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
Bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nP	by
na	na	k7c6	na
čase	čas	k1gInSc6	čas
rozhodnouti	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
z	z	k7c2	z
nás	my	k3xPp1nPc2	my
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
a	a	k8xC	a
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
jazyk	jazyk	k1gInSc1	jazyk
vyříznut	vyříznout	k5eAaPmNgInS	vyříznout
nebo	nebo	k8xC	nebo
ruka	ruka	k1gFnSc1	ruka
uťata	uťat	k2eAgFnSc1d1	uťat
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
bude	být	k5eAaImBp3nS	být
rozhodčím	rozhodčí	k1gMnSc7	rozhodčí
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Mnesippos	Mnesippos	k1gInSc1	Mnesippos
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikdo	nikdo	k3yNnSc1	nikdo
<g/>
;	;	kIx,	;
neboť	neboť	k8xC	neboť
neustanovili	ustanovit	k5eNaPmAgMnP	ustanovit
jsme	být	k5eAaImIp1nP	být
nikoho	nikdo	k3yNnSc2	nikdo
soudcem	soudce	k1gMnSc7	soudce
hovoru	hovor	k1gInSc2	hovor
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
víš	vědět	k5eAaImIp2nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
učiníme	učinit	k5eAaImIp1nP	učinit
<g/>
?	?	kIx.	?
</s>
<s>
Nyní	nyní	k6eAd1	nyní
stříleli	střílet	k5eAaImAgMnP	střílet
jsme	být	k5eAaImIp1nP	být
bez	bez	k7c2	bez
cíle	cíl	k1gInSc2	cíl
<g/>
;	;	kIx,	;
podruhé	podruhé	k6eAd1	podruhé
vyvolíme	vyvolit	k5eAaPmIp1nP	vyvolit
si	se	k3xPyFc3	se
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
a	a	k8xC	a
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
mluviti	mluvit	k5eAaImF	mluvit
budeme	být	k5eAaImBp1nP	být
o	o	k7c6	o
jiných	jiný	k2eAgMnPc6d1	jiný
přátelích	přítel	k1gMnPc6	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
potom	potom	k6eAd1	potom
podlehne	podlehnout	k5eAaPmIp3nS	podlehnout
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
přijde	přijít	k5eAaPmIp3nS	přijít
o	o	k7c4	o
jazyk	jazyk	k1gInSc4	jazyk
-	-	kIx~	-
budu	být	k5eAaImBp1nS	být
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc1	ten
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
-	-	kIx~	-
budeš	být	k5eAaImBp2nS	být
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc1	ten
ty	ty	k3xPp2nSc1	ty
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
to	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
přece	přece	k9	přece
jen	jen	k6eAd1	jen
kruté	krutý	k2eAgNnSc1d1	kruté
<g/>
;	;	kIx,	;
vždyť	vždyť	k9	vždyť
ty	ten	k3xDgMnPc4	ten
jsi	být	k5eAaImIp2nS	být
chválil	chválit	k5eAaImAgMnS	chválit
přátelství	přátelství	k1gNnSc4	přátelství
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
já	já	k3xPp1nSc1	já
soudím	soudit	k5eAaImIp1nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidstvo	lidstvo	k1gNnSc1	lidstvo
nemá	mít	k5eNaImIp3nS	mít
nad	nad	k7c4	nad
ně	on	k3xPp3gMnPc4	on
dražšího	drahý	k2eAgInSc2d2	dražší
a	a	k8xC	a
krásnějšího	krásný	k2eAgInSc2d2	krásnější
pokladu	poklad	k1gInSc2	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Nuže	nuže	k9	nuže
<g/>
,	,	kIx,	,
podejme	podat	k5eAaPmRp1nP	podat
si	se	k3xPyFc3	se
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
buďme	budit	k5eAaImRp1nP	budit
přáteli	přítel	k1gMnPc7	přítel
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
i	i	k9	i
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
a	a	k8xC	a
těšme	těšit	k5eAaImRp1nP	těšit
se	se	k3xPyFc4	se
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
oba	dva	k4xCgMnPc1	dva
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
a	a	k8xC	a
získali	získat	k5eAaPmAgMnP	získat
cen	cena	k1gFnPc2	cena
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
<g/>
,	,	kIx,	,
obdrževše	obdržet	k5eAaPmDgFnP	obdržet
každý	každý	k3xTgMnSc1	každý
místo	místo	k7c2	místo
jednoho	jeden	k4xCgInSc2	jeden
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
jediné	jediný	k2eAgFnSc2d1	jediná
pravice	pravice	k1gFnSc2	pravice
po	po	k7c6	po
dvou	dva	k4xCgMnPc6	dva
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ještě	ještě	k9	ještě
čtyři	čtyři	k4xCgNnPc4	čtyři
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc4	čtyři
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
všeho	všecek	k3xTgNnSc2	všecek
dvojnásob	dvojnásobit	k5eAaImRp2nS	dvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Neboť	neboť	k8xC	neboť
sejdou	sejít	k5eAaPmIp3nP	sejít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
dva	dva	k4xCgMnPc1	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgMnPc1	tři
přátelé	přítel	k1gMnPc1	přítel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
když	když	k8xS	když
malíři	malíř	k1gMnPc1	malíř
vyobrazí	vyobrazit	k5eAaPmIp3nP	vyobrazit
obra	obr	k1gMnSc4	obr
Geryona	Geryon	k1gMnSc4	Geryon
o	o	k7c6	o
šesti	šest	k4xCc6	šest
rukách	ruka	k1gFnPc6	ruka
a	a	k8xC	a
třech	tři	k4xCgFnPc6	tři
hlavách	hlava	k1gFnPc6	hlava
<g/>
;	;	kIx,	;
neboť	neboť	k8xC	neboť
to	ten	k3xDgNnSc4	ten
byli	být	k5eAaImAgMnP	být
asi	asi	k9	asi
tři	tři	k4xCgMnPc1	tři
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
dělali	dělat	k5eAaImAgMnP	dělat
vše	všechen	k3xTgNnSc4	všechen
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
sluší	slušet	k5eAaImIp3nS	slušet
na	na	k7c4	na
přátele	přítel	k1gMnPc4	přítel
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Toxaris	Toxaris	k1gFnSc1	Toxaris
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Máš	mít	k5eAaImIp2nS	mít
docela	docela	k6eAd1	docela
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Nuže	nuže	k9	nuže
<g/>
,	,	kIx,	,
učiňme	učinit	k5eAaImRp1nP	učinit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
přeješ	přát	k5eAaImIp2nS	přát
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Mnesippos	Mnesippos	k1gInSc1	Mnesippos
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ale	ale	k9	ale
k	k	k7c3	k
potvrzení	potvrzení	k1gNnSc3	potvrzení
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
,	,	kIx,	,
Toxaride	Toxarid	k1gInSc5	Toxarid
<g/>
,	,	kIx,	,
neshánějme	shánět	k5eNaImRp1nP	shánět
ani	ani	k8xC	ani
krve	krev	k1gFnSc2	krev
ani	ani	k8xC	ani
dýk	dýka	k1gFnPc2	dýka
<g/>
;	;	kIx,	;
neboť	neboť	k8xC	neboť
hovor	hovor	k1gInSc1	hovor
právě	právě	k6eAd1	právě
skončený	skončený	k2eAgInSc1d1	skončený
a	a	k8xC	a
shoda	shoda	k1gFnSc1	shoda
smýšlení	smýšlení	k1gNnSc1	smýšlení
jsou	být	k5eAaImIp3nP	být
daleko	daleko	k6eAd1	daleko
lepší	dobrý	k2eAgFnSc7d2	lepší
zárukou	záruka	k1gFnSc7	záruka
<g/>
,	,	kIx,	,
než	než	k8xS	než
onen	onen	k3xDgInSc1	onen
pohár	pohár	k1gInSc1	pohár
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
vy	vy	k3xPp2nPc1	vy
píváte	pívat	k5eAaImIp2nP	pívat
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
k	k	k7c3	k
přátelství	přátelství	k1gNnSc3	přátelství
netřeba	netřeba	k6eAd1	netřeba
násilí	násilí	k1gNnSc3	násilí
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Toxaris	Toxaris	k1gFnSc4	Toxaris
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc4	ten
úplně	úplně	k6eAd1	úplně
schvaluji	schvalovat	k5eAaImIp1nS	schvalovat
<g/>
.	.	kIx.	.
</s>
<s>
Nuže	nuže	k9	nuže
<g/>
,	,	kIx,	,
buďme	budit	k5eAaImRp1nP	budit
si	se	k3xPyFc3	se
hostinnými	hostinný	k2eAgMnPc7d1	hostinný
přáteli	přítel	k1gMnPc7	přítel
<g/>
;	;	kIx,	;
ty	ty	k3xPp2nSc1	ty
uhostíš	uhostit	k5eAaPmIp2nS	uhostit
mne	já	k3xPp1nSc4	já
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
zde	zde	k6eAd1	zde
v	v	k7c6	v
Helladě	Hellada	k1gFnSc6	Hellada
<g/>
,	,	kIx,	,
a	a	k8xC	a
já	já	k3xPp1nSc1	já
ti	ty	k3xPp2nSc3	ty
oplatím	oplatit	k5eAaPmIp1nS	oplatit
pohostinství	pohostinství	k1gNnSc2	pohostinství
<g/>
,	,	kIx,	,
přijdeš	přijít	k5eAaPmIp2nS	přijít
<g/>
-li	i	k?	-li
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
Skythie	Skythie	k1gFnSc2	Skythie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Mnesippos	Mnesippos	k1gInSc1	Mnesippos
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Na	na	k7c4	na
mou	můj	k3xOp1gFnSc4	můj
věru	věra	k1gFnSc4	věra
<g/>
,	,	kIx,	,
nerozpakoval	rozpakovat	k5eNaPmAgMnS	rozpakovat
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
dále	daleko	k6eAd2	daleko
jíti	jít	k5eAaImF	jít
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
<g/>
-li	i	k?	-li
bych	by	kYmCp1nS	by
získati	získat	k5eAaPmF	získat
takové	takový	k3xDgMnPc4	takový
přátele	přítel	k1gMnPc4	přítel
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgNnSc7	jaký
ty	ty	k3xPp2nSc1	ty
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
Toxaride	Toxarid	k1gInSc5	Toxarid
<g/>
,	,	kIx,	,
v	v	k7c6	v
řeči	řeč	k1gFnSc6	řeč
své	svůj	k3xOyFgFnPc4	svůj
projevil	projevit	k5eAaPmAgMnS	projevit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
konec	konec	k1gInSc1	konec
díla	dílo	k1gNnSc2	dílo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
LÚKIANOS	LÚKIANOS	kA	LÚKIANOS
<g/>
.	.	kIx.	.
</s>
<s>
Toxaris	Toxaris	k1gFnSc1	Toxaris
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
O	o	k7c6	o
přátelství	přátelství	k1gNnSc6	přátelství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přeložil	přeložit	k5eAaPmAgMnS	přeložit
Antonín	Antonín	k1gMnSc1	Antonín
SALAČ	Salač	k1gMnSc1	Salač
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
F.	F.	kA	F.
Topič	topič	k1gMnSc1	topič
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Osení	osení	k1gNnSc1	osení
<g/>
,	,	kIx,	,
knihy	kniha	k1gFnPc1	kniha
mladých	mladý	k2eAgMnPc2d1	mladý
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Láska	láska	k1gFnSc1	láska
</s>
</p>
<p>
<s>
Platonická	platonický	k2eAgFnSc1d1	platonická
láska	láska	k1gFnSc1	láska
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
přátelství	přátelství	k1gNnSc2	přátelství
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Přátelství	přátelství	k1gNnSc2	přátelství
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
přátelství	přátelství	k1gNnSc2	přátelství
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Přátelství	přátelství	k1gNnSc1	přátelství
</s>
</p>
