<s>
Buňka	buňka	k1gFnSc1	buňka
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
cellula	cellout	k5eAaPmAgFnS	cellout
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
stavební	stavební	k2eAgFnSc1d1	stavební
a	a	k8xC	a
funkční	funkční	k2eAgFnSc1d1	funkční
jednotka	jednotka	k1gFnSc1	jednotka
těl	tělo	k1gNnPc2	tělo
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
těch	ten	k3xDgFnPc2	ten
nebuněčných	buněčný	k2eNgFnPc2d1	nebuněčná
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
viry	vir	k1gInPc1	vir
<g/>
,	,	kIx,	,
viroidy	viroid	k1gInPc1	viroid
a	a	k8xC	a
virusoidy	virusoid	k1gInPc1	virusoid
<g/>
.	.	kIx.	.
</s>
