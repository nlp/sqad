<s>
Buňka	buňka	k1gFnSc1	buňka
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
cellula	cellout	k5eAaPmAgFnS	cellout
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
stavební	stavební	k2eAgFnSc1d1	stavební
a	a	k8xC	a
funkční	funkční	k2eAgFnSc1d1	funkční
jednotka	jednotka	k1gFnSc1	jednotka
těl	tělo	k1gNnPc2	tělo
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
těch	ten	k3xDgFnPc2	ten
nebuněčných	buněčný	k2eNgFnPc2d1	nebuněčná
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
viry	vir	k1gInPc1	vir
<g/>
,	,	kIx,	,
viroidy	viroid	k1gInPc1	viroid
a	a	k8xC	a
virusoidy	virusoid	k1gInPc1	virusoid
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
obklopené	obklopený	k2eAgFnPc1d1	obklopená
membránou	membrána	k1gFnSc7	membrána
a	a	k8xC	a
uvnitř	uvnitř	k6eAd1	uvnitř
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
koncentrovaný	koncentrovaný	k2eAgInSc4d1	koncentrovaný
vodný	vodný	k2eAgInSc4d1	vodný
roztok	roztok	k1gInSc4	roztok
různých	různý	k2eAgFnPc2d1	různá
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
cytoplazmu	cytoplazma	k1gFnSc4	cytoplazma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
genetický	genetický	k2eAgInSc4d1	genetický
materiál	materiál	k1gInSc4	materiál
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
se	se	k3xPyFc4	se
dělit	dělit	k5eAaImF	dělit
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
některé	některý	k3yIgInPc1	některý
organismy	organismus	k1gInPc1	organismus
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
jednobuněčné	jednobuněčný	k2eAgInPc1d1	jednobuněčný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bakterie	bakterie	k1gFnSc2	bakterie
či	či	k8xC	či
různí	různý	k2eAgMnPc1d1	různý
prvoci	prvok	k1gMnPc1	prvok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
organismy	organismus	k1gInPc1	organismus
jsou	být	k5eAaImIp3nP	být
mnohobuněčné	mnohobuněčný	k2eAgInPc1d1	mnohobuněčný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnPc1d2	vyšší
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
a	a	k8xC	a
funkce	funkce	k1gFnSc1	funkce
buněk	buňka	k1gFnPc2	buňka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
velice	velice	k6eAd1	velice
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
<g/>
,	,	kIx,	,
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
druh	druh	k1gInSc1	druh
od	od	k7c2	od
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mnohobuněčného	mnohobuněčný	k2eAgNnSc2d1	mnohobuněčné
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
dělení	dělení	k1gNnSc1	dělení
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
buňky	buňka	k1gFnPc4	buňka
prokaryotické	prokaryotický	k2eAgFnPc4d1	prokaryotická
(	(	kIx(	(
<g/>
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
archeí	arche	k1gFnPc2	arche
<g/>
)	)	kIx)	)
a	a	k8xC	a
eukaryotické	eukaryotický	k2eAgFnPc1d1	eukaryotická
(	(	kIx(	(
<g/>
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
bakterie	bakterie	k1gFnSc2	bakterie
E.	E.	kA	E.
coli	coli	k6eAd1	coli
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
mikrometry	mikrometr	k1gInPc1	mikrometr
<g/>
,	,	kIx,	,
typické	typický	k2eAgFnPc1d1	typická
buňky	buňka	k1gFnPc1	buňka
eukaryot	eukaryota	k1gFnPc2	eukaryota
jsou	být	k5eAaImIp3nP	být
přibližně	přibližně	k6eAd1	přibližně
desetkrát	desetkrát	k6eAd1	desetkrát
větší	veliký	k2eAgFnPc1d2	veliký
než	než	k8xS	než
prokaryotické	prokaryotický	k2eAgFnPc1d1	prokaryotická
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
však	však	k9	však
buňky	buňka	k1gFnPc1	buňka
vzájemně	vzájemně	k6eAd1	vzájemně
liší	lišit	k5eAaImIp3nP	lišit
i	i	k9	i
tvarem	tvar	k1gInSc7	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
cytologie	cytologie	k1gFnSc2	cytologie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Buněčné	buněčný	k2eAgFnSc2d1	buněčná
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
zavedli	zavést	k5eAaPmAgMnP	zavést
botanik	botanik	k1gMnSc1	botanik
Matthias	Matthias	k1gMnSc1	Matthias
Jakob	Jakob	k1gMnSc1	Jakob
Schleiden	Schleidno	k1gNnPc2	Schleidno
a	a	k8xC	a
fyziolog	fyziolog	k1gMnSc1	fyziolog
Theodor	Theodor	k1gMnSc1	Theodor
Schwann	Schwann	k1gMnSc1	Schwann
a	a	k8xC	a
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
základním	základní	k2eAgInSc7d1	základní
nosným	nosný	k2eAgInSc7d1	nosný
pilířem	pilíř	k1gInSc7	pilíř
cytologie	cytologie	k1gFnSc2	cytologie
(	(	kIx(	(
<g/>
buněčné	buněčný	k2eAgFnSc2d1	buněčná
biologie	biologie	k1gFnSc2	biologie
<g/>
)	)	kIx)	)
a	a	k8xC	a
vlastně	vlastně	k9	vlastně
moderní	moderní	k2eAgFnSc1d1	moderní
biologie	biologie	k1gFnSc1	biologie
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
organismus	organismus	k1gInSc1	organismus
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
přímo	přímo	k6eAd1	přímo
složen	složit	k5eAaPmNgInS	složit
nebo	nebo	k8xC	nebo
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
buňkách	buňka	k1gFnPc6	buňka
existenčně	existenčně	k6eAd1	existenčně
závislý	závislý	k2eAgMnSc1d1	závislý
(	(	kIx(	(
<g/>
viry	vir	k1gInPc1	vir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žádná	žádný	k3yNgFnSc1	žádný
buňka	buňka	k1gFnSc1	buňka
nemůže	moct	k5eNaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
zase	zase	k9	zase
z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
a	a	k8xC	a
mateřská	mateřský	k2eAgFnSc1d1	mateřská
buňka	buňka	k1gFnSc1	buňka
předává	předávat	k5eAaImIp3nS	předávat
dceřiné	dceřiný	k2eAgFnSc3d1	dceřiná
buňce	buňka	k1gFnSc3	buňka
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
děděnou	děděný	k2eAgFnSc4d1	děděná
informaci	informace	k1gFnSc4	informace
k	k	k7c3	k
reprodukci	reprodukce	k1gFnSc3	reprodukce
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgFnSc1	sám
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
funkci	funkce	k1gFnSc3	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
vznik	vznik	k1gInSc1	vznik
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc4	všechen
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
známé	známý	k2eAgFnPc1d1	známá
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
ze	z	k7c2	z
společného	společný	k2eAgMnSc2d1	společný
předka	předek	k1gMnSc2	předek
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
žila	žít	k5eAaImAgFnS	žít
asi	asi	k9	asi
před	před	k7c7	před
3,5	[number]	k4	3,5
<g/>
-	-	kIx~	-
<g/>
3,8	[number]	k4	3,8
miliardami	miliarda	k4xCgFnPc7	miliarda
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
buňka	buňka	k1gFnSc1	buňka
zřejmě	zřejmě	k6eAd1	zřejmě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
nukleové	nukleový	k2eAgFnPc1d1	nukleová
kyseliny	kyselina	k1gFnPc1	kyselina
(	(	kIx(	(
<g/>
buď	buď	k8xC	buď
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
teorie	teorie	k1gFnSc2	teorie
RNA	RNA	kA	RNA
světa	svět	k1gInSc2	svět
spíše	spíše	k9	spíše
ještě	ještě	k9	ještě
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
obklopeny	obklopit	k5eAaPmNgFnP	obklopit
fosfolipidovou	fosfolipidový	k2eAgFnSc7d1	fosfolipidová
membránou	membrána	k1gFnSc7	membrána
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc7	jaký
známe	znát	k5eAaImIp1nP	znát
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lipozomy	lipozom	k1gInPc1	lipozom
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
kapénky	kapénka	k1gFnSc2	kapénka
lipidů	lipid	k1gInPc2	lipid
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgInPc1d1	schopný
se	se	k3xPyFc4	se
spontánně	spontánně	k6eAd1	spontánně
uspořádat	uspořádat	k5eAaPmF	uspořádat
do	do	k7c2	do
kulovitých	kulovitý	k2eAgFnPc2d1	kulovitá
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
zřejmě	zřejmě	k6eAd1	zřejmě
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
buněčné	buněčný	k2eAgInPc4d1	buněčný
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
přírodní	přírodní	k2eAgFnSc2d1	přírodní
selekce	selekce	k1gFnSc2	selekce
následně	následně	k6eAd1	následně
zajistil	zajistit	k5eAaPmAgMnS	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
převládly	převládnout	k5eAaPmAgFnP	převládnout
buňky	buňka	k1gFnPc1	buňka
schopné	schopný	k2eAgFnPc1d1	schopná
bezchybně	bezchybně	k6eAd1	bezchybně
replikovat	replikovat	k5eAaImF	replikovat
svůj	svůj	k3xOyFgInSc4	svůj
genetický	genetický	k2eAgInSc4d1	genetický
materiál	materiál	k1gInSc4	materiál
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
zřejmě	zřejmě	k6eAd1	zřejmě
již	již	k9	již
DNA	dna	k1gFnSc1	dna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
stabilnější	stabilní	k2eAgFnSc1d2	stabilnější
než	než	k8xS	než
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přepisovat	přepisovat	k5eAaImF	přepisovat
DNA	dno	k1gNnPc4	dno
na	na	k7c4	na
RNA	RNA	kA	RNA
a	a	k8xC	a
následně	následně	k6eAd1	následně
podle	podle	k7c2	podle
RNA	RNA	kA	RNA
syntetizovat	syntetizovat	k5eAaImF	syntetizovat
proteiny	protein	k1gInPc4	protein
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
progenoti	progenot	k1gMnPc1	progenot
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
tři	tři	k4xCgInPc1	tři
hlavní	hlavní	k2eAgInPc1d1	hlavní
typy	typ	k1gInPc1	typ
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
také	také	k9	také
veškerý	veškerý	k3xTgInSc1	veškerý
buněčný	buněčný	k2eAgInSc1d1	buněčný
život	život	k1gInSc1	život
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
tzv.	tzv.	kA	tzv.
domény	doména	k1gFnSc2	doména
<g/>
:	:	kIx,	:
bakterie	bakterie	k1gFnSc2	bakterie
(	(	kIx(	(
<g/>
Bacteria	Bacterium	k1gNnSc2	Bacterium
<g/>
,	,	kIx,	,
Eubacteria	Eubacterium	k1gNnSc2	Eubacterium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archea	archea	k1gFnSc1	archea
(	(	kIx(	(
<g/>
Archaea	Archaea	k1gFnSc1	Archaea
<g/>
,	,	kIx,	,
Archaebacteria	Archaebacterium	k1gNnPc1	Archaebacterium
<g/>
)	)	kIx)	)
a	a	k8xC	a
eukaryota	eukaryota	k1gFnSc1	eukaryota
(	(	kIx(	(
<g/>
Eukarya	Eukarya	k1gFnSc1	Eukarya
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
domény	doména	k1gFnPc4	doména
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
povrchním	povrchní	k2eAgInSc6d1	povrchní
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
buňku	buňka	k1gFnSc4	buňka
podobné	podobný	k2eAgFnPc1d1	podobná
a	a	k8xC	a
označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
jako	jako	k9	jako
prokaryotické	prokaryotický	k2eAgNnSc1d1	prokaryotický
<g/>
.	.	kIx.	.
</s>
<s>
Eukaryotická	Eukaryotický	k2eAgFnSc1d1	Eukaryotická
buňka	buňka	k1gFnSc1	buňka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
strukturně	strukturně	k6eAd1	strukturně
složitější	složitý	k2eAgFnSc1d2	složitější
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
až	až	k9	až
následně	následně	k6eAd1	následně
z	z	k7c2	z
několika	několik	k4yIc2	několik
prokaryotických	prokaryotický	k2eAgInPc2d1	prokaryotický
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zřejmě	zřejmě	k6eAd1	zřejmě
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
1,8	[number]	k4	1,8
<g/>
–	–	k?	–
<g/>
1,3	[number]	k4	1,3
miliardami	miliarda	k4xCgFnPc7	miliarda
lety	léto	k1gNnPc7	léto
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
tzv.	tzv.	kA	tzv.
eukaryogeneze	eukaryogeneze	k1gFnPc4	eukaryogeneze
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Srovnání	srovnání	k1gNnSc1	srovnání
tří	tři	k4xCgFnPc2	tři
domén	doména	k1gFnPc2	doména
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
<g/>
,	,	kIx,	,
různě	různě	k6eAd1	různě
vnitřně	vnitřně	k6eAd1	vnitřně
uspořádané	uspořádaný	k2eAgFnPc4d1	uspořádaná
a	a	k8xC	a
různě	různě	k6eAd1	různě
fylogeneticky	fylogeneticky	k6eAd1	fylogeneticky
pokročilé	pokročilý	k2eAgInPc4d1	pokročilý
typy	typ	k1gInPc4	typ
buněk	buňka	k1gFnPc2	buňka
–	–	k?	–
prokaryotické	prokaryotický	k2eAgFnSc2d1	prokaryotická
a	a	k8xC	a
eukaryotické	eukaryotický	k2eAgFnSc2d1	eukaryotická
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
prokaryotická	prokaryotický	k2eAgFnSc1d1	prokaryotická
buňka	buňka	k1gFnSc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Prokaryota	Prokaryota	k1gFnSc1	Prokaryota
<g/>
,	,	kIx,	,
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
pro	pro	k7c4	pro
(	(	kIx(	(
<g/>
před	před	k7c7	před
<g/>
)	)	kIx)	)
a	a	k8xC	a
karyo	karyo	k1gNnSc1	karyo
(	(	kIx(	(
<g/>
jádro	jádro	k1gNnSc1	jádro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
evolučně	evolučně	k6eAd1	evolučně
velmi	velmi	k6eAd1	velmi
staré	starý	k2eAgInPc4d1	starý
organismy	organismus	k1gInPc4	organismus
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejstarší	starý	k2eAgInPc4d3	nejstarší
buněčné	buněčný	k2eAgInPc4d1	buněčný
organismy	organismus	k1gInPc4	organismus
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prokaryot	prokaryota	k1gFnPc2	prokaryota
jsou	být	k5eAaImIp3nP	být
řazeny	řazen	k2eAgFnPc1d1	řazena
domény	doména	k1gFnPc1	doména
bakterie	bakterie	k1gFnSc2	bakterie
a	a	k8xC	a
archea	archeum	k1gNnSc2	archeum
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
jednobuněčné	jednobuněčný	k2eAgFnPc1d1	jednobuněčná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
tvořit	tvořit	k5eAaImF	tvořit
kolonie	kolonie	k1gFnSc2	kolonie
s	s	k7c7	s
tendencí	tendence	k1gFnSc7	tendence
k	k	k7c3	k
mnohobuněčnosti	mnohobuněčnost	k1gFnSc3	mnohobuněčnost
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
například	například	k6eAd1	například
sinice	sinice	k1gFnPc1	sinice
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
heterocyty	heterocyt	k1gInPc7	heterocyt
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
specializované	specializovaný	k2eAgFnSc2d1	specializovaná
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Prokaryotická	Prokaryotický	k2eAgFnSc1d1	Prokaryotická
buňka	buňka	k1gFnSc1	buňka
je	být	k5eAaImIp3nS	být
však	však	k9	však
přesto	přesto	k8xC	přesto
podstatně	podstatně	k6eAd1	podstatně
jednodušší	jednoduchý	k2eAgMnSc1d2	jednodušší
a	a	k8xC	a
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
buňka	buňka	k1gFnSc1	buňka
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
haploidní	haploidní	k2eAgFnSc1d1	haploidní
<g/>
,	,	kIx,	,
vlákno	vlákno	k1gNnSc1	vlákno
DNA	DNA	kA	DNA
není	být	k5eNaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
membránou	membrána	k1gFnSc7	membrána
odděleno	oddělen	k2eAgNnSc4d1	odděleno
od	od	k7c2	od
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
váčků	váček	k1gInPc2	váček
nemá	mít	k5eNaImIp3nS	mít
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
systém	systém	k1gInSc1	systém
membrán	membrána	k1gFnPc2	membrána
členící	členící	k2eAgFnSc4d1	členící
buňku	buňka	k1gFnSc4	buňka
má	mít	k5eAaImIp3nS	mít
prokaryotický	prokaryotický	k2eAgInSc1d1	prokaryotický
typ	typ	k1gInSc1	typ
ribozomů	ribozom	k1gInPc2	ribozom
(	(	kIx(	(
<g/>
70	[number]	k4	70
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
má	můj	k3xOp1gFnSc1	můj
<g/>
–	–	k?	–
<g/>
li	li	k8xS	li
bičík	bičík	k1gInSc1	bičík
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
prokaryotického	prokaryotický	k2eAgInSc2d1	prokaryotický
(	(	kIx(	(
<g/>
bakteriálního	bakteriální	k2eAgInSc2d1	bakteriální
<g/>
)	)	kIx)	)
typu	typ	k1gInSc2	typ
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
eukaryotická	eukaryotický	k2eAgFnSc1d1	eukaryotická
buňka	buňka	k1gFnSc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Eukaryotickou	Eukaryotický	k2eAgFnSc4d1	Eukaryotická
buňku	buňka	k1gFnSc4	buňka
mají	mít	k5eAaImIp3nP	mít
veškeré	veškerý	k3xTgInPc1	veškerý
organismy	organismus	k1gInPc1	organismus
náležející	náležející	k2eAgInPc1d1	náležející
do	do	k7c2	do
domény	doména	k1gFnSc2	doména
či	či	k8xC	či
nadříše	nadříš	k1gMnSc2	nadříš
Eukaryota	Eukaryot	k1gMnSc2	Eukaryot
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
veškeří	veškerý	k3xTgMnPc1	veškerý
prvoci	prvok	k1gMnPc1	prvok
<g/>
,	,	kIx,	,
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
rostliny	rostlina	k1gFnPc1	rostlina
a	a	k8xC	a
houby	houba	k1gFnPc1	houba
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jejich	jejich	k3xOp3gFnPc1	jejich
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
navzájem	navzájem	k6eAd1	navzájem
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Eukaryotické	Eukaryotický	k2eAgFnPc1d1	Eukaryotická
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
oproti	oproti	k7c3	oproti
prokaryotickým	prokaryotický	k2eAgFnPc3d1	prokaryotická
buňkám	buňka	k1gFnPc3	buňka
evolučně	evolučně	k6eAd1	evolučně
vyspělejší	vyspělý	k2eAgInSc4d2	vyspělejší
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc4	jejich
složitější	složitý	k2eAgFnSc1d2	složitější
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
strukturace	strukturace	k1gFnSc1	strukturace
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
výživu	výživa	k1gFnSc4	výživa
výrazně	výrazně	k6eAd1	výrazně
větších	veliký	k2eAgFnPc2d2	veliký
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
předpokladem	předpoklad	k1gInSc7	předpoklad
pro	pro	k7c4	pro
výraznější	výrazný	k2eAgFnSc4d2	výraznější
mezibuněčnou	mezibuněčný	k2eAgFnSc4d1	mezibuněčná
spolupráci	spolupráce	k1gFnSc4	spolupráce
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
u	u	k7c2	u
mnohobuněčných	mnohobuněčný	k2eAgMnPc2d1	mnohobuněčný
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
těmito	tento	k3xDgFnPc7	tento
strukturami	struktura	k1gFnPc7	struktura
<g/>
:	:	kIx,	:
Pravé	pravý	k2eAgNnSc1d1	pravé
jádro	jádro	k1gNnSc1	jádro
(	(	kIx(	(
<g/>
karyon	karyon	k1gInSc1	karyon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
přítomné	přítomný	k2eAgNnSc1d1	přítomné
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ohraničeno	ohraničen	k2eAgNnSc1d1	ohraničeno
dvojitou	dvojitý	k2eAgFnSc7d1	dvojitá
membránou	membrána	k1gFnSc7	membrána
a	a	k8xC	a
uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
uchovávána	uchováván	k2eAgFnSc1d1	uchovávána
genetická	genetický	k2eAgFnSc1d1	genetická
informace	informace	k1gFnSc1	informace
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Eukaryotická	Eukaryotický	k2eAgFnSc1d1	Eukaryotická
buňka	buňka	k1gFnSc1	buňka
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
buňka	buňka	k1gFnSc1	buňka
prokaryotická	prokaryotický	k2eAgFnSc1d1	prokaryotická
Endoplazmatické	Endoplazmatický	k2eAgNnSc4d1	Endoplazmatické
retikulum	retikulum	k1gNnSc4	retikulum
<g/>
,	,	kIx,	,
Golgiho	Golgi	k1gMnSc2	Golgi
komplex	komplex	k1gInSc1	komplex
(	(	kIx(	(
<g/>
GA	GA	kA	GA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vakuoly	vakuola	k1gFnPc4	vakuola
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc4d1	ostatní
endozomální	endozomální	k2eAgFnPc4d1	endozomální
struktury	struktura	k1gFnPc4	struktura
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
obvykle	obvykle	k6eAd1	obvykle
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
systém	systém	k1gInSc1	systém
membrán	membrána	k1gFnPc2	membrána
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
buňka	buňka	k1gFnSc1	buňka
dále	daleko	k6eAd2	daleko
členěna	členit	k5eAaImNgFnS	členit
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jí	on	k3xPp3gFnSc3	on
lepší	dobrý	k2eAgFnSc3d2	lepší
organizaci	organizace	k1gFnSc3	organizace
složitějších	složitý	k2eAgInPc2d2	složitější
životních	životní	k2eAgInPc2d1	životní
pochodů	pochod	k1gInPc2	pochod
<g/>
.	.	kIx.	.
</s>
<s>
Semiautonomní	Semiautonomní	k2eAgFnPc1d1	Semiautonomní
organely	organela	k1gFnPc1	organela
jsou	být	k5eAaImIp3nP	být
organely	organela	k1gFnPc1	organela
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zřejmě	zřejmě	k6eAd1	zřejmě
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
symbiotickou	symbiotický	k2eAgFnSc7d1	symbiotická
fúzí	fúze	k1gFnSc7	fúze
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
buňkou	buňka	k1gFnSc7	buňka
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
odděleny	oddělit	k5eAaPmNgFnP	oddělit
od	od	k7c2	od
okolní	okolní	k2eAgFnSc2d1	okolní
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
dvěma	dva	k4xCgFnPc7	dva
membránami	membrána	k1gFnPc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Udílí	udílet	k5eAaImIp3nS	udílet
jí	on	k3xPp3gFnSc2	on
nové	nový	k2eAgFnSc2d1	nová
schopnosti	schopnost	k1gFnSc2	schopnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
nezbytné	zbytný	k2eNgInPc1d1	zbytný
pro	pro	k7c4	pro
život	život	k1gInSc4	život
vícebuněčných	vícebuněčný	k2eAgInPc2d1	vícebuněčný
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
eukaryotických	eukaryotický	k2eAgFnPc2d1	eukaryotická
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
dávají	dávat	k5eAaImIp3nP	dávat
jim	on	k3xPp3gMnPc3	on
schopnost	schopnost	k1gFnSc4	schopnost
získávat	získávat	k5eAaImF	získávat
energii	energie	k1gFnSc4	energie
dýcháním	dýchání	k1gNnSc7	dýchání
<g/>
,	,	kIx,	,
plastidy	plastid	k1gInPc1	plastid
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
eukaryot	eukaryota	k1gFnPc2	eukaryota
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
jejich	jejich	k3xOp3gInPc4	jejich
typy	typ	k1gInPc4	typ
(	(	kIx(	(
<g/>
jmenovitě	jmenovitě	k6eAd1	jmenovitě
chloroplasty	chloroplast	k1gInPc1	chloroplast
<g/>
)	)	kIx)	)
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
rostlinám	rostlina	k1gFnPc3	rostlina
fotosyntézu	fotosyntéza	k1gFnSc4	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Cytoskelet	Cytoskelet	k1gInSc1	Cytoskelet
tvořený	tvořený	k2eAgInSc1d1	tvořený
aktinovými	aktinův	k2eAgInPc7d1	aktinův
mikrofilamenty	mikrofilament	k1gInPc7	mikrofilament
(	(	kIx(	(
<g/>
mikrovlákny	mikrovlákno	k1gNnPc7	mikrovlákno
<g/>
)	)	kIx)	)
a	a	k8xC	a
mikrotubuly	mikrotubula	k1gFnSc2	mikrotubula
udržuje	udržovat	k5eAaImIp3nS	udržovat
její	její	k3xOp3gInSc4	její
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
"	"	kIx"	"
<g/>
kolejnice	kolejnice	k1gFnSc1	kolejnice
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
cílený	cílený	k2eAgInSc4d1	cílený
pohyb	pohyb	k1gInSc4	pohyb
čehokoliv	cokoliv	k3yInSc2	cokoliv
uvnitř	uvnitř	k7c2	uvnitř
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
bičíky	bičík	k1gInPc4	bičík
nebo	nebo	k8xC	nebo
brvy	brva	k1gFnPc4	brva
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
eukaryotického	eukaryotický	k2eAgInSc2d1	eukaryotický
typu	typ	k1gInSc2	typ
Má	mít	k5eAaImIp3nS	mít
eukaryotický	eukaryotický	k2eAgInSc1d1	eukaryotický
typ	typ	k1gInSc1	typ
ribozomů	ribozom	k1gInPc2	ribozom
(	(	kIx(	(
<g/>
80	[number]	k4	80
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
Rostliny	rostlina	k1gFnPc4	rostlina
i	i	k8xC	i
živočichové	živočich	k1gMnPc1	živočich
mají	mít	k5eAaImIp3nP	mít
eukaryotickou	eukaryotický	k2eAgFnSc4d1	eukaryotická
buňku	buňka	k1gFnSc4	buňka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c7	mezi
buňkou	buňka	k1gFnSc7	buňka
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
a	a	k8xC	a
živočišnou	živočišný	k2eAgFnSc7d1	živočišná
existují	existovat	k5eAaImIp3nP	existovat
značné	značný	k2eAgInPc4d1	značný
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Živočišným	živočišný	k2eAgFnPc3d1	živočišná
buňkám	buňka	k1gFnPc3	buňka
chybí	chybit	k5eAaPmIp3nS	chybit
celulózní	celulózní	k2eAgFnSc1d1	celulózní
buněčná	buněčný	k2eAgFnSc1d1	buněčná
stěna	stěna	k1gFnSc1	stěna
a	a	k8xC	a
během	během	k7c2	během
diferenciace	diferenciace	k1gFnSc2	diferenciace
se	se	k3xPyFc4	se
nezvětšují	zvětšovat	k5eNaImIp3nP	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Živočišné	živočišný	k2eAgFnPc1d1	živočišná
buňky	buňka	k1gFnPc1	buňka
bývají	bývat	k5eAaImIp3nP	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
do	do	k7c2	do
20	[number]	k4	20
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
Mívají	mívat	k5eAaImIp3nP	mívat
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc1	jeden
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
výjimky	výjimka	k1gFnPc1	výjimka
(	(	kIx(	(
<g/>
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
,	,	kIx,	,
v	v	k7c6	v
chrupavkách	chrupavka	k1gFnPc6	chrupavka
–	–	k?	–
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
makronukleus	makronukleus	k1gInSc4	makronukleus
a	a	k8xC	a
mikronukleus	mikronukleus	k1gInSc4	mikronukleus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
odbourávají	odbourávat	k5eAaImIp3nP	odbourávat
kostní	kostní	k2eAgFnSc4d1	kostní
tkáň	tkáň	k1gFnSc4	tkáň
(	(	kIx(	(
<g/>
takzvané	takzvaný	k2eAgInPc1d1	takzvaný
osteoklasty	osteoklast	k1gInPc1	osteoklast
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
až	až	k9	až
100	[number]	k4	100
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
živočišných	živočišný	k2eAgFnPc6d1	živočišná
tkáních	tkáň	k1gFnPc6	tkáň
známe	znát	k5eAaImIp1nP	znát
i	i	k9	i
mnohojaderné	mnohojaderný	k2eAgInPc4d1	mnohojaderný
útvary	útvar	k1gInPc4	útvar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
buď	buď	k8xC	buď
dělením	dělení	k1gNnSc7	dělení
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
nedělí	dělit	k5eNaImIp3nS	dělit
cytoplazma	cytoplazma	k1gFnSc1	cytoplazma
(	(	kIx(	(
<g/>
plazmodium	plazmodium	k1gNnSc1	plazmodium
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
splynutím	splynutí	k1gNnSc7	splynutí
více	hodně	k6eAd2	hodně
buněk	buňka	k1gFnPc2	buňka
v	v	k7c4	v
jediný	jediný	k2eAgInSc4d1	jediný
útvar	útvar	k1gInSc4	útvar
(	(	kIx(	(
<g/>
syncytium	syncytium	k1gNnSc1	syncytium
<g/>
,	,	kIx,	,
např.	např.	kA	např.
srdeční	srdeční	k2eAgFnSc1d1	srdeční
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
červené	červený	k2eAgFnSc2d1	červená
krvinky	krvinka	k1gFnSc2	krvinka
člověka	člověk	k1gMnSc2	člověk
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
bezjaderné	bezjaderný	k2eAgFnPc1d1	Bezjaderná
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
uloženo	uložit	k5eAaPmNgNnS	uložit
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Výjimky	výjimka	k1gFnPc1	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
hromadí	hromadit	k5eAaImIp3nP	hromadit
rezervní	rezervní	k2eAgFnPc1d1	rezervní
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgMnPc2	jenž
jsou	být	k5eAaImIp3nP	být
organely	organela	k1gFnPc4	organela
obvykle	obvykle	k6eAd1	obvykle
u	u	k7c2	u
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
osmóze	osmóza	k1gFnSc6	osmóza
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vyrovnávání	vyrovnávání	k1gNnSc3	vyrovnávání
koncentrací	koncentrace	k1gFnPc2	koncentrace
dvou	dva	k4xCgInPc2	dva
roztoků	roztok	k1gInPc2	roztok
o	o	k7c6	o
nestejné	stejný	k2eNgFnSc6d1	nestejná
koncentraci	koncentrace	k1gFnSc6	koncentrace
přes	přes	k7c4	přes
polopropustnou	polopropustný	k2eAgFnSc4d1	polopropustná
membránu	membrána	k1gFnSc4	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Prostupují	prostupovat	k5eAaImIp3nP	prostupovat
pouze	pouze	k6eAd1	pouze
molekuly	molekula	k1gFnSc2	molekula
vody	voda	k1gFnSc2	voda
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
místa	místo	k1gNnSc2	místo
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
koncentrací	koncentrace	k1gFnSc7	koncentrace
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
buňka	buňka	k1gFnSc1	buňka
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
izotonickém	izotonický	k2eAgNnSc6d1	izotonické
<g/>
,	,	kIx,	,
nedochází	docházet	k5eNaImIp3nS	docházet
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
koncentrace	koncentrace	k1gFnSc1	koncentrace
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xC	jako
koncentrace	koncentrace	k1gFnSc1	koncentrace
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
hypotonickém	hypotonický	k2eAgNnSc6d1	hypotonické
je	být	k5eAaImIp3nS	být
koncentrace	koncentrace	k1gFnSc1	koncentrace
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
koncentrace	koncentrace	k1gFnSc1	koncentrace
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
proniká	pronikat	k5eAaImIp3nS	pronikat
přes	přes	k7c4	přes
membránu	membrána	k1gFnSc4	membrána
do	do	k7c2	do
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Rostlinná	rostlinný	k2eAgFnSc1d1	rostlinná
buňka	buňka	k1gFnSc1	buňka
takovému	takový	k3xDgInSc3	takový
osmotickému	osmotický	k2eAgInSc3d1	osmotický
tlaku	tlak	k1gInSc3	tlak
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
odolává	odolávat	k5eAaImIp3nS	odolávat
díky	díky	k7c3	díky
přítomnosti	přítomnost	k1gFnSc3	přítomnost
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
<g/>
,	,	kIx,	,
živočišná	živočišný	k2eAgFnSc1d1	živočišná
buňka	buňka	k1gFnSc1	buňka
však	však	k9	však
záhy	záhy	k6eAd1	záhy
praskne	prasknout	k5eAaPmIp3nS	prasknout
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
plazmoptýza	plazmoptýza	k1gFnSc1	plazmoptýza
(	(	kIx(	(
<g/>
osmotická	osmotický	k2eAgFnSc1d1	osmotická
lýza	lýza	k1gFnSc1	lýza
buňky	buňka	k1gFnSc2	buňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
koncentrace	koncentrace	k1gFnSc1	koncentrace
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
koncentrace	koncentrace	k1gFnSc1	koncentrace
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
(	(	kIx(	(
<g/>
prostředí	prostředí	k1gNnSc4	prostředí
hypertonické	hypertonický	k2eAgFnSc2d1	hypertonická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
odnímání	odnímání	k1gNnSc3	odnímání
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Živočišná	živočišný	k2eAgFnSc1d1	živočišná
buňka	buňka	k1gFnSc1	buňka
se	se	k3xPyFc4	se
svrašťuje	svrašťovat	k5eAaImIp3nS	svrašťovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
plazmorýza	plazmorýza	k1gFnSc1	plazmorýza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rostlinné	rostlinný	k2eAgFnSc6d1	rostlinná
buňce	buňka	k1gFnSc6	buňka
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
protoplastu	protoplast	k1gInSc2	protoplast
od	od	k7c2	od
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
plazmolýza	plazmolýza	k1gFnSc1	plazmolýza
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
umístění	umístění	k1gNnSc6	umístění
buňky	buňka	k1gFnSc2	buňka
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
izonického	izonický	k2eAgInSc2d1	izonický
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
zpětnému	zpětný	k2eAgInSc3d1	zpětný
procesu	proces	k1gInSc3	proces
nazývaném	nazývaný	k2eAgInSc6d1	nazývaný
deplazmolýza	deplazmolýza	k1gFnSc1	deplazmolýza
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
buněčný	buněčný	k2eAgInSc4d1	buněčný
cyklus	cyklus	k1gInSc4	cyklus
a	a	k8xC	a
buněčné	buněčný	k2eAgNnSc4d1	buněčné
dělení	dělení	k1gNnSc4	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
nejsou	být	k5eNaImIp3nP	být
věčné	věčný	k2eAgInPc1d1	věčný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
zachování	zachování	k1gNnSc4	zachování
druhu	druh	k1gInSc2	druh
obnovovaly	obnovovat	k5eAaImAgFnP	obnovovat
<g/>
.	.	kIx.	.
</s>
<s>
Prochází	procházet	k5eAaImIp3nS	procházet
přitom	přitom	k6eAd1	přitom
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
složitým	složitý	k2eAgInSc7d1	složitý
buněčným	buněčný	k2eAgInSc7d1	buněčný
cyklem	cyklus	k1gInSc7	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
u	u	k7c2	u
prokaryot	prokaryota	k1gFnPc2	prokaryota
se	se	k3xPyFc4	se
střídá	střídat	k5eAaImIp3nS	střídat
fáze	fáze	k1gFnSc1	fáze
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
fáze	fáze	k1gFnSc2	fáze
dělení	dělení	k1gNnSc2	dělení
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
protože	protože	k8xS	protože
obvykle	obvykle	k6eAd1	obvykle
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
buňky	buňka	k1gFnSc2	buňka
mateřské	mateřský	k2eAgFnSc2d1	mateřská
stávají	stávat	k5eAaImIp3nP	stávat
dvě	dva	k4xCgFnPc1	dva
dceřiné	dceřiný	k2eAgFnPc1d1	dceřiná
<g/>
,	,	kIx,	,
při	při	k7c6	při
generační	generační	k2eAgFnSc6d1	generační
době	doba	k1gFnSc6	doba
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
může	moct	k5eAaImIp3nS	moct
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
buňky	buňka	k1gFnSc2	buňka
teoreticky	teoreticky	k6eAd1	teoreticky
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
vzniknout	vzniknout	k5eAaPmF	vzniknout
4722	[number]	k4	4722
triliónů	trilión	k4xCgInPc2	trilión
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Prokaryotické	Prokaryotický	k2eAgInPc1d1	Prokaryotický
organismy	organismus	k1gInPc1	organismus
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
tzv.	tzv.	kA	tzv.
binárně	binárně	k6eAd1	binárně
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
u	u	k7c2	u
eukaryotických	eukaryotický	k2eAgFnPc2d1	eukaryotická
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
mitotické	mitotický	k2eAgNnSc1d1	mitotické
a	a	k8xC	a
meiotické	meiotický	k2eAgNnSc1d1	meiotické
dělení	dělení	k1gNnSc1	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Mitóza	mitóza	k1gFnSc1	mitóza
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
dělení	dělení	k1gNnSc3	dělení
vegetativních	vegetativní	k2eAgFnPc2d1	vegetativní
buněk	buňka	k1gFnPc2	buňka
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
meióza	meióza	k1gFnSc1	meióza
(	(	kIx(	(
<g/>
redukční	redukční	k2eAgNnSc1d1	redukční
dělení	dělení	k1gNnSc1	dělení
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
buněk	buňka	k1gFnPc2	buňka
u	u	k7c2	u
pohlavně	pohlavně	k6eAd1	pohlavně
se	se	k3xPyFc4	se
rozmnožujících	rozmnožující	k2eAgInPc2d1	rozmnožující
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
probíhá	probíhat	k5eAaImIp3nS	probíhat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgFnPc3	jenž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přeměnám	přeměna	k1gFnPc3	přeměna
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
metabolismu	metabolismus	k1gInSc3	metabolismus
<g/>
.	.	kIx.	.
</s>
<s>
Skladné	skladný	k2eAgInPc1d1	skladný
procesy	proces	k1gInPc1	proces
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
anabolické	anabolický	k2eAgInPc1d1	anabolický
<g/>
,	,	kIx,	,
rozkladné	rozkladný	k2eAgInPc1d1	rozkladný
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
katabolické	katabolický	k2eAgFnPc4d1	katabolická
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
metabolické	metabolický	k2eAgFnPc1d1	metabolická
dráhy	dráha	k1gFnPc1	dráha
řízeny	řídit	k5eAaImNgFnP	řídit
enzymaticky	enzymaticky	k6eAd1	enzymaticky
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
pomocí	pomocí	k7c2	pomocí
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
katalyzují	katalyzovat	k5eAaImIp3nP	katalyzovat
tyto	tento	k3xDgFnPc4	tento
reakce	reakce	k1gFnPc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
skladným	skladný	k2eAgInSc7d1	skladný
procesem	proces	k1gInSc7	proces
je	být	k5eAaImIp3nS	být
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
<g/>
,	,	kIx,	,
probíhající	probíhající	k2eAgInSc4d1	probíhající
u	u	k7c2	u
fotoautotrofních	fotoautotrofní	k2eAgInPc2d1	fotoautotrofní
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
sinice	sinice	k1gFnPc1	sinice
<g/>
,	,	kIx,	,
rostliny	rostlina	k1gFnPc1	rostlina
a	a	k8xC	a
řasy	řasa	k1gFnPc1	řasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
světelné	světelný	k2eAgFnSc6d1	světelná
fázi	fáze	k1gFnSc6	fáze
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
dochází	docházet	k5eAaImIp3nS	docházet
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
sluneční	sluneční	k2eAgFnSc2d1	sluneční
energie	energie	k1gFnSc2	energie
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
NADPH	NADPH	kA	NADPH
a	a	k8xC	a
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
v	v	k7c6	v
temnostní	temnostní	k2eAgFnSc6d1	temnostní
fázi	fáze	k1gFnSc6	fáze
jsou	být	k5eAaImIp3nP	být
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
těchto	tento	k3xDgFnPc2	tento
látek	látka	k1gFnPc2	látka
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
vody	voda	k1gFnPc1	voda
sacharidy	sacharid	k1gInPc4	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
základním	základní	k2eAgInSc7d1	základní
rozkladným	rozkladný	k2eAgInSc7d1	rozkladný
procesem	proces	k1gInSc7	proces
je	být	k5eAaImIp3nS	být
buněčné	buněčný	k2eAgNnSc1d1	buněčné
dýchání	dýchání	k1gNnSc1	dýchání
(	(	kIx(	(
<g/>
respirace	respirace	k1gFnSc1	respirace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
energeticky	energeticky	k6eAd1	energeticky
bohaté	bohatý	k2eAgFnPc1d1	bohatá
organické	organický	k2eAgFnPc1d1	organická
látky	látka	k1gFnPc1	látka
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
ATP	atp	kA	atp
(	(	kIx(	(
<g/>
a	a	k8xC	a
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
se	se	k3xPyFc4	se
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spotřeba	spotřeba	k1gFnSc1	spotřeba
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
úměrná	úměrná	k1gFnSc1	úměrná
objemu	objem	k1gInSc2	objem
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
centrální	centrální	k2eAgNnSc1d1	centrální
dogma	dogma	k1gNnSc1	dogma
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
třem	tři	k4xCgInPc3	tři
základním	základní	k2eAgInPc3d1	základní
krokům	krok	k1gInPc3	krok
<g/>
:	:	kIx,	:
Replikace	replikace	k1gFnSc2	replikace
-	-	kIx~	-
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
informace	informace	k1gFnSc2	informace
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
generace	generace	k1gFnSc2	generace
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
před	před	k7c7	před
každým	každý	k3xTgNnSc7	každý
buněčným	buněčný	k2eAgNnSc7d1	buněčné
dělením	dělení	k1gNnSc7	dělení
zdvojnásobit	zdvojnásobit	k5eAaPmF	zdvojnásobit
množství	množství	k1gNnPc4	množství
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
replikaci	replikace	k1gFnSc6	replikace
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
vlákno	vlákno	k1gNnSc1	vlákno
komplementární	komplementární	k2eAgNnSc1d1	komplementární
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
identické	identický	k2eAgFnPc4d1	identická
dvoušroubovice	dvoušroubovice	k1gFnPc4	dvoušroubovice
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Transkripce	transkripce	k1gFnSc1	transkripce
-	-	kIx~	-
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
sestavení	sestavení	k1gNnSc4	sestavení
molekuly	molekula	k1gFnSc2	molekula
RNA	RNA	kA	RNA
podle	podle	k7c2	podle
záznamu	záznam	k1gInSc2	záznam
v	v	k7c6	v
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
vzor	vzor	k1gInSc1	vzor
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
kroku	krok	k1gInSc6	krok
<g/>
.	.	kIx.	.
</s>
<s>
Translace	translace	k1gFnSc1	translace
(	(	kIx(	(
<g/>
biologie	biologie	k1gFnSc1	biologie
<g/>
)	)	kIx)	)
-	-	kIx~	-
na	na	k7c6	na
ribozomech	ribozom	k1gInPc6	ribozom
se	se	k3xPyFc4	se
překládá	překládat	k5eAaImIp3nS	překládat
pořadí	pořadí	k1gNnSc1	pořadí
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
na	na	k7c6	na
mRNA	mRNA	k?	mRNA
do	do	k7c2	do
primární	primární	k2eAgFnSc2d1	primární
struktury	struktura	k1gFnSc2	struktura
proteinů	protein	k1gInPc2	protein
připojováním	připojování	k1gNnSc7	připojování
aminokyselinových	aminokyselinový	k2eAgInPc2d1	aminokyselinový
zbytků	zbytek	k1gInPc2	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
probíhá	probíhat	k5eAaImIp3nS	probíhat
podle	podle	k7c2	podle
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
