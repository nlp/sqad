<s>
Kyjev	Kyjev	k1gInSc1	Kyjev
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
К	К	k?	К
<g/>
,	,	kIx,	,
Kyjiv	Kyjiva	k1gFnPc2	Kyjiva
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
rusky	rusky	k6eAd1	rusky
К	К	k?	К
<g/>
,	,	kIx,	,
Kijev	Kijev	k1gFnSc1	Kijev
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
správní	správní	k2eAgNnSc1d1	správní
středisko	středisko	k1gNnSc1	středisko
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
