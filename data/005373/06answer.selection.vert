<s>
Přestože	přestože	k8xS	přestože
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
knihy	kniha	k1gFnPc1	kniha
bible	bible	k1gFnSc2	bible
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
historických	historický	k2eAgNnPc2d1	historické
období	období	k1gNnPc2	období
<g/>
,	,	kIx,	,
křesťané	křesťan	k1gMnPc1	křesťan
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bible	bible	k1gFnSc1	bible
předává	předávat	k5eAaImIp3nS	předávat
jisté	jistý	k2eAgNnSc4d1	jisté
ucelené	ucelený	k2eAgNnSc4d1	ucelené
poselství	poselství	k1gNnSc4	poselství
<g/>
.	.	kIx.	.
</s>
