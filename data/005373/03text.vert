<s>
Bible	bible	k1gFnSc1	bible
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
τ	τ	k?	τ
β	β	k?	β
ta	ten	k3xDgFnSc1	ten
biblia	biblia	k1gFnSc1	biblia
-	-	kIx~	-
knihy	kniha	k1gFnPc1	kniha
<g/>
,	,	kIx,	,
svitky	svitek	k1gInPc1	svitek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc1	soubor
starověkých	starověký	k2eAgInPc2d1	starověký
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
křesťanství	křesťanství	k1gNnSc4	křesťanství
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
i	i	k9	i
judaismus	judaismus	k1gInSc4	judaismus
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
posvátné	posvátný	k2eAgFnPc4d1	posvátná
a	a	k8xC	a
inspirované	inspirovaný	k2eAgFnPc4d1	inspirovaná
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
také	také	k9	také
Písmo	písmo	k1gNnSc1	písmo
svaté	svatá	k1gFnSc2	svatá
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Scriptura	Scriptura	k1gFnSc1	Scriptura
sacra	sacra	k1gFnSc1	sacra
nebo	nebo	k8xC	nebo
Scriptura	Scriptura	k1gFnSc1	Scriptura
sancta	sancta	k1gFnSc1	sancta
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
krátce	krátce	k6eAd1	krátce
jen	jen	k9	jen
Písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
také	také	k9	také
Kniha	kniha	k1gFnSc1	kniha
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Bible	bible	k1gFnSc1	bible
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
Starý	starý	k2eAgInSc4d1	starý
a	a	k8xC	a
Nový	nový	k2eAgInSc4d1	nový
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Bible	bible	k1gFnSc1	bible
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
překládanou	překládaný	k2eAgFnSc7d1	překládaná
a	a	k8xC	a
vydávanou	vydávaný	k2eAgFnSc7d1	vydávaná
knihou	kniha	k1gFnSc7	kniha
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
Gutenbergovo	Gutenbergův	k2eAgNnSc1d1	Gutenbergovo
vydání	vydání	k1gNnSc1	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1452	[number]	k4	1452
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
tištěnou	tištěný	k2eAgFnSc7d1	tištěná
knihou	kniha	k1gFnSc7	kniha
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
celé	celý	k2eAgFnSc2d1	celá
bible	bible	k1gFnSc2	bible
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1360	[number]	k4	1360
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
bible	bible	k1gFnSc1	bible
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
biblia	biblium	k1gNnSc2	biblium
<g/>
,	,	kIx,	,
knihy	kniha	k1gFnSc2	kniha
–	–	k?	–
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
od	od	k7c2	od
neutra	neutrum	k1gNnSc2	neutrum
τ	τ	k?	τ
β	β	k?	β
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
biblion	biblion	k1gInSc1	biblion
<g/>
,	,	kIx,	,
zdrobnělina	zdrobnělina	k1gFnSc1	zdrobnělina
slova	slovo	k1gNnSc2	slovo
biblos	biblosa	k1gFnPc2	biblosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Antické	antický	k2eAgFnPc1d1	antická
knihy	kniha	k1gFnPc1	kniha
měly	mít	k5eAaImAgFnP	mít
podobu	podoba	k1gFnSc4	podoba
svitku	svitek	k1gInSc2	svitek
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
řecké	řecký	k2eAgNnSc1d1	řecké
označení	označení	k1gNnSc1	označení
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
byblos	byblosa	k1gFnPc2	byblosa
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
papyrus	papyrus	k1gInSc1	papyrus
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
svitky	svitek	k1gInPc1	svitek
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Množné	množný	k2eAgNnSc4d1	množné
číslo	číslo	k1gNnSc4	číslo
biblia	biblius	k1gMnSc2	biblius
převzala	převzít	k5eAaPmAgFnS	převzít
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
výraz	výraz	k1gInSc4	výraz
později	pozdě	k6eAd2	pozdě
pochopili	pochopit	k5eAaPmAgMnP	pochopit
jako	jako	k9	jako
jednotné	jednotný	k2eAgNnSc4d1	jednotné
číslo	číslo	k1gNnSc4	číslo
feminina	femininum	k1gNnSc2	femininum
a	a	k8xC	a
slovo	slovo	k1gNnSc1	slovo
bible	bible	k1gFnSc2	bible
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
začalo	začít	k5eAaPmAgNnS	začít
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
užívat	užívat	k5eAaImF	užívat
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
čísle	číslo	k1gNnSc6	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejské	hebrejský	k2eAgNnSc1d1	hebrejské
slovo	slovo	k1gNnSc1	slovo
tóra	tóra	k1gFnSc1	tóra
(	(	kIx(	(
<g/>
naučení	naučení	k1gNnSc1	naučení
<g/>
,	,	kIx,	,
příkaz	příkaz	k1gInSc1	příkaz
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Septuaginta	Septuaginta	k1gFnSc1	Septuaginta
do	do	k7c2	do
řečtiny	řečtina	k1gFnSc2	řečtina
jako	jako	k8xS	jako
diathéké	diathéká	k1gFnSc2	diathéká
(	(	kIx(	(
<g/>
závěť	závěť	k1gFnSc1	závěť
<g/>
,	,	kIx,	,
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
)	)	kIx)	)
a	a	k8xC	a
latinské	latinský	k2eAgInPc1d1	latinský
překlady	překlad	k1gInPc1	překlad
jako	jako	k8xS	jako
testamentum	testamentum	k1gNnSc1	testamentum
(	(	kIx(	(
<g/>
závěť	závěť	k1gFnSc1	závěť
<g/>
,	,	kIx,	,
odkaz	odkaz	k1gInSc1	odkaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Staroslověnský	staroslověnský	k2eAgInSc1d1	staroslověnský
překlad	překlad	k1gInSc1	překlad
použil	použít	k5eAaPmAgInS	použít
slovo	slovo	k1gNnSc4	slovo
zavět	zavět	k5eAaImF	zavět
a	a	k8xC	a
český	český	k2eAgInSc4d1	český
slovo	slovo	k1gNnSc1	slovo
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
původně	původně	k6eAd1	původně
snad	snad	k9	snad
znamenalo	znamenat	k5eAaImAgNnS	znamenat
ustanovení	ustanovení	k1gNnSc1	ustanovení
a	a	k8xC	a
odkaz	odkaz	k1gInSc1	odkaz
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
bible	bible	k1gFnSc1	bible
tvoří	tvořit	k5eAaImIp3nS	tvořit
dva	dva	k4xCgInPc4	dva
oddělené	oddělený	k2eAgInPc4d1	oddělený
soubory	soubor	k1gInPc4	soubor
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgMnPc1d2	starší
a	a	k8xC	a
podstatně	podstatně	k6eAd1	podstatně
obsáhlejší	obsáhlý	k2eAgInSc1d2	obsáhlejší
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
posvátnými	posvátný	k2eAgInPc7d1	posvátný
židovskými	židovský	k2eAgInPc7d1	židovský
texty	text	k1gInPc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgMnPc1d2	mladší
<g/>
,	,	kIx,	,
čistě	čistě	k6eAd1	čistě
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
věnován	věnovat	k5eAaImNgInS	věnovat
Ježíši	Ježíš	k1gMnSc3	Ježíš
Kristu	Krista	k1gFnSc4	Krista
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc3	jeho
učení	učení	k1gNnSc3	učení
a	a	k8xC	a
počátkům	počátek	k1gInPc3	počátek
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Starý	starý	k2eAgInSc4d1	starý
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
nebo	nebo	k8xC	nebo
Stará	starý	k2eAgFnSc1d1	stará
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
posvátné	posvátný	k2eAgFnPc4d1	posvátná
knihy	kniha	k1gFnPc4	kniha
(	(	kIx(	(
<g/>
tóra	tóra	k1gFnSc1	tóra
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
tanach	tanach	k1gInSc1	tanach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
křesťané	křesťan	k1gMnPc1	křesťan
převzali	převzít	k5eAaPmAgMnP	převzít
z	z	k7c2	z
judaismu	judaismus	k1gInSc2	judaismus
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
oběma	dva	k4xCgMnPc7	dva
náboženstvím	náboženství	k1gNnSc7	náboženství
společné	společný	k2eAgFnSc6d1	společná
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xS	jako
hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
bible	bible	k1gFnSc1	bible
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
vznikaly	vznikat	k5eAaImAgInP	vznikat
postupně	postupně	k6eAd1	postupně
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
psány	psát	k5eAaImNgInP	psát
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgFnPc4	některý
pasáže	pasáž	k1gFnPc4	pasáž
aramejsky	aramejsky	k6eAd1	aramejsky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hebrejské	hebrejský	k2eAgFnSc6d1	hebrejská
tradici	tradice	k1gFnSc6	tradice
se	se	k3xPyFc4	se
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
Tóru	tóra	k1gFnSc4	tóra
(	(	kIx(	(
<g/>
pět	pět	k4xCc1	pět
knih	kniha	k1gFnPc2	kniha
Mojžíšových	Mojžíšová	k1gFnPc2	Mojžíšová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Proroky	prorok	k1gMnPc4	prorok
a	a	k8xC	a
Spisy	spis	k1gInPc4	spis
<g/>
;	;	kIx,	;
v	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
tradici	tradice	k1gFnSc6	tradice
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c4	na
pět	pět	k4xCc4	pět
knih	kniha	k1gFnPc2	kniha
Mojžíšových	Mojžíšová	k1gFnPc2	Mojžíšová
<g/>
,	,	kIx,	,
prorocké	prorocký	k2eAgFnSc2d1	prorocká
<g/>
,	,	kIx,	,
dějepisné	dějepisný	k2eAgFnSc2d1	dějepisná
a	a	k8xC	a
mudroslovné	mudroslovný	k2eAgFnSc2d1	mudroslovná
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc1	pět
knih	kniha	k1gFnPc2	kniha
Mojžíšových	Mojžíšová	k1gFnPc2	Mojžíšová
neboli	neboli	k8xC	neboli
Pentateuch	pentateuch	k1gInSc1	pentateuch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
světa	svět	k1gInSc2	svět
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
stvoření	stvoření	k1gNnSc2	stvoření
<g/>
,	,	kIx,	,
vyvedení	vyvedení	k1gNnSc1	vyvedení
izraelského	izraelský	k2eAgInSc2d1	izraelský
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
egyptského	egyptský	k2eAgNnSc2d1	egyptské
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
pouti	pouť	k1gFnSc2	pouť
do	do	k7c2	do
Země	zem	k1gFnSc2	zem
zaslíbené	zaslíbená	k1gFnSc2	zaslíbená
a	a	k8xC	a
přijetí	přijetí	k1gNnSc2	přijetí
desatera	desatero	k1gNnSc2	desatero
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Prorocké	prorocký	k2eAgInPc1d1	prorocký
spisy	spis	k1gInPc1	spis
nesou	nést	k5eAaImIp3nP	nést
jméno	jméno	k1gNnSc4	jméno
daného	daný	k2eAgMnSc2d1	daný
proroka	prorok	k1gMnSc2	prorok
<g/>
,	,	kIx,	,
prostředníka	prostředník	k1gMnSc2	prostředník
přinášející	přinášející	k2eAgInSc1d1	přinášející
Boží	boží	k2eAgInSc1d1	boží
poselství	poselství	k1gNnSc4	poselství
<g/>
,	,	kIx,	,
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nP	dělit
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
čtyři	čtyři	k4xCgMnPc4	čtyři
obsáhlejší	obsáhlý	k2eAgMnPc4d2	obsáhlejší
větší	veliký	k2eAgMnPc4d2	veliký
proroky	prorok	k1gMnPc4	prorok
a	a	k8xC	a
dvanáct	dvanáct	k4xCc1	dvanáct
menších	malý	k2eAgMnPc2d2	menší
proroků	prorok	k1gMnPc2	prorok
<g/>
.	.	kIx.	.
</s>
<s>
Dějepisné	dějepisný	k2eAgFnPc1d1	dějepisná
knihy	kniha	k1gFnPc1	kniha
pojednávají	pojednávat	k5eAaImIp3nP	pojednávat
o	o	k7c6	o
historických	historický	k2eAgFnPc6d1	historická
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
období	období	k1gNnSc2	období
od	od	k7c2	od
příchodu	příchod	k1gInSc2	příchod
do	do	k7c2	do
Země	zem	k1gFnSc2	zem
zaslíbené	zaslíbená	k1gFnSc2	zaslíbená
po	po	k7c4	po
babylonské	babylonský	k2eAgNnSc4d1	babylonské
zajetí	zajetí	k1gNnSc4	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Mudroslovné	mudroslovný	k2eAgFnPc1d1	mudroslovná
knihy	kniha	k1gFnPc1	kniha
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
sdělení	sdělení	k1gNnSc3	sdělení
obecnějšího	obecní	k2eAgInSc2d2	obecní
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
poučné	poučný	k2eAgInPc1d1	poučný
a	a	k8xC	a
písňové	písňový	k2eAgInPc1d1	písňový
texty	text	k1gInPc1	text
<g/>
.	.	kIx.	.
</s>
<s>
Žánrové	žánrový	k2eAgNnSc1d1	žánrové
zařazení	zařazení	k1gNnSc1	zařazení
však	však	k9	však
zpravidla	zpravidla	k6eAd1	zpravidla
není	být	k5eNaImIp3nS	být
ostře	ostro	k6eAd1	ostro
vymezené	vymezený	k2eAgFnPc1d1	vymezená
a	a	k8xC	a
uvedené	uvedený	k2eAgFnPc1d1	uvedená
charakteristiky	charakteristika	k1gFnPc1	charakteristika
se	se	k3xPyFc4	se
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
knih	kniha	k1gFnPc2	kniha
částečně	částečně	k6eAd1	částečně
prolínají	prolínat	k5eAaImIp3nP	prolínat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
část	část	k1gFnSc1	část
bible	bible	k1gFnSc2	bible
-	-	kIx~	-
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
nebo	nebo	k8xC	nebo
Nová	nový	k2eAgFnSc1d1	nová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikal	vznikat	k5eAaImAgInS	vznikat
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
století	století	k1gNnPc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
,	,	kIx,	,
líčí	líčit	k5eAaImIp3nS	líčit
život	život	k1gInSc4	život
a	a	k8xC	a
učení	učení	k1gNnSc4	učení
Ježíše	Ježíš	k1gMnSc2	Ježíš
z	z	k7c2	z
Nazaretu	Nazaret	k1gInSc2	Nazaret
a	a	k8xC	a
první	první	k4xOgFnSc4	první
dobu	doba	k1gFnSc4	doba
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
částí	část	k1gFnSc7	část
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
jsou	být	k5eAaImIp3nP	být
evangelia	evangelium	k1gNnPc1	evangelium
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
popisují	popisovat	k5eAaImIp3nP	popisovat
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
působení	působení	k1gNnSc4	působení
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
zmrtvýchvstání	zmrtvýchvstání	k1gNnSc4	zmrtvýchvstání
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
evangelia	evangelium	k1gNnPc4	evangelium
bezprostředně	bezprostředně	k6eAd1	bezprostředně
navazují	navazovat	k5eAaImIp3nP	navazovat
Skutky	skutek	k1gInPc4	skutek
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
částí	část	k1gFnSc7	část
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
jsou	být	k5eAaImIp3nP	být
epištoly	epištola	k1gFnPc1	epištola
(	(	kIx(	(
<g/>
listy	list	k1gInPc1	list
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dopisy	dopis	k1gInPc1	dopis
apoštolů	apoštol	k1gMnPc2	apoštol
adresované	adresovaný	k2eAgInPc1d1	adresovaný
různým	různý	k2eAgFnPc3d1	různá
skupinám	skupina	k1gFnPc3	skupina
souvěrců	souvěrec	k1gMnPc2	souvěrec
či	či	k8xC	či
jednotlivcům	jednotlivec	k1gMnPc3	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
Pavlovy	Pavlův	k2eAgFnPc4d1	Pavlova
epištoly	epištola	k1gFnPc4	epištola
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
připisované	připisovaný	k2eAgInPc1d1	připisovaný
Pavlovi	Pavel	k1gMnSc6	Pavel
z	z	k7c2	z
Tarsu	Tars	k1gInSc2	Tars
<g/>
,	,	kIx,	,
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
katolické	katolický	k2eAgFnSc2d1	katolická
listy	lista	k1gFnSc2	lista
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
zákoně	zákon	k1gInSc6	zákon
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
Zjevení	zjevení	k1gNnSc1	zjevení
Janovo	Janův	k2eAgNnSc1d1	Janovo
(	(	kIx(	(
<g/>
Apokalypsa	apokalypsa	k1gFnSc1	apokalypsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prorocký	prorocký	k2eAgInSc1d1	prorocký
spis	spis	k1gInSc1	spis
s	s	k7c7	s
alegorickými	alegorický	k2eAgInPc7d1	alegorický
a	a	k8xC	a
symbolickými	symbolický	k2eAgInPc7d1	symbolický
obrazy	obraz	k1gInPc7	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
na	na	k7c4	na
Starý	Starý	k1gMnSc1	Starý
výslovně	výslovně	k6eAd1	výslovně
navazuje	navazovat	k5eAaImIp3nS	navazovat
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Nepřišel	přijít	k5eNaPmAgMnS	přijít
jsem	být	k5eAaImIp1nS	být
Zákon	zákon	k1gInSc4	zákon
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naplnit	naplnit	k5eAaPmF	naplnit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
Ježíše	Ježíš	k1gMnSc4	Ježíš
jako	jako	k8xC	jako
Krista	Kristus	k1gMnSc4	Kristus
(	(	kIx(	(
<g/>
Mesiáše	Mesiáš	k1gMnSc4	Mesiáš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
Hospodin	Hospodin	k1gMnSc1	Hospodin
naplnil	naplnit	k5eAaPmAgInS	naplnit
svůj	svůj	k3xOyFgInSc4	svůj
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
vyložil	vyložit	k5eAaPmAgMnS	vyložit
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
bible	bible	k1gFnSc1	bible
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kromě	kromě	k7c2	kromě
Tóry	tóra	k1gFnSc2	tóra
(	(	kIx(	(
<g/>
ת	ת	k?	ת
<g/>
ֹ	ֹ	k?	ֹ
<g/>
ר	ר	k?	ר
<g/>
ָ	ָ	k?	ָ
<g/>
ה	ה	k?	ה
<g/>
)	)	kIx)	)
ještě	ještě	k9	ještě
Proroky	prorok	k1gMnPc4	prorok
(	(	kIx(	(
<g/>
נ	נ	k?	נ
<g/>
ְ	ְ	k?	ְ
<g/>
ב	ב	k?	ב
<g/>
ִ	ִ	k?	ִ
<g/>
י	י	k?	י
<g/>
ִ	ִ	k?	ִ
<g/>
י	י	k?	י
nevi	nevi	k?	nevi
<g/>
'	'	kIx"	'
<g/>
im	im	k?	im
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spisy	spis	k1gInPc1	spis
(	(	kIx(	(
<g/>
כ	כ	k?	כ
<g/>
ְ	ְ	k?	ְ
<g/>
ּ	ּ	k?	ּ
<g/>
ת	ת	k?	ת
<g/>
ּ	ּ	k?	ּ
<g/>
ב	ב	k?	ב
<g/>
ִ	ִ	k?	ִ
<g/>
י	י	k?	י
ktuvim	ktuvim	k1gInSc1	ktuvim
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dohromady	dohromady	k6eAd1	dohromady
se	se	k3xPyFc4	se
tomuto	tento	k3xDgInSc3	tento
korpusu	korpus	k1gInSc3	korpus
říká	říkat	k5eAaImIp3nS	říkat
též	též	k9	též
tanach	tanach	k1gInSc1	tanach
(	(	kIx(	(
<g/>
ת	ת	k?	ת
<g/>
״	״	k?	״
<g/>
ך	ך	k?	ך
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
text	text	k1gInSc1	text
existoval	existovat	k5eAaImAgInS	existovat
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
<g/>
,	,	kIx,	,
s	s	k7c7	s
částmi	část	k1gFnPc7	část
v	v	k7c6	v
aramejštině	aramejština	k1gFnSc6	aramejština
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
Daniel	Daniel	k1gMnSc1	Daniel
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
Ezdráš	Ezdráš	k1gFnSc1	Ezdráš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Židovští	židovský	k2eAgMnPc1d1	židovský
učenci	učenec	k1gMnPc1	učenec
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
vytvořit	vytvořit	k5eAaPmF	vytvořit
jednotný	jednotný	k2eAgInSc4d1	jednotný
text	text	k1gInSc4	text
Tanachu	Tanach	k1gInSc2	Tanach
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xC	jako
masoretský	masoretský	k2eAgInSc1d1	masoretský
text	text	k1gInSc1	text
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
textu	text	k1gInSc2	text
punktaci	punktace	k1gFnSc4	punktace
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
znaménka	znaménko	k1gNnSc2	znaménko
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
hebrejština	hebrejština	k1gFnSc1	hebrejština
běžně	běžně	k6eAd1	běžně
nezapisuje	zapisovat	k5eNaImIp3nS	zapisovat
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejský	hebrejský	k2eAgInSc1d1	hebrejský
text	text	k1gInSc1	text
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
však	však	k9	však
existoval	existovat	k5eAaImAgMnS	existovat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
variantách	varianta	k1gFnPc6	varianta
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dosvědčují	dosvědčovat	k5eAaImIp3nP	dosvědčovat
kumránské	kumránský	k2eAgInPc1d1	kumránský
svitky	svitek	k1gInPc1	svitek
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
fragmenty	fragment	k1gInPc1	fragment
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
překladů	překlad	k1gInPc2	překlad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
přelomu	přelom	k1gInSc2	přelom
letopočtu	letopočet	k1gInSc2	letopočet
Židé	Žid	k1gMnPc1	Žid
již	již	k6eAd1	již
nemluvili	mluvit	k5eNaImAgMnP	mluvit
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řecky	řecky	k6eAd1	řecky
či	či	k8xC	či
aramejsky	aramejsky	k6eAd1	aramejsky
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
začala	začít	k5eAaPmAgFnS	začít
doba	doba	k1gFnSc1	doba
překladů	překlad	k1gInPc2	překlad
hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
bible	bible	k1gFnSc2	bible
–	–	k?	–
pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
je	být	k5eAaImIp3nS	být
nejvýznamnějším	významný	k2eAgMnSc6d3	nejvýznamnější
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
překladů	překlad	k1gInPc2	překlad
Septuaginta	Septuaginta	k1gFnSc1	Septuaginta
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
první	první	k4xOgMnPc1	první
křesťané	křesťan	k1gMnPc1	křesťan
používali	používat	k5eAaImAgMnP	používat
jako	jako	k8xC	jako
své	svůj	k3xOyFgNnSc4	svůj
"	"	kIx"	"
<g/>
Písmo	písmo	k1gNnSc4	písmo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
též	též	k9	též
židovské	židovský	k2eAgInPc1d1	židovský
překlady	překlad	k1gInPc1	překlad
či	či	k8xC	či
parafráze	parafráze	k1gFnPc1	parafráze
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
targumy	targum	k1gInPc7	targum
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Biblický	biblický	k2eAgInSc4d1	biblický
kánon	kánon	k1gInSc4	kánon
<g/>
.	.	kIx.	.
</s>
<s>
Judaismus	judaismus	k1gInSc1	judaismus
<g/>
,	,	kIx,	,
katolíci	katolík	k1gMnPc1	katolík
<g/>
,	,	kIx,	,
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
<g/>
,	,	kIx,	,
protestanti	protestant	k1gMnPc1	protestant
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
náboženské	náboženský	k2eAgFnPc1d1	náboženská
tradice	tradice	k1gFnPc1	tradice
uvádějí	uvádět	k5eAaImIp3nP	uvádět
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
svých	svůj	k3xOyFgNnPc2	svůj
Písem	písmo	k1gNnPc2	písmo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kánon	kánon	k1gInSc1	kánon
<g/>
,	,	kIx,	,
různý	různý	k2eAgInSc1d1	různý
počet	počet	k1gInSc1	počet
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
protestantská	protestantský	k2eAgNnPc4d1	protestantské
vydání	vydání	k1gNnPc4	vydání
mají	mít	k5eAaImIp3nP	mít
66	[number]	k4	66
<g/>
,	,	kIx,	,
katolická	katolický	k2eAgFnSc1d1	katolická
72	[number]	k4	72
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Židovský	židovský	k2eAgInSc1d1	židovský
neboli	neboli	k8xC	neboli
palestinský	palestinský	k2eAgInSc1d1	palestinský
kánon	kánon	k1gInSc1	kánon
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
judaismu	judaismus	k1gInSc2	judaismus
definován	definovat	k5eAaBmNgInS	definovat
synodou	synoda	k1gFnSc7	synoda
v	v	k7c6	v
Jabne	Jabn	k1gInSc5	Jabn
roku	rok	k1gInSc2	rok
92	[number]	k4	92
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
kánon	kánon	k1gInSc1	kánon
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgInS	vyvíjet
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
kánonu	kánon	k1gInSc6	kánon
judaismu	judaismus	k1gInSc2	judaismus
a	a	k8xC	a
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
staletí	staletí	k1gNnPc4	staletí
nebyl	být	k5eNaImAgInS	být
ustálen	ustálen	k2eAgInSc1d1	ustálen
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
užívala	užívat	k5eAaImAgFnS	užívat
řeckého	řecký	k2eAgInSc2d1	řecký
překladu	překlad	k1gInSc2	překlad
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
Septuaginty	Septuaginta	k1gFnSc2	Septuaginta
<g/>
,	,	kIx,	,
užívala	užívat	k5eAaImAgFnS	užívat
jejího	její	k3xOp3gInSc2	její
kánonu	kánon	k1gInSc2	kánon
<g/>
,	,	kIx,	,
označovaného	označovaný	k2eAgNnSc2d1	označované
jako	jako	k9	jako
kánon	kánon	k1gInSc1	kánon
alexandrijský	alexandrijský	k2eAgInSc1d1	alexandrijský
<g/>
.	.	kIx.	.
</s>
<s>
Protestantský	protestantský	k2eAgInSc1d1	protestantský
kánon	kánon	k1gInSc1	kánon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
přejímá	přejímat	k5eAaImIp3nS	přejímat
kánon	kánon	k1gInSc1	kánon
palestinský	palestinský	k2eAgInSc1d1	palestinský
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
počátek	počátek	k1gInSc4	počátek
v	v	k7c6	v
době	doba	k1gFnSc6	doba
protestantské	protestantský	k2eAgFnSc2d1	protestantská
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
definovala	definovat	k5eAaBmAgFnS	definovat
svůj	svůj	k3xOyFgInSc4	svůj
kánon	kánon	k1gInSc4	kánon
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
zhruba	zhruba	k6eAd1	zhruba
odpovídajícím	odpovídající	k2eAgInSc6d1	odpovídající
alexandrijskému	alexandrijský	k2eAgInSc3d1	alexandrijský
kánonu	kánon	k1gInSc3	kánon
a	a	k8xC	a
schválila	schválit	k5eAaPmAgFnS	schválit
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
Tridentském	tridentský	k2eAgInSc6d1	tridentský
koncilu	koncil	k1gInSc6	koncil
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1546	[number]	k4	1546
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
římskokatolického	římskokatolický	k2eAgInSc2d1	římskokatolický
kánonu	kánon	k1gInSc2	kánon
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
koncilu	koncil	k1gInSc6	koncil
stalo	stát	k5eAaPmAgNnS	stát
i	i	k9	i
sedm	sedm	k4xCc1	sedm
starozákonních	starozákonní	k2eAgFnPc2d1	starozákonní
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
palestinském	palestinský	k2eAgInSc6d1	palestinský
kánonu	kánon	k1gInSc6	kánon
nebyly	být	k5eNaImAgFnP	být
(	(	kIx(	(
<g/>
spor	spor	k1gInSc1	spor
s	s	k7c7	s
protestanty	protestant	k1gMnPc7	protestant
o	o	k7c4	o
inspiraci	inspirace	k1gFnSc4	inspirace
Bohem	bůh	k1gMnSc7	bůh
<g/>
)	)	kIx)	)
a	a	k8xC	a
nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
deuterokanonické	deuterokanonický	k2eAgFnPc1d1	deuterokanonická
<g/>
.	.	kIx.	.
</s>
<s>
Deuterokanonické	Deuterokanonický	k2eAgInPc4d1	Deuterokanonický
spisy	spis	k1gInPc4	spis
však	však	k9	však
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
kánonu	kánon	k1gInSc2	kánon
neuznávají	uznávat	k5eNaImIp3nP	uznávat
židé	žid	k1gMnPc1	žid
ani	ani	k8xC	ani
protestantské	protestantský	k2eAgFnPc1d1	protestantská
církve	církev	k1gFnPc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
napsány	napsán	k2eAgFnPc1d1	napsána
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
řecky	řecky	k6eAd1	řecky
<g/>
.	.	kIx.	.
</s>
<s>
Protestantské	protestantský	k2eAgFnPc1d1	protestantská
církve	církev	k1gFnPc1	církev
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
vydávají	vydávat	k5eAaPmIp3nP	vydávat
bible	bible	k1gFnSc2	bible
tyto	tento	k3xDgInPc1	tento
deuterokanonické	deuterokanonický	k2eAgInPc1d1	deuterokanonický
spisy	spis	k1gInPc1	spis
neobsahující	obsahující	k2eNgInPc1d1	neobsahující
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
Jan	Jan	k1gMnSc1	Jan
Ámos	Ámos	k1gMnSc1	Ámos
Komenský	Komenský	k1gMnSc1	Komenský
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
dobré	dobrý	k2eAgNnSc1d1	dobré
je	být	k5eAaImIp3nS	být
číst	číst	k5eAaImF	číst
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžných	běžný	k2eAgNnPc6d1	běžné
biblických	biblický	k2eAgNnPc6d1	biblické
vydáních	vydání	k1gNnPc6	vydání
je	být	k5eAaImIp3nS	být
text	text	k1gInSc1	text
každé	každý	k3xTgFnSc2	každý
knihy	kniha	k1gFnSc2	kniha
dělen	dělen	k2eAgInSc4d1	dělen
na	na	k7c4	na
kapitoly	kapitola	k1gFnPc4	kapitola
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
kapitola	kapitola	k1gFnSc1	kapitola
na	na	k7c4	na
verše	verš	k1gInPc4	verš
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
na	na	k7c4	na
kapitoly	kapitola	k1gFnPc4	kapitola
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Štěpána	Štěpán	k1gMnSc2	Štěpán
Langtona	Langton	k1gMnSc2	Langton
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1203	[number]	k4	1203
<g/>
,	,	kIx,	,
kapitoly	kapitola	k1gFnPc1	kapitola
posléze	posléze	k6eAd1	posléze
rozčlenil	rozčlenit	k5eAaPmAgMnS	rozčlenit
na	na	k7c4	na
verše	verš	k1gInPc4	verš
Robert	Robert	k1gMnSc1	Robert
Estienne	Estienn	k1gInSc5	Estienn
neboli	neboli	k8xC	neboli
Stephanus	Stephanus	k1gInSc4	Stephanus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
jako	jako	k9	jako
lingvista	lingvista	k1gMnSc1	lingvista
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c4	na
zdokonalení	zdokonalení	k1gNnSc4	zdokonalení
Erasmova	Erasmův	k2eAgNnSc2d1	Erasmovo
vydání	vydání	k1gNnSc2	vydání
biblického	biblický	k2eAgInSc2d1	biblický
textu	text	k1gInSc2	text
<g/>
:	:	kIx,	:
1551	[number]	k4	1551
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
a	a	k8xC	a
1555	[number]	k4	1555
(	(	kIx(	(
<g/>
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
ujalo	ujmout	k5eAaPmAgNnS	ujmout
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
bylo	být	k5eAaImAgNnS	být
praktické	praktický	k2eAgNnSc1d1	praktické
pro	pro	k7c4	pro
rychlou	rychlý	k2eAgFnSc4d1	rychlá
a	a	k8xC	a
přesnou	přesný	k2eAgFnSc4d1	přesná
orientaci	orientace	k1gFnSc4	orientace
v	v	k7c6	v
textu	text	k1gInSc6	text
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
česká	český	k2eAgFnSc1d1	Česká
bible	bible	k1gFnSc1	bible
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přejímá	přejímat	k5eAaImIp3nS	přejímat
dělení	dělení	k1gNnSc4	dělení
na	na	k7c4	na
verše	verš	k1gInPc4	verš
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Bible	bible	k1gFnSc1	bible
kralická	kralický	k2eAgFnSc1d1	Kralická
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
však	však	k9	však
jsou	být	k5eAaImIp3nP	být
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
některé	některý	k3yIgFnPc1	některý
knihy	kniha	k1gFnPc1	kniha
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
textových	textový	k2eAgFnPc2d1	textová
verzí	verze	k1gFnPc2	verze
lišících	lišící	k2eAgFnPc2d1	lišící
se	se	k3xPyFc4	se
délkou	délka	k1gFnSc7	délka
<g/>
,	,	kIx,	,
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
se	se	k3xPyFc4	se
i	i	k9	i
jejich	jejich	k3xOp3gNnSc1	jejich
číslování	číslování	k1gNnSc1	číslování
veršů	verš	k1gInPc2	verš
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
setkat	setkat	k5eAaPmF	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedním	jeden	k4xCgNnSc7	jeden
označením	označení	k1gNnSc7	označení
téhož	týž	k3xTgInSc2	týž
verše	verš	k1gInSc2	verš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnSc2	jaký
textové	textový	k2eAgFnSc2d1	textová
verze	verze	k1gFnSc2	verze
vydavatelé	vydavatel	k1gMnPc1	vydavatel
používají	používat	k5eAaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Odlišné	odlišný	k2eAgNnSc1d1	odlišné
dělení	dělení	k1gNnSc1	dělení
na	na	k7c4	na
kapitoly	kapitola	k1gFnPc4	kapitola
mají	mít	k5eAaImIp3nP	mít
některé	některý	k3yIgFnPc4	některý
východní	východní	k2eAgFnPc4d1	východní
jazykové	jazykový	k2eAgFnPc4d1	jazyková
verze	verze	k1gFnPc4	verze
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
syrské	syrský	k2eAgInPc1d1	syrský
překlady	překlad	k1gInPc1	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Biblické	biblický	k2eAgInPc1d1	biblický
spisy	spis	k1gInPc1	spis
jsou	být	k5eAaImIp3nP	být
jak	jak	k6eAd1	jak
v	v	k7c6	v
chápání	chápání	k1gNnSc6	chápání
judaismu	judaismus	k1gInSc2	judaismus
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
křesťanském	křesťanský	k2eAgNnSc6d1	křesťanské
chápání	chápání	k1gNnSc6	chápání
sbírkou	sbírka	k1gFnSc7	sbírka
svědectví	svědectví	k1gNnSc4	svědectví
o	o	k7c6	o
Božím	boží	k2eAgNnSc6d1	boží
zjevení	zjevení	k1gNnSc6	zjevení
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
o	o	k7c4	o
lidské	lidský	k2eAgFnPc4d1	lidská
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
celých	celá	k1gFnPc6	celá
svých	svůj	k3xOyFgMnPc2	svůj
dějinách	dějiny	k1gFnPc6	dějiny
k	k	k7c3	k
Písmu	písmo	k1gNnSc3	písmo
svatému	svatý	k1gMnSc3	svatý
úctu	úcta	k1gFnSc4	úcta
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
textech	text	k1gInPc6	text
Písma	písmo	k1gNnSc2	písmo
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
Božím	boží	k2eAgNnSc7d1	boží
slovem	slovo	k1gNnSc7	slovo
<g/>
;	;	kIx,	;
některé	některý	k3yIgInPc4	některý
církevní	církevní	k2eAgInPc4d1	církevní
dokumenty	dokument	k1gInPc4	dokument
dokládají	dokládat	k5eAaImIp3nP	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
Písmu	písmo	k1gNnSc3	písmo
má	mít	k5eAaImIp3nS	mít
církev	církev	k1gFnSc1	církev
podobnou	podobný	k2eAgFnSc4d1	podobná
úctu	úcta	k1gFnSc4	úcta
jako	jako	k9	jako
např.	např.	kA	např.
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
k	k	k7c3	k
eucharistii	eucharistie	k1gFnSc3	eucharistie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
církve	církev	k1gFnSc2	církev
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
z	z	k7c2	z
vnuknutí	vnuknutí	k1gNnSc2	vnuknutí
Ducha	duch	k1gMnSc2	duch
svatého	svatý	k1gMnSc2	svatý
(	(	kIx(	(
<g/>
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k9	jako
inspirace	inspirace	k1gFnSc1	inspirace
Písma	písmo	k1gNnSc2	písmo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
věří	věřit	k5eAaImIp3nP	věřit
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
,	,	kIx,	,
vybral	vybrat	k5eAaPmAgMnS	vybrat
lidské	lidský	k2eAgMnPc4d1	lidský
autory	autor	k1gMnPc4	autor
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
sepsání	sepsání	k1gNnSc3	sepsání
a	a	k8xC	a
skrze	skrze	k?	skrze
ně	on	k3xPp3gMnPc4	on
na	na	k7c4	na
sepsání	sepsání	k1gNnSc4	sepsání
textu	text	k1gInSc2	text
působil	působit	k5eAaImAgMnS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
také	také	k9	také
církev	církev	k1gFnSc1	církev
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k9	co
biblické	biblický	k2eAgInPc4d1	biblický
spisy	spis	k1gInPc4	spis
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spolehlivé	spolehlivý	k2eAgNnSc1d1	spolehlivé
<g/>
,	,	kIx,	,
věrné	věrný	k2eAgFnPc1d1	věrná
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
omylu	omyl	k1gInSc2	omyl
a	a	k8xC	a
pravdivé	pravdivý	k2eAgFnPc1d1	pravdivá
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
některá	některý	k3yIgNnPc4	některý
tvrzení	tvrzení	k1gNnPc4	tvrzení
vnitřně	vnitřně	k6eAd1	vnitřně
rozporná	rozporný	k2eAgNnPc4d1	rozporné
<g/>
.	.	kIx.	.
</s>
<s>
Pravdivostí	pravdivost	k1gFnSc7	pravdivost
se	se	k3xPyFc4	se
ale	ale	k9	ale
nemyslí	myslet	k5eNaImIp3nS	myslet
nutně	nutně	k6eAd1	nutně
doslovný	doslovný	k2eAgInSc4d1	doslovný
smysl	smysl	k1gInSc4	smysl
(	(	kIx(	(
<g/>
bibli	bible	k1gFnSc4	bible
nepovažují	považovat	k5eNaImIp3nP	považovat
křesťané	křesťan	k1gMnPc1	křesťan
např.	např.	kA	např.
za	za	k7c4	za
učebnici	učebnice	k1gFnSc4	učebnice
biologie	biologie	k1gFnSc2	biologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgInSc4	ten
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vypovídá	vypovídat	k5eAaImIp3nS	vypovídat
o	o	k7c6	o
Bohu	bůh	k1gMnSc6	bůh
a	a	k8xC	a
Božích	boží	k2eAgFnPc6d1	boží
věcech	věc	k1gFnPc6	věc
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
církve	církev	k1gFnPc1	církev
či	či	k8xC	či
někteří	některý	k3yIgMnPc1	některý
křesťané	křesťan	k1gMnPc1	křesťan
chápou	chápat	k5eAaImIp3nP	chápat
bibli	bible	k1gFnSc3	bible
jako	jako	k9	jako
absolutně	absolutně	k6eAd1	absolutně
přesnou	přesný	k2eAgFnSc4d1	přesná
(	(	kIx(	(
<g/>
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgInPc6	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
ji	on	k3xPp3gFnSc4	on
mohou	moct	k5eAaImIp3nP	moct
chybně	chybně	k6eAd1	chybně
interpretovat	interpretovat	k5eAaBmF	interpretovat
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
příklad	příklad	k1gInSc4	příklad
uveďme	uvést	k5eAaPmRp1nP	uvést
známý	známý	k2eAgInSc4d1	známý
spor	spor	k1gInSc4	spor
o	o	k7c4	o
heliocentrickou	heliocentrický	k2eAgFnSc4d1	heliocentrická
soustavu	soustava	k1gFnSc4	soustava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
církve	církev	k1gFnPc1	církev
vzešlé	vzešlý	k2eAgFnPc1d1	vzešlá
z	z	k7c2	z
protestantské	protestantský	k2eAgFnSc2d1	protestantská
reformace	reformace	k1gFnSc2	reformace
trvají	trvat	k5eAaImIp3nP	trvat
na	na	k7c6	na
doslovné	doslovný	k2eAgFnSc6d1	doslovná
inspiraci	inspirace	k1gFnSc6	inspirace
textu	text	k1gInSc2	text
Bohem	bůh	k1gMnSc7	bůh
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
biblický	biblický	k2eAgInSc1d1	biblický
fundamentalismus	fundamentalismus	k1gInSc1	fundamentalismus
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
knihy	kniha	k1gFnPc1	kniha
bible	bible	k1gFnSc2	bible
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
historických	historický	k2eAgNnPc2d1	historické
období	období	k1gNnPc2	období
<g/>
,	,	kIx,	,
křesťané	křesťan	k1gMnPc1	křesťan
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bible	bible	k1gFnSc1	bible
předává	předávat	k5eAaImIp3nS	předávat
jisté	jistý	k2eAgNnSc4d1	jisté
ucelené	ucelený	k2eAgNnSc4d1	ucelené
poselství	poselství	k1gNnSc4	poselství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
okamžiku	okamžik	k1gInSc6	okamžik
(	(	kIx(	(
<g/>
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
<g/>
)	)	kIx)	)
dějin	dějiny	k1gFnPc2	dějiny
lidé	člověk	k1gMnPc1	člověk
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
Boží	boží	k2eAgInSc4d1	boží
plán	plán	k1gInSc4	plán
a	a	k8xC	a
zhřešili	zhřešit	k5eAaPmAgMnP	zhřešit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
zhřešili	zhřešit	k5eAaPmAgMnP	zhřešit
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
tak	tak	k9	tak
nemohl	moct	k5eNaImAgMnS	moct
přijít	přijít	k5eAaPmF	přijít
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
Bůh	bůh	k1gMnSc1	bůh
se	se	k3xPyFc4	se
dával	dávat	k5eAaImAgMnS	dávat
člověku	člověk	k1gMnSc6	člověk
poznat	poznat	k5eAaPmF	poznat
srozumitelným	srozumitelný	k2eAgInSc7d1	srozumitelný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
Bůh	bůh	k1gMnSc1	bůh
mohl	moct	k5eAaImAgMnS	moct
člověka	člověk	k1gMnSc4	člověk
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
,	,	kIx,	,
povolal	povolat	k5eAaPmAgMnS	povolat
si	se	k3xPyFc3	se
Abraháma	Abrahám	k1gMnSc4	Abrahám
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
potomstvo	potomstvo	k1gNnSc4	potomstvo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
prostředkem	prostředek	k1gInSc7	prostředek
záchrany	záchrana	k1gFnSc2	záchrana
celého	celý	k2eAgNnSc2d1	celé
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
téhož	týž	k3xTgInSc2	týž
důvodu	důvod	k1gInSc2	důvod
daroval	darovat	k5eAaPmAgMnS	darovat
Bůh	bůh	k1gMnSc1	bůh
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Mojžíše	Mojžíš	k1gMnSc2	Mojžíš
izraelskému	izraelský	k2eAgInSc3d1	izraelský
národu	národ	k1gInSc3	národ
zákon	zákon	k1gInSc4	zákon
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Izraelský	izraelský	k2eAgInSc1d1	izraelský
národ	národ	k1gInSc1	národ
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svých	svůj	k3xOyFgFnPc2	svůj
dějin	dějiny	k1gFnPc2	dějiny
odvracel	odvracet	k5eAaImAgMnS	odvracet
od	od	k7c2	od
Boha	bůh	k1gMnSc2	bůh
a	a	k8xC	a
zase	zase	k9	zase
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
navracel	navracet	k5eAaBmAgMnS	navracet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
Bohem	bůh	k1gMnSc7	bůh
poslaní	poslaný	k2eAgMnPc1d1	poslaný
proroci	prorok	k1gMnPc1	prorok
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
napomínání	napomínání	k1gNnSc2	napomínání
Izraelců	Izraelec	k1gMnPc2	Izraelec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
navrátili	navrátit	k5eAaPmAgMnP	navrátit
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
také	také	k9	také
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zákonu	zákon	k1gInSc6	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Bůh	bůh	k1gMnSc1	bůh
lidem	člověk	k1gMnPc3	člověk
dal	dát	k5eAaPmAgMnS	dát
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
pouze	pouze	k6eAd1	pouze
samoúčelné	samoúčelný	k2eAgInPc4d1	samoúčelný
příkazy	příkaz	k1gInPc4	příkaz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
zákon	zákon	k1gInSc1	zákon
odráží	odrážet	k5eAaImIp3nS	odrážet
něco	něco	k3yInSc4	něco
z	z	k7c2	z
vlastností	vlastnost	k1gFnPc2	vlastnost
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgMnSc1d1	dobrý
a	a	k8xC	a
touží	toužit	k5eAaImIp3nS	toužit
po	po	k7c4	po
co	co	k3yQnSc4	co
největším	veliký	k2eAgNnSc6d3	veliký
dobru	dobro	k1gNnSc6	dobro
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Prohloubené	prohloubený	k2eAgNnSc1d1	prohloubené
porozumění	porozumění	k1gNnSc1	porozumění
-	-	kIx~	-
přímo	přímo	k6eAd1	přímo
naplnění	naplnění	k1gNnSc1	naplnění
-	-	kIx~	-
Božího	boží	k2eAgInSc2d1	boží
zákona	zákon	k1gInSc2	zákon
přinesl	přinést	k5eAaPmAgMnS	přinést
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
společně	společně	k6eAd1	společně
s	s	k7c7	s
pozváním	pozvání	k1gNnSc7	pozvání
do	do	k7c2	do
Božího	boží	k2eAgNnSc2d1	boží
království	království	k1gNnSc2	království
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
v	v	k7c4	v
Něj	on	k3xPp3gNnSc4	on
věří	věřit	k5eAaImIp3nS	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
tak	tak	k6eAd1	tak
přinesl	přinést	k5eAaPmAgInS	přinést
milost	milost	k1gFnSc4	milost
<g/>
,	,	kIx,	,
odpuštění	odpuštění	k1gNnSc4	odpuštění
a	a	k8xC	a
slitování	slitování	k1gNnSc1	slitování
proti	proti	k7c3	proti
požadavkům	požadavek	k1gInPc3	požadavek
Zákona	zákon	k1gInSc2	zákon
(	(	kIx(	(
<g/>
Mojžíšova	Mojžíšův	k2eAgFnSc1d1	Mojžíšova
<g/>
)	)	kIx)	)
<g/>
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
Zákon	zákon	k1gInSc4	zákon
uznali	uznat	k5eAaPmAgMnP	uznat
a	a	k8xC	a
pro	pro	k7c4	pro
něž	jenž	k3xRgMnPc4	jenž
Zákon	zákon	k1gInSc1	zákon
existuje	existovat	k5eAaImIp3nS	existovat
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
existuje	existovat	k5eAaImIp3nS	existovat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
až	až	k9	až
je	on	k3xPp3gFnPc4	on
hranice	hranice	k1gFnPc4	hranice
"	"	kIx"	"
<g/>
dobra	dobro	k1gNnSc2	dobro
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
dokonalosti	dokonalost	k1gFnPc4	dokonalost
<g/>
"	"	kIx"	"
a	a	k8xC	a
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
nemohou	moct	k5eNaImIp3nP	moct
vlastními	vlastní	k2eAgFnPc7d1	vlastní
silami	síla	k1gFnPc7	síla
ani	ani	k8xC	ani
skutky	skutek	k1gInPc7	skutek
nikdy	nikdy	k6eAd1	nikdy
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
odpuštění	odpuštění	k1gNnSc4	odpuštění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
Boží	boží	k2eAgFnSc7d1	boží
milostí	milost	k1gFnSc7	milost
dovedeno	dovést	k5eAaPmNgNnS	dovést
k	k	k7c3	k
dokonalosti	dokonalost	k1gFnSc3	dokonalost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
a	a	k8xC	a
svým	svůj	k3xOyFgNnSc7	svůj
vzkříšením	vzkříšení	k1gNnSc7	vzkříšení
získal	získat	k5eAaPmAgMnS	získat
Ježíš	Ježíš	k1gMnSc1	Ježíš
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
v	v	k7c4	v
něho	on	k3xPp3gMnSc4	on
uvěřili	uvěřit	k5eAaPmAgMnP	uvěřit
<g/>
,	,	kIx,	,
záchranu	záchrana	k1gFnSc4	záchrana
a	a	k8xC	a
smíření	smíření	k1gNnSc4	smíření
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
si	se	k3xPyFc3	se
vyvolil	vyvolit	k5eAaPmAgMnS	vyvolit
své	svůj	k3xOyFgMnPc4	svůj
učedníky	učedník	k1gMnPc4	učedník
<g/>
,	,	kIx,	,
apoštoly	apoštol	k1gMnPc4	apoštol
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
zbudoval	zbudovat	k5eAaPmAgInS	zbudovat
nový	nový	k2eAgInSc1d1	nový
Boží	boží	k2eAgInSc1d1	boží
lid	lid	k1gInSc1	lid
(	(	kIx(	(
<g/>
církev	církev	k1gFnSc1	církev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
nadějí	naděje	k1gFnSc7	naděje
očekává	očekávat	k5eAaImIp3nS	očekávat
Ježíšův	Ježíšův	k2eAgInSc1d1	Ježíšův
příchod	příchod	k1gInSc1	příchod
na	na	k7c6	na
konci	konec	k1gInSc6	konec
časů	čas	k1gInPc2	čas
a	a	k8xC	a
naplnění	naplnění	k1gNnSc2	naplnění
jeho	jeho	k3xOp3gNnSc2	jeho
zaslíbení	zaslíbení	k1gNnSc2	zaslíbení
<g/>
.	.	kIx.	.
</s>
<s>
Bible	bible	k1gFnSc1	bible
jako	jako	k8xC	jako
základní	základní	k2eAgFnSc1d1	základní
kniha	kniha	k1gFnSc1	kniha
silně	silně	k6eAd1	silně
dominantního	dominantní	k2eAgNnSc2d1	dominantní
náboženství	náboženství	k1gNnSc2	náboženství
měla	mít	k5eAaImAgFnS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
formování	formování	k1gNnSc4	formování
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
západní	západní	k2eAgFnSc2d1	západní
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
byla	být	k5eAaImAgFnS	být
věnována	věnovat	k5eAaImNgFnS	věnovat
nejen	nejen	k6eAd1	nejen
knize	kniha	k1gFnSc3	kniha
samotné	samotný	k2eAgFnSc3d1	samotná
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
tištěná	tištěný	k2eAgFnSc1d1	tištěná
kniha	kniha	k1gFnSc1	kniha
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
jejímu	její	k3xOp3gInSc3	její
obsahu	obsah	k1gInSc3	obsah
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
až	až	k6eAd1	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
inspiračních	inspirační	k2eAgInPc2d1	inspirační
zdrojů	zdroj	k1gInPc2	zdroj
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Biblická	biblický	k2eAgNnPc1d1	biblické
témata	téma	k1gNnPc1	téma
pronikala	pronikat	k5eAaImAgNnP	pronikat
i	i	k9	i
do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
Quo	Quo	k1gFnSc1	Quo
vadis	vadis	k1gFnSc2	vadis
<g/>
)	)	kIx)	)
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
motivy	motiv	k1gInPc1	motiv
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
všeobecně	všeobecně	k6eAd1	všeobecně
známými	známý	k1gMnPc7	známý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Adam	Adam	k1gMnSc1	Adam
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
<g/>
,	,	kIx,	,
potopa	potopa	k1gFnSc1	potopa
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
solný	solný	k2eAgInSc1d1	solný
sloup	sloup	k1gInSc1	sloup
<g/>
,	,	kIx,	,
milosrdný	milosrdný	k2eAgMnSc1d1	milosrdný
Samaritán	Samaritán	k1gMnSc1	Samaritán
<g/>
,	,	kIx,	,
velbloud	velbloud	k1gMnSc1	velbloud
uchem	ucho	k1gNnSc7	ucho
jehly	jehla	k1gFnSc2	jehla
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Překlady	překlad	k1gInPc4	překlad
bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Bible	bible	k1gFnSc1	bible
je	být	k5eAaImIp3nS	být
nejpřekládanější	překládaný	k2eAgFnSc7d3	nejpřekládanější
knihou	kniha	k1gFnSc7	kniha
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
alespoň	alespoň	k9	alespoň
část	část	k1gFnSc1	část
bible	bible	k1gFnSc2	bible
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
2454	[number]	k4	2454
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
překlady	překlad	k1gInPc1	překlad
byly	být	k5eAaImAgInP	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
,	,	kIx,	,
od	od	k7c2	od
období	období	k1gNnSc2	období
humanismu	humanismus	k1gInSc2	humanismus
se	se	k3xPyFc4	se
studují	studovat	k5eAaImIp3nP	studovat
původní	původní	k2eAgInPc1d1	původní
biblické	biblický	k2eAgInPc1d1	biblický
jazyky	jazyk	k1gInPc1	jazyk
a	a	k8xC	a
klade	klást	k5eAaImIp3nS	klást
se	se	k3xPyFc4	se
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
přesnost	přesnost	k1gFnSc4	přesnost
a	a	k8xC	a
věrnost	věrnost	k1gFnSc4	věrnost
biblických	biblický	k2eAgInPc2d1	biblický
překladů	překlad	k1gInPc2	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Překlady	překlad	k1gInPc1	překlad
Písma	písmo	k1gNnSc2	písmo
svatého	svatý	k1gMnSc2	svatý
mají	mít	k5eAaImIp3nP	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
klasické	klasický	k2eAgFnSc2d1	klasická
řečtiny	řečtina	k1gFnSc2	řečtina
se	se	k3xPyFc4	se
překládal	překládat	k5eAaImAgMnS	překládat
pouze	pouze	k6eAd1	pouze
Starý	starý	k2eAgInSc4d1	starý
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
jazyce	jazyk	k1gInSc6	jazyk
psán	psán	k2eAgInSc1d1	psán
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
před	před	k7c7	před
přelomem	přelom	k1gInSc7	přelom
letopočtu	letopočet	k1gInSc2	letopočet
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
Alexandrii	Alexandrie	k1gFnSc6	Alexandrie
řecký	řecký	k2eAgInSc4d1	řecký
překlad	překlad	k1gInSc4	překlad
bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Septuaginta	Septuaginta	k1gFnSc1	Septuaginta
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
sedmdesát	sedmdesát	k4xCc4	sedmdesát
podle	podle	k7c2	podle
legendárního	legendární	k2eAgInSc2d1	legendární
počtu	počet	k1gInSc2	počet
72	[number]	k4	72
překladatelů	překladatel	k1gMnPc2	překladatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
latinským	latinský	k2eAgInSc7d1	latinský
překladem	překlad	k1gInSc7	překlad
byla	být	k5eAaImAgFnS	být
tzv.	tzv.	kA	tzv.
Vulgata	Vulgata	k1gFnSc1	Vulgata
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
svatý	svatý	k2eAgMnSc1d1	svatý
Jeroným	Jeroným	k1gMnSc1	Jeroným
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
známým	známý	k2eAgInPc3d1	známý
překladům	překlad	k1gInPc3	překlad
z	z	k7c2	z
původních	původní	k2eAgMnPc2d1	původní
jazyků	jazyk	k1gInPc2	jazyk
patří	patřit	k5eAaImIp3nS	patřit
díla	dílo	k1gNnPc4	dílo
protestanských	protestanský	k2eAgMnPc2d1	protestanský
teologů	teolog	k1gMnPc2	teolog
z	z	k7c2	z
období	období	k1gNnSc2	období
reformace	reformace	k1gFnSc2	reformace
<g/>
,	,	kIx,	,
německý	německý	k2eAgInSc1d1	německý
překlad	překlad	k1gInSc1	překlad
Martina	Martin	k1gMnSc2	Martin
Luthera	Luther	k1gMnSc2	Luther
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgInSc1d1	francouzský
překlad	překlad	k1gInSc1	překlad
Jana	Jan	k1gMnSc2	Jan
Kalvína	Kalvín	k1gMnSc2	Kalvín
(	(	kIx(	(
<g/>
Ženevská	ženevský	k2eAgFnSc1d1	Ženevská
bible	bible	k1gFnSc1	bible
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
Bible	bible	k1gFnSc1	bible
kralická	kralický	k2eAgFnSc1d1	Kralická
<g/>
,	,	kIx,	,
italský	italský	k2eAgInSc1d1	italský
překlad	překlad	k1gInSc1	překlad
Giovanniho	Giovanni	k1gMnSc2	Giovanni
Diodati	Diodati	k1gFnSc1	Diodati
či	či	k8xC	či
anglická	anglický	k2eAgFnSc1d1	anglická
(	(	kIx(	(
<g/>
anglikánská	anglikánský	k2eAgFnSc1d1	anglikánská
<g/>
)	)	kIx)	)
Bible	bible	k1gFnSc1	bible
krále	král	k1gMnSc2	král
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
zůstala	zůstat	k5eAaPmAgFnS	zůstat
věrná	věrný	k2eAgFnSc1d1	věrná
revidovanému	revidovaný	k2eAgInSc3d1	revidovaný
textu	text	k1gInSc3	text
Vulgaty	Vulgata	k1gFnSc2	Vulgata
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vydávání	vydávání	k1gNnSc1	vydávání
bible	bible	k1gFnSc2	bible
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
věnují	věnovat	k5eAaImIp3nP	věnovat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
biblická	biblický	k2eAgFnSc1d1	biblická
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
IBS	IBS	kA	IBS
<g/>
)	)	kIx)	)
a	a	k8xC	a
Wycliffovi	Wycliffův	k2eAgMnPc1d1	Wycliffův
překladatelé	překladatel	k1gMnPc1	překladatel
bible	bible	k1gFnSc2	bible
(	(	kIx(	(
<g/>
WBT	WBT	kA	WBT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
organizace	organizace	k1gFnPc1	organizace
vydávají	vydávat	k5eAaPmIp3nP	vydávat
bibli	bible	k1gFnSc3	bible
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnSc4	její
část	část	k1gFnSc4	část
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
600	[number]	k4	600
jazycích	jazyk	k1gInPc6	jazyk
pro	pro	k7c4	pro
127	[number]	k4	127
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Potřeba	potřeba	k1gFnSc1	potřeba
nových	nový	k2eAgInPc2d1	nový
překladů	překlad	k1gInPc2	překlad
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
jazyk	jazyk	k1gInSc1	jazyk
podléhá	podléhat	k5eAaImIp3nS	podléhat
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Překlady	překlad	k1gInPc1	překlad
se	se	k3xPyFc4	se
také	také	k9	také
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
zaměření	zaměření	k1gNnSc2	zaměření
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
se	se	k3xPyFc4	se
například	například	k6eAd1	například
snaží	snažit	k5eAaImIp3nS	snažit
text	text	k1gInSc1	text
co	co	k3yQnSc4	co
nejvíce	hodně	k6eAd3	hodně
přiblížit	přiblížit	k5eAaPmF	přiblížit
dnešnímu	dnešní	k2eAgMnSc3d1	dnešní
čtenáři	čtenář	k1gMnSc3	čtenář
<g/>
,	,	kIx,	,
jiné	jiná	k1gFnPc1	jiná
přihlížejí	přihlížet	k5eAaImIp3nP	přihlížet
k	k	k7c3	k
tradičním	tradiční	k2eAgInPc3d1	tradiční
překladům	překlad	k1gInPc3	překlad
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
překlad	překlad	k1gInSc4	překlad
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
co	co	k9	co
nejvěrněji	věrně	k6eAd3	věrně
struktuře	struktura	k1gFnSc3	struktura
původních	původní	k2eAgInPc2d1	původní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
České	český	k2eAgInPc1d1	český
překlady	překlad	k1gInPc1	překlad
bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
počinem	počin	k1gInSc7	počin
byl	být	k5eAaImAgInS	být
překlad	překlad	k1gInSc1	překlad
části	část	k1gFnSc2	část
Písma	písmo	k1gNnSc2	písmo
(	(	kIx(	(
<g/>
evangelií	evangelium	k1gNnPc2	evangelium
<g/>
)	)	kIx)	)
do	do	k7c2	do
staroslověnského	staroslověnský	k2eAgInSc2d1	staroslověnský
jazyka	jazyk	k1gInSc2	jazyk
sv.	sv.	kA	sv.
Konstantinem	Konstantin	k1gMnSc7	Konstantin
a	a	k8xC	a
Metodějem	Metoděj	k1gMnSc7	Metoděj
během	během	k7c2	během
jejich	jejich	k3xOp3gFnSc2	jejich
velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
mise	mise	k1gFnSc2	mise
(	(	kIx(	(
<g/>
počátek	počátek	k1gInSc1	počátek
862	[number]	k4	862
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
předmluva	předmluva	k1gFnSc1	předmluva
(	(	kIx(	(
<g/>
Proglas	Proglas	k1gInSc1	Proglas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
překladu	překlad	k1gInSc2	překlad
vychází	vycházet	k5eAaImIp3nS	vycházet
česká	český	k2eAgFnSc1d1	Česká
překladatelská	překladatelský	k2eAgFnSc1d1	překladatelská
tradice	tradice	k1gFnSc1	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
nejstarší	starý	k2eAgInPc1d3	nejstarší
české	český	k2eAgInPc1d1	český
překlady	překlad	k1gInPc1	překlad
z	z	k7c2	z
Vulgaty	Vulgata	k1gFnSc2	Vulgata
–	–	k?	–
např.	např.	kA	např.
Bible	bible	k1gFnSc1	bible
leskovecko-drážďanská	leskoveckorážďanský	k2eAgFnSc1d1	leskovecko-drážďanská
(	(	kIx(	(
<g/>
1360	[number]	k4	1360
<g/>
)	)	kIx)	)
či	či	k8xC	či
Bible	bible	k1gFnSc1	bible
olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
a	a	k8xC	a
Bible	bible	k1gFnSc1	bible
třeboňská	třeboňský	k2eAgFnSc1d1	Třeboňská
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
a	a	k8xC	a
nejznámějším	známý	k2eAgInSc7d3	nejznámější
českým	český	k2eAgInSc7d1	český
překladem	překlad	k1gInSc7	překlad
je	být	k5eAaImIp3nS	být
Bible	bible	k1gFnSc1	bible
kralická	kralický	k2eAgFnSc1d1	Kralická
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Šestidílka	Šestidílka	k1gFnSc1	Šestidílka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1579	[number]	k4	1579
<g/>
–	–	k?	–
<g/>
1594	[number]	k4	1594
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
revize	revize	k1gFnSc1	revize
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1613	[number]	k4	1613
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
český	český	k2eAgInSc4d1	český
překlad	překlad	k1gInSc4	překlad
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Bible	bible	k1gFnSc1	bible
kralických	kralický	k2eAgMnPc2d1	kralický
bratří	bratr	k1gMnPc2	bratr
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
dílem	díl	k1gInSc7	díl
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
byla	být	k5eAaImAgFnS	být
vůbec	vůbec	k9	vůbec
první	první	k4xOgInSc4	první
<g/>
,	,	kIx,	,
na	na	k7c6	na
světě	svět	k1gInSc6	svět
pátý	pátý	k4xOgInSc1	pátý
národní	národní	k2eAgInSc1d1	národní
překlad	překlad	k1gInSc1	překlad
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
katolickými	katolický	k2eAgInPc7d1	katolický
barokními	barokní	k2eAgInPc7d1	barokní
překlady	překlad	k1gInPc7	překlad
vyniká	vynikat	k5eAaImIp3nS	vynikat
třísvazková	třísvazkový	k2eAgFnSc1d1	třísvazková
Bible	bible	k1gFnSc1	bible
svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1677	[number]	k4	1677
<g/>
–	–	k?	–
<g/>
1715	[number]	k4	1715
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
Český	český	k2eAgInSc1d1	český
ekumenický	ekumenický	k2eAgInSc1d1	ekumenický
překlad	překlad	k1gInSc1	překlad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikal	vznikat	k5eAaImAgInS	vznikat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
pracovalo	pracovat	k5eAaImAgNnS	pracovat
nebo	nebo	k8xC	nebo
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
několika	několik	k4yIc6	několik
nových	nový	k2eAgInPc6d1	nový
překladech	překlad	k1gInPc6	překlad
bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
