<p>
<s>
Vydra	vydra	k1gFnSc1	vydra
říční	říční	k2eAgFnSc1d1	říční
(	(	kIx(	(
<g/>
Lutra	Lutra	k1gFnSc1	Lutra
lutra	lutra	k1gFnSc1	lutra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
druhem	druh	k1gInSc7	druh
vydry	vydra	k1gFnSc2	vydra
vyskytujícím	vyskytující	k2eAgInPc3d1	vyskytující
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
je	být	k5eAaImIp3nS	být
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
spatřit	spatřit	k5eAaPmF	spatřit
poblíž	poblíž	k7c2	poblíž
stojatých	stojatý	k2eAgFnPc2d1	stojatá
i	i	k8xC	i
tekoucích	tekoucí	k2eAgFnPc2d1	tekoucí
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
až	až	k9	až
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
nebo	nebo	k8xC	nebo
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
však	však	k9	však
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
sladké	sladký	k2eAgFnSc3d1	sladká
vodě	voda	k1gFnSc3	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
57	[number]	k4	57
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
ocasu	ocas	k1gInSc2	ocas
<g/>
:	:	kIx,	:
27	[number]	k4	27
<g/>
–	–	k?	–
<g/>
55	[number]	k4	55
cm	cm	kA	cm
</s>
<s>
Vydra	Vydra	k1gFnSc1	Vydra
říční	říční	k2eAgFnSc2d1	říční
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
lasicovitá	lasicovitý	k2eAgFnSc1d1	lasicovitá
šelma	šelma	k1gFnSc1	šelma
s	s	k7c7	s
protáhlým	protáhlý	k2eAgNnSc7d1	protáhlé
<g/>
,	,	kIx,	,
štíhlým	štíhlý	k2eAgNnSc7d1	štíhlé
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
krátkými	krátký	k2eAgFnPc7d1	krátká
končetinami	končetina	k1gFnPc7	končetina
opatřenými	opatřený	k2eAgFnPc7d1	opatřená
plovacími	plovací	k2eAgFnPc7d1	plovací
blánami	blána	k1gFnPc7	blána
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
svalnatý	svalnatý	k2eAgInSc1d1	svalnatý
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
zúžený	zúžený	k2eAgMnSc1d1	zúžený
<g/>
.	.	kIx.	.
</s>
<s>
Nozdry	nozdra	k1gFnPc1	nozdra
a	a	k8xC	a
ušní	ušní	k2eAgInPc1d1	ušní
otvory	otvor	k1gInPc1	otvor
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
kožními	kožní	k2eAgInPc7d1	kožní
záhyby	záhyb	k1gInPc7	záhyb
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
vydře	vydře	k1gNnSc4	vydře
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
je	on	k3xPp3gNnSc4	on
při	při	k7c6	při
ponoru	ponor	k1gInSc6	ponor
zcela	zcela	k6eAd1	zcela
uzavřít	uzavřít	k5eAaPmF	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
krátkou	krátká	k1gFnSc4	krátká
světle	světle	k6eAd1	světle
až	až	k9	až
tmavě	tmavě	k6eAd1	tmavě
hnědou	hnědý	k2eAgFnSc4d1	hnědá
srst	srst	k1gFnSc4	srst
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
spodinou	spodina	k1gFnSc7	spodina
<g/>
.	.	kIx.	.
</s>
<s>
Pohlaví	pohlaví	k1gNnSc1	pohlaví
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
zbarvením	zbarvení	k1gNnSc7	zbarvení
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Vydra	vydra	k1gFnSc1	vydra
říční	říční	k2eAgNnSc1d1	říční
žije	žít	k5eAaImIp3nS	žít
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
samotářským	samotářský	k2eAgInSc7d1	samotářský
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
mnohdy	mnohdy	k6eAd1	mnohdy
větším	většit	k5eAaImIp1nS	většit
jak	jak	k6eAd1	jak
30	[number]	k4	30
km	km	kA	km
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
značí	značit	k5eAaImIp3nS	značit
trusem	trus	k1gInSc7	trus
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
teritoria	teritorium	k1gNnSc2	teritorium
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
kvalitě	kvalita	k1gFnSc6	kvalita
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
teritorium	teritorium	k1gNnSc4	teritorium
si	se	k3xPyFc3	se
však	však	k9	však
střeží	střežit	k5eAaImIp3nS	střežit
proti	proti	k7c3	proti
jedincům	jedinec	k1gMnPc3	jedinec
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nám	my	k3xPp1nPc3	my
často	často	k6eAd1	často
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dvě	dva	k4xCgNnPc1	dva
rozdílná	rozdílný	k2eAgNnPc1d1	rozdílné
teritoria	teritorium	k1gNnPc1	teritorium
jedinců	jedinec	k1gMnPc2	jedinec
různého	různý	k2eAgNnSc2d1	různé
pohlaví	pohlaví	k1gNnSc2	pohlaví
překrývají	překrývat	k5eAaImIp3nP	překrývat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vydra	Vydra	k1gMnSc1	Vydra
je	být	k5eAaImIp3nS	být
skvělý	skvělý	k2eAgMnSc1d1	skvělý
plavec	plavec	k1gMnSc1	plavec
a	a	k8xC	a
potápěč	potápěč	k1gMnSc1	potápěč
a	a	k8xC	a
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
může	moct	k5eAaImIp3nS	moct
vydržet	vydržet	k5eAaPmF	vydržet
i	i	k9	i
déle	dlouho	k6eAd2	dlouho
jak	jak	k8xS	jak
5	[number]	k4	5
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
může	moct	k5eAaImIp3nS	moct
uplavat	uplavat	k5eAaPmF	uplavat
až	až	k9	až
400	[number]	k4	400
m.	m.	k?	m.
Na	na	k7c4	na
lov	lov	k1gInSc4	lov
se	se	k3xPyFc4	se
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
lokalit	lokalita	k1gFnPc2	lokalita
vydává	vydávat	k5eAaPmIp3nS	vydávat
až	až	k9	až
v	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
části	část	k1gFnSc6	část
odpoledne	odpoledne	k1gNnSc2	odpoledne
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
není	být	k5eNaImIp3nS	být
sluneční	sluneční	k2eAgNnSc1d1	sluneční
záření	záření	k1gNnSc1	záření
tak	tak	k6eAd1	tak
silné	silný	k2eAgNnSc1d1	silné
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrabává	vyhrabávat	k5eAaImIp3nS	vyhrabávat
si	se	k3xPyFc3	se
poměrně	poměrně	k6eAd1	poměrně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
noru	nora	k1gFnSc4	nora
s	s	k7c7	s
doupětem	doupě	k1gNnSc7	doupě
v	v	k7c6	v
hlinitých	hlinitý	k2eAgInPc6d1	hlinitý
březích	břeh	k1gInPc6	břeh
nebo	nebo	k8xC	nebo
pod	pod	k7c4	pod
kořeny	kořen	k1gInPc4	kořen
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
do	do	k7c2	do
ni	on	k3xPp3gFnSc4	on
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
vchod	vchod	k1gInSc1	vchod
pod	pod	k7c7	pod
vodní	vodní	k2eAgFnSc7d1	vodní
hladinou	hladina	k1gFnSc7	hladina
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
nepřístupnějším	přístupný	k2eNgMnSc6d2	nepřístupnější
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
vydřích	vydří	k2eAgMnPc2d1	vydří
predátorů	predátor	k1gMnPc2	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
zkonzumuje	zkonzumovat	k5eAaPmIp3nS	zkonzumovat
vydra	vydra	k1gFnSc1	vydra
říční	říční	k2eAgFnSc1d1	říční
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
především	především	k9	především
vodními	vodní	k2eAgMnPc7d1	vodní
živočichy	živočich	k1gMnPc7	živočich
<g/>
,	,	kIx,	,
zvláště	zvláště	k9	zvláště
pak	pak	k6eAd1	pak
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
i	i	k9	i
měkkýši	měkkýš	k1gMnPc7	měkkýš
<g/>
,	,	kIx,	,
obojživelníky	obojživelník	k1gMnPc7	obojživelník
<g/>
,	,	kIx,	,
drobnými	drobný	k2eAgMnPc7d1	drobný
savci	savec	k1gMnPc7	savec
nebo	nebo	k8xC	nebo
mladými	mladý	k2eAgMnPc7d1	mladý
vodními	vodní	k2eAgMnPc7d1	vodní
ptáky	pták	k1gMnPc7	pták
<g/>
.	.	kIx.	.
</s>
<s>
Vydří	vydří	k2eAgInSc1d1	vydří
trus	trus	k1gInSc1	trus
je	být	k5eAaImIp3nS	být
protáhlého	protáhlý	k2eAgInSc2d1	protáhlý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
rybích	rybí	k2eAgFnPc2d1	rybí
šupin	šupina	k1gFnPc2	šupina
nebo	nebo	k8xC	nebo
kůstek	kůstka	k1gFnPc2	kůstka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pářit	pářit	k5eAaImF	pářit
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Páření	páření	k1gNnSc1	páření
přitom	přitom	k6eAd1	přitom
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
březosti	březost	k1gFnSc2	březost
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejčastěji	často	k6eAd3	často
trvá	trvat	k5eAaImIp3nS	trvat
63	[number]	k4	63
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
občas	občas	k6eAd1	občas
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
utajené	utajený	k2eAgFnSc3d1	utajená
březosti	březost	k1gFnSc3	březost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vrhu	vrh	k1gInSc6	vrh
bývá	bývat	k5eAaImIp3nS	bývat
jedno	jeden	k4xCgNnSc1	jeden
až	až	k8xS	až
čtyři	čtyři	k4xCgNnPc4	čtyři
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
slepá	slepý	k2eAgFnSc1d1	slepá
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
neosrstěná	osrstěný	k2eNgFnSc1d1	neosrstěná
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
otevírají	otevírat	k5eAaImIp3nP	otevírat
po	po	k7c6	po
31	[number]	k4	31
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
dnech	den	k1gInPc6	den
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
a	a	k8xC	a
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
ve	v	k7c6	v
druhém	druhý	k4xOgMnSc6	druhý
až	až	k8xS	až
třetím	třetí	k4xOgInSc6	třetí
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dožít	dožít	k5eAaPmF	dožít
i	i	k9	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Vydry	vydra	k1gFnPc1	vydra
sice	sice	k8xC	sice
mají	mít	k5eAaImIp3nP	mít
přirozené	přirozený	k2eAgMnPc4d1	přirozený
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejnebezpečnějším	bezpečný	k2eNgMnSc7d3	nejnebezpečnější
nepřítelem	nepřítel	k1gMnSc7	nepřítel
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
lokalitách	lokalita	k1gFnPc6	lokalita
jejího	její	k3xOp3gInSc2	její
areálu	areál	k1gInSc2	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
až	až	k6eAd1	až
drastický	drastický	k2eAgInSc1d1	drastický
pokles	pokles	k1gInSc1	pokles
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgFnPc2d1	žijící
vyder	vydra	k1gFnPc2	vydra
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gFnSc3	jejich
náchylnosti	náchylnost	k1gFnSc3	náchylnost
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
znečištěnou	znečištěný	k2eAgFnSc4d1	znečištěná
jedovatými	jedovatý	k2eAgFnPc7d1	jedovatá
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
jistě	jistě	k6eAd1	jistě
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
ilegální	ilegální	k2eAgInSc1d1	ilegální
lov	lov	k1gInSc1	lov
pro	pro	k7c4	pro
vysoce	vysoce	k6eAd1	vysoce
ceněnou	ceněný	k2eAgFnSc4d1	ceněná
kožešinu	kožešina	k1gFnSc4	kožešina
a	a	k8xC	a
ztráta	ztráta	k1gFnSc1	ztráta
přirozeného	přirozený	k2eAgInSc2d1	přirozený
biomu	biom	k1gInSc2	biom
<g/>
.	.	kIx.	.
<g/>
Dle	dle	k7c2	dle
Červeného	Červeného	k2eAgInSc2d1	Červeného
seznamu	seznam	k1gInSc2	seznam
IUCN	IUCN	kA	IUCN
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
globálně	globálně	k6eAd1	globálně
zatím	zatím	k6eAd1	zatím
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
téměř	téměř	k6eAd1	téměř
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
a	a	k8xC	a
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
jako	jako	k9	jako
ubývající	ubývající	k2eAgFnSc1d1	ubývající
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohých	mnohý	k2eAgFnPc6d1	mnohá
částech	část	k1gFnPc6	část
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
však	však	k9	však
díky	díky	k7c3	díky
přísným	přísný	k2eAgNnPc3d1	přísné
záchranářským	záchranářský	k2eAgNnPc3d1	záchranářské
opatřením	opatření	k1gNnPc3	opatření
podařilo	podařit	k5eAaPmAgNnS	podařit
stav	stav	k1gInSc4	stav
vydry	vydra	k1gFnSc2	vydra
říční	říční	k2eAgFnSc2d1	říční
znovu	znovu	k6eAd1	znovu
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
státech	stát	k1gInPc6	stát
také	také	k9	také
platí	platit	k5eAaImIp3nP	platit
přísná	přísný	k2eAgNnPc1d1	přísné
ochranářská	ochranářský	k2eAgNnPc1d1	ochranářské
opatření	opatření	k1gNnPc1	opatření
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
porušení	porušení	k1gNnSc4	porušení
se	se	k3xPyFc4	se
vybírají	vybírat	k5eAaImIp3nP	vybírat
přísné	přísný	k2eAgFnPc4d1	přísná
pokuty	pokuta	k1gFnPc4	pokuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ani	ani	k8xC	ani
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
nevyhnul	vyhnout	k5eNaPmAgInS	vyhnout
vysoký	vysoký	k2eAgInSc1d1	vysoký
pokles	pokles	k1gInSc1	pokles
populací	populace	k1gFnPc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
MŽP	MŽP	kA	MŽP
zvláště	zvláště	k6eAd1	zvláště
chráněna	chráněn	k2eAgFnSc1d1	chráněna
jako	jako	k8xS	jako
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
druh	druh	k1gInSc1	druh
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
i	i	k9	i
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
biotop	biotop	k1gInSc4	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
je	být	k5eAaImIp3nS	být
zakázán	zakázán	k2eAgInSc1d1	zakázán
i	i	k8xC	i
její	její	k3xOp3gInSc1	její
chov	chov	k1gInSc1	chov
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
a	a	k8xC	a
prodej	prodej	k1gInSc1	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
příčina	příčina	k1gFnSc1	příčina
smrti	smrt	k1gFnSc2	smrt
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
nejčastěji	často	k6eAd3	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
zabití	zabití	k1gNnSc1	zabití
dopravními	dopravní	k2eAgInPc7d1	dopravní
prostředky	prostředek	k1gInPc7	prostředek
a	a	k8xC	a
pytláctví	pytláctví	k1gNnPc2	pytláctví
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
se	s	k7c7	s
zájmem	zájem	k1gInSc7	zájem
rybářů	rybář	k1gMnPc2	rybář
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
ale	ale	k9	ale
rybářům	rybář	k1gMnPc3	rybář
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
náhradu	náhrada	k1gFnSc4	náhrada
škod	škoda	k1gFnPc2	škoda
na	na	k7c6	na
rybách	ryba	k1gFnPc6	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Ochranou	ochrana	k1gFnSc7	ochrana
vydry	vydra	k1gFnSc2	vydra
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Český	český	k2eAgInSc1d1	český
nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
pro	pro	k7c4	pro
vydru	vydra	k1gFnSc4	vydra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
ji	on	k3xPp3gFnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
zastihnout	zastihnout	k5eAaPmF	zastihnout
kdekoliv	kdekoliv	k6eAd1	kdekoliv
na	na	k7c6	na
vhodných	vhodný	k2eAgInPc6d1	vhodný
tocích	tok	k1gInPc6	tok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejhojněji	hojně	k6eAd3	hojně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
Plzeňsku	Plzeňsko	k1gNnSc6	Plzeňsko
<g/>
,	,	kIx,	,
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
jihočeské	jihočeský	k2eAgFnSc6d1	Jihočeská
pánvi	pánev	k1gFnSc6	pánev
na	na	k7c6	na
Českomoravské	českomoravský	k2eAgFnSc6d1	Českomoravská
vrchovině	vrchovina	k1gFnSc6	vrchovina
<g/>
,	,	kIx,	,
vzácněji	vzácně	k6eAd2	vzácně
pak	pak	k6eAd1	pak
na	na	k7c6	na
Ohři	Ohře	k1gFnSc6	Ohře
<g/>
,	,	kIx,	,
v	v	k7c6	v
severozápadních	severozápadní	k2eAgFnPc6d1	severozápadní
Čechách	Čechy	k1gFnPc6	Čechy
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Dyji	Dyje	k1gFnSc6	Dyje
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
rovněž	rovněž	k9	rovněž
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
výskyt	výskyt	k1gInSc1	výskyt
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
Vltavy	Vltava	k1gFnSc2	Vltava
v	v	k7c6	v
Dole	dol	k1gInSc6	dol
u	u	k7c2	u
Libčic	Libčice	k1gFnPc2	Libčice
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Výskyt	výskyt	k1gInSc1	výskyt
Vydry	vydra	k1gFnSc2	vydra
byl	být	k5eAaImAgInS	být
též	též	k9	též
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Dřevnici	Dřevnice	k1gFnSc6	Dřevnice
a	a	k8xC	a
jejích	její	k3xOp3gInPc6	její
přítocích	přítok	k1gInPc6	přítok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Eurasian	Eurasian	k1gMnSc1	Eurasian
otter	otter	k1gMnSc1	otter
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Wydra	Wydra	k1gFnSc1	Wydra
europejska	europejsk	k1gInSc2	europejsk
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vydra	vydra	k1gFnSc1	vydra
říční	říční	k2eAgFnSc1d1	říční
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
vydra	vydra	k1gFnSc1	vydra
říční	říční	k2eAgFnSc1d1	říční
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
vydra	vydra	k1gFnSc1	vydra
říční	říční	k2eAgNnSc4d1	říční
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Biolib	Biolib	k1gMnSc1	Biolib
</s>
</p>
<p>
<s>
Vydra	vydra	k1gFnSc1	vydra
říční	říční	k2eAgFnSc1d1	říční
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
www.priroda.cz	www.priroda.cza	k1gFnPc2	www.priroda.cza
</s>
</p>
