<s>
François	François	k1gFnSc1	François
Rabelais	Rabelais	k1gFnSc1	Rabelais
[	[	kIx(	[
<g/>
rab	rab	k1gMnSc1	rab
<g/>
:	:	kIx,	:
<g/>
lé	lé	k?	lé
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
1494	[number]	k4	1494
nebo	nebo	k8xC	nebo
1483	[number]	k4	1483
Chinon	chinon	k1gInSc1	chinon
<g/>
,	,	kIx,	,
Touraine	Tourain	k1gInSc5	Tourain
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1553	[number]	k4	1553
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
botanik	botanik	k1gMnSc1	botanik
a	a	k8xC	a
stavitel	stavitel	k1gMnSc1	stavitel
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
slavného	slavný	k2eAgInSc2d1	slavný
románu	román	k1gInSc2	román
Gargantua	Gargantua	k1gMnSc1	Gargantua
a	a	k8xC	a
Pantagruel	Pantagruel	k1gMnSc1	Pantagruel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přání	přání	k1gNnSc4	přání
otce	otec	k1gMnSc2	otec
advokáta	advokát	k1gMnSc2	advokát
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
k	k	k7c3	k
františkánům	františkán	k1gMnPc3	františkán
poblíž	poblíž	k7c2	poblíž
Angers	Angersa	k1gFnPc2	Angersa
a	a	k8xC	a
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1511	[number]	k4	1511
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
knězem	kněz	k1gMnSc7	kněz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
myšlenkami	myšlenka	k1gFnPc7	myšlenka
italského	italský	k2eAgInSc2d1	italský
humanismu	humanismus	k1gInSc2	humanismus
<g/>
,	,	kIx,	,
naučil	naučit	k5eAaPmAgMnS	naučit
se	se	k3xPyFc4	se
řecky	řecky	k6eAd1	řecky
a	a	k8xC	a
překládal	překládat	k5eAaImAgMnS	překládat
Hérodota	Hérodot	k1gMnSc4	Hérodot
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
narazilo	narazit	k5eAaPmAgNnS	narazit
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
u	u	k7c2	u
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
Sorbonny	Sorbonna	k1gFnSc2	Sorbonna
i	i	k8xC	i
u	u	k7c2	u
jeho	jeho	k3xOp3gFnPc2	jeho
představených	představená	k1gFnPc2	představená
<g/>
.	.	kIx.	.
</s>
<s>
Protekcí	protekce	k1gFnSc7	protekce
biskupa	biskup	k1gMnSc2	biskup
z	z	k7c2	z
Poitiers	Poitiersa	k1gFnPc2	Poitiersa
<g/>
,	,	kIx,	,
benediktínského	benediktínský	k2eAgMnSc2d1	benediktínský
opata	opat	k1gMnSc2	opat
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
roku	rok	k1gInSc2	rok
1524	[number]	k4	1524
přestoupit	přestoupit	k5eAaPmF	přestoupit
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
kláštera	klášter	k1gInSc2	klášter
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
vystoupit	vystoupit	k5eAaPmF	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1528	[number]	k4	1528
<g/>
–	–	k?	–
<g/>
1530	[number]	k4	1530
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
snad	snad	k9	snad
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
medicinu	medicin	k2eAgFnSc4d1	medicina
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
zde	zde	k6eAd1	zde
dvě	dva	k4xCgFnPc1	dva
nemanželské	manželský	k2eNgFnPc1d1	nemanželská
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
v	v	k7c6	v
rostoucím	rostoucí	k2eAgNnSc6d1	rostoucí
napětí	napětí	k1gNnSc6	napětí
mezi	mezi	k7c7	mezi
katolíky	katolík	k1gMnPc7	katolík
a	a	k8xC	a
protestanty	protestant	k1gMnPc7	protestant
nemohl	moct	k5eNaImAgInS	moct
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
pro	pro	k7c4	pro
žádnou	žádný	k3yNgFnSc4	žádný
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
1530	[number]	k4	1530
propustit	propustit	k5eAaPmF	propustit
z	z	k7c2	z
kněžského	kněžský	k2eAgInSc2d1	kněžský
stavu	stav	k1gInSc2	stav
(	(	kIx(	(
<g/>
laicizovat	laicizovat	k5eAaBmF	laicizovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
zapsal	zapsat	k5eAaPmAgMnS	zapsat
se	se	k3xPyFc4	se
na	na	k7c4	na
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
fakultu	fakulta	k1gFnSc4	fakulta
v	v	k7c6	v
Montpellieru	Montpellier	k1gInSc6	Montpellier
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
vydával	vydávat	k5eAaImAgMnS	vydávat
spisy	spis	k1gInPc4	spis
Hippokratovy	Hippokratův	k2eAgInPc4d1	Hippokratův
a	a	k8xC	a
Galénovy	Galénův	k2eAgInPc4d1	Galénův
<g/>
,	,	kIx,	,
o	o	k7c4	o
něž	jenž	k3xRgFnPc4	jenž
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
opíralo	opírat	k5eAaImAgNnS	opírat
studium	studium	k1gNnSc1	studium
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
psaní	psaní	k1gNnSc4	psaní
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1532	[number]	k4	1532
vydal	vydat	k5eAaPmAgInS	vydat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
román	román	k1gInSc4	román
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
Hrozné	hrozný	k2eAgFnSc2d1	hrozná
a	a	k8xC	a
strašlivé	strašlivý	k2eAgInPc1d1	strašlivý
skutky	skutek	k1gInPc1	skutek
i	i	k8xC	i
hrdinství	hrdinství	k1gNnSc1	hrdinství
slavného	slavný	k2eAgMnSc2d1	slavný
Pantagruela	Pantagruel	k1gMnSc2	Pantagruel
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
Dipsodů	Dipsod	k1gInPc2	Dipsod
a	a	k8xC	a
syna	syn	k1gMnSc2	syn
obra	obr	k1gMnSc2	obr
Gargantuy	Gargantua	k1gMnSc2	Gargantua
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
-	-	kIx~	-
parodie	parodie	k1gFnSc1	parodie
na	na	k7c4	na
rytířské	rytířský	k2eAgInPc4d1	rytířský
romány	román	k1gInPc4	román
jeho	jeho	k3xOp3gFnSc2	jeho
doby	doba	k1gFnSc2	doba
–	–	k?	–
vyšel	vyjít	k5eAaPmAgInS	vyjít
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Rabelais	Rabelais	k1gInSc1	Rabelais
usadil	usadit	k5eAaPmAgInS	usadit
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
lékařem	lékař	k1gMnSc7	lékař
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1534	[number]	k4	1534
vydal	vydat	k5eAaPmAgMnS	vydat
knihu	kniha	k1gFnSc4	kniha
o	o	k7c6	o
topografii	topografie	k1gFnSc6	topografie
starého	starý	k2eAgInSc2d1	starý
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
-	-	kIx~	-
po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
úspěchu	úspěch	k1gInSc6	úspěch
Pantagruela	Pantagruela	k1gFnSc1	Pantagruela
–	–	k?	–
opět	opět	k6eAd1	opět
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
román	román	k1gInSc1	román
Strašlivý	strašlivý	k2eAgInSc4d1	strašlivý
život	život	k1gInSc4	život
velkého	velký	k2eAgMnSc2d1	velký
Gargantuy	Gargantua	k1gMnSc2	Gargantua
<g/>
,	,	kIx,	,
otce	otec	k1gMnSc2	otec
Pantagruelova	Pantagruelův	k2eAgMnSc2d1	Pantagruelův
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1535	[number]	k4	1535
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgInS	vzdát
lékařského	lékařský	k2eAgNnSc2d1	lékařské
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
sekretářem	sekretář	k1gInSc7	sekretář
vlivného	vlivný	k2eAgMnSc2d1	vlivný
diplomata	diplomat	k1gMnSc2	diplomat
bordeauxského	bordeauxský	k2eAgMnSc2d1	bordeauxský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
(	(	kIx(	(
<g/>
a	a	k8xC	a
biskupa	biskup	k1gInSc2	biskup
různých	různý	k2eAgNnPc2d1	různé
měst	město	k1gNnPc2	město
mimo	mimo	k7c4	mimo
jiné	jiná	k1gFnPc4	jiná
pařížského	pařížský	k2eAgMnSc2d1	pařížský
<g/>
)	)	kIx)	)
Jeana	Jean	k1gMnSc2	Jean
du	du	k?	du
Bellay	Bellaa	k1gMnSc2	Bellaa
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
opakovaně	opakovaně	k6eAd1	opakovaně
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
papežské	papežský	k2eAgNnSc4d1	papežské
povolení	povolení	k1gNnSc4	povolení
formálně	formálně	k6eAd1	formálně
znovu	znovu	k6eAd1	znovu
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
benediktinského	benediktinský	k2eAgInSc2d1	benediktinský
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
získat	získat	k5eAaPmF	získat
výnosné	výnosný	k2eAgNnSc4d1	výnosné
kanovnické	kanovnický	k2eAgNnSc4d1	kanovnický
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
provozovat	provozovat	k5eAaImF	provozovat
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
.	.	kIx.	.
1537	[number]	k4	1537
byl	být	k5eAaImAgInS	být
v	v	k7c4	v
Montpellier	Montpellier	k1gInSc4	Montpellier
promován	promovat	k5eAaBmNgMnS	promovat
na	na	k7c4	na
doktora	doktor	k1gMnSc4	doktor
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
přednášet	přednášet	k5eAaImF	přednášet
Hippokratovy	Hippokratův	k2eAgInPc4d1	Hippokratův
spisy	spis	k1gInPc4	spis
na	na	k7c6	na
základě	základ	k1gInSc6	základ
řeckého	řecký	k2eAgInSc2d1	řecký
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
arcibiskupa	arcibiskup	k1gMnSc4	arcibiskup
du	du	k?	du
Bellaye	Bellaye	k1gNnPc1	Bellaye
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
politických	politický	k2eAgFnPc6d1	politická
misích	mise	k1gFnPc6	mise
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Sorbonna	Sorbonna	k1gFnSc1	Sorbonna
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
jeho	jeho	k3xOp3gInPc4	jeho
romány	román	k1gInPc4	román
jako	jako	k8xC	jako
nemravné	mravný	k2eNgNnSc1d1	nemravné
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
1542	[number]	k4	1542
jejich	jejich	k3xOp3gFnSc2	jejich
upravené	upravený	k2eAgFnSc2d1	upravená
vydání	vydání	k1gNnSc3	vydání
a	a	k8xC	a
s	s	k7c7	s
královským	královský	k2eAgNnSc7d1	královské
privilegiem	privilegium	k1gNnSc7	privilegium
další	další	k2eAgInPc1d1	další
romány	román	k1gInPc1	román
s	s	k7c7	s
prostým	prostý	k2eAgInSc7d1	prostý
názvem	název	k1gInSc7	název
Třetí	třetí	k4xOgFnSc1	třetí
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
1545	[number]	k4	1545
<g/>
)	)	kIx)	)
a	a	k8xC	a
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
1552	[number]	k4	1552
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
tolerantního	tolerantní	k2eAgMnSc4d1	tolerantní
krále	král	k1gMnSc4	král
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
k	k	k7c3	k
útokům	útok	k1gInPc3	útok
Sorbonny	Sorbonna	k1gFnSc2	Sorbonna
přidal	přidat	k5eAaPmAgMnS	přidat
i	i	k9	i
reformátor	reformátor	k1gMnSc1	reformátor
Jean	Jean	k1gMnSc1	Jean
Calvin	Calvina	k1gFnPc2	Calvina
<g/>
,	,	kIx,	,
odjel	odjet	k5eAaPmAgMnS	odjet
Rabelais	Rabelais	k1gInSc4	Rabelais
do	do	k7c2	do
svobodného	svobodný	k2eAgNnSc2d1	svobodné
města	město	k1gNnSc2	město
Metz	Metza	k1gFnPc2	Metza
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
tajemník	tajemník	k1gMnSc1	tajemník
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
ochránci	ochránce	k1gMnPc1	ochránce
<g/>
,	,	kIx,	,
biskupovi	biskupův	k2eAgMnPc1d1	biskupův
du	du	k?	du
Bellay	Bellaa	k1gFnPc4	Bellaa
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dobře	dobře	k6eAd1	dobře
zaopatřen	zaopatřit	k5eAaPmNgInS	zaopatřit
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
poslední	poslední	k2eAgNnPc4d1	poslední
léta	léto	k1gNnPc4	léto
života	život	k1gInSc2	život
trávit	trávit	k5eAaImF	trávit
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
.	.	kIx.	.
</s>
<s>
Rabelaisovo	Rabelaisův	k2eAgNnSc4d1	Rabelaisovo
dílo	dílo	k1gNnSc4	dílo
zařadila	zařadit	k5eAaPmAgFnS	zařadit
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Zůstalo	zůstat	k5eAaPmAgNnS	zůstat
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
ho	on	k3xPp3gNnSc4	on
ještě	ještě	k9	ještě
Index	index	k1gInSc1	index
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
přestaly	přestat	k5eAaPmAgInP	přestat
být	být	k5eAaImF	být
v	v	k7c6	v
Indexu	index	k1gInSc6	index
uváděny	uváděn	k2eAgInPc4d1	uváděn
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgFnP	být
zavrženy	zavrhnout	k5eAaPmNgInP	zavrhnout
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1600	[number]	k4	1600
<g/>
;	;	kIx,	;
v	v	k7c6	v
Indexech	index	k1gInPc6	index
vydaných	vydaný	k2eAgInPc6d1	vydaný
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
Rabelaisovo	Rabelaisův	k2eAgNnSc1d1	Rabelaisovo
jméno	jméno	k1gNnSc1	jméno
již	již	k6eAd1	již
nenachází	nacházet	k5eNaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
kombinaci	kombinace	k1gFnSc3	kombinace
prostého	prostý	k2eAgMnSc2d1	prostý
až	až	k8xS	až
vulgárního	vulgární	k2eAgInSc2d1	vulgární
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
ironie	ironie	k1gFnSc2	ironie
<g/>
,	,	kIx,	,
sarkasmu	sarkasmus	k1gInSc2	sarkasmus
i	i	k8xC	i
erudovaných	erudovaný	k2eAgFnPc2d1	erudovaná
slovních	slovní	k2eAgFnPc2d1	slovní
hříček	hříčka	k1gFnPc2	hříčka
jsou	být	k5eAaImIp3nP	být
Rabelaisovy	Rabelaisův	k2eAgInPc1d1	Rabelaisův
romány	román	k1gInPc1	román
dodnes	dodnes	k6eAd1	dodnes
živé	živý	k2eAgInPc1d1	živý
a	a	k8xC	a
jméno	jméno	k1gNnSc1	jméno
Gargantua	Gargantuum	k1gNnSc2	Gargantuum
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
pojmem	pojem	k1gInSc7	pojem
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
Rabelaisově	Rabelaisův	k2eAgFnSc6d1	Rabelaisova
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1564	[number]	k4	1564
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ještě	ještě	k9	ještě
Pátá	pátý	k4xOgFnSc1	pátý
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
vládly	vládnout	k5eAaImAgInP	vládnout
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
pochybnosti	pochybnost	k1gFnSc2	pochybnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Rabelaisův	Rabelaisův	k2eAgInSc4d1	Rabelaisův
text	text	k1gInSc4	text
<g/>
;	;	kIx,	;
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
autentický	autentický	k2eAgInSc4d1	autentický
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
autorem	autor	k1gMnSc7	autor
nezredigovaný	zredigovaný	k2eNgInSc4d1	zredigovaný
rukopis	rukopis	k1gInSc4	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Pětidílný	pětidílný	k2eAgInSc4d1	pětidílný
cyklus	cyklus	k1gInSc4	cyklus
Gargantua	Gargantua	k1gMnSc1	Gargantua
a	a	k8xC	a
Pantagruel	Pantagruel	k1gMnSc1	Pantagruel
líčí	líčit	k5eAaImIp3nS	líčit
"	"	kIx"	"
<g/>
život	život	k1gInSc4	život
a	a	k8xC	a
skutky	skutek	k1gInPc4	skutek
<g/>
"	"	kIx"	"
královské	královský	k2eAgFnPc4d1	královská
rodiny	rodina	k1gFnPc4	rodina
obrů	obr	k1gMnPc2	obr
a	a	k8xC	a
Pantagruelovo	Pantagruelův	k2eAgNnSc1d1	Pantagruelův
putování	putování	k1gNnSc1	putování
po	po	k7c6	po
Francii	Francie	k1gFnSc6	Francie
i	i	k8xC	i
fiktivních	fiktivní	k2eAgFnPc6d1	fiktivní
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
díly	díl	k1gInPc1	díl
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgInP	řadit
chronologicky	chronologicky	k6eAd1	chronologicky
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
s	s	k7c7	s
nadsázkou	nadsázka	k1gFnSc7	nadsázka
psanou	psaný	k2eAgFnSc4d1	psaná
parodii	parodie	k1gFnSc4	parodie
na	na	k7c4	na
rytířské	rytířský	k2eAgInPc4d1	rytířský
romány	román	k1gInPc4	román
a	a	k8xC	a
satir	satira	k1gFnPc2	satira
na	na	k7c4	na
středověkou	středověký	k2eAgFnSc4d1	středověká
církev	církev	k1gFnSc4	církev
a	a	k8xC	a
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
vysmívá	vysmívat	k5eAaImIp3nS	vysmívat
soudům	soud	k1gInPc3	soud
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc3	školství
i	i	k8xC	i
klášternímu	klášterní	k2eAgInSc3d1	klášterní
životu	život	k1gInSc3	život
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
propaguje	propagovat	k5eAaImIp3nS	propagovat
užívání	užívání	k1gNnSc1	užívání
vlastního	vlastní	k2eAgInSc2d1	vlastní
rozumu	rozum	k1gInSc2	rozum
a	a	k8xC	a
logiky	logika	k1gFnSc2	logika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
najdeme	najít	k5eAaPmIp1nP	najít
vulgarismy	vulgarismus	k1gInPc1	vulgarismus
<g/>
,	,	kIx,	,
latinismy	latinismus	k1gInPc1	latinismus
<g/>
,	,	kIx,	,
archaismy	archaismus	k1gInPc1	archaismus
<g/>
,	,	kIx,	,
neologismy	neologismus	k1gInPc1	neologismus
a	a	k8xC	a
přísloví	přísloví	k1gNnSc1	přísloví
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
nevyhýbá	vyhýbat	k5eNaImIp3nS	vyhýbat
slovním	slovní	k2eAgFnPc3d1	slovní
hříčkám	hříčka	k1gFnPc3	hříčka
<g/>
,	,	kIx,	,
dlouhým	dlouhý	k2eAgFnPc3d1	dlouhá
charakteristikám	charakteristika	k1gFnPc3	charakteristika
a	a	k8xC	a
velkému	velký	k2eAgNnSc3d1	velké
množství	množství	k1gNnSc3	množství
přívlastků	přívlastek	k1gInPc2	přívlastek
<g/>
.	.	kIx.	.
</s>
<s>
Pantagruel	Pantagruel	k1gInSc1	Pantagruel
–	–	k?	–
Lyon	Lyon	k1gInSc1	Lyon
<g/>
,	,	kIx,	,
1532	[number]	k4	1532
<g/>
?	?	kIx.	?
</s>
<s>
Gargantua	Gargantua	k1gFnSc1	Gargantua
–	–	k?	–
Lyon	Lyon	k1gInSc1	Lyon
<g/>
,	,	kIx,	,
před	před	k7c7	před
1535	[number]	k4	1535
Pantagrueline	Pantagruelin	k1gInSc5	Pantagruelin
Prognostication	Prognostication	k1gInSc4	Prognostication
–	–	k?	–
Lyon	Lyon	k1gInSc1	Lyon
<g/>
,	,	kIx,	,
1542	[number]	k4	1542
Le	Le	k1gFnPc2	Le
Tiers	Tiersa	k1gFnPc2	Tiersa
livre	livr	k1gInSc5	livr
–	–	k?	–
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
1546	[number]	k4	1546
Le	Le	k1gMnPc2	Le
Quart	quart	k1gInSc4	quart
livre	livr	k1gInSc5	livr
–	–	k?	–
Lyon	Lyon	k1gInSc1	Lyon
<g/>
,	,	kIx,	,
1548	[number]	k4	1548
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
isle	islus	k1gMnSc5	islus
sonnante	sonnant	k1gMnSc5	sonnant
ou	ou	k0	ou
le	le	k?	le
Cinquiè	Cinquiè	k1gMnSc1	Cinquiè
livre	livr	k1gInSc5	livr
–	–	k?	–
1562	[number]	k4	1562
Traité	Traitý	k2eAgNnSc4d1	Traité
de	de	k?	de
bon	bon	k1gInSc4	bon
usage	usagat	k5eAaPmIp3nS	usagat
de	de	k?	de
vin	vina	k1gFnPc2	vina
–	–	k?	–
Lyon	Lyon	k1gInSc1	Lyon
<g/>
,	,	kIx,	,
1564	[number]	k4	1564
České	český	k2eAgInPc1d1	český
překlady	překlad	k1gInPc1	překlad
<g/>
:	:	kIx,	:
Stati	stať	k1gFnPc1	stať
výchovné	výchovný	k2eAgFnPc1d1	výchovná
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
Hrůzyplný	hrůzyplný	k2eAgInSc4d1	hrůzyplný
život	život	k1gInSc4	život
velikého	veliký	k2eAgMnSc2d1	veliký
Gargantuy	Gargantua	k1gMnSc2	Gargantua
<g/>
,	,	kIx,	,
otce	otec	k1gMnSc2	otec
Pantagruelova	Pantagruelův	k2eAgFnSc1d1	Pantagruelův
<g/>
,	,	kIx,	,
složený	složený	k2eAgInSc1d1	složený
kdysi	kdysi	k6eAd1	kdysi
panem	pan	k1gMnSc7	pan
Alcofribasem	Alcofribas	k1gMnSc7	Alcofribas
<g/>
,	,	kIx,	,
filosofem	filosof	k1gMnSc7	filosof
kvintesence	kvintesence	k1gFnSc2	kvintesence
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Gargantua	Gargantua	k6eAd1	Gargantua
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
Theléma	Theléma	k1gFnSc1	Theléma
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Gargantua	Gargantua	k6eAd1	Gargantua
<g/>
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Starobylá	starobylý	k2eAgNnPc1d1	starobylé
historia	historium	k1gNnPc1	historium
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kterak	kterak	k8xS	kterak
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Pikrochol	Pikrochol	k1gInSc4	Pikrochol
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
dobýti	dobýt	k5eAaPmF	dobýt
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Hrůzostrašná	hrůzostrašný	k2eAgFnSc1d1	hrůzostrašná
historie	historie	k1gFnSc1	historie
velkého	velký	k2eAgMnSc2d1	velký
Gargantuy	Gargantua	k1gMnSc2	Gargantua
<g/>
,	,	kIx,	,
otce	otec	k1gMnSc2	otec
Pantagruelova	Pantagruelův	k2eAgMnSc2d1	Pantagruelův
<g/>
.	.	kIx.	.
kdysi	kdysi	k6eAd1	kdysi
sepsaná	sepsaný	k2eAgFnSc1d1	sepsaná
panem	pan	k1gMnSc7	pan
Alkofribasem	Alkofribas	k1gMnSc7	Alkofribas
<g/>
,	,	kIx,	,
mistrem	mistr	k1gMnSc7	mistr
quintesence	quintesence	k1gFnSc2	quintesence
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
Gargantuův	Gargantuův	k2eAgInSc1d1	Gargantuův
a	a	k8xC	a
Pantagruelův	Pantagruelův	k2eAgInSc1d1	Pantagruelův
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Prokop	Prokop	k1gMnSc1	Prokop
Haškovec	Haškovec	k1gMnSc1	Haškovec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
;	;	kIx,	;
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
kapitol	kapitola	k1gFnPc2	kapitola
ze	z	k7c2	z
života	život	k1gInSc2	život
Gargantuova	Gargantuův	k2eAgInSc2d1	Gargantuův
a	a	k8xC	a
Pantagruelova	Pantagruelův	k2eAgInSc2d1	Pantagruelův
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Gargantua	Gargantua	k1gMnSc1	Gargantua
a	a	k8xC	a
Pantagruel	Pantagruel	k1gMnSc1	Pantagruel
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
Theléma	Theléma	k1gFnSc1	Theléma
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
;	;	kIx,	;
1962	[number]	k4	1962
<g/>
;	;	kIx,	;
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Pantagruelská	Pantagruelský	k2eAgFnSc1d1	Pantagruelský
pranostyka	pranostyka	k1gFnSc1	pranostyka
<g/>
,	,	kIx,	,
nesporná	sporný	k2eNgFnSc1d1	nesporná
&	&	k?	&
neklamná	klamný	k2eNgFnSc1d1	neklamná
&	&	k?	&
neomylná	omylný	k2eNgFnSc1d1	neomylná
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
perpetuální	perpetuální	k2eAgInSc4d1	perpetuální
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
sepsaná	sepsaný	k2eAgFnSc1d1	sepsaná
ku	k	k7c3	k
prospěchu	prospěch	k1gInSc3	prospěch
&	&	k?	&
poučení	poučení	k1gNnPc1	poučení
slovutných	slovutný	k2eAgMnPc2d1	slovutný
zevlounů	zevloun	k1gMnPc2	zevloun
mistrem	mistr	k1gMnSc7	mistr
Alcofribasem	Alcofribas	k1gMnSc7	Alcofribas
<g/>
,	,	kIx,	,
vrchním	vrchní	k2eAgMnSc7d1	vrchní
číšníkem	číšník	k1gMnSc7	číšník
řečeného	řečený	k2eAgMnSc2d1	řečený
Pantagruela	Pantagruel	k1gMnSc2	Pantagruel
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Patrik	Patrik	k1gMnSc1	Patrik
Ouředník	Ouředník	k1gMnSc1	Ouředník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
UKÁZKA	ukázka	k1gFnSc1	ukázka
Pojednání	pojednání	k1gNnSc2	pojednání
o	o	k7c6	o
případném	případný	k2eAgNnSc6d1	případné
pití	pití	k1gNnSc6	pití
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
velikém	veliký	k2eAgInSc6d1	veliký
&	&	k?	&
ustavičném	ustavičný	k2eAgInSc6d1	ustavičný
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
potěchu	potěcha	k1gFnSc4	potěcha
ducha	duch	k1gMnSc2	duch
&	&	k?	&
těla	tělo	k1gNnSc2	tělo
&	&	k?	&
proti	proti	k7c3	proti
všelikým	všeliký	k3yIgFnPc3	všeliký
chorobám	choroba	k1gFnPc3	choroba
oudů	oud	k1gInPc2	oud
zevnitřních	zevnitřní	k2eAgInPc2d1	zevnitřní
i	i	k8xC	i
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
<g/>
,	,	kIx,	,
sepsané	sepsaný	k2eAgFnSc2d1	sepsaná
ku	k	k7c3	k
poučení	poučení	k1gNnSc3	poučení
&	&	k?	&
užitku	užitek	k1gInSc2	užitek
brachů	brach	k1gMnPc2	brach
mokrého	mokrý	k2eAgInSc2d1	mokrý
cechu	cech	k1gInSc2	cech
mistrem	mistr	k1gMnSc7	mistr
Alcofribasem	Alcofribas	k1gMnSc7	Alcofribas
<g/>
,	,	kIx,	,
vrchním	vrchní	k2eAgMnSc7d1	vrchní
číšníkem	číšník	k1gMnSc7	číšník
velikého	veliký	k2eAgMnSc2d1	veliký
Pantagruela	Pantagruel	k1gMnSc2	Pantagruel
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Patrik	Patrik	k1gMnSc1	Patrik
Ouředník	Ouředník	k1gMnSc1	Ouředník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
;	;	kIx,	;
2002	[number]	k4	2002
<g/>
;	;	kIx,	;
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
<g/>
UKÁZKA	ukázka	k1gFnSc1	ukázka
Do	do	k7c2	do
zlovolné	zlovolný	k2eAgFnSc2d1	zlovolná
duše	duše	k1gFnSc2	duše
se	se	k3xPyFc4	se
moudrost	moudrost	k1gFnSc1	moudrost
nedostane	dostat	k5eNaPmIp3nS	dostat
a	a	k8xC	a
věda	věda	k1gFnSc1	věda
bez	bez	k7c2	bez
svědomí	svědomí	k1gNnSc2	svědomí
duši	duše	k1gFnSc3	duše
jen	jen	k6eAd1	jen
ničí	ničit	k5eAaImIp3nS	ničit
<g/>
.	.	kIx.	.
</s>
<s>
Všem	všecek	k3xTgMnPc3	všecek
těm	ten	k3xDgMnPc3	ten
pomlouvačům	pomlouvač	k1gMnPc3	pomlouvač
radím	radit	k5eAaImIp1nS	radit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
šli	jít	k5eAaImAgMnP	jít
oběsit	oběsit	k5eAaPmF	oběsit
ještě	ještě	k6eAd1	ještě
než	než	k8xS	než
skončí	skončit	k5eAaPmIp3nS	skončit
tento	tento	k3xDgInSc4	tento
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Provaz	provaz	k1gInSc4	provaz
jim	on	k3xPp3gFnPc3	on
poskytnu	poskytnout	k5eAaPmIp1nS	poskytnout
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
neřídím	řídit	k5eNaImIp1nS	řídit
podle	podle	k7c2	podle
hodin	hodina	k1gFnPc2	hodina
<g/>
;	;	kIx,	;
hodiny	hodina	k1gFnPc1	hodina
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
ne	ne	k9	ne
člověk	člověk	k1gMnSc1	člověk
pro	pro	k7c4	pro
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Břicho	břicho	k1gNnSc1	břicho
nemá	mít	k5eNaImIp3nS	mít
uši	ucho	k1gNnPc4	ucho
a	a	k8xC	a
nedá	dát	k5eNaPmIp3nS	dát
se	se	k3xPyFc4	se
naplnit	naplnit	k5eAaPmF	naplnit
pěknými	pěkný	k2eAgNnPc7d1	pěkné
slovy	slovo	k1gNnPc7	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
cenného	cenný	k2eAgNnSc2d1	cenné
nemám	mít	k5eNaImIp1nS	mít
<g/>
,	,	kIx,	,
jen	jen	k9	jen
plno	plno	k6eAd1	plno
dluhů	dluh	k1gInPc2	dluh
<g/>
,	,	kIx,	,
a	a	k8xC	a
co	co	k3yInSc1	co
zbyde	zbýt	k5eAaPmIp3nS	zbýt
<g/>
,	,	kIx,	,
nechávám	nechávat	k5eAaImIp1nS	nechávat
pro	pro	k7c4	pro
chudé	chudý	k2eAgMnPc4d1	chudý
<g/>
.	.	kIx.	.
</s>
<s>
Běžte	běžet	k5eAaImRp2nP	běžet
za	za	k7c7	za
psem	pes	k1gMnSc7	pes
a	a	k8xC	a
nepokouše	pokousat	k5eNaPmIp3nS	pokousat
vás	vy	k3xPp2nPc4	vy
<g/>
,	,	kIx,	,
pijte	pít	k5eAaImRp2nP	pít
před	před	k7c7	před
žízní	žízeň	k1gFnSc7	žízeň
a	a	k8xC	a
nedostanete	dostat	k5eNaPmIp2nP	dostat
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
fraška	fraška	k1gFnSc1	fraška
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
jej	on	k3xPp3gMnSc4	on
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
nuda	nuda	k1gFnSc1	nuda
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
nemá	mít	k5eNaImIp3nS	mít
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
hola	hola	k1gFnSc1	hola
<g/>
!	!	kIx.	!
</s>
<s>
Vězte	vědět	k5eAaImRp2nP	vědět
:	:	kIx,	:
na	na	k7c4	na
zábavu	zábava	k1gFnSc4	zábava
máte	mít	k5eAaImIp2nP	mít
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
smrt	smrt	k1gFnSc4	smrt
na	na	k7c4	na
odpočívání	odpočívání	k1gNnSc4	odpočívání
<g/>
.	.	kIx.	.
</s>
