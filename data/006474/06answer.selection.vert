<s>
François	François	k1gFnSc1	François
Rabelais	Rabelais	k1gFnSc1	Rabelais
[	[	kIx(	[
<g/>
rab	rab	k1gMnSc1	rab
<g/>
:	:	kIx,	:
<g/>
lé	lé	k?	lé
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
1494	[number]	k4	1494
nebo	nebo	k8xC	nebo
1483	[number]	k4	1483
Chinon	chinon	k1gInSc1	chinon
<g/>
,	,	kIx,	,
Touraine	Tourain	k1gInSc5	Tourain
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1553	[number]	k4	1553
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
botanik	botanik	k1gMnSc1	botanik
a	a	k8xC	a
stavitel	stavitel	k1gMnSc1	stavitel
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
slavného	slavný	k2eAgInSc2d1	slavný
románu	román	k1gInSc2	román
Gargantua	Gargantua	k1gMnSc1	Gargantua
a	a	k8xC	a
Pantagruel	Pantagruel	k1gMnSc1	Pantagruel
<g/>
.	.	kIx.	.
</s>
