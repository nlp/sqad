<p>
<s>
Zahradníkův	Zahradníkův	k2eAgInSc4d1	Zahradníkův
rok	rok	k1gInSc4	rok
je	být	k5eAaImIp3nS	být
sbírka	sbírka	k1gFnSc1	sbírka
fejetonů	fejeton	k1gInPc2	fejeton
o	o	k7c4	o
zahradničení	zahradničený	k2eAgMnPc1d1	zahradničený
a	a	k8xC	a
zahrádkářství	zahrádkářství	k1gNnSc3	zahrádkářství
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
fejetony	fejeton	k1gInPc4	fejeton
o	o	k7c6	o
slastech	slast	k1gFnPc6	slast
a	a	k8xC	a
strastech	strast	k1gFnPc6	strast
zahrádkářů	zahrádkář	k1gMnPc2	zahrádkář
během	během	k7c2	během
čtyř	čtyři	k4xCgFnPc2	čtyři
ročních	roční	k2eAgFnPc2d1	roční
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
za	za	k7c2	za
každého	každý	k3xTgNnSc2	každý
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
líčí	líčit	k5eAaImIp3nS	líčit
s	s	k7c7	s
humorem	humor	k1gInSc7	humor
a	a	k8xC	a
osobitým	osobitý	k2eAgInSc7d1	osobitý
nadhledem	nadhled	k1gInSc7	nadhled
vlastní	vlastní	k2eAgFnPc4d1	vlastní
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
,	,	kIx,	,
pocity	pocit	k1gInPc4	pocit
a	a	k8xC	a
dojmy	dojem	k1gInPc4	dojem
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc4	dílo
Zahradníkův	Zahradníkův	k2eAgInSc1d1	Zahradníkův
rok	rok	k1gInSc1	rok
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
na	na	k7c6	na
webu	web	k1gInSc6	web
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
www.capek.misto.cz	www.capek.misto.cz	k1gMnSc1	www.capek.misto.cz
</s>
</p>
