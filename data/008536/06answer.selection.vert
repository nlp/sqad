<s>
Československo	Československo	k1gNnSc1	Československo
byl	být	k5eAaImAgInS	být
stát	stát	k5eAaPmF	stát
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
existoval	existovat	k5eAaImAgMnS	existovat
(	(	kIx(	(
<g/>
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
přestávkou	přestávka	k1gFnSc7	přestávka
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
