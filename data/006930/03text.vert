<s>
HC	HC	kA	HC
La	la	k1gNnSc1	la
Chaux-de-Fonds	Chauxe-Fonds	k1gInSc1	Chaux-de-Fonds
je	být	k5eAaImIp3nS	být
profesionální	profesionální	k2eAgInSc1d1	profesionální
švýcarský	švýcarský	k2eAgInSc1d1	švýcarský
hokejový	hokejový	k2eAgInSc1d1	hokejový
tým	tým	k1gInSc1	tým
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Mistr	mistr	k1gMnSc1	mistr
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
:	:	kIx,	:
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
Vítěz	vítěz	k1gMnSc1	vítěz
National	National	k1gMnSc1	National
League	League	k1gFnSc3	League
B	B	kA	B
<g/>
:	:	kIx,	:
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
HC	HC	kA	HC
La	la	k1gNnSc3	la
Chaux-de-Fonds	Chauxe-Fondsa	k1gFnPc2	Chaux-de-Fondsa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
