<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
–	–	k?	–
žlutý	žlutý	k2eAgInSc1d1	žlutý
<g/>
,	,	kIx,	,
modrý	modrý	k2eAgInSc1d1	modrý
a	a	k8xC	a
červený	červený	k2eAgMnSc1d1	červený
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
žlutá	žlutat	k5eAaImIp3nS	žlutat
zabírá	zabírat	k5eAaImIp3nS	zabírat
celou	celý	k2eAgFnSc4d1	celá
horní	horní	k2eAgFnSc4d1	horní
polovinu	polovina	k1gFnSc4	polovina
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odvozená	odvozený	k2eAgFnSc1d1	odvozená
z	z	k7c2	z
vlajky	vlajka	k1gFnSc2	vlajka
Francisca	Franciscus	k1gMnSc2	Franciscus
de	de	k?	de
Mirandy	Miranda	k1gFnSc2	Miranda
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
<g/>
)	)	kIx)	)
a	a	k8xC	a
Simóna	Simón	k1gMnSc2	Simón
Bolívara	Bolívar	k1gMnSc2	Bolívar
a	a	k8xC	a
z	z	k7c2	z
barev	barva	k1gFnPc2	barva
konfederace	konfederace	k1gFnSc2	konfederace
Velké	velký	k2eAgFnSc2d1	velká
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1819-1830	[number]	k4	1819-1830
sdružovala	sdružovat	k5eAaImAgFnS	sdružovat
Novou	nový	k2eAgFnSc4d1	nová
Granadu	Granada	k1gFnSc4	Granada
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc4d1	dnešní
Kolumbii	Kolumbie	k1gFnSc4	Kolumbie
s	s	k7c7	s
Panamou	Panama	k1gFnSc7	Panama
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Venezuelu	Venezuela	k1gFnSc4	Venezuela
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	on	k3xPp3gMnPc4	on
Bolívar	Bolívar	k1gInSc1	Bolívar
osvobodil	osvobodit	k5eAaPmAgInS	osvobodit
od	od	k7c2	od
španělské	španělský	k2eAgFnSc2d1	španělská
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Shodně	shodně	k6eAd1	shodně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
vykládaná	vykládaný	k2eAgFnSc1d1	vykládaná
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
zlatá	zlatý	k2eAgFnSc1d1	zlatá
Amerika	Amerika	k1gFnSc1	Amerika
je	být	k5eAaImIp3nS	být
oddělená	oddělený	k2eAgFnSc1d1	oddělená
modrou	modrý	k2eAgFnSc7d1	modrá
vodou	voda	k1gFnSc7	voda
oceánu	oceán	k1gInSc2	oceán
od	od	k7c2	od
krvavého	krvavý	k2eAgNnSc2d1	krvavé
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgNnPc1d1	červené
a	a	k8xC	a
žlutá	žlutý	k2eAgNnPc1d1	žluté
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
španělské	španělský	k2eAgFnPc1d1	španělská
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
se	se	k3xPyFc4	se
žlutá	žlutat	k5eAaImIp3nS	žlutat
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
lidem	lid	k1gInSc7	lid
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
s	s	k7c7	s
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
dělí	dělit	k5eAaImIp3nS	dělit
od	od	k7c2	od
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
prolitou	prolitý	k2eAgFnSc7d1	prolitá
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dnešní	dnešní	k2eAgFnSc2d1	dnešní
interpretace	interpretace	k1gFnSc2	interpretace
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
státní	státní	k2eAgFnSc4d1	státní
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
statečnost	statečnost	k1gFnSc1	statečnost
<g/>
,	,	kIx,	,
ušlechtilost	ušlechtilost	k1gFnSc1	ušlechtilost
a	a	k8xC	a
věrnost	věrnost	k1gFnSc1	věrnost
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
krev	krev	k1gFnSc1	krev
vlastenců	vlastenec	k1gMnPc2	vlastenec
prolitou	prolitý	k2eAgFnSc4d1	prolitá
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgFnP	použít
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
pořadí	pořadí	k1gNnSc6	pořadí
také	také	k9	také
na	na	k7c6	na
ekvádorské	ekvádorský	k2eAgFnSc6d1	ekvádorská
a	a	k8xC	a
venezuelské	venezuelský	k2eAgFnSc6d1	venezuelská
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
(	(	kIx(	(
<g/>
až	až	k8xS	až
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
vytvoření	vytvoření	k1gNnSc6	vytvoření
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1834-1861	[number]	k4	1834-1861
měla	mít	k5eAaImAgFnS	mít
však	však	k9	však
Nová	nový	k2eAgFnSc1d1	nová
Granada	Granada	k1gFnSc1	Granada
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
opět	opět	k6eAd1	opět
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
<g/>
,	,	kIx,	,
vlajku	vlajka	k1gFnSc4	vlajka
s	s	k7c7	s
vertikálními	vertikální	k2eAgInPc7d1	vertikální
pruhy	pruh	k1gInPc7	pruh
-	-	kIx~	-
červeným	červený	k2eAgInSc7d1	červený
<g/>
,	,	kIx,	,
modrým	modrý	k2eAgInSc7d1	modrý
a	a	k8xC	a
žlutým	žlutý	k2eAgInSc7d1	žlutý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
kolumbijských	kolumbijský	k2eAgInPc2d1	kolumbijský
departementů	departement	k1gInPc2	departement
==	==	k?	==
</s>
</p>
<p>
<s>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgInSc4d1	unitární
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
distriktu	distrikt	k1gInSc2	distrikt
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Bogotá	Bogotá	k1gFnPc2	Bogotá
a	a	k8xC	a
32	[number]	k4	32
departementů	departement	k1gInPc2	departement
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
</s>
</p>
<p>
<s>
Kolumbijská	kolumbijský	k2eAgFnSc1d1	kolumbijská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kolumbijská	kolumbijský	k2eAgFnSc1d1	kolumbijská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
