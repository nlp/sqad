<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
–	–	k?	–
žlutý	žlutý	k2eAgInSc1d1	žlutý
<g/>
,	,	kIx,	,
modrý	modrý	k2eAgInSc1d1	modrý
a	a	k8xC	a
červený	červený	k2eAgMnSc1d1	červený
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
žlutá	žlutat	k5eAaImIp3nS	žlutat
zabírá	zabírat	k5eAaImIp3nS	zabírat
celou	celý	k2eAgFnSc4d1	celá
horní	horní	k2eAgFnSc4d1	horní
polovinu	polovina	k1gFnSc4	polovina
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
