<s>
MTV	MTV	kA
</s>
<s>
MTV	MTV	kA
Zahájení	zahájení	k1gNnSc1
vysílání	vysílání	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1981	#num#	k4
Provozovatel	provozovatel	k1gMnSc1
</s>
<s>
MTV	MTV	kA
Networks	Networks	k1gInSc1
(	(	kIx(
<g/>
ViacomCBS	ViacomCBS	k1gFnSc1
<g/>
)	)	kIx)
Země	země	k1gFnSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
New	New	k?
York	York	k1gInSc1
City	City	k1gFnSc2
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Jazyk	jazyk	k1gInSc4
</s>
<s>
angličtina	angličtina	k1gFnSc1
Oblast	oblast	k1gFnSc1
vysílání	vysílání	k1gNnSc4
</s>
<s>
celý	celý	k2eAgInSc1d1
svět	svět	k1gInSc1
Vysílací	vysílací	k2eAgInSc4d1
čas	čas	k1gInSc4
</s>
<s>
24	#num#	k4
hodin	hodina	k1gFnPc2
Sesterské	sesterský	k2eAgInPc1d1
kanály	kanál	k1gInPc1
</s>
<s>
MTV	MTV	kA
live	livat	k5eAaPmIp3nS
HD	HD	kA
Web	web	k1gInSc1
</s>
<s>
mtv	mtv	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
Dostupnost	dostupnost	k1gFnSc1
Satelitní	satelitní	k2eAgFnSc1d1
Skylink	Skylink	k1gInSc4
</s>
<s>
Kanál	kanál	k1gInSc1
182	#num#	k4
</s>
<s>
MTV	MTV	kA
v	v	k7c6
balíčcích	balíček	k1gInPc6
Smart	Smarta	k1gFnPc2
<g/>
,	,	kIx,
Flexi	flexe	k1gFnSc4
10	#num#	k4
<g/>
,	,	kIx,
Multi	Multi	k1gNnSc1
<g/>
,	,	kIx,
Kombi	kombi	k1gNnSc1
<g/>
,	,	kIx,
Komplet	komplet	k1gInSc1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
IPTV	IPTV	kA
T-Mobile	T-Mobila	k1gFnSc6
TV	TV	kA
</s>
<s>
Kanál	kanál	k1gInSc1
193	#num#	k4
(	(	kIx(
<g/>
MTV	MTV	kA
Europe	Europ	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
Americká	americký	k2eAgFnSc1d1
MTV	MTV	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
z	z	k7c2
Music	Musice	k1gFnPc2
Television	Television	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
první	první	k4xOgFnSc1
výhradně	výhradně	k6eAd1
hudební	hudební	k2eAgFnSc1d1
televizní	televizní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založena	založen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1981	#num#	k4
společnostmi	společnost	k1gFnPc7
Warner	Warnra	k1gFnPc2
a	a	k8xC
AmEx	AmEx	k1gInSc1
a	a	k8xC
své	svůj	k3xOyFgNnSc4
vysílání	vysílání	k1gNnSc4
zahájila	zahájit	k5eAaPmAgFnS
příznačně	příznačně	k6eAd1
písní	píseň	k1gFnPc2
Video	video	k1gNnSc1
Killed	Killed	k1gMnSc1
the	the	k?
Radio	radio	k1gNnSc4
Star	star	k1gFnSc2
(	(	kIx(
<g/>
Video	video	k1gNnSc1
zabilo	zabít	k5eAaPmAgNnS
hvězdu	hvězda	k1gFnSc4
rádií	rádio	k1gNnPc2
<g/>
)	)	kIx)
od	od	k7c2
skupiny	skupina	k1gFnSc2
Buggles	Bugglesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílání	vysílání	k1gNnSc1
bylo	být	k5eAaImAgNnS
ihned	ihned	k6eAd1
od	od	k7c2
počátku	počátek	k1gInSc2
24	#num#	k4
hodiny	hodina	k1gFnSc2
denně	denně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
vysílá	vysílat	k5eAaImIp3nS
stanice	stanice	k1gFnSc1
v	v	k7c6
mnoha	mnoho	k4c6
jazykových	jazykový	k2eAgFnPc6d1
mutacích	mutace	k1gFnPc6
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
vysílala	vysílat	k5eAaImAgFnS
i	i	k9
česká	český	k2eAgFnSc1d1
verze	verze	k1gFnSc1
MTV	MTV	kA
Czech	Czech	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Aktuální	aktuální	k2eAgInSc1d1
program	program	k1gInSc1
</s>
<s>
Neskriptové	Neskriptový	k2eAgInPc1d1
seriály	seriál	k1gInPc1
</s>
<s>
Hudební	hudební	k2eAgInPc1d1
seriály	seriál	k1gInPc1
</s>
<s>
MTV	MTV	kA
Unplugged	Unplugged	k1gMnSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
MTV	MTV	kA
First	First	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Total	totat	k5eAaImAgMnS
Request	Request	k1gMnSc1
Live	Liv	k1gFnSc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Reality-show	Reality-show	k?
</s>
<s>
The	The	k?
Real	Real	k1gInSc1
World	World	k1gInSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Napsáno	napsán	k2eAgNnSc1d1
životem	život	k1gInSc7
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
16	#num#	k4
a	a	k8xC
těhotná	těhotný	k2eAgFnSc1d1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Teen	Teen	k1gMnSc1
Mom	Mom	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Teen	Teen	k1gInSc1
Mom	Mom	k1gFnSc2
2	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Catfish	Catfish	k1gInSc1
<g/>
:	:	kIx,
Láska	láska	k1gFnSc1
online	onlinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
MTV	MTV	kA
Suspect	Suspect	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Promposal	Promposat	k5eAaPmAgMnS,k5eAaImAgMnS
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Siesta	siesta	k1gFnSc1
Key	Key	k1gFnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Soutěže	soutěž	k1gFnPc1
</s>
<s>
The	The	k?
Challange	Challange	k1gInSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Ideální	ideální	k2eAgInSc1d1
pár	pár	k4xCyI
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Ideální	ideální	k2eAgInSc1d1
pár	pár	k1gInSc1
<g/>
:	:	kIx,
druhá	druhý	k4xOgFnSc1
šance	šance	k1gFnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Faktor	faktor	k1gInSc1
strachu	strach	k1gInSc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Stranded	Stranded	k1gMnSc1
with	with	k1gMnSc1
a	a	k8xC
Million	Million	k1gInSc1
Dollars	Dollarsa	k1gFnPc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
90	#num#	k4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
House	house	k1gNnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Komedie	komedie	k1gFnSc1
</s>
<s>
Kruťárny	Kruťárna	k1gFnPc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Nick	Nick	k6eAd1
Cannon	Cannon	k1gInSc1
Presents	Presents	k1gInSc1
<g/>
:	:	kIx,
Wild	Wild	k1gInSc1
'	'	kIx"
<g/>
n	n	k0
Out	Out	k1gMnSc6
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Safeworld	Safeworld	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Skriptové	Skriptový	k2eAgInPc1d1
seriály	seriál	k1gInPc1
</s>
<s>
Drama	drama	k1gNnSc1
</s>
<s>
Vlčí	vlčí	k2eAgNnSc1d1
mládě	mládě	k1gNnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Scream	Scream	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Minulé	minulý	k2eAgInPc1d1
programy	program	k1gInPc1
</s>
<s>
My	my	k3xPp1nPc1
Wife	Wife	k1gNnPc7
&	&	k?
Kids	Kidsa	k1gFnPc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
George	Georg	k1gMnSc4
Lopez	Lopez	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Fresh	Fresh	k1gMnSc1
Prince	princ	k1gMnSc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Waynehead	Waynehead	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
AMTV	AMTV	kA
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
)	)	kIx)
</s>
<s>
Hangin	Hangin	k1gMnSc1
<g/>
'	'	kIx"
with	with	k1gMnSc1
Mr	Mr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cooper	Cooper	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
C	C	kA
Bear	Bear	k1gMnSc1
and	and	k?
Jamal	Jamal	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eye	Eye	k?
Candy	Canda	k1gFnPc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Happyland	Happyland	k1gInSc1
Happyland	Happyland	k1gInSc1
<g/>
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
AKA	AKA	kA
Cartoon	Cartoon	k1gInSc1
Network	network	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nešika	nešika	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Předstírání	předstírání	k1gNnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tvorba	tvorba	k1gFnSc1
</s>
<s>
Koncem	koncem	k7c2
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
MTV	MTV	kA
začíná	začínat	k5eAaImIp3nS
tvořit	tvořit	k5eAaImF
vlastní	vlastní	k2eAgInPc4d1
programy	program	k1gInPc4
jako	jako	k8xS,k8xC
např.	např.	kA
Club	club	k1gInSc1
MTV	MTV	kA
<g/>
,	,	kIx,
Week	Week	k1gMnSc1
In	In	k1gFnSc2
Rock	rock	k1gInSc1
nebo	nebo	k8xC
YO	YO	kA
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
MTV	MTV	kA
Raps	Rapsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílání	vysílání	k1gNnSc1
pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
je	být	k5eAaImIp3nS
realizováno	realizován	k2eAgNnSc1d1
z	z	k7c2
londýnského	londýnský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
v	v	k7c6
historii	historie	k1gFnSc6
MTV	MTV	kA
zůstává	zůstávat	k5eAaImIp3nS
kultovním	kultovní	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
devadesátých	devadesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
MTV	MTV	kA
rozšiřuje	rozšiřovat	k5eAaImIp3nS
do	do	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Věhlas	věhlas	k1gInSc1
si	se	k3xPyFc3
získala	získat	k5eAaPmAgFnS
pořady	pořad	k1gInPc4
jako	jako	k8xC,k8xS
MTV	MTV	kA
Unplugged	Unplugged	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známé	známá	k1gFnPc1
jsou	být	k5eAaImIp3nP
také	také	k9
hudební	hudební	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
MTV	MTV	kA
Music	Musice	k1gFnPc2
Awards	Awardsa	k1gFnPc2
&	&	k?
MTV	MTV	kA
Europe	Europ	k1gInSc5
Music	Musice	k1gInPc2
Awards	Awardsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
ve	v	k7c6
světě	svět	k1gInSc6
</s>
<s>
MTV	MTV	kA
Adria	Adria	k1gFnSc1
</s>
<s>
MTV	MTV	kA
Arabia	Arabia	k1gFnSc1
</s>
<s>
MTV	MTV	kA
Asia	Asia	k1gFnSc1
</s>
<s>
MTV	MTV	kA
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
MTV	MTV	kA
Base	basa	k1gFnSc6
(	(	kIx(
<g/>
Afrika	Afrika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
MTV	MTV	kA
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
MTV	MTV	kA
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
MTV	MTV	kA
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Europe	Europ	k1gMnSc5
</s>
<s>
MTV	MTV	kA
Filipíny	Filipíny	k1gFnPc1
</s>
<s>
MTV	MTV	kA
Finsko	Finsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Francie	Francie	k1gFnSc1
</s>
<s>
MTV	MTV	kA
Indie	Indie	k1gFnSc1
</s>
<s>
MTV	MTV	kA
Indonésie	Indonésie	k1gFnSc1
</s>
<s>
MTV	MTV	kA
Irsko	Irsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Itálie	Itálie	k1gFnSc1
</s>
<s>
MTV	MTV	kA
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Korea	Korea	k1gFnSc1
</s>
<s>
MTV	MTV	kA
Latin	Latin	k1gMnSc1
America	America	k1gMnSc1
</s>
<s>
MTV	MTV	kA
Litva	Litva	k1gFnSc1
</s>
<s>
MTV	MTV	kA
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Kanada	Kanada	k1gFnSc1
</s>
<s>
MTV	MTV	kA
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Německo	Německo	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Holandsko	Holandsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Norsko	Norsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Nový	nový	k2eAgInSc4d1
Zéland	Zéland	k1gInSc4
</s>
<s>
MTV	MTV	kA
Pákistán	Pákistán	k1gInSc1
</s>
<s>
MTV	MTV	kA
Polsko	Polsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Rusko	Rusko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Řecko	Řecko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Taiwan	Taiwan	k1gMnSc1
</s>
<s>
MTV	MTV	kA
Thajsko	Thajsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Turecko	Turecko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Seriálová	seriálový	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
stanice	stanice	k1gFnSc2
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Barrio	Barrio	k6eAd1
19	#num#	k4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Battle	Battle	k1gFnSc1
for	forum	k1gNnPc2
Ozzfest	Ozzfest	k1gInSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Becoming	Becoming	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Burned	Burned	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Busted	Busted	k1gInSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Buzzin	Buzzin	k1gInSc1
<g/>
'	'	kIx"
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Call	Calnout	k5eAaPmAgInS
to	ten	k3xDgNnSc1
Greatness	Greatness	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Celebrity	celebrita	k1gFnPc1
Rap	rapa	k1gFnPc2
Superstar	superstar	k1gFnPc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
City	city	k1gNnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
College	College	k1gFnSc1
Life	Life	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dancelife	Dancelif	k1gMnSc5
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dismissed	Dismissed	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Downtown	Downtown	k1gNnSc1
Girls	girl	k1gFnPc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Dudesons	Dudesons	k1gInSc1
in	in	k?
America	America	k1gFnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
-dodnes	-dodnesa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
8	#num#	k4
<g/>
th	th	k?
and	and	k?
Ocean	Ocean	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Engaged	Engaged	k1gInSc1
and	and	k?
Underage	Underage	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Exiled	Exiled	k1gInSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Exposed	Exposed	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Faking	Faking	k1gInSc1
the	the	k?
Video	video	k1gNnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
FANatic	FANatice	k1gFnPc2
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fast	Fast	k1gMnSc1
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fear	Fear	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Flipped	Flipped	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
FM	FM	kA
Nation	Nation	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fraternity	fraternita	k1gFnPc1
Life	Life	k1gFnPc2
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Global	globat	k5eAaImAgMnS
Groove	Groov	k1gInSc5
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
</s>
<s>
High	High	k1gMnSc1
School	Schoola	k1gFnPc2
Stories	Stories	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Hills	Hills	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
I	i	k9
Want	Want	k1gMnSc1
A	a	k8xC
Famous	Famous	k1gMnSc1
Face	Fac	k1gFnSc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
From	From	k1gInSc1
Rolling	Rolling	k1gInSc1
Stone	ston	k1gInSc5
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Is	Is	k?
She	She	k1gMnSc1
Really	Realla	k1gFnSc2
Going	Going	k1gMnSc1
Out	Out	k1gMnSc1
With	With	k1gMnSc1
Him	Him	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jackass	Jackass	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jammed	Jammed	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Juvies	Juvies	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jersey	Jersea	k1gFnPc1
Shore	Shor	k1gInSc5
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Laguna	laguna	k1gFnSc1
Beach	Beacha	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gFnPc2
Real	Real	k1gInSc1
Orange	Orang	k1gInSc2
County	Counta	k1gFnSc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Life	Life	k6eAd1
of	of	k?
Ryan	Ryan	k1gMnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
MADE	MADE	kA
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Making	Making	k1gInSc1
the	the	k?
Band	band	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Maui	Maui	k1gNnSc1
Fever	Fevra	k1gFnPc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Miss	miss	k1gFnSc1
Seventeen	Seventena	k1gFnPc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
My	my	k3xPp1nPc1
Life	Life	k1gInSc1
as	as	k1gNnSc6
Liz	liz	k1gInSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
My	my	k3xPp1nPc1
Own	Own	k1gMnPc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
My	my	k3xPp1nPc1
Super	super	k6eAd1
Sweet	Sweet	k1gInSc1
16	#num#	k4
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Newport	Newport	k1gInSc1
Harbor	Harbor	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnPc1
Real	Real	k1gInSc4
Orange	Orang	k1gFnSc2
County	Counta	k1gFnSc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Next	Next	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Once	Once	k1gFnSc1
Upon	Upona	k1gFnPc2
a	a	k8xC
Prom	Proma	k1gFnPc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
One	One	k?
Bad	Bad	k1gMnSc1
Trip	Trip	k1gMnSc1
<g/>
'	'	kIx"
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Paper	Paper	k1gInSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Parental	Parental	k1gMnSc1
Control	Controla	k1gFnPc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pimp	Pimp	k1gMnSc1
My	my	k3xPp1nPc1
Ride	Ride	k1gNnPc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
PoweR	PoweR	k?
Girls	girl	k1gFnPc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Real	Real	k1gInSc1
World	World	k1gInSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Real	Real	k1gInSc1
World	World	k1gInSc1
<g/>
/	/	kIx~
<g/>
Road	Road	k1gInSc1
Rules	Rulesa	k1gFnPc2
Challenge	Challenge	k1gFnPc2
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Rich	Rich	k1gMnSc1
Girls	girl	k1gFnPc2
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Road	Road	k1gMnSc1
Rules	Rules	k1gMnSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rob	roba	k1gFnPc2
&	&	k?
Big	Big	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rob	roba	k1gFnPc2
Dyrdek	Dyrdka	k1gFnPc2
Fantasy	fantas	k1gInPc1
Factory	Factor	k1gInPc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
-dodnes	-dodnesa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
ROOM	ROOM	kA
401	#num#	k4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Room	Room	k1gInSc1
Raiders	Raiders	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Scarred	Scarred	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Score	Scor	k1gMnSc5
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
70	#num#	k4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
House	house	k1gNnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sex	sex	k1gInSc1
with	with	k1gMnSc1
Mom	Mom	k1gMnSc1
and	and	k?
Dad	Dad	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Shop	shop	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
16	#num#	k4
and	and	k?
Pregnant	Pregnant	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Sorority	Sororit	k1gInPc1
Life	Lif	k1gFnSc2
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Surf	surf	k1gInSc1
Girls	girl	k1gFnPc2
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Taking	Taking	k1gInSc1
the	the	k?
Stage	Stage	k1gInSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Teen	Teen	k1gMnSc1
Mom	Mom	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Tiara	Tiara	k1gFnSc1
Girls	girl	k1gFnPc2
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Trailer	Trailer	k1gMnSc1
Fabulous	Fabulous	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Trick	Trick	k1gMnSc1
It	It	k1gMnSc1
Out	Out	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Trip	Trip	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Twentyfourseven	Twentyfourseven	k2eAgMnSc1d1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Two-A-Days	Two-A-Days	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Viva	Viv	k2eAgNnPc1d1
La	la	k1gNnPc1
Bam	bam	k0
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Wade	Wade	k?
Robson	Robson	k1gMnSc1
Project	Project	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Wanna	Wanen	k2eAgFnSc1d1
Come	Come	k1gFnSc1
In	In	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Why	Why	k?
Can	Can	k1gFnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
I	i	k9
Be	Be	k1gMnSc1
You	You	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Wildboyz	Wildboyz	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Wrestling	Wrestling	k1gInSc1
Society	societa	k1gFnSc2
X	X	kA
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
WWE	WWE	kA
Tough	Tough	k1gMnSc1
Enough	Enough	k1gMnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
X	X	kA
Effect	Effect	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
MTV	MTV	kA
Czech	Czech	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
MTV	MTV	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
MTV	MTV	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
MTV	MTV	kA
International	International	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
MTV	MTV	kA
Networks	Networks	k1gInSc1
Europe	Europ	k1gInSc5
pro	pro	k7c4
celou	celý	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
</s>
<s>
MTV	MTV	kA
Dance	Danka	k1gFnSc3
·	·	k?
MTV	MTV	kA
Europe	Europ	k1gInSc5
·	·	k?
VH1	VH1	k1gMnSc1
Classic	Classic	k1gMnSc1
Europe	Europ	k1gInSc5
·	·	k?
VH1	VH1	k1gMnSc5
Europe	Europ	k1gMnSc5
·	·	k?
Nickelodeon	Nickelodeon	k1gInSc1
Europe	Europ	k1gInSc5
·	·	k?
MTV	MTV	kA
Live	Liv	k1gFnSc2
HD	HD	kA
Balkán	Balkán	k1gInSc1
</s>
<s>
MTV	MTV	kA
Adria	Adrius	k1gMnSc4
Pobaltí	Pobaltí	k1gNnSc2
</s>
<s>
MTV	MTV	kA
Estonsko	Estonsko	k1gNnSc1
·	·	k?
MTV	MTV	kA
Litva	Litva	k1gFnSc1
a	a	k8xC
Lotyšsko	Lotyšsko	k1gNnSc1
Česko	Česko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Czech	Czech	k1gMnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gInSc1
Nizozemsko	Nizozemsko	k1gNnSc1
a	a	k8xC
Belgie	Belgie	k1gFnSc1
</s>
<s>
MTV	MTV	kA
Nizozemsko	Nizozemsko	k1gNnSc1
·	·	k?
MTV	MTV	kA
Brand	Brand	k1gInSc1
New	New	k1gMnSc2
·	·	k?
Comedy	Comeda	k1gMnSc2
Central	Central	k1gMnSc2
Nizozemsko	Nizozemsko	k1gNnSc1
·	·	k?
Comedy	Comeda	k1gMnSc2
Central	Central	k1gMnSc2
Family	Famila	k1gFnSc2
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
·	·	k?
Nick	Nick	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nizozemsko	Nizozemsko	k1gNnSc4
·	·	k?
Nick	Nick	k1gInSc1
Toons	Toonsa	k1gFnPc2
Nizozemsko	Nizozemsko	k1gNnSc1
·	·	k?
Nick	Nick	k1gInSc1
Hits	Hitsa	k1gFnPc2
Nizozemsko	Nizozemsko	k1gNnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Valonsko	Valonsko	k1gNnSc1
·	·	k?
Nick	Nick	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Valonsko	Valonsko	k1gNnSc1
·	·	k?
TMF	TMF	kA
Flandry	Flandry	k1gInPc1
·	·	k?
TMF	TMF	kA
Nizozemsko	Nizozemsko	k1gNnSc1
·	·	k?
TMF	TMF	kA
NL	NL	kA
·	·	k?
TMF	TMF	kA
Dance	Danka	k1gFnSc3
·	·	k?
TMF	TMF	kA
Pure	Pur	k1gFnSc2
Skandinávie	Skandinávie	k1gFnSc2
</s>
<s>
MTV	MTV	kA
Dánsko	Dánsko	k1gNnSc1
·	·	k?
MTV	MTV	kA
Norsko	Norsko	k1gNnSc1
·	·	k?
MTV	MTV	kA
Finsko	Finsko	k1gNnSc1
·	·	k?
MTV	MTV	kA
Švédsko	Švédsko	k1gNnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Dánsko	Dánsko	k1gNnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Švédsko	Švédsko	k1gNnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Skandinávie	Skandinávie	k1gFnSc2
·	·	k?
VH1	VH1	k1gMnSc1
Dánsko	Dánsko	k1gNnSc1
·	·	k?
Comedy	Comeda	k1gMnSc2
Central	Central	k1gMnSc2
Švédsko	Švédsko	k1gNnSc1
Francie	Francie	k1gFnSc2
</s>
<s>
MTV	MTV	kA
Francie	Francie	k1gFnSc1
·	·	k?
MTV	MTV	kA
Idol	idol	k1gInSc1
·	·	k?
MTV	MTV	kA
Pulse	puls	k1gInSc5
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc2
<g/>
)	)	kIx)
·	·	k?
MTV	MTV	kA
Base	basa	k1gFnSc3
·	·	k?
Nickelodeon	Nickelodeon	k1gNnSc4
Francie	Francie	k1gFnSc2
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Junior	junior	k1gMnSc1
·	·	k?
Game	game	k1gInSc1
One	One	k1gFnPc2
Německo	Německo	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Německo	Německo	k1gNnSc1
·	·	k?
VIVA	VIVA	kA
Německo	Německo	k1gNnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Německo	Německo	k1gNnSc1
·	·	k?
Comedy	Comeda	k1gMnSc2
Central	Central	k1gMnSc2
Německo	Německo	k1gNnSc1
·	·	k?
MTV	MTV	kA
Rakousko	Rakousko	k1gNnSc1
·	·	k?
Viva	Viva	k1gMnSc1
Rakousko	Rakousko	k1gNnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Rakousko	Rakousko	k1gNnSc1
Pay	Pay	k1gMnSc1
TV	TV	kA
<g/>
:	:	kIx,
MTV	MTV	kA
Entertainment	Entertainment	k1gInSc1
·	·	k?
Nicktoons	Nicktoonsa	k1gFnPc2
Německo	Německo	k1gNnSc1
·	·	k?
Nick	Nick	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německo	Německo	k1gNnSc1
·	·	k?
MTV	MTV	kA
Švýcarsko	Švýcarsko	k1gNnSc1
·	·	k?
Viva	Viva	k1gMnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
Řecko	Řecko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Řecko	Řecko	k1gNnSc1
·	·	k?
MTV	MTV	kA
<g/>
+	+	kIx~
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Maďarsko	Maďarsko	k1gNnSc1
·	·	k?
VIVA	VIVA	kA
Maďarsko	Maďarsko	k1gNnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Maďarsko	Maďarsko	k1gNnSc1
·	·	k?
Comedy	Comeda	k1gMnSc2
Central	Central	k1gMnSc2
Maďarsko	Maďarsko	k1gNnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
MTV	MTV	kA
Itálie	Itálie	k1gFnSc1
·	·	k?
MTV	MTV	kA
Hits	Hits	k1gInSc1
·	·	k?
MTV	MTV	kA
Classic	Classic	k1gMnSc1
·	·	k?
MTV	MTV	kA
Brand	Brand	k1gMnSc1
New	New	k1gMnSc1
·	·	k?
MTV	MTV	kA
Pulse	puls	k1gInSc5
·	·	k?
Nickelodeon	Nickelodeon	k1gNnSc4
Itálie	Itálie	k1gFnSc2
·	·	k?
Nick	Nick	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Itálie	Itálie	k1gFnSc1
·	·	k?
Comedy	Comeda	k1gMnSc2
Central	Central	k1gMnSc2
Itálie	Itálie	k1gFnSc2
·	·	k?
QOOB	QOOB	kA
(	(	kIx(
<g/>
49	#num#	k4
<g/>
%	%	kIx~
s	s	k7c7
TI	ten	k3xDgMnPc1
Media	medium	k1gNnPc1
<g/>
)	)	kIx)
Izrael	Izrael	k1gInSc1
</s>
<s>
MTV	MTV	kA
Izrael	Izrael	k1gInSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gInSc1
Izrael	Izrael	k1gInSc1
Polsko	Polsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Polsko	Polsko	k1gNnSc1
·	·	k?
VH1	VH1	k1gMnSc1
Polsko	Polsko	k1gNnSc1
·	·	k?
VIVA	VIVA	kA
Polsko	Polsko	k1gNnSc1
·	·	k?
Comedy	Comeda	k1gMnSc2
Central	Central	k1gMnSc2
Polsko	Polsko	k1gNnSc1
·	·	k?
Comedy	Comeda	k1gMnSc2
Central	Central	k1gMnSc2
Family	Famila	k1gFnSc2
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Polsko	Polsko	k1gNnSc1
·	·	k?
Switch	Switcha	k1gFnPc2
<g/>
2	#num#	k4
<g/>
One	One	k1gFnPc2
Portugalsko	Portugalsko	k1gNnSc4
</s>
<s>
MTV	MTV	kA
Portugalsko	Portugalsko	k1gNnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Portugalsko	Portugalsko	k1gNnSc1
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Rumunsko	Rumunsko	k1gNnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gNnSc1
Romania	Romanium	k1gNnSc2
Rusko	Rusko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Rusko	Rusko	k1gNnSc1
·	·	k?
VH1	VH1	k1gMnSc1
Rusko	Rusko	k1gNnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gNnSc1
(	(	kIx(
<g/>
CIS	cis	k1gNnSc1
<g/>
)	)	kIx)
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Španělsko	Španělsko	k1gNnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Španělsko	Španělsko	k1gNnSc1
·	·	k?
Paramount	Paramount	k1gMnSc1
Comedy	Comeda	k1gMnSc2
Španělsko	Španělsko	k1gNnSc4
Turecko	Turecko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Turecko	Turecko	k1gNnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Turecko	Turecko	k1gNnSc1
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
MTV	MTV	kA
Ukrajina	Ukrajina	k1gFnSc1
·	·	k?
VH1	VH1	k1gFnSc1
Ukrajina	Ukrajina	k1gFnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gInSc1
(	(	kIx(
<g/>
CIS	cis	k1gNnSc1
<g/>
)	)	kIx)
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
a	a	k8xC
Irsko	Irsko	k1gNnSc1
</s>
<s>
MTV	MTV	kA
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
a	a	k8xC
Irsko	Irsko	k1gNnSc1
·	·	k?
MTV	MTV	kA
Classic	Classic	k1gMnSc1
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
a	a	k8xC
Irsko	Irsko	k1gNnSc1
<g/>
)	)	kIx)
·	·	k?
MTV	MTV	kA
Base	bas	k1gInSc6
·	·	k?
MTV	MTV	kA
Hits	Hits	k1gInSc1
(	(	kIx(
<g/>
Evropa	Evropa	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
MTV	MTV	kA
Rocks	Rocks	k1gInSc1
·	·	k?
MTV	MTV	kA
Shows	Shows	k1gInSc1
·	·	k?
Comedy	Comeda	k1gMnSc2
Central	Central	k1gMnSc2
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
a	a	k8xC
Irsko	Irsko	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Comedy	Comed	k1gMnPc7
Central	Central	k1gFnSc2
Extra	extra	k2eAgMnSc1d1
(	(	kIx(
<g/>
75	#num#	k4
<g/>
%	%	kIx~
s	s	k7c7
BSkyB	BSkyB	k1gFnSc7
<g/>
)	)	kIx)
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
a	a	k8xC
Irsko	Irsko	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Nick	Nick	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
království	království	k1gNnSc1
a	a	k8xC
Irsko	Irsko	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Nick	Nick	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
/	/	kIx~
<g/>
Nicktoons	Nicktoonsa	k1gFnPc2
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
a	a	k8xC
Irsko	Irsko	k1gNnSc1
(	(	kIx(
<g/>
60	#num#	k4
<g/>
%	%	kIx~
s	s	k7c7
BSkyB	BSkyB	k1gFnSc7
<g/>
)	)	kIx)
·	·	k?
VH1	VH1	k1gFnSc2
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
a	a	k8xC
Irsko	Irsko	k1gNnSc1
·	·	k?
Viva	Viv	k1gInSc2
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
a	a	k8xC
Irsko	Irsko	k1gNnSc1
·	·	k?
MTV	MTV	kA
Irsko	Irsko	k1gNnSc1
·	·	k?
Nickelodeon	Nickelodeon	k1gMnSc1
Irsko	Irsko	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Televize	televize	k1gFnSc1
|	|	kIx~
Hudba	hudba	k1gFnSc1
</s>
