<s>
Měď	měď	k1gFnSc1	měď
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
mezi	mezi	k7c4	mezi
biogenní	biogenní	k2eAgInPc4d1	biogenní
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
hemocyaninu	hemocyanina	k1gFnSc4	hemocyanina
obsaženého	obsažený	k2eAgMnSc2d1	obsažený
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
.	.	kIx.	.
</s>
