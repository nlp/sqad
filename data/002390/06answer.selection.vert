<s>
Edikt	edikt	k1gInSc1	edikt
milánský	milánský	k2eAgInSc1d1	milánský
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Milánské	milánský	k2eAgNnSc1d1	Milánské
ujednání	ujednání	k1gNnSc1	ujednání
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Milánský	milánský	k2eAgInSc1d1	milánský
reskript	reskript	k1gInSc1	reskript
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
konvenční	konvenční	k2eAgNnSc1d1	konvenční
označení	označení	k1gNnSc1	označení
dokumentu	dokument	k1gInSc2	dokument
z	z	k7c2	z
roku	rok	k1gInSc2	rok
313	[number]	k4	313
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vydal	vydat	k5eAaPmAgMnS	vydat
císař	císař	k1gMnSc1	císař
Konstantin	Konstantin	k1gMnSc1	Konstantin
I.	I.	kA	I.
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
spoluvladař	spoluvladař	k1gMnSc1	spoluvladař
Licinius	Licinius	k1gMnSc1	Licinius
<g/>
.	.	kIx.	.
</s>
