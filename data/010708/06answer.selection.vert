<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
barevných	barevný	k2eAgFnPc2d1	barevná
fotografií	fotografia	k1gFnPc2	fotografia
je	být	k5eAaImIp3nS	být
Landscape	Landscap	k1gInSc5	Landscap
of	of	k?	of
Southern	Southern	k1gInSc4	Southern
France	Franc	k1gMnSc2	Franc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
pořízena	pořídit	k5eAaPmNgFnS	pořídit
subtraktivní	subtraktivní	k2eAgFnSc7d1	subtraktivní
metodou	metoda	k1gFnSc7	metoda
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
