<p>
<s>
Idiom	idiom	k1gInSc1	idiom
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
idióma	idióma	k1gFnSc1	idióma
<g/>
,	,	kIx,	,
jazyková	jazykový	k2eAgFnSc1d1	jazyková
zvláštnost	zvláštnost	k1gFnSc1	zvláštnost
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ustálený	ustálený	k2eAgInSc1d1	ustálený
víceslovný	víceslovný	k2eAgInSc1d1	víceslovný
výraz	výraz	k1gInSc1	výraz
či	či	k8xC	či
frazém	frazém	k1gInSc1	frazém
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
význam	význam	k1gInSc4	význam
nelze	lze	k6eNd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
z	z	k7c2	z
běžných	běžný	k2eAgInPc2d1	běžný
významů	význam	k1gInPc2	význam
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
význam	význam	k1gInSc1	význam
idiomu	idiom	k1gInSc2	idiom
"	"	kIx"	"
<g/>
ztratit	ztratit	k5eAaPmF	ztratit
hlavu	hlava	k1gFnSc4	hlava
<g/>
"	"	kIx"	"
nesouvisí	souviset	k5eNaImIp3nS	souviset
s	s	k7c7	s
běžným	běžný	k2eAgInSc7d1	běžný
významem	význam	k1gInSc7	význam
slova	slovo	k1gNnSc2	slovo
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nechat	nechat	k5eAaPmF	nechat
na	na	k7c6	na
holičkách	holička	k1gFnPc6	holička
<g/>
"	"	kIx"	"
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Idiom	idiom	k1gInSc1	idiom
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
synonymum	synonymum	k1gNnSc4	synonymum
pro	pro	k7c4	pro
frazém	frazém	k1gInSc4	frazém
<g/>
,	,	kIx,	,
jen	jen	k6eAd1	jen
zkoumaný	zkoumaný	k2eAgInSc1d1	zkoumaný
ze	z	k7c2	z
sémantického	sémantický	k2eAgNnSc2d1	sémantické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
podmnožinu	podmnožina	k1gFnSc4	podmnožina
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumáním	zkoumání	k1gNnSc7	zkoumání
idiomů	idiom	k1gInPc2	idiom
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
frazeologie	frazeologie	k1gFnSc1	frazeologie
a	a	k8xC	a
idiomatika	idiomatika	k1gFnSc1	idiomatika
<g/>
,	,	kIx,	,
součásti	součást	k1gFnPc1	součást
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
idiom	idiom	k1gInSc1	idiom
užívá	užívat	k5eAaImIp3nS	užívat
i	i	k9	i
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
varianty	varianta	k1gFnSc2	varianta
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
nářečí	nářečí	k1gNnSc2	nářečí
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Idiomy	idiom	k1gInPc1	idiom
tvoří	tvořit	k5eAaImIp3nP	tvořit
významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
běžné	běžný	k2eAgInPc1d1	běžný
zejména	zejména	k9	zejména
v	v	k7c6	v
hovorovém	hovorový	k2eAgInSc6d1	hovorový
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Označují	označovat	k5eAaImIp3nP	označovat
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgFnPc4	jenž
jazyk	jazyk	k1gMnSc1	jazyk
nemá	mít	k5eNaImIp3nS	mít
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
z	z	k7c2	z
někoho	někdo	k3yInSc4	někdo
si	se	k3xPyFc3	se
vystřelit	vystřelit	k5eAaPmF	vystřelit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
s	s	k7c7	s
někým	někdo	k3yInSc7	někdo
vyběhnout	vyběhnout	k5eAaPmF	vyběhnout
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
někomu	někdo	k3yInSc3	někdo
to	ten	k3xDgNnSc4	ten
polepit	polepit	k5eAaPmF	polepit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
mnoho	mnoho	k4c4	mnoho
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
metaforami	metafora	k1gFnPc7	metafora
a	a	k8xC	a
často	často	k6eAd1	často
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
idiomy	idiom	k1gInPc1	idiom
vznikají	vznikat	k5eAaImIp3nP	vznikat
snáze	snadno	k6eAd2	snadno
než	než	k8xS	než
nová	nový	k2eAgNnPc1d1	nové
slova	slovo	k1gNnPc1	slovo
a	a	k8xC	a
také	také	k9	také
snadno	snadno	k6eAd1	snadno
zastarávají	zastarávat	k5eAaImIp3nP	zastarávat
a	a	k8xC	a
zanikají	zanikat	k5eAaImIp3nP	zanikat
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
tedy	tedy	k9	tedy
pružně	pružně	k6eAd1	pružně
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
nové	nový	k2eAgFnPc4d1	nová
skutečnosti	skutečnost	k1gFnPc4	skutečnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
mluvčí	mluvčí	k1gMnPc1	mluvčí
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
dodávají	dodávat	k5eAaImIp3nP	dodávat
hovorovému	hovorový	k2eAgInSc3d1	hovorový
jazyku	jazyk	k1gInSc3	jazyk
jeho	on	k3xPp3gMnSc4	on
živost	živost	k1gFnSc1	živost
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
některé	některý	k3yIgNnSc1	některý
ještě	ještě	k6eAd1	ještě
nedostaly	dostat	k5eNaPmAgFnP	dostat
do	do	k7c2	do
jazykových	jazykový	k2eAgInPc2d1	jazykový
slovníků	slovník	k1gInPc2	slovník
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
nesnáze	snadno	k6eNd2	snadno
hlavně	hlavně	k9	hlavně
cizincům	cizinec	k1gMnPc3	cizinec
a	a	k8xC	a
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
jejich	jejich	k3xOp3gFnSc4	jejich
řeč	řeč	k1gFnSc4	řeč
od	od	k7c2	od
rodilých	rodilý	k2eAgMnPc2d1	rodilý
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Odkazují	odkazovat	k5eAaImIp3nP	odkazovat
často	často	k6eAd1	často
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
kulturní	kulturní	k2eAgInSc4d1	kulturní
kontext	kontext	k1gInSc4	kontext
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
srozumitelné	srozumitelný	k2eAgFnPc1d1	srozumitelná
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
přiznat	přiznat	k5eAaPmF	přiznat
barvu	barva	k1gFnSc4	barva
<g/>
"	"	kIx"	"
na	na	k7c4	na
karetní	karetní	k2eAgFnPc4d1	karetní
hry	hra	k1gFnPc4	hra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
produktivní	produktivní	k2eAgMnPc1d1	produktivní
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
nedají	dát	k5eNaPmIp3nP	dát
se	se	k3xPyFc4	se
obměňovat	obměňovat	k5eAaImF	obměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
zastaralé	zastaralý	k2eAgFnPc1d1	zastaralá
nebo	nebo	k8xC	nebo
nepřesně	přesně	k6eNd1	přesně
použité	použitý	k2eAgInPc1d1	použitý
idiomy	idiom	k1gInPc1	idiom
působí	působit	k5eAaImIp3nP	působit
v	v	k7c6	v
řeči	řeč	k1gFnSc6	řeč
směšně	směšně	k6eAd1	směšně
<g/>
.	.	kIx.	.
<g/>
Zvlášť	zvlášť	k6eAd1	zvlášť
časté	častý	k2eAgInPc1d1	častý
a	a	k8xC	a
významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
idiomy	idiom	k1gInPc1	idiom
ve	v	k7c6	v
slangu	slang	k1gInSc6	slang
a	a	k8xC	a
v	v	k7c6	v
argotu	argot	k1gInSc6	argot
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přímo	přímo	k6eAd1	přímo
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zakrývání	zakrývání	k1gNnSc3	zakrývání
skutečného	skutečný	k2eAgInSc2d1	skutečný
významu	význam	k1gInSc2	význam
před	před	k7c7	před
nezasvěcenými	zasvěcený	k2eNgFnPc7d1	nezasvěcená
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
časem	časem	k6eAd1	časem
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
i	i	k9	i
do	do	k7c2	do
běžného	běžný	k2eAgInSc2d1	běžný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
vzít	vzít	k5eAaPmF	vzít
roha	roha	k6eAd1	roha
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
utéci	utéct	k5eAaPmF	utéct
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
natáhnout	natáhnout	k5eAaPmF	natáhnout
bačkory	bačkor	k1gInPc4	bačkor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
)	)	kIx)	)
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Překlad	překlad	k1gInSc1	překlad
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
idiomy	idiom	k1gInPc1	idiom
mají	mít	k5eAaImIp3nP	mít
přesný	přesný	k2eAgInSc4d1	přesný
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
české	český	k2eAgInPc1d1	český
"	"	kIx"	"
<g/>
přiznat	přiznat	k5eAaPmF	přiznat
barvu	barva	k1gFnSc4	barva
<g/>
"	"	kIx"	"
a	a	k8xC	a
německé	německý	k2eAgFnPc1d1	německá
"	"	kIx"	"
<g/>
Farbe	Farb	k1gInSc5	Farb
bekennen	bekennet	k5eAaImNgMnS	bekennet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většinou	většinou	k6eAd1	většinou
ale	ale	k8xC	ale
doslovný	doslovný	k2eAgInSc1d1	doslovný
překlad	překlad	k1gInSc1	překlad
idiomu	idiom	k1gInSc2	idiom
nedává	dávat	k5eNaImIp3nS	dávat
smysl	smysl	k1gInSc1	smysl
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
musí	muset	k5eAaImIp3nS	muset
hledat	hledat	k5eAaImF	hledat
obdobný	obdobný	k2eAgInSc1d1	obdobný
idiom	idiom	k1gInSc1	idiom
v	v	k7c6	v
cílovém	cílový	k2eAgInSc6d1	cílový
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgInPc1d1	důležitý
a	a	k8xC	a
ustálené	ustálený	k2eAgInPc1d1	ustálený
idiomy	idiom	k1gInPc1	idiom
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
uvádějí	uvádět	k5eAaImIp3nP	uvádět
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
jazykových	jazykový	k2eAgInPc6d1	jazykový
slovnících	slovník	k1gInPc6	slovník
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
samostatné	samostatný	k2eAgInPc1d1	samostatný
slovníky	slovník	k1gInPc1	slovník
frazeologismů	frazeologismus	k1gInPc2	frazeologismus
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
takový	takový	k3xDgInSc1	takový
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
nenajde	najít	k5eNaPmIp3nS	najít
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
idiom	idiom	k1gInSc1	idiom
opsat	opsat	k5eAaPmF	opsat
více	hodně	k6eAd2	hodně
slovy	slovo	k1gNnPc7	slovo
nebo	nebo	k8xC	nebo
i	i	k9	i
vedlejší	vedlejší	k2eAgFnSc7d1	vedlejší
větou	věta	k1gFnSc7	věta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lingvistická	lingvistický	k2eAgFnSc1d1	lingvistická
teorie	teorie	k1gFnSc1	teorie
==	==	k?	==
</s>
</p>
<p>
<s>
Idiom	idiom	k1gInSc1	idiom
je	být	k5eAaImIp3nS	být
dokladem	doklad	k1gInSc7	doklad
kontextuality	kontextualita	k1gFnSc2	kontextualita
jazyka	jazyk	k1gInSc2	jazyk
<g/>
:	:	kIx,	:
význam	význam	k1gInSc1	význam
věty	věta	k1gFnSc2	věta
není	být	k5eNaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
jen	jen	k9	jen
spojením	spojení	k1gNnSc7	spojení
významů	význam	k1gInPc2	význam
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
slova	slovo	k1gNnPc1	slovo
se	se	k3xPyFc4	se
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
navzájem	navzájem	k6eAd1	navzájem
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
a	a	k8xC	a
podmiňují	podmiňovat	k5eAaImIp3nP	podmiňovat
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
slova	slovo	k1gNnPc4	slovo
v	v	k7c6	v
idiomu	idiom	k1gInSc6	idiom
zpravidla	zpravidla	k6eAd1	zpravidla
nelze	lze	k6eNd1	lze
nahradit	nahradit	k5eAaPmF	nahradit
synonymem	synonymum	k1gNnSc7	synonymum
<g/>
:	:	kIx,	:
místo	místo	k1gNnSc4	místo
"	"	kIx"	"
<g/>
ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
vyžral	vyžrat	k5eAaPmAgInS	vyžrat
<g/>
"	"	kIx"	"
nelze	lze	k6eNd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
vyjedl	vyjíst	k5eAaPmAgInS	vyjíst
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
"	"	kIx"	"
<g/>
vyžrat	vyžrat	k5eAaPmF	vyžrat
něco	něco	k3yInSc4	něco
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
"	"	kIx"	"
<g/>
vyžírka	vyžírka	k1gFnSc1	vyžírka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
sloveso	sloveso	k1gNnSc4	sloveso
vyžrat	vyžrat	k5eAaPmF	vyžrat
jiný	jiný	k2eAgInSc4d1	jiný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Idiom	idiom	k1gInSc1	idiom
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
frazém	frazém	k1gInSc1	frazém
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
sémantická	sémantický	k2eAgFnSc1d1	sémantická
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
samostatný	samostatný	k2eAgInSc1d1	samostatný
lexém	lexém	k1gInSc1	lexém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČERMÁK	Čermák	k1gMnSc1	Čermák
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Idiomatika	idiomatika	k1gFnSc1	idiomatika
a	a	k8xC	a
frazeologie	frazeologie	k1gFnSc1	frazeologie
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
239	[number]	k4	239
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ČERMÁK	Čermák	k1gMnSc1	Čermák
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
jazykověda	jazykověda	k1gFnSc1	jazykověda
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
380	[number]	k4	380
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČERMÁK	Čermák	k1gMnSc1	Čermák
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
české	český	k2eAgFnSc2d1	Česká
frazeologie	frazeologie	k1gFnSc2	frazeologie
a	a	k8xC	a
idiomatiky	idiomatika	k1gFnSc2	idiomatika
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přepracované	přepracovaný	k2eAgInPc1d1	přepracovaný
a	a	k8xC	a
doplněné	doplněný	k2eAgInPc1d1	doplněný
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Voznice	voznice	k1gFnSc1	voznice
<g/>
:	:	kIx,	:
Leda	leda	k6eAd1	leda
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7335	[number]	k4	7335
<g/>
-	-	kIx~	-
<g/>
215	[number]	k4	215
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČERMÁK	Čermák	k1gMnSc1	Čermák
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Frazeologie	frazeologie	k1gFnSc1	frazeologie
a	a	k8xC	a
idiomatika	idiomatika	k1gFnSc1	idiomatika
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
obecná	obecný	k2eAgFnSc1d1	obecná
=	=	kIx~	=
Czech	Czech	k1gMnSc1	Czech
and	and	k?	and
General	General	k1gMnSc1	General
Phraseology	Phraseolog	k1gMnPc7	Phraseolog
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
718	[number]	k4	718
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
1371	[number]	k4	1371
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Frazém	frazém	k1gInSc1	frazém
</s>
</p>
<p>
<s>
Metafora	metafora	k1gFnSc1	metafora
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
idiom	idiom	k1gInSc1	idiom
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Idiomy	idiom	k1gInPc1	idiom
podle	podle	k7c2	podle
jazyků	jazyk	k1gInPc2	jazyk
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
WikIdioms	WikIdioms	k6eAd1	WikIdioms
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
idiom	idiom	k1gInSc4	idiom
na	na	k7c4	na
Glossary	Glossar	k1gInPc4	Glossar
of	of	k?	of
linguistic	linguistice	k1gFnPc2	linguistice
terms	terms	k6eAd1	terms
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
amerických	americký	k2eAgInPc2d1	americký
idiomů	idiom	k1gInPc2	idiom
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
anglických	anglický	k2eAgInPc2d1	anglický
idiomů	idiom	k1gInPc2	idiom
</s>
</p>
