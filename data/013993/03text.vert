<s>
CSV	CSV	kA
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
souborovém	souborový	k2eAgInSc6d1
formátu	formát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
CSV	CSV	kA
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Comma-separated	Comma-separated	k1gMnSc1
values	values	k1gMnSc1
</s>
<s>
Přípona	přípona	k1gFnSc1
souboru	soubor	k1gInSc2
</s>
<s>
<g/>
csv	csv	k?
Typ	typa	k1gFnPc2
internetového	internetový	k2eAgNnSc2d1
média	médium	k1gNnSc2
</s>
<s>
text	text	k1gInSc1
<g/>
/	/	kIx~
<g/>
csv	csv	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Standard	standard	k1gInSc1
<g/>
(	(	kIx(
<g/>
y	y	k?
<g/>
)	)	kIx)
</s>
<s>
RFC	RFC	kA
4180	#num#	k4
</s>
<s>
CSV	CSV	kA
(	(	kIx(
<g/>
Comma-separated	Comma-separated	k1gMnSc1
values	values	k1gMnSc1
<g/>
,	,	kIx,
hodnoty	hodnota	k1gFnPc1
oddělené	oddělený	k2eAgFnPc1d1
čárkami	čárka	k1gFnPc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednoduchý	jednoduchý	k2eAgInSc1d1
souborový	souborový	k2eAgInSc1d1
formát	formát	k1gInSc1
určený	určený	k2eAgInSc1d1
pro	pro	k7c4
výměnu	výměna	k1gFnSc4
tabulkových	tabulkový	k2eAgNnPc2d1
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soubor	soubor	k1gInSc1
ve	v	k7c6
formátu	formát	k1gInSc6
CSV	CSV	kA
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
řádků	řádek	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgNnPc6,k3yQgNnPc6,k3yIgNnPc6
jsou	být	k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgFnPc4d1
položky	položka	k1gFnPc4
odděleny	oddělen	k2eAgFnPc4d1
znakem	znak	k1gInSc7
čárka	čárka	k1gFnSc1
(	(	kIx(
<g/>
,	,	kIx,
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnoty	hodnota	k1gFnSc2
položek	položka	k1gFnPc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
uzavřeny	uzavřít	k5eAaPmNgFnP
do	do	k7c2
uvozovek	uvozovka	k1gFnPc2
(	(	kIx(
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
umožňuje	umožňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
text	text	k1gInSc1
položky	položka	k1gFnSc2
obsahoval	obsahovat	k5eAaImAgInS
čárku	čárka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
text	text	k1gInSc1
položky	položka	k1gFnSc2
obsahuje	obsahovat	k5eAaImIp3nS
uvozovky	uvozovka	k1gFnPc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgInPc4
zdvojeny	zdvojen	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s>
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
v	v	k7c6
některých	některý	k3yIgInPc6
jazycích	jazyk	k1gInPc6
včetně	včetně	k7c2
češtiny	čeština	k1gFnSc2
čárka	čárka	k1gFnSc1
používá	používat	k5eAaImIp3nS
v	v	k7c6
číslech	číslo	k1gNnPc6
jako	jako	k8xS,k8xC
oddělovač	oddělovač	k1gInSc4
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
existují	existovat	k5eAaImIp3nP
varianty	varianta	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
používají	používat	k5eAaImIp3nP
jiný	jiný	k2eAgInSc4d1
znak	znak	k1gInSc4
pro	pro	k7c4
oddělování	oddělování	k1gNnSc4
položek	položka	k1gFnPc2
než	než	k8xS
čárku	čárka	k1gFnSc4
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
středník	středník	k1gInSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
tabulátor	tabulátor	k1gInSc4
(	(	kIx(
<g/>
taková	takový	k3xDgFnSc1
varianta	varianta	k1gFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
někdy	někdy	k6eAd1
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
TSV	TSV	kA
<g/>
,	,	kIx,
Tab-separated	Tab-separated	k1gMnSc1
values	values	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Variantu	variant	k1gInSc2
se	s	k7c7
středníkem	středník	k1gInSc7
(	(	kIx(
<g/>
ale	ale	k8xC
stále	stále	k6eAd1
pod	pod	k7c7
názvem	název	k1gInSc7
CSV	CSV	kA
<g/>
)	)	kIx)
používá	používat	k5eAaImIp3nS
např.	např.	kA
Microsoft	Microsoft	kA
Excel	Excel	kA
v	v	k7c6
české	český	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
Microsoft	Microsoft	kA
Windows	Windows	kA
(	(	kIx(
<g/>
řídí	řídit	k5eAaImIp3nS
se	se	k3xPyFc4
oddělovačem	oddělovač	k1gInSc7
zadaným	zadaný	k2eAgInSc7d1
v	v	k7c6
Místním	místní	k2eAgNnSc6d1
a	a	k8xC
jazykovém	jazykový	k2eAgNnSc6d1
nastavení	nastavení	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Díky	díky	k7c3
jednoduchosti	jednoduchost	k1gFnSc3
<g/>
,	,	kIx,
nenáročnosti	nenáročnost	k1gFnSc3
a	a	k8xC
čitelnosti	čitelnost	k1gFnSc3
i	i	k8xC
bez	bez	k7c2
specializovaného	specializovaný	k2eAgInSc2d1
softwaru	software	k1gInSc2
se	se	k3xPyFc4
tento	tento	k3xDgInSc4
formát	formát	k1gInSc4
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
výměnu	výměna	k1gFnSc4
informací	informace	k1gFnPc2
mezi	mezi	k7c7
různými	různý	k2eAgInPc7d1
systémy	systém	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
stejnému	stejný	k2eAgInSc3d1
účelu	účel	k1gInSc3
se	se	k3xPyFc4
dnes	dnes	k6eAd1
používá	používat	k5eAaImIp3nS
i	i	k9
modernější	moderní	k2eAgMnSc1d2
a	a	k8xC
univerzálnější	univerzální	k2eAgMnSc1d2
(	(	kIx(
<g/>
ale	ale	k8xC
složitější	složitý	k2eAgInPc4d2
<g/>
)	)	kIx)
formáty	formát	k1gInPc4
XML	XML	kA
či	či	k8xC
JSON	JSON	kA
<g/>
.	.	kIx.
</s>
<s>
Formální	formální	k2eAgFnPc1d1
náležitosti	náležitost	k1gFnPc1
</s>
<s>
Pro	pro	k7c4
tento	tento	k3xDgInSc4
formát	formát	k1gInSc4
neexistuje	existovat	k5eNaImIp3nS
specifikace	specifikace	k1gFnSc1
<g/>
,	,	kIx,
popis	popis	k1gInSc1
formátu	formát	k1gInSc2
se	se	k3xPyFc4
však	však	k9
nachází	nacházet	k5eAaImIp3nS
(	(	kIx(
<g/>
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
<g/>
)	)	kIx)
v	v	k7c6
RFC	RFC	kA
4180	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
data	datum	k1gNnPc4
ve	v	k7c6
formátu	formát	k1gInSc6
CSV	CSV	kA
je	být	k5eAaImIp3nS
registrován	registrovat	k5eAaBmNgMnS
MIME	mim	k1gMnSc5
typ	typ	k1gInSc1
text	text	k1gInSc4
<g/>
/	/	kIx~
<g/>
csv	csv	k?
<g/>
,	,	kIx,
v	v	k7c6
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
ale	ale	k9
objevují	objevovat	k5eAaImIp3nP
i	i	k9
další	další	k2eAgInPc4d1
typy	typ	k1gInPc4
jako	jako	k8xC,k8xS
application	application	k1gInSc4
<g/>
/	/	kIx~
<g/>
csv	csv	k?
<g/>
,	,	kIx,
text	text	k1gInSc1
<g/>
/	/	kIx~
<g/>
x-csv	x-csv	k1gInSc1
apod.	apod.	kA
</s>
<s>
Příklad	příklad	k1gInSc1
</s>
<s>
1995	#num#	k4
<g/>
OpelVectraklimatizace	OpelVectraklimatizace	k1gFnSc1
<g/>
,	,	kIx,
střešní	střešní	k2eAgNnSc1d1
okno	okno	k1gNnSc1
<g/>
45000	#num#	k4
</s>
<s>
1998	#num#	k4
<g/>
ŠkodaFelicia	ŠkodaFelicium	k1gNnPc1
"	"	kIx"
<g/>
Fun	Fun	k1gFnSc1
<g/>
"	"	kIx"
<g/>
80000	#num#	k4
</s>
<s>
2002	#num#	k4
<g/>
ŠkodaOctaviaklimatizace	ŠkodaOctaviaklimatizace	k1gFnSc1
<g/>
,	,	kIx,
ABSbouraná	ABSbouraný	k2eAgFnSc1d1
<g/>
70000	#num#	k4
</s>
<s>
Takovou	takový	k3xDgFnSc4
tabulku	tabulka	k1gFnSc4
lze	lze	k6eAd1
v	v	k7c6
CSV	CSV	kA
zapsat	zapsat	k5eAaPmF
následujícím	následující	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
:	:	kIx,
</s>
<s>
1995	#num#	k4
<g/>
,	,	kIx,
<g/>
Opel	opel	k1gInSc1
<g/>
,	,	kIx,
<g/>
Vectra	Vectra	k1gFnSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
<g/>
klimatizace	klimatizace	k1gFnSc1
<g/>
,	,	kIx,
střešní	střešní	k2eAgNnSc1d1
okno	okno	k1gNnSc1
<g/>
"	"	kIx"
<g/>
,45000	,45000	k4
</s>
<s>
1998	#num#	k4
<g/>
,	,	kIx,
<g/>
Škoda	škoda	k1gFnSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
<g/>
Felicia	felicia	k1gFnSc1
""	""	k?
<g/>
Fun	Fun	k1gFnSc1
<g/>
"""	"""	k?
<g/>
,	,	kIx,
<g/>
,80000	,80000	k4
</s>
<s>
2002	#num#	k4
<g/>
,	,	kIx,
<g/>
Škoda	škoda	k1gFnSc1
<g/>
,	,	kIx,
<g/>
Octavia	octavia	k1gFnSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
<g/>
klimatizace	klimatizace	k1gFnSc1
<g/>
,	,	kIx,
ABS	ABS	kA
</s>
<s>
bouraná	bouraný	k2eAgFnSc1d1
<g/>
"	"	kIx"
<g/>
,70000	,70000	k4
</s>
<s>
Na	na	k7c6
tomto	tento	k3xDgInSc6
příkladu	příklad	k1gInSc6
lze	lze	k6eAd1
vidět	vidět	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
hodnoty	hodnota	k1gFnPc1
obsahující	obsahující	k2eAgFnSc2d1
čárky	čárka	k1gFnSc2
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
uzavřeny	uzavřít	k5eAaPmNgFnP
do	do	k7c2
uvozovek	uvozovka	k1gFnPc2
<g/>
,	,	kIx,
uvozovky	uvozovka	k1gFnPc1
uvnitř	uvnitř	k7c2
hodnot	hodnota	k1gFnPc2
jsou	být	k5eAaImIp3nP
zdvojovány	zdvojován	k2eAgFnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
řádkové	řádkový	k2eAgInPc4d1
zlomy	zlom	k1gInPc4
lze	lze	k6eAd1
v	v	k7c6
hodnotách	hodnota	k1gFnPc6
přímo	přímo	k6eAd1
zapsat	zapsat	k5eAaPmF
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
hodnota	hodnota	k1gFnSc1
v	v	k7c6
uvozovkách	uvozovka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SHAFRANOVICH	SHAFRANOVICH	kA
<g/>
,	,	kIx,
Y.	Y.	kA
Common	Common	k1gInSc1
Format	Format	k1gInSc1
and	and	k?
MIME	mim	k1gMnSc5
Type	typ	k1gInSc5
for	forum	k1gNnPc2
CSV	CSV	kA
Files	Files	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IETF	IETF	kA
<g/>
,	,	kIx,
říjen	říjen	k1gInSc1
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.174	10.174	k4
<g/>
87	#num#	k4
<g/>
/	/	kIx~
<g/>
RFC	RFC	kA
<g/>
4180	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
RFC	RFC	kA
4180	#num#	k4
–	–	k?
Common	Common	k1gInSc1
Format	Format	k1gInSc1
and	and	k?
MIME	mim	k1gMnSc5
Type	typ	k1gInSc5
for	forum	k1gNnPc2
Comma-Separated	Comma-Separated	k1gMnSc1
Values	Values	k1gMnSc1
(	(	kIx(
<g/>
CSV	CSV	kA
<g/>
)	)	kIx)
Files	Files	k1gMnSc1
</s>
