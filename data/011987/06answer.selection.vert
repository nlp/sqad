<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
hospodářským	hospodářský	k2eAgInPc3d1	hospodářský
a	a	k8xC	a
kulturním	kulturní	k2eAgInPc3d1	kulturní
centrům	centr	k1gInPc3	centr
země	zem	k1gFnSc2	zem
a	a	k8xC	a
podle	podle	k7c2	podle
různých	různý	k2eAgInPc2d1	různý
průzkumů	průzkum	k1gInPc2	průzkum
mezi	mezi	k7c4	mezi
nejdražší	drahý	k2eAgNnPc4d3	nejdražší
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
kvalitou	kvalita	k1gFnSc7	kvalita
života	život	k1gInSc2	život
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dalších	další	k2eAgNnPc2d1	další
švýcarských	švýcarský	k2eAgNnPc2d1	švýcarské
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
poměrně	poměrně	k6eAd1	poměrně
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
