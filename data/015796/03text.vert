<s>
Klášťov	Klášťov	k1gInSc1
</s>
<s>
Je	být	k5eAaImIp3nS
navrženo	navržen	k2eAgNnSc1d1
rozdělení	rozdělení	k1gNnSc1
tohoto	tento	k3xDgInSc2
článku	článek	k1gInSc2
a	a	k8xC
přesunutí	přesunutí	k1gNnSc4
části	část	k1gFnSc2
obsahu	obsah	k1gInSc2
na	na	k7c4
nový	nový	k2eAgInSc4d1
název	název	k1gInSc4
Klášťov	Klášťov	k1gInSc1
(	(	kIx(
<g/>
hradiště	hradiště	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
návrhu	návrh	k1gInSc3
se	se	k3xPyFc4
můžete	moct	k5eAaImIp2nP
vyjádřit	vyjádřit	k5eAaPmF
v	v	k7c6
diskusi	diskuse	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
,	,	kIx,
doplnit	doplnit	k5eAaPmF
řádkové	řádkový	k2eAgFnPc4d1
reference	reference	k1gFnPc4
</s>
<s>
Klášťov	Klášťov	k1gInSc1
Čertův	čertův	k2eAgInSc1d1
kámen	kámen	k1gInSc1
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Klášťova	Klášťov	k1gInSc2
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
753	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
246	#num#	k4
m	m	kA
↓	↓	k?
ZJZ	ZJZ	kA
od	od	k7c2
Liptálu	Liptál	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Izolace	izolace	k1gFnPc1
</s>
<s>
10,2	10,2	k4
km	km	kA
→	→	k?
Radošov	Radošov	k1gInSc1
Seznamy	seznam	k1gInPc1
</s>
<s>
Nejprominentnější	prominentní	k2eAgFnPc1d3
hory	hora	k1gFnPc1
CZHory	CZHora	k1gFnSc2
Vizovické	vizovický	k2eAgFnPc1d1
vrchoviny	vrchovina	k1gFnPc1
#	#	kIx~
<g/>
1	#num#	k4
Poznámka	poznámka	k1gFnSc1
</s>
<s>
nejvýše	vysoce	k6eAd3,k6eAd1
položené	položený	k2eAgNnSc1d1
hradiště	hradiště	k1gNnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc4
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Vizovická	vizovický	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
<g/>
21	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
37	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Klášťov	Klášťov	k1gInSc1
</s>
<s>
Povodí	povodí	k1gNnSc1
</s>
<s>
Vlára	Vlára	k1gFnSc1
<g/>
,	,	kIx,
Dřevnice	Dřevnice	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Klášťov	Klášťov	k1gInSc1
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
Vizovické	vizovický	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
(	(	kIx(
<g/>
753	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
v	v	k7c6
poloze	poloha	k1gFnSc6
s	s	k7c7
pomístním	pomístní	k2eAgInSc7d1
názvem	název	k1gInSc7
„	„	k?
<g/>
Zámčisko	zámčisko	k1gNnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
k.	k.	k?
ú.	ú.	k?
Vysoké	vysoký	k2eAgFnSc2d1
Pole	pole	k1gFnSc2
a	a	k8xC
Bratřejov	Bratřejov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jižní	jižní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
vrcholu	vrchol	k1gInSc2
je	být	k5eAaImIp3nS
nádherný	nádherný	k2eAgInSc4d1
výhled	výhled	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradiště	Hradiště	k1gNnSc1
Klášťov	Klášťov	k1gInSc1
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1958	#num#	k4
evidovanou	evidovaný	k2eAgFnSc7d1
kulturní	kulturní	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Areálem	areál	k1gInSc7
Klášťova	Klášťov	k1gInSc2
vede	vést	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
naučný	naučný	k2eAgInSc1d1
okruh	okruh	k1gInSc1
dr	dr	kA
<g/>
.	.	kIx.
Jiřího	Jiří	k1gMnSc2
Kohoutka	Kohoutek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hora	hora	k1gFnSc1
je	být	k5eAaImIp3nS
opředena	opříst	k5eAaPmNgFnS
četnými	četný	k2eAgFnPc7d1
legendami	legenda	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Vrcholové	vrcholový	k2eAgFnSc3d1
partii	partie	k1gFnSc3
Klášťova	Klášťovo	k1gNnSc2
dominuje	dominovat	k5eAaImIp3nS
skalisko	skalisko	k1gNnSc4
Čertův	čertův	k2eAgInSc1d1
kámen	kámen	k1gInSc1
na	na	k7c6
nejvyšším	vysoký	k2eAgInSc6d3
bodě	bod	k1gInSc6
a	a	k8xC
studánka	studánka	k1gFnSc1
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
celém	celý	k2eAgInSc6d1
obvodu	obvod	k1gInSc6
hradiska	hradisko	k1gNnSc2
je	být	k5eAaImIp3nS
patrný	patrný	k2eAgInSc1d1
kamenný	kamenný	k2eAgInSc1d1
val	val	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
neuzavírá	uzavírat	k5eNaImIp3nS
prostor	prostor	k1gInSc4
docela	docela	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
podbíhá	podbíhat	k5eAaImIp3nS
a	a	k8xC
přesahuje	přesahovat	k5eAaImIp3nS
opačný	opačný	k2eAgInSc4d1
konec	konec	k1gInSc4
valu	val	k1gInSc2
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nP
tak	tak	k9
jakousi	jakýsi	k3yIgFnSc4
bránu	brána	k1gFnSc4
krytou	krytý	k2eAgFnSc4d1
z	z	k7c2
boku	bok	k1gInSc2
oběma	dva	k4xCgMnPc7
konci	konec	k1gInSc3
valů	val	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Val	val	k1gInSc4
má	mít	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
výšku	výška	k1gFnSc4
přibližně	přibližně	k6eAd1
jednoho	jeden	k4xCgInSc2
metru	metr	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
severovýchodní	severovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
dosahuje	dosahovat	k5eAaImIp3nS
výšky	výška	k1gFnSc2
až	až	k9
tří	tři	k4xCgInPc2
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
se	se	k3xPyFc4
ztrácí	ztrácet	k5eAaImIp3nS
úplně	úplně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obepíná	obepínat	k5eAaImIp3nS
plochu	plocha	k1gFnSc4
asi	asi	k9
2,1	2,1	k4
ha	ha	kA
ve	v	k7c6
tvaru	tvar	k1gInSc6
trojúhelníka	trojúhelník	k1gInSc2
se	s	k7c7
zaoblenými	zaoblený	k2eAgInPc7d1
vrcholy	vrchol	k1gInPc7
a	a	k8xC
s	s	k7c7
mírně	mírně	k6eAd1
zaoblenými	zaoblený	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
areálu	areál	k1gInSc2
jsou	být	k5eAaImIp3nP
pozůstatky	pozůstatek	k1gInPc1
uměle	uměle	k6eAd1
vytvořené	vytvořený	k2eAgInPc1d1
kamenné	kamenný	k2eAgInPc1d1
terasy	teras	k1gInPc1
s	s	k7c7
prohlubní	prohlubeň	k1gFnSc7
nejasné	jasný	k2eNgFnSc2d1
funkce	funkce	k1gFnSc2
<g/>
,	,	kIx,
jednu	jeden	k4xCgFnSc4
dobu	doba	k1gFnSc4
považované	považovaný	k2eAgFnSc2d1
za	za	k7c4
cisternu	cisterna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
terase	teras	k1gInSc6
se	se	k3xPyFc4
totiž	totiž	k9
nachází	nacházet	k5eAaImIp3nS
i	i	k9
několik	několik	k4yIc4
dalších	další	k2eAgNnPc2d1
zahloubení	zahloubení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Terasy	terasa	k1gFnSc2
upravené	upravený	k2eAgFnSc2d1
kamenným	kamenný	k2eAgNnSc7d1
dlážděním	dláždění	k1gNnSc7
jsou	být	k5eAaImIp3nP
v	v	k7c6
areálu	areál	k1gInSc6
hradiska	hradisko	k1gNnSc2
celkem	celkem	k6eAd1
dvě	dva	k4xCgFnPc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Opevněné	opevněný	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
na	na	k7c6
Klášťově	Klášťův	k2eAgMnSc6d1
zároveň	zároveň	k6eAd1
s	s	k7c7
prvním	první	k4xOgNnSc7
intenzivnějším	intenzivní	k2eAgNnSc7d2
osídlením	osídlení	k1gNnSc7
této	tento	k3xDgFnSc2
hornaté	hornatý	k2eAgFnSc2d1
části	část	k1gFnSc2
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
v	v	k7c6
pozdní	pozdní	k2eAgFnSc6d1
době	doba	k1gFnSc6
bronzové	bronzový	k2eAgFnSc6d1
<g/>
,	,	kIx,
na	na	k7c6
prahu	práh	k1gInSc6
1	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
Kr.	Kr.	k1gMnSc1
Již	již	k6eAd1
tehdy	tehdy	k6eAd1
bylo	být	k5eAaImAgNnS
obehnáno	obehnat	k5eAaPmNgNnS
mohutnou	mohutný	k2eAgFnSc7d1
hradbou	hradba	k1gFnSc7
s	s	k7c7
čelní	čelní	k2eAgFnSc7d1
kamennou	kamenný	k2eAgFnSc7d1
zdí	zeď	k1gFnSc7
a	a	k8xC
dřevohlinitým	dřevohlinitý	k2eAgNnSc7d1
tělesem	těleso	k1gNnSc7
<g/>
,	,	kIx,
širokým	široký	k2eAgNnSc7d1
přes	přes	k7c4
3	#num#	k4
metry	metr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnPc1d1
lidské	lidský	k2eAgFnPc1d1
aktivity	aktivita	k1gFnPc1
jsou	být	k5eAaImIp3nP
odtud	odtud	k6eAd1
zaznamenány	zaznamenat	k5eAaPmNgInP
z	z	k7c2
období	období	k1gNnSc2
raného	raný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
(	(	kIx(
<g/>
éra	éra	k1gFnSc1
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klášťov	Klášťov	k1gInSc1
byl	být	k5eAaImAgInS
nejvýše	vysoce	k6eAd3,k6eAd1
položeným	položený	k2eAgNnSc7d1
velkomoravským	velkomoravský	k2eAgNnSc7d1
hradištěm	hradiště	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Lidové	lidový	k2eAgFnPc1d1
pověsti	pověst	k1gFnPc1
</s>
<s>
Podle	podle	k7c2
pověstí	pověst	k1gFnPc2
tu	tu	k6eAd1
prý	prý	k9
kdysi	kdysi	k6eAd1
stával	stávat	k5eAaImAgInS
dřevěný	dřevěný	k2eAgInSc4d1
hrad	hrad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInPc1d1
bezdětný	bezdětný	k2eAgMnSc1d1
majitel	majitel	k1gMnSc1
měl	mít	k5eAaImAgMnS
údajně	údajně	k6eAd1
svůj	svůj	k3xOyFgInSc4
majetek	majetek	k1gInSc4
zakopat	zakopat	k5eAaPmF
do	do	k7c2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místní	místní	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
pak	pak	k6eAd1
bezúspěšně	bezúspěšně	k6eAd1
pokoušeli	pokoušet	k5eAaImAgMnP
poklad	poklad	k1gInSc4
ze	z	k7c2
země	zem	k1gFnSc2
vyzvednout	vyzvednout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poklad	poklad	k1gInSc1
byl	být	k5eAaImAgInS
však	však	k9
střežen	střežen	k2eAgInSc1d1
hady	had	k1gMnPc7
a	a	k8xC
štíry	štír	k1gMnPc7
ukrytými	ukrytý	k2eAgMnPc7d1
v	v	k7c6
trouchnivějících	trouchnivějící	k2eAgFnPc6d1
troskách	troska	k1gFnPc6
hradu	hrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poklad	poklad	k1gInSc1
v	v	k7c6
noci	noc	k1gFnSc6
„	„	k?
<g/>
vartovali	vartovat	k5eAaImAgMnP
<g/>
“	“	k?
též	též	k6eAd1
černí	černit	k5eAaImIp3nP
psi	pes	k1gMnPc1
se	s	k7c7
svítícíma	svítící	k2eAgNnPc7d1
očima	oko	k1gNnPc7
a	a	k8xC
zdivočelý	zdivočelý	k2eAgMnSc1d1
býk	býk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
na	na	k7c4
Klášťov	Klášťov	k1gInSc4
místní	místní	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
příliš	příliš	k6eAd1
neodvažovali	odvažovat	k5eNaImAgMnP
<g/>
,	,	kIx,
neboť	neboť	k8xC
na	na	k7c6
Klášťově	Klášťův	k2eAgFnSc6d1
našel	najít	k5eAaPmAgMnS
úkryt	úkryt	k1gInSc4
černokněžník	černokněžník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
tu	ten	k3xDgFnSc4
žil	žít	k5eAaImAgMnS
osamoceně	osamoceně	k6eAd1
jako	jako	k9
poustevník	poustevník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černokněžník	černokněžník	k1gMnSc1
byl	být	k5eAaImAgMnS
celkem	celkem	k6eAd1
neškodný	škodný	k2eNgMnSc1d1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
zlatý	zlatý	k2eAgInSc4d1
„	„	k?
<g/>
čagan	čagan	k1gInSc1
<g/>
“	“	k?
zavěšený	zavěšený	k2eAgInSc1d1
na	na	k7c6
vysoké	vysoký	k2eAgFnSc6d1
jedli	jedle	k1gFnSc6
způsobil	způsobit	k5eAaPmAgMnS
náhodnému	náhodný	k2eAgMnSc3d1
poutníkovi	poutník	k1gMnSc3
ztrátu	ztráta	k1gFnSc4
orientace	orientace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
skupině	skupina	k1gFnSc6
kamenů	kámen	k1gInPc2
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Klášťova	Klášťov	k1gInSc2
odedávna	odedávna	k6eAd1
vypráví	vyprávět	k5eAaImIp3nS
lidová	lidový	k2eAgFnSc1d1
pověst	pověst	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
kameny	kámen	k1gInPc4
tam	tam	k6eAd1
donesl	donést	k5eAaPmAgMnS
čert	čert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
nich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
údajně	údajně	k6eAd1
otisk	otisk	k1gInSc4
čertova	čertův	k2eAgNnSc2d1
ramene	rameno	k1gNnSc2
<g/>
,	,	kIx,
hlavy	hlava	k1gFnSc2
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
kopyta	kopyto	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
nedalekém	daleký	k2eNgNnSc6d1
návrší	návrší	k1gNnSc6
směrem	směr	k1gInSc7
k	k	k7c3
Pozděchovu	Pozděchův	k2eAgInSc3d1
zvaném	zvaný	k2eAgInSc6d1
Smolný	smolný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
stával	stávat	k5eAaImAgInS
údajně	údajně	k6eAd1
zámek	zámek	k1gInSc1
s	s	k7c7
jezírkem	jezírko	k1gNnSc7
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
byli	být	k5eAaImAgMnP
pochováváni	pochováván	k2eAgMnPc1d1
velcí	velký	k2eAgMnPc1d1
hříšníci	hříšník	k1gMnPc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nestrašívali	strašívat	k5eNaImAgMnP
na	na	k7c6
běžných	běžný	k2eAgInPc6d1
hřbitovech	hřbitov	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Pověsti	pověst	k1gFnPc1
o	o	k7c6
tajemném	tajemný	k2eAgInSc6d1
Klášťově	Klášťův	k2eAgInSc6d1
se	se	k3xPyFc4
samozřejmě	samozřejmě	k6eAd1
dostaly	dostat	k5eAaPmAgFnP
i	i	k9
do	do	k7c2
sbírek	sbírka	k1gFnPc2
pátera	páter	k1gMnSc2
Františka	František	k1gMnSc2
Müllera	Müller	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
v	v	k7c6
Pověstech	pověst	k1gFnPc6
z	z	k7c2
Vizovických	vizovický	k2eAgFnPc2d1
hor	hora	k1gFnPc2
převyprávěla	převyprávět	k5eAaPmAgFnS
Eva	Eva	k1gFnSc1
Eliášová	Eliášová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
výzkumu	výzkum	k1gInSc2
</s>
<s>
První	první	k4xOgMnPc1
ne	ne	k9
příliš	příliš	k6eAd1
přesný	přesný	k2eAgInSc4d1
popis	popis	k1gInSc4
lokality	lokalita	k1gFnSc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
pera	pero	k1gNnSc2
J.	J.	kA
Kučery	kučera	k1gFnSc2
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1890	#num#	k4
(	(	kIx(
<g/>
ČVSMO	ČVSMO	kA
VII	VII	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
v	v	k7c6
roce	rok	k1gInSc6
1892	#num#	k4
se	se	k3xPyFc4
o	o	k7c6
lokalitě	lokalita	k1gFnSc6
zmiňoval	zmiňovat	k5eAaImAgMnS
E.	E.	kA
Peck	Peck	k1gMnSc1
(	(	kIx(
<g/>
Okresní	okresní	k2eAgNnSc1d1
hejtmanství	hejtmanství	k1gNnSc1
holešovské	holešovský	k2eAgInPc1d1
<g/>
,	,	kIx,
107	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ovšem	ovšem	k9
ve	v	k7c6
formě	forma	k1gFnSc6
lidových	lidový	k2eAgFnPc2d1
pověstí	pověst	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Přesnější	přesný	k2eAgInSc4d2
popis	popis	k1gInSc4
lokality	lokalita	k1gFnSc2
podal	podat	k5eAaPmAgMnS
Inocenc	Inocenc	k1gMnSc1
Ladislav	Ladislav	k1gMnSc1
Červinka	Červinka	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
(	(	kIx(
<g/>
Slované	Slovan	k1gMnPc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
říše	říše	k1gFnSc1
Velkomoravská	velkomoravský	k2eAgFnSc1d1
<g/>
,	,	kIx,
s.	s.	k?
91	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
lokalitu	lokalita	k1gFnSc4
interpretoval	interpretovat	k5eAaBmAgMnS
jako	jako	k9
velkomoravské	velkomoravský	k2eAgNnSc4d1
útočištné	útočištný	k2eAgNnSc4d1
hradisko	hradisko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nalezl	nalézt	k5eAaBmAgMnS,k5eAaPmAgMnS
zde	zde	k6eAd1
několik	několik	k4yIc4
kamenných	kamenný	k2eAgInPc2d1
žernovů	žernov	k1gInPc2
(	(	kIx(
<g/>
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
uložené	uložený	k2eAgInPc1d1
zřejmě	zřejmě	k6eAd1
v	v	k7c6
muzeu	muzeum	k1gNnSc6
ve	v	k7c6
Valašských	valašský	k2eAgInPc6d1
Kloboukách	Klobouky	k1gInPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
pod	pod	k7c7
vlivem	vliv	k1gInSc7
Emanuela	Emanuel	k1gMnSc2
Šimka	Šimek	k1gMnSc2
přiklonil	přiklonit	k5eAaPmAgInS
k	k	k7c3
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
mohlo	moct	k5eAaImAgNnS
jednat	jednat	k5eAaImF
o	o	k7c4
keltské	keltský	k2eAgNnSc4d1
hradiště	hradiště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
chybný	chybný	k2eAgInSc1d1
názor	názor	k1gInSc1
však	však	k9
nebyl	být	k5eNaImAgInS
potvrzen	potvrdit	k5eAaPmNgInS
ani	ani	k8xC
při	při	k7c6
pozdějších	pozdní	k2eAgInPc6d2
archeologických	archeologický	k2eAgInPc6d1
výzkumech	výzkum	k1gInPc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Přesto	přesto	k8xC
po	po	k7c4
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
figuroval	figurovat	k5eAaImAgMnS
v	v	k7c6
povědomí	povědomí	k1gNnSc6
širší	široký	k2eAgFnSc2d2
laické	laický	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
v	v	k7c6
některých	některý	k3yIgInPc6
turistických	turistický	k2eAgInPc6d1
informačních	informační	k2eAgInPc6d1
materiálech	materiál	k1gInPc6
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
vytvořil	vytvořit	k5eAaPmAgMnS
archeolog	archeolog	k1gMnSc1
zlínského	zlínský	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
Vít	Vít	k1gMnSc1
Dohnal	Dohnal	k1gMnSc1
nákres	nákres	k1gInSc4
hradiska	hradisko	k1gNnSc2
a	a	k8xC
provedl	provést	k5eAaPmAgInS
zde	zde	k6eAd1
menší	malý	k2eAgFnSc4d2
sondáž	sondáž	k1gFnSc4
na	na	k7c6
ploše	plocha	k1gFnSc6
1,5	1,5	k4
x	x	k?
2	#num#	k4
m	m	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
půlmetrové	půlmetrový	k2eAgFnSc6d1
hloubce	hloubka	k1gFnSc6
narazil	narazit	k5eAaPmAgMnS
na	na	k7c4
deset	deset	k4xCc4
keramických	keramický	k2eAgInPc2d1
zlomků	zlomek	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
jeden	jeden	k4xCgInSc1
pocházel	pocházet	k5eAaImAgInS
skutečně	skutečně	k6eAd1
z	z	k7c2
doby	doba	k1gFnSc2
velkomoravské	velkomoravský	k2eAgFnSc2d1
a	a	k8xC
zbytek	zbytek	k1gInSc1
patřil	patřit	k5eAaImAgInS
slezskoplatěnické	slezskoplatěnický	k2eAgFnSc3d1
fázi	fáze	k1gFnSc3
kultury	kultura	k1gFnSc2
lidu	lid	k1gInSc2
popelnicových	popelnicový	k2eAgFnPc2d1
polí	pole	k1gFnPc2
z	z	k7c2
doby	doba	k1gFnSc2
prvního	první	k4xOgNnSc2
osídlení	osídlení	k1gNnPc2
hradiska	hradisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
zde	zde	k6eAd1
provedl	provést	k5eAaPmAgMnS
archeolog	archeolog	k1gMnSc1
PhDr.	PhDr.	kA
Jiří	Jiří	k1gMnSc1
Kohoutek	Kohoutek	k1gMnSc1
povrchový	povrchový	k2eAgInSc4d1
průzkum	průzkum	k1gInSc4
spojený	spojený	k2eAgInSc4d1
s	s	k7c7
mikrosondážemi	mikrosondáž	k1gFnPc7
a	a	k8xC
získal	získat	k5eAaPmAgMnS
obdobný	obdobný	k2eAgInSc4d1
počet	počet	k1gInSc4
keramických	keramický	k2eAgInPc2d1
zlomků	zlomek	k1gInPc2
většinou	většinou	k6eAd1
z	z	k7c2
doby	doba	k1gFnSc2
halštatské	halštatský	k2eAgFnSc2d1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
několik	několik	k4yIc4
z	z	k7c2
doby	doba	k1gFnSc2
slovanského	slovanský	k2eAgNnSc2d1
osídlení	osídlení	k1gNnSc2
doby	doba	k1gFnSc2
velkomoravské	velkomoravský	k2eAgFnSc2d1
(	(	kIx(
<g/>
střední	střední	k2eAgFnSc2d1
doby	doba	k1gFnSc2
hradištní	hradištní	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
na	na	k7c6
Klášťově	Klášťův	k2eAgFnSc6d1
Jindra	Jindra	k1gMnSc1
Nekvasil	kvasit	k5eNaImAgInS
z	z	k7c2
Archeologického	archeologický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
ČSAV	ČSAV	kA
v	v	k7c6
Brně	Brno	k1gNnSc6
našel	najít	k5eAaPmAgMnS
ve	v	k7c6
vývratu	vývrat	k1gInSc6
padlého	padlý	k2eAgInSc2d1
stromu	strom	k1gInSc2
část	část	k1gFnSc1
nádoby	nádoba	k1gFnSc2
z	z	k7c2
období	období	k1gNnSc2
kultury	kultura	k1gFnSc2
lidu	lid	k1gInSc2
popelnicových	popelnicový	k2eAgFnPc2d1
polí	pole	k1gFnPc2
doby	doba	k1gFnSc2
halštatské	halštatský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
základě	základ	k1gInSc6
dosavadního	dosavadní	k2eAgInSc2d1
stavu	stav	k1gInSc2
bádání	bádání	k1gNnSc2
však	však	k9
i	i	k9
u	u	k7c2
dr	dr	kA
<g/>
.	.	kIx.
Kohoutka	Kohoutek	k1gMnSc2
stále	stále	k6eAd1
ještě	ještě	k6eAd1
převládal	převládat	k5eAaImAgInS
názor	názor	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
lokalita	lokalita	k1gFnSc1
byla	být	k5eAaImAgFnS
využívána	využívat	k5eAaImNgFnS,k5eAaPmNgFnS
spíše	spíše	k9
až	až	k9
v	v	k7c6
mladší	mladý	k2eAgFnSc6d2
době	doba	k1gFnSc6
hradištní	hradištní	k2eAgFnSc6d1
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
možnost	možnost	k1gFnSc4
dřívějšího	dřívější	k2eAgNnSc2d1
slovanského	slovanský	k2eAgNnSc2d1
osídlení	osídlení	k1gNnSc2
dr	dr	kA
<g/>
.	.	kIx.
Kohoutek	Kohoutek	k1gMnSc1
nevylučoval	vylučovat	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
však	však	k9
chybělo	chybět	k5eAaImAgNnS
více	hodně	k6eAd2
důkazů	důkaz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osídlení	osídlení	k1gNnSc1
střední	střední	k2eAgFnSc2d1
doby	doba	k1gFnSc2
hradištní	hradištní	k2eAgFnSc2d1
se	se	k3xPyFc4
nepovažovalo	považovat	k5eNaImAgNnS
za	za	k7c4
dostatečně	dostatečně	k6eAd1
spolehlivě	spolehlivě	k6eAd1
prokázané	prokázaný	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Názory	názor	k1gInPc1
na	na	k7c4
stáří	stáří	k1gNnSc4
lokality	lokalita	k1gFnSc2
tak	tak	k9
stále	stále	k6eAd1
kolísaly	kolísat	k5eAaImAgFnP
mezi	mezi	k7c7
střední	střední	k2eAgFnSc7d1
a	a	k8xC
mladší	mladý	k2eAgFnSc7d2
dobou	doba	k1gFnSc7
hradištní	hradištní	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Novodobá	novodobý	k2eAgFnSc1d1
rozsáhlejší	rozsáhlý	k2eAgFnSc1d2
výzkumná	výzkumný	k2eAgFnSc1d1
akce	akce	k1gFnSc1
této	tento	k3xDgFnSc2
tajemné	tajemný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
,	,	kIx,
podnícená	podnícený	k2eAgNnPc4d1
amatérským	amatérský	k2eAgInSc7d1
nálezem	nález	k1gInSc7
z	z	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
,	,	kIx,
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
květnu	květen	k1gInSc6
2005	#num#	k4
na	na	k7c4
popud	popud	k1gInSc4
starosty	starosta	k1gMnSc2
obce	obec	k1gFnSc2
Vysoké	vysoký	k2eAgFnSc2d1
Pole	pole	k1gFnSc2
Josefa	Josef	k1gMnSc2
Zichy	Zich	k1gMnPc7
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
katastru	katastr	k1gInSc6
lokalita	lokalita	k1gFnSc1
leží	ležet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
dnech	den	k1gInPc6
8	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
geodeti	geodet	k1gMnPc1
Ústavu	ústav	k1gInSc2
archeologické	archeologický	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
Brno	Brno	k1gNnSc1
nově	nova	k1gFnSc3
zaměřili	zaměřit	k5eAaPmAgMnP
lokalitu	lokalita	k1gFnSc4
a	a	k8xC
ve	v	k7c6
dnech	den	k1gInPc6
23	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
27	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
archeolog	archeolog	k1gMnSc1
PhDr.	PhDr.	kA
Jiří	Jiří	k1gMnSc1
Kohoutek	Kohoutek	k1gMnSc1
provedl	provést	k5eAaPmAgMnS
několik	několik	k4yIc4
zjišťovacích	zjišťovací	k2eAgFnPc2d1
sond	sonda	k1gFnPc2
menšího	malý	k2eAgInSc2d2
rozsahu	rozsah	k1gInSc2
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
hradiska	hradisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
lokalita	lokalita	k1gFnSc1
není	být	k5eNaImIp3nS
příliš	příliš	k6eAd1
dobře	dobře	k6eAd1
dostupná	dostupný	k2eAgFnSc1d1
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
objevy	objev	k1gInPc1
značně	značně	k6eAd1
překvapivé	překvapivý	k2eAgInPc1d1
<g/>
,	,	kIx,
šokující	šokující	k2eAgInPc1d1
zejména	zejména	k9
množstvím	množství	k1gNnSc7
hromadných	hromadný	k2eAgInPc2d1
skladů	sklad	k1gInPc2
–	–	k?
tzv.	tzv.	kA
depotů	depot	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozoruhodný	pozoruhodný	k2eAgInSc1d1
byl	být	k5eAaImAgInS
dosud	dosud	k6eAd1
nevídaný	vídaný	k2eNgInSc1d1
depot	depot	k1gInSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
51	#num#	k4
předmětů	předmět	k1gInPc2
ukrytých	ukrytý	k2eAgInPc2d1
pod	pod	k7c7
žernovem	žernov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dalšího	další	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
bylo	být	k5eAaImAgNnS
získáno	získat	k5eAaPmNgNnS
dalších	další	k2eAgNnPc6d1
více	hodně	k6eAd2
jak	jak	k8xS,k8xC
100	#num#	k4
železných	železný	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
<g/>
,	,	kIx,
nástrojů	nástroj	k1gInPc2
<g/>
,	,	kIx,
zbraní	zbraň	k1gFnPc2
i	i	k8xC
ozdob	ozdoba	k1gFnPc2
z	z	k7c2
doby	doba	k1gFnSc2
halštatské	halštatský	k2eAgFnSc2d1
(	(	kIx(
<g/>
např.	např.	kA
součásti	součást	k1gFnSc2
výbavy	výbava	k1gFnSc2
jezdecké	jezdecký	k2eAgFnSc2d1
bojovnické	bojovnický	k2eAgFnSc2d1
družiny	družina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dále	daleko	k6eAd2
předměty	předmět	k1gInPc1
charakteristické	charakteristický	k2eAgInPc1d1
pro	pro	k7c4
dobu	doba	k1gFnSc4
velkomoravskou	velkomoravský	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nechyběly	chybět	k5eNaImAgInP
ani	ani	k8xC
kousky	kousek	k1gInPc4
velkomoravské	velkomoravský	k2eAgFnSc2d1
keramiky	keramika	k1gFnSc2
<g/>
,	,	kIx,
zdobené	zdobený	k2eAgNnSc1d1
charakteristickou	charakteristický	k2eAgFnSc7d1
vlnicí	vlnice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
bylo	být	k5eAaImAgNnS
definitivně	definitivně	k6eAd1
potvrzeno	potvrzen	k2eAgNnSc1d1
velkomoravské	velkomoravský	k2eAgNnSc1d1
osídlení	osídlení	k1gNnSc1
lokality	lokalita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Archeolog	archeolog	k1gMnSc1
dr	dr	kA
<g/>
.	.	kIx.
Kohoutek	Kohoutek	k1gMnSc1
se	se	k3xPyFc4
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Moravané	Moravan	k1gMnPc1
tuto	tento	k3xDgFnSc4
lokalitu	lokalita	k1gFnSc4
využívali	využívat	k5eAaPmAgMnP,k5eAaImAgMnP
jako	jako	k8xC,k8xS
kultovní	kultovní	k2eAgNnSc4d1
místo	místo	k1gNnSc4
a	a	k8xC
železné	železný	k2eAgInPc1d1
předměty	předmět	k1gInPc1
představovaly	představovat	k5eAaImAgInP
oběť	oběť	k1gFnSc4
bohům	bůh	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
cca	cca	kA
20	#num#	k4
km	km	kA
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Klášťova	Klášťov	k1gInSc2
se	se	k3xPyFc4
totiž	totiž	k9
vyskytuje	vyskytovat	k5eAaImIp3nS
několik	několik	k4yIc1
slovanských	slovanský	k2eAgFnPc2d1
pohřebních	pohřební	k2eAgFnPc2d1
mohyl	mohyla	k1gFnPc2
zhruba	zhruba	k6eAd1
na	na	k7c6
čáře	čára	k1gFnSc6
Luhačovice	Luhačovice	k1gFnPc1
–	–	k?
les	les	k1gInSc1
Obora	obora	k1gFnSc1
<g/>
,	,	kIx,
Rudimov	Rudimov	k1gInSc1
–	–	k?
trať	trať	k1gFnSc1
Kamenná	kamenný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Bojkovice	Bojkovice	k1gFnPc1
–	–	k?
trať	trať	k1gFnSc1
Hradská	hradská	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
dr	dr	kA
<g/>
.	.	kIx.
Jiřího	Jiří	k1gMnSc2
Kohoutka	Kohoutek	k1gMnSc2
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
jít	jít	k5eAaImF
o	o	k7c4
první	první	k4xOgNnSc4
kultovní	kultovní	k2eAgNnSc4d1
místo	místo	k1gNnSc4
starých	starý	k2eAgInPc2d1
Slovanů	Slovan	k1gInPc2
na	na	k7c6
území	území	k1gNnSc6
Moravy	Morava	k1gFnSc2
a	a	k8xC
Čech	Čechy	k1gFnPc2
z	z	k7c2
období	období	k1gNnSc2
před	před	k7c7
nástupem	nástup	k1gInSc7
křesťanství	křesťanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
názoru	názor	k1gInSc2
hora	hora	k1gFnSc1
mohla	moct	k5eAaImAgFnS
mít	mít	k5eAaImF
významný	významný	k2eAgInSc4d1
sakrální	sakrální	k2eAgInSc4d1
účel	účel	k1gInSc4
<g/>
,	,	kIx,
předpokládal	předpokládat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgNnP
zasvěcena	zasvěcen	k2eAgNnPc1d1
bohu	bůh	k1gMnSc3
hromu	hrom	k1gInSc2
<g/>
,	,	kIx,
bouře	bouř	k1gFnSc2
a	a	k8xC
ohně	oheň	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
Perun	perun	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kultiště	Kultiště	k1gNnSc1
však	však	k8xC
nalezeno	nalezen	k2eAgNnSc1d1
nebylo	být	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozdějších	pozdní	k2eAgFnPc6d2
tradicích	tradice	k1gFnPc6
údajně	údajně	k6eAd1
neměla	mít	k5eNaImAgFnS
hora	hora	k1gFnSc1
nejlepší	dobrý	k2eAgFnSc4d3
pověst	pověst	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
názoru	názor	k1gInSc2
dr	dr	kA
<g/>
.	.	kIx.
Kohoutka	Kohoutek	k1gMnSc2
se	se	k3xPyFc4
církev	církev	k1gFnSc1
snažila	snažit	k5eAaImAgFnS
potlačit	potlačit	k5eAaPmF
staré	starý	k2eAgInPc4d1
pohanské	pohanský	k2eAgInPc4d1
kulty	kult	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
horu	hora	k1gFnSc4
zasvětila	zasvětit	k5eAaPmAgFnS
ďáblu	ďábel	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
zdůvodnil	zdůvodnit	k5eAaPmAgInS
existenci	existence	k1gFnSc4
četných	četný	k2eAgFnPc2d1
pověstí	pověst	k1gFnPc2
o	o	k7c6
výskytu	výskyt	k1gInSc6
čertů	čert	k1gMnPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
nadpřirozených	nadpřirozený	k2eAgFnPc2d1
temných	temný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
,	,	kIx,
podle	podle	k7c2
nichž	jenž	k3xRgInPc2
není	být	k5eNaImIp3nS
radno	radno	k6eAd1
Klášťov	Klášťov	k1gInSc4
navštěvovat	navštěvovat	k5eAaImF
v	v	k7c4
kteroukoliv	kterýkoliv	k3yIgFnSc4
denní	denní	k2eAgFnSc4d1
nebo	nebo	k8xC
noční	noční	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
pohanští	pohanský	k2eAgMnPc1d1
předkové	předek	k1gMnPc1
obětovali	obětovat	k5eAaBmAgMnP
část	část	k1gFnSc4
úrody	úroda	k1gFnSc2
nebo	nebo	k8xC
úlovku	úlovek	k1gInSc2
jako	jako	k8xS,k8xC
úlitbu	úlitba	k1gFnSc4
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
ne	ne	k9
všechno	všechen	k3xTgNnSc1
a	a	k8xC
už	už	k6eAd1
vůbec	vůbec	k9
ne	ne	k9
výrobní	výrobní	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
<g/>
,	,	kIx,
prozaičtější	prozaický	k2eAgFnSc7d2
a	a	k8xC
pravděpodobnější	pravděpodobný	k2eAgFnSc7d2
variantou	varianta	k1gFnSc7
se	se	k3xPyFc4
jeví	jevit	k5eAaImIp3nS
využití	využití	k1gNnSc1
hradiska	hradisko	k1gNnSc2
coby	coby	k?
ideální	ideální	k2eAgFnSc2d1
zásobárny	zásobárna	k1gFnSc2
materiálu	materiál	k1gInSc2
k	k	k7c3
obchodním	obchodní	k2eAgInPc3d1
účelům	účel	k1gInPc3
nebo	nebo	k8xC
jako	jako	k9
vhodného	vhodný	k2eAgInSc2d1
úkrytu	úkryt	k1gInSc2
na	na	k7c4
tu	ten	k3xDgFnSc4
dobu	doba	k1gFnSc4
cenných	cenný	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
v	v	k7c6
době	doba	k1gFnSc6
nebezpečí	nebezpečí	k1gNnSc1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
o	o	k7c6
tom	ten	k3xDgNnSc6
svědčí	svědčit	k5eAaImIp3nS
způsob	způsob	k1gInSc1
uložení	uložení	k1gNnSc2
železných	železný	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
a	a	k8xC
suroviny	surovina	k1gFnSc2
-	-	kIx~
překrytí	překrytí	k1gNnSc1
plochým	plochý	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
<g/>
,	,	kIx,
např.	např.	kA
žernovem	žernov	k1gInSc7
<g/>
,	,	kIx,
plochým	plochý	k2eAgInSc7d1
kamenem	kámen	k1gInSc7
nebo	nebo	k8xC
mělkou	mělký	k2eAgFnSc7d1
mísou	mísa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Slavičínsku	Slavičínsko	k1gNnSc6
<g/>
,	,	kIx,
Bojkovicku	Bojkovicko	k1gNnSc6
a	a	k8xC
Luhačovicku	Luhačovicko	k1gNnSc6
se	se	k3xPyFc4
totiž	totiž	k9
vyskytovala	vyskytovat	k5eAaImAgFnS
naleziště	naleziště	k1gNnSc4
poměrně	poměrně	k6eAd1
chudé	chudý	k2eAgFnPc1d1
železné	železný	k2eAgFnPc1d1
rudy	ruda	k1gFnPc1
–	–	k?
pelosideritu	pelosiderit	k1gInSc2
<g/>
,	,	kIx,
tato	tento	k3xDgFnSc1
oblast	oblast	k1gFnSc1
byla	být	k5eAaImAgFnS
i	i	k9
oblastí	oblast	k1gFnSc7
severní	severní	k2eAgFnSc2d1
větve	větev	k1gFnSc2
obchodního	obchodní	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
Pomoraví	Pomoraví	k1gNnSc2
s	s	k7c7
Povážím	Pováží	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedaleko	nedaleko	k7c2
Klášťova	Klášťovo	k1gNnSc2
směrem	směr	k1gInSc7
na	na	k7c6
severozád	severozáda	k1gFnPc2
v	v	k7c6
trati	trať	k1gFnSc6
Padělky	padělek	k1gInPc4
u	u	k7c2
Chrastěšova	Chrastěšův	k2eAgInSc2d1
byly	být	k5eAaImAgFnP
objeveny	objevit	k5eAaPmNgInP
středohradištní	středohradištní	k2eAgInPc1d1
objekty	objekt	k1gInPc1
patrně	patrně	k6eAd1
výrobního	výrobní	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
,	,	kIx,
související	související	k2eAgFnSc1d1
s	s	k7c7
úpravou	úprava	k1gFnSc7
železné	železný	k2eAgFnSc2d1
rudy	ruda	k1gFnSc2
nebo	nebo	k8xC
jejím	její	k3xOp3gNnSc7
následným	následný	k2eAgNnSc7d1
zpracováním	zpracování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
základě	základ	k1gInSc6
překvapivých	překvapivý	k2eAgInPc2d1
nálezů	nález	k1gInPc2
byla	být	k5eAaImAgFnS
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
provedena	proveden	k2eAgFnSc1d1
další	další	k2eAgFnSc1d1
důkladnější	důkladný	k2eAgFnSc1d2
sondáž	sondáž	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
dokončeno	dokončit	k5eAaPmNgNnS
geodetické	geodetický	k2eAgNnSc1d1
zaměření	zaměření	k1gNnSc1
lokality	lokalita	k1gFnSc2
pracovníky	pracovník	k1gMnPc4
z	z	k7c2
ÚAPP	ÚAPP	kA
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastní	k2eAgInSc1d1
sondážní	sondážní	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
proběhl	proběhnout	k5eAaPmAgInS
ve	v	k7c6
dnech	den	k1gInPc6
5	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
až	až	k9
17	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
i	i	k9
s	s	k7c7
depotem	depot	k1gInSc7
z	z	k7c2
předchozího	předchozí	k2eAgInSc2d1
roku	rok	k1gInSc2
dosáhl	dosáhnout	k5eAaPmAgInS
počet	počet	k1gInSc1
celkově	celkově	k6eAd1
nalezených	nalezený	k2eAgInPc2d1
železných	železný	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
na	na	k7c6
Klášťově	Klášťův	k2eAgFnSc6d1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
počtu	počet	k1gInSc2
přes	přes	k7c4
400	#num#	k4
kusů	kus	k1gInPc2
jednotlivých	jednotlivý	k2eAgInPc2d1
exemplářů	exemplář	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
etapa	etapa	k1gFnSc1
zjišťovacího	zjišťovací	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
<g/>
,	,	kIx,
zaměřená	zaměřený	k2eAgFnSc1d1
na	na	k7c4
dokončení	dokončení	k1gNnSc4
sondáží	sondáž	k1gFnPc2
<g/>
,	,	kIx,
pokračovala	pokračovat	k5eAaImAgFnS
ve	v	k7c6
vnitřním	vnitřní	k2eAgInSc6d1
areálu	areál	k1gInSc6
hradiska	hradisko	k1gNnSc2
v	v	k7c6
průběhu	průběh	k1gInSc6
června	červen	k1gInSc2
až	až	k8xS
srpna	srpen	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
objeveny	objevit	k5eAaPmNgInP
4	#num#	k4
nové	nový	k2eAgInPc4d1
depoty	depot	k1gInPc4
<g/>
,	,	kIx,
největší	veliký	k2eAgInSc4d3
z	z	k7c2
nich	on	k3xPp3gInPc2
obsahoval	obsahovat	k5eAaImAgInS
40	#num#	k4
železných	železný	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgFnPc7
byly	být	k5eAaImAgFnP
většinou	většina	k1gFnSc7
zastoupeny	zastoupen	k2eAgFnPc1d1
zemědělské	zemědělský	k2eAgFnSc3d1
i	i	k9
jiné	jiný	k2eAgInPc4d1
druhy	druh	k1gInPc4
nástrojů	nástroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
depotů	depot	k1gInPc2
dosáhl	dosáhnout	k5eAaPmAgInS
desítky	desítka	k1gFnPc4
a	a	k8xC
počet	počet	k1gInSc4
jednotlivých	jednotlivý	k2eAgInPc2d1
železných	železný	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
přesáhl	přesáhnout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
již	již	k9
700	#num#	k4
exemplářů	exemplář	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
započaté	započatý	k2eAgFnSc6d1
práci	práce	k1gFnSc6
dr	dr	kA
<g/>
.	.	kIx.
Kohoutka	Kohoutek	k1gMnSc4
pokračuje	pokračovat	k5eAaImIp3nS
PhDr.	PhDr.	kA
RNDr.	RNDr.	kA
Jana	Jana	k1gFnSc1
Langová	Langová	k1gFnSc1
a	a	k8xC
Ivan	Ivan	k1gMnSc1
Čižmář	čižmář	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
nalezl	nalézt	k5eAaBmAgInS,k5eAaPmAgInS
dalších	další	k2eAgMnPc2d1
asi	asi	k9
300	#num#	k4
nejrůznějších	různý	k2eAgInPc2d3
kovových	kovový	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Preventivním	preventivní	k2eAgInSc7d1
archeologickým	archeologický	k2eAgInSc7d1
průzkumem	průzkum	k1gInSc7
hradiska	hradisko	k1gNnSc2
Klášťov	Klášťov	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
vyzvednout	vyzvednout	k5eAaPmF
a	a	k8xC
geodeticky	geodeticky	k6eAd1
zaměřit	zaměřit	k5eAaPmF
přes	přes	k7c4
200	#num#	k4
železných	železný	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
ze	z	k7c2
středohradištního	středohradištní	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgFnP
vyzvednuty	vyzvednout	k5eAaPmNgInP
také	také	k9
starší	starý	k2eAgInPc1d2
předměty	předmět	k1gInPc1
ze	z	k7c2
slezské	slezský	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
lužické	lužický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
popelnicových	popelnicový	k2eAgNnPc2d1
polí	pole	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Jedinečné	jedinečný	k2eAgInPc4d1
archeologické	archeologický	k2eAgInPc4d1
objevy	objev	k1gInPc4
z	z	k7c2
doby	doba	k1gFnSc2
starých	starý	k2eAgInPc2d1
Slovanů	Slovan	k1gInPc2
jsou	být	k5eAaImIp3nP
tedy	tedy	k9
zastoupeny	zastoupit	k5eAaPmNgFnP
téměř	téměř	k6eAd1
tisícovkou	tisícovka	k1gFnSc7
železných	železný	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
zahrnujících	zahrnující	k2eAgInPc2d1
nejrůznější	různý	k2eAgInPc4d3
typy	typ	k1gInPc4
nástrojů	nástroj	k1gInPc2
či	či	k8xC
součásti	součást	k1gFnSc2
výstroje	výstroj	k1gFnSc2
jezdce	jezdec	k1gMnSc2
a	a	k8xC
koně	kůň	k1gMnSc2
<g/>
,	,	kIx,
především	především	k9
však	však	k9
nález	nález	k1gInSc4
celkově	celkově	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
deseti	deset	k4xCc2
depotů	depot	k1gInPc2
<g/>
,	,	kIx,
obsahujících	obsahující	k2eAgFnPc2d1
často	často	k6eAd1
unikátní	unikátní	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
ze	z	k7c2
železa	železo	k1gNnSc2
(	(	kIx(
<g/>
koňská	koňský	k2eAgNnPc1d1
udidla	udidla	k1gNnPc1
<g/>
,	,	kIx,
třmeny	třmen	k1gInPc1
tauzované	tauzovaný	k2eAgNnSc1d1
stříbrem	stříbro	k1gNnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
sekerovité	sekerovitý	k2eAgFnPc4d1
hřivny	hřivna	k1gFnPc4
používané	používaný	k2eAgFnPc4d1
jako	jako	k8xC,k8xS
platidla	platidlo	k1gNnPc1
<g/>
,	,	kIx,
sekery	sekera	k1gFnPc1
včetně	včetně	k7c2
bradatic	bradatice	k1gFnPc2
<g/>
,	,	kIx,
kosy	kosa	k1gFnSc2
<g/>
,	,	kIx,
radlice	radlice	k1gFnSc2
<g/>
,	,	kIx,
ostruhy	ostruha	k1gFnSc2
<g/>
,	,	kIx,
pily	pila	k1gFnSc2
<g/>
,	,	kIx,
hroty	hrot	k1gInPc4
oštěpů	oštěp	k1gInPc2
<g/>
,	,	kIx,
hroty	hrot	k1gInPc4
šípů	šíp	k1gInPc2
<g/>
,	,	kIx,
klíče	klíč	k1gInPc4
<g/>
,	,	kIx,
zlomky	zlomek	k1gInPc4
petlic	petlice	k1gFnPc2
<g/>
,	,	kIx,
kování	kování	k1gNnSc1
vědérek	vědérko	k1gNnPc2
a	a	k8xC
věder	vědro	k1gNnPc2
<g/>
,	,	kIx,
závěsná	závěsný	k2eAgNnPc1d1
kování	kování	k1gNnPc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
ocílky	ocílka	k1gFnPc1
<g/>
,	,	kIx,
krojidla	krojidlo	k1gNnPc1
<g/>
,	,	kIx,
vrtáky	vrták	k1gInPc1
<g/>
,	,	kIx,
kladivo	kladivo	k1gNnSc1
<g/>
,	,	kIx,
kovářské	kovářský	k2eAgFnPc1d1
kleště	kleště	k1gFnPc1
<g/>
,	,	kIx,
klín	klín	k1gInSc1
či	či	k8xC
průbojník	průbojník	k1gInSc1
<g/>
,	,	kIx,
motyka	motyka	k1gFnSc1
<g/>
,	,	kIx,
osníky	osník	k1gInPc1
<g/>
,	,	kIx,
nože	nůž	k1gInPc1
<g/>
,	,	kIx,
srpy	srp	k1gInPc1
<g/>
,	,	kIx,
zlomek	zlomek	k1gInSc1
čepele	čepel	k1gFnSc2
nějaké	nějaký	k3yIgFnSc2
zbraně	zbraň	k1gFnSc2
<g/>
,	,	kIx,
snad	snad	k9
meče	meč	k1gInPc4
<g/>
,	,	kIx,
výhňové	výhňový	k2eAgFnPc4d1
lopatky	lopatka	k1gFnPc4
<g/>
,	,	kIx,
misky	miska	k1gFnPc4
slezského	slezský	k2eAgInSc2d1
typu	typ	k1gInSc2
v	v	k7c6
neobvykle	obvykle	k6eNd1
velkém	velký	k2eAgNnSc6d1
množství	množství	k1gNnSc6
34	#num#	k4
ks	ks	kA
<g/>
,	,	kIx,
překvapivý	překvapivý	k2eAgInSc1d1
byl	být	k5eAaImAgInS
také	také	k9
nález	nález	k1gInSc1
90	#num#	k4
cm	cm	kA
dlouhé	dlouhý	k2eAgFnSc2d1
obouruční	obouruční	k2eAgFnSc2d1
pily	pila	k1gFnSc2
a	a	k8xC
kosy	kosa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
olověné	olověný	k2eAgInPc1d1
přesleny	přeslen	k1gInPc1
<g/>
,	,	kIx,
pozoruhodný	pozoruhodný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
též	též	k9
nález	nález	k1gInSc4
olověného	olověný	k2eAgInSc2d1
ingotu	ingot	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
představuje	představovat	k5eAaImIp3nS
největší	veliký	k2eAgInSc1d3
soubor	soubor	k1gInSc1
depotů	depot	k1gInPc2
z	z	k7c2
doby	doba	k1gFnSc2
velkomoravské	velkomoravský	k2eAgNnSc1d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
kdy	kdy	k6eAd1
našel	najít	k5eAaPmAgMnS
na	na	k7c6
území	území	k1gNnSc6
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
Slovenska	Slovensko	k1gNnSc2
i	i	k8xC
okolních	okolní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavostí	zajímavost	k1gFnPc2
je	být	k5eAaImIp3nS
i	i	k9
nález	nález	k1gInSc1
kamene	kámen	k1gInSc2
svoru	svora	k1gFnSc4
s	s	k7c7
čočkami	čočka	k1gFnPc7
polodrahokamů	polodrahokam	k1gInPc2
<g/>
,	,	kIx,
pocházející	pocházející	k2eAgFnSc4d1
z	z	k7c2
Jesenicka	Jesenicko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1
většina	většina	k1gFnSc1
nálezů	nález	k1gInPc2
je	být	k5eAaImIp3nS
podle	podle	k7c2
archeologa	archeolog	k1gMnSc2
dr	dr	kA
<g/>
.	.	kIx.
Jiřího	Jiří	k1gMnSc4
Kohoutka	Kohoutek	k1gMnSc4
datována	datován	k2eAgMnSc4d1
od	od	k7c2
konce	konec	k1gInSc2
osmého	osmý	k4xOgNnSc2
do	do	k7c2
zhruba	zhruba	k6eAd1
poloviny	polovina	k1gFnPc1
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
doby	doba	k1gFnSc2
halštatské	halštatský	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
amfora	amfora	k1gFnSc1
<g/>
,	,	kIx,
zdobená	zdobený	k2eAgFnSc1d1
na	na	k7c6
tuhovaném	tuhovaný	k2eAgInSc6d1
povrchu	povrch	k1gInSc6
charakteristickým	charakteristický	k2eAgNnSc7d1
žlábkováním	žlábkování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Tyto	tento	k3xDgInPc1
svého	svůj	k3xOyFgInSc2
druhu	druh	k1gInSc2
výjimečné	výjimečný	k2eAgInPc4d1
nálezy	nález	k1gInPc4
zařadily	zařadit	k5eAaPmAgFnP
tento	tento	k3xDgInSc4
valašský	valašský	k2eAgInSc4d1
kopec	kopec	k1gInSc4
mezi	mezi	k7c7
nejvýznamnější	významný	k2eAgFnSc7d3
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
i	i	k9
poněkud	poněkud	k6eAd1
nejasné	jasný	k2eNgFnPc4d1
lokality	lokalita	k1gFnPc4
spojené	spojený	k2eAgNnSc1d1
s	s	k7c7
velkomoravským	velkomoravský	k2eAgNnSc7d1
obdobím	období	k1gNnSc7
v	v	k7c6
celém	celý	k2eAgInSc6d1
středoevropském	středoevropský	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převratné	převratný	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
výzkumů	výzkum	k1gInPc2
posledních	poslední	k2eAgNnPc2d1
let	léto	k1gNnPc2
tak	tak	k8xC,k8xS
přinesly	přinést	k5eAaPmAgInP
zcela	zcela	k6eAd1
nové	nový	k2eAgInPc1d1
a	a	k8xC
závažné	závažný	k2eAgInPc1d1
poznatky	poznatek	k1gInPc1
o	o	k7c6
historii	historie	k1gFnSc6
jižního	jižní	k2eAgNnSc2d1
Valašska	Valašsko	k1gNnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
širšího	široký	k2eAgInSc2d2
kontextu	kontext	k1gInSc2
východní	východní	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
lokalita	lokalita	k1gFnSc1
i	i	k9
s	s	k7c7
jejími	její	k3xOp3gInPc7
nálezy	nález	k1gInPc7
oprávněně	oprávněně	k6eAd1
stala	stát	k5eAaPmAgFnS
objektem	objekt	k1gInSc7
zvýšené	zvýšený	k2eAgFnSc3d1
pozornosti	pozornost	k1gFnSc3
sdělovacích	sdělovací	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Archeologický	archeologický	k2eAgInSc1d1
průzkum	průzkum	k1gInSc1
zatím	zatím	k6eAd1
nenarazil	narazit	k5eNaPmAgInS
na	na	k7c4
známky	známka	k1gFnPc4
obytných	obytný	k2eAgNnPc2d1
stavení	stavení	k1gNnPc2
ani	ani	k8xC
na	na	k7c4
pohřebiště	pohřebiště	k1gNnSc4
či	či	k8xC
jednotlivé	jednotlivý	k2eAgInPc4d1
hroby	hrob	k1gInPc4
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
tedy	tedy	k9
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
lokalita	lokalita	k1gFnSc1
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
obydlený	obydlený	k2eAgInSc4d1
prostor	prostor	k1gInSc4
nebo	nebo	k8xC
pouze	pouze	k6eAd1
k	k	k7c3
rituálním	rituální	k2eAgInPc3d1
nebo	nebo	k8xC
skladovacím	skladovací	k2eAgInPc3d1
účelům	účel	k1gInPc3
či	či	k8xC
jako	jako	k9
pouhé	pouhý	k2eAgNnSc4d1
útočiště	útočiště	k1gNnSc4
v	v	k7c6
době	doba	k1gFnSc6
nebezpečí	nebezpečí	k1gNnSc2
vojenského	vojenský	k2eAgNnSc2d1
napadení	napadení	k1gNnSc2
nepřítelem	nepřítel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovanské	slovanský	k2eAgFnPc4d1
osady	osada	k1gFnPc4
s	s	k7c7
pohřebišti	pohřebiště	k1gNnPc7
ale	ale	k8xC
s	s	k7c7
určitostí	určitost	k1gFnSc7
existovaly	existovat	k5eAaImAgInP
v	v	k7c6
nezanedbatelném	zanedbatelný	k2eNgNnSc6d1
množství	množství	k1gNnSc6
zejména	zejména	k9
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
od	od	k7c2
hřebene	hřeben	k1gInSc2
Vizovických	vizovický	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
na	na	k7c6
Slavičínsku	Slavičínsko	k1gNnSc6
(	(	kIx(
<g/>
Lipová	lipový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Nevšová	Nevšový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Rudimov	Rudimov	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Totéž	týž	k3xTgNnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
i	i	k8xC
sídlišť	sídliště	k1gNnPc2
nebo	nebo	k8xC
pohřebišť	pohřebiště	k1gNnPc2
období	období	k1gNnSc2
pozdní	pozdní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
bronzové	bronzový	k2eAgFnSc2d1
a	a	k8xC
počátku	počátek	k1gInSc2
doby	doba	k1gFnSc2
železné	železný	k2eAgFnPc1d1
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Klášťova	Klášťov	k1gInSc2
(	(	kIx(
<g/>
Sehradice	Sehradice	k1gFnPc1
<g/>
,	,	kIx,
Slopné	Slopný	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jižně	jižně	k6eAd1
od	od	k7c2
Klášťova	Klášťov	k1gInSc2
(	(	kIx(
<g/>
Vlachovice	Vlachovice	k1gFnSc1
<g/>
,	,	kIx,
Štítná	štítný	k2eAgFnSc1d1
nad	nad	k7c7
Vláří	Vlář	k1gFnSc7
<g/>
)	)	kIx)
nebo	nebo	k8xC
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Klášťova	Klášťov	k1gInSc2
(	(	kIx(
<g/>
Tichov	Tichov	k1gInSc1
<g/>
,	,	kIx,
o	o	k7c4
kus	kus	k1gInSc4
dále	daleko	k6eAd2
pak	pak	k6eAd1
Sidonie	Sidonie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ultrakopce	Ultrakopce	k1gFnPc1
na	na	k7c4
Ultratisicovky	Ultratisicovka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Kohoutek	Kohoutek	k1gMnSc1
Jiří	Jiří	k1gMnSc1
<g/>
:	:	kIx,
Klášťov	Klášťov	k1gInSc1
2006	#num#	k4
-	-	kIx~
Hradisko	hradisko	k1gNnSc4
kultury	kultura	k1gFnSc2
popelnicových	popelnicový	k2eAgNnPc2d1
polí	pole	k1gNnPc2
<g/>
,	,	kIx,
významná	významný	k2eAgFnSc1d1
velkomoravská	velkomoravský	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
jižního	jižní	k2eAgNnSc2d1
Valašska	Valašsko	k1gNnSc2
–	–	k?
závěrečná	závěrečný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
</s>
<s>
Kohoutek	Kohoutek	k1gMnSc1
Jiří	Jiří	k1gMnSc1
<g/>
:	:	kIx,
Vysoké	vysoký	k2eAgNnSc1d1
pole	pole	k1gNnSc1
(	(	kIx(
<g/>
okr	okr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlín	Zlín	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
Klášťov	Klášťov	k1gInSc1
–	–	k?
zjišťovací	zjišťovací	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
hradiska	hradisko	k1gNnSc2
-	-	kIx~
výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
2007	#num#	k4
<g/>
,	,	kIx,
Ústav	ústav	k1gInSc1
archeologické	archeologický	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
Brno	Brno	k1gNnSc1
</s>
<s>
Hlavica	Hlavica	k1gMnSc1
Michal	Michal	k1gMnSc1
<g/>
:	:	kIx,
Raně	raně	k6eAd1
středověké	středověký	k2eAgFnPc1d1
kultovní	kultovní	k2eAgFnPc1d1
aktivity	aktivita	k1gFnPc1
na	na	k7c6
vrchu	vrch	k1gInSc6
Klášťov	Klášťov	k1gInSc1
a	a	k8xC
jejich	jejich	k3xOp3gInSc4
odraz	odraz	k1gInSc4
v	v	k7c6
archeologických	archeologický	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
a	a	k8xC
ústní	ústní	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
slovesnosti	slovesnost	k1gFnSc2
(	(	kIx(
<g/>
bakalářská	bakalářský	k2eAgFnSc1d1
diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
MU	MU	kA
Brno	Brno	k1gNnSc1
</s>
<s>
Čižmář	čižmář	k1gMnSc1
Ivan	Ivan	k1gMnSc1
<g/>
:	:	kIx,
Vysoké	vysoký	k2eAgFnSc2d1
Pole	pole	k1gFnSc2
(	(	kIx(
<g/>
okr	okr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlín	Zlín	k1gInSc1
<g/>
)	)	kIx)
Klášťov	Klášťov	k1gInSc1
-	-	kIx~
archeologický	archeologický	k2eAgInSc1d1
průzkum	průzkum	k1gInSc1
<g/>
,	,	kIx,
výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
Ústav	ústav	k1gInSc1
archeologické	archeologický	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
Brno	Brno	k1gNnSc1
</s>
<s>
Nekuda	Nekuda	k1gMnSc1
Vladimír	Vladimír	k1gMnSc1
<g/>
:	:	kIx,
Vlastivěda	vlastivěda	k1gFnSc1
moravská	moravský	k2eAgFnSc1d1
–	–	k?
Zlínsko	Zlínsko	k1gNnSc1
<g/>
,	,	kIx,
1995	#num#	k4
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Zlín	Zlín	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
115	#num#	k4
-116	-116	k4
<g/>
,	,	kIx,
131-146	131-146	k4
</s>
<s>
Eliášová	Eliášová	k1gFnSc1
Eva	Eva	k1gFnSc1
<g/>
:	:	kIx,
Pověsti	pověst	k1gFnPc1
z	z	k7c2
Vizovických	vizovický	k2eAgFnPc2d1
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
1991	#num#	k4
Zlín	Zlín	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
11	#num#	k4
</s>
<s>
Jiné	jiný	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
</s>
<s>
Přednášky	přednáška	k1gFnPc1
RNDr.	RNDr.	kA
PhDr.	PhDr.	kA
Jany	Jana	k1gFnSc2
Langové	Langová	k1gFnSc2
v	v	k7c6
Muzeu	muzeum	k1gNnSc6
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
ve	v	k7c6
dnech	den	k1gInPc6
6	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Výstava	výstava	k1gFnSc1
Klášťov	Klášťov	k1gInSc1
–	–	k?
hora	hora	k1gFnSc1
čarodějů	čaroděj	k1gMnPc2
<g/>
,	,	kIx,
Muzeum	muzeum	k1gNnSc1
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
Zlín	Zlín	k1gInSc1
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
až	až	k9
21	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Klášťov	Klášťovo	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Klášťov	Klášťov	k1gInSc1
na	na	k7c4
Rozhlas	rozhlas	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Klášťov	Klášťov	k1gInSc1
na	na	k7c4
Tajemnamista	Tajemnamista	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Klášťov	Klášťov	k1gInSc1
na	na	k7c4
Obec-Drnovice	Obec-Drnovice	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Pravěk	pravěk	k1gInSc1
</s>
