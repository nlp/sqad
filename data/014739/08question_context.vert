<s>
Dirham	dirham	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
emirátů	emirát	k1gInPc2
je	být	k5eAaImIp3nS
měna	měna	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
arabských	arabský	k2eAgInPc6d1
emirátech	emirát	k1gInPc6
<g/>
.	.	kIx.
</s>