<s>
Dirham	dirham	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
emirátů	emirát	k1gInPc2
</s>
<s>
Dirham	dirham	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
emirátůد	emirátůد	k?
إ	إ	k?
(	(	kIx(
<g/>
arabsky	arabsky	k6eAd1
<g/>
)	)	kIx)
Přední	přední	k2eAgFnSc1d1
strana	strana	k1gFnSc1
jednodirhamové	jednodirhamový	k2eAgNnSc4d1
minceZemě	minceZemě	k6eAd1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
AED	AED	kA
Inflace	inflace	k1gFnSc1
</s>
<s>
2	#num#	k4
%	%	kIx~
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
,	,	kIx,
odhad	odhad	k1gInSc1
<g/>
)	)	kIx)
Symbol	symbol	k1gInSc1
</s>
<s>
د	د	k?
Mince	mince	k1gFnSc1
</s>
<s>
1	#num#	k4
f	f	k?
<g/>
;	;	kIx,
5	#num#	k4
f	f	k?
<g/>
;	;	kIx,
10	#num#	k4
f	f	k?
<g/>
;	;	kIx,
25	#num#	k4
f	f	k?
<g/>
;	;	kIx,
50	#num#	k4
f	f	k?
<g/>
;	;	kIx,
1	#num#	k4
d	d	k?
Bankovky	bankovka	k1gFnPc1
</s>
<s>
běžně	běžně	k6eAd1
5	#num#	k4
,	,	kIx,
10	#num#	k4
d	d	k?
<g/>
,	,	kIx,
20	#num#	k4
d	d	k?
<g/>
,	,	kIx,
50	#num#	k4
d	d	k?
<g/>
,	,	kIx,
100	#num#	k4
d	d	k?
<g/>
,	,	kIx,
200	#num#	k4
d	d	k?
<g/>
,	,	kIx,
500	#num#	k4
dzřídka	dzřídka	k1gFnSc1
1	#num#	k4
000	#num#	k4
d	d	k?
</s>
<s>
Dirham	dirham	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
emirátů	emirát	k1gInPc2
je	být	k5eAaImIp3nS
měna	měna	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
arabských	arabský	k2eAgInPc6d1
emirátech	emirát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc1
dirhamu	dirham	k1gInSc2
je	být	k5eAaImIp3nS
AED	AED	kA
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc1
dirham	dirham	k1gInSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
100	#num#	k4
filů	fil	k1gInPc2
(	(	kIx(
<g/>
jednotné	jednotný	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
fil	fil	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
AED	AED	kA
se	se	k3xPyFc4
název	název	k1gInSc1
pro	pro	k7c4
dirham	dirham	k1gInSc4
také	také	k9
neoficiálně	oficiálně	k6eNd1,k6eAd1
zkracuje	zkracovat	k5eAaImIp3nS
na	na	k7c6
DH	DH	kA
či	či	k8xC
DHs	DHs	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Název	název	k1gInSc1
dirham	dirham	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
arabštiny	arabština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
staletím	staletí	k1gNnPc3
používání	používání	k1gNnSc3
této	tento	k3xDgFnSc2
měny	měna	k1gFnSc2
se	se	k3xPyFc4
název	název	k1gInSc1
i	i	k8xC
v	v	k7c6
období	období	k1gNnSc6
nadvlády	nadvláda	k1gFnSc2
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
udržel	udržet	k5eAaPmAgInS
až	až	k6eAd1
do	do	k7c2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dirham	dirham	k1gInSc4
Spojených	spojený	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
emirátů	emirát	k1gInPc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
měna	měna	k1gFnSc1
od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1973	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nahradil	nahradit	k5eAaPmAgInS
katarský	katarský	k2eAgInSc1d1
a	a	k8xC
dubajský	dubajský	k2eAgInSc1d1
riál	riál	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
používaly	používat	k5eAaImAgFnP
od	od	k7c2
roku	rok	k1gInSc2
1966	#num#	k4
ve	v	k7c6
všech	všecek	k3xTgInPc6
emirátech	emirát	k1gInPc6
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Abú	abú	k1gMnSc1
Zabí	Zabí	k1gMnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
dirhamem	dirham	k1gInSc7
nahrazen	nahradit	k5eAaPmNgInS
Bahrajnský	Bahrajnský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
v	v	k7c6
poměru	poměr	k1gInSc6
1	#num#	k4
dinár	dinár	k1gInSc1
=	=	kIx~
10	#num#	k4
dirhamů	dirham	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
byly	být	k5eAaImAgFnP
vydány	vydán	k2eAgFnPc1d1
mince	mince	k1gFnPc1
s	s	k7c7
hodnotami	hodnota	k1gFnPc7
1	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
a	a	k8xC
50	#num#	k4
filů	fil	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filové	Filové	k2eAgFnPc4d1
<g/>
,	,	kIx,
pětifilové	pětifilové	k2eAgFnPc4d1
a	a	k8xC
desetifilové	desetifilové	k2eAgFnPc4d1
mince	mince	k1gFnPc4
jsou	být	k5eAaImIp3nP
z	z	k7c2
bronzu	bronz	k1gInSc2
<g/>
,	,	kIx,
vyšší	vysoký	k2eAgFnPc1d2
hodnoty	hodnota	k1gFnPc1
jsou	být	k5eAaImIp3nP
ze	z	k7c2
slitiny	slitina	k1gFnSc2
mědi	měď	k1gFnSc2
a	a	k8xC
niklu	nikl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
byly	být	k5eAaImAgFnP
filové	filová	k1gFnPc1
<g/>
,	,	kIx,
pětifilové	pětifilové	k2eAgFnPc1d1
a	a	k8xC
desetifilové	desetifilové	k2eAgFnPc1d1
mince	mince	k1gFnPc1
zmenšeny	zmenšen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Padesátifilová	Padesátifilová	k1gFnSc1
mince	mince	k1gFnSc1
byla	být	k5eAaImAgFnS
také	také	k9
zmenšena	zmenšen	k2eAgFnSc1d1
a	a	k8xC
změnil	změnit	k5eAaPmAgInS
se	se	k3xPyFc4
její	její	k3xOp3gInSc4
tvar	tvar	k1gInSc4
–	–	k?
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
ostatních	ostatní	k2eAgInPc2d1
není	být	k5eNaImIp3nS
kruhová	kruhový	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc4
zaobleného	zaoblený	k2eAgInSc2d1
pravidelného	pravidelný	k2eAgInSc2d1
sedmihranu	sedmihran	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Hodnota	hodnota	k1gFnSc1
na	na	k7c6
mincích	mince	k1gFnPc6
je	být	k5eAaImIp3nS
zapsána	zapsat	k5eAaPmNgFnS
východoarabskými	východoarabský	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
a	a	k8xC
text	text	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
arabštině	arabština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mince	mince	k1gFnSc1
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
5	#num#	k4
a	a	k8xC
10	#num#	k4
filů	fil	k1gMnPc2
se	se	k3xPyFc4
již	již	k6eAd1
téměř	téměř	k6eAd1
nepoužívají	používat	k5eNaImIp3nP
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc1
ceny	cena	k1gFnPc1
jsou	být	k5eAaImIp3nP
zaokrouhleny	zaokrouhlet	k5eAaImNgFnP,k5eAaPmNgFnP
na	na	k7c4
nejbližší	blízký	k2eAgInSc4d3
násobek	násobek	k1gInSc4
25	#num#	k4
filů	fil	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filová	Filová	k1gFnSc1
mince	mince	k1gFnSc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
velmi	velmi	k6eAd1
vzácně	vzácně	k6eAd1
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
při	při	k7c6
placení	placení	k1gNnSc6
lze	lze	k6eAd1
snadno	snadno	k6eAd1
splést	splést	k5eAaPmF
se	s	k7c7
starou	starý	k2eAgFnSc7d1
padesátifilovou	padesátifilův	k2eAgFnSc7d1
mincí	mince	k1gFnSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
mají	mít	k5eAaImIp3nP
téměř	téměř	k6eAd1
shodnou	shodný	k2eAgFnSc4d1
velikost	velikost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
mnoho	mnoho	k4c4
sérií	série	k1gFnPc2
pamětních	pamětní	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
<g/>
,	,	kIx,
připomínajících	připomínající	k2eAgMnPc6d1
různé	různý	k2eAgFnPc4d1
události	událost	k1gFnPc4
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
arabských	arabský	k2eAgInPc6d1
emirátech	emirát	k1gInPc6
či	či	k8xC
panovníky	panovník	k1gMnPc4
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
ObrázekHodnotaParametryPopis	ObrázekHodnotaParametryPopis	k1gInSc1
</s>
<s>
Líc	líc	k1gFnSc1
</s>
<s>
Rub	rub	k1gInSc1
</s>
<s>
PrůměrTloušťkaVáhaOkraj	PrůměrTloušťkaVáhaOkraj	k1gFnSc1
</s>
<s>
TvarLícRub	TvarLícRub	k1gMnSc1
</s>
<s>
ف	ف	k?
<g/>
25	#num#	k4
</s>
<s>
20	#num#	k4
mm	mm	kA
</s>
<s>
1,5	1,5	k4
mm	mm	kA
</s>
<s>
3,5	3,5	k4
g	g	kA
</s>
<s>
Vroubkovaný	vroubkovaný	k2eAgInSc1d1
</s>
<s>
Kruh	kruh	k1gInSc1
</s>
<s>
Zobrazení	zobrazení	k1gNnSc4
gazely	gazela	k1gFnSc2
<g/>
,	,	kIx,
dole	dole	k6eAd1
datum	datum	k1gNnSc1
ražby	ražba	k1gFnSc2
podle	podle	k7c2
Islámského	islámský	k2eAgInSc2d1
a	a	k8xC
Gregoriánského	gregoriánský	k2eAgInSc2d1
kalendáře	kalendář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nápis	nápis	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
ا	ا	k?
ا	ا	k?
ا	ا	k?
<g/>
"	"	kIx"
<g/>
,	,	kIx,
pod	pod	k7c7
ním	on	k3xPp3gInSc7
"	"	kIx"
<g/>
٢	٢	k?
<g/>
٥	٥	k?
<g/>
"	"	kIx"
<g/>
,	,	kIx,
pod	pod	k7c7
ním	on	k3xPp3gInSc7
"	"	kIx"
<g/>
ف	ف	k?
<g/>
ً	ً	k?
<g/>
"	"	kIx"
a	a	k8xC
dole	dole	k6eAd1
"	"	kIx"
<g/>
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
<g/>
"	"	kIx"
</s>
<s>
ف	ف	k?
<g/>
50	#num#	k4
</s>
<s>
21	#num#	k4
mm	mm	kA
</s>
<s>
1,7	1,7	k4
mm	mm	kA
</s>
<s>
4,4	4,4	k4
g	g	kA
</s>
<s>
Hladký	Hladký	k1gMnSc1
</s>
<s>
Sedmihran	Sedmihran	k1gInSc1
</s>
<s>
Tři	tři	k4xCgFnPc1
ropné	ropný	k2eAgFnPc1d1
věže	věž	k1gFnPc1
<g/>
,	,	kIx,
dole	dole	k6eAd1
datum	datum	k1gNnSc1
ražby	ražba	k1gFnSc2
podle	podle	k7c2
Islámského	islámský	k2eAgInSc2d1
a	a	k8xC
Gregoriánského	gregoriánský	k2eAgInSc2d1
kalendáře	kalendář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nápis	nápis	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
ا	ا	k?
ا	ا	k?
ا	ا	k?
<g/>
"	"	kIx"
<g/>
,	,	kIx,
pod	pod	k7c7
ním	on	k3xPp3gInSc7
"	"	kIx"
<g/>
٥	٥	k?
<g/>
۰	۰	k?
<g/>
"	"	kIx"
<g/>
,	,	kIx,
pod	pod	k7c7
ním	on	k3xPp3gInSc7
"	"	kIx"
<g/>
ف	ف	k?
<g/>
ً	ً	k?
<g/>
"	"	kIx"
a	a	k8xC
dole	dole	k6eAd1
"	"	kIx"
<g/>
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
<g/>
"	"	kIx"
</s>
<s>
د	د	k?
<g/>
1	#num#	k4
</s>
<s>
24	#num#	k4
mm	mm	kA
</s>
<s>
2	#num#	k4
mm	mm	kA
</s>
<s>
6,1	6,1	k4
g	g	kA
</s>
<s>
Vroubkovaný	vroubkovaný	k2eAgInSc1d1
</s>
<s>
Kruh	kruh	k1gInSc1
</s>
<s>
Dallah	Dallah	k1gInSc1
(	(	kIx(
<g/>
konvice	konvice	k1gFnSc1
na	na	k7c4
kávu	káva	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dole	dole	k6eAd1
datum	datum	k1gNnSc1
ražby	ražba	k1gFnSc2
podle	podle	k7c2
Islámského	islámský	k2eAgInSc2d1
a	a	k8xC
Gregoriánského	gregoriánský	k2eAgInSc2d1
kalendáře	kalendář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nápis	nápis	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
ا	ا	k?
ا	ا	k?
ا	ا	k?
<g/>
"	"	kIx"
<g/>
,	,	kIx,
pod	pod	k7c7
ním	on	k3xPp3gInSc7
"	"	kIx"
<g/>
١	١	k?
<g/>
"	"	kIx"
<g/>
,	,	kIx,
pod	pod	k7c7
ním	on	k3xPp3gInSc7
"	"	kIx"
<g/>
د	د	k?
<g/>
"	"	kIx"
a	a	k8xC
dole	dole	k6eAd1
"	"	kIx"
<g/>
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
<g/>
"	"	kIx"
</s>
<s>
Problémy	problém	k1gInPc1
s	s	k7c7
podvody	podvod	k1gInPc7
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
roku	rok	k1gInSc2
2006	#num#	k4
vyšlo	vyjít	k5eAaPmAgNnS
veřejně	veřejně	k6eAd1
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
mince	mince	k1gFnSc1
o	o	k7c6
hodnotě	hodnota	k1gFnSc6
jednoho	jeden	k4xCgNnSc2
Filipínského	filipínský	k2eAgNnSc2d1
pesa	peso	k1gNnSc2
je	být	k5eAaImIp3nS
stejně	stejně	k6eAd1
velká	velký	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
mince	mince	k1gFnSc1
o	o	k7c6
hodnotě	hodnota	k1gFnSc6
jednoho	jeden	k4xCgInSc2
dirhamu	dirham	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
jedno	jeden	k4xCgNnSc1
peso	peso	k1gNnSc1
má	mít	k5eAaImIp3nS
hodnotu	hodnota	k1gFnSc4
jen	jen	k9
přibližně	přibližně	k6eAd1
osmi	osm	k4xCc2
filů	fil	k1gMnPc2
<g/>
,	,	kIx,
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
četným	četný	k2eAgInPc3d1
podvodům	podvod	k1gInPc3
při	při	k7c6
placení	placení	k1gNnPc2
mincemi	mince	k1gFnPc7
v	v	k7c6
automatech	automat	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bankovky	bankovka	k1gFnPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
byly	být	k5eAaImAgFnP
vydány	vydán	k2eAgFnPc1d1
mince	mince	k1gFnPc1
s	s	k7c7
hodnotami	hodnota	k1gFnPc7
1	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
a	a	k8xC
100	#num#	k4
dirhamů	dirham	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
bankovka	bankovka	k1gFnSc1
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
1	#num#	k4
000	#num#	k4
dirhamů	dirham	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
6	#num#	k4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
další	další	k2eAgFnSc1d1
série	série	k1gFnSc1
bankovek	bankovka	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
nevyšly	vyjít	k5eNaPmAgFnP
bankovky	bankovka	k1gFnPc1
v	v	k7c6
hodnotách	hodnota	k1gFnPc6
1	#num#	k4
a	a	k8xC
1	#num#	k4
000	#num#	k4
dirhamů	dirham	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bankovky	bankovka	k1gFnSc2
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
500	#num#	k4
dirhamů	dirham	k1gInPc2
byly	být	k5eAaImAgInP
poprvé	poprvé	k6eAd1
vydány	vydat	k5eAaPmNgInP
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
<g/>
,	,	kIx,
bankovky	bankovka	k1gFnPc1
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
200	#num#	k4
dirhamů	dirham	k1gInPc2
pak	pak	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
dvousetdirhamová	dvousetdirhamový	k2eAgFnSc1d1
bankovka	bankovka	k1gFnSc1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
vzácná	vzácný	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
byla	být	k5eAaImAgFnS
vydávána	vydávat	k5eAaImNgFnS,k5eAaPmNgFnS
jen	jen	k9
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
vyšla	vyjít	k5eAaPmAgFnS
další	další	k2eAgFnSc1d1
série	série	k1gFnSc1
v	v	k7c6
odlišné	odlišný	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
vyšla	vyjít	k5eAaPmAgFnS
také	také	k9
nová	nový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
padesátidirhamové	padesátidirhamový	k2eAgFnSc2d1
bankovky	bankovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nápis	nápis	k1gInSc1
na	na	k7c6
přední	přední	k2eAgFnSc6d1
straně	strana	k1gFnSc6
bankovek	bankovka	k1gFnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
arabštině	arabština	k1gFnSc6
<g/>
,	,	kIx,
číslice	číslice	k1gFnPc1
jsou	být	k5eAaImIp3nP
východoarabské	východoarabský	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
zadní	zadní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
nápis	nápis	k1gInSc1
v	v	k7c6
angličtině	angličtina	k1gFnSc6
a	a	k8xC
číslice	číslice	k1gFnSc1
jsou	být	k5eAaImIp3nP
arabské	arabský	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Série	série	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
</s>
<s>
ObrázekHodnotaBarevný	ObrázekHodnotaBarevný	k2eAgInSc4d1
tónRozměry	tónRozměr	k1gInPc4
(	(	kIx(
<g/>
mm	mm	kA
<g/>
)	)	kIx)
<g/>
Popis	popis	k1gInSc1
</s>
<s>
RubLícRubLíc	RubLícRubLit	k5eAaImSgFnS
</s>
<s>
5	#num#	k4
<g/>
د	د	k?
</s>
<s>
Hnědá	hnědat	k5eAaImIp3nS
</s>
<s>
143	#num#	k4
×	×	k?
60	#num#	k4
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1
Souq	Souq	k1gFnSc1
(	(	kIx(
<g/>
tržiště	tržiště	k1gNnSc1
<g/>
)	)	kIx)
v	v	k7c4
Sharjah	Sharjah	k1gInSc4
</s>
<s>
Mešita	mešita	k1gFnSc1
Imam	Imam	k1gInSc1
Salem	Salem	k1gInSc1
Al	ala	k1gFnPc2
Mutawa	Mutaw	k1gInSc2
v	v	k7c4
Sharjah	Sharjah	k1gInSc4
</s>
<s>
10	#num#	k4
<g/>
د	د	k?
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
</s>
<s>
147	#num#	k4
×	×	k?
62	#num#	k4
</s>
<s>
Chandžar	Chandžar	k1gMnSc1
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1
farma	farma	k1gFnSc1
</s>
<s>
20	#num#	k4
<g/>
د	د	k?
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
</s>
<s>
149	#num#	k4
×	×	k?
63	#num#	k4
</s>
<s>
Přední	přední	k2eAgFnSc1d1
část	část	k1gFnSc1
golfového	golfový	k2eAgInSc2d1
a	a	k8xC
jachtového	jachtový	k2eAgInSc2d1
klubu	klub	k1gInSc2
v	v	k7c6
zálivu	záliv	k1gInSc6
Dubai	Duba	k1gFnSc2
Creek	Creek	k6eAd1
</s>
<s>
Tradiční	tradiční	k2eAgFnSc4d1
obchodní	obchodní	k2eAgFnSc4d1
dhau	dhaa	k1gFnSc4
</s>
<s>
50	#num#	k4
<g/>
د	د	k?
</s>
<s>
Světle	světle	k6eAd1
hnědá	hnědat	k5eAaImIp3nS
</s>
<s>
151	#num#	k4
×	×	k?
64	#num#	k4
</s>
<s>
Oryx	Oryx	k1gInSc1
(	(	kIx(
<g/>
druh	druh	k1gInSc1
antilopy	antilopa	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Pevnost	pevnost	k1gFnSc1
Al	ala	k1gFnPc2
Jahli	Jahl	k1gMnPc1
v	v	k7c6
emirátu	emirát	k1gInSc6
Al	ala	k1gFnPc2
Ain	Ain	k1gMnSc1
</s>
<s>
100	#num#	k4
<g/>
د	د	k?
</s>
<s>
Růžová	růžový	k2eAgFnSc1d1
</s>
<s>
155	#num#	k4
×	×	k?
66	#num#	k4
</s>
<s>
Pevnost	pevnost	k1gFnSc1
Al	ala	k1gFnPc2
Fahidi	Fahid	k1gMnPc1
</s>
<s>
Budova	budova	k1gFnSc1
světového	světový	k2eAgNnSc2d1
obchodního	obchodní	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
v	v	k7c6
Dubaji	Dubaj	k1gFnSc6
</s>
<s>
200	#num#	k4
<g/>
د	د	k?
</s>
<s>
Tmavě	tmavě	k6eAd1
žlutá	žlutý	k2eAgFnSc1d1
</s>
<s>
157	#num#	k4
×	×	k?
67	#num#	k4
</s>
<s>
Sportovní	sportovní	k2eAgInSc1d1
stadion	stadion	k1gInSc1
Zayed	Zayed	k1gInSc1
Sports	Sportsa	k1gFnPc2
City	City	k1gFnSc2
Stadium	stadium	k1gNnSc1
a	a	k8xC
budova	budova	k1gFnSc1
soudu	soud	k1gInSc2
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1
banka	banka	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
emirátů	emirát	k1gInPc2
v	v	k7c6
Abú	abú	k1gMnSc2
Zabí	Zabí	k1gFnSc6
</s>
<s>
500	#num#	k4
<g/>
د	د	k?
</s>
<s>
Světle	světle	k6eAd1
modrá	modrý	k2eAgFnSc1d1
</s>
<s>
159	#num#	k4
×	×	k?
68	#num#	k4
</s>
<s>
Raroh	raroh	k1gMnSc1
velký	velký	k2eAgMnSc1d1
</s>
<s>
Mešita	mešita	k1gFnSc1
Jumeirah	Jumeiraha	k1gFnPc2
</s>
<s>
1000	#num#	k4
<g/>
د	د	k?
</s>
<s>
hnědá	hnědat	k5eAaImIp3nS
</s>
<s>
163	#num#	k4
×	×	k?
70	#num#	k4
</s>
<s>
Qasr	Qasr	k1gMnSc1
al-Hosn	al-Hosn	k1gMnSc1
(	(	kIx(
<g/>
nejstarší	starý	k2eAgFnSc1d3
kamenná	kamenný	k2eAgFnSc1d1
budova	budova	k1gFnSc1
v	v	k7c6
Abú	abú	k1gMnSc6
Zabí	Zabí	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Panoráma	panoráma	k1gFnSc1
Abú	abú	k1gMnSc1
Zabí	Zabí	k1gMnSc1
</s>
<s>
Směnný	směnný	k2eAgInSc1d1
kurz	kurz	k1gInSc1
</s>
<s>
Od	od	k7c2
listopadu	listopad	k1gInSc2
1997	#num#	k4
je	být	k5eAaImIp3nS
hodnota	hodnota	k1gFnSc1
dirhamu	dirham	k1gInSc2
v	v	k7c6
praxi	praxe	k1gFnSc6
fixována	fixovat	k5eAaImNgFnS
na	na	k7c4
americký	americký	k2eAgInSc4d1
dolar	dolar	k1gInSc4
v	v	k7c6
poměru	poměr	k1gInSc6
1	#num#	k4
USD	USD	kA
=	=	kIx~
3,672	3,672	k4
<g/>
5	#num#	k4
AED	AED	kA
<g/>
,	,	kIx,
resp.	resp.	kA
1	#num#	k4
AED	AED	kA
=	=	kIx~
0,272294	0,272294	k4
USD	USD	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Aktuální	aktuální	k2eAgInSc4d1
kurz	kurz	k1gInSc4
měny	měna	k1gFnSc2
Dirham	dirham	k1gInSc4
Spojených	spojený	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
emirátů	emirát	k1gInPc2
Podle	podle	k7c2
ČNB	ČNB	kA
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
Podle	podle	k7c2
Google	Google	k1gFnSc2
Finance	finance	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
EUR	euro	k1gNnPc2
USD	USD	kA
Podle	podle	k7c2
Kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
(	(	kIx(
<g/>
Graf	graf	k1gInSc1
Banky	banka	k1gFnSc2
<g/>
)	)	kIx)
EUR	euro	k1gNnPc2
USD	USD	kA
Podle	podle	k7c2
Yahoo	Yahoo	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Finance	finance	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
EUR	euro	k1gNnPc2
USD	USD	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
United	United	k1gMnSc1
Arab	Arab	k1gMnSc1
Emirates	Emirates	k1gMnSc1
dirham	dirham	k1gInSc4
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
MIDDLE	MIDDLE	kA
EAST	EAST	kA
::	::	k?
UNITED	UNITED	kA
ARAB	Arab	k1gMnSc1
EMIRATES	EMIRATES	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2019-12-06	2019-12-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MENON	MENON	kA
<g/>
,	,	kIx,
Sunita	sunita	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hey	Hey	k1gFnPc2
presto	presto	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
A	a	k8xC
Peso	peso	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
as	as	k1gNnSc7
good	good	k1gInSc1
as	as	k1gInSc1
a	a	k8xC
Dirham	dirham	k1gInSc1
<g/>
.	.	kIx.
gulfnews	gulfnews	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
gulfnews	gulfnews	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LINZMAYER	LINZMAYER	kA
<g/>
,	,	kIx,
Owen	Owen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Banknote	Banknot	k1gInSc5
Book	Book	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
San	San	k1gMnSc1
Francisco	Francisco	k1gMnSc1
<g/>
,	,	kIx,
CA	ca	kA
<g/>
:	:	kIx,
www.BanknoteNews.com	www.BanknoteNews.com	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
United	United	k1gMnSc1
Arab	Arab	k1gMnSc1
Emirates	Emirates	k1gMnSc1
<g/>
.	.	kIx.
↑	↑	k?
Archived	Archived	k1gInSc1
copy	cop	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SCHULER	SCHULER	kA
<g/>
,	,	kIx,
Kurt	Kurt	k1gMnSc1
<g/>
.	.	kIx.
users	users	k1gInSc1
<g/>
.	.	kIx.
<g/>
erols	erols	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2007-02-19	2007-02-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
dirham	dirham	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
emirátů	emirát	k1gInPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
dirham	dirham	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Měny	měna	k1gFnPc4
Asie	Asie	k1gFnSc2
Severní	severní	k2eAgFnSc2d1
</s>
<s>
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
Střední	střední	k2eAgFnSc2d1
</s>
<s>
Kazachstánský	kazachstánský	k2eAgInSc1d1
tenge	tenge	k1gInSc1
•	•	k?
Kyrgyzský	kyrgyzský	k2eAgInSc1d1
som	soma	k1gFnPc2
•	•	k?
Tádžický	tádžický	k2eAgInSc1d1
somoni	somoň	k1gFnSc3
•	•	k?
Turkmenský	turkmenský	k2eAgMnSc1d1
manat	manat	k2eAgMnSc1d1
•	•	k?
Uzbecký	uzbecký	k2eAgInSc1d1
sum	suma	k1gFnPc2
Jihozápadní	jihozápadní	k2eAgInSc4d1
</s>
<s>
Abchazský	abchazský	k2eAgInSc1d1
apsar	apsar	k1gInSc1
•	•	k?
Arménský	arménský	k2eAgInSc1d1
dram	drama	k1gFnPc2
•	•	k?
Ázerbájdžánský	ázerbájdžánský	k2eAgInSc1d1
manat	manat	k1gInSc1
•	•	k?
Bahrajnský	Bahrajnský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Irácký	irácký	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Íránský	íránský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Izraelský	izraelský	k2eAgInSc1d1
šekel	šekel	k1gInSc1
•	•	k?
Jemenský	jemenský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Jordánský	jordánský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Katarský	katarský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Kuvajtský	kuvajtský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Libanonská	libanonský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Ománský	ománský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Saúdský	saúdský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Dirham	dirham	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
emirátů	emirát	k1gInPc2
•	•	k?
Syrská	syrský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Jižní	jižní	k2eAgFnSc2d1
</s>
<s>
Afghánský	afghánský	k2eAgInSc1d1
afghání	afghání	k1gNnSc6
•	•	k?
Bangladéšská	bangladéšský	k2eAgFnSc1d1
taka	taka	k1gFnSc1
•	•	k?
Bhútánský	bhútánský	k2eAgInSc1d1
ngultrum	ngultrum	k1gNnSc1
•	•	k?
Indická	indický	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Maledivská	maledivský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Nepálská	nepálský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Pákistánská	pákistánský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Šrílanská	Šrílanský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
</s>
<s>
Čínský	čínský	k2eAgInSc1d1
jüan	jüan	k1gInSc1
•	•	k?
Hongkongský	hongkongský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
•	•	k?
Japonský	japonský	k2eAgMnSc1d1
jen	jen	k9
•	•	k?
Jihokorejský	jihokorejský	k2eAgInSc1d1
won	won	k1gInSc1
•	•	k?
Macajská	macajský	k2eAgFnSc1d1
pataca	pataca	k1gFnSc1
•	•	k?
Mongolský	mongolský	k2eAgInSc1d1
tugrik	tugrik	k1gInSc1
•	•	k?
Tchajwanský	tchajwanský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
Jihovýchodní	jihovýchodní	k2eAgFnSc2d1
</s>
<s>
Brunejský	brunejský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
•	•	k?
Filipínské	filipínský	k2eAgNnSc4d1
peso	peso	k1gNnSc4
•	•	k?
Indonéská	indonéský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Kambodžský	kambodžský	k2eAgInSc1d1
riel	riel	k1gInSc1
•	•	k?
Laoský	laoský	k2eAgInSc1d1
kip	kip	k?
•	•	k?
Malajsijský	malajsijský	k2eAgInSc1d1
ringgit	ringgit	k1gInSc1
•	•	k?
Myanmarský	Myanmarský	k2eAgInSc1d1
kyat	kyat	k1gInSc1
•	•	k?
Severokorejský	severokorejský	k2eAgInSc1d1
won	won	k1gInSc1
•	•	k?
Singapurský	singapurský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
•	•	k?
Thajský	thajský	k2eAgInSc1d1
baht	baht	k1gInSc1
•	•	k?
Vietnamský	vietnamský	k2eAgInSc1d1
dong	dong	k1gInSc1
•	•	k?
Východotimorské	Východotimorský	k2eAgFnPc4d1
centavové	centavový	k2eAgFnPc4d1
mince	mince	k1gFnPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ekonomie	ekonomie	k1gFnSc1
</s>
