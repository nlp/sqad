<p>
<s>
Hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
(	(	kIx(	(
<g/>
Cephalopoda	Cephalopoda	k1gMnSc1	Cephalopoda
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nejdokonalejší	dokonalý	k2eAgFnSc1d3	nejdokonalejší
třída	třída	k1gFnSc1	třída
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgMnPc1d1	aktivní
mořští	mořský	k2eAgMnPc1d1	mořský
dravci	dravec	k1gMnPc1	dravec
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
starobylá	starobylý	k2eAgFnSc1d1	starobylá
loděnka	loděnka	k1gFnSc1	loděnka
<g/>
,	,	kIx,	,
inteligentní	inteligentní	k2eAgFnSc1d1	inteligentní
chobotnice	chobotnice	k1gFnSc1	chobotnice
<g/>
,	,	kIx,	,
sépie	sépie	k1gFnSc1	sépie
<g/>
,	,	kIx,	,
obří	obří	k2eAgFnSc1d1	obří
krakatice	krakatice	k1gFnSc1	krakatice
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
žijící	žijící	k2eAgMnPc1d1	žijící
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
skupiny	skupina	k1gFnSc2	skupina
čtyřžábří	čtyřžábrý	k2eAgMnPc1d1	čtyřžábrý
(	(	kIx(	(
<g/>
loděnka	loděnka	k1gFnSc1	loděnka
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvoužábří	dvoužábří	k2eAgFnSc1d1	dvoužábří
(	(	kIx(	(
<g/>
chobotnice	chobotnice	k1gFnSc1	chobotnice
<g/>
,	,	kIx,	,
oliheň	oliheň	k1gFnSc1	oliheň
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
měří	měřit	k5eAaImIp3nP	měřit
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
centimetru	centimetr	k1gInSc2	centimetr
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
i	i	k9	i
přes	přes	k7c4	přes
20	[number]	k4	20
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
dvoustranně	dvoustranně	k6eAd1	dvoustranně
souměrné	souměrný	k2eAgNnSc4d1	souměrné
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
chapadla	chapadlo	k1gNnPc4	chapadlo
<g/>
.	.	kIx.	.
</s>
<s>
Chapadel	chapadlo	k1gNnPc2	chapadlo
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
loděnka	loděnka	k1gFnSc1	loděnka
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
30	[number]	k4	30
až	až	k9	až
90	[number]	k4	90
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
.	.	kIx.	.
</s>
<s>
Vyspělé	vyspělý	k2eAgInPc1d1	vyspělý
druhy	druh	k1gInPc1	druh
jich	on	k3xPp3gFnPc2	on
mají	mít	k5eAaImIp3nP	mít
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
jsou	být	k5eAaImIp3nP	být
delší	dlouhý	k2eAgInPc1d2	delší
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
přísavky	přísavka	k1gFnPc4	přísavka
<g/>
.	.	kIx.	.
</s>
<s>
Decapodiformes	Decapodiformes	k1gInSc1	Decapodiformes
(	(	kIx(	(
<g/>
desetiramenatci	desetiramenatec	k1gMnPc1	desetiramenatec
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
8	[number]	k4	8
kratších	krátký	k2eAgNnPc2d2	kratší
chapadel	chapadlo	k1gNnPc2	chapadlo
a	a	k8xC	a
2	[number]	k4	2
delší	dlouhý	k2eAgInSc4d2	delší
a	a	k8xC	a
Octopodiformes	Octopodiformes	k1gInSc1	Octopodiformes
(	(	kIx(	(
<g/>
chobotnice	chobotnice	k1gFnSc1	chobotnice
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
8	[number]	k4	8
stejně	stejně	k6eAd1	stejně
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Chobotnice	chobotnice	k1gFnPc1	chobotnice
mohou	moct	k5eAaImIp3nP	moct
přijít	přijít	k5eAaPmF	přijít
o	o	k7c4	o
několik	několik	k4yIc4	několik
chapadel	chapadlo	k1gNnPc2	chapadlo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jim	on	k3xPp3gMnPc3	on
znovu	znovu	k6eAd1	znovu
dorostou	dorůst	k5eAaPmIp3nP	dorůst
<g/>
.	.	kIx.	.
</s>
<s>
Pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
pomocí	pomocí	k7c2	pomocí
vlnění	vlnění	k1gNnSc2	vlnění
ploutevního	ploutevní	k2eAgInSc2d1	ploutevní
lemu	lem	k1gInSc2	lem
nebo	nebo	k8xC	nebo
chapadel	chapadlo	k1gNnPc2	chapadlo
nebo	nebo	k8xC	nebo
vytlačování	vytlačování	k1gNnSc2	vytlačování
vody	voda	k1gFnSc2	voda
ze	z	k7c2	z
sifonu	sifon	k1gInSc2	sifon
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tryskovitý	tryskovitý	k2eAgInSc1d1	tryskovitý
pohyb	pohyb	k1gInSc1	pohyb
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
některým	některý	k3yIgMnPc3	některý
menším	malý	k2eAgMnPc3d2	menší
druhům	druh	k1gMnPc3	druh
vyskakovat	vyskakovat	k5eAaImF	vyskakovat
nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Dýchají	dýchat	k5eAaImIp3nP	dýchat
žábrami	žábry	k1gFnPc7	žábry
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
ohrožení	ohrožení	k1gNnSc2	ohrožení
vypouštějí	vypouštět	k5eAaImIp3nP	vypouštět
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
inkoustový	inkoustový	k2eAgInSc4d1	inkoustový
oblak	oblak	k1gInSc4	oblak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	on	k3xPp3gNnSc4	on
skryje	skrýt	k5eAaPmIp3nS	skrýt
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
tak	tak	k6eAd1	tak
nepozorovaně	pozorovaně	k6eNd1	pozorovaně
uplavat	uplavat	k5eAaPmF	uplavat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
černý	černý	k2eAgInSc1d1	černý
nebo	nebo	k8xC	nebo
hnědý	hnědý	k2eAgInSc1d1	hnědý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
těla	tělo	k1gNnSc2	tělo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Schránky	schránka	k1gFnSc2	schránka
===	===	k?	===
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
měli	mít	k5eAaImAgMnP	mít
všichni	všechen	k3xTgMnPc1	všechen
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
svou	svůj	k3xOyFgFnSc4	svůj
schránku	schránka	k1gFnSc4	schránka
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
málo	málo	k4c1	málo
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
loděnka	loděnka	k1gFnSc1	loděnka
<g/>
.	.	kIx.	.
</s>
<s>
Schránka	schránka	k1gFnSc1	schránka
se	se	k3xPyFc4	se
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
začala	začít	k5eAaPmAgFnS	začít
přemisťovat	přemisťovat	k5eAaImF	přemisťovat
dovnitř	dovnitř	k7c2	dovnitř
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Sépie	sépie	k1gFnSc1	sépie
má	mít	k5eAaImIp3nS	mít
sépiovou	sépiový	k2eAgFnSc4d1	sépiová
kost	kost	k1gFnSc4	kost
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
desetiramenatci	desetiramenatec	k1gMnPc1	desetiramenatec
jen	jen	k9	jen
tzv.	tzv.	kA	tzv.
meč	meč	k1gInSc1	meč
a	a	k8xC	a
chobotnice	chobotnice	k1gFnPc1	chobotnice
nemají	mít	k5eNaImIp3nP	mít
vůbec	vůbec	k9	vůbec
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
mozek	mozek	k1gInSc1	mozek
je	být	k5eAaImIp3nS	být
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
krytý	krytý	k2eAgInSc1d1	krytý
chrupavčitou	chrupavčitý	k2eAgFnSc7d1	chrupavčitá
schránkou	schránka	k1gFnSc7	schránka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velikost	velikost	k1gFnSc1	velikost
těla	tělo	k1gNnSc2	tělo
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
měří	měřit	k5eAaImIp3nP	měřit
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
centimetru	centimetr	k1gInSc2	centimetr
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
i	i	k9	i
přes	přes	k7c4	přes
20	[number]	k4	20
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současných	současný	k2eAgFnPc2d1	současná
znalostí	znalost	k1gFnPc2	znalost
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
hlavonožcem	hlavonožec	k1gMnSc7	hlavonožec
kalmar	kalmara	k1gFnPc2	kalmara
Hamiltonův	Hamiltonův	k2eAgInSc1d1	Hamiltonův
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
krakatice	krakatice	k1gFnSc1	krakatice
obrovská	obrovský	k2eAgFnSc1d1	obrovská
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
měření	měření	k1gNnSc2	měření
velikosti	velikost	k1gFnSc2	velikost
desetiramenných	desetiramenný	k2eAgMnPc2d1	desetiramenný
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
je	být	k5eAaImIp3nS	být
zásadní	zásadní	k2eAgInSc1d1	zásadní
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
rozměr	rozměr	k1gInSc1	rozměr
považovat	považovat	k5eAaImF	považovat
při	při	k7c6	při
srovnání	srovnání	k1gNnSc6	srovnání
za	za	k7c4	za
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
osmi	osm	k4xCc2	osm
ramen	rameno	k1gNnPc2	rameno
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
blíží	blížit	k5eAaImIp3nS	blížit
délce	délka	k1gFnSc3	délka
samotného	samotný	k2eAgNnSc2d1	samotné
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc4	dva
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
chapadla	chapadlo	k1gNnPc4	chapadlo
s	s	k7c7	s
rozšířenými	rozšířený	k2eAgInPc7d1	rozšířený
konci	konec	k1gInPc7	konec
však	však	k9	však
daleko	daleko	k6eAd1	daleko
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
věnec	věnec	k1gInSc4	věnec
ramen	rameno	k1gNnPc2	rameno
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Krakatice	krakatice	k1gFnSc1	krakatice
nalezená	nalezený	k2eAgFnSc1d1	nalezená
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
měla	mít	k5eAaImAgFnS	mít
při	při	k7c6	při
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
17,35	[number]	k4	17,35
m	m	kA	m
tělo	tělo	k1gNnSc1	tělo
bez	bez	k7c2	bez
ramen	rameno	k1gNnPc2	rameno
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
pouhých	pouhý	k2eAgInPc2d1	pouhý
2,35	[number]	k4	2,35
m.	m.	k?	m.
Celých	celý	k2eAgInPc2d1	celý
15	[number]	k4	15
m	m	kA	m
připadlo	připadnout	k5eAaPmAgNnS	připadnout
na	na	k7c4	na
chapadla	chapadlo	k1gNnPc4	chapadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obří	obří	k2eAgMnPc1d1	obří
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
===	===	k?	===
</s>
</p>
<p>
<s>
Tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
bezobratlí	bezobratlí	k1gMnPc1	bezobratlí
byli	být	k5eAaImAgMnP	být
větší	veliký	k2eAgMnPc1d2	veliký
než	než	k8xS	než
obratlovci	obratlovec	k1gMnPc1	obratlovec
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
ztěžovalo	ztěžovat	k5eAaImAgNnS	ztěžovat
vědcům	vědec	k1gMnPc3	vědec
obhajovat	obhajovat	k5eAaImF	obhajovat
existenci	existence	k1gFnSc4	existence
obrovských	obrovský	k2eAgMnPc2d1	obrovský
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
francouzský	francouzský	k2eAgMnSc1d1	francouzský
vědec	vědec	k1gMnSc1	vědec
Pierre	Pierr	k1gInSc5	Pierr
Denys	Denys	k1gInSc4	Denys
de	de	k?	de
Monfort	Monfort	k1gInSc1	Monfort
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
obří	obří	k2eAgMnPc4d1	obří
hlavonožce	hlavonožec	k1gMnPc4	hlavonožec
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
krakeny	kraken	k2eAgFnPc4d1	kraken
a	a	k8xC	a
kolosální	kolosální	k2eAgFnPc4d1	kolosální
chobotnice	chobotnice	k1gFnPc4	chobotnice
<g/>
.	.	kIx.	.
</s>
<s>
Krakeni	Kraken	k2eAgMnPc1d1	Kraken
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
v	v	k7c6	v
severských	severský	k2eAgNnPc6d1	severské
mořích	moře	k1gNnPc6	moře
<g/>
,	,	kIx,	,
plavali	plavat	k5eAaImAgMnP	plavat
volně	volně	k6eAd1	volně
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
méně	málo	k6eAd2	málo
nebezpeční	bezpečný	k2eNgMnPc1d1	nebezpečný
než	než	k8xS	než
kolosální	kolosální	k2eAgFnPc1d1	kolosální
chobotnice	chobotnice	k1gFnPc1	chobotnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
žily	žít	k5eAaImAgFnP	žít
u	u	k7c2	u
dna	dno	k1gNnSc2	dno
tropických	tropický	k2eAgNnPc2d1	tropické
moří	moře	k1gNnPc2	moře
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
podle	podle	k7c2	podle
něho	on	k3xPp3gNnSc2	on
zodpovědné	zodpovědný	k2eAgNnSc1d1	zodpovědné
za	za	k7c4	za
většinu	většina	k1gFnSc4	většina
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
plachetnice	plachetnice	k1gFnPc4	plachetnice
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
si	se	k3xPyFc3	se
přesnou	přesný	k2eAgFnSc4d1	přesná
hmotnost	hmotnost	k1gFnSc4	hmotnost
těchto	tento	k3xDgMnPc2	tento
obřích	obří	k2eAgMnPc2d1	obří
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
netroufl	troufnout	k5eNaPmAgMnS	troufnout
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
tušil	tušit	k5eAaImAgMnS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
hmotnost	hmotnost	k1gFnSc1	hmotnost
největších	veliký	k2eAgFnPc2d3	veliký
velryb	velryba	k1gFnPc2	velryba
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
ho	on	k3xPp3gNnSc4	on
však	však	k9	však
nikdo	nikdo	k3yNnSc1	nikdo
neuznal	uznat	k5eNaPmAgMnS	uznat
a	a	k8xC	a
dožil	dožít	k5eAaPmAgMnS	dožít
v	v	k7c6	v
bídě	bída	k1gFnSc6	bída
a	a	k8xC	a
zapomnění	zapomnění	k1gNnSc4	zapomnění
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
důkazy	důkaz	k1gInPc1	důkaz
ležely	ležet	k5eAaImAgInP	ležet
v	v	k7c6	v
muzeích	muzeum	k1gNnPc6	muzeum
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
každoročně	každoročně	k6eAd1	každoročně
vyplavovány	vyplavovat	k5eAaImNgInP	vyplavovat
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
vcelku	vcelku	k6eAd1	vcelku
kladně	kladně	k6eAd1	kladně
přijatých	přijatý	k2eAgNnPc2d1	přijaté
svědectví	svědectví	k1gNnPc2	svědectví
byla	být	k5eAaImAgFnS	být
zpráva	zpráva	k1gFnSc1	zpráva
posádky	posádka	k1gFnSc2	posádka
lodi	loď	k1gFnSc2	loď
Alecton	Alecton	k1gInSc1	Alecton
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
narazila	narazit	k5eAaPmAgFnS	narazit
30	[number]	k4	30
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1861	[number]	k4	1861
nedaleko	nedaleko	k7c2	nedaleko
ostrova	ostrov	k1gInSc2	ostrov
Tenerife	Tenerif	k1gInSc5	Tenerif
na	na	k7c4	na
poraněnou	poraněný	k2eAgFnSc4d1	poraněná
krakatici	krakatice	k1gFnSc4	krakatice
ležící	ležící	k2eAgFnSc4d1	ležící
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
a	a	k8xC	a
pokusila	pokusit	k5eAaPmAgFnS	pokusit
se	se	k3xPyFc4	se
několikametrového	několikametrový	k2eAgMnSc4d1	několikametrový
hlavonožce	hlavonožec	k1gMnSc4	hlavonožec
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
přes	přes	k7c4	přes
2	[number]	k4	2
tuny	tuna	k1gFnSc2	tuna
ulovit	ulovit	k5eAaPmF	ulovit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
harpuny	harpuna	k1gFnPc4	harpuna
a	a	k8xC	a
střely	střela	k1gFnPc4	střela
z	z	k7c2	z
pušek	puška	k1gFnPc2	puška
nedokázaly	dokázat	k5eNaPmAgInP	dokázat
v	v	k7c6	v
měkkém	měkký	k2eAgNnSc6d1	měkké
těle	tělo	k1gNnSc6	tělo
živočicha	živočich	k1gMnSc2	živočich
způsobit	způsobit	k5eAaPmF	způsobit
větší	veliký	k2eAgNnSc4d2	veliký
poranění	poranění	k1gNnSc4	poranění
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
kapitán	kapitán	k1gMnSc1	kapitán
na	na	k7c4	na
tělo	tělo	k1gNnSc4	tělo
krakatice	krakatice	k1gFnSc2	krakatice
navléct	navléct	k5eAaBmF	navléct
smyčku	smyčka	k1gFnSc4	smyčka
lana	lano	k1gNnSc2	lano
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
zvíře	zvíře	k1gNnSc4	zvíře
na	na	k7c4	na
palubu	paluba	k1gFnSc4	paluba
zadrhla	zadrhnout	k5eAaPmAgFnS	zadrhnout
u	u	k7c2	u
ocasní	ocasní	k2eAgFnSc2d1	ocasní
ploutve	ploutev	k1gFnSc2	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
zvedaného	zvedaný	k2eAgNnSc2d1	zvedané
těla	tělo	k1gNnSc2	tělo
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
lano	lano	k1gNnSc1	lano
ploutev	ploutev	k1gFnSc4	ploutev
odřízlo	odříznout	k5eAaPmAgNnS	odříznout
a	a	k8xC	a
zmrzačená	zmrzačený	k2eAgFnSc1d1	zmrzačená
krakatice	krakatice	k1gFnSc1	krakatice
zmizela	zmizet	k5eAaPmAgFnS	zmizet
v	v	k7c6	v
mořských	mořský	k2eAgFnPc6d1	mořská
hlubinách	hlubina	k1gFnPc6	hlubina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
roztrhl	roztrhnout	k5eAaPmAgInS	roztrhnout
s	s	k7c7	s
krakaticemi	krakatice	k1gFnPc7	krakatice
pytel	pytel	k1gInSc1	pytel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
břehy	břeh	k1gInPc4	břeh
vyplavala	vyplavat	k5eAaPmAgFnS	vyplavat
obrovská	obrovský	k2eAgFnSc1d1	obrovská
bledá	bledý	k2eAgFnSc1d1	bledá
těla	tělo	k1gNnPc1	tělo
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
krakenů	kraken	k1gMnPc2	kraken
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pravě	pravě	k6eAd1	pravě
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
příliš	příliš	k6eAd1	příliš
dobře	dobře	k6eAd1	dobře
nezacházelo	zacházet	k5eNaImAgNnS	zacházet
<g/>
.	.	kIx.	.
</s>
<s>
Zapsány	zapsán	k2eAgFnPc1d1	zapsána
byly	být	k5eAaImAgFnP	být
obvykle	obvykle	k6eAd1	obvykle
jen	jen	k9	jen
rozměry	rozměr	k1gInPc1	rozměr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
vědcům	vědec	k1gMnPc3	vědec
zdály	zdát	k5eAaImAgFnP	zdát
příliš	příliš	k6eAd1	příliš
přemrštěné	přemrštěný	k2eAgFnPc1d1	přemrštěná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
pravdivé	pravdivý	k2eAgFnPc1d1	pravdivá
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
největší	veliký	k2eAgFnSc7d3	veliký
uznávanou	uznávaný	k2eAgFnSc7d1	uznávaná
krakaticí	krakatice	k1gFnSc7	krakatice
stal	stát	k5eAaPmAgInS	stát
exemplář	exemplář	k1gInSc1	exemplář
vyplavený	vyplavený	k2eAgInSc1d1	vyplavený
na	na	k7c4	na
pláž	pláž	k1gFnSc4	pláž
Timble	Timble	k1gFnSc2	Timble
Tickle	Tickl	k1gInSc5	Tickl
při	při	k7c6	při
odlivu	odliv	k1gInSc6	odliv
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1878	[number]	k4	1878
<g/>
.	.	kIx.	.
</s>
<s>
Šťastný	Šťastný	k1gMnSc1	Šťastný
rybář	rybář	k1gMnSc1	rybář
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
však	však	k8xC	však
nezachoval	zachovat	k5eNaPmAgMnS	zachovat
příliš	příliš	k6eAd1	příliš
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Zarazil	zarazit	k5eAaPmAgMnS	zarazit
do	do	k7c2	do
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
kotvu	kotva	k1gFnSc4	kotva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neuplaval	uplavat	k5eNaPmAgMnS	uplavat
a	a	k8xC	a
podával	podávat	k5eAaImAgMnS	podávat
je	být	k5eAaImIp3nS	být
jako	jako	k8xC	jako
krmení	krmení	k1gNnSc1	krmení
psům	pes	k1gMnPc3	pes
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
důkazů	důkaz	k1gInPc2	důkaz
přinášejí	přinášet	k5eAaImIp3nP	přinášet
také	také	k9	také
možná	možná	k9	možná
jediní	jediný	k2eAgMnPc1d1	jediný
přirození	přirozený	k2eAgMnPc1d1	přirozený
nepřátelé	nepřítel	k1gMnPc1	nepřítel
velkých	velká	k1gFnPc2	velká
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
–	–	k?	–
vorvani	vorvaň	k1gMnSc3	vorvaň
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
velrybáři	velrybář	k1gMnPc1	velrybář
mnohokrát	mnohokrát	k6eAd1	mnohokrát
popisovali	popisovat	k5eAaImAgMnP	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
harpunovaný	harpunovaný	k2eAgMnSc1d1	harpunovaný
vorvaň	vorvaň	k1gMnSc1	vorvaň
vyvrhoval	vyvrhovat	k5eAaImAgMnS	vyvrhovat
kusy	kus	k1gInPc4	kus
chapadel	chapadlo	k1gNnPc2	chapadlo
dodnes	dodnes	k6eAd1	dodnes
nevídaných	vídaný	k2eNgInPc2d1	nevídaný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
největší	veliký	k2eAgInSc1d3	veliký
takto	takto	k6eAd1	takto
zaznamenaný	zaznamenaný	k2eAgInSc1d1	zaznamenaný
údaj	údaj	k1gInSc1	údaj
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
celém	celý	k2eAgNnSc6d1	celé
ramenu	rameno	k1gNnSc6	rameno
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
13,70	[number]	k4	13,70
m	m	kA	m
a	a	k8xC	a
silném	silný	k2eAgInSc6d1	silný
75	[number]	k4	75
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tloušťce	tloušťka	k1gFnSc3	tloušťka
nemohlo	moct	k5eNaImAgNnS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
chapadlo	chapadlo	k1gNnSc4	chapadlo
a	a	k8xC	a
protože	protože	k8xS	protože
délka	délka	k1gFnSc1	délka
ramen	rameno	k1gNnPc2	rameno
zhruba	zhruba	k6eAd1	zhruba
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
délce	délka	k1gFnSc3	délka
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
muselo	muset	k5eAaImAgNnS	muset
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
krakatice	krakatice	k1gFnSc2	krakatice
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
kolem	kolo	k1gNnSc7	kolo
14	[number]	k4	14
m.	m.	k?	m.
Druhým	druhý	k4xOgInSc7	druhý
zdrojem	zdroj	k1gInSc7	zdroj
informací	informace	k1gFnPc2	informace
o	o	k7c4	o
kořisti	kořist	k1gFnPc4	kořist
vorvaňů	vorvaň	k1gMnPc2	vorvaň
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
kůže	kůže	k1gFnSc1	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Jizvy	jizva	k1gFnPc1	jizva
po	po	k7c6	po
přísavkách	přísavka	k1gFnPc6	přísavka
krakatic	krakatice	k1gFnPc2	krakatice
často	často	k6eAd1	často
vypovídají	vypovídat	k5eAaPmIp3nP	vypovídat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ulovený	ulovený	k2eAgMnSc1d1	ulovený
vorvaň	vorvaň	k1gMnSc1	vorvaň
svedl	svést	k5eAaPmAgMnS	svést
boj	boj	k1gInSc4	boj
s	s	k7c7	s
protivníky	protivník	k1gMnPc7	protivník
mnohem	mnohem	k6eAd1	mnohem
většími	veliký	k2eAgMnPc7d2	veliký
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
vyplavení	vyplavený	k2eAgMnPc1d1	vyplavený
na	na	k7c6	na
plážích	pláž	k1gFnPc6	pláž
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
při	při	k7c6	při
velikosti	velikost	k1gFnSc6	velikost
největších	veliký	k2eAgFnPc2d3	veliký
krakatic	krakatice	k1gFnPc2	krakatice
je	být	k5eAaImIp3nS	být
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vorvaň	vorvaň	k1gMnSc1	vorvaň
odkázaný	odkázaný	k2eAgInSc4d1	odkázaný
na	na	k7c4	na
zásobu	zásoba	k1gFnSc4	zásoba
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
plicích	plíce	k1gFnPc6	plíce
vyjde	vyjít	k5eAaPmIp3nS	vyjít
ze	z	k7c2	z
souboje	souboj	k1gInSc2	souboj
vždy	vždy	k6eAd1	vždy
jako	jako	k9	jako
vítěz	vítěz	k1gMnSc1	vítěz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
Andros	Androsa	k1gFnPc2	Androsa
na	na	k7c6	na
Bahamách	Bahamy	k1gFnPc6	Bahamy
jsou	být	k5eAaImIp3nP	být
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
obří	obří	k2eAgFnSc2d1	obří
chobotnice	chobotnice	k1gFnSc2	chobotnice
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
říkají	říkat	k5eAaImIp3nP	říkat
luska	luska	k1gFnSc1	luska
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
dovede	dovést	k5eAaPmIp3nS	dovést
uchvacovat	uchvacovat	k5eAaImF	uchvacovat
i	i	k9	i
lidi	člověk	k1gMnPc4	člověk
stojící	stojící	k2eAgFnSc1d1	stojící
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
u	u	k7c2	u
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
cvičena	cvičen	k2eAgFnSc1d1	cvičena
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Bruce	Bruec	k1gInSc2	Bruec
Wrighta	Wright	k1gInSc2	Wright
britská	britský	k2eAgFnSc1d1	britská
komanda	komando	k1gNnSc2	komando
žabích	žabí	k2eAgMnPc2d1	žabí
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Základna	základna	k1gFnSc1	základna
však	však	k9	však
byla	být	k5eAaImAgFnS	být
kvůli	kvůli	k7c3	kvůli
velkým	velký	k2eAgFnPc3d1	velká
záhadným	záhadný	k2eAgFnPc3d1	záhadná
ztrátám	ztráta	k1gFnPc3	ztráta
nakonec	nakonec	k6eAd1	nakonec
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
jinam	jinam	k6eAd1	jinam
<g/>
.	.	kIx.	.
</s>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
Wright	Wright	k1gMnSc1	Wright
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
otiskl	otisknout	k5eAaPmAgInS	otisknout
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Atlantic	Atlantice	k1gFnPc2	Atlantice
Advocate	Advocat	k1gInSc5	Advocat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
luska	luska	k1gFnSc1	luska
je	být	k5eAaImIp3nS	být
obří	obří	k2eAgFnSc1d1	obří
dosud	dosud	k6eAd1	dosud
neznámá	známý	k2eNgFnSc1d1	neznámá
chobotnice	chobotnice	k1gFnSc1	chobotnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počet	počet	k1gInSc1	počet
druhů	druh	k1gInPc2	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Systematickým	systematický	k2eAgInSc7d1	systematický
výzkumem	výzkum	k1gInSc7	výzkum
oceánů	oceán	k1gInPc2	oceán
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
objevovány	objevován	k2eAgInPc1d1	objevován
nové	nový	k2eAgInPc1d1	nový
druhy	druh	k1gInPc1	druh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
–	–	k?	–
703	[number]	k4	703
recentních	recentní	k2eAgInPc2d1	recentní
druhů	druh	k1gInPc2	druh
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
786	[number]	k4	786
recentních	recentní	k2eAgInPc2d1	recentní
druhů	druh	k1gInPc2	druh
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
odhad	odhad	k1gInSc1	odhad
1000	[number]	k4	1000
až	až	k9	až
1200	[number]	k4	1200
druhůPočet	druhůPočet	k1gInSc4	druhůPočet
vymřelých	vymřelý	k2eAgMnPc2d1	vymřelý
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
je	být	k5eAaImIp3nS	být
však	však	k9	však
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
,	,	kIx,	,
odhadem	odhad	k1gInSc7	odhad
více	hodně	k6eAd2	hodně
než	než	k8xS	než
11	[number]	k4	11
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Smysly	smysl	k1gInPc4	smysl
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInSc4d1	hlavní
smysl	smysl	k1gInSc4	smysl
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
je	být	k5eAaImIp3nS	být
zrak	zrak	k1gInSc4	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Dokonalé	dokonalý	k2eAgNnSc4d1	dokonalé
komorové	komorový	k2eAgNnSc4d1	komorové
oči	oko	k1gNnPc4	oko
dvoužábrých	dvoužábrý	k2eAgMnPc2d1	dvoužábrý
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
mají	mít	k5eAaImIp3nP	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
stavbu	stavba	k1gFnSc4	stavba
jako	jako	k8xC	jako
oči	oko	k1gNnPc4	oko
savčí	savčí	k2eAgNnPc4d1	savčí
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
nim	on	k3xPp3gMnPc3	on
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
obrácené	obrácený	k2eAgNnSc1d1	obrácené
uspořádání	uspořádání	k1gNnSc1	uspořádání
vrstev	vrstva	k1gFnPc2	vrstva
sítnice	sítnice	k1gFnSc2	sítnice
(	(	kIx(	(
<g/>
světločivné	světločivný	k2eAgFnPc1d1	světločivná
buňky	buňka	k1gFnPc1	buňka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
nervy	nerv	k1gInPc1	nerv
a	a	k8xC	a
cévní	cévní	k2eAgNnPc1d1	cévní
zásobení	zásobení	k1gNnPc1	zásobení
až	až	k6eAd1	až
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemají	mít	k5eNaImIp3nP	mít
slepou	slepý	k2eAgFnSc4d1	slepá
skvrnu	skvrna	k1gFnSc4	skvrna
(	(	kIx(	(
<g/>
vizte	vidět	k5eAaImRp2nP	vidět
obrázek	obrázek	k1gInSc4	obrázek
vpravo	vpravo	k6eAd1	vpravo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
oči	oko	k1gNnPc4	oko
obřích	obří	k2eAgFnPc2d1	obří
chobotnic	chobotnice	k1gFnPc2	chobotnice
největší	veliký	k2eAgFnPc4d3	veliký
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
živočišné	živočišný	k2eAgFnSc6d1	živočišná
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc1	průměr
některých	některý	k3yIgInPc2	některý
je	být	k5eAaImIp3nS	být
až	až	k9	až
40	[number]	k4	40
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Starobylá	starobylý	k2eAgFnSc1d1	starobylá
loděnka	loděnka	k1gFnSc1	loděnka
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
štěrbinové	štěrbinový	k2eAgNnSc1d1	štěrbinové
oči	oko	k1gNnPc1	oko
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
naplněné	naplněný	k2eAgFnPc1d1	naplněná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
sítnici	sítnice	k1gFnSc4	sítnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
čočku	čočka	k1gFnSc4	čočka
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
ramenech	rameno	k1gNnPc6	rameno
mají	mít	k5eAaImIp3nP	mít
smyslová	smyslový	k2eAgNnPc4d1	smyslové
čidla	čidlo	k1gNnPc4	čidlo
hmatu	hmat	k1gInSc2	hmat
a	a	k8xC	a
chuti	chuť	k1gFnSc2	chuť
<g/>
,	,	kIx,	,
vnímají	vnímat	k5eAaImIp3nP	vnímat
slanost	slanost	k1gFnSc4	slanost
<g/>
,	,	kIx,	,
kyselost	kyselost	k1gFnSc4	kyselost
a	a	k8xC	a
hořkost	hořkost	k1gFnSc4	hořkost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mají	mít	k5eAaImIp3nP	mít
statokinetická	statokinetický	k2eAgNnPc1d1	statokinetický
čidla	čidlo	k1gNnPc1	čidlo
polohy	poloha	k1gFnSc2	poloha
a	a	k8xC	a
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Inteligence	inteligence	k1gFnSc2	inteligence
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
samotáři	samotář	k1gMnPc1	samotář
<g/>
,	,	kIx,	,
jen	jen	k9	jen
část	část	k1gFnSc1	část
olihní	oliheň	k1gFnPc2	oliheň
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
hejnech	hejno	k1gNnPc6	hejno
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgInPc1	žádný
opravdu	opravdu	k6eAd1	opravdu
sociální	sociální	k2eAgInPc1d1	sociální
druhy	druh	k1gInPc1	druh
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
velmi	velmi	k6eAd1	velmi
krátce	krátce	k6eAd1	krátce
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
uhynou	uhynout	k5eAaPmIp3nP	uhynout
po	po	k7c6	po
páření	páření	k1gNnSc6	páření
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
věci	věc	k1gFnPc4	věc
by	by	kYmCp3nS	by
obyčejně	obyčejně	k6eAd1	obyčejně
vyloučily	vyloučit	k5eAaPmAgFnP	vyloučit
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
chobotnice	chobotnice	k1gFnSc1	chobotnice
<g/>
,	,	kIx,	,
inteligentní	inteligentní	k2eAgMnPc1d1	inteligentní
jsou	být	k5eAaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
nejdokonalejší	dokonalý	k2eAgFnSc1d3	nejdokonalejší
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
smysly	smysl	k1gInPc1	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
komorové	komorový	k2eAgNnSc1d1	komorové
oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
vyvinutější	vyvinutý	k2eAgFnPc1d2	vyvinutější
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
savčí	savčí	k2eAgInSc1d1	savčí
<g/>
.	.	kIx.	.
</s>
<s>
Barvu	barva	k1gFnSc4	barva
pokožky	pokožka	k1gFnSc2	pokožka
mohou	moct	k5eAaImIp3nP	moct
měnit	měnit	k5eAaImF	měnit
nejen	nejen	k6eAd1	nejen
kvůli	kvůli	k7c3	kvůli
maskování	maskování	k1gNnSc3	maskování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
komunikaci	komunikace	k1gFnSc3	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
zbarvením	zbarvení	k1gNnSc7	zbarvení
pokožky	pokožka	k1gFnSc2	pokožka
své	svůj	k3xOyFgFnSc2	svůj
emoce	emoce	k1gFnSc2	emoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Inteligence	inteligence	k1gFnSc1	inteligence
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
se	se	k3xPyFc4	se
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
jejich	jejich	k3xOp3gFnSc2	jejich
konkurence	konkurence	k1gFnSc2	konkurence
<g/>
,	,	kIx,	,
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Chobotnice	chobotnice	k1gFnPc1	chobotnice
jsou	být	k5eAaImIp3nP	být
nejinteligentnější	inteligentní	k2eAgFnPc1d3	nejinteligentnější
<g/>
,	,	kIx,	,
sépie	sépie	k1gFnPc1	sépie
a	a	k8xC	a
olihně	oliheň	k1gFnPc1	oliheň
také	také	k9	také
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
známky	známka	k1gFnPc1	známka
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
prvohorní	prvohorní	k2eAgMnPc1d1	prvohorní
zástupci	zástupce	k1gMnPc1	zástupce
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vládli	vládnout	k5eAaImAgMnP	vládnout
tehdejším	tehdejší	k2eAgNnSc7d1	tehdejší
mořím	mořit	k5eAaImIp1nS	mořit
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
příliš	příliš	k6eAd1	příliš
inteligentní	inteligentní	k2eAgMnPc1d1	inteligentní
nebyli	být	k5eNaImAgMnP	být
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
loděnka	loděnka	k1gFnSc1	loděnka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
"	"	kIx"	"
<g/>
živoucí	živoucí	k2eAgFnSc7d1	živoucí
zkamenělinou	zkamenělina	k1gFnSc7	zkamenělina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
památkou	památka	k1gFnSc7	památka
na	na	k7c4	na
vyhynulé	vyhynulý	k2eAgInPc4d1	vyhynulý
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Inteligence	inteligence	k1gFnSc1	inteligence
chobotnic	chobotnice	k1gFnPc2	chobotnice
===	===	k?	===
</s>
</p>
<p>
<s>
Chobotnice	chobotnice	k1gFnPc1	chobotnice
dovedou	dovést	k5eAaPmIp3nP	dovést
měnit	měnit	k5eAaImF	měnit
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
své	svůj	k3xOyFgFnSc2	svůj
strategie	strategie	k1gFnSc2	strategie
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
kořisti	kořist	k1gFnSc2	kořist
a	a	k8xC	a
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgInPc3	který
patří	patřit	k5eAaImIp3nP	patřit
útoky	útok	k1gInPc1	útok
na	na	k7c4	na
kraby	krab	k1gInPc4	krab
<g/>
,	,	kIx,	,
otevírání	otevírání	k1gNnSc1	otevírání
lastur	lastura	k1gFnPc2	lastura
atd.	atd.	kA	atd.
Staví	stavit	k5eAaImIp3nS	stavit
si	se	k3xPyFc3	se
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
obydlí	obydlí	k1gNnPc2	obydlí
z	z	k7c2	z
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
materiálů	materiál	k1gInPc2	materiál
někdy	někdy	k6eAd1	někdy
i	i	k9	i
s	s	k7c7	s
vchodem	vchod	k1gInSc7	vchod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
případě	případ	k1gInSc6	případ
hrozícího	hrozící	k2eAgNnSc2d1	hrozící
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
zatarasit	zatarasit	k5eAaPmF	zatarasit
<g/>
.	.	kIx.	.
</s>
<s>
Využívají	využívat	k5eAaImIp3nP	využívat
skalních	skalní	k2eAgFnPc2d1	skalní
jeskyní	jeskyně	k1gFnPc2	jeskyně
a	a	k8xC	a
přizpůsobují	přizpůsobovat	k5eAaImIp3nP	přizpůsobovat
se	se	k3xPyFc4	se
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
obydlí	obydlí	k1gNnSc2	obydlí
si	se	k3xPyFc3	se
nosí	nosit	k5eAaImIp3nS	nosit
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
lesklé	lesklý	k2eAgInPc4d1	lesklý
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Chobotnice	chobotnice	k1gFnPc1	chobotnice
si	se	k3xPyFc3	se
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
učit	učit	k5eAaImF	učit
napodobováním	napodobování	k1gNnSc7	napodobování
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
samotářů	samotář	k1gMnPc2	samotář
přinejmenším	přinejmenším	k6eAd1	přinejmenším
podivné	podivný	k2eAgNnSc1d1	podivné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
byly	být	k5eAaImAgInP	být
pozorovány	pozorován	k2eAgInPc1d1	pozorován
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
otevírají	otevírat	k5eAaImIp3nP	otevírat
zašroubované	zašroubovaný	k2eAgFnPc1d1	zašroubovaná
láhve	láhev	k1gFnPc1	láhev
s	s	k7c7	s
potravou	potrava	k1gFnSc7	potrava
a	a	k8xC	a
řeší	řešit	k5eAaImIp3nP	řešit
další	další	k2eAgInPc4d1	další
podobně	podobně	k6eAd1	podobně
složité	složitý	k2eAgInPc4d1	složitý
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Rakouský	rakouský	k2eAgMnSc1d1	rakouský
etolog	etolog	k1gMnSc1	etolog
Hans	Hans	k1gMnSc1	Hans
Fricke	Frick	k1gMnSc4	Frick
sledoval	sledovat	k5eAaImAgMnS	sledovat
chobotnici	chobotnice	k1gFnSc4	chobotnice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dokázala	dokázat	k5eAaPmAgFnS	dokázat
manipulovat	manipulovat	k5eAaImF	manipulovat
s	s	k7c7	s
průhlednými	průhledný	k2eAgFnPc7d1	průhledná
přepážkami	přepážka	k1gFnPc7	přepážka
oddělujícími	oddělující	k2eAgFnPc7d1	oddělující
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnSc2	část
akvária	akvárium	k1gNnSc2	akvárium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
umí	umět	k5eAaImIp3nS	umět
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
mezi	mezi	k7c7	mezi
různými	různý	k2eAgNnPc7d1	různé
geometrickými	geometrický	k2eAgNnPc7d1	geometrické
tělesy	těleso	k1gNnPc7	těleso
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
stejnými	stejná	k1gFnPc7	stejná
<g/>
,	,	kIx,	,
s	s	k7c7	s
odlišným	odlišný	k2eAgInSc7d1	odlišný
povrchem	povrch	k1gInSc7	povrch
<g/>
;	;	kIx,	;
také	také	k9	také
rozpozná	rozpoznat	k5eAaPmIp3nS	rozpoznat
nepatrné	patrný	k2eNgFnPc4d1	patrný
koncentrace	koncentrace	k1gFnPc4	koncentrace
látek	látka	k1gFnPc2	látka
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
si	se	k3xPyFc3	se
vytvořit	vytvořit	k5eAaPmF	vytvořit
podmíněné	podmíněný	k2eAgFnPc4d1	podmíněná
reakce	reakce	k1gFnPc4	reakce
na	na	k7c4	na
určité	určitý	k2eAgInPc4d1	určitý
podněty	podnět	k1gInPc4	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
objeví	objevit	k5eAaPmIp3nS	objevit
určitý	určitý	k2eAgInSc4d1	určitý
předmět	předmět	k1gInSc4	předmět
spojený	spojený	k2eAgInSc4d1	spojený
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
chobotnice	chobotnice	k1gFnSc1	chobotnice
dostane	dostat	k5eAaPmIp3nS	dostat
elektrošok	elektrošok	k1gInSc4	elektrošok
<g/>
,	,	kIx,	,
příště	příště	k6eAd1	příště
se	se	k3xPyFc4	se
potravy	potrava	k1gFnPc1	potrava
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
předmětem	předmět	k1gInSc7	předmět
nedotkne	dotknout	k5eNaPmIp3nS	dotknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Inteligenci	inteligence	k1gFnSc4	inteligence
chobotnic	chobotnice	k1gFnPc2	chobotnice
srovnávají	srovnávat	k5eAaImIp3nP	srovnávat
různí	různý	k2eAgMnPc1d1	různý
vědci	vědec	k1gMnPc1	vědec
s	s	k7c7	s
různě	různě	k6eAd1	různě
vyspělými	vyspělý	k2eAgMnPc7d1	vyspělý
savci	savec	k1gMnPc7	savec
<g/>
.	.	kIx.	.
</s>
<s>
Někdo	někdo	k3yInSc1	někdo
ji	on	k3xPp3gFnSc4	on
dává	dávat	k5eAaImIp3nS	dávat
na	na	k7c4	na
roveň	roveň	k1gFnSc4	roveň
hlodavcům	hlodavec	k1gMnPc3	hlodavec
<g/>
,	,	kIx,	,
někdo	někdo	k3yInSc1	někdo
ji	on	k3xPp3gFnSc4	on
srovnává	srovnávat	k5eAaImIp3nS	srovnávat
i	i	k9	i
s	s	k7c7	s
delfíny	delfín	k1gMnPc7	delfín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Mají	mít	k5eAaImIp3nP	mít
přímý	přímý	k2eAgInSc4d1	přímý
vývin	vývin	k1gInSc4	vývin
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
po	po	k7c6	po
páření	páření	k1gNnSc6	páření
hynou	hynout	k5eAaImIp3nP	hynout
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gInSc1	samec
hektokotylovým	hektokotylový	k2eAgNnSc7d1	hektokotylový
chapadlem	chapadlo	k1gNnSc7	chapadlo
přenese	přenést	k5eAaPmIp3nS	přenést
spermie	spermie	k1gFnSc1	spermie
k	k	k7c3	k
samici	samice	k1gFnSc3	samice
a	a	k8xC	a
následně	následně	k6eAd1	následně
obvykle	obvykle	k6eAd1	obvykle
uhyne	uhynout	k5eAaPmIp3nS	uhynout
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
hyne	hynout	k5eAaImIp3nS	hynout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
krakatice	krakatice	k1gFnSc1	krakatice
Gonatus	Gonatus	k1gInSc1	Gonatus
onyx	onyx	k1gInSc1	onyx
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
svá	svůj	k3xOyFgNnPc4	svůj
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
.	.	kIx.	.
<g/>
Argonaut	argonaut	k1gMnSc1	argonaut
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
hektokotylovým	hektokotylový	k2eAgNnSc7d1	hektokotylový
ramenem	rameno	k1gNnSc7	rameno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
spermií	spermie	k1gFnPc2	spermie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rameno	rameno	k1gNnSc1	rameno
se	se	k3xPyFc4	se
během	během	k7c2	během
páření	páření	k1gNnSc2	páření
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
schopné	schopný	k2eAgNnSc1d1	schopné
volného	volný	k2eAgInSc2d1	volný
pohybu	pohyb	k1gInSc2	pohyb
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
ještě	ještě	k6eAd1	ještě
dlouho	dlouho	k6eAd1	dlouho
uvnitř	uvnitř	k7c2	uvnitř
samice	samice	k1gFnSc2	samice
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
To	ten	k3xDgNnSc1	ten
zmýlilo	zmýlit	k5eAaPmAgNnS	zmýlit
i	i	k9	i
francouzského	francouzský	k2eAgMnSc2d1	francouzský
přírodovědce	přírodovědec	k1gMnSc2	přírodovědec
Cuviera	Cuvier	k1gMnSc2	Cuvier
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k8xS	jako
cizopasného	cizopasný	k2eAgMnSc2d1	cizopasný
červa	červ	k1gMnSc2	červ
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Hectocotylus	Hectocotylus	k1gInSc1	Hectocotylus
(	(	kIx(	(
<g/>
se	se	k3xPyFc4	se
stem	sto	k4xCgNnSc7	sto
přísavek	přísavka	k1gFnPc2	přísavka
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgMnPc2d1	jiný
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
se	se	k3xPyFc4	se
samice	samice	k1gFnSc1	samice
argonautů	argonaut	k1gMnPc2	argonaut
mohou	moct	k5eAaImIp3nP	moct
pářit	pářit	k5eAaImF	pářit
vícekrát	vícekrát	k6eAd1	vícekrát
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samec	samec	k1gInSc4	samec
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Význam	význam	k1gInSc1	význam
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
jsou	být	k5eAaImIp3nP	být
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
pochoutkou	pochoutka	k1gFnSc7	pochoutka
ve	v	k7c6	v
středomořských	středomořský	k2eAgFnPc6d1	středomořská
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
i	i	k9	i
jinde	jinde	k6eAd1	jinde
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chobotnice	chobotnice	k1gFnPc1	chobotnice
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
používány	používat	k5eAaImNgInP	používat
při	při	k7c6	při
pokusech	pokus	k1gInPc6	pokus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odraz	odraz	k1gInSc4	odraz
kultuře	kultura	k1gFnSc3	kultura
a	a	k8xC	a
v	v	k7c6	v
mytologii	mytologie	k1gFnSc6	mytologie
===	===	k?	===
</s>
</p>
<p>
<s>
Obrovští	obrovský	k2eAgMnPc1d1	obrovský
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
jsou	být	k5eAaImIp3nP	být
oblíbenými	oblíbený	k2eAgFnPc7d1	oblíbená
obludami	obluda	k1gFnPc7	obluda
mnoha	mnoho	k4c2	mnoho
hollywoodských	hollywoodský	k2eAgInPc2d1	hollywoodský
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bájný	bájný	k2eAgMnSc1d1	bájný
obří	obří	k2eAgMnSc1d1	obří
hlavonožec	hlavonožec	k1gMnSc1	hlavonožec
severské	severský	k2eAgFnSc2d1	severská
mytologie	mytologie	k1gFnSc2	mytologie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kraken	kraken	k1gInSc1	kraken
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
napadení	napadení	k1gNnSc2	napadení
trosečníků	trosečník	k1gMnPc2	trosečník
na	na	k7c6	na
vorech	vor	k1gInPc6	vor
obřími	obří	k2eAgMnPc7d1	obří
hlavonožci	hlavonožec	k1gMnPc7	hlavonožec
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
jsou	být	k5eAaImIp3nP	být
podnes	podnes	k6eAd1	podnes
obdobné	obdobný	k2eAgFnPc1d1	obdobná
zprávy	zpráva	k1gFnPc1	zpráva
přijímány	přijímat	k5eAaImNgFnP	přijímat
poměrně	poměrně	k6eAd1	poměrně
rezervovaně	rezervovaně	k6eAd1	rezervovaně
<g/>
,	,	kIx,	,
a	a	k8xC	a
ač	ač	k8xS	ač
je	být	k5eAaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
nepatří	patřit	k5eNaImIp3nS	patřit
do	do	k7c2	do
typického	typický	k2eAgInSc2d1	typický
jídelníčku	jídelníček	k1gInSc2	jídelníček
mořských	mořský	k2eAgMnPc2d1	mořský
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejde	jít	k5eNaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
lidovou	lidový	k2eAgFnSc4d1	lidová
slovesnost	slovesnost	k1gFnSc4	slovesnost
<g/>
,	,	kIx,	,
či	či	k8xC	či
výplody	výplod	k1gInPc1	výplod
myslí	mysl	k1gFnPc2	mysl
vystavených	vystavený	k2eAgFnPc2d1	vystavená
útrapám	útrapa	k1gFnPc3	útrapa
mezních	mezní	k2eAgFnPc2d1	mezní
situací	situace	k1gFnPc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obdobným	obdobný	k2eAgInPc3d1	obdobný
"	"	kIx"	"
<g/>
omylům	omyl	k1gInPc3	omyl
<g/>
"	"	kIx"	"
v	v	k7c6	v
potravních	potravní	k2eAgFnPc6d1	potravní
zvyklostech	zvyklost	k1gFnPc6	zvyklost
mořských	mořský	k2eAgMnPc2d1	mořský
živočichů	živočich	k1gMnPc2	živočich
dochází	docházet	k5eAaImIp3nS	docházet
kupříkladu	kupříkladu	k6eAd1	kupříkladu
u	u	k7c2	u
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c4	na
siluetu	silueta	k1gFnSc4	silueta
člověka	člověk	k1gMnSc2	člověk
pohybujícího	pohybující	k2eAgMnSc2d1	pohybující
se	se	k3xPyFc4	se
při	při	k7c6	při
hladině	hladina	k1gFnSc6	hladina
útočí	útočit	k5eAaImIp3nS	útočit
nejspíše	nejspíše	k9	nejspíše
v	v	k7c6	v
omylu	omyl	k1gInSc6	omyl
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
želvu	želva	k1gFnSc4	želva
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
žraloci	žralok	k1gMnPc1	žralok
prokazatelně	prokazatelně	k6eAd1	prokazatelně
živí	živit	k5eAaImIp3nP	živit
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
zápolí	zápolit	k5eAaImIp3nP	zápolit
s	s	k7c7	s
nepoměrně	poměrně	k6eNd1	poměrně
většími	veliký	k2eAgMnPc7d2	veliký
soupeři	soupeř	k1gMnPc7	soupeř
<g/>
,	,	kIx,	,
vorvani	vorvaň	k1gMnPc7	vorvaň
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
příběhy	příběh	k1gInPc1	příběh
mají	mít	k5eAaImIp3nP	mít
reálný	reálný	k2eAgInSc4d1	reálný
základ	základ	k1gInSc4	základ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kryptozoologie	Kryptozoologie	k1gFnSc1	Kryptozoologie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hlavonožec	hlavonožec	k1gMnSc1	hlavonožec
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Cephalopoda	Cephalopod	k1gMnSc2	Cephalopod
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Dichotomický	dichotomický	k2eAgInSc1d1	dichotomický
klíč	klíč	k1gInSc1	klíč
<g/>
/	/	kIx~	/
<g/>
Cephalopoda	Cephalopoda	k1gFnSc1	Cephalopoda
ve	v	k7c6	v
Wikiknihách	Wikikniha	k1gFnPc6	Wikikniha
</s>
</p>
<p>
<s>
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
(	(	kIx(	(
<g/>
BioLib	BioLib	k1gMnSc1	BioLib
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
a	a	k8xC	a
inteligence	inteligence	k1gFnSc1	inteligence
chobotnic	chobotnice	k1gFnPc2	chobotnice
(	(	kIx(	(
<g/>
Kryptozoologie	Kryptozoologie	k1gFnSc1	Kryptozoologie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Krakeni	Kraken	k2eAgMnPc1d1	Kraken
moří	moře	k1gNnPc2	moře
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Kryptozoologie	Kryptozoologie	k1gFnSc1	Kryptozoologie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chobotnice	chobotnice	k1gFnPc1	chobotnice
mají	mít	k5eAaImIp3nP	mít
oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
chapadlo	chapadlo	k1gNnSc4	chapadlo
(	(	kIx(	(
<g/>
osel	osel	k1gMnSc1	osel
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
</s>
</p>
