<s>
Protektor	protektor	k1gInSc1	protektor
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Marka	Marek	k1gMnSc2	Marek
Najbrta	Najbrt	k1gMnSc2	Najbrt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
získal	získat	k5eAaPmAgInS	získat
šest	šest	k4xCc4	šest
Českých	český	k2eAgInPc2d1	český
lvů	lev	k1gInPc2	lev
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
střih	střih	k1gInSc4	střih
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc4d1	hlavní
ženskou	ženský	k2eAgFnSc4d1	ženská
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
těžiště	těžiště	k1gNnSc1	těžiště
příběhu	příběh	k1gInSc2	příběh
ale	ale	k8xC	ale
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c4	o
manželské	manželský	k2eAgFnPc4d1	manželská
dvojici	dvojice	k1gFnSc4	dvojice
Emilovi	Emil	k1gMnSc3	Emil
a	a	k8xC	a
Haně	Hana	k1gFnSc3	Hana
Vrbatových	Vrbatův	k2eAgFnPc2d1	Vrbatova
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
je	být	k5eAaImIp3nS	být
Hana	Hana	k1gFnSc1	Hana
(	(	kIx(	(
<g/>
Jana	Jana	k1gFnSc1	Jana
Plodková	Plodkový	k2eAgFnSc1d1	Plodková
<g/>
)	)	kIx)	)
herečkou	herečka	k1gFnSc7	herečka
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
židovský	židovský	k2eAgInSc4d1	židovský
původ	původ	k1gInSc4	původ
nemůže	moct	k5eNaImIp3nS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Pracovník	pracovník	k1gMnSc1	pracovník
rozhlasu	rozhlas	k1gInSc2	rozhlas
Emil	Emil	k1gMnSc1	Emil
(	(	kIx(	(
<g/>
Marek	Marek	k1gMnSc1	Marek
Daniel	Daniel	k1gMnSc1	Daniel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
reportérem	reportér	k1gMnSc7	reportér
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
uvězněn	uvězněn	k2eAgMnSc1d1	uvězněn
jeho	jeho	k3xOp3gMnSc1	jeho
kolega	kolega	k1gMnSc1	kolega
Franta	Franta	k1gMnSc1	Franta
(	(	kIx(	(
<g/>
Martin	Martin	k1gMnSc1	Martin
Myšička	myšička	k1gFnSc1	myšička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
snaží	snažit	k5eAaImIp3nS	snažit
zachránit	zachránit	k5eAaPmF	zachránit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
německé	německý	k2eAgFnSc3d1	německá
propagandě	propaganda	k1gFnSc3	propaganda
<g/>
.	.	kIx.	.
</s>
<s>
Hana	Hana	k1gFnSc1	Hana
téměř	téměř	k6eAd1	téměř
nevychází	vycházet	k5eNaImIp3nS	vycházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
začne	začít	k5eAaPmIp3nS	začít
brzy	brzy	k6eAd1	brzy
nudit	nudit	k5eAaImF	nudit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
tajně	tajně	k6eAd1	tajně
schází	scházet	k5eAaImIp3nS	scházet
s	s	k7c7	s
promítačem	promítač	k1gMnSc7	promítač
Petrem	Petr	k1gMnSc7	Petr
(	(	kIx(	(
<g/>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Měcháček	Měcháček	k1gMnSc1	Měcháček
<g/>
)	)	kIx)	)
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
kině	kino	k1gNnSc6	kino
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
dívají	dívat	k5eAaImIp3nP	dívat
na	na	k7c4	na
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
zkouší	zkoušet	k5eAaImIp3nS	zkoušet
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
účinky	účinek	k1gInPc4	účinek
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
také	také	k9	také
provokativně	provokativně	k6eAd1	provokativně
vycházejí	vycházet	k5eAaImIp3nP	vycházet
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
promítač	promítač	k1gMnSc1	promítač
Hanu	Hana	k1gFnSc4	Hana
fotografuje	fotografovat	k5eAaImIp3nS	fotografovat
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
je	být	k5eAaImIp3nS	být
Židům	Žid	k1gMnPc3	Žid
vstup	vstup	k1gInSc4	vstup
zakázán	zakázán	k2eAgInSc4d1	zakázán
<g/>
.	.	kIx.	.
</s>
<s>
Emil	Emil	k1gMnSc1	Emil
zatím	zatím	k6eAd1	zatím
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Tomka	Tomek	k1gMnSc2	Tomek
(	(	kIx(	(
<g/>
Richard	Richard	k1gMnSc1	Richard
Stanke	Stank	k1gFnSc2	Stank
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začal	začít	k5eAaPmAgInS	začít
po	po	k7c6	po
Frantově	Frantův	k2eAgNnSc6d1	Frantovo
uvěznění	uvěznění	k1gNnSc6	uvěznění
chodit	chodit	k5eAaImF	chodit
s	s	k7c7	s
Frantovou	Frantův	k2eAgFnSc7d1	Frantova
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Věrou	Věra	k1gFnSc7	Věra
(	(	kIx(	(
<g/>
Klára	Klára	k1gFnSc1	Klára
Melíšková	Melíšková	k1gFnSc1	Melíšková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Emil	Emil	k1gMnSc1	Emil
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
slavným	slavný	k2eAgInSc7d1	slavný
a	a	k8xC	a
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
naivní	naivní	k2eAgFnPc1d1	naivní
novinářka	novinářka	k1gFnSc1	novinářka
Krista	Krista	k1gFnSc1	Krista
(	(	kIx(	(
<g/>
Sandra	Sandra	k1gFnSc1	Sandra
Nováková	Nováková	k1gFnSc1	Nováková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hana	Hana	k1gFnSc1	Hana
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
Věry	Věra	k1gFnSc2	Věra
a	a	k8xC	a
Tomka	Tomek	k1gMnSc2	Tomek
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
rozčílí	rozčílit	k5eAaPmIp3nS	rozčílit
Tomka	Tomek	k1gMnSc2	Tomek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
požaduje	požadovat	k5eAaImIp3nS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Emil	Emil	k1gMnSc1	Emil
s	s	k7c7	s
Hanou	Hana	k1gFnSc7	Hana
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
pak	pak	k6eAd1	pak
Emil	Emil	k1gMnSc1	Emil
stráví	strávit	k5eAaPmIp3nS	strávit
noc	noc	k1gFnSc4	noc
u	u	k7c2	u
Kristy	Krista	k1gFnSc2	Krista
a	a	k8xC	a
ráno	ráno	k6eAd1	ráno
zaspí	zaspat	k5eAaPmIp3nS	zaspat
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zrovna	zrovna	k6eAd1	zrovna
o	o	k7c4	o
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
to	ten	k3xDgNnSc4	ten
ale	ale	k9	ale
neví	vědět	k5eNaImIp3nS	vědět
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
nepřišel	přijít	k5eNaPmAgInS	přijít
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
o	o	k7c4	o
tolik	tolik	k4yIc4	tolik
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
vezme	vzít	k5eAaPmIp3nS	vzít
si	se	k3xPyFc3	se
před	před	k7c7	před
Kristiným	Kristin	k2eAgInSc7d1	Kristin
domem	dům	k1gInSc7	dům
kolo	kolo	k1gNnSc1	kolo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
podobá	podobat	k5eAaImIp3nS	podobat
kolu	kola	k1gFnSc4	kola
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
pátrání	pátrání	k1gNnSc1	pátrání
<g/>
.	.	kIx.	.
</s>
<s>
Kolo	kolo	k1gNnSc1	kolo
pak	pak	k6eAd1	pak
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bytě	byt	k1gInSc6	byt
<g/>
,	,	kIx,	,
gestapo	gestapo	k1gNnSc1	gestapo
jej	on	k3xPp3gMnSc4	on
během	během	k7c2	během
prohlídky	prohlídka	k1gFnSc2	prohlídka
neobjeví	objevit	k5eNaPmIp3nS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Gestapo	gestapo	k1gNnSc1	gestapo
zabije	zabít	k5eAaPmIp3nS	zabít
Frantu	Franta	k1gMnSc4	Franta
i	i	k8xC	i
Petra	Petr	k1gMnSc4	Petr
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
státního	státní	k2eAgInSc2d1	státní
smutku	smutek	k1gInSc2	smutek
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
drog	droga	k1gFnPc2	droga
promítal	promítat	k5eAaImAgInS	promítat
v	v	k7c6	v
kině	kino	k1gNnSc6	kino
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hana	Hana	k1gFnSc1	Hana
se	se	k3xPyFc4	se
dobrovolně	dobrovolně	k6eAd1	dobrovolně
přidá	přidat	k5eAaPmIp3nS	přidat
k	k	k7c3	k
Židům	Žid	k1gMnPc3	Žid
čekajícím	čekající	k2eAgMnPc3d1	čekající
na	na	k7c4	na
transport	transport	k1gInSc4	transport
<g/>
.	.	kIx.	.
</s>
<s>
Emil	Emil	k1gMnSc1	Emil
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
v	v	k7c6	v
rozhlasu	rozhlas	k1gInSc6	rozhlas
přečíst	přečíst	k5eAaPmF	přečíst
slib	slib	k1gInSc4	slib
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
rozhlasu	rozhlas	k1gInSc2	rozhlas
Říši	říše	k1gFnSc4	říše
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
pohřbu	pohřeb	k1gInSc2	pohřeb
Reinharda	Reinhard	k1gMnSc2	Reinhard
Heydricha	Heydrich	k1gMnSc2	Heydrich
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Protektor	protektor	k1gInSc1	protektor
byl	být	k5eAaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
za	za	k7c4	za
Česko	Česko	k1gNnSc4	Česko
ucházel	ucházet	k5eAaImAgMnS	ucházet
o	o	k7c4	o
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
film	film	k1gInSc4	film
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
filmová	filmový	k2eAgFnSc1d1	filmová
akademie	akademie	k1gFnSc1	akademie
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
nezařadila	zařadit	k5eNaPmAgFnS	zařadit
ani	ani	k8xC	ani
do	do	k7c2	do
užšího	úzký	k2eAgInSc2d2	užší
výběru	výběr	k1gInSc2	výběr
9	[number]	k4	9
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jsou	být	k5eAaImIp3nP	být
filmy	film	k1gInPc1	film
nominovány	nominován	k2eAgInPc1d1	nominován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cenách	cena	k1gFnPc6	cena
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
2009	[number]	k4	2009
získal	získat	k5eAaPmAgInS	získat
Protektor	protektor	k1gInSc1	protektor
11	[number]	k4	11
nominací	nominace	k1gFnPc2	nominace
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
proměnil	proměnit	k5eAaPmAgMnS	proměnit
6	[number]	k4	6
(	(	kIx(	(
<g/>
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
střih	střih	k1gInSc1	střih
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc1d1	hlavní
ženská	ženský	k2eAgFnSc1d1	ženská
role	role	k1gFnSc1	role
pro	pro	k7c4	pro
Janu	Jana	k1gFnSc4	Jana
Plodkovou	Plodkový	k2eAgFnSc4d1	Plodková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soundtrack	soundtrack	k1gInSc1	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
6	[number]	k4	6
skladeb	skladba	k1gFnPc2	skladba
(	(	kIx(	(
<g/>
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
79	[number]	k4	79
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
na	na	k7c6	na
webu	web	k1gInSc6	web
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
Krzysztofa	Krzysztof	k1gMnSc2	Krzysztof
Kieslowskeho	Kieslowske	k1gMnSc2	Kieslowske
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
hraný	hraný	k2eAgInSc4d1	hraný
film	film	k1gInSc4	film
na	na	k7c4	na
Starz	Starz	k1gInSc4	Starz
Denver	Denver	k1gInSc1	Denver
Film	film	k1gInSc1	film
Festival	festival	k1gInSc1	festival
2009	[number]	k4	2009
Ondřej	Ondřej	k1gMnSc1	Ondřej
Vosmík	Vosmík	k1gMnSc1	Vosmík
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Kamil	Kamil	k1gMnSc1	Kamil
Fila	Fila	k1gMnSc1	Fila
<g/>
:	:	kIx,	:
Protektor	protektor	k1gMnSc1	protektor
zachránil	zachránit	k5eAaPmAgMnS	zachránit
český	český	k2eAgInSc4d1	český
film	film	k1gInSc4	film
od	od	k7c2	od
provinčnosti	provinčnost	k1gFnSc2	provinčnost
<g/>
,	,	kIx,	,
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g />
.	.	kIx.	.
</s>
<s>
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
Vít	Víta	k1gFnPc2	Víta
Schmarc	Schmarc	k1gInSc1	Schmarc
<g/>
:	:	kIx,	:
Najbrtův	Najbrtův	k2eAgInSc1d1	Najbrtův
Protektor	protektor	k1gInSc1	protektor
konečně	konečně	k6eAd1	konečně
klade	klást	k5eAaImIp3nS	klást
správné	správný	k2eAgFnPc4d1	správná
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
Rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
Viktor	Viktor	k1gMnSc1	Viktor
Palák	Palák	k1gMnSc1	Palák
<g/>
,	,	kIx,	,
Cinepur	Cinepur	k1gMnSc1	Cinepur
66	[number]	k4	66
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
strana	strana	k1gFnSc1	strana
45	[number]	k4	45
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Fuka	Fuka	k1gMnSc1	Fuka
<g/>
,	,	kIx,	,
FFFilm	FFFilm	k1gMnSc1	FFFilm
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
Protektor	protektor	k1gMnSc1	protektor
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
Protektor	protektor	k1gInSc4	protektor
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
