<s>
Zasloužil	zasloužit	k5eAaPmAgMnS
se	se	k3xPyFc4
o	o	k7c4
rozluštění	rozluštění	k1gNnSc4
lineárního	lineární	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
B	B	kA
roku	rok	k1gInSc2
1952	[number]	k4
a	a	k8xC
tím	ten	k3xDgNnSc7
k	k	k7c3
zásadnímu	zásadní	k2eAgInSc3d1
pokroku	pokrok	k1gInSc3
v	v	k7c6
poznání	poznání	k1gNnSc6
mykénská	mykénský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
<g/>
,	,	kIx,
kolébky	kolébka	k1gFnSc2
kultury	kultura	k1gFnSc2
starověkého	starověký	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>