<s>
Michael	Michael	k1gMnSc1	Michael
Ventris	Ventris	k1gFnSc2	Ventris
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1922	[number]	k4	1922
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
amatérský	amatérský	k2eAgMnSc1d1	amatérský
filolog	filolog	k1gMnSc1	filolog
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
rozluštění	rozluštění	k1gNnSc4	rozluštění
lineárního	lineární	k2eAgNnSc2d1	lineární
písma	písmo	k1gNnSc2	písmo
B	B	kA	B
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
k	k	k7c3	k
zásadnímu	zásadní	k2eAgInSc3d1	zásadní
pokroku	pokrok	k1gInSc3	pokrok
v	v	k7c6	v
poznání	poznání	k1gNnSc6	poznání
mykénská	mykénský	k2eAgFnSc1d1	mykénská
civilizace	civilizace	k1gFnSc1	civilizace
<g/>
,	,	kIx,	,
kolébky	kolébka	k1gFnSc2	kolébka
kultury	kultura	k1gFnSc2	kultura
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Ventris	Ventris	k1gInSc1	Ventris
měl	mít	k5eAaImAgInS	mít
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
talent	talent	k1gInSc4	talent
na	na	k7c4	na
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
naučil	naučit	k5eAaPmAgMnS	naučit
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
polsky	polsky	k6eAd1	polsky
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
poloviční	poloviční	k2eAgFnSc1d1	poloviční
Polka	Polka	k1gFnSc1	Polka
<g/>
)	)	kIx)	)
a	a	k8xC	a
pak	pak	k6eAd1	pak
další	další	k2eAgInPc4d1	další
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgNnSc2	který
ale	ale	k9	ale
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
počátek	počátek	k1gInSc4	počátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Ventris	Ventris	k1gInSc1	Ventris
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
letecký	letecký	k2eAgInSc1d1	letecký
navigátor	navigátor	k1gInSc1	navigátor
<g/>
.	.	kIx.	.
</s>
<s>
Školu	škola	k1gFnSc4	škola
dokončil	dokončit	k5eAaPmAgMnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
a	a	k8xC	a
pak	pak	k6eAd1	pak
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
architekt	architekt	k1gMnSc1	architekt
nových	nový	k2eAgFnPc2d1	nová
školních	školní	k2eAgFnPc2d1	školní
budov	budova	k1gFnPc2	budova
na	na	k7c4	na
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
nerozluštěné	rozluštěný	k2eNgFnPc4d1	nerozluštěná
mykénské	mykénský	k2eAgFnPc4d1	mykénská
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
písma	písmo	k1gNnPc4	písmo
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
již	již	k6eAd1	již
ve	v	k7c6	v
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
přednáška	přednáška	k1gFnSc1	přednáška
slaveného	slavený	k2eAgMnSc2d1	slavený
objevitele	objevitel	k1gMnSc2	objevitel
krétského	krétský	k2eAgInSc2d1	krétský
Knóssu	Knóss	k1gInSc2	Knóss
Arthura	Arthur	k1gMnSc2	Arthur
Evanse	Evans	k1gMnSc2	Evans
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
jeho	on	k3xPp3gInSc2	on
systematického	systematický	k2eAgInSc2d1	systematický
zájmu	zájem	k1gInSc2	zájem
byl	být	k5eAaImAgInS	být
článek	článek	k1gInSc1	článek
o	o	k7c6	o
krétském	krétský	k2eAgNnSc6d1	krétské
písmu	písmo	k1gNnSc6	písmo
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
publikoval	publikovat	k5eAaBmAgInS	publikovat
prestižní	prestižní	k2eAgInSc1d1	prestižní
americký	americký	k2eAgInSc1d1	americký
archeologický	archeologický	k2eAgInSc1d1	archeologický
časopis	časopis	k1gInSc1	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vrátil	vrátit	k5eAaPmAgInS	vrátit
až	až	k9	až
po	po	k7c4	po
dokonční	dokonční	k2eAgNnPc4d1	dokonční
studia	studio	k1gNnPc4	studio
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
publikoval	publikovat	k5eAaBmAgMnS	publikovat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
bádání	bádání	k1gNnSc2	bádání
egejských	egejský	k2eAgNnPc2d1	Egejské
písem	písmo	k1gNnPc2	písmo
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
sestavil	sestavit	k5eAaPmAgInS	sestavit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dotazníku	dotazník	k1gInSc2	dotazník
rozeslaného	rozeslaný	k2eAgInSc2d1	rozeslaný
významným	významný	k2eAgMnPc3d1	významný
světovým	světový	k2eAgMnPc3d1	světový
badatelům	badatel	k1gMnPc3	badatel
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
zprávu	zpráva	k1gFnSc4	zpráva
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
poslední	poslední	k2eAgInSc4d1	poslední
příspěvek	příspěvek	k1gInSc4	příspěvek
k	k	k7c3	k
dané	daný	k2eAgFnSc3d1	daná
problematice	problematika	k1gFnSc3	problematika
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
chápáno	chápat	k5eAaImNgNnS	chápat
i	i	k9	i
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
odborníků	odborník	k1gMnPc2	odborník
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgInPc4	který
jako	jako	k9	jako
laik	laik	k1gMnSc1	laik
nebyl	být	k5eNaImAgMnS	být
počítán	počítat	k5eAaImNgMnS	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
toto	tento	k3xDgNnSc4	tento
prohlášení	prohlášení	k1gNnSc4	prohlášení
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
letech	let	k1gInPc6	let
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
systematicky	systematicky	k6eAd1	systematicky
věnuje	věnovat	k5eAaPmIp3nS	věnovat
luštění	luštění	k1gNnSc3	luštění
lineárního	lineární	k2eAgNnSc2d1	lineární
písma	písmo	k1gNnSc2	písmo
B	B	kA	B
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
závěry	závěr	k1gInPc4	závěr
si	se	k3xPyFc3	se
nenechává	nechávat	k5eNaImIp3nS	nechávat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
další	další	k2eAgMnPc4d1	další
badatele	badatel	k1gMnPc4	badatel
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
právě	právě	k9	právě
Ventris	Ventris	k1gFnSc4	Ventris
<g/>
,	,	kIx,	,
komu	kdo	k3yInSc3	kdo
se	se	k3xPyFc4	se
písmo	písmo	k1gNnSc1	písmo
podařilo	podařit	k5eAaPmAgNnS	podařit
rozlušit	rozlušit	k5eAaPmF	rozlušit
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
výzkum	výzkum	k1gInSc4	výzkum
založil	založit	k5eAaPmAgMnS	založit
na	na	k7c6	na
matematicko-statistické	matematickotatistický	k2eAgFnSc6d1	matematicko-statistická
analýze	analýza	k1gFnSc6	analýza
znaků	znak	k1gInPc2	znak
a	a	k8xC	a
výsledky	výsledek	k1gInPc4	výsledek
pak	pak	k6eAd1	pak
aplikoval	aplikovat	k5eAaBmAgMnS	aplikovat
na	na	k7c4	na
předpokládané	předpokládaný	k2eAgInPc4d1	předpokládaný
krétské	krétský	k2eAgInPc4d1	krétský
místní	místní	k2eAgInPc4d1	místní
názvy	název	k1gInPc4	název
v	v	k7c6	v
textu	text	k1gInSc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Překvapivě	překvapivě	k6eAd1	překvapivě
tak	tak	k6eAd1	tak
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
psán	psán	k2eAgInSc1d1	psán
řecky	řecky	k6eAd1	řecky
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
16	[number]	k4	16
let	léto	k1gNnPc2	léto
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
Evansovy	Evansův	k2eAgFnSc2d1	Evansova
teze	teze	k1gFnSc2	teze
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kréťané	Kréťan	k1gMnPc1	Kréťan
byli	být	k5eAaImAgMnP	být
neřečtí	řecký	k2eNgMnPc1d1	řecký
příbuzní	příbuzný	k1gMnPc1	příbuzný
Etrusků	Etrusk	k1gMnPc2	Etrusk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dalším	další	k2eAgNnSc6d1	další
luštění	luštění	k1gNnSc6	luštění
písma	písmo	k1gNnSc2	písmo
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
Ventris	Ventris	k1gInSc1	Ventris
s	s	k7c7	s
mladým	mladý	k2eAgMnSc7d1	mladý
filologem	filolog	k1gMnSc7	filolog
Johnem	John	k1gMnSc7	John
Chadwickem	Chadwick	k1gInSc7	Chadwick
<g/>
.	.	kIx.	.
</s>
<s>
Jimi	on	k3xPp3gInPc7	on
publikovaným	publikovaný	k2eAgInPc3d1	publikovaný
závěrům	závěr	k1gInPc3	závěr
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
širšího	široký	k2eAgNnSc2d2	širší
přijetí	přijetí	k1gNnSc2	přijetí
až	až	k9	až
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nových	nový	k2eAgFnPc2d1	nová
tabulek	tabulka	k1gFnPc2	tabulka
nalezených	nalezený	k2eAgFnPc2d1	nalezená
americkám	americkat	k5eAaPmIp1nS	americkat
archeologem	archeolog	k1gMnSc7	archeolog
Carla	Carl	k1gMnSc2	Carl
Blegena	Blegen	k1gMnSc2	Blegen
v	v	k7c6	v
Pylu	pyl	k1gInSc6	pyl
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
svých	svůj	k3xOyFgInPc2	svůj
výzkumů	výzkum	k1gInPc2	výzkum
oba	dva	k4xCgMnPc1	dva
mladí	mladý	k2eAgMnPc1d1	mladý
badatelé	badatel	k1gMnPc1	badatel
publikovali	publikovat	k5eAaBmAgMnP	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Documents	Documents	k1gInSc4	Documents
in	in	k?	in
Mycenaean	Mycenaean	k1gInSc1	Mycenaean
Greek	Greek	k1gInSc1	Greek
(	(	kIx(	(
<g/>
Dokumenty	dokument	k1gInPc1	dokument
v	v	k7c6	v
mykénské	mykénský	k2eAgFnSc6d1	mykénská
řečtině	řečtina	k1gFnSc6	řečtina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Ventris	Ventris	k1gFnSc2	Ventris
zemřel	zemřít	k5eAaPmAgMnS	zemřít
již	již	k6eAd1	již
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
na	na	k7c4	na
následky	následek	k1gInPc4	následek
dopravní	dopravní	k2eAgFnSc2d1	dopravní
nehody	nehoda	k1gFnSc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Metody	metoda	k1gFnPc1	metoda
a	a	k8xC	a
výsledky	výsledek	k1gInPc1	výsledek
jeho	jeho	k3xOp3gNnSc2	jeho
rozluštění	rozluštění	k1gNnSc2	rozluštění
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
staly	stát	k5eAaPmAgFnP	stát
předmětem	předmět	k1gInSc7	předmět
vědeckých	vědecký	k2eAgInPc2d1	vědecký
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
správnost	správnost	k1gFnSc1	správnost
uznána	uznán	k2eAgFnSc1d1	uznána
<g/>
.	.	kIx.	.
publikace	publikace	k1gFnSc1	publikace
autora	autor	k1gMnSc2	autor
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ventris	Ventris	k1gFnSc1	Ventris
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
and	and	k?	and
Chadwick	Chadwick	k1gMnSc1	Chadwick
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Documents	Documents	k6eAd1	Documents
in	in	k?	in
Mycenaean	Mycenaean	k1gMnSc1	Mycenaean
Greek	Greek	k1gMnSc1	Greek
<g/>
.	.	kIx.	.
</s>
<s>
Second	Second	k1gInSc1	Second
edition	edition	k1gInSc1	edition
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
UP	UP	kA	UP
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8558	[number]	k4	8558
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ventris	Ventris	k1gFnSc1	Ventris
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Work	Work	k6eAd1	Work
notes	notes	k1gInSc4	notes
on	on	k3xPp3gMnSc1	on
Minoan	Minoan	k1gMnSc1	Minoan
language	language	k1gFnSc2	language
research	research	k1gMnSc1	research
and	and	k?	and
other	other	k1gMnSc1	other
unedited	unedited	k1gMnSc1	unedited
papers	papers	k6eAd1	papers
<g/>
.	.	kIx.	.
</s>
<s>
Edizioni	Edizioň	k1gFnSc3	Edizioň
dell	dell	k1gMnSc1	dell
<g/>
'	'	kIx"	'
<g/>
Ateneo	Ateneo	k1gMnSc1	Ateneo
1988	[number]	k4	1988
Roma	Rom	k1gMnSc4	Rom
<g/>
.	.	kIx.	.
monografie	monografie	k1gFnSc2	monografie
BATONĚK	BATONĚK	kA	BATONĚK
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Egeis	Egeis	k1gFnSc1	Egeis
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Mojdl	Mojdnout	k5eAaPmAgMnS	Mojdnout
<g/>
,	,	kIx,	,
Lubor	Lubor	k1gMnSc1	Lubor
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
písem	písmo	k1gNnPc2	písmo
světa	svět	k1gInSc2	svět
I	i	k8xC	i
–	–	k?	–
Písma	písmo	k1gNnSc2	písmo
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
a	a	k8xC	a
helénské	helénský	k2eAgFnSc2d1	helénská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
str	str	kA	str
<g/>
.	.	kIx.	.
101	[number]	k4	101
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Chadwick	Chadwick	k1gMnSc1	Chadwick
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Decipherment	Decipherment	k1gInSc1	Decipherment
of	of	k?	of
Linear	Linear	k1gInSc1	Linear
B.	B.	kA	B.
Second	Second	k1gInSc1	Second
edition	edition	k1gInSc1	edition
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
39830	[number]	k4	39830
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gMnSc1	Andrew
<g/>
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Man	Man	k1gMnSc1	Man
Who	Who	k1gMnSc1	Who
Deciphered	Deciphered	k1gMnSc1	Deciphered
Linear	Linear	k1gMnSc1	Linear
B	B	kA	B
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Story	story	k1gFnSc2	story
of	of	k?	of
Michael	Michael	k1gMnSc1	Michael
Ventris	Ventris	k1gFnSc2	Ventris
<g/>
.	.	kIx.	.
</s>
<s>
Thames	Thames	k1gMnSc1	Thames
&	&	k?	&
Hudson	Hudson	k1gMnSc1	Hudson
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
51077	[number]	k4	51077
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Simon	Simon	k1gMnSc1	Simon
Tetlow	Tetlow	k1gMnSc1	Tetlow
<g/>
,	,	kIx,	,
Ben	Ben	k1gInSc1	Ben
Harris	Harris	k1gFnSc2	Harris
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Roques	Roques	k1gMnSc1	Roques
and	and	k?	and
A.	A.	kA	A.
G.	G.	kA	G.
Meredith	Meredith	k1gMnSc1	Meredith
-	-	kIx~	-
Michael	Michael	k1gMnSc1	Michael
Ventris	Ventris	k1gFnSc2	Ventris
Remembered	Remembered	k1gMnSc1	Remembered
(	(	kIx(	(
<g/>
Stowe	Stowe	k1gInSc1	Stowe
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
</s>
