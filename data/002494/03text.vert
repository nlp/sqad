<s>
Ununtrium	Ununtrium	k1gNnSc1	Ununtrium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Uut	Uut	k1gFnSc2	Uut
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Ununtrium	Ununtrium	k1gNnSc4	Ununtrium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
transuran	transuran	k1gInSc4	transuran
připravený	připravený	k2eAgInSc4d1	připravený
alfa	alfa	k1gNnSc4	alfa
rozpadem	rozpad	k1gInSc7	rozpad
ununpentia	ununpentium	k1gNnSc2	ununpentium
<g/>
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
publikoval	publikovat	k5eAaBmAgInS	publikovat
tým	tým	k1gInSc1	tým
ruských	ruský	k2eAgMnPc2d1	ruský
fyziků	fyzik	k1gMnPc2	fyzik
z	z	k7c2	z
Dubna	duben	k1gInSc2	duben
a	a	k8xC	a
amerických	americký	k2eAgMnPc2d1	americký
vědců	vědec	k1gMnPc2	vědec
z	z	k7c2	z
Lawrence	Lawrence	k1gFnSc2	Lawrence
Berkeley	Berkelea	k1gFnSc2	Berkelea
National	National	k1gFnSc2	National
Laboratory	Laborator	k1gInPc1	Laborator
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
přípravě	příprava	k1gFnSc6	příprava
ununtria	ununtrium	k1gNnSc2	ununtrium
a	a	k8xC	a
ununpentia	ununpentium	k1gNnSc2	ununpentium
<g/>
.	.	kIx.	.
</s>
