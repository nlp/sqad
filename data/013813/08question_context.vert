<s desamb="1">
Proslavil	proslavit	k5eAaPmAgMnS
se	se	k3xPyFc4
dílem	dílo	k1gNnSc7
<g/>
,	,	kIx,
jemuž	jenž	k3xRgNnSc3
dominuje	dominovat	k5eAaImIp3nS
lyrickoepická	lyrickoepický	k2eAgFnSc1d1
skladba	skladba	k1gFnSc1
Máj	máj	k1gFnSc1
(	(	kIx(
<g/>
1836	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejvydávanějších	vydávaný	k2eAgFnPc2d3
českých	český	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
</s>