<s>
Eva	Eva	k1gFnSc1
Burešová	Burešová	k1gFnSc1
</s>
<s>
Eva	Eva	k1gFnSc1
Salvatore	Salvator	k1gMnSc5
Burešová	Burešová	k1gFnSc1
Eva	Eva	k1gFnSc1
Burešová	Burešová	k1gFnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Ef	Ef	k?
Narození	narození	k1gNnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1993	#num#	k4
(	(	kIx(
<g/>
27	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Hlučín	Hlučín	k1gInSc1
Žánry	žánr	k1gInPc4
</s>
<s>
pop	pop	k1gInSc1
<g/>
,	,	kIx,
jazz	jazz	k1gInSc1
<g/>
,	,	kIx,
muzikál	muzikál	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
herečka	herečka	k1gFnSc1
<g/>
,	,	kIx,
zpěvačka	zpěvačka	k1gFnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
hlas	hlas	k1gInSc1
Aktivní	aktivní	k2eAgInSc1d1
roky	rok	k1gInPc4
</s>
<s>
2008	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
Warner	Warner	k1gMnSc1
Music	Music	k1gMnSc1
Czech	Czech	k1gMnSc1
Republic	Republice	k1gFnPc2
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Michael	Michael	k1gMnSc1
Krásný	krásný	k2eAgMnSc1d1
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
Matyáš	Matyáš	k1gMnSc1
Adamec	Adamec	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
Přemek	Přemek	k1gMnSc1
Forejt	Forejt	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
Nathaniel	Nathaniel	k1gMnSc1
Adam	Adam	k1gMnSc1
Salvatore	Salvator	k1gMnSc5
Krásný	krásný	k2eAgInSc1d1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Georgína	georgína	k1gFnSc1
Marika	Marika	k1gFnSc1
BurešováFrantišek	BurešováFrantišek	k1gMnSc1
Bureš	Bureš	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Tereza	Tereza	k1gFnSc1
Burešová	Burešová	k1gFnSc1
(	(	kIx(
<g/>
starší	starý	k2eAgFnSc1d2
sestra	sestra	k1gFnSc1
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Eva	Eva	k1gFnSc1
Burešová	Burešová	k1gFnSc1
v	v	k7c6
seriálu	seriál	k1gInSc6
Slunečná	slunečný	k2eAgFnSc1d1
</s>
<s>
Eva	Eva	k1gFnSc1
Salvatore	Salvator	k1gMnSc5
Burešová	Burešová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1993	#num#	k4
Hlučín	Hlučín	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
zpěvačka	zpěvačka	k1gFnSc1
a	a	k8xC
herečka	herečka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
kariéra	kariéra	k1gFnSc1
</s>
<s>
K	k	k7c3
hudbě	hudba	k1gFnSc3
ji	on	k3xPp3gFnSc4
přivedli	přivést	k5eAaPmAgMnP
rodiče	rodič	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
jsou	být	k5eAaImIp3nP
oba	dva	k4xCgMnPc1
hudebníci	hudebník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpěv	zpěv	k1gInSc1
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
operní	operní	k2eAgFnSc1d1
<g/>
,	,	kIx,
začala	začít	k5eAaPmAgFnS
studovat	studovat	k5eAaImF
v	v	k7c6
8	#num#	k4
letech	léto	k1gNnPc6
u	u	k7c2
prof.	prof.	kA
Sylvie	Sylvie	k1gFnSc2
Pivovarčíkové	Pivovarčíková	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začala	začít	k5eAaPmAgFnS
docházet	docházet	k5eAaImF
do	do	k7c2
operního	operní	k2eAgNnSc2d1
studia	studio	k1gNnSc2
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
moravskoslezském	moravskoslezský	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svůj	svůj	k3xOyFgInSc4
divadelní	divadelní	k2eAgInSc4d1
debut	debut	k1gInSc4
si	se	k3xPyFc3
odbyla	odbýt	k5eAaPmAgFnS
ve	v	k7c6
14	#num#	k4
letech	léto	k1gNnPc6
v	v	k7c6
dětské	dětský	k2eAgFnSc6d1
opeře	opera	k1gFnSc6
Kolotoč	kolotoč	k1gInSc1
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
moravskoslezském	moravskoslezský	k2eAgNnSc6d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
tam	tam	k6eAd1
působila	působit	k5eAaImAgFnS
přes	přes	k7c4
rok	rok	k1gInSc4
i	i	k9
v	v	k7c6
dalších	další	k2eAgFnPc6d1
malých	malý	k2eAgFnPc6d1
rolích	role	k1gFnPc6
nebo	nebo	k8xC
ve	v	k7c6
sboru	sbor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebylo	být	k5eNaImAgNnS
překvapením	překvapení	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
pro	pro	k7c4
studium	studium	k1gNnSc4
na	na	k7c6
konzervatoři	konzervatoř	k1gFnSc6
Jaroslava	Jaroslav	k1gMnSc2
Ježka	Ježek	k1gMnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
obor	obor	k1gInSc1
Muzikál	muzikál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
ve	v	k7c6
druhém	druhý	k4xOgInSc6
ročníku	ročník	k1gInSc6
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
trojroli	trojrole	k1gFnSc4
Gity	Gita	k1gFnSc2
<g/>
,	,	kIx,
Džamily	Džamily	k1gFnSc2
a	a	k8xC
služtičky	služtička	k1gFnSc2
v	v	k7c6
muzikálu	muzikál	k1gInSc6
Baron	baron	k1gMnSc1
Prášil	Prášil	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Zahrála	zahrát	k5eAaPmAgFnS
si	se	k3xPyFc3
company	compan	k1gInPc4
muzikálů	muzikál	k1gInPc2
Jesus	Jesus	k1gMnSc1
Christ	Christ	k1gMnSc1
Superstar	superstar	k1gFnSc1
a	a	k8xC
Vražda	vražda	k1gFnSc1
za	za	k7c7
oponou	opona	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
dvou	dva	k4xCgInPc6
letech	let	k1gInPc6
konzervatoř	konzervatoř	k1gFnSc1
opustila	opustit	k5eAaPmAgFnS
a	a	k8xC
přešla	přejít	k5eAaPmAgFnS
na	na	k7c4
studium	studium	k1gNnSc4
soukromé	soukromý	k2eAgFnSc2d1
mezinárodní	mezinárodní	k2eAgFnSc2d1
konzervatoře	konzervatoř	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
studovala	studovat	k5eAaImAgFnS
obory	obora	k1gFnPc4
Muzikál	muzikál	k1gInSc4
<g/>
,	,	kIx,
Jazz	jazz	k1gInSc4
a	a	k8xC
World	World	k1gInSc4
music	musice	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
zkusit	zkusit	k5eAaPmF
štěstí	štěstí	k1gNnSc4
v	v	k7c6
televizní	televizní	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
Česko	Česko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc4
má	mít	k5eAaImIp3nS
talent	talent	k1gInSc1
2011	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
na	na	k7c6
konkurzu	konkurz	k1gInSc6
předvedla	předvést	k5eAaPmAgFnS
píseň	píseň	k1gFnSc1
Listen	listen	k1gInSc4
od	od	k7c2
Beyoncé	Beyoncá	k1gFnSc2
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
další	další	k2eAgInSc4d1
rok	rok	k1gInSc4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
získala	získat	k5eAaPmAgFnS
roli	role	k1gFnSc4
Ofélie	Ofélie	k1gFnSc2
v	v	k7c6
muzikálu	muzikál	k1gInSc6
Hamlet	Hamlet	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Rock	rock	k1gInSc1
Opera	opera	k1gFnSc1
a	a	k8xC
koncem	koncem	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
se	se	k3xPyFc4
pustila	pustit	k5eAaPmAgFnS
do	do	k7c2
dalšího	další	k2eAgInSc2d1
muzikálu	muzikál	k1gInSc2
<g/>
,	,	kIx,
Lucie	Lucie	k1gFnSc2
<g/>
,	,	kIx,
větší	veliký	k2eAgNnSc4d2
než	než	k8xS
malé	malý	k2eAgNnSc4d1
množství	množství	k1gNnSc4
lásky	láska	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ztvárnila	ztvárnit	k5eAaPmAgFnS
opět	opět	k6eAd1
hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
v	v	k7c6
seriálu	seriál	k1gInSc6
Gympl	gympl	k1gInSc1
s	s	k7c7
(	(	kIx(
<g/>
r	r	kA
<g/>
)	)	kIx)
<g/>
učením	učení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
na	na	k7c6
TV	TV	kA
Nova	nova	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
hrála	hrát	k5eAaImAgFnS
dceru	dcera	k1gFnSc4
přísné	přísný	k2eAgFnSc2d1
učitelky	učitelka	k1gFnSc2
„	„	k?
<g/>
Kobry	kobra	k1gFnSc2
<g/>
“	“	k?
Renatu	Renata	k1gFnSc4
Koberovou	Koberový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
hrála	hrát	k5eAaImAgFnS
v	v	k7c6
druhé	druhý	k4xOgFnSc6
a	a	k8xC
třetí	třetí	k4xOgFnSc3
sérii	série	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
rok	rok	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
si	se	k3xPyFc3
zahrála	zahrát	k5eAaPmAgFnS
společně	společně	k6eAd1
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
kolegou	kolega	k1gMnSc7
ze	z	k7c2
seriálu	seriál	k1gInSc2
Gympl	gympl	k1gInSc4
<g/>
,	,	kIx,
Davidem	David	k1gMnSc7
Gránským	Gránský	k1gMnSc7
v	v	k7c6
muzikálu	muzikál	k1gInSc6
Cyrano	Cyrano	k1gMnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
spolu	spolu	k6eAd1
ztvárnili	ztvárnit	k5eAaPmAgMnP
ústřední	ústřední	k2eAgFnSc4d1
dvojici	dvojice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Rok	rok	k1gInSc1
2015	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c4
hlavní	hlavní	k2eAgFnPc4d1
muzikálové	muzikálový	k2eAgFnPc4d1
role	role	k1gFnPc4
také	také	k9
velmi	velmi	k6eAd1
úspěšný	úspěšný	k2eAgMnSc1d1
a	a	k8xC
Eva	Eva	k1gFnSc1
Burešová	Burešová	k1gFnSc1
získala	získat	k5eAaPmAgFnS
roli	role	k1gFnSc4
Sibyly	Sibyla	k1gFnSc2
v	v	k7c6
muzikálu	muzikál	k1gInSc6
Sibyla	Sibyla	k1gFnSc1
–	–	k?
královna	královna	k1gFnSc1
ze	z	k7c2
Sáby	Sába	k1gFnSc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
od	od	k7c2
konce	konec	k1gInSc2
tohoto	tento	k3xDgInSc2
roku	rok	k1gInSc2
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
účinkuje	účinkovat	k5eAaImIp3nS
v	v	k7c6
Draculovi	Dracul	k1gMnSc6
jako	jako	k8xS,k8xC
Adriana	Adriana	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Sandra	Sandra	k1gFnSc1
a	a	k8xC
Lorraine	Lorrain	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
jako	jako	k9
Aneta	Aneta	k1gFnSc1
v	v	k7c6
muzikálu	muzikál	k1gInSc6
Čas	čas	k1gInSc1
růží	růžit	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
začala	začít	k5eAaPmAgFnS
hrát	hrát	k5eAaImF
v	v	k7c6
seriálu	seriál	k1gInSc6
Modrý	modrý	k2eAgInSc4d1
kód	kód	k1gInSc4
roli	role	k1gFnSc4
sestřičky	sestřička	k1gFnSc2
Petry	Petra	k1gFnSc2
Horvátové	Horvátový	k2eAgFnSc2d1
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
tváří	tvář	k1gFnPc2
Fashion	Fashion	k1gInSc1
in	in	k?
the	the	k?
box	box	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc3
narodil	narodit	k5eAaPmAgMnS
syn	syn	k1gMnSc1
Nathaniel	Nathaniel	k1gMnSc1
Adam	Adam	k1gMnSc1
Salvatore	Salvator	k1gMnSc5
Krásný	krásný	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
účinkovala	účinkovat	k5eAaImAgFnS
v	v	k7c6
5	#num#	k4
<g/>
.	.	kIx.
řadě	řada	k1gFnSc6
zábavné	zábavný	k2eAgNnSc4d1
show	show	k1gNnSc4
Tvoje	tvůj	k3xOp2gFnSc1
tvář	tvář	k1gFnSc1
má	mít	k5eAaImIp3nS
známý	známý	k2eAgInSc4d1
hlas	hlas	k1gInSc4
na	na	k7c6
TV	TV	kA
Nova	nova	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
obsadila	obsadit	k5eAaPmAgFnS
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
účastnila	účastnit	k5eAaImAgFnS
Eurovision	Eurovision	k1gInSc1
Song	song	k1gInSc4
CZ	CZ	kA
<g/>
,	,	kIx,
národního	národní	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
pro	pro	k7c4
Eurovision	Eurovision	k1gInSc4
Song	song	k1gInSc1
Contest	Contest	k1gFnSc1
2018	#num#	k4
s	s	k7c7
písní	píseň	k1gFnSc7
Fly	Fly	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
skončila	skončit	k5eAaPmAgFnS
třetí	třetí	k4xOgFnSc4
a	a	k8xC
o	o	k7c4
místo	místo	k1gNnSc4
se	se	k3xPyFc4
dělila	dělit	k5eAaImAgFnS
se	s	k7c7
zpěvákem	zpěvák	k1gMnSc7
Pavlem	Pavel	k1gMnSc7
Calltou	Callta	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
skončila	skončit	k5eAaPmAgFnS
v	v	k7c6
seriálu	seriál	k1gInSc6
Modrý	modrý	k2eAgInSc4d1
kód	kód	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohla	moct	k5eAaImAgFnS
ztvárnit	ztvárnit	k5eAaPmF
hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
–	–	k?
Kristýnu	Kristýna	k1gFnSc4
„	„	k?
<g/>
Týnu	Týn	k1gInSc2
<g/>
“	“	k?
Popelkovou	Popelková	k1gFnSc4
–	–	k?
v	v	k7c6
seriálu	seriál	k1gInSc6
Slunečná	slunečný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
byla	být	k5eAaImAgFnS
obsazena	obsadit	k5eAaPmNgFnS
do	do	k7c2
role	role	k1gFnSc2
Perdity	Perdita	k1gFnSc2
v	v	k7c6
Zimní	zimní	k2eAgFnSc6d1
pohádce	pohádka	k1gFnSc6
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
shakespearovských	shakespearovský	k2eAgFnPc6d1
slavnostech	slavnost	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
zaskočila	zaskočit	k5eAaPmAgFnS
za	za	k7c4
herečku	herečka	k1gFnSc4
Veroniku	Veronika	k1gFnSc4
Arichtevu	Arichtev	k1gInSc2
připravující	připravující	k2eAgFnSc1d1
se	se	k3xPyFc4
na	na	k7c4
mateřské	mateřský	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Vrátila	vrátit	k5eAaPmAgFnS
se	se	k3xPyFc4
do	do	k7c2
zábavné	zábavný	k2eAgFnSc2d1
show	show	k1gFnSc2
Tvoje	tvůj	k3xOp2gFnSc1
tvář	tvář	k1gFnSc4
má	mít	k5eAaImIp3nS
známý	známý	k2eAgInSc1d1
hlas	hlas	k1gInSc1
na	na	k7c6
TV	TV	kA
Nova	nova	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
působí	působit	k5eAaImIp3nS
jako	jako	k9
porotkyně	porotkyně	k1gFnSc1
7	#num#	k4
<g/>
.	.	kIx.
řady	řada	k1gFnPc1
a	a	k8xC
nové	nový	k2eAgInPc1d1
8	#num#	k4
<g/>
.	.	kIx.
řady	řada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Filmografie	filmografie	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Role	role	k1gFnSc1
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc4
má	mít	k5eAaImIp3nS
talent	talent	k1gInSc1
</s>
<s>
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
sama	sám	k3xTgFnSc1
(	(	kIx(
<g/>
soutěžící	soutěžící	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
televizní	televizní	k2eAgInSc1d1
pořad	pořad	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
</s>
<s>
Ceny	cena	k1gFnPc1
paměti	paměť	k1gFnSc2
národa	národ	k1gInSc2
</s>
<s>
krátkometrážní	krátkometrážní	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Modrý	modrý	k2eAgMnSc1d1
tygr	tygr	k1gMnSc1
</s>
<s>
studentka	studentka	k1gFnSc1
</s>
<s>
film	film	k1gInSc1
</s>
<s>
2013	#num#	k4
</s>
<s>
Gympl	gympl	k1gInSc1
s	s	k7c7
(	(	kIx(
<g/>
r	r	kA
<g/>
)	)	kIx)
<g/>
učením	učení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
</s>
<s>
Renata	Renata	k1gFnSc1
Koberová	Koberová	k1gFnSc1
</s>
<s>
televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
Giant	Giant	k1gInSc1
Business	business	k1gInSc1
</s>
<s>
Eva	Eva	k1gFnSc1
</s>
<s>
amatérský	amatérský	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
2015	#num#	k4
</s>
<s>
Trhlina	trhlina	k1gFnSc1
</s>
<s>
Zuzana	Zuzana	k1gFnSc1
</s>
<s>
amatérský	amatérský	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Gangster	gangster	k1gMnSc1
Ka	Ka	k1gMnSc1
</s>
<s>
zpěv	zpěv	k1gInSc1
</s>
<s>
film	film	k1gInSc1
</s>
<s>
2016	#num#	k4
</s>
<s>
V.I.	V.I.	k?
<g/>
P.	P.	kA
vraždy	vražda	k1gFnSc2
</s>
<s>
Tereza	Tereza	k1gFnSc1
Rázlová	Rázlová	k1gFnSc1
</s>
<s>
televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
díl	díl	k1gInSc1
<g/>
:	:	kIx,
Recept	recept	k1gInSc1
na	na	k7c4
smrt	smrt	k1gFnSc4
</s>
<s>
Krycí	krycí	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
Holec	holec	k1gMnSc1
</s>
<s>
Eva	Eva	k1gFnSc1
(	(	kIx(
<g/>
zpěv	zpěv	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
film	film	k1gInSc1
</s>
<s>
2017	#num#	k4
</s>
<s>
Modrý	modrý	k2eAgInSc4d1
kód	kód	k1gInSc4
</s>
<s>
Petra	Petra	k1gFnSc1
Horvátová	Horvátový	k2eAgFnSc1d1
</s>
<s>
televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
</s>
<s>
2018	#num#	k4
</s>
<s>
Hotel	hotel	k1gInSc1
Hvězdář	hvězdář	k1gMnSc1
</s>
<s>
čarodějka	čarodějka	k1gFnSc1
Dobrodějka	dobrodějka	k1gFnSc1
</s>
<s>
televizní	televizní	k2eAgInSc1d1
pořad	pořad	k1gInSc1
</s>
<s>
Máme	mít	k5eAaImIp1nP
rádi	rád	k2eAgMnPc1d1
Česko	Česko	k1gNnSc4
</s>
<s>
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
sama	sám	k3xTgFnSc1
</s>
<s>
televizní	televizní	k2eAgInSc1d1
pořad	pořad	k1gInSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
</s>
<s>
Tvoje	tvůj	k3xOp2gFnSc1
tvář	tvář	k1gFnSc1
má	mít	k5eAaImIp3nS
známý	známý	k2eAgInSc4d1
hlas	hlas	k1gInSc4
</s>
<s>
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
sama	sám	k3xTgFnSc1
(	(	kIx(
<g/>
soutěžící	soutěžící	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
televizní	televizní	k2eAgInSc1d1
pořad	pořad	k1gInSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
</s>
<s>
Show	show	k1gFnSc1
Jana	Jan	k1gMnSc2
Krause	Kraus	k1gMnSc2
</s>
<s>
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
sama	sám	k3xTgFnSc1
</s>
<s>
talk	talk	k6eAd1
show	show	k1gFnSc1
</s>
<s>
Backstage	Backstage	k6eAd1
</s>
<s>
hlas	hlas	k1gInSc1
</s>
<s>
film	film	k1gInSc1
</s>
<s>
Raubíř	raubíř	k1gMnSc1
Ralf	Ralf	k1gMnSc1
a	a	k8xC
Internet	Internet	k1gInSc1
</s>
<s>
Shank	Shank	k1gInSc1
(	(	kIx(
<g/>
hlas	hlas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
2019	#num#	k4
</s>
<s>
Specialisté	specialista	k1gMnPc1
</s>
<s>
Alice	Alice	k1gFnSc1
Vernerová	Vernerová	k1gFnSc1
</s>
<s>
televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
díl	díl	k1gInSc1
<g/>
:	:	kIx,
Párty	párty	k1gFnSc1
</s>
<s>
Krejzovi	Krejzův	k2eAgMnPc1d1
</s>
<s>
slečna	slečna	k1gFnSc1
s	s	k7c7
psíčkem	psíček	k1gMnSc7
</s>
<s>
televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
díl	díl	k1gInSc1
<g/>
:	:	kIx,
Mise	mise	k1gFnSc1
na	na	k7c4
Mars	Mars	k1gInSc4
</s>
<s>
Jak	jak	k6eAd1
si	se	k3xPyFc3
nepodělat	podělat	k5eNaPmF
život	život	k1gInSc4
</s>
<s>
Lotta	Lotta	k1gFnSc1
</s>
<s>
televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
díl	díl	k1gInSc1
<g/>
:	:	kIx,
Až	až	k6eAd1
budou	být	k5eAaImBp3nP
krávy	kráva	k1gFnPc1
lítat	lítat	k5eAaImF
</s>
<s>
2020	#num#	k4
</s>
<s>
Slunečná	slunečný	k2eAgFnSc1d1
</s>
<s>
Týna	Týn	k1gInSc2
Popelková	popelkový	k2eAgNnPc4d1
</s>
<s>
televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
</s>
<s>
7	#num#	k4
pádů	pád	k1gInPc2
Honzy	Honza	k1gMnSc2
Dědka	Dědek	k1gMnSc2
</s>
<s>
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
sama	sám	k3xTgFnSc1
</s>
<s>
talk	talk	k6eAd1
show	show	k1gFnSc1
</s>
<s>
Show	show	k1gFnSc1
Jana	Jan	k1gMnSc2
Krause	Kraus	k1gMnSc2
</s>
<s>
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
sama	sám	k3xTgFnSc1
</s>
<s>
talk	talk	k6eAd1
show	show	k1gFnSc1
</s>
<s>
Stáří	stáří	k1gNnSc1
není	být	k5eNaImIp3nS
pro	pro	k7c4
sraby	srab	k1gInPc4
</s>
<s>
film	film	k1gInSc1
</s>
<s>
Tvoje	tvůj	k3xOp2gFnSc1
tvář	tvář	k1gFnSc1
má	mít	k5eAaImIp3nS
známý	známý	k2eAgInSc4d1
hlas	hlas	k1gInSc4
</s>
<s>
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
sama	sám	k3xTgFnSc1
(	(	kIx(
<g/>
porotkyně	porotkyně	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
televizní	televizní	k2eAgInSc1d1
pořad	pořad	k1gInSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
maska	maska	k1gFnSc1
</s>
<s>
Jednorožec	jednorožec	k1gMnSc1
(	(	kIx(
<g/>
soutěžící	soutěžící	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
televizní	televizní	k2eAgInSc1d1
pořad	pořad	k1gInSc1
</s>
<s>
Až	až	k9
na	na	k7c4
měsíc	měsíc	k1gInSc4
(	(	kIx(
<g/>
Over	Over	k1gInSc1
the	the	k?
moon	moon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ruthie	Ruthie	k1gFnSc1
Ann	Ann	k1gFnSc2
Miles	Miles	k1gInSc1
–	–	k?
máma	máma	k1gFnSc1
(	(	kIx(
<g/>
hlas	hlas	k1gInSc1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Pan	Pan	k1gMnSc1
Jangle	Jangle	k1gFnSc2
a	a	k8xC
vánoční	vánoční	k2eAgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
<g/>
(	(	kIx(
<g/>
Jingle	Jingle	k1gFnSc1
Jangle	Jangle	k1gFnSc2
<g/>
:	:	kIx,
A	a	k9
Christmas	Christmas	k1gInSc1
Journey	Journea	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Joanne	Joannout	k5eAaPmIp3nS,k5eAaImIp3nS
Jangle	Jangle	k1gFnSc1
(	(	kIx(
<g/>
hlas	hlas	k1gInSc1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
film	film	k1gInSc1
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Role	role	k1gFnSc1
</s>
<s>
Divadlo	divadlo	k1gNnSc1
</s>
<s>
2008	#num#	k4
</s>
<s>
Kolotoč	kolotoč	k1gInSc1
</s>
<s>
Chlapeček	chlapeček	k1gMnSc1
v	v	k7c6
eroplánu	eroplán	k1gInSc6
<g/>
,	,	kIx,
Chlapeček	chlapeček	k1gMnSc1
v	v	k7c6
autíčku	autíčko	k1gNnSc6
</s>
<s>
Národní	národní	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
</s>
<s>
2010	#num#	k4
</s>
<s>
Baron	baron	k1gMnSc1
Prášil	Prášil	k1gMnSc1
</s>
<s>
Gita	Gita	k1gFnSc1
<g/>
,	,	kIx,
Džamila	Džamila	k1gFnSc1
<g/>
,	,	kIx,
služtička	služtička	k1gFnSc1
</s>
<s>
Divadlo	divadlo	k1gNnSc1
Hybernia	Hybernium	k1gNnSc2
</s>
<s>
Jesus	Jesus	k1gMnSc1
Christ	Christ	k1gMnSc1
Superstar	superstar	k1gFnSc4
</s>
<s>
Máří	Máří	k?
Magdaléna	Magdaléna	k1gFnSc1
<g/>
,	,	kIx,
company	compana	k1gFnPc1
</s>
<s>
Hudební	hudební	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Karlín	Karlín	k1gInSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
Vražda	vražda	k1gFnSc1
za	za	k7c7
oponou	opona	k1gFnSc7
</s>
<s>
company	compana	k1gFnPc1
</s>
<s>
Hudební	hudební	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Karlín	Karlín	k1gInSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Hamlet	Hamlet	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Rock	rock	k1gInSc1
Opera	opera	k1gFnSc1
</s>
<s>
Ofélie	Ofélie	k1gFnSc1
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
</s>
<s>
Divadlo	divadlo	k1gNnSc1
Broadway	Broadwaa	k1gFnSc2
</s>
<s>
2013	#num#	k4
</s>
<s>
Lucie	Lucie	k1gFnSc1
<g/>
,	,	kIx,
větší	veliký	k2eAgNnSc1d2
než	než	k8xS
malé	malý	k2eAgNnSc1d1
množství	množství	k1gNnSc1
lásky	láska	k1gFnSc2
</s>
<s>
Lucie	Lucie	k1gFnSc1
</s>
<s>
Hudební	hudební	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Karlín	Karlín	k1gInSc1
</s>
<s>
Superstar	superstar	k1gFnSc1
Company	Compana	k1gFnSc2
</s>
<s>
Marie	Marie	k1gFnSc1
<g/>
,	,	kIx,
Viki	Viki	k1gNnSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
konzervatoř	konzervatoř	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
Cyrano	Cyrano	k1gMnSc1
</s>
<s>
Roxana	Roxana	k1gFnSc1
</s>
<s>
Divadlo	divadlo	k1gNnSc1
Radka	Radek	k1gMnSc2
Brzobohatého	Brzobohatý	k1gMnSc2
</s>
<s>
2015	#num#	k4
</s>
<s>
Dracula	Dracula	k1gFnSc1
</s>
<s>
Lorraine	Lorrainout	k5eAaPmIp3nS
<g/>
,	,	kIx,
Adriana	Adriana	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Sandra	Sandra	k1gFnSc1
</s>
<s>
Hudební	hudební	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Karlín	Karlín	k1gInSc1
</s>
<s>
2016	#num#	k4
</s>
<s>
Sibyla	Sibyla	k1gFnSc1
<g/>
,	,	kIx,
královna	královna	k1gFnSc1
ze	z	k7c2
Sáby	Sába	k1gFnSc2
</s>
<s>
Sibyla	Sibyla	k1gFnSc1
</s>
<s>
Divadlo	divadlo	k1gNnSc1
Hybernia	Hybernium	k1gNnSc2
</s>
<s>
Boeing	boeing	k1gInSc1
–	–	k?
Boeing	boeing	k1gInSc1
aneb	aneb	k?
Tři	tři	k4xCgFnPc1
letušky	letuška	k1gFnPc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
</s>
<s>
Jane	Jan	k1gMnSc5
–	–	k?
americká	americký	k2eAgFnSc1d1
letuškaGréta	letuškaGréta	k1gFnSc1
–	–	k?
německá	německý	k2eAgFnSc1d1
letuška	letuška	k1gFnSc1
</s>
<s>
Divadlo	divadlo	k1gNnSc1
Radka	Radek	k1gMnSc2
Brzobohatého	Brzobohatý	k1gMnSc2
</s>
<s>
2017	#num#	k4
</s>
<s>
Čas	čas	k1gInSc1
růží	růžit	k5eAaImIp3nS
</s>
<s>
Aneta	Aneta	k1gFnSc1
</s>
<s>
Hudební	hudební	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Karlín	Karlín	k1gInSc1
</s>
<s>
2019	#num#	k4
</s>
<s>
Sopranistky	sopranistka	k1gFnPc1
</s>
<s>
Kylah	Kylah	k1gMnSc1
</s>
<s>
Činoherní	činoherní	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
2020	#num#	k4
</s>
<s>
Zimní	zimní	k2eAgFnSc1d1
pohádka	pohádka	k1gFnSc1
</s>
<s>
Perdita	Perdita	k1gFnSc1
</s>
<s>
Letní	letní	k2eAgFnPc1d1
shakespearovské	shakespearovský	k2eAgFnPc1d1
slavnosti	slavnost	k1gFnPc1
</s>
<s>
Cyrano	Cyrano	k1gMnSc1
(	(	kIx(
<g/>
obnovená	obnovený	k2eAgFnSc1d1
hra	hra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Roxana	Roxana	k1gFnSc1
</s>
<s>
Divadlo	divadlo	k1gNnSc1
Radka	Radek	k1gMnSc2
Brzobohatého	Brzobohatý	k1gMnSc2
</s>
<s>
Hledám	hledat	k5eAaImIp1nS
ženu	žena	k1gFnSc4
<g/>
,	,	kIx,
nástup	nástup	k1gInSc4
ihned	ihned	k6eAd1
</s>
<s>
Helena	Helena	k1gFnSc1
Fosterová	Fosterová	k1gFnSc1
</s>
<s>
Divadlo	divadlo	k1gNnSc1
Palace	Palace	k1gFnSc2
</s>
<s>
Hudební	hudební	k2eAgInPc1d1
klipy	klip	k1gInPc1
a	a	k8xC
písně	píseň	k1gFnPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Interpret	interpret	k1gMnSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
I	i	k8xC
belong	belong	k1gMnSc1
to	ten	k3xDgNnSc4
you	you	k?
</s>
<s>
Jakub	Jakub	k1gMnSc1
Hubner	Hubner	k1gMnSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
I	i	k9
love	lov	k1gInSc5
you	you	k?
</s>
<s>
Flattus	Flattus	k1gMnSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Pouta	pouto	k1gNnPc1
</s>
<s>
Flattus	Flattus	k1gMnSc1
</s>
<s>
Hoříš	hořet	k5eAaImIp2nS
</s>
<s>
Flattus	Flattus	k1gMnSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
Ak	Ak	k?
nie	nie	k?
si	se	k3xPyFc3
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
budeš	být	k5eAaImBp2nS
</s>
<s>
Mušnula	Mušnout	k5eAaPmAgFnS
</s>
<s>
Představ	představ	k1gInSc1
si	se	k3xPyFc3
</s>
<s>
Chucki	Chucki	k6eAd1
</s>
<s>
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
worried	worried	k1gMnSc1
about	about	k1gMnSc1
you	you	k?
</s>
<s>
2015	#num#	k4
</s>
<s>
V	v	k7c6
noci	noc	k1gFnSc6
se	se	k3xPyFc4
mi	já	k3xPp1nSc3
zdálo	zdát	k5eAaImAgNnS
</s>
<s>
Smrtislav	Smrtislav	k1gMnSc1
</s>
<s>
2016	#num#	k4
</s>
<s>
Nádherná	nádherný	k2eAgFnSc1d1
</s>
<s>
Petr	Petr	k1gMnSc1
Kutheil	Kutheil	k1gMnSc1
</s>
<s>
Kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
je	být	k5eAaImIp3nS
tady	tady	k6eAd1
víc	hodně	k6eAd2
než	než	k8xS
ty	ten	k3xDgFnPc1
</s>
<s>
Maličkej	Maličkej	k?
</s>
<s>
Galen	Galen	k1gInSc1
</s>
<s>
2017	#num#	k4
</s>
<s>
Anděl	Anděla	k1gFnPc2
tvýho	tvýho	k?
světa	svět	k1gInSc2
</s>
<s>
Vojta	Vojta	k1gMnSc1
D	D	kA
</s>
<s>
2018	#num#	k4
</s>
<s>
Brzy	brzy	k6eAd1
se	se	k3xPyFc4
vrať	vrátit	k5eAaPmRp2nS
</s>
<s>
Forever	Forever	k1gInSc1
here	herat	k5eAaPmIp3nS
</s>
<s>
Fly	Fly	k?
(	(	kIx(
<g/>
ESCZ	ESCZ	kA
song	song	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Fly	Fly	k?
(	(	kIx(
<g/>
unplugged	unplugged	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Půjdeš	jít	k5eAaImIp2nS
sám	sám	k3xTgMnSc1
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Klímek	Klímek	k1gMnSc1
</s>
<s>
Nejdeš	jít	k5eNaImIp2nS
stíhat	stíhat	k5eAaImF
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Klímek	Klímek	k1gMnSc1
</s>
<s>
2019	#num#	k4
</s>
<s>
Once	Once	k5eAaImRp2nP
upon	upon	k1gInSc4
a	a	k8xC
time	time	k1gInSc4
</s>
<s>
Derek	Derek	k1gMnSc1
Wolf	Wolf	k1gMnSc1
</s>
<s>
Another	Anothra	k1gFnPc2
day	day	k?
</s>
<s>
Marco	Marco	k6eAd1
</s>
<s>
Já	já	k3xPp1nSc1
tě	ty	k3xPp2nSc4
znám	znát	k5eAaImIp1nS
</s>
<s>
I.V.M	I.V.M	k?
</s>
<s>
Noc	noc	k1gFnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Noid	Noid	k1gMnSc1
Bárta	Bárta	k1gMnSc1
</s>
<s>
Posedlá	posedlý	k2eAgFnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
Julie	Julie	k1gFnSc1
Slováčková	Slováčková	k1gFnSc1
</s>
<s>
2020	#num#	k4
</s>
<s>
Slunečná	slunečný	k2eAgFnSc1d1
znělka	znělka	k1gFnSc1
(	(	kIx(
<g/>
Píseň	píseň	k1gFnSc1
kovbojská	kovbojský	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
FTV	FTV	kA
Prima	prima	k6eAd1
</s>
<s>
Nadechnout	nadechnout	k5eAaPmF
</s>
<s>
Botox	Botox	k1gInSc1
</s>
<s>
Daddy	Daddy	k6eAd1
Lessons	Lessons	k1gInSc1
</s>
<s>
Elis	Elis	k1gFnSc1
Mraz	mrazit	k5eAaImRp2nS
</s>
<s>
Nevěrná	věrný	k2eNgFnSc1d1
</s>
<s>
Štěpán	Štěpán	k1gMnSc1
Soukup	Soukup	k1gMnSc1
</s>
<s>
Ty	ty	k3xPp2nSc1
a	a	k8xC
já	já	k3xPp1nSc1
</s>
<s>
Roman	Roman	k1gMnSc1
Tomeš	Tomeš	k1gMnSc1
</s>
<s>
Jako	jako	k9
by	by	kYmCp3nS
byl	být	k5eAaImAgMnS
náš	náš	k3xOp1gInSc4
poslední	poslední	k2eAgInSc4d1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Callta	Callta	k1gMnSc1
</s>
<s>
Slunečné	slunečný	k2eAgNnSc1d1
léto	léto	k1gNnSc1
</s>
<s>
FTV	FTV	kA
Prima	prima	k6eAd1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2018	#num#	k4
vyhrála	vyhrát	k5eAaPmAgFnS
ženskou	ženský	k2eAgFnSc4d1
kategorii	kategorie	k1gFnSc4
čtenářských	čtenářský	k2eAgFnPc2d1
Hudebních	hudební	k2eAgFnPc2d1
cen	cena	k1gFnPc2
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Burešová	Burešová	k1gFnSc1
<g/>
:	:	kIx,
Vztah	vztah	k1gInSc1
s	s	k7c7
Přemkem	Přemek	k1gMnSc7
jsem	být	k5eAaImIp1nS
tajit	tajit	k5eAaImF
nechtěla	chtít	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vadí	vadit	k5eAaImIp3nS
mi	já	k3xPp1nSc3
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
píší	psát	k5eAaImIp3nP
lži	lež	k1gFnPc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-02-05	2021-02-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Eurovision	Eurovision	k1gInSc1
Song	song	k1gInSc1
Contest	Contest	k1gFnSc1
2018	#num#	k4
-	-	kIx~
Eva	Eva	k1gFnSc1
Burešová	Burešová	k1gFnSc1
<g/>
.	.	kIx.
www.ceskatelevize.cz	www.ceskatelevize.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KOŠATKA	košatka	k1gFnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musical	musical	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-03-09	2010-03-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hudební	hudební	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Karlín	Karlín	k1gInSc1
-	-	kIx~
Umělci	umělec	k1gMnPc1
-	-	kIx~
Eva	Eva	k1gFnSc1
Burešová	Burešová	k1gFnSc1
<g/>
.	.	kIx.
www.hdk.cz	www.hdk.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hudební	hudební	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Karlín	Karlín	k1gInSc1
-	-	kIx~
Repertoár	repertoár	k1gInSc1
-	-	kIx~
Lucie	Lucie	k1gFnSc1
<g/>
,	,	kIx,
větší	veliký	k2eAgNnSc1d2
než	než	k8xS
malé	malý	k2eAgNnSc1d1
množství	množství	k1gNnSc1
lásky	láska	k1gFnSc2
<g/>
.	.	kIx.
www.hdk.cz	www.hdk.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hdk	hdk	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Obsazení	obsazení	k1gNnSc1
<g/>
.	.	kIx.
sibyla	sibyla	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hudební	hudební	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Karlín	Karlín	k1gInSc1
-	-	kIx~
Repertoár	repertoár	k1gInSc1
-	-	kIx~
Čas	čas	k1gInSc1
růží	růž	k1gFnPc2
<g/>
.	.	kIx.
www.hdk.cz	www.hdk.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hdk	hdk	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Eva	Eva	k1gFnSc1
Burešová	Burešová	k1gFnSc1
o	o	k7c6
porodu	porod	k1gInSc6
<g/>
:	:	kIx,
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc3
obr	obr	k1gMnSc1
<g/>
,	,	kIx,
placentu	placenta	k1gFnSc4
vypila	vypít	k5eAaPmAgFnS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
www.blesk.cz	www.blesk.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Finále	finála	k1gFnSc6
českého	český	k2eAgInSc2d1
výběru	výběr	k1gInSc2
Eurovision	Eurovision	k1gInSc1
Song	song	k1gInSc1
Contest	Contest	k1gFnSc1
2018	#num#	k4
<g/>
.	.	kIx.
escportal	escportat	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MRAZEK	MRAZEK	k?
(	(	kIx(
<g/>
MRAZEK@MSYSTEM.CZ	MRAZEK@MSYSTEM.CZ	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lubor	Lubor	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
TZ	TZ	kA
<g/>
)	)	kIx)
Jedeme	jet	k5eAaImIp1nP
na	na	k7c4
plný	plný	k2eAgInSc4d1
plyn	plyn	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
Letní	letní	k2eAgFnPc1d1
shakespearovské	shakespearovský	k2eAgFnPc1d1
slavnosti	slavnost	k1gFnPc1
začnou	začít	k5eAaPmIp3nP
prodávat	prodávat	k5eAaImF
plnou	plný	k2eAgFnSc4d1
kapacitu	kapacita	k1gFnSc4
hledišť	hlediště	k1gNnPc2
<g/>
..	..	k?
www.shakespeare.cz	www.shakespeare.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hudební	hudební	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
čtenáři	čtenář	k1gMnPc1
zvolili	zvolit	k5eAaPmAgMnP
Ortel	ortel	k1gInSc4
<g/>
,	,	kIx,
Řezníka	řezník	k1gMnSc4
a	a	k8xC
Evu	Eva	k1gFnSc4
Burešovou	Burešová	k1gFnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-11-24	2018-11-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Hvězda	hvězda	k1gFnSc1
Talentu	talent	k1gInSc2
Eva	Eva	k1gFnSc1
Burešová	Burešová	k1gFnSc1
se	se	k3xPyFc4
spojila	spojit	k5eAaPmAgFnS
s	s	k7c7
kapelou	kapela	k1gFnSc7
Venefica	Venefic	k1gInSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
ShowBiz	ShowBiz	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-11-14	2012-11-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
KOŠATKA	košatka	k1gFnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eva	Eva	k1gFnSc1
Burešová	Burešová	k1gFnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Baron	baron	k1gMnSc1
Prášil	Prášil	k1gMnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
mě	já	k3xPp1nSc4
obrovská	obrovský	k2eAgFnSc1d1
zkušenost	zkušenost	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musical	musical	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-03-09	2010-03-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Eva	Eva	k1gFnSc1
Burešová	Burešová	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Eva	Eva	k1gFnSc1
Burešová	Burešová	k1gFnSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Eva	Eva	k1gFnSc1
Burešová	Burešová	k1gFnSc1
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Eva	Eva	k1gFnSc1
Burešová	Burešová	k1gFnSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Film	film	k1gInSc1
|	|	kIx~
Hudba	hudba	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Televize	televize	k1gFnSc1
</s>
