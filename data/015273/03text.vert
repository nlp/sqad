<s>
Leguánovití	Leguánovitý	k2eAgMnPc1d1
</s>
<s>
Leguánovití	Leguánovitý	k2eAgMnPc1d1
leguán	leguán	k1gMnSc1
zelený	zelený	k2eAgMnSc1d1
(	(	kIx(
<g/>
Iguana	Iguana	k1gFnSc1
iguana	iguana	k1gFnSc1
<g/>
)	)	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
plazi	plaz	k1gMnPc1
(	(	kIx(
<g/>
Reptilia	Reptilia	k1gFnSc1
<g/>
)	)	kIx)
Podtřída	podtřída	k1gFnSc1
</s>
<s>
Lepidosauria	Lepidosaurium	k1gNnPc4
Řád	řád	k1gInSc1
</s>
<s>
šupinatí	šupinatit	k5eAaImIp3nP
(	(	kIx(
<g/>
Squamata	Squama	k1gNnPc1
<g/>
)	)	kIx)
Podřád	podřád	k1gInSc1
</s>
<s>
ještěři	ještěr	k1gMnPc1
(	(	kIx(
<g/>
Sauria	Saurium	k1gNnSc2
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
leguánovití	leguánovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Iguanidae	Iguanidae	k1gNnSc7
<g/>
)	)	kIx)
<g/>
Oppel	Oppel	k1gMnSc1
<g/>
,	,	kIx,
1811	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Čeleď	čeleď	k1gFnSc1
leguánovití	leguánovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Iguanidae	Iguanidae	k1gNnSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
početná	početný	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
ještěrů	ještěr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Areálem	areál	k1gInSc7
jejich	jejich	k3xOp3gNnSc4
rozšíření	rozšíření	k1gNnSc4
jsou	být	k5eAaImIp3nP
především	především	k9
tropy	trop	k1gInPc1
a	a	k8xC
subtropy	subtropy	k1gInPc1
severní	severní	k2eAgFnSc2d1
a	a	k8xC
jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
na	na	k7c6
Galapágách	Galapágy	k1gFnPc6
a	a	k8xC
ostrovech	ostrov	k1gInPc6
Tonga	Tonga	k1gFnSc1
a	a	k8xC
Fidži	Fidž	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštní	zvláštní	k2eAgInSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
dva	dva	k4xCgInPc1
druhy	druh	k1gInPc1
žijí	žít	k5eAaImIp3nP
i	i	k9
na	na	k7c6
Madagaskaru	Madagaskar	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jsou	být	k5eAaImIp3nP
různorodou	různorodý	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
plazů	plaz	k1gMnPc2
<g/>
,	,	kIx,
nejmenší	malý	k2eAgInPc1d3
druhy	druh	k1gInPc1
dorůstají	dorůstat	k5eAaImIp3nP
asi	asi	k9
7	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
největší	veliký	k2eAgFnSc1d3
pak	pak	k6eAd1
kolem	kolem	k7c2
2	#num#	k4
metrů	metr	k1gInPc2
-	-	kIx~
i	i	k9
když	když	k8xS
podstatnou	podstatný	k2eAgFnSc4d1
část	část	k1gFnSc4
z	z	k7c2
této	tento	k3xDgFnSc2
délky	délka	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nP
nelámavý	lámavý	k2eNgInSc4d1
ocas	ocas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
mají	mít	k5eAaImIp3nP
krční	krční	k2eAgInPc1d1
laloky	lalok	k1gInPc1
nebo	nebo	k8xC
hřbetní	hřbetní	k2eAgInPc1d1
hřebeny	hřeben	k1gInPc1
<g/>
,	,	kIx,
aktivní	aktivní	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
dne	den	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
druhů	druh	k1gInPc2
je	být	k5eAaImIp3nS
vejcorodá	vejcorodý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
druhů	druh	k1gInPc2
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nP
jak	jak	k6eAd1
rostlinnou	rostlinný	k2eAgFnSc4d1
tak	tak	k8xC,k8xS
i	i	k9
živočišnou	živočišný	k2eAgFnSc7d1
potravou	potrava	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
existují	existovat	k5eAaImIp3nP
i	i	k9
druhy	druh	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
pouze	pouze	k6eAd1
vegeteriány	vegeterián	k1gInPc1
-	-	kIx~
leguán	leguán	k1gMnSc1
mořský	mořský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Amblyrhynchus	Amblyrhynchus	k1gMnSc1
cristatus	cristatus	k1gMnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
kupříkladu	kupříkladu	k6eAd1
živí	živit	k5eAaImIp3nS
pouze	pouze	k6eAd1
mořskými	mořský	k2eAgFnPc7d1
řasami	řasa	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přebývají	přebývat	k5eAaImIp3nP
většinou	většinou	k6eAd1
na	na	k7c6
stromech	strom	k1gInPc6
nebo	nebo	k8xC
keřích	keř	k1gInPc6
<g/>
,	,	kIx,
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
vodních	vodní	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
druhy	druh	k1gInPc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
najít	najít	k5eAaPmF
i	i	k9
v	v	k7c6
poušti	poušť	k1gFnSc6
<g/>
,	,	kIx,
příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
leguánek	leguánek	k1gMnSc1
písečný	písečný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Uma	uma	k1gFnSc1
notata	notata	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
podčeledi	podčeleď	k1gFnSc2
Phrynosomatinae	Phrynosomatina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Systematika	systematika	k1gFnSc1
</s>
<s>
Do	do	k7c2
této	tento	k3xDgFnSc2
čeledi	čeleď	k1gFnSc2
patří	patřit	k5eAaImIp3nP
také	také	k9
druhy	druh	k1gInPc1
anolisů	anolis	k1gInPc2
<g/>
,	,	kIx,
bazilišků	bazilišek	k1gMnPc2
a	a	k8xC
ropušníků	ropušník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
bylo	být	k5eAaImAgNnS
objeveno	objevit	k5eAaPmNgNnS
asi	asi	k9
700	#num#	k4
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesná	přesný	k2eAgFnSc1d1
taxonomická	taxonomický	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
skupiny	skupina	k1gFnSc2
Iguania	Iguanium	k1gNnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
šupinatých	šupinatý	k2eAgMnPc2d1
plazů	plaz	k1gMnPc2
však	však	k9
není	být	k5eNaImIp3nS
dosud	dosud	k6eAd1
uspokojivě	uspokojivě	k6eAd1
vyřešena	vyřešit	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Po	po	k7c6
objevu	objev	k1gInSc6
prvních	první	k4xOgFnPc2
koster	kostra	k1gFnPc2
dinosaurů	dinosaurus	k1gMnPc2
počátkem	počátkem	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
předpokládali	předpokládat	k5eAaImAgMnP
vědci	vědec	k1gMnPc1
podobnost	podobnost	k1gFnSc4
právě	právě	k6eAd1
s	s	k7c7
leguány	leguán	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
popsaný	popsaný	k2eAgMnSc1d1
dinosaurus	dinosaurus	k1gMnSc1
Iguanodon	Iguanodon	k1gMnSc1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
leguání	leguánět	k5eAaImIp3nS
zub	zub	k1gInSc4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1825	#num#	k4
rekonstruován	rekonstruovat	k5eAaBmNgInS
jako	jako	k8xS,k8xC
obří	obří	k2eAgMnSc1d1
leguán	leguán	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
však	však	k9
nesmyslné	smyslný	k2eNgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
dinosauři	dinosaurus	k1gMnPc1
reprezentují	reprezentovat	k5eAaImIp3nP
zcela	zcela	k6eAd1
jinou	jiný	k2eAgFnSc4d1
vývojovou	vývojový	k2eAgFnSc4d1
linii	linie	k1gFnSc4
plazů	plaz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
druhů	druh	k1gInPc2
</s>
<s>
rod	rod	k1gInSc1
<g/>
:	:	kIx,
Amblyrhynchus	Amblyrhynchus	k1gInSc1
</s>
<s>
leguán	leguán	k1gMnSc1
mořský	mořský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Amblyrhynchus	Amblyrhynchus	k1gMnSc1
cristatus	cristatus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
rod	rod	k1gInSc1
<g/>
:	:	kIx,
Brachylophus	Brachylophus	k1gInSc1
</s>
<s>
leguán	leguán	k1gMnSc1
fidžijský	fidžijský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Brachylophus	Brachylophus	k1gMnSc1
fasciatus	fasciatus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
leguán	leguán	k1gMnSc1
chocholatý	chocholatý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Brachylophus	Brachylophus	k1gInSc1
vitiensis	vitiensis	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Brachylophus	Brachylophus	k1gInSc1
bulabula	bulabulum	k1gNnSc2
</s>
<s>
Brachylophus	Brachylophus	k1gInSc1
gau	gau	k?
</s>
<s>
rod	rod	k1gInSc1
<g/>
:	:	kIx,
Conolophus	Conolophus	k1gInSc1
</s>
<s>
leguán	leguán	k1gMnSc1
bledý	bledý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Conolophus	Conolophus	k1gMnSc1
pallidus	pallidus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
leguán	leguán	k1gMnSc1
galapážský	galapážský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Conolophus	Conolophus	k1gMnSc1
subcristatus	subcristatus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
leguán	leguán	k1gMnSc1
růžový	růžový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Conolophus	Conolophus	k1gInSc1
marthae	martha	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
rod	rod	k1gInSc1
<g/>
:	:	kIx,
Ctenosaura	Ctenosaura	k1gFnSc1
</s>
<s>
Ctenosaura	Ctenosaura	k1gFnSc1
acanthura	acanthura	k1gFnSc1
</s>
<s>
Ctenosaura	Ctenosaura	k1gFnSc1
alfredschmidti	alfredschmidť	k1gFnSc2
</s>
<s>
Ctenosaura	Ctenosaura	k1gFnSc1
bakeri	baker	k1gFnSc2
</s>
<s>
Ctenosaura	Ctenosaura	k1gFnSc1
clarki	clark	k1gFnSc2
</s>
<s>
Ctenosaura	Ctenosaura	k1gFnSc1
defensor	defensor	k1gMnSc1
</s>
<s>
Ctenosaura	Ctenosaura	k1gFnSc1
flavidorsalis	flavidorsalis	k1gFnSc2
</s>
<s>
Ctenosaura	Ctenosaura	k1gFnSc1
hemilopha	hemilopha	k1gFnSc1
</s>
<s>
Ctenosaura	Ctenosaura	k1gFnSc1
melanosterna	melanosterna	k1gFnSc1
</s>
<s>
Ctenosaura	Ctenosaura	k1gFnSc1
oaxacana	oaxacana	k1gFnSc1
</s>
<s>
Ctenosaura	Ctenosaura	k1gFnSc1
oedirhina	oedirhina	k1gFnSc1
</s>
<s>
Ctenosaura	Ctenosaura	k1gFnSc1
palearis	palearis	k1gFnSc2
</s>
<s>
Ctenosaura	Ctenosaura	k1gFnSc1
pectinata	pectinata	k1gFnSc1
</s>
<s>
leguán	leguán	k1gMnSc1
kyjoocasý	kyjoocasý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Ctenosaura	Ctenosaur	k1gMnSc4
quinquecarinata	quinquecarinat	k1gMnSc4
<g/>
)	)	kIx)
</s>
<s>
leguán	leguán	k1gMnSc1
černý	černý	k1gMnSc1
(	(	kIx(
<g/>
Ctenosaura	Ctenosaura	k1gFnSc1
similis	similis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
rod	rod	k1gInSc1
<g/>
:	:	kIx,
Cyclura	Cyclura	k1gFnSc1
</s>
<s>
leguán	leguán	k1gMnSc1
východobahamský	východobahamský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cyclura	Cyclur	k1gMnSc4
carinata	carinat	k1gMnSc4
<g/>
)	)	kIx)
</s>
<s>
leguán	leguán	k1gMnSc1
jamajský	jamajský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cyclura	Cyclura	k1gFnSc1
collei	colle	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
leguán	leguán	k1gMnSc1
nosorohý	nosorohý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cyclura	Cyclura	k1gFnSc1
cornuta	cornut	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
leguán	leguán	k1gMnSc1
bahamský	bahamský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cyclura	Cyclur	k1gMnSc4
cychlura	cychlur	k1gMnSc4
<g/>
)	)	kIx)
</s>
<s>
leguán	leguán	k1gMnSc1
modravý	modravý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cyclura	Cyclura	k1gFnSc1
lewisi	lewise	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
leguán	leguán	k1gMnSc1
kubánský	kubánský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cyclura	Cyclur	k1gMnSc4
nubila	nubil	k1gMnSc4
<g/>
)	)	kIx)
</s>
<s>
leguán	leguán	k1gMnSc1
tučný	tučný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cyclura	Cyclura	k1gFnSc1
pinguis	pinguis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
leguán	leguán	k1gMnSc1
Ricordův	Ricordův	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cyclura	Cyclura	k1gFnSc1
ricordi	ricordit	k5eAaPmRp2nS
<g/>
)	)	kIx)
</s>
<s>
leguán	leguán	k1gMnSc1
Rileyův	Rileyův	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cyclura	Cyclura	k1gFnSc1
rileyi	riley	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
rod	rod	k1gInSc1
<g/>
:	:	kIx,
Dipsosaurus	Dipsosaurus	k1gInSc1
</s>
<s>
leguán	leguán	k1gMnSc1
pustinný	pustinný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Dipsosaurus	Dipsosaurus	k1gInSc1
dorsalis	dorsalis	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
rod	rod	k1gInSc1
<g/>
:	:	kIx,
Iguana	Iguana	k1gFnSc1
</s>
<s>
leguán	leguán	k1gMnSc1
zelený	zelený	k2eAgMnSc1d1
(	(	kIx(
<g/>
Iguana	Iguan	k1gMnSc4
iguana	iguan	k1gMnSc4
<g/>
)	)	kIx)
</s>
<s>
leguán	leguán	k1gMnSc1
jedlý	jedlý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Iguana	Iguan	k1gMnSc4
delicatissima	delicatissim	k1gMnSc4
<g/>
)	)	kIx)
</s>
<s>
rod	rod	k1gInSc1
<g/>
:	:	kIx,
Polychrus	Polychrus	k1gInSc1
</s>
<s>
Polychrus	Polychrus	k1gMnSc1
gutturosus	gutturosus	k1gMnSc1
</s>
<s>
rod	rod	k1gInSc1
<g/>
:	:	kIx,
Sauromalus	Sauromalus	k1gInSc1
</s>
<s>
leguán	leguán	k1gMnSc1
čuakvala	čuakvat	k5eAaBmAgFnS,k5eAaPmAgFnS,k5eAaImAgFnS
(	(	kIx(
<g/>
Sauromalus	Sauromalus	k1gMnSc1
ater	ater	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Sauromalus	Sauromalus	k1gMnSc1
hispidus	hispidus	k1gMnSc1
</s>
<s>
Sauromalus	Sauromalus	k1gMnSc1
slevini	slevin	k2eAgMnPc1d1
</s>
<s>
Sauromalus	Sauromalus	k1gMnSc1
varius	varius	k1gMnSc1
</s>
<s>
Sauromalus	Sauromalus	k1gInSc1
klauberi	klauber	k1gFnSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Nicolás	Nicolás	k1gInSc1
Mongiardino	Mongiardin	k2eAgNnSc1d1
Koch	Koch	k1gMnSc1
&	&	k?
Jacques	Jacques	k1gMnSc1
A.	A.	kA
Gauthier	Gauthier	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Noise	Noise	k1gFnSc1
and	and	k?
biases	biases	k1gMnSc1
in	in	k?
genomic	genomic	k1gMnSc1
data	datum	k1gNnSc2
may	may	k?
underlie	underlie	k1gFnSc2
radically	radicalla	k1gFnSc2
different	different	k1gMnSc1
hypotheses	hypotheses	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
position	position	k1gInSc1
of	of	k?
Iguania	Iguanium	k1gNnSc2
within	withina	k1gFnPc2
Squamata	Squama	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PLoS	plos	k1gMnSc1
ONE	ONE	kA
13	#num#	k4
<g/>
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
e	e	k0
<g/>
0	#num#	k4
<g/>
202729	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1371/journal.pone.0202729	https://doi.org/10.1371/journal.pone.0202729	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
GAISLER	GAISLER	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoologie	zoologie	k1gFnSc1
obratlovců	obratlovec	k1gMnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
536	#num#	k4
s.	s.	k?
</s>
<s>
NOUAILHETAS	NOUAILHETAS	kA
<g/>
,	,	kIx,
M.	M.	kA
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bite	bit	k1gInSc5
performance	performance	k1gFnSc2
surfaces	surfaces	k1gInSc1
of	of	k?
three	three	k6eAd1
ecologically	ecologicalnout	k5eAaPmAgInP,k5eAaImAgInP,k5eAaBmAgInP
divergent	divergent	k1gInSc4
Iguanidae	Iguanidae	k1gFnSc3
lizards	lizards	k1gInSc1
<g/>
:	:	kIx,
relationships	relationships	k1gInSc1
with	with	k1gMnSc1
lower	lower	k1gMnSc1
jaw	jawa	k1gFnPc2
bones	bones	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biological	Biological	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
the	the	k?
Linnean	Linnean	k1gInSc4
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
blz	blz	k?
<g/>
0	#num#	k4
<g/>
67	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1093/biolinnean/blz067	https://doi.org/10.1093/biolinnean/blz067	k4
</s>
<s>
Simon	Simon	k1gMnSc1
G.	G.	kA
Scarpetta	Scarpetta	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Combined-evidence	Combined-evidence	k1gFnSc1
analyses	analysesa	k1gFnPc2
of	of	k?
ultraconserved	ultraconserved	k1gInSc1
elements	elements	k1gInSc1
and	and	k?
morphological	morphologicat	k5eAaPmAgInS
data	datum	k1gNnPc4
<g/>
:	:	kIx,
an	an	k?
empirical	empiricat	k5eAaPmAgMnS
example	example	k6eAd1
in	in	k?
iguanian	iguanian	k1gInSc1
lizards	lizards	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biology	biolog	k1gMnPc7
Letters	Lettersa	k1gFnPc2
<g/>
,	,	kIx,
16	#num#	k4
<g/>
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
20200356	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1098/rsbl.2020.0356	https://doi.org/10.1098/rsbl.2020.0356	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
leguánovití	leguánovitý	k2eAgMnPc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Iguanidae	Iguanidae	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Živočichové	živočich	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4167139-9	4167139-9	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85064219	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85064219	#num#	k4
</s>
