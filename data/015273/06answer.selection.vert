<s>
Jsou	být	k5eAaImIp3nP
různorodou	různorodý	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
plazů	plaz	k1gMnPc2
<g/>
,	,	kIx,
nejmenší	malý	k2eAgInPc1d3
druhy	druh	k1gInPc1
dorůstají	dorůstat	k5eAaImIp3nP
asi	asi	k9
7	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
největší	veliký	k2eAgFnSc1d3
pak	pak	k6eAd1
kolem	kolem	k7c2
2	#num#	k4
metrů	metr	k1gInPc2
-	-	kIx~
i	i	k9
když	když	k8xS
podstatnou	podstatný	k2eAgFnSc4d1
část	část	k1gFnSc4
z	z	k7c2
této	tento	k3xDgFnSc2
délky	délka	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nP
nelámavý	lámavý	k2eNgInSc4d1
ocas	ocas	k1gInSc4
<g/>
.	.	kIx.
</s>