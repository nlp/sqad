<s>
Mississippi	Mississippi	k1gFnSc1	Mississippi
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Mississippi	Mississippi	k1gFnSc7	Mississippi
River	Rivero	k1gNnPc2	Rivero
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
místních	místní	k2eAgMnPc2d1	místní
indiánů	indián	k1gMnPc2	indián
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
řeka	řeka	k1gFnSc1	řeka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
řeka	řeka	k1gFnSc1	řeka
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
řek	řeka	k1gFnPc2	řeka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgMnPc7d1	americký
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
3770	[number]	k4	3770
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
svého	svůj	k3xOyFgInSc2	svůj
přítoku	přítok	k1gInSc2	přítok
Missouri	Missouri	k1gNnSc2	Missouri
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
6420	[number]	k4	6420
km	km	kA	km
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
od	od	k7c2	od
Skalnatých	skalnatý	k2eAgFnPc2d1	skalnatá
hor	hora	k1gFnPc2	hora
k	k	k7c3	k
Appalačskému	Appalačský	k2eAgNnSc3d1	Appalačské
pohoří	pohoří	k1gNnSc3	pohoří
a	a	k8xC	a
od	od	k7c2	od
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
k	k	k7c3	k
Mexickému	mexický	k2eAgInSc3d1	mexický
zálivu	záliv	k1gInSc3	záliv
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
3	[number]	k4	3
268	[number]	k4	268
000	[number]	k4	000
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
40	[number]	k4	40
%	%	kIx~	%
rozlohy	rozloha	k1gFnPc1	rozloha
pevninských	pevninský	k2eAgFnPc2d1	pevninská
USA	USA	kA	USA
bez	bez	k7c2	bez
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
Mississippi	Mississippi	k1gFnSc2	Mississippi
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
přítoků	přítok	k1gInPc2	přítok
je	být	k5eAaImIp3nS	být
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
nejrozsáhlejší	rozsáhlý	k2eAgInSc1d3	nejrozsáhlejší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
Amazonce	Amazonka	k1gFnSc6	Amazonka
<g/>
,	,	kIx,	,
Kongu	Kongo	k1gNnSc6	Kongo
a	a	k8xC	a
Nilu	Nil	k1gInSc6	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
USA	USA	kA	USA
západně	západně	k6eAd1	západně
od	od	k7c2	od
Hořejšího	Hořejšího	k2eAgNnSc2d1	Hořejšího
jezera	jezero	k1gNnSc2	jezero
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Nicollet	Nicollet	k1gInSc1	Nicollet
Creek	Creek	k1gInSc1	Creek
a	a	k8xC	a
pod	pod	k7c7	pod
jezerem	jezero	k1gNnSc7	jezero
Itasca	Itascum	k1gNnSc2	Itascum
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
pouhých	pouhý	k2eAgInPc2d1	pouhý
446	[number]	k4	446
m	m	kA	m
přijímá	přijímat	k5eAaImIp3nS	přijímat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
deseti	deset	k4xCc2	deset
státy	stát	k1gInPc4	stát
USA	USA	kA	USA
Minnesota	Minnesota	k1gFnSc1	Minnesota
<g/>
,	,	kIx,	,
Wisconsin	Wisconsin	k1gInSc1	Wisconsin
<g/>
,	,	kIx,	,
Iowa	Iowum	k1gNnPc1	Iowum
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gFnPc1	Illinois
<g/>
,	,	kIx,	,
Missouri	Missouri	k1gFnPc1	Missouri
<g/>
,	,	kIx,	,
Kentucky	Kentucka	k1gFnPc1	Kentucka
<g/>
,	,	kIx,	,
Arkansas	Arkansas	k1gInSc1	Arkansas
<g/>
,	,	kIx,	,
Tennessee	Tennessee	k1gFnSc1	Tennessee
<g/>
,	,	kIx,	,
Mississippi	Mississippi	k1gFnSc1	Mississippi
a	a	k8xC	a
Louisiana	Louisiana	k1gFnSc1	Louisiana
<g/>
.	.	kIx.	.
</s>
<s>
Údolí	údolí	k1gNnSc1	údolí
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
hlavního	hlavní	k2eAgInSc2d1	hlavní
odtoku	odtok	k1gInSc2	odtok
ledovcových	ledovcový	k2eAgFnPc2d1	ledovcová
vod	voda	k1gFnPc2	voda
na	na	k7c6	na
konci	konec	k1gInSc6	konec
čtvrtohorního	čtvrtohorní	k2eAgNnSc2d1	čtvrtohorní
zalednění	zalednění	k1gNnSc2	zalednění
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
morfologie	morfologie	k1gFnSc2	morfologie
doliny	dolina	k1gFnSc2	dolina
<g/>
,	,	kIx,	,
podmínek	podmínka	k1gFnPc2	podmínka
průtoku	průtok	k1gInSc2	průtok
a	a	k8xC	a
vodního	vodní	k2eAgInSc2d1	vodní
režimu	režim	k1gInSc2	režim
se	se	k3xPyFc4	se
tok	tok	k1gInSc1	tok
řeky	řeka	k1gFnSc2	řeka
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byla	být	k5eAaImAgFnS	být
vybrána	vybrán	k2eAgNnPc1d1	vybráno
ústí	ústí	k1gNnPc4	ústí
velkých	velký	k2eAgInPc2d1	velký
přítoků	přítok	k1gInPc2	přítok
Missouri	Missouri	k1gNnSc1	Missouri
a	a	k8xC	a
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
protéká	protékat	k5eAaImIp3nS	protékat
nejprve	nejprve	k6eAd1	nejprve
přes	přes	k7c4	přes
nevelká	velký	k2eNgNnPc4d1	nevelké
jezera	jezero	k1gNnPc4	jezero
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
kamenité	kamenitý	k2eAgFnSc2d1	kamenitá
peřeje	peřej	k1gFnSc2	peřej
a	a	k8xC	a
říční	říční	k2eAgInPc4d1	říční
prahy	práh	k1gInPc4	práh
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
vodopád	vodopád	k1gInSc1	vodopád
St.	st.	kA	st.
Anthony	Anthon	k1gInPc1	Anthon
u	u	k7c2	u
města	město	k1gNnSc2	město
Minneapolis	Minneapolis	k1gFnSc2	Minneapolis
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
u	u	k7c2	u
měst	město	k1gNnPc2	město
Davenport	Davenport	k1gInSc4	Davenport
a	a	k8xC	a
Keokuk	Keokuk	k1gInSc4	Keokuk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
města	město	k1gNnSc2	město
Minneapolis	Minneapolis	k1gFnSc2	Minneapolis
je	být	k5eAaImIp3nS	být
koryto	koryto	k1gNnSc1	koryto
řeky	řeka	k1gFnSc2	řeka
upraveno	upravit	k5eAaPmNgNnS	upravit
pomocí	pomocí	k7c2	pomocí
zdymadel	zdymadlo	k1gNnPc2	zdymadlo
a	a	k8xC	a
až	až	k9	až
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
Missouri	Missouri	k1gFnSc2	Missouri
je	být	k5eAaImIp3nS	být
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
hrázemi	hráz	k1gFnPc7	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
teče	téct	k5eAaImIp3nS	téct
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
korytě	koryto	k1gNnSc6	koryto
<g/>
.	.	kIx.	.
</s>
<s>
Dolina	dolina	k1gFnSc1	dolina
je	být	k5eAaImIp3nS	být
široká	široký	k2eAgFnSc1d1	široká
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
km	km	kA	km
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
prudkými	prudký	k2eAgInPc7d1	prudký
břehy	břeh	k1gInPc7	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
ústím	ústí	k1gNnSc7	ústí
Missouri	Missouri	k1gFnSc2	Missouri
teče	téct	k5eAaImIp3nS	téct
její	její	k3xOp3gFnSc1	její
kalná	kalný	k2eAgFnSc1d1	kalná
a	a	k8xC	a
blátivá	blátivý	k2eAgFnSc1d1	blátivá
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
150	[number]	k4	150
až	až	k9	až
180	[number]	k4	180
km	km	kA	km
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
s	s	k7c7	s
víceméně	víceméně	k9	víceméně
průzračnou	průzračný	k2eAgFnSc7d1	průzračná
vodou	voda	k1gFnSc7	voda
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
protéká	protékat	k5eAaImIp3nS	protékat
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
rovinou	rovina	k1gFnSc7	rovina
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
naplavenými	naplavený	k2eAgFnPc7d1	naplavená
usazeninami	usazenina	k1gFnPc7	usazenina
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
říčního	říční	k2eAgNnSc2d1	říční
údolí	údolí	k1gNnSc2	údolí
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
z	z	k7c2	z
25	[number]	k4	25
na	na	k7c4	na
70	[number]	k4	70
až	až	k9	až
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Koryto	koryto	k1gNnSc1	koryto
je	být	k5eAaImIp3nS	být
členité	členitý	k2eAgNnSc1d1	členité
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
četná	četný	k2eAgNnPc4d1	četné
ramena	rameno	k1gNnPc4	rameno
včetně	včetně	k7c2	včetně
slepých	slepý	k2eAgInPc2d1	slepý
<g/>
.	.	kIx.	.
</s>
<s>
Nízký	nízký	k2eAgInSc1d1	nízký
sklon	sklon	k1gInSc1	sklon
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
délka	délka	k1gFnSc1	délka
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
časté	častý	k2eAgNnSc1d1	časté
vytváření	vytváření	k1gNnSc4	vytváření
meandrů	meandr	k1gInPc2	meandr
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
labyrint	labyrint	k1gInSc4	labyrint
průtoků	průtok	k1gInPc2	průtok
<g/>
,	,	kIx,	,
říčních	říční	k2eAgNnPc2d1	říční
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
nivních	nivní	k2eAgInPc2d1	nivní
mokřadů	mokřad	k1gInPc2	mokřad
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
zaplavovány	zaplavovat	k5eAaImNgInP	zaplavovat
při	při	k7c6	při
povodních	povodeň	k1gFnPc6	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Koryto	koryto	k1gNnSc1	koryto
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
téměř	téměř	k6eAd1	téměř
všude	všude	k6eAd1	všude
obehnáno	obehnat	k5eAaPmNgNnS	obehnat
přirozenými	přirozený	k2eAgInPc7d1	přirozený
břehovými	břehový	k2eAgInPc7d1	břehový
valy	val	k1gInPc7	val
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
opevněny	opevnit	k5eAaPmNgInP	opevnit
systémem	systém	k1gInSc7	systém
protipovodňových	protipovodňový	k2eAgFnPc2d1	protipovodňová
hrází	hráz	k1gFnPc2	hráz
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zabránit	zabránit	k5eAaPmF	zabránit
zavodnění	zavodnění	k1gNnSc4	zavodnění
okolní	okolní	k2eAgFnSc2d1	okolní
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
těchto	tento	k3xDgFnPc2	tento
hrází	hráz	k1gFnPc2	hráz
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
4000	[number]	k4	4000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
valy	val	k1gInPc7	val
teče	téct	k5eAaImIp3nS	téct
řeka	řeka	k1gFnSc1	řeka
místy	místy	k6eAd1	místy
nad	nad	k7c7	nad
úrovní	úroveň	k1gFnSc7	úroveň
okolní	okolní	k2eAgFnSc2d1	okolní
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
do	do	k7c2	do
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
deltu	delta	k1gFnSc4	delta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
pod	pod	k7c7	pod
městem	město	k1gNnSc7	město
Baton	baton	k1gInSc1	baton
Rouge	rouge	k1gFnSc3	rouge
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
lopatovitý	lopatovitý	k2eAgInSc1d1	lopatovitý
tvar	tvar	k1gInSc1	tvar
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
rozloha	rozloha	k1gFnSc1	rozloha
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
36	[number]	k4	36
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
o	o	k7c4	o
85	[number]	k4	85
až	až	k9	až
100	[number]	k4	100
m	m	kA	m
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
díky	dík	k1gInPc1	dík
přibližně	přibližně	k6eAd1	přibližně
400	[number]	k4	400
Mt	Mt	k1gFnPc2	Mt
nánosů	nános	k1gInPc2	nános
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
řeka	řeka	k1gFnSc1	řeka
přináší	přinášet	k5eAaImIp3nS	přinášet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konečné	konečný	k2eAgFnSc6d1	konečná
fázi	fáze	k1gFnSc6	fáze
se	se	k3xPyFc4	se
delta	delta	k1gFnSc1	delta
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
6	[number]	k4	6
základních	základní	k2eAgNnPc2d1	základní
poměrně	poměrně	k6eAd1	poměrně
krátkých	krátký	k2eAgNnPc2d1	krátké
ramen	rameno	k1gNnPc2	rameno
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
20	[number]	k4	20
až	až	k9	až
40	[number]	k4	40
km	km	kA	km
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
jihozápadní	jihozápadní	k2eAgInSc4d1	jihozápadní
Southwest-Pass	Southwest-Pass	k1gInSc4	Southwest-Pass
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
protéká	protékat	k5eAaImIp3nS	protékat
asi	asi	k9	asi
30	[number]	k4	30
<g/>
%	%	kIx~	%
odtoku	odtok	k1gInSc2	odtok
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
povodních	povodeň	k1gFnPc6	povodeň
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
se	se	k3xPyFc4	se
část	část	k1gFnSc1	část
vod	voda	k1gFnPc2	voda
na	na	k7c6	na
východě	východ	k1gInSc6	východ
přelévá	přelévat	k5eAaImIp3nS	přelévat
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
Pontchartrain	Pontchartraina	k1gFnPc2	Pontchartraina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
města	město	k1gNnSc2	město
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
a	a	k8xC	a
využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
právě	právě	k9	právě
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
zachycení	zachycení	k1gNnSc4	zachycení
a	a	k8xC	a
ochránění	ochránění	k1gNnSc4	ochránění
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
část	část	k1gFnSc1	část
vod	voda	k1gFnPc2	voda
přelévá	přelévat	k5eAaImIp3nS	přelévat
do	do	k7c2	do
koryta	koryto	k1gNnSc2	koryto
řeky	řeka	k1gFnSc2	řeka
Atchafalaya	Atchafalaya	k1gFnSc1	Atchafalaya
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
teče	téct	k5eAaImIp3nS	téct
paralelně	paralelně	k6eAd1	paralelně
s	s	k7c7	s
Mississippi	Mississippi	k1gFnSc7	Mississippi
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
15	[number]	k4	15
až	až	k9	až
40	[number]	k4	40
km	km	kA	km
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
také	také	k9	také
do	do	k7c2	do
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Přítoky	přítok	k1gInPc1	přítok
zleva	zleva	k6eAd1	zleva
<g/>
:	:	kIx,	:
Přítoky	přítok	k1gInPc4	přítok
zprava	zprava	k6eAd1	zprava
<g/>
:	:	kIx,	:
Zdrojem	zdroj	k1gInSc7	zdroj
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
sněhové	sněhový	k2eAgFnPc1d1	sněhová
i	i	k8xC	i
dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Pravé	pravý	k2eAgInPc1d1	pravý
přítoky	přítok	k1gInPc1	přítok
přivádějí	přivádět	k5eAaImIp3nP	přivádět
převážně	převážně	k6eAd1	převážně
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
roztátého	roztátý	k2eAgInSc2d1	roztátý
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
ledu	led	k1gInSc2	led
ve	v	k7c6	v
Skalnatých	skalnatý	k2eAgFnPc6d1	skalnatá
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
levé	levý	k2eAgInPc4d1	levý
přítoky	přítok	k1gInPc4	přítok
přivádějí	přivádět	k5eAaImIp3nP	přivádět
vody	voda	k1gFnPc1	voda
z	z	k7c2	z
dešťů	dešť	k1gInPc2	dešť
a	a	k8xC	a
průtrží	průtrž	k1gFnPc2	průtrž
mračen	mračno	k1gNnPc2	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
režim	režim	k1gInSc4	režim
řeky	řeka	k1gFnSc2	řeka
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
jarní	jarní	k2eAgInPc4d1	jarní
a	a	k8xC	a
letní	letní	k2eAgInPc4d1	letní
vzestupy	vzestup	k1gInPc4	vzestup
hladiny	hladina	k1gFnSc2	hladina
a	a	k8xC	a
prudké	prudký	k2eAgFnSc2d1	prudká
dešťové	dešťový	k2eAgFnSc2d1	dešťová
povodně	povodeň	k1gFnSc2	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvyšším	vysoký	k2eAgFnPc3d3	nejvyšší
povodním	povodeň	k1gFnPc3	povodeň
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
potkají	potkat	k5eAaPmIp3nP	potkat
období	období	k1gNnSc3	období
tání	tání	k1gNnSc1	tání
sněhu	sníh	k1gInSc2	sníh
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
horní	horní	k2eAgFnSc2d1	horní
Mississippi	Mississippi	k1gFnSc2	Mississippi
a	a	k8xC	a
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Missouri	Missouri	k1gFnSc2	Missouri
s	s	k7c7	s
obdobím	období	k1gNnSc7	období
vydatných	vydatný	k2eAgInPc2d1	vydatný
dešťů	dešť	k1gInPc2	dešť
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Ohia	Ohio	k1gNnSc2	Ohio
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
značně	značně	k6eAd1	značně
větší	veliký	k2eAgInSc4d2	veliký
průtok	průtok	k1gInSc4	průtok
než	než	k8xS	než
Mississippi	Mississippi	k1gFnSc4	Mississippi
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
povodně	povodeň	k1gFnPc1	povodeň
nastaly	nastat	k5eAaPmAgFnP	nastat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
a	a	k8xC	a
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
hydrotechnické	hydrotechnický	k2eAgFnPc1d1	hydrotechnická
stavby	stavba	k1gFnPc1	stavba
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
nemohou	moct	k5eNaImIp3nP	moct
plně	plně	k6eAd1	plně
ochránit	ochránit	k5eAaPmF	ochránit
sídla	sídlo	k1gNnPc4	sídlo
a	a	k8xC	a
pole	pole	k1gNnPc4	pole
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
19	[number]	k4	19
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
při	při	k7c6	při
katastrofálních	katastrofální	k2eAgFnPc6d1	katastrofální
povodních	povodeň	k1gFnPc6	povodeň
narůstá	narůstat	k5eAaImIp3nS	narůstat
na	na	k7c4	na
50	[number]	k4	50
000	[number]	k4	000
až	až	k9	až
80	[number]	k4	80
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
naopak	naopak	k6eAd1	naopak
klesá	klesat	k5eAaImIp3nS	klesat
na	na	k7c4	na
3000	[number]	k4	3000
až	až	k9	až
5000	[number]	k4	5000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
rozsah	rozsah	k1gInSc1	rozsah
kolísání	kolísání	k1gNnSc2	kolísání
úrovně	úroveň	k1gFnSc2	úroveň
hladiny	hladina	k1gFnSc2	hladina
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
7,2	[number]	k4	7,2
m	m	kA	m
(	(	kIx(	(
<g/>
u	u	k7c2	u
Saint	Sainta	k1gFnPc2	Sainta
Paul	Paula	k1gFnPc2	Paula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
14,3	[number]	k4	14,3
m	m	kA	m
(	(	kIx(	(
<g/>
u	u	k7c2	u
St.	st.	kA	st.
Louis	louis	k1gInSc1	louis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
18,3	[number]	k4	18,3
m	m	kA	m
(	(	kIx(	(
<g/>
u	u	k7c2	u
Cairo	Cairo	k1gNnSc4	Cairo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
5	[number]	k4	5
až	až	k9	až
6	[number]	k4	6
m	m	kA	m
(	(	kIx(	(
<g/>
u	u	k7c2	u
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
řeka	řeka	k1gFnSc1	řeka
zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
na	na	k7c4	na
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
pohodlnou	pohodlný	k2eAgFnSc7d1	pohodlná
vodní	vodní	k2eAgFnSc7d1	vodní
cestou	cesta	k1gFnSc7	cesta
od	od	k7c2	od
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
do	do	k7c2	do
středu	střed	k1gInSc2	střed
severoamerického	severoamerický	k2eAgInSc2d1	severoamerický
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
dopravní	dopravní	k2eAgFnSc7d1	dopravní
tepnou	tepna	k1gFnSc7	tepna
USA	USA	kA	USA
a	a	k8xC	a
spojuje	spojovat	k5eAaImIp3nS	spojovat
rozvinuté	rozvinutý	k2eAgFnSc2d1	rozvinutá
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
a	a	k8xC	a
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
oblasti	oblast	k1gFnSc2	oblast
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
sice	sice	k8xC	sice
prohrávala	prohrávat	k5eAaImAgFnS	prohrávat
v	v	k7c6	v
konkurenci	konkurence	k1gFnSc6	konkurence
železniční	železniční	k2eAgFnSc6d1	železniční
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vzmáhat	vzmáhat	k5eAaImF	vzmáhat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
rozvojem	rozvoj	k1gInSc7	rozvoj
oblasti	oblast	k1gFnSc2	oblast
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
vzestup	vzestup	k1gInSc4	vzestup
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ještě	ještě	k9	ještě
výrazněji	výrazně	k6eAd2	výrazně
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
povodím	povodí	k1gNnSc7	povodí
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
s	s	k7c7	s
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
vodní	vodní	k2eAgFnSc7d1	vodní
cestou	cesta	k1gFnSc7	cesta
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
s	s	k7c7	s
Atlantským	atlantský	k2eAgInSc7d1	atlantský
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Illinoiská	Illinoiský	k2eAgFnSc1d1	Illinoiská
vodní	vodní	k2eAgFnSc1d1	vodní
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
Michiganském	michiganský	k2eAgNnSc6d1	Michiganské
jezeře	jezero	k1gNnSc6	jezero
u	u	k7c2	u
města	město	k1gNnSc2	město
Chicago	Chicago	k1gNnSc1	Chicago
vede	vést	k5eAaImIp3nS	vést
systémem	systém	k1gInSc7	systém
kanálů	kanál	k1gInPc2	kanál
a	a	k8xC	a
splavněných	splavněný	k2eAgFnPc2d1	splavněná
řek	řeka	k1gFnPc2	řeka
k	k	k7c3	k
levému	levý	k2eAgInSc3d1	levý
přítoku	přítok	k1gInSc3	přítok
Mississippi	Mississippi	k1gFnSc2	Mississippi
řece	řeka	k1gFnSc3	řeka
Illinois	Illinois	k1gFnSc3	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
leží	ležet	k5eAaImIp3nS	ležet
město	město	k1gNnSc1	město
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
námořních	námořní	k2eAgInPc2d1	námořní
a	a	k8xC	a
říčních	říční	k2eAgInPc2d1	říční
přístavů	přístav	k1gInPc2	přístav
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
něj	on	k3xPp3gInSc2	on
řeku	řeka	k1gFnSc4	řeka
křižuje	křižovat	k5eAaImIp3nS	křižovat
Pobřežní	pobřežní	k2eAgInSc1d1	pobřežní
kanál	kanál	k1gInSc1	kanál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
spojení	spojení	k1gNnSc2	spojení
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
oblastí	oblast	k1gFnPc2	oblast
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Říční	říční	k2eAgFnSc1d1	říční
doprava	doprava	k1gFnSc1	doprava
probíhá	probíhat	k5eAaImIp3nS	probíhat
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
do	do	k7c2	do
Saint	Sainta	k1gFnPc2	Sainta
Paul	Paula	k1gFnPc2	Paula
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
3130	[number]	k4	3130
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Námořní	námořní	k2eAgFnPc1d1	námořní
lodě	loď	k1gFnPc1	loď
plují	plout	k5eAaImIp3nP	plout
do	do	k7c2	do
Baton	baton	k1gInSc4	baton
Rouge	rouge	k1gFnSc4	rouge
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Mississippi	Mississippi	k1gFnSc2	Mississippi
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
25	[number]	k4	25
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
přepravované	přepravovaný	k2eAgInPc1d1	přepravovaný
náklady	náklad	k1gInPc1	náklad
jsou	být	k5eAaImIp3nP	být
ropné	ropný	k2eAgInPc1d1	ropný
produkty	produkt	k1gInPc1	produkt
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgInPc1d1	stavební
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
kamenné	kamenný	k2eAgNnSc1d1	kamenné
uhlí	uhlí	k1gNnSc1	uhlí
a	a	k8xC	a
chemické	chemický	k2eAgFnPc1d1	chemická
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Překážkami	překážka	k1gFnPc7	překážka
plavby	plavba	k1gFnPc4	plavba
jsou	být	k5eAaImIp3nP	být
skalnaté	skalnatý	k2eAgInPc1d1	skalnatý
prahy	práh	k1gInPc1	práh
<g/>
,	,	kIx,	,
pohybující	pohybující	k2eAgInPc1d1	pohybující
se	se	k3xPyFc4	se
písky	písek	k1gInPc7	písek
a	a	k8xC	a
kolísavý	kolísavý	k2eAgInSc4d1	kolísavý
vodní	vodní	k2eAgInSc4d1	vodní
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
pravostranných	pravostranný	k2eAgInPc2d1	pravostranný
přítoků	přítok	k1gInPc2	přítok
byly	být	k5eAaImAgFnP	být
postaveny	postaven	k2eAgFnPc1d1	postavena
mnohé	mnohý	k2eAgFnPc1d1	mnohá
vodní	vodní	k2eAgFnPc1d1	vodní
elektrárny	elektrárna	k1gFnPc1	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
umístěná	umístěný	k2eAgFnSc1d1	umístěná
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
města	město	k1gNnSc2	město
Keokuk	Keokuka	k1gFnPc2	Keokuka
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnPc1d3	veliký
města	město	k1gNnPc1	město
a	a	k8xC	a
přístavy	přístav	k1gInPc1	přístav
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
jsou	být	k5eAaImIp3nP	být
Minneapolis	Minneapolis	k1gFnPc1	Minneapolis
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
Davenport	Davenport	k1gInSc1	Davenport
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Louis	louis	k1gInSc1	louis
<g/>
,	,	kIx,	,
Memphis	Memphis	k1gInSc1	Memphis
<g/>
,	,	kIx,	,
Baton	baton	k1gInSc1	baton
Rouge	rouge	k1gFnSc1	rouge
a	a	k8xC	a
New	New	k1gFnSc1	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
.	.	kIx.	.
</s>
