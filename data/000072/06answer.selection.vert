<s>
Čeština	čeština	k1gFnSc1	čeština
používá	používat	k5eAaImIp3nS	používat
42	[number]	k4	42
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
26	[number]	k4	26
písmen	písmeno	k1gNnPc2	písmeno
základní	základní	k2eAgFnSc2d1	základní
latinky	latinka	k1gFnSc2	latinka
doplněných	doplněný	k2eAgFnPc2d1	doplněná
písmeny	písmeno	k1gNnPc7	písmeno
s	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
různými	různý	k2eAgMnPc7d1	různý
diakritickými	diakritický	k2eAgNnPc7d1	diakritické
znaménky	znaménko	k1gNnPc7	znaménko
<g/>
,	,	kIx,	,
háčkem	háček	k1gMnSc7	háček
(	(	kIx(	(
<g/>
ˇ	ˇ	k?	ˇ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čárkou	čárka	k1gFnSc7	čárka
(	(	kIx(	(
<g/>
́	́	k?	́
<g/>
)	)	kIx)	)
a	a	k8xC	a
kroužkem	kroužek	k1gInSc7	kroužek
(	(	kIx(	(
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
