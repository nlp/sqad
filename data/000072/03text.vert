<s>
Česká	český	k2eAgFnSc1d1	Česká
abeceda	abeceda	k1gFnSc1	abeceda
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
všech	všecek	k3xTgNnPc2	všecek
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
používána	používán	k2eAgMnSc4d1	používán
v	v	k7c6	v
grafické	grafický	k2eAgFnSc6d1	grafická
soustavě	soustava	k1gFnSc6	soustava
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
používá	používat	k5eAaImIp3nS	používat
42	[number]	k4	42
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
26	[number]	k4	26
písmen	písmeno	k1gNnPc2	písmeno
základní	základní	k2eAgFnSc2d1	základní
latinky	latinka	k1gFnSc2	latinka
doplněných	doplněný	k2eAgFnPc2d1	doplněná
písmeny	písmeno	k1gNnPc7	písmeno
s	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
různými	různý	k2eAgMnPc7d1	různý
diakritickými	diakritický	k2eAgNnPc7d1	diakritické
znaménky	znaménko	k1gNnPc7	znaménko
<g/>
,	,	kIx,	,
háčkem	háček	k1gMnSc7	háček
(	(	kIx(	(
<g/>
ˇ	ˇ	k?	ˇ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čárkou	čárka	k1gFnSc7	čárka
(	(	kIx(	(
<g/>
́	́	k?	́
<g/>
)	)	kIx)	)
a	a	k8xC	a
kroužkem	kroužek	k1gInSc7	kroužek
(	(	kIx(	(
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgNnSc1d1	speciální
postavení	postavení	k1gNnSc1	postavení
má	mít	k5eAaImIp3nS	mít
ch	ch	k0	ch
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
počítáno	počítat	k5eAaImNgNnS	počítat
mezi	mezi	k7c4	mezi
42	[number]	k4	42
písmen	písmeno	k1gNnPc2	písmeno
české	český	k2eAgFnSc2d1	Česká
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
fakticky	fakticky	k6eAd1	fakticky
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
spřežka	spřežka	k1gFnSc1	spřežka
(	(	kIx(	(
<g/>
digraf	digraf	k1gInSc1	digraf
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnPc1	písmeno
q	q	k?	q
a	a	k8xC	a
w	w	k?	w
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
a	a	k8xC	a
při	při	k7c6	při
počešťování	počešťování	k1gNnSc6	počešťování
(	(	kIx(	(
<g/>
zdomácnění	zdomácnění	k1gNnSc1	zdomácnění
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
převádějí	převádět	k5eAaImIp3nP	převádět
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
znaky	znak	k1gInPc4	znak
(	(	kIx(	(
<g/>
kv	kv	k?	kv
a	a	k8xC	a
v	v	k7c6	v
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
téměř	téměř	k6eAd1	téměř
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
opodstatnění	opodstatnění	k1gNnSc4	opodstatnění
ó	ó	k0	ó
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
citoslovcí	citoslovce	k1gNnPc2	citoslovce
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
označuje	označovat	k5eAaImIp3nS	označovat
kvantitu	kvantita	k1gFnSc4	kvantita
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
balón	balón	k1gInSc1	balón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reformy	reforma	k1gFnPc1	reforma
zavádějící	zavádějící	k2eAgFnSc4d1	zavádějící
diakritiku	diakritika	k1gFnSc4	diakritika
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
měly	mít	k5eAaImAgInP	mít
také	také	k6eAd1	také
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
abecedy	abeceda	k1gFnSc2	abeceda
dalších	další	k2eAgInPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
slovanských	slovanský	k2eAgFnPc6d1	Slovanská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
baltských	baltský	k2eAgFnPc2d1	Baltská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některých	některý	k3yIgInPc2	některý
znaků	znak	k1gInPc2	znak
je	být	k5eAaImIp3nS	být
užíváno	užívat	k5eAaImNgNnS	užívat
i	i	k9	i
ve	v	k7c6	v
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
transliteracích	transliterace	k1gFnPc6	transliterace
nelatinkových	latinkový	k2eNgNnPc2d1	latinkový
písem	písmo	k1gNnPc2	písmo
(	(	kIx(	(
<g/>
cyrilice	cyrilice	k1gFnSc1	cyrilice
u	u	k7c2	u
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
sada	sada	k1gFnSc1	sada
holých	holý	k2eAgNnPc2d1	holé
písmen	písmeno	k1gNnPc2	písmeno
bez	bez	k7c2	bez
znaků	znak	k1gInPc2	znak
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
a	a	k8xC	a
bez	bez	k7c2	bez
spřežky	spřežka	k1gFnSc2	spřežka
Ch	Ch	kA	Ch
je	být	k5eAaImIp3nS	být
shodná	shodný	k2eAgFnSc1d1	shodná
např.	např.	kA	např.
s	s	k7c7	s
anglickou	anglický	k2eAgFnSc7d1	anglická
abecedou	abeceda	k1gFnSc7	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
znak	znak	k1gInSc1	znak
pořadí	pořadí	k1gNnSc2	pořadí
pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
k	k	k7c3	k
číselnému	číselný	k2eAgNnSc3d1	číselné
označování	označování	k1gNnSc3	označování
<g/>
,	,	kIx,	,
např.	např.	kA	např.
částí	část	k1gFnSc7	část
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
právních	právní	k2eAgInPc2d1	právní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
písmenech	písmeno	k1gNnPc6	písmeno
pro	pro	k7c4	pro
menší	malý	k2eAgFnPc4d2	menší
části	část	k1gFnPc4	část
-	-	kIx~	-
odstavce	odstavec	k1gInPc4	odstavec
<g/>
,	,	kIx,	,
položky	položka	k1gFnPc4	položka
seznamu	seznam	k1gInSc2	seznam
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velká	k1gFnSc1	velká
<g/>
:	:	kIx,	:
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
,	,	kIx,	,
J	J	kA	J
<g/>
,	,	kIx,	,
K	K	kA	K
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Q	Q	kA	Q
<g/>
,	,	kIx,	,
R	R	kA	R
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
,	,	kIx,	,
U	U	kA	U
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
,	,	kIx,	,
W	W	kA	W
<g/>
,	,	kIx,	,
X	X	kA	X
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
Malá	malý	k2eAgFnSc1d1	malá
<g/>
:	:	kIx,	:
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
g	g	kA	g
<g/>
,	,	kIx,	,
h	h	k?	h
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
,	,	kIx,	,
j	j	k?	j
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
p	p	k?	p
<g/>
,	,	kIx,	,
q	q	k?	q
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
v	v	k7c6	v
<g/>
,	,	kIx,	,
w	w	k?	w
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
z.	z.	k?	z.
Velká	velká	k1gFnSc1	velká
písmena	písmeno	k1gNnSc2	písmeno
(	(	kIx(	(
<g/>
majuskule	majuskule	k1gFnSc2	majuskule
<g/>
,	,	kIx,	,
verzálky	verzálka	k1gFnSc2	verzálka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
historicky	historicky	k6eAd1	historicky
za	za	k7c4	za
základní	základní	k2eAgFnSc4d1	základní
variantu	varianta	k1gFnSc4	varianta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
textu	text	k1gInSc6	text
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
podle	podle	k7c2	podle
pravopisu	pravopis	k1gInSc2	pravopis
pro	pro	k7c4	pro
ozvláštnění	ozvláštnění	k1gNnSc4	ozvláštnění
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
nadpisech	nadpis	k1gInPc6	nadpis
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
písmeno	písmeno	k1gNnSc1	písmeno
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
vět	věta	k1gFnPc2	věta
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
názvů	název	k1gInPc2	název
a	a	k8xC	a
jmen	jméno	k1gNnPc2	jméno
<g/>
:	:	kIx,	:
A	A	kA	A
<g/>
,	,	kIx,	,
Á	Á	kA	Á
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
Č	Č	kA	Č
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
Ď	Ď	kA	Ď
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
É	É	kA	É
<g/>
,	,	kIx,	,
Ě	Ě	kA	Ě
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
,	,	kIx,	,
Ch	Ch	kA	Ch
<g/>
,	,	kIx,	,
I	i	k8xC	i
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Í	Í	kA	Í
<g/>
,	,	kIx,	,
J	J	kA	J
<g/>
,	,	kIx,	,
K	K	kA	K
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
,	,	kIx,	,
Ň	Ň	kA	Ň
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
,	,	kIx,	,
Ó	Ó	kA	Ó
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
,	,	kIx,	,
Q	Q	kA	Q
<g/>
,	,	kIx,	,
R	R	kA	R
<g/>
,	,	kIx,	,
Ř	Ř	kA	Ř
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
Š	Š	kA	Š
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Ť	Ť	kA	Ť
<g/>
,	,	kIx,	,
U	U	kA	U
<g/>
,	,	kIx,	,
Ú	Ú	kA	Ú
<g/>
,	,	kIx,	,
Ů	Ů	kA	Ů
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
,	,	kIx,	,
W	W	kA	W
<g/>
,	,	kIx,	,
X	X	kA	X
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
,	,	kIx,	,
Ý	Ý	kA	Ý
<g/>
,	,	kIx,	,
Z	Z	kA	Z
<g/>
,	,	kIx,	,
Ž.	Ž.	kA	Ž.
Malá	malý	k2eAgFnSc1d1	malá
písmena	písmeno	k1gNnPc4	písmeno
(	(	kIx(	(
<g/>
minuskule	minuskule	k1gFnSc1	minuskule
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
praxi	praxe	k1gFnSc4	praxe
častější	častý	k2eAgFnSc7d2	častější
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc7d1	základní
variantou	varianta	k1gFnSc7	varianta
písmen	písmeno	k1gNnPc2	písmeno
v	v	k7c6	v
textu	text	k1gInSc6	text
<g/>
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
<g/>
,	,	kIx,	,
á	á	k0	á
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
č	č	k0	č
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
ď	ď	k?	ď
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
é	é	k0	é
<g/>
,	,	kIx,	,
ě	ě	k?	ě
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
h	h	k?	h
<g/>
,	,	kIx,	,
ch	ch	k0	ch
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
,	,	kIx,	,
í	í	k0	í
<g/>
,	,	kIx,	,
j	j	k?	j
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
,	,	kIx,	,
ň	ň	k?	ň
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
ó	ó	k0	ó
<g/>
,	,	kIx,	,
p	p	k?	p
<g/>
,	,	kIx,	,
q	q	k?	q
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
,	,	kIx,	,
ř	ř	k?	ř
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
,	,	kIx,	,
š	š	k?	š
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
ť	ť	k?	ť
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
ú	ú	k0	ú
<g/>
,	,	kIx,	,
ů	ů	k?	ů
<g/>
,	,	kIx,	,
v	v	k7c6	v
<g/>
,	,	kIx,	,
w	w	k?	w
<g/>
,	,	kIx,	,
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
ý	ý	k?	ý
<g/>
,	,	kIx,	,
z	z	k0	z
<g/>
,	,	kIx,	,
ž.	ž.	k?	ž.
Pro	pro	k7c4	pro
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
období	období	k1gNnSc4	období
psané	psaný	k2eAgFnSc2d1	psaná
češtiny	čeština	k1gFnSc2	čeština
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
nejběžnějším	běžný	k2eAgInSc7d3	nejběžnější
grafickým	grafický	k2eAgInSc7d1	grafický
systémem	systém	k1gInSc7	systém
latinka	latinka	k1gFnSc1	latinka
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
nejstarších	starý	k2eAgFnPc6d3	nejstarší
dobách	doba	k1gFnPc6	doba
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
se	s	k7c7	s
zápisy	zápis	k1gInPc7	zápis
i	i	k8xC	i
v	v	k7c6	v
hebrejském	hebrejský	k2eAgNnSc6d1	hebrejské
písmu	písmo	k1gNnSc6	písmo
(	(	kIx(	(
<g/>
srv	srv	k?	srv
<g/>
.	.	kIx.	.
lešon	lešon	k1gMnSc1	lešon
Kenaan	Kenaan	k1gMnSc1	Kenaan
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
tehdy	tehdy	k6eAd1	tehdy
existujících	existující	k2eAgNnPc6d1	existující
písmech	písmo	k1gNnPc6	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
abeceda	abeceda	k1gFnSc1	abeceda
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
několika	několik	k4yIc7	několik
šířeji	šířej	k1gInPc7	šířej
pojatých	pojatý	k2eAgFnPc2d1	pojatá
reforem	reforma	k1gFnPc2	reforma
celého	celý	k2eAgInSc2d1	celý
pravopisného	pravopisný	k2eAgInSc2d1	pravopisný
systému	systém	k1gInSc2	systém
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
probíhaly	probíhat	k5eAaImAgFnP	probíhat
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
ustálila	ustálit	k5eAaPmAgFnS	ustálit
dnes	dnes	k6eAd1	dnes
známá	známý	k2eAgFnSc1d1	známá
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
české	český	k2eAgInPc1d1	český
zápisy	zápis	k1gInPc1	zápis
v	v	k7c6	v
latince	latinka	k1gFnSc6	latinka
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
primitivního	primitivní	k2eAgInSc2d1	primitivní
a	a	k8xC	a
dvou	dva	k4xCgInPc2	dva
spřežkových	spřežkový	k2eAgInPc2d1	spřežkový
pravopisů	pravopis	k1gInPc2	pravopis
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
prostou	prostý	k2eAgFnSc4d1	prostá
latinskou	latinský	k2eAgFnSc4d1	Latinská
abecedu	abeceda	k1gFnSc4	abeceda
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
F	F	kA	F
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
,	,	kIx,	,
K	K	kA	K
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
,	,	kIx,	,
Q	Q	kA	Q
<g/>
,	,	kIx,	,
R	R	kA	R
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
,	,	kIx,	,
X	X	kA	X
<g/>
)	)	kIx)	)
obohacenou	obohacený	k2eAgFnSc4d1	obohacená
o	o	k7c6	o
u	u	k7c2	u
<g/>
,	,	kIx,	,
w	w	k?	w
<g/>
,	,	kIx,	,
y	y	k?	y
a	a	k8xC	a
z.	z.	k?	z.
Písmeno	písmeno	k1gNnSc1	písmeno
j	j	k?	j
získává	získávat	k5eAaImIp3nS	získávat
svoji	svůj	k3xOyFgFnSc4	svůj
pevnou	pevný	k2eAgFnSc4d1	pevná
pozici	pozice	k1gFnSc4	pozice
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
hlásku	hláska	k1gFnSc4	hláska
/	/	kIx~	/
<g/>
í	í	k0	í
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
gotickém	gotický	k2eAgNnSc6d1	gotické
písmu	písmo	k1gNnSc6	písmo
(	(	kIx(	(
<g/>
od	od	k7c2	od
novověku	novověk	k1gInSc2	novověk
novogotické	novogotický	k2eAgNnSc1d1	novogotické
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
češtinu	čeština	k1gFnSc4	čeština
používalo	používat	k5eAaImAgNnS	používat
až	až	k6eAd1	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
řady	řada	k1gFnSc2	řada
variant	varianta	k1gFnPc2	varianta
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýraznější	výrazný	k2eAgFnSc1d3	nejvýraznější
byla	být	k5eAaImAgFnS	být
dvojice	dvojice	k1gFnSc1	dvojice
koncového	koncový	k2eAgInSc2d1	koncový
s	s	k7c7	s
a	a	k8xC	a
dlouhým	dlouhý	k2eAgFnPc3d1	dlouhá
s	s	k7c7	s
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
poměrně	poměrně	k6eAd1	poměrně
důsledné	důsledný	k2eAgNnSc1d1	důsledné
pravidlo	pravidlo	k1gNnSc1	pravidlo
na	na	k7c6	na
rozlišování	rozlišování	k1gNnSc6	rozlišování
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
první	první	k4xOgFnSc3	první
větší	veliký	k2eAgFnSc3d2	veliký
změno	změna	k1gFnSc5	změna
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
po	po	k7c6	po
popsání	popsání	k1gNnSc6	popsání
diakritického	diakritický	k2eAgInSc2d1	diakritický
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
(	(	kIx(	(
<g/>
a	a	k8xC	a
dílem	dílo	k1gNnSc7	dílo
Orthographia	Orthographius	k1gMnSc2	Orthographius
Bohemica	Bohemicus	k1gMnSc2	Bohemicus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
zavedl	zavést	k5eAaPmAgMnS	zavést
do	do	k7c2	do
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
diakritických	diakritický	k2eAgNnPc2d1	diakritické
znamének	znaménko	k1gNnPc2	znaménko
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
nazval	nazvat	k5eAaPmAgMnS	nazvat
punctus	punctus	k1gMnSc1	punctus
rotundus	rotundus	k1gMnSc1	rotundus
a	a	k8xC	a
gracilis	gracilis	k1gFnSc1	gracilis
virgula	virgula	k1gFnSc1	virgula
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
mylně	mylně	k6eAd1	mylně
překládáno	překládán	k2eAgNnSc4d1	překládáno
jako	jako	k8xC	jako
nabodeníčko	nabodeníčko	k1gNnSc4	nabodeníčko
krátké	krátký	k2eAgNnSc4d1	krátké
a	a	k8xC	a
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krátká	krátký	k2eAgNnPc1d1	krátké
nabodeníčka	nabodeníčko	k1gNnPc1	nabodeníčko
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
tečky	tečka	k1gFnPc4	tečka
nad	nad	k7c7	nad
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
)	)	kIx)	)
označovala	označovat	k5eAaImAgFnS	označovat
měkkost	měkkost	k1gFnSc1	měkkost
a	a	k8xC	a
tvrdé	tvrdý	k2eAgFnPc1d1	tvrdá
l.	l.	k?	l.
Takto	takto	k6eAd1	takto
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
abeceda	abeceda	k1gFnSc1	abeceda
obohacena	obohacen	k2eAgFnSc1d1	obohacena
o	o	k7c4	o
znaky	znak	k1gInPc4	znak
ċ	ċ	k?	ċ
<g/>
,	,	kIx,	,
ṡ	ṡ	k?	ṡ
<g/>
,	,	kIx,	,
ż	ż	k?	ż
<g/>
,	,	kIx,	,
ṙ	ṙ	k?	ṙ
<g/>
,	,	kIx,	,
ḋ	ḋ	k?	ḋ
<g/>
,	,	kIx,	,
ṫ	ṫ	k?	ṫ
<g/>
,	,	kIx,	,
ṅ	ṅ	k?	ṅ
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
̇	̇	k?	̇
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
nabodeníčka	nabodeníčko	k1gNnPc4	nabodeníčko
byla	být	k5eAaImAgFnS	být
obdobná	obdobný	k2eAgFnSc1d1	obdobná
dnešní	dnešní	k2eAgFnSc3d1	dnešní
čárce	čárka	k1gFnSc3	čárka
jak	jak	k6eAd1	jak
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
příliš	příliš	k6eAd1	příliš
neprosadila	prosadit	k5eNaPmAgFnS	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Husův	Husův	k2eAgInSc1d1	Husův
systém	systém	k1gInSc1	systém
také	také	k9	také
dal	dát	k5eAaPmAgInS	dát
základ	základ	k1gInSc4	základ
specifickému	specifický	k2eAgNnSc3d1	specifické
postavení	postavení	k1gNnSc3	postavení
písmena-spřežky	písmenapřežka	k1gFnSc2	písmena-spřežka
ch	ch	k0	ch
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jako	jako	k9	jako
jedinou	jediný	k2eAgFnSc4d1	jediná
ponechal	ponechat	k5eAaPmAgMnS	ponechat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
tzv.	tzv.	kA	tzv.
bratrský	bratrský	k2eAgInSc1d1	bratrský
pravopis	pravopis	k1gInSc1	pravopis
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
výrazné	výrazný	k2eAgFnSc3d1	výrazná
reformě	reforma	k1gFnSc3	reforma
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
ještě	ještě	k6eAd1	ještě
poněkud	poněkud	k6eAd1	poněkud
nesmělé	smělý	k2eNgInPc1d1	nesmělý
náznaky	náznak	k1gInPc1	náznak
reformy	reforma	k1gFnSc2	reforma
v	v	k7c6	v
sobě	se	k3xPyFc3	se
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Gramatika	gramatika	k1gFnSc1	gramatika
česká	český	k2eAgFnSc1d1	Česká
v	v	k7c6	v
dvojí	dvojí	k4xRgFnSc6	dvojí
stránce	stránka	k1gFnSc6	stránka
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
Optát	Optát	k1gMnSc1	Optát
a	a	k8xC	a
Gzel	Gzel	k1gMnSc1	Gzel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
nahradili	nahradit	k5eAaPmAgMnP	nahradit
krátká	krátký	k2eAgNnPc4d1	krátké
nabodeníčka	nabodeníčko	k1gNnPc4	nabodeníčko
háčkem	háček	k1gInSc7	háček
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
hlásky	hláska	k1gFnSc2	hláska
/	/	kIx~	/
<g/>
š	š	k?	š
<g/>
/	/	kIx~	/
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
dva	dva	k4xCgInPc4	dva
způsoby	způsob	k1gInPc4	způsob
zápisu	zápis	k1gInSc2	zápis
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
s	s	k7c7	s
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
koncové	koncový	k2eAgNnSc4d1	koncové
s	s	k7c7	s
<g/>
,	,	kIx,	,
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
spřežkou	spřežka	k1gFnSc7	spřežka
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
s	s	k7c7	s
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
nepraktické	praktický	k2eNgNnSc1d1	nepraktické
nadepisovat	nadepisovat	k5eAaImF	nadepisovat
znaménkem	znaménko	k1gNnSc7	znaménko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
diakritikou	diakritika	k1gFnSc7	diakritika
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
ale	ale	k9	ale
nepoužíval	používat	k5eNaImAgMnS	používat
háček	háček	k1gInSc4	háček
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dvojtečka	dvojtečka	k1gFnSc1	dvojtečka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
psalo	psát	k5eAaImAgNnS	psát
s	s	k7c7	s
<g/>
̈	̈	k?	̈
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
užito	užít	k5eAaPmNgNnS	užít
speciálního	speciální	k2eAgNnSc2d1	speciální
znaku	znak	k1gInSc3	znak
pro	pro	k7c4	pro
tvrdé	tvrdý	k2eAgNnSc4d1	tvrdé
l.	l.	k?	l.
Kvantitu	kvantita	k1gFnSc4	kvantita
označují	označovat	k5eAaImIp3nP	označovat
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Hus	Hus	k1gMnSc1	Hus
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavější	zajímavý	k2eAgInPc4d2	zajímavější
dopady	dopad	k1gInPc4	dopad
na	na	k7c4	na
abecedu	abeceda	k1gFnSc4	abeceda
mělo	mít	k5eAaImAgNnS	mít
historicky	historicky	k6eAd1	historicky
dané	daný	k2eAgNnSc4d1	dané
složité	složitý	k2eAgNnSc4d1	složité
psaní	psaní	k1gNnSc4	psaní
hlásek	hláska	k1gFnPc2	hláska
/	/	kIx~	/
<g/>
i	i	k9	i
<g/>
/	/	kIx~	/
-	-	kIx~	-
/	/	kIx~	/
<g/>
í	í	k0	í
<g/>
/	/	kIx~	/
-	-	kIx~	-
/	/	kIx~	/
<g/>
j	j	k?	j
<g/>
/	/	kIx~	/
-	-	kIx~	-
/	/	kIx~	/
<g/>
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
hlásku	hláska	k1gFnSc4	hláska
/	/	kIx~	/
<g/>
í	í	k0	í
<g/>
/	/	kIx~	/
nepoužívali	používat	k5eNaImAgMnP	používat
diakritiku	diakritika	k1gFnSc4	diakritika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spřežku	spřežka	k1gFnSc4	spřežka
ij	ij	k?	ij
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
zjednodušilo	zjednodušit	k5eAaPmAgNnS	zjednodušit
na	na	k7c6	na
j.	j.	k?	j.
Naopak	naopak	k6eAd1	naopak
háčkem	háček	k1gInSc7	háček
nadepisovali	nadepisovat	k5eAaImAgMnP	nadepisovat
ještě	ještě	k6eAd1	ještě
ǧ	ǧ	k?	ǧ
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
reprezentovalo	reprezentovat	k5eAaImAgNnS	reprezentovat
dnešní	dnešní	k2eAgMnSc1d1	dnešní
/	/	kIx~	/
<g/>
g	g	kA	g
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
prosté	prostý	k2eAgFnSc6d1	prostá
g	g	kA	g
bylo	být	k5eAaImAgNnS	být
vyhrazeno	vyhradit	k5eAaPmNgNnS	vyhradit
pro	pro	k7c4	pro
hlásku	hláska	k1gFnSc4	hláska
/	/	kIx~	/
<g/>
j	j	k?	j
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
první	první	k4xOgFnSc4	první
českou	český	k2eAgFnSc4d1	Česká
gramatiku	gramatika	k1gFnSc4	gramatika
reagoval	reagovat	k5eAaBmAgMnS	reagovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1571	[number]	k4	1571
významný	významný	k2eAgMnSc1d1	významný
bratrský	bratrský	k2eAgMnSc1d1	bratrský
učenec	učenec	k1gMnSc1	učenec
Jan	Jan	k1gMnSc1	Jan
Blahoslav	blahoslavit	k5eAaImRp2nS	blahoslavit
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zejména	zejména	k9	zejména
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
některé	některý	k3yIgFnPc4	některý
příliš	příliš	k6eAd1	příliš
zjednodušující	zjednodušující	k2eAgFnPc4d1	zjednodušující
úpravy	úprava	k1gFnPc4	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
zavedl	zavést	k5eAaPmAgInS	zavést
doposud	doposud	k6eAd1	doposud
užívané	užívaný	k2eAgNnSc4d1	užívané
ů.	ů.	k?	ů.
Znovu	znovu	k6eAd1	znovu
zavedl	zavést	k5eAaPmAgInS	zavést
odlišování	odlišování	k1gNnSc4	odlišování
tvrdého	tvrdé	k1gNnSc2	tvrdé
l	l	kA	l
(	(	kIx(	(
<g/>
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
znak	znak	k1gInSc4	znak
tzv.	tzv.	kA	tzv.
l	l	kA	l
s	s	k7c7	s
kličkou	klička	k1gFnSc7	klička
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
koncový	koncový	k2eAgInSc4d1	koncový
dřík	dřík	k1gInSc4	dřík
písmena	písmeno	k1gNnSc2	písmeno
nekončí	končit	k5eNaImIp3nS	končit
otevřeným	otevřený	k2eAgInSc7d1	otevřený
obloučkem	oblouček	k1gInSc7	oblouček
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
dotažen	dotáhnout	k5eAaPmNgInS	dotáhnout
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kličky	klička	k1gFnSc2	klička
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
dříku	dřík	k1gInSc3	dřík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
nepoužívalo	používat	k5eNaImAgNnS	používat
zcela	zcela	k6eAd1	zcela
důsledně	důsledně	k6eAd1	důsledně
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
vymizelo	vymizet	k5eAaPmAgNnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
neprosadilo	prosadit	k5eNaPmAgNnS	prosadit
ani	ani	k8xC	ani
navrhované	navrhovaný	k2eAgNnSc1d1	navrhované
odlišování	odlišování	k1gNnSc1	odlišování
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
ě	ě	k?	ě
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
znaku	znak	k1gInSc2	znak
j	j	k?	j
<g/>
ͤ	ͤ	k?	ͤ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
několik	několik	k4yIc4	několik
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
abecedu	abeceda	k1gFnSc4	abeceda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
užívaná	užívaný	k2eAgNnPc4d1	užívané
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
přecházela	přecházet	k5eAaImAgFnS	přecházet
čeština	čeština	k1gFnSc1	čeština
z	z	k7c2	z
novogotické	novogotický	k2eAgFnSc2d1	novogotická
podoby	podoba	k1gFnSc2	podoba
latinky	latinka	k1gFnSc2	latinka
na	na	k7c4	na
humanistickou	humanistický	k2eAgFnSc4d1	humanistická
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
odpadla	odpadnout	k5eAaPmAgFnS	odpadnout
nutnost	nutnost	k1gFnSc4	nutnost
dvojího	dvojí	k4xRgNnSc2	dvojí
psaní	psaní	k1gNnSc2	psaní
hlásky	hláska	k1gFnSc2	hláska
/	/	kIx~	/
<g/>
š	š	k?	š
<g/>
/	/	kIx~	/
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
hláska	hláska	k1gFnSc1	hláska
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
psát	psát	k5eAaImF	psát
již	již	k6eAd1	již
moderním	moderní	k2eAgInSc7d1	moderní
znakem	znak	k1gInSc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
provedl	provést	k5eAaPmAgMnS	provést
P.	P.	kA	P.
J.	J.	kA	J.
Šafařík	Šafařík	k1gMnSc1	Šafařík
tzv.	tzv.	kA	tzv.
reformu	reforma	k1gFnSc4	reforma
skladnou	skladný	k2eAgFnSc7d1	skladná
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ustálila	ustálit	k5eAaPmAgFnS	ustálit
moderní	moderní	k2eAgNnSc4d1	moderní
psaní	psaní	k1gNnSc4	psaní
i	i	k8xC	i
-	-	kIx~	-
í	í	k0	í
-	-	kIx~	-
j	j	k?	j
-	-	kIx~	-
g	g	kA	g
(	(	kIx(	(
<g/>
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
abecedy	abeceda	k1gFnSc2	abeceda
definitivně	definitivně	k6eAd1	definitivně
zmizel	zmizet	k5eAaPmAgInS	zmizet
znak	znak	k1gInSc1	znak
ǧ	ǧ	k?	ǧ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pak	pak	k6eAd1	pak
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
již	již	k6eAd1	již
poslední	poslední	k2eAgFnSc1d1	poslední
reforma	reforma	k1gFnSc1	reforma
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zavedla	zavést	k5eAaPmAgFnS	zavést
psaní	psaní	k1gNnSc4	psaní
v	v	k7c4	v
místo	místo	k1gNnSc4	místo
dvojitého	dvojitý	k2eAgInSc2d1	dvojitý
w.	w.	k?	w.
Tak	tak	k9	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
definitivní	definitivní	k2eAgFnSc1d1	definitivní
česká	český	k2eAgFnSc1d1	Česká
abeceda	abeceda	k1gFnSc1	abeceda
-	-	kIx~	-
sada	sada	k1gFnSc1	sada
znaků	znak	k1gInPc2	znak
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
pořadí	pořadí	k1gNnSc2	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
standardů	standard	k1gInPc2	standard
pro	pro	k7c4	pro
kódování	kódování	k1gNnSc4	kódování
české	český	k2eAgFnSc2d1	Česká
abecedy	abeceda	k1gFnSc2	abeceda
v	v	k7c6	v
počítačovém	počítačový	k2eAgNnSc6d1	počítačové
prostředí	prostředí	k1gNnSc6	prostředí
ISO	ISO	kA	ISO
8859-2	[number]	k4	8859-2
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
code	code	k1gFnSc1	code
page	page	k1gFnSc1	page
1250	[number]	k4	1250
IBM	IBM	kA	IBM
PC	PC	kA	PC
code	cod	k1gMnSc2	cod
page	pagat	k5eAaPmIp3nS	pagat
852	[number]	k4	852
Kód	kód	k1gInSc1	kód
Kamenických	Kamenických	k2eAgInSc1d1	Kamenických
Unicode	Unicod	k1gInSc5	Unicod
Kromě	kromě	k7c2	kromě
písmen	písmeno	k1gNnPc2	písmeno
české	český	k2eAgFnSc2d1	Česká
abecedy	abeceda	k1gFnSc2	abeceda
se	se	k3xPyFc4	se
v	v	k7c6	v
nečeských	český	k2eNgNnPc6d1	nečeské
jménech	jméno	k1gNnPc6	jméno
(	(	kIx(	(
<g/>
zejména	zejména	k6eAd1	zejména
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
zeměpisných	zeměpisný	k2eAgInPc2d1	zeměpisný
prvků	prvek	k1gInPc2	prvek
<g/>
)	)	kIx)	)
často	často	k6eAd1	často
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
latinková	latinkový	k2eAgNnPc4d1	latinkový
písmena	písmeno	k1gNnPc4	písmeno
s	s	k7c7	s
diakritickými	diakritický	k2eAgFnPc7d1	diakritická
značkami	značka	k1gFnPc7	značka
v	v	k7c6	v
originální	originální	k2eAgFnSc6d1	originální
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
abeced	abeceda	k1gFnPc2	abeceda
<g/>
,	,	kIx,	,
nejběžněji	běžně	k6eAd3	běžně
německé	německý	k2eAgFnSc2d1	německá
přehlásky	přehláska	k1gFnSc2	přehláska
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
někdy	někdy	k6eAd1	někdy
méně	málo	k6eAd2	málo
běžné	běžný	k2eAgInPc1d1	běžný
znaky	znak	k1gInPc1	znak
(	(	kIx(	(
<g/>
např.	např.	kA	např.
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
pod	pod	k7c7	pod
základmním	základmní	k2eAgNnSc7d1	základmní
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
českými	český	k2eAgNnPc7d1	české
písmeny	písmeno	k1gNnPc7	písmeno
s	s	k7c7	s
odpovídající	odpovídající	k2eAgFnSc7d1	odpovídající
hláskou	hláska	k1gFnSc7	hláska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Braș	Braș	k1gFnSc4	Braș
jako	jako	k8xS	jako
Brašov	Brašov	k1gInSc4	Brašov
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
těchto	tento	k3xDgInPc2	tento
znaků	znak	k1gInPc2	znak
pak	pak	k6eAd1	pak
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
originální	originální	k2eAgFnSc7d1	originální
výslovností	výslovnost	k1gFnSc7	výslovnost
původního	původní	k2eAgInSc2d1	původní
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
(	(	kIx(	(
<g/>
častěji	často	k6eAd2	často
<g/>
)	)	kIx)	)
plnou	plný	k2eAgFnSc4d1	plná
náhradou	náhrada	k1gFnSc7	náhrada
českými	český	k2eAgInPc7d1	český
hláskami	hláska	k1gFnPc7	hláska
nejbližšími	blízký	k2eAgMnPc7d3	nejbližší
k	k	k7c3	k
originálu	originál	k1gInSc3	originál
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
hlásku	hláska	k1gFnSc4	hláska
neobsaženou	obsažený	k2eNgFnSc4d1	neobsažená
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Händel	Händlo	k1gNnPc2	Händlo
jako	jako	k8xC	jako
/	/	kIx~	/
<g/>
Hendl	Hendl	k1gFnSc1	Hendl
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
Göring	Göring	k1gInSc1	Göring
jako	jako	k8xC	jako
/	/	kIx~	/
<g/>
Gérink	Gérink	k1gInSc1	Gérink
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
Wałęsa	Wałęsa	k1gFnSc1	Wałęsa
jako	jako	k9	jako
/	/	kIx~	/
<g/>
Valensa	Valensa	k1gFnSc1	Valensa
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
Kieślowski	Kieślowski	k1gNnPc1	Kieślowski
jako	jako	k8xS	jako
/	/	kIx~	/
<g/>
kěšlovsky	kěšlovsky	k6eAd1	kěšlovsky
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
Braș	Braș	k1gMnSc1	Braș
jako	jako	k8xC	jako
/	/	kIx~	/
<g/>
Brašov	Brašov	k1gInSc1	Brašov
<g/>
/	/	kIx~	/
nebo	nebo	k8xC	nebo
Besançon	Besançon	k1gInSc4	Besançon
jako	jako	k8xC	jako
/	/	kIx~	/
<g/>
Bezanson	Bezanson	k1gInSc1	Bezanson
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Abecední	abecední	k2eAgNnSc1d1	abecední
řazení	řazení	k1gNnSc1	řazení
Psací	psací	k2eAgNnSc1d1	psací
písmo	písmo	k1gNnSc1	písmo
</s>
