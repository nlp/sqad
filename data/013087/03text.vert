<p>
<s>
Saniyya	Saniyya	k1gFnSc1	Saniyya
Sidney	Sidnea	k1gFnSc2	Sidnea
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Proslavila	proslavit	k5eAaPmAgFnS	proslavit
se	se	k3xPyFc4	se
rolemi	role	k1gFnPc7	role
v	v	k7c6	v
seriálech	seriál	k1gInPc6	seriál
American	Americana	k1gFnPc2	Americana
Horror	horror	k1gInSc4	horror
Story	story	k1gFnSc4	story
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Passage	Passage	k1gFnSc1	Passage
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Ploty	plot	k1gInPc1	plot
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
a	a	k8xC	a
Skrytá	skrytý	k2eAgNnPc1d1	skryté
čísla	číslo	k1gNnPc1	číslo
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
herectvím	herectví	k1gNnSc7	herectví
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
krátkometrážním	krátkometrážní	k2eAgInSc6d1	krátkometrážní
filmu	film	k1gInSc6	film
The	The	k1gFnSc1	The
Babysitters	Babysitters	k1gInSc1	Babysitters
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
mini-sérii	miniérie	k1gFnSc6	mini-série
Roots	Rootsa	k1gFnPc2	Rootsa
a	a	k8xC	a
seriálu	seriál	k1gInSc2	seriál
American	American	k1gInSc4	American
Horror	horror	k1gInSc1	horror
Story	story	k1gFnSc1	story
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Ploty	plot	k1gInPc4	plot
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
čtyři	čtyři	k4xCgFnPc4	čtyři
nominace	nominace	k1gFnPc4	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Oscar	Oscara	k1gFnPc2	Oscara
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jednoho	jeden	k4xCgNnSc2	jeden
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
si	se	k3xPyFc3	se
také	také	k9	také
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc2	film
Skrytá	skrytý	k2eAgNnPc1d1	skryté
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
tři	tři	k4xCgFnPc4	tři
nominace	nominace	k1gFnPc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2017	[number]	k4	2017
stanice	stanice	k1gFnSc1	stanice
Fox	fox	k1gInSc1	fox
oznámila	oznámit	k5eAaPmAgFnS	oznámit
obsazení	obsazení	k1gNnSc4	obsazení
Sidney	Sidnea	k1gFnSc2	Sidnea
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
The	The	k1gFnSc2	The
Passage	Passag	k1gFnSc2	Passag
<g/>
.	.	kIx.	.
</s>
<s>
Pilotní	pilotní	k2eAgInSc1d1	pilotní
díl	díl	k1gInSc1	díl
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Fast	Fast	k1gMnSc1	Fast
Color	Color	k1gMnSc1	Color
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
světovou	světový	k2eAgFnSc4d1	světová
premiéru	premiéra	k1gFnSc4	premiéra
na	na	k7c4	na
South	South	k1gInSc4	South
by	by	kYmCp3nS	by
Southwest	Southwest	k1gMnSc1	Southwest
festivalu	festival	k1gInSc2	festival
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Televize	televize	k1gFnSc1	televize
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Saniyya	Saniyyus	k1gMnSc2	Saniyyus
Sidney	Sidnea	k1gMnSc2	Sidnea
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Saniyya	Saniyya	k1gFnSc1	Saniyya
Sidney	Sidnea	k1gFnSc2	Sidnea
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
