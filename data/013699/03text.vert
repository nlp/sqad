<s>
Výbuchy	výbuch	k1gInPc1
muničních	muniční	k2eAgInPc2d1
skladů	sklad	k1gInPc2
ve	v	k7c6
Vrběticích	Vrběticí	k2eAgFnPc6d1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
reaguje	reagovat	k5eAaBmIp3nS
na	na	k7c4
aktuální	aktuální	k2eAgFnPc4d1
nebo	nebo	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Informace	informace	k1gFnPc1
zde	zde	k6eAd1
uvedené	uvedený	k2eAgInPc4d1
se	s	k7c7
vzhledem	vzhled	k1gInSc7
k	k	k7c3
neustálému	neustálý	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
mohou	moct	k5eAaImIp3nP
průběžně	průběžně	k6eAd1
měnit	měnit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
je	být	k5eAaImIp3nS
se	s	k7c7
zvýšenou	zvýšený	k2eAgFnSc7d1
péčí	péče	k1gFnSc7
aktualizovat	aktualizovat	k5eAaBmF
a	a	k8xC
doplňovat	doplňovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Výbuchy	výbuch	k1gInPc1
muničních	muniční	k2eAgInPc2d1
skladů	sklad	k1gInPc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ohrazení	ohrazení	k1gNnPc1
Policie	policie	k1gFnSc2
ČR	ČR	kA
se	s	k7c7
zákazem	zákaz	k1gInSc7
vstupu	vstup	k1gInSc2
při	při	k7c6
silnici	silnice	k1gFnSc6
Lipová	lipový	k2eAgFnSc1d1
<g/>
–	–	k?
<g/>
Haluzice	Haluzice	k1gFnSc1
v	v	k7c6
květnu	květen	k1gInSc6
2018	#num#	k4
Cíl	cíl	k1gInSc1
</s>
<s>
muniční	muniční	k2eAgInSc4d1
sklad	sklad	k1gInSc4
Mrtví	mrtvit	k5eAaImIp3nP
lidé	člověk	k1gMnPc1
</s>
<s>
2	#num#	k4
Výsledek	výsledek	k1gInSc1
</s>
<s>
zničení	zničení	k1gNnSc1
dvou	dva	k4xCgFnPc2
skladištních	skladištní	k2eAgFnPc2d1
budov	budova	k1gFnPc2
<g/>
,	,	kIx,
rozmetání	rozmetání	k1gNnSc4
nevybuchlé	vybuchlý	k2eNgFnSc2d1
munice	munice	k1gFnSc2
do	do	k7c2
okolí	okolí	k1gNnSc2
Důsledky	důsledek	k1gInPc1
</s>
<s>
škody	škoda	k1gFnPc1
v	v	k7c6
řádu	řád	k1gInSc6
stovek	stovka	k1gFnPc2
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
Datum	datum	k1gNnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
20143	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
Místo	místo	k7c2
</s>
<s>
Vrbětice	Vrbětice	k1gFnSc1
Motiv	motiv	k1gInSc1
</s>
<s>
válka	válka	k1gFnSc1
na	na	k7c6
východě	východ	k1gInSc6
Ukrajiny	Ukrajina	k1gFnSc2
Pachatelé	pachatel	k1gMnPc1
</s>
<s>
GRU	GRU	kA
Použité	použitý	k2eAgFnPc4d1
zbraně	zbraň	k1gFnPc4
</s>
<s>
sabotáž	sabotáž	k1gFnSc1
munice	munice	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
8	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
42	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Muniční	muniční	k2eAgInSc1d1
sklad	sklad	k1gInSc1
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
</s>
<s>
K	k	k7c3
výbuchům	výbuch	k1gInPc3
dvou	dva	k4xCgInPc2
muničních	muniční	k2eAgInPc2d1
skladů	sklad	k1gInPc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
v	v	k7c6
okrese	okres	k1gInSc6
Zlín	Zlín	k1gInSc1
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
říjnu	říjen	k1gInSc6
a	a	k8xC
prosinci	prosinec	k1gInSc6
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
výbuch	výbuch	k1gInSc1
nastal	nastat	k5eAaPmAgInS
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
,	,	kIx,
zemřeli	zemřít	k5eAaPmAgMnP
při	při	k7c6
něm	on	k3xPp3gNnSc6
dva	dva	k4xCgMnPc1
lidé	člověk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
sklad	sklad	k1gInSc1
vybuchl	vybuchnout	k5eAaPmAgInS
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sousední	sousední	k2eAgFnPc1d1
obce	obec	k1gFnPc1
musely	muset	k5eAaImAgFnP
být	být	k5eAaImF
krátkodobě	krátkodobě	k6eAd1
evakuovány	evakuovat	k5eAaBmNgFnP
<g/>
,	,	kIx,
areál	areál	k1gInSc1
byl	být	k5eAaImAgInS
ohrazen	ohradit	k5eAaPmNgInS
a	a	k8xC
poté	poté	k6eAd1
následovalo	následovat	k5eAaImAgNnS
několikaleté	několikaletý	k2eAgNnSc1d1
odklízení	odklízení	k1gNnSc1
následků	následek	k1gInPc2
a	a	k8xC
vyšetřování	vyšetřování	k1gNnSc2
příčin	příčina	k1gFnPc2
výbuchu	výbuch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zásah	zásah	k1gInSc1
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
složek	složka	k1gFnPc2
byl	být	k5eAaImAgInS
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
ukončen	ukončit	k5eAaPmNgInS
po	po	k7c6
šesti	šest	k4xCc6
letech	léto	k1gNnPc6
13	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
dubnu	duben	k1gInSc6
2021	#num#	k4
vláda	vláda	k1gFnSc1
zveřejnila	zveřejnit	k5eAaPmAgFnS
důvodné	důvodný	k2eAgNnSc4d1
podezření	podezření	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
výbuchů	výbuch	k1gInPc2
byli	být	k5eAaImAgMnP
zapojení	zapojený	k2eAgMnPc1d1
důstojníci	důstojník	k1gMnPc1
ruských	ruský	k2eAgFnPc2d1
tajných	tajný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nájemce	nájemce	k1gMnSc1
</s>
<s>
Oba	dva	k4xCgInPc4
zničené	zničený	k2eAgInPc4d1
sklady	sklad	k1gInPc4
měla	mít	k5eAaImAgFnS
v	v	k7c6
pronájmu	pronájem	k1gInSc6
ostravská	ostravský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Imex	Imex	k1gInSc1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
obchodem	obchod	k1gInSc7
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
a	a	k8xC
v	v	k7c6
areálu	areál	k1gInSc6
měla	mít	k5eAaImAgFnS
od	od	k7c2
armády	armáda	k1gFnSc2
pronajato	pronajmout	k5eAaPmNgNnS
celkem	celkem	k6eAd1
šest	šest	k4xCc1
budov	budova	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgMnPc6,k3yIgMnPc6,k3yQgMnPc6
skladovala	skladovat	k5eAaImAgFnS
munici	munice	k1gFnSc4
<g/>
,	,	kIx,
zbraně	zbraň	k1gFnPc4
a	a	k8xC
jiné	jiný	k2eAgNnSc4d1
zboží	zboží	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastníkem	vlastník	k1gMnSc7
byl	být	k5eAaImAgMnS
Petr	Petr	k1gMnSc1
Bernatík	Bernatík	k1gMnSc1
ml.	ml.	kA
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
současně	současně	k6eAd1
výkonným	výkonný	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
a	a	k8xC
společně	společně	k6eAd1
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
otcem	otec	k1gMnSc7
působil	působit	k5eAaImAgMnS
také	také	k9
v	v	k7c6
několika	několik	k4yIc6
dalších	další	k2eAgFnPc6d1
zbrojařských	zbrojařský	k2eAgFnPc6d1
firmách	firma	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
byli	být	k5eAaImAgMnP
oba	dva	k4xCgInPc4
obviněni	obvinit	k5eAaPmNgMnP
z	z	k7c2
nelegálního	legální	k2eNgInSc2d1
vývozu	vývoz	k1gInSc2
zbraní	zbraň	k1gFnPc2
na	na	k7c4
Slovensko	Slovensko	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
soudem	soud	k1gInSc7
byli	být	k5eAaImAgMnP
zproštěni	zproštěn	k2eAgMnPc1d1
viny	vina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výbuch	výbuch	k1gInSc1
prvního	první	k4xOgInSc2
skladu	sklad	k1gInSc2
</s>
<s>
K	k	k7c3
prvnímu	první	k4xOgInSc3
výbuchu	výbuch	k1gInSc3
došlo	dojít	k5eAaPmAgNnS
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
před	před	k7c7
desátou	desátá	k1gFnSc7
hodinou	hodina	k1gFnSc7
ve	v	k7c6
skladu	sklad	k1gInSc6
č.	č.	k?
16	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
obsahoval	obsahovat	k5eAaImAgInS
50	#num#	k4
tun	tuna	k1gFnPc2
munice	munice	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
po	po	k7c6
výbuchu	výbuch	k1gInSc6
odhozena	odhozen	k2eAgFnSc1d1
do	do	k7c2
vzdálenosti	vzdálenost	k1gFnSc2
až	až	k9
800	#num#	k4
metrů	metr	k1gInPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
výbuchu	výbuch	k1gInSc6
byli	být	k5eAaImAgMnP
zabiti	zabít	k5eAaPmNgMnP
dva	dva	k4xCgMnPc1
zaměstnanci	zaměstnanec	k1gMnPc1
společnosti	společnost	k1gFnSc2
Imex	Imex	k1gInSc1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
si	se	k3xPyFc3
sklad	sklad	k1gInSc4
pronajímala	pronajímat	k5eAaImAgFnS
od	od	k7c2
státního	státní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
Vojenský	vojenský	k2eAgInSc1d1
technický	technický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Den	den	k1gInSc1
před	před	k7c7
výbuchem	výbuch	k1gInSc7
navštívil	navštívit	k5eAaPmAgMnS
sklad	sklad	k1gInSc4
majitel	majitel	k1gMnSc1
Imexu	Imex	k1gInSc2
Petr	Petr	k1gMnSc1
Bernatík	Bernatík	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Výbuch	výbuch	k1gInSc1
nastal	nastat	k5eAaPmAgInS
až	až	k9
po	po	k7c6
cca	cca	kA
40	#num#	k4
minut	minuta	k1gFnPc2
od	od	k7c2
vypuknutí	vypuknutí	k1gNnSc2
požáru	požár	k1gInSc2
<g/>
,	,	kIx,
hasiči	hasič	k1gMnPc1
přijeli	přijet	k5eAaPmAgMnP
<g/>
,	,	kIx,
obě	dva	k4xCgNnPc4
pancířová	pancířový	k2eAgNnPc4d1
vrata	vrata	k1gNnPc4
byla	být	k5eAaImAgFnS
otevřena	otevřen	k2eAgFnSc1d1
<g/>
,	,	kIx,
ze	z	k7c2
střechy	střecha	k1gFnSc2
vycházel	vycházet	k5eAaImAgInS
kouř	kouř	k1gInSc1
a	a	k8xC
plameny	plamen	k1gInPc1
<g/>
,	,	kIx,
udělali	udělat	k5eAaPmAgMnP
fotodokumentaci	fotodokumentace	k1gFnSc4
<g/>
,	,	kIx,
zhodnotili	zhodnotit	k5eAaPmAgMnP
situaci	situace	k1gFnSc4
a	a	k8xC
odjeli	odjet	k5eAaPmAgMnP
a	a	k8xC
cca	cca	kA
20	#num#	k4
minut	minuta	k1gFnPc2
po	po	k7c6
jejich	jejich	k3xOp3gInSc6
odjezdu	odjezd	k1gInSc6
sklad	sklad	k1gInSc1
explodoval	explodovat	k5eAaBmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Muniční	muniční	k2eAgInSc1d1
sklad	sklad	k1gInSc1
nebyl	být	k5eNaImAgInS
zanesen	zanést	k5eAaPmNgInS
do	do	k7c2
krizového	krizový	k2eAgInSc2d1
plánu	plán	k1gInSc2
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
zasahující	zasahující	k2eAgMnPc1d1
hasiči	hasič	k1gMnPc1
tak	tak	k6eAd1
neměli	mít	k5eNaImAgMnP
tušení	tušení	k1gNnSc4
<g/>
,	,	kIx,
k	k	k7c3
jakému	jaký	k3yQgInSc3,k3yRgInSc3,k3yIgInSc3
typu	typ	k1gInSc3
požáru	požár	k1gInSc2
vyjíždějí	vyjíždět	k5eAaImIp3nP
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	on	k3xPp3gMnPc4
vystavilo	vystavit	k5eAaPmAgNnS
zbytečnému	zbytečný	k2eAgNnSc3d1
riziku	riziko	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Velitel	velitel	k1gMnSc1
hasičů	hasič	k1gMnPc2
z	z	k7c2
Valašských	valašský	k2eAgInPc2d1
Klobouk	Klobouky	k1gInPc2
Jiří	Jiří	k1gMnSc1
Ovesný	ovesný	k2eAgMnSc1d1
po	po	k7c6
krátkém	krátký	k2eAgInSc6d1
průzkumu	průzkum	k1gInSc6
rozhodl	rozhodnout	k5eAaPmAgInS
o	o	k7c6
okamžitém	okamžitý	k2eAgNnSc6d1
stažení	stažení	k1gNnSc6
zasahujících	zasahující	k2eAgMnPc2d1
hasičů	hasič	k1gMnPc2
a	a	k8xC
díky	díky	k7c3
tomuto	tento	k3xDgNnSc3
rozhodnutí	rozhodnutí	k1gNnSc3
zachránil	zachránit	k5eAaPmAgMnS
lidské	lidský	k2eAgInPc4d1
životy	život	k1gInPc4
<g/>
;	;	kIx,
posléze	posléze	k6eAd1
za	za	k7c4
to	ten	k3xDgNnSc4
byl	být	k5eAaImAgMnS
prezidentem	prezident	k1gMnSc7
vyznamenán	vyznamenat	k5eAaPmNgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Bezprostředně	bezprostředně	k6eAd1
po	po	k7c6
výbuchu	výbuch	k1gInSc6
bylo	být	k5eAaImAgNnS
z	z	k7c2
areálu	areál	k1gInSc2
muničního	muniční	k2eAgInSc2d1
skladu	sklad	k1gInSc2
evakuováno	evakuován	k2eAgNnSc1d1
sto	sto	k4xCgNnSc4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evakuováni	evakuovat	k5eAaBmNgMnP
byli	být	k5eAaImAgMnP
též	též	k9
obyvatelé	obyvatel	k1gMnPc1
přilehlé	přilehlý	k2eAgFnSc2d1
obce	obec	k1gFnSc2
Vlachovice	Vlachovice	k1gFnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
žáci	žák	k1gMnPc1
ze	z	k7c2
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
a	a	k8xC
učni	učeň	k1gMnPc1
ze	z	k7c2
SOŠ	SOŠ	kA
Slavičín	Slavičína	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
zahájila	zahájit	k5eAaPmAgFnS
policie	policie	k1gFnSc1
evakuaci	evakuace	k1gFnSc4
375	#num#	k4
obyvatel	obyvatel	k1gMnPc2
z	z	k7c2
obcí	obec	k1gFnPc2
Lipová	lipový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Vlachovic	Vlachovice	k1gFnPc2
a	a	k8xC
průmyslového	průmyslový	k2eAgInSc2d1
areálu	areál	k1gInSc2
u	u	k7c2
města	město	k1gNnSc2
Slavičín	Slavičína	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evakuace	evakuace	k1gFnSc1
<g/>
,	,	kIx,
trvající	trvající	k2eAgInPc1d1
dva	dva	k4xCgInPc1
dny	den	k1gInPc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
z	z	k7c2
preventivních	preventivní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
pyrotechnici	pyrotechnik	k1gMnPc1
totiž	totiž	k9
zahájili	zahájit	k5eAaPmAgMnP
průzkum	průzkum	k1gInSc4
částí	část	k1gFnPc2
areálu	areál	k1gInSc2
blíže	blíž	k1gFnSc2
k	k	k7c3
obcím	obec	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
areálu	areál	k1gInSc6
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
náhodným	náhodný	k2eAgFnPc3d1
neřízeným	řízený	k2eNgFnPc3d1
detonacím	detonace	k1gFnPc3
způsobených	způsobený	k2eAgFnPc2d1
například	například	k6eAd1
pádem	pád	k1gInSc7
munice	munice	k1gFnSc2
ze	z	k7c2
stromů	strom	k1gInPc2
nebo	nebo	k8xC
kontaktem	kontakt	k1gInSc7
s	s	k7c7
lesní	lesní	k2eAgFnSc7d1
zvěří	zvěř	k1gFnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
znemožňovaly	znemožňovat	k5eAaImAgFnP
ohledání	ohledání	k1gNnSc4
místa	místo	k1gNnSc2
a	a	k8xC
likvidační	likvidační	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
policie	policie	k1gFnSc2
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
přístupové	přístupový	k2eAgFnPc1d1
cesty	cesta	k1gFnPc1
v	v	k7c6
areálu	areál	k1gInSc6
byly	být	k5eAaImAgInP
vyčištěny	vyčištěn	k2eAgInPc1d1
a	a	k8xC
brzy	brzy	k6eAd1
tak	tak	k9
může	moct	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
k	k	k7c3
odvozu	odvoz	k1gInSc3
7	#num#	k4
tisíc	tisíc	k4xCgInPc2
tun	tuna	k1gFnPc2
munice	munice	k1gFnSc2
ze	z	k7c2
skladů	sklad	k1gInPc2
v	v	k7c6
postiženém	postižený	k2eAgInSc6d1
areálu	areál	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výbuch	výbuch	k1gInSc1
druhého	druhý	k4xOgInSc2
skladu	sklad	k1gInSc2
</s>
<s>
Dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
došlo	dojít	k5eAaPmAgNnS
ve	v	k7c6
skladu	sklad	k1gInSc6
číslo	číslo	k1gNnSc1
12	#num#	k4
k	k	k7c3
dalšímu	další	k2eAgInSc3d1
výbuchu	výbuch	k1gInSc3
munice	munice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sklad	sklad	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
obsahoval	obsahovat	k5eAaImAgInS
13	#num#	k4
tun	tuna	k1gFnPc2
výbušnin	výbušnina	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgInS
vzdálen	vzdálit	k5eAaPmNgInS
1	#num#	k4
200	#num#	k4
metrů	metr	k1gInPc2
od	od	k7c2
epicentra	epicentrum	k1gNnSc2
původního	původní	k2eAgInSc2d1
výbuchu	výbuch	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Evakuováno	evakuovat	k5eAaBmNgNnS
bylo	být	k5eAaImAgNnS
430	#num#	k4
obyvatel	obyvatel	k1gMnPc2
okolních	okolní	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
vyjádření	vyjádření	k1gNnSc2
právního	právní	k2eAgMnSc4d1
zástupce	zástupce	k1gMnSc4
Imex	Imex	k1gInSc4
Group	Group	k1gInSc4
byla	být	k5eAaImAgFnS
v	v	k7c6
objektu	objekt	k1gInSc6
uskladněna	uskladnit	k5eAaPmNgFnS
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
munice	munice	k1gFnSc1
a	a	k8xC
samopaly	samopal	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sklad	sklad	k1gInSc1
podle	podle	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
názoru	názor	k1gInSc2
nemohl	moct	k5eNaImAgInS
samovolně	samovolně	k6eAd1
explodovat	explodovat	k5eAaBmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odklízení	odklízení	k1gNnSc1
následků	následek	k1gInPc2
</s>
<s>
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
explozi	exploze	k1gFnSc6
se	se	k3xPyFc4
z	z	k7c2
areálu	areál	k1gInSc2
stále	stále	k6eAd1
ozývaly	ozývat	k5eAaImAgInP
neřízené	řízený	k2eNgInPc1d1
výbuchy	výbuch	k1gInPc1
<g/>
,	,	kIx,
poslední	poslední	k2eAgInPc1d1
byl	být	k5eAaImAgInS
zaznamenán	zaznamenat	k5eAaPmNgInS
v	v	k7c6
polovině	polovina	k1gFnSc6
prosince	prosinec	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pyrotechnici	pyrotechnik	k1gMnPc1
se	se	k3xPyFc4
tak	tak	k9
do	do	k7c2
Vrbětic	Vrbětice	k1gFnPc2
vrátili	vrátit	k5eAaPmAgMnP
až	až	k6eAd1
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odvážení	odvážení	k1gNnSc3
munice	munice	k1gFnSc2
z	z	k7c2
areálu	areál	k1gInSc2
probíhalo	probíhat	k5eAaImAgNnS
od	od	k7c2
ledna	leden	k1gInSc2
do	do	k7c2
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
šlo	jít	k5eAaImAgNnS
o	o	k7c4
téměř	téměř	k6eAd1
550	#num#	k4
kamionů	kamion	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Munice	munice	k1gFnSc1
byla	být	k5eAaImAgFnS
dočasně	dočasně	k6eAd1
uložena	uložit	k5eAaPmNgFnS
do	do	k7c2
armádního	armádní	k2eAgInSc2d1
muničního	muniční	k2eAgInSc2d1
skladu	sklad	k1gInSc2
v	v	k7c4
Květné	květný	k2eAgNnSc4d1
na	na	k7c6
Svitavsku	Svitavsko	k1gNnSc6
<g/>
,	,	kIx,
opuštěného	opuštěný	k2eAgNnSc2d1
armádou	armáda	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
původně	původně	k6eAd1
měla	mít	k5eAaImAgFnS
zůstat	zůstat	k5eAaPmF
jen	jen	k9
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
<g/>
,	,	kIx,
poslední	poslední	k2eAgInPc1d1
kamiony	kamion	k1gInPc1
byly	být	k5eAaImAgInP
odtud	odtud	k6eAd1
nakonec	nakonec	k6eAd1
odvezeny	odvézt	k5eAaPmNgInP
až	až	k9
v	v	k7c6
srpnu	srpen	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2016	#num#	k4
pak	pak	k6eAd1
začalo	začít	k5eAaPmAgNnS
čištění	čištění	k1gNnSc1
areálu	areál	k1gInSc2
od	od	k7c2
nevybuchlé	vybuchlý	k2eNgFnSc2d1
munice	munice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
pyrotechnické	pyrotechnický	k2eAgFnSc2d1
práce	práce	k1gFnSc2
v	v	k7c6
areálu	areál	k1gInSc6
ještě	ještě	k6eAd1
stále	stále	k6eAd1
pokračovaly	pokračovat	k5eAaImAgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
pročištění	pročištění	k1gNnSc6
okolí	okolí	k1gNnSc2
prvního	první	k4xOgMnSc2
výbuchu	výbuch	k1gInSc2
k	k	k7c3
srpnu	srpen	k1gInSc3
2017	#num#	k4
policie	policie	k1gFnSc1
uvedla	uvést	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
nalezeno	naleznout	k5eAaPmNgNnS,k5eAaBmNgNnS
3775	#num#	k4
kusů	kus	k1gInPc2
munice	munice	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
264	#num#	k4
bylo	být	k5eAaImAgNnS
zlikvidováno	zlikvidovat	k5eAaPmNgNnS
na	na	k7c6
místě	místo	k1gNnSc6
v	v	k7c6
trhací	trhací	k2eAgFnSc6d1
jámě	jáma	k1gFnSc6
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
3326	#num#	k4
kusů	kus	k1gInPc2
provozuschopné	provozuschopný	k2eAgFnSc2d1
munice	munice	k1gFnSc2
bylo	být	k5eAaImAgNnS
odvezeno	odvézt	k5eAaPmNgNnS
k	k	k7c3
likvidaci	likvidace	k1gFnSc3
ve	v	k7c6
vojenském	vojenský	k2eAgInSc6d1
újezdu	újezd	k1gInSc6
Libavá	Libavý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Pět	pět	k4xCc4
let	léto	k1gNnPc2
po	po	k7c6
výbuchu	výbuch	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
říjnu	říjen	k1gInSc6
2019	#num#	k4
<g/>
,	,	kIx,
pyrotechnická	pyrotechnický	k2eAgFnSc1d1
služba	služba	k1gFnSc1
uváděla	uvádět	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
bylo	být	k5eAaImAgNnS
dosud	dosud	k6eAd1
nalezeno	naleznout	k5eAaPmNgNnS,k5eAaBmNgNnS
celkem	celek	k1gInSc7
6007	#num#	k4
kusů	kus	k1gInPc2
munice	munice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
K	k	k7c3
přelomu	přelom	k1gInSc3
let	léto	k1gNnPc2
2019	#num#	k4
a	a	k8xC
2020	#num#	k4
bylo	být	k5eAaImAgNnS
asi	asi	k9
80	#num#	k4
majitelům	majitel	k1gMnPc3
předáno	předat	k5eAaPmNgNnS
zpět	zpět	k6eAd1
k	k	k7c3
užívání	užívání	k1gNnSc3
celkem	celkem	k6eAd1
asi	asi	k9
952,5	952,5	k4
hektaru	hektar	k1gInSc2
pozemků	pozemek	k1gInPc2
zasažených	zasažený	k2eAgInPc2d1
explozemi	exploze	k1gFnPc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
dalších	další	k2eAgFnPc2d1
366,5	366,5	k4
hektaru	hektar	k1gInSc2
ještě	ještě	k6eAd1
zbývalo	zbývat	k5eAaImAgNnS
pročistit	pročistit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Bezpečnostní	bezpečnostní	k2eAgFnPc1d1
složky	složka	k1gFnPc1
ukončily	ukončit	k5eAaPmAgFnP
záchranné	záchranný	k2eAgFnPc1d1
práce	práce	k1gFnPc1
na	na	k7c4
podzim	podzim	k1gInSc4
2020	#num#	k4
a	a	k8xC
zpřístupnily	zpřístupnit	k5eAaPmAgFnP
dočasně	dočasně	k6eAd1
uzavřenou	uzavřený	k2eAgFnSc4d1
silnici	silnice	k1gFnSc4
mezi	mezi	k7c7
Haluzicemi	Haluzice	k1gFnPc7
a	a	k8xC
Lipovou	lipový	k2eAgFnSc7d1
<g/>
,	,	kIx,
likvidační	likvidační	k2eAgFnPc1d1
práce	práce	k1gFnPc1
však	však	k9
měly	mít	k5eAaImAgFnP
nadále	nadále	k6eAd1
pokračovat	pokračovat	k5eAaImF
<g/>
,	,	kIx,
včetně	včetně	k7c2
bourání	bourání	k1gNnSc2
dočasného	dočasný	k2eAgNnSc2d1
bezpečnostního	bezpečnostní	k2eAgNnSc2d1
oplocení	oplocení	k1gNnSc2
areálu	areál	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Náklady	náklad	k1gInPc1
na	na	k7c4
likvidaci	likvidace	k1gFnSc4
následků	následek	k1gInPc2
katastrofy	katastrofa	k1gFnSc2
se	se	k3xPyFc4
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2015	#num#	k4
přiblížily	přiblížit	k5eAaPmAgFnP
částce	částka	k1gFnSc6
350	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
,	,	kIx,
částka	částka	k1gFnSc1
potřebná	potřebný	k2eAgFnSc1d1
na	na	k7c4
následné	následný	k2eAgNnSc4d1
vyčištění	vyčištění	k1gNnSc4
areálu	areál	k1gInSc2
od	od	k7c2
nevybuchlé	vybuchlý	k2eNgFnSc2d1
munice	munice	k1gFnSc2
byla	být	k5eAaImAgFnS
odhadována	odhadovat	k5eAaImNgFnS
až	až	k9
na	na	k7c4
1	#num#	k4
miliardu	miliarda	k4xCgFnSc4
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odškodnění	odškodnění	k1gNnSc1
</s>
<s>
Vláda	vláda	k1gFnSc1
rozhodla	rozhodnout	k5eAaPmAgFnS
o	o	k7c6
plošném	plošný	k2eAgNnSc6d1
odškodnění	odškodnění	k1gNnSc6
obyvatel	obyvatel	k1gMnPc2
Vrbětic	Vrbětice	k1gFnPc2
jednorázovou	jednorázový	k2eAgFnSc7d1
částkou	částka	k1gFnSc7
3	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
si	se	k3xPyFc3
obyvatelé	obyvatel	k1gMnPc1
mohli	moct	k5eAaImAgMnP
standardním	standardní	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
podat	podat	k5eAaPmF
žádost	žádost	k1gFnSc4
o	o	k7c4
odškodnění	odškodnění	k1gNnSc4
škod	škoda	k1gFnPc2
na	na	k7c6
majetku	majetek	k1gInSc6
či	či	k8xC
ušlý	ušlý	k2eAgInSc4d1
zisk	zisk	k1gInSc4
<g/>
,	,	kIx,
ze	z	k7c2
136	#num#	k4
žádostí	žádost	k1gFnPc2
však	však	k9
k	k	k7c3
srpnu	srpen	k1gInSc3
2017	#num#	k4
byly	být	k5eAaImAgInP
krajským	krajský	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
či	či	k8xC
ministerstvem	ministerstvo	k1gNnSc7
vnitra	vnitro	k1gNnSc2
uznány	uznán	k2eAgFnPc4d1
jen	jen	k9
dvě	dva	k4xCgFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Krádeže	krádež	k1gFnPc1
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
odklízení	odklízení	k1gNnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
prosincem	prosinec	k1gInSc7
2016	#num#	k4
a	a	k8xC
jarem	jar	k1gInSc7
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
,	,	kIx,
nejméně	málo	k6eAd3
čtyřikrát	čtyřikrát	k6eAd1
vnikla	vniknout	k5eAaPmAgFnS
do	do	k7c2
uzavřené	uzavřený	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
dvojice	dvojice	k1gFnSc2
zlodějů	zloděj	k1gMnPc2
a	a	k8xC
kradla	krást	k5eAaImAgFnS
zde	zde	k6eAd1
samopaly	samopal	k1gInPc4
<g/>
,	,	kIx,
součásti	součást	k1gFnPc4
zbraní	zbraň	k1gFnPc2
a	a	k8xC
rozmetané	rozmetaný	k2eAgFnSc2d1
výbušniny	výbušnina	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
některé	některý	k3yIgFnPc1
ve	v	k7c6
skupině	skupina	k1gFnSc6
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
uváděli	uvádět	k5eAaImAgMnP
do	do	k7c2
provozu	provoz	k1gInSc2
a	a	k8xC
následně	následně	k6eAd1
přeprodávali	přeprodávat	k5eAaImAgMnP,k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soud	soud	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
uložil	uložit	k5eAaPmAgMnS
Milanu	Milan	k1gMnSc3
Chaloupkovi	Chaloupka	k1gMnSc3
tříletý	tříletý	k2eAgInSc4d1
trest	trest	k1gInSc4
odnětí	odnětí	k1gNnSc2
svobody	svoboda	k1gFnSc2
se	s	k7c7
čtyřletým	čtyřletý	k2eAgInSc7d1
podmíněným	podmíněný	k2eAgInSc7d1
odkladem	odklad	k1gInSc7
<g/>
,	,	kIx,
Zdeňku	Zdeněk	k1gMnSc3
Šuranskému	Šuranský	k2eAgMnSc3d1
trest	trest	k1gInSc1
34	#num#	k4
měsíců	měsíc	k1gInPc2
a	a	k8xC
dalším	další	k2eAgMnPc3d1
účastníkům	účastník	k1gMnPc3
skupiny	skupina	k1gFnSc2
podmíněné	podmíněný	k2eAgInPc4d1
tresty	trest	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Policie	policie	k1gFnSc1
rovněž	rovněž	k9
prověřovala	prověřovat	k5eAaImAgFnS
<g/>
,	,	kIx,
zda	zda	k8xS
krádeže	krádež	k1gFnPc1
nebyly	být	k5eNaImAgFnP
umožněny	umožnit	k5eAaPmNgFnP
nějakým	nějaký	k3yIgNnSc7
pochybením	pochybení	k1gNnSc7
z	z	k7c2
její	její	k3xOp3gFnSc2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
však	však	k9
nepotvrdilo	potvrdit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
areálech	areál	k1gInPc6
poté	poté	k6eAd1
byla	být	k5eAaImAgFnS
zvýšena	zvýšit	k5eAaPmNgFnS
ostraha	ostraha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Skladování	skladování	k1gNnSc1
min	mina	k1gFnPc2
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2015	#num#	k4
podal	podat	k5eAaPmAgMnS
státní	státní	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
Leo	Leo	k1gMnSc1
Foltýn	Foltýn	k1gMnSc1
obžalobu	obžaloba	k1gFnSc4
na	na	k7c4
pět	pět	k4xCc4
lidí	člověk	k1gMnPc2
a	a	k8xC
dvě	dva	k4xCgFnPc1
firmy	firma	k1gFnPc1
kvůli	kvůli	k7c3
skladování	skladování	k1gNnSc3
údajně	údajně	k6eAd1
zakázané	zakázaný	k2eAgFnSc2d1
munice	munice	k1gFnSc2
ve	v	k7c6
skladu	sklad	k1gInSc6
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
obžaloba	obžaloba	k1gFnSc1
nijak	nijak	k6eAd1
nesouvisela	souviset	k5eNaImAgFnS
s	s	k7c7
výbuchem	výbuch	k1gInSc7
<g/>
,	,	kIx,
miny	mina	k1gFnPc1
byly	být	k5eAaImAgFnP
ale	ale	k9
nalezeny	naleznout	k5eAaPmNgFnP,k5eAaBmNgFnP
během	během	k7c2
zvýšené	zvýšený	k2eAgFnSc2d1
inspekční	inspekční	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
policie	policie	k1gFnSc2
po	po	k7c6
výbuchu	výbuch	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obžaloba	obžaloba	k1gFnSc1
se	se	k3xPyFc4
týkala	týkat	k5eAaImAgFnS
firem	firma	k1gFnPc2
Excalibur	Excalibura	k1gFnPc2
Army	Arma	k1gFnSc2
a	a	k8xC
Real	Real	k1gInSc1
Trade	Trad	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
státního	státní	k2eAgMnSc2d1
zástupce	zástupce	k1gMnSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
objektu	objekt	k1gInSc6
skladováno	skladovat	k5eAaImNgNnS
asi	asi	k9
500	#num#	k4
protipěchotních	protipěchotní	k2eAgFnPc2d1
min	mina	k1gFnPc2
typu	typ	k1gInSc2
MON-100	MON-100	k1gFnSc1
a	a	k8xC
MON-	MON-	k1gFnSc1
<g/>
200	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
podle	podle	k7c2
něj	on	k3xPp3gNnSc2
zakázané	zakázaný	k2eAgInPc1d1
jak	jak	k6eAd1
právními	právní	k2eAgFnPc7d1
normami	norma	k1gFnPc7
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
mezinárodněprávními	mezinárodněprávní	k2eAgFnPc7d1
normami	norma	k1gFnPc7
vycházejícími	vycházející	k2eAgFnPc7d1
z	z	k7c2
Ottawské	ottawský	k2eAgFnSc2d1
konvence	konvence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mluvčí	mluvčí	k1gFnSc1
společnosti	společnost	k1gFnSc2
Excalibur	Excalibura	k1gFnPc2
Army	Arma	k1gFnSc2
Andrej	Andrej	k1gMnSc1
Čírtek	Čírtek	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgFnPc4
miny	mina	k1gFnPc4
nepovažovalo	považovat	k5eNaImAgNnS
za	za	k7c4
zakázané	zakázaný	k2eAgNnSc4d1
ani	ani	k8xC
Maďarsko	Maďarsko	k1gNnSc4
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
byly	být	k5eAaImAgFnP
dovezeny	dovézt	k5eAaPmNgFnP
<g/>
,	,	kIx,
ani	ani	k8xC
české	český	k2eAgInPc1d1
licenční	licenční	k2eAgInPc1d1
orgány	orgán	k1gInPc1
<g/>
,	,	kIx,
a	a	k8xC
před	před	k7c7
dovozem	dovoz	k1gInSc7
bylo	být	k5eAaImAgNnS
žádáno	žádat	k5eAaImNgNnS
o	o	k7c4
úřední	úřední	k2eAgNnSc4d1
povolení	povolení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
schváleno	schválit	k5eAaPmNgNnS
ministerstvy	ministerstvo	k1gNnPc7
ČR	ČR	kA
i	i	k8xC
Maďarska	Maďarsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
pozdějším	pozdní	k2eAgNnSc6d2
soudním	soudní	k2eAgNnSc6d1
řízení	řízení	k1gNnSc6
bylo	být	k5eAaImAgNnS
prokázáno	prokázat	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
trestný	trestný	k2eAgInSc1d1
čin	čin	k1gInSc1
nestal	stát	k5eNaPmAgInS
<g/>
,	,	kIx,
a	a	k8xC
všichni	všechen	k3xTgMnPc1
obžalovaní	obžalovaný	k1gMnPc1
byli	být	k5eAaImAgMnP
Krajským	krajský	k2eAgInSc7d1
soudem	soud	k1gInSc7
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
v	v	k7c6
červenci	červenec	k1gInSc6
2016	#num#	k4
osvobozeni	osvobodit	k5eAaPmNgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Firmy	firma	k1gFnPc1
totiž	totiž	k9
neskladovaly	skladovat	k5eNaImAgFnP
miny	mina	k1gFnPc1
jako	jako	k8xC,k8xS
takové	takový	k3xDgInPc1
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
pouze	pouze	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
jejich	jejich	k3xOp3gNnSc2
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Státní	státní	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
se	se	k3xPyFc4
nejprve	nejprve	k6eAd1
odvolal	odvolat	k5eAaPmAgInS
<g/>
,	,	kIx,
v	v	k7c6
prosinci	prosinec	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
ale	ale	k8xC
vzal	vzít	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
odvolání	odvolání	k1gNnSc4
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Soud	soud	k1gInSc1
též	též	k9
v	v	k7c6
lednu	leden	k1gInSc6
2018	#num#	k4
nařídil	nařídit	k5eAaPmAgMnS
vrátit	vrátit	k5eAaPmF
zabavené	zabavený	k2eAgFnPc4d1
miny	mina	k1gFnPc4
zpět	zpět	k6eAd1
firmě	firma	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1
</s>
<s>
Policie	policie	k1gFnSc1
exploze	exploze	k1gFnSc1
šetřila	šetřit	k5eAaImAgFnS
jako	jako	k9
úmyslné	úmyslný	k2eAgNnSc4d1
obecné	obecný	k2eAgNnSc4d1
ohrožení	ohrožení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Necelé	celý	k2eNgInPc4d1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
událostech	událost	k1gFnPc6
bylo	být	k5eAaImAgNnS
vyšetřování	vyšetřování	k1gNnSc1
stále	stále	k6eAd1
ve	v	k7c6
fázi	fáze	k1gFnSc6
prověřování	prověřování	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
konce	konec	k1gInSc2
června	červen	k1gInSc2
2017	#num#	k4
policie	policie	k1gFnSc1
prováděla	provádět	k5eAaImAgFnS
ohledání	ohledání	k1gNnSc2
prvního	první	k4xOgNnSc2
epicentra	epicentrum	k1gNnSc2
<g/>
,	,	kIx,
od	od	k7c2
července	červenec	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
pokračovala	pokračovat	k5eAaImAgFnS
na	na	k7c6
místě	místo	k1gNnSc6
druhého	druhý	k4xOgMnSc2
výbuchu	výbuch	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
vyšetřování	vyšetřování	k1gNnSc6
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
hejtman	hejtman	k1gMnSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
nevěděl	vědět	k5eNaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
muniční	muniční	k2eAgInSc4d1
sklad	sklad	k1gInSc4
už	už	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
neslouží	sloužit	k5eNaImIp3nP
armádě	armáda	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
ho	on	k3xPp3gMnSc4
pronajímá	pronajímat	k5eAaImIp3nS
soukromým	soukromý	k2eAgFnPc3d1
zbrojařským	zbrojařský	k2eAgFnPc3d1
firmám	firma	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
informaci	informace	k1gFnSc4
neměl	mít	k5eNaImAgInS
ani	ani	k9
Český	český	k2eAgInSc1d1
báňský	báňský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
měl	mít	k5eAaImAgInS
v	v	k7c6
muničním	muniční	k2eAgInSc6d1
skladu	sklad	k1gInSc6
provádět	provádět	k5eAaImF
kontroly	kontrola	k1gFnPc4
výbušnin	výbušnina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Kontroly	kontrola	k1gFnPc1
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
neprováděla	provádět	k5eNaImAgFnS
ani	ani	k8xC
Policie	policie	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
má	mít	k5eAaImIp3nS
podle	podle	k7c2
zákona	zákon	k1gInSc2
provádět	provádět	k5eAaImF
kontrolu	kontrola	k1gFnSc4
munice	munice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
skladu	sklad	k1gInSc6
byly	být	k5eAaImAgInP
porušeny	porušen	k2eAgInPc1d1
bezpečnostní	bezpečnostní	k2eAgInPc1d1
předpisy	předpis	k1gInPc1
<g/>
,	,	kIx,
protože	protože	k8xS
letecké	letecký	k2eAgFnSc2d1
pumy	puma	k1gFnSc2
a	a	k8xC
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
munice	munice	k1gFnSc1
byly	být	k5eAaImAgFnP
uloženy	uložit	k5eAaPmNgFnP
hned	hned	k6eAd1
vedle	vedle	k7c2
vysoce	vysoce	k6eAd1
vznětlivých	vznětlivý	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zajišťování	zajišťování	k1gNnSc1
vnější	vnější	k2eAgFnSc2d1
ostrahy	ostraha	k1gFnSc2
areálu	areál	k1gInSc2
bylo	být	k5eAaImAgNnS
povinností	povinnost	k1gFnSc7
státního	státní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
Vojenský	vojenský	k2eAgInSc1d1
technický	technický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
(	(	kIx(
<g/>
VTÚ	VTÚ	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
pod	pod	k7c4
Ministerstvo	ministerstvo	k1gNnSc4
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gMnSc1
ředitel	ředitel	k1gMnSc1
Václav	Václav	k1gMnSc1
Irovský	Irovský	k2eAgMnSc1d1
pochybení	pochybení	k1gNnSc2
odmítl	odmítnout	k5eAaPmAgMnS
a	a	k8xC
v	v	k7c6
prosinci	prosinec	k1gInSc6
roku	rok	k1gInSc2
2014	#num#	k4
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
plot	plot	k1gInSc4
kolem	kolem	k7c2
areálu	areál	k1gInSc2
byl	být	k5eAaImAgInS
opakovaně	opakovaně	k6eAd1
opravován	opravován	k2eAgInSc1d1
a	a	k8xC
„	„	k?
<g/>
Letos	letos	k6eAd1
se	se	k3xPyFc4
tam	tam	k6eAd1
nejela	jet	k5eNaImAgFnS
žádná	žádný	k3yNgFnSc1
rallye	rallye	k1gFnSc1
<g/>
,	,	kIx,
nechodili	chodit	k5eNaImAgMnP
tam	tam	k6eAd1
ani	ani	k8xC
myslivci	myslivec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
kamerových	kamerový	k2eAgInPc6d1
záznamech	záznam	k1gInPc6
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgInPc1
vstupy	vstup	k1gInPc1
a	a	k8xC
vjezdy	vjezd	k1gInPc1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Firma	firma	k1gFnSc1
Imex	Imex	k1gInSc1
Group	Group	k1gMnSc1
si	se	k3xPyFc3
v	v	k7c6
areálu	areál	k1gInSc6
kolem	kolem	k7c2
každého	každý	k3xTgInSc2
skladu	sklad	k1gInSc2
na	na	k7c4
vlastní	vlastní	k2eAgInPc4d1
náklady	náklad	k1gInPc4
postavila	postavit	k5eAaPmAgFnS
další	další	k2eAgNnSc4d1
oplocení	oplocení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Role	role	k1gFnSc1
ruské	ruský	k2eAgFnSc2d1
GRU	GRU	kA
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Kauza	kauza	k1gFnSc1
Vrbětice	Vrbětice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
mimořádné	mimořádný	k2eAgFnSc6d1
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
prohlásil	prohlásit	k5eAaPmAgMnS
premiér	premiér	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
podle	podle	k7c2
zjištění	zjištění	k1gNnSc2
Bezpečnostní	bezpečnostní	k2eAgFnSc2d1
informační	informační	k2eAgFnSc2d1
služby	služba	k1gFnSc2
(	(	kIx(
<g/>
BIS	BIS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Národní	národní	k2eAgFnSc2d1
centrály	centrála	k1gFnSc2
proti	proti	k7c3
organizovanému	organizovaný	k2eAgInSc3d1
zločinu	zločin	k1gInSc3
a	a	k8xC
policie	policie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
existuje	existovat	k5eAaImIp3nS
důvodné	důvodný	k2eAgNnSc4d1
podezření	podezření	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c7
výbuchem	výbuch	k1gInSc7
skladů	sklad	k1gInPc2
mohla	moct	k5eAaImAgFnS
<g />
.	.	kIx.
</s>
<s hack="1">
být	být	k5eAaImF
aktivita	aktivita	k1gFnSc1
ruské	ruský	k2eAgFnSc2d1
zpravodajské	zpravodajský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
GRU	GRU	kA
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
jednotky	jednotka	k1gFnPc1
29155	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
Jan	Jan	k1gMnSc1
Hamáček	Hamáček	k1gMnSc1
to	ten	k3xDgNnSc4
označil	označit	k5eAaPmAgMnS
za	za	k7c4
teroristický	teroristický	k2eAgInSc4d1
čin	čin	k1gInSc4
a	a	k8xC
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
něj	on	k3xPp3gMnSc4
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
vyhostí	vyhostit	k5eAaPmIp3nS
18	#num#	k4
pracovníků	pracovník	k1gMnPc2
ruského	ruský	k2eAgNnSc2d1
velvyslanectví	velvyslanectví	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
identifikovaných	identifikovaný	k2eAgFnPc2d1
českými	český	k2eAgFnPc7d1
zpravodajskými	zpravodajský	k2eAgFnPc7d1
službami	služba	k1gFnPc7
jako	jako	k8xC,k8xS
důstojníci	důstojník	k1gMnPc1
ruských	ruský	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
SVR	SVR	kA
a	a	k8xC
GRU	GRU	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Rudolf	Rudolf	k1gMnSc1
Jindrák	Jindrák	k1gMnSc1
později	pozdě	k6eAd2
upřesnil	upřesnit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
14	#num#	k4
diplomatů	diplomat	k1gMnPc2
a	a	k8xC
čtyři	čtyři	k4xCgInPc1
další	další	k2eAgInPc1d1
zaměstnance	zaměstnanec	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
souvislosti	souvislost	k1gFnSc6
česká	český	k2eAgFnSc1d1
policie	policie	k1gFnSc1
vyhlásila	vyhlásit	k5eAaPmAgFnS
pátrání	pátrání	k1gNnSc4
po	po	k7c6
dvou	dva	k4xCgInPc6
agentech	agens	k1gInPc6
GRU	GRU	kA
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
na	na	k7c6
českém	český	k2eAgNnSc6d1
území	území	k1gNnSc6
pohybovali	pohybovat	k5eAaImAgMnP
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
k	k	k7c3
výbuchům	výbuch	k1gInPc3
došlo	dojít	k5eAaPmAgNnS
–	–	k?
Alexandra	Alexandr	k1gMnSc4
Petrova	Petrov	k1gInSc2
(	(	kIx(
<g/>
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
Dr	dr	kA
<g/>
.	.	kIx.
Alexandr	Alexandr	k1gMnSc1
Jevgeňjevič	Jevgeňjevič	k1gMnSc1
Miškin	Miškin	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Ruslana	Ruslana	k1gFnSc1
Boširova	Boširov	k1gInSc2
(	(	kIx(
<g/>
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
plukovník	plukovník	k1gMnSc1
GRU	GRU	kA
Anatolij	Anatolij	k1gMnSc1
Vladimirovič	Vladimirovič	k1gMnSc1
Čepiga	Čepiga	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
policie	policie	k1gFnSc1
neměla	mít	k5eNaImAgFnS
přímý	přímý	k2eAgInSc4d1
důkaz	důkaz	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
areálu	areál	k1gInSc6
oba	dva	k4xCgMnPc1
muži	muž	k1gMnPc1
fyzicky	fyzicky	k6eAd1
vstoupili	vstoupit	k5eAaPmAgMnP
<g/>
,	,	kIx,
považovala	považovat	k5eAaImAgFnS
tuto	tento	k3xDgFnSc4
hypotézu	hypotéza	k1gFnSc4
za	za	k7c4
vysoce	vysoce	k6eAd1
reálnou	reálný	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
médií	médium	k1gNnPc2
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
jednat	jednat	k5eAaImF
o	o	k7c4
stejné	stejný	k2eAgFnPc4d1
osoby	osoba	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
vyšetřovatelé	vyšetřovatel	k1gMnPc1
v	v	k7c4
Anglii	Anglie	k1gFnSc4
označili	označit	k5eAaPmAgMnP
za	za	k7c4
možné	možný	k2eAgMnPc4d1
pachatele	pachatel	k1gMnPc4
otravy	otrava	k1gFnSc2
Sergeje	Sergej	k1gMnSc4
Skripala	Skripal	k1gMnSc4
v	v	k7c6
britském	britský	k2eAgInSc6d1
Salisbury	Salisbura	k1gFnSc2
roku	rok	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Policie	policie	k1gFnSc1
ověřila	ověřit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
oba	dva	k4xCgMnPc1
muži	muž	k1gMnPc1
přiletěli	přiletět	k5eAaPmAgMnP
do	do	k7c2
Česka	Česko	k1gNnSc2
13	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
pravidelnou	pravidelný	k2eAgFnSc7d1
linkou	linka	k1gFnSc7
Aeroflotu	aeroflot	k1gInSc2
z	z	k7c2
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
území	území	k1gNnSc6
opustili	opustit	k5eAaPmAgMnP
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
odjeli	odjet	k5eAaPmAgMnP
do	do	k7c2
Rakouska	Rakousko	k1gNnSc2
a	a	k8xC
odtud	odtud	k6eAd1
odletěli	odletět	k5eAaPmAgMnP
zpět	zpět	k6eAd1
do	do	k7c2
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
agenty	agent	k1gMnPc4
vyznamenal	vyznamenat	k5eAaPmAgInS
Vladimir	Vladimir	k1gInSc1
Putin	putin	k2eAgInSc1d1
na	na	k7c6
přelomu	přelom	k1gInSc6
let	léto	k1gNnPc2
2014	#num#	k4
a	a	k8xC
2015	#num#	k4
(	(	kIx(
<g/>
tedy	tedy	k8xC
po	po	k7c6
výbuchu	výbuch	k1gInSc6
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
<g/>
)	)	kIx)
vysokým	vysoký	k2eAgInSc7d1
řádem	řád	k1gInSc7
Hrdina	Hrdina	k1gMnSc1
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
vstupu	vstup	k1gInSc6
do	do	k7c2
Česka	Česko	k1gNnSc2
se	se	k3xPyFc4
oba	dva	k4xCgMnPc1
prokázali	prokázat	k5eAaPmAgMnP
krycími	krycí	k2eAgInPc7d1
pasy	pas	k1gInPc7
vystavenými	vystavený	k2eAgInPc7d1
rozvědkou	rozvědka	k1gFnSc7
GRU	GRU	kA
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yIgMnPc4,k3yRgMnPc4
později	pozdě	k6eAd2
použili	použít	k5eAaPmAgMnP
i	i	k9
během	během	k7c2
akce	akce	k1gFnSc2
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Cestovní	cestovní	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
byly	být	k5eAaImAgInP
vydány	vydat	k5eAaPmNgInP
na	na	k7c4
fiktivní	fiktivní	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
Ruslan	Ruslany	k1gInPc2
Boširov	Boširov	k1gInSc1
a	a	k8xC
Alexandr	Alexandr	k1gMnSc1
Petrov	Petrov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
žádosti	žádost	k1gFnSc6
o	o	k7c4
vstup	vstup	k1gInSc4
do	do	k7c2
vrbětického	vrbětický	k2eAgInSc2d1
skladu	sklad	k1gInSc2
se	se	k3xPyFc4
vykazovali	vykazovat	k5eAaImAgMnP
pasem	pas	k1gInSc7
Moldavska	Moldavsko	k1gNnSc2
na	na	k7c4
jméno	jméno	k1gNnSc4
Nicolai	Nicolai	k1gNnSc2
Popa	pop	k1gMnSc2
a	a	k8xC
pasem	pas	k1gInSc7
Tádžikistánu	Tádžikistán	k1gInSc2
na	na	k7c4
jméno	jméno	k1gNnSc4
Ruslan	Ruslany	k1gInPc2
Tabarov	Tabarovo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Pasy	pas	k1gInPc1
měly	mít	k5eAaImAgInP
být	být	k5eAaImF
zaslány	zaslat	k5eAaPmNgInP
státnímu	státní	k2eAgInSc3d1
podniku	podnik	k1gInSc3
Vojenský	vojenský	k2eAgInSc1d1
technický	technický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
zajišťujícímu	zajišťující	k2eAgMnSc3d1
vnější	vnější	k2eAgInSc4d1
ostrahu	ostraha	k1gFnSc4
areálu	areál	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
zpráv	zpráva	k1gFnPc2
médií	médium	k1gNnPc2
jim	on	k3xPp3gMnPc3
měl	mít	k5eAaImAgInS
provozovatel	provozovatel	k1gMnSc1
vydat	vydat	k5eAaPmF
povolení	povolení	k1gNnPc4
ke	k	k7c3
vstupu	vstup	k1gInSc3
do	do	k7c2
Vrbětic	Vrbětice	k1gFnPc2
na	na	k7c6
období	období	k1gNnSc6
od	od	k7c2
13	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
,	,	kIx,
k	k	k7c3
výbuchu	výbuch	k1gInSc3
v	v	k7c6
muničním	muniční	k2eAgInSc6d1
skladu	sklad	k1gInSc6
č.	č.	k?
16	#num#	k4
došlo	dojít	k5eAaPmAgNnS
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
v	v	k7c4
9.25	9.25	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radek	Radek	k1gMnSc1
Ondruš	Ondruš	k1gMnSc1
<g/>
,	,	kIx,
právní	právní	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
firmy	firma	k1gFnSc2
Imex	Imex	k1gInSc1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
nikdo	nikdo	k3yNnSc1
takový	takový	k3xDgInSc1
v	v	k7c6
téhle	tenhle	k3xDgFnSc6
době	doba	k1gFnSc6
do	do	k7c2
firmy	firma	k1gFnSc2
nepřijel	přijet	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
návštěvníky	návštěvník	k1gMnPc4
z	z	k7c2
jednoho	jeden	k4xCgInSc2
ze	z	k7c2
zahraničních	zahraniční	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
to	ten	k3xDgNnSc1
Rusko	Rusko	k1gNnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
vyřizovalo	vyřizovat	k5eAaImAgNnS
povolení	povolení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
pro	pro	k7c4
dva	dva	k4xCgMnPc4
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
ti	ten	k3xDgMnPc1
nakonec	nakonec	k6eAd1
nepřijeli	přijet	k5eNaPmAgMnP
nebo	nebo	k8xC
se	se	k3xPyFc4
neohlásili	ohlásit	k5eNaPmAgMnP
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
2014	#num#	k4
<g/>
,	,	kIx,
devět	devět	k4xCc4
měsíců	měsíc	k1gInPc2
před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
než	než	k8xS
vybuchl	vybuchnout	k5eAaPmAgInS
vrbětický	vrbětický	k2eAgInSc1d1
muniční	muniční	k2eAgInSc1d1
areál	areál	k1gInSc1
<g/>
,	,	kIx,
navštívil	navštívit	k5eAaPmAgInS
Miškin	Miškin	k1gInSc1
Česko	Česko	k1gNnSc1
spolu	spolu	k6eAd1
s	s	k7c7
velícím	velící	k2eAgMnSc7d1
důstojníkem	důstojník	k1gMnSc7
této	tento	k3xDgFnSc2
jednotky	jednotka	k1gFnSc2
GRU	GRU	kA
<g/>
,	,	kIx,
generálmajorem	generálmajor	k1gMnSc7
Denisem	Denis	k1gInSc7
Vjačeslavovičem	Vjačeslavovič	k1gMnSc7
Sergejevem	Sergejev	k1gInSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
vystupoval	vystupovat	k5eAaImAgMnS
pod	pod	k7c7
krycím	krycí	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Sergej	Sergej	k1gMnSc1
Fedotov	Fedotovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
důstojník	důstojník	k1gMnSc1
velel	velet	k5eAaImAgMnS
i	i	k9
akci	akce	k1gFnSc4
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
byl	být	k5eAaImAgMnS
otráven	otráven	k2eAgMnSc1d1
Sergej	Sergej	k1gMnSc1
Skripal	Skripal	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
cestoval	cestovat	k5eAaImAgMnS
do	do	k7c2
Sofie	Sofia	k1gFnSc2
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
podobným	podobný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
zabit	zabit	k2eAgMnSc1d1
bulharský	bulharský	k2eAgMnSc1d1
obchodník	obchodník	k1gMnSc1
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
Emilijan	Emilijana	k1gFnPc2
Gebrev	Gebrva	k1gFnPc2
(	(	kIx(
<g/>
Е	Е	k?
Г	Г	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shodou	shoda	k1gFnSc7
okolností	okolnost	k1gFnPc2
také	také	k6eAd1
v	v	k7c6
Bulharsku	Bulharsko	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
vybuchl	vybuchnout	k5eAaPmAgInS
sklad	sklad	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
uložen	uložit	k5eAaPmNgInS
vojenský	vojenský	k2eAgInSc1d1
materiál	materiál	k1gInSc1
tří	tři	k4xCgFnPc2
firem	firma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
nich	on	k3xPp3gMnPc2
byla	být	k5eAaImAgFnS
Imex	Imex	k1gInSc4
Group	Group	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Jedním	jeden	k4xCgInSc7
z	z	k7c2
klíčových	klíčový	k2eAgInPc2d1
důkazů	důkaz	k1gInPc2
je	být	k5eAaImIp3nS
e-mail	e-mail	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yRgMnSc7,k3yIgMnSc7,k3yQgMnSc7
o	o	k7c4
vstup	vstup	k1gInSc4
na	na	k7c4
inspekci	inspekce	k1gFnSc4
areálu	areál	k1gInSc2
pro	pro	k7c4
oba	dva	k4xCgInPc4
agenty	agens	k1gInPc4
pod	pod	k7c7
krycími	krycí	k2eAgNnPc7d1
jmény	jméno	k1gNnPc7
Popa	pop	k1gMnSc2
a	a	k8xC
Tabarov	Tabarov	k1gInSc4
žádal	žádat	k5eAaImAgMnS
jménem	jméno	k1gNnSc7
Národní	národní	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
Tádžikistánu	Tádžikistán	k1gInSc2
„	„	k?
<g/>
Andrej	Andrej	k1gMnSc1
O.	O.	kA
<g/>
“	“	k?
<g/>
,	,	kIx,
později	pozdě	k6eAd2
identifikovaný	identifikovaný	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
velitel	velitel	k1gMnSc1
jednotky	jednotka	k1gFnSc2
GRU	GRU	kA
29155	#num#	k4
Andrej	Andrej	k1gMnSc1
Averjanov	Averjanov	k1gInSc1
<g/>
,	,	kIx,
vystupující	vystupující	k2eAgFnSc1d1
pod	pod	k7c7
krycím	krycí	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Andrej	Andrej	k1gMnSc1
Overjanov	Overjanov	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jak	jak	k6eAd1
zjistilo	zjistit	k5eAaPmAgNnS
společné	společný	k2eAgNnSc1d1
pátrání	pátrání	k1gNnSc1
Bellingcat	Bellingcat	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Insider	insider	k1gMnSc1
a	a	k8xC
týdeníku	týdeník	k1gInSc3
Respekt	respekt	k1gInSc1
<g/>
,	,	kIx,
akce	akce	k1gFnSc1
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
celkem	celkem	k6eAd1
šest	šest	k4xCc1
agentů	agens	k1gInPc2
GRU	GRU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
zřejmě	zřejmě	k6eAd1
probíhala	probíhat	k5eAaImAgFnS
v	v	k7c6
září	září	k1gNnSc6
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	s	k7c7
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
setkali	setkat	k5eAaPmAgMnP
agenti	agent	k1gMnPc1
generálmajor	generálmajor	k1gMnSc1
Denis	Denisa	k1gFnPc2
Sergejev	Sergejev	k1gMnSc1
(	(	kIx(
<g/>
krycí	krycí	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
“	“	k?
<g/>
Sergej	Sergej	k1gMnSc1
Fedotov	Fedotovo	k1gNnPc2
<g/>
”	”	k?
<g/>
)	)	kIx)
a	a	k8xC
podplukovník	podplukovník	k1gMnSc1
Jegor	Jegor	k1gMnSc1
Gordienko	Gordienka	k1gFnSc5
(	(	kIx(
<g/>
krycí	krycí	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
„	„	k?
<g/>
Georgy	Georg	k1gInPc1
Gorškov	Gorškov	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
s	s	k7c7
velitelem	velitel	k1gMnSc7
Andrejem	Andrej	k1gMnSc7
Averjanovem	Averjanov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akce	akce	k1gFnSc1
byla	být	k5eAaImAgFnS
patrně	patrně	k6eAd1
plánována	plánovat	k5eAaImNgFnS
o	o	k7c4
týden	týden	k1gInSc4
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
ale	ale	k8xC
byla	být	k5eAaImAgFnS
odložena	odložit	k5eAaPmNgFnS
a	a	k8xC
Averjanov	Averjanov	k1gInSc1
se	se	k3xPyFc4
přes	přes	k7c4
Varšavu	Varšava	k1gFnSc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
se	se	k3xPyFc4
všichni	všechen	k3xTgMnPc1
agenti	agent	k1gMnPc1
sešli	sejít	k5eAaPmAgMnP
na	na	k7c6
velitelství	velitelství	k1gNnSc6
GRU	GRU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Andrej	Andrej	k1gMnSc1
Averjanov	Averjanov	k1gInSc4
odletěl	odletět	k5eAaPmAgMnS
13	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
setkal	setkat	k5eAaPmAgMnS
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
s	s	k7c7
jiným	jiný	k2eAgMnSc7d1
agentem	agent	k1gMnSc7
<g/>
,	,	kIx,
podplukovníkem	podplukovník	k1gMnSc7
Nikolajem	Nikolaj	k1gMnSc7
Ježovem	Ježov	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
tam	tam	k6eAd1
přiletěl	přiletět	k5eAaPmAgInS
už	už	k6eAd1
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
stejnou	stejný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
)	)	kIx)
dorazili	dorazit	k5eAaPmAgMnP
z	z	k7c2
Moskvy	Moskva	k1gFnSc2
do	do	k7c2
Prahy	Praha	k1gFnSc2
Miškin	Miškin	k1gInSc1
a	a	k8xC
Čepiga	Čepiga	k1gFnSc1
pod	pod	k7c7
krycími	krycí	k2eAgNnPc7d1
jmény	jméno	k1gNnPc7
Petrov	Petrov	k1gInSc1
a	a	k8xC
Boshirov	Boshirov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Prahy	Praha	k1gFnSc2
se	s	k7c7
13	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
přemístili	přemístit	k5eAaPmAgMnP
do	do	k7c2
ostravského	ostravský	k2eAgInSc2d1
hotelu	hotel	k1gInSc2
Corrado	Corrada	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgMnPc1d1
dva	dva	k4xCgMnPc1
agenti	agent	k1gMnPc1
cestovali	cestovat	k5eAaImAgMnP
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
pod	pod	k7c7
diplomatickým	diplomatický	k2eAgNnSc7d1
krytím	krytí	k1gNnSc7
a	a	k8xC
svými	svůj	k3xOyFgNnPc7
skutečnými	skutečný	k2eAgNnPc7d1
jmény	jméno	k1gNnPc7
Alexej	Alexej	k1gMnSc1
Kapinos	Kapinos	k1gMnSc1
a	a	k8xC
Jevgenij	Jevgenij	k1gMnSc1
Kalinin	Kalinin	k2eAgMnSc1d1
do	do	k7c2
Budapešti	Budapešť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všech	všecek	k3xTgMnPc2
šest	šest	k4xCc1
agentů	agent	k1gMnPc2
mělo	mít	k5eAaImAgNnS
zamluvené	zamluvený	k2eAgNnSc4d1
zpáteční	zpáteční	k2eAgNnSc4d1
lety	léto	k1gNnPc7
na	na	k7c4
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Averjanov	Averjanov	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
často	často	k6eAd1
telefonicky	telefonicky	k6eAd1
komunikuje	komunikovat	k5eAaImIp3nS
s	s	k7c7
velitelem	velitel	k1gMnSc7
GRU	GRU	kA
i	i	k8xC
ministrem	ministr	k1gMnSc7
zahraničí	zahraničí	k1gNnSc2
Lavrovem	Lavrovo	k1gNnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
podobné	podobný	k2eAgFnPc1d1
akce	akce	k1gFnPc1
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
osobně	osobně	k6eAd1
zúčastnil	zúčastnit	k5eAaPmAgInS
pouze	pouze	k6eAd1
dvakrát	dvakrát	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
vyšetřovatelů	vyšetřovatel	k1gMnPc2
agenti	agent	k1gMnPc1
GRU	GRU	kA
muniční	muniční	k2eAgInSc4d1
sklad	sklad	k1gInSc4
pravděpodobně	pravděpodobně	k6eAd1
navštívili	navštívit	k5eAaPmAgMnP
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdroj	zdroj	k1gInSc1
blízký	blízký	k2eAgInSc1d1
vyšetřovatelům	vyšetřovatel	k1gMnPc3
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
nastražené	nastražený	k2eAgFnSc2d1
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
zákazníky	zákazník	k1gMnPc4
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
bouchlo	bouchnout	k5eAaPmAgNnS
to	ten	k3xDgNnSc1
asi	asi	k9
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chtěli	chtít	k5eAaImAgMnP
nejspíš	nejspíš	k9
zabít	zabít	k5eAaPmF
někoho	někdo	k3yInSc4
<g/>
,	,	kIx,
komu	kdo	k3yRnSc3,k3yQnSc3,k3yInSc3
byla	být	k5eAaImAgFnS
určena	určen	k2eAgFnSc1d1
dodávka	dodávka	k1gFnSc1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
serveru	server	k1gInSc2
iROZHLAS	iROZHLAS	k?
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
cílem	cíl	k1gInSc7
útoku	útok	k1gInSc2
bulharský	bulharský	k2eAgMnSc1d1
obchodník	obchodník	k1gMnSc1
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
Emilijan	Emilijana	k1gFnPc2
Gebrev	Gebrva	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
kterého	který	k3yQgNnSc2,k3yIgNnSc2,k3yRgNnSc2
se	se	k3xPyFc4
stejné	stejný	k2eAgNnSc1d1
komando	komando	k1gNnSc1
pokusilo	pokusit	k5eAaPmAgNnS
otrávit	otrávit	k5eAaPmF
neznámým	známý	k2eNgInSc7d1
jedem	jed	k1gInSc7
<g />
.	.	kIx.
</s>
<s hack="1">
roku	rok	k1gInSc2
2015	#num#	k4
v	v	k7c6
Sofii	Sofia	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Naopak	naopak	k6eAd1
bývalý	bývalý	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
Bohuslav	Bohuslav	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
přišel	přijít	k5eAaPmAgMnS
s	s	k7c7
hypotézou	hypotéza	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
Rusko	Rusko	k1gNnSc1
se	se	k3xPyFc4
snažilo	snažit	k5eAaImAgNnS
destabilizovat	destabilizovat	k5eAaBmF
jeho	jeho	k3xOp3gFnSc4
vládu	vláda	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
nebylo	být	k5eNaImAgNnS
spokojeno	spokojen	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
dostala	dostat	k5eAaPmAgFnS
k	k	k7c3
moci	moc	k1gFnSc3
koalice	koalice	k1gFnSc2
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
KDU-ČSL	KDU-ČSL	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
redaktorů	redaktor	k1gMnPc2
Respektu	respekt	k1gInSc2
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
důvodem	důvod	k1gInSc7
pro	pro	k7c4
akci	akce	k1gFnSc4
GRU	GRU	kA
snaha	snaha	k1gFnSc1
zabránit	zabránit	k5eAaPmF
dodávce	dodávka	k1gFnSc3
zbraní	zbraň	k1gFnPc2
pro	pro	k7c4
ukrajinskou	ukrajinský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
během	během	k7c2
války	válka	k1gFnSc2
na	na	k7c6
východní	východní	k2eAgFnSc6d1
Ukrajině	Ukrajina	k1gFnSc6
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
zabránit	zabránit	k5eAaPmF
dodávce	dodávka	k1gFnSc3
zbraní	zbraň	k1gFnPc2
povstalcům	povstalec	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
s	s	k7c7
podporou	podpora	k1gFnSc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
syrské	syrský	k2eAgFnSc6d1
občanské	občanský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
bojovali	bojovat	k5eAaImAgMnP
proti	proti	k7c3
vládě	vláda	k1gFnSc3
prezidenta	prezident	k1gMnSc2
Bašára	Bašár	k1gMnSc2
Asada	Asad	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
policie	policie	k1gFnSc2
je	být	k5eAaImIp3nS
nejpravděpodobnější	pravděpodobný	k2eAgNnSc1d3
<g/>
,	,	kIx,
že	že	k8xS
zbraně	zbraň	k1gFnPc1
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
poslány	poslat	k5eAaPmNgInP
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Společnost	společnost	k1gFnSc1
Imex	Imex	k1gInSc1
Group	Group	k1gInSc1
uvedla	uvést	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
neměla	mít	k5eNaImAgFnS
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
její	její	k3xOp3gInSc1
vojenský	vojenský	k2eAgInSc1d1
materiál	materiál	k1gInSc1
měl	mít	k5eAaImAgInS
putovat	putovat	k5eAaImF
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
nebo	nebo	k8xC
na	na	k7c4
Ukrajinu	Ukrajina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
právníka	právník	k1gMnSc2
firmy	firma	k1gFnSc2
se	se	k3xPyFc4
s	s	k7c7
agenty	agens	k1gInPc7
na	na	k7c6
fotografii	fotografia	k1gFnSc6
nikdo	nikdo	k3yNnSc1
z	z	k7c2
vedení	vedení	k1gNnSc2
firmy	firma	k1gFnSc2
nesetkal	setkat	k5eNaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Zbraně	zbraň	k1gFnPc1
ze	z	k7c2
skladu	sklad	k1gInSc2
Imexu	Imex	k1gInSc2
byly	být	k5eAaImAgFnP
určeny	určit	k5eAaPmNgInP
pro	pro	k7c4
bulharského	bulharský	k2eAgMnSc4d1
obchodníka	obchodník	k1gMnSc4
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
Emilijana	Emilijan	k1gMnSc2
Gebreva	Gebrev	k1gMnSc2
a	a	k8xC
podle	podle	k7c2
ministra	ministr	k1gMnSc2
Hamáčka	Hamáček	k1gMnSc2
měla	mít	k5eAaImAgFnS
nejspíš	nejspíš	k9
munice	munice	k1gFnSc1
explodovat	explodovat	k5eAaBmF
až	až	k9
později	pozdě	k6eAd2
na	na	k7c6
území	území	k1gNnSc6
Bulharska	Bulharsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
zbrojní	zbrojní	k2eAgInSc1d1
materiál	materiál	k1gInSc1
znehodnocen	znehodnocen	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
agenti	agent	k1gMnPc1
GRU	GRU	kA
měli	mít	k5eAaImAgMnP
v	v	k7c6
areálu	areál	k1gInSc6
českého	český	k2eAgMnSc2d1
komplice	komplic	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
zjistili	zjistit	k5eAaPmAgMnP
novináři	novinář	k1gMnPc7
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
zabitých	zabitý	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
Imex	Imex	k1gInSc4
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
kapitán	kapitán	k1gMnSc1
Luděk	Luděk	k1gMnSc1
Petřík	Petřík	k1gMnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
do	do	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
příslušníkem	příslušník	k1gMnSc7
I.	I.	kA
odboru	odbor	k1gInSc3
Krajské	krajský	k2eAgFnSc2d1
správy	správa	k1gFnSc2
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Souvislosti	souvislost	k1gFnPc1
s	s	k7c7
Bulharskem	Bulharsko	k1gNnSc7
</s>
<s>
Ruští	ruský	k2eAgMnPc1d1
agenti	agent	k1gMnPc1
byli	být	k5eAaImAgMnP
přítomni	přítomen	k2eAgMnPc1d1
také	také	k9
v	v	k7c6
Bulharsku	Bulharsko	k1gNnSc6
v	v	k7c6
letech	léto	k1gNnPc6
2014	#num#	k4
a	a	k8xC
2015	#num#	k4
v	v	k7c6
době	doba	k1gFnSc6
výbuchů	výbuch	k1gInPc2
v	v	k7c6
šesti	šest	k4xCc6
tamních	tamní	k2eAgInPc6d1
zbrojních	zbrojní	k2eAgInPc6d1
skladech	sklad	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
odehrály	odehrát	k5eAaPmAgInP
podle	podle	k7c2
stejného	stejný	k2eAgInSc2d1
vzoru	vzor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2014	#num#	k4
až	až	k9
2015	#num#	k4
cestovali	cestovat	k5eAaImAgMnP
do	do	k7c2
Bulharska	Bulharsko	k1gNnSc2
agenti	agent	k1gMnPc1
GRU	GRU	kA
Vladimir	Vladimir	k1gMnSc1
Mojsejev	Mojsejev	k1gMnSc1
(	(	kIx(
<g/>
alias	alias	k9
Vladimir	Vladimir	k1gMnSc1
Popov	Popov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Denis	Denisa	k1gFnPc2
Sergejev	Sergejev	k1gFnPc2
(	(	kIx(
<g/>
alias	alias	k9
Sergej	Sergej	k1gMnSc1
Fedotov	Fedotov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sergej	Sergej	k1gMnSc1
Ljutenkov	Ljutenkov	k1gInSc1
(	(	kIx(
<g/>
alias	alias	k9
Sergej	Sergej	k1gMnSc1
Pavlov	Pavlov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nikolaj	Nikolaj	k1gMnSc1
Ježov	Ježov	k1gInSc1
(	(	kIx(
<g/>
alias	alias	k9
Nikolaj	Nikolaj	k1gMnSc1
Kononichin	Kononichin	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Terentěv	Terentěv	k1gMnSc1
(	(	kIx(
<g/>
alis	alis	k1gInSc1
Ivan	Ivan	k1gMnSc1
Lebeděv	Lebeděv	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Alexej	Alexej	k1gMnSc1
Kalinin	Kalinin	k2eAgMnSc1d1
(	(	kIx(
<g/>
alias	alias	k9
Alexej	Alexej	k1gMnSc1
Nikitin	Nikitin	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jegor	Jegor	k1gMnSc1
Gordienko	Gordienka	k1gFnSc5
(	(	kIx(
<g/>
alias	alias	k9
Georgij	Georgij	k1gMnSc1
Gorškov	Gorškov	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
explozi	exploze	k1gFnSc6
dílny	dílna	k1gFnSc2
na	na	k7c4
střelný	střelný	k2eAgInSc4d1
prach	prach	k1gInSc4
v	v	k7c6
Kazanlaku	Kazanlak	k1gInSc6
dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2014	#num#	k4
zahynul	zahynout	k5eAaPmAgMnS
jeden	jeden	k4xCgMnSc1
člověk	člověk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vojenské	vojenský	k2eAgFnSc6d1
továrně	továrna	k1gFnSc6
TEREM-Tsar	TEREM-Tsar	k1gMnSc1
Samuil	Samuil	k1gMnSc1
v	v	k7c6
Kostenci	Kostence	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vybuchla	vybuchnout	k5eAaPmAgFnS
8	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2014	#num#	k4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
zraněno	zranit	k5eAaPmNgNnS
10	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vojenském	vojenský	k2eAgInSc6d1
skladu	sklad	k1gInSc6
Gorni	Goreň	k1gFnSc3
Lom	lom	k1gInSc4
továrny	továrna	k1gFnSc2
Midzhur	Midzhura	k1gFnPc2
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
při	při	k7c6
výbuchu	výbuch	k1gInSc6
zahynulo	zahynout	k5eAaPmAgNnS
15	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgInSc4
týden	týden	k1gInSc4
v	v	k7c6
prosinci	prosinec	k1gInSc6
2014	#num#	k4
při	při	k7c6
výbuchu	výbuch	k1gInSc6
soukromé	soukromý	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
v	v	k7c4
Maglizh	Maglizh	k1gInSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vyráběla	vyrábět	k5eAaImAgFnS
munici	munice	k1gFnSc4
a	a	k8xC
střelivo	střelivo	k1gNnSc4
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
jeden	jeden	k4xCgMnSc1
člověk	člověk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mojsejev	Mojsejev	k1gMnSc1
cestoval	cestovat	k5eAaImAgMnS
do	do	k7c2
Bulharska	Bulharsko	k1gNnSc2
roku	rok	k1gInSc2
2014	#num#	k4
celkem	celkem	k6eAd1
čtyřikrát	čtyřikrát	k6eAd1
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
tam	tam	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
výbuchům	výbuch	k1gInPc3
skladů	sklad	k1gInPc2
s	s	k7c7
municí	munice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
2016	#num#	k4
zapleten	zaplést	k5eAaPmNgInS
do	do	k7c2
neúspěšného	úspěšný	k2eNgInSc2d1
pokusu	pokus	k1gInSc2
o	o	k7c4
státní	státní	k2eAgInSc4d1
převrat	převrat	k1gInSc4
v	v	k7c6
Černé	Černé	k2eAgFnSc6d1
Hoře	hora	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2015	#num#	k4
se	se	k3xPyFc4
ruští	ruský	k2eAgMnPc1d1
agenti	agent	k1gMnPc1
GRU	GRU	kA
Sergejev	Sergejev	k1gMnSc1
a	a	k8xC
Gordienko	Gordienka	k1gFnSc5
pokusili	pokusit	k5eAaPmAgMnP
otrávit	otrávit	k5eAaPmF
obchodníka	obchodník	k1gMnSc4
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
Emilijana	Emilijan	k1gMnSc2
Gebreva	Gebrev	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Souvislosti	souvislost	k1gFnPc1
s	s	k7c7
Tádžikistánem	Tádžikistán	k1gInSc7
</s>
<s>
Týden	týden	k1gInSc1
před	před	k7c7
výbuchem	výbuch	k1gInSc7
muničního	muniční	k2eAgInSc2d1
skladu	sklad	k1gInSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
dostal	dostat	k5eAaPmAgMnS
majitel	majitel	k1gMnSc1
firmy	firma	k1gFnSc2
Imex	Imex	k1gInSc1
Petr	Petr	k1gMnSc1
Bernatík	Bernatík	k1gMnSc1
starší	starý	k2eAgInSc4d2
e-mail	e-mail	k1gInSc4
od	od	k7c2
generála	generál	k1gMnSc2
tádžických	tádžický	k2eAgNnPc2d1
pohraničních	pohraniční	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
Radschabali	Radschabali	k1gMnSc2
Rachmonaliho	Rachmonali	k1gMnSc2
s	s	k7c7
žádostí	žádost	k1gFnSc7
aby	aby	kYmCp3nS
pustil	pustit	k5eAaPmAgMnS
do	do	k7c2
areálu	areál	k1gInSc2
dva	dva	k4xCgInPc1
zbrojní	zbrojní	k2eAgInPc1d1
inspektory	inspektor	k1gMnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
pasy	pas	k1gInPc1
dojdou	dojít	k5eAaPmIp3nP
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
e-mail	e-mail	k1gInSc1
s	s	k7c7
dokumenty	dokument	k1gInPc7
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
pro	pro	k7c4
něž	jenž	k3xRgNnSc4
generál	generál	k1gMnSc1
Rachmonali	Rachmonali	k1gMnSc1
pomoc	pomoc	k1gFnSc4
žádal	žádat	k5eAaImAgMnS
<g/>
,	,	kIx,
došel	dojít	k5eAaPmAgInS
řediteli	ředitel	k1gMnSc3
Imexu	Imex	k1gInSc2
Petru	Petr	k1gMnSc3
Bernatíkovi	Bernatík	k1gMnSc3
mladšímu	mladý	k2eAgMnSc3d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
"	"	kIx"
<g/>
inspektorů	inspektor	k1gMnPc2
<g/>
"	"	kIx"
byl	být	k5eAaImAgInS
později	pozdě	k6eAd2
identifikován	identifikován	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
agent	agent	k1gMnSc1
GRU	GRU	kA
Anatolij	Anatolij	k1gMnSc4
Čepiga	Čepig	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
tehdy	tehdy	k6eAd1
používal	používat	k5eAaImAgMnS
tádžický	tádžický	k2eAgInSc4d1
pas	pas	k1gInSc4
na	na	k7c4
jméno	jméno	k1gNnSc4
Ruslan	Ruslany	k1gInPc2
Tabarov	Tabarovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bernatíkovi	Bernatík	k1gMnSc6
zaslal	zaslat	k5eAaPmAgMnS
ještě	ještě	k9
třetí	třetí	k4xOgInSc4
e-mail	e-mail	k1gInSc4
ruský	ruský	k2eAgMnSc1d1
obchodník	obchodník	k1gMnSc1
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
s	s	k7c7
českým	český	k2eAgInSc7d1
pasem	pas	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
upřesňoval	upřesňovat	k5eAaImAgInS
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
oba	dva	k4xCgMnPc1
"	"	kIx"
<g/>
inspektoři	inspektor	k1gMnPc1
<g/>
"	"	kIx"
do	do	k7c2
Vrbětic	Vrbětice	k1gFnPc2
dorazí	dorazit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
GRU	GRU	kA
byla	být	k5eAaImAgFnS
zapojena	zapojit	k5eAaPmNgFnS
do	do	k7c2
ozbrojeného	ozbrojený	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
Tádžikistánu	Tádžikistán	k1gInSc2
s	s	k7c7
uzbeckými	uzbecký	k2eAgMnPc7d1
islamisty	islamista	k1gMnPc7
v	v	k7c6
letech	léto	k1gNnPc6
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruští	ruský	k2eAgMnPc1d1
agenti	agent	k1gMnPc1
GRU	GRU	kA
ze	z	k7c2
speciální	speciální	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
29155	#num#	k4
<g/>
,	,	kIx,
jmenovitě	jmenovitě	k6eAd1
Fedotov	Fedotov	k1gInSc1
<g/>
,	,	kIx,
Boširov	Boširov	k1gInSc1
<g/>
,	,	kIx,
Gorškov	Gorškov	k1gInSc1
<g/>
,	,	kIx,
Lebeděv	Lebeděv	k1gFnSc1
a	a	k8xC
Nikitin	Nikitin	k1gInSc1
<g/>
,	,	kIx,
cestují	cestovat	k5eAaImIp3nP
do	do	k7c2
Tádžikistánu	Tádžikistán	k1gInSc2
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
často	často	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fedotov	Fedotov	k1gInSc4
a	a	k8xC
Gorškov	Gorškov	k1gInSc4
byli	být	k5eAaImAgMnP
i	i	k8xC
členy	člen	k1gMnPc7
podpůrného	podpůrný	k2eAgInSc2d1
týmu	tým	k1gInSc2
agentů	agent	k1gMnPc2
Čepigy	Čepiga	k1gFnSc2
a	a	k8xC
Miškina	Miškino	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
české	český	k2eAgFnPc1d1
tajné	tajný	k2eAgFnPc1d1
služby	služba	k1gFnPc1
obviňují	obviňovat	k5eAaImIp3nP
za	za	k7c7
výbuchy	výbuch	k1gInPc7
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Majitel	majitel	k1gMnSc1
firmy	firma	k1gFnSc2
Imex	Imex	k1gInSc1
Petr	Petr	k1gMnSc1
Bernatík	Bernatík	k1gMnSc1
starší	starší	k1gMnSc1
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
cesty	cesta	k1gFnSc2
prezidenta	prezident	k1gMnSc2
Miloše	Miloš	k1gMnSc2
Zemana	Zeman	k1gMnSc2
do	do	k7c2
Kazachstánu	Kazachstán	k1gInSc2
a	a	k8xC
Tádžikistánu	Tádžikistán	k1gInSc2
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
krátce	krátce	k6eAd1
po	po	k7c6
první	první	k4xOgFnSc6
explozi	exploze	k1gFnSc6
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
v	v	k7c6
listopadu	listopad	k1gInSc6
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sešel	sejít	k5eAaPmAgMnS
se	se	k3xPyFc4
tam	tam	k6eAd1
s	s	k7c7
generálem	generál	k1gMnSc7
Radschabali	Radschabali	k1gMnSc7
Rachmonalim	Rachmonalim	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
odmítá	odmítat	k5eAaImIp3nS
sdělit	sdělit	k5eAaPmF
o	o	k7c6
čem	co	k3yInSc6,k3yQnSc6,k3yRnSc6
jednali	jednat	k5eAaImAgMnP
ani	ani	k9
potvrdit	potvrdit	k5eAaPmF
<g/>
,	,	kIx,
zda	zda	k8xS
se	se	k3xPyFc4
schůzky	schůzka	k1gFnSc2
zúčastnil	zúčastnit	k5eAaPmAgMnS
i	i	k9
Miloš	Miloš	k1gMnSc1
Zeman	Zeman	k1gMnSc1
nebo	nebo	k8xC
někdo	někdo	k3yInSc1
z	z	k7c2
prezidentské	prezidentský	k2eAgFnSc2d1
kanceláře	kancelář	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šest	šest	k4xCc1
dní	den	k1gInPc2
po	po	k7c6
návratu	návrat	k1gInSc6
prezidentského	prezidentský	k2eAgInSc2d1
speciálu	speciál	k1gInSc2
následoval	následovat	k5eAaImAgInS
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
další	další	k2eAgInSc4d1
výbuch	výbuch	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
</s>
<s>
Velvyslanectví	velvyslanectví	k1gNnSc1
USA	USA	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
prohlásilo	prohlásit	k5eAaPmAgNnS
že	že	k9
„	„	k?
<g/>
pevně	pevně	k6eAd1
stojí	stát	k5eAaImIp3nS
za	za	k7c7
svým	svůj	k3xOyFgMnSc7
spolehlivým	spolehlivý	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
Českou	český	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
a	a	k8xC
oceňuje	oceňovat	k5eAaImIp3nS
její	její	k3xOp3gFnPc4
sankce	sankce	k1gFnPc4
za	za	k7c4
nebezpečný	bezpečný	k2eNgInSc4d1
útok	útok	k1gInSc4
na	na	k7c6
jejím	její	k3xOp3gNnSc6
území	území	k1gNnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
plně	plně	k6eAd1
podpořil	podpořit	k5eAaPmAgMnS
také	také	k9
ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
Dominic	Dominice	k1gFnPc2
Raab	Raab	k1gMnSc1
a	a	k8xC
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
že	že	k8xS
tato	tento	k3xDgFnSc1
událost	událost	k1gFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
předchozímu	předchozí	k2eAgNnSc3d1
chování	chování	k1gNnSc3
agentů	agens	k1gInPc2
tajných	tajný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
jaké	jaký	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
bylo	být	k5eAaImAgNnS
odhaleno	odhalen	k2eAgNnSc1d1
např.	např.	kA
v	v	k7c4
Salisbury	Salisbura	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Slovenská	slovenský	k2eAgFnSc1d1
prezidentka	prezidentka	k1gFnSc1
Zuzana	Zuzana	k1gFnSc1
Čaputová	Čaputový	k2eAgFnSc1d1
na	na	k7c6
svém	svůj	k3xOyFgInSc6
twitterovém	twitterový	k2eAgInSc6d1
účtu	účet	k1gInSc6
uvedla	uvést	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
Slovensko	Slovensko	k1gNnSc1
stojí	stát	k5eAaImIp3nS
při	při	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
Podobně	podobně	k6eAd1
se	s	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
vyjádřil	vyjádřit	k5eAaPmAgMnS
také	také	k9
slovenský	slovenský	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
Ivan	Ivan	k1gMnSc1
Korčok	Korčok	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c7
dalšími	další	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jako	jako	k9
první	první	k4xOgFnPc1
vyjádřily	vyjádřit	k5eAaPmAgFnP
podporu	podpora	k1gFnSc4
České	český	k2eAgFnSc3d1
republice	republika	k1gFnSc3
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
také	také	k9
Lotyšsko	Lotyšsko	k1gNnSc1
<g/>
,	,	kIx,
Ukrajina	Ukrajina	k1gFnSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
a	a	k8xC
Polsko	Polsko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Předseda	předseda	k1gMnSc1
zahraničního	zahraniční	k2eAgInSc2d1
výboru	výbor	k1gInSc2
britské	britský	k2eAgFnSc2d1
Dolní	dolní	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Tom	Tom	k1gMnSc1
Tugendhat	Tugendhat	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
výbuchy	výbuch	k1gInPc1
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
jsou	být	k5eAaImIp3nP
vraždou	vražda	k1gFnSc7
dvou	dva	k4xCgMnPc2
českých	český	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
a	a	k8xC
přímým	přímý	k2eAgInSc7d1
útokem	útok	k1gInSc7
na	na	k7c4
členskou	členský	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
NATO	NATO	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudil	soudil	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
okamžitě	okamžitě	k6eAd1
snížit	snížit	k5eAaPmF
úroveň	úroveň	k1gFnSc4
vztahů	vztah	k1gInPc2
s	s	k7c7
Ruskou	ruský	k2eAgFnSc7d1
federací	federace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
něj	on	k3xPp3gInSc2
by	by	kYmCp3nP
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
Severoatlantické	severoatlantický	k2eAgFnSc2d1
aliance	aliance	k1gFnSc2
měly	mít	k5eAaImAgFnP
vyhostit	vyhostit	k5eAaPmF
ruské	ruský	k2eAgMnPc4d1
velvyslance	velvyslanec	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předsedové	předseda	k1gMnPc1
tří	tři	k4xCgFnPc2
hlavních	hlavní	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
vyjádřili	vyjádřit	k5eAaPmAgMnP
plnou	plný	k2eAgFnSc4d1
solidaritu	solidarita	k1gFnSc4
Česku	Česko	k1gNnSc3
a	a	k8xC
vyzvali	vyzvat	k5eAaPmAgMnP
unijní	unijní	k2eAgFnPc4d1
země	zem	k1gFnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
„	„	k?
<g/>
také	také	k9
projevily	projevit	k5eAaPmAgFnP
solidaritu	solidarita	k1gFnSc4
přijetím	přijetí	k1gNnSc7
silné	silný	k2eAgFnSc2d1
<g/>
,	,	kIx,
společné	společný	k2eAgFnSc2d1
a	a	k8xC
konkrétní	konkrétní	k2eAgFnSc2d1
akce	akce	k1gFnSc2
vůči	vůči	k7c3
Rusku	Rusko	k1gNnSc3
v	v	k7c6
odpovědi	odpověď	k1gFnSc6
na	na	k7c4
tento	tento	k3xDgInSc4
útok	útok	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
prohlášení	prohlášení	k1gNnSc3
se	se	k3xPyFc4
nepřipojila	připojit	k5eNaPmAgFnS
ultralevice	ultralevice	k1gFnSc1
<g/>
,	,	kIx,
kam	kam	k6eAd1
patří	patřit	k5eAaImIp3nS
i	i	k9
KSČM	KSČM	kA
<g/>
,	,	kIx,
a	a	k8xC
ultrapravice	ultrapravice	k1gFnSc1
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
členem	člen	k1gInSc7
je	být	k5eAaImIp3nS
SPD	SPD	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
oznámil	oznámit	k5eAaPmAgMnS
slovenský	slovenský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
Eduard	Eduard	k1gMnSc1
Heger	Heger	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
Slovensko	Slovensko	k1gNnSc1
vyhostí	vyhostit	k5eAaPmIp3nS
tři	tři	k4xCgMnPc4
ruské	ruský	k2eAgMnPc4d1
diplomaty	diplomat	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
Ministři	ministr	k1gMnPc1
zahraničí	zahraničí	k1gNnSc2
Baltských	baltský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
uvedli	uvést	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
vyhostí	vyhostit	k5eAaPmIp3nS
dohromady	dohromady	k6eAd1
čtyři	čtyři	k4xCgInPc4
diplomaty	diplomat	k1gInPc4
–	–	k?
Litva	Litva	k1gFnSc1
vyhostí	vyhostit	k5eAaPmIp3nP
dva	dva	k4xCgInPc1
<g/>
,	,	kIx,
Lotyšsko	Lotyšsko	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
Estonsko	Estonsko	k1gNnSc1
jednoho	jeden	k4xCgMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Ruského	ruský	k2eAgMnSc2d1
diplomata	diplomat	k1gMnSc2
vyhostilo	vyhostit	k5eAaPmAgNnS
také	také	k9
Rumunsko	Rumunsko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Rusko	Rusko	k1gNnSc4
na	na	k7c4
to	ten	k3xDgNnSc4
zaregovalo	zaregovat	k5eAaBmAgNnS,k5eAaPmAgNnS,k5eAaImAgNnS
tak	tak	k9
<g/>
,	,	kIx,
že	že	k8xS
vyhostilo	vyhostit	k5eAaPmAgNnS
stejný	stejný	k2eAgInSc1d1
počet	počet	k1gInSc1
diplomatů	diplomat	k1gMnPc2
z	z	k7c2
každé	každý	k3xTgFnSc2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vyhostila	vyhostit	k5eAaPmAgFnS
ruské	ruský	k2eAgInPc4d1
diplomaty	diplomat	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reakce	reakce	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
V	v	k7c6
neděli	neděle	k1gFnSc6
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
vicepremiér	vicepremiér	k1gMnSc1
a	a	k8xC
ministr	ministr	k1gMnSc1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
Karel	Karel	k1gMnSc1
Havlíček	Havlíček	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
ruská	ruský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Rosatom	Rosatom	k1gInSc4
během	během	k7c2
několika	několik	k4yIc2
dní	den	k1gInPc2
vyřazena	vyřadit	k5eAaPmNgFnS
z	z	k7c2
možnosti	možnost	k1gFnSc2
účasti	účast	k1gFnSc2
na	na	k7c6
tendru	tendr	k1gInSc6
na	na	k7c4
dostavbu	dostavba	k1gFnSc4
jaderné	jaderný	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
Dukovany	Dukovany	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Minoritní	minoritní	k2eAgMnSc1d1
akcionář	akcionář	k1gMnSc1
energetické	energetický	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
ČEZ	ČEZ	kA
a	a	k8xC
energetický	energetický	k2eAgMnSc1d1
expert	expert	k1gMnSc1
Michal	Michal	k1gMnSc1
Šnobr	Šnobr	k1gMnSc1
se	se	k3xPyFc4
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
odhalení	odhalení	k1gNnSc6
účasti	účast	k1gFnSc2
agentů	agent	k1gMnPc2
GRU	GRU	kA
na	na	k7c6
výbuchu	výbuch	k1gInSc6
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
je	být	k5eAaImIp3nS
účast	účast	k1gFnSc4
Ruska	Rusko	k1gNnSc2
v	v	k7c6
tendru	tendr	k1gInSc6
vyloučená	vyloučený	k2eAgFnSc1d1
a	a	k8xC
tendr	tendr	k1gInSc1
tím	ten	k3xDgNnSc7
fakticky	fakticky	k6eAd1
končí	končit	k5eAaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
podle	podle	k7c2
Šnobra	Šnobr	k1gInSc2
„	„	k?
<g/>
jediný	jediný	k2eAgInSc1d1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
mohl	moct	k5eAaImAgInS
aktuálně	aktuálně	k6eAd1
nabídnout	nabídnout	k5eAaPmF
odzkoušený	odzkoušený	k2eAgInSc4d1
reaktor	reaktor	k1gInSc4
o	o	k7c6
výkonu	výkon	k1gInSc6
do	do	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
1	#num#	k4
200	#num#	k4
MW	MW	kA
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
Rusko	Rusko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
ministra	ministr	k1gMnSc2
Hamáčka	Hamáček	k1gMnSc2
také	také	k9
v	v	k7c6
situaci	situace	k1gFnSc6
po	po	k7c4
podezření	podezření	k1gNnSc4
českých	český	k2eAgFnPc2d1
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
složek	složka	k1gFnPc2
nebylo	být	k5eNaImAgNnS
na	na	k7c6
stole	stol	k1gInSc6
využívání	využívání	k1gNnSc2
ruské	ruský	k2eAgFnSc2d1
vakcíny	vakcína	k1gFnSc2
Sputnik	sputnik	k1gInSc1
V	V	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
o	o	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
v	v	k7c6
předchozím	předchozí	k2eAgNnSc6d1
období	období	k1gNnSc6
rovněž	rovněž	k9
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
uvažovalo	uvažovat	k5eAaImAgNnS
<g/>
,	,	kIx,
přestože	přestože	k8xS
dosud	dosud	k6eAd1
nebyla	být	k5eNaImAgFnS
schválená	schválený	k2eAgFnSc1d1
agenturou	agentura	k1gFnSc7
EMA	Ema	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Možnou	možný	k2eAgFnSc4d1
účast	účast	k1gFnSc4
ruských	ruský	k2eAgFnPc2d1
tajných	tajný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
na	na	k7c6
výbuších	výbuch	k1gInPc6
muničního	muniční	k2eAgInSc2d1
areálu	areál	k1gInSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
přirovnal	přirovnat	k5eAaPmAgMnS
bývalý	bývalý	k2eAgMnSc1d1
náčelník	náčelník	k1gMnSc1
Generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
AČR	AČR	kA
Jiří	Jiří	k1gMnSc1
Šedivý	Šedivý	k1gMnSc1
k	k	k7c3
vojenskému	vojenský	k2eAgNnSc3d1
napadení	napadení	k1gNnSc3
země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Premiér	premiér	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
později	pozdě	k6eAd2
událost	událost	k1gFnSc4
označil	označit	k5eAaPmAgMnS
za	za	k7c4
útok	útok	k1gInSc4
na	na	k7c4
zboží	zboží	k1gNnSc4
bulharského	bulharský	k2eAgMnSc2d1
obchodníka	obchodník	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
ji	on	k3xPp3gFnSc4
zřejmě	zřejmě	k6eAd1
prodával	prodávat	k5eAaImAgInS
zemím	zem	k1gFnPc3
proti	proti	k7c3
<g />
.	.	kIx.
</s>
<s hack="1">
Rusku	Rusko	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
nešlo	jít	k5eNaImAgNnS
tedy	tedy	k9
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
o	o	k7c4
útok	útok	k1gInSc4
na	na	k7c4
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
Straničtí	stranický	k2eAgMnPc1d1
předsedové	předseda	k1gMnPc1
Vojtěch	Vojtěch	k1gMnSc1
Filip	Filip	k1gMnSc1
(	(	kIx(
<g/>
KSČM	KSČM	kA
<g/>
)	)	kIx)
a	a	k8xC
Tomio	Tomio	k6eAd1
Okamura	Okamura	k1gFnSc1
(	(	kIx(
<g/>
SPD	SPD	kA
<g/>
)	)	kIx)
zjištěním	zjištění	k1gNnSc7
o	o	k7c6
Vrběticích	Vrbětice	k1gFnPc6
podle	podle	k7c2
prvních	první	k4xOgNnPc2
vyjádření	vyjádření	k1gNnPc2
zatím	zatím	k6eAd1
nevěřili	věřit	k5eNaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Politici	politik	k1gMnPc1
volební	volební	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
Spolu	spolu	k6eAd1
Petr	Petr	k1gMnSc1
Fiala	Fiala	k1gMnSc1
(	(	kIx(
<g/>
ODS	ODS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Markéta	Markéta	k1gFnSc1
Pekarová	Pekarová	k1gFnSc1
Adamová	Adamová	k1gFnSc1
(	(	kIx(
<g/>
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Miloš	Miloš	k1gMnSc1
Vystrčil	Vystrčil	k1gMnSc1
(	(	kIx(
<g/>
ODS	ODS	kA
<g/>
)	)	kIx)
považovali	považovat	k5eAaImAgMnP
výbuch	výbuch	k1gInSc4
za	za	k7c4
státní	státní	k2eAgInSc4d1
terorismus	terorismus	k1gInSc4
a	a	k8xC
požadovali	požadovat	k5eAaImAgMnP
tvrdší	tvrdý	k2eAgFnSc4d2
reakci	reakce	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
81	#num#	k4
<g/>
]	]	kIx)
Nejvyšší	vysoký	k2eAgMnSc1d3
státní	státní	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
Pavel	Pavel	k1gMnSc1
Zeman	Zeman	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
trestní	trestní	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
nezná	znát	k5eNaImIp3nS,k5eAaImIp3nS
pojem	pojem	k1gInSc1
„	„	k?
<g/>
státní	státní	k2eAgInSc1d1
terorismus	terorismus	k1gInSc1
<g/>
”	”	k?
a	a	k8xC
z	z	k7c2
„	„	k?
<g/>
právního	právní	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
není	být	k5eNaImIp3nS
věc	věc	k1gFnSc1
prověřována	prověřován	k2eAgFnSc1d1
jako	jako	k9
trestný	trestný	k2eAgInSc4d1
čin	čin	k1gInSc4
teroristického	teroristický	k2eAgInSc2d1
útoku	útok	k1gInSc2
<g/>
”	”	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Bývalý	bývalý	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
považoval	považovat	k5eAaImAgMnS
kauzu	kauza	k1gFnSc4
za	za	k7c4
„	„	k?
<g/>
vykonstruovaného	vykonstruovaný	k2eAgMnSc4d1
strašáka	strašák	k1gMnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
Prezident	prezident	k1gMnSc1
Miloš	Miloš	k1gMnSc1
Zeman	Zeman	k1gMnSc1
prostřednictvím	prostřednictvím	k7c2
mluvčího	mluvčí	k1gMnSc2
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
k	k	k7c3
záležitosti	záležitost	k1gFnSc3
vyjádří	vyjádřit	k5eAaPmIp3nS
až	až	k9
později	pozdě	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
noci	noc	k1gFnSc6
na	na	k7c6
středu	střed	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
na	na	k7c6
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
prováděli	provádět	k5eAaImAgMnP
příslušníci	příslušník	k1gMnPc1
Národní	národní	k2eAgFnSc2d1
centrály	centrála	k1gFnSc2
proti	proti	k7c3
organizovanému	organizovaný	k2eAgInSc3d1
zločinu	zločin	k1gInSc3
rozsáhlá	rozsáhlý	k2eAgNnPc4d1
zatýkání	zatýkání	k1gNnPc4
Čechů	Čech	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
paramilitantních	paramilitantní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
řízených	řízený	k2eAgFnPc2d1
vyhoštěnými	vyhoštěný	k2eAgMnPc7d1
ruskými	ruský	k2eAgMnPc7d1
diplomaty	diplomat	k1gMnPc7
<g/>
,	,	kIx,
agenty	agens	k1gInPc7
GRU	GRU	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
ministr	ministr	k1gMnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
Jakub	Jakub	k1gMnSc1
Kulhánek	Kulhánek	k1gMnSc1
na	na	k7c6
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
po	po	k7c6
jednání	jednání	k1gNnSc6
s	s	k7c7
ruským	ruský	k2eAgMnSc7d1
velvyslancem	velvyslanec	k1gMnSc7
Alexandrem	Alexandr	k1gMnSc7
Zmejevským	Zmejevský	k2eAgMnPc3d1
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Rusko	Rusko	k1gNnSc1
má	mít	k5eAaImIp3nS
čas	čas	k1gInSc4
do	do	k7c2
následujícího	následující	k2eAgInSc2d1
dne	den	k1gInSc2
do	do	k7c2
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
umožnilo	umožnit	k5eAaPmAgNnS
návrat	návrat	k1gInSc4
všem	všecek	k3xTgMnPc3
vyhoštěným	vyhoštěný	k2eAgMnPc3d1
diplomatům	diplomat	k1gMnPc3
zpět	zpět	k6eAd1
na	na	k7c6
velvyslanectví	velvyslanectví	k1gNnSc6
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
tak	tak	k6eAd1
nestane	stanout	k5eNaPmIp3nS
<g/>
,	,	kIx,
ministerstvo	ministerstvo	k1gNnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
rozhodne	rozhodnout	k5eAaPmIp3nS
o	o	k7c4
snížení	snížení	k1gNnSc4
ruských	ruský	k2eAgMnPc2d1
diplomatů	diplomat	k1gMnPc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odpovídal	odpovídat	k5eAaImAgMnS
stavu	stav	k1gInSc3
českého	český	k2eAgNnSc2d1
velvyslanectví	velvyslanectví	k1gNnSc2
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
ministr	ministr	k1gMnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
Jakub	Jakub	k1gMnSc1
Kulhánek	Kulhánek	k1gMnSc1
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
s	s	k7c7
odvoláním	odvolání	k1gNnSc7
na	na	k7c4
článek	článek	k1gInSc4
11	#num#	k4
Vídeňské	vídeňský	k2eAgFnPc4d1
úmluvy	úmluva	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
zastoupení	zastoupení	k1gNnSc1
obou	dva	k4xCgFnPc2
zemí	zem	k1gFnPc2
na	na	k7c6
ambasádách	ambasáda	k1gFnPc6
vyrovná	vyrovnat	k5eAaBmIp3nS,k5eAaPmIp3nS
do	do	k7c2
konce	konec	k1gInSc2
května	květen	k1gInSc2
2021	#num#	k4
a	a	k8xC
budou	být	k5eAaImBp3nP
vypovězeni	vypovědět	k5eAaPmNgMnP
další	další	k2eAgMnSc1d1
diplomaté	diplomat	k1gMnPc1
a	a	k8xC
administrativní	administrativní	k2eAgMnPc1d1
pracovníci	pracovník	k1gMnPc1
ruského	ruský	k2eAgNnSc2d1
velvyslanectví	velvyslanectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc4
rozhodnutí	rozhodnutí	k1gNnSc4
učinil	učinit	k5eAaImAgInS,k5eAaPmAgInS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
ruská	ruský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
nereagovala	reagovat	k5eNaBmAgFnS
na	na	k7c4
výzvu	výzva	k1gFnSc4
povolit	povolit	k5eAaPmF
návrat	návrat	k1gInSc4
českých	český	k2eAgMnPc2d1
diplomatů	diplomat	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
vypovězeni	vypovědět	k5eAaPmNgMnP
bezdůvodně	bezdůvodně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastropování	Zastropování	k1gNnSc1
počtu	počet	k1gInSc2
pracovníků	pracovník	k1gMnPc2
ruské	ruský	k2eAgFnSc2d1
ambasády	ambasáda	k1gFnSc2
bude	být	k5eAaImBp3nS
paritní	paritní	k2eAgFnSc1d1
s	s	k7c7
českým	český	k2eAgNnSc7d1
zastoupením	zastoupení	k1gNnSc7
<g/>
,	,	kIx,
tzn.	tzn.	kA
pět	pět	k4xCc4
diplomatů	diplomat	k1gMnPc2
a	a	k8xC
19	#num#	k4
administrativních	administrativní	k2eAgMnPc2d1
zaměstnaců	zaměstnace	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
prezident	prezident	k1gMnSc1
Miloš	Miloš	k1gMnSc1
Zeman	Zeman	k1gMnSc1
při	při	k7c6
svém	svůj	k3xOyFgInSc6
mimořádném	mimořádný	k2eAgInSc6d1
projevu	projev	k1gInSc6
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
neexistují	existovat	k5eNaImIp3nP
důkazy	důkaz	k1gInPc4
ani	ani	k8xC
svědectví	svědectví	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
tito	tento	k3xDgMnPc1
dva	dva	k4xCgMnPc1
agenti	agent	k1gMnPc1
byli	být	k5eAaImAgMnP
ve	v	k7c6
vrbětickém	vrbětický	k2eAgInSc6d1
areálu	areál	k1gInSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
také	také	k9
uvedl	uvést	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Žádná	žádný	k3yNgFnSc1
suverénní	suverénní	k2eAgFnSc1d1
země	země	k1gFnSc1
si	se	k3xPyFc3
nemůže	moct	k5eNaImIp3nS
dovolit	dovolit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
na	na	k7c6
jejím	její	k3xOp3gNnSc6
území	území	k1gNnSc6
dva	dva	k4xCgInPc1
agenti	agent	k1gMnPc1
cizího	cizí	k2eAgInSc2d1
státu	stát	k1gInSc2
způsobili	způsobit	k5eAaPmAgMnP
teroristický	teroristický	k2eAgInSc4d1
atentát	atentát	k1gInSc4
<g/>
,	,	kIx,
při	při	k7c6
němž	jenž	k3xRgMnSc6
zahynuli	zahynout	k5eAaPmAgMnP
dva	dva	k4xCgMnPc1
čeští	český	k2eAgMnPc1d1
občané	občan	k1gMnPc1
a	a	k8xC
byla	být	k5eAaImAgFnS
způsobena	způsobit	k5eAaPmNgFnS
miliardová	miliardový	k2eAgFnSc1d1
škoda	škoda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reakce	reakce	k1gFnSc1
Ruska	Rusko	k1gNnSc2
</s>
<s>
Zástupce	zástupce	k1gMnSc1
předsedy	předseda	k1gMnSc2
zahraničního	zahraniční	k2eAgInSc2d1
výboru	výbor	k1gInSc2
Rady	rada	k1gFnSc2
federace	federace	k1gFnSc2
Vladimir	Vladimir	k1gMnSc1
Džabarov	Džabarov	k1gInSc4
tvrzení	tvrzení	k1gNnSc2
českých	český	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
označil	označit	k5eAaPmAgInS
za	za	k7c7
„	„	k?
<g/>
nesmysl	nesmysl	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
dále	daleko	k6eAd2
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
Rusko	Rusko	k1gNnSc1
nemá	mít	k5eNaImIp3nS
nic	nic	k3yNnSc1
lepšího	dobrý	k2eAgNnSc2d2
na	na	k7c6
práci	práce	k1gFnSc6
než	než	k8xS
vyhazovat	vyhazovat	k5eAaImF
do	do	k7c2
povětří	povětří	k1gNnSc6
nějaké	nějaký	k3yIgInPc4
sklady	sklad	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikdy	nikdy	k6eAd1
jsme	být	k5eAaImIp1nP
Česko	Česko	k1gNnSc4
nepovažovali	považovat	k5eNaImAgMnP
za	za	k7c4
svého	svůj	k3xOyFgMnSc4
protivníka	protivník	k1gMnSc4
<g/>
“	“	k?
a	a	k8xC
že	že	k8xS
kroky	krok	k1gInPc1
Česka	Česko	k1gNnSc2
jsou	být	k5eAaImIp3nP
„	„	k?
<g/>
vynalezená	vynalezený	k2eAgFnSc1d1
situace	situace	k1gFnSc1
na	na	k7c4
podporu	podpora	k1gFnSc4
Američanů	Američan	k1gMnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c6
vyhoštění	vyhoštění	k1gNnSc6
18	#num#	k4
pracovníků	pracovník	k1gMnPc2
ruského	ruský	k2eAgNnSc2d1
velvyslanectví	velvyslanectví	k1gNnSc2
oznámilo	oznámit	k5eAaPmAgNnS
dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
ruské	ruský	k2eAgFnSc2d1
ministerstvo	ministerstvo	k1gNnSc1
zahraničí	zahraničí	k1gNnSc2
českému	český	k2eAgMnSc3d1
velvyslanci	velvyslanec	k1gMnSc3
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
Vítězslavu	Vítězslava	k1gFnSc4
Pivoňkovi	Pivoňka	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
vyhostí	vyhostit	k5eAaPmIp3nS
dvacet	dvacet	k4xCc4
zaměstnanců	zaměstnanec	k1gMnPc2
české	český	k2eAgFnSc2d1
ambasády	ambasáda	k1gFnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
šestnáct	šestnáct	k4xCc1
diplomatů	diplomat	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
šéf	šéf	k1gMnSc1
ruské	ruský	k2eAgFnSc2d1
rozvědky	rozvědka	k1gFnSc2
SVR	SVR	kA
Sergej	Sergej	k1gMnSc1
Naryškin	Naryškin	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
účast	účast	k1gFnSc1
Ruska	Rusko	k1gNnSc2
na	na	k7c6
těchto	tento	k3xDgInPc6
výbuších	výbuch	k1gInPc6
je	být	k5eAaImIp3nS
„	„	k?
<g/>
ubohá	ubohý	k2eAgFnSc1d1
a	a	k8xC
nízká	nízký	k2eAgFnSc1d1
lež	lež	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zásah	zásah	k1gInSc1
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
je	být	k5eAaImIp3nS
po	po	k7c6
dlouhých	dlouhý	k2eAgInPc6d1
letech	let	k1gInPc6
ukončen	ukončen	k2eAgMnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hzscr	Hzscr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2020-10-14	2020-10-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Babiš	Babiš	k1gInSc1
<g/>
:	:	kIx,
Rusko	Rusko	k1gNnSc1
bylo	být	k5eAaImAgNnS
zapojeno	zapojit	k5eAaPmNgNnS
do	do	k7c2
výbuchu	výbuch	k1gInSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
<g/>
,	,	kIx,
2021-04-17	2021-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kulomety	kulomet	k1gInPc1
do	do	k7c2
Konga	Kongo	k1gNnSc2
<g/>
,	,	kIx,
prošlé	prošlý	k2eAgInPc1d1
náboje	náboj	k1gInPc1
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staré	Staré	k2eAgInPc1d1
hříchy	hřích	k1gInPc1
vrbětického	vrbětický	k2eAgMnSc2d1
zbrojaře	zbrojař	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nejvyšší	vysoký	k2eAgInSc1d3
soud	soud	k1gInSc1
vrátil	vrátit	k5eAaPmAgInS
případ	případ	k1gInSc4
odškodnění	odškodnění	k1gNnSc2
za	za	k7c4
Babišovy	Babišův	k2eAgInPc4d1
výroky	výrok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Šéf	šéf	k1gMnSc1
Imexu	Imex	k1gInSc2
kontroloval	kontrolovat	k5eAaImAgMnS
v	v	k7c6
muničním	muniční	k2eAgInSc6d1
skladu	sklad	k1gInSc6
balení	balení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc4
den	dna	k1gFnPc2
přišel	přijít	k5eAaPmAgInS
výbuch	výbuch	k1gInSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2014-12-07	2014-12-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Majitelé	majitel	k1gMnPc1
vybuchlého	vybuchlý	k2eAgInSc2d1
skladu	sklad	k1gInSc2
se	se	k3xPyFc4
už	už	k6eAd1
v	v	k7c6
minulosti	minulost	k1gFnSc6
ocitli	ocitnout	k5eAaPmAgMnP
v	v	k7c6
hledáčku	hledáček	k1gInSc6
policie	policie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2014-12-05	2014-12-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výbuchy	výbuch	k1gInPc7
zničily	zničit	k5eAaPmAgFnP
muniční	muniční	k2eAgInSc4d1
sklad	sklad	k1gInSc4
na	na	k7c6
Zlínsku	Zlínsko	k1gNnSc6
<g/>
,	,	kIx,
dva	dva	k4xCgMnPc1
lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
stále	stále	k6eAd1
pohřešují	pohřešovat	k5eAaImIp3nP
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2014-10-16	2014-10-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Z	z	k7c2
muničního	muniční	k2eAgInSc2d1
areálu	areál	k1gInSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
znovu	znovu	k6eAd1
zněly	znět	k5eAaImAgInP
exploze	exploze	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
<g/>
,	,	kIx,
2014-12-10	2014-12-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Okolí	okolí	k1gNnPc2
vybuchlého	vybuchlý	k2eAgInSc2d1
skladu	sklad	k1gInSc2
obsadili	obsadit	k5eAaPmAgMnP
pyrotechnici	pyrotechnik	k1gMnPc1
<g/>
,	,	kIx,
evakuace	evakuace	k1gFnSc1
začala	začít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2014-10-23	2014-10-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Po	po	k7c6
výbuchu	výbuch	k1gInSc6
v	v	k7c6
muničním	muniční	k2eAgInSc6d1
skladu	sklad	k1gInSc6
jsou	být	k5eAaImIp3nP
dva	dva	k4xCgMnPc1
lidé	člověk	k1gMnPc1
nezvěstní	zvěstný	k2eNgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2014-10-16	2014-10-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Za	za	k7c4
Babišovy	Babišův	k2eAgInPc4d1
výroky	výrok	k1gInPc4
o	o	k7c6
Vrběticích	Vrbětik	k1gMnPc6
zbrojaři	zbrojař	k1gMnPc1
odškodné	odškodná	k1gFnSc2
nedostanou	dostat	k5eNaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Imex	Imex	k1gInSc1
žádal	žádat	k5eAaImAgInS
po	po	k7c6
státu	stát	k1gInSc6
15	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hasiči	hasič	k1gMnPc1
netušili	tušit	k5eNaImAgMnP
<g/>
,	,	kIx,
do	do	k7c2
jakého	jaký	k3yIgNnSc2,k3yQgNnSc2,k3yRgNnSc2
pekla	peklo	k1gNnSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
jedou	jet	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
<g/>
,	,	kIx,
2014-11-03	2014-11-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://tn.nova.cz/clanek/nasledky-explozi-ve-vrbeticich-hasici-likvidovali-6-let-pripomente-si-udalost.html	https://tn.nova.cz/clanek/nasledky-explozi-ve-vrbeticich-hasici-likvidovali-6-let-pripomente-si-udalost.html	k1gMnSc1
<g/>
↑	↑	k?
U	u	k7c2
vybuchlého	vybuchlý	k2eAgInSc2d1
skladu	sklad	k1gInSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
našli	najít	k5eAaPmAgMnP
ostatky	ostatek	k1gInPc1
dvou	dva	k4xCgFnPc2
pohřešovaných	pohřešovaná	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
<g/>
,	,	kIx,
2014-11-21	2014-11-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pyrotechnici	pyrotechnik	k1gMnPc1
vyčistili	vyčistit	k5eAaPmAgMnP
areál	areál	k1gInSc4
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
<g/>
,	,	kIx,
munice	munice	k1gFnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
odvézt	odvézt	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
<g/>
,	,	kIx,
2014-11-30	2014-11-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Okolí	okolí	k1gNnSc2
Vrbětic	Vrbětice	k1gFnPc2
prohledávají	prohledávat	k5eAaImIp3nP
pyrotechnici	pyrotechnik	k1gMnPc1
<g/>
,	,	kIx,
v	v	k7c4
sobotu	sobota	k1gFnSc4
tam	tam	k6eAd1
přijede	přijet	k5eAaPmIp3nS
premiér	premiér	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2014-12-04	2014-12-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
explodoval	explodovat	k5eAaBmAgInS
jiný	jiný	k2eAgInSc4d1
sklad	sklad	k1gInSc4
s	s	k7c7
municí	munice	k1gFnSc7
<g/>
,	,	kIx,
lidé	člověk	k1gMnPc1
stráví	strávit	k5eAaPmIp3nP
noc	noc	k1gFnSc4
mimo	mimo	k7c4
domov	domov	k1gInSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2014-12-03	2014-12-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Sklad	sklad	k1gInSc1
nemohl	moct	k5eNaImAgInS
vybuchnout	vybuchnout	k5eAaPmF
samovolně	samovolně	k6eAd1
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
zástupce	zástupce	k1gMnSc1
firmy	firma	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
si	se	k3xPyFc3
jej	on	k3xPp3gInSc4
pronajímala	pronajímat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
<g/>
,	,	kIx,
2014-12-03	2014-12-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
pes	pes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
pěti	pět	k4xCc7
lety	léto	k1gNnPc7
vybuchlo	vybuchnout	k5eAaPmAgNnS
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
60	#num#	k4
tun	tuna	k1gFnPc2
munice	munice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policejní	policejní	k2eAgInSc1d1
zásah	zásah	k1gInSc1
trvá	trvat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2019-10-16	2019-10-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sklad	sklad	k1gInSc1
v	v	k7c6
Květné	květný	k2eAgFnSc6d1
je	být	k5eAaImIp3nS
prázdný	prázdný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyři	čtyři	k4xCgInPc1
kamiony	kamion	k1gInPc1
odvezly	odvézt	k5eAaPmAgInP
poslední	poslední	k2eAgFnSc4d1
munici	munice	k1gFnSc4
pocházející	pocházející	k2eAgFnSc4d1
z	z	k7c2
Vrbětic	Vrbětice	k1gFnPc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2018-08-16	2018-08-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
MAZANCOVÁ	MAZANCOVÁ	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pachatel	pachatel	k1gMnSc1
neznámý	známý	k2eNgMnSc1d1
<g/>
,	,	kIx,
konec	konec	k1gInSc1
v	v	k7c6
nedohlednu	nedohledno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policie	policie	k1gFnSc1
už	už	k9
1025	#num#	k4
dnů	den	k1gInPc2
prověřuje	prověřovat	k5eAaImIp3nS
výbuchy	výbuch	k1gInPc4
skladů	sklad	k1gInPc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2017-08-06	2017-08-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pět	pět	k4xCc1
let	léto	k1gNnPc2
po	po	k7c6
výbuchu	výbuch	k1gInSc6
v	v	k7c4
munice	munice	k1gFnPc4
ve	v	k7c6
Vrběticích	Vrbětik	k1gMnPc6
pyrotechnici	pyrotechnik	k1gMnPc1
areál	areál	k1gInSc1
stále	stále	k6eAd1
čistí	čistit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hotovo	hotov	k2eAgNnSc1d1
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
příští	příští	k2eAgNnSc4d1
léto	léto	k1gNnSc4
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2019-10-16	2019-10-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výbuch	výbuch	k1gInSc1
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
<g/>
:	:	kIx,
policie	policie	k1gFnSc1
vrátila	vrátit	k5eAaPmAgFnS
lidem	lid	k1gInSc7
k	k	k7c3
užívání	užívání	k1gNnSc3
43,5	43,5	k4
ha	ha	kA
pozemků	pozemek	k1gInPc2
<g/>
,	,	kIx,
366,5	366,5	k4
ha	ha	kA
ještě	ještě	k6eAd1
zbývá	zbývat	k5eAaImIp3nS
uvolnit	uvolnit	k5eAaPmF
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
šesti	šest	k4xCc6
letech	léto	k1gNnPc6
skončily	skončit	k5eAaPmAgInP
v	v	k7c6
bývalém	bývalý	k2eAgInSc6d1
muničním	muniční	k2eAgInSc6d1
skladu	sklad	k1gInSc6
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
záchranné	záchranný	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2020-09-29	2020-09-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
;	;	kIx,
VERNER	Verner	k1gMnSc1
<g/>
,	,	kIx,
Roman	Roman	k1gMnSc1
(	(	kIx(
<g/>
rv	rv	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkradli	rozkrást	k5eAaPmAgMnP
zbraně	zbraň	k1gFnPc4
z	z	k7c2
vybuchlého	vybuchlý	k2eAgInSc2d1
vrbětického	vrbětický	k2eAgInSc2d1
muničního	muniční	k2eAgInSc2d1
skladu	sklad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soud	soud	k1gInSc1
jim	on	k3xPp3gMnPc3
potvrdil	potvrdit	k5eAaPmAgInS
podmínky	podmínka	k1gFnPc1
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2019-06-06	2019-06-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
krádeže	krádež	k1gFnPc4
zbraní	zbraň	k1gFnPc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
rozdal	rozdat	k5eAaPmAgInS
soud	soud	k1gInSc1
jeden	jeden	k4xCgInSc1
trest	trest	k1gInSc1
vězení	vězení	k1gNnSc2
a	a	k8xC
pět	pět	k4xCc1
podmínek	podmínka	k1gFnPc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2019-02-11	2019-02-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policie	policie	k1gFnSc1
navrhla	navrhnout	k5eAaPmAgFnS
obžalovat	obžalovat	k5eAaPmF
osm	osm	k4xCc4
lidí	člověk	k1gMnPc2
za	za	k7c4
krádež	krádež	k1gFnSc4
zbraní	zbraň	k1gFnPc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
<g/>
,	,	kIx,
hrozí	hrozit	k5eAaImIp3nS
jim	on	k3xPp3gFnPc3
osm	osm	k4xCc1
let	léto	k1gNnPc2
vězení	vězení	k1gNnSc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2018-07-18	2018-07-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Selhal	selhat	k5eAaPmAgMnS
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
jedinec	jedinec	k1gMnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
systém	systém	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Chovanec	Chovanec	k1gMnSc1
chce	chtít	k5eAaImIp3nS
prověřit	prověřit	k5eAaPmF
zabezpečení	zabezpečení	k1gNnSc4
areálu	areál	k1gInSc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2017-09-13	2017-09-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policie	policie	k1gFnSc1
v	v	k7c6
kauze	kauza	k1gFnSc6
krádeží	krádež	k1gFnPc2
zbraní	zbraň	k1gFnPc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
nechybovala	chybovat	k5eNaImAgFnS
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
Chovanec	Chovanec	k1gMnSc1
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2017-09-29	2017-09-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
;	;	kIx,
ZELENKA	Zelenka	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
(	(	kIx(
<g/>
jzl	jzl	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obžaloba	obžaloba	k1gFnSc1
za	za	k7c4
výbušniny	výbušnina	k1gFnPc4
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
míří	mířit	k5eAaImIp3nS
na	na	k7c4
firmy	firma	k1gFnPc4
Excalibur	Excalibura	k1gFnPc2
Army	Arm	k2eAgFnPc4d1
<g/>
,	,	kIx,
Real	Real	k1gInSc1
Trade	Trad	k1gInSc5
a	a	k8xC
pět	pět	k4xCc1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2015-11-11	2015-11-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soud	soud	k1gInSc1
zprostil	zprostit	k5eAaPmAgInS
obžaloby	obžaloba	k1gFnPc4
Excalibur	Excalibura	k1gFnPc2
Army	Arma	k1gFnPc4
a	a	k8xC
Real	Real	k1gInSc4
Trade	Trad	k1gInSc5
v	v	k7c6
kauze	kauza	k1gFnSc6
Vrbětice	Vrbětice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2016-07-27	2016-07-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravomocný	pravomocný	k2eAgInSc4d1
osvobozující	osvobozující	k2eAgInSc4d1
rozsudek	rozsudek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
skladování	skladování	k1gNnSc4
munice	munice	k1gFnSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
nebude	být	k5eNaImBp3nS
nikdo	nikdo	k3yNnSc1
potrestán	potrestán	k2eAgInSc4d1
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2017-12-15	2017-12-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
NETOPIL	topit	k5eNaImAgMnS
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
<g/>
;	;	kIx,
FRÁNEK	FRÁNEK	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firma	firma	k1gFnSc1
získá	získat	k5eAaPmIp3nS
zpět	zpět	k6eAd1
500	#num#	k4
protipěchotních	protipěchotní	k2eAgFnPc2d1
min	mina	k1gFnPc2
z	z	k7c2
Vrbětic	Vrbětice	k1gFnPc2
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgInS
soud	soud	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
Zlín	Zlín	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2018-01-17	2018-01-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
selhal	selhat	k5eAaPmAgMnS
<g/>
?	?	kIx.
</s>
<s desamb="1">
Hejtman	hejtman	k1gMnSc1
nevěděl	vědět	k5eNaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
muniční	muniční	k2eAgInSc1d1
sklad	sklad	k1gInSc1
nepatří	patřit	k5eNaImIp3nS
armádě	armáda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Kauza	kauza	k1gFnSc1
Vrbětice	Vrbětice	k1gFnSc2
smetla	smetnout	k5eAaPmAgFnS
tři	tři	k4xCgMnPc4
ředitele	ředitel	k1gMnPc4
<g/>
,	,	kIx,
šéf	šéf	k1gMnSc1
VTÚ	VTÚ	kA
pochybení	pochybení	k1gNnSc1
odmítá	odmítat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
E15	E15	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CZECH	CZECH	kA
NEWS	NEWS	kA
CENTER	centrum	k1gNnPc2
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nic	nic	k6eAd1
jsme	být	k5eAaImIp1nP
nezanedbali	zanedbat	k5eNaPmAgMnP
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
šéf	šéf	k1gMnSc1
úřadu	úřad	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
staral	starat	k5eAaImAgInS
o	o	k7c4
vrbětický	vrbětický	k2eAgInSc4d1
sklad	sklad	k1gInSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ŽIVĚ	živě	k6eAd1
<g/>
:	:	kIx,
Do	do	k7c2
výbuchu	výbuch	k1gInSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
byli	být	k5eAaImAgMnP
zapojení	zapojený	k2eAgMnPc1d1
ruští	ruský	k2eAgMnPc1d1
agenti	agent	k1gMnPc1
<g/>
,	,	kIx,
oznámil	oznámit	k5eAaPmAgMnS
Babiš	Babiš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česko	Česko	k1gNnSc1
jich	on	k3xPp3gMnPc2
osmnáct	osmnáct	k4xCc4
vyhostí	vyhostit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2021-04-17	2021-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Mimořádné	mimořádný	k2eAgInPc4d1
pořady	pořad	k1gInPc4
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
:	:	kIx,
Zapojení	zapojení	k1gNnPc2
ruských	ruský	k2eAgMnPc2d1
agentů	agent	k1gMnPc2
do	do	k7c2
výbuchu	výbuch	k1gInSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2021-04-18	2021-04-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čas	čas	k1gInSc1
41	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
41	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
SPURNÝ	Spurný	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
Kundra	Kundra	k1gFnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
výbuchem	výbuch	k1gInSc7
muničního	muniční	k2eAgInSc2d1
skladu	sklad	k1gInSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
stojí	stát	k5eAaImIp3nS
ruští	ruský	k2eAgMnPc1d1
agenti	agent	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
pokusili	pokusit	k5eAaPmAgMnP
zabít	zabít	k5eAaPmF
Skripala	Skripal	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Respekt	respekt	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economium	k1gNnPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BLAŽEK	Blažek	k1gMnSc1
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Exkluzivně	exkluzivně	k6eAd1
<g/>
:	:	kIx,
Rusové	Rus	k1gMnPc1
podezřelí	podezřelý	k2eAgMnPc1d1
z	z	k7c2
výbuchu	výbuch	k1gInSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
jsou	být	k5eAaImIp3nP
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
otrávili	otrávit	k5eAaPmAgMnP
agenta	agent	k1gMnSc4
Skripala	Skripal	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2021-04-17	2021-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jiří	Jiří	k1gMnSc1
Hošek	Hošek	k1gMnSc1
<g/>
,	,	kIx,
Tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
od	od	k7c2
útoku	útok	k1gInSc2
novičokem	novičok	k1gMnSc7
v	v	k7c4
Salisbury	Salisbur	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámější	známý	k2eAgInSc4d3
čin	čin	k1gInSc4
zabijáků	zabiják	k1gInPc2
z	z	k7c2
GRU	GRU	kA
<g/>
,	,	kIx,
Seznam	seznam	k1gInSc4
Zprávy	zpráva	k1gFnSc2
<g/>
,	,	kIx,
18.4	18.4	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko	Rusko	k1gNnSc1
hledané	hledaný	k2eAgFnSc2d1
agenty	agens	k1gInPc4
Čechům	Čech	k1gMnPc3
nevydá	vydat	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
proti	proti	k7c3
ústavě	ústava	k1gFnSc3
<g/>
,	,	kIx,
argumentuje	argumentovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2021-04-17	2021-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Stopy	stopa	k1gFnPc1
ruských	ruský	k2eAgMnPc2d1
špionů	špion	k1gMnPc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
‚	‚	k?
<g/>
Objednali	objednat	k5eAaPmAgMnP
se	se	k3xPyFc4
<g/>
,	,	kIx,
ale	ale	k8xC
nikdo	nikdo	k3yNnSc1
nepřijel	přijet	k5eNaPmAgMnS
<g/>
,	,	kIx,
<g/>
’	’	k?
reaguje	reagovat	k5eAaBmIp3nS
zástupce	zástupce	k1gMnSc1
firmy	firma	k1gFnSc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Do	do	k7c2
výbuchu	výbuch	k1gInSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
byla	být	k5eAaImAgFnS
zapojena	zapojen	k2eAgFnSc1d1
ruská	ruský	k2eAgFnSc1d1
tajná	tajný	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
Babiš	Babiš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česko	Česko	k1gNnSc1
vyhostí	vyhostit	k5eAaPmIp3nS
18	#num#	k4
lidí	člověk	k1gMnPc2
z	z	k7c2
ambasády	ambasáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gMnSc1
GRU	GRU	kA
Globetrotters	Globetrotters	k1gInSc1
<g/>
:	:	kIx,
Mission	Mission	k1gInSc1
London	London	k1gMnSc1
<g/>
.	.	kIx.
bellingcat	bellingcat	k5eAaPmF
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-28	2019-06-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Imex	Imex	k1gInSc1
Group	Group	k1gInSc1
podala	podat	k5eAaPmAgFnS
trestní	trestní	k2eAgNnSc4d1
oznámení	oznámení	k1gNnSc4
kvůli	kvůli	k7c3
úniku	únik	k1gInSc3
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
16.12	16.12	k4
<g/>
.2014	.2014	k4
<g/>
↑	↑	k?
Gordon	Gordon	k1gMnSc1
Corera	Corero	k1gNnSc2
<g/>
,	,	kIx,
Salisbury	Salisbura	k1gFnSc2
poisoning	poisoning	k1gInSc1
suspects	suspects	k1gInSc1
'	'	kIx"
<g/>
linked	linked	k1gMnSc1
to	ten	k3xDgNnSc4
Czech	Czech	k1gMnSc1
blast	blast	k1gMnSc1
<g/>
'	'	kIx"
<g/>
,	,	kIx,
BBC	BBC	kA
<g />
.	.	kIx.
</s>
<s hack="1">
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
18.4	18.4	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
Senior	senior	k1gMnSc1
GRU	GRU	kA
Leader	leader	k1gMnSc1
Directly	Directly	k1gMnSc1
Involved	Involved	k1gMnSc1
With	With	k1gMnSc1
Czech	Czech	k1gMnSc1
Arms	Arms	k1gInSc4
Depot	depot	k1gNnSc2
Explosion	Explosion	k1gInSc1
<g/>
,	,	kIx,
Bellingcat	Bellingcat	k1gFnSc1
<g/>
,	,	kIx,
20.4	20.4	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
Ondřej	Ondřej	k1gMnSc1
Kundra	Kundra	k1gFnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Spurný	Spurný	k1gMnSc1
<g/>
,	,	kIx,
Za	za	k7c7
výbuchem	výbuch	k1gInSc7
muničního	muniční	k2eAgInSc2d1
skladu	sklad	k1gInSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
je	být	k5eAaImIp3nS
šest	šest	k4xCc1
členů	člen	k1gMnPc2
ruského	ruský	k2eAgNnSc2d1
komanda	komando	k1gNnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
velitele	velitel	k1gMnSc2
<g/>
,	,	kIx,
Respekt	respekt	k1gInSc1
<g/>
,	,	kIx,
20.4	20.4	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
Ruské	ruský	k2eAgInPc1d1
agenty	agens	k1gInPc1
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
pomohly	pomoct	k5eAaPmAgInP
odhalit	odhalit	k5eAaPmF
záběry	záběr	k1gInPc1
z	z	k7c2
kamer	kamera	k1gFnPc2
<g/>
,	,	kIx,
výbušné	výbušný	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
explodovalo	explodovat	k5eAaBmAgNnS
dřív	dříve	k6eAd2
<g/>
,	,	kIx,
ukázalo	ukázat	k5eAaPmAgNnS
vyšetřování	vyšetřování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VRBOVÁ	Vrbová	k1gFnSc1
<g/>
,	,	kIx,
Vendula	Vendula	k1gFnSc1
(	(	kIx(
<g/>
vvr	vvr	k?
<g/>
)	)	kIx)
<g/>
;	;	kIx,
MAGDOŇOVÁ	MAGDOŇOVÁ	kA
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
(	(	kIx(
<g/>
mag	mag	k?
<g/>
)	)	kIx)
<g/>
;	;	kIx,
ROHÁČKOVÁ	ROHÁČKOVÁ	kA
<g/>
,	,	kIx,
Kristina	Kristina	k1gFnSc1
(	(	kIx(
<g/>
kro	kra	k1gFnSc5
<g/>
)	)	kIx)
<g/>
;	;	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
výbuchu	výbuch	k1gInSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
byli	být	k5eAaImAgMnP
podle	podle	k7c2
zjištění	zjištění	k1gNnSc2
BIS	BIS	kA
zapojeni	zapojen	k2eAgMnPc1d1
příslušníci	příslušník	k1gMnPc1
ruské	ruský	k2eAgFnSc2d1
tajné	tajný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
,	,	kIx,
oznámil	oznámit	k5eAaPmAgMnS
Babiš	Babiš	k1gMnSc1
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2021-04-17	2021-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Third	Third	k1gMnSc1
Skripal	Skripal	k1gMnSc1
Suspect	Suspect	k1gMnSc1
Linked	Linked	k1gMnSc1
to	ten	k3xDgNnSc4
2015	#num#	k4
Bulgaria	Bulgarium	k1gNnSc2
Poisoning	Poisoning	k1gInSc1
<g/>
,	,	kIx,
Bellingcat	Bellingcat	k1gFnSc1
<g/>
,	,	kIx,
7.2	7.2	k4
<g/>
.2019	.2019	k4
<g/>
↑	↑	k?
Expremiér	expremiér	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
exkluzivně	exkluzivně	k6eAd1
o	o	k7c6
Vrběticích	Vrbětice	k1gFnPc6
<g/>
:	:	kIx,
Rusko	Rusko	k1gNnSc1
chtělo	chtít	k5eAaImAgNnS
destabilizovat	destabilizovat	k5eAaBmF
mou	můj	k3xOp1gFnSc4
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BLAŽEK	Blažek	k1gMnSc1
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proč	proč	k6eAd1
ruští	ruský	k2eAgMnPc1d1
agenti	agent	k1gMnPc1
zabíjeli	zabíjet	k5eAaImAgMnP
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
:	:	kIx,
aby	aby	kYmCp3nP
zabránili	zabránit	k5eAaPmAgMnP
dodávce	dodávka	k1gFnSc6
zbraní	zbraň	k1gFnPc2
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2021-04-17	2021-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Od	od	k7c2
nás	my	k3xPp1nPc2
nic	nic	k3yNnSc1
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
nešlo	jít	k5eNaImAgNnS
<g/>
,	,	kIx,
tváře	tvář	k1gFnPc1
agentů	agent	k1gMnPc2
neznáme	znát	k5eNaImIp1nP,k5eAaImIp1nP
<g/>
,	,	kIx,
říkají	říkat	k5eAaImIp3nP
majitelé	majitel	k1gMnPc1
skladu	sklad	k1gInSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hamáček	Hamáček	k1gMnSc1
<g/>
:	:	kIx,
Výbuchy	výbuch	k1gInPc1
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
zřejmě	zřejmě	k6eAd1
neměly	mít	k5eNaImAgInP
nastat	nastat	k5eAaPmF
na	na	k7c6
území	území	k1gNnSc6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
tisková	tiskový	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
5003	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Jan	Jan	k1gMnSc1
Hrbáček	Hrbáček	k1gMnSc1
<g/>
,	,	kIx,
Rusové	Rus	k1gMnPc1
„	„	k?
<g/>
popravili	popravit	k5eAaPmAgMnP
vlastního	vlastní	k2eAgNnSc2d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgMnSc1
ze	z	k7c2
zabitých	zabitý	k2eAgMnPc2d1
zbrojířů	zbrojíř	k1gMnPc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
byl	být	k5eAaImAgMnS
příslušníkem	příslušník	k1gMnSc7
StB	StB	k1gFnSc2
<g/>
,	,	kIx,
Ekonomický	ekonomický	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
,	,	kIx,
19.4	19.4	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
Boris	Boris	k1gMnSc1
Mitov	Mitov	k1gInSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Bedrov	Bedrovo	k1gNnPc2
<g/>
,	,	kIx,
Data	datum	k1gNnSc2
Shows	Showsa	k1gFnPc2
Alleged	Alleged	k1gMnSc1
Russian	Russian	k1gMnSc1
Agents	Agents	k1gInSc4
In	In	k1gFnSc2
Bulgaria	Bulgarium	k1gNnSc2
Around	Around	k1gMnSc1
Time	Tim	k1gFnSc2
Of	Of	k1gMnSc1
Arms-Depot	Arms-Depot	k1gMnSc1
Blasts	Blasts	k1gInSc4
<g/>
,	,	kIx,
RFE	RFE	kA
RL	RL	kA
<g/>
,	,	kIx,
22.4	22.4	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
Eliška	Eliška	k1gFnSc1
Kubátová	Kubátová	k1gFnSc1
<g/>
,	,	kIx,
Stopy	stopa	k1gFnPc1
ruských	ruský	k2eAgMnPc2d1
agentů	agent	k1gMnPc2
v	v	k7c6
Bulharsku	Bulharsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Data	datum	k1gNnPc1
naznačují	naznačovat	k5eAaImIp3nP
možné	možný	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
GRU	GRU	kA
s	s	k7c7
výbuchy	výbuch	k1gInPc7
v	v	k7c6
tamních	tamní	k2eAgInPc6d1
skladech	sklad	k1gInPc6
<g/>
,	,	kIx,
iROZHLAS	iROZHLAS	k?
<g/>
,	,	kIx,
23.4	23.4	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
Výbuch	výbuch	k1gInSc1
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
obrátil	obrátit	k5eAaPmAgMnS
pozornost	pozornost	k1gFnSc4
k	k	k7c3
Zemanově	Zemanův	k2eAgFnSc3d1
cestě	cesta	k1gFnSc3
do	do	k7c2
Tádžikistánu	Tádžikistán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Majitel	majitel	k1gMnSc1
Imexu	Imex	k1gInSc2
tam	tam	k6eAd1
s	s	k7c7
ním	on	k3xPp3gNnSc7
letěl	letět	k5eAaImAgMnS
<g/>
,	,	kIx,
Forum	forum	k1gNnSc1
24	#num#	k4
<g/>
,	,	kIx,
30.4	30.4	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
Janek	Janek	k1gMnSc1
Kroupa	Kroupa	k1gMnSc1
<g/>
,	,	kIx,
Kvůli	kvůli	k7c3
Vrběticím	Vrbětice	k1gFnPc3
se	se	k3xPyFc4
zkoumá	zkoumat	k5eAaImIp3nS
i	i	k9
Zemanova	Zemanův	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
do	do	k7c2
Střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
,	,	kIx,
Seznam	seznam	k1gInSc4
Zprávy	zpráva	k1gFnSc2
<g/>
,	,	kIx,
30.4	30.4	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
Reuters	Reuters	k1gInSc1
<g/>
;	;	kIx,
LOPATKA	lopatka	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czechs	Czechs	k1gInSc1
expel	expet	k5eAaBmAgInS,k5eAaImAgInS,k5eAaPmAgInS
18	#num#	k4
Russian	Russiany	k1gInPc2
envoys	envoys	k6eAd1
<g/>
,	,	kIx,
accuse	accuse	k6eAd1
Moscow	Moscow	k1gFnSc1
over	over	k1gInSc4
ammunition	ammunition	k1gInSc1
depot	depot	k1gNnSc1
blast	blast	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuters	Reuters	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuters	Reutersa	k1gFnPc2
<g/>
,	,	kIx,
2021-04-17	2021-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HOUSKA	houska	k1gFnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výbuch	výbuch	k1gInSc1
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
není	být	k5eNaImIp3nS
jen	jen	k9
český	český	k2eAgInSc1d1
problém	problém	k1gInSc1
–	–	k?
Česko	Česko	k1gNnSc1
už	už	k9
ruský	ruský	k2eAgInSc1d1
útok	útok	k1gInSc1
řeší	řešit	k5eAaImIp3nS
se	s	k7c7
spojenci	spojenec	k1gMnPc7
v	v	k7c6
NATO	NATO	kA
i	i	k9
v	v	k7c6
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2021-04-17	2021-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nejrychleji	rychle	k6eAd3
na	na	k7c6
Vrbětice	Vrbětika	k1gFnSc6
reagovali	reagovat	k5eAaBmAgMnP
Slováci	Slovák	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polsko	Polsko	k1gNnSc1
je	být	k5eAaImIp3nS
překvapivě	překvapivě	k6eAd1
stručné	stručný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
"	"	kIx"
<g/>
Rusko	Rusko	k1gNnSc1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
potrestat	potrestat	k5eAaPmF
<g/>
,	,	kIx,
stojíme	stát	k5eAaImIp1nP
při	při	k7c6
vás	vy	k3xPp2nPc6
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Spojenci	spojenec	k1gMnPc1
vyjadřují	vyjadřovat	k5eAaImIp3nP
Česku	Česko	k1gNnSc6
jasnou	jasný	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
<g/>
,	,	kIx,
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
18.4	18.4	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
NATO	NATO	kA
vyjádřilo	vyjádřit	k5eAaPmAgNnS
Česku	Česko	k1gNnSc6
podporu	podpor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
ním	on	k3xPp3gNnSc7
i	i	k9
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
,	,	kIx,
Británie	Británie	k1gFnSc1
nebo	nebo	k8xC
Polsko	Polsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jiří	Jiří	k1gMnSc1
Hošek	Hošek	k1gMnSc1
<g/>
,	,	kIx,
Šéf	šéf	k1gMnSc1
zahraničního	zahraniční	k2eAgInSc2d1
výboru	výbor	k1gInSc2
Dolní	dolní	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
:	:	kIx,
Pojďme	jít	k5eAaImRp1nP
vyhostit	vyhostit	k5eAaPmF
ruské	ruský	k2eAgMnPc4d1
velvyslance	velvyslanec	k1gMnPc4
<g/>
,	,	kIx,
Seznam	seznam	k1gInSc4
Zprávy	zpráva	k1gFnSc2
<g/>
,	,	kIx,
18.4	18.4	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
<g/>
:	:	kIx,
Země	zem	k1gFnSc2
EU	EU	kA
musí	muset	k5eAaImIp3nS
jasně	jasně	k6eAd1
a	a	k8xC
společně	společně	k6eAd1
odpovědět	odpovědět	k5eAaPmF
na	na	k7c4
Vrbětice	Vrbětice	k1gFnPc4
<g/>
,	,	kIx,
Forum	forum	k1gNnSc1
24	#num#	k4
<g/>
,	,	kIx,
21.4	21.4	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
Akt	akt	k1gInSc1
solidarity	solidarita	k1gFnSc2
<g/>
:	:	kIx,
Slovensko	Slovensko	k1gNnSc1
vyhostí	vyhostit	k5eAaPmIp3nS
tři	tři	k4xCgMnPc4
ruské	ruský	k2eAgMnPc4d1
diplomaty	diplomat	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Slovensko	Slovensko	k1gNnSc1
vyhostí	vyhostit	k5eAaPmIp3nS
tři	tři	k4xCgMnPc4
ruské	ruský	k2eAgMnPc4d1
diplomaty	diplomat	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
dle	dle	k7c2
Reuters	Reutersa	k1gFnPc2
odpoví	odpovědět	k5eAaPmIp3nS
stejným	stejný	k2eAgInSc7d1
počtem	počet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Baltské	baltský	k2eAgInPc1d1
státy	stát	k1gInPc1
vyhošťují	vyhošťovat	k5eAaImIp3nP
ruské	ruský	k2eAgMnPc4d1
diplomaty	diplomat	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyjadřují	vyjadřovat	k5eAaImIp3nP
tak	tak	k9
solidaritu	solidarita	k1gFnSc4
s	s	k7c7
Českem	Česko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pobaltské	pobaltský	k2eAgInPc1d1
státy	stát	k1gInPc1
vyhostí	vyhostit	k5eAaPmIp3nP
ze	z	k7c2
solidarity	solidarita	k1gFnSc2
s	s	k7c7
Českem	Česko	k1gNnSc7
diplomaty	diplomat	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
N	N	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
N	N	kA
Media	medium	k1gNnPc4
<g/>
,	,	kIx,
2021-04-23	2021-04-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
2571	#num#	k4
<g/>
-	-	kIx~
<g/>
1717	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Na	na	k7c4
českou	český	k2eAgFnSc4d1
výzvu	výzva	k1gFnSc4
k	k	k7c3
solidaritě	solidarita	k1gFnSc3
zareaguje	zareagovat	k5eAaPmIp3nS
i	i	k9
Rumunsko	Rumunsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Bukurešti	Bukurešť	k1gFnSc2
vyhostí	vyhostit	k5eAaPmIp3nS
ruského	ruský	k2eAgMnSc4d1
diplomata	diplomat	k1gMnSc4
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rusko	Rusko	k1gNnSc1
trestá	trestat	k5eAaImIp3nS
evropské	evropský	k2eAgInPc4d1
státy	stát	k1gInPc4
za	za	k7c4
solidaritu	solidarita	k1gFnSc4
s	s	k7c7
Českem	Česko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opustit	opustit	k5eAaPmF
Moskvu	Moskva	k1gFnSc4
musí	muset	k5eAaImIp3nS
osm	osm	k4xCc1
diplomatů	diplomat	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2021-04-27	2021-04-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Havlíček	Havlíček	k1gMnSc1
<g/>
:	:	kIx,
Rosatom	Rosatom	k1gInSc1
bude	být	k5eAaImBp3nS
vyřazen	vyřadit	k5eAaPmNgInS
z	z	k7c2
účasti	účast	k1gFnSc2
v	v	k7c6
tendru	tendr	k1gInSc6
na	na	k7c4
Dukovany	Dukovany	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tendr	tendr	k1gInSc1
na	na	k7c4
Dukovany	Dukovany	k1gInPc4
končí	končit	k5eAaImIp3nS
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
expert	expert	k1gMnSc1
na	na	k7c4
energetiku	energetika	k1gFnSc4
Šnobr	Šnobra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hamáček	Hamáček	k1gMnSc1
<g/>
:	:	kIx,
Cestou	cesta	k1gFnSc7
jsou	být	k5eAaImIp3nP
schválené	schválený	k2eAgFnPc1d1
vakcíny	vakcína	k1gFnPc1
<g/>
,	,	kIx,
debaty	debata	k1gFnPc1
o	o	k7c6
Sputniku	sputnik	k1gInSc6
nejsou	být	k5eNaImIp3nP
na	na	k7c6
stole	stol	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
tisková	tiskový	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
5003	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KLÍMOVÁ	Klímová	k1gFnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kauza	kauza	k1gFnSc1
Vrbětice	Vrbětice	k1gFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
úrovni	úroveň	k1gFnSc6
vojenského	vojenský	k2eAgNnSc2d1
napadení	napadení	k1gNnSc2
země	zem	k1gFnSc2
a	a	k8xC
státního	státní	k2eAgInSc2d1
terorismu	terorismus	k1gInSc2
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nP
bývalí	bývalý	k2eAgMnPc1d1
šéfové	šéf	k1gMnPc1
armády	armáda	k1gFnSc2
Šedivý	Šedivý	k1gMnSc1
a	a	k8xC
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2021-04-17	2021-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
útok	útok	k1gInSc1
na	na	k7c4
zboží	zboží	k1gNnSc4
<g/>
,	,	kIx,
ne	ne	k9
akt	akt	k1gInSc1
státního	státní	k2eAgInSc2d1
terorismu	terorismus	k1gInSc2
<g/>
,	,	kIx,
prohlásil	prohlásit	k5eAaPmAgMnS
Babiš	Babiš	k1gInSc4
k	k	k7c3
Vrběticím	Vrbětice	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Filip	Filip	k1gMnSc1
a	a	k8xC
Okamura	Okamura	k1gFnSc1
zjištěním	zjištění	k1gNnSc7
o	o	k7c6
Vrběticích	Vrbětice	k1gFnPc6
zatím	zatím	k6eAd1
nevěří	věřit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Atak	atak	k1gInSc1
Ruska	Rusko	k1gNnSc2
je	být	k5eAaImIp3nS
státní	státní	k2eAgInSc1d1
terorismus	terorismus	k1gInSc1
<g/>
,	,	kIx,
hřímá	hřímat	k5eAaImIp3nS
Vystrčil	Vystrčil	k1gMnSc1
s	s	k7c7
Fialou	Fiala	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chtějí	chtít	k5eAaImIp3nP
pro	pro	k7c4
Moskvu	Moskva	k1gFnSc4
tvrdší	tvrdý	k2eAgInSc4d2
trest	trest	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
Prima	primo	k1gNnSc2
NEWS	NEWS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FTV	FTV	kA
Prima	prima	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Žalobce	žalobce	k1gMnSc1
Zeman	Zeman	k1gMnSc1
<g/>
:	:	kIx,
Výbuchy	výbuch	k1gInPc1
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
neprověřujeme	prověřovat	k5eNaImIp1nP
jako	jako	k9
teroristický	teroristický	k2eAgInSc4d1
útok	útok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Klaus	Klaus	k1gMnSc1
<g/>
:	:	kIx,
Vrbětice	Vrbětice	k1gFnPc1
jsou	být	k5eAaImIp3nP
vykonstruovaný	vykonstruovaný	k2eAgInSc4d1
strašák	strašák	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
se	se	k3xPyFc4
ke	k	k7c3
zjištěním	zjištění	k1gNnPc3
o	o	k7c6
výbuchu	výbuch	k1gInSc6
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
vyjádří	vyjádřit	k5eAaPmIp3nS
až	až	k9
za	za	k7c4
týden	týden	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
opozice	opozice	k1gFnSc2
za	za	k7c4
to	ten	k3xDgNnSc4
sklidil	sklidit	k5eAaPmAgInS
kritiku	kritika	k1gFnSc4
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2021-04-18	2021-04-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tony	Tony	k1gMnSc1
Havlík	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Celostátní	celostátní	k2eAgFnPc1d1
akce	akce	k1gFnPc1
NCOZ	NCOZ	kA
proti	proti	k7c3
ruskému	ruský	k2eAgInSc3d1
vlivu	vliv	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policie	policie	k1gFnSc1
v	v	k7c6
noci	noc	k1gFnSc6
zatkla	zatknout	k5eAaPmAgFnS
české	český	k2eAgInPc4d1
členy	člen	k1gInPc4
polovojenských	polovojenský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
Forum	forum	k1gNnSc1
24	#num#	k4
<g/>
,	,	kIx,
21.4	21.4	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
Ultimátum	ultimátum	k1gNnSc1
pro	pro	k7c4
Moskvu	Moskva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
umožnit	umožnit	k5eAaPmF
návrat	návrat	k1gInSc4
českých	český	k2eAgMnPc2d1
diplomatů	diplomat	k1gMnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
budou	být	k5eAaImBp3nP
z	z	k7c2
ruské	ruský	k2eAgFnSc2d1
ambasády	ambasáda	k1gFnSc2
vyhoštěné	vyhoštěný	k2eAgFnSc2d1
další	další	k2eAgFnPc4d1
osoby	osoba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Kulhánek	Kulhánek	k1gMnSc1
<g/>
:	:	kIx,
Pokud	pokud	k8xS
Moskva	Moskva	k1gFnSc1
neumožní	umožnit	k5eNaPmIp3nS
návrat	návrat	k1gInSc4
českých	český	k2eAgMnPc2d1
diplomatů	diplomat	k1gMnPc2
<g/>
,	,	kIx,
vyhostíme	vyhostit	k5eAaPmIp1nP
další	další	k2eAgMnPc4d1
Rusy	Rus	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2021-04-21	2021-04-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Česko	Česko	k1gNnSc1
dalo	dát	k5eAaPmAgNnS
Rusku	Rusko	k1gNnSc3
ultimátum	ultimátum	k1gNnSc1
<g/>
,	,	kIx,
požaduje	požadovat	k5eAaImIp3nS
návrat	návrat	k1gInSc4
všech	všecek	k3xTgMnPc2
vyhoštěných	vyhoštěný	k2eAgMnPc2d1
diplomatů	diplomat	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kreml	Kreml	k1gInSc1
odmítá	odmítat	k5eAaImIp3nS
takový	takový	k3xDgInSc4
tón	tón	k1gInSc4
jednání	jednání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2021-04-21	2021-04-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
1385	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tvrdá	tvrdý	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
,	,	kIx,
Prahu	Praha	k1gFnSc4
musí	muset	k5eAaImIp3nP
opustit	opustit	k5eAaPmF
desítky	desítka	k1gFnPc4
ruských	ruský	k2eAgMnPc2d1
diplomatů	diplomat	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Česko	Česko	k1gNnSc1
zastropuje	zastropovat	k5eAaImIp3nS
počet	počet	k1gInSc1
lidí	člověk	k1gMnPc2
na	na	k7c6
ruském	ruský	k2eAgNnSc6d1
velvyslanectví	velvyslanectví	k1gNnSc6
podle	podle	k7c2
současného	současný	k2eAgInSc2d1
stavu	stav	k1gInSc2
na	na	k7c6
naší	náš	k3xOp1gFnSc6
ambasádě	ambasáda	k1gFnSc6
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Přepis	přepis	k1gInSc1
<g/>
:	:	kIx,
Projev	projev	k1gInSc1
prezidenta	prezident	k1gMnSc2
Zemana	Zeman	k1gMnSc2
ke	k	k7c3
kauze	kauza	k1gFnSc3
Vrbětice	Vrbětice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POLACZYKOVÁ	POLACZYKOVÁ	kA
<g/>
,	,	kIx,
Tereza	Tereza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistují	existovat	k5eNaImIp3nP
důkazy	důkaz	k1gInPc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
ruští	ruský	k2eAgMnPc1d1
agenti	agent	k1gMnPc1
byli	být	k5eAaImAgMnP
ve	v	k7c6
Vrběticících	Vrběticík	k1gInPc6
<g/>
,	,	kIx,
míní	mínit	k5eAaImIp3nS
prezident	prezident	k1gMnSc1
Zeman	Zeman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
VLTAVA	Vltava	k1gFnSc1
LABE	Labe	k1gNnSc2
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Chci	chtít	k5eAaImIp1nS
důkladně	důkladně	k6eAd1
vyšetřit	vyšetřit	k5eAaPmF
obě	dva	k4xCgFnPc4
verze	verze	k1gFnPc4
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
k	k	k7c3
Vrběticím	Vrbětice	k1gFnPc3
prezident	prezident	k1gMnSc1
Zeman	Zeman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
kritiků	kritik	k1gMnPc2
relativizuje	relativizovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nesmysl	nesmysl	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
si	se	k3xPyFc3
Češi	Čech	k1gMnPc1
vycucali	vycucat	k5eAaPmAgMnP
z	z	k7c2
prstu	prst	k1gInSc2
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
vlivný	vlivný	k2eAgMnSc1d1
ruský	ruský	k2eAgMnSc1d1
poslanec	poslanec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absurdní	absurdní	k2eAgInSc1d1
<g/>
,	,	kIx,
Češi	Čech	k1gMnPc1
asi	asi	k9
chtějí	chtít	k5eAaImIp3nP
zavřít	zavřít	k5eAaPmF
ambasádu	ambasáda	k1gFnSc4
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
,	,	kIx,
reagují	reagovat	k5eAaBmIp3nP
Rusové	Rus	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
<g/>
,	,	kIx,
2021-04-17	2021-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rusko	Rusko	k1gNnSc1
vyhostí	vyhostit	k5eAaPmIp3nS
dvacet	dvacet	k4xCc4
zaměstnanců	zaměstnanec	k1gMnPc2
české	český	k2eAgFnSc2d1
ambasády	ambasáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemi	zem	k1gFnSc3
musí	muset	k5eAaImIp3nP
opustit	opustit	k5eAaPmF
do	do	k7c2
pondělí	pondělí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rusko	Rusko	k1gNnSc1
musí	muset	k5eAaImIp3nS
do	do	k7c2
pondělní	pondělní	k2eAgFnSc2d1
půlnoci	půlnoc	k1gFnSc2
opustit	opustit	k5eAaPmF
20	#num#	k4
českých	český	k2eAgMnPc2d1
diplomatů	diplomat	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velvyslanec	velvyslanec	k1gMnSc1
Pivoňka	Pivoňka	k1gMnSc1
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
zůstane	zůstat	k5eAaPmIp3nS
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Šéf	šéf	k1gMnSc1
ruské	ruský	k2eAgFnSc2d1
civilní	civilní	k2eAgFnSc2d1
rozvědky	rozvědka	k1gFnSc2
<g/>
:	:	kIx,
Obvinění	obvinění	k1gNnSc1
z	z	k7c2
výbuchu	výbuch	k1gInSc2
ve	v	k7c6
Vrběticích	Vrbětice	k1gFnPc6
je	být	k5eAaImIp3nS
lež	lež	k1gFnSc1
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
lidi	člověk	k1gMnPc4
s	s	k7c7
nízkou	nízký	k2eAgFnSc7d1
inteligencí	inteligence	k1gFnSc7
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Šéf	šéf	k1gMnSc1
ruské	ruský	k2eAgFnSc2d1
rozvědky	rozvědka	k1gFnSc2
<g/>
:	:	kIx,
České	český	k2eAgNnSc1d1
obvinění	obvinění	k1gNnSc1
z	z	k7c2
explozí	exploze	k1gFnPc2
je	být	k5eAaImIp3nS
ubohá	ubohý	k2eAgFnSc1d1
lež	lež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
„	„	k?
<g/>
Ubohá	ubohý	k2eAgFnSc1d1
a	a	k8xC
nízká	nízký	k2eAgFnSc1d1
lež	lež	k1gFnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
Šéf	šéf	k1gMnSc1
ruské	ruský	k2eAgFnSc2d1
rozvědky	rozvědka	k1gFnSc2
Sergej	Sergej	k1gMnSc1
Naryškin	Naryškin	k1gMnSc1
odmítá	odmítat	k5eAaImIp3nS
ruskou	ruský	k2eAgFnSc4d1
účast	účast	k1gFnSc4
na	na	k7c6
Vrběticích	Vrbětice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
N	N	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
N	N	kA
Media	medium	k1gNnPc4
<g/>
,	,	kIx,
2021-04-23	2021-04-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
2571	#num#	k4
<g/>
-	-	kIx~
<g/>
1717	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kauza	kauza	k1gFnSc1
agenta	agent	k1gMnSc2
s	s	k7c7
ricinem	ricin	k1gInSc7
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1
<g/>
:	:	kIx,
Zemanovi	Zemanův	k2eAgMnPc1d1
muži	muž	k1gMnPc1
pomáhali	pomáhat	k5eAaImAgMnP
zbrojaři	zbrojař	k1gMnPc1
z	z	k7c2
Vrbětic	Vrbětice	k1gFnPc2
vyvážet	vyvážet	k5eAaImF
zbraně	zbraň	k1gFnPc4
na	na	k7c4
Lidovky	Lidovky	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
</s>
<s>
Vrbětická	Vrbětický	k2eAgFnSc1d1
exploze	exploze	k1gFnSc1
<g/>
:	:	kIx,
od	od	k7c2
tun	tuna	k1gFnPc2
munice	munice	k1gFnSc2
přes	přes	k7c4
2	#num#	k4
mrtvé	mrtvý	k2eAgInPc4d1
po	po	k7c4
soudy	soud	k1gInPc4
a	a	k8xC
ruské	ruský	k2eAgMnPc4d1
agenty	agent	k1gMnPc4
–	–	k?
rekapitulace	rekapitulace	k1gFnSc1
událostí	událost	k1gFnPc2
na	na	k7c6
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
</s>
