<s>
Frazém	frazém	k1gInSc1	frazém
neboli	neboli	k8xC	neboli
frazeologismus	frazeologismus	k1gInSc1	frazeologismus
je	být	k5eAaImIp3nS	být
ustálené	ustálený	k2eAgNnSc4d1	ustálené
spojení	spojení	k1gNnSc4	spojení
slovních	slovní	k2eAgInPc2d1	slovní
tvarů	tvar	k1gInPc2	tvar
slov	slovo	k1gNnPc2	slovo
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
významem	význam	k1gInSc7	význam
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c6	o
sousloví	sousloví	k1gNnSc6	sousloví
<g/>
,	,	kIx,	,
rčení	rčení	k1gNnSc1	rčení
<g/>
,	,	kIx,	,
pořekadlo	pořekadlo	k1gNnSc1	pořekadlo
<g/>
,	,	kIx,	,
přirovnání	přirovnání	k1gNnSc2	přirovnání
či	či	k8xC	či
přísloví	přísloví	k1gNnSc2	přísloví
apod.	apod.	kA	apod.
Frazeologismy	frazeologismus	k1gInPc1	frazeologismus
většinou	většinou	k6eAd1	většinou
výrazně	výrazně	k6eAd1	výrazně
posunují	posunovat	k5eAaImIp3nP	posunovat
původní	původní	k2eAgInSc4d1	původní
význam	význam	k1gInSc4	význam
slov	slovo	k1gNnPc2	slovo
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
zcela	zcela	k6eAd1	zcela
jinému	jiný	k2eAgInSc3d1	jiný
smyslu	smysl	k1gInSc3	smysl
sdělení	sdělení	k1gNnSc2	sdělení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vymezení	vymezení	k1gNnSc2	vymezení
typů	typ	k1gInPc2	typ
jsou	být	k5eAaImIp3nP	být
frazémy	frazém	k1gInPc7	frazém
podle	podle	k7c2	podle
sémantických	sémantický	k2eAgNnPc2d1	sémantické
kritérií	kritérion	k1gNnPc2	kritérion
pojmenování	pojmenování	k1gNnSc2	pojmenování
pro	pro	k7c4	pro
typ	typ	k1gInSc4	typ
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
výrazné	výrazný	k2eAgFnSc3d1	výrazná
desemantizaci	desemantizace	k1gFnSc3	desemantizace
komponent	komponenta	k1gFnPc2	komponenta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
frazeologizovanost	frazeologizovanost	k1gFnSc1	frazeologizovanost
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c4	na
ustálenosti	ustálenost	k1gFnPc4	ustálenost
používání	používání	k1gNnSc1	používání
a	a	k8xC	a
složení	složení	k1gNnSc1	složení
takového	takový	k3xDgInSc2	takový
výrazu	výraz	k1gInSc2	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
typ	typ	k1gInSc1	typ
volnější	volný	k2eAgFnSc4d2	volnější
než	než	k8xS	než
frazeologická	frazeologický	k2eAgNnPc1d1	frazeologické
spojení	spojení	k1gNnPc1	spojení
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
typ	typ	k1gInSc1	typ
stojící	stojící	k2eAgMnSc1d1	stojící
na	na	k7c6	na
samé	samý	k3xTgFnSc6	samý
hranici	hranice	k1gFnSc6	hranice
frazeologie	frazeologie	k1gFnSc2	frazeologie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Idiom	idiom	k1gInSc1	idiom
<g/>
.	.	kIx.	.
</s>
<s>
Idiom	idiom	k1gInSc1	idiom
(	(	kIx(	(
<g/>
také	také	k9	také
idiomatické	idiomatický	k2eAgNnSc1d1	idiomatické
spojení	spojení	k1gNnSc1	spojení
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
chápe	chápat	k5eAaImIp3nS	chápat
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
jako	jako	k8xC	jako
synonymum	synonymum	k1gNnSc4	synonymum
k	k	k7c3	k
výrazu	výraz	k1gInSc3	výraz
frazém	frazém	k1gInSc4	frazém
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
označení	označení	k1gNnSc1	označení
frazém	frazém	k1gInSc1	frazém
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
formální	formální	k2eAgFnSc4d1	formální
(	(	kIx(	(
<g/>
morfosyntaktickou	morfosyntaktický	k2eAgFnSc4d1	morfosyntaktický
<g/>
)	)	kIx)	)
a	a	k8xC	a
idiom	idiom	k1gInSc4	idiom
sémantickou	sémantický	k2eAgFnSc4d1	sémantická
(	(	kIx(	(
<g/>
významovou	významový	k2eAgFnSc4d1	významová
<g/>
)	)	kIx)	)
stránku	stránka	k1gFnSc4	stránka
téhož	týž	k3xTgInSc2	týž
výrazu	výraz	k1gInSc2	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
definice	definice	k1gFnSc1	definice
idiomu	idiom	k1gInSc2	idiom
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc1	výraz
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
smysl	smysl	k1gInSc4	smysl
není	být	k5eNaImIp3nS	být
odvoditelný	odvoditelný	k2eAgMnSc1d1	odvoditelný
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
chytat	chytat	k5eAaImF	chytat
lelky	lelek	k1gInPc4	lelek
-	-	kIx~	-
lenošit	lenošit	k5eAaImF	lenošit
mít	mít	k5eAaImF	mít
nakoupíno	nakoupína	k1gFnSc5	nakoupína
/	/	kIx~	/
mít	mít	k5eAaImF	mít
(	(	kIx(	(
<g/>
v	v	k7c6	v
<g/>
)	)	kIx)	)
<g/>
opici	opice	k1gFnSc6	opice
<g/>
,	,	kIx,	,
draka	drak	k1gMnSc4	drak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
z	z	k7c2	z
praku	prak	k1gInSc2	prak
(	(	kIx(	(
<g/>
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
-	-	kIx~	-
být	být	k5eAaImF	být
opilý	opilý	k2eAgMnSc1d1	opilý
pěšky	pěšky	k6eAd1	pěšky
jako	jako	k8xS	jako
za	za	k7c7	za
vozem	vůz	k1gInSc7	vůz
-	-	kIx~	-
nastejno	nastejno	k6eAd1	nastejno
dostat	dostat	k5eAaPmF	dostat
čočku	čočka	k1gFnSc4	čočka
–	–	k?	–
dostat	dostat	k5eAaPmF	dostat
vynadáno	vynadán	k2eAgNnSc1d1	vynadáno
<g/>
/	/	kIx~	/
<g/>
namláceno	namlácen	k2eAgNnSc1d1	namláceno
dělat	dělat	k5eAaImF	dělat
Zagorku	Zagorka	k1gFnSc4	Zagorka
–	–	k?	–
vymlouvat	vymlouvat	k5eAaImF	vymlouvat
se	se	k3xPyFc4	se
<g/>
/	/	kIx~	/
<g/>
dělat	dělat	k5eAaImF	dělat
drahoty	drahota	k1gFnSc2	drahota
oxidovat	oxidovat	k5eAaBmF	oxidovat
–	–	k?	–
obtěžovat	obtěžovat	k5eAaImF	obtěžovat
polykat	polykat	k5eAaImF	polykat
andělíčky	andělíček	k1gMnPc4	andělíček
-	-	kIx~	-
topit	topit	k5eAaImF	topit
se	se	k3xPyFc4	se
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Frazeologie	frazeologie	k1gFnSc2	frazeologie
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
frazeologie	frazeologie	k1gFnSc2	frazeologie
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
významy	význam	k1gInPc4	význam
<g/>
:	:	kIx,	:
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
soubor	soubor	k1gInSc4	soubor
všech	všecek	k3xTgInPc2	všecek
frazeologismů	frazeologismus	k1gInPc2	frazeologismus
daného	daný	k2eAgInSc2d1	daný
jazyka	jazyk	k1gInSc2	jazyk
nebo	nebo	k8xC	nebo
také	také	k9	také
o	o	k7c4	o
nauku	nauka	k1gFnSc4	nauka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
frazeologismy	frazeologismus	k1gInPc4	frazeologismus
daného	daný	k2eAgInSc2d1	daný
jazyka	jazyk	k1gInSc2	jazyk
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
frazeologismy	frazeologismus	k1gInPc1	frazeologismus
mohou	moct	k5eAaImIp3nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
frazeologickou	frazeologický	k2eAgFnSc4d1	frazeologická
homonymii	homonymie	k1gFnSc4	homonymie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
dvojznačnost	dvojznačnost	k1gFnSc1	dvojznačnost
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
nejednoznačnost	nejednoznačnost	k1gFnSc4	nejednoznačnost
sdělení	sdělení	k1gNnSc3	sdělení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
nutno	nutno	k6eAd1	nutno
podle	podle	k7c2	podle
celkového	celkový	k2eAgInSc2d1	celkový
kontextu	kontext	k1gInSc2	kontext
sdělení	sdělení	k1gNnSc2	sdělení
(	(	kIx(	(
<g/>
věty	věta	k1gFnSc2	věta
<g/>
)	)	kIx)	)
usuzovat	usuzovat	k5eAaImF	usuzovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
přímý	přímý	k2eAgInSc4d1	přímý
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc4d1	původní
<g/>
)	)	kIx)	)
význam	význam	k1gInSc4	význam
sousloví	sousloví	k1gNnSc2	sousloví
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jeho	jeho	k3xOp3gInSc4	jeho
přenesený	přenesený	k2eAgInSc4d1	přenesený
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
skutečný	skutečný	k2eAgInSc4d1	skutečný
frazeologismus	frazeologismus	k1gInSc4	frazeologismus
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
frazeologické	frazeologický	k2eAgFnSc2d1	frazeologická
homonymie	homonymie	k1gFnSc2	homonymie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
sousloví	sousloví	k1gNnSc4	sousloví
"	"	kIx"	"
<g/>
lízat	lízat	k5eAaImF	lízat
si	se	k3xPyFc3	se
rány	rána	k1gFnPc4	rána
<g/>
"	"	kIx"	"
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
jak	jak	k6eAd1	jak
doslovný	doslovný	k2eAgInSc4d1	doslovný
význam	význam	k1gInSc4	význam
sdělení	sdělení	k1gNnSc2	sdělení
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
kocour	kocour	k1gMnSc1	kocour
si	se	k3xPyFc3	se
líže	lízat	k5eAaImIp3nS	lízat
rány	rána	k1gFnPc4	rána
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
mu	on	k3xPp3gMnSc3	on
způsobil	způsobit	k5eAaPmAgMnS	způsobit
sousedův	sousedův	k2eAgMnSc1d1	sousedův
pes	pes	k1gMnSc1	pes
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
o	o	k7c4	o
frazeologismus	frazeologismus	k1gInSc4	frazeologismus
popisující	popisující	k2eAgFnSc4d1	popisující
např.	např.	kA	např.
náladu	nálada	k1gFnSc4	nálada
sportovního	sportovní	k2eAgInSc2d1	sportovní
týmu	tým	k1gInSc2	tým
po	po	k7c6	po
těžké	těžký	k2eAgFnSc6d1	těžká
porážce	porážka	k1gFnSc6	porážka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oborech	obor	k1gInPc6	obor
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
profesní	profesní	k2eAgFnPc1d1	profesní
hantýrky	hantýrka	k1gFnPc1	hantýrka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
frazeologismů	frazeologismus	k1gInPc2	frazeologismus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
mimo	mimo	k7c4	mimo
kontext	kontext	k1gInSc4	kontext
daného	daný	k2eAgInSc2d1	daný
oboru	obor	k1gInSc2	obor
nesrozumitelné	srozumitelný	k2eNgFnPc1d1	nesrozumitelná
<g/>
.	.	kIx.	.
</s>
