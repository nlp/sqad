<s>
CEPT	CEPT	kA
<g/>
,	,	kIx,
Konference	konference	k1gFnSc1
evropských	evropský	k2eAgFnPc2d1
správ	správa	k1gFnPc2
pošt	pošta	k1gFnPc2
a	a	k8xC
telekomunikací	telekomunikace	k1gFnPc2
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
Conférence	Conférence	k1gFnSc1
européenne	européennout	k5eAaPmIp3nS,k5eAaImIp3nS
des	des	k1gNnSc7
administrations	administrationsa	k1gFnPc2
des	des	k1gNnSc2
postes	postes	k1gMnSc1
et	et	k?
des	des	k1gNnPc2
télécommunications	télécommunications	k1gInSc1
<g/>
;	;	kIx,
anglicky	anglicky	k6eAd1
European	European	k1gMnSc1
Conference	Conference	k1gFnSc2
of	of	k?
Postal	Postal	k1gMnSc1
and	and	k?
Telecommunications	Telecommunications	k1gInSc1
Administrations	Administrations	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
koordinační	koordinační	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
evropských	evropský	k2eAgFnPc2d1
státních	státní	k2eAgFnPc2d1
telekomunikačních	telekomunikační	k2eAgFnPc2d1
a	a	k8xC
poštovních	poštovní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
založená	založený	k2eAgFnSc1d1
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1959	#num#	k4
<g/>
.	.	kIx.
</s>