<s>
CEPT	CEPT	kA
</s>
<s>
CEPT	CEPT	kA
<g/>
,	,	kIx,
Konference	konference	k1gFnSc1
evropských	evropský	k2eAgFnPc2d1
správ	správa	k1gFnPc2
pošt	pošta	k1gFnPc2
a	a	k8xC
telekomunikací	telekomunikace	k1gFnPc2
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
Conférence	Conférence	k1gFnSc1
européenne	européennout	k5eAaPmIp3nS,k5eAaImIp3nS
des	des	k1gNnSc7
administrations	administrationsa	k1gFnPc2
des	des	k1gNnSc2
postes	postes	k1gMnSc1
et	et	k?
des	des	k1gNnPc2
télécommunications	télécommunications	k1gInSc1
<g/>
;	;	kIx,
anglicky	anglicky	k6eAd1
European	European	k1gMnSc1
Conference	Conference	k1gFnSc2
of	of	k?
Postal	Postal	k1gMnSc1
and	and	k?
Telecommunications	Telecommunications	k1gInSc1
Administrations	Administrations	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
koordinační	koordinační	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
evropských	evropský	k2eAgFnPc2d1
státních	státní	k2eAgFnPc2d1
telekomunikačních	telekomunikační	k2eAgFnPc2d1
a	a	k8xC
poštovních	poštovní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
založená	založený	k2eAgFnSc1d1
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1959	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
CEPT	CEPT	kA
založila	založit	k5eAaPmAgFnS
Evropský	evropský	k2eAgInSc4d1
ústav	ústav	k1gInSc4
pro	pro	k7c4
telekomunikační	telekomunikační	k2eAgFnPc4d1
normy	norma	k1gFnPc4
(	(	kIx(
<g/>
ETSI	ETSI	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
vytvořil	vytvořit	k5eAaPmAgMnS
standard	standard	k1gInSc4
GSM	GSM	kA
pro	pro	k7c4
mobilní	mobilní	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
CEPT	CEPT	kA
má	mít	k5eAaImIp3nS
tři	tři	k4xCgFnPc4
hlavní	hlavní	k2eAgFnPc4d1
složky	složka	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
ECC	ECC	kA
(	(	kIx(
<g/>
Electronic	Electronice	k1gFnPc2
Communications	Communications	k1gInSc1
Committee	Committe	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
zodpovědné	zodpovědný	k2eAgFnPc4d1
za	za	k7c4
oblast	oblast	k1gFnSc4
radiokomunikací	radiokomunikace	k1gFnPc2
a	a	k8xC
telekomunikací	telekomunikace	k1gFnPc2
<g/>
;	;	kIx,
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
září	září	k1gNnSc6
2001	#num#	k4
sloučením	sloučení	k1gNnSc7
ECTRA	ECTRA	kA
a	a	k8xC
ERC	ERC	kA
(	(	kIx(
<g/>
European	Europeana	k1gFnPc2
Radiocommunications	Radiocommunications	k1gInSc1
Committee	Committe	k1gInSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stálý	stálý	k2eAgInSc1d1
sekretariát	sekretariát	k1gInSc1
ECC	ECC	kA
je	být	k5eAaImIp3nS
ECO	ECO	kA
(	(	kIx(
<g/>
European	European	k1gMnSc1
Communications	Communicationsa	k1gFnPc2
Office	Office	kA
<g/>
)	)	kIx)
</s>
<s>
CERP	CERP	kA
(	(	kIx(
<g/>
European	European	k1gMnSc1
Committee	Committe	k1gFnSc2
for	forum	k1gNnPc2
Postal	Postal	k1gMnSc1
Regulation	Regulation	k1gInSc1
francouzsky	francouzsky	k6eAd1
Comité	Comita	k1gMnPc1
européen	européna	k1gFnPc2
des	des	k1gNnSc2
régulateurs	régulateurs	k6eAd1
postaux	postaux	k1gInSc4
<g/>
)	)	kIx)
–	–	k?
zodpovědné	zodpovědný	k2eAgFnPc4d1
za	za	k7c4
oblast	oblast	k1gFnSc4
pošt	pošta	k1gFnPc2
</s>
<s>
Com-ITU	Com-ITU	k?
(	(	kIx(
<g/>
Committee	Committee	k1gNnSc1
for	forum	k1gNnPc2
ITU	ITU	kA
Policy	Polica	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
zodpovědné	zodpovědný	k2eAgFnPc4d1
za	za	k7c4
organizování	organizování	k1gNnPc4
a	a	k8xC
koordinaci	koordinace	k1gFnSc4
CEPT	CEPT	kA
aktivit	aktivita	k1gFnPc2
při	při	k7c6
přípravě	příprava	k1gFnSc6
zasedání	zasedání	k1gNnSc2
ITU	ITU	kA
–	–	k?
zasedání	zasedání	k1gNnSc2
Rady	rada	k1gFnSc2
ITU	ITU	kA
<g/>
,	,	kIx,
Plenipotentiary	Plenipotentiara	k1gFnSc2
konference	konference	k1gFnSc2
<g/>
,	,	kIx,
konference	konference	k1gFnSc2
World	Worlda	k1gFnPc2
Telecommunication	Telecommunication	k1gInSc1
Development	Development	k1gMnSc1
a	a	k8xC
shromáždění	shromáždění	k1gNnSc1
World	World	k1gInSc1
Telecommunication	Telecommunication	k1gInSc1
Standardisation	Standardisation	k1gInSc4
Assembly	Assembly	k1gFnSc2
</s>
<s>
Každá	každý	k3xTgFnSc1
ze	z	k7c2
složek	složka	k1gFnPc2
má	mít	k5eAaImIp3nS
svého	svůj	k3xOyFgMnSc4
předsedu	předseda	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trojice	trojice	k1gFnSc1
předsedů	předseda	k1gMnPc2
tvoří	tvořit	k5eAaImIp3nS
vedení	vedení	k1gNnSc4
CEPT	CEPT	kA
<g/>
.	.	kIx.
</s>
<s>
Členské	členský	k2eAgFnPc1d1
země	zem	k1gFnPc1
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
2018	#num#	k4
mělo	mít	k5eAaImAgNnS
CEPT	CEPT	kA
48	#num#	k4
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Albánie	Albánie	k1gFnSc1
<g/>
,	,	kIx,
Andorra	Andorra	k1gFnSc1
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
Ázerbájdžán	Ázerbájdžán	k1gInSc1
<g/>
,	,	kIx,
Bělorusko	Bělorusko	k1gNnSc1
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc1
<g/>
,	,	kIx,
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
<g/>
,	,	kIx,
Kypr	Kypr	k1gInSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
<g/>
,	,	kIx,
Dánsko	Dánsko	k1gNnSc1
<g/>
,	,	kIx,
Estonsko	Estonsko	k1gNnSc1
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Řecko	Řecko	k1gNnSc1
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
<g/>
,	,	kIx,
Irsko	Irsko	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
<g/>
,	,	kIx,
Litva	Litva	k1gFnSc1
<g/>
,	,	kIx,
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
<g/>
,	,	kIx,
Lotyšsko	Lotyšsko	k1gNnSc1
<g/>
,	,	kIx,
Lucembursko	Lucembursko	k1gNnSc1
<g/>
,	,	kIx,
Malta	Malta	k1gFnSc1
<g/>
,	,	kIx,
Moldavsko	Moldavsko	k1gNnSc1
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc1
<g/>
,	,	kIx,
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
,	,	kIx,
Portugalsko	Portugalsko	k1gNnSc1
<g/>
,	,	kIx,
Rumunsko	Rumunsko	k1gNnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
,	,	kIx,
San	San	k1gFnSc1
Marino	Marina	k1gFnSc5
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
<g/>
,	,	kIx,
Srbsko	Srbsko	k1gNnSc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
<g/>
,	,	kIx,
Slovinsko	Slovinsko	k1gNnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
<g/>
,	,	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
<g/>
,	,	kIx,
Ukrajina	Ukrajina	k1gFnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
Vatikán	Vatikán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
International	Internationat	k5eAaImAgInS,k5eAaPmAgInS
Telecommunication	Telecommunication	k1gInSc1
Union	union	k1gInSc1
</s>
<s>
Světová	světový	k2eAgFnSc1d1
poštovní	poštovní	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
CEPT	CEPT	kA
doporučení	doporučení	k1gNnPc2
T	T	kA
<g/>
/	/	kIx~
<g/>
CD	CD	kA
06-01	06-01	k4
(	(	kIx(
<g/>
standard	standard	k1gInSc4
pro	pro	k7c4
videotex	videotex	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
E1	E1	k4
(	(	kIx(
<g/>
standard	standard	k1gInSc1
pro	pro	k7c4
multiplexované	multiplexovaný	k2eAgInPc4d1
telefonní	telefonní	k2eAgInPc4d1
okruhy	okruh	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
WiMAX	WiMAX	k?
</s>
<s>
PMR446	PMR446	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.cept.org	www.cept.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
European	Europeana	k1gFnPc2
Conference	Conference	k1gFnSc2
of	of	k?
Postal	Postal	k1gMnSc1
and	and	k?
Telecommunications	Telecommunications	k1gInSc1
Administrations	Administrations	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CEPT	CEPT	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
CEPT	CEPT	kA
</s>
<s>
ECC	ECC	kA
</s>
<s>
ECO	ECO	kA
</s>
<s>
CERP	CERP	kA
</s>
<s>
Com-ITU	Com-ITU	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
804596-3	804596-3	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0661	#num#	k4
8037	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
88657946	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
132135008	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
88657946	#num#	k4
</s>
