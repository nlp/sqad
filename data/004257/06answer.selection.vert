<s>
Nová	nový	k2eAgFnSc1d1	nová
hlavní	hlavní	k2eAgFnSc1d1	hlavní
budova	budova	k1gFnSc1	budova
v	v	k7c6	v
secesním	secesní	k2eAgInSc6d1	secesní
slohu	sloh	k1gInSc6	sloh
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	let	k1gInPc6	let
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
podle	podle	k7c2	podle
vítězného	vítězný	k2eAgInSc2d1	vítězný
návrhu	návrh	k1gInSc2	návrh
Josefa	Josef	k1gMnSc2	Josef
Fanty	Fanta	k1gMnSc2	Fanta
na	na	k7c6	na
základě	základ	k1gInSc6	základ
architektonické	architektonický	k2eAgFnSc2d1	architektonická
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
