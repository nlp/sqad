<s>
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Neutitschein	Neutitschein	k2eAgInSc1d1	Neutitschein
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
34	[number]	k4	34
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Ostravy	Ostrava	k1gFnSc2	Ostrava
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Jičínce	Jičínka	k1gFnSc6	Jičínka
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
téměř	téměř	k6eAd1	téměř
24	[number]	k4	24
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
především	především	k6eAd1	především
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
čtvercovým	čtvercový	k2eAgNnSc7d1	čtvercové
náměstím	náměstí	k1gNnSc7	náměstí
s	s	k7c7	s
podloubím	podloubí	k1gNnSc7	podloubí
a	a	k8xC	a
historickým	historický	k2eAgInSc7d1	historický
renesančním	renesanční	k2eAgInSc7d1	renesanční
domem	dům	k1gInSc7	dům
Stará	starý	k2eAgFnSc1d1	stará
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
Žerotínským	žerotínský	k2eAgInSc7d1	žerotínský
zámkem	zámek	k1gInSc7	zámek
a	a	k8xC	a
bohatou	bohatý	k2eAgFnSc7d1	bohatá
česko-německou	českoěmecký	k2eAgFnSc7d1	česko-německá
historií	historie	k1gFnSc7	historie
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
také	také	k9	také
Muzeum	muzeum	k1gNnSc1	muzeum
Novojičínska	Novojičínsko	k1gNnSc2	Novojičínsko
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlášen	k2eAgInSc1d1	vyhlášen
Historickým	historický	k2eAgNnSc7d1	historické
městem	město	k1gNnSc7	město
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
pak	pak	k6eAd1	pak
novojičínské	novojičínský	k2eAgNnSc1d1	Novojičínské
náměstí	náměstí	k1gNnSc1	náměstí
získalo	získat	k5eAaPmAgNnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
"	"	kIx"	"
<g/>
Nejkrásnější	krásný	k2eAgNnSc4d3	nejkrásnější
náměstí	náměstí	k1gNnSc4	náměstí
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Novojičínské	novojičínský	k2eAgNnSc1d1	Novojičínské
vlakové	vlakový	k2eAgNnSc1d1	vlakové
nádraží	nádraží	k1gNnSc1	nádraží
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
získalo	získat	k5eAaPmAgNnS	získat
ocenění	ocenění	k1gNnSc1	ocenění
"	"	kIx"	"
<g/>
Nejkrásnější	krásný	k2eAgNnSc1d3	nejkrásnější
nádraží	nádraží	k1gNnSc1	nádraží
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
známé	známý	k2eAgFnSc2d1	známá
firmy	firma	k1gFnSc2	firma
Tonak	tonak	k1gInSc4	tonak
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
zde	zde	k6eAd1	zde
slavný	slavný	k2eAgMnSc1d1	slavný
generál	generál	k1gMnSc1	generál
Ernst	Ernst	k1gMnSc1	Ernst
Gideon	Gideon	k1gMnSc1	Gideon
von	von	k1gInSc4	von
Laudon	Laudon	k1gMnSc1	Laudon
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Ruka	ruka	k1gFnSc1	ruka
vystupující	vystupující	k2eAgFnSc1d1	vystupující
z	z	k7c2	z
oblaku	oblak	k1gInSc2	oblak
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
drží	držet	k5eAaImIp3nS	držet
polovinu	polovina	k1gFnSc4	polovina
zavinuté	zavinutý	k2eAgFnSc2d1	zavinutá
střely	střela	k1gFnSc2	střela
v	v	k7c6	v
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
zavinutá	zavinutý	k2eAgFnSc1d1	zavinutá
střela	střela	k1gFnSc1	střela
v	v	k7c6	v
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
byla	být	k5eAaImAgFnS	být
znakem	znak	k1gInSc7	znak
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
,	,	kIx,	,
městský	městský	k2eAgInSc1d1	městský
znak	znak	k1gInSc1	znak
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
využívá	využívat	k5eAaImIp3nS	využívat
část	část	k1gFnSc1	část
této	tento	k3xDgFnSc2	tento
figury	figura	k1gFnSc2	figura
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
podobu	podoba	k1gFnSc4	podoba
znaku	znak	k1gInSc2	znak
máme	mít	k5eAaImIp1nP	mít
vyobrazenou	vyobrazený	k2eAgFnSc4d1	vyobrazená
na	na	k7c4	na
nejstarší	starý	k2eAgFnPc4d3	nejstarší
zachované	zachovaný	k2eAgFnPc4d1	zachovaná
pečeti	pečeť	k1gFnPc4	pečeť
města	město	k1gNnSc2	město
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1398	[number]	k4	1398
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
názvu	název	k1gInSc6	název
města	město	k1gNnSc2	město
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
pověstí	pověst	k1gFnPc2	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
pověstí	pověst	k1gFnPc2	pověst
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
podle	podle	k7c2	podle
udatné	udatný	k2eAgFnSc2d1	udatná
dcery	dcera	k1gFnSc2	dcera
majitele	majitel	k1gMnSc2	majitel
starojického	starojický	k2eAgInSc2d1	starojický
hradu	hrad	k1gInSc2	hrad
Jitky	Jitka	k1gFnSc2	Jitka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
záchraně	záchrana	k1gFnSc3	záchrana
malého	malý	k2eAgMnSc2d1	malý
chlapce	chlapec	k1gMnSc2	chlapec
pustila	pustit	k5eAaPmAgFnS	pustit
do	do	k7c2	do
boje	boj	k1gInSc2	boj
s	s	k7c7	s
medvědem	medvěd	k1gMnSc7	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rozzuřeným	rozzuřený	k2eAgNnSc7d1	rozzuřené
zraněným	zraněný	k2eAgNnSc7d1	zraněné
zvířetem	zvíře	k1gNnSc7	zvíře
ji	on	k3xPp3gFnSc4	on
zachránil	zachránit	k5eAaPmAgMnS	zachránit
pastýř	pastýř	k1gMnSc1	pastýř
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnSc1	její
budoucí	budoucí	k2eAgMnSc1d1	budoucí
manžel	manžel	k1gMnSc1	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
zachráněna	zachránit	k5eAaPmNgFnS	zachránit
<g/>
,	,	kIx,	,
nechala	nechat	k5eAaPmAgFnS	nechat
Jitka	Jitka	k1gFnSc1	Jitka
postavit	postavit	k5eAaPmF	postavit
kapličku	kaplička	k1gFnSc4	kaplička
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
ní	on	k3xPp3gFnSc2	on
pak	pak	k9	pak
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
postavil	postavit	k5eAaPmAgMnS	postavit
lovecký	lovecký	k2eAgInSc4d1	lovecký
zámek	zámek	k1gInSc4	zámek
<g/>
,	,	kIx,	,
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
Jitce	Jitka	k1gFnSc3	Jitka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Jitčin	Jitčin	k2eAgInSc1d1	Jitčin
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Jičín	Jičín	k1gInSc1	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vědecké	vědecký	k2eAgFnSc2d1	vědecká
teorie	teorie	k1gFnSc2	teorie
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
odvozováno	odvozovat	k5eAaImNgNnS	odvozovat
od	od	k7c2	od
slovanského	slovanský	k2eAgNnSc2d1	slovanské
slova	slovo	k1gNnSc2	slovo
dik	dik	k1gMnSc1	dik
<g/>
,	,	kIx,	,
divoký	divoký	k2eAgMnSc1d1	divoký
kanec	kanec	k1gMnSc1	kanec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
území	území	k1gNnSc6	území
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
v	v	k7c6	v
hojném	hojný	k2eAgInSc6d1	hojný
počtu	počet	k1gInSc6	počet
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
zprvu	zprvu	k6eAd1	zprvu
Dičín	Dičína	k1gFnPc2	Dičína
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Jičín	Jičín	k1gInSc1	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
také	také	k9	také
jméno	jméno	k1gNnSc1	jméno
nedalekého	daleký	k2eNgInSc2d1	nedaleký
kopce	kopec	k1gInSc2	kopec
Svinec	svinec	k1gInSc1	svinec
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
písemná	písemný	k2eAgFnSc1d1	písemná
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
městě	město	k1gNnSc6	město
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
r.	r.	kA	r.
1313	[number]	k4	1313
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
vydal	vydat	k5eAaPmAgInS	vydat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
města	město	k1gNnSc2	město
listinu	listina	k1gFnSc4	listina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
uděluje	udělovat	k5eAaImIp3nS	udělovat
právo	právo	k1gNnSc4	právo
vybírat	vybírat	k5eAaImF	vybírat
clo	clo	k1gNnSc4	clo
a	a	k8xC	a
mýto	mýto	k1gNnSc4	mýto
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
váže	vázat	k5eAaImIp3nS	vázat
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
svůj	svůj	k3xOyFgInSc4	svůj
vznik	vznik	k1gInSc4	vznik
s	s	k7c7	s
datem	datum	k1gNnSc7	datum
udělení	udělení	k1gNnSc2	udělení
tohoto	tento	k3xDgNnSc2	tento
privilegia	privilegium	k1gNnSc2	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Zakládací	zakládací	k2eAgFnSc1d1	zakládací
listina	listina	k1gFnSc1	listina
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
osada	osada	k1gFnSc1	osada
pod	pod	k7c7	pod
hradem	hrad	k1gInSc7	hrad
Starý	starý	k2eAgInSc1d1	starý
Jičín	Jičín	k1gInSc1	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Výhodná	výhodný	k2eAgFnSc1d1	výhodná
geografická	geografický	k2eAgFnSc1d1	geografická
poloha	poloha	k1gFnSc1	poloha
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
důležitých	důležitý	k2eAgFnPc2d1	důležitá
obchodních	obchodní	k2eAgFnPc2d1	obchodní
cest	cesta	k1gFnPc2	cesta
měla	mít	k5eAaImAgFnS	mít
příznivý	příznivý	k2eAgInSc4d1	příznivý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc4	město
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
čtvercový	čtvercový	k2eAgInSc1d1	čtvercový
tvar	tvar	k1gInSc1	tvar
a	a	k8xC	a
původní	původní	k2eAgInSc1d1	původní
půdorys	půdorys	k1gInSc1	půdorys
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Pravoúhlý	pravoúhlý	k2eAgMnSc1d1	pravoúhlý
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
čtvercový	čtvercový	k2eAgInSc4d1	čtvercový
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
i	i	k9	i
městské	městský	k2eAgNnSc1d1	Městské
opevnění	opevnění	k1gNnSc1	opevnění
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
něhož	jenž	k3xRgNnSc2	jenž
procházela	procházet	k5eAaImAgFnS	procházet
obvodová	obvodový	k2eAgFnSc1d1	obvodová
ulice	ulice	k1gFnSc1	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Opevnění	opevnění	k1gNnSc1	opevnění
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
jen	jen	k9	jen
z	z	k7c2	z
hliněného	hliněný	k2eAgInSc2d1	hliněný
valu	val	k1gInSc2	val
s	s	k7c7	s
palisádou	palisáda	k1gFnSc7	palisáda
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
během	během	k7c2	během
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
kamennými	kamenný	k2eAgFnPc7d1	kamenná
hradbami	hradba	k1gFnPc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
obepínaly	obepínat	k5eAaImAgFnP	obepínat
i	i	k9	i
panské	panský	k2eAgNnSc4d1	panské
sídlo	sídlo	k1gNnSc4	sídlo
(	(	kIx(	(
<g/>
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nynější	nynější	k2eAgInSc1d1	nynější
zámek	zámek	k1gInSc1	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgInSc1d1	městský
plán	plán	k1gInSc1	plán
je	být	k5eAaImIp3nS	být
orientován	orientovat	k5eAaBmNgInS	orientovat
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
severozápadu	severozápad	k1gInSc2	severozápad
k	k	k7c3	k
jihovýchodu	jihovýchod	k1gInSc3	jihovýchod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
obchodní	obchodní	k2eAgFnSc2d1	obchodní
cesty	cesta	k1gFnSc2	cesta
od	od	k7c2	od
Fulneku	Fulnek	k1gInSc2	Fulnek
do	do	k7c2	do
Valašského	valašský	k2eAgNnSc2d1	Valašské
Meziříčí	Meziříčí	k1gNnSc2	Meziříčí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
vedly	vést	k5eAaImAgFnP	vést
dvě	dva	k4xCgFnPc1	dva
brány	brána	k1gFnPc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
navazovala	navazovat	k5eAaImAgFnS	navazovat
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
předměstí	předměstí	k1gNnPc2	předměstí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
rozvoj	rozvoj	k1gInSc1	rozvoj
spadá	spadat	k5eAaPmIp3nS	spadat
až	až	k9	až
do	do	k7c2	do
pozdější	pozdní	k2eAgFnSc2d2	pozdější
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
získal	získat	k5eAaPmAgInS	získat
město	město	k1gNnSc4	město
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
i	i	k9	i
se	s	k7c7	s
starojickým	starojický	k2eAgNnSc7d1	starojický
panstvím	panství	k1gNnSc7	panství
představitel	představitel	k1gMnSc1	představitel
významného	významný	k2eAgInSc2d1	významný
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
Vok	Vok	k1gMnPc1	Vok
I.	I.	kA	I.
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rod	rod	k1gInSc1	rod
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
rozrodu	rozrod	k1gInSc2	rozrod
Benešoviců	Benešovice	k1gMnPc2	Benešovice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
pozemky	pozemka	k1gFnPc4	pozemka
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
Opavsku	Opavsko	k1gNnSc6	Opavsko
<g/>
.	.	kIx.	.
</s>
<s>
Vok	Vok	k?	Vok
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
udělili	udělit	k5eAaPmAgMnP	udělit
městu	město	k1gNnSc3	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1373	[number]	k4	1373
práva	právo	k1gNnSc2	právo
odúmrti	odúmrť	k1gFnSc2	odúmrť
a	a	k8xC	a
výročního	výroční	k2eAgInSc2d1	výroční
trhu	trh	k1gInSc2	trh
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
výsady	výsada	k1gFnPc4	výsada
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
město	město	k1gNnSc1	město
dobyl	dobýt	k5eAaPmAgMnS	dobýt
husitský	husitský	k2eAgMnSc1d1	husitský
hejtman	hejtman	k1gMnSc1	hejtman
Jan	Jan	k1gMnSc1	Jan
Tovačovský	Tovačovský	k1gMnSc1	Tovačovský
z	z	k7c2	z
Cimburka	Cimburek	k1gMnSc2	Cimburek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1427	[number]	k4	1427
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
panství	panství	k1gNnSc1	panství
a	a	k8xC	a
město	město	k1gNnSc1	město
patřilo	patřit	k5eAaImAgNnS	patřit
Janu	Jana	k1gFnSc4	Jana
Jičínskému	jičínský	k2eAgInSc3d1	jičínský
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
smrtí	smrt	k1gFnSc7	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1434	[number]	k4	1434
končí	končit	k5eAaImIp3nS	končit
vláda	vláda	k1gFnSc1	vláda
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
období	období	k1gNnSc2	období
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
pochází	pocházet	k5eAaImIp3nS	pocházet
původní	původní	k2eAgNnSc4d1	původní
gotické	gotický	k2eAgNnSc4d1	gotické
přízemí	přízemí	k1gNnSc4	přízemí
nynějšího	nynější	k2eAgInSc2d1	nynější
Žerotínského	žerotínský	k2eAgInSc2d1	žerotínský
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
městského	městský	k2eAgNnSc2d1	Městské
opevnění	opevnění	k1gNnSc2	opevnění
a	a	k8xC	a
základ	základ	k1gInSc1	základ
městského	městský	k2eAgInSc2d1	městský
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgNnSc7d1	důležité
obdobím	období	k1gNnSc7	období
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesátileté	padesátiletý	k2eAgNnSc1d1	padesátileté
působení	působení	k1gNnSc1	působení
dalšího	další	k2eAgInSc2d1	další
význačného	význačný	k2eAgInSc2d1	význačný
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
rodu	rod	k1gInSc2	rod
Žerotínů	Žerotín	k1gMnPc2	Žerotín
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1500	[number]	k4	1500
<g/>
–	–	k?	–
<g/>
1558	[number]	k4	1558
zapsali	zapsat	k5eAaPmAgMnP	zapsat
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
města	město	k1gNnSc2	město
zejména	zejména	k9	zejména
přebudováním	přebudování	k1gNnSc7	přebudování
gotické	gotický	k2eAgFnSc2d1	gotická
tvrze	tvrz	k1gFnSc2	tvrz
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
na	na	k7c4	na
pohodlné	pohodlný	k2eAgNnSc4d1	pohodlné
renesanční	renesanční	k2eAgNnSc4d1	renesanční
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
,	,	kIx,	,
nynější	nynější	k2eAgInSc1d1	nynější
Žerotínský	žerotínský	k2eAgInSc1d1	žerotínský
zámek	zámek	k1gInSc1	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Darovali	darovat	k5eAaPmAgMnP	darovat
městu	město	k1gNnSc3	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1501	[number]	k4	1501
dům	dům	k1gInSc4	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
důstojným	důstojný	k2eAgNnSc7d1	důstojné
sídlem	sídlo	k1gNnSc7	sídlo
městské	městský	k2eAgFnSc2d1	městská
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
r.	r.	kA	r.
1503	[number]	k4	1503
lehly	lehnout	k5eAaPmAgInP	lehnout
popelem	popel	k1gInSc7	popel
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
domy	dům	k1gInPc4	dům
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc7	jejich
zásluhou	zásluha	k1gFnSc7	zásluha
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
kamenného	kamenný	k2eAgNnSc2d1	kamenné
podloubí	podloubí	k1gNnSc2	podloubí
a	a	k8xC	a
měšťanských	měšťanský	k2eAgInPc2d1	měšťanský
domů	dům	k1gInPc2	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Šestnácté	šestnáctý	k4xOgNnSc1	šestnáctý
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
obdobím	období	k1gNnPc3	období
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
rozkvětu	rozkvět	k1gInSc2	rozkvět
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bohatlo	bohatnout	k5eAaImAgNnS	bohatnout
vařením	vaření	k1gNnSc7	vaření
a	a	k8xC	a
šenkováním	šenkováním	k?	šenkováním
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
se	se	k3xPyFc4	se
především	především	k9	především
soukenictví	soukenictví	k1gNnSc1	soukenictví
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
200	[number]	k4	200
řemeslníků	řemeslník	k1gMnPc2	řemeslník
celou	celý	k2eAgFnSc4d1	celá
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
tvořili	tvořit	k5eAaImAgMnP	tvořit
koncem	koncem	k7c2	koncem
století	století	k1gNnSc2	století
soukeníci	soukeník	k1gMnPc1	soukeník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Jihlavě	Jihlava	k1gFnSc6	Jihlava
byl	být	k5eAaImAgInS	být
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
druhým	druhý	k4xOgNnSc7	druhý
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
městem	město	k1gNnSc7	město
soukenické	soukenický	k2eAgFnSc2d1	Soukenická
výroby	výroba	k1gFnSc2	výroba
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
bohatla	bohatnout	k5eAaImAgFnS	bohatnout
městská	městský	k2eAgFnSc1d1	městská
pokladna	pokladna	k1gFnSc1	pokladna
a	a	k8xC	a
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
natolik	natolik	k6eAd1	natolik
ekonomicky	ekonomicky	k6eAd1	ekonomicky
silné	silný	k2eAgNnSc1d1	silné
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
Žerotínů	Žerotín	k1gInPc2	Žerotín
r.	r.	kA	r.
1558	[number]	k4	1558
odkoupilo	odkoupit	k5eAaPmAgNnS	odkoupit
jejich	jejich	k3xOp3gNnSc1	jejich
panství	panství	k1gNnSc2	panství
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Vykoupilo	vykoupit	k5eAaPmAgNnS	vykoupit
se	se	k3xPyFc4	se
z	z	k7c2	z
poddanství	poddanství	k1gNnSc2	poddanství
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
městem	město	k1gNnSc7	město
komorním	komorní	k1gMnSc7	komorní
<g/>
,	,	kIx,	,
podřízeným	podřízený	k2eAgMnSc7d1	podřízený
jen	jen	k9	jen
královské	královský	k2eAgFnSc3d1	královská
koruně	koruna	k1gFnSc3	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
se	s	k7c7	s
zásluhou	zásluha	k1gFnSc7	zásluha
"	"	kIx"	"
<g/>
zimního	zimní	k2eAgMnSc2d1	zimní
krále	král	k1gMnSc2	král
<g/>
"	"	kIx"	"
Fridricha	Fridrich	k1gMnSc2	Fridrich
Falckého	falcký	k2eAgMnSc2d1	falcký
stalo	stát	k5eAaPmAgNnS	stát
r.	r.	kA	r.
1620	[number]	k4	1620
dokonce	dokonce	k9	dokonce
městem	město	k1gNnSc7	město
královským	královský	k2eAgNnSc7d1	královské
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
šestnácté	šestnáctý	k4xOgNnSc4	šestnáctý
století	století	k1gNnSc4	století
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
prosperity	prosperita	k1gFnSc2	prosperita
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
polovina	polovina	k1gFnSc1	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
obdobím	období	k1gNnSc7	období
nestability	nestabilita	k1gFnSc2	nestabilita
<g/>
,	,	kIx,	,
náboženských	náboženský	k2eAgInPc2d1	náboženský
a	a	k8xC	a
politických	politický	k2eAgInPc2d1	politický
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ohrožovaly	ohrožovat	k5eAaImAgFnP	ohrožovat
jeho	jeho	k3xOp3gFnSc4	jeho
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zachvátila	zachvátit	k5eAaPmAgFnS	zachvátit
většinu	většina	k1gFnSc4	většina
zemí	zem	k1gFnPc2	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
i	i	k8xC	i
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1621	[number]	k4	1621
přepadlo	přepadnout	k5eAaPmAgNnS	přepadnout
a	a	k8xC	a
pobilo	pobít	k5eAaPmAgNnS	pobít
vojsko	vojsko	k1gNnSc1	vojsko
markraběte	markrabě	k1gMnSc2	markrabě
Jana	Jan	k1gMnSc2	Jan
Jiřího	Jiří	k1gMnSc2	Jiří
Krnovského	krnovský	k2eAgMnSc2d1	krnovský
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
oddíl	oddíl	k1gInSc1	oddíl
Španělů	Španěl	k1gMnPc2	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
jejich	jejich	k3xOp3gInPc2	jejich
hrobů	hrob	k1gInPc2	hrob
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
postavena	postavit	k5eAaPmNgFnS	postavit
tzv.	tzv.	kA	tzv.
Španělská	španělský	k2eAgFnSc1d1	španělská
kaple	kaple	k1gFnSc1	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bitvě	bitva	k1gFnSc6	bitva
město	město	k1gNnSc1	město
opět	opět	k6eAd1	opět
vyhořelo	vyhořet	k5eAaPmAgNnS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1624	[number]	k4	1624
a	a	k8xC	a
1680	[number]	k4	1680
postihla	postihnout	k5eAaPmAgFnS	postihnout
Nový	nový	k2eAgInSc4d1	nový
Jičín	Jičín	k1gInSc4	Jičín
morová	morový	k2eAgFnSc1d1	morová
rána	rána	k1gFnSc1	rána
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
protestantské	protestantský	k2eAgNnSc1d1	protestantské
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
stavovském	stavovský	k2eAgNnSc6d1	Stavovské
proticísařském	proticísařský	k2eAgNnSc6d1	proticísařský
povstání	povstání	k1gNnSc6	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1624	[number]	k4	1624
potrestáno	potrestat	k5eAaPmNgNnS	potrestat
<g/>
.	.	kIx.	.
</s>
<s>
Ztratilo	ztratit	k5eAaPmAgNnS	ztratit
svou	svůj	k3xOyFgFnSc4	svůj
samostatnost	samostatnost	k1gFnSc4	samostatnost
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
městem	město	k1gNnSc7	město
poddanským	poddanský	k2eAgNnSc7d1	poddanské
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
darováno	darován	k2eAgNnSc1d1	darováno
olomouckým	olomoucký	k2eAgMnPc3d1	olomoucký
jezuitům	jezuita	k1gMnPc3	jezuita
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
majetku	majetek	k1gInSc6	majetek
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
až	až	k6eAd1	až
do	do	k7c2	do
zrušení	zrušení	k1gNnSc2	zrušení
jezuitského	jezuitský	k2eAgInSc2d1	jezuitský
řádu	řád	k1gInSc2	řád
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1626	[number]	k4	1626
obsadili	obsadit	k5eAaPmAgMnP	obsadit
město	město	k1gNnSc4	město
mansfeldští	mansfeldský	k2eAgMnPc1d1	mansfeldský
<g/>
,	,	kIx,	,
císařovi	císařův	k2eAgMnPc1d1	císařův
protivníci	protivník	k1gMnPc1	protivník
<g/>
,	,	kIx,	,
a	a	k8xC	a
vypudili	vypudit	k5eAaPmAgMnP	vypudit
katolické	katolický	k2eAgMnPc4d1	katolický
duchovní	duchovní	k1gMnPc4	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1629	[number]	k4	1629
<g/>
,	,	kIx,	,
po	po	k7c6	po
nejistotách	nejistota	k1gFnPc6	nejistota
a	a	k8xC	a
náboženských	náboženský	k2eAgInPc6d1	náboženský
bojích	boj	k1gInPc6	boj
město	město	k1gNnSc4	město
rezignovalo	rezignovat	k5eAaBmAgNnS	rezignovat
a	a	k8xC	a
přestoupilo	přestoupit	k5eAaPmAgNnS	přestoupit
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
víru	víra	k1gFnSc4	víra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1635	[number]	k4	1635
bylo	být	k5eAaImAgNnS	být
panství	panství	k1gNnSc1	panství
jezuity	jezuita	k1gMnSc2	jezuita
pokatoličtěno	pokatoličtěn	k2eAgNnSc4d1	pokatoličtěn
<g/>
.	.	kIx.	.
</s>
<s>
Válečné	válečný	k2eAgFnPc4d1	válečná
útrapy	útrapa	k1gFnPc4	útrapa
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
však	však	k9	však
ještě	ještě	k6eAd1	ještě
neskončily	skončit	k5eNaPmAgFnP	skončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1643	[number]	k4	1643
napadli	napadnout	k5eAaPmAgMnP	napadnout
město	město	k1gNnSc4	město
Švédové	Švédová	k1gFnSc2	Švédová
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
ho	on	k3xPp3gMnSc4	on
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
utichaly	utichat	k5eAaImAgInP	utichat
válečné	válečný	k2eAgInPc1d1	válečný
a	a	k8xC	a
náboženské	náboženský	k2eAgInPc1d1	náboženský
rozbroje	rozbroj	k1gInPc1	rozbroj
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
příhodnější	příhodný	k2eAgFnSc1d2	příhodnější
doba	doba	k1gFnSc1	doba
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
převažuje	převažovat	k5eAaImIp3nS	převažovat
soukenictví	soukenictví	k1gNnSc4	soukenictví
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
obchodu	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1768	[number]	k4	1768
zachvátil	zachvátit	k5eAaPmAgInS	zachvátit
požár	požár	k1gInSc4	požár
barokní	barokní	k2eAgInPc1d1	barokní
měšťanské	měšťanský	k2eAgInPc1d1	měšťanský
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
přestavěny	přestavět	k5eAaPmNgInP	přestavět
v	v	k7c6	v
klasicistním	klasicistní	k2eAgMnSc6d1	klasicistní
duchu	duch	k1gMnSc6	duch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1773	[number]	k4	1773
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
jezuitského	jezuitský	k2eAgInSc2d1	jezuitský
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
až	až	k6eAd1	až
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
město	město	k1gNnSc4	město
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
pak	pak	k6eAd1	pak
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
Nový	nový	k2eAgInSc4d1	nový
Jičín	Jičín	k1gInSc4	Jičín
za	za	k7c4	za
svobodné	svobodný	k2eAgNnSc4d1	svobodné
municipální	municipální	k2eAgNnSc4d1	municipální
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
města	město	k1gNnSc2	město
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
zavedením	zavedení	k1gNnSc7	zavedení
státních	státní	k2eAgInPc2d1	státní
úřadů	úřad	k1gInPc2	úřad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
stal	stát	k5eAaPmAgInS	stát
sídlem	sídlo	k1gNnSc7	sídlo
okresního	okresní	k2eAgNnSc2d1	okresní
hejtmanství	hejtmanství	k1gNnSc2	hejtmanství
<g/>
,	,	kIx,	,
berního	berní	k2eAgInSc2d1	berní
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
okresního	okresní	k2eAgInSc2d1	okresní
a	a	k8xC	a
krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
město	město	k1gNnSc1	město
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
právním	právní	k2eAgNnSc7d1	právní
<g/>
,	,	kIx,	,
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
a	a	k8xC	a
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
města	město	k1gNnSc2	město
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jistě	jistě	k9	jistě
posílil	posílit	k5eAaPmAgMnS	posílit
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
jeho	jeho	k3xOp3gMnPc1	jeho
občané	občan	k1gMnPc1	občan
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nepodcenili	podcenit	k5eNaPmAgMnP	podcenit
význam	význam	k1gInSc4	význam
železnice	železnice	k1gFnSc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
neodpustitelná	odpustitelný	k2eNgFnSc1d1	neodpustitelná
chyba	chyba	k1gFnSc1	chyba
a	a	k8xC	a
město	město	k1gNnSc1	město
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
doplácí	doplácet	k5eAaImIp3nS	doplácet
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
odvětvím	odvětví	k1gNnSc7	odvětví
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stala	stát	k5eAaPmAgFnS	stát
výroba	výroba	k1gFnSc1	výroba
klobouků	klobouk	k1gInPc2	klobouk
<g/>
,	,	kIx,	,
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
založil	založit	k5eAaPmAgMnS	založit
Johan	Johan	k1gMnSc1	Johan
Hückel	Hückel	k1gMnSc1	Hückel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
první	první	k4xOgFnSc4	první
továrnu	továrna	k1gFnSc4	továrna
na	na	k7c4	na
strojní	strojní	k2eAgFnSc4d1	strojní
výrobu	výroba	k1gFnSc4	výroba
klobouků	klobouk	k1gInPc2	klobouk
v	v	k7c6	v
Rakouském	rakouský	k2eAgNnSc6d1	rakouské
císařství	císařství	k1gNnSc6	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
továrnou	továrna	k1gFnSc7	továrna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
doutníková	doutníkový	k2eAgFnSc1d1	doutníková
a	a	k8xC	a
tabáková	tabákový	k2eAgFnSc1d1	tabáková
továrna	továrna	k1gFnSc1	továrna
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
zřídil	zřídit	k5eAaPmAgInS	zřídit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
okolo	okolo	k7c2	okolo
2	[number]	k4	2
500	[number]	k4	500
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
začal	začít	k5eAaPmAgMnS	začít
Josef	Josef	k1gMnSc1	Josef
Rotter	Rotter	k1gMnSc1	Rotter
vyrábět	vyrábět	k5eAaImF	vyrábět
kočárové	kočárový	k2eAgFnPc4d1	Kočárová
svítilny	svítilna	k1gFnPc4	svítilna
a	a	k8xC	a
položil	položit	k5eAaPmAgMnS	položit
tak	tak	k6eAd1	tak
základy	základ	k1gInPc4	základ
následné	následný	k2eAgFnSc2d1	následná
společnosti	společnost	k1gFnSc2	společnost
Autopal	Autopal	k1gFnSc2	Autopal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
před	před	k7c7	před
1	[number]	k4	1
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
převažovalo	převažovat	k5eAaImAgNnS	převažovat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
německé	německý	k2eAgNnSc4d1	německé
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
projevovat	projevovat	k5eAaImF	projevovat
národnostní	národnostní	k2eAgInPc4d1	národnostní
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Měly	mít	k5eAaImAgFnP	mít
silný	silný	k2eAgInSc4d1	silný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
život	život	k1gInSc4	život
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
nacismu	nacismus	k1gInSc2	nacismus
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
baštou	bašta	k1gFnSc7	bašta
Henleinovy	Henleinův	k2eAgFnSc2d1	Henleinova
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
za	za	k7c4	za
starostování	starostování	k?	starostování
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Ernsta	Ernst	k1gMnSc2	Ernst
Schollicha	Schollich	k1gMnSc2	Schollich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
měl	mít	k5eAaImAgInS	mít
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
13	[number]	k4	13
226	[number]	k4	226
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
3	[number]	k4	3
917	[number]	k4	917
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
měl	mít	k5eAaImAgInS	mít
13	[number]	k4	13
997	[number]	k4	997
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
4	[number]	k4	4
236	[number]	k4	236
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
do	do	k7c2	do
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
součástí	součást	k1gFnPc2	součást
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
podíl	podíl	k1gInSc4	podíl
obyvatel	obyvatel	k1gMnPc2	obyvatel
české	český	k2eAgFnSc2d1	Česká
národnosti	národnost	k1gFnSc2	národnost
na	na	k7c4	na
2	[number]	k4	2
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Válečná	válečný	k2eAgNnPc1d1	válečné
léta	léto	k1gNnPc1	léto
město	město	k1gNnSc1	město
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
spíše	spíše	k9	spíše
celkovým	celkový	k2eAgInSc7d1	celkový
úpadkem	úpadek	k1gInSc7	úpadek
než	než	k8xS	než
vyslovenými	vyslovený	k2eAgFnPc7d1	vyslovená
ztrátami	ztráta	k1gFnPc7	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Nedošlo	dojít	k5eNaPmAgNnS	dojít
zde	zde	k6eAd1	zde
k	k	k7c3	k
žádným	žádný	k3yNgFnPc3	žádný
bojovým	bojový	k2eAgFnPc3d1	bojová
akcím	akce	k1gFnPc3	akce
ani	ani	k8xC	ani
k	k	k7c3	k
bombardování	bombardování	k1gNnSc3	bombardování
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc4d1	nový
Jičín	Jičín	k1gInSc4	Jičín
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
vrátili	vrátit	k5eAaPmAgMnP	vrátit
menšinoví	menšinový	k2eAgMnPc1d1	menšinový
čeští	český	k2eAgMnPc1d1	český
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zde	zde	k6eAd1	zde
žili	žít	k5eAaImAgMnP	žít
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
přicházeli	přicházet	k5eAaImAgMnP	přicházet
noví	nový	k2eAgMnPc1d1	nový
osídlenci	osídlenec	k1gMnPc1	osídlenec
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
krajů	kraj	k1gInPc2	kraj
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Volyně	Volyně	k1gFnSc2	Volyně
a	a	k8xC	a
východních	východní	k2eAgFnPc2d1	východní
státu	stát	k1gInSc2	stát
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odsunu	odsun	k1gInSc6	odsun
Němců	Němec	k1gMnPc2	Němec
se	se	k3xPyFc4	se
poměr	poměr	k1gInSc1	poměr
národnostního	národnostní	k2eAgNnSc2d1	národnostní
složení	složení	k1gNnSc2	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
města	město	k1gNnSc2	město
změnil	změnit	k5eAaPmAgMnS	změnit
v	v	k7c4	v
majoritní	majoritní	k2eAgFnSc4d1	majoritní
většinu	většina	k1gFnSc4	většina
českého	český	k2eAgNnSc2d1	české
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
dosídlením	dosídlení	k1gNnSc7	dosídlení
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
Hané	Haná	k1gFnSc2	Haná
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc2d1	jižní
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
bytů	byt	k1gInPc2	byt
byl	být	k5eAaImAgInS	být
řešen	řešit	k5eAaImNgInS	řešit
masovou	masový	k2eAgFnSc7d1	masová
výstavbou	výstavba	k1gFnSc7	výstavba
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
v	v	k7c6	v
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
dvě	dva	k4xCgNnPc4	dva
nová	nový	k2eAgNnPc4d1	nové
sídliště	sídliště	k1gNnPc4	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
byl	být	k5eAaImAgInS	být
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1966	[number]	k4	1966
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
městu	město	k1gNnSc3	město
připojena	připojen	k2eAgFnSc1d1	připojena
vesnice	vesnice	k1gFnSc1	vesnice
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1974	[number]	k4	1974
byly	být	k5eAaImAgFnP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
obce	obec	k1gFnPc1	obec
Bludovice	Bludovice	k1gFnPc1	Bludovice
a	a	k8xC	a
Kojetín	Kojetín	k1gInSc1	Kojetín
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1975	[number]	k4	1975
byla	být	k5eAaImAgFnS	být
připojena	připojit	k5eAaPmNgFnS	připojit
vesnice	vesnice	k1gFnSc1	vesnice
Loučka	loučka	k1gFnSc1	loučka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1979	[number]	k4	1979
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
Novému	nový	k2eAgInSc3d1	nový
Jičínu	Jičín	k1gInSc3	Jičín
připojen	připojen	k2eAgMnSc1d1	připojen
Straník	straník	k1gMnSc1	straník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
připojena	připojit	k5eAaPmNgFnS	připojit
Libhošť	Libhošť	k1gFnSc7	Libhošť
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
oddělil	oddělit	k5eAaPmAgInS	oddělit
Šenov	Šenov	k1gInSc1	Šenov
u	u	k7c2	u
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
počátku	počátek	k1gInSc3	počátek
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
místního	místní	k2eAgNnSc2d1	místní
referenda	referendum	k1gNnSc2	referendum
oddělila	oddělit	k5eAaPmAgFnS	oddělit
Libhošť	Libhošť	k1gFnSc1	Libhošť
<g/>
.	.	kIx.	.
1313	[number]	k4	1313
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1558	[number]	k4	1558
–	–	k?	–
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
byl	být	k5eAaImAgInS	být
poddanským	poddanský	k2eAgNnSc7d1	poddanské
městem	město	k1gNnSc7	město
20	[number]	k4	20
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1558	[number]	k4	1558
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1624	[number]	k4	1624
–	–	k?	–
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
byl	být	k5eAaImAgInS	být
svobodným	svobodný	k2eAgNnSc7d1	svobodné
komorním	komorní	k2eAgNnSc7d1	komorní
městem	město	k1gNnSc7	město
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1624	[number]	k4	1624
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1775	[number]	k4	1775
–	–	k?	–
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
poddanským	poddanský	k2eAgNnSc7d1	poddanské
městem	město	k1gNnSc7	město
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
aktivní	aktivní	k2eAgFnSc3d1	aktivní
účasti	účast	k1gFnSc3	účast
odboje	odboj	k1gInSc2	odboj
protestantských	protestantský	k2eAgInPc2d1	protestantský
stavů	stav	k1gInPc2	stav
proti	proti	k7c3	proti
císaři	císař	k1gMnSc3	císař
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1775	[number]	k4	1775
–	–	k?	–
Díky	díky	k7c3	díky
Marii	Maria	k1gFnSc3	Maria
Terezii	Terezie	k1gFnSc3	Terezie
se	se	k3xPyFc4	se
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
stal	stát	k5eAaPmAgInS	stát
svobodným	svobodný	k2eAgNnSc7d1	svobodné
městem	město	k1gNnSc7	město
municipiálním	municipiální	k2eAgInPc3d1	municipiální
<g/>
;	;	kIx,	;
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
byl	být	k5eAaImAgMnS	být
<g />
.	.	kIx.	.
</s>
<s>
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
a	a	k8xC	a
sídlem	sídlo	k1gNnSc7	sídlo
krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
–	–	k?	–
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
ČSR	ČSR	kA	ČSR
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
–	–	k?	–
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
byl	být	k5eAaImAgInS	být
připojen	připojit	k5eAaPmNgInS	připojit
ke	k	k7c3	k
Třetí	třetí	k4xOgFnSc3	třetí
říši	říš	k1gFnSc3	říš
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
–	–	k?	–
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
byl	být	k5eAaImAgInS	být
osvobozen	osvobodit	k5eAaPmNgInS	osvobodit
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
součástí	součást	k1gFnSc7	součást
ČSR	ČSR	kA	ČSR
jako	jako	k8xS	jako
město	město	k1gNnSc1	město
okresní	okresní	k2eAgFnSc2d1	okresní
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Čtvercové	čtvercový	k2eAgNnSc1d1	čtvercové
Masarykovo	Masarykův	k2eAgNnSc1d1	Masarykovo
náměstí	náměstí	k1gNnSc1	náměstí
s	s	k7c7	s
podloubím	podloubí	k1gNnSc7	podloubí
<g/>
,	,	kIx,	,
řadou	řada	k1gFnSc7	řada
historických	historický	k2eAgInPc2d1	historický
domů	dům	k1gInPc2	dům
a	a	k8xC	a
Mariánským	mariánský	k2eAgInSc7d1	mariánský
sloupem	sloup	k1gInSc7	sloup
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1710	[number]	k4	1710
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
sloupem	sloup	k1gInSc7	sloup
stojí	stát	k5eAaImIp3nS	stát
drobná	drobný	k2eAgFnSc1d1	drobná
kašna	kašna	k1gFnSc1	kašna
s	s	k7c7	s
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
plastikou	plastika	k1gFnSc7	plastika
Fr.	Fr.	k1gMnSc2	Fr.
Barwiga	Barwig	k1gMnSc2	Barwig
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
přibyly	přibýt	k5eAaPmAgFnP	přibýt
fontánky	fontánka	k1gFnPc1	fontánka
a	a	k8xC	a
miskovitá	miskovitý	k2eAgFnSc1d1	miskovitá
kašna	kašna	k1gFnSc1	kašna
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
.	.	kIx.	.
</s>
<s>
Měšťanský	měšťanský	k2eAgInSc1d1	měšťanský
dům	dům	k1gInSc1	dům
Stará	starý	k2eAgFnSc1d1	stará
pošta	pošta	k1gFnSc1	pošta
(	(	kIx(	(
<g/>
Masarykovo	Masarykův	k2eAgNnSc1d1	Masarykovo
náměstí	náměstí	k1gNnSc1	náměstí
20	[number]	k4	20
<g/>
)	)	kIx)	)
s	s	k7c7	s
dvojitou	dvojitý	k2eAgFnSc7d1	dvojitá
renesanční	renesanční	k2eAgFnSc7d1	renesanční
lodžií	lodžie	k1gFnSc7	lodžie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1563	[number]	k4	1563
Zbytky	zbytek	k1gInPc7	zbytek
hradeb	hradba	k1gFnPc2	hradba
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
nárožní	nárožní	k2eAgFnSc7d1	nárožní
Farskou	farský	k2eAgFnSc7d1	Farská
baštou	bašta	k1gFnSc7	bašta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1613	[number]	k4	1613
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
cípu	cíp	k1gInSc6	cíp
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Renesanční	renesanční	k2eAgInSc1d1	renesanční
Žerotínský	žerotínský	k2eAgInSc1d1	žerotínský
zámek	zámek	k1gInSc1	zámek
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
náměstí	náměstí	k1gNnSc2	náměstí
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
přestavbou	přestavba	k1gFnSc7	přestavba
staršího	starý	k2eAgNnSc2d2	starší
panského	panský	k2eAgNnSc2d1	panské
sídla	sídlo	k1gNnSc2	sídlo
v	v	k7c6	v
letech	let	k1gInPc6	let
1533	[number]	k4	1533
<g/>
–	–	k?	–
<g/>
1558	[number]	k4	1558
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
romanticky	romanticky	k6eAd1	romanticky
upraven	upravit	k5eAaPmNgInS	upravit
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgFnSc1d1	barokní
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
s	s	k7c7	s
bočními	boční	k2eAgFnPc7d1	boční
oratořemi	oratoř	k1gFnPc7	oratoř
<g/>
,	,	kIx,	,
vystavěné	vystavěný	k2eAgFnPc1d1	vystavěná
na	na	k7c6	na
starších	starý	k2eAgInPc6d2	starší
základech	základ	k1gInPc6	základ
v	v	k7c6	v
letech	let	k1gInPc6	let
1729	[number]	k4	1729
<g/>
–	–	k?	–
<g/>
1734	[number]	k4	1734
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
průčelí	průčelí	k1gNnSc6	průčelí
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1587	[number]	k4	1587
<g/>
–	–	k?	–
<g/>
1618	[number]	k4	1618
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
latinský	latinský	k2eAgInSc1d1	latinský
nápis	nápis	k1gInSc1	nápis
nad	nad	k7c7	nad
branou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
věži	věž	k1gFnSc6	věž
je	být	k5eAaImIp3nS	být
zvon	zvon	k1gInSc1	zvon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1488	[number]	k4	1488
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
zvony	zvon	k1gInPc1	zvon
jsou	být	k5eAaImIp3nP	být
moderní	moderní	k2eAgInPc1d1	moderní
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
cenné	cenný	k2eAgNnSc1d1	cenné
barokní	barokní	k2eAgNnSc1d1	barokní
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
oltářní	oltářní	k2eAgInSc1d1	oltářní
obraz	obraz	k1gInSc1	obraz
od	od	k7c2	od
Eliase	Eliasa	k1gFnSc6	Eliasa
Herberta	Herbert	k1gMnSc2	Herbert
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1739	[number]	k4	1739
<g/>
,	,	kIx,	,
plastiky	plastika	k1gFnPc1	plastika
od	od	k7c2	od
O.	O.	kA	O.
Zahnera	Zahner	k1gMnSc4	Zahner
a	a	k8xC	a
vyřezávané	vyřezávaný	k2eAgFnPc4d1	vyřezávaná
lavice	lavice	k1gFnPc4	lavice
z	z	k7c2	z
téže	tenže	k3xDgFnSc2	tenže
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
1500	[number]	k4	1500
je	být	k5eAaImIp3nS	být
jednolodní	jednolodní	k2eAgFnSc1d1	jednolodní
obdélná	obdélný	k2eAgFnSc1d1	obdélná
stavba	stavba	k1gFnSc1	stavba
s	s	k7c7	s
pětibokým	pětiboký	k2eAgInSc7d1	pětiboký
závěrem	závěr	k1gInSc7	závěr
a	a	k8xC	a
věží	věž	k1gFnSc7	věž
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
západního	západní	k2eAgNnSc2d1	západní
průčelí	průčelí	k1gNnSc2	průčelí
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
patrně	patrně	k6eAd1	patrně
sloužil	sloužit	k5eAaImAgInS	sloužit
utrakvistům	utrakvista	k1gMnPc3	utrakvista
a	a	k8xC	a
Českým	český	k2eAgMnPc3d1	český
bratřím	bratr	k1gMnPc3	bratr
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1650	[number]	k4	1650
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
věž	věž	k1gFnSc1	věž
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
i	i	k9	i
většina	většina	k1gFnSc1	většina
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
hřbitovní	hřbitovní	k2eAgInSc1d1	hřbitovní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgInS	být
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
oceněn	oceněn	k2eAgInSc1d1	oceněn
titulem	titul	k1gInSc7	titul
Historické	historický	k2eAgNnSc1d1	historické
město	město	k1gNnSc1	město
roku	rok	k1gInSc2	rok
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nositelem	nositel	k1gMnSc7	nositel
Ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
přípravu	příprava	k1gFnSc4	příprava
a	a	k8xC	a
realizaci	realizace	k1gFnSc4	realizace
Programu	program	k1gInSc2	program
regenerace	regenerace	k1gFnSc2	regenerace
městských	městský	k2eAgFnPc2d1	městská
památkových	památkový	k2eAgFnPc2d1	památková
rezervací	rezervace	k1gFnPc2	rezervace
a	a	k8xC	a
zón	zóna	k1gFnPc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
má	mít	k5eAaImIp3nS	mít
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
čtvercové	čtvercový	k2eAgNnSc4d1	čtvercové
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
jako	jako	k9	jako
jediné	jediný	k2eAgNnSc1d1	jediné
v	v	k7c6	v
ČR	ČR	kA	ČR
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
obvodu	obvod	k1gInSc6	obvod
lemováno	lemovat	k5eAaImNgNnS	lemovat
podloubím	podloubí	k1gNnSc7	podloubí
pokračujícím	pokračující	k2eAgNnSc7d1	pokračující
i	i	k8xC	i
v	v	k7c6	v
postranních	postranní	k2eAgFnPc6d1	postranní
uličkách	ulička	k1gFnPc6	ulička
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
podloubí	podloubí	k1gNnSc2	podloubí
a	a	k8xC	a
morového	morový	k2eAgInSc2d1	morový
sloupu	sloup	k1gInSc2	sloup
<g/>
,	,	kIx,	,
prošlo	projít	k5eAaPmAgNnS	projít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
plochu	plocha	k1gFnSc4	plocha
náměstí	náměstí	k1gNnSc2	náměstí
přibyly	přibýt	k5eAaPmAgInP	přibýt
nové	nový	k2eAgInPc1d1	nový
vodní	vodní	k2eAgInPc1d1	vodní
prvky	prvek	k1gInPc1	prvek
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
nezpoplatněné	zpoplatněný	k2eNgFnSc2d1	nezpoplatněná
veřejné	veřejný	k2eAgFnSc2d1	veřejná
WC	WC	kA	WC
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
centra	centrum	k1gNnSc2	centrum
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dříve	dříve	k6eAd2	dříve
světoznámá	světoznámý	k2eAgFnSc1d1	světoznámá
továrna	továrna	k1gFnSc1	továrna
Tonak	tonak	k1gInSc1	tonak
a.s.	a.s.	k?	a.s.
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
let	léto	k1gNnPc2	léto
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
tradicí	tradice	k1gFnSc7	tradice
<g/>
;	;	kIx,	;
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
klobouky	klobouk	k1gInPc4	klobouk
a	a	k8xC	a
vyváží	vyvážet	k5eAaImIp3nS	vyvážet
je	on	k3xPp3gNnSc4	on
i	i	k9	i
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
do	do	k7c2	do
Štramberka	Štramberk	k1gInSc2	Štramberk
vede	vést	k5eAaImIp3nS	vést
Křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
délkou	délka	k1gFnSc7	délka
8,5	[number]	k4	8,5
kilometru	kilometr	k1gInSc2	kilometr
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejdelších	dlouhý	k2eAgFnPc2d3	nejdelší
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
otevřená	otevřený	k2eAgFnSc1d1	otevřená
obchodní	obchodní	k2eAgFnSc1d1	obchodní
galerie	galerie	k1gFnSc1	galerie
Tabačka	Tabačka	k1gFnSc1	Tabačka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Novojičínské	novojičínský	k2eAgFnSc2d1	novojičínská
tabákové	tabákový	k2eAgFnSc2d1	tabáková
továrny	továrna	k1gFnSc2	továrna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
najdete	najít	k5eAaPmIp2nP	najít
Beskydské	beskydský	k2eAgNnSc4d1	Beskydské
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
kino	kino	k1gNnSc4	kino
Květen	květena	k1gFnPc2	květena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Staré	Staré	k2eAgFnSc2d1	Staré
pošty	pošta	k1gFnSc2	pošta
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
galerie	galerie	k1gFnSc1	galerie
a	a	k8xC	a
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Žerotínského	žerotínský	k2eAgInSc2d1	žerotínský
zámku	zámek	k1gInSc2	zámek
najdete	najít	k5eAaPmIp2nP	najít
výstavu	výstava	k1gFnSc4	výstava
klobouků	klobouk	k1gInPc2	klobouk
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
expozice	expozice	k1gFnSc2	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
léto	léto	k1gNnSc4	léto
se	se	k3xPyFc4	se
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
konají	konat	k5eAaImIp3nP	konat
různé	různý	k2eAgFnPc1d1	různá
kulturní	kulturní	k2eAgFnPc1d1	kulturní
akce	akce	k1gFnPc1	akce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
završeny	završit	k5eAaPmNgInP	završit
každoročními	každoroční	k2eAgFnPc7d1	každoroční
Slavnostmi	slavnost	k1gFnPc7	slavnost
města	město	k1gNnSc2	město
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
vždy	vždy	k6eAd1	vždy
první	první	k4xOgFnSc4	první
sobotu	sobota	k1gFnSc4	sobota
v	v	k7c6	v
září	září	k1gNnSc6	září
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
historický	historický	k2eAgInSc1d1	historický
průvod	průvod	k1gInSc1	průvod
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
představitelů	představitel	k1gMnPc2	představitel
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
náměstí	náměstí	k1gNnSc1	náměstí
se	se	k3xPyFc4	se
přenese	přenést	k5eAaPmIp3nS	přenést
do	do	k7c2	do
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
návštěvníci	návštěvník	k1gMnPc1	návštěvník
uvidí	uvidět	k5eAaPmIp3nP	uvidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
bojovalo	bojovat	k5eAaImAgNnS	bojovat
<g/>
,	,	kIx,	,
bavilo	bavit	k5eAaImAgNnS	bavit
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
den	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
zakončen	zakončit	k5eAaPmNgInS	zakončit
ohňostrojem	ohňostroj	k1gInSc7	ohňostroj
a	a	k8xC	a
večerním	večerní	k2eAgNnSc7d1	večerní
vystoupením	vystoupení	k1gNnSc7	vystoupení
určité	určitý	k2eAgFnSc2d1	určitá
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
Peha	Peha	k1gFnSc1	Peha
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
Miro	Mira	k1gFnSc5	Mira
Žbirka	Žbirek	k1gMnSc4	Žbirek
a	a	k8xC	a
Mandrage	Mandrag	k1gMnSc4	Mandrag
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
UDG	UDG	kA	UDG
a	a	k8xC	a
Čechomor	Čechomor	k1gInSc1	Čechomor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
též	též	k9	též
funguje	fungovat	k5eAaImIp3nS	fungovat
světově	světově	k6eAd1	světově
uznávaný	uznávaný	k2eAgInSc1d1	uznávaný
dětský	dětský	k2eAgInSc1d1	dětský
pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
sbor	sbor	k1gInSc1	sbor
Ondrášek	Ondráška	k1gFnPc2	Ondráška
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
profesorem	profesor	k1gMnSc7	profesor
Václavem	Václav	k1gMnSc7	Václav
Ptáčkem	Ptáček	k1gMnSc7	Ptáček
<g/>
.	.	kIx.	.
</s>
<s>
Sbor	sbor	k1gInSc1	sbor
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
účastní	účastnit	k5eAaImIp3nS	účastnit
domácích	domácí	k2eAgFnPc2d1	domácí
i	i	k8xC	i
světových	světový	k2eAgFnPc2d1	světová
pěveckých	pěvecký	k2eAgFnPc2d1	pěvecká
soutěží	soutěž	k1gFnPc2	soutěž
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
četnými	četný	k2eAgInPc7d1	četný
úspěchy	úspěch	k1gInPc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Sbor	sbor	k1gInSc1	sbor
vydal	vydat	k5eAaPmAgInS	vydat
osm	osm	k4xCc4	osm
samostatných	samostatný	k2eAgFnPc2d1	samostatná
CD	CD	kA	CD
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
neodmyslitelnou	odmyslitelný	k2eNgFnSc7d1	neodmyslitelná
součástí	součást	k1gFnSc7	součást
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Uměleckým	umělecký	k2eAgMnSc7d1	umělecký
vedoucím	vedoucí	k1gMnSc7	vedoucí
a	a	k8xC	a
sbormistrem	sbormistr	k1gMnSc7	sbormistr
je	být	k5eAaImIp3nS	být
Josef	Josef	k1gMnSc1	Josef
Zajíček	Zajíček	k1gMnSc1	Zajíček
<g/>
.	.	kIx.	.
</s>
<s>
Sportem	sport	k1gInSc7	sport
číslo	číslo	k1gNnSc1	číslo
jedna	jeden	k4xCgFnSc1	jeden
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
basketbal	basketbal	k1gInSc1	basketbal
<g/>
,	,	kIx,	,
tým	tým	k1gInSc1	tým
BC	BC	kA	BC
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
ale	ale	k8xC	ale
po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
ukončil	ukončit	k5eAaPmAgMnS	ukončit
své	svůj	k3xOyFgNnSc4	svůj
působení	působení	k1gNnSc4	působení
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
lize	liga	k1gFnSc6	liga
Mattoni	Matton	k1gMnPc1	Matton
NBL	NBL	kA	NBL
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
nedostatku	nedostatek	k1gInSc2	nedostatek
financí	finance	k1gFnPc2	finance
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
sezonu	sezona	k1gFnSc4	sezona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
lize	liga	k1gFnSc6	liga
tak	tak	k6eAd1	tak
odehrál	odehrát	k5eAaPmAgMnS	odehrát
19	[number]	k4	19
sezón	sezóna	k1gFnPc2	sezóna
<g/>
,	,	kIx,	,
859	[number]	k4	859
ligových	ligový	k2eAgInPc2d1	ligový
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
jeden	jeden	k4xCgInSc1	jeden
titul	titul	k1gInSc1	titul
Mistra	mistr	k1gMnSc2	mistr
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
osmkrát	osmkrát	k6eAd1	osmkrát
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
pětkrát	pětkrát	k6eAd1	pětkrát
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
ČP.	ČP.	k1gFnSc2	ČP.
Klub	klub	k1gInSc1	klub
proto	proto	k8xC	proto
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
lize	liga	k1gFnSc6	liga
Poměrně	poměrně	k6eAd1	poměrně
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
jsou	být	k5eAaImIp3nP	být
novojičínští	novojičínský	k2eAgMnPc1d1	novojičínský
sportovci	sportovec	k1gMnPc1	sportovec
také	také	k9	také
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
–	–	k?	–
HC	HC	kA	HC
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
snaží	snažit	k5eAaImIp3nP	snažit
bojovat	bojovat	k5eAaImF	bojovat
o	o	k7c4	o
play-off	playff	k1gInSc4	play-off
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
lize	liga	k1gFnSc6	liga
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgFnSc1	třetí
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
FK	FK	kA	FK
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
se	se	k3xPyFc4	se
po	po	k7c6	po
spojení	spojení	k1gNnSc6	spojení
a	a	k8xC	a
opětovném	opětovný	k2eAgNnSc6d1	opětovné
rozdělení	rozdělení	k1gNnSc6	rozdělení
s	s	k7c7	s
FC	FC	kA	FC
Vítkovice	Vítkovice	k1gInPc4	Vítkovice
propadl	propadnout	k5eAaPmAgInS	propadnout
až	až	k9	až
na	na	k7c4	na
samé	samý	k3xTgNnSc4	samý
dno	dno	k1gNnSc4	dno
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
však	však	k9	však
opět	opět	k6eAd1	opět
stoupá	stoupat	k5eAaImIp3nS	stoupat
vzhůru	vzhůru	k6eAd1	vzhůru
a	a	k8xC	a
od	od	k7c2	od
sezony	sezona	k1gFnSc2	sezona
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
Moravskoslezské	moravskoslezský	k2eAgFnSc6d1	Moravskoslezská
divizi	divize	k1gFnSc6	divize
E	E	kA	E
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
Žerotínové	Žerotínová	k1gFnSc2	Žerotínová
Hugo	Hugo	k1gMnSc1	Hugo
Baar	Baar	k1gMnSc1	Baar
–	–	k?	–
malíř	malíř	k1gMnSc1	malíř
Miloslav	Miloslav	k1gMnSc1	Miloslav
Baláš	Baláš	k1gMnSc1	Baláš
–	–	k?	–
spisovatel	spisovatel	k1gMnSc1	spisovatel
Božena	Božena	k1gFnSc1	Božena
Benešová	Benešová	k1gFnSc1	Benešová
–	–	k?	–
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Anton	Anton	k1gMnSc1	Anton
Berger	Berger	k1gMnSc1	Berger
–	–	k?	–
malíř	malíř	k1gMnSc1	malíř
Ignác	Ignác	k1gMnSc1	Ignác
Johann	Johann	k1gMnSc1	Johann
Berger	Berger	k1gMnSc1	Berger
–	–	k?	–
malíř	malíř	k1gMnSc1	malíř
Julius	Julius	k1gMnSc1	Julius
Valentin	Valentin	k1gMnSc1	Valentin
Berger	Berger	k1gMnSc1	Berger
–	–	k?	–
malíř	malíř	k1gMnSc1	malíř
Jan	Jan	k1gMnSc1	Jan
Brod	Brod	k1gInSc1	Brod
–	–	k?	–
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
nefrolog	nefrolog	k1gMnSc1	nefrolog
Otakar	Otakar	k1gMnSc1	Otakar
Bystřina	bystřina	k1gFnSc1	bystřina
–	–	k?	–
spisovatel	spisovatel	k1gMnSc1	spisovatel
Max	Max	k1gMnSc1	Max
Czeike	Czeik	k1gFnSc2	Czeik
–	–	k?	–
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
<g />
.	.	kIx.	.
</s>
<s>
1910	[number]	k4	1910
až	až	k6eAd1	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
slovinském	slovinský	k2eAgInSc6d1	slovinský
Mariboru	Maribor	k1gInSc6	Maribor
A.	A.	kA	A.
F.	F.	kA	F.
Dopper	Dopper	k1gMnSc1	Dopper
–	–	k?	–
použil	použít	k5eAaPmAgMnS	použít
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
továrně	továrna	k1gFnSc6	továrna
Karel	Karel	k1gMnSc1	Karel
Dostál-Lutinov	Dostál-Lutinovo	k1gNnPc2	Dostál-Lutinovo
–	–	k?	–
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Antonín	Antonín	k1gMnSc1	Antonín
Kolig	Kolig	k1gMnSc1	Kolig
–	–	k?	–
malíř	malíř	k1gMnSc1	malíř
Karel	Karel	k1gMnSc1	Karel
Kryl	Kryl	k1gMnSc1	Kryl
–	–	k?	–
prožil	prožít	k5eAaPmAgMnS	prožít
zde	zde	k6eAd1	zde
své	svůj	k3xOyFgNnSc4	svůj
mládí	mládí	k1gNnSc4	mládí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
vojenské	vojenský	k2eAgFnSc2d1	vojenská
<g />
.	.	kIx.	.
</s>
<s>
intervence	intervence	k1gFnSc1	intervence
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
také	také	k9	také
složil	složit	k5eAaPmAgMnS	složit
legendární	legendární	k2eAgFnSc4d1	legendární
píseň	píseň	k1gFnSc4	píseň
Bratříčku	bratříček	k1gMnSc3	bratříček
<g/>
,	,	kIx,	,
zavírej	zavírat	k5eAaImRp2nS	zavírat
vrátka	vrátka	k1gNnPc4	vrátka
Max	Max	k1gMnSc1	Max
Mannheimer	Mannheimer	k1gMnSc1	Mannheimer
–	–	k?	–
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
osvětimský	osvětimský	k2eAgMnSc1d1	osvětimský
vězeň	vězeň	k1gMnSc1	vězeň
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přežil	přežít	k5eAaPmAgMnS	přežít
holokaust	holokaust	k1gInSc4	holokaust
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
<g/>
,	,	kIx,	,
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
sudetoněmeckého	sudetoněmecký	k2eAgInSc2d1	sudetoněmecký
původu	původ	k1gInSc2	původ
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
do	do	k7c2	do
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g />
.	.	kIx.	.
</s>
<s>
Lenka	Lenka	k1gFnSc1	Lenka
Masná	masný	k2eAgFnSc1d1	Masná
–	–	k?	–
atletka	atletka	k1gFnSc1	atletka
<g/>
,	,	kIx,	,
olympionička	olympionička	k1gFnSc1	olympionička
Karel	Karel	k1gMnSc1	Karel
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Maška	Mašek	k1gMnSc2	Mašek
–	–	k?	–
archeolog	archeolog	k1gMnSc1	archeolog
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
šipecké	šipecký	k2eAgFnSc2d1	šipecký
čelisti	čelist	k1gFnSc2	čelist
Alfred	Alfred	k1gMnSc1	Alfred
Neubauer	Neubauer	k1gMnSc1	Neubauer
–	–	k?	–
slavný	slavný	k2eAgMnSc1d1	slavný
manažer	manažer	k1gMnSc1	manažer
Mercedes	mercedes	k1gInSc1	mercedes
<g/>
,	,	kIx,	,
rodák	rodák	k1gMnSc1	rodák
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
Eduard	Eduard	k1gMnSc1	Eduard
Orel	Orel	k1gMnSc1	Orel
–	–	k?	–
námořní	námořní	k2eAgMnSc1d1	námořní
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
polární	polární	k2eAgMnSc1d1	polární
badatel	badatel	k1gMnSc1	badatel
Ondřej	Ondřej	k1gMnSc1	Ondřej
Pavelka	Pavelka	k1gMnSc1	Pavelka
–	–	k?	–
herec	herec	k1gMnSc1	herec
Vlasta	Vlasta	k1gMnSc1	Vlasta
Redl	Redl	k1gMnSc1	Redl
–	–	k?	–
zpěvák	zpěvák	k1gMnSc1	zpěvák
Ondřej	Ondřej	k1gMnSc1	Ondřej
Řepa	Řepa	k1gMnSc1	Řepa
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
Greiffensdorfu	Greiffensdorf	k1gInSc2	Greiffensdorf
–	–	k?	–
nejznámější	známý	k2eAgMnSc1d3	nejznámější
měšťan	měšťan	k1gMnSc1	měšťan
Jan	Jan	k1gMnSc1	Jan
Šrámek	Šrámek	k1gMnSc1	Šrámek
–	–	k?	–
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
ČSL	ČSL	kA	ČSL
Otto	Otto	k1gMnSc1	Otto
Thienemann	Thienemann	k1gMnSc1	Thienemann
–	–	k?	–
dvorní	dvorní	k2eAgMnSc1d1	dvorní
architekt	architekt	k1gMnSc1	architekt
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
Eduard	Eduard	k1gMnSc1	Eduard
Veith	Veith	k1gMnSc1	Veith
–	–	k?	–
malíř	malíř	k1gMnSc1	malíř
Otakar	Otakar	k1gMnSc1	Otakar
Zelenka	Zelenka	k1gMnSc1	Zelenka
–	–	k?	–
malíř	malíř	k1gMnSc1	malíř
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1787	[number]	k4	1787
přenocoval	přenocovat	k5eAaPmAgMnS	přenocovat
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
Ernst	Ernst	k1gMnSc1	Ernst
Gideon	Gideona	k1gFnPc2	Gideona
von	von	k1gInSc1	von
Laudon	Laudon	k1gMnSc1	Laudon
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g />
.	.	kIx.	.
</s>
<s>
1790	[number]	k4	1790
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
rohovém	rohový	k2eAgInSc6d1	rohový
domě	dům	k1gInSc6	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
restaurace	restaurace	k1gFnSc1	restaurace
U	u	k7c2	u
Laudona	Laudon	k1gMnSc2	Laudon
<g/>
)	)	kIx)	)
hrabě	hrabě	k1gMnSc1	hrabě
Alexandr	Alexandr	k1gMnSc1	Alexandr
Vasiljevič	Vasiljevič	k1gMnSc1	Vasiljevič
Suvorov	Suvorovo	k1gNnPc2	Suvorovo
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1800	[number]	k4	1800
přespal	přespat	k5eAaPmAgMnS	přespat
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Staré	Staré	k2eAgFnSc2d1	Staré
pošty	pošta	k1gFnSc2	pošta
maršál	maršál	k1gMnSc1	maršál
Michail	Michail	k1gMnSc1	Michail
Kutuzov	Kutuzov	k1gInSc1	Kutuzov
–	–	k?	–
roku	rok	k1gInSc2	rok
1805	[number]	k4	1805
přespal	přespat	k5eAaPmAgMnS	přespat
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Staré	Staré	k2eAgFnSc2d1	Staré
pošty	pošta	k1gFnSc2	pošta
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
zde	zde	k6eAd1	zde
<g />
.	.	kIx.	.
</s>
<s>
přednášel	přednášet	k5eAaImAgMnS	přednášet
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gNnSc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
každoročně	každoročně	k6eAd1	každoročně
zde	zde	k6eAd1	zde
pobýval	pobývat	k5eAaImAgMnS	pobývat
a	a	k8xC	a
přednášel	přednášet	k5eAaImAgMnS	přednášet
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
–	–	k?	–
jako	jako	k8xS	jako
prezident	prezident	k1gMnSc1	prezident
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1999	[number]	k4	1999
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
–	–	k?	–
jako	jako	k8xS	jako
prezident	prezident	k1gMnSc1	prezident
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2003	[number]	k4	2003
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2006	[number]	k4	2006
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
socialismu	socialismus	k1gInSc2	socialismus
byl	být	k5eAaImAgInS	být
jediným	jediný	k2eAgInSc7d1	jediný
zdrojem	zdroj	k1gInSc7	zdroj
určitých	určitý	k2eAgFnPc2d1	určitá
informací	informace	k1gFnPc2	informace
okresní	okresní	k2eAgInSc1d1	okresní
týdeník	týdeník	k1gInSc1	týdeník
OV	OV	kA	OV
KSČ	KSČ	kA	KSČ
Rozkvět	rozkvět	k1gInSc1	rozkvět
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
koncem	koncem	k7c2	koncem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
jen	jen	k9	jen
týdeník	týdeník	k1gInSc1	týdeník
Region	region	k1gInSc1	region
(	(	kIx(	(
<g/>
založen	založit	k5eAaPmNgInS	založit
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
patřící	patřící	k2eAgInSc1d1	patřící
vydavatelskému	vydavatelský	k2eAgInSc3d1	vydavatelský
domu	dům	k1gInSc3	dům
Vltava	Vltava	k1gFnSc1	Vltava
Labe	Labe	k1gNnSc2	Labe
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
o	o	k7c6	o
životě	život	k1gInSc6	život
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
píše	psát	k5eAaImIp3nS	psát
jeho	jeho	k3xOp3gMnSc1	jeho
následovník	následovník	k1gMnSc1	následovník
Novojičínský	novojičínský	k2eAgInSc1d1	novojičínský
deník	deník	k1gInSc1	deník
a	a	k8xC	a
měsíčník	měsíčník	k1gInSc1	měsíčník
Novojičínský	novojičínský	k2eAgInSc1d1	novojičínský
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
<g/>
,	,	kIx,	,
vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
městským	městský	k2eAgInSc7d1	městský
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dění	dění	k1gNnSc6	dění
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
se	se	k3xPyFc4	se
dočtete	dočíst	k5eAaPmIp2nP	dočíst
i	i	k9	i
v	v	k7c6	v
regionální	regionální	k2eAgFnSc6d1	regionální
příloze	příloha	k1gFnSc6	příloha
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
DNES	dnes	k6eAd1	dnes
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
celostátních	celostátní	k2eAgInPc6d1	celostátní
denících	deník	k1gInPc6	deník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
působí	působit	k5eAaImIp3nS	působit
již	již	k6eAd1	již
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
i	i	k9	i
televize	televize	k1gFnSc1	televize
Polar	Polara	k1gFnPc2	Polara
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
provoz	provoz	k1gInSc4	provoz
přispívá	přispívat	k5eAaImIp3nS	přispívat
městský	městský	k2eAgInSc1d1	městský
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
vysílá	vysílat	k5eAaImIp3nS	vysílat
jak	jak	k6eAd1	jak
digitálně	digitálně	k6eAd1	digitálně
terestricky	terestricky	k6eAd1	terestricky
(	(	kIx(	(
<g/>
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
kabelově	kabelově	k6eAd1	kabelově
<g/>
.	.	kIx.	.
</s>
<s>
Archiv	archiv	k1gInSc1	archiv
televizních	televizní	k2eAgFnPc2d1	televizní
zpráv	zpráva	k1gFnPc2	zpráva
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
září	září	k1gNnSc2	září
2007	[number]	k4	2007
dostupný	dostupný	k2eAgMnSc1d1	dostupný
i	i	k8xC	i
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Rybí	rybí	k2eAgNnSc1d1	rybí
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
zachytit	zachytit	k5eAaPmF	zachytit
digitální	digitální	k2eAgInSc4d1	digitální
terestrický	terestrický	k2eAgInSc4d1	terestrický
televizní	televizní	k2eAgInSc4d1	televizní
signál	signál	k1gInSc4	signál
z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
léto	léto	k1gNnSc4	léto
do	do	k7c2	do
města	město	k1gNnSc2	město
míří	mířit	k5eAaImIp3nS	mířit
mnoho	mnoho	k4c1	mnoho
turistů	turist	k1gMnPc2	turist
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Autobusové	autobusový	k2eAgNnSc1d1	autobusové
nádraží	nádraží	k1gNnSc1	nádraží
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
kompletně	kompletně	k6eAd1	kompletně
zbouráno	zbourán	k2eAgNnSc1d1	zbouráno
a	a	k8xC	a
místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
nové	nový	k2eAgNnSc1d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2006	[number]	k4	2006
převzaly	převzít	k5eAaPmAgInP	převzít
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
nový	nový	k2eAgInSc4d1	nový
systém	systém	k1gInSc4	systém
zavedly	zavést	k5eAaPmAgFnP	zavést
Technické	technický	k2eAgFnPc1d1	technická
služby	služba	k1gFnPc1	služba
města	město	k1gNnSc2	město
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jezdí	jezdit	k5eAaImIp3nP	jezdit
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
čtvrtí	čtvrt	k1gFnPc2	čtvrt
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
Straníku	straník	k1gMnSc6	straník
<g/>
)	)	kIx)	)
a	a	k8xC	a
ne	ne	k9	ne
jen	jen	k9	jen
po	po	k7c6	po
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
kdysi	kdysi	k6eAd1	kdysi
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Linky	linka	k1gFnPc1	linka
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
provozují	provozovat	k5eAaImIp3nP	provozovat
Technické	technický	k2eAgFnPc1d1	technická
služby	služba	k1gFnPc1	služba
města	město	k1gNnSc2	město
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
<g/>
,	,	kIx,	,
linku	linka	k1gFnSc4	linka
7	[number]	k4	7
Veolia	Veolia	k1gFnSc1	Veolia
Transport	transporta	k1gFnPc2	transporta
Morava	Morava	k1gFnSc1	Morava
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
provozovala	provozovat	k5eAaImAgFnS	provozovat
původní	původní	k2eAgFnPc4d1	původní
linky	linka	k1gFnPc4	linka
MHD	MHD	kA	MHD
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
1	[number]	k4	1
<g/>
:	:	kIx,	:
Aut	aut	k1gInSc1	aut
<g/>
.	.	kIx.	.
<g/>
nádr	nádr	k1gInSc1	nádr
<g/>
.	.	kIx.	.
-	-	kIx~	-
Autopal	Autopal	k1gFnSc1	Autopal
-	-	kIx~	-
Hřbitovní	hřbitovní	k2eAgInSc1d1	hřbitovní
-	-	kIx~	-
Aut	aut	k1gInSc1	aut
<g/>
.	.	kIx.	.
nádr	nádr	k1gInSc1	nádr
<g/>
.	.	kIx.	.
-	-	kIx~	-
Nemocnice	nemocnice	k1gFnSc1	nemocnice
-	-	kIx~	-
Loučka	loučka	k1gFnSc1	loučka
-	-	kIx~	-
Dlouhá	Dlouhá	k1gFnSc1	Dlouhá
-	-	kIx~	-
Aut	aut	k1gInSc1	aut
<g/>
.	.	kIx.	.
nádr	nádr	k1gInSc1	nádr
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
2	[number]	k4	2
<g/>
:	:	kIx,	:
Aut	aut	k1gInSc1	aut
<g/>
.	.	kIx.	.
<g/>
nádr	nádr	k1gInSc1	nádr
<g/>
.	.	kIx.	.
-	-	kIx~	-
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
-	-	kIx~	-
Loučka	loučka	k1gFnSc1	loučka
-	-	kIx~	-
Nemocnice	nemocnice	k1gFnSc1	nemocnice
-	-	kIx~	-
Aut	aut	k1gInSc1	aut
<g/>
.	.	kIx.	.
nádr	nádr	k1gInSc1	nádr
<g/>
.	.	kIx.	.
-	-	kIx~	-
Máchova	Máchův	k2eAgFnSc1d1	Máchova
-	-	kIx~	-
Autopal	Autopal	k1gFnSc1	Autopal
-	-	kIx~	-
Aut	aut	k1gInSc1	aut
<g/>
.	.	kIx.	.
nádr	nádr	k1gInSc1	nádr
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
3	[number]	k4	3
<g/>
:	:	kIx,	:
Aut	aut	k1gInSc1	aut
<g/>
.	.	kIx.	.
<g/>
nádr	nádr	k1gInSc1	nádr
<g/>
.	.	kIx.	.
-	-	kIx~	-
Palackého	Palackého	k2eAgFnSc1d1	Palackého
-	-	kIx~	-
Loučka	loučka	k1gFnSc1	loučka
-	-	kIx~	-
nemocnice	nemocnice	k1gFnSc1	nemocnice
-	-	kIx~	-
Aut	aut	k1gInSc1	aut
<g/>
.	.	kIx.	.
<g/>
nádr	nádr	k1gInSc1	nádr
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
4	[number]	k4	4
<g/>
:	:	kIx,	:
Aut	aut	k1gInSc1	aut
<g/>
.	.	kIx.	.
<g/>
nádr	nádr	k1gInSc1	nádr
<g/>
.	.	kIx.	.
-	-	kIx~	-
Smetanovy	Smetanův	k2eAgFnPc1d1	Smetanova
sady	sada	k1gFnPc1	sada
/	/	kIx~	/
nemocnice	nemocnice	k1gFnPc1	nemocnice
-	-	kIx~	-
Skalky	skalka	k1gFnPc1	skalka
-	-	kIx~	-
Kojetín	Kojetín	k1gInSc1	Kojetín
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
V	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
fungovala	fungovat	k5eAaImAgFnS	fungovat
až	až	k6eAd1	až
do	do	k7c2	do
povodně	povodeň	k1gFnSc2	povodeň
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
dvě	dva	k4xCgNnPc4	dva
nádraží	nádraží	k1gNnPc4	nádraží
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nebyla	být	k5eNaImAgNnP	být
na	na	k7c6	na
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
propojena	propojen	k2eAgFnSc1d1	propojena
(	(	kIx(	(
<g/>
obdobná	obdobný	k2eAgFnSc1d1	obdobná
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
např.	např.	kA	např.
v	v	k7c6	v
Křenovicích	Křenovice	k1gFnPc6	Křenovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
jejich	jejich	k3xOp3gFnSc3	jejich
pouhé	pouhý	k2eAgFnSc3d1	pouhá
dvojkilometrové	dvojkilometrový	k2eAgFnSc3d1	dvojkilometrový
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
a	a	k8xC	a
historickým	historický	k2eAgInPc3d1	historický
plánům	plán	k1gInPc3	plán
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
propojení	propojení	k1gNnSc1	propojení
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
i	i	k8xC	i
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
husté	hustý	k2eAgFnSc3d1	hustá
zástavbě	zástavba	k1gFnSc3	zástavba
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nádraží	nádraží	k1gNnSc1	nádraží
Nový	nový	k2eAgMnSc1d1	nový
Jičín-město	Jičíněsta	k1gMnSc5	Jičín-města
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
278	[number]	k4	278
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
vedle	vedle	k7c2	vedle
autobusového	autobusový	k2eAgNnSc2d1	autobusové
nádraží	nádraží	k1gNnSc2	nádraží
a	a	k8xC	a
jezdí	jezdit	k5eAaImIp3nS	jezdit
odsud	odsud	k6eAd1	odsud
lokálka	lokálka	k1gFnSc1	lokálka
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Suchdol	Suchdol	k1gInSc4	Suchdol
nad	nad	k7c7	nad
Odrou	Odra	k1gFnSc7	Odra
(	(	kIx(	(
<g/>
doba	doba	k1gFnSc1	doba
jízdy	jízda	k1gFnSc2	jízda
je	být	k5eAaImIp3nS	být
14	[number]	k4	14
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
prochází	procházet	k5eAaImIp3nS	procházet
hlavní	hlavní	k2eAgFnSc4d1	hlavní
trať	trať	k1gFnSc4	trať
270	[number]	k4	270
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Bohumína	Bohumín	k1gInSc2	Bohumín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Suchdole	Suchdol	k1gInSc6	Suchdol
staví	stavit	k5eAaBmIp3nS	stavit
většina	většina	k1gFnSc1	většina
rychlíků	rychlík	k1gMnPc2	rychlík
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
vlaky	vlak	k1gInPc4	vlak
IC	IC	kA	IC
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
vlakem	vlak	k1gInSc7	vlak
z	z	k7c2	z
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
do	do	k7c2	do
Ostravy-Svinova	Ostravy-Svinův	k2eAgMnSc2d1	Ostravy-Svinův
trvá	trvat	k5eAaImIp3nS	trvat
i	i	k8xC	i
s	s	k7c7	s
přestupem	přestup	k1gInSc7	přestup
38	[number]	k4	38
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
trať	trať	k1gFnSc4	trať
do	do	k7c2	do
Suchdola	Suchdola	k1gFnSc1	Suchdola
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
nasazena	nasazen	k2eAgFnSc1d1	nasazena
nová	nový	k2eAgFnSc1d1	nová
souprava	souprava	k1gFnSc1	souprava
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1	Moravskoslezská
Regionova	Regionův	k2eAgFnSc1d1	Regionova
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
cestování	cestování	k1gNnPc1	cestování
značně	značně	k6eAd1	značně
zpříjemnila	zpříjemnit	k5eAaPmAgNnP	zpříjemnit
a	a	k8xC	a
také	také	k6eAd1	také
učinila	učinit	k5eAaPmAgFnS	učinit
bezbariérovým	bezbariérový	k2eAgInSc7d1	bezbariérový
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Nového	Nového	k2eAgNnSc2d1	Nového
Jičína-horního	Jičínaorní	k2eAgNnSc2d1	Jičína-horní
nádraží	nádraží	k1gNnSc2	nádraží
jezdila	jezdit	k5eAaImAgFnS	jezdit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
také	také	k6eAd1	také
starší	starý	k2eAgFnSc1d2	starší
lokálka	lokálka	k1gFnSc1	lokálka
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Hostašovice	Hostašovice	k1gFnSc2	Hostašovice
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
326	[number]	k4	326
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
napojovala	napojovat	k5eAaImAgFnS	napojovat
na	na	k7c4	na
trať	trať	k1gFnSc4	trať
303	[number]	k4	303
z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
do	do	k7c2	do
Valašského	valašský	k2eAgNnSc2d1	Valašské
Meziříčí	Meziříčí	k1gNnSc2	Meziříčí
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
jízdy	jízda	k1gFnSc2	jízda
byla	být	k5eAaImAgFnS	být
18	[number]	k4	18
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc1	trať
byla	být	k5eAaImAgFnS	být
vážně	vážně	k6eAd1	vážně
poškozena	poškodit	k5eAaPmNgFnS	poškodit
při	při	k7c6	při
bleskové	bleskový	k2eAgFnSc6d1	blesková
povodni	povodeň	k1gFnSc6	povodeň
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
a	a	k8xC	a
provoz	provoz	k1gInSc4	provoz
byl	být	k5eAaImAgMnS	být
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
zastaven	zastavit	k5eAaPmNgMnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vleklých	vleklý	k2eAgFnPc6d1	vleklá
diskusích	diskuse	k1gFnPc6	diskuse
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2010	[number]	k4	2010
Správa	správa	k1gFnSc1	správa
železniční	železniční	k2eAgFnSc2d1	železniční
dopravní	dopravní	k2eAgFnSc2d1	dopravní
cesty	cesta	k1gFnSc2	cesta
financovat	financovat	k5eAaBmF	financovat
obnovu	obnova	k1gFnSc4	obnova
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
krajský	krajský	k2eAgInSc1d1	krajský
úřad	úřad	k1gInSc1	úřad
nebyl	být	k5eNaImAgInS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
poskytnout	poskytnout	k5eAaPmF	poskytnout
záruku	záruka	k1gFnSc4	záruka
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
bude	být	k5eAaImBp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
objednávat	objednávat	k5eAaImF	objednávat
provoz	provoz	k1gInSc4	provoz
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
záruky	záruka	k1gFnSc2	záruka
by	by	kYmCp3nS	by
hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
investice	investice	k1gFnSc1	investice
<g/>
,	,	kIx,	,
odhadovaná	odhadovaný	k2eAgFnSc1d1	odhadovaná
na	na	k7c4	na
82	[number]	k4	82
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
přijde	přijít	k5eAaPmIp3nS	přijít
vniveč	vniveč	k6eAd1	vniveč
<g/>
.	.	kIx.	.
</s>
<s>
Vytíženost	vytíženost	k1gFnSc1	vytíženost
spojů	spoj	k1gInPc2	spoj
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
nízká	nízký	k2eAgFnSc1d1	nízká
a	a	k8xC	a
náhradou	náhrada	k1gFnSc7	náhrada
vlaků	vlak	k1gInPc2	vlak
autobusy	autobus	k1gInPc7	autobus
ušetří	ušetřit	k5eAaPmIp3nS	ušetřit
kraj	kraj	k1gInSc4	kraj
asi	asi	k9	asi
4	[number]	k4	4
miliony	milion	k4xCgInPc1	milion
Kč	Kč	kA	Kč
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
usilovalo	usilovat	k5eAaImAgNnS	usilovat
především	především	k9	především
město	město	k1gNnSc4	město
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
<g/>
;	;	kIx,	;
další	další	k2eAgFnPc1d1	další
obce	obec	k1gFnPc1	obec
ležící	ležící	k2eAgFnSc1d1	ležící
podél	podél	k7c2	podél
trati	trať	k1gFnSc2	trať
preferovaly	preferovat	k5eAaImAgInP	preferovat
využití	využití	k1gNnSc4	využití
pozemku	pozemek	k1gInSc2	pozemek
pro	pro	k7c4	pro
cyklostezku	cyklostezka	k1gFnSc4	cyklostezka
<g/>
.	.	kIx.	.
</s>
<s>
Citáty	citát	k1gInPc1	citát
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgInP	použít
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
Městský	městský	k2eAgInSc1d1	městský
úřad	úřad	k1gInSc1	úřad
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Repronis	Repronis	k1gFnSc2	Repronis
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-902142-1-5	[number]	k4	80-902142-1-5
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
je	být	k5eAaImIp3nS	být
krásné	krásný	k2eAgNnSc4d1	krásné
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
jeden	jeden	k4xCgInSc1	jeden
nedostatek	nedostatek	k1gInSc1	nedostatek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
známé	známý	k2eAgNnSc1d1	známé
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
hovoří	hovořit	k5eAaImIp3nS	hovořit
i	i	k9	i
návštěvník	návštěvník	k1gMnSc1	návštěvník
z	z	k7c2	z
blízkých	blízký	k2eAgNnPc2d1	blízké
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
velmi	velmi	k6eAd1	velmi
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
úžasu	úžas	k1gInSc6	úžas
stane	stanout	k5eAaPmIp3nS	stanout
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
náladu	nálada	k1gFnSc4	nálada
sotva	sotva	k6eAd1	sotva
jinde	jinde	k6eAd1	jinde
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
novojičínské	novojičínský	k2eAgNnSc1d1	Novojičínské
náměstí	náměstí	k1gNnSc1	náměstí
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
nejkrásnějším	krásný	k2eAgMnSc7d3	nejkrásnější
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
citát	citát	k1gInSc4	citát
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1661	[number]	k4	1661
nad	nad	k7c7	nad
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
obřadní	obřadní	k2eAgFnSc2d1	obřadní
místnosti	místnost	k1gFnSc2	místnost
na	na	k7c4	na
Radnici	radnice	k1gFnSc4	radnice
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
psán	psán	k2eAgInSc1d1	psán
latinsky	latinsky	k6eAd1	latinsky
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Buď	buď	k8xC	buď
bdělý	bdělý	k2eAgMnSc1d1	bdělý
<g/>
,	,	kIx,	,
města	město	k1gNnSc2	město
milující	milující	k2eAgFnSc2d1	milující
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
ve	v	k7c6	v
všem	všecek	k3xTgNnSc6	všecek
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
<g/>
.	.	kIx.	.
</s>
<s>
Moudrosti	moudrost	k1gFnPc4	moudrost
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
<g/>
,	,	kIx,	,
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
překračuje	překračovat	k5eAaImIp3nS	překračovat
tento	tento	k3xDgInSc4	tento
práh	práh	k1gInSc4	práh
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
starosta	starosta	k1gMnSc1	starosta
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
má	mít	k5eAaImIp3nS	mít
29	[number]	k4	29
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
dohody	dohoda	k1gFnSc2	dohoda
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
devět	devět	k4xCc4	devět
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
je	být	k5eAaImIp3nS	být
starostou	starosta	k1gMnSc7	starosta
města	město	k1gNnSc2	město
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
Břetislav	Břetislav	k1gMnSc1	Břetislav
Gelnar	Gelnar	k1gMnSc1	Gelnar
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
městské	městský	k2eAgFnSc2d1	městská
samosprávy	samospráva	k1gFnSc2	samospráva
i	i	k9	i
po	po	k7c6	po
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
tvořily	tvořit	k5eAaImAgFnP	tvořit
koalici	koalice	k1gFnSc4	koalice
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
a	a	k8xC	a
Strana	strana	k1gFnSc1	strana
zelených	zelená	k1gFnPc2	zelená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
radniční	radniční	k2eAgFnSc4d1	radniční
koalici	koalice	k1gFnSc4	koalice
opustila	opustit	k5eAaPmAgFnS	opustit
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zbylé	zbylý	k2eAgFnPc1d1	zbylá
dvě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
nadále	nadále	k6eAd1	nadále
disponovaly	disponovat	k5eAaBmAgFnP	disponovat
nadpoloviční	nadpoloviční	k2eAgFnSc7d1	nadpoloviční
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
starostů	starosta	k1gMnPc2	starosta
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
šest	šest	k4xCc4	šest
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
na	na	k7c6	na
osmi	osm	k4xCc2	osm
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
k.	k.	k?	k.
ú.	ú.	k?	ú.
Nový	Nový	k1gMnSc5	Nový
Jičín-město	Jičíněsta	k1gMnSc5	Jičín-města
<g/>
,	,	kIx,	,
Nový	nový	k2eAgMnSc1d1	nový
Jičín-Dolní	Jičín-Dolní	k2eAgNnSc4d1	Jičín-Dolní
Předměstí	předměstí	k1gNnSc4	předměstí
a	a	k8xC	a
Nový	nový	k2eAgMnSc1d1	nový
Jičín-Horní	Jičín-Horní	k2eAgNnSc4d1	Jičín-Horní
Předměstí	předměstí	k1gNnSc4	předměstí
Bludovice	Bludovice	k1gFnPc1	Bludovice
–	–	k?	–
integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
vesnice	vesnice	k1gFnSc1	vesnice
rozkládající	rozkládající	k2eAgFnSc1d1	rozkládající
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
cesty	cesta	k1gFnSc2	cesta
na	na	k7c4	na
Valašské	valašský	k2eAgNnSc4d1	Valašské
Meziříčí	Meziříčí	k1gNnSc4	Meziříčí
<g/>
,	,	kIx,	,
k.	k.	k?	k.
ú.	ú.	k?	ú.
Bludovice	Bludovice	k1gFnPc4	Bludovice
<g />
.	.	kIx.	.
</s>
<s>
u	u	k7c2	u
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
Kojetín	Kojetín	k1gInSc1	Kojetín
–	–	k?	–
bývalá	bývalý	k2eAgFnSc1d1	bývalá
vesnice	vesnice	k1gFnSc1	vesnice
malebně	malebně	k6eAd1	malebně
umístěná	umístěný	k2eAgFnSc1d1	umístěná
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
pod	pod	k7c7	pod
kopcem	kopec	k1gInSc7	kopec
Svinec	svinec	k1gInSc1	svinec
<g/>
,	,	kIx,	,
k.	k.	k?	k.
ú.	ú.	k?	ú.
Kojetín	Kojetín	k1gInSc1	Kojetín
u	u	k7c2	u
Starého	Starého	k2eAgInSc2d1	Starého
Jičína	Jičín	k1gInSc2	Jičín
Loučka	loučka	k1gFnSc1	loučka
–	–	k?	–
dříve	dříve	k6eAd2	dříve
vesnická	vesnický	k2eAgFnSc1d1	vesnická
část	část	k1gFnSc1	část
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
na	na	k7c4	na
Starý	starý	k2eAgInSc4d1	starý
Jičín	Jičín	k1gInSc4	Jičín
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
také	také	k9	také
veliké	veliký	k2eAgNnSc4d1	veliké
sídliště	sídliště	k1gNnSc4	sídliště
<g/>
,	,	kIx,	,
k.	k.	k?	k.
ú.	ú.	k?	ú.
Loučka	loučka	k1gFnSc1	loučka
u	u	k7c2	u
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
Straník	straník	k1gMnSc1	straník
–	–	k?	–
vesnická	vesnický	k2eAgFnSc1d1	vesnická
část	část	k1gFnSc1	část
<g />
.	.	kIx.	.
</s>
<s>
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Hodslavice	Hodslavice	k1gInPc1	Hodslavice
(	(	kIx(	(
<g/>
směr	směr	k1gInSc4	směr
Valašské	valašský	k2eAgNnSc4d1	Valašské
Meziříčí	Meziříčí	k1gNnSc4	Meziříčí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k.	k.	k?	k.
ú.	ú.	k?	ú.
Straník	straník	k1gMnSc1	straník
Žilina	Žilina	k1gFnSc1	Žilina
–	–	k?	–
vesnická	vesnický	k2eAgFnSc1d1	vesnická
část	část	k1gFnSc1	část
podél	podél	k7c2	podél
silnice	silnice	k1gFnSc2	silnice
na	na	k7c4	na
Frenštát	Frenštát	k1gInSc4	Frenštát
pod	pod	k7c7	pod
Radhoštěm	Radhošť	k1gInSc7	Radhošť
<g/>
,	,	kIx,	,
k.	k.	k?	k.
ú.	ú.	k?	ú.
Žilina	Žilina	k1gFnSc1	Žilina
u	u	k7c2	u
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
Částí	část	k1gFnPc2	část
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějin	dějiny	k1gFnPc2	dějiny
i	i	k8xC	i
další	další	k2eAgFnPc1d1	další
obce	obec	k1gFnPc1	obec
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
postupně	postupně	k6eAd1	postupně
osamostatnily	osamostatnit	k5eAaPmAgFnP	osamostatnit
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Bernartice	Bernartice	k1gFnSc1	Bernartice
nad	nad	k7c7	nad
Odrou	Odra	k1gFnSc7	Odra
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Hostašovice	Hostašovice	k1gFnSc2	Hostašovice
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Kunín	Kunína	k1gFnPc2	Kunína
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Libhošť	Libhošť	k1gFnSc2	Libhošť
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Rybí	rybí	k2eAgFnSc2d1	rybí
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Šenov	Šenov	k1gInSc1	Šenov
u	u	k7c2	u
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Životice	Životice	k1gFnSc2	Životice
u	u	k7c2	u
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Okres	okres	k1gInSc4	okres
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
a	a	k8xC	a
Obvod	obvod	k1gInSc1	obvod
obce	obec	k1gFnSc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
53	[number]	k4	53
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
ORP	ORP	kA	ORP
z	z	k7c2	z
15	[number]	k4	15
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc4	město
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
udržuje	udržovat	k5eAaImIp3nS	udržovat
kontakty	kontakt	k1gInPc4	kontakt
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
některými	některý	k3yIgNnPc7	některý
zahraničními	zahraniční	k2eAgNnPc7d1	zahraniční
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
se	se	k3xPyFc4	se
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
či	či	k8xC	či
turismu	turismus	k1gInSc2	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
partnerských	partnerský	k2eAgNnPc2d1	partnerské
měst	město	k1gNnPc2	město
například	například	k6eAd1	například
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
na	na	k7c6	na
městských	městský	k2eAgFnPc6d1	městská
slavnostech	slavnost	k1gFnPc6	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Novellara	Novellara	k1gFnSc1	Novellara
–	–	k?	–
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
partnerství	partnerství	k1gNnSc2	partnerství
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
Görlitz	Görlitz	k1gMnSc1	Görlitz
–	–	k?	–
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
partnerství	partnerství	k1gNnSc4	partnerství
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
Ludwigsburg	Ludwigsburg	k1gMnSc1	Ludwigsburg
–	–	k?	–
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
partnerství	partnerství	k1gNnSc4	partnerství
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
Świętochłowice	Świętochłowice	k1gFnSc2	Świętochłowice
–	–	k?	–
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
partnerství	partnerství	k1gNnSc1	partnerství
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
Kremnica	Kremnica	k1gFnSc1	Kremnica
–	–	k?	–
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
partnerství	partnerství	k1gNnSc1	partnerství
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
Épinal	Épinal	k1gFnSc2	Épinal
–	–	k?	–
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
partnerství	partnerství	k1gNnSc2	partnerství
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
</s>
