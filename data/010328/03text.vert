<p>
<s>
Tučňáci	tučňák	k1gMnPc1	tučňák
(	(	kIx(	(
<g/>
Sphenisciformes	Sphenisciformes	k1gInSc1	Sphenisciformes
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nelétaví	létavý	k2eNgMnPc1d1	nelétavý
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
dokonale	dokonale	k6eAd1	dokonale
přizpůsobeni	přizpůsoben	k2eAgMnPc1d1	přizpůsoben
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
potápění	potápění	k1gNnSc2	potápění
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
chladných	chladný	k2eAgInPc2d1	chladný
mořských	mořský	k2eAgInPc2d1	mořský
proudů	proud	k1gInPc2	proud
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
druh	druh	k1gMnSc1	druh
<g/>
,	,	kIx,	,
hnízdící	hnízdící	k2eAgMnSc1d1	hnízdící
na	na	k7c6	na
Galapágách	Galapágy	k1gFnPc6	Galapágy
<g/>
,	,	kIx,	,
okrajově	okrajově	k6eAd1	okrajově
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
klasifikováno	klasifikovat	k5eAaImNgNnS	klasifikovat
asi	asi	k9	asi
17	[number]	k4	17
druhů	druh	k1gInPc2	druh
řazených	řazený	k2eAgInPc2d1	řazený
do	do	k7c2	do
šesti	šest	k4xCc2	šest
rodů	rod	k1gInPc2	rod
jediné	jediný	k2eAgFnSc2d1	jediná
čeledi	čeleď	k1gFnSc2	čeleď
tučňákovití	tučňákovitý	k2eAgMnPc1d1	tučňákovitý
(	(	kIx(	(
<g/>
Spheniscidae	Spheniscidae	k1gNnSc7	Spheniscidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
tučňák	tučňák	k1gMnSc1	tučňák
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
tučný	tučný	k2eAgInSc4d1	tučný
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavější	zajímavý	k2eAgFnSc1d2	zajímavější
je	být	k5eAaImIp3nS	být
však	však	k9	však
etymologie	etymologie	k1gFnSc1	etymologie
mezinárodně	mezinárodně	k6eAd1	mezinárodně
užívaného	užívaný	k2eAgInSc2d1	užívaný
názvu	název	k1gInSc2	název
Penguin	Penguina	k1gFnPc2	Penguina
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
či	či	k8xC	či
Pingvin	Pingvina	k1gFnPc2	Pingvina
(	(	kIx(	(
<g/>
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
,	,	kIx,	,
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
španělštině	španělština	k1gFnSc6	španělština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
původ	původ	k1gInSc1	původ
se	se	k3xPyFc4	se
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
velšském	velšský	k2eAgNnSc6d1	Velšské
pen	pen	k?	pen
gwyn	gwyn	k1gNnSc1	gwyn
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
hlava	hlava	k1gFnSc1	hlava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
Velšané	Velšan	k1gMnPc1	Velšan
označovali	označovat	k5eAaImAgMnP	označovat
alku	alka	k1gFnSc4	alka
velkou	velký	k2eAgFnSc4d1	velká
<g/>
,	,	kIx,	,
severského	severský	k2eAgMnSc2d1	severský
mořského	mořský	k2eAgMnSc2d1	mořský
ptáka	pták	k1gMnSc2	pták
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
připomínal	připomínat	k5eAaImAgMnS	připomínat
tučňáky	tučňák	k1gMnPc4	tučňák
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
vyhuben	vyhubit	k5eAaPmNgMnS	vyhubit
<g/>
.	.	kIx.	.
</s>
<s>
Alka	alka	k1gFnSc1	alka
totiž	totiž	k9	totiž
měla	mít	k5eAaImAgFnS	mít
nad	nad	k7c7	nad
okem	oke	k1gNnSc7	oke
výraznou	výrazný	k2eAgFnSc4d1	výrazná
bílou	bílý	k2eAgFnSc4d1	bílá
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Popsaný	popsaný	k2eAgInSc1d1	popsaný
vznik	vznik	k1gInSc1	vznik
slova	slovo	k1gNnSc2	slovo
však	však	k9	však
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
velšské	velšský	k2eAgFnSc3d1	velšská
slovotvorbě	slovotvorba	k1gFnSc3	slovotvorba
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pinguin	pinguina	k1gFnPc2	pinguina
je	být	k5eAaImIp3nS	být
však	však	k9	však
poprvé	poprvé	k6eAd1	poprvé
doloženo	doložit	k5eAaPmNgNnS	doložit
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
italského	italský	k2eAgMnSc2d1	italský
učence	učenec	k1gMnSc2	učenec
Antonia	Antonio	k1gMnSc2	Antonio
Pigafetty	Pigafetta	k1gMnSc2	Pigafetta
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
Magalhã	Magalhã	k1gMnSc7	Magalhã
námořní	námořní	k2eAgFnSc2d1	námořní
výpravy	výprava	k1gFnSc2	výprava
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
se	se	k3xPyFc4	se
i	i	k9	i
proto	proto	k8xC	proto
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
latinským	latinský	k2eAgNnSc7d1	latinské
slovem	slovo	k1gNnSc7	slovo
pinguis	pinguis	k1gFnSc2	pinguis
–	–	k?	–
"	"	kIx"	"
<g/>
tučný	tučný	k2eAgInSc4d1	tučný
<g/>
,	,	kIx,	,
šťavnatý	šťavnatý	k2eAgInSc4d1	šťavnatý
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
či	či	k8xC	či
obrazně	obrazně	k6eAd1	obrazně
"	"	kIx"	"
<g/>
těžký	těžký	k2eAgInSc1d1	těžký
<g/>
,	,	kIx,	,
těžkopádný	těžkopádný	k2eAgInSc1d1	těžkopádný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Objevení	objevení	k1gNnSc1	objevení
tučňáků	tučňák	k1gMnPc2	tučňák
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňáci	tučňák	k1gMnPc1	tučňák
byli	být	k5eAaImAgMnP	být
objeveni	objevit	k5eAaPmNgMnP	objevit
námořníky	námořník	k1gMnPc4	námořník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
hledali	hledat	k5eAaImAgMnP	hledat
námořní	námořní	k2eAgFnPc4d1	námořní
cesty	cesta	k1gFnPc4	cesta
kolem	kolem	k7c2	kolem
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lodních	lodní	k2eAgInPc6d1	lodní
denících	deník	k1gInPc6	deník
slavných	slavný	k2eAgMnPc2d1	slavný
mořeplavců	mořeplavec	k1gMnPc2	mořeplavec
jako	jako	k8xS	jako
byli	být	k5eAaImAgMnP	být
Vasco	Vasco	k1gNnSc4	Vasco
da	da	k?	da
Gama	gama	k1gNnSc2	gama
a	a	k8xC	a
Fernã	Fernã	k1gFnPc2	Fernã
de	de	k?	de
Magalhã	Magalhã	k1gMnPc2	Magalhã
či	či	k8xC	či
pirátů	pirát	k1gMnPc2	pirát
Francise	Francise	k1gFnSc2	Francise
Drakea	Drakeus	k1gMnSc2	Drakeus
a	a	k8xC	a
Williama	William	k1gMnSc2	William
Dampiera	Dampiero	k1gNnSc2	Dampiero
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
tučňácích	tučňák	k1gMnPc6	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
mořeplavci	mořeplavec	k1gMnPc1	mořeplavec
je	on	k3xPp3gFnPc4	on
pokládali	pokládat	k5eAaImAgMnP	pokládat
za	za	k7c2	za
opeřené	opeřený	k2eAgFnSc2d1	opeřená
ryby	ryba	k1gFnSc2	ryba
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Francise	Francise	k1gFnSc1	Francise
Drake	Drak	k1gFnSc2	Drak
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
jako	jako	k9	jako
o	o	k7c6	o
nelétavých	létavý	k2eNgFnPc6d1	nelétavá
černobílých	černobílý	k2eAgFnPc6d1	černobílá
husách	husa	k1gFnPc6	husa
<g/>
.	.	kIx.	.
</s>
<s>
Mořeplavci	mořeplavec	k1gMnPc1	mořeplavec
tučňáky	tučňák	k1gMnPc4	tučňák
chytali	chytat	k5eAaImAgMnP	chytat
a	a	k8xC	a
jedli	jíst	k5eAaImAgMnP	jíst
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jim	on	k3xPp3gMnPc3	on
jejich	jejich	k3xOp3gMnPc4	jejich
tučné	tučný	k2eAgNnSc1d1	tučné
<g/>
,	,	kIx,	,
rybinou	rybina	k1gFnSc7	rybina
páchnoucí	páchnoucí	k2eAgNnSc1d1	páchnoucí
maso	maso	k1gNnSc1	maso
moc	moc	k6eAd1	moc
nechutnalo	chutnat	k5eNaImAgNnS	chutnat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
anglický	anglický	k2eAgMnSc1d1	anglický
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
James	James	k1gMnSc1	James
Cook	Cook	k1gMnSc1	Cook
však	však	k9	však
jedl	jíst	k5eAaImAgMnS	jíst
tučňáky	tučňák	k1gMnPc4	tučňák
raději	rád	k6eAd2	rád
než	než	k8xS	než
nasolené	nasolený	k2eAgNnSc1d1	nasolené
maso	maso	k1gNnSc1	maso
z	z	k7c2	z
lodních	lodní	k2eAgFnPc2d1	lodní
zásob	zásoba	k1gFnPc2	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Podivný	podivný	k2eAgInSc4d1	podivný
vzhled	vzhled	k1gInSc4	vzhled
tučňáků	tučňák	k1gMnPc2	tučňák
fascinoval	fascinovat	k5eAaBmAgMnS	fascinovat
dobové	dobový	k2eAgMnPc4d1	dobový
učence	učenec	k1gMnPc4	učenec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dohadovali	dohadovat	k5eAaImAgMnP	dohadovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tučňáci	tučňák	k1gMnPc1	tučňák
jsou	být	k5eAaImIp3nP	být
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc1	ryba
nebo	nebo	k8xC	nebo
čtvernožci	čtvernožec	k1gMnPc1	čtvernožec
<g/>
,	,	kIx,	,
až	až	k9	až
roku	rok	k1gInSc2	rok
1758	[number]	k4	1758
švédský	švédský	k2eAgMnSc1d1	švédský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Carl	Carl	k1gMnSc1	Carl
Linné	Linný	k2eAgNnSc4d1	Linný
poprvé	poprvé	k6eAd1	poprvé
systematizoval	systematizovat	k5eAaBmAgMnS	systematizovat
tučňáka	tučňák	k1gMnSc4	tučňák
brýlového	brýlový	k2eAgMnSc4d1	brýlový
jako	jako	k8xS	jako
ptáka	pták	k1gMnSc4	pták
příbuzného	příbuzný	k1gMnSc4	příbuzný
albatrosům	albatros	k1gMnPc3	albatros
<g/>
.	.	kIx.	.
</s>
<s>
Živé	živý	k2eAgMnPc4d1	živý
tučňáky	tučňák	k1gMnPc4	tučňák
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dovézt	dovézt	k5eAaPmF	dovézt
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
až	až	k9	až
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
chov	chov	k1gInSc4	chov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
obtíží	obtíž	k1gFnPc2	obtíž
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
výsadou	výsada	k1gFnSc7	výsada
těch	ten	k3xDgFnPc2	ten
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
středně	středně	k6eAd1	středně
velcí	velký	k2eAgMnPc1d1	velký
až	až	k9	až
velcí	velký	k2eAgMnPc1d1	velký
nelétaví	létavý	k2eNgMnPc1d1	nelétavý
ptáci	pták	k1gMnPc1	pták
dobře	dobře	k6eAd1	dobře
přizpůsobení	přizpůsobení	k1gNnSc4	přizpůsobení
lovu	lov	k1gInSc2	lov
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgInSc1d3	nejmenší
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
tučňák	tučňák	k1gMnSc1	tučňák
nejmenší	malý	k2eAgMnSc1d3	nejmenší
(	(	kIx(	(
<g/>
Eudyptula	Eudyptula	k1gFnSc1	Eudyptula
minor	minor	k2eAgFnSc1d1	minor
<g/>
)	)	kIx)	)
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
do	do	k7c2	do
40	[number]	k4	40
cm	cm	kA	cm
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
1,5	[number]	k4	1,5
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
kg	kg	kA	kg
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc3	strana
tučňák	tučňák	k1gMnSc1	tučňák
císařský	císařský	k2eAgMnSc1d1	císařský
(	(	kIx(	(
<g/>
Aptenodytes	Aptenodytes	k1gInSc1	Aptenodytes
forsteri	forster	k1gFnSc2	forster
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
115	[number]	k4	115
cm	cm	kA	cm
vysoký	vysoký	k2eAgInSc4d1	vysoký
při	při	k7c6	při
hmotnosti	hmotnost	k1gFnSc6	hmotnost
až	až	k9	až
46	[number]	k4	46
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
fosilní	fosilní	k2eAgInPc1d1	fosilní
druhy	druh	k1gInPc1	druh
byly	být	k5eAaImAgInP	být
ještě	ještě	k9	ještě
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
vysocí	vysoký	k2eAgMnPc1d1	vysoký
jako	jako	k8xS	jako
průměrný	průměrný	k2eAgMnSc1d1	průměrný
dospělý	dospělý	k1gMnSc1	dospělý
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Morfologie	morfologie	k1gFnSc1	morfologie
a	a	k8xC	a
anatomie	anatomie	k1gFnSc1	anatomie
===	===	k?	===
</s>
</p>
<p>
<s>
Tučňáci	tučňák	k1gMnPc1	tučňák
jsou	být	k5eAaImIp3nP	být
dokonale	dokonale	k6eAd1	dokonale
přizpůsobeni	přizpůsoben	k2eAgMnPc1d1	přizpůsoben
potápění	potápění	k1gNnSc3	potápění
a	a	k8xC	a
lovu	lov	k1gInSc3	lov
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
vodě	voda	k1gFnSc6	voda
<g/>
;	;	kIx,	;
kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgNnSc3	tento
přizpůsobení	přizpůsobení	k1gNnSc1	přizpůsobení
druhotně	druhotně	k6eAd1	druhotně
ztratili	ztratit	k5eAaPmAgMnP	ztratit
schopnost	schopnost	k1gFnSc4	schopnost
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
tělo	tělo	k1gNnSc1	tělo
má	mít	k5eAaImIp3nS	mít
torpédovitý	torpédovitý	k2eAgInSc4d1	torpédovitý
(	(	kIx(	(
<g/>
hydrodynamický	hydrodynamický	k2eAgInSc4d1	hydrodynamický
<g/>
)	)	kIx)	)
tvar	tvar	k1gInSc4	tvar
s	s	k7c7	s
těžištěm	těžiště	k1gNnSc7	těžiště
umístěným	umístěný	k2eAgNnSc7d1	umístěné
dále	daleko	k6eAd2	daleko
než	než	k8xS	než
u	u	k7c2	u
létajících	létající	k2eAgMnPc2d1	létající
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Kosti	kost	k1gFnPc1	kost
tučňáků	tučňák	k1gMnPc2	tučňák
nejsou	být	k5eNaImIp3nP	být
pneumatizované	pneumatizovaný	k2eAgFnPc1d1	pneumatizovaná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
jejich	jejich	k3xOp3gNnSc2	jejich
těla	tělo	k1gNnSc2	tělo
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
hustotě	hustota	k1gFnSc3	hustota
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
potápění	potápění	k1gNnSc4	potápění
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
posunuty	posunout	k5eAaPmNgFnP	posunout
dozadu	dozadu	k6eAd1	dozadu
a	a	k8xC	a
mezi	mezi	k7c7	mezi
třemi	tři	k4xCgInPc7	tři
prsty	prst	k1gInPc7	prst
mají	mít	k5eAaImIp3nP	mít
plovací	plovací	k2eAgFnSc4d1	plovací
blánu	blána	k1gFnSc4	blána
<g/>
,	,	kIx,	,
při	při	k7c6	při
plavání	plavání	k1gNnSc6	plavání
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
kormidlo	kormidlo	k1gNnSc4	kormidlo
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
umístění	umístění	k1gNnSc3	umístění
nohou	noha	k1gFnPc2	noha
se	se	k3xPyFc4	se
tučňáci	tučňák	k1gMnPc1	tučňák
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
vzpřímeně	vzpřímeně	k6eAd1	vzpřímeně
<g/>
.	.	kIx.	.
</s>
<s>
Páteř	páteř	k1gFnSc1	páteř
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
ohebná	ohebný	k2eAgFnSc1d1	ohebná
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgFnSc1d1	umožňující
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
prudké	prudký	k2eAgInPc4d1	prudký
obraty	obrat	k1gInPc4	obrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
orgánem	orgán	k1gInSc7	orgán
pohybu	pohyb	k1gInSc2	pohyb
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
jsou	být	k5eAaImIp3nP	být
křídla	křídlo	k1gNnPc1	křídlo
<g/>
,	,	kIx,	,
přeměněná	přeměněný	k2eAgNnPc1d1	přeměněné
ve	v	k7c4	v
veslovité	veslovitý	k2eAgInPc4d1	veslovitý
útvary	útvar	k1gInPc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Kostra	kostra	k1gFnSc1	kostra
křídel	křídlo	k1gNnPc2	křídlo
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
křídlům	křídlo	k1gNnPc3	křídlo
létajících	létající	k2eAgMnPc2d1	létající
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
kosti	kost	k1gFnPc1	kost
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
silně	silně	k6eAd1	silně
zploštělé	zploštělý	k2eAgFnPc4d1	zploštělá
a	a	k8xC	a
zpevněné	zpevněný	k2eAgFnPc4d1	zpevněná
v	v	k7c6	v
loketním	loketní	k2eAgInSc6d1	loketní
a	a	k8xC	a
zápěstním	zápěstní	k2eAgInSc6d1	zápěstní
kloubu	kloub	k1gInSc6	kloub
přídatnými	přídatný	k2eAgFnPc7d1	přídatná
kůstkami	kůstka	k1gFnPc7	kůstka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
brání	bránit	k5eAaImIp3nP	bránit
jeho	jeho	k3xOp3gNnSc4	jeho
ohýbání	ohýbání	k1gNnSc4	ohýbání
<g/>
.	.	kIx.	.
</s>
<s>
Silně	silně	k6eAd1	silně
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
jsou	být	k5eAaImIp3nP	být
prsní	prsní	k2eAgInPc1d1	prsní
svaly	sval	k1gInPc1	sval
a	a	k8xC	a
hřeben	hřeben	k1gInSc1	hřeben
na	na	k7c4	na
prsní	prsní	k2eAgFnPc4d1	prsní
kosti	kost	k1gFnPc4	kost
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
souši	souš	k1gFnSc6	souš
jsou	být	k5eAaImIp3nP	být
tučňáci	tučňák	k1gMnPc1	tučňák
vcelku	vcelku	k6eAd1	vcelku
neohrabaní	neohrabaný	k2eAgMnPc1d1	neohrabaný
<g/>
;	;	kIx,	;
jelikož	jelikož	k8xS	jelikož
mají	mít	k5eAaImIp3nP	mít
zakrnělá	zakrnělý	k2eAgFnSc1d1	zakrnělá
–	–	k?	–
neohebná	ohebný	k2eNgFnSc1d1	neohebná
–	–	k?	–
kolena	koleno	k1gNnSc2	koleno
<g/>
,	,	kIx,	,
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
pomalou	pomalý	k2eAgFnSc7d1	pomalá
kolébavou	kolébavý	k2eAgFnSc7d1	kolébavá
chůzí	chůze	k1gFnSc7	chůze
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
tučňáků	tučňák	k1gMnPc2	tučňák
však	však	k9	však
zdolávají	zdolávat	k5eAaImIp3nP	zdolávat
i	i	k9	i
strmé	strmý	k2eAgInPc1d1	strmý
skalní	skalní	k2eAgInPc1d1	skalní
útesy	útes	k1gInPc1	útes
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
tučňák	tučňák	k1gMnSc1	tučňák
skalní	skalní	k2eAgMnSc1d1	skalní
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
prokazuje	prokazovat	k5eAaImIp3nS	prokazovat
hbitými	hbitý	k2eAgInPc7d1	hbitý
skoky	skok	k1gInPc7	skok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zobák	zobák	k1gInSc1	zobák
tučňáků	tučňák	k1gMnPc2	tučňák
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
patro	patro	k1gNnSc1	patro
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
ostré	ostrý	k2eAgFnSc2d1	ostrá
rohovité	rohovitý	k2eAgFnSc2d1	rohovitá
bradavky	bradavka	k1gFnSc2	bradavka
<g/>
,	,	kIx,	,
bránící	bránící	k2eAgNnSc1d1	bránící
vysmeknutí	vysmeknutí	k1gNnSc1	vysmeknutí
ulovené	ulovený	k2eAgFnSc2d1	ulovená
ryby	ryba	k1gFnSc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Jícen	jícen	k1gInSc1	jícen
a	a	k8xC	a
žaludek	žaludek	k1gInSc1	žaludek
jsou	být	k5eAaImIp3nP	být
prostorné	prostorný	k2eAgFnPc1d1	prostorná
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgFnPc1d1	umožňující
polknout	polknout	k5eAaPmF	polknout
i	i	k9	i
celé	celý	k2eAgFnPc4d1	celá
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oči	oko	k1gNnPc1	oko
tučňáků	tučňák	k1gMnPc2	tučňák
jsou	být	k5eAaImIp3nP	být
přizpůsobené	přizpůsobený	k2eAgNnSc4d1	přizpůsobené
vidění	vidění	k1gNnSc4	vidění
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
–	–	k?	–
čočka	čočka	k1gFnSc1	čočka
dovede	dovést	k5eAaPmIp3nS	dovést
výrazně	výrazně	k6eAd1	výrazně
měnit	měnit	k5eAaImF	měnit
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
sítnice	sítnice	k1gFnSc1	sítnice
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
citlivá	citlivý	k2eAgFnSc1d1	citlivá
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
(	(	kIx(	(
<g/>
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
se	se	k3xPyFc4	se
zornička	zornička	k1gFnSc1	zornička
stahuje	stahovat	k5eAaImIp3nS	stahovat
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
špendlíkové	špendlíkový	k2eAgFnSc2d1	špendlíková
hlavičky	hlavička	k1gFnSc2	hlavička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
druh	druh	k1gInSc1	druh
(	(	kIx(	(
<g/>
tučňák	tučňák	k1gMnSc1	tučňák
nejmenší	malý	k2eAgMnSc1d3	nejmenší
<g/>
,	,	kIx,	,
Eudyptula	Eudyptula	k1gFnSc1	Eudyptula
minor	minor	k2eAgFnSc1d1	minor
<g/>
)	)	kIx)	)
dokonce	dokonce	k9	dokonce
přešel	přejít	k5eAaPmAgInS	přejít
k	k	k7c3	k
nočnímu	noční	k2eAgInSc3d1	noční
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nP	aby
ve	v	k7c6	v
studených	studený	k2eAgFnPc6d1	studená
vodách	voda	k1gFnPc6	voda
i	i	k8xC	i
mrazech	mráz	k1gInPc6	mráz
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
udrželi	udržet	k5eAaPmAgMnP	udržet
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
se	se	k3xPyFc4	se
u	u	k7c2	u
tučňáků	tučňák	k1gMnPc2	tučňák
silná	silný	k2eAgFnSc1d1	silná
vrstva	vrstva	k1gFnSc1	vrstva
podkožního	podkožní	k2eAgInSc2d1	podkožní
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc7d1	tvořící
téměř	téměř	k6eAd1	téměř
třetinu	třetina	k1gFnSc4	třetina
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
husté	hustý	k2eAgNnSc1d1	husté
peří	peří	k1gNnSc1	peří
roste	růst	k5eAaImIp3nS	růst
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
pečlivě	pečlivě	k6eAd1	pečlivě
promašťované	promašťovaný	k2eAgNnSc1d1	promašťovaný
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
pera	pero	k1gNnPc1	pero
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
krátká	krátký	k2eAgFnSc1d1	krátká
a	a	k8xC	a
pevná	pevný	k2eAgFnSc1d1	pevná
a	a	k8xC	a
připomínají	připomínat	k5eAaImIp3nP	připomínat
spíše	spíše	k9	spíše
šupiny	šupina	k1gFnPc1	šupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučňáci	tučňák	k1gMnPc1	tučňák
mají	mít	k5eAaImIp3nP	mít
poměrně	poměrně	k6eAd1	poměrně
uniformní	uniformní	k2eAgNnSc4d1	uniformní
zbarvení	zbarvení	k1gNnSc4	zbarvení
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
např.	např.	kA	např.
alky	alka	k1gFnSc2	alka
<g/>
)	)	kIx)	)
–	–	k?	–
shora	shora	k6eAd1	shora
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
černí	černý	k2eAgMnPc1d1	černý
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
tučňák	tučňák	k1gMnSc1	tučňák
nejmenší	malý	k2eAgNnPc4d3	nejmenší
má	mít	k5eAaImIp3nS	mít
záda	záda	k1gNnPc4	záda
spíše	spíše	k9	spíše
namodralá	namodralý	k2eAgFnSc1d1	namodralá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zespodu	zespodu	k6eAd1	zespodu
bílí	bílý	k2eAgMnPc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
životem	život	k1gInSc7	život
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bílá	bílý	k2eAgFnSc1d1	bílá
spodina	spodina	k1gFnSc1	spodina
těla	tělo	k1gNnSc2	tělo
není	být	k5eNaImIp3nS	být
proti	proti	k7c3	proti
lesknoucí	lesknoucí	k2eAgFnSc3d1	lesknoucí
se	se	k3xPyFc4	se
hladině	hladina	k1gFnSc3	hladina
vidět	vidět	k5eAaImF	vidět
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nepozorované	pozorovaný	k2eNgNnSc4d1	nepozorované
přiblížení	přiblížení	k1gNnSc4	přiblížení
ke	k	k7c3	k
kořisti	kořist	k1gFnSc3	kořist
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tmavý	tmavý	k2eAgInSc4d1	tmavý
hřbet	hřbet	k1gInSc4	hřbet
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
rychlejšímu	rychlý	k2eAgNnSc3d2	rychlejší
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
<g/>
Dva	dva	k4xCgInPc1	dva
největší	veliký	k2eAgInPc1d3	veliký
druhy	druh	k1gInPc1	druh
tučňáků	tučňák	k1gMnPc2	tučňák
(	(	kIx(	(
<g/>
tučňák	tučňák	k1gMnSc1	tučňák
císařský	císařský	k2eAgMnSc1d1	císařský
a	a	k8xC	a
tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
<g/>
)	)	kIx)	)
však	však	k9	však
disponují	disponovat	k5eAaBmIp3nP	disponovat
pestřejším	pestrý	k2eAgNnPc3d2	pestřejší
zbarvením	zbarvení	k1gNnPc3	zbarvení
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
krku	krk	k1gInSc2	krk
a	a	k8xC	a
prsou	prsa	k1gNnPc2	prsa
–	–	k?	–
slabě	slabě	k6eAd1	slabě
žlutou	žlutý	k2eAgFnSc4d1	žlutá
až	až	k6eAd1	až
po	po	k7c4	po
výrazně	výrazně	k6eAd1	výrazně
oranžovou	oranžový	k2eAgFnSc4d1	oranžová
<g/>
.	.	kIx.	.
</s>
<s>
Duhový	duhový	k2eAgInSc1d1	duhový
rozvrh	rozvrh	k1gInSc1	rozvrh
barev	barva	k1gFnPc2	barva
slouží	sloužit	k5eAaImIp3nS	sloužit
samci	samec	k1gMnSc3	samec
k	k	k7c3	k
upoutání	upoutání	k1gNnSc3	upoutání
potenciální	potenciální	k2eAgFnSc2d1	potenciální
družky	družka	k1gFnSc2	družka
v	v	k7c6	v
období	období	k1gNnSc6	období
námluv	námluva	k1gFnPc2	námluva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
životě	život	k1gInSc6	život
tučňáků	tučňák	k1gMnPc2	tučňák
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
toto	tento	k3xDgNnSc4	tento
zbarvení	zbarvení	k1gNnSc4	zbarvení
zjevně	zjevně	k6eAd1	zjevně
hlubší	hluboký	k2eAgInSc1d2	hlubší
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jím	on	k3xPp3gInSc7	on
jsou	být	k5eAaImIp3nP	být
obdařené	obdařený	k2eAgFnPc1d1	obdařená
také	také	k9	také
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fyziologie	fyziologie	k1gFnSc2	fyziologie
===	===	k?	===
</s>
</p>
<p>
<s>
Tučňáci	tučňák	k1gMnPc1	tučňák
jsou	být	k5eAaImIp3nP	být
dokonalí	dokonalý	k2eAgMnPc1d1	dokonalý
plavci	plavec	k1gMnPc1	plavec
<g/>
;	;	kIx,	;
průměrně	průměrně	k6eAd1	průměrně
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
rychlosti	rychlost	k1gFnSc2	rychlost
10	[number]	k4	10
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
například	například	k6eAd1	například
ohroženi	ohrozit	k5eAaPmNgMnP	ohrozit
predátorem	predátor	k1gMnSc7	predátor
<g/>
,	,	kIx,	,
dokážou	dokázat	k5eAaPmIp3nP	dokázat
během	během	k7c2	během
okamžiku	okamžik	k1gInSc2	okamžik
zrychlit	zrychlit	k5eAaPmF	zrychlit
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
tak	tak	k9	tak
jasně	jasně	k6eAd1	jasně
navrch	navrch	k6eAd1	navrch
i	i	k8xC	i
před	před	k7c7	před
světovými	světový	k2eAgMnPc7d1	světový
rekordmany	rekordman	k1gMnPc7	rekordman
v	v	k7c6	v
plavání	plavání	k1gNnSc6	plavání
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
potápění	potápění	k1gNnSc6	potápění
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
potápějí	potápět	k5eAaImIp3nP	potápět
do	do	k7c2	do
hloubek	hloubka	k1gFnPc2	hloubka
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
rekordní	rekordní	k2eAgFnSc1d1	rekordní
hloubka	hloubka	k1gFnSc1	hloubka
ponoru	ponor	k1gInSc2	ponor
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenán	k2eAgFnSc1d1	zaznamenána
u	u	k7c2	u
tučňáka	tučňák	k1gMnSc2	tučňák
císařského	císařský	k2eAgInSc2d1	císařský
–	–	k?	–
534	[number]	k4	534
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
a	a	k8xC	a
vydrží	vydržet	k5eAaPmIp3nS	vydržet
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
kolem	kolem	k7c2	kolem
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
fyziologickému	fyziologický	k2eAgNnSc3d1	fyziologické
přizpůsobení	přizpůsobení	k1gNnSc3	přizpůsobení
však	však	k9	však
dokáží	dokázat	k5eAaPmIp3nP	dokázat
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
vydržet	vydržet	k5eAaPmF	vydržet
až	až	k9	až
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgFnPc1d1	Červené
krvinky	krvinka	k1gFnPc1	krvinka
tučňáků	tučňák	k1gMnPc2	tučňák
dovedou	dovést	k5eAaPmIp3nP	dovést
vázat	vázat	k5eAaImF	vázat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
krevní	krevní	k2eAgNnSc1d1	krevní
barvivo	barvivo	k1gNnSc1	barvivo
(	(	kIx(	(
<g/>
myoglobin	myoglobin	k2eAgMnSc1d1	myoglobin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
i	i	k9	i
ve	v	k7c6	v
svalech	sval	k1gInPc6	sval
(	(	kIx(	(
<g/>
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vázat	vázat	k5eAaImF	vázat
až	až	k9	až
15	[number]	k4	15
%	%	kIx~	%
potřebného	potřebný	k2eAgInSc2d1	potřebný
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
tučňáci	tučňák	k1gMnPc1	tučňák
snižují	snižovat	k5eAaImIp3nP	snižovat
během	během	k7c2	během
ponoru	ponor	k1gInSc2	ponor
tepovou	tepový	k2eAgFnSc4d1	tepová
frekvenci	frekvence	k1gFnSc4	frekvence
z	z	k7c2	z
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
tepů	tep	k1gInPc2	tep
na	na	k7c4	na
pouhých	pouhý	k2eAgInPc2d1	pouhý
20	[number]	k4	20
tepů	tep	k1gInPc2	tep
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
kyslíku	kyslík	k1gInSc2	kyslík
je	být	k5eAaImIp3nS	být
při	pře	k1gFnSc4	pře
potápění	potápění	k1gNnSc2	potápění
hnána	hnán	k2eAgFnSc1d1	hnána
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
svaly	sval	k1gInPc1	sval
mohou	moct	k5eAaImIp3nP	moct
po	po	k7c6	po
vyčerpání	vyčerpání	k1gNnSc6	vyčerpání
kyslíku	kyslík	k1gInSc2	kyslík
štěpit	štěpit	k5eAaImF	štěpit
glykogen	glykogen	k1gInSc4	glykogen
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
mléčnou	mléčný	k2eAgFnSc4d1	mléčná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opeření	opeření	k1gNnSc1	opeření
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
kvalita	kvalita	k1gFnSc1	kvalita
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
tučňáky	tučňák	k1gMnPc4	tučňák
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
pelichají	pelichat	k5eAaImIp3nP	pelichat
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peří	k1gNnSc4	peří
neztrácejí	ztrácet	k5eNaImIp3nP	ztrácet
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
jednorázově	jednorázově	k6eAd1	jednorázově
v	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tráví	trávit	k5eAaImIp3nP	trávit
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
<g/>
.	.	kIx.	.
</s>
<s>
Stará	Stará	k1gFnSc1	Stará
pera	pero	k1gNnSc2	pero
se	se	k3xPyFc4	se
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
a	a	k8xC	a
vypadávají	vypadávat	k5eAaImIp3nP	vypadávat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nová	nový	k2eAgNnPc1d1	nové
vyčnívají	vyčnívat	k5eAaImIp3nP	vyčnívat
nad	nad	k7c4	nad
kůži	kůže	k1gFnSc4	kůže
asi	asi	k9	asi
o	o	k7c4	o
půl	půl	k1xP	půl
centimetru	centimetr	k1gInSc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
peří	peří	k1gNnSc1	peří
narůstá	narůstat	k5eAaImIp3nS	narůstat
během	během	k7c2	během
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
týdnů	týden	k1gInPc2	týden
(	(	kIx(	(
<g/>
u	u	k7c2	u
velkých	velký	k2eAgInPc2d1	velký
druhů	druh	k1gInPc2	druh
až	až	k9	až
6	[number]	k4	6
týdnů	týden	k1gInPc2	týden
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
po	po	k7c4	po
celou	celá	k1gFnSc4	celá
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
tučňáci	tučňák	k1gMnPc1	tučňák
hladoví	hladovět	k5eAaImIp3nP	hladovět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
nemohou	moct	k5eNaImIp3nP	moct
nalovit	nalovit	k5eAaBmF	nalovit
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
většinou	většinou	k6eAd1	většinou
nehnutě	nehnutě	k6eAd1	nehnutě
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
šetřili	šetřit	k5eAaImAgMnP	šetřit
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jiní	jiný	k2eAgMnPc1d1	jiný
mořští	mořský	k2eAgMnPc1d1	mořský
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
tučňáci	tučňák	k1gMnPc1	tučňák
odkázáni	odkázat	k5eAaPmNgMnP	odkázat
na	na	k7c4	na
pití	pití	k1gNnSc4	pití
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
;	;	kIx,	;
nadbytečnou	nadbytečný	k2eAgFnSc4d1	nadbytečná
sůl	sůl	k1gFnSc4	sůl
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
speciální	speciální	k2eAgFnPc4d1	speciální
nosní	nosní	k2eAgFnPc4d1	nosní
(	(	kIx(	(
<g/>
supraorbitální	supraorbitální	k2eAgFnPc4d1	supraorbitální
<g/>
)	)	kIx)	)
žlázy	žláza	k1gFnPc4	žláza
<g/>
,	,	kIx,	,
umístěné	umístěný	k2eAgFnPc4d1	umístěná
nad	nad	k7c7	nad
očnicí	očnice	k1gFnSc7	očnice
a	a	k8xC	a
ústící	ústící	k2eAgMnSc1d1	ústící
do	do	k7c2	do
nosních	nosní	k2eAgFnPc2d1	nosní
dutin	dutina	k1gFnPc2	dutina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
a	a	k8xC	a
chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňáci	tučňák	k1gMnPc1	tučňák
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
převážně	převážně	k6eAd1	převážně
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
hlavonožci	hlavonožec	k1gMnPc7	hlavonožec
a	a	k8xC	a
krilem	kril	k1gInSc7	kril
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
a	a	k8xC	a
líná	línat	k5eAaImIp3nS	línat
jim	on	k3xPp3gMnPc3	on
peří	peřit	k5eAaImIp3nP	peřit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
hladovět	hladovět	k5eAaImF	hladovět
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
tučňák	tučňák	k1gMnSc1	tučňák
císařský	císařský	k2eAgMnSc1d1	císařský
hladoví	hladovět	k5eAaImIp3nS	hladovět
až	až	k9	až
4	[number]	k4	4
měsíce	měsíc	k1gInPc4	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
denního	denní	k2eAgInSc2d1	denní
programu	program	k1gInSc2	program
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
věnují	věnovat	k5eAaPmIp3nP	věnovat
tučňáci	tučňák	k1gMnPc1	tučňák
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
peří	peří	k1gNnSc4	peří
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
jej	on	k3xPp3gMnSc4	on
čistí	čistit	k5eAaImIp3nS	čistit
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
nohama	noha	k1gFnPc7	noha
a	a	k8xC	a
pečlivě	pečlivě	k6eAd1	pečlivě
promazávají	promazávat	k5eAaImIp3nP	promazávat
výměškem	výměšek	k1gInSc7	výměšek
nadocasní	nadocasní	k2eAgFnSc2d1	nadocasní
žlázy	žláza	k1gFnSc2	žláza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
společenští	společenský	k2eAgMnPc1d1	společenský
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
schopni	schopen	k2eAgMnPc1d1	schopen
žít	žít	k5eAaImF	žít
v	v	k7c6	v
ohromných	ohromný	k2eAgFnPc6d1	ohromná
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
monogamičtí	monogamický	k2eAgMnPc1d1	monogamický
a	a	k8xC	a
k	k	k7c3	k
hnízdění	hnízdění	k1gNnSc3	hnízdění
se	se	k3xPyFc4	se
vracejí	vracet	k5eAaImIp3nP	vracet
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c4	na
stejné	stejný	k2eAgNnSc4d1	stejné
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Snášejí	snášet	k5eAaImIp3nP	snášet
zpravidla	zpravidla	k6eAd1	zpravidla
jedno	jeden	k4xCgNnSc1	jeden
až	až	k9	až
dvě	dva	k4xCgNnPc4	dva
vejce	vejce	k1gNnPc4	vejce
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
tři	tři	k4xCgFnPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdem	hnízdo	k1gNnSc7	hnízdo
je	být	k5eAaImIp3nS	být
maximálně	maximálně	k6eAd1	maximálně
vytlačený	vytlačený	k2eAgInSc1d1	vytlačený
důlek	důlek	k1gInSc1	důlek
vystlaný	vystlaný	k2eAgInSc1d1	vystlaný
trávou	tráva	k1gFnSc7	tráva
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kamínky	kamínek	k1gInPc1	kamínek
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
v	v	k7c6	v
norách	nora	k1gFnPc6	nora
<g/>
,	,	kIx,	,
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
skal	skála	k1gFnPc2	skála
či	či	k8xC	či
pod	pod	k7c7	pod
kořeny	kořen	k1gInPc7	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
největší	veliký	k2eAgInPc1d3	veliký
druhy	druh	k1gInPc1	druh
nestaví	stavit	k5eNaImIp3nP	stavit
vůbec	vůbec	k9	vůbec
žádné	žádný	k3yNgNnSc4	žádný
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tučňák	tučňák	k1gMnSc1	tučňák
císařský	císařský	k2eAgMnSc1d1	císařský
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
sněhu	sníh	k1gInSc6	sníh
a	a	k8xC	a
tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
na	na	k7c6	na
zcela	zcela	k6eAd1	zcela
rovinaté	rovinatý	k2eAgFnSc6d1	rovinatá
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Vejce	vejce	k1gNnPc1	vejce
tak	tak	k9	tak
pokládají	pokládat	k5eAaImIp3nP	pokládat
na	na	k7c4	na
svá	svůj	k3xOyFgNnPc4	svůj
chodidla	chodidlo	k1gNnPc4	chodidlo
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
nepřetržitě	přetržitě	k6eNd1	přetržitě
jej	on	k3xPp3gNnSc4	on
na	na	k7c6	na
nohách	noha	k1gFnPc6	noha
nosí	nosit	k5eAaImIp3nS	nosit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tímto	tento	k3xDgMnSc7	tento
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
chladnou	chladný	k2eAgFnSc7d1	chladná
půdou	půda	k1gFnSc7	půda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
oba	dva	k4xCgMnPc1	dva
partneři	partner	k1gMnPc1	partner
a	a	k8xC	a
oběma	dva	k4xCgMnPc7	dva
se	se	k3xPyFc4	se
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
silně	silně	k6eAd1	silně
prokrvený	prokrvený	k2eAgInSc1d1	prokrvený
holý	holý	k2eAgInSc1d1	holý
záhyb	záhyb	k1gInSc1	záhyb
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
inkubace	inkubace	k1gFnSc2	inkubace
sedí	sedit	k5eAaImIp3nS	sedit
zpravidla	zpravidla	k6eAd1	zpravidla
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
–	–	k?	–
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
až	až	k8xS	až
týdnů	týden	k1gInPc2	týden
loví	lovit	k5eAaImIp3nS	lovit
a	a	k8xC	a
vykrmuje	vykrmovat	k5eAaImIp3nS	vykrmovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
úlohy	úloha	k1gFnPc1	úloha
vymění	vyměnit	k5eAaPmIp3nP	vyměnit
a	a	k8xC	a
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
zamíří	zamířit	k5eAaPmIp3nS	zamířit
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
samice	samice	k1gFnSc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Patřičně	patřičně	k6eAd1	patřičně
odlišné	odlišný	k2eAgNnSc1d1	odlišné
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
tučňáka	tučňák	k1gMnSc2	tučňák
císařského	císařský	k2eAgMnSc2d1	císařský
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
matka	matka	k1gFnSc1	matka
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vyčerpaní	vyčerpaný	k2eAgMnPc1d1	vyčerpaný
předá	předat	k5eAaPmIp3nS	předat
vejce	vejce	k1gNnPc4	vejce
partnerovi	partner	k1gMnSc3	partner
ihned	ihned	k6eAd1	ihned
po	po	k7c4	po
snesení	snesení	k1gNnSc4	snesení
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
jej	on	k3xPp3gInSc4	on
inkubuje	inkubovat	k5eAaImIp3nS	inkubovat
sám	sám	k3xTgInSc1	sám
celé	celý	k2eAgInPc4d1	celý
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
do	do	k7c2	do
vylíhnutí	vylíhnutí	k1gNnSc2	vylíhnutí
mláděte	mládě	k1gNnSc2	mládě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
slepá	slepý	k2eAgFnSc1d1	slepá
a	a	k8xC	a
porostlá	porostlý	k2eAgFnSc1d1	porostlá
prachovým	prachový	k2eAgNnSc7d1	prachové
peřím	peří	k1gNnSc7	peří
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
ho	on	k3xPp3gInSc2	on
jeden	jeden	k4xCgMnSc1	jeden
rodič	rodič	k1gMnSc1	rodič
hlídá	hlídat	k5eAaImIp3nS	hlídat
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
loví	lovit	k5eAaImIp3nS	lovit
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
mláďata	mládě	k1gNnPc1	mládě
tvoří	tvořit	k5eAaImIp3nP	tvořit
"	"	kIx"	"
<g/>
školky	školka	k1gFnPc4	školka
<g/>
"	"	kIx"	"
a	a	k8xC	a
loví	lovit	k5eAaImIp3nP	lovit
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
školky	školka	k1gFnPc4	školka
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
buď	buď	k8xC	buď
mladí	mladý	k2eAgMnPc1d1	mladý
nehnízdící	hnízdící	k2eNgMnPc1d1	nehnízdící
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
o	o	k7c4	o
svoji	svůj	k3xOyFgFnSc4	svůj
snůšku	snůška	k1gFnSc4	snůška
přišli	přijít	k5eAaPmAgMnP	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
ve	v	k7c6	v
školce	školka	k1gFnSc6	školka
najít	najít	k5eAaPmF	najít
své	svůj	k3xOyFgNnSc4	svůj
mládě	mládě	k1gNnSc4	mládě
a	a	k8xC	a
krmí	krmě	k1gFnSc7	krmě
jen	jen	k9	jen
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
druhů	druh	k1gInPc2	druh
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
mláďata	mládě	k1gNnPc1	mládě
schovaná	schovaný	k2eAgNnPc1d1	schované
uvnitř	uvnitř	k7c2	uvnitř
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
školky	školka	k1gFnSc2	školka
nevytváří	vytvářet	k5eNaImIp3nS	vytvářet
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaImIp3nP	vydávat
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
až	až	k6eAd1	až
když	když	k8xS	když
mají	mít	k5eAaImIp3nP	mít
kompletní	kompletní	k2eAgNnSc4d1	kompletní
opeření	opeření	k1gNnSc4	opeření
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgInPc1d1	Malé
druhy	druh	k1gInPc1	druh
dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
velké	velký	k2eAgInPc4d1	velký
dokonce	dokonce	k9	dokonce
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
v	v	k7c6	v
9	[number]	k4	9
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgMnPc1	první
tučňáci	tučňák	k1gMnPc1	tučňák
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
70	[number]	k4	70
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
na	na	k7c6	na
kontinentu	kontinent	k1gInSc6	kontinent
Gondwana	Gondwan	k1gMnSc2	Gondwan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ležel	ležet	k5eAaImAgMnS	ležet
více	hodně	k6eAd2	hodně
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
v	v	k7c6	v
chladnějších	chladný	k2eAgFnPc6d2	chladnější
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
šířkách	šířka	k1gFnPc6	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
až	až	k9	až
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
teplých	teplý	k2eAgFnPc2d1	teplá
vod	voda	k1gFnPc2	voda
u	u	k7c2	u
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
globálního	globální	k2eAgNnSc2d1	globální
ochlazování	ochlazování	k1gNnSc2	ochlazování
(	(	kIx(	(
<g/>
v	v	k7c6	v
eocénu	eocén	k1gInSc6	eocén
<g/>
)	)	kIx)	)
tyto	tento	k3xDgInPc1	tento
často	často	k6eAd1	často
gigantické	gigantický	k2eAgInPc1d1	gigantický
druhy	druh	k1gInPc1	druh
tučňáků	tučňák	k1gMnPc2	tučňák
(	(	kIx(	(
<g/>
Icadyptes	Icadyptes	k1gMnSc1	Icadyptes
salasi	salas	k1gMnPc1	salas
<g/>
,	,	kIx,	,
Perudyptes	Perudyptes	k1gInSc1	Perudyptes
devriesi	devriese	k1gFnSc3	devriese
)	)	kIx)	)
vyhynuly	vyhynout	k5eAaPmAgFnP	vyhynout
<g/>
;	;	kIx,	;
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
možných	možný	k2eAgFnPc2d1	možná
příčin	příčina	k1gFnPc2	příčina
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc1	rozvoj
rybožravých	rybožravý	k2eAgMnPc2d1	rybožravý
kytovců	kytovec	k1gMnPc2	kytovec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
jejich	jejich	k3xOp3gMnPc7	jejich
úspěšnějšími	úspěšný	k2eAgMnPc7d2	úspěšnější
konkurenty	konkurent	k1gMnPc7	konkurent
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
z	z	k7c2	z
postupně	postupně	k6eAd1	postupně
ledem	led	k1gInSc7	led
zakrývané	zakrývaný	k2eAgFnSc2d1	zakrývaná
Antarktidy	Antarktida	k1gFnSc2	Antarktida
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
cirkumpolárních	cirkumpolární	k2eAgInPc2d1	cirkumpolární
oceánských	oceánský	k2eAgInPc2d1	oceánský
proudů	proud	k1gInPc2	proud
šířili	šířit	k5eAaImAgMnP	šířit
předci	předek	k1gMnPc1	předek
dnešních	dnešní	k2eAgInPc2d1	dnešní
druhů	druh	k1gInPc2	druh
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
osídlili	osídlit	k5eAaPmAgMnP	osídlit
ostrovy	ostrov	k1gInPc4	ostrov
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jižní	jižní	k2eAgNnSc4d1	jižní
pobřeží	pobřeží	k1gNnSc4	pobřeží
přilehlých	přilehlý	k2eAgInPc2d1	přilehlý
světadílů	světadíl	k1gInPc2	světadíl
<g/>
.	.	kIx.	.
</s>
<s>
Nejdále	daleko	k6eAd3	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
se	se	k3xPyFc4	se
moderní	moderní	k2eAgMnPc1d1	moderní
tučňáci	tučňák	k1gMnPc1	tučňák
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
chladných	chladný	k2eAgFnPc2d1	chladná
tropických	tropický	k2eAgFnPc2d1	tropická
vod	voda	k1gFnPc2	voda
Galapág	Galapágy	k1gFnPc2	Galapágy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
teprve	teprve	k6eAd1	teprve
před	před	k7c7	před
4	[number]	k4	4
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rovníkovou	rovníkový	k2eAgFnSc4d1	Rovníková
termální	termální	k2eAgFnSc4d1	termální
bariéru	bariéra	k1gFnSc4	bariéra
zřejmě	zřejmě	k6eAd1	zřejmě
kvůli	kvůli	k7c3	kvůli
adaptaci	adaptace	k1gFnSc3	adaptace
na	na	k7c4	na
život	život	k1gInSc4	život
v	v	k7c6	v
chladných	chladný	k2eAgFnPc6d1	chladná
vodách	voda	k1gFnPc6	voda
nikdy	nikdy	k6eAd1	nikdy
nepřekročili	překročit	k5eNaPmAgMnP	překročit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
paleogénu	paleogén	k1gInSc2	paleogén
i	i	k8xC	i
neogénu	neogén	k1gInSc2	neogén
se	se	k3xPyFc4	se
opakovaně	opakovaně	k6eAd1	opakovaně
vyskytovaly	vyskytovat	k5eAaImAgFnP	vyskytovat
obří	obří	k2eAgFnPc1d1	obří
formy	forma	k1gFnPc1	forma
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
však	však	k9	však
vymizely	vymizet	k5eAaPmAgFnP	vymizet
po	po	k7c6	po
evoluční	evoluční	k2eAgFnSc6d1	evoluční
radiaci	radiace	k1gFnSc6	radiace
dravých	dravý	k2eAgMnPc2d1	dravý
mořských	mořský	k2eAgMnPc2d1	mořský
savců	savec	k1gMnPc2	savec
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
oligocénu	oligocén	k1gInSc2	oligocén
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
miocénu	miocén	k1gInSc2	miocén
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výskyt	výskyt	k1gInSc1	výskyt
moderních	moderní	k2eAgInPc2d1	moderní
druhů	druh	k1gInPc2	druh
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
jsou	být	k5eAaImIp3nP	být
tučňáci	tučňák	k1gMnPc1	tučňák
obecně	obecně	k6eAd1	obecně
spojováni	spojován	k2eAgMnPc1d1	spojován
se	se	k3xPyFc4	se
zimou	zima	k1gFnSc7	zima
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
mírném	mírný	k2eAgNnSc6d1	mírné
až	až	k8xS	až
subtropickém	subtropický	k2eAgNnSc6d1	subtropické
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
;	;	kIx,	;
zatímco	zatímco	k8xS	zatímco
tučňák	tučňák	k1gMnSc1	tučňák
císařský	císařský	k2eAgMnSc1d1	císařský
(	(	kIx(	(
<g/>
Aptenodytes	Aptenodytes	k1gInSc1	Aptenodytes
forsteri	forster	k1gFnSc2	forster
<g/>
)	)	kIx)	)
čelí	čelit	k5eAaImIp3nS	čelit
třeskutým	třeskutý	k2eAgInPc3d1	třeskutý
antarktickým	antarktický	k2eAgInPc3d1	antarktický
mrazům	mráz	k1gInPc3	mráz
v	v	k7c6	v
mínus	mínus	k6eAd1	mínus
čtyřiceti	čtyřicet	k4xCc6	čtyřicet
stupních	stupeň	k1gInPc6	stupeň
pod	pod	k7c7	pod
nulou	nula	k1gFnSc7	nula
<g/>
,	,	kIx,	,
svádí	svádět	k5eAaImIp3nS	svádět
tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
(	(	kIx(	(
<g/>
Spheniscus	Spheniscus	k1gMnSc1	Spheniscus
demersus	demersus	k1gMnSc1	demersus
<g/>
)	)	kIx)	)
opačný	opačný	k2eAgInSc1d1	opačný
boj	boj	k1gInSc1	boj
s	s	k7c7	s
čtyřiceti	čtyřicet	k4xCc7	čtyřicet
stupňovým	stupňový	k2eAgNnSc7d1	stupňové
parnem	parno	k1gNnSc7	parno
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gMnSc1	druh
tučňák	tučňák	k1gMnSc1	tučňák
galapážský	galapážský	k2eAgMnSc1d1	galapážský
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
u	u	k7c2	u
samotného	samotný	k2eAgInSc2d1	samotný
rovníku	rovník	k1gInSc2	rovník
na	na	k7c6	na
Galapážských	galapážský	k2eAgInPc6d1	galapážský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
ocitá	ocitat	k5eAaImIp3nS	ocitat
i	i	k9	i
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
"	"	kIx"	"
<g/>
severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
linií	linie	k1gFnSc7	linie
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Včetně	včetně	k7c2	včetně
Antarktidy	Antarktida	k1gFnSc2	Antarktida
a	a	k8xC	a
přilehlých	přilehlý	k2eAgInPc2d1	přilehlý
ostrovů	ostrov	k1gInPc2	ostrov
chladné	chladný	k2eAgFnSc2d1	chladná
subantarktické	subantarktický	k2eAgFnSc2d1	subantarktický
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Aptenodytes	Aptenodytesa	k1gFnPc2	Aptenodytesa
<g/>
,	,	kIx,	,
Pygoscelis	Pygoscelis	k1gFnPc2	Pygoscelis
a	a	k8xC	a
Eudyptes	Eudyptesa	k1gFnPc2	Eudyptesa
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
tito	tento	k3xDgMnPc1	tento
ptáci	pták	k1gMnPc1	pták
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
již	již	k6eAd1	již
zmíněné	zmíněný	k2eAgFnSc6d1	zmíněná
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Spheniscus	Spheniscus	k1gInSc1	Spheniscus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandě	Zéland	k1gInSc6	Zéland
(	(	kIx(	(
<g/>
rody	rod	k1gInPc1	rod
Megadyptes	Megadyptesa	k1gFnPc2	Megadyptesa
a	a	k8xC	a
Eudyptula	Eudyptulum	k1gNnSc2	Eudyptulum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fylogeneze	fylogeneze	k1gFnSc1	fylogeneze
a	a	k8xC	a
systematika	systematika	k1gFnSc1	systematika
==	==	k?	==
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc4	řád
tučňáci	tučňák	k1gMnPc1	tučňák
(	(	kIx(	(
<g/>
Sphenisciformes	Sphenisciformes	k1gMnSc1	Sphenisciformes
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jedinou	jediný	k2eAgFnSc4d1	jediná
čeleď	čeleď	k1gFnSc4	čeleď
tučňákovití	tučňákovitý	k2eAgMnPc1d1	tučňákovitý
(	(	kIx(	(
<g/>
Spheniscidae	Spheniscidae	k1gNnSc7	Spheniscidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
společných	společný	k2eAgInPc2d1	společný
znaků	znak	k1gInPc2	znak
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
rohovité	rohovitý	k2eAgFnSc2d1	rohovitá
destičky	destička	k1gFnSc2	destička
na	na	k7c6	na
zobáku	zobák	k1gInSc6	zobák
a	a	k8xC	a
supraorbitální	supraorbitální	k2eAgFnPc1d1	supraorbitální
žlázy	žláza	k1gFnPc1	žláza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
jako	jako	k8xC	jako
nejbližší	blízký	k2eAgMnPc1d3	nejbližší
příbuzní	příbuzný	k1gMnPc1	příbuzný
označovány	označovat	k5eAaImNgFnP	označovat
potáplice	potáplice	k1gFnPc1	potáplice
a	a	k8xC	a
buřňáci	buřňák	k1gMnPc1	buřňák
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studií	studie	k1gFnPc2	studie
DNA	dno	k1gNnSc2	dno
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
blízce	blízce	k6eAd1	blízce
příbuzné	příbuzný	k2eAgFnPc1d1	příbuzná
označovány	označován	k2eAgFnPc1d1	označována
i	i	k8xC	i
fregatky	fregatka	k1gFnPc1	fregatka
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
čápi	čáp	k1gMnPc1	čáp
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
tučňáci	tučňák	k1gMnPc1	tučňák
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
čeleď	čeleď	k1gFnSc1	čeleď
Spheniscidae	Spheniscida	k1gFnSc2	Spheniscida
<g/>
)	)	kIx)	)
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
čeleděmi	čeleď	k1gFnPc7	čeleď
(	(	kIx(	(
<g/>
např.	např.	kA	např.
potáplice	potáplice	k1gFnSc2	potáplice
<g/>
,	,	kIx,	,
albatrosi	albatros	k1gMnPc1	albatros
<g/>
)	)	kIx)	)
řazeni	řazen	k2eAgMnPc1d1	řazen
do	do	k7c2	do
nadčeledi	nadčeleď	k1gFnSc2	nadčeleď
buřňáků	buřňák	k1gMnPc2	buřňák
(	(	kIx(	(
<g/>
Procellarioidea	Procellarioidea	k1gMnSc1	Procellarioidea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
rozšířeného	rozšířený	k2eAgInSc2d1	rozšířený
řádu	řád	k1gInSc2	řád
brodivých	brodivý	k2eAgFnPc2d1	brodivá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
fosilní	fosilní	k2eAgInPc1d1	fosilní
nálezy	nález	k1gInPc1	nález
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
paleocénu	paleocén	k1gInSc2	paleocén
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
rodu	rod	k1gInSc2	rod
Waimanu	Waiman	k1gInSc2	Waiman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
tučňáci	tučňák	k1gMnPc1	tučňák
byli	být	k5eAaImAgMnP	být
velcí	velký	k2eAgMnPc1d1	velký
(	(	kIx(	(
<g/>
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
štíhlým	štíhlý	k2eAgInSc7d1	štíhlý
zobákem	zobák	k1gInSc7	zobák
<g/>
,	,	kIx,	,
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
křídly	křídlo	k1gNnPc7	křídlo
a	a	k8xC	a
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
běháky	běhák	k1gInPc7	běhák
<g/>
;	;	kIx,	;
celkově	celkově	k6eAd1	celkově
poněkud	poněkud	k6eAd1	poněkud
připomínali	připomínat	k5eAaImAgMnP	připomínat
potáplice	potáplice	k1gFnSc2	potáplice
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
nejstarším	starý	k2eAgInSc7d3	nejstarší
druhem	druh	k1gInSc7	druh
je	být	k5eAaImIp3nS	být
Crossvallia	Crossvallia	k1gFnSc1	Crossvallia
unienwillia	unienwillia	k1gFnSc1	unienwillia
z	z	k7c2	z
paleocénu	paleocén	k1gInSc2	paleocén
Antarktického	antarktický	k2eAgInSc2d1	antarktický
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
klima	klima	k1gNnSc1	klima
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
mnohem	mnohem	k6eAd1	mnohem
teplejší	teplý	k2eAgInSc1d2	teplejší
a	a	k8xC	a
vlhčí	vlhký	k2eAgInSc1d2	vlhčí
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
130	[number]	k4	130
<g/>
–	–	k?	–
<g/>
140	[number]	k4	140
cm	cm	kA	cm
<g/>
.	.	kIx.	.
<g/>
Mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
tučňáků	tučňák	k1gMnPc2	tučňák
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
z	z	k7c2	z
eocénu	eocén	k1gInSc2	eocén
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
nejméně	málo	k6eAd3	málo
14	[number]	k4	14
druhů	druh	k1gInPc2	druh
patřících	patřící	k2eAgInPc2d1	patřící
do	do	k7c2	do
sedmi	sedm	k4xCc2	sedm
rodů	rod	k1gInPc2	rod
(	(	kIx(	(
<g/>
Anthropornis	Anthropornis	k1gInSc1	Anthropornis
<g/>
,	,	kIx,	,
Archaeospheniscus	Archaeospheniscus	k1gInSc1	Archaeospheniscus
<g/>
,	,	kIx,	,
Delphinornis	Delphinornis	k1gInSc1	Delphinornis
<g/>
,	,	kIx,	,
Ichthyopteryx	Ichthyopteryx	k1gInSc1	Ichthyopteryx
<g/>
,	,	kIx,	,
Marambiornis	Marambiornis	k1gInSc1	Marambiornis
<g/>
,	,	kIx,	,
Mesetaornis	Mesetaornis	k1gInSc1	Mesetaornis
<g/>
,	,	kIx,	,
Palaeeudyptes	Palaeeudyptes	k1gInSc1	Palaeeudyptes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
tučňáci	tučňák	k1gMnPc1	tučňák
tvořili	tvořit	k5eAaImAgMnP	tvořit
dvě	dva	k4xCgFnPc4	dva
skupiny	skupina	k1gFnPc4	skupina
–	–	k?	–
menší	malý	k2eAgInPc4d2	menší
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
velikostí	velikost	k1gFnSc7	velikost
odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
dnešním	dnešní	k2eAgInSc7d1	dnešní
a	a	k8xC	a
obří	obří	k2eAgInSc4d1	obří
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnPc4d1	dosahující
velikosti	velikost	k1gFnPc4	velikost
výrazně	výrazně	k6eAd1	výrazně
přes	přes	k7c4	přes
165	[number]	k4	165
cm	cm	kA	cm
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
80	[number]	k4	80
kg	kg	kA	kg
(	(	kIx(	(
<g/>
Anthropornis	Anthropornis	k1gFnPc2	Anthropornis
nordenskjoeldi	nordenskjoeld	k1gMnPc1	nordenskjoeld
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
eocénu	eocén	k1gInSc2	eocén
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
známy	znám	k2eAgInPc1d1	znám
první	první	k4xOgInPc1	první
nálezy	nález	k1gInPc1	nález
mimo	mimo	k7c4	mimo
Antarktidu	Antarktida	k1gFnSc4	Antarktida
(	(	kIx(	(
<g/>
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Rovněž	rovněž	k9	rovněž
z	z	k7c2	z
oligocénu	oligocén	k1gInSc2	oligocén
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
řada	řada	k1gFnSc1	řada
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
modernějších	moderní	k2eAgFnPc2d2	modernější
forem	forma	k1gFnPc2	forma
s	s	k7c7	s
pokročilejší	pokročilý	k2eAgFnSc7d2	pokročilejší
adaptací	adaptace	k1gFnSc7	adaptace
křídel	křídlo	k1gNnPc2	křídlo
(	(	kIx(	(
<g/>
Platydyptes	Platydyptes	k1gInSc1	Platydyptes
z	z	k7c2	z
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
miocénu	miocén	k1gInSc2	miocén
pochází	pocházet	k5eAaImIp3nP	pocházet
početné	početný	k2eAgInPc1d1	početný
nálezy	nález	k1gInPc1	nález
tučňáků	tučňák	k1gMnPc2	tučňák
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevují	objevovat	k5eAaImIp3nP	objevovat
první	první	k4xOgInPc1	první
nálezy	nález	k1gInPc1	nález
zástupců	zástupce	k1gMnPc2	zástupce
moderních	moderní	k2eAgInPc2d1	moderní
rodů	rod	k1gInPc2	rod
(	(	kIx(	(
<g/>
Spheniscus	Spheniscus	k1gMnSc1	Spheniscus
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
<g/>
,	,	kIx,	,
Pygoscelis	Pygoscelis	k1gFnSc1	Pygoscelis
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
nálezy	nález	k1gInPc1	nález
jihoafrických	jihoafrický	k2eAgMnPc2d1	jihoafrický
tučňáků	tučňák	k1gMnPc2	tučňák
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
pliocénu	pliocén	k1gInSc2	pliocén
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Spheniscus	Spheniscus	k1gInSc1	Spheniscus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Minimálně	minimálně	k6eAd1	minimálně
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc4	druh
tučňáků	tučňák	k1gMnPc2	tučňák
vymřely	vymřít	k5eAaPmAgFnP	vymřít
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
době	doba	k1gFnSc6	doba
–	–	k?	–
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
druh	druh	k1gInSc1	druh
Tasidyptes	Tasidyptesa	k1gFnPc2	Tasidyptesa
hunteri	hunteri	k6eAd1	hunteri
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Hunter	Hunter	k1gInSc4	Hunter
u	u	k7c2	u
Tasmánie	Tasmánie	k1gFnSc2	Tasmánie
<g/>
,	,	kIx,	,
asi	asi	k9	asi
před	před	k7c7	před
500	[number]	k4	500
lety	léto	k1gNnPc7	léto
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
tučňák	tučňák	k1gMnSc1	tučňák
Megadyptes	Megadyptes	k1gMnSc1	Megadyptes
waitaha	waitaha	k1gMnSc1	waitaha
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
blíže	blíž	k1gFnSc2	blíž
nezařazený	zařazený	k2eNgMnSc1d1	nezařazený
tučňák	tučňák	k1gMnSc1	tučňák
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Eudyptes	Eudyptesa	k1gFnPc2	Eudyptesa
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Chatham	Chatham	k1gInSc1	Chatham
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
druhů	druh	k1gInPc2	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
Spheniscidae	Spheniscida	k1gInSc2	Spheniscida
<g/>
,	,	kIx,	,
tučňákovití	tučňákovitý	k2eAgMnPc1d1	tučňákovitý
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
</s>
</p>
<p>
<s>
AptenodytesOba	AptenodytesOba	k1gFnSc1	AptenodytesOba
druhy	druh	k1gInPc1	druh
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgNnPc3d3	veliký
a	a	k8xC	a
nejvýrazněji	výrazně	k6eAd3	výrazně
zbarveným	zbarvený	k2eAgMnPc3d1	zbarvený
tučňákům	tučňák	k1gMnPc3	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
záda	záda	k1gNnPc1	záda
jsou	být	k5eAaImIp3nP	být
modravě	modravě	k6eAd1	modravě
šedá	šedá	k1gFnSc1	šedá
<g/>
,	,	kIx,	,
na	na	k7c6	na
krku	krk	k1gInSc6	krk
mají	mít	k5eAaImIp3nP	mít
zlatožluté	zlatožlutý	k2eAgFnPc1d1	zlatožlutá
skvrny	skvrna	k1gFnPc1	skvrna
<g/>
,	,	kIx,	,
prsa	prsa	k1gNnPc1	prsa
jsou	být	k5eAaImIp3nP	být
žlutavá	žlutavý	k2eAgNnPc1d1	žlutavé
nebo	nebo	k8xC	nebo
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
zobáku	zobák	k1gInSc2	zobák
mají	mít	k5eAaImIp3nP	mít
purpurové	purpurový	k2eAgFnPc1d1	purpurová
nebo	nebo	k8xC	nebo
fialové	fialový	k2eAgFnPc1d1	fialová
skvrny	skvrna	k1gFnPc1	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
císařský	císařský	k2eAgMnSc1d1	císařský
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
drsných	drsný	k2eAgFnPc6d1	drsná
podmínkách	podmínka	k1gFnPc6	podmínka
na	na	k7c4	na
špatně	špatně	k6eAd1	špatně
přístupných	přístupný	k2eAgNnPc6d1	přístupné
místech	místo	k1gNnPc6	místo
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
(	(	kIx(	(
<g/>
na	na	k7c6	na
stabilní	stabilní	k2eAgFnSc6d1	stabilní
ledové	ledový	k2eAgFnSc6d1	ledová
pláni	pláň	k1gFnSc6	pláň
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zdlouhavý	zdlouhavý	k2eAgInSc1d1	zdlouhavý
rozmnožovací	rozmnožovací	k2eAgInSc1d1	rozmnožovací
cyklus	cyklus	k1gInSc1	cyklus
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
jako	jako	k9	jako
tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
odchová	odchovat	k5eAaPmIp3nS	odchovat
maximálně	maximálně	k6eAd1	maximálně
dvě	dva	k4xCgNnPc4	dva
mláďata	mládě	k1gNnPc4	mládě
během	během	k7c2	během
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgMnPc3d1	jiný
druhům	druh	k1gMnPc3	druh
stráví	strávit	k5eAaPmIp3nS	strávit
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
nikterak	nikterak	k6eAd1	nikterak
monogamičtí	monogamický	k2eAgMnPc1d1	monogamický
<g/>
,	,	kIx,	,
povětšinou	povětšinou	k6eAd1	povětšinou
spolu	spolu	k6eAd1	spolu
jeden	jeden	k4xCgInSc1	jeden
stejný	stejný	k2eAgInSc1d1	stejný
pár	pár	k1gInSc1	pár
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
maximálně	maximálně	k6eAd1	maximálně
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
chovné	chovný	k2eAgFnPc4d1	chovná
sezóny	sezóna	k1gFnPc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
veřejnosti	veřejnost	k1gFnSc3	veřejnost
často	často	k6eAd1	často
předkládán	předkládat	k5eAaImNgInS	předkládat
opak	opak	k1gInSc1	opak
<g/>
,	,	kIx,	,
zjevně	zjevně	k6eAd1	zjevně
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
popularity	popularita	k1gFnSc2	popularita
a	a	k8xC	a
vzhledové	vzhledový	k2eAgFnSc2d1	vzhledová
oblíbenosti	oblíbenost	k1gFnSc2	oblíbenost
<g/>
.	.	kIx.	.
</s>
<s>
Snášejí	snášet	k5eAaImIp3nP	snášet
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc4	jeden
velké	velký	k2eAgNnSc4d1	velké
vejce	vejce	k1gNnSc4	vejce
a	a	k8xC	a
nestaví	stavit	k5eNaBmIp3nS	stavit
si	se	k3xPyFc3	se
hnízda	hnízdo	k1gNnPc1	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
vejce	vejce	k1gNnSc2	vejce
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
mládě	mládě	k1gNnSc4	mládě
do	do	k7c2	do
určitého	určitý	k2eAgNnSc2d1	určité
stáří	stáří	k1gNnSc2	stáří
vysedává	vysedávat	k5eAaImIp3nS	vysedávat
na	na	k7c6	na
nohách	noha	k1gFnPc6	noha
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
skryto	skrýt	k5eAaPmNgNnS	skrýt
pod	pod	k7c7	pod
prokrveným	prokrvený	k2eAgInSc7d1	prokrvený
záhybem	záhyb	k1gInSc7	záhyb
kůže	kůže	k1gFnSc2	kůže
nacházejícím	nacházející	k2eAgInPc3d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
břišní	břišní	k2eAgFnSc6d1	břišní
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
dospělých	dospělí	k1gMnPc2	dospělí
(	(	kIx(	(
<g/>
kožním	kožní	k2eAgInSc7d1	kožní
vakem	vak	k1gInSc7	vak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
čítají	čítat	k5eAaImIp3nP	čítat
obvykle	obvykle	k6eAd1	obvykle
7000	[number]	k4	7000
až	až	k9	až
50	[number]	k4	50
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
obou	dva	k4xCgInPc2	dva
rodičů	rodič	k1gMnPc2	rodič
shlukují	shlukovat	k5eAaImIp3nP	shlukovat
do	do	k7c2	do
takzvaných	takzvaný	k2eAgFnPc2d1	takzvaná
školek	školka	k1gFnPc2	školka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nemalý	malý	k2eNgInSc4d1	nemalý
počet	počet	k1gInSc4	počet
stejně	stejně	k6eAd1	stejně
starých	starý	k2eAgFnPc2d1	stará
vrstevníku	vrstevník	k1gMnSc6	vrstevník
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
těsně	těsně	k6eAd1	těsně
při	při	k7c6	při
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Pospolu	pospolu	k6eAd1	pospolu
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
bezpečí	bezpečí	k1gNnSc6	bezpečí
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	s	k7c7	s
choulením	choulení	k1gNnSc7	choulení
zahřejí	zahřát	k5eAaPmIp3nP	zahřát
v	v	k7c6	v
panujících	panující	k2eAgFnPc6d1	panující
nepříznivých	příznivý	k2eNgFnPc6d1	nepříznivá
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
tučňák	tučňák	k1gMnSc1	tučňák
císařský	císařský	k2eAgMnSc1d1	císařský
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
velmi	velmi	k6eAd1	velmi
náročný	náročný	k2eAgInSc4d1	náročný
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
můžou	můžou	k?	můžou
chovat	chovat	k5eAaImF	chovat
jen	jen	k9	jen
ty	ten	k3xDgFnPc4	ten
nejprestižnější	prestižní	k2eAgFnPc4d3	nejprestižnější
zoologické	zoologický	k2eAgFnPc4d1	zoologická
zahrady	zahrada	k1gFnPc4	zahrada
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jej	on	k3xPp3gNnSc2	on
trvale	trvale	k6eAd1	trvale
nechová	chovat	k5eNaImIp3nS	chovat
doposud	doposud	k6eAd1	doposud
žádná	žádný	k3yNgFnSc1	žádný
profesionální	profesionální	k2eAgFnSc1d1	profesionální
instituce	instituce	k1gFnSc1	instituce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
císařský	císařský	k2eAgMnSc1d1	císařský
(	(	kIx(	(
<g/>
Aptenodytes	Aptenodytes	k1gInSc1	Aptenodytes
forsteri	forster	k1gFnSc2	forster
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Antarktida	Antarktida	k1gFnSc1	Antarktida
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
(	(	kIx(	(
<g/>
Aptenodytes	Aptenodytes	k1gMnSc1	Aptenodytes
patagonicus	patagonicus	k1gMnSc1	patagonicus
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
subantarktické	subantarktický	k2eAgInPc1d1	subantarktický
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Falklandské	Falklandský	k2eAgInPc1d1	Falklandský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Georgie	Georgie	k1gFnSc1	Georgie
<g/>
)	)	kIx)	)
<g/>
rod	rod	k1gInSc1	rod
</s>
</p>
<p>
<s>
EudyptesPředstavuje	EudyptesPředstavovat	k5eAaImIp3nS	EudyptesPředstavovat
středně	středně	k6eAd1	středně
velké	velký	k2eAgMnPc4d1	velký
tučňáky	tučňák	k1gMnPc4	tučňák
<g/>
,	,	kIx,	,
svrchu	svrchu	k6eAd1	svrchu
tmavě	tmavě	k6eAd1	tmavě
šedé	šedý	k2eAgFnPc1d1	šedá
až	až	k8xS	až
černé	černý	k2eAgFnPc1d1	černá
<g/>
,	,	kIx,	,
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
spodinou	spodina	k1gFnSc7	spodina
<g/>
,	,	kIx,	,
s	s	k7c7	s
očima	oko	k1gNnPc7	oko
s	s	k7c7	s
hnědou	hnědý	k2eAgFnSc7d1	hnědá
nebo	nebo	k8xC	nebo
červenou	červený	k2eAgFnSc7d1	červená
duhovkou	duhovka	k1gFnSc7	duhovka
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
očima	oko	k1gNnPc7	oko
jim	on	k3xPp3gMnPc3	on
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
ozdobná	ozdobný	k2eAgNnPc1d1	ozdobné
zlatavě	zlatavě	k6eAd1	zlatavě
žlutá	žlutý	k2eAgNnPc1d1	žluté
pera	pero	k1gNnPc1	pero
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
bývají	bývat	k5eAaImIp3nP	bývat
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
části	část	k1gFnSc2	část
roku	rok	k1gInSc2	rok
na	na	k7c6	na
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
sociálnějším	sociální	k2eAgMnPc3d2	sociálnější
tučňákům	tučňák	k1gMnPc3	tučňák
<g/>
,	,	kIx,	,
využívajících	využívající	k2eAgInPc2d1	využívající
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
spektra	spektrum	k1gNnSc2	spektrum
dorozumívacích	dorozumívací	k2eAgInPc2d1	dorozumívací
–	–	k?	–
zvukových	zvukový	k2eAgInPc2d1	zvukový
–	–	k?	–
signálů	signál	k1gInPc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
obrovských	obrovský	k2eAgFnPc6d1	obrovská
koloniích	kolonie	k1gFnPc6	kolonie
čítajících	čítající	k2eAgMnPc2d1	čítající
až	až	k8xS	až
500	[number]	k4	500
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
páry	pár	k1gInPc1	pár
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
těsně	těsně	k6eAd1	těsně
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
tři	tři	k4xCgInPc1	tři
páry	pár	k1gInPc1	pár
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
metr	metr	k1gInSc4	metr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
<g/>
)	)	kIx)	)
a	a	k8xC	a
často	často	k6eAd1	často
tak	tak	k6eAd1	tak
svádějí	svádět	k5eAaImIp3nP	svádět
boje	boj	k1gInPc1	boj
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
k	k	k7c3	k
zahnízdění	zahnízdění	k1gNnSc3	zahnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Umějí	umět	k5eAaImIp3nP	umět
být	být	k5eAaImF	být
poměrně	poměrně	k6eAd1	poměrně
agresivní	agresivní	k2eAgNnSc1d1	agresivní
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
na	na	k7c6	na
pozoru	pozor	k1gInSc6	pozor
i	i	k9	i
chovatelé	chovatel	k1gMnPc1	chovatel
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
na	na	k7c6	na
neobvyklých	obvyklý	k2eNgInPc6d1	neobvyklý
skalnatých	skalnatý	k2eAgInPc6d1	skalnatý
výběžcích	výběžek	k1gInPc6	výběžek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
s	s	k7c7	s
kuráží	kuráží	k?	kuráží
zdolávají	zdolávat	k5eAaImIp3nP	zdolávat
a	a	k8xC	a
překonávají	překonávat	k5eAaImIp3nP	překonávat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nedostanou	dostat	k5eNaPmIp3nP	dostat
na	na	k7c4	na
vytyčený	vytyčený	k2eAgInSc4d1	vytyčený
cíl	cíl	k1gInSc4	cíl
–	–	k?	–
hnízdiště	hnízdiště	k1gNnSc2	hnízdiště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
mnohem	mnohem	k6eAd1	mnohem
silnější	silný	k2eAgInSc1d2	silnější
a	a	k8xC	a
robustnější	robustní	k2eAgInSc1d2	robustnější
zobák	zobák	k1gInSc1	zobák
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterému	který	k3yIgMnSc3	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
odlišní	odlišný	k2eAgMnPc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
Vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
velmi	velmi	k6eAd1	velmi
jednoduchá	jednoduchý	k2eAgNnPc1d1	jednoduché
hnízda	hnízdo	k1gNnPc1	hnízdo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vystlaný	vystlaný	k2eAgInSc4d1	vystlaný
důlek	důlek	k1gInSc4	důlek
osázený	osázený	k2eAgInSc4d1	osázený
kamínky	kamínek	k1gInPc4	kamínek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
snášejí	snášet	k5eAaImIp3nP	snášet
obvykle	obvykle	k6eAd1	obvykle
dvě	dva	k4xCgNnPc4	dva
vejce	vejce	k1gNnPc4	vejce
ale	ale	k8xC	ale
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případech	případ	k1gInPc6	případ
vychová	vychovat	k5eAaPmIp3nS	vychovat
pár	pár	k4xCyI	pár
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc4	jeden
mládě	mládě	k1gNnSc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
obou	dva	k4xCgMnPc2	dva
rodičů	rodič	k1gMnPc2	rodič
na	na	k7c6	na
moře	mora	k1gFnSc6	mora
se	se	k3xPyFc4	se
mláďata	mládě	k1gNnPc1	mládě
shlukují	shlukovat	k5eAaImIp3nP	shlukovat
do	do	k7c2	do
nevelkých	velký	k2eNgFnPc2d1	nevelká
skupinek	skupinka	k1gFnPc2	skupinka
<g/>
,	,	kIx,	,
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
se	s	k7c7	s
vzájemnou	vzájemný	k2eAgFnSc7d1	vzájemná
pomocí	pomoc	k1gFnSc7	pomoc
proti	proti	k7c3	proti
chladným	chladný	k2eAgFnPc3d1	chladná
teplotám	teplota	k1gFnPc3	teplota
ale	ale	k8xC	ale
především	především	k9	především
proti	proti	k7c3	proti
predátorům	predátor	k1gMnPc3	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
ve	v	k7c6	v
skutečných	skutečný	k2eAgFnPc6d1	skutečná
školkách	školka	k1gFnPc6	školka
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
mláďata	mládě	k1gNnPc1	mládě
tučňáků	tučňák	k1gMnPc2	tučňák
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
starších	starší	k1gMnPc2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
mladé	mladý	k2eAgInPc4d1	mladý
páry	pár	k1gInPc4	pár
bez	bez	k7c2	bez
potomků	potomek	k1gMnPc2	potomek
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
teprve	teprve	k6eAd1	teprve
získávají	získávat	k5eAaImIp3nP	získávat
cenné	cenný	k2eAgFnPc4d1	cenná
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
páry	pár	k1gInPc4	pár
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
o	o	k7c4	o
snůšku	snůška	k1gFnSc4	snůška
přišly	přijít	k5eAaPmAgFnP	přijít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
(	(	kIx(	(
<g/>
Eudyptes	Eudyptes	k1gInSc1	Eudyptes
sclateri	sclater	k1gFnSc2	sclater
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
souostroví	souostroví	k1gNnSc1	souostroví
Bounty	Bounta	k1gFnSc2	Bounta
a	a	k8xC	a
ostrovy	ostrov	k1gInPc1	ostrov
Protinožců	protinožec	k1gMnPc2	protinožec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
(	(	kIx(	(
<g/>
Eudyptes	Eudyptes	k1gMnSc1	Eudyptes
schlegeli	schleget	k5eAaBmAgMnP	schleget
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
především	především	k9	především
ostrov	ostrov	k1gInSc1	ostrov
Maquarie	Maquarie	k1gFnSc2	Maquarie
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
novozélandský	novozélandský	k2eAgMnSc1d1	novozélandský
(	(	kIx(	(
<g/>
Eudyptes	Eudyptes	k1gMnSc1	Eudyptes
pachyrhynchus	pachyrhynchus	k1gMnSc1	pachyrhynchus
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
skalní	skalní	k2eAgMnSc1d1	skalní
(	(	kIx(	(
<g/>
Eudyptes	Eudyptes	k1gMnSc1	Eudyptes
chrysocome	chrysocom	k1gInSc5	chrysocom
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Falklandské	Falklandský	k2eAgInPc1d1	Falklandský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
...	...	k?	...
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
snárský	snárský	k2eAgMnSc1d1	snárský
(	(	kIx(	(
<g/>
Eudyptes	Eudyptes	k1gMnSc1	Eudyptes
robustus	robustus	k1gMnSc1	robustus
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
(	(	kIx(	(
<g/>
ostrovy	ostrov	k1gInPc1	ostrov
Snares	Snaresa	k1gFnPc2	Snaresa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
žlutorohý	žlutorohý	k2eAgMnSc1d1	žlutorohý
(	(	kIx(	(
<g/>
Eudyptes	Eudyptes	k1gMnSc1	Eudyptes
chrysolophus	chrysolophus	k1gMnSc1	chrysolophus
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Falklandské	Falklandský	k2eAgInPc1d1	Falklandský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Georgie	Georgie	k1gFnSc1	Georgie
<g/>
,	,	kIx,	,
Crozetovy	Crozetův	k2eAgInPc1d1	Crozetův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
...	...	k?	...
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
</s>
</p>
<p>
<s>
EudyptulaPředstavuje	EudyptulaPředstavovat	k5eAaImIp3nS	EudyptulaPředstavovat
nejmenšího	malý	k2eAgInSc2d3	nejmenší
zástupce	zástupce	k1gMnSc2	zástupce
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
nápadně	nápadně	k6eAd1	nápadně
světlým	světlý	k2eAgMnPc3d1	světlý
<g/>
,	,	kIx,	,
modravě	modravě	k6eAd1	modravě
šedým	šedý	k2eAgNnSc7d1	šedé
peřím	peří	k1gNnSc7	peří
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
nejmenší	malý	k2eAgMnSc1d3	nejmenší
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
adaptoval	adaptovat	k5eAaBmAgInS	adaptovat
k	k	k7c3	k
nočnímu	noční	k2eAgInSc3d1	noční
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrabává	vyhrabávat	k5eAaImIp3nS	vyhrabávat
si	se	k3xPyFc3	se
nory	nora	k1gFnSc2	nora
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
celý	celý	k2eAgInSc4d1	celý
rozmnožovací	rozmnožovací	k2eAgInSc4d1	rozmnožovací
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
rodičů	rodič	k1gMnPc2	rodič
se	se	k3xPyFc4	se
ptáčata	ptáče	k1gNnPc4	ptáče
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
jednoduše	jednoduše	k6eAd1	jednoduše
schovají	schovat	k5eAaPmIp3nP	schovat
do	do	k7c2	do
útrob	útroba	k1gFnPc2	útroba
nor	nora	k1gFnPc2	nora
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
nejmenší	malý	k2eAgMnSc1d3	nejmenší
(	(	kIx(	(
<g/>
Eudyptula	Eudyptula	k1gFnSc1	Eudyptula
minor	minor	k2eAgFnSc1d1	minor
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Zélandrod	Zélandrod	k1gInSc4	Zélandrod
</s>
</p>
<p>
<s>
MegadyptesZahrnuje	MegadyptesZahrnovat	k5eAaBmIp3nS	MegadyptesZahrnovat
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgInSc1d1	jediný
žijící	žijící	k2eAgInSc1d1	žijící
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgMnSc1d1	vzácný
–	–	k?	–
hrozí	hrozit	k5eAaImIp3nS	hrozit
vyhynutí	vyhynutí	k1gNnSc4	vyhynutí
<g/>
.	.	kIx.	.
</s>
<s>
Specifický	specifický	k2eAgInSc1d1	specifický
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
žlutý	žlutý	k2eAgInSc1d1	žlutý
pásek	pásek	k1gInSc1	pásek
táhnoucí	táhnoucí	k2eAgFnSc2d1	táhnoucí
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
hlavy	hlava	k1gFnSc2	hlava
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
žlutooký	žlutooký	k2eAgMnSc1d1	žlutooký
(	(	kIx(	(
<g/>
Megadyptes	Megadyptes	k1gMnSc1	Megadyptes
antipodes	antipodes	k1gMnSc1	antipodes
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
</s>
</p>
<p>
<s>
†	†	k?	†
<g/>
Megadyptes	Megadyptes	k1gInSc1	Megadyptes
waitaharod	waitaharod	k1gInSc1	waitaharod
</s>
</p>
<p>
<s>
PygoscelisJsou	PygoscelisJsa	k1gFnSc7	PygoscelisJsa
to	ten	k3xDgNnSc1	ten
ptáci	pták	k1gMnPc1	pták
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
kartáčovým	kartáčový	k2eAgInSc7d1	kartáčový
ocasem	ocas	k1gInSc7	ocas
a	a	k8xC	a
jasně	jasně	k6eAd1	jasně
definovaným	definovaný	k2eAgInSc7d1	definovaný
černobílým	černobílý	k2eAgInSc7d1	černobílý
"	"	kIx"	"
<g/>
fráčkem	fráček	k1gInSc7	fráček
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
tučňákům	tučňák	k1gMnPc3	tučňák
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Aptenodytes	Aptenodytesa	k1gFnPc2	Aptenodytesa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
podstatně	podstatně	k6eAd1	podstatně
kratší	krátký	k2eAgInSc4d2	kratší
rozmnožovací	rozmnožovací	k2eAgInSc4d1	rozmnožovací
cyklus	cyklus	k1gInSc4	cyklus
a	a	k8xC	a
vychovávají	vychovávat	k5eAaImIp3nP	vychovávat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedno	jeden	k4xCgNnSc4	jeden
mládě	mládě	k1gNnSc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
hojní	hojný	k2eAgMnPc1d1	hojný
a	a	k8xC	a
přizpůsobiví	přizpůsobivý	k2eAgMnPc1d1	přizpůsobivý
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dnes	dnes	k6eAd1	dnes
nečelí	čelit	k5eNaImIp3nP	čelit
prakticky	prakticky	k6eAd1	prakticky
žádné	žádný	k3yNgFnSc3	žádný
větší	veliký	k2eAgFnSc3d2	veliký
hrozbě	hrozba	k1gFnSc3	hrozba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
(	(	kIx(	(
<g/>
Pygoscelis	Pygoscelis	k1gInSc1	Pygoscelis
adeliae	adelia	k1gInSc2	adelia
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Antarktida	Antarktida	k1gFnSc1	Antarktida
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
oslí	oslí	k2eAgMnSc1d1	oslí
(	(	kIx(	(
<g/>
Pygoscelis	Pygoscelis	k1gInSc1	Pygoscelis
papua	papu	k1gInSc2	papu
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Antarktida	Antarktida	k1gFnSc1	Antarktida
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
subantarktické	subantarktický	k2eAgInPc1d1	subantarktický
ostrovy	ostrov	k1gInPc1	ostrov
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
uzdičkový	uzdičkový	k2eAgMnSc1d1	uzdičkový
(	(	kIx(	(
<g/>
Pygoscelis	Pygoscelis	k1gInSc1	Pygoscelis
antarctica	antarctic	k1gInSc2	antarctic
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Antarktida	Antarktida	k1gFnSc1	Antarktida
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
subantarktické	subantarktický	k2eAgFnSc2d1	subantarktický
ostrovyrod	ostrovyrod	k1gInSc4	ostrovyrod
</s>
</p>
<p>
<s>
SpheniscusZahrnuje	SpheniscusZahrnovat	k5eAaBmIp3nS	SpheniscusZahrnovat
tučňáky	tučňák	k1gMnPc4	tučňák
tmavě	tmavě	k6eAd1	tmavě
šedé	šedý	k2eAgFnPc1d1	šedá
až	až	k8xS	až
černé	černý	k2eAgFnPc1d1	černá
barvy	barva	k1gFnPc1	barva
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
zádech	zádech	k1gInSc1	zádech
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
břicho	břicho	k1gNnSc4	břicho
bílé	bílý	k2eAgNnSc4d1	bílé
<g/>
,	,	kIx,	,
okolí	okolí	k1gNnSc4	okolí
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
zobáku	zobák	k1gInSc2	zobák
a	a	k8xC	a
tváří	tvářet	k5eAaImIp3nS	tvářet
nepokryté	pokrytý	k2eNgNnSc1d1	nepokryté
peřím	peří	k1gNnSc7	peří
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tváře	tvář	k1gFnPc4	tvář
mají	mít	k5eAaImIp3nP	mít
bílý	bílý	k2eAgInSc4d1	bílý
pruh	pruh	k1gInSc4	pruh
<g/>
,	,	kIx,	,
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
příčné	příčný	k2eAgFnSc2d1	příčná
černé	černá	k1gFnSc2	černá
pruhy	pruh	k1gInPc4	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
subtropičtí	subtropický	k2eAgMnPc1d1	subtropický
tučňáci	tučňák	k1gMnPc1	tučňák
navyklí	navyklý	k2eAgMnPc1d1	navyklý
vysokým	vysoký	k2eAgFnPc3d1	vysoká
teplotám	teplota	k1gFnPc3	teplota
<g/>
,	,	kIx,	,
hnízdící	hnízdící	k2eAgNnPc1d1	hnízdící
i	i	k9	i
v	v	k7c6	v
čtyřiceti	čtyřicet	k4xCc6	čtyřicet
stupňových	stupňový	k2eAgNnPc6d1	stupňové
vedrech	vedro	k1gNnPc6	vedro
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
denní	denní	k2eAgFnSc1d1	denní
aktivita	aktivita	k1gFnSc1	aktivita
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
často	často	k6eAd1	často
omezena	omezit	k5eAaPmNgFnS	omezit
na	na	k7c4	na
brzké	brzký	k2eAgNnSc4d1	brzké
nebo	nebo	k8xC	nebo
pozdní	pozdní	k2eAgNnSc4d1	pozdní
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
raději	rád	k6eAd2	rád
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
skrývají	skrývat	k5eAaImIp3nP	skrývat
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
antarktickým	antarktický	k2eAgMnPc3d1	antarktický
tučňákům	tučňák	k1gMnPc3	tučňák
tak	tak	k6eAd1	tak
mají	mít	k5eAaImIp3nP	mít
kratší	krátký	k2eAgNnSc4d2	kratší
opeření	opeření	k1gNnSc4	opeření
o	o	k7c6	o
menší	malý	k2eAgFnSc6d2	menší
hustotě	hustota	k1gFnSc6	hustota
<g/>
,	,	kIx,	,
a	a	k8xC	a
nedisponují	disponovat	k5eNaBmIp3nP	disponovat
ani	ani	k8xC	ani
tak	tak	k6eAd1	tak
výraznou	výrazný	k2eAgFnSc7d1	výrazná
vrstvou	vrstva	k1gFnSc7	vrstva
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Jistě	jistě	k9	jistě
také	také	k9	také
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
chovají	chovat	k5eAaImIp3nP	chovat
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
a	a	k8xC	a
návazně	návazně	k6eAd1	návazně
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
si	se	k3xPyFc3	se
zvykli	zvyknout	k5eAaPmAgMnP	zvyknout
si	se	k3xPyFc3	se
i	i	k9	i
na	na	k7c4	na
evropské	evropský	k2eAgFnPc4d1	Evropská
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
se	se	k3xPyFc4	se
také	také	k9	také
úspěšně	úspěšně	k6eAd1	úspěšně
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
Nehnízdi	hnízdit	k5eNaImRp2nS	hnízdit
ve	v	k7c6	v
velkých	velká	k1gFnPc6	velká
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
,	,	kIx,	,
nepochybně	pochybně	k6eNd1	pochybně
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nízkého	nízký	k2eAgInSc2d1	nízký
stavu	stav	k1gInSc2	stav
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Stavějí	stavět	k5eAaImIp3nP	stavět
si	se	k3xPyFc3	se
hnízda	hnízdo	k1gNnSc2	hnízdo
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
tučňák	tučňák	k1gMnSc1	tučňák
nejmenší	malý	k2eAgFnSc2d3	nejmenší
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Eudyptes	Eudyptesa	k1gFnPc2	Eudyptesa
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nory	nora	k1gFnPc1	nora
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
využívají	využívat	k5eAaImIp3nP	využívat
různých	různý	k2eAgFnPc2d1	různá
děr	děra	k1gFnPc2	děra
v	v	k7c6	v
měkké	měkký	k2eAgFnSc6d1	měkká
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dutiny	dutina	k1gFnPc1	dutina
ve	v	k7c6	v
skalách	skála	k1gFnPc6	skála
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
rodů	rod	k1gInPc2	rod
tučňáků	tučňák	k1gMnPc2	tučňák
jsou	být	k5eAaImIp3nP	být
tito	tento	k3xDgMnPc1	tento
ptáci	pták	k1gMnPc1	pták
asi	asi	k9	asi
nejvíce	hodně	k6eAd3	hodně
monogamičtí	monogamický	k2eAgMnPc1d1	monogamický
<g/>
;	;	kIx,	;
jeden	jeden	k4xCgInSc1	jeden
stejný	stejný	k2eAgInSc1d1	stejný
pár	pár	k1gInSc1	pár
spolu	spolu	k6eAd1	spolu
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
prakticky	prakticky	k6eAd1	prakticky
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
zde	zde	k6eAd1	zde
celkovou	celkový	k2eAgFnSc4d1	celková
bilanci	bilance	k1gFnSc4	bilance
narušují	narušovat	k5eAaImIp3nP	narušovat
hlavně	hlavně	k9	hlavně
mladé	mladý	k2eAgFnPc1d1	mladá
nezkušené	zkušený	k2eNgFnPc1d1	nezkušená
páry	pára	k1gFnPc1	pára
nebo	nebo	k8xC	nebo
úmrtí	úmrtí	k1gNnPc1	úmrtí
partnerů	partner	k1gMnPc2	partner
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejohroženější	ohrožený	k2eAgFnSc4d3	nejohroženější
vyhubením	vyhubení	k1gNnSc7	vyhubení
<g/>
,	,	kIx,	,
decimováni	decimován	k2eAgMnPc1d1	decimován
řadou	řada	k1gFnSc7	řada
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
způsobených	způsobený	k2eAgFnPc2d1	způsobená
především	především	k9	především
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
(	(	kIx(	(
<g/>
komerčním	komerční	k2eAgInSc7d1	komerční
rybolovem	rybolov	k1gInSc7	rybolov
<g/>
,	,	kIx,	,
invazí	invaze	k1gFnSc7	invaze
nepůvodních	původní	k2eNgInPc2d1	nepůvodní
predátorů	predátor	k1gMnPc2	predátor
<g/>
,	,	kIx,	,
haváriemi	havárie	k1gFnPc7	havárie
ropných	ropný	k2eAgInPc2d1	ropný
tankerů	tanker	k1gInPc2	tanker
<g/>
,	,	kIx,	,
obecným	obecný	k2eAgNnSc7d1	obecné
znečišťováním	znečišťování	k1gNnSc7	znečišťování
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
cestovním	cestovní	k2eAgInSc7d1	cestovní
ruchem	ruch	k1gInSc7	ruch
či	či	k8xC	či
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
rozvojem	rozvoj	k1gInSc7	rozvoj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
(	(	kIx(	(
<g/>
Spheniscus	Spheniscus	k1gMnSc1	Spheniscus
demersus	demersus	k1gMnSc1	demersus
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
galapážský	galapážský	k2eAgMnSc1d1	galapážský
(	(	kIx(	(
<g/>
Spheniscus	Spheniscus	k1gMnSc1	Spheniscus
mendiculus	mendiculus	k1gMnSc1	mendiculus
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Galapážské	galapážský	k2eAgInPc1d1	galapážský
ostrovy	ostrov	k1gInPc1	ostrov
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
(	(	kIx(	(
<g/>
Spheniscus	Spheniscus	k1gInSc1	Spheniscus
humboldti	humboldť	k1gFnSc2	humboldť
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
magellanský	magellanský	k2eAgMnSc1d1	magellanský
(	(	kIx(	(
<g/>
Spheniscus	Spheniscus	k1gMnSc1	Spheniscus
magellanicus	magellanicus	k1gMnSc1	magellanicus
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
chováno	chovat	k5eAaImNgNnS	chovat
10	[number]	k4	10
druhů	druh	k1gInPc2	druh
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgMnSc7d3	nejčastější
zástupcem	zástupce	k1gMnSc7	zástupce
tučňáků	tučňák	k1gMnPc2	tučňák
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
zoo	zoo	k1gFnPc6	zoo
je	být	k5eAaImIp3nS	být
tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
chován	chovat	k5eAaImNgInS	chovat
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
130	[number]	k4	130
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
(	(	kIx(	(
<g/>
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Zlín	Zlín	k1gInSc1	Zlín
a	a	k8xC	a
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
a	a	k8xC	a
tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
(	(	kIx(	(
<g/>
Zoo	zoo	k1gFnSc3	zoo
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
mezi	mezi	k7c4	mezi
méně	málo	k6eAd2	málo
náročné	náročný	k2eAgMnPc4d1	náročný
zástupce	zástupce	k1gMnPc4	zástupce
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
třeba	třeba	k6eAd1	třeba
antarktickým	antarktický	k2eAgInPc3d1	antarktický
druhům	druh	k1gInPc3	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Jihlava	Jihlava	k1gFnSc1	Jihlava
chováni	chovat	k5eAaImNgMnP	chovat
tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
a	a	k8xC	a
tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
zoo	zoo	k1gFnSc6	zoo
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
tučňáka	tučňák	k1gMnSc4	tučňák
brýlového	brýlový	k2eAgMnSc4d1	brýlový
<g/>
,	,	kIx,	,
tučňáka	tučňák	k1gMnSc2	tučňák
žlutorohého	žlutorohý	k2eAgMnSc2d1	žlutorohý
či	či	k8xC	či
tučňáka	tučňák	k1gMnSc2	tučňák
magellanského	magellanský	k2eAgMnSc2d1	magellanský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Zoo	zoo	k1gNnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
objevil	objevit	k5eAaPmAgInS	objevit
také	také	k9	také
tučňák	tučňák	k1gMnSc1	tučňák
skalní	skalní	k2eAgMnSc1d1	skalní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odraz	odraz	k1gInSc4	odraz
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
je	být	k5eAaImIp3nS	být
maskot	maskot	k1gInSc4	maskot
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Linux	Linux	kA	Linux
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
spatřila	spatřit	k5eAaPmAgFnS	spatřit
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Vývojářem	vývojář	k1gMnSc7	vývojář
linuxového	linuxový	k2eAgNnSc2d1	linuxové
jádra	jádro	k1gNnSc2	jádro
byl	být	k5eAaImAgInS	být
Linus	Linus	k1gMnSc1	Linus
Torvalds	Torvaldsa	k1gFnPc2	Torvaldsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
emblém	emblém	k1gInSc1	emblém
zobrazující	zobrazující	k2eAgInSc1d1	zobrazující
především	především	k9	především
"	"	kIx"	"
<g/>
tučňáka	tučňák	k1gMnSc4	tučňák
s	s	k7c7	s
hokejkou	hokejka	k1gFnSc7	hokejka
<g/>
"	"	kIx"	"
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
podobách	podoba	k1gFnPc6	podoba
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
hokejový	hokejový	k2eAgInSc1d1	hokejový
tým	tým	k1gInSc1	tým
Pittsburgh	Pittsburgha	k1gFnPc2	Pittsburgha
Penguins	Penguinsa	k1gFnPc2	Penguinsa
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
a	a	k8xC	a
do	do	k7c2	do
dnes	dnes	k6eAd1	dnes
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
nejprestižnější	prestižní	k2eAgFnSc6d3	nejprestižnější
hokejové	hokejový	k2eAgFnSc6d1	hokejová
soutěži	soutěž	k1gFnSc6	soutěž
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Piotr	Piotr	k1gMnSc1	Piotr
Jadwiszczak	Jadwiszczak	k1gMnSc1	Jadwiszczak
and	and	k?	and
Thomas	Thomas	k1gMnSc1	Thomas
Mörs	Mörs	k1gInSc1	Mörs
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
First	First	k1gMnSc1	First
partial	partiat	k5eAaImAgMnS	partiat
skeleton	skeleton	k1gInSc4	skeleton
of	of	k?	of
Delphinornis	Delphinornis	k1gFnSc2	Delphinornis
larseni	larsen	k2eAgMnPc1d1	larsen
Wiman	Wiman	k1gMnSc1	Wiman
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
a	a	k8xC	a
slender-footed	slenderooted	k1gMnSc1	slender-footed
penguin	penguin	k1gMnSc1	penguin
from	from	k1gMnSc1	from
the	the	k?	the
Eocene	Eocen	k1gInSc5	Eocen
of	of	k?	of
Antarctic	Antarctice	k1gFnPc2	Antarctice
Peninsula	Peninsula	k1gFnSc1	Peninsula
<g/>
.	.	kIx.	.
</s>
<s>
Palaeontologia	Palaeontologia	k1gFnSc1	Palaeontologia
Electronica	Electronica	k1gFnSc1	Electronica
22.2	[number]	k4	22.2
<g/>
.32	.32	k4	.32
<g/>
A	a	k9	a
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
doi	doi	k?	doi
<g/>
:	:	kIx,	:
https://doi.org/10.26879/933	[url]	k4	https://doi.org/10.26879/933
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tučňákovití	tučňákovitý	k2eAgMnPc1d1	tučňákovitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
tučňák	tučňák	k1gMnSc1	tučňák
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
tučňácích	tučňák	k1gMnPc6	tučňák
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
