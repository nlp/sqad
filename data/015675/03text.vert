<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1950	#num#	k4
(	(	kIx(
<g/>
71	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
PrahaČeskoslovensko	PrahaČeskoslovensko	k1gNnSc1
Československo	Československo	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
AMU	AMU	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
Aktivní	aktivní	k2eAgInPc4d1
roky	rok	k1gInPc4
</s>
<s>
1969	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Lábus	Lábus	k1gMnSc1
(	(	kIx(
<g/>
bratr	bratr	k1gMnSc1
<g/>
)	)	kIx)
Český	český	k2eAgMnSc1d1
lev	lev	k1gMnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
mužský	mužský	k2eAgInSc4d1
herecký	herecký	k2eAgInSc4d1
výkon	výkon	k1gInSc4
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
1994	#num#	k4
–	–	k?
Amerika	Amerika	k1gFnSc1
2013	#num#	k4
–	–	k?
Klauni	klaun	k1gMnPc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1950	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
dabér	dabér	k1gMnSc1
a	a	k8xC
bratr	bratr	k1gMnSc1
českého	český	k2eAgMnSc2d1
architekta	architekt	k1gMnSc2
Ladislava	Ladislav	k1gMnSc2
Lábuse	Lábuse	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1950	#num#	k4
v	v	k7c6
Praze	Praha	k1gFnSc6
v	v	k7c6
porodnici	porodnice	k1gFnSc6
v	v	k7c6
Londýnské	londýnský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
architekt	architekt	k1gMnSc1
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
zdravotní	zdravotní	k2eAgFnSc1d1
sestra	sestra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Vinohradech	Vinohrady	k1gInPc6
v	v	k7c6
Bělehradské	bělehradský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
také	také	k9
společně	společně	k6eAd1
s	s	k7c7
rodiči	rodič	k1gMnPc7
a	a	k8xC
bratrem	bratr	k1gMnSc7
bydlel	bydlet	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
od	od	k7c2
dětství	dětství	k1gNnSc2
se	se	k3xPyFc4
zajímal	zajímat	k5eAaImAgInS
o	o	k7c4
dramatické	dramatický	k2eAgNnSc4d1
umění	umění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
základní	základní	k2eAgFnSc6d1
škole	škola	k1gFnSc6
studoval	studovat	k5eAaImAgMnS
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
a	a	k8xC
po	po	k7c6
maturitě	maturita	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c6
DAMU	DAMU	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
absolvoval	absolvovat	k5eAaPmAgMnS
rolí	role	k1gFnSc7
Mackieho	Mackie	k1gMnSc2
Messera	Messer	k1gMnSc2
ve	v	k7c6
hře	hra	k1gFnSc6
Johna	John	k1gMnSc2
Gaye	gay	k1gMnSc2
Žebrácká	žebrácký	k2eAgFnSc1d1
opera	opera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
základní	základní	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
službě	služba	k1gFnSc6
byl	být	k5eAaImAgMnS
členem	člen	k1gInSc7
v	v	k7c6
Armádního	armádní	k2eAgInSc2d1
uměleckého	umělecký	k2eAgInSc2d1
souboru	soubor	k1gInSc2
Víta	Vít	k1gMnSc2
Nejedlého	Nejedlý	k1gMnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
dělal	dělat	k5eAaImAgMnS
konferenciéra	konferenciér	k1gMnSc4
tanečního	taneční	k2eAgInSc2d1
orchestru	orchestr	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k6eAd1
byl	být	k5eAaImAgInS
zdravotníkem	zdravotník	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
škole	škola	k1gFnSc6
byl	být	k5eAaImAgInS
Janem	Jan	k1gMnSc7
Schmidem	Schmid	k1gMnSc7
přijat	přijmout	k5eAaPmNgMnS
do	do	k7c2
angažmá	angažmá	k1gNnSc2
tehdy	tehdy	k6eAd1
libereckého	liberecký	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
Studio	studio	k1gNnSc1
Ypsilon	ypsilon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Liberce	Liberec	k1gInSc2
zpočátku	zpočátku	k6eAd1
dojížděl	dojíždět	k5eAaImAgMnS
<g/>
,	,	kIx,
pak	pak	k6eAd1
se	se	k3xPyFc4
tam	tam	k6eAd1
částečně	částečně	k6eAd1
přestěhoval	přestěhovat	k5eAaPmAgMnS
do	do	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
soubor	soubor	k1gInSc1
divadla	divadlo	k1gNnSc2
přesídlil	přesídlit	k5eAaPmAgInS
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Žije	žít	k5eAaImIp3nS
v	v	k7c6
Praze	Praha	k1gFnSc6
ve	v	k7c6
Vršovicích	Vršovice	k1gFnPc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
svobodný	svobodný	k2eAgMnSc1d1
a	a	k8xC
bezdětný	bezdětný	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Umělecká	umělecký	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
a	a	k8xC
ocenění	ocenění	k1gNnSc1
</s>
<s>
Svůj	svůj	k3xOyFgInSc4
především	především	k6eAd1
komediální	komediální	k2eAgInSc4d1
talent	talent	k1gInSc4
za	za	k7c4
více	hodně	k6eAd2
než	než	k8xS
čtyřicet	čtyřicet	k4xCc4
let	léto	k1gNnPc2
uplatnil	uplatnit	k5eAaPmAgMnS
v	v	k7c6
dlouhé	dlouhý	k2eAgFnSc6d1
řadě	řada	k1gFnSc6
divadelních	divadelní	k2eAgFnPc2d1
<g/>
,	,	kIx,
filmových	filmový	k2eAgFnPc2d1
<g/>
,	,	kIx,
rozhlasových	rozhlasový	k2eAgFnPc2d1
i	i	k8xC
televizních	televizní	k2eAgFnPc2d1
rolí	role	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stálé	stálý	k2eAgNnSc1d1
angažmá	angažmá	k1gNnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
divadle	divadlo	k1gNnSc6
Studio	studio	k1gNnSc1
Ypsilon	ypsilon	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hostuje	hostovat	k5eAaImIp3nS
také	také	k9
ve	v	k7c6
Viole	Viola	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Divadlu	divadlo	k1gNnSc3
Kalich	kalich	k1gInSc4
a	a	k8xC
v	v	k7c6
jiných	jiný	k2eAgNnPc6d1
divadlech	divadlo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
roli	role	k1gFnSc4
podváděného	podváděný	k2eAgMnSc2d1
manžela	manžel	k1gMnSc2
ve	v	k7c6
Vianově	Vianův	k2eAgFnSc6d1
hře	hra	k1gFnSc6
Hlava	hlava	k1gFnSc1
Medúzy	Medúza	k1gFnSc2
obdržel	obdržet	k5eAaPmAgMnS
za	za	k7c4
rok	rok	k1gInSc4
1996	#num#	k4
Cenu	cena	k1gFnSc4
Thálie	Thálie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
vedlejší	vedlejší	k2eAgFnPc4d1
roli	role	k1gFnSc4
strýce	strýc	k1gMnSc2
Jakoba	Jakob	k1gMnSc2
v	v	k7c6
Michálkově	Michálkův	k2eAgInSc6d1
filmu	film	k1gInSc6
Amerika	Amerika	k1gFnSc1
získal	získat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
Českého	český	k2eAgInSc2d1
lva	lev	k1gInSc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
taktéž	taktéž	k?
za	za	k7c4
vedlejší	vedlejší	k2eAgInSc4d1
mužský	mužský	k2eAgInSc4d1
herecký	herecký	k2eAgInSc4d1
výkon	výkon	k1gInSc4
ve	v	k7c6
filmu	film	k1gInSc6
Klauni	klaun	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Velmi	velmi	k6eAd1
známými	známý	k2eAgMnPc7d1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgInP
některé	některý	k3yIgInPc1
televizní	televizní	k2eAgInPc1d1
a	a	k8xC
rozhlasové	rozhlasový	k2eAgInPc1d1
pořady	pořad	k1gInPc1
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yQgFnPc6,k3yRgFnPc6,k3yIgFnPc6
spolupracoval	spolupracovat	k5eAaImAgMnS
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
dlouholetým	dlouholetý	k2eAgMnSc7d1
kolegou	kolega	k1gMnSc7
Oldřichem	Oldřich	k1gMnSc7
Kaiserem	Kaiser	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
spolupráce	spolupráce	k1gFnSc1
započala	započnout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
několika	několik	k4yIc7
scénkami	scénka	k1gFnPc7
v	v	k7c6
pořadu	pořad	k1gInSc6
Kabaret	kabaret	k1gInSc4
U	u	k7c2
dobré	dobrý	k2eAgFnSc2d1
pohody	pohoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
dvojice	dvojice	k1gFnSc1
spoluvytvářela	spoluvytvářet	k5eAaImAgFnS
a	a	k8xC
účinkovala	účinkovat	k5eAaImAgFnS
v	v	k7c6
televizních	televizní	k2eAgInPc6d1
pořadech	pořad	k1gInPc6
Možná	možná	k9
přijde	přijít	k5eAaPmIp3nS
i	i	k9
kouzelník	kouzelník	k1gMnSc1
<g/>
,	,	kIx,
Dva	dva	k4xCgInPc4
z	z	k7c2
jednoho	jeden	k4xCgNnSc2
města	město	k1gNnSc2
<g/>
,	,	kIx,
Zeměkoule	zeměkoule	k1gFnSc2
<g/>
,	,	kIx,
Letem	letem	k6eAd1
světem	svět	k1gInSc7
nebo	nebo	k8xC
Ruská	ruský	k2eAgFnSc1d1
ruleta	ruleta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
také	také	k9
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
nekonečný	konečný	k2eNgInSc1d1
rozhlasový	rozhlasový	k2eAgInSc1d1
seriál	seriál	k1gInSc1
Tlučhořovi	Tlučhoř	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
srdce	srdce	k1gNnSc2
dětských	dětský	k2eAgMnPc2d1
diváků	divák	k1gMnPc2
se	se	k3xPyFc4
zapsal	zapsat	k5eAaPmAgMnS
jako	jako	k9
čaroděj	čaroděj	k1gMnSc1
Rumburak	Rumburak	k1gMnSc1
ze	z	k7c2
seriálu	seriál	k1gInSc2
Arabela	Arabel	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
pro	pro	k7c4
veliký	veliký	k2eAgInSc4d1
zájem	zájem	k1gInSc4
dočkal	dočkat	k5eAaPmAgMnS
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
pokračování	pokračování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
děti	dítě	k1gFnPc4
také	také	k6eAd1
namlouval	namlouvat	k5eAaImAgMnS
plyšovou	plyšový	k2eAgFnSc4d1
postavu	postava	k1gFnSc4
Jů	Jů	k1gFnSc2
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
pořadu	pořad	k1gInSc6
Studio	studio	k1gNnSc1
Kamarád	kamarád	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
při	při	k7c6
dabování	dabování	k1gNnSc6
</s>
<s>
Svůj	svůj	k3xOyFgInSc1
hlas	hlas	k1gInSc1
propůjčil	propůjčit	k5eAaPmAgInS
postavám	postava	k1gFnPc3
z	z	k7c2
filmů	film	k1gInPc2
a	a	k8xC
seriálů	seriál	k1gInPc2
jako	jako	k9
dabér	dabér	k1gMnSc1
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
seriálu	seriál	k1gInSc6
M	M	kA
<g/>
*	*	kIx~
<g/>
A	A	kA
<g/>
*	*	kIx~
<g/>
S	s	k7c7
<g/>
*	*	kIx~
<g/>
H	H	kA
<g/>
,	,	kIx,
anebo	anebo	k8xC
kresleným	kreslený	k2eAgFnPc3d1
postavám	postava	k1gFnPc3
<g/>
,	,	kIx,
například	například	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
roli	role	k1gFnSc4
Obelixe	Obelixe	k1gFnSc2
ve	v	k7c6
filmech	film	k1gInPc6
Asterix	Asterix	k1gInSc4
a	a	k8xC
překvapení	překvapení	k1gNnSc4
pro	pro	k7c4
Caesara	Caesar	k1gMnSc4
a	a	k8xC
Asterix	Asterix	k1gInSc4
a	a	k8xC
velký	velký	k2eAgInSc4d1
boj	boj	k1gInSc4
<g/>
,	,	kIx,
lenochoda	lenochod	k1gMnSc4
Sida	Sidus	k1gMnSc4
ve	v	k7c6
filmu	film	k1gInSc6
Doba	doba	k1gFnSc1
ledová	ledový	k2eAgFnSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gNnPc2
dalších	další	k2eAgNnPc2d1
pokračování	pokračování	k1gNnPc2
nebo	nebo	k8xC
Marge	Marge	k1gFnPc2
v	v	k7c6
seriálu	seriál	k1gInSc6
Simpsonovi	Simpson	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
daboval	dabovat	k5eAaBmAgInS
postavy	postava	k1gFnSc2
v	v	k7c6
PC	PC	kA
hrách	hrách	k1gInSc1
(	(	kIx(
<g/>
např.	např.	kA
Ve	v	k7c6
stínu	stín	k1gInSc6
havrana	havran	k1gMnSc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
šest	šest	k4xCc4
dílů	díl	k1gInPc2
Polda	Polda	k1gMnSc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
účinkoval	účinkovat	k5eAaImAgMnS
v	v	k7c6
komunistickém	komunistický	k2eAgInSc6d1
propagandistickém	propagandistický	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
Třicet	třicet	k4xCc1
případů	případ	k1gInPc2
majora	major	k1gMnSc2
Zemana	Zeman	k1gMnSc2
<g/>
,	,	kIx,
v	v	k7c6
díle	dílo	k1gNnSc6
Mimikry	mimikry	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
očernit	očernit	k5eAaPmF
kapelu	kapela	k1gFnSc4
The	The	k1gFnSc2
Plastic	Plastice	k1gFnPc2
People	People	k1gFnSc2
of	of	k?
the	the	k?
Universe	Universe	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
tuto	tento	k3xDgFnSc4
roli	role	k1gFnSc4
se	se	k3xPyFc4
později	pozdě	k6eAd2
omluvil	omluvit	k5eAaPmAgMnS
veřejnosti	veřejnost	k1gFnPc4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
i	i	k8xC
kapele	kapela	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
namluvil	namluvit	k5eAaPmAgMnS,k5eAaBmAgMnS
pro	pro	k7c4
nakladatelství	nakladatelství	k1gNnSc4
Albatros	albatros	k1gMnSc1
podle	podle	k7c2
knihy	kniha	k1gFnSc2
Joanny	Joanna	k1gFnPc4
Rowlingové	Rowlingový	k2eAgFnPc4d1
audioknihu	audioknih	k1gInSc2
Harry	Harra	k1gFnPc4
Potter	Pottra	k1gFnPc2
a	a	k8xC
kámen	kámen	k1gInSc1
mudrců	mudrc	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
vytvořil	vytvořit	k5eAaPmAgInS
spolu	spolu	k6eAd1
s	s	k7c7
Petrem	Petr	k1gMnSc7
Čtvrtníčkem	Čtvrtníček	k1gMnSc7
hru	hra	k1gFnSc4
Ivánku	Ivánek	k1gMnSc3
<g/>
,	,	kIx,
kamaráde	kamarád	k1gMnSc5
<g/>
,	,	kIx,
můžeš	moct	k5eAaImIp2nS
mluvit	mluvit	k5eAaImF
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
vycházející	vycházející	k2eAgInSc1d1
z	z	k7c2
policejních	policejní	k2eAgInPc2d1
odposlechů	odposlech	k1gInPc2
úplatných	úplatný	k2eAgMnPc2d1
fotbalových	fotbalový	k2eAgMnPc2d1
činovníků	činovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Několikrát	několikrát	k6eAd1
též	též	k9
vystoupil	vystoupit	k5eAaPmAgMnS
na	na	k7c6
festivalu	festival	k1gInSc6
Trutnoff	Trutnoff	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
publiku	publikum	k1gNnSc3
předčítal	předčítat	k5eAaImAgMnS
básně	báseň	k1gFnPc4
Ivana	Ivan	k1gMnSc2
Martina	Martin	k1gMnSc2
Jirouse	Jirouse	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
ztvárnil	ztvárnit	k5eAaPmAgMnS
v	v	k7c6
seriálu	seriál	k1gInSc6
České	český	k2eAgNnSc1d1
století	století	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
díle	díl	k1gInSc6
věnovaném	věnovaný	k2eAgInSc6d1
Sametové	sametový	k2eAgFnPc4d1
revoluci	revoluce	k1gFnSc4
<g/>
,	,	kIx,
roli	role	k1gFnSc4
československého	československý	k2eAgMnSc4d1
komunistického	komunistický	k2eAgMnSc4d1
premiéra	premiér	k1gMnSc4
Ladislava	Ladislav	k1gMnSc4
Adamce	Adamec	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1
aktivity	aktivita	k1gFnPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
připojil	připojit	k5eAaPmAgInS
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
mnozí	mnohý	k2eAgMnPc1d1
tehdejší	tehdejší	k2eAgMnPc1d1
umělci	umělec	k1gMnPc1
svůj	svůj	k3xOyFgInSc4
podpis	podpis	k1gInSc4
pod	pod	k7c4
prohlášení	prohlášení	k1gNnSc4
odsuzující	odsuzující	k2eAgFnSc4d1
Chartu	charta	k1gFnSc4
77	#num#	k4
<g/>
,	,	kIx,
tzv.	tzv.	kA
Antichartu	anticharta	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
lednu	leden	k1gInSc6
1989	#num#	k4
podepsal	podepsat	k5eAaPmAgMnS
petici	petice	k1gFnSc4
kulturních	kulturní	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
za	za	k7c4
propuštění	propuštění	k1gNnSc4
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
a	a	k8xC
petici	petice	k1gFnSc4
Několik	několik	k4yIc1
vět	věta	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
to	ten	k3xDgNnSc4
byl	být	k5eAaImAgInS
zakázán	zakázán	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
pořad	pořad	k1gInSc4
Možná	možná	k9
přijde	přijít	k5eAaPmIp3nS
i	i	k9
kouzelník	kouzelník	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
tváří	tvář	k1gFnSc7
vládní	vládní	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
podporující	podporující	k2eAgFnSc4d1
výstavbu	výstavba	k1gFnSc4
radaru	radar	k1gInSc2
americké	americký	k2eAgFnSc2d1
protiraketové	protiraketový	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lábus	Lábus	k1gMnSc1
podporuje	podporovat	k5eAaImIp3nS
obnovu	obnova	k1gFnSc4
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
signatářů	signatář	k1gMnPc2
monarchistického	monarchistický	k2eAgNnSc2d1
prohlášení	prohlášení	k1gNnSc2
Na	na	k7c6
prahu	práh	k1gInSc6
nového	nový	k2eAgNnSc2d1
milénia	milénium	k1gNnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
byl	být	k5eAaImAgMnS
spisovatel	spisovatel	k1gMnSc1
Petr	Petr	k1gMnSc1
Placák	placák	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1
role	role	k1gFnSc1
(	(	kIx(
<g/>
výběr	výběr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Studio	studio	k1gNnSc1
Ypsilon	ypsilon	k1gNnSc2
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Škaredá	škaredý	k2eAgFnSc1d1
středa	středa	k1gFnSc1
aneb	aneb	k?
Teta	Teta	k1gFnSc1
z	z	k7c2
Halifaxu	Halifax	k1gInSc2
(	(	kIx(
<g/>
prem	prem	k6eAd1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
26	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pánská	pánský	k2eAgFnSc1d1
šatna	šatna	k1gFnSc1
aneb	aneb	k?
Improvizace	improvizace	k1gFnSc1
v	v	k7c6
Ypsilonce	Ypsilonka	k1gFnSc6
(	(	kIx(
<g/>
prem	prem	k6eAd1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
12	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Babička	babička	k1gFnSc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
čili	čili	k8xC
Dichtung	Dichtung	k1gMnSc1
und	und	k?
Wahrheit	Wahrheit	k1gMnSc1
(	(	kIx(
<g/>
prem	prem	k1gMnSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
11	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vdovou	vdova	k1gFnSc7
proti	proti	k7c3
své	svůj	k3xOyFgFnSc3
vůli	vůle	k1gFnSc3
(	(	kIx(
<g/>
prem	prem	k6eAd1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
20.12	20.12	k4
<g/>
.2007	.2007	k4
<g/>
)	)	kIx)
</s>
<s>
Vinobraní	vinobraní	k1gNnSc1
v	v	k7c6
Ypsilonce	Ypsilonka	k1gFnSc6
aneb	aneb	k?
Dožínky	dožínky	k1gFnPc4
operety	opereta	k1gFnSc2
(	(	kIx(
<g/>
prem	prem	k6eAd1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
25	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
,	,	kIx,
uváděno	uváděn	k2eAgNnSc1d1
do	do	k7c2
<g/>
:	:	kIx,
leden	leden	k1gInSc1
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rusalka	rusalka	k1gFnSc1
nejen	nejen	k6eAd1
podle	podle	k7c2
Dvořáka	Dvořák	k1gMnSc2
(	(	kIx(
<g/>
prem	prem	k6eAd1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
28	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
stověžatá	stověžatý	k2eAgFnSc1d1
(	(	kIx(
<g/>
prem	prem	k1gMnSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
12	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
(	(	kIx(
<g/>
prem	prem	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
8	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Prodaná	prodaný	k2eAgFnSc1d1
nevěsta	nevěsta	k1gFnSc1
(	(	kIx(
<g/>
prem	prem	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
6	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mozart	Mozart	k1gMnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
prem	prem	k6eAd1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
10	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Studio	studio	k1gNnSc1
Ypsilon	ypsilon	k1gNnSc2
(	(	kIx(
<g/>
malá	malý	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vratká	vratký	k2eAgNnPc1d1
prkna	prkno	k1gNnPc1
(	(	kIx(
<g/>
prem	prem	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
8	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hlava	hlava	k1gFnSc1
medúzy	medúza	k1gFnSc2
(	(	kIx(
<g/>
prem	prem	k6eAd1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
2	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Divadlo	divadlo	k1gNnSc1
Kalich	kalich	k1gInSc1
(	(	kIx(
<g/>
činohra	činohra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Žena	žena	k1gFnSc1
za	za	k7c7
pultem	pult	k1gInSc7
2	#num#	k4
<g/>
:	:	kIx,
Pult	pult	k1gInSc1
osobnosti	osobnost	k1gFnSc2
(	(	kIx(
<g/>
prem	prem	k6eAd1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
23	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Don	Don	k1gMnSc1
Quijote	Quijot	k1gInSc5
(	(	kIx(
<g/>
prem	prem	k1gInPc3
<g/>
.	.	kIx.
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Divadlo	divadlo	k1gNnSc1
MANA	mana	k1gFnSc1
(	(	kIx(
<g/>
Vršovické	vršovický	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
MANA	mana	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vršovice	Vršovice	k1gFnPc1
jsou	být	k5eAaImIp3nP
zlatý	zlatý	k2eAgInSc4d1
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
tatínek	tatínek	k1gMnSc1
(	(	kIx(
<g/>
prem	prem	k1gMnSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
24.3	24.3	k4
<g/>
.2015	.2015	k4
<g/>
)	)	kIx)
</s>
<s>
Divadlo	divadlo	k1gNnSc1
v	v	k7c6
Dlouhé	Dlouhé	k2eAgFnSc6d1
</s>
<s>
S	s	k7c7
nadějí	naděje	k1gFnSc7
i	i	k8xC
bez	bez	k7c2
ní	on	k3xPp3gFnSc2
<g/>
/	/	kIx~
Ela	Ela	k1gFnSc1
<g/>
,	,	kIx,
Hela	Hela	k1gFnSc1
a	a	k8xC
stop	stop	k1gInSc1
(	(	kIx(
<g/>
prem	prem	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
13	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
,	,	kIx,
uváděno	uváděn	k2eAgNnSc1d1
do	do	k7c2
<g/>
:	:	kIx,
únor	únor	k1gInSc1
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Divadlo	divadlo	k1gNnSc1
Viola	Viola	k1gFnSc1
</s>
<s>
Normální	normální	k2eAgInSc1d1
okruh	okruh	k1gInSc1
(	(	kIx(
<g/>
prem	prem	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
22	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
uváděno	uváděn	k2eAgNnSc1d1
do	do	k7c2
<g/>
:	:	kIx,
červen	červen	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Proč	proč	k6eAd1
si	se	k3xPyFc3
nezatančíte	zatančit	k5eNaPmIp2nP
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
prem	prem	k6eAd1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
15	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
rovněž	rovněž	k9
režíroval	režírovat	k5eAaImAgMnS
<g/>
)	)	kIx)
</s>
<s>
Divadlo	divadlo	k1gNnSc1
Ungelt	ungelt	k1gInSc1
</s>
<s>
Play	play	k0
Strindberg	Strindberg	k1gMnSc1
(	(	kIx(
<g/>
prem	prem	k1gMnSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
23	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
uváděno	uváděn	k2eAgNnSc1d1
do	do	k7c2
<g/>
:	:	kIx,
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Filmografie	filmografie	k1gFnSc1
(	(	kIx(
<g/>
výběr	výběr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1975	#num#	k4
Páni	pan	k1gMnPc1
kluci	kluk	k1gMnPc1
</s>
<s>
1977	#num#	k4
Zítra	zítra	k6eAd1
vstanu	vstát	k5eAaPmIp1nS
a	a	k8xC
opařím	opařit	k5eAaPmIp1nS
se	se	k3xPyFc4
čajem	čaj	k1gInSc7
</s>
<s>
1980	#num#	k4
Krakonoš	Krakonoš	k1gMnSc1
a	a	k8xC
lyžníci	lyžník	k1gMnPc1
</s>
<s>
1981	#num#	k4
Velká	velký	k2eAgFnSc1d1
sázka	sázka	k1gFnSc1
o	o	k7c4
malé	malý	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
</s>
<s>
Slunce	slunce	k1gNnSc1
<g/>
,	,	kIx,
seno	seno	k1gNnSc1
<g/>
,	,	kIx,
jahody	jahoda	k1gFnPc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Babičky	babička	k1gFnPc4
dobíjejte	dobíjet	k5eAaImRp2nP
přesně	přesně	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1984	#num#	k4
Rumburak	Rumburak	k1gInSc1
-	-	kIx~
role	role	k1gFnSc1
<g/>
:	:	kIx,
Rumburak	Rumburak	k1gMnSc1
<g/>
,	,	kIx,
čaroděj	čaroděj	k1gMnSc1
druhé	druhý	k4xOgFnSc2
kategorie	kategorie	k1gFnSc2
</s>
<s>
Cirkus	cirkus	k1gInSc1
Humberto	Humberta	k1gFnSc5
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
TV	TV	kA
seriál	seriál	k1gInSc1
</s>
<s>
Slunce	slunce	k1gNnSc1
<g/>
,	,	kIx,
seno	seno	k1gNnSc1
a	a	k8xC
pár	pár	k4xCyI
facek	facka	k1gFnPc2
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Slunce	slunce	k1gNnSc1
<g/>
,	,	kIx,
seno	seno	k1gNnSc1
<g/>
,	,	kIx,
erotika	erotika	k1gFnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Amerika	Amerika	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pension	pension	k1gInSc1
Lola	Lol	k1gInSc2
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nesmrtelná	smrtelný	k2eNgFnSc1d1
teta	teta	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Arabela	Arabela	k1gFnSc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
aneb	aneb	k?
Rumburak	Rumburak	k1gInSc1
králem	král	k1gMnSc7
Říše	říš	k1gFnSc2
pohádek	pohádka	k1gFnPc2
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
TV	TV	kA
seriál	seriál	k1gInSc1
</s>
<s>
Andělské	andělský	k2eAgNnSc1d1
oči	oko	k1gNnPc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Spiklenci	spiklenec	k1gMnPc1
slasti	slast	k1gFnSc2
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zdivočelá	zdivočelý	k2eAgFnSc1d1
země	země	k1gFnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
TV	TV	kA
seriál	seriál	k1gInSc1
</s>
<s>
Lotrando	Lotrando	k1gMnSc1
a	a	k8xC
Zubejda	Zubejda	k1gMnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eliška	Eliška	k1gFnSc1
má	mít	k5eAaImIp3nS
ráda	rád	k2eAgFnSc1d1
divočinu	divočina	k1gFnSc4
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Babí	babí	k2eAgNnSc1d1
léto	léto	k1gNnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tmavomodrý	tmavomodrý	k2eAgInSc1d1
svět	svět	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čert	čert	k1gMnSc1
ví	vědět	k5eAaImIp3nS
proč	proč	k6eAd1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Obsluhoval	obsluhovat	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
anglického	anglický	k2eAgMnSc4d1
krále	král	k1gMnSc4
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kozí	kozí	k2eAgInSc1d1
příběh	příběh	k1gInSc1
–	–	k?
pověsti	pověst	k1gFnSc2
staré	starý	k2eAgFnSc2d1
Prahy	Praha	k1gFnSc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odcházení	odcházení	k1gNnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tajemství	tajemství	k1gNnSc1
staré	starý	k2eAgFnSc2d1
bambitky	bambitka	k1gFnSc2
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kozí	kozí	k2eAgInSc1d1
příběh	příběh	k1gInSc1
se	s	k7c7
sýrem	sýr	k1gInSc7
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Klauni	klaun	k1gMnPc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tři	tři	k4xCgMnPc1
bratři	bratr	k1gMnPc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Goblin	Goblin	k1gInSc1
2	#num#	k4
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Řachanda	Řachanda	k1gFnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Případ	případ	k1gInSc1
pro	pro	k7c4
lyžaře	lyžař	k1gMnSc4
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jak	jak	k8xC,k8xS
básníci	básník	k1gMnPc1
čekají	čekat	k5eAaImIp3nP
na	na	k7c4
zázrak	zázrak	k1gInSc4
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Holčička	holčička	k1gFnSc1
s	s	k7c7
náhradní	náhradní	k2eAgFnSc7d1
hlavou	hlava	k1gFnSc7
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tabule	tabule	k1gFnSc1
modrá	modrat	k5eAaImIp3nS
jako	jako	k9
nebe	nebe	k1gNnSc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
přítel	přítel	k1gMnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
TV	TV	kA
film	film	k1gInSc1
</s>
<s>
Hurvínek	Hurvínek	k1gMnSc1
a	a	k8xC
kouzelné	kouzelný	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Austerlitz	Austerlitz	k1gInSc1
Advent	advent	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Věčně	věčně	k6eAd1
tvá	tvůj	k3xOp2gFnSc1
nevěrná	věrný	k2eNgFnSc1d1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Revival	revival	k1gInSc1
–	–	k?
Klip	klip	k1gInSc1
života	život	k1gInSc2
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odborný	odborný	k2eAgInSc1d1
dohled	dohled	k1gInSc1
nad	nad	k7c7
výkladem	výklad	k1gInSc7
snu	sen	k1gInSc2
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kluci	kluk	k1gMnPc1
z	z	k7c2
hor	hora	k1gFnPc2
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hmyz	hmyz	k1gInSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hastrman	hastrman	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dívka	dívka	k1gFnSc1
za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
TV	TV	kA
film	film	k1gInSc1
</s>
<s>
Úhoři	úhoř	k1gMnPc1
mají	mít	k5eAaImIp3nP
nabito	nabit	k2eAgNnSc1d1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2019	#num#	k4
Vlastníci	vlastník	k1gMnPc1
</s>
<s>
2019	#num#	k4
Velké	velký	k2eAgInPc1d1
dobrodružství	dobrodružství	k1gNnSc4
Čtyřlístku	čtyřlístek	k1gInSc2
</s>
<s>
2021	#num#	k4
Zločiny	zločin	k1gMnPc7
Velké	velký	k2eAgFnSc2d1
Prahy	Praha	k1gFnSc2
</s>
<s>
Televize	televize	k1gFnSc1
</s>
<s>
1977	#num#	k4
Parádní	parádní	k2eAgInSc4d1
číslo	číslo	k1gNnSc1
(	(	kIx(
<g/>
TV	TV	kA
komedie	komedie	k1gFnSc2
<g/>
)	)	kIx)
-	-	kIx~
role	role	k1gFnSc1
<g/>
:	:	kIx,
aranžér	aranžér	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
Sládeček	Sládeček	k1gMnSc1
</s>
<s>
1979	#num#	k4
Arabela	Arabela	k1gFnSc1
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
-	-	kIx~
role	role	k1gFnSc1
<g/>
:	:	kIx,
Rumburak	Rumburak	k1gMnSc1
<g/>
,	,	kIx,
čaroděj	čaroděj	k1gMnSc1
druhé	druhý	k4xOgFnSc2
kategorie	kategorie	k1gFnSc2
</s>
<s>
Rozhlasové	rozhlasový	k2eAgFnPc1d1
role	role	k1gFnPc1
(	(	kIx(
<g/>
výběr	výběr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
Jiří	Jiří	k1gMnSc1
Robert	Robert	k1gMnSc1
Pick	Pick	k1gMnSc1
<g/>
:	:	kIx,
Anekdoty	anekdota	k1gFnSc2
Franci	Franci	k1gMnSc1
Roubíčka	Roubíček	k1gMnSc2
<g/>
,	,	kIx,
tragikomedie	tragikomedie	k1gFnSc1
o	o	k7c6
muži	muž	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
nedokázal	dokázat	k5eNaPmAgMnS
neříct	říct	k5eNaPmF
anekdotu	anekdota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hudba	hudba	k1gFnSc1
Vladimír	Vladimír	k1gMnSc1
Truc	truc	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dramaturg	dramaturg	k1gMnSc1
Dušan	Dušan	k1gMnSc1
Všelicha	Všelich	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režie	režie	k1gFnSc1
Josef	Josef	k1gMnSc1
Červinka	Červinka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účinkují	účinkovat	k5eAaImIp3nP
<g/>
:	:	kIx,
Tomáš	Tomáš	k1gMnSc1
Töpfer	Töpfer	k1gMnSc1
<g/>
,	,	kIx,
Růžena	Růžena	k1gFnSc1
Merunková	Merunková	k1gFnSc1
<g/>
,	,	kIx,
Barbora	Barbora	k1gFnSc1
Kodetová	Kodetový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
Marešová	Marešová	k1gFnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
Hlavica	Hlavica	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Ornest	Ornest	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Velda	Velda	k1gMnSc1
<g/>
,	,	kIx,
Oldřich	Oldřich	k1gMnSc1
Vízner	Vízner	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Velda	Velda	k1gMnSc1
<g/>
,	,	kIx,
Simona	Simona	k1gFnSc1
Stašová	Stašová	k1gFnSc1
a	a	k8xC
Jaroslava	Jaroslava	k1gFnSc1
Kretschmerová	Kretschmerová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Natočeno	natočit	k5eAaBmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1998	#num#	k4
Carey	Carey	k1gInPc1
Harrison	Harrison	k1gMnSc1
<g/>
:	:	kIx,
To	ten	k3xDgNnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
psychiatrovi	psychiatr	k1gMnSc3
stát	stát	k5eAaImF,k5eAaPmF
nemělo	mít	k5eNaImAgNnS
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Červinka	Červinka	k1gMnSc1
<g/>
,	,	kIx,
hráli	hrát	k5eAaImAgMnP
<g/>
:	:	kIx,
Vladimír	Vladimír	k1gMnSc1
Brabec	Brabec	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Kepka	Kepka	k1gMnSc1
<g/>
,	,	kIx,
Dana	Dana	k1gFnSc1
Syslová	Syslová	k1gFnSc1
a	a	k8xC
Josef	Josef	k1gMnSc1
Červinka	Červinka	k1gMnSc1
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příběhy	příběh	k1gInPc1
zvířátek	zvířátko	k1gNnPc2
(	(	kIx(
<g/>
audiokniha	audiokniha	k1gFnSc1
<g/>
,	,	kIx,
vydavatelstvo	vydavatelstvo	k1gNnSc1
Audiotéka	Audiotéek	k1gInSc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
David	David	k1gMnSc1
Williams	Williams	k1gInSc1
<g/>
:	:	kIx,
Babička	babička	k1gFnSc1
drsňačka	drsňačka	k1gFnSc1
<g/>
,	,	kIx,
audiokniha	audiokniha	k1gFnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jů	Jů	k?
a	a	k8xC
Hele	Hela	k1gFnSc6
–	–	k?
Pohádky	pohádka	k1gFnSc2
<g/>
,	,	kIx,
audiokniha	audiokniha	k1gFnSc1
</s>
<s>
Miloslav	Miloslav	k1gMnSc1
Švandrlík	Švandrlík	k1gMnSc1
<g/>
:	:	kIx,
Draculův	Draculův	k2eAgMnSc1d1
švagr	švagr	k1gMnSc1
<g/>
,	,	kIx,
audiokniha	audiokniha	k1gFnSc1
<g/>
,	,	kIx,
vydavatelství	vydavatelství	k1gNnSc1
Fonia	Fonia	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Friedrich	Friedrich	k1gMnSc1
Dürrenmatt	Dürrenmatt	k1gMnSc1
<g/>
:	:	kIx,
Proces	proces	k1gInSc1
o	o	k7c4
oslí	oslí	k2eAgInSc4d1
stín	stín	k1gInSc4
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jana	Jana	k1gFnSc1
Synková	Synková	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Schmid	Schmid	k1gInSc1
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Hynek	Hynek	k1gMnSc1
Mácha	Mácha	k1gMnSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
<g/>
,	,	kIx,
Oldřich	Oldřich	k1gMnSc1
Kaiser	Kaiser	k1gMnSc1
<g/>
:	:	kIx,
Ženich	ženich	k1gMnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Jiráň	Jiráň	k1gFnSc1
<g/>
,	,	kIx,
Marián	Marián	k1gMnSc1
Palla	Palla	k1gMnSc1
<g/>
:	:	kIx,
Zmatky	zmatek	k1gInPc1
Hanuše	Hanuš	k1gMnSc2
Hanuše	Hanuš	k1gMnSc2
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Paulo	Paula	k1gFnSc5
Coelho	Coelha	k1gFnSc5
<g/>
:	:	kIx,
Alchymista	alchymista	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ingmar	Ingmar	k1gMnSc1
Bergman	Bergman	k1gMnSc1
<g/>
:	:	kIx,
Malba	malba	k1gFnSc1
na	na	k7c6
dřevě	dřevo	k1gNnSc6
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2006	#num#	k4
George	George	k1gNnSc7
Tabori	Tabori	k1gNnSc2
<g/>
:	:	kIx,
Matčina	matčin	k2eAgFnSc1d1
Kuráž	Kuráž	k?
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
<g/>
:	:	kIx,
Petr	Petr	k1gMnSc1
Štědroň	Štědroň	k1gMnSc1
<g/>
,	,	kIx,
hudba	hudba	k1gFnSc1
<g/>
:	:	kIx,
Marko	Marko	k1gMnSc1
Ivanovič	Ivanovič	k1gMnSc1
<g/>
,	,	kIx,
dramaturgie	dramaturgie	k1gFnSc1
<g/>
:	:	kIx,
Martin	Martin	k1gMnSc1
Velíšek	Velíšek	k1gMnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Aleš	Aleš	k1gMnSc1
Vrzák	Vrzák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osoby	osoba	k1gFnPc1
a	a	k8xC
obsazení	obsazení	k1gNnSc1
<g/>
:	:	kIx,
syn	syn	k1gMnSc1
(	(	kIx(
<g/>
Jiří	Jiří	k1gMnSc1
Ornest	Ornest	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
(	(	kIx(
<g/>
Květa	Květa	k1gFnSc1
Fialová	Fialová	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kelemen	Kelemen	k1gInSc1
(	(	kIx(
<g/>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Usoplenec	usoplenec	k1gMnSc1
(	(	kIx(
<g/>
David	David	k1gMnSc1
Novotný	Novotný	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
(	(	kIx(
<g/>
Jaromír	Jaromír	k1gMnSc1
Dulava	Dulava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
policista	policista	k1gMnSc1
(	(	kIx(
<g/>
Stanislav	Stanislav	k1gMnSc1
Zindulka	Zindulka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
policista	policista	k1gMnSc1
(	(	kIx(
<g/>
Antonín	Antonín	k1gMnSc1
Molčík	Molčík	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strýc	strýc	k1gMnSc1
Julius	Julius	k1gMnSc1
(	(	kIx(
<g/>
Miloš	Miloš	k1gMnSc1
Hlavica	Hlavica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Marta	Marta	k1gFnSc1
(	(	kIx(
<g/>
Růžena	Růžena	k1gFnSc1
Merunková	Merunková	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
milenec	milenec	k1gMnSc1
+	+	kIx~
hlas	hlas	k1gInSc1
(	(	kIx(
<g/>
Vojtěch	Vojtěch	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Hájek	Hájek	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
milenec	milenec	k1gMnSc1
+	+	kIx~
hlas	hlas	k1gInSc1
(	(	kIx(
<g/>
Michal	Michal	k1gMnSc1
Zelenka	Zelenka	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hlasy	hlas	k1gInPc1
(	(	kIx(
<g/>
Petra	Petra	k1gFnSc1
Jungmanová	Jungmanová	k1gFnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Hess	Hess	k1gInSc1
a	a	k8xC
Otmar	Otmar	k1gMnSc1
Brancuzský	Brancuzský	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
žena	žena	k1gFnSc1
domovníka	domovník	k1gMnSc2
+	+	kIx~
hlas	hlas	k1gInSc1
(	(	kIx(
<g/>
Bohumila	Bohumila	k1gFnSc1
Dolejšová	Dolejšová	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
modlení	modlení	k1gNnSc1
+	+	kIx~
zpěv	zpěv	k1gInSc1
(	(	kIx(
<g/>
Michael	Michael	k1gMnSc1
Dushinsky	Dushinsky	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
byla	být	k5eAaImAgFnS
vybrána	vybrat	k5eAaPmNgFnS
do	do	k7c2
užšího	úzký	k2eAgInSc2d2
výběru	výběr	k1gInSc2
v	v	k7c6
soutěži	soutěž	k1gFnSc6
Prix	Prix	k1gInSc4
Bohemia	bohemia	k1gFnSc1
Radio	radio	k1gNnSc1
2006	#num#	k4
v	v	k7c6
kategorii	kategorie	k1gFnSc6
„	„	k?
<g/>
Rozhlasová	rozhlasový	k2eAgFnSc1d1
inscenace	inscenace	k1gFnSc1
pro	pro	k7c4
dospělého	dospělý	k2eAgMnSc4d1
posluchače	posluchač	k1gMnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2010	#num#	k4
Rudolf	Rudolfa	k1gFnPc2
Těsnohlídek	Těsnohlídka	k1gFnPc2
<g/>
:	:	kIx,
Dobrodružství	dobrodružství	k1gNnSc1
lišky	liška	k1gFnSc2
Bystroušky	Bystrouška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dramatizace	dramatizace	k1gFnSc1
Anna	Anna	k1gFnSc1
Jurásková	Jurásková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hudba	hudba	k1gFnSc1
Vlastimil	Vlastimil	k1gMnSc1
Redl	Redl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dramaturgie	dramaturgie	k1gFnSc1
Václava	Václava	k1gFnSc1
Ledvinková	Ledvinková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režie	režie	k1gFnSc1
Jaroslav	Jaroslav	k1gMnSc1
Kodeš	Kodeš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účinkují	účinkovat	k5eAaImIp3nP
<g/>
:	:	kIx,
Lucie	Lucie	k1gFnSc1
Pernetová	Pernetový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Vydra	Vydra	k1gMnSc1
<g/>
,	,	kIx,
Taťjana	Taťjana	k1gFnSc1
Medvecká	Medvecký	k2eAgFnSc1d1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Köhler	Köhler	k1gMnSc1
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
Zindulka	Zindulka	k1gFnSc1
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
Holub	Holub	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Maršál	maršál	k1gMnSc1
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
Eben	eben	k1gInSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Vlach	Vlach	k1gMnSc1
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
Smutná	Smutná	k1gFnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
Drbohlavová	Drbohlavový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Racek	racek	k1gMnSc1
<g/>
,	,	kIx,
Denisa	Denisa	k1gFnSc1
Nová	Nová	k1gFnSc1
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
Suchánková	Suchánková	k1gFnSc1
<g/>
,	,	kIx,
Nikola	Nikola	k1gFnSc1
Bartošová	Bartošová	k1gFnSc1
<g/>
,	,	kIx,
Dana	Dana	k1gFnSc1
Reichová	Reichová	k1gFnSc1
<g/>
,	,	kIx,
Valerie	Valerie	k1gFnSc1
Rosa	Rosa	k1gFnSc1
Hetzendorfová	Hetzendorfový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Justýna	Justýna	k1gFnSc1
Anna	Anna	k1gFnSc1
Šmuclerová	Šmuclerová	k1gFnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Tuček	Tuček	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
Fišer	Fišer	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
Tuček	Tuček	k1gMnSc1
a	a	k8xC
Dorota	Dorota	k1gFnSc1
Tučková	Tučková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Roland	Roland	k1gInSc1
Schimmelpfennig	Schimmelpfennig	k1gInSc1
<g/>
:	:	kIx,
Arabská	arabský	k2eAgFnSc1d1
noc	noc	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Thomas	Thomas	k1gMnSc1
Bernhard	Bernhard	k1gMnSc1
<g/>
:	:	kIx,
Prezident	prezident	k1gMnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://kramerius4.nkp.cz/search/i.jsp?pid=uuid:9e2dcd10-4b0c-11e4-aded-005056827e51&	http://kramerius4.nkp.cz/search/i.jsp?pid=uuid:9e2dcd10-4b0c-11e4-aded-005056827e51&	k1gMnSc1
<g/>
↑	↑	k?
http://www.ceskatelevize.cz/porady/1093836883-na-plovarne/20036816005-jiri-labus	http://www.ceskatelevize.cz/porady/1093836883-na-plovarne/20036816005-jiri-labus	k1gMnSc1
<g/>
↑	↑	k?
Historie	historie	k1gFnSc1
|	|	kIx~
Ypsilonka	Ypsilonka	k1gFnSc1
<g/>
.	.	kIx.
www.ypsilonka.cz	www.ypsilonka.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
-kim-	-kim-	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
prozradil	prozradit	k5eAaPmAgMnS
<g/>
,	,	kIx,
proč	proč	k6eAd1
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
neoženil	oženit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-02-04	2014-02-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1210	#num#	k4
<g/>
-	-	kIx~
<g/>
9940	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
DIGIONE	DIGIONE	kA
S.	S.	kA
<g/>
R.	R.	kA
<g/>
O.	O.	kA
-	-	kIx~
INDEPENDENT	independent	k1gMnSc1
DIGITAL	Digital	kA
AGENCY	AGENCY	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
-	-	kIx~
CENY	cena	k1gFnPc1
THÁLIE	Thálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
CENY	cena	k1gFnSc2
THÁLIE	Thálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
http://www.filmovaakademie.cz/cz/detail?creator=Ji%C5%99%C3%AD%20L%C3%A1bus	http://www.filmovaakademie.cz/cz/detail?creator=Ji%C5%99%C3%AD%20L%C3%A1bus	k1gMnSc1
<g/>
↑	↑	k?
CINEMAX	CINEMAX	kA
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
.	.	kIx.
cinemax	cinemax	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
↑	↑	k?
PEŇÁS	PEŇÁS	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
jsme	být	k5eAaImIp1nP
herci	herec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Respekt	respekt	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
10	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
49	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1801	#num#	k4
<g/>
-	-	kIx~
<g/>
1446	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
LÁBUS	LÁBUS	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mejla	Mejla	k1gMnSc1
Hlavsa	Hlavsa	k1gMnSc1
<g/>
:	:	kIx,
Interview	interview	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guerilla	guerilla	k1gFnSc1
Records	Recordsa	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FRANK	Frank	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plastic	Plastice	k1gFnPc2
People	People	k1gFnSc2
sa	sa	k?
správali	správat	k5eAaImAgMnP,k5eAaPmAgMnP
<g/>
,	,	kIx,
akože	akozat	k5eAaPmIp3nS
tí	tí	k?
boľševici	boľševice	k1gFnSc4
neexistujú	neexistujú	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korzár	korzár	k1gMnSc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ROWLING	ROWLING	kA
<g/>
,	,	kIx,
J.	J.	kA
K.	K.	kA
Harry	Harra	k1gMnSc2
Potter	Potter	k1gMnSc1
a	a	k8xC
kámen	kámen	k1gInSc1
mudrců	mudrc	k1gMnPc2
(	(	kIx(
<g/>
katalogový	katalogový	k2eAgInSc4d1
lístek	lístek	k1gInSc4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberec	Liberec	k1gInSc1
<g/>
:	:	kIx,
Krajská	krajský	k2eAgFnSc1d1
vědecká	vědecký	k2eAgFnSc1d1
knihhovna	knihhovna	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
<g/>
)	)	kIx)
↑	↑	k?
KOCÁBEK	kocábka	k1gFnPc2
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trutnoff	Trutnoff	k1gInSc1
2014	#num#	k4
<g/>
:	:	kIx,
bluesový	bluesový	k2eAgMnSc1d1
dědeček	dědeček	k1gMnSc1
a	a	k8xC
rocková	rockový	k2eAgFnSc1d1
babička	babička	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-08-7	2014-08-7	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1210	#num#	k4
<g/>
-	-	kIx~
<g/>
9940	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BEZR	BEZR	kA
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
OBRAZEM	obraz	k1gInSc7
<g/>
:	:	kIx,
Podívejte	podívat	k5eAaPmRp2nP,k5eAaImRp2nP
se	se	k3xPyFc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
bude	být	k5eAaImBp3nS
vypadat	vypadat	k5eAaImF,k5eAaPmF
Havel	Havel	k1gMnSc1
<g/>
,	,	kIx,
Hutka	Hutka	k1gMnSc1
<g/>
,	,	kIx,
Dubček	Dubček	k1gMnSc1
a	a	k8xC
Adamec	Adamec	k1gMnSc1
<g/>
.	.	kIx.
iDnes	iDnes	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-02-24	2014-02-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VOLNÝ	volný	k2eAgInSc1d1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naši	náš	k3xOp1gMnPc1
umělci	umělec	k1gMnPc1
a	a	k8xC
Anticharta	anticharta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Necenzurované	cenzurovaný	k2eNgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
1995	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
„	„	k?
<g/>
Ta	ten	k3xDgFnSc1
komedie	komedie	k1gFnSc1
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jací	jaký	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
lidé	člověk	k1gMnPc1
jsou	být	k5eAaImIp3nP
<g/>
,	,	kIx,
<g/>
“	“	k?
popisuje	popisovat	k5eAaImIp3nS
Lábus	Lábus	k1gInSc1
film	film	k1gInSc1
Vlastníci	vlastník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Natočili	natočit	k5eAaBmAgMnP
ho	on	k3xPp3gInSc4
během	během	k7c2
8	#num#	k4
dní	den	k1gInPc2
a	a	k8xC
v	v	k7c6
jedné	jeden	k4xCgFnSc6
místnosti	místnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radiožurnál	radiožurnál	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-11-18	2019-11-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TELEVIZE	televize	k1gFnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
srpnu	srpen	k1gInSc6
1989	#num#	k4
sílila	sílit	k5eAaImAgFnS
komunistická	komunistický	k2eAgFnSc1d1
propaganda	propaganda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chartisty	chartista	k1gMnPc7
spojovala	spojovat	k5eAaImAgFnS
se	s	k7c7
sérií	série	k1gFnSc7
požárů	požár	k1gInPc2
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ECKSTEIN	ECKSTEIN	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládní	vládní	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
k	k	k7c3
radaru	radar	k1gInSc3
<g/>
:	:	kIx,
Lábus	Lábus	k1gMnSc1
vylepil	vylepit	k5eAaPmAgMnS
plakát	plakát	k1gInSc4
na	na	k7c4
lavičku	lavička	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2007-11-02	2007-11-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1210	#num#	k4
<g/>
-	-	kIx~
<g/>
9940	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
"	"	kIx"
<g/>
Ať	ať	k9
žije	žít	k5eAaImIp3nS
monarchie	monarchie	k1gFnSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
křičel	křičet	k5eAaImAgMnS
průvod	průvod	k1gInSc4
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radio	radio	k1gNnSc1
Praha	Praha	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nepraktická	praktický	k2eNgFnSc1d1
výzva	výzva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babylon	Babylon	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-01-04	2018-01-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.divadlokalich.cz	www.divadlokalich.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Jiří	Jiří	k1gMnSc1
Robert	Robert	k1gMnSc1
Pick	Pick	k1gMnSc1
<g/>
:	:	kIx,
Anekdoty	anekdota	k1gFnSc2
Franci	Franci	k1gMnSc1
Roubíčka	Roubíček	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvojka	dvojka	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-01-09	2021-01-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tvůrčí	tvůrčí	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
Drama	drama	k1gFnSc1
a	a	k8xC
literatura	literatura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carey	Carey	k1gInPc1
Harrison	Harrison	k1gMnSc1
<g/>
:	:	kIx,
To	ten	k3xDgNnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
psychiatrovi	psychiatr	k1gMnSc3
stát	stát	k5eAaImF,k5eAaPmF
nemělo	mít	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvojka	dvojka	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2020-09-27	2020-09-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.bontonland.cz/babicka-drsnacka-david-williams/%5B%5D	http://www.bontonland.cz/babicka-drsnacka-david-williams/%5B%5D	k4
<g/>
↑	↑	k?
ONDRA@MACOSZEK.CZ	ONDRA@MACOSZEK.CZ	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
Macoszek	Macoszek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
–	–	k?
Draculův	Draculův	k2eAgMnSc1d1
švagr	švagr	k1gMnSc1
(	(	kIx(
<g/>
Miloslav	Miloslav	k1gMnSc1
Švandrlík	Švandrlík	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2	#num#	k4
CD	CD	kA
|	|	kIx~
Fonia	Fonia	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Friedrich	Friedrich	k1gMnSc1
Dürrenmatt	Dürrenmatt	k1gMnSc1
:	:	kIx,
Proces	proces	k1gInSc1
o	o	k7c4
oslí	oslí	k2eAgInSc4d1
stín	stín	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vltava	Vltava	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-03-05	2011-03-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jana	Jana	k1gFnSc1
Synková	Synková	k1gFnSc1
–	–	k?
Jan	Jan	k1gMnSc1
Schmid	Schmid	k1gInSc1
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Hynek	Hynek	k1gMnSc1
Mácha	Mácha	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vltava	Vltava	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-04-29	2014-04-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
A.S	A.S	k1gFnSc1
<g/>
,	,	kIx,
SUPRAPHON	supraphon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiráň	Jiráň	k1gFnSc1
<g/>
,	,	kIx,
Palla	palla	k1gFnSc1
<g/>
:	:	kIx,
Zmatky	zmatek	k1gInPc1
Hanuše	Hanuš	k1gMnSc2
Hanuše	Hanuš	k1gMnSc2
–	–	k?
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
<g/>
,	,	kIx,
Svatopluk	Svatopluk	k1gMnSc1
Beneš	Beneš	k1gMnSc1
–	–	k?
Supraphonline	Supraphonlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Panáček	Panáček	k1gMnSc1
v	v	k7c6
říši	říš	k1gFnSc6
mluveného	mluvený	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Malba	malba	k1gFnSc1
na	na	k7c6
dřevě	dřevo	k1gNnSc6
otevírá	otevírat	k5eAaImIp3nS
rozhlasovou	rozhlasový	k2eAgFnSc4d1
přehlídku	přehlídka	k1gFnSc4
Ingmara	Ingmar	k1gMnSc2
Bergmana	Bergman	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vltava	Vltava	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-11-06	2012-11-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hry	hra	k1gFnSc2
postupující	postupující	k2eAgFnSc1d1
do	do	k7c2
užšího	úzký	k2eAgInSc2d2
výběru	výběr	k1gInSc2
a	a	k8xC
soutěžící	soutěžící	k1gFnSc2
o	o	k7c4
Cenu	cena	k1gFnSc4
Prix	Prix	k1gInSc1
Bohemia	bohemia	k1gFnSc1
Radio	radio	k1gNnSc1
2006	#num#	k4
v	v	k7c6
kategorii	kategorie	k1gFnSc6
"	"	kIx"
<g/>
Rozhlasová	rozhlasový	k2eAgFnSc1d1
inscenace	inscenace	k1gFnSc1
pro	pro	k7c4
dospělého	dospělý	k2eAgMnSc4d1
posluchače	posluchač	k1gMnSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rozhlasová	rozhlasový	k2eAgFnSc1d1
premiéra	premiéra	k1gFnSc1
Arabské	arabský	k2eAgFnSc2d1
noci	noc	k1gFnSc2
Rolanda	Rolanda	k1gFnSc1
Schimmmelpfenniga	Schimmmelpfenniga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vltava	Vltava	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-11-12	2011-11-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
CZECH	CZECH	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaiser	Kaiser	k1gMnSc1
<g/>
,	,	kIx,
Lábus	Lábus	k1gMnSc1
a	a	k8xC
rodinka	rodinka	k1gFnSc1
Tlučhořových	Tlučhořová	k1gFnPc2
:	:	kIx,
o	o	k7c6
kultovním	kultovní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
a	a	k8xC
mistrech	mistr	k1gMnPc6
improvizace	improvizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Pražská	pražský	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
254	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86102	#num#	k4
<g/>
-	-	kIx~
<g/>
42	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
CZECH	CZECH	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
Lábus	Lábus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
vyprávění	vyprávění	k1gNnSc2
zaznamenal	zaznamenat	k5eAaPmAgMnS
a	a	k8xC
otázky	otázka	k1gFnPc4
kladl	klást	k5eAaImAgMnS
Jan	Jan	k1gMnSc1
Czech	Czech	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Venkow	Venkow	k1gMnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
112	#num#	k4
stran	strana	k1gFnPc2
</s>
<s>
FIKEJZ	FIKEJZ	kA
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
film	film	k1gInSc1
:	:	kIx,
herci	herec	k1gMnPc1
a	a	k8xC
herečky	herečka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
.	.	kIx.
díl	díl	k1gInSc1
:	:	kIx,
L	L	kA
<g/>
–	–	k?
<g/>
Ř.	Ř.	kA
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
656	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
471	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
15	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
:	:	kIx,
91	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
:	:	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
federální	federální	k2eAgInPc4d1
orgány	orgán	k1gInPc4
ČSFR	ČSFR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
1	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
–	–	k?
<g/>
M.	M.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
je	být	k5eAaImIp3nS
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
636	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901103	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
526	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
je	být	k5eAaImIp3nS
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
=	=	kIx~
Who	Who	k1gMnPc1
is	is	k?
who	who	k?
:	:	kIx,
osobnosti	osobnost	k1gFnPc4
české	český	k2eAgFnSc2d1
současnosti	současnost	k1gFnSc2
:	:	kIx,
5000	#num#	k4
životopisů	životopis	k1gInPc2
/	/	kIx~
(	(	kIx(
<g/>
Michael	Michael	k1gMnSc1
Třeštík	Třeštík	k1gMnSc1
editor	editor	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Agentura	agentura	k1gFnSc1
Kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
je	být	k5eAaImIp3nS
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
775	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902586	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
363	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MALÉŘOVÁ	MALÉŘOVÁ	kA
<g/>
,	,	kIx,
Zuzana	Zuzana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psáno	psát	k5eAaImNgNnS
v	v	k7c4
kóji	kóje	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Venkow	Venkow	k1gMnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
70	#num#	k4
s.	s.	k?
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
–	–	k?
Česko	Česko	k1gNnSc1
:	:	kIx,
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ottovo	Ottův	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
823	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7360	#num#	k4
<g/>
-	-	kIx~
<g/>
796	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
394	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TOMEŠ	Tomeš	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
XX	XX	kA
<g/>
.	.	kIx.
století	století	k1gNnPc2
:	:	kIx,
II	II	kA
<g/>
.	.	kIx.
díl	díl	k1gInSc1
:	:	kIx,
K	k	k7c3
<g/>
–	–	k?
<g/>
P.	P.	kA
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
;	;	kIx,
Petr	Petr	k1gMnSc1
Meissner	Meissner	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
649	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
246	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
242	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jiří	Jiří	k1gMnPc1
Lábus	Lábus	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
životopis	životopis	k1gInSc4
na	na	k7c6
kaiser-labus	kaiser-labus	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
na	na	k7c6
Kinoboxu	Kinobox	k1gInSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
v	v	k7c6
Divadelní	divadelní	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
na	na	k7c4
Dabingforum	Dabingforum	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
(	(	kIx(
<g/>
rozhovor	rozhovor	k1gInSc1
z	z	k7c2
cyklu	cyklus	k1gInSc2
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
„	„	k?
<g/>
Na	na	k7c6
plovárně	plovárna	k1gFnSc6
<g/>
“	“	k?
<g/>
)	)	kIx)
–	–	k?
video	video	k1gNnSc1
on-line	on-lin	k1gInSc5
v	v	k7c6
archivu	archiv	k1gInSc6
ČT	ČT	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Český	český	k2eAgInSc1d1
lev	lev	k1gInSc1
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
mužský	mužský	k2eAgInSc4d1
herecký	herecký	k2eAgInSc4d1
výkon	výkon	k1gInSc4
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
</s>
<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Marián	Marián	k1gMnSc1
Labuda	Labuda	k1gMnSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Andrej	Andrej	k1gMnSc1
Chalimon	Chalimon	k1gMnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jiří	Jiří	k1gMnSc1
Kodet	Kodet	k1gMnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Donutil	donutit	k5eAaPmAgMnS
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jiří	Jiří	k1gMnSc1
Bartoška	Bartoška	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jiří	Jiří	k1gMnSc1
Macháček	Macháček	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Zindulka	Zindulka	k1gFnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Ivan	Ivan	k1gMnSc1
Trojan	Trojan	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Ivan	Ivan	k1gMnSc1
Trojan	Trojan	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jan	Jan	k1gMnSc1
Budař	Budař	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Krobot	Krobot	k?
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Martin	Martin	k1gMnSc1
Huba	huba	k1gFnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jan	Jan	k1gMnSc1
Budař	Budař	k1gMnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Dlouhý	Dlouhý	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Chudík	Chudík	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Dlouhý	Dlouhý	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Hynek	Hynek	k1gMnSc1
Čermák	Čermák	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Vetchý	vetchý	k2eAgMnSc1d1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Plesl	Plesl	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Kryštof	Kryštof	k1gMnSc1
Hádek	hádek	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Kaiser	Kaiser	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Kaiser	Kaiser	k1gMnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jan	Jan	k1gMnSc1
František	František	k1gMnSc1
Uher	Uher	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Mrkvička	mrkvička	k1gFnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jiří	Jiří	k1gMnSc1
Mádl	Mádl	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Simpsonovi	Simpson	k1gMnSc3
Simpsonovi	Simpson	k1gMnSc3
(	(	kIx(
<g/>
franšíza	franšíza	k1gFnSc1
<g/>
)	)	kIx)
Rodina	rodina	k1gFnSc1
</s>
<s>
Homer	Homer	k1gMnSc1
Simpson	Simpson	k1gMnSc1
•	•	k?
Marge	Marge	k1gInSc1
Simpsonová	Simpsonová	k1gFnSc1
•	•	k?
Bart	Bart	k1gMnSc1
Simpson	Simpson	k1gMnSc1
•	•	k?
Líza	Líza	k1gFnSc1
Simpsonová	Simpsonový	k2eAgFnSc1d1
•	•	k?
Maggie	Maggie	k1gFnSc1
Simpsonová	Simpsonová	k1gFnSc1
Řady	řada	k1gFnSc2
</s>
<s>
1	#num#	k4
•	•	k?
2	#num#	k4
•	•	k?
3	#num#	k4
•	•	k?
4	#num#	k4
•	•	k?
5	#num#	k4
•	•	k?
6	#num#	k4
•	•	k?
7	#num#	k4
•	•	k?
8	#num#	k4
•	•	k?
9	#num#	k4
•	•	k?
10	#num#	k4
•	•	k?
11	#num#	k4
•	•	k?
12	#num#	k4
•	•	k?
13	#num#	k4
•	•	k?
14	#num#	k4
•	•	k?
15	#num#	k4
•	•	k?
16	#num#	k4
•	•	k?
17	#num#	k4
•	•	k?
18	#num#	k4
•	•	k?
19	#num#	k4
•	•	k?
20	#num#	k4
•	•	k?
21	#num#	k4
•	•	k?
22	#num#	k4
•	•	k?
23	#num#	k4
•	•	k?
24	#num#	k4
•	•	k?
25	#num#	k4
•	•	k?
26	#num#	k4
•	•	k?
27	#num#	k4
•	•	k?
28	#num#	k4
•	•	k?
29	#num#	k4
•	•	k?
30	#num#	k4
•	•	k?
31	#num#	k4
•	•	k?
32	#num#	k4
Postavy	postava	k1gFnSc2
</s>
<s>
Abraham	Abraham	k1gMnSc1
Simpson	Simpsona	k1gFnPc2
•	•	k?
Agnes	Agnesa	k1gFnPc2
Skinnerová	Skinnerový	k2eAgFnSc1d1
•	•	k?
Apu	Apu	k1gFnSc1
Nahasapímapetilon	Nahasapímapetilon	k1gInSc1
•	•	k?
Barney	Barnea	k1gFnSc2
Gumble	Gumble	k1gMnSc1
•	•	k?
Carl	Carl	k1gMnSc1
Carlson	Carlson	k1gMnSc1
•	•	k?
Cecil	Cecil	k1gMnSc1
Terwilliger	Terwilliger	k1gMnSc1
•	•	k?
Clancy	Clancy	k1gInPc1
Wiggum	Wiggum	k1gInSc1
•	•	k?
Cletus	Cletus	k1gInSc1
Spuckler	Spuckler	k1gMnSc1
•	•	k?
Čmeláčí	čmeláčí	k2eAgMnSc1d1
muž	muž	k1gMnSc1
•	•	k?
Disco	disco	k1gNnPc2
Stu	sto	k4xCgNnSc3
•	•	k?
Duffman	Duffman	k1gMnSc1
•	•	k?
Edna	Edna	k1gFnSc1
Krabappelová	Krabappelová	k1gFnSc1
•	•	k?
Gary	Gara	k1gFnSc2
Chalmers	Chalmersa	k1gFnPc2
•	•	k?
Gil	Gil	k1gMnSc1
Gunderson	Gunderson	k1gMnSc1
•	•	k?
Gino	Gino	k1gMnSc1
•	•	k?
Haďák	Haďák	k1gMnSc1
•	•	k?
Hans	Hans	k1gMnSc1
Krtkovic	Krtkovice	k1gFnPc2
•	•	k?
Helena	Helena	k1gFnSc1
Lovejoyová	Lovejoyová	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Herbert	Herbert	k1gMnSc1
Powell	Powell	k1gMnSc1
•	•	k?
Herman	Herman	k1gMnSc1
•	•	k?
Itchy	Itcha	k1gFnSc2
•	•	k?
Jebediáš	Jebediáš	k1gMnSc1
Springfield	Springfield	k1gMnSc1
•	•	k?
Joe	Joe	k1gMnSc1
Quimby	Quimba	k1gFnSc2
•	•	k?
Julius	Julius	k1gMnSc1
Dlaha	dlaha	k1gFnSc1
•	•	k?
Kang	Kang	k1gMnSc1
•	•	k?
Kent	Kent	k1gMnSc1
Brockman	Brockman	k1gMnSc1
•	•	k?
Kirk	Kirk	k1gInSc1
Van	van	k1gInSc1
Houten	Houten	k2eAgInSc1d1
•	•	k?
Kodos	Kodos	k1gInSc1
•	•	k?
Komiksák	Komiksák	k1gInSc1
•	•	k?
Lenny	Lenny	k?
Leonard	Leonard	k1gMnSc1
•	•	k?
Levák	levák	k1gMnSc1
Bob	Bob	k1gMnSc1
•	•	k?
Lionel	Lionel	k1gMnSc1
Hutz	Hutz	k1gMnSc1
•	•	k?
Martin	Martin	k1gMnSc1
Prince	princ	k1gMnSc2
•	•	k?
Maude	Maud	k1gInSc5
Flandersová	Flandersový	k2eAgFnSc1d1
•	•	k?
Milhouse	Milhouse	k1gFnSc1
Van	vana	k1gFnPc2
Houten	Houten	k2eAgInSc1d1
•	•	k?
Mona	Mona	k1gFnSc1
Simpsonová	Simpsonová	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Montgomery	Montgomer	k1gInPc1
Burns	Burns	k1gInSc1
•	•	k?
Ned	Ned	k1gFnSc2
Flanders	Flandersa	k1gFnPc2
•	•	k?
Nelson	Nelson	k1gMnSc1
Muntz	Muntz	k1gMnSc1
•	•	k?
Nick	Nick	k1gMnSc1
Riviera	Riviero	k1gNnSc2
•	•	k?
Otto	Otto	k1gMnSc1
Mann	Mann	k1gMnSc1
•	•	k?
Patty	Patta	k1gFnSc2
Bouvierová	Bouvierová	k1gFnSc1
•	•	k?
Poochie	Poochie	k1gFnSc2
•	•	k?
Rainier	Rainier	k1gMnSc1
Wolfcastle	Wolfcastle	k1gMnSc1
•	•	k?
Ralph	Ralph	k1gMnSc1
Wiggum	Wiggum	k1gNnSc1
•	•	k?
Reverend	reverend	k1gMnSc1
Timothy	Timotha	k1gMnSc2
Lovejoy	Lovejoa	k1gFnSc2
•	•	k?
Rod	rod	k1gInSc1
Flanders	Flanders	k1gInSc1
•	•	k?
Scratchy	Scratcha	k1gFnSc2
•	•	k?
Selma	Selma	k1gFnSc1
Bouvierová	Bouvierová	k1gFnSc1
•	•	k?
Seymour	Seymour	k1gMnSc1
Skinner	Skinner	k1gMnSc1
•	•	k?
Sněhulka	Sněhulka	k1gFnSc1
•	•	k?
Spasitel	spasitel	k1gMnSc1
•	•	k?
Šáša	Šáša	k1gMnSc1
Krusty	krusta	k1gFnSc2
•	•	k?
Školník	školník	k1gMnSc1
Willie	Willie	k1gFnSc2
•	•	k?
Todd	Todd	k1gInSc1
Flanders	Flanders	k1gInSc1
•	•	k?
Tlustý	tlustý	k2eAgMnSc1d1
Tony	Tony	k1gMnSc1
•	•	k?
Troy	Troa	k1gFnSc2
McClure	McClur	k1gMnSc5
•	•	k?
Vočko	Vočko	k1gNnSc1
Szyslak	Szyslak	k1gMnSc1
•	•	k?
Waylon	Waylon	k1gInSc1
Smithers	Smithers	k1gInSc1
•	•	k?
a	a	k8xC
další	další	k2eAgNnPc4d1
<g/>
…	…	k?
Místa	místo	k1gNnPc4
</s>
<s>
Springfield	Springfield	k1gInSc1
•	•	k?
Shelbyville	Shelbyville	k1gInSc1
•	•	k?
Dům	dům	k1gInSc1
Simpsonových	Simpsonová	k1gFnPc2
•	•	k?
a	a	k8xC
další	další	k2eAgFnSc1d1
<g/>
…	…	k?
Výroba	výroba	k1gFnSc1
</s>
<s>
Původní	původní	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
</s>
<s>
Danny	Danna	k1gFnPc1
Elfman	Elfman	k1gMnSc1
•	•	k?
David	David	k1gMnSc1
Silverman	Silverman	k1gMnSc1
•	•	k?
Matt	Matt	k1gInSc1
Groening	Groening	k1gInSc1
•	•	k?
James	James	k1gInSc1
L.	L.	kA
Brooks	Brooks	k1gInSc1
•	•	k?
Sam	Sam	k1gMnSc1
Simon	Simon	k1gMnSc1
•	•	k?
Al	ala	k1gFnPc2
Jean	Jean	k1gMnSc1
Česká	český	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
•	•	k?
Vojtěch	Vojtěch	k1gMnSc1
Kostiha	Kostiha	k1gMnSc1
Původní	původní	k2eAgInSc1d1
dabing	dabing	k1gInSc1
</s>
<s>
Dan	Dan	k1gMnSc1
Castellaneta	Castellaneto	k1gNnSc2
•	•	k?
Hank	Hank	k1gMnSc1
Azaria	Azarium	k1gNnSc2
•	•	k?
Harry	Harr	k1gInPc1
Shearer	Shearer	k1gInSc1
•	•	k?
Julie	Julie	k1gFnSc1
Kavnerová	Kavnerový	k2eAgFnSc1d1
•	•	k?
Nancy	Nancy	k1gFnSc1
Cartwrightová	Cartwrightová	k1gFnSc1
•	•	k?
Yeardley	Yeardley	k1gInPc1
Smithová	Smithová	k1gFnSc1
Český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc4
</s>
<s>
Helena	Helena	k1gFnSc1
Štáchová	Štáchový	k2eAgFnSc1d1
•	•	k?
Ivana	Ivana	k1gFnSc1
Korolová	Korolový	k2eAgFnSc1d1
•	•	k?
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Carda	Carda	k1gMnSc1
•	•	k?
Martin	Martin	k1gMnSc1
Dejdar	Dejdar	k1gMnSc1
•	•	k?
Vlastimil	Vlastimil	k1gMnSc1
Bedrna	Bedrno	k1gNnSc2
•	•	k?
Vlastimil	Vlastimil	k1gMnSc1
Zavřel	Zavřel	k1gMnSc1
</s>
<s>
Filmy	film	k1gInPc1
</s>
<s>
Simpsonovi	Simpsonův	k2eAgMnPc1d1
ve	v	k7c6
filmu	film	k1gInSc6
•	•	k?
Simpsonovi	Simpsonovi	k1gRnPc1
<g/>
:	:	kIx,
Maggie	Maggie	k1gFnSc1
zasahuje	zasahovat	k5eAaImIp3nS
•	•	k?
Playdate	Playdat	k1gInSc5
with	with	k1gInSc4
Destiny	Destin	k2eAgFnSc2d1
Hry	hra	k1gFnSc2
</s>
<s>
Bart	Bart	k1gInSc1
vs	vs	k?
<g/>
.	.	kIx.
the	the	k?
Space	Spaec	k1gInSc2
Mutants	Mutantsa	k1gFnPc2
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc1
Simpsons	Simpsons	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Arcade	Arcad	k1gInSc5
Game	game	k1gInSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Virtual	Virtual	k1gMnSc1
Springfield	Springfield	k1gMnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Night	Night	k1gInSc1
of	of	k?
the	the	k?
Living	Living	k1gInSc1
Treehouse	Treehouse	k1gFnSc1
of	of	k?
Horror	horror	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Skateboarding	skateboarding	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hit	hit	k1gInSc1
&	&	k?
Run	run	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc2
Simpsons	Simpsonsa	k1gFnPc2
Game	game	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tapped	Tapped	k1gMnSc1
Out	Out	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
The	The	k?
Tracey	Tracea	k1gFnSc2
Ullman	Ullman	k1gMnSc1
Show	show	k1gFnSc1
(	(	kIx(
<g/>
skeče	skeč	k1gInPc1
Simpsonových	Simpsonová	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
arabská	arabský	k2eAgFnSc1d1
verze	verze	k1gFnSc1
seriálu	seriál	k1gInSc2
•	•	k?
hudební	hudební	k2eAgFnSc1d1
znělka	znělka	k1gFnSc1
•	•	k?
úvodní	úvodní	k2eAgFnSc1d1
znělka	znělka	k1gFnSc1
•	•	k?
Duff	Duff	k1gInSc1
•	•	k?
D	D	kA
<g/>
'	'	kIx"
<g/>
oh	oh	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
•	•	k?
¡	¡	k?
<g/>
Ay	Ay	k1gMnSc2
<g/>
,	,	kIx,
caramba	caramba	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
Seznamy	seznam	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
dílů	díl	k1gInPc2
seriálu	seriál	k1gInSc2
Simpsonovi	Simpson	k1gMnSc3
•	•	k?
Seznam	seznam	k1gInSc1
míst	místo	k1gNnPc2
v	v	k7c6
seriálu	seriál	k1gInSc6
Simpsonovi	Simpson	k1gMnSc3
•	•	k?
Seznam	seznam	k1gInSc4
postav	postava	k1gFnPc2
seriálu	seriál	k1gInSc2
Simpsonovi	Simpson	k1gMnSc3
•	•	k?
Seznam	seznam	k1gInSc4
vedlejších	vedlejší	k2eAgFnPc2d1
postav	postava	k1gFnPc2
seriálu	seriál	k1gInSc2
Simpsonovi	Simpson	k1gMnSc3
•	•	k?
Seznam	seznam	k1gInSc4
zvířat	zvíře	k1gNnPc2
seriálu	seriál	k1gInSc2
Simpsonovi	Simpson	k1gMnSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990209472	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1024801276	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0920	#num#	k4
0840	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nb	nb	k?
<g/>
99165821	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
83998909	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nb	lccn-nb	k1gInSc1
<g/>
99165821	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
|	|	kIx~
Televize	televize	k1gFnSc1
</s>
