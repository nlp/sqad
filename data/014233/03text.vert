<s>
Mečoun	mečoun	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
</s>
<s>
Mečoun	mečoun	k1gMnSc1
obecný	obecný	k2eAgInSc1d1
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
paprskoploutví	paprskoploutvit	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
Actinopterygii	Actinopterygie	k1gFnSc3
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
ostnoploutví	ostnoploutví	k1gMnPc1
(	(	kIx(
<g/>
Perciformes	Perciformes	k1gMnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
mečounovití	mečounovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Xiphiidae	Xiphiidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
mečoun	mečoun	k1gMnSc1
(	(	kIx(
<g/>
Xiphias	Xiphias	k1gMnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Xiphias	Xiphias	k1gMnSc1
gladiusLinné	gladiusLinný	k2eAgFnSc2d1
<g/>
,	,	kIx,
1758	#num#	k4
Synonyma	synonymum	k1gNnSc2
</s>
<s>
Phaethonichthys	Phaethonichthys	k1gInSc1
tuberculatus	tuberculatus	k1gInSc1
(	(	kIx(
<g/>
Nichols	Nichols	k1gInSc1
<g/>
,	,	kIx,
1923	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tetrapterus	Tetrapterus	k1gMnSc1
imperator	imperator	k1gMnSc1
(	(	kIx(
<g/>
Bloch	Bloch	k1gMnSc1
&	&	k?
Schneider	Schneider	k1gMnSc1
<g/>
,	,	kIx,
1801	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Xiphasia	Xiphasia	k1gFnSc1
gladius	gladius	k1gInSc1
(	(	kIx(
<g/>
Linné	Linné	k1gNnSc1
<g/>
,	,	kIx,
1758	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Xiphias	Xiphias	k1gInSc1
estara	estara	k1gFnSc1
(	(	kIx(
<g/>
Phillipps	Phillipps	k1gInSc1
<g/>
,	,	kIx,
1932	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Xiphias	Xiphias	k1gInSc1
gladius	gladius	k1gInSc1
estara	estara	k1gFnSc1
(	(	kIx(
<g/>
Phillipps	Phillipps	k1gInSc1
<g/>
,	,	kIx,
1932	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Xiphias	Xiphias	k1gMnSc1
imperator	imperator	k1gMnSc1
(	(	kIx(
<g/>
Bloch	Bloch	k1gMnSc1
&	&	k?
Schneider	Schneider	k1gMnSc1
<g/>
,	,	kIx,
1801	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Xiphias	Xiphias	k1gInSc1
rondeletti	rondeletť	k1gFnSc2
(	(	kIx(
<g/>
Leach	Leach	k1gMnSc1
<g/>
,	,	kIx,
1818	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Xiphias	Xiphias	k1gMnSc1
thermaicus	thermaicus	k1gMnSc1
(	(	kIx(
<g/>
Serbetis	Serbetis	k1gInSc1
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mečoun	mečoun	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Xiphias	Xiphias	k1gMnSc1
gladius	gladius	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jediný	jediný	k2eAgInSc1d1
druh	druh	k1gInSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
mečounovitých	mečounovitý	k2eAgInPc2d1
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nP
tak	tak	k6eAd1
celou	celý	k2eAgFnSc4d1
tuto	tento	k3xDgFnSc4
čeleď	čeleď	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
až	až	k9
455	#num#	k4
cm	cm	kA
dlouhá	dlouhý	k2eAgFnSc1d1
a	a	k8xC
téměř	téměř	k6eAd1
650	#num#	k4
<g/>
kilogramová	kilogramový	k2eAgFnSc1d1
ryba	ryba	k1gFnSc1
žijící	žijící	k2eAgFnSc1d1
v	v	k7c6
oceánech	oceán	k1gInPc6
do	do	k7c2
hloubky	hloubka	k1gFnSc2
až	až	k9
800	#num#	k4
m.	m.	k?
Často	často	k6eAd1
vyskakuje	vyskakovat	k5eAaImIp3nS
nad	nad	k7c4
hladinu	hladina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plave	plavat	k5eAaImIp3nS
rychlostí	rychlost	k1gFnSc7
až	až	k9
100	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Fyziologie	fyziologie	k1gFnSc1
</s>
<s>
Tělo	tělo	k1gNnSc1
je	být	k5eAaImIp3nS
drsné	drsný	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nemá	mít	k5eNaImIp3nS
šupiny	šupina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
horní	horní	k2eAgFnSc1d1
čelist	čelist	k1gFnSc1
je	být	k5eAaImIp3nS
protáhlá	protáhlý	k2eAgFnSc1d1
v	v	k7c4
typický	typický	k2eAgInSc4d1
mečovitý	mečovitý	k2eAgInSc4d1
výběžek	výběžek	k1gInSc4
dlouhý	dlouhý	k2eAgInSc4d1
asi	asi	k9
1	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hřbetní	hřbetní	k2eAgFnPc4d1
ploutve	ploutev	k1gFnPc4
má	mít	k5eAaImIp3nS
umístěné	umístěný	k2eAgNnSc1d1
daleko	daleko	k6eAd1
od	od	k7c2
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řitní	řitní	k2eAgFnPc1d1
ploutve	ploutev	k1gFnPc1
jsou	být	k5eAaImIp3nP
2	#num#	k4
a	a	k8xC
břišní	břišní	k2eAgMnSc1d1
nemá	mít	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Obživa	obživa	k1gFnSc1
</s>
<s>
Loví	lovit	k5eAaImIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
vnikne	vniknout	k5eAaPmIp3nS
do	do	k7c2
hejna	hejno	k1gNnSc2
sardinek	sardinka	k1gFnPc2
nebo	nebo	k8xC
sleďů	sleď	k1gMnPc2
<g/>
,	,	kIx,
začne	začít	k5eAaPmIp3nS
tlouct	tlouct	k5eAaImF
kolem	kolem	k6eAd1
sebe	sebe	k3xPyFc4
mečem	meč	k1gInSc7
a	a	k8xC
sežere	sežrat	k5eAaPmIp3nS
mrtvé	mrtvý	k2eAgFnPc4d1
nebo	nebo	k8xC
zraněné	zraněný	k2eAgFnPc4d1
ryby	ryba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Množení	množení	k1gNnSc1
</s>
<s>
Mečoun	mečoun	k1gMnSc1
se	se	k3xPyFc4
tře	třít	k5eAaImIp3nS
v	v	k7c6
létě	léto	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Plůdek	plůdek	k1gInSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
čelistech	čelist	k1gFnPc6
drobné	drobný	k2eAgInPc4d1
ostré	ostrý	k2eAgInPc4d1
zuby	zub	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jim	on	k3xPp3gMnPc3
vypadají	vypadat	k5eAaImIp3nP,k5eAaPmIp3nP
<g/>
,	,	kIx,
než	než	k8xS
dospějí	dochvít	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s>
Hospodářský	hospodářský	k2eAgInSc1d1
význam	význam	k1gInSc1
</s>
<s>
Mečoun	mečoun	k1gMnSc1
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
loven	loven	k2eAgInSc1d1
pro	pro	k7c4
chutné	chutný	k2eAgNnSc4d1
maso	maso	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
málo	málo	k6eAd1
dotčený	dotčený	k2eAgInSc4d1
druh	druh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
ZICHA	Zich	k1gMnSc4
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xiphias	Xiphias	k1gMnSc1
gladius	gladius	k1gMnSc1
(	(	kIx(
<g/>
mečoun	mečoun	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
4.10	4.10	k4
<g/>
.2005	.2005	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8	#num#	k4
CET	ceta	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
piloun	piloun	k1gMnSc1
mnohozubý	mnohozubý	k2eAgMnSc1d1
</s>
<s>
plachetník	plachetník	k1gMnSc1
</s>
<s>
marlín	marlín	k1gInSc1
modrý	modrý	k2eAgInSc1d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
mečoun	mečoun	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
mečoun	mečoun	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Xiphias	Xiphias	k1gInSc1
gladius	gladius	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Xiphias	Xiphias	k1gMnSc1
gladius	gladius	k1gMnSc1
(	(	kIx(
<g/>
mečoun	mečoun	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Xiphias	Xiphias	k1gMnSc1
gladius	gladius	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Rainer	Rainra	k1gFnPc2
Froese	Froese	k1gFnSc2
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Pauly	Paula	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
FishBase	FishBas	k1gInSc6
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
