<s>
Tanalyk	Tanalyk	k6eAd1
</s>
<s>
Tanalyk	Tanalyk	k6eAd1
Most	most	k1gInSc1
přes	přes	k7c4
řeku	řeka	k1gFnSc4
v	v	k7c6
BajmakuZákladní	BajmakuZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
225	#num#	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
4160	#num#	k4
km²	km²	k?
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
</s>
<s>
2,96	2,96	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Pramen	pramen	k1gInSc1
</s>
<s>
Irendyk	Irendyk	k1gInSc1
52	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
<g/>
33,8	33,8	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
58	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
<g/>
12,72	12,72	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
do	do	k7c2
Iriklinské	Iriklinský	k2eAgFnSc2d1
přehrady	přehrada	k1gFnSc2
51	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
<g/>
35,23	35,23	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
58	#num#	k4
<g/>
°	°	k?
<g/>
44	#num#	k4
<g/>
′	′	k?
<g/>
2,53	2,53	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
Baškortostán	Baškortostán	k1gInSc1
<g/>
,	,	kIx,
Orenburská	orenburský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Úmoří	úmoří	k1gNnSc1
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc1
</s>
<s>
bezodtoká	bezodtoký	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
Kaspické	kaspický	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Ural	Ural	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tanalyk	Tanalyk	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
Т	Т	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
v	v	k7c6
Baškortostánu	Baškortostán	k1gInSc6
a	a	k8xC
v	v	k7c6
Orenburské	orenburský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
(	(	kIx(
<g/>
dolní	dolní	k2eAgInSc1d1
tok	tok	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
225	#num#	k4
km	km	kA
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povodí	povodí	k1gNnSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
4160	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
toku	tok	k1gInSc2
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS
na	na	k7c6
hřbetu	hřbet	k1gInSc6
Irendyk	Irendyka	k1gFnPc2
v	v	k7c6
Jižním	jižní	k2eAgInSc6d1
Uralu	Ural	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústí	ústí	k1gNnPc1
do	do	k7c2
Iriklinské	Iriklinský	k2eAgFnSc2d1
přehrady	přehrada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
pravý	pravý	k2eAgInSc1d1
přítok	přítok	k1gInSc1
Uralu	Ural	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
vody	voda	k1gFnSc2
je	být	k5eAaImIp3nS
převážně	převážně	k6eAd1
sněhový	sněhový	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
roční	roční	k2eAgInSc1d1
průtok	průtok	k1gInSc1
vody	voda	k1gFnSc2
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
59	#num#	k4
km	km	kA
od	od	k7c2
ústí	ústí	k1gNnSc2
činí	činit	k5eAaImIp3nS
2,96	2,96	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Vysychá	vysychat	k5eAaImIp3nS
na	na	k7c6
konci	konec	k1gInSc6
léta	léto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zamrzá	zamrzat	k5eAaImIp3nS
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
října	říjen	k1gInSc2
až	až	k6eAd1
v	v	k7c6
listopadu	listopad	k1gInSc6
a	a	k8xC
rozmrzá	rozmrzat	k5eAaImIp3nS
v	v	k7c6
dubnu	duben	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Na	na	k7c6
řece	řeka	k1gFnSc6
leží	ležet	k5eAaImIp3nS
město	město	k1gNnSc1
Bajmak	Bajmak	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
Т	Т	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
