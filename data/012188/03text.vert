<p>
<s>
Péter	Péter	k1gMnSc1	Péter
Marót	Marót	k1gMnSc1	Marót
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1935	[number]	k4	1935
Ostřihom	Ostřihom	k1gInSc1	Ostřihom
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
maďarský	maďarský	k2eAgMnSc1d1	maďarský
sportovní	sportovní	k2eAgMnSc1d1	sportovní
šermíř	šermíř	k1gMnSc1	šermíř
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
specializoval	specializovat	k5eAaBmAgMnS	specializovat
na	na	k7c4	na
šerm	šerm	k1gInSc4	šerm
šavlí	šavle	k1gFnPc2	šavle
<g/>
.	.	kIx.	.
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
a	a	k8xC	a
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
startoval	startovat	k5eAaBmAgInS	startovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
a	a	k8xC	a
1972	[number]	k4	1972
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
a	a	k8xC	a
družstev	družstvo	k1gNnPc2	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
získal	získat	k5eAaPmAgInS	získat
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1964	[number]	k4	1964
zlatou	zlatý	k2eAgFnSc4d1	zlatá
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
a	a	k8xC	a
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1968	[number]	k4	1968
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
maďarským	maďarský	k2eAgNnSc7d1	Maďarské
družstvem	družstvo	k1gNnSc7	družstvo
šavlistů	šavlista	k1gMnPc2	šavlista
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1968	[number]	k4	1968
a	a	k8xC	a
1972	[number]	k4	1972
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
získal	získat	k5eAaPmAgInS	získat
s	s	k7c7	s
družstvem	družstvo	k1gNnSc7	družstvo
titul	titul	k1gInSc1	titul
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Pézsa	Pézs	k1gMnSc2	Pézs
Tibor	Tibor	k1gMnSc1	Tibor
na	na	k7c6	na
maďarské	maďarský	k2eAgFnSc6d1	maďarská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
