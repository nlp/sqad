<s>
Souborné	souborný	k2eAgNnSc1d1	souborné
Koperníkovo	Koperníkův	k2eAgNnSc1d1	Koperníkovo
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
rozvržené	rozvržený	k2eAgNnSc1d1	rozvržené
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
svazků	svazek	k1gInPc2	svazek
vydává	vydávat	k5eAaImIp3nS	vydávat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Nicolaus	Nicolaus	k1gMnSc1	Nicolaus
Copernicus	Copernicus	k1gMnSc1	Copernicus
Gesamtausgabe	Gesamtausgab	k1gInSc5	Gesamtausgab
bavorské	bavorský	k2eAgFnPc1d1	bavorská
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Akademie	akademie	k1gFnSc2	akademie
Verlag	Verlaga	k1gFnPc2	Verlaga
<g/>
.	.	kIx.	.
</s>
