<s>
Byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1985	[number]	k4	1985
až	až	k9	až
1992	[number]	k4	1992
Inženýrskými	inženýrský	k2eAgFnPc7d1	inženýrská
a	a	k8xC	a
průmyslovými	průmyslový	k2eAgFnPc7d1	průmyslová
stavbami	stavba	k1gFnPc7	stavba
Ostrava	Ostrava	k1gFnSc1	Ostrava
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
Václava	Václav	k1gMnSc2	Václav
Aulického	Aulický	k2eAgMnSc2d1	Aulický
<g/>
,	,	kIx,	,
statika	statik	k1gMnSc2	statik
Jiřího	Jiří	k1gMnSc2	Jiří
Kozáka	Kozák	k1gMnSc2	Kozák
a	a	k8xC	a
Alexe	Alex	k1gMnSc5	Alex
Béma	Bémum	k1gNnPc5	Bémum
<g/>
.	.	kIx.	.
</s>
