<s>
Ecma	Ecm	k2eAgFnSc1d1
International	International	k1gFnSc1
</s>
<s>
Ecma	Ecm	k2eAgFnSc1d1
International	International	k1gFnSc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
Ecma	Ecma	k6eAd1
Vznik	vznik	k1gInSc1
</s>
<s>
1961	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
Normalizační	normalizační	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Účel	účel	k1gInSc1
</s>
<s>
Normalizace	normalizace	k1gFnSc1
informačních	informační	k2eAgFnPc2d1
a	a	k8xC
komunikačních	komunikační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
a	a	k8xC
spotřební	spotřební	k2eAgFnSc2d1
elektroniky	elektronika	k1gFnSc2
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Ženeva	Ženeva	k1gFnSc1
Působnost	působnost	k1gFnSc1
</s>
<s>
celosvětová	celosvětový	k2eAgFnSc1d1
Úřední	úřední	k2eAgFnSc1d1
jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
Prezident	prezident	k1gMnSc1
/	/	kIx~
Generální	generální	k2eAgMnSc1d1
sekretář	sekretář	k1gMnSc1
</s>
<s>
Ms.	Ms.	k?
Josée	Josée	k1gNnSc1
Auber	Auber	k1gMnSc1
/	/	kIx~
Dr	dr	kA
<g/>
.	.	kIx.
István	István	k2eAgInSc1d1
Sebestyén	Sebestyén	k1gInSc1
Klíčové	klíčový	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
</s>
<s>
pan	pan	k1gMnSc1
K.	K.	kA
Yamashita	Yamashita	k1gMnSc1
<g/>
,	,	kIx,
pan	pan	k1gMnSc1
D.	D.	kA
Mc	Mc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Allister	Allistra	k1gFnPc2
<g/>
,	,	kIx,
paní	paní	k1gFnSc2
I.	I.	kA
Valet-Harper	Valet-Harper	k1gInSc4
Hlavní	hlavní	k2eAgInSc1d1
orgán	orgán	k1gInSc1
</s>
<s>
valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
Rozpočet	rozpočet	k1gInSc1
</s>
<s>
členské	členský	k2eAgInPc1d1
poplatky	poplatek	k1gInPc1
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc4
</s>
<s>
www.ecma-international.org	www.ecma-international.org	k1gInSc1
Dřívější	dřívější	k2eAgInSc4d1
název	název	k1gInSc4
</s>
<s>
ECMA	ECMA	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ecma	Ecm	k2eAgFnSc1d1
International	International	k1gFnSc1
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
soukromá	soukromý	k2eAgFnSc1d1
nevýdělečná	výdělečný	k2eNgFnSc1d1
organizace	organizace	k1gFnSc1
pro	pro	k7c4
normalizaci	normalizace	k1gFnSc4
informačních	informační	k2eAgInPc2d1
a	a	k8xC
komunikačních	komunikační	k2eAgInPc2d1
systémů	systém	k1gInPc2
s	s	k7c7
otevřeným	otevřený	k2eAgNnSc7d1
členstvím	členství	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Název	název	k1gInSc1
ECMA	ECMA	kA
vznikl	vzniknout	k5eAaPmAgInS
jako	jako	k9
zkratka	zkratka	k1gFnSc1
Evropská	evropský	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
výrobců	výrobce	k1gMnPc2
počítačů	počítač	k1gInPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
European	European	k1gInSc1
Computer	computer	k1gInSc1
Manufacturers	Manufacturers	k1gInSc1
Association	Association	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
změněn	změnit	k5eAaPmNgInS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážel	odrážet	k5eAaImAgMnS
globální	globální	k2eAgFnSc4d1
působnost	působnost	k1gFnSc4
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
není	být	k5eNaImIp3nS
Ecma	Ecma	k1gFnSc1
považována	považován	k2eAgFnSc1d1
za	za	k7c4
zkratku	zkratka	k1gFnSc4
a	a	k8xC
nepíše	psát	k5eNaImIp3nS
se	se	k3xPyFc4
velkými	velký	k2eAgNnPc7d1
písmeny	písmeno	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Organizace	organizace	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
s	s	k7c7
cílem	cíl	k1gInSc7
standardizovat	standardizovat	k5eAaBmF
počítačové	počítačový	k2eAgInPc4d1
systémy	systém	k1gInPc4
vyráběné	vyráběný	k2eAgInPc4d1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členství	členství	k1gNnSc1
je	být	k5eAaImIp3nS
otevřené	otevřený	k2eAgNnSc1d1
pro	pro	k7c4
velké	velká	k1gFnPc4
i	i	k8xC
malé	malý	k2eAgFnPc4d1
společnosti	společnost	k1gFnPc4
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
produkují	produkovat	k5eAaImIp3nP
<g/>
,	,	kIx,
prodávají	prodávat	k5eAaImIp3nP
nebo	nebo	k8xC
vyvíjejí	vyvíjet	k5eAaImIp3nP
počítačové	počítačový	k2eAgInPc1d1
nebo	nebo	k8xC
komunikační	komunikační	k2eAgInPc1d1
systémy	systém	k1gInPc1
a	a	k8xC
mají	mít	k5eAaImIp3nP
zájem	zájem	k1gInSc4
a	a	k8xC
zkušenosti	zkušenost	k1gFnPc4
v	v	k7c6
některé	některý	k3yIgFnSc6
z	z	k7c2
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
jsou	být	k5eAaImIp3nP
zaměřeny	zaměřit	k5eAaPmNgInP
technické	technický	k2eAgInPc1d1
orgány	orgán	k1gInPc1
některé	některý	k3yIgFnSc2
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlem	sídlo	k1gNnSc7
organizace	organizace	k1gFnSc2
je	být	k5eAaImIp3nS
Ženeva	Ženeva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Cíle	cíl	k1gInPc1
</s>
<s>
Ecma	Ecma	k6eAd1
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
vývoj	vývoj	k1gInSc4
standardů	standard	k1gInPc2
a	a	k8xC
technických	technický	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
a	a	k8xC
standardizují	standardizovat	k5eAaBmIp3nP
používání	používání	k1gNnSc4
informačních	informační	k2eAgFnPc2d1
a	a	k8xC
komunikačních	komunikační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
a	a	k8xC
spotřební	spotřební	k2eAgFnSc2d1
elektroniky	elektronika	k1gFnSc2
<g/>
;	;	kIx,
ovlivňováním	ovlivňování	k1gNnSc7
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgNnSc6,k3yQgNnSc6,k3yIgNnSc6
jsou	být	k5eAaImIp3nP
standardy	standard	k1gInPc1
používány	používat	k5eAaImNgInP
<g/>
,	,	kIx,
podněcuje	podněcovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
správnou	správný	k2eAgFnSc4d1
aplikaci	aplikace	k1gFnSc4
<g/>
;	;	kIx,
standardy	standard	k1gInPc4
a	a	k8xC
zprávy	zpráva	k1gFnPc4
publikuje	publikovat	k5eAaBmIp3nS
v	v	k7c6
elektronické	elektronický	k2eAgFnSc6d1
i	i	k8xC
tištěné	tištěný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gFnSc1
publikace	publikace	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
standardů	standard	k1gInPc2
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
volně	volně	k6eAd1
kopírovány	kopírován	k2eAgFnPc4d1
všemi	všecek	k3xTgFnPc7
zainteresovanými	zainteresovaný	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
bez	bez	k7c2
omezení	omezení	k1gNnSc2
autorskými	autorský	k2eAgNnPc7d1
právy	právo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc4
standardů	standard	k1gInPc2
a	a	k8xC
technických	technický	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
vhodnými	vhodný	k2eAgFnPc7d1
národními	národní	k2eAgFnPc7d1
<g/>
,	,	kIx,
evropskými	evropský	k2eAgFnPc7d1
a	a	k8xC
mezinárodními	mezinárodní	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
národních	národní	k2eAgInPc2d1
standardizačních	standardizační	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
je	být	k5eAaImIp3nS
Ecma	Ecm	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
s	s	k7c7
otevřeným	otevřený	k2eAgNnSc7d1
členstvím	členství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrdě	hrdě	k6eAd1
se	se	k3xPyFc4
hlásí	hlásit	k5eAaImIp3nP
k	k	k7c3
„	„	k?
<g/>
obchodnímu	obchodní	k2eAgMnSc3d1
<g/>
“	“	k?
přístupu	přístup	k1gInSc6
ke	k	k7c3
standardům	standard	k1gInPc3
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
cestu	cesta	k1gFnSc4
k	k	k7c3
lepším	dobrý	k2eAgInPc3d2
standardům	standard	k1gInPc3
vytvořeným	vytvořený	k2eAgInPc3d1
v	v	k7c6
kratším	krátký	k2eAgInSc6d2
čase	čas	k1gInSc6
<g/>
,	,	kIx,
díky	díky	k7c3
snížení	snížení	k1gNnSc3
byrokracie	byrokracie	k1gFnSc2
procesu	proces	k1gInSc2
zaměřeném	zaměřený	k2eAgNnSc6d1
na	na	k7c6
dosahování	dosahování	k1gNnSc6
výsledků	výsledek	k1gInPc2
cestou	cestou	k7c2
konsenzu	konsenz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ecma	Ecma	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
50	#num#	k4
let	léto	k1gNnPc2
aktivně	aktivně	k6eAd1
přispívá	přispívat	k5eAaImIp3nS
k	k	k7c3
celosvětové	celosvětový	k2eAgFnSc3d1
standardizaci	standardizace	k1gFnSc3
v	v	k7c6
informačních	informační	k2eAgFnPc6d1
technologiích	technologie	k1gFnPc6
a	a	k8xC
telekomunikacích	telekomunikace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Publikovala	publikovat	k5eAaBmAgFnS
více	hodně	k6eAd2
než	než	k8xS
400	#num#	k4
standardů	standard	k1gInPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
100	#num#	k4
technických	technický	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
více	hodně	k6eAd2
než	než	k8xS
2	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
byly	být	k5eAaImAgInP
převzaty	převzat	k2eAgInPc1d1
jako	jako	k8xS,k8xC
mezinárodní	mezinárodní	k2eAgInPc4d1
standardy	standard	k1gInPc4
nebo	nebo	k8xC
technické	technický	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
členů	člen	k1gMnPc2
Ecma	Ecmus	k1gMnSc2
International	International	k1gMnSc2
je	být	k5eAaImIp3nS
dostupný	dostupný	k2eAgInSc1d1
na	na	k7c6
stránkách	stránka	k1gFnPc6
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Standardy	standard	k1gInPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
Ecma	Ecmum	k1gNnSc2
standardů	standard	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Sdružení	sdružení	k1gNnSc1
Ecma	Ecm	k1gInSc2
International	International	k1gFnSc2
je	být	k5eAaImIp3nS
aktuálně	aktuálně	k6eAd1
odpovědné	odpovědný	k2eAgNnSc1d1
za	za	k7c4
množství	množství	k1gNnSc4
standardů	standard	k1gInPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
ECMA-6	ECMA-6	k4
–	–	k?
7	#num#	k4
<g/>
bitová	bitový	k2eAgFnSc1d1
znaková	znakový	k2eAgFnSc1d1
sada	sada	k1gFnSc1
(	(	kIx(
<g/>
vycházející	vycházející	k2eAgFnSc1d1
z	z	k7c2
ASCII	ascii	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
verze	verze	k1gFnSc1
přijata	přijat	k2eAgFnSc1d1
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1965	#num#	k4
<g/>
,	,	kIx,
změny	změna	k1gFnPc1
zahrnuté	zahrnutý	k2eAgFnPc1d1
ve	v	k7c4
3	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
zahrnuty	zahrnout	k5eAaPmNgInP
do	do	k7c2
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
646	#num#	k4
<g/>
-	-	kIx~
<g/>
1972	#num#	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
součástí	součást	k1gFnPc2
je	být	k5eAaImIp3nS
strojově	strojově	k6eAd1
čitelné	čitelný	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
OCR-B	OCR-B	k1gFnSc2
</s>
<s>
ECMA-35	ECMA-35	k4
–	–	k?
Struktura	struktura	k1gFnSc1
znakového	znakový	k2eAgInSc2d1
kódu	kód	k1gInSc2
a	a	k8xC
techniky	technika	k1gFnSc2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
rozšíření	rozšíření	k1gNnSc2
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
vydáno	vydat	k5eAaPmNgNnS
1971	#num#	k4
<g/>
,	,	kIx,
přijato	přijmout	k5eAaPmNgNnS
také	také	k6eAd1
jako	jako	k8xS,k8xC
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
2022	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-43	ECMA-43	k4
–	–	k?
Struktura	struktura	k1gFnSc1
a	a	k8xC
pravidla	pravidlo	k1gNnSc2
8	#num#	k4
<g/>
bitové	bitový	k2eAgFnSc2d1
kódované	kódovaný	k2eAgFnSc2d1
znakové	znakový	k2eAgFnSc2d1
sady	sada	k1gFnSc2
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
vydáno	vydat	k5eAaPmNgNnS
v	v	k7c6
prosinci	prosinec	k1gInSc6
1974	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-48	ECMA-48	k4
–	–	k?
Řídicí	řídicí	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
pro	pro	k7c4
kódované	kódovaný	k2eAgInPc4d1
znakové	znakový	k2eAgInPc4d1
sady	sad	k1gInPc4
<g/>
,	,	kIx,
přijato	přijmout	k5eAaPmNgNnS
také	také	k6eAd1
jako	jako	k8xS,k8xC
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
6429	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-94	ECMA-94	k4
–	–	k?
8	#num#	k4
<g/>
bitové	bitový	k2eAgFnSc2d1
kódované	kódovaný	k2eAgFnSc2d1
znakové	znakový	k2eAgFnSc2d1
sady	sada	k1gFnSc2
(	(	kIx(
<g/>
poprvé	poprvé	k6eAd1
vydáno	vydat	k5eAaPmNgNnS
v	v	k7c6
březnu	březen	k1gInSc6
1985	#num#	k4
<g/>
,	,	kIx,
přijato	přijmout	k5eAaPmNgNnS
jako	jako	k8xS,k8xC
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
8859	#num#	k4
<g/>
,	,	kIx,
část	část	k1gFnSc1
1	#num#	k4
až	až	k9
4	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-113	ECMA-113	k4
–	–	k?
8	#num#	k4
<g/>
bitová	bitový	k2eAgFnSc1d1
jednobytová	jednobytový	k2eAgFnSc1d1
kódovaná	kódovaný	k2eAgFnSc1d1
znaková	znakový	k2eAgFnSc1d1
sada	sada	k1gFnSc1
–	–	k?
latinka	latinka	k1gFnSc1
<g/>
/	/	kIx~
<g/>
cyrilice	cyrilice	k1gFnSc1
(	(	kIx(
<g/>
přijato	přijmout	k5eAaPmNgNnS
jako	jako	k9
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
8859	#num#	k4
<g/>
,	,	kIx,
část	část	k1gFnSc1
5	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-114	ECMA-114	k4
–	–	k?
8	#num#	k4
<g/>
bitová	bitový	k2eAgFnSc1d1
jednobytová	jednobytový	k2eAgFnSc1d1
kódovaná	kódovaný	k2eAgFnSc1d1
znaková	znakový	k2eAgFnSc1d1
sada	sada	k1gFnSc1
–	–	k?
latinka	latinka	k1gFnSc1
<g/>
/	/	kIx~
<g/>
arabská	arabský	k2eAgFnSc1d1
abeceda	abeceda	k1gFnSc1
(	(	kIx(
<g/>
přijato	přijmout	k5eAaPmNgNnS
jako	jako	k9
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
8859	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-119	ECMA-119	k4
–	–	k?
Struktura	struktura	k1gFnSc1
systému	systém	k1gInSc2
souborů	soubor	k1gInPc2
na	na	k7c6
CD-ROM	CD-ROM	k1gFnSc6
(	(	kIx(
<g/>
přijato	přijmout	k5eAaPmNgNnS
jako	jako	k9
ISO	ISO	kA
9660	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-130	ECMA-130	k4
–	–	k?
„	„	k?
<g/>
Yellow	Yellow	k1gMnSc1
Book	Book	k1gMnSc1
<g/>
“	“	k?
formát	formát	k1gInSc1
CD-ROM	CD-ROM	k1gFnPc2
</s>
<s>
ECMA-262	ECMA-262	k4
–	–	k?
Specifikace	specifikace	k1gFnSc1
jazyka	jazyk	k1gInSc2
ECMAScript	ECMAScripta	k1gFnPc2
(	(	kIx(
<g/>
implementace	implementace	k1gFnSc1
<g/>
:	:	kIx,
JavaScript	JavaScript	k1gMnSc1
<g/>
,	,	kIx,
JScript	JScript	k1gMnSc1
a	a	k8xC
ActionScript	ActionScript	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
také	také	k9
jako	jako	k9
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
16262	#num#	k4
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-334	ECMA-334	k4
–	–	k?
Programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
C	C	kA
<g/>
#	#	kIx~
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-335	ECMA-335	k4
–	–	k?
Common	Common	k1gNnSc4
Language	language	k1gFnSc2
Infrastructure	Infrastructur	k1gMnSc5
(	(	kIx(
<g/>
CLI	clít	k5eAaImRp2nS
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-341	ECMA-341	k4
–	–	k?
Kritéria	kritérion	k1gNnPc1
dopadu	dopad	k1gInSc2
návrhu	návrh	k1gInSc2
elektronických	elektronický	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
na	na	k7c4
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-363	ECMA-363	k4
–	–	k?
Formát	formát	k1gInSc1
souborů	soubor	k1gInPc2
Universal	Universal	k1gFnSc4
3	#num#	k4
<g/>
D	D	kA
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-367	ECMA-367	k4
–	–	k?
Eiffel	Eiffel	k1gMnSc1
<g/>
:	:	kIx,
Analýza	analýza	k1gFnSc1
<g/>
,	,	kIx,
design	design	k1gInSc1
a	a	k8xC
programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-372	ECMA-372	k4
–	–	k?
Specifikace	specifikace	k1gFnSc1
jazyka	jazyk	k1gInSc2
C	C	kA
<g/>
++	++	k?
<g/>
/	/	kIx~
<g/>
CLI	clít	k5eAaImRp2nS
(	(	kIx(
<g/>
Common	Common	k1gInSc1
Language	language	k1gFnSc2
Infrastructure	Infrastructur	k1gMnSc5
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-376	ECMA-376	k4
–	–	k?
Office	Office	kA
Open	Open	k1gInSc1
XML	XML	kA
(	(	kIx(
<g/>
přijatý	přijatý	k2eAgInSc1d1
jako	jako	k8xC,k8xS
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
29500	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-377	ECMA-377	k4
–	–	k?
Zapisovatelná	zapisovatelný	k2eAgNnPc1d1
média	médium	k1gNnPc1
Holographic	Holographice	k1gFnPc2
Versatile	Versatil	k1gMnSc5
Disc	disco	k1gNnPc2
(	(	kIx(
<g/>
HVD	HVD	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-378	ECMA-378	k4
–	–	k?
Nezapisovatelná	zapisovatelný	k2eNgNnPc1d1
média	médium	k1gNnPc1
Holographic	Holographice	k1gFnPc2
Versatile	Versatil	k1gMnSc5
Disc	disco	k1gNnPc2
(	(	kIx(
<g/>
HVD-ROM	HVD-ROM	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-388	ECMA-388	k4
–	–	k?
Open	Opena	k1gFnPc2
XML	XML	kA
Paper	Paper	k1gInSc1
Specification	Specification	k1gInSc1
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-402	ECMA-402	k4
–	–	k?
Specifikace	specifikace	k1gFnSc1
internacionalizačního	internacionalizační	k2eAgMnSc2d1
API	API	kA
pro	pro	k7c4
ECMAScript	ECMAScript	k1gInSc4
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-404	ECMA-404	k4
–	–	k?
JSON	JSON	kA
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ECMA-408	ECMA-408	k4
–	–	k?
Standard	standard	k1gInSc1
jazyka	jazyk	k1gInSc2
Dart	Darta	k1gFnPc2
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
Java	Jav	k1gInSc2
</s>
<s>
Firma	firma	k1gFnSc1
Sun	suna	k1gFnPc2
Microsystems	Microsystemsa	k1gFnPc2
sice	sice	k8xC
požádala	požádat	k5eAaPmAgFnS
Ecma	Ecmum	k1gNnPc4
o	o	k7c4
normalizaci	normalizace	k1gFnSc4
svého	svůj	k3xOyFgInSc2
jazyka	jazyk	k1gInSc2
Java	Jav	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
tuto	tento	k3xDgFnSc4
žádost	žádost	k1gFnSc4
stáhla	stáhnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecmum	k1gNnPc1
proto	proto	k8xC
za	za	k7c4
standardizaci	standardizace	k1gFnSc4
jazyka	jazyk	k1gInSc2
Java	Javum	k1gNnSc2
není	být	k5eNaImIp3nS
odpovědná	odpovědný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Formáty	formát	k1gInPc1
Office	Office	kA
Open	Openo	k1gNnPc2
XML	XML	kA
</s>
<s>
Ecma	Ecma	k6eAd1
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
na	na	k7c4
normalizaci	normalizace	k1gFnSc4
formátu	formát	k1gInSc2
Office	Office	kA
Open	Open	k1gInSc1
XML	XML	kA
vycházející	vycházející	k2eAgFnSc2d1
z	z	k7c2
formátu	formát	k1gInSc2
XML	XML	kA
office	office	k1gFnSc1
dokument	dokument	k1gInSc1
formáty	formát	k1gInPc7
Microsoft	Microsoft	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecm	k1gInSc2
Office	Office	kA
Open	Open	k1gMnSc1
XML	XML	kA
správa	správa	k1gFnSc1
proces	proces	k1gInSc1
je	být	k5eAaImIp3nS
aktuálně	aktuálně	k6eAd1
prováděn	provádět	k5eAaImNgInS
technickým	technický	k2eAgInSc7d1
výborem	výbor	k1gInSc7
45	#num#	k4
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Eco	Eco	k?
Deklarace	deklarace	k1gFnSc1
</s>
<s>
Ecma	Ecma	k6eAd1
společně	společně	k6eAd1
se	s	k7c7
skandinávskou	skandinávský	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
IT	IT	kA
Eco	Eco	k1gMnSc1
Declaration	Declaration	k1gInSc4
prosadila	prosadit	k5eAaPmAgFnS
směrnice	směrnice	k1gFnSc1
pro	pro	k7c4
informování	informování	k1gNnSc4
spotřebitelů	spotřebitel	k1gMnPc2
o	o	k7c6
postupech	postup	k1gInPc6
neohrožujících	ohrožující	k2eNgInPc2d1
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
výrobci	výrobce	k1gMnPc1
ICT	ICT	kA
a	a	k8xC
spotřební	spotřební	k2eAgFnSc2d1
elektroniky	elektronika	k1gFnSc2
ve	v	k7c6
standardu	standard	k1gInSc6
370	#num#	k4
Ecma	Ecma	k1gMnSc1
International	International	k1gMnSc1
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
IT	IT	kA
Eco	Eco	k1gFnSc1
Declaration	Declaration	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
informace	informace	k1gFnPc4
o	o	k7c6
postupech	postup	k1gInPc6
výrobců	výrobce	k1gMnPc2
a	a	k8xC
vlastnostech	vlastnost	k1gFnPc6
výrobků	výrobek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
ovlivňují	ovlivňovat	k5eAaImIp3nP
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
návrh	návrh	k1gInSc4
výrobků	výrobek	k1gInPc2
<g/>
,	,	kIx,
baterie	baterie	k1gFnSc1
<g/>
,	,	kIx,
hluk	hluk	k1gInSc1
<g/>
,	,	kIx,
elektrická	elektrický	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
<g/>
,	,	kIx,
spotřeba	spotřeba	k1gFnSc1
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
chemické	chemický	k2eAgFnPc4d1
emise	emise	k1gFnPc4
<g/>
,	,	kIx,
použité	použitý	k2eAgFnPc4d1
látky	látka	k1gFnPc4
a	a	k8xC
materiály	materiál	k1gInPc4
a	a	k8xC
obaly	obal	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgFnPc1
vlastnosti	vlastnost	k1gFnPc1
jsou	být	k5eAaImIp3nP
popsány	popsat	k5eAaPmNgFnP
jednotně	jednotně	k6eAd1
společným	společný	k2eAgInSc7d1
průmyslovým	průmyslový	k2eAgInSc7d1
standardem	standard	k1gInSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
porovnávání	porovnávání	k1gNnSc4
různých	různý	k2eAgMnPc2d1
dodavatelů	dodavatel	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc2
výrobků	výrobek	k1gInPc2
snazší	snadný	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Ecma	Ecmus	k1gMnSc2
International	International	k1gMnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Ecma	Ecma	k1gFnSc1
By-laws	By-laws	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanovy	stanova	k1gFnSc2
Ecma	Ecma	k1gFnSc1
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ecma	Ecma	k1gMnSc1
International	International	k1gMnSc1
<g/>
,	,	kIx,
October	October	k1gMnSc1
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ecma	Ecma	k1gMnSc1
International	International	k1gMnSc1
<g/>
,	,	kIx,
October	October	k1gMnSc1
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ecma	Ecm	k1gInSc2
Members	Membersa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
,	,	kIx,
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
-bit	-bit	k2eAgInSc1d1
Coded	Coded	k1gInSc1
Character	Charactra	k1gFnPc2
Set	set	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
December	December	k1gInSc1
1991	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
35	#num#	k4
<g/>
,	,	kIx,
Structure	Structur	k1gMnSc5
and	and	k?
Extension	Extension	k1gInSc1
Techniques	Techniques	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
December	December	k1gInSc1
1994	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
43	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
-bit	-bit	k2eAgInSc1d1
Coded	Coded	k1gInSc1
Character	Charactrum	k1gNnPc2
Set	sto	k4xCgNnPc2
Structure	Structur	k1gMnSc5
and	and	k?
Rules	Rules	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
December	December	k1gInSc1
1991	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
48	#num#	k4
<g/>
,	,	kIx,
Control	Control	k1gInSc1
Functions	Functionsa	k1gFnPc2
for	forum	k1gNnPc2
Coded	Coded	k1gMnSc1
Character	Character	k1gMnSc1
Sets	Sets	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
June	jun	k1gMnSc5
1991	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
94	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
-bit	-bita	k1gFnPc2
Single-byte	Single-byt	k1gInSc5
Coded	Coded	k1gInSc4
Graphics	Graphics	k1gInSc4
Character	Charactra	k1gFnPc2
Sets	Setsa	k1gFnPc2
<g/>
,	,	kIx,
Latin	latina	k1gFnPc2
Alphabets	Alphabetsa	k1gFnPc2
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
to	ten	k3xDgNnSc1
No	no	k9
<g/>
.	.	kIx.
4	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
June	jun	k1gMnSc5
1986	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
113	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
-bit	-bita	k1gFnPc2
Single-Byte	Single-Byt	k1gInSc5
Coded	Coded	k1gMnSc1
Graphics	Graphicsa	k1gFnPc2
Character	Charactra	k1gFnPc2
Set	set	k1gInSc1
<g/>
,	,	kIx,
Latin	Latin	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Cyrillic	Cyrillice	k1gFnPc2
Alphabet	Alphabet	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
December	December	k1gInSc1
2000	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
114	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
-bit	-bita	k1gFnPc2
Single-Byte	Single-Byt	k1gInSc5
Coded	Coded	k1gMnSc1
Graphics	Graphicsa	k1gFnPc2
Character	Charactra	k1gFnPc2
Set	set	k1gInSc1
<g/>
,	,	kIx,
Latin	Latin	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Arabic	Arabic	k1gMnSc1
Alphabet	Alphabet	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
December	December	k1gInSc1
2000	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
119	#num#	k4
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
and	and	k?
File	Filus	k1gMnSc5
Structure	Structur	k1gMnSc5
of	of	k?
CDROM	CDROM	kA
for	forum	k1gNnPc2
Information	Information	k1gInSc4
Interchange	Interchange	k1gNnPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gFnSc1
International	International	k1gFnSc1
<g/>
,	,	kIx,
December	December	k1gInSc1
1987	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
262	#num#	k4
<g/>
,	,	kIx,
Specifikace	specifikace	k1gFnSc1
jazyka	jazyk	k1gInSc2
ECMAScript	ECMAScripta	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gFnSc1
International	International	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
334	#num#	k4
<g/>
,	,	kIx,
Specifikace	specifikace	k1gFnSc1
jazyka	jazyk	k1gInSc2
C	C	kA
<g/>
#	#	kIx~
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gFnSc1
International	International	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
335	#num#	k4
<g/>
,	,	kIx,
Common	Common	k1gInSc1
Language	language	k1gFnSc2
Infrastructure	Infrastructur	k1gMnSc5
(	(	kIx(
<g/>
CLI	clít	k5eAaImRp2nS
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gFnSc1
International	International	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
341	#num#	k4
<g/>
,	,	kIx,
Environmental	Environmental	k1gMnSc1
design	design	k1gInSc4
considerations	considerations	k1gInSc4
for	forum	k1gNnPc2
electronic	electronice	k1gFnPc2
products	productsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gFnSc1
International	International	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
363	#num#	k4
<g/>
,	,	kIx,
Universal	Universal	k1gFnSc1
3D	3D	k4
File	Fil	k1gInSc2
Format	Format	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gFnSc1
International	International	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
367	#num#	k4
<g/>
,	,	kIx,
Eiffel	Eiffel	k1gInSc1
analysis	analysis	k1gInSc1
<g/>
,	,	kIx,
design	design	k1gInSc1
and	and	k?
programming	programming	k1gInSc1
Language	language	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gFnSc1
International	International	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ecma	Ecma	k1gMnSc1
International	International	k1gMnSc1
<g/>
,	,	kIx,
December	December	k1gMnSc1
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
376	#num#	k4
<g/>
:	:	kIx,
Office	Office	kA
Open	Opena	k1gFnPc2
XML	XML	kA
File	File	k1gNnSc4
Formats	Formats	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gFnSc1
International	International	k1gFnSc1
<g/>
,	,	kIx,
December	December	k1gInSc1
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
377	#num#	k4
<g/>
:	:	kIx,
Information	Information	k1gInSc1
Interchange	Interchang	k1gFnSc2
on	on	k3xPp3gMnSc1
Holographic	Holographic	k1gMnSc1
Versatile	Versatil	k1gMnSc5
Disc	disco	k1gNnPc2
(	(	kIx(
<g/>
HVD	HVD	kA
<g/>
)	)	kIx)
Recordable	Recordable	k1gMnSc1
Cartridges	Cartridges	k1gMnSc1
<g/>
;	;	kIx,
Capacity	Capacit	k1gInPc1
<g/>
:	:	kIx,
200	#num#	k4
Gbytes	Gbytes	k1gInSc1
per	pero	k1gNnPc2
Cartridge	Cartridg	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gMnSc1
International	International	k1gMnSc1
<g/>
,	,	kIx,
May	May	k1gMnSc1
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
378	#num#	k4
<g/>
:	:	kIx,
Information	Information	k1gInSc1
Interchange	Interchang	k1gFnSc2
on	on	k3xPp3gMnSc1
Read-Only	Read-Onla	k1gFnSc2
Memory	Memor	k1gInPc4
Holographic	Holographice	k1gInPc2
Versatile	Versatil	k1gMnSc5
Disc	disco	k1gNnPc2
(	(	kIx(
<g/>
HVD-ROM	HVD-ROM	k1gFnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Capacity	Capacit	k1gInPc1
<g/>
:	:	kIx,
100	#num#	k4
Gbytes	Gbytesa	k1gFnPc2
per	pero	k1gNnPc2
disk	disk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gMnSc1
International	International	k1gMnSc1
<g/>
,	,	kIx,
May	May	k1gMnSc1
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
388	#num#	k4
<g/>
:	:	kIx,
Open	Open	k1gInSc1
XML	XML	kA
Paper	Paper	k1gInSc1
Specification	Specification	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gMnSc1
International	International	k1gMnSc1
<g/>
,	,	kIx,
June	jun	k1gMnSc5
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
402	#num#	k4
<g/>
:	:	kIx,
ECMAScript	ECMAScript	k1gInSc1
<g/>
®	®	k?
Internationalization	Internationalization	k1gInSc1
API	API	kA
Specification	Specification	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gFnSc1
International	International	k1gFnSc1
<g/>
,	,	kIx,
December	December	k1gInSc1
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
404	#num#	k4
<g/>
:	:	kIx,
The	The	k1gFnSc1
JSON	JSON	kA
Data	datum	k1gNnSc2
Interchange	Interchange	k1gNnSc1
Format	Format	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gFnSc1
International	International	k1gFnSc1
<g/>
,	,	kIx,
October	October	k1gInSc1
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
408	#num#	k4
<g/>
:	:	kIx,
Dart	Dart	k2eAgInSc1d1
Programming	Programming	k1gInSc1
Language	language	k1gFnPc2
Specification	Specification	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gFnSc1
International	International	k1gFnSc1
<g/>
,	,	kIx,
December	December	k1gInSc1
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Sun	Sun	kA
to	ten	k3xDgNnSc4
retain	retain	k2eAgInSc4d1
grip	grip	k1gInSc4
on	on	k3xPp3gMnSc1
Java	Java	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1999-12-08	1999-12-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
TC45	TC45	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gFnSc1
International	International	k1gFnSc1
<g/>
,	,	kIx,
December	December	k1gInSc1
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc1
<g/>
370	#num#	k4
<g/>
:	:	kIx,
TED	Ted	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Eco	Eco	k1gFnSc2
Declaration	Declaration	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecma	Ecma	k1gMnSc1
International	International	k1gMnSc1
<g/>
,	,	kIx,
June	jun	k1gMnSc5
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
Ecma	Ecmum	k1gNnSc2
standardů	standard	k1gInPc2
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
pro	pro	k7c4
normalizaci	normalizace	k1gFnSc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ecma	Ecm	k1gInSc2
International	International	k1gMnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Domovská	domovský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Ecma	Ecmus	k1gMnSc2
International	International	k1gMnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
norem	norma	k1gFnPc2
Ecma	Ecm	k1gInSc2
</s>
<s>
Specifikace	specifikace	k1gFnSc1
jazyka	jazyk	k1gInSc2
C	C	kA
<g/>
#	#	kIx~
(	(	kIx(
<g/>
s	s	k7c7
hypertextovými	hypertextový	k2eAgInPc7d1
odkazy	odkaz	k1gInPc7
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
11	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Standardy	standard	k1gInPc1
Ecma	Ecmum	k1gNnSc2
International	International	k1gFnSc2
Aplikační	aplikační	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
</s>
<s>
ANSI	ANSI	kA
escape	escapat	k5eAaPmIp3nS
kód	kód	k1gInSc4
</s>
<s>
Common	Common	k1gInSc1
Language	language	k1gFnSc2
Infrastructure	Infrastructur	k1gMnSc5
</s>
<s>
Office	Office	kA
Open	Openo	k1gNnPc2
XML	XML	kA
</s>
<s>
OpenXPS	OpenXPS	k?
Souborové	souborový	k2eAgInPc4d1
systémy	systém	k1gInPc4
(	(	kIx(
<g/>
magnetické	magnetický	k2eAgInPc4d1
pásky	pásek	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Advanced	Advanced	k1gMnSc1
Intelligent	Intelligent	k1gMnSc1
Tape	Tape	k1gInSc4
</s>
<s>
DDS	DDS	kA
</s>
<s>
DLT	DLT	kA
</s>
<s>
Super	super	k1gInSc1
DLT	DLT	kA
</s>
<s>
Holographic	Holographic	k1gMnSc1
Versatile	Versatil	k1gMnSc5
Disc	disco	k1gNnPc2
</s>
<s>
Linear	Linear	k1gInSc1
Tape-Open	Tape-Open	k2eAgInSc1d1
(	(	kIx(
<g/>
Ultrium-	Ultrium-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
VXA	VXA	kA
Souborové	souborový	k2eAgInPc4d1
systémy	systém	k1gInPc4
(	(	kIx(
<g/>
disky	disk	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
CD-ROM	CD-ROM	k?
</s>
<s>
CD	CD	kA
File	File	k1gFnSc1
System	Syst	k1gInSc7
(	(	kIx(
<g/>
CDFS	CDFS	kA
<g/>
)	)	kIx)
</s>
<s>
FAT	fatum	k1gNnPc2
</s>
<s>
FAT12	FAT12	k4
</s>
<s>
FAT16	FAT16	k4
</s>
<s>
FAT16B	FAT16B	k4
</s>
<s>
FD	FD	kA
</s>
<s>
UDF	UDF	kA
</s>
<s>
Ultra	ultra	k2eAgInPc1d1
Density	Densit	k1gInPc1
Optical	Optical	k1gFnSc2
</s>
<s>
Universal	Universat	k5eAaPmAgMnS,k5eAaImAgMnS
Media	medium	k1gNnPc4
Disc	disco	k1gNnPc2
Grafika	grafika	k1gFnSc1
</s>
<s>
Universal	Universat	k5eAaPmAgMnS,k5eAaImAgMnS
3D	3D	k4
Programovací	programovací	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
</s>
<s>
C	C	kA
<g/>
++	++	k?
<g/>
/	/	kIx~
<g/>
CLI	clít	k5eAaImRp2nS
</s>
<s>
C	C	kA
<g/>
#	#	kIx~
</s>
<s>
Eiffel	Eiffel	k1gMnSc1
</s>
<s>
JavaScript	JavaScript	k1gMnSc1
(	(	kIx(
<g/>
E	E	kA
<g/>
4	#num#	k4
<g/>
X	X	kA
<g/>
,	,	kIx,
ECMAScript	ECMAScript	k1gInSc1
<g/>
)	)	kIx)
Rádiová	rádiový	k2eAgFnSc1d1
síťová	síťový	k2eAgFnSc1d1
rozhraní	rozhraní	k1gNnSc1
</s>
<s>
NFC	NFC	kA
</s>
<s>
UWB	UWB	kA
Ostatní	ostatní	k2eAgFnPc1d1
</s>
<s>
TC20	TC20	k4
Elektromagnetická	elektromagnetický	k2eAgFnSc1d1
kompatibilita	kompatibilita	k1gFnSc1
</s>
<s>
TC12	TC12	k4
Bezpečnost	bezpečnost	k1gFnSc1
výrobků	výrobek	k1gInPc2
Seznam	seznam	k1gInSc4
Ecma	Ecmum	k1gNnSc2
standardů	standard	k1gInPc2
(	(	kIx(
<g/>
1961	#num#	k4
-	-	kIx~
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
ECMAScript	ECMAScript	k2eAgInSc4d1
Dialekty	dialekt	k1gInPc4
</s>
<s>
ActionScript	ActionScript	k1gMnSc1
</s>
<s>
Caja	Caja	k6eAd1
</s>
<s>
JavaScript	JavaScript	k1gMnSc1
</s>
<s>
engines	engines	k1gMnSc1
</s>
<s>
asm	asm	k?
<g/>
.	.	kIx.
<g/>
js	js	k?
</s>
<s>
JScript	JScript	k1gMnSc1
</s>
<s>
JScript	JScript	k1gMnSc1
.	.	kIx.
<g/>
NET	NET	kA
</s>
<s>
QtScript	QtScript	k1gMnSc1
</s>
<s>
WMLScript	WMLScript	k2eAgInSc1d1
PorovnáníECMAScript	PorovnáníECMAScript	k1gInSc1
enginů	engin	k1gMnPc2
</s>
<s>
Carakan	Carakan	k1gMnSc1
</s>
<s>
Futhark	Futhark	k1gInSc1
</s>
<s>
InScript	InScript	k1gMnSc1
</s>
<s>
JavaScriptCore	JavaScriptCor	k1gMnSc5
</s>
<s>
SquirrelFish	SquirrelFish	k1gMnSc1
</s>
<s>
JScript	JScript	k1gMnSc1
</s>
<s>
KJS	KJS	kA
</s>
<s>
Linear	Linear	k1gMnSc1
B	B	kA
</s>
<s>
Narcissus	Narcissus	k1gMnSc1
</s>
<s>
QtScript	QtScript	k1gMnSc1
</s>
<s>
Rhino	Rhino	k6eAd1
</s>
<s>
SpiderMonkey	SpiderMonkea	k1gFnPc1
</s>
<s>
TraceMonkey	TraceMonkea	k1gFnPc1
</s>
<s>
JägerMonkey	JägerMonkea	k1gFnPc1
</s>
<s>
Tamarin	Tamarin	k1gInSc1
</s>
<s>
V8	V8	k4
</s>
<s>
Chakra	Chakra	k6eAd1
</s>
<s>
JScript	JScript	k1gMnSc1
.	.	kIx.
<g/>
NET	NET	kA
</s>
<s>
Nashorn	Nashorn	k1gInSc1
Frameworky	Frameworka	k1gFnSc2
</s>
<s>
Client-side	Client-sid	k1gMnSc5
</s>
<s>
AJAX	AJAX	kA
</s>
<s>
Ample	Ample	k1gFnSc1
SDK	SDK	kA
</s>
<s>
Chaplin	Chaplin	k1gInSc1
<g/>
.	.	kIx.
<g/>
js	js	k?
</s>
<s>
Dojo	Dojo	k6eAd1
</s>
<s>
Echo	echo	k1gNnSc1
</s>
<s>
Ext	Ext	k?
JS	JS	kA
</s>
<s>
Google	Google	k6eAd1
Web	web	k1gInSc1
Toolkit	Toolkit	k2eAgInSc1d1
</s>
<s>
JQuery	JQuera	k1gFnPc1
</s>
<s>
Lively	Livela	k1gFnPc1
Kernel	kernel	k1gInSc1
</s>
<s>
midori	midori	k6eAd1
</s>
<s>
MochiKit	MochiKit	k1gMnSc1
</s>
<s>
MooTools	MooTools	k6eAd1
</s>
<s>
Prototype	prototyp	k1gInSc5
</s>
<s>
Pyjamas	Pyjamas	k1gMnSc1
</s>
<s>
qooxdoo	qooxdoo	k6eAd1
</s>
<s>
Rialto	Rialto	k1gNnSc1
</s>
<s>
Rico	Rico	k6eAd1
</s>
<s>
script	script	k1gInSc1
<g/>
.	.	kIx.
<g/>
aculo	aculo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
us	us	k?
</s>
<s>
SmartClient	SmartClient	k1gMnSc1
</s>
<s>
SproutCore	SproutCor	k1gMnSc5
</s>
<s>
Spry	Spra	k1gFnPc1
</s>
<s>
Wakanda	Wakanda	k1gFnSc1
Framework	Framework	k1gInSc1
</s>
<s>
YUI	YUI	kA
Library	Librar	k1gInPc4
Server-side	Server-sid	k1gMnSc5
</s>
<s>
AppJet	AppJet	k1gMnSc1
</s>
<s>
Jaxer	Jaxer	k1gMnSc1
</s>
<s>
Node	Node	k1gFnSc1
<g/>
.	.	kIx.
<g/>
js	js	k?
</s>
<s>
WakandaDB	WakandaDB	k?
Multiple	multipl	k1gInSc5
</s>
<s>
Cappuccino	Cappuccino	k1gNnSc1
</s>
<s>
Objective-J	Objective-J	k?
</s>
<s>
PureMVC	PureMVC	k?
Knihovny	knihovna	k1gFnPc1
</s>
<s>
Backbone	Backbon	k1gMnSc5
<g/>
.	.	kIx.
<g/>
js	js	k?
</s>
<s>
SWFObject	SWFObject	k1gMnSc1
</s>
<s>
SWFAddress	SWFAddress	k6eAd1
</s>
<s>
Underscore	Underscor	k1gMnSc5
<g/>
.	.	kIx.
<g/>
js	js	k?
</s>
<s>
Lidé	člověk	k1gMnPc1
</s>
<s>
Brendan	Brendan	k1gMnSc1
Eich	Eich	k1gMnSc1
</s>
<s>
Douglas	Douglas	k1gMnSc1
Crockford	Crockford	k1gMnSc1
</s>
<s>
John	John	k1gMnSc1
Resig	Resig	k1gMnSc1
</s>
<s>
Scott	Scott	k1gInSc1
Isaacs	Isaacs	k1gInSc1
Různé	různý	k2eAgInPc1d1
</s>
<s>
DHTML	DHTML	kA
</s>
<s>
Ecma	Ecm	k2eAgFnSc1d1
International	International	k1gFnSc1
</s>
<s>
JSDoc	JSDoc	k6eAd1
</s>
<s>
JSGI	JSGI	kA
</s>
<s>
JSHint	JSHint	k1gMnSc1
</s>
<s>
JSLint	JSLint	k1gMnSc1
</s>
<s>
JSON	JSON	kA
</s>
<s>
JSSS	JSSS	kA
</s>
<s>
Sputnik	sputnik	k1gInSc1
</s>
<s>
SunSpider	SunSpider	k1gMnSc1
</s>
<s>
Asynchronous	Asynchronous	k1gMnSc1
module	modul	k1gInSc5
definition	definition	k1gInSc1
</s>
<s>
CommonJS	CommonJS	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
24044-8	24044-8	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1015	#num#	k4
5629	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82210698	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
147166695	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82210698	#num#	k4
</s>
