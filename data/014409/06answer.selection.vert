<s>
Ecma	Ecm	k2eAgFnSc1d1
International	International	k1gFnSc1
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
soukromá	soukromý	k2eAgFnSc1d1
nevýdělečná	výdělečný	k2eNgFnSc1d1
organizace	organizace	k1gFnSc1
pro	pro	k7c4
normalizaci	normalizace	k1gFnSc4
informačních	informační	k2eAgInPc2d1
a	a	k8xC
komunikačních	komunikační	k2eAgInPc2d1
systémů	systém	k1gInPc2
s	s	k7c7
otevřeným	otevřený	k2eAgNnSc7d1
členstvím	členství	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Název	název	k1gInSc1
ECMA	ECMA	kA
vznikl	vzniknout	k5eAaPmAgInS
jako	jako	k9
zkratka	zkratka	k1gFnSc1
Evropská	evropský	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
výrobců	výrobce	k1gMnPc2
počítačů	počítač	k1gInPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
European	European	k1gInSc1
Computer	computer	k1gInSc1
Manufacturers	Manufacturers	k1gInSc1
Association	Association	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
změněn	změnit	k5eAaPmNgInS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážel	odrážet	k5eAaImAgMnS
globální	globální	k2eAgFnSc4d1
působnost	působnost	k1gFnSc4
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
</s>