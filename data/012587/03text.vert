<p>
<s>
Nerez	nerez	k1gFnSc1	nerez
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
folková	folkový	k2eAgFnSc1d1	folková
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
jádro	jádro	k1gNnSc4	jádro
tvořil	tvořit	k5eAaImAgInS	tvořit
autorský	autorský	k2eAgInSc1d1	autorský
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarová	k1gFnSc1	Navarová
–	–	k?	–
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vřešťál	Vřešťál	k1gMnSc1	Vřešťál
–	–	k?	–
Vít	Vít	k1gMnSc1	Vít
Sázavský	sázavský	k2eAgMnSc1d1	sázavský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Začátky	začátek	k1gInPc1	začátek
===	===	k?	===
</s>
</p>
<p>
<s>
Pražští	pražský	k2eAgMnPc1d1	pražský
vysokoškoláci	vysokoškolák	k1gMnPc1	vysokoškolák
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vřešťál	Vřešťál	k1gMnSc1	Vřešťál
a	a	k8xC	a
Vít	Vít	k1gMnSc1	Vít
Sázavský	sázavský	k2eAgMnSc1d1	sázavský
spolu	spolu	k6eAd1	spolu
tvořili	tvořit	k5eAaImAgMnP	tvořit
folkové	folkový	k2eAgNnSc4d1	folkové
duo	duo	k1gNnSc4	duo
hrající	hrající	k2eAgFnSc2d1	hrající
humorné	humorný	k2eAgFnSc2d1	humorná
písně	píseň	k1gFnSc2	píseň
(	(	kIx(	(
<g/>
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
soutěž	soutěž	k1gFnSc4	soutěž
satirických	satirický	k2eAgFnPc2d1	satirická
písní	píseň	k1gFnPc2	píseň
Příbramský	příbramský	k2eAgMnSc1d1	příbramský
permoník	permoník	k1gMnSc1	permoník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
seznámili	seznámit	k5eAaPmAgMnP	seznámit
se	s	k7c7	s
Zuzanou	Zuzana	k1gFnSc7	Zuzana
Navarovou	Navarová	k1gFnSc7	Navarová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosud	dosud	k6eAd1	dosud
zpívala	zpívat	k5eAaImAgFnS	zpívat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Výlety	výlet	k1gInPc4	výlet
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
Navarové	Navarové	k2eAgInSc6d1	Navarové
ze	z	k7c2	z
studijní	studijní	k2eAgFnSc2d1	studijní
stáže	stáž	k1gFnSc2	stáž
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
Nerez	nerez	k1gFnSc4	nerez
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
Nerez	nerez	k1gInSc1	nerez
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Portu	porta	k1gFnSc4	porta
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
a	a	k8xC	a
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarová	k1gFnSc1	Navarová
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
vokální	vokální	k2eAgInSc4d1	vokální
projev	projev	k1gInSc4	projev
na	na	k7c6	na
Vokalíze	vokalíza	k1gFnSc6	vokalíza
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
skupina	skupina	k1gFnSc1	skupina
opět	opět	k6eAd1	opět
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Portu	porta	k1gFnSc4	porta
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
vítězství	vítězství	k1gNnSc4	vítězství
na	na	k7c6	na
Vokalíze	vokalíza	k1gFnSc6	vokalíza
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
článek	článek	k1gInSc4	článek
Vokalíza	vokalíza	k1gFnSc1	vokalíza
značky	značka	k1gFnSc2	značka
Nerez	nerez	k1gFnSc1	nerez
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Melodie	melodie	k1gFnSc2	melodie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovina	polovina	k1gFnSc1	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
Nerezu	nerez	k1gInSc2	nerez
první	první	k4xOgFnSc2	první
LP	LP	kA	LP
<g/>
,	,	kIx,	,
Masopust	masopust	k1gInSc1	masopust
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vydal	vydat	k5eAaPmAgMnS	vydat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vřešťál	Vřešťál	k1gMnSc1	Vřešťál
autorské	autorský	k2eAgNnSc4d1	autorské
EP	EP	kA	EP
Písničky	písnička	k1gFnSc2	písnička
ze	z	k7c2	z
šuplíku	šuplíku	k?	šuplíku
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
druhé	druhý	k4xOgNnSc1	druhý
řadové	řadový	k2eAgNnSc1d1	řadové
album	album	k1gNnSc1	album
Na	na	k7c4	na
vařený	vařený	k2eAgInSc4d1	vařený
nudli	nudle	k1gFnSc6	nudle
<g/>
,	,	kIx,	,
Navarová	Navarový	k2eAgNnPc4d1	Navarový
se	s	k7c7	s
Sázavským	sázavský	k2eAgNnSc7d1	Sázavské
napsali	napsat	k5eAaPmAgMnP	napsat
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
animovanému	animovaný	k2eAgInSc3d1	animovaný
filmu	film	k1gInSc3	film
Michaely	Michaela	k1gFnSc2	Michaela
Pavlátové	Pavlátový	k2eAgFnSc2d1	Pavlátová
Křížovka	křížovka	k1gFnSc1	křížovka
a	a	k8xC	a
Vřešťál	Vřešťál	k1gInSc1	Vřešťál
produkoval	produkovat	k5eAaImAgInS	produkovat
EP	EP	kA	EP
Drobné	drobná	k1gFnSc2	drobná
skladby	skladba	k1gFnPc4	skladba
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
12	[number]	k4	12
drobných	drobný	k2eAgFnPc2d1	drobná
písniček	písnička	k1gFnPc2	písnička
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
kratší	krátký	k2eAgFnSc1d2	kratší
než	než	k8xS	než
minuta	minuta	k1gFnSc1	minuta
<g/>
)	)	kIx)	)
od	od	k7c2	od
různých	různý	k2eAgMnPc2d1	různý
autorů	autor	k1gMnPc2	autor
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Burian	Burian	k1gMnSc1	Burian
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Merta	Merta	k1gMnSc1	Merta
<g/>
,	,	kIx,	,
Bratři	bratr	k1gMnPc1	bratr
Ebenové	ebenový	k2eAgFnSc2d1	ebenová
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
členové	člen	k1gMnPc1	člen
Nerezu	nerez	k1gInSc2	nerez
pořádali	pořádat	k5eAaImAgMnP	pořádat
festival	festival	k1gInSc4	festival
Stará	starý	k2eAgFnSc1d1	stará
láska	láska	k1gFnSc1	láska
Nerez	nerez	k1gFnSc1	nerez
a	a	k8xC	a
Vy	vy	k3xPp2nPc1	vy
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
si	se	k3xPyFc3	se
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
zvali	zvát	k5eAaImAgMnP	zvát
spřátelené	spřátelený	k2eAgMnPc4d1	spřátelený
muzikanty	muzikant	k1gMnPc4	muzikant
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
koncertovali	koncertovat	k5eAaImAgMnP	koncertovat
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Začátek	začátek	k1gInSc1	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
třetí	třetí	k4xOgFnSc4	třetí
(	(	kIx(	(
<g/>
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
)	)	kIx)	)
řadové	řadový	k2eAgNnSc1d1	řadové
LP	LP	kA	LP
Ke	k	k7c3	k
zdi	zeď	k1gFnSc3	zeď
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
vydali	vydat	k5eAaPmAgMnP	vydat
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
nákladu	náklad	k1gInSc6	náklad
1	[number]	k4	1
800	[number]	k4	800
kusů	kus	k1gInPc2	kus
demokazetu	demokazet	k1gInSc2	demokazet
Co	co	k8xS	co
se	se	k3xPyFc4	se
nevešlo	vejít	k5eNaPmAgNnS	vejít
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
soustředili	soustředit	k5eAaPmAgMnP	soustředit
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dosud	dosud	k6eAd1	dosud
nevyšly	vyjít	k5eNaPmAgFnP	vyjít
<g/>
,	,	kIx,	,
v	v	k7c6	v
"	"	kIx"	"
<g/>
live	live	k1gFnSc6	live
<g/>
"	"	kIx"	"
verzích	verze	k1gFnPc6	verze
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Navarová	Navarová	k1gFnSc1	Navarová
se	s	k7c7	s
Sázavským	sázavský	k2eAgInSc7d1	sázavský
znovu	znovu	k6eAd1	znovu
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
s	s	k7c7	s
Michaelou	Michaela	k1gFnSc7	Michaela
Pavlátovou	Pavlátův	k2eAgFnSc7d1	Pavlátova
<g/>
,	,	kIx,	,
složili	složit	k5eAaPmAgMnP	složit
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
Řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
řeči	řeč	k1gFnSc2	řeč
<g/>
...	...	k?	...
nominovanému	nominovaný	k2eAgNnSc3d1	nominované
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
přišla	přijít	k5eAaPmAgFnS	přijít
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarová	k1gFnSc1	Navarová
se	s	k7c7	s
sólovým	sólový	k2eAgInSc7d1	sólový
projektem	projekt	k1gInSc7	projekt
Caribe	Carib	k1gMnSc5	Carib
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
živé	živé	k1gNnSc1	živé
CD	CD	kA	CD
Stará	starat	k5eAaImIp3nS	starat
láska	láska	k1gFnSc1	láska
Nerez	nerez	k1gFnSc1	nerez
a	a	k8xC	a
vy	vy	k3xPp2nPc1	vy
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
je	být	k5eAaImIp3nS	být
záznamem	záznam	k1gInSc7	záznam
koncertu	koncert	k1gInSc2	koncert
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
kromě	kromě	k7c2	kromě
hostování	hostování	k1gNnSc2	hostování
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Nejezchleby	Nejezchleb	k1gMnPc7	Nejezchleb
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
koncert	koncert	k1gInSc4	koncert
samotného	samotný	k2eAgNnSc2d1	samotné
hlavního	hlavní	k2eAgNnSc2d1	hlavní
tria	trio	k1gNnSc2	trio
<g/>
.	.	kIx.	.
</s>
<s>
Nerez	nerez	k1gFnSc1	nerez
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
nahráli	nahrát	k5eAaPmAgMnP	nahrát
album	album	k1gNnSc4	album
koled	koleda	k1gFnPc2	koleda
Nerez	nerez	k2eAgFnPc2d1	nerez
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
<g/>
.	.	kIx.	.
</s>
<s>
Uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
ještě	ještě	k6eAd1	ještě
jedno	jeden	k4xCgNnSc4	jeden
větší	veliký	k2eAgNnSc4d2	veliký
turné	turné	k1gNnSc4	turné
a	a	k8xC	a
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
Lucerně	lucerna	k1gFnSc6	lucerna
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hostovalo	hostovat	k5eAaImAgNnS	hostovat
mnoho	mnoho	k4c1	mnoho
špičkových	špičkový	k2eAgMnPc2d1	špičkový
rockových	rockový	k2eAgMnPc2d1	rockový
a	a	k8xC	a
jazzových	jazzový	k2eAgMnPc2d1	jazzový
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vřešťál	Vřešťál	k1gMnSc1	Vřešťál
začal	začít	k5eAaPmAgMnS	začít
připravovat	připravovat	k5eAaImF	připravovat
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
Čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
víc	hodně	k6eAd2	hodně
Vřešťál	Vřešťál	k1gInSc1	Vřešťál
a	a	k8xC	a
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarová	k1gFnSc1	Navarová
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ivánem	Iván	k1gInSc7	Iván
Gutiérrezem	Gutiérrez	k1gInSc7	Gutiérrez
a	a	k8xC	a
Karlem	Karel	k1gMnSc7	Karel
Cábou	Cába	k1gMnSc7	Cába
trio	trio	k1gNnSc4	trio
Tres	tresa	k1gFnPc2	tresa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
činnosti	činnost	k1gFnSc2	činnost
skupiny	skupina	k1gFnSc2	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vyšly	vyjít	k5eAaPmAgFnP	vyjít
v	v	k7c6	v
reedici	reedice	k1gFnSc6	reedice
na	na	k7c4	na
CD	CD	kA	CD
tři	tři	k4xCgFnPc4	tři
řadová	řadový	k2eAgFnSc1d1	řadová
LP	LP	kA	LP
Nerezu	nerez	k1gInSc2	nerez
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Nerez	nerez	k2eAgFnSc2d1	nerez
–	–	k?	–
antologie	antologie	k1gFnSc2	antologie
doplněné	doplněná	k1gFnSc2	doplněná
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
bonusy	bonus	k1gInPc4	bonus
<g/>
.	.	kIx.	.
</s>
<s>
Tres	tresa	k1gFnPc2	tresa
vydali	vydat	k5eAaPmAgMnP	vydat
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
CD	CD	kA	CD
a	a	k8xC	a
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vřešťál	Vřešťál	k1gMnSc1	Vřešťál
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
producent	producent	k1gMnSc1	producent
s	s	k7c7	s
Jaromírem	Jaromír	k1gMnSc7	Jaromír
Nohavicou	Nohavica	k1gMnSc7	Nohavica
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
spolupráce	spolupráce	k1gFnSc1	spolupráce
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
Nohavicovým	Nohavicový	k2eAgNnSc7d1	Nohavicový
albem	album	k1gNnSc7	album
Divné	divný	k2eAgFnSc2d1	divná
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
;	;	kIx,	;
aranže	aranže	k1gFnSc1	aranže
Vít	Vít	k1gMnSc1	Vít
Sázavský	sázavský	k2eAgMnSc1d1	sázavský
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytvořením	vytvoření	k1gNnSc7	vytvoření
Kapely	kapela	k1gFnSc2	kapela
(	(	kIx(	(
<g/>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vřešťál	Vřešťál	k1gMnSc1	Vřešťál
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
Sázavský	sázavský	k2eAgMnSc1d1	sázavský
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Plánka	plánka	k1gFnSc1	plánka
<g/>
,	,	kIx,	,
Filip	Filip	k1gMnSc1	Filip
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
Vlaďka	Vlaďka	k1gFnSc1	Vlaďka
Hořovská	Hořovská	k1gFnSc1	Hořovská
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
Pukovcová	Pukovcový	k2eAgFnSc1d1	Pukovcová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Nohavicu	Nohavica	k1gFnSc4	Nohavica
začala	začít	k5eAaPmAgFnS	začít
doprovázet	doprovázet	k5eAaImF	doprovázet
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Zaznám	Zaznat	k5eAaImIp1nS	Zaznat
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
vyšel	vyjít	k5eAaPmAgInS	vyjít
na	na	k7c6	na
CD	CD	kA	CD
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
a	a	k8xC	a
Kapela	kapela	k1gFnSc1	kapela
–	–	k?	–
Koncert	koncert	k1gInSc1	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
opět	opět	k6eAd1	opět
Nohavica	Nohavica	k1gMnSc1	Nohavica
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
sám	sám	k3xTgMnSc1	sám
s	s	k7c7	s
kytarou	kytara	k1gFnSc7	kytara
<g/>
,	,	kIx,	,
z	z	k7c2	z
Kapely	kapela	k1gFnSc2	kapela
ale	ale	k8xC	ale
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
skupina	skupina	k1gFnSc1	skupina
Neřež	řezat	k5eNaImRp2nS	řezat
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
písně	píseň	k1gFnPc4	píseň
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Vřeštála	Vřeštál	k1gMnSc2	Vřeštál
včetně	včetně	k7c2	včetně
několika	několik	k4yIc2	několik
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
repertoáru	repertoár	k1gInSc2	repertoár
Nerezu	nerez	k1gInSc2	nerez
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
CD	CD	kA	CD
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
nevešlo	vejít	k5eNaPmAgNnS	vejít
(	(	kIx(	(
<g/>
pozdní	pozdní	k2eAgInSc1d1	pozdní
sběr	sběr	k1gInSc1	sběr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c6	na
základech	základ	k1gInPc6	základ
kazety	kazeta	k1gFnSc2	kazeta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
digitálním	digitální	k2eAgNnSc7d1	digitální
odstraněním	odstranění	k1gNnSc7	odstranění
šumů	šum	k1gInPc2	šum
<g/>
.	.	kIx.	.
</s>
<s>
Neřež	řezat	k5eNaImRp2nS	řezat
odehrál	odehrát	k5eAaPmAgMnS	odehrát
několik	několik	k4yIc4	několik
vzpomínkových	vzpomínkový	k2eAgInPc2d1	vzpomínkový
koncertů	koncert	k1gInPc2	koncert
se	s	k7c7	s
Zuzanou	Zuzana	k1gFnSc7	Zuzana
Navarovou	Navarová	k1gFnSc7	Navarová
a	a	k8xC	a
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
vyšel	vyjít	k5eAaPmAgInS	vyjít
výběr	výběr	k1gInSc1	výběr
Nej	Nej	k1gFnSc2	Nej
<g/>
,	,	kIx,	,
nej	nej	k?	nej
<g/>
,	,	kIx,	,
nej	nej	k?	nej
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Zuzany	Zuzana	k1gFnSc2	Zuzana
Navarové	Navarový	k2eAgFnSc2d1	Navarová
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
Neřež	řezat	k5eNaImRp2nS	řezat
občas	občas	k6eAd1	občas
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
s	s	k7c7	s
koncertním	koncertní	k2eAgInSc7d1	koncertní
programem	program	k1gInSc7	program
Neřež	řezat	k5eNaImRp2nS	řezat
hraje	hrát	k5eAaImIp3nS	hrát
Nerez	nerez	k2eAgFnPc2d1	nerez
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2007	[number]	k4	2007
vydalo	vydat	k5eAaPmAgNnS	vydat
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Supraphon	supraphon	k1gInSc1	supraphon
reedici	reedice	k1gFnSc6	reedice
všech	všecek	k3xTgFnPc2	všecek
tří	tři	k4xCgFnPc2	tři
studiových	studiový	k2eAgFnPc2d1	studiová
alb	alba	k1gFnPc2	alba
Nerezu	nerez	k1gInSc2	nerez
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
masteringem	mastering	k1gInSc7	mastering
a	a	k8xC	a
DVD	DVD	kA	DVD
s	s	k7c7	s
klipy	klip	k1gInPc7	klip
a	a	k8xC	a
archivními	archivní	k2eAgInPc7d1	archivní
záběry	záběr	k1gInPc7	záběr
doplněné	doplněný	k2eAgFnSc2d1	doplněná
zpěvníkem	zpěvník	k1gInSc7	zpěvník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vřešťál	Vřešťál	k1gMnSc1	Vřešťál
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
píše	psát	k5eAaImIp3nS	psát
podrobnou	podrobný	k2eAgFnSc4d1	podrobná
knihu	kniha	k1gFnSc4	kniha
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
skupiny	skupina	k1gFnSc2	skupina
Nerez	nerez	k2eAgFnSc2d1	nerez
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Ne	ne	k9	ne
<g/>
,	,	kIx,	,
Nerez	nerez	k1gFnSc1	nerez
nerezne	reznout	k5eNaPmIp3nS	reznout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reunion	Reunion	k1gInSc4	Reunion
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
oznámili	oznámit	k5eAaPmAgMnP	oznámit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vřeštál	Vřeštál	k1gMnSc1	Vřeštál
a	a	k8xC	a
Vít	Vít	k1gMnSc1	Vít
Sázavský	sázavský	k2eAgMnSc1d1	sázavský
<g/>
,	,	kIx,	,
že	že	k8xS	že
obnoví	obnovit	k5eAaPmIp3nS	obnovit
skupinu	skupina	k1gFnSc4	skupina
Nerez	nerez	k2eAgFnSc4d1	nerez
<g/>
.	.	kIx.	.
</s>
<s>
Náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c4	za
zesnulou	zesnulý	k2eAgFnSc4d1	zesnulá
Zuzanu	Zuzana	k1gFnSc4	Zuzana
Navarovou	Navarová	k1gFnSc4	Navarová
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
Lucia	Lucia	k1gFnSc1	Lucia
Šoralová	Šoralový	k2eAgFnSc1d1	Šoralová
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
zvaný	zvaný	k2eAgInSc1d1	zvaný
Nerez	nerez	k1gInSc1	nerez
&	&	k?	&
Lucia	Lucia	k1gFnSc1	Lucia
poběží	poběžet	k5eAaPmIp3nS	poběžet
paralelně	paralelně	k6eAd1	paralelně
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Neřež	řezat	k5eNaImRp2nS	řezat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarová	k1gFnSc1	Navarová
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnSc1	perkuse
<g/>
,	,	kIx,	,
triangl	triangl	k1gInSc1	triangl
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
melodie	melodie	k1gFnPc1	melodie
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vřešťál	Vřešťál	k1gMnSc1	Vřešťál
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
melodie	melodie	k1gFnPc1	melodie
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Vít	Vít	k1gMnSc1	Vít
Sázavský	sázavský	k2eAgMnSc1d1	sázavský
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
aranžmá	aranžmá	k1gNnSc1	aranžmá
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Kolář	Kolář	k1gMnSc1	Kolář
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnSc1	perkuse
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vytiska	Vytiska	k1gFnSc1	Vytiska
<g/>
,	,	kIx,	,
kontrabas	kontrabas	k1gInSc1	kontrabas
1982	[number]	k4	1982
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Bratrych	Bratrych	k1gMnSc1	Bratrych
<g/>
,	,	kIx,	,
saxofon	saxofon	k1gInSc1	saxofon
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Plánka	plánka	k1gFnSc1	plánka
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgFnSc1d1	bicí
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Raba	rab	k1gMnSc2	rab
<g/>
,	,	kIx,	,
kontrabas	kontrabas	k1gInSc1	kontrabas
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Porta	porta	k1gFnSc1	porta
'	'	kIx"	'
<g/>
83	[number]	k4	83
(	(	kIx(	(
<g/>
různí	různý	k2eAgMnPc1d1	různý
interpreti	interpret	k1gMnPc1	interpret
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dostavník	dostavník	k1gInSc1	dostavník
21	[number]	k4	21
<g/>
:	:	kIx,	:
Tisíc	tisíc	k4xCgInSc1	tisíc
dnů	den	k1gInPc2	den
mezi	mezi	k7c7	mezi
námi	my	k3xPp1nPc7	my
/	/	kIx~	/
Za	za	k7c4	za
poledne	poledne	k1gNnSc4	poledne
(	(	kIx(	(
<g/>
SP	SP	kA	SP
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Imaginární	imaginární	k2eAgFnSc1d1	imaginární
hospoda	hospoda	k?	hospoda
(	(	kIx(	(
<g/>
EP	EP	kA	EP
<g/>
;	;	kIx,	;
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Plíhalem	Plíhal	k1gMnSc7	Plíhal
a	a	k8xC	a
Slávkem	Slávek	k1gMnSc7	Slávek
Janouškem	Janoušek	k1gMnSc7	Janoušek
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Masopust	masopust	k1gInSc1	masopust
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vařený	vařený	k2eAgInSc1d1	vařený
nudli	nudle	k1gFnSc3	nudle
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
zdi	zeď	k1gFnSc3	zeď
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
nevešlo	vejít	k5eNaPmAgNnS	vejít
(	(	kIx(	(
<g/>
Nerez	nerez	k1gInSc1	nerez
a	a	k8xC	a
Vy	vy	k3xPp2nPc1	vy
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stará	starý	k2eAgFnSc1d1	stará
láska	láska	k1gFnSc1	láska
Nerez	nerez	k1gFnSc1	nerez
a	a	k8xC	a
vy	vy	k3xPp2nPc1	vy
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nerez	nerez	k1gFnSc1	nerez
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nerez	nerez	k2eAgFnSc1d1	nerez
antologie	antologie	k1gFnSc1	antologie
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
–	–	k?	–
znovu	znovu	k6eAd1	znovu
Masopust	masopust	k1gInSc4	masopust
<g/>
,	,	kIx,	,
Na	na	k7c4	na
vařený	vařený	k2eAgInSc4d1	vařený
nudli	nudle	k1gFnSc3	nudle
a	a	k8xC	a
Ke	k	k7c3	k
zdi	zeď	k1gFnSc3	zeď
(	(	kIx(	(
<g/>
+	+	kIx~	+
bonusy	bonus	k1gInPc1	bonus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
nevešlo	vejít	k5eNaPmAgNnS	vejít
(	(	kIx(	(
<g/>
pozdní	pozdní	k2eAgInSc1d1	pozdní
sběr	sběr	k1gInSc1	sběr
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nej	Nej	k?	Nej
nej	nej	k?	nej
nej	nej	k?	nej
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Smutkům	smutek	k1gInPc3	smutek
na	na	k7c4	na
kabát	kabát	k1gInSc4	kabát
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
–	–	k?	–
výběr	výběr	k1gInSc1	výběr
písní	píseň	k1gFnPc2	píseň
Zuzany	Zuzana	k1gFnSc2	Zuzana
Navarové	Navarová	k1gFnPc1	Navarová
včetně	včetně	k7c2	včetně
několika	několik	k4yIc2	několik
písní	píseň	k1gFnPc2	píseň
Nerezu	nerez	k1gInSc2	nerez
</s>
</p>
<p>
<s>
Do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
dechu	dech	k1gInSc2	dech
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
–	–	k?	–
výběr	výběr	k1gInSc1	výběr
písní	píseň	k1gFnPc2	píseň
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Vřešťála	Vřešťál	k1gMnSc2	Vřešťál
včetně	včetně	k7c2	včetně
několika	několik	k4yIc2	několik
písní	píseň	k1gFnPc2	píseň
Nerezu	nerez	k1gInSc2	nerez
</s>
</p>
<p>
<s>
...	...	k?	...
<g/>
a	a	k8xC	a
bastafidli	bastafidnout	k5eAaPmAgMnP	bastafidnout
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
–	–	k?	–
znovu	znovu	k6eAd1	znovu
Masopust	masopust	k1gInSc4	masopust
<g/>
,	,	kIx,	,
Na	na	k7c4	na
vařený	vařený	k2eAgInSc4d1	vařený
nudli	nudle	k1gFnSc3	nudle
a	a	k8xC	a
Ke	k	k7c3	k
zdi	zeď	k1gFnSc3	zeď
+	+	kIx~	+
bonusy	bonus	k1gInPc1	bonus
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
a	a	k8xC	a
zpěvník	zpěvník	k1gInSc1	zpěvník
</s>
</p>
<p>
<s>
Nerez	nerez	k1gFnSc1	nerez
–	–	k?	–
Neřež	řezat	k5eNaImRp2nS	řezat
1982	[number]	k4	1982
–	–	k?	–
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
–	–	k?	–
výběr	výběr	k1gInSc1	výběr
písní	píseň	k1gFnPc2	píseň
Víta	Vít	k1gMnSc4	Vít
Sázavského	sázavský	k2eAgInSc2d1	sázavský
včetně	včetně	k7c2	včetně
několika	několik	k4yIc2	několik
písní	píseň	k1gFnPc2	píseň
Nerezu	nerez	k1gInSc2	nerez
</s>
</p>
<p>
<s>
Nerez	nerez	k1gFnSc1	nerez
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
/	/	kIx~	/
Koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
Orlové	Orlová	k1gFnSc6	Orlová
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
–	–	k?	–
reedice	reedice	k1gFnSc2	reedice
alba	album	k1gNnSc2	album
Nerez	nerez	k2eAgNnSc2d1	nerez
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
+	+	kIx~	+
dosud	dosud	k6eAd1	dosud
nevydaný	vydaný	k2eNgInSc1d1	nevydaný
koncert	koncert	k1gInSc1	koncert
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
