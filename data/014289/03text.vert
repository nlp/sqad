<s>
Anemograf	anemograf	k1gInSc1
</s>
<s>
Anemograf	anemograf	k1gInSc1
je	být	k5eAaImIp3nS
meteorologický	meteorologický	k2eAgInSc1d1
přístroj	přístroj	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
měření	měření	k1gNnSc3
a	a	k8xC
trvalému	trvalý	k2eAgNnSc3d1
zaznamenávání	zaznamenávání	k1gNnSc3
jak	jak	k8xC,k8xS
rychlosti	rychlost	k1gFnSc2
tak	tak	k6eAd1
i	i	k8xC
směru	směr	k1gInSc2
větru	vítr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
vlastně	vlastně	k9
o	o	k7c4
anemometr	anemometr	k1gInSc4
(	(	kIx(
<g/>
přístroj	přístroj	k1gInSc4
na	na	k7c4
měření	měření	k1gNnSc4
rychlosti	rychlost	k1gFnSc2
větru	vítr	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
doplněn	doplnit	k5eAaPmNgInS
o	o	k7c4
mechanické	mechanický	k2eAgNnSc4d1
zapisovací	zapisovací	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
poháněné	poháněný	k2eAgNnSc1d1
hodinovým	hodinový	k2eAgInSc7d1
strojem	stroj	k1gInSc7
popřípadě	popřípadě	k6eAd1
i	i	k9
o	o	k7c4
snímací	snímací	k2eAgFnSc4d1
elektroniku	elektronika	k1gFnSc4
<g/>
,	,	kIx,
u	u	k7c2
novějších	nový	k2eAgInPc2d2
přístrojů	přístroj	k1gInPc2
též	též	k9
o	o	k7c6
A	A	kA
<g/>
/	/	kIx~
<g/>
D	D	kA
převodník	převodník	k1gInSc1
pro	pro	k7c4
digitální	digitální	k2eAgInSc4d1
záznam	záznam	k1gInSc4
naměřených	naměřený	k2eAgNnPc2d1
dat	datum	k1gNnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInSc4
případný	případný	k2eAgInSc4d1
přenos	přenos	k1gInSc4
do	do	k7c2
paměti	paměť	k1gFnSc2
počítače	počítač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
přístroj	přístroj	k1gInSc1
slouží	sloužit	k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgInPc7d1
přístroji	přístroj	k1gInPc7
a	a	k8xC
vědeckými	vědecký	k2eAgNnPc7d1
zařízeními	zařízení	k1gNnPc7
<g/>
,	,	kIx,
mimo	mimo	k6eAd1
jiné	jiný	k2eAgInPc1d1
<g/>
,	,	kIx,
také	také	k9
ke	k	k7c3
studiu	studio	k1gNnSc3
<g/>
,	,	kIx,
popisu	popis	k1gInSc3
a	a	k8xC
dokumentaci	dokumentace	k1gFnSc3
základních	základní	k2eAgInPc2d1
reálných	reálný	k2eAgInPc2d1
fyzikálních	fyzikální	k2eAgInPc2d1
a	a	k8xC
meteorologických	meteorologický	k2eAgInPc2d1
dějů	děj	k1gInPc2
probíhajících	probíhající	k2eAgInPc2d1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Přístroj	přístroj	k1gInSc4
sestrojil	sestrojit	k5eAaPmAgInS
a	a	k8xC
do	do	k7c2
dnešní	dnešní	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
zdokonalil	zdokonalit	k5eAaPmAgMnS
švýcarský	švýcarský	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
<g/>
,	,	kIx,
meteorolog	meteorolog	k1gMnSc1
<g/>
,	,	kIx,
konstruktér	konstruktér	k1gMnSc1
a	a	k8xC
vynálezce	vynálezce	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
von	von	k7
Wild	Wild	k1gInSc1
(	(	kIx(
<g/>
1833	#num#	k4
<g/>
-	-	kIx~
<g/>
1902	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
https://web.archive.org/web/20080614040020/http://encyklopedie.seznam.cz/heslo/373125-anemograf	https://web.archive.org/web/20080614040020/http://encyklopedie.seznam.cz/heslo/373125-anemograf	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Meteorologie	meteorologie	k1gFnPc1
obory	obor	k1gInPc1
</s>
<s>
aerologie	aerologie	k1gFnSc1
•	•	k?
aeronomie	aeronomie	k1gFnSc2
•	•	k?
bioklimatologie	bioklimatologie	k1gFnSc2
•	•	k?
dynamická	dynamický	k2eAgFnSc1d1
meteorologie	meteorologie	k1gFnSc1
•	•	k?
fyzikální	fyzikální	k2eAgFnPc4d1
meteorologie	meteorologie	k1gFnPc4
•	•	k?
hydrometeorologie	hydrometeorologie	k1gFnSc2
•	•	k?
klimatologie	klimatologie	k1gFnSc2
•	•	k?
meteorologická	meteorologický	k2eAgFnSc1d1
technika	technika	k1gFnSc1
•	•	k?
nauka	nauka	k1gFnSc1
o	o	k7c6
chemismu	chemismus	k1gInSc6
atmosféry	atmosféra	k1gFnSc2
•	•	k?
nauka	nauka	k1gFnSc1
o	o	k7c6
radioaktivitě	radioaktivita	k1gFnSc6
atmosféry	atmosféra	k1gFnSc2
•	•	k?
synoptická	synoptický	k2eAgFnSc1d1
meteorologie	meteorologie	k1gFnSc1
užitá	užitý	k2eAgFnSc1d1
meteorologie	meteorologie	k1gFnSc1
</s>
<s>
agrometeorologie	agrometeorologie	k1gFnSc1
•	•	k?
lesnická	lesnický	k2eAgFnSc1d1
meteorologie	meteorologie	k1gFnSc1
•	•	k?
letecká	letecký	k2eAgFnSc1d1
meteorologie	meteorologie	k1gFnSc1
•	•	k?
lékařská	lékařský	k2eAgFnSc1d1
meteorologie	meteorologie	k1gFnSc1
•	•	k?
námořní	námořní	k2eAgFnSc1d1
meteorologie	meteorologie	k1gFnSc1
•	•	k?
silniční	silniční	k2eAgFnSc1d1
meteorologie	meteorologie	k1gFnSc1
přístroje	přístroj	k1gInSc2
</s>
<s>
aktinometr	aktinometr	k1gInSc1
•	•	k?
anemograf	anemograf	k1gInSc1
•	•	k?
anemometr	anemometr	k1gInSc1
•	•	k?
aneroid	aneroid	k1gInSc1
•	•	k?
barograf	barograf	k1gInSc1
•	•	k?
barometr	barometr	k1gInSc1
•	•	k?
družice	družice	k1gFnSc1
(	(	kIx(
<g/>
METEOSAT	METEOSAT	kA
<g/>
)	)	kIx)
•	•	k?
heliograf	heliograf	k1gInSc1
•	•	k?
profiler	profiler	k1gInSc1
•	•	k?
psychrometr	psychrometr	k1gInSc1
•	•	k?
pyranometr	pyranometr	k1gInSc1
•	•	k?
pyrheliometr	pyrheliometr	k1gInSc1
•	•	k?
radar	radar	k1gInSc1
•	•	k?
raketa	raketa	k1gFnSc1
•	•	k?
sodar	sodar	k1gInSc1
•	•	k?
srážkoměr	srážkoměr	k1gInSc1
•	•	k?
teploměr	teploměr	k1gInSc1
•	•	k?
vlhkoměr	vlhkoměr	k1gInSc1
prvky	prvek	k1gInPc7
</s>
<s>
oblačnost	oblačnost	k1gFnSc1
•	•	k?
rosný	rosný	k2eAgInSc1d1
bod	bod	k1gInSc1
•	•	k?
rychlost	rychlost	k1gFnSc1
větru	vítr	k1gInSc2
•	•	k?
směr	směr	k1gInSc1
větru	vítr	k1gInSc2
•	•	k?
srážky	srážka	k1gFnSc2
•	•	k?
teplota	teplota	k1gFnSc1
•	•	k?
tlak	tlak	k1gInSc1
•	•	k?
vlhkost	vlhkost	k1gFnSc1
vzduchu	vzduch	k1gInSc2
jevy	jev	k1gInPc4
</s>
<s>
anticyklóna	anticyklóna	k1gFnSc1
•	•	k?
blesk	blesk	k1gInSc1
•	•	k?
bouře	bouře	k1gFnSc1
•	•	k?
bouřka	bouřka	k1gFnSc1
•	•	k?
bouřlivý	bouřlivý	k2eAgInSc1d1
příliv	příliv	k1gInSc1
•	•	k?
cyklóna	cyklóna	k1gFnSc1
•	•	k?
déšť	déšť	k1gInSc1
•	•	k?
duha	duha	k1gFnSc1
•	•	k?
sníh	sníh	k1gInSc1
•	•	k?
El	Ela	k1gFnPc2
Niñ	Niñ	k1gMnSc1
•	•	k?
fronta	fronta	k1gFnSc1
(	(	kIx(
<g/>
okluzní	okluzní	k2eAgFnSc1d1
•	•	k?
studená	studený	k2eAgFnSc1d1
•	•	k?
teplá	teplat	k5eAaImIp3nS
<g/>
)	)	kIx)
•	•	k?
změny	změna	k1gFnSc2
klimatu	klima	k1gNnSc2
a	a	k8xC
globální	globální	k2eAgNnSc4d1
oteplování	oteplování	k1gNnSc4
•	•	k?
globální	globální	k2eAgNnSc4d1
stmívání	stmívání	k1gNnSc4
•	•	k?
halové	halový	k2eAgInPc4d1
jevy	jev	k1gInPc4
•	•	k?
jinovatka	jinovatka	k1gFnSc1
•	•	k?
konvekční	konvekční	k2eAgFnPc4d1
bouře	bouř	k1gFnPc4
(	(	kIx(
<g/>
unicela	unicet	k5eAaPmAgFnS,k5eAaImAgFnS
•	•	k?
multicela	multicet	k5eAaImAgFnS,k5eAaPmAgFnS
•	•	k?
supercela	supercet	k5eAaPmAgFnS,k5eAaImAgFnS
<g/>
)	)	kIx)
•	•	k?
kroupy	kroupa	k1gFnSc2
•	•	k?
ledovka	ledovka	k1gFnSc1
•	•	k?
mlha	mlha	k1gFnSc1
•	•	k?
opar	opar	k1gInSc1
•	•	k?
námraza	námraza	k1gFnSc1
•	•	k?
náledí	náledí	k1gNnSc6
•	•	k?
oblak	oblak	k1gInSc1
•	•	k?
počasí	počasí	k1gNnSc2
•	•	k?
rosa	rosa	k1gFnSc1
•	•	k?
severoatlantická	severoatlantický	k2eAgFnSc1d1
oscilace	oscilace	k1gFnSc1
•	•	k?
skleníkový	skleníkový	k2eAgInSc1d1
efekt	efekt	k1gInSc1
•	•	k?
sněhový	sněhový	k2eAgInSc1d1
efekt	efekt	k1gInSc1
vodních	vodní	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
•	•	k?
tornádo	tornádo	k1gNnSc4
•	•	k?
tropická	tropický	k2eAgFnSc1d1
cyklóna	cyklóna	k1gFnSc1
•	•	k?
vítr	vítr	k1gInSc1
informace	informace	k1gFnSc1
</s>
<s>
aerologický	aerologický	k2eAgInSc1d1
diagram	diagram	k1gInSc1
•	•	k?
biozátěž	biozátěž	k1gFnSc1
•	•	k?
družicový	družicový	k2eAgInSc1d1
snímek	snímek	k1gInSc1
•	•	k?
izolinie	izolinie	k1gFnSc1
(	(	kIx(
<g/>
izobara	izobara	k1gFnSc1
•	•	k?
izohumida	izohumida	k1gFnSc1
•	•	k?
izohypsa	izohypsa	k1gFnSc1
•	•	k?
izochora	izochora	k1gFnSc1
•	•	k?
izoterma	izoterma	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
kód	kód	k1gInSc1
(	(	kIx(
<g/>
SYNOP	SYNOP	kA
•	•	k?
TEMP	tempo	k1gNnPc2
•	•	k?
CLIMAT	CLIMAT	kA
•	•	k?
TAF	TAF	kA
•	•	k?
Q	Q	kA
<g/>
)	)	kIx)
•	•	k?
mapa	mapa	k1gFnSc1
•	•	k?
meteogram	meteogram	k1gInSc1
•	•	k?
meteorologická	meteorologický	k2eAgFnSc1d1
výstraha	výstraha	k1gFnSc1
•	•	k?
model	model	k1gInSc4
numerické	numerický	k2eAgFnSc2d1
předpovědi	předpověď	k1gFnSc2
počasí	počasí	k1gNnSc2
(	(	kIx(
<g/>
ALADIN	ALADIN	kA
•	•	k?
MEDARD	Medard	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
předpověď	předpověď	k1gFnSc1
počasí	počasí	k1gNnSc2
•	•	k?
aktivita	aktivita	k1gFnSc1
klíšťat	klíště	k1gNnPc2
•	•	k?
povodňová	povodňový	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
•	•	k?
předpověď	předpověď	k1gFnSc1
počasí	počasí	k1gNnSc2
pro	pro	k7c4
alergiky	alergik	k1gMnPc4
•	•	k?
UV	UV	kA
index	index	k1gInSc1
•	•	k?
vertikální	vertikální	k2eAgInSc1d1
řez	řez	k1gInSc1
atmosférou	atmosféra	k1gFnSc7
•	•	k?
znečištění	znečištění	k1gNnSc1
ovzduší	ovzduší	k1gNnSc2
</s>
