<s>
Přístroj	přístroj	k1gInSc4
sestrojil	sestrojit	k5eAaPmAgInS
a	a	k8xC
do	do	k7c2
dnešní	dnešní	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
zdokonalil	zdokonalit	k5eAaPmAgMnS
švýcarský	švýcarský	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
<g/>
,	,	kIx,
meteorolog	meteorolog	k1gMnSc1
<g/>
,	,	kIx,
konstruktér	konstruktér	k1gMnSc1
a	a	k8xC
vynálezce	vynálezce	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
von	von	k7
Wild	Wild	k1gInSc1
(	(	kIx(
<g/>
1833	#num#	k4
<g/>
-	-	kIx~
<g/>
1902	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>