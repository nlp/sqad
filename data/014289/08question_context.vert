<s>
Anemograf	anemograf	k1gInSc1
je	být	k5eAaImIp3nS
meteorologický	meteorologický	k2eAgInSc1d1
přístroj	přístroj	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
měření	měření	k1gNnSc3
a	a	k8xC
trvalému	trvalý	k2eAgNnSc3d1
zaznamenávání	zaznamenávání	k1gNnSc3
jak	jak	k8xC,k8xS
rychlosti	rychlost	k1gFnSc2
tak	tak	k6eAd1
i	i	k8xC
směru	směr	k1gInSc2
větru	vítr	k1gInSc2
<g/>
.	.	kIx.
</s>