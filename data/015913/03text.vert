<s>
Faradayův	Faradayův	k2eAgInSc1d1
paradox	paradox	k1gInSc1
</s>
<s>
Faraday	Faradaa	k1gFnPc1
paradox	paradox	k1gInSc1
-	-	kIx~
stacionární	stacionární	k2eAgInPc1d1
kartáče	kartáč	k1gInPc1
a	a	k8xC
<g/>
,	,	kIx,
<g/>
b	b	k?
<g/>
;	;	kIx,
stacionární	stacionární	k2eAgInSc1d1
disk	disk	k1gInSc1
c	c	k0
<g/>
;	;	kIx,
rotující	rotující	k2eAgInSc1d1
magnet	magnet	k1gInSc1
d	d	k?
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
působí	působit	k5eAaImIp3nS
na	na	k7c4
disk	disk	k1gInSc4
vektorem	vektor	k1gInSc7
magnetické	magnetický	k2eAgFnSc2d1
indukce	indukce	k1gFnSc2
B	B	kA
</s>
<s>
Faradayův	Faradayův	k2eAgInSc1d1
paradox	paradox	k1gInSc1
se	se	k3xPyFc4
jeví	jevit	k5eAaImIp3nS
z	z	k7c2
pohledu	pohled	k1gInSc2
moderní	moderní	k2eAgFnSc2d1
elektrodynamiky	elektrodynamika	k1gFnSc2
jako	jako	k8xC,k8xS
zcela	zcela	k6eAd1
vyřešený	vyřešený	k2eAgInSc1d1
fenomén	fenomén	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Již	již	k6eAd1
samotný	samotný	k2eAgMnSc1d1
Michael	Michael	k1gMnSc1
Faraday	Faradaa	k1gFnSc2
na	na	k7c4
tento	tento	k3xDgInSc4
problém	problém	k1gInSc4
narazil	narazit	k5eAaPmAgMnS
a	a	k8xC
nikdy	nikdy	k6eAd1
nebyl	být	k5eNaImAgMnS
spokojen	spokojen	k2eAgMnSc1d1
s	s	k7c7
jeho	jeho	k3xOp3gNnSc7
vysvětlením	vysvětlení	k1gNnSc7
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
jako	jako	k9
první	první	k4xOgInSc4
slovně	slovně	k6eAd1
formuloval	formulovat	k5eAaImAgMnS
Faradayův	Faradayův	k2eAgInSc4d1
zákon	zákon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
tento	tento	k3xDgInSc1
zákon	zákon	k1gInSc1
matematicky	matematicky	k6eAd1
popsal	popsat	k5eAaPmAgInS
James	James	k1gInSc1
Clerk	Clerk	k1gInSc1
Maxwell	maxwell	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
laboratorní	laboratorní	k2eAgFnSc2d1
podmínky	podmínka	k1gFnSc2
umožnily	umožnit	k5eAaPmAgInP
provést	provést	k5eAaPmF
jednoduché	jednoduchý	k2eAgInPc1d1
experimenty	experiment	k1gInPc1
s	s	k7c7
vodivým	vodivý	k2eAgInSc7d1
diskem	disk	k1gInSc7
<g/>
,	,	kIx,
kartáči	kartáč	k1gInPc7
a	a	k8xC
magnetem	magnet	k1gInSc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
schematicky	schematicky	k6eAd1
naznačeny	naznačit	k5eAaPmNgInP
ve	v	k7c6
zjednodušené	zjednodušený	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
Faraday	Faradaa	k1gFnSc2
generátoru	generátor	k1gInSc2
na	na	k7c6
obrázku	obrázek	k1gInSc6
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
tři	tři	k4xCgFnPc4
varianty	varianta	k1gFnPc4
vzájemné	vzájemný	k2eAgFnSc2d1
interakce	interakce	k1gFnSc2
disku	disk	k1gInSc2
a	a	k8xC
magnetu	magnet	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
disk	disk	k1gInSc1
s	s	k7c7
kartáči	kartáč	k1gInPc7
jsou	být	k5eAaImIp3nP
stacionární	stacionární	k2eAgInPc1d1
a	a	k8xC
magnet	magnet	k1gInSc1
se	se	k3xPyFc4
otáčí	otáčet	k5eAaImIp3nS
-	-	kIx~
neindukuje	indukovat	k5eNaBmIp3nS
se	se	k3xPyFc4
žádné	žádný	k3yNgNnSc1
napětí	napětí	k1gNnSc1
</s>
<s>
disk	disk	k1gInSc1
se	se	k3xPyFc4
otáčí	otáčet	k5eAaImIp3nS
vůči	vůči	k7c3
stacionárním	stacionární	k2eAgInPc3d1
kartáčům	kartáč	k1gInPc3
a	a	k8xC
magnet	magnet	k1gInSc1
je	být	k5eAaImIp3nS
stacionární	stacionární	k2eAgInSc1d1
-	-	kIx~
indukuje	indukovat	k5eAaBmIp3nS
se	se	k3xPyFc4
napětí	napětí	k1gNnSc1
</s>
<s>
disk	disk	k1gInSc1
se	se	k3xPyFc4
otáčí	otáčet	k5eAaImIp3nS
vůči	vůči	k7c3
stacionárním	stacionární	k2eAgInPc3d1
kartáčům	kartáč	k1gInPc3
a	a	k8xC
magnet	magnet	k1gInSc1
se	se	k3xPyFc4
otáčí	otáčet	k5eAaImIp3nS
s	s	k7c7
diskem	disk	k1gInSc7
-	-	kIx~
indukuje	indukovat	k5eAaBmIp3nS
se	se	k3xPyFc4
napětí	napětí	k1gNnSc1
</s>
<s>
Vysvětlující	vysvětlující	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
dvě	dva	k4xCgFnPc4
linie	linie	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
ta	ten	k3xDgFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zachovává	zachovávat	k5eAaImIp3nS
Faradayův	Faradayův	k2eAgInSc4d1
zákon	zákon	k1gInSc4
v	v	k7c6
neměnné	měnný	k2eNgFnSc6d1,k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
linie	linie	k1gFnSc1
je	být	k5eAaImIp3nS
reprezentována	reprezentovat	k5eAaImNgFnS
moderní	moderní	k2eAgFnSc7d1
teoretickou	teoretický	k2eAgFnSc7d1
elektrodynamikou	elektrodynamika	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
nejlépe	dobře	k6eAd3
prověřenou	prověřený	k2eAgFnSc4d1
disciplínu	disciplína	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opírá	opírat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
relativistické	relativistický	k2eAgNnSc4d1
vysvětlení	vysvětlení	k1gNnSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
se	se	k3xPyFc4
upřednostňuje	upřednostňovat	k5eAaImIp3nS
inerciální	inerciální	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
rámu	rám	k1gInSc2
před	před	k7c7
inerciální	inerciální	k2eAgFnSc7d1
soustavou	soustava	k1gFnSc7
magnetu	magnet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
vysvětlení	vysvětlení	k1gNnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
předpokladu	předpoklad	k1gInSc2
zachování	zachování	k1gNnSc2
kontinuity	kontinuita	k1gFnSc2
magnetického	magnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
pro	pro	k7c4
vnitřní	vnitřní	k2eAgInSc4d1
i	i	k8xC
vnější	vnější	k2eAgInSc4d1
magnetický	magnetický	k2eAgInSc4d1
tok	tok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgNnSc7d1
kritériem	kritérion	k1gNnSc7
je	být	k5eAaImIp3nS
pohyb	pohyb	k1gInSc1
indukčních	indukční	k2eAgFnPc2d1
čar	čára	k1gFnPc2
vůči	vůči	k7c3
vnějšímu	vnější	k2eAgNnSc3d1
nebo	nebo	k8xC
vnitřnímu	vnitřní	k2eAgInSc3d1
obvodu	obvod	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vodič	vodič	k1gInSc1
s	s	k7c7
generátorem	generátor	k1gInSc7
uzavírá	uzavírat	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
pohledu	pohled	k1gInSc2
se	se	k3xPyFc4
o	o	k7c4
žádný	žádný	k3yNgInSc4
paradox	paradox	k1gInSc4
nejedná	jednat	k5eNaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
např.	např.	kA
dle	dle	k7c2
třetí	třetí	k4xOgFnSc2
varianty	varianta	k1gFnSc2
jsou	být	k5eAaImIp3nP
indukční	indukční	k2eAgFnPc1d1
čáry	čára	k1gFnPc1
magnetu	magnet	k1gInSc2
vůči	vůči	k7c3
disku	disk	k1gInSc3
v	v	k7c6
klidu	klid	k1gInSc6
a	a	k8xC
vůči	vůči	k7c3
rámu	rám	k1gInSc3
s	s	k7c7
kartáči	kartáč	k1gInPc7
a	a	k8xC
vodičem	vodič	k1gInSc7
obvodu	obvod	k1gInSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
pohybu	pohyb	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
první	první	k4xOgFnSc2
varianty	varianta	k1gFnSc2
jsou	být	k5eAaImIp3nP
indukční	indukční	k2eAgFnPc1d1
čáry	čára	k1gFnPc1
magnetu	magnet	k1gInSc2
v	v	k7c6
pohybu	pohyb	k1gInSc6
vůči	vůči	k7c3
vnejšímu	vnejší	k2eAgMnSc3d1,k2eAgMnSc3d2
i	i	k8xC
vnitřnímu	vnitřní	k2eAgInSc3d1
vodivému	vodivý	k2eAgInSc3d1
okruhu	okruh	k1gInSc3
-	-	kIx~
dochází	docházet	k5eAaImIp3nS
tak	tak	k6eAd1
k	k	k7c3
rovnováze	rovnováha	k1gFnSc3
EMF	EMF	kA
na	na	k7c6
vnitřní	vnitřní	k2eAgFnSc6d1
i	i	k8xC
vnejší	vnejší	k2eAgFnSc6d1
části	část	k1gFnSc6
generátoru	generátor	k1gInSc2
a	a	k8xC
výsledné	výsledný	k2eAgNnSc4d1
indukované	indukovaný	k2eAgNnSc4d1
napětí	napětí	k1gNnSc4
je	být	k5eAaImIp3nS
nulové	nulový	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
ty	ten	k3xDgInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
Faradayův	Faradayův	k2eAgInSc4d1
zákon	zákon	k1gInSc4
zpochybňují	zpochybňovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těchto	tento	k3xDgFnPc2
teorií	teorie	k1gFnPc2
je	být	k5eAaImIp3nS
minimum	minimum	k1gNnSc1
a	a	k8xC
vznikaly	vznikat	k5eAaImAgFnP
převážně	převážně	k6eAd1
za	za	k7c2
dob	doba	k1gFnPc2
života	život	k1gInSc2
Faradaye	Faraday	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
prosazení	prosazení	k1gNnSc2
Maxwellových	Maxwellův	k2eAgFnPc2d1
rovnic	rovnice	k1gFnPc2
(	(	kIx(
<g/>
zveřejněné	zveřejněný	k2eAgFnPc1d1
roku	rok	k1gInSc2
1865	#num#	k4
<g/>
)	)	kIx)
převládal	převládat	k5eAaImAgInS
názor	názor	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
indukované	indukovaný	k2eAgNnSc1d1
napětí	napětí	k1gNnSc1
je	být	k5eAaImIp3nS
způsobeno	způsobit	k5eAaPmNgNnS
jiskřením	jiskření	k1gNnSc7
kartáčů	kartáč	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
vzniká	vznikat	k5eAaImIp3nS
na	na	k7c6
třecích	třecí	k2eAgFnPc6d1
plochách	plocha	k1gFnPc6
disku	disk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Elektřina	elektřina	k1gFnSc1
</s>
<s>
Magnetismus	magnetismus	k1gInSc1
</s>
<s>
Elektromagnetismus	elektromagnetismus	k1gInSc1
</s>
<s>
Maxwellovy	Maxwellův	k2eAgFnPc1d1
rovnice	rovnice	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fyzika	fyzika	k1gFnSc1
</s>
