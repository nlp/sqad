<s>
Faradayův	Faradayův	k2eAgInSc1d1
paradox	paradox	k1gInSc1
se	se	k3xPyFc4
jeví	jevit	k5eAaImIp3nS
z	z	k7c2
pohledu	pohled	k1gInSc2
moderní	moderní	k2eAgFnSc2d1
elektrodynamiky	elektrodynamika	k1gFnSc2
jako	jako	k8xC,k8xS
zcela	zcela	k6eAd1
vyřešený	vyřešený	k2eAgInSc1d1
fenomén	fenomén	k1gInSc1
<g/>
.	.	kIx.
</s>