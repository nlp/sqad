<p>
<s>
Zvýrazňovač	Zvýrazňovač	k1gInSc1	Zvýrazňovač
je	být	k5eAaImIp3nS	být
psací	psací	k2eAgFnSc1d1	psací
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
zvýraznění	zvýraznění	k1gNnSc3	zvýraznění
napsaného	napsaný	k2eAgInSc2d1	napsaný
nebo	nebo	k8xC	nebo
vytištěného	vytištěný	k2eAgInSc2d1	vytištěný
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Zvýrazňovač	Zvýrazňovač	k1gInSc1	Zvýrazňovač
má	mít	k5eAaImIp3nS	mít
různé	různý	k2eAgFnPc4d1	různá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
zvýrazňovače	zvýrazňovač	k1gInSc2	zvýrazňovač
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
neefektivní	efektivní	k2eNgFnSc1d1	neefektivní
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zvýrazňování	zvýrazňování	k1gNnSc6	zvýrazňování
textu	text	k1gInSc2	text
vytištěného	vytištěný	k2eAgInSc2d1	vytištěný
na	na	k7c6	na
inkoustové	inkoustový	k2eAgFnSc6d1	inkoustová
tiskárně	tiskárna	k1gFnSc6	tiskárna
se	se	k3xPyFc4	se
text	text	k1gInSc1	text
rozmazává	rozmazávat	k5eAaImIp3nS	rozmazávat
<g/>
,	,	kIx,	,
a	a	k8xC	a
zvýrazňovač	zvýrazňovač	k1gInSc1	zvýrazňovač
zanáší	zanášet	k5eAaImIp3nS	zanášet
barvou	barva	k1gFnSc7	barva
inkoustu	inkoust	k1gInSc2	inkoust
<g/>
.	.	kIx.	.
</s>
<s>
Náplň	náplň	k1gFnSc1	náplň
zvýrazňovače	zvýrazňovač	k1gInSc2	zvýrazňovač
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přeměňují	přeměňovat	k5eAaImIp3nP	přeměňovat
ultrafialové	ultrafialový	k2eAgNnSc4d1	ultrafialové
záření	záření	k1gNnSc4	záření
na	na	k7c4	na
viditelné	viditelný	k2eAgNnSc4d1	viditelné
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ho	on	k3xPp3gMnSc4	on
zesilují	zesilovat	k5eAaImIp3nP	zesilovat
<g/>
.	.	kIx.	.
</s>
</p>
