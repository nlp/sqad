<s>
Zvýrazňovač	Zvýrazňovač	k1gInSc1	Zvýrazňovač
je	být	k5eAaImIp3nS	být
psací	psací	k2eAgFnSc1d1	psací
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
zvýraznění	zvýraznění	k1gNnSc3	zvýraznění
napsaného	napsaný	k2eAgInSc2d1	napsaný
nebo	nebo	k8xC	nebo
vytištěného	vytištěný	k2eAgInSc2d1	vytištěný
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
