<s>
Kalliopé	Kalliopé	k1gFnSc1	Kalliopé
měla	mít	k5eAaImAgFnS	mít
syna	syn	k1gMnSc4	syn
Orfea	Orfeus	k1gMnSc4	Orfeus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
thrácký	thrácký	k2eAgMnSc1d1	thrácký
král	král	k1gMnSc1	král
Oiagros	Oiagrosa	k1gFnPc2	Oiagrosa
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
bůh	bůh	k1gMnSc1	bůh
Apollón	Apollón	k1gMnSc1	Apollón
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
Apollónův	Apollónův	k2eAgMnSc1d1	Apollónův
syn	syn	k1gMnSc1	syn
byl	být	k5eAaImAgMnS	být
Ialemos	Ialemos	k1gMnSc1	Ialemos
<g/>
,	,	kIx,	,
pěvec	pěvec	k1gMnSc1	pěvec
truchlivých	truchlivý	k2eAgFnPc2d1	truchlivá
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
mlád	mlád	k2eAgMnSc1d1	mlád
<g/>
.	.	kIx.	.
</s>
