<p>
<s>
Kalliopé	Kalliopé	k1gFnSc1	Kalliopé
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Calliope	Calliop	k1gMnSc5	Calliop
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Κ	Κ	k?	Κ
-	-	kIx~	-
vlastně	vlastně	k9	vlastně
Krasohlasá	Krasohlasý	k2eAgFnSc1d1	Krasohlasý
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
dcera	dcera	k1gFnSc1	dcera
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
boha	bůh	k1gMnSc2	bůh
Dia	Dia	k1gMnSc2	Dia
a	a	k8xC	a
bohyně	bohyně	k1gFnSc2	bohyně
paměti	paměť	k1gFnSc2	paměť
Mnémosyné	Mnémosyná	k1gFnSc2	Mnémosyná
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Múzu	Múza	k1gFnSc4	Múza
epického	epický	k2eAgNnSc2d1	epické
básnictví	básnictví	k1gNnSc2	básnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kalliopé	Kalliopé	k1gFnSc1	Kalliopé
měla	mít	k5eAaImAgFnS	mít
syna	syn	k1gMnSc4	syn
Orfea	Orfeus	k1gMnSc4	Orfeus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
thrácký	thrácký	k2eAgMnSc1d1	thrácký
král	král	k1gMnSc1	král
Oiagros	Oiagrosa	k1gFnPc2	Oiagrosa
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
bůh	bůh	k1gMnSc1	bůh
Apollón	Apollón	k1gMnSc1	Apollón
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
Apollónův	Apollónův	k2eAgMnSc1d1	Apollónův
syn	syn	k1gMnSc1	syn
byl	být	k5eAaImAgMnS	být
Ialemos	Ialemos	k1gMnSc1	Ialemos
<g/>
,	,	kIx,	,
pěvec	pěvec	k1gMnSc1	pěvec
truchlivých	truchlivý	k2eAgFnPc2d1	truchlivá
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
mlád	mlád	k2eAgMnSc1d1	mlád
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Linos	Linos	k1gMnSc1	Linos
byl	být	k5eAaImAgMnS	být
slavný	slavný	k2eAgMnSc1d1	slavný
pěvec	pěvec	k1gMnSc1	pěvec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
neměl	mít	k5eNaImAgMnS	mít
konkurenci	konkurence	k1gFnSc3	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
prý	prý	k9	prý
byl	být	k5eAaImAgMnS	být
učitelem	učitel	k1gMnSc7	učitel
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
samotného	samotný	k2eAgMnSc4d1	samotný
Hérakla	Hérakles	k1gMnSc4	Hérakles
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gNnSc4	on
právě	právě	k9	právě
kvůli	kvůli	k7c3	kvůli
vyučování	vyučování	k1gNnSc4	vyučování
hře	hra	k1gFnSc3	hra
na	na	k7c4	na
lyru	lyra	k1gFnSc4	lyra
zabil	zabít	k5eAaPmAgMnS	zabít
kvůli	kvůli	k7c3	kvůli
častému	častý	k2eAgNnSc3d1	časté
vytýkání	vytýkání	k1gNnSc3	vytýkání
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Jinde	jinde	k6eAd1	jinde
se	se	k3xPyFc4	se
ale	ale	k9	ale
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
zabil	zabít	k5eAaPmAgMnS	zabít
sám	sám	k3xTgMnSc1	sám
bůh	bůh	k1gMnSc1	bůh
Apollón	Apollón	k1gMnSc1	Apollón
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
syn	syn	k1gMnSc1	syn
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
a	a	k8xC	a
zpěvu	zpěv	k1gInSc2	zpěv
předčil	předčít	k5eAaPmAgInS	předčít
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kalliopé	Kalliopé	k1gFnSc1	Kalliopé
je	být	k5eAaImIp3nS	být
i	i	k9	i
matkou	matka	k1gFnSc7	matka
Sirén	Siréna	k1gFnPc2	Siréna
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
uváděny	uvádět	k5eAaImNgFnP	uvádět
i	i	k8xC	i
jiné	jiný	k2eAgFnPc1d1	jiná
Múzy	Múza	k1gFnPc1	Múza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odraz	odraz	k1gInSc4	odraz
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
soch	socha	k1gFnPc2	socha
Kalliopé	Kalliopé	k1gFnSc1	Kalliopé
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Vatikánském	vatikánský	k2eAgNnSc6d1	Vatikánské
muzeu	muzeum	k1gNnSc6	muzeum
(	(	kIx(	(
<g/>
římská	římský	k2eAgFnSc1d1	římská
kopie	kopie	k1gFnSc1	kopie
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
až	až	k9	až
2	[number]	k4	2
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
četné	četný	k2eAgFnPc1d1	četná
další	další	k2eAgFnPc1d1	další
sochy	socha	k1gFnPc1	socha
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
<g/>
,	,	kIx,	,
v	v	k7c6	v
pařížském	pařížský	k2eAgInSc6d1	pařížský
Louvru	Louvre	k1gInSc6	Louvre
a	a	k8xC	a
v	v	k7c6	v
mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
Glyptothéce	Glyptothéka	k1gFnSc6	Glyptothéka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
,	,	kIx,	,
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
</s>
</p>
<p>
<s>
Graves	Graves	k1gMnSc1	Graves
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
Řecké	řecký	k2eAgInPc4d1	řecký
mýty	mýtus	k1gInPc4	mýtus
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7309-153-4	[number]	k4	80-7309-153-4
</s>
</p>
<p>
<s>
Houtzager	Houtzager	k1gInSc1	Houtzager
<g/>
,	,	kIx,	,
Guus	Guus	k1gInSc1	Guus
<g/>
,	,	kIx,	,
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7234-287-8	[number]	k4	80-7234-287-8
</s>
</p>
<p>
<s>
Gerhard	Gerhard	k1gMnSc1	Gerhard
Löwe	Löw	k1gInSc2	Löw
<g/>
,	,	kIx,	,
Heindrich	Heindrich	k1gMnSc1	Heindrich
Alexander	Alexandra	k1gFnPc2	Alexandra
Stoll	Stoll	k1gMnSc1	Stoll
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
Antiky	antika	k1gFnSc2	antika
</s>
</p>
