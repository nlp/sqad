<p>
<s>
Fjodor	Fjodor	k1gInSc1	Fjodor
Fjodorovič	Fjodorovič	k1gInSc1	Fjodorovič
Ušakov	Ušakov	k1gInSc1	Ušakov
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
únorajul	únorajout	k5eAaPmAgInS	únorajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1744	[number]	k4	1744
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
</s>
<s>
Burnakovo	Burnakův	k2eAgNnSc1d1	Burnakův
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1817	[number]	k4	1817
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Alexejevka	Alexejevka	k1gFnSc1	Alexejevka
<g/>
,	,	kIx,	,
tambovská	tambovský	k2eAgFnSc1d1	tambovský
gubernie	gubernie	k1gFnSc1	gubernie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
námořní	námořní	k2eAgMnSc1d1	námořní
admirál	admirál	k1gMnSc1	admirál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
ruského	ruský	k2eAgNnSc2d1	ruské
vojenského	vojenský	k2eAgNnSc2d1	vojenské
námořního	námořní	k2eAgNnSc2d1	námořní
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
průkopníka	průkopník	k1gMnSc2	průkopník
útočné	útočný	k2eAgFnSc2d1	útočná
taktiky	taktika	k1gFnSc2	taktika
námořního	námořní	k2eAgInSc2d1	námořní
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
nezámožné	zámožný	k2eNgFnSc6d1	nezámožná
šlechtické	šlechtický	k2eAgFnSc6d1	šlechtická
rodině	rodina	k1gFnSc6	rodina
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Burnakovo	Burnakův	k2eAgNnSc1d1	Burnakův
v	v	k7c6	v
jaroslavském	jaroslavský	k2eAgInSc6d1	jaroslavský
obvodu	obvod	k1gInSc6	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1766	[number]	k4	1766
ukončil	ukončit	k5eAaPmAgInS	ukončit
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
vojenském	vojenský	k2eAgNnSc6d1	vojenské
námořním	námořní	k2eAgNnSc6d1	námořní
učilišti	učiliště	k1gNnSc6	učiliště
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
sloužit	sloužit	k5eAaImF	sloužit
v	v	k7c6	v
Baltské	baltský	k2eAgFnSc6d1	Baltská
flotě	flota	k1gFnSc6	flota
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1769	[number]	k4	1769
byl	být	k5eAaImAgInS	být
přidělen	přidělit	k5eAaPmNgInS	přidělit
k	k	k7c3	k
Azovské	azovský	k2eAgFnSc3d1	azovský
flotě	flota	k1gFnSc3	flota
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
bojů	boj	k1gInPc2	boj
s	s	k7c7	s
Turky	turek	k1gInPc7	turek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velitelem	velitel	k1gMnSc7	velitel
fregaty	fregata	k1gFnSc2	fregata
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1780	[number]	k4	1780
-	-	kIx~	-
1782	[number]	k4	1782
byl	být	k5eAaImAgMnS	být
kapitánem	kapitán	k1gMnSc7	kapitán
křižníku	křižník	k1gInSc2	křižník
"	"	kIx"	"
<g/>
Viktor	Viktor	k1gMnSc1	Viktor
<g/>
"	"	kIx"	"
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
byla	být	k5eAaImAgFnS	být
ochrana	ochrana	k1gFnSc1	ochrana
ruských	ruský	k2eAgFnPc2d1	ruská
obchodních	obchodní	k2eAgFnPc2d1	obchodní
lodí	loď	k1gFnPc2	loď
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
sloužil	sloužit	k5eAaImAgMnS	sloužit
u	u	k7c2	u
Černomořské	černomořský	k2eAgFnSc2d1	černomořská
floty	flota	k1gFnSc2	flota
<g/>
,	,	kIx,	,
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
výstavbě	výstavba	k1gFnSc6	výstavba
ruské	ruský	k2eAgFnSc2d1	ruská
hlavní	hlavní	k2eAgFnSc2d1	hlavní
černomořské	černomořský	k2eAgFnSc2d1	černomořská
námořní	námořní	k2eAgFnSc2d1	námořní
základny	základna	k1gFnSc2	základna
v	v	k7c6	v
Sevastopolu	Sevastopol	k1gInSc6	Sevastopol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1787	[number]	k4	1787
-	-	kIx~	-
1792	[number]	k4	1792
byl	být	k5eAaImAgMnS	být
kapitánem	kapitán	k1gMnSc7	kapitán
křižníku	křižník	k1gInSc2	křižník
"	"	kIx"	"
<g/>
Sv.	sv.	kA	sv.
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
šesté	šestý	k4xOgFnSc2	šestý
rusko-turecké	ruskourecký	k2eAgFnSc2d1	rusko-turecká
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1788	[number]	k4	1788
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
1	[number]	k4	1
<g/>
.	.	kIx.	.
eskadry	eskadra	k1gFnPc4	eskadra
Černomořské	černomořský	k2eAgFnSc2d1	černomořská
floty	flota	k1gFnSc2	flota
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1789	[number]	k4	1789
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
kontradmirálem	kontradmirál	k1gMnSc7	kontradmirál
a	a	k8xC	a
velitelem	velitel	k1gMnSc7	velitel
celé	celý	k2eAgFnSc2d1	celá
Černomořské	černomořský	k2eAgFnSc2d1	černomořská
floty	flota	k1gFnSc2	flota
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
porazil	porazit	k5eAaPmAgMnS	porazit
v	v	k7c6	v
několika	několik	k4yIc6	několik
bitvách	bitva	k1gFnPc6	bitva
turecké	turecký	k2eAgNnSc1d1	turecké
loďstvo	loďstvo	k1gNnSc1	loďstvo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
viceadmirála	viceadmirál	k1gMnSc4	viceadmirál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1798	[number]	k4	1798
-	-	kIx~	-
1800	[number]	k4	1800
velel	velet	k5eAaImAgInS	velet
eskadře	eskadra	k1gFnSc6	eskadra
černomořského	černomořský	k2eAgNnSc2d1	černomořské
loďstva	loďstvo	k1gNnSc2	loďstvo
<g/>
,	,	kIx,	,
vyslaného	vyslaný	k2eAgInSc2d1	vyslaný
proti	proti	k7c3	proti
francouzské	francouzský	k2eAgFnSc3d1	francouzská
flotile	flotila	k1gFnSc3	flotila
Napoleona	Napoleon	k1gMnSc2	Napoleon
Bonaparta	Bonapart	k1gMnSc2	Bonapart
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
několika	několik	k4yIc2	několik
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
,	,	kIx,	,
dobyl	dobýt	k5eAaPmAgInS	dobýt
pevnost	pevnost	k1gFnSc4	pevnost
Korfu	Korfu	k1gNnSc2	Korfu
a	a	k8xC	a
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
řecké	řecký	k2eAgFnSc2d1	řecká
Republiky	republika	k1gFnSc2	republika
sedmi	sedm	k4xCc2	sedm
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Jónském	jónský	k2eAgNnSc6d1	Jónské
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
osvobozování	osvobozování	k1gNnSc2	osvobozování
italských	italský	k2eAgNnPc2d1	italské
měst	město	k1gNnPc2	město
Ancona	Ancona	k1gFnSc1	Ancona
<g/>
,	,	kIx,	,
Janov	Janov	k1gInSc1	Janov
<g/>
,	,	kIx,	,
Neapol	Neapol	k1gFnSc1	Neapol
a	a	k8xC	a
Řím	Řím	k1gInSc1	Řím
od	od	k7c2	od
francouzské	francouzský	k2eAgFnSc2d1	francouzská
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
tažení	tažení	k1gNnSc2	tažení
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
nečekala	čekat	k5eNaImAgFnS	čekat
sláva	sláva	k1gFnSc1	sláva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
intrikám	intrika	k1gFnPc3	intrika
na	na	k7c6	na
carského	carský	k2eAgInSc2d1	carský
dvoře	dvůr	k1gInSc6	dvůr
ho	on	k3xPp3gNnSc4	on
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
odsunul	odsunout	k5eAaPmAgInS	odsunout
do	do	k7c2	do
druhořadé	druhořadý	k2eAgFnSc2d1	druhořadá
pozice	pozice	k1gFnSc2	pozice
velitele	velitel	k1gMnSc2	velitel
Baltské	baltský	k2eAgFnSc2d1	Baltská
floty	flota	k1gFnSc2	flota
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1807	[number]	k4	1807
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
a	a	k8xC	a
odešel	odejít	k5eAaPmAgInS	odejít
do	do	k7c2	do
penze	penze	k1gFnSc2	penze
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1817	[number]	k4	1817
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Alexejevka	Alexejevka	k1gFnSc1	Alexejevka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
ruského	ruský	k2eAgNnSc2d1	ruské
vojenského	vojenský	k2eAgNnSc2d1	vojenské
námořního	námořní	k2eAgNnSc2d1	námořní
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
průkopníka	průkopník	k1gMnSc2	průkopník
útočné	útočný	k2eAgFnSc2d1	útočná
taktiky	taktika	k1gFnSc2	taktika
námořního	námořní	k2eAgInSc2d1	námořní
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
taktika	taktika	k1gFnSc1	taktika
boje	boj	k1gInSc2	boj
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
i	i	k9	i
známého	známý	k2eAgMnSc4d1	známý
britského	britský	k2eAgMnSc4d1	britský
admirála	admirál	k1gMnSc4	admirál
Horatio	Horatio	k1gMnSc1	Horatio
Nelsona	Nelson	k1gMnSc4	Nelson
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
admirála	admirál	k1gMnSc2	admirál
Ušakova	Ušakov	k1gInSc2	Ušakov
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
několik	několik	k4yIc4	několik
lodí	loď	k1gFnPc2	loď
ruského	ruský	k2eAgNnSc2d1	ruské
a	a	k8xC	a
sovětského	sovětský	k2eAgNnSc2d1	sovětské
vojenského	vojenský	k2eAgNnSc2d1	vojenské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
pojmenovány	pojmenován	k2eAgFnPc1d1	pojmenována
ulice	ulice	k1gFnPc1	ulice
či	či	k8xC	či
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řeckém	řecký	k2eAgInSc6d1	řecký
ostrově	ostrov	k1gInSc6	ostrov
Korfu	Korfu	k1gNnSc2	Korfu
má	mít	k5eAaImIp3nS	mít
admirál	admirál	k1gMnSc1	admirál
Ušakov	Ušakov	k1gInSc1	Ušakov
pomník	pomník	k1gInSc4	pomník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
i	i	k8xC	i
ulice	ulice	k1gFnSc1	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
zřízen	zřídit	k5eAaPmNgInS	zřídit
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
Řád	řád	k1gInSc1	řád
Ušakova	Ušakov	k1gInSc2	Ušakov
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
planetka	planetka	k1gFnSc1	planetka
3010	[number]	k4	3010
Ushakov	Ushakov	k1gInSc1	Ushakov
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2001	[number]	k4	2001
byl	být	k5eAaImAgInS	být
svatořečen	svatořečit	k5eAaBmNgInS	svatořečit
ruskou	ruský	k2eAgFnSc7d1	ruská
pravoslavnou	pravoslavný	k2eAgFnSc7d1	pravoslavná
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
jej	on	k3xPp3gMnSc4	on
patriarcha	patriarcha	k1gMnSc1	patriarcha
Alexij	Alexij	k1gMnSc1	Alexij
II	II	kA	II
<g/>
.	.	kIx.	.
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
patrona	patron	k1gMnSc4	patron
ruských	ruský	k2eAgInPc2d1	ruský
strategických	strategický	k2eAgInPc2d1	strategický
nukleárních	nukleární	k2eAgInPc2d1	nukleární
bombardérů	bombardér	k1gInPc2	bombardér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fjodor	Fjodor	k1gInSc1	Fjodor
Fjodorovič	Fjodorovič	k1gInSc1	Fjodorovič
Ušakov	Ušakov	k1gInSc4	Ušakov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
