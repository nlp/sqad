<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
z	z	k7c2	z
Vorličné	Vorličný	k2eAgFnSc2d1	Vorličný
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbinus	Balbinus	k1gMnSc1	Balbinus
<g/>
;	;	kIx,	;
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1621	[number]	k4	1621
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1688	[number]	k4	1688
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
literát	literát	k1gMnSc1	literát
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
zeměpisec	zeměpisec	k1gMnSc1	zeměpisec
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jezuita	jezuita	k1gMnSc1	jezuita
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
rekatolizace	rekatolizace	k1gFnPc4	rekatolizace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
vlastenec	vlastenec	k1gMnSc1	vlastenec
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
řadil	řadit	k5eAaImAgMnS	řadit
mezi	mezi	k7c4	mezi
obhájce	obhájce	k1gMnSc4	obhájce
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
osobnosti	osobnost	k1gFnPc4	osobnost
českého	český	k2eAgNnSc2d1	české
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
a	a	k8xC	a
rodový	rodový	k2eAgInSc1d1	rodový
původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Ludvík	Ludvík	k1gMnSc1	Ludvík
Balbín	Balbín	k1gMnSc1	Balbín
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
v	v	k7c6	v
nepříliš	příliš	k6eNd1	příliš
bohaté	bohatý	k2eAgFnSc3d1	bohatá
<g/>
,	,	kIx,	,
kdysi	kdysi	k6eAd1	kdysi
erbovní	erbovní	k2eAgFnSc3d1	erbovní
<g/>
,	,	kIx,	,
královéhradecké	královéhradecký	k2eAgFnSc3d1	Královéhradecká
měšťanské	měšťanský	k2eAgFnSc3d1	měšťanská
rodině	rodina	k1gFnSc3	rodina
jako	jako	k8xS	jako
potomek	potomek	k1gMnSc1	potomek
rytířského	rytířský	k2eAgInSc2d1	rytířský
rodu	rod	k1gInSc2	rod
Balbínů	Balbín	k1gMnPc2	Balbín
z	z	k7c2	z
Vorličné	Vorličný	k2eAgFnSc2d1	Vorličný
<g/>
,	,	kIx,	,
doložené	doložený	k2eAgFnSc2d1	doložená
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
nazývali	nazývat	k5eAaImAgMnP	nazývat
Škvornicové	Škvornicový	k2eAgInPc1d1	Škvornicový
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
přibrali	přibrat	k5eAaPmAgMnP	přibrat
jméno	jméno	k1gNnSc4	jméno
Balbinus	Balbinus	k1gMnSc1	Balbinus
a	a	k8xC	a
po	po	k7c6	po
nabytí	nabytí	k1gNnSc6	nabytí
titulu	titul	k1gInSc2	titul
z	z	k7c2	z
Vorličné	Vorličný	k2eAgFnSc2d1	Vorličný
dle	dle	k7c2	dle
usedlosti	usedlost	k1gFnSc2	usedlost
u	u	k7c2	u
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pojmenování	pojmenování	k1gNnSc1	pojmenování
Balbínové	Balbínová	k1gFnSc2	Balbínová
z	z	k7c2	z
Vorličné	Vorličný	k2eAgFnSc2d1	Vorličný
<g/>
.	.	kIx.	.
<g/>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Vodičkovském	Vodičkovský	k2eAgInSc6d1	Vodičkovský
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
později	pozdě	k6eAd2	pozdě
patřil	patřit	k5eAaImAgInS	patřit
do	do	k7c2	do
komplexu	komplex	k1gInSc2	komplex
jezuitských	jezuitský	k2eAgFnPc2d1	jezuitská
kolejí	kolej	k1gFnPc2	kolej
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nS	tvořit
část	část	k1gFnSc1	část
Nového	Nový	k1gMnSc2	Nový
Adalbertina	Adalbertin	k1gMnSc2	Adalbertin
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc4	dům
prodala	prodat	k5eAaPmAgFnS	prodat
jezuitům	jezuita	k1gMnPc3	jezuita
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1636	[number]	k4	1636
Bohuslavova	Bohuslavův	k2eAgFnSc1d1	Bohuslavova
matka	matka	k1gFnSc1	matka
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Vodičková	Vodičková	k1gFnSc1	Vodičková
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1622	[number]	k4	1622
jeho	jeho	k3xOp3gFnSc4	jeho
otec	otec	k1gMnSc1	otec
Lukáš	Lukáš	k1gMnSc1	Lukáš
Škvornic	Škvornice	k1gFnPc2	Škvornice
náhle	náhle	k6eAd1	náhle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
byl	být	k5eAaImAgMnS	být
vychováván	vychovávat	k5eAaImNgMnS	vychovávat
rodinným	rodinný	k2eAgMnSc7d1	rodinný
přítelem	přítel	k1gMnSc7	přítel
Otou	Ota	k1gMnSc7	Ota
z	z	k7c2	z
Oppersdorfu	Oppersdorf	k1gInSc2	Oppersdorf
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Častolovice	Častolovice	k1gFnSc2	Častolovice
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Matku	matka	k1gFnSc4	matka
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
podporoval	podporovat	k5eAaImAgMnS	podporovat
také	také	k9	také
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
ze	z	k7c2	z
Schonfeldtu	Schonfeldt	k1gInSc2	Schonfeldt
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
hocha	hoch	k1gMnSc2	hoch
při	při	k7c6	při
křtu	křest	k1gInSc6	křest
držel	držet	k5eAaImAgInS	držet
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
Albrecht	Albrecht	k1gMnSc1	Albrecht
Václav	Václav	k1gMnSc1	Václav
Eusebius	Eusebius	k1gMnSc1	Eusebius
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
rovněž	rovněž	k9	rovněž
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
jeho	jeho	k3xOp3gMnSc7	jeho
kmotrem	kmotr	k1gMnSc7	kmotr
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
chlapec	chlapec	k1gMnSc1	chlapec
byl	být	k5eAaImAgMnS	být
Balbín	Balbín	k1gInSc4	Balbín
několikrát	několikrát	k6eAd1	několikrát
vážně	vážně	k6eAd1	vážně
nemocen	nemocen	k2eAgMnSc1d1	nemocen
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
ho	on	k3xPp3gMnSc2	on
(	(	kIx(	(
<g/>
ve	v	k7c6	v
strachu	strach	k1gInSc2	strach
<g/>
)	)	kIx)	)
zaslíbila	zaslíbit	k5eAaPmAgFnS	zaslíbit
staroboleslavské	staroboleslavský	k2eAgFnSc3d1	staroboleslavská
Bohorodičce	Bohorodička	k1gFnSc3	Bohorodička
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
se	se	k3xPyFc4	se
uzdravil	uzdravit	k5eAaPmAgMnS	uzdravit
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
přežil	přežít	k5eAaPmAgMnS	přežít
všech	všecek	k3xTgMnPc2	všecek
šest	šest	k4xCc1	šest
starších	starší	k1gMnPc2	starší
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Zajisté	zajisté	k9	zajisté
i	i	k9	i
tato	tento	k3xDgFnSc1	tento
okolnost	okolnost	k1gFnSc1	okolnost
formovala	formovat	k5eAaImAgFnS	formovat
jeho	jeho	k3xOp3gFnSc4	jeho
povahu	povaha	k1gFnSc4	povaha
<g/>
,	,	kIx,	,
pevnou	pevný	k2eAgFnSc4d1	pevná
víru	víra	k1gFnSc4	víra
a	a	k8xC	a
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
úctu	úcta	k1gFnSc4	úcta
ke	k	k7c3	k
svatým	svatá	k1gFnPc3	svatá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
projevoval	projevovat	k5eAaImAgInS	projevovat
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
četbu	četba	k1gFnSc4	četba
a	a	k8xC	a
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
pramenů	pramen	k1gInPc2	pramen
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
brzy	brzy	k6eAd1	brzy
číst	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
měl	mít	k5eAaImAgMnS	mít
prý	prý	k9	prý
třikrát	třikrát	k6eAd1	třikrát
přečtenou	přečtený	k2eAgFnSc4d1	přečtená
Hájkovu	Hájkův	k2eAgFnSc4d1	Hájkova
Kroniku	kronika	k1gFnSc4	kronika
českou	český	k2eAgFnSc4d1	Česká
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
pobělohorském	pobělohorský	k2eAgNnSc6d1	pobělohorské
katolickém	katolický	k2eAgNnSc6d1	katolické
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
od	od	k7c2	od
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
jezuitskou	jezuitský	k2eAgFnSc4d1	jezuitská
školu	škola	k1gFnSc4	škola
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
studoval	studovat	k5eAaImAgMnS	studovat
u	u	k7c2	u
benediktinů	benediktin	k1gMnPc2	benediktin
v	v	k7c6	v
Broumově	Broumov	k1gInSc6	Broumov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
překvapující	překvapující	k2eAgMnSc1d1	překvapující
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
řad	řada	k1gFnPc2	řada
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
Ježíšova	Ježíšův	k2eAgFnSc1d1	Ježíšova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Předci	předek	k1gMnPc5	předek
===	===	k?	===
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejznámějším	známý	k2eAgInPc3d3	nejznámější
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gMnPc1	jeho
předkové	předek	k1gMnPc1	předek
nepatřili	patřit	k5eNaImAgMnP	patřit
mezi	mezi	k7c4	mezi
bezvýznamné	bezvýznamný	k2eAgFnPc4d1	bezvýznamná
osoby	osoba	k1gFnPc4	osoba
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prapraděd	prapraděd	k1gMnSc1	prapraděd
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
bakalář	bakalář	k1gMnSc1	bakalář
svobodných	svobodný	k2eAgFnPc2d1	svobodná
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
humanistický	humanistický	k2eAgMnSc1d1	humanistický
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
tvorba	tvorba	k1gFnSc1	tvorba
byla	být	k5eAaImAgFnS	být
otištěna	otisknout	k5eAaPmNgFnS	otisknout
ve	v	k7c6	v
Veleslavínově	Veleslavínův	k2eAgInSc6d1	Veleslavínův
kalendáři	kalendář	k1gInSc6	kalendář
historickém	historický	k2eAgInSc6d1	historický
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
městským	městský	k2eAgMnSc7d1	městský
písařem	písař	k1gMnSc7	písař
a	a	k8xC	a
sekretářem	sekretář	k1gMnSc7	sekretář
apelačního	apelační	k2eAgInSc2d1	apelační
soudu	soud	k1gInSc2	soud
pro	pro	k7c4	pro
latinské	latinský	k2eAgFnPc4d1	Latinská
i	i	k8xC	i
české	český	k2eAgFnPc4d1	Česká
expedice	expedice	k1gFnPc4	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
zakladatelem	zakladatel	k1gMnSc7	zakladatel
pražské	pražský	k2eAgFnSc2d1	Pražská
větve	větev	k1gFnSc2	větev
Balbínů	Balbín	k1gMnPc2	Balbín
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1553	[number]	k4	1553
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Filipem	Filip	k1gMnSc7	Filip
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
vladyckého	vladycký	k2eAgInSc2d1	vladycký
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1556	[number]	k4	1556
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
stal	stát	k5eAaPmAgMnS	stát
příslušníkem	příslušník	k1gMnSc7	příslušník
stavu	stav	k1gInSc2	stav
vladyckého	vladycký	k2eAgInSc2d1	vladycký
dědičně	dědičně	k6eAd1	dědičně
<g/>
.	.	kIx.	.
<g/>
Janův	Janův	k2eAgMnSc1d1	Janův
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Filip	Filip	k1gMnSc1	Filip
založil	založit	k5eAaPmAgMnS	založit
královéhradeckou	královéhradecký	k2eAgFnSc4d1	Královéhradecká
větev	větev	k1gFnSc4	větev
Balbínů	Balbín	k1gMnPc2	Balbín
z	z	k7c2	z
Vorličné	Vorličný	k2eAgFnSc2d1	Vorličný
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
malířem	malíř	k1gMnSc7	malíř
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
malby	malba	k1gFnPc4	malba
v	v	k7c6	v
hradeckém	hradecký	k2eAgInSc6d1	hradecký
kancionálu	kancionál	k1gInSc6	kancionál
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
i	i	k9	i
úřad	úřad	k1gInSc4	úřad
hradeckého	hradecký	k2eAgMnSc2d1	hradecký
primátora	primátor	k1gMnSc2	primátor
<g/>
.	.	kIx.	.
<g/>
Balbínův	Balbínův	k2eAgMnSc1d1	Balbínův
pradědeček	pradědeček	k1gMnSc1	pradědeček
Jiří	Jiří	k1gMnSc1	Jiří
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
primátora	primátor	k1gMnSc2	primátor
a	a	k8xC	a
královéhradeckého	královéhradecký	k2eAgMnSc2d1	královéhradecký
královského	královský	k2eAgMnSc2d1	královský
rychtáře	rychtář	k1gMnSc2	rychtář
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1584	[number]	k4	1584
byl	být	k5eAaImAgMnS	být
dědičně	dědičně	k6eAd1	dědičně
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
rytířského	rytířský	k2eAgInSc2d1	rytířský
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
doložen	doložen	k2eAgMnSc1d1	doložen
jako	jako	k8xS	jako
dárce	dárce	k1gMnSc1	dárce
příspěvku	příspěvek	k1gInSc2	příspěvek
na	na	k7c4	na
graduál	graduál	k1gInSc4	graduál
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
podobizna	podobizna	k1gFnSc1	podobizna
<g/>
.	.	kIx.	.
<g/>
Otec	otec	k1gMnSc1	otec
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Balbína	Balbín	k1gInSc2	Balbín
Lukáš	Lukáš	k1gMnSc1	Lukáš
Škvornic	Škvornice	k1gFnPc2	Škvornice
Balbín	Balbín	k1gInSc4	Balbín
z	z	k7c2	z
Vorličné	Vorličný	k2eAgFnSc2d1	Vorličný
na	na	k7c6	na
Petrovicích	Petrovice	k1gFnPc6	Petrovice
byl	být	k5eAaImAgInS	být
správcem	správce	k1gMnSc7	správce
pardubického	pardubický	k2eAgNnSc2d1	pardubické
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1618	[number]	k4	1618
a	a	k8xC	a
1619	[number]	k4	1619
úspěšně	úspěšně	k6eAd1	úspěšně
ubránil	ubránit	k5eAaPmAgInS	ubránit
pardubický	pardubický	k2eAgInSc1d1	pardubický
zámek	zámek	k1gInSc1	zámek
proti	proti	k7c3	proti
povstalým	povstalý	k2eAgInPc3d1	povstalý
stavům	stav	k1gInPc3	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Jiří	Jiří	k1gMnSc1	Jiří
padl	padnout	k5eAaPmAgMnS	padnout
roku	rok	k1gInSc2	rok
1619	[number]	k4	1619
<g/>
,	,	kIx,	,
když	když	k8xS	když
bojoval	bojovat	k5eAaImAgMnS	bojovat
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
císaře	císař	k1gMnSc2	císař
proti	proti	k7c3	proti
Matyáši	Matyáš	k1gMnSc3	Matyáš
Thurnovi	Thurn	k1gMnSc3	Thurn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Studium	studium	k1gNnSc1	studium
==	==	k?	==
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
Broumově	Broumov	k1gInSc6	Broumov
<g/>
,	,	kIx,	,
Jičíně	Jičín	k1gInSc6	Jičín
<g/>
,	,	kIx,	,
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Kladsku	Kladsko	k1gNnSc6	Kladsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nebyla	být	k5eNaImAgFnS	být
častá	častý	k2eAgFnSc1d1	častá
změna	změna	k1gFnSc1	změna
studijního	studijní	k2eAgInSc2d1	studijní
pobytu	pobyt	k1gInSc2	pobyt
ničím	ničí	k3xOyNgInSc7	ničí
výjimečným	výjimečný	k2eAgMnSc7d1	výjimečný
<g/>
,	,	kIx,	,
zvláště	zvláště	k9	zvláště
u	u	k7c2	u
jezuitů	jezuita	k1gMnPc2	jezuita
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zde	zde	k6eAd1	zde
studoval	studovat	k5eAaImAgMnS	studovat
klasické	klasický	k2eAgInPc4d1	klasický
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
,	,	kIx,	,
prohloubil	prohloubit	k5eAaPmAgMnS	prohloubit
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc4	jeho
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
českým	český	k2eAgFnPc3d1	Česká
dějinám	dějiny	k1gFnPc3	dějiny
a	a	k8xC	a
vlasti	vlast	k1gFnSc3	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
byl	být	k5eAaImAgInS	být
Balbín	Balbín	k1gInSc1	Balbín
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
páterem	páter	k1gMnSc7	páter
Mikulášem	Mikuláš	k1gMnSc7	Mikuláš
Leczyckým	Leczycký	k1gMnSc7	Leczycký
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
životopis	životopis	k1gInSc1	životopis
později	pozdě	k6eAd2	pozdě
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
O	o	k7c6	o
prázdninách	prázdniny	k1gFnPc6	prázdniny
pobýval	pobývat	k5eAaImAgInS	pobývat
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
Častolovicích	Častolovice	k1gFnPc6	Častolovice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
Otu	Ota	k1gMnSc4	Ota
z	z	k7c2	z
Oppersdorfu	Oppersdorf	k1gInSc2	Oppersdorf
při	při	k7c6	při
loveckých	lovecký	k2eAgFnPc6d1	lovecká
vycházkách	vycházka	k1gFnPc6	vycházka
a	a	k8xC	a
osvojil	osvojit	k5eAaPmAgMnS	osvojit
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
krásy	krása	k1gFnPc4	krása
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
<g/>
Církevní	církevní	k2eAgFnSc4d1	církevní
dráhu	dráha	k1gFnSc4	dráha
započal	započnout	k5eAaPmAgInS	započnout
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
v	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
novicem	novic	k1gMnSc7	novic
brněnské	brněnský	k2eAgFnSc2d1	brněnská
koleje	kolej	k1gFnSc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
složil	složit	k5eAaPmAgMnS	složit
první	první	k4xOgInPc4	první
řeholní	řeholní	k2eAgInPc4d1	řeholní
sliby	slib	k1gInPc4	slib
v	v	k7c6	v
Tovaryšstvu	tovaryšstvo	k1gNnSc6	tovaryšstvo
Ježíšovu	Ježíšův	k2eAgFnSc4d1	Ježíšova
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
V	v	k7c6	v
osmnácti	osmnáct	k4xCc6	osmnáct
letech	let	k1gInPc6	let
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
filosofii	filosofie	k1gFnSc4	filosofie
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
jezuitské	jezuitský	k2eAgFnSc6d1	jezuitská
koleji	kolej	k1gFnSc6	kolej
Klementinu	Klementina	k1gFnSc4	Klementina
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
v	v	k7c6	v
napjaté	napjatý	k2eAgFnSc6d1	napjatá
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1639	[number]	k4	1639
zažil	zažít	k5eAaPmAgMnS	zažít
drancování	drancování	k1gNnSc3	drancování
Čech	Čechy	k1gFnPc2	Čechy
švédskou	švédský	k2eAgFnSc7d1	švédská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
bakalářského	bakalářský	k2eAgInSc2d1	bakalářský
titulu	titul	k1gInSc2	titul
učil	učit	k5eAaImAgMnS	učit
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
u	u	k7c2	u
sv.	sv.	kA	sv.
Klimenta	Kliment	k1gMnSc4	Kliment
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
pravidel	pravidlo	k1gNnPc2	pravidlo
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Když	když	k8xS	když
Švédové	Švédové	k2eAgInSc2d1	Švédové
roku	rok	k1gInSc2	rok
1642	[number]	k4	1642
plenili	plenit	k5eAaImAgMnP	plenit
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
Balbín	Balbín	k1gMnSc1	Balbín
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
doktorát	doktorát	k1gInSc4	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
provázel	provázet	k5eAaImAgMnS	provázet
po	po	k7c6	po
Čechách	Čechy	k1gFnPc6	Čechy
španělského	španělský	k2eAgMnSc2d1	španělský
církevního	církevní	k2eAgMnSc2d1	církevní
pedagoga	pedagog	k1gMnSc2	pedagog
Rodriga	Rodrig	k1gMnSc2	Rodrig
Arriagu	Arriaga	k1gFnSc4	Arriaga
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
obrany	obrana	k1gFnSc2	obrana
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
hrozil	hrozit	k5eAaImAgInS	hrozit
útok	útok	k1gInSc4	útok
švédských	švédský	k2eAgNnPc2d1	švédské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
se	se	k3xPyFc4	se
Balbín	Balbín	k1gMnSc1	Balbín
ihned	ihned	k6eAd1	ihned
do	do	k7c2	do
akademického	akademický	k2eAgInSc2d1	akademický
praporu	prapor	k1gInSc2	prapor
a	a	k8xC	a
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
svého	svůj	k3xOyFgMnSc2	svůj
profesora	profesor	k1gMnSc2	profesor
<g/>
,	,	kIx,	,
jezuity	jezuita	k1gMnSc2	jezuita
Jiřího	Jiří	k1gMnSc2	Jiří
Plachého	Plachý	k1gMnSc2	Plachý
<g/>
,	,	kIx,	,
udatně	udatně	k6eAd1	udatně
hájil	hájit	k5eAaImAgMnS	hájit
Karlův	Karlův	k2eAgInSc4d1	Karlův
most	most	k1gInSc4	most
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studentském	studentský	k2eAgInSc6d1	studentský
praporu	prapor	k1gInSc6	prapor
bojoval	bojovat	k5eAaImAgMnS	bojovat
také	také	k9	také
malíř	malíř	k1gMnSc1	malíř
Karel	Karel	k1gMnSc1	Karel
Škréta	Škréta	k1gMnSc1	Škréta
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
Jan	Jan	k1gMnSc1	Jan
Marek	Marek	k1gMnSc1	Marek
Marků	Marek	k1gMnPc2	Marek
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
Staroměstské	staroměstský	k2eAgFnSc2d1	Staroměstská
mostecké	mostecký	k2eAgFnSc2d1	Mostecká
věže	věž	k1gFnSc2	věž
byl	být	k5eAaImAgMnS	být
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
vážně	vážně	k6eAd1	vážně
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Literární	literární	k2eAgFnPc1d1	literární
<g/>
,	,	kIx,	,
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
a	a	k8xC	a
misionářská	misionářský	k2eAgFnSc1d1	misionářská
činnost	činnost	k1gFnSc1	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1646	[number]	k4	1646
o	o	k7c6	o
korunovaci	korunovace	k1gFnSc6	korunovace
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
IV	IV	kA	IV
<g/>
.	.	kIx.	.
vydává	vydávat	k5eAaPmIp3nS	vydávat
Balbín	Balbín	k1gMnSc1	Balbín
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
spisek	spisek	k1gInSc4	spisek
Legatio	Legatio	k1gNnSc4	Legatio
Apollinis	Apollinis	k1gFnSc2	Apollinis
coelestis	coelestis	k1gFnSc2	coelestis
ad	ad	k7c4	ad
universitatem	universitat	k1gMnSc7	universitat
Pragensem	Pragenso	k1gNnSc7	Pragenso
etc	etc	k?	etc
<g/>
.	.	kIx.	.
</s>
<s>
Nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
ještě	ještě	k9	ještě
o	o	k7c4	o
dějepisnou	dějepisný	k2eAgFnSc4d1	dějepisná
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
veršované	veršovaný	k2eAgNnSc4d1	veršované
dílo	dílo	k1gNnSc4	dílo
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
soudobého	soudobý	k2eAgInSc2d1	soudobý
humanismu	humanismus	k1gInSc2	humanismus
<g/>
.	.	kIx.	.
</s>
<s>
Balbín	Balbín	k1gMnSc1	Balbín
sice	sice	k8xC	sice
zprvu	zprvu	k6eAd1	zprvu
psal	psát	k5eAaImAgMnS	psát
latinské	latinský	k2eAgFnPc4d1	Latinská
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
i	i	k9	i
později	pozdě	k6eAd2	pozdě
toho	ten	k3xDgMnSc4	ten
nezanechal	zanechat	k5eNaPmAgMnS	zanechat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
nabyly	nabýt	k5eAaPmAgFnP	nabýt
naprosté	naprostý	k2eAgFnPc4d1	naprostá
převahy	převaha	k1gFnPc4	převaha
práce	práce	k1gFnSc2	práce
dějepisné	dějepisný	k2eAgFnSc2d1	dějepisná
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Roku	rok	k1gInSc6	rok
1650	[number]	k4	1650
poslední	poslední	k2eAgInPc4d1	poslední
švédské	švédský	k2eAgInPc4d1	švédský
oddíly	oddíl	k1gInPc4	oddíl
konečně	konečně	k6eAd1	konečně
opustily	opustit	k5eAaPmAgFnP	opustit
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
,	,	kIx,	,
Balbín	Balbín	k1gMnSc1	Balbín
byl	být	k5eAaImAgInS	být
týž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
vysvěcen	vysvětit	k5eAaPmNgMnS	vysvětit
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
<g/>
,	,	kIx,	,
získává	získávat	k5eAaImIp3nS	získávat
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
svobodných	svobodný	k2eAgFnPc2d1	svobodná
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
žádost	žádost	k1gFnSc4	žádost
se	se	k3xPyFc4	se
na	na	k7c4	na
několik	několik	k4yIc4	několik
příštích	příští	k2eAgNnPc2d1	příští
let	léto	k1gNnPc2	léto
stává	stávat	k5eAaImIp3nS	stávat
misionářem	misionář	k1gMnSc7	misionář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Misionářská	misionářský	k2eAgFnSc1d1	misionářská
činnost	činnost	k1gFnSc1	činnost
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
studií	studie	k1gFnPc2	studie
působil	působit	k5eAaImAgInS	působit
Balbín	Balbín	k1gInSc1	Balbín
rok	rok	k1gInSc4	rok
na	na	k7c6	na
Rychnovsku	Rychnovsko	k1gNnSc6	Rychnovsko
a	a	k8xC	a
rok	rok	k1gInSc1	rok
na	na	k7c4	na
Žambersku	Žamberska	k1gFnSc4	Žamberska
jako	jako	k8xS	jako
misionář	misionář	k1gMnSc1	misionář
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
měl	mít	k5eAaImAgInS	mít
pro	pro	k7c4	pro
Církev	církev	k1gFnSc4	církev
získat	získat	k5eAaPmF	získat
do	do	k7c2	do
1500	[number]	k4	1500
mužův	mužův	k2eAgInSc1d1	mužův
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
na	na	k7c4	na
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prý	prý	k9	prý
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
jeho	jeho	k3xOp3gFnSc4	jeho
nadřízení	nadřízený	k1gMnPc1	nadřízený
neshledávali	shledávat	k5eNaImAgMnP	shledávat
horlivost	horlivost	k1gFnSc4	horlivost
ostatních	ostatní	k2eAgMnPc2d1	ostatní
misionářů	misionář	k1gMnPc2	misionář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
činnost	činnost	k1gFnSc1	činnost
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
pedagogické	pedagogický	k2eAgFnSc3d1	pedagogická
činnosti	činnost	k1gFnSc3	činnost
měl	mít	k5eAaImAgInS	mít
Balbín	Balbín	k1gInSc1	Balbín
vynikající	vynikající	k2eAgFnSc2d1	vynikající
schopnosti	schopnost	k1gFnSc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
výborným	výborný	k2eAgMnSc7d1	výborný
a	a	k8xC	a
svědomitým	svědomitý	k2eAgMnSc7d1	svědomitý
učitelem	učitel	k1gMnSc7	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgMnPc6	svůj
žácích	žák	k1gMnPc6	žák
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
podporovat	podporovat	k5eAaImF	podporovat
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
vlasti	vlast	k1gFnSc3	vlast
a	a	k8xC	a
národu	národ	k1gInSc3	národ
<g/>
.	.	kIx.	.
</s>
<s>
Pedagogickou	pedagogický	k2eAgFnSc7d1	pedagogická
činností	činnost	k1gFnSc7	činnost
strávil	strávit	k5eAaPmAgMnS	strávit
celkem	celek	k1gInSc7	celek
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
v	v	k7c6	v
Kladsku	Kladsko	k1gNnSc6	Kladsko
<g/>
,	,	kIx,	,
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Jindřichově	Jindřichův	k2eAgInSc6d1	Jindřichův
Hradci	Hradec	k1gInSc6	Hradec
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
a	a	k8xC	a
Jičíně	Jičín	k1gInSc6	Jičín
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1661	[number]	k4	1661
byl	být	k5eAaImAgMnS	být
zbaven	zbavit	k5eAaPmNgMnS	zbavit
oprávnění	oprávněný	k2eAgMnPc1d1	oprávněný
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
mládeží	mládež	k1gFnSc7	mládež
a	a	k8xC	a
umístěn	umístit	k5eAaPmNgMnS	umístit
do	do	k7c2	do
svérázné	svérázný	k2eAgFnSc2d1	svérázná
izolace	izolace	k1gFnSc2	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
historikové	historik	k1gMnPc1	historik
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
následek	následek	k1gInSc4	následek
nepřátelského	přátelský	k2eNgInSc2d1	nepřátelský
postoje	postoj	k1gInSc2	postoj
jeho	jeho	k3xOp3gMnPc2	jeho
nadřízených	nadřízený	k1gMnPc2	nadřízený
k	k	k7c3	k
Balbínovu	Balbínův	k2eAgNnSc3d1	Balbínovo
vlastenectví	vlastenectví	k1gNnSc3	vlastenectví
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
obavy	obava	k1gFnPc1	obava
jeho	jeho	k3xOp3gMnPc2	jeho
nadřízených	nadřízený	k1gMnPc2	nadřízený
<g/>
,	,	kIx,	,
že	že	k8xS	že
Balbínovy	Balbínův	k2eAgInPc1d1	Balbínův
kroky	krok	k1gInPc1	krok
popuzují	popuzovat	k5eAaImIp3nP	popuzovat
významné	významný	k2eAgMnPc4d1	významný
šlechtice	šlechtic	k1gMnPc4	šlechtic
a	a	k8xC	a
dobrodince	dobrodinec	k1gMnPc4	dobrodinec
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgInPc1d1	dostupný
prameny	pramen	k1gInPc1	pramen
ovšem	ovšem	k9	ovšem
nejsou	být	k5eNaImIp3nP	být
nijak	nijak	k6eAd1	nijak
konkrétní	konkrétní	k2eAgFnPc1d1	konkrétní
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
rozličné	rozličný	k2eAgFnPc1d1	rozličná
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Kučera	Kučera	k1gMnSc1	Kučera
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Rak	rak	k1gMnSc1	rak
vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvodem	důvod	k1gInSc7	důvod
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
Balbínova	Balbínův	k2eAgFnSc1d1	Balbínova
pedofilní	pedofilní	k2eAgFnSc1d1	pedofilní
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
homosexuální	homosexuální	k2eAgFnSc1d1	homosexuální
sexuální	sexuální	k2eAgFnSc1d1	sexuální
orientace	orientace	k1gFnSc1	orientace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlastenectví	vlastenectví	k1gNnSc2	vlastenectví
===	===	k?	===
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
bývá	bývat	k5eAaImIp3nS	bývat
veřejností	veřejnost	k1gFnSc7	veřejnost
nezřídka	nezřídka	k6eAd1	nezřídka
vnímán	vnímat	k5eAaImNgMnS	vnímat
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
havlíčkovských	havlíčkovský	k2eAgFnPc2d1	havlíčkovská
představ	představa	k1gFnPc2	představa
o	o	k7c6	o
jezuitech	jezuita	k1gMnPc6	jezuita
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jako	jako	k9	jako
typického	typický	k2eAgMnSc4d1	typický
katolického	katolický	k2eAgMnSc4d1	katolický
kněze	kněz	k1gMnSc4	kněz
pobělohorské	pobělohorský	k2eAgFnSc2d1	pobělohorská
habsburské	habsburský	k2eAgFnSc2d1	habsburská
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
o	o	k7c4	o
germanizaci	germanizace	k1gFnSc4	germanizace
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Přitom	přitom	k6eAd1	přitom
Balbín	Balbín	k1gMnSc1	Balbín
si	se	k3xPyFc3	se
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
českému	český	k2eAgInSc3d1	český
jazyku	jazyk	k1gInSc3	jazyk
a	a	k8xC	a
kultuře	kultura	k1gFnSc3	kultura
hrozí	hrozit	k5eAaImIp3nP	hrozit
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
bojovat	bojovat	k5eAaImF	bojovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
misionářského	misionářský	k2eAgNnSc2d1	misionářské
a	a	k8xC	a
učitelského	učitelský	k2eAgNnSc2d1	učitelské
působení	působení	k1gNnSc2	působení
sbíral	sbírat	k5eAaImAgInS	sbírat
staré	starý	k2eAgFnPc4d1	stará
české	český	k2eAgFnPc4d1	Česká
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
listiny	listina	k1gFnPc4	listina
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
písemnosti	písemnost	k1gFnPc4	písemnost
nikoli	nikoli	k9	nikoli
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
spálení	spálení	k1gNnSc1	spálení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
dokumentaci	dokumentace	k1gFnSc3	dokumentace
národní	národní	k2eAgFnSc2d1	národní
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Např.	např.	kA	např.
ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
svých	svůj	k3xOyFgNnPc2	svůj
studií	studio	k1gNnPc2	studio
teologie	teologie	k1gFnSc2	teologie
navštívil	navštívit	k5eAaPmAgInS	navštívit
Balbín	Balbín	k1gInSc1	Balbín
královskou	královský	k2eAgFnSc4d1	královská
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
byly	být	k5eAaImAgFnP	být
před	před	k7c7	před
nedávnem	nedávno	k1gNnSc7	nedávno
převezeny	převezen	k2eAgInPc1d1	převezen
rožmberské	rožmberský	k2eAgInPc1d1	rožmberský
rukopisy	rukopis	k1gInPc1	rukopis
z	z	k7c2	z
Třeboně	Třeboň	k1gFnSc2	Třeboň
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
nalezl	naleznout	k5eAaPmAgMnS	naleznout
vzácný	vzácný	k2eAgInSc4d1	vzácný
rukopis	rukopis	k1gInSc4	rukopis
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
Chval	chvála	k1gFnPc2	chvála
Mariánských	mariánský	k2eAgFnPc2d1	Mariánská
(	(	kIx(	(
<g/>
Mariale	Mariala	k1gFnSc3	Mariala
Arnesti	Arnest	k1gFnSc2	Arnest
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
prý	prý	k9	prý
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Arnošt	Arnošt	k1gMnSc1	Arnošt
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Balbín	Balbín	k1gMnSc1	Balbín
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
rukopis	rukopis	k1gInSc1	rukopis
přepíše	přepsat	k5eAaPmIp3nS	přepsat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Opisováním	opisování	k1gNnSc7	opisování
tohoto	tento	k3xDgInSc2	tento
rukopisu	rukopis	k1gInSc2	rukopis
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
i	i	k9	i
v	v	k7c6	v
době	doba	k1gFnSc6	doba
švédského	švédský	k2eAgInSc2d1	švédský
vpádu	vpád	k1gInSc2	vpád
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
královská	královský	k2eAgFnSc1d1	královská
knihovna	knihovna	k1gFnSc1	knihovna
Švédy	švéda	k1gFnSc2	švéda
zničena	zničen	k2eAgFnSc1d1	zničena
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Díky	díky	k7c3	díky
Balbínovi	Balbín	k1gMnSc3	Balbín
se	se	k3xPyFc4	se
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
rukopisů	rukopis	k1gInPc2	rukopis
zde	zde	k6eAd1	zde
uchovávaných	uchovávaný	k2eAgMnPc2d1	uchovávaný
zachránil	zachránit	k5eAaPmAgMnS	zachránit
jen	jen	k9	jen
Mariale	Mariala	k1gFnSc3	Mariala
Arnesti	Arnest	k1gFnSc2	Arnest
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Balbín	Balbín	k1gMnSc1	Balbín
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kolik	kolik	k9	kolik
já	já	k3xPp1nSc1	já
sám	sám	k3xTgMnSc1	sám
zachránil	zachránit	k5eAaPmAgMnS	zachránit
jsem	být	k5eAaImIp1nS	být
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
rukopisů	rukopis	k1gInPc2	rukopis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
již	již	k6eAd1	již
odvezeny	odvezen	k2eAgInPc1d1	odvezen
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
pepř	pepř	k1gInSc4	pepř
židovský	židovský	k2eAgInSc4d1	židovský
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
dějepisné	dějepisný	k2eAgFnSc6d1	dějepisná
oblasti	oblast	k1gFnSc6	oblast
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
žáky	žák	k1gMnPc4	žák
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
učitel	učitel	k1gMnSc1	učitel
byl	být	k5eAaImAgMnS	být
Balbín	Balbín	k1gInSc4	Balbín
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Uznával	uznávat	k5eAaImAgMnS	uznávat
také	také	k9	také
autory	autor	k1gMnPc4	autor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgNnSc3	svůj
vyznání	vyznání	k1gNnSc3	vyznání
museli	muset	k5eAaImAgMnP	muset
emigrovat	emigrovat	k5eAaBmF	emigrovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Pavla	Pavel	k1gMnSc2	Pavel
Stránského	Stránský	k1gMnSc2	Stránský
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
Respublica	Respublica	k1gFnSc1	Respublica
Bojema	Bojema	k?	Bojema
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1645	[number]	k4	1645
v	v	k7c6	v
Třeboňském	třeboňský	k2eAgInSc6d1	třeboňský
archivu	archiv	k1gInSc6	archiv
také	také	k9	také
znovuobjevil	znovuobjevit	k5eAaPmAgMnS	znovuobjevit
významnou	významný	k2eAgFnSc4d1	významná
svatováclavskou	svatováclavský	k2eAgFnSc4d1	Svatováclavská
Kristiánovu	Kristiánův	k2eAgFnSc4d1	Kristiánova
legendu	legenda	k1gFnSc4	legenda
"	"	kIx"	"
<g/>
Život	život	k1gInSc1	život
a	a	k8xC	a
umučení	umučení	k1gNnSc1	umučení
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
babičky	babička	k1gFnSc2	babička
svaté	svatý	k2eAgFnSc2d1	svatá
Ludmily	Ludmila	k1gFnSc2	Ludmila
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Balbínovo	Balbínův	k2eAgNnSc1d1	Balbínovo
vlastenectví	vlastenectví	k1gNnSc1	vlastenectví
se	se	k3xPyFc4	se
nelíbilo	líbit	k5eNaImAgNnS	líbit
nadřízeným	nadřízený	k1gMnPc3	nadřízený
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
napadal	napadat	k5eAaPmAgMnS	napadat
nejvyššího	vysoký	k2eAgMnSc4d3	nejvyšší
zemského	zemský	k2eAgMnSc4d1	zemský
úředníka	úředník	k1gMnSc4	úředník
<g/>
,	,	kIx,	,
místodržitele	místodržitel	k1gMnSc2	místodržitel
Bernarda	Bernard	k1gMnSc2	Bernard
Ignáce	Ignác	k1gMnSc2	Ignác
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Martinic	Martinice	k1gFnPc2	Martinice
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
proto	proto	k8xC	proto
musel	muset	k5eAaImAgInS	muset
později	pozdě	k6eAd2	pozdě
opustit	opustit	k5eAaPmF	opustit
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Bořita	Bořita	k1gFnSc1	Bořita
z	z	k7c2	z
Martinic	Martinice	k1gFnPc2	Martinice
a	a	k8xC	a
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
se	se	k3xPyFc4	se
vskutku	vskutku	k9	vskutku
neměli	mít	k5eNaImAgMnP	mít
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
<g/>
.	.	kIx.	.
</s>
<s>
Bořita	Bořita	k1gFnSc1	Bořita
z	z	k7c2	z
Martinic	Martinice	k1gFnPc2	Martinice
totiž	totiž	k9	totiž
zastavil	zastavit	k5eAaPmAgInS	zastavit
tisk	tisk	k1gInSc1	tisk
Balbínova	Balbínův	k2eAgInSc2d1	Balbínův
Výtahu	výtah	k1gInSc2	výtah
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
národních	národní	k2eAgInPc2d1	národní
<g/>
,	,	kIx,	,
politických	politický	k2eAgInPc2d1	politický
a	a	k8xC	a
dynastických	dynastický	k2eAgInPc2d1	dynastický
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
purkrabí	purkrabí	k1gMnSc1	purkrabí
nebyl	být	k5eNaImAgMnS	být
trnem	trn	k1gInSc7	trn
v	v	k7c6	v
oku	oko	k1gNnSc6	oko
jen	jen	k9	jen
Balbínovi	Balbínův	k2eAgMnPc1d1	Balbínův
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
i	i	k8xC	i
patriotická	patriotický	k2eAgFnSc1d1	patriotická
zemská	zemský	k2eAgFnSc1d1	zemská
aristokracie	aristokracie	k1gFnSc1	aristokracie
Bořitu	Bořit	k1gInSc2	Bořit
obviňovala	obviňovat	k5eAaImAgFnS	obviňovat
ze	z	k7c2	z
zrady	zrada	k1gFnSc2	zrada
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
vytýkáno	vytýkán	k2eAgNnSc1d1	vytýkáno
slabošství	slabošství	k1gNnSc1	slabošství
či	či	k8xC	či
přílišná	přílišný	k2eAgFnSc1d1	přílišná
k	k	k7c3	k
ústupkům	ústupek	k1gInPc3	ústupek
dvoru	dvůr	k1gInSc6	dvůr
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
hospodářských	hospodářský	k2eAgMnPc2d1	hospodářský
a	a	k8xC	a
společenských	společenský	k2eAgInPc2d1	společenský
zájmů	zájem	k1gInPc2	zájem
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dějepisecká	dějepisecký	k2eAgFnSc1d1	dějepisecká
činnost	činnost	k1gFnSc1	činnost
===	===	k?	===
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
dílem	díl	k1gInSc7	díl
jsou	být	k5eAaImIp3nP	být
Rozmanitosti	rozmanitost	k1gFnPc1	rozmanitost
z	z	k7c2	z
historie	historie	k1gFnSc2	historie
království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
omylů	omyl	k1gInPc2	omyl
ovšem	ovšem	k9	ovšem
převzal	převzít	k5eAaPmAgMnS	převzít
od	od	k7c2	od
Hájka	hájka	k1gFnSc1	hájka
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
Kroniky	kronika	k1gFnSc2	kronika
byl	být	k5eAaImAgMnS	být
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jezuité	jezuita	k1gMnPc1	jezuita
mu	on	k3xPp3gMnSc3	on
přikázali	přikázat	k5eAaPmAgMnP	přikázat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sepsal	sepsat	k5eAaPmAgMnS	sepsat
historii	historie	k1gFnSc4	historie
české	český	k2eAgFnSc2d1	Česká
provincie	provincie	k1gFnSc2	provincie
<g/>
,	,	kIx,	,
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
úkolem	úkol	k1gInSc7	úkol
ale	ale	k9	ale
Balbín	Balbín	k1gMnSc1	Balbín
spokojen	spokojen	k2eAgMnSc1d1	spokojen
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
napsal	napsat	k5eAaBmAgInS	napsat
knihu	kniha	k1gFnSc4	kniha
Výtah	výtah	k1gInSc1	výtah
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
českých	český	k2eAgFnPc2d1	Česká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dlouho	dlouho	k6eAd1	dlouho
bojoval	bojovat	k5eAaImAgMnS	bojovat
s	s	k7c7	s
cenzurou	cenzura	k1gFnSc7	cenzura
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
s	s	k7c7	s
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
purkrabím	purkrabí	k1gMnSc7	purkrabí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Výtah	výtah	k1gInSc1	výtah
mohl	moct	k5eAaImAgInS	moct
vyjít	vyjít	k5eAaPmF	vyjít
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
však	však	k9	však
u	u	k7c2	u
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
církevních	církevní	k2eAgFnPc2d1	církevní
a	a	k8xC	a
státních	státní	k2eAgNnPc2d1	státní
míst	místo	k1gNnPc2	místo
nepolepšil	polepšit	k5eNaPmAgInS	polepšit
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
Klatov	Klatovy	k1gInPc2	Klatovy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
vyhnanství	vyhnanství	k1gNnSc3	vyhnanství
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Balbín	Balbín	k1gMnSc1	Balbín
pod	pod	k7c7	pod
ochrannými	ochranný	k2eAgNnPc7d1	ochranné
křídly	křídlo	k1gNnPc7	křídlo
svého	svůj	k3xOyFgMnSc2	svůj
bývalého	bývalý	k2eAgMnSc2d1	bývalý
žáka	žák	k1gMnSc2	žák
Františka	František	k1gMnSc2	František
Oldřicha	Oldřich	k1gMnSc2	Oldřich
hraběte	hrabě	k1gMnSc2	hrabě
Kinského	Kinský	k1gMnSc2	Kinský
<g/>
,	,	kIx,	,
prezidenta	prezident	k1gMnSc2	prezident
nad	nad	k7c7	nad
apelacemi	apelace	k1gFnPc7	apelace
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
byl	být	k5eAaImAgInS	být
Výtah	výtah	k1gInSc1	výtah
poslán	poslat	k5eAaPmNgInS	poslat
ke	k	k7c3	k
zkoumání	zkoumání	k1gNnSc3	zkoumání
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
ke	k	k7c3	k
generálovi	generál	k1gMnSc3	generál
řádu	řád	k1gInSc2	řád
i	i	k9	i
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
k	k	k7c3	k
císaři	císař	k1gMnSc3	císař
Leopoldu	Leopolda	k1gFnSc4	Leopolda
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
Balbínovu	Balbínův	k2eAgFnSc4d1	Balbínova
spisu	spis	k1gInSc2	spis
nakloněn	nakloněn	k2eAgMnSc1d1	nakloněn
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Cenzoři	cenzor	k1gMnPc1	cenzor
Výtahu	výtah	k1gInSc2	výtah
vytýkali	vytýkat	k5eAaImAgMnP	vytýkat
kritiku	kritika	k1gFnSc4	kritika
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
nevděk	nevděk	k1gInSc4	nevděk
vůči	vůči	k7c3	vůči
Jiřímu	Jiří	k1gMnSc3	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
a	a	k8xC	a
pro	pro	k7c4	pro
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
s	s	k7c7	s
Čechy	Čech	k1gMnPc7	Čech
na	na	k7c4	na
vyzvání	vyzvání	k1gNnSc4	vyzvání
papežovo	papežův	k2eAgNnSc4d1	papežovo
vedl	vést	k5eAaImAgMnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
cenzorům	cenzor	k1gMnPc3	cenzor
nelíbila	líbit	k5eNaImAgFnS	líbit
Balbínova	Balbínův	k2eAgFnSc1d1	Balbínova
chvála	chvála	k1gFnSc1	chvála
Boleslava	Boleslav	k1gMnSc2	Boleslav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc4	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
prý	prý	k9	prý
autor	autor	k1gMnSc1	autor
málo	málo	k6eAd1	málo
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
rodě	rod	k1gInSc6	rod
rakouském	rakouský	k2eAgInSc6d1	rakouský
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Balbínovo	Balbínův	k2eAgNnSc1d1	Balbínovo
odsouzení	odsouzení	k1gNnSc1	odsouzení
neúměrného	úměrný	k2eNgNnSc2d1	neúměrné
zvyšování	zvyšování	k1gNnSc2	zvyšování
daní	daň	k1gFnPc2	daň
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
mínění	mínění	k1gNnSc2	mínění
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
cenzorů	cenzor	k1gMnPc2	cenzor
<g/>
,	,	kIx,	,
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Konfliktní	konfliktní	k2eAgInSc1d1	konfliktní
byl	být	k5eAaImAgInS	být
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
výrok	výrok	k1gInSc1	výrok
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
a	a	k8xC	a
králem	král	k1gMnSc7	král
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nepravdu	nepravda	k1gFnSc4	nepravda
psáti	psát	k5eAaImF	psát
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
nenaučil	naučit	k5eNaPmAgMnS	naučit
<g/>
,	,	kIx,	,
pravdu	pravda	k1gFnSc4	pravda
psáti	psát	k5eAaImF	psát
se	se	k3xPyFc4	se
neodvažuji	odvažovat	k5eNaImIp1nS	odvažovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Historicky	historicky	k6eAd1	historicky
je	být	k5eAaImIp3nS	být
Výtah	výtah	k1gInSc1	výtah
tak	tak	k6eAd1	tak
významný	významný	k2eAgMnSc1d1	významný
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
přehledem	přehled	k1gInSc7	přehled
dějin	dějiny	k1gFnPc2	dějiny
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
od	od	k7c2	od
časných	časný	k2eAgFnPc2d1	časná
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
dob	doba	k1gFnPc2	doba
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
Balbínovy	Balbínův	k2eAgFnSc2d1	Balbínova
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Balbín	Balbín	k1gMnSc1	Balbín
tu	tu	k6eAd1	tu
využil	využít	k5eAaPmAgMnS	využít
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
známých	známý	k2eAgFnPc2d1	známá
i	i	k8xC	i
méně	málo	k6eAd2	málo
známějších	známý	k2eAgInPc2d2	známější
českých	český	k2eAgInPc2d1	český
i	i	k8xC	i
cizích	cizí	k2eAgInPc2d1	cizí
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
V	v	k7c6	v
Klatovech	Klatovy	k1gInPc6	Klatovy
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1672	[number]	k4	1672
<g/>
–	–	k?	–
<g/>
73	[number]	k4	73
Balbín	Balbína	k1gFnPc2	Balbína
na	na	k7c4	na
popud	popud	k1gInSc4	popud
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
Tomáše	Tomáš	k1gMnSc2	Tomáš
Pešiny	Pešina	k1gMnSc2	Pešina
z	z	k7c2	z
Čechorodu	Čechorod	k1gInSc2	Čechorod
napsal	napsat	k5eAaPmAgMnS	napsat
Rozpravu	rozprava	k1gFnSc4	rozprava
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
jazyka	jazyk	k1gInSc2	jazyk
slovanského	slovanský	k2eAgInSc2d1	slovanský
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
českého	český	k2eAgNnSc2d1	české
(	(	kIx(	(
<g/>
Dissertatio	Dissertatio	k1gNnSc1	Dissertatio
apologetica	apologetic	k1gInSc2	apologetic
pro	pro	k7c4	pro
lingua	lingu	k2eAgMnSc4d1	lingu
Slavonica	Slavonicus	k1gMnSc4	Slavonicus
<g/>
,	,	kIx,	,
praecipue	praecipu	k1gMnSc4	praecipu
Bohemica	Bohemicus	k1gMnSc4	Bohemicus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
anonymně	anonymně	k6eAd1	anonymně
vydané	vydaný	k2eAgNnSc1d1	vydané
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
latinsky	latinsky	k6eAd1	latinsky
psané	psaný	k2eAgNnSc1d1	psané
dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stalo	stát	k5eAaPmAgNnS	stát
vzorem	vzor	k1gInSc7	vzor
obrozeneckého	obrozenecký	k2eAgNnSc2d1	obrozenecké
úsilí	úsilí	k1gNnSc2	úsilí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jiné	jiný	k2eAgInPc4d1	jiný
Balbínovy	Balbínův	k2eAgInPc4d1	Balbínův
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
pro	pro	k7c4	pro
přílišné	přílišný	k2eAgNnSc4d1	přílišné
vlastenectví	vlastenectví	k1gNnSc4	vlastenectví
i	i	k8xC	i
údajné	údajný	k2eAgInPc4d1	údajný
protivládní	protivládní	k2eAgInPc4d1	protivládní
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Balbín	Balbín	k1gMnSc1	Balbín
zde	zde	k6eAd1	zde
hájí	hájit	k5eAaImIp3nS	hájit
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
vlastní	vlastní	k2eAgInSc4d1	vlastní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
odsuzuje	odsuzovat	k5eAaImIp3nS	odsuzovat
odrodilou	odrodilý	k2eAgFnSc4d1	odrodilá
šlechtu	šlechta	k1gFnSc4	šlechta
a	a	k8xC	a
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
s	s	k7c7	s
vládní	vládní	k2eAgFnSc7d1	vládní
politikou	politika	k1gFnSc7	politika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
český	český	k2eAgInSc4d1	český
jazyk	jazyk	k1gInSc4	jazyk
z	z	k7c2	z
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
škol	škola	k1gFnPc2	škola
i	i	k8xC	i
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
zakončeno	zakončit	k5eAaPmNgNnS	zakončit
modlitbou	modlitba	k1gFnSc7	modlitba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
"	"	kIx"	"
<g/>
svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
nedal	dát	k5eNaPmAgMnS	dát
zahynouti	zahynout	k5eAaPmF	zahynout
nám	my	k3xPp1nPc3	my
i	i	k9	i
budoucím	budoucí	k2eAgMnPc3d1	budoucí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Obrana	obrana	k1gFnSc1	obrana
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
době	doba	k1gFnSc6	doba
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
Františkem	František	k1gMnSc7	František
Martinem	Martin	k1gMnSc7	Martin
Pelclem	Pelcl	k1gMnSc7	Pelcl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
latinsky	latinsky	k6eAd1	latinsky
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
vyšly	vyjít	k5eAaPmAgInP	vyjít
úryvky	úryvek	k1gInPc1	úryvek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
ve	v	k7c6	v
Slovanu	Slovan	k1gMnSc6	Slovan
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Národě	národ	k1gInSc6	národ
<g/>
;	;	kIx,	;
celý	celý	k2eAgInSc4d1	celý
spis	spis	k1gInSc4	spis
vyšel	vyjít	k5eAaPmAgMnS	vyjít
česky	česky	k6eAd1	česky
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Emanuela	Emanuel	k1gMnSc2	Emanuel
Tonnera	Tonner	k1gMnSc2	Tonner
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Obrana	obrana	k1gFnSc1	obrana
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
Balbínův	Balbínův	k2eAgInSc1d1	Balbínův
nejznámější	známý	k2eAgInSc1d3	nejznámější
spis	spis	k1gInSc1	spis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1676	[number]	k4	1676
se	se	k3xPyFc4	se
Balbín	Balbín	k1gMnSc1	Balbín
konečně	konečně	k6eAd1	konečně
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Výtah	výtah	k1gInSc1	výtah
byl	být	k5eAaImAgInS	být
Římem	Řím	k1gInSc7	Řím
shledán	shledat	k5eAaPmNgInS	shledat
nezávadným	závadný	k2eNgInSc7d1	nezávadný
<g/>
,	,	kIx,	,
i	i	k8xC	i
císařský	císařský	k2eAgMnSc1d1	císařský
knihovník	knihovník	k1gMnSc1	knihovník
Petr	Petr	k1gMnSc1	Petr
Lambecius	Lambecius	k1gMnSc1	Lambecius
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
pochvalně	pochvalně	k6eAd1	pochvalně
a	a	k8xC	a
doporučil	doporučit	k5eAaPmAgMnS	doporučit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
již	již	k6eAd1	již
neotálelo	otálet	k5eNaImAgNnS	otálet
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
vydává	vydávat	k5eAaPmIp3nS	vydávat
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
svého	svůj	k3xOyFgNnSc2	svůj
hlavního	hlavní	k2eAgNnSc2d1	hlavní
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc1	který
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
úplně	úplně	k6eAd1	úplně
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
)	)	kIx)	)
–	–	k?	–
Rozmanitosti	rozmanitost	k1gFnPc4	rozmanitost
z	z	k7c2	z
historie	historie	k1gFnSc2	historie
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
(	(	kIx(	(
<g/>
Miscellanea	Miscellaneum	k1gNnSc2	Miscellaneum
historica	historicum	k1gNnSc2	historicum
regni	regn	k1gMnPc1	regn
Bohemiae	Bohemia	k1gFnSc2	Bohemia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
práce	práce	k1gFnSc1	práce
o	o	k7c6	o
českých	český	k2eAgFnPc6d1	Česká
kulturních	kulturní	k2eAgFnPc6d1	kulturní
dějinách	dějiny	k1gFnPc6	dějiny
Učené	učený	k2eAgFnPc1d1	učená
Čechy	Čechy	k1gFnPc1	Čechy
(	(	kIx(	(
<g/>
Bohemia	bohemia	k1gFnSc1	bohemia
docta	docta	k1gFnSc1	docta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Učené	učený	k2eAgFnPc1d1	učená
Čechy	Čechy	k1gFnPc1	Čechy
náleží	náležet	k5eAaImIp3nP	náležet
k	k	k7c3	k
Balbínovým	Balbínův	k2eAgNnPc3d1	Balbínovo
nejzáslužnějším	záslužní	k2eAgNnPc3d3	záslužní
dílům	dílo	k1gNnPc3	dílo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
zachycení	zachycení	k1gNnSc3	zachycení
celé	celý	k2eAgFnSc2d1	celá
literární	literární	k2eAgFnSc2d1	literární
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
kulturních	kulturní	k2eAgFnPc2d1	kulturní
snah	snaha	k1gFnPc2	snaha
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Balbín	Balbín	k1gMnSc1	Balbín
plánoval	plánovat	k5eAaImAgMnS	plánovat
celkem	celkem	k6eAd1	celkem
30	[number]	k4	30
svazků	svazek	k1gInPc2	svazek
svých	svůj	k3xOyFgFnPc2	svůj
Rozmanitostí	rozmanitost	k1gFnPc2	rozmanitost
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
ale	ale	k8xC	ale
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
stihl	stihnout	k5eAaPmAgInS	stihnout
dokončit	dokončit	k5eAaPmF	dokončit
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc1	část
a	a	k8xC	a
něco	něco	k3yInSc4	něco
nebylo	být	k5eNaImAgNnS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
vůbec	vůbec	k9	vůbec
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
v	v	k7c6	v
rekatolizačním	rekatolizační	k2eAgMnSc6d1	rekatolizační
duchu	duch	k1gMnSc6	duch
<g/>
,	,	kIx,	,
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
zcela	zcela	k6eAd1	zcela
jasně	jasně	k6eAd1	jasně
o	o	k7c6	o
Balbínově	Balbínův	k2eAgFnSc6d1	Balbínova
lásce	láska	k1gFnSc6	láska
k	k	k7c3	k
národu	národ	k1gInSc3	národ
<g/>
,	,	kIx,	,
vlasti	vlast	k1gFnSc6	vlast
a	a	k8xC	a
jejím	její	k3xOp3gFnPc3	její
dějinám	dějiny	k1gFnPc3	dějiny
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kniha	kniha	k1gFnSc1	kniha
první	první	k4xOgFnSc6	první
Liber	libra	k1gFnPc2	libra
naturalis	naturalis	k1gFnPc1	naturalis
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
přírodopise	přírodopis	k1gInSc6	přírodopis
země	zem	k1gFnSc2	zem
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
druhá	druhý	k4xOgFnSc1	druhý
Popularis	Popularis	k1gFnSc1	Popularis
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
jejich	jejich	k3xOp3gMnPc6	jejich
obyvatelích	obyvatel	k1gMnPc6	obyvatel
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
třetí	třetí	k4xOgMnSc1	třetí
Chorographicus	Chorographicus	k1gMnSc1	Chorographicus
o	o	k7c6	o
místopisu	místopis	k1gInSc6	místopis
<g/>
,	,	kIx,	,
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Hagiographicus	Hagiographicus	k1gMnSc1	Hagiographicus
neboli	neboli	k8xC	neboli
Bohemia	bohemia	k1gFnSc1	bohemia
sancta	sancta	k1gFnSc1	sancta
shrnul	shrnout	k5eAaPmAgMnS	shrnout
Balbín	Balbín	k1gMnSc1	Balbín
informace	informace	k1gFnSc2	informace
o	o	k7c4	o
134	[number]	k4	134
českých	český	k2eAgFnPc2d1	Česká
svatých	svatá	k1gFnPc2	svatá
<g/>
,	,	kIx,	,
blahoslavených	blahoslavený	k2eAgMnPc6d1	blahoslavený
<g/>
,	,	kIx,	,
mučednících	mučedník	k1gMnPc6	mučedník
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc6	jejich
zázracích	zázrak	k1gInPc6	zázrak
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
kniha	kniha	k1gFnSc1	kniha
Parochialis	Parochialis	k1gFnSc1	Parochialis
čili	čili	k8xC	čili
Sacer	Sacer	k1gInSc1	Sacer
dotalis	dotalis	k1gFnSc2	dotalis
o	o	k7c6	o
farách	fara	k1gFnPc6	fara
a	a	k8xC	a
záduší	záduší	k1gNnSc6	záduší
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Episcopalis	Episcopalis	k1gInSc1	Episcopalis
o	o	k7c6	o
arcibiskupství	arcibiskupství	k1gNnSc6	arcibiskupství
pražském	pražský	k2eAgNnSc6d1	Pražské
<g/>
,	,	kIx,	,
Regalis	Regalis	k1gInSc1	Regalis
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
českých	český	k2eAgMnPc6d1	český
panovnících	panovník	k1gMnPc6	panovník
<g/>
,	,	kIx,	,
a	a	k8xC	a
Epistolaris	Epistolaris	k1gFnSc1	Epistolaris
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
státoprávních	státoprávní	k2eAgFnPc6d1	státoprávní
listinách	listina	k1gFnPc6	listina
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
věnovány	věnovat	k5eAaImNgFnP	věnovat
rodopisu	rodopis	k1gInSc6	rodopis
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rodopis	rodopis	k1gInSc1	rodopis
vydal	vydat	k5eAaPmAgInS	vydat
podruhé	podruhé	k6eAd1	podruhé
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vlastními	vlastní	k2eAgInPc7d1	vlastní
dodatky	dodatek	k1gInPc7	dodatek
roku	rok	k1gInSc2	rok
1770	[number]	k4	1770
J.	J.	kA	J.
Diesbach	Diesbacha	k1gFnPc2	Diesbacha
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Součástí	součást	k1gFnSc7	součást
Rozmanitostí	rozmanitost	k1gFnPc2	rozmanitost
je	být	k5eAaImIp3nS	být
i	i	k9	i
Kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
soudech	soud	k1gInPc6	soud
a	a	k8xC	a
úřadech	úřad	k1gInPc6	úřad
zemských	zemský	k2eAgFnPc2d1	zemská
království	království	k1gNnSc3	království
českého	český	k2eAgNnSc2d1	české
(	(	kIx(	(
<g/>
Liber	libra	k1gFnPc2	libra
curialis	curialis	k1gFnSc2	curialis
seu	seu	k?	seu
de	de	k?	de
magistratibus	magistratibus	k1gInSc1	magistratibus
et	et	k?	et
officiis	officiis	k1gInSc1	officiis
curialibus	curialibus	k1gMnSc1	curialibus
regm	regma	k1gFnPc2	regma
Bohemiae	Bohemiae	k1gFnPc2	Bohemiae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
českém	český	k2eAgNnSc6d1	české
ptactvu	ptactvo	k1gNnSc6	ptactvo
v	v	k7c6	v
Rozmanitostech	rozmanitost	k1gFnPc6	rozmanitost
bývá	bývat	k5eAaImIp3nS	bývat
jmenováno	jmenován	k2eAgNnSc1d1	jmenováno
jako	jako	k8xC	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
českých	český	k2eAgNnPc2d1	české
historických	historický	k2eAgNnPc2d1	historické
ornitologických	ornitologický	k2eAgNnPc2d1	ornitologické
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
Balbínův	Balbínův	k2eAgMnSc1d1	Balbínův
řádový	řádový	k2eAgMnSc1d1	řádový
bratr	bratr	k1gMnSc1	bratr
Jan	Jan	k1gMnSc1	Jan
Viktorín	Viktorín	k1gMnSc1	Viktorín
(	(	kIx(	(
<g/>
Joannes	Joannes	k1gMnSc1	Joannes
Victorinus	Victorinus	k1gMnSc1	Victorinus
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
prvního	první	k4xOgMnSc4	první
českého	český	k2eAgMnSc4d1	český
ornitologa	ornitolog	k1gMnSc4	ornitolog
<g/>
.	.	kIx.	.
<g/>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
je	být	k5eAaImIp3nS	být
také	také	k9	také
autorem	autor	k1gMnSc7	autor
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
církevních	církevní	k2eAgFnPc6d1	církevní
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
,	,	kIx,	,
o	o	k7c6	o
životě	život	k1gInSc6	život
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
i	i	k8xC	i
jiných	jiný	k2eAgFnPc2d1	jiná
svatých	svatá	k1gFnPc2	svatá
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Napsal	napsat	k5eAaPmAgInS	napsat
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
líčí	líčit	k5eAaImIp3nS	líčit
popis	popis	k1gInSc4	popis
a	a	k8xC	a
dějiny	dějiny	k1gFnPc4	dějiny
míst	místo	k1gNnPc2	místo
zasvěcených	zasvěcený	k2eAgMnPc2d1	zasvěcený
Panně	Panna	k1gFnSc6	Panna
Marii	Maria	k1gFnSc6	Maria
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
žákem	žák	k1gMnSc7	žák
Tomášem	Tomáš	k1gMnSc7	Tomáš
Pešinou	Pešina	k1gMnSc7	Pešina
z	z	k7c2	z
Čechorodu	Čechorod	k1gInSc2	Čechorod
je	být	k5eAaImIp3nS	být
Balbín	Balbín	k1gInSc4	Balbín
autorem	autor	k1gMnSc7	autor
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
vžitého	vžitý	k2eAgInSc2d1	vžitý
názvu	název	k1gInSc2	název
Dalimilova	Dalimilův	k2eAgFnSc1d1	Dalimilova
kronika	kronika	k1gFnSc1	kronika
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Balbínův	Balbínův	k2eAgInSc1d1	Balbínův
životopis	životopis	k1gInSc1	životopis
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
si	se	k3xPyFc3	se
také	také	k9	také
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
být	být	k5eAaImF	být
podrobněji	podrobně	k6eAd2	podrobně
zmíněn	zmíněn	k2eAgMnSc1d1	zmíněn
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
o	o	k7c6	o
popularizaci	popularizace	k1gFnSc6	popularizace
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Pomuku	Pomuk	k1gInSc2	Pomuk
<g/>
,	,	kIx,	,
významné	významný	k2eAgFnPc4d1	významná
osobnosti	osobnost	k1gFnPc4	osobnost
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Václava	Václav	k1gMnSc2	Václav
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
existující	existující	k2eAgInPc4d1	existující
prameny	pramen	k1gInPc4	pramen
<g/>
,	,	kIx,	,
pověsti	pověst	k1gFnPc4	pověst
i	i	k8xC	i
lidové	lidový	k2eAgFnPc4d1	lidová
zkazky	zkazka	k1gFnPc4	zkazka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nepomucká	pomuckat	k5eNaPmIp3nS	pomuckat
legenda	legenda	k1gFnSc1	legenda
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Při	při	k7c6	při
heuristice	heuristika	k1gFnSc6	heuristika
Balbínovi	Balbínův	k2eAgMnPc1d1	Balbínův
pomohli	pomoct	k5eAaPmAgMnP	pomoct
především	především	k9	především
svatovítští	svatovítský	k2eAgMnPc1d1	svatovítský
kanovníci	kanovník	k1gMnPc1	kanovník
Tomáš	Tomáš	k1gMnSc1	Tomáš
Pěšina	pěšina	k1gFnSc1	pěšina
z	z	k7c2	z
Čechorodu	Čechorod	k1gInSc2	Čechorod
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Dlouhoveský	Dlouhoveský	k2eAgMnSc1d1	Dlouhoveský
<g/>
.	.	kIx.	.
</s>
<s>
Nepomuckého	Nepomuckého	k2eAgInSc1d1	Nepomuckého
životopis	životopis	k1gInSc1	životopis
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaBmNgInS	napsat
pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
sbírku	sbírka	k1gFnSc4	sbírka
Životy	život	k1gInPc1	život
svatých	svatá	k1gFnPc2	svatá
roku	rok	k1gInSc2	rok
1670	[number]	k4	1670
<g/>
.	.	kIx.	.
</s>
<s>
Balbínovy	Balbínův	k2eAgFnSc2d1	Balbínova
legendy	legenda	k1gFnSc2	legenda
využila	využít	k5eAaPmAgFnS	využít
roku	rok	k1gInSc2	rok
1729	[number]	k4	1729
jako	jako	k8xS	jako
oporu	opora	k1gFnSc4	opora
papežská	papežský	k2eAgFnSc1d1	Papežská
kanonizační	kanonizační	k2eAgFnSc1d1	kanonizační
bula	bula	k1gFnSc1	bula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
posuzování	posuzování	k1gNnSc4	posuzování
životopisu	životopis	k1gInSc2	životopis
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
dějepisného	dějepisný	k2eAgNnSc2d1	dějepisné
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Balbín	Balbín	k1gMnSc1	Balbín
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
díval	dívat	k5eAaImAgMnS	dívat
jako	jako	k9	jako
na	na	k7c4	na
legendu	legenda	k1gFnSc4	legenda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
volně	volně	k6eAd1	volně
doplnit	doplnit	k5eAaPmF	doplnit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
striktně	striktně	k6eAd1	striktně
historický	historický	k2eAgInSc4d1	historický
spis	spis	k1gInSc4	spis
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
vytýkáno	vytýkán	k2eAgNnSc1d1	vytýkáno
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
historické	historický	k2eAgFnPc4d1	historická
nepřesnosti	nepřesnost	k1gFnPc4	nepřesnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
datum	datum	k1gNnSc1	datum
světcovy	světcův	k2eAgFnSc2d1	světcova
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
Balbínův	Balbínův	k2eAgInSc4d1	Balbínův
spis	spis	k1gInSc4	spis
důležitou	důležitý	k2eAgFnSc7d1	důležitá
českou	český	k2eAgFnSc7d1	Česká
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
žáky	žák	k1gMnPc4	žák
napsal	napsat	k5eAaPmAgMnS	napsat
Balbín	Balbín	k1gMnSc1	Balbín
roku	rok	k1gInSc2	rok
1666	[number]	k4	1666
spis	spis	k1gInSc1	spis
Verisimilia	Verisimilium	k1gNnSc2	Verisimilium
humanorum	humanorum	k1gNnSc1	humanorum
disciplinarum	disciplinarum	k1gInSc1	disciplinarum
<g/>
,	,	kIx,	,
o	o	k7c4	o
jedenáct	jedenáct	k4xCc4	jedenáct
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
následovaný	následovaný	k2eAgInSc1d1	následovaný
dílkem	dílek	k1gInSc7	dílek
Auxilia	Auxilia	k1gFnSc1	Auxilia
poetices	poetices	k1gMnSc1	poetices
<g/>
.	.	kIx.	.
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
Pro	pro	k7c4	pro
žáky	žák	k1gMnPc4	žák
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
určeno	určit	k5eAaPmNgNnS	určit
dílo	dílo	k1gNnSc4	dílo
Quaesita	Quaesitum	k1gNnSc2	Quaesitum
oratoria	oratorium	k1gNnSc2	oratorium
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
životě	život	k1gInSc6	život
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Arnošta	Arnošt	k1gMnSc2	Arnošt
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
kniha	kniha	k1gFnSc1	kniha
Vita	vit	k2eAgFnSc1d1	Vita
venerabilis	venerabilis	k1gFnSc1	venerabilis
Arnesti	Arnest	k1gFnSc2	Arnest
primi	pri	k1gFnPc7	pri
archiepiscopi	archiepiscopi	k1gNnSc2	archiepiscopi
Pragensis	Pragensis	k1gFnPc2	Pragensis
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1664	[number]	k4	1664
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Origines	Origines	k1gInSc4	Origines
illustrissimorum	illustrissimorum	k1gInSc1	illustrissimorum
comitum	comitum	k1gNnSc1	comitum
de	de	k?	de
Guttenstein	Guttenstein	k1gMnSc1	Guttenstein
<g/>
,	,	kIx,	,
ubi	ubi	k?	ubi
refertur	refertura	k1gFnPc2	refertura
Vita	vit	k2eAgFnSc1d1	Vita
B.	B.	kA	B.
Hroznatae	Hroznatae	k1gFnSc1	Hroznatae
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
Balbín	Balbín	k1gInSc4	Balbín
původ	původ	k1gInSc4	původ
hrabat	hrabě	k1gNnPc2	hrabě
z	z	k7c2	z
Guttensteinu	Guttenstein	k1gInSc2	Guttenstein
a	a	k8xC	a
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrabata	hrabě	k1gNnPc1	hrabě
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
blahoslaveného	blahoslavený	k2eAgInSc2d1	blahoslavený
Hroznaty	Hroznaty	k?	Hroznaty
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
psal	psát	k5eAaImAgMnS	psát
i	i	k9	i
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
sbírku	sbírka	k1gFnSc4	sbírka
<g/>
,	,	kIx,	,
pojmenovanou	pojmenovaný	k2eAgFnSc4d1	pojmenovaná
Examen	examen	k1gNnSc1	examen
melissaeum	melissaeum	k1gNnSc1	melissaeum
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
dokončenou	dokončený	k2eAgFnSc7d1	dokončená
prací	práce	k1gFnSc7	práce
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Balbína	Balbín	k1gMnSc2	Balbín
je	být	k5eAaImIp3nS	být
Život	život	k1gInSc1	život
důstojného	důstojný	k2eAgMnSc2d1	důstojný
otce	otec	k1gMnSc2	otec
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Leczyckého	Leczycký	k1gMnSc2	Leczycký
(	(	kIx(	(
<g/>
Vita	vit	k2eAgFnSc1d1	Vita
vener	vener	k1gInSc1	vener
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
Nicolai	Nicolae	k1gFnSc4	Nicolae
Lancicii	Lancicie	k1gFnSc4	Lancicie
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Veškerá	veškerý	k3xTgFnSc1	veškerý
Balbínova	Balbínův	k2eAgFnSc1d1	Balbínova
tvorba	tvorba	k1gFnSc1	tvorba
je	být	k5eAaImIp3nS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
dvěma	dva	k4xCgInPc7	dva
póly	pól	k1gInPc7	pól
<g/>
:	:	kIx,	:
pravověrným	pravověrný	k2eAgNnSc7d1	pravověrné
katolictvím	katolictví	k1gNnSc7	katolictví
a	a	k8xC	a
loajalitou	loajalita	k1gFnSc7	loajalita
k	k	k7c3	k
Habsburkům	Habsburk	k1gInPc3	Habsburk
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
srdečnou	srdečný	k2eAgFnSc7d1	srdečná
láskou	láska	k1gFnSc7	láska
k	k	k7c3	k
vlasti	vlast	k1gFnSc3	vlast
<g/>
,	,	kIx,	,
jejímu	její	k3xOp3gInSc3	její
jazyku	jazyk	k1gInSc3	jazyk
a	a	k8xC	a
kultuře	kultura	k1gFnSc3	kultura
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zastavila	zastavit	k5eAaPmAgFnS	zastavit
ho	on	k3xPp3gInSc4	on
nemoc	nemoc	k1gFnSc1	nemoc
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
slepota	slepota	k1gFnSc1	slepota
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
nechtěl	chtít	k5eNaImAgMnS	chtít
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Útěchu	útěcha	k1gFnSc4	útěcha
mu	on	k3xPp3gMnSc3	on
přinášelo	přinášet	k5eAaImAgNnS	přinášet
heslo	heslo	k1gNnSc4	heslo
In	In	k1gFnSc2	In
silentio	silentio	k1gMnSc1	silentio
et	et	k?	et
spe	spe	k?	spe
fortitudo	fortitudo	k1gNnSc4	fortitudo
mea	mea	k?	mea
(	(	kIx(	(
<g/>
Mlče	mlčet	k5eAaImSgInS	mlčet
a	a	k8xC	a
doufaje	doufat	k5eAaImSgInS	doufat
jsem	být	k5eAaImIp1nS	být
statečný	statečný	k2eAgMnSc1d1	statečný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
až	až	k6eAd1	až
do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
možného	možný	k2eAgInSc2d1	možný
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1687	[number]	k4	1687
stižen	stižen	k2eAgInSc1d1	stižen
mrtvicí	mrtvice	k1gFnSc7	mrtvice
<g/>
,	,	kIx,	,
ochrnul	ochrnout	k5eAaPmAgInS	ochrnout
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
psát	psát	k5eAaImF	psát
pouze	pouze	k6eAd1	pouze
levou	levý	k2eAgFnSc7d1	levá
rukou	ruka	k1gFnSc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
mu	on	k3xPp3gMnSc3	on
zbyla	zbýt	k5eAaPmAgFnS	zbýt
jen	jen	k6eAd1	jen
možnost	možnost	k1gFnSc4	možnost
využívat	využívat	k5eAaPmF	využívat
služeb	služba	k1gFnPc2	služba
písaře	písař	k1gMnSc2	písař
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
<g/>
,	,	kIx,	,
bezesporu	bezesporu	k9	bezesporu
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
osobností	osobnost	k1gFnPc2	osobnost
barokní	barokní	k2eAgFnSc2d1	barokní
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
před	před	k7c7	před
svými	svůj	k3xOyFgInPc7	svůj
67	[number]	k4	67
<g/>
.	.	kIx.	.
narozeninami	narozeniny	k1gFnPc7	narozeniny
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1688	[number]	k4	1688
okolo	okolo	k7c2	okolo
osmé	osmý	k4xOgFnSc2	osmý
hodiny	hodina	k1gFnSc2	hodina
večerní	večerní	k2eAgFnSc2d1	večerní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
v	v	k7c6	v
kryptě	krypta	k1gFnSc6	krypta
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Salvátora	Salvátor	k1gMnSc2	Salvátor
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Souhrn	souhrn	k1gInSc1	souhrn
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Psal	psát	k5eAaImAgMnS	psát
výhradně	výhradně	k6eAd1	výhradně
latinsky	latinsky	k6eAd1	latinsky
<g/>
.	.	kIx.	.
</s>
<s>
Uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
si	se	k3xPyFc3	se
svízelnou	svízelný	k2eAgFnSc4d1	svízelná
situaci	situace	k1gFnSc4	situace
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
řešit	řešit	k5eAaImF	řešit
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
psát	psát	k5eAaImF	psát
proti	proti	k7c3	proti
necitlivé	citlivý	k2eNgFnSc3d1	necitlivá
rekatolizaci	rekatolizace	k1gFnSc3	rekatolizace
a	a	k8xC	a
germanizaci	germanizace	k1gFnSc3	germanizace
<g/>
,	,	kIx,	,
za	za	k7c4	za
tyto	tento	k3xDgInPc4	tento
názory	názor	k1gInPc4	názor
byl	být	k5eAaImAgInS	být
pronásledován	pronásledován	k2eAgInSc1d1	pronásledován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Legatio	Legatio	k6eAd1	Legatio
Apollinis	Apollinis	k1gFnSc1	Apollinis
coelestis	coelestis	k1gFnSc2	coelestis
ad	ad	k7c4	ad
universitatem	universitat	k1gMnSc7	universitat
Pragensem	Pragenso	k1gNnSc7	Pragenso
etc	etc	k?	etc
<g/>
.	.	kIx.	.
–	–	k?	–
vydáno	vydat	k5eAaPmNgNnS	vydat
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
korunovace	korunovace	k1gFnSc2	korunovace
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
(	(	kIx(	(
<g/>
1646	[number]	k4	1646
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
spis	spis	k1gInSc4	spis
byl	být	k5eAaImAgInS	být
poslán	poslán	k2eAgInSc1d1	poslán
do	do	k7c2	do
"	"	kIx"	"
<g/>
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
"	"	kIx"	"
do	do	k7c2	do
Klatov	Klatovy	k1gInPc2	Klatovy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Epitome	Epitom	k1gInSc5	Epitom
rerum	rerum	k1gNnSc1	rerum
Bohem	bůh	k1gMnSc7	bůh
–	–	k?	–
Výtah	výtah	k1gInSc1	výtah
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
českých	český	k2eAgFnPc2d1	Česká
</s>
</p>
<p>
<s>
Dissertatio	Dissertatio	k1gMnSc1	Dissertatio
apologetica	apologetica	k1gMnSc1	apologetica
pro	pro	k7c4	pro
lingua	lingu	k2eAgMnSc4d1	lingu
Slavonica	Slavonicus	k1gMnSc4	Slavonicus
<g/>
,	,	kIx,	,
praecipue	praecipu	k1gMnSc4	praecipu
Bohemica	Bohemic	k1gInSc2	Bohemic
–	–	k?	–
Rozprava	rozprava	k1gFnSc1	rozprava
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
jazyka	jazyk	k1gInSc2	jazyk
slovanského	slovanský	k2eAgInSc2d1	slovanský
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
českého	český	k2eAgNnSc2d1	české
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známá	k1gFnPc1	známá
též	též	k9	též
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Balbínova	Balbínův	k2eAgFnSc1d1	Balbínova
obrana	obrana	k1gFnSc1	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Přesvědčuje	přesvědčovat	k5eAaImIp3nS	přesvědčovat
zde	zde	k6eAd1	zde
o	o	k7c4	o
schopnosti	schopnost	k1gFnPc4	schopnost
češtiny	čeština	k1gFnSc2	čeština
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
vzkříšení	vzkříšení	k1gNnSc4	vzkříšení
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
za	za	k7c2	za
Balbínova	Balbínův	k2eAgInSc2d1	Balbínův
života	život	k1gInSc2	život
tiskem	tisk	k1gInSc7	tisk
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
<g/>
,	,	kIx,	,
šířilo	šířit	k5eAaImAgNnS	šířit
se	se	k3xPyFc4	se
opisem	opis	k1gInSc7	opis
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
je	být	k5eAaImIp3nS	být
vydal	vydat	k5eAaPmAgMnS	vydat
až	až	k9	až
František	František	k1gMnSc1	František
Martin	Martin	k1gMnSc1	Martin
Pelcl	Pelcl	k1gMnSc1	Pelcl
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1775	[number]	k4	1775
<g/>
.	.	kIx.	.
<g/>
Dále	daleko	k6eAd2	daleko
vydal	vydat	k5eAaPmAgInS	vydat
popis	popis	k1gInSc1	popis
tří	tři	k4xCgNnPc2	tři
hlavních	hlavní	k2eAgNnPc2d1	hlavní
poutních	poutní	k2eAgNnPc2d1	poutní
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
zasvěcených	zasvěcený	k2eAgFnPc2d1	zasvěcená
zázračným	zázračný	k2eAgMnPc3d1	zázračný
sochám	sochat	k5eAaImIp1nS	sochat
Panny	Panna	k1gFnPc4	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Diva	diva	k1gFnSc1	diva
Vartensis	Vartensis	k1gFnSc2	Vartensis
–	–	k?	–
v	v	k7c6	v
dolním	dolní	k2eAgNnSc6d1	dolní
Slezsku	Slezsko	k1gNnSc6	Slezsko
ve	v	k7c6	v
Vartě	varta	k1gFnSc6	varta
</s>
</p>
<p>
<s>
Diva	diva	k1gFnSc1	diva
Turzanensis	Turzanensis	k1gFnSc2	Turzanensis
–	–	k?	–
v	v	k7c6	v
Tuřanech	Tuřan	k1gInPc6	Tuřan
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
</s>
</p>
<p>
<s>
Diva	diva	k1gFnSc1	diva
Montis	Montis	k1gFnSc2	Montis
Sancti	Sancti	k1gNnSc2	Sancti
–	–	k?	–
Svatá	svatý	k2eAgFnSc1d1	svatá
Hora	hora	k1gFnSc1	hora
u	u	k7c2	u
Příbrami	Příbram	k1gFnSc2	Příbram
(	(	kIx(	(
<g/>
na	na	k7c6	na
knize	kniha	k1gFnSc6	kniha
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
Jiří	Jiří	k1gMnSc1	Jiří
Kruger	Kruger	k1gMnSc1	Kruger
<g/>
)	)	kIx)	)
<g/>
Díla	dílo	k1gNnPc1	dílo
historická	historický	k2eAgNnPc1d1	historické
<g/>
,	,	kIx,	,
politická	politický	k2eAgNnPc1d1	politické
a	a	k8xC	a
životopisná	životopisný	k2eAgNnPc1d1	životopisné
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Origines	Origines	k1gMnSc1	Origines
Comitibus	Comitibus	k1gMnSc1	Comitibus
de	de	k?	de
Guttenstein	Guttenstein	k1gInSc1	Guttenstein
–	–	k?	–
původ	původ	k1gInSc4	původ
hrabat	hrabat	k5eAaImF	hrabat
z	z	k7c2	z
Guttenštejna	Guttenštejn	k1gInSc2	Guttenštejn
</s>
</p>
<p>
<s>
Vita	vit	k2eAgFnSc1d1	Vita
venerabilis	venerabilis	k1gFnSc1	venerabilis
Arnesti	Arnest	k1gFnSc2	Arnest
–	–	k?	–
život	život	k1gInSc1	život
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Arnošta	Arnošt	k1gMnSc2	Arnošt
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
</s>
</p>
<p>
<s>
Miscellanea	Miscellane	k2eAgFnSc1d1	Miscellanea
historica	historica	k1gFnSc1	historica
regni	regň	k1gMnSc3	regň
Bohemiae	Bohemia	k1gFnSc2	Bohemia
–	–	k?	–
Směs	směs	k1gFnSc1	směs
rozprav	rozprava	k1gFnPc2	rozprava
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
českých	český	k2eAgFnPc6d1	Česká
<g/>
,	,	kIx,	,
vycházelo	vycházet	k5eAaImAgNnS	vycházet
od	od	k7c2	od
r.	r.	kA	r.
1679	[number]	k4	1679
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
rozvrženo	rozvrhnout	k5eAaPmNgNnS	rozvrhnout
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
deset	deset	k4xCc4	deset
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
vyšly	vyjít	k5eAaPmAgFnP	vyjít
až	až	k9	až
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I.	I.	kA	I.
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
dekáda	dekáda	k1gFnSc1	dekáda
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Liber	libra	k1gFnPc2	libra
naturalis	naturalis	k1gFnSc2	naturalis
–	–	k?	–
o	o	k7c6	o
přirozené	přirozený	k2eAgFnSc6d1	přirozená
povaze	povaha	k1gFnSc6	povaha
země	zem	k1gFnSc2	zem
České	český	k2eAgFnSc2d1	Česká
</s>
</p>
<p>
<s>
Liber	libra	k1gFnPc2	libra
popularis	popularis	k1gFnSc2	popularis
–	–	k?	–
o	o	k7c6	o
obyvatelích	obyvatel	k1gMnPc6	obyvatel
</s>
</p>
<p>
<s>
Liber	libra	k1gFnPc2	libra
chorographicus	chorographicus	k1gMnSc1	chorographicus
–	–	k?	–
místopis	místopis	k1gInSc1	místopis
</s>
</p>
<p>
<s>
Liber	libra	k1gFnPc2	libra
hagiographicus	hagiographicus	k1gMnSc1	hagiographicus
–	–	k?	–
též	též	k9	též
Bohemia	bohemia	k1gFnSc1	bohemia
sancta	sancto	k1gNnSc2	sancto
-	-	kIx~	-
o	o	k7c6	o
svatých	svatá	k1gFnPc6	svatá
</s>
</p>
<p>
<s>
Liber	libra	k1gFnPc2	libra
parochialis	parochialis	k1gFnSc1	parochialis
–	–	k?	–
též	též	k9	též
Sacer	Sacer	k1gMnSc1	Sacer
dotalis	dotalis	k1gFnSc2	dotalis
-	-	kIx~	-
o	o	k7c6	o
farách	fara	k1gFnPc6	fara
</s>
</p>
<p>
<s>
Liber	libra	k1gFnPc2	libra
episcopalis	episcopalis	k1gFnSc2	episcopalis
–	–	k?	–
o	o	k7c6	o
pražském	pražský	k2eAgNnSc6d1	Pražské
arcibiskupství	arcibiskupství	k1gNnSc6	arcibiskupství
</s>
</p>
<p>
<s>
Liber	libra	k1gFnPc2	libra
regalis	regalis	k1gFnSc2	regalis
–	–	k?	–
o	o	k7c6	o
panovnících	panovník	k1gMnPc6	panovník
</s>
</p>
<p>
<s>
Liber	libra	k1gFnPc2	libra
epistolaris	epistolaris	k1gFnSc2	epistolaris
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
listin	listina	k1gFnPc2	listina
</s>
</p>
<p>
<s>
Bohemia	bohemia	k1gFnSc1	bohemia
docta	docta	k1gFnSc1	docta
–	–	k?	–
Učené	učený	k2eAgFnPc4d1	učená
Čechy	Čechy	k1gFnPc4	Čechy
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
až	až	k9	až
roku	rok	k1gInSc2	rok
1777	[number]	k4	1777
<g/>
,	,	kIx,	,
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
zde	zde	k6eAd1	zde
českými	český	k2eAgFnPc7d1	Česká
kulturními	kulturní	k2eAgFnPc7d1	kulturní
a	a	k8xC	a
literárními	literární	k2eAgFnPc7d1	literární
dějinami	dějiny	k1gFnPc7	dějiny
</s>
</p>
<p>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
dekáda	dekáda	k1gFnSc1	dekáda
<g/>
)	)	kIx)	)
nebyla	být	k5eNaImAgFnS	být
napsána	napsat	k5eAaBmNgFnS	napsat
celá	celý	k2eAgFnSc1d1	celá
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
knihy	kniha	k1gFnPc4	kniha
jsou	být	k5eAaImIp3nP	být
věnovány	věnovat	k5eAaPmNgFnP	věnovat
genealogii	genealogie	k1gFnSc4	genealogie
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
</s>
</p>
<p>
<s>
Liber	libra	k1gFnPc2	libra
curialis	curialis	k1gFnSc2	curialis
seu	seu	k?	seu
de	de	k?	de
magistratibus	magistratibus	k1gInSc1	magistratibus
et	et	k?	et
officiis	officiis	k1gFnSc3	officiis
curialibus	curialibus	k1gInSc4	curialibus
regni	regeň	k1gFnSc3	regeň
Bohemiae	Bohemiae	k1gInSc1	Bohemiae
–	–	k?	–
Kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
soudech	soud	k1gInPc6	soud
a	a	k8xC	a
úřadech	úřad	k1gInPc6	úřad
království	království	k1gNnSc2	království
českéhoUčebnice	českéhoUčebnice	k1gFnSc2	českéhoUčebnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Quaesita	Quaesita	k1gFnSc1	Quaesita
oratoria	oratorium	k1gNnSc2	oratorium
</s>
</p>
<p>
<s>
Verisimilia	Verisimilia	k1gFnSc1	Verisimilia
humaniorum	humaniorum	k1gInSc1	humaniorum
disciplinarumPoezie	disciplinarumPoezie	k1gFnSc1	disciplinarumPoezie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Examen	examen	k1gInSc1	examen
Mellisaeum	Mellisaeum	k1gInSc1	Mellisaeum
-	-	kIx~	-
doslova	doslova	k6eAd1	doslova
Roj	roj	k1gInSc1	roj
včel	včela	k1gFnPc2	včela
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
satirických	satirický	k2eAgInPc2d1	satirický
epigramůDostupné	epigramůDostupný	k2eAgNnSc4d1	epigramůDostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
BALBÍN	BALBÍN	kA	BALBÍN
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
,	,	kIx,	,
pražského	pražský	k2eAgInSc2d1	pražský
chrámu	chrám	k1gInSc2	chrám
metropolitního	metropolitní	k2eAgInSc2d1	metropolitní
u	u	k7c2	u
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
kanovníka	kanovník	k1gMnSc2	kanovník
<g/>
,	,	kIx,	,
kněze	kněz	k1gMnSc2	kněz
a	a	k8xC	a
mučedníka	mučedník	k1gMnSc2	mučedník
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
Říše	říše	k1gFnSc1	říše
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
:	:	kIx,	:
Antonín	Antonín	k1gMnSc1	Antonín
Ludvík	Ludvík	k1gMnSc1	Ludvík
Stříž	stříž	k1gFnSc1	stříž
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nová	nový	k2eAgNnPc1d1	nové
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Krásy	krása	k1gFnPc1	krása
a	a	k8xC	a
bohatství	bohatství	k1gNnSc1	bohatství
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Helena	Helena	k1gFnSc1	Helena
Businská	Businský	k2eAgFnSc1d1	Businská
<g/>
,	,	kIx,	,
úvodní	úvodní	k2eAgFnSc1d1	úvodní
studie	studie	k1gFnSc1	studie
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Tichá	Tichá	k1gFnSc1	Tichá
<g/>
,	,	kIx,	,
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
351	[number]	k4	351
str	str	kA	str
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozmanitosti	rozmanitost	k1gFnPc1	rozmanitost
z	z	k7c2	z
historie	historie	k1gFnSc2	historie
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
(	(	kIx(	(
<g/>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
614	[number]	k4	614
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jiří	Jiří	k1gMnSc1	Jiří
A.	A.	kA	A.
Čepelák	Čepelák	k1gMnSc1	Čepelák
<g/>
,	,	kIx,	,
komentáři	komentář	k1gInSc6	komentář
opatřili	opatřit	k5eAaPmAgMnP	opatřit
Stanislav	Stanislav	k1gMnSc1	Stanislav
Komárek	Komárek	k1gMnSc1	Komárek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Cílek	Cílek	k1gMnSc1	Cílek
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
A.	A.	kA	A.
Čepelák	Čepelák	k1gMnSc1	Čepelák
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BENEŠ	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
ještě	ještě	k9	ještě
mluví	mluvit	k5eAaImIp3nS	mluvit
:	:	kIx,	:
Medailony	medailon	k1gInPc4	medailon
českých	český	k2eAgMnPc2d1	český
katolických	katolický	k2eAgMnPc2d1	katolický
vlasteneckých	vlastenecký	k2eAgMnPc2d1	vlastenecký
kněží	kněz	k1gMnPc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
katolická	katolický	k2eAgFnSc1d1	katolická
charita	charita	k1gFnSc1	charita
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
449	[number]	k4	449
s.	s.	k?	s.
S.	S.	kA	S.
34	[number]	k4	34
<g/>
–	–	k?	–
<g/>
41	[number]	k4	41
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČERNÝ	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
HOLEŠ	HOLEŠ	kA	HOLEŠ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yQnSc1	kdo
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
české	český	k2eAgFnSc2d1	Česká
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
739	[number]	k4	739
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
369	[number]	k4	369
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
38	[number]	k4	38
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Starší	starý	k2eAgFnSc1d2	starší
česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
/	/	kIx~	/
Redaktor	redaktor	k1gMnSc1	redaktor
svazku	svazek	k1gInSc2	svazek
Josef	Josef	k1gMnSc1	Josef
Hrabák	Hrabák	k1gMnSc1	Hrabák
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československá	československý	k2eAgFnSc1d1	Československá
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
531	[number]	k4	531
s.	s.	k?	s.
S.	S.	kA	S.
463	[number]	k4	463
<g/>
–	–	k?	–
<g/>
465	[number]	k4	465
<g/>
;	;	kIx,	;
470	[number]	k4	470
<g/>
–	–	k?	–
<g/>
471	[number]	k4	471
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FORST	FORST	kA	FORST
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
<g/>
–	–	k?	–
<g/>
G.	G.	kA	G.
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
900	[number]	k4	900
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
797	[number]	k4	797
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
120	[number]	k4	120
<g/>
–	–	k?	–
<g/>
123	[number]	k4	123
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOMÁREK	Komárek	k1gMnSc1	Komárek
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
,	,	kIx,	,
Jezovitova	jezovitův	k2eAgFnSc1d1	jezovitův
jiná	jiný	k2eAgFnSc1d1	jiná
tvář	tvář	k1gFnSc1	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
jako	jako	k8xC	jako
biolog	biolog	k1gMnSc1	biolog
<g/>
,	,	kIx,	,
ĎaS	ďas	k1gMnSc1	ďas
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
č.	č.	k?	č.
12	[number]	k4	12
<g/>
,	,	kIx,	,
s.	s.	k?	s.
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KVĚTOŇOVÁ-KLÍMOVÁ	KVĚTOŇOVÁ-KLÍMOVÁ	k?	KVĚTOŇOVÁ-KLÍMOVÁ
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
<g/>
.	.	kIx.	.
</s>
<s>
Styky	styk	k1gInPc1	styk
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Balbína	Balbín	k1gMnSc2	Balbín
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
šlechtou	šlechta	k1gFnSc7	šlechta
pobělohorskou	pobělohorský	k2eAgFnSc7d1	pobělohorská
<g/>
.	.	kIx.	.
</s>
<s>
ČČH	ČČH	kA	ČČH
<g/>
,	,	kIx,	,
32	[number]	k4	32
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
497	[number]	k4	497
<g/>
-	-	kIx~	-
<g/>
541	[number]	k4	541
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
REJZEK	REJZEK	kA	REJZEK
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
T.J	T.J	k1gMnSc1	T.J
<g/>
:	:	kIx,	:
jeho	on	k3xPp3gInSc4	on
život	život	k1gInSc4	život
a	a	k8xC	a
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Dědictví	dědictví	k1gNnSc1	dědictví
sv.	sv.	kA	sv.
Prokopa	Prokop	k1gMnSc2	Prokop
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
465	[number]	k4	465
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRA	Vondra	k1gMnSc1	Vondra
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
1621	[number]	k4	1621
<g/>
–	–	k?	–
<g/>
1688	[number]	k4	1688
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
276	[number]	k4	276
<g/>
–	–	k?	–
<g/>
279	[number]	k4	279
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
B	B	kA	B
<g/>
–	–	k?	–
<g/>
Bař	Bař	k1gMnSc1	Bař
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
154	[number]	k4	154
<g/>
–	–	k?	–
<g/>
264	[number]	k4	264
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
252	[number]	k4	252
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
191	[number]	k4	191
<g/>
–	–	k?	–
<g/>
193	[number]	k4	193
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VEČERKA	Večerka	k1gMnSc1	Večerka
<g/>
,	,	kIx,	,
Radoslav	Radoslav	k1gMnSc1	Radoslav
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgMnPc2d1	český
jazykovědců	jazykovědec	k1gMnPc2	jazykovědec
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
bohemistiky	bohemistika	k1gFnSc2	bohemistika
a	a	k8xC	a
slavistiky	slavistika	k1gFnSc2	slavistika
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
341	[number]	k4	341
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
6265	[number]	k4	6265
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Pobělohorská	pobělohorský	k2eAgFnSc1d1	pobělohorská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
Dvaasedmdesát	dvaasedmdesát	k4xCc1	dvaasedmdesát
jmen	jméno	k1gNnPc2	jméno
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
</s>
</p>
<p>
<s>
Biskupské	biskupský	k2eAgNnSc1d1	Biskupské
gymnázium	gymnázium	k1gNnSc1	gymnázium
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
Balbína	Balbína	k1gFnSc1	Balbína
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bohuslav	Bohuslava	k1gFnPc2	Bohuslava
Balbín	Balbína	k1gFnPc2	Balbína
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Balbin	Balbin	k2eAgMnSc1d1	Balbin
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Dvaasedmdesát	dvaasedmdesát	k4xCc4	dvaasedmdesát
jmen	jméno	k1gNnPc2	jméno
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
</s>
</p>
