<s>
Citerárium	Citerárium	k1gNnSc1
</s>
<s>
Citerárium	Citerárium	k1gNnSc1
Citerárium	Citerárium	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
třetím	třetí	k4xOgNnSc6
patře	patro	k1gNnSc6
Údaje	údaj	k1gInSc2
o	o	k7c6
muzeu	muzeum	k1gNnSc6
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
hotel	hotel	k1gInSc1
Gambrinus	gambrinus	k1gInSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
10	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
17	#num#	k4
<g/>
′	′	k?
<g/>
31,41	31,41	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Citerárium	Citerárium	k1gNnSc1
je	být	k5eAaImIp3nS
ostravské	ostravský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
věnované	věnovaný	k2eAgFnSc2d1
citerám	citera	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yRgFnPc4
založil	založit	k5eAaPmAgMnS
a	a	k8xC
provozuje	provozovat	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Folprecht	Folprecht	k1gMnSc1xF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gFnPc6
sbírkách	sbírka	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1
jsou	být	k5eAaImIp3nP
ojedinělé	ojedinělý	k2eAgFnPc1d1
v	v	k7c6
rámci	rámec	k1gInSc6
celé	celý	k2eAgFnSc2d1
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
sto	sto	k4xCgNnSc1
dvacet	dvacet	k4xCc1
nástrojů	nástroj	k1gInPc2
<g/>
,	,	kIx,
citerová	citerový	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
a	a	k8xC
6000	#num#	k4
notových	notový	k2eAgFnPc2d1
partitur	partitura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstava	výstava	k1gFnSc1
nastiňuje	nastiňovat	k5eAaImIp3nS
pravděpodobné	pravděpodobný	k2eAgFnPc4d1
příčiny	příčina	k1gFnPc4
<g/>
,	,	kIx,
proč	proč	k6eAd1
citery	citera	k1gFnPc1
upadly	upadnout	k5eAaPmAgFnP
v	v	k7c6
zapomenutí	zapomenutí	k1gNnSc6
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
oblíbené	oblíbený	k2eAgInPc4d1
a	a	k8xC
rozšířené	rozšířený	k2eAgInPc4d1
hudební	hudební	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
;	;	kIx,
na	na	k7c4
citeru	citera	k1gFnSc4
hrál	hrát	k5eAaImAgMnS
např.	např.	kA
František	František	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
Antonína	Antonín	k1gMnSc2
Dvořáka	Dvořák	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Citerárium	Citerárium	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
třetím	třetí	k4xOgInSc6
patře	patro	k1gNnSc6
budovy	budova	k1gFnSc2
někdejšího	někdejší	k2eAgInSc2d1
hotelu	hotel	k1gInSc2
Gambrinus	Gambrinus	k1gMnSc1
na	na	k7c6
ostravském	ostravský	k2eAgNnSc6d1
Masarykově	Masarykův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Citerárium	Citerárium	k1gNnSc1
je	být	k5eAaImIp3nS
trochu	trochu	k6eAd1
opomíjené	opomíjený	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
deník	deník	k1gInSc1
9	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Ostrava	Ostrava	k1gFnSc1
</s>
