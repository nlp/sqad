<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
byl	být	k5eAaImAgInS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
papežem	papež	k1gMnSc7	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pontifikát	pontifikát	k1gInSc1	pontifikát
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
let	léto	k1gNnPc2	léto
105	[number]	k4	105
<g/>
/	/	kIx~	/
<g/>
106	[number]	k4	106
<g/>
/	/	kIx~	/
<g/>
107	[number]	k4	107
<g/>
/	/	kIx~	/
<g/>
109	[number]	k4	109
–	–	k?	–
115	[number]	k4	115
<g/>
/	/	kIx~	/
<g/>
116	[number]	k4	116
(	(	kIx(	(
<g/>
období	období	k1gNnSc1	období
pontifikátu	pontifikát	k1gInSc2	pontifikát
nejasné	jasný	k2eNgNnSc1d1	nejasné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
byl	být	k5eAaImAgMnS	být
patrně	patrně	k6eAd1	patrně
prvním	první	k4xOgMnSc7	první
římským	římský	k2eAgMnSc7d1	římský
biskupem	biskup	k1gMnSc7	biskup
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
zvolili	zvolit	k5eAaPmAgMnP	zvolit
věřící	věřící	k2eAgMnPc1d1	věřící
hlasováním	hlasování	k1gNnSc7	hlasování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
zaznamenané	zaznamenaný	k2eAgFnSc2d1	zaznamenaná
v	v	k7c6	v
"	"	kIx"	"
<g/>
Liber	libra	k1gFnPc2	libra
Pontificalis	Pontificalis	k1gFnPc2	Pontificalis
<g/>
"	"	kIx"	"
skončil	skončit	k5eAaPmAgMnS	skončit
mučednickou	mučednický	k2eAgFnSc7d1	mučednická
smrtí	smrt	k1gFnSc7	smrt
stětím	stětit	k5eAaPmIp1nS	stětit
na	na	k7c4	na
Via	via	k7c4	via
Nomentata	Nomente	k1gNnPc4	Nomente
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
téže	tenže	k3xDgFnSc2	tenže
legendy	legenda	k1gFnSc2	legenda
byl	být	k5eAaImAgMnS	být
římským	římský	k2eAgMnSc7d1	římský
občanem	občan	k1gMnSc7	občan
narozeným	narozený	k2eAgMnSc7d1	narozený
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
pontifikát	pontifikát	k1gInSc1	pontifikát
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
období	období	k1gNnSc2	období
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Traiana	Traian	k1gMnSc2	Traian
(	(	kIx(	(
<g/>
98	[number]	k4	98
<g/>
–	–	k?	–
<g/>
117	[number]	k4	117
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Připisují	připisovat	k5eAaImIp3nP	připisovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
různá	různý	k2eAgNnPc4d1	různé
nařízení	nařízení	k1gNnSc4	nařízení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
aby	aby	kYmCp3nS	aby
při	při	k7c6	při
oběti	oběť	k1gFnSc6	oběť
mše	mše	k1gFnSc2	mše
svaté	svatá	k1gFnSc2	svatá
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
věřících	věřící	k1gMnPc2	věřící
obětován	obětován	k2eAgMnSc1d1	obětován
pouze	pouze	k6eAd1	pouze
chléb	chléb	k1gInSc4	chléb
a	a	k8xC	a
víno	víno	k1gNnSc4	víno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
k	k	k7c3	k
vínu	víno	k1gNnSc3	víno
byla	být	k5eAaImAgFnS	být
přimíchávána	přimícháván	k2eAgFnSc1d1	přimícháván
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
chrámech	chrám	k1gInPc6	chrám
i	i	k8xC	i
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
uchovávána	uchovávat	k5eAaImNgFnS	uchovávat
svěcená	svěcený	k2eAgFnSc1d1	svěcená
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
zavedl	zavést	k5eAaPmAgInS	zavést
užití	užití	k1gNnSc4	užití
svěcené	svěcený	k2eAgFnSc2d1	svěcená
vody	voda	k1gFnSc2	voda
smíchané	smíchaný	k2eAgFnSc2d1	smíchaná
se	se	k3xPyFc4	se
solí	sůl	k1gFnSc7	sůl
k	k	k7c3	k
očištění	očištění	k1gNnSc3	očištění
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
domácností	domácnost	k1gFnPc2	domácnost
od	od	k7c2	od
ďábelských	ďábelský	k2eAgInPc2d1	ďábelský
vlivů	vliv	k1gInPc2	vliv
(	(	kIx(	(
<g/>
constituit	constituit	k2eAgInSc1d1	constituit
aquam	aquam	k1gInSc1	aquam
sparsionis	sparsionis	k1gFnSc1	sparsionis
cum	cum	k?	cum
sale	salat	k5eAaPmIp3nS	salat
benedici	benedice	k1gFnSc3	benedice
in	in	k?	in
habitaculis	habitaculis	k1gInSc1	habitaculis
hominum	hominum	k1gInSc1	hominum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
mešního	mešní	k2eAgInSc2d1	mešní
obřadu	obřad	k1gInSc2	obřad
zavedl	zavést	k5eAaPmAgInS	zavést
kánon	kánon	k1gInSc1	kánon
"	"	kIx"	"
<g/>
Qui	Qui	k1gFnSc1	Qui
Pridie	Pridie	k1gFnSc2	Pridie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
byla	být	k5eAaImAgFnS	být
nedaleko	nedaleko	k7c2	nedaleko
Říma	Řím	k1gInSc2	Řím
objevena	objevit	k5eAaPmNgFnS	objevit
hrobka	hrobka	k1gFnSc1	hrobka
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
svědčícím	svědčící	k2eAgInSc7d1	svědčící
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
pochováni	pochován	k2eAgMnPc1d1	pochován
mučedníci	mučedník	k1gMnPc1	mučedník
Alexander	Alexandra	k1gFnPc2	Alexandra
<g/>
,	,	kIx,	,
Eventulus	Eventulus	k1gMnSc1	Eventulus
a	a	k8xC	a
Theodulus	Theodulus	k1gMnSc1	Theodulus
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
archeologové	archeolog	k1gMnPc1	archeolog
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
Alexander	Alexandra	k1gFnPc2	Alexandra
je	být	k5eAaImIp3nS	být
totožný	totožný	k2eAgMnSc1d1	totožný
s	s	k7c7	s
Alexandrem	Alexandr	k1gMnSc7	Alexandr
I.	I.	kA	I.
a	a	k8xC	a
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
skutečného	skutečný	k2eAgNnSc2d1	skutečné
umučení	umučení	k1gNnSc2	umučení
svatého	svatý	k2eAgMnSc2d1	svatý
Alexandra	Alexandr	k1gMnSc2	Alexandr
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
ovšem	ovšem	k9	ovšem
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
mučednickou	mučednický	k2eAgFnSc4d1	mučednická
smrt	smrt	k1gFnSc4	smrt
papeže	papež	k1gMnSc4	papež
Alexandra	Alexandr	k1gMnSc4	Alexandr
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
historické	historický	k2eAgNnSc4d1	historické
splynutí	splynutí	k1gNnSc4	splynutí
dvou	dva	k4xCgFnPc2	dva
různých	různý	k2eAgFnPc2d1	různá
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Památku	památka	k1gFnSc4	památka
svatého	svatý	k2eAgMnSc2d1	svatý
Alexandra	Alexandr	k1gMnSc2	Alexandr
I.	I.	kA	I.
si	se	k3xPyFc3	se
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
připomíná	připomínat	k5eAaImIp3nS	připomínat
v	v	k7c4	v
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gNnSc2	jeho
údajného	údajný	k2eAgNnSc2d1	údajné
stětí	stětí	k1gNnSc2	stětí
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
GELMI	GELMI	kA	GELMI
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
:	:	kIx,	:
Od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
po	po	k7c4	po
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
457	[number]	k4	457
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAXWELL-STUART	MAXWELL-STUART	k?	MAXWELL-STUART
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
G.	G.	kA	G.
Papežové	Papež	k1gMnPc1	Papež
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
servis	servis	k1gInSc1	servis
<g/>
)	)	kIx)	)
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902300	[number]	k4	902300
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
:	:	kIx,	:
životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
