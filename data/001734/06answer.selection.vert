<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
koncepční	koncepční	k2eAgNnSc4d1	koncepční
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
příběhu	příběh	k1gInSc6	příběh
z	z	k7c2	z
novely	novela	k1gFnSc2	novela
Farma	farma	k1gFnSc1	farma
zvířat	zvíře	k1gNnPc2	zvíře
od	od	k7c2	od
George	Georg	k1gFnSc2	Georg
Orwella	Orwello	k1gNnSc2	Orwello
<g/>
.	.	kIx.	.
</s>
