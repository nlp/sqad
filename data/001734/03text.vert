<s>
Animals	Animals	k6eAd1	Animals
je	být	k5eAaImIp3nS	být
desáté	desátý	k4xOgNnSc4	desátý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
anglické	anglický	k2eAgFnSc2d1	anglická
skupiny	skupina	k1gFnSc2	skupina
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1977	[number]	k4	1977
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
1977	[number]	k4	1977
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
žebříčku	žebříček	k1gInSc6	žebříček
prodejnosti	prodejnost	k1gFnSc2	prodejnost
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
nejlépe	dobře	k6eAd3	dobře
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
koncepční	koncepční	k2eAgNnSc4d1	koncepční
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
příběhu	příběh	k1gInSc6	příběh
z	z	k7c2	z
novely	novela	k1gFnSc2	novela
Farma	farma	k1gFnSc1	farma
zvířat	zvíře	k1gNnPc2	zvíře
od	od	k7c2	od
George	Georg	k1gFnSc2	Georg
Orwella	Orwello	k1gNnSc2	Orwello
<g/>
.	.	kIx.	.
</s>
<s>
Animals	Animals	k6eAd1	Animals
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
autorské	autorský	k2eAgFnSc6d1	autorská
stránce	stránka	k1gFnSc6	stránka
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc1d1	celé
dílem	dílo	k1gNnSc7	dílo
baskytaristy	baskytarista	k1gMnSc2	baskytarista
<g/>
,	,	kIx,	,
zpěváka	zpěvák	k1gMnSc2	zpěvák
<g/>
,	,	kIx,	,
textaře	textař	k1gMnSc2	textař
a	a	k8xC	a
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
hlavního	hlavní	k2eAgMnSc2d1	hlavní
frontmana	frontman	k1gMnSc2	frontman
skupiny	skupina	k1gFnSc2	skupina
Rogera	Roger	k1gMnSc2	Roger
Waterse	Waterse	k1gFnSc2	Waterse
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
David	David	k1gMnSc1	David
Gilmour	Gilmour	k1gMnSc1	Gilmour
se	se	k3xPyFc4	se
autorsky	autorsky	k6eAd1	autorsky
podílel	podílet	k5eAaImAgInS	podílet
jen	jen	k9	jen
na	na	k7c6	na
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Dogs	Dogs	k1gInSc1	Dogs
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
autorský	autorský	k2eAgInSc1d1	autorský
podíl	podíl	k1gInSc1	podíl
klávesisty	klávesista	k1gMnSc2	klávesista
Ricka	Ricek	k1gMnSc2	Ricek
Wrighta	Wright	k1gMnSc2	Wright
je	být	k5eAaImIp3nS	být
nulový	nulový	k2eAgInSc1d1	nulový
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
celkovému	celkový	k2eAgNnSc3d1	celkové
aranžmá	aranžmá	k1gNnSc3	aranžmá
a	a	k8xC	a
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
skladbách	skladba	k1gFnPc6	skladba
hraje	hrát	k5eAaImIp3nS	hrát
sólo	sólo	k1gNnSc1	sólo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Animals	Animals	k1gInSc4	Animals
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc4	pět
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
album	album	k1gNnSc4	album
otevírají	otevírat	k5eAaImIp3nP	otevírat
a	a	k8xC	a
uzavírají	uzavírat	k5eAaImIp3nP	uzavírat
dvě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
akustické	akustický	k2eAgFnSc2d1	akustická
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Pigs	Pigs	k1gInSc1	Pigs
on	on	k3xPp3gInSc1	on
the	the	k?	the
Wing	Wing	k1gInSc1	Wing
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tři	tři	k4xCgFnPc1	tři
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
skladby	skladba	k1gFnPc1	skladba
"	"	kIx"	"
<g/>
Dogs	Dogs	k1gInSc1	Dogs
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Pigs	Pigs	k1gInSc1	Pigs
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Sheep	Sheep	k1gInSc1	Sheep
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Dogs	Dogs	k1gInSc1	Dogs
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
"	"	kIx"	"
<g/>
You	You	k1gFnSc4	You
Gotta	Gott	k1gMnSc2	Gott
Be	Be	k1gMnSc2	Be
Crazy	Craza	k1gFnSc2	Craza
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Sheep	Sheep	k1gInSc1	Sheep
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
hrána	hrát	k5eAaImNgFnS	hrát
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Raving	Raving	k1gInSc1	Raving
and	and	k?	and
Drolling	Drolling	k1gInSc1	Drolling
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
společně	společně	k6eAd1	společně
se	s	k7c7	s
"	"	kIx"	"
<g/>
Shine	Shin	k1gInSc5	Shin
On	on	k3xPp3gMnSc1	on
You	You	k1gMnSc1	You
Crazy	Craza	k1gFnSc2	Craza
Diamond	Diamond	k1gMnSc1	Diamond
<g/>
"	"	kIx"	"
a	a	k8xC	a
původně	původně	k6eAd1	původně
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
na	na	k7c4	na
album	album	k1gNnSc4	album
Wish	Wisha	k1gFnPc2	Wisha
You	You	k1gMnSc2	You
Were	Wer	k1gMnSc2	Wer
Here	Her	k1gMnSc2	Her
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
"	"	kIx"	"
<g/>
Pigs	Pigs	k1gInSc1	Pigs
(	(	kIx(	(
<g/>
Three	Three	k1gFnSc1	Three
Different	Different	k1gMnSc1	Different
Ones	Ones	k1gInSc1	Ones
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Pigs	Pigs	k1gInSc1	Pigs
on	on	k3xPp3gInSc1	on
the	the	k?	the
Wing	Wing	k1gInSc1	Wing
<g/>
"	"	kIx"	"
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
koncepci	koncepce	k1gFnSc4	koncepce
alba	album	k1gNnSc2	album
Animals	Animalsa	k1gFnPc2	Animalsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
nahrávaní	nahrávaný	k2eAgMnPc1d1	nahrávaný
Animals	Animals	k1gInSc4	Animals
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
prvním	první	k4xOgNnPc3	první
náznakům	náznak	k1gInPc3	náznak
vznikajícího	vznikající	k2eAgNnSc2d1	vznikající
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
členy	člen	k1gMnPc7	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Koncepce	koncepce	k1gFnSc1	koncepce
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
Farmou	farma	k1gFnSc7	farma
zvířat	zvíře	k1gNnPc2	zvíře
od	od	k7c2	od
George	Georg	k1gFnSc2	Georg
Orwella	Orwello	k1gNnSc2	Orwello
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
hlavních	hlavní	k2eAgFnPc2d1	hlavní
skladeb	skladba	k1gFnPc2	skladba
odráží	odrážet	k5eAaImIp3nS	odrážet
víru	víra	k1gFnSc4	víra
v	v	k7c4	v
třídní	třídní	k2eAgInSc4d1	třídní
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kapitalistické	kapitalistický	k2eAgFnSc6d1	kapitalistická
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
jsou	být	k5eAaImIp3nP	být
o	o	k7c6	o
třech	tři	k4xCgInPc6	tři
druzích	druh	k1gInPc6	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
lidské	lidský	k2eAgFnPc4d1	lidská
vlastnosti	vlastnost	k1gFnPc4	vlastnost
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
ve	v	k7c6	v
zmíněné	zmíněný	k2eAgFnSc6d1	zmíněná
knize	kniha	k1gFnSc6	kniha
<g/>
:	:	kIx,	:
Psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Dogs	Dogs	k1gInSc1	Dogs
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
o	o	k7c6	o
bezohledných	bezohledný	k2eAgMnPc6d1	bezohledný
megalomanských	megalomanský	k2eAgMnPc6d1	megalomanský
byznysmenech	byznysmen	k1gMnPc6	byznysmen
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
vyšší	vysoký	k2eAgFnSc6d2	vyšší
střední	střední	k2eAgFnSc6d1	střední
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
konci	konec	k1gInSc6	konec
stáhnou	stáhnout	k5eAaPmIp3nP	stáhnout
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
i	i	k8xC	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
okolo	okolo	k7c2	okolo
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Prasata	prase	k1gNnPc4	prase
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pigs	Pigs	k1gInSc1	Pigs
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
o	o	k7c6	o
politicích	politik	k1gMnPc6	politik
a	a	k8xC	a
názorotvorných	názorotvorný	k2eAgFnPc6d1	názorotvorný
vrstvách	vrstva	k1gFnPc6	vrstva
<g/>
,	,	kIx,	,
vlastnících	vlastnící	k2eAgFnPc2d1	vlastnící
obrovských	obrovský	k2eAgFnPc2d1	obrovská
korporací	korporace	k1gFnPc2	korporace
(	(	kIx(	(
<g/>
o	o	k7c6	o
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
vrstvách	vrstva	k1gFnPc6	vrstva
<g/>
)	)	kIx)	)
částečně	částečně	k6eAd1	částečně
reprezentovaných	reprezentovaný	k2eAgInPc2d1	reprezentovaný
Mary	Mary	k1gFnPc7	Mary
Whitehousovou	Whitehousův	k2eAgFnSc7d1	Whitehousův
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
tvorby	tvorba	k1gFnSc2	tvorba
alba	album	k1gNnSc2	album
velmi	velmi	k6eAd1	velmi
snažila	snažit	k5eAaImAgFnS	snažit
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
silnému	silný	k2eAgInSc3d1	silný
politickému	politický	k2eAgInSc3d1	politický
podtextu	podtext	k1gInSc3	podtext
tvorby	tvorba	k1gFnSc2	tvorba
cenzurovat	cenzurovat	k5eAaImF	cenzurovat
produkci	produkce	k1gFnSc4	produkce
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
Music	Music	k1gMnSc1	Music
<g/>
.	.	kIx.	.
</s>
<s>
Ovce	ovce	k1gFnSc1	ovce
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Sheep	Sheep	k1gInSc1	Sheep
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
o	o	k7c6	o
všech	všecek	k3xTgNnPc6	všecek
ostatních	ostatní	k1gNnPc6	ostatní
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nespadají	spadat	k5eNaPmIp3nP	spadat
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
vyjmenovaných	vyjmenovaný	k2eAgFnPc2d1	vyjmenovaná
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Pracující	pracující	k2eAgFnSc3d1	pracující
třídě	třída	k1gFnSc3	třída
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
respektuje	respektovat	k5eAaImIp3nS	respektovat
politiky	politik	k1gMnPc4	politik
a	a	k8xC	a
management	management	k1gInSc4	management
slepě	slepě	k6eAd1	slepě
a	a	k8xC	a
bez	bez	k7c2	bez
vlastních	vlastní	k2eAgFnPc2d1	vlastní
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
tři	tři	k4xCgFnPc1	tři
skladby	skladba	k1gFnPc1	skladba
jsou	být	k5eAaImIp3nP	být
ohraničeny	ohraničen	k2eAgFnPc1d1	ohraničena
dvěma	dva	k4xCgFnPc7	dva
částmi	část	k1gFnPc7	část
Watersovy	Watersův	k2eAgFnSc2d1	Watersova
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
Pigs	Pigs	k1gInSc1	Pigs
on	on	k3xPp3gInSc1	on
the	the	k?	the
Wing	Wing	k1gInSc1	Wing
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
věnoval	věnovat	k5eAaPmAgMnS	věnovat
své	svůj	k3xOyFgMnPc4	svůj
tehdejší	tehdejší	k2eAgMnPc4d1	tehdejší
manželce	manželka	k1gFnSc3	manželka
Caroline	Carolin	k1gInSc5	Carolin
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
části	část	k1gFnSc6	část
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
nenávistně	nenávistně	k6eAd1	nenávistně
laděnými	laděný	k2eAgFnPc7d1	laděná
skladbami	skladba	k1gFnPc7	skladba
uprostřed	uprostřed	k7c2	uprostřed
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
chtěly	chtít	k5eAaImAgFnP	chtít
naznačit	naznačit	k5eAaPmF	naznačit
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc1	jejich
téma	téma	k1gNnSc1	téma
a	a	k8xC	a
spojitost	spojitost	k1gFnSc1	spojitost
dokáže	dokázat	k5eAaPmIp3nS	dokázat
pomoci	pomoct	k5eAaPmF	pomoct
překonat	překonat	k5eAaPmF	překonat
naznačené	naznačený	k2eAgInPc4d1	naznačený
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Obal	obal	k1gInSc1	obal
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Storm	Storm	k1gInSc4	Storm
Thorgerson	Thorgersona	k1gFnPc2	Thorgersona
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Hipgnosis	Hipgnosis	k1gFnSc2	Hipgnosis
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
londýnskou	londýnský	k2eAgFnSc4d1	londýnská
elektrárnu	elektrárna	k1gFnSc4	elektrárna
Battersea	Batterseum	k1gNnSc2	Batterseum
se	s	k7c7	s
vznášejícím	vznášející	k2eAgMnSc7d1	vznášející
se	se	k3xPyFc4	se
prasetem	prase	k1gNnSc7	prase
<g/>
.	.	kIx.	.
</s>
<s>
Obrovské	obrovský	k2eAgNnSc1d1	obrovské
nafukovací	nafukovací	k2eAgNnSc1d1	nafukovací
prase	prase	k1gNnSc1	prase
naplněné	naplněný	k2eAgNnSc1d1	naplněné
heliem	helium	k1gNnSc7	helium
bylo	být	k5eAaImAgNnS	být
skutečně	skutečně	k6eAd1	skutečně
vypuštěno	vypuštěn	k2eAgNnSc1d1	vypuštěno
a	a	k8xC	a
třikrát	třikrát	k6eAd1	třikrát
vyfoceno	vyfotit	k5eAaPmNgNnS	vyfotit
nad	nad	k7c7	nad
elektrárnou	elektrárna	k1gFnSc7	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
den	den	k1gInSc4	den
měli	mít	k5eAaImAgMnP	mít
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
prase	prase	k1gNnSc1	prase
uletělo	uletět	k5eAaPmAgNnS	uletět
<g/>
,	,	kIx,	,
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
odstřelovače	odstřelovač	k1gMnSc2	odstřelovač
<g/>
.	.	kIx.	.
</s>
<s>
Thorgenson	Thorgenson	k1gMnSc1	Thorgenson
jej	on	k3xPp3gMnSc4	on
považoval	považovat	k5eAaImAgMnS	považovat
jen	jen	k9	jen
za	za	k7c4	za
pojistku	pojistka	k1gFnSc4	pojistka
a	a	k8xC	a
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
fotografování	fotografování	k1gNnSc2	fotografování
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
ho	on	k3xPp3gMnSc4	on
už	už	k6eAd1	už
nepřizval	přizvat	k5eNaPmAgMnS	přizvat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
ale	ale	k8xC	ale
nevydržela	vydržet	k5eNaPmAgNnP	vydržet
kotvící	kotvící	k2eAgNnPc1d1	kotvící
lana	lano	k1gNnPc1	lano
a	a	k8xC	a
prase	prase	k1gNnSc1	prase
uletělo	uletět	k5eAaPmAgNnS	uletět
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yRnSc3	což
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
pozastaven	pozastaven	k2eAgInSc4d1	pozastaven
provoz	provoz	k1gInSc4	provoz
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Heathrow	Heathrow	k1gFnSc2	Heathrow
<g/>
.	.	kIx.	.
</s>
<s>
Prase	prase	k1gNnSc1	prase
nakonec	nakonec	k6eAd1	nakonec
bez	bez	k7c2	bez
většího	veliký	k2eAgNnSc2d2	veliký
poškození	poškození	k1gNnSc2	poškození
přistálo	přistát	k5eAaPmAgNnS	přistát
asi	asi	k9	asi
50	[number]	k4	50
km	km	kA	km
od	od	k7c2	od
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opravě	oprava	k1gFnSc6	oprava
bylo	být	k5eAaImAgNnS	být
nad	nad	k7c4	nad
elektrárnu	elektrárna	k1gFnSc4	elektrárna
vypuštěno	vypuštěn	k2eAgNnSc1d1	vypuštěno
i	i	k9	i
třetí	třetí	k4xOgInSc4	třetí
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výslednými	výsledný	k2eAgFnPc7d1	výsledná
fotografiemi	fotografia	k1gFnPc7	fotografia
ale	ale	k8xC	ale
autoři	autor	k1gMnPc1	autor
nebyli	být	k5eNaImAgMnP	být
spokojení	spokojení	k1gNnSc4	spokojení
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
obloha	obloha	k1gFnSc1	obloha
z	z	k7c2	z
třetího	třetí	k4xOgInSc2	třetí
dne	den	k1gInSc2	den
nebyla	být	k5eNaImAgFnS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pro	pro	k7c4	pro
koláž	koláž	k1gFnSc4	koláž
fotografií	fotografia	k1gFnPc2	fotografia
elektrárny	elektrárna	k1gFnSc2	elektrárna
se	s	k7c7	s
zamračenou	zamračený	k2eAgFnSc7d1	zamračená
oblohou	obloha	k1gFnSc7	obloha
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
dne	den	k1gInSc2	den
a	a	k8xC	a
prasete	prase	k1gNnSc2	prase
z	z	k7c2	z
dne	den	k1gInSc2	den
třetího	třetí	k4xOgNnSc2	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Animals	Animalsa	k1gFnPc2	Animalsa
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1977	[number]	k4	1977
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
USA	USA	kA	USA
relativně	relativně	k6eAd1	relativně
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
(	(	kIx(	(
<g/>
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
třetí	třetí	k4xOgFnPc4	třetí
příčky	příčka	k1gFnPc4	příčka
v	v	k7c6	v
žebříčku	žebříčko	k1gNnSc6	žebříčko
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
kritiky	kritika	k1gFnSc2	kritika
bylo	být	k5eAaImAgNnS	být
bráno	brát	k5eAaImNgNnS	brát
za	za	k7c4	za
určité	určitý	k2eAgNnSc4d1	určité
zklamání	zklamání	k1gNnSc4	zklamání
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
úrovni	úroveň	k1gFnSc3	úroveň
předchozích	předchozí	k2eAgNnPc2d1	předchozí
alb	album	k1gNnPc2	album
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
amerických	americký	k2eAgInPc6d1	americký
žebříčcích	žebříček	k1gInPc6	žebříček
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
udrželo	udržet	k5eAaPmAgNnS	udržet
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc1	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
druhého	druhý	k4xOgNnSc2	druhý
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Animals	Animals	k6eAd1	Animals
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
i	i	k9	i
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
osmistopé	osmistopý	k2eAgFnSc6d1	osmistopý
kazetě	kazeta	k1gFnSc6	kazeta
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
-track	racko	k1gNnPc2	-tracko
cartridge	cartridge	k1gFnPc2	cartridge
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
má	mít	k5eAaImIp3nS	mít
odlišné	odlišný	k2eAgNnSc4d1	odlišné
pořadí	pořadí	k1gNnSc4	pořadí
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Dogs	Dogs	k1gInSc1	Dogs
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
"	"	kIx"	"
<g/>
Pigs	Pigs	k1gInSc1	Pigs
on	on	k3xPp3gInSc1	on
the	the	k?	the
Wing	Wing	k1gInSc1	Wing
<g/>
"	"	kIx"	"
spojena	spojit	k5eAaPmNgFnS	spojit
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
pomocí	pomocí	k7c2	pomocí
kytarového	kytarový	k2eAgNnSc2d1	kytarové
sóla	sólo	k1gNnSc2	sólo
Snowyho	Snowy	k1gMnSc2	Snowy
Whita	Whit	k1gMnSc2	Whit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
CD	CD	kA	CD
album	album	k1gNnSc1	album
poprvé	poprvé	k6eAd1	poprvé
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
v	v	k7c6	v
digitálně	digitálně	k6eAd1	digitálně
remasterované	remasterovaný	k2eAgFnSc6d1	remasterovaná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
(	(	kIx(	(
<g/>
i	i	k9	i
<g/>
)	)	kIx)	)
Roger	Roger	k1gInSc1	Roger
Waters	Watersa	k1gFnPc2	Watersa
<g/>
.	.	kIx.	.
</s>
<s>
Pink	pink	k2eAgMnSc1d1	pink
Floyd	Floyd	k1gMnSc1	Floyd
<g/>
:	:	kIx,	:
David	David	k1gMnSc1	David
Gilmour	Gilmour	k1gMnSc1	Gilmour
-	-	kIx~	-
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
syntezátor	syntezátor	k1gInSc1	syntezátor
<g/>
,	,	kIx,	,
talk	talk	k1gInSc1	talk
box	box	k1gInSc1	box
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Dogs	Dogs	k1gInSc1	Dogs
<g/>
"	"	kIx"	"
Roger	Roger	k1gInSc1	Roger
Waters	Waters	k1gInSc1	Waters
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
akustická	akustický	k2eAgFnSc1d1	akustická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Rick	Rick	k1gMnSc1	Rick
Wright	Wright	k1gMnSc1	Wright
-	-	kIx~	-
Hammondovy	Hammondův	k2eAgFnPc1d1	Hammondova
varhany	varhany	k1gFnPc1	varhany
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
elektrické	elektrický	k2eAgNnSc4d1	elektrické
piano	piano	k1gNnSc4	piano
<g/>
,	,	kIx,	,
piano	piano	k1gNnSc4	piano
<g/>
,	,	kIx,	,
synzezátor	synzezátor	k1gInSc4	synzezátor
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc4	vokál
Nick	Nick	k1gMnSc1	Nick
Mason	mason	k1gMnSc1	mason
-	-	kIx~	-
bicí	bicí	k2eAgFnSc2d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnSc2	perkuse
Snowy	Snowa	k1gFnSc2	Snowa
White	Whit	k1gInSc5	Whit
-	-	kIx~	-
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Pigs	Pigs	k1gInSc1	Pigs
on	on	k3xPp3gInSc1	on
the	the	k?	the
Wing	Wing	k1gInSc1	Wing
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
na	na	k7c6	na
osmistopé	osmistopý	k2eAgFnSc6d1	osmistopý
kazetě	kazeta	k1gFnSc6	kazeta
<g/>
)	)	kIx)	)
Brian	Brian	k1gMnSc1	Brian
Humphries	Humphries	k1gMnSc1	Humphries
-	-	kIx~	-
zvukový	zvukový	k2eAgMnSc1d1	zvukový
inženýr	inženýr	k1gMnSc1	inženýr
Storm	Storm	k1gInSc4	Storm
Thorgerson	Thorgerson	k1gNnSc1	Thorgerson
<g/>
,	,	kIx,	,
Aubery	Auber	k1gInPc1	Auber
Powell	Powell	k1gInSc1	Powell
-	-	kIx~	-
design	design	k1gInSc1	design
James	James	k1gMnSc1	James
Guthrie	Guthrie	k1gFnSc1	Guthrie
<g/>
,	,	kIx,	,
Doug	Doug	k1gInSc1	Doug
Sax	sax	k1gInSc1	sax
-	-	kIx~	-
remastering	remastering	k1gInSc1	remastering
Informace	informace	k1gFnSc2	informace
o	o	k7c6	o
albu	album	k1gNnSc6	album
<g/>
,	,	kIx,	,
pinkfloyd	pinkfloyd	k1gMnSc1	pinkfloyd
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
