<s>
Černobyl	Černobyl	k1gInSc1	Černobyl
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
Ч	Ч	k?	Ч
<g/>
́	́	k?	́
<g/>
б	б	k?	б
<g/>
,	,	kIx,	,
Čornobyl	Čornobyl	k1gInSc1	Čornobyl
<g/>
;	;	kIx,	;
rusky	rusky	k6eAd1	rusky
Ч	Ч	k?	Ч
<g/>
́	́	k?	́
<g/>
б	б	k?	б
<g/>
,	,	kIx,	,
Černobyl	Černobyl	k1gInSc1	Černobyl
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
zvaném	zvaný	k2eAgInSc6d1	zvaný
Polesí	Polesí	k1gNnSc3	Polesí
<g/>
.	.	kIx.	.
</s>
