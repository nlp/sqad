<s>
Černobyl	Černobyl	k1gInSc1	Černobyl
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
Ч	Ч	k?	Ч
<g/>
́	́	k?	́
<g/>
б	б	k?	б
<g/>
,	,	kIx,	,
Čornobyl	Čornobyl	k1gInSc1	Čornobyl
<g/>
;	;	kIx,	;
rusky	rusky	k6eAd1	rusky
Ч	Ч	k?	Ч
<g/>
́	́	k?	́
<g/>
б	б	k?	б
<g/>
,	,	kIx,	,
Černobyl	Černobyl	k1gInSc1	Černobyl
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
zvaném	zvaný	k2eAgInSc6d1	zvaný
Polesí	Polesí	k1gNnSc3	Polesí
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
jím	on	k3xPp3gNnSc7	on
řeka	řeka	k1gFnSc1	řeka
Pripjať	Pripjať	k1gFnPc2	Pripjať
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
neslavně	slavně	k6eNd1	slavně
proslulé	proslulý	k2eAgFnPc1d1	proslulá
havárií	havárie	k1gFnSc7	havárie
jaderné	jaderný	k2eAgFnPc1d1	jaderná
elektrárny	elektrárna	k1gFnPc1	elektrárna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
rozsahem	rozsah	k1gInSc7	rozsah
následků	následek	k1gInPc2	následek
nejhorší	zlý	k2eAgInSc4d3	Nejhorší
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energetiky	energetika	k1gFnSc2	energetika
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
města	město	k1gNnSc2	město
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1193	[number]	k4	1193
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
kozáckými	kozácký	k2eAgFnPc7d1	kozácká
válkami	válka	k1gFnPc7	válka
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
komunikační	komunikační	k2eAgInSc1d1	komunikační
uzel	uzel	k1gInSc1	uzel
a	a	k8xC	a
středisko	středisko	k1gNnSc1	středisko
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
převažovalo	převažovat	k5eAaImAgNnS	převažovat
židovské	židovský	k2eAgNnSc4d1	Židovské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
Černobyl	Černobyl	k1gInSc1	Černobyl
také	také	k9	také
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
chasidismu	chasidismus	k1gInSc2	chasidismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
města	město	k1gNnSc2	město
budována	budován	k2eAgFnSc1d1	budována
jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
a	a	k8xC	a
pro	pro	k7c4	pro
její	její	k3xOp3gMnPc4	její
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
nové	nový	k2eAgNnSc1d1	nové
město	město	k1gNnSc1	město
Pripjať	Pripjať	k1gFnSc2	Pripjať
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
byl	být	k5eAaImAgInS	být
Černobyl	Černobyl	k1gInSc1	Černobyl
evakuován	evakuovat	k5eAaBmNgInS	evakuovat
<g/>
,	,	kIx,	,
po	po	k7c6	po
postupné	postupný	k2eAgFnSc6d1	postupná
dekontaminaci	dekontaminace	k1gFnSc6	dekontaminace
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
částečně	částečně	k6eAd1	částečně
obyvatelný	obyvatelný	k2eAgMnSc1d1	obyvatelný
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Černobylská	černobylský	k2eAgFnSc1d1	Černobylská
jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
a	a	k8xC	a
Černobylská	černobylský	k2eAgFnSc1d1	Černobylská
havárie	havárie	k1gFnSc1	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
byl	být	k5eAaImAgInS	být
18	[number]	k4	18
kilometrů	kilometr	k1gInPc2	kilometr
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
dobudován	dobudován	k2eAgMnSc1d1	dobudován
1	[number]	k4	1
<g/>
.	.	kIx.	.
reaktor	reaktor	k1gInSc1	reaktor
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Černobyl	Černobyl	k1gInSc1	Černobyl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
v	v	k7c4	v
1	[number]	k4	1
hodinu	hodina	k1gFnSc4	hodina
23	[number]	k4	23
minut	minuta	k1gFnPc2	minuta
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
4	[number]	k4	4
<g/>
.	.	kIx.	.
reaktorovém	reaktorový	k2eAgInSc6d1	reaktorový
bloku	blok	k1gInSc6	blok
k	k	k7c3	k
dosud	dosud	k6eAd1	dosud
největší	veliký	k2eAgFnSc3d3	veliký
zaznamenané	zaznamenaný	k2eAgFnSc3d1	zaznamenaná
havárii	havárie	k1gFnSc3	havárie
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Výbuch	výbuch	k1gInSc1	výbuch
způsobil	způsobit	k5eAaPmAgInS	způsobit
uvolnění	uvolnění	k1gNnSc4	uvolnění
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
130	[number]	k4	130
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
blízkého	blízký	k2eAgNnSc2d1	blízké
okolí	okolí	k1gNnSc2	okolí
bylo	být	k5eAaImAgNnS	být
evakuováno	evakuovat	k5eAaBmNgNnS	evakuovat
<g/>
.	.	kIx.	.
</s>
<s>
Evakuace	evakuace	k1gFnSc1	evakuace
však	však	k9	však
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
až	až	k9	až
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelům	obyvatel	k1gMnPc3	obyvatel
také	také	k9	také
dlouho	dlouho	k6eAd1	dlouho
nebylo	být	k5eNaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
děje	dít	k5eAaImIp3nS	dít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ani	ani	k8xC	ani
nevěděla	vědět	k5eNaImAgFnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
během	během	k7c2	během
evakuace	evakuace	k1gFnSc2	evakuace
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
ozařováni	ozařovat	k5eAaImNgMnP	ozařovat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
aspoň	aspoň	k9	aspoň
zmírňovat	zmírňovat	k5eAaImF	zmírňovat
<g/>
,	,	kIx,	,
následky	následek	k1gInPc1	následek
této	tento	k3xDgFnSc2	tento
havárie	havárie	k1gFnSc2	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
stále	stále	k6eAd1	stále
radioaktivní	radioaktivní	k2eAgFnSc1d1	radioaktivní
a	a	k8xC	a
město	město	k1gNnSc1	město
oficiálně	oficiálně	k6eAd1	oficiálně
opuštěné	opuštěný	k2eAgNnSc1d1	opuštěné
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
sedmi	sedm	k4xCc2	sedm
set	sto	k4xCgNnPc2	sto
zejména	zejména	k9	zejména
starých	starý	k2eAgMnPc2d1	starý
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Černobylu	Černobyl	k1gInSc6	Černobyl
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
3000	[number]	k4	3000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
úředníky	úředník	k1gMnPc4	úředník
<g/>
,	,	kIx,	,
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
zaměstnance	zaměstnanec	k1gMnSc4	zaměstnanec
elektrárny	elektrárna	k1gFnSc2	elektrárna
a	a	k8xC	a
pracovníky	pracovník	k1gMnPc7	pracovník
starající	starající	k2eAgFnSc2d1	starající
se	se	k3xPyFc4	se
o	o	k7c4	o
"	"	kIx"	"
<g/>
Zónu	zóna	k1gFnSc4	zóna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Střídají	střídat	k5eAaImIp3nP	střídat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
po	po	k7c6	po
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
pracovní	pracovní	k2eAgInPc4d1	pracovní
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Černobylu	Černobyl	k1gInSc6	Černobyl
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
žije	žít	k5eAaImIp3nS	žít
cca	cca	kA	cca
700	[number]	k4	700
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
do	do	k7c2	do
města	město	k1gNnSc2	město
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
.	.	kIx.	.
</s>
