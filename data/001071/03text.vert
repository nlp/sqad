<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Roithová	Roithová	k1gFnSc1	Roithová
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Tůmová	Tůmová	k1gFnSc1	Tůmová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1953	[number]	k4	1953
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
a	a	k8xC	a
manažerka	manažerka	k1gFnSc1	manažerka
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
povoláním	povolání	k1gNnSc7	povolání
lékařka	lékařka	k1gFnSc1	lékařka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byla	být	k5eAaImAgFnS	být
ministryní	ministryně	k1gFnSc7	ministryně
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Josefa	Josef	k1gMnSc2	Josef
Tošovského	Tošovský	k1gMnSc2	Tošovský
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
až	až	k9	až
2004	[number]	k4	2004
senátorkou	senátorka	k1gFnSc7	senátorka
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
2004-2014	[number]	k4	2004-2014
poslankyní	poslankyně	k1gFnPc2	poslankyně
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zvolena	zvolit	k5eAaPmNgFnS	zvolit
místopředsedkyní	místopředsedkyně	k1gFnSc7	místopředsedkyně
KDU-ČSL	KDU-ČSL	k1gMnSc2	KDU-ČSL
(	(	kIx(	(
<g/>
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
již	již	k6eAd1	již
zastávala	zastávat	k5eAaImAgFnS	zastávat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001	[number]	k4	2001
až	až	k9	až
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
je	být	k5eAaImIp3nS	být
zastupitelkou	zastupitelka	k1gFnSc7	zastupitelka
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1972-1978	[number]	k4	1972-1978
studovala	studovat	k5eAaImAgFnS	studovat
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
lékařství	lékařství	k1gNnSc2	lékařství
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Atestaci	atestace	k1gFnSc3	atestace
II	II	kA	II
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
Radiodiagnostika	radiodiagnostika	k1gFnSc1	radiodiagnostika
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992-1997	[number]	k4	1992-1997
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c4	na
Sheffield	Sheffield	k1gInSc4	Sheffield
Hallam	Hallam	k1gInSc4	Hallam
University	universita	k1gFnSc2	universita
titul	titul	k1gInSc1	titul
MBA	MBA	kA	MBA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1978-1979	[number]	k4	1978-1979
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
lékařka	lékařka	k1gFnSc1	lékařka
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
Berouně	Beroun	k1gInSc6	Beroun
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1980-1985	[number]	k4	1980-1985
ve	v	k7c6	v
Fakultní	fakultní	k2eAgFnSc6d1	fakultní
nemocnici	nemocnice	k1gFnSc6	nemocnice
Motol	Motol	k1gInSc1	Motol
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
ve	v	k7c6	v
Fakultní	fakultní	k2eAgFnSc6d1	fakultní
nemocnici	nemocnice	k1gFnSc6	nemocnice
Královské	královský	k2eAgInPc1d1	královský
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
její	její	k3xOp3gFnSc7	její
ředitelkou	ředitelka	k1gFnSc7	ředitelka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vdaná	vdaný	k2eAgFnSc1d1	vdaná
za	za	k7c2	za
sochaře	sochař	k1gMnSc2	sochař
Jana	Jan	k1gMnSc2	Jan
Roitha	Roith	k1gMnSc2	Roith
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
syna	syn	k1gMnSc4	syn
Matyáše	Matyáš	k1gMnSc4	Matyáš
a	a	k8xC	a
dva	dva	k4xCgMnPc4	dva
vnuky	vnuk	k1gMnPc4	vnuk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
jako	jako	k8xS	jako
nestranická	stranický	k2eNgFnSc1d1	nestranická
ministryně	ministryně	k1gFnSc1	ministryně
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Josefa	Josef	k1gMnSc2	Josef
Tošovského	Tošovský	k1gMnSc2	Tošovský
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byla	být	k5eAaImAgNnP	být
zvolena	zvolit	k5eAaPmNgNnP	zvolit
na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
čtyřkoalice	čtyřkoalice	k1gFnSc1	čtyřkoalice
senátorkou	senátorka	k1gFnSc7	senátorka
za	za	k7c4	za
volební	volební	k2eAgInSc4d1	volební
obvod	obvod	k1gInSc4	obvod
Praha	Praha	k1gFnSc1	Praha
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001-2003	[number]	k4	2001-2003
byla	být	k5eAaImAgNnP	být
její	její	k3xOp3gFnSc7	její
místopředsedkyní	místopředsedkyně	k1gFnSc7	místopředsedkyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000-2002	[number]	k4	2000-2002
působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Evropského	evropský	k2eAgNnSc2d1	Evropské
hnutí	hnutí	k1gNnSc2	hnutí
ČR	ČR	kA	ČR
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002-2005	[number]	k4	2002-2005
jako	jako	k8xC	jako
čestná	čestný	k2eAgFnSc1d1	čestná
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Nemocnice	nemocnice	k1gFnSc2	nemocnice
Milosrdných	milosrdný	k2eAgFnPc2d1	milosrdná
sester	sestra	k1gFnPc2	sestra
sv.	sv.	kA	sv.
Karla	Karel	k1gMnSc2	Karel
Boromejského	Boromejský	k2eAgMnSc2d1	Boromejský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
poslankyní	poslankyně	k1gFnSc7	poslankyně
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
za	za	k7c4	za
KDU-ČSL	KDU-ČSL	k1gFnSc4	KDU-ČSL
a	a	k8xC	a
v	v	k7c6	v
EP	EP	kA	EP
působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
Vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
trh	trh	k1gInSc4	trh
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
spotřebitelů	spotřebitel	k1gMnPc2	spotřebitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
svůj	svůj	k3xOyFgInSc4	svůj
mandát	mandát	k1gInSc4	mandát
obhájila	obhájit	k5eAaPmAgFnS	obhájit
na	na	k7c4	na
dalších	další	k2eAgInPc2d1	další
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
působila	působit	k5eAaImAgFnS	působit
ve	v	k7c6	v
Výboru	výbor	k1gInSc6	výbor
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
trh	trh	k1gInSc4	trh
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
spotřebitelů	spotřebitel	k1gMnPc2	spotřebitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
již	již	k9	již
nekandidovala	kandidovat	k5eNaImAgFnS	kandidovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
výběrem	výběr	k1gInSc7	výběr
českého	český	k2eAgMnSc2d1	český
eurokomisaře	eurokomisař	k1gMnSc2	eurokomisař
na	na	k7c6	na
období	období	k1gNnSc6	období
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
možné	možný	k2eAgFnSc6d1	možná
nominaci	nominace	k1gFnSc6	nominace
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
<g/>
.	.	kIx.	.
</s>
<s>
Roithová	Roithovat	k5eAaImIp3nS	Roithovat
se	se	k3xPyFc4	se
ale	ale	k9	ale
nakonec	nakonec	k6eAd1	nakonec
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c4	o
funkci	funkce	k1gFnSc4	funkce
neusilovat	usilovat	k5eNaImF	usilovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
slíbila	slíbit	k5eAaPmAgFnS	slíbit
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
vydané	vydaný	k2eAgFnSc2d1	vydaná
zprávy	zpráva	k1gFnSc2	zpráva
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgInSc2d1	uvedený
think-tanku	thinkanka	k1gFnSc4	think-tanka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
na	na	k7c6	na
období	období	k1gNnSc6	období
před	před	k7c7	před
následujícími	následující	k2eAgFnPc7d1	následující
volbami	volba	k1gFnPc7	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
následující	následující	k2eAgFnSc1d1	následující
<g/>
:	:	kIx,	:
Docházka	docházka	k1gFnSc1	docházka
-	-	kIx~	-
obsadila	obsadit	k5eAaPmAgFnS	obsadit
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
22	[number]	k4	22
českých	český	k2eAgInPc2d1	český
europoslanců	europoslanec	k1gInPc2	europoslanec
<g/>
,	,	kIx,	,
Účast	účast	k1gFnSc1	účast
na	na	k7c6	na
jmenovitých	jmenovitý	k2eAgFnPc6d1	jmenovitá
hlasování	hlasování	k1gNnSc6	hlasování
českých	český	k2eAgMnPc2d1	český
europoslanců	europoslanec	k1gMnPc2	europoslanec
-	-	kIx~	-
obsadila	obsadit	k5eAaPmAgFnS	obsadit
<g />
.	.	kIx.	.
</s>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
22	[number]	k4	22
českých	český	k2eAgInPc2d1	český
europoslanců	europoslanec	k1gInPc2	europoslanec
<g/>
,	,	kIx,	,
Zprávy	zpráva	k1gFnPc1	zpráva
předložené	předložený	k2eAgFnPc1d1	předložená
zpravodajem	zpravodaj	k1gInSc7	zpravodaj
českými	český	k2eAgInPc7d1	český
europoslanci	europoslanec	k1gInPc7	europoslanec
-	-	kIx~	-
obsadila	obsadit	k5eAaPmAgFnS	obsadit
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
22	[number]	k4	22
českých	český	k2eAgInPc2d1	český
europoslanců	europoslanec	k1gInPc2	europoslanec
<g/>
,	,	kIx,	,
Stanoviska	stanovisko	k1gNnSc2	stanovisko
předložená	předložený	k2eAgFnSc1d1	předložená
českými	český	k2eAgMnPc7d1	český
europoslanci	europoslanec	k1gMnPc7	europoslanec
-	-	kIx~	-
obsadila	obsadit	k5eAaPmAgFnS	obsadit
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
22	[number]	k4	22
českých	český	k2eAgInPc2d1	český
europoslanců	europoslanec	k1gInPc2	europoslanec
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Pozměňovací	pozměňovací	k2eAgInPc4d1	pozměňovací
návrhy	návrh	k1gInPc4	návrh
českých	český	k2eAgMnPc2d1	český
europoslanců	europoslanec	k1gMnPc2	europoslanec
-	-	kIx~	-
obsadila	obsadit	k5eAaPmAgFnS	obsadit
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
22	[number]	k4	22
českých	český	k2eAgInPc2d1	český
europoslanců	europoslanec	k1gInPc2	europoslanec
<g/>
,	,	kIx,	,
Parlamentní	parlamentní	k2eAgFnPc4d1	parlamentní
otázky	otázka	k1gFnPc4	otázka
českých	český	k2eAgMnPc2d1	český
europoslanců	europoslanec	k1gMnPc2	europoslanec
-	-	kIx~	-
obsadila	obsadit	k5eAaPmAgFnS	obsadit
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
22	[number]	k4	22
českých	český	k2eAgInPc2d1	český
europoslanců	europoslanec	k1gInPc2	europoslanec
<g/>
,	,	kIx,	,
Písemná	písemný	k2eAgNnPc4d1	písemné
prohlášení	prohlášení	k1gNnPc4	prohlášení
českých	český	k2eAgMnPc2d1	český
europoslanců	europoslanec	k1gMnPc2	europoslanec
-	-	kIx~	-
obsadila	obsadit	k5eAaPmAgFnS	obsadit
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
22	[number]	k4	22
českých	český	k2eAgInPc2d1	český
europoslanců	europoslanec	k1gInPc2	europoslanec
<g/>
,	,	kIx,	,
Návrhy	návrh	k1gInPc1	návrh
usnesení	usnesení	k1gNnSc2	usnesení
českých	český	k2eAgInPc2d1	český
europoslanců	europoslanec	k1gInPc2	europoslanec
-	-	kIx~	-
obsadila	obsadit	k5eAaPmAgFnS	obsadit
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
22	[number]	k4	22
českých	český	k2eAgInPc2d1	český
europoslanců	europoslanec	k1gInPc2	europoslanec
<g/>
,	,	kIx,	,
Vystoupení	vystoupení	k1gNnSc1	vystoupení
na	na	k7c6	na
plenárním	plenární	k2eAgNnSc6d1	plenární
zasedání	zasedání	k1gNnSc6	zasedání
českých	český	k2eAgMnPc2d1	český
europoslanců	europoslanec	k1gMnPc2	europoslanec
-	-	kIx~	-
obsadila	obsadit	k5eAaPmAgFnS	obsadit
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
22	[number]	k4	22
českých	český	k2eAgInPc2d1	český
europoslanců	europoslanec	k1gInPc2	europoslanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
stavěla	stavět	k5eAaImAgNnP	stavět
za	za	k7c4	za
přijetí	přijetí	k1gNnSc4	přijetí
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přesvědčovala	přesvědčovat	k5eAaImAgFnS	přesvědčovat
voliče	volič	k1gMnPc4	volič
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
nebáli	bát	k5eNaImAgMnP	bát
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
nekontrolovatelné	kontrolovatelný	k2eNgFnSc3d1	nekontrolovatelná
migraci	migrace	k1gFnSc3	migrace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
edukativní	edukativní	k2eAgNnSc4d1	edukativní
video	video	k1gNnSc4	video
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
stránek	stránka	k1gFnPc2	stránka
smazala	smazat	k5eAaPmAgFnS	smazat
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
manželské	manželský	k2eAgInPc1d1	manželský
svazky	svazek	k1gInPc1	svazek
homosexuálů	homosexuál	k1gMnPc2	homosexuál
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
adopce	adopce	k1gFnSc1	adopce
dětí	dítě	k1gFnPc2	dítě
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
jako	jako	k9	jako
pro	pro	k7c4	pro
politika	politik	k1gMnSc4	politik
důležitými	důležitý	k2eAgNnPc7d1	důležité
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
relativizují	relativizovat	k5eAaImIp3nP	relativizovat
význam	význam	k1gInSc4	význam
rodiny	rodina	k1gFnSc2	rodina
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
institut	institut	k1gInSc1	institut
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
českých	český	k2eAgMnPc2d1	český
europoslanců	europoslanec	k1gMnPc2	europoslanec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
proti	proti	k7c3	proti
Usnesení	usnesení	k1gNnSc3	usnesení
o	o	k7c6	o
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
homofobii	homofobie	k1gFnSc3	homofobie
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
8	[number]	k4	8
českých	český	k2eAgMnPc2d1	český
poslanců	poslanec	k1gMnPc2	poslanec
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
pro	pro	k7c4	pro
usnesení	usnesení	k1gNnSc4	usnesení
a	a	k8xC	a
7	[number]	k4	7
se	se	k3xPyFc4	se
hlasování	hlasování	k1gNnSc2	hlasování
zdrželo	zdržet	k5eAaPmAgNnS	zdržet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
na	na	k7c4	na
dotaz	dotaz	k1gInSc4	dotaz
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jí	on	k3xPp3gFnSc3	on
vadí	vadit	k5eAaImIp3nS	vadit
gay	gay	k1gMnSc1	gay
pochod	pochod	k1gInSc4	pochod
Prahou	Praha	k1gFnSc7	Praha
<g/>
,	,	kIx,	,
řekla	říct	k5eAaPmAgFnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ctím	ctít	k5eAaImIp1nS	ctít
právo	právo	k1gNnSc4	právo
různých	různý	k2eAgFnPc2d1	různá
skupin	skupina	k1gFnPc2	skupina
na	na	k7c6	na
vyjadřování	vyjadřování	k1gNnSc6	vyjadřování
názorů	názor	k1gInPc2	názor
a	a	k8xC	a
do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
patří	patřit	k5eAaImIp3nS	patřit
samozřejmě	samozřejmě	k6eAd1	samozřejmě
třeba	třeba	k6eAd1	třeba
i	i	k9	i
forma	forma	k1gFnSc1	forma
pochodu	pochod	k1gInSc2	pochod
Prahou	Praha	k1gFnSc7	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Nevadí	vadit	k5eNaImIp3nS	vadit
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
kritiku	kritika	k1gFnSc4	kritika
upřesnila	upřesnit	k5eAaPmAgFnS	upřesnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pochod	pochod	k1gInSc1	pochod
gayů	gay	k1gMnPc2	gay
Prahou	Praha	k1gFnSc7	Praha
nikdy	nikdy	k6eAd1	nikdy
neviděla	vidět	k5eNaImAgFnS	vidět
a	a	k8xC	a
ani	ani	k8xC	ani
by	by	kYmCp3nS	by
vidět	vidět	k5eAaImF	vidět
nechtěla	chtít	k5eNaImAgFnS	chtít
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
předpokládaná	předpokládaný	k2eAgFnSc1d1	předpokládaná
forma	forma	k1gFnSc1	forma
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
jako	jako	k8xS	jako
občanovi	občan	k1gMnSc3	občan
určitě	určitě	k6eAd1	určitě
nelíbila	líbit	k5eNaImAgFnS	líbit
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
mítinku	mítink	k1gInSc3	mítink
Mladých	mladý	k2eAgFnPc2d1	mladá
<g />
.	.	kIx.	.
</s>
<s>
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgMnS	konat
jako	jako	k8xC	jako
protiakce	protiakce	k1gFnSc1	protiakce
pochodu	pochod	k1gInSc2	pochod
Prague	Praguus	k1gMnSc5	Praguus
Pride	Prid	k1gMnSc5	Prid
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
pozdravu	pozdrav	k1gInSc6	pozdrav
řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
musíme	muset	k5eAaImIp1nP	muset
udělat	udělat	k5eAaPmF	udělat
vše	všechen	k3xTgNnSc4	všechen
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
udržela	udržet	k5eAaPmAgFnS	udržet
tradiční	tradiční	k2eAgFnSc1d1	tradiční
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
otiskla	otisknout	k5eAaPmAgFnS	otisknout
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
rozhovor	rozhovor	k1gInSc4	rozhovor
se	s	k7c7	s
Zuzanou	Zuzana	k1gFnSc7	Zuzana
Roithovou	Roithová	k1gFnSc7	Roithová
<g/>
.	.	kIx.	.
</s>
<s>
Novinářka	novinářka	k1gFnSc1	novinářka
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
mnoholetou	mnoholetý	k2eAgFnSc4d1	mnoholetá
političku	politička	k1gFnSc4	politička
s	s	k7c7	s
minimem	minimum	k1gNnSc7	minimum
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nepopouzí	popouzet	k5eNaImIp3nS	popouzet
<g/>
,	,	kIx,	,
neirituje	iritovat	k5eNaImIp3nS	iritovat
<g/>
,	,	kIx,	,
neprovokuje	provokovat	k5eNaImIp3nS	provokovat
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
žena	žena	k1gFnSc1	žena
mít	mít	k5eAaImF	mít
právo	právo	k1gNnSc4	právo
se	se	k3xPyFc4	se
samostatně	samostatně	k6eAd1	samostatně
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
projde	projít	k5eAaPmIp3nS	projít
umělým	umělý	k2eAgNnSc7d1	umělé
přerušením	přerušení	k1gNnSc7	přerušení
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
,	,	kIx,	,
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
to	ten	k3xDgNnSc4	ten
právo	právo	k1gNnSc4	právo
každá	každý	k3xTgFnSc1	každý
žena	žena	k1gFnSc1	žena
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dané	daný	k2eAgFnSc2d1	daná
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
osvěta	osvěta	k1gFnSc1	osvěta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
určitým	určitý	k2eAgInSc7d1	určitý
korektivem	korektiv	k1gInSc7	korektiv
pro	pro	k7c4	pro
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
hledala	hledat	k5eAaImAgFnS	hledat
jiné	jiný	k2eAgFnPc4d1	jiná
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
svoji	svůj	k3xOyFgFnSc4	svůj
situaci	situace	k1gFnSc4	situace
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Web	web	k1gInSc1	web
Hnutí	hnutí	k1gNnSc2	hnutí
pro	pro	k7c4	pro
život	život	k1gInSc4	život
následně	následně	k6eAd1	následně
otiskl	otisknout	k5eAaPmAgInS	otisknout
zprávu	zpráva	k1gFnSc4	zpráva
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
"	"	kIx"	"
<g/>
kdu	kdu	k?	kdu
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
prolife	prolif	k1gMnSc5	prolif
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
"	"	kIx"	"
a	a	k8xC	a
nadepsanou	nadepsaný	k2eAgFnSc4d1	nadepsaná
"	"	kIx"	"
<g/>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Roithová	Roithový	k2eAgFnSc1d1	Roithová
pro	pro	k7c4	pro
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
potrat	potrat	k1gInSc4	potrat
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
z	z	k7c2	z
rozhovoru	rozhovor	k1gInSc2	rozhovor
v	v	k7c6	v
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
citují	citovat	k5eAaBmIp3nP	citovat
pasáže	pasáž	k1gFnPc4	pasáž
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
potratů	potrat	k1gInPc2	potrat
a	a	k8xC	a
gay	gay	k1gMnSc1	gay
pochodu	pochod	k1gInSc6	pochod
Prahou	Praha	k1gFnSc7	Praha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dáváno	dávat	k5eAaImNgNnS	dávat
do	do	k7c2	do
protikladu	protiklad	k1gInSc2	protiklad
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
předseda	předseda	k1gMnSc1	předseda
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
Pavel	Pavel	k1gMnSc1	Pavel
Bělobrádek	Bělobrádek	k1gInSc4	Bělobrádek
přijal	přijmout	k5eAaPmAgMnS	přijmout
záštitu	záštita	k1gFnSc4	záštita
nad	nad	k7c7	nad
XII	XII	kA	XII
<g/>
.	.	kIx.	.
</s>
<s>
Pochodem	pochod	k1gInSc7	pochod
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
pořádaným	pořádaný	k2eAgMnSc7d1	pořádaný
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
iniciovat	iniciovat	k5eAaBmF	iniciovat
zrušení	zrušení	k1gNnSc4	zrušení
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
umělých	umělý	k2eAgInPc6d1	umělý
potratech	potrat	k1gInPc6	potrat
<g/>
.	.	kIx.	.
</s>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Roithová	Roithová	k1gFnSc1	Roithová
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
důrazně	důrazně	k6eAd1	důrazně
ohradila	ohradit	k5eAaPmAgFnS	ohradit
proti	proti	k7c3	proti
šíření	šíření	k1gNnSc3	šíření
zkresleného	zkreslený	k2eAgInSc2d1	zkreslený
výkladu	výklad	k1gInSc2	výklad
jejích	její	k3xOp3gInPc2	její
postojů	postoj	k1gInPc2	postoj
a	a	k8xC	a
vysvětlila	vysvětlit	k5eAaPmAgFnS	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
potrat	potrat	k1gInSc4	potrat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
náš	náš	k3xOp1gInSc1	náš
zákon	zákon	k1gInSc1	zákon
dává	dávat	k5eAaImIp3nS	dávat
právo	právo	k1gNnSc4	právo
ženám	žena	k1gFnPc3	žena
na	na	k7c6	na
ukončení	ukončení	k1gNnSc6	ukončení
těhotenství	těhotenství	k1gNnSc2	těhotenství
a	a	k8xC	a
toto	tento	k3xDgNnSc4	tento
právo	právo	k1gNnSc4	právo
dál	daleko	k6eAd2	daleko
trvá	trvat	k5eAaImIp3nS	trvat
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
předsedové	předseda	k1gMnPc1	předseda
největších	veliký	k2eAgFnPc2d3	veliký
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
stran	strana	k1gFnPc2	strana
hlásí	hlásit	k5eAaImIp3nP	hlásit
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
a	a	k8xC	a
vládne	vládnout	k5eAaImIp3nS	vládnout
většinová	většinový	k2eAgFnSc1d1	většinová
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
váží	vážit	k5eAaImIp3nS	vážit
i	i	k9	i
jiných	jiný	k2eAgInPc2d1	jiný
názorů	názor	k1gInPc2	názor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
cesta	cesta	k1gFnSc1	cesta
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
přijmout	přijmout	k5eAaPmF	přijmout
srdcem	srdce	k1gNnSc7	srdce
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
vynutit	vynutit	k5eAaPmF	vynutit
zákonem	zákon	k1gInSc7	zákon
a	a	k8xC	a
represí	represe	k1gFnSc7	represe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
iniciátorů	iniciátor	k1gInPc2	iniciátor
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
zveřejnění	zveřejnění	k1gNnSc4	zveřejnění
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
dohody	dohoda	k1gFnSc2	dohoda
proti	proti	k7c3	proti
padělání	padělání	k1gNnSc3	padělání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
oznámila	oznámit	k5eAaPmAgFnS	oznámit
svoji	svůj	k3xOyFgFnSc4	svůj
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
prezidentku	prezidentka	k1gFnSc4	prezidentka
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Přestože	přestože	k8xS	přestože
údajně	údajně	k6eAd1	údajně
dostala	dostat	k5eAaPmAgFnS	dostat
nabídku	nabídka	k1gFnSc4	nabídka
nominace	nominace	k1gFnSc2	nominace
10	[number]	k4	10
senátory	senátor	k1gMnPc7	senátor
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
sbírat	sbírat	k5eAaImF	sbírat
50	[number]	k4	50
000	[number]	k4	000
podpisů	podpis	k1gInPc2	podpis
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
zahájila	zahájit	k5eAaPmAgFnS	zahájit
sběr	sběr	k1gInSc4	sběr
podpisů	podpis	k1gInPc2	podpis
<g/>
,	,	kIx,	,
dosažení	dosažení	k1gNnSc3	dosažení
potřebného	potřebný	k2eAgInSc2d1	potřebný
počtu	počet	k1gInSc2	počet
oznámila	oznámit	k5eAaPmAgFnS	oznámit
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Kandidátní	kandidátní	k2eAgFnSc4d1	kandidátní
listinu	listina	k1gFnSc4	listina
podal	podat	k5eAaPmAgMnS	podat
Jan	Jan	k1gMnSc1	Jan
Sokol	Sokol	k1gMnSc1	Sokol
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
000	[number]	k4	000
podpisů	podpis	k1gInPc2	podpis
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
MV	MV	kA	MV
uznalo	uznat	k5eAaPmAgNnS	uznat
jako	jako	k8xC	jako
platných	platný	k2eAgNnPc2d1	platné
75	[number]	k4	75
0	[number]	k4	0
<g/>
66	[number]	k4	66
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
kandidátku	kandidátka	k1gFnSc4	kandidátka
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
<g/>
,	,	kIx,	,
podpořili	podpořit	k5eAaPmAgMnP	podpořit
ji	on	k3xPp3gFnSc4	on
například	například	k6eAd1	například
Ivan	Ivan	k1gMnSc1	Ivan
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Horáček	Horáček	k1gMnSc1	Horáček
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Grygar	Grygar	k1gMnSc1	Grygar
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Marvanová	Marvanová	k1gFnSc1	Marvanová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průzkumech	průzkum	k1gInPc6	průzkum
mediálních	mediální	k2eAgFnPc2d1	mediální
agentur	agentura	k1gFnPc2	agentura
jí	on	k3xPp3gFnSc3	on
vychází	vycházet	k5eAaImIp3nS	vycházet
1,5	[number]	k4	1,5
až	až	k9	až
5,4	[number]	k4	5,4
%	%	kIx~	%
podpory	podpora	k1gFnSc2	podpora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
přímé	přímý	k2eAgFnSc2d1	přímá
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
probíhalo	probíhat	k5eAaImAgNnS	probíhat
ve	v	k7c6	v
dnech	den	k1gInPc6	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
získala	získat	k5eAaPmAgFnS	získat
255	[number]	k4	255
045	[number]	k4	045
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
4,95	[number]	k4	4,95
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
jako	jako	k9	jako
šestá	šestý	k4xOgFnSc1	šestý
z	z	k7c2	z
devíti	devět	k4xCc2	devět
kandidátů	kandidát	k1gMnPc2	kandidát
a	a	k8xC	a
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
volby	volba	k1gFnSc2	volba
nepostoupila	postoupit	k5eNaPmAgFnS	postoupit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
byla	být	k5eAaImAgFnS	být
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
zvolena	zvolit	k5eAaPmNgFnS	zvolit
místopředsedkyní	místopředsedkyně	k1gFnSc7	místopředsedkyně
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
post	post	k1gInSc4	post
obhájila	obhájit	k5eAaPmAgFnS	obhájit
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
220	[number]	k4	220
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
za	za	k7c7	za
KDU-ČSL	KDU-ČSL	k1gFnSc7	KDU-ČSL
zastupitelkou	zastupitelka	k1gFnSc7	zastupitelka
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
figurovala	figurovat	k5eAaImAgNnP	figurovat
původně	původně	k6eAd1	původně
na	na	k7c6	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlivem	vliv	k1gInSc7	vliv
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
skončila	skončit	k5eAaPmAgFnS	skončit
první	první	k4xOgFnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Čestná	čestný	k2eAgFnSc1d1	čestná
medaile	medaile	k1gFnSc1	medaile
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
univerzity	univerzita	k1gFnSc2	univerzita
Evropanka	Evropanka	k1gFnSc1	Evropanka
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
cena	cena	k1gFnSc1	cena
Evropského	evropský	k2eAgNnSc2d1	Evropské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
)	)	kIx)	)
</s>
