<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Popov	Popov	k1gInSc1	Popov
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
výrobce	výrobce	k1gMnSc1	výrobce
bylinných	bylinný	k2eAgMnPc2d1	bylinný
čajů	čaj	k1gInPc2	čaj
a	a	k8xC	a
mastí	mast	k1gFnPc2	mast
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnPc4d1	přírodní
kosmetiky	kosmetika	k1gFnPc4	kosmetika
<g/>
,	,	kIx,	,
produktů	produkt	k1gInPc2	produkt
s	s	k7c7	s
psylliem	psyllium	k1gNnSc7	psyllium
a	a	k8xC	a
potravních	potravní	k2eAgInPc2d1	potravní
doplňků	doplněk	k1gInPc2	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Staví	stavit	k5eAaBmIp3nS	stavit
na	na	k7c6	na
základech	základ	k1gInPc6	základ
rodinné	rodinný	k2eAgFnSc2d1	rodinná
bylinářské	bylinářský	k2eAgFnSc2d1	bylinářská
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgMnSc1d1	současný
majitel	majitel	k1gMnSc1	majitel
firmy	firma	k1gFnSc2	firma
RNDr.	RNDr.	kA	RNDr.
Pavel	Pavel	k1gMnSc1	Pavel
Popov	Popov	k1gInSc4	Popov
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
třetí	třetí	k4xOgFnSc4	třetí
generací	generace	k1gFnPc2	generace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
bylinami	bylina	k1gFnPc7	bylina
a	a	k8xC	a
přírodní	přírodní	k2eAgFnSc7d1	přírodní
léčbou	léčba	k1gFnSc7	léčba
zabývá	zabývat	k5eAaImIp3nS	zabývat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
zaměření	zaměření	k1gNnSc1	zaměření
==	==	k?	==
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
českým	český	k2eAgMnSc7d1	český
výrobcem	výrobce	k1gMnSc7	výrobce
bylinných	bylinný	k2eAgInPc2d1	bylinný
čajů	čaj	k1gInPc2	čaj
<g/>
,	,	kIx,	,
mastí	mast	k1gFnPc2	mast
a	a	k8xC	a
doplňků	doplněk	k1gInPc2	doplněk
stravy	strava	k1gFnSc2	strava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trhu	trh	k1gInSc6	trh
působí	působit	k5eAaImIp3nS	působit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
dlouholetou	dlouholetý	k2eAgFnSc4d1	dlouholetá
rodinnou	rodinný	k2eAgFnSc4d1	rodinná
bylinářskou	bylinářský	k2eAgFnSc4d1	bylinářská
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Dědeček	dědeček	k1gMnSc1	dědeček
zakladatele	zakladatel	k1gMnSc2	zakladatel
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
MUDr.	MUDr.	kA	MUDr.
Mitrofan	Mitrofan	k1gInSc1	Mitrofan
Popov	Popov	k1gInSc1	Popov
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
Rus	Rus	k1gMnSc1	Rus
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
lékařem	lékař	k1gMnSc7	lékař
ve	v	k7c6	v
francouzských	francouzský	k2eAgFnPc6d1	francouzská
koloniích	kolonie	k1gFnPc6	kolonie
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
část	část	k1gFnSc4	část
života	život	k1gInSc2	život
strávil	strávit	k5eAaPmAgInS	strávit
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
jako	jako	k8xS	jako
venkovský	venkovský	k2eAgMnSc1d1	venkovský
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
používal	používat	k5eAaImAgInS	používat
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
svých	svůj	k3xOyFgMnPc2	svůj
pacientů	pacient	k1gMnPc2	pacient
též	též	k9	též
byliny	bylina	k1gFnPc1	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
práci	práce	k1gFnSc4	práce
navázal	navázat	k5eAaPmAgInS	navázat
syn	syn	k1gMnSc1	syn
Ing.	ing.	kA	ing.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Popov	Popov	k1gInSc1	Popov
<g/>
,	,	kIx,	,
lidový	lidový	k2eAgMnSc1d1	lidový
léčitel	léčitel	k1gMnSc1	léčitel
a	a	k8xC	a
velký	velký	k2eAgMnSc1d1	velký
znalec	znalec	k1gMnSc1	znalec
bylin	bylina	k1gFnPc2	bylina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodinné	rodinný	k2eAgFnSc6d1	rodinná
tradici	tradice	k1gFnSc6	tradice
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
nyní	nyní	k6eAd1	nyní
vnuk	vnuk	k1gMnSc1	vnuk
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Popov	Popov	k1gInSc1	Popov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
založil	založit	k5eAaPmAgMnS	založit
firmu	firma	k1gFnSc4	firma
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
je	být	k5eAaImIp3nS	být
majitelem	majitel	k1gMnSc7	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Přírodovědeckou	přírodovědecký	k2eAgFnSc4d1	Přírodovědecká
fakultu	fakulta	k1gFnSc4	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Výroba	výroba	k1gFnSc1	výroba
produktů	produkt	k1gInPc2	produkt
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
dvou	dva	k4xCgNnPc6	dva
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
kosmetika	kosmetika	k1gFnSc1	kosmetika
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
v	v	k7c6	v
Tachově	Tachov	k1gInSc6	Tachov
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
Plané	Planá	k1gFnSc6	Planá
je	být	k5eAaImIp3nS	být
soustředěna	soustředěn	k2eAgFnSc1d1	soustředěna
výroba	výroba	k1gFnSc1	výroba
čajů	čaj	k1gInPc2	čaj
a	a	k8xC	a
potravních	potravní	k2eAgInPc2d1	potravní
doplňků	doplněk	k1gInPc2	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
distribuuje	distribuovat	k5eAaBmIp3nS	distribuovat
produkty	produkt	k1gInPc4	produkt
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
obchodů	obchod	k1gInPc2	obchod
<g/>
,	,	kIx,	,
lékáren	lékárna	k1gFnPc2	lékárna
a	a	k8xC	a
internetového	internetový	k2eAgInSc2d1	internetový
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
společnost	společnost	k1gFnSc1	společnost
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trh	trh	k1gInSc4	trh
dodává	dodávat	k5eAaImIp3nS	dodávat
vlákninu	vláknina	k1gFnSc4	vláknina
psyllium	psyllium	k1gNnSc1	psyllium
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
přesunula	přesunout	k5eAaPmAgFnS	přesunout
výrobnu	výrobna	k1gFnSc4	výrobna
čajů	čaj	k1gInPc2	čaj
do	do	k7c2	do
nových	nový	k2eAgFnPc2d1	nová
prostor	prostora	k1gFnPc2	prostora
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
postavila	postavit	k5eAaPmAgFnS	postavit
nové	nový	k2eAgFnPc4d1	nová
budovy	budova	k1gFnPc4	budova
s	s	k7c7	s
výrobními	výrobní	k2eAgInPc7d1	výrobní
a	a	k8xC	a
skladovými	skladový	k2eAgInPc7d1	skladový
prostory	prostor	k1gInPc7	prostor
a	a	k8xC	a
administrativní	administrativní	k2eAgFnSc7d1	administrativní
částí	část	k1gFnSc7	část
v	v	k7c6	v
Plané	Planá	k1gFnSc6	Planá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
firma	firma	k1gFnSc1	firma
navázala	navázat	k5eAaPmAgFnS	navázat
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
akciovou	akciový	k2eAgFnSc7d1	akciová
společností	společnost	k1gFnSc7	společnost
Ryor	Ryora	k1gFnPc2	Ryora
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc1	druh
čajů	čaj	k1gInPc2	čaj
(	(	kIx(	(
<g/>
Lymfodren	Lymfodrna	k1gFnPc2	Lymfodrna
<g/>
,	,	kIx,	,
Modelin	Modelina	k1gFnPc2	Modelina
<g/>
,	,	kIx,	,
Stop	stop	k2eAgInSc1d1	stop
Apetit	apetit	k1gInSc1	apetit
<g/>
)	)	kIx)	)
a	a	k8xC	a
nativní	nativní	k2eAgFnSc4d1	nativní
stravu	strava	k1gFnSc4	strava
Pohár	pohár	k1gInSc4	pohár
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zahájila	zahájit	k5eAaPmAgFnS	zahájit
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
výrobně	výrobna	k1gFnSc6	výrobna
kosmetiky	kosmetika	k1gFnSc2	kosmetika
v	v	k7c6	v
Tachově	Tachov	k1gInSc6	Tachov
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
získala	získat	k5eAaPmAgFnS	získat
certifikaci	certifikace	k1gFnSc4	certifikace
HACCP	HACCP	kA	HACCP
a	a	k8xC	a
ISO	ISO	kA	ISO
2007	[number]	k4	2007
a	a	k8xC	a
ISO	ISO	kA	ISO
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Produktové	produktový	k2eAgFnSc2d1	produktová
řady	řada	k1gFnSc2	řada
==	==	k?	==
</s>
</p>
<p>
<s>
Vláknina	vláknina	k1gFnSc1	vláknina
Psyllium	Psyllium	k1gNnSc1	Psyllium
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Popova	popův	k2eAgMnSc4d1	popův
(	(	kIx(	(
<g/>
největší	veliký	k2eAgInSc4d3	veliký
sortiment	sortiment	k1gInSc4	sortiment
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
i	i	k8xC	i
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
vláknina	vláknina	k1gFnSc1	vláknina
<g/>
,	,	kIx,	,
doplněk	doplněk	k1gInSc1	doplněk
stravy	strava	k1gFnSc2	strava
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Psyllicol	Psyllicol	k1gInSc1	Psyllicol
<g/>
®	®	k?	®
(	(	kIx(	(
<g/>
obohacené	obohacený	k2eAgNnSc1d1	obohacené
Psyllium	Psyllium	k1gNnSc1	Psyllium
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Popova	popův	k2eAgFnSc1d1	Popova
<g/>
,	,	kIx,	,
doplněk	doplněk	k1gInSc1	doplněk
stravy	strava	k1gFnSc2	strava
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bylinné	bylinný	k2eAgFnSc2d1	bylinná
masti	mast	k1gFnSc2	mast
–	–	k?	–
vyráběné	vyráběný	k2eAgNnSc1d1	vyráběné
podle	podle	k7c2	podle
tradičních	tradiční	k2eAgFnPc2d1	tradiční
lidových	lidový	k2eAgFnPc2d1	lidová
receptur	receptura	k1gFnPc2	receptura
(	(	kIx(	(
<g/>
Svízelová	svízelový	k2eAgFnSc1d1	svízelová
<g/>
,	,	kIx,	,
Heřmánková	heřmánkový	k2eAgFnSc1d1	heřmánková
<g/>
,	,	kIx,	,
Propolisová	Propolisový	k2eAgFnSc1d1	Propolisová
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bylinné	bylinný	k2eAgInPc1d1	bylinný
čajově	čajově	k6eAd1	čajově
směsi	směs	k1gFnPc1	směs
<g/>
,	,	kIx,	,
sypané	sypaný	k2eAgInPc1d1	sypaný
i	i	k8xC	i
porcované	porcovaný	k2eAgInPc1d1	porcovaný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Otylka	Otylka	k1gFnSc1	Otylka
<g/>
,	,	kIx,	,
Univerzální	univerzální	k2eAgInSc1d1	univerzální
čistící	čistící	k2eAgInSc1d1	čistící
čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
společnosti	společnost	k1gFnSc2	společnost
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Popov	Popov	k1gInSc1	Popov
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
společnosti	společnost	k1gFnSc2	společnost
Ryor	Ryor	k1gMnSc1	Ryor
a.s.	a.s.	k?	a.s.
</s>
</p>
