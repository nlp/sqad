<s>
Pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
dvacetistěn	dvacetistěn	k2eAgInSc1d1	dvacetistěn
(	(	kIx(	(
<g/>
ikosaedr	ikosaedr	k1gInSc1	ikosaedr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
trojrozměrné	trojrozměrný	k2eAgNnSc1d1	trojrozměrné
těleso	těleso	k1gNnSc1	těleso
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
stěny	stěna	k1gFnPc1	stěna
tvoří	tvořit	k5eAaImIp3nP	tvořit
dvacet	dvacet	k4xCc4	dvacet
stejných	stejný	k2eAgInPc2d1	stejný
rovnostranných	rovnostranný	k2eAgInPc2d1	rovnostranný
trojúhelníků	trojúhelník	k1gInPc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
