<s>
Kytice	kytice	k1gFnSc1
z	z	k7c2
pověstí	pověst	k1gFnPc2
národních	národní	k2eAgInPc2d1
(	(	kIx(
<g/>
1853	#num#	k4
<g/>
,	,	kIx,
rozšířené	rozšířený	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
1861	#num#	k4
<g/>
)	)	kIx)
–	–	kIx~
jediná	jediný	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
básní	báseň	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc7
vydal	vydat	k5eAaPmAgMnS
<g/>
;	;	kIx,
podkladem	podklad	k1gInSc7
Kytice	kytice	k1gFnSc2
jsou	být	k5eAaImIp3nP
staré	starý	k2eAgFnPc1d1
české	český	k2eAgFnPc1d1
lidové	lidový	k2eAgFnPc1d1
báje	báj	k1gFnPc1
</s>