<s>
Pestrý	pestrý	k2eAgInSc1d1	pestrý
týden	týden	k1gInSc1	týden
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
ilustrovaným	ilustrovaný	k2eAgInPc3d1	ilustrovaný
časopisům	časopis	k1gInPc3	časopis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vycházely	vycházet	k5eAaImAgFnP	vycházet
v	v	k7c6	v
období	období	k1gNnSc6	období
první	první	k4xOgFnSc2	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
během	během	k7c2	během
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1926	[number]	k4	1926
až	až	k9	až
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
udržoval	udržovat	k5eAaImAgMnS	udržovat
vysoký	vysoký	k2eAgInSc4d1	vysoký
standard	standard	k1gInSc4	standard
psaného	psaný	k2eAgNnSc2d1	psané
i	i	k8xC	i
obrazového	obrazový	k2eAgNnSc2d1	obrazové
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
němu	on	k3xPp3gInSc3	on
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
špičkoví	špičkový	k2eAgMnPc1d1	špičkový
fotoreportéři	fotoreportér	k1gMnPc1	fotoreportér
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
jako	jako	k8xC	jako
Karel	Karel	k1gMnSc1	Karel
Hájek	Hájek	k1gMnSc1	Hájek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Jírů	Jíra	k1gMnPc2	Jíra
nebo	nebo	k8xC	nebo
Ladislav	Ladislav	k1gMnSc1	Ladislav
Sitenský	Sitenský	k2eAgMnSc1d1	Sitenský
<g/>
.	.	kIx.	.
</s>
<s>
Týdeník	týdeník	k1gInSc1	týdeník
Pestrý	pestrý	k2eAgInSc4d1	pestrý
týden	týden	k1gInSc4	týden
vydávaly	vydávat	k5eAaPmAgFnP	vydávat
a	a	k8xC	a
tiskly	tisknout	k5eAaImAgFnP	tisknout
Grafické	grafický	k2eAgInPc4d1	grafický
závody	závod	k1gInPc4	závod
Václav	Václav	k1gMnSc1	Václav
Neubert	Neubert	k1gMnSc1	Neubert
a	a	k8xC	a
synové	syn	k1gMnPc1	syn
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
sídlo	sídlo	k1gNnSc4	sídlo
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
necelých	celý	k2eNgNnPc2d1	necelé
dvaceti	dvacet	k4xCc2	dvacet
let	léto	k1gNnPc2	léto
existence	existence	k1gFnSc2	existence
Pestrého	pestrý	k2eAgInSc2d1	pestrý
týdne	týden	k1gInSc2	týden
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
963	[number]	k4	963
běžných	běžný	k2eAgNnPc2d1	běžné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc4	vznik
časopisu	časopis	k1gInSc2	časopis
Pestrý	pestrý	k2eAgInSc4d1	pestrý
týden	týden	k1gInSc4	týden
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
Karel	Karel	k1gMnSc1	Karel
Neubert	Neubert	k1gMnSc1	Neubert
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
řídil	řídit	k5eAaImAgMnS	řídit
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
zániku	zánik	k1gInSc2	zánik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Redakční	redakční	k2eAgInSc4d1	redakční
kruh	kruh	k1gInSc4	kruh
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
tvořili	tvořit	k5eAaImAgMnP	tvořit
<g/>
:	:	kIx,	:
novinářka	novinářka	k1gFnSc1	novinářka
Milena	Milena	k1gFnSc1	Milena
Jesenská	Jesenská	k1gFnSc1	Jesenská
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
Vratislav	Vratislav	k1gMnSc1	Vratislav
Hugo	Hugo	k1gMnSc1	Hugo
Brunner	Brunner	k1gMnSc1	Brunner
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
Devětsilu	Devětsil	k1gInSc2	Devětsil
Adolf	Adolf	k1gMnSc1	Adolf
Hoffmeister	Hoffmeister	k1gMnSc1	Hoffmeister
a	a	k8xC	a
vydavatel	vydavatel	k1gMnSc1	vydavatel
Karel	Karel	k1gMnSc1	Karel
Neubert	Neubert	k1gMnSc1	Neubert
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
redaktorů	redaktor	k1gMnPc2	redaktor
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
intelektuálního	intelektuální	k2eAgInSc2d1	intelektuální
magazínu	magazín	k1gInSc2	magazín
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
tuto	tento	k3xDgFnSc4	tento
redakci	redakce	k1gFnSc4	redakce
střídá	střídat	k5eAaImIp3nS	střídat
skupina	skupina	k1gFnSc1	skupina
okolo	okolo	k7c2	okolo
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Johna	John	k1gMnSc2	John
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přinesla	přinést	k5eAaPmAgFnS	přinést
přeměnu	přeměna	k1gFnSc4	přeměna
Pestrého	pestrý	k2eAgInSc2d1	pestrý
týdne	týden	k1gInSc2	týden
z	z	k7c2	z
elitní	elitní	k2eAgFnSc2d1	elitní
revue	revue	k1gFnSc2	revue
na	na	k7c4	na
obrazový	obrazový	k2eAgInSc4d1	obrazový
celorodinný	celorodinný	k2eAgInSc4d1	celorodinný
týdeník	týdeník	k1gInSc4	týdeník
pro	pro	k7c4	pro
střední	střední	k2eAgFnPc4d1	střední
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
novému	nový	k2eAgNnSc3d1	nové
zaměření	zaměření	k1gNnSc3	zaměření
se	se	k3xPyFc4	se
Pestrý	pestrý	k2eAgInSc4d1	pestrý
týden	týden	k1gInSc4	týden
v	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
stal	stát	k5eAaPmAgMnS	stát
periodikem	periodikum	k1gNnSc7	periodikum
tištěným	tištěný	k2eAgInPc3d1	tištěný
ve	v	k7c6	v
statisícovém	statisícový	k2eAgInSc6d1	statisícový
nákladu	náklad	k1gInSc6	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
se	se	k3xPyFc4	se
natrvalo	natrvalo	k6eAd1	natrvalo
prosadit	prosadit	k5eAaPmF	prosadit
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
konkurenci	konkurence	k1gFnSc6	konkurence
prvorepublikových	prvorepublikový	k2eAgNnPc2d1	prvorepublikové
periodik	periodikum	k1gNnPc2	periodikum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Pražský	pražský	k2eAgInSc1d1	pražský
ilustrovaný	ilustrovaný	k2eAgInSc1d1	ilustrovaný
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
<g/>
,	,	kIx,	,
Letem	letem	k6eAd1	letem
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
Světozor	světozor	k1gInSc1	světozor
<g/>
,	,	kIx,	,
Ahoj	ahoj	k0	ahoj
<g/>
,	,	kIx,	,
Star	star	k1gFnPc1	star
<g/>
,	,	kIx,	,
Světový	světový	k2eAgInSc1d1	světový
zdroj	zdroj	k1gInSc1	zdroj
zábavy	zábava	k1gFnSc2	zábava
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
<g/>
,	,	kIx,	,
Hvězda	hvězda	k1gFnSc1	hvězda
československých	československý	k2eAgFnPc2d1	Československá
paní	paní	k1gFnPc2	paní
a	a	k8xC	a
dívek	dívka	k1gFnPc2	dívka
či	či	k8xC	či
List	list	k1gInSc1	list
paní	paní	k1gFnSc2	paní
a	a	k8xC	a
dívek	dívka	k1gFnPc2	dívka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholným	vrcholný	k2eAgNnSc7d1	vrcholné
obdobím	období	k1gNnSc7	období
Pestrého	pestrý	k2eAgInSc2d1	pestrý
týdne	týden	k1gInSc2	týden
byly	být	k5eAaImAgInP	být
ročníky	ročník	k1gInPc1	ročník
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
Pestrý	pestrý	k2eAgInSc4d1	pestrý
týden	týden	k1gInSc4	týden
kvalitativně	kvalitativně	k6eAd1	kvalitativně
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
světových	světový	k2eAgInPc2d1	světový
obrazových	obrazový	k2eAgInPc2d1	obrazový
týdeníků	týdeník	k1gInPc2	týdeník
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vypjaté	vypjatý	k2eAgFnSc6d1	vypjatá
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dějiny	dějiny	k1gFnPc1	dějiny
převracely	převracet	k5eAaImAgFnP	převracet
svět	svět	k1gInSc4	svět
naruby	naruby	k6eAd1	naruby
<g/>
,	,	kIx,	,
otiskoval	otiskovat	k5eAaImAgInS	otiskovat
tento	tento	k3xDgInSc1	tento
časopis	časopis	k1gInSc1	časopis
snímky	snímka	k1gFnSc2	snímka
naprosté	naprostý	k2eAgFnSc2d1	naprostá
špičky	špička	k1gFnSc2	špička
reportážních	reportážní	k2eAgMnPc2d1	reportážní
fotografů	fotograf	k1gMnPc2	fotograf
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
míře	míra	k1gFnSc6	míra
v	v	k7c6	v
konkurenčních	konkurenční	k2eAgInPc6d1	konkurenční
listech	list	k1gInPc6	list
nevídané	vídaný	k2eNgFnSc2d1	nevídaná
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
se	se	k3xPyFc4	se
Pestrý	pestrý	k2eAgInSc1d1	pestrý
týden	týden	k1gInSc1	týden
stává	stávat	k5eAaImIp3nS	stávat
oficiálním	oficiální	k2eAgInSc7d1	oficiální
listem	list	k1gInSc7	list
Radosti	radost	k1gFnSc2	radost
ze	z	k7c2	z
života	život	k1gInSc2	život
–	–	k?	–
odnože	odnož	k1gFnSc2	odnož
jediného	jediné	k1gNnSc2	jediné
společensko-politického	společenskoolitický	k2eAgNnSc2d1	společensko-politické
hnutí	hnutí	k1gNnSc2	hnutí
v	v	k7c6	v
protektorátu	protektorát	k1gInSc6	protektorát
–	–	k?	–
Národního	národní	k2eAgNnSc2d1	národní
souručenství	souručenství	k1gNnSc2	souručenství
(	(	kIx(	(
<g/>
organizaci	organizace	k1gFnSc4	organizace
Radost	radost	k1gFnSc1	radost
ze	z	k7c2	z
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protektorátní	protektorátní	k2eAgNnSc1d1	protektorátní
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
časem	časem	k6eAd1	časem
pozvolného	pozvolný	k2eAgInSc2d1	pozvolný
kvalitativního	kvalitativní	k2eAgInSc2d1	kvalitativní
úpadku	úpadek	k1gInSc2	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
Pestrý	pestrý	k2eAgInSc1d1	pestrý
týden	týden	k1gInSc1	týden
rozchází	rozcházet	k5eAaImIp3nS	rozcházet
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
list	list	k1gInSc4	list
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
výjimku	výjimka	k1gFnSc4	výjimka
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
dodávaných	dodávaný	k2eAgMnPc2d1	dodávaný
–	–	k?	–
ideologicky	ideologicky	k6eAd1	ideologicky
překroucených	překroucený	k2eAgFnPc2d1	překroucená
–	–	k?	–
aktualit	aktualita	k1gFnPc2	aktualita
píše	psát	k5eAaImIp3nS	psát
o	o	k7c6	o
tématech	téma	k1gNnPc6	téma
časově	časově	k6eAd1	časově
neurčitých	určitý	k2eNgNnPc6d1	neurčité
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
díky	díky	k7c3	díky
svému	svůj	k3xOyFgMnSc3	svůj
vydavateli	vydavatel	k1gMnSc3	vydavatel
a	a	k8xC	a
šéfredaktorovi	šéfredaktor	k1gMnSc3	šéfredaktor
Karlu	Karel	k1gMnSc3	Karel
Neubertovi	Neubert	k1gMnSc3	Neubert
nepropagoval	propagovat	k5eNaImAgMnS	propagovat
nacistickou	nacistický	k2eAgFnSc4d1	nacistická
ideologii	ideologie	k1gFnSc4	ideologie
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
úřady	úřad	k1gInPc4	úřad
nucen	nucen	k2eAgInSc1d1	nucen
<g/>
.	.	kIx.	.
</s>
<s>
Pestrý	pestrý	k2eAgInSc1d1	pestrý
týden	týden	k1gInSc1	týden
přestal	přestat	k5eAaPmAgInS	přestat
vycházet	vycházet	k5eAaImF	vycházet
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
pražského	pražský	k2eAgNnSc2d1	Pražské
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
necelého	celý	k2eNgInSc2d1	necelý
měsíce	měsíc	k1gInSc2	měsíc
zformovali	zformovat	k5eAaPmAgMnP	zformovat
bývalí	bývalý	k2eAgMnPc1d1	bývalý
redaktoři	redaktor	k1gMnPc1	redaktor
Pestrého	pestrý	k2eAgInSc2d1	pestrý
týdne	týden	k1gInSc2	týden
nový	nový	k2eAgInSc4d1	nový
zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
týdeník	týdeník	k1gInSc4	týdeník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nazvali	nazvat	k5eAaPmAgMnP	nazvat
Svět	svět	k1gInSc4	svět
v	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Vilgus	Vilgus	k1gMnSc1	Vilgus
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
:	:	kIx,	:
České	český	k2eAgInPc1d1	český
ilustrované	ilustrovaný	k2eAgInPc1d1	ilustrovaný
časopisy	časopis	k1gInPc1	časopis
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
na	na	k7c6	na
příkladu	příklad	k1gInSc6	příklad
Pražského	pražský	k2eAgInSc2d1	pražský
ilustrovaného	ilustrovaný	k2eAgInSc2d1	ilustrovaný
zpravodaje	zpravodaj	k1gInSc2	zpravodaj
a	a	k8xC	a
Pestrého	pestrý	k2eAgInSc2d1	pestrý
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Vilgus	Vilgus	k1gMnSc1	Vilgus
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
:	:	kIx,	:
Pestrý	pestrý	k2eAgInSc1d1	pestrý
týden	týden	k1gInSc1	týden
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1926	[number]	k4	1926
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
:	:	kIx,	:
Vznik	vznik	k1gInSc1	vznik
<g/>
,	,	kIx,	,
existence	existence	k1gFnSc1	existence
a	a	k8xC	a
zánik	zánik	k1gInSc1	zánik
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
ilustrovaného	ilustrovaný	k2eAgInSc2d1	ilustrovaný
týdeníku	týdeník	k1gInSc2	týdeník
první	první	k4xOgFnSc2	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
období	období	k1gNnSc2	období
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
Praha-Opava	Praha-Opava	k1gFnSc1	Praha-Opava
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
238	[number]	k4	238
<g/>
-	-	kIx~	-
<g/>
7874	[number]	k4	7874
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pestrý	pestrý	k2eAgInSc4d1	pestrý
týden	týden	k1gInSc4	týden
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Pestrý	pestrý	k2eAgInSc4d1	pestrý
týden	týden	k1gInSc4	týden
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
</s>
