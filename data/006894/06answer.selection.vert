<s>
Pestrý	pestrý	k2eAgInSc1d1	pestrý
týden	týden	k1gInSc1	týden
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
ilustrovaným	ilustrovaný	k2eAgInPc3d1	ilustrovaný
časopisům	časopis	k1gInPc3	časopis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vycházely	vycházet	k5eAaImAgFnP	vycházet
v	v	k7c6	v
období	období	k1gNnSc6	období
první	první	k4xOgFnSc2	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
během	během	k7c2	během
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
