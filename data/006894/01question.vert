<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgInS	jmenovat
ilustrovaný	ilustrovaný	k2eAgInSc1d1	ilustrovaný
časopis	časopis	k1gInSc1	časopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vycházel	vycházet	k5eAaImAgInS	vycházet
v	v	k7c6	v
období	období	k1gNnSc6	období
první	první	k4xOgFnSc2	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
během	během	k7c2	během
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
?	?	kIx.	?
</s>
