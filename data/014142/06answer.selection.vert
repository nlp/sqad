<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgInSc1d3
mariánský	mariánský	k2eAgInSc1d1
kostel	kostel	k1gInSc1
na	na	k7c6
území	území	k1gNnSc6
města	město	k1gNnSc2
Ostravy	Ostrava	k1gFnSc2
a	a	k8xC
druhý	druhý	k4xOgInSc1
ostravský	ostravský	k2eAgInSc1d1
kostel	kostel	k1gInSc1
zasvěcený	zasvěcený	k2eAgInSc1d1
Nanebevzetí	nanebevzetí	k1gNnSc3
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
tím	ten	k3xDgNnSc7
dalším	další	k1gNnSc7
je	být	k5eAaImIp3nS
michálkovický	michálkovický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>