<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Třebovice	Třebovice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Farní	farní	k2eAgNnSc1d1
kostelNanebevzetí	kostelNanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
MarieMísto	MarieMísta	k1gMnSc5
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Obec	obec	k1gFnSc1
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
Třebovice	Třebovice	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
2,8	2,8	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
34,8	34,8	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Církev	církev	k1gFnSc1
</s>
<s>
římskokatolická	římskokatolický	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
moravská	moravský	k2eAgFnSc1d1
Diecéze	diecéze	k1gFnSc1
</s>
<s>
ostravsko-opavská	ostravsko-opavský	k2eAgFnSc1d1
Děkanát	děkanát	k1gInSc1
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
Farnost	farnost	k1gFnSc1
</s>
<s>
Ostrava-Třebovice	Ostrava-Třebovice	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
farní	farní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Architektonický	architektonický	k2eAgInSc1d1
popis	popis	k1gInSc4
Stavební	stavební	k2eAgInSc4d1
sloh	sloh	k1gInSc4
</s>
<s>
baroko	baroko	k1gNnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1738	#num#	k4
Specifikace	specifikace	k1gFnSc1
Stavební	stavební	k2eAgFnSc1d1
materiál	materiál	k1gInSc4
</s>
<s>
zděný	zděný	k2eAgInSc1d1
Další	další	k2eAgInSc1d1
informace	informace	k1gFnPc4
Adresa	adresa	k1gFnSc1
</s>
<s>
V	v	k7c6
Mešníku	Mešník	k1gInSc6
<g/>
722	#num#	k4
00	#num#	k4
Ostrava-Třebovice	Ostrava-Třebovice	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
Trebovická	Trebovický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Kód	kód	k1gInSc4
památky	památka	k1gFnSc2
</s>
<s>
104263	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgInSc1d3
mariánský	mariánský	k2eAgInSc1d1
kostel	kostel	k1gInSc1
na	na	k7c6
území	území	k1gNnSc6
města	město	k1gNnSc2
Ostravy	Ostrava	k1gFnSc2
a	a	k8xC
druhý	druhý	k4xOgInSc1
ostravský	ostravský	k2eAgInSc1d1
kostel	kostel	k1gInSc1
zasvěcený	zasvěcený	k2eAgInSc1d1
Nanebevzetí	nanebevzetí	k1gNnSc3
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
tím	ten	k3xDgNnSc7
dalším	další	k1gNnSc7
je	být	k5eAaImIp3nS
michálkovický	michálkovický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2011	#num#	k4
je	být	k5eAaImIp3nS
památkově	památkově	k6eAd1
chráněn	chránit	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
městském	městský	k2eAgInSc6d1
obvodu	obvod	k1gInSc6
Třebovice	Třebovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
dřevěný	dřevěný	k2eAgInSc1d1
kostelík	kostelík	k1gInSc1
je	být	k5eAaImIp3nS
písemně	písemně	k6eAd1
doložen	doložen	k2eAgInSc1d1
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1553	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
místě	místo	k1gNnSc6
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1738	#num#	k4
postaven	postaven	k2eAgInSc4d1
zděný	zděný	k2eAgInSc4d1
barokní	barokní	k2eAgInSc4d1
kostel	kostel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
byl	být	k5eAaImAgInS
kostel	kostel	k1gInSc1
vyzdoben	vyzdoben	k2eAgInSc1d1
malbami	malba	k1gFnPc7
dle	dle	k7c2
návrhu	návrh	k1gInSc2
vídeňského	vídeňský	k2eAgMnSc2d1
malíře	malíř	k1gMnSc2
Josefa	Josef	k1gMnSc2
Kleinerta	Kleinert	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ke	k	k7c3
zdi	zeď	k1gFnSc3
kostela	kostel	k1gInSc2
přiléhá	přiléhat	k5eAaImIp3nS
hrobka	hrobka	k1gFnSc1
rodiny	rodina	k1gFnSc2
Stonawských	Stonawský	k1gMnPc2
(	(	kIx(
<g/>
evangelíků	evangelík	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
Scholzových	Scholzových	k2eAgFnSc1d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
pohřbena	pohřben	k2eAgFnSc1d1
i	i	k8xC
spisovatelka	spisovatelka	k1gFnSc1
Maria	Maria	k1gFnSc1
Stona	Stona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
JUŘICA	Juřica	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostravské	ostravský	k2eAgInPc1d1
svatostánky	svatostánek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
Repronis	Repronis	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7329	#num#	k4
<g/>
-	-	kIx~
<g/>
116	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
10	#num#	k4
<g/>
–	–	k?
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
osa	osa	k1gFnSc1
<g/>
2011629686	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
169628255	#num#	k4
</s>
