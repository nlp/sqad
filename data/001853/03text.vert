<s>
David	David	k1gMnSc1	David
Hume	Hum	k1gFnSc2	Hum
[	[	kIx(	[
<g/>
deɪ	deɪ	k?	deɪ
<g/>
.	.	kIx.	.
<g/>
vɪ	vɪ	k?	vɪ
hjʊ	hjʊ	k?	hjʊ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1711	[number]	k4	1711
<g/>
,	,	kIx,	,
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1776	[number]	k4	1776
<g/>
,	,	kIx,	,
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
skotský	skotský	k2eAgMnSc1d1	skotský
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
anglicky	anglicky	k6eAd1	anglicky
píšících	píšící	k2eAgMnPc2d1	píšící
filosofů	filosof	k1gMnPc2	filosof
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Domyšlením	domyšlení	k1gNnSc7	domyšlení
některých	některý	k3yIgInPc2	některý
důsledků	důsledek	k1gInPc2	důsledek
teorie	teorie	k1gFnSc2	teorie
poznání	poznání	k1gNnSc2	poznání
známé	známý	k2eAgNnSc4d1	známé
jako	jako	k9	jako
empirismus	empirismus	k1gInSc4	empirismus
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
jeho	jeho	k3xOp3gInPc4	jeho
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
prý	prý	k9	prý
vytrhlo	vytrhnout	k5eAaPmAgNnS	vytrhnout
Immanuela	Immanuel	k1gMnSc4	Immanuel
Kanta	Kant	k1gMnSc4	Kant
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
dogmatického	dogmatický	k2eAgInSc2d1	dogmatický
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Znám	znám	k2eAgMnSc1d1	znám
je	být	k5eAaImIp3nS	být
svojí	svůj	k3xOyFgFnSc7	svůj
kritikou	kritika	k1gFnSc7	kritika
kategorie	kategorie	k1gFnSc2	kategorie
kauzality	kauzalita	k1gFnSc2	kauzalita
jako	jako	k9	jako
něčeho	něco	k3yInSc2	něco
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
fenomenologické	fenomenologický	k2eAgFnSc2d1	fenomenologická
analýzy	analýza	k1gFnSc2	analýza
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pojem	pojem	k1gInSc1	pojem
kauzality	kauzalita	k1gFnSc2	kauzalita
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
např.	např.	kA	např.
pojem	pojem	k1gInSc1	pojem
substance	substance	k1gFnSc1	substance
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
člověku	člověk	k1gMnSc6	člověk
opakováním	opakování	k1gNnSc7	opakování
sledu	sled	k1gInSc2	sled
či	či	k8xC	či
souvýskytu	souvýskyt	k1gInSc2	souvýskyt
určitých	určitý	k2eAgFnPc2d1	určitá
stabilně	stabilně	k6eAd1	stabilně
se	se	k3xPyFc4	se
vyskytujících	vyskytující	k2eAgInPc2d1	vyskytující
vjemů	vjem	k1gInPc2	vjem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
oblastí	oblast	k1gFnSc7	oblast
kritiky	kritika	k1gFnSc2	kritika
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
pojetí	pojetí	k1gNnSc2	pojetí
poznání	poznání	k1gNnSc2	poznání
jako	jako	k8xC	jako
indukce	indukce	k1gFnSc2	indukce
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
opět	opět	k6eAd1	opět
impuls	impuls	k1gInSc4	impuls
pro	pro	k7c4	pro
Kantův	Kantův	k2eAgInSc4d1	Kantův
"	"	kIx"	"
<g/>
koperníkovský	koperníkovský	k2eAgInSc4d1	koperníkovský
<g/>
"	"	kIx"	"
obrat	obrat	k1gInSc4	obrat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
ateismu	ateismus	k1gInSc2	ateismus
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
deklarující	deklarující	k2eAgFnPc1d1	deklarující
se	se	k3xPyFc4	se
jako	jako	k9	jako
tvrdošíjný	tvrdošíjný	k2eAgMnSc1d1	tvrdošíjný
ateista	ateista	k1gMnSc1	ateista
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
význam	význam	k1gInSc1	význam
svým	svůj	k3xOyFgInSc7	svůj
principem	princip	k1gInSc7	princip
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
zázrakům	zázrak	k1gInPc3	zázrak
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
svědectvím	svědectví	k1gNnSc7	svědectví
o	o	k7c4	o
porušení	porušení	k1gNnSc4	porušení
přírodních	přírodní	k2eAgInPc2d1	přírodní
zákonů	zákon	k1gInPc2	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Humeovou	Humeův	k2eAgFnSc7d1	Humeova
břitvou	břitva	k1gFnSc7	břitva
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
právě	právě	k9	právě
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
zázrak	zázrak	k1gInSc1	zázrak
<g/>
"	"	kIx"	"
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k9	jako
porušení	porušení	k1gNnSc1	porušení
přírodního	přírodní	k2eAgInSc2d1	přírodní
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
fungování	fungování	k1gNnSc1	fungování
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
vší	všecek	k3xTgFnSc7	všecek
lidskou	lidský	k2eAgFnSc7d1	lidská
zkušeností	zkušenost	k1gFnSc7	zkušenost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zázrak	zázrak	k1gInSc1	zázrak
něčím	něco	k3yInSc7	něco
nemožným	možný	k2eNgNnSc7d1	nemožné
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
právě	právě	k6eAd1	právě
tím	ten	k3xDgNnSc7	ten
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odmítá	odmítat	k5eAaImIp3nS	odmítat
i	i	k9	i
deismus	deismus	k1gInSc1	deismus
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
nelze	lze	k6eNd1	lze
na	na	k7c4	na
nějakého	nějaký	k3yIgMnSc4	nějaký
Boha-Tvůrce	Boha-Tvůrce	k1gMnSc4	Boha-Tvůrce
usuzovat	usuzovat	k5eAaImF	usuzovat
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
z	z	k7c2	z
"	"	kIx"	"
<g/>
výtvoru	výtvor	k1gInSc2	výtvor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
do	do	k7c2	do
právnické	právnický	k2eAgFnSc2d1	právnická
rodiny	rodina	k1gFnSc2	rodina
v	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
jako	jako	k8xS	jako
David	David	k1gMnSc1	David
Home	Hom	k1gFnSc2	Hom
(	(	kIx(	(
<g/>
příjmení	příjmení	k1gNnSc4	příjmení
si	se	k3xPyFc3	se
změnil	změnit	k5eAaPmAgInS	změnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1734	[number]	k4	1734
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Angličané	Angličan	k1gMnPc1	Angličan
měli	mít	k5eAaImAgMnP	mít
problém	problém	k1gInSc4	problém
skotské	skotská	k1gFnSc2	skotská
"	"	kIx"	"
<g/>
Home	Home	k1gInSc1	Home
<g/>
"	"	kIx"	"
vyslovovat	vyslovovat	k5eAaImF	vyslovovat
správně	správně	k6eAd1	správně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nadané	nadaný	k2eAgNnSc4d1	nadané
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
Edinburskou	Edinburský	k2eAgFnSc4d1	Edinburská
univerzitu	univerzita	k1gFnSc4	univerzita
již	již	k6eAd1	již
ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
normální	normální	k2eAgFnSc4d1	normální
tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
čtrnáct	čtrnáct	k4xCc4	čtrnáct
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
napsal	napsat	k5eAaBmAgMnS	napsat
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
víceletého	víceletý	k2eAgInSc2d1	víceletý
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
své	svůj	k3xOyFgNnSc4	svůj
základní	základní	k2eAgNnSc4d1	základní
dílo	dílo	k1gNnSc4	dílo
Pojednání	pojednání	k1gNnSc2	pojednání
o	o	k7c6	o
lidské	lidský	k2eAgFnSc6d1	lidská
přirozenosti	přirozenost	k1gFnSc6	přirozenost
(	(	kIx(	(
<g/>
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
tiskem	tisk	k1gInSc7	tisk
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
roku	rok	k1gInSc2	rok
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
se	se	k3xPyFc4	se
ucházel	ucházet	k5eAaImAgInS	ucházet
o	o	k7c4	o
akademickou	akademický	k2eAgFnSc4d1	akademická
katedru	katedra	k1gFnSc4	katedra
<g/>
,	,	kIx,	,
leč	leč	k8xC	leč
marně	marně	k6eAd1	marně
<g/>
.	.	kIx.	.
</s>
<s>
Přijal	přijmout	k5eAaPmAgMnS	přijmout
tedy	tedy	k9	tedy
místo	místo	k7c2	místo
knihovníka	knihovník	k1gMnSc2	knihovník
v	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
činnost	činnost	k1gFnSc1	činnost
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
podnětem	podnět	k1gInSc7	podnět
k	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
díla	dílo	k1gNnSc2	dílo
Dějiny	dějiny	k1gFnPc1	dějiny
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jej	on	k3xPp3gMnSc4	on
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
a	a	k8xC	a
přineslo	přinést	k5eAaPmAgNnS	přinést
mu	on	k3xPp3gMnSc3	on
i	i	k8xC	i
hmotný	hmotný	k2eAgInSc1d1	hmotný
blahobyt	blahobyt	k1gInSc1	blahobyt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
další	další	k2eAgInSc1d1	další
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
bohatý	bohatý	k2eAgInSc1d1	bohatý
na	na	k7c4	na
úspěchy	úspěch	k1gInPc4	úspěch
a	a	k8xC	a
vnější	vnější	k2eAgFnPc4d1	vnější
pocty	pocta	k1gFnPc4	pocta
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
dělal	dělat	k5eAaImAgMnS	dělat
tajemníka	tajemník	k1gMnSc2	tajemník
lordu	lord	k1gMnSc3	lord
Hertfordovi	Hertforda	k1gMnSc3	Hertforda
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
J.	J.	kA	J.
J.	J.	kA	J.
Rousseauem	Rousseau	k1gMnSc7	Rousseau
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xS	jako
vysoký	vysoký	k2eAgMnSc1d1	vysoký
úředník	úředník	k1gMnSc1	úředník
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Závěr	závěr	k1gInSc1	závěr
života	život	k1gInSc2	život
prožil	prožít	k5eAaPmAgInS	prožít
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
svých	svůj	k3xOyFgMnPc2	svůj
oddaných	oddaný	k2eAgMnPc2d1	oddaný
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
neoženil	oženit	k5eNaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1776	[number]	k4	1776
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
zařadila	zařadit	k5eAaPmAgFnS	zařadit
veškeré	veškerý	k3xTgInPc4	veškerý
jeho	jeho	k3xOp3gInPc4	jeho
spisy	spis	k1gInPc4	spis
(	(	kIx(	(
<g/>
opera	opera	k1gFnSc1	opera
omnia	omnium	k1gNnSc2	omnium
<g/>
)	)	kIx)	)
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Anglie	Anglie	k1gFnSc2	Anglie
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
History	Histor	k1gInPc4	Histor
Of	Of	k1gFnSc2	Of
England	Englanda	k1gFnPc2	Englanda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1754	[number]	k4	1754
<g/>
-	-	kIx~	-
<g/>
61	[number]	k4	61
(	(	kIx(	(
<g/>
6	[number]	k4	6
dílů	díl	k1gInPc2	díl
<g/>
)	)	kIx)	)
Dopis	dopis	k1gInSc1	dopis
džentlmena	džentlmen	k1gMnSc2	džentlmen
příteli	přítel	k1gMnSc3	přítel
v	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
(	(	kIx(	(
<g/>
A	a	k8xC	a
Letter	Letter	k1gMnSc1	Letter
from	from	k1gMnSc1	from
a	a	k8xC	a
Gentleman	gentleman	k1gMnSc1	gentleman
to	ten	k3xDgNnSc4	ten
his	his	k1gNnSc4	his
Friend	Friend	k1gMnSc1	Friend
at	at	k?	at
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1745	[number]	k4	1745
Four	Four	k1gInSc1	Four
Dissertations	Dissertations	k1gInSc4	Dissertations
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Přirozené	přirozený	k2eAgFnPc4d1	přirozená
dějiny	dějiny	k1gFnPc4	dějiny
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
O	o	k7c6	o
normě	norma	k1gFnSc6	norma
vkusu	vkus	k1gInSc2	vkus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1757	[number]	k4	1757
Morální	morální	k2eAgInPc1d1	morální
a	a	k8xC	a
politické	politický	k2eAgInPc1d1	politický
eseje	esej	k1gInPc1	esej
(	(	kIx(	(
<g/>
Essays	Essays	k1gInSc1	Essays
<g/>
,	,	kIx,	,
Moral	Moral	k1gMnSc1	Moral
And	Anda	k1gFnPc2	Anda
Political	Political	k1gMnSc1	Political
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1741	[number]	k4	1741
<g/>
-	-	kIx~	-
<g/>
42	[number]	k4	42
Pojednání	pojednání	k1gNnPc2	pojednání
o	o	k7c6	o
lidské	lidský	k2eAgFnSc6d1	lidská
přirozenosti	přirozenost	k1gFnSc6	přirozenost
(	(	kIx(	(
<g/>
A	a	k9	a
Treatise	Treatise	k1gFnSc1	Treatise
Of	Of	k1gFnSc2	Of
<g />
.	.	kIx.	.
</s>
<s>
Human	Human	k1gMnSc1	Human
Nature	Natur	k1gMnSc5	Natur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1739	[number]	k4	1739
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
Political	Political	k1gMnSc1	Political
Discourses	Discourses	k1gMnSc1	Discourses
<g/>
,	,	kIx,	,
1752	[number]	k4	1752
esej	esej	k1gFnSc1	esej
O	o	k7c6	o
penězích	peníze	k1gInPc6	peníze
(	(	kIx(	(
<g/>
On	on	k3xPp3gMnSc1	on
money	moneum	k1gNnPc7	moneum
<g/>
)	)	kIx)	)
-	-	kIx~	-
zde	zde	k6eAd1	zde
dal	dát	k5eAaPmAgInS	dát
Hume	Hume	k1gInSc1	Hume
pevný	pevný	k2eAgInSc4d1	pevný
základ	základ	k1gInSc4	základ
kvantitativní	kvantitativní	k2eAgFnSc4d1	kvantitativní
teorii	teorie	k1gFnSc4	teorie
peněz	peníze	k1gInPc2	peníze
Rozmluvy	rozmluva	k1gFnSc2	rozmluva
o	o	k7c6	o
přirozeném	přirozený	k2eAgNnSc6d1	přirozené
náboženství	náboženství	k1gNnSc1	náboženství
(	(	kIx(	(
<g/>
Dialogues	Dialogues	k1gInSc1	Dialogues
Concerning	Concerning	k1gInSc1	Concerning
Natural	Natural	k?	Natural
Religion	religion	k1gInSc1	religion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1779	[number]	k4	1779
Zkoumání	zkoumání	k1gNnSc2	zkoumání
lidského	lidský	k2eAgInSc2d1	lidský
rozumu	rozum	k1gInSc2	rozum
(	(	kIx(	(
<g/>
An	An	k1gFnSc1	An
Enquiry	Enquira	k1gFnSc2	Enquira
Concerning	Concerning	k1gInSc1	Concerning
Human	Human	k1gMnSc1	Human
Understanding	Understanding	k1gInSc1	Understanding
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1748	[number]	k4	1748
Zkoumání	zkoumání	k1gNnSc2	zkoumání
morálních	morální	k2eAgInPc2d1	morální
principů	princip	k1gInPc2	princip
(	(	kIx(	(
<g/>
An	An	k1gMnSc1	An
Enquiry	Enquira	k1gFnSc2	Enquira
Concerning	Concerning	k1gInSc1	Concerning
The	The	k1gMnSc1	The
Principles	Principles	k1gMnSc1	Principles
Of	Of	k1gMnSc1	Of
Morals	Moralsa	k1gFnPc2	Moralsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1751	[number]	k4	1751
HUME	HUME	kA	HUME
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Dialogy	dialog	k1gInPc1	dialog
o	o	k7c6	o
přirozeném	přirozený	k2eAgNnSc6d1	přirozené
náboženství	náboženství	k1gNnSc6	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Tomáš	Tomáš	k1gMnSc1	Tomáš
Marvan	Marvan	k1gMnSc1	Marvan
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Dybbuk	Dybbuk	k1gMnSc1	Dybbuk
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
206	[number]	k4	206
s.	s.	k?	s.
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7438	[number]	k4	7438
<g/>
-	-	kIx~	-
<g/>
101	[number]	k4	101
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
HUME	HUME	kA	HUME
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
o	o	k7c6	o
lidském	lidský	k2eAgInSc6d1	lidský
rozumu	rozum	k1gInSc6	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Moural	Moural	k1gMnSc1	Moural
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
překl	překl	k1gInSc1	překl
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
235	[number]	k4	235
s.	s.	k?	s.
Filozofické	filozofický	k2eAgNnSc4d1	filozofické
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
205	[number]	k4	205
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
HUME	HUME	kA	HUME
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
lidského	lidský	k2eAgInSc2d1	lidský
rozumu	rozum	k1gInSc2	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Gaja	Gaja	k1gMnSc1	Gaja
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
223	[number]	k4	223
s.	s.	k?	s.
HUME	HUME	kA	HUME
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Přirozené	přirozený	k2eAgFnPc1d1	přirozená
dějiny	dějiny	k1gFnPc1	dějiny
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
;	;	kIx,	;
a	a	k8xC	a
Rozmluvy	rozmluva	k1gFnPc1	rozmluva
o	o	k7c6	o
náboženství	náboženství	k1gNnSc6	náboženství
přirozeném	přirozený	k2eAgNnSc6d1	přirozené
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
Škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Nákladem	náklad	k1gInSc7	náklad
Jana	Jan	k1gMnSc2	Jan
Laichtera	Laichter	k1gMnSc2	Laichter
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
207	[number]	k4	207
s.	s.	k?	s.
Laichterův	Laichterův	k2eAgInSc4d1	Laichterův
výbor	výbor	k1gInSc4	výbor
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
spisů	spis	k1gInPc2	spis
poučných	poučný	k2eAgInPc2d1	poučný
<g/>
;	;	kIx,	;
kn	kn	k?	kn
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
HUME	HUME	kA	HUME
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
o	o	k7c6	o
zásadách	zásada	k1gFnPc6	zásada
mravnosti	mravnost	k1gFnSc2	mravnost
a	a	k8xC	a
zkoumání	zkoumání	k1gNnSc2	zkoumání
o	o	k7c6	o
rozumu	rozum	k1gInSc6	rozum
lidském	lidský	k2eAgInSc6d1	lidský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
368	[number]	k4	368
s.	s.	k?	s.
Laichterův	Laichterův	k2eAgInSc4d1	Laichterův
výbor	výbor	k1gInSc4	výbor
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
spisů	spis	k1gInPc2	spis
poučných	poučný	k2eAgInPc2d1	poučný
<g/>
;	;	kIx,	;
kn	kn	k?	kn
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
kromě	kromě	k7c2	kromě
ryze	ryze	k6eAd1	ryze
filosofických	filosofický	k2eAgFnPc2d1	filosofická
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
také	také	k6eAd1	také
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
témat	téma	k1gNnPc2	téma
týkajících	týkající	k2eAgNnPc2d1	týkající
se	se	k3xPyFc4	se
dnešní	dnešní	k2eAgFnSc2d1	dnešní
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
právě	právě	k6eAd1	právě
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
souboru	soubor	k1gInSc6	soubor
esejí	esej	k1gFnPc2	esej
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Political	Political	k1gMnSc1	Political
discourses	discourses	k1gInSc4	discourses
rozpracoval	rozpracovat	k5eAaPmAgMnS	rozpracovat
jak	jak	k8xS	jak
počátky	počátek	k1gInPc4	počátek
kvantitativní	kvantitativní	k2eAgFnSc2d1	kvantitativní
teorie	teorie	k1gFnSc2	teorie
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
obhájil	obhájit	k5eAaPmAgMnS	obhájit
jejich	jejich	k3xOp3gFnSc4	jejich
neutralitu	neutralita	k1gFnSc4	neutralita
a	a	k8xC	a
objasnil	objasnit	k5eAaPmAgMnS	objasnit
vztah	vztah	k1gInSc4	vztah
množství	množství	k1gNnSc2	množství
peněz	peníze	k1gInPc2	peníze
k	k	k7c3	k
úrokové	úrokový	k2eAgFnSc3d1	úroková
míře	míra	k1gFnSc3	míra
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
náznaky	náznak	k1gInPc1	náznak
kvantitativní	kvantitativní	k2eAgFnSc2d1	kvantitativní
teorie	teorie	k1gFnSc2	teorie
peněz	peníze	k1gInPc2	peníze
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaBmF	nalézt
už	už	k9	už
u	u	k7c2	u
Cantillona	Cantillon	k1gMnSc2	Cantillon
a	a	k8xC	a
Johna	John	k1gMnSc2	John
Locka	Locek	k1gMnSc2	Locek
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
Huma	Humus	k1gMnSc4	Humus
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
prvního	první	k4xOgMnSc4	první
filosofa	filosof	k1gMnSc4	filosof
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
eseji	esej	k1gFnSc6	esej
O	o	k7c6	o
penězích	peníze	k1gInPc6	peníze
důkladněji	důkladně	k6eAd2	důkladně
rozpracoval	rozpracovat	k5eAaPmAgMnS	rozpracovat
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
původní	původní	k2eAgFnSc4d1	původní
kvantitativní	kvantitativní	k2eAgFnSc4d1	kvantitativní
teorii	teorie	k1gFnSc4	teorie
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
téměř	téměř	k6eAd1	téměř
nezměněně	změněně	k6eNd1	změněně
až	až	k9	až
do	do	k7c2	do
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
zvýšení	zvýšení	k1gNnSc1	zvýšení
množství	množství	k1gNnSc2	množství
peněz	peníze	k1gInPc2	peníze
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
promítne	promítnout	k5eAaPmIp3nS	promítnout
vždy	vždy	k6eAd1	vždy
do	do	k7c2	do
růstu	růst	k1gInSc2	růst
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
ekonomiky	ekonomika	k1gFnSc2	ekonomika
peníze	peníz	k1gInPc1	peníz
přiváděny	přiváděn	k2eAgInPc1d1	přiváděn
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
se	se	k3xPyFc4	se
tak	tak	k9	tak
občas	občas	k6eAd1	občas
děje	dít	k5eAaImIp3nS	dít
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
časovou	časový	k2eAgFnSc7d1	časová
prodlevou	prodleva	k1gFnSc7	prodleva
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc1	peníz
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Nezáleží	záležet	k5eNaImIp3nS	záležet
tedy	tedy	k9	tedy
vůbec	vůbec	k9	vůbec
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
přivedeme	přivést	k5eAaPmIp1nP	přivést
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
zisků	zisk	k1gInPc2	zisk
a	a	k8xC	a
mezd	mzda	k1gFnPc2	mzda
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
pokaždé	pokaždé	k6eAd1	pokaždé
pouze	pouze	k6eAd1	pouze
nominální	nominální	k2eAgInSc1d1	nominální
<g/>
.	.	kIx.	.
</s>
<s>
Vydatnou	vydatný	k2eAgFnSc7d1	vydatná
peněžní	peněžní	k2eAgFnSc7d1	peněžní
emisí	emise	k1gFnSc7	emise
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
nikdy	nikdy	k6eAd1	nikdy
nemůže	moct	k5eNaImIp3nS	moct
podařit	podařit	k5eAaPmF	podařit
zvýšit	zvýšit	k5eAaPmF	zvýšit
výkon	výkon	k1gInSc4	výkon
reálné	reálný	k2eAgFnSc2d1	reálná
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Maximálně	maximálně	k6eAd1	maximálně
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
může	moct	k5eAaImIp3nS	moct
podařit	podařit	k5eAaPmF	podařit
změnit	změnit	k5eAaPmF	změnit
reálné	reálný	k2eAgInPc4d1	reálný
poměry	poměr	k1gInPc4	poměr
mezi	mezi	k7c7	mezi
příjmy	příjem	k1gInPc7	příjem
a	a	k8xC	a
majetkem	majetek	k1gInSc7	majetek
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
skupin	skupina	k1gFnPc2	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
kvantitativní	kvantitativní	k2eAgFnSc2d1	kvantitativní
teorie	teorie	k1gFnSc2	teorie
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
zvrátit	zvrátit	k5eAaPmF	zvrátit
mylné	mylný	k2eAgNnSc1d1	mylné
tvrzení	tvrzení	k1gNnSc1	tvrzení
merkantilistů	merkantilista	k1gMnPc2	merkantilista
o	o	k7c4	o
aktivní	aktivní	k2eAgFnSc4d1	aktivní
obchodní	obchodní	k2eAgFnSc4d1	obchodní
bilanci	bilance	k1gFnSc4	bilance
<g/>
.	.	kIx.	.
</s>
<s>
Vzkázal	vzkázat	k5eAaPmAgMnS	vzkázat
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
snažit	snažit	k5eAaImF	snažit
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
dosahovat	dosahovat	k5eAaImF	dosahovat
aktivní	aktivní	k2eAgFnSc2d1	aktivní
obchodní	obchodní	k2eAgFnSc2d1	obchodní
bilance	bilance	k1gFnSc2	bilance
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
hromadit	hromadit	k5eAaImF	hromadit
stále	stále	k6eAd1	stále
větší	veliký	k2eAgNnSc4d2	veliký
a	a	k8xC	a
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
přílivu	příliv	k1gInSc3	příliv
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
do	do	k7c2	do
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
cenová	cenový	k2eAgFnSc1d1	cenová
hladina	hladina	k1gFnSc1	hladina
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
a	a	k8xC	a
v	v	k7c6	v
globále	globál	k1gInSc6	globál
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
snižuje	snižovat	k5eAaImIp3nS	snižovat
její	její	k3xOp3gFnSc1	její
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
konkurenceschopnost	konkurenceschopnost	k1gFnSc1	konkurenceschopnost
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgInSc1d2	nižší
export	export	k1gInSc1	export
země	zem	k1gFnSc2	zem
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
vyšší	vysoký	k2eAgInSc1d2	vyšší
import	import	k1gInSc1	import
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
bilance	bilance	k1gFnSc1	bilance
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
opět	opět	k6eAd1	opět
vyrovnávají	vyrovnávat	k5eAaImIp3nP	vyrovnávat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
deficitu	deficit	k1gInSc2	deficit
<g/>
.	.	kIx.	.
</s>
<s>
Dostáváme	dostávat	k5eAaImIp1nP	dostávat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
našeho	náš	k3xOp1gNnSc2	náš
marného	marný	k2eAgNnSc2d1	marné
snažení	snažení	k1gNnSc2	snažení
<g/>
.	.	kIx.	.
</s>
<s>
Bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
přirozené	přirozený	k2eAgFnSc3d1	přirozená
tržní	tržní	k2eAgFnSc3d1	tržní
rovnováze	rovnováha	k1gFnSc3	rovnováha
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
naprostým	naprostý	k2eAgInSc7d1	naprostý
nesmyslem	nesmysl	k1gInSc7	nesmysl
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrátil	vyvrátit	k5eAaPmAgMnS	vyvrátit
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
obchod	obchod	k1gInSc1	obchod
není	být	k5eNaImIp3nS	být
žádná	žádný	k3yNgFnSc1	žádný
hra	hra	k1gFnSc1	hra
s	s	k7c7	s
nulovým	nulový	k2eAgInSc7d1	nulový
součtem	součet	k1gInSc7	součet
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
velice	velice	k6eAd1	velice
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
bohatství	bohatství	k1gNnSc4	bohatství
všech	všecek	k3xTgInPc2	všecek
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
Proto	proto	k8xC	proto
si	se	k3xPyFc3	se
troufám	troufat	k5eAaImIp1nS	troufat
přiznat	přiznat	k5eAaPmF	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejen	nejen	k6eAd1	nejen
jako	jako	k9	jako
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
Brit	Brit	k1gMnSc1	Brit
<g/>
,	,	kIx,	,
prosím	prosit	k5eAaImIp1nS	prosit
za	za	k7c4	za
vzkvétající	vzkvétající	k2eAgInSc4d1	vzkvétající
obchod	obchod	k1gInSc4	obchod
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
samotné	samotný	k2eAgFnSc2d1	samotná
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
jist	jist	k2eAgMnSc1d1	jist
<g/>
,	,	kIx,	,
že	že	k8xS	že
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
i	i	k8xC	i
všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
národy	národ	k1gInPc1	národ
by	by	kYmCp3nP	by
více	hodně	k6eAd2	hodně
vzkvétaly	vzkvétat	k5eAaImAgFnP	vzkvétat
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
jejich	jejich	k3xOp3gMnPc1	jejich
panovníci	panovník	k1gMnPc1	panovník
a	a	k8xC	a
ministři	ministr	k1gMnPc1	ministr
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
uplatnili	uplatnit	k5eAaPmAgMnP	uplatnit
takové	takový	k3xDgNnSc4	takový
velkorysé	velkorysý	k2eAgNnSc4d1	velkorysé
a	a	k8xC	a
benevolentní	benevolentní	k2eAgNnSc4d1	benevolentní
smýšlení	smýšlení	k1gNnSc4	smýšlení
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
David	David	k1gMnSc1	David
Hume	Hume	k1gFnSc1	Hume
<g/>
,	,	kIx,	,
Politické	politický	k2eAgFnPc1d1	politická
rozpravy	rozprava	k1gFnPc1	rozprava
<g/>
,	,	kIx,	,
esej	esej	k1gFnSc1	esej
O	o	k7c6	o
žárlivosti	žárlivost	k1gFnSc6	žárlivost
obchodu	obchod	k1gInSc2	obchod
<g/>
)	)	kIx)	)
Teorii	teorie	k1gFnSc4	teorie
úrokové	úrokový	k2eAgFnSc2d1	úroková
míry	míra	k1gFnSc2	míra
podávanou	podávaný	k2eAgFnSc4d1	podávaná
merkantilisty	merkantilista	k1gMnPc4	merkantilista
také	také	k9	také
vyvrátil	vyvrátit	k5eAaPmAgMnS	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
peněz	peníze	k1gInPc2	peníze
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c4	na
úrokovou	úrokový	k2eAgFnSc4d1	úroková
míru	míra	k1gFnSc4	míra
naprosto	naprosto	k6eAd1	naprosto
žádný	žádný	k3yNgInSc1	žádný
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Úrok	úrok	k1gInSc1	úrok
není	být	k5eNaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
peněžním	peněžní	k2eAgInSc7d1	peněžní
jevem	jev	k1gInSc7	jev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
reálným	reálný	k2eAgInSc7d1	reálný
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
nabídkou	nabídka	k1gFnSc7	nabídka
peněz	peníze	k1gInPc2	peníze
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
ho	on	k3xPp3gMnSc4	on
proto	proto	k8xC	proto
nikdy	nikdy	k6eAd1	nikdy
nepodaří	podařit	k5eNaPmIp3nS	podařit
snížit	snížit	k5eAaPmF	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Příliv	příliv	k1gInSc1	příliv
peněz	peníze	k1gInPc2	peníze
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
pouze	pouze	k6eAd1	pouze
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
ceny	cena	k1gFnPc1	cena
všeho	všecek	k3xTgNnSc2	všecek
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
množství	množství	k1gNnSc1	množství
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
si	se	k3xPyFc3	se
chtějí	chtít	k5eAaImIp3nP	chtít
lidé	člověk	k1gMnPc1	člověk
vypůjčit	vypůjčit	k5eAaPmF	vypůjčit
<g/>
.	.	kIx.	.
</s>
<s>
Převis	převis	k1gInSc1	převis
nabídky	nabídka	k1gFnSc2	nabídka
peněz	peníze	k1gInPc2	peníze
nad	nad	k7c7	nad
poptávkou	poptávka	k1gFnSc7	poptávka
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
rozplyne	rozplynout	k5eAaPmIp3nS	rozplynout
a	a	k8xC	a
úroková	úrokový	k2eAgFnSc1d1	úroková
míra	míra	k1gFnSc1	míra
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
vrátí	vrátit	k5eAaPmIp3nS	vrátit
na	na	k7c4	na
hodnotu	hodnota	k1gFnSc4	hodnota
předchozí	předchozí	k2eAgFnSc4d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
ty	ten	k3xDgFnPc1	ten
ceny	cena	k1gFnPc1	cena
budou	být	k5eAaImBp3nP	být
opět	opět	k6eAd1	opět
o	o	k7c4	o
něco	něco	k3yInSc4	něco
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Locka	Locek	k1gMnSc4	Locek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
co	co	k3yQnSc1	co
přináší	přinášet	k5eAaImIp3nS	přinášet
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nové	nový	k2eAgNnSc1d1	nové
<g/>
,	,	kIx,	,
přesné	přesný	k2eAgNnSc1d1	přesné
rozlišení	rozlišení	k1gNnSc1	rozlišení
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
představ	představa	k1gFnPc2	představa
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
přítomno	přítomen	k2eAgNnSc1d1	přítomno
a	a	k8xC	a
fakticky	fakticky	k6eAd1	fakticky
vnímáno	vnímat	k5eAaImNgNnS	vnímat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vnější	vnější	k2eAgFnSc2d1	vnější
nebo	nebo	k8xC	nebo
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
percepce	percepce	k1gFnSc2	percepce
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
impression	impression	k1gInSc1	impression
(	(	kIx(	(
<g/>
dojem	dojem	k1gInSc1	dojem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
(	(	kIx(	(
<g/>
kopie	kopie	k1gFnSc1	kopie
<g/>
)	)	kIx)	)
dojmů	dojem	k1gInPc2	dojem
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
plodí	plodit	k5eAaImIp3nP	plodit
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
a	a	k8xC	a
fantazie	fantazie	k1gFnSc1	fantazie
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
ideas	ideas	k1gInSc1	ideas
(	(	kIx(	(
<g/>
ideje	idea	k1gFnPc1	idea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Složené	složený	k2eAgFnPc1d1	složená
ideje	idea	k1gFnPc1	idea
jsou	být	k5eAaImIp3nP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
v	v	k7c6	v
rozumu	rozum	k1gInSc3	rozum
<g/>
,	,	kIx,	,
kombinací	kombinace	k1gFnSc7	kombinace
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
elementů	element	k1gInPc2	element
(	(	kIx(	(
<g/>
impresí	imprese	k1gFnPc2	imprese
a	a	k8xC	a
idejí	idea	k1gFnPc2	idea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
podobnosti	podobnost	k1gFnSc2	podobnost
a	a	k8xC	a
odlišnosti	odlišnost	k1gFnSc2	odlišnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
principu	princip	k1gInSc2	princip
vzniká	vznikat	k5eAaImIp3nS	vznikat
matematická	matematický	k2eAgFnSc1d1	matematická
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
prostorové	prostorový	k2eAgFnSc2d1	prostorová
a	a	k8xC	a
časové	časový	k2eAgFnSc2d1	časová
soumeznosti	soumeznost	k1gFnSc2	soumeznost
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
kauzálního	kauzální	k2eAgNnSc2d1	kauzální
spojení	spojení	k1gNnSc2	spojení
jako	jako	k8xC	jako
příčina	příčina	k1gFnSc1	příčina
a	a	k8xC	a
účinek	účinek	k1gInSc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
pravdivost	pravdivost	k1gFnSc4	pravdivost
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
takové	takový	k3xDgInPc1	takový
poznatky	poznatek	k1gInPc1	poznatek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
lze	lze	k6eAd1	lze
přímo	přímo	k6eAd1	přímo
vyvodit	vyvodit	k5eAaBmF	vyvodit
z	z	k7c2	z
impresí	imprese	k1gFnPc2	imprese
(	(	kIx(	(
<g/>
dojmů	dojem	k1gInPc2	dojem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
skepticismem	skepticismus	k1gInSc7	skepticismus
zasadil	zasadit	k5eAaPmAgMnS	zasadit
metafyzikům	metafyzik	k1gMnPc3	metafyzik
těžkou	těžký	k2eAgFnSc4d1	těžká
ránu	rána	k1gFnSc4	rána
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
-li	i	k?	-li
zničující	zničující	k2eAgFnSc1d1	zničující
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc4	on
dogmatičtí	dogmatický	k2eAgMnPc1d1	dogmatický
filosofové	filosof	k1gMnPc1	filosof
překračují	překračovat	k5eAaImIp3nP	překračovat
hranice	hranice	k1gFnPc4	hranice
a	a	k8xC	a
předstírají	předstírat	k5eAaImIp3nP	předstírat
vědění	vědění	k1gNnSc4	vědění
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prostě	prostě	k9	prostě
vědět	vědět	k5eAaImF	vědět
nemůžeme	moct	k5eNaImIp1nP	moct
<g/>
.	.	kIx.	.
</s>
<s>
Paměť	paměť	k1gFnSc1	paměť
a	a	k8xC	a
představivost	představivost	k1gFnSc1	představivost
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
spočívá	spočívat	k5eAaImIp3nS	spočívat
celý	celý	k2eAgInSc1d1	celý
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgInSc1d2	vyšší
duchovní	duchovní	k2eAgInSc1d1	duchovní
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
uzpůsobeny	uzpůsoben	k2eAgFnPc1d1	uzpůsobena
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
při	při	k7c6	při
sdružování	sdružování	k1gNnSc6	sdružování
představ	představa	k1gFnPc2	představa
mohou	moct	k5eAaImIp3nP	moct
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
mýlit	mýlit	k5eAaImF	mýlit
<g/>
.	.	kIx.	.
</s>
<s>
Určitým	určitý	k2eAgFnPc3d1	určitá
idejím	idea	k1gFnPc3	idea
podsouváme	podsouvat	k5eAaImIp1nP	podsouvat
nesprávné	správný	k2eNgFnSc2d1	nesprávná
imprese	imprese	k1gFnSc2	imprese
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
omyly	omyl	k1gInPc7	omyl
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
svorně	svorně	k6eAd1	svorně
podléháme	podléhat	k5eAaImIp1nP	podléhat
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
jakési	jakýsi	k3yIgInPc4	jakýsi
idoly	idol	k1gInPc4	idol
nebo	nebo	k8xC	nebo
klamy	klam	k1gInPc4	klam
lidského	lidský	k2eAgInSc2d1	lidský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Vezmu	vzít	k5eAaPmIp1nS	vzít
<g/>
-li	i	k?	-li
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
všechny	všechen	k3xTgFnPc4	všechen
kvality	kvalita	k1gFnPc4	kvalita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nám	my	k3xPp1nPc3	my
dány	dát	k5eAaPmNgInP	dát
impresemi	imprese	k1gFnPc7	imprese
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
zbude	zbýt	k5eAaPmIp3nS	zbýt
<g/>
?	?	kIx.	?
</s>
<s>
Představa	představa	k1gFnSc1	představa
<g/>
?	?	kIx.	?
</s>
<s>
Ale	ale	k9	ale
představa	představa	k1gFnSc1	představa
substance	substance	k1gFnSc1	substance
přece	přece	k9	přece
musí	muset	k5eAaImIp3nS	muset
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
nějaké	nějaký	k3yIgFnSc2	nějaký
imprese	imprese	k1gFnSc2	imprese
<g/>
.	.	kIx.	.
</s>
<s>
Představa	představa	k1gFnSc1	představa
nemá	mít	k5eNaImIp3nS	mít
vůbec	vůbec	k9	vůbec
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
vnějším	vnější	k2eAgNnSc6d1	vnější
vnímání	vnímání	k1gNnSc6	vnímání
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
pouze	pouze	k6eAd1	pouze
kvality	kvalita	k1gFnSc2	kvalita
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
spojení	spojení	k1gNnSc2	spojení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
ve	v	k7c6	v
vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
nazírání	nazírání	k1gNnSc6	nazírání
<g/>
,	,	kIx,	,
v	v	k7c6	v
činnosti	činnost	k1gFnSc6	činnost
rozumu	rozum	k1gInSc2	rozum
<g/>
,	,	kIx,	,
pozorujícího	pozorující	k2eAgMnSc2d1	pozorující
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
i	i	k9	i
o	o	k7c6	o
duchu	duch	k1gMnSc6	duch
<g/>
.	.	kIx.	.
</s>
<s>
Nutně	nutně	k6eAd1	nutně
se	se	k3xPyFc4	se
klade	klást	k5eAaImIp3nS	klást
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
pro	pro	k7c4	pro
takový	takový	k3xDgInSc4	takový
názor	názor	k1gInSc4	názor
ještě	ještě	k6eAd1	ještě
zbývá	zbývat	k5eAaImIp3nS	zbývat
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
<g/>
?	?	kIx.	?
</s>
<s>
Představa	představa	k1gFnSc1	představa
<g/>
.	.	kIx.	.
</s>
<s>
Zbývá	zbývat	k5eAaImIp3nS	zbývat
pouze	pouze	k6eAd1	pouze
uplývání	uplývání	k1gNnSc1	uplývání
představ	představa	k1gFnPc2	představa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
a	a	k8xC	a
mizí	mizet	k5eAaImIp3nP	mizet
zcela	zcela	k6eAd1	zcela
nahodile	nahodile	k6eAd1	nahodile
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
pojmu	pojem	k1gInSc2	pojem
kauzality	kauzalita	k1gFnSc2	kauzalita
<g/>
:	:	kIx,	:
Co	co	k3yQnSc1	co
znamená	znamenat	k5eAaImIp3nS	znamenat
kauzalita	kauzalita	k1gFnSc1	kauzalita
pro	pro	k7c4	pro
běžné	běžný	k2eAgNnSc4d1	běžné
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
?	?	kIx.	?
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
způsobená	způsobený	k2eAgFnSc1d1	způsobená
prvou	prvý	k4xOgFnSc4	prvý
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
její	její	k3xOp3gInSc1	její
nutný	nutný	k2eAgInSc1d1	nutný
následek	následek	k1gInSc1	následek
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
ale	ale	k8xC	ale
dospíváme	dospívat	k5eAaImIp1nP	dospívat
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
spojení	spojení	k1gNnSc1	spojení
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
?	?	kIx.	?
</s>
<s>
Představa	představa	k1gFnSc1	představa
kauzality	kauzalita	k1gFnSc2	kauzalita
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
pravdivá	pravdivý	k2eAgFnSc1d1	pravdivá
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
vykázat	vykázat	k5eAaPmF	vykázat
nějaká	nějaký	k3yIgFnSc1	nějaký
imprese	imprese	k1gFnSc1	imprese
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
nám	my	k3xPp1nPc3	my
toto	tento	k3xDgNnSc1	tento
spojení	spojení	k1gNnSc1	spojení
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
příčinné	příčinný	k2eAgInPc4d1	příčinný
a	a	k8xC	a
nevyhnutelné	vyhnutelný	k2eNgInPc4d1	nevyhnutelný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nějaká	nějaký	k3yIgFnSc1	nějaký
taková	takový	k3xDgFnSc1	takový
imprese	imprese	k1gFnSc1	imprese
<g/>
?	?	kIx.	?
</s>
<s>
Nikoli	nikoli	k9	nikoli
<g/>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
<g/>
,	,	kIx,	,
ne	ne	k9	ne
ve	v	k7c6	v
vnějším	vnější	k2eAgNnSc6d1	vnější
vnímání	vnímání	k1gNnSc6	vnímání
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
substance	substance	k1gFnSc2	substance
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
vnímám	vnímat	k5eAaImIp1nS	vnímat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
současnost	současnost	k1gFnSc4	současnost
a	a	k8xC	a
následnost	následnost	k1gFnSc4	následnost
<g/>
.	.	kIx.	.
</s>
<s>
Nevidím	vidět	k5eNaImIp1nS	vidět
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
že	že	k8xS	že
po	po	k7c6	po
události	událost	k1gFnSc6	událost
A	a	k9	a
následuje	následovat	k5eAaImIp3nS	následovat
událost	událost	k1gFnSc1	událost
B.	B.	kA	B.
Pozoruji	pozorovat	k5eAaImIp1nS	pozorovat
<g/>
-li	i	k?	-li
nějaký	nějaký	k3yIgInSc4	nějaký
proces	proces	k1gInSc4	proces
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vůbec	vůbec	k9	vůbec
nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
mám	mít	k5eAaImIp1nS	mít
před	před	k7c7	před
sebou	se	k3xPyFc7	se
kauzální	kauzální	k2eAgFnSc4d1	kauzální
spojitost	spojitost	k1gFnSc4	spojitost
anebo	anebo	k8xC	anebo
nahodilé	nahodilý	k2eAgNnSc4d1	nahodilé
setkání	setkání	k1gNnSc4	setkání
dvou	dva	k4xCgFnPc2	dva
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tomu	ten	k3xDgNnSc3	ten
bude	být	k5eAaImBp3nS	být
tak	tak	k6eAd1	tak
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chci	chtít	k5eAaImIp1nS	chtít
tomu	ten	k3xDgNnSc3	ten
věřit	věřit	k5eAaImF	věřit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
vnucuji	vnucovat	k5eAaImIp1nS	vnucovat
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
<g/>
,	,	kIx,	,
nutná	nutný	k2eAgFnSc1d1	nutná
příčinná	příčinný	k2eAgFnSc1d1	příčinná
souvislost	souvislost	k1gFnSc1	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
to	ten	k3xDgNnSc1	ten
dělá	dělat	k5eAaImIp3nS	dělat
pouhý	pouhý	k2eAgInSc1d1	pouhý
zvyk	zvyk	k1gInSc1	zvyk
ve	v	k7c6	v
mně	já	k3xPp1nSc6	já
<g/>
,	,	kIx,	,
psychologická	psychologický	k2eAgFnSc1d1	psychologická
potřeba	potřeba	k1gFnSc1	potřeba
a	a	k8xC	a
právě	právě	k9	právě
toto	tento	k3xDgNnSc4	tento
naléhání	naléhání	k1gNnSc4	naléhání
vnímám	vnímat	k5eAaImIp1nS	vnímat
jako	jako	k9	jako
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
impresi	imprese	k1gFnSc4	imprese
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gNnSc1	náš
vědění	vědění	k1gNnSc1	vědění
o	o	k7c6	o
přírodních	přírodní	k2eAgInPc6d1	přírodní
dějích	děj	k1gInPc6	děj
<g/>
,	,	kIx,	,
o	o	k7c6	o
souvislosti	souvislost	k1gFnSc6	souvislost
mezi	mezi	k7c7	mezi
vnímanými	vnímaný	k2eAgInPc7d1	vnímaný
fakty	fakt	k1gInPc7	fakt
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
přísně	přísně	k6eAd1	přísně
vzato	vzat	k2eAgNnSc1d1	vzato
žádné	žádný	k3yNgNnSc4	žádný
vědění	vědění	k1gNnSc4	vědění
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
představy	představa	k1gFnPc1	představa
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
zevrubnou	zevrubný	k2eAgFnSc4d1	zevrubná
teorii	teorie	k1gFnSc4	teorie
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
rozum	rozum	k1gInSc1	rozum
je	být	k5eAaImIp3nS	být
otrokem	otrok	k1gMnSc7	otrok
vášní	vášeň	k1gFnPc2	vášeň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
jsou	být	k5eAaImIp3nP	být
rozum	rozum	k1gInSc1	rozum
i	i	k8xC	i
mínění	mínění	k1gNnSc1	mínění
rozumu	rozum	k1gInSc2	rozum
(	(	kIx(	(
<g/>
víry	víra	k1gFnSc2	víra
<g/>
)	)	kIx)	)
motivačně	motivačně	k6eAd1	motivačně
neutrální	neutrální	k2eAgFnPc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Motivační	motivační	k2eAgFnSc1d1	motivační
síla	síla	k1gFnSc1	síla
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
z	z	k7c2	z
vášní	vášeň	k1gFnPc2	vášeň
-	-	kIx~	-
tužeb	tužba	k1gFnPc2	tužba
<g/>
,	,	kIx,	,
lásek	láska	k1gFnPc2	láska
<g/>
,	,	kIx,	,
citů	cit	k1gInPc2	cit
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
pocitů	pocit	k1gInPc2	pocit
mysli	mysl	k1gFnSc2	mysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
obrazu	obraz	k1gInSc6	obraz
hraje	hrát	k5eAaImIp3nS	hrát
rozum	rozum	k1gInSc4	rozum
plně	plně	k6eAd1	plně
reprezentační	reprezentační	k2eAgInSc4d1	reprezentační
a	a	k8xC	a
informační	informační	k2eAgFnPc4d1	informační
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
informace	informace	k1gFnSc1	informace
nemůže	moct	k5eNaImIp3nS	moct
motivovat	motivovat	k5eAaBmF	motivovat
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
je	on	k3xPp3gInPc4	on
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
vášněmi	vášeň	k1gFnPc7	vášeň
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
vášně	vášeň	k1gFnPc1	vášeň
jsou	být	k5eAaImIp3nP	být
nenázorné	názorný	k2eNgFnPc1d1	nenázorná
(	(	kIx(	(
<g/>
cit	cit	k1gInSc1	cit
světa	svět	k1gInSc2	svět
nijak	nijak	k6eAd1	nijak
nepředstaví	představit	k5eNaPmIp3nS	představit
<g/>
)	)	kIx)	)
a	a	k8xC	a
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
ani	ani	k8xC	ani
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
ani	ani	k8xC	ani
nepravdivé	pravdivý	k2eNgNnSc1d1	nepravdivé
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
rozumem	rozum	k1gInSc7	rozum
ani	ani	k8xC	ani
proti	proti	k7c3	proti
rozumu	rozum	k1gInSc3	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jeho	jeho	k3xOp3gFnSc1	jeho
morální	morální	k2eAgFnSc1d1	morální
psychologie	psychologie	k1gFnSc1	psychologie
dělí	dělit	k5eAaImIp3nS	dělit
mentální	mentální	k2eAgInPc4d1	mentální
stavy	stav	k1gInPc4	stav
ve	v	k7c4	v
dvě	dva	k4xCgFnPc4	dva
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
mínění	mínění	k1gNnSc1	mínění
rozumu	rozum	k1gInSc2	rozum
(	(	kIx(	(
<g/>
víry	víra	k1gFnSc2	víra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jsou	být	k5eAaImIp3nP	být
schopná	schopný	k2eAgFnSc1d1	schopná
pravdy	pravda	k1gFnPc1	pravda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
motivačně	motivačně	k6eAd1	motivačně
neutrální	neutrální	k2eAgFnPc4d1	neutrální
vášně	vášeň	k1gFnPc4	vášeň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
motivačně	motivačně	k6eAd1	motivačně
nabité	nabitý	k2eAgFnPc1d1	nabitá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
pravdy	pravda	k1gFnPc1	pravda
<g/>
.	.	kIx.	.
</s>
<s>
ALEXANDROV	ALEXANDROV	kA	ALEXANDROV
<g/>
,	,	kIx,	,
G.	G.	kA	G.
F.	F.	kA	F.
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
,	,	kIx,	,
Filosofie	filosofie	k1gFnSc1	filosofie
XV	XV	kA	XV
<g/>
.	.	kIx.	.
-	-	kIx~	-
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Hana	Hana	k1gFnSc1	Hana
Malínská	malínský	k2eAgFnSc1d1	Malínská
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vlček	Vlček	k1gMnSc1	Vlček
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
autoris	autoris	k1gFnSc1	autoris
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
518	[number]	k4	518
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
str	str	kA	str
<g/>
.	.	kIx.	.
296	[number]	k4	296
<g/>
-	-	kIx~	-
<g/>
302	[number]	k4	302
(	(	kIx(	(
<g/>
výrazně	výrazně	k6eAd1	výrazně
marxistický	marxistický	k2eAgInSc1d1	marxistický
pohled	pohled	k1gInSc1	pohled
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
KACHNÍK	kachník	k1gInSc1	kachník
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Cyrillo-Methodějská	Cyrillo-Methodějský	k2eAgFnSc1d1	Cyrillo-Methodějská
knihtiskárna	knihtiskárna	k1gFnSc1	knihtiskárna
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
V.	V.	kA	V.
Kotrba	kotrba	k1gFnSc1	kotrba
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
380	[number]	k4	380
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
str	str	kA	str
<g/>
.	.	kIx.	.
263	[number]	k4	263
<g/>
-	-	kIx~	-
<g/>
267	[number]	k4	267
(	(	kIx(	(
<g/>
výrazně	výrazně	k6eAd1	výrazně
katolický	katolický	k2eAgInSc4d1	katolický
pohled	pohled	k1gInSc4	pohled
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
LEGOWICZ	LEGOWICZ	kA	LEGOWICZ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Prehľad	Prehľad	k1gInSc1	Prehľad
dejín	dejín	k1gInSc1	dejín
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
:	:	kIx,	:
základy	základ	k1gInPc1	základ
doxografie	doxografie	k1gFnSc1	doxografie
<g/>
.	.	kIx.	.
</s>
<s>
Preložila	Preložit	k5eAaPmAgFnS	Preložit
Anna	Anna	k1gFnSc1	Anna
Varsiková	Varsikový	k2eAgFnSc1d1	Varsikový
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
655	[number]	k4	655
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
str	str	kA	str
<g/>
.	.	kIx.	.
415	[number]	k4	415
<g/>
-	-	kIx~	-
<g/>
419	[number]	k4	419
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
MASARYK	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gFnSc2	Garrigu
<g/>
.	.	kIx.	.
</s>
<s>
Přednášky	přednáška	k1gFnPc1	přednáška
a	a	k8xC	a
studie	studie	k1gFnPc1	studie
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1882	[number]	k4	1882
<g/>
-	-	kIx~	-
<g/>
1884	[number]	k4	1884
<g/>
:	:	kIx,	:
Hume	Hum	k1gInSc2	Hum
-	-	kIx~	-
Pascal	pascal	k1gInSc1	pascal
-	-	kIx~	-
Buckle	Buckl	k1gInSc5	Buckl
-	-	kIx~	-
O	o	k7c6	o
studiu	studio	k1gNnSc6	studio
děl	dělo	k1gNnPc2	dělo
básnických	básnický	k2eAgInPc2d1	básnický
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
T.G.	T.G.	k1gMnSc2	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
182	[number]	k4	182
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86142	[number]	k4	86142
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
str	str	kA	str
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
62	[number]	k4	62
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
McGREAL	McGREAL	k1gMnSc1	McGREAL
<g/>
,	,	kIx,	,
Ian	Ian	k1gMnSc1	Ian
Philip	Philip	k1gMnSc1	Philip
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
postavy	postava	k1gFnPc1	postava
západního	západní	k2eAgNnSc2d1	západní
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
:	:	kIx,	:
slovník	slovník	k1gInSc1	slovník
myslitelů	myslitel	k1gMnPc2	myslitel
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Martin	Martin	k1gMnSc1	Martin
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
707	[number]	k4	707
s.	s.	k?	s.
Obzor	obzor	k1gInSc1	obzor
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85190	[number]	k4	85190
<g/>
-	-	kIx~	-
<g/>
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Stať	stať	k1gFnSc1	stať
"	"	kIx"	"
<g/>
David	David	k1gMnSc1	David
Hume	Hum	k1gFnSc2	Hum
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
322	[number]	k4	322
<g/>
-	-	kIx~	-
<g/>
327	[number]	k4	327
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
MOURAL	MOURAL	kA	MOURAL
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Filosofie	filosofie	k1gFnSc1	filosofie
Davida	David	k1gMnSc2	David
Huma	Humus	k1gMnSc2	Humus
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
SOBOTKA	Sobotka	k1gMnSc1	Sobotka
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
,	,	kIx,	,
MOURAL	MOURAL	kA	MOURAL
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
ZNOJ	znoj	k1gFnSc1	znoj
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
novověké	novověký	k2eAgFnSc2d1	novověká
filosofie	filosofie	k1gFnSc2	filosofie
od	od	k7c2	od
Descarta	Descart	k1gMnSc2	Descart
po	po	k7c4	po
Hegela	Hegel	k1gMnSc4	Hegel
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
oprav	oprava	k1gFnPc2	oprava
<g/>
.	.	kIx.	.
<g/>
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Filosofický	filosofický	k2eAgInSc1d1	filosofický
ústav	ústav	k1gInSc1	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
272	[number]	k4	272
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7007	[number]	k4	7007
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
57	[number]	k4	57
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
RÖD	RÖD	kA	RÖD
<g/>
,	,	kIx,	,
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
<g/>
.	.	kIx.	.
</s>
<s>
Novověká	novověký	k2eAgFnSc1d1	novověká
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
,	,	kIx,	,
Od	od	k7c2	od
Newtona	Newton	k1gMnSc2	Newton
po	po	k7c4	po
Rousseaua	Rousseau	k1gMnSc4	Rousseau
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jindřich	Jindřich	k1gMnSc1	Jindřich
Karásek	Karásek	k1gMnSc1	Karásek
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
OIKOYMENH	OIKOYMENH	kA	OIKOYMENH
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
579	[number]	k4	579
s.	s.	k?	s.
Dějiny	dějiny	k1gFnPc1	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7298	[number]	k4	7298
<g/>
-	-	kIx~	-
<g/>
109	[number]	k4	109
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Kapitola	kapitola	k1gFnSc1	kapitola
"	"	kIx"	"
<g/>
David	David	k1gMnSc1	David
Hume	Hum	k1gFnSc2	Hum
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
397	[number]	k4	397
<g/>
-	-	kIx~	-
<g/>
443	[number]	k4	443
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
SCRUTON	SCRUTON	kA	SCRUTON
<g/>
,	,	kIx,	,
Roger	Roger	k1gInSc1	Roger
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Konzervativní	konzervativní	k2eAgMnPc1d1	konzervativní
myslitelé	myslitel	k1gMnPc1	myslitel
:	:	kIx,	:
výbor	výbor	k1gInSc1	výbor
esejů	esej	k1gInPc2	esej
z	z	k7c2	z
britského	britský	k2eAgInSc2d1	britský
konzervativního	konzervativní	k2eAgInSc2d1	konzervativní
čtvrtletníku	čtvrtletník	k1gInSc2	čtvrtletník
The	The	k1gFnSc2	The
Salisbury	Salisbura	k1gFnSc2	Salisbura
Review	Review	k1gFnSc2	Review
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
196	[number]	k4	196
s.	s.	k?	s.
TUMPACH	TUMPACH	k?	TUMPACH
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
a	a	k8xC	a
PODLAHA	podlaha	k1gFnSc1	podlaha
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
slovník	slovník	k1gInSc1	slovník
bohovědný	bohovědný	k2eAgInSc1d1	bohovědný
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Holbein	Holbein	k1gMnSc1	Holbein
<g/>
-	-	kIx~	-
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
sešity	sešit	k1gInPc1	sešit
131	[number]	k4	131
<g/>
-	-	kIx~	-
<g/>
151	[number]	k4	151
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Cyrillo-Methodějská	Cyrillo-Methodějský	k2eAgFnSc1d1	Cyrillo-Methodějská
knihtiskárna	knihtiskárna	k1gFnSc1	knihtiskárna
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
V.	V.	kA	V.
Kotrba	kotrba	k1gFnSc1	kotrba
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
-	-	kIx~	-
<g/>
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
448	[number]	k4	448
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Hume	Hume	k1gInSc1	Hume
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
125	[number]	k4	125
<g/>
-	-	kIx~	-
<g/>
126	[number]	k4	126
(	(	kIx(	(
<g/>
výrazně	výrazně	k6eAd1	výrazně
katolický	katolický	k2eAgInSc4d1	katolický
pohled	pohled	k1gInSc4	pohled
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
TVRDÝ	Tvrdý	k1gMnSc1	Tvrdý
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
dějinami	dějiny	k1gFnPc7	dějiny
evropské	evropský	k2eAgFnSc2d1	Evropská
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Komenium	Komenium	k1gNnSc1	Komenium
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
490	[number]	k4	490
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
str	str	kA	str
<g/>
.	.	kIx.	.
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
244	[number]	k4	244
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
TVRDÝ	Tvrdý	k1gMnSc1	Tvrdý
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
skutečnosti	skutečnost	k1gFnSc2	skutečnost
u	u	k7c2	u
Davida	David	k1gMnSc2	David
Huma	Humus	k1gMnSc2	Humus
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc3	jeho
význam	význam	k1gInSc4	význam
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
universita	universita	k1gFnSc1	universita
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Filosofická	filosofický	k2eAgFnSc1d1	filosofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
90	[number]	k4	90
s.	s.	k?	s.
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
David	David	k1gMnSc1	David
Hume	Hume	k1gFnSc7	Hume
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
David	David	k1gMnSc1	David
Hume	Hume	k1gInSc1	Hume
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Autor	autor	k1gMnSc1	autor
David	David	k1gMnSc1	David
Hume	Hume	k1gFnSc4	Hume
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
David	David	k1gMnSc1	David
Hume	Hum	k1gFnSc2	Hum
Plné	plný	k2eAgInPc4d1	plný
texty	text	k1gInPc4	text
děl	dít	k5eAaBmAgMnS	dít
autora	autor	k1gMnSc2	autor
David	David	k1gMnSc1	David
Hume	Hum	k1gMnSc2	Hum
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
David	David	k1gMnSc1	David
Hume	Hume	k1gFnSc1	Hume
<g/>
:	:	kIx,	:
Skeptik	skeptik	k1gMnSc1	skeptik
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Zkoumání	zkoumání	k1gNnSc4	zkoumání
morálních	morální	k2eAgInPc2d1	morální
principů	princip	k1gInPc2	princip
<g/>
,	,	kIx,	,
v	v	k7c6	v
anglictině	anglictina	k1gFnSc6	anglictina
a	a	k8xC	a
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
(	(	kIx(	(
<g/>
překl	překl	k1gInSc1	překl
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Škola	škola	k1gFnSc1	škola
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
</s>
