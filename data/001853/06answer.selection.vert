<s>
Přestože	přestože	k8xS	přestože
náznaky	náznak	k1gInPc1	náznak
kvantitativní	kvantitativní	k2eAgFnSc2d1	kvantitativní
teorie	teorie	k1gFnSc2	teorie
peněz	peníze	k1gInPc2	peníze
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
už	už	k9	už
u	u	k7c2	u
Cantillona	Cantillon	k1gMnSc2	Cantillon
a	a	k8xC	a
Johna	John	k1gMnSc2	John
Locka	Locek	k1gMnSc2	Locek
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
Huma	Humus	k1gMnSc4	Humus
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
prvního	první	k4xOgMnSc4	první
filosofa	filosof	k1gMnSc4	filosof
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
eseji	esej	k1gFnSc6	esej
O	o	k7c6	o
penězích	peníze	k1gInPc6	peníze
důkladněji	důkladně	k6eAd2	důkladně
rozpracoval	rozpracovat	k5eAaPmAgMnS	rozpracovat
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
původní	původní	k2eAgFnSc4d1	původní
kvantitativní	kvantitativní	k2eAgFnSc4d1	kvantitativní
teorii	teorie	k1gFnSc4	teorie
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
téměř	téměř	k6eAd1	téměř
nezměněně	změněně	k6eNd1	změněně
až	až	k9	až
do	do	k7c2	do
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
