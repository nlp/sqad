<p>
<s>
Klíčava	Klíčava	k1gFnSc1	Klíčava
je	být	k5eAaImIp3nS	být
potok	potok	k1gInSc4	potok
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
levostranný	levostranný	k2eAgInSc1d1	levostranný
přítok	přítok	k1gInSc1	přítok
Berounky	Berounka	k1gFnSc2	Berounka
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
toku	tok	k1gInSc2	tok
činí	činit	k5eAaImIp3nS	činit
22,6	[number]	k4	22,6
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
87,1	[number]	k4	87,1
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jejího	její	k3xOp3gInSc2	její
toku	tok	k1gInSc2	tok
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Chráněné	chráněný	k2eAgFnSc6d1	chráněná
krajinné	krajinný	k2eAgFnSc6d1	krajinná
oblasti	oblast	k1gFnSc6	oblast
Křivoklátsko	Křivoklátsko	k1gNnSc1	Křivoklátsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Klíčava	Klíčava	k1gFnSc1	Klíčava
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
Džbánu	džbán	k1gInSc2	džbán
<g/>
,	,	kIx,	,
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
mezi	mezi	k7c7	mezi
vrchy	vrch	k1gInPc7	vrch
Malý	Malý	k1gMnSc1	Malý
Louštín	Louštín	k1gMnSc1	Louštín
a	a	k8xC	a
Žalý	Žalý	k1gMnSc1	Žalý
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
450	[number]	k4	450
metrů	metr	k1gInPc2	metr
u	u	k7c2	u
samoty	samota	k1gFnSc2	samota
Třtická	Třtická	k1gFnSc1	Třtická
Lísa	lísa	k1gFnSc1	lísa
<g/>
,	,	kIx,	,
asi	asi	k9	asi
4	[number]	k4	4
kilometry	kilometr	k1gInPc7	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
Nového	Nového	k2eAgNnSc2d1	Nového
Strašecí	Strašecí	k1gNnSc2	Strašecí
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
teče	téct	k5eAaImIp3nS	téct
Křivoklátskou	křivoklátský	k2eAgFnSc7d1	Křivoklátská
vrchovinou	vrchovina	k1gFnSc7	vrchovina
<g/>
,	,	kIx,	,
údolím	údolí	k1gNnSc7	údolí
zaříznutým	zaříznutý	k2eAgFnPc3d1	zaříznutá
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
terénu	terén	k1gInSc2	terén
přibližně	přibližně	k6eAd1	přibližně
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
asi	asi	k9	asi
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
až	až	k6eAd1	až
1955	[number]	k4	1955
vybudována	vybudován	k2eAgFnSc1d1	vybudována
Klíčavská	Klíčavský	k2eAgFnSc1d1	Klíčavská
přehrada	přehrada	k1gFnSc1	přehrada
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc1	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
Kladno	Kladno	k1gNnSc4	Kladno
a	a	k8xC	a
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Klíčava	Klíčava	k1gFnSc1	Klíčava
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Berounky	Berounka	k1gFnSc2	Berounka
poblíž	poblíž	k7c2	poblíž
obce	obec	k1gFnSc2	obec
Zbečno	Zbečno	k1gNnSc1	Zbečno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Větší	veliký	k2eAgInPc1d2	veliký
přítoky	přítok	k1gInPc1	přítok
===	===	k?	===
</s>
</p>
<p>
<s>
levé	levá	k1gFnSc3	levá
–	–	k?	–
Lánský	lánský	k2eAgInSc4d1	lánský
potok	potok	k1gInSc4	potok
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Klíčava	Klíčava	k1gFnSc1	Klíčava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Odtok	odtok	k1gInSc1	odtok
VD	VD	kA	VD
Klíčava	Klíčava	k1gFnSc1	Klíčava
–	–	k?	–
aktuální	aktuální	k2eAgInSc4d1	aktuální
vodní	vodní	k2eAgInSc4d1	vodní
stav	stav	k1gInSc4	stav
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Povodí	povodí	k1gNnSc2	povodí
Vltavy	Vltava	k1gFnSc2	Vltava
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnPc1d1	střední
Čechy	Čechy	k1gFnPc1	Čechy
–	–	k?	–
vodstvo	vodstvo	k1gNnSc1	vodstvo
</s>
</p>
