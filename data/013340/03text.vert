<p>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1443	[number]	k4	1443
Kluž	Kluž	k1gFnSc1	Kluž
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1490	[number]	k4	1490
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1458	[number]	k4	1458
král	král	k1gMnSc1	král
uherský	uherský	k2eAgMnSc1d1	uherský
(	(	kIx(	(
<g/>
a	a	k8xC	a
chorvatský	chorvatský	k2eAgMnSc1d1	chorvatský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1469	[number]	k4	1469
také	také	k9	také
český	český	k2eAgInSc1d1	český
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1485	[number]	k4	1485
(	(	kIx(	(
<g/>
titulární	titulární	k2eAgMnSc1d1	titulární
<g/>
)	)	kIx)	)
rakouský	rakouský	k2eAgMnSc1d1	rakouský
vévoda	vévoda	k1gMnSc1	vévoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1469	[number]	k4	1469
byl	být	k5eAaImAgInS	být
částí	část	k1gFnSc7	část
českých	český	k2eAgInPc2d1	český
a	a	k8xC	a
moravských	moravský	k2eAgInPc2d1	moravský
stavů	stav	k1gInPc2	stav
zvolen	zvolit	k5eAaPmNgMnS	zvolit
českým	český	k2eAgMnSc7d1	český
vzdorokrálem	vzdorokrál	k1gMnSc7	vzdorokrál
proti	proti	k7c3	proti
Jiřímu	Jiří	k1gMnSc3	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
;	;	kIx,	;
pozdější	pozdní	k2eAgFnSc1d2	pozdější
dohoda	dohoda	k1gFnSc1	dohoda
s	s	k7c7	s
Vladislavem	Vladislav	k1gMnSc7	Vladislav
Jagellonským	jagellonský	k2eAgInSc7d1	jagellonský
(	(	kIx(	(
<g/>
1479	[number]	k4	1479
<g/>
)	)	kIx)	)
znamenala	znamenat	k5eAaImAgFnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
oba	dva	k4xCgMnPc1	dva
podrželi	podržet	k5eAaPmAgMnP	podržet
titul	titul	k1gInSc4	titul
dědičného	dědičný	k2eAgMnSc2d1	dědičný
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
a	a	k8xC	a
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
si	se	k3xPyFc3	se
pravomoci	pravomoc	k1gFnSc2	pravomoc
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zemském	zemský	k2eAgInSc6d1	zemský
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
vládl	vládnout	k5eAaImAgMnS	vládnout
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
Matyáš	Matyáš	k1gMnSc1	Matyáš
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
vedlejších	vedlejší	k2eAgFnPc6d1	vedlejší
zemích	zem	k1gFnPc6	zem
Koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgMnPc2d3	nejmocnější
panovníků	panovník	k1gMnPc2	panovník
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Uhersko	Uhersko	k1gNnSc1	Uhersko
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
zažilo	zažít	k5eAaPmAgNnS	zažít
vrchol	vrchol	k1gInSc4	vrchol
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnost	osobnost	k1gFnSc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Jméno	jméno	k1gNnSc1	jméno
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
===	===	k?	===
</s>
</p>
<p>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
Hunyadi	Hunyad	k1gMnPc1	Hunyad
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
Matyáš	Matyáš	k1gMnSc1	Matyáš
(	(	kIx(	(
<g/>
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
Matej	Matej	k1gInSc1	Matej
<g/>
)	)	kIx)	)
Korvín	Korvín	k1gInSc1	Korvín
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
mladší	mladý	k2eAgMnSc1d2	mladší
syn	syn	k1gMnSc1	syn
uherského	uherský	k2eAgMnSc2d1	uherský
gubernátora	gubernátor	k1gMnSc2	gubernátor
(	(	kIx(	(
<g/>
správce	správce	k1gMnSc2	správce
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
a	a	k8xC	a
slavného	slavný	k2eAgMnSc2d1	slavný
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
Jana	Jan	k1gMnSc2	Jan
(	(	kIx(	(
<g/>
Jánose	Jánosa	k1gFnSc6	Jánosa
<g/>
)	)	kIx)	)
Hunyadiho	Hunyadi	k1gMnSc2	Hunyadi
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
nepatřil	patřit	k5eNaImAgInS	patřit
ke	k	k7c3	k
staré	starý	k2eAgFnSc3d1	stará
rodové	rodový	k2eAgFnSc3d1	rodová
šlechtě	šlechta	k1gFnSc3	šlechta
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Hunyadi	Hunyad	k1gMnPc1	Hunyad
schopen	schopen	k2eAgMnSc1d1	schopen
vyšvihnout	vyšvihnout	k5eAaPmF	vyšvihnout
mezi	mezi	k7c4	mezi
barony	baron	k1gMnPc4	baron
a	a	k8xC	a
největší	veliký	k2eAgMnPc4d3	veliký
magnáty	magnát	k1gMnPc4	magnát
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
titul	titul	k1gInSc4	titul
jeho	jeho	k3xOp3gMnPc1	jeho
předkové	předek	k1gMnPc1	předek
získali	získat	k5eAaPmAgMnP	získat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
až	až	k6eAd1	až
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
-	-	kIx~	-
poprvé	poprvé	k6eAd1	poprvé
je	být	k5eAaImIp3nS	být
rodina	rodina	k1gFnSc1	rodina
zmiňována	zmiňován	k2eAgFnSc1d1	zmiňována
v	v	k7c6	v
privilegiu	privilegium	k1gNnSc6	privilegium
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgInSc1d1	lucemburský
udělil	udělit	k5eAaPmAgInS	udělit
roku	rok	k1gInSc2	rok
1409	[number]	k4	1409
Janovu	Janův	k2eAgFnSc4d1	Janova
otci	otec	k1gMnSc6	otec
Vajkovi	Vajek	k1gMnSc6	Vajek
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
panství	panství	k1gNnSc2	panství
Hunyad	Hunyad	k1gInSc1	Hunyad
(	(	kIx(	(
<g/>
Vajdahunyad	Vajdahunyad	k1gInSc1	Vajdahunyad
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Hunedoara	Hunedoara	k1gFnSc1	Hunedoara
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
)	)	kIx)	)
v	v	k7c6	v
Transylvánii	Transylvánie	k1gFnSc6	Transylvánie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nedostatek	nedostatek	k1gInSc4	nedostatek
urozeného	urozený	k2eAgInSc2d1	urozený
původu	původ	k1gInSc2	původ
Jan	Jan	k1gMnSc1	Jan
i	i	k8xC	i
Matyáš	Matyáš	k1gMnSc1	Matyáš
dorovnávali	dorovnávat	k5eAaImAgMnP	dorovnávat
především	především	k9	především
vojenskými	vojenský	k2eAgInPc7d1	vojenský
a	a	k8xC	a
majetkovými	majetkový	k2eAgInPc7d1	majetkový
zisky	zisk	k1gInPc7	zisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Matyášově	Matyášův	k2eAgFnSc6d1	Matyášova
byly	být	k5eAaImAgFnP	být
pak	pak	k6eAd1	pak
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
pověsti	pověst	k1gFnPc1	pověst
o	o	k7c6	o
původu	původ	k1gInSc6	původ
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
nemanželským	manželský	k2eNgMnSc7d1	nemanželský
synem	syn	k1gMnSc7	syn
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
císař	císař	k1gMnSc1	císař
zplodil	zplodit	k5eAaPmAgMnS	zplodit
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
transylvánskou	transylvánský	k2eAgFnSc7d1	Transylvánská
dívkou	dívka	k1gFnSc7	dívka
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Morzsinai	Morzsina	k1gFnSc2	Morzsina
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
prý	prý	k9	prý
dostala	dostat	k5eAaPmAgFnS	dostat
od	od	k7c2	od
Zikmunda	Zikmund	k1gMnSc2	Zikmund
prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
později	pozdě	k6eAd2	pozdě
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
prsten	prsten	k1gInSc4	prsten
odnesl	odnést	k5eAaPmAgMnS	odnést
havran	havran	k1gMnSc1	havran
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jí	jíst	k5eAaImIp3nS	jíst
navrácen	navrácen	k2eAgMnSc1d1	navrácen
a	a	k8xC	a
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
se	se	k3xPyFc4	se
jím	on	k3xPp3gInSc7	on
Jan	Jana	k1gFnPc2	Jana
Hunyadi	Hunyad	k1gMnPc1	Hunyad
skutečně	skutečně	k6eAd1	skutečně
prokázal	prokázat	k5eAaPmAgInS	prokázat
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
na	na	k7c4	na
Zikmundův	Zikmundův	k2eAgInSc4d1	Zikmundův
dvůr	dvůr	k1gInSc4	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
alespoň	alespoň	k9	alespoň
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
pověst	pověst	k1gFnSc4	pověst
Matyášův	Matyášův	k2eAgMnSc1d1	Matyášův
dvorní	dvorní	k2eAgMnSc1d1	dvorní
historiograf	historiograf	k1gMnSc1	historiograf
Antonio	Antonio	k1gMnSc1	Antonio
Bonfini	Bonfin	k2eAgMnPc1d1	Bonfin
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
sám	sám	k3xTgMnSc1	sám
jí	jíst	k5eAaImIp3nS	jíst
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
totiž	totiž	k9	totiž
jinou	jiný	k2eAgFnSc4d1	jiná
pověst	pověst	k1gFnSc4	pověst
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
základ	základ	k1gInSc4	základ
v	v	k7c6	v
antickém	antický	k2eAgInSc6d1	antický
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
rodovou	rodový	k2eAgFnSc4d1	rodová
erbovní	erbovní	k2eAgFnSc4d1	erbovní
pověst	pověst	k1gFnSc4	pověst
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
římský	římský	k2eAgInSc4d1	římský
patricijský	patricijský	k2eAgInSc4d1	patricijský
rod	rod	k1gInSc4	rod
Corvini	Corvin	k2eAgMnPc1d1	Corvin
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jiný	jiný	k2eAgMnSc1d1	jiný
Ital	Ital	k1gMnSc1	Ital
na	na	k7c6	na
uherském	uherský	k2eAgInSc6d1	uherský
královském	královský	k2eAgInSc6d1	královský
dvoře	dvůr	k1gInSc6	dvůr
Pietro	Pietro	k1gNnSc1	Pietro
Ransano	Ransana	k1gFnSc5	Ransana
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
pověst	pověst	k1gFnSc1	pověst
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
dunajským	dunajský	k2eAgInSc7d1	dunajský
ostrovem	ostrov	k1gInSc7	ostrov
Keve	Keve	k1gInSc1	Keve
(	(	kIx(	(
<g/>
Covinus	Covinus	k1gInSc1	Covinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prý	prý	k9	prý
rodina	rodina	k1gFnSc1	rodina
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
přežívala	přežívat	k5eAaImAgFnS	přežívat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Rodové	rodový	k2eAgNnSc1d1	rodové
jméno	jméno	k1gNnSc1	jméno
Corvinus	Corvinus	k1gInSc1	Corvinus
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
erbovní	erbovní	k2eAgFnSc2d1	erbovní
pověsti	pověst	k1gFnPc1	pověst
o	o	k7c6	o
havranu	havran	k1gMnSc6	havran
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
corvus	corvus	k1gInSc1	corvus
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Havrana	Havran	k1gMnSc4	Havran
s	s	k7c7	s
prstenem	prsten	k1gInSc7	prsten
v	v	k7c6	v
zobáku	zobák	k1gInSc6	zobák
najdeme	najít	k5eAaPmIp1nP	najít
na	na	k7c6	na
Matyášově	Matyášův	k2eAgInSc6d1	Matyášův
kostele	kostel	k1gInSc6	kostel
(	(	kIx(	(
<g/>
též	též	k9	též
Korunovační	korunovační	k2eAgInSc4d1	korunovační
kostel	kostel	k1gInSc4	kostel
<g/>
)	)	kIx)	)
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
a	a	k8xC	a
na	na	k7c6	na
mnohých	mnohý	k2eAgNnPc6d1	mnohé
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
pošty	pošta	k1gFnSc2	pošta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nepřesné	přesný	k2eNgNnSc1d1	nepřesné
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
přijímané	přijímaný	k2eAgNnSc1d1	přijímané
téměř	téměř	k6eAd1	téměř
všemi	všecek	k3xTgFnPc7	všecek
národními	národní	k2eAgFnPc7d1	národní
historiografiemi	historiografie	k1gFnPc7	historiografie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Matyáš	Matyáš	k1gMnSc1	Matyáš
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nikdy	nikdy	k6eAd1	nikdy
neoznačoval	označovat	k5eNaImAgMnS	označovat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
už	už	k9	už
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
jej	on	k3xPp3gInSc4	on
tak	tak	k6eAd1	tak
nazývali	nazývat	k5eAaImAgMnP	nazývat
jeho	jeho	k3xOp3gMnPc1	jeho
životopisci	životopisec	k1gMnPc1	životopisec
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
autoři	autor	k1gMnPc1	autor
oslavující	oslavující	k2eAgFnSc4d1	oslavující
jeho	jeho	k3xOp3gFnSc4	jeho
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Přesnější	přesný	k2eAgFnSc1d2	přesnější
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
jméno	jméno	k1gNnSc4	jméno
Matyáš	Matyáš	k1gMnSc1	Matyáš
Hunyadi	Hunyad	k1gMnPc1	Hunyad
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Hunyadi	Hunyad	k1gMnPc1	Hunyad
Mátyás	Mátyása	k1gFnPc2	Mátyása
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzdělání	vzdělání	k1gNnSc1	vzdělání
===	===	k?	===
</s>
</p>
<p>
<s>
Matyášovi	Matyáš	k1gMnSc3	Matyáš
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
důkladného	důkladný	k2eAgNnSc2d1	důkladné
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
ovlivněného	ovlivněný	k2eAgNnSc2d1	ovlivněné
nově	nově	k6eAd1	nově
do	do	k7c2	do
Uher	uher	k1gInSc1	uher
pronikajícím	pronikající	k2eAgMnSc7d1	pronikající
humanistickým	humanistický	k2eAgMnSc7d1	humanistický
duchem	duch	k1gMnSc7	duch
<g/>
.	.	kIx.	.
</s>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
vedle	vedle	k7c2	vedle
latiny	latina	k1gFnSc2	latina
i	i	k9	i
němčinu	němčina	k1gFnSc4	němčina
<g/>
,	,	kIx,	,
češtinu	čeština	k1gFnSc4	čeština
<g/>
,	,	kIx,	,
chorvatštinu	chorvatština	k1gFnSc4	chorvatština
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
i	i	k9	i
znalosti	znalost	k1gFnPc4	znalost
římských	římský	k2eAgFnPc2d1	římská
a	a	k8xC	a
řeckých	řecký	k2eAgFnPc2d1	řecká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
mocný	mocný	k2eAgMnSc1d1	mocný
panovník	panovník	k1gMnSc1	panovník
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
na	na	k7c6	na
budínském	budínský	k2eAgInSc6d1	budínský
dvoře	dvůr	k1gInSc6	dvůr
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
slavnou	slavný	k2eAgFnSc4d1	slavná
knihovnu	knihovna	k1gFnSc4	knihovna
o	o	k7c4	o
asi	asi	k9	asi
2	[number]	k4	2
500	[number]	k4	500
svazcích	svazek	k1gInPc6	svazek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
ovlivnění	ovlivnění	k1gNnSc1	ovlivnění
italskou	italský	k2eAgFnSc7d1	italská
renesancí	renesance	k1gFnSc7	renesance
projevovalo	projevovat	k5eAaImAgNnS	projevovat
i	i	k9	i
v	v	k7c6	v
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
mecenášské	mecenášský	k2eAgFnSc6d1	mecenášská
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
dvoře	dvůr	k1gInSc6	dvůr
pobývali	pobývat	k5eAaImAgMnP	pobývat
četní	četný	k2eAgMnPc1d1	četný
učenci	učenec	k1gMnPc1	učenec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Martius	Martius	k1gInSc1	Martius
Galeotti	Galeotti	k1gNnSc1	Galeotti
z	z	k7c2	z
Narni	Nareň	k1gFnSc5	Nareň
<g/>
,	,	kIx,	,
Antonio	Antonio	k1gMnSc1	Antonio
Bonfini	Bonfin	k2eAgMnPc1d1	Bonfin
z	z	k7c2	z
Ascoli	Ascole	k1gFnSc6	Ascole
<g/>
,	,	kIx,	,
Aurelio	Aurelio	k6eAd1	Aurelio
Brandolini	Brandolin	k2eAgMnPc1d1	Brandolin
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Ranzano	Ranzana	k1gFnSc5	Ranzana
a	a	k8xC	a
Tadeáš	Tadeáš	k1gMnSc1	Tadeáš
Ugoletti	Ugoletť	k1gFnSc2	Ugoletť
<g/>
;	;	kIx,	;
Marsilio	Marsilio	k1gMnSc1	Marsilio
Ficino	Ficino	k1gNnSc4	Ficino
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
uherský	uherský	k2eAgInSc4d1	uherský
královský	královský	k2eAgInSc4d1	královský
dvůr	dvůr	k1gInSc4	dvůr
zván	zván	k2eAgInSc4d1	zván
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepřijel	přijet	k5eNaPmAgMnS	přijet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
králových	králův	k2eAgFnPc6d1	králova
objednávkách	objednávka	k1gFnPc6	objednávka
pracovali	pracovat	k5eAaImAgMnP	pracovat
např.	např.	kA	např.
Filippo	Filippa	k1gFnSc5	Filippa
Lippi	Lippi	k1gNnSc1	Lippi
<g/>
,	,	kIx,	,
Verrocchio	Verrocchio	k1gNnSc1	Verrocchio
nebo	nebo	k8xC	nebo
Benedetto	Benedetto	k1gNnSc1	Benedetto
da	da	k?	da
Maiano	Maiana	k1gFnSc5	Maiana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rodinný	rodinný	k2eAgInSc1d1	rodinný
život	život	k1gInSc1	život
===	===	k?	===
</s>
</p>
<p>
<s>
Ženatý	ženatý	k2eAgMnSc1d1	ženatý
byl	být	k5eAaImAgMnS	být
Matyáš	Matyáš	k1gMnSc1	Matyáš
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
Kateřinou	Kateřina	k1gFnSc7	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
krátké	krátký	k2eAgNnSc1d1	krátké
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Kateřina	Kateřina	k1gFnSc1	Kateřina
zemřela	zemřít	k5eAaPmAgFnS	zemřít
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
porodu	porod	k1gInSc6	porod
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1464	[number]	k4	1464
<g/>
.	.	kIx.	.
</s>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
najít	najít	k5eAaPmF	najít
další	další	k2eAgFnSc4d1	další
ženu	žena	k1gFnSc4	žena
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
středoevropských	středoevropský	k2eAgInPc2d1	středoevropský
panovnických	panovnický	k2eAgInPc2d1	panovnický
dvorů	dvůr	k1gInPc2	dvůr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
snaha	snaha	k1gFnSc1	snaha
nebyla	být	k5eNaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Beatricí	Beatrice	k1gFnSc7	Beatrice
Neapolskou	neapolský	k2eAgFnSc7d1	neapolská
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
neapolského	neapolský	k2eAgMnSc4d1	neapolský
krále	král	k1gMnSc4	král
Ferranteho	Ferrante	k1gMnSc2	Ferrante
I.	I.	kA	I.
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
sice	sice	k8xC	sice
neporodila	porodit	k5eNaPmAgFnS	porodit
legitimního	legitimní	k2eAgMnSc4d1	legitimní
potomka	potomek	k1gMnSc4	potomek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
už	už	k9	už
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
a	a	k8xC	a
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
více	hodně	k6eAd2	hodně
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
důležitou	důležitý	k2eAgFnSc7d1	důležitá
figurou	figura	k1gFnSc7	figura
ovlivňující	ovlivňující	k2eAgFnSc4d1	ovlivňující
vnitropolitickou	vnitropolitický	k2eAgFnSc4d1	vnitropolitická
situaci	situace	k1gFnSc4	situace
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
jejím	její	k3xOp3gInSc7	její
příchodem	příchod	k1gInSc7	příchod
upevnila	upevnit	k5eAaPmAgFnS	upevnit
kulturní	kulturní	k2eAgNnPc1d1	kulturní
pouta	pouto	k1gNnPc4	pouto
budínského	budínský	k2eAgInSc2d1	budínský
dvora	dvůr	k1gInSc2	dvůr
s	s	k7c7	s
renesanční	renesanční	k2eAgFnSc7d1	renesanční
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Jediného	jediný	k2eAgMnSc4d1	jediný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nelegitimního	legitimní	k2eNgMnSc2d1	nelegitimní
<g/>
,	,	kIx,	,
potomka	potomek	k1gMnSc4	potomek
porodila	porodit	k5eAaPmAgFnS	porodit
Matyášovi	Matyáš	k1gMnSc6	Matyáš
Barbara	Barbara	k1gFnSc1	Barbara
Edelpöcková	Edelpöcková	k1gFnSc1	Edelpöcková
ze	z	k7c2	z
Steinu	Stein	k1gMnSc6	Stein
nad	nad	k7c7	nad
Dunajem	Dunaj	k1gInSc7	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jasně	jasně	k6eAd1	jasně
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
druhého	druhý	k4xOgNnSc2	druhý
manželství	manželství	k1gNnSc1	manželství
dítě	dítě	k1gNnSc4	dítě
nevzejde	vzejít	k5eNaPmIp3nS	vzejít
<g/>
,	,	kIx,	,
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Jana	Jan	k1gMnSc4	Jan
mezinárodně	mezinárodně	k6eAd1	mezinárodně
potvrdit	potvrdit	k5eAaPmF	potvrdit
legitimitu	legitimita	k1gFnSc4	legitimita
a	a	k8xC	a
prosadit	prosadit	k5eAaPmF	prosadit
jej	on	k3xPp3gMnSc4	on
jako	jako	k8xC	jako
svého	svůj	k3xOyFgMnSc2	svůj
nástupce	nástupce	k1gMnSc2	nástupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politický	politický	k2eAgInSc1d1	politický
styl	styl	k1gInSc1	styl
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jeho	jeho	k3xOp3gMnSc2	jeho
otce	otec	k1gMnSc2	otec
v	v	k7c6	v
rodové	rodový	k2eAgFnSc6d1	rodová
politice	politika	k1gFnSc6	politika
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Pohrobkem	pohrobek	k1gMnSc7	pohrobek
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
hraběte	hrabě	k1gMnSc2	hrabě
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Cellského	Cellský	k2eAgMnSc2d1	Cellský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
byla	být	k5eAaImAgFnS	být
stoupající	stoupající	k2eAgFnSc4d1	stoupající
moc	moc	k1gFnSc4	moc
obou	dva	k4xCgMnPc2	dva
bratrů	bratr	k1gMnPc2	bratr
ovlivňující	ovlivňující	k2eAgFnSc4d1	ovlivňující
panovnickou	panovnický	k2eAgFnSc4d1	panovnická
autoritu	autorita	k1gFnSc4	autorita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
a	a	k8xC	a
Matyáš	Matyáš	k1gMnSc1	Matyáš
Hunyadiové	Hunyadius	k1gMnPc1	Hunyadius
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1456	[number]	k4	1456
pozváni	pozván	k2eAgMnPc1d1	pozván
panovníkem	panovník	k1gMnSc7	panovník
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Pohrobkem	pohrobek	k1gMnSc7	pohrobek
na	na	k7c4	na
hostinu	hostina	k1gFnSc4	hostina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
zajati	zajat	k2eAgMnPc1d1	zajat
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Hunyadi	Hunyad	k1gMnPc1	Hunyad
byl	být	k5eAaImAgInS	být
ihned	ihned	k6eAd1	ihned
popraven	popraven	k2eAgMnSc1d1	popraven
a	a	k8xC	a
mladší	mladý	k2eAgMnSc1d2	mladší
Matyáš	Matyáš	k1gMnSc1	Matyáš
odvezen	odvezen	k2eAgMnSc1d1	odvezen
nejdříve	dříve	k6eAd3	dříve
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
pak	pak	k6eAd1	pak
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
také	také	k6eAd1	také
sťat	stnout	k5eAaPmNgInS	stnout
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Pohrobek	pohrobek	k1gMnSc1	pohrobek
ale	ale	k8xC	ale
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
k	k	k7c3	k
exekuci	exekuce	k1gFnSc3	exekuce
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
<g/>
,	,	kIx,	,
a	a	k8xC	a
osvobozený	osvobozený	k2eAgMnSc1d1	osvobozený
Matyáš	Matyáš	k1gMnSc1	Matyáš
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
upevnil	upevnit	k5eAaPmAgInS	upevnit
své	svůj	k3xOyFgInPc4	svůj
vztahy	vztah	k1gInPc4	vztah
k	k	k7c3	k
Jiřímu	Jiří	k1gMnSc3	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zkušenost	zkušenost	k1gFnSc1	zkušenost
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
významná	významný	k2eAgFnSc1d1	významná
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
politický	politický	k2eAgInSc4d1	politický
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
praktikoval	praktikovat	k5eAaImAgInS	praktikovat
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vnější	vnější	k2eAgFnSc4d1	vnější
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
<s>
Vnitropoliticky	vnitropoliticky	k6eAd1	vnitropoliticky
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
stala	stát	k5eAaPmAgFnS	stát
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
centralizovaná	centralizovaný	k2eAgFnSc1d1	centralizovaná
monarchie	monarchie	k1gFnSc1	monarchie
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
podílem	podíl	k1gInSc7	podíl
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Matyášovi	Matyáš	k1gMnSc3	Matyáš
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
rozhodným	rozhodný	k2eAgInSc7d1	rozhodný
způsobem	způsob	k1gInSc7	způsob
omezit	omezit	k5eAaPmF	omezit
moc	moc	k1gFnSc4	moc
uherských	uherský	k2eAgMnPc2d1	uherský
magnátů	magnát	k1gMnPc2	magnát
a	a	k8xC	a
církevních	církevní	k2eAgMnPc2d1	církevní
hodnostářů	hodnostář	k1gMnPc2	hodnostář
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jeho	jeho	k3xOp3gFnSc4	jeho
protivníci	protivník	k1gMnPc1	protivník
označovali	označovat	k5eAaImAgMnP	označovat
za	za	k7c4	za
tyranský	tyranský	k2eAgInSc4d1	tyranský
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc4d1	významný
úřady	úřad	k1gInPc4	úřad
rozděloval	rozdělovat	k5eAaImAgMnS	rozdělovat
mezi	mezi	k7c4	mezi
své	svůj	k3xOyFgMnPc4	svůj
stoupence	stoupenec	k1gMnPc4	stoupenec
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
drobné	drobný	k2eAgFnSc2d1	drobná
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
měšťanů	měšťan	k1gMnPc2	měšťan
a	a	k8xC	a
cizích	cizí	k2eAgMnPc2d1	cizí
odborníků	odborník	k1gMnPc2	odborník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
svými	svůj	k3xOyFgFnPc7	svůj
ambicemi	ambice	k1gFnPc7	ambice
nemohli	moct	k5eNaImAgMnP	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
církevním	církevní	k2eAgMnPc3d1	církevní
hodnostářům	hodnostář	k1gMnPc3	hodnostář
vládl	vládnout	k5eAaImAgInS	vládnout
stejně	stejně	k6eAd1	stejně
pevnou	pevný	k2eAgFnSc7d1	pevná
rukou	ruka	k1gFnSc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
další	další	k2eAgMnPc1d1	další
silní	silný	k2eAgMnPc1d1	silný
středověcí	středověký	k2eAgMnPc1d1	středověký
panovníci	panovník	k1gMnPc1	panovník
si	se	k3xPyFc3	se
nárokoval	nárokovat	k5eAaImAgMnS	nárokovat
právo	právo	k1gNnSc4	právo
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c6	o
osobách	osoba	k1gFnPc6	osoba
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
ten	ten	k3xDgInSc4	ten
který	který	k3yIgInSc4	který
úřad	úřad	k1gInSc4	úřad
udělen	udělen	k2eAgInSc1d1	udělen
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
úřadu	úřad	k1gInSc2	úřad
ostřihomského	ostřihomský	k2eAgMnSc2d1	ostřihomský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
udělil	udělit	k5eAaPmAgMnS	udělit
např.	např.	kA	např.
Hipolytovi	Hipolyt	k1gMnSc3	Hipolyt
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Este	Est	k1gMnSc5	Est
<g/>
,	,	kIx,	,
svému	svůj	k3xOyFgMnSc3	svůj
teprve	teprve	k6eAd1	teprve
sedmiletému	sedmiletý	k2eAgMnSc3d1	sedmiletý
synovci	synovec	k1gMnSc3	synovec
a	a	k8xC	a
ferrarskému	ferrarský	k2eAgMnSc3d1	ferrarský
princi	princ	k1gMnSc3	princ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
zahraničním	zahraniční	k2eAgFnPc3d1	zahraniční
ambicím	ambice	k1gFnPc3	ambice
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
Korvín	Korvín	k1gInSc1	Korvín
disponovat	disponovat	k5eAaBmF	disponovat
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
peněz	peníze	k1gInPc2	peníze
v	v	k7c6	v
hotovosti	hotovost	k1gFnSc6	hotovost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jej	on	k3xPp3gMnSc4	on
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
aktivní	aktivní	k2eAgFnSc3d1	aktivní
hospodářské	hospodářský	k2eAgFnSc3d1	hospodářská
politice	politika	k1gFnSc3	politika
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
podpoře	podpora	k1gFnSc6	podpora
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
jejich	jejich	k3xOp3gNnPc2	jejich
privilegií	privilegium	k1gNnPc2	privilegium
a	a	k8xC	a
upevňování	upevňování	k1gNnSc2	upevňování
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
pout	pouto	k1gNnPc2	pouto
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
Slezskem	Slezsko	k1gNnSc7	Slezsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
Uher	Uhry	k1gFnPc2	Uhry
často	často	k6eAd1	často
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
součinnosti	součinnost	k1gFnSc6	součinnost
s	s	k7c7	s
římskou	římský	k2eAgFnSc7d1	římská
kurií	kurie	k1gFnSc7	kurie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
bylo	být	k5eAaImAgNnS	být
dáno	dát	k5eAaPmNgNnS	dát
geografickou	geografický	k2eAgFnSc7d1	geografická
polohou	poloha	k1gFnSc7	poloha
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
celou	celý	k2eAgFnSc4d1	celá
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
Evropu	Evropa	k1gFnSc4	Evropa
bránila	bránit	k5eAaImAgFnS	bránit
před	před	k7c7	před
osmanskými	osmanský	k2eAgInPc7d1	osmanský
Turky	turek	k1gInPc7	turek
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterým	který	k3yIgInPc3	který
Matyáš	Matyáš	k1gMnSc1	Matyáš
za	za	k7c4	za
finanční	finanční	k2eAgFnPc4d1	finanční
podpory	podpora	k1gFnPc4	podpora
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgFnPc1	tři
výpravy	výprava	k1gFnPc1	výprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
rámci	rámec	k1gInSc6	rámec
ovšem	ovšem	k9	ovšem
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
zásadnímu	zásadní	k2eAgNnSc3d1	zásadní
střetnutí	střetnutí	k1gNnSc3	střetnutí
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
měly	mít	k5eAaImAgFnP	mít
podobu	podoba	k1gFnSc4	podoba
drobných	drobný	k2eAgFnPc2d1	drobná
šarvátek	šarvátka	k1gFnPc2	šarvátka
a	a	k8xC	a
v	v	k7c6	v
okamžicích	okamžik	k1gInPc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Matyáš	Matyáš	k1gMnSc1	Matyáš
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
své	svůj	k3xOyFgNnSc4	svůj
vojsko	vojsko	k1gNnSc4	vojsko
a	a	k8xC	a
finance	finance	k1gFnPc4	finance
zapojit	zapojit	k5eAaPmF	zapojit
do	do	k7c2	do
akcí	akce	k1gFnPc2	akce
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
uzavíral	uzavírat	k5eAaPmAgMnS	uzavírat
s	s	k7c7	s
Turky	turek	k1gInPc7	turek
příměří	příměří	k1gNnSc2	příměří
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
pomíjením	pomíjení	k1gNnSc7	pomíjení
hrozícího	hrozící	k2eAgNnSc2d1	hrozící
osmanského	osmanský	k2eAgNnSc2d1	osmanské
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
snad	snad	k9	snad
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
katastrofa	katastrofa	k1gFnSc1	katastrofa
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
pozice	pozice	k1gFnSc1	pozice
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
mezinárodně	mezinárodně	k6eAd1	mezinárodně
upevňována	upevňován	k2eAgFnSc1d1	upevňována
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
aktivitám	aktivita	k1gFnPc3	aktivita
proti	proti	k7c3	proti
českým	český	k2eAgMnPc3d1	český
kacířům	kacíř	k1gMnPc3	kacíř
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgMnPc2	který
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
navíc	navíc	k6eAd1	navíc
finančně	finančně	k6eAd1	finančně
profitoval	profitovat	k5eAaBmAgMnS	profitovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc2	jeho
výpravy	výprava	k1gFnSc2	výprava
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
do	do	k7c2	do
Slezska	Slezsko	k1gNnSc2	Slezsko
byly	být	k5eAaImAgFnP	být
většinou	většinou	k6eAd1	většinou
kořistnického	kořistnický	k2eAgInSc2d1	kořistnický
rázu	ráz	k1gInSc2	ráz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1468	[number]	k4	1468
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
proti	proti	k7c3	proti
Jiřímu	Jiří	k1gMnSc3	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
velice	velice	k6eAd1	velice
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Postavil	postavit	k5eAaPmAgMnS	postavit
se	se	k3xPyFc4	se
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
stavovského	stavovský	k2eAgInSc2d1	stavovský
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
velkých	velký	k2eAgInPc2d1	velký
katolických	katolický	k2eAgInPc2d1	katolický
moravských	moravský	k2eAgNnPc2d1	Moravské
královských	královský	k2eAgNnPc2d1	královské
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dobyl	dobýt	k5eAaPmAgInS	dobýt
Třebíč	Třebíč	k1gFnSc4	Třebíč
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1469	[number]	k4	1469
také	také	k9	také
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1469	[number]	k4	1469
se	se	k3xPyFc4	se
také	také	k9	také
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
vpád	vpád	k1gInSc4	vpád
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gFnSc1	jeho
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
překvapena	překvapit	k5eAaPmNgFnS	překvapit
u	u	k7c2	u
Vilémova	Vilémův	k2eAgNnSc2d1	Vilémovo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zasadí	zasadit	k5eAaPmIp3nS	zasadit
o	o	k7c4	o
dohodu	dohoda	k1gFnSc4	dohoda
Jiřího	Jiří	k1gMnSc2	Jiří
a	a	k8xC	a
papežské	papežský	k2eAgFnSc2d1	Papežská
kurie	kurie	k1gFnSc2	kurie
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
tento	tento	k3xDgInSc4	tento
slib	slib	k1gInSc4	slib
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
nereálný	reálný	k2eNgInSc1d1	nereálný
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1469	[number]	k4	1469
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
zvolit	zvolit	k5eAaPmF	zvolit
českými	český	k2eAgInPc7d1	český
a	a	k8xC	a
moravskými	moravský	k2eAgInPc7d1	moravský
katolickými	katolický	k2eAgInPc7d1	katolický
stavy	stav	k1gInPc7	stav
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dobových	dobový	k2eAgInPc2d1	dobový
pramenů	pramen	k1gInPc2	pramen
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
volit	volit	k5eAaImF	volit
až	až	k9	až
po	po	k7c4	po
jejich	jejich	k3xOp3gNnSc4	jejich
naléhání	naléhání	k1gNnSc4	naléhání
a	a	k8xC	a
neúspěšných	úspěšný	k2eNgNnPc6d1	neúspěšné
jednáních	jednání	k1gNnPc6	jednání
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Vladislavem	Vladislav	k1gMnSc7	Vladislav
Jagellonským	jagellonský	k2eAgMnSc7d1	jagellonský
===	===	k?	===
</s>
</p>
<p>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
začal	začít	k5eAaPmAgMnS	začít
Matyáš	Matyáš	k1gMnSc1	Matyáš
jednat	jednat	k5eAaImF	jednat
s	s	k7c7	s
českými	český	k2eAgInPc7d1	český
stavy	stav	k1gInPc7	stav
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
svého	svůj	k3xOyFgInSc2	svůj
nástupu	nástup	k1gInSc2	nástup
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
Německém	německý	k2eAgInSc6d1	německý
Brodě	Brod	k1gInSc6	Brod
(	(	kIx(	(
<g/>
duben	duben	k1gInSc1	duben
1471	[number]	k4	1471
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
umožnit	umožnit	k5eAaPmF	umožnit
uspořádat	uspořádat	k5eAaPmF	uspořádat
volební	volební	k2eAgInSc1d1	volební
sněm	sněm	k1gInSc1	sněm
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
by	by	kYmCp3nS	by
vzešel	vzejít	k5eAaPmAgMnS	vzejít
nový	nový	k2eAgMnSc1d1	nový
český	český	k2eAgMnSc1d1	český
panovník	panovník	k1gMnSc1	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
jednohlasně	jednohlasně	k6eAd1	jednohlasně
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1471	[number]	k4	1471
o	o	k7c6	o
Vladislavovi	Vladislav	k1gMnSc6	Vladislav
Jagellonském	jagellonský	k2eAgMnSc6d1	jagellonský
jako	jako	k8xC	jako
budoucím	budoucí	k2eAgMnSc6d1	budoucí
českém	český	k2eAgMnSc6d1	český
králi	král	k1gMnSc6	král
(	(	kIx(	(
<g/>
přál	přát	k5eAaImAgMnS	přát
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
tak	tak	k6eAd1	tak
ostatně	ostatně	k6eAd1	ostatně
i	i	k9	i
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
protiakci	protiakce	k1gFnSc3	protiakce
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
od	od	k7c2	od
papežského	papežský	k2eAgMnSc2d1	papežský
legáta	legát	k1gMnSc2	legát
znovu	znovu	k6eAd1	znovu
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
stvrdila	stvrdit	k5eAaPmAgFnS	stvrdit
svým	svůj	k3xOyFgMnPc3	svůj
souhlasem	souhlas	k1gInSc7	souhlas
i	i	k9	i
uherská	uherský	k2eAgFnSc1d1	uherská
strana	strana	k1gFnSc1	strana
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1474	[number]	k4	1474
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
plné	plný	k2eAgFnSc3d1	plná
obnově	obnova	k1gFnSc3	obnova
bojů	boj	k1gInPc2	boj
mezi	mezi	k7c7	mezi
Matyášem	Matyáš	k1gMnSc7	Matyáš
a	a	k8xC	a
tentokrát	tentokrát	k6eAd1	tentokrát
Vladislavem	Vladislav	k1gMnSc7	Vladislav
Jagellonským	jagellonský	k2eAgNnPc3d1	jagellonské
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
podporovaným	podporovaný	k2eAgMnSc7d1	podporovaný
svým	svůj	k1gMnSc7	svůj
otcem	otec	k1gMnSc7	otec
Kazimírem	Kazimír	k1gMnSc7	Kazimír
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
trvaly	trvat	k5eAaImAgInP	trvat
prakticky	prakticky	k6eAd1	prakticky
nepřetržitě	přetržitě	k6eNd1	přetržitě
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1479	[number]	k4	1479
<g/>
,	,	kIx,	,
za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
byly	být	k5eAaImAgFnP	být
české	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
politicky	politicky	k6eAd1	politicky
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
ovládal	ovládat	k5eAaImAgMnS	ovládat
Lužici	Lužice	k1gFnSc4	Lužice
<g/>
,	,	kIx,	,
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Moravy	Morava	k1gFnSc2	Morava
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
Matyášovi	Matyáš	k1gMnSc3	Matyáš
zde	zde	k6eAd1	zde
stála	stát	k5eAaImAgFnS	stát
např.	např.	kA	např.
města	město	k1gNnSc2	město
Uničov	Uničov	k1gInSc1	Uničov
a	a	k8xC	a
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
získal	získat	k5eAaPmAgMnS	získat
podporu	podpora	k1gFnSc4	podpora
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
,	,	kIx,	,
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Jindřichova	Jindřichův	k2eAgInSc2d1	Jindřichův
Hradce	Hradec	k1gInSc2	Hradec
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
Čechách	Čechy	k1gFnPc6	Čechy
Švamberků	Švamberka	k1gMnPc2	Švamberka
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ovládal	ovládat	k5eAaImAgInS	ovládat
královské	královský	k2eAgInPc4d1	královský
města	město	k1gNnSc2	město
Plzeň	Plzeň	k1gFnSc1	Plzeň
a	a	k8xC	a
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
Matyášovy	Matyášův	k2eAgMnPc4d1	Matyášův
spojence	spojenec	k1gMnPc4	spojenec
patřil	patřit	k5eAaImAgMnS	patřit
Albrecht	Albrecht	k1gMnSc1	Albrecht
Kostka	Kostka	k1gMnSc1	Kostka
z	z	k7c2	z
Postupic	Postupice	k1gFnPc2	Postupice
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
Šternberkové	Šternberek	k1gMnPc1	Šternberek
a	a	k8xC	a
Zajícové	Zajíc	k1gMnPc1	Zajíc
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1479	[number]	k4	1479
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
urputných	urputný	k2eAgNnPc6d1	urputné
diplomatických	diplomatický	k2eAgNnPc6d1	diplomatické
jednáních	jednání	k1gNnPc6	jednání
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
míru	mír	k1gInSc2	mír
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
znamenal	znamenat	k5eAaImAgInS	znamenat
porážku	porážka	k1gFnSc4	porážka
Vladislava	Vladislav	k1gMnSc2	Vladislav
Jagellonského	jagellonský	k2eAgMnSc2d1	jagellonský
<g/>
.	.	kIx.	.
</s>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
nadále	nadále	k6eAd1	nadále
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
titul	titul	k1gInSc4	titul
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
země	zem	k1gFnPc4	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
uherskou	uherský	k2eAgFnSc4d1	uherská
korunu	koruna	k1gFnSc4	koruna
válčil	válčit	k5eAaImAgMnS	válčit
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Fridrichem	Fridrich	k1gMnSc7	Fridrich
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
nárokoval	nárokovat	k5eAaImAgMnS	nárokovat
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
zesnulém	zesnulý	k1gMnSc6	zesnulý
synovci	synovec	k1gMnSc6	synovec
Ladislavu	Ladislav	k1gMnSc6	Ladislav
Pohrobkovi	pohrobek	k1gMnSc6	pohrobek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1463	[number]	k4	1463
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
mír	mír	k1gInSc4	mír
v	v	k7c6	v
Šoproni	Šoproň	k1gFnSc6	Šoproň
<g/>
.	.	kIx.	.
</s>
<s>
Uhrám	uhrát	k5eAaPmIp1nS	uhrát
měl	mít	k5eAaImAgInS	mít
nadále	nadále	k6eAd1	nadále
vládnout	vládnout	k5eAaImF	vládnout
Matyáš	Matyáš	k1gMnSc1	Matyáš
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
však	však	k9	však
směl	smět	k5eAaImAgMnS	smět
používat	používat	k5eAaImF	používat
titul	titul	k1gInSc4	titul
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toužil	toužit	k5eAaImAgMnS	toužit
po	po	k7c6	po
císařské	císařský	k2eAgFnSc6d1	císařská
koruně	koruna	k1gFnSc6	koruna
a	a	k8xC	a
marně	marně	k6eAd1	marně
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
zvolení	zvolení	k1gNnSc4	zvolení
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dobyl	dobýt	k5eAaPmAgMnS	dobýt
dědičné	dědičný	k2eAgFnSc2d1	dědičná
habsburské	habsburský	k2eAgFnSc2d1	habsburská
země	zem	k1gFnSc2	zem
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1485	[number]	k4	1485
se	se	k3xPyFc4	se
usídlil	usídlit	k5eAaPmAgMnS	usídlit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c6	o
smíření	smíření	k1gNnSc6	smíření
s	s	k7c7	s
Habsburky	Habsburk	k1gMnPc7	Habsburk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
garantovat	garantovat	k5eAaBmF	garantovat
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
jeho	jeho	k3xOp3gMnSc3	jeho
nemanželskému	manželský	k2eNgMnSc3d1	nemanželský
synu	syn	k1gMnSc3	syn
Janovi	Jan	k1gMnSc3	Jan
<g/>
.	.	kIx.	.
<g/>
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1490	[number]	k4	1490
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
na	na	k7c6	na
uherském	uherský	k2eAgInSc6d1	uherský
trůně	trůn	k1gInSc6	trůn
byl	být	k5eAaImAgMnS	být
právě	právě	k9	právě
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
uherskými	uherský	k2eAgInPc7d1	uherský
stavy	stav	k1gInPc7	stav
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nerespektovaly	respektovat	k5eNaImAgInP	respektovat
Matyášovu	Matyášův	k2eAgFnSc4d1	Matyášova
vůli	vůle	k1gFnSc4	vůle
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	s	k7c7	s
králem	král	k1gMnSc7	král
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
nemanželský	manželský	k2eNgMnSc1d1	nemanželský
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
uherských	uherský	k2eAgMnPc2d1	uherský
panovníků	panovník	k1gMnPc2	panovník
se	se	k3xPyFc4	se
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
v	v	k7c6	v
pověstech	pověst	k1gFnPc6	pověst
zachoval	zachovat	k5eAaPmAgMnS	zachovat
jako	jako	k9	jako
dobrý	dobrý	k2eAgMnSc1d1	dobrý
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnSc7d1	další
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
na	na	k7c6	na
dnešní	dnešní	k2eAgFnSc6d1	dnešní
maďarské	maďarský	k2eAgFnSc6d1	maďarská
bankovce	bankovka	k1gFnSc6	bankovka
1000	[number]	k4	1000
forintů	forint	k1gInPc2	forint
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ČECHURA	Čechura	k1gMnSc1	Čechura
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
v	v	k7c6	v
letech	let	k1gInPc6	let
1437	[number]	k4	1437
<g/>
–	–	k?	–
<g/>
1526	[number]	k4	1526
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
Jagellonci	Jagellonek	k1gMnPc1	Jagellonek
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
300	[number]	k4	300
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
493	[number]	k4	493
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
BARTLOVÁ	Bartlová	k1gFnSc1	Bartlová
<g/>
,	,	kIx,	,
Milena	Milena	k1gFnSc1	Milena
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
VI	VI	kA	VI
<g/>
.	.	kIx.	.
1437	[number]	k4	1437
<g/>
-	-	kIx~	-
<g/>
1526	[number]	k4	1526
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
839	[number]	k4	839
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
873	[number]	k4	873
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOENSCH	HOENSCH	kA	HOENSCH
<g/>
,	,	kIx,	,
Jörg	Jörg	k1gInSc4	Jörg
Konrad	Konrada	k1gFnPc2	Konrada
<g/>
.	.	kIx.	.
</s>
<s>
Matthias	Matthias	k1gMnSc1	Matthias
Corvinus	Corvinus	k1gMnSc1	Corvinus
<g/>
.	.	kIx.	.
</s>
<s>
Diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
Feldherr	Feldherr	k1gMnSc1	Feldherr
und	und	k?	und
Mäzen	Mäzen	k1gInSc1	Mäzen
<g/>
.	.	kIx.	.
</s>
<s>
Graz	Graz	k1gMnSc1	Graz
;	;	kIx,	;
Wien	Wien	k1gMnSc1	Wien
;	;	kIx,	;
Köln	Köln	k1gMnSc1	Köln
<g/>
:	:	kIx,	:
Verlag	Verlag	k1gInSc1	Verlag
Styria	Styrium	k1gNnSc2	Styrium
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
222	[number]	k4	222
<g/>
-	-	kIx~	-
<g/>
12640	[number]	k4	12640
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KALOUS	Kalous	k1gMnSc1	Kalous
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
(	(	kIx(	(
<g/>
1443	[number]	k4	1443
<g/>
-	-	kIx~	-
<g/>
1490	[number]	k4	1490
<g/>
)	)	kIx)	)
:	:	kIx,	:
uherský	uherský	k2eAgMnSc1d1	uherský
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Veduta	veduta	k1gFnSc1	veduta
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
512	[number]	k4	512
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86829	[number]	k4	86829
<g/>
-	-	kIx~	-
<g/>
48	[number]	k4	48
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KALOUS	Kalous	k1gMnSc1	Kalous
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Morava	Morava	k1gFnSc1	Morava
a	a	k8xC	a
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
MITÁČEK	MITÁČEK	kA	MITÁČEK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Vládcové	vládce	k1gMnPc1	vládce
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
statí	stať	k1gFnPc2	stať
ze	z	k7c2	z
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
cyklu	cyklus	k1gInSc2	cyklus
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Moravské	moravský	k2eAgNnSc1d1	Moravské
zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7028	[number]	k4	7028
<g/>
-	-	kIx~	-
<g/>
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
99	[number]	k4	99
<g/>
-	-	kIx~	-
<g/>
111	[number]	k4	111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MACEK	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonský	jagellonský	k2eAgInSc1d1	jagellonský
věk	věk	k1gInSc1	věk
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
1471	[number]	k4	1471
<g/>
-	-	kIx~	-
<g/>
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
základna	základna	k1gFnSc1	základna
a	a	k8xC	a
královská	královský	k2eAgFnSc1d1	královská
moc	moc	k1gFnSc1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
342	[number]	k4	342
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
300	[number]	k4	300
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RAZSO	RAZSO	kA	RAZSO
<g/>
,	,	kIx,	,
Gyula	Gyulo	k1gNnSc2	Gyulo
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
feldzuge	feldzuge	k1gInSc1	feldzuge
des	des	k1gNnSc1	des
Konigs	Konigs	k1gInSc1	Konigs
Matthias	Matthias	k1gMnSc1	Matthias
Corvinus	Corvinus	k1gMnSc1	Corvinus
in	in	k?	in
Niederosterreich	Niederosterreich	k1gMnSc1	Niederosterreich
1477	[number]	k4	1477
<g/>
-	-	kIx~	-
<g/>
1490	[number]	k4	1490
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Wien	Wien	k1gNnSc1	Wien
<g/>
:	:	kIx,	:
Osterreichischer	Osterreichischra	k1gFnPc2	Osterreichischra
Bundesverlag	Bundesverlag	k1gInSc1	Bundesverlag
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
36	[number]	k4	36
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
215	[number]	k4	215
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1666	[number]	k4	1666
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
smlouva	smlouva	k1gFnSc1	smlouva
</s>
</p>
<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvína	k1gFnPc2	Korvína
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
Rodokmen	rodokmen	k1gInSc1	rodokmen
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
</s>
</p>
