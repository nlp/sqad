<p>
<s>
Soustava	soustava	k1gFnSc1	soustava
CGS	CGS	kA	CGS
(	(	kIx(	(
<g/>
systém	systém	k1gInSc1	systém
CGS	CGS	kA	CGS
<g/>
,	,	kIx,	,
z	z	k7c2	z
centimetr-gram-sekunda	centimetrramekund	k1gMnSc2	centimetr-gram-sekund
<g/>
,	,	kIx,	,
též	též	k9	též
absolutní	absolutní	k2eAgInSc1d1	absolutní
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
soustava	soustava	k1gFnSc1	soustava
<g/>
)	)	kIx)	)
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
starší	starý	k2eAgInSc4d2	starší
systém	systém	k1gInSc4	systém
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1874	[number]	k4	1874
<g/>
-	-	kIx~	-
<g/>
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
obory	obor	k1gInPc4	obor
existovaly	existovat	k5eAaImAgFnP	existovat
různé	různý	k2eAgFnPc4d1	různá
modifikace	modifikace	k1gFnPc4	modifikace
této	tento	k3xDgFnSc2	tento
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
CGS	CGS	kA	CGS
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
nahrazen	nahrazen	k2eAgInSc4d1	nahrazen
soustavou	soustava	k1gFnSc7	soustava
MKS	MKS	kA	MKS
(	(	kIx(	(
<g/>
metr-kilogram-sekunda	metrilogramekund	k1gMnSc2	metr-kilogram-sekund
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
soustavou	soustava	k1gFnSc7	soustava
SI	si	k1gNnSc4	si
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
;	;	kIx,	;
výhodné	výhodný	k2eAgNnSc1d1	výhodné
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jeho	jeho	k3xOp3gNnSc1	jeho
použití	použití	k1gNnSc1	použití
v	v	k7c6	v
teoretické	teoretický	k2eAgFnSc6d1	teoretická
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Soustava	soustava	k1gFnSc1	soustava
SI	se	k3xPyFc3	se
</s>
</p>
<p>
<s>
Soustava	soustava	k1gFnSc1	soustava
MKS	MKS	kA	MKS
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
CGS	CGS	kA	CGS
systém	systém	k1gInSc1	systém
<g/>
:	:	kIx,	:
http://www.converter.cz/cgs.htm	[url]	k1gInSc1	http://www.converter.cz/cgs.htm
</s>
</p>
<p>
<s>
http://elektrika.cz/data/clanky/volt-watt-franklin-gauss-gilbert-maxwell	[url]	k1gMnSc1	http://elektrika.cz/data/clanky/volt-watt-franklin-gauss-gilbert-maxwell
</s>
</p>
<p>
<s>
130	[number]	k4	130
let	let	k1gInSc4	let
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
Metrické	metrický	k2eAgFnSc2d1	metrická
konvence	konvence	k1gFnSc2	konvence
<g/>
:	:	kIx,	:
https://web.archive.org/web/20061216044014/http://www.cscasfyz.fzu.cz/2006/04/240.html	[url]	k1gInSc1	https://web.archive.org/web/20061216044014/http://www.cscasfyz.fzu.cz/2006/04/240.html
</s>
</p>
