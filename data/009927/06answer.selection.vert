<s>
Soustava	soustava	k1gFnSc1	soustava
CGS	CGS	kA	CGS
(	(	kIx(	(
<g/>
systém	systém	k1gInSc1	systém
CGS	CGS	kA	CGS
<g/>
,	,	kIx,	,
z	z	k7c2	z
centimetr-gram-sekunda	centimetrramekund	k1gMnSc2	centimetr-gram-sekund
<g/>
,	,	kIx,	,
též	též	k9	též
absolutní	absolutní	k2eAgInSc1d1	absolutní
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
soustava	soustava	k1gFnSc1	soustava
<g/>
)	)	kIx)	)
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
starší	starý	k2eAgInSc4d2	starší
systém	systém	k1gInSc4	systém
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1874	[number]	k4	1874
<g/>
-	-	kIx~	-
<g/>
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
