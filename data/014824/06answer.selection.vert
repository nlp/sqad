<s>
Davidova	Davidův	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
je	být	k5eAaImIp3nS
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
v	v	k7c6
okrese	okres	k1gInSc6
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
věnovaná	věnovaný	k2eAgFnSc1d1
odkazu	odkaz	k1gInSc3
zdejšího	zdejší	k2eAgMnSc2d1
rodáka	rodák	k1gMnSc2
<g/>
,	,	kIx,
významného	významný	k2eAgMnSc2d1
vědce	vědec	k1gMnSc2
<g/>
,	,	kIx,
geodeta	geodet	k1gMnSc2
a	a	k8xC
astronoma	astronom	k1gMnSc2
<g/>
,	,	kIx,
Aloise	Alois	k1gMnSc2
Martina	Martin	k1gMnSc2
Davida	David	k1gMnSc2
<g/>
.	.	kIx.
</s>