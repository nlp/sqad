<s>
Davidova	Davidův	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
</s>
<s>
Davidova	Davidův	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
je	být	k5eAaImIp3nS
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
v	v	k7c6
okrese	okres	k1gInSc6
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
věnovaná	věnovaný	k2eAgFnSc1d1
odkazu	odkaz	k1gInSc3
zdejšího	zdejší	k2eAgMnSc2d1
rodáka	rodák	k1gMnSc2
<g/>
,	,	kIx,
významného	významný	k2eAgMnSc2d1
vědce	vědec	k1gMnSc2
<g/>
,	,	kIx,
geodeta	geodet	k1gMnSc2
a	a	k8xC
astronoma	astronom	k1gMnSc2
<g/>
,	,	kIx,
Aloise	Alois	k1gMnSc2
Martina	Martin	k1gMnSc2
Davida	David	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
délka	délka	k1gFnSc1
je	být	k5eAaImIp3nS
33,0	33,0	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Okružní	okružní	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
spojuje	spojovat	k5eAaImIp3nS
Klášter	klášter	k1gInSc1
Teplá	teplý	k2eAgFnSc1d1
<g/>
,	,	kIx,
klášterní	klášterní	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
s	s	k7c7
Davidovým	Davidův	k2eAgInSc7d1
hrobem	hrob	k1gInSc7
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
rodiště	rodiště	k1gNnSc2
Dřevohryzy	Dřevohryza	k1gFnSc2
s	s	k7c7
památníkem	památník	k1gInSc7
<g/>
,	,	kIx,
zříceniny	zřícenina	k1gFnSc2
jeho	jeho	k3xOp3gFnSc2
astronomické	astronomický	k2eAgFnSc2d1
observatoře	observatoř	k1gFnSc2
na	na	k7c6
Branišovském	Branišovský	k2eAgInSc6d1
vrchu	vrch	k1gInSc6
s	s	k7c7
Vidžínem	Vidžín	k1gInSc7
<g/>
,	,	kIx,
kam	kam	k6eAd1
David	David	k1gMnSc1
docházel	docházet	k5eAaImAgMnS
do	do	k7c2
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
