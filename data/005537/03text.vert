<s>
Sergej	Sergej	k1gMnSc1	Sergej
Nikitič	Nikitič	k1gMnSc1	Nikitič
Chruščov	Chruščov	k1gInSc1	Chruščov
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
С	С	k?	С
Н	Н	k?	Н
Х	Х	k?	Х
<g/>
;	;	kIx,	;
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1935	[number]	k4	1935
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
syn	syn	k1gMnSc1	syn
bývalého	bývalý	k2eAgMnSc2d1	bývalý
sovětského	sovětský	k2eAgMnSc2d1	sovětský
vůdce	vůdce	k1gMnSc2	vůdce
Nikity	Nikita	k1gMnSc2	Nikita
Chruščova	Chruščův	k2eAgMnSc2d1	Chruščův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
pracoval	pracovat	k5eAaImAgInS	pracovat
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
vysokých	vysoký	k2eAgInPc6d1	vysoký
vědeckých	vědecký	k2eAgInPc6d1	vědecký
postech	post	k1gInPc6	post
<g/>
,	,	kIx,	,
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
například	například	k6eAd1	například
na	na	k7c6	na
konstrukci	konstrukce	k1gFnSc6	konstrukce
sovětských	sovětský	k2eAgFnPc2d1	sovětská
raket	raketa	k1gFnPc2	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
žije	žít	k5eAaImIp3nS	žít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Valentinou	Valentina	k1gFnSc7	Valentina
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
získal	získat	k5eAaPmAgMnS	získat
americké	americký	k2eAgNnSc4d1	americké
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
přednáší	přednášet	k5eAaImIp3nS	přednášet
na	na	k7c4	na
Watson	Watson	k1gInSc4	Watson
Institute	institut	k1gInSc5	institut
Brown	Brown	k1gMnSc1	Brown
University	universita	k1gFnPc4	universita
v	v	k7c4	v
Providence	providence	k1gFnPc4	providence
na	na	k7c4	na
Rhode	Rhodos	k1gInSc5	Rhodos
Islandu	Island	k1gInSc6	Island
<g/>
.	.	kIx.	.
</s>
<s>
Sergej	Sergej	k1gMnSc1	Sergej
Chruščov	Chruščov	k1gInSc4	Chruščov
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jinými	jiná	k1gFnPc7	jiná
i	i	k8xC	i
prací	práce	k1gFnSc7	práce
o	o	k7c6	o
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
<g/>
.	.	kIx.	.
</s>
<s>
Sergej	Sergej	k1gMnSc1	Sergej
Chruščov	Chruščov	k1gInSc1	Chruščov
<g/>
,	,	kIx,	,
Khrushchev	Khrushchev	k1gFnSc1	Khrushchev
on	on	k3xPp3gMnSc1	on
Khrushchev	Khrushchvo	k1gNnPc2	Khrushchvo
–	–	k?	–
An	An	k1gMnSc5	An
Inside	Insid	k1gMnSc5	Insid
Account	Accounta	k1gFnPc2	Accounta
of	of	k?	of
the	the	k?	the
Man	mana	k1gFnPc2	mana
and	and	k?	and
His	his	k1gNnSc4	his
Era	Era	k1gFnSc2	Era
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
His	his	k1gNnSc1	his
Son	son	k1gInSc4	son
<g/>
,	,	kIx,	,
Sergei	Sergee	k1gFnSc4	Sergee
Khrushchev	Khrushchev	k1gFnSc4	Khrushchev
<g/>
,	,	kIx,	,
Verlag	Verlag	k1gInSc4	Verlag
Little	Little	k1gFnSc2	Little
<g/>
,	,	kIx,	,
Brown	Brown	k1gMnSc1	Brown
and	and	k?	and
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-316-49194-2	[number]	k4	0-316-49194-2
Sergej	Sergej	k1gMnSc1	Sergej
Chruščov	Chruščov	k1gInSc1	Chruščov
<g/>
,	,	kIx,	,
Nikita	Nikit	k2eAgFnSc1d1	Nikita
Khrushchev	Khrushchev	k1gFnSc1	Khrushchev
and	and	k?	and
the	the	k?	the
<g />
.	.	kIx.	.
</s>
<s>
Creation	Creation	k1gInSc1	Creation
of	of	k?	of
a	a	k8xC	a
Superpower	Superpower	k1gInSc1	Superpower
<g/>
,	,	kIx,	,
Pennsylvania	Pennsylvanium	k1gNnPc1	Pennsylvanium
State	status	k1gInSc5	status
University	universita	k1gFnPc1	universita
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-271-01927-1	[number]	k4	0-271-01927-1
Sergej	Sergej	k1gMnSc1	Sergej
Chruščov	Chruščov	k1gInSc1	Chruščov
<g/>
,	,	kIx,	,
Memoirs	Memoirs	k1gInSc1	Memoirs
of	of	k?	of
Nikita	Nikita	k1gFnSc1	Nikita
Khrushchev	Khrushchev	k1gFnSc1	Khrushchev
<g/>
:	:	kIx,	:
Reformer	Reformer	k1gMnSc1	Reformer
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
State	status	k1gInSc5	status
University	universita	k1gFnPc1	universita
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-271-02861-0	[number]	k4	0-271-02861-0
Sergej	Sergej	k1gMnSc1	Sergej
Chruščov	Chruščov	k1gInSc1	Chruščov
<g/>
,	,	kIx,	,
Geburt	Geburt	k1gInSc1	Geburt
einer	einer	k1gMnSc1	einer
Supermacht	Supermacht	k1gMnSc1	Supermacht
<g/>
.	.	kIx.	.
</s>
<s>
Ein	Ein	k?	Ein
Buch	buch	k1gInSc1	buch
über	über	k1gMnSc1	über
meinen	meinna	k1gFnPc2	meinna
Vater	vatra	k1gFnPc2	vatra
<g/>
,	,	kIx,	,
Elbe-Dnjepr-Verlag	Elbe-Dnjepr-Verlaga	k1gFnPc2	Elbe-Dnjepr-Verlaga
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-933395-38-0	[number]	k4	3-933395-38-0
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sergej	Sergej	k1gMnSc1	Sergej
Nikitič	Nikitič	k1gInSc4	Nikitič
Chruščov	Chruščov	k1gInSc4	Chruščov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Sergej	Sergej	k1gMnSc1	Sergej
Nikitič	Nikitič	k1gMnSc1	Nikitič
Chruščov	Chruščov	k1gInSc4	Chruščov
</s>
