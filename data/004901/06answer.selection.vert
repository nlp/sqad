<s>
Počet	počet	k1gInSc1	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
<g/>
)	)	kIx)	)
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
snižoval	snižovat	k5eAaImAgInS	snižovat
a	a	k8xC	a
očekává	očekávat	k5eAaImIp3nS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
bude	být	k5eAaImBp3nS	být
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
přechodnému	přechodný	k2eAgNnSc3d1	přechodné
zvyšování	zvyšování	k1gNnSc3	zvyšování
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
