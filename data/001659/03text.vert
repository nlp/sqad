<s>
Maria	Maria	k1gFnSc1	Maria
Göppert-Mayer	Göppert-Mayra	k1gFnPc2	Göppert-Mayra
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Katovice	Katovice	k1gFnPc1	Katovice
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
Německo	Německo	k1gNnSc1	Německo
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Diego	Diego	k1gMnSc1	Diego
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
fyzička	fyzička	k1gFnSc1	fyzička
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
obdržela	obdržet	k5eAaPmAgFnS	obdržet
jako	jako	k9	jako
druhá	druhý	k4xOgFnSc1	druhý
žena	žena	k1gFnSc1	žena
po	po	k7c6	po
Marii	Maria	k1gFnSc6	Maria
Curie	curie	k1gNnSc2	curie
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
výzkum	výzkum	k1gInSc4	výzkum
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
objevila	objevit	k5eAaPmAgNnP	objevit
magická	magický	k2eAgNnPc1d1	magické
čísla	číslo	k1gNnPc1	číslo
a	a	k8xC	a
podala	podat	k5eAaPmAgNnP	podat
jejich	jejich	k3xOp3gNnSc4	jejich
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
pomocí	pomocí	k7c2	pomocí
slupkového	slupkový	k2eAgInSc2d1	slupkový
modelu	model	k1gInSc2	model
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Katovicích	Katovice	k1gFnPc6	Katovice
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
patřících	patřící	k2eAgFnPc2d1	patřící
Polsku	Polska	k1gFnSc4	Polska
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jediné	jediný	k2eAgNnSc1d1	jediné
dítě	dítě	k1gNnSc1	dítě
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
byly	být	k5eAaImAgFnP	být
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Göttingenu	Göttingen	k1gInSc2	Göttingen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
získal	získat	k5eAaPmAgMnS	získat
profesorské	profesorský	k2eAgNnSc4d1	profesorské
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
tamní	tamní	k2eAgFnSc6d1	tamní
prestižní	prestižní	k2eAgFnSc6d1	prestižní
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
Marii	Maria	k1gFnSc4	Maria
podle	podle	k7c2	podle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
vždy	vždy	k6eAd1	vždy
blízký	blízký	k2eAgInSc1d1	blízký
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
vědec	vědec	k1gMnSc1	vědec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Göttingenu	Göttingen	k1gInSc6	Göttingen
vychodila	vychodit	k5eAaImAgFnS	vychodit
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
na	na	k7c4	na
soukromou	soukromý	k2eAgFnSc4d1	soukromá
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
vedenou	vedený	k2eAgFnSc4d1	vedená
sufražetkami	sufražetka	k1gFnPc7	sufražetka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
připravit	připravit	k5eAaPmF	připravit
dívky	dívka	k1gFnPc4	dívka
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
na	na	k7c6	na
skutečných	skutečný	k2eAgFnPc6d1	skutečná
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
úspěšně	úspěšně	k6eAd1	úspěšně
složila	složit	k5eAaPmAgFnS	složit
maturitu	maturita	k1gFnSc4	maturita
a	a	k8xC	a
následně	následně	k6eAd1	následně
udělal	udělat	k5eAaPmAgMnS	udělat
přijímací	přijímací	k2eAgFnPc4d1	přijímací
zkoušky	zkouška	k1gFnPc4	zkouška
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Göttingenu	Göttingen	k1gInSc6	Göttingen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
studovala	studovat	k5eAaImAgFnS	studovat
matematiku	matematika	k1gFnSc4	matematika
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatku	nedostatek	k1gInSc2	nedostatek
učitelů	učitel	k1gMnPc2	učitel
tohoto	tento	k3xDgInSc2	tento
předmětu	předmět	k1gInSc2	předmět
studovala	studovat	k5eAaImAgFnS	studovat
řada	řada	k1gFnSc1	řada
dívek	dívka	k1gFnPc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
jejích	její	k3xOp3gFnPc2	její
profesorek	profesorka	k1gFnPc2	profesorka
byla	být	k5eAaImAgFnS	být
i	i	k8xC	i
Emmy	Emma	k1gFnSc2	Emma
Noetherová	Noetherová	k1gFnSc1	Noetherová
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ale	ale	k9	ale
začala	začít	k5eAaPmAgFnS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
získala	získat	k5eAaPmAgFnS	získat
doktorský	doktorský	k2eAgInSc4d1	doktorský
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
disertační	disertační	k2eAgFnSc6d1	disertační
práci	práce	k1gFnSc6	práce
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
zásadní	zásadní	k2eAgFnSc4d1	zásadní
myšlenku	myšlenka	k1gFnSc4	myšlenka
možné	možný	k2eAgFnSc2d1	možná
absorpce	absorpce	k1gFnSc2	absorpce
dvou	dva	k4xCgInPc2	dva
fotonů	foton	k1gInPc2	foton
jádrem	jádro	k1gNnSc7	jádro
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
nemožné	možný	k2eNgNnSc1d1	nemožné
tuto	tento	k3xDgFnSc4	tento
myšlenku	myšlenka	k1gFnSc4	myšlenka
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pozdější	pozdní	k2eAgInSc1d2	pozdější
vývoj	vývoj	k1gInSc1	vývoj
laserů	laser	k1gInPc2	laser
dovolil	dovolit	k5eAaPmAgInS	dovolit
tuto	tento	k3xDgFnSc4	tento
hypotézu	hypotéza	k1gFnSc4	hypotéza
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
pozorována	pozorován	k2eAgFnSc1d1	pozorována
dvoufotonová	dvoufotonový	k2eAgFnSc1d1	dvoufotonová
excitace	excitace	k1gFnSc1	excitace
v	v	k7c6	v
krystalu	krystal	k1gInSc6	krystal
europia	europium	k1gNnSc2	europium
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
zásadní	zásadní	k2eAgInSc4d1	zásadní
příspěvek	příspěvek	k1gInSc4	příspěvek
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
jednotka	jednotka	k1gFnSc1	jednotka
průřezu	průřez	k1gInSc2	průřez
dvoufotonové	dvoufotonový	k2eAgFnSc2d1	dvoufotonová
absorpce	absorpce	k1gFnSc2	absorpce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
chemika	chemik	k1gMnSc4	chemik
Josepha	Joseph	k1gMnSc4	Joseph
Mayera	Mayer	k1gMnSc4	Mayer
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
asistentů	asistent	k1gMnPc2	asistent
fyzika	fyzik	k1gMnSc2	fyzik
Jamese	Jamese	k1gFnSc2	Jamese
Francka	Francek	k1gMnSc2	Francek
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Joseph	Joseph	k1gMnSc1	Joseph
získal	získat	k5eAaPmAgMnS	získat
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
chemie	chemie	k1gFnSc2	chemie
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Johnse	Johns	k1gMnSc2	Johns
Hopkinse	Hopkins	k1gMnSc2	Hopkins
<g/>
.	.	kIx.	.
</s>
<s>
Maria	Maria	k1gFnSc1	Maria
profesorské	profesorský	k2eAgNnSc4d1	profesorské
místo	místo	k1gNnSc4	místo
nezískala	získat	k5eNaPmAgFnS	získat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
přijata	přijmout	k5eAaPmNgFnS	přijmout
jako	jako	k9	jako
asistentka	asistentka	k1gFnSc1	asistentka
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
tedy	tedy	k9	tedy
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
vědeckému	vědecký	k2eAgNnSc3d1	vědecké
vybavení	vybavení	k1gNnSc3	vybavení
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
několik	několik	k4yIc4	několik
kurzů	kurz	k1gInPc2	kurz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
vydala	vydat	k5eAaPmAgFnS	vydat
další	další	k2eAgInSc4d1	další
zásadní	zásadní	k2eAgInSc4d1	zásadní
článek	článek	k1gInSc4	článek
o	o	k7c6	o
dvojitém	dvojitý	k2eAgNnSc6d1	dvojité
beta	beta	k1gNnSc6	beta
rozpadu	rozpad	k1gInSc3	rozpad
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
jako	jako	k9	jako
profesorka	profesorka	k1gFnSc1	profesorka
na	na	k7c4	na
Chicagskou	chicagský	k2eAgFnSc4d1	Chicagská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
Maxem	Max	k1gMnSc7	Max
Bornem	Born	k1gMnSc7	Born
na	na	k7c6	na
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
se	se	k3xPyFc4	se
však	však	k9	však
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
dostala	dostat	k5eAaPmAgFnS	dostat
NSDAP	NSDAP	kA	NSDAP
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
akademiků	akademik	k1gMnPc2	akademik
včetně	včetně	k7c2	včetně
Borna	Borno	k1gNnSc2	Borno
a	a	k8xC	a
Francka	Francek	k1gMnSc4	Francek
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Maria	Maria	k1gFnSc1	Maria
se	se	k3xPyFc4	se
tak	tak	k9	tak
definitivně	definitivně	k6eAd1	definitivně
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
přišel	přijít	k5eAaPmAgMnS	přijít
její	její	k3xOp3gMnSc1	její
muž	muž	k1gMnSc1	muž
Joseph	Joseph	k1gMnSc1	Joseph
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
Johnse	Johns	k1gMnSc2	Johns
Hopkinse	Hopkins	k1gMnSc2	Hopkins
z	z	k7c2	z
neznámých	známý	k2eNgInPc2d1	neznámý
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
se	se	k3xPyFc4	se
o	o	k7c6	o
nepřátelském	přátelský	k2eNgInSc6d1	nepřátelský
vztahu	vztah	k1gInSc6	vztah
děkana	děkan	k1gMnSc2	děkan
k	k	k7c3	k
ženám	žena	k1gFnPc3	žena
na	na	k7c6	na
katedře	katedra	k1gFnSc6	katedra
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
také	také	k9	také
o	o	k7c6	o
nepřátelském	přátelský	k2eNgInSc6d1	nepřátelský
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
německým	německý	k2eAgMnPc3d1	německý
vědcům	vědec	k1gMnPc3	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
přijati	přijmout	k5eAaPmNgMnP	přijmout
na	na	k7c4	na
Kolumbijskou	kolumbijský	k2eAgFnSc4d1	kolumbijská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
jako	jako	k8xC	jako
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
opět	opět	k6eAd1	opět
jako	jako	k8xS	jako
asistentka	asistentka	k1gFnSc1	asistentka
<g/>
.	.	kIx.	.
</s>
<s>
Enrico	Enrico	k6eAd1	Enrico
Fermi	Fer	k1gFnPc7	Fer
ji	on	k3xPp3gFnSc4	on
požádal	požádat	k5eAaPmAgInS	požádat
o	o	k7c6	o
prozkoumání	prozkoumání	k1gNnSc6	prozkoumání
valenčních	valenční	k2eAgInPc2d1	valenční
orbitalů	orbital	k1gInPc2	orbital
dosud	dosud	k6eAd1	dosud
neobjevených	objevený	k2eNgInPc2d1	neobjevený
transuranových	transuranový	k2eAgInPc2d1	transuranový
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvoří	tvořit	k5eAaImIp3nP	tvořit
novou	nový	k2eAgFnSc4d1	nová
sérii	série	k1gFnSc4	série
podobnou	podobný	k2eAgFnSc4d1	podobná
vzácným	vzácný	k2eAgFnPc3d1	vzácná
zeminám	zemina	k1gFnPc3	zemina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
získala	získat	k5eAaPmAgFnS	získat
Maria	Maria	k1gFnSc1	Maria
částečný	částečný	k2eAgInSc4d1	částečný
úvazek	úvazek	k1gInSc4	úvazek
na	na	k7c6	na
Sarah	Sarah	k1gFnSc6	Sarah
Lawrence	Lawrenec	k1gMnSc2	Lawrenec
College	Colleg	k1gMnSc2	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
Projekt	projekt	k1gInSc1	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
jehož	jehož	k3xOyRp3gNnPc2	jehož
se	se	k3xPyFc4	se
Maria	Maria	k1gFnSc1	Maria
rovněž	rovněž	k9	rovněž
účastnila	účastnit	k5eAaImAgFnS	účastnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
Haroldem	Harold	k1gMnSc7	Harold
Ureyem	Urey	k1gMnSc7	Urey
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
najít	najít	k5eAaPmF	najít
způsob	způsob	k1gInSc4	způsob
jak	jak	k8xC	jak
oddělit	oddělit	k5eAaPmF	oddělit
potřebný	potřebný	k2eAgInSc4d1	potřebný
uran-	uran-	k?	uran-
<g/>
235	[number]	k4	235
od	od	k7c2	od
přírodního	přírodní	k2eAgInSc2d1	přírodní
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
tvořeného	tvořený	k2eAgInSc2d1	tvořený
převážně	převážně	k6eAd1	převážně
izotopem	izotop	k1gInSc7	izotop
238	[number]	k4	238
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
tedy	tedy	k9	tedy
chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
termodynamické	termodynamický	k2eAgFnPc1d1	termodynamická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
hexafluoridu	hexafluorid	k1gInSc2	hexafluorid
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
separace	separace	k1gFnSc2	separace
fotochemickou	fotochemický	k2eAgFnSc7d1	fotochemická
reakcí	reakce	k1gFnSc7	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
možnost	možnost	k1gFnSc1	možnost
neproveditelná	proveditelný	k2eNgFnSc1d1	neproveditelná
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
laserovými	laserový	k2eAgFnPc7d1	laserová
technologiemi	technologie	k1gFnPc7	technologie
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
<s>
Mariin	Mariin	k2eAgMnSc1d1	Mariin
známý	známý	k2eAgMnSc1d1	známý
Edward	Edward	k1gMnSc1	Edward
Teller	Teller	k1gMnSc1	Teller
jí	on	k3xPp3gFnSc7	on
zajistil	zajistit	k5eAaPmAgMnS	zajistit
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
Kolumbijské	kolumbijský	k2eAgFnSc6d1	kolumbijská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
záření	záření	k1gNnSc2	záření
při	při	k7c6	při
extrémně	extrémně	k6eAd1	extrémně
vysokých	vysoký	k2eAgFnPc6d1	vysoká
teplotách	teplota	k1gFnPc6	teplota
<g/>
,	,	kIx,	,
přítomných	přítomný	k2eAgFnPc6d1	přítomná
například	například	k6eAd1	například
při	při	k7c6	při
termonukleární	termonukleární	k2eAgFnSc6d1	termonukleární
explozi	exploze	k1gFnSc6	exploze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
byl	být	k5eAaImAgMnS	být
její	její	k3xOp3gMnSc1	její
muž	muž	k1gMnSc1	muž
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
Tichomoří	Tichomoří	k1gNnSc2	Tichomoří
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nakrátko	nakrátko	k6eAd1	nakrátko
opustila	opustit	k5eAaPmAgFnS	opustit
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
a	a	k8xC	a
odešla	odejít	k5eAaPmAgFnS	odejít
do	do	k7c2	do
Los	los	k1gInSc1	los
Alamos	Alamos	k1gInSc4	Alamos
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
i	i	k9	i
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
války	válka	k1gFnSc2	válka
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
získali	získat	k5eAaPmAgMnP	získat
Maria	Mario	k1gMnSc4	Mario
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
Joseph	Joseph	k1gInSc4	Joseph
profesorská	profesorský	k2eAgNnPc4d1	profesorské
místa	místo	k1gNnPc4	místo
na	na	k7c6	na
Chicagské	chicagský	k2eAgFnSc6d1	Chicagská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
Maria	Maria	k1gFnSc1	Maria
získala	získat	k5eAaPmAgFnS	získat
rovněž	rovněž	k9	rovněž
částečný	částečný	k2eAgInSc4d1	částečný
úvazek	úvazek	k1gInSc4	úvazek
v	v	k7c6	v
Argonne	Argonn	k1gInSc5	Argonn
National	National	k1gFnSc1	National
Laboratory	Laborator	k1gMnPc4	Laborator
jako	jako	k8xS	jako
vedoucí	vedoucí	k1gMnPc4	vedoucí
teoretické	teoretický	k2eAgFnSc2d1	teoretická
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
například	například	k6eAd1	například
problém	problém	k1gInSc4	problém
kritičnosti	kritičnost	k1gFnSc2	kritičnost
tekutým	tekutý	k2eAgInSc7d1	tekutý
kovem	kov	k1gInSc7	kov
chlazeného	chlazený	k2eAgInSc2d1	chlazený
jaderného	jaderný	k2eAgInSc2d1	jaderný
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdních	pozdní	k2eAgNnPc6d1	pozdní
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
slupkový	slupkový	k2eAgInSc4d1	slupkový
model	model	k1gInSc4	model
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
publikovaný	publikovaný	k2eAgInSc1d1	publikovaný
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
určitý	určitý	k2eAgInSc4d1	určitý
počet	počet	k1gInSc4	počet
nukleonů	nukleon	k1gInPc2	nukleon
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
zvlášť	zvlášť	k6eAd1	zvlášť
vysoké	vysoký	k2eAgFnSc3d1	vysoká
stabilitě	stabilita	k1gFnSc3	stabilita
tohoto	tento	k3xDgNnSc2	tento
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
čísla	číslo	k1gNnPc1	číslo
nazval	nazvat	k5eAaBmAgMnS	nazvat
Eugene	Eugen	k1gInSc5	Eugen
Paul	Paul	k1gMnSc1	Paul
Wigner	Wigner	k1gInSc1	Wigner
jako	jako	k8xS	jako
magická	magický	k2eAgFnSc1d1	magická
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
čísla	číslo	k1gNnPc1	číslo
2	[number]	k4	2
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
,	,	kIx,	,
82	[number]	k4	82
a	a	k8xC	a
126	[number]	k4	126
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
problému	problém	k1gInSc6	problém
pracovali	pracovat	k5eAaImAgMnP	pracovat
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
rovněž	rovněž	k9	rovněž
němečtí	německý	k2eAgMnPc1d1	německý
vědci	vědec	k1gMnPc1	vědec
J.	J.	kA	J.
Hans	Hans	k1gMnSc1	Hans
D.	D.	kA	D.
Jensen	Jensen	k1gInSc1	Jensen
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Haxel	Haxel	k1gMnSc1	Haxel
a	a	k8xC	a
Hans	Hans	k1gMnSc1	Hans
Suess	Suessa	k1gFnPc2	Suessa
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dospěli	dochvít	k5eAaPmAgMnP	dochvít
ke	k	k7c3	k
stejným	stejný	k2eAgInPc3d1	stejný
výsledkům	výsledek	k1gInPc3	výsledek
a	a	k8xC	a
publikovali	publikovat	k5eAaBmAgMnP	publikovat
je	on	k3xPp3gNnPc4	on
již	již	k9	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Nasledně	Nasledně	k6eAd1	Nasledně
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
Maria	Maria	k1gFnSc1	Maria
navázala	navázat	k5eAaPmAgFnS	navázat
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
,	,	kIx,	,
Jensen	Jensen	k2eAgMnSc1d1	Jensen
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
spoluautorem	spoluautor	k1gMnSc7	spoluautor
její	její	k3xOp3gFnSc2	její
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
získali	získat	k5eAaPmAgMnP	získat
Maria	Maria	k1gFnSc1	Maria
Goeppert-Mayer	Goeppert-Mayer	k1gMnSc1	Goeppert-Mayer
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Jensen	Jensen	k1gInSc1	Jensen
a	a	k8xC	a
Eugene	Eugen	k1gInSc5	Eugen
Paul	Paul	k1gMnSc1	Paul
Wigner	Wigner	k1gInSc1	Wigner
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
řádnou	řádný	k2eAgFnSc7d1	řádná
profesorkou	profesorka	k1gFnSc7	profesorka
na	na	k7c6	na
Kalifornské	kalifornský	k2eAgFnSc6d1	kalifornská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
San	San	k1gFnSc6	San
Diegu	Dieg	k1gInSc2	Dieg
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
mrtvici	mrtvice	k1gFnSc4	mrtvice
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
ještě	ještě	k6eAd1	ještě
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
výuce	výuka	k1gFnSc6	výuka
a	a	k8xC	a
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
do	do	k7c2	do
Americké	americký	k2eAgFnSc2d1	americká
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1972	[number]	k4	1972
na	na	k7c4	na
následky	následek	k1gInPc4	následek
infarktu	infarkt	k1gInSc2	infarkt
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
kómatu	kóma	k1gNnSc6	kóma
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgInS	zemřít
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
měli	mít	k5eAaImAgMnP	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Marii	Maria	k1gFnSc4	Maria
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
Petera	Peter	k1gMnSc4	Peter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Maria	Mario	k1gMnSc2	Mario
Goeppert-Mayer	Goeppert-Mayra	k1gFnPc2	Goeppert-Mayra
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Maria	Maria	k1gFnSc1	Maria
Göppert-Mayer	Göppert-Mayer	k1gInSc4	Göppert-Mayer
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
