<s>
Brzy	brzy	k6eAd1	brzy
přišly	přijít	k5eAaPmAgFnP	přijít
8	[number]	k4	8
<g/>
bitové	bitový	k2eAgInPc1d1	bitový
procesory	procesor	k1gInPc1	procesor
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
dá	dát	k5eAaPmIp3nS	dát
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
umí	umět	k5eAaImIp3nS	umět
přímo	přímo	k6eAd1	přímo
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
od	od	k7c2	od
0	[number]	k4	0
do	do	k7c2	do
255	[number]	k4	255
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
16	[number]	k4	16
<g/>
bitový	bitový	k2eAgInSc4d1	bitový
procesor	procesor	k1gInSc4	procesor
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
od	od	k7c2	od
0	[number]	k4	0
do	do	k7c2	do
65535	[number]	k4	65535
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
0	[number]	k4	0
až	až	k9	až
216	[number]	k4	216
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
</s>
