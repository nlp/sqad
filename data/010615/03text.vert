<p>
<s>
Tunika	tunika	k1gFnSc1	tunika
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
tunica	tunic	k1gInSc2	tunic
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
starověká	starověký	k2eAgFnSc1d1	starověká
římská	římský	k2eAgFnSc1d1	římská
košile	košile	k1gFnSc1	košile
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
krátkými	krátký	k2eAgInPc7d1	krátký
rukávy	rukáv	k1gInPc7	rukáv
<g/>
.	.	kIx.	.
</s>
<s>
Nosili	nosit	k5eAaImAgMnP	nosit
ji	on	k3xPp3gFnSc4	on
muži	muž	k1gMnPc1	muž
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
doma	doma	k6eAd1	doma
i	i	k9	i
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
svobodní	svobodný	k2eAgMnPc1d1	svobodný
lidé	člověk	k1gMnPc1	člověk
přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
nosili	nosit	k5eAaImAgMnP	nosit
togu	togu	k6eAd1	togu
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
tunika	tunika	k1gFnSc1	tunika
nosila	nosit	k5eAaImAgFnS	nosit
jako	jako	k8xS	jako
svrchní	svrchní	k2eAgInSc1d1	svrchní
oděv	oděv	k1gInSc1	oděv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Tunika	tunika	k1gFnSc1	tunika
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
dva	dva	k4xCgInPc4	dva
obdélné	obdélný	k2eAgInPc4d1	obdélný
kusy	kus	k1gInPc4	kus
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
lněné	lněný	k2eAgFnPc1d1	lněná
nebo	nebo	k8xC	nebo
vlněné	vlněný	k2eAgFnPc1d1	vlněná
<g/>
,	,	kIx,	,
na	na	k7c6	na
ramenou	rameno	k1gNnPc6	rameno
sepnuté	sepnutý	k2eAgFnSc2d1	sepnutá
sponami	spona	k1gFnPc7	spona
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
však	však	k9	však
oba	dva	k4xCgInPc1	dva
kusy	kus	k1gInPc1	kus
na	na	k7c6	na
ramenou	rameno	k1gNnPc6	rameno
i	i	k8xC	i
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
sešívaly	sešívat	k5eAaImAgFnP	sešívat
a	a	k8xC	a
často	často	k6eAd1	často
měla	mít	k5eAaImAgFnS	mít
tunika	tunika	k1gFnSc1	tunika
i	i	k8xC	i
kratší	krátký	k2eAgInPc1d2	kratší
nebo	nebo	k8xC	nebo
delší	dlouhý	k2eAgInPc1d2	delší
rukávy	rukáv	k1gInPc1	rukáv
<g/>
.	.	kIx.	.
</s>
<s>
Tunika	tunika	k1gFnSc1	tunika
sedláků	sedlák	k1gMnPc2	sedlák
<g/>
,	,	kIx,	,
řemeslníků	řemeslník	k1gMnPc2	řemeslník
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
sahala	sahat	k5eAaImAgFnS	sahat
nad	nad	k7c4	nad
kolena	koleno	k1gNnPc4	koleno
<g/>
,	,	kIx,	,
tuniky	tunika	k1gFnPc4	tunika
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
svobodných	svobodný	k2eAgMnPc2d1	svobodný
občanů	občan	k1gMnPc2	občan
po	po	k7c4	po
kotníky	kotník	k1gInPc4	kotník
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
v	v	k7c6	v
pase	pas	k1gInSc6	pas
převazovaly	převazovat	k5eAaImAgInP	převazovat
provazem	provaz	k1gInSc7	provaz
nebo	nebo	k8xC	nebo
opaskem	opasek	k1gInSc7	opasek
<g/>
.	.	kIx.	.
</s>
<s>
Bohatší	bohatý	k2eAgMnPc1d2	bohatší
lidé	člověk	k1gMnPc1	člověk
nosili	nosit	k5eAaImAgMnP	nosit
tuniky	tunika	k1gFnPc4	tunika
barevné	barevný	k2eAgFnSc2d1	barevná
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
různě	různě	k6eAd1	různě
ozdobené	ozdobený	k2eAgNnSc4d1	ozdobené
–	–	k?	–
například	například	k6eAd1	například
římští	římský	k2eAgMnPc1d1	římský
senátoři	senátor	k1gMnPc1	senátor
měli	mít	k5eAaImAgMnP	mít
vpředu	vpředu	k6eAd1	vpředu
na	na	k7c6	na
tunice	tunika	k1gFnSc6	tunika
svislý	svislý	k2eAgInSc1d1	svislý
purpurový	purpurový	k2eAgInSc1d1	purpurový
pruh	pruh	k1gInSc1	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
chladnějších	chladný	k2eAgInPc6d2	chladnější
krajích	kraj	k1gInPc6	kraj
se	se	k3xPyFc4	se
nosila	nosit	k5eAaImAgFnS	nosit
tunika	tunika	k1gFnSc1	tunika
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
rukávy	rukáv	k1gInPc7	rukáv
<g/>
,	,	kIx,	,
římští	římský	k2eAgMnPc1d1	římský
legionáři	legionář	k1gMnPc1	legionář
někdy	někdy	k6eAd1	někdy
nosili	nosit	k5eAaImAgMnP	nosit
přes	přes	k7c4	přes
spodní	spodní	k2eAgFnSc4d1	spodní
lněnou	lněný	k2eAgFnSc4d1	lněná
tuniku	tunika	k1gFnSc4	tunika
ještě	ještě	k6eAd1	ještě
svrchní	svrchní	k2eAgFnSc4d1	svrchní
vlněnou	vlněný	k2eAgFnSc4d1	vlněná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Církevní	církevní	k2eAgInSc1d1	církevní
oděv	oděv	k1gInSc1	oděv
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
tunika	tunika	k1gFnSc1	tunika
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
oděv	oděv	k1gInSc1	oděv
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
svrchní	svrchní	k2eAgInSc1d1	svrchní
řádový	řádový	k2eAgInSc1d1	řádový
oděv	oděv	k1gInSc1	oděv
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
součásti	součást	k1gFnPc1	součást
liturgického	liturgický	k2eAgNnSc2d1	liturgické
oblečení	oblečení	k1gNnSc2	oblečení
ap.	ap.	kA	ap.
Tunica	Tunic	k2eAgFnSc1d1	Tunica
alba	alba	k1gFnSc1	alba
(	(	kIx(	(
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
tunika	tunika	k1gFnSc1	tunika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc1d1	základní
spodní	spodní	k2eAgInSc1d1	spodní
oděv	oděv	k1gInSc1	oděv
kněze	kněz	k1gMnSc2	kněz
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
bílá	bílý	k2eAgFnSc1d1	bílá
košile	košile	k1gFnSc1	košile
<g/>
.	.	kIx.	.
</s>
<s>
Dalmatika	dalmatika	k1gFnSc1	dalmatika
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
kratší	krátký	k2eAgFnSc2d2	kratší
tuniky	tunika	k1gFnSc2	tunika
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
zdobená	zdobený	k2eAgFnSc1d1	zdobená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Tunika	tunika	k1gFnSc1	tunika
se	se	k3xPyFc4	se
podobala	podobat	k5eAaImAgFnS	podobat
řeckému	řecký	k2eAgInSc3d1	řecký
chitónu	chitón	k1gInSc3	chitón
a	a	k8xC	a
pocházela	pocházet	k5eAaImAgFnS	pocházet
snad	snad	k9	snad
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
tuniky	tunika	k1gFnPc1	tunika
staly	stát	k5eAaPmAgFnP	stát
svrchním	svrchní	k2eAgInSc7d1	svrchní
oděvem	oděv	k1gInSc7	oděv
<g/>
,	,	kIx,	,
bývaly	bývat	k5eAaImAgInP	bývat
vlněné	vlněný	k2eAgInPc1d1	vlněný
nebo	nebo	k8xC	nebo
lněné	lněný	k2eAgInPc1d1	lněný
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
pestře	pestro	k6eAd1	pestro
barevné	barevný	k2eAgFnPc1d1	barevná
a	a	k8xC	a
vyšívané	vyšívaný	k2eAgFnPc1d1	vyšívaná
<g/>
.	.	kIx.	.
</s>
<s>
Bohatí	bohatý	k2eAgMnPc1d1	bohatý
lidé	člověk	k1gMnPc1	člověk
mívali	mívat	k5eAaImAgMnP	mívat
i	i	k9	i
hedvábné	hedvábný	k2eAgFnPc4d1	hedvábná
tuniky	tunika	k1gFnPc4	tunika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tunika	tunika	k1gFnSc1	tunika
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
tuniky	tunika	k1gFnPc1	tunika
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
dámských	dámský	k2eAgInPc6d1	dámský
šatnících	šatník	k1gInPc6	šatník
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
rozvolněné	rozvolněný	k2eAgFnPc1d1	rozvolněná
halenky	halenka	k1gFnPc1	halenka
různých	různý	k2eAgInPc2d1	různý
střihů	střih	k1gInPc2	střih
a	a	k8xC	a
barev	barva	k1gFnPc2	barva
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
délka	délka	k1gFnSc1	délka
pod	pod	k7c4	pod
zadek	zadek	k1gInSc4	zadek
nebo	nebo	k8xC	nebo
až	až	k9	až
po	po	k7c4	po
kolena	koleno	k1gNnPc4	koleno
opticky	opticky	k6eAd1	opticky
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
postavu	postava	k1gFnSc4	postava
a	a	k8xC	a
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
její	její	k3xOp3gInPc4	její
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
tuniky	tunika	k1gFnPc1	tunika
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
<g/>
,	,	kIx,	,
tříčtvrtečními	tříčtvrteční	k2eAgInPc7d1	tříčtvrteční
nebo	nebo	k8xC	nebo
krátkými	krátký	k2eAgInPc7d1	krátký
rukávy	rukáv	k1gInPc7	rukáv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
i	i	k9	i
tuniky	tunika	k1gFnPc4	tunika
bez	bez	k7c2	bez
rukávů	rukáv	k1gInPc2	rukáv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
antiky	antika	k1gFnSc2	antika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Římané	Říman	k1gMnPc1	Říman
–	–	k?	–
soukromý	soukromý	k2eAgInSc1d1	soukromý
život	život	k1gInSc1	život
–	–	k?	–
kroj	kroj	k1gInSc1	kroj
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
21	[number]	k4	21
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
790	[number]	k4	790
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
odívání	odívání	k1gNnSc2	odívání
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
</s>
</p>
<p>
<s>
Starověký	starověký	k2eAgInSc1d1	starověký
Řím	Řím	k1gInSc1	Řím
</s>
</p>
<p>
<s>
Toga	Togo	k1gNnPc1	Togo
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tunika	tunika	k1gFnSc1	tunika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Clothing	Clothing	k1gInSc1	Clothing
for	forum	k1gNnPc2	forum
Men	Men	k1gFnSc2	Men
in	in	k?	in
Ancient	Ancient	k1gMnSc1	Ancient
Rome	Rom	k1gMnSc5	Rom
</s>
</p>
<p>
<s>
William	William	k1gInSc1	William
Smith	Smith	k1gInSc1	Smith
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
A	a	k9	a
Dictionary	Dictionara	k1gFnSc2	Dictionara
of	of	k?	of
Greek	Greek	k1gMnSc1	Greek
and	and	k?	and
Roman	Roman	k1gMnSc1	Roman
Antiquities	Antiquities	k1gMnSc1	Antiquities
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oblečení	oblečení	k1gNnSc1	oblečení
a	a	k8xC	a
výzbroj	výzbroj	k1gFnSc1	výzbroj
římského	římský	k2eAgMnSc2d1	římský
legionáře	legionář	k1gMnSc2	legionář
</s>
</p>
