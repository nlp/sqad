<s>
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc2
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc2
Zkratka	zkratka	k1gFnSc1
</s>
<s>
DSP	DSP	kA
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1991	#num#	k4
Datum	datum	k1gInSc1
rozpuštění	rozpuštění	k1gNnSc2
</s>
<s>
1993	#num#	k4
Předseda	předseda	k1gMnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Wurm	Wurm	k1gMnSc1
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
KSČM	KSČM	kA
Nástupce	nástupce	k1gMnSc1
</s>
<s>
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
SDL	SDL	kA
Politická	politický	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
</s>
<s>
levice	levice	k1gFnSc1
<g/>
,	,	kIx,
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc2
(	(	kIx(
<g/>
DSP	DSP	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
malá	malý	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
levicová	levicový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
působící	působící	k2eAgMnPc4d1
v	v	k7c6
letech	léto	k1gNnPc6
1991	#num#	k4
<g/>
-	-	kIx~
<g/>
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stranu	strana	k1gFnSc4
založili	založit	k5eAaPmAgMnP
především	především	k9
bývalí	bývalý	k2eAgMnPc1d1
členové	člen	k1gMnPc1
KSČM	KSČM	kA
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
stavěli	stavět	k5eAaImAgMnP
za	za	k7c4
přejmenování	přejmenování	k1gNnSc4
strany	strana	k1gFnSc2
a	a	k8xC
reformu	reforma	k1gFnSc4
programu	program	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
někteří	některý	k3yIgMnPc1
členové	člen	k1gMnPc1
strany	strana	k1gFnSc2
byli	být	k5eAaImAgMnP
zvolení	zvolený	k2eAgMnPc1d1
za	za	k7c4
jiná	jiný	k2eAgNnPc4d1
levicová	levicový	k2eAgNnPc4d1
uskupení	uskupení	k1gNnPc4
<g/>
,	,	kIx,
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
stranu	strana	k1gFnSc4
parlamentní	parlamentní	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
prvním	první	k4xOgInSc6
sjezdu	sjezd	k1gInSc6
strany	strana	k1gFnSc2
byl	být	k5eAaImAgInS
předsedou	předseda	k1gMnSc7
zvolen	zvolen	k2eAgMnSc1d1
Petr	Petr	k1gMnSc1
Wurm	Wurm	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
předtím	předtím	k6eAd1
nebyl	být	k5eNaImAgMnS
v	v	k7c6
komunistické	komunistický	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
budoucna	budoucno	k1gNnSc2
se	se	k3xPyFc4
DSP	DSP	kA
mínila	mínit	k5eAaImAgFnS
stát	stát	k5eAaPmF,k5eAaImF
stranou	strana	k1gFnSc7
sociálně	sociálně	k6eAd1
demokratického	demokratický	k2eAgInSc2d1
typu	typ	k1gInSc2
a	a	k8xC
sjednotit	sjednotit	k5eAaPmF
levicové	levicový	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
(	(	kIx(
<g/>
do	do	k7c2
DSP	DSP	kA
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
začlenilo	začlenit	k5eAaPmAgNnS
Československé	československý	k2eAgNnSc1d1
demokratické	demokratický	k2eAgNnSc1d1
fórum	fórum	k1gNnSc1
a	a	k8xC
Nezávislá	závislý	k2eNgFnSc1d1
levice	levice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voleb	volba	k1gFnPc2
roku	rok	k1gInSc2
1992	#num#	k4
se	se	k3xPyFc4
strana	strana	k1gFnSc1
nezúčastnila	zúčastnit	k5eNaPmAgFnS
<g/>
,	,	kIx,
své	svůj	k3xOyFgMnPc4
členy	člen	k1gMnPc4
měla	mít	k5eAaImAgFnS
na	na	k7c6
kandidátkách	kandidátka	k1gFnPc6
moravistů	moravista	k1gMnPc2
(	(	kIx(
<g/>
HSD-SMS	HSD-SMS	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
levicových	levicový	k2eAgMnPc2d1
liberálů	liberál	k1gMnPc2
(	(	kIx(
<g/>
Liberálně	liberálně	k6eAd1
sociální	sociální	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
)	)	kIx)
z	z	k7c2
nichž	jenž	k3xRgInPc2
tři	tři	k4xCgInPc1
uspěli	uspět	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1993	#num#	k4
strana	strana	k1gFnSc1
zanikla	zaniknout	k5eAaPmAgFnS
–	–	k?
část	část	k1gFnSc1
strany	strana	k1gFnSc2
odešla	odejít	k5eAaPmAgFnS
po	po	k7c6
zvolení	zvolení	k1gNnSc6
Zemana	Zeman	k1gMnSc2
předsedou	předseda	k1gMnSc7
ČSSD	ČSSD	kA
do	do	k7c2
jeho	jeho	k3xOp3gFnPc2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
do	do	k7c2
Strany	strana	k1gFnSc2
demokratické	demokratický	k2eAgFnSc2d1
levice	levice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
