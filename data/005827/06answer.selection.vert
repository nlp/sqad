<s>
Roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
vydal	vydat	k5eAaPmAgMnS	vydat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
knihu	kniha	k1gFnSc4	kniha
Protokoly	protokol	k1gInPc1	protokol
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
vedle	vedle	k7c2	vedle
her	hra	k1gFnPc2	hra
Zahradní	zahradní	k2eAgFnSc4d1	zahradní
slavnost	slavnost	k1gFnSc4	slavnost
a	a	k8xC	a
Vyrozumění	vyrozumění	k1gNnSc4	vyrozumění
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
sbírku	sbírka	k1gFnSc4	sbírka
typogramů	typogram	k1gInPc2	typogram
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
eseje	esej	k1gInPc4	esej
<g/>
.	.	kIx.	.
</s>
