<s>
Praděd	praděd	k1gMnSc1	praděd
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Altvater	Altvater	k1gInSc1	Altvater
<g/>
;	;	kIx,	;
polsky	polsky	k6eAd1	polsky
Pradziad	Pradziad	k1gInSc1	Pradziad
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
1491,3	[number]	k4	1491,3
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
Hrubém	hrubý	k2eAgInSc6d1	hrubý
Jeseníku	Jeseník	k1gInSc6	Jeseník
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrchol	vrchol	k1gInSc1	vrchol
tohoto	tento	k3xDgNnSc2	tento
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
Českého	český	k2eAgNnSc2d1	české
Slezska	Slezsko	k1gNnSc2	Slezsko
i	i	k8xC	i
Horního	horní	k2eAgNnSc2d1	horní
Slezska	Slezsko	k1gNnSc2	Slezsko
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>

