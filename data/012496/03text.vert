<p>
<s>
Stratosféra	stratosféra	k1gFnSc1	stratosféra
je	být	k5eAaImIp3nS	být
vrstva	vrstva	k1gFnSc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
11	[number]	k4	11
až	až	k8xS	až
50	[number]	k4	50
km	km	kA	km
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Stratosféra	stratosféra	k1gFnSc1	stratosféra
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
další	další	k2eAgFnSc2d1	další
vrstvy	vrstva	k1gFnSc2	vrstva
oddělena	oddělen	k2eAgFnSc1d1	oddělena
stratopauzou	stratopauza	k1gFnSc7	stratopauza
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
30	[number]	k4	30
km	km	kA	km
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
stálá	stálý	k2eAgFnSc1d1	stálá
teplota	teplota	k1gFnSc1	teplota
od	od	k7c2	od
−	−	k?	−
do	do	k7c2	do
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
šířky	šířka	k1gFnSc2	šířka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
vrstvě	vrstva	k1gFnSc6	vrstva
stratosféry	stratosféra	k1gFnSc2	stratosféra
teplota	teplota	k1gFnSc1	teplota
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
stoupá	stoupat	k5eAaImIp3nS	stoupat
až	až	k9	až
na	na	k7c4	na
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
stratosféry	stratosféra	k1gFnSc2	stratosféra
mezi	mezi	k7c7	mezi
25	[number]	k4	25
až	až	k9	až
35	[number]	k4	35
km	km	kA	km
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ozonová	ozonový	k2eAgFnSc1d1	ozonová
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
relativně	relativně	k6eAd1	relativně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
koncentraci	koncentrace	k1gFnSc4	koncentrace
ozonu	ozon	k1gInSc2	ozon
(	(	kIx(	(
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Molekuly	molekula	k1gFnPc1	molekula
ozónu	ozón	k1gInSc2	ozón
pohlcují	pohlcovat	k5eAaImIp3nP	pohlcovat
krátkovlnné	krátkovlnný	k2eAgFnPc1d1	krátkovlnná
<g/>
,	,	kIx,	,
především	především	k9	především
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
zhoubný	zhoubný	k2eAgInSc1d1	zhoubný
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
tkáně	tkáň	k1gFnPc4	tkáň
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ozónové	ozónový	k2eAgFnSc3d1	ozónová
vrstvě	vrstva	k1gFnSc3	vrstva
se	se	k3xPyFc4	se
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
Země	zem	k1gFnSc2	zem
dostává	dostávat	k5eAaImIp3nS	dostávat
jen	jen	k9	jen
asi	asi	k9	asi
1	[number]	k4	1
%	%	kIx~	%
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
přicházejícího	přicházející	k2eAgNnSc2d1	přicházející
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Ozónová	ozónový	k2eAgFnSc1d1	ozónová
vrstva	vrstva	k1gFnSc1	vrstva
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
vysvětlujeme	vysvětlovat	k5eAaImIp1nP	vysvětlovat
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
teplotu	teplota	k1gFnSc4	teplota
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
vrstvě	vrstva	k1gFnSc6	vrstva
stratosféry	stratosféra	k1gFnSc2	stratosféra
<g/>
.	.	kIx.	.
</s>
<s>
Ozón	ozón	k1gInSc1	ozón
vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotony	foton	k1gInPc1	foton
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
rozbijí	rozbít	k5eAaPmIp3nP	rozbít
běžné	běžný	k2eAgFnPc1d1	běžná
dvouatomové	dvouatomový	k2eAgFnPc1d1	dvouatomová
molekuly	molekula	k1gFnPc1	molekula
kyslíku	kyslík	k1gInSc2	kyslík
(	(	kIx(	(
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
kyslíkové	kyslíkový	k2eAgInPc1d1	kyslíkový
radikály	radikál	k1gInPc1	radikál
O	o	k7c6	o
<g/>
•	•	k?	•
se	se	k3xPyFc4	se
slučují	slučovat	k5eAaImIp3nP	slučovat
s	s	k7c7	s
molekulami	molekula	k1gFnPc7	molekula
O2	O2	k1gFnPc2	O2
na	na	k7c4	na
molekuly	molekula	k1gFnPc4	molekula
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lze	lze	k6eAd1	lze
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
pozorovat	pozorovat	k5eAaImF	pozorovat
perleťová	perleťový	k2eAgNnPc1d1	perleťové
oblaka	oblaka	k1gNnPc1	oblaka
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
zde	zde	k6eAd1	zde
vzdušné	vzdušný	k2eAgInPc1d1	vzdušný
proudy	proud	k1gInPc1	proud
o	o	k7c6	o
rychlosti	rychlost	k1gFnSc6	rychlost
až	až	k6eAd1	až
několika	několik	k4yIc2	několik
set	sto	k4xCgNnPc2	sto
kilometrů	kilometr	k1gInPc2	kilometr
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
téměř	téměř	k6eAd1	téměř
chybí	chybit	k5eAaPmIp3nS	chybit
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
a	a	k8xC	a
prach	prach	k1gInSc1	prach
pozemského	pozemský	k2eAgInSc2d1	pozemský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
případě	případ	k1gInSc6	případ
energetických	energetický	k2eAgFnPc2d1	energetická
sopečných	sopečný	k2eAgFnPc2d1	sopečná
erupcí	erupce	k1gFnPc2	erupce
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
pyroklastické	pyroklastický	k2eAgNnSc1d1	pyroklastický
mračno	mračno	k1gNnSc1	mračno
stoupající	stoupající	k2eAgFnSc2d1	stoupající
desítky	desítka	k1gFnSc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
nad	nad	k7c4	nad
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
dosahujíc	dosahovat	k5eAaImSgNnS	dosahovat
až	až	k9	až
do	do	k7c2	do
stratosféry	stratosféra	k1gFnSc2	stratosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
stratosféry	stratosféra	k1gFnSc2	stratosféra
dodáno	dodat	k5eAaPmNgNnS	dodat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
,	,	kIx,	,
sopečného	sopečný	k2eAgInSc2d1	sopečný
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
sulfan	sulfan	k1gMnSc1	sulfan
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vlivem	k7c2	vlivem
silných	silný	k2eAgInPc2d1	silný
větrů	vítr	k1gInPc2	vítr
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
materiál	materiál	k1gInSc1	materiál
rychle	rychle	k6eAd1	rychle
rozprostřen	rozprostřít	k5eAaPmNgInS	rozprostřít
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
erupce	erupce	k1gFnSc2	erupce
<g/>
,	,	kIx,	,
bodového	bodový	k2eAgInSc2d1	bodový
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
globálně	globálně	k6eAd1	globálně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
tomu	ten	k3xDgMnSc3	ten
stalo	stát	k5eAaPmAgNnS	stát
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
při	při	k7c6	při
erupci	erupce	k1gFnSc6	erupce
sopky	sopka	k1gFnSc2	sopka
El	Ela	k1gFnPc2	Ela
Chichón	Chichón	k1gInSc1	Chichón
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
při	při	k7c6	při
erupci	erupce	k1gFnSc6	erupce
sopky	sopka	k1gFnSc2	sopka
Pinatubo	Pinatuba	k1gMnSc5	Pinatuba
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
stačilo	stačit	k5eAaBmAgNnS	stačit
22	[number]	k4	22
dní	den	k1gInPc2	den
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyvržený	vyvržený	k2eAgInSc1d1	vyvržený
sopečný	sopečný	k2eAgInSc1d1	sopečný
materiál	materiál	k1gInSc1	materiál
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
materiál	materiál	k1gInSc1	materiál
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
významný	významný	k2eAgInSc4d1	významný
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
klima	klima	k1gNnSc4	klima
v	v	k7c6	v
lokálním	lokální	k2eAgNnSc6d1	lokální
i	i	k8xC	i
globálním	globální	k2eAgNnSc6d1	globální
měřítku	měřítko	k1gNnSc6	měřítko
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
jak	jak	k6eAd1	jak
ochlazení	ochlazení	k1gNnSc3	ochlazení
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
oteplení	oteplení	k1gNnSc4	oteplení
Země	zem	k1gFnSc2	zem
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
částic	částice	k1gFnPc2	částice
aerosolu	aerosol	k1gInSc2	aerosol
a	a	k8xC	a
na	na	k7c6	na
konkrétním	konkrétní	k2eAgNnSc6d1	konkrétní
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Stratosféra	stratosféra	k1gFnSc1	stratosféra
se	se	k3xPyFc4	se
otepluje	oteplovat	k5eAaImIp3nS	oteplovat
a	a	k8xC	a
troposféra	troposféra	k1gFnSc1	troposféra
se	se	k3xPyFc4	se
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
<g/>
.	.	kIx.	.
<g/>
Zatímco	zatímco	k8xS	zatímco
sopečný	sopečný	k2eAgInSc1d1	sopečný
prach	prach	k1gInSc1	prach
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
až	až	k8xS	až
týdnech	týden	k1gInPc6	týden
odstraněn	odstraněn	k2eAgInSc1d1	odstraněn
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
a	a	k8xC	a
sulfan	sulfan	k1gInSc1	sulfan
se	se	k3xPyFc4	se
zapojuje	zapojovat	k5eAaImIp3nS	zapojovat
do	do	k7c2	do
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
a	a	k8xC	a
za	za	k7c2	za
spoluúčasti	spoluúčast	k1gFnSc2	spoluúčast
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
se	se	k3xPyFc4	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
μ	μ	k?	μ
drobné	drobný	k2eAgFnSc2d1	drobná
kapičky	kapička	k1gFnSc2	kapička
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
,	,	kIx,	,
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
aerosol	aerosol	k1gInSc4	aerosol
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
drobné	drobný	k2eAgFnPc1d1	drobná
částice	částice	k1gFnPc1	částice
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnPc1d1	sírová
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
přetrvávat	přetrvávat	k5eAaImF	přetrvávat
po	po	k7c4	po
mnohem	mnohem	k6eAd1	mnohem
delší	dlouhý	k2eAgInSc4d2	delší
čas	čas	k1gInSc4	čas
než	než	k8xS	než
prachové	prachový	k2eAgFnSc2d1	prachová
částice	částice	k1gFnSc2	částice
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
oproti	oproti	k7c3	oproti
prachu	prach	k1gInSc3	prach
mají	mít	k5eAaImIp3nP	mít
až	až	k9	až
desetkrát	desetkrát	k6eAd1	desetkrát
větší	veliký	k2eAgFnSc4d2	veliký
schopnost	schopnost	k1gFnSc4	schopnost
blokovat	blokovat	k5eAaImF	blokovat
dopadající	dopadající	k2eAgNnSc4d1	dopadající
sluneční	sluneční	k2eAgNnSc4d1	sluneční
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
aerosolu	aerosol	k1gInSc2	aerosol
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
tak	tak	k8xC	tak
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
odrazivost	odrazivost	k1gFnSc4	odrazivost
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
albedo	albedo	k1gNnSc1	albedo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
od	od	k7c2	od
částeček	částečka	k1gFnPc2	částečka
odráží	odrážet	k5eAaImIp3nS	odrážet
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
světelného	světelný	k2eAgNnSc2d1	světelné
záření	záření	k1gNnSc2	záření
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
kosmu	kosmos	k1gInSc2	kosmos
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
zemský	zemský	k2eAgMnSc1d1	zemský
povrch	povrch	k6eAd1wR	povrch
nedostává	dostávat	k5eNaImIp3nS	dostávat
obvyklý	obvyklý	k2eAgInSc4d1	obvyklý
přísun	přísun	k1gInSc4	přísun
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
erupce	erupce	k1gFnSc1	erupce
sopky	sopka	k1gFnSc2	sopka
Pinatubo	Pinatuba	k1gFnSc5	Pinatuba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
tak	tak	k6eAd1	tak
například	například	k6eAd1	například
způsobila	způsobit	k5eAaPmAgFnS	způsobit
celosvětový	celosvětový	k2eAgInSc4d1	celosvětový
pokles	pokles	k1gInSc4	pokles
průměrné	průměrný	k2eAgFnSc2d1	průměrná
teploty	teplota	k1gFnSc2	teplota
o	o	k7c4	o
0,5	[number]	k4	0,5
°	°	k?	°
<g/>
C.	C.	kA	C.
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
aerosolem	aerosol	k1gInSc7	aerosol
zachycena	zachycen	k2eAgFnSc1d1	zachycena
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zahřátí	zahřátí	k1gNnSc3	zahřátí
těchto	tento	k3xDgFnPc2	tento
kapiček	kapička	k1gFnPc2	kapička
a	a	k8xC	a
významnému	významný	k2eAgInSc3d1	významný
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
stratosféry	stratosféra	k1gFnSc2	stratosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Země	zem	k1gFnSc2	zem
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
stratosféra	stratosféra	k1gFnSc1	stratosféra
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
