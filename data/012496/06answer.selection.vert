<s>
Stratosféra	stratosféra	k1gFnSc1	stratosféra
je	být	k5eAaImIp3nS	být
vrstva	vrstva	k1gFnSc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
11	[number]	k4	11
až	až	k8xS	až
50	[number]	k4	50
km	km	kA	km
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
