<p>
<s>
Ozobí	ozobí	k1gNnSc1	ozobí
(	(	kIx(	(
<g/>
Cera	Cera	k1gFnSc1	Cera
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
měkká	měkký	k2eAgFnSc1d1	měkká
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
neopeřená	opeřený	k2eNgFnSc1d1	neopeřená
část	část	k1gFnSc1	část
ptačího	ptačí	k2eAgNnSc2d1	ptačí
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
kůží	kůže	k1gFnSc7	kůže
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nad	nad	k7c7	nad
zobákem	zobák	k1gInSc7	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
umístěny	umístit	k5eAaPmNgInP	umístit
nosní	nosní	k2eAgInPc1d1	nosní
otvory	otvor	k1gInPc1	otvor
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
sov	sova	k1gFnPc2	sova
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
nosní	nosní	k2eAgInPc4d1	nosní
otvory	otvor	k1gInPc4	otvor
mimo	mimo	k7c4	mimo
ozobí	ozobí	k1gNnSc4	ozobí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ozobí	ozobí	k1gNnSc1	ozobí
bývá	bývat	k5eAaImIp3nS	bývat
u	u	k7c2	u
samce	samec	k1gInSc2	samec
a	a	k8xC	a
samice	samice	k1gFnSc2	samice
různobarevné	různobarevný	k2eAgFnSc2d1	různobarevná
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
barevná	barevný	k2eAgFnSc1d1	barevná
různorodost	různorodost	k1gFnSc1	různorodost
ozobí	ozobí	k1gNnSc2	ozobí
u	u	k7c2	u
andulek	andulka	k1gFnPc2	andulka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
samec	samec	k1gMnSc1	samec
ozobí	ozobí	k1gNnSc2	ozobí
modré	modrý	k2eAgFnSc2d1	modrá
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
růžové	růžový	k2eAgNnSc4d1	růžové
až	až	k6eAd1	až
hnědé	hnědý	k2eAgNnSc4d1	hnědé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ozobí	ozobí	k1gNnSc1	ozobí
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
například	například	k6eAd1	například
u	u	k7c2	u
holubů	holub	k1gMnPc2	holub
<g/>
,	,	kIx,	,
sov	sova	k1gFnPc2	sova
<g/>
,	,	kIx,	,
papoušků	papoušek	k1gMnPc2	papoušek
a	a	k8xC	a
krocanů	krocan	k1gMnPc2	krocan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Beak	Beaka	k1gFnPc2	Beaka
<g/>
#	#	kIx~	#
<g/>
Cere	cer	k1gInSc5	cer
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ozobí	ozobí	k1gNnSc2	ozobí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
