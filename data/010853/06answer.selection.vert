<s>
Ozobí	ozobí	k1gNnSc1	ozobí
bývá	bývat	k5eAaImIp3nS	bývat
u	u	k7c2	u
samce	samec	k1gInSc2	samec
a	a	k8xC	a
samice	samice	k1gFnSc2	samice
různobarevné	různobarevný	k2eAgFnSc2d1	různobarevná
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
barevná	barevný	k2eAgFnSc1d1	barevná
různorodost	různorodost	k1gFnSc1	různorodost
ozobí	ozobí	k1gNnSc2	ozobí
u	u	k7c2	u
andulek	andulka	k1gFnPc2	andulka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
samec	samec	k1gMnSc1	samec
ozobí	ozobí	k1gNnSc2	ozobí
modré	modrý	k2eAgFnSc2d1	modrá
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
růžové	růžový	k2eAgNnSc4d1	růžové
až	až	k6eAd1	až
hnědé	hnědý	k2eAgNnSc4d1	hnědé
<g/>
.	.	kIx.	.
</s>
