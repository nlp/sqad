<p>
<s>
Katarakt	katarakt	k1gInSc1	katarakt
je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
řecký	řecký	k2eAgInSc1d1	řecký
výraz	výraz	k1gInSc1	výraz
(	(	kIx(	(
<g/>
κ	κ	k?	κ
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
peřeje	peřej	k1gInPc4	peřej
ve	v	k7c6	v
vodních	vodní	k2eAgInPc6d1	vodní
tocích	tok	k1gInPc6	tok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
výraz	výraz	k1gInSc1	výraz
katarakt	katarakta	k1gFnPc2	katarakta
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
nízkého	nízký	k2eAgInSc2d1	nízký
stupňovitého	stupňovitý	k2eAgInSc2d1	stupňovitý
vodopádu	vodopád	k1gInSc2	vodopád
případně	případně	k6eAd1	případně
souvislého	souvislý	k2eAgInSc2d1	souvislý
peřejnatého	peřejnatý	k2eAgInSc2d1	peřejnatý
úseku	úsek	k1gInSc2	úsek
řeky	řeka	k1gFnSc2	řeka
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
vysokým	vysoký	k2eAgInSc7d1	vysoký
spádem	spád	k1gInSc7	spád
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodáckém	vodácký	k2eAgNnSc6d1	vodácké
názvosloví	názvosloví	k1gNnSc6	názvosloví
je	být	k5eAaImIp3nS	být
katarakt	katarakt	k1gInSc1	katarakt
mezistupněm	mezistupeň	k1gInSc7	mezistupeň
mezi	mezi	k7c7	mezi
peřejí	peřej	k1gFnSc7	peřej
a	a	k8xC	a
vodopádem	vodopád	k1gInSc7	vodopád
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
na	na	k7c4	na
mezi	mezi	k7c4	mezi
sjízdnosti	sjízdnost	k1gFnPc4	sjízdnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámějšími	známý	k2eAgMnPc7d3	nejznámější
jsou	být	k5eAaImIp3nP	být
katarakty	katarakta	k1gFnPc4	katarakta
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Nilu	Nil	k1gInSc2	Nil
<g/>
.	.	kIx.	.
</s>
</p>
