<s>
Raleigh	Raleigh	k1gInSc1	Raleigh
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Severní	severní	k2eAgFnSc1d1	severní
Karolína	Karolína	k1gFnSc1	Karolína
a	a	k8xC	a
ve	v	k7c6	v
státu	stát	k1gInSc2	stát
druhé	druhý	k4xOgInPc1	druhý
největší	veliký	k2eAgInPc1d3	veliký
<g/>
,	,	kIx,	,
za	za	k7c7	za
městem	město	k1gNnSc7	město
Charlotte	Charlott	k1gInSc5	Charlott
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
více	hodně	k6eAd2	hodně
než	než	k8xS	než
370	[number]	k4	370
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přezdívku	přezdívka	k1gFnSc4	přezdívka
Dubové	Dubové	k2eAgNnSc1d1	Dubové
město	město	k1gNnSc1	město
pro	pro	k7c4	pro
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
zde	zde	k6eAd1	zde
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
dubů	dub	k1gInPc2	dub
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
Raleigh	Raleigha	k1gFnPc2	Raleigha
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
nové	nový	k2eAgNnSc1d1	nové
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Severní	severní	k2eAgFnSc2d1	severní
Karolíny	Karolína	k1gFnSc2	Karolína
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1788	[number]	k4	1788
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgInSc1d1	oficiální
zakládací	zakládací	k2eAgInSc1d1	zakládací
akt	akt	k1gInSc1	akt
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
získalo	získat	k5eAaPmAgNnS	získat
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6	po
Walteru	Walter	k1gInSc6	Walter
Raleighovi	Raleigh	k1gMnSc3	Raleigh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
světové	světový	k2eAgNnSc4d1	světové
sídlo	sídlo	k1gNnSc4	sídlo
společnosti	společnost	k1gFnSc2	společnost
Red	Red	k1gFnSc2	Red
Hat	hat	k0	hat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
403	[number]	k4	403
892	[number]	k4	892
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
57,5	[number]	k4	57,5
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
29,3	[number]	k4	29,3
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,5	[number]	k4	0,5
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
4,3	[number]	k4	4,3
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
2,6	[number]	k4	2,6
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
1,4	[number]	k4	1,4
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
11,3	[number]	k4	11,3
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Andrew	Andrew	k?	Andrew
Johnson	Johnson	k1gMnSc1	Johnson
(	(	kIx(	(
<g/>
1808	[number]	k4	1808
<g/>
-	-	kIx~	-
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
Daniel	Daniel	k1gMnSc1	Daniel
McFadden	McFaddna	k1gFnPc2	McFaddna
(	(	kIx(	(
<g/>
*	*	kIx~	*
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ekonometr	ekonometr	k1gInSc1	ekonometr
Reginald	Reginald	k1gMnSc1	Reginald
VelJohnson	VelJohnson	k1gMnSc1	VelJohnson
(	(	kIx(	(
<g/>
*	*	kIx~	*
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Robert	Robert	k1gMnSc1	Robert
Duncan	Duncan	k1gMnSc1	Duncan
McNeill	McNeill	k1gMnSc1	McNeill
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Emily	Emil	k1gMnPc4	Emil
Procterová	Procterový	k2eAgNnPc1d1	Procterový
(	(	kIx(	(
<g/>
*	*	kIx~	*
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Michael	Michael	k1gMnSc1	Michael
C.	C.	kA	C.
Hall	Hall	k1gMnSc1	Hall
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Jeff	Jeff	k1gMnSc1	Jeff
Hardy	Harda	k1gFnSc2	Harda
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
wrestler	wrestler	k1gMnSc1	wrestler
Evan	Evan	k1gMnSc1	Evan
Rachel	Rachel	k1gMnSc1	Rachel
Woodová	Woodová	k1gFnSc1	Woodová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Mike	Mik	k1gFnSc2	Mik
Connell	Connella	k1gFnPc2	Connella
(	(	kIx(	(
<g/>
*	*	kIx~	*
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Connells	Connellsa	k1gFnPc2	Connellsa
Compiè	Compiè	k1gMnSc1	Compiè
Francie	Francie	k1gFnSc2	Francie
Hull	Hull	k1gMnSc1	Hull
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
Kolomna	Kolomna	k1gFnSc1	Kolomna
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Rostock	Rostocka	k1gFnPc2	Rostocka
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
