<s>
Je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
původního	původní	k2eAgInSc2d1	původní
seriálu	seriál	k1gInSc2	seriál
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
ze	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Gene	gen	k1gInSc5	gen
Roddenberry	Roddenberr	k1gInPc4	Roddenberr
<g/>
.	.	kIx.	.
</s>
