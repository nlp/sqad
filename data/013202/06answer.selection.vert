<s>
Jako	jako	k9	jako
Bible	bible	k1gFnSc1	bible
litoměřická	litoměřický	k2eAgFnSc1d1	Litoměřická
jednosvazková	jednosvazkový	k2eAgFnSc1d1	jednosvazková
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
Bible	bible	k1gFnSc2	bible
dokončený	dokončený	k2eAgInSc1d1	dokončený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1429	[number]	k4	1429
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
sepsal	sepsat	k5eAaPmAgMnS	sepsat
písař	písař	k1gMnSc1	písař
Duchek	Duchek	k1gMnSc1	Duchek
z	z	k7c2	z
Mníška	mníšek	k1gMnSc2	mníšek
<g/>
.	.	kIx.	.
</s>
