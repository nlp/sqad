<p>
<s>
Jako	jako	k9	jako
Bible	bible	k1gFnSc1	bible
litoměřická	litoměřický	k2eAgFnSc1d1	Litoměřická
jednosvazková	jednosvazkový	k2eAgFnSc1d1	jednosvazková
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
Bible	bible	k1gFnSc2	bible
dokončený	dokončený	k2eAgInSc1d1	dokončený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1429	[number]	k4	1429
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
sepsal	sepsat	k5eAaPmAgMnS	sepsat
písař	písař	k1gMnSc1	písař
Duchek	Duchek	k1gMnSc1	Duchek
z	z	k7c2	z
Mníška	mníšek	k1gMnSc2	mníšek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
památka	památka	k1gFnSc1	památka
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
druhou	druhý	k4xOgFnSc4	druhý
staročeskou	staročeský	k2eAgFnSc4d1	staročeská
redakci	redakce	k1gFnSc4	redakce
českého	český	k2eAgInSc2d1	český
překladu	překlad	k1gInSc2	překlad
Bible	bible	k1gFnSc2	bible
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nezvěstná	zvěstný	k2eNgFnSc1d1	nezvěstná
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
díky	díky	k7c3	díky
včasnému	včasný	k2eAgNnSc3d1	včasné
pořízení	pořízení	k1gNnSc3	pořízení
fotokopií	fotokopie	k1gFnPc2	fotokopie
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc4d1	znám
její	její	k3xOp3gInSc4	její
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
Biblí	bible	k1gFnSc7	bible
Duchkovou	Duchková	k1gFnSc7	Duchková
téhož	týž	k3xTgMnSc2	týž
písaře	písař	k1gMnSc2	písař
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1433	[number]	k4	1433
<g/>
,	,	kIx,	,
určité	určitý	k2eAgInPc1d1	určitý
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
však	však	k9	však
existují	existovat	k5eAaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
</p>
