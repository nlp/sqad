<s>
Člověk	člověk	k1gMnSc1	člověk
moudrý	moudrý	k2eAgMnSc1d1	moudrý
(	(	kIx(	(
<g/>
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
sapiens	sapiens	k6eAd1	sapiens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
člověk	člověk	k1gMnSc1	člověk
rozumný	rozumný	k2eAgMnSc1d1	rozumný
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
označován	označován	k2eAgMnSc1d1	označován
jako	jako	k8xS	jako
člověk	člověk	k1gMnSc1	člověk
(	(	kIx(	(
<g/>
všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
dohromady	dohromady	k6eAd1	dohromady
pak	pak	k6eAd1	pak
lidstvo	lidstvo	k1gNnSc1	lidstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
také	také	k9	také
označení	označení	k1gNnSc1	označení
celého	celý	k2eAgInSc2d1	celý
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
žijícím	žijící	k2eAgInSc7d1	žijící
druhem	druh	k1gInSc7	druh
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
charakteristické	charakteristický	k2eAgNnSc4d1	charakteristické
vertikální	vertikální	k2eAgNnSc4d1	vertikální
držení	držení	k1gNnSc4	držení
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
rozumová	rozumový	k2eAgFnSc1d1	rozumová
inteligence	inteligence	k1gFnSc1	inteligence
a	a	k8xC	a
schopnost	schopnost	k1gFnSc1	schopnost
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
sama	sám	k3xTgMnSc4	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
subjekt	subjekt	k1gInSc4	subjekt
socio-historické	socioistorický	k2eAgFnSc2d1	socio-historický
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
rysem	rys	k1gInSc7	rys
dnešního	dnešní	k2eAgMnSc2d1	dnešní
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc4	schopnost
vyrábět	vyrábět	k5eAaImF	vyrábět
komplexní	komplexní	k2eAgInPc4d1	komplexní
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
použít	použít	k5eAaPmF	použít
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
ovlivnění	ovlivnění	k1gNnSc3	ovlivnění
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
také	také	k9	také
jiní	jiný	k2eAgMnPc1d1	jiný
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
např.	např.	kA	např.
všichni	všechen	k3xTgMnPc1	všechen
žijící	žijící	k2eAgMnPc1d1	žijící
zástupci	zástupce	k1gMnPc1	zástupce
nadčeledi	nadčeleď	k1gFnSc2	nadčeleď
Hominoidea	Hominoidea	k1gMnSc1	Hominoidea
(	(	kIx(	(
<g/>
orangutan	orangutan	k1gMnSc1	orangutan
<g/>
,	,	kIx,	,
šimpanz	šimpanz	k1gMnSc1	šimpanz
<g/>
,	,	kIx,	,
gorila	gorila	k1gFnSc1	gorila
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umějí	umět	k5eAaImIp3nP	umět
použít	použít	k5eAaPmF	použít
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
nástroje	nástroj	k1gInPc1	nástroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
antropogeneze	antropogeneze	k1gFnSc1	antropogeneze
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
postupná	postupný	k2eAgFnSc1d1	postupná
evoluce	evoluce	k1gFnSc1	evoluce
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
čtvrtohor	čtvrtohory	k1gFnPc2	čtvrtohory
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
poslední	poslední	k2eAgFnSc2d1	poslední
fáze	fáze	k1gFnSc2	fáze
dospěl	dochvít	k5eAaPmAgInS	dochvít
vývojový	vývojový	k2eAgInSc1d1	vývojový
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
začal	začít	k5eAaPmAgInS	začít
u	u	k7c2	u
primitivních	primitivní	k2eAgMnPc2d1	primitivní
primátů	primát	k1gMnPc2	primát
v	v	k7c6	v
paleocénu	paleocén	k1gInSc6	paleocén
a	a	k8xC	a
který	který	k3yRgInSc1	který
skončil	skončit	k5eAaPmAgInS	skončit
vznikem	vznik	k1gInSc7	vznik
moderního	moderní	k2eAgMnSc2d1	moderní
člověka	člověk	k1gMnSc2	člověk
Homo	Homo	k1gMnSc1	Homo
sapiens	sapiensa	k1gFnPc2	sapiensa
–	–	k?	–
asi	asi	k9	asi
před	před	k7c7	před
200	[number]	k4	200
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
ale	ale	k8xC	ale
už	už	k6eAd1	už
před	před	k7c7	před
300	[number]	k4	300
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
vzniku	vznik	k1gInSc2	vznik
člověka	člověk	k1gMnSc2	člověk
moderního	moderní	k2eAgInSc2d1	moderní
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
<g/>
)	)	kIx)	)
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
prochází	procházet	k5eAaImIp3nS	procházet
překotným	překotný	k2eAgInSc7d1	překotný
vývojem	vývoj	k1gInSc7	vývoj
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nově	nově	k6eAd1	nově
získaných	získaný	k2eAgInPc2d1	získaný
genetických	genetický	k2eAgInPc2d1	genetický
poznatků	poznatek	k1gInPc2	poznatek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
teorie	teorie	k1gFnPc1	teorie
–	–	k?	–
multiregionální	multiregionální	k2eAgFnPc1d1	multiregionální
teorie	teorie	k1gFnPc1	teorie
a	a	k8xC	a
teorie	teorie	k1gFnPc1	teorie
Out-of-Africa	Outf-Africum	k1gNnSc2	Out-of-Africum
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
poměrně	poměrně	k6eAd1	poměrně
nedávno	nedávno	k6eAd1	nedávno
doplněny	doplnit	k5eAaPmNgFnP	doplnit
o	o	k7c4	o
kompromisní	kompromisní	k2eAgFnSc4d1	kompromisní
teorii	teorie	k1gFnSc4	teorie
difúzní	difúzní	k2eAgFnSc2d1	difúzní
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tzv.	tzv.	kA	tzv.
multiregionální	multiregionální	k2eAgFnSc1d1	multiregionální
teorie	teorie	k1gFnSc1	teorie
se	se	k3xPyFc4	se
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
několikrát	několikrát	k6eAd1	několikrát
nezávisle	závisle	k6eNd1	závisle
z	z	k7c2	z
lokálních	lokální	k2eAgFnPc2d1	lokální
populací	populace	k1gFnPc2	populace
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
jsou	být	k5eAaImIp3nP	být
logicky	logicky	k6eAd1	logicky
toho	ten	k3xDgInSc2	ten
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
neandrtálci	neandrtálec	k1gMnPc1	neandrtálec
jsou	být	k5eAaImIp3nP	být
poddruh	poddruh	k1gInSc4	poddruh
moderního	moderní	k2eAgMnSc2d1	moderní
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
neanderthalensis	neanderthalensis	k1gFnPc2	neanderthalensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
křížením	křížení	k1gNnSc7	křížení
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
unifikaci	unifikace	k1gFnSc3	unifikace
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
populací	populace	k1gFnPc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
Out-of-Africa	Outf-Africa	k1gFnSc1	Out-of-Africa
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
šíření	šíření	k1gNnSc1	šíření
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
)	)	kIx)	)
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
moderní	moderní	k2eAgMnPc1d1	moderní
lidé	člověk	k1gMnPc1	člověk
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
jediného	jediný	k2eAgNnSc2d1	jediné
evolučního	evoluční	k2eAgNnSc2d1	evoluční
centra	centrum	k1gNnSc2	centrum
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
nahradili	nahradit	k5eAaPmAgMnP	nahradit
(	(	kIx(	(
<g/>
možná	možná	k9	možná
i	i	k9	i
vyhubili	vyhubit	k5eAaPmAgMnP	vyhubit
<g/>
)	)	kIx)	)
starší	starší	k1gMnSc1	starší
eurasijské	eurasijský	k2eAgFnSc2d1	eurasijská
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
plodně	plodně	k6eAd1	plodně
křížili	křížit	k5eAaImAgMnP	křížit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
světové	světový	k2eAgFnPc1d1	světová
populace	populace	k1gFnPc1	populace
lidstva	lidstvo	k1gNnSc2	lidstvo
daleko	daleko	k6eAd1	daleko
bližší	blízký	k2eAgMnPc1d2	bližší
<g/>
.	.	kIx.	.
</s>
<s>
Genetické	genetický	k2eAgFnPc1d1	genetická
studie	studie	k1gFnPc1	studie
ale	ale	k9	ale
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šíření	šíření	k1gNnSc1	šíření
moderního	moderní	k2eAgMnSc2d1	moderní
člověka	člověk	k1gMnSc2	člověk
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
bylo	být	k5eAaImAgNnS	být
vícenásobné	vícenásobný	k2eAgNnSc1d1	vícenásobné
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
i	i	k9	i
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
dříve	dříve	k6eAd2	dříve
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
)	)	kIx)	)
a	a	k8xC	a
jen	jen	k9	jen
potomci	potomek	k1gMnPc1	potomek
posledního	poslední	k2eAgNnSc2d1	poslední
jsou	být	k5eAaImIp3nP	být
předky	předek	k1gMnPc7	předek
většiny	většina	k1gFnSc2	většina
dnešní	dnešní	k2eAgFnSc2d1	dnešní
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
difuzní	difuzní	k2eAgFnSc2d1	difuzní
vlny	vlna	k1gFnSc2	vlna
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
nedávným	dávný	k2eNgInSc7d1	nedávný
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
kompromis	kompromis	k1gInSc4	kompromis
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
pohledy	pohled	k1gInPc7	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
africký	africký	k2eAgInSc1d1	africký
původ	původ	k1gInSc1	původ
anatomicky	anatomicky	k6eAd1	anatomicky
moderního	moderní	k2eAgMnSc2d1	moderní
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
připouští	připouštět	k5eAaImIp3nS	připouštět
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
populace	populace	k1gFnPc1	populace
<g/>
,	,	kIx,	,
migrující	migrující	k2eAgNnPc1d1	migrující
mimo	mimo	k7c4	mimo
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
plodně	plodně	k6eAd1	plodně
křížily	křížit	k5eAaImAgFnP	křížit
s	s	k7c7	s
archaickými	archaický	k2eAgMnPc7d1	archaický
zástupci	zástupce	k1gMnPc7	zástupce
rodu	rod	k1gInSc2	rod
Homo	Homo	k1gMnSc1	Homo
z	z	k7c2	z
Eurasie	Eurasie	k1gFnSc2	Eurasie
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
jejich	jejich	k3xOp3gInPc1	jejich
geny	gen	k1gInPc1	gen
"	"	kIx"	"
<g/>
difundovaly	difundovat	k5eAaImAgFnP	difundovat
<g/>
"	"	kIx"	"
do	do	k7c2	do
genofondu	genofond	k1gInSc2	genofond
anatomicky	anatomicky	k6eAd1	anatomicky
moderních	moderní	k2eAgMnPc2d1	moderní
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
nových	nový	k2eAgInPc2d1	nový
objevů	objev	k1gInPc2	objev
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgMnPc1	první
zástupci	zástupce	k1gMnPc1	zástupce
člověka	člověk	k1gMnSc2	člověk
rozumného	rozumný	k2eAgMnSc4d1	rozumný
vznikli	vzniknout	k5eAaPmAgMnP	vzniknout
již	již	k6eAd1	již
před	před	k7c4	před
asi	asi	k9	asi
280	[number]	k4	280
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c4	o
80	[number]	k4	80
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
genetických	genetický	k2eAgFnPc2d1	genetická
studií	studie	k1gFnPc2	studie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
moderní	moderní	k2eAgMnSc1d1	moderní
člověk	člověk	k1gMnSc1	člověk
i	i	k8xC	i
starší	starý	k2eAgMnSc1d2	starší
než	než	k8xS	než
300	[number]	k4	300
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
paleontologickým	paleontologický	k2eAgInPc3d1	paleontologický
nálezům	nález	k1gInPc3	nález
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
nyní	nyní	k6eAd1	nyní
patří	patřit	k5eAaImIp3nS	patřit
190	[number]	k4	190
000	[number]	k4	000
let	léto	k1gNnPc2	léto
staré	stará	k1gFnSc2	stará
nálezy	nález	k1gInPc1	nález
(	(	kIx(	(
<g/>
Omo	Omo	k1gFnSc1	Omo
<g/>
,	,	kIx,	,
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k1gNnPc7	další
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
160	[number]	k4	160
000	[number]	k4	000
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnPc1d1	stará
(	(	kIx(	(
<g/>
Jebel	Jebel	k1gMnSc1	Jebel
Irhoud	Irhoud	k1gMnSc1	Irhoud
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc1	Maroko
<g/>
)	)	kIx)	)
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
000	[number]	k4	000
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnPc1d1	stará
(	(	kIx(	(
<g/>
Qafzeh	Qafzeh	k1gInSc1	Qafzeh
a	a	k8xC	a
Skhul	Skhul	k1gInSc1	Skhul
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
<g/>
;	;	kIx,	;
Klasies	Klasies	k1gMnSc1	Klasies
River	River	k1gMnSc1	River
Caves	Caves	k1gMnSc1	Caves
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následují	následovat	k5eAaImIp3nP	následovat
nálezy	nález	k1gInPc1	nález
až	až	k9	až
ze	z	k7c2	z
svrchního	svrchní	k2eAgInSc2d1	svrchní
paleolitu	paleolit	k1gInSc2	paleolit
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
výskytem	výskyt	k1gInSc7	výskyt
je	být	k5eAaImIp3nS	být
Asie	Asie	k1gFnSc1	Asie
a	a	k8xC	a
až	až	k9	až
pak	pak	k6eAd1	pak
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
paleontologických	paleontologický	k2eAgInPc2d1	paleontologický
nálezů	nález	k1gInPc2	nález
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
zcela	zcela	k6eAd1	zcela
moderního	moderní	k2eAgInSc2d1	moderní
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
zvaní	zvaní	k1gNnSc4	zvaní
také	také	k9	také
kromaňonci	kromaňonek	k1gMnPc1	kromaňonek
<g/>
)	)	kIx)	)
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
asi	asi	k9	asi
před	před	k7c7	před
45	[number]	k4	45
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
postupně	postupně	k6eAd1	postupně
nahradili	nahradit	k5eAaPmAgMnP	nahradit
neandrtálce	neandrtálec	k1gMnPc4	neandrtálec
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
křížení	křížení	k1gNnSc3	křížení
obou	dva	k4xCgInPc6	dva
populací	populace	k1gFnPc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
%	%	kIx~	%
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
lidí	člověk	k1gMnPc2	člověk
nepocházejících	pocházející	k2eNgMnPc2d1	nepocházející
ze	z	k7c2	z
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
je	být	k5eAaImIp3nS	být
neandrtálského	neandrtálský	k2eAgInSc2d1	neandrtálský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Neandrtálci	neandrtálec	k1gMnPc1	neandrtálec
vyhynuli	vyhynout	k5eAaPmAgMnP	vyhynout
asi	asi	k9	asi
před	před	k7c7	před
24	[number]	k4	24
tisíci	tisíc	k4xCgInPc7	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
nálezy	nález	k1gInPc1	nález
s	s	k7c7	s
genetickými	genetický	k2eAgNnPc7d1	genetické
daty	datum	k1gNnPc7	datum
<g/>
.	.	kIx.	.
</s>
<s>
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
rozumný	rozumný	k2eAgMnSc1d1	rozumný
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
lidského	lidský	k2eAgInSc2d1	lidský
druhu	druh	k1gInSc2	druh
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
vzpřímený	vzpřímený	k2eAgMnSc1d1	vzpřímený
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
asi	asi	k9	asi
před	před	k7c7	před
2	[number]	k4	2
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
z	z	k7c2	z
nejstaršího	starý	k2eAgInSc2d3	nejstarší
druhu	druh	k1gInSc2	druh
lidského	lidský	k2eAgInSc2d1	lidský
rodu	rod	k1gInSc2	rod
zvaného	zvaný	k2eAgInSc2d1	zvaný
Homo	Homo	k6eAd1	Homo
habilis	habilis	k1gFnPc4	habilis
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
zručný	zručný	k2eAgMnSc1d1	zručný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgInSc1d3	nejnovější
výzkum	výzkum	k1gInSc1	výzkum
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
evolučních	evoluční	k2eAgFnPc6d1	evoluční
liniích	linie	k1gFnPc6	linie
(	(	kIx(	(
<g/>
africké	africký	k2eAgFnPc1d1	africká
a	a	k8xC	a
evropské	evropský	k2eAgFnPc1d1	Evropská
<g/>
)	)	kIx)	)
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
různých	různý	k2eAgInPc2d1	různý
poddruhů	poddruh	k1gInPc2	poddruh
Homo	Homo	k1gMnSc1	Homo
erecta	erecta	k1gMnSc1	erecta
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejichž	jejichž	k3xOyRp3gNnSc3	jejichž
rozlišení	rozlišení	k1gNnSc3	rozlišení
ze	z	k7c2	z
společného	společný	k2eAgMnSc2d1	společný
předka	předek	k1gMnSc2	předek
došlo	dojít	k5eAaPmAgNnS	dojít
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
400	[number]	k4	400
tisíci	tisíc	k4xCgInPc7	tisíc
až	až	k8xS	až
350	[number]	k4	350
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc1	dva
nejstarší	starý	k2eAgInPc1d3	nejstarší
poddruhy	poddruh	k1gInPc1	poddruh
samotného	samotný	k2eAgNnSc2d1	samotné
Homo	Homo	k1gNnSc1	Homo
sapienta	sapienta	k1gFnSc1	sapienta
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
liniích	linie	k1gFnPc6	linie
navzájem	navzájem	k6eAd1	navzájem
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
lišily	lišit	k5eAaImAgFnP	lišit
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
již	již	k6eAd1	již
vyznačovaly	vyznačovat	k5eAaImAgFnP	vyznačovat
plně	plně	k6eAd1	plně
sapientními	sapientní	k2eAgInPc7d1	sapientní
rysy	rys	k1gInPc7	rys
a	a	k8xC	a
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c4	o
Homo	Homo	k1gNnSc4	Homo
sapientovi	sapientův	k2eAgMnPc1d1	sapientův
jako	jako	k8xS	jako
o	o	k7c6	o
lidském	lidský	k2eAgInSc6d1	lidský
druhu	druh	k1gInSc6	druh
dvojího	dvojí	k4xRgInSc2	dvojí
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Rozdvojení	rozdvojení	k1gNnSc1	rozdvojení
společného	společný	k2eAgNnSc2d1	společné
předka	předek	k1gMnSc4	předek
obou	dva	k4xCgFnPc6	dva
evolučních	evoluční	k2eAgFnPc2d1	evoluční
linií	linie	k1gFnPc2	linie
předznamenal	předznamenat	k5eAaPmAgInS	předznamenat
prostorový	prostorový	k2eAgInSc1d1	prostorový
rozmach	rozmach	k1gInSc1	rozmach
části	část	k1gFnSc2	část
východoafrické	východoafrický	k2eAgFnSc2d1	východoafrická
populace	populace	k1gFnSc2	populace
Homo	Homo	k1gMnSc1	Homo
erecta	erecta	k1gMnSc1	erecta
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
1	[number]	k4	1
miliónem	milión	k4xCgInSc7	milión
až	až	k8xS	až
600	[number]	k4	600
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
putující	putující	k2eAgMnSc1d1	putující
<g/>
,	,	kIx,	,
rozšiřující	rozšiřující	k2eAgMnSc1d1	rozšiřující
se	se	k3xPyFc4	se
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
se	se	k3xPyFc4	se
ve	v	k7c6	v
vědeckých	vědecký	k2eAgInPc6d1	vědecký
kruzích	kruh	k1gInPc6	kruh
začal	začít	k5eAaPmAgInS	začít
specificky	specificky	k6eAd1	specificky
či	či	k8xC	či
alternativně	alternativně	k6eAd1	alternativně
nazývat	nazývat	k5eAaImF	nazývat
Homo	Homo	k6eAd1	Homo
antecessor	antecessor	k1gInSc4	antecessor
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
průkopník	průkopník	k1gMnSc1	průkopník
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
600	[number]	k4	600
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
se	se	k3xPyFc4	se
z	z	k7c2	z
Homo	Homo	k6eAd1	Homo
erecta	erecto	k1gNnSc2	erecto
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
poddruh	poddruh	k1gInSc1	poddruh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
co	co	k9	co
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
východoafrické	východoafrický	k2eAgFnSc2d1	východoafrická
populace	populace	k1gFnSc2	populace
nazývá	nazývat	k5eAaImIp3nS	nazývat
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
rhodéský	rhodéský	k1gMnSc1	rhodéský
(	(	kIx(	(
<g/>
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
rhodesiensis	rhodesiensis	k1gFnSc1	rhodesiensis
<g/>
)	)	kIx)	)
a	a	k8xC	a
co	co	k3yQnSc1	co
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
populace	populace	k1gFnSc2	populace
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
heidelberský	heidelberský	k2eAgMnSc1d1	heidelberský
(	(	kIx(	(
<g/>
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
heidelbergensis	heidelbergensis	k1gFnSc1	heidelbergensis
-	-	kIx~	-
název	název	k1gInSc1	název
po	po	k7c6	po
německém	německý	k2eAgNnSc6d1	německé
nalezišti	naleziště	k1gNnSc6	naleziště
Heidelberg	Heidelberg	k1gInSc1	Heidelberg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eventuálně	eventuálně	k6eAd1	eventuálně
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
staromaďarský	staromaďarský	k2eAgMnSc1d1	staromaďarský
(	(	kIx(	(
<g/>
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
paleohungaricus	paleohungaricus	k1gMnSc1	paleohungaricus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odlišnější	odlišný	k2eAgFnPc1d2	odlišnější
přírodní	přírodní	k2eAgFnPc1d1	přírodní
podmínky	podmínka	k1gFnPc1	podmínka
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
způsobily	způsobit	k5eAaPmAgFnP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zdejší	zdejší	k2eAgFnSc2d1	zdejší
populace	populace	k1gFnSc2	populace
antecessorovitého	antecessorovitý	k2eAgInSc2d1	antecessorovitý
Homo	Homo	k6eAd1	Homo
erecta	erecto	k1gNnSc2	erecto
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
400	[number]	k4	400
až	až	k8xS	až
350	[number]	k4	350
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
vývoji	vývoj	k1gInSc6	vývoj
odlišila	odlišit	k5eAaPmAgFnS	odlišit
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
východoafrické	východoafrický	k2eAgFnSc2d1	východoafrická
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
Homo	Homo	k1gMnSc1	Homo
erecta	erecta	k1gMnSc1	erecta
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
začaly	začít	k5eAaPmAgFnP	začít
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
dvě	dva	k4xCgFnPc1	dva
výrazné	výrazný	k2eAgFnPc1d1	výrazná
evoluční	evoluční	k2eAgFnPc1d1	evoluční
linie	linie	k1gFnPc1	linie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
linií	linie	k1gFnPc2	linie
se	se	k3xPyFc4	se
Homo	Homo	k6eAd1	Homo
erectus	erectus	k1gInSc1	erectus
heidelberský	heidelberský	k2eAgInSc1d1	heidelberský
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
300	[number]	k4	300
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
dále	daleko	k6eAd2	daleko
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
v	v	k7c4	v
Homo	Homo	k1gNnSc4	Homo
antecessora	antecessor	k1gMnSc2	antecessor
steinheimského	steinheimský	k2eAgMnSc2d1	steinheimský
(	(	kIx(	(
<g/>
Homo	Homo	k1gNnSc1	Homo
antecessor	antecessora	k1gFnPc2	antecessora
steinheimensis	steinheimensis	k1gFnSc2	steinheimensis
–	–	k?	–
název	název	k1gInSc1	název
po	po	k7c6	po
německém	německý	k2eAgNnSc6d1	německé
nalezišti	naleziště	k1gNnSc6	naleziště
Steinheim	Steinheima	k1gFnPc2	Steinheima
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
asi	asi	k9	asi
před	před	k7c7	před
250	[number]	k4	250
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
do	do	k7c2	do
nejstaršího	starý	k2eAgInSc2d3	nejstarší
poddruhu	poddruh	k1gInSc2	poddruh
plnohodnotného	plnohodnotný	k2eAgInSc2d1	plnohodnotný
Homo	Homo	k6eAd1	Homo
sapienta	sapient	k1gMnSc2	sapient
–	–	k?	–
Homo	Homo	k1gNnSc1	Homo
sapienta	sapient	k1gMnSc2	sapient
neandertálského	neandertálský	k2eAgMnSc2d1	neandertálský
(	(	kIx(	(
<g/>
Homo	Homo	k1gNnSc1	Homo
sapiens	sapiensa	k1gFnPc2	sapiensa
neanderthalensis	neanderthalensis	k1gFnSc2	neanderthalensis
–	–	k?	–
název	název	k1gInSc1	název
po	po	k7c6	po
německém	německý	k2eAgNnSc6d1	německé
nalezišti	naleziště	k1gNnSc6	naleziště
Neanderthal	Neanderthal	k1gMnSc1	Neanderthal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
evoluční	evoluční	k2eAgFnSc6d1	evoluční
linii	linie	k1gFnSc6	linie
se	se	k3xPyFc4	se
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
rhodéský	rhodéský	k1gMnSc1	rhodéský
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgInSc1d3	nejstarší
nález	nález	k1gInSc1	nález
ve	v	k7c6	v
východoafrické	východoafrický	k2eAgFnSc6d1	východoafrická
Etiopii	Etiopie	k1gFnSc6	Etiopie
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Awaš	Awaš	k1gInSc1	Awaš
-	-	kIx~	-
600	[number]	k4	600
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
dále	daleko	k6eAd2	daleko
vyvíjel	vyvíjet	k5eAaImAgInS	vyvíjet
až	až	k9	až
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
nejstaršímu	starý	k2eAgInSc3d3	nejstarší
poddruhu	poddruh	k1gInSc2	poddruh
plnohodnotného	plnohodnotný	k2eAgInSc2d1	plnohodnotný
Homo	Homo	k6eAd1	Homo
sapienta	sapient	k1gMnSc2	sapient
–	–	k?	–
Homo	Homo	k6eAd1	Homo
sapientovi	sapientův	k2eAgMnPc1d1	sapientův
idaltskému	idaltský	k2eAgInSc3d1	idaltský
(	(	kIx(	(
<g/>
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
idaltu	idalt	k1gInSc2	idalt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnPc1d3	nejstarší
představitelé	představitel	k1gMnPc1	představitel
tohoto	tento	k3xDgInSc2	tento
poddruhu	poddruh	k1gInSc2	poddruh
Homo	Homo	k6eAd1	Homo
sapienta	sapiento	k1gNnSc2	sapiento
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
asi	asi	k9	asi
před	před	k7c7	před
200	[number]	k4	200
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
(	(	kIx(	(
<g/>
tanzánský	tanzánský	k2eAgInSc4d1	tanzánský
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
Omo	Omo	k1gFnSc2	Omo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
konstatovat	konstatovat	k5eAaBmF	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejstarší	starý	k2eAgInSc1d3	nejstarší
poddruh	poddruh	k1gInSc1	poddruh
Homo	Homo	k1gNnSc1	Homo
sapienta	sapient	k1gMnSc2	sapient
(	(	kIx(	(
<g/>
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
neandertálský	neandertálský	k2eAgInSc1d1	neandertálský
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
evoluční	evoluční	k2eAgFnSc6d1	evoluční
linii	linie	k1gFnSc6	linie
z	z	k7c2	z
Homo	Homo	k1gMnSc1	Homo
erecta	erect	k1gMnSc2	erect
heidelberského	heidelberský	k2eAgInSc2d1	heidelberský
asi	asi	k9	asi
před	před	k7c7	před
250	[number]	k4	250
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
nejstarší	starý	k2eAgInSc1d3	nejstarší
poddruh	poddruh	k1gInSc1	poddruh
Homo	Homo	k1gNnSc1	Homo
sapienta	sapient	k1gMnSc2	sapient
(	(	kIx(	(
<g/>
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
idaltský	idaltský	k2eAgInSc1d1	idaltský
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
evoluční	evoluční	k2eAgFnSc6d1	evoluční
linii	linie	k1gFnSc6	linie
z	z	k7c2	z
Homo	Homo	k1gMnSc1	Homo
erecta	erect	k1gMnSc2	erect
rhodéského	rhodéské	k1gNnSc2	rhodéské
asi	asi	k9	asi
před	před	k7c7	před
200	[number]	k4	200
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
porovnáním	porovnání	k1gNnSc7	porovnání
obou	dva	k4xCgFnPc2	dva
linií	linie	k1gFnPc2	linie
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
linie	linie	k1gFnSc2	linie
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
k	k	k7c3	k
Homo	Homo	k6eAd1	Homo
sapientovi	sapientův	k2eAgMnPc1d1	sapientův
neandertálskému	neandertálský	k2eAgInSc3d1	neandertálský
se	se	k3xPyFc4	se
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
podmínkách	podmínka	k1gFnPc6	podmínka
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
o	o	k7c4	o
něco	něco	k3yInSc4	něco
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
linie	linie	k1gFnSc2	linie
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
k	k	k7c3	k
Homo	Homo	k6eAd1	Homo
sapientovi	sapient	k1gMnSc3	sapient
idaltskému	idaltský	k2eAgMnSc3d1	idaltský
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
neandertálský	neandertálský	k2eAgMnSc1d1	neandertálský
se	se	k3xPyFc4	se
z	z	k7c2	z
Homo	Homo	k1gMnSc1	Homo
erecta	erect	k1gMnSc2	erect
heidelberského	heidelberský	k2eAgInSc2d1	heidelberský
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
50	[number]	k4	50
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k1gInSc1	sapiens
idaltský	idaltský	k2eAgInSc1d1	idaltský
z	z	k7c2	z
Homo	Homo	k6eAd1	Homo
erecta	erecto	k1gNnSc2	erecto
rhodéského	rhodéského	k2eAgMnPc1d1	rhodéského
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
přes	přes	k7c4	přes
Homo	Homo	k1gNnSc4	Homo
antecessora	antecessor	k1gMnSc2	antecessor
steinheimského	steinheimský	k2eAgMnSc2d1	steinheimský
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
Homo	Homo	k1gMnSc1	Homo
sapienta	sapient	k1gMnSc4	sapient
však	však	k9	však
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatímco	zatímco	k8xS	zatímco
linie	linie	k1gFnSc2	linie
Homo	Homo	k1gNnSc1	Homo
sapienta	sapiento	k1gNnSc2	sapiento
neandertálského	neandertálský	k2eAgNnSc2d1	neandertálský
evolučně	evolučně	k6eAd1	evolučně
stagnovala	stagnovat	k5eAaImAgFnS	stagnovat
<g/>
,	,	kIx,	,
zakrněla	zakrnět	k5eAaPmAgFnS	zakrnět
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vyhynula	vyhynout	k5eAaPmAgFnS	vyhynout
asi	asi	k9	asi
před	před	k7c7	před
40	[number]	k4	40
až	až	k8xS	až
10	[number]	k4	10
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
linie	linie	k1gFnSc1	linie
Homo	Homo	k1gNnSc1	Homo
sapienta	sapient	k1gMnSc2	sapient
idaltského	idaltský	k2eAgMnSc2d1	idaltský
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
100	[number]	k4	100
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
poddruh	poddruh	k1gInSc1	poddruh
Homo	Homo	k1gNnSc1	Homo
sapienta	sapient	k1gMnSc2	sapient
palestinského	palestinský	k2eAgMnSc2d1	palestinský
(	(	kIx(	(
<g/>
Homo	Homo	k1gNnSc1	Homo
sapiens	sapiens	k6eAd1	sapiens
palestinus	palestinus	k1gInSc1	palestinus
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
45	[number]	k4	45
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
v	v	k7c6	v
euroasijské	euroasijský	k2eAgFnSc6d1	euroasijská
oblasti	oblast	k1gFnSc6	oblast
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
sapiens	sapiens	k6eAd1	sapiens
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
rozumný	rozumný	k2eAgInSc1d1	rozumný
rozumný	rozumný	k2eAgInSc1d1	rozumný
<g/>
"	"	kIx"	"
-	-	kIx~	-
jednoduše	jednoduše	k6eAd1	jednoduše
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
moudrý	moudrý	k2eAgMnSc1d1	moudrý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
známým	známý	k1gMnSc7	známý
také	také	k9	také
jako	jako	k9	jako
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
kromaňonský	kromaňonský	k2eAgMnSc1d1	kromaňonský
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
prvním	první	k4xOgNnSc6	první
nalezišti	naleziště	k1gNnSc6	naleziště
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
Cro-Magnonu	Cro-Magnon	k1gInSc6	Cro-Magnon
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
již	již	k9	již
o	o	k7c4	o
moderního	moderní	k2eAgMnSc4d1	moderní
člověka	člověk	k1gMnSc4	člověk
dnešního	dnešní	k2eAgInSc2d1	dnešní
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
diverzifikovat	diverzifikovat	k5eAaImF	diverzifikovat
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
rasy	rasa	k1gFnPc4	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
ontogeneze	ontogeneze	k1gFnSc2	ontogeneze
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
prenatální	prenatální	k2eAgNnSc1d1	prenatální
obdobíoplozené	obdobíoplozený	k2eAgNnSc1d1	obdobíoplozený
vajíčko	vajíčko	k1gNnSc1	vajíčko
(	(	kIx(	(
<g/>
zygota	zygota	k1gFnSc1	zygota
<g/>
)	)	kIx)	)
od	od	k7c2	od
oplození	oplození	k1gNnSc2	oplození
do	do	k7c2	do
konce	konec	k1gInSc2	konec
1	[number]	k4	1
<g/>
.	.	kIx.	.
týdne	týden	k1gInSc2	týden
embryo	embryo	k1gNnSc1	embryo
(	(	kIx(	(
<g/>
zárodek	zárodek	k1gInSc1	zárodek
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
týden	týden	k1gInSc1	týden
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
týden	týden	k1gInSc1	týden
fetus	fetus	k1gInSc1	fetus
(	(	kIx(	(
<g/>
plod	plod	k1gInSc1	plod
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
týden	týden	k1gInSc1	týden
–	–	k?	–
porod	porod	k1gInSc4	porod
novorozenecké	novorozenecký	k2eAgNnSc4d1	novorozenecké
období	období	k1gNnSc4	období
1	[number]	k4	1
<g/>
.	.	kIx.	.
den	den	k1gInSc1	den
–	–	k?	–
28	[number]	k4	28
dní	den	k1gInPc2	den
<g/>
(	(	kIx(	(
<g/>
perinatální	perinatální	k2eAgNnSc4d1	perinatální
období	období	k1gNnSc4	období
–	–	k?	–
doba	doba	k1gFnSc1	doba
porodu	porod	k1gInSc2	porod
až	až	k9	až
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
)	)	kIx)	)
kojenec	kojenec	k1gMnSc1	kojenec
28	[number]	k4	28
dní	den	k1gInPc2	den
–	–	k?	–
1	[number]	k4	1
rok	rok	k1gInSc1	rok
batolecí	batolecí	k2eAgInSc1d1	batolecí
období	období	k1gNnSc4	období
1	[number]	k4	1
rok	rok	k1gInSc4	rok
–	–	k?	–
3	[number]	k4	3
rokymladší	rokymladší	k2eAgFnSc1d1	rokymladší
batolecí	batolecí	k2eAgNnSc4d1	batolecí
období	období	k1gNnSc4	období
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
roky	rok	k1gInPc4	rok
starší	starý	k2eAgFnSc7d2	starší
batolecí	batolecí	k2eAgFnSc7d1	batolecí
<g />
.	.	kIx.	.
</s>
<s>
období	období	k1gNnSc1	období
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
roky	rok	k1gInPc4	rok
předškolní	předškolní	k2eAgNnSc4d1	předškolní
období	období	k1gNnSc4	období
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
let	léto	k1gNnPc2	léto
školní	školní	k2eAgNnSc4d1	školní
období	období	k1gNnSc4	období
6	[number]	k4	6
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
–	–	k?	–
15	[number]	k4	15
letmladší	letmladší	k2eAgFnSc1d1	letmladší
školní	školní	k2eAgNnSc4d1	školní
období	období	k1gNnSc4	období
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
let	léto	k1gNnPc2	léto
starší	starý	k2eAgMnSc1d2	starší
školní	školní	k2eAgNnSc4d1	školní
období	období	k1gNnSc4	období
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
puberta	puberta	k1gFnSc1	puberta
<g/>
)	)	kIx)	)
dospívání	dospívání	k1gNnSc1	dospívání
–	–	k?	–
adolescence	adolescence	k1gFnSc1	adolescence
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
let	léto	k1gNnPc2	léto
dospělost	dospělost	k1gFnSc1	dospělost
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
65	[number]	k4	65
letmladší	letmladší	k2eAgFnSc1d1	letmladší
dospělost	dospělost	k1gFnSc1	dospělost
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
let	léto	k1gNnPc2	léto
střední	střední	k2eAgFnSc1d1	střední
dospělost	dospělost	k1gFnSc1	dospělost
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
let	léto	k1gNnPc2	léto
starší	starý	k2eAgFnSc1d2	starší
dospělost	dospělost	k1gFnSc1	dospělost
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
65	[number]	k4	65
let	léto	k1gNnPc2	léto
stáří	stáří	k1gNnSc2	stáří
65	[number]	k4	65
a	a	k8xC	a
více	hodně	k6eAd2	hodně
letpresénium	letpresénium	k1gNnSc1	letpresénium
(	(	kIx(	(
<g/>
stárnutí	stárnutí	k1gNnSc1	stárnutí
<g/>
)	)	kIx)	)
65	[number]	k4	65
<g/>
–	–	k?	–
<g/>
75	[number]	k4	75
let	let	k1gInSc1	let
sénium	sénium	k1gNnSc1	sénium
(	(	kIx(	(
<g/>
stáří	stáří	k1gNnSc1	stáří
<g/>
)	)	kIx)	)
75	[number]	k4	75
<g/>
–	–	k?	–
<g/>
85	[number]	k4	85
let	léto	k1gNnPc2	léto
dlouhověkost	dlouhověkost	k1gFnSc4	dlouhověkost
nad	nad	k7c4	nad
85	[number]	k4	85
let	léto	k1gNnPc2	léto
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
fázi	fáze	k1gFnSc6	fáze
ontogenetického	ontogenetický	k2eAgInSc2d1	ontogenetický
vývoje	vývoj	k1gInSc2	vývoj
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
řadu	řada	k1gFnSc4	řada
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
jsou	být	k5eAaImIp3nP	být
dvojího	dvojí	k4xRgMnSc2	dvojí
druhu	druh	k1gInSc2	druh
<g/>
:	:	kIx,	:
růstové	růstový	k2eAgInPc1d1	růstový
a	a	k8xC	a
vývojové	vývojový	k2eAgInPc1d1	vývojový
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
procesy	proces	k1gInPc1	proces
jsou	být	k5eAaImIp3nP	být
primárně	primárně	k6eAd1	primárně
určeny	určen	k2eAgInPc4d1	určen
dědičnými	dědičný	k2eAgInPc7d1	dědičný
faktory	faktor	k1gInPc7	faktor
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
uplatnění	uplatnění	k1gNnSc4	uplatnění
však	však	k9	však
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
faktory	faktor	k1gInPc1	faktor
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
stáří	stáří	k1gNnSc1	stáří
život	život	k1gInSc4	život
jedince	jedinec	k1gMnSc2	jedinec
přirozeně	přirozeně	k6eAd1	přirozeně
končí	končit	k5eAaImIp3nS	končit
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
mrtvým	mrtvý	k2eAgNnSc7d1	mrtvé
tělem	tělo	k1gNnSc7	tělo
je	být	k5eAaImIp3nS	být
lidmi	člověk	k1gMnPc7	člověk
podle	podle	k7c2	podle
všeobecného	všeobecný	k2eAgInSc2d1	všeobecný
etického	etický	k2eAgInSc2d1	etický
kodexu	kodex	k1gInSc2	kodex
zacházeno	zacházen	k2eAgNnSc1d1	zacházeno
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
úctou	úcta	k1gFnSc7	úcta
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spáleno	spálen	k2eAgNnSc1d1	spáleno
nebo	nebo	k8xC	nebo
pohřbeno	pohřben	k2eAgNnSc1d1	pohřbeno
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
obřadem	obřad	k1gInSc7	obřad
či	či	k8xC	či
výjimečně	výjimečně	k6eAd1	výjimečně
bez	bez	k7c2	bez
obřadu	obřad	k1gInSc2	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
příčiny	příčina	k1gFnSc2	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
lze	lze	k6eAd1	lze
před	před	k7c7	před
pohřbem	pohřeb	k1gInSc7	pohřeb
provést	provést	k5eAaPmF	provést
pitvu	pitva	k1gFnSc4	pitva
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
jedinců	jedinec	k1gMnPc2	jedinec
podle	podle	k7c2	podle
stupně	stupeň	k1gInSc2	stupeň
vývinu	vývin	k1gInSc2	vývin
<g/>
,	,	kIx,	,
stáří	stáří	k1gNnSc2	stáří
<g/>
,	,	kIx,	,
příbuzenství	příbuzenství	k1gNnSc2	příbuzenství
a	a	k8xC	a
společenského	společenský	k2eAgNnSc2d1	společenské
postavení	postavení	k1gNnSc2	postavení
pak	pak	k6eAd1	pak
také	také	k9	také
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Dítě	dítě	k1gNnSc1	dítě
(	(	kIx(	(
<g/>
krajově	krajově	k6eAd1	krajově
též	též	k9	též
děcko	děcko	k1gNnSc4	děcko
<g/>
)	)	kIx)	)
–	–	k?	–
označení	označení	k1gNnSc2	označení
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
věku	věk	k1gInSc2	věk
konce	konec	k1gInSc2	konec
školního	školní	k2eAgNnSc2d1	školní
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
obecněji	obecně	k6eAd2	obecně
pak	pak	k6eAd1	pak
slovo	slovo	k1gNnSc1	slovo
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jedinec	jedinec	k1gMnSc1	jedinec
<g />
.	.	kIx.	.
</s>
<s>
potomkem	potomek	k1gMnSc7	potomek
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
<g/>
/	/	kIx~	/
<g/>
otce	otec	k1gMnSc2	otec
<g/>
)	)	kIx)	)
Dívka	dívka	k1gFnSc1	dívka
(	(	kIx(	(
<g/>
též	též	k9	též
děvče	děvče	k1gNnSc1	děvče
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
pak	pak	k6eAd1	pak
holka	holka	k1gFnSc1	holka
<g/>
)	)	kIx)	)
–	–	k?	–
označení	označení	k1gNnSc2	označení
samičího	samičí	k2eAgNnSc2d1	samičí
pohlaví	pohlaví	k1gNnSc2	pohlaví
v	v	k7c6	v
období	období	k1gNnSc6	období
nejčastěji	často	k6eAd3	často
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
školního	školní	k2eAgNnSc2d1	školní
období	období	k1gNnSc2	období
do	do	k7c2	do
konce	konec	k1gInSc2	konec
dospívání	dospívání	k1gNnSc2	dospívání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
i	i	k9	i
pro	pro	k7c4	pro
ranější	raný	k2eAgFnPc4d2	ranější
fáze	fáze	k1gFnPc4	fáze
(	(	kIx(	(
<g/>
holčička	holčička	k1gFnSc1	holčička
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
děvčátko	děvčátko	k1gNnSc1	děvčátko
<g/>
)	)	kIx)	)
Slečna	slečna	k1gFnSc1	slečna
–	–	k?	–
označení	označení	k1gNnSc1	označení
děvčete	děvče	k1gNnSc2	děvče
či	či	k8xC	či
mladé	mladý	k2eAgFnSc2d1	mladá
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
není	být	k5eNaImIp3nS	být
vdaná	vdaný	k2eAgFnSc1d1	vdaná
Chlapec	chlapec	k1gMnSc1	chlapec
(	(	kIx(	(
<g/>
též	též	k9	též
hoch	hoch	k1gMnSc1	hoch
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
pak	pak	k6eAd1	pak
kluk	kluk	k1gMnSc1	kluk
<g/>
)	)	kIx)	)
–	–	k?	–
označení	označení	k1gNnSc2	označení
samčího	samčí	k2eAgNnSc2d1	samčí
pohlaví	pohlaví	k1gNnSc2	pohlaví
v	v	k7c6	v
období	období	k1gNnSc6	období
nejčastěji	často	k6eAd3	často
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
školního	školní	k2eAgNnSc2d1	školní
období	období	k1gNnSc2	období
do	do	k7c2	do
konce	konec	k1gInSc2	konec
dospívání	dospívání	k1gNnSc2	dospívání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
i	i	k9	i
pro	pro	k7c4	pro
ranější	raný	k2eAgNnSc4d2	ranější
<g />
.	.	kIx.	.
</s>
<s>
fáze	fáze	k1gFnSc1	fáze
(	(	kIx(	(
<g/>
chlapeček	chlapeček	k1gMnSc1	chlapeček
<g/>
)	)	kIx)	)
Žena	žena	k1gFnSc1	žena
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
též	též	k9	též
ženská	ženská	k1gFnSc1	ženská
<g/>
)	)	kIx)	)
–	–	k?	–
označení	označení	k1gNnSc2	označení
samičího	samičí	k2eAgNnSc2d1	samičí
pohlaví	pohlaví	k1gNnSc2	pohlaví
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
dospělosti	dospělost	k1gFnSc2	dospělost
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
Matka	matka	k1gFnSc1	matka
(	(	kIx(	(
<g/>
též	též	k9	též
máma	máma	k1gFnSc1	máma
<g/>
)	)	kIx)	)
–	–	k?	–
označení	označení	k1gNnSc2	označení
samičího	samičí	k2eAgNnSc2d1	samičí
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
žena	žena	k1gFnSc1	žena
rodičkou	rodička	k1gFnSc7	rodička
dítěte	dítě	k1gNnSc2	dítě
Stařenka	stařenka	k1gFnSc1	stařenka
–	–	k?	–
označení	označení	k1gNnSc2	označení
<g />
.	.	kIx.	.
</s>
<s>
samičího	samičí	k2eAgNnSc2d1	samičí
pohlaví	pohlaví	k1gNnSc2	pohlaví
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
stáří	stáří	k1gNnSc2	stáří
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
Babička	babička	k1gFnSc1	babička
–	–	k?	–
označení	označení	k1gNnSc2	označení
samičího	samičí	k2eAgNnSc2d1	samičí
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dítě	dítě	k1gNnSc1	dítě
ženy	žena	k1gFnSc2	žena
je	být	k5eAaImIp3nS	být
matkou	matka	k1gFnSc7	matka
<g/>
/	/	kIx~	/
<g/>
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
obecněji	obecně	k6eAd2	obecně
též	též	k9	též
synonymum	synonymum	k1gNnSc4	synonymum
pro	pro	k7c4	pro
stařenku	stařenka	k1gFnSc4	stařenka
Paní	paní	k1gFnSc2	paní
–	–	k?	–
všednější	všední	k2eAgInSc1d2	všednější
společenský	společenský	k2eAgInSc1d1	společenský
titul	titul	k1gInSc1	titul
ženy	žena	k1gFnSc2	žena
od	od	k7c2	od
dospělosti	dospělost	k1gFnSc2	dospělost
Dáma	dáma	k1gFnSc1	dáma
(	(	kIx(	(
<g/>
též	též	k9	též
madam	madam	k1gFnSc1	madam
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
–	–	k?	–
urozenější	urozený	k2eAgInSc4d2	urozený
společenský	společenský	k2eAgInSc4d1	společenský
titul	titul	k1gInSc4	titul
ženy	žena	k1gFnSc2	žena
od	od	k7c2	od
dospělosti	dospělost	k1gFnSc2	dospělost
Muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
též	též	k9	též
chlap	chlap	k1gMnSc1	chlap
<g/>
)	)	kIx)	)
–	–	k?	–
označení	označení	k1gNnSc2	označení
samčího	samčí	k2eAgNnSc2d1	samčí
pohlaví	pohlaví	k1gNnSc2	pohlaví
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
dospělosti	dospělost	k1gFnSc2	dospělost
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
Otec	otec	k1gMnSc1	otec
(	(	kIx(	(
<g/>
též	též	k9	též
táta	táta	k1gMnSc1	táta
<g/>
)	)	kIx)	)
–	–	k?	–
označení	označení	k1gNnSc2	označení
samčího	samčí	k2eAgNnSc2d1	samčí
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
muž	muž	k1gMnSc1	muž
rodičem	rodič	k1gMnSc7	rodič
dítěte	dítě	k1gNnSc2	dítě
Stařec	stařec	k1gMnSc1	stařec
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
označení	označení	k1gNnSc1	označení
samčího	samčí	k2eAgNnSc2d1	samčí
pohlaví	pohlaví	k1gNnSc2	pohlaví
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
stáří	stáří	k1gNnSc2	stáří
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
Dědeček	dědeček	k1gMnSc1	dědeček
–	–	k?	–
označení	označení	k1gNnSc2	označení
samčího	samčí	k2eAgNnSc2d1	samčí
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dítě	dítě	k1gNnSc1	dítě
muže	muž	k1gMnSc2	muž
je	být	k5eAaImIp3nS	být
matkou	matka	k1gFnSc7	matka
<g/>
/	/	kIx~	/
<g/>
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
obecněji	obecně	k6eAd2	obecně
též	též	k9	též
synonymum	synonymum	k1gNnSc4	synonymum
pro	pro	k7c4	pro
starce	stařec	k1gMnSc4	stařec
Pan	Pan	k1gMnSc1	Pan
–	–	k?	–
společenský	společenský	k2eAgInSc1d1	společenský
titul	titul	k1gInSc1	titul
muže	muž	k1gMnSc2	muž
od	od	k7c2	od
dospělosti	dospělost	k1gFnSc2	dospělost
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
článku	článek	k1gInSc2	článek
lidské	lidský	k2eAgNnSc4d1	lidské
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
menší	malý	k2eAgFnSc1d2	menší
postava	postava	k1gFnSc1	postava
(	(	kIx(	(
<g/>
než	než	k8xS	než
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
<g/>
)	)	kIx)	)
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
se	se	k3xPyFc4	se
brada	brada	k1gFnSc1	brada
zvětšení	zvětšení	k1gNnSc2	zvětšení
mozkovny	mozkovna	k1gFnPc1	mozkovna
malé	malý	k2eAgInPc4d1	malý
nadočnicové	nadočnicový	k2eAgInPc4d1	nadočnicový
oblouky	oblouk	k1gInPc4	oblouk
zřetelný	zřetelný	k2eAgInSc4d1	zřetelný
nos	nos	k1gInSc4	nos
silně	silně	k6eAd1	silně
redukované	redukovaný	k2eAgNnSc4d1	redukované
ochlupení	ochlupení	k1gNnSc4	ochlupení
Člověk	člověk	k1gMnSc1	člověk
dokáže	dokázat	k5eAaPmIp3nS	dokázat
krátkodobě	krátkodobě	k6eAd1	krátkodobě
(	(	kIx(	(
<g/>
v	v	k7c6	v
rekordních	rekordní	k2eAgInPc6d1	rekordní
případech	případ	k1gInPc6	případ
<g/>
)	)	kIx)	)
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
až	až	k9	až
36	[number]	k4	36
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
náboženských	náboženský	k2eAgInPc2d1	náboženský
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
,	,	kIx,	,
židovství	židovství	k1gNnSc1	židovství
<g/>
,	,	kIx,	,
islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
hinduismus	hinduismus	k1gInSc1	hinduismus
a	a	k8xC	a
mnohé	mnohé	k1gNnSc1	mnohé
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
připisují	připisovat	k5eAaImIp3nP	připisovat
člověku	člověk	k1gMnSc3	člověk
nesmrtelnou	smrtelný	k2eNgFnSc4d1	nesmrtelná
duši	duše	k1gFnSc3	duše
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
teistických	teistický	k2eAgNnPc2d1	teistické
náboženství	náboženství	k1gNnPc2	náboženství
byl	být	k5eAaImAgMnS	být
člověk	člověk	k1gMnSc1	člověk
stvořen	stvořen	k2eAgMnSc1d1	stvořen
Bohem	bůh	k1gMnSc7	bůh
či	či	k8xC	či
bohy	bůh	k1gMnPc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
židovského	židovský	k2eAgInSc2d1	židovský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Židé	Žid	k1gMnPc1	Žid
i	i	k9	i
některé	některý	k3yIgInPc1	některý
křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
směry	směr	k1gInPc1	směr
počítají	počítat	k5eAaImIp3nP	počítat
od	od	k7c2	od
biblického	biblický	k2eAgNnSc2d1	biblické
stvoření	stvoření	k1gNnSc2	stvoření
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
stvoření	stvoření	k1gNnSc3	stvoření
došlo	dojít	k5eAaPmAgNnS	dojít
asi	asi	k9	asi
před	před	k7c7	před
5	[number]	k4	5
700	[number]	k4	700
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
stvoření	stvoření	k1gNnSc2	stvoření
duše	duše	k1gFnSc2	duše
vnímání	vnímání	k1gNnSc2	vnímání
city	city	k1gNnSc1	city
vůle	vůle	k1gFnSc2	vůle
vědomí	vědomí	k1gNnSc2	vědomí
činnost	činnost	k1gFnSc4	činnost
postupně	postupně	k6eAd1	postupně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
rod	rod	k1gInSc1	rod
pokrevně	pokrevně	k6eAd1	pokrevně
příbuzných	příbuzný	k1gMnPc2	příbuzný
v	v	k7c6	v
jeskyních	jeskyně	k1gFnPc6	jeskyně
<g/>
,	,	kIx,	,
chatách	chata	k1gFnPc6	chata
z	z	k7c2	z
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
paroží	paroží	k1gNnSc1	paroží
<g/>
,	,	kIx,	,
větví	větvit	k5eAaImIp3nS	větvit
sběr	sběr	k1gInSc4	sběr
<g/>
,	,	kIx,	,
specializovaný	specializovaný	k2eAgInSc4d1	specializovaný
lov	lov	k1gInSc4	lov
velké	velký	k2eAgFnSc2d1	velká
zvěře	zvěř	k1gFnSc2	zvěř
měl	mít	k5eAaImAgMnS	mít
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
"	"	kIx"	"
<g/>
duši	duše	k1gFnSc6	duše
<g/>
"	"	kIx"	"
člověka	člověk	k1gMnSc4	člověk
⇒	⇒	k?	⇒
první	první	k4xOgInPc4	první
pohřby	pohřeb	k1gInPc4	pohřeb
(	(	kIx(	(
<g/>
skrčená	skrčený	k2eAgFnSc1d1	skrčená
poloha	poloha	k1gFnSc1	poloha
<g/>
,	,	kIx,	,
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
zbraně	zbraň	k1gFnPc4	zbraň
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
<g/>
)	)	kIx)	)
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc1	schopnost
myšlení	myšlení	k1gNnSc2	myšlení
lidská	lidský	k2eAgFnSc1d1	lidská
osobnost	osobnost	k1gFnSc1	osobnost
rodina	rodina	k1gFnSc1	rodina
společenské	společenský	k2eAgInPc4d1	společenský
vztahy	vztah	k1gInPc4	vztah
práce	práce	k1gFnSc2	práce
odpočinek	odpočinek	k1gInSc1	odpočinek
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
plánování	plánování	k1gNnSc2	plánování
činností	činnost	k1gFnPc2	činnost
<g/>
,	,	kIx,	,
předávání	předávání	k1gNnSc1	předávání
znalostí	znalost	k1gFnPc2	znalost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
generací	generace	k1gFnPc2	generace
průběžně	průběžně	k6eAd1	průběžně
vylepšovat	vylepšovat	k5eAaImF	vylepšovat
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
technologie	technologie	k1gFnPc4	technologie
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
člověk	člověk	k1gMnSc1	člověk
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
je	on	k3xPp3gMnPc4	on
<g/>
:	:	kIx,	:
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
<g/>
:	:	kIx,	:
Kamenné	kamenný	k2eAgInPc1d1	kamenný
a	a	k8xC	a
kostěné	kostěný	k2eAgInPc1d1	kostěný
nože	nůž	k1gInPc1	nůž
Dokonalejší	dokonalý	k2eAgInPc1d2	dokonalejší
nástroje	nástroj	k1gInPc1	nástroj
<g/>
:	:	kIx,	:
hroty	hrot	k1gInPc1	hrot
<g/>
,	,	kIx,	,
škrabadla	škrabadlo	k1gNnPc1	škrabadlo
<g/>
,	,	kIx,	,
<g/>
rydla	rydlo	k1gNnPc1	rydlo
člověk	člověk	k1gMnSc1	člověk
dnešního	dnešní	k2eAgInSc2d1	dnešní
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
rozdělání	rozdělání	k1gNnSc2	rozdělání
ohně	oheň	k1gInSc2	oheň
výroba	výroba	k1gFnSc1	výroba
nových	nový	k2eAgInPc2d1	nový
lepších	dobrý	k2eAgInPc2d2	lepší
nástrojů	nástroj	k1gInPc2	nástroj
pomocí	pomocí	k7c2	pomocí
již	již	k6eAd1	již
existujících	existující	k2eAgInPc2d1	existující
nástrojů	nástroj	k1gInPc2	nástroj
zpracování	zpracování	k1gNnSc4	zpracování
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
kamene	kámen	k1gInSc2	kámen
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
výroby	výroba	k1gFnSc2	výroba
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
materiálů	materiál	k1gInPc2	materiál
vojenské	vojenský	k2eAgFnSc2d1	vojenská
technologie	technologie	k1gFnSc2	technologie
např.	např.	kA	např.
oštěpy	oštěp	k1gInPc1	oštěp
<g/>
,	,	kIx,	,
střelné	střelný	k2eAgFnPc1d1	střelná
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
tanky	tank	k1gInPc1	tank
<g/>
,	,	kIx,	,
stíhací	stíhací	k2eAgInPc1d1	stíhací
letouny	letoun	k1gInPc1	letoun
<g/>
,	,	kIx,	,
jaderné	jaderný	k2eAgFnPc1d1	jaderná
zbraně	zbraň	k1gFnPc1	zbraň
možnost	možnost	k1gFnSc4	možnost
přenášení	přenášení	k1gNnSc4	přenášení
a	a	k8xC	a
uchovávání	uchovávání	k1gNnSc4	uchovávání
informací	informace	k1gFnPc2	informace
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nástrojů	nástroj	k1gInPc2	nástroj
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
knihtisk	knihtisk	k1gInSc1	knihtisk
<g/>
,	,	kIx,	,
telefon	telefon	k1gInSc1	telefon
<g/>
,	,	kIx,	,
radio	radio	k1gNnSc1	radio
<g/>
,	,	kIx,	,
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
internet	internet	k1gInSc1	internet
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
lidský	lidský	k2eAgInSc4d1	lidský
genom	genom	k1gInSc4	genom
<g/>
.	.	kIx.	.
</s>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
genom	genom	k1gInSc1	genom
je	být	k5eAaImIp3nS	být
souhrn	souhrn	k1gInSc4	souhrn
veškeré	veškerý	k3xTgFnSc2	veškerý
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
zapsané	zapsaný	k2eAgFnPc4d1	zapsaná
v	v	k7c4	v
DNA	dno	k1gNnPc4	dno
uvnitř	uvnitř	k7c2	uvnitř
lidských	lidský	k2eAgFnPc2d1	lidská
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgMnSc1d1	současný
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgMnS	řadit
mezi	mezi	k7c4	mezi
eukaryotické	eukaryotický	k2eAgInPc4d1	eukaryotický
organizmy	organizmus	k1gInPc4	organizmus
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
DNA	dno	k1gNnPc4	dno
uvnitř	uvnitř	k7c2	uvnitř
buněčného	buněčný	k2eAgNnSc2d1	buněčné
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
ovšem	ovšem	k9	ovšem
i	i	k9	i
v	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
DNA	dna	k1gFnSc1	dna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
je	být	k5eAaImIp3nS	být
však	však	k9	však
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
nejvíce	hodně	k6eAd3	hodně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tělních	tělní	k2eAgFnPc6d1	tělní
buňkách	buňka	k1gFnPc6	buňka
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
diploidní	diploidní	k2eAgInSc4d1	diploidní
jaderný	jaderný	k2eAgInSc4d1	jaderný
genom	genom	k1gInSc4	genom
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
gen	gen	k1gInSc1	gen
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
dvakrát	dvakrát	k6eAd1	dvakrát
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
geny	gen	k1gInPc1	gen
na	na	k7c6	na
pohlavních	pohlavní	k2eAgInPc6d1	pohlavní
chromozomech	chromozom	k1gInPc6	chromozom
u	u	k7c2	u
muže	muž	k1gMnSc2	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
jaderná	jaderný	k2eAgFnSc1d1	jaderná
DNA	dna	k1gFnSc1	dna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
v	v	k7c6	v
haploidním	haploidní	k2eAgInSc6d1	haploidní
stavu	stav	k1gInSc6	stav
z	z	k7c2	z
3,2	[number]	k4	3,2
miliardy	miliarda	k4xCgFnSc2	miliarda
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
3,2	[number]	k4	3,2
Gbp	Gbp	k1gFnPc2	Gbp
<g/>
)	)	kIx)	)
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
genů	gen	k1gInPc2	gen
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
mimochodem	mimochodem	k9	mimochodem
počet	počet	k1gInSc1	počet
genů	gen	k1gInPc2	gen
srovnatelný	srovnatelný	k2eAgMnSc1d1	srovnatelný
například	například	k6eAd1	například
s	s	k7c7	s
hlísticí	hlístice	k1gFnSc7	hlístice
Caenorhabditis	Caenorhabditis	k1gFnSc1	Caenorhabditis
elegans	elegans	k6eAd1	elegans
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
prameny	pramen	k1gInPc1	pramen
udávají	udávat	k5eAaImIp3nP	udávat
pouhých	pouhý	k2eAgInPc2d1	pouhý
18	[number]	k4	18
000	[number]	k4	000
genů	gen	k1gInPc2	gen
<g/>
.	.	kIx.	.
</s>
<s>
Jaderný	jaderný	k2eAgInSc1d1	jaderný
genom	genom	k1gInSc1	genom
je	být	k5eAaImIp3nS	být
rozeset	rozesít	k5eAaPmNgInS	rozesít
po	po	k7c6	po
23	[number]	k4	23
párech	pár	k1gInPc6	pár
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jeden	jeden	k4xCgInSc1	jeden
pár	pár	k1gInSc1	pár
je	být	k5eAaImIp3nS	být
představován	představovat	k5eAaImNgInS	představovat
pohlavními	pohlavní	k2eAgInPc7d1	pohlavní
chromozomy	chromozom	k1gInPc7	chromozom
X	X	kA	X
a	a	k8xC	a
Y.	Y.	kA	Y.
Samotné	samotný	k2eAgInPc1d1	samotný
geny	gen	k1gInPc1	gen
kódující	kódující	k2eAgFnSc2d1	kódující
bílkoviny	bílkovina	k1gFnSc2	bílkovina
však	však	k9	však
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouhých	pouhý	k2eAgInPc2d1	pouhý
1,5	[number]	k4	1,5
<g/>
%	%	kIx~	%
genomu	genom	k1gInSc2	genom
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
představují	představovat	k5eAaImIp3nP	představovat
buď	buď	k8xC	buď
geny	gen	k1gInPc4	gen
kódující	kódující	k2eAgInPc4d1	kódující
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
různé	různý	k2eAgFnPc4d1	různá
regulační	regulační	k2eAgFnPc4d1	regulační
sekvence	sekvence	k1gFnPc4	sekvence
<g/>
,	,	kIx,	,
introny	intron	k1gInPc4	intron
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
junk	junko	k1gNnPc2	junko
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
odpadní	odpadní	k2eAgNnPc4d1	odpadní
<g/>
"	"	kIx"	"
DNA	dno	k1gNnPc4	dno
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
známé	známý	k2eAgFnSc2d1	známá
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
