<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
rysem	rys	k1gInSc7	rys
dnešního	dnešní	k2eAgMnSc2d1	dnešní
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc4	schopnost
vyrábět	vyrábět	k5eAaImF	vyrábět
komplexní	komplexní	k2eAgInPc4d1	komplexní
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
použít	použít	k5eAaPmF	použít
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
ovlivnění	ovlivnění	k1gNnSc3	ovlivnění
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
také	také	k9	také
jiní	jiný	k2eAgMnPc1d1	jiný
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
např.	např.	kA	např.
všichni	všechen	k3xTgMnPc1	všechen
žijící	žijící	k2eAgMnPc1d1	žijící
zástupci	zástupce	k1gMnPc1	zástupce
nadčeledi	nadčeleď	k1gFnSc2	nadčeleď
Hominoidea	Hominoidea	k1gMnSc1	Hominoidea
(	(	kIx(	(
<g/>
orangutan	orangutan	k1gMnSc1	orangutan
<g/>
,	,	kIx,	,
šimpanz	šimpanz	k1gMnSc1	šimpanz
<g/>
,	,	kIx,	,
gorila	gorila	k1gFnSc1	gorila
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umějí	umět	k5eAaImIp3nP	umět
použít	použít	k5eAaPmF	použít
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
nástroje	nástroj	k1gInPc1	nástroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
