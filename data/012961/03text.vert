<p>
<s>
Patentmotorwagen	Patentmotorwagen	k1gInSc1	Patentmotorwagen
Popp	Popp	k1gInSc1	Popp
je	být	k5eAaImIp3nS	být
švýcarský	švýcarský	k2eAgInSc4d1	švýcarský
automobil	automobil	k1gInSc4	automobil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Inženýr	inženýr	k1gMnSc1	inženýr
Lorenz	Lorenz	k1gMnSc1	Lorenz
Popp	Popp	k1gMnSc1	Popp
z	z	k7c2	z
Basileje	Basilej	k1gFnSc2	Basilej
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
importérem	importér	k1gMnSc7	importér
německých	německý	k2eAgInPc2d1	německý
vozů	vůz	k1gInPc2	vůz
Benz	Benza	k1gFnPc2	Benza
&	&	k?	&
Cie	Cie	k1gMnSc1	Cie
<g/>
.	.	kIx.	.
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
a	a	k8xC	a
1899	[number]	k4	1899
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
dva	dva	k4xCgInPc4	dva
automobily	automobil	k1gInPc4	automobil
za	za	k7c4	za
přispění	přispění	k1gNnSc4	přispění
finančníka	finančník	k1gMnSc2	finančník
Edouarda	Edouard	k1gMnSc2	Edouard
Burkhardta	Burkhardt	k1gMnSc2	Burkhardt
<g/>
.	.	kIx.	.
</s>
<s>
Lorenz	Lorenz	k1gMnSc1	Lorenz
Popp	Popp	k1gMnSc1	Popp
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zástupcem	zástupce	k1gMnSc7	zástupce
německé	německý	k2eAgFnSc2d1	německá
automobilky	automobilka	k1gFnSc2	automobilka
Stoewer	Stoewra	k1gFnPc2	Stoewra
a	a	k8xC	a
italského	italský	k2eAgInSc2d1	italský
Fiatu	fiat	k1gInSc2	fiat
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vozidla	vozidlo	k1gNnPc1	vozidlo
==	==	k?	==
</s>
</p>
<p>
<s>
Oba	dva	k4xCgInPc4	dva
vozy	vůz	k1gInPc4	vůz
Popp	Popp	k1gInSc1	Popp
byly	být	k5eAaImAgInP	být
navenek	navenek	k6eAd1	navenek
podobné	podobný	k2eAgInPc1d1	podobný
německým	německý	k2eAgMnPc3d1	německý
Benzům	Benz	k1gInPc3	Benz
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
modelu	model	k1gInSc2	model
Benz	Benz	k1gMnSc1	Benz
Velo	velo	k1gNnSc1	velo
<g/>
,	,	kIx,	,
karoserie	karoserie	k1gFnSc1	karoserie
<g/>
,	,	kIx,	,
motor	motor	k1gInSc1	motor
a	a	k8xC	a
mechanické	mechanický	k2eAgInPc1d1	mechanický
díly	díl	k1gInPc1	díl
ale	ale	k8xC	ale
byly	být	k5eAaImAgInP	být
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
Typ	typ	k1gInSc1	typ
7	[number]	k4	7
CV	CV	kA	CV
měl	mít	k5eAaImAgInS	mít
vzadu	vzadu	k6eAd1	vzadu
uložený	uložený	k2eAgInSc1d1	uložený
čtyřtaktní	čtyřtaktní	k2eAgInSc1d1	čtyřtaktní
dvouválcový	dvouválcový	k2eAgInSc1d1	dvouválcový
<g/>
,	,	kIx,	,
vodou	voda	k1gFnSc7	voda
chlazený	chlazený	k2eAgInSc1d1	chlazený
plochý	plochý	k2eAgInSc1d1	plochý
motor	motor	k1gInSc1	motor
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
objemem	objem	k1gInSc7	objem
1594	[number]	k4	1594
cm3	cm3	k4	cm3
(	(	kIx(	(
<g/>
vrtání	vrtání	k1gNnSc2	vrtání
90	[number]	k4	90
mm	mm	kA	mm
<g/>
,	,	kIx,	,
zdvih	zdvih	k1gInSc1	zdvih
122,5	[number]	k4	122,5
mm	mm	kA	mm
<g/>
)	)	kIx)	)
dával	dávat	k5eAaImAgInS	dávat
výkon	výkon	k1gInSc4	výkon
5,2	[number]	k4	5,2
kW	kW	kA	kW
(	(	kIx(	(
<g/>
7	[number]	k4	7
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sací	sací	k2eAgInPc1d1	sací
ventily	ventil	k1gInPc1	ventil
byly	být	k5eAaImAgInP	být
samočinné	samočinný	k2eAgInPc4d1	samočinný
<g/>
,	,	kIx,	,
výfukové	výfukový	k2eAgInPc4d1	výfukový
ovládala	ovládat	k5eAaImAgFnS	ovládat
vačková	vačkový	k2eAgFnSc1d1	vačková
hřídel	hřídel	k1gFnSc1	hřídel
uložená	uložený	k2eAgFnSc1d1	uložená
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
(	(	kIx(	(
<g/>
OHC	OHC	kA	OHC
<g/>
)	)	kIx)	)
a	a	k8xC	a
poháněná	poháněný	k2eAgNnPc4d1	poháněné
nekrytým	krytý	k2eNgInSc7d1	nekrytý
řetězem	řetěz	k1gInSc7	řetěz
<g/>
.	.	kIx.	.
</s>
<s>
Zapalování	zapalování	k1gNnSc1	zapalování
pomocí	pomocí	k7c2	pomocí
žárových	žárový	k2eAgFnPc2d1	Žárová
trubic	trubice	k1gFnPc2	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Karburátor	karburátor	k1gInSc1	karburátor
byl	být	k5eAaImAgInS	být
odpařovací	odpařovací	k2eAgMnSc1d1	odpařovací
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
pohonu	pohon	k1gInSc2	pohon
byl	být	k5eAaImAgInS	být
řešen	řešit	k5eAaImNgInS	řešit
řemeny	řemen	k1gInPc1	řemen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nahrazovaly	nahrazovat	k5eAaImAgInP	nahrazovat
i	i	k9	i
spojku	spojka	k1gFnSc4	spojka
<g/>
.	.	kIx.	.
</s>
<s>
Výkon	výkon	k1gInSc1	výkon
motoru	motor	k1gInSc2	motor
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
předlohy	předloha	k1gFnSc2	předloha
přenášen	přenášen	k2eAgInSc1d1	přenášen
řetězem	řetěz	k1gInSc7	řetěz
na	na	k7c4	na
zadní	zadní	k2eAgFnSc4d1	zadní
nápravu	náprava	k1gFnSc4	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
vozu	vůz	k1gInSc2	vůz
byla	být	k5eAaImAgFnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
700	[number]	k4	700
kg	kg	kA	kg
<g/>
,	,	kIx,	,
maimální	maimální	k2eAgFnSc1d1	maimální
rychlost	rychlost	k1gFnSc1	rychlost
kolem	kolem	k7c2	kolem
40	[number]	k4	40
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgInSc1	jeden
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
automobil	automobil	k1gInSc1	automobil
7	[number]	k4	7
CV	CV	kA	CV
je	být	k5eAaImIp3nS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c4	v
Lucernu	lucerna	k1gFnSc4	lucerna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
automobilem	automobil	k1gInSc7	automobil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
jej	on	k3xPp3gMnSc4	on
muzeu	muzeum	k1gNnSc6	muzeum
věnoval	věnovat	k5eAaPmAgMnS	věnovat
Fritz	Fritz	k1gMnSc1	Fritz
Scheidegger	Scheidegger	k1gMnSc1	Scheidegger
z	z	k7c2	z
Curychu	Curych	k1gInSc2	Curych
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Popp	Popp	k1gInSc1	Popp
(	(	kIx(	(
<g/>
Automobilhersteller	Automobilhersteller	k1gInSc1	Automobilhersteller
<g/>
)	)	kIx)	)
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Harald	Harald	k1gMnSc1	Harald
H.	H.	kA	H.
Linz	Linz	k1gMnSc1	Linz
<g/>
,	,	kIx,	,
Halwart	Halwart	k1gInSc1	Halwart
Schrader	Schrader	k1gInSc1	Schrader
<g/>
:	:	kIx,	:
Die	Die	k1gFnSc1	Die
große	große	k1gFnSc1	große
Automobil-Enzyklopädie	Automobil-Enzyklopädie	k1gFnSc1	Automobil-Enzyklopädie
<g/>
.	.	kIx.	.
</s>
<s>
BLV	BLV	kA	BLV
<g/>
,	,	kIx,	,
München	München	k1gInSc1	München
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-405-12974-5	[number]	k4	3-405-12974-5
</s>
</p>
<p>
<s>
G.	G.	kA	G.
N.	N.	kA	N.
Georgano	Georgana	k1gFnSc5	Georgana
<g/>
:	:	kIx,	:
Autos	Autosa	k1gFnPc2	Autosa
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopédie	Encyclopédie	k1gFnSc1	Encyclopédie
complè	complè	k?	complè
<g/>
.	.	kIx.	.
1885	[number]	k4	1885
à	à	k?	à
nos	nos	k1gInSc1	nos
jours	jours	k1gInSc1	jours
<g/>
.	.	kIx.	.
</s>
<s>
Courtille	Courtille	k1gNnSc1	Courtille
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
</s>
</p>
<p>
<s>
Ernest	Ernest	k1gMnSc1	Ernest
Schmid	Schmid	k1gInSc1	Schmid
<g/>
:	:	kIx,	:
Schweizer	Schweizer	k1gMnSc1	Schweizer
Autos	Autos	k1gMnSc1	Autos
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
schweizerischen	schweizerischen	k2eAgInSc1d1	schweizerischen
Automobilkonstruktionen	Automobilkonstruktionen	k2eAgInSc1d1	Automobilkonstruktionen
von	von	k1gInSc1	von
1868	[number]	k4	1868
bis	bis	k?	bis
heute	heute	k5eAaPmIp2nP	heute
<g/>
.	.	kIx.	.
</s>
<s>
Auto-Jahr	Auto-Jahr	k1gInSc1	Auto-Jahr
<g/>
,	,	kIx,	,
Lausanne	Lausanne	k1gNnSc1	Lausanne
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
2-88001-058-6	[number]	k4	2-88001-058-6
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Popp	Popp	k1gInSc1	Popp
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
GTÜ	GTÜ	kA	GTÜ
Gesellschaft	Gesellschaft	k1gMnSc1	Gesellschaft
für	für	k?	für
Technische	Technische	k1gInSc1	Technische
Überwachung	Überwachung	k1gInSc1	Überwachung
mbH	mbH	k?	mbH
(	(	kIx(	(
<g/>
navštíveno	navštívit	k5eAaPmNgNnS	navštívit
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Töfftöff	Töfftöff	k1gInSc1	Töfftöff
<g/>
;	;	kIx,	;
als	als	k?	als
das	das	k?	das
Automobil	automobil	k1gInSc1	automobil
in	in	k?	in
Basel	Basel	k1gInSc1	Basel
Einzug	Einzug	k1gMnSc1	Einzug
hielt	hielt	k1gMnSc1	hielt
</s>
</p>
