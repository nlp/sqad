<p>
<s>
Český	český	k2eAgInSc1d1	český
les	les	k1gInSc1	les
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
na	na	k7c6	na
bavorské	bavorský	k2eAgFnSc6d1	bavorská
straně	strana	k1gFnSc6	strana
Oberpfälzer	Oberpfälzra	k1gFnPc2	Oberpfälzra
Wald	Wald	k1gInSc1	Wald
–	–	k?	–
Hornofalcký	Hornofalcký	k2eAgInSc1d1	Hornofalcký
les	les	k1gInSc1	les
nebo	nebo	k8xC	nebo
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
straně	strana	k1gFnSc6	strana
Böhmischer	Böhmischra	k1gFnPc2	Böhmischra
Wald	Wald	k1gInSc1	Wald
–	–	k?	–
Český	český	k2eAgInSc1d1	český
les	les	k1gInSc1	les
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
členitá	členitý	k2eAgFnSc1d1	členitá
vrchovina	vrchovina	k1gFnSc1	vrchovina
a	a	k8xC	a
geomorfologický	geomorfologický	k2eAgInSc1d1	geomorfologický
celek	celek	k1gInSc1	celek
podél	podél	k7c2	podél
česko-německé	českoěmecký	k2eAgFnSc2d1	česko-německá
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
–	–	k?	–
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
tak	tak	k9	tak
do	do	k7c2	do
jihozápadních	jihozápadní	k2eAgFnPc2d1	jihozápadní
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
do	do	k7c2	do
východního	východní	k2eAgNnSc2d1	východní
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
leží	ležet	k5eAaImIp3nS	ležet
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
Českého	český	k2eAgInSc2d1	český
lesa	les	k1gInSc2	les
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vymezení	vymezení	k1gNnSc2	vymezení
==	==	k?	==
</s>
</p>
<p>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
Chebské	chebský	k2eAgFnSc2d1	Chebská
pánve	pánev	k1gFnSc2	pánev
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
až	až	k9	až
po	po	k7c4	po
Všerubskou	všerubský	k2eAgFnSc4d1	Všerubská
vrchovinu	vrchovina	k1gFnSc4	vrchovina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Český	český	k2eAgInSc4d1	český
les	les	k1gInSc4	les
od	od	k7c2	od
Šumavy	Šumava	k1gFnSc2	Šumava
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
pohoří	pohoří	k1gNnPc2	pohoří
je	být	k5eAaImIp3nS	být
80	[number]	k4	80
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
Českého	český	k2eAgInSc2d1	český
lesa	les	k1gInSc2	les
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
rázem	ráz	k1gInSc7	ráz
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
Šumavě	Šumava	k1gFnSc3	Šumava
(	(	kIx(	(
<g/>
tvary	tvar	k1gInPc1	tvar
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
lesnatostí	lesnatost	k1gFnSc7	lesnatost
<g/>
,	,	kIx,	,
řídkým	řídký	k2eAgNnSc7d1	řídké
osídlením	osídlení	k1gNnSc7	osídlení
jako	jako	k8xC	jako
důsledkem	důsledek	k1gInSc7	důsledek
vysídlení	vysídlení	k1gNnSc2	vysídlení
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
o	o	k7c4	o
300	[number]	k4	300
<g/>
–	–	k?	–
<g/>
500	[number]	k4	500
m	m	kA	m
nižší	nízký	k2eAgFnPc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
vrchů	vrch	k1gInPc2	vrch
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
600	[number]	k4	600
do	do	k7c2	do
1042	[number]	k4	1042
m.	m.	k?	m.
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
Čerchov	Čerchov	k1gInSc1	Čerchov
(	(	kIx(	(
<g/>
1	[number]	k4	1
042	[number]	k4	042
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Čerchova	Čerchův	k2eAgInSc2d1	Čerchův
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Kurzova	Kurzův	k2eAgFnSc1d1	Kurzova
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
,	,	kIx,	,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Vilému	Vilém	k1gMnSc3	Vilém
Kurzovi	Kurz	k1gMnSc3	Kurz
<g/>
,	,	kIx,	,
jednom	jeden	k4xCgMnSc6	jeden
za	za	k7c2	za
zakladatelů	zakladatel	k1gMnPc2	zakladatel
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geomorfologické	geomorfologický	k2eAgNnSc4d1	Geomorfologické
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
les	les	k1gInSc1	les
je	být	k5eAaImIp3nS	být
samostatným	samostatný	k2eAgInSc7d1	samostatný
geomorfologickým	geomorfologický	k2eAgInSc7d1	geomorfologický
celkem	celek	k1gInSc7	celek
masivu	masiv	k1gInSc2	masiv
Šumavské	šumavský	k2eAgFnSc2d1	Šumavská
subprovincie	subprovincie	k1gFnSc2	subprovincie
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgNnSc1d1	kompletní
geomorfologické	geomorfologický	k2eAgNnSc1d1	Geomorfologické
členění	členění	k1gNnSc1	členění
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
hor	hora	k1gFnPc2	hora
uvádí	uvádět	k5eAaImIp3nS	uvádět
následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Geologie	geologie	k1gFnSc2	geologie
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
Český	český	k2eAgInSc4d1	český
les	les	k1gInSc4	les
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
ploché	plochý	k2eAgInPc1d1	plochý
hřbety	hřbet	k1gInPc1	hřbet
a	a	k8xC	a
široká	široký	k2eAgNnPc1d1	široké
mělká	mělký	k2eAgNnPc1d1	mělké
údolí	údolí	k1gNnPc1	údolí
s	s	k7c7	s
občasně	občasně	k6eAd1	občasně
vystupujícími	vystupující	k2eAgInPc7d1	vystupující
suky	suk	k1gInPc7	suk
a	a	k8xC	a
strukturními	strukturní	k2eAgInPc7d1	strukturní
hřbety	hřbet	k1gInPc7	hřbet
s	s	k7c7	s
tvary	tvar	k1gInPc7	tvar
zvětrávání	zvětrávání	k1gNnSc2	zvětrávání
a	a	k8xC	a
odnosu	odnos	k1gInSc2	odnos
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
les	les	k1gInSc1	les
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
rulami	rula	k1gFnPc7	rula
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
granity	granit	k1gInPc1	granit
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInPc1d1	východní
svahy	svah	k1gInPc1	svah
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
výskytu	výskyt	k1gInSc2	výskyt
zlomových	zlomový	k2eAgFnPc2d1	zlomová
linií	linie	k1gFnPc2	linie
příkřejší	příkrý	k2eAgFnSc1d2	příkřejší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
přírody	příroda	k1gFnSc2	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Českého	český	k2eAgInSc2d1	český
lesa	les	k1gInSc2	les
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
zřízena	zřídit	k5eAaPmNgFnS	zřídit
CHKO	CHKO	kA	CHKO
Český	český	k2eAgInSc1d1	český
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
menších	malý	k2eAgNnPc2d2	menší
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Lesní	lesní	k2eAgInPc1d1	lesní
porosty	porost	k1gInPc1	porost
zaujímající	zaujímající	k2eAgFnSc4d1	zaujímající
většinu	většina	k1gFnSc4	většina
plochy	plocha	k1gFnSc2	plocha
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořen	k2eAgInPc1d1	tvořen
převážně	převážně	k6eAd1	převážně
smrkovými	smrkový	k2eAgFnPc7d1	smrková
monokulturami	monokultura	k1gFnPc7	monokultura
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnPc1d1	původní
jedlobučiny	jedlobučina	k1gFnPc1	jedlobučina
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
pouze	pouze	k6eAd1	pouze
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
Naturpark	Naturpark	k1gInSc4	Naturpark
Oberpfälzer	Oberpfälzer	k1gInSc1	Oberpfälzer
Wald	Wald	k1gInSc1	Wald
a	a	k8xC	a
Naturpark	Naturpark	k1gInSc1	Naturpark
Nördlicher	Nördlichra	k1gFnPc2	Nördlichra
Oberpfälzer	Oberpfälzer	k1gMnSc1	Oberpfälzer
Wald	Wald	k1gMnSc1	Wald
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vrcholy	vrchol	k1gInPc4	vrchol
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
lese	les	k1gInSc6	les
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
tisícovky	tisícovka	k1gFnPc1	tisícovka
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
straně	strana	k1gFnSc6	strana
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Čerchov	Čerchov	k1gInSc1	Čerchov
(	(	kIx(	(
<g/>
1042	[number]	k4	1042
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Skalka	skalka	k1gFnSc1	skalka
(	(	kIx(	(
<g/>
1005	[number]	k4	1005
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
vrcholy	vrchol	k1gInPc1	vrchol
měří	měřit	k5eAaImIp3nP	měřit
méně	málo	k6eAd2	málo
než	než	k8xS	než
1000	[number]	k4	1000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
skála	skála	k1gFnSc1	skála
(	(	kIx(	(
<g/>
968	[number]	k4	968
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Dyleň	Dyleň	k1gFnSc1	Dyleň
(	(	kIx(	(
<g/>
939	[number]	k4	939
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Gibacht	Gibacht	k1gInSc1	Gibacht
(	(	kIx(	(
<g/>
938	[number]	k4	938
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Smrčí	smrčí	k1gNnSc1	smrčí
(	(	kIx(	(
<g/>
935	[number]	k4	935
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Entenbühl	Entenbühnout	k5eAaPmAgMnS	Entenbühnout
(	(	kIx(	(
<g/>
901	[number]	k4	901
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Weingartenfels	Weingartenfels	k1gInSc1	Weingartenfels
(	(	kIx(	(
<g/>
896	[number]	k4	896
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Havran	Havran	k1gMnSc1	Havran
(	(	kIx(	(
<g/>
894	[number]	k4	894
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Signalberg	Signalberg	k1gInSc1	Signalberg
(	(	kIx(	(
<g/>
888	[number]	k4	888
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Škarmanka	Škarmanka	k1gFnSc1	Škarmanka
(	(	kIx(	(
<g/>
888	[number]	k4	888
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Haltrava	Haltrava	k1gFnSc1	Haltrava
(	(	kIx(	(
<g/>
882	[number]	k4	882
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Reichenstein	Reichenstein	k1gInSc1	Reichenstein
(	(	kIx(	(
<g/>
874	[number]	k4	874
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
Zvon	zvon	k1gInSc1	zvon
(	(	kIx(	(
<g/>
856	[number]	k4	856
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Sádek	sádek	k1gInSc1	sádek
(	(	kIx(	(
<g/>
853	[number]	k4	853
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Přimda	Přimda	k1gFnSc1	Přimda
(	(	kIx(	(
<g/>
848	[number]	k4	848
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
na	na	k7c6	na
území	území	k1gNnSc6	území
Českého	český	k2eAgInSc2d1	český
lesa	les	k1gInSc2	les
jsou	být	k5eAaImIp3nP	být
rozptýlené	rozptýlený	k2eAgInPc1d1	rozptýlený
a	a	k8xC	a
vzácné	vzácný	k2eAgInPc1d1	vzácný
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
knížecí	knížecí	k2eAgFnSc2d1	knížecí
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
nejmladšího	mladý	k2eAgInSc2d3	nejmladší
paleolitu	paleolit	k1gInSc2	paleolit
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
10	[number]	k4	10
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
pozdější	pozdní	k2eAgFnSc2d2	pozdější
obce	obec	k1gFnSc2	obec
Labuť	labuť	k1gFnSc1	labuť
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Sedmihoří	Sedmihoří	k1gNnSc2	Sedmihoří
v	v	k7c6	v
době	doba	k1gFnSc6	doba
antické	antický	k2eAgNnSc4d1	antické
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
ale	ale	k8xC	ale
zabíral	zabírat	k5eAaImAgInS	zabírat
těžko	těžko	k6eAd1	těžko
prostupný	prostupný	k2eAgInSc1d1	prostupný
<g/>
,	,	kIx,	,
hluboký	hluboký	k2eAgInSc1d1	hluboký
les	les	k1gInSc1	les
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
systematičtější	systematický	k2eAgNnSc1d2	systematičtější
osidlování	osidlování	k1gNnSc1	osidlování
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgNnSc1d1	spojené
odlesnění	odlesnění	k1gNnSc1	odlesnění
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
klášterů	klášter	k1gInPc2	klášter
v	v	k7c6	v
Kladrubech	Kladruby	k1gInPc6	Kladruby
<g/>
,	,	kIx,	,
Teplé	Teplé	k2eAgFnPc1d1	Teplé
a	a	k8xC	a
ve	v	k7c4	v
Waldsassenu	Waldsassen	k2eAgFnSc4d1	Waldsassen
ve	v	k7c4	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turistika	turistika	k1gFnSc1	turistika
==	==	k?	==
</s>
</p>
<p>
<s>
Výchozími	výchozí	k2eAgNnPc7d1	výchozí
místy	místo	k1gNnPc7	místo
pro	pro	k7c4	pro
turistiku	turistika	k1gFnSc4	turistika
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
lese	les	k1gInSc6	les
jsou	být	k5eAaImIp3nP	být
Tachov	Tachov	k1gInSc4	Tachov
<g/>
,	,	kIx,	,
Rozvadov	Rozvadov	k1gInSc1	Rozvadov
<g/>
,	,	kIx,	,
Přimda	Přimdo	k1gNnPc1	Přimdo
<g/>
,	,	kIx,	,
Poběžovice	Poběžovice	k1gFnPc1	Poběžovice
a	a	k8xC	a
Klenčí	Klenčí	k1gNnSc1	Klenčí
pod	pod	k7c7	pod
Čerchovem	Čerchov	k1gInSc7	Čerchov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Český	český	k2eAgInSc4d1	český
les	les	k1gInSc4	les
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Rozbor	rozbor	k1gInSc1	rozbor
Českého	český	k2eAgInSc2d1	český
lesa	les	k1gInSc2	les
na	na	k7c6	na
státních	státní	k2eAgFnPc6d1	státní
hranicích	hranice	k1gFnPc6	hranice
a	a	k8xC	a
v	v	k7c6	v
pohraničí	pohraničí	k1gNnSc6	pohraničí
</s>
</p>
