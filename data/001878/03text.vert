<s>
Trypanosoma	trypanosoma	k1gFnSc1	trypanosoma
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
také	také	k9	také
trypanozoma	trypanozoma	k1gFnSc1	trypanozoma
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zdravotnicky	zdravotnicky	k6eAd1	zdravotnicky
významný	významný	k2eAgInSc1d1	významný
rod	rod	k1gInSc1	rod
parazitických	parazitický	k2eAgMnPc2d1	parazitický
prvoků	prvok	k1gMnPc2	prvok
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Euglenozoa	Euglenozoum	k1gNnSc2	Euglenozoum
<g/>
,	,	kIx,	,
třídy	třída	k1gFnSc2	třída
bičivky	bičivka	k1gFnSc2	bičivka
a	a	k8xC	a
řádu	řád	k1gInSc2	řád
trypanozomy	trypanozoma	k1gFnSc2	trypanozoma
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc7	jejich
hostiteli	hostitel	k1gMnPc7	hostitel
jsou	být	k5eAaImIp3nP	být
různí	různý	k2eAgMnPc1d1	různý
obratlovci	obratlovec	k1gMnPc1	obratlovec
a	a	k8xC	a
přenáší	přenášet	k5eAaImIp3nP	přenášet
je	on	k3xPp3gFnPc4	on
zejména	zejména	k9	zejména
dvoukřídlý	dvoukřídlý	k2eAgInSc4d1	dvoukřídlý
hmyz	hmyz	k1gInSc4	hmyz
a	a	k8xC	a
ploštice	ploštice	k1gFnPc4	ploštice
<g/>
.	.	kIx.	.
</s>
<s>
Trypanozomy	trypanozoma	k1gFnPc1	trypanozoma
jsou	být	k5eAaImIp3nP	být
heterotrofní	heterotrofní	k2eAgInPc4d1	heterotrofní
organismy	organismus	k1gInPc4	organismus
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
další	další	k2eAgFnSc1d1	další
Mastigophora	Mastigophora	k1gFnSc1	Mastigophora
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
stavbu	stavba	k1gFnSc4	stavba
bičíku	bičík	k1gInSc2	bičík
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ostatní	ostatní	k2eAgFnSc1d1	ostatní
Kinetoplastida	Kinetoplastida	k1gFnSc1	Kinetoplastida
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
mitochondrii	mitochondrie	k1gFnSc6	mitochondrie
kinetoplast	kinetoplast	k1gInSc4	kinetoplast
<g/>
.	.	kIx.	.
</s>
<s>
Buňka	buňka	k1gFnSc1	buňka
trypanozom	trypanozoma	k1gFnPc2	trypanozoma
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
životním	životní	k2eAgInSc6d1	životní
cyklu	cyklus	k1gInSc6	cyklus
a	a	k8xC	a
umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hostitelského	hostitelský	k2eAgInSc2d1	hostitelský
organismu	organismus	k1gInSc2	organismus
měnit	měnit	k5eAaImF	měnit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
morfologické	morfologický	k2eAgInPc4d1	morfologický
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
trypomastigot	trypomastigot	k1gMnSc1	trypomastigot
<g/>
,	,	kIx,	,
epimastigot	epimastigot	k1gMnSc1	epimastigot
a	a	k8xC	a
amastigot	amastigot	k1gMnSc1	amastigot
<g/>
.	.	kIx.	.
</s>
<s>
Pozice	pozice	k1gFnSc1	pozice
a	a	k8xC	a
orientace	orientace	k1gFnSc1	orientace
kinetoplastu	kinetoplast	k1gInSc2	kinetoplast
je	být	k5eAaImIp3nS	být
klíčovým	klíčový	k2eAgInSc7d1	klíčový
znakem	znak	k1gInSc7	znak
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
tři	tři	k4xCgFnPc1	tři
formy	forma	k1gFnPc1	forma
dají	dát	k5eAaPmIp3nP	dát
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavými	zajímavý	k2eAgFnPc7d1	zajímavá
strukturami	struktura	k1gFnPc7	struktura
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
jsou	být	k5eAaImIp3nP	být
kalcizomy	kalcizom	k1gInPc1	kalcizom
a	a	k8xC	a
rezervozomy	rezervozom	k1gInPc1	rezervozom
<g/>
.	.	kIx.	.
</s>
<s>
Trypanosoma	trypanosoma	k1gFnSc1	trypanosoma
brucei	bruce	k1gFnSc2	bruce
-	-	kIx~	-
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
onemocnění	onemocnění	k1gNnSc1	onemocnění
nagana	nagana	k1gFnSc1	nagana
<g/>
,	,	kIx,	,
přenáší	přenášet	k5eAaImIp3nS	přenášet
Glossina	Glossina	k1gFnSc1	Glossina
morsitans	morsitansa	k1gFnPc2	morsitansa
Trypanosoma	trypanosoma	k1gFnSc1	trypanosoma
cruzi	cruze	k1gFnSc4	cruze
-	-	kIx~	-
Chagasova	Chagasův	k2eAgFnSc1d1	Chagasova
choroba	choroba	k1gFnSc1	choroba
<g/>
,	,	kIx,	,
přenáší	přenášet	k5eAaImIp3nP	přenášet
tropické	tropický	k2eAgFnPc1d1	tropická
krevsající	krevsající	k2eAgFnPc1d1	krevsající
ploštice	ploštice	k1gFnPc1	ploštice
Trypanosoma	trypanosoma	k1gFnSc1	trypanosoma
gambiense	gambiense	k1gFnSc1	gambiense
-	-	kIx~	-
spavá	spavý	k2eAgFnSc1d1	spavá
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
přenáší	přenášet	k5eAaImIp3nS	přenášet
Glossina	Glossin	k2eAgFnSc1d1	Glossina
gambiense	gambiense	k1gFnSc1	gambiense
</s>
