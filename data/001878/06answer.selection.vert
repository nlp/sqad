<s>
Trypanozomy	trypanozoma	k1gFnPc1	trypanozoma
jsou	být	k5eAaImIp3nP	být
heterotrofní	heterotrofní	k2eAgInPc4d1	heterotrofní
organismy	organismus	k1gInPc4	organismus
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
další	další	k2eAgFnSc1d1	další
Mastigophora	Mastigophora	k1gFnSc1	Mastigophora
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
stavbu	stavba	k1gFnSc4	stavba
bičíku	bičík	k1gInSc2	bičík
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ostatní	ostatní	k2eAgFnSc1d1	ostatní
Kinetoplastida	Kinetoplastida	k1gFnSc1	Kinetoplastida
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
mitochondrii	mitochondrie	k1gFnSc6	mitochondrie
kinetoplast	kinetoplast	k1gInSc4	kinetoplast
<g/>
.	.	kIx.	.
</s>
