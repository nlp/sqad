<s>
Vostok	Vostok	k1gInSc1	Vostok
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
С	С	k?	С
В	В	k?	В
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ruská	ruský	k2eAgFnSc1d1	ruská
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
stanice	stanice	k1gFnSc1	stanice
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
nedostupnosti	nedostupnost	k1gFnSc2	nedostupnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
činí	činit	k5eAaImIp3nS	činit
nejizolovanější	izolovaný	k2eAgFnSc4d3	nejizolovanější
výzkumnou	výzkumný	k2eAgFnSc4d1	výzkumná
stanici	stanice	k1gFnSc4	stanice
na	na	k7c6	na
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
3488	[number]	k4	3488
m.	m.	k?	m.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
zde	zde	k6eAd1	zde
naměřili	naměřit	k5eAaBmAgMnP	naměřit
absolutně	absolutně	k6eAd1	absolutně
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
teplotu	teplota	k1gFnSc4	teplota
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
Poloha	poloha	k1gFnSc1	poloha
stanice	stanice	k1gFnSc1	stanice
poblíž	poblíž	k7c2	poblíž
jižního	jižní	k2eAgInSc2d1	jižní
magnetického	magnetický	k2eAgInSc2d1	magnetický
pólu	pól	k1gInSc2	pól
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
ideální	ideální	k2eAgNnSc4d1	ideální
místo	místo	k1gNnSc4	místo
ke	k	k7c3	k
zkoumání	zkoumání	k1gNnSc3	zkoumání
zemské	zemský	k2eAgFnSc2d1	zemská
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
geofyzikou	geofyzika	k1gFnSc7	geofyzika
<g/>
,	,	kIx,	,
medicínou	medicína	k1gFnSc7	medicína
a	a	k8xC	a
klimatologii	klimatologie	k1gFnSc6	klimatologie
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
a	a	k8xC	a
nepřetržitě	přetržitě	k6eNd1	přetržitě
fungovala	fungovat	k5eAaImAgFnS	fungovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
dočasně	dočasně	k6eAd1	dočasně
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
stanici	stanice	k1gFnSc4	stanice
využívají	využívat	k5eAaImIp3nP	využívat
vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zásobována	zásobovat	k5eAaImNgFnS	zásobovat
pomocí	pomocí	k7c2	pomocí
speciálních	speciální	k2eAgInPc2d1	speciální
konvojů	konvoj	k1gInPc2	konvoj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
jednou	jednou	k6eAd1	jednou
ročně	ročně	k6eAd1	ročně
dovážejí	dovážet	k5eAaImIp3nP	dovážet
potraviny	potravina	k1gFnPc4	potravina
<g/>
,	,	kIx,	,
palivo	palivo	k1gNnSc4	palivo
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
materiál	materiál	k1gInSc4	materiál
ze	z	k7c2	z
1410	[number]	k4	1410
kilometrů	kilometr	k1gInPc2	kilometr
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
polární	polární	k2eAgFnSc2d1	polární
stanice	stanice	k1gFnSc2	stanice
Mirnyj	Mirnyj	k1gFnSc1	Mirnyj
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
stanicí	stanice	k1gFnSc7	stanice
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
4000	[number]	k4	4000
m	m	kA	m
jezero	jezero	k1gNnSc1	jezero
Vostok	Vostok	k1gInSc1	Vostok
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc1d3	veliký
známé	známý	k2eAgNnSc1d1	známé
subglaciální	subglaciální	k2eAgNnSc1d1	subglaciální
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
je	být	k5eAaImIp3nS	být
14	[number]	k4	14
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Zeměpisné	zeměpisný	k2eAgInPc1d1	zeměpisný
rekordy	rekord	k1gInPc1	rekord
světa	svět	k1gInSc2	svět
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vostok	Vostok	k1gInSc1	Vostok
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
