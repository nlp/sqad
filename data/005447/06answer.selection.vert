<s>
Vostok	Vostok	k1gInSc1	Vostok
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
<g/>
С	С	k?	С
В	В	k?	В
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ruská	ruský	k2eAgFnSc1d1	ruská
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
stanice	stanice	k1gFnSc1	stanice
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
nedostupnosti	nedostupnost	k1gFnSc2	nedostupnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
činí	činit	k5eAaImIp3nS	činit
nejizolovanější	izolovaný	k2eAgFnSc4d3	nejizolovanější
výzkumnou	výzkumný	k2eAgFnSc4d1	výzkumná
stanici	stanice	k1gFnSc4	stanice
na	na	k7c6	na
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
