<s>
Michelle	Michelle	k1gFnSc1
Dockery	Dockera	k1gFnSc2
</s>
<s>
Michelle	Michelle	k1gInSc1
Dockery	Dockera	k1gFnSc2
Dockery	Dockera	k1gFnSc2
na	na	k7c6
předávání	předávání	k1gNnSc6
cen	cena	k1gFnPc2
Zlatých	zlatý	k2eAgInPc2d1
glóbusů	glóbus	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
Rodné	rodný	k2eAgInPc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Michelle	Michell	k1gMnSc5
Suzanne	Suzann	k1gMnSc5
Dockery	Docker	k1gMnPc4
Narození	narození	k1gNnPc4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1981	#num#	k4
(	(	kIx(
<g/>
39	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1
a	a	k8xC
dramatická	dramatický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
v	v	k7c6
Guildhallu	Guildhallo	k1gNnSc6
(	(	kIx(
<g/>
do	do	k7c2
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
Chadwell	Chadwell	k1gMnSc1
Heath	Heath	k1gMnSc1
Academy	Academa	k1gFnSc2
Aktivní	aktivní	k2eAgInPc4d1
roky	rok	k1gInPc4
</s>
<s>
2004	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Josh	Josh	k1gInSc1
Dineen	Dineen	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Michelle	Michell	k1gFnSc1
Suzanne	Suzanne	k1gFnSc1
Dockeryová	Dockeryová	k1gFnSc1
(	(	kIx(
<g/>
nar	nar	kA
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1981	#num#	k4
<g/>
,	,	kIx,
Romford	Romford	k1gInSc1
<g/>
,	,	kIx,
Essex	Essex	k1gInSc1
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
britská	britský	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
a	a	k8xC
zpěvačka	zpěvačka	k1gFnSc1
žijící	žijící	k2eAgFnSc1d1
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proslavila	proslavit	k5eAaPmAgFnS
se	se	k3xPyFc4
díky	díky	k7c3
historickému	historický	k2eAgInSc3d1
seriálu	seriál	k1gInSc3
stanice	stanice	k1gFnSc1
ITV	ITV	kA
Panství	panství	k1gNnSc1
Downton	Downton	k1gInSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
výkon	výkon	k1gInSc4
v	v	k7c6
seriálu	seriál	k1gInSc6
získala	získat	k5eAaPmAgFnS
nominaci	nominace	k1gFnSc4
na	na	k7c4
Zlatý	zlatý	k2eAgInSc4d1
glóbus	glóbus	k1gInSc4
a	a	k8xC
tři	tři	k4xCgFnPc4
ceny	cena	k1gFnPc4
Emmy	Emma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
divadelních	divadelní	k2eAgNnPc6d1
prknech	prkno	k1gNnPc6
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
objevila	objevit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
s	s	k7c7
divadelní	divadelní	k2eAgFnSc7d1
hrou	hra	k1gFnSc7
Jeho	jeho	k3xOp3gFnPc4
temné	temný	k2eAgFnPc4d1
esence	esence	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
si	se	k3xPyFc3
zahrála	zahrát	k5eAaPmAgFnS
v	v	k7c6
Pygmalionu	Pygmalion	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
výkon	výkon	k1gInSc4
v	v	k7c6
divadelní	divadelní	k2eAgFnSc6d1
hře	hra	k1gFnSc6
Unaveni	unaven	k2eAgMnPc1d1
sluncem	slunce	k1gNnSc7
v	v	k7c6
Londýnském	londýnský	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
mládeže	mládež	k1gFnSc2
získala	získat	k5eAaPmAgFnS
cenu	cena	k1gFnSc4
Laurence	Laurenec	k1gMnSc2
Oliviera	Olivier	k1gMnSc2
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
výkon	výkon	k1gInSc4
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
starší	starý	k2eAgFnPc4d2
sestry	sestra	k1gFnPc4
Joanne	Joann	k1gInSc5
a	a	k8xC
Luise	Luisa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
otec	otec	k1gMnSc1
Michael	Michael	k1gMnSc1
Dockery	Dockera	k1gFnSc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Irska	Irsko	k1gNnSc2
a	a	k8xC
její	její	k3xOp3gFnSc1
matka	matka	k1gFnSc1
Lorraine	Lorrain	k1gInSc5
(	(	kIx(
<g/>
rozená	rozený	k2eAgFnSc1d1
Witton	Witton	k1gInSc1
<g/>
)	)	kIx)
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
východního	východní	k2eAgInSc2d1
Londýna	Londýn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdělávala	vzdělávat	k5eAaImAgFnS
na	na	k7c4
Chadwell	Chadwell	k1gInSc4
Heath	Heath	k1gInSc1
Foundation	Foundation	k1gInSc1
School	School	k1gInSc4
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
the	the	k?
Chadwell	Chadwell	k1gInSc1
Heath	Heatha	k1gFnPc2
Academy	Academa	k1gFnSc2
<g/>
)	)	kIx)
ve	v	k7c6
východním	východní	k2eAgInSc6d1
Londýně	Londýn	k1gInSc6
poté	poté	k6eAd1
studovala	studovat	k5eAaImAgFnS
na	na	k7c4
Finch	Finch	k1gInSc4
Stage	Stage	k1gNnSc2
School	Schoola	k1gFnPc2
a	a	k8xC
po	po	k7c6
maturitě	maturita	k1gFnSc6
se	se	k3xPyFc4
zapsala	zapsat	k5eAaPmAgFnS
na	na	k7c4
Hudební	hudební	k2eAgFnSc4d1
a	a	k8xC
dramatickou	dramatický	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
Guildhallu	Guildhall	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
Divadlo	divadlo	k1gNnSc1
</s>
<s>
Michelle	Michelle	k6eAd1
Dockeryová	Dockeryová	k1gFnSc1
byla	být	k5eAaImAgFnS
členkou	členka	k1gFnSc7
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
mládeže	mládež	k1gFnSc2
<g/>
,	,	kIx,
studovala	studovat	k5eAaImAgFnS
na	na	k7c4
Guildhall	Guildhall	k1gInSc4
School	Schoola	k1gFnPc2
of	of	k?
Music	Music	k1gMnSc1
and	and	k?
Drama	drama	k1gFnSc1
kde	kde	k6eAd1
získala	získat	k5eAaPmAgFnS
zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
za	za	k7c4
drama	drama	k1gNnSc4
a	a	k8xC
ztvárnila	ztvárnit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
profesionální	profesionální	k2eAgInSc4d1
debut	debut	k1gInSc4
ve	v	k7c6
hře	hra	k1gFnSc6
Jeho	jeho	k3xOp3gFnPc1
temné	temný	k2eAgFnPc1d1
esence	esence	k1gFnPc1
v	v	k7c6
Královském	královský	k2eAgNnSc6d1
národním	národní	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
byla	být	k5eAaImAgFnS
nominována	nominovat	k5eAaBmNgFnS
na	na	k7c4
Cenu	cena	k1gFnSc4
Iana	Ianus	k1gMnSc4
Charlesona	Charleson	k1gMnSc4
za	za	k7c4
své	svůj	k3xOyFgNnSc4
ztvárnění	ztvárnění	k1gNnSc4
Diny	din	k1gInPc4
Dorfové	Dorfový	k2eAgInPc4d1
v	v	k7c6
Ibsenových	Ibsenových	k2eAgInPc6d1
Pilířích	pilíř	k1gInPc6
společnosti	společnost	k1gFnSc2
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevila	objevit	k5eAaPmAgFnS
se	se	k3xPyFc4
ve	v	k7c6
hře	hra	k1gFnSc6
Unaveni	unaven	k2eAgMnPc1d1
sluncem	slunce	k1gNnSc7
v	v	k7c6
Londýnském	londýnský	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
mládeže	mládež	k1gFnSc2
<g/>
,	,	kIx,
za	za	k7c4
niž	jenž	k3xRgFnSc4
získala	získat	k5eAaPmAgFnS
cenu	cena	k1gFnSc4
Laurence	Laurenec	k1gMnSc2
Oliviera	Olivier	k1gMnSc2
za	za	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
herečku	herečka	k1gFnSc4
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
hrála	hrát	k5eAaImAgFnS
Ofélii	Ofélie	k1gFnSc4
v	v	k7c6
Hamletovi	Hamlet	k1gMnSc6
v	v	k7c6
Crucible	Crucible	k1gFnSc6
Theatre	Theatr	k1gInSc5
po	po	k7c6
boku	bok	k1gInSc6
Johna	John	k1gMnSc2
Simma	Simm	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Film	film	k1gInSc1
a	a	k8xC
televize	televize	k1gFnSc1
</s>
<s>
Dockeryová	Dockeryová	k1gFnSc1
utvořila	utvořit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
televizní	televizní	k2eAgInSc4d1
debut	debut	k1gInSc4
ve	v	k7c6
filmu	film	k1gInSc6
Prstoklad	prstoklad	k1gInSc1
v	v	k7c6
roli	role	k1gFnSc6
Betty	Betty	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
hrála	hrát	k5eAaImAgFnS
Zuzanu	Zuzana	k1gFnSc4
Stohelitskou	Stohelitský	k2eAgFnSc4d1
v	v	k7c6
dvoudílné	dvoudílný	k2eAgFnSc6d1
adaptaci	adaptace	k1gFnSc6
románu	román	k1gInSc2
Otec	otec	k1gMnSc1
prasátek	prasátko	k1gNnPc2
Terryho	Terry	k1gMnSc2
Pratchetta	Pratchett	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc2
nejznámější	známý	k2eAgFnSc2d3
role	role	k1gFnSc2
je	být	k5eAaImIp3nS
Lady	lady	k1gFnSc1
Mary	Mary	k1gFnSc2
Crawley	Crawlea	k1gFnSc2
v	v	k7c6
dramatu	drama	k1gNnSc6
Panství	panství	k1gNnSc2
Downton	Downton	k1gInSc1
od	od	k7c2
Juliana	Julian	k1gMnSc2
Fellowese	Fellowese	k1gFnSc2
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
řada	řada	k1gFnSc1
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
v	v	k7c6
listopadu	listopad	k1gInSc6
2010	#num#	k4
na	na	k7c6
ITV	ITV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
2010	#num#	k4
nominována	nominovat	k5eAaBmNgFnS
na	na	k7c4
cenu	cena	k1gFnSc4
Southbank	Southbank	k1gInSc4
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
průlomový	průlomový	k2eAgInSc4d1
výkon	výkon	k1gInSc4
v	v	k7c6
televizním	televizní	k2eAgNnSc6d1
dramatu	drama	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
řada	řada	k1gFnSc1
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
<g/>
,	,	kIx,
následovala	následovat	k5eAaImAgFnS
speciální	speciální	k2eAgInSc4d1
díl	díl	k1gInSc4
„	„	k?
<g/>
Štědrý	štědrý	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
po	po	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
byla	být	k5eAaImAgFnS
nominována	nominován	k2eAgFnSc1d1
na	na	k7c4
cenu	cena	k1gFnSc4
Emmy	Emma	k1gFnSc2
v	v	k7c6
kategorii	kategorie	k1gFnSc6
nejlepší	dobrý	k2eAgInSc4d3
ženský	ženský	k2eAgInSc4d1
herecký	herecký	k2eAgInSc4d1
výkon	výkon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šestá	šestý	k4xOgFnSc1
řada	řada	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
bude	být	k5eAaImBp3nS
uvedena	uvést	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
,	,	kIx,
úspěšný	úspěšný	k2eAgInSc1d1
seriál	seriál	k1gInSc1
skončil	skončit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
měl	mít	k5eAaImAgInS
premiéru	premiéra	k1gFnSc4
film	film	k1gInSc1
Panství	panství	k1gNnSc2
Downton	Downton	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
si	se	k3xPyFc3
roli	role	k1gFnSc4
zopakovala	zopakovat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
si	se	k3xPyFc3
zahrála	zahrát	k5eAaPmAgFnS
Charlotte	Charlott	k1gInSc5
Rampling	Rampling	k1gInSc1
ve	v	k7c6
dvoudílné	dvoudílný	k2eAgFnSc6d1
dramatizaci	dramatizace	k1gFnSc6
špionážního	špionážní	k2eAgInSc2d1
thrilleru	thriller	k1gInSc2
Neklidní	klidnit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
po	po	k7c6
boku	bok	k1gInSc6
Liama	Liam	k1gMnSc2
Neesona	Neeson	k1gMnSc2
<g/>
,	,	kIx,
Julianne	Juliann	k1gMnSc5
Moore	Moor	k1gMnSc5
a	a	k8xC
Lupity	Lupita	k1gMnSc2
Nyong	Nyonga	k1gFnPc2
<g/>
'	'	kIx"
<g/>
o	o	k7c6
ve	v	k7c6
thrillerovém	thrillerový	k2eAgInSc6d1
filmu	film	k1gInSc6
NON-STOP	NON-STOP	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Ryanem	Ryan	k1gMnSc7
Reynoldsem	Reynolds	k1gMnSc7
si	se	k3xPyFc3
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
zahrála	zahrát	k5eAaPmAgFnS
ve	v	k7c6
sci-fi	sci-fi	k1gNnSc6
thrilleru	thriller	k1gInSc2
Nesmrtelný	smrtelný	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
206	#num#	k4
získala	získat	k5eAaPmAgFnS
hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
Letty	Letta	k1gFnSc2
Raines	Rainesa	k1gFnPc2
v	v	k7c6
americkém	americký	k2eAgInSc6d1
dramatickém	dramatický	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
Dobré	dobrý	k2eAgInPc4d1
mravy	mrav	k1gInPc4
V	v	k7c6
listopadu	listopad	k1gInSc6
2018	#num#	k4
byl	být	k5eAaImAgInS
seriál	seriál	k1gInSc1
po	po	k7c6
dvou	dva	k4xCgFnPc6
odvysílaných	odvysílaný	k2eAgFnPc6d1
řadách	řada	k1gFnPc6
zrušen	zrušit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
si	se	k3xPyFc3
zahrála	zahrát	k5eAaPmAgFnS
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
filmu	film	k1gInSc6
The	The	k1gFnSc2
Sense	Sense	k1gFnSc2
of	of	k?
an	an	k?
Ending	Ending	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Hudba	hudba	k1gFnSc1
</s>
<s>
Michelle	Michelle	k1gFnSc1
Dockeryová	Dockeryová	k1gFnSc1
dále	daleko	k6eAd2
vystupuje	vystupovat	k5eAaImIp3nS
jako	jako	k9
jazzová	jazzový	k2eAgFnSc1d1
zpěvačka	zpěvačka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpívala	zpívat	k5eAaImAgFnS
na	na	k7c4
50	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
Jazz	jazz	k1gInSc4
klubu	klub	k1gInSc2
Ronnieho	Ronnie	k1gMnSc2
Scotta	Scott	k1gMnSc2
v	v	k7c6
Londýně	Londýn	k1gInSc6
a	a	k8xC
občas	občas	k6eAd1
zpívala	zpívat	k5eAaImAgFnS
se	se	k3xPyFc4
Sadie	Sadie	k1gFnSc1
and	and	k?
the	the	k?
Hotheads	Hotheads	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Filmografie	filmografie	k1gFnSc1
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Role	role	k1gFnSc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
2010	#num#	k4
</s>
<s>
Spoiler	spoiler	k1gInSc1
</s>
<s>
Goth	Gotha	k1gFnPc2
Girl	girl	k1gFnSc2
</s>
<s>
krátkometrážní	krátkometrážní	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
Shades	Shades	k1gInSc1
of	of	k?
Beige	Beige	k1gInSc1
</s>
<s>
Jodie	Jodie	k1gFnSc1
</s>
<s>
krátkometrážní	krátkometrážní	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
Hanna	Hanna	k1gFnSc1
</s>
<s>
False	False	k1gFnSc1
Marissa	Marissa	k1gFnSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Out	Out	k?
of	of	k?
Time	Time	k1gInSc1
</s>
<s>
Christine	Christin	k1gMnSc5
</s>
<s>
krátkometrážní	krátkometrážní	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Anna	Anna	k1gFnSc1
Karenina	Karenina	k1gFnSc1
</s>
<s>
princezna	princezna	k1gFnSc1
Myagkaya	Myagkay	k1gInSc2
</s>
<s>
2012	#num#	k4
</s>
<s>
A	a	k9
Poem	poema	k1gFnPc2
Is	Is	k1gMnSc1
<g/>
..	..	k?
</s>
<s>
vypravěč	vypravěč	k1gMnSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Angelic	Angelice	k1gFnPc2
Voices	Voicesa	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gFnSc1
Choristers	Choristers	k1gInSc1
of	of	k?
Salisbury	Salisbura	k1gFnSc2
Cathedral	Cathedral	k1gFnSc2
</s>
<s>
dokument	dokument	k1gInSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
NON-STOP	NON-STOP	k?
</s>
<s>
Nancy	Nancy	k1gNnSc1
Hoffman	Hoffman	k1gMnSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
Tough	Tough	k1gInSc1
Justice	justice	k1gFnSc2
</s>
<s>
Connie	Connie	k1gFnSc1
Tough	Tougha	k1gFnPc2
</s>
<s>
krátkometrážní	krátkometrážní	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
2015	#num#	k4
</s>
<s>
Nesmrtelný	nesmrtelný	k1gMnSc1
</s>
<s>
Claire	Clair	k1gMnSc5
Hale	hala	k1gFnSc6
</s>
<s>
2015	#num#	k4
</s>
<s>
Many	mana	k1gFnPc1
Beautiful	Beautifula	k1gFnPc2
Things	Thingsa	k1gFnPc2
</s>
<s>
Lilias	Lilias	k1gMnSc1
Trotter	Trotter	k1gMnSc1
(	(	kIx(
<g/>
hlas	hlas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
dokument	dokument	k1gInSc1
</s>
<s>
2015	#num#	k4
</s>
<s>
District	District	k1gMnSc1
Zero	Zero	k1gMnSc1
<g/>
:	:	kIx,
What	What	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Hidden	Hiddno	k1gNnPc2
Inside	Insid	k1gInSc5
the	the	k?
Smartphone	Smartphon	k1gInSc5
of	of	k?
a	a	k8xC
Refugee	Refugee	k1gFnSc1
</s>
<s>
vypravěč	vypravěč	k1gMnSc1
</s>
<s>
2017	#num#	k4
</s>
<s>
The	The	k?
Sense	Sense	k1gFnSc1
of	of	k?
an	an	k?
Ending	Ending	k1gInSc1
</s>
<s>
Susie	Susie	k1gFnSc1
Webster	Webstra	k1gFnPc2
</s>
<s>
2019	#num#	k4
</s>
<s>
Panství	panství	k1gNnSc1
Downton	Downton	k1gInSc1
</s>
<s>
Lady	lady	k1gFnSc1
Mary	Mary	k1gFnSc2
Talbot	Talbota	k1gFnPc2
</s>
<s>
2019	#num#	k4
</s>
<s>
Gentlemani	gentleman	k1gMnPc1
</s>
<s>
Rosalind	Rosalinda	k1gFnPc2
„	„	k?
<g/>
Roz	Roz	k1gMnSc1
<g/>
“	“	k?
Pearson	Pearson	k1gMnSc1
</s>
<s>
Televize	televize	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Role	role	k1gFnSc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
2005	#num#	k4
</s>
<s>
Prstoklad	prstoklad	k1gInSc1
</s>
<s>
Betty	Betty	k1gFnSc1
</s>
<s>
TV	TV	kA
film	film	k1gInSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
Otec	otec	k1gMnSc1
prasátek	prasátko	k1gNnPc2
</s>
<s>
Susan	Susan	k1gMnSc1
</s>
<s>
TV	TV	kA
film	film	k1gInSc1
</s>
<s>
2007	#num#	k4
</s>
<s>
Consent	Consent	k1gMnSc1
</s>
<s>
TV	TV	kA
film	film	k1gInSc1
</s>
<s>
2007	#num#	k4
</s>
<s>
Dalziel	Dalziel	k1gInSc1
a	a	k8xC
Pascoe	Pascoe	k1gInSc1
</s>
<s>
Aimee	Aimee	k1gFnSc1
Hobbs	Hobbsa	k1gFnPc2
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Projekt	projekt	k1gInSc4
Afrodité	Afroditý	k2eAgFnSc2d1
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
</s>
<s>
Heartbeat	Heartbeat	k1gInSc1
</s>
<s>
Sue	Sue	k?
Padgett	Padgett	k1gInSc1
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Take	Take	k1gInSc1
Three	Three	k1gFnPc2
Girls	girl	k1gFnPc2
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
</s>
<s>
Poppy	Poppa	k1gFnPc1
Shakespeare	Shakespeare	k1gMnSc1
</s>
<s>
Dawn	Dawn	k1gMnSc1
</s>
<s>
TV	TV	kA
film	film	k1gInSc1
</s>
<s>
2009	#num#	k4
</s>
<s>
Vraždy	vražda	k1gFnPc1
v	v	k7c6
Yorkshiru	Yorkshiro	k1gNnSc6
<g/>
:	:	kIx,
1974	#num#	k4
</s>
<s>
Kathryn	Kathryn	k1gMnSc1
Taylor	Taylor	k1gMnSc1
</s>
<s>
2009	#num#	k4
</s>
<s>
Vraždy	vražda	k1gFnPc1
v	v	k7c6
Yorkshiru	Yorkshiro	k1gNnSc6
<g/>
:	:	kIx,
1983	#num#	k4
</s>
<s>
Kathryn	Kathryn	k1gMnSc1
Taylor	Taylor	k1gMnSc1
</s>
<s>
2009	#num#	k4
</s>
<s>
The	The	k?
Courageous	Courageous	k1gInSc1
Heart	Heart	k1gInSc1
of	of	k?
Irena	Irena	k1gFnSc1
Sendler	Sendler	k1gMnSc1
</s>
<s>
Ewa	Ewa	k?
Rozenfeld	Rozenfeld	k1gInSc1
</s>
<s>
2009	#num#	k4
</s>
<s>
Návrat	návrat	k1gInSc1
mrtvých	mrtvý	k1gMnPc2
</s>
<s>
Ann	Ann	k?
</s>
<s>
2009	#num#	k4
</s>
<s>
Waking	Waking	k1gInSc1
the	the	k?
Dead	Dead	k1gInSc1
</s>
<s>
Gemma	Gemma	k1gFnSc1
Morrison	Morrisona	k1gFnPc2
</s>
<s>
2	#num#	k4
díly	dílo	k1gNnPc7
</s>
<s>
2009	#num#	k4
</s>
<s>
Cranford	Cranford	k6eAd1
</s>
<s>
Erminia	Erminium	k1gNnPc1
Whyte	Whyt	k1gMnSc5
</s>
<s>
2	#num#	k4
díly	dílo	k1gNnPc7
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
</s>
<s>
Panství	panství	k1gNnSc1
Downton	Downton	k1gInSc1
</s>
<s>
Lady	lady	k1gFnSc1
Mary	Mary	k1gFnSc2
Crawley	Crawlea	k1gFnSc2
</s>
<s>
hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
<g/>
,	,	kIx,
52	#num#	k4
dílů	díl	k1gInPc2
</s>
<s>
Screen	Screen	k2eAgInSc1d1
Actors	Actors	k1gInSc1
Guild	Guild	k1gInSc1
Awards	Awards	k1gInSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
nejlepší	dobrý	k2eAgNnSc1d3
obsazení	obsazení	k1gNnSc1
(	(	kIx(
<g/>
drama	drama	k1gNnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
nominace	nominace	k1gFnSc1
–	–	k?
Cena	cena	k1gFnSc1
Emmy	Emma	k1gFnSc2
v	v	k7c6
kategorii	kategorie	k1gFnSc6
nejlepší	dobrý	k2eAgInSc4d3
ženský	ženský	k2eAgInSc4d1
herecký	herecký	k2eAgInSc4d1
výkon	výkon	k1gInSc4
v	v	k7c6
dramatickém	dramatický	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2012	#num#	k4
</s>
<s>
Restless	Restless	k6eAd1
</s>
<s>
Ruth	Ruth	k1gFnSc1
Gilmartin	Gilmartina	k1gFnPc2
</s>
<s>
mini-série	mini-série	k1gFnSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Americký	americký	k2eAgMnSc1d1
táta	táta	k1gMnSc1
</s>
<s>
Margaret	Margareta	k1gFnPc2
Watkins	Watkinsa	k1gFnPc2
(	(	kIx(
<g/>
hlas	hlas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
National	National	k1gMnSc5
Treasure	Treasur	k1gMnSc5
4	#num#	k4
<g/>
:	:	kIx,
Baby	baby	k1gNnSc4
Franny	Franna	k1gFnPc1
<g/>
:	:	kIx,
She	She	k1gFnPc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Doing	Doing	k1gMnSc1
Well	Well	k1gMnSc1
-	-	kIx~
The	The	k1gFnSc1
Hole	hole	k1gFnSc1
Story	story	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
2012	#num#	k4
</s>
<s>
V	v	k7c6
kruhu	kruh	k1gInSc6
koruny	koruna	k1gFnSc2
<g/>
:	:	kIx,
Jindřich	Jindřich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Lady	lady	k1gFnSc1
Kate	kat	k1gMnSc5
Percy	Percum	k1gNnPc7
</s>
<s>
TV	TV	kA
film	film	k1gInSc1
</s>
<s>
2013	#num#	k4
</s>
<s>
Griffinovi	Griffinův	k2eAgMnPc1d1
</s>
<s>
Lady	lady	k1gFnSc1
Mary	Mary	k1gFnSc2
Crawley	Crawlea	k1gFnSc2
(	(	kIx(
<g/>
hlas	hlas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Boopa-dee	Boopa-dee	k1gInSc1
Bappa-dee	Bappa-dee	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
2015	#num#	k4
</s>
<s>
Japan	japan	k1gInSc1
<g/>
:	:	kIx,
Earth	Earth	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Enchanted	Enchanted	k1gInSc1
Islands	Islandsa	k1gFnPc2
</s>
<s>
vypravěč	vypravěč	k1gMnSc1
</s>
<s>
dokumentární	dokumentární	k2eAgInSc1d1
seriál	seriál	k1gInSc1
</s>
<s>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
</s>
<s>
Dobré	dobrý	k2eAgInPc1d1
mravy	mrav	k1gInPc1
</s>
<s>
Letty	Letta	k1gFnPc1
Raines	Rainesa	k1gFnPc2
</s>
<s>
hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
<g/>
,	,	kIx,
20	#num#	k4
dílů	díl	k1gInPc2
</s>
<s>
2017	#num#	k4
</s>
<s>
Angie	Angie	k1gFnSc1
Tribeca	Tribec	k1gInSc2
</s>
<s>
Victoria	Victorium	k1gNnPc1
Nova	novum	k1gNnSc2
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Turn	Turn	k1gMnSc1
Me	Me	k1gMnSc1
On	on	k3xPp3gMnSc1
<g/>
,	,	kIx,
Geils	Geils	k1gInSc1
<g/>
“	“	k?
</s>
<s>
2017	#num#	k4
</s>
<s>
Godless	Godless	k6eAd1
</s>
<s>
Alice	Alice	k1gFnSc1
Fletcher	Fletchra	k1gFnPc2
</s>
<s>
mini-série	mini-série	k1gFnSc1
<g/>
,	,	kIx,
7	#num#	k4
dílů	díl	k1gInPc2
</s>
<s>
nominace	nominace	k1gFnSc1
–	–	k?
Cena	cena	k1gFnSc1
Emmy	Emma	k1gFnSc2
v	v	k7c6
kategorii	kategorie	k1gFnSc6
nejlepší	dobrý	k2eAgInSc4d3
ženský	ženský	k2eAgInSc4d1
herecký	herecký	k2eAgInSc4d1
výkon	výkon	k1gInSc4
v	v	k7c6
limitovaném	limitovaný	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
</s>
<s>
2019	#num#	k4
</s>
<s>
Tuca	Tuc	k2eAgFnSc1d1
&	&	k?
Bertie	Bertie	k1gFnSc1
</s>
<s>
Lady	lady	k1gFnSc1
Netherfield	Netherfield	k1gMnSc1
(	(	kIx(
<g/>
voice	voice	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
The	The	k1gMnSc1
Deli	Del	k1gFnSc2
Guy	Guy	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
2020	#num#	k4
</s>
<s>
Defending	Defending	k1gInSc1
Jacob	Jacoba	k1gFnPc2
</s>
<s>
Laurie	Laurie	k1gFnSc1
Barber	Barbra	k1gFnPc2
</s>
<s>
mini-série	mini-série	k1gFnSc1
<g/>
,	,	kIx,
8	#num#	k4
dílů	díl	k1gInPc2
</s>
<s>
Divadlo	divadlo	k1gNnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Role	role	k1gFnSc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
2004	#num#	k4
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1
temné	temný	k2eAgFnPc1d1
esence	esence	k1gFnPc1
</s>
<s>
Jessie	Jessie	k1gFnSc1
</s>
<s>
Národní	národní	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Londýn	Londýn	k1gInSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
V	v	k7c6
kruhu	kruh	k1gInSc6
koruny	koruna	k1gFnSc2
<g/>
:	:	kIx,
Jindřich	Jindřich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Carrier	Carrier	k1gMnSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
The	The	k?
UN	UN	kA
Inspector	Inspector	k1gInSc1
</s>
<s>
aktivistka	aktivistka	k1gFnSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
Pillars	Pillars	k1gInSc1
of	of	k?
the	the	k?
Community	Communita	k1gFnSc2
</s>
<s>
Dina	Dina	k6eAd1
</s>
<s>
2007	#num#	k4
</s>
<s>
Dying	Dying	k1gInSc1
for	forum	k1gNnPc2
It	It	k1gFnSc2
</s>
<s>
Kleopatra	Kleopatra	k1gFnSc1
</s>
<s>
Almeida	Almeida	k1gFnSc1
Theatre	Theatr	k1gInSc5
</s>
<s>
2007	#num#	k4
</s>
<s>
Pygmalion	Pygmalion	k1gInSc1
</s>
<s>
Eliza	Eliza	k1gFnSc1
Doolittle	Doolittle	k1gFnSc2
</s>
<s>
Britské	britský	k2eAgNnSc1d1
turné	turné	k1gNnSc1
</s>
<s>
2008	#num#	k4
</s>
<s>
Strýček	strýček	k1gMnSc1
Váňa	Váňa	k1gMnSc1
</s>
<s>
Yelena	Yelena	k1gFnSc1
</s>
<s>
Britské	britský	k2eAgNnSc1d1
turné	turné	k1gNnSc1
</s>
<s>
2008	#num#	k4
</s>
<s>
Pygmalion	Pygmalion	k1gInSc1
</s>
<s>
Eliza	Eliza	k1gFnSc1
Doolittle	Doolittle	k1gFnSc2
</s>
<s>
Old	Olda	k1gFnPc2
Vic	Vic	k1gFnSc2
Theatre	Theatr	k1gInSc5
</s>
<s>
2009	#num#	k4
</s>
<s>
Unaveni	unaven	k2eAgMnPc1d1
sluncem	slunce	k1gNnSc7
</s>
<s>
Maroussia	Maroussia	k1gFnSc1
</s>
<s>
Národní	národní	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Londýn	Londýn	k1gInSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
Hamlet	Hamlet	k1gMnSc1
</s>
<s>
Ophelia	Ophelia	k1gFnSc1
</s>
<s>
Crucible	Crucible	k6eAd1
Theatre	Theatr	k1gInSc5
<g/>
,	,	kIx,
Sheffield	Sheffield	k1gMnSc1
</s>
<s>
2017	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
</s>
<s>
Network	network	k1gInSc1
</s>
<s>
Diana	Diana	k1gFnSc1
Christensen	Christensna	k1gFnPc2
</s>
<s>
Národní	národní	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Londýn	Londýn	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Michelle	Michelle	k1gNnSc2
Dockery	Dockera	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Michelle	Michelle	k1gInSc1
Dockery	Dockera	k1gFnSc2
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Michelle	Michelle	k1gFnSc1
Dockery	Dockera	k1gFnSc2
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
249970	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
143380346	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1512	#num#	k4
3263	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2010154801	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
132948684	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2010154801	#num#	k4
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
|	|	kIx~
Televize	televize	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
