<s desamb="1">
Proslavila	proslavit	k5eAaPmAgFnS
se	se	k3xPyFc4
díky	díky	k7c3
historickému	historický	k2eAgInSc3d1
seriálu	seriál	k1gInSc3
stanice	stanice	k1gFnSc1
ITV	ITV	kA
Panství	panství	k1gNnSc1
Downton	Downton	k1gInSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
výkon	výkon	k1gInSc4
v	v	k7c6
seriálu	seriál	k1gInSc6
získala	získat	k5eAaPmAgFnS
nominaci	nominace	k1gFnSc4
na	na	k7c4
Zlatý	zlatý	k2eAgInSc4d1
glóbus	glóbus	k1gInSc4
a	a	k8xC
tři	tři	k4xCgFnPc4
ceny	cena	k1gFnPc4
Emmy	Emma	k1gFnSc2
<g/>
.	.	kIx.
</s>