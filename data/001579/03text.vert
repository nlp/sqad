<s>
Enrico	Enrico	k6eAd1	Enrico
Fermi	Fer	k1gFnPc7	Fer
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1901	[number]	k4	1901
Řím	Řím	k1gInSc1	Řím
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1954	[number]	k4	1954
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
fyzik	fyzik	k1gMnSc1	fyzik
známý	známý	k1gMnSc1	známý
svými	svůj	k3xOyFgFnPc7	svůj
výzkumy	výzkum	k1gInPc1	výzkum
jaderných	jaderný	k2eAgFnPc2d1	jaderná
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
částicové	částicový	k2eAgFnSc2d1	částicová
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
statistické	statistický	k2eAgFnSc2d1	statistická
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
fyziků	fyzik	k1gMnPc2	fyzik
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
výzkumem	výzkum	k1gInSc7	výzkum
jak	jak	k8xS	jak
teoretickým	teoretický	k2eAgInSc7d1	teoretický
tak	tak	k8xS	tak
i	i	k9	i
experimentálním	experimentální	k2eAgInPc3d1	experimentální
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumal	zkoumat	k5eAaImAgInS	zkoumat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
beta-	beta-	k?	beta-
a	a	k8xC	a
gama-	gama-	k?	gama-
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
prvního	první	k4xOgInSc2	první
jaderného	jaderný	k2eAgInSc2d1	jaderný
reaktoru	reaktor	k1gInSc2	reaktor
Chicago	Chicago	k1gNnSc1	Chicago
Pile-	Pile-	k1gFnSc2	Pile-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Podal	podat	k5eAaPmAgMnS	podat
několik	několik	k4yIc4	několik
patentů	patent	k1gInPc2	patent
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
využití	využití	k1gNnSc2	využití
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
indukované	indukovaný	k2eAgFnSc6d1	indukovaná
radioaktivitě	radioaktivita	k1gFnSc6	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
"	"	kIx"	"
<g/>
otců	otec	k1gMnPc2	otec
<g/>
"	"	kIx"	"
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
autorem	autor	k1gMnSc7	autor
tzv.	tzv.	kA	tzv.
Fermiho	Fermi	k1gMnSc4	Fermi
paradoxu	paradox	k1gInSc2	paradox
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
si	se	k3xPyFc3	se
klade	klást	k5eAaImIp3nS	klást
otázku	otázka	k1gFnSc4	otázka
kolik	kolik	k9	kolik
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
inteligentních	inteligentní	k2eAgFnPc2d1	inteligentní
mimozemských	mimozemský	k2eAgFnPc2d1	mimozemská
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
jako	jako	k9	jako
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
Alberta	Albert	k1gMnSc2	Albert
Fermiho	Fermi	k1gMnSc2	Fermi
-	-	kIx~	-
hlavního	hlavní	k2eAgMnSc2d1	hlavní
inspektora	inspektor	k1gMnSc2	inspektor
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gNnPc4	jeho
rodina	rodina	k1gFnSc1	rodina
Římsko-katolického	římskoatolický	k2eAgNnSc2d1	římsko-katolické
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
svých	svůj	k3xOyFgMnPc2	svůj
prarodičů	prarodič	k1gMnPc2	prarodič
byl	být	k5eAaImAgInS	být
pokřtěn	pokřtěn	k2eAgInSc1d1	pokřtěn
<g/>
,	,	kIx,	,
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
dospělý	dospělý	k2eAgInSc4d1	dospělý
život	život	k1gInSc4	život
byl	být	k5eAaImAgMnS	být
Fermi	Fer	k1gFnPc7	Fer
agnostik	agnostik	k1gMnSc1	agnostik
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
přírodní	přírodní	k2eAgFnPc4d1	přírodní
vědy	věda	k1gFnPc4	věda
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
fyziku	fyzika	k1gFnSc4	fyzika
byl	být	k5eAaImAgInS	být
rodinou	rodina	k1gFnSc7	rodina
podporován	podporovat	k5eAaImNgMnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Giuliem	Giulium	k1gNnSc7	Giulium
sestavovali	sestavovat	k5eAaImAgMnP	sestavovat
elektrické	elektrický	k2eAgInPc4d1	elektrický
motory	motor	k1gInPc4	motor
a	a	k8xC	a
hráli	hrát	k5eAaImAgMnP	hrát
si	se	k3xPyFc3	se
s	s	k7c7	s
mechanickými	mechanický	k2eAgFnPc7d1	mechanická
hračkami	hračka	k1gFnPc7	hračka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
operaci	operace	k1gFnSc6	operace
krku	krk	k1gInSc2	krk
během	během	k7c2	během
podávání	podávání	k1gNnSc2	podávání
anestezie	anestezie	k1gFnSc2	anestezie
<g/>
.	.	kIx.	.
</s>
<s>
Fermiho	Fermize	k6eAd1	Fermize
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
fyziku	fyzika	k1gFnSc4	fyzika
později	pozdě	k6eAd2	pozdě
podpořil	podpořit	k5eAaPmAgMnS	podpořit
kolega	kolega	k1gMnSc1	kolega
jeho	jeho	k3xOp3gMnPc4	jeho
otce	otec	k1gMnPc4	otec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgInS	dát
několik	několik	k4yIc4	několik
tematických	tematický	k2eAgFnPc2d1	tematická
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
až	až	k9	až
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Pise	Pisa	k1gFnSc6	Pisa
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
studentům	student	k1gMnPc3	student
ubytování	ubytování	k1gNnSc4	ubytování
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zájemci	zájemce	k1gMnPc1	zájemce
museli	muset	k5eAaImAgMnP	muset
zvládnout	zvládnout	k5eAaPmF	zvládnout
obtížné	obtížný	k2eAgFnPc4d1	obtížná
přijímací	přijímací	k2eAgFnPc4d1	přijímací
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
esej	esej	k1gFnSc4	esej
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
uvedené	uvedený	k2eAgNnSc4d1	uvedené
téma	téma	k1gNnSc4	téma
"	"	kIx"	"
Specifické	specifický	k2eAgFnPc1d1	specifická
charakteristiky	charakteristika	k1gFnPc1	charakteristika
zvuku	zvuk	k1gInSc2	zvuk
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
sedmnáctiletý	sedmnáctiletý	k2eAgInSc1d1	sedmnáctiletý
Fermi	Fer	k1gFnPc7	Fer
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
odvodit	odvodit	k5eAaPmF	odvodit
parciální	parciální	k2eAgFnSc4d1	parciální
diferenciální	diferenciální	k2eAgFnSc4d1	diferenciální
rovnici	rovnice	k1gFnSc4	rovnice
pro	pro	k7c4	pro
vibrační	vibrační	k2eAgFnSc4d1	vibrační
tyč	tyč	k1gFnSc4	tyč
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
Fouriérovy	Fouriérův	k2eAgFnSc2d1	Fouriérův
analýzy	analýza	k1gFnSc2	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Giuseppe	Giusepp	k1gMnSc5	Giusepp
Pittarelli	Pittarell	k1gMnSc5	Pittarell
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
práci	práce	k1gFnSc4	práce
posuzoval	posuzovat	k5eAaImAgMnS	posuzovat
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
shledal	shledat	k5eAaPmAgMnS	shledat
natolik	natolik	k6eAd1	natolik
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
stačila	stačit	k5eAaBmAgFnS	stačit
i	i	k9	i
pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
do	do	k7c2	do
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Fermi	Fer	k1gFnPc7	Fer
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
umístil	umístit	k5eAaPmAgMnS	umístit
v	v	k7c6	v
přijímacím	přijímací	k2eAgNnSc6d1	přijímací
řízení	řízení	k1gNnSc6	řízení
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Francem	Franc	k1gMnSc7	Franc
Rasettim	Rasettim	k1gInSc4	Rasettim
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
blízkým	blízký	k2eAgMnSc7d1	blízký
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
studia	studio	k1gNnSc2	studio
také	také	k6eAd1	také
publikoval	publikovat	k5eAaBmAgMnS	publikovat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
časopise	časopis	k1gInSc6	časopis
Nuovo	Nuovo	k1gNnSc4	Nuovo
Cimento	Cimento	k1gNnSc1	Cimento
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Dynamika	dynamika	k1gFnSc1	dynamika
tuhého	tuhý	k2eAgInSc2d1	tuhý
systému	systém	k1gInSc2	systém
elektrických	elektrický	k2eAgInPc2d1	elektrický
nábojů	náboj	k1gInPc2	náboj
v	v	k7c6	v
translačním	translační	k2eAgInSc6d1	translační
pohybu	pohyb	k1gInSc6	pohyb
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
promoval	promovat	k5eAaBmAgInS	promovat
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
italský	italský	k2eAgInSc4d1	italský
vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
titul	titul	k1gInSc4	titul
laurea	laure	k1gInSc2	laure
v	v	k7c6	v
nezvykle	zvykle	k6eNd1	zvykle
nízkém	nízký	k2eAgInSc6d1	nízký
věku	věk	k1gInSc6	věk
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgMnS	získat
mladý	mladý	k2eAgMnSc1d1	mladý
fyzik	fyzik	k1gMnSc1	fyzik
stipendium	stipendium	k1gNnSc4	stipendium
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
mohl	moct	k5eAaImAgInS	moct
studovat	studovat	k5eAaImF	studovat
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Göttingenu	Göttingen	k1gInSc6	Göttingen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc7	jeho
profesorem	profesor	k1gMnSc7	profesor
také	také	k9	také
Max	Max	k1gMnSc1	Max
Born	Born	k1gMnSc1	Born
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Leidenu	Leiden	k1gInSc2	Leiden
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
univerzitě	univerzita	k1gFnSc6	univerzita
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Ehrenfestem	Ehrenfest	k1gMnSc7	Ehrenfest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1924-1926	[number]	k4	1924-1926
pak	pak	k6eAd1	pak
Fermi	Fer	k1gFnPc7	Fer
přednášel	přednášet	k5eAaImAgMnS	přednášet
matematickou	matematický	k2eAgFnSc4d1	matematická
fyziku	fyzika	k1gFnSc4	fyzika
a	a	k8xC	a
mechaniku	mechanika	k1gFnSc4	mechanika
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
fyziku	fyzika	k1gFnSc4	fyzika
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
zvolen	zvolit	k5eAaPmNgMnS	zvolit
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
se	se	k3xPyFc4	se
Fermi	Fer	k1gFnPc7	Fer
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Laurou	Laura	k1gFnSc7	Laura
Capon	Capon	k1gInSc4	Capon
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
později	pozdě	k6eAd2	pozdě
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
Fermi	Fer	k1gFnPc7	Fer
objevil	objevit	k5eAaPmAgMnS	objevit
statistické	statistický	k2eAgInPc4d1	statistický
zákony	zákon	k1gInPc4	zákon
známé	známá	k1gFnSc2	známá
dnes	dnes	k6eAd1	dnes
jako	jako	k9	jako
Fermiho	Fermi	k1gMnSc4	Fermi
statistiky	statistika	k1gFnSc2	statistika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Královské	královský	k2eAgFnSc2d1	královská
italské	italský	k2eAgFnSc2d1	italská
akademie	akademie	k1gFnSc2	akademie
a	a	k8xC	a
následně	následně	k6eAd1	následně
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
fašistické	fašistický	k2eAgFnSc2d1	fašistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
však	však	k9	však
oponoval	oponovat	k5eAaImAgMnS	oponovat
ideologii	ideologie	k1gFnSc4	ideologie
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgInP	být
přijaty	přijmout	k5eAaPmNgInP	přijmout
rasové	rasový	k2eAgInPc1d1	rasový
zákony	zákon	k1gInPc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
zákony	zákon	k1gInPc1	zákon
zastrašovaly	zastrašovat	k5eAaImAgInP	zastrašovat
jeho	jeho	k3xOp3gNnSc4	jeho
ženu	žena	k1gFnSc4	žena
Lauru	Laura	k1gFnSc4	Laura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
Židovka	Židovka	k1gFnSc1	Židovka
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
znemožnily	znemožnit	k5eAaPmAgInP	znemožnit
pokračování	pokračování	k1gNnSc4	pokračování
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
několika	několik	k4yIc2	několik
jeho	jeho	k3xOp3gMnPc3	jeho
asistentům	asistent	k1gMnPc3	asistent
a	a	k8xC	a
spolupracovníkům	spolupracovník	k1gMnPc3	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1934	[number]	k4	1934
Fermi	Fer	k1gFnPc7	Fer
mezi	mezi	k7c4	mezi
zdroj	zdroj	k1gInSc4	zdroj
neutronů	neutron	k1gInPc2	neutron
a	a	k8xC	a
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
fólii	fólie	k1gFnSc4	fólie
na	na	k7c4	na
zkoušku	zkouška	k1gFnSc4	zkouška
vložil	vložit	k5eAaPmAgMnS	vložit
vosk	vosk	k1gInSc4	vosk
<g/>
,	,	kIx,	,
indukovaná	indukovaný	k2eAgFnSc1d1	indukovaná
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
stříbra	stříbro	k1gNnSc2	stříbro
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
stokrát	stokrát	k6eAd1	stokrát
<g/>
.	.	kIx.	.
</s>
<s>
Srážkami	srážka	k1gFnPc7	srážka
s	s	k7c7	s
lehkými	lehký	k2eAgInPc7d1	lehký
atomy	atom	k1gInPc7	atom
vosku	vosk	k1gInSc2	vosk
se	se	k3xPyFc4	se
neutrony	neutron	k1gInPc1	neutron
zpomalily	zpomalit	k5eAaPmAgInP	zpomalit
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
objevu	objev	k1gInSc3	objev
štěpení	štěpení	k1gNnSc4	štěpení
uranu	uran	k1gInSc2	uran
byla	být	k5eAaImAgFnS	být
volná	volný	k2eAgFnSc1d1	volná
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výzkum	výzkum	k1gInSc1	výzkum
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
korunován	korunovat	k5eAaBmNgMnS	korunovat
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
potvrzení	potvrzení	k1gNnSc4	potvrzení
existence	existence	k1gFnSc2	existence
nových	nový	k2eAgInPc2d1	nový
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
prvků	prvek	k1gInPc2	prvek
vytvořených	vytvořený	k2eAgInPc2d1	vytvořený
neutronovým	neutronový	k2eAgNnSc7d1	neutronové
ozařováním	ozařování	k1gNnSc7	ozařování
a	a	k8xC	a
objev	objev	k1gInSc1	objev
jaderných	jaderný	k2eAgFnPc2d1	jaderná
reakcí	reakce	k1gFnPc2	reakce
způsobovaných	způsobovaný	k2eAgFnPc2d1	způsobovaná
ozařováním	ozařování	k1gNnSc7	ozařování
tepelnými	tepelný	k2eAgInPc7d1	tepelný
neutrony	neutron	k1gInPc7	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
obdržel	obdržet	k5eAaPmAgMnS	obdržet
cenu	cena	k1gFnSc4	cena
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
pronásledování	pronásledování	k1gNnSc4	pronásledování
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
fašistů	fašista	k1gMnPc2	fašista
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
jako	jako	k8xC	jako
profesor	profesor	k1gMnSc1	profesor
na	na	k7c6	na
Columbijské	Columbijský	k2eAgFnSc6d1	Columbijská
univerzitě	univerzita	k1gFnSc6	univerzita
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
prvního	první	k4xOgInSc2	první
jaderného	jaderný	k2eAgInSc2d1	jaderný
reaktoru	reaktor	k1gInSc2	reaktor
Chicago	Chicago	k1gNnSc1	Chicago
Pile-	Pile-	k1gFnSc2	Pile-
<g/>
1	[number]	k4	1
a	a	k8xC	a
na	na	k7c6	na
Projektu	projekt	k1gInSc6	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
zkonstruovat	zkonstruovat	k5eAaPmF	zkonstruovat
atomovou	atomový	k2eAgFnSc4d1	atomová
bombu	bomba	k1gFnSc4	bomba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
Fermi	Fer	k1gFnPc7	Fer
stal	stát	k5eAaPmAgInS	stát
občanem	občan	k1gMnSc7	občan
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
a	a	k8xC	a
díky	díky	k7c3	díky
krátké	krátký	k2eAgFnSc3d1	krátká
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
zároveň	zároveň	k6eAd1	zároveň
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Argonnské	Argonnský	k2eAgFnSc6d1	Argonnský
národní	národní	k2eAgFnSc6d1	národní
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c6	na
Atomic	Atomice	k1gFnPc2	Atomice
Energy	Energ	k1gInPc1	Energ
Commission	Commission	k1gInSc1	Commission
a	a	k8xC	a
Fermi	Fer	k1gFnPc7	Fer
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
generálního	generální	k2eAgInSc2d1	generální
poradního	poradní	k2eAgInSc2d1	poradní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
neutronové	neutronový	k2eAgFnSc3d1	neutronová
optice	optika	k1gFnSc3	optika
a	a	k8xC	a
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
elementární	elementární	k2eAgFnPc4d1	elementární
částice	částice	k1gFnPc4	částice
vysokých	vysoký	k2eAgFnPc2d1	vysoká
energií	energie	k1gFnPc2	energie
a	a	k8xC	a
problém	problém	k1gInSc1	problém
nukleon-mezonové	nukleonezonový	k2eAgFnSc2d1	nukleon-mezonový
interakce	interakce	k1gFnSc2	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1954	[number]	k4	1954
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
žaludku	žaludek	k1gInSc2	žaludek
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
Oak	Oak	k1gFnSc6	Oak
Woods	Woodsa	k1gFnPc2	Woodsa
Cemetery	Cemeter	k1gMnPc4	Cemeter
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Fermiho	Fermi	k1gMnSc2	Fermi
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
stý	stý	k4xOgInSc1	stý
prvek	prvek	k1gInSc1	prvek
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
-	-	kIx~	-
fermium	fermium	k1gNnSc4	fermium
-	-	kIx~	-
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Fermi	Fer	k1gFnPc7	Fer
je	být	k5eAaImIp3nS	být
také	také	k9	také
délková	délkový	k2eAgFnSc1d1	délková
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
1	[number]	k4	1
fermi	fer	k1gFnPc7	fer
=	=	kIx~	=
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
"	"	kIx"	"
<g/>
fm	fm	k?	fm
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
neoficiální	oficiální	k2eNgInSc1d1	neoficiální
název	název	k1gInSc1	název
femtometru	femtometr	k1gInSc2	femtometr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
užíván	užívat	k5eAaImNgInS	užívat
jadernými	jaderný	k2eAgMnPc7d1	jaderný
fyziky	fyzik	k1gMnPc7	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
Fermiho	Fermi	k1gMnSc2	Fermi
paradox	paradox	k1gInSc4	paradox
Fermion	fermion	k1gInSc4	fermion
Owen	Owena	k1gFnPc2	Owena
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Enrico	Enrico	k6eAd1	Enrico
Fermi	Fer	k1gFnPc7	Fer
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
zkrotit	zkrotit	k5eAaPmF	zkrotit
i	i	k8xC	i
rozbouřit	rozbouřit	k5eAaPmF	rozbouřit
atom	atom	k1gInSc4	atom
www.aldebaran.cz	www.aldebaran.cza	k1gFnPc2	www.aldebaran.cza
</s>
