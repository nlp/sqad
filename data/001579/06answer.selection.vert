<s>
Enrico	Enrico	k6eAd1	Enrico
Fermi	Fer	k1gFnPc7	Fer
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1901	[number]	k4	1901
Řím	Řím	k1gInSc1	Řím
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1954	[number]	k4	1954
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
fyzik	fyzik	k1gMnSc1	fyzik
známý	známý	k1gMnSc1	známý
svými	svůj	k3xOyFgFnPc7	svůj
výzkumy	výzkum	k1gInPc1	výzkum
jaderných	jaderný	k2eAgFnPc2d1	jaderná
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
částicové	částicový	k2eAgFnSc2d1	částicová
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
statistické	statistický	k2eAgFnSc2d1	statistická
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
