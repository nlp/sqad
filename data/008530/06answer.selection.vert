<s>
Titanic	Titanic	k1gInSc1	Titanic
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
Belfastu	Belfast	k1gInSc6	Belfast
v	v	k7c6	v
loděnicích	loděnice	k1gFnPc6	loděnice
Harland	Harland	k1gInSc1	Harland
&	&	k?	&
Wolff	Wolff	k1gInSc1	Wolff
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
společnosti	společnost	k1gFnSc2	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
druhý	druhý	k4xOgMnSc1	druhý
ze	z	k7c2	z
série	série	k1gFnSc2	série
tří	tři	k4xCgFnPc2	tři
lodí	loď	k1gFnPc2	loď
třídy	třída	k1gFnSc2	třída
Olympic	Olympic	k1gMnSc1	Olympic
(	(	kIx(	(
<g/>
RMS	RMS	kA	RMS
Olympic	Olympic	k1gMnSc1	Olympic
<g/>
,	,	kIx,	,
RMS	RMS	kA	RMS
Titanic	Titanic	k1gInSc1	Titanic
a	a	k8xC	a
HMHS	HMHS	kA	HMHS
Britannic	Britannice	k1gFnPc2	Britannice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
