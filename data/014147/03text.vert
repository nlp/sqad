<s>
Richie	Richie	k1gFnSc1
Faulkner	Faulknra	k1gFnPc2
</s>
<s>
Richie	Richie	k1gFnSc1
Faulkner	Faulknra	k1gFnPc2
2018	#num#	k4
<g/>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc4
</s>
<s>
Richard	Richard	k1gMnSc1
Ian	Ian	k1gMnSc1
Faulkner	Faulkner	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1980	#num#	k4
(	(	kIx(
<g/>
41	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
Žánry	žánr	k1gInPc4
</s>
<s>
heavy	heava	k1gFnPc1
metal	metal	k1gInSc1
<g/>
,	,	kIx,
hard	hard	k6eAd1
rock	rock	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
hudebník	hudebník	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
kytara	kytara	k1gFnSc1
Aktivní	aktivní	k2eAgFnSc1d1
roky	rok	k1gInPc4
</s>
<s>
2001	#num#	k4
<g/>
-dosud	-dosud	k6eAd1
Příbuzná	příbuzný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
Judas	Judas	k1gMnSc1
Priest	Priest	k1gMnSc1
<g/>
,	,	kIx,
Lauren	Laurna	k1gFnPc2
Harris	Harris	k1gFnPc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Richard	Richard	k1gMnSc1
Ian	Ian	k1gMnSc1
Faulkner	Faulkner	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1980	#num#	k4
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
britský	britský	k2eAgMnSc1d1
heavy	heavy	k2eAgMnSc1d1
metalový	metalový	k2eAgMnSc1d1
kytarista	kytarista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
doprovodné	doprovodný	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
dcery	dcera	k1gFnSc2
Stevea	Stevea	k1gFnSc1
Harrise	Harrise	k1gFnSc1
z	z	k7c2
Iron	iron	k1gInSc4
Maiden	Maidna	k1gFnPc2
Lauren	Laurna	k1gFnPc2
Harris	Harris	k1gFnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
nahradil	nahradit	k5eAaPmAgMnS
K.	K.	kA
K.	K.	kA
Downinga	Downing	k1gMnSc2
ve	v	k7c6
skupině	skupina	k1gFnSc6
Judas	Judas	k1gMnSc1
Priest	Priest	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Lauren	Laurna	k1gFnPc2
Harris	Harris	k1gFnSc2
–	–	k?
Calm	Calm	k1gMnSc1
Before	Befor	k1gInSc5
the	the	k?
Storm	Storm	k1gInSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Judas	Judas	k1gMnSc1
Priest	Priest	k1gMnSc1
–	–	k?
Redeemer	Redeemer	k1gMnSc1
of	of	k?
Souls	Souls	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Judas	Judas	k1gMnSc1
Priest	Priest	k1gMnSc1
Rob	roba	k1gFnPc2
Halford	Halford	k1gMnSc1
•	•	k?
Glenn	Glenn	k1gInSc1
Tipton	Tipton	k1gInSc1
•	•	k?
Ian	Ian	k1gMnSc1
Hill	Hill	k1gMnSc1
•	•	k?
Scott	Scott	k1gMnSc1
Travis	Travis	k1gFnSc2
•	•	k?
Richie	Richie	k1gFnSc2
Faulkner	Faulknra	k1gFnPc2
Bývalí	bývalý	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
K.	K.	kA
K.	K.	kA
Downing	Downing	k1gInSc1
•	•	k?
Al	ala	k1gFnPc2
Atkins	Atkinsa	k1gFnPc2
•	•	k?
John	John	k1gMnSc1
Ellis	Ellis	k1gFnSc2
•	•	k?
Alan	Alan	k1gMnSc1
Moore	Moor	k1gInSc5
•	•	k?
Chris	Chris	k1gFnSc2
"	"	kIx"
<g/>
Congo	Congo	k1gMnSc1
<g/>
"	"	kIx"
Campbell	Campbell	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Hinch	Hinch	k1gMnSc1
•	•	k?
Les	les	k1gInSc1
Binks	Binks	k1gInSc1
•	•	k?
Dave	Dav	k1gInSc5
Holland	Hollanda	k1gFnPc2
•	•	k?
Tim	Tim	k?
"	"	kIx"
<g/>
Ripper	Ripper	k1gMnSc1
<g/>
"	"	kIx"
Owens	Owens	k1gInSc1
Alba	album	k1gNnSc2
</s>
<s>
Rocka	Rocka	k1gMnSc1
Rolla	Rolla	k1gMnSc1
•	•	k?
Sad	sad	k1gInSc1
Wings	Wings	k1gInSc1
of	of	k?
Destiny	Destina	k1gFnSc2
•	•	k?
Sin	sin	kA
After	After	k1gInSc4
Sin	sin	kA
•	•	k?
Stained	Stained	k1gInSc1
Class	Class	k1gInSc1
•	•	k?
Killing	Killing	k1gInSc1
Machine	Machin	k1gInSc5
•	•	k?
British	British	k1gInSc1
Steel	Steel	k1gInSc1
•	•	k?
Point	pointa	k1gFnPc2
of	of	k?
Entry	Entr	k1gInPc1
•	•	k?
Screaming	Screaming	k1gInSc1
for	forum	k1gNnPc2
Vengeance	Vengeance	k1gFnSc2
•	•	k?
Defenders	Defenders	k1gInSc1
of	of	k?
the	the	k?
Faith	Faith	k1gInSc1
•	•	k?
Turbo	turba	k1gFnSc5
•	•	k?
Ram	Ram	k1gMnSc1
It	It	k1gMnSc1
Down	Down	k1gMnSc1
•	•	k?
Painkiller	Painkiller	k1gMnSc1
•	•	k?
Jugulator	Jugulator	k1gMnSc1
•	•	k?
Demolition	Demolition	k1gInSc1
•	•	k?
Angel	angel	k1gMnSc1
of	of	k?
Retribution	Retribution	k1gInSc1
•	•	k?
Nostradamus	Nostradamus	k1gInSc1
•	•	k?
Redeemer	Redeemer	k1gInSc1
of	of	k?
Souls	Souls	k1gInSc1
•	•	k?
Firepower	Firepower	k1gInSc1
Záznamy	záznam	k1gInPc7
</s>
<s>
Unleashed	Unleashed	k1gInSc1
in	in	k?
the	the	k?
East	East	k1gInSc1
•	•	k?
Priest	Priest	k1gInSc1
<g/>
...	...	k?
<g/>
Live	Live	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
•	•	k?
Live	Live	k1gInSc1
Meltdown	Meltdown	k1gInSc1
•	•	k?
Demolition	Demolition	k1gInSc1
•	•	k?
Live	Liv	k1gFnSc2
in	in	k?
London	London	k1gMnSc1
•	•	k?
A	a	k9
Touch	Touch	k1gMnSc1
of	of	k?
Evil	Evil	k1gMnSc1
<g/>
:	:	kIx,
Live	Live	k1gFnSc1
•	•	k?
British	British	k1gMnSc1
Steel	Steel	k1gMnSc1
<g/>
:	:	kIx,
30	#num#	k4
<g/>
th	th	k?
Anniversary	Anniversara	k1gFnSc2
Live	Liv	k1gFnSc2
Kompilace	kompilace	k1gFnSc2
a	a	k8xC
sady	sada	k1gFnSc2
</s>
<s>
The	The	k?
Best	Best	k1gMnSc1
of	of	k?
Judas	Judas	k1gMnSc1
Priest	Priest	k1gMnSc1
•	•	k?
Hero	Hero	k1gMnSc1
<g/>
,	,	kIx,
Hero	Hero	k1gMnSc1
•	•	k?
The	The	k1gFnSc1
Collection	Collection	k1gInSc1
•	•	k?
Genocide	Genocid	k1gInSc5
•	•	k?
Metal	metal	k1gInSc1
Works	Works	kA
•	•	k?
The	The	k1gMnSc1
Best	Best	k1gMnSc1
of	of	k?
Judas	Judas	k1gMnSc1
Priest	Priest	k1gMnSc1
<g/>
:	:	kIx,
Living	Living	k1gInSc1
After	After	k1gMnSc1
Midnight	Midnight	k1gMnSc1
•	•	k?
Metalogy	metalog	k1gMnPc4
•	•	k?
The	The	k1gFnSc1
Essential	Essential	k1gMnSc1
Judas	Judas	k1gMnSc1
Priest	Priest	k1gMnSc1
•	•	k?
Single	singl	k1gInSc5
Cuts	Cutsa	k1gFnPc2
•	•	k?
The	The	k1gMnSc1
Chosen	Chosen	k2eAgMnSc1d1
Few	Few	k1gMnSc1
DVD	DVD	kA
</s>
<s>
Live	Live	k6eAd1
in	in	k?
London	London	k1gMnSc1
•	•	k?
Electric	Electric	k1gMnSc1
Eye	Eye	k1gMnSc1
•	•	k?
Rising	Rising	k1gInSc1
in	in	k?
the	the	k?
East	East	k1gInSc1
•	•	k?
Live	Liv	k1gMnSc2
Vengeance	Vengeanec	k1gMnSc2
'	'	kIx"
<g/>
82	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1163324868	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
903153289927332770004	#num#	k4
</s>
