<p>
<s>
Děvčátko	děvčátko	k1gNnSc1	děvčátko
v	v	k7c6	v
kožuchu	kožuch	k1gInSc6	kožuch
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc1	obraz
malíře	malíř	k1gMnSc2	malíř
Joži	Joža	k1gMnSc2	Joža
Uprky	Uprka	k1gMnSc2	Uprka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
autor	autor	k1gMnSc1	autor
uvolněným	uvolněný	k2eAgInSc7d1	uvolněný
až	až	k9	až
impresionistickým	impresionistický	k2eAgInSc7d1	impresionistický
stylem	styl	k1gInSc7	styl
a	a	k8xC	a
jemnými	jemný	k2eAgInPc7d1	jemný
tóny	tón	k1gInPc7	tón
zachytil	zachytit	k5eAaPmAgMnS	zachytit
ponurou	ponurý	k2eAgFnSc4d1	ponurá
podzimní	podzimní	k2eAgFnSc4d1	podzimní
atmosféru	atmosféra	k1gFnSc4	atmosféra
s	s	k7c7	s
pozdně	pozdně	k6eAd1	pozdně
odpoledním	odpolední	k2eAgNnSc7d1	odpolední
nebem	nebe	k1gNnSc7	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Centrálním	centrální	k2eAgInSc7d1	centrální
výjevem	výjev	k1gInSc7	výjev
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
malé	malý	k2eAgNnSc1d1	malé
děvče	děvče	k1gNnSc1	děvče
z	z	k7c2	z
Kněždubu	Kněždub	k1gInSc2	Kněždub
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
dívkou	dívka	k1gFnSc7	dívka
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
pár	pár	k4xCyI	pár
stromů	strom	k1gInPc2	strom
s	s	k7c7	s
uschlými	uschlý	k2eAgInPc7d1	uschlý
listy	list	k1gInPc7	list
a	a	k8xC	a
pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
Děvčátko	děvčátko	k1gNnSc1	děvčátko
se	s	k7c7	s
klidným	klidný	k2eAgInSc7d1	klidný
pohledem	pohled	k1gInSc7	pohled
dívá	dívat	k5eAaImIp3nS	dívat
mimo	mimo	k7c4	mimo
</s>
</p>
<p>
<s>
diváky	divák	k1gMnPc4	divák
doprava	doprava	k6eAd1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
má	mít	k5eAaImIp3nS	mít
tradiční	tradiční	k2eAgInSc4d1	tradiční
kněždubský	kněždubský	k2eAgInSc4d1	kněždubský
kožuch	kožuch	k1gInSc4	kožuch
ovázaný	ovázaný	k2eAgInSc4d1	ovázaný
žlutým	žlutý	k2eAgInSc7d1	žlutý
šátkem	šátek	k1gInSc7	šátek
<g/>
,	,	kIx,	,
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
čepeček	čepeček	k1gInSc4	čepeček
a	a	k8xC	a
rudý	rudý	k2eAgInSc4d1	rudý
šátek	šátek	k1gInSc4	šátek
<g/>
.	.	kIx.	.
</s>
<s>
Tváře	tvář	k1gFnPc4	tvář
má	mít	k5eAaImIp3nS	mít
trošku	trošku	k6eAd1	trošku
narůžovělé	narůžovělý	k2eAgNnSc1d1	narůžovělé
od	od	k7c2	od
chladného	chladný	k2eAgInSc2d1	chladný
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Doplňující	doplňující	k2eAgFnPc4d1	doplňující
informace	informace	k1gFnPc4	informace
===	===	k?	===
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
se	se	k3xPyFc4	se
momentálně	momentálně	k6eAd1	momentálně
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
depozitáři	depozitář	k1gInSc6	depozitář
MG	mg	kA	mg
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
vystavován	vystavován	k2eAgInSc1d1	vystavován
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
souborné	souborný	k2eAgFnSc6d1	souborná
výstavě	výstava	k1gFnSc6	výstava
ke	k	k7c3	k
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
narození	narození	k1gNnSc2	narození
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Hodoníně	Hodonín	k1gInSc6	Hodonín
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
stálé	stálý	k2eAgFnSc6d1	stálá
expozici	expozice	k1gFnSc6	expozice
MG	mg	kA	mg
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Děvčátko	děvčátko	k1gNnSc1	děvčátko
v	v	k7c6	v
kožuchu	kožuch	k1gInSc6	kožuch
se	se	k3xPyFc4	se
neřadí	řadit	k5eNaImIp3nS	řadit
mezi	mezi	k7c4	mezi
zásadní	zásadní	k2eAgFnPc4d1	zásadní
díla	dílo	k1gNnPc4	dílo
Joži	Joža	k1gMnSc2	Joža
Uprky	Uprka	k1gMnSc2	Uprka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
JEDLIČKOVÁ	Jedličková	k1gFnSc1	Jedličková
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
.	.	kIx.	.
</s>
<s>
Joža	Joža	k1gFnSc1	Joža
Uprka	Uprka	k1gFnSc1	Uprka
<g/>
,	,	kIx,	,
Oblastní	oblastní	k2eAgFnSc1d1	oblastní
galerie	galerie	k1gFnSc1	galerie
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
</s>
</p>
<p>
<s>
JEŽ	Jež	k1gMnSc1	Jež
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
Joža	Joža	k1gMnSc1	Joža
Uprka	Uprka	k1gMnSc1	Uprka
<g/>
:	:	kIx,	:
k	k	k7c3	k
pátému	pátý	k4xOgNnSc3	pátý
výročí	výročí	k1gNnSc3	výročí
umělcovy	umělcův	k2eAgFnSc2d1	umělcova
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
</s>
</p>
<p>
<s>
Joža	Joža	k1gMnSc1	Joža
a	a	k8xC	a
Franta	Franta	k1gMnSc1	Franta
Uprkové	Uprková	k1gFnSc2	Uprková
<g/>
:	:	kIx,	:
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
<g/>
,	,	kIx,	,
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
Joža	Joža	k1gMnSc1	Joža
Uprka	Uprka	k1gMnSc1	Uprka
<g/>
,	,	kIx,	,
výstava	výstava	k1gFnSc1	výstava
souborného	souborný	k2eAgNnSc2d1	souborné
díla	dílo	k1gNnSc2	dílo
Joži	Joža	k1gFnSc2	Joža
Uprky	Uprka	k1gFnSc2	Uprka
k	k	k7c3	k
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Výročí	výročí	k1gNnSc1	výročí
jeho	on	k3xPp3gNnSc2	on
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc2	umění
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
Joža	Joža	k1gFnSc1	Joža
Uprka	Uprka	k1gFnSc1	Uprka
<g/>
:	:	kIx,	:
souborná	souborný	k2eAgFnSc1d1	souborná
výstava	výstava	k1gFnSc1	výstava
k	k	k7c3	k
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Výročí	výročí	k1gNnSc1	výročí
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
katalog	katalog	k1gInSc4	katalog
Domu	dům	k1gInSc2	dům
umění	umění	k1gNnSc1	umění
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
KAČER	kačer	k1gMnSc1	kačer
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Joža	Joža	k1gFnSc1	Joža
Uprka	Uprka	k1gFnSc1	Uprka
<g/>
,	,	kIx,	,
Moravská	moravský	k2eAgFnSc1d1	Moravská
galerie	galerie	k1gFnSc1	galerie
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
</s>
</p>
<p>
<s>
KAČER	kačer	k1gMnSc1	kačer
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Joža	Joža	k1gMnSc1	Joža
Uprka	Uprka	k1gMnSc1	Uprka
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
KARPÍŠKOVÁ	Karpíšková	k1gFnSc1	Karpíšková
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
moderního	moderní	k2eAgMnSc2d1	moderní
tvůrce	tvůrce	k1gMnSc2	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Joža	Joža	k1gMnSc1	Joža
Uprka	Uprka	k1gMnSc1	Uprka
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Č.	Č.	kA	Č.
Bud	bouda	k1gFnPc2	bouda
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
diplomová	diplomový	k2eAgFnSc1d1	Diplomová
</s>
</p>
<p>
<s>
práce	práce	k1gFnSc1	práce
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
JIHOČESKÁ	jihočeský	k2eAgFnSc1d1	Jihočeská
UNIVERZITA	univerzita	k1gFnSc1	univerzita
V	v	k7c6	v
ČESKÝCH	český	k2eAgInPc6d1	český
BUDĚJOVICÍCH	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
</s>
</p>
<p>
<s>
KLVAŇA	KLVAŇA	kA	KLVAŇA
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Uprka	Uprka	k1gMnSc1	Uprka
<g/>
:	:	kIx,	:
několik	několik	k4yIc4	několik
kapitol	kapitola	k1gFnPc2	kapitola
k	k	k7c3	k
charakteristice	charakteristika	k1gFnSc3	charakteristika
jeho	jeho	k3xOp3gNnSc2	jeho
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Literární	literární	k2eAgInSc1d1	literární
odbor	odbor	k1gInSc1	odbor
Moravsko-slezské	moravskolezský	k2eAgFnSc2d1	moravsko-slezská
</s>
</p>
<p>
<s>
Besedy	beseda	k1gFnPc1	beseda
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1897	[number]	k4	1897
</s>
</p>
<p>
<s>
KRETZ	KRETZ	kA	KRETZ
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Ženské	ženský	k2eAgInPc1d1	ženský
kožuchy	kožuch	k1gInPc1	kožuch
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
</s>
</p>
<p>
<s>
LOLEK	LOLEK	kA	LOLEK
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Joža	Joža	k1gFnSc1	Joža
Uprka	Uprka	k1gFnSc1	Uprka
<g/>
,	,	kIx,	,
Oblastní	oblastní	k2eAgFnSc1d1	oblastní
galerie	galerie	k1gFnSc1	galerie
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
</s>
</p>
<p>
<s>
Souborná	souborný	k2eAgFnSc1d1	souborná
výstava	výstava	k1gFnSc1	výstava
akvarelů	akvarel	k1gInPc2	akvarel
a	a	k8xC	a
olejomaleb	olejomalba	k1gFnPc2	olejomalba
Jože	Joža	k1gFnSc3	Joža
Uprky	Uprk	k1gInPc4	Uprk
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
1897	[number]	k4	1897
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1897	[number]	k4	1897
</s>
</p>
<p>
<s>
UPRKA	UPRKA	kA	UPRKA
<g/>
,	,	kIx,	,
Joža	Joža	k1gFnSc1	Joža
<g/>
.	.	kIx.	.
</s>
<s>
Kožuchy	Kožucha	k1gFnPc1	Kožucha
<g/>
.	.	kIx.	.
</s>
<s>
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
,1920	,1920	k4	,1920
</s>
</p>
<p>
<s>
UPRKA	UPRKA	kA	UPRKA
<g/>
,	,	kIx,	,
<g/>
J.	J.	kA	J.
<g/>
,	,	kIx,	,
KLVAŇA	KLVAŇA	kA	KLVAŇA
<g/>
,	,	kIx,	,
<g/>
J.	J.	kA	J.
a	a	k8xC	a
MRŠTÍK	MRŠTÍK	kA	MRŠTÍK
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Joža	Joža	k1gFnSc1	Joža
Uprka	Uprka	k1gFnSc1	Uprka
<g/>
:	:	kIx,	:
Výbor	výbor	k1gInSc1	výbor
jeho	jeho	k3xOp3gFnPc2	jeho
prací	práce	k1gFnPc2	práce
I.	I.	kA	I.
<g/>
,	,	kIx,	,
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
</s>
</p>
<p>
<s>
X.	X.	kA	X.
<g/>
výstava	výstava	k1gFnSc1	výstava
Sdružení	sdružení	k1gNnSc1	sdružení
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
moravských	moravský	k2eAgMnPc2d1	moravský
<g/>
,	,	kIx,	,
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
</s>
</p>
