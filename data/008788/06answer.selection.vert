<s>
Planeta	planeta	k1gFnSc1	planeta
Mars	Mars	k1gInSc1	Mars
má	mít	k5eAaImIp3nS	mít
načervenalou	načervenalý	k2eAgFnSc4d1	načervenalá
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
přítomnosti	přítomnost	k1gFnSc3	přítomnost
oxidů	oxid	k1gInPc2	oxid
železa	železo	k1gNnSc2	železo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yQgFnSc4	který
byl	být	k5eAaImAgMnS	být
asociován	asociován	k2eAgMnSc1d1	asociován
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Martem	Mars	k1gMnSc7	Mars
<g/>
.	.	kIx.	.
</s>
