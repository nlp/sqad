<s>
Jostein	Jostein	k2eAgInSc1d1	Jostein
Gaarder	Gaarder	k1gInSc1	Gaarder
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
norský	norský	k2eAgMnSc1d1	norský
spisovatel	spisovatel	k1gMnSc1	spisovatel
knih	kniha	k1gFnPc2	kniha
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
učitelské	učitelský	k2eAgFnSc6d1	učitelská
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
učitelka	učitelka	k1gFnSc1	učitelka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Inger	Ingero	k1gNnPc2	Ingero
Margrethe	Margreth	k1gFnSc2	Margreth
je	být	k5eAaImIp3nS	být
autorkou	autorka	k1gFnSc7	autorka
několika	několik	k4yIc2	několik
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jostein	Jostein	k1gMnSc1	Jostein
studoval	studovat	k5eAaImAgMnS	studovat
skandinávské	skandinávský	k2eAgInPc4d1	skandinávský
jazyky	jazyk	k1gInPc4	jazyk
a	a	k8xC	a
teologii	teologie	k1gFnSc4	teologie
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
filosofii	filosofie	k1gFnSc4	filosofie
a	a	k8xC	a
literaturu	literatura	k1gFnSc4	literatura
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Bergenu	Bergen	k1gInSc6	Bergen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
napsal	napsat	k5eAaPmAgMnS	napsat
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
kolegy	kolega	k1gMnPc7	kolega
šest	šest	k4xCc4	šest
učebnic	učebnice	k1gFnPc2	učebnice
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
úspěch	úspěch	k1gInSc1	úspěch
Sofiina	Sofiin	k2eAgInSc2d1	Sofiin
světa	svět	k1gInSc2	svět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
románu	román	k1gInSc2	román
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
"	"	kIx"	"
vydaného	vydaný	k2eAgInSc2d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
mu	on	k3xPp3gMnSc3	on
umožnil	umožnit	k5eAaPmAgInS	umožnit
se	s	k7c7	s
psaním	psaní	k1gNnSc7	psaní
živit	živit	k5eAaImF	živit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
knihách	kniha	k1gFnPc6	kniha
provází	provázet	k5eAaImIp3nS	provázet
dějinami	dějiny	k1gFnPc7	dějiny
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
základních	základní	k2eAgFnPc6d1	základní
otázkách	otázka	k1gFnPc6	otázka
lidské	lidský	k2eAgFnSc2d1	lidská
existence	existence	k1gFnSc2	existence
a	a	k8xC	a
nabádá	nabádat	k5eAaImIp3nS	nabádat
čtenáře	čtenář	k1gMnPc4	čtenář
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
neztráceli	ztrácet	k5eNaImAgMnP	ztrácet
schopnost	schopnost	k1gFnSc4	schopnost
okouzlení	okouzlení	k1gNnSc2	okouzlení
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Angažuje	angažovat	k5eAaBmIp3nS	angažovat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
a	a	k8xC	a
ekologii	ekologie	k1gFnSc6	ekologie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Nadace	nadace	k1gFnSc2	nadace
Sophie	Sophie	k1gFnSc2	Sophie
<g/>
.	.	kIx.	.
</s>
<s>
Diagnosen	Diagnosen	k2eAgInSc1d1	Diagnosen
og	og	k?	og
andre	andr	k1gInSc5	andr
noveller	noveller	k1gInSc4	noveller
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Barna	Baren	k2eAgFnSc1d1	Baren
fra	fra	k?	fra
Sukhavati	Sukhavati	k1gFnSc1	Sukhavati
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Froskeslottet	Froskeslottet	k1gInSc1	Froskeslottet
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Žabí	žabí	k2eAgInSc4d1	žabí
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
92	[number]	k4	92
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
2046	[number]	k4	2046
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Kabalmysteriet	Kabalmysteriet	k1gInSc1	Kabalmysteriet
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Tajemství	tajemství	k1gNnSc1	tajemství
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
322	[number]	k4	322
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
529	[number]	k4	529
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Sofies	Sofies	k1gInSc1	Sofies
verden	verdna	k1gFnPc2	verdna
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Sofiin	Sofiin	k2eAgInSc4d1	Sofiin
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Košice	Košice	k1gInPc1	Košice
:	:	kIx,	:
Knižná	knižný	k2eAgNnPc1d1	knižný
dielňa	dielňum	k1gNnPc1	dielňum
Timotej	Timotej	k1gFnPc2	Timotej
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
512	[number]	k4	512
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
967294	[number]	k4	967294
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Julemysteriet	Julemysteriet	k1gInSc1	Julemysteriet
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Kouzelný	kouzelný	k2eAgInSc1d1	kouzelný
kalendář	kalendář	k1gInSc1	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
241	[number]	k4	241
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7192	[number]	k4	7192
<g/>
-	-	kIx~	-
<g/>
682	[number]	k4	682
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Bibi	Bibi	k1gNnSc1	Bibi
Bokkens	Bokkensa	k1gFnPc2	Bokkensa
magiske	magiskat	k5eAaPmIp3nS	magiskat
bibliothek	bibliothek	k6eAd1	bibliothek
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
I	i	k9	i
et	et	k?	et
speil	speit	k5eAaPmAgInS	speit
<g/>
,	,	kIx,	,
i	i	k8xC	i
en	en	k?	en
gate	gate	k1gFnSc1	gate
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Jako	jako	k9	jako
v	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
<g/>
,	,	kIx,	,
jen	jen	k9	jen
v	v	k7c6	v
hádance	hádanka	k1gFnSc6	hádanka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
136	[number]	k4	136
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
706	[number]	k4	706
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Hallo	Hallo	k1gNnSc1	Hallo
<g/>
?	?	kIx.	?
</s>
<s>
Er	Er	k?	Er
det	det	k?	det
noen	noen	k1gInSc1	noen
her	hra	k1gFnPc2	hra
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Haló	haló	k0	haló
<g/>
!	!	kIx.	!
</s>
<s>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
někdo	někdo	k3yInSc1	někdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
73	[number]	k4	73
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
617	[number]	k4	617
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Vita	vit	k2eAgFnSc1d1	Vita
brevis	brevis	k1gFnSc1	brevis
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Vita	vit	k2eAgFnSc1d1	Vita
brevis	brevis	k1gFnSc1	brevis
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
Florie	Florie	k1gFnSc2	Florie
Aemilie	Aemilie	k1gFnSc2	Aemilie
Aureliovi	Aurelius	k1gMnSc3	Aurelius
Augustinovi	Augustin	k1gMnSc3	Augustin
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
269	[number]	k4	269
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
551	[number]	k4	551
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Maya	Maya	k?	Maya
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Sirkusdirektø	Sirkusdirektø	k1gMnSc1	Sirkusdirektø
datter	datter	k1gMnSc1	datter
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Principálova	principálův	k2eAgFnSc1d1	Principálova
dcera	dcera	k1gFnSc1	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
206	[number]	k4	206
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
1199	[number]	k4	1199
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Appelsinpiken	Appelsinpiken	k1gInSc1	Appelsinpiken
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Dívka	dívka	k1gFnSc1	dívka
s	s	k7c7	s
pomeranči	pomeranč	k1gInPc7	pomeranč
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
158	[number]	k4	158
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1405	[number]	k4	1405
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
<g/>
)	)	kIx)	)
Sjakk	Sjakk	k1gMnSc1	Sjakk
Matt	Matt	k1gMnSc1	Matt
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
De	De	k?	De
gule	gula	k1gFnSc6	gula
dvergene	dvergen	k1gInSc5	dvergen
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Slottet	Slottet	k1gInSc1	Slottet
i	i	k8xC	i
Pyreneene	Pyreneen	k1gInSc5	Pyreneen
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Hrad	hrad	k1gInSc1	hrad
v	v	k7c6	v
Pyrenejích	Pyreneje	k1gFnPc6	Pyreneje
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
168	[number]	k4	168
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2643	[number]	k4	2643
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Det	Det	k1gFnSc1	Det
spø	spø	k?	spø
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
otázka	otázka	k1gFnSc1	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
72	[number]	k4	72
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3075	[number]	k4	3075
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Dukkeforeren	Dukkeforerna	k1gFnPc2	Dukkeforerna
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Loutkař	loutkařit	k5eAaImRp2nS	loutkařit
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Plus	plus	k1gInSc1	plus
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
248	[number]	k4	248
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
259	[number]	k4	259
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
634	[number]	k4	634
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
Anna	Anna	k1gFnSc1	Anna
–	–	k?	–
Jaký	jaký	k3yIgInSc1	jaký
bude	být	k5eAaImBp3nS	být
rok	rok	k1gInSc1	rok
2082	[number]	k4	2082
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
)	)	kIx)	)
Anně	Anna	k1gFnSc6	Anna
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
baví	bavit	k5eAaImIp3nS	bavit
ji	on	k3xPp3gFnSc4	on
přírodovědný	přírodovědný	k2eAgInSc1d1	přírodovědný
kroužek	kroužek	k1gInSc1	kroužek
a	a	k8xC	a
moc	moc	k1gFnSc1	moc
by	by	kYmCp3nS	by
chtěla	chtít	k5eAaImAgFnS	chtít
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bude	být	k5eAaImBp3nS	být
vypadat	vypadat	k5eAaImF	vypadat
svět	svět	k1gInSc4	svět
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
vyplní	vyplnit	k5eAaPmIp3nS	vyplnit
–	–	k?	–
po	po	k7c6	po
gaarderovsku	gaarderovsko	k1gNnSc6	gaarderovsko
–	–	k?	–
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
snu	sen	k1gInSc2	sen
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
se	se	k3xPyFc4	se
ocitne	ocitnout	k5eAaPmIp3nS	ocitnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2082	[number]	k4	2082
a	a	k8xC	a
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
svět	svět	k1gInSc1	svět
budoucnosti	budoucnost	k1gFnSc2	budoucnost
vypadá	vypadat	k5eAaPmIp3nS	vypadat
docela	docela	k6eAd1	docela
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Kam	kam	k6eAd1	kam
zmizel	zmizet	k5eAaPmAgMnS	zmizet
hmyz	hmyz	k1gInSc4	hmyz
potřebný	potřebný	k2eAgInSc4d1	potřebný
k	k	k7c3	k
opylování	opylování	k1gNnSc3	opylování
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
vyhynula	vyhynout	k5eAaPmAgFnS	vyhynout
některá	některý	k3yIgNnPc4	některý
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
?	?	kIx.	?
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
najdou	najít	k5eAaPmIp3nP	najít
čtenáři	čtenář	k1gMnPc1	čtenář
řadu	řada	k1gFnSc4	řada
opakujících	opakující	k2eAgInPc2d1	opakující
se	se	k3xPyFc4	se
motivů	motiv	k1gInPc2	motiv
ze	z	k7c2	z
Sofiina	Sofiin	k2eAgInSc2d1	Sofiin
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
dějin	dějiny	k1gFnPc2	dějiny
myšlení	myšlení	k1gNnSc2	myšlení
se	se	k3xPyFc4	se
román	román	k1gInSc1	román
zajímá	zajímat	k5eAaImIp3nS	zajímat
o	o	k7c4	o
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
ekologii	ekologie	k1gFnSc4	ekologie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
otázka	otázka	k1gFnSc1	otázka
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
)	)	kIx)	)
Odkud	odkud	k6eAd1	odkud
a	a	k8xC	a
kam	kam	k6eAd1	kam
jdeme	jít	k5eAaImIp1nP	jít
<g/>
?	?	kIx.	?
</s>
<s>
Byl	být	k5eAaImAgInS	být
svět	svět	k1gInSc1	svět
odjakživa	odjakživa	k6eAd1	odjakživa
<g/>
?	?	kIx.	?
</s>
<s>
Bude	být	k5eAaImBp3nS	být
dál	daleko	k6eAd2	daleko
existovat	existovat	k5eAaImF	existovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
tu	tu	k6eAd1	tu
nebudu	být	k5eNaImBp1nS	být
<g/>
?	?	kIx.	?
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
sestavil	sestavit	k5eAaPmAgInS	sestavit
Jostein	Jostein	k2eAgInSc1d1	Jostein
Gaarder	Gaarder	k1gInSc1	Gaarder
s	s	k7c7	s
hlubokým	hluboký	k2eAgNnSc7d1	hluboké
porozuměním	porozumění	k1gNnSc7	porozumění
a	a	k8xC	a
citem	cit	k1gInSc7	cit
pro	pro	k7c4	pro
dětské	dětský	k2eAgNnSc4d1	dětské
chápání	chápání	k1gNnSc4	chápání
a	a	k8xC	a
vnímání	vnímání	k1gNnSc4	vnímání
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
otázka	otázka	k1gFnSc1	otázka
navozuje	navozovat	k5eAaImIp3nS	navozovat
okruh	okruh	k1gInSc4	okruh
dalších	další	k2eAgNnPc2d1	další
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
témat	téma	k1gNnPc2	téma
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgInPc6	jenž
si	se	k3xPyFc3	se
budou	být	k5eAaImBp3nP	být
děti	dítě	k1gFnPc1	dítě
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
povídat	povídat	k5eAaImF	povídat
<g/>
.	.	kIx.	.
</s>
<s>
Jostein	Jostein	k1gMnSc1	Jostein
Gaarder	Gaarder	k1gMnSc1	Gaarder
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
románu	román	k1gInSc2	román
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
Sofiin	Sofiin	k2eAgInSc4d1	Sofiin
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejně	stejně	k6eAd1	stejně
citlivě	citlivě	k6eAd1	citlivě
dokáže	dokázat	k5eAaPmIp3nS	dokázat
promlouvat	promlouvat	k5eAaImF	promlouvat
k	k	k7c3	k
dětem	dítě	k1gFnPc3	dítě
jako	jako	k8xS	jako
k	k	k7c3	k
teenagerům	teenager	k1gMnPc3	teenager
<g/>
.	.	kIx.	.
</s>
<s>
Dívka	dívka	k1gFnSc1	dívka
s	s	k7c7	s
pomeranči	pomeranč	k1gInPc7	pomeranč
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
stuha	stuha	k1gFnSc1	stuha
za	za	k7c4	za
překlad	překlad	k1gInSc4	překlad
<g/>
)	)	kIx)	)
Oduševněle	oduševněle	k6eAd1	oduševněle
napsaný	napsaný	k2eAgInSc4d1	napsaný
zamilovaný	zamilovaný	k2eAgInSc4d1	zamilovaný
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
osloví	oslovit	k5eAaPmIp3nS	oslovit
čtenáře	čtenář	k1gMnPc4	čtenář
všech	všecek	k3xTgFnPc2	všecek
věkových	věkový	k2eAgFnPc2d1	věková
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jednoho	jeden	k4xCgInSc2	jeden
večera	večer	k1gInSc2	večer
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgNnSc2	jenž
15	[number]	k4	15
<g/>
letý	letý	k2eAgMnSc1d1	letý
hrdina	hrdina	k1gMnSc1	hrdina
Georg	Georg	k1gMnSc1	Georg
čte	číst	k5eAaImIp3nS	číst
dopis	dopis	k1gInSc4	dopis
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
před	před	k7c7	před
jeho	jeho	k3xOp3gNnPc7	jeho
očima	oko	k1gNnPc7	oko
celý	celý	k2eAgInSc4d1	celý
příběh	příběh	k1gInSc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Georgův	Georgův	k2eAgMnSc1d1	Georgův
otec	otec	k1gMnSc1	otec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
před	před	k7c7	před
jedenácti	jedenáct	k4xCc2	jedenáct
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
dopis	dopis	k1gInSc1	dopis
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
teprve	teprve	k6eAd1	teprve
nyní	nyní	k6eAd1	nyní
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
líčení	líčení	k1gNnSc2	líčení
prostého	prostý	k2eAgInSc2d1	prostý
příběhu	příběh	k1gInSc2	příběh
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
pokládá	pokládat	k5eAaImIp3nS	pokládat
otec	otec	k1gMnSc1	otec
synovi	syn	k1gMnSc3	syn
nejednoduché	jednoduchý	k2eNgFnSc2d1	nejednoduchá
otázky	otázka	k1gFnSc2	otázka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
samotné	samotný	k2eAgFnSc2d1	samotná
podstaty	podstata	k1gFnSc2	podstata
lidské	lidský	k2eAgFnSc2d1	lidská
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Sofiin	Sofiin	k2eAgInSc1d1	Sofiin
svět	svět	k1gInSc1	svět
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
)	)	kIx)	)
Román	Román	k1gMnSc1	Román
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
Norský	norský	k2eAgMnSc1d1	norský
učitel	učitel	k1gMnSc1	učitel
J.	J.	kA	J.
Gaarder	Gaarder	k1gInSc4	Gaarder
kombinací	kombinace	k1gFnSc7	kombinace
zdánlivě	zdánlivě	k6eAd1	zdánlivě
neatraktivních	atraktivní	k2eNgNnPc2d1	neatraktivní
témat	téma	k1gNnPc2	téma
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
dějiny	dějiny	k1gFnPc1	dějiny
filosofie	filosofie	k1gFnSc1	filosofie
a	a	k8xC	a
příběh	příběh	k1gInSc1	příběh
dospívání	dospívání	k1gNnSc2	dospívání
patnáctileté	patnáctiletý	k2eAgFnSc2d1	patnáctiletá
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
knížku	knížka	k1gFnSc4	knížka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slaví	slavit	k5eAaImIp3nS	slavit
mezi	mezi	k7c7	mezi
čtenáři	čtenář	k1gMnPc7	čtenář
neuvěřitelný	uvěřitelný	k2eNgInSc4d1	neuvěřitelný
úspěch	úspěch	k1gInSc4	úspěch
<g/>
;	;	kIx,	;
vzápětí	vzápětí	k6eAd1	vzápětí
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
norském	norský	k2eAgNnSc6d1	norské
vydání	vydání	k1gNnSc6	vydání
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
desítkách	desítka	k1gFnPc6	desítka
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejprodávanější	prodávaný	k2eAgFnSc7d3	nejprodávanější
knihou	kniha	k1gFnSc7	kniha
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
