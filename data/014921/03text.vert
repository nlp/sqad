<s>
Pediatrie	pediatrie	k1gFnSc1
</s>
<s>
Očkování	očkování	k1gNnSc4
proti	proti	k7c3
dětské	dětský	k2eAgFnSc3d1
obrně	obrna	k1gFnSc3
v	v	k7c6
Bangladéši	Bangladéš	k1gInSc6
</s>
<s>
Pediatrie	pediatrie	k1gFnSc1
neboli	neboli	k8xC
dětské	dětský	k2eAgNnSc1d1
lékařství	lékařství	k1gNnSc1
je	být	k5eAaImIp3nS
obor	obor	k1gInSc4
vnitřního	vnitřní	k2eAgNnSc2d1
lékařství	lékařství	k1gNnSc2
zabývající	zabývající	k2eAgFnSc2d1
se	se	k3xPyFc4
péčí	péče	k1gFnSc7
o	o	k7c6
zdraví	zdraví	k1gNnSc6
kojenců	kojenec	k1gMnPc2
<g/>
,	,	kIx,
dětí	dítě	k1gFnPc2
a	a	k8xC
mladistvých	mladistvý	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horní	horní	k2eAgFnSc1d1
věková	věkový	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
pacientů	pacient	k1gMnPc2
se	se	k3xPyFc4
v	v	k7c6
různých	různý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
liší	lišit	k5eAaImIp3nS
a	a	k8xC
pohybuje	pohybovat	k5eAaImIp3nS
se	se	k3xPyFc4
od	od	k7c2
14	#num#	k4
do	do	k7c2
21	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
je	být	k5eAaImIp3nS
touto	tento	k3xDgFnSc7
hranicí	hranice	k1gFnSc7
den	den	k1gInSc4
předcházející	předcházející	k2eAgInSc4d1
19	#num#	k4
<g/>
.	.	kIx.
narozeninám	narozeniny	k1gFnPc3
(	(	kIx(
<g/>
v	v	k7c6
odůvodněných	odůvodněný	k2eAgInPc6d1
případech	případ	k1gInPc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
věk	věk	k1gInSc1
i	i	k8xC
vyšší	vysoký	k2eAgFnSc1d2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Praktickému	praktický	k2eAgMnSc3d1
lékaři	lékař	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
tímto	tento	k3xDgInSc7
oborem	obor	k1gInSc7
zabývá	zabývat	k5eAaImIp3nS
<g/>
,	,	kIx,
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
také	také	k9
pediatr	pediatr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Slovo	slovo	k1gNnSc1
pediatrie	pediatrie	k1gFnSc2
(	(	kIx(
<g/>
a	a	k8xC
slova	slovo	k1gNnSc2
z	z	k7c2
něj	on	k3xPp3gMnSc2
odvozená	odvozený	k2eAgFnSc1d1
<g/>
)	)	kIx)
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
řeckého	řecký	k2eAgInSc2d1
π	π	k?
(	(	kIx(
<g/>
pais	pais	k1gInSc1
=	=	kIx~
dítě	dítě	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
ι	ι	k?
(	(	kIx(
<g/>
iatros	iatrosa	k1gFnPc2
=	=	kIx~
lékař	lékař	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
znamená	znamenat	k5eAaImIp3nS
tedy	tedy	k9
dětský	dětský	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Pediatrie	pediatrie	k1gFnSc1
obecně	obecně	k6eAd1
</s>
<s>
Rozdíly	rozdíl	k1gInPc1
mezi	mezi	k7c7
léčením	léčení	k1gNnSc7
dětí	dítě	k1gFnPc2
a	a	k8xC
dospělých	dospělí	k1gMnPc2
</s>
<s>
Léčba	léčba	k1gFnSc1
dětí	dítě	k1gFnPc2
a	a	k8xC
dospělých	dospělí	k1gMnPc2
se	se	k3xPyFc4
od	od	k7c2
sebe	sebe	k3xPyFc4
liší	lišit	k5eAaImIp3nP
v	v	k7c6
mnoha	mnoho	k4c6
ohledech	ohled	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíly	rozdíl	k1gInPc1
ve	v	k7c4
velikosti	velikost	k1gFnPc4
lidského	lidský	k2eAgNnSc2d1
těla	tělo	k1gNnSc2
odpovídají	odpovídat	k5eAaImIp3nP
i	i	k9
rozdílům	rozdíl	k1gInPc3
a	a	k8xC
změnám	změna	k1gFnPc3
během	během	k7c2
dospívání	dospívání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgInSc4d2
tělo	tělo	k1gNnSc1
kojenců	kojenec	k1gMnPc2
či	či	k8xC
novorozeňat	novorozeně	k1gNnPc2
se	se	k3xPyFc4
fyziologicky	fyziologicky	k6eAd1
podstatně	podstatně	k6eAd1
liší	lišit	k5eAaImIp3nP
od	od	k7c2
těla	tělo	k1gNnSc2
dospělého	dospělý	k2eAgMnSc2d1
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pediatři	pediatr	k1gMnPc1
si	se	k3xPyFc3
musí	muset	k5eAaImIp3nP
více	hodně	k6eAd2
všímat	všímat	k5eAaImF
vrozených	vrozený	k2eAgFnPc2d1
a	a	k8xC
vývojových	vývojový	k2eAgFnPc2d1
vad	vada	k1gFnPc2
nebo	nebo	k8xC
genetických	genetický	k2eAgFnPc2d1
odlišností	odlišnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Také	také	k9
spoustu	spousta	k1gFnSc4
dědičných	dědičný	k2eAgFnPc2d1
chorob	choroba	k1gFnPc2
mnohem	mnohem	k6eAd1
častěji	často	k6eAd2
léčí	léčit	k5eAaImIp3nP
pediatři	pediatr	k1gMnPc1
než	než	k8xS
lékaři	lékař	k1gMnPc1
pro	pro	k7c4
dospělé	dospělý	k2eAgFnPc4d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
ještě	ještě	k9
donedávna	donedávna	k6eAd1
se	se	k3xPyFc4
většina	většina	k1gFnSc1
takto	takto	k6eAd1
nemocných	nemocný	k1gMnPc2
dospělosti	dospělost	k1gFnSc2
vůbec	vůbec	k9
nedožila	dožít	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejznámější	známý	k2eAgInPc4d3
příklady	příklad	k1gInPc4
patří	patřit	k5eAaImIp3nS
srpkovitá	srpkovitý	k2eAgFnSc1d1
anémie	anémie	k1gFnSc1
<g/>
,	,	kIx,
cystická	cystický	k2eAgFnSc1d1
fibróza	fibróza	k1gFnSc1
nebo	nebo	k8xC
thalasemie	thalasemie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Infekčními	infekční	k2eAgFnPc7d1
chorobami	choroba	k1gFnPc7
a	a	k8xC
imunizací	imunizace	k1gFnSc7
se	se	k3xPyFc4
opět	opět	k6eAd1
zabývají	zabývat	k5eAaImIp3nP
především	především	k9
dětští	dětský	k2eAgMnPc1d1
lékaři	lékař	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Dětství	dětství	k1gNnSc1
je	být	k5eAaImIp3nS
období	období	k1gNnSc4
nejvýraznějšího	výrazný	k2eAgInSc2d3
růstu	růst	k1gInSc2
<g/>
,	,	kIx,
vývoje	vývoj	k1gInSc2
a	a	k8xC
dozrávání	dozrávání	k1gNnSc4
všech	všecek	k3xTgInPc2
orgánů	orgán	k1gInPc2
lidského	lidský	k2eAgNnSc2d1
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
rozpoznání	rozpoznání	k1gNnSc3
normálních	normální	k2eAgFnPc2d1
odlišností	odlišnost	k1gFnPc2
od	od	k7c2
těch	ten	k3xDgFnPc2
patologických	patologický	k2eAgFnPc2d1
vedou	vést	k5eAaImIp3nP
léta	léto	k1gNnPc1
školení	školení	k1gNnSc1
a	a	k8xC
praxe	praxe	k1gFnSc1
nad	nad	k7c4
rámec	rámec	k1gInSc4
běžného	běžný	k2eAgNnSc2d1
lékařského	lékařský	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Léčit	léčit	k5eAaImF
dítě	dítě	k1gNnSc4
není	být	k5eNaImIp3nS
stejné	stejný	k2eAgNnSc1d1
jako	jako	k9
léčit	léčit	k5eAaImF
zmenšeného	zmenšený	k2eAgMnSc4d1
dospělého	dospělý	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
rozdíl	rozdíl	k1gInSc1
mezi	mezi	k7c7
léčením	léčení	k1gNnSc7
dětí	dítě	k1gFnPc2
a	a	k8xC
dospělých	dospělí	k1gMnPc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
děti	dítě	k1gFnPc1
<g/>
,	,	kIx,
ve	v	k7c6
většině	většina	k1gFnSc6
právních	právní	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
nemohou	moct	k5eNaImIp3nP
rozhodovat	rozhodovat	k5eAaImF
samy	sám	k3xTgFnPc1
za	za	k7c4
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pediatr	pediatr	k1gMnSc1
vždy	vždy	k6eAd1
musí	muset	k5eAaImIp3nS
brát	brát	k5eAaImF
ohled	ohled	k1gInSc4
na	na	k7c4
otázku	otázka	k1gFnSc4
poručnictví	poručnictví	k1gNnSc2
<g/>
,	,	kIx,
práva	právo	k1gNnPc1
na	na	k7c6
soukromí	soukromí	k1gNnSc6
<g/>
,	,	kIx,
právní	právní	k2eAgFnSc2d1
odpovědnosti	odpovědnost	k1gFnSc2
a	a	k8xC
informovaného	informovaný	k2eAgInSc2d1
souhlasu	souhlas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinými	jiný	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
<g/>
,	,	kIx,
pediatr	pediatr	k1gMnSc1
musí	muset	k5eAaImIp3nS
častěji	často	k6eAd2
jednat	jednat	k5eAaImF
i	i	k9
s	s	k7c7
rodiči	rodič	k1gMnPc7
(	(	kIx(
<g/>
a	a	k8xC
někdy	někdy	k6eAd1
s	s	k7c7
celou	celý	k2eAgFnSc7d1
rodinou	rodina	k1gFnSc7
<g/>
)	)	kIx)
než	než	k8xS
jen	jen	k9
se	s	k7c7
samotným	samotný	k2eAgNnSc7d1
dítětem	dítě	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotnou	samotný	k2eAgFnSc4d1
právní	právní	k2eAgFnSc4d1
kategorii	kategorie	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nP
mladiství	mladistvý	k2eAgMnPc1d1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
mají	mít	k5eAaImIp3nP
za	za	k7c2
jistých	jistý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
právo	právo	k1gNnSc4
sami	sám	k3xTgMnPc1
rozhodovat	rozhodovat	k5eAaImF
o	o	k7c6
jim	on	k3xPp3gNnPc3
poskytované	poskytovaný	k2eAgFnPc4d1
zdravotní	zdravotní	k2eAgFnSc4d1
péči	péče	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
právo	právo	k1gNnSc1
ovšem	ovšem	k9
často	často	k6eAd1
podléhá	podléhat	k5eAaImIp3nS
změnám	změna	k1gFnPc3
a	a	k8xC
liší	lišit	k5eAaImIp3nS
se	se	k3xPyFc4
stát	stát	k5eAaImF,k5eAaPmF
od	od	k7c2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vzdělávání	vzdělávání	k1gNnSc1
pediatrů	pediatr	k1gMnPc2
</s>
<s>
Vzdělávání	vzdělávání	k1gNnSc1
pediatrů	pediatr	k1gMnPc2
je	být	k5eAaImIp3nS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
částech	část	k1gFnPc6
světa	svět	k1gInSc2
jiné	jiný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
vzato	vzít	k5eAaPmNgNnS
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
všichni	všechen	k3xTgMnPc1
ostatní	ostatní	k2eAgMnPc1d1
praktičtí	praktický	k2eAgMnPc1d1
lékaři	lékař	k1gMnPc1
i	i	k8xC
pediatři	pediatr	k1gMnPc1
nejprve	nejprve	k6eAd1
absolvují	absolvovat	k5eAaPmIp3nP
třístupňový	třístupňový	k2eAgInSc4d1
studijní	studijní	k2eAgInSc4d1
cyklus	cyklus	k1gInSc4
na	na	k7c6
lékařské	lékařský	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
příslušné	příslušný	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
a	a	k8xC
poté	poté	k6eAd1
obdrží	obdržet	k5eAaPmIp3nS
lékařský	lékařský	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Záleží	záležet	k5eAaImIp3nS
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
příslušné	příslušný	k2eAgFnSc3d1
právní	právní	k2eAgFnSc3d1
normě	norma	k1gFnSc3
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
k	k	k7c3
přijetí	přijetí	k1gNnSc3
na	na	k7c4
lékařský	lékařský	k2eAgInSc4d1
obor	obor	k1gInSc4
nutné	nutný	k2eAgNnSc4d1
předchozí	předchozí	k2eAgNnSc4d1
vysokoškolské	vysokoškolský	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
či	či	k8xC
nikoliv	nikoliv	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
způsob	způsob	k1gInSc1
je	být	k5eAaImIp3nS
častější	častý	k2eAgInSc1d2
v	v	k7c6
zemích	zem	k1gFnPc6
Commonwealthu	Commonwealth	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgNnSc1d1
studium	studium	k1gNnSc1
pak	pak	k6eAd1
trvá	trvat	k5eAaImIp3nS
pět	pět	k4xCc4
až	až	k9
šest	šest	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
USA	USA	kA
ale	ale	k8xC
například	například	k6eAd1
uchazeči	uchazeč	k1gMnPc1
o	o	k7c4
magisterské	magisterský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
potom	potom	k6eAd1
čtyř	čtyři	k4xCgFnPc2
až	až	k8xS
pětileté	pětiletý	k2eAgInPc1d1
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
již	již	k6eAd1
mít	mít	k5eAaImF
titul	titul	k1gInSc4
z	z	k7c2
předešlého	předešlý	k2eAgNnSc2d1
tří	tři	k4xCgFnPc2
až	až	k9
čtyřletého	čtyřletý	k2eAgNnSc2d1
studia	studio	k1gNnSc2
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
pravidlem	pravidlo	k1gNnSc7
<g/>
,	,	kIx,
z	z	k7c2
nějakého	nějaký	k3yIgInSc2
přírodovědného	přírodovědný	k2eAgInSc2d1
oboru	obor	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolvent	absolvent	k1gMnSc1
medicíny	medicína	k1gFnSc2
obdrží	obdržet	k5eAaPmIp3nS
titul	titul	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
podoba	podoba	k1gFnSc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
podle	podle	k7c2
státu	stát	k1gInSc2
a	a	k8xC
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yIgNnSc4,k3yRgNnSc4,k3yQgNnSc4
jej	on	k3xPp3gNnSc4
získal	získat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
titul	titul	k1gInSc1
jej	on	k3xPp3gNnSc4
pak	pak	k6eAd1
opravňuje	opravňovat	k5eAaImIp3nS
k	k	k7c3
vykonávání	vykonávání	k1gNnSc3
lékařské	lékařský	k2eAgFnSc2d1
praxe	praxe	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pediatři	pediatr	k1gMnPc1
se	se	k3xPyFc4
poté	poté	k6eAd1
musí	muset	k5eAaImIp3nS
dále	daleko	k6eAd2
vzdělávat	vzdělávat	k5eAaImF
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
oboru	obor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
příslušných	příslušný	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
nebo	nebo	k8xC
stupně	stupeň	k1gInSc2
specializace	specializace	k1gFnSc2
to	ten	k3xDgNnSc4
obvykle	obvykle	k6eAd1
trvá	trvat	k5eAaImIp3nS
od	od	k7c2
tří	tři	k4xCgInPc2
do	do	k7c2
šesti	šest	k4xCc2
let	léto	k1gNnPc2
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
samozřejmě	samozřejmě	k6eAd1
i	i	k8xC
více	hodně	k6eAd2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
praktické	praktický	k2eAgMnPc4d1
lékaře	lékař	k1gMnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
práce	práce	k1gFnSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
primární	primární	k2eAgFnSc6d1
péči	péče	k1gFnSc6
<g/>
,	,	kIx,
toto	tento	k3xDgNnSc4
studium	studium	k1gNnSc4
bývá	bývat	k5eAaImIp3nS
kratší	krátký	k2eAgFnSc1d2
než	než	k8xS
u	u	k7c2
nemocničních	nemocniční	k2eAgMnPc2d1
specialistů	specialista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
většině	většina	k1gFnSc6
zemí	zem	k1gFnPc2
je	být	k5eAaImIp3nS
první	první	k4xOgInSc1
cyklus	cyklus	k1gInSc1
studia	studio	k1gNnSc2
obvykle	obvykle	k6eAd1
společný	společný	k2eAgMnSc1d1
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
lékařské	lékařský	k2eAgFnPc4d1
profese	profes	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Najdou	najít	k5eAaPmIp3nP
se	se	k3xPyFc4
ale	ale	k8xC
i	i	k9
takové	takový	k3xDgFnSc2
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
pediatři	pediatr	k1gMnPc1
profilují	profilovat	k5eAaImIp3nP
ještě	ještě	k9
před	před	k7c7
ukončením	ukončení	k1gNnSc7
prvního	první	k4xOgInSc2
cyklu	cyklus	k1gInSc2
studia	studio	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
budoucí	budoucí	k2eAgMnSc1d1
pediatři	pediatr	k1gMnPc1
po	po	k7c6
ukončení	ukončení	k1gNnSc6
tohoto	tento	k3xDgInSc2
prvního	první	k4xOgInSc2
cyklu	cyklus	k1gInSc2
začínají	začínat	k5eAaImIp3nP
rovnou	rovnou	k6eAd1
studovat	studovat	k5eAaImF
obor	obor	k1gInSc4
pediatrie	pediatrie	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
musí	muset	k5eAaImIp3nS
ještě	ještě	k6eAd1
předtím	předtím	k6eAd1
absolvovat	absolvovat	k5eAaPmF
další	další	k2eAgNnSc4d1
vzdělávání	vzdělávání	k1gNnSc4
v	v	k7c6
oboru	obor	k1gInSc6
všeobecného	všeobecný	k2eAgNnSc2d1
lékařství	lékařství	k1gNnSc2
a	a	k8xC
až	až	k9
poté	poté	k6eAd1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
dále	daleko	k6eAd2
specializovat	specializovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Častěji	často	k6eAd2
než	než	k8xS
pod	pod	k7c4
univerzitu	univerzita	k1gFnSc4
tito	tento	k3xDgMnPc1
specialisté	specialista	k1gMnPc1
spadají	spadat	k5eAaPmIp3nP,k5eAaImIp3nP
pod	pod	k7c4
různé	různý	k2eAgFnPc4d1
pediatrické	pediatrický	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
bývají	bývat	k5eAaImIp3nP
jednotlivými	jednotlivý	k2eAgFnPc7d1
vládami	vláda	k1gFnPc7
podporovány	podporovat	k5eAaImNgFnP
různou	různý	k2eAgFnSc7d1
měrou	míra	k1gFnSc7wR
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
pediatrických	pediatrický	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
je	být	k5eAaImIp3nS
celonárodních	celonárodní	k2eAgFnPc2d1
<g/>
,	,	kIx,
jako	jako	k9
například	například	k6eAd1
Česká	český	k2eAgFnSc1d1
pediatrická	pediatrický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Užší	úzký	k2eAgFnSc1d2
specializace	specializace	k1gFnSc1
v	v	k7c6
pediatrii	pediatrie	k1gFnSc6
</s>
<s>
Plané	planý	k2eAgFnPc1d1
neštovice	neštovice	k1gFnPc1
</s>
<s>
Pediatři	pediatr	k1gMnPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
dále	daleko	k6eAd2
specializovat	specializovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdělávání	vzdělávání	k1gNnSc1
specializovaných	specializovaný	k2eAgInPc2d1
pediatrů	pediatr	k1gMnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
mnohých	mnohý	k2eAgInPc6d1
ohledech	ohled	k1gInPc6
podobné	podobný	k2eAgInPc4d1
tomu	ten	k3xDgNnSc3
u	u	k7c2
specialistů	specialista	k1gMnPc2
pro	pro	k7c4
dospělé	dospělý	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc1d3
rozdíl	rozdíl	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
typech	typ	k1gInPc6
nemocí	nemoc	k1gFnPc2
<g/>
,	,	kIx,
kterými	který	k3yQgFnPc7,k3yRgFnPc7,k3yIgFnPc7
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nP
zabývat	zabývat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemoci	nemoc	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
často	často	k6eAd1
vyskytují	vyskytovat	k5eAaImIp3nP
u	u	k7c2
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
totiž	totiž	k9
bývají	bývat	k5eAaImIp3nP
vzácné	vzácný	k2eAgInPc1d1
u	u	k7c2
dospělých	dospělý	k2eAgFnPc2d1
(	(	kIx(
<g/>
například	například	k6eAd1
rotavirus	rotavirus	k1gInSc1
či	či	k8xC
zánět	zánět	k1gInSc1
průdušinek	průdušinka	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
naopak	naopak	k6eAd1
některé	některý	k3yIgFnPc1
časté	častý	k2eAgFnPc1d1
choroby	choroba	k1gFnPc1
dospělých	dospělí	k1gMnPc2
se	se	k3xPyFc4
u	u	k7c2
dětí	dítě	k1gFnPc2
vyskytují	vyskytovat	k5eAaImIp3nP
jen	jen	k9
zřídka	zřídka	k6eAd1
(	(	kIx(
<g/>
například	například	k6eAd1
choroby	choroba	k1gFnPc4
srdeční	srdeční	k2eAgFnSc2d1
tepny	tepna	k1gFnSc2
či	či	k8xC
žilní	žilní	k2eAgFnSc1d1
trombóza	trombóza	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pediatr	pediatr	k1gMnSc1
kardiolog	kardiolog	k1gMnSc1
se	se	k3xPyFc4
tedy	tedy	k9
zabývá	zabývat	k5eAaImIp3nS
stavem	stav	k1gInSc7
srdce	srdce	k1gNnSc2
u	u	k7c2
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
vrozenými	vrozený	k2eAgFnPc7d1
srdečními	srdeční	k2eAgFnPc7d1
vadami	vada	k1gFnPc7
<g/>
,	,	kIx,
pediatr	pediatr	k1gMnSc1
onkolog	onkolog	k1gMnSc1
většinou	většinou	k6eAd1
léčí	léčit	k5eAaImIp3nS
ty	ten	k3xDgInPc4
typy	typ	k1gInPc4
rakovin	rakovina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
spíše	spíše	k9
objevují	objevovat	k5eAaImIp3nP
u	u	k7c2
dětí	dítě	k1gFnPc2
(	(	kIx(
<g/>
jisté	jistý	k2eAgInPc1d1
druhy	druh	k1gInPc1
leukémie	leukémie	k1gFnSc2
<g/>
,	,	kIx,
lymfomů	lymfom	k1gInPc2
nebo	nebo	k8xC
zhoubných	zhoubný	k2eAgInPc2d1
nádorů	nádor	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pediatrie	pediatrie	k1gFnSc1
se	se	k3xPyFc4
specializuje	specializovat	k5eAaBmIp3nS
na	na	k7c4
stejné	stejný	k2eAgInPc4d1
obory	obor	k1gInPc4
jako	jako	k8xC,k8xS
lékařství	lékařství	k1gNnSc4
pro	pro	k7c4
dospělé	dospělí	k1gMnPc4
(	(	kIx(
<g/>
samozřejmě	samozřejmě	k6eAd1
až	až	k9
na	na	k7c6
geriatrii	geriatrie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Stále	stále	k6eAd1
více	hodně	k6eAd2
se	se	k3xPyFc4
rozvíjející	rozvíjející	k2eAgFnSc7d1
specializací	specializace	k1gFnSc7
je	být	k5eAaImIp3nS
lékařství	lékařství	k1gNnSc1
pro	pro	k7c4
dorost	dorost	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typy	typa	k1gFnSc2
nemocí	nemoc	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
u	u	k7c2
mladistvých	mladistvý	k1gMnPc2
objevují	objevovat	k5eAaImIp3nP
připomínají	připomínat	k5eAaImIp3nP
nemoci	nemoc	k1gFnPc4
dospělých	dospělí	k1gMnPc2
a	a	k8xC
specializovaní	specializovaný	k2eAgMnPc1d1
lékaři	lékař	k1gMnPc1
pro	pro	k7c4
dorost	dorost	k1gInSc4
se	se	k3xPyFc4
také	také	k9
často	často	k6eAd1
profilují	profilovat	k5eAaImIp3nP
z	z	k7c2
oblasti	oblast	k1gFnSc2
vnitřního	vnitřní	k2eAgMnSc2d1
nebo	nebo	k8xC
rodinného	rodinný	k2eAgNnSc2d1
lékařství	lékařství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Zcela	zcela	k6eAd1
jedinečným	jedinečný	k2eAgInSc7d1
oborem	obor	k1gInSc7
je	být	k5eAaImIp3nS
neonatologie	neonatologie	k1gFnSc1
<g/>
,	,	kIx,
péče	péče	k1gFnSc1
o	o	k7c4
novorozence	novorozenec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1
role	role	k1gFnSc1
pediatrů	pediatr	k1gMnPc2
</s>
<s>
Díky	díky	k7c3
značnému	značný	k2eAgNnSc3d1
vzdělání	vzdělání	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
pro	pro	k7c4
vykonávání	vykonávání	k1gNnSc4
praxe	praxe	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
povolání	povolání	k1gNnSc1
lékaře	lékař	k1gMnSc2
vyžaduje	vyžadovat	k5eAaImIp3nS
jistou	jistý	k2eAgFnSc4d1
morální	morální	k2eAgFnSc4d1
a	a	k8xC
právní	právní	k2eAgFnSc4d1
zodpovědnost	zodpovědnost	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
na	na	k7c4
pediatry	pediatr	k1gMnPc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
na	na	k7c4
ostatní	ostatní	k2eAgMnPc4d1
lékaře	lékař	k1gMnPc4
nahlíženo	nahlížen	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
na	na	k7c4
učenou	učený	k2eAgFnSc4d1
vrstvu	vrstva	k1gFnSc4
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pediatři	pediatr	k1gMnPc1
mívají	mívat	k5eAaImIp3nP
vysoké	vysoký	k2eAgNnSc4d1
sociální	sociální	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
a	a	k8xC
často	často	k6eAd1
se	se	k3xPyFc4
čeká	čekat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
vysoký	vysoký	k2eAgInSc4d1
a	a	k8xC
stabilní	stabilní	k2eAgInSc4d1
příjem	příjem	k1gInSc4
a	a	k8xC
další	další	k2eAgFnPc4d1
pracovní	pracovní	k2eAgFnPc4d1
jistoty	jistota	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praktičtí	praktický	k2eAgMnPc1d1
lékaři	lékař	k1gMnPc1
ale	ale	k8xC
obecně	obecně	k6eAd1
mají	mít	k5eAaImIp3nP
dlouhé	dlouhý	k2eAgFnPc4d1
a	a	k8xC
nepravidelné	pravidelný	k2eNgFnPc4d1
směny	směna	k1gFnPc4
(	(	kIx(
<g/>
často	často	k6eAd1
v	v	k7c6
nočních	noční	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
ale	ale	k9
mohou	moct	k5eAaImIp3nP
vydělávat	vydělávat	k5eAaImF
méně	málo	k6eAd2
než	než	k8xS
lidé	člověk	k1gMnPc1
se	s	k7c7
srovnatelným	srovnatelný	k2eAgNnSc7d1
vzděláním	vzdělání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neonatologové	Neonatolog	k1gMnPc1
a	a	k8xC
všeobecní	všeobecný	k2eAgMnPc1d1
dětští	dětský	k2eAgMnPc1d1
lékaři	lékař	k1gMnPc1
zaměstnaní	zaměstnaný	k2eAgMnPc1d1
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
často	často	k6eAd1
neustále	neustále	k6eAd1
na	na	k7c6
příjmu	příjem	k1gInSc6
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k9
kvůli	kvůli	k7c3
možným	možný	k2eAgFnPc3d1
perinatálním	perinatální	k2eAgFnPc3d1
komplikacím	komplikace	k1gFnPc3
(	(	kIx(
<g/>
hlavně	hlavně	k9
u	u	k7c2
rizikových	rizikový	k2eAgFnPc2d1
těhotenstvích	těhotenství	k1gNnPc6
<g/>
)	)	kIx)
nebo	nebo	k8xC
kvůli	kvůli	k7c3
péči	péče	k1gFnSc3
o	o	k7c4
nemocné	nemocný	k2eAgMnPc4d1,k2eNgMnPc4d1
novorozence	novorozenec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Pediatrie	pediatrie	k1gFnSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Pediatrie	pediatrie	k1gFnSc1
má	mít	k5eAaImIp3nS
podle	podle	k7c2
současné	současný	k2eAgFnSc2d1
koncepce	koncepce	k1gFnSc2
dva	dva	k4xCgInPc1
hlavní	hlavní	k2eAgInPc1d1
atestační	atestační	k2eAgInPc1d1
obory	obor	k1gInPc1
<g/>
:	:	kIx,
dětské	dětský	k2eAgNnSc1d1
lékařství	lékařství	k1gNnSc1
a	a	k8xC
praktické	praktický	k2eAgNnSc1d1
lékařství	lékařství	k1gNnSc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
a	a	k8xC
dorost	dorost	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Dětské	dětský	k2eAgNnSc4d1
lékařství	lékařství	k1gNnSc4
</s>
<s>
Vyšetření	vyšetření	k1gNnSc1
dítěte	dítě	k1gNnSc2
</s>
<s>
Atestovaný	atestovaný	k2eAgMnSc1d1
dětský	dětský	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgMnS
mít	mít	k5eAaImF
teoretické	teoretický	k2eAgFnPc4d1
znalosti	znalost	k1gFnPc4
a	a	k8xC
praktické	praktický	k2eAgFnPc4d1
dovednosti	dovednost	k1gFnPc4
z	z	k7c2
dětského	dětský	k2eAgNnSc2d1
lékařství	lékařství	k1gNnSc2
nezbytné	zbytný	k2eNgFnSc2d1,k2eAgFnSc2d1
k	k	k7c3
samostatné	samostatný	k2eAgFnSc3d1
práci	práce	k1gFnSc3
na	na	k7c6
dětských	dětský	k2eAgNnPc6d1
odděleních	oddělení	k1gNnPc6
<g/>
/	/	kIx~
<g/>
klinikách	klinika	k1gFnPc6
poskytujících	poskytující	k2eAgFnPc6d1
péči	péče	k1gFnSc4
dětem	dítě	k1gFnPc3
a	a	k8xC
dorostu	dorůst	k5eAaPmIp1nS
hospitalizovaným	hospitalizovaný	k2eAgFnPc3d1
nebo	nebo	k8xC
léčeným	léčený	k2eAgFnPc3d1
na	na	k7c6
ambulancích	ambulance	k1gFnPc6
nemocnic	nemocnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Předatestační	Předatestační	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
trvá	trvat	k5eAaImIp3nS
minimálně	minimálně	k6eAd1
5	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
během	během	k7c2
kterých	který	k3yQgFnPc2,k3yRgFnPc2,k3yIgFnPc2
lékař	lékař	k1gMnSc1
kromě	kromě	k7c2
dětských	dětský	k2eAgNnPc2d1
oddělení	oddělení	k1gNnPc2
absolvuje	absolvovat	k5eAaPmIp3nS
praxi	praxe	k1gFnSc3
i	i	k8xC
na	na	k7c6
ORL	ORL	kA
<g/>
,	,	kIx,
chirurgii	chirurgie	k1gFnSc6
<g/>
,	,	kIx,
ortopedii	ortopedie	k1gFnSc6
<g/>
,	,	kIx,
traumatologii	traumatologie	k1gFnSc6
<g/>
,	,	kIx,
dermatologii	dermatologie	k1gFnSc6
<g/>
,	,	kIx,
ARO	ara	k1gMnSc5
<g/>
,	,	kIx,
infektologii	infektologie	k1gFnSc6
a	a	k8xC
psychiatrii	psychiatrie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Absolvování	absolvování	k1gNnSc1
specializovaného	specializovaný	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
v	v	k7c6
oboru	obor	k1gInSc6
dětské	dětský	k2eAgNnSc1d1
lékařství	lékařství	k1gNnSc1
je	být	k5eAaImIp3nS
předpokladem	předpoklad	k1gInSc7
pro	pro	k7c4
vstup	vstup	k1gInSc4
do	do	k7c2
specializovaného	specializovaný	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
v	v	k7c6
oboru	obor	k1gInSc6
neonatologie	neonatologie	k1gFnSc2
<g/>
,	,	kIx,
dorostové	dorostový	k2eAgNnSc1d1
lékařství	lékařství	k1gNnSc1
<g/>
,	,	kIx,
dětská	dětský	k2eAgFnSc1d1
gastroenterologie	gastroenterologie	k1gFnSc1
a	a	k8xC
hepatologie	hepatologie	k1gFnSc1
<g/>
,	,	kIx,
dětská	dětský	k2eAgFnSc1d1
kardiologie	kardiologie	k1gFnSc1
<g/>
,	,	kIx,
dětská	dětský	k2eAgFnSc1d1
pneumologie	pneumologie	k1gFnSc1
<g/>
,	,	kIx,
dětská	dětský	k2eAgFnSc1d1
revmatologie	revmatologie	k1gFnSc1
<g/>
,	,	kIx,
dětská	dětský	k2eAgFnSc1d1
nefrologie	nefrologie	k1gFnSc1
<g/>
,	,	kIx,
dětská	dětský	k2eAgFnSc1d1
onkologie	onkologie	k1gFnSc1
a	a	k8xC
hematologie	hematologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Praktické	praktický	k2eAgNnSc1d1
lékařství	lékařství	k1gNnSc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
a	a	k8xC
dorost	dorost	k1gInSc4
</s>
<s>
Atestovaný	atestovaný	k2eAgMnSc1d1
praktický	praktický	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
a	a	k8xC
dorost	dorost	k1gInSc1
získává	získávat	k5eAaImIp3nS
způsobilost	způsobilost	k1gFnSc4
k	k	k7c3
samostatnému	samostatný	k2eAgNnSc3d1
vykonávání	vykonávání	k1gNnSc3
praxe	praxe	k1gFnSc2
praktického	praktický	k2eAgMnSc2d1
lékaře	lékař	k1gMnSc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
a	a	k8xC
dorost	dorost	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
práci	práce	k1gFnSc4
v	v	k7c6
primární	primární	k2eAgFnSc6d1
péči	péče	k1gFnSc6
<g/>
,	,	kIx,
tedy	tedy	k9
o	o	k7c6
zajištění	zajištění	k1gNnSc3
prvního	první	k4xOgInSc2
kontaktu	kontakt	k1gInSc2
pacienta	pacient	k1gMnSc2
se	s	k7c7
zdravotnickým	zdravotnický	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristickým	charakteristický	k2eAgInSc7d1
rysem	rys	k1gInSc7
primární	primární	k2eAgFnSc2d1
péče	péče	k1gFnSc2
v	v	k7c6
pediatrii	pediatrie	k1gFnSc6
je	být	k5eAaImIp3nS
velký	velký	k2eAgInSc1d1
důraz	důraz	k1gInSc1
kladený	kladený	k2eAgInSc1d1
na	na	k7c4
prevenci	prevence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Preventivní	preventivní	k2eAgInSc1d1
charakter	charakter	k1gInSc1
oboru	obor	k1gInSc2
podtrhuje	podtrhovat	k5eAaImIp3nS
klíčová	klíčový	k2eAgFnSc1d1
úloha	úloha	k1gFnSc1
pravidelných	pravidelný	k2eAgFnPc2d1
prohlídek	prohlídka	k1gFnPc2
a	a	k8xC
pravidelného	pravidelný	k2eAgNnSc2d1
očkování	očkování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praktický	praktický	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
umožňuje	umožňovat	k5eAaImIp3nS
včasný	včasný	k2eAgInSc4d1
záchyt	záchyt	k1gInSc4
některých	některý	k3yIgFnPc2
chorob	choroba	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zpočátku	zpočátku	k6eAd1
mohou	moct	k5eAaImIp3nP
projevit	projevit	k5eAaPmF
jen	jen	k9
zpomalením	zpomalení	k1gNnSc7
fyziologického	fyziologický	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
dítěte	dítě	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Předatestační	Předatestační	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
trvá	trvat	k5eAaImIp3nS
minimálně	minimálně	k6eAd1
5	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
během	během	k7c2
kterých	který	k3yRgFnPc2,k3yIgFnPc2,k3yQgFnPc2
lékař	lékař	k1gMnSc1
kromě	kromě	k7c2
dětských	dětský	k2eAgNnPc2d1
oddělení	oddělení	k1gNnPc2
absolvuje	absolvovat	k5eAaPmIp3nS
praxi	praxe	k1gFnSc3
i	i	k8xC
na	na	k7c6
ORL	ORL	kA
<g/>
,	,	kIx,
chirurgii	chirurgie	k1gFnSc6
<g/>
,	,	kIx,
ortopedii	ortopedie	k1gFnSc6
<g/>
,	,	kIx,
traumatologii	traumatologie	k1gFnSc6
<g/>
,	,	kIx,
dermatologii	dermatologie	k1gFnSc6
<g/>
,	,	kIx,
ARO	ara	k1gMnSc5
<g/>
,	,	kIx,
infektologii	infektologie	k1gFnSc6
a	a	k8xC
psychiatrii	psychiatrie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Primární	primární	k2eAgFnSc7d1
péčí	péče	k1gFnSc7
o	o	k7c4
děti	dítě	k1gFnPc4
a	a	k8xC
dorost	dorost	k1gInSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
zabývat	zabývat	k5eAaImF
i	i	k9
lékař	lékař	k1gMnSc1
s	s	k7c7
atestací	atestace	k1gFnSc7
z	z	k7c2
oboru	obor	k1gInSc2
rodinné	rodinný	k2eAgNnSc1d1
lékařství	lékařství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Pediatrics	Pediatricsa	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
LÉBL	LÉBL	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
PROVAZNÍK	Provazník	k1gMnSc1
<g/>
,	,	kIx,
Kamil	Kamil	k1gMnSc1
<g/>
;	;	kIx,
HEJCMANOVÁ	HEJCMANOVÁ	kA
<g/>
,	,	kIx,
Ludmila	Ludmila	k1gFnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Preklinická	Preklinický	k2eAgFnSc1d1
pediatrie	pediatrie	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Galén	Galén	k1gInSc1
<g/>
,	,	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7262	#num#	k4
<g/>
-	-	kIx~
<g/>
438	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HRODEK	HRODEK	kA
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
<g/>
;	;	kIx,
VAVŘINEC	Vavřinec	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pediatrie	pediatrie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Galén	Galén	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7262	#num#	k4
<g/>
-	-	kIx~
<g/>
178	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
pediatrie	pediatrie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Paediatrie	Paediatrie	k1gFnSc2
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
pediatrie	pediatrie	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Medicína	medicína	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4030594-6	4030594-6	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
13023	#num#	k4
</s>
