<s>
Profesor	profesor	k1gMnSc1	profesor
RNDr.	RNDr.	kA	RNDr.
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Veselovský	Veselovský	k1gMnSc1	Veselovský
<g/>
,	,	kIx,	,
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1928	[number]	k4	1928
-	-	kIx~	-
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznačnějších	význačný	k2eAgMnPc2d3	nejvýznačnější
českých	český	k2eAgMnPc2d1	český
zoologů	zoolog	k1gMnPc2	zoolog
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
vedl	vést	k5eAaImAgInS	vést
Zoologickou	zoologický	k2eAgFnSc4d1	zoologická
zahradu	zahrada	k1gFnSc4	zahrada
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
české	český	k2eAgFnSc2d1	Česká
etologie	etologie	k1gFnSc2	etologie
a	a	k8xC	a
autorem	autor	k1gMnSc7	autor
mnoha	mnoho	k4c2	mnoho
odborných	odborný	k2eAgInPc2d1	odborný
článků	článek	k1gInPc2	článek
a	a	k8xC	a
populárně	populárně	k6eAd1	populárně
naučných	naučný	k2eAgFnPc2d1	naučná
knih	kniha	k1gFnPc2	kniha
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
zoologie	zoologie	k1gFnSc2	zoologie
a	a	k8xC	a
etologie	etologie	k1gFnSc2	etologie
<g/>
.	.	kIx.	.
</s>
<s>
Narozen	narozen	k2eAgMnSc1d1	narozen
v	v	k7c6	v
Jaroměři	Jaroměř	k1gFnSc6	Jaroměř
<g/>
,	,	kIx,	,
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
Přírodovědeckou	přírodovědecký	k2eAgFnSc4d1	Přírodovědecká
fakultu	fakulta	k1gFnSc4	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
posléze	posléze	k6eAd1	posléze
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
jako	jako	k8xC	jako
pedagog	pedagog	k1gMnSc1	pedagog
(	(	kIx(	(
<g/>
profesor	profesor	k1gMnSc1	profesor
zoologie	zoologie	k1gFnSc2	zoologie
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
ředitelem	ředitel	k1gMnSc7	ředitel
pražské	pražský	k2eAgFnSc2d1	Pražská
zoo	zoo	k1gFnSc2	zoo
(	(	kIx(	(
<g/>
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
si	se	k3xPyFc3	se
podržel	podržet	k5eAaPmAgInS	podržet
téměř	téměř	k6eAd1	téměř
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
zoo	zoo	k1gFnSc6	zoo
jako	jako	k9	jako
zoolog	zoolog	k1gMnSc1	zoolog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c4	za
generálního	generální	k2eAgMnSc4d1	generální
sekretáře	sekretář	k1gMnSc4	sekretář
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
unie	unie	k1gFnSc2	unie
ředitelů	ředitel	k1gMnPc2	ředitel
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
pak	pak	k6eAd1	pak
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
této	tento	k3xDgFnSc2	tento
prestižní	prestižní	k2eAgFnSc2d1	prestižní
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
největších	veliký	k2eAgInPc2d3	veliký
úspěchů	úspěch	k1gInPc2	úspěch
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
ředitele	ředitel	k1gMnSc2	ředitel
pražské	pražský	k2eAgFnSc2d1	Pražská
zoo	zoo	k1gFnSc2	zoo
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
záchrana	záchrana	k1gFnSc1	záchrana
koně	kůň	k1gMnSc2	kůň
Převalského	převalský	k2eAgMnSc2d1	převalský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
konflikt	konflikt	k1gInSc4	konflikt
s	s	k7c7	s
komunistickými	komunistický	k2eAgMnPc7d1	komunistický
představiteli	představitel	k1gMnPc7	představitel
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c4	na
hodinu	hodina	k1gFnSc4	hodina
vyhozen	vyhozen	k2eAgMnSc1d1	vyhozen
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
protesty	protest	k1gInPc4	protest
a	a	k8xC	a
spontánní	spontánní	k2eAgInSc4d1	spontánní
bojkot	bojkot	k1gInSc4	bojkot
pražské	pražský	k2eAgFnSc2d1	Pražská
zoo	zoo	k1gFnSc2	zoo
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
mnoha	mnoho	k4c2	mnoho
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
brzy	brzy	k6eAd1	brzy
ukončila	ukončit	k5eAaPmAgFnS	ukončit
Sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
Fyziologický	fyziologický	k2eAgInSc4d1	fyziologický
ústav	ústav	k1gInSc4	ústav
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
přednášel	přednášet	k5eAaImAgMnS	přednášet
kurzy	kurz	k1gInPc4	kurz
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
zoologie	zoologie	k1gFnSc2	zoologie
a	a	k8xC	a
etologie	etologie	k1gFnSc2	etologie
na	na	k7c6	na
Biologické	biologický	k2eAgFnSc6d1	biologická
fakultě	fakulta	k1gFnSc6	fakulta
Jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
Univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
Přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
žákem	žák	k1gMnSc7	žák
nositele	nositel	k1gMnSc2	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
a	a	k8xC	a
zakladatele	zakladatel	k1gMnSc2	zakladatel
etologie	etologie	k1gFnSc2	etologie
Konráda	Konrád	k1gMnSc2	Konrád
Lorenze	Lorenz	k1gMnSc2	Lorenz
<g/>
.	.	kIx.	.
</s>
<s>
Prof.	prof.	kA	prof.
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Veselovský	Veselovský	k1gMnSc1	Veselovský
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
motolské	motolský	k2eAgFnSc6d1	motolská
nemocnici	nemocnice	k1gFnSc6	nemocnice
po	po	k7c6	po
nedlouhé	dlouhý	k2eNgFnSc6d1	nedlouhá
hospitalizaci	hospitalizace	k1gFnSc6	hospitalizace
na	na	k7c4	na
srdeční	srdeční	k2eAgFnPc4d1	srdeční
selhání	selhání	k1gNnPc4	selhání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
získal	získat	k5eAaPmAgMnS	získat
Cenu	cena	k1gFnSc4	cena
ministra	ministr	k1gMnSc2	ministr
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
in	in	k?	in
memoriam	memoriam	k1gInSc1	memoriam
"	"	kIx"	"
<g/>
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
zoologie	zoologie	k1gFnSc2	zoologie
a	a	k8xC	a
etologie	etologie	k1gFnSc2	etologie
a	a	k8xC	a
za	za	k7c4	za
obdivuhodnou	obdivuhodný	k2eAgFnSc4d1	obdivuhodná
aktivitu	aktivita	k1gFnSc4	aktivita
v	v	k7c6	v
činnostech	činnost	k1gFnPc6	činnost
podílejících	podílející	k2eAgMnPc2d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
záchraně	záchrana	k1gFnSc6	záchrana
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
propagaci	propagace	k1gFnSc4	propagace
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
jako	jako	k8xS	jako
vzdělávacích	vzdělávací	k2eAgFnPc2d1	vzdělávací
institucí	instituce	k1gFnPc2	instituce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Myslivec	Myslivec	k1gMnSc1	Myslivec
hospodaří	hospodařit	k5eAaImIp3nS	hospodařit
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
SZN	SZN	kA	SZN
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
Světem	svět	k1gInSc7	svět
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
Savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
SNDK	SNDK	kA	SNDK
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Hanzákem	Hanzák	k1gMnSc7	Hanzák
Praobyčejná	praobyčejný	k2eAgNnPc1d1	praobyčejný
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
Výlet	výlet	k1gInSc1	výlet
do	do	k7c2	do
třetihor	třetihory	k1gFnPc2	třetihory
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
a	a	k8xC	a
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Umíte	umět	k5eAaImIp2nP	umět
se	se	k3xPyFc4	se
dívat	dívat	k5eAaImF	dívat
na	na	k7c4	na
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
Vždyť	vždyť	k8xC	vždyť
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
jen	jen	k9	jen
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
Hlasy	hlas	k1gInPc1	hlas
džungle	džungle	k1gFnSc2	džungle
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
Sloni	sloň	k1gFnSc2wR	sloň
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
příbuzní	příbuzný	k2eAgMnPc1d1	příbuzný
<g/>
,	,	kIx,	,
SZN	SZN	kA	SZN
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
Všední	všední	k2eAgInSc1d1	všední
den	den	k1gInSc1	den
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
ZOO	zoo	k1gFnSc6	zoo
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
Tučňáci	tučňák	k1gMnPc1	tučňák
<g/>
,	,	kIx,	,
SZN	SZN	kA	SZN
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
Ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
K	k	k7c3	k
pramenům	pramen	k1gInPc3	pramen
Orinoka	Orinoko	k1gNnSc2	Orinoko
<g/>
,	,	kIx,	,
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
Šimpanz	šimpanz	k1gMnSc1	šimpanz
<g/>
,	,	kIx,	,
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7038-081-0	[number]	k4	80-7038-081-0
Chováme	chovat	k5eAaImIp1nP	chovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7038-240-6	[number]	k4	80-7038-240-6
Rys	rys	k1gInSc1	rys
<g/>
,	,	kIx,	,
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7038-294-5	[number]	k4	80-7038-294-5
Dobytí	dobytí	k1gNnPc2	dobytí
Tróje	Trója	k1gFnSc2	Trója
aneb	aneb	k?	aneb
Nejen	nejen	k6eAd1	nejen
sloni	slon	k1gMnPc1	slon
mají	mít	k5eAaImIp3nP	mít
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
ISV	ISV	kA	ISV
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85866-10-2	[number]	k4	80-85866-10-2
Tygr	tygr	k1gMnSc1	tygr
<g/>
,	,	kIx,	,
Aventinum	Aventinum	k1gInSc1	Aventinum
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7151-018-1	[number]	k4	80-7151-018-1
Říše	říš	k1gFnSc2	říš
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7151-071-8	[number]	k4	80-7151-071-8
Vydra	vydra	k1gFnSc1	vydra
<g/>
,	,	kIx,	,
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
Třeboňská	třeboňský	k2eAgFnSc1d1	Třeboňská
nadace	nadace	k1gFnSc1	nadace
pro	pro	k7c4	pro
vydru	vydra	k1gFnSc4	vydra
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7151-063-7	[number]	k4	80-7151-063-7
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-200-0756-3	[number]	k4	80-200-0756-3
Obecná	obecný	k2eAgFnSc1d1	obecná
ornitologie	ornitologie	k1gFnSc1	ornitologie
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-200-0857-8	[number]	k4	80-200-0857-8
Etologie	etologie	k1gFnSc2	etologie
<g/>
.	.	kIx.	.
</s>
<s>
Biologie	biologie	k1gFnSc1	biologie
chování	chování	k1gNnSc2	chování
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-200-1331-8	[number]	k4	80-200-1331-8
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
Veselovský	Veselovský	k1gMnSc1	Veselovský
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Veselovský	Veselovský	k1gMnSc1	Veselovský
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Veselovský	Veselovský	k1gMnSc1	Veselovský
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
etologii	etologie	k1gFnSc4	etologie
Další	další	k2eAgFnPc4d1	další
informace	informace	k1gFnPc4	informace
včetně	včetně	k7c2	včetně
životopisných	životopisný	k2eAgNnPc2d1	životopisné
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
seznamů	seznam	k1gInPc2	seznam
publikací	publikace	k1gFnPc2	publikace
ve	v	k7c6	v
zvláštním	zvláštní	k2eAgNnSc6d1	zvláštní
čísle	číslo	k1gNnSc6	číslo
zpravodaje	zpravodaj	k1gInSc2	zpravodaj
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Etologické	etologický	k2eAgFnSc2d1	etologická
Společnosti	společnost	k1gFnSc2	společnost
</s>
