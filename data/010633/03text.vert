<p>
<s>
Pulovr	pulovr	k1gInSc1	pulovr
je	být	k5eAaImIp3nS	být
svetr	svetr	k1gInSc4	svetr
bez	bez	k7c2	bez
zapínání	zapínání	k1gNnSc2	zapínání
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
názvu	název	k1gInSc2	název
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
pull	pull	k1gInSc1	pull
over	over	k1gInSc1	over
=	=	kIx~	=
přetáhnout	přetáhnout	k5eAaPmF	přetáhnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pulovr	pulovr	k1gInSc1	pulovr
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
přes	přes	k7c4	přes
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
<g/>
Původ	původ	k1gInSc1	původ
pulovru	pulovr	k1gInSc2	pulovr
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
nosili	nosit	k5eAaImAgMnP	nosit
námořníci	námořník	k1gMnPc1	námořník
a	a	k8xC	a
rybáři	rybář	k1gMnPc1	rybář
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
společenskému	společenský	k2eAgNnSc3d1	společenské
oblečení	oblečení	k1gNnSc3	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
s	s	k7c7	s
košilemi	košile	k1gFnPc7	košile
a	a	k8xC	a
polokošilemi	polokošile	k1gFnPc7	polokošile
k	k	k7c3	k
oblekovým	oblekový	k2eAgFnPc3d1	obleková
kalhotám	kalhoty	k1gFnPc3	kalhoty
i	i	k8xC	i
k	k	k7c3	k
džínám	džíny	k1gFnPc3	džíny
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
méně	málo	k6eAd2	málo
formálnímu	formální	k2eAgInSc3d1	formální
rázu	ráz	k1gInSc3	ráz
se	se	k3xPyFc4	se
nehodí	hodit	k5eNaPmIp3nS	hodit
pod	pod	k7c4	pod
sako	sako	k1gNnSc4	sako
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
obleku	oblek	k1gInSc2	oblek
<g/>
.	.	kIx.	.
</s>
<s>
Pulovr	pulovr	k1gInSc1	pulovr
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
v	v	k7c6	v
klasických	klasický	k2eAgFnPc6d1	klasická
barvách	barva	k1gFnPc6	barva
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
tmavě	tmavě	k6eAd1	tmavě
modrá	modrý	k2eAgNnPc1d1	modré
<g/>
,	,	kIx,	,
šedá	šedý	k2eAgNnPc1d1	šedé
nebo	nebo	k8xC	nebo
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jej	on	k3xPp3gMnSc4	on
zakoupit	zakoupit	k5eAaPmF	zakoupit
i	i	k9	i
v	v	k7c6	v
pastelových	pastelový	k2eAgFnPc6d1	pastelová
barvách	barva	k1gFnPc6	barva
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
vzory	vzor	k1gInPc7	vzor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
