<s>
Forrest	Forrest	k1gMnSc1	Forrest
Gump	Gump	k1gMnSc1	Gump
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Winstona	Winston	k1gMnSc2	Winston
Grooma	groom	k1gMnSc2	groom
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
Forresta	Forrest	k1gMnSc2	Forrest
Gumpa	Gump	k1gMnSc2	Gump
<g/>
,	,	kIx,	,
muže	muž	k1gMnSc2	muž
s	s	k7c7	s
IQ	iq	kA	iq
75	[number]	k4	75
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
neobvyklého	obvyklý	k2eNgInSc2d1	neobvyklý
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gFnSc1	Forrest
se	se	k3xPyFc4	se
potkává	potkávat	k5eAaImIp3nS	potkávat
s	s	k7c7	s
významnými	významný	k2eAgFnPc7d1	významná
historickými	historický	k2eAgFnPc7d1	historická
osobnostmi	osobnost	k1gFnPc7	osobnost
a	a	k8xC	a
na	na	k7c6	na
vlastní	vlastní	k2eAgFnSc6d1	vlastní
kůži	kůže	k1gFnSc6	kůže
prožívá	prožívat	k5eAaImIp3nS	prožívat
významné	významný	k2eAgFnPc4d1	významná
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
si	se	k3xPyFc3	se
neuvědomuje	uvědomovat	k5eNaImIp3nS	uvědomovat
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
s	s	k7c7	s
humorem	humor	k1gInSc7	humor
<g/>
,	,	kIx,	,
nadsázkou	nadsázka	k1gFnSc7	nadsázka
a	a	k8xC	a
obrovským	obrovský	k2eAgInSc7d1	obrovský
nadhledem	nadhled	k1gInSc7	nadhled
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
velkých	velký	k2eAgFnPc6d1	velká
událostech	událost	k1gFnPc6	událost
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
vyprávěn	vyprávět	k5eAaImNgInS	vyprávět
v	v	k7c6	v
ich-formě	ichorma	k1gFnSc6	ich-forma
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
Šimon	Šimon	k1gMnSc1	Šimon
Pellar	Pellar	k1gMnSc1	Pellar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
knihy	kniha	k1gFnSc2	kniha
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
natočen	natočen	k2eAgInSc1d1	natočen
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
film	film	k1gInSc1	film
s	s	k7c7	s
Tomem	Tom	k1gMnSc7	Tom
Hanksem	Hanks	k1gMnSc7	Hanks
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Volným	volný	k2eAgNnSc7d1	volné
pokračováním	pokračování	k1gNnSc7	pokračování
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
Gump	Gump	k1gInSc1	Gump
&	&	k?	&
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Gump	Gump	k1gInSc1	Gump
&	&	k?	&
Co	co	k3yInSc4	co
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
IQ	iq	kA	iq
Forresta	Forresta	k1gFnSc1	Forresta
Gumpa	Gumpa	k1gFnSc1	Gumpa
je	být	k5eAaImIp3nS	být
75	[number]	k4	75
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
Forrest	Forrest	k1gFnSc1	Forrest
rád	rád	k6eAd1	rád
čte	číst	k5eAaImIp3nS	číst
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Idiota	idiot	k1gMnSc4	idiot
od	od	k7c2	od
Dostojevského	Dostojevský	k2eAgMnSc2d1	Dostojevský
<g/>
,	,	kIx,	,
Krále	Král	k1gMnSc2	Král
Leara	Lear	k1gMnSc2	Lear
od	od	k7c2	od
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejvíce	nejvíce	k6eAd1	nejvíce
si	se	k3xPyFc3	se
oblíbil	oblíbit	k5eAaPmAgInS	oblíbit
Lennyho	Lennyho	k?	Lennyho
ze	z	k7c2	z
Steinbeckovy	Steinbeckův	k2eAgFnSc2d1	Steinbeckova
novely	novela	k1gFnSc2	novela
O	o	k7c6	o
myších	myš	k1gFnPc6	myš
a	a	k8xC	a
lidech	lid	k1gInPc6	lid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Forrestova	Forrestův	k2eAgFnSc1d1	Forrestova
matka	matka	k1gFnSc1	matka
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
po	po	k7c6	po
Nathanovi	Nathan	k1gMnSc6	Nathan
Bedfordovi	Bedforda	k1gMnSc6	Bedforda
Forrestovi	Forresta	k1gMnSc6	Forresta
(	(	kIx(	(
<g/>
jižanský	jižanský	k2eAgMnSc1d1	jižanský
generál	generál	k1gMnSc1	generál
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
Severu	sever	k1gInSc2	sever
proti	proti	k7c3	proti
jihu	jih	k1gInSc3	jih
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
členem	člen	k1gInSc7	člen
Ku	k	k7c3	k
Klux	Klux	k1gInSc4	Klux
Klanu	klan	k1gInSc2	klan
<g/>
,	,	kIx,	,
Forrestův	Forrestův	k2eAgMnSc1d1	Forrestův
otec	otec	k1gMnSc1	otec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
hned	hned	k6eAd1	hned
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
Forrest	Forrest	k1gMnSc1	Forrest
narodil	narodit	k5eAaPmAgMnS	narodit
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
v	v	k7c4	v
práci	práce	k1gFnSc4	práce
zasypán	zasypán	k2eAgInSc1d1	zasypán
půl	půl	k6eAd1	půl
tunou	tuna	k1gFnSc7	tuna
banánů	banán	k1gInPc2	banán
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
ho	on	k3xPp3gInSc4	on
slisovaly	slisovat	k5eAaPmAgFnP	slisovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
Forrest	Forrest	k1gMnSc1	Forrest
neměl	mít	k5eNaImAgMnS	mít
kamarády	kamarád	k1gMnPc4	kamarád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k9	už
v	v	k7c6	v
první	první	k4xOgFnSc6	první
třídě	třída	k1gFnSc6	třída
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Jenny	Jenn	k1gMnPc7	Jenn
Curranovou	Curranová	k1gFnSc7	Curranová
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
byl	být	k5eAaImAgInS	být
Forrest	Forrest	k1gInSc1	Forrest
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
pět	pět	k4xCc1	pět
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třinácti	třináct	k4xCc3	třináct
Forrest	Forrest	k1gInSc4	Forrest
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rostl	růst	k5eAaImAgInS	růst
<g/>
,	,	kIx,	,
v	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
měřil	měřit	k5eAaImAgInS	měřit
téměř	téměř	k6eAd1	téměř
dva	dva	k4xCgInPc4	dva
metry	metr	k1gInPc4	metr
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
mohutný	mohutný	k2eAgInSc1d1	mohutný
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
z	z	k7c2	z
Forresta	Forrest	k1gMnSc2	Forrest
stal	stát	k5eAaPmAgMnS	stát
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
dopomohl	dopomoct	k5eAaPmAgMnS	dopomoct
mu	on	k3xPp3gMnSc3	on
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
náhodný	náhodný	k2eAgMnSc1d1	náhodný
kolemjdoucí	kolemjdoucí	k1gMnSc1	kolemjdoucí
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
naverboval	naverbovat	k5eAaPmAgMnS	naverbovat
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
vybrán	vybrán	k2eAgInSc1d1	vybrán
do	do	k7c2	do
celostátního	celostátní	k2eAgInSc2d1	celostátní
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
výběru	výběr	k1gInSc2	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nešťastné	šťastný	k2eNgFnSc6d1	nešťastná
náhodě	náhoda	k1gFnSc6	náhoda
s	s	k7c7	s
Jenny	Jenn	k1gMnPc7	Jenn
byl	být	k5eAaImAgMnS	být
nespravedlivě	spravedlivě	k6eNd1	spravedlivě
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
znásilnění	znásilnění	k1gNnSc4	znásilnění
<g/>
,	,	kIx,	,
soudce	soudce	k1gMnSc1	soudce
chtěl	chtít	k5eAaImAgMnS	chtít
Forresta	Forrest	k1gMnSc4	Forrest
poslat	poslat	k5eAaPmF	poslat
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
mu	on	k3xPp3gMnSc3	on
povolil	povolit	k5eAaPmAgInS	povolit
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
přijala	přijmout	k5eAaPmAgFnS	přijmout
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
získá	získat	k5eAaPmIp3nS	získat
výborného	výborný	k2eAgMnSc4d1	výborný
fotbalistu	fotbalista	k1gMnSc4	fotbalista
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Curtisem	Curtis	k1gInSc7	Curtis
a	a	k8xC	a
Bubbou	Bubba	k1gFnSc7	Bubba
<g/>
.	.	kIx.	.
</s>
<s>
Bubba	Bubba	k1gMnSc1	Bubba
naučil	naučit	k5eAaPmAgMnS	naučit
hrát	hrát	k5eAaImF	hrát
Forresta	Forresta	k1gMnSc1	Forresta
na	na	k7c4	na
foukací	foukací	k2eAgFnSc4d1	foukací
harmoniku	harmonika	k1gFnSc4	harmonika
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
výborně	výborně	k6eAd1	výborně
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
také	také	k9	také
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Jenny	Jenno	k1gNnPc7	Jenno
<g/>
,	,	kIx,	,
studuje	studovat	k5eAaImIp3nS	studovat
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
drama	drama	k1gNnSc4	drama
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
ve	v	k7c6	v
folkové	folkový	k2eAgFnSc6d1	folková
kapele	kapela	k1gFnSc6	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Forrest	Forrest	k1gMnSc1	Forrest
musel	muset	k5eAaImAgMnS	muset
opustit	opustit	k5eAaPmF	opustit
nedokončené	dokončený	k2eNgNnSc4d1	nedokončené
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
řada	řada	k1gFnSc1	řada
předmětů	předmět	k1gInPc2	předmět
dělala	dělat	k5eAaImAgFnS	dělat
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nebyl	být	k5eNaImAgInS	být
s	s	k7c7	s
to	ten	k3xDgNnSc4	ten
překonat	překonat	k5eAaPmF	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dorazil	dorazit	k5eAaPmAgMnS	dorazit
domu	dům	k1gInSc3	dům
do	do	k7c2	do
Mobile	mobile	k1gNnSc2	mobile
<g/>
,	,	kIx,	,
tak	tak	k9	tak
už	už	k9	už
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
čekal	čekat	k5eAaImAgInS	čekat
dopis	dopis	k1gInSc1	dopis
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
k	k	k7c3	k
odvodu	odvod	k1gInSc3	odvod
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gFnSc1	Forrest
v	v	k7c6	v
Armádě	armáda	k1gFnSc6	armáda
prožíval	prožívat	k5eAaImAgInS	prožívat
mnohá	mnohý	k2eAgNnPc4d1	mnohé
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
vařil	vařit	k5eAaImAgMnS	vařit
guláš	guláš	k1gInSc4	guláš
v	v	k7c6	v
boileru	boiler	k1gInSc6	boiler
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zanedlouho	zanedlouho	k6eAd1	zanedlouho
vybouchl	vybouchnout	k5eAaPmAgInS	vybouchnout
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gFnSc1	Forrest
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
shledal	shledat	k5eAaPmAgMnS	shledat
s	s	k7c7	s
Bubbou	Bubba	k1gFnSc7	Bubba
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
sportovní	sportovní	k2eAgNnSc4d1	sportovní
stipendium	stipendium	k1gNnSc4	stipendium
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
odletěli	odletět	k5eAaPmAgMnP	odletět
posléze	posléze	k6eAd1	posléze
bojovat	bojovat	k5eAaImF	bojovat
do	do	k7c2	do
Vietnamu	Vietnam	k1gInSc2	Vietnam
(	(	kIx(	(
<g/>
Qui	Qui	k1gMnSc1	Qui
Nhon	Nhon	k1gMnSc1	Nhon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velel	velet	k5eAaImAgMnS	velet
seržant	seržant	k1gMnSc1	seržant
Krantz	Krantz	k1gMnSc1	Krantz
<g/>
.	.	kIx.	.
</s>
<s>
Jenny	Jenen	k2eAgInPc1d1	Jenen
mezitím	mezitím	k6eAd1	mezitím
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
objíždí	objíždět	k5eAaImIp3nS	objíždět
demonstrace	demonstrace	k1gFnPc4	demonstrace
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
hippiesačkou	hippiesačka	k1gFnSc7	hippiesačka
<g/>
,	,	kIx,	,
Jenny	Jenen	k2eAgInPc4d1	Jenen
si	se	k3xPyFc3	se
s	s	k7c7	s
Forrestem	Forrest	k1gInSc7	Forrest
za	za	k7c2	za
války	válka	k1gFnSc2	válka
dopisovala	dopisovat	k5eAaImAgFnS	dopisovat
<g/>
.	.	kIx.	.
</s>
<s>
Forresta	Forresta	k1gFnSc1	Forresta
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
postřelili	postřelit	k5eAaPmAgMnP	postřelit
do	do	k7c2	do
zadku	zadek	k1gInSc2	zadek
<g/>
,	,	kIx,	,
Bubba	Bubba	k1gMnSc1	Bubba
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gFnSc1	Forrest
kvůli	kvůli	k7c3	kvůli
zranění	zranění	k1gNnSc3	zranění
ležel	ležet	k5eAaImAgMnS	ležet
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
Danangu	Danang	k1gInSc6	Danang
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Danem	Dan	k1gMnSc7	Dan
(	(	kIx(	(
<g/>
učitel	učitel	k1gMnSc1	učitel
Dějepisu	dějepis	k1gInSc2	dějepis
z	z	k7c2	z
Connecticutu	Connecticut	k1gInSc2	Connecticut
<g/>
)	)	kIx)	)
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc4	kongres
USA	USA	kA	USA
udělil	udělit	k5eAaPmAgMnS	udělit
Forrestovi	Forresta	k1gMnSc3	Forresta
medaili	medaile	k1gFnSc4	medaile
za	za	k7c4	za
statečnost	statečnost	k1gFnSc4	statečnost
<g/>
.	.	kIx.	.
</s>
<s>
Forrestova	Forrestův	k2eAgFnSc1d1	Forrestova
máma	máma	k1gFnSc1	máma
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
chudobinci	chudobinec	k1gInSc6	chudobinec
Milosrdných	milosrdný	k2eAgFnPc2d1	milosrdná
sester	sestra	k1gFnPc2	sestra
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
její	její	k3xOp3gInSc1	její
dům	dům	k1gInSc1	dům
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Forresta	Forresta	k1gMnSc1	Forresta
začal	začít	k5eAaPmAgMnS	začít
bavit	bavit	k5eAaImF	bavit
ping-pong	pingong	k1gInSc4	ping-pong
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Lyndon	Lyndon	k1gMnSc1	Lyndon
Baines	Baines	k1gMnSc1	Baines
Johnson	Johnson	k1gMnSc1	Johnson
osobně	osobně	k6eAd1	osobně
vyznamenával	vyznamenávat	k5eAaImAgMnS	vyznamenávat
Forresta	Forresta	k1gMnSc1	Forresta
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
chtěl	chtít	k5eAaImAgMnS	chtít
ze	z	k7c2	z
zvědavosti	zvědavost	k1gFnSc2	zvědavost
vidět	vidět	k5eAaImF	vidět
Forrestovu	Forrestův	k2eAgFnSc4d1	Forrestova
ránu	rána	k1gFnSc4	rána
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
mu	on	k3xPp3gMnSc3	on
ji	on	k3xPp3gFnSc4	on
ukázal	ukázat	k5eAaPmAgMnS	ukázat
a	a	k8xC	a
vystrčil	vystrčit	k5eAaPmAgMnS	vystrčit
na	na	k7c4	na
Prezidenta	prezident	k1gMnSc4	prezident
zadek	zadek	k1gInSc4	zadek
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
pochopitelně	pochopitelně	k6eAd1	pochopitelně
poprask	poprask	k1gInSc1	poprask
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
podplukovníkem	podplukovník	k1gMnSc7	podplukovník
Goochem	Gooch	k1gMnSc7	Gooch
pak	pak	k6eAd1	pak
chvíli	chvíle	k1gFnSc4	chvíle
jezdil	jezdit	k5eAaImAgMnS	jezdit
po	po	k7c6	po
státech	stát	k1gInPc6	stát
a	a	k8xC	a
přesvědčovali	přesvědčovat	k5eAaImAgMnP	přesvědčovat
studenty	student	k1gMnPc7	student
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skončilo	skončit	k5eAaPmAgNnS	skončit
to	ten	k3xDgNnSc1	ten
fiaskem	fiasko	k1gNnSc7	fiasko
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gMnSc1	Forrest
poté	poté	k6eAd1	poté
hrál	hrát	k5eAaImAgMnS	hrát
ping-pong	pingong	k1gInSc4	ping-pong
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
za	za	k7c4	za
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
nešlo	jít	k5eNaImAgNnS	jít
jen	jen	k9	jen
o	o	k7c4	o
ping-pong	pingong	k1gInSc4	ping-pong
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
o	o	k7c4	o
diplomatickou	diplomatický	k2eAgFnSc4d1	diplomatická
misi	mise	k1gFnSc4	mise
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
Forresta	Forrest	k1gMnSc4	Forrest
zatkli	zatknout	k5eAaPmAgMnP	zatknout
<g/>
,	,	kIx,	,
považovali	považovat	k5eAaImAgMnP	považovat
jej	on	k3xPp3gMnSc4	on
za	za	k7c4	za
špiona	špion	k1gMnSc4	špion
<g/>
,	,	kIx,	,
když	když	k8xS	když
zabloudil	zabloudit	k5eAaPmAgMnS	zabloudit
a	a	k8xC	a
hledal	hledat	k5eAaImAgMnS	hledat
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
hotelu	hotel	k1gInSc2	hotel
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ho	on	k3xPp3gMnSc4	on
propustili	propustit	k5eAaPmAgMnP	propustit
a	a	k8xC	a
Forrest	Forrest	k1gInSc1	Forrest
poté	poté	k6eAd1	poté
zachránil	zachránit	k5eAaPmAgInS	zachránit
život	život	k1gInSc1	život
Mao	Mao	k1gMnSc3	Mao
Ce-tungovi	Ceung	k1gMnSc3	Ce-tung
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
se	se	k3xPyFc4	se
topil	topit	k5eAaImAgMnS	topit
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
přeplavat	přeplavat	k5eAaPmF	přeplavat
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
zanedlouho	zanedlouho	k6eAd1	zanedlouho
Forresta	Forresta	k1gFnSc1	Forresta
předčasně	předčasně	k6eAd1	předčasně
propustila	propustit	k5eAaPmAgFnS	propustit
<g/>
,	,	kIx,	,
netušil	tušit	k5eNaImAgMnS	tušit
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
myslel	myslet	k5eAaImAgMnS	myslet
na	na	k7c4	na
Jenny	Jenna	k1gFnPc4	Jenna
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
odjel	odjet	k5eAaPmAgInS	odjet
autobusem	autobus	k1gInSc7	autobus
do	do	k7c2	do
Bostonu	Boston	k1gInSc2	Boston
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
Forrest	Forrest	k1gFnSc4	Forrest
v	v	k7c4	v
Hodaddy	Hodadda	k1gFnPc4	Hodadda
clubu	club	k1gInSc2	club
našel	najít	k5eAaPmAgMnS	najít
hudební	hudební	k2eAgFnSc4d1	hudební
kapelu	kapela	k1gFnSc4	kapela
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
Jenny	Jenna	k1gFnPc4	Jenna
zpívala	zpívat	k5eAaImAgFnS	zpívat
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gFnSc1	Forrest
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
Jenny	Jenn	k1gInPc7	Jenn
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
žila	žít	k5eAaImAgFnS	žít
s	s	k7c7	s
Hippies	Hippies	k1gInSc1	Hippies
<g/>
,	,	kIx,	,
chodila	chodit	k5eAaImAgFnS	chodit
se	s	k7c7	s
spoustou	spousta	k1gFnSc7	spousta
kluků	kluk	k1gMnPc2	kluk
<g/>
,	,	kIx,	,
účastnila	účastnit	k5eAaImAgFnS	účastnit
se	se	k3xPyFc4	se
většiny	většina	k1gFnSc2	většina
protiválečných	protiválečný	k2eAgFnPc2d1	protiválečná
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
,	,	kIx,	,
momentálně	momentálně	k6eAd1	momentálně
zpívá	zpívat	k5eAaImIp3nS	zpívat
s	s	k7c7	s
"	"	kIx"	"
<g/>
Nakřáplými	nakřáplý	k2eAgFnPc7d1	nakřáplá
šiškami	šiška	k1gFnPc7	šiška
<g/>
"	"	kIx"	"
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
se	s	k7c7	s
studentem	student	k1gMnSc7	student
filozofie	filozofie	k1gFnSc2	filozofie
Rudolphem	Rudolph	k1gInSc7	Rudolph
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k5eAaPmF	Forrest
se	se	k3xPyFc4	se
k	k	k7c3	k
ním	on	k3xPp3gNnSc7	on
nastěhuje	nastěhovat	k5eAaPmIp3nS	nastěhovat
a	a	k8xC	a
jako	jako	k8xC	jako
externista	externista	k1gMnSc1	externista
začne	začít	k5eAaPmIp3nS	začít
na	na	k7c6	na
Harvardově	Harvardův	k2eAgFnSc6d1	Harvardova
univerzitě	univerzita	k1gFnSc6	univerzita
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
semináře	seminář	k1gInPc4	seminář
doktora	doktor	k1gMnSc2	doktor
Quackenbushe	Quackenbush	k1gMnSc2	Quackenbush
(	(	kIx(	(
<g/>
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
Jenny	Jenna	k1gFnPc4	Jenna
dříve	dříve	k6eAd2	dříve
chodila	chodit	k5eAaImAgFnS	chodit
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Role	role	k1gFnSc1	role
idiota	idiot	k1gMnSc2	idiot
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
literatuře	literatura	k1gFnSc6	literatura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gFnSc1	Forrest
také	také	k9	také
začne	začít	k5eAaPmIp3nS	začít
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
foukací	foukací	k2eAgFnSc4d1	foukací
harmoniku	harmonika	k1gFnSc4	harmonika
v	v	k7c6	v
Nakřáplejch	Nakřáplejch	k?	Nakřáplejch
šiškách	šiška	k1gFnPc6	šiška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semináři	seminář	k1gInSc6	seminář
studenti	student	k1gMnPc1	student
hrají	hrát	k5eAaImIp3nP	hrát
divadlo	divadlo	k1gNnSc4	divadlo
Krále	Král	k1gMnSc2	Král
Leara	Lear	k1gMnSc2	Lear
<g/>
,	,	kIx,	,
Forrest	Forrest	k1gInSc4	Forrest
v	v	k7c6	v
roli	role	k1gFnSc6	role
Vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Gloucesteru	Gloucester	k1gInSc2	Gloucester
nedopatřením	nedopatření	k1gNnSc7	nedopatření
způsobí	způsobit	k5eAaPmIp3nS	způsobit
požár	požár	k1gInSc1	požár
<g/>
.	.	kIx.	.
</s>
<s>
Jenny	Jenna	k1gFnPc1	Jenna
se	se	k3xPyFc4	se
rozchází	rozcházet	k5eAaImIp3nP	rozcházet
s	s	k7c7	s
Rudophem	Rudoph	k1gInSc7	Rudoph
a	a	k8xC	a
chodí	chodit	k5eAaImIp3nP	chodit
s	s	k7c7	s
Forrestem	Forrest	k1gInSc7	Forrest
<g/>
.	.	kIx.	.
</s>
<s>
Chvíli	chvíle	k1gFnSc4	chvíle
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
ovšem	ovšem	k9	ovšem
rozchází	rozcházet	k5eAaImIp3nS	rozcházet
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Jenny	Jenn	k1gMnPc4	Jenn
viděla	vidět	k5eAaImAgFnS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
na	na	k7c6	na
Forrestově	Forrestův	k2eAgInSc6d1	Forrestův
klíně	klín	k1gInSc6	klín
sedí	sedit	k5eAaImIp3nS	sedit
fanynky	fanynka	k1gFnPc4	fanynka
Nakřáplejch	Nakřáplejch	k?	Nakřáplejch
šišek	šiška	k1gFnPc2	šiška
<g/>
.	.	kIx.	.
</s>
<s>
Jenny	Jenna	k1gFnPc1	Jenna
si	se	k3xPyFc3	se
špatně	špatně	k6eAd1	špatně
vyložila	vyložit	k5eAaPmAgFnS	vyložit
Forrestovu	Forrestův	k2eAgFnSc4d1	Forrestova
dobromyslnost	dobromyslnost	k1gFnSc4	dobromyslnost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Forrest	Forrest	k1gFnSc4	Forrest
nezamýšlel	zamýšlet	k5eNaImAgMnS	zamýšlet
nic	nic	k3yNnSc1	nic
nevhodného	vhodný	k2eNgNnSc2d1	nevhodné
a	a	k8xC	a
rozchází	rozcházet	k5eAaImIp3nP	rozcházet
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
zanedlouho	zanedlouho	k6eAd1	zanedlouho
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
a	a	k8xC	a
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
Forresta	Forresta	k1gFnSc1	Forresta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
demonstraci	demonstrace	k1gFnSc4	demonstrace
zahodil	zahodit	k5eAaPmAgMnS	zahodit
svou	svůj	k3xOyFgFnSc4	svůj
medaili	medaile	k1gFnSc4	medaile
za	za	k7c4	za
statečnost	statečnost	k1gFnSc4	statečnost
jako	jako	k8xC	jako
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gFnSc1	Forrest
tak	tak	k9	tak
učiní	učinit	k5eAaImIp3nS	učinit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
tak	tak	k9	tak
nešťastně	šťastně	k6eNd1	šťastně
<g/>
,	,	kIx,	,
že	že	k8xS	že
medailí	medaile	k1gFnPc2	medaile
uhodí	uhodit	k5eAaPmIp3nS	uhodit
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
předsedu	předseda	k1gMnSc4	předseda
Senátu	senát	k1gInSc2	senát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Soudce	soudce	k1gMnSc1	soudce
ho	on	k3xPp3gNnSc4	on
posílá	posílat	k5eAaImIp3nS	posílat
do	do	k7c2	do
psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
léčebny	léčebna	k1gFnSc2	léčebna
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gInSc1	Forrest
tam	tam	k6eAd1	tam
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
doktorka	doktorka	k1gFnSc1	doktorka
Waltonová	Waltonový	k2eAgFnSc1d1	Waltonová
s	s	k7c7	s
doktorem	doktor	k1gMnSc7	doktor
Earlem	earl	k1gMnSc7	earl
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
mozek	mozek	k1gInSc1	mozek
pracuje	pracovat	k5eAaImIp3nS	pracovat
naprosto	naprosto	k6eAd1	naprosto
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
počítač	počítač	k1gInSc1	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Forresta	Forresta	k1gFnSc1	Forresta
tedy	tedy	k9	tedy
posílají	posílat	k5eAaImIp3nP	posílat
do	do	k7c2	do
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
prý	prý	k9	prý
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
hodit	hodit	k5eAaImF	hodit
v	v	k7c6	v
kosmickém	kosmický	k2eAgNnSc6d1	kosmické
středisku	středisko	k1gNnSc6	středisko
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
Forrest	Forrest	k1gFnSc1	Forrest
společně	společně	k6eAd1	společně
s	s	k7c7	s
Majorkou	majorka	k1gFnSc7	majorka
Janet	Janet	k1gInSc4	Janet
Fritchovou	Fritchová	k1gFnSc7	Fritchová
a	a	k8xC	a
opičákem	opičák	k1gMnSc7	opičák
Sue	Sue	k1gMnSc7	Sue
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
orangutanka	orangutanka	k1gFnSc1	orangutanka
Sue	Sue	k1gFnSc2	Sue
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
omylem	omylem	k6eAd1	omylem
zamění	zaměnit	k5eAaPmIp3nS	zaměnit
s	s	k7c7	s
netrénovaným	trénovaný	k2eNgMnSc7d1	netrénovaný
opičákem	opičák	k1gMnSc7	opičák
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jméno	jméno	k1gNnSc1	jméno
mu	on	k3xPp3gMnSc3	on
už	už	k9	už
zůstane	zůstat	k5eAaPmIp3nS	zůstat
<g/>
)	)	kIx)	)
odletí	odletět	k5eAaPmIp3nS	odletět
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Sue	Sue	k?	Sue
zničí	zničit	k5eAaPmIp3nS	zničit
ovládací	ovládací	k2eAgInSc1d1	ovládací
panel	panel	k1gInSc1	panel
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
Forrest	Forrest	k1gFnSc4	Forrest
s	s	k7c7	s
Janet	Janet	k1gInSc1	Janet
nuceni	nucen	k2eAgMnPc1d1	nucen
nouzově	nouzově	k6eAd1	nouzově
přistát	přistát	k5eAaPmF	přistát
a	a	k8xC	a
ztroskotají	ztroskotat	k5eAaPmIp3nP	ztroskotat
na	na	k7c4	na
Nové	Nové	k2eAgNnSc4d1	Nové
Guinei	Guinei	k1gNnSc4	Guinei
<g/>
,	,	kIx,	,
setkávají	setkávat	k5eAaImIp3nP	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
domorodci	domorodec	k1gMnPc7	domorodec
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Sam	Sam	k1gMnSc1	Sam
<g/>
,	,	kIx,	,
umí	umět	k5eAaImIp3nS	umět
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
<s>
Sam	Sam	k1gMnSc1	Sam
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Yale	Yale	k1gFnSc6	Yale
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
rozvědka	rozvědka	k1gFnSc1	rozvědka
ho	on	k3xPp3gInSc4	on
zaúkolovala	zaúkolovat	k5eAaPmAgFnS	zaúkolovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vedl	vést	k5eAaImAgMnS	vést
partyzány	partyzána	k1gFnSc2	partyzána
proti	proti	k7c3	proti
Japoncům	Japonec	k1gMnPc3	Japonec
<g/>
.	.	kIx.	.
</s>
<s>
Domorodci	domorodec	k1gMnPc1	domorodec
sice	sice	k8xC	sice
byli	být	k5eAaImAgMnP	být
lidojedi	lidojed	k1gMnPc1	lidojed
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
jim	on	k3xPp3gMnPc3	on
Forrest	Forrest	k1gMnSc1	Forrest
a	a	k8xC	a
Janet	Janeta	k1gFnPc2	Janeta
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
pěstovat	pěstovat	k5eAaImF	pěstovat
bavlnu	bavlna	k1gFnSc4	bavlna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
Samův	Samův	k2eAgInSc4d1	Samův
nápad	nápad	k1gInSc4	nápad
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
domorodci	domorodec	k1gMnPc7	domorodec
spřátelili	spřátelit	k5eAaPmAgMnP	spřátelit
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gMnSc1	Forrest
hrával	hrávat	k5eAaImAgMnS	hrávat
se	s	k7c7	s
Samem	Samos	k1gInSc7	Samos
šachy	šach	k1gInPc4	šach
a	a	k8xC	a
Janet	Janet	k1gInSc4	Janet
se	se	k3xPyFc4	se
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
do	do	k7c2	do
Grurcka	Grurcko	k1gNnSc2	Grurcko
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
milenci	milenec	k1gMnPc1	milenec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oslavě	oslava	k1gFnSc6	oslava
dožínek	dožínky	k1gFnPc2	dožínky
se	se	k3xPyFc4	se
domorodci	domorodec	k1gMnPc1	domorodec
Forresta	Forresta	k1gFnSc1	Forresta
a	a	k8xC	a
Janet	Janet	k1gInSc4	Janet
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
sníst	sníst	k5eAaPmF	sníst
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
vesnici	vesnice	k1gFnSc4	vesnice
napadají	napadat	k5eAaImIp3nP	napadat
Pygmejové	Pygmej	k1gMnPc1	Pygmej
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
nakonec	nakonec	k6eAd1	nakonec
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
vesnice	vesnice	k1gFnSc2	vesnice
Velkého	velký	k2eAgMnSc2d1	velký
Sama	Sam	k1gMnSc2	Sam
zabili	zabít	k5eAaPmAgMnP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Forresta	Forresta	k1gMnSc1	Forresta
<g/>
,	,	kIx,	,
Janet	Janet	k1gMnSc1	Janet
<g/>
,	,	kIx,	,
Sue	Sue	k1gMnSc1	Sue
a	a	k8xC	a
Grurcka	Grurcka	k1gFnSc1	Grurcka
zajali	zajmout	k5eAaPmAgMnP	zajmout
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
je	on	k3xPp3gInPc4	on
ale	ale	k9	ale
přiletěli	přiletět	k5eAaPmAgMnP	přiletět
zachránit	zachránit	k5eAaPmF	zachránit
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
NASA	NASA	kA	NASA
a	a	k8xC	a
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
čtyřech	čtyři	k4xCgInPc6	čtyři
letech	let	k1gInPc6	let
vzali	vzít	k5eAaPmAgMnP	vzít
Forresta	Forrest	k1gMnSc4	Forrest
a	a	k8xC	a
Sue	Sue	k1gMnSc4	Sue
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Janet	Janet	k1gInSc4	Janet
zůstala	zůstat	k5eAaPmAgFnS	zůstat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
s	s	k7c7	s
Grurckem	Grurck	k1gInSc7	Grurck
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
Forresta	Forrest	k1gMnSc2	Forrest
vítalo	vítat	k5eAaImAgNnS	vítat
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
prezidenta	prezident	k1gMnSc2	prezident
Nixona	Nixon	k1gMnSc2	Nixon
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gInSc1	Forrest
hledal	hledat	k5eAaImAgInS	hledat
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ona	onen	k3xDgFnSc1	onen
utekla	utéct	k5eAaPmAgFnS	utéct
z	z	k7c2	z
chudobince	chudobinec	k1gInSc2	chudobinec
s	s	k7c7	s
nějakým	nějaký	k3yIgMnSc7	nějaký
protestantem	protestant	k1gMnSc7	protestant
<g/>
,	,	kIx,	,
náhodou	náhoda	k1gFnSc7	náhoda
ale	ale	k8xC	ale
potkal	potkat	k5eAaPmAgInS	potkat
Dana	Dan	k1gMnSc4	Dan
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
bezdomovec	bezdomovec	k1gMnSc1	bezdomovec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Danem	Dan	k1gMnSc7	Dan
se	se	k3xPyFc4	se
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
začal	začít	k5eAaPmAgMnS	začít
pít	pít	k5eAaImF	pít
<g/>
,	,	kIx,	,
vyhodili	vyhodit	k5eAaPmAgMnP	vyhodit
ho	on	k3xPp3gMnSc4	on
z	z	k7c2	z
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
taky	taky	k6eAd1	taky
ho	on	k3xPp3gMnSc4	on
vykradli	vykrást	k5eAaPmAgMnP	vykrást
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gMnSc1	Forrest
chtěl	chtít	k5eAaImAgMnS	chtít
najít	najít	k5eAaPmF	najít
Jenny	Jenn	k1gInPc4	Jenn
<g/>
,	,	kIx,	,
zavolal	zavolat	k5eAaPmAgMnS	zavolat
bubeníkovi	bubeník	k1gMnSc3	bubeník
Nakřáplejch	Nakřáplejch	k?	Nakřáplejch
šišek	šiška	k1gFnPc2	šiška
Mosovi	Mosa	k1gMnSc6	Mosa
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
mu	on	k3xPp3gMnSc3	on
pověděl	povědět	k5eAaPmAgMnS	povědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jenny	Jenn	k1gMnPc4	Jenn
začala	začít	k5eAaPmAgFnS	začít
mít	mít	k5eAaImF	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
a	a	k8xC	a
kapelu	kapela	k1gFnSc4	kapela
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gMnSc1	Forrest
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
Temperer	Temperer	k1gInSc1	Temperer
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
pneumatik	pneumatika	k1gFnPc2	pneumatika
<g/>
)	)	kIx)	)
v	v	k7c6	v
Indianopolis	Indianopolis	k1gFnSc6	Indianopolis
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
i	i	k9	i
s	s	k7c7	s
Danem	Dan	k1gMnSc7	Dan
vydal	vydat	k5eAaPmAgMnS	vydat
a	a	k8xC	a
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Jenny	Jenn	k1gMnPc7	Jenn
<g/>
.	.	kIx.	.
</s>
<s>
Jenny	Jenna	k1gFnSc2	Jenna
bydlela	bydlet	k5eAaImAgFnS	bydlet
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
bytě	byt	k1gInSc6	byt
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
Forrest	Forrest	k1gMnSc1	Forrest
s	s	k7c7	s
Danem	Dan	k1gMnSc7	Dan
nastěhovali	nastěhovat	k5eAaPmAgMnP	nastěhovat
<g/>
,	,	kIx,	,
Jenny	Jenn	k1gMnPc4	Jenn
vyprávěla	vyprávět	k5eAaImAgFnS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
bydlela	bydlet	k5eAaImAgFnS	bydlet
ve	v	k7c6	v
squatu	squat	k1gInSc6	squat
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
na	na	k7c4	na
Forresta	Forrest	k1gMnSc4	Forrest
nezapomněla	zapomnět	k5eNaImAgFnS	zapomnět
<g/>
,	,	kIx,	,
chtěla	chtít	k5eAaImAgFnS	chtít
být	být	k5eAaImF	být
u	u	k7c2	u
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
když	když	k8xS	když
letěl	letět	k5eAaImAgMnS	letět
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
a	a	k8xC	a
taky	taky	k9	taky
když	když	k8xS	když
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Dan	Dan	k1gMnSc1	Dan
posléze	posléze	k6eAd1	posléze
Forresta	Forresta	k1gMnSc1	Forresta
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
stal	stát	k5eAaPmAgInS	stát
Wrestler	Wrestler	k1gInSc4	Wrestler
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gInSc1	Forrest
(	(	kIx(	(
<g/>
s	s	k7c7	s
pseudonymem	pseudonym	k1gInSc7	pseudonym
Hňup	hňup	k1gMnSc1	hňup
<g/>
)	)	kIx)	)
často	často	k6eAd1	často
vyhrával	vyhrávat	k5eAaImAgMnS	vyhrávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
domluvenými	domluvený	k2eAgInPc7d1	domluvený
zápasy	zápas	k1gInPc7	zápas
a	a	k8xC	a
podobnými	podobný	k2eAgFnPc7d1	podobná
nekalými	kalý	k2eNgFnPc7d1	nekalá
praktikami	praktika	k1gFnPc7	praktika
v	v	k7c6	v
zákulisí	zákulisí	k1gNnSc6	zákulisí
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Jenny	Jenn	k1gInPc4	Jenn
byla	být	k5eAaImAgFnS	být
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
<g/>
,	,	kIx,	,
s	s	k7c7	s
Forrestem	Forrest	k1gInSc7	Forrest
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
milovali	milovat	k5eAaImAgMnP	milovat
<g/>
,	,	kIx,	,
Jenny	Jenen	k2eAgFnPc1d1	Jenna
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gNnSc4	on
bála	bát	k5eAaImAgFnS	bát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
Jenny	Jenna	k1gMnSc2	Jenna
Forresta	Forrest	k1gMnSc2	Forrest
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Forrest	Forrest	k1gInSc4	Forrest
končí	končit	k5eAaImIp3nS	končit
se	se	k3xPyFc4	se
zápasy	zápas	k1gInPc1	zápas
a	a	k8xC	a
Dan	Dan	k1gMnSc1	Dan
ho	on	k3xPp3gNnSc4	on
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
,	,	kIx,	,
Forrest	Forrest	k1gFnSc1	Forrest
autobusem	autobus	k1gInSc7	autobus
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
do	do	k7c2	do
Mobile	mobile	k1gNnSc2	mobile
<g/>
,	,	kIx,	,
přestupuje	přestupovat	k5eAaImIp3nS	přestupovat
v	v	k7c6	v
Nashville	Nashvilla	k1gFnSc6	Nashvilla
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
se	s	k7c7	s
staříkem	stařík	k1gMnSc7	stařík
Tribblem	Tribbl	k1gMnSc7	Tribbl
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
šachy	šach	k1gInPc4	šach
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němu	on	k3xPp3gMnSc3	on
se	se	k3xPyFc4	se
Forrest	Forrest	k1gInSc1	Forrest
přihlásí	přihlásit	k5eAaPmIp3nS	přihlásit
do	do	k7c2	do
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
šachového	šachový	k2eAgInSc2d1	šachový
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gInSc4	on
první	první	k4xOgInSc4	první
šachový	šachový	k2eAgInSc4d1	šachový
zápas	zápas	k1gInSc4	zápas
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Forrest	Forrest	k1gInSc1	Forrest
s	s	k7c7	s
Tribblem	Tribbl	k1gInSc7	Tribbl
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Disneyland	Disneyland	k1gInSc1	Disneyland
a	a	k8xC	a
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
režisérem	režisér	k1gMnSc7	režisér
Felderem	Felder	k1gMnSc7	Felder
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
ho	on	k3xPp3gMnSc4	on
obsadí	obsadit	k5eAaPmIp3nS	obsadit
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
remake	remake	k1gFnSc1	remake
"	"	kIx"	"
<g/>
Netvora	netvora	k1gFnSc1	netvora
z	z	k7c2	z
Černé	Černé	k2eAgFnSc2d1	Černé
laguny	laguna	k1gFnSc2	laguna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
do	do	k7c2	do
role	role	k1gFnSc2	role
mořského	mořský	k2eAgNnSc2d1	mořské
monstra	monstrum	k1gNnSc2	monstrum
<g/>
,	,	kIx,	,
Forrest	Forrest	k1gFnSc1	Forrest
tak	tak	k9	tak
má	mít	k5eAaImIp3nS	mít
hrát	hrát	k5eAaImF	hrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Raquel	Raquela	k1gFnPc2	Raquela
Welchové	Welchový	k2eAgFnPc1d1	Welchový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šachovém	šachový	k2eAgInSc6d1	šachový
turnaji	turnaj	k1gInSc6	turnaj
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
se	se	k3xPyFc4	se
Forrest	Forrest	k1gInSc1	Forrest
opět	opět	k6eAd1	opět
shledá	shledat	k5eAaPmIp3nS	shledat
s	s	k7c7	s
opičákem	opičák	k1gMnSc7	opičák
Sue	Sue	k1gFnSc2	Sue
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
Tarzanovi	Tarzan	k1gMnSc6	Tarzan
<g/>
,	,	kIx,	,
Forrestova	Forrestův	k2eAgFnSc1d1	Forrestova
herecká	herecký	k2eAgFnSc1d1	herecká
kariéra	kariéra	k1gFnSc1	kariéra
je	být	k5eAaImIp3nS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
humorným	humorný	k2eAgNnSc7d1	humorné
dobrodružstvím	dobrodružství	k1gNnSc7	dobrodružství
s	s	k7c7	s
nahou	nahý	k2eAgFnSc7d1	nahá
Raquel	Raquela	k1gFnPc2	Raquela
<g/>
,	,	kIx,	,
po	po	k7c4	po
které	který	k3yQgInPc4	který
oba	dva	k4xCgInPc4	dva
skončily	skončit	k5eAaPmAgFnP	skončit
krátce	krátce	k6eAd1	krátce
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Forrestovo	Forrestův	k2eAgNnSc4d1	Forrestův
šachové	šachový	k2eAgNnSc4d1	šachové
finále	finále	k1gNnSc4	finále
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
ukončenou	ukončený	k2eAgFnSc7d1	ukončená
"	"	kIx"	"
<g/>
díky	díky	k7c3	díky
<g/>
"	"	kIx"	"
Sue	Sue	k1gFnSc3	Sue
a	a	k8xC	a
Forrestovu	Forrestův	k2eAgInSc3d1	Forrestův
močovému	močový	k2eAgInSc3d1	močový
měchýři	měchýř	k1gInSc3	měchýř
(	(	kIx(	(
<g/>
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
si	se	k3xPyFc3	se
jít	jít	k5eAaImF	jít
ulevit	ulevit	k5eAaPmF	ulevit
v	v	k7c4	v
nejméně	málo	k6eAd3	málo
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
dobu	doba	k1gFnSc4	doba
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tribble	Tribble	k6eAd1	Tribble
pošle	poslat	k5eAaPmIp3nS	poslat
Suea	Suea	k1gFnSc1	Suea
do	do	k7c2	do
Alabamy	Alabam	k1gInPc1	Alabam
a	a	k8xC	a
Forrestovi	Forresta	k1gMnSc3	Forresta
dá	dát	k5eAaPmIp3nS	dát
výhry	výhra	k1gFnPc1	výhra
z	z	k7c2	z
šachu	šach	k1gInSc2	šach
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
5000	[number]	k4	5000
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
Forrest	Forrest	k1gFnSc1	Forrest
konečně	konečně	k6eAd1	konečně
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
domů	dům	k1gInPc2	dům
do	do	k7c2	do
Mobile	mobile	k1gNnSc2	mobile
<g/>
.	.	kIx.	.
</s>
<s>
Shledává	shledávat	k5eAaImIp3nS	shledávat
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
už	už	k9	už
opustil	opustit	k5eAaPmAgMnS	opustit
její	její	k3xOp3gMnSc1	její
milenec	milenec	k1gMnSc1	milenec
protestant	protestant	k1gMnSc1	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gFnSc4	Forrest
touží	toužit	k5eAaImIp3nP	toužit
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
svůj	svůj	k3xOyFgInSc4	svůj
sen	sen	k1gInSc4	sen
<g/>
,	,	kIx,	,
pěstovat	pěstovat	k5eAaImF	pěstovat
krevety	kreveta	k1gFnPc4	kreveta
<g/>
,	,	kIx,	,
setkává	setkávat	k5eAaImIp3nS	setkávat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
s	s	k7c7	s
Bubbovým	Bubbův	k2eAgMnSc7d1	Bubbův
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gMnSc1	Forrest
rozjíždí	rozjíždět	k5eAaImIp3nS	rozjíždět
svou	svůj	k3xOyFgFnSc4	svůj
firmu	firma	k1gFnSc4	firma
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
krevety	kreveta	k1gFnPc4	kreveta
a	a	k8xC	a
poté	poté	k6eAd1	poté
je	on	k3xPp3gFnPc4	on
výhodně	výhodně	k6eAd1	výhodně
prodává	prodávat	k5eAaImIp3nS	prodávat
<g/>
,	,	kIx,	,
máma	máma	k1gFnSc1	máma
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
o	o	k7c6	o
účetnictví	účetnictví	k1gNnSc6	účetnictví
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
i	i	k9	i
opičák	opičák	k1gMnSc1	opičák
Sue	Sue	k1gMnSc1	Sue
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Forrestem	Forrest	k1gInSc7	Forrest
přijel	přijet	k5eAaPmAgMnS	přijet
Curtis	Curtis	k1gInSc4	Curtis
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
Forrest	Forrest	k1gMnSc1	Forrest
zaměstnal	zaměstnat	k5eAaPmAgMnS	zaměstnat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
firmě	firma	k1gFnSc6	firma
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Forresta	Forrest	k1gMnSc2	Forrest
se	se	k3xPyFc4	se
zanedlouho	zanedlouho	k6eAd1	zanedlouho
stává	stávat	k5eAaImIp3nS	stávat
milionář	milionář	k1gMnSc1	milionář
<g/>
.	.	kIx.	.
</s>
<s>
Zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jenny	Jenna	k1gFnSc2	Jenna
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Karolíně	Karolína	k1gFnSc6	Karolína
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Forrestem	Forrest	k1gInSc7	Forrest
přišli	přijít	k5eAaPmAgMnP	přijít
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
chtěli	chtít	k5eAaImAgMnP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nakonec	nakonec	k6eAd1	nakonec
taky	taky	k6eAd1	taky
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gInSc1	Forrest
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Co	co	k3yInSc4	co
považujete	považovat	k5eAaImIp2nP	považovat
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
za	za	k7c4	za
nejnaléhavější	naléhavý	k2eAgInSc4d3	nejnaléhavější
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Odpověděl	odpovědět	k5eAaPmAgInS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
čase	čas	k1gInSc6	čas
si	se	k3xPyFc3	se
ulevit	ulevit	k5eAaPmF	ulevit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
skutečně	skutečně	k6eAd1	skutečně
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
ulevit	ulevit	k5eAaPmF	ulevit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gNnSc4	jeho
motto	motto	k1gNnSc4	motto
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
pochopeno	pochopit	k5eAaPmNgNnS	pochopit
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
země	země	k1gFnSc1	země
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
ujalo	ujmout	k5eAaPmAgNnS	ujmout
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc7	jeho
volebním	volební	k2eAgNnSc7d1	volební
heslem	heslo	k1gNnSc7	heslo
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k5eAaPmF	Forrest
by	by	kYmCp3nS	by
snad	snad	k9	snad
i	i	k9	i
volby	volba	k1gFnPc4	volba
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
novináři	novinář	k1gMnPc1	novinář
na	na	k7c6	na
Forresta	Forrest	k1gMnSc4	Forrest
vytahovali	vytahovat	k5eAaImAgMnP	vytahovat
špínu	špína	k1gMnSc4	špína
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
minulosti	minulost	k1gFnSc2	minulost
a	a	k8xC	a
Forrest	Forrest	k1gInSc1	Forrest
nebyl	být	k5eNaImAgInS	být
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
,	,	kIx,	,
nechtěl	chtít	k5eNaImAgMnS	chtít
žít	žít	k5eAaImF	žít
takový	takový	k3xDgInSc4	takový
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
rád	rád	k6eAd1	rád
chytal	chytat	k5eAaImAgMnS	chytat
krevety	kreveta	k1gFnSc2	kreveta
a	a	k8xC	a
myslel	myslet	k5eAaImAgMnS	myslet
na	na	k7c4	na
Jenny	Jenna	k1gFnPc4	Jenna
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gInSc1	Forrest
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
někam	někam	k6eAd1	někam
odjet	odjet	k5eAaPmF	odjet
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
na	na	k7c6	na
autobusovém	autobusový	k2eAgNnSc6d1	autobusové
nádraží	nádraží	k1gNnSc6	nádraží
koupil	koupit	k5eAaPmAgInS	koupit
lístek	lístek	k1gInSc4	lístek
do	do	k7c2	do
Savannah	Savannaha	k1gFnPc2	Savannaha
a	a	k8xC	a
vzal	vzít	k5eAaPmAgMnS	vzít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
i	i	k8xC	i
opičáka	opičák	k1gMnSc4	opičák
<g/>
.	.	kIx.	.
</s>
<s>
Nevěděl	vědět	k5eNaImAgMnS	vědět
co	co	k3yInSc4	co
by	by	kYmCp3nS	by
dělal	dělat	k5eAaImAgMnS	dělat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
v	v	k7c4	v
Savannah	Savannah	k1gInSc4	Savannah
na	na	k7c6	na
lavičce	lavička	k1gFnSc6	lavička
v	v	k7c6	v
parku	park	k1gInSc6	park
začal	začít	k5eAaPmAgInS	začít
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
foukací	foukací	k2eAgFnSc4d1	foukací
harmoniku	harmonika	k1gFnSc4	harmonika
<g/>
,	,	kIx,	,
kolemjdoucí	kolemjdoucí	k1gMnPc1	kolemjdoucí
mu	on	k3xPp3gMnSc3	on
začali	začít	k5eAaPmAgMnP	začít
házet	házet	k5eAaImF	házet
drobné	drobná	k1gFnPc4	drobná
<g/>
,	,	kIx,	,
Forrest	Forrest	k1gFnSc4	Forrest
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
opět	opět	k6eAd1	opět
začal	začít	k5eAaPmAgMnS	začít
vydělávat	vydělávat	k5eAaImF	vydělávat
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
jeho	on	k3xPp3gInSc4	on
záměr	záměr	k1gInSc4	záměr
<g/>
.	.	kIx.	.
</s>
<s>
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
Danem	Dan	k1gMnSc7	Dan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
čistič	čistič	k1gMnSc1	čistič
bot	bota	k1gFnPc2	bota
<g/>
.	.	kIx.	.
</s>
<s>
Žili	žít	k5eAaImAgMnP	žít
tak	tak	k6eAd1	tak
spolu	spolu	k6eAd1	spolu
asi	asi	k9	asi
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
když	když	k8xS	když
Forrest	Forrest	k1gInSc1	Forrest
potkal	potkat	k5eAaPmAgInS	potkat
Jenny	Jenna	k1gFnPc4	Jenna
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
malého	malý	k2eAgMnSc4d1	malý
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vedoucí	vedoucí	k1gMnSc1	vedoucí
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
střešních	střešní	k2eAgFnPc2d1	střešní
tašek	taška	k1gFnPc2	taška
<g/>
.	.	kIx.	.
</s>
<s>
Jenny	Jenna	k1gMnSc2	Jenna
synka	synek	k1gMnSc2	synek
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
Forrest	Forrest	k1gFnSc4	Forrest
a	a	k8xC	a
přiznala	přiznat	k5eAaPmAgFnS	přiznat
se	se	k3xPyFc4	se
Forrestovi	Forresta	k1gMnSc3	Forresta
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jeho	on	k3xPp3gMnSc4	on
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Forrest	Forrest	k1gMnSc1	Forrest
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
zisku	zisk	k1gInSc2	zisk
sádek	sádek	k1gInSc4	sádek
s	s	k7c7	s
krevetami	kreveta	k1gFnPc7	kreveta
rozdělit	rozdělit	k5eAaPmF	rozdělit
mezi	mezi	k7c4	mezi
matku	matka	k1gFnSc4	matka
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bubbova	Bubbův	k2eAgMnSc4d1	Bubbův
tátu	táta	k1gMnSc4	táta
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
pro	pro	k7c4	pro
malého	malý	k2eAgMnSc4d1	malý
Forresta	Forrest	k1gMnSc4	Forrest
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Danem	Dan	k1gMnSc7	Dan
a	a	k8xC	a
Sue	Sue	k1gFnSc7	Sue
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
jali	jmout	k5eAaPmAgMnP	jmout
na	na	k7c4	na
cestování	cestování	k1gNnSc4	cestování
po	po	k7c6	po
Státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Forrestovi	Forresta	k1gMnSc3	Forresta
je	být	k5eAaImIp3nS	být
čtyřicet	čtyřicet	k4xCc1	čtyřicet
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
spoustu	spousta	k1gFnSc4	spousta
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
sny	sen	k1gInPc1	sen
<g/>
,	,	kIx,	,
neví	vědět	k5eNaImIp3nS	vědět
co	co	k3yInSc4	co
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
dít	dít	k5eAaImF	dít
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
neměl	mít	k5eNaImAgMnS	mít
nudný	nudný	k2eAgInSc4d1	nudný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
vám	vy	k3xPp2nPc3	vy
teda	teda	k?	teda
povim	povim	k?	povim
<g/>
:	:	kIx,	:
bejt	bejt	k?	bejt
idijot	idijot	k1gInSc1	idijot
není	být	k5eNaImIp3nS	být
žádnej	žádnej	k?	žádnej
med	med	k1gInSc1	med
<g/>
.	.	kIx.	.
</s>
<s>
Lidi	člověk	k1gMnPc4	člověk
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
akorát	akorát	k6eAd1	akorát
smějou	smát	k5eAaImIp3nP	smát
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
s	s	k7c7	s
váma	váma	k?	váma
trpělivost	trpělivost	k1gFnSc1	trpělivost
a	a	k8xC	a
chovaj	chovaj	k?	chovaj
se	se	k3xPyFc4	se
hnusně	hnusně	k6eAd1	hnusně
<g/>
.	.	kIx.	.
</s>
<s>
Vono	Vono	k6eAd1	Vono
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
namluví	namluvit	k5eAaBmIp3nS	namluvit
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
na	na	k7c4	na
nás	my	k3xPp1nPc4	my
postižený	postižený	k2eAgMnSc1d1	postižený
měly	mít	k5eAaImAgInP	mít
bejt	bejt	k?	bejt
hodný	hodný	k2eAgInSc4d1	hodný
jenomže	jenomže	k8xC	jenomže
povídali	povídat	k5eAaImAgMnP	povídat
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
hráli	hrát	k5eAaImAgMnP	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
nestěžuju	stěžovat	k5eNaImIp1nS	stěžovat
si	se	k3xPyFc3	se
poněvač	poněvač	k1gInSc4	poněvač
si	se	k3xPyFc3	se
myslím	myslet	k5eAaImIp1nS	myslet
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
užil	užít	k5eAaPmAgMnS	užít
docela	docela	k6eAd1	docela
zajímavej	zajímavej	k?	zajímavej
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Idijot	Idijota	k1gFnPc2	Idijota
jsem	být	k5eAaImIp1nS	být
vod	voda	k1gFnPc2	voda
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
</s>
<s>
IQ	iq	kA	iq
mi	já	k3xPp1nSc3	já
zjistili	zjistit	k5eAaPmAgMnP	zjistit
pod	pod	k7c4	pod
sedumdesát	sedumdesát	k1gInSc4	sedumdesát
takže	takže	k8xS	takže
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
mám	mít	k5eAaImIp1nS	mít
i	i	k9	i
papíry	papír	k1gInPc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Teda	Ted	k1gMnSc4	Ted
já	já	k3xPp1nSc1	já
bysem	bysem	k?	bysem
tipoval	tipovat	k5eAaImAgMnS	tipovat
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
spíš	spíš	k9	spíš
inbecil	inbecil	k1gInSc4	inbecil
nebo	nebo	k8xC	nebo
možná	možná	k9	možná
dokonce	dokonce	k9	dokonce
debil	debil	k1gMnSc1	debil
i	i	k8xC	i
když	když	k8xS	když
osobně	osobně	k6eAd1	osobně
se	se	k3xPyFc4	se
nejradějc	jradějc	k6eNd1	jradějc
považuju	považovat	k5eAaImIp1nS	považovat
spíš	spíš	k9	spíš
za	za	k7c4	za
slabomyslnýho	slabomyslnýho	k?	slabomyslnýho
protože	protože	k8xS	protože
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
řekne	říct	k5eAaPmIp3nS	říct
idijot	idijot	k1gInSc1	idijot
lidi	člověk	k1gMnPc4	člověk
si	se	k3xPyFc3	se
většinou	většinou	k6eAd1	většinou
představěj	představět	k5eAaPmRp2nS	představět
nějakýho	nějakýho	k?	nějakýho
mongolojda	mongolojdo	k1gNnSc2	mongolojdo
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
jsou	být	k5eAaImIp3nP	být
takový	takový	k3xDgInSc4	takový
ty	ten	k3xDgInPc4	ten
co	co	k9	co
maj	maj	k?	maj
voči	voči	k6eAd1	voči
až	až	k6eAd1	až
u	u	k7c2	u
sebe	se	k3xPyFc2	se
a	a	k8xC	a
vypadaj	vypadaj	k?	vypadaj
jak	jak	k8xS	jak
čínani	čínan	k1gMnPc1	čínan
takže	takže	k8xS	takže
furt	furt	k?	furt
sliněj	sliněj	k?	sliněj
a	a	k8xC	a
sahaj	sahaj	k?	sahaj
si	se	k3xPyFc3	se
na	na	k7c4	na
příro	příro	k1gNnSc4	příro
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Winston	Winston	k1gInSc1	Winston
Groom	groom	k1gMnSc1	groom
"	"	kIx"	"
<g/>
Forrest	Forrest	k1gMnSc1	Forrest
Gump	Gump	k1gMnSc1	Gump
<g/>
"	"	kIx"	"
Praha	Praha	k1gFnSc1	Praha
Nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
XYZ	XYZ	kA	XYZ
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Šimon	Šimon	k1gMnSc1	Šimon
Pellar	Pellar	k1gMnSc1	Pellar
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
byla	být	k5eAaImAgFnS	být
natočena	natočen	k2eAgFnSc1d1	natočena
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
Forrest	Forrest	k1gMnSc1	Forrest
Gump	Gump	k1gMnSc1	Gump
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Zemeckis	Zemeckis	k1gFnSc2	Zemeckis
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Tom	Tom	k1gMnSc1	Tom
Hanks	Hanksa	k1gFnPc2	Hanksa
<g/>
,	,	kIx,	,
Robin	robin	k2eAgMnSc1d1	robin
Wright	Wright	k1gMnSc1	Wright
Penn	Penn	k1gMnSc1	Penn
<g/>
,	,	kIx,	,
Gary	Gar	k2eAgFnPc1d1	Gara
Sinise	Sinise	k1gFnPc1	Sinise
<g/>
,	,	kIx,	,
Sally	Sall	k1gInPc1	Sall
Field	Field	k1gInSc1	Field
<g/>
)	)	kIx)	)
filmová	filmový	k2eAgFnSc1d1	filmová
verze	verze	k1gFnSc1	verze
zastínila	zastínit	k5eAaPmAgFnS	zastínit
svou	svůj	k3xOyFgFnSc4	svůj
literární	literární	k2eAgFnSc4d1	literární
předlohu	předloha	k1gFnSc4	předloha
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
6	[number]	k4	6
Oscarů	Oscar	k1gInPc2	Oscar
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
se	se	k3xPyFc4	se
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
literární	literární	k2eAgFnSc2d1	literární
předlohy	předloha	k1gFnSc2	předloha
Winstona	Winston	k1gMnSc2	Winston
Grooma	groom	k1gMnSc2	groom
<g/>
.	.	kIx.	.
</s>
