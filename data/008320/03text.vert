<p>
<s>
Hoboj	hoboj	k1gFnSc1	hoboj
(	(	kIx(	(
<g/>
z	z	k7c2	z
francouzského	francouzský	k2eAgMnSc2d1	francouzský
hautbois	hautbois	k1gInSc1	hautbois
–	–	k?	–
"	"	kIx"	"
<g/>
vysoké	vysoký	k2eAgNnSc1d1	vysoké
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dvojplátkový	dvojplátkový	k2eAgInSc1d1	dvojplátkový
dechový	dechový	k2eAgInSc1d1	dechový
nástroj	nástroj	k1gInSc1	nástroj
laděný	laděný	k2eAgInSc1d1	laděný
v	v	k7c6	v
C.	C.	kA	C.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
široké	široký	k2eAgNnSc4d1	široké
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
hudbě	hudba	k1gFnSc6	hudba
–	–	k?	–
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
symfonických	symfonický	k2eAgInPc2d1	symfonický
i	i	k8xC	i
komorních	komorní	k2eAgInPc2d1	komorní
orchestrů	orchestr	k1gInPc2	orchestr
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dechových	dechový	k2eAgMnPc2d1	dechový
komorních	komorní	k2eAgMnPc2d1	komorní
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
a	a	k8xC	a
stavba	stavba	k1gFnSc1	stavba
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
hoboj	hoboj	k1gInSc4	hoboj
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
složitější	složitý	k2eAgNnSc1d2	složitější
vytvořit	vytvořit	k5eAaPmF	vytvořit
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
tón	tón	k1gInSc4	tón
než	než	k8xS	než
například	například	k6eAd1	například
na	na	k7c4	na
klarinet	klarinet	k1gInSc4	klarinet
nebo	nebo	k8xC	nebo
příčnou	příčný	k2eAgFnSc4d1	příčná
flétnu	flétna	k1gFnSc4	flétna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
nástroji	nástroj	k1gInPc7	nástroj
má	mít	k5eAaImIp3nS	mít
hoboj	hoboj	k1gFnSc4	hoboj
také	také	k9	také
pronikavější	pronikavý	k2eAgInSc4d2	pronikavější
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
stavba	stavba	k1gFnSc1	stavba
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
sudé	sudý	k2eAgFnPc4d1	sudá
vyšší	vysoký	k2eAgFnPc4d2	vyšší
harmonické	harmonický	k2eAgFnPc4d1	harmonická
frekvence	frekvence	k1gFnPc4	frekvence
(	(	kIx(	(
<g/>
klarinet	klarinet	k1gInSc4	klarinet
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
silnější	silný	k2eAgFnSc1d2	silnější
liché	lichý	k2eAgFnPc4d1	lichá
vyšší	vysoký	k2eAgFnPc4d2	vyšší
harmonické	harmonický	k2eAgFnPc4d1	harmonická
frekvence	frekvence	k1gFnPc4	frekvence
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
zvuk	zvuk	k1gInSc1	zvuk
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
jemnější	jemný	k2eAgNnSc1d2	jemnější
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
orchestru	orchestr	k1gInSc6	orchestr
přítomen	přítomen	k2eAgInSc1d1	přítomen
klavír	klavír	k1gInSc1	klavír
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc1d1	jiný
klávesový	klávesový	k2eAgInSc1d1	klávesový
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc1d1	ostatní
nástroje	nástroj	k1gInPc1	nástroj
se	se	k3xPyFc4	se
ladí	ladit	k5eAaImIp3nP	ladit
podle	podle	k7c2	podle
hoboje	hoboj	k1gFnSc2	hoboj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gNnSc4	jeho
ladění	ladění	k1gNnSc4	ladění
je	být	k5eAaImIp3nS	být
nejstálejší	stálý	k2eAgMnSc1d3	nejstálejší
a	a	k8xC	a
nejodolnější	odolný	k2eAgFnPc1d3	nejodolnější
vůči	vůči	k7c3	vůči
výkyvům	výkyv	k1gInPc3	výkyv
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
vlhkosti	vlhkost	k1gFnSc2	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
hoboje	hoboj	k1gInPc1	hoboj
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
z	z	k7c2	z
grenadilly	grenadilla	k1gFnSc2	grenadilla
(	(	kIx(	(
<g/>
typ	typ	k1gInSc4	typ
afrického	africký	k2eAgNnSc2d1	africké
tropického	tropický	k2eAgNnSc2d1	tropické
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
některé	některý	k3yIgInPc1	některý
nástroje	nástroj	k1gInPc1	nástroj
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
i	i	k9	i
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
dřev	dřevo	k1gNnPc2	dřevo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
palisandru	palisandr	k1gInSc2	palisandr
<g/>
.	.	kIx.	.
</s>
<s>
Hoboj	hoboj	k1gFnSc1	hoboj
nemá	mít	k5eNaImIp3nS	mít
hubičku	hubička	k1gFnSc4	hubička
jako	jako	k8xS	jako
klarinet	klarinet	k1gInSc4	klarinet
nebo	nebo	k8xC	nebo
saxofon	saxofon	k1gInSc4	saxofon
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
ní	on	k3xPp3gFnSc2	on
má	mít	k5eAaImIp3nS	mít
tzv.	tzv.	kA	tzv.
strojek	strojek	k1gInSc1	strojek
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
jej	on	k3xPp3gMnSc4	on
dvojice	dvojice	k1gFnSc1	dvojice
úzkých	úzký	k2eAgInPc2d1	úzký
plátků	plátek	k1gInPc2	plátek
ze	z	k7c2	z
zdřevnatělé	zdřevnatělý	k2eAgFnSc2d1	zdřevnatělá
trstě	trsť	k1gFnSc2	trsť
rákosovité	rákosovitý	k2eAgFnSc2d1	rákosovitá
(	(	kIx(	(
<g/>
Arundo	Arundo	k6eAd1	Arundo
donax	donax	k1gInSc1	donax
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
připevněny	připevněn	k2eAgInPc1d1	připevněn
na	na	k7c6	na
kovové	kovový	k2eAgFnSc6d1	kovová
rource	rourka	k1gFnSc6	rourka
a	a	k8xC	a
narážejí	narážet	k5eAaImIp3nP	narážet
o	o	k7c4	o
sebe	sebe	k3xPyFc4	sebe
volnými	volný	k2eAgNnPc7d1	volné
konci	konec	k1gInSc3	konec
<g/>
.	.	kIx.	.
</s>
<s>
Vespod	vespod	k6eAd1	vespod
je	být	k5eAaImIp3nS	být
utěsněn	utěsnit	k5eAaPmNgInS	utěsnit
korkem	korek	k1gInSc7	korek
a	a	k8xC	a
nasazen	nasadit	k5eAaPmNgMnS	nasadit
na	na	k7c4	na
nástroj	nástroj	k1gInSc4	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
hobojů	hoboj	k1gInPc2	hoboj
<g/>
:	:	kIx,	:
německý	německý	k2eAgMnSc1d1	německý
a	a	k8xC	a
francouzský	francouzský	k2eAgMnSc1d1	francouzský
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
typ	typ	k1gInSc1	typ
má	mít	k5eAaImIp3nS	mít
kónicky	kónicky	k6eAd1	kónicky
vrtanou	vrtaný	k2eAgFnSc4d1	vrtaná
trubici	trubice	k1gFnSc4	trubice
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
francouzského	francouzský	k2eAgMnSc2d1	francouzský
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
trubice	trubice	k1gFnSc1	trubice
je	být	k5eAaImIp3nS	být
vrtaná	vrtaný	k2eAgFnSc1d1	vrtaná
válcově	válcově	k6eAd1	válcově
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1	francouzský
typ	typ	k1gInSc1	typ
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
německým	německý	k2eAgInSc7d1	německý
také	také	k9	také
menší	malý	k2eAgFnSc4d2	menší
menzuru	menzura	k1gFnSc4	menzura
<g/>
,	,	kIx,	,
užší	úzký	k2eAgInSc4d2	užší
plátek	plátek	k1gInSc4	plátek
a	a	k8xC	a
poněkud	poněkud	k6eAd1	poněkud
jinak	jinak	k6eAd1	jinak
položené	položený	k2eAgFnPc4d1	položená
dírky	dírka	k1gFnPc4	dírka
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
zní	znět	k5eAaImIp3nS	znět
ostřeji	ostro	k6eAd2	ostro
než	než	k8xS	než
německý	německý	k2eAgMnSc1d1	německý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nástroje	nástroj	k1gInPc1	nástroj
příbuzné	příbuzná	k1gFnSc2	příbuzná
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
hoboji	hoboj	k1gInSc3	hoboj
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
již	již	k6eAd1	již
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
Přední	přední	k2eAgFnSc6d1	přední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
(	(	kIx(	(
<g/>
aulos	aulos	k1gInSc1	aulos
<g/>
)	)	kIx)	)
a	a	k8xC	a
Říma	Řím	k1gInSc2	Řím
(	(	kIx(	(
<g/>
tibia	tibia	k1gFnSc1	tibia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
Evropa	Evropa	k1gFnSc1	Evropa
znovu	znovu	k6eAd1	znovu
objevila	objevit	k5eAaPmAgFnS	objevit
tento	tento	k3xDgInSc4	tento
nástroj	nástroj	k1gInSc4	nástroj
díky	díky	k7c3	díky
stykům	styk	k1gInPc3	styk
s	s	k7c7	s
arabskými	arabský	k2eAgFnPc7d1	arabská
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přímým	přímý	k2eAgMnSc7d1	přímý
předchůdcem	předchůdce	k1gMnSc7	předchůdce
hoboje	hoboj	k1gInSc2	hoboj
je	být	k5eAaImIp3nS	být
středověká	středověký	k2eAgFnSc1d1	středověká
šalmaj	šalmaj	k1gFnSc1	šalmaj
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc7	její
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
vkládal	vkládat	k5eAaImAgInS	vkládat
celý	celý	k2eAgInSc1d1	celý
strojek	strojek	k1gInSc1	strojek
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
snížila	snížit	k5eAaPmAgFnS	snížit
možnost	možnost	k1gFnSc1	možnost
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
dynamiku	dynamika	k1gFnSc4	dynamika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
hoboj	hoboj	k1gFnSc4	hoboj
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
sestrojen	sestrojit	k5eAaPmNgInS	sestrojit
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
šalmaje	šalmaj	k1gInSc2	šalmaj
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgInPc4d1	možný
rty	ret	k1gInPc4	ret
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
tvorbu	tvorba	k1gFnSc4	tvorba
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
hobojů	hoboj	k1gInPc2	hoboj
==	==	k?	==
</s>
</p>
<p>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
roh	roh	k1gInSc1	roh
<g/>
:	:	kIx,	:
ladění	ladění	k1gNnSc1	ladění
in	in	k?	in
F	F	kA	F
<g/>
,	,	kIx,	,
zní	znět	k5eAaImIp3nS	znět
o	o	k7c4	o
kvintu	kvinta	k1gFnSc4	kvinta
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
hoboj	hoboj	k1gFnSc1	hoboj
</s>
</p>
<p>
<s>
Milostný	milostný	k2eAgInSc1d1	milostný
hoboj	hoboj	k1gInSc1	hoboj
(	(	kIx(	(
<g/>
oboe	oboe	k1gNnSc1	oboe
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
amore	amor	k1gMnSc5	amor
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
laděný	laděný	k2eAgMnSc1d1	laděný
in	in	k?	in
A	A	kA	A
<g/>
,	,	kIx,	,
s	s	k7c7	s
kulovitým	kulovitý	k2eAgInSc7d1	kulovitý
ozvučníkem	ozvučník	k1gInSc7	ozvučník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
dodává	dodávat	k5eAaImIp3nS	dodávat
jemnější	jemný	k2eAgInSc1d2	jemnější
tón	tón	k1gInSc1	tón
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
velice	velice	k6eAd1	velice
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
do	do	k7c2	do
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Heckelfon	Heckelfon	k1gInSc1	Heckelfon
<g/>
:	:	kIx,	:
barytonový	barytonový	k2eAgInSc1d1	barytonový
hoboj	hoboj	k1gInSc1	hoboj
laděný	laděný	k2eAgInSc1d1	laděný
in	in	k?	in
C	C	kA	C
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
širší	široký	k2eAgFnSc4d2	širší
kónickou	kónický	k2eAgFnSc4d1	kónická
trubici	trubice	k1gFnSc4	trubice
a	a	k8xC	a
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
ozvučník	ozvučník	k1gInSc4	ozvučník
</s>
</p>
<p>
<s>
Sarussafon	Sarussafon	k1gInSc1	Sarussafon
<g/>
:	:	kIx,	:
má	mít	k5eAaImIp3nS	mít
kovovou	kovový	k2eAgFnSc4d1	kovová
trubici	trubice	k1gFnSc4	trubice
se	s	k7c7	s
širokou	široký	k2eAgFnSc7d1	široká
menzurou	menzura	k1gFnSc7	menzura
a	a	k8xC	a
saxofonovou	saxofonový	k2eAgFnSc4d1	saxofonová
mechaniku	mechanika	k1gFnSc4	mechanika
</s>
</p>
<p>
<s>
==	==	k?	==
Nejdůležitější	důležitý	k2eAgNnPc4d3	nejdůležitější
díla	dílo	k1gNnPc4	dílo
pro	pro	k7c4	pro
hoboj	hoboj	k1gFnSc4	hoboj
==	==	k?	==
</s>
</p>
<p>
<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
:	:	kIx,	:
Hobojový	hobojový	k2eAgInSc1d1	hobojový
koncert	koncert	k1gInSc1	koncert
C	C	kA	C
dur	dur	k1gNnPc4	dur
<g/>
,	,	kIx,	,
Kvartet	kvartet	k1gInSc4	kvartet
F	F	kA	F
dur	dur	k1gNnSc1	dur
</s>
</p>
<p>
<s>
Antonio	Antonio	k1gMnSc1	Antonio
Vivaldi	Vivald	k1gMnPc1	Vivald
<g/>
:	:	kIx,	:
Hobojové	hobojový	k2eAgInPc1d1	hobojový
koncerty	koncert	k1gInPc1	koncert
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Braniborský	braniborský	k2eAgInSc1d1	braniborský
koncert	koncert	k1gInSc1	koncert
</s>
</p>
<p>
<s>
Tomaso	Tomasa	k1gFnSc5	Tomasa
Albinoni	Albinoň	k1gFnSc5	Albinoň
<g/>
:	:	kIx,	:
Hobojové	hobojový	k2eAgInPc4d1	hobojový
koncerty	koncert	k1gInPc4	koncert
</s>
</p>
<p>
<s>
Georg	Georg	k1gMnSc1	Georg
Friedrich	Friedrich	k1gMnSc1	Friedrich
Händel	Händlo	k1gNnPc2	Händlo
<g/>
:	:	kIx,	:
Příjezd	příjezd	k1gInSc1	příjezd
královny	královna	k1gFnSc2	královna
ze	z	k7c2	z
Sáby	Sába	k1gFnSc2	Sába
<g/>
,	,	kIx,	,
Hobojové	hobojový	k2eAgInPc4d1	hobojový
koncerty	koncert	k1gInPc4	koncert
a	a	k8xC	a
sonáty	sonáta	k1gFnPc4	sonáta
</s>
</p>
<p>
<s>
Benedetto	Benedett	k2eAgNnSc1d1	Benedetto
Marcello	Marcello	k1gNnSc1	Marcello
<g/>
:	:	kIx,	:
Koncert	koncert	k1gInSc1	koncert
c	c	k0	c
moll	moll	k1gNnSc6	moll
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
d	d	k?	d
moll	moll	k1gNnPc1	moll
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Georg	Georg	k1gMnSc1	Georg
Philipp	Philipp	k1gMnSc1	Philipp
Telemann	Telemann	k1gMnSc1	Telemann
<g/>
:	:	kIx,	:
Hobojové	hobojový	k2eAgInPc4d1	hobojový
koncerty	koncert	k1gInPc4	koncert
a	a	k8xC	a
sonáty	sonáta	k1gFnPc4	sonáta
</s>
</p>
<p>
<s>
Joseph	Joseph	k1gInSc1	Joseph
Haydn	Haydn	k1gInSc1	Haydn
<g/>
:	:	kIx,	:
Hobojový	hobojový	k2eAgInSc4d1	hobojový
koncert	koncert	k1gInSc4	koncert
C	C	kA	C
dur	dur	k1gNnSc1	dur
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Strauss	Strauss	k1gInSc1	Strauss
<g/>
:	:	kIx,	:
Hobojový	hobojový	k2eAgInSc1d1	hobojový
koncert	koncert	k1gInSc1	koncert
</s>
</p>
<p>
<s>
Vincenzo	Vincenza	k1gFnSc5	Vincenza
Bellini	Bellin	k2eAgMnPc1d1	Bellin
<g/>
:	:	kIx,	:
Hobojový	hobojový	k2eAgInSc1d1	hobojový
koncert	koncert	k1gInSc1	koncert
</s>
</p>
<p>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
Berio	Beria	k1gFnSc5	Beria
<g/>
:	:	kIx,	:
Sequenza	Sequenz	k1gMnSc4	Sequenz
VII	VII	kA	VII
</s>
</p>
<p>
<s>
Domenico	Domenico	k6eAd1	Domenico
Cimarosa	Cimarosa	k1gFnSc1	Cimarosa
<g/>
:	:	kIx,	:
Hobojový	hobojový	k2eAgInSc1d1	hobojový
koncert	koncert	k1gInSc1	koncert
C	C	kA	C
dur	dur	k1gNnPc6	dur
</s>
</p>
<p>
<s>
Francis	Francis	k1gFnSc1	Francis
Poulenc	Poulenc	k1gFnSc1	Poulenc
<g/>
:	:	kIx,	:
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
hoboj	hoboj	k1gFnSc4	hoboj
</s>
</p>
<p>
<s>
Benjamin	Benjamin	k1gMnSc1	Benjamin
Britten	Britten	k2eAgMnSc1d1	Britten
<g/>
:	:	kIx,	:
Šest	šest	k4xCc1	šest
metamorfóz	metamorfóza	k1gFnPc2	metamorfóza
na	na	k7c4	na
Ovidia	Ovidius	k1gMnSc4	Ovidius
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Schumann	Schumann	k1gMnSc1	Schumann
<g/>
:	:	kIx,	:
Tři	tři	k4xCgFnPc1	tři
romance	romance	k1gFnPc1	romance
pro	pro	k7c4	pro
hoboj	hoboj	k1gInSc4	hoboj
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
</s>
</p>
<p>
<s>
Carl	Carl	k1gMnSc1	Carl
Nielsen	Nielsna	k1gFnPc2	Nielsna
<g/>
:	:	kIx,	:
Dvě	dva	k4xCgFnPc1	dva
fantazie	fantazie	k1gFnPc1	fantazie
pro	pro	k7c4	pro
hoboj	hoboj	k1gInSc4	hoboj
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
</s>
</p>
<p>
<s>
Karl	Karl	k1gInSc1	Karl
Ditters	Ditters	k1gInSc1	Ditters
von	von	k1gInSc1	von
Dittersdorf	Dittersdorf	k1gInSc4	Dittersdorf
<g/>
:	:	kIx,	:
Hobojové	hobojový	k2eAgInPc4d1	hobojový
koncerty	koncert	k1gInPc4	koncert
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Martinů	Martinů	k1gMnSc1	Martinů
<g/>
:	:	kIx,	:
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
hoboj	hoboj	k1gFnSc4	hoboj
a	a	k8xC	a
malý	malý	k2eAgInSc1d1	malý
orchestr	orchestr	k1gInSc1	orchestr
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hoboj	hoboj	k1gInSc1	hoboj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hoboj	hoboj	k1gFnSc1	hoboj
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Oboe	oboe	k1gNnSc2	oboe
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oboetrainer	Oboetrainer	k1gInSc1	Oboetrainer
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Ruské	ruský	k2eAgNnSc1d1	ruské
hobojové	hobojový	k2eAgNnSc1d1	hobojový
rádio	rádio	k1gNnSc1	rádio
-	-	kIx~	-
www.oboe.fm	www.oboe.fm	k1gInSc1	www.oboe.fm
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
História	Histórium	k1gNnSc2	Histórium
hoboja	hoboj	k1gInSc2	hoboj
-	-	kIx~	-
annastarr	annastarr	k1gInSc1	annastarr
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Albinoni	Albinon	k1gMnPc1	Albinon
oboe	oboe	k1gNnSc2	oboe
concerto	concerta	k1gFnSc5	concerta
D	D	kA	D
minor	minor	k2eAgInSc4d1	minor
Movement	Movement	k1gInSc4	Movement
1	[number]	k4	1
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
McCandless	McCandlessa	k1gFnPc2	McCandlessa
plays	playsa	k1gFnPc2	playsa
oboe	oboe	k1gNnSc2	oboe
in	in	k?	in
Crestone	Creston	k1gInSc5	Creston
<g/>
,	,	kIx,	,
CO	co	k3yInSc1	co
</s>
</p>
<p>
<s>
Oboe	oboe	k1gNnSc1	oboe
band	banda	k1gFnPc2	banda
goes	goes	k6eAd1	goes
funky	funk	k1gInPc1	funk
</s>
</p>
