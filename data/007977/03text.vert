<s>
Benito	Benita	k1gMnSc5	Benita
Amilcare	Amilcar	k1gMnSc5	Amilcar
Andrea	Andrea	k1gFnSc1	Andrea
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1883	[number]	k4	1883
Dovia	Dovia	k1gFnSc1	Dovia
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
Giulino	Giulin	k2eAgNnSc4d1	Giulin
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
premiér	premiér	k1gMnSc1	premiér
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
diktátor	diktátor	k1gMnSc1	diktátor
<g/>
,	,	kIx,	,
spolutvůrce	spolutvůrce	k1gMnSc1	spolutvůrce
a	a	k8xC	a
průkopník	průkopník	k1gMnSc1	průkopník
fašismu	fašismus	k1gInSc2	fašismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Alessandro	Alessandra	k1gFnSc5	Alessandra
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
byl	být	k5eAaImAgInS	být
nejdříve	dříve	k6eAd3	dříve
kovářem	kovář	k1gMnSc7	kovář
v	v	k7c6	v
Predappiu	Predappium	k1gNnSc6	Predappium
a	a	k8xC	a
pak	pak	k6eAd1	pak
majitelem	majitel	k1gMnSc7	majitel
hostince	hostinec	k1gInSc2	hostinec
ve	v	k7c6	v
Forli	Forle	k1gFnSc6	Forle
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
učitelkou	učitelka	k1gFnSc7	učitelka
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
byla	být	k5eAaImAgFnS	být
revolucionářská	revolucionářský	k2eAgFnSc1d1	revolucionářská
(	(	kIx(	(
<g/>
Benito	Benit	k2eAgNnSc1d1	Benito
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
po	po	k7c6	po
Benitu	Benit	k1gMnSc6	Benit
Juárezovi	Juárez	k1gMnSc6	Juárez
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
učitelský	učitelský	k2eAgInSc1d1	učitelský
ústav	ústav	k1gInSc1	ústav
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
vojenské	vojenský	k2eAgFnSc3d1	vojenská
službě	služba	k1gFnSc3	služba
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
různými	různý	k2eAgFnPc7d1	různá
nekvalifikovanými	kvalifikovaný	k2eNgFnPc7d1	nekvalifikovaná
pracemi	práce	k1gFnPc7	práce
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
pomocník	pomocník	k1gMnSc1	pomocník
zedníka	zedník	k1gMnSc2	zedník
<g/>
,	,	kIx,	,
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Italské	italský	k2eAgFnSc2d1	italská
socialistické	socialistický	k2eAgFnSc2d1	socialistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
se	se	k3xPyFc4	se
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
stýkal	stýkat	k5eAaImAgInS	stýkat
s	s	k7c7	s
ruskými	ruský	k2eAgMnPc7d1	ruský
emigranty	emigrant	k1gMnPc7	emigrant
a	a	k8xC	a
s	s	k7c7	s
bolševiky	bolševik	k1gMnPc7	bolševik
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
V.	V.	kA	V.
I.	I.	kA	I.
Leninem	Lenin	k1gMnSc7	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
už	už	k6eAd1	už
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
nenašel	najít	k5eNaPmAgInS	najít
žádné	žádný	k3yNgNnSc4	žádný
pracovní	pracovní	k2eAgNnSc4d1	pracovní
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
žebrat	žebrat	k5eAaImF	žebrat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
za	za	k7c4	za
potulku	potulka	k1gFnSc4	potulka
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vykázán	vykázat	k5eAaPmNgInS	vykázat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
které	který	k3yIgInPc1	který
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
byl	být	k5eAaImAgInS	být
novinářem	novinář	k1gMnSc7	novinář
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
redaktorem	redaktor	k1gMnSc7	redaktor
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
Avanti	avanti	k0	avanti
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
novin	novina	k1gFnPc2	novina
Italské	italský	k2eAgFnSc2d1	italská
socialistické	socialistický	k2eAgFnSc2d1	socialistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
při	při	k7c6	při
demonstraci	demonstrace	k1gFnSc6	demonstrace
proti	proti	k7c3	proti
italsko-turecké	italskourecký	k2eAgFnSc3d1	italsko-turecký
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
napsal	napsat	k5eAaBmAgMnS	napsat
své	svůj	k3xOyFgNnSc4	svůj
pojednání	pojednání	k1gNnSc4	pojednání
<g/>
:	:	kIx,	:
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
il	il	k?	il
Veridico	Veridico	k1gMnSc1	Veridico
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
hlasatel	hlasatel	k1gMnSc1	hlasatel
pravdy	pravda	k1gFnSc2	pravda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
odborná	odborný	k2eAgFnSc1d1	odborná
studie	studie	k1gFnSc1	studie
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
publicistika	publicistika	k1gFnSc1	publicistika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
vydal	vydat	k5eAaPmAgMnS	vydat
knihu	kniha	k1gFnSc4	kniha
Pavel	Pavel	k1gMnSc1	Pavel
Helan	Helana	k1gFnPc2	Helana
Duce	Duce	k1gInSc1	Duce
a	a	k8xC	a
kacíř	kacíř	k1gMnSc1	kacíř
–	–	k?	–
Literární	literární	k2eAgNnSc1d1	literární
mládí	mládí	k1gNnSc1	mládí
Benita	Benit	k1gMnSc2	Benit
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
muž	muž	k1gMnSc1	muž
pravdy	pravda	k1gFnSc2	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
Pravda	pravda	k1gFnSc1	pravda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
veridico	veridico	k6eAd1	veridico
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
je	být	k5eAaImIp3nS	být
přiřknuto	přiřknout	k5eAaPmNgNnS	přiřknout
jako	jako	k8xC	jako
synonymum	synonymum	k1gNnSc4	synonymum
k	k	k7c3	k
Husovi	Hus	k1gMnSc3	Hus
<g/>
,	,	kIx,	,
použil	použít	k5eAaPmAgMnS	použít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
pseudonymu	pseudonym	k1gInSc6	pseudonym
vero	vero	k1gMnSc1	vero
eretico	eretico	k1gMnSc1	eretico
(	(	kIx(	(
<g/>
opravdový	opravdový	k2eAgMnSc1d1	opravdový
kacíř	kacíř	k1gMnSc1	kacíř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
podepisoval	podepisovat	k5eAaImAgMnS	podepisovat
své	svůj	k3xOyFgInPc4	svůj
články	článek	k1gInPc4	článek
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
se	s	k7c7	s
socialistickou	socialistický	k2eAgFnSc7d1	socialistická
stranou	strana	k1gFnSc7	strana
rozešel	rozejít	k5eAaPmAgInS	rozejít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
postupně	postupně	k6eAd1	postupně
přešel	přejít	k5eAaPmAgMnS	přejít
na	na	k7c4	na
pozice	pozice	k1gFnPc4	pozice
zastánců	zastánce	k1gMnPc2	zastánce
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
války	válka	k1gFnSc2	válka
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
schůzi	schůze	k1gFnSc4	schůze
v	v	k7c6	v
Milánu	Milán	k1gInSc3	Milán
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
pro	pro	k7c4	pro
politické	politický	k2eAgNnSc4d1	politické
a	a	k8xC	a
morální	morální	k2eAgNnSc4d1	morální
selhání	selhání	k1gNnSc4	selhání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
kritiky	kritik	k1gMnPc4	kritik
tehdy	tehdy	k6eAd1	tehdy
zvolal	zvolat	k5eAaPmAgMnS	zvolat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Myslíte	myslet	k5eAaImIp2nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mě	já	k3xPp1nSc2	já
můžete	moct	k5eAaImIp2nP	moct
zbavit	zbavit	k5eAaPmF	zbavit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uvidíte	uvidět	k5eAaPmIp2nP	uvidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vrátím	vrátit	k5eAaPmIp1nS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Jsem	být	k5eAaImIp1nS	být
a	a	k8xC	a
zůstanu	zůstat	k5eAaPmIp1nS	zůstat
socialistou	socialista	k1gMnSc7	socialista
a	a	k8xC	a
moje	můj	k3xOp1gInPc4	můj
názory	názor	k1gInPc4	názor
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nezmění	změnit	k5eNaPmIp3nS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Mám	mít	k5eAaImIp1nS	mít
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
však	však	k9	však
vlastní	vlastní	k2eAgFnSc7d1	vlastní
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgMnS	založit
také	také	k9	také
vlastní	vlastní	k2eAgFnPc4d1	vlastní
noviny	novina	k1gFnPc4	novina
Il	Il	k1gFnSc2	Il
popolo	popola	k1gFnSc5	popola
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italia	k1gFnSc1	Italia
(	(	kIx(	(
<g/>
Italský	italský	k2eAgInSc1d1	italský
lid	lid	k1gInSc1	lid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
intervenci	intervence	k1gFnSc4	intervence
Itálie	Itálie	k1gFnSc2	Itálie
propagoval	propagovat	k5eAaImAgMnS	propagovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
svém	svůj	k3xOyFgInSc6	svůj
listě	list	k1gInSc6	list
psal	psát	k5eAaImAgInS	psát
také	také	k9	také
oslavně	oslavně	k6eAd1	oslavně
o	o	k7c6	o
československých	československý	k2eAgFnPc6d1	Československá
legiích	legie	k1gFnPc6	legie
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
bylo	být	k5eAaImAgNnS	být
hloupostí	hloupost	k1gFnSc7	hloupost
myslet	myslet	k5eAaImF	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
socialistický	socialistický	k2eAgInSc1d1	socialistický
stát	stát	k1gInSc1	stát
by	by	kYmCp3nS	by
dokázal	dokázat	k5eAaPmAgInS	dokázat
překonat	překonat	k5eAaPmF	překonat
staré	starý	k2eAgFnPc4d1	stará
bariéry	bariéra	k1gFnPc4	bariéra
rasy	rasa	k1gFnSc2	rasa
a	a	k8xC	a
historických	historický	k2eAgInPc2d1	historický
nároků	nárok	k1gInPc2	nárok
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
něj	on	k3xPp3gMnSc4	on
nacionální	nacionální	k2eAgInSc1d1	nacionální
cit	cit	k1gInSc1	cit
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
nelze	lze	k6eNd1	lze
ho	on	k3xPp3gMnSc4	on
popřít	popřít	k5eAaPmF	popřít
<g/>
.	.	kIx.	.
</s>
<s>
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
se	se	k3xPyFc4	se
také	také	k9	také
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
ptal	ptat	k5eAaImAgMnS	ptat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
internacionalismus	internacionalismus	k1gInSc4	internacionalismus
absolutně	absolutně	k6eAd1	absolutně
nutnou	nutný	k2eAgFnSc7d1	nutná
součástí	součást	k1gFnSc7	součást
myšlenky	myšlenka	k1gFnSc2	myšlenka
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Itálie	Itálie	k1gFnSc1	Itálie
válku	válka	k1gFnSc4	válka
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc2	Rakousko-Uhersek
<g/>
.	.	kIx.	.
</s>
<s>
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
co	co	k3yQnSc4	co
nejdříve	dříve	k6eAd3	dříve
stát	stát	k5eAaPmF	stát
vojákem	voják	k1gMnSc7	voják
a	a	k8xC	a
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
úřední	úřední	k2eAgInPc4d1	úřední
předpisy	předpis	k1gInPc4	předpis
a	a	k8xC	a
mobilizační	mobilizační	k2eAgInSc4d1	mobilizační
plán	plán	k1gInSc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
jeho	jeho	k3xOp3gMnPc1	jeho
životopisci	životopisec	k1gMnPc1	životopisec
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nS	kdyby
opravdu	opravdu	k6eAd1	opravdu
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
k	k	k7c3	k
bojovým	bojový	k2eAgFnPc3d1	bojová
jednotkám	jednotka	k1gFnPc3	jednotka
hned	hned	k6eAd1	hned
dostane	dostat	k5eAaPmIp3nS	dostat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nezdá	zdát	k5eNaPmIp3nS	zdát
příliš	příliš	k6eAd1	příliš
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
.	.	kIx.	.
</s>
<s>
Povolán	povolat	k5eAaPmNgInS	povolat
byl	být	k5eAaImAgInS	být
až	až	k9	až
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1915	[number]	k4	1915
a	a	k8xC	a
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
základního	základní	k2eAgInSc2d1	základní
výcviku	výcvik	k1gInSc2	výcvik
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
odvelen	odvelen	k2eAgMnSc1d1	odvelen
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
jako	jako	k8xS	jako
příslušník	příslušník	k1gMnSc1	příslušník
33	[number]	k4	33
<g/>
.	.	kIx.	.
praporu	prapor	k1gInSc2	prapor
11	[number]	k4	11
<g/>
.	.	kIx.	.
elitního	elitní	k2eAgInSc2d1	elitní
pluku	pluk	k1gInSc2	pluk
bersagliérů	bersagliér	k1gInPc2	bersagliér
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
onemocněl	onemocnět	k5eAaPmAgInS	onemocnět
paratyfem	paratyf	k1gInSc7	paratyf
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemoc	nemoc	k1gFnSc4	nemoc
přežil	přežít	k5eAaPmAgMnS	přežít
a	a	k8xC	a
hned	hned	k6eAd1	hned
po	po	k7c6	po
uzdravení	uzdravení	k1gNnSc6	uzdravení
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
milenkou	milenka	k1gFnSc7	milenka
Rachel	Rachel	k1gInSc1	Rachel
Guidi	Guid	k1gMnPc1	Guid
<g/>
.	.	kIx.	.
</s>
<s>
Vojákem	voják	k1gMnSc7	voják
italské	italský	k2eAgFnSc2d1	italská
armády	armáda	k1gFnSc2	armáda
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1915	[number]	k4	1915
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
těžkém	těžký	k2eAgNnSc6d1	těžké
zranění	zranění	k1gNnSc6	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
založil	založit	k5eAaPmAgMnS	založit
svoji	svůj	k3xOyFgFnSc4	svůj
organizaci	organizace	k1gFnSc4	organizace
Fasci	Fasce	k1gFnSc3	Fasce
di	di	k?	di
Combattimento	Combattimento	k1gNnSc4	Combattimento
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
"	"	kIx"	"
<g/>
Bojové	bojový	k2eAgInPc1d1	bojový
svazky	svazek	k1gInPc1	svazek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
organizace	organizace	k1gFnSc1	organizace
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
fašistickou	fašistický	k2eAgFnSc4d1	fašistická
stranu	strana	k1gFnSc4	strana
–	–	k?	–
Partito	partita	k1gFnSc5	partita
Nazionale	Nazionale	k1gFnPc4	Nazionale
Fascista	Fascista	k1gMnSc1	Fascista
<g/>
.	.	kIx.	.
</s>
<s>
Raný	raný	k2eAgInSc1d1	raný
program	program	k1gInSc1	program
fašistického	fašistický	k2eAgNnSc2d1	fašistické
hnutí	hnutí	k1gNnSc2	hnutí
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
socialismem	socialismus	k1gInSc7	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Požadoval	požadovat	k5eAaImAgMnS	požadovat
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
rolníky	rolník	k1gMnPc4	rolník
<g/>
,	,	kIx,	,
zastoupení	zastoupení	k1gNnSc4	zastoupení
dělníků	dělník	k1gMnPc2	dělník
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
progresivní	progresivní	k2eAgNnSc4d1	progresivní
zdanění	zdanění	k1gNnSc4	zdanění
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
,	,	kIx,	,
znárodnění	znárodnění	k1gNnSc2	znárodnění
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
podniků	podnik	k1gInPc2	podnik
atd.	atd.	kA	atd.
Při	při	k7c6	při
smiřování	smiřování	k1gNnSc6	smiřování
socialismu	socialismus	k1gInSc2	socialismus
a	a	k8xC	a
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
převzal	převzít	k5eAaPmAgInS	převzít
fašismus	fašismus	k1gInSc1	fašismus
učení	učení	k1gNnSc2	učení
Enrika	Enrik	k1gMnSc2	Enrik
Corradiniho	Corradini	k1gMnSc2	Corradini
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
kterého	který	k3yRgInSc2	který
byla	být	k5eAaImAgFnS	být
Itálie	Itálie	k1gFnSc1	Itálie
vykořisťována	vykořisťovat	k5eAaImNgFnS	vykořisťovat
předními	přední	k2eAgFnPc7d1	přední
evropskými	evropský	k2eAgFnPc7d1	Evropská
mocnostmi	mocnost	k1gFnPc7	mocnost
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
proletářským	proletářský	k2eAgInSc7d1	proletářský
národem	národ	k1gInSc7	národ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
bohatým	bohatý	k2eAgInPc3d1	bohatý
národům	národ	k1gInPc3	národ
byl	být	k5eAaImAgInS	být
stejně	stejně	k6eAd1	stejně
naléhavý	naléhavý	k2eAgInSc1d1	naléhavý
jako	jako	k8xS	jako
boj	boj	k1gInSc1	boj
dělníků	dělník	k1gMnPc2	dělník
proti	proti	k7c3	proti
kapitalistům	kapitalista	k1gMnPc3	kapitalista
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
italského	italský	k2eAgInSc2d1	italský
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
ještě	ještě	k9	ještě
za	za	k7c4	za
Fasci	Fasce	k1gFnSc4	Fasce
di	di	k?	di
Combattimento	Combattimento	k1gNnSc1	Combattimento
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Údernou	úderný	k2eAgFnSc7d1	úderná
silou	síla	k1gFnSc7	síla
nového	nový	k2eAgNnSc2d1	nové
hnutí	hnutí	k1gNnSc2	hnutí
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
oddíly	oddíl	k1gInPc1	oddíl
(	(	kIx(	(
<g/>
squadry	squadr	k1gInPc1	squadr
<g/>
)	)	kIx)	)
Černých	Černých	k2eAgFnPc2d1	Černých
košil	košile	k1gFnPc2	košile
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
byli	být	k5eAaImAgMnP	být
převážně	převážně	k6eAd1	převážně
příslušníci	příslušník	k1gMnPc1	příslušník
Arditů	Ardit	k1gInPc2	Ardit
(	(	kIx(	(
<g/>
přepadové	přepadový	k2eAgFnPc1d1	přepadová
jednotky	jednotka	k1gFnPc1	jednotka
italské	italský	k2eAgFnSc2d1	italská
armády	armáda	k1gFnSc2	armáda
zformované	zformovaný	k2eAgFnSc2d1	zformovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
byla	být	k5eAaImAgFnS	být
černá	černý	k2eAgFnSc1d1	černá
košile	košile	k1gFnSc1	košile
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Squadristé	Squadrista	k1gMnPc1	Squadrista
zahájili	zahájit	k5eAaPmAgMnP	zahájit
sérii	série	k1gFnSc4	série
útoku	útok	k1gInSc2	útok
proti	proti	k7c3	proti
odborovým	odborový	k2eAgFnPc3d1	odborová
centrálám	centrála	k1gFnPc3	centrála
<g/>
,	,	kIx,	,
sídlům	sídlo	k1gNnPc3	sídlo
socialistické	socialistický	k2eAgFnSc2d1	socialistická
a	a	k8xC	a
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
redakcím	redakce	k1gFnPc3	redakce
levicových	levicový	k2eAgFnPc2d1	levicová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Terčem	terč	k1gInSc7	terč
útoků	útok	k1gInPc2	útok
se	se	k3xPyFc4	se
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Itálie	Itálie	k1gFnSc2	Itálie
stala	stát	k5eAaPmAgFnS	stát
i	i	k9	i
slovinská	slovinský	k2eAgFnSc1d1	slovinská
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Levice	levice	k1gFnSc1	levice
zareagovala	zareagovat	k5eAaPmAgFnS	zareagovat
formováním	formování	k1gNnSc7	formování
úderek	úderka	k1gFnPc2	úderka
jako	jako	k8xS	jako
byli	být	k5eAaImAgMnP	být
Arditi	Ardit	k2eAgMnPc1d1	Ardit
del	del	k?	del
Popolo	Popola	k1gFnSc5	Popola
a	a	k8xC	a
tak	tak	k9	tak
boje	boj	k1gInPc4	boj
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
italských	italský	k2eAgNnPc6d1	italské
městech	město	k1gNnPc6	město
připomínaly	připomínat	k5eAaImAgFnP	připomínat
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
podpořil	podpořit	k5eAaPmAgMnS	podpořit
zabírání	zabírání	k1gNnPc4	zabírání
některých	některý	k3yIgFnPc2	některý
továren	továrna	k1gFnPc2	továrna
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
dělníky	dělník	k1gMnPc4	dělník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ho	on	k3xPp3gMnSc4	on
uznali	uznat	k5eAaPmAgMnP	uznat
jako	jako	k8xC	jako
svého	svůj	k3xOyFgMnSc2	svůj
vůdce	vůdce	k1gMnSc2	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
koalici	koalice	k1gFnSc4	koalice
s	s	k7c7	s
levicí	levice	k1gFnSc7	levice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
neuskutečnilo	uskutečnit	k5eNaPmAgNnS	uskutečnit
a	a	k8xC	a
sjezd	sjezd	k1gInSc4	sjezd
fašistické	fašistický	k2eAgFnSc2d1	fašistická
strany	strana	k1gFnSc2	strana
přijal	přijmout	k5eAaPmAgMnS	přijmout
dle	dle	k7c2	dle
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
rozhodně	rozhodně	k6eAd1	rozhodně
antisocialistický	antisocialistický	k2eAgInSc1d1	antisocialistický
program	program	k1gInSc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Fašisté	fašista	k1gMnPc1	fašista
krom	krom	k7c2	krom
fyzického	fyzický	k2eAgNnSc2d1	fyzické
násilí	násilí	k1gNnSc2	násilí
užívali	užívat	k5eAaImAgMnP	užívat
proti	proti	k7c3	proti
opozici	opozice	k1gFnSc3	opozice
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
ricinový	ricinový	k2eAgInSc1d1	ricinový
olej	olej	k1gInSc1	olej
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
dávkách	dávka	k1gFnPc6	dávka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
donutili	donutit	k5eAaPmAgMnP	donutit
své	svůj	k3xOyFgFnPc4	svůj
oběti	oběť	k1gFnPc4	oběť
vypít	vypít	k5eAaPmF	vypít
a	a	k8xC	a
pokud	pokud	k8xS	pokud
chtěli	chtít	k5eAaImAgMnP	chtít
mít	mít	k5eAaImF	mít
jistotu	jistota	k1gFnSc4	jistota
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepřežije	přežít	k5eNaPmIp3nS	přežít
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jej	on	k3xPp3gMnSc4	on
"	"	kIx"	"
<g/>
ochutili	ochutit	k5eAaPmAgMnP	ochutit
<g/>
"	"	kIx"	"
benzínem	benzín	k1gInSc7	benzín
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgInPc4d1	oblíbený
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
fašistů	fašista	k1gMnPc2	fašista
i	i	k8xC	i
stávkokazectví	stávkokazectví	k1gNnSc2	stávkokazectví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jim	on	k3xPp3gMnPc3	on
přineslo	přinést	k5eAaPmAgNnS	přinést
uznání	uznání	k1gNnSc1	uznání
mnoha	mnoho	k4c2	mnoho
Italů	Ital	k1gMnPc2	Ital
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Popádí	Popádí	k1gNnSc6	Popádí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
fašisté	fašista	k1gMnPc1	fašista
podporu	podpora	k1gFnSc4	podpora
velkých	velký	k2eAgMnPc2d1	velký
i	i	k8xC	i
středních	střední	k2eAgMnPc2d1	střední
vlastníků	vlastník	k1gMnPc2	vlastník
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
oblastním	oblastní	k2eAgMnPc3d1	oblastní
velitelům	velitel	k1gMnPc3	velitel
úderek	úderek	k1gInSc4	úderek
dařilo	dařit	k5eAaImAgNnS	dařit
ovládnout	ovládnout	k5eAaPmF	ovládnout
města	město	k1gNnSc2	město
jako	jako	k8xS	jako
Ferrara	Ferrar	k1gMnSc4	Ferrar
<g/>
,	,	kIx,	,
Cremona	Cremon	k1gMnSc4	Cremon
<g/>
,	,	kIx,	,
Piacenza	Piacenz	k1gMnSc4	Piacenz
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
padla	padnout	k5eAaImAgFnS	padnout
i	i	k9	i
významná	významný	k2eAgFnSc1d1	významná
bašta	bašta	k1gFnSc1	bašta
levice	levice	k1gFnSc2	levice
Bologna	Bologna	k1gFnSc1	Bologna
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
začátkem	začátkem	k7c2	začátkem
října	říjen	k1gInSc2	říjen
1922	[number]	k4	1922
provedli	provést	k5eAaPmAgMnP	provést
fašisté	fašista	k1gMnPc1	fašista
výpravy	výprava	k1gFnSc2	výprava
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
Neapole	Neapol	k1gFnSc2	Neapol
a	a	k8xC	a
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Apulii	Apulie	k1gFnSc4	Apulie
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgMnPc1d1	další
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
je	být	k5eAaImIp3nS	být
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1922	[number]	k4	1922
zorganizoval	zorganizovat	k5eAaPmAgInS	zorganizovat
Pochod	pochod	k1gInSc1	pochod
na	na	k7c4	na
Řím	Řím	k1gInSc4	Řím
a	a	k8xC	a
vyhrožoval	vyhrožovat	k5eAaImAgMnS	vyhrožovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
svrhne	svrhnout	k5eAaPmIp3nS	svrhnout
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
vynutil	vynutit	k5eAaPmAgMnS	vynutit
své	svůj	k3xOyFgMnPc4	svůj
jmenování	jmenování	k1gNnSc6	jmenování
předsedou	předseda	k1gMnSc7	předseda
italské	italský	k2eAgFnSc2d1	italská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
premiér	premiér	k1gMnSc1	premiér
pak	pak	k6eAd1	pak
zahájil	zahájit	k5eAaPmAgMnS	zahájit
velké	velký	k2eAgFnPc4d1	velká
reformy	reforma	k1gFnPc4	reforma
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgFnPc2	jenž
neustále	neustále	k6eAd1	neustále
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
své	svůj	k3xOyFgFnPc4	svůj
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
vládl	vládnout	k5eAaImAgInS	vládnout
již	již	k6eAd1	již
jako	jako	k9	jako
diktátor	diktátor	k1gMnSc1	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
začal	začít	k5eAaPmAgMnS	začít
používat	používat	k5eAaImF	používat
titul	titul	k1gInSc4	titul
capo	capa	k1gFnSc5	capa
del	del	k?	del
governo	governa	k1gFnSc5	governa
e	e	k0	e
duce	duc	k1gMnPc4	duc
del	del	k?	del
fascismo	fascismo	k6eAd1	fascismo
(	(	kIx(	(
<g/>
šéf	šéf	k1gMnSc1	šéf
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
vůdce	vůdce	k1gMnSc2	vůdce
fašismu	fašismus	k1gInSc2	fašismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1924	[number]	k4	1924
získala	získat	k5eAaPmAgFnS	získat
společná	společný	k2eAgFnSc1d1	společná
kandidátka	kandidátka	k1gFnSc1	kandidátka
fašistů	fašista	k1gMnPc2	fašista
<g/>
,	,	kIx,	,
nacionalistů	nacionalista	k1gMnPc2	nacionalista
a	a	k8xC	a
liberálů	liberál	k1gMnPc2	liberál
64	[number]	k4	64
%	%	kIx~	%
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
dolní	dolní	k2eAgFnSc4d1	dolní
komoru	komora	k1gFnSc4	komora
italského	italský	k2eAgInSc2d1	italský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Squadristé	Squadrista	k1gMnPc1	Squadrista
byli	být	k5eAaImAgMnP	být
reorganizováni	reorganizovat	k5eAaBmNgMnP	reorganizovat
do	do	k7c2	do
Milizia	Milizium	k1gNnSc2	Milizium
Volontaria	Volontarium	k1gNnSc2	Volontarium
per	pero	k1gNnPc2	pero
la	la	k1gNnSc7	la
Sicurezza	Sicurezza	k1gFnSc1	Sicurezza
Nazionale	Nazionale	k1gMnSc1	Nazionale
(	(	kIx(	(
<g/>
MVSN	MVSN	kA	MVSN
–	–	k?	–
milice	milice	k1gFnSc1	milice
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
pro	pro	k7c4	pro
státní	státní	k2eAgFnSc4d1	státní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
poslanec	poslanec	k1gMnSc1	poslanec
za	za	k7c2	za
socialisty	socialista	k1gMnSc2	socialista
Giacomo	Giacoma	k1gFnSc5	Giacoma
Matteotti	Matteotti	k1gNnPc1	Matteotti
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
napadl	napadnout	k5eAaPmAgMnS	napadnout
regulérnost	regulérnost	k1gFnSc4	regulérnost
voleb	volba	k1gFnPc2	volba
poznamenané	poznamenaný	k2eAgFnPc4d1	poznamenaná
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
provinciích	provincie	k1gFnPc6	provincie
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Veneto	Veneto	k1gNnSc1	Veneto
<g/>
,	,	kIx,	,
Piemont	Piemont	k1gInSc1	Piemont
či	či	k8xC	či
Ligurie	Ligurie	k1gFnSc1	Ligurie
násilím	násilí	k1gNnSc7	násilí
vůči	vůči	k7c3	vůči
opozici	opozice	k1gFnSc3	opozice
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1924	[number]	k4	1924
unesen	unesen	k2eAgInSc1d1	unesen
fašistickým	fašistický	k2eAgNnSc7d1	fašistické
komandem	komando	k1gNnSc7	komando
a	a	k8xC	a
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ho	on	k3xPp3gMnSc4	on
unesli	unést	k5eAaPmAgMnP	unést
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
členy	člen	k1gInPc4	člen
zvlášť	zvlášť	k6eAd1	zvlášť
násilnického	násilnický	k2eAgInSc2d1	násilnický
oddílu	oddíl	k1gInSc2	oddíl
(	(	kIx(	(
<g/>
squadry	squadr	k1gMnPc7	squadr
<g/>
)	)	kIx)	)
přezdívaného	přezdívaný	k2eAgNnSc2d1	přezdívané
Čeka	čeka	k1gFnSc1	čeka
(	(	kIx(	(
<g/>
Ceka	Ceka	k1gFnSc1	Ceka
<g/>
)	)	kIx)	)
po	po	k7c6	po
Leninově	Leninův	k2eAgFnSc6d1	Leninova
tajné	tajný	k2eAgFnSc6d1	tajná
polici	police	k1gFnSc6	police
<g/>
,	,	kIx,	,
řetěz	řetěz	k1gInSc4	řetěz
velení	velení	k1gNnSc2	velení
této	tento	k3xDgFnSc2	tento
čeky	čeka	k1gFnSc2	čeka
vedl	vést	k5eAaImAgInS	vést
až	až	k9	až
do	do	k7c2	do
nejbližšího	blízký	k2eAgNnSc2d3	nejbližší
okolí	okolí	k1gNnSc2	okolí
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
citelně	citelně	k6eAd1	citelně
zachvěla	zachvět	k5eAaPmAgFnS	zachvět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
jej	on	k3xPp3gMnSc4	on
podpořili	podpořit	k5eAaPmAgMnP	podpořit
lídři	lídr	k1gMnPc1	lídr
liberální	liberální	k2eAgFnSc2d1	liberální
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
samotný	samotný	k2eAgMnSc1d1	samotný
italský	italský	k2eAgMnSc1d1	italský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
vyhovět	vyhovět	k5eAaPmF	vyhovět
tlaku	tlak	k1gInSc3	tlak
radikálního	radikální	k2eAgNnSc2d1	radikální
křídla	křídlo	k1gNnSc2	křídlo
ve	v	k7c6	v
fašistické	fašistický	k2eAgFnSc6d1	fašistická
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1925	[number]	k4	1925
převzal	převzít	k5eAaPmAgMnS	převzít
"	"	kIx"	"
<g/>
mravní	mravní	k2eAgFnSc4d1	mravní
<g/>
,	,	kIx,	,
politickou	politický	k2eAgFnSc4d1	politická
a	a	k8xC	a
historickou	historický	k2eAgFnSc4d1	historická
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
vše	všechen	k3xTgNnSc4	všechen
co	co	k9	co
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Opozice	opozice	k1gFnSc1	opozice
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
zklamala	zklamat	k5eAaPmAgFnS	zklamat
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
Giacoma	Giacom	k1gMnSc4	Giacom
Matteottiho	Matteotti	k1gMnSc4	Matteotti
jejich	jejich	k3xOp3gFnSc2	jejich
pasivní	pasivní	k2eAgFnSc2d1	pasivní
rezistence	rezistence	k1gFnSc2	rezistence
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
(	(	kIx(	(
<g/>
Aventinská	Aventinský	k2eAgFnSc1d1	Aventinská
secese	secese	k1gFnSc1	secese
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokusila	pokusit	k5eAaPmAgFnS	pokusit
zastavit	zastavit	k5eAaPmF	zastavit
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
na	na	k7c4	na
budoucího	budoucí	k2eAgMnSc4d1	budoucí
diktátora	diktátor	k1gMnSc4	diktátor
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zaútočit	zaútočit	k5eAaPmF	zaútočit
člen	člen	k1gMnSc1	člen
Socialistické	socialistický	k2eAgFnSc2d1	socialistická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
Svobodný	svobodný	k2eAgMnSc1d1	svobodný
zednář	zednář	k1gMnSc1	zednář
Tito	tento	k3xDgMnPc1	tento
Zaniboni	Zanibon	k1gMnPc1	Zanibon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
zastřelit	zastřelit	k5eAaPmF	zastřelit
Il	Il	k1gFnSc4	Il
duceho	duceze	k6eAd1	duceze
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
projevu	projev	k1gInSc6	projev
na	na	k7c6	na
balkonu	balkon	k1gInSc6	balkon
Palazzo	Palazza	k1gFnSc5	Palazza
Chigi	Chigi	k1gNnSc4	Chigi
odstřelovací	odstřelovací	k2eAgFnSc7d1	odstřelovací
puškou	puška	k1gFnSc7	puška
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
zatčen	zatknout	k5eAaPmNgInS	zatknout
3	[number]	k4	3
hodiny	hodina	k1gFnPc4	hodina
před	před	k7c7	před
útokem	útok	k1gInSc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
další	další	k2eAgInSc1d1	další
útok	útok	k1gInSc1	útok
vedený	vedený	k2eAgInSc1d1	vedený
Violet	Violeta	k1gFnPc2	Violeta
Gibsonovou	Gibsonová	k1gFnSc4	Gibsonová
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1926	[number]	k4	1926
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
nezdařil	zdařit	k5eNaPmAgMnS	zdařit
<g/>
.	.	kIx.	.
</s>
<s>
Škrábla	škrábnout	k5eAaPmAgFnS	škrábnout
ho	on	k3xPp3gNnSc4	on
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
kulka	kulka	k1gFnSc1	kulka
na	na	k7c6	na
nose	nos	k1gInSc6	nos
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jej	on	k3xPp3gMnSc4	on
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
bonmotu	bonmot	k1gInSc3	bonmot
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
kulky	kulka	k1gFnPc1	kulka
mizí	mizet	k5eAaImIp3nP	mizet
a	a	k8xC	a
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
útok	útok	k1gInSc1	útok
nastal	nastat	k5eAaPmAgInS	nastat
u	u	k7c2	u
Porta	porto	k1gNnSc2	porto
Pia	Pius	k1gMnSc2	Pius
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Carrarský	Carrarský	k2eAgMnSc1d1	Carrarský
anarchista	anarchista	k1gMnSc1	anarchista
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
levicových	levicový	k2eAgMnPc2d1	levicový
Arditi	Ardit	k2eAgMnPc1d1	Ardit
del	del	k?	del
Popolo	Popola	k1gFnSc5	Popola
Gino	Gino	k1gMnSc1	Gino
Lucetti	Lucetti	k1gNnPc2	Lucetti
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
diktátorův	diktátorův	k2eAgInSc4d1	diktátorův
vůz	vůz	k1gInSc4	vůz
ručním	ruční	k2eAgInSc7d1	ruční
granátem	granát	k1gInSc7	granát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
zranil	zranit	k5eAaPmAgMnS	zranit
8	[number]	k4	8
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
vyvázl	vyváznout	k5eAaPmAgMnS	vyváznout
nezraněn	zranit	k5eNaPmNgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Sérii	série	k1gFnSc4	série
neúspěšných	úspěšný	k2eNgMnPc2d1	neúspěšný
útoku	útok	k1gInSc2	útok
završil	završit	k5eAaPmAgMnS	završit
patnáctiletý	patnáctiletý	k2eAgMnSc1d1	patnáctiletý
anarchista	anarchista	k1gMnSc1	anarchista
Anteo	Anteo	k1gMnSc1	Anteo
Zamboni	Zambon	k1gMnPc1	Zambon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
na	na	k7c4	na
diktátora	diktátor	k1gMnSc4	diktátor
zahájil	zahájit	k5eAaPmAgMnS	zahájit
palbu	palba	k1gFnSc4	palba
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1926	[number]	k4	1926
na	na	k7c6	na
přehlídce	přehlídka	k1gFnSc6	přehlídka
fašistické	fašistický	k2eAgFnSc2d1	fašistická
milice	milice	k1gFnSc2	milice
v	v	k7c6	v
Boloni	Boloňa	k1gFnSc6	Boloňa
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
byl	být	k5eAaImAgMnS	být
vzápětí	vzápětí	k6eAd1	vzápětí
černokošiláky	černokošilák	k1gMnPc4	černokošilák
zlynčován	zlynčován	k2eAgMnSc1d1	zlynčován
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
využil	využít	k5eAaPmAgMnS	využít
těchto	tento	k3xDgInPc2	tento
útoků	útok	k1gInPc2	útok
dokonale	dokonale	k6eAd1	dokonale
<g/>
.	.	kIx.	.
</s>
<s>
Zorganizoval	zorganizovat	k5eAaPmAgInS	zorganizovat
tajnou	tajný	k2eAgFnSc4d1	tajná
politickou	politický	k2eAgFnSc4d1	politická
policii	policie	k1gFnSc4	policie
(	(	kIx(	(
<g/>
OVRA	OVRA	kA	OVRA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
speciální	speciální	k2eAgInPc4d1	speciální
tribunály	tribunál	k1gInPc4	tribunál
pro	pro	k7c4	pro
bezpečnosti	bezpečnost	k1gFnPc4	bezpečnost
státu	stát	k1gInSc2	stát
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zlikvidoval	zlikvidovat	k5eAaPmAgMnS	zlikvidovat
zákonnost	zákonnost	k1gFnSc1	zákonnost
zavedením	zavedení	k1gNnSc7	zavedení
Leggi	Legg	k1gFnSc2	Legg
Fascistissime	Fascistissim	k1gInSc5	Fascistissim
–	–	k?	–
fašistického	fašistický	k2eAgNnSc2d1	fašistické
zákonodárství	zákonodárství	k1gNnSc2	zákonodárství
<g/>
.	.	kIx.	.
</s>
<s>
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
fašizovat	fašizovat	k5eAaImF	fašizovat
společnost	společnost	k1gFnSc4	společnost
a	a	k8xC	a
často	často	k6eAd1	často
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
"	"	kIx"	"
<g/>
nového	nový	k2eAgMnSc2d1	nový
Itala	Ital	k1gMnSc2	Ital
<g/>
"	"	kIx"	"
–	–	k?	–
tj.	tj.	kA	tj.
nového	nový	k2eAgMnSc4d1	nový
člověka	člověk	k1gMnSc4	člověk
socialismu	socialismus	k1gInSc2	socialismus
s	s	k7c7	s
nacionalistickou	nacionalistický	k2eAgFnSc7d1	nacionalistická
tváří	tvář	k1gFnSc7	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
socialistických	socialistický	k2eAgInPc2d1	socialistický
režimů	režim	k1gInPc2	režim
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
k	k	k7c3	k
zákazu	zákaz	k1gInSc3	zákaz
stávek	stávka	k1gFnPc2	stávka
veřejných	veřejný	k2eAgMnPc2d1	veřejný
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
stávek	stávka	k1gFnPc2	stávka
s	s	k7c7	s
politickými	politický	k2eAgInPc7d1	politický
cíli	cíl	k1gInPc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Fašistické	fašistický	k2eAgInPc1d1	fašistický
odborové	odborový	k2eAgInPc1d1	odborový
svazy	svaz	k1gInPc1	svaz
byly	být	k5eAaImAgInP	být
prohlášeny	prohlásit	k5eAaPmNgInP	prohlásit
za	za	k7c4	za
jediné	jediné	k1gNnSc4	jediné
legální	legální	k2eAgNnSc4d1	legální
zástupce	zástupce	k1gMnSc2	zástupce
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Komsomolu	Komsomol	k1gInSc2	Komsomol
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
mládežnická	mládežnický	k2eAgFnSc1d1	mládežnická
organizace	organizace	k1gFnSc1	organizace
s	s	k7c7	s
povinným	povinný	k2eAgNnSc7d1	povinné
členstvím	členství	k1gNnSc7	členství
Opera	opera	k1gFnSc1	opera
Nazionale	Nazionale	k1gMnSc4	Nazionale
Balilla	Balill	k1gMnSc4	Balill
<g/>
.	.	kIx.	.
</s>
<s>
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
moto	moto	k1gNnSc4	moto
<g/>
:	:	kIx,	:
všechno	všechen	k3xTgNnSc1	všechen
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
;	;	kIx,	;
nic	nic	k3yNnSc1	nic
mimo	mimo	k7c4	mimo
stát	stát	k1gInSc4	stát
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgInS	zavést
termín	termín	k1gInSc4	termín
totalitní	totalitní	k2eAgInSc1d1	totalitní
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
parlamentu	parlament	k1gInSc2	parlament
převzal	převzít	k5eAaPmAgInS	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
nový	nový	k2eAgInSc1d1	nový
orgán	orgán	k1gInSc1	orgán
–	–	k?	–
Velká	velký	k2eAgFnSc1d1	velká
fašistická	fašistický	k2eAgFnSc1d1	fašistická
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
10	[number]	k4	10
000	[number]	k4	000
opozičníků	opozičník	k1gMnPc2	opozičník
bylo	být	k5eAaImAgNnS	být
uvězněno	uvěznit	k5eAaPmNgNnS	uvěznit
v	v	k7c6	v
internačních	internační	k2eAgInPc6d1	internační
táborech	tábor	k1gInPc6	tábor
na	na	k7c6	na
Ponsetijských	Ponsetijský	k2eAgInPc6d1	Ponsetijský
<g/>
,	,	kIx,	,
Liparských	Liparský	k2eAgInPc6d1	Liparský
a	a	k8xC	a
Tremitských	Tremitský	k2eAgInPc6d1	Tremitský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
zavedl	zavést	k5eAaPmAgInS	zavést
jistou	jistý	k2eAgFnSc4d1	jistá
"	"	kIx"	"
<g/>
normalizaci	normalizace	k1gFnSc4	normalizace
<g/>
"	"	kIx"	"
svého	své	k1gNnSc2	své
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
během	během	k7c2	během
let	léto	k1gNnPc2	léto
1927-1940	[number]	k4	1927-1940
dal	dát	k5eAaPmAgMnS	dát
popravit	popravit	k5eAaPmF	popravit
za	za	k7c4	za
antifašistickou	antifašistický	k2eAgFnSc4d1	Antifašistická
činnost	činnost	k1gFnSc4	činnost
jen	jen	k9	jen
9	[number]	k4	9
oponentů	oponent	k1gMnPc2	oponent
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jiného	jiný	k2eAgInSc2d1	jiný
zdroje	zdroj	k1gInSc2	zdroj
bylo	být	k5eAaImAgNnS	být
popraveno	popravit	k5eAaPmNgNnS	popravit
a	a	k8xC	a
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
parlamentu	parlament	k1gInSc2	parlament
vypuzeni	vypuzen	k2eAgMnPc1d1	vypuzen
socialisté	socialist	k1gMnPc1	socialist
<g/>
,	,	kIx,	,
komunisté	komunista	k1gMnPc1	komunista
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
levicoví	levicový	k2eAgMnPc1d1	levicový
poslanci	poslanec	k1gMnPc1	poslanec
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
strany	strana	k1gFnPc1	strana
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
zakázány	zakázán	k2eAgInPc1d1	zakázán
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
a	a	k8xC	a
dřívější	dřívější	k2eAgMnPc1d1	dřívější
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
(	(	kIx(	(
<g/>
ze	z	k7c2	z
socialistického	socialistický	k2eAgNnSc2d1	socialistické
období	období	k1gNnSc2	období
<g/>
)	)	kIx)	)
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
Antonio	Antonio	k1gMnSc1	Antonio
Gramsci	Gramsek	k1gMnPc1	Gramsek
a	a	k8xC	a
Amadeo	Amadeo	k6eAd1	Amadeo
Bordiga	Bordig	k1gMnSc4	Bordig
byli	být	k5eAaImAgMnP	být
uvězněni	uvězněn	k2eAgMnPc1d1	uvězněn
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
fašistická	fašistický	k2eAgFnSc1d1	fašistická
Itálie	Itálie	k1gFnSc1	Itálie
i	i	k9	i
přes	přes	k7c4	přes
velkou	velký	k2eAgFnSc4d1	velká
represi	represe	k1gFnSc4	represe
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
národně	národně	k6eAd1	národně
socialistickými	socialistický	k2eAgInPc7d1	socialistický
a	a	k8xC	a
komunistickými	komunistický	k2eAgInPc7d1	komunistický
režimy	režim	k1gInPc7	režim
nejevila	jevit	k5eNaImAgFnS	jevit
jako	jako	k9	jako
totalitní	totalitní	k2eAgFnSc1d1	totalitní
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vojenské	vojenský	k2eAgFnSc3d1	vojenská
a	a	k8xC	a
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
slabosti	slabost	k1gFnSc3	slabost
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
nutnosti	nutnost	k1gFnSc2	nutnost
upevnit	upevnit	k5eAaPmF	upevnit
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
duce	duce	k1gFnSc4	duce
vzdor	vzdor	k7c3	vzdor
jeho	jeho	k3xOp3gFnPc3	jeho
snahám	snaha	k1gFnPc3	snaha
o	o	k7c6	o
obnovení	obnovení	k1gNnSc6	obnovení
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
ze	z	k7c2	z
Středomoří	středomoří	k1gNnSc2	středomoří
učinila	učinit	k5eAaImAgFnS	učinit
Mare	Mare	k1gFnSc1	Mare
Nostrum	Nostrum	k1gNnSc4	Nostrum
(	(	kIx(	(
<g/>
Naše	náš	k3xOp1gNnSc1	náš
Moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
zpočátku	zpočátku	k6eAd1	zpočátku
pomýšlet	pomýšlet	k5eAaImF	pomýšlet
na	na	k7c4	na
expanzivní	expanzivní	k2eAgFnSc4d1	expanzivní
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
násilný	násilný	k2eAgInSc1d1	násilný
debut	debut	k1gInSc1	debut
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
okupace	okupace	k1gFnSc2	okupace
řeckého	řecký	k2eAgInSc2d1	řecký
ostrova	ostrov	k1gInSc2	ostrov
Korfu	Korfu	k1gNnSc1	Korfu
v	v	k7c4	v
odvetu	odveta	k1gFnSc4	odveta
za	za	k7c4	za
zabití	zabití	k1gNnSc4	zabití
italského	italský	k2eAgMnSc2d1	italský
generála	generál	k1gMnSc2	generál
Enrica	Enricus	k1gMnSc2	Enricus
Telliniho	Tellini	k1gMnSc2	Tellini
na	na	k7c6	na
řecko-albánské	řeckolbánský	k2eAgFnSc6d1	řecko-albánský
hranici	hranice	k1gFnSc6	hranice
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1923	[number]	k4	1923
byl	být	k5eAaImAgMnS	být
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
a	a	k8xC	a
Anglie	Anglie	k1gFnSc1	Anglie
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
jej	on	k3xPp3gInSc2	on
nakonec	nakonec	k6eAd1	nakonec
donutila	donutit	k5eAaPmAgNnP	donutit
po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
ostrov	ostrov	k1gInSc1	ostrov
opět	opět	k6eAd1	opět
vyklidit	vyklidit	k5eAaPmF	vyklidit
<g/>
.	.	kIx.	.
</s>
<s>
Diplomatické	diplomatický	k2eAgNnSc4d1	diplomatické
snažení	snažení	k1gNnSc4	snažení
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
přineslo	přinést	k5eAaPmAgNnS	přinést
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1924	[number]	k4	1924
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
dohody	dohoda	k1gFnPc4	dohoda
se	s	k7c7	s
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Slovinců	Slovinec	k1gMnPc2	Slovinec
a	a	k8xC	a
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
Itálii	Itálie	k1gFnSc3	Itálie
město	město	k1gNnSc1	město
Fiume	Fium	k1gInSc5	Fium
(	(	kIx(	(
<g/>
Rijeka	Rijeka	k1gFnSc1	Rijeka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1925	[number]	k4	1925
odstoupili	odstoupit	k5eAaPmAgMnP	odstoupit
Britové	Brit	k1gMnPc1	Brit
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
Italům	Ital	k1gMnPc3	Ital
oblast	oblast	k1gFnSc4	oblast
Jubalandu	Jubalanda	k1gFnSc4	Jubalanda
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Oltre	Oltr	k1gInSc5	Oltr
Giuba	Giuba	k1gMnSc1	Giuba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
italskému	italský	k2eAgNnSc3d1	italské
Somálsku	Somálsko	k1gNnSc3	Somálsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1927	[number]	k4	1927
Italové	Ital	k1gMnPc1	Ital
definitivně	definitivně	k6eAd1	definitivně
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Ahmeta	Ahmet	k1gMnSc2	Ahmet
Zoga	Zogus	k1gMnSc2	Zogus
Albánii	Albánie	k1gFnSc4	Albánie
podpisem	podpis	k1gInSc7	podpis
druhého	druhý	k4xOgInSc2	druhý
tiranského	tiranský	k2eAgInSc2d1	tiranský
paktu	pakt	k1gInSc2	pakt
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
umožnil	umožnit	k5eAaPmAgMnS	umožnit
Římu	Řím	k1gInSc3	Řím
ovládnout	ovládnout	k5eAaPmF	ovládnout
ropná	ropný	k2eAgNnPc1d1	ropné
pole	pole	k1gNnPc1	pole
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
a	a	k8xC	a
italské	italský	k2eAgFnSc6d1	italská
vojenské	vojenský	k2eAgFnSc6d1	vojenská
misi	mise	k1gFnSc6	mise
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
albánskou	albánský	k2eAgFnSc7d1	albánská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Albánie	Albánie	k1gFnSc1	Albánie
se	se	k3xPyFc4	se
tak	tak	k9	tak
de	de	k?	de
facto	facto	k1gNnSc1	facto
stala	stát	k5eAaPmAgFnS	stát
italským	italský	k2eAgInSc7d1	italský
protektorátem	protektorát	k1gInSc7	protektorát
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Ahmet	Ahmet	k1gInSc4	Ahmet
Zogu	Zogus	k1gInSc2	Zogus
albánským	albánský	k2eAgMnSc7d1	albánský
králem	král	k1gMnSc7	král
Zogem	Zog	k1gMnSc7	Zog
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Italská	italský	k2eAgFnSc1d1	italská
diplomacie	diplomacie	k1gFnSc1	diplomacie
podporovala	podporovat	k5eAaImAgFnS	podporovat
územní	územní	k2eAgInPc4d1	územní
požadavky	požadavek	k1gInPc4	požadavek
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
získala	získat	k5eAaPmAgFnS	získat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kancléře	kancléř	k1gMnSc2	kancléř
Engelberta	Engelbert	k1gMnSc2	Engelbert
Dollfusse	Dollfuss	k1gMnSc2	Dollfuss
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gInSc1	on
sám	sám	k3xTgInSc1	sám
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
získat	získat	k5eAaPmF	získat
další	další	k2eAgInSc4d1	další
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
podporoval	podporovat	k5eAaImAgInS	podporovat
chorvatské	chorvatský	k2eAgMnPc4d1	chorvatský
a	a	k8xC	a
makedonské	makedonský	k2eAgMnPc4d1	makedonský
separatisty	separatista	k1gMnPc4	separatista
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
uvítal	uvítat	k5eAaPmAgInS	uvítat
i	i	k9	i
nástup	nástup	k1gInSc1	nástup
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
doba	doba	k1gFnSc1	doba
nepřála	přát	k5eNaImAgFnS	přát
agresi	agrese	k1gFnSc4	agrese
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgMnS	zahájit
agresi	agrese	k1gFnSc4	agrese
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
a	a	k8xC	a
v	v	k7c6	v
italských	italský	k2eAgFnPc6d1	italská
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Sicílie	Sicílie	k1gFnSc1	Sicílie
byla	být	k5eAaImAgFnS	být
odnepaměti	odnepaměti	k6eAd1	odnepaměti
v	v	k7c6	v
područí	područí	k1gNnSc6	područí
Mafie	mafie	k1gFnSc2	mafie
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
změnit	změnit	k5eAaPmF	změnit
ani	ani	k8xC	ani
vláda	vláda	k1gFnSc1	vláda
Neapolského	neapolský	k2eAgNnSc2d1	Neapolské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
Království	království	k1gNnSc1	království
obojí	oboj	k1gFnPc2	oboj
Sicílie	Sicílie	k1gFnSc2	Sicílie
nebo	nebo	k8xC	nebo
sjednoceného	sjednocený	k2eAgNnSc2d1	sjednocené
Italského	italský	k2eAgNnSc2d1	italské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
místní	místní	k2eAgNnSc1d1	místní
vedení	vedení	k1gNnSc1	vedení
fašistické	fašistický	k2eAgFnSc2d1	fašistická
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
snažilo	snažit	k5eAaImAgNnS	snažit
žít	žít	k5eAaImF	žít
s	s	k7c7	s
Mafií	mafie	k1gFnSc7	mafie
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
symbióze	symbióza	k1gFnSc6	symbióza
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
střetu	střet	k1gInSc3	střet
fašismu	fašismus	k1gInSc2	fašismus
a	a	k8xC	a
Mafie	mafie	k1gFnSc2	mafie
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
došlo	dojít	k5eAaPmAgNnS	dojít
během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Palermu	Palermo	k1gNnSc6	Palermo
se	se	k3xPyFc4	se
ducemu	ducemat	k5eAaPmIp1nS	ducemat
ještě	ještě	k6eAd1	ještě
dostalo	dostat	k5eAaPmAgNnS	dostat
nadšeného	nadšený	k2eAgNnSc2d1	nadšené
přijetí	přijetí	k1gNnSc2	přijetí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
starosta	starosta	k1gMnSc1	starosta
městečka	městečko	k1gNnSc2	městečko
Piana	piano	k1gNnSc2	piano
dei	dei	k?	dei
Greci	Greec	k1gInPc7	Greec
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
místní	místní	k2eAgMnSc1d1	místní
šéf	šéf	k1gMnSc1	šéf
mafie	mafie	k1gFnSc2	mafie
Francesco	Francesco	k1gMnSc1	Francesco
Cuccia	Cuccia	k1gFnSc1	Cuccia
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
don	don	k1gMnSc1	don
Ciccio	Ciccio	k1gMnSc1	Ciccio
<g/>
,	,	kIx,	,
zajistil	zajistit	k5eAaPmAgMnS	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gInSc4	jeho
proslov	proslov	k1gInSc4	proslov
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bojkotovalo	bojkotovat	k5eAaImAgNnS	bojkotovat
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
volnou	volný	k2eAgFnSc4d1	volná
ruku	ruka	k1gFnSc4	ruka
někdejšímu	někdejší	k2eAgInSc3d1	někdejší
boloňskému	boloňský	k2eAgInSc3d1	boloňský
prefektovi	prefekt	k1gMnSc3	prefekt
Cesare	Cesar	k1gMnSc5	Cesar
Morimu	Morim	k1gMnSc3	Morim
k	k	k7c3	k
"	"	kIx"	"
<g/>
prosazení	prosazení	k1gNnSc3	prosazení
autority	autorita	k1gFnSc2	autorita
státu	stát	k1gInSc2	stát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
zahájil	zahájit	k5eAaPmAgMnS	zahájit
bezohledné	bezohledný	k2eAgNnSc4d1	bezohledné
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
mafiánským	mafiánský	k2eAgFnPc3d1	mafiánská
strukturám	struktura	k1gFnPc3	struktura
na	na	k7c4	na
Sicílii	Sicílie	k1gFnSc4	Sicílie
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
přezdívku	přezdívka	k1gFnSc4	přezdívka
Železný	Železný	k1gMnSc1	Železný
prefekt	prefekt	k1gMnSc1	prefekt
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
mafiánských	mafiánský	k2eAgMnPc2d1	mafiánský
bossů	boss	k1gMnPc2	boss
byla	být	k5eAaImAgFnS	být
uvězněna	uvěznit	k5eAaPmNgFnS	uvěznit
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
prchla	prchnout	k5eAaPmAgFnS	prchnout
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
nebo	nebo	k8xC	nebo
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
dokonce	dokonce	k9	dokonce
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
fašistické	fašistický	k2eAgFnSc2d1	fašistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nelítostného	lítostný	k2eNgMnSc4d1	nelítostný
Moriho	Mori	k1gMnSc4	Mori
zastavil	zastavit	k5eAaPmAgMnS	zastavit
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
až	až	k8xS	až
diktátor	diktátor	k1gMnSc1	diktátor
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ocenil	ocenit	k5eAaPmAgMnS	ocenit
jeho	jeho	k3xOp3gFnPc4	jeho
zásluhy	zásluha	k1gFnPc4	zásluha
a	a	k8xC	a
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
jej	on	k3xPp3gMnSc4	on
senátorem	senátor	k1gMnSc7	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
Moriho	Mori	k1gMnSc4	Mori
odchodu	odchod	k1gInSc2	odchod
teror	teror	k1gInSc1	teror
prakticky	prakticky	k6eAd1	prakticky
ustal	ustat	k5eAaPmAgInS	ustat
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
halasně	halasně	k6eAd1	halasně
vykřikoval	vykřikovat	k5eAaImAgMnS	vykřikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Mafie	mafie	k1gFnSc1	mafie
už	už	k6eAd1	už
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
organizaci	organizace	k1gFnSc6	organizace
zčásti	zčásti	k6eAd1	zčásti
přesunula	přesunout	k5eAaPmAgFnS	přesunout
mimo	mimo	k7c4	mimo
Sicílii	Sicílie	k1gFnSc4	Sicílie
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
přešla	přejít	k5eAaPmAgFnS	přejít
do	do	k7c2	do
ilegality	ilegalita	k1gFnSc2	ilegalita
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
až	až	k6eAd1	až
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
fašistického	fašistický	k2eAgInSc2d1	fašistický
režimu	režim	k1gInSc2	režim
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1925	[number]	k4	1925
zahájil	zahájit	k5eAaPmAgMnS	zahájit
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
italského	italský	k2eAgNnSc2d1	italské
Somálska	Somálsko	k1gNnSc2	Somálsko
Cesare	Cesar	k1gMnSc5	Cesar
de	de	k?	de
Vecchi	Vecch	k1gFnSc2	Vecch
tažení	tažení	k1gNnSc2	tažení
proti	proti	k7c3	proti
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
velmi	velmi	k6eAd1	velmi
samostatnému	samostatný	k2eAgInSc3d1	samostatný
svazu	svaz	k1gInSc3	svaz
proitalských	proitalský	k2eAgInPc2d1	proitalský
sultanátů	sultanát	k1gInPc2	sultanát
a	a	k8xC	a
během	během	k7c2	během
let	léto	k1gNnPc2	léto
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
pomocí	pomocí	k7c2	pomocí
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
ale	ale	k8xC	ale
také	také	k9	také
korupce	korupce	k1gFnSc2	korupce
a	a	k8xC	a
intrik	intrika	k1gFnPc2	intrika
zlikvidoval	zlikvidovat	k5eAaPmAgMnS	zlikvidovat
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
místní	místní	k2eAgFnSc4d1	místní
autonomii	autonomie	k1gFnSc4	autonomie
a	a	k8xC	a
připravil	připravit	k5eAaPmAgMnS	připravit
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
italské	italský	k2eAgMnPc4d1	italský
kolonisty	kolonista	k1gMnPc4	kolonista
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
brutálněji	brutálně	k6eAd2	brutálně
si	se	k3xPyFc3	se
počínal	počínat	k5eAaImAgMnS	počínat
plukovník	plukovník	k1gMnSc1	plukovník
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
generál	generál	k1gMnSc1	generál
Rodolfo	Rodolfo	k6eAd1	Rodolfo
Graziani	Graziaň	k1gFnSc6	Graziaň
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Graziani	Graziaň	k1gFnSc6	Graziaň
brutálně	brutálně	k6eAd1	brutálně
potlačil	potlačit	k5eAaPmAgMnS	potlačit
revoltu	revolta	k1gFnSc4	revolta
Tuaregů	Tuareg	k1gMnPc2	Tuareg
v	v	k7c6	v
protektorátu	protektorát	k1gInSc6	protektorát
Fezzan	Fezzana	k1gFnPc2	Fezzana
a	a	k8xC	a
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
si	se	k3xPyFc3	se
zato	zato	k6eAd1	zato
přiléhavou	přiléhavý	k2eAgFnSc4d1	přiléhavá
přezdívku	přezdívka	k1gFnSc4	přezdívka
Fezzánský	Fezzánský	k2eAgMnSc1d1	Fezzánský
řezník	řezník	k1gMnSc1	řezník
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
svým	svůj	k3xOyFgNnSc7	svůj
trestným	trestný	k2eAgNnSc7d1	trestné
tažením	tažení	k1gNnSc7	tažení
porazil	porazit	k5eAaPmAgInS	porazit
vlivný	vlivný	k2eAgInSc1d1	vlivný
kmen	kmen	k1gInSc1	kmen
Senússiú	Senússiú	k1gFnSc2	Senússiú
v	v	k7c6	v
Tripolsku	Tripolsko	k1gNnSc6	Tripolsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
letech	let	k1gInPc6	let
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
nechvalně	chvalně	k6eNd1	chvalně
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k8xS	jako
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
Kyrenaiky	Kyrenaika	k1gFnSc2	Kyrenaika
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
charismatickému	charismatický	k2eAgMnSc3d1	charismatický
vůdci	vůdce	k1gMnSc3	vůdce
odporu	odpor	k1gInSc2	odpor
Omaru	Omar	k1gMnSc3	Omar
Muktarovi	Muktar	k1gMnSc3	Muktar
nasadil	nasadit	k5eAaPmAgMnS	nasadit
lehké	lehký	k2eAgInPc4d1	lehký
tanky	tank	k1gInPc4	tank
<g/>
,	,	kIx,	,
yperitové	yperitový	k2eAgInPc4d1	yperitový
nálety	nálet	k1gInPc4	nálet
a	a	k8xC	a
internaci	internace	k1gFnSc4	internace
odbojných	odbojný	k2eAgInPc2d1	odbojný
kmenů	kmen	k1gInPc2	kmen
do	do	k7c2	do
sběrných	sběrný	k2eAgInPc2d1	sběrný
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Muktar	Muktar	k1gMnSc1	Muktar
byl	být	k5eAaImAgMnS	být
nakonec	nakonec	k6eAd1	nakonec
zajat	zajmout	k5eAaPmNgMnS	zajmout
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1931	[number]	k4	1931
oběšen	oběsit	k5eAaPmNgMnS	oběsit
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
Saluk	Saluk	k1gInSc1	Saluk
<g/>
.	.	kIx.	.
</s>
<s>
Tripolsko	Tripolsko	k1gNnSc1	Tripolsko
<g/>
,	,	kIx,	,
Fezzán	Fezzán	k2eAgInSc1d1	Fezzán
a	a	k8xC	a
Kyrenaika	Kyrenaika	k1gFnSc1	Kyrenaika
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xC	jako
Libye	Libye	k1gFnSc1	Libye
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1934	[number]	k4	1934
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
na	na	k7c4	na
saharskou	saharský	k2eAgFnSc4d1	saharská
část	část	k1gFnSc4	část
a	a	k8xC	a
přímořskou	přímořský	k2eAgFnSc4d1	přímořská
část	část	k1gFnSc4	část
označovanou	označovaný	k2eAgFnSc4d1	označovaná
jako	jako	k8xC	jako
Italská	italský	k2eAgFnSc1d1	italská
severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
zde	zde	k6eAd1	zde
fašisté	fašista	k1gMnPc1	fašista
přemístili	přemístit	k5eAaPmAgMnP	přemístit
110	[number]	k4	110
000	[number]	k4	000
italských	italský	k2eAgMnPc2d1	italský
kolonistů	kolonista	k1gMnPc2	kolonista
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kampaň	kampaň	k1gFnSc1	kampaň
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
předobrazem	předobraz	k1gInSc7	předobraz
pozdější	pozdní	k2eAgFnSc1d2	pozdější
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Etiopie	Etiopie	k1gFnSc2	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdařil	zdařit	k5eAaPmAgInS	zdařit
jiný	jiný	k2eAgInSc4d1	jiný
kousek	kousek	k1gInSc4	kousek
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1929	[number]	k4	1929
Lateránskou	lateránský	k2eAgFnSc7d1	Lateránská
smlouvou	smlouva	k1gFnSc7	smlouva
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
Piem	Pius	k1gMnSc7	Pius
XI	XI	kA	XI
<g/>
.	.	kIx.	.
získal	získat	k5eAaPmAgInS	získat
podporu	podpora	k1gFnSc4	podpora
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
nedořešené	dořešený	k2eNgInPc1d1	nedořešený
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Svatým	svatý	k2eAgInSc7d1	svatý
stolcem	stolec	k1gInSc7	stolec
a	a	k8xC	a
italským	italský	k2eAgInSc7d1	italský
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
sféře	sféra	k1gFnSc6	sféra
již	již	k6eAd1	již
tak	tak	k6eAd1	tak
zcela	zcela	k6eAd1	zcela
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1922-1926	[number]	k4	1922-1926
zažila	zažít	k5eAaPmAgFnS	zažít
italská	italský	k2eAgFnSc1d1	italská
ekonomika	ekonomika	k1gFnSc1	ekonomika
období	období	k1gNnSc2	období
rychlého	rychlý	k2eAgInSc2d1	rychlý
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
výrobní	výrobní	k2eAgFnSc2d1	výrobní
produkce	produkce	k1gFnSc2	produkce
o	o	k7c4	o
10	[number]	k4	10
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
export	export	k1gInSc1	export
a	a	k8xC	a
úspora	úspora	k1gFnSc1	úspora
veřejných	veřejný	k2eAgInPc2d1	veřejný
výdajů	výdaj	k1gInPc2	výdaj
z	z	k7c2	z
35	[number]	k4	35
%	%	kIx~	%
na	na	k7c4	na
13	[number]	k4	13
%	%	kIx~	%
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
z	z	k7c2	z
600	[number]	k4	600
000	[number]	k4	000
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Alberto	Alberta	k1gFnSc5	Alberta
de	de	k?	de
Stefani	Stefan	k1gMnPc1	Stefan
tak	tak	k6eAd1	tak
už	už	k6eAd1	už
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1925	[number]	k4	1925
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
vyrovnaného	vyrovnaný	k2eAgInSc2d1	vyrovnaný
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Vytvoření	vytvoření	k1gNnSc1	vytvoření
systému	systém	k1gInSc2	systém
korporací	korporace	k1gFnPc2	korporace
však	však	k9	však
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
rezignaci	rezignace	k1gFnSc3	rezignace
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
i	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
hrabě	hrabě	k1gMnSc1	hrabě
de	de	k?	de
Stefani	Stefan	k1gMnPc1	Stefan
přesvědčeným	přesvědčený	k2eAgMnSc7d1	přesvědčený
fašistou	fašista	k1gMnSc7	fašista
<g/>
,	,	kIx,	,
při	při	k7c6	při
výkonu	výkon	k1gInSc6	výkon
své	svůj	k3xOyFgFnSc2	svůj
ministerské	ministerský	k2eAgFnSc2d1	ministerská
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
řídil	řídit	k5eAaImAgMnS	řídit
liberální	liberální	k2eAgFnSc7d1	liberální
zásadou	zásada	k1gFnSc7	zásada
laissez	laisseza	k1gFnPc2	laisseza
faire	fair	k1gMnSc5	fair
<g/>
.	.	kIx.	.
</s>
<s>
Korporativní	korporativní	k2eAgFnSc1d1	korporativní
Itálie	Itálie	k1gFnSc1	Itálie
sice	sice	k8xC	sice
v	v	k7c6	v
letech	let	k1gInPc6	let
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
produkci	produkce	k1gFnSc4	produkce
obilovin	obilovina	k1gFnPc2	obilovina
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zcela	zcela	k6eAd1	zcela
soběstačná	soběstačný	k2eAgFnSc1d1	soběstačná
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
obilí	obilí	k1gNnSc2	obilí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
preference	preference	k1gFnPc4	preference
jednoho	jeden	k4xCgInSc2	jeden
druhu	druh	k1gInSc2	druh
plodin	plodina	k1gFnPc2	plodina
způsobila	způsobit	k5eAaPmAgFnS	způsobit
nedostatek	nedostatek	k1gInSc4	nedostatek
jiných	jiný	k2eAgInPc2d1	jiný
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
porodnost	porodnost	k1gFnSc1	porodnost
na	na	k7c4	na
duceho	duceze	k6eAd1	duceze
rozkaz	rozkaz	k1gInSc4	rozkaz
výrazně	výrazně	k6eAd1	výrazně
nestoupla	stoupnout	k5eNaPmAgFnS	stoupnout
<g/>
.	.	kIx.	.
</s>
<s>
Izolovaný	izolovaný	k2eAgInSc4d1	izolovaný
korporativní	korporativní	k2eAgInSc4d1	korporativní
systém	systém	k1gInSc4	systém
sice	sice	k8xC	sice
Itálii	Itálie	k1gFnSc4	Itálie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
uchránil	uchránit	k5eAaPmAgMnS	uchránit
přímého	přímý	k2eAgInSc2d1	přímý
zásahu	zásah	k1gInSc2	zásah
velké	velký	k2eAgFnSc2d1	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
krach	krach	k1gInSc1	krach
rakouské	rakouský	k2eAgFnSc2d1	rakouská
banky	banka	k1gFnSc2	banka
Credit	Credit	k1gMnSc1	Credit
Anstalt	Anstalt	k1gMnSc1	Anstalt
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1931	[number]	k4	1931
poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
italský	italský	k2eAgInSc1d1	italský
bankovní	bankovní	k2eAgInSc1d1	bankovní
sektor	sektor	k1gInSc1	sektor
a	a	k8xC	a
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
italský	italský	k2eAgInSc4d1	italský
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ocelářský	ocelářský	k2eAgInSc1d1	ocelářský
gigant	gigant	k1gInSc1	gigant
Ansaldo	Ansaldo	k1gNnSc1	Ansaldo
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
musel	muset	k5eAaImAgMnS	muset
státem	stát	k1gInSc7	stát
zřízený	zřízený	k2eAgInSc1d1	zřízený
Institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
průmyslu	průmysl	k1gInSc2	průmysl
(	(	kIx(	(
<g/>
IRI	IRI	kA	IRI
<g/>
)	)	kIx)	)
investovat	investovat	k5eAaBmF	investovat
mnoho	mnoho	k4c4	mnoho
prostředků	prostředek	k1gInPc2	prostředek
do	do	k7c2	do
stabilizace	stabilizace	k1gFnSc2	stabilizace
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgInSc6d1	průmyslový
sektoru	sektor	k1gInSc6	sektor
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1927	[number]	k4	1927
obnovil	obnovit	k5eAaPmAgInS	obnovit
zlatý	zlatý	k2eAgInSc1d1	zlatý
standard	standard	k1gInSc1	standard
i	i	k9	i
přes	přes	k7c4	přes
odpor	odpor	k1gInSc4	odpor
svého	svůj	k3xOyFgMnSc2	svůj
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
italská	italský	k2eAgFnSc1d1	italská
ekonomika	ekonomika	k1gFnSc1	ekonomika
nepodávala	podávat	k5eNaImAgFnS	podávat
nikterak	nikterak	k6eAd1	nikterak
oslnivé	oslnivý	k2eAgInPc4d1	oslnivý
výkony	výkon	k1gInPc4	výkon
a	a	k8xC	a
růst	růst	k1gInSc1	růst
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
ustálil	ustálit	k5eAaPmAgMnS	ustálit
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
na	na	k7c4	na
2	[number]	k4	2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
zaveden	zavést	k5eAaPmNgInS	zavést
stát	stát	k1gInSc1	stát
blahobytu	blahobyt	k1gInSc2	blahobyt
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zavedeno	zaveden	k2eAgNnSc4d1	zavedeno
povinné	povinný	k2eAgNnSc4d1	povinné
pojištění	pojištění	k1gNnSc4	pojištění
proti	proti	k7c3	proti
nehodám	nehoda	k1gFnPc3	nehoda
<g/>
,	,	kIx,	,
nemocem	nemoc	k1gFnPc3	nemoc
<g/>
,	,	kIx,	,
nemocem	nemoc	k1gFnPc3	nemoc
stáří	stář	k1gFnPc2	stář
<g/>
,	,	kIx,	,
opatření	opatření	k1gNnPc2	opatření
proti	proti	k7c3	proti
malárii	malárie	k1gFnSc3	malárie
a	a	k8xC	a
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
a	a	k8xC	a
stipendia	stipendium	k1gNnPc4	stipendium
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgFnSc1d1	minimální
mzda	mzda	k1gFnSc1	mzda
<g/>
,	,	kIx,	,
placená	placený	k2eAgFnSc1d1	placená
dovolená	dovolená	k1gFnSc1	dovolená
atp.	atp.	kA	atp.
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgMnS	být
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
režim	režim	k1gInSc4	režim
pro	pro	k7c4	pro
soukromé	soukromý	k2eAgNnSc4d1	soukromé
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
však	však	k9	však
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
socializaci	socializace	k1gFnSc3	socializace
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Mussoliniho	Mussolinize	k6eAd1	Mussolinize
korporativismus	korporativismus	k1gInSc1	korporativismus
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
během	během	k7c2	během
velké	velký	k2eAgFnSc2d1	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
vyšel	vyjít	k5eAaPmAgInS	vyjít
z	z	k7c2	z
Karla	Karel	k1gMnSc2	Karel
Marxe	Marx	k1gMnSc2	Marx
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
korporativismu	korporativismus	k1gInSc2	korporativismus
byla	být	k5eAaImAgFnS	být
metoda	metoda	k1gFnSc1	metoda
kapitalistické	kapitalistický	k2eAgFnSc2d1	kapitalistická
výroby	výroba	k1gFnSc2	výroba
překonána	překonat	k5eAaPmNgFnS	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
dokonce	dokonce	k9	dokonce
korporativismu	korporativismus	k1gInSc6	korporativismus
překonával	překonávat	k5eAaImAgInS	překonávat
socialismus	socialismus	k1gInSc1	socialismus
a	a	k8xC	a
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
novu	nova	k1gFnSc4	nova
syntézu	syntéza	k1gFnSc4	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Osudovým	osudový	k2eAgMnPc3d1	osudový
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stal	stát	k5eAaPmAgInS	stát
nástup	nástup	k1gInSc1	nástup
nacistů	nacista	k1gMnPc2	nacista
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
nastolení	nastolení	k1gNnSc1	nastolení
vlády	vláda	k1gFnSc2	vláda
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
netajil	tajit	k5eNaImAgMnS	tajit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
politický	politický	k2eAgInSc4d1	politický
vzor	vzor	k1gInSc4	vzor
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
Itálii	Itálie	k1gFnSc4	Itálie
jen	jen	k6eAd1	jen
výhodou	výhoda	k1gFnSc7	výhoda
a	a	k8xC	a
proitalská	proitalský	k2eAgFnSc1d1	proitalský
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
případným	případný	k2eAgInSc7d1	případný
trumfem	trumf	k1gInSc7	trumf
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
s	s	k7c7	s
Brity	Brit	k1gMnPc7	Brit
a	a	k8xC	a
především	především	k6eAd1	především
s	s	k7c7	s
Francouzi	Francouz	k1gMnPc7	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
však	však	k9	však
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
případná	případný	k2eAgFnSc1d1	případná
německo-italská	německotalský	k2eAgFnSc1d1	německo-italská
spolupráce	spolupráce	k1gFnSc1	spolupráce
není	být	k5eNaImIp3nS	být
bez	bez	k7c2	bez
kazu	kaz	k1gInSc2	kaz
<g/>
.	.	kIx.	.
</s>
<s>
Třecí	třecí	k2eAgFnSc7d1	třecí
plochou	plocha	k1gFnSc7	plocha
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
stalo	stát	k5eAaPmAgNnS	stát
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
toto	tento	k3xDgNnSc4	tento
svoje	svůj	k3xOyFgNnSc4	svůj
rodiště	rodiště	k1gNnSc4	rodiště
mínil	mínit	k5eAaImAgMnS	mínit
připojit	připojit	k5eAaPmF	připojit
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
tak	tak	k6eAd1	tak
základ	základ	k1gInSc4	základ
nové	nový	k2eAgFnSc2d1	nová
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
duceho	duceze	k6eAd1	duceze
strategických	strategický	k2eAgInPc6d1	strategický
plánech	plán	k1gInPc6	plán
však	však	k9	však
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
Rakousko	Rakousko	k1gNnSc1	Rakousko
hrálo	hrát	k5eAaImAgNnS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Podepsáním	podepsání	k1gNnSc7	podepsání
Římských	římský	k2eAgInPc2d1	římský
protokolů	protokol	k1gInPc2	protokol
mezi	mezi	k7c7	mezi
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
,	,	kIx,	,
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1934	[number]	k4	1934
posílilo	posílit	k5eAaPmAgNnS	posílit
italský	italský	k2eAgInSc4d1	italský
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
Podunají	Podunají	k1gNnSc6	Podunají
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
Itálie	Itálie	k1gFnSc1	Itálie
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
obdivována	obdivován	k2eAgFnSc1d1	obdivována
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
obdivovatele	obdivovatel	k1gMnSc4	obdivovatel
byl	být	k5eAaImAgMnS	být
například	například	k6eAd1	například
Winston	Winston	k1gInSc4	Winston
Churchill	Churchilla	k1gFnPc2	Churchilla
<g/>
,	,	kIx,	,
Mahátma	Mahátma	k1gFnSc1	Mahátma
Gándhí	Gándhí	k1gMnSc1	Gándhí
anebo	anebo	k8xC	anebo
americký	americký	k2eAgMnSc1d1	americký
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
Washbur	Washbur	k1gMnSc1	Washbur
Child	Child	k1gMnSc1	Child
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
sovětského	sovětský	k2eAgMnSc2d1	sovětský
komunistického	komunistický	k2eAgMnSc2d1	komunistický
politika	politik	k1gMnSc2	politik
Nikolaje	Nikolaj	k1gMnSc4	Nikolaj
Bucharina	Bucharin	k1gMnSc4	Bucharin
fašisté	fašista	k1gMnPc1	fašista
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
metodách	metoda	k1gFnPc6	metoda
převzali	převzít	k5eAaPmAgMnP	převzít
a	a	k8xC	a
aplikovali	aplikovat	k5eAaBmAgMnP	aplikovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
kterákoliv	kterýkoliv	k3yIgFnSc1	kterýkoliv
jiná	jiný	k2eAgFnSc1d1	jiná
strana	strana	k1gFnSc1	strana
zkušenosti	zkušenost	k1gFnSc2	zkušenost
ruské	ruský	k2eAgFnSc2d1	ruská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
sám	sám	k3xTgMnSc1	sám
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
v	v	k7c6	v
úctě	úcta	k1gFnSc6	úcta
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Ijiče	Ijič	k1gFnSc2	Ijič
Lenina	Lenin	k1gMnSc2	Lenin
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
Teoretik	teoretik	k1gMnSc1	teoretik
fašismu	fašismus	k1gInSc2	fašismus
Ugo	Ugo	k1gMnSc1	Ugo
Spirito	Spirito	k?	Spirito
spekuloval	spekulovat	k5eAaImAgMnS	spekulovat
o	o	k7c6	o
možné	možný	k2eAgFnSc6d1	možná
syntéze	syntéza	k1gFnSc6	syntéza
fašistického	fašistický	k2eAgInSc2d1	fašistický
a	a	k8xC	a
sovětského	sovětský	k2eAgInSc2d1	sovětský
(	(	kIx(	(
<g/>
komunistického	komunistický	k2eAgInSc2d1	komunistický
<g/>
)	)	kIx)	)
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1934	[number]	k4	1934
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
rakouských	rakouský	k2eAgMnPc2d1	rakouský
nacistů	nacista	k1gMnPc2	nacista
pokusila	pokusit	k5eAaPmAgFnS	pokusit
svrhnout	svrhnout	k5eAaPmF	svrhnout
Dollfussovu	Dollfussův	k2eAgFnSc4d1	Dollfussova
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
připojit	připojit	k5eAaPmF	připojit
Rakousko	Rakousko	k1gNnSc4	Rakousko
k	k	k7c3	k
Německé	německý	k2eAgFnSc3d1	německá
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Amatérský	amatérský	k2eAgInSc1d1	amatérský
puč	puč	k1gInSc1	puč
však	však	k9	však
selhal	selhat	k5eAaPmAgInS	selhat
a	a	k8xC	a
jediné	jediný	k2eAgFnSc6d1	jediná
<g/>
,	,	kIx,	,
čeho	co	k3yInSc2	co
pučisté	pučista	k1gMnPc1	pučista
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
spolkového	spolkový	k2eAgMnSc4d1	spolkový
kancléře	kancléř	k1gMnSc4	kancléř
<g/>
.	.	kIx.	.
</s>
<s>
Puč	puč	k1gInSc4	puč
sám	sám	k3xTgInSc4	sám
byl	být	k5eAaImAgInS	být
zlikvidován	zlikvidovat	k5eAaPmNgInS	zlikvidovat
policejními	policejní	k2eAgFnPc7d1	policejní
silami	síla	k1gFnPc7	síla
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
Kurta	kurta	k1gFnSc1	kurta
Schusschnigga	Schusschnigga	k1gFnSc1	Schusschnigga
sáhla	sáhnout	k5eAaPmAgFnS	sáhnout
k	k	k7c3	k
represím	represe	k1gFnPc3	represe
proti	proti	k7c3	proti
rakouské	rakouský	k2eAgFnSc3d1	rakouská
odnoži	odnož	k1gFnSc3	odnož
NSDAP	NSDAP	kA	NSDAP
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
duceho	duceze	k6eAd1	duceze
tak	tak	k6eAd1	tak
rozzuřilo	rozzuřit	k5eAaPmAgNnS	rozzuřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyslal	vyslat	k5eAaPmAgMnS	vyslat
k	k	k7c3	k
rakouským	rakouský	k2eAgFnPc3d1	rakouská
hranicím	hranice	k1gFnPc3	hranice
4	[number]	k4	4
armádní	armádní	k2eAgFnPc4d1	armádní
divize	divize	k1gFnPc4	divize
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
donutilo	donutit	k5eAaPmAgNnS	donutit
Hitlera	Hitler	k1gMnSc2	Hitler
od	od	k7c2	od
celé	celý	k2eAgFnSc2d1	celá
věci	věc	k1gFnSc2	věc
se	se	k3xPyFc4	se
distancovat	distancovat	k5eAaBmF	distancovat
<g/>
.	.	kIx.	.
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
krize	krize	k1gFnSc1	krize
vztahy	vztah	k1gInPc1	vztah
obou	dva	k4xCgMnPc2	dva
diktátorů	diktátor	k1gMnPc2	diktátor
na	na	k7c6	na
dlouho	dlouho	k6eAd1	dlouho
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
si	se	k3xPyFc3	se
tímto	tento	k3xDgInSc7	tento
zásahem	zásah	k1gInSc7	zásah
fašistická	fašistický	k2eAgFnSc1d1	fašistická
Itálie	Itálie	k1gFnSc1	Itálie
získala	získat	k5eAaPmAgFnS	získat
určité	určitý	k2eAgNnSc4d1	určité
uznání	uznání	k1gNnSc4	uznání
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
garant	garant	k1gMnSc1	garant
evropského	evropský	k2eAgInSc2d1	evropský
míru	mír	k1gInSc2	mír
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
italská	italský	k2eAgFnSc1d1	italská
ekonomika	ekonomika	k1gFnSc1	ekonomika
úpěla	úpět	k5eAaImAgFnS	úpět
pod	pod	k7c7	pod
tíhou	tíha	k1gFnSc7	tíha
zbrojení	zbrojení	k1gNnSc2	zbrojení
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
mohutných	mohutný	k2eAgFnPc2d1	mohutná
leteckých	letecký	k2eAgFnPc2d1	letecká
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
velkorysé	velkorysý	k2eAgNnSc1d1	velkorysé
námořní	námořní	k2eAgNnSc1d1	námořní
zbrojení	zbrojení	k1gNnSc1	zbrojení
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
kontingentu	kontingent	k1gInSc2	kontingent
pozemní	pozemní	k2eAgFnSc2d1	pozemní
armády	armáda	k1gFnSc2	armáda
dávala	dávat	k5eAaImAgFnS	dávat
iluzi	iluze	k1gFnSc4	iluze
silné	silný	k2eAgFnSc2d1	silná
a	a	k8xC	a
akceschopné	akceschopný	k2eAgFnSc2d1	akceschopná
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nic	nic	k3yNnSc1	nic
nebylo	být	k5eNaImAgNnS	být
vzdálenější	vzdálený	k2eAgFnSc3d2	vzdálenější
realitě	realita	k1gFnSc3	realita
<g/>
.	.	kIx.	.
</s>
<s>
Regia	Regia	k1gFnSc1	Regia
Aeronautica	Aeronautica	k1gFnSc1	Aeronautica
(	(	kIx(	(
<g/>
Královské	královský	k2eAgNnSc1d1	královské
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
)	)	kIx)	)
sice	sice	k8xC	sice
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
kontě	konto	k1gNnSc6	konto
řadu	řad	k1gInSc2	řad
rekordů	rekord	k1gInPc2	rekord
a	a	k8xC	a
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzdor	vzdor	k7c3	vzdor
jisté	jistý	k2eAgFnSc3d1	jistá
modernizaci	modernizace	k1gFnSc3	modernizace
prosazované	prosazovaný	k2eAgNnSc1d1	prosazované
maršálem	maršál	k1gMnSc7	maršál
letectva	letectvo	k1gNnSc2	letectvo
Italo	Italo	k1gNnSc1	Italo
Balbem	Balb	k1gInSc7	Balb
tvořily	tvořit	k5eAaImAgInP	tvořit
ještě	ještě	k9	ještě
koncem	koncem	k7c2	koncem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
páteř	páteř	k1gFnSc4	páteř
stíhacího	stíhací	k2eAgNnSc2d1	stíhací
letectva	letectvo	k1gNnSc2	letectvo
dvouplošníky	dvouplošník	k1gInPc1	dvouplošník
<g/>
.	.	kIx.	.
</s>
<s>
Bombardovací	bombardovací	k2eAgFnPc1d1	bombardovací
síly	síla	k1gFnPc1	síla
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
byly	být	k5eAaImAgInP	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
klientelismus	klientelismus	k1gInSc1	klientelismus
při	při	k7c6	při
zadávání	zadávání	k1gNnSc6	zadávání
zakázek	zakázka	k1gFnPc2	zakázka
způsobil	způsobit	k5eAaPmAgInS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
letecký	letecký	k2eAgInSc1d1	letecký
průmysl	průmysl	k1gInSc1	průmysl
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
najednou	najednou	k6eAd1	najednou
mnoho	mnoho	k4c4	mnoho
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
odlišných	odlišný	k2eAgInPc2d1	odlišný
parametrů	parametr	k1gInPc2	parametr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
činilo	činit	k5eAaImAgNnS	činit
potíže	potíž	k1gFnPc4	potíž
při	při	k7c6	při
snaze	snaha	k1gFnSc6	snaha
ujednotit	ujednotit	k5eAaPmF	ujednotit
výzbroj	výzbroj	k1gFnSc4	výzbroj
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byl	být	k5eAaImAgInS	být
Balbo	Balba	k1gFnSc5	Balba
odsunut	odsunut	k2eAgInSc1d1	odsunut
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
do	do	k7c2	do
Libye	Libye	k1gFnSc2	Libye
a	a	k8xC	a
o	o	k7c6	o
nových	nový	k2eAgInPc6d1	nový
typech	typ	k1gInPc6	typ
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
mimořádně	mimořádně	k6eAd1	mimořádně
nešťastným	šťastný	k2eNgInSc7d1	nešťastný
způsobem	způsob	k1gInSc7	způsob
generál	generál	k1gMnSc1	generál
Giuseppe	Giusepp	k1gMnSc5	Giusepp
Valle	Vall	k1gMnSc5	Vall
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
italské	italský	k2eAgNnSc1d1	italské
vojenské	vojenský	k2eAgNnSc1d1	vojenské
letectvo	letectvo	k1gNnSc1	letectvo
ztrácet	ztrácet	k5eAaImF	ztrácet
krok	krok	k1gInSc4	krok
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
<g/>
.	.	kIx.	.
</s>
<s>
Regia	Regia	k1gFnSc1	Regia
Marina	Marina	k1gFnSc1	Marina
(	(	kIx(	(
<g/>
Královské	královský	k2eAgNnSc1d1	královské
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
mohla	moct	k5eAaImAgFnS	moct
pochlubit	pochlubit	k5eAaPmF	pochlubit
řadou	řada	k1gFnSc7	řada
moderních	moderní	k2eAgInPc2d1	moderní
či	či	k8xC	či
modernizovaných	modernizovaný	k2eAgFnPc2d1	modernizovaná
bitevních	bitevní	k2eAgFnPc2d1	bitevní
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
těžkých	těžký	k2eAgInPc2d1	těžký
křižníků	křižník	k1gInPc2	křižník
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
pancéřováním	pancéřování	k1gNnSc7	pancéřování
<g/>
,	,	kIx,	,
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
výzbrojí	výzbroj	k1gFnSc7	výzbroj
vyrovnaly	vyrovnat	k5eAaBmAgInP	vyrovnat
vojenským	vojenský	k2eAgNnPc3d1	vojenské
plavidlům	plavidlo	k1gNnPc3	plavidlo
ostatních	ostatní	k2eAgInPc2d1	ostatní
velmocí	velmoc	k1gFnSc7	velmoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neustálý	neustálý	k2eAgInSc1d1	neustálý
nedostatek	nedostatek	k1gInSc1	nedostatek
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
nutil	nutit	k5eAaImAgInS	nutit
námořní	námořní	k2eAgNnSc4d1	námořní
velení	velení	k1gNnSc4	velení
zkracovat	zkracovat	k5eAaImF	zkracovat
výcvik	výcvik	k1gInSc4	výcvik
na	na	k7c4	na
minimum	minimum	k1gNnSc4	minimum
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
úroveň	úroveň	k1gFnSc1	úroveň
výcviku	výcvik	k1gInSc2	výcvik
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
britským	britský	k2eAgMnSc7d1	britský
či	či	k8xC	či
francouzským	francouzský	k2eAgNnSc7d1	francouzské
vojenským	vojenský	k2eAgNnSc7d1	vojenské
námořnictvem	námořnictvo	k1gNnSc7	námořnictvo
naprosto	naprosto	k6eAd1	naprosto
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lehčích	lehký	k2eAgNnPc2d2	lehčí
plavidel	plavidlo	k1gNnPc2	plavidlo
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
oceli	ocel	k1gFnSc2	ocel
šetřilo	šetřit	k5eAaImAgNnS	šetřit
na	na	k7c6	na
pancéřování	pancéřování	k1gNnSc6	pancéřování
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
velkých	velký	k2eAgFnPc2d1	velká
bojových	bojový	k2eAgFnPc2d1	bojová
ztrát	ztráta	k1gFnPc2	ztráta
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zanedbán	zanedbat	k5eAaPmNgInS	zanedbat
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
technický	technický	k2eAgInSc1d1	technický
vývoj	vývoj	k1gInSc1	vývoj
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
radar	radar	k1gInSc1	radar
či	či	k8xC	či
sonar	sonar	k1gInSc1	sonar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Námořní	námořní	k2eAgNnSc4d1	námořní
a	a	k8xC	a
letecké	letecký	k2eAgNnSc4d1	letecké
zbrojení	zbrojení	k1gNnSc4	zbrojení
se	se	k3xPyFc4	se
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
především	především	k9	především
na	na	k7c6	na
stavu	stav	k1gInSc6	stav
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
Regio	Regio	k6eAd1	Regio
Esercito	Esercit	k2eAgNnSc1d1	Esercit
(	(	kIx(	(
<g/>
Královská	královský	k2eAgFnSc1d1	královská
armáda	armáda	k1gFnSc1	armáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pěchota	pěchota	k1gFnSc1	pěchota
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
spokojit	spokojit	k5eAaPmF	spokojit
se	s	k7c7	s
zastaralými	zastaralý	k2eAgFnPc7d1	zastaralá
zbraněmi	zbraň	k1gFnPc7	zbraň
malé	malý	k2eAgFnSc2d1	malá
ráže	ráže	k1gFnSc2	ráže
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dělostřelectvo	dělostřelectvo	k1gNnSc1	dělostřelectvo
celkově	celkově	k6eAd1	celkově
zastarávalo	zastarávat	k5eAaImAgNnS	zastarávat
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
italské	italský	k2eAgFnSc2d1	italská
armády	armáda	k1gFnSc2	armáda
byla	být	k5eAaImAgFnS	být
motorizována	motorizován	k2eAgFnSc1d1	motorizována
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
mobilitu	mobilita	k1gFnSc4	mobilita
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
zkomplikoval	zkomplikovat	k5eAaPmAgMnS	zkomplikovat
generál	generál	k1gMnSc1	generál
Alberto	Alberta	k1gFnSc5	Alberta
Pariani	Pariaň	k1gFnSc6	Pariaň
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
sestná	sestný	k2eAgFnSc1d1	sestný
koncepce	koncepce	k1gFnSc1	koncepce
"	"	kIx"	"
<g/>
binární	binární	k2eAgFnSc1d1	binární
divize	divize	k1gFnSc1	divize
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
nyní	nyní	k6eAd1	nyní
složena	složit	k5eAaPmNgFnS	složit
jen	jen	k9	jen
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
pluků	pluk	k1gInPc2	pluk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
omezilo	omezit	k5eAaPmAgNnS	omezit
živou	živá	k1gFnSc4	živá
a	a	k8xC	a
palebnou	palebný	k2eAgFnSc4d1	palebná
sílu	síla	k1gFnSc4	síla
divize	divize	k1gFnSc2	divize
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
slabých	slabý	k2eAgFnPc2d1	slabá
brigád	brigáda	k1gFnPc2	brigáda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
vysokými	vysoký	k2eAgFnPc7d1	vysoká
ztrátami	ztráta	k1gFnPc7	ztráta
během	během	k7c2	během
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dovršení	dovršení	k1gNnSc3	dovršení
všeho	všecek	k3xTgNnSc2	všecek
propadlo	propadnout	k5eAaPmAgNnS	propadnout
italské	italský	k2eAgNnSc4d1	italské
velení	velení	k1gNnSc4	velení
koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
"	"	kIx"	"
<g/>
tančíkové	tančíkový	k2eAgFnSc3d1	tančíkový
horečce	horečka	k1gFnSc3	horečka
<g/>
"	"	kIx"	"
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
páteří	páteř	k1gFnSc7	páteř
tankových	tankový	k2eAgFnPc2d1	tanková
sil	síla	k1gFnPc2	síla
italské	italský	k2eAgFnSc2d1	italská
armády	armáda	k1gFnSc2	armáda
stalo	stát	k5eAaPmAgNnS	stát
2	[number]	k4	2
500	[number]	k4	500
slabě	slabě	k6eAd1	slabě
vyzbrojených	vyzbrojený	k2eAgFnPc2d1	vyzbrojená
<g/>
,	,	kIx,	,
pancéřovaných	pancéřovaný	k2eAgFnPc2d1	pancéřovaná
a	a	k8xC	a
k	k	k7c3	k
boji	boj	k1gInSc3	boj
proti	proti	k7c3	proti
jiným	jiný	k2eAgInPc3d1	jiný
obrněncům	obrněnec	k1gInPc3	obrněnec
nevhodných	vhodný	k2eNgFnPc2d1	nevhodná
tančíků	tančík	k1gInPc2	tančík
L	L	kA	L
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Hnán	hnán	k2eAgInSc1d1	hnán
svým	svůj	k3xOyFgInSc7	svůj
sklonem	sklon	k1gInSc7	sklon
k	k	k7c3	k
avanturismu	avanturismus	k1gInSc3	avanturismus
<g/>
,	,	kIx,	,
snem	sen	k1gInSc7	sen
o	o	k7c6	o
italské	italský	k2eAgFnSc6d1	italská
hegemonii	hegemonie	k1gFnSc6	hegemonie
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
soupeření	soupeření	k1gNnSc1	soupeření
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
v	v	k7c6	v
expanzi	expanze	k1gFnSc6	expanze
však	však	k9	však
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
přehlížel	přehlížet	k5eAaImAgMnS	přehlížet
a	a	k8xC	a
vršil	vršit	k5eAaImAgMnS	vršit
chybu	chyba	k1gFnSc4	chyba
za	za	k7c7	za
chybou	chyba	k1gFnSc7	chyba
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
zbavil	zbavit	k5eAaPmAgInS	zbavit
možných	možný	k2eAgMnPc2d1	možný
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Maršál	maršál	k1gMnSc1	maršál
Balbo	Balba	k1gFnSc5	Balba
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzdor	vzdor	k7c3	vzdor
tomu	ten	k3xDgMnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
prominentním	prominentní	k2eAgMnPc3d1	prominentní
přátelům	přítel	k1gMnPc3	přítel
patřil	patřit	k5eAaImAgMnS	patřit
i	i	k9	i
velitel	velitel	k1gMnSc1	velitel
nacistické	nacistický	k2eAgFnSc2d1	nacistická
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
Hermann	Hermann	k1gMnSc1	Hermann
Göring	Göring	k1gInSc4	Göring
<g/>
,	,	kIx,	,
odmítal	odmítat	k5eAaImAgMnS	odmítat
nacisty	nacista	k1gMnPc4	nacista
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
antisemitismus	antisemitismus	k1gInSc4	antisemitismus
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
odsunut	odsunout	k5eAaPmNgInS	odsunout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
coby	coby	k?	coby
nový	nový	k2eAgMnSc1d1	nový
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
Libye	Libye	k1gFnSc2	Libye
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
v	v	k7c6	v
duceho	duce	k1gMnSc2	duce
okolí	okolí	k1gNnSc4	okolí
zmlkl	zmlknout	k5eAaPmAgMnS	zmlknout
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejrealističtějších	realistický	k2eAgInPc2d3	nejrealističtější
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
katastrofálnější	katastrofální	k2eAgMnSc1d2	katastrofálnější
bylo	být	k5eAaImAgNnS	být
odsunutí	odsunutí	k1gNnSc1	odsunutí
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
hraběte	hrabě	k1gMnSc2	hrabě
Dina	Dinus	k1gMnSc2	Dinus
Grandiho	Grandi	k1gMnSc2	Grandi
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
postu	post	k1gInSc2	post
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
<g/>
.	.	kIx.	.
</s>
<s>
Opatrná	opatrný	k2eAgFnSc1d1	opatrná
Grandiho	Grandi	k1gMnSc2	Grandi
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
a	a	k8xC	a
nedůvěra	nedůvěra	k1gFnSc1	nedůvěra
vůči	vůči	k7c3	vůči
Německu	Německo	k1gNnSc3	Německo
vzala	vzít	k5eAaPmAgFnS	vzít
definitivně	definitivně	k6eAd1	definitivně
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1936	[number]	k4	1936
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
dosadil	dosadit	k5eAaPmAgInS	dosadit
na	na	k7c4	na
uprázdněný	uprázdněný	k2eAgInSc4d1	uprázdněný
post	post	k1gInSc4	post
svého	svůj	k3xOyFgMnSc2	svůj
zetě	zeť	k1gMnSc2	zeť
hraběte	hrabě	k1gMnSc2	hrabě
Galeazzo	Galeazza	k1gFnSc5	Galeazza
Ciana	Ciaen	k2eAgMnSc4d1	Ciaen
<g/>
,	,	kIx,	,
nadšeného	nadšený	k2eAgMnSc4d1	nadšený
přívržence	přívrženec	k1gMnSc4	přívrženec
aliance	aliance	k1gFnSc2	aliance
s	s	k7c7	s
Berlínem	Berlín	k1gInSc7	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
jugoslávského	jugoslávský	k2eAgMnSc4d1	jugoslávský
krále	král	k1gMnSc4	král
Alexandera	Alexander	k1gMnSc2	Alexander
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
provedlo	provést	k5eAaPmAgNnS	provést
makedonsko-chorvatské	makedonskohorvatský	k2eAgNnSc1d1	makedonsko-chorvatský
komando	komando	k1gNnSc1	komando
vyzbrojené	vyzbrojený	k2eAgNnSc1d1	vyzbrojené
Němci	Němec	k1gMnPc7	Němec
a	a	k8xC	a
vycvičené	vycvičený	k2eAgInPc4d1	vycvičený
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1934	[number]	k4	1934
a	a	k8xC	a
jehož	jehož	k3xOyRp3gInPc4	jehož
následky	následek	k1gInPc4	následek
nepřežil	přežít	k5eNaPmAgMnS	přežít
ani	ani	k9	ani
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Francie	Francie	k1gFnSc2	Francie
Louis	Louis	k1gMnSc1	Louis
Barthou	Bartha	k1gMnSc7	Bartha
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
oslabení	oslabení	k1gNnSc3	oslabení
vlivu	vliv	k1gInSc2	vliv
Francouzů	Francouz	k1gMnPc2	Francouz
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
(	(	kIx(	(
<g/>
princ-regent	princegent	k1gMnSc1	princ-regent
Petar	Petar	k1gMnSc1	Petar
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
dohodnout	dohodnout	k5eAaPmF	dohodnout
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Barthouův	Barthouův	k2eAgMnSc1d1	Barthouův
nástupce	nástupce	k1gMnSc1	nástupce
Pierre	Pierr	k1gInSc5	Pierr
Laval	Laval	k1gMnSc1	Laval
pokusil	pokusit	k5eAaPmAgMnS	pokusit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1935	[number]	k4	1935
získat	získat	k5eAaPmF	získat
si	se	k3xPyFc3	se
Mussoliniho	Mussolini	k1gMnSc4	Mussolini
územními	územní	k2eAgInPc7d1	územní
ústupky	ústupek	k1gInPc7	ústupek
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Čadu	Čad	k1gInSc2	Čad
a	a	k8xC	a
francouzského	francouzský	k2eAgNnSc2d1	francouzské
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterou	který	k3yIgFnSc7	který
neoficiálně	oficiálně	k6eNd1	oficiálně
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
i	i	k9	i
britský	britský	k2eAgMnSc1d1	britský
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Samuel	Samuel	k1gMnSc1	Samuel
Hoare	Hoar	k1gInSc5	Hoar
byla	být	k5eAaImAgNnP	být
i	i	k9	i
uznání	uznání	k1gNnPc1	uznání
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
Habešské	habešský	k2eAgNnSc1d1	habešské
císařství	císařství	k1gNnSc1	císařství
(	(	kIx(	(
<g/>
vzdor	vzdor	k7c3	vzdor
tomu	ten	k3xDgNnSc3	ten
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
členský	členský	k2eAgInSc4d1	členský
stát	stát	k1gInSc4	stát
Společnosti	společnost	k1gFnSc2	společnost
Národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
sféře	sféra	k1gFnSc6	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1935	[number]	k4	1935
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
italských	italský	k2eAgFnPc6d1	italská
lázních	lázeň	k1gFnPc6	lázeň
Stresa	Stresa	k1gFnSc1	Stresa
schválena	schválit	k5eAaPmNgFnS	schválit
tzv.	tzv.	kA	tzv.
Streská	Streský	k2eAgFnSc1d1	Streský
fronta	fronta	k1gFnSc1	fronta
(	(	kIx(	(
<g/>
pakt	pakt	k1gInSc1	pakt
Británie-Francie-Itálie	Británie-Francie-Itálie	k1gFnSc2	Británie-Francie-Itálie
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
však	však	k9	však
ještě	ještě	k9	ještě
během	během	k7c2	během
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
podepsali	podepsat	k5eAaPmAgMnP	podepsat
Německo-britskou	německoritský	k2eAgFnSc4d1	německo-britská
námořní	námořní	k2eAgFnSc4d1	námořní
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
což	což	k9	což
plánovaný	plánovaný	k2eAgInSc4d1	plánovaný
pakt	pakt	k1gInSc4	pakt
poněkud	poněkud	k6eAd1	poněkud
pošramotilo	pošramotit	k5eAaPmAgNnS	pošramotit
<g/>
.	.	kIx.	.
</s>
<s>
Definitivní	definitivní	k2eAgFnSc4d1	definitivní
ránu	rána	k1gFnSc4	rána
z	z	k7c2	z
milosti	milost	k1gFnSc2	milost
paktu	pakt	k1gInSc2	pakt
zasadil	zasadit	k5eAaPmAgMnS	zasadit
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1935	[number]	k4	1935
Itálie	Itálie	k1gFnSc1	Itálie
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
na	na	k7c4	na
Habeš	Habeš	k1gFnSc4	Habeš
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
Etiopii	Etiopie	k1gFnSc3	Etiopie
tehdy	tehdy	k6eAd1	tehdy
říkalo	říkat	k5eAaImAgNnS	říkat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nerovném	rovný	k2eNgInSc6d1	nerovný
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
více	hodně	k6eAd2	hodně
sil	síla	k1gFnPc2	síla
než	než	k8xS	než
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vojska	vojsko	k1gNnSc2	vojsko
maršála	maršál	k1gMnSc2	maršál
Pietra	Pietr	k1gMnSc2	Pietr
Badoglia	Badoglius	k1gMnSc2	Badoglius
a	a	k8xC	a
generála	generál	k1gMnSc2	generál
Grazianiho	Graziani	k1gMnSc2	Graziani
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Habeš	Habeš	k1gFnSc1	Habeš
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1936	[number]	k4	1936
zařazena	zařadit	k5eAaPmNgFnS	zařadit
do	do	k7c2	do
italského	italský	k2eAgNnSc2d1	italské
koloniálního	koloniální	k2eAgNnSc2d1	koloniální
panství	panství	k1gNnSc2	panství
jako	jako	k8xC	jako
Italská	italský	k2eAgFnSc1d1	italská
východní	východní	k2eAgFnSc1d1	východní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Vzdor	vzdor	k7c3	vzdor
vítězným	vítězný	k2eAgFnPc3d1	vítězná
fanfárám	fanfára	k1gFnPc3	fanfára
v	v	k7c6	v
italských	italský	k2eAgFnPc6d1	italská
ulicí	ulice	k1gFnSc7	ulice
a	a	k8xC	a
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
vyletěla	vyletět	k5eAaPmAgFnS	vyletět
do	do	k7c2	do
nadoblačných	nadoblačný	k2eAgFnPc2d1	nadoblačná
výšek	výška	k1gFnPc2	výška
<g/>
,	,	kIx,	,
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
válka	válka	k1gFnSc1	válka
do	do	k7c2	do
"	"	kIx"	"
<g/>
oslnivého	oslnivý	k2eAgNnSc2d1	oslnivé
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
"	"	kIx"	"
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
velkých	velký	k2eAgInPc2d1	velký
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
tažení	tažení	k1gNnSc4	tažení
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zatížily	zatížit	k5eAaPmAgFnP	zatížit
již	již	k6eAd1	již
tak	tak	k6eAd1	tak
přetíženou	přetížený	k2eAgFnSc4d1	přetížená
ekonomiku	ekonomika	k1gFnSc4	ekonomika
zde	zde	k6eAd1	zde
Itálie	Itálie	k1gFnSc1	Itálie
musela	muset	k5eAaImAgFnS	muset
vydržovat	vydržovat	k5eAaImF	vydržovat
téměř	téměř	k6eAd1	téměř
300	[number]	k4	300
000	[number]	k4	000
italských	italský	k2eAgMnPc2d1	italský
a	a	k8xC	a
domorodých	domorodý	k2eAgMnPc2d1	domorodý
vojáků	voják	k1gMnPc2	voják
jako	jako	k8xC	jako
okupační	okupační	k2eAgFnSc4d1	okupační
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1936-1940	[number]	k4	1936-1940
zde	zde	k6eAd1	zde
místokrálové	místokrál	k1gMnPc1	místokrál
Badoglio	Badoglio	k1gMnSc1	Badoglio
<g/>
,	,	kIx,	,
Graziani	Grazian	k1gMnPc1	Grazian
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Aosty	Aosta	k1gFnSc2	Aosta
museli	muset	k5eAaImAgMnP	muset
nadále	nadále	k6eAd1	nadále
bojovat	bojovat	k5eAaImF	bojovat
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
císařské	císařský	k2eAgFnSc2d1	císařská
armády	armáda	k1gFnSc2	armáda
vedoucí	vedoucí	k1gMnSc1	vedoucí
gerilovou	gerilový	k2eAgFnSc4d1	gerilová
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
prostředky	prostředek	k1gInPc4	prostředek
pohltila	pohltit	k5eAaPmAgFnS	pohltit
stavba	stavba	k1gFnSc1	stavba
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
,	,	kIx,	,
železnic	železnice	k1gFnPc2	železnice
a	a	k8xC	a
nových	nový	k2eAgNnPc2d1	nové
měst	město	k1gNnPc2	město
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgMnPc4d1	budoucí
kolonisty	kolonista	k1gMnPc4	kolonista
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
tak	tak	k9	tak
získala	získat	k5eAaPmAgFnS	získat
impérium	impérium	k1gNnSc4	impérium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
projekt	projekt	k1gInSc1	projekt
Streské	Streský	k2eAgFnSc2d1	Streský
fronty	fronta	k1gFnSc2	fronta
definitivně	definitivně	k6eAd1	definitivně
padl	padnout	k5eAaPmAgMnS	padnout
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
vzdor	vzdor	k6eAd1	vzdor
předchozím	předchozí	k2eAgNnSc7d1	předchozí
ujištěním	ujištění	k1gNnSc7	ujištění
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
pro	pro	k7c4	pro
uvalení	uvalení	k1gNnSc4	uvalení
sankcí	sankce	k1gFnPc2	sankce
proti	proti	k7c3	proti
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
izolace	izolace	k1gFnSc2	izolace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jej	on	k3xPp3gNnSc2	on
nenávratně	návratně	k6eNd1	návratně
připoutalo	připoutat	k5eAaPmAgNnS	připoutat
k	k	k7c3	k
Hitlerovi	Hitler	k1gMnSc3	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podporoval	podporovat	k5eAaImAgInS	podporovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
povstaleckého	povstalecký	k2eAgMnSc2d1	povstalecký
generála	generál	k1gMnSc2	generál
Francisca	Franciscus	k1gMnSc2	Franciscus
Franca	Franca	k?	Franca
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podpora	podpora	k1gFnSc1	podpora
pohltila	pohltit	k5eAaPmAgFnS	pohltit
další	další	k2eAgInPc4d1	další
prostředky	prostředek	k1gInPc4	prostředek
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1938	[number]	k4	1938
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Felice	Felice	k1gFnSc2	Felice
Guarnari	Guarnar	k1gFnSc2	Guarnar
<g/>
,	,	kIx,	,
že	že	k8xS	že
země	země	k1gFnSc1	země
stojí	stát	k5eAaImIp3nS	stát
před	před	k7c7	před
bankrotem	bankrot	k1gInSc7	bankrot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1936	[number]	k4	1936
podepsaly	podepsat	k5eAaPmAgInP	podepsat
oba	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
pakt	pakt	k1gInSc4	pakt
Osa	osa	k1gFnSc1	osa
Berlín-Řím	Berlín-Řím	k2eAgInSc7d2	Berlín-Řím
<g/>
,	,	kIx,	,
což	což	k9	což
byl	být	k5eAaImAgMnS	být
první	první	k4xOgInSc4	první
krok	krok	k1gInSc4	krok
prohlubující	prohlubující	k2eAgNnSc1d1	prohlubující
spojenectví	spojenectví	k1gNnSc1	spojenectví
obou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
tento	tento	k3xDgInSc4	tento
pakt	pakt	k1gInSc4	pakt
zpečetil	zpečetit	k5eAaPmAgInS	zpečetit
osud	osud	k1gInSc1	osud
Rakouska	Rakousko	k1gNnSc2	Rakousko
coby	coby	k?	coby
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1937	[number]	k4	1937
duce	duc	k1gInSc2	duc
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Německo	Německo	k1gNnSc4	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Nacistický	nacistický	k2eAgMnSc1d1	nacistický
diktátor	diktátor	k1gMnSc1	diktátor
si	se	k3xPyFc3	se
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
návštěvy	návštěva	k1gFnSc2	návštěva
nenechal	nechat	k5eNaPmAgMnS	nechat
ujít	ujít	k5eAaPmF	ujít
příležitost	příležitost	k1gFnSc4	příležitost
ukázat	ukázat	k5eAaPmF	ukázat
svému	svůj	k3xOyFgMnSc3	svůj
italskému	italský	k2eAgMnSc3d1	italský
kolegovi	kolega	k1gMnSc3	kolega
jak	jak	k6eAd1	jak
vysoký	vysoký	k2eAgInSc1d1	vysoký
stupeň	stupeň	k1gInSc1	stupeň
industrializace	industrializace	k1gFnSc2	industrializace
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
svou	svůj	k3xOyFgFnSc4	svůj
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
značně	značně	k6eAd1	značně
mechanizovanou	mechanizovaný	k2eAgFnSc4d1	mechanizovaná
moderní	moderní	k2eAgFnSc4d1	moderní
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Pobyt	pobyt	k1gInSc1	pobyt
v	v	k7c6	v
Třetí	třetí	k4xOgFnSc6	třetí
říši	říš	k1gFnSc6	říš
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
učinil	učinit	k5eAaImAgInS	učinit
hluboký	hluboký	k2eAgInSc1d1	hluboký
dojem	dojem	k1gInSc1	dojem
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1937	[number]	k4	1937
opustila	opustit	k5eAaPmAgFnS	opustit
Itálie	Itálie	k1gFnSc1	Itálie
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
a	a	k8xC	a
připojila	připojit	k5eAaPmAgFnS	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
Antikominternímu	Antikominterní	k2eAgInSc3d1	Antikominterní
paktu	pakt	k1gInSc3	pakt
(	(	kIx(	(
<g/>
pakt	pakt	k1gInSc1	pakt
z	z	k7c2	z
listopadu	listopad	k1gInSc2	listopad
1936	[number]	k4	1936
mezi	mezi	k7c7	mezi
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Japonským	japonský	k2eAgNnSc7d1	Japonské
císařstvím	císařství	k1gNnSc7	císařství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
si	se	k3xPyFc3	se
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
beztrestně	beztrestně	k6eAd1	beztrestně
mohl	moct	k5eAaImAgInS	moct
dovolit	dovolit	k5eAaPmF	dovolit
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
mu	on	k3xPp3gInSc3	on
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
sám	sám	k3xTgInSc4	sám
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
nedovolil	dovolit	k5eNaPmAgMnS	dovolit
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1938	[number]	k4	1938
okupovala	okupovat	k5eAaBmAgFnS	okupovat
německá	německý	k2eAgFnSc1d1	německá
vojska	vojsko	k1gNnPc1	vojsko
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
italský	italský	k2eAgMnSc1d1	italský
diktátor	diktátor	k1gMnSc1	diktátor
jen	jen	k9	jen
trpně	trpně	k6eAd1	trpně
přihlížel	přihlížet	k5eAaImAgMnS	přihlížet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1938	[number]	k4	1938
překvapil	překvapit	k5eAaPmAgInS	překvapit
<g/>
,	,	kIx,	,
když	když	k8xS	když
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
zavádět	zavádět	k5eAaImF	zavádět
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
nacistů	nacista	k1gMnPc2	nacista
protižidovské	protižidovský	k2eAgInPc4d1	protižidovský
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1938	[number]	k4	1938
ochotně	ochotně	k6eAd1	ochotně
zprostředkoval	zprostředkovat	k5eAaPmAgInS	zprostředkovat
setkání	setkání	k1gNnSc4	setkání
čtyř	čtyři	k4xCgFnPc2	čtyři
velmocí	velmoc	k1gFnPc2	velmoc
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
společně	společně	k6eAd1	společně
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
donutily	donutit	k5eAaPmAgFnP	donutit
československou	československý	k2eAgFnSc4d1	Československá
vládu	vláda	k1gFnSc4	vláda
odstoupit	odstoupit	k5eAaPmF	odstoupit
Němcům	Němec	k1gMnPc3	Němec
pohraniční	pohraniční	k2eAgFnPc4d1	pohraniční
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
menšinou	menšina	k1gFnSc7	menšina
(	(	kIx(	(
<g/>
Sudety	Sudety	k1gFnPc1	Sudety
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
ústupek	ústupek	k1gInSc1	ústupek
však	však	k9	však
Hitlerovi	Hitler	k1gMnSc3	Hitler
nestačil	stačit	k5eNaBmAgMnS	stačit
nadlouho	nadlouho	k6eAd1	nadlouho
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
vpochodoval	vpochodovat	k5eAaPmAgInS	vpochodovat
Wehrmacht	wehrmacht	k1gInSc1	wehrmacht
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Torzo	torzo	k1gNnSc1	torzo
Československa	Československo	k1gNnSc2	Československo
bylo	být	k5eAaImAgNnS	být
roztrženo	roztrhnout	k5eAaPmNgNnS	roztrhnout
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
Protektorát	protektorát	k1gInSc1	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
s	s	k7c7	s
loutkovou	loutkový	k2eAgFnSc7d1	loutková
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
Slovenský	slovenský	k2eAgInSc1d1	slovenský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
spojenec	spojenec	k1gMnSc1	spojenec
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
de	de	k?	de
facto	facto	k1gNnSc4	facto
veřejně	veřejně	k6eAd1	veřejně
pošlapána	pošlapat	k5eAaPmNgFnS	pošlapat
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
již	již	k6eAd1	již
Hitlerovi	Hitlerův	k2eAgMnPc1d1	Hitlerův
přestali	přestat	k5eAaPmAgMnP	přestat
věřit	věřit	k5eAaImF	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
nezůstal	zůstat	k5eNaPmAgMnS	zůstat
pozadu	pozadu	k6eAd1	pozadu
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1939	[number]	k4	1939
Itálie	Itálie	k1gFnSc1	Itálie
okupovala	okupovat	k5eAaBmAgFnS	okupovat
Albánii	Albánie	k1gFnSc4	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Narychlo	narychlo	k6eAd1	narychlo
zformovaný	zformovaný	k2eAgInSc1d1	zformovaný
a	a	k8xC	a
nedostatečně	dostatečně	k6eNd1	dostatečně
vycvičený	vycvičený	k2eAgInSc1d1	vycvičený
sbor	sbor	k1gInSc1	sbor
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
50	[number]	k4	50
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
první	první	k4xOgFnSc6	první
chvíli	chvíle	k1gFnSc6	chvíle
velké	velký	k2eAgInPc1d1	velký
problémy	problém	k1gInPc1	problém
i	i	k9	i
s	s	k7c7	s
chaotickým	chaotický	k2eAgInSc7d1	chaotický
albánským	albánský	k2eAgInSc7d1	albánský
odporem	odpor	k1gInSc7	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
pomohla	pomoct	k5eAaPmAgFnS	pomoct
prodejnost	prodejnost	k1gFnSc1	prodejnost
albánských	albánský	k2eAgMnPc2d1	albánský
generálů	generál	k1gMnPc2	generál
a	a	k8xC	a
útěk	útěk	k1gInSc4	útěk
samozvaného	samozvaný	k2eAgMnSc2d1	samozvaný
krále	král	k1gMnSc2	král
Zogua	Zoguus	k1gMnSc2	Zoguus
do	do	k7c2	do
řeckého	řecký	k2eAgInSc2d1	řecký
exilu	exil	k1gInSc2	exil
a	a	k8xC	a
okupace	okupace	k1gFnSc1	okupace
země	zem	k1gFnSc2	zem
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
prakticky	prakticky	k6eAd1	prakticky
nekrvavě	krvavě	k6eNd1	krvavě
<g/>
.	.	kIx.	.
</s>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
Emanuel	Emanuel	k1gMnSc1	Emanuel
III	III	kA	III
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
k	k	k7c3	k
titulu	titul	k1gInSc3	titul
král	král	k1gMnSc1	král
italský	italský	k2eAgMnSc1d1	italský
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
etiopský	etiopský	k2eAgMnSc1d1	etiopský
mohl	moct	k5eAaImAgMnS	moct
přidat	přidat	k5eAaPmF	přidat
další	další	k2eAgInSc4d1	další
titul	titul	k1gInSc4	titul
–	–	k?	–
král	král	k1gMnSc1	král
albánský	albánský	k2eAgMnSc1d1	albánský
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1939	[number]	k4	1939
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
celému	celý	k2eAgInSc3d1	celý
světu	svět	k1gInSc3	svět
svůj	svůj	k3xOyFgInSc4	svůj
vazalský	vazalský	k2eAgInSc4d1	vazalský
poměr	poměr	k1gInSc4	poměr
k	k	k7c3	k
Berlínu	Berlín	k1gInSc3	Berlín
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
ministři	ministr	k1gMnPc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Joachim	Joachi	k1gNnSc7	Joachi
von	von	k1gInSc1	von
Ribbentrop	Ribbentrop	k1gInSc1	Ribbentrop
a	a	k8xC	a
Galeazzo	Galeazza	k1gFnSc5	Galeazza
Ciano	Ciano	k6eAd1	Ciano
podepsali	podepsat	k5eAaPmAgMnP	podepsat
Ocelový	ocelový	k2eAgInSc4d1	ocelový
pakt	pakt	k1gInSc4	pakt
<g/>
,	,	kIx,	,
definitivně	definitivně	k6eAd1	definitivně
zavazující	zavazující	k2eAgFnSc4d1	zavazující
Itálii	Itálie	k1gFnSc4	Itálie
za	za	k7c2	za
všech	všecek	k3xTgFnPc2	všecek
okolností	okolnost	k1gFnPc2	okolnost
podporovat	podporovat	k5eAaImF	podporovat
Německo	Německo	k1gNnSc4	Německo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
(	(	kIx(	(
<g/>
a	a	k8xC	a
Ciana	Ciana	k1gFnSc1	Ciana
<g/>
)	)	kIx)	)
Ribbentrop	Ribbentrop	k1gInSc1	Ribbentrop
ujišťoval	ujišťovat	k5eAaImAgInS	ujišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
případná	případný	k2eAgFnSc1d1	případná
velká	velký	k2eAgFnSc1d1	velká
válka	válka	k1gFnSc1	válka
nevypukne	vypuknout	k5eNaPmIp3nS	vypuknout
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
za	za	k7c4	za
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
suše	suš	k1gFnSc2	suš
oznámil	oznámit	k5eAaPmAgInS	oznámit
Cianovi	Cian	k1gMnSc3	Cian
<g/>
,	,	kIx,	,
že	že	k8xS	že
útok	útok	k1gInSc1	útok
na	na	k7c4	na
Polsko	Polsko	k1gNnSc4	Polsko
nastane	nastat	k5eAaPmIp3nS	nastat
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
záchvatu	záchvat	k1gInSc6	záchvat
zdravého	zdravý	k2eAgInSc2d1	zdravý
rozumu	rozum	k1gInSc2	rozum
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svému	svůj	k3xOyFgMnSc3	svůj
spojenci	spojenec	k1gMnSc3	spojenec
<g/>
,	,	kIx,	,
že	že	k8xS	že
Itálie	Itálie	k1gFnSc1	Itálie
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
termínu	termín	k1gInSc6	termín
nemůže	moct	k5eNaImIp3nS	moct
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Hitlerův	Hitlerův	k2eAgInSc4d1	Hitlerův
dotaz	dotaz	k1gInSc4	dotaz
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Itálie	Itálie	k1gFnSc1	Itálie
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dodržela	dodržet	k5eAaPmAgFnS	dodržet
spojenecké	spojenecký	k2eAgInPc4d1	spojenecký
závazky	závazek	k1gInPc4	závazek
<g/>
,	,	kIx,	,
následoval	následovat	k5eAaImAgInS	následovat
přemrštěný	přemrštěný	k2eAgInSc1d1	přemrštěný
seznam	seznam	k1gInSc1	seznam
požadavků	požadavek	k1gInPc2	požadavek
jak	jak	k8xS	jak
strategických	strategický	k2eAgFnPc2d1	strategická
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vojenského	vojenský	k2eAgInSc2d1	vojenský
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
seznam	seznam	k1gInSc1	seznam
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
Ciano	Ciano	k6eAd1	Ciano
"	"	kIx"	"
<g/>
porazil	porazit	k5eAaPmAgMnS	porazit
i	i	k9	i
býka	býk	k1gMnSc4	býk
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
mohl	moct	k5eAaImAgMnS	moct
přečíst	přečíst	k5eAaPmF	přečíst
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
nakonec	nakonec	k6eAd1	nakonec
oznámil	oznámit	k5eAaPmAgMnS	oznámit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
že	že	k8xS	že
italskou	italský	k2eAgFnSc4d1	italská
účast	účast	k1gFnSc4	účast
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
italský	italský	k2eAgMnSc1d1	italský
diktátor	diktátor	k1gMnSc1	diktátor
svou	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
za	za	k7c4	za
"	"	kIx"	"
<g/>
stát	stát	k5eAaPmF	stát
válku	válka	k1gFnSc4	válka
nevedoucí	vedoucí	k2eNgInPc1d1	nevedoucí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
ani	ani	k8xC	ani
tento	tento	k3xDgInSc1	tento
vzdor	vzdor	k1gInSc1	vzdor
neměl	mít	k5eNaImAgInS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
německých	německý	k2eAgInPc6d1	německý
úspěších	úspěch	k1gInPc6	úspěch
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
Holandsku	Holandsko	k1gNnSc6	Holandsko
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc6	Belgie
a	a	k8xC	a
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
rozdrtila	rozdrtit	k5eAaPmAgFnS	rozdrtit
Francouze	Francouz	k1gMnPc4	Francouz
nakonec	nakonec	k9	nakonec
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
válku	válka	k1gFnSc4	válka
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
Británii	Británie	k1gFnSc4	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Učinil	učinit	k5eAaImAgMnS	učinit
tak	tak	k9	tak
z	z	k7c2	z
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
válka	válka	k1gFnSc1	válka
za	za	k7c4	za
pár	pár	k4xCyI	pár
týdnů	týden	k1gInPc2	týden
nejpozději	pozdě	k6eAd3	pozdě
měsíců	měsíc	k1gInPc2	měsíc
skončí	skončit	k5eAaPmIp3nS	skončit
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
účastí	účast	k1gFnSc7	účast
si	se	k3xPyFc3	se
zajistí	zajistit	k5eAaPmIp3nP	zajistit
možnost	možnost	k1gFnSc4	možnost
zasednout	zasednout	k5eAaPmF	zasednout
za	za	k7c4	za
jednací	jednací	k2eAgInSc4d1	jednací
stůl	stůl	k1gInSc4	stůl
s	s	k7c7	s
právy	právo	k1gNnPc7	právo
válečné	válečná	k1gFnSc2	válečná
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
aby	aby	kYmCp3nS	aby
nevypadal	vypadat	k5eNaImAgMnS	vypadat
jako	jako	k9	jako
šakal	šakal	k1gMnSc1	šakal
<g/>
,	,	kIx,	,
živící	živící	k2eAgMnSc1d1	živící
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
mu	on	k3xPp3gMnSc3	on
jeho	jeho	k3xOp3gMnSc1	jeho
silnější	silný	k2eAgMnSc1d2	silnější
partner	partner	k1gMnSc1	partner
nechá	nechat	k5eAaPmIp3nS	nechat
při	při	k7c6	při
polykání	polykání	k1gNnSc6	polykání
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
označil	označit	k5eAaPmAgMnS	označit
tuto	tento	k3xDgFnSc4	tento
válku	válka	k1gFnSc4	válka
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
paralelní	paralelní	k2eAgInSc1d1	paralelní
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
proti	proti	k7c3	proti
stejným	stejný	k2eAgMnPc3d1	stejný
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
pro	pro	k7c4	pro
italské	italský	k2eAgInPc4d1	italský
zájmy	zájem	k1gInPc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
se	se	k3xPyFc4	se
vskutku	vskutku	k9	vskutku
zapojila	zapojit	k5eAaPmAgNnP	zapojit
aktivně	aktivně	k6eAd1	aktivně
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zcela	zcela	k6eAd1	zcela
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
než	než	k8xS	než
si	se	k3xPyFc3	se
italský	italský	k2eAgMnSc1d1	italský
diktátor	diktátor	k1gMnSc1	diktátor
představoval	představovat	k5eAaImAgMnS	představovat
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
jeho	jeho	k3xOp3gNnSc4	jeho
první	první	k4xOgNnSc4	první
bojové	bojový	k2eAgNnSc4d1	bojové
střetnutí	střetnutí	k1gNnSc4	střetnutí
s	s	k7c7	s
Francouzi	Francouz	k1gMnPc7	Francouz
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
bídně	bídně	k6eAd1	bídně
a	a	k8xC	a
do	do	k7c2	do
francouzské	francouzský	k2eAgFnSc2d1	francouzská
kapitulace	kapitulace	k1gFnSc2	kapitulace
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Italové	Ital	k1gMnPc1	Ital
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
otřeseným	otřesený	k2eAgMnSc7d1	otřesený
nepřítelem	nepřítel	k1gMnSc7	nepřítel
jen	jen	k6eAd1	jen
pohraniční	pohraniční	k2eAgFnSc1d1	pohraniční
město	město	k1gNnSc4	město
Menton	menton	k1gInSc1	menton
a	a	k8xC	a
několik	několik	k4yIc1	několik
alpských	alpský	k2eAgNnPc2d1	alpské
údolí	údolí	k1gNnPc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
tak	tak	k9	tak
kromě	kromě	k7c2	kromě
Mentonu	menton	k1gInSc2	menton
nezískala	získat	k5eNaPmAgFnS	získat
žádná	žádný	k3yNgNnPc4	žádný
území	území	k1gNnPc4	území
a	a	k8xC	a
jen	jen	k9	jen
Hitlerovým	Hitlerův	k2eAgNnSc7d1	Hitlerovo
přispěním	přispění	k1gNnSc7	přispění
musela	muset	k5eAaImAgFnS	muset
francouzská	francouzský	k2eAgFnSc1d1	francouzská
delegace	delegace	k1gFnSc1	delegace
souhlasit	souhlasit	k5eAaImF	souhlasit
s	s	k7c7	s
demilitarizovanou	demilitarizovaný	k2eAgFnSc7d1	demilitarizovaná
zónou	zóna	k1gFnSc7	zóna
na	na	k7c6	na
francouzsko-italské	francouzskotalský	k2eAgFnSc6d1	francouzsko-italská
hranici	hranice	k1gFnSc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
nic	nic	k3yNnSc4	nic
lépe	dobře	k6eAd2	dobře
si	se	k3xPyFc3	se
italská	italský	k2eAgFnSc1d1	italská
armáda	armáda	k1gFnSc1	armáda
nevedla	vést	k5eNaImAgFnS	vést
proti	proti	k7c3	proti
Britům	Brit	k1gMnPc3	Brit
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1940	[number]	k4	1940
sice	sice	k8xC	sice
hrdě	hrdě	k6eAd1	hrdě
oznámil	oznámit	k5eAaPmAgMnS	oznámit
světu	svět	k1gInSc3	svět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
armáda	armáda	k1gFnSc1	armáda
porazila	porazit	k5eAaPmAgFnS	porazit
Brity	Brit	k1gMnPc4	Brit
a	a	k8xC	a
dobyla	dobýt	k5eAaPmAgFnS	dobýt
britské	britský	k2eAgNnSc4d1	Britské
Somálsko	Somálsko	k1gNnSc4	Somálsko
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
nyní	nyní	k6eAd1	nyní
bude	být	k5eAaImBp3nS	být
blokovat	blokovat	k5eAaImF	blokovat
zásobování	zásobování	k1gNnSc4	zásobování
britských	britský	k2eAgNnPc2d1	Britské
vojsk	vojsko	k1gNnPc2	vojsko
přes	přes	k7c4	přes
Rudé	rudý	k2eAgNnSc4d1	Rudé
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
jediný	jediný	k2eAgInSc1d1	jediný
skutečný	skutečný	k2eAgInSc1d1	skutečný
úspěch	úspěch	k1gInSc1	úspěch
jeho	jeho	k3xOp3gNnPc2	jeho
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1940	[number]	k4	1940
překročila	překročit	k5eAaPmAgFnS	překročit
10	[number]	k4	10
<g/>
.	.	kIx.	.
italská	italský	k2eAgFnSc1d1	italská
armáda	armáda	k1gFnSc1	armáda
egyptské	egyptský	k2eAgFnSc2d1	egyptská
hranice	hranice	k1gFnSc2	hranice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
dnech	den	k1gInPc6	den
a	a	k8xC	a
sto	sto	k4xCgNnSc4	sto
kilometrech	kilometr	k1gInPc6	kilometr
zastavila	zastavit	k5eAaPmAgFnS	zastavit
v	v	k7c4	v
Sídí	Sídí	k2eAgNnSc4d1	Sídí
Barrání	Barrání	k1gNnSc4	Barrání
<g/>
.	.	kIx.	.
</s>
<s>
Maršál	maršál	k1gMnSc1	maršál
Graziani	Grazian	k1gMnPc1	Grazian
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
velí	velet	k5eAaImIp3nS	velet
sice	sice	k8xC	sice
početné	početný	k2eAgNnSc1d1	početné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mizerně	mizerně	k6eAd1	mizerně
vyzbrojené	vyzbrojený	k2eAgFnSc3d1	vyzbrojená
a	a	k8xC	a
vycvičené	vycvičený	k2eAgFnSc3d1	vycvičená
armádě	armáda	k1gFnSc3	armáda
s	s	k7c7	s
nedostatečnými	dostatečný	k2eNgFnPc7d1	nedostatečná
zásobami	zásoba	k1gFnPc7	zásoba
kategoricky	kategoricky	k6eAd1	kategoricky
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
limitu	limita	k1gFnSc4	limita
bojových	bojový	k2eAgFnPc2d1	bojová
operací	operace	k1gFnPc2	operace
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
vyžíval	vyžívat	k5eAaImAgInS	vyžívat
psaním	psaní	k1gNnSc7	psaní
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
žádal	žádat	k5eAaImAgMnS	žádat
jak	jak	k9	jak
posily	posila	k1gFnPc4	posila
a	a	k8xC	a
i	i	k9	i
zásoby	zásoba	k1gFnPc4	zásoba
a	a	k8xC	a
bojovou	bojový	k2eAgFnSc4d1	bojová
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1940	[number]	k4	1940
osobně	osobně	k6eAd1	osobně
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
tak	tak	k9	tak
dost	dost	k6eAd1	dost
napjaté	napjatý	k2eAgNnSc1d1	napjaté
zásobování	zásobování	k1gNnSc1	zásobování
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
přetíženo	přetížen	k2eAgNnSc1d1	přetíženo
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c4	na
radu	rada	k1gFnSc4	rada
Ciana	Cian	k1gMnSc2	Cian
a	a	k8xC	a
italského	italský	k2eAgMnSc2d1	italský
ambasadora	ambasador	k1gMnSc2	ambasador
v	v	k7c6	v
Tiraně	Tirana	k1gFnSc6	Tirana
Francesca	Francescus	k1gMnSc4	Francescus
Jacomoniho	Jacomoni	k1gMnSc4	Jacomoni
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1940	[number]	k4	1940
na	na	k7c4	na
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
generál	generál	k1gMnSc1	generál
Sebastiano	Sebastiana	k1gFnSc5	Sebastiana
Visconti	Visconti	k1gNnSc4	Visconti
Prasca	Prascus	k1gMnSc4	Prascus
sebevědomě	sebevědomě	k6eAd1	sebevědomě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
k	k	k7c3	k
záboru	zábor	k1gInSc2	zábor
Řecka	Řecko	k1gNnSc2	Řecko
stačí	stačit	k5eAaBmIp3nS	stačit
jeho	jeho	k3xOp3gNnSc2	jeho
šest	šest	k4xCc1	šest
převážně	převážně	k6eAd1	převážně
binárních	binární	k2eAgFnPc2d1	binární
divizí	divize	k1gFnPc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
však	však	k9	však
italský	italský	k2eAgInSc1d1	italský
nástup	nástup	k1gInSc1	nástup
do	do	k7c2	do
týdne	týden	k1gInSc2	týden
zastavili	zastavit	k5eAaPmAgMnP	zastavit
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1940	[number]	k4	1940
zahájili	zahájit	k5eAaPmAgMnP	zahájit
protiofenzívu	protiofenzíva	k1gFnSc4	protiofenzíva
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
postoupili	postoupit	k5eAaPmAgMnP	postoupit
a	a	k8xC	a
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
Italy	Ital	k1gMnPc4	Ital
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
armáda	armáda	k1gFnSc1	armáda
pak	pak	k6eAd1	pak
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
útoku	útok	k1gInSc6	útok
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
ledna	leden	k1gInSc2	leden
1941	[number]	k4	1941
obsadila	obsadit	k5eAaPmAgFnS	obsadit
část	část	k1gFnSc1	část
jižní	jižní	k2eAgFnSc2d1	jižní
Albánie	Albánie	k1gFnSc2	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Definitivní	definitivní	k2eAgInSc1d1	definitivní
konec	konec	k1gInSc1	konec
"	"	kIx"	"
<g/>
paralelní	paralelní	k2eAgFnSc2d1	paralelní
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
mu	on	k3xPp3gMnSc3	on
připravila	připravit	k5eAaPmAgFnS	připravit
britská	britský	k2eAgFnSc1d1	britská
protiofenzíva	protiofenzíva	k1gFnSc1	protiofenzíva
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgNnPc1d1	Britské
vojska	vojsko	k1gNnPc1	vojsko
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generála	generál	k1gMnSc2	generál
Archibalda	Archibald	k1gMnSc2	Archibald
Wavella	Wavell	k1gMnSc2	Wavell
uštědřila	uštědřit	k5eAaPmAgFnS	uštědřit
Italům	Ital	k1gMnPc3	Ital
porážku	porážka	k1gFnSc4	porážka
u	u	k7c2	u
Sídí	Sídí	k1gFnSc2	Sídí
Barrání	Barrání	k1gNnSc2	Barrání
a	a	k8xC	a
při	při	k7c6	při
pronásledování	pronásledování	k1gNnSc6	pronásledování
ustupující	ustupující	k2eAgFnSc2d1	ustupující
10	[number]	k4	10
<g/>
.	.	kIx.	.
italské	italský	k2eAgFnSc2d1	italská
armády	armáda	k1gFnSc2	armáda
dobyla	dobýt	k5eAaPmAgFnS	dobýt
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1941	[number]	k4	1941
Bardii	Bardium	k1gNnPc7	Bardium
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
strategicky	strategicky	k6eAd1	strategicky
důležitý	důležitý	k2eAgInSc1d1	důležitý
přístav	přístav	k1gInSc1	přístav
Tobrúk	Tobrúka	k1gFnPc2	Tobrúka
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1941	[number]	k4	1941
byly	být	k5eAaImAgInP	být
zbytky	zbytek	k1gInPc1	zbytek
italských	italský	k2eAgNnPc2d1	italské
vojsk	vojsko	k1gNnPc2	vojsko
obklíčeny	obklíčen	k2eAgInPc1d1	obklíčen
a	a	k8xC	a
donuceny	donucen	k2eAgInPc1d1	donucen
ke	k	k7c3	k
kapitulaci	kapitulace	k1gFnSc3	kapitulace
u	u	k7c2	u
Beda	Beda	k1gMnSc1	Beda
Fomm	Fommo	k1gNnPc2	Fommo
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
Kyrenaika	Kyrenaika	k1gFnSc1	Kyrenaika
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
britských	britský	k2eAgFnPc2d1	britská
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
nakonec	nakonec	k6eAd1	nakonec
svému	svůj	k3xOyFgMnSc3	svůj
italskému	italský	k2eAgMnSc3d1	italský
spojenci	spojenec	k1gMnSc3	spojenec
musel	muset	k5eAaImAgMnS	muset
pomoci	pomoct	k5eAaPmF	pomoct
z	z	k7c2	z
nejhoršího	zlý	k2eAgMnSc2d3	nejhorší
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Tripolisu	Tripolis	k1gInSc2	Tripolis
dorazil	dorazit	k5eAaPmAgMnS	dorazit
generál	generál	k1gMnSc1	generál
Erwin	Erwin	k1gMnSc1	Erwin
Rommel	Rommel	k1gMnSc1	Rommel
s	s	k7c7	s
prvními	první	k4xOgInPc7	první
jednotky	jednotka	k1gFnPc1	jednotka
svého	svůj	k3xOyFgInSc2	svůj
Afrikakorpsu	Afrikakorps	k1gInSc2	Afrikakorps
a	a	k8xC	a
Řecko	Řecko	k1gNnSc1	Řecko
bylo	být	k5eAaImAgNnS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1941	[number]	k4	1941
napadeno	napadnout	k5eAaPmNgNnS	napadnout
a	a	k8xC	a
okupováno	okupovat	k5eAaBmNgNnS	okupovat
německou	německý	k2eAgFnSc7d1	německá
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
1941	[number]	k4	1941
tak	tak	k9	tak
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
svém	svůj	k3xOyFgMnSc6	svůj
německém	německý	k2eAgMnSc6d1	německý
spojenci	spojenec	k1gMnSc6	spojenec
naprosto	naprosto	k6eAd1	naprosto
závislý	závislý	k2eAgMnSc1d1	závislý
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
napadlo	napadnout	k5eAaPmAgNnS	napadnout
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc4	Německo
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
k	k	k7c3	k
boji	boj	k1gInSc3	boj
připojil	připojit	k5eAaPmAgMnS	připojit
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Sovětům	Sovět	k1gMnPc3	Sovět
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
japonském	japonský	k2eAgInSc6d1	japonský
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Pearl	Pearl	k1gInSc4	Pearl
Harbor	Harbora	k1gFnPc2	Harbora
společně	společně	k6eAd1	společně
s	s	k7c7	s
Adolfem	Adolf	k1gMnSc7	Adolf
Hitlerem	Hitler	k1gMnSc7	Hitler
podpořil	podpořit	k5eAaPmAgMnS	podpořit
Japonsko	Japonsko	k1gNnSc4	Japonsko
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1941	[number]	k4	1941
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
oba	dva	k4xCgInPc4	dva
válku	válka	k1gFnSc4	válka
i	i	k8xC	i
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Italští	italský	k2eAgMnPc1d1	italský
vojáci	voják	k1gMnPc1	voják
nadále	nadále	k6eAd1	nadále
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
pod	pod	k7c7	pod
německým	německý	k2eAgNnSc7d1	německé
velením	velení	k1gNnSc7	velení
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1943	[number]	k4	1943
zahájili	zahájit	k5eAaPmAgMnP	zahájit
Spojenci	spojenec	k1gMnPc1	spojenec
invazi	invaze	k1gFnSc4	invaze
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
sešla	sejít	k5eAaPmAgFnS	sejít
fašistická	fašistický	k2eAgFnSc1d1	fašistická
Velká	velký	k2eAgFnSc1d1	velká
rada	rada	k1gFnSc1	rada
za	za	k7c2	za
předsednictví	předsednictví	k1gNnSc2	předsednictví
hraběte	hrabě	k1gMnSc2	hrabě
Grandiho	Grandi	k1gMnSc2	Grandi
a	a	k8xC	a
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
19	[number]	k4	19
hlasů	hlas	k1gInPc2	hlas
ku	k	k7c3	k
7	[number]	k4	7
mu	on	k3xPp3gMnSc3	on
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
hlasoval	hlasovat	k5eAaImAgMnS	hlasovat
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
zeť	zeť	k1gMnSc1	zeť
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
Galeazzo	Galeazza	k1gFnSc5	Galeazza
Ciano	Ciano	k1gNnSc4	Ciano
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byl	být	k5eAaImAgMnS	být
sesazen	sesazen	k2eAgMnSc1d1	sesazen
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
na	na	k7c4	na
rozkaz	rozkaz	k1gInSc4	rozkaz
krále	král	k1gMnSc2	král
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
v	v	k7c6	v
horském	horský	k2eAgInSc6d1	horský
hotelu	hotel	k1gInSc6	hotel
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Gran	Grana	k1gFnPc2	Grana
Sasso	Sassa	k1gFnSc5	Sassa
v	v	k7c6	v
Abruzii	Abruzie	k1gFnSc4	Abruzie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgInS	zůstat
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
osvobozen	osvobodit	k5eAaPmNgMnS	osvobodit
výsadkáři	výsadkář	k1gMnPc7	výsadkář
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
dopraven	dopravna	k1gFnPc2	dopravna
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
(	(	kIx(	(
<g/>
komandu	komando	k1gNnSc6	komando
velel	velet	k5eAaImAgMnS	velet
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Otto	Otto	k1gMnSc1	Otto
Skorzeny	Skorzen	k2eAgInPc1d1	Skorzen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
ustavil	ustavit	k5eAaPmAgMnS	ustavit
na	na	k7c6	na
italském	italský	k2eAgNnSc6d1	italské
území	území	k1gNnSc6	území
ovládaném	ovládaný	k2eAgNnSc6d1	ovládané
Německem	Německo	k1gNnSc7	Německo
diktaturu	diktatura	k1gFnSc4	diktatura
tzv.	tzv.	kA	tzv.
Italskou	italský	k2eAgFnSc4d1	italská
sociální	sociální	k2eAgFnSc4d1	sociální
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
dle	dle	k7c2	dle
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
Republika	republika	k1gFnSc1	republika
Saló	Saló	k1gFnPc7	Saló
<g/>
.	.	kIx.	.
</s>
<s>
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
se	se	k3xPyFc4	se
naposledy	naposledy	k6eAd1	naposledy
pokusil	pokusit	k5eAaPmAgMnS	pokusit
získat	získat	k5eAaPmF	získat
si	se	k3xPyFc3	se
popularitu	popularita	k1gFnSc4	popularita
svým	svůj	k3xOyFgInSc7	svůj
programem	program	k1gInSc7	program
socializace	socializace	k1gFnSc2	socializace
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
formoval	formovat	k5eAaImAgMnS	formovat
novou	nový	k2eAgFnSc4d1	nová
republikánskou	republikánský	k2eAgFnSc4d1	republikánská
fašistickou	fašistický	k2eAgFnSc4d1	fašistická
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Ústavu	ústava	k1gFnSc4	ústava
republiky	republika	k1gFnSc2	republika
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
sepsal	sepsat	k5eAaPmAgMnS	sepsat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
komunista	komunista	k1gMnSc1	komunista
a	a	k8xC	a
Leninův	Leninův	k2eAgMnSc1d1	Leninův
přítel	přítel	k1gMnSc1	přítel
Nicolo	Nicola	k1gFnSc5	Nicola
Bombacci	Bombacce	k1gMnSc6	Bombacce
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
fašistům	fašista	k1gMnPc3	fašista
až	až	k8xS	až
během	během	k7c2	během
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Socializace	socializace	k1gFnSc1	socializace
spočívala	spočívat	k5eAaImAgFnS	spočívat
ve	v	k7c6	v
znárodnění	znárodnění	k1gNnSc6	znárodnění
firem	firma	k1gFnPc2	firma
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedním	jeden	k4xCgInSc7	jeden
stem	sto	k4xCgNnSc7	sto
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
brojil	brojit	k5eAaImAgMnS	brojit
proti	proti	k7c3	proti
buržoazii	buržoazie	k1gFnSc3	buržoazie
<g/>
.	.	kIx.	.
</s>
<s>
Prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Odkazuji	odkazovat	k5eAaImIp1nS	odkazovat
republiku	republika	k1gFnSc4	republika
republikánům	republikán	k1gMnPc3	republikán
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
monarchistům	monarchista	k1gMnPc3	monarchista
<g/>
,	,	kIx,	,
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
sociální	sociální	k2eAgFnSc2d1	sociální
reformy	reforma	k1gFnSc2	reforma
socialistům	socialist	k1gMnPc3	socialist
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
středním	střední	k2eAgFnPc3d1	střední
třídám	třída	k1gFnPc3	třída
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
republika	republika	k1gFnSc1	republika
také	také	k6eAd1	také
shromažďovala	shromažďovat	k5eAaImAgFnS	shromažďovat
Židy	Žid	k1gMnPc4	Žid
a	a	k8xC	a
posílala	posílat	k5eAaImAgFnS	posílat
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
je	být	k5eAaImIp3nS	být
Mussoliniho	Mussolini	k1gMnSc4	Mussolini
režim	režim	k1gInSc4	režim
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
míře	míra	k1gFnSc6	míra
dříve	dříve	k6eAd2	dříve
neperzekvoval	perzekvovat	k5eNaImAgMnS	perzekvovat
<g/>
.	.	kIx.	.
</s>
<s>
Vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
si	se	k3xPyFc3	se
rovněž	rovněž	k9	rovněž
své	svůj	k3xOyFgInPc4	svůj
účty	účet	k1gInPc4	účet
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
zrádci	zrádce	k1gMnPc7	zrádce
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1944	[number]	k4	1944
stanulo	stanout	k5eAaPmAgNnS	stanout
před	před	k7c7	před
soudem	soud	k1gInSc7	soud
ve	v	k7c6	v
Veroně	Verona	k1gFnSc6	Verona
pět	pět	k4xCc4	pět
členů	člen	k1gInPc2	člen
Velké	velký	k2eAgFnSc2d1	velká
fašistické	fašistický	k2eAgFnSc2d1	fašistická
rady	rada	k1gFnSc2	rada
včetně	včetně	k7c2	včetně
duceho	duce	k1gMnSc2	duce
zetě	zeť	k1gMnSc2	zeť
Ciana	Cian	k1gMnSc2	Cian
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1944	[number]	k4	1944
byli	být	k5eAaImAgMnP	být
Ciano	Ciano	k1gNnSc4	Ciano
<g/>
,	,	kIx,	,
maršál	maršál	k1gMnSc1	maršál
de	de	k?	de
Bono	bona	k1gFnSc5	bona
<g/>
,	,	kIx,	,
někdejší	někdejší	k2eAgMnSc1d1	někdejší
velitel	velitel	k1gMnSc1	velitel
squadristů	squadrista	k1gMnPc2	squadrista
Marinetti	Marinetti	k1gNnSc2	Marinetti
a	a	k8xC	a
ministři	ministr	k1gMnPc1	ministr
Gottardi	Gottard	k1gMnPc1	Gottard
a	a	k8xC	a
Pareschi	Paresch	k1gMnPc1	Paresch
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
veronském	veronský	k2eAgInSc6d1	veronský
procesu	proces	k1gInSc6	proces
odsouzeni	odsouzen	k2eAgMnPc1d1	odsouzen
a	a	k8xC	a
popraveni	popravit	k5eAaPmNgMnP	popravit
ranou	raný	k2eAgFnSc7d1	raná
do	do	k7c2	do
týlu	týl	k1gInSc2	týl
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
nově	nově	k6eAd1	nově
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
Italská	italský	k2eAgFnSc1d1	italská
republika	republika	k1gFnSc1	republika
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
<g/>
,	,	kIx,	,
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Milána	Milán	k1gInSc2	Milán
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
už	už	k9	už
tehdy	tehdy	k6eAd1	tehdy
spěšně	spěšně	k6eAd1	spěšně
stahovala	stahovat	k5eAaImAgNnP	stahovat
německá	německý	k2eAgNnPc1d1	německé
vojska	vojsko	k1gNnPc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
o	o	k7c4	o
kapitulaci	kapitulace	k1gFnSc4	kapitulace
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
opustil	opustit	k5eAaPmAgMnS	opustit
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
své	svůj	k3xOyFgFnSc2	svůj
milenky	milenka	k1gFnSc2	milenka
Clary	Clara	k1gFnSc2	Clara
Petacci	Petacce	k1gFnSc3	Petacce
bombardované	bombardovaný	k2eAgNnSc1d1	bombardované
Miláno	Milán	k2eAgNnSc1d1	Miláno
a	a	k8xC	a
v	v	k7c6	v
koloně	kolona	k1gFnSc6	kolona
aut	auto	k1gNnPc2	auto
prchal	prchat	k5eAaImAgInS	prchat
do	do	k7c2	do
Coma	Com	k1gInSc2	Com
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
kolona	kolona	k1gFnSc1	kolona
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
německému	německý	k2eAgInSc3d1	německý
oddílu	oddíl	k1gInSc3	oddíl
protiletecké	protiletecký	k2eAgFnSc2d1	protiletecká
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
uprchnout	uprchnout	k5eAaPmF	uprchnout
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gFnSc7	jeho
ochranou	ochrana	k1gFnSc7	ochrana
do	do	k7c2	do
Innsbrucku	Innsbruck	k1gInSc2	Innsbruck
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oddíl	oddíl	k1gInSc1	oddíl
byl	být	k5eAaImAgInS	být
zastaven	zastavit	k5eAaPmNgInS	zastavit
52	[number]	k4	52
<g/>
.	.	kIx.	.
partyzánskou	partyzánský	k2eAgFnSc7d1	Partyzánská
brigádou	brigáda	k1gFnSc7	brigáda
Luigi	Luig	k1gFnSc2	Luig
Clerici	Clerice	k1gFnSc6	Clerice
Garibaldi	Garibald	k1gMnPc1	Garibald
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
Italové	Ital	k1gMnPc1	Ital
dostali	dostat	k5eAaPmAgMnP	dostat
příkaz	příkaz	k1gInSc4	příkaz
zůstat	zůstat	k5eAaPmF	zůstat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Němci	Němec	k1gMnPc1	Němec
jeli	jet	k5eAaImAgMnP	jet
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
oblékl	obléct	k5eAaPmAgMnS	obléct
německou	německý	k2eAgFnSc4d1	německá
uniformu	uniforma	k1gFnSc4	uniforma
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Donga	Dong	k1gMnSc2	Dong
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgNnP	být
německá	německý	k2eAgNnPc1d1	německé
auta	auto	k1gNnPc1	auto
prohlédnuta	prohlédnut	k2eAgNnPc1d1	prohlédnuto
a	a	k8xC	a
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
tak	tak	k8xS	tak
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
objeven	objevit	k5eAaPmNgInS	objevit
a	a	k8xC	a
zatčen	zatknout	k5eAaPmNgInS	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
měli	mít	k5eAaImAgMnP	mít
členové	člen	k1gMnPc1	člen
Národního	národní	k2eAgInSc2d1	národní
osvobozovacího	osvobozovací	k2eAgInSc2d1	osvobozovací
výboru	výbor	k1gInSc2	výbor
severní	severní	k2eAgFnSc2d1	severní
Itálie	Itálie	k1gFnSc2	Itálie
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
dalším	další	k2eAgInSc6d1	další
osudu	osud	k1gInSc6	osud
jasno	jasno	k6eAd1	jasno
<g/>
.	.	kIx.	.
</s>
<s>
Walter	Walter	k1gMnSc1	Walter
Audisio	Audisio	k1gMnSc1	Audisio
<g/>
,	,	kIx,	,
antifašistický	antifašistický	k2eAgMnSc1d1	antifašistický
bojovník	bojovník	k1gMnSc1	bojovník
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
plukovník	plukovník	k1gMnSc1	plukovník
Valerio	Valerio	k1gMnSc1	Valerio
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
příkaz	příkaz	k1gInSc4	příkaz
jej	on	k3xPp3gInSc4	on
popravit	popravit	k5eAaPmF	popravit
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
vtrhl	vtrhnout	k5eAaPmAgInS	vtrhnout
Valerio	Valerio	k6eAd1	Valerio
do	do	k7c2	do
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
ložnice	ložnice	k1gFnSc2	ložnice
v	v	k7c6	v
selském	selský	k2eAgNnSc6d1	selské
stavení	stavení	k1gNnSc6	stavení
u	u	k7c2	u
Bonzaniga	Bonzanig	k1gMnSc2	Bonzanig
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
Mussolinimu	Mussolinim	k1gInSc3	Mussolinim
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
milence	milenka	k1gFnSc3	milenka
pomoci	pomoct	k5eAaPmF	pomoct
k	k	k7c3	k
útěku	útěk	k1gInSc3	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
pak	pak	k6eAd1	pak
odjeli	odjet	k5eAaPmAgMnP	odjet
autem	auto	k1gNnSc7	auto
k	k	k7c3	k
vile	vila	k1gFnSc3	vila
Belmonte	Belmont	k1gMnSc5	Belmont
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
Clarou	Clara	k1gFnSc7	Clara
Petacci	Petacce	k1gMnPc1	Petacce
opustili	opustit	k5eAaPmAgMnP	opustit
auto	auto	k1gNnSc4	auto
a	a	k8xC	a
následně	následně	k6eAd1	následně
byli	být	k5eAaImAgMnP	být
zastřeleni	zastřelit	k5eAaPmNgMnP	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
mrtvá	mrtvý	k2eAgNnPc1d1	mrtvé
těla	tělo	k1gNnPc1	tělo
byla	být	k5eAaImAgNnP	být
převezena	převézt	k5eAaPmNgNnP	převézt
do	do	k7c2	do
Azzony	Azzona	k1gFnSc2	Azzona
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
pověšena	pověsit	k5eAaPmNgFnS	pověsit
hlavou	hlava	k1gFnSc7	hlava
dolů	dol	k1gInPc2	dol
na	na	k7c6	na
nosnících	nosník	k1gInPc6	nosník
rozbombardované	rozbombardovaný	k2eAgFnSc2d1	rozbombardovaná
čerpací	čerpací	k2eAgFnSc2d1	čerpací
stanice	stanice	k1gFnSc2	stanice
na	na	k7c6	na
Piazzale	Piazzala	k1gFnSc6	Piazzala
Loreto	Loreto	k1gNnSc1	Loreto
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
naložení	naložení	k1gNnSc4	naložení
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
mrtvým	mrtvý	k2eAgNnSc7d1	mrtvé
tělem	tělo	k1gNnSc7	tělo
mělo	mít	k5eAaImAgNnS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
se	se	k3xPyFc4	se
Benito	Benita	k1gMnSc5	Benita
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
milenkou	milenka	k1gFnSc7	milenka
Rachel	Rachel	k1gInSc4	Rachel
Guidi	Guid	k1gMnPc1	Guid
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
žil	žít	k5eAaImAgMnS	žít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
dceru	dcera	k1gFnSc4	dcera
Eddu	Edda	k1gFnSc4	Edda
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
–	–	k?	–
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
manželství	manželství	k1gNnSc6	manželství
narodili	narodit	k5eAaPmAgMnP	narodit
potomci	potomek	k1gMnPc1	potomek
<g/>
:	:	kIx,	:
Vittorio	Vittorio	k1gMnSc1	Vittorio
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
–	–	k?	–
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bruno	Bruno	k1gMnSc1	Bruno
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
–	–	k?	–
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Romano	Romano	k1gMnSc1	Romano
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
–	–	k?	–
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
–	–	k?	–
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
dcera	dcera	k1gFnSc1	dcera
Edda	Edda	k1gFnSc1	Edda
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
hraběte	hrabě	k1gMnSc2	hrabě
Galeazza	Galeazz	k1gMnSc2	Galeazz
Ciana	Cian	k1gMnSc2	Cian
<g/>
,	,	kIx,	,
italského	italský	k2eAgMnSc2d1	italský
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
dal	dát	k5eAaPmAgMnS	dát
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
Benito	Benita	k1gMnSc5	Benita
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
popravit	popravit	k5eAaPmF	popravit
za	za	k7c4	za
velezradu	velezrada	k1gFnSc4	velezrada
<g/>
.	.	kIx.	.
</s>
<s>
Vittorio	Vittorio	k6eAd1	Vittorio
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
byl	být	k5eAaImAgInS	být
filmovým	filmový	k2eAgMnSc7d1	filmový
producentem	producent	k1gMnSc7	producent
<g/>
,	,	kIx,	,
Bruno	Bruno	k1gMnSc1	Bruno
zahynul	zahynout	k5eAaPmAgMnS	zahynout
při	při	k7c6	při
testování	testování	k1gNnSc6	testování
bombardéru	bombardér	k1gInSc2	bombardér
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
Romano	Romano	k1gMnSc1	Romano
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
byl	být	k5eAaImAgInS	být
uznávaným	uznávaný	k2eAgMnSc7d1	uznávaný
klavíristou	klavírista	k1gMnSc7	klavírista
a	a	k8xC	a
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Romano	Romano	k1gMnSc1	Romano
Full	Full	k1gMnSc1	Full
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
také	také	k9	také
knihu	kniha	k1gFnSc4	kniha
Můj	můj	k3xOp1gMnSc1	můj
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Duce	Duce	k1gFnSc1	Duce
<g/>
.	.	kIx.	.
</s>
<s>
Životopisnou	životopisný	k2eAgFnSc4d1	životopisná
knihu	kniha	k1gFnSc4	kniha
vydala	vydat	k5eAaPmAgFnS	vydat
i	i	k9	i
vdova	vdova	k1gFnSc1	vdova
po	po	k7c6	po
Benitu	Benit	k1gInSc6	Benit
Mussolinim	Mussolinim	k1gMnSc1	Mussolinim
Rachel	Rachel	k1gMnSc1	Rachel
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Můj	můj	k3xOp1gInSc4	můj
život	život	k1gInSc4	život
s	s	k7c7	s
Benitem	Benit	k1gInSc7	Benit
<g/>
.	.	kIx.	.
</s>
<s>
Příbuznou	příbuzná	k1gFnSc7	příbuzná
rodiny	rodina	k1gFnSc2	rodina
Mussoliniových	Mussoliniový	k2eAgInPc2d1	Mussoliniový
je	být	k5eAaImIp3nS	být
i	i	k9	i
herečka	herečka	k1gFnSc1	herečka
Sofia	Sofia	k1gFnSc1	Sofia
Lorenová	Lorenová	k1gFnSc1	Lorenová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
sňatkem	sňatek	k1gInSc7	sňatek
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
švagrovou	švagrová	k1gFnSc4	švagrová
Romana	Roman	k1gMnSc2	Roman
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
uveřejnil	uveřejnit	k5eAaPmAgMnS	uveřejnit
italský	italský	k2eAgMnSc1d1	italský
novinář	novinář	k1gMnSc1	novinář
Marco	Marco	k1gMnSc1	Marco
Zeni	Zene	k1gFnSc4	Zene
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Benito	Benit	k2eAgNnSc1d1	Benito
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
poprvé	poprvé	k6eAd1	poprvé
oženil	oženit	k5eAaPmAgMnS	oženit
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
s	s	k7c7	s
jistou	jistý	k2eAgFnSc7d1	jistá
Idou	Ida	k1gFnSc7	Ida
Dalserovou	Dalserův	k2eAgFnSc7d1	Dalserův
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
syna	syn	k1gMnSc4	syn
Benita	Benit	k1gMnSc4	Benit
Albiniega	Albinieg	k1gMnSc4	Albinieg
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
údajně	údajně	k6eAd1	údajně
veškeré	veškerý	k3xTgFnPc4	veškerý
stopy	stopa	k1gFnPc4	stopa
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
bigamii	bigamie	k1gFnSc6	bigamie
zahladit	zahladit	k5eAaPmF	zahladit
tajnou	tajný	k2eAgFnSc7d1	tajná
policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
</s>
<s>
Ida	Ida	k1gFnSc1	Ida
Dalserová	Dalserová	k1gFnSc1	Dalserová
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
Benito	Benit	k2eAgNnSc4d1	Benito
Albiniego	Albiniego	k1gNnSc4	Albiniego
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
měl	mít	k5eAaImAgInS	mít
během	během	k7c2	během
života	život	k1gInSc2	život
řadu	řada	k1gFnSc4	řada
milenek	milenka	k1gFnPc2	milenka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
Clara	Clara	k1gFnSc1	Clara
Petacci	Petacce	k1gFnSc4	Petacce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
mu	on	k3xPp3gNnSc3	on
byl	být	k5eAaImAgInS	být
udělen	udělit	k5eAaPmNgInS	udělit
Československem	Československo	k1gNnSc7	Československo
Řád	řád	k1gInSc1	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
<g/>
.	.	kIx.	.
</s>
<s>
Podnětem	podnět	k1gInSc7	podnět
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gNnSc4	jeho
kladný	kladný	k2eAgInSc1d1	kladný
postoj	postoj	k1gInSc1	postoj
k	k	k7c3	k
husitství	husitství	k1gNnSc3	husitství
a	a	k8xC	a
československým	československý	k2eAgFnPc3d1	Československá
legiím	legie	k1gFnPc3	legie
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
propagoval	propagovat	k5eAaImAgMnS	propagovat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
postoji	postoj	k1gInSc3	postoj
již	již	k6eAd1	již
nehlásil	hlásit	k5eNaImAgMnS	hlásit
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
jeho	jeho	k3xOp3gFnPc2	jeho
raných	raný	k2eAgFnPc2d1	raná
antiklerikálních	antiklerikální	k2eAgFnPc2d1	antiklerikální
knih	kniha	k1gFnPc2	kniha
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
ve	v	k7c6	v
fašistické	fašistický	k2eAgFnSc6d1	fašistická
Itálii	Itálie	k1gFnSc6	Itálie
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
