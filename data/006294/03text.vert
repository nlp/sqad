<s>
Hyde	Hyde	k6eAd1	Hyde
Park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
parků	park	k1gInPc2	park
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Londýna	Londýn	k1gInSc2	Londýn
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
Westminster	Westminster	k1gInSc4	Westminster
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
královských	královský	k2eAgInPc2d1	královský
parků	park	k1gInPc2	park
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
rozdělen	rozdělen	k2eAgInSc1d1	rozdělen
Serpentinovým	serpentinový	k2eAgNnSc7d1	serpentinový
jezerem	jezero	k1gNnSc7	jezero
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
poloviny	polovina	k1gFnPc4	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Hyde	Hyde	k6eAd1	Hyde
Park	park	k1gInSc1	park
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Kensingtonskými	Kensingtonský	k2eAgFnPc7d1	Kensingtonský
zahradami	zahrada	k1gFnPc7	zahrada
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
Hyde	Hyd	k1gFnSc2	Hyd
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
parky	park	k1gInPc7	park
tvoří	tvořit	k5eAaImIp3nS	tvořit
West	West	k2eAgInSc1d1	West
Carriage	Carriage	k1gInSc1	Carriage
Drive	drive	k1gInSc1	drive
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
Hyde	Hyd	k1gFnSc2	Hyd
Parku	park	k1gInSc2	park
činí	činit	k5eAaImIp3nS	činit
1,4	[number]	k4	1,4
km2	km2	k4	km2
a	a	k8xC	a
Kensingtonských	Kensingtonský	k2eAgFnPc2d1	Kensingtonský
zahrad	zahrada	k1gFnPc2	zahrada
1,1	[number]	k4	1,1
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
část	část	k1gFnSc1	část
parku	park	k1gInSc2	park
patřila	patřit	k5eAaImAgFnS	patřit
panství	panství	k1gNnSc4	panství
Ebury	Ebura	k1gFnSc2	Ebura
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
byla	být	k5eAaImAgFnS	být
jeden	jeden	k4xCgInSc4	jeden
hide	hide	k1gFnPc2	hide
(	(	kIx(	(
<g/>
plocha	plocha	k1gFnSc1	plocha
0,24	[number]	k4	0,24
<g/>
–	–	k?	–
<g/>
0,49	[number]	k4	0,49
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
slova	slovo	k1gNnSc2	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
název	název	k1gInSc1	název
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Pozemek	pozemek	k1gInSc4	pozemek
získal	získat	k5eAaPmAgMnS	získat
Jindřich	Jindřich	k1gMnSc1	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1536	[number]	k4	1536
od	od	k7c2	od
mnichů	mnich	k1gMnPc2	mnich
Westminsterského	Westminsterský	k2eAgNnSc2d1	Westminsterské
opatství	opatství	k1gNnSc2	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
většiny	většina	k1gFnSc2	většina
vybavení	vybavení	k1gNnSc2	vybavení
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
architekt	architekt	k1gMnSc1	architekt
Decimus	Decimus	k1gMnSc1	Decimus
Burton	Burton	k1gInSc4	Burton
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
původním	původní	k2eAgNnSc7d1	původní
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stál	stát	k5eAaImAgInS	stát
Křišťálový	křišťálový	k2eAgInSc4d1	křišťálový
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
navržený	navržený	k2eAgInSc4d1	navržený
Josefem	Josef	k1gMnSc7	Josef
Paxtonem	Paxton	k1gInSc7	Paxton
pro	pro	k7c4	pro
Velkou	velký	k2eAgFnSc4d1	velká
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
se	se	k3xPyFc4	se
také	také	k9	také
stal	stát	k5eAaPmAgMnS	stát
dějištěm	dějiště	k1gNnSc7	dějiště
několika	několik	k4yIc2	několik
velkých	velký	k2eAgInPc2d1	velký
koncertů	koncert	k1gInPc2	koncert
včetně	včetně	k7c2	včetně
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stones	k1gInSc1	Stones
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pink	pink	k2eAgInSc1d1	pink
Floyd	Floyd	k1gInSc1	Floyd
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Queen	Queen	k1gInSc1	Queen
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
a	a	k8xC	a
Red	Red	k1gFnSc1	Red
Hot	hot	k0	hot
Chili	Chile	k1gFnSc3	Chile
Peppers	Peppers	k1gInSc1	Peppers
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konal	konat	k5eAaImAgInS	konat
koncert	koncert	k1gInSc1	koncert
Live	Liv	k1gInSc2	Liv
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
návrhu	návrh	k1gInSc2	návrh
Velké	velký	k2eAgFnSc2d1	velká
brány	brána	k1gFnSc2	brána
byl	být	k5eAaImAgInS	být
Decimus	Decimus	k1gInSc1	Decimus
Burton	Burton	k1gInSc1	Burton
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
několik	několik	k4yIc4	několik
ionských	ionský	k2eAgInPc2d1	ionský
sloupů	sloup	k1gInPc2	sloup
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
oblouky	oblouk	k1gInPc7	oblouk
pro	pro	k7c4	pro
vjezd	vjezd	k1gInSc4	vjezd
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
<g/>
.	.	kIx.	.
</s>
<s>
Brána	brána	k1gFnSc1	brána
je	být	k5eAaImIp3nS	být
vyrobena	vyrobit	k5eAaPmNgFnS	vyrobit
z	z	k7c2	z
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
bronzem	bronz	k1gInSc7	bronz
a	a	k8xC	a
uchycena	uchytit	k5eAaPmNgFnS	uchytit
do	do	k7c2	do
pilířů	pilíř	k1gInPc2	pilíř
kruhy	kruh	k1gInPc1	kruh
z	z	k7c2	z
materiálů	materiál	k1gInPc2	materiál
používaného	používaný	k2eAgInSc2d1	používaný
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ozdobena	ozdobit	k5eAaPmNgFnS	ozdobit
řeckými	řecký	k2eAgInPc7d1	řecký
zimolezovými	zimolezový	k2eAgInPc7d1	zimolezový
ornamenty	ornament	k1gInPc7	ornament
<g/>
.	.	kIx.	.
</s>
<s>
Speakers	Speakers	k1gInSc1	Speakers
<g/>
'	'	kIx"	'
Corner	Corner	k1gMnSc1	Corner
–	–	k?	–
veřejné	veřejný	k2eAgNnSc1d1	veřejné
prostranství	prostranství	k1gNnSc1	prostranství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
se	se	k3xPyFc4	se
na	na	k7c4	na
jakékoli	jakýkoli	k3yIgNnSc4	jakýkoli
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
poblíž	poblíž	k7c2	poblíž
Mramorového	mramorový	k2eAgInSc2d1	mramorový
oblouku	oblouk	k1gInSc2	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
možnosti	možnost	k1gFnSc2	možnost
zde	zde	k6eAd1	zde
volně	volně	k6eAd1	volně
promluvit	promluvit	k5eAaPmF	promluvit
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
i	i	k9	i
název	název	k1gInSc1	název
pořadu	pořad	k1gInSc2	pořad
ČT24	ČT24	k1gMnPc2	ČT24
Hyde	Hyd	k1gFnSc2	Hyd
park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Rotten	Rotten	k2eAgMnSc1d1	Rotten
Row	Row	k1gMnSc1	Row
–	–	k?	–
široká	široký	k2eAgFnSc1d1	široká
dráha	dráha	k1gFnSc1	dráha
vytvářejícií	vytvářejície	k1gFnPc2	vytvářejície
jižní	jižní	k2eAgFnSc4d1	jižní
hranici	hranice	k1gFnSc4	hranice
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
dříve	dříve	k6eAd2	dříve
pro	pro	k7c4	pro
projížďky	projížďka	k1gFnPc4	projížďka
na	na	k7c6	na
koních	kůň	k1gMnPc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Hyde	Hyde	k1gNnSc1	Hyde
Park	park	k1gInSc1	park
Corner	Corner	k1gMnSc1	Corner
–	–	k?	–
hlavní	hlavní	k2eAgFnSc1d1	hlavní
křižovatka	křižovatka	k1gFnSc1	křižovatka
parku	park	k1gInSc2	park
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
Diany	Diana	k1gFnSc2	Diana
<g/>
,	,	kIx,	,
princezny	princezna	k1gFnSc2	princezna
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
–	–	k?	–
oválná	oválný	k2eAgFnSc1d1	oválná
kamenná	kamenný	k2eAgFnSc1d1	kamenná
kruhová	kruhový	k2eAgFnSc1d1	kruhová
fontána	fontána	k1gFnSc1	fontána
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Serpentinového	serpentinový	k2eAgNnSc2d1	serpentinový
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
byla	být	k5eAaImAgFnS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Bombový	bombový	k2eAgInSc4d1	bombový
útok	útok	k1gInSc4	útok
v	v	k7c6	v
Hyde	Hyde	k1gNnSc6	Hyde
Parku	park	k1gInSc2	park
a	a	k8xC	a
Regent	regent	k1gMnSc1	regent
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Parku	park	k1gInSc3	park
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hyde	Hyd	k1gInSc2	Hyd
Park	park	k1gInSc1	park
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Dílo	dílo	k1gNnSc1	dílo
Anglické	anglický	k2eAgInPc1d1	anglický
listy	list	k1gInPc1	list
<g/>
/	/	kIx~	/
<g/>
Hyde	Hyde	k1gFnSc1	Hyde
Park	park	k1gInSc1	park
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
http://www.royalparks.gov.uk/parks/hyde_park/	[url]	k?	http://www.royalparks.gov.uk/parks/hyde_park/
–	–	k?	–
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
Hyde	Hyde	k1gNnSc2	Hyde
Parku	park	k1gInSc2	park
</s>
