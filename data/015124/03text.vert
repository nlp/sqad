<s>
Petr	Petr	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aragonský	aragonský	k2eAgInSc1d1
</s>
<s>
Petr	Petr	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aragonský	aragonský	k2eAgInSc1d1
</s>
<s>
Král	Král	k1gMnSc1
aragonský	aragonský	k2eAgMnSc1d1
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
1196	#num#	k4
–	–	k?
1213	#num#	k4
</s>
<s>
Korunovace	korunovace	k1gFnSc1
</s>
<s>
1205	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
červenec	červenec	k1gInSc1
1174	#num#	k4
</s>
<s>
Huesca	Huesca	k6eAd1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1213	#num#	k4
</s>
<s>
Muret	Muret	k1gMnSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
klášter	klášter	k1gInSc1
Sigena	Sigeno	k1gNnSc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Alfons	Alfons	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Jakub	Jakub	k1gMnSc1
I.	I.	kA
</s>
<s>
Manželka	manželka	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
z	z	k7c2
Montpellieru	Montpellier	k1gInSc2
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
MatyldaPetronilaSanchaJakub	MatyldaPetronilaSanchaJakub	k1gInSc1
I.	I.	kA
Aragonský	aragonský	k2eAgInSc1d1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Barcelonští	barcelonský	k2eAgMnPc1d1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Alfons	Alfons	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aragonský	aragonský	k2eAgInSc1d1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Sancha	Sancha	k1gFnSc1
Kastilská	kastilský	k2eAgFnSc1d1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Petr	Petr	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aragonský	aragonský	k2eAgInSc1d1
(	(	kIx(
<g/>
Pedro	Pedro	k1gNnSc1
II	II	kA
de	de	k?
Aragón	Aragón	k1gInSc1
<g/>
,	,	kIx,
el	ela	k1gFnPc2
Católico	Católico	k6eAd1
<g/>
,	,	kIx,
1174	#num#	k4
–	–	k?
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1213	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
král	král	k1gMnSc1
aragonský	aragonský	k2eAgMnSc1d1
a	a	k8xC
barcelonský	barcelonský	k2eAgMnSc1d1
hrabě	hrabě	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Petr	Petr	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
jako	jako	k9
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
aragonského	aragonský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Alfonse	Alfons	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
Sanchy	Sancha	k1gFnSc2
<g/>
,	,	kIx,
dcery	dcera	k1gFnSc2
kastilského	kastilský	k2eAgMnSc4d1
krále	král	k1gMnSc4
Alfonse	Alfons	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
trůn	trůn	k1gInSc4
nastoupil	nastoupit	k5eAaPmAgMnS
jako	jako	k9
dospělý	dospělý	k1gMnSc1
po	po	k7c6
smrti	smrt	k1gFnSc6
otce	otka	k1gFnSc6
roku	rok	k1gInSc2
1196	#num#	k4
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1204	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Montpellieru	Montpelliero	k1gNnSc6
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Marií	Maria	k1gFnSc7
(	(	kIx(
<g/>
†	†	k?
1213	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
hraběte	hrabě	k1gMnSc2
Viléma	Vilém	k1gMnSc2
z	z	k7c2
Montpellieru	Montpellier	k1gInSc2
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k9
švagrem	švagr	k1gMnSc7
Raimunda	Raimunda	k1gFnSc1
Rogera	Roger	k1gMnSc4
Trencavela	Trencavel	k1gMnSc4
<g/>
,	,	kIx,
vikomta	vikomt	k1gMnSc4
z	z	k7c2
Carcassonne	Carcassonn	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královskému	královský	k2eAgInSc3d1
páru	pár	k1gInSc3
se	se	k3xPyFc4
narodily	narodit	k5eAaPmAgFnP
pouze	pouze	k6eAd1
dvě	dva	k4xCgFnPc4
děti	dítě	k1gFnPc4
–	–	k?
dcera	dcera	k1gFnSc1
Sancha	Sancha	k1gFnSc1
a	a	k8xC
budoucí	budoucí	k2eAgMnSc1d1
král	král	k1gMnSc1
Jakub	Jakub	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1200	#num#	k4
se	se	k3xPyFc4
Petr	Petr	k1gMnSc1
definitivně	definitivně	k6eAd1
smířil	smířit	k5eAaPmAgMnS
s	s	k7c7
kastilským	kastilský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Alfonsem	Alfons	k1gMnSc7
VIII	VIII	kA
<g/>
.	.	kIx.
a	a	k8xC
pomohl	pomoct	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
králi	král	k1gMnSc3
leonskému	leonský	k2eAgNnSc3d1
a	a	k8xC
navarrskému	navarrský	k2eAgNnSc3d1
a	a	k8xC
proti	proti	k7c3
Maurům	Maur	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1212	#num#	k4
se	se	k3xPyFc4
účastnil	účastnit	k5eAaImAgInS
spolu	spolu	k6eAd1
se	s	k7c7
Sanchem	Sanch	k1gInSc7
Udatným	udatný	k2eAgInSc7d1
a	a	k8xC
Alfonsem	Alfons	k1gMnSc7
Kastilským	kastilský	k2eAgMnSc7d1
bitvy	bitva	k1gFnSc2
u	u	k7c2
Las	laso	k1gNnPc2
Navas	Navas	k1gInSc1
de	de	k?
Tolosa	Tolosa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křesťanští	křesťanský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
se	se	k3xPyFc4
společně	společně	k6eAd1
s	s	k7c7
davem	dav	k1gInSc7
odpustků	odpustek	k1gInPc2
chtivého	chtivý	k2eAgInSc2d1
lidu	lid	k1gInSc2
poučili	poučit	k5eAaPmAgMnP
z	z	k7c2
arabské	arabský	k2eAgFnSc2d1
taktiky	taktika	k1gFnSc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Alarcosu	Alarcos	k1gInSc2
a	a	k8xC
porazili	porazit	k5eAaPmAgMnP
arabsko-berberské	arabsko-berberský	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
chalífy	chalífa	k1gMnSc2
Muhammada	Muhammada	k1gFnSc1
an-Násira	an-Násira	k1gFnSc1
Ibn	Ibn	k1gFnSc1
Jákuba	Jákuba	k1gFnSc1
na	na	k7c4
hlavu	hlava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získali	získat	k5eAaPmAgMnP
přístup	přístup	k1gInSc4
k	k	k7c3
Gibraltarské	gibraltarský	k2eAgFnSc3d1
úžině	úžina	k1gFnSc3
<g/>
,	,	kIx,
obrovskou	obrovský	k2eAgFnSc4d1
kořist	kořist	k1gFnSc4
a	a	k8xC
podporu	podpora	k1gFnSc4
papeže	papež	k1gMnSc2
Inocence	Inocenc	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obsazení	obsazení	k1gNnSc6
celého	celý	k2eAgNnSc2d1
muslimského	muslimský	k2eAgNnSc2d1
území	území	k1gNnSc2
zabránila	zabránit	k5eAaPmAgFnS
křesťanům	křesťan	k1gMnPc3
epidemie	epidemie	k1gFnSc2
úplavice	úplavice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Roku	rok	k1gInSc2
vtělení	vtělení	k1gNnSc2
Pána	pán	k1gMnSc2
našeho	náš	k3xOp1gMnSc2
tisícího	tisící	k4xOgMnSc2
dvoustého	dvoustý	k2eAgMnSc2d1
devátého	devátý	k4xOgNnSc2
a	a	k8xC
jedenáctý	jedenáctý	k4xOgInSc4
rok	rok	k1gInSc4
duchovní	duchovní	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
papeže	papež	k1gMnSc2
Inocence	Inocenc	k1gMnSc2
<g/>
,	,	kIx,
za	za	k7c4
panování	panování	k1gNnSc4
Filipa	Filip	k1gMnSc2
<g/>
,	,	kIx,
krále	král	k1gMnSc2
francouzského	francouzský	k2eAgMnSc2d1
<g/>
,	,	kIx,
okolo	okolo	k7c2
svátku	svátek	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
všichni	všechen	k3xTgMnPc1
křižáci	křižák	k1gMnPc1
<g/>
,	,	kIx,
připutovalí	připutovalit	k5eAaPmIp3nP
z	z	k7c2
různých	různý	k2eAgFnPc2d1
částí	část	k1gFnPc2
Francie	Francie	k1gFnSc2
(	(	kIx(
<g/>
...	...	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
shromáždili	shromáždit	k5eAaPmAgMnP
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
Lyonu	Lyon	k1gInSc2
<g/>
,	,	kIx,
města	město	k1gNnSc2
francouzského	francouzský	k2eAgNnSc2d1
<g/>
...	...	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Petr	Petr	k1gMnSc1
z	z	k7c2
Vaux-de-Cernay	Vaux-de-Cernaa	k1gFnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kataři	katar	k1gMnPc1
propuštění	propuštěný	k2eAgMnPc1d1
z	z	k7c2
Carcassonne	Carcassonn	k1gInSc5
(	(	kIx(
<g/>
1209	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
proti	proti	k7c3
katarům	katar	k1gInPc3
započala	započnout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1209	#num#	k4
a	a	k8xC
jako	jako	k9
záminka	záminka	k1gFnSc1
pro	pro	k7c4
vpád	vpád	k1gInSc4
do	do	k7c2
bohaté	bohatý	k2eAgFnSc2d1
okcitánské	okcitánský	k2eAgFnSc2d1
země	zem	k1gFnSc2
posloužila	posloužit	k5eAaPmAgFnS
vražda	vražda	k1gFnSc1
papežského	papežský	k2eAgMnSc2d1
legáta	legát	k1gMnSc2
Petra	Petr	k1gMnSc2
z	z	k7c2
Castelnau	Castelnaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vůdci	vůdce	k1gMnPc1
kruciáty	kruciáta	k1gFnSc2
byli	být	k5eAaImAgMnP
Simon	Simona	k1gFnPc2
z	z	k7c2
Montfortu	Montfort	k1gInSc2
a	a	k8xC
cisterciácký	cisterciácký	k2eAgMnSc1d1
opat	opat	k1gMnSc1
z	z	k7c2
kláštera	klášter	k1gInSc2
Citeaux	Citeaux	k1gInSc4
Arnauld	Arnauld	k1gInSc1
Amaury	Amaura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Králův	Králův	k2eAgMnSc1d1
vazal	vazal	k1gMnSc1
hrabě	hrabě	k1gMnSc1
Raimond	Raimond	k1gMnSc1
z	z	k7c2
Toulouse	Toulouse	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
obavě	obava	k1gFnSc6
před	před	k7c7
ztrátou	ztráta	k1gFnSc7
panství	panství	k1gNnPc2
a	a	k8xC
hrozícím	hrozící	k2eAgInSc7d1
masakrem	masakr	k1gInSc7
snažil	snažit	k5eAaImAgMnS
o	o	k7c4
smír	smír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
1209	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
rouchu	roucho	k1gNnSc6
kajícníka	kajícník	k1gMnSc2
klaněl	klanět	k5eAaImAgMnS
náhrobku	náhrobek	k1gInSc3
zavražděného	zavražděný	k2eAgMnSc2d1
legáta	legát	k1gMnSc2
a	a	k8xC
dokonce	dokonce	k9
přijal	přijmout	k5eAaPmAgMnS
znamení	znamení	k1gNnSc2
kříže	kříž	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
koncem	konec	k1gInSc7
července	červenec	k1gInSc2
krutě	krutě	k6eAd1
vyplenili	vyplenit	k5eAaPmAgMnP
Beziérs	Beziérs	k1gInSc4
a	a	k8xC
pokračovali	pokračovat	k5eAaImAgMnP
ke	k	k7c3
Carcassonne	Carcassonn	k1gMnSc5
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
centrem	centr	k1gInSc7
katarského	katarský	k2eAgInSc2d1
jihu	jih	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aragonský	aragonský	k2eAgMnSc1d1
král	král	k1gMnSc1
Petr	Petr	k1gMnSc1
se	se	k3xPyFc4
pokoušel	pokoušet	k5eAaImAgMnS
zprostředkovat	zprostředkovat	k5eAaPmF
dohodu	dohoda	k1gFnSc4
při	při	k7c6
obléhání	obléhání	k1gNnSc6
Carcassonne	Carcassonn	k1gMnSc5
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
patřilo	patřit	k5eAaImAgNnS
dalšímu	další	k2eAgMnSc3d1
leníkovi	leník	k1gMnSc3
a	a	k8xC
zároveň	zároveň	k6eAd1
příbuznému	příbuzný	k1gMnSc3
Raimundovi	Raimund	k1gMnSc3
Rogerovi	Roger	k1gMnSc3
Trencavelovi	Trencavel	k1gMnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
zcela	zcela	k6eAd1
marně	marně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cisterciácký	cisterciácký	k2eAgMnSc1d1
opat	opat	k1gMnSc1
byl	být	k5eAaImAgMnS
ochoten	ochoten	k2eAgMnSc1d1
Trencavelovi	Trencavelův	k2eAgMnPc1d1
odpustit	odpustit	k5eAaPmF
pouze	pouze	k6eAd1
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
vyjde	vyjít	k5eAaPmIp3nS
společně	společně	k6eAd1
s	s	k7c7
dvanácti	dvanáct	k4xCc7
rytíři	rytíř	k1gMnPc7
z	z	k7c2
obleženého	obležený	k2eAgNnSc2d1
města	město	k1gNnSc2
a	a	k8xC
zbytek	zbytek	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
zanechá	zanechat	k5eAaPmIp3nS
osudu	osud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
na	na	k7c4
to	ten	k3xDgNnSc4
údajně	údajně	k6eAd1
podle	podle	k7c2
dobové	dobový	k2eAgFnSc2d1
Písně	píseň	k1gFnSc2
o	o	k7c6
kruciátě	kruciáta	k1gFnSc6
reagoval	reagovat	k5eAaBmAgMnS
slovy	slovo	k1gNnPc7
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Něco	něco	k3yInSc1
takového	takový	k3xDgNnSc2
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
<g/>
,	,	kIx,
až	až	k6eAd1
bude	být	k5eAaImBp3nS
osel	osel	k1gMnSc1
létat	létat	k5eAaImF
po	po	k7c6
nebi	nebe	k1gNnSc6
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
Vikomt	vikomt	k1gMnSc1
Trencavel	Trencavel	k1gMnSc1
se	se	k3xPyFc4
po	po	k7c6
králově	králův	k2eAgInSc6d1
odjezdu	odjezd	k1gInSc6
a	a	k8xC
dalším	další	k2eAgNnSc6d1
obléhání	obléhání	k1gNnSc6
nakonec	nakonec	k6eAd1
vydal	vydat	k5eAaPmAgMnS
jako	jako	k9
rukojmí	rukojmí	k1gMnSc1
a	a	k8xC
usmlouval	usmlouvat	k5eAaPmAgMnS
volný	volný	k2eAgInSc4d1
odchod	odchod	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
</s>
<s>
Všichni	všechen	k3xTgMnPc1
vyšli	vyjít	k5eAaPmAgMnP
nazí	nahý	k2eAgMnPc1d1
a	a	k8xC
chvatně	chvatně	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
košilích	košile	k1gFnPc6
a	a	k8xC
kytlicích	kytlice	k1gFnPc6
<g/>
,	,	kIx,
bez	bez	k7c2
jakéhokoli	jakýkoli	k3yIgInSc2
dalšího	další	k2eAgInSc2d1
oděvu	oděv	k1gInSc2
<g/>
,	,	kIx,
křižáci	křižák	k1gMnPc1
jim	on	k3xPp3gMnPc3
neponechali	ponechat	k5eNaPmAgMnP
nic	nic	k3yNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
cenu	cena	k1gFnSc4
jediného	jediný	k2eAgInSc2d1
knoflíku	knoflík	k1gInSc2
<g/>
...	...	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Muretu	Muret	k1gInSc2
(	(	kIx(
<g/>
1213	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Trencavel	Trencavel	k1gInSc1
byl	být	k5eAaImAgInS
uvězněn	uvěznit	k5eAaPmNgInS
a	a	k8xC
ve	v	k7c6
vězení	vězení	k1gNnSc6
také	také	k9
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celé	celý	k2eAgNnSc1d1
vikomtovo	vikomtův	k2eAgNnSc1d1
panství	panství	k1gNnSc1
připadlo	připadnout	k5eAaPmAgNnS
vůdci	vůdce	k1gMnSc3
kruciáty	kruciáta	k1gFnSc2
hraběti	hrabě	k1gMnSc3
z	z	k7c2
Montfortu	Montfort	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Aragonský	aragonský	k2eAgInSc1d1
jej	on	k3xPp3gMnSc4
odmítl	odmítnout	k5eAaPmAgMnS
uznat	uznat	k5eAaPmF
jako	jako	k8xS,k8xC
svého	svůj	k3xOyFgMnSc2
právoplatného	právoplatný	k2eAgMnSc2d1
vazala	vazal	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Raimond	Raimond	k1gInSc1
z	z	k7c2
Toulouse	Toulouse	k1gInSc2
se	se	k3xPyFc4
od	od	k7c2
křižácké	křižácký	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
později	pozdě	k6eAd2
opět	opět	k6eAd1
odklonil	odklonit	k5eAaPmAgMnS
a	a	k8xC
nakonec	nakonec	k6eAd1
byl	být	k5eAaImAgInS
sám	sám	k3xTgInSc1
znovu	znovu	k6eAd1
Montfortovou	Montfortová	k1gFnSc7
kořistí	kořist	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papežští	papežský	k2eAgMnPc1d1
legáti	legát	k1gMnPc1
chtěli	chtít	k5eAaImAgMnP
po	po	k7c6
mocném	mocný	k2eAgMnSc6d1
feudálovi	feudál	k1gMnSc6
další	další	k2eAgInPc4d1
a	a	k8xC
další	další	k2eAgInPc4d1
ústupky	ústupek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1211	#num#	k4
Montfort	Montfort	k1gInSc1
neúspěšně	úspěšně	k6eNd1
oblehl	oblehnout	k5eAaPmAgInS
Raimondovo	Raimondův	k2eAgNnSc4d1
město	město	k1gNnSc4
Toulouse	Toulouse	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obléhání	obléhání	k1gNnSc1
po	po	k7c6
dvou	dva	k4xCgInPc6
dnech	den	k1gInPc6
vzdal	vzdát	k5eAaPmAgMnS
a	a	k8xC
vydal	vydat	k5eAaPmAgMnS
se	se	k3xPyFc4
pokořit	pokořit	k5eAaPmF
jiné	jiný	k2eAgFnSc2d1
katarské	katarský	k2eAgFnSc2d1
pevnosti	pevnost	k1gFnSc2
a	a	k8xC
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohutnou	mohutný	k2eAgFnSc4d1
ofenzivu	ofenziva	k1gFnSc4
proti	proti	k7c3
Raimondovi	Raimond	k1gMnSc3
zahájil	zahájit	k5eAaPmAgMnS
znovu	znovu	k6eAd1
na	na	k7c6
jaře	jaro	k1gNnSc6
roku	rok	k1gInSc2
1213	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ohrožený	ohrožený	k2eAgMnSc1d1
hrabě	hrabě	k1gMnSc1
požádal	požádat	k5eAaPmAgMnS
o	o	k7c4
pomoc	pomoc	k1gFnSc4
aragonského	aragonský	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Petr	Petr	k1gMnSc1
v	v	k7c6
únoru	únor	k1gInSc6
1213	#num#	k4
zajel	zajet	k5eAaPmAgInS
za	za	k7c7
Montfortem	Montfort	k1gInSc7
k	k	k7c3
hradu	hrad	k1gInSc3
Lavaur	Lavaura	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
nepodařilo	podařit	k5eNaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
uzavřít	uzavřít	k5eAaPmF
dohodu	dohoda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
<g/>
,	,	kIx,
král	král	k1gMnSc1
i	i	k9
Montfort	Montfort	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
pokusili	pokusit	k5eAaPmAgMnP
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
získat	získat	k5eAaPmF
svatého	svatý	k2eAgMnSc4d1
otce	otec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Montfort	Montfort	k1gInSc1
byl	být	k5eAaImAgInS
úspěšnější	úspěšný	k2eAgMnSc1d2
a	a	k8xC
papež	papež	k1gMnSc1
poslal	poslat	k5eAaPmAgMnS
králi	král	k1gMnSc3
vzkaz	vzkaz	k1gInSc4
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Rozhodli	rozhodnout	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
se	se	k3xPyFc4
výslovně	výslovně	k6eAd1
přikázat	přikázat	k5eAaPmF
Tvé	tvůj	k3xOp2gFnPc4
jasnosti	jasnost	k1gFnPc4
(	(	kIx(
<g/>
...	...	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
abys	aby	kYmCp2nS
Toulousany	Toulousan	k1gInPc4
a	a	k8xC
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
jim	on	k3xPp3gMnPc3
pomáhají	pomáhat	k5eAaImIp3nP
<g/>
,	,	kIx,
neprodleně	prodleně	k6eNd1
opustil	opustit	k5eAaPmAgMnS
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
jakékoli	jakýkoli	k3yIgNnSc4
sliby	slib	k1gInPc1
a	a	k8xC
závazky	závazek	k1gInPc1
z	z	k7c2
Tvé	tvůj	k3xOp2gFnSc2
nebo	nebo	k8xC
z	z	k7c2
jejich	jejich	k3xOp3gFnSc2
strany	strana	k1gFnSc2
<g/>
...	...	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Inocenc	Inocenc	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Aragonský	aragonský	k2eAgMnSc1d1
král	král	k1gMnSc1
měl	mít	k5eAaImAgMnS
ještě	ještě	k6eAd1
v	v	k7c6
paměti	paměť	k1gFnSc6
loňské	loňský	k2eAgNnSc1d1
slavné	slavný	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
<g/>
,	,	kIx,
sebral	sebrat	k5eAaPmAgInS
mohutné	mohutný	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
a	a	k8xC
vydal	vydat	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c6
konci	konec	k1gInSc6
srpna	srpen	k1gInSc2
k	k	k7c3
městu	město	k1gNnSc3
Muret	Mureta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
Montfortovi	Montfort	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
zastihnout	zastihnout	k5eAaPmF
protivníka	protivník	k1gMnSc4
nepřipraveného	připravený	k2eNgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ráno	ráno	k6eAd1
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
napadli	napadnout	k5eAaPmAgMnP
křižáci	křižák	k1gMnPc1
spojená	spojený	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
Aragonie	Aragonie	k1gFnSc2
<g/>
,	,	kIx,
Navarry	Navarra	k1gFnSc2
a	a	k8xC
Katalánska	Katalánsko	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
ani	ani	k8xC
ve	v	k7c6
snu	sen	k1gInSc6
nenapadlo	napadnout	k5eNaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
je	on	k3xPp3gInPc4
po	po	k7c6
noci	noc	k1gFnSc6
plné	plný	k2eAgNnSc4d1
veselí	veselí	k1gNnSc4
mohl	moct	k5eAaImAgMnS
potkat	potkat	k5eAaPmF
tvrdý	tvrdý	k2eAgInSc4d1
útok	útok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojsko	vojsko	k1gNnSc1
se	se	k3xPyFc4
nestačilo	stačit	k5eNaBmAgNnS
sešikovat	sešikovat	k5eAaPmF
<g/>
,	,	kIx,
když	když	k8xS
jej	on	k3xPp3gMnSc4
Montfortovi	Montfortův	k2eAgMnPc1d1
muži	muž	k1gMnPc1
napadli	napadnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Petr	Petr	k1gMnSc1
se	se	k3xPyFc4
odvážně	odvážně	k6eAd1
vrhl	vrhnout	k5eAaImAgMnS,k5eAaPmAgMnS
do	do	k7c2
útoku	útok	k1gInSc2
a	a	k8xC
byl	být	k5eAaImAgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
zabit	zabít	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tělo	tělo	k1gNnSc1
rytířského	rytířský	k2eAgMnSc2d1
panovníka	panovník	k1gMnSc2
bylo	být	k5eAaImAgNnS
nalezeno	nalézt	k5eAaBmNgNnS,k5eAaPmNgNnS
na	na	k7c6
bojišti	bojiště	k1gNnSc6
nahé	nahý	k2eAgFnPc1d1
<g/>
,	,	kIx,
obrané	obraný	k2eAgFnPc1d1
o	o	k7c4
vše	všechen	k3xTgNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc4d1
odpočinek	odpočinek	k1gInSc4
nalezl	naleznout	k5eAaPmAgMnS,k5eAaBmAgMnS
král	král	k1gMnSc1
v	v	k7c6
cisterciáckém	cisterciácký	k2eAgInSc6d1
klášteře	klášter	k1gInSc6
Sigena	Sigena	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Nástupcem	nástupce	k1gMnSc7
na	na	k7c6
aragonském	aragonský	k2eAgInSc6d1
trůně	trůn	k1gInSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jediný	jediný	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Jakub	Jakub	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Ramon	Ramona	k1gFnPc2
Berenguer	Berengura	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelonský	barcelonský	k2eAgInSc1d1
</s>
<s>
Ramon	Ramona	k1gFnPc2
Berenguer	Berengura	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelonský	barcelonský	k2eAgInSc1d1
</s>
<s>
Matylda	Matylda	k1gFnSc1
z	z	k7c2
Hauteville	Hauteville	k1gFnSc2
</s>
<s>
Ramon	Ramona	k1gFnPc2
Berenguer	Berengura	k1gFnPc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelonský	barcelonský	k2eAgInSc1d1
</s>
<s>
Gilbert	Gilbert	k1gMnSc1
z	z	k7c2
Gévaudanu	Gévaudan	k1gInSc2
</s>
<s>
Dulce	Dulko	k6eAd1
Provensálská	provensálský	k2eAgFnSc1d1
</s>
<s>
Gerberga	Gerberga	k1gFnSc1
Provensálská	provensálský	k2eAgFnSc1d1
</s>
<s>
Alfons	Alfons	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aragonský	aragonský	k2eAgInSc1d1
</s>
<s>
Sancho	Sancho	k6eAd1
I.	I.	kA
Aragonský	aragonský	k2eAgMnSc5d1
</s>
<s>
Ramiro	Ramiro	k6eAd1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aragonský	aragonský	k2eAgInSc1d1
</s>
<s>
Felicie	Felicie	k1gFnSc1
z	z	k7c2
Roussy	Roussa	k1gFnSc2
</s>
<s>
Petronila	Petronila	k1gFnSc1
Aragonská	aragonský	k2eAgFnSc1d1
</s>
<s>
Vilém	Vilém	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akvitánský	Akvitánský	k2eAgInSc1d1
</s>
<s>
Anežka	Anežka	k1gFnSc1
Akvitánská	Akvitánský	k2eAgFnSc1d1
</s>
<s>
Filipa	Filip	k1gMnSc4
z	z	k7c2
Toulouse	Toulouse	k1gInSc2
</s>
<s>
'	'	kIx"
<g/>
Petr	Petr	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aragonský	aragonský	k2eAgInSc1d1
<g/>
'	'	kIx"
</s>
<s>
Vilém	Vilém	k1gMnSc1
I.	I.	kA
Burgundský	burgundský	k2eAgInSc1d1
</s>
<s>
Raymond	Raymond	k1gMnSc1
Burgundský	burgundský	k2eAgMnSc1d1
</s>
<s>
Štěpánka	Štěpánka	k1gFnSc1
Burgundská	burgundský	k2eAgFnSc1d1
</s>
<s>
Alfons	Alfons	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kastilský	kastilský	k2eAgInSc1d1
</s>
<s>
Alfons	Alfons	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kastilský	kastilský	k2eAgInSc1d1
</s>
<s>
Urraca	Urrac	k2eAgFnSc1d1
Kastilská	kastilský	k2eAgFnSc1d1
</s>
<s>
Konstancie	Konstancie	k1gFnSc1
Burgundská	burgundský	k2eAgFnSc1d1
</s>
<s>
Sancha	Sancha	k1gFnSc1
Kastilská	kastilský	k2eAgFnSc1d1
</s>
<s>
Boleslav	Boleslav	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křivoústý	křivoústý	k2eAgInSc1d1
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhnanec	vyhnanec	k1gMnSc1
</s>
<s>
Zbyslava	Zbyslava	k1gFnSc1
Kyjevská	kyjevský	k2eAgFnSc1d1
</s>
<s>
Richenza	Richenza	k1gFnSc1
Slezská	slezský	k2eAgFnSc1d1
</s>
<s>
Leopold	Leopold	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babenberský	babenberský	k2eAgInSc1d1
</s>
<s>
Anežka	Anežka	k1gFnSc1
Babenberská	babenberský	k2eAgFnSc1d1
</s>
<s>
Anežka	Anežka	k1gFnSc1
z	z	k7c2
Waiblingenu	Waiblingen	k1gInSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KOLEKTIV	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Španělska	Španělsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
117	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
KOVAŘÍK	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rytířská	rytířský	k2eAgFnSc1d1
krev	krev	k1gFnSc1
<g/>
,	,	kIx,
Rytířské	rytířský	k2eAgFnPc1d1
bitvy	bitva	k1gFnPc1
a	a	k8xC
osudy	osud	k1gInPc1
(	(	kIx(
<g/>
1208	#num#	k4
<g/>
-	-	kIx~
<g/>
1346	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
1401	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.enciclopedia-aragonesa.com	www.enciclopedia-aragonesa.com	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Las	laso	k1gNnPc2
Navas	Navas	k1gInSc1
de	de	k?
Tolosa	Tolosa	k1gFnSc1
</s>
<s>
Albigenská	albigenský	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Muretu	Muret	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Petr	Petr	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aragonský	aragonský	k2eAgInSc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Genealogie	genealogie	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
genealogie-mittelalter	genealogie-mittelalter	k1gInSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Las	laso	k1gNnPc2
Navas	Navas	k1gInSc1
de	de	k?
Tolosa	Tolosa	k1gFnSc1
(	(	kIx(
<g/>
červenec	červenec	k1gInSc1
1212	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Muretu	Muret	k1gInSc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Alfons	Alfons	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Aragonský	aragonský	k2eAgMnSc1d1
král	král	k1gMnSc1
1196	#num#	k4
-	-	kIx~
1213	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Jakub	Jakub	k1gMnSc1
I.	I.	kA
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Alfons	Alfons	k1gMnSc1
I.	I.	kA
</s>
<s>
Barcelonský	barcelonský	k2eAgMnSc1d1
hrabě	hrabě	k1gMnSc1
Petr	Petr	k1gMnSc1
I	i	k9
<g/>
.1196	.1196	k4
-	-	kIx~
1213	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Jakub	Jakub	k1gMnSc1
I.	I.	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1018870210	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7690	#num#	k4
7486	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80164752	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
10113655	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80164752	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Španělsko	Španělsko	k1gNnSc1
|	|	kIx~
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
|	|	kIx~
Středověk	středověk	k1gInSc1
</s>
