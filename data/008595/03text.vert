<p>
<s>
Hotel	hotel	k1gInSc1	hotel
Gambrinus	gambrinus	k1gInSc1	gambrinus
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
Ostravě	Ostrava	k1gFnSc6	Ostrava
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
letech	let	k1gInPc6	let
1891	[number]	k4	1891
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
staršího	starý	k2eAgMnSc2d2	starší
dříve	dříve	k6eAd2	dříve
právovárečného	právovárečný	k2eAgInSc2d1	právovárečný
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Novostavbu	novostavba	k1gFnSc4	novostavba
v	v	k7c6	v
historizujících	historizující	k2eAgFnPc6d1	historizující
formách	forma	k1gFnPc6	forma
projektoval	projektovat	k5eAaBmAgMnS	projektovat
architekt	architekt	k1gMnSc1	architekt
Felix	Felix	k1gMnSc1	Felix
Neumann	Neumann	k1gMnSc1	Neumann
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
a	a	k8xC	a
majitele	majitel	k1gMnSc2	majitel
domu	dům	k1gInSc2	dům
Emanuela	Emanuel	k1gMnSc4	Emanuel
Neumanna	Neumann	k1gMnSc4	Neumann
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
bylo	být	k5eAaImAgNnS	být
hotelové	hotelový	k2eAgNnSc1d1	hotelové
ubytování	ubytování	k1gNnSc1	ubytování
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
,	,	kIx,	,
kavárna	kavárna	k1gFnSc1	kavárna
a	a	k8xC	a
pivnice	pivnice	k1gFnSc1	pivnice
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
městské	městský	k2eAgFnSc2d1	městská
památkové	památkový	k2eAgFnSc2d1	památková
zóny	zóna	k1gFnSc2	zóna
Moravská	moravský	k2eAgFnSc1d1	Moravská
Ostrava	Ostrava	k1gFnSc1	Ostrava
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
slavných	slavný	k2eAgMnPc2d1	slavný
hostů	host	k1gMnPc2	host
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
ubytován	ubytován	k2eAgMnSc1d1	ubytován
například	například	k6eAd1	například
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Rainer	Rainer	k1gMnSc1	Rainer
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
von	von	k1gInSc4	von
Österreich	Österreich	k1gInSc1	Österreich
(	(	kIx(	(
<g/>
1827	[number]	k4	1827
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gInSc1	člen
rodu	rod	k1gInSc2	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
a	a	k8xC	a
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
c.	c.	k?	c.
k.	k.	k?	k.
zeměbrany	zeměbrana	k1gFnSc2	zeměbrana
<g/>
,	,	kIx,	,
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
návštěvě	návštěva	k1gFnSc6	návštěva
Ostravy	Ostrava	k1gFnSc2	Ostrava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
získala	získat	k5eAaPmAgFnS	získat
dům	dům	k1gInSc4	dům
Pozemková	pozemkový	k2eAgFnSc1d1	pozemková
banka	banka	k1gFnSc1	banka
a	a	k8xC	a
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
objekt	objekt	k1gInSc1	objekt
pro	pro	k7c4	pro
nový	nový	k2eAgInSc4d1	nový
účel	účel	k1gInSc4	účel
adaptován	adaptován	k2eAgInSc4d1	adaptován
<g/>
.	.	kIx.	.
</s>
<s>
Průčelí	průčelí	k1gNnSc1	průčelí
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
upraveno	upravit	k5eAaPmNgNnS	upravit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
domu	dům	k1gInSc2	dům
pro	pro	k7c4	pro
peněžní	peněžní	k2eAgInSc4d1	peněžní
ústav	ústav	k1gInSc4	ústav
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
Pražskou	pražský	k2eAgFnSc4d1	Pražská
úvěrní	úvěrní	k2eAgFnSc4d1	úvěrní
banku	banka	k1gFnSc4	banka
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
štítu	štít	k1gInSc2	štít
domu	dům	k1gInSc2	dům
instalována	instalován	k2eAgFnSc1d1	instalována
hlava	hlava	k1gFnSc1	hlava
Merkura	Merkur	k1gMnSc2	Merkur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
domu	dům	k1gInSc2	dům
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
lékárna	lékárna	k1gFnSc1	lékárna
a	a	k8xC	a
ve	v	k7c6	v
vyšších	vysoký	k2eAgNnPc6d2	vyšší
patrech	patro	k1gNnPc6	patro
ordinace	ordinace	k1gFnSc1	ordinace
lékařů	lékař	k1gMnPc2	lékař
a	a	k8xC	a
kanceláře	kancelář	k1gFnSc2	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
patře	patro	k1gNnSc6	patro
budovy	budova	k1gFnSc2	budova
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
sbírky	sbírka	k1gFnPc1	sbírka
a	a	k8xC	a
expozice	expozice	k1gFnSc1	expozice
Citerária	Citerárium	k1gNnSc2	Citerárium
<g/>
,	,	kIx,	,
soukromého	soukromý	k2eAgNnSc2d1	soukromé
muzea	muzeum	k1gNnSc2	muzeum
citer	citera	k1gFnPc2	citera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
KROČEK	kroček	k1gInSc1	kroček
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Osudy	osud	k1gInPc1	osud
a	a	k8xC	a
podoba	podoba	k1gFnSc1	podoba
měšťanských	měšťanský	k2eAgInPc2d1	měšťanský
domů	dům	k1gInPc2	dům
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
Moravské	moravský	k2eAgFnSc6d1	Moravská
Ostravě	Ostrava	k1gFnSc6	Ostrava
II	II	kA	II
(	(	kIx(	(
<g/>
pokračování	pokračování	k1gNnSc1	pokračování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
a	a	k8xC	a
současnost	současnost	k1gFnSc4	současnost
Ostravy	Ostrava	k1gFnSc2	Ostrava
a	a	k8xC	a
Ostravska	Ostravsko	k1gNnSc2	Ostravsko
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
</s>
<s>
XIII	XIII	kA	XIII
<g/>
,	,	kIx,	,
s.	s.	k?	s.
545	[number]	k4	545
-	-	kIx~	-
571	[number]	k4	571
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
STRAKOŠ	strakoš	k1gMnSc1	strakoš
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Průvodce	průvodce	k1gMnSc1	průvodce
architekturou	architektura	k1gFnSc7	architektura
Moravské	moravský	k2eAgFnSc2d1	Moravská
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DLOUHÝ	Dlouhý	k1gMnSc1	Dlouhý
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Zajímavé	zajímavý	k2eAgFnSc2d1	zajímavá
návštěvy	návštěva	k1gFnSc2	návštěva
slavných	slavný	k2eAgMnPc2d1	slavný
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
od	od	k7c2	od
jejího	její	k3xOp3gNnSc2	její
založení	založení	k1gNnSc2	založení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1267	[number]	k4	1267
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
