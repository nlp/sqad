<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Zimbabwe	Zimbabw	k1gInSc2	Zimbabw
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
zeleným	zelený	k2eAgInSc7d1	zelený
štítem	štít	k1gInSc7	štít
se	s	k7c7	s
stylizovanou	stylizovaný	k2eAgFnSc7d1	stylizovaná
kresbou	kresba	k1gFnSc7	kresba
ruin	ruina	k1gFnPc2	ruina
Velké	velká	k1gFnSc2	velká
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stříbrné	stříbrný	k2eAgFnSc6d1	stříbrná
(	(	kIx(	(
<g/>
bílé	bílý	k2eAgFnSc6d1	bílá
<g/>
)	)	kIx)	)
hlavě	hlava	k1gFnSc6	hlava
štítu	štít	k1gInSc2	štít
je	být	k5eAaImIp3nS	být
sedm	sedm	k4xCc1	sedm
svislých	svislý	k2eAgMnPc2d1	svislý
<g/>
,	,	kIx,	,
zvlněných	zvlněný	k2eAgInPc2d1	zvlněný
<g/>
,	,	kIx,	,
modrých	modrý	k2eAgInPc2d1	modrý
pruhů	pruh	k1gInPc2	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
štítem	štít	k1gInSc7	štít
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
žlutá	žlutý	k2eAgFnSc1d1	žlutá
soška	soška	k1gFnSc1	soška
ptáka	pták	k1gMnSc2	pták
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
nalezena	naleznout	k5eAaPmNgFnS	naleznout
v	v	k7c6	v
ruinách	ruina	k1gFnPc6	ruina
Velké	velký	k2eAgFnSc2d1	velká
Zimbabwe	Zimbabw	k1gFnSc2	Zimbabw
a	a	k8xC	a
žluto-zelená	žlutoelený	k2eAgFnSc1d1	žluto-zelená
točenice	točenice	k1gFnSc1	točenice
<g/>
.	.	kIx.	.
</s>
<s>
Štít	štít	k1gInSc4	štít
podpírají	podpírat	k5eAaImIp3nP	podpírat
zkřížený	zkřížený	k2eAgInSc4d1	zkřížený
samopal	samopal	k1gInSc4	samopal
s	s	k7c7	s
motykou	motyka	k1gFnSc7	motyka
a	a	k8xC	a
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
pár	pár	k4xCyI	pár
antilop	antilopa	k1gFnPc2	antilopa
kudu	kudu	k1gMnPc2	kudu
<g/>
.	.	kIx.	.
</s>
<s>
Antilopy	antilopa	k1gFnPc1	antilopa
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
tmavě	tmavě	k6eAd1	tmavě
žlutém	žlutý	k2eAgInSc6d1	žlutý
pažitu	pažit	k1gInSc6	pažit
ozdobeným	ozdobený	k2eAgNnSc7d1	ozdobené
klasy	klasa	k1gFnPc1	klasa
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
,	,	kIx,	,
bavlníkem	bavlník	k1gInSc7	bavlník
a	a	k8xC	a
kukuřičným	kukuřičný	k2eAgInSc7d1	kukuřičný
klasem	klas	k1gInSc7	klas
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
štítem	štít	k1gInSc7	štít
je	být	k5eAaImIp3nS	být
stříbrná	stříbrná	k1gFnSc1	stříbrná
(	(	kIx(	(
<g/>
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
červená	červený	k2eAgFnSc1d1	červená
<g/>
)	)	kIx)	)
stuha	stuha	k1gFnSc1	stuha
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
UNITY	unita	k1gMnPc4	unita
•	•	k?	•
FREEDOM	FREEDOM	kA	FREEDOM
•	•	k?	•
WORK	WORK	kA	WORK
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Jednota	jednota	k1gFnSc1	jednota
<g/>
,	,	kIx,	,
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc1	práce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
znaku	znak	k1gInSc2	znak
Jižní	jižní	k2eAgFnSc2d1	jižní
Rhodesie	Rhodesie	k1gFnSc2	Rhodesie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
vyvěšovali	vyvěšovat	k5eAaImAgMnP	vyvěšovat
britští	britský	k2eAgMnPc1d1	britský
kolonialisté	kolonialista	k1gMnPc1	kolonialista
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Rhodesii	Rhodesie	k1gFnSc6	Rhodesie
vlajku	vlajka	k1gFnSc4	vlajka
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
s	s	k7c7	s
uprostřed	uprostřed	k6eAd1	uprostřed
umístěným	umístěný	k2eAgInSc7d1	umístěný
znakem	znak	k1gInSc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
znak	znak	k1gInSc4	znak
Britské	britský	k2eAgFnSc2d1	britská
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
British	British	k1gMnSc1	British
South	South	k1gMnSc1	South
Africa	Africa	k1gMnSc1	Africa
Company	Compana	k1gFnSc2	Compana
<g/>
)	)	kIx)	)
podnikatele	podnikatel	k1gMnSc2	podnikatel
Cecila	Cecil	k1gMnSc2	Cecil
Rhodese	Rhodese	k1gFnSc2	Rhodese
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
byl	být	k5eAaImAgInS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
žlutým	žlutý	k2eAgInSc7d1	žlutý
<g/>
,	,	kIx,	,
kráčejícím	kráčející	k2eAgInSc7d1	kráčející
lvem	lev	k1gInSc7	lev
<g/>
,	,	kIx,	,
držícím	držící	k2eAgMnSc7d1	držící
v	v	k7c6	v
pravé	pravá	k1gFnSc6	pravá
<g/>
,	,	kIx,	,
přední	přední	k2eAgFnSc6d1	přední
tlapě	tlapa	k1gFnSc6	tlapa
sloní	sloní	k2eAgInSc4d1	sloní
kel	kel	k1gInSc4	kel
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
firemním	firemní	k2eAgInSc7d1	firemní
znakem	znak	k1gInSc7	znak
se	se	k3xPyFc4	se
užívala	užívat	k5eAaImAgFnS	užívat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
skončila	skončit	k5eAaPmAgFnS	skončit
platnost	platnost	k1gFnSc4	platnost
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
B.	B.	kA	B.
<g/>
S.	S.	kA	S.
<g/>
C.A.	C.A.	k1gMnSc1	C.A.
na	na	k7c6	na
kolonizaci	kolonizace	k1gFnSc6	kolonizace
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
kolonialisté	kolonialista	k1gMnPc1	kolonialista
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
(	(	kIx(	(
<g/>
s	s	k7c7	s
blížícím	blížící	k2eAgMnSc7d1	blížící
se	se	k3xPyFc4	se
koncem	konec	k1gInSc7	konec
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
)	)	kIx)	)
začlenění	začlenění	k1gNnSc4	začlenění
do	do	k7c2	do
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1923	[number]	k4	1923
britskou	britský	k2eAgFnSc7d1	britská
vládou	vláda	k1gFnSc7	vláda
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
samosprávná	samosprávný	k2eAgFnSc1d1	samosprávná
kolonie	kolonie	k1gFnSc1	kolonie
Jižní	jižní	k2eAgFnSc2d1	jižní
Rhodesie	Rhodesie	k1gFnSc2	Rhodesie
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1924	[number]	k4	1924
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
zaveden	zavést	k5eAaPmNgInS	zavést
i	i	k9	i
znak	znak	k1gInSc1	znak
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
byl	být	k5eAaImAgInS	být
vyobrazen	vyobrazen	k2eAgInSc1d1	vyobrazen
emblém	emblém	k1gInSc1	emblém
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Badge	Badg	k1gMnSc2	Badg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
štítem	štít	k1gInSc7	štít
ze	z	k7c2	z
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Štít	štít	k1gInSc1	štít
mě	já	k3xPp1nSc4	já
bílý	bílý	k2eAgInSc1d1	bílý
pruh	pruh	k1gInSc1	pruh
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
byl	být	k5eAaImAgInS	být
uprostřed	uprostřed	k6eAd1	uprostřed
umístěn	umístit	k5eAaPmNgInS	umístit
červený	červený	k2eAgInSc1d1	červený
britský	britský	k2eAgInSc1d1	britský
lev	lev	k1gInSc1	lev
a	a	k8xC	a
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
dva	dva	k4xCgInPc4	dva
zeleno-fialové	zelenoialové	k2eAgInPc4d1	zeleno-fialové
bodláky	bodlák	k1gInPc4	bodlák
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pruhem	pruh	k1gInSc7	pruh
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
zeleném	zelený	k2eAgNnSc6d1	zelené
poli	pole	k1gNnSc6	pole
žlutý	žlutý	k2eAgInSc4d1	žlutý
krumpáč	krumpáč	k1gInSc4	krumpáč
<g/>
.	.	kIx.	.
</s>
<s>
Bodláky	bodlák	k1gInPc1	bodlák
byly	být	k5eAaImAgInP	být
převzaty	převzít	k5eAaPmNgInP	převzít
ze	z	k7c2	z
znaku	znak	k1gInSc2	znak
Cecila	Cecil	k1gMnSc2	Cecil
Rhodese	Rhodese	k1gFnSc2	Rhodese
(	(	kIx(	(
<g/>
obrázek	obrázek	k1gInSc1	obrázek
samotného	samotný	k2eAgInSc2d1	samotný
emblému	emblém	k1gInSc2	emblém
není	být	k5eNaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
štítem	štít	k1gInSc7	štít
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
státním	státní	k2eAgInSc6d1	státní
znaku	znak	k1gInSc6	znak
deviza	deviza	k1gFnSc1	deviza
"	"	kIx"	"
<g/>
SIT	SIT	kA	SIT
NOMINE	NOMINE	kA	NOMINE
DIGNA	DIGNA	kA	DIGNA
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Ať	ať	k9	ať
je	být	k5eAaImIp3nS	být
hodna	hoden	k2eAgFnSc1d1	hodna
svého	svůj	k3xOyFgNnSc2	svůj
jména	jméno	k1gNnSc2	jméno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
helm	helm	k6eAd1	helm
s	s	k7c7	s
přikrývadly	přikrývadlo	k1gNnPc7	přikrývadlo
a	a	k8xC	a
v	v	k7c6	v
klenotu	klenot	k1gInSc6	klenot
vyobrazení	vyobrazení	k1gNnSc2	vyobrazení
sošky	soška	k1gFnSc2	soška
ptáka	pták	k1gMnSc2	pták
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
nalezena	nalézt	k5eAaBmNgFnS	nalézt
v	v	k7c6	v
ruinách	ruina	k1gFnPc6	ruina
Velké	velký	k2eAgFnSc2d1	velká
Zimbabwe	Zimbabw	k1gFnSc2	Zimbabw
<g/>
.	.	kIx.	.
</s>
<s>
Štítonoši	štítonoš	k1gMnPc1	štítonoš
byly	být	k5eAaImAgFnP	být
dvě	dva	k4xCgFnPc1	dva
antilopy	antilopa	k1gFnPc1	antilopa
stojící	stojící	k2eAgFnPc1d1	stojící
na	na	k7c6	na
zeleném	zelený	k2eAgInSc6d1	zelený
pažitu	pažit	k1gInSc6	pažit
<g/>
.1	.1	k4	.1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1953	[number]	k4	1953
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Federace	federace	k1gFnSc1	federace
Rhodesie	Rhodesie	k1gFnSc2	Rhodesie
a	a	k8xC	a
Ňaska	Ňasko	k1gNnSc2	Ňasko
(	(	kIx(	(
<g/>
spojením	spojení	k1gNnSc7	spojení
Jižní	jižní	k2eAgFnSc2d1	jižní
Rhodesie	Rhodesie	k1gFnSc2	Rhodesie
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Rhodesie	Rhodesie	k1gFnSc1	Rhodesie
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Zambie	Zambie	k1gFnSc1	Zambie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ňaska	Ňasko	k1gNnPc1	Ňasko
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnPc1d1	dnešní
Malawi	Malawi	k1gNnPc1	Malawi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
zavedené	zavedený	k2eAgNnSc1d1	zavedené
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1954	[number]	k4	1954
byl	být	k5eAaImAgInS	být
emblém	emblém	k1gInSc1	emblém
(	(	kIx(	(
<g/>
nezaměňovat	zaměňovat	k5eNaImF	zaměňovat
za	za	k7c4	za
znak	znak	k1gInSc4	znak
<g/>
)	)	kIx)	)
této	tento	k3xDgFnSc2	tento
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Emblém	emblém	k1gInSc4	emblém
tvořil	tvořit	k5eAaImAgInS	tvořit
štít	štít	k1gInSc1	štít
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
na	na	k7c4	na
tři	tři	k4xCgNnPc4	tři
pole	pole	k1gNnPc4	pole
<g/>
:	:	kIx,	:
horní	horní	k2eAgNnSc4d1	horní
modré	modré	k1gNnSc4	modré
se	s	k7c7	s
žlutou	žlutý	k2eAgFnSc7d1	žlutá
polovinou	polovina	k1gFnSc7	polovina
vycházejícího	vycházející	k2eAgNnSc2d1	vycházející
slunce	slunce	k1gNnSc2	slunce
s	s	k7c7	s
patnácti	patnáct	k4xCc7	patnáct
paprsky	paprsek	k1gInPc7	paprsek
(	(	kIx(	(
<g/>
Ňasko	Ňasko	k1gNnSc4	Ňasko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc2d1	střední
bílé	bílý	k2eAgFnSc2d1	bílá
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
hradeb	hradba	k1gFnPc2	hradba
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
britským	britský	k2eAgInSc7d1	britský
lvem	lev	k1gInSc7	lev
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
Rhodesie	Rhodesie	k1gFnSc1	Rhodesie
<g/>
)	)	kIx)	)
a	a	k8xC	a
dolní	dolní	k2eAgFnSc2d1	dolní
černé	černá	k1gFnSc2	černá
se	s	k7c7	s
šesti	šest	k4xCc7	šest
bílými	bílý	k2eAgInPc7d1	bílý
zvlněnými	zvlněný	k2eAgInPc7d1	zvlněný
pruhy	pruh	k1gInPc7	pruh
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc2d1	severní
Rhodesie	Rhodesie	k1gFnSc2	Rhodesie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Federace	federace	k1gFnSc1	federace
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
a	a	k8xC	a
na	na	k7c4	na
vlajku	vlajka	k1gFnSc4	vlajka
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
jihorhodeský	jihorhodeský	k2eAgInSc1d1	jihorhodeský
emblém	emblém	k1gInSc1	emblém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
další	další	k2eAgNnSc1d1	další
referendum	referendum	k1gNnSc1	referendum
bílých	bílý	k2eAgMnPc2d1	bílý
kolonistů	kolonista	k1gMnPc2	kolonista
o	o	k7c6	o
nezávislosti	nezávislost	k1gFnSc6	nezávislost
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
pro	pro	k7c4	pro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
bezvýsledném	bezvýsledný	k2eAgNnSc6d1	bezvýsledné
jednání	jednání	k1gNnSc6	jednání
s	s	k7c7	s
britskou	britský	k2eAgFnSc7d1	britská
vládou	vláda	k1gFnSc7	vláda
byla	být	k5eAaImAgFnS	být
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1965	[number]	k4	1965
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
Ianem	Ianus	k1gMnSc7	Ianus
Douglasem	Douglas	k1gMnSc7	Douglas
Smithem	Smith	k1gInSc7	Smith
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
jednostranně	jednostranně	k6eAd1	jednostranně
(	(	kIx(	(
<g/>
stát	stát	k1gInSc1	stát
nebyl	být	k5eNaImAgInS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznán	uznán	k2eAgInSc1d1	uznán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
názvem	název	k1gInSc7	název
Rhodesie	Rhodesie	k1gFnSc2	Rhodesie
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1968	[number]	k4	1968
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
<g />
.	.	kIx.	.
</s>
<s>
nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
uprostřed	uprostřed	k6eAd1	uprostřed
umístěným	umístěný	k2eAgInSc7d1	umístěný
rhodeským	rhodeský	k2eAgInSc7d1	rhodeský
státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
březen	březen	k1gInSc4	březen
1970	[number]	k4	1970
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
republika	republika	k1gFnSc1	republika
<g/>
.1	.1	k4	.1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1979	[number]	k4	1979
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Zimbabwe	Zimbabwe	k1gNnSc4	Zimbabwe
Rhodesie	Rhodesie	k1gFnSc2	Rhodesie
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
opět	opět	k6eAd1	opět
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Rhodesii	Rhodesie	k1gFnSc4	Rhodesie
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
kolonie	kolonie	k1gFnSc1	kolonie
a	a	k8xC	a
britská	britský	k2eAgFnSc1d1	britská
správa	správa	k1gFnSc1	správa
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
Zimbabwe	Zimbabw	k1gInSc2	Zimbabw
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
i	i	k8xC	i
nový	nový	k2eAgInSc1d1	nový
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
užíván	užívat	k5eAaImNgInS	užívat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgNnSc1d1	další
použití	použití	k1gNnSc1	použití
znaku	znak	k1gInSc2	znak
==	==	k?	==
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vyobrazen	vyobrazen	k2eAgMnSc1d1	vyobrazen
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
zimbabwského	zimbabwský	k2eAgMnSc2d1	zimbabwský
prezidenta	prezident	k1gMnSc2	prezident
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
je	být	k5eAaImIp3nS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
i	i	k9	i
na	na	k7c6	na
současné	současný	k2eAgFnSc6d1	současná
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
vlajce	vlajka	k1gFnSc6	vlajka
(	(	kIx(	(
<g/>
zdroj	zdroj	k1gInSc1	zdroj
však	však	k9	však
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
cca	cca	kA	cca
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
platnosti	platnost	k1gFnSc2	platnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Zimbabwe	Zimbabw	k1gFnSc2	Zimbabw
</s>
</p>
<p>
<s>
Hymna	hymna	k1gFnSc1	hymna
Zimbabwe	Zimbabw	k1gFnSc2	Zimbabw
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
Zimbabwe	Zimbabwe	k1gFnPc2	Zimbabwe
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
