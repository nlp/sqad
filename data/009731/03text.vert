<p>
<s>
Rozlišovací	rozlišovací	k2eAgFnSc1d1	rozlišovací
značka	značka	k1gFnSc1	značka
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
termín	termín	k1gInSc1	termín
dle	dle	k7c2	dle
§	§	k?	§
30	[number]	k4	30
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
343	[number]	k4	343
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
rozlišovací	rozlišovací	k2eAgFnSc1d1	rozlišovací
značka	značka	k1gFnSc1	značka
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
§	§	k?	§
38	[number]	k4	38
zákona	zákon	k1gInSc2	zákon
číslo	číslo	k1gNnSc4	číslo
56	[number]	k4	56
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
poznávací	poznávací	k2eAgFnSc1d1	poznávací
značka	značka	k1gFnSc1	značka
(	(	kIx(	(
<g/>
MPZ	MPZ	kA	MPZ
<g/>
,	,	kIx,	,
starší	starý	k2eAgInSc4d2	starší
československý	československý	k2eAgInSc4d1	československý
termín	termín	k1gInSc4	termín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
bylo	být	k5eAaImAgNnS	být
dané	daný	k2eAgNnSc4d1	dané
motorové	motorový	k2eAgNnSc4d1	motorové
vozidlo	vozidlo	k1gNnSc4	vozidlo
registrováno	registrován	k2eAgNnSc4d1	registrováno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
doplňkem	doplněk	k1gInSc7	doplněk
státní	státní	k2eAgFnSc2d1	státní
poznávací	poznávací	k2eAgFnSc2d1	poznávací
značky	značka	k1gFnSc2	značka
silničního	silniční	k2eAgNnSc2d1	silniční
motorového	motorový	k2eAgNnSc2d1	motorové
vozidla	vozidlo	k1gNnSc2	vozidlo
nebo	nebo	k8xC	nebo
jeho	on	k3xPp3gNnSc2	on
přípojného	přípojný	k2eAgNnSc2d1	přípojné
vozidla	vozidlo	k1gNnSc2	vozidlo
(	(	kIx(	(
<g/>
registrační	registrační	k2eAgFnSc2d1	registrační
značky	značka	k1gFnSc2	značka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
mezinárodně	mezinárodně	k6eAd1	mezinárodně
zaručenu	zaručen	k2eAgFnSc4d1	zaručena
unikátnost	unikátnost	k1gFnSc4	unikátnost
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
MPZ	MPZ	kA	MPZ
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
jednoznačnou	jednoznačný	k2eAgFnSc4d1	jednoznačná
identifikaci	identifikace	k1gFnSc4	identifikace
libovolného	libovolný	k2eAgNnSc2d1	libovolné
vozidla	vozidlo	k1gNnSc2	vozidlo
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každému	každý	k3xTgInSc3	každý
státu	stát	k1gInSc3	stát
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
OSN	OSN	kA	OSN
přidělena	přidělen	k2eAgFnSc1d1	přidělena
rozlišovací	rozlišovací	k2eAgFnSc1d1	rozlišovací
značka	značka	k1gFnSc1	značka
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
až	až	k6eAd1	až
čtyř	čtyři	k4xCgNnPc2	čtyři
písmen	písmeno	k1gNnPc2	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
písmena	písmeno	k1gNnPc1	písmeno
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
vyznačena	vyznačit	k5eAaPmNgNnP	vyznačit
na	na	k7c6	na
vozidle	vozidlo	k1gNnSc6	vozidlo
černou	černý	k2eAgFnSc7d1	černá
barvou	barva	k1gFnSc7	barva
na	na	k7c6	na
bílém	bílé	k1gNnSc6	bílé
podkladu	podklad	k1gInSc2	podklad
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
elipsy	elipsa	k1gFnSc2	elipsa
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišovací	rozlišovací	k2eAgFnSc1d1	rozlišovací
značka	značka	k1gFnSc1	značka
státu	stát	k1gInSc2	stát
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgNnSc1d1	různé
provedení	provedení	k1gNnSc1	provedení
např.	např.	kA	např.
tvar	tvar	k1gInSc4	tvar
eliptická	eliptický	k2eAgFnSc1d1	eliptická
tabulka	tabulka	k1gFnSc1	tabulka
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
nebo	nebo	k8xC	nebo
přilepená	přilepený	k2eAgFnSc1d1	přilepená
samolepka	samolepka	k1gFnSc1	samolepka
tvaru	tvar	k1gInSc2	tvar
elipsy	elipsa	k1gFnSc2	elipsa
nebo	nebo	k8xC	nebo
jí	on	k3xPp3gFnSc2	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
grafické	grafický	k2eAgNnSc1d1	grafické
vyznačení	vyznačení	k1gNnSc1	vyznačení
barvami	barva	k1gFnPc7	barva
(	(	kIx(	(
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
černá	černý	k2eAgFnSc1d1	černá
<g/>
)	)	kIx)	)
na	na	k7c4	na
karoserii	karoserie	k1gFnSc4	karoserie
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišovací	rozlišovací	k2eAgFnSc1d1	rozlišovací
značka	značka	k1gFnSc1	značka
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
zadní	zadní	k2eAgFnSc4d1	zadní
část	část	k1gFnSc4	část
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišovací	rozlišovací	k2eAgFnSc1d1	rozlišovací
značka	značka	k1gFnSc1	značka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
CZ	CZ	kA	CZ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
poznávací	poznávací	k2eAgFnSc2d1	poznávací
značky	značka	k1gFnSc2	značka
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vozidle	vozidlo	k1gNnSc6	vozidlo
připevněna	připevnit	k5eAaPmNgFnS	připevnit
poblíž	poblíž	k7c2	poblíž
registrační	registrační	k2eAgFnSc2d1	registrační
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
bílé	bílý	k2eAgFnSc2d1	bílá
elipsy	elipsa	k1gFnSc2	elipsa
(	(	kIx(	(
<g/>
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
minimálně	minimálně	k6eAd1	minimálně
175	[number]	k4	175
×	×	k?	×
115	[number]	k4	115
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
černými	černý	k2eAgNnPc7d1	černé
písmeny	písmeno	k1gNnPc7	písmeno
(	(	kIx(	(
<g/>
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
nejméně	málo	k6eAd3	málo
80	[number]	k4	80
mm	mm	kA	mm
<g/>
)	)	kIx)	)
označení	označení	k1gNnSc1	označení
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Provedení	provedení	k1gNnSc1	provedení
a	a	k8xC	a
umístění	umístění	k1gNnSc1	umístění
MPZ	MPZ	kA	MPZ
upravuje	upravovat	k5eAaImIp3nS	upravovat
vyhláška	vyhláška	k1gFnSc1	vyhláška
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
spojů	spoj	k1gInPc2	spoj
č.	č.	k?	č.
243	[number]	k4	243
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
§	§	k?	§
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
rozlišovací	rozlišovací	k2eAgFnSc1d1	rozlišovací
značka	značka	k1gFnSc1	značka
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
vozidle	vozidlo	k1gNnSc6	vozidlo
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
vozidlo	vozidlo	k1gNnSc1	vozidlo
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
registrační	registrační	k2eAgFnSc7d1	registrační
značkou	značka	k1gFnSc7	značka
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
pruhem	pruh	k1gInSc7	pruh
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
vydanou	vydaný	k2eAgFnSc7d1	vydaná
správními	správní	k2eAgInPc7d1	správní
orgány	orgán	k1gInPc7	orgán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
poznávacích	poznávací	k2eAgFnPc2d1	poznávací
značek	značka	k1gFnPc2	značka
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
poznávací	poznávací	k2eAgFnSc1d1	poznávací
značka	značka	k1gFnSc1	značka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
