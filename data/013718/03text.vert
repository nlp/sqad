<s>
Josef	Josef	k1gMnSc1
Šimáček	Šimáček	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Šimáček	Šimáček	k1gMnSc1
Josef	Josef	k1gMnSc1
Šimáček	Šimáček	k1gMnSc1
(	(	kIx(
<g/>
1899	#num#	k4
<g/>
)	)	kIx)
<g/>
Rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Jozef	Jozef	k1gInSc1
Šimaček	Šimačka	k1gFnPc2
Narození	narození	k1gNnSc2
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1837	#num#	k4
<g/>
Kostelec	Kostelec	k1gInSc1
nad	nad	k7c4
LabemRakouské	LabemRakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1904	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
67	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Český	český	k2eAgInSc1d1
BrodRakousko-Uhersko	BrodRakousko-Uherska	k1gFnSc5
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
Národnost	národnost	k1gFnSc1
</s>
<s>
česká	český	k2eAgNnPc1d1
Povolání	povolání	k1gNnPc1
</s>
<s>
učitel	učitel	k1gMnSc1
a	a	k8xC
redaktor	redaktor	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
František	František	k1gMnSc1
Šimáček	Šimáček	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Šimáček	Šimáček	k1gMnSc1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1837	#num#	k4
Kostelec	Kostelec	k1gInSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1904	#num#	k4
Český	český	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
vinař	vinař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Studoval	studovat	k5eAaImAgInS
na	na	k7c6
vyšší	vysoký	k2eAgFnSc6d2
reálce	reálka	k1gFnSc6
a	a	k8xC
technice	technika	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1859	#num#	k4
<g/>
–	–	k?
<g/>
1863	#num#	k4
vyučoval	vyučovat	k5eAaImAgMnS
přírodní	přírodní	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
na	na	k7c6
reálné	reálný	k2eAgFnSc6d1
škole	škola	k1gFnSc6
v	v	k7c6
Jičíně	Jičín	k1gInSc6
a	a	k8xC
následujících	následující	k2eAgNnPc2d1
šest	šest	k4xCc4
let	léto	k1gNnPc2
na	na	k7c6
staroměstské	staroměstský	k2eAgFnSc6d1
reálce	reálka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
svých	svůj	k3xOyFgFnPc2
učitelských	učitelský	k2eAgFnPc2d1
povinností	povinnost	k1gFnPc2
pořádal	pořádat	k5eAaImAgInS
přednášky	přednáška	k1gFnPc4
v	v	k7c6
oboru	obor	k1gInSc6
chemie	chemie	k1gFnSc2
a	a	k8xC
psal	psát	k5eAaImAgInS
články	článek	k1gInPc4
do	do	k7c2
odborných	odborný	k2eAgInPc2d1
časopisů	časopis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1869	#num#	k4
jej	on	k3xPp3gMnSc4
majitel	majitel	k1gMnSc1
lobkovického	lobkovický	k2eAgNnSc2d1
panství	panství	k1gNnSc2
v	v	k7c6
Dolních	dolní	k2eAgFnPc6d1
Beřkovicích	Beřkovice	k1gFnPc6
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
z	z	k7c2
Lobkovic	Lobkovice	k1gInPc2
<g/>
,	,	kIx,
požádal	požádat	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
na	na	k7c6
jeho	jeho	k3xOp3gInPc6
pozemcích	pozemek	k1gInPc6
obnovil	obnovit	k5eAaPmAgInS
vinice	vinice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
vinařství	vinařství	k1gNnSc4
v	v	k7c6
Polabí	Polabí	k1gNnSc6
<g/>
,	,	kIx,
kdysi	kdysi	k6eAd1
založené	založený	k2eAgInPc1d1
Karlem	Karel	k1gMnSc7
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
staletích	staletí	k1gNnPc6
(	(	kIx(
<g/>
hlavně	hlavně	k6eAd1
po	po	k7c6
bělohorské	bělohorský	k2eAgFnSc6d1
porážce	porážka	k1gFnSc6
<g/>
)	)	kIx)
upadalo	upadat	k5eAaImAgNnS,k5eAaPmAgNnS
a	a	k8xC
záměrem	záměr	k1gInSc7
Lobkoviců	Lobkovice	k1gMnPc2
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
navrátit	navrátit	k5eAaPmF
mu	on	k3xPp3gMnSc3
někdejší	někdejší	k2eAgFnSc4d1
slávu	sláva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Šimáček	Šimáček	k1gMnSc1
si	se	k3xPyFc3
jako	jako	k9
ředitel	ředitel	k1gMnSc1
vinic	vinice	k1gFnPc2
získal	získat	k5eAaPmAgMnS
brzy	brzy	k6eAd1
pověst	pověst	k1gFnSc4
významného	významný	k2eAgMnSc4d1
odborníka	odborník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
třiceti	třicet	k4xCc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
obnovit	obnovit	k5eAaPmF
tradiční	tradiční	k2eAgInPc4d1
druhy	druh	k1gInPc4
vinné	vinný	k2eAgFnSc2d1
révy	réva	k1gFnSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
stoupla	stoupnout	k5eAaPmAgFnS
kvalita	kvalita	k1gFnSc1
vína	víno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
se	se	k3xPyFc4
i	i	k9
propagaci	propagace	k1gFnSc4
—	—	k?
pořádal	pořádat	k5eAaImAgMnS
přednášky	přednáška	k1gFnPc4
<g/>
,	,	kIx,
publikoval	publikovat	k5eAaBmAgMnS
odborné	odborný	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
,	,	kIx,
reprezentoval	reprezentovat	k5eAaImAgMnS
české	český	k2eAgNnSc4d1
vinařství	vinařství	k1gNnSc4
na	na	k7c6
výstavách	výstava	k1gFnPc6
a	a	k8xC
kongresech	kongres	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Obor	obor	k1gInSc1
podporoval	podporovat	k5eAaImAgInS
i	i	k8xC
veřejnou	veřejný	k2eAgFnSc7d1
a	a	k8xC
organizátorskou	organizátorský	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přispěl	přispět	k5eAaPmAgMnS
k	k	k7c3
založení	založení	k1gNnSc3
vinařského	vinařský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
na	na	k7c6
Mělnicku	Mělnicko	k1gNnSc6
a	a	k8xC
r.	r.	kA
1882	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc7
jednatelem	jednatel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasloužil	zasloužit	k5eAaPmAgMnS
se	se	k3xPyFc4
o	o	k7c4
zřízení	zřízení	k1gNnSc4
české	český	k2eAgFnSc2d1
vinařsko-ovocnické	vinařsko-ovocnický	k2eAgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
Mělníku	Mělník	k1gInSc6
a	a	k8xC
německé	německý	k2eAgFnPc1d1
vinařské	vinařský	k2eAgFnPc1d1
školy	škola	k1gFnPc1
v	v	k7c6
Litoměřicích	Litoměřice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1892	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
členem	člen	k1gMnSc7
českého	český	k2eAgInSc2d1
odboru	odbor	k1gInSc2
zemědělské	zemědělský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
pro	pro	k7c4
Království	království	k1gNnSc4
české	český	k2eAgFnSc2d1
a	a	k8xC
komise	komise	k1gFnSc2
pro	pro	k7c4
boj	boj	k1gInSc4
s	s	k7c7
phyloxerou	phyloxera	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1898	#num#	k4
získal	získat	k5eAaPmAgInS
za	za	k7c4
dlouholetou	dlouholetý	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
zlatý	zlatý	k2eAgInSc4d1
záslužný	záslužný	k2eAgInSc4d1
kříž	kříž	k1gInSc4
s	s	k7c7
korunou	koruna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc7
úsilím	úsilí	k1gNnSc7
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
pozvednout	pozvednout	k5eAaPmF
úroveň	úroveň	k1gFnSc4
vinařství	vinařství	k1gNnSc2
v	v	k7c6
celých	celý	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Šimáček	Šimáček	k1gMnSc1
během	během	k7c2
života	život	k1gInSc2
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
asi	asi	k9
třicet	třicet	k4xCc4
odborných	odborný	k2eAgFnPc2d1
publikací	publikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
k	k	k7c3
nim	on	k3xPp3gInPc3
<g/>
:	:	kIx,
</s>
<s>
Důležitější	důležitý	k2eAgFnPc4d2
listiny	listina	k1gFnPc4
a	a	k8xC
zápisky	zápisek	k1gInPc4
bývalého	bývalý	k2eAgInSc2d1
poctivého	poctivý	k2eAgInSc2d1
pořádku	pořádek	k1gInSc2
vinařského	vinařský	k2eAgInSc2d1
král	král	k1gMnSc1
<g/>
.	.	kIx.
věnného	věnný	k2eAgNnSc2d1
města	město	k1gNnSc2
Mělníka	Mělník	k1gInSc2
(	(	kIx(
<g/>
1877	#num#	k4
<g/>
;	;	kIx,
soubor	soubor	k1gInSc1
historických	historický	k2eAgInPc2d1
dokumentů	dokument	k1gInPc2
týkajících	týkající	k2eAgInPc2d1
se	se	k3xPyFc4
mělnického	mělnický	k2eAgNnSc2d1
vinařství	vinařství	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Materiál	materiál	k1gInSc1
k	k	k7c3
technologickému	technologický	k2eAgInSc3d1
slovníku	slovník	k1gInSc3
vinařskému	vinařský	k2eAgInSc3d1
(	(	kIx(
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vinařství	vinařství	k1gNnSc1
v	v	k7c6
království	království	k1gNnSc6
českém	český	k2eAgNnSc6d1
(	(	kIx(
<g/>
česko-německý	česko-německý	k2eAgInSc1d1
sborník	sborník	k1gInSc1
vzniklý	vzniklý	k2eAgInSc1d1
pod	pod	k7c7
jeho	jeho	k3xOp3gFnSc7
redakcí	redakce	k1gFnSc7
<g/>
,	,	kIx,
vyšel	vyjít	k5eAaPmAgMnS
např.	např.	kA
v	v	k7c6
letech	let	k1gInPc6
1886	#num#	k4
<g/>
,	,	kIx,
1890	#num#	k4
a	a	k8xC
1891	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Byl	být	k5eAaImAgMnS
redaktorem	redaktor	k1gMnSc7
časopisu	časopis	k1gInSc2
Český	český	k2eAgMnSc1d1
vinař	vinař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
kolektiv	kolektiv	k1gInSc4
autorů	autor	k1gMnPc2
<g/>
:	:	kIx,
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
farnosti	farnost	k1gFnSc2
Kostelec	Kostelec	k1gInSc4
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Autor	autor	k1gMnSc1
Josef	Josef	k1gMnSc1
Šimáček	Šimáček	k1gMnSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Šimáček	Šimáček	k1gMnSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Josef	Josef	k1gMnSc1
Šimáček	Šimáček	k1gMnSc1
</s>
<s>
Článek	článek	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
s	s	k7c7
využitím	využití	k1gNnSc7
materiálů	materiál	k1gInPc2
z	z	k7c2
Digitálního	digitální	k2eAgInSc2d1
archivu	archiv	k1gInSc2
časopisů	časopis	k1gInPc2
ÚČL	ÚČL	kA
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
v.	v.	k?
v.	v.	k?
i.	i.	k?
(	(	kIx(
<g/>
http://archiv.ucl.cas.cz/	http://archiv.ucl.cas.cz/	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
JEŘÁBEK	Jeřábek	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Šimáček	Šimáček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světozor	světozor	k1gInSc1
<g/>
.	.	kIx.
1899	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
33	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
51	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
610	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Databáze	databáze	k1gFnSc1
autorit	autorita	k1gFnPc2
SVK	SVK	kA
Kladno	Kladno	k1gNnSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1122964	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1034113577	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5799	#num#	k4
2101	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
83920249	#num#	k4
</s>
