<p>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
Myles	Myles	k1gMnSc1	Myles
Dworkin	Dworkin	k1gMnSc1	Dworkin
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1931	[number]	k4	1931
Providence	providence	k1gFnSc2	providence
<g/>
,	,	kIx,	,
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
USA	USA	kA	USA
−	−	k?	−
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
filosof	filosof	k1gMnSc1	filosof
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
představitel	představitel	k1gMnSc1	představitel
politické	politický	k2eAgFnSc2d1	politická
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c4	na
University	universita	k1gFnPc4	universita
College	College	k1gInSc4	College
London	London	k1gMnSc1	London
a	a	k8xC	a
na	na	k7c4	na
New	New	k1gFnSc4	New
York	York	k1gInSc4	York
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
Dworkin	Dworkin	k1gMnSc1	Dworkin
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
v	v	k7c4	v
Providence	providence	k1gFnPc4	providence
v	v	k7c6	v
státe	stát	k1gInSc5	stát
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Bakalariát	Bakalariát	k1gMnSc1	Bakalariát
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
na	na	k7c4	na
Harvard	Harvard	k1gInSc4	Harvard
University	universita	k1gFnSc2	universita
a	a	k8xC	a
na	na	k7c6	na
Magdalen	Magdalena	k1gFnPc2	Magdalena
College	Colleg	k1gInSc2	Colleg
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
učiteli	učitel	k1gMnPc7	učitel
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Sir	sir	k1gMnSc1	sir
Rupert	Rupert	k1gMnSc1	Rupert
Cross	Cross	k1gInSc4	Cross
<g/>
.	.	kIx.	.
</s>
<s>
Dworkin	Dworkin	k1gInSc1	Dworkin
poté	poté	k6eAd1	poté
studoval	studovat	k5eAaImAgInS	studovat
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
Harvard	Harvard	k1gInSc4	Harvard
Law	Law	k1gFnSc2	Law
School	Schoola	k1gFnPc2	Schoola
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
asistentem	asistent	k1gMnSc7	asistent
soudce	soudce	k1gMnSc1	soudce
Learned	Learned	k1gMnSc1	Learned
Handa	Handa	k1gMnSc1	Handa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
u	u	k7c2	u
amerického	americký	k2eAgInSc2d1	americký
Federálního	federální	k2eAgInSc2d1	federální
odvolacího	odvolací	k2eAgInSc2d1	odvolací
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Soudce	soudce	k1gMnSc1	soudce
Hand	Hand	k1gMnSc1	Hand
označil	označit	k5eAaPmAgMnS	označit
Dworkina	Dworkina	k1gMnSc1	Dworkina
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
asistenta	asistent	k1gMnSc4	asistent
a	a	k8xC	a
Dworkin	Dworkin	k1gInSc1	Dworkin
uznává	uznávat	k5eAaImIp3nS	uznávat
soudce	soudce	k1gMnSc4	soudce
Handa	Hand	k1gMnSc4	Hand
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
mentorů	mentor	k1gInPc2	mentor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dworkin	Dworkin	k1gMnSc1	Dworkin
pak	pak	k6eAd1	pak
krátce	krátce	k6eAd1	krátce
pracoval	pracovat	k5eAaImAgMnS	pracovat
u	u	k7c2	u
významné	významný	k2eAgFnSc2d1	významná
právnické	právnický	k2eAgFnSc2d1	právnická
firmy	firma	k1gFnSc2	firma
Sullivan	Sullivan	k1gMnSc1	Sullivan
and	and	k?	and
Cromwell	Cromwell	k1gMnSc1	Cromwell
v	v	k7c6	v
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
City	City	k1gFnPc2	City
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
byl	být	k5eAaImAgMnS	být
profesorem	profesor	k1gMnSc7	profesor
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
Yale	Yale	k1gNnSc4	Yale
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
jako	jako	k8xC	jako
držitel	držitel	k1gMnSc1	držitel
Hohfeldovy	Hohfeldův	k2eAgFnSc2d1	Hohfeldův
stolice	stolice	k1gFnSc2	stolice
právní	právní	k2eAgFnSc2d1	právní
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
po	po	k7c6	po
H.	H.	kA	H.
L.	L.	kA	L.
A.	A.	kA	A.
Hartovi	Hart	k1gMnSc3	Hart
na	na	k7c6	na
stolici	stolice	k1gFnSc6	stolice
právních	právní	k2eAgFnPc2d1	právní
věd	věda	k1gFnPc2	věda
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
byl	být	k5eAaImAgInS	být
také	také	k9	také
profesorem	profesor	k1gMnSc7	profesor
práv	právo	k1gNnPc2	právo
na	na	k7c4	na
New	New	k1gFnSc4	New
York	York	k1gInSc4	York
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Oxfordu	Oxford	k1gInSc2	Oxford
přednášel	přednášet	k5eAaImAgInS	přednášet
také	také	k9	také
na	na	k7c4	na
University	universita	k1gFnPc4	universita
college	collegat	k5eAaPmIp3nS	collegat
London	London	k1gMnSc1	London
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
přispěvatelem	přispěvatel	k1gMnSc7	přispěvatel
pro	pro	k7c4	pro
The	The	k1gFnSc4	The
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Review	Review	k1gMnSc1	Review
of	of	k?	of
Books	Books	k1gInSc1	Books
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Myšlení	myšlení	k1gNnSc1	myšlení
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Dworkin	Dworkin	k1gMnSc1	Dworkin
byl	být	k5eAaImAgMnS	být
radikální	radikální	k2eAgMnPc4d1	radikální
zastánce	zastánce	k1gMnPc4	zastánce
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
právo	právo	k1gNnSc1	právo
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
především	především	k9	především
historicky	historicky	k6eAd1	historicky
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
interpretovat	interpretovat	k5eAaBmF	interpretovat
a	a	k8xC	a
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Dworkin	Dworkin	k1gMnSc1	Dworkin
odmítal	odmítat	k5eAaImAgMnS	odmítat
pozitivistické	pozitivistický	k2eAgNnSc4d1	pozitivistické
pojetí	pojetí	k1gNnSc4	pojetí
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
teorie	teorie	k1gFnSc2	teorie
svého	svůj	k3xOyFgMnSc2	svůj
oxfordského	oxfordský	k2eAgMnSc2d1	oxfordský
předchůdce	předchůdce	k1gMnSc2	předchůdce
H.	H.	kA	H.
L.	L.	kA	L.
A.	A.	kA	A.
Harta	Harta	k1gMnSc1	Harta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
začíná	začínat	k5eAaImIp3nS	začínat
od	od	k7c2	od
oprávnění	oprávnění	k1gNnSc2	oprávnění
zákonodárce	zákonodárce	k1gMnSc2	zákonodárce
a	a	k8xC	a
nezabývá	zabývat	k5eNaImIp3nS	zabývat
se	se	k3xPyFc4	se
souvislostí	souvislost	k1gFnSc7	souvislost
práva	právo	k1gNnSc2	právo
s	s	k7c7	s
morálkou	morálka	k1gFnSc7	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
podle	podle	k7c2	podle
Dworkina	Dworkino	k1gNnSc2	Dworkino
naopak	naopak	k6eAd1	naopak
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
práv	právo	k1gNnPc2	právo
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
vývoji	vývoj	k1gInSc6	vývoj
i	i	k8xC	i
výkladu	výklad	k1gInSc6	výklad
s	s	k7c7	s
morálkou	morálka	k1gFnSc7	morálka
nutně	nutně	k6eAd1	nutně
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
právní	právní	k2eAgFnSc2d1	právní
teorie	teorie	k1gFnSc2	teorie
je	být	k5eAaImIp3nS	být
poradit	poradit	k5eAaPmF	poradit
soudci	soudce	k1gMnSc3	soudce
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
má	mít	k5eAaImIp3nS	mít
správně	správně	k6eAd1	správně
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
podle	podle	k7c2	podle
Dworkina	Dworkin	k1gMnSc2	Dworkin
by	by	kYmCp3nS	by
dokonale	dokonale	k6eAd1	dokonale
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
dost	dost	k6eAd1	dost
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
vždycky	vždycky	k6eAd1	vždycky
našel	najít	k5eAaPmAgMnS	najít
to	ten	k3xDgNnSc4	ten
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
R.	R.	kA	R.
Dworkin	Dworkin	k1gInSc1	Dworkin
<g/>
,	,	kIx,	,
Když	když	k8xS	když
se	se	k3xPyFc4	se
práva	právo	k1gNnPc1	právo
berou	brát	k5eAaImIp3nP	brát
vážně	vážně	k6eAd1	vážně
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
OIKOYMENH	OIKOYMENH	kA	OIKOYMENH
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
-	-	kIx~	-
455	[number]	k4	455
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-7298-022-X	[number]	k4	80-7298-022-X
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Lidská	lidský	k2eAgNnPc1d1	lidské
práva	právo	k1gNnPc1	právo
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ronald	Ronald	k1gMnSc1	Ronald
Dworkin	Dworkin	k2eAgMnSc1d1	Dworkin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Ronald	Ronald	k1gMnSc1	Ronald
Dworkin	Dworkin	k1gMnSc1	Dworkin
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
R.	R.	kA	R.
Dworkin	Dworkin	k1gMnSc1	Dworkin
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Conservative	Conservativ	k1gInSc5	Conservativ
Phalanx	Phalanx	k1gInSc1	Phalanx
Ruling	Ruling	k1gInSc1	Ruling
the	the	k?	the
Supreme	Suprem	k1gInSc5	Suprem
Court	Court	k1gInSc1	Court
</s>
</p>
<p>
<s>
Dworkin	Dworkin	k1gInSc1	Dworkin
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
NYU	NYU	kA	NYU
Law	Law	k1gMnPc7	Law
Faculty	Facult	k1gInPc7	Facult
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
na	na	k7c6	na
UCL	UCL	kA	UCL
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Ronald	Ronald	k1gMnSc1	Ronald
Dworkin	Dworkin	k1gMnSc1	Dworkin
-	-	kIx~	-
Mr	Mr	k1gMnSc1	Mr
Justice	justice	k1gFnSc2	justice
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
článek	článek	k1gInSc1	článek
v	v	k7c4	v
Times	Times	k1gInSc4	Times
literary	literara	k1gFnSc2	literara
supplement	supplement	k1gInSc1	supplement
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
Stanford	Stanford	k1gInSc1	Stanford
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Interpretation	Interpretation	k1gInSc1	Interpretation
and	and	k?	and
Coherence	Coherence	k1gFnSc2	Coherence
in	in	k?	in
Legal	Legal	k1gInSc1	Legal
Reasoning	Reasoning	k1gInSc1	Reasoning
</s>
</p>
<p>
<s>
Stanford	Stanford	k1gInSc1	Stanford
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Interpretivist	Interpretivist	k1gInSc1	Interpretivist
Theories	Theories	k1gMnSc1	Theories
of	of	k?	of
Law	Law	k1gMnSc1	Law
</s>
</p>
