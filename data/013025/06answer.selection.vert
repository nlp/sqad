<s>
Bakalariát	Bakalariát	k1gMnSc1	Bakalariát
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
na	na	k7c4	na
Harvard	Harvard	k1gInSc4	Harvard
University	universita	k1gFnSc2	universita
a	a	k8xC	a
na	na	k7c6	na
Magdalen	Magdalena	k1gFnPc2	Magdalena
College	Colleg	k1gInSc2	Colleg
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
učiteli	učitel	k1gMnPc7	učitel
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Sir	sir	k1gMnSc1	sir
Rupert	Rupert	k1gMnSc1	Rupert
Cross	Cross	k1gInSc4	Cross
<g/>
.	.	kIx.	.
</s>
