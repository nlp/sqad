<s>
Braniborská	braniborský	k2eAgFnSc1d1	Braniborská
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Brandenburger	Brandenburger	k1gMnSc1	Brandenburger
Tor	Tor	k1gMnSc1	Tor
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
pamětihodnostem	pamětihodnost	k1gFnPc3	pamětihodnost
Berlína	Berlín	k1gInSc2	Berlín
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
významným	významný	k2eAgInPc3d1	významný
symbolům	symbol	k1gInPc3	symbol
<g/>
.	.	kIx.	.
</s>
