<s>
Hřib	hřib	k1gInSc1
Špinarův	Špinarův	k2eAgInSc1d1
</s>
<s>
Hřib	hřib	k1gInSc1
Špinarův	Špinarův	k2eAgInSc1d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc2
Říše	říš	k1gFnSc2
</s>
<s>
houby	houba	k1gFnPc1
(	(	kIx(
<g/>
Fungi	Fungi	k1gNnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
houby	houby	k6eAd1
stopkovýtrusné	stopkovýtrusný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Basidiomycota	Basidiomycota	k1gFnSc1
<g/>
)	)	kIx)
Pododdělení	pododdělení	k1gNnSc1
</s>
<s>
Agaricomycotina	Agaricomycotina	k1gFnSc1
Třída	třída	k1gFnSc1
</s>
<s>
Agaricomycetes	Agaricomycetes	k1gInSc1
Řád	řád	k1gInSc1
</s>
<s>
hřibotvaré	hřibotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Boletales	Boletales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
hřibovité	hřibovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Boletaceae	Boletacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Rubroboletus	Rubroboletus	k1gInSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Rubroboletus	Rubroboletus	k1gMnSc1
legaliae	legalia	k1gFnSc2
f.	f.	k?
spinarii	spinarie	k1gFnSc4
<g/>
(	(	kIx(
<g/>
Hlaváček	Hlaváček	k1gMnSc1
<g/>
)	)	kIx)
Mikšík	Mikšík	k1gInSc1
2015	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hřib	hřib	k1gInSc1
Špinarův	Špinarův	k2eAgInSc1d1
neboli	neboli	k8xC
hřib	hřib	k1gInSc1
Le	Le	k1gFnSc2
Galové	Galové	k2eAgFnSc2d1
Špinarův	Špinarův	k2eAgInSc4d1
(	(	kIx(
<g/>
Rubroboletus	Rubroboletus	k1gInSc4
legaliae	legaliae	k1gFnSc4
f.	f.	k?
spinarii	spinarie	k1gFnSc3
(	(	kIx(
<g/>
Hlaváček	hlaváček	k1gInSc1
<g/>
)	)	kIx)
Mikšík	Mikšík	k1gInSc1
2015	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jedovatá	jedovatý	k2eAgFnSc1d1
houba	houba	k1gFnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
hřibovitých	hřibovitý	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Synonyma	synonymum	k1gNnPc1
</s>
<s>
Boletus	Boletus	k1gInSc4
spinarii	spinarie	k1gFnSc3
Hlaváček	hlaváček	k1gInSc1
2000	#num#	k4
</s>
<s>
Boletus	Boletus	k1gMnSc1
legaliae	legalia	k1gFnSc2
f.	f.	k?
spinarii	spinarie	k1gFnSc4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Hlaváček	Hlaváček	k1gMnSc1
<g/>
)	)	kIx)
Janda	Janda	k1gMnSc1
2009	#num#	k4
</s>
<s>
Taxonomie	taxonomie	k1gFnSc1
</s>
<s>
Hřib	hřib	k1gInSc1
Špinarův	Špinarův	k2eAgInSc1d1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
popsán	popsán	k2eAgInSc1d1
roku	rok	k1gInSc2
2000	#num#	k4
Jiřím	Jiří	k1gMnSc7
Hlaváčkem	Hlaváček	k1gMnSc7
jakožto	jakožto	k8xS
modrající	modrající	k2eAgInSc1d1
druh	druh	k1gInSc1
z	z	k7c2
okruhu	okruh	k1gInSc2
hřibu	hřib	k1gInSc2
královského	královský	k2eAgInSc2d1
<g/>
,	,	kIx,
tedy	tedy	k9
sekce	sekce	k1gFnSc1
Appendiculati	Appendiculati	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
mykolog	mykolog	k1gMnSc1
Václav	Václav	k1gMnSc1
Janda	Janda	k1gMnSc1
přeřadil	přeřadit	k5eAaPmAgMnS
na	na	k7c4
formu	forma	k1gFnSc4
hřibu	hřib	k1gInSc2
Le	Le	k1gFnSc2
Galové	Galové	k2eAgFnSc2d1
(	(	kIx(
<g/>
sekce	sekce	k1gFnSc2
Luridi	Lurid	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
běžné	běžný	k2eAgFnSc2d1
formy	forma	k1gFnSc2
hřibu	hřib	k1gInSc2
Le	Le	k1gMnPc2
Galové	Galové	k2eAgMnPc2d1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
především	především	k6eAd1
barvou	barva	k1gFnSc7
pórů	pór	k1gInPc2
rourek	rourka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
obou	dva	k4xCgInPc2
hřibů	hřib	k1gInPc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
mládí	mládí	k1gNnSc6
žluté	žlutý	k2eAgFnPc1d1
<g/>
,	,	kIx,
po	po	k7c6
rozvití	rozvití	k1gNnSc6
klobouku	klobouk	k1gInSc2
se	se	k3xPyFc4
zbarvují	zbarvovat	k5eAaImIp3nP
do	do	k7c2
různých	různý	k2eAgInPc2d1
odstínů	odstín	k1gInPc2
červené	červený	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
toto	tento	k3xDgNnSc4
zbarvení	zbarvení	k1gNnSc4
u	u	k7c2
hřibu	hřib	k1gInSc2
Špinarova	Špinarův	k2eAgNnSc2d1
mizí	mizet	k5eAaImIp3nS
a	a	k8xC
opět	opět	k6eAd1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
žluté	žlutý	k2eAgNnSc1d1
(	(	kIx(
<g/>
následně	následně	k6eAd1
vystřídané	vystřídaný	k2eAgFnPc1d1
olivovým	olivový	k2eAgMnPc3d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Hřib	hřib	k1gInSc1
Špinarův	Špinarův	k2eAgInSc1d1
se	se	k3xPyFc4
oproti	oproti	k7c3
hřibu	hřib	k1gInSc3
Le	Le	k1gFnSc2
Galové	Galová	k1gFnSc2
liší	lišit	k5eAaImIp3nS
žlutými	žlutý	k2eAgInPc7d1
póry	pór	k1gInPc7
</s>
<s>
Klobouk	klobouk	k1gInSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
průměru	průměr	k1gInSc6
60-140	60-140	k4
(	(	kIx(
<g/>
180	#num#	k4
<g/>
)	)	kIx)
milimetrů	milimetr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
povrch	povrch	k6eAd1wR
je	být	k5eAaImIp3nS
původně	původně	k6eAd1
špinavě	špinavě	k6eAd1
bělavý	bělavý	k2eAgInSc4d1
či	či	k8xC
našedlý	našedlý	k2eAgInSc4d1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
na	na	k7c6
temeni	temeno	k1gNnSc6
nahnědlý	nahnědlý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stáří	stáří	k1gNnSc6
a	a	k8xC
obzvlášť	obzvlášť	k6eAd1
v	v	k7c6
důsledku	důsledek	k1gInSc6
deštivého	deštivý	k2eAgNnSc2d1
počasí	počasí	k1gNnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
povrchová	povrchový	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
smyta	smyt	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
důsledku	důsledek	k1gInSc6
čehož	což	k3yRnSc2,k3yQnSc2
je	být	k5eAaImIp3nS
patrná	patrný	k2eAgFnSc1d1
narůžovělá	narůžovělý	k2eAgFnSc1d1
<g/>
,	,	kIx,
růžová	růžový	k2eAgFnSc1d1
až	až	k9
téměř	téměř	k6eAd1
červená	červený	k2eAgFnSc1d1
barva	barva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stáří	stáří	k1gNnSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
povrch	povrch	k1gInSc4
rozpraskaný	rozpraskaný	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rourky	rourka	k1gFnPc1
jsou	být	k5eAaImIp3nP
před	před	k7c7
rozvitím	rozvití	k1gNnSc7
jasně	jasně	k6eAd1
žluté	žlutý	k2eAgFnSc2d1
<g/>
,	,	kIx,
ve	v	k7c6
stáří	stáří	k1gNnSc6
žlutoolivové	žlutoolivový	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Póry	pór	k1gInPc7
mají	mít	k5eAaImIp3nP
do	do	k7c2
rozvití	rozvití	k1gNnSc2
žlutou	žlutý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
,	,	kIx,
poté	poté	k6eAd1
na	na	k7c4
krátkou	krátký	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
červenají	červenat	k5eAaImIp3nP
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
jen	jen	k9
načervenalé	načervenalý	k2eAgInPc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
později	pozdě	k6eAd2
opět	opět	k6eAd1
přecházejí	přecházet	k5eAaImIp3nP
do	do	k7c2
žluté	žlutý	k2eAgFnSc2d1
a	a	k8xC
ve	v	k7c6
staří	starý	k1gMnPc1
na	na	k7c4
olivově	olivově	k6eAd1
zelenou	zelený	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třeň	třeň	k1gInSc1
u	u	k7c2
země	zem	k1gFnSc2
bývá	bývat	k5eAaImIp3nS
růžový	růžový	k2eAgMnSc1d1
<g/>
,	,	kIx,
výše	vysoce	k6eAd2
přechází	přecházet	k5eAaImIp3nS
dožluta	dožlut	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žlutou	žlutý	k2eAgFnSc4d1
část	část	k1gFnSc4
kryje	krýt	k5eAaImIp3nS
síťka	síťka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
má	mít	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
odstíny	odstín	k1gInPc4
červené	červený	k2eAgInPc4d1
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
však	však	k9
být	být	k5eAaImF
odbarvená	odbarvený	k2eAgFnSc1d1
(	(	kIx(
<g/>
žlutá	žlutý	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dužnina	dužnina	k1gFnSc1
po	po	k7c6
nakrojení	nakrojení	k1gNnSc6
či	či	k8xC
poškození	poškození	k1gNnSc6
modrá	modrat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
zasychání	zasychání	k1gNnSc6
je	být	k5eAaImIp3nS
cítit	cítit	k5eAaImF
po	po	k7c6
polévkovém	polévkový	k2eAgNnSc6d1
koření	koření	k1gNnSc6
Maggi	maggi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Hřib	hřib	k1gInSc1
Špinarův	Špinarův	k2eAgInSc1d1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
vzácná	vzácný	k2eAgFnSc1d1
houba	houba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
vyskytuje	vyskytovat	k5eAaImIp3nS
jen	jen	k9
na	na	k7c6
několika	několik	k4yIc6
lokalitách	lokalita	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
nejznámějších	známý	k2eAgInPc2d3
je	být	k5eAaImIp3nS
národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Luční	luční	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výskyt	výskyt	k1gInSc4
této	tento	k3xDgFnSc2
teplomilné	teplomilný	k2eAgFnSc2d1
houby	houba	k1gFnSc2
je	být	k5eAaImIp3nS
vázaný	vázaný	k2eAgMnSc1d1
na	na	k7c4
duby	dub	k1gInPc4
<g/>
,	,	kIx,
často	často	k6eAd1
je	být	k5eAaImIp3nS
proto	proto	k8xC
spojená	spojený	k2eAgFnSc1d1
s	s	k7c7
hrázemi	hráz	k1gFnPc7
rybníků	rybník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Záměna	záměna	k1gFnSc1
</s>
<s>
Vlivem	vliv	k1gInSc7
barevné	barevný	k2eAgFnSc2d1
variability	variabilita	k1gFnSc2
související	související	k2eAgFnPc4d1
s	s	k7c7
počasím	počasí	k1gNnSc7
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
vzhled	vzhled	k1gInSc1
blížit	blížit	k5eAaImF
několika	několik	k4yIc3
dalším	další	k2eAgInPc3d1
hřibům	hřib	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodnice	plodnice	k1gFnSc1
s	s	k7c7
růžovým	růžový	k2eAgInSc7d1
kloboukem	klobouk	k1gInSc7
a	a	k8xC
již	již	k6eAd1
odbarvenými	odbarvený	k2eAgInPc7d1
póry	pór	k1gInPc7
mohou	moct	k5eAaImIp3nP
připomínat	připomínat	k5eAaImF
hřib	hřib	k1gInSc4
královský	královský	k2eAgInSc4d1
(	(	kIx(
<g/>
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
však	však	k9
při	při	k7c6
poškození	poškození	k1gNnSc6
nemodrá	modrat	k5eNaImIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
hřib	hřib	k1gInSc1
růžovník	růžovník	k1gInSc1
(	(	kIx(
<g/>
jehož	jehož	k3xOyRp3gFnSc1
síťka	síťka	k1gFnSc1
a	a	k8xC
póry	pór	k1gInPc4
ale	ale	k8xC
nemívají	mívat	k5eNaImIp3nP
červenou	červený	k2eAgFnSc4d1
či	či	k8xC
načervenalou	načervenalý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodnice	plodnice	k1gFnSc1
s	s	k7c7
bílým	bílý	k2eAgInSc7d1
kloboukem	klobouk	k1gInSc7
mohou	moct	k5eAaImIp3nP
během	během	k7c2
krátké	krátký	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jejich	jejich	k3xOp3gFnPc1
rourky	rourka	k1gFnPc1
nabírají	nabírat	k5eAaImIp3nP
červenou	červený	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
,	,	kIx,
připomínat	připomínat	k5eAaImF
hřib	hřib	k1gInSc4
satan	satan	k1gInSc1
či	či	k8xC
hřib	hřib	k1gInSc1
nachový	nachový	k2eAgInSc1d1
(	(	kIx(
<g/>
ten	ten	k3xDgMnSc1
má	mít	k5eAaImIp3nS
výrazněji	výrazně	k6eAd2
vyvinutou	vyvinutý	k2eAgFnSc4d1
síťku	síťka	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
hřib	hřib	k1gInSc1
královský	královský	k2eAgInSc1d1
</s>
<s>
hřib	hřib	k1gInSc1
růžovník	růžovník	k1gInSc1
</s>
<s>
hřib	hřib	k1gInSc1
satan	satan	k1gInSc1
</s>
<s>
hřib	hřib	k1gInSc1
nachový	nachový	k2eAgInSc4d1
</s>
<s>
Hřib	hřib	k1gInSc1
Le	Le	k1gFnSc2
Galové	Galová	k1gFnSc2
</s>
<s>
Hřib	hřib	k1gInSc1
přívěskatý	přívěskatý	k2eAgInSc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
ŠUTARA	ŠUTARA	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
MIKŠÍK	MIKŠÍK	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
;	;	kIx,
JANDA	Janda	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hřibovité	hřibovitý	k2eAgFnPc4d1
houby	houba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
294	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1717	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Hřib	hřib	k1gInSc4
Le	Le	k1gFnSc2
Galové	Galová	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
180	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hřib	hřib	k1gInSc1
Špinarův	Špinarův	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Boletus	Boletus	k1gInSc1
spinarii	spinarie	k1gFnSc4
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Boletus	Boletus	k1gMnSc1
spinarii	spinarie	k1gFnSc4
(	(	kIx(
<g/>
hřib	hřib	k1gInSc4
Špinarův	Špinarův	k2eAgInSc4d1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
NašeHouby	NašeHouba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Hřib	hřib	k1gInSc4
Le	Le	k1gFnSc2
Galové	Galové	k2eAgFnSc2d1
Špinarův	Špinarův	k2eAgInSc4d1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Hřibovité	hřibovitý	k2eAgInPc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Aureoboletus	Aureoboletus	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc4
moravský	moravský	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
pružný	pružný	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
štíhlotřenný	štíhlotřenný	k2eAgInSc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Boletus	Boletus	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
borový	borový	k2eAgInSc1d1
(	(	kIx(
<g/>
hnědofialový	hnědofialový	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc4
bronzový	bronzový	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
dubový	dubový	k2eAgInSc4d1
(	(	kIx(
<g/>
habrový	habrový	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc4
Melzerův	Melzerův	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
nádherný	nádherný	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
smrkový	smrkový	k2eAgInSc4d1
(	(	kIx(
<g/>
březový	březový	k2eAgInSc4d1
<g/>
,	,	kIx,
citronový	citronový	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc1
šumavský	šumavský	k2eAgInSc1d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Buchwaldoboletus	Buchwaldoboletus	k1gInSc1
<g/>
)	)	kIx)
<g/>
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Chalciporus	Chalciporus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
dřevožijný	dřevožijný	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
sírovýHřib	sírovýHřib	k1gInSc1
maličký	maličký	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
malinový	malinový	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
peprný	peprný	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
rubínový	rubínový	k2eAgInSc1d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Butyriboletus	Butyriboletus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc4
Fechtnerův	Fechtnerův	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
hnědorůžový	hnědorůžový	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
horský	horský	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
královský	královský	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
přívěskatý	přívěskatý	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc1
růžovník	růžovník	k1gInSc1
•	•	k?
Hřib	hřib	k1gInSc4
šedorůžový	šedorůžový	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
Wichanského	Wichanský	k2eAgInSc2d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Caloboletus	Caloboletus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
Kluzákův	Kluzákův	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
kříšť	kříšť	k1gInSc1
•	•	k?
Hřib	hřib	k1gInSc4
medotrpký	medotrpký	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
olivovožlutý	olivovožlutý	k2eAgInSc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Cyanoboletus	Cyanoboletus	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
modračka	modračka	k1gFnSc1
•	•	k?
Hřib	hřib	k1gInSc4
žlutokrvavý	žlutokrvavý	k2eAgInSc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Hemileccinum	Hemileccinum	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
plavý	plavý	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
skvrnitý	skvrnitý	k2eAgInSc1d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Hortiboletus	Hortiboletus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc4
červený	červený	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
Engelův	Engelův	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
lindový	lindový	k2eAgInSc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Imleria	Imlerium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
hnědý	hnědý	k2eAgInSc1d1
(	(	kIx(
<g/>
hnědočervený	hnědočervený	k2eAgInSc1d1
<g/>
,	,	kIx,
naduřelý	naduřelý	k2eAgInSc1d1
<g/>
)	)	kIx)
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Imperator	Imperator	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc4
měďový	měďový	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
rudonachový	rudonachový	k2eAgInSc4d1
(	(	kIx(
<g/>
žlutonachový	žlutonachový	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc1
zavalitý	zavalitý	k2eAgInSc1d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Neoboletus	Neoboletus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
kovář	kovář	k1gMnSc1
(	(	kIx(
<g/>
neměnný	neměnný	k2eAgInSc1d1,k2eNgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc1
žlutý	žlutý	k2eAgInSc1d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Pseudoboletus	Pseudoboletus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
hvězdákožijný	hvězdákožijný	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
příživný	příživný	k2eAgInSc1d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Rheubarbariboletus	Rheubarbariboletus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
broskvový	broskvový	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
meruňkový	meruňkový	k2eAgInSc1d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Rubroboletus	Rubroboletus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
Dupainův	Dupainův	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
Le	Le	k1gFnSc2
Galové	Galová	k1gFnSc2
(	(	kIx(
<g/>
Špinarův	Špinarův	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc4
Moserův	Moserův	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
nachový	nachový	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
narůžovělý	narůžovělý	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc1
satan	satan	k1gInSc1
(	(	kIx(
<g/>
hlohový	hlohový	k2eAgInSc1d1
<g/>
,	,	kIx,
satanovitý	satanovitý	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc4
středomořský	středomořský	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
vlčí	vlčí	k2eAgInSc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Suillellus	Suillellus	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc4
Adalgisin	Adalgisin	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
atlantický	atlantický	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc1
koloděj	koloděj	k1gMnSc1
(	(	kIx(
<g/>
kavkazský	kavkazský	k2eAgMnSc1d1
<g/>
,	,	kIx,
rudomasý	rudomasý	k2eAgMnSc1d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc4
kolodějovitý	kolodějovitý	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
polosíťkovaný	polosíťkovaný	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
Quéletův	Quéletův	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
tajemný	tajemný	k2eAgInSc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Tylopilus	Tylopilus	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
nachovýtrusý	nachovýtrusý	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
žlučník	žlučník	k1gInSc1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Xerocomus	Xerocomus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc4
osmahlý	osmahlý	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
plstnatý	plstnatý	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
topolový	topolový	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
zlatokořenný	zlatokořenný	k2eAgInSc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Xerocomellus	Xerocomellus	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc4
Markův	Markův	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
mokřadní	mokřadní	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
políčkatý	políčkatý	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
Redeuilhův	Redeuilhův	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
sametový	sametový	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
Sarnariho	Sarnari	k1gMnSc2
•	•	k?
Hřib	hřib	k1gInSc1
suchomilný	suchomilný	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
uťatovýtrusý	uťatovýtrusý	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
žlutomasý	žlutomasý	k2eAgMnSc1d1
Kozák	Kozák	k1gMnSc1
(	(	kIx(
<g/>
Leccinellum	Leccinellum	k1gInSc1
<g/>
)	)	kIx)
<g/>
Kozák	kozák	k1gInSc1
a	a	k8xC
křemenáč	křemenáč	k1gInSc1
<g/>
(	(	kIx(
<g/>
Leccinum	Leccinum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kozák	Kozák	k1gMnSc1
dubový	dubový	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
habrový	habrový	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
půvabný	půvabný	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
korsickýKozák	korsickýKozák	k1gMnSc1
bílý	bílý	k1gMnSc1
•	•	k?
Kozák	Kozák	k1gMnSc1
březový	březový	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
černohnědý	černohnědý	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
maličký	maličký	k1gMnSc1
•	•	k?
Kozák	Kozák	k1gMnSc1
pískomilný	pískomilný	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
šedohnědý	šedohnědý	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
šedozelený	šedozelený	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
topolový	topolový	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
zelenající	zelenající	k2eAgMnSc1d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
bělostný	bělostný	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
bělotřeňový	bělotřeňový	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
borový	borový	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
březový	březový	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
černotřeňový	černotřeňový	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
dubový	dubový	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
hnědý	hnědý	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
osikový	osikový	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
smrkový	smrkový	k2eAgInSc4d1
Další	další	k2eAgFnSc7d1
rody	rod	k1gInPc7
</s>
<s>
Lupenopórka	Lupenopórka	k1gFnSc1
(	(	kIx(
<g/>
Lupenopórka	Lupenopórka	k1gFnSc1
červenožlutá	červenožlutý	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Šiškovec	Šiškovec	k1gInSc1
(	(	kIx(
<g/>
Šiškovec	Šiškovec	k1gInSc1
černý	černý	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Lanmoa	Lanmoa	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Hřib	hřib	k1gInSc4
vonný	vonný	k2eAgInSc4d1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Houby	houba	k1gFnPc5
</s>
