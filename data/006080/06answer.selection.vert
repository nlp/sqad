<s>
V	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
Minkowskiho	Minkowski	k1gMnSc2	Minkowski
prostoročasu	prostoročas	k1gInSc2	prostoročas
používá	používat	k5eAaImIp3nS	používat
Riemannův	Riemannův	k2eAgInSc1d1	Riemannův
prostoročas	prostoročas	k1gInSc1	prostoročas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obecně	obecně	k6eAd1	obecně
zakřivený	zakřivený	k2eAgInSc1d1	zakřivený
a	a	k8xC	a
metrika	metrika	k1gFnSc1	metrika
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
symetrickým	symetrický	k2eAgInSc7d1	symetrický
metrickým	metrický	k2eAgInSc7d1	metrický
tenzorem	tenzor	k1gInSc7	tenzor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iota	k1gFnSc1	iota
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
}}	}}	k?	}}
,	,	kIx,	,
který	který	k3yQgMnSc1	který
obecně	obecně	k6eAd1	obecně
není	být	k5eNaImIp3nS	být
diagonální	diagonální	k2eAgFnSc1d1	diagonální
<g/>
.	.	kIx.	.
</s>
