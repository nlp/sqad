<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Reynek	Reynek	k1gMnSc1	Reynek
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
Petrkov	Petrkov	k1gInSc1	Petrkov
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Petrkov	Petrkov	k1gInSc1	Petrkov
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Petrkově	Petrkův	k2eAgFnSc6d1	Petrkova
u	u	k7c2	u
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Německý	německý	k2eAgInSc1d1	německý
Brod	Brod	k1gInSc1	Brod
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
jediný	jediný	k2eAgMnSc1d1	jediný
syn	syn	k1gMnSc1	syn
statkáře	statkář	k1gMnSc2	statkář
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Reynka	Reynek	k1gMnSc2	Reynek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
ve	v	k7c6	v
Svatém	svatý	k2eAgInSc6d1	svatý
Kříži	kříž	k1gInSc6	kříž
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
reálku	reálka	k1gFnSc4	reálka
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
ovlivněn	ovlivnit	k5eAaPmNgMnS	ovlivnit
svým	svůj	k3xOyFgMnSc7	svůj
profesorem	profesor	k1gMnSc7	profesor
Maxem	Max	k1gMnSc7	Max
Eislerem	Eisler	k1gInSc7	Eisler
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
studoval	studovat	k5eAaImAgMnS	studovat
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
)	)	kIx)	)
zemědělství	zemědělství	k1gNnSc1	zemědělství
na	na	k7c4	na
C.K.	C.K.	k1gFnSc4	C.K.
Vysoké	vysoký	k2eAgFnSc3d1	vysoká
škole	škola	k1gFnSc3	škola
technické	technický	k2eAgFnSc3d1	technická
<g/>
,	,	kIx,	,
studia	studio	k1gNnPc4	studio
však	však	k9	však
po	po	k7c6	po
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
nechal	nechat	k5eAaPmAgMnS	nechat
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Petrkova	Petrkův	k2eAgNnSc2d1	Petrkův
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
podnikl	podniknout	k5eAaPmAgMnS	podniknout
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
básně	báseň	k1gFnPc4	báseň
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Žízně	žízeň	k1gFnSc2	žízeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
vydavatelem	vydavatel	k1gMnSc7	vydavatel
Josefem	Josef	k1gMnSc7	Josef
Florianem	Florian	k1gMnSc7	Florian
ze	z	k7c2	z
Staré	Staré	k2eAgFnSc2d1	Staré
Říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
překlady	překlad	k1gInPc7	překlad
poezie	poezie	k1gFnSc2	poezie
i	i	k8xC	i
prózy	próza	k1gFnSc2	próza
<g/>
,	,	kIx,	,
vlastními	vlastní	k2eAgFnPc7d1	vlastní
básněmi	báseň	k1gFnPc7	báseň
a	a	k8xC	a
grafikami	grafika	k1gFnPc7	grafika
velmi	velmi	k6eAd1	velmi
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
mnoho	mnoho	k4c4	mnoho
staroříšských	staroříšský	k2eAgFnPc2d1	staroříšská
edic	edice	k1gFnPc2	edice
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
edici	edice	k1gFnSc4	edice
Dobré	dobrý	k2eAgNnSc1d1	dobré
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Grenoblu	Grenoble	k1gInSc2	Grenoble
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
poznal	poznat	k5eAaPmAgMnS	poznat
autorku	autorka	k1gFnSc4	autorka
knihy	kniha	k1gFnSc2	kniha
Ta	ten	k3xDgFnSc1	ten
vie	vie	k?	vie
est	est	k?	est
la	la	k1gNnPc2	la
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
básnířku	básnířek	k1gMnSc5	básnířek
Suzanne	Suzann	k1gMnSc5	Suzann
Renaudovou	Renaudový	k2eAgFnSc4d1	Renaudový
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
poté	poté	k6eAd1	poté
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
vzal	vzít	k5eAaPmAgInS	vzít
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Následujících	následující	k2eAgInPc2d1	následující
deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
žil	žít	k5eAaImAgMnS	žít
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
syny	syn	k1gMnPc7	syn
<g/>
,	,	kIx,	,
narozenými	narozený	k2eAgFnPc7d1	narozená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1928	[number]	k4	1928
a	a	k8xC	a
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
střídavě	střídavě	k6eAd1	střídavě
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
v	v	k7c6	v
Petrkově	Petrkův	k2eAgInSc6d1	Petrkův
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
nastálo	nastálo	k6eAd1	nastálo
vrátil	vrátit	k5eAaPmAgInS	vrátit
až	až	k9	až
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
správy	správa	k1gFnSc2	správa
statku	statek	k1gInSc2	statek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
byli	být	k5eAaImAgMnP	být
Reynkovi	Reynkův	k2eAgMnPc1d1	Reynkův
donuceni	donucen	k2eAgMnPc1d1	donucen
se	se	k3xPyFc4	se
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
ze	z	k7c2	z
statku	statek	k1gInSc2	statek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
hledání	hledání	k1gNnSc2	hledání
nového	nový	k2eAgNnSc2d1	nové
bydliště	bydliště	k1gNnSc2	bydliště
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
nastěhovala	nastěhovat	k5eAaPmAgFnS	nastěhovat
do	do	k7c2	do
Staré	Staré	k2eAgFnSc2d1	Staré
Říše	říš	k1gFnSc2	říš
k	k	k7c3	k
dětem	dítě	k1gFnPc3	dítě
J.	J.	kA	J.
Floriana	Florian	k1gMnSc2	Florian
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstala	zůstat	k5eAaPmAgFnS	zůstat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
na	na	k7c4	na
statek	statek	k1gInSc4	statek
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
komunistickém	komunistický	k2eAgInSc6d1	komunistický
puči	puč	k1gInSc6	puč
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgInS	být
Reynkův	Reynkův	k2eAgInSc1d1	Reynkův
statek	statek	k1gInSc1	statek
v	v	k7c6	v
Petrkově	Petrkův	k2eAgFnSc6d1	Petrkova
zestátněn	zestátněn	k2eAgMnSc1d1	zestátněn
a	a	k8xC	a
Reynek	Reynek	k1gMnSc1	Reynek
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
poté	poté	k6eAd1	poté
pracoval	pracovat	k5eAaImAgMnS	pracovat
jen	jen	k9	jen
jako	jako	k9	jako
zemědělský	zemědělský	k2eAgMnSc1d1	zemědělský
dělník	dělník	k1gMnSc1	dělník
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
První	první	k4xOgInPc4	první
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
a	a	k8xC	a
ani	ani	k8xC	ani
neopouštěl	opouštět	k5eNaImAgMnS	opouštět
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vzniká	vznikat	k5eAaImIp3nS	vznikat
většina	většina	k1gFnSc1	většina
Reynkova	Reynkův	k2eAgNnSc2d1	Reynkovo
grafického	grafický	k2eAgNnSc2d1	grafické
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
dozrává	dozrávat	k5eAaImIp3nS	dozrávat
také	také	k9	také
jeho	jeho	k3xOp3gInSc1	jeho
osobitý	osobitý	k2eAgInSc1d1	osobitý
básnický	básnický	k2eAgInSc1d1	básnický
projev	projev	k1gInSc1	projev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
na	na	k7c4	na
Reynka	Reynek	k1gMnSc4	Reynek
znovu	znovu	k6eAd1	znovu
začíná	začínat	k5eAaImIp3nS	začínat
obracet	obracet	k5eAaImF	obracet
pozornost	pozornost	k1gFnSc1	pozornost
mnohých	mnohý	k2eAgMnPc2d1	mnohý
mladých	mladý	k2eAgMnPc2d1	mladý
umělců	umělec	k1gMnPc2	umělec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jiří	Jiří	k1gMnSc1	Jiří
Kolář	Kolář	k1gMnSc1	Kolář
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Diviš	Diviš	k1gMnSc1	Diviš
nebo	nebo	k8xC	nebo
Ivan	Ivan	k1gMnSc1	Ivan
Martin	Martin	k1gMnSc1	Martin
Jirous	Jirous	k1gMnSc1	Jirous
vulgo	vulgo	k?	vulgo
Magor	magor	k1gMnSc1	magor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ovdověl	ovdovět	k5eAaPmAgMnS	ovdovět
<g/>
,	,	kIx,	,
směl	smět	k5eAaImAgMnS	smět
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
pětatřiceti	pětatřicet	k4xCc6	pětatřicet
letech	léto	k1gNnPc6	léto
znovu	znovu	k6eAd1	znovu
vystavovat	vystavovat	k5eAaImF	vystavovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
ještě	ještě	k9	ještě
stihl	stihnout	k5eAaPmAgInS	stihnout
vydat	vydat	k5eAaPmF	vydat
jeden	jeden	k4xCgInSc4	jeden
svazek	svazek	k1gInSc4	svazek
své	svůj	k3xOyFgFnSc2	svůj
poezie	poezie	k1gFnSc2	poezie
(	(	kIx(	(
<g/>
byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc1	ten
sbírky	sbírka	k1gFnPc1	sbírka
Sníh	sníh	k1gInSc4	sníh
na	na	k7c4	na
zápraží	zápraží	k1gNnSc4	zápraží
<g/>
,	,	kIx,	,
Mráz	mráz	k1gInSc4	mráz
v	v	k7c6	v
okně	okno	k1gNnSc6	okno
a	a	k8xC	a
Podzimní	podzimní	k2eAgMnSc1d1	podzimní
motýli	motýl	k1gMnPc1	motýl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
básnickou	básnický	k2eAgFnSc4d1	básnická
sbírku	sbírka	k1gFnSc4	sbírka
Odlet	odlet	k1gInSc1	odlet
vlaštovek	vlaštovka	k1gFnPc2	vlaštovka
už	už	k6eAd1	už
vydat	vydat	k5eAaPmF	vydat
nemohl	moct	k5eNaImAgInS	moct
-	-	kIx~	-
její	její	k3xOp3gFnSc1	její
sazba	sazba	k1gFnSc1	sazba
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
postupující	postupující	k2eAgFnSc2d1	postupující
normalizace	normalizace	k1gFnSc2	normalizace
rozmetána	rozmetán	k2eAgFnSc1d1	rozmetána
<g/>
.	.	kIx.	.
</s>
<s>
Pochován	pochován	k2eAgInSc1d1	pochován
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
ve	v	k7c6	v
Svatém	svatý	k2eAgInSc6d1	svatý
Kříži	kříž	k1gInSc6	kříž
(	(	kIx(	(
<g/>
obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
součástí	součást	k1gFnSc7	součást
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reynkova	Reynkův	k2eAgFnSc1d1	Reynkova
poezie	poezie	k1gFnSc1	poezie
vycházela	vycházet	k5eAaImAgFnS	vycházet
dlouho	dlouho	k6eAd1	dlouho
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
bibliofilských	bibliofilský	k2eAgNnPc6d1	bibliofilské
vydáních	vydání	k1gNnPc6	vydání
nebo	nebo	k8xC	nebo
časopisecky	časopisecky	k6eAd1	časopisecky
<g/>
,	,	kIx,	,
prvního	první	k4xOgNnSc2	první
souborného	souborný	k2eAgNnSc2d1	souborné
vydání	vydání	k1gNnSc2	vydání
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
sbírky	sbírka	k1gFnPc1	sbírka
<g/>
,	,	kIx,	,
psané	psaný	k2eAgFnPc1d1	psaná
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
expresionismem	expresionismus	k1gInSc7	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Expresionistické	expresionistický	k2eAgInPc1d1	expresionistický
tóny	tón	k1gInPc1	tón
však	však	k9	však
postupně	postupně	k6eAd1	postupně
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
na	na	k7c6	na
významu	význam	k1gInSc6	význam
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
Reynkově	Reynkův	k2eAgFnSc3d1	Reynkova
poezii	poezie	k1gFnSc3	poezie
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
desetileté	desetiletý	k2eAgFnSc6d1	desetiletá
odmlce	odmlka	k1gFnSc6	odmlka
se	se	k3xPyFc4	se
básníkův	básníkův	k2eAgInSc1d1	básníkův
projev	projev	k1gInSc1	projev
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Významnější	významný	k2eAgInSc1d2	významnější
je	být	k5eAaImIp3nS	být
motiv	motiv	k1gInSc1	motiv
rodného	rodný	k2eAgInSc2d1	rodný
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
Vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
básní	báseň	k1gFnPc2	báseň
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
patrnější	patrný	k2eAgFnSc1d2	patrnější
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
spiritualita	spiritualita	k1gFnSc1	spiritualita
a	a	k8xC	a
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
a	a	k8xC	a
ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
božímu	boží	k2eAgNnSc3d1	boží
stvoření	stvoření	k1gNnSc3	stvoření
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
sv.	sv.	kA	sv.
Františka	František	k1gMnSc4	František
z	z	k7c2	z
Assisi	Assis	k1gMnSc3	Assis
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
básních	báseň	k1gFnPc6	báseň
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
obrazy	obraz	k1gInPc1	obraz
z	z	k7c2	z
lidského	lidský	k2eAgNnSc2d1	lidské
hlediska	hledisko	k1gNnSc2	hledisko
zcela	zcela	k6eAd1	zcela
nepatrných	patrný	k2eNgMnPc2d1	nepatrný
tvorů	tvor	k1gMnPc2	tvor
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
vylíčení	vylíčení	k1gNnSc3	vylíčení
básníkovy	básníkův	k2eAgFnSc2d1	básníkova
pokorné	pokorný	k2eAgFnSc2d1	pokorná
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
životu	život	k1gInSc3	život
a	a	k8xC	a
ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
žije	žít	k5eAaImIp3nS	žít
a	a	k8xC	a
trpí	trpět	k5eAaImIp3nS	trpět
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
Reynkovy	Reynkův	k2eAgFnSc2d1	Reynkova
tvorby	tvorba	k1gFnSc2	tvorba
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
poslední	poslední	k2eAgFnSc1d1	poslední
sbírka	sbírka	k1gFnSc1	sbírka
Odlet	odlet	k1gInSc4	odlet
vlaštovek	vlaštovka	k1gFnPc2	vlaštovka
<g/>
.	.	kIx.	.
</s>
<s>
Žízně	žízně	k6eAd1	žízně
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
Rybí	rybí	k2eAgFnPc1d1	rybí
šupiny	šupina	k1gFnPc1	šupina
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
Smutek	smutek	k1gInSc1	smutek
země	zem	k1gFnSc2	zem
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
1924	[number]	k4	1924
Had	had	k1gMnSc1	had
na	na	k7c6	na
sněhu	sníh	k1gInSc6	sníh
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
Rty	ret	k1gInPc1	ret
a	a	k8xC	a
zuby	zub	k1gInPc1	zub
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
Setba	setba	k1gFnSc1	setba
samot	samota	k1gFnPc2	samota
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1936	[number]	k4	1936
Pieta	pieta	k1gFnSc1	pieta
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
Podzimní	podzimní	k2eAgMnPc1d1	podzimní
motýli	motýl	k1gMnPc1	motýl
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
Mráz	mráz	k1gInSc1	mráz
v	v	k7c6	v
okně	okno	k1gNnSc6	okno
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
Sníh	sníh	k1gInSc1	sníh
na	na	k7c4	na
zápraží	zápraží	k1gNnSc4	zápraží
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
Odlet	odlet	k1gInSc1	odlet
vlaštovek	vlaštovka	k1gFnPc2	vlaštovka
-	-	kIx~	-
posmrtně	posmrtně	k6eAd1	posmrtně
vydaná	vydaný	k2eAgFnSc1d1	vydaná
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
71	[number]	k4	71
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prvně	prvně	k?	prvně
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
Významná	významný	k2eAgFnSc1d1	významná
byla	být	k5eAaImAgFnS	být
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
překladatelská	překladatelský	k2eAgFnSc1d1	překladatelská
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Překládal	překládat	k5eAaImAgMnS	překládat
z	z	k7c2	z
francouzského	francouzský	k2eAgMnSc2d1	francouzský
a	a	k8xC	a
německého	německý	k2eAgInSc2d1	německý
jazyka	jazyk	k1gInSc2	jazyk
především	především	k9	především
díla	dílo	k1gNnSc2	dílo
francouzských	francouzský	k2eAgMnPc2d1	francouzský
katolických	katolický	k2eAgMnPc2d1	katolický
básníků	básník	k1gMnPc2	básník
(	(	kIx(	(
<g/>
G.	G.	kA	G.
Bernanos	Bernanos	k1gInSc1	Bernanos
<g/>
,	,	kIx,	,
L.	L.	kA	L.
Bloy	Bloa	k1gFnPc1	Bloa
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Giono	Giono	k1gNnSc1	Giono
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Jammes	Jammes	k1gInSc1	Jammes
<g/>
,	,	kIx,	,
J.	J.	kA	J.
de	de	k?	de
La	la	k0	la
Fontaine	Fontain	k1gMnSc5	Fontain
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Verlaine	Verlain	k1gInSc5	Verlain
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
a	a	k8xC	a
německých	německý	k2eAgMnPc2d1	německý
expresionistů	expresionista	k1gMnPc2	expresionista
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgMnS	soustředit
na	na	k7c4	na
překlady	překlad	k1gInPc4	překlad
básnických	básnický	k2eAgFnPc2d1	básnická
sbírek	sbírka	k1gFnPc2	sbírka
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
Suzanne	Suzann	k1gInSc5	Suzann
Renaudové	Renaudové	k2eAgNnSc3d1	Renaudové
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
jeho	jeho	k3xOp3gNnSc2	jeho
překladatelského	překladatelský	k2eAgNnSc2d1	překladatelské
úsilí	úsilí	k1gNnSc2	úsilí
spočívá	spočívat	k5eAaImIp3nS	spočívat
zejména	zejména	k9	zejména
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgMnSc1d1	dobrý
znalec	znalec	k1gMnSc1	znalec
francouzské	francouzský	k2eAgFnSc2d1	francouzská
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
literatury	literatura	k1gFnSc2	literatura
přeložil	přeložit	k5eAaPmAgMnS	přeložit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
díla	dílo	k1gNnSc2	dílo
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
českému	český	k2eAgMnSc3d1	český
čtenáři	čtenář	k1gMnPc7	čtenář
neznámých	známý	k2eNgMnPc2d1	neznámý
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeho	on	k3xPp3gInSc4	on
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
překladatelský	překladatelský	k2eAgInSc4d1	překladatelský
čin	čin	k1gInSc4	čin
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
přeložení	přeložení	k1gNnSc1	přeložení
veršů	verš	k1gInPc2	verš
G.	G.	kA	G.
Trakla	Trakla	k1gFnSc2	Trakla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
významně	významně	k6eAd1	významně
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
soudobou	soudobý	k2eAgFnSc4d1	soudobá
českou	český	k2eAgFnSc4d1	Česká
poezii	poezie	k1gFnSc4	poezie
(	(	kIx(	(
<g/>
F.	F.	kA	F.
Halas	halas	k1gInSc1	halas
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Závada	závada	k1gFnSc1	závada
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Zahradníček	Zahradníček	k1gMnSc1	Zahradníček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
jihlavské	jihlavský	k2eAgFnSc6d1	Jihlavská
reálce	reálka	k1gFnSc6	reálka
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaImF	věnovat
kresbě	kresba	k1gFnSc3	kresba
a	a	k8xC	a
malbě	malba	k1gFnSc3	malba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
olejomalba	olejomalba	k1gFnSc1	olejomalba
Portrét	portrét	k1gInSc4	portrét
otce	otec	k1gMnSc4	otec
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navázal	navázat	k5eAaPmAgMnS	navázat
kontakty	kontakt	k1gInPc4	kontakt
se	s	k7c7	s
členy	člen	k1gMnPc7	člen
skupiny	skupina	k1gFnSc2	skupina
Osma	osma	k1gFnSc1	osma
a	a	k8xC	a
Skupiny	skupina	k1gFnPc1	skupina
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Tvrdošíjní	tvrdošíjný	k2eAgMnPc1d1	tvrdošíjný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c4	v
souvislosti	souvislost	k1gFnPc4	souvislost
s	s	k7c7	s
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Josefa	Josef	k1gMnSc2	Josef
Floriana	Florian	k1gMnSc2	Florian
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Říši	říš	k1gFnSc6	říš
expresionistické	expresionistický	k2eAgInPc4d1	expresionistický
linoryty	linoryt	k1gInPc4	linoryt
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vydávání	vydávání	k1gNnSc6	vydávání
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
edice	edice	k1gFnSc2	edice
Sešitů	sešit	k1gInPc2	sešit
poezie	poezie	k1gFnSc2	poezie
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
J.	J.	kA	J.
Čapkem	Čapek	k1gMnSc7	Čapek
a	a	k8xC	a
V.	V.	kA	V.
Hofmanem	Hofman	k1gMnSc7	Hofman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
vystavoval	vystavovat	k5eAaImAgMnS	vystavovat
svoje	svůj	k3xOyFgFnPc4	svůj
kresby	kresba	k1gFnPc4	kresba
<g/>
,	,	kIx,	,
pastely	pastel	k1gInPc4	pastel
a	a	k8xC	a
lepty	lept	k1gInPc4	lept
na	na	k7c6	na
několika	několik	k4yIc6	několik
výstavách	výstava	k1gFnPc6	výstava
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
přijato	přijmout	k5eAaPmNgNnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
tvorba	tvorba	k1gFnSc1	tvorba
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
znamenala	znamenat	k5eAaImAgFnS	znamenat
hlavní	hlavní	k2eAgInSc4d1	hlavní
zdroj	zdroj	k1gInSc4	zdroj
obživy	obživa	k1gFnSc2	obživa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
si	se	k3xPyFc3	se
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Reynek	Reynek	k1gMnSc1	Reynek
poprvé	poprvé	k6eAd1	poprvé
vyzkoušel	vyzkoušet	k5eAaPmAgMnS	vyzkoušet
techniku	technika	k1gFnSc4	technika
suché	suchý	k2eAgFnSc2d1	suchá
jehly	jehla	k1gFnSc2	jehla
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
grafiky	grafika	k1gFnPc1	grafika
začínají	začínat	k5eAaImIp3nP	začínat
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nad	nad	k7c7	nad
kresbami	kresba	k1gFnPc7	kresba
uhlem	uhel	k1gInSc7	uhel
a	a	k8xC	a
pastelem	pastel	k1gInSc7	pastel
převažovat	převažovat	k5eAaImF	převažovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
tak	tak	k6eAd1	tak
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
šest	šest	k4xCc4	šest
set	sto	k4xCgNnPc2	sto
grafických	grafický	k2eAgFnPc2d1	grafická
listů	list	k1gInPc2	list
technikou	technika	k1gFnSc7	technika
suché	suchý	k2eAgFnSc2d1	suchá
jehly	jehla	k1gFnSc2	jehla
nebo	nebo	k8xC	nebo
leptu	lept	k1gInSc2	lept
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
tiskem	tisk	k1gInSc7	tisk
z	z	k7c2	z
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
dominuje	dominovat	k5eAaImIp3nS	dominovat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
grafikách	grafika	k1gFnPc6	grafika
krajina	krajina	k1gFnSc1	krajina
<g/>
,	,	kIx,	,
až	až	k9	až
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
se	se	k3xPyFc4	se
výrazněji	výrazně	k6eAd2	výrazně
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
motivy	motiv	k1gInPc4	motiv
biblické	biblický	k2eAgInPc4d1	biblický
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
jsou	být	k5eAaImIp3nP	být
nejčastějšími	častý	k2eAgInPc7d3	nejčastější
motivy	motiv	k1gInPc7	motiv
ukřižování	ukřižování	k1gNnSc2	ukřižování
<g/>
,	,	kIx,	,
pieta	pieta	k1gFnSc1	pieta
<g/>
,	,	kIx,	,
zapření	zapření	k1gNnSc1	zapření
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
také	také	k9	také
Pašijový	pašijový	k2eAgInSc1d1	pašijový
cyklus	cyklus	k1gInSc1	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
část	část	k1gFnSc1	část
Reynkova	Reynkův	k2eAgNnSc2d1	Reynkovo
grafického	grafický	k2eAgNnSc2d1	grafické
díla	dílo	k1gNnSc2	dílo
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
např.	např.	kA	např.
cyklus	cyklus	k1gInSc1	cyklus
Job	Job	k1gMnSc1	Job
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
-	-	kIx~	-
<g/>
49	[number]	k4	49
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cyklus	cyklus	k1gInSc1	cyklus
Don	dona	k1gFnPc2	dona
Quijote	Quijot	k1gInSc5	Quijot
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
<g/>
)	)	kIx)	)
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
dalších	další	k2eAgFnPc2d1	další
grafik	grafika	k1gFnPc2	grafika
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
básnická	básnický	k2eAgFnSc1d1	básnická
tvorba	tvorba	k1gFnSc1	tvorba
i	i	k8xC	i
tvorba	tvorba	k1gFnSc1	tvorba
grafická	grafický	k2eAgFnSc1d1	grafická
došla	dojít	k5eAaPmAgFnS	dojít
plného	plný	k2eAgNnSc2d1	plné
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
uznání	uznání	k1gNnSc2	uznání
až	až	k9	až
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
kočka	kočka	k1gFnSc1	kočka
Vrak	vrak	k1gInSc4	vrak
z	z	k7c2	z
bídy	bída	k1gFnSc2	bída
břehu	břeh	k1gInSc2	břeh
<g/>
,	,	kIx,	,
vychrtlá	vychrtlý	k2eAgFnSc1d1	vychrtlá
<g/>
,	,	kIx,	,
malá	malý	k2eAgFnSc1d1	malá
kočka	kočka	k1gFnSc1	kočka
tu	tu	k6eAd1	tu
leží	ležet	k5eAaImIp3nS	ležet
<g/>
.	.	kIx.	.
</s>
<s>
Teplou	Teplá	k1gFnSc4	Teplá
a	a	k8xC	a
svěží	svěžit	k5eAaImIp3nS	svěžit
rozdala	rozdat	k5eAaPmAgFnS	rozdat
něhu	něha	k1gFnSc4	něha
<g/>
.	.	kIx.	.
</s>
<s>
Smutně	smutně	k6eAd1	smutně
tu	tu	k6eAd1	tu
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
krůpěj	krůpěj	k1gFnSc4	krůpěj
mléka	mléko	k1gNnSc2	mléko
od	od	k7c2	od
luny	luna	k1gFnSc2	luna
a	a	k8xC	a
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Noc	noc	k1gFnSc1	noc
po	po	k7c6	po
sobotě	sobota	k1gFnSc6	sobota
<g/>
,	,	kIx,	,
zima	zima	k6eAd1	zima
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
svítá	svítat	k5eAaImIp3nS	svítat
luna	luna	k1gFnSc1	luna
kamenitá	kamenitý	k2eAgFnSc1d1	kamenitá
<g/>
.	.	kIx.	.
</s>
<s>
Stydne	stydnout	k5eAaImIp3nS	stydnout
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
kotě	kotě	k1gNnSc1	kotě
<g/>
.	.	kIx.	.
</s>
<s>
Čí	čí	k3xOyRgFnSc1	čí
vina	vina	k1gFnSc1	vina
je	být	k5eAaImIp3nS	být
smyta	smyt	k2eAgFnSc1d1	smyta
<g/>
?	?	kIx.	?
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Reynka	Reynek	k1gMnSc2	Reynek
zhudebnili	zhudebnit	k5eAaPmAgMnP	zhudebnit
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Iva	Iva	k1gFnSc1	Iva
Bittová	Bittová	k1gFnSc1	Bittová
<g/>
:	:	kIx,	:
Iva	Iva	k1gFnSc1	Iva
Bittová	Bittová	k1gFnSc1	Bittová
<g/>
,	,	kIx,	,
Pavian	Pavian	k1gInSc1	Pavian
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
-	-	kIx~	-
píseň	píseň	k1gFnSc1	píseň
Adventní	adventní	k2eAgFnSc1d1	adventní
Iva	Iva	k1gFnSc1	Iva
Bittová	Bittová	k1gFnSc1	Bittová
<g/>
:	:	kIx,	:
Ne	ne	k9	ne
<g/>
,	,	kIx,	,
nehledej	hledat	k5eNaImRp2nS	hledat
<g/>
,	,	kIx,	,
BMG	BMG	kA	BMG
Ariola	Ariola	k1gFnSc1	Ariola
ČR	ČR	kA	ČR
<g/>
/	/	kIx~	/
<g/>
SR	SR	kA	SR
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
-	-	kIx~	-
píseň	píseň	k1gFnSc1	píseň
Blázen	blázen	k1gMnSc1	blázen
Pluto	Pluto	k1gMnSc1	Pluto
<g/>
:	:	kIx,	:
Tři	tři	k4xCgMnPc1	tři
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
písně	píseň	k1gFnPc1	píseň
Země	zem	k1gFnSc2	zem
pokladů	poklad	k1gInPc2	poklad
<g/>
,	,	kIx,	,
Vítr	vítr	k1gInSc1	vítr
v	v	k7c6	v
mracích	mrak	k1gInPc6	mrak
a	a	k8xC	a
Masopust	Masopust	k1gMnSc1	Masopust
Kopir	Kopir	k1gMnSc1	Kopir
Rozsywal	Rozsywal	k1gMnSc1	Rozsywal
Bestar	Bestar	k1gMnSc1	Bestar
<g/>
:	:	kIx,	:
Kopir	Kopir	k1gMnSc1	Kopir
Rozsywal	Rozsywal	k1gMnSc1	Rozsywal
Bestar	Bestar	k1gMnSc1	Bestar
-	-	kIx~	-
písně	píseň	k1gFnPc1	píseň
Žáby	žába	k1gFnSc2	žába
a	a	k8xC	a
Zimní	zimní	k2eAgFnSc1d1	zimní
krajina	krajina	k1gFnSc1	krajina
Ondřej	Ondřej	k1gMnSc1	Ondřej
Metyš	Metyš	k1gMnSc1	Metyš
<g/>
:	:	kIx,	:
Mys	mys	k1gInSc1	mys
dobré	dobrý	k2eAgFnSc2d1	dobrá
naděje	naděje	k1gFnSc2	naděje
(	(	kIx(	(
<g/>
audiokazeta	audiokazeta	k1gFnSc1	audiokazeta
<g/>
)	)	kIx)	)
Iva	Iva	k1gFnSc1	Iva
Bittová	Bittová	k1gFnSc1	Bittová
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Václavek	Václavek	k1gMnSc1	Václavek
<g/>
:	:	kIx,	:
Bílé	bílý	k2eAgNnSc1d1	bílé
inferno	inferno	k1gNnSc1	inferno
<g/>
,	,	kIx,	,
Indies	Indies	k1gInSc1	Indies
Records	Records	k1gInSc1	Records
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
-	-	kIx~	-
písně	píseň	k1gFnSc2	píseň
Sirka	sirka	k1gFnSc1	sirka
v	v	k7c6	v
louži	louž	k1gFnSc6	louž
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc1d1	starý
mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
Kdoule	kdoule	k1gFnSc1	kdoule
<g/>
,	,	kIx,	,
Moucha	moucha	k1gFnSc1	moucha
a	a	k8xC	a
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
Lukáš	Lukáš	k1gMnSc1	Lukáš
Sommer	Sommer	k1gMnSc1	Sommer
<g/>
:	:	kIx,	:
Ostny	osten	k1gInPc1	osten
v	v	k7c6	v
závoji	závoj	k1gInSc6	závoj
-	-	kIx~	-
kantáta	kantáta	k1gFnSc1	kantáta
na	na	k7c4	na
texty	text	k1gInPc4	text
Bohuslava	Bohuslav	k1gMnSc4	Bohuslav
Reynka	Reynek	k1gMnSc2	Reynek
pro	pro	k7c4	pro
recitátora	recitátor	k1gMnSc4	recitátor
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
části	část	k1gFnSc6	část
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Sen	sen	k1gInSc1	sen
<g/>
,	,	kIx,	,
Mžení	mžení	k1gNnSc1	mžení
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
,	,	kIx,	,
Zima	zima	k1gFnSc1	zima
a	a	k8xC	a
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
Říjen	říjen	k1gInSc1	říjen
<g/>
,	,	kIx,	,
Blázen	blázen	k1gMnSc1	blázen
<g/>
;	;	kIx,	;
premiéra	premiér	k1gMnSc2	premiér
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Pražské	pražský	k2eAgFnSc2d1	Pražská
premiéry	premiéra	k1gFnSc2	premiéra
2009	[number]	k4	2009
Hukl	huknout	k5eAaPmAgMnS	huknout
<g/>
:	:	kIx,	:
Reynkarnace	Reynkarnace	k1gFnSc1	Reynkarnace
<g/>
,	,	kIx,	,
Indies	Indies	k1gInSc1	Indies
Scope	Scop	k1gInSc5	Scop
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Vladimír	Vladimír	k1gMnSc1	Vladimír
Václavek	Václavek	k1gMnSc1	Václavek
<g/>
:	:	kIx,	:
Písně	píseň	k1gFnPc1	píseň
nepísně	písně	k6eNd1	písně
<g/>
,	,	kIx,	,
Indies	Indies	k1gInSc1	Indies
Scope	Scop	k1gMnSc5	Scop
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
2003	[number]	k4	2003
-	-	kIx~	-
písně	píseň	k1gFnSc2	píseň
Blázen	blázen	k1gMnSc1	blázen
a	a	k8xC	a
Odlet	odlet	k1gInSc1	odlet
vlaštovek	vlaštovka	k1gFnPc2	vlaštovka
Karel	Karla	k1gFnPc2	Karla
Vepřek	Vepřek	k1gInSc1	Vepřek
<g/>
:	:	kIx,	:
Artinodhás	Artinodhás	k1gInSc1	Artinodhás
<g/>
,	,	kIx,	,
Indies	Indies	k1gInSc1	Indies
Scope	Scop	k1gInSc5	Scop
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
Vladimír	Vladimír	k1gMnSc1	Vladimír
Václavek	Václavek	k1gMnSc1	Václavek
<g/>
:	:	kIx,	:
Jsem	být	k5eAaImIp1nS	být
hlína	hlína	k1gFnSc1	hlína
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
Indies	Indies	k1gInSc1	Indies
Scope	Scop	k1gInSc5	Scop
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
-	-	kIx~	-
píseň	píseň	k1gFnSc1	píseň
Opuštění	opuštění	k1gNnSc2	opuštění
Karel	Karla	k1gFnPc2	Karla
Vepřek	Vepřka	k1gFnPc2	Vepřka
<g/>
:	:	kIx,	:
Želví	želví	k2eAgInPc1d1	želví
sny	sen	k1gInPc1	sen
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Indies	Indies	k1gMnSc1	Indies
Scope	Scop	k1gInSc5	Scop
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Karel	Karla	k1gFnPc2	Karla
Vepřek	Vepřka	k1gFnPc2	Vepřka
<g/>
:	:	kIx,	:
Najdem	Najdem	k?	Najdem
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
Indies	Indies	k1gInSc1	Indies
Scope	Scop	k1gInSc5	Scop
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
-	-	kIx~	-
píseň	píseň	k1gFnSc1	píseň
Měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
jíní	jíní	k1gNnSc1	jíní
Jiří	Jiří	k1gMnSc1	Jiří
Schneider	Schneider	k1gMnSc1	Schneider
-	-	kIx~	-
skupina	skupina	k1gFnSc1	skupina
Ejhle	ejhle	k0	ejhle
<g/>
:	:	kIx,	:
básně	báseň	k1gFnPc4	báseň
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Odlet	odlet	k1gInSc4	odlet
vlaštovek	vlaštovka	k1gFnPc2	vlaštovka
1980-84	[number]	k4	1980-84
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
Ondřej	Ondřej	k1gMnSc1	Ondřej
Škoch	Škoch	k1gInSc1	Škoch
-	-	kIx~	-
projekt	projekt	k1gInSc1	projekt
Nitky	nitka	k1gFnSc2	nitka
-	-	kIx~	-
CD	CD	kA	CD
NITKY	nitka	k1gFnSc2	nitka
r.	r.	kA	r.
v.	v.	k?	v.
2003	[number]	k4	2003
Utlučtumůru	Utlučtumůro	k1gNnSc6	Utlučtumůro
-	-	kIx~	-
Kopce	kopka	k1gFnSc6	kopka
jsou	být	k5eAaImIp3nP	být
lysé	lysý	k2eAgInPc1d1	lysý
<g/>
,	,	kIx,	,
sníh	sníh	k1gInSc1	sníh
hoří	hořet	k5eAaImIp3nS	hořet
<g/>
,	,	kIx,	,
Blázen	blázen	k1gMnSc1	blázen
jsem	být	k5eAaImIp1nS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vsi	ves	k1gFnSc6	ves
-	-	kIx~	-
1999	[number]	k4	1999
Transitus	Transitus	k1gInSc1	Transitus
Irregularis	Irregularis	k1gInSc4	Irregularis
-	-	kIx~	-
Pod	pod	k7c7	pod
prahem	práh	k1gInSc7	práh
svítá	svítat	k5eAaImIp3nS	svítat
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
HANUŠ	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
slovník	slovník	k1gInSc1	slovník
osobností	osobnost	k1gFnPc2	osobnost
českého	český	k2eAgInSc2d1	český
katolicismu	katolicismus	k1gInSc2	katolicismus
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
antologií	antologie	k1gFnSc7	antologie
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
307	[number]	k4	307
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7325	[number]	k4	7325
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
29	[number]	k4	29
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
CHALUPA	Chalupa	k1gMnSc1	Chalupa
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Reynek	Reynek	k1gMnSc1	Reynek
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Řevnicích	řevnice	k1gFnPc6	řevnice
<g/>
:	:	kIx,	:
Arbor	Arbora	k1gFnPc2	Arbora
vitae	vitae	k1gNnPc2	vitae
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Fondem	fond	k1gInSc7	fond
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Reynka	Reynek	k1gMnSc2	Reynek
a	a	k8xC	a
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Měsíc	měsíc	k1gInSc1	měsíc
ve	v	k7c6	v
dne	den	k1gInSc2	den
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
:	:	kIx,	:
Arbor	Arbor	k1gInSc1	Arbor
vitae	vita	k1gInSc2	vita
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
399	[number]	k4	399
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87164	[number]	k4	87164
<g/>
-	-	kIx~	-
<g/>
84	[number]	k4	84
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Monografie	monografie	k1gFnSc1	monografie
vydaná	vydaný	k2eAgFnSc1d1	vydaná
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
výstavy	výstava	k1gFnSc2	výstava
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Reynka	Reynek	k1gMnSc2	Reynek
v	v	k7c6	v
Galerii	galerie	k1gFnSc6	galerie
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
CHALUPA	Chalupa	k1gMnSc1	Chalupa
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
;	;	kIx,	;
REYNEK	REYNEK	kA	REYNEK
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Reynek	Reynek	k1gMnSc1	Reynek
<g/>
:	:	kIx,	:
mezi	mezi	k7c7	mezi
nebem	nebe	k1gNnSc7	nebe
a	a	k8xC	a
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Řevnicích	řevnice	k1gFnPc6	řevnice
:	:	kIx,	:
Arbor	Arbora	k1gFnPc2	Arbora
vitae	vitae	k1gNnPc2	vitae
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
galerií	galerie	k1gFnSc7	galerie
a	a	k8xC	a
nakl	nakla	k1gFnPc2	nakla
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
ve	v	k7c6	v
dne	den	k1gInSc2	den
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
prospěšnou	prospěšný	k2eAgFnSc7d1	prospěšná
společností	společnost	k1gFnSc7	společnost
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
131	[number]	k4	131
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87164	[number]	k4	87164
<g/>
-	-	kIx~	-
<g/>
71	[number]	k4	71
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Životopisná	životopisný	k2eAgNnPc1d1	životopisné
data	datum	k1gNnPc1	datum
<g/>
.	.	kIx.	.
</s>
<s>
PUTNA	putna	k1gFnSc1	putna
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
C.	C.	kA	C.
Česká	český	k2eAgFnSc1d1	Česká
katolická	katolický	k2eAgFnSc1d1	katolická
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
kontextech	kontext	k1gInPc6	kontext
<g/>
:	:	kIx,	:
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Bedřich	Bedřich	k1gMnSc1	Bedřich
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Torst	Torst	k1gMnSc1	Torst
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
1390	[number]	k4	1390
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7215	[number]	k4	7215
<g/>
-	-	kIx~	-
<g/>
391	[number]	k4	391
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
kultury	kultura	k1gFnSc2	kultura
ČR	ČR	kA	ČR
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
Literatura	literatura	k1gFnSc1	literatura
a	a	k8xC	a
autoři	autor	k1gMnPc1	autor
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
-	-	kIx~	-
Rub	roubit	k5eAaImRp2nS	roubit
tit	tit	k?	tit
<g/>
.	.	kIx.	.
s.	s.	k?	s.
GERMAIN	GERMAIN	kA	GERMAIN
<g/>
,	,	kIx,	,
Sylvie	Sylvie	k1gFnSc1	Sylvie
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Reynek	Reynek	k1gMnSc1	Reynek
v	v	k7c6	v
Petrkově	Petrkův	k2eAgFnSc6d1	Petrkova
<g/>
:	:	kIx,	:
poutník	poutník	k1gMnSc1	poutník
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
příbytku	příbytek	k1gInSc6	příbytek
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
:	:	kIx,	:
Literární	literární	k2eAgFnSc1d1	literární
čajovna	čajovna	k1gFnSc1	čajovna
Suzanne	Suzann	k1gInSc5	Suzann
Renaud	Renaud	k1gMnSc1	Renaud
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
124	[number]	k4	124
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902231	[number]	k4	902231
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
HALASOVÁ	Halasová	k1gFnSc1	Halasová
<g/>
,	,	kIx,	,
Dagmar	Dagmar	k1gFnSc1	Dagmar
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Reynek	Reynek	k1gMnSc1	Reynek
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Petrov	Petrov	k1gInSc1	Petrov
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
126	[number]	k4	126
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85247	[number]	k4	85247
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Frontispis	frontispis	k1gInSc1	frontispis
<g/>
.	.	kIx.	.
</s>
<s>
REYNEK	REYNEK	kA	REYNEK
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
chodí	chodit	k5eAaImIp3nS	chodit
tmami	tma	k1gFnPc7	tma
<g/>
:	:	kIx,	:
Daniel	Daniel	k1gMnSc1	Daniel
Reynek	Reynek	k1gMnSc1	Reynek
-	-	kIx~	-
Jiří	Jiří	k1gMnSc1	Jiří
Reynek	Reynek	k1gMnSc1	Reynek
<g/>
.	.	kIx.	.
rozhovor	rozhovor	k1gInSc1	rozhovor
Aleše	Aleš	k1gMnSc2	Aleš
Palána	Palán	k1gMnSc2	Palán
;	;	kIx,	;
předmluvu	předmluva	k1gFnSc4	předmluva
napsal	napsat	k5eAaPmAgMnS	napsat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Med	med	k1gInSc1	med
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Torst	Torst	k1gMnSc1	Torst
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
310	[number]	k4	310
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7215	[number]	k4	7215
<g/>
-	-	kIx~	-
<g/>
218	[number]	k4	218
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
Grafika	grafika	k1gFnSc1	grafika
Grafické	grafický	k2eAgFnSc2d1	grafická
techniky	technika	k1gFnSc2	technika
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bohuslav	Bohuslava	k1gFnPc2	Bohuslava
Reynek	Reynko	k1gNnPc2	Reynko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Reynek	Reynek	k1gMnSc1	Reynek
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Reynek	Reynek	k1gMnSc1	Reynek
Informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
abART	abART	k?	abART
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Rynek	rynek	k1gInSc4	rynek
Osamělý	osamělý	k2eAgMnSc1d1	osamělý
pastýř	pastýř	k1gMnSc1	pastýř
-	-	kIx~	-
Grafické	grafický	k2eAgNnSc1d1	grafické
dílo	dílo	k1gNnSc1	dílo
(	(	kIx(	(
<g/>
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
prodeje	prodej	k1gInSc2	prodej
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Grafické	grafický	k2eAgNnSc4d1	grafické
dílo	dílo	k1gNnSc4	dílo
a	a	k8xC	a
básně	báseň	k1gFnPc4	báseň
-	-	kIx~	-
mnoho	mnoho	k4c1	mnoho
ukázek	ukázka	k1gFnPc2	ukázka
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Dokument	dokument	k1gInSc1	dokument
ČT2	ČT2	k1gFnSc2	ČT2
o	o	k7c6	o
Reynkovi	Reynek	k1gMnSc6	Reynek
<g/>
,	,	kIx,	,
básně	báseň	k1gFnPc4	báseň
čte	číst	k5eAaImIp3nS	číst
Ivan	Ivan	k1gMnSc1	Ivan
Martin	Martin	k1gMnSc1	Martin
Jirous	Jirous	k1gMnSc1	Jirous
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
recenze	recenze	k1gFnSc1	recenze
knihy	kniha	k1gFnSc2	kniha
Suzanne	Suzann	k1gInSc5	Suzann
Renaud	Renaud	k1gInSc1	Renaud
<g/>
,	,	kIx,	,
Petrkov	Petrkov	k1gInSc1	Petrkov
13	[number]	k4	13
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Reynkova	Reynkův	k2eAgFnSc1d1	Reynkova
báseň	báseň	k1gFnSc1	báseň
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
New	New	k1gMnSc1	New
Yorker	Yorker	k1gMnSc1	Yorker
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
