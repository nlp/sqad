<p>
<s>
Naruhito	Naruhit	k2eAgNnSc1d1	Naruhito
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
皇	皇	k?	皇
Kótaiši	Kótaiš	k1gMnSc5	Kótaiš
Naruhito	Naruhita	k1gMnSc5	Naruhita
Šinnó	Šinnó	k1gMnSc5	Šinnó
<g/>
,	,	kIx,	,
narozen	narozen	k2eAgMnSc1d1	narozen
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
syn	syn	k1gMnSc1	syn
bývalého	bývalý	k2eAgMnSc2d1	bývalý
císaře	císař	k1gMnSc2	císař
Akihita	Akihitum	k1gNnSc2	Akihitum
a	a	k8xC	a
126	[number]	k4	126
<g/>
.	.	kIx.	.
japonský	japonský	k2eAgMnSc1d1	japonský
císař	císař	k1gMnSc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
světovým	světový	k2eAgMnSc7d1	světový
monarchou	monarcha	k1gMnSc7	monarcha
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nosí	nosit	k5eAaImIp3nS	nosit
císařský	císařský	k2eAgInSc4d1	císařský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
je	být	k5eAaImIp3nS	být
oslovován	oslovován	k2eAgInSc1d1	oslovován
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Jeho	jeho	k3xOp3gFnSc4	jeho
císařské	císařský	k2eAgNnSc1d1	císařské
veličenstvo	veličenstvo	k1gNnSc1	veličenstvo
Císař	Císař	k1gMnSc1	Císař
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
či	či	k8xC	či
jen	jen	k6eAd1	jen
"	"	kIx"	"
<g/>
Jeho	jeho	k3xOp3gNnSc1	jeho
císařské	císařský	k2eAgNnSc1d1	císařské
veličenstvo	veličenstvo	k1gNnSc1	veličenstvo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
psaném	psaný	k2eAgInSc6d1	psaný
projevu	projev	k1gInSc6	projev
pak	pak	k6eAd1	pak
"	"	kIx"	"
<g/>
Panující	panující	k2eAgMnSc1d1	panující
Císař	Císař	k1gMnSc1	Císař
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Naruhito	Naruhit	k2eAgNnSc1d1	Naruhito
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
císařské	císařský	k2eAgFnSc6d1	císařská
nemocnici	nemocnice	k1gFnSc6	nemocnice
Kunaičó	Kunaičó	k1gMnSc1	Kunaičó
bjóin	bjóin	k1gMnSc1	bjóin
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1960	[number]	k4	1960
jako	jako	k8xC	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
císaře	císař	k1gMnSc2	císař
Akihita	Akihitum	k1gNnSc2	Akihitum
a	a	k8xC	a
císařovny	císařovna	k1gFnSc2	císařovna
Mičiko	Mičika	k1gFnSc5	Mičika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
magistrem	magister	k1gMnSc7	magister
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
autorem	autor	k1gMnSc7	autor
knihy	kniha	k1gFnSc2	kniha
The	The	k1gMnSc1	The
Thames	Thames	k1gMnSc1	Thames
and	and	k?	and
I	i	k9	i
<g/>
:	:	kIx,	:
A	a	k9	a
Memoir	Memoir	k1gMnSc1	Memoir
of	of	k?	of
Two	Two	k1gFnSc1	Two
Years	Years	k1gInSc1	Years
at	at	k?	at
Oxford	Oxford	k1gInSc1	Oxford
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1991	[number]	k4	1991
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
stal	stát	k5eAaPmAgInS	stát
korunním	korunní	k2eAgMnSc7d1	korunní
princem	princ	k1gMnSc7	princ
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
diplomatkou	diplomatka	k1gFnSc7	diplomatka
Masako	Masako	k1gNnSc4	Masako
Owada	Owad	k1gMnSc2	Owad
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
má	mít	k5eAaImIp3nS	mít
jediného	jediný	k2eAgMnSc4d1	jediný
potomka	potomek	k1gMnSc4	potomek
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Aiko	Aiko	k6eAd1	Aiko
(	(	kIx(	(
<g/>
*	*	kIx~	*
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
podle	podle	k7c2	podle
současného	současný	k2eAgNnSc2d1	současné
dědického	dědický	k2eAgNnSc2d1	dědické
práva	právo	k1gNnSc2	právo
nemůže	moct	k5eNaImIp3nS	moct
nastoupit	nastoupit	k5eAaPmF	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
po	po	k7c6	po
Naruhitově	Naruhitův	k2eAgFnSc6d1	Naruhitův
smrti	smrt	k1gFnSc6	smrt
musí	muset	k5eAaImIp3nP	muset
nastoupit	nastoupit	k5eAaPmF	nastoupit
jiný	jiný	k2eAgInSc4d1	jiný
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
)	)	kIx)	)
rodinný	rodinný	k2eAgMnSc1d1	rodinný
příslušník	příslušník	k1gMnSc1	příslušník
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k1gNnSc7	další
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Fumihito	Fumihit	k2eAgNnSc1d1	Fumihit
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
princ	princ	k1gMnSc1	princ
Hisahito	Hisahit	k2eAgNnSc1d1	Hisahit
(	(	kIx(	(
<g/>
*	*	kIx~	*
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Císařem	Císař	k1gMnSc7	Císař
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
po	po	k7c6	po
předem	předem	k6eAd1	předem
ohlášené	ohlášený	k2eAgFnSc6d1	ohlášená
abdikaci	abdikace	k1gFnSc6	abdikace
Akihita	Akihitum	k1gNnSc2	Akihitum
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Éra	éra	k1gFnSc1	éra
císaře	císař	k1gMnSc2	císař
Naruhita	Naruhit	k1gMnSc2	Naruhit
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Reiwa	Reiwa	k1gFnSc1	Reiwa
(	(	kIx(	(
<g/>
令	令	k?	令
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tituly	titul	k1gInPc4	titul
==	==	k?	==
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1960	[number]	k4	1960
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
Jeho	jeho	k3xOp3gFnSc1	jeho
císařská	císařský	k2eAgFnSc1d1	císařská
Výsost	výsost	k1gFnSc1	výsost
Princ	princ	k1gMnSc1	princ
Hiro	Hiro	k1gMnSc1	Hiro
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
<g/>
:	:	kIx,	:
Jeho	jeho	k3xOp3gFnSc1	jeho
císařská	císařský	k2eAgFnSc1d1	císařská
Výsost	výsost	k1gFnSc1	výsost
Korunní	korunní	k2eAgMnSc1d1	korunní
princ	princ	k1gMnSc1	princ
Japonska	Japonsko	k1gNnSc2	Japonsko
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
:	:	kIx,	:
Jeho	jeho	k3xOp3gFnSc4	jeho
císařské	císařský	k2eAgNnSc1d1	císařské
Veličenstvo	veličenstvo	k1gNnSc1	veličenstvo
Císař	Císař	k1gMnSc1	Císař
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Naruhito	Naruhit	k2eAgNnSc1d1	Naruhito
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Crown	Crown	k1gNnSc1	Crown
Prince	princ	k1gMnSc2	princ
Naruhito	Naruhit	k2eAgNnSc1d1	Naruhito
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
Emperor	Emperor	k1gInSc1	Emperor
Naruhito	Naruhit	k2eAgNnSc1d1	Naruhito
</s>
</p>
