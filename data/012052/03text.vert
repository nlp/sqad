<p>
<s>
Exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jedná	jednat	k5eAaImIp3nS	jednat
jako	jako	k9	jako
vláda	vláda	k1gFnSc1	vláda
určité	určitý	k2eAgFnSc2d1	určitá
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
musí	muset	k5eAaImIp3nP	muset
vykonávat	vykonávat	k5eAaImF	vykonávat
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
zemi	zem	k1gFnSc6	zem
zbavena	zbaven	k2eAgFnSc1d1	zbavena
moci	moc	k1gFnSc3	moc
buď	buď	k8xC	buď
revolucí	revoluce	k1gFnSc7	revoluce
či	či	k8xC	či
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
okupací	okupace	k1gFnSc7	okupace
cizí	cizí	k2eAgFnSc7d1	cizí
mocností	mocnost	k1gFnSc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
případ	případ	k1gInSc1	případ
mnoha	mnoho	k4c2	mnoho
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
okupovaných	okupovaný	k2eAgInPc2d1	okupovaný
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Československo	Československo	k1gNnSc1	Československo
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1940-1945	[number]	k4	1940-1945
své	své	k1gNnSc4	své
Prozatímní	prozatímní	k2eAgNnSc4d1	prozatímní
státní	státní	k2eAgNnSc4d1	státní
zřízení	zřízení	k1gNnSc4	zřízení
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgNnSc1d1	vedené
prezidentem	prezident	k1gMnSc7	prezident
Edvardem	Edvard	k1gMnSc7	Edvard
Benešem	Beneš	k1gMnSc7	Beneš
a	a	k8xC	a
premiérem	premiér	k1gMnSc7	premiér
Janem	Jan	k1gMnSc7	Jan
Šrámkem	Šrámek	k1gMnSc7	Šrámek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Funkce	funkce	k1gFnPc4	funkce
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
Exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
nemůže	moct	k5eNaImIp3nS	moct
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zemi	zem	k1gFnSc6	zem
vládnout	vládnout	k5eAaImF	vládnout
a	a	k8xC	a
její	její	k3xOp3gFnPc4	její
možnosti	možnost	k1gFnPc4	možnost
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgFnPc1d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
změna	změna	k1gFnSc1	změna
poměrů	poměr	k1gInPc2	poměr
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
obnovení	obnovení	k1gNnSc4	obnovení
její	její	k3xOp3gFnSc2	její
nezávislosti	nezávislost	k1gFnSc2	nezávislost
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
změnu	změna	k1gFnSc4	změna
může	moct	k5eAaImIp3nS	moct
připravovat	připravovat	k5eAaImF	připravovat
a	a	k8xC	a
podporovat	podporovat	k5eAaImF	podporovat
i	i	k9	i
z	z	k7c2	z
ciziny	cizina	k1gFnSc2	cizina
diplomatickým	diplomatický	k2eAgNnSc7d1	diplomatické
jednáním	jednání	k1gNnSc7	jednání
i	i	k8xC	i
účinnou	účinný	k2eAgFnSc7d1	účinná
pomocí	pomoc	k1gFnSc7	pomoc
<g/>
,	,	kIx,	,
za	za	k7c2	za
války	válka	k1gFnSc2	válka
například	například	k6eAd1	například
vytvářením	vytváření	k1gNnSc7	vytváření
vlastních	vlastní	k2eAgFnPc2d1	vlastní
vojenských	vojenský	k2eAgFnPc2d1	vojenská
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
možnosti	možnost	k1gFnPc1	možnost
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
závisejí	záviset	k5eAaImIp3nP	záviset
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
politické	politický	k2eAgFnSc6d1	politická
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
na	na	k7c6	na
míře	míra	k1gFnSc6	míra
podpory	podpora	k1gFnSc2	podpora
domácí	domácí	k2eAgFnSc2d1	domácí
-	-	kIx~	-
a	a	k8xC	a
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
nelegitimní	legitimní	k2eNgFnSc2d1	nelegitimní
-	-	kIx~	-
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
i	i	k8xC	i
na	na	k7c6	na
podpoře	podpora	k1gFnSc6	podpora
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
má	mít	k5eAaImIp3nS	mít
exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
starostí	starost	k1gFnSc7	starost
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
přesvědčovat	přesvědčovat	k5eAaImF	přesvědčovat
zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
vlády	vláda	k1gFnPc1	vláda
o	o	k7c4	o
nelegitimnosti	nelegitimnost	k1gFnPc4	nelegitimnost
vlády	vláda	k1gFnSc2	vláda
domácí	domácí	k2eAgFnSc6d1	domácí
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
o	o	k7c6	o
vlastní	vlastní	k2eAgFnSc6d1	vlastní
legitimitě	legitimita	k1gFnSc6	legitimita
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
snazší	snadný	k2eAgNnSc1d2	snazší
za	za	k7c2	za
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
domácí	domácí	k2eAgFnSc1d1	domácí
vláda	vláda	k1gFnSc1	vláda
dosazena	dosazen	k2eAgFnSc1d1	dosazena
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
významné	významný	k2eAgFnPc1d1	významná
země	zem	k1gFnPc1	zem
pokládají	pokládat	k5eAaImIp3nP	pokládat
za	za	k7c4	za
nelegitimní	legitimní	k2eNgInSc4d1	nelegitimní
převrat	převrat	k1gInSc4	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Legitimitu	legitimita	k1gFnSc4	legitimita
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
nelze	lze	k6eNd1	lze
pochopitelně	pochopitelně	k6eAd1	pochopitelně
ověřit	ověřit	k5eAaPmF	ověřit
svobodnými	svobodný	k2eAgFnPc7d1	svobodná
volbami	volba	k1gFnPc7	volba
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
ji	on	k3xPp3gFnSc4	on
zejména	zejména	k9	zejména
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
vykázat	vykázat	k5eAaPmF	vykázat
nějakou	nějaký	k3yIgFnSc7	nějaký
kontinuitou	kontinuita	k1gFnSc7	kontinuita
s	s	k7c7	s
předchozí	předchozí	k2eAgFnSc7d1	předchozí
řádnou	řádný	k2eAgFnSc7d1	řádná
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
exilových	exilový	k2eAgFnPc2d1	exilová
vlád	vláda	k1gFnPc2	vláda
byli	být	k5eAaImAgMnP	být
uprchlí	uprchlý	k2eAgMnPc1d1	uprchlý
panovníci	panovník	k1gMnPc1	panovník
a	a	k8xC	a
pretendenti	pretendent	k1gMnPc1	pretendent
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápasech	zápas	k1gInPc6	zápas
o	o	k7c4	o
následnictví	následnictví	k1gNnSc4	následnictví
po	po	k7c6	po
zemřelém	zemřelý	k2eAgMnSc6d1	zemřelý
panovníkovi	panovník	k1gMnSc6	panovník
se	se	k3xPyFc4	se
poražení	poražený	k2eAgMnPc1d1	poražený
uchazeči	uchazeč	k1gMnPc1	uchazeč
mohli	moct	k5eAaImAgMnP	moct
uchýlit	uchýlit	k5eAaPmF	uchýlit
například	například	k6eAd1	například
k	k	k7c3	k
sousednímu	sousední	k2eAgNnSc3d1	sousední
panovníkovi	panovník	k1gMnSc3	panovník
a	a	k8xC	a
přesvědčovat	přesvědčovat	k5eAaImF	přesvědčovat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c4	v
jejich	jejich	k3xOp3gInSc4	jejich
prospěch	prospěch	k1gInSc4	prospěch
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k9	ještě
v	v	k7c6	v
případě	případ	k1gInSc6	případ
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1649-1660	[number]	k4	1649-1660
z	z	k7c2	z
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
domáhal	domáhat	k5eAaImAgMnS	domáhat
svého	svůj	k3xOyFgInSc2	svůj
návratu	návrat	k1gInSc2	návrat
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
francouzských	francouzský	k2eAgInPc2d1	francouzský
Bourbonů	bourbon	k1gInPc2	bourbon
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1789	[number]	k4	1789
<g/>
-	-	kIx~	-
<g/>
1814	[number]	k4	1814
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Několik	několik	k4yIc1	několik
exilových	exilový	k2eAgFnPc2d1	exilová
vlád	vláda	k1gFnPc2	vláda
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
a	a	k8xC	a
Gruzie	Gruzie	k1gFnPc1	Gruzie
připojeny	připojit	k5eAaPmNgFnP	připojit
k	k	k7c3	k
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
španělská	španělský	k2eAgFnSc1d1	španělská
republikánská	republikánský	k2eAgFnSc1d1	republikánská
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
polská	polský	k2eAgFnSc1d1	polská
exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
exilové	exilový	k2eAgFnPc1d1	exilová
vlády	vláda	k1gFnPc1	vláda
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Holandska	Holandsko	k1gNnSc2	Holandsko
<g/>
,	,	kIx,	,
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
<g/>
,	,	kIx,	,
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
rok	rok	k1gInSc1	rok
později	pozdě	k6eAd2	pozdě
ještě	ještě	k9	ještě
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
a	a	k8xC	a
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
polská	polský	k2eAgFnSc1d1	polská
však	však	k9	však
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
a	a	k8xC	a
rozpustila	rozpustit	k5eAaPmAgFnS	rozpustit
se	se	k3xPyFc4	se
až	až	k9	až
po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
Lecha	Lech	k1gMnSc2	Lech
Wałęsy	Wałęsa	k1gFnSc2	Wałęsa
prezidentem	prezident	k1gMnSc7	prezident
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
indičtí	indický	k2eAgMnPc1d1	indický
nacionalisté	nacionalista	k1gMnPc1	nacionalista
exilovou	exilový	k2eAgFnSc4d1	exilová
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
známou	známý	k2eAgFnSc4d1	známá
také	také	k9	také
jako	jako	k8xS	jako
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Japonci	Japonec	k1gMnPc1	Japonec
obsazeném	obsazený	k2eAgInSc6d1	obsazený
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
také	také	k9	také
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
albánský	albánský	k2eAgMnSc1d1	albánský
král	král	k1gMnSc1	král
Zog	Zog	k1gMnSc1	Zog
a	a	k8xC	a
etiopský	etiopský	k2eAgMnSc1d1	etiopský
císař	císař	k1gMnSc1	císař
Haile	Haile	k1gNnSc2	Haile
Selassie	Selassie	k1gFnSc2	Selassie
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
založil	založit	k5eAaPmAgMnS	založit
novou	nový	k2eAgFnSc4d1	nová
exilovou	exilový	k2eAgFnSc4d1	exilová
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
exilových	exilový	k2eAgFnPc2d1	exilová
vlád	vláda	k1gFnPc2	vláda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
afrických	africký	k2eAgFnPc2d1	africká
a	a	k8xC	a
asijských	asijský	k2eAgFnPc2d1	asijská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
K.	K.	kA	K.
Žaloudek	Žaloudek	k1gMnSc1	Žaloudek
<g/>
,	,	kIx,	,
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Maltézský	maltézský	k2eAgInSc1d1	maltézský
řád	řád	k1gInSc1	řád
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
exilových	exilový	k2eAgFnPc2d1	exilová
vlád	vláda	k1gFnPc2	vláda
Československa	Československo	k1gNnSc2	Československo
</s>
</p>
<p>
<s>
Prozatímní	prozatímní	k2eAgNnSc1d1	prozatímní
státní	státní	k2eAgNnSc1d1	státní
zřízení	zřízení	k1gNnSc1	zřízení
(	(	kIx(	(
<g/>
exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
Jana	Jan	k1gMnSc2	Jan
Šrámka	Šrámek	k1gMnSc2	Šrámek
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
Jana	Jan	k1gMnSc2	Jan
Šrámka	Šrámek	k1gMnSc2	Šrámek
</s>
</p>
<p>
<s>
Státní	státní	k2eAgFnSc1d1	státní
rada	rada	k1gFnSc1	rada
Československá	československý	k2eAgFnSc1d1	Československá
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
fronta	fronta	k1gFnSc1	fronta
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
</s>
</p>
<p>
<s>
Forces	Forces	k1gMnSc1	Forces
françaises	françaises	k1gMnSc1	françaises
libres	libres	k1gMnSc1	libres
(	(	kIx(	(
<g/>
Svobodní	svobodný	k2eAgMnPc1d1	svobodný
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
tibetská	tibetský	k2eAgFnSc1d1	tibetská
správa	správa	k1gFnSc1	správa
</s>
</p>
