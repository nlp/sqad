<s>
Exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jedná	jednat	k5eAaImIp3nS	jednat
jako	jako	k9	jako
vláda	vláda	k1gFnSc1	vláda
určité	určitý	k2eAgFnSc2d1	určitá
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
musí	muset	k5eAaImIp3nP	muset
vykonávat	vykonávat	k5eAaImF	vykonávat
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
