<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
skupina	skupina	k1gFnSc1	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jedná	jednat	k5eAaImIp3nS	jednat
jako	jako	k9	jako
vláda	vláda	k1gFnSc1	vláda
určité	určitý	k2eAgFnSc2d1	určitá
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
?	?	kIx.	?
</s>
