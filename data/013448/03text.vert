<p>
<s>
Závod	závod	k1gInSc1	závod
ve	v	k7c6	v
vodním	vodní	k2eAgInSc6d1	vodní
slalomu	slalom	k1gInSc6	slalom
K1	K1	k1gFnSc2	K1
žen	žena	k1gFnPc2	žena
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1992	[number]	k4	1992
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
na	na	k7c6	na
kanále	kanál	k1gInSc6	kanál
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Parc	Parc	k1gInSc1	Parc
Olímpic	Olímpic	k1gMnSc1	Olímpic
del	del	k?	del
Segre	Segr	k1gInSc5	Segr
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
československých	československý	k2eAgFnPc2d1	Československá
závodnic	závodnice	k1gFnPc2	závodnice
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
Zdenka	Zdenka	k1gFnSc1	Zdenka
Grossmannová	Grossmannová	k1gFnSc1	Grossmannová
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Štěpánka	Štěpánka	k1gFnSc1	Štěpánka
Hilgertová	Hilgertová	k1gFnSc1	Hilgertová
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Marcela	Marcela	k1gFnSc1	Marcela
Sadilová	Sadilová	k1gFnSc1	Sadilová
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
získala	získat	k5eAaPmAgFnS	získat
Němka	Němka	k1gFnSc1	Němka
Elisabeth	Elisabetha	k1gFnPc2	Elisabetha
Michelerová	Michelerová	k1gFnSc1	Michelerová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Barcelona	Barcelona	k1gFnSc1	Barcelona
1992	[number]	k4	1992
Official	Official	k1gInSc1	Official
Report	report	k1gInSc4	report
-	-	kIx~	-
Canoe	Canoe	k1gInSc1	Canoe
/	/	kIx~	/
Kayak	Kayak	k1gInSc1	Kayak
<g/>
,	,	kIx,	,
la	la	k1gNnSc1	la
<g/>
84	[number]	k4	84
<g/>
foundation	foundation	k1gInSc1	foundation
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
