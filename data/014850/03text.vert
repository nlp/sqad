<s>
Nebesa	nebesa	k1gNnPc4
(	(	kIx(
<g/>
Aš	Aš	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Nebesa	nebesa	k1gNnPc4
Lokalita	lokalita	k1gFnSc1
Charakter	charakter	k1gInSc1
</s>
<s>
osada	osada	k1gFnSc1
Obec	obec	k1gFnSc1
</s>
<s>
Aš	Aš	k1gInSc1
Okres	okres	k1gInSc1
</s>
<s>
Cheb	Cheb	k1gInSc1
Kraj	kraj	k1gInSc1
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1
Historická	historický	k2eAgFnSc1d1
země	zem	k1gFnPc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
<g/>
43	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
36	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Nebesa	nebesa	k1gNnPc4
(	(	kIx(
<g/>
5,84	5,84	k4
km²	km²	k?
<g/>
)	)	kIx)
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
685	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
352	#num#	k4
01	#num#	k4
Počet	počet	k1gInSc1
domů	domů	k6eAd1
</s>
<s>
13	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nebesa	nebesa	k1gNnPc4
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
98281	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nebesa	nebesa	k1gNnPc4
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
:	:	kIx,
Himmelreich	Himmelreich	k1gInSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
Egrisch	Egrisch	k1gMnSc1
Reuth	Reuth	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
místní	místní	k2eAgFnSc7d1
částí	část	k1gFnSc7
města	město	k1gNnSc2
Aše	Aš	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
685	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
zde	zde	k6eAd1
trvale	trvale	k6eAd1
žilo	žít	k5eAaImAgNnS
36	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozloha	rozloha	k1gFnSc1
Nebes	nebesa	k1gNnPc2
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
velká	velký	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
většinu	většina	k1gFnSc4
z	z	k7c2
ní	on	k3xPp3gFnSc2
pokrývají	pokrývat	k5eAaImIp3nP
lesy	les	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebesa	nebesa	k1gNnPc4
leží	ležet	k5eAaImIp3nP
3	#num#	k4
kilometry	kilometr	k1gInPc7
od	od	k7c2
Aše	Aš	k1gFnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
Mokřinami	mokřina	k1gFnPc7
a	a	k8xC
Výhledy	výhled	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Zachována	zachován	k2eAgFnSc1d1
zůstala	zůstat	k5eAaPmAgFnS
kaple	kaple	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1907	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
hostinský	hostinský	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
častým	častý	k2eAgInSc7d1
cílem	cíl	k1gInSc7
nejen	nejen	k6eAd1
českých	český	k2eAgNnPc2d1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
německých	německý	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
Nebesích	nebesa	k1gNnPc6
pochází	pocházet	k5eAaImIp3nP
z	z	k7c2
roku	rok	k1gInSc2
1598	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
sedmileté	sedmiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
bylo	být	k5eAaImAgNnS
poblíž	poblíž	k7c2
Nebes	nebesa	k1gNnPc2
vystavěno	vystavěn	k2eAgNnSc1d1
rakouským	rakouský	k2eAgMnSc7d1
maršálem	maršál	k1gMnSc7
Macquirem	Macquir	k1gMnSc7
polní	polní	k2eAgMnSc1d1
opevnění	opevnění	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
rakouské	rakouský	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
8	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1759	#num#	k4
střetly	střetnout	k5eAaPmAgInP
s	s	k7c7
pruským	pruský	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
generála	generál	k1gMnSc2
von	von	k1gInSc4
Fincka	Fincko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
vesnice	vesnice	k1gFnSc1
patřila	patřit	k5eAaImAgFnS
k	k	k7c3
okresu	okres	k1gInSc3
Cheb	Cheb	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
poté	poté	k6eAd1
co	co	k9
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
ašský	ašský	k2eAgInSc1d1
okres	okres	k1gInSc1
<g/>
,	,	kIx,
připadla	připadnout	k5eAaPmAgFnS
jemu	on	k3xPp3gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
nato	nato	k6eAd1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
samostatnou	samostatný	k2eAgFnSc7d1
obcí	obec	k1gFnSc7
i	i	k9
přes	přes	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
Nebes	nebesa	k1gNnPc2
nikdy	nikdy	k6eAd1
nepřekročil	překročit	k5eNaPmAgInS
hranici	hranice	k1gFnSc4
152	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1800	#num#	k4
vedla	vést	k5eAaImAgFnS
Nebesy	nebesa	k1gNnPc7
poštovní	poštovní	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
ve	v	k7c6
velmi	velmi	k6eAd1
špatném	špatný	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
historických	historický	k2eAgInPc2d1
dokumentů	dokument	k1gInPc2
toto	tento	k3xDgNnSc1
potvrzuje	potvrzovat	k5eAaImIp3nS
i	i	k9
básník	básník	k1gMnSc1
Goethe	Goeth	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
tudy	tudy	k6eAd1
projížděl	projíždět	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silnice	silnice	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vede	vést	k5eAaImIp3nS
Nebesy	nebesa	k1gNnPc7
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
leží	ležet	k5eAaImIp3nS
na	na	k7c6
jiném	jiný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1907	#num#	k4
nechal	nechat	k5eAaPmAgMnS
na	na	k7c6
tehdejším	tehdejší	k2eAgNnSc6d1
náměstíčku	náměstíčko	k1gNnSc6
vystavět	vystavět	k5eAaPmF
nebeský	nebeský	k2eAgInSc4d1
lesní	lesní	k2eAgInSc4d1
Glaser	Glaser	k1gInSc4
kapličku	kaplička	k1gFnSc4
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
nyní	nyní	k6eAd1
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
pod	pod	k7c4
ašskou	ašský	k2eAgFnSc4d1
katolickou	katolický	k2eAgFnSc4d1
faru	fara	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyššího	vysoký	k2eAgInSc2d3
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
dosáhly	dosáhnout	k5eAaPmAgFnP
Nebesa	nebesa	k1gNnPc4
v	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
v	v	k7c6
19	#num#	k4
domech	dům	k1gInPc6
152	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skončení	skončení	k1gNnSc6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
vysídleno	vysídlen	k2eAgNnSc1d1
německé	německý	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
<g/>
,	,	kIx,
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
klesl	klesnout	k5eAaPmAgInS
o	o	k7c4
více	hodně	k6eAd2
než	než	k8xS
polovinu	polovina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
následujících	následující	k2eAgNnPc2d1
let	léto	k1gNnPc2
stále	stále	k6eAd1
jen	jen	k9
klesal	klesat	k5eAaImAgMnS
<g/>
,	,	kIx,
na	na	k7c4
dnešních	dnešní	k2eAgInPc2d1
11	#num#	k4
domů	dům	k1gInPc2
a	a	k8xC
13	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Památky	památka	k1gFnPc1
</s>
<s>
kaple	kaple	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1907	#num#	k4
</s>
<s>
krucifix	krucifix	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1862	#num#	k4
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
1850	#num#	k4
</s>
<s>
1930	#num#	k4
</s>
<s>
1947	#num#	k4
</s>
<s>
1961	#num#	k4
</s>
<s>
1970	#num#	k4
</s>
<s>
2001	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
104	#num#	k4
</s>
<s>
152	#num#	k4
</s>
<s>
72	#num#	k4
</s>
<s>
58	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
900	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
-	-	kIx~
<g/>
2394	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
264	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1850	#num#	k4
a	a	k8xC
2001	#num#	k4
<g/>
,	,	kIx,
podle	podle	k7c2
publikace	publikace	k1gFnSc2
"	"	kIx"
<g/>
Obce	obec	k1gFnPc1
Ašska	Ašsko	k1gNnSc2
v	v	k7c6
proměnách	proměna	k1gFnPc6
času	čas	k1gInSc2
<g/>
,	,	kIx,
Kolektiv	kolektiv	k1gInSc1
autorů	autor	k1gMnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
"	"	kIx"
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Části	část	k1gFnPc1
města	město	k1gNnSc2
Aš	Aš	k1gFnSc4
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
AšDolní	AšDolní	k2eAgInSc1d1
PasekyDoubravaHorní	PasekyDoubravaHorní	k2eAgInSc1d1
PasekyKopaninyMokřinyNebesaNový	PasekyKopaninyMokřinyNebesaNový	k2eAgInSc1d1
ŽďárVernéřov	ŽďárVernéřov	k1gInSc1
</s>
