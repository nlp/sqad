<s>
Cizinka	cizinka	k1gFnSc1
(	(	kIx(
<g/>
kniha	kniha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CizinkaAutor	CizinkaAutor	k1gMnSc1
</s>
<s>
Diana	Diana	k1gFnSc1
Gabaldon	Gabaldona	k1gFnPc2
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Outlander	Outlander	k1gInSc1
Cyklus	cyklus	k1gInSc1
</s>
<s>
Cizinka	cizinka	k1gFnSc1
Datum	datum	k1gNnSc1
vydání	vydání	k1gNnSc6
</s>
<s>
1991	#num#	k4
Česky	česky	k6eAd1
vydáno	vydat	k5eAaPmNgNnS
</s>
<s>
2010	#num#	k4
Počet	počet	k1gInSc1
stran	strana	k1gFnPc2
</s>
<s>
663	#num#	k4
ISBN	ISBN	kA
</s>
<s>
978-80-87374-16-0	978-80-87374-16-0	k4
Předchozí	předchozí	k2eAgInSc4d1
a	a	k8xC
následující	následující	k2eAgInSc4d1
díl	díl	k1gInSc4
</s>
<s>
Vážka	vážka	k1gFnSc1
v	v	k7c6
jantaru	jantar	k1gInSc6
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Cizinka	cizinka	k1gFnSc1
je	být	k5eAaImIp3nS
první	první	k4xOgInPc1
ze	z	k7c2
série	série	k1gFnSc2
osmi	osm	k4xCc2
historických	historický	k2eAgInPc2d1
multižánrových	multižánrová	k2eAgInPc2d1
románů	román	k1gInPc2
od	od	k7c2
Diany	Diana	k1gFnSc2
Gabaldon	Gabaldon	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
a	a	k8xC
zaměřuje	zaměřovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
zdravotní	zdravotní	k2eAgFnSc4d1
sestru	sestra	k1gFnSc4
Claire	Clair	k1gInSc5
Randallovou	Randallová	k1gFnSc7
z	z	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
cestuje	cestovat	k5eAaImIp3nS
v	v	k7c6
čase	čas	k1gInSc6
do	do	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
zažívá	zažívat	k5eAaImIp3nS
dobrodružství	dobrodružství	k1gNnSc4
a	a	k8xC
nachází	nacházet	k5eAaImIp3nS
lásku	láska	k1gFnSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
temperamentního	temperamentní	k2eAgInSc2d1
Jamese	James	k1gInSc6
Frasera	Fraser	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
V	v	k7c6
sérii	série	k1gFnSc6
Cizinka	cizinka	k1gFnSc1
se	se	k3xPyFc4
prolíná	prolínat	k5eAaImIp3nS
hned	hned	k6eAd1
několik	několik	k4yIc4
žánrů	žánr	k1gInPc2
od	od	k7c2
historické	historický	k2eAgFnSc2d1
fikce	fikce	k1gFnSc2
<g/>
,	,	kIx,
romance	romance	k1gFnSc2
<g/>
,	,	kIx,
dobrodružství	dobrodružství	k1gNnSc2
až	až	k8xS
po	po	k7c6
science	scienka	k1gFnSc6
fiction	fiction	k1gInSc4
a	a	k8xC
fantasy	fantas	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Cizinka	cizinka	k1gFnSc1
vyhrála	vyhrát	k5eAaPmAgFnS
cenu	cena	k1gFnSc4
Romance	romance	k1gFnSc2
Writers	Writers	k1gInSc1
of	of	k?
America	America	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
RITA	rito	k1gNnSc2
za	za	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
romanci	romance	k1gFnSc4
roku	rok	k1gInSc2
1991	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2014	#num#	k4
měla	mít	k5eAaImAgFnS
premiéru	premiéra	k1gFnSc4
televizní	televizní	k2eAgFnSc1d1
adaptace	adaptace	k1gFnSc1
tohoto	tento	k3xDgInSc2
románu	román	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
jsou	být	k5eAaImIp3nP
kvůli	kvůli	k7c3
své	svůj	k3xOyFgFnSc3
práci	práce	k1gFnSc3
během	během	k7c2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
britská	britský	k2eAgFnSc1d1
zdravotní	zdravotní	k2eAgFnSc1d1
sestra	sestra	k1gFnSc1
Claire	Clair	k1gInSc5
Randallová	Randallová	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gMnSc1
manžel	manžel	k1gMnSc1
Frank	Frank	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
historie	historie	k1gFnSc2
<g/>
,	,	kIx,
odloučeni	odloučen	k2eAgMnPc1d1
<g/>
,	,	kIx,
vydají	vydat	k5eAaPmIp3nP
se	se	k3xPyFc4
na	na	k7c4
druhé	druhý	k4xOgFnPc4
líbánky	líbánky	k1gFnPc4
do	do	k7c2
Inverness	Invernessa	k1gFnPc2
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
Frank	Frank	k1gMnSc1
pátrá	pátrat	k5eAaImIp3nS
po	po	k7c6
historii	historie	k1gFnSc6
své	svůj	k3xOyFgFnSc2
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Claire	Clair	k1gInSc5
sbírá	sbírat	k5eAaImIp3nS
léčivé	léčivý	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
poblíž	poblíž	k7c2
menhirů	menhir	k1gInPc2
na	na	k7c6
kopci	kopec	k1gInSc6
Craigh	Craigh	k1gInSc4
na	na	k7c4
Dun	duna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Najednou	najednou	k6eAd1
poblíž	poblíž	k7c2
kamenů	kámen	k1gInPc2
uslyší	uslyšet	k5eAaPmIp3nS
bzučivý	bzučivý	k2eAgInSc1d1
zvuk	zvuk	k1gInSc1
a	a	k8xC
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
omdlí	omdlet	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
probere	probrat	k5eAaPmIp3nS
<g/>
,	,	kIx,
čelí	čelit	k5eAaImIp3nS
Frankovu	Frankův	k2eAgMnSc3d1
předku	předek	k1gMnSc3
kapitánu	kapitán	k1gMnSc3
Jacku	Jacek	k1gMnSc3
Randallovi	Randall	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Než	než	k8xS
ji	on	k3xPp3gFnSc4
kapitán	kapitán	k1gMnSc1
stihne	stihnout	k5eAaPmIp3nS
zatknout	zatknout	k5eAaPmF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
omráčen	omráčet	k5eAaImNgMnS,k5eAaPmNgMnS
Skotem	skot	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
Claire	Clair	k1gInSc5
přivede	přivést	k5eAaPmIp3nS
do	do	k7c2
své	svůj	k3xOyFgFnSc2
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
Skotové	Skot	k1gMnPc1
pokoušení	pokoušený	k2eAgMnPc1d1
napravit	napravit	k5eAaPmF
vykloubené	vykloubený	k2eAgNnSc4d1
rameno	rameno	k1gNnSc4
svého	svůj	k3xOyFgMnSc2
druha	druh	k1gMnSc2
Jamieho	Jamie	k1gMnSc2
<g/>
,	,	kIx,
Claire	Clair	k1gInSc5
využije	využít	k5eAaPmIp3nS
svoje	svůj	k3xOyFgFnPc4
zkušenosti	zkušenost	k1gFnPc4
a	a	k8xC
napraví	napravit	k5eAaPmIp3nS
ho	on	k3xPp3gNnSc4
sama	sám	k3xTgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muži	muž	k1gMnPc1
se	se	k3xPyFc4
představí	představit	k5eAaPmIp3nP
jako	jako	k9
členové	člen	k1gMnPc1
klanu	klan	k1gInSc2
MacKenzie	MacKenzie	k1gFnSc2
a	a	k8xC
Claire	Clair	k1gInSc5
si	se	k3xPyFc3
musí	muset	k5eAaImIp3nP
přiznat	přiznat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
přicestovala	přicestovat	k5eAaPmAgFnS
v	v	k7c6
čase	čas	k1gInSc6
do	do	k7c2
minulosti	minulost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představí	představit	k5eAaPmIp3nS
se	se	k3xPyFc4
proto	proto	k8xC
jako	jako	k9
anglická	anglický	k2eAgFnSc1d1
vdova	vdova	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
cestuje	cestovat	k5eAaImIp3nS
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
setkala	setkat	k5eAaPmAgFnS
se	se	k3xPyFc4
svojí	svůj	k3xOyFgFnSc7
rodinou	rodina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skotové	Skot	k1gMnPc1
jí	on	k3xPp3gFnSc7
ale	ale	k8xC
neuvěří	uvěřit	k5eNaPmIp3nP
a	a	k8xC
vezmou	vzít	k5eAaPmIp3nP
ji	on	k3xPp3gFnSc4
do	do	k7c2
hradu	hrad	k1gInSc2
Leoch	Leocha	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Claire	Clair	k1gInSc5
hledá	hledat	k5eAaImIp3nS
cestu	cesta	k1gFnSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
by	by	kYmCp3nS
se	se	k3xPyFc4
mohla	moct	k5eAaImAgFnS
vrátit	vrátit	k5eAaPmF
do	do	k7c2
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Skotové	Skot	k1gMnPc1
vidí	vidět	k5eAaImIp3nP
Claire	Clair	k1gInSc5
jako	jako	k8xC,k8xS
„	„	k?
<g/>
Sassenach	Sassenach	k1gInSc1
<g/>
“	“	k?
–	–	k?
Cizinku	cizinka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
ignoruje	ignorovat	k5eAaImIp3nS
kulturu	kultura	k1gFnSc4
skotské	skotský	k2eAgFnSc2d1
vysočiny	vysočina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc2
lékařské	lékařský	k2eAgFnSc2d1
dovednosti	dovednost	k1gFnSc2
si	se	k3xPyFc3
ovšem	ovšem	k9
získají	získat	k5eAaPmIp3nP
jejich	jejich	k3xOp3gInSc4
respekt	respekt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Colum	Colum	k1gNnSc1
MacKenzie	MacKenzie	k1gFnSc1
ji	on	k3xPp3gFnSc4
podezřívá	podezřívat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
britskou	britský	k2eAgFnSc7d1
špionkou	špionka	k1gFnSc7
<g/>
,	,	kIx,
proto	proto	k8xC
ji	on	k3xPp3gFnSc4
pošle	poslat	k5eAaPmIp3nS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
bratrem	bratr	k1gMnSc7
Dougalem	Dougal	k1gInSc7
vybrat	vybrat	k5eAaPmF
rentu	renta	k1gFnSc4
a	a	k8xC
dary	dar	k1gInPc4
pro	pro	k7c4
jakobity	jakobita	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
to	ten	k3xDgNnSc4
dohlíží	dohlížet	k5eAaImIp3nS
Ned	Ned	k1gMnSc1
Gowan	Gowan	k1gMnSc1
<g/>
,	,	kIx,
právník	právník	k1gMnSc1
z	z	k7c2
Edinburghu	Edinburgh	k1gInSc2
<g/>
,	,	kIx,
také	také	k9
patřící	patřící	k2eAgFnSc1d1
do	do	k7c2
klanu	klan	k1gInSc2
MacKenziů	MacKenzi	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitán	kapitán	k1gMnSc1
Randall	Randall	k1gMnSc1
požádá	požádat	k5eAaPmIp3nS
Dougala	Dougal	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
přivedl	přivést	k5eAaPmAgMnS
Claire	Clair	k1gInSc5
k	k	k7c3
výslechu	výslech	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nP
ji	on	k3xPp3gFnSc4
od	od	k7c2
něj	on	k3xPp3gNnSc2
zachránil	zachránit	k5eAaPmAgMnS
<g/>
,	,	kIx,
Dougal	Dougal	k1gMnSc1
jí	on	k3xPp3gFnSc3
poradí	poradit	k5eAaPmIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vdala	vdát	k5eAaPmAgFnS
za	za	k7c4
Jamieho	Jamie	k1gMnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
také	také	k9
udělá	udělat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdělená	rozdělená	k1gFnSc1
mezi	mezi	k7c4
svoje	svůj	k3xOyFgNnSc4
pouto	pouto	k1gNnSc4
k	k	k7c3
Jamiemu	Jamiem	k1gInSc3
a	a	k8xC
myšlenkami	myšlenka	k1gFnPc7
na	na	k7c4
Franka	Frank	k1gMnSc4
se	s	k7c7
Claire	Clair	k1gInSc5
pokusí	pokusit	k5eAaPmIp3nS
vrátit	vrátit	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c4
Craigh	Craigh	k1gInSc4
na	na	k7c4
Dun	duna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
to	ten	k3xDgNnSc1
nepodaří	podařit	k5eNaPmIp3nS
<g/>
,	,	kIx,
přijme	přijmout	k5eAaPmIp3nS
svoji	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
hradní	hradní	k2eAgFnSc2d1
léčitelky	léčitelka	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
seznámí	seznámit	k5eAaPmIp3nS
s	s	k7c7
Geilis	Geilis	k1gFnSc7
Duncanovou	Duncanový	k2eAgFnSc7d1
–	–	k?
ženou	žena	k1gFnSc7
místního	místní	k2eAgMnSc2d1
úředníka	úředník	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
sdílí	sdílet	k5eAaImIp3nS
její	její	k3xOp3gFnSc4
lásku	láska	k1gFnSc4
k	k	k7c3
medicíně	medicína	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
Jamie	Jamie	k1gFnSc1
pryč	pryč	k6eAd1
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc1
jsou	být	k5eAaImIp3nP
obviněny	obvinit	k5eAaPmNgInP
z	z	k7c2
čarodějnictví	čarodějnictví	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
Jamie	Jamie	k1gFnSc1
se	se	k3xPyFc4
včas	včas	k6eAd1
vrátí	vrátit	k5eAaPmIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Claire	Clair	k1gInSc5
zachránil	zachránit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těsně	těsně	k6eAd1
před	před	k7c7
útěkem	útěk	k1gInSc7
si	se	k3xPyFc3
Claire	Clair	k1gInSc5
uvědomí	uvědomit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Geilis	Geilis	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
z	z	k7c2
budoucnosti	budoucnost	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
uvidí	uvidět	k5eAaPmIp3nS
na	na	k7c6
její	její	k3xOp3gFnSc6
ruce	ruka	k1gFnSc6
jizvu	jizva	k1gFnSc4
po	po	k7c6
očkování	očkování	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geilis	Geilis	k1gInSc1
si	se	k3xPyFc3
uvědomí	uvědomit	k5eAaPmIp3nS
totéž	týž	k3xTgNnSc4
<g/>
.	.	kIx.
</s>
<s>
Claire	Clair	k1gMnSc5
se	se	k3xPyFc4
stále	stále	k6eAd1
potýká	potýkat	k5eAaImIp3nS
se	s	k7c7
svými	svůj	k3xOyFgInPc7
dilematem	dilema	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
ji	on	k3xPp3gFnSc4
Jamie	Jamie	k1gFnSc1
vezme	vzít	k5eAaPmIp3nS
na	na	k7c4
Craigh	Craigh	k1gInSc4
na	na	k7c4
Dun	duna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
jí	on	k3xPp3gFnSc3
umožní	umožnit	k5eAaPmIp3nS
rozhodnout	rozhodnout	k5eAaPmF
se	se	k3xPyFc4
<g/>
,	,	kIx,
jestli	jestli	k8xS
chce	chtít	k5eAaImIp3nS
zůstat	zůstat	k5eAaPmF
s	s	k7c7
ním	on	k3xPp3gMnSc7
nebo	nebo	k8xC
se	se	k3xPyFc4
vrátit	vrátit	k5eAaPmF
k	k	k7c3
Frankovi	Frank	k1gMnSc3
<g/>
,	,	kIx,
Claire	Clair	k1gInSc5
zůstane	zůstat	k5eAaPmIp3nS
a	a	k8xC
Jamie	Jamie	k1gFnSc1
ji	on	k3xPp3gFnSc4
vezme	vzít	k5eAaPmIp3nS
k	k	k7c3
sobě	se	k3xPyFc3
domů	domů	k6eAd1
na	na	k7c4
Lallybroch	Lallybroch	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
žijí	žít	k5eAaImIp3nP
šťastně	šťastně	k6eAd1
a	a	k8xC
poklidně	poklidně	k6eAd1
s	s	k7c7
Jamieho	Jamie	k1gMnSc2
sestrou	sestra	k1gFnSc7
Jenny	Jenna	k1gFnSc2
a	a	k8xC
jejím	její	k3xOp3gMnSc7
manželem	manžel	k1gMnSc7
Ianem	Ianus	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jamie	Jamie	k1gFnSc1
je	být	k5eAaImIp3nS
však	však	k9
stále	stále	k6eAd1
uprchlíkem	uprchlík	k1gMnSc7
z	z	k7c2
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k6eAd1
si	se	k3xPyFc3
nárokuje	nárokovat	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
titul	titul	k1gInSc4
pána	pán	k1gMnSc2
Lallybrochu	Lallybroch	k1gInSc2
<g/>
,	,	kIx,
dokud	dokud	k8xS
ho	on	k3xPp3gInSc4
jeden	jeden	k4xCgMnSc1
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
poddaných	poddaný	k1gMnPc2
nezradí	zradit	k5eNaPmIp3nS
a	a	k8xC
on	on	k3xPp3gMnSc1
je	být	k5eAaImIp3nS
poslán	poslat	k5eAaPmNgMnS
do	do	k7c2
Wentworthského	Wentworthské	k1gNnSc2
vězení	vězení	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
pod	pod	k7c7
velením	velení	k1gNnSc7
kapitána	kapitán	k1gMnSc2
Randalla	Randall	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Claire	Clair	k1gInSc5
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
za	za	k7c2
pomoci	pomoc	k1gFnSc2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
klanu	klan	k1gInSc2
pokusí	pokusit	k5eAaPmIp3nS
osvobodit	osvobodit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
Randall	Randall	k1gInSc1
ji	on	k3xPp3gFnSc4
chytí	chytit	k5eAaPmIp3nS
a	a	k8xC
hrozí	hrozit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
znásilní	znásilnit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jamie	Jamie	k1gFnSc1
se	se	k3xPyFc4
nabídne	nabídnout	k5eAaPmIp3nS
na	na	k7c4
její	její	k3xOp3gNnSc4
místo	místo	k1gNnSc4
a	a	k8xC
Randall	Randall	k1gInSc4
pošle	poslat	k5eAaPmIp3nS
Claire	Clair	k1gInSc5
do	do	k7c2
lesa	les	k1gInSc2
mimo	mimo	k7c4
hrad	hrad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Claire	Clair	k1gInSc5
Randallovi	Randallův	k2eAgMnPc1d1
pohrozí	pohrozit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	on	k3xPp3gFnPc4
čarodějnice	čarodějnice	k1gFnPc4
a	a	k8xC
předpovídá	předpovídat	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
svatbě	svatba	k1gFnSc6
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
otcem	otec	k1gMnSc7
dítěte	dítě	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
ale	ale	k9
nikdy	nikdy	k6eAd1
neuvidí	uvidět	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Claire	Clair	k1gInSc5
se	se	k3xPyFc4
pak	pak	k6eAd1
ujme	ujmout	k5eAaPmIp3nS
sir	sir	k1gMnSc1
Marcus	Marcus	k1gMnSc1
MacRannoch	MacRannoch	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
nápadník	nápadník	k1gMnSc1
Jamieho	Jamie	k1gMnSc2
matky	matka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
jeho	jeho	k3xOp3gMnPc7
muži	muž	k1gMnPc7
rozptýlí	rozptýlit	k5eAaPmIp3nP
stráže	stráž	k1gFnPc1
ve	v	k7c6
vězení	vězení	k1gNnSc6
<g/>
,	,	kIx,
MacKenziovi	MacKenziův	k2eAgMnPc1d1
muži	muž	k1gMnPc1
vpustí	vpustit	k5eAaPmIp3nP
dovnitř	dovnitř	k6eAd1
stádo	stádo	k1gNnSc4
dobytka	dobytek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
stráže	stráž	k1gFnSc2
udupe	udupat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachrání	zachránit	k5eAaPmIp3nS
Jamieho	Jamie	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
Randallem	Randall	k1gMnSc7
psychicky	psychicky	k6eAd1
i	i	k9
sexuálně	sexuálně	k6eAd1
mučen	mučen	k2eAgInSc4d1
a	a	k8xC
vezmou	vzít	k5eAaPmIp3nP
ho	on	k3xPp3gNnSc4
do	do	k7c2
MacRannochova	MacRannochův	k2eAgNnSc2d1
sídla	sídlo	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Claire	Clair	k1gInSc5
ošetří	ošetřit	k5eAaPmIp3nS
nejhorší	zlý	k2eAgMnSc1d3
z	z	k7c2
jeho	jeho	k3xOp3gNnPc2
zranění	zranění	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
je	on	k3xPp3gFnPc4
Jamie	Jamie	k1gFnPc4
schopný	schopný	k2eAgInSc1d1
<g/>
,	,	kIx,
utečou	utéct	k5eAaPmIp3nP
spolu	spolu	k6eAd1
s	s	k7c7
jeho	jeho	k3xOp3gMnSc7
kmotrem	kmotr	k1gMnSc7
Murtaghem	Murtagh	k1gInSc7
do	do	k7c2
kláštera	klášter	k1gInSc2
svaté	svatý	k2eAgFnSc2d1
Anny	Anna	k1gFnSc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
žije	žít	k5eAaImIp3nS
Jamieho	Jamie	k1gMnSc4
strýček	strýček	k1gMnSc1
Abbot	Abbot	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
Jamie	Jamie	k1gFnSc1
zažívá	zažívat	k5eAaImIp3nS
nesnesitelné	snesitelný	k2eNgFnPc4d1
muka	muka	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc1
zranění	zranění	k1gNnPc1
jsou	být	k5eAaImIp3nP
vážná	vážné	k1gNnPc4
<g/>
,	,	kIx,
pravou	pravý	k2eAgFnSc4d1
ruku	ruka	k1gFnSc4
má	mít	k5eAaImIp3nS
zdeformovanou	zdeformovaný	k2eAgFnSc7d1
a	a	k8xC
po	po	k7c6
psychické	psychický	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
na	na	k7c6
tom	ten	k3xDgNnSc6
taky	taky	k6eAd1
není	být	k5eNaImIp3nS
zrovna	zrovna	k6eAd1
nejlépe	dobře	k6eAd3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
Claire	Clair	k1gInSc5
se	se	k3xPyFc4
nevzdá	vzdát	k5eNaPmIp3nS
a	a	k8xC
za	za	k7c4
každou	každý	k3xTgFnSc4
cenu	cena	k1gFnSc4
chce	chtít	k5eAaImIp3nS
svého	svůj	k3xOyFgMnSc2
manžela	manžel	k1gMnSc2
zachránit	zachránit	k5eAaPmF
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
nakonec	nakonec	k9
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
je	být	k5eAaImIp3nS
Claire	Clair	k1gInSc5
poprvé	poprvé	k6eAd1
těhotná	těhotný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
</s>
<s>
Claire	Clair	k1gMnSc5
Beauchampová	Beauchampový	k2eAgFnSc1d1
Randallová	Randallový	k2eAgFnSc1d1
Fraserová	Fraserová	k1gFnSc1
</s>
<s>
Horkokrevná	horkokrevný	k2eAgFnSc1d1
<g/>
,	,	kIx,
praktická	praktický	k2eAgFnSc1d1
a	a	k8xC
nezávislá	závislý	k2eNgFnSc1d1
zdravotní	zdravotní	k2eAgFnSc1d1
sestra	sestra	k1gFnSc1
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
cestuje	cestovat	k5eAaImIp3nS
v	v	k7c6
čase	čas	k1gInSc6
na	na	k7c4
skotskou	skotský	k2eAgFnSc4d1
vysočinu	vysočina	k1gFnSc4
do	do	k7c2
poloviny	polovina	k1gFnSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
je	být	k5eAaImIp3nS
ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vdaná	vdaný	k2eAgFnSc1d1
za	za	k7c4
Franka	Frank	k1gMnSc4
Randalla	Randall	k1gMnSc4
<g/>
,	,	kIx,
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
zamiluje	zamilovat	k5eAaPmIp3nS
do	do	k7c2
Jamieho	Jamie	k1gMnSc2
Frasera	Fraser	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nadanou	nadaný	k2eAgFnSc7d1
léčitelkou	léčitelka	k1gFnSc7
a	a	k8xC
amatérskou	amatérský	k2eAgFnSc7d1
botaničkou	botanička	k1gFnSc7
<g/>
,	,	kIx,
jedináčkem	jedináček	k1gMnSc7
a	a	k8xC
sirotkem	sirotek	k1gMnSc7
vychovaným	vychovaný	k2eAgMnSc7d1
strýcem	strýc	k1gMnSc7
archeologem	archeolog	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
James	James	k1gMnSc1
„	„	k?
<g/>
Jamie	Jamie	k1gFnSc2
<g/>
“	“	k?
MacKenzie	MacKenzie	k1gFnSc2
Fraser	Fraser	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
alias	alias	k9
Jamie	Jamie	k1gFnSc1
MacTavish	MacTavish	k1gMnSc1
<g/>
)	)	kIx)
Urostlý	urostlý	k2eAgMnSc1d1
<g/>
,	,	kIx,
mladý	mladý	k2eAgMnSc1d1
<g/>
,	,	kIx,
skotský	skotský	k2eAgMnSc1d1
zrzek	zrzek	k1gMnSc1
s	s	k7c7
komplikovanou	komplikovaný	k2eAgFnSc7d1
minulostí	minulost	k1gFnSc7
a	a	k8xC
odzbrojujícím	odzbrojující	k2eAgInSc7d1
smyslem	smysl	k1gInSc7
pro	pro	k7c4
humor	humor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jamie	Jamie	k1gFnSc1
je	být	k5eAaImIp3nS
inteligentní	inteligentní	k2eAgFnSc4d1
<g/>
,	,	kIx,
na	na	k7c4
standardy	standard	k1gInPc4
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zásadový	zásadový	k2eAgInSc4d1
<g/>
,	,	kIx,
vzdělaný	vzdělaný	k2eAgInSc4d1
a	a	k8xC
světácký	světácký	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velice	velice	k6eAd1
dobře	dobře	k6eAd1
ovládá	ovládat	k5eAaImIp3nS
jazyky	jazyk	k1gInPc4
a	a	k8xC
po	po	k7c6
počátečním	počáteční	k2eAgInSc6d1
konfliktu	konflikt	k1gInSc6
se	se	k3xPyFc4
zamiluje	zamilovat	k5eAaPmIp3nS
do	do	k7c2
záhadné	záhadný	k2eAgFnSc2d1
Claire	Clair	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
vždy	vždy	k6eAd1
nechápe	chápat	k5eNaImIp3nS
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
dělá	dělat	k5eAaImIp3nS
<g/>
,	,	kIx,
bezvýhradně	bezvýhradně	k6eAd1
důvěřuje	důvěřovat	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
to	ten	k3xDgNnSc4
ví	vědět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Frank	Frank	k1gMnSc1
Randall	Randall	k1gMnSc1
</s>
<s>
Claiřin	Claiřin	k2eAgMnSc1d1
manžel	manžel	k1gMnSc1
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
historie	historie	k1gFnSc2
s	s	k7c7
hlubokým	hluboký	k2eAgInSc7d1
zájmem	zájem	k1gInSc7
o	o	k7c4
genealogii	genealogie	k1gFnSc4
a	a	k8xC
dědictví	dědictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
špion	špion	k1gMnSc1
pro	pro	k7c4
MI	já	k3xPp1nSc3
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jonathan	Jonathan	k1gMnSc1
Randall	Randall	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
alias	alias	k9
„	„	k?
<g/>
Black	Black	k1gMnSc1
Jack	Jack	k1gMnSc1
<g/>
“	“	k?
Randall	Randall	k1gMnSc1
<g/>
)	)	kIx)
Hlavní	hlavní	k2eAgMnSc1d1
padouch	padouch	k1gMnSc1
<g/>
,	,	kIx,
předek	předek	k1gMnSc1
Franka	Frank	k1gMnSc2
Randalla	Randallo	k1gNnSc2
<g/>
,	,	kIx,
důstojník	důstojník	k1gMnSc1
britské	britský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Jamieho	Jamie	k1gMnSc2
je	být	k5eAaImIp3nS
přezdívka	přezdívka	k1gFnSc1
„	„	k?
<g/>
Black	Black	k1gInSc1
<g/>
“	“	k?
odkazem	odkaz	k1gInSc7
na	na	k7c4
barvu	barva	k1gFnSc4
jeho	jeho	k3xOp3gFnSc2
duše	duše	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sadistický	sadistický	k2eAgInSc1d1
Jack	Jack	k1gInSc1
vypadá	vypadat	k5eAaImIp3nS,k5eAaPmIp3nS
skoro	skoro	k6eAd1
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
jeho	jeho	k3xOp3gMnSc1
potomek	potomek	k1gMnSc1
a	a	k8xC
je	být	k5eAaImIp3nS
sadisticky	sadisticky	k6eAd1
sexuálně	sexuálně	k6eAd1
posedlý	posedlý	k2eAgInSc4d1
Jamiem	Jamius	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Colum	Colum	k1gInSc1
MacKenzie	MacKenzie	k1gFnSc2
</s>
<s>
Hlava	hlava	k1gFnSc1
klanu	klan	k1gInSc2
MacKenziů	MacKenzi	k1gMnPc2
a	a	k8xC
Jamieho	Jamie	k1gMnSc2
strýc	strýc	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
chrání	chránit	k5eAaImIp3nS
Jamieho	Jamie	k1gMnSc4
a	a	k8xC
Claire	Clair	k1gInSc5
před	před	k7c4
Angličany	Angličan	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starší	starší	k1gMnSc1
bratr	bratr	k1gMnSc1
Dougala	Dougala	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trpí	trpět	k5eAaImIp3nS
Toulouse-Lautrecovým	Toulouse-Lautrecův	k2eAgInSc7d1
syndromem	syndrom	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Dougal	Dougal	k1gInSc1
MacKenzie	MacKenzie	k1gFnSc2
</s>
<s>
Columův	Columův	k2eAgMnSc1d1
mladší	mladý	k2eAgMnSc1d2
jakobitský	jakobitský	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
vede	vést	k5eAaImIp3nS
klan	klan	k1gInSc4
do	do	k7c2
boje	boj	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
fyzicky	fyzicky	k6eAd1
nemohoucí	mohoucí	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naznačuje	naznačovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
biologickým	biologický	k2eAgMnSc7d1
otcem	otec	k1gMnSc7
Columova	Columův	k2eAgMnSc4d1
syna	syn	k1gMnSc4
Hamishe	Hamish	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jamieho	Jamie	k1gMnSc4
přijal	přijmout	k5eAaPmAgMnS
za	za	k7c4
svého	svůj	k3xOyFgMnSc4
nevlastního	vlastní	k2eNgMnSc4d1
syna	syn	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dougal	Dougal	k1gInSc1
má	mít	k5eAaImIp3nS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
ženou	hnát	k5eAaImIp3nP
čtyři	čtyři	k4xCgFnPc4
dcery	dcera	k1gFnPc4
a	a	k8xC
jednoho	jeden	k4xCgMnSc2
syna	syn	k1gMnSc2
s	s	k7c7
Geilis	Geilis	k1gFnSc7
Duncanovou	Duncanová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Geillis	Geillis	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Geilie	Geilie	k1gFnSc1
Duncanová	Duncanová	k1gFnSc1
</s>
<s>
Manželka	manželka	k1gFnSc1
prokurátora	prokurátor	k1gMnSc2
<g/>
,	,	kIx,
obviňovaná	obviňovaný	k2eAgFnSc1d1
z	z	k7c2
čarodějnictví	čarodějnictví	k1gNnSc2
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
znalost	znalost	k1gFnSc4
rostlin	rostlina	k1gFnPc2
a	a	k8xC
bylin	bylina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
uvěznění	uvěznění	k1gNnSc6
a	a	k8xC
obvinění	obviněný	k1gMnPc1
z	z	k7c2
čarodějnictví	čarodějnictví	k1gNnSc2
získá	získat	k5eAaPmIp3nS
krátký	krátký	k2eAgInSc4d1
odklad	odklad	k1gInSc4
trestu	trest	k1gInSc2
smrti	smrt	k1gFnSc2
z	z	k7c2
důvodů	důvod	k1gInPc2
těhotenství	těhotenství	k1gNnSc2
s	s	k7c7
Dougalem	Dougal	k1gMnSc7
MacKenziem	MacKenzius	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabije	zabít	k5eAaPmIp3nS
svého	svůj	k3xOyFgMnSc2
manžela	manžel	k1gMnSc2
Arthura	Arthur	k1gMnSc2
Duncana	Duncan	k1gMnSc2
a	a	k8xC
několikrát	několikrát	k6eAd1
podvede	podvést	k5eAaPmIp3nS
Claire	Clair	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Claire	Clair	k1gInSc5
si	se	k3xPyFc3
nakonec	nakonec	k6eAd1
uvědomí	uvědomit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Geilis	Geilis	k1gFnSc1
přicestovala	přicestovat	k5eAaPmAgFnS
ze	z	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Murtagh	Murtagh	k1gMnSc1
Fitzgibbons	Fitzgibbonsa	k1gFnPc2
Fraser	Fraser	k1gMnSc1
</s>
<s>
Jamieho	Jamieze	k6eAd1
kmotr	kmotr	k1gMnSc1
<g/>
,	,	kIx,
zamlklý	zamlklý	k2eAgMnSc1d1
<g/>
,	,	kIx,
tichý	tichý	k2eAgInSc1d1
a	a	k8xC
statečný	statečný	k2eAgMnSc1d1
<g/>
,	,	kIx,
věrný	věrný	k2eAgInSc1d1
Jamiemu	Jamiemo	k1gNnSc3
<g/>
,	,	kIx,
o	o	k7c4
kterého	který	k3yIgMnSc4,k3yRgMnSc4,k3yQgMnSc4
se	se	k3xPyFc4
stará	starat	k5eAaImIp3nS
jako	jako	k9
o	o	k7c4
vlastního	vlastní	k2eAgMnSc4d1
syna	syn	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
začátku	začátek	k1gInSc2
Claire	Clair	k1gInSc5
nepřijímá	přijímat	k5eNaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
rozmyslí	rozmyslet	k5eAaPmIp3nS
si	se	k3xPyFc3
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
když	když	k8xS
pochopí	pochopit	k5eAaPmIp3nP
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
moc	moc	k6eAd1
ji	on	k3xPp3gFnSc4
Jamie	Jamie	k1gFnSc1
miluje	milovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Laoghaire	Laoghair	k1gMnSc5
MacKenzie	MacKenzie	k1gFnPc1
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1
<g/>
,	,	kIx,
šestnáctiletá	šestnáctiletý	k2eAgFnSc1d1
dívka	dívka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
zamilovaná	zamilovaný	k2eAgFnSc1d1
do	do	k7c2
Jamieho	Jamie	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pošle	pošle	k6eAd1
Claire	Clair	k1gInSc5
za	za	k7c4
Geilis	Geilis	k1gFnSc4
těsně	těsně	k6eAd1
před	před	k7c7
razií	razie	k1gFnSc7
na	na	k7c4
čarodějnice	čarodějnice	k1gFnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Jamieho	Jamie	k1gMnSc4
získala	získat	k5eAaPmAgFnS
pro	pro	k7c4
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
a	a	k8xC
inspirace	inspirace	k1gFnSc1
</s>
<s>
Diana	Diana	k1gFnSc1
Gabaldon	Gabaldona	k1gFnPc2
měla	mít	k5eAaImAgFnS
v	v	k7c6
plánu	plán	k1gInSc6
napsat	napsat	k5eAaPmF,k5eAaBmF
historický	historický	k2eAgInSc4d1
román	román	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
neměla	mít	k5eNaImAgFnS
konkrétní	konkrétní	k2eAgInSc4d1
nápad	nápad	k1gInSc4
<g/>
,	,	kIx,
dokud	dokud	k6eAd1
neshlédla	shlédnout	k5eNaPmAgFnS
The	The	k1gFnSc1
War	War	k1gFnSc2
Games	Gamesa	k1gFnPc2
a	a	k8xC
klasiku	klasika	k1gFnSc4
Doctor	Doctor	k1gInSc4
Who	Who	k1gFnSc2
na	na	k7c4
PBS	PBS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Zaujala	zaujmout	k5eAaPmAgFnS
ji	on	k3xPp3gFnSc4
postava	postava	k1gFnSc1
Jamieho	Jamie	k1gMnSc4
McCrimmona	McCrimmon	k1gMnSc4
<g/>
,	,	kIx,
mladého	mladý	k2eAgMnSc4d1
Skota	Skot	k1gMnSc4
z	z	k7c2
roku	rok	k1gInSc2
1756	#num#	k4
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
ztvárnil	ztvárnit	k5eAaPmAgMnS
Frazer	Frazer	k1gMnSc1
Hines	Hines	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Obrázek	obrázek	k1gInSc1
mladého	mladý	k2eAgMnSc2d1
muže	muž	k1gMnSc2
v	v	k7c6
kiltu	kilt	k1gInSc6
jí	jíst	k5eAaImIp3nS
zůstal	zůstat	k5eAaPmAgMnS
v	v	k7c6
mysli	mysl	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
román	román	k1gInSc4
situovat	situovat	k5eAaBmF
do	do	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jejím	její	k3xOp3gInSc7
původním	původní	k2eAgInSc7d1
záměrem	záměr	k1gInSc7
bylo	být	k5eAaImAgNnS
napsat	napsat	k5eAaPmF,k5eAaBmF
tradiční	tradiční	k2eAgInSc4d1
historický	historický	k2eAgInSc4d1
román	román	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
když	když	k8xS
začala	začít	k5eAaPmAgFnS
vytvářet	vytvářet	k5eAaImF
postavu	postava	k1gFnSc4
Claire	Clair	k1gInSc5
<g/>
,	,	kIx,
řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
její	její	k3xOp3gFnSc1
postava	postava	k1gFnSc1
„	„	k?
<g/>
okamžitě	okamžitě	k6eAd1
převzala	převzít	k5eAaPmAgFnS
příběh	příběh	k1gInSc4
a	a	k8xC
do	do	k7c2
všeho	všecek	k3xTgNnSc2
vnesla	vnést	k5eAaPmAgFnS
chytrácké	chytrácký	k2eAgFnPc4d1
moderní	moderní	k2eAgFnPc4d1
poznámky	poznámka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Gabaldon	Gabaldon	k1gNnSc4
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
vnést	vnést	k5eAaPmF
do	do	k7c2
svého	svůj	k3xOyFgInSc2
příběhu	příběh	k1gInSc2
postavu	postava	k1gFnSc4
moderní	moderní	k2eAgFnSc2d1
ženy	žena	k1gFnSc2
a	a	k8xC
až	až	k9
později	pozdě	k6eAd2
vymyslela	vymyslet	k5eAaPmAgFnS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přijetí	přijetí	k1gNnSc1
a	a	k8xC
ocenění	ocenění	k1gNnSc1
</s>
<s>
Publishers	Publishers	k6eAd1
Weekly	Weekl	k1gInPc1
o	o	k7c6
Cizince	cizinka	k1gFnSc6
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Pohlcující	pohlcující	k2eAgFnSc2d1
a	a	k8xC
hřejivé	hřejivý	k2eAgFnSc2d1
<g/>
,	,	kIx,
tahle	tenhle	k3xDgFnSc1
první	první	k4xOgFnSc1
kniha	kniha	k1gFnSc1
evokuje	evokovat	k5eAaBmIp3nS
zemi	zem	k1gFnSc4
a	a	k8xC
tradice	tradice	k1gFnPc4
Skotska	Skotsko	k1gNnSc2
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc2
postavy	postava	k1gFnSc2
jsou	být	k5eAaImIp3nP
realistické	realistický	k2eAgNnSc1d1
<g/>
,	,	kIx,
divoké	divoký	k2eAgNnSc1d1
a	a	k8xC
sympatické	sympatický	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Román	Román	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
získal	získat	k5eAaPmAgMnS
cenu	cena	k1gFnSc4
Romance	romance	k1gFnSc2
Writers	Writers	k1gInSc1
of	of	k?
America	America	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
RITA	rito	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
2013	#num#	k4
si	se	k3xPyFc3
televizní	televizní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
Starz	Starza	k1gFnPc2
objednala	objednat	k5eAaPmAgFnS
16	#num#	k4
epizod	epizoda	k1gFnPc2
televizní	televizní	k2eAgFnSc2d1
adaptace	adaptace	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
produkce	produkce	k1gFnSc1
začala	začít	k5eAaPmAgFnS
v	v	k7c6
říjnu	říjen	k1gInSc6
2013	#num#	k4
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
USA	USA	kA
měl	mít	k5eAaImAgInS
seriál	seriál	k1gInSc1
premiéru	premiéra	k1gFnSc4
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzápětí	vzápětí	k6eAd1
byla	být	k5eAaImAgFnS
oznámena	oznámit	k5eAaPmNgFnS
také	také	k9
druhá	druhý	k4xOgFnSc1
řada	řada	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnSc1d1
adaptace	adaptace	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
adaptovala	adaptovat	k5eAaBmAgFnS
Gabaldon	Gabaldon	k1gInSc4
první	první	k4xOgFnSc4
třetinu	třetina	k1gFnSc4
Cizinky	cizinka	k1gFnSc2
do	do	k7c2
grafického	grafický	k2eAgInSc2d1
románu	román	k1gInSc2
<g/>
,	,	kIx,
ilustrovaného	ilustrovaný	k2eAgInSc2d1
Hoangem	Hoang	k1gInSc7
Nguyenem	Nguyeno	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
tom	ten	k3xDgInSc6
samém	samý	k3xTgInSc6
roce	rok	k1gInSc6
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
14	#num#	k4
písní	píseň	k1gFnPc2
inspirovaných	inspirovaný	k2eAgFnPc2d1
románem	román	k1gInSc7
s	s	k7c7
názvem	název	k1gInSc7
Cizinka	cizinka	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Muzikál	muzikál	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Outlander	Outlandra	k1gFnPc2
(	(	kIx(
<g/>
novel	novela	k1gFnPc2
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.1	.1	k4
2	#num#	k4
REESE	REESE	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Book	Book	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Lord	lord	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
the	the	k?
Hand	Hand	k1gInSc1
of	of	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
REESE	REESE	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Book	Book	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Lord	lord	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
the	the	k?
Hand	Hand	k1gInSc1
of	of	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
REESE	REESE	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Book	Book	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Lord	lord	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
the	the	k?
Hand	Hand	k1gInSc1
of	of	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
REESE	REESE	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Book	Book	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Lord	lord	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
the	the	k?
Hand	Hand	k1gInSc1
of	of	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
REESE	REESE	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Book	Book	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Lord	lord	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
the	the	k?
Hand	Hand	k1gInSc1
of	of	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
REESE	REESE	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Book	Book	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Lord	lord	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
the	the	k?
Hand	Hand	k1gInSc1
of	of	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
REESE	REESE	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Book	Book	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Lord	lord	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
the	the	k?
Hand	Hand	k1gInSc1
of	of	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
REESE	REESE	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Book	Book	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Lord	lord	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
the	the	k?
Hand	Hand	k1gInSc1
of	of	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
REESE	REESE	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Book	Book	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Lord	lord	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
the	the	k?
Hand	Hand	k1gInSc1
of	of	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
REESE	REESE	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Book	Book	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Lord	lord	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
the	the	k?
Hand	Hand	k1gInSc1
of	of	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
REESE	REESE	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Book	Book	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Lord	lord	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
the	the	k?
Hand	Hand	k1gInSc1
of	of	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
REESE	REESE	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Book	Book	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Lord	lord	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
the	the	k?
Hand	Hand	k1gInSc1
of	of	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
REESE	REESE	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Book	Book	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Lord	lord	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
the	the	k?
Hand	Hand	k1gInSc1
of	of	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
REESE	REESE	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Book	Book	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Lord	lord	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
the	the	k?
Hand	Hand	k1gInSc1
of	of	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
REESE	REESE	kA
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Book	Book	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Lord	lord	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
the	the	k?
Hand	Hand	k1gInSc1
of	of	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Official	Official	k1gMnSc1
website	websit	k1gInSc5
</s>
<s>
"	"	kIx"
<g/>
An	An	k1gFnSc6
Outlander	Outlandra	k1gFnPc2
Family	Famila	k1gFnSc2
Tree	Tre	k1gFnSc2
(	(	kIx(
<g/>
Official	Official	k1gMnSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Random	Random	k1gInSc1
House	house	k1gNnSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Photos	Photos	k1gInSc1
of	of	k?
the	the	k?
first	first	k1gInSc1
edition	edition	k1gInSc1
of	of	k?
Outlander	Outlander	k1gInSc1
</s>
<s>
Todos	Todos	k1gInSc1
los	los	k1gInSc1
libros	librosa	k1gFnPc2
de	de	k?
la	la	k1gNnSc4
serie	serie	k1gFnSc2
Forastera	Foraster	k1gMnSc2
</s>
<s>
Cizinka	cizinka	k1gFnSc1
(	(	kIx(
<g/>
knižní	knižní	k2eAgFnSc1d1
série	série	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
-	-	kIx~
</s>
<s>
1991	#num#	k4
Cizinka	cizinka	k1gFnSc1
(	(	kIx(
<g/>
kniha	kniha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Vážka	vážka	k1gFnSc1
v	v	k7c6
jantaru	jantar	k1gInSc6
</s>
