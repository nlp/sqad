<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Neptunu	Neptun	k1gInSc2	Neptun
má	mít	k5eAaImIp3nS	mít
zelenomodrou	zelenomodrý	k2eAgFnSc4d1	zelenomodrá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
zabírá	zabírat	k5eAaImIp3nS	zabírat
nejspíše	nejspíše	k9	nejspíše
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
planetárního	planetární	k2eAgInSc2d1	planetární
poloměru	poloměr	k1gInSc2	poloměr
<g/>
.	.	kIx.	.
</s>
