<s>
Minerál	minerál	k1gInSc1	minerál
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
<g/>
,	,	kIx,	,
z	z	k7c2	z
minera	minero	k1gNnSc2	minero
<g/>
,	,	kIx,	,
důl	důl	k1gInSc1	důl
<g/>
)	)	kIx)	)
čili	čili	k8xC	čili
nerost	nerost	k1gInSc4	nerost
je	být	k5eAaImIp3nS	být
prvek	prvek	k1gInSc1	prvek
nebo	nebo	k8xC	nebo
chemická	chemický	k2eAgFnSc1d1	chemická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
krystalická	krystalický	k2eAgFnSc1d1	krystalická
a	a	k8xC	a
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
výsledek	výsledek	k1gInSc4	výsledek
geologických	geologický	k2eAgInPc2d1	geologický
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
