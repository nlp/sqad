<s>
Minerál	minerál	k1gInSc1	minerál
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
<g/>
,	,	kIx,	,
z	z	k7c2	z
minera	minero	k1gNnSc2	minero
<g/>
,	,	kIx,	,
důl	důl	k1gInSc1	důl
<g/>
)	)	kIx)	)
čili	čili	k8xC	čili
nerost	nerost	k1gInSc4	nerost
je	být	k5eAaImIp3nS	být
prvek	prvek	k1gInSc1	prvek
nebo	nebo	k8xC	nebo
chemická	chemický	k2eAgFnSc1d1	chemická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
krystalická	krystalický	k2eAgFnSc1d1	krystalická
a	a	k8xC	a
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
výsledek	výsledek	k1gInSc4	výsledek
geologických	geologický	k2eAgInPc2d1	geologický
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
obecnější	obecní	k2eAgFnPc1d2	obecní
definice	definice	k1gFnPc1	definice
ji	on	k3xPp3gFnSc4	on
nedefinují	definovat	k5eNaBmIp3nP	definovat
jako	jako	k8xC	jako
produkt	produkt	k1gInSc4	produkt
geologických	geologický	k2eAgInPc2d1	geologický
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
produkt	produkt	k1gInSc1	produkt
přírodních	přírodní	k2eAgInPc2d1	přírodní
procesů	proces	k1gInPc2	proces
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
nebo	nebo	k8xC	nebo
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
kosmickém	kosmický	k2eAgNnSc6d1	kosmické
tělese	těleso	k1gNnSc6	těleso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
přes	přes	k7c4	přes
5	[number]	k4	5
200	[number]	k4	200
minerálů	minerál	k1gInPc2	minerál
a	a	k8xC	a
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
bývá	bývat	k5eAaImIp3nS	bývat
objeveno	objevit	k5eAaPmNgNnS	objevit
kolem	kolem	k7c2	kolem
50	[number]	k4	50
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Častý	častý	k2eAgInSc1d1	častý
výskyt	výskyt	k1gInSc1	výskyt
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
přiřknout	přiřknout	k5eAaPmF	přiřknout
jen	jen	k9	jen
cca	cca	kA	cca
300	[number]	k4	300
minerálům	minerál	k1gInPc3	minerál
a	a	k8xC	a
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
hornin	hornina	k1gFnPc2	hornina
je	být	k5eAaImIp3nS	být
významných	významný	k2eAgNnPc2d1	významné
jen	jen	k6eAd1	jen
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Některé	některý	k3yIgFnPc4	některý
výjimky	výjimka	k1gFnPc4	výjimka
definici	definice	k1gFnSc3	definice
minerálu	minerál	k1gInSc2	minerál
porušují	porušovat	k5eAaImIp3nP	porušovat
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
za	za	k7c4	za
minerály	minerál	k1gInPc4	minerál
tradičně	tradičně	k6eAd1	tradičně
pokládány	pokládán	k2eAgInPc4d1	pokládán
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
minerály	minerál	k1gInPc4	minerál
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
považují	považovat	k5eAaImIp3nP	považovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
i	i	k8xC	i
<g/>
:	:	kIx,	:
</s>
<s>
Rtuť	rtuť	k1gFnSc1	rtuť
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
kapalná	kapalný	k2eAgFnSc1d1	kapalná
<g/>
)	)	kIx)	)
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
amorfní	amorfní	k2eAgFnPc4d1	amorfní
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
opály	opál	k1gInPc1	opál
<g/>
)	)	kIx)	)
Látky	látka	k1gFnPc1	látka
pocházející	pocházející	k2eAgFnPc1d1	pocházející
z	z	k7c2	z
jiných	jiný	k2eAgNnPc2d1	jiné
kosmických	kosmický	k2eAgNnPc2d1	kosmické
těles	těleso	k1gNnPc2	těleso
(	(	kIx(	(
<g/>
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
,	,	kIx,	,
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
meteority	meteorit	k1gInPc1	meteorit
<g/>
)	)	kIx)	)
Biogenní	biogenní	k2eAgInPc4d1	biogenní
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
formování	formování	k1gNnSc6	formování
podílely	podílet	k5eAaImAgInP	podílet
geologické	geologický	k2eAgInPc1d1	geologický
procesy	proces	k1gInPc1	proces
(	(	kIx(	(
<g/>
např.	např.	kA	např.
minerály	minerál	k1gInPc4	minerál
guana	guana	k1gFnSc1	guana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
za	za	k7c4	za
minerály	minerál	k1gInPc4	minerál
nepovažují	považovat	k5eNaImIp3nP	považovat
<g/>
:	:	kIx,	:
Voda	voda	k1gFnSc1	voda
v	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
stavu	stav	k1gInSc6	stav
(	(	kIx(	(
<g/>
led	led	k1gInSc1	led
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
řazen	řadit	k5eAaImNgInS	řadit
mezi	mezi	k7c4	mezi
minerály	minerál	k1gInPc4	minerál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atmosférické	atmosférický	k2eAgInPc4d1	atmosférický
plyny	plyn	k1gInPc4	plyn
atd.	atd.	kA	atd.
Ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
nekrystalické	krystalický	k2eNgFnPc1d1	nekrystalická
bitumenní	bitumenný	k2eAgMnPc1d1	bitumenný
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
)	)	kIx)	)
Antropogenní	antropogenní	k2eAgFnSc1d1	antropogenní
(	(	kIx(	(
<g/>
člověkem	člověk	k1gMnSc7	člověk
vytvořené	vytvořený	k2eAgInPc4d1	vytvořený
<g/>
)	)	kIx)	)
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
geologickými	geologický	k2eAgInPc7d1	geologický
procesy	proces	k1gInPc7	proces
modifikované	modifikovaný	k2eAgFnSc2d1	modifikovaná
<g />
.	.	kIx.	.
</s>
<s>
antropogenní	antropogenní	k2eAgInPc1d1	antropogenní
materiály	materiál	k1gInPc1	materiál
Látky	látka	k1gFnSc2	látka
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
zásahem	zásah	k1gInSc7	zásah
člověka	člověk	k1gMnSc2	člověk
do	do	k7c2	do
přírody	příroda	k1gFnSc2	příroda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
produkty	produkt	k1gInPc1	produkt
hoření	hoření	k1gNnPc2	hoření
uhelných	uhelný	k2eAgFnPc2d1	uhelná
hald	halda	k1gFnPc2	halda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
za	za	k7c7	za
nerosty	nerost	k1gInPc7	nerost
považujeme	považovat	k5eAaImIp1nP	považovat
látky	látka	k1gFnPc4	látka
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
přeměnou	přeměna	k1gFnSc7	přeměna
materiálu	materiál	k1gInSc2	materiál
hald	halda	k1gFnPc2	halda
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
důlních	důlní	k2eAgNnPc2d1	důlní
děl	dělo	k1gNnPc2	dělo
atd.	atd.	kA	atd.
Biogenní	biogenní	k2eAgInPc4d1	biogenní
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
modifikovány	modifikován	k2eAgInPc1d1	modifikován
geologickými	geologický	k2eAgInPc7d1	geologický
procesy	proces	k1gInPc7	proces
(	(	kIx(	(
<g/>
žlučové	žlučový	k2eAgInPc1d1	žlučový
kameny	kámen	k1gInPc1	kámen
<g/>
,	,	kIx,	,
schránky	schránka	k1gFnPc1	schránka
měkkýšů	měkkýš	k1gMnPc2	měkkýš
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Směsi	směs	k1gFnSc3	směs
minerálů	minerál	k1gInPc2	minerál
Horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
směsí	směs	k1gFnSc7	směs
různých	různý	k2eAgInPc2d1	různý
minerálů	minerál	k1gInPc2	minerál
(	(	kIx(	(
<g/>
např.	např.	kA	např.
žuly	žula	k1gFnSc2	žula
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
křemene	křemen	k1gInSc2	křemen
<g/>
,	,	kIx,	,
živců	živec	k1gInPc2	živec
<g/>
,	,	kIx,	,
slíd	slída	k1gFnPc2	slída
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
minerálů	minerál	k1gInPc2	minerál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
monominerální	monominerální	k2eAgFnPc1d1	monominerální
horniny	hornina	k1gFnPc1	hornina
tvořené	tvořený	k2eAgFnPc1d1	tvořená
jen	jen	k9	jen
jedním	jeden	k4xCgInSc7	jeden
minerálem	minerál	k1gInSc7	minerál
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mramor	mramor	k1gInSc1	mramor
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
zrn	zrno	k1gNnPc2	zrno
kalcitu	kalcit	k1gInSc2	kalcit
<g/>
)	)	kIx)	)
Dle	dle	k7c2	dle
vzniku	vznik	k1gInSc2	vznik
Primární	primární	k2eAgInPc4d1	primární
minerály	minerál	k1gInPc4	minerál
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
jako	jako	k8xC	jako
hornina	hornina	k1gFnSc1	hornina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Sekundární	sekundární	k2eAgInPc1d1	sekundární
minerály	minerál	k1gInPc1	minerál
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
až	až	k6eAd1	až
chemickým	chemický	k2eAgNnSc7d1	chemické
zvětráváním	zvětrávání	k1gNnSc7	zvětrávání
nebo	nebo	k8xC	nebo
metamorfózou	metamorfóza	k1gFnSc7	metamorfóza
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
primárních	primární	k2eAgInPc2d1	primární
minerálů	minerál	k1gInPc2	minerál
Nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dole	dole	k6eAd1	dole
kapitola	kapitola	k1gFnSc1	kapitola
Vznik	vznik	k1gInSc1	vznik
<g/>
)	)	kIx)	)
Endogenní	endogenní	k2eAgInPc1d1	endogenní
minerály	minerál	k1gInPc1	minerál
Exogenní	exogenní	k2eAgInPc1d1	exogenní
minerály	minerál	k1gInPc4	minerál
Podle	podle	k7c2	podle
obsahu	obsah	k1gInSc2	obsah
kovů	kov	k1gInPc2	kov
rudní	rudní	k2eAgInPc1d1	rudní
minerály	minerál	k1gInPc1	minerál
–	–	k?	–
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
využitelné	využitelný	k2eAgInPc1d1	využitelný
kovy	kov	k1gInPc1	kov
(	(	kIx(	(
<g/>
např.	např.	kA	např.
magnetit	magnetit	k1gInSc1	magnetit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
dolech	dol	k1gInPc6	dol
nerudní	rudní	k2eNgInPc1d1	rudní
minerály	minerál	k1gInPc1	minerál
–	–	k?	–
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
kovy	kov	k1gInPc4	kov
nebo	nebo	k8xC	nebo
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jen	jen	k9	jen
nevyužitelné	využitelný	k2eNgInPc1d1	nevyužitelný
kovy	kov	k1gInPc1	kov
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kamenná	kamenný	k2eAgFnSc1d1	kamenná
sůl	sůl	k1gFnSc1	sůl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
<g/>
,	,	kIx,	,
sklářství	sklářství	k1gNnSc6	sklářství
<g/>
,	,	kIx,	,
energetickém	energetický	k2eAgInSc6d1	energetický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
apod.	apod.	kA	apod.
Podle	podle	k7c2	podle
chemické	chemický	k2eAgFnSc2d1	chemická
"	"	kIx"	"
<g/>
složitosti	složitost	k1gFnSc2	složitost
<g/>
"	"	kIx"	"
Prvky	prvek	k1gInPc1	prvek
Sloučeniny	sloučenina	k1gFnSc2	sloučenina
Systematické	systematický	k2eAgNnSc4d1	systematické
třídění	třídění	k1gNnSc4	třídění
minerálů	minerál	k1gInPc2	minerál
lze	lze	k6eAd1	lze
provést	provést	k5eAaPmF	provést
dle	dle	k7c2	dle
různých	různý	k2eAgNnPc2d1	různé
fyzikálně-chemických	fyzikálněhemický	k2eAgNnPc2d1	fyzikálně-chemické
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
kategorizaci	kategorizace	k1gFnSc6	kategorizace
na	na	k7c6	na
wikipedii	wikipedie	k1gFnSc6	wikipedie
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
Strunzova	Strunzův	k2eAgInSc2d1	Strunzův
mineralogického	mineralogický	k2eAgInSc2d1	mineralogický
systému	systém	k1gInSc2	systém
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
Prvky	prvek	k1gInPc1	prvek
(	(	kIx(	(
<g/>
elementy	element	k1gInPc1	element
<g/>
)	)	kIx)	)
Prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
minerály	minerál	k1gInPc4	minerál
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
pouze	pouze	k6eAd1	pouze
jedním	jeden	k4xCgInSc7	jeden
chemickým	chemický	k2eAgInSc7d1	chemický
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
20	[number]	k4	20
minerálů	minerál	k1gInPc2	minerál
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
10	[number]	k4	10
geologicky	geologicky	k6eAd1	geologicky
signifikantních	signifikantní	k2eAgMnPc2d1	signifikantní
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
kovové	kovový	k2eAgFnPc4d1	kovová
a	a	k8xC	a
nekovové	kovový	k2eNgNnSc1d1	nekovové
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
i	i	k9	i
polokovové	polokovový	k2eAgFnSc3d1	polokovový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1	kovové
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c6	na
křehké	křehký	k2eAgFnSc6d1	křehká
a	a	k8xC	a
tvárné	tvárný	k2eAgFnSc6d1	tvárná
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
třídy	třída	k1gFnSc2	třída
se	se	k3xPyFc4	se
mineralogicky	mineralogicky	k6eAd1	mineralogicky
zařazují	zařazovat	k5eAaImIp3nP	zařazovat
i	i	k9	i
minerály	minerál	k1gInPc4	minerál
tvořené	tvořený	k2eAgInPc4d1	tvořený
některými	některý	k3yIgFnPc7	některý
sloučeninami	sloučenina	k1gFnPc7	sloučenina
<g/>
:	:	kIx,	:
do	do	k7c2	do
nekovových	kovový	k2eNgInPc2d1	nekovový
karbidy	karbid	k1gInPc7	karbid
<g/>
,	,	kIx,	,
nitridy	nitrid	k1gInPc7	nitrid
<g/>
,	,	kIx,	,
fosfidy	fosfid	k1gInPc7	fosfid
a	a	k8xC	a
silicidy	silicid	k1gInPc7	silicid
<g/>
,	,	kIx,	,
do	do	k7c2	do
kovových	kovový	k2eAgInPc2d1	kovový
vícekovové	vícekovový	k2eAgFnSc2d1	vícekovový
přírodní	přírodní	k2eAgFnPc4d1	přírodní
slitiny	slitina	k1gFnSc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
měď	měď	k1gFnSc1	měď
(	(	kIx(	(
<g/>
Cu	Cu	k1gFnSc1	Cu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
(	(	kIx(	(
<g/>
Ag	Ag	k1gFnSc1	Ag
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
(	(	kIx(	(
<g/>
Au	au	k0	au
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
(	(	kIx(	(
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
grafit	grafit	k1gInSc1	grafit
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diamant	diamant	k1gInSc1	diamant
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
Sulfidy	sulfid	k1gInPc1	sulfid
(	(	kIx(	(
<g/>
sirníky	sirník	k1gInPc1	sirník
<g/>
)	)	kIx)	)
Sulfidy	sulfid	k1gInPc1	sulfid
sestávají	sestávat	k5eAaImIp3nP	sestávat
ze	z	k7c2	z
sloučeniny	sloučenina	k1gFnSc2	sloučenina
síry	síra	k1gFnSc2	síra
(	(	kIx(	(
<g/>
aniont	aniont	k1gInSc1	aniont
S	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
s	s	k7c7	s
kovy	kov	k1gInPc7	kov
nebo	nebo	k8xC	nebo
metaloidy	metaloid	k1gInPc7	metaloid
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
sulfidům	sulfid	k1gInPc3	sulfid
patří	patřit	k5eAaImIp3nP	patřit
asi	asi	k9	asi
600	[number]	k4	600
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mineralogického	mineralogický	k2eAgNnSc2d1	mineralogické
hlediska	hledisko	k1gNnSc2	hledisko
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
selenidy	selenid	k1gInPc1	selenid
(	(	kIx(	(
<g/>
obsahující	obsahující	k2eAgInSc1d1	obsahující
selen	selen	k1gInSc1	selen
<g/>
;	;	kIx,	;
Se	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teluridy	telurid	k1gInPc1	telurid
(	(	kIx(	(
<g/>
obsahující	obsahující	k2eAgInSc1d1	obsahující
tellur	tellur	k1gInSc1	tellur
<g/>
;	;	kIx,	;
Te	Te	k1gFnSc1	Te
<g/>
2	[number]	k4	2
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
antimonidy	antimonid	k1gInPc1	antimonid
(	(	kIx(	(
<g/>
obsahující	obsahující	k2eAgInSc1d1	obsahující
antimon	antimon	k1gInSc1	antimon
<g/>
;	;	kIx,	;
Sb	sb	kA	sb
<g/>
3	[number]	k4	3
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
a	a	k8xC	a
bizmutidy	bizmutid	k1gInPc1	bizmutid
(	(	kIx(	(
<g/>
obsahující	obsahující	k2eAgInSc1d1	obsahující
bismut	bismut	k1gInSc1	bismut
<g/>
;	;	kIx,	;
Bi	Bi	k1gFnSc1	Bi
<g/>
3	[number]	k4	3
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
galenit	galenit	k1gInSc1	galenit
(	(	kIx(	(
<g/>
PbS	PbS	k1gFnSc1	PbS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pyrit	pyrit	k1gInSc1	pyrit
(	(	kIx(	(
<g/>
FeS	fes	k1gNnSc1	fes
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sfalerit	sfalerit	k1gInSc1	sfalerit
(	(	kIx(	(
<g/>
ZnS	ZnS	k1gFnSc1	ZnS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rumělka	rumělka	k1gFnSc1	rumělka
(	(	kIx(	(
<g/>
HgS	HgS	k1gFnSc1	HgS
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
Halogenidy	Halogenid	k1gInPc1	Halogenid
(	(	kIx(	(
<g/>
halovce	halovec	k1gInPc1	halovec
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
asi	asi	k9	asi
<g />
.	.	kIx.	.
</s>
<s>
140	[number]	k4	140
a	a	k8xC	a
sestávají	sestávat	k5eAaImIp3nP	sestávat
ze	z	k7c2	z
sloučenin	sloučenina	k1gFnPc2	sloučenina
fluoru	fluor	k1gInSc2	fluor
(	(	kIx(	(
<g/>
aniont	aniont	k1gInSc1	aniont
F	F	kA	F
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chloru	chlor	k1gInSc2	chlor
(	(	kIx(	(
<g/>
aniont	aniont	k1gInSc1	aniont
Cl	Cl	k1gFnSc2	Cl
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brómu	bróm	k1gInSc2	bróm
(	(	kIx(	(
<g/>
aniont	aniont	k1gInSc1	aniont
Br	br	k0	br
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jódu	jód	k1gInSc2	jód
(	(	kIx(	(
<g/>
aniont	aniont	k1gInSc1	aniont
I	i	k8xC	i
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
s	s	k7c7	s
kationty	kation	k1gInPc7	kation
kovů	kov	k1gInPc2	kov
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
sodík	sodík	k1gInSc1	sodík
nebo	nebo	k8xC	nebo
vápník	vápník	k1gInSc1	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Mineralogicky	mineralogicky	k6eAd1	mineralogicky
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
oxihalohenidy	oxihalohenida	k1gFnPc1	oxihalohenida
(	(	kIx(	(
<g/>
obsahující	obsahující	k2eAgInSc4d1	obsahující
i	i	k9	i
aniont	aniont	k1gInSc4	aniont
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
fluorit	fluorit	k1gInSc1	fluorit
(	(	kIx(	(
<g/>
CaF	CaF	k1gFnSc1	CaF
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kamenná	kamenný	k2eAgFnSc1d1	kamenná
sůl	sůl	k1gFnSc1	sůl
(	(	kIx(	(
<g/>
NaCl	NaCl	k1gInSc1	NaCl
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
Oxidy	oxid	k1gInPc1	oxid
a	a	k8xC	a
hydroxidy	hydroxid	k1gInPc1	hydroxid
Sestávají	sestávat	k5eAaImIp3nP	sestávat
ze	z	k7c2	z
sloučeniny	sloučenina	k1gFnSc2	sloučenina
kovů	kov	k1gInPc2	kov
nebo	nebo	k8xC	nebo
nekovů	nekov	k1gInPc2	nekov
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
(	(	kIx(	(
<g/>
aniont	aniont	k1gInSc1	aniont
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
hydroxylovými	hydroxylový	k2eAgFnPc7d1	hydroxylová
skupinami	skupina	k1gFnPc7	skupina
(	(	kIx(	(
<g/>
skupiny	skupina	k1gFnSc2	skupina
OH	OH	kA	OH
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
asi	asi	k9	asi
400	[number]	k4	400
oxidů	oxid	k1gInPc2	oxid
resp.	resp.	kA	resp.
hydroxidů	hydroxid	k1gInPc2	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
Mineralogicky	mineralogicky	k6eAd1	mineralogicky
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
řadí	řadit	k5eAaImIp3nP	řadit
i	i	k9	i
jodáty	jodáta	k1gFnPc1	jodáta
(	(	kIx(	(
<g/>
chem.	chem.	k?	chem.
jodičnany	jodičnana	k1gFnSc2	jodičnana
<g/>
;	;	kIx,	;
[	[	kIx(	[
<g/>
IO	IO	kA	IO
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vanadáty	vanadát	k1gInPc1	vanadát
(	(	kIx(	(
<g/>
chem.	chem.	k?	chem.
vanadičnany	vanadičnana	k1gFnSc2	vanadičnana
a	a	k8xC	a
vanadany	vanadana	k1gFnSc2	vanadana
<g/>
;	;	kIx,	;
i	i	k9	i
se	s	k7c7	s
složitější	složitý	k2eAgFnSc7d2	složitější
strukturou	struktura	k1gFnSc7	struktura
aniontového	aniontový	k2eAgInSc2d1	aniontový
komplexu	komplex	k1gInSc2	komplex
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arsenity	arsenity	k?	arsenity
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
antimonity	antimonit	k1gInPc1	antimonit
a	a	k8xC	a
bizmutity	bizmutit	k1gInPc1	bizmutit
(	(	kIx(	(
<g/>
chem.	chem.	k?	chem.
arsenitany	arsenitan	k1gInPc4	arsenitan
<g/>
;	;	kIx,	;
[	[	kIx(	[
<g/>
AsO	AsO	k1gFnSc1	AsO
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
−	−	k?	−
resp.	resp.	kA	resp.
antimonitany	antimonitan	k1gInPc4	antimonitan
a	a	k8xC	a
bizmutitany	bizmutitan	k1gInPc4	bizmutitan
<g/>
)	)	kIx)	)
a	a	k8xC	a
sulfity	sulfit	k1gInPc1	sulfit
<g/>
,	,	kIx,	,
selenity	selenit	k1gInPc1	selenit
a	a	k8xC	a
telurity	telurit	k1gInPc1	telurit
(	(	kIx(	(
<g/>
chem.	chem.	k?	chem.
siřičitany	siřičitan	k1gInPc4	siřičitan
<g/>
;	;	kIx,	;
[	[	kIx(	[
<g/>
SO	So	kA	So
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
−	−	k?	−
<g />
.	.	kIx.	.
</s>
<s>
resp.	resp.	kA	resp.
seleničitany	seleničitan	k1gInPc4	seleničitan
a	a	k8xC	a
teluričitany	teluričitan	k1gInPc4	teluričitan
<g/>
)	)	kIx)	)
Příklady	příklad	k1gInPc4	příklad
<g/>
:	:	kIx,	:
spinel	spinel	k1gInSc4	spinel
(	(	kIx(	(
<g/>
MgAl	MgAl	k1gInSc1	MgAl
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hematit	hematit	k1gInSc1	hematit
(	(	kIx(	(
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
magnetit	magnetit	k1gInSc1	magnetit
(	(	kIx(	(
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
křemen	křemen	k1gInSc1	křemen
(	(	kIx(	(
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
korund	korund	k1gInSc4	korund
(	(	kIx(	(
<g/>
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
smolinec	smolinec	k1gInSc1	smolinec
(	(	kIx(	(
<g/>
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
goethit	goethit	k1gInSc1	goethit
(	(	kIx(	(
<g/>
FeO	FeO	k1gMnSc1	FeO
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
))	))	k?	))
5	[number]	k4	5
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
Karbonáty	karbonát	k1gInPc1	karbonát
(	(	kIx(	(
<g/>
uhličitany	uhličitan	k1gInPc1	uhličitan
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
přes	přes	k7c4	přes
200	[number]	k4	200
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
kyslíkaté	kyslíkatý	k2eAgFnPc1d1	kyslíkatá
soli	sůl	k1gFnPc1	sůl
s	s	k7c7	s
aniontovým	aniontův	k2eAgInSc7d1	aniontův
komplexem	komplex	k1gInSc7	komplex
[	[	kIx(	[
<g/>
CO	co	k6eAd1	co
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
−	−	k?	−
<g/>
.	.	kIx.	.
</s>
<s>
Mineralogicky	mineralogicky	k6eAd1	mineralogicky
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
řadí	řadit	k5eAaImIp3nS	řadit
i	i	k8xC	i
nitráty	nitrát	k1gInPc4	nitrát
(	(	kIx(	(
<g/>
dusičnany	dusičnan	k1gInPc4	dusičnan
<g/>
;	;	kIx,	;
[	[	kIx(	[
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
a	a	k8xC	a
boráty	borát	k1gInPc1	borát
(	(	kIx(	(
<g/>
boritany	boritan	k1gInPc1	boritan
<g/>
;	;	kIx,	;
[	[	kIx(	[
<g/>
BO	BO	k?	BO
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Boritany	boritan	k1gInPc1	boritan
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyčleňují	vyčleňovat	k5eAaImIp3nP	vyčleňovat
do	do	k7c2	do
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
analogicky	analogicky	k6eAd1	analogicky
křemičitanům	křemičitan	k1gInPc3	křemičitan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tvoří	tvořit	k5eAaImIp3nP	tvořit
z	z	k7c2	z
atomů	atom	k1gInPc2	atom
boru	bor	k1gInSc2	bor
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
i	i	k8xC	i
složitější	složitý	k2eAgFnSc2d2	složitější
skupinové	skupinový	k2eAgFnSc2d1	skupinová
struktury	struktura	k1gFnSc2	struktura
plošné	plošný	k2eAgNnSc1d1	plošné
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
4	[number]	k4	4
<g/>
−	−	k?	−
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
B	B	kA	B
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
či	či	k8xC	či
řetězcové	řetězcový	k2eAgNnSc1d1	řetězcové
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
−	−	k?	−
až	až	k9	až
[	[	kIx(	[
<g/>
B	B	kA	B
<g/>
6	[number]	k4	6
<g/>
O	o	k7c4	o
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
dolomit	dolomit	k1gInSc1	dolomit
(	(	kIx(	(
<g/>
CaMg	CaMg	k1gInSc1	CaMg
(	(	kIx(	(
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kalcit	kalcit	k1gInSc1	kalcit
(	(	kIx(	(
<g/>
CaCO	CaCO	k1gFnSc1	CaCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malachit	malachit	k1gInSc1	malachit
(	(	kIx(	(
<g/>
Cu	Cu	k1gFnSc1	Cu
<g/>
2	[number]	k4	2
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
Sulfáty	sulfát	k1gInPc4	sulfát
(	(	kIx(	(
<g/>
sírany	síran	k1gInPc4	síran
<g/>
)	)	kIx)	)
Sulfátů	sulfát	k1gInPc2	sulfát
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
300	[number]	k4	300
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
kyslíkaté	kyslíkatý	k2eAgFnPc1d1	kyslíkatá
soli	sůl	k1gFnPc1	sůl
s	s	k7c7	s
aniontovým	aniontův	k2eAgInSc7d1	aniontův
komplexem	komplex	k1gInSc7	komplex
[	[	kIx(	[
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
−	−	k?	−
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
anhydrit	anhydrit	k1gInSc1	anhydrit
(	(	kIx(	(
<g/>
CaSO	CaSO	k1gFnSc1	CaSO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sádrovec	sádrovec	k1gInSc1	sádrovec
(	(	kIx(	(
<g/>
CaSO	CaSO	k1gFnSc1	CaSO
<g/>
4	[number]	k4	4
·	·	k?	·
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
Zařazují	zařazovat	k5eAaImIp3nP	zařazovat
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
i	i	k9	i
chromáty	chromát	k1gInPc1	chromát
(	(	kIx(	(
<g/>
chromany	chroman	k1gInPc1	chroman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
molybdáty	molybdát	k1gInPc1	molybdát
(	(	kIx(	(
<g/>
molybdenany	molybdenan	k1gInPc1	molybdenan
<g/>
)	)	kIx)	)
a	a	k8xC	a
wolframáty	wolframáta	k1gFnSc2	wolframáta
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
wolframany	wolframan	k1gInPc1	wolframan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
sloučeniny	sloučenina	k1gFnSc2	sloučenina
kovů	kov	k1gInPc2	kov
s	s	k7c7	s
aniontovým	aniontův	k2eAgInSc7d1	aniontův
komplexem	komplex	k1gInSc7	komplex
[	[	kIx(	[
<g/>
CrO	CrO	k1gFnSc1	CrO
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
−	−	k?	−
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
MoO	MoO	k1gFnSc1	MoO
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
−	−	k?	−
resp.	resp.	kA	resp.
[	[	kIx(	[
<g/>
WO	WO	kA	WO
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
−	−	k?	−
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
wulfenit	wulfenit	k1gInSc1	wulfenit
(	(	kIx(	(
<g/>
PbMoO	PbMoO	k1gFnSc1	PbMoO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
wolframit	wolframit	k1gInSc1	wolframit
((	((	k?	((
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
,	,	kIx,	,
Mn	Mn	k1gMnSc1	Mn
<g/>
)	)	kIx)	)
WO	WO	kA	WO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
Fosfáty	fosfát	k1gInPc1	fosfát
(	(	kIx(	(
<g/>
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
<g/>
)	)	kIx)	)
Fosfáty	fosfát	k1gInPc1	fosfát
jsou	být	k5eAaImIp3nP	být
kyslíkové	kyslíkový	k2eAgFnSc2d1	kyslíková
soli	sůl	k1gFnSc2	sůl
aniontovým	aniontův	k2eAgInSc7d1	aniontův
komplexem	komplex	k1gInSc7	komplex
[	[	kIx(	[
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
−	−	k?	−
<g/>
.	.	kIx.	.
</s>
<s>
Mineralogicky	mineralogicky	k6eAd1	mineralogicky
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
zařazují	zařazovat	k5eAaImIp3nP	zařazovat
i	i	k9	i
arzenáty	arzenát	k1gInPc1	arzenát
(	(	kIx(	(
<g/>
arzeničnany	arzeničnana	k1gFnPc1	arzeničnana
<g/>
;	;	kIx,	;
[	[	kIx(	[
<g/>
AsO	AsO	k1gFnSc1	AsO
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
a	a	k8xC	a
vanadáty	vanadát	k1gInPc1	vanadát
(	(	kIx(	(
<g/>
vanadičnany	vanadičnana	k1gFnPc1	vanadičnana
<g/>
;	;	kIx,	;
[	[	kIx(	[
<g/>
VO	VO	k?	VO
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nS	patřit
asi	asi	k9	asi
400	[number]	k4	400
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
apatit	apatit	k1gInSc1	apatit
(	(	kIx(	(
<g/>
Ca	ca	kA	ca
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
Cl	Cl	k1gFnSc1	Cl
<g/>
,	,	kIx,	,
OH	OH	kA	OH
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
tyrkys	tyrkys	k1gInSc1	tyrkys
(	(	kIx(	(
<g/>
CuAl	CuAl	k1gInSc1	CuAl
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
8	[number]	k4	8
·	·	k?	·
5	[number]	k4	5
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
karnotit	karnotit	k1gInSc1	karnotit
(	(	kIx(	(
<g/>
K	K	kA	K
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
VO	VO	k?	VO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
·	·	k?	·
3	[number]	k4	3
<g/>
H	H	kA	H
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
Silikáty	silikát	k1gInPc1	silikát
(	(	kIx(	(
<g/>
křemičitany	křemičitan	k1gInPc1	křemičitan
<g/>
)	)	kIx)	)
Křemičitany	křemičitan	k1gInPc1	křemičitan
představují	představovat	k5eAaImIp3nP	představovat
asi	asi	k9	asi
500	[number]	k4	500
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
čtyřstěn	čtyřstěn	k1gInSc1	čtyřstěn
[	[	kIx(	[
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
4	[number]	k4	4
<g/>
−	−	k?	−
tvoří	tvořit	k5eAaImIp3nS	tvořit
základní	základní	k2eAgInSc1d1	základní
stavební	stavební	k2eAgInSc1d1	stavební
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
v	v	k7c6	v
izolovaných	izolovaný	k2eAgInPc6d1	izolovaný
ostrůvcích	ostrůvek	k1gInPc6	ostrůvek
po	po	k7c6	po
jednom	jeden	k4xCgNnSc6	jeden
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
nesosilikáty	nesosilikát	k1gInPc4	nesosilikát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
dvojicích	dvojice	k1gFnPc6	dvojice
(	(	kIx(	(
<g/>
Si	se	k3xPyFc3	se
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
76	[number]	k4	76
<g/>
−	−	k?	−
<g/>
;	;	kIx,	;
sorosilikáty	sorosilikát	k1gInPc4	sorosilikát
<g/>
)	)	kIx)	)
i	i	k9	i
ve	v	k7c6	v
složitějších	složitý	k2eAgFnPc6d2	složitější
strukturách	struktura	k1gFnPc6	struktura
uzavřených	uzavřený	k2eAgInPc2d1	uzavřený
kruhů	kruh	k1gInPc2	kruh
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
SinO	sino	k1gNnSc1	sino
<g/>
3	[number]	k4	3
<g/>
n	n	k0	n
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
−	−	k?	−
-	-	kIx~	-
cyklosilikáty	cyklosilikát	k1gInPc4	cyklosilikát
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
či	či	k8xC	či
dvojitých	dvojitý	k2eAgInPc2d1	dvojitý
lineárních	lineární	k2eAgInPc2d1	lineární
řetězců	řetězec	k1gInPc2	řetězec
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
SinO	sino	k1gNnSc1	sino
<g/>
3	[number]	k4	3
<g/>
n	n	k0	n
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
−	−	k?	−
resp.	resp.	kA	resp.
[	[	kIx(	[
<g/>
Si	se	k3xPyFc3	se
<g/>
4	[number]	k4	4
<g/>
nO	no	k9	no
<g/>
11	[number]	k4	11
<g/>
n	n	k0	n
<g/>
]	]	kIx)	]
<g/>
6	[number]	k4	6
<g/>
n	n	k0	n
<g/>
−	−	k?	−
-	-	kIx~	-
inosilikáty	inosilikát	k1gInPc4	inosilikát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plošných	plošný	k2eAgFnPc2d1	plošná
vrstev	vrstva	k1gFnPc2	vrstva
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
Si	se	k3xPyFc3	se
<g/>
2	[number]	k4	2
<g/>
nO	no	k9	no
<g/>
5	[number]	k4	5
<g/>
n	n	k0	n
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
−	−	k?	−
-	-	kIx~	-
fylosilikáty	fylosilikát	k1gInPc4	fylosilikát
<g/>
)	)	kIx)	)
či	či	k8xC	či
prostorového	prostorový	k2eAgInSc2d1	prostorový
skeletu	skelet	k1gInSc2	skelet
(	(	kIx(	(
<g/>
tektosilikáty	tektosilikát	k1gInPc4	tektosilikát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
olivín	olivín	k1gInSc1	olivín
((	((	k?	((
<g/>
Mg	mg	kA	mg
<g/>
,	,	kIx,	,
Fe	Fe	k1gFnSc1	Fe
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zirkon	zirkon	k1gInSc1	zirkon
(	(	kIx(	(
<g/>
ZrSiO	ZrSiO	k1gFnSc1	ZrSiO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
andaluzit	andaluzit	k1gInSc4	andaluzit
(	(	kIx(	(
<g/>
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
SiO	SiO	k1gFnPc2	SiO
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
topas	topas	k1gInSc4	topas
(	(	kIx(	(
<g/>
Al	ala	k1gFnPc2	ala
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2	[number]	k4	2
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
4	[number]	k4	4
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
beryl	beryl	k1gInSc1	beryl
(	(	kIx(	(
<g/>
Be	Be	k1gFnSc1	Be
<g/>
3	[number]	k4	3
<g/>
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
Si	se	k3xPyFc3	se
<g/>
6	[number]	k4	6
<g/>
O	o	k7c4	o
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
Organolity	Organolit	k1gInPc1	Organolit
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
organické	organický	k2eAgInPc4d1	organický
minerály	minerál	k1gInPc4	minerál
<g/>
)	)	kIx)	)
Mezi	mezi	k7c4	mezi
organolity	organolit	k1gInPc4	organolit
patří	patřit	k5eAaImIp3nP	patřit
minerály	minerál	k1gInPc4	minerál
tvořené	tvořený	k2eAgInPc4d1	tvořený
organickými	organický	k2eAgFnPc7d1	organická
sloučeninami	sloučenina	k1gFnPc7	sloučenina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
solemi	sůl	k1gFnPc7	sůl
karboxylových	karboxylový	k2eAgFnPc2d1	karboxylová
kyselin	kyselina	k1gFnPc2	kyselina
(	(	kIx(	(
<g/>
mravenčí	mravenčí	k2eAgInPc4d1	mravenčí
<g/>
,	,	kIx,	,
octové	octový	k2eAgInPc4d1	octový
<g/>
,	,	kIx,	,
citronové	citronový	k2eAgInPc4d1	citronový
<g/>
,	,	kIx,	,
šťavelové	šťavelový	k2eAgInPc4d1	šťavelový
a	a	k8xC	a
mellitové	mellitový	k2eAgInPc4d1	mellitový
-	-	kIx~	-
formáty	formát	k1gInPc4	formát
<g/>
,	,	kIx,	,
acetáty	acetát	k1gInPc4	acetát
<g/>
,	,	kIx,	,
citráty	citrát	k1gInPc4	citrát
<g/>
,	,	kIx,	,
oxaláty	oxalát	k1gInPc4	oxalát
a	a	k8xC	a
mellitáty	mellitát	k1gInPc4	mellitát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kyseliny	kyselina	k1gFnSc2	kyselina
(	(	kIx(	(
<g/>
iso	iso	k?	iso
<g/>
)	)	kIx)	)
<g/>
kyanaté	kyanatý	k2eAgNnSc1d1	kyanatý
(	(	kIx(	(
<g/>
s	s	k7c7	s
aniontem	anion	k1gInSc7	anion
OCN	OCN	kA	OCN
<g/>
−	−	k?	−
-	-	kIx~	-
kyanáty	kyanát	k1gInPc4	kyanát
<g/>
)	)	kIx)	)
a	a	k8xC	a
thiokyanaté	thiokyanatý	k2eAgInPc1d1	thiokyanatý
(	(	kIx(	(
<g/>
thiokyanatany	thiokyanatan	k1gInPc1	thiokyanatan
s	s	k7c7	s
aniontem	anion	k1gInSc7	anion
SCN	SCN	kA	SCN
<g/>
−	−	k?	−
-	-	kIx~	-
thiokyanáty	thiokyanát	k1gInPc4	thiokyanát
<g/>
)	)	kIx)	)
a	a	k8xC	a
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
mellit	mellit	k5eAaImF	mellit
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Systematická	systematický	k2eAgFnSc1d1	systematická
mineralogie	mineralogie	k1gFnSc1	mineralogie
<g/>
.	.	kIx.	.
</s>
<s>
Minerály	minerál	k1gInPc1	minerál
mají	mít	k5eAaImIp3nP	mít
množství	množství	k1gNnSc4	množství
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
a	a	k8xC	a
chemických	chemický	k2eAgFnPc2d1	chemická
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
identifikovat	identifikovat	k5eAaBmF	identifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
vlastností	vlastnost	k1gFnPc2	vlastnost
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
lesk	lesk	k1gInSc1	lesk
a	a	k8xC	a
vzhled	vzhled	k1gInSc1	vzhled
(	(	kIx(	(
<g/>
habitus	habitus	k1gInSc1	habitus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
tvrdost	tvrdost	k1gFnSc1	tvrdost
<g/>
,	,	kIx,	,
specifická	specifický	k2eAgFnSc1d1	specifická
hmotnost	hmotnost	k1gFnSc1	hmotnost
–	–	k?	–
hustota	hustota	k1gFnSc1	hustota
a	a	k8xC	a
barva	barva	k1gFnSc1	barva
vrypu	vryp	k1gInSc2	vryp
<g/>
.	.	kIx.	.
</s>
<s>
Štěpnost	štěpnost	k1gFnSc1	štěpnost
a	a	k8xC	a
lom	lom	k1gInSc1	lom
bývají	bývat	k5eAaImIp3nP	bývat
převážně	převážně	k6eAd1	převážně
dobře	dobře	k6eAd1	dobře
viditelné	viditelný	k2eAgFnPc1d1	viditelná
zejména	zejména	k9	zejména
na	na	k7c6	na
čerstvém	čerstvý	k2eAgInSc6d1	čerstvý
úlomku	úlomek	k1gInSc6	úlomek
minerálu	minerál	k1gInSc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
zjišťované	zjišťovaný	k2eAgFnPc1d1	zjišťovaná
fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
minerálů	minerál	k1gInPc2	minerál
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Vzhled	vzhled	k1gInSc1	vzhled
krystalu	krystal	k1gInSc2	krystal
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
krystalografie	krystalografie	k1gFnSc1	krystalografie
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
habitus	habitus	k1gInSc1	habitus
minerálu	minerál	k1gInSc2	minerál
krystalový	krystalový	k2eAgInSc1d1	krystalový
tvar	tvar	k1gInSc1	tvar
krystalová	krystalový	k2eAgFnSc1d1	krystalová
soustava	soustava	k1gFnSc1	soustava
Mechanické	mechanický	k2eAgFnSc2d1	mechanická
vlastnosti	vlastnost	k1gFnSc2	vlastnost
hustota	hustota	k1gFnSc1	hustota
minerálu	minerál	k1gInSc2	minerál
tvrdost	tvrdost	k1gFnSc4	tvrdost
minerálu	minerál	k1gInSc2	minerál
Pevnost	pevnost	k1gFnSc4	pevnost
minerálu	minerál	k1gInSc2	minerál
štěpnost	štěpnost	k1gFnSc4	štěpnost
minerálu	minerál	k1gInSc2	minerál
Dělitelnost	dělitelnost	k1gFnSc4	dělitelnost
minerálu	minerál	k1gInSc2	minerál
lom	lom	k1gInSc1	lom
minerálu	minerál	k1gInSc2	minerál
soudržnost	soudržnost	k1gFnSc4	soudržnost
minerálu	minerál	k1gInSc2	minerál
(	(	kIx(	(
<g/>
křehké	křehký	k2eAgFnPc1d1	křehká
<g/>
,	,	kIx,	,
jemné	jemný	k2eAgFnPc1d1	jemná
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
tvárné	tvárný	k2eAgInPc4d1	tvárný
minerály	minerál	k1gInPc4	minerál
<g/>
)	)	kIx)	)
Pružnost	pružnost	k1gFnSc1	pružnost
minerálu	minerál	k1gInSc2	minerál
Optické	optický	k2eAgFnSc2d1	optická
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
:	:	kIx,	:
barva	barva	k1gFnSc1	barva
minerálu	minerál	k1gInSc2	minerál
barva	barva	k1gFnSc1	barva
a	a	k8xC	a
lesk	lesk	k1gInSc4	lesk
vrypu	vryp	k1gInSc2	vryp
lesk	lesk	k1gInSc1	lesk
minerálu	minerál	k1gInSc2	minerál
propustnost	propustnost	k1gFnSc4	propustnost
světla	světlo	k1gNnSc2	světlo
Lom	lom	k1gInSc4	lom
světla	světlo	k1gNnSc2	světlo
Reflexe	reflexe	k1gFnSc2	reflexe
Dvojlom	dvojlom	k1gInSc4	dvojlom
Optická	optický	k2eAgFnSc1d1	optická
jedno-	jedno-	k?	jedno-
a	a	k8xC	a
dvojosovost	dvojosovost	k1gFnSc1	dvojosovost
pleochroizmus	pleochroizmus	k1gInSc4	pleochroizmus
Luminiscence	luminiscence	k1gFnSc2	luminiscence
Tepelné	tepelný	k2eAgFnSc2d1	tepelná
vlastnosti	vlastnost	k1gFnSc2	vlastnost
tavitelnost	tavitelnost	k1gFnSc1	tavitelnost
žáruvzdornost	žáruvzdornost	k1gFnSc1	žáruvzdornost
Fyziologické	fyziologický	k2eAgFnSc2d1	fyziologická
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
:	:	kIx,	:
Chuť	chuť	k1gFnSc1	chuť
Vůně	vůně	k1gFnSc1	vůně
Omak	omak	k1gInSc1	omak
Magnetické	magnetický	k2eAgFnSc2d1	magnetická
vlastnosti	vlastnost	k1gFnSc2	vlastnost
Elektrické	elektrický	k2eAgFnSc2d1	elektrická
vlastnosti	vlastnost	k1gFnSc2	vlastnost
Radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
Chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
dělení	dělení	k1gNnSc2	dělení
minerálů	minerál	k1gInPc2	minerál
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
nahoře	nahoře	k6eAd1	nahoře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
závisí	záviset	k5eAaImIp3nP	záviset
na	na	k7c6	na
chemickém	chemický	k2eAgNnSc6d1	chemické
složení	složení	k1gNnSc6	složení
a	a	k8xC	a
krystalové	krystalový	k2eAgFnSc6d1	krystalová
struktuře	struktura	k1gFnSc6	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
chemickou	chemický	k2eAgFnSc7d1	chemická
vlastností	vlastnost	k1gFnSc7	vlastnost
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
samotné	samotný	k2eAgNnSc1d1	samotné
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
vyjádřitelné	vyjádřitelný	k2eAgNnSc1d1	vyjádřitelné
formou	forma	k1gFnSc7	forma
vzorce	vzorec	k1gInPc1	vzorec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zjišťování	zjišťování	k1gNnSc6	zjišťování
chemických	chemický	k2eAgFnPc2d1	chemická
vlastností	vlastnost	k1gFnPc2	vlastnost
minerálů	minerál	k1gInPc2	minerál
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
minerály	minerál	k1gInPc7	minerál
rozpouštějí	rozpouštět	k5eAaImIp3nP	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
halit	halit	k1gInSc1	halit
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
reagují	reagovat	k5eAaBmIp3nP	reagovat
s	s	k7c7	s
kyselinami	kyselina	k1gFnPc7	kyselina
<g/>
,	,	kIx,	,
s	s	k7c7	s
roztoky	roztok	k1gInPc7	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Minerály	minerál	k1gInPc1	minerál
jako	jako	k8xC	jako
platina	platina	k1gFnSc1	platina
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
křemen	křemen	k1gInSc1	křemen
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
odolné	odolný	k2eAgFnPc1d1	odolná
proti	proti	k7c3	proti
působení	působení	k1gNnSc3	působení
kyselin	kyselina	k1gFnPc2	kyselina
či	či	k8xC	či
hyroxidů	hyroxid	k1gInPc2	hyroxid
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
minerály	minerál	k1gInPc1	minerál
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
i	i	k9	i
při	při	k7c6	při
vysoké	vysoký	k2eAgFnSc6d1	vysoká
teplotě	teplota	k1gFnSc6	teplota
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
kalcit	kalcit	k1gInSc1	kalcit
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
a	a	k8xC	a
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
<g/>
)	)	kIx)	)
Chemické	chemický	k2eAgFnPc1d1	chemická
podmínky	podmínka	k1gFnPc1	podmínka
vzniku	vznik	k1gInSc2	vznik
minerálů	minerál	k1gInPc2	minerál
a	a	k8xC	a
látkové	látkový	k2eAgNnSc4d1	látkové
složení	složení	k1gNnSc4	složení
zemského	zemský	k2eAgNnSc2d1	zemské
tělesa	těleso	k1gNnSc2	těleso
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
geochemie	geochemie	k1gFnSc1	geochemie
<g/>
.	.	kIx.	.
</s>
<s>
Minerály	minerál	k1gInPc1	minerál
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
různorodými	různorodý	k2eAgInPc7d1	různorodý
pochody	pochod	k1gInPc7	pochod
a	a	k8xC	a
za	za	k7c2	za
různých	různý	k2eAgFnPc2d1	různá
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Endogenní	endogenní	k2eAgMnPc1d1	endogenní
(	(	kIx(	(
<g/>
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
)	)	kIx)	)
minerály	minerál	k1gInPc1	minerál
vznikají	vznikat	k5eAaImIp3nP	vznikat
díky	díky	k7c3	díky
uvolňování	uvolňování	k1gNnSc3	uvolňování
tepelné	tepelný	k2eAgFnSc2d1	tepelná
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
nitra	nitro	k1gNnSc2	nitro
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
<g/>
.	.	kIx.	.
</s>
<s>
Minerály	minerál	k1gInPc1	minerál
takto	takto	k6eAd1	takto
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
produkty	produkt	k1gInPc7	produkt
magmatické	magmatický	k2eAgFnSc2d1	magmatická
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Horniny	hornina	k1gFnPc1	hornina
a	a	k8xC	a
ložiska	ložisko	k1gNnPc1	ložisko
vznikají	vznikat	k5eAaImIp3nP	vznikat
krystalizací	krystalizace	k1gFnSc7	krystalizace
samotného	samotný	k2eAgNnSc2d1	samotné
magmatu	magma	k1gNnSc2	magma
<g/>
.	.	kIx.	.
</s>
<s>
Procesy	proces	k1gInPc4	proces
vzniku	vznik	k1gInSc2	vznik
minerálů	minerál	k1gInPc2	minerál
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
hloubkách	hloubka	k1gFnPc6	hloubka
při	při	k7c6	při
rozličných	rozličný	k2eAgFnPc6d1	rozličná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
převážně	převážně	k6eAd1	převážně
vysokých	vysoký	k2eAgFnPc6d1	vysoká
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Magmatická	magmatický	k2eAgFnSc1d1	magmatická
tvorba	tvorba	k1gFnSc1	tvorba
minerálů	minerál	k1gInPc2	minerál
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
podléhá	podléhat	k5eAaImIp3nS	podléhat
všechno	všechen	k3xTgNnSc1	všechen
neustálým	neustálý	k2eAgFnPc3d1	neustálá
proměnám	proměna	k1gFnPc3	proměna
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
minerály	minerál	k1gInPc1	minerál
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
,	,	kIx,	,
rostou	růst	k5eAaImIp3nP	růst
a	a	k8xC	a
mění	měnit	k5eAaImIp3nP	měnit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
a	a	k8xC	a
i	i	k9	i
dnes	dnes	k6eAd1	dnes
vzniká	vznikat	k5eAaImIp3nS	vznikat
uvnitř	uvnitř	k7c2	uvnitř
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
900	[number]	k4	900
<g/>
–	–	k?	–
<g/>
1300	[number]	k4	1300
<g/>
°	°	k?	°
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
tlak	tlak	k1gInSc4	tlak
tisíců	tisíc	k4xCgInPc2	tisíc
atmosfér	atmosféra	k1gFnPc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
hloubkách	hloubka	k1gFnPc6	hloubka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
oblast	oblast	k1gFnSc1	oblast
žhavé	žhavý	k2eAgFnSc2d1	žhavá
–	–	k?	–
tekuté	tekutý	k2eAgFnSc2d1	tekutá
silikátové	silikátový	k2eAgFnSc2d1	silikátová
taveniny	tavenina	k1gFnSc2	tavenina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
magmatem	magma	k1gNnSc7	magma
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
zemská	zemský	k2eAgFnSc1d1	zemská
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vznik	vznik	k1gInSc1	vznik
zlomů	zlom	k1gInPc2	zlom
<g/>
,	,	kIx,	,
vrásnění	vrásnění	k1gNnSc2	vrásnění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proniká	pronikat	k5eAaImIp3nS	pronikat
část	část	k1gFnSc1	část
magmatu	magma	k1gNnSc2	magma
do	do	k7c2	do
vyšších	vysoký	k2eAgFnPc2d2	vyšší
a	a	k8xC	a
chladnějších	chladný	k2eAgFnPc2d2	chladnější
vrstev	vrstva	k1gFnPc2	vrstva
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
postupně	postupně	k6eAd1	postupně
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
masivy	masiv	k1gInPc4	masiv
hlubinných	hlubinný	k2eAgFnPc2d1	hlubinná
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Magma	magma	k1gNnSc1	magma
je	být	k5eAaImIp3nS	být
tavenina	tavenina	k1gFnSc1	tavenina
rozličných	rozličný	k2eAgInPc2d1	rozličný
křemičitanů	křemičitan	k1gInPc2	křemičitan
a	a	k8xC	a
oxidů	oxid	k1gInPc2	oxid
nasycená	nasycený	k2eAgFnSc1d1	nasycená
plyny	plyn	k1gInPc7	plyn
a	a	k8xC	a
vodní	vodní	k2eAgFnSc7d1	vodní
párou	pára	k1gFnSc7	pára
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
složení	složení	k1gNnSc1	složení
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
chemickému	chemický	k2eAgNnSc3d1	chemické
složení	složení	k1gNnSc3	složení
hornin	hornina	k1gFnPc2	hornina
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Rozmanité	rozmanitý	k2eAgNnSc1d1	rozmanité
proudění	proudění	k1gNnSc1	proudění
udržují	udržovat	k5eAaImIp3nP	udržovat
magma	magma	k1gNnSc1	magma
ve	v	k7c6	v
stálém	stálý	k2eAgInSc6d1	stálý
pohybu	pohyb	k1gInSc6	pohyb
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
uskutečňují	uskutečňovat	k5eAaImIp3nP	uskutečňovat
chemické	chemický	k2eAgFnPc1d1	chemická
reakce	reakce	k1gFnPc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nové	nový	k2eAgFnPc4d1	nová
sloučeniny	sloučenina	k1gFnPc4	sloučenina
odpovídající	odpovídající	k2eAgFnPc4d1	odpovídající
nově	nově	k6eAd1	nově
vytvořeným	vytvořený	k2eAgInSc7d1	vytvořený
minerálem	minerál	k1gInSc7	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pronikne	proniknout	k5eAaPmIp3nS	proniknout
žhavá	žhavý	k2eAgNnPc4d1	žhavé
tekutá	tekutý	k2eAgNnPc4d1	tekuté
magma	magma	k1gNnSc4	magma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
velkým	velký	k2eAgInSc7d1	velký
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
do	do	k7c2	do
vyšších	vysoký	k2eAgFnPc2d2	vyšší
a	a	k8xC	a
chladnějších	chladný	k2eAgFnPc2d2	chladnější
vrstev	vrstva	k1gFnPc2	vrstva
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
ochlazování	ochlazování	k1gNnSc2	ochlazování
magmatu	magma	k1gNnSc2	magma
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
první	první	k4xOgInPc4	první
minerály	minerál	k1gInPc4	minerál
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přibývajícím	přibývající	k2eAgNnSc7d1	přibývající
ochlazováním	ochlazování	k1gNnSc7	ochlazování
magmatu	magma	k1gNnSc2	magma
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
i	i	k9	i
počet	počet	k1gInSc1	počet
vznikajících	vznikající	k2eAgInPc2d1	vznikající
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Specificky	specificky	k6eAd1	specificky
lehčí	lehký	k2eAgInPc4d2	lehčí
minerály	minerál	k1gInPc4	minerál
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vykrystalizovaly	vykrystalizovat	k5eAaPmAgInP	vykrystalizovat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
prvním	první	k4xOgInSc6	první
stadiu	stadion	k1gNnSc6	stadion
<g/>
,	,	kIx,	,
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
vrstvách	vrstva	k1gFnPc6	vrstva
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
těžší	těžký	k2eAgInPc1d2	těžší
pozvolna	pozvolna	k6eAd1	pozvolna
klesají	klesat	k5eAaImIp3nP	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
nazýváme	nazývat	k5eAaImIp1nP	nazývat
magmatickou	magmatický	k2eAgFnSc7d1	magmatická
diferenciací	diferenciace	k1gFnSc7	diferenciace
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
nahromadí	nahromadit	k5eAaPmIp3nP	nahromadit
určité	určitý	k2eAgInPc4d1	určitý
minerálty	minerált	k1gInPc4	minerált
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
ložiska	ložisko	k1gNnPc4	ložisko
(	(	kIx(	(
<g/>
např.	např.	kA	např.
magnetitu	magnetit	k1gInSc2	magnetit
nebo	nebo	k8xC	nebo
chromitu	chromit	k1gInSc2	chromit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ochlazování	ochlazování	k1gNnSc2	ochlazování
magmatu	magma	k1gNnSc2	magma
rostou	růst	k5eAaImIp3nP	růst
další	další	k2eAgInPc1d1	další
krystaly	krystal	k1gInPc1	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
nepatrných	patrný	k2eNgInPc2d1	patrný
zárodků	zárodek	k1gInPc2	zárodek
zákonitým	zákonitý	k2eAgNnSc7d1	zákonité
navrstvováním	navrstvování	k1gNnSc7	navrstvování
nových	nový	k2eAgFnPc2d1	nová
stavebních	stavební	k2eAgFnPc2d1	stavební
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
končí	končit	k5eAaImIp3nS	končit
až	až	k9	až
po	po	k7c6	po
úplném	úplný	k2eAgNnSc6d1	úplné
ztuhnutí	ztuhnutí	k1gNnSc6	ztuhnutí
celé	celá	k1gFnSc2	celá
magmatu	magma	k1gNnSc2	magma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
fázi	fáze	k1gFnSc6	fáze
krystalizace	krystalizace	k1gFnSc2	krystalizace
se	se	k3xPyFc4	se
v	v	k7c6	v
magmatu	magma	k1gNnSc6	magma
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
obsah	obsah	k1gInSc1	obsah
snadno	snadno	k6eAd1	snadno
pohyblivých	pohyblivý	k2eAgFnPc2d1	pohyblivá
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
řidší	řídký	k2eAgFnSc1d2	řidší
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
původního	původní	k2eAgNnSc2d1	původní
magmatického	magmatický	k2eAgNnSc2d1	magmatické
centra	centrum	k1gNnSc2	centrum
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
pegmatit	pegmatit	k1gInSc1	pegmatit
<g/>
.	.	kIx.	.
</s>
<s>
Soustřeďují	soustřeďovat	k5eAaImIp3nP	soustřeďovat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
minerály	minerál	k1gInPc4	minerál
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
slídy	slída	k1gFnSc2	slída
<g/>
,	,	kIx,	,
turmalín	turmalín	k1gInSc1	turmalín
<g/>
,	,	kIx,	,
beryl	beryl	k1gInSc1	beryl
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInPc1d1	obsahující
prvky	prvek	k1gInPc1	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
rudy	ruda	k1gFnSc2	ruda
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
wolframu	wolfram	k1gInSc2	wolfram
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ztuhne	ztuhnout	k5eAaPmIp3nS	ztuhnout
i	i	k9	i
tzv.	tzv.	kA	tzv.
zbytková	zbytkový	k2eAgNnPc4d1	zbytkové
magma	magma	k1gNnSc4	magma
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
mandlovcovité	mandlovcovitý	k2eAgFnPc4d1	mandlovcovitý
dutiny	dutina	k1gFnPc4	dutina
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
vzduchové	vzduchový	k2eAgFnPc4d1	vzduchová
bubliny	bublina	k1gFnPc4	bublina
například	například	k6eAd1	například
v	v	k7c6	v
bochníku	bochník	k1gInSc6	bochník
chleba	chléb	k1gInSc2	chléb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
dutiny	dutina	k1gFnPc1	dutina
později	pozdě	k6eAd2	pozdě
vyplní	vyplnit	k5eAaPmIp3nP	vyplnit
křemenem	křemen	k1gInSc7	křemen
<g/>
,	,	kIx,	,
achátem	achát	k1gInSc7	achát
<g/>
,	,	kIx,	,
chalcedon	chalcedon	k1gInSc1	chalcedon
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgInPc7d1	jiný
minerály	minerál	k1gInPc7	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc4d1	podobná
"	"	kIx"	"
<g/>
mandle	mandle	k1gFnPc4	mandle
<g/>
"	"	kIx"	"
často	často	k6eAd1	často
nacházíme	nacházet	k5eAaImIp1nP	nacházet
v	v	k7c6	v
melafyrových	melafyrový	k2eAgFnPc6d1	melafyrový
horninách	hornina	k1gFnPc6	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
par	para	k1gFnPc2	para
uniká	unikat	k5eAaImIp3nS	unikat
přes	přes	k7c4	přes
pukliny	puklina	k1gFnPc4	puklina
a	a	k8xC	a
trhliny	trhlina	k1gFnPc4	trhlina
v	v	k7c6	v
hornině	hornina	k1gFnSc6	hornina
k	k	k7c3	k
zemskému	zemský	k2eAgInSc3d1	zemský
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
horké	horký	k2eAgInPc1d1	horký
roztoky	roztok	k1gInPc1	roztok
ochlazují	ochlazovat	k5eAaImIp3nP	ochlazovat
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nové	nový	k2eAgInPc4d1	nový
minerály	minerál	k1gInPc4	minerál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
stěny	stěna	k1gFnPc4	stěna
puklin	puklina	k1gFnPc2	puklina
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
krystalů	krystal	k1gInPc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stadiu	stadion	k1gNnSc6	stadion
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
hydrotermální	hydrotermální	k2eAgFnPc1d1	hydrotermální
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
nejznámější	známý	k2eAgInPc4d3	nejznámější
minerály	minerál	k1gInPc4	minerál
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
křemen	křemen	k1gInSc1	křemen
a	a	k8xC	a
kalcit	kalcit	k1gInSc1	kalcit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
prvky	prvek	k1gInPc1	prvek
těžkých	těžký	k2eAgInPc2d1	těžký
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
různé	různý	k2eAgFnPc4d1	různá
rudní	rudní	k2eAgFnPc4d1	rudní
žíly	žíla	k1gFnPc4	žíla
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
určité	určitý	k2eAgFnPc1d1	určitá
rudy	ruda	k1gFnPc1	ruda
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
rudy	ruda	k1gFnSc2	ruda
molybdenu	molybden	k1gInSc2	molybden
<g/>
,	,	kIx,	,
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
wolframu	wolfram	k1gInSc2	wolfram
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
horkých	horký	k2eAgInPc2d1	horký
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
par	para	k1gFnPc2	para
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
pneumatolýze	pneumatolýza	k1gFnSc6	pneumatolýza
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
o	o	k7c6	o
pneumatolytickém	pneumatolytický	k2eAgInSc6d1	pneumatolytický
vzniku	vznik	k1gInSc6	vznik
ložisek	ložisko	k1gNnPc2	ložisko
<g/>
.	.	kIx.	.
</s>
<s>
Blízko	blízko	k7c2	blízko
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
nasycená	nasycený	k2eAgFnSc1d1	nasycená
minerálními	minerální	k2eAgFnPc7d1	minerální
látkami	látka	k1gFnPc7	látka
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
prosakující	prosakující	k2eAgFnSc7d1	prosakující
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
vodou	voda	k1gFnSc7	voda
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
minerálního	minerální	k2eAgInSc2d1	minerální
pramene	pramen	k1gInSc2	pramen
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
horkých	horký	k2eAgInPc2d1	horký
nebo	nebo	k8xC	nebo
chladných	chladný	k2eAgInPc2d1	chladný
minerálních	minerální	k2eAgInPc2d1	minerální
pramenů	pramen	k1gInPc2	pramen
se	se	k3xPyFc4	se
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
další	další	k2eAgInPc1d1	další
minerály	minerál	k1gInPc1	minerál
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
vřídlovec	vřídlovec	k1gInSc1	vřídlovec
(	(	kIx(	(
<g/>
aragonit	aragonit	k1gInSc1	aragonit
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
gejzírit	gejzírit	k1gInSc4	gejzírit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pronikají	pronikat	k5eAaImIp3nP	pronikat
horké	horký	k2eAgInPc1d1	horký
roztoky	roztok	k1gInPc1	roztok
a	a	k8xC	a
plyny	plyn	k1gInPc1	plyn
přes	přes	k7c4	přes
trhliny	trhlina	k1gFnPc4	trhlina
a	a	k8xC	a
pukliny	puklina	k1gFnPc4	puklina
v	v	k7c6	v
usazených	usazený	k2eAgFnPc6d1	usazená
horninách	hornina	k1gFnPc6	hornina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
přes	přes	k7c4	přes
vápenec	vápenec	k1gInSc4	vápenec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozpouštějí	rozpouštět	k5eAaImIp3nP	rozpouštět
je	on	k3xPp3gInPc4	on
a	a	k8xC	a
tak	tak	k6eAd1	tak
vznikají	vznikat	k5eAaImIp3nP	vznikat
nové	nový	k2eAgInPc4d1	nový
<g/>
,	,	kIx,	,
druhotné	druhotný	k2eAgInPc4d1	druhotný
minerály	minerál	k1gInPc4	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
tvorba	tvorba	k1gFnSc1	tvorba
minerálů	minerál	k1gInPc2	minerál
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
metasomatóza	metasomatóza	k1gFnSc1	metasomatóza
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
např.	např.	kA	např.
některé	některý	k3yIgFnPc1	některý
ložiska	ložisko	k1gNnPc1	ložisko
magnezitu	magnezit	k1gInSc2	magnezit
nebo	nebo	k8xC	nebo
sideritu	siderit	k1gInSc2	siderit
(	(	kIx(	(
<g/>
ocelku	ocelek	k1gInSc2	ocelek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Exogenní	exogenní	k2eAgMnPc1d1	exogenní
(	(	kIx(	(
<g/>
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
venku	venku	k6eAd1	venku
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
procesech	proces	k1gInPc6	proces
probíhajících	probíhající	k2eAgFnPc2d1	probíhající
díky	díky	k7c3	díky
vnější	vnější	k2eAgFnSc3d1	vnější
sluneční	sluneční	k2eAgFnSc3d1	sluneční
energii	energie	k1gFnSc3	energie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dopadá	dopadat	k5eAaImIp3nS	dopadat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
záření	záření	k1gNnSc2	záření
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
materiálu	materiál	k1gInSc2	materiál
jsou	být	k5eAaImIp3nP	být
rozličné	rozličný	k2eAgFnPc1d1	rozličná
horniny	hornina	k1gFnPc1	hornina
a	a	k8xC	a
rudy	ruda	k1gFnPc1	ruda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
obnažují	obnažovat	k5eAaImIp3nP	obnažovat
a	a	k8xC	a
rozrušují	rozrušovat	k5eAaImIp3nP	rozrušovat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Procesy	proces	k1gInPc4	proces
vzniku	vznik	k1gInSc2	vznik
minerálů	minerál	k1gInPc2	minerál
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
nejsvrchnější	svrchní	k2eAgFnSc6d3	nejsvrchnější
části	část	k1gFnSc6	část
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c2	za
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
a	a	k8xC	a
tlaků	tlak	k1gInPc2	tlak
blízkých	blízký	k2eAgInPc2d1	blízký
k	k	k7c3	k
atmosférickým	atmosférický	k2eAgFnPc3d1	atmosférická
<g/>
,	,	kIx,	,
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
působení	působení	k1gNnSc2	působení
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
a	a	k8xC	a
chemických	chemický	k2eAgInPc2d1	chemický
činitelů	činitel	k1gInPc2	činitel
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
hydrosféry	hydrosféra	k1gFnSc2	hydrosféra
a	a	k8xC	a
biosféry	biosféra	k1gFnSc2	biosféra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
minerály	minerál	k1gInPc4	minerál
a	a	k8xC	a
horniny	hornina	k1gFnPc4	hornina
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
působí	působit	k5eAaImIp3nP	působit
mnohé	mnohý	k2eAgInPc1d1	mnohý
rušivé	rušivý	k2eAgInPc1d1	rušivý
vlivy	vliv	k1gInPc1	vliv
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
souborně	souborně	k6eAd1	souborně
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k8xS	jako
zvětrávání	zvětrávání	k1gNnSc4	zvětrávání
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
složitý	složitý	k2eAgInSc4d1	složitý
komplexní	komplexní	k2eAgInSc4d1	komplexní
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
posuzování	posuzování	k1gNnSc6	posuzování
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
přihlédnout	přihlédnout	k5eAaPmF	přihlédnout
k	k	k7c3	k
hlavním	hlavní	k2eAgInPc3d1	hlavní
zvětrávacím	zvětrávací	k2eAgInPc3d1	zvětrávací
procesům	proces	k1gInPc3	proces
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
působí	působit	k5eAaImIp3nP	působit
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neustále	neustále	k6eAd1	neustále
a	a	k8xC	a
nezadržitelně	zadržitelně	k6eNd1	zadržitelně
<g/>
.	.	kIx.	.
</s>
<s>
Změnami	změna	k1gFnPc7	změna
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
trhavými	trhavý	k2eAgInPc7d1	trhavý
účinky	účinek	k1gInPc7	účinek
mrazu	mráz	k1gInSc2	mráz
<g/>
,	,	kIx,	,
krystalizací	krystalizace	k1gFnPc2	krystalizace
sekundárních	sekundární	k2eAgFnPc2d1	sekundární
solí	sůl	k1gFnPc2	sůl
<g/>
,	,	kIx,	,
přenosem	přenos	k1gInSc7	přenos
horninového	horninový	k2eAgInSc2d1	horninový
materiálu	materiál	k1gInSc2	materiál
větrem	vítr	k1gInSc7	vítr
či	či	k8xC	či
vodou	voda	k1gFnSc7	voda
se	se	k3xPyFc4	se
horniny	hornina	k1gFnPc1	hornina
rozrušují	rozrušovat	k5eAaImIp3nP	rozrušovat
mechanicky	mechanicky	k6eAd1	mechanicky
<g/>
,	,	kIx,	,
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
a	a	k8xC	a
vodou	voda	k1gFnSc7	voda
zase	zase	k9	zase
chemicky	chemicky	k6eAd1	chemicky
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc4d1	značný
rušivý	rušivý	k2eAgInSc4d1	rušivý
vliv	vliv	k1gInSc4	vliv
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
biologické	biologický	k2eAgInPc4d1	biologický
procesy	proces	k1gInPc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Zvětrávání	zvětrávání	k1gNnSc1	zvětrávání
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
podstatné	podstatný	k2eAgFnPc4d1	podstatná
proměny	proměna	k1gFnPc4	proměna
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
živce	živec	k1gInPc1	živec
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
na	na	k7c4	na
kaolin	kaolin	k1gInSc4	kaolin
či	či	k8xC	či
jiné	jiný	k2eAgInPc4d1	jiný
jílové	jílový	k2eAgInPc4d1	jílový
minerály	minerál	k1gInPc4	minerál
<g/>
,	,	kIx,	,
olivín	olivín	k1gInSc1	olivín
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
serpentin	serpentin	k1gInSc4	serpentin
a	a	k8xC	a
zlatožlutý	zlatožlutý	k2eAgInSc1d1	zlatožlutý
pyrit	pyrit	k1gInSc1	pyrit
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
hnědý	hnědý	k2eAgInSc4d1	hnědý
limonit	limonit	k1gInSc4	limonit
<g/>
.	.	kIx.	.
</s>
<s>
Zvětráváním	zvětrávání	k1gNnSc7	zvětrávání
pyritu	pyrit	k1gInSc2	pyrit
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pak	pak	k6eAd1	pak
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
účinkem	účinek	k1gInSc7	účinek
může	moct	k5eAaImIp3nS	moct
vznikat	vznikat	k5eAaImF	vznikat
např.	např.	kA	např.
z	z	k7c2	z
vápence	vápenec	k1gInSc2	vápenec
sádrovec	sádrovec	k1gInSc1	sádrovec
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgInPc1d1	jiný
sírany	síran	k1gInPc1	síran
<g/>
.	.	kIx.	.
</s>
<s>
Podobnými	podobný	k2eAgInPc7d1	podobný
procesy	proces	k1gInPc7	proces
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
vzácný	vzácný	k2eAgInSc4d1	vzácný
opál	opál	k1gInSc4	opál
<g/>
.	.	kIx.	.
</s>
<s>
Zvětráváním	zvětrávání	k1gNnSc7	zvětrávání
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
minerálu	minerál	k1gInSc2	minerál
<g/>
,	,	kIx,	,
např.	např.	kA	např.
chalkopyritu	chalkopyrit	k1gInSc2	chalkopyrit
<g/>
,	,	kIx,	,
sekundární	sekundární	k2eAgInPc1d1	sekundární
minerálty	minerált	k1gInPc1	minerált
jako	jako	k8xS	jako
malachit	malachit	k1gInSc1	malachit
<g/>
,	,	kIx,	,
azurit	azurit	k1gInSc1	azurit
nebo	nebo	k8xC	nebo
limonit	limonit	k1gInSc1	limonit
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
krápníkové	krápníkový	k2eAgFnPc1d1	krápníková
jeskyně	jeskyně	k1gFnPc1	jeskyně
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
také	také	k9	také
důsledkem	důsledek	k1gInSc7	důsledek
zvětrávacích	zvětrávací	k2eAgInPc2d1	zvětrávací
pochodů	pochod	k1gInPc2	pochod
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
endogenní	endogenní	k2eAgFnPc1d1	endogenní
i	i	k8xC	i
exogenní	exogenní	k2eAgFnPc1d1	exogenní
minerální	minerální	k2eAgFnPc1d1	minerální
masy	masa	k1gFnPc1	masa
překonávají	překonávat	k5eAaImIp3nP	překonávat
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
za	za	k7c2	za
změněných	změněný	k2eAgFnPc2d1	změněná
vnějších	vnější	k2eAgFnPc2d1	vnější
podmínek	podmínka	k1gFnPc2	podmínka
různé	různý	k2eAgFnSc2d1	různá
změny	změna	k1gFnSc2	změna
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vlastně	vlastně	k9	vlastně
vznikají	vznikat	k5eAaImIp3nP	vznikat
nové	nový	k2eAgInPc4d1	nový
minerály	minerál	k1gInPc4	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgFnPc3	tento
změnám	změna	k1gFnPc3	změna
říkáme	říkat	k5eAaImIp1nP	říkat
metamorfóza	metamorfóza	k1gFnSc1	metamorfóza
<g/>
.	.	kIx.	.
</s>
<s>
Zvlášť	zvlášť	k6eAd1	zvlášť
velké	velký	k2eAgFnPc1d1	velká
změny	změna	k1gFnPc1	změna
nastávají	nastávat	k5eAaImIp3nP	nastávat
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
regionální	regionální	k2eAgFnSc6d1	regionální
metamorfóze	metamorfóza	k1gFnSc6	metamorfóza
<g/>
.	.	kIx.	.
</s>
<s>
Žhavé	žhavý	k2eAgNnSc1d1	žhavé
tekuté	tekutý	k2eAgNnSc1d1	tekuté
magma	magma	k1gNnSc1	magma
vystupující	vystupující	k2eAgNnSc1d1	vystupující
z	z	k7c2	z
hlubin	hlubina	k1gFnPc2	hlubina
Země	zem	k1gFnSc2	zem
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
vrstvy	vrstva	k1gFnPc4	vrstva
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
proniká	pronikat	k5eAaImIp3nS	pronikat
<g/>
.	.	kIx.	.
</s>
<s>
Mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
metamorfuje	metamorfovat	k5eAaImIp3nS	metamorfovat
okolní	okolní	k2eAgFnPc4d1	okolní
starší	starý	k2eAgFnPc4d2	starší
horniny	hornina	k1gFnPc4	hornina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
usazené	usazený	k2eAgNnSc1d1	usazené
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
a	a	k8xC	a
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
chemickými	chemický	k2eAgFnPc7d1	chemická
reakcemi	reakce	k1gFnPc7	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Usazené	usazený	k2eAgFnPc1d1	usazená
horniny	hornina	k1gFnPc1	hornina
přitom	přitom	k6eAd1	přitom
nabývají	nabývat	k5eAaImIp3nP	nabývat
jiný	jiný	k2eAgInSc4d1	jiný
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
i	i	k8xC	i
chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgInPc6	tento
procesech	proces	k1gInPc6	proces
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
nové	nový	k2eAgInPc1d1	nový
<g/>
,	,	kIx,	,
přeměněné	přeměněný	k2eAgInPc1d1	přeměněný
(	(	kIx(	(
<g/>
metamorfované	metamorfovaný	k2eAgFnPc1d1	metamorfovaná
<g/>
)	)	kIx)	)
horniny	hornina	k1gFnPc1	hornina
a	a	k8xC	a
minerály	minerál	k1gInPc1	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikají	vznikat	k5eAaImIp3nP	vznikat
např.	např.	kA	např.
některé	některý	k3yIgFnPc4	některý
slídy	slída	k1gFnPc4	slída
<g/>
,	,	kIx,	,
granáty	granát	k1gInPc4	granát
<g/>
,	,	kIx,	,
kyanit	kyanit	k1gInSc1	kyanit
<g/>
,	,	kIx,	,
staurolit	staurolit	k1gInSc1	staurolit
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
minerály	minerál	k1gInPc1	minerál
se	se	k3xPyFc4	se
usazují	usazovat	k5eAaImIp3nP	usazovat
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
buď	buď	k8xC	buď
odpařením	odpaření	k1gNnSc7	odpaření
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
změnou	změna	k1gFnSc7	změna
její	její	k3xOp3gInSc4	její
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
ložiska	ložisko	k1gNnPc1	ložisko
kamenné	kamenný	k2eAgFnSc2d1	kamenná
soli	sůl	k1gFnSc2	sůl
nebo	nebo	k8xC	nebo
sylvínu	sylvín	k1gInSc2	sylvín
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
sádrovec	sádrovec	k1gInSc1	sádrovec
<g/>
,	,	kIx,	,
vápenec	vápenec	k1gInSc1	vápenec
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
železné	železný	k2eAgFnPc1d1	železná
rudy	ruda	k1gFnPc1	ruda
(	(	kIx(	(
<g/>
chamozit	chamozit	k5eAaPmF	chamozit
nebo	nebo	k8xC	nebo
tzv.	tzv.	kA	tzv.
bahenní	bahenní	k2eAgFnSc2d1	bahenní
rudy	ruda	k1gFnSc2	ruda
–	–	k?	–
limonit	limonit	k1gInSc1	limonit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Živá	živý	k2eAgFnSc1d1	živá
příroda	příroda	k1gFnSc1	příroda
nepůsobí	působit	k5eNaImIp3nS	působit
na	na	k7c4	na
minerály	minerál	k1gInPc4	minerál
(	(	kIx(	(
<g/>
a	a	k8xC	a
horniny	hornina	k1gFnSc2	hornina
<g/>
)	)	kIx)	)
jen	jen	k9	jen
rušivě	rušivě	k6eAd1	rušivě
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
i	i	k9	i
nové	nový	k2eAgInPc4d1	nový
nerosty	nerost	k1gInPc4	nerost
z	z	k7c2	z
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rozpuštěny	rozpustit	k5eAaPmNgFnP	rozpustit
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
atoly	atol	k1gInPc1	atol
a	a	k8xC	a
celé	celý	k2eAgInPc1d1	celý
vápencové	vápencový	k2eAgInPc1d1	vápencový
masivy	masiv	k1gInPc1	masiv
jsou	být	k5eAaImIp3nP	být
vlastně	vlastně	k9	vlastně
produktem	produkt	k1gInSc7	produkt
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
minerály	minerál	k1gInPc1	minerál
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
tvořit	tvořit	k5eAaImF	tvořit
i	i	k9	i
z	z	k7c2	z
rozložených	rozložený	k2eAgInPc2d1	rozložený
zbytků	zbytek	k1gInPc2	zbytek
mrtvých	mrtvý	k2eAgInPc2d1	mrtvý
organizmů	organizmus	k1gInPc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příklad	příklad	k1gInSc4	příklad
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vznikající	vznikající	k2eAgNnPc1d1	vznikající
ložiska	ložisko	k1gNnPc1	ložisko
fosfátů	fosfát	k1gInPc2	fosfát
<g/>
.	.	kIx.	.
</s>
<s>
Biogenním	biogenní	k2eAgInSc7d1	biogenní
způsobem	způsob	k1gInSc7	způsob
může	moct	k5eAaImIp3nS	moct
vznikat	vznikat	k5eAaImF	vznikat
i	i	k9	i
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
ledek	ledek	k1gInSc1	ledek
<g/>
,	,	kIx,	,
pyrit	pyrit	k1gInSc1	pyrit
a	a	k8xC	a
markazit	markazit	k1gInSc1	markazit
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
se	se	k3xPyFc4	se
nerost	nerost	k1gInSc1	nerost
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
ho	on	k3xPp3gMnSc4	on
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
další	další	k2eAgMnPc1d1	další
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
minerály	minerál	k1gInPc4	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc4	takový
nerostné	nerostný	k2eAgNnSc4d1	nerostné
společenství	společenství	k1gNnSc4	společenství
označujeme	označovat	k5eAaImIp1nP	označovat
termínem	termín	k1gInSc7	termín
parageneze	parageneze	k1gFnSc2	parageneze
<g/>
.	.	kIx.	.
</s>
<s>
Společný	společný	k2eAgInSc1d1	společný
vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
výskyt	výskyt	k1gInSc1	výskyt
nerostů	nerost	k1gInPc2	nerost
podléhá	podléhat	k5eAaImIp3nS	podléhat
určitým	určitý	k2eAgInPc3d1	určitý
zákonům	zákon	k1gInPc3	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Poznání	poznání	k1gNnSc1	poznání
těchto	tento	k3xDgFnPc2	tento
zákonitosti	zákonitost	k1gFnSc2	zákonitost
nám	my	k3xPp1nPc3	my
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
poznat	poznat	k5eAaPmF	poznat
pochody	pochod	k1gInPc4	pochod
vzniku	vznik	k1gInSc2	vznik
nerostů	nerost	k1gInPc2	nerost
<g/>
.	.	kIx.	.
</s>
<s>
Nalezením	nalezení	k1gNnSc7	nalezení
jednoho	jeden	k4xCgInSc2	jeden
minerálu	minerál	k1gInSc2	minerál
můžeme	moct	k5eAaImIp1nP	moct
předpokládat	předpokládat	k5eAaImF	předpokládat
existenci	existence	k1gFnSc4	existence
dalších	další	k2eAgInPc2d1	další
doprovodných	doprovodný	k2eAgInPc2d1	doprovodný
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Krystalické	krystalický	k2eAgFnPc1d1	krystalická
látky	látka	k1gFnPc1	látka
o	o	k7c6	o
přesně	přesně	k6eAd1	přesně
známém	známý	k2eAgNnSc6d1	známé
chemickém	chemický	k2eAgNnSc6d1	chemické
složení	složení	k1gNnSc6	složení
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
většinou	většinou	k6eAd1	většinou
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
nějakému	nějaký	k3yIgInSc3	nějaký
minerálu	minerál	k1gInSc3	minerál
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
běžně	běžně	k6eAd1	běžně
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
synteticky	synteticky	k6eAd1	synteticky
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
za	za	k7c7	za
různými	různý	k2eAgInPc7d1	různý
účely	účel	k1gInPc7	účel
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
kalibrace	kalibrace	k1gFnSc1	kalibrace
analytických	analytický	k2eAgInPc2d1	analytický
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
je	být	k5eAaImIp3nS	být
cenná	cenný	k2eAgFnSc1d1	cenná
ta	ten	k3xDgFnSc1	ten
vlastnost	vlastnost	k1gFnSc1	vlastnost
syntetického	syntetický	k2eAgInSc2d1	syntetický
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známého	známý	k2eAgNnSc2d1	známé
složení	složení	k1gNnSc2	složení
a	a	k8xC	a
struktury	struktura	k1gFnSc2	struktura
a	a	k8xC	a
tedy	tedy	k9	tedy
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
jeho	jeho	k3xOp3gFnPc4	jeho
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
předpovědět	předpovědět	k5eAaPmF	předpovědět
pomocí	pomocí	k7c2	pomocí
matematického	matematický	k2eAgNnSc2d1	matematické
modelování	modelování	k1gNnSc2	modelování
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
aplikací	aplikace	k1gFnSc7	aplikace
umělé	umělý	k2eAgFnSc2d1	umělá
krystalické	krystalický	k2eAgFnSc2d1	krystalická
látky	látka	k1gFnSc2	látka
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
např.	např.	kA	např.
diamantu	diamant	k1gInSc2	diamant
na	na	k7c6	na
brusných	brusný	k2eAgInPc6d1	brusný
nebo	nebo	k8xC	nebo
řezných	řezný	k2eAgInPc6d1	řezný
nástrojích	nástroj	k1gInPc6	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
syntetické	syntetický	k2eAgFnPc1d1	syntetická
látky	látka	k1gFnPc1	látka
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
podle	podle	k7c2	podle
výše	výše	k1gFnSc2	výše
uvedené	uvedený	k2eAgFnSc2d1	uvedená
definice	definice	k1gFnSc2	definice
minerálem	minerál	k1gInSc7	minerál
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgInSc1d1	přírodní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
spojení	spojení	k1gNnSc1	spojení
jako	jako	k8xS	jako
syntetický	syntetický	k2eAgInSc1d1	syntetický
křemen	křemen	k1gInSc1	křemen
nebo	nebo	k8xC	nebo
syntetický	syntetický	k2eAgInSc1d1	syntetický
diamant	diamant	k1gInSc1	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
hornická	hornický	k2eAgFnSc1d1	hornická
legislativa	legislativa	k1gFnSc1	legislativa
chápe	chápat	k5eAaImIp3nS	chápat
nerosty	nerost	k1gInPc4	nerost
oproti	oproti	k7c3	oproti
mineralogickému	mineralogický	k2eAgNnSc3d1	mineralogické
pojetí	pojetí	k1gNnSc3	pojetí
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
a	a	k8xC	a
směšuje	směšovat	k5eAaImIp3nS	směšovat
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
nerost	nerost	k1gInSc4	nerost
minerály	minerál	k1gInPc4	minerál
i	i	k8xC	i
horniny	hornina	k1gFnPc4	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Nerosty	nerost	k1gInPc1	nerost
se	se	k3xPyFc4	se
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
hornictví	hornictví	k1gNnSc2	hornictví
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
vyhrazené	vyhrazený	k2eAgInPc4d1	vyhrazený
nerosty	nerost	k1gInPc4	nerost
a	a	k8xC	a
nevyhrazené	vyhrazený	k2eNgInPc4d1	nevyhrazený
nerosty	nerost	k1gInPc4	nerost
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrazenými	vyhrazený	k2eAgInPc7d1	vyhrazený
nerosty	nerost	k1gInPc7	nerost
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
44	[number]	k4	44
<g/>
/	/	kIx~	/
<g/>
1988	[number]	k4	1988
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
horního	horní	k2eAgInSc2d1	horní
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
a	a	k8xC	a
<g/>
)	)	kIx)	)
radioaktivní	radioaktivní	k2eAgInPc1d1	radioaktivní
nerosty	nerost	k1gInPc1	nerost
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
)	)	kIx)	)
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
hořlavého	hořlavý	k2eAgInSc2d1	hořlavý
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
(	(	kIx(	(
<g/>
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
bituminosní	bituminosní	k2eAgFnSc2d1	bituminosní
horniny	hornina	k1gFnSc2	hornina
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
)	)	kIx)	)
nerosty	nerost	k1gInPc7	nerost
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
průmyslově	průmyslově	k6eAd1	průmyslově
vyrábět	vyrábět	k5eAaImF	vyrábět
kovy	kov	k1gInPc4	kov
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
)	)	kIx)	)
magnezit	magnezit	k1gInSc1	magnezit
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
)	)	kIx)	)
nerosty	nerost	k1gInPc7	nerost
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
průmyslově	průmyslově	k6eAd1	průmyslově
vyrábět	vyrábět	k5eAaImF	vyrábět
fosfor	fosfor	k1gInSc4	fosfor
<g/>
,	,	kIx,	,
síru	síra	k1gFnSc4	síra
a	a	k8xC	a
fluór	fluór	k1gInSc4	fluór
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc1	jejich
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
)	)	kIx)	)
kamenná	kamenný	k2eAgFnSc1d1	kamenná
<g />
.	.	kIx.	.
</s>
<s>
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
draselné	draselný	k2eAgFnSc2d1	draselná
<g/>
,	,	kIx,	,
borové	borový	k2eAgFnSc2d1	Borová
<g/>
,	,	kIx,	,
bromové	bromový	k2eAgFnSc2d1	Bromová
a	a	k8xC	a
jodové	jodový	k2eAgFnSc2d1	jodová
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
)	)	kIx)	)
tuha	tuha	k1gFnSc1	tuha
<g/>
,	,	kIx,	,
baryt	baryt	k1gInSc1	baryt
<g/>
,	,	kIx,	,
azbest	azbest	k1gInSc1	azbest
<g/>
,	,	kIx,	,
slída	slída	k1gFnSc1	slída
<g/>
,	,	kIx,	,
mastek	mastek	k1gInSc1	mastek
<g/>
,	,	kIx,	,
diatomit	diatomit	k5eAaPmF	diatomit
<g/>
,	,	kIx,	,
sklářský	sklářský	k2eAgInSc4d1	sklářský
a	a	k8xC	a
slévárenský	slévárenský	k2eAgInSc4d1	slévárenský
písek	písek	k1gInSc4	písek
<g/>
,	,	kIx,	,
minerální	minerální	k2eAgNnPc1d1	minerální
barviva	barvivo	k1gNnPc1	barvivo
<g/>
,	,	kIx,	,
bentonit	bentonit	k1gInSc1	bentonit
<g/>
,	,	kIx,	,
h	h	k?	h
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
nerosty	nerost	k1gInPc7	nerost
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
průmyslově	průmyslově	k6eAd1	průmyslově
vyrábět	vyrábět	k5eAaImF	vyrábět
prvky	prvek	k1gInPc4	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
a	a	k8xC	a
prvky	prvek	k1gInPc4	prvek
s	s	k7c7	s
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
polovodičů	polovodič	k1gInPc2	polovodič
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
)	)	kIx)	)
granit	granit	k1gInSc1	granit
<g/>
,	,	kIx,	,
granodiorit	granodiorit	k1gInSc1	granodiorit
<g/>
,	,	kIx,	,
diorit	diorit	k1gInSc1	diorit
<g/>
,	,	kIx,	,
gabro	gabro	k1gNnSc1	gabro
<g/>
,	,	kIx,	,
diabas	diabas	k1gInSc1	diabas
<g/>
,	,	kIx,	,
hadec	hadec	k1gInSc1	hadec
<g/>
,	,	kIx,	,
dolomit	dolomit	k1gInSc1	dolomit
a	a	k8xC	a
vápenec	vápenec	k1gInSc1	vápenec
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
blokově	blokově	k6eAd1	blokově
<g />
.	.	kIx.	.
</s>
<s>
dobyvatelné	dobyvatelný	k2eAgNnSc1d1	dobyvatelné
a	a	k8xC	a
leštitelné	leštitelný	k2eAgNnSc1d1	leštitelné
<g/>
,	,	kIx,	,
a	a	k8xC	a
travertin	travertin	k1gInSc1	travertin
<g/>
,	,	kIx,	,
j	j	k?	j
<g/>
)	)	kIx)	)
technicky	technicky	k6eAd1	technicky
využitelné	využitelný	k2eAgInPc1d1	využitelný
krystaly	krystal	k1gInPc1	krystal
nerostů	nerost	k1gInPc2	nerost
a	a	k8xC	a
drahé	drahý	k2eAgInPc1d1	drahý
kameny	kámen	k1gInPc1	kámen
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g/>
)	)	kIx)	)
halloyzit	halloyzit	k1gInSc1	halloyzit
<g/>
,	,	kIx,	,
kaolin	kaolin	k1gInSc1	kaolin
<g/>
,	,	kIx,	,
keramické	keramický	k2eAgInPc1d1	keramický
a	a	k8xC	a
žáruvzdorné	žáruvzdorný	k2eAgInPc1d1	žáruvzdorný
jíly	jíl	k1gInPc1	jíl
a	a	k8xC	a
jílovce	jílovec	k1gInPc1	jílovec
<g/>
,	,	kIx,	,
sádrovec	sádrovec	k1gInSc1	sádrovec
<g/>
,	,	kIx,	,
anhydrit	anhydrit	k1gInSc1	anhydrit
<g/>
,	,	kIx,	,
živce	živec	k1gInPc1	živec
<g/>
,	,	kIx,	,
perlit	perlit	k1gInSc1	perlit
a	a	k8xC	a
zeolit	zeolit	k1gInSc1	zeolit
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
)	)	kIx)	)
křemen	křemen	k1gInSc1	křemen
<g/>
,	,	kIx,	,
křemenec	křemenec	k1gInSc1	křemenec
<g/>
,	,	kIx,	,
vápenec	vápenec	k1gInSc1	vápenec
<g/>
,	,	kIx,	,
dolomit	dolomit	k1gInSc1	dolomit
<g/>
,	,	kIx,	,
slín	slín	k1gInSc1	slín
<g/>
,	,	kIx,	,
čedič	čedič	k1gInSc1	čedič
<g/>
,	,	kIx,	,
znělec	znělec	k1gInSc1	znělec
<g/>
,	,	kIx,	,
trachyt	trachyt	k1gInSc1	trachyt
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
tyto	tento	k3xDgInPc1	tento
nerosty	nerost	k1gInPc1	nerost
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
k	k	k7c3	k
chemicko-technologickému	chemickoechnologický	k2eAgNnSc3d1	chemicko-technologické
zpracování	zpracování	k1gNnSc3	zpracování
nebo	nebo	k8xC	nebo
zpracování	zpracování	k1gNnSc4	zpracování
tavením	tavení	k1gNnSc7	tavení
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
)	)	kIx)	)
mineralizované	mineralizovaný	k2eAgFnSc2d1	mineralizovaná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
průmyslově	průmyslově	k6eAd1	průmyslově
získávat	získávat	k5eAaImF	získávat
vyhrazené	vyhrazený	k2eAgInPc4d1	vyhrazený
nerosty	nerost	k1gInPc4	nerost
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
)	)	kIx)	)
technicky	technicky	k6eAd1	technicky
využitelné	využitelný	k2eAgInPc1d1	využitelný
přírodní	přírodní	k2eAgInPc1d1	přírodní
plyny	plyn	k1gInPc1	plyn
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nepatří	patřit	k5eNaImIp3nS	patřit
mezi	mezi	k7c4	mezi
plyny	plyn	k1gInPc4	plyn
uvedené	uvedený	k2eAgInPc4d1	uvedený
pod	pod	k7c7	pod
písmenem	písmeno	k1gNnSc7	písmeno
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
nerosty	nerost	k1gInPc1	nerost
jsou	být	k5eAaImIp3nP	být
nerosty	nerost	k1gInPc1	nerost
nevyhrazené	vyhrazený	k2eNgInPc1d1	nevyhrazený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pochybnostech	pochybnost	k1gFnPc6	pochybnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
některý	některý	k3yIgInSc1	některý
nerost	nerost	k1gInSc1	nerost
je	být	k5eAaImIp3nS	být
nerostem	nerost	k1gInSc7	nerost
vyhrazeným	vyhrazený	k2eAgInSc7d1	vyhrazený
nebo	nebo	k8xC	nebo
nevyhrazeným	vyhrazený	k2eNgInSc7d1	nevyhrazený
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
v	v	k7c6	v
dohodě	dohoda	k1gFnSc6	dohoda
s	s	k7c7	s
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
a	a	k8xC	a
lékařství	lékařství	k1gNnSc6	lékařství
mají	mít	k5eAaImIp3nP	mít
minerály	minerál	k1gInPc4	minerál
mnohem	mnohem	k6eAd1	mnohem
širší	široký	k2eAgInPc4d2	širší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neexaktně	exaktně	k6eNd1	exaktně
vymezený	vymezený	k2eAgInSc4d1	vymezený
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
orgánů	orgán	k1gInPc2	orgán
či	či	k8xC	či
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c6	o
výživě	výživa	k1gFnSc6	výživa
<g/>
,	,	kIx,	,
pojmem	pojem	k1gInSc7	pojem
minerál	minerál	k1gInSc1	minerál
se	se	k3xPyFc4	se
často	často	k6eAd1	často
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
anorganická	anorganický	k2eAgFnSc1d1	anorganická
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
minerální	minerální	k2eAgInSc1d1	minerální
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pouze	pouze	k6eAd1	pouze
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
v	v	k7c4	v
jaké	jaký	k3yRgFnPc4	jaký
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
(	(	kIx(	(
<g/>
molekuly	molekula	k1gFnPc4	molekula
<g/>
,	,	kIx,	,
krystaly	krystal	k1gInPc4	krystal
<g/>
,	,	kIx,	,
ionty	ion	k1gInPc4	ion
<g/>
)	)	kIx)	)
či	či	k8xC	či
chemické	chemický	k2eAgFnSc3d1	chemická
formě	forma	k1gFnSc3	forma
(	(	kIx(	(
<g/>
čistý	čistý	k2eAgInSc1d1	čistý
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
ionty	ion	k1gInPc1	ion
<g/>
,	,	kIx,	,
anorganické	anorganický	k2eAgFnPc1d1	anorganická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
soli	sůl	k1gFnPc4	sůl
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
významech	význam	k1gInPc6	význam
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
český	český	k2eAgInSc1d1	český
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
nerost	nerost	k1gInSc1	nerost
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
se	se	k3xPyFc4	se
pojem	pojem	k1gInSc1	pojem
minerál	minerál	k1gInSc1	minerál
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
souvisejících	související	k2eAgInPc6d1	související
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
potravinářství	potravinářství	k1gNnSc1	potravinářství
a	a	k8xC	a
farmacie	farmacie	k1gFnSc1	farmacie
(	(	kIx(	(
<g/>
minerální	minerální	k2eAgInPc1d1	minerální
doplňky	doplněk	k1gInPc1	doplněk
stravy	strava	k1gFnSc2	strava
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
