<s>
Lublaň	Lublaň	k1gFnSc1	Lublaň
(	(	kIx(	(
<g/>
slovinsky	slovinsky	k6eAd1	slovinsky
Ljubljana	Ljubljan	k1gMnSc2	Ljubljan
[	[	kIx(	[
<g/>
lj	lj	k?	lj
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
Lubiana	Lubiana	k1gFnSc1	Lubiana
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Laibach	Laibach	k1gInSc1	Laibach
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Aemona	Aemona	k1gFnSc1	Aemona
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
Ľubľana	Ľubľana	k1gFnSc1	Ľubľana
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
.	.	kIx.	.
</s>
