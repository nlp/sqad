<p>
<s>
Muž	muž	k1gMnSc1	muž
je	být	k5eAaImIp3nS	být
dospělý	dospělý	k2eAgMnSc1d1	dospělý
člověk	člověk	k1gMnSc1	člověk
samčího	samčí	k2eAgNnSc2d1	samčí
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
muž	muž	k1gMnSc1	muž
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
pohlaví	pohlaví	k1gNnSc2	pohlaví
člověka	člověk	k1gMnSc2	člověk
nebo	nebo	k8xC	nebo
role	role	k1gFnSc2	role
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
opačného	opačný	k2eAgInSc2d1	opačný
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
samičího	samičí	k2eAgNnSc2d1	samičí
pohlaví	pohlaví	k1gNnSc2	pohlaví
je	být	k5eAaImIp3nS	být
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Nedospělý	dospělý	k2eNgMnSc1d1	nedospělý
člověk	člověk	k1gMnSc1	člověk
samčího	samčí	k2eAgNnSc2d1	samčí
pohlaví	pohlaví	k1gNnSc2	pohlaví
je	být	k5eAaImIp3nS	být
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Symbol	symbol	k1gInSc1	symbol
pro	pro	k7c4	pro
planetu	planeta	k1gFnSc4	planeta
Mars	Mars	k1gInSc1	Mars
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
samčího	samčí	k2eAgInSc2d1	samčí
a	a	k8xC	a
tedy	tedy	k8xC	tedy
i	i	k9	i
mužského	mužský	k2eAgNnSc2d1	mužské
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
stylizované	stylizovaný	k2eAgNnSc4d1	stylizované
znázornění	znázornění	k1gNnSc4	znázornění
štítu	štít	k1gInSc2	štít
a	a	k8xC	a
oštěpu	oštěp	k1gInSc2	oštěp
boha	bůh	k1gMnSc2	bůh
Marta	Mars	k1gMnSc2	Mars
–	–	k?	–
kolečko	kolečko	k1gNnSc1	kolečko
se	s	k7c7	s
šipkou	šipka	k1gFnSc7	šipka
vpravo	vpravo	k6eAd1	vpravo
nahoře	nahoře	k6eAd1	nahoře
(	(	kIx(	(
<g/>
Unicode	Unicod	k1gInSc5	Unicod
<g/>
:	:	kIx,	:
♂	♂	k?	♂
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
mj.	mj.	kA	mj.
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
mužnost	mužnost	k1gFnSc4	mužnost
a	a	k8xC	a
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
alchymii	alchymie	k1gFnSc6	alchymie
označoval	označovat	k5eAaImAgInS	označovat
také	také	k9	také
železo	železo	k1gNnSc4	železo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
role	role	k1gFnSc1	role
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
měli	mít	k5eAaImAgMnP	mít
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
roli	role	k1gFnSc4	role
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lovecko-sběračských	loveckoběračský	k2eAgFnPc6d1	lovecko-sběračský
společnostech	společnost	k1gFnPc6	společnost
měli	mít	k5eAaImAgMnP	mít
muži	muž	k1gMnPc1	muž
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
lov	lov	k1gInSc4	lov
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
ženy	žena	k1gFnPc1	žena
sbíraly	sbírat	k5eAaImAgFnP	sbírat
rostlinnou	rostlinný	k2eAgFnSc4d1	rostlinná
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muži	muž	k1gMnPc1	muž
se	se	k3xPyFc4	se
od	od	k7c2	od
žen	žena	k1gFnPc2	žena
liší	lišit	k5eAaImIp3nS	lišit
svým	svůj	k3xOyFgNnSc7	svůj
chováním	chování	k1gNnSc7	chování
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
běžných	běžný	k2eAgNnPc2d1	běžné
klišé	klišé	k1gNnPc2	klišé
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
agresivnější	agresivní	k2eAgFnPc1d2	agresivnější
než	než	k8xS	než
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezilidských	mezilidský	k2eAgInPc6d1	mezilidský
vztazích	vztah	k1gInPc6	vztah
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
agresivní	agresivní	k2eAgNnSc1d1	agresivní
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
agresivnější	agresivní	k2eAgInPc1d2	agresivnější
mimo	mimo	k7c4	mimo
domov	domov	k1gInSc4	domov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
odvážnější	odvážný	k2eAgFnPc1d2	odvážnější
<g/>
,	,	kIx,	,
dobrodružnější	dobrodružný	k2eAgFnPc1d2	dobrodružnější
a	a	k8xC	a
soutěživější	soutěživý	k2eAgFnPc1d2	soutěživější
než	než	k8xS	než
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
méně	málo	k6eAd2	málo
citoví	citový	k2eAgMnPc1d1	citový
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
kultuře	kultura	k1gFnSc6	kultura
se	se	k3xPyFc4	se
muži	muž	k1gMnPc1	muž
jen	jen	k6eAd1	jen
zřídka	zřídka	k6eAd1	zřídka
oblékají	oblékat	k5eAaImIp3nP	oblékat
jako	jako	k9	jako
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
však	však	k9	však
móda	móda	k1gFnSc1	móda
s	s	k7c7	s
časem	čas	k1gInSc7	čas
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
nošení	nošení	k1gNnSc2	nošení
náušnice	náušnice	k1gFnSc2	náušnice
se	se	k3xPyFc4	se
již	již	k6eAd1	již
obvykle	obvykle	k6eAd1	obvykle
nepovažuje	považovat	k5eNaImIp3nS	považovat
za	za	k7c4	za
ženský	ženský	k2eAgInSc4d1	ženský
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
za	za	k7c4	za
nemužské	mužský	k2eNgNnSc4d1	mužský
již	již	k9	již
nepovažuje	považovat	k5eNaImIp3nS	považovat
holení	holení	k1gNnSc4	holení
chlupů	chlup	k1gInPc2	chlup
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
a	a	k8xC	a
vousů	vous	k1gInPc2	vous
na	na	k7c6	na
tváři	tvář	k1gFnSc6	tvář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muži	muž	k1gMnPc1	muž
zpravidla	zpravidla	k6eAd1	zpravidla
páchají	páchat	k5eAaImIp3nP	páchat
nejvíce	hodně	k6eAd3	hodně
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
pohlaví	pohlaví	k1gNnSc2	pohlaví
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
biologie	biologie	k1gFnSc2	biologie
mají	mít	k5eAaImIp3nP	mít
muži	muž	k1gMnPc1	muž
řadu	řada	k1gFnSc4	řada
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
charakteristik	charakteristika	k1gFnPc2	charakteristika
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
je	on	k3xPp3gNnSc4	on
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
ženy	žena	k1gFnSc2	žena
jsou	být	k5eAaImIp3nP	být
mužské	mužský	k2eAgFnPc1d1	mužská
pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
orgány	orgány	k1gFnPc1	orgány
částí	část	k1gFnPc2	část
rozmnožovací	rozmnožovací	k2eAgFnSc2d1	rozmnožovací
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
patří	patřit	k5eAaImIp3nS	patřit
hlavně	hlavně	k9	hlavně
penis	penis	k1gInSc1	penis
<g/>
,	,	kIx,	,
varlata	varle	k1gNnPc1	varle
<g/>
,	,	kIx,	,
chámovod	chámovod	k1gInSc1	chámovod
a	a	k8xC	a
prostata	prostata	k1gFnSc1	prostata
<g/>
.	.	kIx.	.
</s>
<s>
Samčí	samčí	k2eAgFnSc1d1	samčí
rozmnožovací	rozmnožovací	k2eAgFnSc1d1	rozmnožovací
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
a	a	k8xC	a
ejakulaci	ejakulace	k1gFnSc4	ejakulace
spermatu	sperma	k1gNnSc2	sperma
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
spermie	spermie	k1gFnSc1	spermie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
genetickou	genetický	k2eAgFnSc4d1	genetická
informaci	informace	k1gFnSc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
samčí	samčí	k2eAgFnSc2d1	samčí
rozmnožovací	rozmnožovací	k2eAgFnSc2d1	rozmnožovací
soustavy	soustava	k1gFnSc2	soustava
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
lidského	lidský	k2eAgInSc2d1	lidský
plodu	plod	k1gInSc2	plod
končí	končit	k5eAaImIp3nS	končit
ejakulací	ejakulace	k1gFnSc7	ejakulace
spermatu	sperma	k1gNnSc2	sperma
do	do	k7c2	do
dělohy	děloha	k1gFnSc2	děloha
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
ejakulát	ejakulát	k1gInSc1	ejakulát
zpravidla	zpravidla	k6eAd1	zpravidla
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
vejcovodů	vejcovod	k1gInPc2	vejcovod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
oplodní	oplodnit	k5eAaPmIp3nS	oplodnit
vajíčko	vajíčko	k1gNnSc1	vajíčko
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
v	v	k7c6	v
děloze	děloha	k1gFnSc6	děloha
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
plod	plod	k1gInSc4	plod
<g/>
.	.	kIx.	.
</s>
<s>
Pojetí	pojetí	k1gNnSc4	pojetí
otcovství	otcovství	k1gNnSc2	otcovství
a	a	k8xC	a
rodiny	rodina	k1gFnSc2	rodina
nalézáme	nalézat	k5eAaImIp1nP	nalézat
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
lidské	lidský	k2eAgFnSc6d1	lidská
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
varlata	varle	k1gNnPc4	varle
místem	místem	k6eAd1	místem
tvorby	tvorba	k1gFnSc2	tvorba
mužského	mužský	k2eAgInSc2d1	mužský
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
hormonu	hormon	k1gInSc2	hormon
testosteronu	testosteron	k1gInSc2	testosteron
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
vznik	vznik	k1gInSc4	vznik
primárních	primární	k2eAgInPc2d1	primární
i	i	k8xC	i
sekundárních	sekundární	k2eAgInPc2d1	sekundární
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
rysů	rys	k1gInPc2	rys
chování	chování	k1gNnSc2	chování
typických	typický	k2eAgInPc2d1	typický
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhotné	druhotný	k2eAgInPc1d1	druhotný
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
chlupy	chlup	k1gInPc1	chlup
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
a	a	k8xC	a
silné	silný	k2eAgInPc1d1	silný
svaly	sval	k1gInPc1	sval
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přitahovaly	přitahovat	k5eAaImAgFnP	přitahovat
družku	družka	k1gFnSc4	družka
nebo	nebo	k8xC	nebo
pomohly	pomoct	k5eAaPmAgFnP	pomoct
porazit	porazit	k5eAaPmF	porazit
rivaly	rival	k1gMnPc7	rival
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
žen	žena	k1gFnPc2	žena
mají	mít	k5eAaImIp3nP	mít
muži	muž	k1gMnPc1	muž
pohlavní	pohlavní	k2eAgMnPc1d1	pohlavní
orgány	orgán	k1gInPc4	orgán
převážně	převážně	k6eAd1	převážně
vně	vně	k7c2	vně
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
mnoho	mnoho	k4c1	mnoho
částí	část	k1gFnPc2	část
samčí	samčí	k2eAgFnSc2d1	samčí
rozmnožovací	rozmnožovací	k2eAgFnSc2d1	rozmnožovací
soustavy	soustava	k1gFnSc2	soustava
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
též	též	k9	též
uvnitř	uvnitř	k7c2	uvnitř
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
kupříkladu	kupříkladu	k6eAd1	kupříkladu
prostata	prostata	k1gFnSc1	prostata
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
a	a	k8xC	a
odvětví	odvětví	k1gNnSc1	odvětví
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
samčí	samčí	k2eAgFnSc7d1	samčí
rozmnožovací	rozmnožovací	k2eAgFnSc7d1	rozmnožovací
soustavou	soustava	k1gFnSc7	soustava
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
andrologie	andrologie	k1gFnSc1	andrologie
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
karyotyp	karyotyp	k1gInSc1	karyotyp
46	[number]	k4	46
<g/>
,	,	kIx,	,
<g/>
XY	XY	kA	XY
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
muže	muž	k1gMnSc4	muž
trápí	trápit	k5eAaImIp3nS	trápit
tytéž	týž	k3xTgFnPc4	týž
nemoci	nemoc	k1gFnPc4	nemoc
jako	jako	k8xC	jako
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
několik	několik	k4yIc1	několik
pohlavně	pohlavně	k6eAd1	pohlavně
závislých	závislý	k2eAgFnPc2d1	závislá
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
trpí	trpět	k5eAaImIp3nP	trpět
více	hodně	k6eAd2	hodně
nebo	nebo	k8xC	nebo
výhradně	výhradně	k6eAd1	výhradně
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
autisté	autistý	k2eAgNnSc1d1	autisté
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
barvoslepí	barvoslepý	k2eAgMnPc1d1	barvoslepý
jsou	být	k5eAaImIp3nP	být
častěji	často	k6eAd2	často
muži	muž	k1gMnPc1	muž
než	než	k8xS	než
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
některé	některý	k3yIgFnPc4	některý
věkově	věkově	k6eAd1	věkově
závislé	závislý	k2eAgFnPc4d1	závislá
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Alzheimerova	Alzheimerův	k2eAgFnSc1d1	Alzheimerova
choroba	choroba	k1gFnSc1	choroba
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
častější	častý	k2eAgMnPc1d2	častější
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
nežli	nežli	k8xS	nežli
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Příčina	příčina	k1gFnSc1	příčina
této	tento	k3xDgFnSc2	tento
disproporcionality	disproporcionalita	k1gFnSc2	disproporcionalita
dosud	dosud	k6eAd1	dosud
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jen	jen	k9	jen
biologické	biologický	k2eAgInPc1d1	biologický
faktory	faktor	k1gInPc1	faktor
samotné	samotný	k2eAgInPc1d1	samotný
neurčují	určovat	k5eNaImIp3nP	určovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
osoby	osoba	k1gFnPc1	osoba
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
samy	sám	k3xTgFnPc1	sám
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
někteří	některý	k3yIgMnPc1	některý
muži	muž	k1gMnPc1	muž
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nP	narodit
bez	bez	k7c2	bez
typické	typický	k2eAgFnSc2d1	typická
mužské	mužský	k2eAgFnSc2d1	mužská
fyziologie	fyziologie	k1gFnSc2	fyziologie
(	(	kIx(	(
<g/>
odhady	odhad	k1gInPc1	odhad
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
000	[number]	k4	000
po	po	k7c4	po
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mohou	moct	k5eAaImIp3nP	moct
trpět	trpět	k5eAaImF	trpět
hormonální	hormonální	k2eAgMnPc1d1	hormonální
<g/>
,	,	kIx,	,
či	či	k8xC	či
genetickou	genetický	k2eAgFnSc7d1	genetická
odlišností	odlišnost	k1gFnSc7	odlišnost
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
syndrom	syndrom	k1gInSc1	syndrom
necitlivosti	necitlivost	k1gFnSc2	necitlivost
na	na	k7c4	na
androgen	androgen	k1gInSc4	androgen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
důvodu	důvod	k1gInSc2	důvod
částečně	částečně	k6eAd1	částečně
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
pohlavní	pohlavní	k2eAgInPc4d1	pohlavní
znaky	znak	k1gInPc4	znak
obou	dva	k4xCgInPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Podívejte	podívat	k5eAaPmRp2nP	podívat
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c4	na
transsexualita	transsexualita	k1gFnSc1	transsexualita
a	a	k8xC	a
pohlavní	pohlavní	k2eAgFnSc1d1	pohlavní
identita	identita	k1gFnSc1	identita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
Muslimů	muslim	k1gMnPc2	muslim
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
zemí	zem	k1gFnPc2	zem
jsou	být	k5eAaImIp3nP	být
obřezáni	obřezán	k2eAgMnPc1d1	obřezán
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obřízce	obřízka	k1gFnSc6	obřízka
se	se	k3xPyFc4	se
z	z	k7c2	z
penisu	penis	k1gInSc2	penis
odstraní	odstranit	k5eAaPmIp3nS	odstranit
předkožka	předkožka	k1gFnSc1	předkožka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Statistika	statistika	k1gFnSc1	statistika
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
poměr	poměr	k1gInSc1	poměr
pohlaví	pohlaví	k1gNnSc2	pohlaví
zhruba	zhruba	k6eAd1	zhruba
105	[number]	k4	105
chlapců	chlapec	k1gMnPc2	chlapec
na	na	k7c4	na
100	[number]	k4	100
dívek	dívka	k1gFnPc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
však	však	k9	však
počet	počet	k1gInSc1	počet
mužů	muž	k1gMnPc2	muž
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
počet	počet	k1gInSc1	počet
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Demografická	demografický	k2eAgFnSc1d1	demografická
statistika	statistika	k1gFnSc1	statistika
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Patriarchát	patriarchát	k1gInSc1	patriarchát
</s>
</p>
<p>
<s>
Šovinismus	šovinismus	k1gInSc1	šovinismus
</s>
</p>
<p>
<s>
Urologie	urologie	k1gFnSc1	urologie
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
muž	muž	k1gMnSc1	muž
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Muž	muž	k1gMnSc1	muž
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Muž	muž	k1gMnSc1	muž
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
