<s>
Ministerstvo	ministerstvo	k1gNnSc1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Budova	budova	k1gFnSc1
MPO	MPO	kA
nad	nad	k7c7
náplavkou	náplavka	k1gFnSc7
Na	na	k7c4
FrantiškuZaloženo	FrantiškuZaložen	k2eAgNnSc4d1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1992	#num#	k4
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Na	na	k7c6
Františku	františek	k1gInSc6
1039	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Staré	Stará	k1gFnPc1
Město	město	k1gNnSc1
<g/>
110	#num#	k4
15	#num#	k4
Praha	Praha	k1gFnSc1
1	#num#	k4
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
34,82	34,82	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
32,95	32,95	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Počet	počet	k1gInSc4
zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
913	#num#	k4
(	(	kIx(
<g/>
říjen	říjen	k1gInSc1
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozpočet	rozpočet	k1gInSc1
</s>
<s>
58,2	58,2	k4
mld.	mld.	k?
Kč	Kč	kA
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Ministr	ministr	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Havlíček	Havlíček	k1gMnSc1
(	(	kIx(
<g/>
nestr	nestr	k1gMnSc1
<g/>
.	.	kIx.
za	za	k7c4
ANO	ano	k9
<g/>
)	)	kIx)
Náměstci	náměstek	k1gMnPc1
</s>
<s>
Silvana	Silvana	k1gFnSc1
JirotkováMartina	JirotkováMartin	k1gMnSc2
TauberováJan	TauberováJany	k1gInPc2
DejlMarian	DejlMarian	k1gMnSc1
PiechaPetr	PiechaPetr	k1gMnSc1
OčkoRené	OčkoRená	k1gFnSc2
NedělaEduard	NedělaEduard	k1gInSc1
Muřický	Muřický	k2eAgInSc1d1
Státní	státní	k2eAgInSc1d1
tajemnice	tajemnice	k1gFnSc1
</s>
<s>
Martina	Martina	k1gFnSc1
Děvěrová	Děvěrový	k2eAgFnSc1d1
Webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
www.mpo.cz	www.mpo.cz	k1gMnSc1
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
MPO	MPO	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ústřední	ústřední	k2eAgInSc1d1
orgán	orgán	k1gInSc1
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
pro	pro	k7c4
státní	státní	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
průmyslu	průmysl	k1gInSc2
<g/>
,	,	kIx,
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
surovin	surovina	k1gFnPc2
a	a	k8xC
ekonomických	ekonomický	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
vůči	vůči	k7c3
zahraničí	zahraničí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
zřídil	zřídit	k5eAaPmAgInS
s	s	k7c7
účinností	účinnost	k1gFnSc7
od	od	k7c2
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1992	#num#	k4
zákon	zákon	k1gInSc1
č.	č.	k?
474	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
ze	z	k7c2
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
<g/>
,	,	kIx,
novelizující	novelizující	k2eAgInSc1d1
kompetenční	kompetenční	k2eAgInSc1d1
zákon	zákon	k1gInSc1
zákon	zákon	k1gInSc1
č.	č.	k?
2	#num#	k4
<g/>
/	/	kIx~
<g/>
1969	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c4
zřízení	zřízení	k1gNnSc4
ministerstev	ministerstvo	k1gNnPc2
a	a	k8xC
jiných	jiný	k2eAgInPc2d1
ústředních	ústřední	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
(	(	kIx(
<g/>
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
)	)	kIx)
vymezuje	vymezovat	k5eAaImIp3nS
základní	základní	k2eAgFnSc4d1
působnost	působnost	k1gFnSc4
ministerstva	ministerstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblasti	oblast	k1gFnPc4
působnosti	působnost	k1gFnSc2
MPO	MPO	kA
byly	být	k5eAaImAgInP
předtím	předtím	k6eAd1
rozděleny	rozdělit	k5eAaPmNgInP
mezi	mezi	k7c4
rušené	rušený	k2eAgNnSc4d1
ministerstvo	ministerstvo	k1gNnSc4
průmyslu	průmysl	k1gInSc2
a	a	k8xC
ministerstvo	ministerstvo	k1gNnSc1
obchodu	obchod	k1gInSc2
a	a	k8xC
cestovního	cestovní	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
(	(	kIx(
<g/>
cestovní	cestovní	k2eAgInSc1d1
ruch	ruch	k1gInSc1
převzalo	převzít	k5eAaPmAgNnS
zároveň	zároveň	k6eAd1
vzniklé	vzniklý	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
hospodářství	hospodářství	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
v	v	k7c6
dřívějších	dřívější	k2eAgFnPc6d1
dobách	doba	k1gFnPc6
existovaly	existovat	k5eAaImAgInP
rovněž	rovněž	k9
(	(	kIx(
<g/>
české	český	k2eAgNnSc1d1
i	i	k8xC
federální	federální	k2eAgNnSc1d1
<g/>
)	)	kIx)
ministerstvo	ministerstvo	k1gNnSc1
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
ministerstvo	ministerstvo	k1gNnSc4
plánování	plánování	k1gNnSc2
a	a	k8xC
ministerstva	ministerstvo	k1gNnSc2
jednotlivých	jednotlivý	k2eAgInPc2d1
průmyslových	průmyslový	k2eAgInPc2d1
podoborů	podobor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Pravé	pravý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
MPO	MPO	kA
z	z	k7c2
Letné	Letná	k1gFnSc2
<g/>
;	;	kIx,
vlevo	vlevo	k6eAd1
za	za	k7c7
parčíkem	parčík	k1gInSc7
žluté	žlutý	k2eAgFnSc2d1
min	min	kA
<g/>
.	.	kIx.
dopravy	doprava	k1gFnSc2
</s>
<s>
Sloučení	sloučení	k1gNnSc1
s	s	k7c7
ministerstvem	ministerstvo	k1gNnSc7
dopravy	doprava	k1gFnSc2
</s>
<s>
Nejméně	málo	k6eAd3
od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
přetrvávají	přetrvávat	k5eAaImIp3nP
snahy	snaha	k1gFnPc1
o	o	k7c4
sloučení	sloučení	k1gNnSc4
ministerstva	ministerstvo	k1gNnSc2
dopravy	doprava	k1gFnSc2
s	s	k7c7
ministerstvem	ministerstvo	k1gNnSc7
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
návrhem	návrh	k1gInSc7
na	na	k7c4
sloučení	sloučení	k1gNnSc4
přišla	přijít	k5eAaPmAgFnS
předsedkyně	předsedkyně	k1gFnSc1
strany	strana	k1gFnSc2
LIDEM	lid	k1gInSc7
a	a	k8xC
vicepremiérka	vicepremiérka	k1gFnSc1
Nečasovy	Nečasův	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
Karolína	Karolína	k1gFnSc1
Peake	Peake	k1gFnSc1
v	v	k7c6
říjnu	říjen	k1gInSc6
2012	#num#	k4
o	o	k7c6
odůvodněním	odůvodnění	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
činnost	činnost	k1gFnSc1
obou	dva	k4xCgNnPc2
ministerstev	ministerstvo	k1gNnPc2
jde	jít	k5eAaImIp3nS
ruku	ruka	k1gFnSc4
v	v	k7c6
ruce	ruka	k1gFnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
úsporu	úspora	k1gFnSc4
odhadla	odhadnout	k5eAaPmAgFnS
na	na	k7c4
až	až	k6eAd1
sto	sto	k4xCgNnSc4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
ročně	ročně	k6eAd1
<g/>
,	,	kIx,
hlavně	hlavně	k9
na	na	k7c6
podpůrných	podpůrný	k2eAgFnPc6d1
činnostech	činnost	k1gFnPc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
účetnictví	účetnictví	k1gNnSc1
<g/>
,	,	kIx,
personalistika	personalistika	k1gFnSc1
<g/>
,	,	kIx,
IT	IT	kA
systémy	systém	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
náměstek	náměstek	k1gMnSc1
ministra	ministr	k1gMnSc2
financí	finance	k1gFnPc2
Ladislav	Ladislav	k1gMnSc1
Minčič	Minčič	k1gMnSc1
(	(	kIx(
<g/>
ODS	ODS	kA
<g/>
)	)	kIx)
tehdy	tehdy	k6eAd1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
krok	krok	k1gInSc4
správným	správný	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
,	,	kIx,
ministr	ministr	k1gMnSc1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
Martin	Martin	k1gMnSc1
Kuba	Kuba	k1gMnSc1
(	(	kIx(
<g/>
ODS	ODS	kA
<g/>
)	)	kIx)
označil	označit	k5eAaPmAgMnS
plán	plán	k1gInSc4
za	za	k7c4
„	„	k?
<g/>
v	v	k7c6
této	tento	k3xDgFnSc6
chvíli	chvíle	k1gFnSc6
za	za	k7c4
víceméně	víceméně	k9
nerealizovatelný	realizovatelný	k2eNgInSc4d1
<g/>
“	“	k?
a	a	k8xC
odmítl	odmítnout	k5eAaPmAgMnS
stanout	stanout	k5eAaPmF
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
čele	čelo	k1gNnSc6
sloučeného	sloučený	k2eAgNnSc2d1
ministerstva	ministerstvo	k1gNnSc2
<g/>
,	,	kIx,
ministr	ministr	k1gMnSc1
dopravy	doprava	k1gFnSc2
Pavel	Pavel	k1gMnSc1
Dobeš	Dobeš	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
práce	práce	k1gFnSc2
na	na	k7c4
sloučení	sloučení	k1gNnSc4
těchto	tento	k3xDgInPc2
dvou	dva	k4xCgNnPc2
ministerstev	ministerstvo	k1gNnPc2
by	by	kYmCp3nS
opravdu	opravdu	k9
nebyla	být	k5eNaImAgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
týdnů	týden	k1gInPc2
ani	ani	k8xC
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
poměrně	poměrně	k6eAd1
dlouhého	dlouhý	k2eAgNnSc2d1
časového	časový	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
Ministr	ministr	k1gMnSc1
bez	bez	k7c2
portfeje	portfej	k1gInSc2
Petr	Petr	k1gMnSc1
Mlsna	Mlsno	k1gNnSc2
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
toto	tento	k3xDgNnSc4
sloučení	sloučení	k1gNnSc4
navrhl	navrhnout	k5eAaPmAgMnS
v	v	k7c6
lednu	leden	k1gInSc6
2013	#num#	k4
v	v	k7c6
rámci	rámec	k1gInSc6
balíčku	balíček	k1gInSc2
úsporných	úsporný	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
a	a	k8xC
vláda	vláda	k1gFnSc1
Petra	Petr	k1gMnSc2
Nečase	Nečas	k1gMnSc2
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2013	#num#	k4
návrh	návrh	k1gInSc1
schválila	schválit	k5eAaPmAgFnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
sloučení	sloučení	k1gNnSc4
má	mít	k5eAaImIp3nS
přinést	přinést	k5eAaPmF
úsporu	úspora	k1gFnSc4
8,6	8,6	k4
miliard	miliarda	k4xCgFnPc2
Kč	Kč	kA
a	a	k8xC
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
proveden	provést	k5eAaPmNgInS
do	do	k7c2
konce	konec	k1gInSc2
volebního	volební	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
dřívějších	dřívější	k2eAgFnPc2d1
informací	informace	k1gFnPc2
se	se	k3xPyFc4
sloučené	sloučený	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
mělo	mít	k5eAaImAgNnS
vrátit	vrátit	k5eAaPmF
k	k	k7c3
názvu	název	k1gInSc3
„	„	k?
<g/>
ministerstvo	ministerstvo	k1gNnSc1
hospodářství	hospodářství	k1gNnSc2
<g/>
“	“	k?
a	a	k8xC
vést	vést	k5eAaImF
jej	on	k3xPp3gNnSc4
měl	mít	k5eAaImAgMnS
dosavadní	dosavadní	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Pavel	Pavel	k1gMnSc1
Švagr	švagr	k1gMnSc1
o	o	k7c4
týden	týden	k1gInSc4
později	pozdě	k6eAd2
<g />
.	.	kIx.
</s>
<s hack="1">
publikoval	publikovat	k5eAaBmAgMnS
článek	článek	k1gInSc4
s	s	k7c7
historickým	historický	k2eAgInSc7d1
exkurzem	exkurz	k1gInSc7
a	a	k8xC
celoevropským	celoevropský	k2eAgNnSc7d1
porovnáním	porovnání	k1gNnSc7
<g/>
,	,	kIx,
se	s	k7c7
závěrem	závěr	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
agendy	agenda	k1gFnPc4
obou	dva	k4xCgInPc2
ministerstev	ministerstvo	k1gNnPc2
nemají	mít	k5eNaImIp3nP
mnoho	mnoho	k4c4
průsečíků	průsečík	k1gInPc2
<g/>
,	,	kIx,
jediným	jediný	k2eAgInSc7d1
hlavním	hlavní	k2eAgInSc7d1
společným	společný	k2eAgInSc7d1
jmenovatelem	jmenovatel	k1gInSc7
je	být	k5eAaImIp3nS
stavebnictví	stavebnictví	k1gNnSc1
<g/>
,	,	kIx,
respektive	respektive	k9
dopravní	dopravní	k2eAgNnPc1d1
stavebnictví	stavebnictví	k1gNnPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
však	však	k9
nelze	lze	k6eNd1
čekat	čekat	k5eAaImF
synergický	synergický	k2eAgInSc4d1
efekt	efekt	k1gInSc4
plynoucí	plynoucí	k2eAgInPc4d1
ze	z	k7c2
sloučení	sloučení	k1gNnSc2
obou	dva	k4xCgInPc2
resortů	resort	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
rozsah	rozsah	k1gInSc1
působnosti	působnost	k1gFnSc2
obou	dva	k4xCgInPc2
resortů	resort	k1gInPc2
je	být	k5eAaImIp3nS
natolik	natolik	k6eAd1
široký	široký	k2eAgInSc1d1
<g/>
,	,	kIx,
že	že	k8xS
požadavek	požadavek	k1gInSc1
na	na	k7c4
samostatné	samostatný	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
je	být	k5eAaImIp3nS
určitě	určitě	k6eAd1
namístě	namístě	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připomenul	připomenout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
18	#num#	k4
<g/>
stránkový	stránkový	k2eAgInSc4d1
návrh	návrh	k1gInSc4
1	#num#	k4
<g/>
.	.	kIx.
etapy	etapa	k1gFnSc2
úsporných	úsporný	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
neobsahoval	obsahovat	k5eNaImAgInS
žádný	žádný	k3yNgInSc4
konkrétní	konkrétní	k2eAgInSc4d1
postup	postup	k1gInSc4
<g/>
,	,	kIx,
harmonogram	harmonogram	k1gInSc4
nebo	nebo	k8xC
vyčíslené	vyčíslený	k2eAgInPc4d1
očekávané	očekávaný	k2eAgInPc4d1
úspory	úspor	k1gInPc4
ani	ani	k8xC
žádnou	žádný	k3yNgFnSc4
analýzu	analýza	k1gFnSc4
potenciálních	potenciální	k2eAgFnPc2d1
duplicit	duplicita	k1gFnPc2
či	či	k8xC
průsečíků	průsečík	k1gInPc2
agend	agenda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
záměru	záměr	k1gInSc3
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
premiér	premiér	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
lednu	leden	k1gInSc6
2020	#num#	k4
navrhl	navrhnout	k5eAaPmAgMnS
odvolání	odvolání	k1gNnSc4
ministra	ministr	k1gMnSc2
dopravy	doprava	k1gFnSc2
Vladimíra	Vladimír	k1gMnSc2
Kremlíka	Kremlík	k1gMnSc2
kvůli	kvůli	k7c3
předražené	předražený	k2eAgFnSc3d1
zakázce	zakázka	k1gFnSc3
na	na	k7c4
elektronické	elektronický	k2eAgFnPc4d1
dálniční	dálniční	k2eAgFnPc4d1
známky	známka	k1gFnPc4
a	a	k8xC
současně	současně	k6eAd1
navrhl	navrhnout	k5eAaPmAgMnS
vedením	vedení	k1gNnSc7
resortu	resort	k1gInSc2
dopravy	doprava	k1gFnSc2
trvale	trvale	k6eAd1
pověřit	pověřit	k5eAaPmF
vicepremiéra	vicepremiér	k1gMnSc4
<g/>
,	,	kIx,
ministra	ministr	k1gMnSc4
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
Karla	Karel	k1gMnSc2
Havlíčka	Havlíček	k1gMnSc2
<g/>
,	,	kIx,
s	s	k7c7
čímž	což	k3yQnSc7,k3yRnSc7
Miloš	Miloš	k1gMnSc1
Zeman	Zeman	k1gMnSc1
souhlasil	souhlasit	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mluvčí	mluvčí	k1gMnSc1
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
Miroslav	Miroslav	k1gMnSc1
Diro	Diro	k1gMnSc1
vyjádřil	vyjádřit	k5eAaPmAgMnS
obavu	obava	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
v	v	k7c6
takto	takto	k6eAd1
krátkém	krátký	k2eAgInSc6d1
čase	čas	k1gInSc6
bude	být	k5eAaImBp3nS
vůbec	vůbec	k9
možné	možný	k2eAgNnSc1d1
v	v	k7c6
jedné	jeden	k4xCgFnSc6
osobě	osoba	k1gFnSc6
dva	dva	k4xCgInPc4
resorty	resort	k1gInPc4
uřídit	uřídit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dlouhodobého	dlouhodobý	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
myšlenka	myšlenka	k1gFnSc1
sloučení	sloučení	k1gNnSc2
ministerstev	ministerstvo	k1gNnPc2
není	být	k5eNaImIp3nS
úplně	úplně	k6eAd1
nesmyslná	smyslný	k2eNgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
k	k	k7c3
tomu	ten	k3xDgNnSc3
je	být	k5eAaImIp3nS
potřeba	potřeba	k1gFnSc1
realizovat	realizovat	k5eAaBmF
mnoho	mnoho	k4c4
legislativních	legislativní	k2eAgInPc2d1
i	i	k8xC
procesně	procesně	k6eAd1
organizačních	organizační	k2eAgInPc2d1
kroků	krok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výkonný	výkonný	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Svazu	svaz	k1gInSc2
dopravy	doprava	k1gFnSc2
ČR	ČR	kA
Petr	Petr	k1gMnSc1
Kašík	Kašík	k1gMnSc1
připomenul	připomenout	k5eAaPmAgMnS
nestabilitu	nestabilita	k1gFnSc4
resortu	resort	k1gInSc2
dopravy	doprava	k1gFnSc2
a	a	k8xC
časté	častý	k2eAgNnSc1d1
střídání	střídání	k1gNnSc1
ministrů	ministr	k1gMnPc2
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
další	další	k2eAgFnSc1d1
změna	změna	k1gFnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
tohoto	tento	k3xDgInSc2
resortu	resort	k1gInSc2
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
katastrofou	katastrofa	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Havlíček	Havlíček	k1gMnSc1
bohužel	bohužel	k9
nemůže	moct	k5eNaImIp3nS
mít	mít	k5eAaImF
žádný	žádný	k3yNgInSc4
čas	čas	k1gInSc4
hájení	hájení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
by	by	kYmCp3nP
snad	snad	k9
ke	k	k7c3
sloučení	sloučení	k1gNnSc3
mělo	mít	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
<g/>
,	,	kIx,
nemůže	moct	k5eNaImIp3nS
se	se	k3xPyFc4
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
jednat	jednat	k5eAaImF
pouze	pouze	k6eAd1
o	o	k7c4
sloučení	sloučení	k1gNnSc4
vrcholných	vrcholný	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
koncepční	koncepční	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
rozhodovacích	rozhodovací	k2eAgInPc2d1
procesů	proces	k1gInPc2
včetně	včetně	k7c2
revize	revize	k1gFnSc2
odborných	odborný	k2eAgFnPc2d1
agend	agenda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generální	generální	k2eAgInSc1d1
tajemník	tajemník	k1gInSc1
Sdružení	sdružení	k1gNnSc2
automobilových	automobilový	k2eAgMnPc2d1
dopravců	dopravce	k1gMnPc2
Česmad	Česmad	k1gInSc1
Bohemia	bohemia	k1gFnSc1
Vojtěch	Vojtěch	k1gMnSc1
Hromíř	Hromíř	k1gMnSc1
konstatoval	konstatovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
případné	případný	k2eAgInPc1d1
kroky	krok	k1gInPc1
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
slučování	slučování	k1gNnSc4
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
dobře	dobře	k6eAd1
zváženy	zvážen	k2eAgFnPc4d1
a	a	k8xC
případně	případně	k6eAd1
zredukována	zredukován	k2eAgFnSc1d1
současná	současný	k2eAgFnSc1d1
agenda	agenda	k1gFnSc1
každého	každý	k3xTgInSc2
z	z	k7c2
nich	on	k3xPp3gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Programový	programový	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Svazu	svaz	k1gInSc2
moderní	moderní	k2eAgFnSc2d1
energetiky	energetika	k1gFnSc2
Martin	Martin	k1gMnSc1
Sedlák	Sedlák	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
spojením	spojení	k1gNnSc7
dopravy	doprava	k1gFnSc2
a	a	k8xC
průmyslu	průmysl	k1gInSc2
může	moct	k5eAaImIp3nS
vzniknout	vzniknout	k5eAaPmF
zajímavý	zajímavý	k2eAgInSc1d1
celek	celek	k1gInSc1
ministerstva	ministerstvo	k1gNnSc2
infrastruktury	infrastruktura	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Den	den	k1gInSc1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
premiér	premiér	k1gMnSc1
Babiš	Babiš	k1gMnSc1
hovořil	hovořit	k5eAaImAgMnS
o	o	k7c4
spojení	spojení	k1gNnSc4
ministerstev	ministerstvo	k1gNnPc2
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
dopravy	doprava	k1gFnSc2
a	a	k8xC
pro	pro	k7c4
místní	místní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
nebo	nebo	k8xC
ministerstva	ministerstvo	k1gNnSc2
práce	práce	k1gFnSc2
a	a	k8xC
sociálních	sociální	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
s	s	k7c7
ministerstvem	ministerstvo	k1gNnSc7
zdravotnictví	zdravotnictví	k1gNnSc2
<g/>
,	,	kIx,
sice	sice	k8xC
potvrdil	potvrdit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
dvě	dva	k4xCgNnPc4
ministerstva	ministerstvo	k1gNnPc4
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
volebního	volební	k2eAgNnSc2d1
období	období	k1gNnSc2
jeden	jeden	k4xCgMnSc1
ministr	ministr	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Nikdo	nikdo	k3yNnSc1
nejde	jít	k5eNaImIp3nS
ta	ten	k3xDgNnPc4
ministerstva	ministerstvo	k1gNnPc4
slučovat	slučovat	k5eAaImF
<g/>
,	,	kIx,
ale	ale	k8xC
to	ten	k3xDgNnSc1
řízení	řízení	k1gNnSc1
bude	být	k5eAaImBp3nS
takto	takto	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
myslím	myslet	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
dobře	dobře	k6eAd1
<g/>
.	.	kIx.
<g/>
“	“	k?
Proti	proti	k7c3
slučování	slučování	k1gNnSc3
ministerstev	ministerstvo	k1gNnPc2
se	se	k3xPyFc4
mezitím	mezitím	k6eAd1
postavila	postavit	k5eAaPmAgFnS
jak	jak	k6eAd1
koaliční	koaliční	k2eAgFnSc4d1
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
tak	tak	k6eAd1
KSČM	KSČM	kA
<g/>
,	,	kIx,
na	na	k7c6
jejíž	jejíž	k3xOyRp3gFnSc6
podpoře	podpora	k1gFnSc6
vláda	vláda	k1gFnSc1
stojí	stát	k5eAaImIp3nS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
předseda	předseda	k1gMnSc1
KSČM	KSČM	kA
Vojtěch	Vojtěch	k1gMnSc1
Filip	Filip	k1gMnSc1
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
pověření	pověření	k1gNnSc4
Havlíčka	Havlíček	k1gMnSc2
vedením	vedení	k1gNnSc7
i	i	k9
ministerstva	ministerstvo	k1gNnSc2
dopravy	doprava	k1gFnSc2
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
dočasné	dočasný	k2eAgNnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oblasti	oblast	k1gFnPc1
působnosti	působnost	k1gFnSc2
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
je	být	k5eAaImIp3nS
ústředním	ústřední	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
pro	pro	k7c4
</s>
<s>
státní	státní	k2eAgFnSc4d1
průmyslovou	průmyslový	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
,	,	kIx,
obchodní	obchodní	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
,	,	kIx,
zahraničně	zahraničně	k6eAd1
ekonomickou	ekonomický	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
,	,	kIx,
tvorbu	tvorba	k1gFnSc4
jednotné	jednotný	k2eAgFnSc2d1
surovinové	surovinový	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
,	,	kIx,
využívání	využívání	k1gNnSc1
nerostného	nerostný	k2eAgNnSc2d1
bohatství	bohatství	k1gNnSc2
<g/>
,	,	kIx,
energetiku	energetika	k1gFnSc4
<g/>
,	,	kIx,
teplárenství	teplárenství	k1gNnSc4
<g/>
,	,	kIx,
plynárenství	plynárenství	k1gNnSc4
<g/>
,	,	kIx,
těžbu	těžba	k1gFnSc4
<g/>
,	,	kIx,
úpravu	úprava	k1gFnSc4
a	a	k8xC
zušlechťování	zušlechťování	k1gNnSc4
ropy	ropa	k1gFnSc2
a	a	k8xC
zemního	zemní	k2eAgInSc2d1
plynu	plyn	k1gInSc2
<g/>
,	,	kIx,
tuhých	tuhý	k2eAgNnPc2d1
paliv	palivo	k1gNnPc2
<g/>
,	,	kIx,
radioaktivních	radioaktivní	k2eAgFnPc2d1
surovin	surovina	k1gFnPc2
<g/>
,	,	kIx,
rud	ruda	k1gFnPc2
a	a	k8xC
nerud	neruda	k1gFnPc2
</s>
<s>
hutnictví	hutnictví	k1gNnSc1
<g/>
,	,	kIx,
strojírenství	strojírenství	k1gNnSc2
<g/>
,	,	kIx,
elektrotechniku	elektrotechnika	k1gFnSc4
a	a	k8xC
elektroniku	elektronika	k1gFnSc4
<g/>
,	,	kIx,
průmysl	průmysl	k1gInSc4
chemický	chemický	k2eAgInSc4d1
a	a	k8xC
zpracování	zpracování	k1gNnSc4
ropy	ropa	k1gFnSc2
<g/>
,	,	kIx,
gumárenský	gumárenský	k2eAgMnSc1d1
a	a	k8xC
plastikářský	plastikářský	k2eAgMnSc1d1
<g/>
,	,	kIx,
skla	sklo	k1gNnSc2
a	a	k8xC
keramiky	keramika	k1gFnSc2
<g/>
,	,	kIx,
textilní	textilní	k2eAgFnSc2d1
a	a	k8xC
oděvní	oděvní	k2eAgFnSc2d1
<g/>
,	,	kIx,
kožedělný	kožedělný	k2eAgMnSc1d1
a	a	k8xC
polygrafický	polygrafický	k2eAgInSc1d1
<g/>
,	,	kIx,
papíru	papír	k1gInSc2
a	a	k8xC
celulózy	celulóza	k1gFnSc2
a	a	k8xC
dřevozpracující	dřevozpracující	k2eAgFnSc2d1
</s>
<s>
výrobu	výroba	k1gFnSc4
stavebních	stavební	k2eAgFnPc2d1
hmot	hmota	k1gFnPc2
<g/>
,	,	kIx,
stavební	stavební	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
<g/>
,	,	kIx,
zdravotnickou	zdravotnický	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
<g/>
,	,	kIx,
sběrné	sběrný	k2eAgFnPc1d1
suroviny	surovina	k1gFnPc1
a	a	k8xC
kovový	kovový	k2eAgInSc1d1
odpad	odpad	k1gInSc1
</s>
<s>
vnitřní	vnitřní	k2eAgInSc4d1
obchod	obchod	k1gInSc4
a	a	k8xC
ochranu	ochrana	k1gFnSc4
zájmů	zájem	k1gInPc2
spotřebitelů	spotřebitel	k1gMnPc2
<g/>
,	,	kIx,
zahraniční	zahraniční	k2eAgInSc4d1
obchod	obchod	k1gInSc4
a	a	k8xC
podporu	podpora	k1gFnSc4
exportu	export	k1gInSc2
</s>
<s>
věci	věc	k1gFnPc1
malých	malý	k2eAgInPc2d1
a	a	k8xC
středních	střední	k2eAgInPc2d1
podniků	podnik	k1gInPc2
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
věci	věc	k1gFnPc1
živností	živnost	k1gFnPc2
</s>
<s>
technickou	technický	k2eAgFnSc4d1
normalizaci	normalizace	k1gFnSc4
<g/>
,	,	kIx,
metrologii	metrologie	k1gFnSc4
a	a	k8xC
státní	státní	k2eAgNnSc4d1
zkušebnictví	zkušebnictví	k1gNnSc4
</s>
<s>
průmyslový	průmyslový	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
<g/>
,	,	kIx,
rozvoj	rozvoj	k1gInSc4
techniky	technika	k1gFnSc2
a	a	k8xC
technologií	technologie	k1gFnPc2
</s>
<s>
věci	věc	k1gFnPc1
komoditních	komoditní	k2eAgFnPc2d1
burz	burza	k1gFnPc2
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
věcí	věc	k1gFnPc2
náležejících	náležející	k2eAgFnPc2d1
do	do	k7c2
působnosti	působnost	k1gFnSc2
Ministerstva	ministerstvo	k1gNnSc2
zemědělství	zemědělství	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
</s>
<s>
koordinuje	koordinovat	k5eAaBmIp3nS
zahraničně	zahraničně	k6eAd1
obchodní	obchodní	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
jednotlivým	jednotlivý	k2eAgInPc3d1
státům	stát	k1gInPc3
</s>
<s>
zabezpečuje	zabezpečovat	k5eAaImIp3nS
sjednávání	sjednávání	k1gNnSc1
dvoustranných	dvoustranný	k2eAgFnPc2d1
a	a	k8xC
mnohostranných	mnohostranný	k2eAgFnPc2d1
obchodních	obchodní	k2eAgFnPc2d1
a	a	k8xC
ekonomických	ekonomický	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
včetně	včetně	k7c2
komoditních	komoditní	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
</s>
<s>
realizuje	realizovat	k5eAaBmIp3nS
obchodní	obchodní	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
s	s	k7c7
Evropskými	evropský	k2eAgNnPc7d1
společenstvími	společenství	k1gNnPc7
<g/>
,	,	kIx,
Evropským	evropský	k2eAgNnSc7d1
sdružením	sdružení	k1gNnSc7
volného	volný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
a	a	k8xC
jinými	jiný	k2eAgFnPc7d1
mezinárodními	mezinárodní	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
a	a	k8xC
integračními	integrační	k2eAgNnPc7d1
seskupeními	seskupení	k1gNnPc7
</s>
<s>
řídí	řídit	k5eAaImIp3nS
a	a	k8xC
vykonává	vykonávat	k5eAaImIp3nS
činnosti	činnost	k1gFnPc4
spojené	spojený	k2eAgFnPc4d1
s	s	k7c7
uplatňováním	uplatňování	k1gNnSc7
licenčního	licenční	k2eAgInSc2d1
režimu	režim	k1gInSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
hospodářských	hospodářský	k2eAgInPc2d1
styků	styk	k1gInPc2
se	se	k3xPyFc4
zahraničím	zahraničit	k5eAaPmIp1nS
</s>
<s>
posuzuje	posuzovat	k5eAaImIp3nS
dovoz	dovoz	k1gInSc4
dumpingových	dumpingový	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
a	a	k8xC
přijímá	přijímat	k5eAaImIp3nS
opatření	opatření	k1gNnSc1
na	na	k7c4
ochranu	ochrana	k1gFnSc4
proti	proti	k7c3
dovozu	dovoz	k1gInSc3
těchto	tento	k3xDgInPc2
výrobků	výrobek	k1gInPc2
</s>
<s>
řídí	řídit	k5eAaImIp3nS
puncovnictví	puncovnictví	k1gNnSc4
a	a	k8xC
zkoušení	zkoušení	k1gNnSc4
drahých	drahý	k2eAgInPc2d1
kovů	kov	k1gInPc2
</s>
<s>
je	on	k3xPp3gFnPc4
nadřízené	nadřízená	k1gFnPc4
České	český	k2eAgFnSc3d1
energetické	energetický	k2eAgFnSc3d1
inspekci	inspekce	k1gFnSc3
<g/>
,	,	kIx,
České	český	k2eAgFnSc3d1
obchodní	obchodní	k2eAgFnSc3d1
inspekci	inspekce	k1gFnSc3
<g/>
,	,	kIx,
Puncovnímu	puncovní	k2eAgInSc3d1
úřadu	úřad	k1gInSc3
a	a	k8xC
Licenčnímu	licenční	k2eAgInSc3d1
úřadu	úřad	k1gInSc3
</s>
<s>
Působení	působení	k1gNnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
metrologie	metrologie	k1gFnSc2
</s>
<s>
řídí	řídit	k5eAaImIp3nS
státní	státní	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
a	a	k8xC
vypracovává	vypracovávat	k5eAaImIp3nS
koncepci	koncepce	k1gFnSc4
rozvoje	rozvoj	k1gInSc2
metrologie	metrologie	k1gFnSc2
</s>
<s>
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
předpisy	předpis	k1gInPc4
</s>
<s>
řídí	řídit	k5eAaImIp3nS
Úřad	úřad	k1gInSc1
pro	pro	k7c4
technickou	technický	k2eAgFnSc4d1
normalizaci	normalizace	k1gFnSc4
<g/>
,	,	kIx,
metrologii	metrologie	k1gFnSc4
a	a	k8xC
státní	státní	k2eAgNnSc4d1
zkušebnictví	zkušebnictví	k1gNnSc4
a	a	k8xC
Český	český	k2eAgInSc4d1
metrologický	metrologický	k2eAgInSc4d1
institut	institut	k1gInSc4
</s>
<s>
zabezpečuje	zabezpečovat	k5eAaImIp3nS
účast	účast	k1gFnSc4
ČR	ČR	kA
v	v	k7c6
mezinárodních	mezinárodní	k2eAgInPc6d1
metrologických	metrologický	k2eAgInPc6d1
orgánech	orgán	k1gInPc6
a	a	k8xC
zajišťuje	zajišťovat	k5eAaImIp3nS
nebo	nebo	k8xC
pověřuje	pověřovat	k5eAaImIp3nS
Úřad	úřad	k1gInSc1
pro	pro	k7c4
technickou	technický	k2eAgFnSc4d1
normalizaci	normalizace	k1gFnSc4
<g/>
,	,	kIx,
metrologii	metrologie	k1gFnSc4
a	a	k8xC
státní	státní	k2eAgNnSc4d1
zkušebnictví	zkušebnictví	k1gNnSc4
nebo	nebo	k8xC
Český	český	k2eAgInSc4d1
metrologický	metrologický	k2eAgInSc4d1
institut	institut	k1gInSc4
zabezpečováním	zabezpečování	k1gNnSc7
úkolů	úkol	k1gInPc2
plynoucích	plynoucí	k2eAgFnPc2d1
z	z	k7c2
tohoto	tento	k3xDgNnSc2
členství	členství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
rozhoduje	rozhodovat	k5eAaImIp3nS
o	o	k7c6
opravných	opravný	k2eAgInPc6d1
prostředcích	prostředek	k1gInPc6
proti	proti	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
Úřadu	úřad	k1gInSc2
pro	pro	k7c4
technickou	technický	k2eAgFnSc4d1
normalizaci	normalizace	k1gFnSc4
<g/>
,	,	kIx,
metrologii	metrologie	k1gFnSc4
a	a	k8xC
státní	státní	k2eAgNnSc4d1
zkušebnictví	zkušebnictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Pravé	pravý	k2eAgFnSc3d1
(	(	kIx(
<g/>
západní	západní	k2eAgFnSc3d1
<g/>
)	)	kIx)
křídlo	křídlo	k1gNnSc1
MPO	MPO	kA
s	s	k7c7
rohovými	rohový	k2eAgFnPc7d1
vížkami	vížka	k1gFnPc7
a	a	k8xC
zdobným	zdobný	k2eAgInSc7d1
arkýřem	arkýř	k1gInSc7
ze	z	k7c2
zahrad	zahrada	k1gFnPc2
Anežského	anežský	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
</s>
<s>
Bludný	bludný	k2eAgInSc1d1
balvan	balvan	k1gInSc1
</s>
<s>
Český	český	k2eAgInSc4d1
klub	klub	k1gInSc4
skeptiků	skeptik	k1gMnPc2
Sisyfos	Sisyfos	k1gMnSc1
ministerstvu	ministerstvo	k1gNnSc6
udělil	udělit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
anticenu	anticen	k2eAgFnSc4d1
zlatý	zlatý	k2eAgInSc1d1
Bludný	bludný	k2eAgInSc1d1
balvan	balvan	k1gInSc1
za	za	k7c4
„	„	k?
<g/>
podporu	podpora	k1gFnSc4
a	a	k8xC
financování	financování	k1gNnSc4
‚	‚	k?
<g/>
Zařízení	zařízení	k1gNnSc2
pro	pro	k7c4
usnadnění	usnadnění	k1gNnSc4
detekce	detekce	k1gFnSc2
osob	osoba	k1gFnPc2
za	za	k7c7
překážkou	překážka	k1gFnSc7
<g/>
‘	‘	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Levé	levý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
MPO	MPO	kA
z	z	k7c2
Revoluční	revoluční	k2eAgFnSc2d1
se	se	k3xPyFc4
sochařskou	sochařský	k2eAgFnSc7d1
výzdobou	výzdoba	k1gFnSc7
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
reprezentativní	reprezentativní	k2eAgFnSc6d1
budově	budova	k1gFnSc6
na	na	k7c6
adrese	adresa	k1gFnSc6
Praha	Praha	k1gFnSc1
1	#num#	k4
–	–	k?
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
Na	na	k7c6
Františku	františek	k1gInSc6
1039	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
(	(	kIx(
<g/>
s	s	k7c7
vedlejšími	vedlejší	k2eAgFnPc7d1
adresami	adresa	k1gFnPc7
Řásnovka	Řásnovka	k1gFnSc1
1039	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
,	,	kIx,
Revoluční	revoluční	k2eAgFnSc1d1
1039	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
<g/>
,	,	kIx,
Klášterská	klášterský	k2eAgFnSc1d1
1039	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
)	)	kIx)
u	u	k7c2
Dvořákova	Dvořákův	k2eAgNnSc2d1
nábřeží	nábřeží	k1gNnSc2
mezi	mezi	k7c7
Štefánikovým	Štefánikův	k2eAgInSc7d1
mostem	most	k1gInSc7
a	a	k8xC
Anežským	anežský	k2eAgInSc7d1
klášterem	klášter	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jednou	jednou	k6eAd1
z	z	k7c2
trojice	trojice	k1gFnSc2
reprezentativních	reprezentativní	k2eAgFnPc2d1
státních	státní	k2eAgFnPc2d1
budov	budova	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
nechala	nechat	k5eAaPmAgFnS
československá	československý	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
postavit	postavit	k5eAaPmF
na	na	k7c6
nově	nově	k6eAd1
regulovaném	regulovaný	k2eAgNnSc6d1
nábřeží	nábřeží	k1gNnSc6
Vltavy	Vltava	k1gFnSc2
<g/>
;	;	kIx,
další	další	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
ministerstvo	ministerstvo	k1gNnSc1
železnic	železnice	k1gFnPc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
dopravy	doprava	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
ministerstvo	ministerstvo	k1gNnSc1
zemědělství	zemědělství	k1gNnSc2
na	na	k7c6
Novém	nový	k2eAgNnSc6d1
Městě	město	k1gNnSc6
poblíž	poblíž	k7c2
Hlávkova	Hlávkův	k2eAgInSc2d1
mostu	most	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
výstavbu	výstavba	k1gFnSc4
budovy	budova	k1gFnSc2
paláce	palác	k1gInSc2
Ministerstva	ministerstvo	k1gNnSc2
průmyslu	průmysl	k1gInSc2
<g/>
,	,	kIx,
obchodu	obchod	k1gInSc2
a	a	k8xC
živností	živnost	k1gFnPc2
a	a	k8xC
Patentního	patentní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
byly	být	k5eAaImAgInP
zvoleny	zvolit	k5eAaPmNgInP
tehdy	tehdy	k6eAd1
velmi	velmi	k6eAd1
zanedbané	zanedbaný	k2eAgInPc4d1
pozemky	pozemek	k1gInPc4
v	v	k7c6
prostoru	prostor	k1gInSc6
vyústění	vyústění	k1gNnSc2
ulic	ulice	k1gFnPc2
Na	na	k7c4
Františku	Františka	k1gFnSc4
<g/>
,	,	kIx,
Klášterské	klášterský	k2eAgInPc4d1
a	a	k8xC
Plžové	Plžové	k?
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
odedávna	odedávna	k6eAd1
sloužily	sloužit	k5eAaImAgInP
průmyslu	průmysl	k1gInSc2
<g/>
,	,	kIx,
obchodu	obchod	k1gInSc2
a	a	k8xC
živnostem	živnost	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedaleko	daleko	k6eNd1
sídlila	sídlit	k5eAaImAgFnS
Pražská	pražský	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
komora	komora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstavba	výstavba	k1gFnSc1
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkový	celkový	k2eAgInSc1d1
rozpočet	rozpočet	k1gInSc1
činil	činit	k5eAaImAgInS
32,5	32,5	k4
miliónu	milión	k4xCgInSc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokončení	dokončení	k1gNnSc1
bylo	být	k5eAaImAgNnS
původně	původně	k6eAd1
plánováno	plánovat	k5eAaImNgNnS
na	na	k7c4
rok	rok	k1gInSc4
1931	#num#	k4
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
však	však	k9
kolaudace	kolaudace	k1gFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Architektem	architekt	k1gMnSc7
budovy	budova	k1gFnSc2
byl	být	k5eAaImAgMnS
Josef	Josef	k1gMnSc1
Fanta	Fanta	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
návrh	návrh	k1gInSc1
byl	být	k5eAaImAgInS
vybrán	vybrat	k5eAaPmNgInS
ve	v	k7c6
veřejné	veřejný	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
se	se	k3xPyFc4
účastnilo	účastnit	k5eAaImAgNnS
6	#num#	k4
architektů	architekt	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xC,k8xS
dodavatel	dodavatel	k1gMnSc1
byla	být	k5eAaImAgFnS
vybrána	vybrat	k5eAaPmNgFnS
stavební	stavební	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Antonína	Antonín	k1gMnSc2
Belady	Belada	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Architektura	architektura	k1gFnSc1
je	být	k5eAaImIp3nS
duchem	duch	k1gMnSc7
obdobná	obdobný	k2eAgNnPc1d1
stavbě	stavba	k1gFnSc6
prefekturálního	prefekturální	k2eAgInSc2d1
paláce	palác	k1gInSc2
pro	pro	k7c4
výstavy	výstava	k1gFnPc4
produktů	produkt	k1gInPc2
<g/>
,	,	kIx,
realizované	realizovaný	k2eAgNnSc1d1
v	v	k7c6
letech	let	k1gInPc6
1912	#num#	k4
<g/>
–	–	k?
<g/>
1919	#num#	k4
v	v	k7c6
japonské	japonský	k2eAgFnSc6d1
Hirošimě	Hirošima	k1gFnSc6
náchodským	náchodský	k2eAgMnSc7d1
rodákem	rodák	k1gMnSc7
Janem	Jan	k1gMnSc7
Letzelem	Letzel	k1gMnSc7
(	(	kIx(
<g/>
známé	známý	k2eAgFnSc2d1
jako	jako	k8xC,k8xS
tzv.	tzv.	kA
atomový	atomový	k2eAgInSc4d1
dóm	dóm	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Třípatrová	třípatrový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
je	být	k5eAaImIp3nS
postavena	postaven	k2eAgFnSc1d1
na	na	k7c6
půdorysu	půdorys	k1gInSc6
obdélníku	obdélník	k1gInSc2
o	o	k7c6
rozměrech	rozměr	k1gInPc6
107	#num#	k4
x	x	k?
49	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
samonosné	samonosný	k2eAgFnPc4d1
zdi	zeď	k1gFnPc4
a	a	k8xC
kamenné	kamenný	k2eAgFnPc4d1
fasády	fasáda	k1gFnPc4
ze	z	k7c2
žuly	žula	k1gFnSc2
a	a	k8xC
pískovce	pískovec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejde	jít	k5eNaImIp3nS
o	o	k7c4
typický	typický	k2eAgInSc4d1
obklad	obklad	k1gInSc4
<g/>
,	,	kIx,
kvádry	kvádr	k1gInPc4
žuly	žula	k1gFnSc2
a	a	k8xC
pískovce	pískovec	k1gInPc1
jsou	být	k5eAaImIp3nP
silné	silný	k2eAgNnSc4d1
25	#num#	k4
<g/>
–	–	k?
<g/>
35	#num#	k4
centimetrů	centimetr	k1gInPc2
a	a	k8xC
na	na	k7c4
ně	on	k3xPp3gMnPc4
navazuje	navazovat	k5eAaImIp3nS
cihlová	cihlový	k2eAgFnSc1d1
zeď	zeď	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žula	žula	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
většinou	většina	k1gFnSc7
z	z	k7c2
lokalit	lokalita	k1gFnPc2
Dolní	dolní	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
Tanvald	Tanvald	k1gInSc1
a	a	k8xC
Železná	železný	k2eAgFnSc1d1
Ruda	ruda	k1gFnSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
pískovce	pískovec	k1gInPc1
převážně	převážně	k6eAd1
z	z	k7c2
lomu	lom	k1gInSc2
u	u	k7c2
Lázní	lázeň	k1gFnPc2
Mšené	Mšená	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
<g/>
,	,	kIx,
severní	severní	k2eAgNnSc1d1
průčelí	průčelí	k1gNnSc1
s	s	k7c7
hlavním	hlavní	k2eAgInSc7d1
vchodem	vchod	k1gInSc7
směřuje	směřovat	k5eAaImIp3nS
k	k	k7c3
Vltavě	Vltava	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
je	být	k5eAaImIp3nS
ukončené	ukončený	k2eAgInPc1d1
čtyřbokými	čtyřboký	k2eAgFnPc7d1
věžemi	věž	k1gFnPc7
<g/>
,	,	kIx,
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
ose	osa	k1gFnSc6
je	být	k5eAaImIp3nS
členěno	členěn	k2eAgNnSc1d1
rizalitem	rizalit	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
prostor	prostor	k1gInSc4
vysokého	vysoký	k2eAgNnSc2d1
přízemí	přízemí	k1gNnSc2
a	a	k8xC
dvě	dva	k4xCgNnPc1
podlaží	podlaží	k1gNnPc1
hlavních	hlavní	k2eAgFnPc2d1
reprezentačních	reprezentační	k2eAgFnPc2d1
místností	místnost	k1gFnPc2
a	a	k8xC
vrcholí	vrcholit	k5eAaImIp3nS
balustrovou	balustrový	k2eAgFnSc7d1
atikou	atika	k1gFnSc7
s	s	k7c7
nadživotními	nadživotní	k2eAgFnPc7d1
figurálními	figurální	k2eAgFnPc7d1
plastikami	plastika	k1gFnPc7
symbolizujícími	symbolizující	k2eAgFnPc7d1
Průmysl	průmysl	k1gInSc1
<g/>
,	,	kIx,
Obchod	obchod	k1gInSc1
<g/>
,	,	kIx,
Řemesla	řemeslo	k1gNnPc4
a	a	k8xC
Plavbu	plavba	k1gFnSc4
<g/>
,	,	kIx,
nad	nad	k7c7
kterými	který	k3yIgInPc7,k3yQgInPc7,k3yRgInPc7
se	se	k3xPyFc4
vypíná	vypínat	k5eAaImIp3nS
dekorativní	dekorativní	k2eAgFnSc1d1
prosklená	prosklený	k2eAgFnSc1d1
kopule	kopule	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
stranách	strana	k1gFnPc6
jsou	být	k5eAaImIp3nP
mohutné	mohutný	k2eAgMnPc4d1
<g/>
,	,	kIx,
kulovitě	kulovitě	k6eAd1
ukončené	ukončený	k2eAgInPc4d1
pylony	pylon	k1gInPc4
<g/>
,	,	kIx,
nesoucí	nesoucí	k2eAgInPc4d1
čtyři	čtyři	k4xCgInPc4
reliéfy	reliéf	k1gInPc4
státních	státní	k2eAgInPc2d1
znaků	znak	k1gInPc2
Československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
západní	západní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
budovy	budova	k1gFnSc2
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
vchod	vchod	k1gInSc1
do	do	k7c2
Patentního	patentní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
ním	on	k3xPp3gNnSc7
je	být	k5eAaImIp3nS
v	v	k7c6
úrovni	úroveň	k1gFnSc6
prvního	první	k4xOgNnSc2
patra	patro	k1gNnSc2
balkón	balkón	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
zábradlí	zábradlí	k1gNnSc1
je	být	k5eAaImIp3nS
tvořeno	tvořit	k5eAaImNgNnS
pilířky	pilířek	k1gInPc7
<g/>
,	,	kIx,
nesoucími	nesoucí	k2eAgInPc7d1
čtyři	čtyři	k4xCgFnPc1
sochy	socha	k1gFnPc1
v	v	k7c6
nadživotní	nadživotní	k2eAgFnSc6d1
velikosti	velikost	k1gFnSc6
<g/>
,	,	kIx,
symbolizující	symbolizující	k2eAgFnSc4d1
Matematiku	matematika	k1gFnSc4
<g/>
,	,	kIx,
Fyziku	fyzika	k1gFnSc4
<g/>
,	,	kIx,
Chemii	chemie	k1gFnSc4
a	a	k8xC
Inženýrství	inženýrství	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
jižního	jižní	k2eAgNnSc2d1
průčelí	průčelí	k1gNnSc2
jsou	být	k5eAaImIp3nP
vchody	vchod	k1gInPc4
do	do	k7c2
dvou	dva	k4xCgInPc2
uzavřených	uzavřený	k2eAgInPc2d1
dvorů	dvůr	k1gInPc2
<g/>
,	,	kIx,
zvýrazněné	zvýrazněný	k2eAgInPc1d1
portály	portál	k1gInPc1
se	s	k7c7
sochami	socha	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
Střední	střední	k2eAgFnSc1d1
část	část	k1gFnSc1
východního	východní	k2eAgNnSc2d1
průčelí	průčelí	k1gNnSc2
nad	nad	k7c4
sloupy	sloup	k1gInPc4
zvýrazňují	zvýrazňovat	k5eAaImIp3nP
figurální	figurální	k2eAgFnPc4d1
alegorické	alegorický	k2eAgFnPc4d1
plastiky	plastika	k1gFnPc4
Podnikavost	podnikavost	k1gFnSc1
<g/>
,	,	kIx,
Vynalézavost	vynalézavost	k1gFnSc1
<g/>
,	,	kIx,
Vytrvalost	vytrvalost	k1gFnSc1
a	a	k8xC
Pravdivost	pravdivost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ležící	ležící	k2eAgFnPc4d1
sochy	socha	k1gFnPc4
s	s	k7c7
dětmi	dítě	k1gFnPc7
po	po	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
průčelí	průčelí	k1gNnSc2
představují	představovat	k5eAaImIp3nP
Obchod	obchod	k1gInSc1
a	a	k8xC
Průmysl	průmysl	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
celé	celý	k2eAgFnSc6d1
budově	budova	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
přes	přes	k7c4
sto	sto	k4xCgNnSc4
dvacet	dvacet	k4xCc4
soch	socha	k1gFnPc2
<g/>
,	,	kIx,
doplněných	doplněný	k2eAgNnPc2d1
množstvím	množství	k1gNnSc7
sgrafit	sgrafito	k1gNnPc2
<g/>
,	,	kIx,
ornamentů	ornament	k1gInPc2
<g/>
,	,	kIx,
kovových	kovový	k2eAgFnPc2d1
mříží	mříž	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
ozdobných	ozdobný	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celek	k1gInSc7
se	se	k3xPyFc4
na	na	k7c6
této	tento	k3xDgFnSc6
výzdobě	výzdoba	k1gFnSc6
podílelo	podílet	k5eAaImAgNnS
čtrnáct	čtrnáct	k4xCc1
umělců	umělec	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
nejznámější	známý	k2eAgMnPc4d3
jsou	být	k5eAaImIp3nP
Josef	Josef	k1gMnSc1
A.	A.	kA
Paukert	Paukert	k1gMnSc1
a	a	k8xC
Čeněk	Čeněk	k1gMnSc1
Vosmík	Vosmík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
původních	původní	k2eAgInPc2d1
sádrových	sádrový	k2eAgInPc2d1
modelů	model	k1gInPc2
soch	socha	k1gFnPc2
v	v	k7c6
měřítku	měřítko	k1gNnSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
,	,	kIx,
určených	určený	k2eAgFnPc6d1
ke	k	k7c3
schválení	schválení	k1gNnSc6
stavebním	stavební	k2eAgInSc7d1
výborem	výbor	k1gInSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dosud	dosud	k6eAd1
uchována	uchovat	k5eAaPmNgFnS
na	na	k7c6
půdě	půda	k1gFnSc6
budovy	budova	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
oprava	oprava	k1gFnSc1
celé	celý	k2eAgFnSc2d1
fasády	fasáda	k1gFnSc2
objektu	objekt	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c7
hlavním	hlavní	k2eAgInSc7d1
vchodem	vchod	k1gInSc7
a	a	k8xC
vstupním	vstupní	k2eAgNnSc7d1
vestibulem	vestibulum	k1gNnSc7
stoupá	stoupat	k5eAaImIp3nS
oválná	oválný	k2eAgFnSc1d1
schodišťová	schodišťový	k2eAgFnSc1d1
dvorana	dvorana	k1gFnSc1
po	po	k7c6
celé	celý	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
budovy	budova	k1gFnSc2
až	až	k9
do	do	k7c2
prosklené	prosklený	k2eAgFnSc2d1
hladké	hladký	k2eAgFnSc2d1
kopule	kopule	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podlaha	podlaha	k1gFnSc1
dvorany	dvorana	k1gFnSc2
byla	být	k5eAaImAgFnS
vyskládána	vyskládat	k5eAaPmNgFnS
z	z	k7c2
mramorových	mramorový	k2eAgFnPc2d1
dlaždic	dlaždice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
později	pozdě	k6eAd2
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
nahrazeny	nahradit	k5eAaPmNgInP
kamennými	kamenný	k2eAgFnPc7d1
dlaždičkami	dlaždička	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
mramoru	mramor	k1gInSc2
jsou	být	k5eAaImIp3nP
rovněž	rovněž	k9
obklady	obklad	k1gInPc1
stěn	stěna	k1gFnPc2
<g/>
,	,	kIx,
schodnice	schodnice	k1gFnPc1
a	a	k8xC
sloupy	sloup	k1gInPc1
pod	pod	k7c7
dělícími	dělící	k2eAgInPc7d1
oblouky	oblouk	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostor	prostor	k1gInSc4
dvorany	dvorana	k1gFnSc2
vymezují	vymezovat	k5eAaImIp3nP
ochozy	ochoz	k1gInPc1
hlavního	hlavní	k2eAgNnSc2d1
schodiště	schodiště	k1gNnSc2
s	s	k7c7
klenutým	klenutý	k2eAgInSc7d1
stropem	strop	k1gInSc7
a	a	k8xC
tepaným	tepaný	k2eAgNnSc7d1
zábradlím	zábradlí	k1gNnSc7
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yRgMnSc2,k3yIgMnSc2,k3yQgMnSc2
vybíhají	vybíhat	k5eAaImIp3nP
chodby	chodba	k1gFnPc1
do	do	k7c2
křídel	křídlo	k1gNnPc2
budovy	budova	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Ministerské	ministerský	k2eAgNnSc1d1
schodiště	schodiště	k1gNnSc1
ze	z	k7c2
žuly	žula	k1gFnSc2
a	a	k8xC
mramoru	mramor	k1gInSc2
je	být	k5eAaImIp3nS
prosvětleno	prosvětlit	k5eAaPmNgNnS
trojdílným	trojdílný	k2eAgNnSc7d1
oknem	okno	k1gNnSc7
s	s	k7c7
barevnou	barevný	k2eAgFnSc7d1
vitráží	vitráž	k1gFnSc7
do	do	k7c2
olova	olovo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k7c3
schodišti	schodiště	k1gNnSc3
jsou	být	k5eAaImIp3nP
dveře	dveře	k1gFnPc1
do	do	k7c2
oválné	oválný	k2eAgFnSc2d1
zasedací	zasedací	k2eAgFnSc2d1
síně	síň	k1gFnSc2
se	s	k7c7
dvěma	dva	k4xCgInPc7
krby	krb	k1gInPc7
z	z	k7c2
krkonošského	krkonošský	k2eAgInSc2d1
mramoru	mramor	k1gInSc2
<g/>
,	,	kIx,
mramorovým	mramorový	k2eAgNnSc7d1
obložením	obložení	k1gNnSc7
stěn	stěna	k1gFnPc2
a	a	k8xC
štukovým	štukový	k2eAgInSc7d1
stropem	strop	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracovna	pracovna	k1gFnSc1
ministra	ministr	k1gMnSc2
a	a	k8xC
přilehlé	přilehlý	k2eAgInPc4d1
prostory	prostor	k1gInPc4
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
reprezentační	reprezentační	k2eAgFnSc1d1
síň	síň	k1gFnSc1
ministerstva	ministerstvo	k1gNnSc2
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
masivním	masivní	k2eAgNnSc7d1
obložením	obložení	k1gNnSc7
z	z	k7c2
dubového	dubový	k2eAgNnSc2d1
dřeva	dřevo	k1gNnSc2
s	s	k7c7
bohatou	bohatý	k2eAgFnSc7d1
intarzií	intarzie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgInSc1d2
přijímací	přijímací	k2eAgInSc1d1
salonek	salonek	k1gInSc1
kombinuje	kombinovat	k5eAaImIp3nS
mramor	mramor	k1gInSc4
ve	v	k7c6
vstupní	vstupní	k2eAgFnSc6d1
části	část	k1gFnSc6
s	s	k7c7
intarzovaným	intarzovaný	k2eAgNnSc7d1
obložením	obložení	k1gNnSc7
stěn	stěna	k1gFnPc2
v	v	k7c4
části	část	k1gFnPc4
určené	určený	k2eAgFnPc4d1
pro	pro	k7c4
jednání	jednání	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgInSc1d2
salonek	salonek	k1gInSc1
má	mít	k5eAaImIp3nS
dřevěné	dřevěný	k2eAgNnSc4d1
obložení	obložení	k1gNnSc4
doplněné	doplněná	k1gFnSc2
ozdobnými	ozdobný	k2eAgInPc7d1
kovovými	kovový	k2eAgInPc7d1
prvky	prvek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
přijímací	přijímací	k2eAgInPc1d1
salonky	salonek	k1gInPc1
spojují	spojovat	k5eAaImIp3nP
vysoké	vysoký	k2eAgFnPc4d1
prosklené	prosklený	k2eAgFnPc4d1
dveře	dveře	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
mají	mít	k5eAaImIp3nP
kazetové	kazetový	k2eAgInPc1d1
stropy	strop	k1gInPc1
s	s	k7c7
propracovaným	propracovaný	k2eAgInSc7d1
ornamentem	ornament	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
většiny	většina	k1gFnSc2
dalších	další	k2eAgNnPc2d1
prvorepublikových	prvorepublikový	k2eAgNnPc2d1
ministerstev	ministerstvo	k1gNnPc2
není	být	k5eNaImIp3nS
budova	budova	k1gFnSc1
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
prošla	projít	k5eAaPmAgFnS
budova	budova	k1gFnSc1
čtyřmi	čtyři	k4xCgFnPc7
rozsáhlými	rozsáhlý	k2eAgFnPc7d1
vnitřními	vnitřní	k2eAgFnPc7d1
stavebními	stavební	k2eAgFnPc7d1
úpravami	úprava	k1gFnPc7
<g/>
,	,	kIx,
přesto	přesto	k8xC
si	se	k3xPyFc3
zachovala	zachovat	k5eAaPmAgFnS
stylový	stylový	k2eAgInSc4d1
charakter	charakter	k1gInSc4
a	a	k8xC
mnohé	mnohý	k2eAgInPc4d1
původní	původní	k2eAgInPc4d1
prvky	prvek	k1gInPc4
vnitřního	vnitřní	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
povodních	povodeň	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
byl	být	k5eAaImAgInS
znemožněn	znemožněn	k2eAgInSc1d1
přístup	přístup	k1gInSc1
do	do	k7c2
budovy	budova	k1gFnSc2
a	a	k8xC
evakuovaní	evakuovaný	k2eAgMnPc1d1
zaměstnanci	zaměstnanec	k1gMnPc1
působili	působit	k5eAaImAgMnP
z	z	k7c2
jiných	jiný	k2eAgFnPc2d1
budov	budova	k1gFnPc2
ministerstva	ministerstvo	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatopeny	zatopen	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
podzemní	podzemní	k2eAgFnPc1d1
části	část	k1gFnPc1
budovy	budova	k1gFnSc2
<g/>
,	,	kIx,
sanační	sanační	k2eAgFnSc2d1
práce	práce	k1gFnSc2
probíhaly	probíhat	k5eAaImAgFnP
po	po	k7c4
dobu	doba	k1gFnSc4
dvou	dva	k4xCgInPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Organizace	organizace	k1gFnSc1
v	v	k7c6
resortu	resort	k1gInSc6
</s>
<s>
Český	český	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
zkoušení	zkoušení	k1gNnSc4
zbraní	zbraň	k1gFnPc2
a	a	k8xC
střeliva	střelivo	k1gNnSc2
</s>
<s>
Český	český	k2eAgInSc1d1
metrologický	metrologický	k2eAgInSc1d1
institut	institut	k1gInSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
</s>
<s>
Úřad	úřad	k1gInSc1
pro	pro	k7c4
technickou	technický	k2eAgFnSc4d1
normalizaci	normalizace	k1gFnSc4
<g/>
,	,	kIx,
metrologii	metrologie	k1gFnSc4
a	a	k8xC
státní	státní	k2eAgNnSc4d1
zkušebnictví	zkušebnictví	k1gNnSc4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://www.mpo.cz/cz/rozcestnik/ministerstvo/aplikace-zakona-c-106-1999-sb/informace-zverejnovane-podle-paragrafu-5-odstavec-3-zakona/pocet-zamestnancu-ministerstva-s-vysokoskolskym-vzdelanim-v-doktorskem-studijnim-programu--249172/	https://www.mpo.cz/cz/rozcestnik/ministerstvo/aplikace-zakona-c-106-1999-sb/informace-zverejnovane-podle-paragrafu-5-odstavec-3-zakona/pocet-zamestnancu-ministerstva-s-vysokoskolskym-vzdelanim-v-doktorskem-studijnim-programu--249172/	k4
<g/>
↑	↑	k?
https://www.mpo.cz/rozpocet/r-1.1-104.1-p.html	https://www.mpo.cz/rozpocet/r-1.1-104.1-p.html	k1gInSc1
<g/>
↑	↑	k?
https://www.zakonyprolidi.cz/cs/1992-474/zneni-0	https://www.zakonyprolidi.cz/cs/1992-474/zneni-0	k4
<g/>
↑	↑	k?
Sloučení	sloučení	k1gNnSc4
ministerstev	ministerstvo	k1gNnPc2
dopravy	doprava	k1gFnSc2
a	a	k8xC
průmyslu	průmysl	k1gInSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
říká	říkat	k5eAaImIp3nS
ANO	ano	k9
<g/>
,	,	kIx,
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
Vláda	vláda	k1gFnSc1
schválila	schválit	k5eAaPmAgFnS
sloučení	sloučení	k1gNnSc4
ministerstev	ministerstvo	k1gNnPc2
průmyslu	průmysl	k1gInSc2
a	a	k8xC
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
ušetří	ušetřit	k5eAaPmIp3nS
miliardy	miliarda	k4xCgFnPc4
<g/>
,	,	kIx,
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
Vláda	vláda	k1gFnSc1
schválila	schválit	k5eAaPmAgFnS
<g />
.	.	kIx.
</s>
<s hack="1">
sloučení	sloučení	k1gNnSc1
úřadů	úřad	k1gInPc2
<g/>
,	,	kIx,
uspoří	uspořit	k5eAaPmIp3nS
8,6	8,6	k4
miliardy	miliarda	k4xCgFnPc4
<g/>
,	,	kIx,
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
↑	↑	k?
Pavel	Pavel	k1gMnSc1
Švagr	švagr	k1gMnSc1
<g/>
:	:	kIx,
Ministerstvo	ministerstvo	k1gNnSc1
dopravy	doprava	k1gFnSc2
-	-	kIx~
dvakrát	dvakrát	k6eAd1
měřit	měřit	k5eAaImF
<g/>
,	,	kIx,
jednou	jednou	k6eAd1
(	(	kIx(
<g/>
ne	ne	k9
<g/>
)	)	kIx)
<g/>
slučovat	slučovat	k5eAaImF
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
Myšlenka	myšlenka	k1gFnSc1
dává	dávat	k5eAaImIp3nS
smysl	smysl	k1gInSc4
<g/>
,	,	kIx,
další	další	k2eAgFnPc1d1
změny	změna	k1gFnPc1
by	by	kYmCp3nP
ale	ale	k9
byly	být	k5eAaImAgFnP
katastrofou	katastrofa	k1gFnSc7
<g/>
,	,	kIx,
míní	mínit	k5eAaImIp3nP
odborníci	odborník	k1gMnPc1
o	o	k7c4
spojení	spojení	k1gNnSc4
ministerstev	ministerstvo	k1gNnPc2
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Havlíček	Havlíček	k1gMnSc1
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
průmysl	průmysl	k1gInSc4
i	i	k8xC
dopravu	doprava	k1gFnSc4
natrvalo	natrvalo	k6eAd1
<g/>
,	,	kIx,
stojí	stát	k5eAaImIp3nS
si	se	k3xPyFc3
za	za	k7c7
svým	svůj	k3xOyFgNnSc7
Babiš	Babiš	k1gInSc1
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
↑	↑	k?
Zlatý	zlatý	k2eAgInSc1d1
Bludný	bludný	k2eAgInSc1d1
balvan	balvan	k1gInSc1
za	za	k7c4
rok	rok	k1gInSc4
2009	#num#	k4
-	-	kIx~
Ministerstvo	ministerstvo	k1gNnSc1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc4d1
klub	klub	k1gInSc4
skeptiků	skeptik	k1gMnPc2
Sisyfos	Sisyfos	k1gMnSc1
<g/>
,	,	kIx,
2010-03-08	2010-03-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
Historie	historie	k1gFnSc1
budovy	budova	k1gFnSc2
Ministerstva	ministerstvo	k1gNnSc2
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
MPO	MPO	kA
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
ministrů	ministr	k1gMnPc2
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ministerstvo	ministerstvo	k1gNnSc1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
ministerstva	ministerstvo	k1gNnSc2
</s>
<s>
https://www.mpo.cz/cz/rozcestnik/ministerstvo/o-ministerstvu/historie/historie-budovy-ministerstva-prumyslu-a-obchodu--18907/	https://www.mpo.cz/cz/rozcestnik/ministerstvo/o-ministerstvu/historie/historie-budovy-ministerstva-prumyslu-a-obchodu--18907/	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Ministerstva	ministerstvo	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
doprava	doprava	k6eAd1
•	•	k?
finance	finance	k1gFnPc4
•	•	k?
kultura	kultura	k1gFnSc1
•	•	k?
místní	místní	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
•	•	k?
obrana	obrana	k1gFnSc1
•	•	k?
práce	práce	k1gFnSc2
a	a	k8xC
sociální	sociální	k2eAgFnSc2d1
věci	věc	k1gFnSc2
•	•	k?
průmysl	průmysl	k1gInSc1
a	a	k8xC
obchod	obchod	k1gInSc1
•	•	k?
spravedlnost	spravedlnost	k1gFnSc1
•	•	k?
školství	školství	k1gNnSc1
<g/>
,	,	kIx,
mládež	mládež	k1gFnSc1
a	a	k8xC
tělovýchova	tělovýchova	k1gFnSc1
•	•	k?
vnitro	vnitro	k1gNnSc1
•	•	k?
zahraniční	zahraniční	k2eAgFnSc3d1
věci	věc	k1gFnSc3
•	•	k?
zdravotnictví	zdravotnictví	k1gNnSc2
•	•	k?
zemědělství	zemědělství	k1gNnSc2
•	•	k?
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
•	•	k?
informatika	informatika	k1gFnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
národní	národní	k2eAgInSc4d1
majetek	majetek	k1gInSc4
a	a	k8xC
privatizace	privatizace	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
|	|	kIx~
Praha	Praha	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20020321698	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1086033310	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0610	#num#	k4
9293	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
153499667	#num#	k4
</s>
