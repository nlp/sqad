<s>
Ministerstvo	ministerstvo	k1gNnSc1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
MPO	MPO	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ústřední	ústřední	k2eAgInSc1d1
orgán	orgán	k1gInSc1
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
pro	pro	k7c4
státní	státní	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
průmyslu	průmysl	k1gInSc2
<g/>
,	,	kIx,
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
surovin	surovina	k1gFnPc2
a	a	k8xC
ekonomických	ekonomický	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
vůči	vůči	k7c3
zahraničí	zahraničí	k1gNnSc3
<g/>
.	.	kIx.
</s>