<s>
Flora	Flora	k1gFnSc1	Flora
Olomouc	Olomouc	k1gFnSc1	Olomouc
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
zahradnická	zahradnický	k2eAgFnSc1d1	zahradnická
výstava	výstava	k1gFnSc1	výstava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
každoročně	každoročně	k6eAd1	každoročně
pořádá	pořádat	k5eAaImIp3nS	pořádat
společnost	společnost	k1gFnSc1	společnost
Výstaviště	výstaviště	k1gNnSc2	výstaviště
Flora	Flora	k1gFnSc1	Flora
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Výstava	výstava	k1gFnSc1	výstava
je	být	k5eAaImIp3nS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
každoročně	každoročně	k6eAd1	každoročně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
termínech	termín	k1gInPc6	termín
-	-	kIx~	-
"	"	kIx"	"
<g/>
etapách	etapa	k1gFnPc6	etapa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
se	se	k3xPyFc4	se
také	také	k9	také
podílí	podílet	k5eAaImIp3nS	podílet
Český	český	k2eAgInSc1d1	český
zahrádkářský	zahrádkářský	k2eAgInSc1d1	zahrádkářský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
Svaz	svaz	k1gInSc1	svaz
květinářů	květinář	k1gMnPc2	květinář
a	a	k8xC	a
floristů	florista	k1gMnPc2	florista
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Svaz	svaz	k1gInSc1	svaz
školkařů	školkař	k1gMnPc2	školkař
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zahradnické	zahradnický	k2eAgFnPc1d1	zahradnická
výstavy	výstava	k1gFnPc1	výstava
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Flora	Flora	k1gFnSc1	Flora
Olomouc	Olomouc	k1gFnSc1	Olomouc
se	se	k3xPyFc4	se
v	v	k7c6	v
městě	město	k1gNnSc6	město
Olomouci	Olomouc	k1gFnSc6	Olomouc
konají	konat	k5eAaImIp3nP	konat
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
jen	jen	k9	jen
v	v	k7c6	v
lichých	lichý	k2eAgNnPc6d1	liché
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
sudých	sudý	k2eAgFnPc6d1	sudá
je	on	k3xPp3gFnPc4	on
pořádala	pořádat	k5eAaImAgFnS	pořádat
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
velkých	velký	k2eAgFnPc2d1	velká
květinových	květinový	k2eAgFnPc2d1	květinová
výstav	výstava	k1gFnPc2	výstava
ale	ale	k8xC	ale
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
výstavy	výstava	k1gFnSc2	výstava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
Smetanových	Smetanových	k2eAgInPc6d1	Smetanových
sadech	sad	k1gInPc6	sad
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
celkovou	celkový	k2eAgFnSc4d1	celková
výstavní	výstavní	k2eAgFnSc4d1	výstavní
plochu	plocha	k1gFnSc4	plocha
4	[number]	k4	4
395	[number]	k4	395
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
expozice	expozice	k1gFnSc1	expozice
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
pavilonu	pavilon	k1gInSc6	pavilon
A.	A.	kA	A.
Při	při	k7c6	při
letní	letní	k2eAgFnSc6d1	letní
etapě	etapa	k1gFnSc6	etapa
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
výstavy	výstava	k1gFnSc2	výstava
je	být	k5eAaImIp3nS	být
expozice	expozice	k1gFnSc1	expozice
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
pavilonu	pavilon	k1gInSc6	pavilon
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
větší	veliký	k2eAgFnPc4d2	veliký
části	část	k1gFnPc4	část
jeho	jeho	k3xOp3gFnSc2	jeho
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
pavilon	pavilon	k1gInSc1	pavilon
A	a	k9	a
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
v	v	k7c6	v
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
do	do	k7c2	do
jara	jaro	k1gNnSc2	jaro
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
změnám	změna	k1gFnPc3	změna
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
třech	tři	k4xCgInPc6	tři
pavilonech	pavilon	k1gInPc6	pavilon
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
během	během	k7c2	během
jarní	jarní	k2eAgFnSc2d1	jarní
etapy	etapa	k1gFnSc2	etapa
expozice	expozice	k1gFnSc2	expozice
také	také	k9	také
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
etapě	etapa	k1gFnSc6	etapa
<g/>
,	,	kIx,	,
použity	použit	k2eAgInPc4d1	použit
k	k	k7c3	k
pronájmu	pronájem	k1gInSc3	pronájem
prodejcům	prodejce	k1gMnPc3	prodejce
různého	různý	k2eAgNnSc2d1	různé
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
některé	některý	k3yIgFnPc4	některý
informace	informace	k1gFnPc4	informace
jako	jako	k9	jako
<g/>
:	:	kIx,	:
Velký	velký	k2eAgInSc4d1	velký
zájem	zájem	k1gInSc4	zájem
však	však	k9	však
vzbudily	vzbudit	k5eAaPmAgFnP	vzbudit
také	také	k9	také
expozice	expozice	k1gFnSc1	expozice
zahrádkářů	zahrádkář	k1gMnPc2	zahrádkář
v	v	k7c6	v
pavilonech	pavilon	k1gInPc6	pavilon
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
prodejní	prodejní	k2eAgInPc4d1	prodejní
Letní	letní	k2eAgInPc4d1	letní
zahradnické	zahradnický	k2eAgInPc4d1	zahradnický
trhy	trh	k1gInPc4	trh
v	v	k7c6	v
pavilonech	pavilon	k1gInPc6	pavilon
E	E	kA	E
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
,	,	kIx,	,
G	G	kA	G
a	a	k8xC	a
na	na	k7c6	na
venkovních	venkovní	k2eAgFnPc6d1	venkovní
plochách	plocha	k1gFnPc6	plocha
výstaviště	výstaviště	k1gNnSc2	výstaviště
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
18.08	[number]	k4	18.08
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
Metropole	metropole	k1gFnSc1	metropole
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Heger	Heger	k1gMnSc1	Heger
<g/>
)	)	kIx)	)
naplnila	naplnit	k5eAaPmAgFnS	naplnit
rozvolněná	rozvolněný	k2eAgFnSc1d1	rozvolněná
výstava	výstava	k1gFnSc1	výstava
některých	některý	k3yIgFnPc2	některý
odrůd	odrůda	k1gFnPc2	odrůda
ovoce	ovoce	k1gNnSc2	ovoce
zahrádkářů	zahrádkář	k1gMnPc2	zahrádkář
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
horní	horní	k2eAgInSc4d1	horní
patro	patro	k1gNnSc1	patro
pavilonu	pavilon	k1gInSc2	pavilon
A	A	kA	A
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
pavilony	pavilon	k1gInPc1	pavilon
byly	být	k5eAaImAgInP	být
pronajaty	pronajmout	k5eAaPmNgInP	pronajmout
prodejcům	prodejce	k1gMnPc3	prodejce
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zboží	zboží	k1gNnSc4	zboží
prodávané	prodávaný	k2eAgNnSc4d1	prodávané
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
pavilonech	pavilon	k1gInPc6	pavilon
patří	patřit	k5eAaImIp3nP	patřit
z	z	k7c2	z
největší	veliký	k2eAgFnSc2d3	veliký
části	část	k1gFnSc2	část
zboží	zboží	k1gNnSc2	zboží
bez	bez	k7c2	bez
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
zahradnictví	zahradnictví	k1gNnSc3	zahradnictví
jako	jako	k8xC	jako
bižuterie	bižuterie	k1gFnSc2	bižuterie
<g/>
,	,	kIx,	,
hračky	hračka	k1gFnPc1	hračka
<g/>
,	,	kIx,	,
autopotahy	autopotah	k1gInPc1	autopotah
<g/>
,	,	kIx,	,
záclony	záclona	k1gFnPc1	záclona
<g/>
,	,	kIx,	,
koberce	koberec	k1gInPc1	koberec
<g/>
,	,	kIx,	,
boty	bota	k1gFnPc1	bota
<g/>
,	,	kIx,	,
textil	textil	k1gInSc1	textil
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnPc1d1	sportovní
potřeby	potřeba	k1gFnPc1	potřeba
<g/>
,	,	kIx,	,
digitální	digitální	k2eAgInPc1d1	digitální
nosiče	nosič	k1gInPc1	nosič
<g/>
,	,	kIx,	,
lepidla	lepidlo	k1gNnPc1	lepidlo
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
tak	tak	k9	tak
sortiment	sortiment	k1gInSc1	sortiment
prodávaný	prodávaný	k2eAgInSc1d1	prodávaný
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
dekádě	dekáda	k1gFnSc6	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
Flora	Flora	k1gFnSc1	Flora
Olomouc	Olomouc	k1gFnSc1	Olomouc
připomíná	připomínat	k5eAaImIp3nS	připomínat
spíše	spíše	k9	spíše
běžné	běžný	k2eAgNnSc1d1	běžné
"	"	kIx"	"
<g/>
vietnamské	vietnamský	k2eAgNnSc1d1	vietnamské
<g/>
"	"	kIx"	"
tržiště	tržiště	k1gNnSc1	tržiště
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Flora	Flora	k1gFnSc1	Flora
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c6	v
tržnici	tržnice	k1gFnSc6	tržnice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
snad	snad	k9	snad
ještě	ještě	k6eAd1	ještě
horší	zlý	k2eAgMnSc1d2	horší
než	než	k8xS	než
nechvalně	chvalně	k6eNd1	chvalně
proslulé	proslulý	k2eAgFnPc1d1	proslulá
burzy	burza	k1gFnPc1	burza
na	na	k7c6	na
Lokomotivě	lokomotiva	k1gFnSc6	lokomotiva
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Způsob	způsob	k1gInSc1	způsob
vedení	vedení	k1gNnSc2	vedení
výstavy	výstava	k1gFnSc2	výstava
Flora	Flora	k1gFnSc1	Flora
Olomouc	Olomouc	k1gFnSc1	Olomouc
se	se	k3xPyFc4	se
nelíbí	líbit	k5eNaImIp3nS	líbit
ani	ani	k8xC	ani
olomoucké	olomoucký	k2eAgFnSc3d1	olomoucká
radnici	radnice	k1gFnSc3	radnice
a	a	k8xC	a
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
jej	on	k3xPp3gInSc4	on
otevřeně	otevřeně	k6eAd1	otevřeně
i	i	k9	i
primátor	primátor	k1gMnSc1	primátor
Martin	Martin	k1gMnSc1	Martin
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
.	.	kIx.	.
</s>
<s>
Slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
milionové	milionový	k2eAgFnPc4d1	milionová
investice	investice	k1gFnPc4	investice
města	město	k1gNnSc2	město
do	do	k7c2	do
výstaviště	výstaviště	k1gNnSc2	výstaviště
<g/>
,	,	kIx,	,
změnu	změna	k1gFnSc4	změna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
termínu	termín	k1gInSc6	termín
nekonala	konat	k5eNaImAgFnS	konat
<g/>
.	.	kIx.	.
</s>
<s>
Volná	volný	k2eAgNnPc1d1	volné
prostranství	prostranství	k1gNnPc1	prostranství
a	a	k8xC	a
záhony	záhon	k1gInPc1	záhon
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
dění	dění	k1gNnSc2	dění
na	na	k7c6	na
výstavišti	výstaviště	k1gNnSc6	výstaviště
jsou	být	k5eAaImIp3nP	být
mnohdy	mnohdy	k6eAd1	mnohdy
zaplevelená	zaplevelený	k2eAgNnPc1d1	zaplevelené
<g/>
,	,	kIx,	,
trávníky	trávník	k1gInPc1	trávník
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k8xC	jako
parkoviště	parkoviště	k1gNnSc4	parkoviště
prodejců	prodejce	k1gMnPc2	prodejce
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
úroveň	úroveň	k1gFnSc4	úroveň
výstavy	výstava	k1gFnSc2	výstava
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
terčem	terč	k1gInSc7	terč
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
i	i	k9	i
hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
záchranu	záchrana	k1gFnSc4	záchrana
tradic	tradice	k1gFnPc2	tradice
výstav	výstava	k1gFnPc2	výstava
Flora	Flora	k1gFnSc1	Flora
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
poukázat	poukázat	k5eAaPmF	poukázat
na	na	k7c4	na
neřešené	řešený	k2eNgInPc4d1	neřešený
problémy	problém	k1gInPc4	problém
výstaviště	výstaviště	k1gNnSc2	výstaviště
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Podzimní	podzimní	k2eAgFnSc1d1	podzimní
etapa	etapa	k1gFnSc1	etapa
Flory	Flora	k1gFnSc2	Flora
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vešla	vejít	k5eAaPmAgFnS	vejít
do	do	k7c2	do
sálu	sál	k1gInSc2	sál
středně	středně	k6eAd1	středně
velkého	velký	k2eAgInSc2d1	velký
kulturního	kulturní	k2eAgInSc2d1	kulturní
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
místa	místo	k1gNnPc4	místo
výstavního	výstavní	k2eAgInSc2d1	výstavní
areálu	areál	k1gInSc2	areál
patřila	patřit	k5eAaImAgFnS	patřit
zejména	zejména	k9	zejména
stánkařům	stánkař	k1gMnPc3	stánkař
s	s	k7c7	s
nezahradnickým	zahradnický	k2eNgInSc7d1	zahradnický
sortimentem	sortiment	k1gInSc7	sortiment
a	a	k8xC	a
neuvěřitelnému	uvěřitelný	k2eNgInSc3d1	neuvěřitelný
nepořádku	nepořádek	k1gInSc3	nepořádek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
historickými	historický	k2eAgInPc7d1	historický
Smetanovými	Smetanův	k2eAgInPc7d1	Smetanův
sady	sad	k1gInPc7	sad
prohnala	prohnat	k5eAaPmAgFnS	prohnat
blesková	bleskový	k2eAgFnSc1d1	blesková
povodeň	povodeň	k1gFnSc1	povodeň
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kritici	kritik	k1gMnPc1	kritik
současného	současný	k2eAgInSc2d1	současný
stavu	stav	k1gInSc2	stav
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
výstaviště	výstaviště	k1gNnSc1	výstaviště
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
ve	v	k7c6	v
skromných	skromný	k2eAgFnPc6d1	skromná
podmínkách	podmínka	k1gFnPc6	podmínka
by	by	kYmCp3nS	by
výstavišti	výstaviště	k1gNnSc6	výstaviště
prospěla	prospět	k5eAaPmAgFnS	prospět
důstojnější	důstojný	k2eAgFnSc1d2	důstojnější
podoba	podoba	k1gFnSc1	podoba
expozic	expozice	k1gFnPc2	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kritiku	kritika	k1gFnSc4	kritika
a	a	k8xC	a
snahu	snaha	k1gFnSc4	snaha
sdružení	sdružení	k1gNnSc2	sdružení
situaci	situace	k1gFnSc3	situace
změnit	změnit	k5eAaPmF	změnit
původně	původně	k6eAd1	původně
výstaviště	výstaviště	k1gNnSc1	výstaviště
nereagovalo	reagovat	k5eNaBmAgNnS	reagovat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
kritiku	kritika	k1gFnSc4	kritika
označilo	označit	k5eAaPmAgNnS	označit
za	za	k7c4	za
nekonstruktivní	konstruktivní	k2eNgNnSc4d1	nekonstruktivní
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
expozice	expozice	k1gFnPc4	expozice
a	a	k8xC	a
výpěstky	výpěstek	k1gInPc4	výpěstek
nebo	nebo	k8xC	nebo
za	za	k7c4	za
úspěchy	úspěch	k1gInPc4	úspěch
v	v	k7c6	v
soutěžích	soutěž	k1gFnPc6	soutěž
aranžování	aranžování	k1gNnSc2	aranžování
květin	květina	k1gFnPc2	květina
jsou	být	k5eAaImIp3nP	být
udíleny	udílen	k2eAgFnPc1d1	udílena
ceny	cena	k1gFnPc1	cena
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
oceněním	ocenění	k1gNnSc7	ocenění
je	být	k5eAaImIp3nS	být
cena	cena	k1gFnSc1	cena
Novitas	Novitas	k1gInSc4	Novitas
Olomucensis	Olomucensis	k1gFnSc2	Olomucensis
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
slavnostně	slavnostně	k6eAd1	slavnostně
křtěny	křtěn	k2eAgFnPc4d1	křtěna
nové	nový	k2eAgFnPc4d1	nová
květinové	květinový	k2eAgFnPc4d1	květinová
odrůdy	odrůda	k1gFnPc4	odrůda
<g/>
,	,	kIx,	,
kmotry	kmotr	k1gMnPc4	kmotr
už	už	k6eAd1	už
např.	např.	kA	např.
byli	být	k5eAaImAgMnP	být
Hana	Hana	k1gFnSc1	Hana
Maciuchová	Maciuchová	k1gFnSc1	Maciuchová
<g/>
,	,	kIx,	,
Emília	Emília	k1gFnSc1	Emília
Vášáryová	Vášáryová	k1gFnSc1	Vášáryová
nebo	nebo	k8xC	nebo
Jiří	Jiří	k1gMnSc1	Jiří
Žáček	Žáček	k1gMnSc1	Žáček
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
aranžování	aranžování	k1gNnSc6	aranžování
expozice	expozice	k1gFnSc2	expozice
Flora	Flora	k1gFnSc1	Flora
Olomouc	Olomouc	k1gFnSc1	Olomouc
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
podílí	podílet	k5eAaImIp3nS	podílet
Ivar	Ivar	k1gInSc4	Ivar
Otruba	otruba	k1gFnSc1	otruba
<g/>
,	,	kIx,	,
významná	významný	k2eAgFnSc1d1	významná
osobnost	osobnost	k1gFnSc1	osobnost
zahradní	zahradní	k2eAgFnSc2d1	zahradní
architektury	architektura	k1gFnSc2	architektura
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
letní	letní	k2eAgFnSc6d1	letní
etapě	etapa	k1gFnSc6	etapa
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
výstavy	výstava	k1gFnSc2	výstava
Flora	Flora	k1gFnSc1	Flora
Olomouc	Olomouc	k1gFnSc1	Olomouc
2014	[number]	k4	2014
se	se	k3xPyFc4	se
prezentovalo	prezentovat	k5eAaBmAgNnS	prezentovat
asi	asi	k9	asi
tři	tři	k4xCgNnPc1	tři
sta	sto	k4xCgNnPc1	sto
vystavovatelů	vystavovatel	k1gMnPc2	vystavovatel
a	a	k8xC	a
prodejců	prodejce	k1gMnPc2	prodejce
z	z	k7c2	z
ČR	ČR	kA	ČR
i	i	k8xC	i
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
byli	být	k5eAaImAgMnP	být
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
zahradnictví	zahradnictví	k1gNnSc2	zahradnictví
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
výrobci	výrobce	k1gMnPc7	výrobce
a	a	k8xC	a
prodejci	prodejce	k1gMnPc7	prodejce
zahradní	zahradní	k2eAgFnSc2d1	zahradní
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zahradnických	zahradnický	k2eAgFnPc2d1	zahradnická
školek	školka	k1gFnPc2	školka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
výstavy	výstava	k1gFnPc1	výstava
v	v	k7c6	v
symbolickém	symbolický	k2eAgNnSc6d1	symbolické
měřítku	měřítko	k1gNnSc6	měřítko
nebo	nebo	k8xC	nebo
vůbec	vůbec	k9	vůbec
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
nakolik	nakolik	k6eAd1	nakolik
to	ten	k3xDgNnSc4	ten
lze	lze	k6eAd1	lze
přičítat	přičítat	k5eAaImF	přičítat
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
situaci	situace	k1gFnSc3	situace
v	v	k7c6	v
zahradnictví	zahradnictví	k1gNnSc6	zahradnictví
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
nakolik	nakolik	k6eAd1	nakolik
obavě	obava	k1gFnSc3	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
pověst	pověst	k1gFnSc1	pověst
firmy	firma	k1gFnSc2	firma
spíše	spíše	k9	spíše
degradována	degradovat	k5eAaBmNgFnS	degradovat
účastí	účast	k1gFnSc7	účast
na	na	k7c4	na
takto	takto	k6eAd1	takto
realizované	realizovaný	k2eAgFnSc6d1	realizovaná
výstavě	výstava	k1gFnSc6	výstava
a	a	k8xC	a
zařazením	zařazení	k1gNnSc7	zařazení
prezentace	prezentace	k1gFnSc2	prezentace
mezi	mezi	k7c4	mezi
stánkové	stánkový	k2eAgMnPc4d1	stánkový
prodejce	prodejce	k1gMnPc4	prodejce
zboží	zboží	k1gNnSc2	zboží
importovaného	importovaný	k2eAgNnSc2d1	importované
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Nejnavštěvovanější	navštěvovaný	k2eAgFnSc4d3	nejnavštěvovanější
jarní	jarní	k2eAgFnSc4d1	jarní
etapu	etapa	k1gFnSc4	etapa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
66	[number]	k4	66
251	[number]	k4	251
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
se	se	k3xPyFc4	se
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
výstavy	výstava	k1gFnSc2	výstava
Flora	Flora	k1gFnSc1	Flora
Olomouc	Olomouc	k1gFnSc1	Olomouc
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
30	[number]	k4	30
000	[number]	k4	000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
výstavy	výstava	k1gFnSc2	výstava
počítala	počítat	k5eAaImAgFnS	počítat
na	na	k7c4	na
statisíce	statisíce	k1gInPc4	statisíce
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Flora	Flora	k1gMnSc1	Flora
Olomouc	Olomouc	k1gFnSc4	Olomouc
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
společnosti	společnost	k1gFnSc2	společnost
Výstaviště	výstaviště	k1gNnSc2	výstaviště
Flora	Flora	k1gFnSc1	Flora
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
letní	letní	k2eAgFnSc6d1	letní
etapě	etapa	k1gFnSc6	etapa
květinové	květinový	k2eAgFnSc2d1	květinová
výstavy	výstava	k1gFnSc2	výstava
Flora	Flora	k1gFnSc1	Flora
Olomouc	Olomouc	k1gFnSc1	Olomouc
</s>
