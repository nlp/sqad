<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
známá	známá	k1gFnSc1	známá
kombinováním	kombinování	k1gNnSc7	kombinování
metalové	metalový	k2eAgFnSc2d1	metalová
muziky	muzika	k1gFnSc2	muzika
s	s	k7c7	s
rapem	rape	k1gNnSc7	rape
<g/>
,	,	kIx,	,
hardcorem	hardcor	k1gInSc7	hardcor
a	a	k8xC	a
alternativním	alternativní	k2eAgInSc7d1	alternativní
rockem	rock	k1gInSc7	rock
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
"	"	kIx"	"
<g/>
Velké	velký	k2eAgFnSc2d1	velká
čtyřky	čtyřka	k1gFnSc2	čtyřka
<g/>
"	"	kIx"	"
thrash	thrash	k1gInSc1	thrash
metalu	metal	k1gInSc2	metal
–	–	k?	–
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
Metallica	Metallica	k1gMnSc1	Metallica
<g/>
,	,	kIx,	,
Slayer	Slayer	k1gMnSc1	Slayer
a	a	k8xC	a
Megadeth	Megadeth	k1gMnSc1	Megadeth
<g/>
.	.	kIx.	.
</s>
