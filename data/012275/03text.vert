<p>
<s>
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkit	k1gMnSc1	Bizkit
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
nu	nu	k9	nu
metalová	metalový	k2eAgFnSc1d1	metalová
kapela	kapela	k1gFnSc1	kapela
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nS	tvořit
frontman	frontman	k1gMnSc1	frontman
Fred	Fred	k1gMnSc1	Fred
Durst	Durst	k1gMnSc1	Durst
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
Wes	Wes	k1gFnSc2	Wes
Borland	Borland	kA	Borland
<g/>
,	,	kIx,	,
basák	basák	k1gInSc1	basák
Sam	Sam	k1gMnSc1	Sam
Rivers	Rivers	k1gInSc1	Rivers
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
John	John	k1gMnSc1	John
Otto	Otto	k1gMnSc1	Otto
a	a	k8xC	a
DJ	DJ	kA	DJ
Lethal	Lethal	k1gMnSc1	Lethal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkita	k1gFnPc2	Bizkita
prodali	prodat	k5eAaPmAgMnP	prodat
celosvětově	celosvětově	k6eAd1	celosvětově
33	[number]	k4	33
milionů	milion	k4xCgInPc2	milion
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
čtyři	čtyři	k4xCgNnPc4	čtyři
kompletní	kompletní	k2eAgNnPc4d1	kompletní
studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Unquestionable	Unquestionable	k1gMnSc1	Unquestionable
Truth	Truth	k1gMnSc1	Truth
(	(	kIx(	(
<g/>
Part	part	k1gInSc1	part
1	[number]	k4	1
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
EP	EP	kA	EP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
oddělil	oddělit	k5eAaPmAgMnS	oddělit
Wes	Wes	k1gMnSc1	Wes
Borland	Borland	kA	Borland
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
nahradil	nahradit	k5eAaPmAgInS	nahradit
Mike	Mike	k1gInSc1	Mike
Smith	Smitha	k1gFnPc2	Smitha
na	na	k7c4	na
Results	Results	k1gInSc4	Results
May	May	k1gMnSc1	May
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
znovu	znovu	k6eAd1	znovu
připojil	připojit	k5eAaPmAgInS	připojit
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
The	The	k1gMnSc1	The
Unquestionable	Unquestionable	k1gMnSc1	Unquestionable
Truth	Truth	k1gMnSc1	Truth
(	(	kIx(	(
<g/>
Part	part	k1gInSc1	part
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkit	k1gInSc1	Bizkit
opět	opět	k6eAd1	opět
opustil	opustit	k5eAaPmAgInS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
skupina	skupina	k1gFnSc1	skupina
sjednotila	sjednotit	k5eAaPmAgFnS	sjednotit
a	a	k8xC	a
plánuje	plánovat	k5eAaImIp3nS	plánovat
vydání	vydání	k1gNnSc4	vydání
nového	nový	k2eAgInSc2d1	nový
CD	CD	kA	CD
s	s	k7c7	s
názvem	název	k1gInSc7	název
Gold	Gold	k1gMnSc1	Gold
Cobra	Cobra	k1gMnSc1	Cobra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
===	===	k?	===
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkita	k1gFnPc2	Bizkita
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Jacksonville	Jacksonville	k1gFnSc2	Jacksonville
<g/>
.	.	kIx.	.
</s>
<s>
Zakládající	zakládající	k2eAgMnPc1d1	zakládající
členové	člen	k1gMnPc1	člen
byli	být	k5eAaImAgMnP	být
Fred	Fred	k1gMnSc1	Fred
Durst	Durst	k1gMnSc1	Durst
a	a	k8xC	a
Sam	Sam	k1gMnSc1	Sam
Rivers	Riversa	k1gFnPc2	Riversa
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
Samův	Samův	k2eAgMnSc1d1	Samův
bratranec	bratranec	k1gMnSc1	bratranec
John	John	k1gMnSc1	John
Otto	Otto	k1gMnSc1	Otto
a	a	k8xC	a
jako	jako	k9	jako
poslední	poslední	k2eAgFnSc4d1	poslední
skupinu	skupina	k1gFnSc4	skupina
doplnil	doplnit	k5eAaPmAgMnS	doplnit
Wes	Wes	k1gMnSc1	Wes
Borland	Borland	kA	Borland
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
počátcích	počátek	k1gInPc6	počátek
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Limp	limpa	k1gFnPc2	limpa
Bizcut	Bizcut	k2eAgMnSc1d1	Bizcut
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
Jacksonville	Jacksonvilla	k1gFnSc6	Jacksonvilla
Korn	Korn	k1gMnSc1	Korn
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
basák	basák	k1gInSc1	basák
Fieldy	Fielda	k1gFnSc2	Fielda
od	od	k7c2	od
Freda	Fred	k1gMnSc2	Fred
tetovat	tetovat	k5eAaImF	tetovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
objevili	objevit	k5eAaPmAgMnP	objevit
podruhé	podruhé	k6eAd1	podruhé
vzali	vzít	k5eAaPmAgMnP	vzít
si	se	k3xPyFc3	se
od	od	k7c2	od
LB	LB	kA	LB
jejich	jejich	k3xOp3gInPc4	jejich
demo	demo	k2eAgInPc4d1	demo
a	a	k8xC	a
dali	dát	k5eAaPmAgMnP	dát
ho	on	k3xPp3gMnSc4	on
svému	svůj	k3xOyFgMnSc3	svůj
producentovi	producent	k1gMnSc3	producent
Rossu	Ross	k1gMnSc3	Ross
Robinsovi	Robins	k1gMnSc3	Robins
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgInPc3	tento
kontaktům	kontakt	k1gInPc3	kontakt
vyjeli	vyjet	k5eAaPmAgMnP	vyjet
LB	LB	kA	LB
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
Deftones	Deftonesa	k1gFnPc2	Deftonesa
a	a	k8xC	a
House	house	k1gNnSc1	house
of	of	k?	of
Pain	Pain	k1gMnSc1	Pain
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
turné	turné	k1gNnSc2	turné
se	se	k3xPyFc4	se
seznámili	seznámit	k5eAaPmAgMnP	seznámit
s	s	k7c7	s
DJem	DJe	k1gNnSc7	DJe
skupiny	skupina	k1gFnSc2	skupina
House	house	k1gNnSc1	house
of	of	k?	of
Pain	Pain	k1gMnSc1	Pain
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
rozpadu	rozpad	k1gInSc6	rozpad
k	k	k7c3	k
LB	LB	kA	LB
přidal	přidat	k5eAaPmAgMnS	přidat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInPc1	první
úspěchy	úspěch	k1gInPc1	úspěch
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
turné	turné	k1gNnSc6	turné
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
vydávají	vydávat	k5eAaPmIp3nP	vydávat
své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
Three	Thre	k1gMnSc2	Thre
Dollar	dollar	k1gInSc4	dollar
Bill	Bill	k1gMnSc1	Bill
<g/>
,	,	kIx,	,
Yall	Yall	k1gMnSc1	Yall
<g/>
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
songy	song	k1gInPc4	song
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Faith	Faith	k1gInSc1	Faith
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Counterfeit	Counterfeit	k1gInSc1	Counterfeit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
se	se	k3xPyFc4	se
tahle	tenhle	k3xDgFnSc1	tenhle
partička	partička	k1gFnSc1	partička
stává	stávat	k5eAaImIp3nS	stávat
velice	velice	k6eAd1	velice
známá	známý	k2eAgFnSc1d1	známá
<g/>
,	,	kIx,	,
koncertují	koncertovat	k5eAaImIp3nP	koncertovat
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
Faith	Faitha	k1gFnPc2	Faitha
No	no	k9	no
More	mor	k1gInSc5	mor
a	a	k8xC	a
Primus	primus	k1gMnSc1	primus
<g/>
.	.	kIx.	.
</s>
<s>
Začínají	začínat	k5eAaImIp3nP	začínat
se	se	k3xPyFc4	se
také	také	k9	také
objevovat	objevovat	k5eAaImF	objevovat
na	na	k7c6	na
MTV	MTV	kA	MTV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Significant	Significant	k1gMnSc1	Significant
Other	Other	k1gMnSc1	Other
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
vydáni	vydán	k2eAgMnPc1d1	vydán
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
světoznámou	světoznámý	k2eAgFnSc7d1	světoznámá
skupinou	skupina	k1gFnSc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Obdrželi	obdržet	k5eAaPmAgMnP	obdržet
také	také	k9	také
spoustu	spousta	k1gFnSc4	spousta
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
například	například	k6eAd1	například
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
rockové	rockový	k2eAgNnSc4d1	rockové
video	video	k1gNnSc4	video
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Break	break	k1gInSc1	break
Stuff	Stuff	k1gInSc1	Stuff
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
hip-hopové	hipopový	k2eAgNnSc4d1	hip-hopové
video	video	k1gNnSc4	video
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
N	N	kA	N
<g/>
2	[number]	k4	2
<g/>
gether	gethra	k1gFnPc2	gethra
now	now	k?	now
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
spoustu	spousta	k1gFnSc4	spousta
dalších	další	k2eAgFnPc2d1	další
skvělých	skvělý	k2eAgFnPc2d1	skvělá
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
živou	živý	k2eAgFnSc4d1	živá
verzi	verze	k1gFnSc4	verze
"	"	kIx"	"
<g/>
Show	show	k1gFnSc1	show
Me	Me	k1gMnSc1	Me
What	What	k1gMnSc1	What
You	You	k1gMnSc1	You
Got	Got	k1gMnSc1	Got
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Nookie	Nookie	k1gFnSc1	Nookie
<g/>
"	"	kIx"	"
a	a	k8xC	a
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
"	"	kIx"	"
<g/>
Re-arranged	Rerranged	k1gInSc1	Re-arranged
<g/>
"	"	kIx"	"
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
4	[number]	k4	4
miliony	milion	k4xCgInPc4	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
LB	LB	kA	LB
velice	velice	k6eAd1	velice
oblíbení	oblíbení	k1gNnSc1	oblíbení
ale	ale	k8xC	ale
začínaji	začínat	k5eAaPmIp1nS	začínat
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
i	i	k9	i
jejich	jejich	k3xOp3gMnPc1	jejich
odpůrci	odpůrce	k1gMnPc1	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
tak	tak	k6eAd1	tak
dvě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
lidí	člověk	k1gMnPc2	člověk
okolo	okolo	k7c2	okolo
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
co	co	k3yQnSc1	co
ji	on	k3xPp3gFnSc4	on
milují	milovat	k5eAaImIp3nP	milovat
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
co	co	k9	co
ji	on	k3xPp3gFnSc4	on
nenávidí	návidět	k5eNaImIp3nP	návidět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chocolate	Chocolat	k1gInSc5	Chocolat
Starfish	Starfisha	k1gFnPc2	Starfisha
and	and	k?	and
the	the	k?	the
Hot	hot	k0	hot
Dog	doga	k1gFnPc2	doga
Flavoured	Flavoured	k1gMnSc1	Flavoured
Water	Water	k1gMnSc1	Water
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
skupina	skupina	k1gFnSc1	skupina
vydává	vydávat	k5eAaImIp3nS	vydávat
album	album	k1gNnSc4	album
Chocolate	Chocolat	k1gInSc5	Chocolat
Starfish	Starfisha	k1gFnPc2	Starfisha
and	and	k?	and
the	the	k?	the
Hotdog	Hotdog	k1gMnSc1	Hotdog
Flavored	Flavored	k1gMnSc1	Flavored
Water	Water	k1gMnSc1	Water
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
ihned	ihned	k6eAd1	ihned
na	na	k7c6	na
prvních	první	k4xOgFnPc6	první
příčkách	příčka	k1gFnPc6	příčka
světových	světový	k2eAgInPc2d1	světový
žebříčků	žebříček	k1gInPc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
také	také	k9	také
byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc4d1	jediná
která	který	k3yRgNnPc1	který
měla	mít	k5eAaImAgNnP	mít
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
žebříčku	žebříček	k1gInSc6	žebříček
nejprodávanějších	prodávaný	k2eAgNnPc2d3	nejprodávanější
alb	album	k1gNnPc2	album
hned	hned	k6eAd1	hned
dvě	dva	k4xCgNnPc1	dva
alba	album	k1gNnPc1	album
současně	současně	k6eAd1	současně
-	-	kIx~	-
Chocolate	Chocolat	k1gInSc5	Chocolat
Starfish	Starfish	k1gMnSc1	Starfish
a	a	k8xC	a
Significant	Significant	k1gMnSc1	Significant
Other	Other	k1gMnSc1	Other
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
své	svůj	k3xOyFgNnSc4	svůj
nu-metalové	nuetal	k1gMnPc1	nu-metal
působnosti	působnost	k1gFnSc2	působnost
<g/>
.	.	kIx.	.
</s>
<s>
Obdrželi	obdržet	k5eAaPmAgMnP	obdržet
spoustu	spoustu	k6eAd1	spoustu
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
od	od	k7c2	od
MTV	MTV	kA	MTV
hned	hned	k9	hned
čtyři	čtyři	k4xCgInPc4	čtyři
-	-	kIx~	-
v	v	k7c6	v
Music	Musice	k1gInPc2	Musice
Awards	Awards	k1gInSc4	Awards
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
webovou	webový	k2eAgFnSc4d1	webová
stránku	stránka	k1gFnSc4	stránka
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
skupinu	skupina	k1gFnSc4	skupina
a	a	k8xC	a
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
ve	v	k7c4	v
Video	video	k1gNnSc4	video
Awards	Awardsa	k1gFnPc2	Awardsa
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
rockové	rockový	k2eAgNnSc4d1	rockové
video	video	k1gNnSc4	video
-	-	kIx~	-
"	"	kIx"	"
<g/>
Rollin	Rollin	k1gInSc1	Rollin
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Fred	Fred	k1gMnSc1	Fred
tehdy	tehdy	k6eAd1	tehdy
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Prosím	prosit	k5eAaImIp1nS	prosit
vás	vy	k3xPp2nPc4	vy
poslouchejte	poslouchat	k5eAaImRp2nP	poslouchat
tohle	tenhle	k3xDgNnSc4	tenhle
album	album	k1gNnSc4	album
s	s	k7c7	s
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
myslí	mysl	k1gFnSc7	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Tyhle	tenhle	k3xDgFnPc4	tenhle
písničky	písnička	k1gFnPc1	písnička
jsou	být	k5eAaImIp3nP	být
bránou	brána	k1gFnSc7	brána
k	k	k7c3	k
našim	náš	k3xOp1gFnPc3	náš
duším	duše	k1gFnPc3	duše
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Poté	poté	k6eAd1	poté
následovala	následovat	k5eAaImAgFnS	následovat
Anger	Anger	k1gInSc4	Anger
Management	management	k1gInSc1	management
Tour	Toura	k1gFnPc2	Toura
na	na	k7c4	na
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
Eminem	Emino	k1gNnSc7	Emino
<g/>
,	,	kIx,	,
Papa	papa	k1gMnSc1	papa
Roach	Roach	k1gMnSc1	Roach
<g/>
,	,	kIx,	,
Xzibit	Xzibit	k1gMnSc1	Xzibit
a	a	k8xC	a
Zen	zen	k2eAgMnSc1d1	zen
Master	master	k1gMnSc1	master
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
koncertovali	koncertovat	k5eAaImAgMnP	koncertovat
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
festivalu	festival	k1gInSc2	festival
Big	Big	k1gMnSc1	Big
Day	Day	k1gMnSc1	Day
Out	Out	k1gMnSc1	Out
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tragédie	tragédie	k1gFnSc1	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
při	při	k7c6	při
koncertu	koncert	k1gInSc6	koncert
zemřela	zemřít	k5eAaPmAgFnS	zemřít
patnáctiletá	patnáctiletý	k2eAgFnSc1d1	patnáctiletá
Jessica	Jessica	k1gFnSc1	Jessica
na	na	k7c4	na
následky	následek	k1gInPc4	následek
infarktu	infarkt	k1gInSc2	infarkt
a	a	k8xC	a
ušlapání	ušlapání	k1gNnSc4	ušlapání
davem	dav	k1gInSc7	dav
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
to	ten	k3xDgNnSc1	ten
tehdy	tehdy	k6eAd1	tehdy
hodně	hodně	k6eAd1	hodně
zakývalo	zakývat	k5eAaPmAgNnS	zakývat
<g/>
.	.	kIx.	.
</s>
<s>
Zrušili	zrušit	k5eAaPmAgMnP	zrušit
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
kvůli	kvůli	k7c3	kvůli
nevyhovujícím	vyhovující	k2eNgFnPc3d1	nevyhovující
zábranám	zábrana	k1gFnPc3	zábrana
-	-	kIx~	-
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
pro	pro	k7c4	pro
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgNnSc1d1	Evropské
turné	turné	k1gNnSc1	turné
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
velice	velice	k6eAd1	velice
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Fred	Fred	k1gMnSc1	Fred
se	se	k3xPyFc4	se
ale	ale	k9	ale
musel	muset	k5eAaImAgInS	muset
vrátit	vrátit	k5eAaPmF	vrátit
domů	domů	k6eAd1	domů
do	do	k7c2	do
USA	USA	kA	USA
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
se	s	k7c7	s
zády	záda	k1gNnPc7	záda
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
turné	turné	k1gNnSc2	turné
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odchod	odchod	k1gInSc1	odchod
Borlanda	Borlando	k1gNnSc2	Borlando
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
přišla	přijít	k5eAaPmAgFnS	přijít
šokující	šokující	k2eAgFnSc1d1	šokující
zpráva	zpráva	k1gFnSc1	zpráva
-	-	kIx~	-
Wes	Wes	k1gFnSc1	Wes
Borland	Borland	kA	Borland
odchází	odcházet	k5eAaImIp3nS	odcházet
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
Fred	Fred	k1gMnSc1	Fred
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snažíte	snažit	k5eAaImIp2nP	snažit
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
teď	teď	k6eAd1	teď
bude	být	k5eAaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnPc1	první
hodláme	hodlat	k5eAaImIp1nP	hodlat
prohledat	prohledat	k5eAaPmF	prohledat
celou	celý	k2eAgFnSc4d1	celá
zeměkouli	zeměkoule	k1gFnSc4	zeměkoule
a	a	k8xC	a
najít	najít	k5eAaPmF	najít
nejzkaženějšího	zkažený	k2eAgMnSc4d3	nejzkaženější
kytaristu	kytarista	k1gMnSc4	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
s	s	k7c7	s
tím	ten	k3xDgMnSc7	ten
nejzkaženějším	zkažený	k2eAgMnSc7d3	nejzkaženější
kytaristou	kytarista	k1gMnSc7	kytarista
dokončíme	dokončit	k5eAaPmIp1nP	dokončit
to	ten	k3xDgNnSc4	ten
nejohavnější	ohavný	k2eAgNnSc4d3	nejohavnější
album	album	k1gNnSc4	album
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkita	k1gFnPc2	Bizkita
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
byste	by	kYmCp2nP	by
si	se	k3xPyFc3	se
jen	jen	k9	jen
mohli	moct	k5eAaImAgMnP	moct
představit	představit	k5eAaPmF	představit
<g/>
.	.	kIx.	.
</s>
<s>
Wes	Wes	k?	Wes
byl	být	k5eAaImAgMnS	být
velkou	velký	k2eAgFnSc7d1	velká
částí	část	k1gFnSc7	část
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkita	k1gFnPc2	Bizkita
<g/>
,	,	kIx,	,
jste	být	k5eAaImIp2nP	být
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc4	jeho
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
odejít	odejít	k5eAaPmF	odejít
nás	my	k3xPp1nPc2	my
postavilo	postavit	k5eAaPmAgNnS	postavit
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
nej	nej	k?	nej
z	z	k7c2	z
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkita	k1gFnPc2	Bizkita
teprve	teprve	k6eAd1	teprve
přijde	přijít	k5eAaPmIp3nS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Lethal	Lethal	k1gMnSc1	Lethal
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
Sam	Sam	k1gMnSc1	Sam
a	a	k8xC	a
já	já	k3xPp1nSc1	já
to	ten	k3xDgNnSc1	ten
bereme	brát	k5eAaImIp1nP	brát
jako	jako	k9	jako
příležitost	příležitost	k1gFnSc4	příležitost
změnit	změnit	k5eAaPmF	změnit
naši	náš	k3xOp1gFnSc4	náš
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
Wesovým	Wesův	k2eAgInSc7d1	Wesův
odchodem	odchod	k1gInSc7	odchod
stihli	stihnout	k5eAaPmAgMnP	stihnout
vydat	vydat	k5eAaPmF	vydat
remixové	remixový	k2eAgNnSc4d1	remixové
album	album	k1gNnSc4	album
New	New	k1gFnSc2	New
Old	Olda	k1gFnPc2	Olda
Songs	Songsa	k1gFnPc2	Songsa
<g/>
.	.	kIx.	.
</s>
<s>
Dokončovali	dokončovat	k5eAaImAgMnP	dokončovat
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
už	už	k6eAd1	už
bez	bez	k7c2	bez
Wese	Wes	k1gFnSc2	Wes
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
vydáni	vydat	k5eAaPmNgMnP	vydat
4	[number]	k4	4
<g/>
.	.	kIx.	.
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
alba	album	k1gNnSc2	album
LB	LB	kA	LB
byl	být	k5eAaImAgMnS	být
tedy	tedy	k9	tedy
prosinec	prosinec	k1gInSc4	prosinec
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
na	na	k7c6	na
albu	album	k1gNnSc6	album
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
velké	velký	k2eAgFnPc4d1	velká
hvězdy	hvězda	k1gFnPc4	hvězda
hip-hopu	hipop	k1gInSc2	hip-hop
jako	jako	k8xC	jako
například	například	k6eAd1	například
Timbaland	Timbaland	k1gInSc1	Timbaland
<g/>
,	,	kIx,	,
Puff	puff	k1gInSc1	puff
Diddy	Didda	k1gFnSc2	Didda
<g/>
,	,	kIx,	,
Xzibit	Xzibit	k1gFnSc1	Xzibit
<g/>
,	,	kIx,	,
Neptunes	Neptunes	k1gInSc1	Neptunes
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Orbit	orbita	k1gFnPc2	orbita
<g/>
,	,	kIx,	,
Butch	Butch	k1gMnSc1	Butch
Vig	Vig	k1gMnSc1	Vig
<g/>
,	,	kIx,	,
Bubba	Bubba	k1gMnSc1	Bubba
Sparxxx	Sparxxx	k1gInSc1	Sparxxx
<g/>
,	,	kIx,	,
Everlast	Everlast	k1gInSc1	Everlast
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
začalo	začít	k5eAaPmAgNnS	začít
období	období	k1gNnSc4	období
konkurzů	konkurz	k1gInPc2	konkurz
a	a	k8xC	a
pohovorů	pohovor	k1gInPc2	pohovor
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
najít	najít	k5eAaPmF	najít
náhradu	náhrada	k1gFnSc4	náhrada
na	na	k7c4	na
post	post	k1gInSc4	post
kytaristy	kytarista	k1gMnSc2	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Trvalo	trvat	k5eAaImAgNnS	trvat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Fred	Fred	k1gMnSc1	Fred
také	také	k9	také
žádal	žádat	k5eAaImAgMnS	žádat
Wese	Wes	k1gMnSc4	Wes
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
znovu	znovu	k6eAd1	znovu
popřemýšlel	popřemýšlet	k5eAaPmAgMnS	popřemýšlet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
proslýchat	proslýchat	k5eAaImF	proslýchat
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
alba	album	k1gNnSc2	album
bylo	být	k5eAaImAgNnS	být
neustále	neustále	k6eAd1	neustále
měněno	měnit	k5eAaImNgNnS	měnit
od	od	k7c2	od
Less	Lessa	k1gFnPc2	Lessa
is	is	k?	is
More	mor	k1gInSc5	mor
až	až	k9	až
po	po	k7c4	po
Surrender	Surrender	k1gInSc4	Surrender
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
a	a	k8xC	a
půl	půl	k1xP	půl
svět	svět	k1gInSc1	svět
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
jméno	jméno	k1gNnSc4	jméno
nového	nový	k2eAgMnSc2d1	nový
kytaristy	kytarista	k1gMnSc2	kytarista
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Mike	Mike	k1gInSc4	Mike
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
chlapík	chlapík	k1gMnSc1	chlapík
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
začínal	začínat	k5eAaImAgInS	začínat
s	s	k7c7	s
punkovou	punkový	k2eAgFnSc7d1	punková
kapelou	kapela	k1gFnSc7	kapela
Snot	Snota	k1gFnPc2	Snota
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
tedy	tedy	k9	tedy
začala	začít	k5eAaPmAgFnS	začít
okamžitě	okamžitě	k6eAd1	okamžitě
nahrávat	nahrávat	k5eAaImF	nahrávat
<g/>
.	.	kIx.	.
20	[number]	k4	20
songů	song	k1gInPc2	song
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
mezi	mezi	k7c7	mezi
odchodem	odchod	k1gInSc7	odchod
Wese	Wese	k1gFnSc7	Wese
a	a	k8xC	a
příchodem	příchod	k1gInSc7	příchod
Mika	Mik	k1gMnSc2	Mik
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
nahráli	nahrát	k5eAaPmAgMnP	nahrát
s	s	k7c7	s
hostujícími	hostující	k2eAgMnPc7d1	hostující
kytaristy	kytarista	k1gMnPc7	kytarista
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
tedy	tedy	k9	tedy
s	s	k7c7	s
Headem	Head	k1gInSc7	Head
a	a	k8xC	a
Munkym	Munkym	k1gInSc4	Munkym
z	z	k7c2	z
Korn	Korna	k1gFnPc2	Korna
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
shrnuty	shrnut	k2eAgMnPc4d1	shrnut
ze	z	k7c2	z
stolu	stol	k1gInSc2	stol
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
nanovo	nanovo	k6eAd1	nanovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Results	Results	k1gInSc1	Results
May	May	k1gMnSc1	May
Vary	Vary	k1gInPc1	Vary
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
tedy	tedy	k8xC	tedy
vydávají	vydávat	k5eAaImIp3nP	vydávat
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
Results	Resultsa	k1gFnPc2	Resultsa
May	May	k1gMnSc1	May
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
alba	album	k1gNnSc2	album
ale	ale	k8xC	ale
přišlo	přijít	k5eAaPmAgNnS	přijít
překvapení	překvapení	k1gNnSc1	překvapení
-	-	kIx~	-
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkit	k1gInSc1	Bizkit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Metallicou	Metallica	k1gMnSc7	Metallica
a	a	k8xC	a
Linkin	Linkin	k2eAgInSc1d1	Linkin
Park	park	k1gInSc1	park
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
turné	turné	k1gNnSc6	turné
po	po	k7c6	po
USA	USA	kA	USA
-	-	kIx~	-
Summer	Summer	k1gInSc1	Summer
Sanitarium	Sanitarium	k1gNnSc1	Sanitarium
Tour	Tour	k1gInSc1	Tour
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Rockoví	rockový	k2eAgMnPc1d1	rockový
giganti	gigant	k1gMnPc1	gigant
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
pódiu	pódium	k1gNnSc6	pódium
a	a	k8xC	a
jako	jako	k9	jako
předskokani	předskokan	k1gMnPc1	předskokan
The	The	k1gMnSc1	The
Deftones	Deftones	k1gMnSc1	Deftones
a	a	k8xC	a
Mudwayne	Mudwayn	k1gInSc5	Mudwayn
<g/>
,	,	kIx,	,
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
ukázali	ukázat	k5eAaPmAgMnP	ukázat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turné	turné	k1gNnSc6	turné
se	se	k3xPyFc4	se
však	však	k9	však
stalo	stát	k5eAaPmAgNnS	stát
na	na	k7c4	na
co	co	k3yRnSc4	co
čekali	čekat	k5eAaImAgMnP	čekat
všichni	všechen	k3xTgMnPc1	všechen
odpůrci	odpůrce	k1gMnPc1	odpůrce
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkit	k2eAgInSc1d1	Bizkit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
napadena	napadnout	k5eAaPmNgFnS	napadnout
a	a	k8xC	a
zavalena	zavalit	k5eAaPmNgFnS	zavalit
odpadky	odpadek	k1gInPc1	odpadek
za	za	k7c2	za
řevu	řev	k1gInSc2	řev
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Fred	Fred	k1gMnSc1	Fred
Durst	Durst	k1gMnSc1	Durst
fuck	fuck	k1gMnSc1	fuck
off	off	k?	off
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Fred	Fred	k1gMnSc1	Fred
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
nenechal	nechat	k5eNaPmAgMnS	nechat
líbit	líbit	k5eAaImF	líbit
<g/>
,	,	kIx,	,
zanadával	zanadávat	k5eAaPmAgMnS	zanadávat
si	se	k3xPyFc3	se
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
publikum	publikum	k1gNnSc4	publikum
k	k	k7c3	k
boji	boj	k1gInSc3	boj
a	a	k8xC	a
poté	poté	k6eAd1	poté
odehráli	odehrát	k5eAaPmAgMnP	odehrát
7	[number]	k4	7
písniček	písnička	k1gFnPc2	písnička
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
turné	turné	k1gNnSc2	turné
však	však	k9	však
skončil	skončit	k5eAaPmAgMnS	skončit
pro	pro	k7c4	pro
LB	LB	kA	LB
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Results	Results	k6eAd1	Results
May	May	k1gMnSc1	May
Very	Vera	k1gFnSc2	Vera
je	být	k5eAaImIp3nS	být
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
<g/>
,	,	kIx,	,
pochmurné	pochmurný	k2eAgNnSc1d1	pochmurné
album	album	k1gNnSc1	album
plné	plný	k2eAgFnSc2d1	plná
balad	balada	k1gFnPc2	balada
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
byli	být	k5eAaImAgMnP	být
nejprve	nejprve	k6eAd1	nejprve
šokováni	šokován	k2eAgMnPc1d1	šokován
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
docela	docela	k6eAd1	docela
zklamáni	zklamat	k5eAaPmNgMnP	zklamat
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
písně	píseň	k1gFnPc4	píseň
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Gime	Gime	k1gInSc1	Gime
the	the	k?	the
Mic	Mic	k1gFnSc2	Mic
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
hit	hit	k1gInSc4	hit
"	"	kIx"	"
<g/>
Eat	Eat	k1gMnSc5	Eat
You	You	k1gMnSc5	You
Alive	Aliv	k1gMnSc5	Aliv
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
song	song	k1gInSc1	song
proti	proti	k7c3	proti
Britney	Britne	k2eAgInPc1d1	Britne
"	"	kIx"	"
<g/>
Let	let	k1gInSc1	let
Me	Me	k1gMnSc1	Me
Down	Down	k1gMnSc1	Down
<g/>
"	"	kIx"	"
a	a	k8xC	a
také	také	k9	také
cover	cover	k1gInSc1	cover
"	"	kIx"	"
<g/>
Behind	Behind	k1gInSc1	Behind
Blue	Blue	k1gNnSc1	Blue
Eyes	Eyes	k1gInSc1	Eyes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
od	od	k7c2	od
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
The	The	k1gMnSc1	The
Unquestionable	Unquestionable	k1gMnSc1	Unquestionable
Truth	Truth	k1gMnSc1	Truth
(	(	kIx(	(
<g/>
Part	part	k1gInSc1	part
1	[number]	k4	1
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
vydává	vydávat	k5eAaPmIp3nS	vydávat
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
po	po	k7c4	po
snad	snad	k9	snad
všech	všecek	k3xTgFnPc6	všecek
evrpoských	evrposký	k2eAgFnPc6d1	evrposký
zemích	zem	k1gFnPc6	zem
včetně	včetně	k7c2	včetně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
LB	LB	kA	LB
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
USA	USA	kA	USA
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
proslýchat	proslýchat	k5eAaImF	proslýchat
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
možném	možný	k2eAgInSc6d1	možný
návratu	návrat	k1gInSc6	návrat
Borlanda	Borlando	k1gNnSc2	Borlando
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c4	na
limpbizkit	limpbizkit	k1gInSc4	limpbizkit
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
objevily	objevit	k5eAaPmAgFnP	objevit
fotky	fotka	k1gFnPc4	fotka
celé	celý	k2eAgFnPc4d1	celá
skupiny	skupina	k1gFnPc4	skupina
i	i	k9	i
s	s	k7c7	s
Wesem	Wesum	k1gNnSc7	Wesum
bylo	být	k5eAaImAgNnS	být
jasno	jasno	k6eAd1	jasno
<g/>
.	.	kIx.	.
</s>
<s>
Wes	Wes	k?	Wes
je	být	k5eAaImIp3nS	být
zpátky	zpátky	k6eAd1	zpátky
<g/>
!	!	kIx.	!
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
očekávají	očekávat	k5eAaImIp3nP	očekávat
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plánovaný	plánovaný	k2eAgInSc1d1	plánovaný
přelom	přelom	k1gInSc1	přelom
roku	rok	k1gInSc2	rok
2004-2005	[number]	k4	2004-2005
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
albem	album	k1gNnSc7	album
se	se	k3xPyFc4	se
nekoná	konat	k5eNaImIp3nS	konat
<g/>
.	.	kIx.	.
</s>
<s>
Lidi	člověk	k1gMnPc4	člověk
okolo	okolo	k7c2	okolo
LB	LB	kA	LB
jsou	být	k5eAaImIp3nP	být
zmateni	zmaten	k2eAgMnPc1d1	zmaten
<g/>
.	.	kIx.	.
</s>
<s>
Nevědědí	Nevědědit	k5eAaPmIp3nP	Nevědědit
co	co	k3yInSc4	co
si	se	k3xPyFc3	se
myslet	myslet	k5eAaImF	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
Wese	Wes	k1gInSc2	Wes
odešel	odejít	k5eAaPmAgMnS	odejít
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
jeho	jeho	k3xOp3gMnPc3	jeho
"	"	kIx"	"
<g/>
záskok	záskok	k1gInSc4	záskok
<g/>
"	"	kIx"	"
Mike	Mike	k1gNnSc7	Mike
Smith	Smith	k1gMnSc1	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
bez	bez	k7c2	bez
nějakých	nějaký	k3yIgInPc2	nějaký
velkých	velký	k2eAgInPc2d1	velký
boomů	boom	k1gInPc2	boom
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Fred	Fred	k1gMnSc1	Fred
prohlásíl	prohlásínout	k5eAaPmAgMnS	prohlásínout
že	že	k8xS	že
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
v	v	k7c6	v
tajnosti	tajnost	k1gFnSc6	tajnost
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtějí	chtít	k5eAaImIp3nP	chtít
dělat	dělat	k5eAaImF	dělat
hudbu	hudba	k1gFnSc4	hudba
především	především	k9	především
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
The	The	k1gMnPc2	The
Unquestionable	Unquestionable	k1gMnSc1	Unquestionable
Truth	Truth	k1gMnSc1	Truth
(	(	kIx(	(
<g/>
Part	part	k1gInSc1	part
1	[number]	k4	1
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
i	i	k9	i
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
-	-	kIx~	-
part	part	k1gInSc1	part
1	[number]	k4	1
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
jen	jen	k9	jen
7	[number]	k4	7
písniček	písnička	k1gFnPc2	písnička
<g/>
)	)	kIx)	)
a	a	k8xC	a
Fred	Fred	k1gMnSc1	Fred
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
hodně	hodně	k6eAd1	hodně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
hudbou	hudba	k1gFnSc7	hudba
Rage	Rag	k1gInSc2	Rag
Against	Against	k1gInSc1	Against
the	the	k?	the
Machine	Machin	k1gMnSc5	Machin
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Aggression	Aggression	k1gInSc1	Aggression
without	without	k1gMnSc1	without
screaming	screaming	k1gInSc1	screaming
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
agrese	agrese	k1gFnSc1	agrese
bez	bez	k7c2	bez
řvaní	řvaní	k1gNnSc2	řvaní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
úplně	úplně	k6eAd1	úplně
o	o	k7c6	o
něčem	něco	k3yInSc6	něco
jiném	jiný	k2eAgNnSc6d1	jiné
než	než	k8xS	než
všechny	všechen	k3xTgFnPc1	všechen
předchozí	předchozí	k2eAgFnPc1d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
Fredův	Fredův	k2eAgInSc1d1	Fredův
hlas	hlas	k1gInSc1	hlas
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
výrazný	výrazný	k2eAgInSc1d1	výrazný
a	a	k8xC	a
kytary	kytara	k1gFnPc1	kytara
také	také	k9	také
nezní	znět	k5eNaImIp3nP	znět
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hodně	hodně	k6eAd1	hodně
pesimistické	pesimistický	k2eAgNnSc1d1	pesimistické
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
práce	práce	k1gFnSc2	práce
neměl	mít	k5eNaImAgInS	mít
ani	ani	k8xC	ani
DJ	DJ	kA	DJ
Lethal	Lethal	k1gMnSc1	Lethal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Greatest	Greatest	k1gMnSc1	Greatest
Hitz	Hitz	k1gMnSc1	Hitz
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vydali	vydat	k5eAaPmAgMnP	vydat
také	také	k9	také
album	album	k1gNnSc4	album
svých	svůj	k3xOyFgInPc2	svůj
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
Greatest	Greatest	k1gMnSc1	Greatest
Hitz	Hitz	k1gMnSc1	Hitz
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgInSc2	ten
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
pár	pár	k4xCyI	pár
nových	nový	k2eAgInPc2d1	nový
songů	song	k1gInPc2	song
-	-	kIx~	-
všechny	všechen	k3xTgInPc1	všechen
z	z	k7c2	z
éry	éra	k1gFnSc2	éra
bez	bez	k7c2	bez
Wese	Wes	k1gFnSc2	Wes
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
další	další	k2eAgFnSc1d1	další
rána	rána	k1gFnSc1	rána
<g/>
.	.	kIx.	.
</s>
<s>
Wes	Wes	k?	Wes
znovu	znovu	k6eAd1	znovu
odchazí	odchazit	k5eAaPmIp3nS	odchazit
a	a	k8xC	a
sebou	se	k3xPyFc7	se
bere	brát	k5eAaImIp3nS	brát
i	i	k9	i
několik	několik	k4yIc1	několik
soundů	sound	k1gInPc2	sound
co	co	k9	co
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
nahráli	nahrát	k5eAaPmAgMnP	nahrát
<g/>
.	.	kIx.	.
</s>
<s>
Wes	Wes	k?	Wes
se	se	k3xPyFc4	se
také	také	k9	také
hodně	hodně	k6eAd1	hodně
o	o	k7c6	o
LB	LB	kA	LB
rozpovídal	rozpovídat	k5eAaPmAgInS	rozpovídat
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ne	ne	k9	ne
zrovna	zrovna	k6eAd1	zrovna
v	v	k7c6	v
dobrém	dobré	k1gNnSc6	dobré
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
hodně	hodně	k6eAd1	hodně
znechucen	znechutit	k5eAaPmNgInS	znechutit
situací	situace	k1gFnSc7	situace
v	v	k7c6	v
LB	LB	kA	LB
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkita	k1gFnPc2	Bizkita
připravují	připravovat	k5eAaImIp3nP	připravovat
The	The	k1gMnPc1	The
Unquestionable	Unquestionable	k1gMnSc1	Unquestionable
Truth	Truth	k1gMnSc1	Truth
part	part	k1gInSc4	part
2	[number]	k4	2
<g/>
,	,	kIx,	,
Fred	Fred	k1gMnSc1	Fred
natáčí	natáčet	k5eAaImIp3nS	natáčet
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Sam	Sam	k1gMnSc1	Sam
Rivers	Riversa	k1gFnPc2	Riversa
dělá	dělat	k5eAaImIp3nS	dělat
ze	z	k7c2	z
skupinou	skupina	k1gFnSc7	skupina
The	The	k1gFnSc2	The
Embraced	Embraced	k1gInSc1	Embraced
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
první	první	k4xOgInSc4	první
full-length	fullength	k1gInSc4	full-length
nahrávce	nahrávka	k1gFnSc3	nahrávka
a	a	k8xC	a
John	John	k1gMnSc1	John
Otto	Otto	k1gMnSc1	Otto
si	se	k3xPyFc3	se
založil	založit	k5eAaPmAgMnS	založit
svůj	svůj	k3xOyFgInSc4	svůj
projekt	projekt	k1gInSc4	projekt
kapelu	kapela	k1gFnSc4	kapela
Stereochemix	Stereochemix	k1gInSc1	Stereochemix
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hip-hopový	hipopový	k2eAgInSc1d1	hip-hopový
projekt	projekt	k1gInSc1	projekt
několika	několik	k4yIc2	několik
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
snažit	snažit	k5eAaImF	snažit
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
žánru	žánr	k1gInSc6	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládané	předpokládaný	k2eAgNnSc1d1	předpokládané
datum	datum	k1gNnSc1	datum
vydání	vydání	k1gNnSc1	vydání
TUT2	TUT2	k1gFnSc2	TUT2
bylo	být	k5eAaImAgNnS	být
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
limpbizkit	limpbizkita	k1gFnPc2	limpbizkita
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
zeje	zet	k5eAaImIp3nS	zet
prázdnotou	prázdnota	k1gFnSc7	prázdnota
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
novinek	novinka	k1gFnPc2	novinka
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c4	na
jejich	jejich	k3xOp3gFnPc4	jejich
myspace	myspace	k1gFnPc4	myspace
-	-	kIx~	-
nové	nový	k2eAgInPc4d1	nový
songy	song	k1gInPc4	song
<g/>
,	,	kIx,	,
videa	video	k1gNnSc2	video
a	a	k8xC	a
Fredova	Fredův	k2eAgNnSc2d1	Fredovo
prohlášení	prohlášení	k1gNnSc2	prohlášení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sjednocení	sjednocení	k1gNnSc1	sjednocení
a	a	k8xC	a
Gold	Gold	k1gMnSc1	Gold
Cobra	Cobra	k1gMnSc1	Cobra
===	===	k?	===
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
oznámil	oznámit	k5eAaPmAgMnS	oznámit
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Sam	Sam	k1gMnSc1	Sam
Rivers	Rivers	k1gInSc1	Rivers
<g/>
,	,	kIx,	,
že	že	k8xS	že
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkita	k1gFnPc2	Bizkita
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
studiovém	studiový	k2eAgInSc6d1	studiový
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušky	Fanoušek	k1gMnPc4	Fanoušek
skupiny	skupina	k1gFnSc2	skupina
tato	tento	k3xDgFnSc1	tento
zpráva	zpráva	k1gFnSc1	zpráva
velmi	velmi	k6eAd1	velmi
potěšila	potěšit	k5eAaPmAgFnS	potěšit
a	a	k8xC	a
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
znovu	znovu	k6eAd1	znovu
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Wes	Wes	k1gMnSc1	Wes
Borland	Borland	kA	Borland
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
oficiální	oficiální	k2eAgInSc1d1	oficiální
začátek	začátek	k1gInSc1	začátek
tvoření	tvoření	k1gNnSc2	tvoření
nového	nový	k2eAgInSc2d1	nový
CD	CD	kA	CD
a	a	k8xC	a
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
uvedl	uvést	k5eAaPmAgMnS	uvést
zpěvák	zpěvák	k1gMnSc1	zpěvák
Fred	Fred	k1gMnSc1	Fred
Durst	Durst	k1gMnSc1	Durst
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
již	již	k6eAd1	již
pracuje	pracovat	k5eAaImIp3nS	pracovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
vokálech	vokál	k1gInPc6	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
deska	deska	k1gFnSc1	deska
ponese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Gold	Golda	k1gFnPc2	Golda
Cobra	Cobr	k1gInSc2	Cobr
a	a	k8xC	a
do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkita	k1gFnPc2	Bizkita
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svého	svůj	k3xOyFgNnSc2	svůj
comebackového	comebackový	k2eAgNnSc2d1	comebackové
turné	turné	k1gNnSc2	turné
se	se	k3xPyFc4	se
zastaví	zastavit	k5eAaPmIp3nS	zastavit
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
jmenujme	jmenovat	k5eAaImRp1nP	jmenovat
třeba	třeba	k6eAd1	třeba
uznávaný	uznávaný	k2eAgInSc4d1	uznávaný
německý	německý	k2eAgInSc4d1	německý
festival	festival	k1gInSc4	festival
Rock	rock	k1gInSc1	rock
Am	Am	k1gFnPc2	Am
Ring	ring	k1gInSc1	ring
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
chystá	chystat	k5eAaImIp3nS	chystat
do	do	k7c2	do
Slovenska	Slovensko	k1gNnSc2	Slovensko
na	na	k7c4	na
Top	topit	k5eAaImRp2nS	topit
Fest	fest	k6eAd1	fest
a	a	k8xC	a
ochuzeno	ochuzen	k2eAgNnSc1d1	ochuzeno
by	by	kYmCp3nS	by
nemělo	mít	k5eNaImAgNnS	mít
být	být	k5eAaImF	být
ani	ani	k8xC	ani
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
turné	turné	k1gNnSc1	turné
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Současní	současný	k2eAgMnPc1d1	současný
===	===	k?	===
</s>
</p>
<p>
<s>
Fred	Fred	k1gMnSc1	Fred
Durst	Durst	k1gMnSc1	Durst
-	-	kIx~	-
Zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
<g/>
občas	občas	k6eAd1	občas
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Sam	Sam	k1gMnSc1	Sam
Rivers	Rivers	k1gInSc1	Rivers
-	-	kIx~	-
Bass	Bass	k1gMnSc1	Bass
</s>
</p>
<p>
<s>
DJ	DJ	kA	DJ
Lethal	Lethal	k1gMnSc1	Lethal
-	-	kIx~	-
Turntables	Turntables	k1gMnSc1	Turntables
<g/>
/	/	kIx~	/
<g/>
keyboards	keyboards	k1gInSc1	keyboards
<g/>
/	/	kIx~	/
<g/>
samples	samples	k1gInSc1	samples
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Otto	Otto	k1gMnSc1	Otto
-	-	kIx~	-
Bicí	bicí	k2eAgMnSc1d1	bicí
</s>
</p>
<p>
<s>
Wes	Wes	k?	Wes
Borland	Borland	kA	Borland
-	-	kIx~	-
Kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
===	===	k?	===
dočasní	dočasný	k2eAgMnPc1d1	dočasný
===	===	k?	===
</s>
</p>
<p>
<s>
Mike	Mike	k1gFnSc1	Mike
Smith	Smith	k1gMnSc1	Smith
-	-	kIx~	-
Kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
-	-	kIx~	-
Three	Thre	k1gInSc2	Thre
Dollar	dollar	k1gInSc1	dollar
Bill	Bill	k1gMnSc1	Bill
<g/>
,	,	kIx,	,
Yall	Yall	k1gMnSc1	Yall
<g/>
$	$	kIx~	$
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
-	-	kIx~	-
Significant	Significant	k1gMnSc1	Significant
Other	Other	k1gMnSc1	Other
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
-	-	kIx~	-
Chocolate	Chocolat	k1gInSc5	Chocolat
Starfish	Starfisha	k1gFnPc2	Starfisha
and	and	k?	and
the	the	k?	the
Hotdog	Hotdog	k1gMnSc1	Hotdog
Flavored	Flavored	k1gMnSc1	Flavored
Water	Water	k1gMnSc1	Water
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
-	-	kIx~	-
Results	Results	k1gInSc1	Results
May	May	k1gMnSc1	May
Vary	Vary	k1gInPc1	Vary
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
Smelly	Smella	k1gFnSc2	Smella
Beaver	Beaver	k1gInSc1	Beaver
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
-	-	kIx~	-
Gold	Gold	k1gMnSc1	Gold
Cobra	Cobra	k1gMnSc1	Cobra
</s>
</p>
<p>
<s>
===	===	k?	===
EP	EP	kA	EP
===	===	k?	===
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
The	The	k1gMnSc1	The
Unquestionable	Unquestionable	k1gMnSc1	Unquestionable
Truth	Truth	k1gMnSc1	Truth
(	(	kIx(	(
<g/>
Part	part	k1gInSc1	part
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Živá	živý	k2eAgNnPc4d1	živé
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
-	-	kIx~	-
Rock	rock	k1gInSc4	rock
Im	Im	k1gFnSc2	Im
Park	park	k1gInSc1	park
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
===	===	k?	===
Kompilace	kompilace	k1gFnSc2	kompilace
===	===	k?	===
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
-	-	kIx~	-
New	New	k1gFnSc6	New
Old	Olda	k1gFnPc2	Olda
Songs	Songsa	k1gFnPc2	Songsa
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
Greatest	Greatest	k1gMnSc1	Greatest
Hitz	Hitz	k1gMnSc1	Hitz
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
-	-	kIx~	-
Collected	Collected	k1gInSc1	Collected
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkit	k2eAgMnSc1d1	Bizkit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
Myspace	Myspace	k1gFnSc1	Myspace
</s>
</p>
<p>
<s>
Fanouškovské	fanouškovský	k2eAgFnPc1d1	fanouškovská
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
