<s>
Královna	královna	k1gFnSc1	královna
Elissar	Elissara	k1gFnPc2	Elissara
(	(	kIx(	(
<g/>
známá	známá	k1gFnSc1	známá
také	také	k9	také
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Alissa	Alissa	k1gFnSc1	Alissa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
arabským	arabský	k2eAgNnSc7d1	arabské
jménem	jméno	k1gNnSc7	jméno
ا	ا	k?	ا
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ا	ا	k?	ا
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ع	ع	k?	ع
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
princeznou	princezna	k1gFnSc7	princezna
z	z	k7c2	z
Tyru	Tyrus	k1gInSc2	Tyrus
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
založila	založit	k5eAaPmAgFnS	založit
Kartágo	Kartágo	k1gNnSc4	Kartágo
<g/>
.	.	kIx.	.
</s>
