<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
psáno	psát	k5eAaImNgNnS	psát
také	také	k9	také
Karthágo	Karthágo	k1gNnSc1	Karthágo
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
<g/>
:	:	kIx,	:
Κ	Κ	k?	Κ
<g/>
;	;	kIx,	;
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
ق	ق	k?	ق
také	také	k9	také
ق	ق	k?	ق
<g/>
;	;	kIx,	;
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Carthago	Carthago	k1gNnSc1	Carthago
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jak	jak	k6eAd1	jak
starověké	starověký	k2eAgNnSc1d1	starověké
město	město	k1gNnSc1	město
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
civilizace	civilizace	k1gFnSc1	civilizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
sféře	sféra	k1gFnSc6	sféra
vlivu	vliv	k1gInSc2	vliv
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
nazývá	nazývat	k5eAaImIp3nS	nazývat
jako	jako	k9	jako
Kartaginská	kartaginský	k2eAgFnSc1d1	kartaginská
říše	říše	k1gFnSc1	říše
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
pozdějším	pozdní	k2eAgNnSc7d2	pozdější
zřízením	zřízení	k1gNnSc7	zřízení
Kartaginská	kartaginský	k2eAgFnSc1d1	kartaginská
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Republika	republika	k1gFnSc1	republika
Kartágo	Kartágo	k1gNnSc4	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
Kartágo	Kartágo	k1gNnSc1	Kartágo
královstvím	království	k1gNnSc7	království
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př.n.l.	př.n.l.	k?	př.n.l.
velmocí	velmoc	k1gFnPc2	velmoc
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
s	s	k7c7	s
Římskou	římský	k2eAgFnSc7d1	římská
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jejím	její	k3xOp3gMnSc7	její
rivalem	rival	k1gMnSc7	rival
v	v	k7c6	v
nadvládě	nadvláda	k1gFnSc6	nadvláda
nad	nad	k7c7	nad
západním	západní	k2eAgNnSc7d1	západní
Středomořím	středomoří	k1gNnSc7	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
rivalita	rivalita	k1gFnSc1	rivalita
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
sérii	série	k1gFnSc3	série
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
známých	známá	k1gFnPc2	známá
jako	jako	k8xC	jako
punské	punský	k2eAgFnSc2d1	punská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
Kartágo	Kartágo	k1gNnSc1	Kartágo
vždy	vždy	k6eAd1	vždy
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
prohry	prohra	k1gFnPc1	prohra
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
úpadku	úpadek	k1gInSc3	úpadek
politické	politický	k2eAgFnSc2d1	politická
a	a	k8xC	a
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
síly	síla	k1gFnSc2	síla
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
kvůli	kvůli	k7c3	kvůli
krutým	krutý	k2eAgFnPc3d1	krutá
sankcím	sankce	k1gFnPc3	sankce
uvaleným	uvalený	k2eAgInSc7d1	uvalený
Římem	Řím	k1gInSc7	Řím
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
podmínek	podmínka	k1gFnPc2	podmínka
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
punských	punský	k2eAgFnPc2d1	punská
válek	válka	k1gFnPc2	válka
skončila	skončit	k5eAaPmAgFnS	skončit
úplným	úplný	k2eAgNnSc7d1	úplné
zničením	zničení	k1gNnSc7	zničení
města	město	k1gNnSc2	město
Kartágo	Kartágo	k1gNnSc4	Kartágo
a	a	k8xC	a
anektováním	anektování	k1gNnSc7	anektování
posledních	poslední	k2eAgInPc2d1	poslední
zbytků	zbytek	k1gInPc2	zbytek
kartáginského	kartáginský	k2eAgNnSc2d1	kartáginské
území	území	k1gNnSc2	území
Římany	Říman	k1gMnPc4	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
svébytná	svébytný	k2eAgFnSc1d1	svébytná
kartáginská	kartáginský	k2eAgFnSc1d1	kartáginská
civilizace	civilizace	k1gFnSc1	civilizace
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc1	její
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
přispěly	přispět	k5eAaPmAgInP	přispět
pozdějším	pozdní	k2eAgFnPc3d2	pozdější
středomořským	středomořský	k2eAgFnPc3d1	středomořská
kulturám	kultura	k1gFnPc3	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Kartágo	Kartágo	k1gNnSc4	Kartágo
leželo	ležet	k5eAaImAgNnS	ležet
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Tuniského	tuniský	k2eAgNnSc2d1	tuniské
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
než	než	k8xS	než
leží	ležet	k5eAaImIp3nS	ležet
centrum	centrum	k1gNnSc1	centrum
moderního	moderní	k2eAgInSc2d1	moderní
Tunisu	Tunis	k1gInSc2	Tunis
v	v	k7c6	v
Tunisku	Tunisko	k1gNnSc6	Tunisko
<g/>
.	.	kIx.	.
</s>
<s>
Založeno	založen	k2eAgNnSc1d1	založeno
bylo	být	k5eAaImAgNnS	být
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
814	[number]	k4	814
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Féničany	Féničan	k1gMnPc7	Féničan
jako	jako	k8xS	jako
kolonie	kolonie	k1gFnSc1	kolonie
Tyru	Tyrus	k1gInSc2	Tyrus
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
odvádělo	odvádět	k5eAaImAgNnS	odvádět
tribut	tribut	k1gInSc4	tribut
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
Kartágo	Kartágo	k1gNnSc4	Kartágo
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
v	v	k7c4	v
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
mocnost	mocnost	k1gFnSc4	mocnost
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
shromažďující	shromažďující	k2eAgNnSc4d1	shromažďující
bohatství	bohatství	k1gNnSc4	bohatství
a	a	k8xC	a
vliv	vliv	k1gInSc4	vliv
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
obratnosti	obratnost	k1gFnSc3	obratnost
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Kartágo	Kartágo	k1gNnSc4	Kartágo
je	být	k5eAaImIp3nS	být
odvozeno	odvozen	k2eAgNnSc1d1	odvozeno
z	z	k7c2	z
fénického	fénický	k2eAgMnSc2d1	fénický
QRT	QRT	kA	QRT
HDŠT	HDŠT	kA	HDŠT
neboli	neboli	k8xC	neboli
Qart	Qart	k2eAgInSc1d1	Qart
Hadašt	Hadašt	k1gInSc1	Hadašt
znamenající	znamenající	k2eAgInSc1d1	znamenající
"	"	kIx"	"
<g/>
Nové	Nové	k2eAgNnSc1d1	Nové
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
fénických	fénický	k2eAgNnPc2d1	fénické
sídlišť	sídliště	k1gNnPc2	sídliště
neslo	nést	k5eAaImAgNnS	nést
toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc1	jeden
město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
tu	ten	k3xDgFnSc4	ten
čest	čest	k1gFnSc4	čest
být	být	k5eAaImF	být
Kartágem	Kartágo	k1gNnSc7	Kartágo
starověkého	starověký	k2eAgInSc2d1	starověký
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
kartáginský	kartáginský	k2eAgMnSc1d1	kartáginský
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
užíván	užívat	k5eAaImNgInS	užívat
spoustou	spousta	k1gFnSc7	spousta
současných	současný	k2eAgMnPc2d1	současný
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
starověkých	starověký	k2eAgInPc2d1	starověký
spisů	spis	k1gInPc2	spis
užívá	užívat	k5eAaImIp3nS	užívat
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
punský	punský	k2eAgMnSc1d1	punský
<g/>
"	"	kIx"	"
k	k	k7c3	k
popsání	popsání	k1gNnSc3	popsání
čehokoli	cokoli	k3yInSc2	cokoli
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
mělo	mít	k5eAaImAgNnS	mít
co	co	k9	co
dělat	dělat	k5eAaImF	dělat
s	s	k7c7	s
kartáginskou	kartáginský	k2eAgFnSc7d1	kartáginská
civilizací	civilizace	k1gFnSc7	civilizace
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
latinský	latinský	k2eAgInSc1d1	latinský
termín	termín	k1gInSc1	termín
punicus	punicus	k1gInSc1	punicus
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
poenicus	poenicus	k1gInSc1	poenicus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
propůjčen	propůjčen	k2eAgMnSc1d1	propůjčen
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
Φ	Φ	k?	Φ
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Fénicie	Fénicie	k1gFnSc1	Fénicie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
jsou	být	k5eAaImIp3nP	být
ruiny	ruina	k1gFnPc1	ruina
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
602	[number]	k4	602
ha	ha	kA	ha
<g/>
)	)	kIx)	)
součástí	součást	k1gFnSc7	součást
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
historie	historie	k1gFnSc2	historie
Kartága	Kartágo	k1gNnSc2	Kartágo
je	být	k5eAaImIp3nS	být
problematické	problematický	k2eAgNnSc1d1	problematické
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
podrobení	podrobení	k1gNnSc3	podrobení
civilizace	civilizace	k1gFnSc2	civilizace
Římany	Říman	k1gMnPc7	Říman
na	na	k7c6	na
konci	konec	k1gInSc6	konec
třetí	třetí	k4xOgFnSc2	třetí
punské	punský	k2eAgFnSc2d1	punská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
jen	jen	k9	jen
pár	pár	k4xCyI	pár
historických	historický	k2eAgInPc2d1	historický
pramenů	pramen	k1gInPc2	pramen
původní	původní	k2eAgFnSc2d1	původní
kartaginské	kartaginský	k2eAgFnSc2d1	kartaginská
provenience	provenience	k1gFnSc2	provenience
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
starověkých	starověký	k2eAgInPc2d1	starověký
překladů	překlad	k1gInPc2	překlad
punských	punský	k2eAgInPc2d1	punský
textů	text	k1gInPc2	text
do	do	k7c2	do
řečtiny	řečtina	k1gFnSc2	řečtina
a	a	k8xC	a
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
nápisy	nápis	k1gInPc1	nápis
na	na	k7c6	na
monumentech	monument	k1gInPc6	monument
a	a	k8xC	a
budovách	budova	k1gFnPc6	budova
objevených	objevený	k2eAgInPc2d1	objevený
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
většina	většina	k1gFnSc1	většina
dostupných	dostupný	k2eAgInPc2d1	dostupný
původních	původní	k2eAgInPc2d1	původní
materiálů	materiál	k1gInPc2	materiál
o	o	k7c6	o
Kartáginské	kartáginský	k2eAgFnSc6d1	kartáginská
civilizaci	civilizace	k1gFnSc6	civilizace
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaBmNgFnS	napsat
řeckými	řecký	k2eAgInPc7d1	řecký
a	a	k8xC	a
římskými	římský	k2eAgMnPc7d1	římský
historiky	historik	k1gMnPc7	historik
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Livius	Livius	k1gMnSc1	Livius
<g/>
,	,	kIx,	,
Polybios	Polybios	k1gMnSc1	Polybios
<g/>
,	,	kIx,	,
Appiános	Appiános	k1gMnSc1	Appiános
<g/>
,	,	kIx,	,
Cornelius	Cornelius	k1gMnSc1	Cornelius
Nepos	Nepos	k1gMnSc1	Nepos
<g/>
,	,	kIx,	,
Silius	Silius	k1gMnSc1	Silius
Italicus	Italicus	k1gMnSc1	Italicus
<g/>
,	,	kIx,	,
Plútarchos	Plútarchos	k1gMnSc1	Plútarchos
<g/>
,	,	kIx,	,
Cassius	Cassius	k1gMnSc1	Cassius
Dio	Dio	k1gMnSc1	Dio
a	a	k8xC	a
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
autoři	autor	k1gMnPc1	autor
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
konkurenci	konkurence	k1gFnSc6	konkurence
<g/>
,	,	kIx,	,
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
<g/>
,	,	kIx,	,
s	s	k7c7	s
Kartágem	Kartágo	k1gNnSc7	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
např.	např.	kA	např.
zápolili	zápolit	k5eAaImAgMnP	zápolit
s	s	k7c7	s
Kartágem	Kartágo	k1gNnSc7	Kartágo
o	o	k7c6	o
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
,	,	kIx,	,
a	a	k8xC	a
Římané	Říman	k1gMnPc1	Říman
vedli	vést	k5eAaImAgMnP	vést
proti	proti	k7c3	proti
Kartágu	Kartágo	k1gNnSc3	Kartágo
již	již	k9	již
zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
punské	punský	k2eAgFnPc1d1	punská
války	válka	k1gFnPc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
Kartágu	Kartágo	k1gNnSc6	Kartágo
napsané	napsaný	k2eAgInPc1d1	napsaný
cizinci	cizinec	k1gMnSc3	cizinec
tak	tak	k9	tak
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
zaujaté	zaujatý	k2eAgInPc1d1	zaujatý
soudy	soud	k1gInPc1	soud
a	a	k8xC	a
hodnocení	hodnocení	k1gNnSc1	hodnocení
<g/>
.	.	kIx.	.
</s>
<s>
Nedávné	dávný	k2eNgFnPc4d1	nedávná
vykopávky	vykopávka	k1gFnPc4	vykopávka
starých	starý	k2eAgNnPc2d1	staré
Kartáginských	kartáginský	k2eAgNnPc2d1	kartáginské
míst	místo	k1gNnPc2	místo
přinesly	přinést	k5eAaPmAgFnP	přinést
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
původních	původní	k2eAgInPc2d1	původní
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
nálezů	nález	k1gInPc2	nález
popírají	popírat	k5eAaImIp3nP	popírat
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
aspekty	aspekt	k1gInPc1	aspekt
tradičních	tradiční	k2eAgInPc2d1	tradiční
obrazů	obraz	k1gInPc2	obraz
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spousta	spousta	k1gFnSc1	spousta
materiálu	materiál	k1gInSc2	materiál
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
814	[number]	k4	814
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
fénickými	fénický	k2eAgMnPc7d1	fénický
osadníky	osadník	k1gMnPc7	osadník
z	z	k7c2	z
města	město	k1gNnSc2	město
Týros	Týrosa	k1gFnPc2	Týrosa
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinesli	přinést	k5eAaPmAgMnP	přinést
i	i	k9	i
městského	městský	k2eAgMnSc4d1	městský
boha	bůh	k1gMnSc4	bůh
Melqarta	Melqart	k1gMnSc4	Melqart
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
založila	založit	k5eAaPmAgFnS	založit
město	město	k1gNnSc1	město
královna	královna	k1gFnSc1	královna
Dídó	Dídó	k1gFnSc1	Dídó
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Elissa	Elissa	k1gFnSc1	Elissa
příp	příp	kA	příp
<g/>
.	.	kIx.	.
Elissar	Elissar	k1gMnSc1	Elissar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
utekla	utéct	k5eAaPmAgFnS	utéct
z	z	k7c2	z
Tyru	Tyrus	k1gInSc2	Tyrus
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
byl	být	k5eAaImAgMnS	být
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
během	během	k7c2	během
pokusu	pokus	k1gInSc2	pokus
jejího	její	k3xOp3gMnSc2	její
mladšího	mladý	k2eAgMnSc2d2	mladší
bratra	bratr	k1gMnSc2	bratr
o	o	k7c4	o
posílení	posílení	k1gNnSc4	posílení
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
mýtů	mýtus	k1gInPc2	mýtus
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
skrze	skrze	k?	skrze
řeckou	řecký	k2eAgFnSc4d1	řecká
a	a	k8xC	a
římskou	římský	k2eAgFnSc4d1	římská
literaturu	literatura	k1gFnSc4	literatura
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
Byrsu	Byrs	k1gInSc3	Byrs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
509	[number]	k4	509
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
Kartágem	Kartágo	k1gNnSc7	Kartágo
a	a	k8xC	a
Římem	Řím	k1gInSc7	Řím
podepsána	podepsán	k2eAgFnSc1d1	podepsána
smlouva	smlouva	k1gFnSc1	smlouva
udávající	udávající	k2eAgNnSc1d1	udávající
rozdělení	rozdělení	k1gNnSc1	rozdělení
vlivu	vliv	k1gInSc2	vliv
a	a	k8xC	a
obchodních	obchodní	k2eAgFnPc2d1	obchodní
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
známý	známý	k2eAgInSc1d1	známý
zdroj	zdroj	k1gInSc1	zdroj
oznamující	oznamující	k2eAgInSc1d1	oznamující
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kartágo	Kartágo	k1gNnSc1	Kartágo
získalo	získat	k5eAaPmAgNnS	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Sicílií	Sicílie	k1gFnSc7	Sicílie
a	a	k8xC	a
Sardinií	Sardinie	k1gFnSc7	Sardinie
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Kartágo	Kartágo	k1gNnSc1	Kartágo
stalo	stát	k5eAaPmAgNnS	stát
komerčním	komerční	k2eAgMnSc7d1	komerční
centrem	centr	k1gMnSc7	centr
západního	západní	k2eAgNnSc2d1	západní
středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
pozici	pozice	k1gFnSc4	pozice
si	se	k3xPyFc3	se
udrželo	udržet	k5eAaPmAgNnS	udržet
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebylo	být	k5eNaImAgNnS	být
svrženo	svrhnout	k5eAaPmNgNnS	svrhnout
Římem	Řím	k1gInSc7	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
dobylo	dobýt	k5eAaPmAgNnS	dobýt
většinu	většina	k1gFnSc4	většina
starých	starý	k1gMnPc2	starý
fénických	fénický	k2eAgFnPc2d1	fénická
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Hadrumentum	Hadrumentum	k1gNnSc1	Hadrumentum
<g/>
,	,	kIx,	,
Utica	Utica	k1gFnSc1	Utica
a	a	k8xC	a
Kerkouane	Kerkouan	k1gMnSc5	Kerkouan
<g/>
,	,	kIx,	,
podrobilo	podrobit	k5eAaPmAgNnS	podrobit
si	se	k3xPyFc3	se
libyjské	libyjský	k2eAgInPc4d1	libyjský
kmeny	kmen	k1gInPc4	kmen
a	a	k8xC	a
převzalo	převzít	k5eAaPmAgNnS	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
celým	celý	k2eAgNnSc7d1	celé
pobřežím	pobřeží	k1gNnSc7	pobřeží
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
od	od	k7c2	od
současného	současný	k2eAgNnSc2d1	současné
Maroka	Maroko	k1gNnSc2	Maroko
až	až	k9	až
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vliv	vliv	k1gInSc1	vliv
se	se	k3xPyFc4	se
také	také	k9	také
rozšiřoval	rozšiřovat	k5eAaImAgMnS	rozšiřovat
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
;	;	kIx,	;
převzalo	převzít	k5eAaPmAgNnS	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Sardinií	Sardinie	k1gFnSc7	Sardinie
<g/>
,	,	kIx,	,
Maltou	Malta	k1gFnSc7	Malta
<g/>
,	,	kIx,	,
Baleárskými	baleárský	k2eAgInPc7d1	baleárský
ostrovy	ostrov	k1gInPc7	ostrov
a	a	k8xC	a
západní	západní	k2eAgFnSc7d1	západní
polovinou	polovina	k1gFnSc7	polovina
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgFnPc1d1	důležitá
kolonie	kolonie	k1gFnPc1	kolonie
byly	být	k5eAaImAgFnP	být
také	také	k6eAd1	také
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
Elissar	Elissara	k1gFnPc2	Elissara
(	(	kIx(	(
<g/>
známá	známá	k1gFnSc1	známá
také	také	k9	také
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Alissa	Alissa	k1gFnSc1	Alissa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
arabským	arabský	k2eAgNnSc7d1	arabské
jménem	jméno	k1gNnSc7	jméno
ا	ا	k?	ا
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ا	ا	k?	ا
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ع	ع	k?	ع
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
princeznou	princezna	k1gFnSc7	princezna
z	z	k7c2	z
Tyru	Tyrus	k1gInSc2	Tyrus
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
založila	založit	k5eAaPmAgFnS	založit
Kartágo	Kartágo	k1gNnSc4	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jejího	její	k3xOp3gInSc2	její
vrcholu	vrchol	k1gInSc2	vrchol
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc1	její
metropole	metropole	k1gFnSc1	metropole
nazývána	nazývat	k5eAaImNgFnS	nazývat
"	"	kIx"	"
<g/>
zářící	zářící	k2eAgNnSc1d1	zářící
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
dalším	další	k2eAgInSc7d1	další
300	[number]	k4	300
městům	město	k1gNnPc3	město
kolem	kolo	k1gNnSc7	kolo
západního	západní	k2eAgNnSc2d1	západní
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
vedoucí	vedoucí	k2eAgInSc1d1	vedoucí
fénický	fénický	k2eAgInSc1d1	fénický
punský	punský	k2eAgInSc1d1	punský
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
Elissary	Elissara	k1gFnSc2	Elissara
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
z	z	k7c2	z
Týru	Týrus	k1gInSc2	Týrus
<g/>
,	,	kIx,	,
zavraždil	zavraždit	k5eAaPmAgMnS	zavraždit
jejího	její	k3xOp3gMnSc4	její
manžela	manžel	k1gMnSc4	manžel
<g/>
,	,	kIx,	,
vrchního	vrchní	k2eAgMnSc2d1	vrchní
kněze	kněz	k1gMnSc2	kněz
Sychaea	Sychaeus	k1gMnSc2	Sychaeus
<g/>
.	.	kIx.	.
</s>
<s>
Elissar	Elissar	k1gInSc4	Elissar
utekla	utéct	k5eAaPmAgFnS	utéct
před	před	k7c7	před
tyranií	tyranie	k1gFnSc7	tyranie
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
země	zem	k1gFnSc2	zem
a	a	k8xC	a
založila	založit	k5eAaPmAgFnS	založit
Kartágo	Kartágo	k1gNnSc4	Kartágo
a	a	k8xC	a
následovně	následovně	k6eAd1	následovně
jeho	jeho	k3xOp3gNnSc4	jeho
pozdější	pozdní	k2eAgNnSc4d2	pozdější
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnosti	podrobnost	k1gFnPc1	podrobnost
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
jsou	být	k5eAaImIp3nP	být
útržkovité	útržkovitý	k2eAgInPc1d1	útržkovitý
a	a	k8xC	a
matoucí	matoucí	k2eAgInPc1d1	matoucí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
posloupnost	posloupnost	k1gFnSc1	posloupnost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Justina	Justin	k1gMnSc2	Justin
byla	být	k5eAaImAgFnS	být
princezna	princezna	k1gFnSc1	princezna
Elissar	Elissar	k1gInSc4	Elissar
dcerou	dcera	k1gFnSc7	dcera
krále	král	k1gMnSc4	král
Mattena	Matten	k2eAgMnSc4d1	Matten
z	z	k7c2	z
Tyru	Tyrus	k1gInSc2	Tyrus
(	(	kIx(	(
<g/>
známého	známý	k2eAgMnSc2d1	známý
také	také	k9	také
jako	jako	k9	jako
Muttoial	Muttoial	k1gMnSc1	Muttoial
nebo	nebo	k8xC	nebo
Belus	Belus	k1gMnSc1	Belus
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
odkázal	odkázat	k5eAaPmAgInS	odkázat
trůn	trůn	k1gInSc4	trůn
jí	jíst	k5eAaImIp3nS	jíst
a	a	k8xC	a
jejímu	její	k3xOp3gMnSc3	její
bratrovi	bratr	k1gMnSc3	bratr
<g/>
,	,	kIx,	,
Pygmalionovi	Pygmalion	k1gMnSc3	Pygmalion
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
strýce	strýc	k1gMnSc4	strýc
Acherbase	Acherbasa	k1gFnSc6	Acherbasa
(	(	kIx(	(
<g/>
známého	známý	k2eAgMnSc2d1	známý
též	též	k9	též
jako	jako	k8xC	jako
Sychaeus	Sychaeus	k1gMnSc1	Sychaeus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrchního	vrchní	k2eAgMnSc2d1	vrchní
kněze	kněz	k1gMnSc2	kněz
boha	bůh	k1gMnSc2	bůh
Melqarta	Melqart	k1gMnSc2	Melqart
<g/>
,	,	kIx,	,
muže	muž	k1gMnSc2	muž
s	s	k7c7	s
autoritou	autorita	k1gFnSc7	autorita
a	a	k8xC	a
bohatstvím	bohatství	k1gNnSc7	bohatství
srovnatelným	srovnatelný	k2eAgNnSc7d1	srovnatelné
s	s	k7c7	s
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Pygmalion	Pygmalion	k1gInSc4	Pygmalion
byl	být	k5eAaImAgMnS	být
tyran	tyran	k1gMnSc1	tyran
<g/>
,	,	kIx,	,
milovník	milovník	k1gMnSc1	milovník
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
intrik	intrika	k1gFnPc2	intrika
<g/>
,	,	kIx,	,
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
autoritu	autorita	k1gFnSc4	autorita
a	a	k8xC	a
jmění	jmění	k1gNnSc4	jmění
z	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgMnS	těšit
Acherbas	Acherbas	k1gMnSc1	Acherbas
<g/>
.	.	kIx.	.
</s>
<s>
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
Acherbase	Acherbas	k1gInSc6	Acherbas
zavraždil	zavraždit	k5eAaPmAgMnS	zavraždit
a	a	k8xC	a
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
dokázal	dokázat	k5eAaPmAgMnS	dokázat
svůj	svůj	k3xOyFgInSc4	svůj
zločin	zločin	k1gInSc4	zločin
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
skrývat	skrývat	k5eAaImF	skrývat
<g/>
,	,	kIx,	,
klamající	klamající	k2eAgFnSc1d1	klamající
ji	on	k3xPp3gFnSc4	on
lží	lež	k1gFnSc7	lež
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
jejího	její	k3xOp3gMnSc2	její
manžela	manžel	k1gMnSc2	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
volali	volat	k5eAaImAgMnP	volat
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
Tyru	Tyrus	k1gInSc2	Tyrus
po	po	k7c6	po
jednom	jeden	k4xCgMnSc6	jeden
panovníkovi	panovník	k1gMnSc6	panovník
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
rozpory	rozpor	k1gInPc4	rozpor
v	v	k7c6	v
královské	královský	k2eAgFnSc6d1	královská
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
založila	založit	k5eAaPmAgFnS	založit
Kartágo	Kartágo	k1gNnSc4	Kartágo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
814	[number]	k4	814
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
fénická	fénický	k2eAgFnSc1d1	fénická
princezna	princezna	k1gFnSc1	princezna
Dido	Dido	k6eAd1	Dido
<g/>
.	.	kIx.	.
</s>
<s>
Starověké	starověký	k2eAgFnPc1d1	starověká
báje	báj	k1gFnPc1	báj
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
bratr	bratr	k1gMnSc1	bratr
Pygmalión	Pygmalión	k1gMnSc1	Pygmalión
zabil	zabít	k5eAaPmAgMnS	zabít
jejího	její	k3xOp3gMnSc4	její
manžela	manžel	k1gMnSc4	manžel
Sychaea	Sychaeus	k1gMnSc4	Sychaeus
a	a	k8xC	a
ji	on	k3xPp3gFnSc4	on
sesadil	sesadit	k5eAaPmAgMnS	sesadit
z	z	k7c2	z
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
Dido	Dido	k6eAd1	Dido
opustila	opustit	k5eAaPmAgFnS	opustit
rodný	rodný	k2eAgInSc4d1	rodný
Týros	Týros	k1gInSc4	Týros
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInSc4d1	ležící
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
břehu	břeh	k1gInSc6	břeh
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
a	a	k8xC	a
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
se	se	k3xPyFc4	se
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zakládá	zakládat	k5eAaImIp3nS	zakládat
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
fénicky	fénicky	k6eAd1	fénicky
Kart	Kart	k2eAgInSc4d1	Kart
Hadašt	Hadašt	k1gInSc4	Hadašt
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Karthago	Karthago	k6eAd1	Karthago
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
ale	ale	k8xC	ale
mohla	moct	k5eAaImAgFnS	moct
Dido	Dido	k6eAd1	Dido
založit	založit	k5eAaPmF	založit
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
dohodnout	dohodnout	k5eAaPmF	dohodnout
s	s	k7c7	s
numidským	numidský	k2eAgMnSc7d1	numidský
králem	král	k1gMnSc7	král
Jarbasem	Jarbas	k1gMnSc7	Jarbas
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
dal	dát	k5eAaPmAgMnS	dát
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Užívej	užívat	k5eAaImRp2nS	užívat
pozemek	pozemek	k1gInSc4	pozemek
tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc4d1	velký
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
pokryje	pokrýt	k5eAaPmIp3nS	pokrýt
volská	volský	k2eAgFnSc1d1	volská
kůže	kůže	k1gFnSc1	kůže
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Chytrá	chytrá	k1gFnSc1	chytrá
Dido	Dido	k6eAd1	Dido
ale	ale	k8xC	ale
Jarbase	Jarbasa	k1gFnSc3	Jarbasa
přelstí	přelstít	k5eAaPmIp3nP	přelstít
<g/>
.	.	kIx.	.
</s>
<s>
Kůži	kůže	k1gFnSc4	kůže
rozřezává	rozřezávat	k5eAaImIp3nS	rozřezávat
na	na	k7c4	na
tenké	tenký	k2eAgInPc4d1	tenký
proužky	proužek	k1gInPc4	proužek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
svazuje	svazovat	k5eAaImIp3nS	svazovat
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
a	a	k8xC	a
obkládá	obkládat	k5eAaImIp3nS	obkládat
tak	tak	k9	tak
jimi	on	k3xPp3gInPc7	on
velké	velký	k2eAgNnSc4d1	velké
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jak	jak	k6eAd1	jak
budoucí	budoucí	k2eAgNnSc1d1	budoucí
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
přístav	přístav	k1gInSc1	přístav
i	i	k9	i
skálu	skála	k1gFnSc4	skála
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
později	pozdě	k6eAd2	pozdě
vyroste	vyrůst	k5eAaPmIp3nS	vyrůst
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
památku	památka	k1gFnSc4	památka
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
získává	získávat	k5eAaImIp3nS	získávat
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc4	jméno
Byrsa	Byrs	k1gMnSc2	Byrs
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
ke	k	k7c3	k
"	"	kIx"	"
<g/>
stažené	stažený	k2eAgFnSc3d1	stažená
kůži	kůže	k1gFnSc3	kůže
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
básník	básník	k1gMnSc1	básník
Vergilius	Vergilius	k1gMnSc1	Vergilius
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
báje	báj	k1gFnSc2	báj
ještě	ještě	k9	ještě
příběh	příběh	k1gInSc1	příběh
Aenea	Aeneas	k1gMnSc2	Aeneas
<g/>
,	,	kIx,	,
uprchlíka	uprchlík	k1gMnSc2	uprchlík
z	z	k7c2	z
Tróje	Trója	k1gFnSc2	Trója
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
loď	loď	k1gFnSc1	loď
bouře	bouř	k1gFnPc4	bouř
zahnala	zahnat	k5eAaPmAgFnS	zahnat
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
právě	právě	k6eAd1	právě
založeného	založený	k2eAgNnSc2d1	založené
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Princezna	princezna	k1gFnSc1	princezna
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
a	a	k8xC	a
prožijí	prožít	k5eAaPmIp3nP	prožít
spolu	spolu	k6eAd1	spolu
krátký	krátký	k2eAgInSc4d1	krátký
románek	románek	k1gInSc4	románek
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
se	se	k3xPyFc4	se
ale	ale	k9	ale
nestane	stanout	k5eNaPmIp3nS	stanout
jejím	její	k3xOp3gMnSc7	její
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
kartaginským	kartaginský	k2eAgMnSc7d1	kartaginský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
si	se	k3xPyFc3	se
ona	onen	k3xDgFnSc1	onen
představuje	představovat	k5eAaImIp3nS	představovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
bohů	bůh	k1gMnPc2	bůh
má	mít	k5eAaImIp3nS	mít
založit	založit	k5eAaPmF	založit
Římskou	římský	k2eAgFnSc4d1	římská
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
tedy	tedy	k9	tedy
tajně	tajně	k6eAd1	tajně
vyplouvá	vyplouvat	k5eAaImIp3nS	vyplouvat
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
k	k	k7c3	k
italským	italský	k2eAgInPc3d1	italský
břehům	břeh	k1gInPc3	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Zoufalá	zoufalý	k2eAgFnSc1d1	zoufalá
Dido	Dido	k6eAd1	Dido
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
z	z	k7c2	z
nešťastné	šťastný	k2eNgFnSc2d1	nešťastná
lásky	láska	k1gFnSc2	láska
sama	sám	k3xTgMnSc4	sám
upálí	upálit	k5eAaPmIp3nP	upálit
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
proklíná	proklínat	k5eAaImIp3nS	proklínat
Aenea	Aeneas	k1gMnSc4	Aeneas
a	a	k8xC	a
všechny	všechen	k3xTgMnPc4	všechen
jeho	jeho	k3xOp3gMnPc4	jeho
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Odsud	odsud	k6eAd1	odsud
pramení	pramenit	k5eAaImIp3nS	pramenit
nepřekonatelná	překonatelný	k2eNgFnSc1d1	nepřekonatelná
nenávist	nenávist	k1gFnSc1	nenávist
mezi	mezi	k7c7	mezi
Kartaginci	Kartaginec	k1gMnPc7	Kartaginec
a	a	k8xC	a
Římany	Říman	k1gMnPc7	Říman
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
Vergilius	Vergilius	k1gMnSc1	Vergilius
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
bylo	být	k5eAaImAgNnS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
fénických	fénický	k2eAgFnPc2d1	fénická
osad	osada	k1gFnPc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
pobřeží	pobřeží	k1gNnSc1	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
osídleno	osídlen	k2eAgNnSc1d1	osídleno
rozmanitou	rozmanitý	k2eAgFnSc7d1	rozmanitá
semitsky	semitsky	k6eAd1	semitsky
mluvící	mluvící	k2eAgFnSc7d1	mluvící
populací	populace	k1gFnSc7	populace
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
obývající	obývající	k2eAgFnSc4d1	obývající
oblast	oblast	k1gFnSc4	oblast
současného	současný	k2eAgInSc2d1	současný
Libanonu	Libanon	k1gInSc2	Libanon
nazývali	nazývat	k5eAaImAgMnP	nazývat
svůj	svůj	k3xOyFgInSc4	svůj
jazyk	jazyk	k1gInSc4	jazyk
kanaánštinou	kanaánština	k1gFnSc7	kanaánština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Řekové	Řek	k1gMnPc1	Řek
je	on	k3xPp3gMnPc4	on
uvádějí	uvádět	k5eAaImIp3nP	uvádět
jako	jako	k9	jako
Féničany	Féničan	k1gMnPc4	Féničan
<g/>
.	.	kIx.	.
</s>
<s>
Féničtina	Féničtina	k1gFnSc1	Féničtina
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
starověké	starověký	k2eAgFnSc3d1	starověká
hebrejštině	hebrejština	k1gFnSc3	hebrejština
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
hebrejština	hebrejština	k1gFnSc1	hebrejština
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používána	používán	k2eAgFnSc1d1	používána
jako	jako	k8xS	jako
pomůcka	pomůcka	k1gFnSc1	pomůcka
při	při	k7c6	při
překladech	překlad	k1gInPc6	překlad
fénických	fénický	k2eAgInPc2d1	fénický
nápisů	nápis	k1gInPc2	nápis
<g/>
.	.	kIx.	.
</s>
<s>
Fénická	fénický	k2eAgNnPc1d1	fénické
města	město	k1gNnPc1	město
byla	být	k5eAaImAgNnP	být
vysoce	vysoce	k6eAd1	vysoce
závislá	závislý	k2eAgNnPc1d1	závislé
na	na	k7c6	na
obchodu	obchod	k1gInSc6	obchod
a	a	k8xC	a
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
mnoho	mnoho	k4c4	mnoho
důležitých	důležitý	k2eAgInPc2d1	důležitý
přístavů	přístav	k1gInPc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Fénické	fénický	k2eAgNnSc4d1	fénické
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
byl	být	k5eAaImAgMnS	být
Týros	Týros	k1gMnSc1	Týros
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
kolem	kolem	k7c2	kolem
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
založil	založit	k5eAaPmAgInS	založit
spoustu	spousta	k1gFnSc4	spousta
obchodních	obchodní	k2eAgFnPc2d1	obchodní
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
osad	osada	k1gFnPc2	osada
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozvinulo	rozvinout	k5eAaPmAgNnS	rozvinout
v	v	k7c4	v
samostatná	samostatný	k2eAgNnPc4d1	samostatné
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
místo	místo	k1gNnSc4	místo
k	k	k7c3	k
odpočinku	odpočinek	k1gInSc3	odpočinek
pro	pro	k7c4	pro
obchodní	obchodní	k2eAgFnPc4d1	obchodní
flotily	flotila	k1gFnPc4	flotila
udržující	udržující	k2eAgInSc4d1	udržující
fénický	fénický	k2eAgInSc4d1	fénický
monopol	monopol	k1gInSc4	monopol
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
přírodních	přírodní	k2eAgFnPc2d1	přírodní
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
aby	aby	kYmCp3nP	aby
sami	sám	k3xTgMnPc1	sám
vedli	vést	k5eAaImAgMnP	vést
obchod	obchod	k1gInSc4	obchod
<g/>
,	,	kIx,	,
založili	založit	k5eAaPmAgMnP	založit
Féničané	Féničan	k1gMnPc1	Féničan
podél	podél	k7c2	podél
břehů	břeh	k1gInPc2	břeh
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
mnoho	mnoho	k4c4	mnoho
koloniálních	koloniální	k2eAgNnPc2d1	koloniální
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zakládání	zakládání	k1gNnSc3	zakládání
svých	svůj	k3xOyFgNnPc2	svůj
měst	město	k1gNnPc2	město
byli	být	k5eAaImAgMnP	být
povzbuzováni	povzbuzován	k2eAgMnPc1d1	povzbuzován
potřebou	potřeba	k1gFnSc7	potřeba
k	k	k7c3	k
oživení	oživení	k1gNnSc3	oživení
obchodu	obchod	k1gInSc2	obchod
aby	aby	kYmCp3nS	aby
mohli	moct	k5eAaImAgMnP	moct
zaplatit	zaplatit	k5eAaPmF	zaplatit
poplatky	poplatek	k1gInPc1	poplatek
vyžadované	vyžadovaný	k2eAgInPc1d1	vyžadovaný
Tyrem	Tyr	k1gInSc7	Tyr
<g/>
,	,	kIx,	,
Sidónem	Sidón	k1gInSc7	Sidón
a	a	k8xC	a
Byblem	Bybl	k1gInSc7	Bybl
následované	následovaný	k2eAgFnPc1d1	následovaná
říšemi	říš	k1gFnPc7	říš
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jim	on	k3xPp3gFnPc3	on
vládly	vládnout	k5eAaImAgFnP	vládnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
kompletní	kompletní	k2eAgFnSc2d1	kompletní
řecké	řecký	k2eAgFnSc2d1	řecká
kolonizace	kolonizace	k1gFnSc2	kolonizace
těch	ten	k3xDgFnPc2	ten
částí	část	k1gFnPc2	část
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgFnPc1d1	vhodná
k	k	k7c3	k
obchodu	obchod	k1gInSc3	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Féničané	Féničan	k1gMnPc1	Féničan
postrádali	postrádat	k5eAaImAgMnP	postrádat
dostatek	dostatek	k1gInSc4	dostatek
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
založit	založit	k5eAaPmF	založit
soběstačná	soběstačný	k2eAgNnPc4d1	soběstačné
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
měst	město	k1gNnPc2	město
měla	mít	k5eAaImAgFnS	mít
méně	málo	k6eAd2	málo
než	než	k8xS	než
1000	[number]	k4	1000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Kartágo	Kartágo	k1gNnSc1	Kartágo
a	a	k8xC	a
pár	pár	k4xCyI	pár
dalších	další	k2eAgNnPc2d1	další
měst	město	k1gNnPc2	město
se	se	k3xPyFc4	se
rozrostlo	rozrůst	k5eAaPmAgNnS	rozrůst
v	v	k7c4	v
ohromné	ohromný	k2eAgFnPc4d1	ohromná
metropole	metropol	k1gFnPc4	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Nějakých	nějaký	k3yIgNnPc2	nějaký
300	[number]	k4	300
kolonií	kolonie	k1gFnPc2	kolonie
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
Tunisku	Tunisko	k1gNnSc6	Tunisko
<g/>
,	,	kIx,	,
Maroku	Maroko	k1gNnSc6	Maroko
<g/>
,	,	kIx,	,
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
<g/>
,	,	kIx,	,
Ibérii	Ibérie	k1gFnSc6	Ibérie
a	a	k8xC	a
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
menším	malý	k2eAgInSc6d2	menší
rozsahu	rozsah	k1gInSc6	rozsah
také	také	k9	také
na	na	k7c6	na
vyprahlých	vyprahlý	k2eAgInPc6d1	vyprahlý
březích	břeh	k1gInPc6	břeh
Libye	Libye	k1gFnSc2	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Féničané	Féničan	k1gMnPc1	Féničan
kontrolovali	kontrolovat	k5eAaImAgMnP	kontrolovat
Kypr	Kypr	k1gInSc4	Kypr
<g/>
,	,	kIx,	,
Sardínii	Sardínie	k1gFnSc4	Sardínie
<g/>
,	,	kIx,	,
Korsiku	Korsika	k1gFnSc4	Korsika
a	a	k8xC	a
Baleárské	baleárský	k2eAgInPc4d1	baleárský
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
také	také	k9	také
minoritní	minoritní	k2eAgFnSc2d1	minoritní
oblasti	oblast	k1gFnSc2	oblast
na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
a	a	k8xC	a
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgNnSc4d2	pozdější
osídlení	osídlení	k1gNnSc4	osídlení
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
ustavičných	ustavičný	k2eAgInPc2d1	ustavičný
konfliktů	konflikt	k1gInPc2	konflikt
s	s	k7c7	s
Řeky	Řek	k1gMnPc7	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Féničané	Féničan	k1gMnPc1	Féničan
na	na	k7c4	na
omezenou	omezený	k2eAgFnSc4d1	omezená
dobu	doba	k1gFnSc4	doba
dokázali	dokázat	k5eAaPmAgMnP	dokázat
ovládnout	ovládnout	k5eAaPmF	ovládnout
celou	celý	k2eAgFnSc4d1	celá
Sicílii	Sicílie	k1gFnSc4	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
později	pozdě	k6eAd2	pozdě
přešla	přejít	k5eAaPmAgFnS	přejít
pod	pod	k7c4	pod
vedení	vedení	k1gNnSc4	vedení
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obratem	obratem	k6eAd1	obratem
odeslalo	odeslat	k5eAaPmAgNnS	odeslat
své	svůj	k3xOyFgMnPc4	svůj
vlastní	vlastní	k2eAgMnPc4d1	vlastní
kolonisty	kolonista	k1gMnPc4	kolonista
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
nových	nový	k2eAgNnPc2d1	nové
měst	město	k1gNnPc2	město
nebo	nebo	k8xC	nebo
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
těch	ten	k3xDgInPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
odmítaly	odmítat	k5eAaImAgInP	odmítat
Týros	Týros	k1gInSc4	Týros
a	a	k8xC	a
Sidón	Sidón	k1gInSc4	Sidón
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
kolonie	kolonie	k1gFnPc1	kolonie
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
cestách	cesta	k1gFnPc6	cesta
k	k	k7c3	k
iberskému	iberský	k2eAgNnSc3d1	Iberské
minerálnímu	minerální	k2eAgNnSc3d1	minerální
bohatství	bohatství	k1gNnSc3	bohatství
-	-	kIx~	-
podél	podél	k6eAd1	podél
afrického	africký	k2eAgNnSc2d1	africké
pobřeží	pobřeží	k1gNnSc2	pobřeží
a	a	k8xC	a
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
,	,	kIx,	,
Sardínii	Sardínie	k1gFnSc6	Sardínie
a	a	k8xC	a
Baleárských	baleárský	k2eAgInPc6d1	baleárský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
Fénického	fénický	k2eAgInSc2d1	fénický
světa	svět	k1gInSc2	svět
by	by	kYmCp3nS	by
Týros	Týrosa	k1gFnPc2	Týrosa
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgNnSc1d1	sloužící
jako	jako	k8xS	jako
ekonomické	ekonomický	k2eAgNnSc1d1	ekonomické
a	a	k8xC	a
politické	politický	k2eAgNnSc1d1	politické
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k1gFnSc1	moc
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
ubývala	ubývat	k5eAaImAgFnS	ubývat
následkem	následkem	k7c2	následkem
mnoha	mnoho	k4c2	mnoho
obléhání	obléhání	k1gNnPc2	obléhání
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
následném	následný	k2eAgInSc6d1	následný
zničením	zničení	k1gNnSc7	zničení
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Velikým	veliký	k2eAgMnSc7d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
vůdce	vůdce	k1gMnPc4	vůdce
pak	pak	k6eAd1	pak
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
Sidón	Sidón	k1gInSc4	Sidón
<g/>
,	,	kIx,	,
a	a	k8xC	a
případně	případně	k6eAd1	případně
Kartágo	Kartágo	k1gNnSc1	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
kolonie	kolonie	k1gFnSc1	kolonie
sice	sice	k8xC	sice
platila	platit	k5eAaImAgFnS	platit
poplatky	poplatek	k1gInPc4	poplatek
buď	buď	k8xC	buď
Tyru	Tyra	k1gFnSc4	Tyra
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Sidonu	Sidona	k1gFnSc4	Sidona
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
neměl	mít	k5eNaImAgMnS	mít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
koloniemi	kolonie	k1gFnPc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
Kartáginci	Kartáginec	k1gMnPc1	Kartáginec
ustanovili	ustanovit	k5eAaPmAgMnP	ustanovit
své	svůj	k3xOyFgMnPc4	svůj
vlastní	vlastní	k2eAgMnPc4d1	vlastní
starosty	starosta	k1gMnPc4	starosta
k	k	k7c3	k
vládě	vláda	k1gFnSc3	vláda
nad	nad	k7c7	nad
městy	město	k1gNnPc7	město
a	a	k8xC	a
samo	sám	k3xTgNnSc1	sám
Kartágo	Kartágo	k1gNnSc1	Kartágo
si	se	k3xPyFc3	se
ponechalo	ponechat	k5eAaPmAgNnS	ponechat
mnoho	mnoho	k6eAd1	mnoho
přímé	přímý	k2eAgFnPc4d1	přímá
kontroly	kontrola	k1gFnPc4	kontrola
nad	nad	k7c7	nad
koloniemi	kolonie	k1gFnPc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
politika	politika	k1gFnSc1	politika
vedla	vést	k5eAaImAgFnS	vést
nakonec	nakonec	k6eAd1	nakonec
k	k	k7c3	k
přeběhnutí	přeběhnutí	k1gNnSc3	přeběhnutí
iberských	iberský	k2eAgNnPc2d1	Iberské
měst	město	k1gNnPc2	město
k	k	k7c3	k
Římanům	Říman	k1gMnPc3	Říman
během	během	k7c2	během
punských	punský	k2eAgFnPc2d1	punská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Kartáginci	Kartáginec	k1gMnPc1	Kartáginec
mluvili	mluvit	k5eAaImAgMnP	mluvit
punsky	punsky	k6eAd1	punsky
<g/>
,	,	kIx,	,
dialektem	dialekt	k1gInSc7	dialekt
féničtiny	féničtina	k1gFnSc2	féničtina
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
na	na	k7c6	na
útesu	útes	k1gInSc6	útes
s	s	k7c7	s
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Pozice	pozice	k1gFnSc1	pozice
města	město	k1gNnSc2	město
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
učinila	učinit	k5eAaPmAgFnS	učinit
mistra	mistr	k1gMnSc2	mistr
středomořského	středomořský	k2eAgInSc2d1	středomořský
námořního	námořní	k2eAgInSc2d1	námořní
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
lodě	loď	k1gFnPc1	loď
přeplouvající	přeplouvající	k2eAgFnPc1d1	přeplouvající
moře	moře	k1gNnSc4	moře
musely	muset	k5eAaImAgFnP	muset
proplout	proplout	k5eAaPmF	proplout
mezi	mezi	k7c7	mezi
Sicílií	Sicílie	k1gFnSc7	Sicílie
a	a	k8xC	a
pobřežím	pobřeží	k1gNnSc7	pobřeží
Tuniska	Tunisko	k1gNnSc2	Tunisko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
Kartágo	Kartágo	k1gNnSc1	Kartágo
postaveno	postavit	k5eAaPmNgNnS	postavit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dopřálo	dopřát	k5eAaPmAgNnS	dopřát
městu	město	k1gNnSc3	město
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
města	město	k1gNnSc2	město
byly	být	k5eAaImAgInP	být
vybudovány	vybudován	k2eAgInPc1d1	vybudován
dva	dva	k4xCgInPc1	dva
velké	velký	k2eAgInPc1d1	velký
umělé	umělý	k2eAgInPc1d1	umělý
přístavy	přístav	k1gInPc1	přístav
-	-	kIx~	-
jeden	jeden	k4xCgInSc1	jeden
jako	jako	k9	jako
kotviště	kotviště	k1gNnPc4	kotviště
obrovské	obrovský	k2eAgFnSc2d1	obrovská
městské	městský	k2eAgFnSc2d1	městská
flotily	flotila	k1gFnSc2	flotila
220	[number]	k4	220
válečných	válečná	k1gFnPc2	válečná
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc4	dva
přístavy	přístav	k1gInPc4	přístav
přehlížela	přehlížet	k5eAaImAgFnS	přehlížet
obezděná	obezděný	k2eAgFnSc1d1	obezděná
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
mělo	mít	k5eAaImAgNnS	mít
pevné	pevný	k2eAgFnPc4d1	pevná
hradby	hradba	k1gFnPc4	hradba
<g/>
,	,	kIx,	,
37	[number]	k4	37
km	km	kA	km
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgFnPc4d2	delší
než	než	k8xS	než
hradby	hradba	k1gFnPc1	hradba
srovnatelných	srovnatelný	k2eAgNnPc2d1	srovnatelné
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hradeb	hradba	k1gFnPc2	hradba
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
tak	tak	k6eAd1	tak
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
méně	málo	k6eAd2	málo
působivé	působivý	k2eAgNnSc1d1	působivé
jak	jak	k8xC	jak
kartáginská	kartáginský	k2eAgFnSc1d1	kartáginská
kontrola	kontrola	k1gFnSc1	kontrola
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
útok	útok	k1gInSc1	útok
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
4-5	[number]	k4	4-5
km	km	kA	km
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
hradby	hradba	k1gFnSc2	hradba
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
úžině	úžina	k1gFnSc6	úžina
na	na	k7c6	na
západě	západ	k1gInSc6	západ
byly	být	k5eAaImAgInP	být
skutečně	skutečně	k6eAd1	skutečně
neproniknutelné	proniknutelný	k2eNgInPc1d1	neproniknutelný
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
mělo	mít	k5eAaImAgNnS	mít
také	také	k6eAd1	také
velké	velký	k2eAgNnSc4d1	velké
pohřebiště	pohřebiště	k1gNnSc4	pohřebiště
(	(	kIx(	(
<g/>
nekropoli	nekropole	k1gFnSc4	nekropole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
tržiště	tržiště	k1gNnSc4	tržiště
<g/>
,	,	kIx,	,
radnici	radnice	k1gFnSc4	radnice
<g/>
,	,	kIx,	,
pevnosti	pevnost	k1gFnSc3	pevnost
a	a	k8xC	a
divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
stejně	stejně	k6eAd1	stejně
velkých	velký	k2eAgNnPc2d1	velké
sídlišť	sídliště	k1gNnPc2	sídliště
se	s	k7c7	s
stejným	stejný	k2eAgNnSc7d1	stejné
rozložením	rozložení	k1gNnSc7	rozložení
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
stála	stát	k5eAaImAgFnS	stát
vysoká	vysoký	k2eAgFnSc1d1	vysoká
citadela	citadela	k1gFnSc1	citadela
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Byrsa	Byrsa	k1gFnSc1	Byrsa
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
měst	město	k1gNnPc2	město
helénistického	helénistický	k2eAgNnSc2d1	helénistické
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
některých	některý	k3yIgInPc2	některý
odhadů	odhad	k1gInPc2	odhad
byla	být	k5eAaImAgFnS	být
větší	veliký	k2eAgFnSc1d2	veliký
pouze	pouze	k6eAd1	pouze
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
<g/>
)	)	kIx)	)
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
patřilo	patřit	k5eAaImAgNnS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgNnPc4d3	veliký
města	město	k1gNnPc4	město
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
Kartága	Kartágo	k1gNnSc2	Kartágo
byla	být	k5eAaImAgFnS	být
silně	silně	k6eAd1	silně
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
obchodu	obchod	k1gInSc6	obchod
s	s	k7c7	s
Tartéssem	Tartéss	k1gMnSc7	Tartéss
a	a	k8xC	a
jinými	jiný	k2eAgNnPc7d1	jiné
městy	město	k1gNnPc7	město
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterých	který	k3yRgFnPc2	který
získávala	získávat	k5eAaImAgFnS	získávat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
důležitější	důležitý	k2eAgFnSc4d2	důležitější
cínovou	cínový	k2eAgFnSc4d1	cínová
rudu	ruda	k1gFnSc4	ruda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
přísadou	přísada	k1gFnSc7	přísada
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
bronzových	bronzový	k2eAgInPc2d1	bronzový
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
obchodní	obchodní	k2eAgInPc1d1	obchodní
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Ibery	Iber	k1gMnPc7	Iber
a	a	k8xC	a
námořní	námořní	k2eAgFnSc1d1	námořní
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zajistila	zajistit	k5eAaPmAgFnS	zajistit
kartaginský	kartaginský	k2eAgInSc4d1	kartaginský
monopol	monopol	k1gInSc4	monopol
v	v	k7c6	v
obchodu	obchod	k1gInSc6	obchod
s	s	k7c7	s
Británií	Británie	k1gFnSc7	Británie
bohatou	bohatý	k2eAgFnSc7d1	bohatá
na	na	k7c4	na
cín	cín	k1gInSc4	cín
a	a	k8xC	a
Kanárskými	kanárský	k2eAgInPc7d1	kanárský
ostrovy	ostrov	k1gInPc7	ostrov
<g/>
,	,	kIx,	,
jí	on	k3xPp3gFnSc7	on
umožnily	umožnit	k5eAaPmAgFnP	umožnit
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
jediným	jediný	k2eAgMnSc7d1	jediný
významným	významný	k2eAgMnSc7d1	významný
překupníkem	překupník	k1gMnSc7	překupník
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
výrobcem	výrobce	k1gMnSc7	výrobce
bronzu	bronz	k1gInSc2	bronz
<g/>
.	.	kIx.	.
</s>
<s>
Udržování	udržování	k1gNnSc1	udržování
tohoto	tento	k3xDgInSc2	tento
monopolu	monopol	k1gInSc2	monopol
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
zdrojů	zdroj	k1gInPc2	zdroj
kartáginské	kartáginský	k2eAgFnSc2d1	kartáginská
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
prosperity	prosperita	k1gFnSc2	prosperita
a	a	k8xC	a
kartáginský	kartáginský	k2eAgMnSc1d1	kartáginský
obchodník	obchodník	k1gMnSc1	obchodník
by	by	kYmCp3nS	by
raději	rád	k6eAd2	rád
rozbil	rozbít	k5eAaPmAgInS	rozbít
svou	svůj	k3xOyFgFnSc4	svůj
loď	loď	k1gFnSc4	loď
o	o	k7c4	o
skalnaté	skalnatý	k2eAgInPc4d1	skalnatý
břehy	břeh	k1gInPc4	břeh
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
odhalil	odhalit	k5eAaPmAgMnS	odhalit
kterémukoli	kterýkoli	k3yIgMnSc3	kterýkoli
rivalovi	rival	k1gMnSc3	rival
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
bezpečně	bezpečně	k6eAd1	bezpečně
přiblížit	přiblížit	k5eAaPmF	přiblížit
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Kartágo	Kartágo	k1gNnSc4	Kartágo
jediným	jediný	k2eAgMnSc7d1	jediný
významným	významný	k2eAgMnSc7d1	významný
dodavatelem	dodavatel	k1gMnSc7	dodavatel
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
pozice	pozice	k1gFnSc1	pozice
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
kontrola	kontrola	k1gFnSc1	kontrola
nad	nad	k7c7	nad
vodami	voda	k1gFnPc7	voda
mezi	mezi	k7c7	mezi
Sicílií	Sicílie	k1gFnSc7	Sicílie
a	a	k8xC	a
Tuniskem	Tunisko	k1gNnSc7	Tunisko
mu	on	k3xPp3gMnSc3	on
umožňovaly	umožňovat	k5eAaImAgInP	umožňovat
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
zásobování	zásobování	k1gNnSc2	zásobování
východních	východní	k2eAgInPc2d1	východní
národů	národ	k1gInPc2	národ
tímto	tento	k3xDgInSc7	tento
kovem	kov	k1gInSc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
největším	veliký	k2eAgMnSc7d3	veliký
středomořským	středomořský	k2eAgMnSc7d1	středomořský
producentem	producent	k1gMnSc7	producent
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
těženého	těžený	k2eAgNnSc2d1	těžené
v	v	k7c6	v
Ibérii	Ibérie	k1gFnSc6	Ibérie
a	a	k8xC	a
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
monopolu	monopol	k1gInSc6	monopol
s	s	k7c7	s
cínem	cín	k1gInSc7	cín
bylo	být	k5eAaImAgNnS	být
stříbro	stříbro	k1gNnSc1	stříbro
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
nejvýdělečnějších	výdělečný	k2eAgInPc2d3	nejvýdělečnější
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
důl	důl	k1gInSc1	důl
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
poskytoval	poskytovat	k5eAaImAgMnS	poskytovat
Hannibalovi	Hannibal	k1gMnSc3	Hannibal
300	[number]	k4	300
římských	římský	k2eAgFnPc2d1	římská
liber	libra	k1gFnPc2	libra
(	(	kIx(	(
<g/>
98,235	[number]	k4	98,235
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
stříbra	stříbro	k1gNnSc2	stříbro
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Kartáginská	kartáginský	k2eAgFnSc1d1	kartáginská
ekonomika	ekonomika	k1gFnSc1	ekonomika
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
rozšíření	rozšíření	k1gNnSc4	rozšíření
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
vlivu	vliv	k1gInSc2	vliv
svého	svůj	k3xOyFgNnSc2	svůj
mateřského	mateřský	k2eAgNnSc2d1	mateřské
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Tyru	Tyrus	k1gInSc2	Tyrus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
početná	početný	k2eAgFnSc1d1	početná
obchodní	obchodní	k2eAgFnSc1d1	obchodní
flotila	flotila	k1gFnSc1	flotila
putovala	putovat	k5eAaImAgFnS	putovat
po	po	k7c6	po
obchodních	obchodní	k2eAgFnPc6d1	obchodní
trasách	trasa	k1gFnPc6	trasa
zmapovaných	zmapovaný	k2eAgFnPc6d1	zmapovaná
Tyrem	Tyr	k1gInSc7	Tyr
<g/>
,	,	kIx,	,
a	a	k8xC	a
Kartágo	Kartágo	k1gNnSc1	Kartágo
po	po	k7c6	po
Tyru	Tyrus	k1gInSc6	Tyrus
zdědilo	zdědit	k5eAaPmAgNnS	zdědit
i	i	k9	i
umění	umění	k1gNnSc1	umění
vyrábět	vyrábět	k5eAaImF	vyrábět
velmi	velmi	k6eAd1	velmi
hodnotné	hodnotný	k2eAgNnSc1d1	hodnotné
barvivo	barvivo	k1gNnSc1	barvivo
-	-	kIx~	-
tyrský	tyrský	k2eAgInSc1d1	tyrský
purpur	purpur	k1gInSc1	purpur
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Středomoří	středomoří	k1gNnSc2	středomoří
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejcennějších	cenný	k2eAgNnPc2d3	nejcennější
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
mající	mající	k2eAgFnSc4d1	mající
cenu	cena	k1gFnSc4	cena
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
násobku	násobek	k1gInSc2	násobek
jeho	jeho	k3xOp3gFnSc2	jeho
váhy	váha	k1gFnSc2	váha
ve	v	k7c6	v
zlatě	zlato	k1gNnSc6	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Vysocí	vysoký	k2eAgMnPc1d1	vysoký
římští	římský	k2eAgMnPc1d1	římský
úředníci	úředník	k1gMnPc1	úředník
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
dovolit	dovolit	k5eAaPmF	dovolit
tógy	tóga	k1gFnPc4	tóga
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
úzkým	úzký	k2eAgInSc7d1	úzký
proužkem	proužek	k1gInSc7	proužek
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
také	také	k9	také
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
méně	málo	k6eAd2	málo
hodnotné	hodnotný	k2eAgInPc4d1	hodnotný
karmínové	karmínový	k2eAgInPc4d1	karmínový
pigmenty	pigment	k1gInPc4	pigment
z	z	k7c2	z
šarlatu	šarlat	k1gInSc2	šarlat
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
jemně	jemně	k6eAd1	jemně
vyšívané	vyšívaný	k2eAgFnPc4d1	vyšívaná
a	a	k8xC	a
barvené	barvený	k2eAgFnPc4d1	barvená
textilie	textilie	k1gFnPc4	textilie
z	z	k7c2	z
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
,	,	kIx,	,
lnu	len	k1gInSc2	len
<g/>
,	,	kIx,	,
vlny	vlna	k1gFnSc2	vlna
a	a	k8xC	a
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
,	,	kIx,	,
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
i	i	k8xC	i
užitnou	užitný	k2eAgFnSc4d1	užitná
keramiku	keramika	k1gFnSc4	keramika
<g/>
,	,	kIx,	,
fajáns	fajáns	k1gFnSc4	fajáns
<g/>
,	,	kIx,	,
kadidla	kadidlo	k1gNnPc4	kadidlo
a	a	k8xC	a
parfémy	parfém	k1gInPc4	parfém
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
řemeslníci	řemeslník	k1gMnPc1	řemeslník
pracovali	pracovat	k5eAaImAgMnP	pracovat
se	s	k7c7	s
sklem	sklo	k1gNnSc7	sklo
<g/>
,	,	kIx,	,
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
,	,	kIx,	,
alabastrem	alabastr	k1gInSc7	alabastr
<g/>
,	,	kIx,	,
slonovinou	slonovina	k1gFnSc7	slonovina
<g/>
,	,	kIx,	,
bronzem	bronz	k1gInSc7	bronz
<g/>
,	,	kIx,	,
mosazí	mosaz	k1gFnSc7	mosaz
<g/>
,	,	kIx,	,
olovem	olovo	k1gNnSc7	olovo
<g/>
,	,	kIx,	,
zlatem	zlato	k1gNnSc7	zlato
<g/>
,	,	kIx,	,
stříbrem	stříbro	k1gNnSc7	stříbro
a	a	k8xC	a
drahými	drahý	k2eAgInPc7d1	drahý
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
široký	široký	k2eAgInSc4d1	široký
sortiment	sortiment	k1gInSc4	sortiment
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
ceněného	ceněný	k2eAgInSc2d1	ceněný
nábytku	nábytek	k1gInSc2	nábytek
a	a	k8xC	a
postelí	postel	k1gFnPc2	postel
<g/>
,	,	kIx,	,
ložního	ložní	k2eAgNnSc2d1	ložní
prádla	prádlo	k1gNnSc2	prádlo
a	a	k8xC	a
polštářů	polštář	k1gInPc2	polštář
<g/>
,	,	kIx,	,
šperků	šperk	k1gInPc2	šperk
<g/>
,	,	kIx,	,
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
kuchyňského	kuchyňský	k2eAgNnSc2d1	kuchyňské
nádobí	nádobí	k1gNnSc2	nádobí
<g/>
.	.	kIx.	.
</s>
<s>
Přiváželo	přivážet	k5eAaImAgNnS	přivážet
nasolené	nasolený	k2eAgFnPc4d1	nasolená
ryby	ryba	k1gFnPc4	ryba
z	z	k7c2	z
Atlantiku	Atlantik	k1gInSc2	Atlantik
a	a	k8xC	a
rybí	rybí	k2eAgFnSc2d1	rybí
omáčky	omáčka	k1gFnSc2	omáčka
a	a	k8xC	a
zprostředkovávalo	zprostředkovávat	k5eAaImAgNnS	zprostředkovávat
prodej	prodej	k1gFnSc4	prodej
řemeslných	řemeslný	k2eAgInPc2d1	řemeslný
<g/>
,	,	kIx,	,
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
a	a	k8xC	a
přírodních	přírodní	k2eAgInPc2d1	přírodní
produktů	produkt	k1gInPc2	produkt
všem	všecek	k3xTgMnPc3	všecek
obyvatelům	obyvatel	k1gMnPc3	obyvatel
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
mělo	mít	k5eAaImAgNnS	mít
Kartágo	Kartágo	k1gNnSc1	Kartágo
vysoce	vysoce	k6eAd1	vysoce
pokročilé	pokročilý	k2eAgNnSc4d1	pokročilé
a	a	k8xC	a
produktivní	produktivní	k2eAgNnSc4d1	produktivní
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
,	,	kIx,	,
používající	používající	k2eAgInPc4d1	používající
železné	železný	k2eAgInPc4d1	železný
pluhy	pluh	k1gInPc4	pluh
<g/>
,	,	kIx,	,
zavlažování	zavlažování	k1gNnSc4	zavlažování
a	a	k8xC	a
střídavé	střídavý	k2eAgNnSc4d1	střídavé
obhospodařování	obhospodařování	k1gNnSc4	obhospodařování
<g/>
.	.	kIx.	.
</s>
<s>
Mago	Maga	k1gFnSc5	Maga
napsal	napsat	k5eAaPmAgMnS	napsat
slavné	slavný	k2eAgNnSc1d1	slavné
pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Římané	Říman	k1gMnPc1	Říman
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Kartága	Kartágo	k1gNnSc2	Kartágo
nařízením	nařízení	k1gNnSc7	nařízení
přeložili	přeložit	k5eAaPmAgMnP	přeložit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
punské	punský	k2eAgFnSc6d1	punská
válce	válka	k1gFnSc6	válka
Hannibal	Hannibal	k1gInSc1	Hannibal
upřednostnil	upřednostnit	k5eAaPmAgInS	upřednostnit
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pomohl	pomoct	k5eAaPmAgInS	pomoct
obnovit	obnovit	k5eAaPmF	obnovit
kartáginskou	kartáginský	k2eAgFnSc4d1	kartáginská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
aby	aby	kYmCp3nP	aby
Římu	Řím	k1gInSc3	Řím
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
válečné	válečný	k2eAgFnPc4d1	válečná
odškodné	odškodná	k1gFnPc4	odškodná
800	[number]	k4	800
000	[number]	k4	000
římských	římský	k2eAgFnPc2d1	římská
liber	libra	k1gFnPc2	libra
(	(	kIx(	(
<g/>
261	[number]	k4	261
960	[number]	k4	960
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
byl	být	k5eAaImAgInS	být
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
pěstovalo	pěstovat	k5eAaImAgNnS	pěstovat
víno	víno	k1gNnSc4	víno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
Etrurii	Etrurie	k1gFnSc6	Etrurie
a	a	k8xC	a
Řecku	Řecko	k1gNnSc6	Řecko
vysoce	vysoce	k6eAd1	vysoce
ceněné	ceněný	k2eAgInPc1d1	ceněný
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
byl	být	k5eAaImAgMnS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
konzumentem	konzument	k1gMnSc7	konzument
rozinek	rozinka	k1gFnPc2	rozinka
<g/>
,	,	kIx,	,
kartáginské	kartáginský	k2eAgFnPc4d1	kartáginská
speciality	specialita	k1gFnPc4	specialita
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
pěstováno	pěstován	k2eAgNnSc1d1	pěstováno
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
obilí	obilí	k1gNnSc1	obilí
<g/>
,	,	kIx,	,
hrozny	hrozen	k1gInPc1	hrozen
<g/>
,	,	kIx,	,
datle	datle	k1gFnPc1	datle
a	a	k8xC	a
olivy	oliva	k1gFnPc1	oliva
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyvážen	vyvážen	k2eAgInSc1d1	vyvážen
olivový	olivový	k2eAgInSc1d1	olivový
olej	olej	k1gInSc1	olej
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
konkuroval	konkurovat	k5eAaImAgMnS	konkurovat
řeckému	řecký	k2eAgNnSc3d1	řecké
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
také	také	k9	také
chovalo	chovat	k5eAaImAgNnS	chovat
skvělé	skvělý	k2eAgNnSc1d1	skvělé
koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgFnPc1d1	podobná
dnešním	dnešní	k2eAgMnPc3d1	dnešní
arabským	arabský	k2eAgMnPc3d1	arabský
koním	kůň	k1gMnPc3	kůň
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byly	být	k5eAaImAgFnP	být
značně	značně	k6eAd1	značně
ceněni	ceněn	k2eAgMnPc1d1	ceněn
a	a	k8xC	a
vyváženi	vyvážen	k2eAgMnPc1d1	vyvážen
<g/>
.	.	kIx.	.
</s>
<s>
Kartáginské	kartáginský	k2eAgFnPc1d1	kartáginská
obchodní	obchodní	k2eAgFnPc1d1	obchodní
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
překonaly	překonat	k5eAaPmAgFnP	překonat
dokonce	dokonce	k9	dokonce
ty	ten	k3xDgFnPc1	ten
z	z	k7c2	z
Levanty	Levanta	k1gFnSc2	Levanta
<g/>
,	,	kIx,	,
navštívily	navštívit	k5eAaPmAgFnP	navštívit
každý	každý	k3xTgInSc4	každý
důležitý	důležitý	k2eAgInSc4d1	důležitý
přístav	přístav	k1gInSc4	přístav
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
na	na	k7c6	na
Kanárských	kanárský	k2eAgInPc6d1	kanárský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
lodě	loď	k1gFnPc1	loď
byly	být	k5eAaImAgFnP	být
schopny	schopen	k2eAgFnPc1d1	schopna
převážet	převážet	k5eAaImF	převážet
přes	přes	k7c4	přes
100	[number]	k4	100
tun	tuna	k1gFnPc2	tuna
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
flotila	flotila	k1gFnSc1	flotila
Kartága	Kartágo	k1gNnSc2	Kartágo
byla	být	k5eAaImAgFnS	být
velikostí	velikost	k1gFnSc7	velikost
a	a	k8xC	a
tonáží	tonáž	k1gFnPc2	tonáž
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
flotilami	flotila	k1gFnPc7	flotila
hlavních	hlavní	k2eAgFnPc2d1	hlavní
evropských	evropský	k2eAgFnPc2d1	Evropská
mocností	mocnost	k1gFnPc2	mocnost
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
Kupci	kupec	k1gMnPc1	kupec
zpočátku	zpočátku	k6eAd1	zpočátku
dávali	dávat	k5eAaImAgMnP	dávat
přednost	přednost	k1gFnSc4	přednost
východním	východní	k2eAgInPc3d1	východní
přístavům	přístav	k1gInPc3	přístav
<g/>
:	:	kIx,	:
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Kypr	Kypr	k1gInSc1	Kypr
a	a	k8xC	a
Malá	malý	k2eAgFnSc1d1	malá
Asie	Asie	k1gFnSc1	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
poté	poté	k6eAd1	poté
co	co	k9	co
se	se	k3xPyFc4	se
Kartágo	Kartágo	k1gNnSc1	Kartágo
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
řeckými	řecký	k2eAgMnPc7d1	řecký
kolonisty	kolonista	k1gMnPc7	kolonista
kvůli	kvůli	k7c3	kvůli
kontrole	kontrola	k1gFnSc3	kontrola
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
,	,	kIx,	,
zavedlo	zavést	k5eAaPmAgNnS	zavést
obchod	obchod	k1gInSc4	obchod
se	s	k7c7	s
západním	západní	k2eAgNnSc7d1	západní
Středomořím	středomoří	k1gNnSc7	středomoří
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
Etrusky	Etrusk	k1gMnPc7	Etrusk
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
také	také	k6eAd1	také
vypravilo	vypravit	k5eAaPmAgNnS	vypravit
karavany	karavana	k1gFnPc4	karavana
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Persie	Persie	k1gFnSc2	Persie
<g/>
.	.	kIx.	.
</s>
<s>
Směňovali	směňovat	k5eAaImAgMnP	směňovat
své	svůj	k3xOyFgNnSc4	svůj
zboží	zboží	k1gNnSc4	zboží
a	a	k8xC	a
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
výrobky	výrobek	k1gInPc4	výrobek
s	s	k7c7	s
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
i	i	k8xC	i
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
Afriky	Afrika	k1gFnSc2	Afrika
za	za	k7c4	za
sůl	sůl	k1gFnSc4	sůl
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
,	,	kIx,	,
slonovinu	slonovina	k1gFnSc4	slonovina
<g/>
,	,	kIx,	,
ebenové	ebenový	k2eAgNnSc4d1	ebenové
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
,	,	kIx,	,
opice	opice	k1gFnPc4	opice
<g/>
,	,	kIx,	,
pávy	páv	k1gMnPc4	páv
<g/>
,	,	kIx,	,
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
usně	useň	k1gFnSc2	useň
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
obchodníci	obchodník	k1gMnPc1	obchodník
vynalezli	vynaleznout	k5eAaPmAgMnP	vynaleznout
aukční	aukční	k2eAgInSc4d1	aukční
prodej	prodej	k1gInSc4	prodej
a	a	k8xC	a
užívali	užívat	k5eAaImAgMnP	užívat
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
s	s	k7c7	s
africkými	africký	k2eAgInPc7d1	africký
kmeny	kmen	k1gInPc7	kmen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
přístavech	přístav	k1gInPc6	přístav
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
založit	založit	k5eAaPmF	založit
trvalé	trvalý	k2eAgInPc4d1	trvalý
velkosklady	velkosklad	k1gInPc4	velkosklad
nebo	nebo	k8xC	nebo
prodat	prodat	k5eAaPmF	prodat
své	svůj	k3xOyFgNnSc4	svůj
zboží	zboží	k1gNnSc4	zboží
na	na	k7c6	na
tržištích	tržiště	k1gNnPc6	tržiště
<g/>
.	.	kIx.	.
</s>
<s>
Získávali	získávat	k5eAaImAgMnP	získávat
jantar	jantar	k1gInSc4	jantar
ze	z	k7c2	z
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
a	a	k8xC	a
cín	cín	k1gInSc1	cín
z	z	k7c2	z
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Keltiberů	Keltiber	k1gMnPc2	Keltiber
<g/>
,	,	kIx,	,
Galů	Gal	k1gMnPc2	Gal
a	a	k8xC	a
Keltů	Kelt	k1gMnPc2	Kelt
získávali	získávat	k5eAaImAgMnP	získávat
jantar	jantar	k1gInSc4	jantar
<g/>
,	,	kIx,	,
cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
a	a	k8xC	a
kožešiny	kožešina	k1gFnPc1	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
Sardínie	Sardínie	k1gFnSc1	Sardínie
a	a	k8xC	a
Korsika	Korsika	k1gFnSc1	Korsika
pro	pro	k7c4	pro
Kartágo	Kartágo	k1gNnSc4	Kartágo
produkovaly	produkovat	k5eAaImAgFnP	produkovat
zlato	zlato	k1gNnSc4	zlato
a	a	k8xC	a
stříbro	stříbro	k1gNnSc4	stříbro
a	a	k8xC	a
Fénické	fénický	k2eAgFnSc2d1	fénická
kolonie	kolonie	k1gFnSc2	kolonie
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Malta	Malta	k1gFnSc1	Malta
a	a	k8xC	a
Baleáry	Baleáry	k1gFnPc1	Baleáry
<g/>
,	,	kIx,	,
produkovaly	produkovat	k5eAaImAgInP	produkovat
zboží	zboží	k1gNnSc4	zboží
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vraceno	vracet	k5eAaImNgNnS	vracet
do	do	k7c2	do
Kartága	Kartágo	k1gNnSc2	Kartágo
k	k	k7c3	k
distribuci	distribuce	k1gFnSc3	distribuce
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
zásobovalo	zásobovat	k5eAaImAgNnS	zásobovat
chudší	chudý	k2eAgFnPc4d2	chudší
civilizace	civilizace	k1gFnPc4	civilizace
jednoduchými	jednoduchý	k2eAgFnPc7d1	jednoduchá
věcmi	věc	k1gFnPc7	věc
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
keramikou	keramika	k1gFnSc7	keramika
<g/>
,	,	kIx,	,
výrobky	výrobek	k1gInPc4	výrobek
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
a	a	k8xC	a
ozdobami	ozdoba	k1gFnPc7	ozdoba
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nahrazujícími	nahrazující	k2eAgInPc7d1	nahrazující
místní	místní	k2eAgMnPc1d1	místní
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc2	jejich
nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
práce	práce	k1gFnSc2	práce
naopak	naopak	k6eAd1	naopak
vyváželo	vyvážet	k5eAaImAgNnS	vyvážet
k	k	k7c3	k
těm	ten	k3xDgInPc3	ten
bohatším	bohatý	k2eAgInPc3d2	bohatší
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Řekům	Řek	k1gMnPc3	Řek
nebo	nebo	k8xC	nebo
Etruskům	Etrusk	k1gMnPc3	Etrusk
<g/>
;	;	kIx,	;
Kartágo	Kartágo	k1gNnSc4	Kartágo
obchodovalo	obchodovat	k5eAaImAgNnS	obchodovat
téměř	téměř	k6eAd1	téměř
s	s	k7c7	s
každým	každý	k3xTgNnSc7	každý
zbožím	zboží	k1gNnSc7	zboží
žádaným	žádaný	k2eAgInPc3d1	žádaný
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
koření	koření	k1gNnSc2	koření
z	z	k7c2	z
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Indie	Indie	k1gFnSc2	Indie
či	či	k8xC	či
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
obchodní	obchodní	k2eAgFnPc1d1	obchodní
lodě	loď	k1gFnPc1	loď
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
podél	podél	k7c2	podél
Atlantského	atlantský	k2eAgNnSc2d1	Atlantské
pobřeží	pobřeží	k1gNnSc2	pobřeží
Afriky	Afrika	k1gFnSc2	Afrika
až	až	k9	až
k	k	k7c3	k
Senegalu	Senegal	k1gInSc3	Senegal
a	a	k8xC	a
Nigérii	Nigérie	k1gFnSc3	Nigérie
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
zpráva	zpráva	k1gFnSc1	zpráva
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
kartáginské	kartáginský	k2eAgNnSc4d1	kartáginské
obchodní	obchodní	k2eAgNnSc4d1	obchodní
plavidlo	plavidlo	k1gNnSc4	plavidlo
prozkoumávající	prozkoumávající	k2eAgFnSc4d1	prozkoumávající
Nigérii	Nigérie	k1gFnSc4	Nigérie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
popisu	popis	k1gInSc2	popis
výrazných	výrazný	k2eAgInPc2d1	výrazný
geografických	geografický	k2eAgInPc2d1	geografický
rysů	rys	k1gInPc2	rys
jako	jako	k8xS	jako
pobřežní	pobřežní	k2eAgInSc1d1	pobřežní
vulkán	vulkán	k1gInSc1	vulkán
a	a	k8xC	a
setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
gorilami	gorila	k1gFnPc7	gorila
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
také	také	k6eAd1	také
Hanno	Hanno	k6eAd1	Hanno
Mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nepravidelné	pravidelný	k2eNgFnPc1d1	nepravidelná
obchodní	obchodní	k2eAgFnPc1d1	obchodní
výměny	výměna	k1gFnPc1	výměna
se	se	k3xPyFc4	se
odehrávaly	odehrávat	k5eAaImAgFnP	odehrávat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
až	až	k6eAd1	až
na	na	k7c6	na
Madeiře	Madeira	k1gFnSc6	Madeira
a	a	k8xC	a
Kanárských	kanárský	k2eAgInPc6d1	kanárský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
až	až	k6eAd1	až
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
také	také	k9	také
obchodovalo	obchodovat	k5eAaImAgNnS	obchodovat
s	s	k7c7	s
Indií	Indie	k1gFnSc7	Indie
<g/>
:	:	kIx,	:
cestovali	cestovat	k5eAaImAgMnP	cestovat
přes	přes	k7c4	přes
Rudé	rudý	k2eAgNnSc4d1	Rudé
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
–	–	k?	–
spíše	spíše	k9	spíše
bájné	bájný	k2eAgFnPc1d1	bájná
–	–	k?	–
země	zem	k1gFnPc1	zem
Ofir	Ofira	k1gFnPc2	Ofira
a	a	k8xC	a
Punt	punto	k1gNnPc2	punto
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Somálsko	Somálsko	k1gNnSc1	Somálsko
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
přítomnost	přítomnost	k1gFnSc4	přítomnost
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
výměn	výměna	k1gFnPc2	výměna
<g/>
,	,	kIx,	,
od	od	k7c2	od
ohromného	ohromný	k2eAgNnSc2d1	ohromné
množství	množství	k1gNnSc2	množství
cínu	cín	k1gInSc2	cín
tolik	tolik	k6eAd1	tolik
potřebného	potřebný	k2eAgInSc2d1	potřebný
pro	pro	k7c4	pro
civilizaci	civilizace	k1gFnSc4	civilizace
stále	stále	k6eAd1	stále
používající	používající	k2eAgInSc4d1	používající
bronz	bronz	k1gInSc4	bronz
<g/>
,	,	kIx,	,
až	až	k8xS	až
po	po	k7c4	po
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
textilií	textilie	k1gFnPc2	textilie
<g/>
,	,	kIx,	,
keramiky	keramika	k1gFnSc2	keramika
a	a	k8xC	a
drobných	drobný	k2eAgInPc2d1	drobný
kovových	kovový	k2eAgInPc2d1	kovový
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
a	a	k8xC	a
během	během	k7c2	během
válek	válka	k1gFnPc2	válka
kartáginští	kartáginský	k2eAgMnPc1d1	kartáginský
kupci	kupec	k1gMnPc1	kupec
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
přístavu	přístav	k1gInSc6	přístav
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
;	;	kIx,	;
kupující	kupující	k1gMnSc1	kupující
a	a	k8xC	a
prodávající	prodávající	k2eAgMnSc1d1	prodávající
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgNnSc1d1	zakládající
skladiště	skladiště	k1gNnSc1	skladiště
kdekoli	kdekoli	k6eAd1	kdekoli
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
smlouvající	smlouvající	k2eAgFnSc4d1	smlouvající
na	na	k7c6	na
otevřených	otevřený	k2eAgNnPc6d1	otevřené
tržištích	tržiště	k1gNnPc6	tržiště
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgFnPc1d1	archeologická
vykopávky	vykopávka	k1gFnPc1	vykopávka
etruských	etruský	k2eAgNnPc2d1	etruské
měst	město	k1gNnPc2	město
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
etruská	etruský	k2eAgFnSc1d1	etruská
civilizace	civilizace	k1gFnSc1	civilizace
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
zákazníkem	zákazník	k1gMnSc7	zákazník
i	i	k8xC	i
dodavatelem	dodavatel	k1gMnSc7	dodavatel
Kartáginců	Kartáginec	k1gInPc2	Kartáginec
<g/>
,	,	kIx,	,
dlouho	dlouho	k6eAd1	dlouho
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Etruské	etruský	k2eAgInPc1d1	etruský
městské	městský	k2eAgInPc1d1	městský
státy	stát	k1gInPc1	stát
byly	být	k5eAaImAgInP	být
Kartágu	Kartágo	k1gNnSc3	Kartágo
občas	občas	k6eAd1	občas
i	i	k9	i
vojenskými	vojenský	k2eAgMnPc7d1	vojenský
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
