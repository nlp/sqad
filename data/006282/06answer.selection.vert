<s>
John	John	k1gMnSc1	John
F.	F.	kA	F.
Kennedy	Kenned	k1gMnPc4	Kenned
byl	být	k5eAaImAgMnS	být
smrtelně	smrtelně	k6eAd1	smrtelně
raněn	ranit	k5eAaPmNgMnS	ranit
střelou	střela	k1gFnSc7	střela
z	z	k7c2	z
pušky	puška	k1gFnSc2	puška
Carcano	Carcana	k1gFnSc5	Carcana
M	M	kA	M
<g/>
91	[number]	k4	91
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kampaně	kampaň	k1gFnSc2	kampaň
pro	pro	k7c4	pro
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
1964	[number]	k4	1964
projížděl	projíždět	k5eAaImAgInS	projíždět
ulicemi	ulice	k1gFnPc7	ulice
Dallasu	Dallas	k1gInSc6	Dallas
v	v	k7c6	v
otevřeném	otevřený	k2eAgInSc6d1	otevřený
voze	vůz	k1gInSc6	vůz
doprovázený	doprovázený	k2eAgInSc1d1	doprovázený
manželkou	manželka	k1gFnSc7	manželka
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
<g/>
,	,	kIx,	,
texaským	texaský	k2eAgMnSc7d1	texaský
guvernérem	guvernér	k1gMnSc7	guvernér
Johnem	John	k1gMnSc7	John
Connallym	Connallym	k1gInSc4	Connallym
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
Nellie	Nellie	k1gFnSc2	Nellie
<g/>
.	.	kIx.	.
</s>
