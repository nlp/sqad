<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
texaský	texaský	k2eAgMnSc1d1	texaský
guverér	guverér	k1gMnSc1	guverér
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
prezidenta	prezident	k1gMnSc4	prezident
Kennedyho	Kennedy	k1gMnSc4	Kennedy
v	v	k7c4	v
den	den	k1gInSc4	den
atentátu	atentát	k1gInSc2	atentát
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
<g/>
?	?	kIx.	?
</s>
