<p>
<s>
Adenosintrifosfát	Adenosintrifosfát	k1gMnSc1	Adenosintrifosfát
(	(	kIx(	(
<g/>
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
z	z	k7c2	z
angl.	angl.	k?	angl.
adenosine	adenosin	k1gMnSc5	adenosin
triphosphate	triphosphat	k1gMnSc5	triphosphat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc4d1	důležitý
nukleotid	nukleotid	k1gInSc4	nukleotid
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
nukleosidtrifosfát	nukleosidtrifosfát	k1gInSc1	nukleosidtrifosfát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
adenosinu	adenosina	k1gFnSc4	adenosina
a	a	k8xC	a
trojice	trojice	k1gFnPc4	trojice
fosfátů	fosfát	k1gInPc2	fosfát
navázané	navázaný	k2eAgFnPc4d1	navázaná
na	na	k7c4	na
5	[number]	k4	5
<g/>
'	'	kIx"	'
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
zásadní	zásadní	k2eAgInSc1d1	zásadní
pro	pro	k7c4	pro
funkci	funkce	k1gFnSc4	funkce
všech	všecek	k3xTgFnPc2	všecek
známých	známý	k2eAgFnPc2d1	známá
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
rozkladu	rozklad	k1gInSc6	rozklad
ATP	atp	kA	atp
na	na	k7c4	na
ADP	ADP	kA	ADP
a	a	k8xC	a
Pi	pi	k0	pi
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
značného	značný	k2eAgNnSc2d1	značné
množství	množství	k1gNnSc2	množství
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
energie	energie	k1gFnSc1	energie
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
typech	typ	k1gInPc6	typ
buněčných	buněčný	k2eAgInPc2d1	buněčný
pochodů	pochod	k1gInPc2	pochod
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
namátkou	namátkou	k6eAd1	namátkou
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
biosyntetických	biosyntetický	k2eAgFnPc2d1	biosyntetická
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
vnitrobuněčný	vnitrobuněčný	k2eAgInSc1d1	vnitrobuněčný
transport	transport	k1gInSc1	transport
a	a	k8xC	a
membránový	membránový	k2eAgInSc1d1	membránový
transport	transport	k1gInSc1	transport
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
proteinů	protein	k1gInPc2	protein
či	či	k8xC	či
syntéza	syntéza	k1gFnSc1	syntéza
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ATP	atp	kA	atp
se	se	k3xPyFc4	se
z	z	k7c2	z
chemického	chemický	k2eAgNnSc2d1	chemické
hlediska	hledisko	k1gNnSc2	hledisko
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
5	[number]	k4	5
<g/>
'	'	kIx"	'
ribonukleotidy	ribonukleotida	k1gFnPc1	ribonukleotida
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
cukr	cukr	k1gInSc4	cukr
ribózu	ribóza	k1gFnSc4	ribóza
a	a	k8xC	a
na	na	k7c4	na
5	[number]	k4	5
<g/>
'	'	kIx"	'
místě	místo	k1gNnSc6	místo
fosfátové	fosfátový	k2eAgFnSc2d1	fosfátová
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgInPc1d1	další
nukleotidy	nukleotid	k1gInPc1	nukleotid
s	s	k7c7	s
podobnou	podobný	k2eAgFnSc7d1	podobná
stavbou	stavba	k1gFnSc7	stavba
a	a	k8xC	a
funkcí	funkce	k1gFnSc7	funkce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
GTP	GTP	kA	GTP
<g/>
,	,	kIx,	,
UTP	UTP	kA	UTP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgMnSc1	žádný
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
tak	tak	k6eAd1	tak
všestranné	všestranný	k2eAgNnSc1d1	všestranné
využití	využití	k1gNnSc1	využití
jako	jako	k8xS	jako
ATP.	atp.	kA	atp.
Od	od	k7c2	od
ATP	atp	kA	atp
je	být	k5eAaImIp3nS	být
také	také	k9	také
vhodné	vhodný	k2eAgNnSc1d1	vhodné
odlišovat	odlišovat	k5eAaImF	odlišovat
vzácnější	vzácný	k2eAgInSc4d2	vzácnější
deoxyadenosintrifosfát	deoxyadenosintrifosfát	k1gInSc4	deoxyadenosintrifosfát
(	(	kIx(	(
<g/>
dATP	dATP	k?	dATP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
deoxyribonukleotidy	deoxyribonukleotid	k1gMnPc7	deoxyribonukleotid
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
stavebních	stavební	k2eAgInPc2d1	stavební
kamenů	kámen	k1gInPc2	kámen
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
výzkumu	výzkum	k1gInSc2	výzkum
==	==	k?	==
</s>
</p>
<p>
<s>
Adenosintrifosfát	Adenosintrifosfát	k1gInSc1	Adenosintrifosfát
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
K.	K.	kA	K.
Lohmannem	Lohmann	k1gInSc7	Lohmann
poprvé	poprvé	k6eAd1	poprvé
izolován	izolovat	k5eAaBmNgMnS	izolovat
z	z	k7c2	z
extraktu	extrakt	k1gInSc2	extrakt
svalu	sval	k1gInSc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
ale	ale	k8xC	ale
Fritz	Fritz	k1gMnSc1	Fritz
Albert	Albert	k1gMnSc1	Albert
Lipmann	Lipmann	k1gMnSc1	Lipmann
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ATP	atp	kA	atp
hlavní	hlavní	k2eAgFnSc7d1	hlavní
biomolekulou	biomolekula	k1gFnSc7	biomolekula
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
krátkodobé	krátkodobý	k2eAgNnSc4d1	krátkodobé
uchování	uchování	k1gNnSc4	uchování
a	a	k8xC	a
udílení	udílení	k1gNnSc4	udílení
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
ATP	atp	kA	atp
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
uměle	uměle	k6eAd1	uměle
připraveno	připraven	k2eAgNnSc1d1	připraveno
Alexanderem	Alexander	k1gMnSc7	Alexander
Toddem	Todd	k1gMnSc7	Todd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
ukázal	ukázat	k5eAaPmAgMnS	ukázat
Albert	Albert	k1gMnSc1	Albert
Szent-Györgyi	Szent-György	k1gFnSc2	Szent-György
<g/>
,	,	kIx,	,
že	že	k8xS	že
svalový	svalový	k2eAgInSc1d1	svalový
stah	stah	k1gInSc1	stah
izolovaných	izolovaný	k2eAgFnPc2d1	izolovaná
myofibril	myofibrila	k1gFnPc2	myofibrila
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
uměle	uměle	k6eAd1	uměle
vyvolán	vyvolat	k5eAaPmNgInS	vyvolat
přidáním	přidání	k1gNnSc7	přidání
ATP.	atp.	kA	atp.
O	o	k7c6	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
podobným	podobný	k2eAgInSc7d1	podobný
experimentem	experiment	k1gInSc7	experiment
popsána	popsán	k2eAgFnSc1d1	popsána
role	role	k1gFnSc1	role
ATP	atp	kA	atp
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
řasinek	řasinka	k1gFnPc2	řasinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Struktura	struktura	k1gFnSc1	struktura
a	a	k8xC	a
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Adenosintrifosfát	Adenosintrifosfát	k1gInSc1	Adenosintrifosfát
je	být	k5eAaImIp3nS	být
nukleotid	nukleotid	k1gInSc4	nukleotid
složený	složený	k2eAgInSc4d1	složený
z	z	k7c2	z
pětiuhlíkatého	pětiuhlíkatý	k2eAgInSc2d1	pětiuhlíkatý
cukru	cukr	k1gInSc2	cukr
ribózy	ribóza	k1gFnSc2	ribóza
<g/>
,	,	kIx,	,
adeninu	adenin	k1gInSc2	adenin
navěšeného	navěšený	k2eAgInSc2d1	navěšený
na	na	k7c4	na
1	[number]	k4	1
<g/>
'	'	kIx"	'
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
trojice	trojice	k1gFnSc2	trojice
fosfátových	fosfátový	k2eAgFnPc2d1	fosfátová
skupin	skupina	k1gFnPc2	skupina
na	na	k7c4	na
5	[number]	k4	5
<g/>
'	'	kIx"	'
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Vazba	vazba	k1gFnSc1	vazba
mezi	mezi	k7c7	mezi
adeninem	adenin	k1gInSc7	adenin
a	a	k8xC	a
ribózou	ribóza	k1gFnSc7	ribóza
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
N-glykosidická	Nlykosidický	k2eAgFnSc1d1	N-glykosidický
<g/>
.	.	kIx.	.
</s>
<s>
Fosfátové	fosfátový	k2eAgFnPc1d1	fosfátová
skupiny	skupina	k1gFnPc1	skupina
jsou	být	k5eAaImIp3nP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
anhydridovými	anhydridový	k2eAgFnPc7d1	anhydridová
vazbami	vazba	k1gFnPc7	vazba
a	a	k8xC	a
k	k	k7c3	k
ribóze	ribóza	k1gFnSc3	ribóza
tzv.	tzv.	kA	tzv.
fosfodiesterovou	fosfodiesterův	k2eAgFnSc7d1	fosfodiesterův
vazbou	vazba	k1gFnSc7	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
anhydridové	anhydridový	k2eAgFnPc1d1	anhydridová
vazby	vazba	k1gFnPc1	vazba
mezi	mezi	k7c7	mezi
fosfáty	fosfát	k1gInPc7	fosfát
jsou	být	k5eAaImIp3nP	být
bohaté	bohatý	k2eAgInPc1d1	bohatý
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
či	či	k8xC	či
makroergické	makroergický	k2eAgMnPc4d1	makroergický
a	a	k8xC	a
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
se	s	k7c7	s
vlnovkami	vlnovka	k1gFnPc7	vlnovka
<g/>
:	:	kIx,	:
Ade	Ade	k1gFnSc7	Ade
<g/>
–	–	k?	–
<g/>
Rib	Rib	k1gFnSc2	Rib
<g/>
–	–	k?	–
<g/>
P	P	kA	P
<g/>
~	~	kIx~	~
<g/>
P	P	kA	P
<g/>
~	~	kIx~	~
<g/>
P.	P.	kA	P.
To	to	k9	to
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
a	a	k8xC	a
vysokoenergetičnost	vysokoenergetičnost	k1gFnSc1	vysokoenergetičnost
vazby	vazba	k1gFnSc2	vazba
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
přímo	přímo	k6eAd1	přímo
nesouvisí	souviset	k5eNaImIp3nS	souviset
s	s	k7c7	s
danou	daný	k2eAgFnSc7d1	daná
vazebnou	vazebný	k2eAgFnSc7d1	vazebná
energií	energie	k1gFnSc7	energie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
přenos	přenos	k1gInSc1	přenos
fosfátových	fosfátový	k2eAgFnPc2d1	fosfátová
skupin	skupina	k1gFnPc2	skupina
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
uvolňováním	uvolňování	k1gNnSc7	uvolňování
značné	značný	k2eAgFnSc2d1	značná
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
makroergická	makroergický	k2eAgFnSc1d1	makroergická
vazba	vazba	k1gFnSc1	vazba
<g/>
"	"	kIx"	"
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
opodstatnitelný	opodstatnitelný	k2eAgMnSc1d1	opodstatnitelný
<g/>
.	.	kIx.	.
<g/>
Fyzikálně-chemické	Fyzikálněhemický	k2eAgFnPc1d1	Fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
ATP	atp	kA	atp
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
vlastnostem	vlastnost	k1gFnPc3	vlastnost
jiných	jiný	k2eAgInPc2d1	jiný
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
bílou	bílý	k2eAgFnSc4d1	bílá
práškovitou	práškovitý	k2eAgFnSc4d1	práškovitá
látku	látka	k1gFnSc4	látka
rozpustnou	rozpustný	k2eAgFnSc4d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
s	s	k7c7	s
několika	několik	k4yIc7	několik
záporně	záporně	k6eAd1	záporně
nabitými	nabitý	k2eAgFnPc7d1	nabitá
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
záření	záření	k1gNnSc1	záření
v	v	k7c6	v
ultrafialové	ultrafialový	k2eAgFnSc6d1	ultrafialová
oblasti	oblast	k1gFnSc6	oblast
kolem	kolem	k7c2	kolem
260	[number]	k4	260
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
biochemii	biochemie	k1gFnSc6	biochemie
je	být	k5eAaImIp3nS	být
ATP	atp	kA	atp
znám	znát	k5eAaImIp1nS	znát
jako	jako	k9	jako
nejběžnější	běžný	k2eAgNnSc1d3	nejběžnější
energetické	energetický	k2eAgNnSc1d1	energetické
oběživo	oběživo	k1gNnSc1	oběživo
živých	živý	k2eAgInPc2d1	živý
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
uvnitř	uvnitř	k7c2	uvnitř
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
struktury	struktura	k1gFnSc2	struktura
vratně	vratně	k6eAd1	vratně
uschovat	uschovat	k5eAaPmF	uschovat
relativně	relativně	k6eAd1	relativně
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
exergonickým	exergonický	k2eAgMnSc7d1	exergonický
(	(	kIx(	(
<g/>
energii	energie	k1gFnSc4	energie
uvolňujícím	uvolňující	k2eAgInSc7d1	uvolňující
<g/>
)	)	kIx)	)
rozkladem	rozklad	k1gInSc7	rozklad
pohánět	pohánět	k5eAaImF	pohánět
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
endergonické	endergonický	k2eAgNnSc4d1	endergonický
(	(	kIx(	(
<g/>
energii	energie	k1gFnSc4	energie
spotřebovávající	spotřebovávající	k2eAgInPc1d1	spotřebovávající
<g/>
)	)	kIx)	)
procesy	proces	k1gInPc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
tzv.	tzv.	kA	tzv.
makroergická	makroergický	k2eAgFnSc1d1	makroergická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
biochemických	biochemický	k2eAgInPc2d1	biochemický
pochodů	pochod	k1gInPc2	pochod
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
spotřebě	spotřeba	k1gFnSc3	spotřeba
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jeho	jeho	k3xOp3gFnSc3	jeho
hydrolýze	hydrolýza	k1gFnSc3	hydrolýza
na	na	k7c4	na
ADP	ADP	kA	ADP
+	+	kIx~	+
Pi	pi	k0	pi
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dokonce	dokonce	k9	dokonce
na	na	k7c6	na
AMP	AMP	kA	AMP
a	a	k8xC	a
PPi	ppi	kA	ppi
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ATP	atp	kA	atp
+	+	kIx~	+
H2O	H2O	k1gMnSc3	H2O
→	→	k?	→
ADP	ADP	kA	ADP
+	+	kIx~	+
Pi	pi	k0	pi
</s>
</p>
<p>
<s>
ATP	atp	kA	atp
+	+	kIx~	+
H2O	H2O	k1gMnSc3	H2O
→	→	k?	→
AMP	AMP	kA	AMP
+	+	kIx~	+
PPiVzniklý	PPiVzniklý	k2eAgInSc1d1	PPiVzniklý
pyrofosfát	pyrofosfát	k1gInSc1	pyrofosfát
PPi	ppi	kA	ppi
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
hydrolyzován	hydrolyzovat	k5eAaBmNgInS	hydrolyzovat
přítomným	přítomný	k2eAgInSc7d1	přítomný
enzymem	enzym	k1gInSc7	enzym
pyrofosfatasou	pyrofosfatasa	k1gFnSc7	pyrofosfatasa
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
dvojnásobné	dvojnásobný	k2eAgNnSc1d1	dvojnásobné
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimobuněčný	Mimobuněčný	k2eAgInSc1d1	Mimobuněčný
ATP	atp	kA	atp
má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
i	i	k9	i
jako	jako	k9	jako
neurotransmiter	neurotransmiter	k1gInSc4	neurotransmiter
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
kapitola	kapitola	k1gFnSc1	kapitola
výskyt	výskyt	k1gInSc1	výskyt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Energetika	energetik	k1gMnSc4	energetik
ATP	atp	kA	atp
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
reakcím	reakce	k1gFnPc3	reakce
spřaženým	spřažený	k2eAgFnPc3d1	spřažená
se	s	k7c7	s
spotřebou	spotřeba	k1gFnSc7	spotřeba
(	(	kIx(	(
<g/>
hydrolýzou	hydrolýza	k1gFnSc7	hydrolýza
<g/>
)	)	kIx)	)
ATP	atp	kA	atp
patří	patřit	k5eAaImIp3nS	patřit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
fundamentálních	fundamentální	k2eAgInPc2d1	fundamentální
chemických	chemický	k2eAgInPc2d1	chemický
procesů	proces	k1gInPc2	proces
probíhajících	probíhající	k2eAgInPc2d1	probíhající
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
spřažení	spřažení	k1gNnSc2	spřažení
s	s	k7c7	s
hydrolýzou	hydrolýza	k1gFnSc7	hydrolýza
ATP	atp	kA	atp
by	by	kYmCp3nP	by
tyto	tento	k3xDgFnPc1	tento
reakce	reakce	k1gFnPc1	reakce
probíhaly	probíhat	k5eAaImAgFnP	probíhat
jen	jen	k9	jen
velice	velice	k6eAd1	velice
pomalu	pomalu	k6eAd1	pomalu
(	(	kIx(	(
<g/>
měly	mít	k5eAaImAgInP	mít
by	by	kYmCp3nP	by
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hodnotu	hodnota	k1gFnSc4	hodnota
změny	změna	k1gFnSc2	změna
Gibbsovy	Gibbsův	k2eAgFnSc2d1	Gibbsova
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
Δ	Δ	k?	Δ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
krok	krok	k1gInSc4	krok
glykolýzy	glykolýza	k1gFnSc2	glykolýza
<g/>
,	,	kIx,	,
přeměna	přeměna	k1gFnSc1	přeměna
glukózy	glukóza	k1gFnSc2	glukóza
na	na	k7c6	na
glukóza-	glukóza-	k?	glukóza-
<g/>
6	[number]	k4	6
<g/>
-fosfát	osfát	k1gInSc1	-fosfát
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnPc1	reakce
glukózy	glukóza	k1gFnSc2	glukóza
s	s	k7c7	s
anorganickým	anorganický	k2eAgInSc7d1	anorganický
fosfátem	fosfát	k1gInSc7	fosfát
má	mít	k5eAaImIp3nS	mít
Δ	Δ	k?	Δ
=	=	kIx~	=
+13,8	+13,8	k4	+13,8
kJ	kJ	k?	kJ
<g/>
/	/	kIx~	/
<g/>
mol	mol	k1gMnSc1	mol
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spřažená	spřažený	k2eAgFnSc1d1	spřažená
hydrolýza	hydrolýza	k1gFnSc1	hydrolýza
ATP	atp	kA	atp
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
změnou	změna	k1gFnSc7	změna
Gibbsovy	Gibbsův	k2eAgFnSc2d1	Gibbsova
energie	energie	k1gFnSc2	energie
Δ	Δ	k?	Δ
=	=	kIx~	=
-30,5	-30,5	k4	-30,5
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
sled	sled	k1gInSc1	sled
reakcí	reakce	k1gFnSc7	reakce
energeticky	energeticky	k6eAd1	energeticky
favorizovaný	favorizovaný	k2eAgMnSc1d1	favorizovaný
a	a	k8xC	a
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
<g/>
.	.	kIx.	.
</s>
<s>
Přesná	přesný	k2eAgFnSc1d1	přesná
hodnota	hodnota	k1gFnSc1	hodnota
Δ	Δ	k?	Δ
pro	pro	k7c4	pro
hydrolytický	hydrolytický	k2eAgInSc4d1	hydrolytický
rozklad	rozklad	k1gInSc4	rozklad
ATP	atp	kA	atp
na	na	k7c4	na
ADP	ADP	kA	ADP
a	a	k8xC	a
Pi	pi	k0	pi
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
má	mít	k5eAaImIp3nS	mít
zejména	zejména	k9	zejména
pH	ph	kA	ph
<g/>
,	,	kIx,	,
koncentrace	koncentrace	k1gFnSc1	koncentrace
dvojmocných	dvojmocný	k2eAgInPc2d1	dvojmocný
kovových	kovový	k2eAgInPc2d1	kovový
iontů	ion	k1gInPc2	ion
(	(	kIx(	(
<g/>
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
asociují	asociovat	k5eAaBmIp3nP	asociovat
s	s	k7c7	s
fosfátem	fosfát	k1gInSc7	fosfát
<g/>
)	)	kIx)	)
a	a	k8xC	a
celková	celkový	k2eAgFnSc1d1	celková
iontová	iontový	k2eAgFnSc1d1	iontová
síla	síla	k1gFnSc1	síla
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
reakcích	reakce	k1gFnPc6	reakce
udává	udávat	k5eAaImIp3nS	udávat
standardní	standardní	k2eAgFnSc1d1	standardní
změna	změna	k1gFnSc1	změna
Gibbsovy	Gibbsův	k2eAgFnSc2d1	Gibbsova
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
Δ	Δ	k?	Δ
<g/>
°	°	k?	°
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
-30,5	-30,5	k4	-30,5
kJ	kJ	k?	kJ
<g/>
/	/	kIx~	/
<g/>
mol	mol	k1gMnSc1	mol
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
buněčném	buněčný	k2eAgNnSc6d1	buněčné
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
reálnější	reální	k2eAgFnSc1d2	reální
hodnota	hodnota	k1gFnSc1	hodnota
-50	-50	k4	-50
kJ	kJ	k?	kJ
<g/>
/	/	kIx~	/
<g/>
mol	mol	k1gMnSc1	mol
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
takto	takto	k6eAd1	takto
vysoký	vysoký	k2eAgInSc1d1	vysoký
energetický	energetický	k2eAgInSc1d1	energetický
schodek	schodek	k1gInSc1	schodek
nemusí	muset	k5eNaImIp3nS	muset
pro	pro	k7c4	pro
průběh	průběh	k1gInSc4	průběh
řady	řada	k1gFnSc2	řada
reakcí	reakce	k1gFnPc2	reakce
stačit	stačit	k5eAaBmF	stačit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ještě	ještě	k6eAd1	ještě
existuje	existovat	k5eAaImIp3nS	existovat
alternativa	alternativa	k1gFnSc1	alternativa
<g/>
.	.	kIx.	.
</s>
<s>
Rozklad	rozklad	k1gInSc1	rozklad
ATP	atp	kA	atp
na	na	k7c4	na
AMP	AMP	kA	AMP
a	a	k8xC	a
PPi	ppi	kA	ppi
je	být	k5eAaImIp3nS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
vyšší	vysoký	k2eAgFnSc7d2	vyšší
hodnotou	hodnota	k1gFnSc7	hodnota
Δ	Δ	k?	Δ
<g/>
:	:	kIx,	:
samotná	samotný	k2eAgFnSc1d1	samotná
hydrolýza	hydrolýza	k1gFnSc1	hydrolýza
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
produkty	produkt	k1gInPc4	produkt
sice	sice	k8xC	sice
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
podobné	podobný	k2eAgNnSc1d1	podobné
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
hydrolýza	hydrolýza	k1gFnSc1	hydrolýza
na	na	k7c4	na
ADP	ADP	kA	ADP
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
konečného	konečný	k2eAgInSc2d1	konečný
Δ	Δ	k?	Δ
se	se	k3xPyFc4	se
započítává	započítávat	k5eAaImIp3nS	započítávat
i	i	k9	i
hydrolýza	hydrolýza	k1gFnSc1	hydrolýza
volného	volný	k2eAgInSc2d1	volný
difosfátu	difosfát	k1gInSc2	difosfát
(	(	kIx(	(
<g/>
PPi	ppi	kA	ppi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
výsledná	výsledný	k2eAgFnSc1d1	výsledná
Δ	Δ	k?	Δ
<g/>
°	°	k?	°
<g/>
'	'	kIx"	'
může	moct	k5eAaImIp3nS	moct
činit	činit	k5eAaImF	činit
až	až	k9	až
-109	-109	k4	-109
kJ	kJ	k?	kJ
<g/>
/	/	kIx~	/
<g/>
mol	mol	k1gMnSc1	mol
<g/>
.	.	kIx.	.
<g/>
Co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
příčinou	příčina	k1gFnSc7	příčina
energetického	energetický	k2eAgInSc2d1	energetický
schodku	schodek	k1gInSc2	schodek
při	při	k7c6	při
hydrolýze	hydrolýza	k1gFnSc6	hydrolýza
ATP	atp	kA	atp
na	na	k7c4	na
produkty	produkt	k1gInPc4	produkt
s	s	k7c7	s
nižším	nízký	k2eAgInSc7d2	nižší
počtem	počet	k1gInSc7	počet
fosfátů	fosfát	k1gInPc2	fosfát
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
dohadů	dohad	k1gInPc2	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc1	vliv
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Fosfátové	fosfátový	k2eAgFnPc1d1	fosfátová
skupiny	skupina	k1gFnPc1	skupina
v	v	k7c6	v
ATP	atp	kA	atp
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
záporně	záporně	k6eAd1	záporně
nabité	nabitý	k2eAgFnPc1d1	nabitá
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
elektrostaticky	elektrostaticky	k6eAd1	elektrostaticky
odpuzují	odpuzovat	k5eAaImIp3nP	odpuzovat
<g/>
;	;	kIx,	;
hydrolýza	hydrolýza	k1gFnSc1	hydrolýza
snižuje	snižovat	k5eAaImIp3nS	snižovat
velikost	velikost	k1gFnSc4	velikost
těchto	tento	k3xDgFnPc2	tento
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
mají	mít	k5eAaImIp3nP	mít
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
silnou	silný	k2eAgFnSc4d1	silná
tendenci	tendence	k1gFnSc4	tendence
asociovat	asociovat	k5eAaBmF	asociovat
se	se	k3xPyFc4	se
s	s	k7c7	s
rozpouštědlem	rozpouštědlo	k1gNnSc7	rozpouštědlo
(	(	kIx(	(
<g/>
vodou	voda	k1gFnSc7	voda
<g/>
)	)	kIx)	)
–	–	k?	–
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
snazší	snadný	k2eAgMnSc1d2	snazší
u	u	k7c2	u
produktů	produkt	k1gInPc2	produkt
hydrolýzy	hydrolýza	k1gFnSc2	hydrolýza
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
zřejmě	zřejmě	k6eAd1	zřejmě
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
hydrolýze	hydrolýza	k1gFnSc3	hydrolýza
ATP	atp	kA	atp
uspokojena	uspokojen	k2eAgFnSc1d1	uspokojena
poptávka	poptávka	k1gFnSc1	poptávka
fosfátových	fosfátový	k2eAgFnPc2d1	fosfátová
skupin	skupina	k1gFnPc2	skupina
po	po	k7c6	po
elektronech	elektron	k1gInPc6	elektron
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jinak	jinak	k6eAd1	jinak
v	v	k7c6	v
ATP	atp	kA	atp
musí	muset	k5eAaImIp3nS	muset
sousední	sousední	k2eAgInPc4d1	sousední
fosfáty	fosfát	k1gInPc4	fosfát
sdílet	sdílet	k5eAaImF	sdílet
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
druhý	druhý	k4xOgInSc4	druhý
jmenovaný	jmenovaný	k2eAgInSc4d1	jmenovaný
efekt	efekt	k1gInSc4	efekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Role	role	k1gFnPc4	role
v	v	k7c6	v
živých	živý	k2eAgInPc6d1	živý
systémech	systém	k1gInPc6	systém
===	===	k?	===
</s>
</p>
<p>
<s>
ATP	atp	kA	atp
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
substrát	substrát	k1gInSc1	substrát
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
enzymů	enzym	k1gInPc2	enzym
zvaných	zvaný	k2eAgFnPc2d1	zvaná
kinázy	kináza	k1gFnSc2	kináza
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
enzymy	enzym	k1gInPc4	enzym
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgInPc4d1	patřící
mezi	mezi	k7c4	mezi
transferázy	transferáza	k1gFnPc4	transferáza
<g/>
,	,	kIx,	,
přenáší	přenášet	k5eAaImIp3nS	přenášet
fosfátový	fosfátový	k2eAgInSc1d1	fosfátový
zbytek	zbytek	k1gInSc1	zbytek
na	na	k7c4	na
cílovou	cílový	k2eAgFnSc4d1	cílová
molekulu	molekula	k1gFnSc4	molekula
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
aminokyselinový	aminokyselinový	k2eAgInSc1d1	aminokyselinový
zbytek	zbytek	k1gInSc1	zbytek
v	v	k7c6	v
případě	případ	k1gInSc6	případ
proteinkináz	proteinkináza	k1gFnPc2	proteinkináza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ATP	atp	kA	atp
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k9	jako
kofaktor	kofaktor	k1gInSc1	kofaktor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
přenáší	přenášet	k5eAaImIp3nS	přenášet
funkční	funkční	k2eAgFnSc4d1	funkční
skupinu	skupina	k1gFnSc4	skupina
atomů	atom	k1gInPc2	atom
(	(	kIx(	(
<g/>
fosfát	fosfát	k1gInSc1	fosfát
<g/>
)	)	kIx)	)
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
regeneruje	regenerovat	k5eAaBmIp3nS	regenerovat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
kináz	kináza	k1gFnPc2	kináza
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
ATP	atp	kA	atp
na	na	k7c6	na
ADP	ADP	kA	ADP
a	a	k8xC	a
Pi	pi	k0	pi
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
však	však	k9	však
přenášet	přenášet	k5eAaImF	přenášet
i	i	k9	i
difosfát	difosfát	k1gInSc4	difosfát
(	(	kIx(	(
<g/>
za	za	k7c2	za
současného	současný	k2eAgInSc2d1	současný
vzniku	vznik	k1gInSc2	vznik
AMP	AMP	kA	AMP
<g/>
)	)	kIx)	)
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
adenosylový	adenosylový	k2eAgInSc1d1	adenosylový
zbytek	zbytek	k1gInSc1	zbytek
za	za	k7c2	za
současného	současný	k2eAgNnSc2d1	současné
uvolnění	uvolnění	k1gNnSc2	uvolnění
difosfátu	difosfát	k1gInSc2	difosfát
a	a	k8xC	a
monofosfátu	monofosfát	k1gInSc2	monofosfát
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnPc1	reakce
katalyzované	katalyzovaný	k2eAgFnPc1d1	katalyzovaná
kinázami	kináza	k1gFnPc7	kináza
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
víceméně	víceméně	k9	víceméně
jednosměrné	jednosměrný	k2eAgNnSc1d1	jednosměrné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
při	při	k7c6	při
hydrolýze	hydrolýza	k1gFnSc6	hydrolýza
ATP	atp	kA	atp
nevratně	vratně	k6eNd1	vratně
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
volné	volný	k2eAgFnSc2d1	volná
energie	energie	k1gFnSc2	energie
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
princip	princip	k1gInSc1	princip
využívá	využívat	k5eAaImIp3nS	využívat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
ligáz	ligáza	k1gFnPc2	ligáza
<g/>
:	:	kIx,	:
ty	ten	k3xDgFnPc1	ten
mnohdy	mnohdy	k6eAd1	mnohdy
hydrolyzují	hydrolyzovat	k5eAaBmIp3nP	hydrolyzovat
ATP	atp	kA	atp
(	(	kIx(	(
<g/>
mají	mít	k5eAaImIp3nP	mít
ATPázovou	ATPázový	k2eAgFnSc4d1	ATPázový
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
)	)	kIx)	)
a	a	k8xC	a
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
využita	využít	k5eAaPmNgFnS	využít
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
ligační	ligační	k2eAgFnSc2d1	ligační
reakce	reakce	k1gFnSc2	reakce
(	(	kIx(	(
<g/>
typu	typ	k1gInSc2	typ
A	A	kA	A
+	+	kIx~	+
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
ATP	atp	kA	atp
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
GTP	GTP	kA	GTP
<g/>
,	,	kIx,	,
CTP	CTP	kA	CTP
a	a	k8xC	a
UTP	UTP	kA	UTP
<g/>
)	)	kIx)	)
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
substrátů	substrát	k1gInPc2	substrát
pro	pro	k7c4	pro
syntézu	syntéza	k1gFnSc4	syntéza
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
transkripci	transkripce	k1gFnSc4	transkripce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
čtyř	čtyři	k4xCgInPc2	čtyři
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
nukleotidů	nukleotid	k1gInPc2	nukleotid
nemá	mít	k5eNaImIp3nS	mít
nicméně	nicméně	k8xC	nicméně
žádnou	žádný	k3yNgFnSc4	žádný
výlučnou	výlučný	k2eAgFnSc4d1	výlučná
roli	role	k1gFnSc4	role
a	a	k8xC	a
začleňuje	začleňovat	k5eAaImIp3nS	začleňovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
AMP	AMP	kA	AMP
do	do	k7c2	do
prodlužujícího	prodlužující	k2eAgMnSc2d1	prodlužující
se	se	k3xPyFc4	se
řetězce	řetězec	k1gInSc2	řetězec
RNA	RNA	kA	RNA
za	za	k7c2	za
současného	současný	k2eAgNnSc2d1	současné
uvolňování	uvolňování	k1gNnSc2	uvolňování
pyrofosfátu	pyrofosfát	k1gInSc2	pyrofosfát
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
<g/>
Vazba	vazba	k1gFnSc1	vazba
ATP	atp	kA	atp
do	do	k7c2	do
vazebného	vazebný	k2eAgNnSc2d1	vazebné
místa	místo	k1gNnSc2	místo
enzymů	enzym	k1gInPc2	enzym
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
důležitou	důležitý	k2eAgFnSc4d1	důležitá
regulační	regulační	k2eAgFnSc4d1	regulační
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
ATP	atp	kA	atp
může	moct	k5eAaImIp3nS	moct
totiž	totiž	k9	totiž
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
alosterický	alosterický	k2eAgInSc4d1	alosterický
regulátor	regulátor	k1gInSc4	regulátor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
schopný	schopný	k2eAgInSc1d1	schopný
změnit	změnit	k5eAaPmF	změnit
prostorovou	prostorový	k2eAgFnSc4d1	prostorová
strukturu	struktura	k1gFnSc4	struktura
enzymu	enzym	k1gInSc2	enzym
a	a	k8xC	a
například	například	k6eAd1	například
ho	on	k3xPp3gMnSc4	on
aktivovat	aktivovat	k5eAaBmF	aktivovat
či	či	k8xC	či
naopak	naopak	k6eAd1	naopak
deaktivovat	deaktivovat	k5eAaImF	deaktivovat
<g/>
.	.	kIx.	.
</s>
<s>
Enzymy	enzym	k1gInPc1	enzym
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
nastaveny	nastavit	k5eAaPmNgInP	nastavit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vázaly	vázat	k5eAaImAgFnP	vázat
ATP	atp	kA	atp
a	a	k8xC	a
hydrolyzovaly	hydrolyzovat	k5eAaBmAgFnP	hydrolyzovat
ho	on	k3xPp3gMnSc4	on
(	(	kIx(	(
<g/>
ATPázy	ATPáza	k1gFnSc2	ATPáza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
například	například	k6eAd1	například
udělovat	udělovat	k5eAaImF	udělovat
energii	energie	k1gFnSc4	energie
k	k	k7c3	k
mechanické	mechanický	k2eAgFnSc3d1	mechanická
práci	práce	k1gFnSc3	práce
enzymů	enzym	k1gInPc2	enzym
či	či	k8xC	či
ke	k	k7c3	k
koordinovanému	koordinovaný	k2eAgInSc3d1	koordinovaný
pohybu	pohyb	k1gInSc3	pohyb
molekulárních	molekulární	k2eAgInPc2d1	molekulární
motorů	motor	k1gInPc2	motor
(	(	kIx(	(
<g/>
myosin	myosin	k1gInSc1	myosin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
ATP	atp	kA	atp
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
jako	jako	k9	jako
substrát	substrát	k1gInSc1	substrát
pro	pro	k7c4	pro
adenylátcyklázu	adenylátcykláza	k1gFnSc4	adenylátcykláza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
z	z	k7c2	z
ATP	atp	kA	atp
cyklický	cyklický	k2eAgInSc1d1	cyklický
adenosinmonofosfát	adenosinmonofosfát	k1gInSc1	adenosinmonofosfát
(	(	kIx(	(
<g/>
cAMP	camp	k1gInSc1	camp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
druhý	druhý	k4xOgMnSc1	druhý
posel	posel	k1gMnSc1	posel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ATP	atp	kA	atp
také	také	k9	také
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
extracelulární	extracelulární	k2eAgFnSc1d1	extracelulární
signální	signální	k2eAgFnSc1d1	signální
molekula	molekula	k1gFnSc1	molekula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
ATP	atp	kA	atp
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
mnohobuněčných	mnohobuněčný	k2eAgInPc2d1	mnohobuněčný
organismů	organismus	k1gInPc2	organismus
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
vnitrobuněčně	vnitrobuněčně	k6eAd1	vnitrobuněčně
<g/>
,	,	kIx,	,
jen	jen	k9	jen
zanedbatelné	zanedbatelný	k2eAgNnSc1d1	zanedbatelné
množství	množství	k1gNnSc1	množství
ATP	atp	kA	atp
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
mimo	mimo	k7c4	mimo
vlastní	vlastní	k2eAgFnPc4d1	vlastní
buňky	buňka	k1gFnPc4	buňka
v	v	k7c6	v
extracelulárním	extracelulární	k2eAgInSc6d1	extracelulární
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
ATP	atp	kA	atp
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
druh	druh	k1gInSc1	druh
od	od	k7c2	od
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednoho	jeden	k4xCgNnSc2	jeden
těla	tělo	k1gNnSc2	tělo
tkáň	tkáň	k1gFnSc1	tkáň
od	od	k7c2	od
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
ATP	atp	kA	atp
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
způsobem	způsob	k1gInSc7	způsob
pravidelně	pravidelně	k6eAd1	pravidelně
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
zase	zase	k9	zase
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dozrávání	dozrávání	k1gNnSc4	dozrávání
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednoho	jeden	k4xCgInSc2	jeden
buněčného	buněčný	k2eAgInSc2d1	buněčný
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
koncentrace	koncentrace	k1gFnSc1	koncentrace
ATP	atp	kA	atp
liší	lišit	k5eAaImIp3nS	lišit
při	při	k7c6	při
srovnání	srovnání	k1gNnSc6	srovnání
několika	několik	k4yIc2	několik
jedinců	jedinec	k1gMnPc2	jedinec
až	až	k6eAd1	až
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
však	však	k9	však
vnitrobuněčná	vnitrobuněčný	k2eAgFnSc1d1	vnitrobuněčná
koncentrace	koncentrace	k1gFnSc1	koncentrace
ATP	atp	kA	atp
činí	činit	k5eAaImIp3nS	činit
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
mmol	mmol	k1gInSc1	mmol
<g/>
/	/	kIx~	/
<g/>
litr	litr	k1gInSc1	litr
(	(	kIx(	(
<g/>
mM	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Méně	málo	k6eAd2	málo
známou	známý	k2eAgFnSc7d1	známá
skutečností	skutečnost	k1gFnSc7	skutečnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ATP	atp	kA	atp
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
mimo	mimo	k7c4	mimo
buňku	buňka	k1gFnSc4	buňka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
tkáňovém	tkáňový	k2eAgInSc6d1	tkáňový
moku	mok	k1gInSc6	mok
či	či	k8xC	či
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Ven	ven	k6eAd1	ven
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
se	se	k3xPyFc4	se
ATP	atp	kA	atp
může	moct	k5eAaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
buď	buď	k8xC	buď
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
poranění	poranění	k1gNnSc2	poranění
(	(	kIx(	(
<g/>
z	z	k7c2	z
umírajících	umírající	k2eAgFnPc2d1	umírající
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohdy	mnohdy	k6eAd1	mnohdy
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
děje	dít	k5eAaImIp3nS	dít
i	i	k9	i
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
například	například	k6eAd1	například
pomocí	pomocí	k7c2	pomocí
speciálních	speciální	k2eAgInPc2d1	speciální
membránových	membránový	k2eAgInPc2d1	membránový
kanálů	kanál	k1gInPc2	kanál
či	či	k8xC	či
exocytózou	exocytóza	k1gFnSc7	exocytóza
pomocí	pomocí	k7c2	pomocí
váčků	váček	k1gInPc2	váček
např.	např.	kA	např.
na	na	k7c6	na
synaptické	synaptický	k2eAgFnSc6d1	synaptická
štěrbině	štěrbina	k1gFnSc6	štěrbina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
tkáních	tkáň	k1gFnPc6	tkáň
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
mimo	mimo	k7c4	mimo
buňku	buňka	k1gFnSc4	buňka
nabývat	nabývat	k5eAaImF	nabývat
koncentrace	koncentrace	k1gFnSc1	koncentrace
ATP	atp	kA	atp
nanomolárních	nanomolární	k2eAgInPc2d1	nanomolární
(	(	kIx(	(
<g/>
nM	nM	k?	nM
<g/>
)	)	kIx)	)
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
mikromolárních	mikromolární	k2eAgNnPc2d1	mikromolární
(	(	kIx(	(
<g/>
μ	μ	k?	μ
<g/>
)	)	kIx)	)
koncentrací	koncentrace	k1gFnPc2	koncentrace
<g/>
.	.	kIx.	.
</s>
<s>
Extracelulární	Extracelulární	k2eAgInSc1d1	Extracelulární
ATP	atp	kA	atp
má	mít	k5eAaImIp3nS	mít
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
váže	vázat	k5eAaImIp3nS	vázat
se	se	k3xPyFc4	se
na	na	k7c4	na
P1	P1	k1gMnSc4	P1
a	a	k8xC	a
P2	P2	k1gMnSc4	P2
receptory	receptor	k1gInPc1	receptor
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nervové	nervový	k2eAgFnSc6d1	nervová
soustavě	soustava	k1gFnSc6	soustava
někdy	někdy	k6eAd1	někdy
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
pomocný	pomocný	k2eAgInSc1d1	pomocný
neurotransmiter	neurotransmiter	k1gInSc1	neurotransmiter
hrající	hrající	k2eAgFnSc4d1	hrající
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
procesech	proces	k1gInPc6	proces
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
učení	učení	k1gNnSc2	učení
a	a	k8xC	a
vnímání	vnímání	k1gNnSc2	vnímání
bolesti	bolest	k1gFnSc2	bolest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hladké	hladký	k2eAgFnSc6d1	hladká
svalovině	svalovina	k1gFnSc6	svalovina
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
kontrakci	kontrakce	k1gFnSc4	kontrakce
<g/>
,	,	kIx,	,
ve	v	k7c6	v
varlatech	varle	k1gNnPc6	varle
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
uvolňování	uvolňování	k1gNnSc4	uvolňování
testosteronu	testosteron	k1gInSc2	testosteron
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
byly	být	k5eAaImAgFnP	být
objeveny	objeven	k2eAgFnPc1d1	objevena
desítky	desítka	k1gFnPc1	desítka
fyziologických	fyziologický	k2eAgFnPc2d1	fyziologická
rolí	role	k1gFnPc2	role
mimobuněčného	mimobuněčný	k2eAgInSc2d1	mimobuněčný
ATP.	atp.	kA	atp.
</s>
</p>
<p>
<s>
==	==	k?	==
Doplňování	doplňování	k1gNnSc2	doplňování
hladiny	hladina	k1gFnSc2	hladina
ATP	atp	kA	atp
==	==	k?	==
</s>
</p>
<p>
<s>
Může	moct	k5eAaImIp3nS	moct
<g/>
-li	i	k?	-li
hydrolýza	hydrolýza	k1gFnSc1	hydrolýza
ATP	atp	kA	atp
poskytovat	poskytovat	k5eAaImF	poskytovat
energii	energie	k1gFnSc4	energie
pro	pro	k7c4	pro
průběh	průběh	k1gInSc4	průběh
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
musí	muset	k5eAaImIp3nS	muset
existovat	existovat	k5eAaImF	existovat
i	i	k9	i
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
ještě	ještě	k6eAd1	ještě
energeticky	energeticky	k6eAd1	energeticky
bohatších	bohatý	k2eAgFnPc2d2	bohatší
látek	látka	k1gFnPc2	látka
doplňovat	doplňovat	k5eAaImF	doplňovat
zásobu	zásoba	k1gFnSc4	zásoba
ATP.	atp.	kA	atp.
ATP	atp	kA	atp
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
neustále	neustále	k6eAd1	neustále
regenerováno	regenerovat	k5eAaBmNgNnS	regenerovat
ve	v	k7c6	v
fosforylačních	fosforylační	k2eAgFnPc6d1	fosforylační
buněčných	buněčný	k2eAgFnPc6d1	buněčná
reakcích	reakce	k1gFnPc6	reakce
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
obvykle	obvykle	k6eAd1	obvykle
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozkladu	rozklad	k1gInSc2	rozklad
energeticky	energeticky	k6eAd1	energeticky
bohatých	bohatý	k2eAgFnPc2d1	bohatá
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
neslouží	sloužit	k5eNaImIp3nP	sloužit
ATP	atp	kA	atp
jako	jako	k8xC	jako
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
zásobárna	zásobárna	k1gFnSc1	zásobárna
energie	energie	k1gFnSc2	energie
"	"	kIx"	"
<g/>
na	na	k7c4	na
horší	zlý	k2eAgInPc4d2	horší
časy	čas	k1gInPc4	čas
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
ATP	atp	kA	atp
neustále	neustále	k6eAd1	neustále
nedoplňovalo	doplňovat	k5eNaImAgNnS	doplňovat
<g/>
,	,	kIx,	,
došly	dojít	k5eAaPmAgInP	dojít
by	by	kYmCp3nP	by
jeho	jeho	k3xOp3gFnPc4	jeho
zásoby	zásoba	k1gFnPc4	zásoba
v	v	k7c6	v
průměrné	průměrný	k2eAgFnSc6d1	průměrná
savčí	savčí	k2eAgFnSc6d1	savčí
buňce	buňka	k1gFnSc6	buňka
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
či	či	k8xC	či
dvou	dva	k4xCgFnPc6	dva
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
buňka	buňka	k1gFnSc1	buňka
tedy	tedy	k9	tedy
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
asi	asi	k9	asi
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
(	(	kIx(	(
<g/>
107	[number]	k4	107
<g/>
)	)	kIx)	)
molekul	molekula	k1gFnPc2	molekula
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
celé	celý	k2eAgNnSc1d1	celé
lidské	lidský	k2eAgNnSc1d1	lidské
tělo	tělo	k1gNnSc1	tělo
každou	každý	k3xTgFnSc4	každý
minutu	minuta	k1gFnSc4	minuta
rozloží	rozložit	k5eAaPmIp3nS	rozložit
asi	asi	k9	asi
1	[number]	k4	1
gram	gram	k1gInSc4	gram
ATP.	atp.	kA	atp.
V	v	k7c6	v
extrémních	extrémní	k2eAgInPc6d1	extrémní
případech	případ	k1gInPc6	případ
samozřejmě	samozřejmě	k6eAd1	samozřejmě
potřeba	potřeba	k6eAd1	potřeba
ATP	atp	kA	atp
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
spočítáno	spočítán	k2eAgNnSc1d1	spočítáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
svaly	sval	k1gInPc1	sval
maratonského	maratonský	k2eAgMnSc2d1	maratonský
běžce	běžec	k1gMnSc2	běžec
Channučiho	Channuči	k1gMnSc2	Channuči
za	za	k7c4	za
přibližně	přibližně	k6eAd1	přibližně
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
běhu	běh	k1gInSc6	běh
spotřebovaly	spotřebovat	k5eAaPmAgFnP	spotřebovat
(	(	kIx(	(
<g/>
rozložily	rozložit	k5eAaPmAgFnP	rozložit
<g/>
)	)	kIx)	)
kolem	kolem	k7c2	kolem
60	[number]	k4	60
kg	kg	kA	kg
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
vážil	vážit	k5eAaImAgInS	vážit
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
tohoto	tento	k3xDgInSc2	tento
zdánlivého	zdánlivý	k2eAgInSc2d1	zdánlivý
paradoxu	paradox	k1gInSc2	paradox
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
molekula	molekula	k1gFnSc1	molekula
ATP	atp	kA	atp
byla	být	k5eAaImAgFnS	být
mnohokrát	mnohokrát	k6eAd1	mnohokrát
ve	v	k7c6	v
svalech	sval	k1gInPc6	sval
spotřebována	spotřebován	k2eAgFnSc1d1	spotřebována
na	na	k7c6	na
ADP	ADP	kA	ADP
a	a	k8xC	a
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
ADP	ADP	kA	ADP
opětovně	opětovně	k6eAd1	opětovně
fosforylován	fosforylovat	k5eAaPmNgInS	fosforylovat
na	na	k7c4	na
nové	nový	k2eAgNnSc4d1	nové
ATP.	atp.	kA	atp.
Naopak	naopak	k6eAd1	naopak
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
se	se	k3xPyFc4	se
potřeba	potřeba	k6eAd1	potřeba
ATP	atp	kA	atp
(	(	kIx(	(
<g/>
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
bděním	bdění	k1gNnSc7	bdění
<g/>
)	)	kIx)	)
někdy	někdy	k6eAd1	někdy
až	až	k9	až
stonásobně	stonásobně	k6eAd1	stonásobně
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozklad	rozklad	k1gInSc1	rozklad
a	a	k8xC	a
biosyntéza	biosyntéza	k1gFnSc1	biosyntéza
===	===	k?	===
</s>
</p>
<p>
<s>
ATP	atp	kA	atp
jako	jako	k8xS	jako
takový	takový	k3xDgMnSc1	takový
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
recykluje	recyklovat	k5eAaBmIp3nS	recyklovat
vždy	vždy	k6eAd1	vždy
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
hydrolýze	hydrolýza	k1gFnSc6	hydrolýza
na	na	k7c4	na
ADP	ADP	kA	ADP
či	či	k8xC	či
AMP	AMP	kA	AMP
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
těchto	tento	k3xDgInPc2	tento
nukleotidů	nukleotid	k1gInPc2	nukleotid
nicméně	nicméně	k8xC	nicméně
podléhá	podléhat	k5eAaImIp3nS	podléhat
rozkladu	rozklást	k5eAaPmIp1nS	rozklást
na	na	k7c4	na
nukleosid	nukleosid	k1gInSc4	nukleosid
adenosin	adenosin	k1gMnSc1	adenosin
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
adenin	adenin	k1gInSc4	adenin
a	a	k8xC	a
ribózu	ribóza	k1gFnSc4	ribóza
<g/>
.	.	kIx.	.
</s>
<s>
Adenin	adenin	k1gInSc1	adenin
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
ureát	ureát	k1gInSc4	ureát
<g/>
.	.	kIx.	.
</s>
<s>
Odbourané	odbouraný	k2eAgInPc1d1	odbouraný
nukleotidy	nukleotid	k1gInPc1	nukleotid
se	se	k3xPyFc4	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
musí	muset	k5eAaImIp3nS	muset
stále	stále	k6eAd1	stále
doplňovat	doplňovat	k5eAaImF	doplňovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
složitou	složitý	k2eAgFnSc7d1	složitá
biosyntetickou	biosyntetický	k2eAgFnSc7d1	biosyntetická
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
purinový	purinový	k2eAgInSc1d1	purinový
skelet	skelet	k1gInSc1	skelet
adeninu	adenin	k1gInSc2	adenin
sestavován	sestavovat	k5eAaImNgInS	sestavovat
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
různých	různý	k2eAgInPc2d1	různý
"	"	kIx"	"
<g/>
dílů	díl	k1gInPc2	díl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
cukerná	cukerný	k2eAgFnSc1d1	cukerná
a	a	k8xC	a
fosfátová	fosfátový	k2eAgFnSc1d1	fosfátová
část	část	k1gFnSc1	část
molekuly	molekula	k1gFnSc2	molekula
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
teprve	teprve	k6eAd1	teprve
začne	začít	k5eAaPmIp3nS	začít
skládat	skládat	k5eAaImF	skládat
adenin	adenin	k1gInSc4	adenin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Regenerace	regenerace	k1gFnSc1	regenerace
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
ATP	atp	kA	atp
se	se	k3xPyFc4	se
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
rozkladu	rozklad	k1gInSc6	rozklad
na	na	k7c4	na
ADP	ADP	kA	ADP
či	či	k8xC	či
AMP	AMP	kA	AMP
obvykle	obvykle	k6eAd1	obvykle
znovu	znovu	k6eAd1	znovu
regeneruje	regenerovat	k5eAaBmIp3nS	regenerovat
opětovným	opětovný	k2eAgNnSc7d1	opětovné
dodáním	dodání	k1gNnSc7	dodání
koncových	koncový	k2eAgFnPc2d1	koncová
fosfátových	fosfátový	k2eAgFnPc2d1	fosfátová
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
AMP	AMP	kA	AMP
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
reakci	reakce	k1gFnSc3	reakce
AMP	AMP	kA	AMP
+	+	kIx~	+
ATP	atp	kA	atp
→	→	k?	→
2ADP	[number]	k4	2ADP
(	(	kIx(	(
<g/>
za	za	k7c2	za
katalýzy	katalýza	k1gFnSc2	katalýza
adenylylkinázou	adenylylkináza	k1gFnSc7	adenylylkináza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hladina	hladina	k1gFnSc1	hladina
ADP	ADP	kA	ADP
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
postupně	postupně	k6eAd1	postupně
vzrůstala	vzrůstat	k5eAaImAgFnS	vzrůstat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ATP	atp	kA	atp
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
spotřebovává	spotřebovávat	k5eAaImIp3nS	spotřebovávat
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
nicméně	nicméně	k8xC	nicméně
několik	několik	k4yIc4	několik
mechanismů	mechanismus	k1gInPc2	mechanismus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
fosforylaci	fosforylace	k1gFnSc4	fosforylace
ADP	ADP	kA	ADP
na	na	k7c6	na
ATP.	atp.	kA	atp.
V	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
negativní	negativní	k2eAgFnSc1d1	negativní
zpětná	zpětný	k2eAgFnSc1d1	zpětná
vazba	vazba	k1gFnSc1	vazba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
hladina	hladina	k1gFnSc1	hladina
ADP	ADP	kA	ADP
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
příliš	příliš	k6eAd1	příliš
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
aktivaci	aktivace	k1gFnSc3	aktivace
enzymů	enzym	k1gInPc2	enzym
podílejících	podílející	k2eAgInPc2d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
oxidaci	oxidace	k1gFnSc6	oxidace
cukrů	cukr	k1gInPc2	cukr
<g/>
:	:	kIx,	:
to	ten	k3xDgNnSc1	ten
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
regeneraci	regenerace	k1gFnSc4	regenerace
<g />
.	.	kIx.	.
</s>
<s>
ATP	atp	kA	atp
z	z	k7c2	z
ADP	ADP	kA	ADP
<g/>
.	.	kIx.	.
<g/>
Jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
fosforylace	fosforylace	k1gFnSc2	fosforylace
na	na	k7c6	na
substrátové	substrátový	k2eAgFnSc6d1	substrátová
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
součástí	součást	k1gFnSc7	součást
raných	raný	k2eAgFnPc2d1	raná
fází	fáze	k1gFnPc2	fáze
metabolismu	metabolismus	k1gInSc2	metabolismus
cukrů	cukr	k1gInPc2	cukr
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
mikroorganismy	mikroorganismus	k1gInPc4	mikroorganismus
představuje	představovat	k5eAaImIp3nS	představovat
jedinou	jediný	k2eAgFnSc4d1	jediná
možnost	možnost	k1gFnSc4	možnost
syntézy	syntéza	k1gFnSc2	syntéza
ATP.	atp.	kA	atp.
V	v	k7c6	v
glykolýze	glykolýza	k1gFnSc6	glykolýza
vzniká	vznikat	k5eAaImIp3nS	vznikat
ATP	atp	kA	atp
účinkem	účinek	k1gInSc7	účinek
fosfoglycerátkinázy	fosfoglycerátkináza	k1gFnSc2	fosfoglycerátkináza
při	při	k7c6	při
přeměně	přeměna	k1gFnSc6	přeměna
1,3	[number]	k4	1,3
<g/>
-bisfosfoglycerátu	isfosfoglycerát	k1gInSc2	-bisfosfoglycerát
na	na	k7c4	na
3	[number]	k4	3
<g/>
-fosfoglycerát	osfoglycerát	k1gInSc4	-fosfoglycerát
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
účinkem	účinek	k1gInSc7	účinek
pyruvátkinázy	pyruvátkináza	k1gFnSc2	pyruvátkináza
při	při	k7c6	při
přeměně	přeměna	k1gFnSc6	přeměna
fosfoenolpyruvátu	fosfoenolpyruvát	k1gInSc2	fosfoenolpyruvát
na	na	k7c4	na
pyruvát	pyruvát	k1gInSc4	pyruvát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Krebsově	Krebsův	k2eAgInSc6d1	Krebsův
cyklu	cyklus	k1gInSc6	cyklus
přímo	přímo	k6eAd1	přímo
ATP	atp	kA	atp
zpravidla	zpravidla	k6eAd1	zpravidla
nevzniká	vznikat	k5eNaImIp3nS	vznikat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
katalyzované	katalyzovaný	k2eAgFnSc6d1	katalyzovaná
sukcinyl-CoA-syntetázou	sukcinyl-CoAyntetáza	k1gFnSc7	sukcinyl-CoA-syntetáza
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
GTP	GTP	kA	GTP
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
snadno	snadno	k6eAd1	snadno
na	na	k7c6	na
ATP	atp	kA	atp
převeden	převeden	k2eAgMnSc1d1	převeden
<g/>
:	:	kIx,	:
ADP	ADP	kA	ADP
+	+	kIx~	+
GTP	GTP	kA	GTP
⇆	⇆	k?	⇆
ATP	atp	kA	atp
+	+	kIx~	+
GDP	GDP	kA	GDP
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc7d1	speciální
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc1	vznik
ATP	atp	kA	atp
ve	v	k7c6	v
svalech	sval	k1gInPc6	sval
přenosem	přenos	k1gInSc7	přenos
fosfátové	fosfátový	k2eAgFnSc2d1	fosfátová
skupiny	skupina	k1gFnSc2	skupina
z	z	k7c2	z
kreatinfosfátu	kreatinfosfát	k1gInSc2	kreatinfosfát
(	(	kIx(	(
<g/>
či	či	k8xC	či
argininfosfátu	argininfosfáta	k1gFnSc4	argininfosfáta
<g/>
)	)	kIx)	)
na	na	k7c6	na
ADP	ADP	kA	ADP
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
ATP	atp	kA	atp
a	a	k8xC	a
kreatinu	kreatin	k1gInSc2	kreatin
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
argininu	arginin	k2eAgFnSc4d1	arginin
<g/>
)	)	kIx)	)
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnPc4	reakce
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
pohotovou	pohotový	k2eAgFnSc4d1	pohotová
zásobu	zásoba	k1gFnSc4	zásoba
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svalové	svalový	k2eAgFnSc2d1	svalová
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
<g/>
Tyto	tento	k3xDgFnPc1	tento
všechny	všechen	k3xTgFnPc1	všechen
jmenované	jmenovaný	k2eAgFnPc1d1	jmenovaná
metody	metoda	k1gFnPc1	metoda
však	však	k9	však
zpravidla	zpravidla	k6eAd1	zpravidla
nemají	mít	k5eNaImIp3nP	mít
takový	takový	k3xDgInSc4	takový
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
reakce	reakce	k1gFnPc1	reakce
odehrávající	odehrávající	k2eAgFnPc1d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
a	a	k8xC	a
plastidech	plastid	k1gInPc6	plastid
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
na	na	k7c6	na
prokaryotických	prokaryotický	k2eAgFnPc6d1	prokaryotická
cytoplazmatických	cytoplazmatický	k2eAgFnPc6d1	cytoplazmatická
membránách	membrána	k1gFnPc6	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
případech	případ	k1gInPc6	případ
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
vytvořit	vytvořit	k5eAaPmF	vytvořit
protonový	protonový	k2eAgInSc4d1	protonový
gradient	gradient	k1gInSc4	gradient
napříč	napříč	k7c7	napříč
membránou	membrána	k1gFnSc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
gradient	gradient	k1gInSc1	gradient
protonů	proton	k1gInPc2	proton
podle	podle	k7c2	podle
chemiosmotické	chemiosmotický	k2eAgFnSc2d1	chemiosmotický
teorie	teorie	k1gFnSc2	teorie
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
činnost	činnost	k1gFnSc4	činnost
enzymu	enzym	k1gInSc2	enzym
ATP	atp	kA	atp
syntázy	syntáza	k1gFnPc1	syntáza
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
molekulární	molekulární	k2eAgInSc4d1	molekulární
motor	motor	k1gInSc4	motor
umožňující	umožňující	k2eAgFnSc4d1	umožňující
regeneraci	regenerace	k1gFnSc4	regenerace
ATP	atp	kA	atp
z	z	k7c2	z
ADP	ADP	kA	ADP
<g/>
.	.	kIx.	.
</s>
<s>
Způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vytvořit	vytvořit	k5eAaPmF	vytvořit
protonový	protonový	k2eAgInSc4d1	protonový
gradient	gradient	k1gInSc4	gradient
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
a	a	k8xC	a
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
světelná	světelný	k2eAgFnSc1d1	světelná
fáze	fáze	k1gFnSc1	fáze
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
(	(	kIx(	(
<g/>
a	a	k8xC	a
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
navazující	navazující	k2eAgFnSc1d1	navazující
fotofosforylace	fotofosforylace	k1gFnSc1	fotofosforylace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dýchací	dýchací	k2eAgInSc1d1	dýchací
řetězec	řetězec	k1gInSc1	řetězec
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
buněčného	buněčný	k2eAgNnSc2d1	buněčné
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
činnost	činnost	k1gFnSc4	činnost
bakteriorodopsinu	bakteriorodopsina	k1gFnSc4	bakteriorodopsina
některých	některý	k3yIgFnPc2	některý
archebakterií	archebakterie	k1gFnPc2	archebakterie
<g/>
.	.	kIx.	.
</s>
<s>
Fotosyntetické	fotosyntetický	k2eAgInPc1d1	fotosyntetický
pochody	pochod	k1gInPc1	pochod
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
chloroplastech	chloroplast	k1gInPc6	chloroplast
<g/>
,	,	kIx,	,
dýchací	dýchací	k2eAgInSc1d1	dýchací
řetězec	řetězec	k1gInSc1	řetězec
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
či	či	k8xC	či
na	na	k7c6	na
buněčné	buněčný	k2eAgFnSc6d1	buněčná
membráně	membrána	k1gFnSc6	membrána
prokaryot	prokaryota	k1gFnPc2	prokaryota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VOET	VOET	kA	VOET
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
<g/>
;	;	kIx,	;
VOET	VOET	kA	VOET
<g/>
,	,	kIx,	,
Judith	Judith	k1gInSc1	Judith
<g/>
.	.	kIx.	.
</s>
<s>
Biochemie	biochemie	k1gFnSc1	biochemie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
..	..	k?	..
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Victoria	Victorium	k1gNnSc2	Victorium
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85605	[number]	k4	85605
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VODRÁŽKA	Vodrážka	k1gMnSc1	Vodrážka
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Biochemie	biochemie	k1gFnSc1	biochemie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
600	[number]	k4	600
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
adenosintrifosfát	adenosintrifosfát	k5eAaPmF	adenosintrifosfát
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
