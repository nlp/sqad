<p>
<s>
Soutěska	soutěska	k1gFnSc1	soutěska
je	být	k5eAaImIp3nS	být
geomorfologický	geomorfologický	k2eAgInSc4d1	geomorfologický
tvar	tvar	k1gInSc4	tvar
obvykle	obvykle	k6eAd1	obvykle
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
erozí	eroze	k1gFnSc7	eroze
nebo	nebo	k8xC	nebo
tektonickou	tektonický	k2eAgFnSc7d1	tektonická
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
tento	tento	k3xDgInSc4	tento
termín	termín	k1gInSc4	termín
zavedl	zavést	k5eAaPmAgMnS	zavést
Pavel	Pavel	k1gMnSc1	Pavel
Josef	Josef	k1gMnSc1	Josef
Šafařík	Šafařík	k1gMnSc1	Šafařík
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgMnS	nahradit
tak	tak	k6eAd1	tak
starší	starý	k2eAgInSc4d2	starší
Jungmannův	Jungmannův	k2eAgInSc4d1	Jungmannův
termín	termín	k1gInSc4	termín
těsnina	těsnina	k1gFnSc1	těsnina
<g/>
.	.	kIx.	.
</s>
<s>
Příbuzným	příbuzný	k2eAgInSc7d1	příbuzný
pojmem	pojem	k1gInSc7	pojem
soutěsky	soutěska	k1gFnSc2	soutěska
je	být	k5eAaImIp3nS	být
kaňon	kaňon	k1gInSc1	kaňon
<g/>
.	.	kIx.	.
</s>
<s>
Srázné	srázný	k2eAgNnSc1d1	srázné
úzké	úzký	k2eAgNnSc1d1	úzké
údolí	údolí	k1gNnSc1	údolí
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
rokle	rokle	k1gFnSc1	rokle
<g/>
,	,	kIx,	,
roklina	roklina	k1gFnSc1	roklina
či	či	k8xC	či
strž	strž	k1gFnSc1	strž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typologie	typologie	k1gFnSc2	typologie
==	==	k?	==
</s>
</p>
<p>
<s>
Vymezení	vymezení	k1gNnSc1	vymezení
pojmů	pojem	k1gInPc2	pojem
soutěska	soutěska	k1gFnSc1	soutěska
a	a	k8xC	a
kaňon	kaňon	k1gInSc1	kaňon
z	z	k7c2	z
geomorfologického	geomorfologický	k2eAgNnSc2d1	Geomorfologické
hlediska	hledisko	k1gNnSc2	hledisko
není	být	k5eNaImIp3nS	být
jednotné	jednotný	k2eAgNnSc1d1	jednotné
a	a	k8xC	a
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Demek	Demek	k1gInSc1	Demek
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
pojem	pojem	k1gInSc4	pojem
soutěska	soutěska	k1gFnSc1	soutěska
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
lineární	lineární	k2eAgFnSc7d1	lineární
erozí	eroze	k1gFnSc7	eroze
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
(	(	kIx(	(
<g/>
hloubkové	hloubkový	k2eAgFnSc3d1	hloubková
erozi	eroze	k1gFnSc3	eroze
<g/>
)	)	kIx)	)
a	a	k8xC	a
vývojem	vývoj	k1gInSc7	vývoj
svahů	svah	k1gInPc2	svah
(	(	kIx(	(
<g/>
boční	boční	k2eAgFnSc6d1	boční
erozi	eroze	k1gFnSc6	eroze
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
říční	říční	k2eAgNnPc1d1	říční
údolí	údolí	k1gNnPc1	údolí
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
výrazně	výrazně	k6eAd1	výrazně
převažuje	převažovat	k5eAaImIp3nS	převažovat
hloubková	hloubkový	k2eAgFnSc1d1	hloubková
eroze	eroze	k1gFnSc1	eroze
nad	nad	k7c7	nad
boční	boční	k2eAgFnSc7d1	boční
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
hluboké	hluboký	k2eAgFnPc4d1	hluboká
soutěsky	soutěska	k1gFnPc4	soutěska
pak	pak	k6eAd1	pak
nazývá	nazývat	k5eAaImIp3nS	nazývat
kaňony	kaňon	k1gInPc4	kaňon
<g/>
.	.	kIx.	.
<g/>
Jiní	jiný	k2eAgMnPc1d1	jiný
geologové	geolog	k1gMnPc1	geolog
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Vitásek	Vitásek	k1gMnSc1	Vitásek
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
a	a	k8xC	a
Klimaszewski	Klimaszewske	k1gFnSc4	Klimaszewske
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
definují	definovat	k5eAaBmIp3nP	definovat
kaňon	kaňon	k1gInSc1	kaňon
jako	jako	k8xS	jako
říční	říční	k2eAgNnPc1d1	říční
údolí	údolí	k1gNnPc1	údolí
specifického	specifický	k2eAgInSc2d1	specifický
tvaru	tvar	k1gInSc2	tvar
příčného	příčný	k2eAgInSc2d1	příčný
profilu	profil	k1gInSc2	profil
<g/>
,	,	kIx,	,
odlišného	odlišný	k2eAgInSc2d1	odlišný
od	od	k7c2	od
soutěsky	soutěska	k1gFnSc2	soutěska
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
erozního	erozní	k2eAgInSc2d1	erozní
zářezu	zářez	k1gInSc2	zářez
(	(	kIx(	(
<g/>
Klimaszewski	Klimaszewski	k1gNnSc2	Klimaszewski
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Soutěsky	soutěska	k1gFnPc1	soutěska
nejčastěji	často	k6eAd3	často
vznikají	vznikat	k5eAaImIp3nP	vznikat
hloubkovou	hloubkový	k2eAgFnSc7d1	hloubková
erozí	eroze	k1gFnSc7	eroze
prouděním	proudění	k1gNnSc7	proudění
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pohybem	pohyb	k1gInSc7	pohyb
ledovce	ledovec	k1gInSc2	ledovec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
do	do	k7c2	do
zemského	zemský	k2eAgInSc2d1	zemský
reliéfu	reliéf	k1gInSc2	reliéf
vymývá	vymývat	k5eAaImIp3nS	vymývat
rýhy	rýha	k1gFnPc1	rýha
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
V	V	kA	V
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
U.	U.	kA	U.
Tyto	tento	k3xDgFnPc1	tento
rýhy	rýha	k1gFnPc1	rýha
vznikají	vznikat	k5eAaImIp3nP	vznikat
i	i	k9	i
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
tento	tento	k3xDgInSc4	tento
čas	čas	k1gInSc4	čas
závisí	záviset	k5eAaImIp3nS	záviset
i	i	k9	i
na	na	k7c6	na
typu	typ	k1gInSc6	typ
podloží	podloží	k1gNnSc2	podloží
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pískovec	pískovec	k1gInSc1	pískovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
případě	případ	k1gInSc6	případ
tektonická	tektonický	k2eAgFnSc1d1	tektonická
činnost	činnost	k1gFnSc1	činnost
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
rýhu	rýha	k1gFnSc4	rýha
zdvihem	zdvih	k1gInSc7	zdvih
okolního	okolní	k2eAgInSc2d1	okolní
zemského	zemský	k2eAgInSc2d1	zemský
reliéfu	reliéf	k1gInSc2	reliéf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kaňon	kaňon	k1gInSc4	kaňon
==	==	k?	==
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
kaňonem	kaňon	k1gInSc7	kaňon
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
Grand	grand	k1gMnSc1	grand
Canyon	Canyon	k1gMnSc1	Canyon
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
kaňon	kaňon	k1gInSc1	kaňon
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
terminologii	terminologie	k1gFnSc6	terminologie
označována	označovat	k5eAaImNgFnS	označovat
též	též	k6eAd1	též
údolí	údolí	k1gNnPc1	údolí
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
kaňonem	kaňon	k1gInSc7	kaňon
nejsou	být	k5eNaImIp3nP	být
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Bryce	Bryce	k1gMnSc1	Bryce
Canyon	Canyon	k1gMnSc1	Canyon
v	v	k7c6	v
Utahu	Utah	k1gInSc6	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Mnohá	mnohý	k2eAgNnPc1d1	mnohé
údolí	údolí	k1gNnPc1	údolí
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
kaňonem	kaňon	k1gInSc7	kaňon
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
názvech	název	k1gInPc6	název
jako	jako	k8xS	jako
kaňon	kaňon	k1gInSc4	kaňon
označována	označován	k2eAgFnSc1d1	označována
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kaňony	kaňon	k1gInPc1	kaňon
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
také	také	k9	také
v	v	k7c6	v
krasových	krasový	k2eAgFnPc6d1	krasová
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc1	jejich
vznik	vznik	k1gInSc1	vznik
spojen	spojen	k2eAgInSc1d1	spojen
s	s	k7c7	s
chemickými	chemický	k2eAgFnPc7d1	chemická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
krasověním	krasovění	k1gNnSc7	krasovění
<g/>
.	.	kIx.	.
</s>
<s>
Kaňon	kaňon	k1gInSc1	kaňon
je	být	k5eAaImIp3nS	být
také	také	k9	také
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
paleontology	paleontolog	k1gMnPc4	paleontolog
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jim	on	k3xPp3gMnPc3	on
dává	dávat	k5eAaImIp3nS	dávat
možnost	možnost	k1gFnSc4	možnost
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
zkoumat	zkoumat	k5eAaImF	zkoumat
fosílie	fosílie	k1gFnPc4	fosílie
v	v	k7c6	v
chronologickém	chronologický	k2eAgNnSc6d1	chronologické
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turismus	turismus	k1gInSc4	turismus
==	==	k?	==
</s>
</p>
<p>
<s>
Soutěsky	soutěsk	k1gInPc1	soutěsk
jsou	být	k5eAaImIp3nP	být
atraktivními	atraktivní	k2eAgInPc7d1	atraktivní
turistickými	turistický	k2eAgInPc7d1	turistický
cíli	cíl	k1gInPc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
převážně	převážně	k6eAd1	převážně
nízkým	nízký	k2eAgInSc7d1	nízký
stavem	stav	k1gInSc7	stav
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaImF	stát
nebezpečnými	bezpečný	k2eNgFnPc7d1	nebezpečná
v	v	k7c4	v
období	období	k1gNnSc4	období
přívalových	přívalový	k2eAgInPc2d1	přívalový
dešťů	dešť	k1gInPc2	dešť
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
koryto	koryto	k1gNnSc1	koryto
náhle	náhle	k6eAd1	náhle
zaplaví	zaplavit	k5eAaPmIp3nS	zaplavit
a	a	k8xC	a
příkré	příkrý	k2eAgInPc1d1	příkrý
stěny	stěn	k1gInPc1	stěn
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
špatnou	špatný	k2eAgFnSc4d1	špatná
únikovou	únikový	k2eAgFnSc4d1	úniková
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Opakovaně	opakovaně	k6eAd1	opakovaně
tak	tak	k6eAd1	tak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
utonutí	utonutí	k1gNnSc3	utonutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejznámější	známý	k2eAgInPc1d3	nejznámější
soutěsky	soutěsk	k1gInPc1	soutěsk
==	==	k?	==
</s>
</p>
<p>
<s>
Soutěsky	soutěsk	k1gInPc1	soutěsk
Kamenice	Kamenice	k1gFnSc2	Kamenice
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Grand	grand	k1gMnSc1	grand
Canyon	Canyon	k1gMnSc1	Canyon
(	(	kIx(	(
<g/>
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tři	tři	k4xCgFnPc1	tři
soutěsky	soutěska	k1gFnPc1	soutěska
(	(	kIx(	(
<g/>
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Samaria	samarium	k1gNnPc1	samarium
(	(	kIx(	(
<g/>
Kréta	Kréta	k1gFnSc1	Kréta
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Darialská	Darialský	k2eAgFnSc1d1	Darialský
soutěska	soutěska	k1gFnSc1	soutěska
(	(	kIx(	(
<g/>
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
<g/>
,	,	kIx,	,
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
fotografie	fotografia	k1gFnPc1	fotografia
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
soutěska	soutěska	k1gFnSc1	soutěska
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Hydrografie	hydrografie	k1gFnSc1	hydrografie
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
(	(	kIx(	(
<g/>
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
MU	MU	kA	MU
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Geomorfologické	geomorfologický	k2eAgInPc1d1	geomorfologický
tvary	tvar	k1gInPc1	tvar
<g/>
.	.	kIx.	.
</s>
</p>
