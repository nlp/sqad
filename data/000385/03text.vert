<s>
LZ-129	LZ-129	k4	LZ-129
Hindenburg	Hindenburg	k1gInSc1	Hindenburg
byla	být	k5eAaImAgFnS	být
německá	německý	k2eAgFnSc1d1	německá
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
požárem	požár	k1gInSc7	požár
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1937	[number]	k4	1937
při	při	k7c6	při
přistávání	přistávání	k1gNnSc6	přistávání
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Lakehurst	Lakehurst	k1gFnSc1	Lakehurst
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
97	[number]	k4	97
osob	osoba	k1gFnPc2	osoba
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
katastrofě	katastrofa	k1gFnSc6	katastrofa
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
13	[number]	k4	13
pasažérů	pasažér	k1gMnPc2	pasažér
a	a	k8xC	a
22	[number]	k4	22
členů	člen	k1gInPc2	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeden	jeden	k4xCgMnSc1	jeden
člen	člen	k1gMnSc1	člen
pozemního	pozemní	k2eAgInSc2d1	pozemní
personálu	personál	k1gInSc2	personál
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
si	se	k3xPyFc3	se
katastrofa	katastrofa	k1gFnSc1	katastrofa
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
36	[number]	k4	36
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Zeppelin	Zeppelin	k2eAgMnSc1d1	Zeppelin
LZ-129	LZ-129	k1gMnSc1	LZ-129
Hindenburg	Hindenburg	k1gMnSc1	Hindenburg
byl	být	k5eAaImAgMnS	být
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sesterskou	sesterský	k2eAgFnSc7d1	sesterská
lodí	loď	k1gFnSc7	loď
LZ-130	LZ-130	k1gFnSc2	LZ-130
Graf	graf	k1gInSc1	graf
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
II	II	kA	II
největším	veliký	k2eAgInSc7d3	veliký
létajícím	létající	k2eAgInSc7d1	létající
strojem	stroj	k1gInSc7	stroj
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
říšském	říšský	k2eAgMnSc6d1	říšský
prezidentovi	prezident	k1gMnSc6	prezident
Paulovi	Paul	k1gMnSc6	Paul
von	von	k1gInSc4	von
Hindenburg	Hindenburg	k1gMnSc1	Hindenburg
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
ho	on	k3xPp3gMnSc4	on
sestrojila	sestrojit	k5eAaPmAgFnS	sestrojit
firma	firma	k1gFnSc1	firma
Luftschiffbau	Luftschiffbaus	k1gInSc2	Luftschiffbaus
Zeppelin	Zeppelin	k2eAgMnSc1d1	Zeppelin
s	s	k7c7	s
náklady	náklad	k1gInPc7	náklad
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
500	[number]	k4	500
000	[number]	k4	000
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
zcela	zcela	k6eAd1	zcela
novou	nový	k2eAgFnSc4d1	nová
konstrukci	konstrukce	k1gFnSc4	konstrukce
z	z	k7c2	z
duralu	dural	k1gInSc2	dural
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgInS	být
245	[number]	k4	245
m	m	kA	m
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
(	(	kIx(	(
<g/>
jen	jen	k9	jen
o	o	k7c4	o
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
m	m	kA	m
kratší	krátký	k2eAgMnSc1d2	kratší
než	než	k8xS	než
Titanic	Titanic	k1gInSc1	Titanic
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
41	[number]	k4	41
m	m	kA	m
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
,	,	kIx,	,
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
zhruba	zhruba	k6eAd1	zhruba
200	[number]	k4	200
000	[number]	k4	000
m3	m3	k4	m3
plynu	plyn	k1gInSc2	plyn
rozděleného	rozdělený	k2eAgNnSc2d1	rozdělené
do	do	k7c2	do
16	[number]	k4	16
oddílů	oddíl	k1gInPc2	oddíl
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
vztlakem	vztlak	k1gInSc7	vztlak
cca	cca	kA	cca
240	[number]	k4	240
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
112	[number]	k4	112
tun	tuna	k1gFnPc2	tuna
bylo	být	k5eAaImAgNnS	být
užitečné	užitečný	k2eAgNnSc1d1	užitečné
zatížení	zatížení	k1gNnSc1	zatížení
<g/>
.	.	kIx.	.
</s>
<s>
Poháněly	pohánět	k5eAaImAgFnP	pohánět
jej	on	k3xPp3gMnSc4	on
čtyři	čtyři	k4xCgInPc4	čtyři
dieselové	dieselový	k2eAgInPc4d1	dieselový
motory	motor	k1gInPc4	motor
Mercedes-Benz	Mercedes-Benza	k1gFnPc2	Mercedes-Benza
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
890	[number]	k4	890
kW	kW	kA	kW
<g/>
;	;	kIx,	;
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
maximální	maximální	k2eAgFnSc4d1	maximální
rychlost	rychlost	k1gFnSc4	rychlost
135	[number]	k4	135
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Potah	potah	k1gInSc1	potah
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
z	z	k7c2	z
bavlny	bavlna	k1gFnSc2	bavlna
impregnované	impregnovaný	k2eAgFnSc2d1	impregnovaná
pro	pro	k7c4	pro
nepropustnost	nepropustnost	k1gFnSc4	nepropustnost
směsí	směs	k1gFnPc2	směs
oxidu	oxid	k1gInSc2	oxid
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
acetátu	acetát	k1gInSc2	acetát
celulózy	celulóza	k1gFnSc2	celulóza
a	a	k8xC	a
potažené	potažený	k2eAgNnSc1d1	potažené
hliníkovým	hliníkový	k2eAgInSc7d1	hliníkový
prachem	prach	k1gInSc7	prach
<g/>
.	.	kIx.	.
</s>
<s>
Hindenburg	Hindenburg	k1gInSc1	Hindenburg
mohl	moct	k5eAaImAgInS	moct
nést	nést	k5eAaImF	nést
72	[number]	k4	72
pasažérů	pasažér	k1gMnPc2	pasažér
(	(	kIx(	(
<g/>
50	[number]	k4	50
při	při	k7c6	při
transatlantickém	transatlantický	k2eAgInSc6d1	transatlantický
letu	let	k1gInSc6	let
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
61	[number]	k4	61
členů	člen	k1gInPc2	člen
posádky	posádka	k1gFnSc2	posádka
a	a	k8xC	a
náklad	náklad	k1gInSc1	náklad
(	(	kIx(	(
<g/>
v	v	k7c6	v
nákladním	nákladní	k2eAgInSc6d1	nákladní
prostoru	prostor	k1gInSc6	prostor
na	na	k7c6	na
zádi	záď	k1gFnSc6	záď
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
přepravovaly	přepravovat	k5eAaImAgInP	přepravovat
i	i	k9	i
malé	malý	k2eAgInPc1d1	malý
automobily	automobil	k1gInPc1	automobil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
lepších	dobrý	k2eAgFnPc2d2	lepší
aerodynamických	aerodynamický	k2eAgFnPc2d1	aerodynamická
vlastností	vlastnost	k1gFnPc2	vlastnost
byly	být	k5eAaImAgFnP	být
kabiny	kabina	k1gFnPc1	kabina
pasažérů	pasažér	k1gMnPc2	pasažér
umístěny	umístit	k5eAaPmNgInP	umístit
uvnitř	uvnitř	k7c2	uvnitř
trupu	trup	k1gInSc2	trup
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
v	v	k7c6	v
podvěšených	podvěšený	k2eAgFnPc6d1	podvěšená
gondolách	gondola	k1gFnPc6	gondola
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bývalo	bývat	k5eAaImAgNnS	bývat
dříve	dříve	k6eAd2	dříve
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
nabízel	nabízet	k5eAaImAgInS	nabízet
na	na	k7c4	na
leteckou	letecký	k2eAgFnSc4d1	letecká
dopravu	doprava	k1gFnSc4	doprava
nebývalý	nebývalý	k2eAgInSc4d1	nebývalý
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
luxus	luxus	k1gInSc4	luxus
<g/>
:	:	kIx,	:
teplá	teplý	k2eAgNnPc4d1	teplé
jídla	jídlo	k1gNnPc4	jídlo
<g/>
,	,	kIx,	,
sprchy	sprcha	k1gFnPc4	sprcha
se	s	k7c7	s
studenou	studený	k2eAgFnSc7d1	studená
i	i	k8xC	i
teplou	teplý	k2eAgFnSc7d1	teplá
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
klubovně	klubovna	k1gFnSc6	klubovna
na	na	k7c6	na
vyhlídkové	vyhlídkový	k2eAgFnSc6d1	vyhlídková
palubě	paluba	k1gFnSc6	paluba
hrál	hrát	k5eAaImAgMnS	hrát
dokonce	dokonce	k9	dokonce
pianista	pianista	k1gMnSc1	pianista
na	na	k7c4	na
duralové	duralový	k2eAgNnSc4d1	duralové
piáno	piáno	k?	piáno
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
letenky	letenka	k1gFnSc2	letenka
byla	být	k5eAaImAgFnS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
levnějším	levný	k2eAgInSc7d2	levnější
automobilem	automobil	k1gInSc7	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
plán	plán	k1gInSc1	plán
počítal	počítat	k5eAaImAgInS	počítat
s	s	k7c7	s
plněním	plnění	k1gNnSc7	plnění
heliem	helium	k1gNnSc7	helium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vojenské	vojenský	k2eAgNnSc1d1	vojenské
embargo	embargo	k1gNnSc1	embargo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
přinutilo	přinutit	k5eAaPmAgNnS	přinutit
německé	německý	k2eAgNnSc1d1	německé
konstruktéry	konstruktér	k1gMnPc4	konstruktér
změnit	změnit	k5eAaPmF	změnit
projekt	projekt	k1gInSc1	projekt
a	a	k8xC	a
použít	použít	k5eAaPmF	použít
jako	jako	k8xC	jako
nosný	nosný	k2eAgInSc4d1	nosný
plyn	plyn	k1gInSc4	plyn
vysoce	vysoce	k6eAd1	vysoce
hořlavý	hořlavý	k2eAgInSc4d1	hořlavý
vodík	vodík	k1gInSc4	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
helia	helium	k1gNnSc2	helium
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
USA	USA	kA	USA
tehdy	tehdy	k6eAd1	tehdy
nestačila	stačit	k5eNaBmAgFnS	stačit
ani	ani	k9	ani
pro	pro	k7c4	pro
americké	americký	k2eAgFnPc4d1	americká
vzducholodě	vzducholoď	k1gFnPc4	vzducholoď
a	a	k8xC	a
nikde	nikde	k6eAd1	nikde
jinde	jinde	k6eAd1	jinde
se	se	k3xPyFc4	se
helium	helium	k1gNnSc1	helium
nevyrábělo	vyrábět	k5eNaImAgNnS	vyrábět
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Protože	protože	k8xS	protože
však	však	k9	však
vodík	vodík	k1gInSc1	vodík
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
oproti	oproti	k7c3	oproti
héliu	hélium	k1gNnSc3	hélium
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c6	o
10	[number]	k4	10
%	%	kIx~	%
větší	veliký	k2eAgInSc4d2	veliký
vztlak	vztlak	k1gInSc4	vztlak
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
při	při	k7c6	při
úpravách	úprava	k1gFnPc6	úprava
přidány	přidán	k2eAgInPc4d1	přidán
i	i	k9	i
další	další	k2eAgFnSc2d1	další
kabiny	kabina	k1gFnSc2	kabina
pro	pro	k7c4	pro
pasažéry	pasažér	k1gMnPc4	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
používáním	používání	k1gNnSc7	používání
vodíku	vodík	k1gInSc2	vodík
ve	v	k7c6	v
vzducholodích	vzducholoď	k1gFnPc6	vzducholoď
již	již	k6eAd1	již
měli	mít	k5eAaImAgMnP	mít
Němci	Němec	k1gMnPc1	Němec
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
změna	změna	k1gFnSc1	změna
neznamenala	znamenat	k5eNaImAgFnS	znamenat
důvod	důvod	k1gInSc4	důvod
ke	k	k7c3	k
znepokojení	znepokojení	k1gNnSc3	znepokojení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
známá	známý	k2eAgNnPc4d1	známé
rizika	riziko	k1gNnPc4	riziko
vodíku	vodík	k1gInSc2	vodík
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
konstrukce	konstrukce	k1gFnSc1	konstrukce
Hindenburgu	Hindenburg	k1gInSc2	Hindenburg
různé	různý	k2eAgInPc1d1	různý
bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgInP	mít
zabránit	zabránit	k5eAaPmF	zabránit
požáru	požár	k1gInSc2	požár
v	v	k7c6	v
případě	případ	k1gInSc6	případ
úniku	únik	k1gInSc2	únik
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
a	a	k8xC	a
potah	potah	k1gInSc1	potah
byl	být	k5eAaImAgInS	být
speciálně	speciálně	k6eAd1	speciálně
upraven	upravit	k5eAaPmNgInS	upravit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
jiskrám	jiskra	k1gFnPc3	jiskra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
způsobit	způsobit	k5eAaPmF	způsobit
požár	požár	k1gInSc4	požár
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
zacházení	zacházení	k1gNnSc2	zacházení
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
byla	být	k5eAaImAgFnS	být
tak	tak	k9	tak
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Hindenburgu	Hindenburg	k1gInSc6	Hindenburg
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
i	i	k9	i
kuřárna	kuřárna	k1gFnSc1	kuřárna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
let	let	k1gInSc4	let
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
Hindenburg	Hindenburg	k1gMnSc1	Hindenburg
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1936	[number]	k4	1936
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
provedl	provést	k5eAaPmAgInS	provést
rekordní	rekordní	k2eAgInSc1d1	rekordní
dvojnásobný	dvojnásobný	k2eAgInSc1d1	dvojnásobný
přelet	přelet	k1gInSc1	přelet
Atlantiku	Atlantik	k1gInSc2	Atlantik
v	v	k7c6	v
čase	čas	k1gInSc6	čas
5	[number]	k4	5
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
19	[number]	k4	19
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
51	[number]	k4	51
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1937	[number]	k4	1937
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
Hindenburg	Hindenburg	k1gInSc1	Hindenburg
z	z	k7c2	z
frankfurtského	frankfurtský	k2eAgNnSc2d1	Frankfurtské
letiště	letiště	k1gNnSc2	letiště
na	na	k7c4	na
první	první	k4xOgFnPc4	první
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
osmnácti	osmnáct	k4xCc2	osmnáct
plánovaných	plánovaný	k2eAgInPc2d1	plánovaný
transatlantických	transatlantický	k2eAgInPc2d1	transatlantický
letů	let	k1gInPc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
protivětru	protivítr	k1gInSc3	protivítr
nabral	nabrat	k5eAaPmAgMnS	nabrat
zpoždění	zpoždění	k1gNnSc2	zpoždění
již	již	k6eAd1	již
při	při	k7c6	při
přeletu	přelet	k1gInSc6	přelet
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příletu	přílet	k1gInSc6	přílet
k	k	k7c3	k
New	New	k1gFnPc3	New
Yorku	York	k1gInSc2	York
byla	být	k5eAaImAgFnS	být
nad	nad	k7c4	nad
New	New	k1gFnSc4	New
Jersey	Jerse	k2eAgFnPc4d1	Jerse
bouře	bouř	k1gFnPc4	bouř
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
vzducholoď	vzducholoď	k1gFnSc4	vzducholoď
město	město	k1gNnSc4	město
dvakrát	dvakrát	k6eAd1	dvakrát
obkroužila	obkroužit	k5eAaPmAgFnS	obkroužit
a	a	k8xC	a
k	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
Lakehurst	Lakehurst	k1gMnSc1	Lakehurst
dorazila	dorazit	k5eAaPmAgFnS	dorazit
s	s	k7c7	s
půldenním	půldenní	k2eAgNnSc7d1	půldenní
zpožděním	zpoždění	k1gNnSc7	zpoždění
pozdě	pozdě	k6eAd1	pozdě
odpoledne	odpoledne	k6eAd1	odpoledne
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
přistávací	přistávací	k2eAgFnSc1d1	přistávací
operace	operace	k1gFnSc1	operace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
21	[number]	k4	21
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
shozeno	shozen	k2eAgNnSc1d1	shozeno
první	první	k4xOgNnSc1	první
přistávací	přistávací	k2eAgNnSc1d1	přistávací
lano	lano	k1gNnSc1	lano
a	a	k8xC	a
pozemní	pozemní	k2eAgInSc1d1	pozemní
personál	personál	k1gInSc1	personál
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
upoutáváním	upoutávání	k1gNnSc7	upoutávání
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgFnPc4	čtyři
minuty	minuta	k1gFnPc4	minuta
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
Hindenburg	Hindenburg	k1gInSc4	Hindenburg
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
asi	asi	k9	asi
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
vzplanul	vzplanout	k5eAaPmAgMnS	vzplanout
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
požár	požár	k1gInSc4	požár
a	a	k8xC	a
během	během	k7c2	během
34	[number]	k4	34
sekund	sekunda	k1gFnPc2	sekunda
celá	celý	k2eAgFnSc1d1	celá
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
shořela	shořet	k5eAaPmAgFnS	shořet
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
36	[number]	k4	36
pasažérů	pasažér	k1gMnPc2	pasažér
a	a	k8xC	a
61	[number]	k4	61
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
při	při	k7c6	při
katastrofě	katastrofa	k1gFnSc6	katastrofa
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
13	[number]	k4	13
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
22	[number]	k4	22
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
život	život	k1gInSc4	život
přišel	přijít	k5eAaPmAgMnS	přijít
i	i	k9	i
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
pozemního	pozemní	k2eAgInSc2d1	pozemní
personálu	personál	k1gInSc2	personál
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
"	"	kIx"	"
<g/>
slávě	sláva	k1gFnSc3	sláva
<g/>
"	"	kIx"	"
katastrofy	katastrofa	k1gFnSc2	katastrofa
bezesporu	bezesporu	k9	bezesporu
přispělo	přispět	k5eAaPmAgNnS	přispět
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
nezvykle	zvykle	k6eNd1	zvykle
dokonalé	dokonalý	k2eAgNnSc1d1	dokonalé
zpravodajské	zpravodajský	k2eAgNnSc1d1	zpravodajské
pokrytí	pokrytí	k1gNnSc1	pokrytí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
na	na	k7c4	na
Hindenburg	Hindenburg	k1gInSc4	Hindenburg
totiž	totiž	k9	totiž
čekalo	čekat	k5eAaImAgNnS	čekat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
reportérů	reportér	k1gMnPc2	reportér
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
existuje	existovat	k5eAaImIp3nS	existovat
záznam	záznam	k1gInSc1	záznam
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
Herberta	Herbert	k1gMnSc2	Herbert
Morrisona	Morrison	k1gMnSc2	Morrison
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgInSc1d1	filmový
záznam	záznam	k1gInSc1	záznam
a	a	k8xC	a
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Morrisonovo	Morrisonův	k2eAgNnSc1d1	Morrisonovo
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
bylo	být	k5eAaImAgNnS	být
vysíláno	vysílat	k5eAaImNgNnS	vysílat
později	pozdě	k6eAd2	pozdě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
událostí	událost	k1gFnPc2	událost
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Existující	existující	k2eAgFnSc1d1	existující
moderní	moderní	k2eAgFnSc1d1	moderní
verze	verze	k1gFnSc1	verze
filmového	filmový	k2eAgInSc2d1	filmový
záznamu	záznam	k1gInSc2	záznam
doprovázeného	doprovázený	k2eAgInSc2d1	doprovázený
úryvky	úryvek	k1gInPc4	úryvek
z	z	k7c2	z
Morrisonovy	Morrisonův	k2eAgFnSc2d1	Morrisonova
reportáže	reportáž	k1gFnSc2	reportáž
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
mnohem	mnohem	k6eAd1	mnohem
pozdější	pozdní	k2eAgFnSc2d2	pozdější
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
příčinách	příčina	k1gFnPc6	příčina
katastrofy	katastrofa	k1gFnSc2	katastrofa
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
vedou	vést	k5eAaImIp3nP	vést
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
teorií	teorie	k1gFnPc2	teorie
<g/>
:	:	kIx,	:
Těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
katastrofě	katastrofa	k1gFnSc6	katastrofa
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
běžně	běžně	k6eAd1	běžně
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
teorie	teorie	k1gFnSc1	teorie
sabotáže	sabotáž	k1gFnSc2	sabotáž
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
především	především	k9	především
zástupci	zástupce	k1gMnPc1	zástupce
firmy	firma	k1gFnSc2	firma
Zeppelin	Zeppelin	k2eAgMnSc1d1	Zeppelin
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
Hugo	Hugo	k1gMnSc1	Hugo
Eckener	Eckener	k1gMnSc1	Eckener
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
předseda	předseda	k1gMnSc1	předseda
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vzducholodi	vzducholoď	k1gFnPc1	vzducholoď
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
symboly	symbol	k1gInPc4	symbol
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
nacistické	nacistický	k2eAgFnSc2d1	nacistická
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
lákavým	lákavý	k2eAgInSc7d1	lákavý
cílem	cíl	k1gInSc7	cíl
jak	jak	k8xC	jak
pro	pro	k7c4	pro
protivníky	protivník	k1gMnPc4	protivník
nacistů	nacista	k1gMnPc2	nacista
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
soupeřící	soupeřící	k2eAgFnPc4d1	soupeřící
frakce	frakce	k1gFnPc4	frakce
uvnitř	uvnitř	k7c2	uvnitř
nacistické	nacistický	k2eAgFnSc2d1	nacistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Teorii	teorie	k1gFnSc4	teorie
také	také	k9	také
nahrávalo	nahrávat	k5eAaImAgNnS	nahrávat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
záznamů	záznam	k1gInPc2	záznam
požár	požár	k1gInSc1	požár
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
uprostřed	uprostřed	k7c2	uprostřed
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
kotvicích	kotvice	k1gFnPc6	kotvice
lan	lano	k1gNnPc2	lano
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
však	však	k9	však
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
nepodpořily	podpořit	k5eNaPmAgInP	podpořit
žádné	žádný	k3yNgInPc1	žádný
konkrétní	konkrétní	k2eAgInPc1d1	konkrétní
důkazy	důkaz	k1gInPc1	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
to	ten	k3xDgNnSc4	ten
nelze	lze	k6eNd1	lze
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
za	za	k7c4	za
nejpravděpodobnější	pravděpodobný	k2eAgFnSc4d3	nejpravděpodobnější
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
považuje	považovat	k5eAaImIp3nS	považovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
požár	požár	k1gInSc4	požár
způsobila	způsobit	k5eAaPmAgFnS	způsobit
jiskra	jiskra	k1gFnSc1	jiskra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
nashromážděné	nashromážděný	k2eAgFnSc2d1	nashromážděná
statické	statický	k2eAgFnSc2d1	statická
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
svědčí	svědčit	k5eAaImIp3nS	svědčit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
nebyla	být	k5eNaImAgFnS	být
konstruována	konstruovat	k5eAaImNgFnS	konstruovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
mohl	moct	k5eAaImAgMnS	moct
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
volně	volně	k6eAd1	volně
rozprostřít	rozprostřít	k5eAaPmF	rozprostřít
<g/>
,	,	kIx,	,
a	a	k8xC	a
potah	potah	k1gInSc1	potah
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
hliníkové	hliníkový	k2eAgFnSc2d1	hliníková
kostry	kostra	k1gFnSc2	kostra
oddělen	oddělit	k5eAaPmNgInS	oddělit
nevodivými	vodivý	k2eNgFnPc7d1	nevodivá
šňůrami	šňůra	k1gFnPc7	šňůra
z	z	k7c2	z
ramie	ramie	k1gFnSc2	ramie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
letu	let	k1gInSc6	let
prošla	projít	k5eAaPmAgFnS	projít
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
přes	přes	k7c4	přes
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kotvicí	kotvice	k1gFnSc7	kotvice
lana	lano	k1gNnSc2	lano
zvlhla	zvlhnout	k5eAaPmAgFnS	zvlhnout
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
vodivými	vodivý	k2eAgFnPc7d1	vodivá
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tření	tření	k1gNnSc6	tření
povrchu	povrch	k1gInSc2	povrch
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
o	o	k7c4	o
vzduch	vzduch	k1gInSc4	vzduch
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
vzniká	vznikat	k5eAaImIp3nS	vznikat
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
kotvicí	kotvice	k1gFnSc7	kotvice
lana	lano	k1gNnPc4	lano
připojená	připojený	k2eAgNnPc4d1	připojené
ke	k	k7c3	k
kostře	kostra	k1gFnSc3	kostra
dotkla	dotknout	k5eAaPmAgFnS	dotknout
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
uzemnila	uzemnit	k5eAaPmAgFnS	uzemnit
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
celá	celý	k2eAgFnSc1d1	celá
hliníková	hliníkový	k2eAgFnSc1d1	hliníková
kostra	kostra	k1gFnSc1	kostra
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
potahem	potah	k1gInSc7	potah
a	a	k8xC	a
kostrou	kostra	k1gFnSc7	kostra
přeskočil	přeskočit	k5eAaPmAgMnS	přeskočit
elektrický	elektrický	k2eAgInSc4d1	elektrický
výboj	výboj	k1gInSc4	výboj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svědků	svědek	k1gMnPc2	svědek
bylo	být	k5eAaImAgNnS	být
před	před	k7c7	před
požárem	požár	k1gInSc7	požár
kolem	kolem	k7c2	kolem
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
vidět	vidět	k5eAaImF	vidět
světélkování	světélkování	k1gNnSc4	světélkování
podobné	podobný	k2eAgNnSc4d1	podobné
ohni	oheň	k1gInSc3	oheň
Svatého	svatý	k2eAgMnSc2d1	svatý
Eliáše	Eliáš	k1gMnSc2	Eliáš
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
populární	populární	k2eAgFnSc1d1	populární
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zastává	zastávat	k5eAaImIp3nS	zastávat
např.	např.	kA	např.
přední	přední	k2eAgMnSc1d1	přední
americký	americký	k2eAgMnSc1d1	americký
odborník	odborník	k1gMnSc1	odborník
na	na	k7c4	na
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
leteckých	letecký	k2eAgFnPc2d1	letecká
katastrof	katastrofa	k1gFnPc2	katastrofa
Greg	Greg	k1gMnSc1	Greg
Feith	Feith	k1gMnSc1	Feith
<g/>
,	,	kIx,	,
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
filmový	filmový	k2eAgInSc4d1	filmový
záznam	záznam	k1gInSc4	záznam
nehody	nehoda	k1gFnSc2	nehoda
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
vzplanutím	vzplanutí	k1gNnSc7	vzplanutí
vidět	vidět	k5eAaImF	vidět
poměrně	poměrně	k6eAd1	poměrně
prudký	prudký	k2eAgInSc1d1	prudký
obrat	obrat	k1gInSc1	obrat
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
<g/>
.	.	kIx.	.
</s>
<s>
Instrukce	instrukce	k1gFnPc4	instrukce
vštěpované	vštěpovaný	k2eAgFnSc3d1	vštěpovaná
posádce	posádka	k1gFnSc3	posádka
při	při	k7c6	při
výcviku	výcvik	k1gInSc6	výcvik
přitom	přitom	k6eAd1	přitom
vyžadovaly	vyžadovat	k5eAaImAgFnP	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
takovýmto	takovýto	k3xDgInPc3	takovýto
manévrům	manévr	k1gInPc3	manévr
vyhýbala	vyhýbat	k5eAaImAgFnS	vyhýbat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
příliš	příliš	k6eAd1	příliš
zatěžovaly	zatěžovat	k5eAaImAgFnP	zatěžovat
její	její	k3xOp3gFnSc4	její
relativně	relativně	k6eAd1	relativně
křehkou	křehký	k2eAgFnSc4d1	křehká
konstrukci	konstrukce	k1gFnSc4	konstrukce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zádě	záď	k1gFnSc2	záď
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
proto	proto	k8xC	proto
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
obratu	obrat	k1gInSc6	obrat
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prasknutí	prasknutí	k1gNnSc3	prasknutí
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
výztuh	výztuha	k1gFnPc2	výztuha
kostry	kostra	k1gFnSc2	kostra
vnějšího	vnější	k2eAgInSc2d1	vnější
pláště	plášť	k1gInSc2	plášť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
poté	poté	k6eAd1	poté
při	při	k7c6	při
zpětném	zpětný	k2eAgInSc6d1	zpětný
zášvihu	zášvih	k1gInSc6	zášvih
prorazila	prorazit	k5eAaPmAgFnS	prorazit
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
záďových	záďový	k2eAgInPc2d1	záďový
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
způsobila	způsobit	k5eAaPmAgFnS	způsobit
masívní	masívní	k2eAgInSc4d1	masívní
únik	únik	k1gInSc4	únik
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Unikající	unikající	k2eAgInSc1d1	unikající
vodík	vodík	k1gInSc1	vodík
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
zapálen	zapálit	k5eAaPmNgInS	zapálit
přeskokem	přeskok	k1gInSc7	přeskok
výše	výše	k1gFnSc2	výše
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
statického	statický	k2eAgInSc2d1	statický
náboje	náboj	k1gInSc2	náboj
mezi	mezi	k7c7	mezi
kotevními	kotevní	k2eAgNnPc7d1	kotevní
lany	lano	k1gNnPc7	lano
uzemněnou	uzemněný	k2eAgFnSc4d1	uzemněná
kostrou	kostra	k1gFnSc7	kostra
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
nevybitým	vybitý	k2eNgInSc7d1	nevybitý
vnějším	vnější	k2eAgInSc7d1	vnější
pláštěm	plášť	k1gInSc7	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
svědčí	svědčit	k5eAaImIp3nS	svědčit
nejen	nejen	k6eAd1	nejen
svědectví	svědectví	k1gNnSc4	svědectví
některých	některý	k3yIgMnPc2	některý
přihlížejících	přihlížející	k2eAgMnPc2d1	přihlížející
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgMnPc2	jenž
nahoře	nahoře	k6eAd1	nahoře
na	na	k7c6	na
zádi	záď	k1gFnSc6	záď
povlávaly	povlávat	k5eAaImAgInP	povlávat
kousky	kousek	k1gInPc1	kousek
pláště	plášť	k1gInSc2	plášť
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mohlo	moct	k5eAaImAgNnS	moct
znamenat	znamenat	k5eAaImF	znamenat
únik	únik	k1gInSc4	únik
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
vyvažováním	vyvažování	k1gNnSc7	vyvažování
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
během	během	k7c2	během
poslední	poslední	k2eAgFnSc2d1	poslední
fáze	fáze	k1gFnSc2	fáze
manévru	manévr	k1gInSc2	manévr
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nečekané	čekaný	k2eNgNnSc4d1	nečekané
náhlé	náhlý	k2eAgNnSc4d1	náhlé
ztěžknutí	ztěžknutí	k1gNnSc4	ztěžknutí
zádě	záď	k1gFnSc2	záď
přinutilo	přinutit	k5eAaPmAgNnS	přinutit
posádku	posádka	k1gFnSc4	posádka
nejen	nejen	k6eAd1	nejen
vypustit	vypustit	k5eAaPmF	vypustit
veškerou	veškerý	k3xTgFnSc4	veškerý
vodní	vodní	k2eAgFnSc4d1	vodní
zátěž	zátěž	k1gFnSc4	zátěž
na	na	k7c6	na
zádi	záď	k1gFnSc6	záď
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
i	i	k9	i
přesunout	přesunout	k5eAaPmF	přesunout
členy	člen	k1gInPc4	člen
posádky	posádka	k1gFnSc2	posádka
ze	z	k7c2	z
zádě	záď	k1gFnSc2	záď
na	na	k7c4	na
příď	příď	k1gFnSc4	příď
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
samotné	samotný	k2eAgNnSc1d1	samotné
vypuštění	vypuštění	k1gNnSc1	vypuštění
zadní	zadní	k2eAgFnSc2d1	zadní
zátěže	zátěž	k1gFnSc2	zátěž
nestačilo	stačit	k5eNaBmAgNnS	stačit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
oblastí	oblast	k1gFnSc7	oblast
sporů	spor	k1gInPc2	spor
kolem	kolem	k7c2	kolem
katastrofy	katastrofa	k1gFnSc2	katastrofa
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
látku	látka	k1gFnSc4	látka
jiskra	jiskra	k1gFnSc1	jiskra
zapálila	zapálit	k5eAaPmAgFnS	zapálit
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
evidentní	evidentní	k2eAgFnSc2d1	evidentní
možnosti	možnost	k1gFnSc2	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
hořel	hořet	k5eAaImAgInS	hořet
samotný	samotný	k2eAgInSc1d1	samotný
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohl	moct	k5eAaImAgInS	moct
vzplanout	vzplanout	k5eAaPmF	vzplanout
potah	potah	k1gInSc1	potah
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
hořlavé	hořlavý	k2eAgInPc4d1	hořlavý
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
nátěr	nátěr	k1gInSc1	nátěr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
látku	látka	k1gFnSc4	látka
chránit	chránit	k5eAaImF	chránit
před	před	k7c7	před
účinky	účinek	k1gInPc7	účinek
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
kterým	který	k3yIgInPc3	který
byla	být	k5eAaImAgFnS	být
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
opatřena	opatřit	k5eAaPmNgFnS	opatřit
před	před	k7c7	před
odletem	odlet	k1gInSc7	odlet
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
svědčí	svědčit	k5eAaImIp3nS	svědčit
oranžová	oranžový	k2eAgFnSc1d1	oranžová
barva	barva	k1gFnSc1	barva
plamene	plamen	k1gInSc2	plamen
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
na	na	k7c4	na
filmový	filmový	k2eAgInSc4d1	filmový
záznam	záznam	k1gInSc4	záznam
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vodík	vodík	k1gInSc1	vodík
hoří	hořet	k5eAaImIp3nS	hořet
bezbarvým	bezbarvý	k2eAgInSc7d1	bezbarvý
plamenem	plamen	k1gInSc7	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
před	před	k7c7	před
nehodou	nehoda	k1gFnSc7	nehoda
Hindenburgu	Hindenburg	k1gInSc2	Hindenburg
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
nehod	nehoda	k1gFnPc2	nehoda
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
způsobených	způsobený	k2eAgInPc2d1	způsobený
špatným	špatný	k2eAgNnSc7d1	špatné
počasím	počasí	k1gNnSc7	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
vážnější	vážní	k2eAgFnSc1d2	vážnější
nehoda	nehoda	k1gFnSc1	nehoda
se	se	k3xPyFc4	se
však	však	k9	však
netýkala	týkat	k5eNaImAgFnS	týkat
Zeppelinů	Zeppelin	k1gInPc2	Zeppelin
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
držely	držet	k5eAaImAgFnP	držet
pozoruhodný	pozoruhodný	k2eAgInSc4d1	pozoruhodný
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
letecké	letecký	k2eAgFnSc2d1	letecká
dopravy	doprava	k1gFnSc2	doprava
<g/>
;	;	kIx,	;
např.	např.	kA	např.
Graf	graf	k1gInSc4	graf
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
bezpečně	bezpečně	k6eAd1	bezpečně
nalétal	nalétat	k5eAaBmAgMnS	nalétat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1,6	[number]	k4	1,6
miliónu	milión	k4xCgInSc2	milión
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
i	i	k9	i
první	první	k4xOgInSc4	první
úplný	úplný	k2eAgInSc4d1	úplný
oblet	oblet	k1gInSc4	oblet
zeměkoule	zeměkoule	k1gFnSc1	zeměkoule
a	a	k8xC	a
dopravila	dopravit	k5eAaPmAgFnS	dopravit
12000	[number]	k4	12000
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
hrdě	hrdě	k6eAd1	hrdě
zdůrazňovala	zdůrazňovat	k5eAaImAgFnS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jejích	její	k3xOp3gFnPc6	její
vzducholodích	vzducholoď	k1gFnPc6	vzducholoď
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nezranil	zranit	k5eNaPmAgMnS	zranit
jediný	jediný	k2eAgMnSc1d1	jediný
pasažér	pasažér	k1gMnSc1	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Katastrofa	katastrofa	k1gFnSc1	katastrofa
Hindenburgu	Hindenburg	k1gInSc6	Hindenburg
to	ten	k3xDgNnSc4	ten
zcela	zcela	k6eAd1	zcela
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Efektní	efektní	k2eAgInPc4d1	efektní
filmové	filmový	k2eAgInPc4d1	filmový
záběry	záběr	k1gInPc4	záběr
a	a	k8xC	a
vzrušený	vzrušený	k2eAgInSc4d1	vzrušený
rozhlasový	rozhlasový	k2eAgInSc4d1	rozhlasový
komentář	komentář	k1gInSc4	komentář
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
neštěstí	štěstit	k5eNaImIp3nP	štěstit
zcela	zcela	k6eAd1	zcela
zničily	zničit	k5eAaPmAgFnP	zničit
důvěru	důvěra	k1gFnSc4	důvěra
veřejnosti	veřejnost	k1gFnSc3	veřejnost
ve	v	k7c4	v
vzducholodě	vzducholoď	k1gFnPc4	vzducholoď
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
negativní	negativní	k2eAgFnSc1d1	negativní
publicita	publicita	k1gFnSc1	publicita
znamenala	znamenat	k5eAaImAgFnS	znamenat
konec	konec	k1gInSc4	konec
Zeppelinů	Zeppelin	k1gInPc2	Zeppelin
a	a	k8xC	a
dopravy	doprava	k1gFnSc2	doprava
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
ohromných	ohromný	k2eAgFnPc2d1	ohromná
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Sesterský	sesterský	k2eAgInSc4d1	sesterský
stroj	stroj	k1gInSc4	stroj
Hindenburgu	Hindenburg	k1gInSc2	Hindenburg
Graf	graf	k1gInSc1	graf
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
II	II	kA	II
létal	létat	k5eAaImAgInS	létat
ještě	ještě	k9	ještě
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
na	na	k7c4	na
výzvědné	výzvědný	k2eAgInPc4d1	výzvědný
a	a	k8xC	a
propagační	propagační	k2eAgInPc4d1	propagační
lety	let	k1gInPc4	let
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc4	ten
už	už	k6eAd1	už
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
labutí	labutí	k2eAgFnSc1d1	labutí
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
ukončená	ukončený	k2eAgFnSc1d1	ukončená
začátkem	začátkem	k7c2	začátkem
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
