<s>
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
Tolkien	Tolkien	k2eAgMnSc1d1	Tolkien
<g/>
,	,	kIx,	,
CBE	CBE	kA	CBE
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1892	[number]	k4	1892
Bloemfontein	Bloemfontein	k1gInSc1	Bloemfontein
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1973	[number]	k4	1973
Bournemouth	Bournemoutha	k1gFnPc2	Bournemoutha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
filolog	filolog	k1gMnSc1	filolog
a	a	k8xC	a
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgMnSc1d3	nejznámější
jako	jako	k8xS	jako
autor	autor	k1gMnSc1	autor
Hobita	hobit	k1gMnSc2	hobit
a	a	k8xC	a
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
letech	let	k1gInPc6	let
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
jako	jako	k8xS	jako
profesor	profesor	k1gMnSc1	profesor
staré	starý	k2eAgFnSc2d1	stará
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
pak	pak	k6eAd1	pak
jako	jako	k8xS	jako
profesor	profesor	k1gMnSc1	profesor
anglického	anglický	k2eAgInSc2d1	anglický
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
význačným	význačný	k2eAgMnSc7d1	význačný
jazykovědcem	jazykovědec	k1gMnSc7	jazykovědec
a	a	k8xC	a
znalcem	znalec	k1gMnSc7	znalec
staré	starý	k2eAgFnSc2d1	stará
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
staré	starý	k2eAgFnSc2d1	stará
severštiny	severština	k1gFnSc2	severština
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nejbližším	blízký	k2eAgMnSc7d3	nejbližší
přítelem	přítel	k1gMnSc7	přítel
C.	C.	kA	C.
S.	S.	kA	S.
Lewisem	Lewis	k1gInSc7	Lewis
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
literárního	literární	k2eAgInSc2d1	literární
diskusního	diskusní	k2eAgInSc2d1	diskusní
klubu	klub	k1gInSc2	klub
Inklings	Inklingsa	k1gFnPc2	Inklingsa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Tušitelé	Tušitel	k1gMnPc1	Tušitel
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Inkousťata	Inkoustě	k1gNnPc1	Inkoustě
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1972	[number]	k4	1972
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Hobita	hobit	k1gMnSc2	hobit
<g/>
,	,	kIx,	,
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
pojednání	pojednání	k1gNnPc2	pojednání
a	a	k8xC	a
překladů	překlad	k1gInPc2	překlad
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
Tolkienovo	Tolkienův	k2eAgNnSc1d1	Tolkienovo
dílo	dílo	k1gNnSc1	dílo
množství	množství	k1gNnSc1	množství
textů	text	k1gInPc2	text
z	z	k7c2	z
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
fiktivního	fiktivní	k2eAgInSc2d1	fiktivní
světa	svět	k1gInSc2	svět
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
Hobit	hobit	k1gMnSc1	hobit
a	a	k8xC	a
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Silmarillion	Silmarillion	k1gInSc4	Silmarillion
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
autorův	autorův	k2eAgMnSc1d1	autorův
syn	syn	k1gMnSc1	syn
Christopher	Christophra	k1gFnPc2	Christophra
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
tyto	tento	k3xDgInPc4	tento
příběhy	příběh	k1gInPc4	příběh
používal	používat	k5eAaImAgInS	používat
slovo	slovo	k1gNnSc4	slovo
legendárium	legendárium	k1gNnSc4	legendárium
<g/>
.	.	kIx.	.
</s>
<s>
Zbylá	zbylý	k2eAgNnPc1d1	zbylé
Tolkienova	Tolkienův	k2eAgNnPc1d1	Tolkienovo
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
pohádky	pohádka	k1gFnPc1	pohádka
a	a	k8xC	a
příběhy	příběh	k1gInPc1	příběh
původně	původně	k6eAd1	původně
vyprávěné	vyprávěný	k2eAgInPc1d1	vyprávěný
jeho	jeho	k3xOp3gFnPc3	jeho
dětem	dítě	k1gFnPc3	dítě
a	a	k8xC	a
nevztahují	vztahovat	k5eNaImIp3nP	vztahovat
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
ke	k	k7c3	k
Středozemi	Středozem	k1gFnSc3	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
neutuchající	utuchající	k2eNgFnSc4d1	neutuchající
popularitu	popularita	k1gFnSc4	popularita
a	a	k8xC	a
vliv	vliv	k1gInSc4	vliv
je	být	k5eAaImIp3nS	být
Tolkien	Tolkien	k1gInSc1	Tolkien
často	často	k6eAd1	často
považován	považován	k2eAgInSc1d1	považován
za	za	k7c2	za
nejdůležitějšího	důležitý	k2eAgInSc2d3	nejdůležitější
z	z	k7c2	z
otců	otec	k1gMnPc2	otec
žánru	žánr	k1gInSc2	žánr
moderní	moderní	k2eAgInPc1d1	moderní
hrdinské	hrdinský	k2eAgInPc1d1	hrdinský
fantasy	fantas	k1gInPc1	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1892	[number]	k4	1892
v	v	k7c6	v
Bloemfonteinu	Bloemfonteino	k1gNnSc6	Bloemfonteino
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Oranžského	oranžský	k2eAgInSc2d1	oranžský
svobodného	svobodný	k2eAgInSc2d1	svobodný
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
provincie	provincie	k1gFnPc4	provincie
Svobodný	svobodný	k2eAgInSc1d1	svobodný
stát	stát	k1gInSc1	stát
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arthuru	Arthur	k1gMnSc3	Arthur
Tolkienovi	Tolkien	k1gMnSc3	Tolkien
<g/>
,	,	kIx,	,
řediteli	ředitel	k1gMnSc3	ředitel
tamní	tamní	k2eAgFnSc2d1	tamní
bankovní	bankovní	k2eAgFnSc2d1	bankovní
pobočky	pobočka	k1gFnSc2	pobočka
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
ženě	žena	k1gFnSc3	žena
Mabel	Mabel	k1gInSc4	Mabel
Tolkienové	Tolkienová	k1gFnSc2	Tolkienová
(	(	kIx(	(
<g/>
rozené	rozený	k2eAgFnSc2d1	rozená
Suffieldové	Suffieldová	k1gFnSc2	Suffieldová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tolkienova	Tolkienův	k2eAgFnSc1d1	Tolkienova
rodina	rodina	k1gFnSc1	rodina
pocházela	pocházet	k5eAaImAgFnS	pocházet
ze	z	k7c2	z
Saska	Sasko	k1gNnSc2	Sasko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
už	už	k6eAd1	už
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Příjmení	příjmení	k1gNnSc1	příjmení
Tolkien	Tolkina	k1gFnPc2	Tolkina
je	být	k5eAaImIp3nS	být
poangličtěnou	poangličtěný	k2eAgFnSc7d1	poangličtěná
verzí	verze	k1gFnSc7	verze
Tollkiehn	Tollkiehn	k1gMnSc1	Tollkiehn
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
německy	německy	k6eAd1	německy
tollkühn	tollkühn	k1gNnSc1	tollkühn
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
šíleně	šíleně	k6eAd1	šíleně
odvážný	odvážný	k2eAgInSc1d1	odvážný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
profesora	profesor	k1gMnSc2	profesor
Rashbolda	Rashbold	k1gMnSc2	Rashbold
v	v	k7c6	v
The	The	k1gFnSc6	The
Notion	Notion	k1gInSc4	Notion
Club	club	k1gInSc1	club
Papers	Papers	k1gInSc1	Papers
(	(	kIx(	(
<g/>
příběh	příběh	k1gInSc1	příběh
z	z	k7c2	z
IX	IX	kA	IX
<g/>
.	.	kIx.	.
svazku	svazek	k1gInSc2	svazek
The	The	k1gFnSc2	The
History	Histor	k1gInPc4	Histor
of	of	k?	of
Middle-earth	Middlearth	k1gInSc1	Middle-earth
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vtipem	vtip	k1gInSc7	vtip
narážejícím	narážející	k2eAgInSc7d1	narážející
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
znamená	znamenat	k5eAaImIp3nS	znamenat
totéž	týž	k3xTgNnSc4	týž
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
křestních	křestní	k2eAgNnPc6d1	křestní
jménech	jméno	k1gNnPc6	jméno
jeho	jeho	k3xOp3gNnSc1	jeho
otec	otec	k1gMnSc1	otec
Arthur	Arthur	k1gMnSc1	Arthur
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1892	[number]	k4	1892
napsal	napsat	k5eAaBmAgMnS	napsat
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
Bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
první	první	k4xOgNnSc4	první
jméno	jméno	k1gNnSc4	jméno
'	'	kIx"	'
<g/>
John	John	k1gMnSc1	John
<g/>
'	'	kIx"	'
po	po	k7c6	po
dědečkovi	dědeček	k1gMnSc6	dědeček
<g/>
,	,	kIx,	,
dohromady	dohromady	k6eAd1	dohromady
asi	asi	k9	asi
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
.	.	kIx.	.
</s>
<s>
Mab	Mab	k?	Mab
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Ronald	Ronald	k1gMnSc1	Ronald
<g/>
,	,	kIx,	,
a	a	k8xC	a
já	já	k3xPp1nSc1	já
chci	chtít	k5eAaImIp1nS	chtít
zachovat	zachovat	k5eAaPmF	zachovat
Johna	John	k1gMnSc4	John
a	a	k8xC	a
Reuela	Reuel	k1gMnSc4	Reuel
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Tolkien	Tolkien	k1gInSc1	Tolkien
měl	mít	k5eAaImAgInS	mít
pouze	pouze	k6eAd1	pouze
jednoho	jeden	k4xCgMnSc4	jeden
sourozence	sourozenec	k1gMnSc4	sourozenec
<g/>
,	,	kIx,	,
bratra	bratr	k1gMnSc4	bratr
Hilaryho	Hilary	k1gMnSc4	Hilary
Arthura	Arthur	k1gMnSc4	Arthur
Reuela	Reuel	k1gMnSc4	Reuel
Tolkiena	Tolkien	k1gMnSc4	Tolkien
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
odcestoval	odcestovat	k5eAaPmAgInS	odcestovat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nemohla	moct	k5eNaImAgFnS	moct
přivyknout	přivyknout	k5eAaPmF	přivyknout
africkému	africký	k2eAgNnSc3d1	africké
podnebí	podnebí	k1gNnSc3	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
bydleli	bydlet	k5eAaImAgMnP	bydlet
u	u	k7c2	u
příbuzných	příbuzný	k1gMnPc2	příbuzný
na	na	k7c6	na
farmě	farma	k1gFnSc6	farma
Bag	Bag	k?	Bag
End	End	k1gFnSc2	End
v	v	k7c6	v
Worcestershire	Worcestershir	k1gMnSc5	Worcestershir
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
název	název	k1gInSc4	název
Dno	dno	k1gNnSc1	dno
pytle	pytel	k1gInSc2	pytel
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
knihách	kniha	k1gFnPc6	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
však	však	k9	však
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
revmatickou	revmatický	k2eAgFnSc7d1	revmatická
horečkou	horečka	k1gFnSc7	horečka
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
krvácení	krvácení	k1gNnSc4	krvácení
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
živitele	živitel	k1gMnSc2	živitel
rodiny	rodina	k1gFnSc2	rodina
přinutila	přinutit	k5eAaPmAgFnS	přinutit
Tolkienovu	Tolkienův	k2eAgFnSc4d1	Tolkienova
matku	matka	k1gFnSc4	matka
krátce	krátce	k6eAd1	krátce
žít	žít	k5eAaImF	žít
u	u	k7c2	u
jejích	její	k3xOp3gMnPc2	její
rodičů	rodič	k1gMnPc2	rodič
v	v	k7c6	v
Birminghamu	Birmingham	k1gInSc6	Birmingham
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
Sarehole	Sarehole	k1gFnSc2	Sarehole
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
součást	součást	k1gFnSc1	součást
Birminghamu	Birmingham	k1gInSc2	Birmingham
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
trávil	trávit	k5eAaImAgMnS	trávit
hodiny	hodina	k1gFnSc2	hodina
zkoumáním	zkoumání	k1gNnSc7	zkoumání
starého	starý	k2eAgInSc2d1	starý
vodního	vodní	k2eAgInSc2d1	vodní
mlýnu	mlýn	k1gInSc3	mlýn
v	v	k7c6	v
Sarehole	Sarehol	k1gInSc6	Sarehol
a	a	k8xC	a
močálu	močál	k1gInSc6	močál
v	v	k7c4	v
Moseley	Moselea	k1gFnPc4	Moselea
<g/>
.	.	kIx.	.
</s>
<s>
Zážitky	zážitek	k1gInPc1	zážitek
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
zdrojem	zdroj	k1gInSc7	zdroj
inspirace	inspirace	k1gFnSc2	inspirace
pro	pro	k7c4	pro
scenérie	scenérie	k1gFnPc4	scenérie
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
knihách	kniha	k1gFnPc6	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Mabel	Mabela	k1gFnPc2	Mabela
Tolkienová	Tolkienová	k1gFnSc1	Tolkienová
učila	učit	k5eAaImAgFnS	učit
své	svůj	k3xOyFgMnPc4	svůj
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
a	a	k8xC	a
Ronald	Ronald	k1gInSc4	Ronald
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
doma	doma	k6eAd1	doma
říkalo	říkat	k5eAaImAgNnS	říkat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dychtivým	dychtivý	k2eAgMnSc7d1	dychtivý
žákem	žák	k1gMnSc7	žák
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
znalosti	znalost	k1gFnPc4	znalost
botaniky	botanika	k1gFnSc2	botanika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejoblíbenějšími	oblíbený	k2eAgFnPc7d3	nejoblíbenější
hodinami	hodina	k1gFnPc7	hodina
byly	být	k5eAaImAgFnP	být
lekce	lekce	k1gFnPc1	lekce
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
ho	on	k3xPp3gInSc4	on
proto	proto	k6eAd1	proto
naučila	naučit	k5eAaPmAgFnS	naučit
velmi	velmi	k6eAd1	velmi
záhy	záhy	k6eAd1	záhy
základům	základ	k1gInPc3	základ
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
uměl	umět	k5eAaImAgMnS	umět
číst	číst	k5eAaImF	číst
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
i	i	k9	i
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
Školu	škola	k1gFnSc4	škola
svatého	svatý	k2eAgMnSc2d1	svatý
Filipa	Filip	k1gMnSc2	Filip
<g/>
,	,	kIx,	,
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
Školu	škola	k1gFnSc4	škola
krále	král	k1gMnSc2	král
Edwarda	Edward	k1gMnSc2	Edward
v	v	k7c6	v
Birminghamu	Birmingham	k1gInSc6	Birmingham
i	i	k8xC	i
Exeterskou	Exeterský	k2eAgFnSc4d1	Exeterská
kolej	kolej	k1gFnSc4	kolej
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
konvertovala	konvertovat	k5eAaBmAgFnS	konvertovat
matka	matka	k1gFnSc1	matka
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
navzdory	navzdory	k7c3	navzdory
bouřlivým	bouřlivý	k2eAgInPc3d1	bouřlivý
protestům	protest	k1gInPc3	protest
jejich	jejich	k3xOp3gFnSc2	jejich
převážně	převážně	k6eAd1	převážně
baptistické	baptistický	k2eAgFnSc2d1	baptistická
rodiny	rodina	k1gFnSc2	rodina
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Tolkienovi	Tolkienův	k2eAgMnPc1d1	Tolkienův
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
cukrovku	cukrovka	k1gFnSc4	cukrovka
<g/>
.	.	kIx.	.
</s>
<s>
Martyrium	martyrium	k1gNnSc1	martyrium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
podstoupila	podstoupit	k5eAaPmAgFnS	podstoupit
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
pociťoval	pociťovat	k5eAaImAgMnS	pociťovat
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
jako	jako	k8xC	jako
závazek	závazek	k1gInSc4	závazek
s	s	k7c7	s
hlubokým	hluboký	k2eAgInSc7d1	hluboký
vlivem	vliv	k1gInSc7	vliv
na	na	k7c4	na
vlastní	vlastní	k2eAgNnSc4d1	vlastní
katolictví	katolictví	k1gNnSc4	katolictví
<g/>
.	.	kIx.	.
</s>
<s>
Tolkienův	Tolkienův	k2eAgInSc1d1	Tolkienův
zápal	zápal	k1gInSc1	zápal
pro	pro	k7c4	pro
víru	víra	k1gFnSc4	víra
lze	lze	k6eAd1	lze
doložit	doložit	k5eAaPmF	doložit
dopisy	dopis	k1gInPc1	dopis
C.	C.	kA	C.
S.	S.	kA	S.
Lewisovi	Lewis	k1gMnSc3	Lewis
během	během	k7c2	během
jeho	jeho	k3xOp3gNnSc2	jeho
obrácení	obrácení	k1gNnSc2	obrácení
na	na	k7c4	na
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
své	svůj	k3xOyFgFnPc4	svůj
křesťanské	křesťanský	k2eAgFnPc4d1	křesťanská
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
sirotek	sirotek	k1gMnSc1	sirotek
byl	být	k5eAaImAgMnS	být
vychováván	vychovávat	k5eAaImNgMnS	vychovávat
knězem	kněz	k1gMnSc7	kněz
Francisem	Francis	k1gInSc7	Francis
Xavierem	Xavier	k1gMnSc7	Xavier
Morganem	morgan	k1gMnSc7	morgan
z	z	k7c2	z
birminghamské	birminghamský	k2eAgFnSc2d1	birminghamská
Oratoře	oratoř	k1gFnSc2	oratoř
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Edgbaston	Edgbaston	k1gInSc1	Edgbaston
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
matka	matka	k1gFnSc1	matka
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
svěřila	svěřit	k5eAaPmAgFnS	svěřit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
dobrou	dobrý	k2eAgFnSc4d1	dobrá
katolickou	katolický	k2eAgFnSc4d1	katolická
výchovu	výchova	k1gFnSc4	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgFnSc3d1	zdejší
architektuře	architektura	k1gFnSc3	architektura
dominovala	dominovat	k5eAaImAgFnS	dominovat
29	[number]	k4	29
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
budova	budova	k1gFnSc1	budova
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
Perrott	Perrott	k1gInSc1	Perrott
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Folly	Foll	k1gMnPc7	Foll
a	a	k8xC	a
blízko	blízko	k6eAd1	blízko
ní	on	k3xPp3gFnSc3	on
stojící	stojící	k2eAgFnSc1d1	stojící
pozdější	pozdní	k2eAgFnSc1d2	pozdější
viktoriánská	viktoriánský	k2eAgFnSc1d1	viktoriánská
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
edgbastonské	edgbastonský	k2eAgFnSc2d1	edgbastonský
vodárny	vodárna	k1gFnSc2	vodárna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
představy	představa	k1gFnPc1	představa
různých	různý	k2eAgFnPc2d1	různá
temných	temný	k2eAgFnPc2d1	temná
věží	věž	k1gFnPc2	věž
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
pracích	práce	k1gFnPc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
16	[number]	k4	16
<g/>
,	,	kIx,	,
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Edith	Editha	k1gFnPc2	Editha
Brattové	Brattová	k1gFnSc2	Brattová
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
předlohou	předloha	k1gFnSc7	předloha
pro	pro	k7c4	pro
postavu	postava	k1gFnSc4	postava
Lúthien	Lúthina	k1gFnPc2	Lúthina
<g/>
)	)	kIx)	)
a	a	k8xC	a
navzdory	navzdory	k7c3	navzdory
mnoha	mnoho	k4c2	mnoho
překážkám	překážka	k1gFnPc3	překážka
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jeho	jeho	k3xOp3gFnSc4	jeho
jedinou	jediný	k2eAgFnSc7d1	jediná
láskou	láska	k1gFnSc7	láska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Oxfordské	oxfordský	k2eAgFnSc6d1	Oxfordská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1913	[number]	k4	1913
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
zkoušky	zkouška	k1gFnSc2	zkouška
Honour	Honoura	k1gFnPc2	Honoura
Moderations	Moderations	k1gInSc1	Moderations
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
sice	sice	k8xC	sice
jen	jen	k9	jen
hodnocení	hodnocení	k1gNnSc4	hodnocení
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
ze	z	k7c2	z
srovnávací	srovnávací	k2eAgFnSc2d1	srovnávací
filologie	filologie	k1gFnSc2	filologie
byla	být	k5eAaImAgFnS	být
ohodnocena	ohodnocen	k2eAgFnSc1d1	ohodnocena
jako	jako	k8xS	jako
bezchybná	bezchybný	k2eAgFnSc1d1	bezchybná
<g/>
.	.	kIx.	.
</s>
<s>
Přešel	přejít	k5eAaPmAgMnS	přejít
tedy	tedy	k9	tedy
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
anglistiky	anglistika	k1gFnSc2	anglistika
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
akademického	akademický	k2eAgInSc2d1	akademický
titulu	titul	k1gInSc2	titul
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1916	[number]	k4	1916
se	se	k3xPyFc4	se
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
Tolkien	Tolkien	k1gInSc4	Tolkien
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Edith	Edith	k1gInSc1	Edith
Brattovou	Brattová	k1gFnSc4	Brattová
<g/>
.	.	kIx.	.
</s>
<s>
Zuřila	zuřit	k5eAaImAgFnS	zuřit
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
Tolkien	Tolkien	k1gInSc1	Tolkien
byl	být	k5eAaImAgInS	být
povolán	povolat	k5eAaPmNgInS	povolat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
k	k	k7c3	k
Lancashirským	Lancashirský	k2eAgMnPc3d1	Lancashirský
střelcům	střelec	k1gMnPc3	střelec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
podporučík	podporučík	k1gMnSc1	podporučík
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
jeho	jeho	k3xOp3gMnPc2	jeho
spolubojovníků	spolubojovník	k1gMnPc2	spolubojovník
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
blízkých	blízký	k2eAgMnPc2d1	blízký
přátel	přítel	k1gMnPc2	přítel
padlo	padnout	k5eAaPmAgNnS	padnout
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Sommy	Somma	k1gFnSc2	Somma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1916	[number]	k4	1916
nakazil	nakazit	k5eAaPmAgInS	nakazit
zákopovou	zákopový	k2eAgFnSc7d1	zákopová
horečkou	horečka	k1gFnSc7	horečka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
repatriován	repatriovat	k5eAaBmNgMnS	repatriovat
a	a	k8xC	a
strávil	strávit	k5eAaPmAgMnS	strávit
pak	pak	k6eAd1	pak
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zotavování	zotavování	k1gNnSc2	zotavování
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
Knize	kniha	k1gFnSc6	kniha
ztracených	ztracený	k2eAgInPc2d1	ztracený
příběhů	příběh	k1gInPc2	příběh
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Book	Book	k1gMnSc1	Book
of	of	k?	of
Lost	Lost	k1gMnSc1	Lost
Tales	Tales	k1gMnSc1	Tales
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sérii	série	k1gFnSc4	série
pohádkových	pohádkový	k2eAgInPc2d1	pohádkový
příběhů	příběh	k1gInPc2	příběh
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
milované	milovaný	k2eAgFnSc6d1	milovaná
mytologii	mytologie	k1gFnSc6	mytologie
a	a	k8xC	a
folklóru	folklór	k1gInSc6	folklór
<g/>
.	.	kIx.	.
</s>
<s>
Bojů	boj	k1gInPc2	boj
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
otřesené	otřesený	k2eAgNnSc4d1	otřesené
zdraví	zdraví	k1gNnSc4	zdraví
již	již	k6eAd1	již
neúčastnil	účastnit	k5eNaImAgInS	účastnit
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
a	a	k8xC	a
1918	[number]	k4	1918
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
důstojník	důstojník	k1gMnSc1	důstojník
v	v	k7c6	v
zázemí	zázemí	k1gNnSc6	zázemí
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
poručíka	poručík	k1gMnSc4	poručík
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
pracoval	pracovat	k5eAaImAgMnS	pracovat
Tolkien	Tolkien	k1gInSc4	Tolkien
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
Novoanglickém	Novoanglický	k2eAgInSc6d1	Novoanglický
slovníku	slovník	k1gInSc6	slovník
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
English	Englisha	k1gFnPc2	Englisha
Dictionary	Dictionara	k1gFnSc2	Dictionara
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
Oxford	Oxford	k1gInSc1	Oxford
English	Englisha	k1gFnPc2	Englisha
Dictionary	Dictionara	k1gFnSc2	Dictionara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Leedsu	Leeds	k1gInSc2	Leeds
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
katedře	katedra	k1gFnSc6	katedra
angličtiny	angličtina	k1gFnSc2	angličtina
tamní	tamní	k2eAgFnSc2d1	tamní
univerzity	univerzita	k1gFnSc2	univerzita
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
titulu	titul	k1gInSc2	titul
profesora	profesor	k1gMnSc2	profesor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Oxfordu	Oxford	k1gInSc2	Oxford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
anglosaštiny	anglosaština	k1gFnSc2	anglosaština
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
na	na	k7c6	na
Mertonově	Mertonův	k2eAgFnSc6d1	Mertonova
koleji	kolej	k1gFnSc6	kolej
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
anglického	anglický	k2eAgInSc2d1	anglický
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
zde	zde	k6eAd1	zde
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
odchodu	odchod	k1gInSc2	odchod
do	do	k7c2	do
penze	penze	k1gFnSc2	penze
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgMnS	mít
více	hodně	k6eAd2	hodně
klidu	klid	k1gInSc2	klid
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
již	již	k6eAd1	již
stával	stávat	k5eAaImAgInS	stávat
slavným	slavný	k2eAgMnSc7d1	slavný
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
,	,	kIx,	,
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Bournemouthu	Bournemouth	k1gInSc2	Bournemouth
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc4	Tolkien
a	a	k8xC	a
Edith	Edith	k1gInSc4	Edith
sdíleli	sdílet	k5eAaImAgMnP	sdílet
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
rodině	rodina	k1gFnSc3	rodina
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
John	John	k1gMnSc1	John
Francis	Francis	k1gFnSc2	Francis
Reuel	Reuel	k1gMnSc1	Reuel
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Hilary	Hilara	k1gFnSc2	Hilara
Reuel	Reuel	k1gMnSc1	Reuel
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
John	John	k1gMnSc1	John
Reuel	Reuel	k1gMnSc1	Reuel
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
a	a	k8xC	a
Priscilla	Priscilla	k1gMnSc1	Priscilla
Anne	Ann	k1gFnSc2	Ann
Reuel	Reuel	k1gMnSc1	Reuel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
otec	otec	k1gMnSc1	otec
nesmírně	smírně	k6eNd1	smírně
laskavý	laskavý	k2eAgMnSc1d1	laskavý
a	a	k8xC	a
chápavý	chápavý	k2eAgMnSc1d1	chápavý
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
projevy	projev	k1gInPc7	projev
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
za	za	k7c4	za
něž	jenž	k3xRgFnPc4	jenž
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nestyděl	stydět	k5eNaImAgInS	stydět
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
pohádkové	pohádkový	k2eAgInPc4d1	pohádkový
příběhy	příběh	k1gInPc4	příběh
s	s	k7c7	s
ilustracemi	ilustrace	k1gFnPc7	ilustrace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
malého	malý	k2eAgMnSc4d1	malý
psíka	psík	k1gMnSc4	psík
Rovera	rover	k1gMnSc4	rover
<g/>
,	,	kIx,	,
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
Toma	Tom	k1gMnSc2	Tom
Bombadila	Bombadila	k1gFnSc2	Bombadila
<g/>
,	,	kIx,	,
historky	historka	k1gFnPc4	historka
o	o	k7c6	o
nezlomném	zlomný	k2eNgMnSc6d1	nezlomný
darebákovi	darebák	k1gMnSc6	darebák
"	"	kIx"	"
<g/>
Lepiči	lepič	k1gMnPc1	lepič
plakátů	plakát	k1gInPc2	plakát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vznešenější	vznešený	k2eAgMnSc1d2	vznešenější
Sedlák	Sedlák	k1gMnSc1	Sedlák
Jiljí	Jiljí	k1gMnSc1	Jiljí
z	z	k7c2	z
Oujezda	Oujezdo	k1gNnSc2	Oujezdo
a	a	k8xC	a
především	především	k9	především
každoroční	každoroční	k2eAgInPc1d1	každoroční
dopisy	dopis	k1gInPc1	dopis
od	od	k7c2	od
vánočního	vánoční	k2eAgMnSc2d1	vánoční
dědečka	dědeček	k1gMnSc2	dědeček
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
Edith	Edith	k1gInSc1	Edith
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1971	[number]	k4	1971
se	se	k3xPyFc4	se
Tolkien	Tolkien	k1gInSc1	Tolkien
odstěhoval	odstěhovat	k5eAaPmAgInS	odstěhovat
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Merton	Merton	k1gInSc4	Merton
Street	Street	k1gInSc1	Street
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1972	[number]	k4	1972
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
a	a	k8xC	a
Oxfordská	oxfordský	k2eAgFnSc1d1	Oxfordská
univerzita	univerzita	k1gFnSc1	univerzita
mu	on	k3xPp3gMnSc3	on
udělila	udělit	k5eAaPmAgFnS	udělit
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
z	z	k7c2	z
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1973	[number]	k4	1973
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
Tolkien	Tolkien	k2eAgMnSc1d1	Tolkien
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
81	[number]	k4	81
let	léto	k1gNnPc2	léto
zemřel	zemřít	k5eAaPmAgInS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náhrobku	náhrobek	k1gInSc6	náhrobek
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Wolvercote	Wolvercot	k1gMnSc5	Wolvercot
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
on	on	k3xPp3gMnSc1	on
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
jejich	jejich	k3xOp3gNnPc2	jejich
jmen	jméno	k1gNnPc2	jméno
vytesána	vytesán	k2eAgFnSc1d1	vytesána
i	i	k8xC	i
jména	jméno	k1gNnPc4	jméno
Berena	Bereno	k1gNnSc2	Bereno
a	a	k8xC	a
Lúthien	Lúthina	k1gFnPc2	Lúthina
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
rád	rád	k6eAd1	rád
vymýšlel	vymýšlet	k5eAaImAgInS	vymýšlet
fantastické	fantastický	k2eAgInPc4d1	fantastický
příběhy	příběh	k1gInPc4	příběh
pro	pro	k7c4	pro
pobavení	pobavení	k1gNnSc4	pobavení
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgFnPc1	každý
Vánoce	Vánoce	k1gFnPc1	Vánoce
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
psal	psát	k5eAaImAgMnS	psát
dopisy	dopis	k1gInPc1	dopis
typickým	typický	k2eAgNnSc7d1	typické
třaslavým	třaslavý	k2eAgNnSc7d1	třaslavé
písmem	písmo	k1gNnSc7	písmo
vánočního	vánoční	k2eAgMnSc2d1	vánoční
dědečka	dědeček	k1gMnSc2	dědeček
(	(	kIx(	(
<g/>
Father	Fathra	k1gFnPc2	Fathra
Christmas	Christmas	k1gInSc1	Christmas
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
S.	S.	kA	S.
Pošustové	Pošust	k1gMnPc1	Pošust
"	"	kIx"	"
<g/>
Děda	děda	k1gMnSc1	děda
Mráz	Mráz	k1gMnSc1	Mráz
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
tak	tak	k6eAd1	tak
sérii	série	k1gFnSc4	série
krátkých	krátký	k2eAgInPc2d1	krátký
příběhů	příběh	k1gInPc2	příběh
–	–	k?	–
později	pozdě	k6eAd2	pozdě
sebranou	sebraný	k2eAgFnSc4d1	sebraná
a	a	k8xC	a
publikovanou	publikovaný	k2eAgFnSc4d1	publikovaná
jako	jako	k8xS	jako
Dopisy	dopis	k1gInPc1	dopis
Dědy	děda	k1gMnSc2	děda
Mráze	Mráz	k1gMnSc2	Mráz
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Father	Fathra	k1gFnPc2	Fathra
Christmas	Christmasa	k1gFnPc2	Christmasa
Letters	Letters	k1gInSc1	Letters
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
nikdy	nikdy	k6eAd1	nikdy
neočekával	očekávat	k5eNaImAgInS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInPc4	jeho
vymyšlené	vymyšlený	k2eAgInPc4d1	vymyšlený
příběhy	příběh	k1gInPc4	příběh
budou	být	k5eAaImBp3nP	být
zajímat	zajímat	k5eAaImF	zajímat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jen	jen	k6eAd1	jen
úzkou	úzký	k2eAgFnSc4d1	úzká
skupinu	skupina	k1gFnSc4	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
naléhání	naléhání	k1gNnSc4	naléhání
bývalého	bývalý	k2eAgMnSc2d1	bývalý
studenta	student	k1gMnSc2	student
publikoval	publikovat	k5eAaBmAgMnS	publikovat
knihu	kniha	k1gFnSc4	kniha
Hobit	hobit	k1gMnSc1	hobit
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Hobbit	Hobbit	k1gMnSc1	Hobbit
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
původně	původně	k6eAd1	původně
napsal	napsat	k5eAaPmAgInS	napsat
jen	jen	k9	jen
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
také	také	k6eAd1	také
dospělé	dospělí	k1gMnPc4	dospělí
čtenáře	čtenář	k1gMnSc4	čtenář
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
natolik	natolik	k6eAd1	natolik
populární	populární	k2eAgFnPc1d1	populární
<g/>
,	,	kIx,	,
že	že	k8xS	že
nakladatel	nakladatel	k1gMnSc1	nakladatel
<g/>
,	,	kIx,	,
Allen	Allen	k1gMnSc1	Allen
&	&	k?	&
Unwin	Unwin	k1gMnSc1	Unwin
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
Tolkiena	Tolkien	k1gMnSc4	Tolkien
o	o	k7c4	o
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ho	on	k3xPp3gMnSc4	on
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
jeho	jeho	k3xOp3gNnSc2	jeho
nejznámějšího	známý	k2eAgNnSc2d3	nejznámější
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
výpravný	výpravný	k2eAgInSc1d1	výpravný
román	román	k1gInSc1	román
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Lord	lord	k1gMnSc1	lord
of	of	k?	of
the	the	k?	the
Rings	Ringsa	k1gFnPc2	Ringsa
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
55	[number]	k4	55
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyšel	vyjít	k5eAaPmAgMnS	vyjít
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
svazcích	svazek	k1gInPc6	svazek
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
bývá	bývat	k5eAaImIp3nS	bývat
nepřesně	přesně	k6eNd1	přesně
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
trilogie	trilogie	k1gFnSc1	trilogie
–	–	k?	–
Tolkien	Tolkien	k1gInSc1	Tolkien
chtěl	chtít	k5eAaImAgMnS	chtít
Pána	pán	k1gMnSc4	pán
prstenů	prsten	k1gInPc2	prsten
vydat	vydat	k5eAaPmF	vydat
jako	jako	k9	jako
jediný	jediný	k2eAgInSc4d1	jediný
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
sehnat	sehnat	k5eAaPmF	sehnat
papír	papír	k1gInSc4	papír
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
přes	přes	k7c4	přes
tisíc	tisíc	k4xCgInSc4	tisíc
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
přistoupit	přistoupit	k5eAaPmF	přistoupit
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
vydavatele	vydavatel	k1gMnSc4	vydavatel
vydat	vydat	k5eAaPmF	vydat
román	román	k1gInSc4	román
postupně	postupně	k6eAd1	postupně
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přineslo	přinést	k5eAaPmAgNnS	přinést
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
nazvat	nazvat	k5eAaBmF	nazvat
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
díly	díl	k1gInPc1	díl
–	–	k?	–
u	u	k7c2	u
názvu	název	k1gInSc2	název
Dvě	dva	k4xCgFnPc1	dva
věže	věž	k1gFnSc2	věž
autor	autor	k1gMnSc1	autor
nikdy	nikdy	k6eAd1	nikdy
nesdělil	sdělit	k5eNaPmAgMnS	sdělit
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc2	který
věže	věž	k1gFnSc2	věž
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
<g/>
,	,	kIx,	,
s	s	k7c7	s
názvem	název	k1gInSc7	název
Návrat	návrat	k1gInSc1	návrat
krále	král	k1gMnSc4	král
také	také	k6eAd1	také
moc	moc	k6eAd1	moc
nesouhlasil	souhlasit	k5eNaImAgInS	souhlasit
–	–	k?	–
podle	podle	k7c2	podle
jeho	on	k3xPp3gNnSc2	on
mínění	mínění	k1gNnSc2	mínění
příliš	příliš	k6eAd1	příliš
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
kniha	kniha	k1gFnSc1	kniha
vyvrcholí	vyvrcholit	k5eAaPmIp3nS	vyvrcholit
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
románu	román	k1gInSc2	román
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
téměř	téměř	k6eAd1	téměř
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgMnPc2	jenž
ho	on	k3xPp3gNnSc4	on
povzbuzovali	povzbuzovat	k5eAaImAgMnP	povzbuzovat
členové	člen	k1gMnPc1	člen
jeho	on	k3xPp3gInSc2	on
klubu	klub	k1gInSc2	klub
Inklings	Inklingsa	k1gFnPc2	Inklingsa
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
přítel	přítel	k1gMnSc1	přítel
C.	C.	kA	C.
S.	S.	kA	S.
Lewis	Lewis	k1gInSc1	Lewis
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
knih	kniha	k1gFnPc2	kniha
o	o	k7c4	o
Narnii	Narnie	k1gFnSc4	Narnie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
stal	stát	k5eAaPmAgInS	stát
ohromně	ohromně	k6eAd1	ohromně
populárním	populární	k2eAgInSc7d1	populární
zvláště	zvláště	k9	zvláště
mezi	mezi	k7c4	mezi
studenty	student	k1gMnPc4	student
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jej	on	k3xPp3gInSc4	on
nezmenšující	zmenšující	k2eNgInSc4d1	zmenšující
se	se	k3xPyFc4	se
okruh	okruh	k1gInSc4	okruh
obdivovatelů	obdivovatel	k1gMnPc2	obdivovatel
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nejpopulárnější	populární	k2eAgInPc4d3	nejpopulárnější
romány	román	k1gInPc4	román
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
podle	podle	k7c2	podle
odhadovaného	odhadovaný	k2eAgNnSc2d1	odhadované
množství	množství	k1gNnSc2	množství
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
prodaných	prodaný	k2eAgInPc2d1	prodaný
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
byl	být	k5eAaImAgInS	být
čtenáři	čtenář	k1gMnPc7	čtenář
na	na	k7c6	na
britském	britský	k2eAgNnSc6d1	Britské
TV	TV	kA	TV
kanálu	kanál	k1gInSc2	kanál
4	[number]	k4	4
a	a	k8xC	a
v	v	k7c6	v
řetězci	řetězec	k1gInSc6	řetězec
knihkupectví	knihkupectví	k1gNnSc2	knihkupectví
firmy	firma	k1gFnSc2	firma
Waterstone	Waterston	k1gInSc5	Waterston
zvolen	zvolit	k5eAaPmNgMnS	zvolit
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
knihou	kniha	k1gFnSc7	kniha
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
zákazníci	zákazník	k1gMnPc1	zákazník
internetového	internetový	k2eAgNnSc2d1	internetové
knihkupectví	knihkupectví	k1gNnSc2	knihkupectví
Amazon	amazona	k1gFnPc2	amazona
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
v	v	k7c6	v
hlasování	hlasování	k1gNnSc6	hlasování
vybrali	vybrat	k5eAaPmAgMnP	vybrat
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
jako	jako	k8xC	jako
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
knihu	kniha	k1gFnSc4	kniha
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
začínal	začínat	k5eAaImAgMnS	začínat
psát	psát	k5eAaImF	psát
první	první	k4xOgFnSc4	první
kapitolu	kapitola	k1gFnSc4	kapitola
Pána	pán	k1gMnSc4	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
domníval	domnívat	k5eAaImAgMnS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
vyprávět	vyprávět	k5eAaImF	vyprávět
pohádku	pohádka	k1gFnSc4	pohádka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
Hobitovi	hobit	k1gMnSc6	hobit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
příběh	příběh	k1gInSc1	příběh
stále	stále	k6eAd1	stále
temněl	temnět	k5eAaImAgInS	temnět
a	a	k8xC	a
stával	stávat	k5eAaImAgInS	stávat
se	se	k3xPyFc4	se
vážnějším	vážní	k2eAgMnSc7d2	vážnější
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
pokračováním	pokračování	k1gNnSc7	pokračování
Hobita	hobit	k1gMnSc2	hobit
<g/>
,	,	kIx,	,
obrací	obracet	k5eAaImIp3nS	obracet
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
na	na	k7c4	na
starší	starý	k2eAgNnSc4d2	starší
publikum	publikum	k1gNnSc4	publikum
<g/>
,	,	kIx,	,
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
hlavního	hlavní	k2eAgInSc2d1	hlavní
námětu	námět	k1gInSc2	námět
rozehrává	rozehrávat	k5eAaImIp3nS	rozehrávat
útržky	útržka	k1gFnPc4	útržka
dalších	další	k2eAgInPc2d1	další
příběhů	příběh	k1gInPc2	příběh
a	a	k8xC	a
vykresluje	vykreslovat	k5eAaImIp3nS	vykreslovat
tak	tak	k6eAd1	tak
fascinující	fascinující	k2eAgFnSc4d1	fascinující
atmosféru	atmosféra	k1gFnSc4	atmosféra
neodhalených	odhalený	k2eNgNnPc2d1	neodhalené
tajemství	tajemství	k1gNnPc2	tajemství
starobylé	starobylý	k2eAgFnSc2d1	starobylá
historie	historie	k1gFnSc2	historie
světa	svět	k1gInSc2	svět
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Mytologie	mytologie	k1gFnSc1	mytologie
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
rozvinuta	rozvinut	k2eAgFnSc1d1	rozvinuta
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
z	z	k7c2	z
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
vydal	vydat	k5eAaPmAgMnS	vydat
autorův	autorův	k2eAgMnSc1d1	autorův
syn	syn	k1gMnSc1	syn
Christopher	Christophra	k1gFnPc2	Christophra
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
výrazně	výrazně	k6eAd1	výrazně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
žánr	žánr	k1gInSc4	žánr
fantasy	fantas	k1gInPc7	fantas
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
obliba	obliba	k1gFnSc1	obliba
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
významně	významně	k6eAd1	významně
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
byl	být	k5eAaImAgMnS	být
profesionálním	profesionální	k2eAgMnSc7d1	profesionální
lingvistou	lingvista	k1gMnSc7	lingvista
a	a	k8xC	a
jazyky	jazyk	k1gInPc4	jazyk
a	a	k8xC	a
mytologie	mytologie	k1gFnPc4	mytologie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
studoval	studovat	k5eAaImAgMnS	studovat
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
ovlivňovaly	ovlivňovat	k5eAaImAgFnP	ovlivňovat
jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
trpasličí	trpasličí	k2eAgNnSc1d1	trpasličí
jména	jméno	k1gNnPc1	jméno
v	v	k7c6	v
Hobitovi	hobit	k1gMnSc3	hobit
byla	být	k5eAaImAgFnS	být
převzata	převzít	k5eAaPmNgFnS	převzít
ze	z	k7c2	z
severské	severský	k2eAgFnSc2d1	severská
mytologie	mytologie	k1gFnSc2	mytologie
z	z	k7c2	z
básně	báseň	k1gFnSc2	báseň
Völuspá	Völuspý	k2eAgFnSc1d1	Völuspá
(	(	kIx(	(
<g/>
Vědmina	vědmin	k2eAgFnSc1d1	Vědmina
věštba	věštba	k1gFnSc1	věštba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
části	část	k1gFnPc1	část
eposu	epos	k1gInSc2	epos
Edda	Edda	k1gFnSc1	Edda
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
některé	některý	k3yIgFnSc2	některý
zápletky	zápletka	k1gFnSc2	zápletka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zloděje	zloděj	k1gMnSc2	zloděj
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ukradne	ukradnout	k5eAaPmIp3nS	ukradnout
pohár	pohár	k1gInSc4	pohár
z	z	k7c2	z
dračího	dračí	k2eAgInSc2d1	dračí
pokladu	poklad	k1gInSc2	poklad
<g/>
)	)	kIx)	)
převzal	převzít	k5eAaPmAgInS	převzít
ze	z	k7c2	z
staroanglické	staroanglický	k2eAgFnSc2d1	staroanglická
epické	epický	k2eAgFnSc2d1	epická
básně	báseň	k1gFnSc2	báseň
Beowulf	Beowulf	k1gMnSc1	Beowulf
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
byl	být	k5eAaImAgMnS	být
uznávaným	uznávaný	k2eAgMnSc7d1	uznávaný
znalcem	znalec	k1gMnSc7	znalec
Beowulfa	Beowulf	k1gMnSc2	Beowulf
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
několik	několik	k4yIc4	několik
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
posmrtně	posmrtně	k6eAd1	posmrtně
byl	být	k5eAaImAgInS	být
uveřejněn	uveřejněn	k2eAgInSc1d1	uveřejněn
i	i	k8xC	i
jeho	jeho	k3xOp3gInSc1	jeho
překlad	překlad	k1gInSc1	překlad
této	tento	k3xDgFnSc2	tento
básně	báseň	k1gFnSc2	báseň
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
pracoval	pracovat	k5eAaImAgInS	pracovat
na	na	k7c6	na
historii	historie	k1gFnSc6	historie
Středozemě	Středozem	k1gFnSc2	Středozem
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Christopher	Christophra	k1gFnPc2	Christophra
Tolkien	Tolkien	k1gInSc4	Tolkien
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
spisovatele	spisovatel	k1gMnSc2	spisovatel
fantasy	fantas	k1gInPc7	fantas
Guye	Guy	k1gMnSc2	Guy
Gavriela	Gavriel	k1gMnSc2	Gavriel
Kaye	Kay	k1gMnSc2	Kay
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
některé	některý	k3yIgInPc4	některý
materiály	materiál	k1gInPc4	materiál
do	do	k7c2	do
ucelené	ucelený	k2eAgFnSc2d1	ucelená
podoby	podoba	k1gFnSc2	podoba
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
Tolkien	Tolkino	k1gNnPc2	Tolkino
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c4	v
publikování	publikování	k1gNnSc4	publikování
materiálu	materiál	k1gInSc2	materiál
použitého	použitý	k2eAgInSc2d1	použitý
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Tituly	titul	k1gInPc1	titul
z	z	k7c2	z
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
jako	jako	k8xS	jako
Historie	historie	k1gFnSc1	historie
Středozemě	Středozem	k1gFnSc2	Středozem
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
History	Histor	k1gInPc1	Histor
of	of	k?	of
Middle-earth	Middlearth	k1gInSc1	Middle-earth
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Nedokončené	dokončený	k2eNgInPc4d1	nedokončený
příběhy	příběh	k1gInPc4	příběh
(	(	kIx(	(
<g/>
Unfinished	Unfinished	k1gMnSc1	Unfinished
Tales	Tales	k1gMnSc1	Tales
<g/>
)	)	kIx)	)
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
nedokončené	dokončený	k2eNgInPc4d1	nedokončený
<g/>
,	,	kIx,	,
opuštěné	opuštěný	k2eAgInPc4d1	opuštěný
<g/>
,	,	kIx,	,
alternativní	alternativní	k2eAgInPc4d1	alternativní
nebo	nebo	k8xC	nebo
odporující	odporující	k2eAgMnPc1d1	odporující
si	se	k3xPyFc3	se
verze	verze	k1gFnSc2	verze
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Tolkien	Tolkien	k1gInSc1	Tolkien
pracoval	pracovat	k5eAaImAgInS	pracovat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
mytologii	mytologie	k1gFnSc6	mytologie
po	po	k7c6	po
desetiletí	desetiletí	k1gNnSc6	desetiletí
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
příběhy	příběh	k1gInPc4	příběh
přepisoval	přepisovat	k5eAaImAgInS	přepisovat
<g/>
,	,	kIx,	,
upravoval	upravovat	k5eAaImAgInS	upravovat
a	a	k8xC	a
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
udržet	udržet	k5eAaPmF	udržet
opravdovou	opravdový	k2eAgFnSc4d1	opravdová
konzistenci	konzistence	k1gFnSc4	konzistence
s	s	k7c7	s
Pánem	pán	k1gMnSc7	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
díky	díky	k7c3	díky
rozsáhlým	rozsáhlý	k2eAgFnPc3d1	rozsáhlá
úpravám	úprava	k1gFnPc3	úprava
Christophera	Christophero	k1gNnSc2	Christophero
Tolkiena	Tolkiena	k1gFnSc1	Tolkiena
–	–	k?	–
přestože	přestože	k8xS	přestože
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
ještě	ještě	k9	ještě
mnoho	mnoho	k4c1	mnoho
nesrovnalostí	nesrovnalost	k1gFnPc2	nesrovnalost
zbylo	zbýt	k5eAaPmAgNnS	zbýt
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
Hobit	hobit	k1gMnSc1	hobit
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
Pánem	pán	k1gMnSc7	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
při	při	k7c6	při
druhém	druhý	k4xOgNnSc6	druhý
vydání	vydání	k1gNnSc6	vydání
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
kapitola	kapitola	k1gFnSc1	kapitola
podstatně	podstatně	k6eAd1	podstatně
změněna	změnit	k5eAaPmNgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
Marquette	Marquett	k1gInSc5	Marquett
University	universita	k1gFnSc2	universita
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
Milwaukee	Milwaukee	k1gNnSc6	Milwaukee
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c1	mnoho
originálů	originál	k1gMnPc2	originál
Tolkienových	Tolkienův	k2eAgInPc2d1	Tolkienův
rukopisů	rukopis	k1gInPc2	rukopis
<g/>
,	,	kIx,	,
poznámek	poznámka	k1gFnPc2	poznámka
a	a	k8xC	a
dopisů	dopis	k1gInPc2	dopis
<g/>
;	;	kIx,	;
jiné	jiný	k2eAgInPc4d1	jiný
původní	původní	k2eAgInPc4d1	původní
materiály	materiál	k1gInPc4	materiál
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
oxfordské	oxfordský	k2eAgFnSc6d1	Oxfordská
Bodleyově	Bodleyův	k2eAgFnSc6d1	Bodleyův
knihovně	knihovna	k1gFnSc6	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Marquette	Marquette	k5eAaPmIp2nP	Marquette
University	universita	k1gFnPc1	universita
vlastní	vlastní	k2eAgInPc4d1	vlastní
rukopisy	rukopis	k1gInPc4	rukopis
a	a	k8xC	a
korektury	korektura	k1gFnSc2	korektura
Pána	pán	k1gMnSc4	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
Hobita	hobit	k1gMnSc2	hobit
<g/>
,	,	kIx,	,
rukopisy	rukopis	k1gInPc1	rukopis
mnoha	mnoho	k4c2	mnoho
menších	malý	k2eAgNnPc2d2	menší
děl	dělo	k1gNnPc2	dělo
jako	jako	k8xS	jako
Sedlák	Sedlák	k1gMnSc1	Sedlák
Jiljí	Jiljí	k1gMnSc1	Jiljí
z	z	k7c2	z
Oujezda	Oujezda	k1gMnSc1	Oujezda
(	(	kIx(	(
<g/>
Farmer	Farmer	k1gMnSc1	Farmer
Giles	Giles	k1gMnSc1	Giles
of	of	k?	of
Ham	ham	k0	ham
<g/>
)	)	kIx)	)
a	a	k8xC	a
materiály	materiál	k1gInPc4	materiál
od	od	k7c2	od
Tolkienových	Tolkienův	k2eAgMnPc2d1	Tolkienův
obdivovatelů	obdivovatel	k1gMnPc2	obdivovatel
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Bodleyova	Bodleyův	k2eAgFnSc1d1	Bodleyův
knihovna	knihovna	k1gFnSc1	knihovna
má	mít	k5eAaImIp3nS	mít
materiály	materiál	k1gInPc4	materiál
k	k	k7c3	k
Silmarillionu	Silmarillion	k1gInSc3	Silmarillion
a	a	k8xC	a
Tolkienovy	Tolkienův	k2eAgFnSc2d1	Tolkienova
vědecké	vědecký	k2eAgFnSc2d1	vědecká
práce	práce	k1gFnSc2	práce
Filologie	filologie	k1gFnSc2	filologie
<g/>
,	,	kIx,	,
studium	studium	k1gNnSc1	studium
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Tolkieova	Tolkieův	k2eAgFnSc1d1	Tolkieův
první	první	k4xOgFnSc1	první
univerzitní	univerzitní	k2eAgFnSc1d1	univerzitní
láska	láska	k1gFnSc1	láska
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
zaujetí	zaujetí	k1gNnPc2	zaujetí
jazykovědou	jazykověda	k1gFnSc7	jazykověda
ho	on	k3xPp3gMnSc4	on
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
téměř	téměř	k6eAd1	téměř
15	[number]	k4	15
umělých	umělý	k2eAgInPc2d1	umělý
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgMnSc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
elfí	elfí	k2eAgInPc1d1	elfí
jazyky	jazyk	k1gInPc1	jazyk
v	v	k7c6	v
Pánovi	pán	k1gMnSc6	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
:	:	kIx,	:
quenijština	quenijština	k1gFnSc1	quenijština
a	a	k8xC	a
sindarština	sindarština	k1gFnSc1	sindarština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
propracoval	propracovat	k5eAaPmAgInS	propracovat
celou	celý	k2eAgFnSc4d1	celá
kosmogonii	kosmogonie	k1gFnSc4	kosmogonie
a	a	k8xC	a
historii	historie	k1gFnSc4	historie
Středozemě	Středozem	k1gFnSc2	Středozem
jako	jako	k8xC	jako
pozadí	pozadí	k1gNnSc2	pozadí
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
těchto	tento	k3xDgInPc2	tento
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hluboké	hluboký	k2eAgFnSc2d1	hluboká
znalosti	znalost	k1gFnSc2	znalost
anglosaštiny	anglosaština	k1gFnSc2	anglosaština
(	(	kIx(	(
<g/>
staré	starý	k2eAgFnSc2d1	stará
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
)	)	kIx)	)
a	a	k8xC	a
staré	starý	k2eAgFnPc4d1	stará
severštiny	severština	k1gFnPc4	severština
Tolkien	Tolkien	k1gInSc1	Tolkien
hovořil	hovořit	k5eAaImAgInS	hovořit
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
mírou	míra	k1gFnSc7	míra
plynulosti	plynulost	k1gFnSc2	plynulost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tuctem	tucet	k1gInSc7	tucet
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
velštinou	velština	k1gFnSc7	velština
<g/>
,	,	kIx,	,
galštinou	galština	k1gFnSc7	galština
<g/>
,	,	kIx,	,
románskými	románský	k2eAgInPc7d1	románský
jazyky	jazyk	k1gInPc7	jazyk
jako	jako	k8xC	jako
francouzštinou	francouzština	k1gFnSc7	francouzština
<g/>
,	,	kIx,	,
španělštinou	španělština	k1gFnSc7	španělština
a	a	k8xC	a
italštinou	italština	k1gFnSc7	italština
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatními	ostatní	k2eAgInPc7d1	ostatní
germánskými	germánský	k2eAgInPc7d1	germánský
jazyky	jazyk	k1gInPc7	jazyk
(	(	kIx(	(
<g/>
ranými	raný	k2eAgFnPc7d1	raná
formami	forma	k1gFnPc7	forma
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
nizozemštiny	nizozemština	k1gFnSc2	nizozemština
jako	jako	k9	jako
například	například	k6eAd1	například
starou	starý	k2eAgFnSc7d1	stará
saštinou	saština	k1gFnSc7	saština
<g/>
)	)	kIx)	)
a	a	k8xC	a
baltskými	baltský	k2eAgInPc7d1	baltský
a	a	k8xC	a
slovanskými	slovanský	k2eAgInPc7d1	slovanský
jazyky	jazyk	k1gInPc7	jazyk
(	(	kIx(	(
<g/>
litevštinou	litevština	k1gFnSc7	litevština
a	a	k8xC	a
ruštinou	ruština	k1gFnSc7	ruština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
osobní	osobní	k2eAgFnSc6d1	osobní
korespondenci	korespondence	k1gFnSc6	korespondence
zmiňoval	zmiňovat	k5eAaImAgMnS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvuk	zvuk	k1gInSc1	zvuk
finského	finský	k2eAgInSc2d1	finský
jazyka	jazyk	k1gInSc2	jazyk
zněl	znět	k5eAaImAgMnS	znět
jeho	jeho	k3xOp3gNnPc3	jeho
uším	ucho	k1gNnPc3	ucho
nejliběji	libě	k6eAd3	libě
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
zdrojem	zdroj	k1gInSc7	zdroj
inspirace	inspirace	k1gFnSc2	inspirace
pro	pro	k7c4	pro
quenijštinu	quenijština	k1gFnSc4	quenijština
<g/>
,	,	kIx,	,
nejdůležitější	důležitý	k2eAgInSc4d3	nejdůležitější
z	z	k7c2	z
jím	jíst	k5eAaImIp1nS	jíst
vytvořených	vytvořený	k2eAgInPc2d1	vytvořený
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
jeho	jeho	k3xOp3gFnPc2	jeho
knih	kniha	k1gFnPc2	kniha
měla	mít	k5eAaImAgFnS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
stránku	stránka	k1gFnSc4	stránka
anglicky	anglicky	k6eAd1	anglicky
psané	psaný	k2eAgInPc4d1	psaný
fantasy	fantas	k1gInPc4	fantas
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
nápadné	nápadný	k2eAgNnSc1d1	nápadné
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
užívání	užívání	k1gNnSc1	užívání
nestandardních	standardní	k2eNgFnPc2d1	nestandardní
forem	forma	k1gFnPc2	forma
slov	slovo	k1gNnPc2	slovo
elf	elf	k1gMnSc1	elf
a	a	k8xC	a
trpaslík	trpaslík	k1gMnSc1	trpaslík
(	(	kIx(	(
<g/>
běžné	běžný	k2eAgInPc1d1	běžný
tvary	tvar	k1gInPc1	tvar
elfin	elfina	k1gFnPc2	elfina
a	a	k8xC	a
dwarfs	dwarfs	k1gInSc1	dwarfs
byly	být	k5eAaImAgFnP	být
vytlačeny	vytlačit	k5eAaPmNgInP	vytlačit
tolkienovskými	tolkienovský	k2eAgInPc7d1	tolkienovský
elvish	elvish	k1gMnSc1	elvish
a	a	k8xC	a
dwarves	dwarves	k1gMnSc1	dwarves
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
eseji	esej	k1gFnSc6	esej
O	o	k7c6	o
pohádkách	pohádka	k1gFnPc6	pohádka
představuje	představovat	k5eAaImIp3nS	představovat
svůj	svůj	k3xOyFgInSc4	svůj
poněkud	poněkud	k6eAd1	poněkud
svérázný	svérázný	k2eAgInSc4d1	svérázný
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Neshodl	shodnout	k5eNaPmAgMnS	shodnout
se	se	k3xPyFc4	se
s	s	k7c7	s
žádnou	žádný	k3yNgFnSc7	žádný
slovníkovou	slovníkový	k2eAgFnSc7d1	slovníková
definicí	definice	k1gFnSc7	definice
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
nejblíže	blízce	k6eAd3	blízce
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
je	být	k5eAaImIp3nS	být
pohádka	pohádka	k1gFnSc1	pohádka
vyprávění	vyprávění	k1gNnSc2	vyprávění
o	o	k7c4	o
faeries	faeries	k1gInSc4	faeries
(	(	kIx(	(
<g/>
malých	malý	k2eAgFnPc6d1	malá
nadpřirozených	nadpřirozený	k2eAgFnPc6d1	nadpřirozená
bytostech	bytost	k1gFnPc6	bytost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
pohádka	pohádka	k1gFnSc1	pohádka
definovaná	definovaný	k2eAgFnSc1d1	definovaná
především	především	k9	především
Faerií	Faerie	k1gFnSc7	Faerie
<g/>
,	,	kIx,	,
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kouzelný	kouzelný	k2eAgInSc1d1	kouzelný
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
faeries	faeries	k1gInSc4	faeries
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc1	přítomnost
faeries	faeriesa	k1gFnPc2	faeriesa
však	však	k9	však
není	být	k5eNaImIp3nS	být
nezbytně	nezbytně	k6eAd1	nezbytně
nutná	nutný	k2eAgFnSc1d1	nutná
<g/>
,	,	kIx,	,
důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
samotné	samotný	k2eAgNnSc1d1	samotné
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
příroda	příroda	k1gFnSc1	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
popisuje	popisovat	k5eAaImIp3nS	popisovat
také	také	k9	také
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
pohádka	pohádka	k1gFnSc1	pohádka
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
pohádky	pohádka	k1gFnPc4	pohádka
nezařazuje	zařazovat	k5eNaImIp3nS	zařazovat
především	především	k6eAd1	především
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
neodehrávají	odehrávat	k5eNaImIp3nP	odehrávat
v	v	k7c6	v
kouzelném	kouzelný	k2eAgInSc6d1	kouzelný
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
nijak	nijak	k6eAd1	nijak
tematicky	tematicky	k6eAd1	tematicky
nedotýkají	dotýkat	k5eNaImIp3nP	dotýkat
<g/>
.	.	kIx.	.
</s>
<s>
Vyřazuje	vyřazovat	k5eAaImIp3nS	vyřazovat
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
příběhy	příběh	k1gInPc1	příběh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
odehrávající	odehrávající	k2eAgMnSc1d1	odehrávající
se	se	k3xPyFc4	se
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
<g/>
)	)	kIx)	)
a	a	k8xC	a
příběhy	příběh	k1gInPc1	příběh
o	o	k7c6	o
zvířatech	zvíře	k1gNnPc6	zvíře
s	s	k7c7	s
lidskými	lidský	k2eAgFnPc7d1	lidská
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pohádka	pohádka	k1gFnSc1	pohádka
O	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
prasátkách	prasátko	k1gNnPc6	prasátko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hledání	hledání	k1gNnSc6	hledání
původu	původ	k1gInSc2	původ
pohádek	pohádka	k1gFnPc2	pohádka
nepovažuje	považovat	k5eNaImIp3nS	považovat
podstatné	podstatný	k2eAgFnPc4d1	podstatná
metody	metoda	k1gFnPc4	metoda
folkloristů	folklorista	k1gMnPc2	folklorista
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
hledání	hledání	k1gNnSc1	hledání
podobných	podobný	k2eAgInPc2d1	podobný
motivů	motiv	k1gInPc2	motiv
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
porovnávání	porovnávání	k1gNnSc4	porovnávání
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
důležitý	důležitý	k2eAgInSc1d1	důležitý
konkrétní	konkrétní	k2eAgInSc1d1	konkrétní
nápad	nápad	k1gInSc1	nápad
autora	autor	k1gMnSc2	autor
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
šíření	šíření	k1gNnSc4	šíření
příběhu	příběh	k1gInSc2	příběh
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
vznik	vznik	k1gInSc1	vznik
pohádky	pohádka	k1gFnSc2	pohádka
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
k	k	k7c3	k
vytahování	vytahování	k1gNnSc3	vytahování
různých	různý	k2eAgInPc2d1	různý
motivů	motiv	k1gInPc2	motiv
z	z	k7c2	z
jakéhosi	jakýsi	k3yIgInSc2	jakýsi
kotle	kotel	k1gInSc2	kotel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
v	v	k7c6	v
pohádkách	pohádka	k1gFnPc6	pohádka
mohou	moct	k5eAaImIp3nP	moct
setkávat	setkávat	k5eAaImF	setkávat
božské	božský	k2eAgFnPc1d1	božská
bytosti	bytost	k1gFnPc1	bytost
s	s	k7c7	s
bytostmi	bytost	k1gFnPc7	bytost
z	z	k7c2	z
lidského	lidský	k2eAgInSc2d1	lidský
světa	svět	k1gInSc2	svět
apod.	apod.	kA	apod.
Ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
mytologie	mytologie	k1gFnSc2	mytologie
a	a	k8xC	a
pohádek	pohádka	k1gFnPc2	pohádka
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mytologické	mytologický	k2eAgInPc1d1	mytologický
postavy	postav	k1gInPc1	postav
vznikaly	vznikat	k5eAaImAgInP	vznikat
inspirováním	inspirování	k1gNnSc7	inspirování
se	s	k7c7	s
přírodními	přírodní	k2eAgInPc7d1	přírodní
jevy	jev	k1gInPc7	jev
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
připojením	připojení	k1gNnSc7	připojení
určitých	určitý	k2eAgFnPc2d1	určitá
lidských	lidský	k2eAgFnPc2d1	lidská
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
božstev	božstvo	k1gNnPc2	božstvo
dala	dát	k5eAaPmAgFnS	dát
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
pohádkového	pohádkový	k2eAgInSc2d1	pohádkový
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
se	se	k3xPyFc4	se
také	také	k9	také
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
s	s	k7c7	s
typickou	typický	k2eAgFnSc7d1	typická
vlastností	vlastnost	k1gFnSc7	vlastnost
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
byla	být	k5eAaImAgFnS	být
přidělena	přidělen	k2eAgFnSc1d1	přidělena
přírodní	přírodní	k2eAgFnSc1d1	přírodní
moc	moc	k1gFnSc1	moc
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
velkým	velký	k2eAgNnSc7d1	velké
tématem	téma	k1gNnSc7	téma
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vztah	vztah	k1gInSc4	vztah
pohádek	pohádka	k1gFnPc2	pohádka
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
pohádky	pohádka	k1gFnPc1	pohádka
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
určené	určený	k2eAgFnPc1d1	určená
primárně	primárně	k6eAd1	primárně
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gFnPc4	on
četli	číst	k5eAaImAgMnP	číst
i	i	k9	i
dospělí	dospělí	k1gMnPc1	dospělí
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pohádky	pohádka	k1gFnPc1	pohádka
čtou	číst	k5eAaImIp3nP	číst
i	i	k9	i
dospělí	dospělí	k1gMnPc1	dospělí
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nP	vyprávět
je	on	k3xPp3gFnPc4	on
pak	pak	k6eAd1	pak
svým	svůj	k3xOyFgFnPc3	svůj
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
předávané	předávaný	k2eAgFnPc1d1	předávaná
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
zkušeností	zkušenost	k1gFnPc2	zkušenost
než	než	k8xS	než
dospělí	dospělí	k1gMnPc1	dospělí
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
schopné	schopný	k2eAgNnSc4d1	schopné
spíše	spíše	k9	spíše
uvěřit	uvěřit	k5eAaPmF	uvěřit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
vymyká	vymykat	k5eAaImIp3nS	vymykat
logice	logika	k1gFnSc3	logika
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
dospělí	dospělí	k1gMnPc1	dospělí
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
plně	plně	k6eAd1	plně
rozvinuté	rozvinutý	k2eAgNnSc4d1	rozvinuté
abstraktní	abstraktní	k2eAgNnSc4d1	abstraktní
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
schopni	schopen	k2eAgMnPc1d1	schopen
odhalit	odhalit	k5eAaPmF	odhalit
skryté	skrytý	k2eAgFnPc4d1	skrytá
souvislosti	souvislost	k1gFnPc4	souvislost
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
pohádky	pohádka	k1gFnPc4	pohádka
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
některé	některý	k3yIgFnPc4	některý
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
s	s	k7c7	s
přibývajícím	přibývající	k2eAgInSc7d1	přibývající
věkem	věk	k1gInSc7	věk
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
pohádky	pohádka	k1gFnPc4	pohádka
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
<g/>
)	)	kIx)	)
prohloubí	prohloubit	k5eAaPmIp3nS	prohloubit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byly	být	k5eAaImAgFnP	být
pohádky	pohádka	k1gFnPc1	pohádka
původně	původně	k6eAd1	původně
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
různé	různý	k2eAgInPc4d1	různý
motivy	motiv	k1gInPc4	motiv
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
postupně	postupně	k6eAd1	postupně
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
vhodné	vhodný	k2eAgFnPc4d1	vhodná
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yQnSc7	což
Tolkien	Tolkien	k2eAgMnSc1d1	Tolkien
příliš	příliš	k6eAd1	příliš
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
ani	ani	k8xC	ani
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
děti	dítě	k1gFnPc1	dítě
mají	mít	k5eAaImIp3nP	mít
pohádky	pohádka	k1gFnPc4	pohádka
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
uvěřit	uvěřit	k5eAaPmF	uvěřit
nadpřirozeným	nadpřirozený	k2eAgFnPc3d1	nadpřirozená
věcem	věc	k1gFnPc3	věc
<g/>
,	,	kIx,	,
argumentuje	argumentovat	k5eAaImIp3nS	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
rád	rád	k6eAd1	rád
pohádky	pohádka	k1gFnSc2	pohádka
např.	např.	kA	např.
s	s	k7c7	s
draky	drak	k1gMnPc7	drak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
skuteční	skutečný	k2eAgMnPc1d1	skutečný
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
1936	[number]	k4	1936
Songs	Songsa	k1gFnPc2	Songsa
for	forum	k1gNnPc2	forum
the	the	k?	the
Philologists	Philologists	k1gInSc1	Philologists
(	(	kIx(	(
<g/>
Písně	píseň	k1gFnPc1	píseň
pro	pro	k7c4	pro
filology	filolog	k1gMnPc4	filolog
<g/>
)	)	kIx)	)
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
humorných	humorný	k2eAgFnPc2d1	humorná
básní	báseň	k1gFnPc2	báseň
Tolkiena	Tolkieno	k1gNnSc2	Tolkieno
(	(	kIx(	(
<g/>
13	[number]	k4	13
básní	báseň	k1gFnPc2	báseň
z	z	k7c2	z
30	[number]	k4	30
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
E.	E.	kA	E.
V.	V.	kA	V.
Gordona	Gordon	k1gMnSc2	Gordon
a	a	k8xC	a
lidových	lidový	k2eAgFnPc2d1	lidová
písní	píseň	k1gFnPc2	píseň
1937	[number]	k4	1937
The	The	k1gFnPc2	The
Hobbit	Hobbit	k1gFnPc2	Hobbit
or	or	k?	or
There	Ther	k1gInSc5	Ther
and	and	k?	and
Back	Back	k1gMnSc1	Back
Again	Again	k1gMnSc1	Again
(	(	kIx(	(
<g/>
Hobit	hobit	k1gMnSc1	hobit
aneb	aneb	k?	aneb
Cesta	cesta	k1gFnSc1	cesta
tam	tam	k6eAd1	tam
a	a	k8xC	a
zase	zase	k9	zase
zpátky	zpátky	k6eAd1	zpátky
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
–	–	k?	–
pohádkový	pohádkový	k2eAgInSc4d1	pohádkový
příběh	příběh	k1gInSc4	příběh
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
Středozemě	Středozem	k1gFnSc2	Středozem
1945	[number]	k4	1945
Leaf	Leaf	k1gMnSc1	Leaf
by	by	k9	by
Niggle	Niggle	k1gInSc4	Niggle
(	(	kIx(	(
<g/>
Nimralův	nimralův	k2eAgInSc4d1	nimralův
list	list	k1gInSc4	list
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
poprvé	poprvé	k6eAd1	poprvé
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
Winston	Winston	k1gInSc4	Winston
Smith	Smitha	k1gFnPc2	Smitha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
<g/>
;	;	kIx,	;
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
také	také	k9	také
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
List	list	k1gInSc1	list
od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
Nimrala	nimrat	k5eAaImAgFnS	nimrat
<g/>
)	)	kIx)	)
–	–	k?	–
krátký	krátký	k2eAgInSc4d1	krátký
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
alegorie	alegorie	k1gFnSc1	alegorie
Tolkienovy	Tolkienův	k2eAgFnSc2d1	Tolkienova
tvorby	tvorba	k1gFnSc2	tvorba
1945	[number]	k4	1945
The	The	k1gFnPc2	The
Lay	Lay	k1gMnSc7	Lay
of	of	k?	of
Aotrou	Aotra	k1gMnSc7	Aotra
and	and	k?	and
Itroun	Itroun	k1gMnSc1	Itroun
–	–	k?	–
publikováno	publikován	k2eAgNnSc1d1	Publikováno
v	v	k7c6	v
prosincovém	prosincový	k2eAgInSc6d1	prosincový
Welsh	Welsh	k1gInSc1	Welsh
Review	Review	k1gFnSc1	Review
1949	[number]	k4	1949
Farmer	Farmer	k1gMnSc1	Farmer
Giles	Giles	k1gMnSc1	Giles
of	of	k?	of
Ham	ham	k0	ham
(	(	kIx(	(
<g/>
Sedlák	Sedlák	k1gMnSc1	Sedlák
Jiljí	Jiljí	k1gMnSc1	Jiljí
z	z	k7c2	z
Oujezda	Oujezdo	k1gNnSc2	Oujezdo
<g/>
,	,	kIx,	,
uváděno	uváděn	k2eAgNnSc1d1	uváděno
též	též	k9	též
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Farmář	farmář	k1gMnSc1	farmář
Giles	Giles	k1gMnSc1	Giles
z	z	k7c2	z
Hamu	Hamus	k1gInSc2	Hamus
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
poprvé	poprvé	k6eAd1	poprvé
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
Winston	Winston	k1gInSc4	Winston
Smith	Smitha	k1gFnPc2	Smitha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
–	–	k?	–
středověká	středověký	k2eAgFnSc1d1	středověká
báje	báje	k1gFnSc1	báje
1953	[number]	k4	1953
The	The	k1gFnPc2	The
Homecoming	Homecoming	k1gInSc4	Homecoming
of	of	k?	of
Beorhtnoth	Beorhtnoth	k1gInSc1	Beorhtnoth
<g/>
,	,	kIx,	,
Beorhthelm	Beorhthelm	k1gInSc1	Beorhthelm
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Son	son	k1gInSc1	son
(	(	kIx(	(
<g/>
Návrat	návrat	k1gInSc1	návrat
Beorhtnothe	Beorhtnoth	k1gMnSc2	Beorhtnoth
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
Beorhthelmova	Beorhthelmův	k2eAgFnSc1d1	Beorhthelmův
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Příběhy	příběh	k1gInPc1	příběh
z	z	k7c2	z
čarovné	čarovný	k2eAgFnSc2d1	čarovná
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
–	–	k?	–
hra	hra	k1gFnSc1	hra
v	v	k7c6	v
aliteračním	aliterační	k2eAgInSc6d1	aliterační
verši	verš	k1gInSc6	verš
<g/>
,	,	kIx,	,
publikována	publikovat	k5eAaBmNgFnS	publikovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
esejemi	esej	k1gFnPc7	esej
Beorhtnoth	Beorhtnotha	k1gFnPc2	Beorhtnotha
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Death	Death	k1gInSc1	Death
a	a	k8xC	a
Ofermod	Ofermod	k1gInSc1	Ofermod
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
vědeckého	vědecký	k2eAgInSc2d1	vědecký
časopisu	časopis	k1gInSc2	časopis
Essays	Essaysa	k1gFnPc2	Essaysa
and	and	k?	and
Studies	Studies	k1gMnSc1	Studies
by	by	kYmCp3nS	by
members	members	k6eAd1	members
of	of	k?	of
the	the	k?	the
English	English	k1gInSc1	English
Association	Association	k1gInSc1	Association
třísvazkový	třísvazkový	k2eAgInSc1d1	třísvazkový
román	román	k1gInSc1	román
The	The	k1gMnSc1	The
Lord	lord	k1gMnSc1	lord
of	of	k?	of
the	the	k?	the
Rings	Rings	k1gInSc1	Rings
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
1954	[number]	k4	1954
The	The	k1gMnSc1	The
Fellowship	Fellowship	k1gMnSc1	Fellowship
of	of	k?	of
the	the	k?	the
Ring	ring	k1gInSc1	ring
(	(	kIx(	(
<g/>
Společenstvo	společenstvo	k1gNnSc1	společenstvo
prstenu	prsten	k1gInSc2	prsten
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
1954	[number]	k4	1954
The	The	k1gFnSc2	The
Two	Two	k1gFnPc2	Two
Towers	Towers	k1gInSc1	Towers
(	(	kIx(	(
<g/>
Dvě	dva	k4xCgFnPc1	dva
věže	věž	k1gFnPc1	věž
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
1955	[number]	k4	1955
The	The	k1gMnSc1	The
Return	Return	k1gMnSc1	Return
of	of	k?	of
the	the	k?	the
King	King	k1gMnSc1	King
(	(	kIx(	(
<g/>
Návrat	návrat	k1gInSc1	návrat
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
1962	[number]	k4	1962
The	The	k1gMnSc1	The
Adventures	Adventures	k1gMnSc1	Adventures
of	of	k?	of
Tom	Tom	k1gMnSc1	Tom
Bombadil	Bombadil	k1gMnSc1	Bombadil
and	and	k?	and
Other	Other	k1gMnSc1	Other
Verses	Verses	k1gMnSc1	Verses
from	from	k1gMnSc1	from
the	the	k?	the
Red	Red	k1gMnSc1	Red
Book	Book	k1gMnSc1	Book
(	(	kIx(	(
<g/>
Příhody	Příhoda	k1gMnSc2	Příhoda
Toma	Tom	k1gMnSc2	Tom
Bombadila	Bombadila	k1gMnSc2	Bombadila
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
sbírce	sbírka	k1gFnSc3	sbírka
Příběhy	příběh	k1gInPc1	příběh
z	z	k7c2	z
čarovné	čarovný	k2eAgFnSc2d1	čarovná
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
1964	[number]	k4	1964
Tree	Tre	k1gFnSc2	Tre
and	and	k?	and
Leaf	Leaf	k1gInSc1	Leaf
(	(	kIx(	(
<g/>
Strom	strom	k1gInSc1	strom
a	a	k8xC	a
list	list	k1gInSc1	list
<g/>
)	)	kIx)	)
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
obsahující	obsahující	k2eAgFnSc1d1	obsahující
<g/>
:	:	kIx,	:
On	on	k3xPp3gMnSc1	on
Fairy-Stories	Fairy-Stories	k1gMnSc1	Fairy-Stories
(	(	kIx(	(
<g/>
O	o	k7c6	o
pohádkách	pohádka	k1gFnPc6	pohádka
<g/>
)	)	kIx)	)
–	–	k?	–
vizte	vidět	k5eAaImRp2nP	vidět
vědecké	vědecký	k2eAgFnPc4d1	vědecká
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
Leaf	Leaf	k1gInSc4	Leaf
by	by	k9	by
Niggle	Niggle	k1gFnSc1	Niggle
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
List	list	k1gInSc1	list
od	od	k7c2	od
Nimrala	nimral	k1gMnSc2	nimral
<g/>
)	)	kIx)	)
–	–	k?	–
poprvé	poprvé	k6eAd1	poprvé
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
1966	[number]	k4	1966
The	The	k1gFnPc2	The
Tolkien	Tolkien	k2eAgMnSc1d1	Tolkien
Reader	Reader	k1gMnSc1	Reader
–	–	k?	–
antologie	antologie	k1gFnSc1	antologie
dříve	dříve	k6eAd2	dříve
vyšlých	vyšlý	k2eAgInPc2d1	vyšlý
textů	text	k1gInPc2	text
(	(	kIx(	(
<g/>
The	The	k1gFnPc2	The
Homecoming	Homecoming	k1gInSc1	Homecoming
of	of	k?	of
Beorhtnoth	Beorhtnoth	k1gInSc1	Beorhtnoth
Beorhthelm	Beorhthelm	k1gInSc1	Beorhthelm
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Son	son	k1gInSc1	son
<g/>
,	,	kIx,	,
On	on	k3xPp3gMnSc1	on
Fairy-Stories	Fairy-Stories	k1gMnSc1	Fairy-Stories
<g/>
,	,	kIx,	,
Leaf	Leaf	k1gInSc4	Leaf
by	by	kYmCp3nS	by
Niggle	Niggle	k1gFnSc1	Niggle
<g/>
,	,	kIx,	,
Farmer	Farmer	k1gMnSc1	Farmer
Giles	Giles	k1gMnSc1	Giles
of	of	k?	of
Ham	ham	k0	ham
a	a	k8xC	a
The	The	k1gFnPc7	The
<g />
.	.	kIx.	.
</s>
<s>
Adventures	Adventures	k1gMnSc1	Adventures
of	of	k?	of
Tom	Tom	k1gMnSc1	Tom
Bombadil	Bombadil	k1gMnSc1	Bombadil
<g/>
)	)	kIx)	)
1967	[number]	k4	1967
The	The	k1gMnSc1	The
Road	Road	k1gMnSc1	Road
Goes	Goesa	k1gFnPc2	Goesa
Ever	Ever	k1gMnSc1	Ever
On	on	k3xPp3gMnSc1	on
(	(	kIx(	(
<g/>
Cesta	cesta	k1gFnSc1	cesta
jde	jít	k5eAaImIp3nS	jít
pořád	pořád	k6eAd1	pořád
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
–	–	k?	–
pochodová	pochodový	k2eAgFnSc1d1	pochodová
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
útržky	útržka	k1gFnPc4	útržka
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
Pánovi	pán	k1gMnSc6	pán
prstenů	prsten	k1gInPc2	prsten
i	i	k9	i
v	v	k7c6	v
Hobitovi	hobit	k1gMnSc6	hobit
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
složil	složit	k5eAaPmAgMnS	složit
Donald	Donald	k1gMnSc1	Donald
Swann	Swann	k1gInSc4	Swann
1967	[number]	k4	1967
Smith	Smith	k1gMnSc1	Smith
of	of	k?	of
Wootton	Wootton	k1gInSc1	Wootton
Major	major	k1gMnSc1	major
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Kovář	Kovář	k1gMnSc1	Kovář
z	z	k7c2	z
Velké	velká	k1gFnSc2	velká
Lesné	lesný	k2eAgFnSc2d1	Lesná
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
Winston	Winston	k1gInSc4	Winston
Smith	Smitha	k1gFnPc2	Smitha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
samostatně	samostatně	k6eAd1	samostatně
pak	pak	k6eAd1	pak
v	v	k7c4	v
Aulos	Aulos	k1gInSc4	Aulos
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
–	–	k?	–
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
kováři	kovář	k1gMnSc6	kovář
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
kouzelné	kouzelný	k2eAgFnSc3d1	kouzelná
hvězdičce	hvězdička	k1gFnSc3	hvězdička
<g/>
,	,	kIx,	,
o	o	k7c6	o
královně	královna	k1gFnSc6	královna
a	a	k8xC	a
králi	král	k1gMnPc7	král
víl	víla	k1gFnPc2	víla
<g/>
,	,	kIx,	,
o	o	k7c6	o
<g />
.	.	kIx.	.
</s>
<s>
pohádkové	pohádkový	k2eAgFnSc3d1	pohádková
říši	říš	k1gFnSc3	říš
1922	[number]	k4	1922
A	a	k9	a
Middle	Middle	k1gFnPc1	Middle
English	English	k1gInSc1	English
Vocabulary	Vocabulara	k1gFnSc2	Vocabulara
(	(	kIx(	(
<g/>
Slovník	slovník	k1gInSc1	slovník
středověké	středověký	k2eAgFnSc2d1	středověká
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
Clarendon	Clarendon	k1gNnSc1	Clarendon
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
168	[number]	k4	168
stran	strana	k1gFnPc2	strana
1924	[number]	k4	1924
Sir	sir	k1gMnSc1	sir
Gawain	Gawain	k1gMnSc1	Gawain
and	and	k?	and
the	the	k?	the
Green	Green	k2eAgMnSc1d1	Green
Knight	Knight	k1gMnSc1	Knight
(	(	kIx(	(
<g/>
Pan	Pan	k1gMnSc1	Pan
Gawain	Gawain	k1gMnSc1	Gawain
a	a	k8xC	a
Zelený	Zelený	k1gMnSc1	Zelený
rytíř	rytíř	k1gMnSc1	rytíř
nebo	nebo	k8xC	nebo
též	též	k9	též
Sir	sir	k1gMnSc1	sir
Gawain	Gawain	k1gMnSc1	Gawain
a	a	k8xC	a
Zelený	Zelený	k1gMnSc1	Zelený
rytíř	rytíř	k1gMnSc1	rytíř
<g/>
)	)	kIx)	)
–	–	k?	–
nové	nový	k2eAgFnSc2d1	nová
<g />
.	.	kIx.	.
</s>
<s>
vydání	vydání	k1gNnSc1	vydání
středoanglické	středoanglický	k2eAgFnPc1d1	středoanglický
básně	báseň	k1gFnPc1	báseň
s	s	k7c7	s
poznámkami	poznámka	k1gFnPc7	poznámka
<g/>
,	,	kIx,	,
spoluautor	spoluautor	k1gMnSc1	spoluautor
s	s	k7c7	s
E.	E.	kA	E.
V.	V.	kA	V.
Gordonem	Gordon	k1gMnSc7	Gordon
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
211	[number]	k4	211
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
revidovanou	revidovaný	k2eAgFnSc7d1	revidovaná
edicí	edice	k1gFnSc7	edice
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
Clarendon	Clarendon	k1gNnSc1	Clarendon
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
232	[number]	k4	232
stran	strana	k1gFnPc2	strana
1925	[number]	k4	1925
Some	Some	k1gFnPc2	Some
Contributions	Contributionsa	k1gFnPc2	Contributionsa
to	ten	k3xDgNnSc4	ten
Middle-English	Middle-English	k1gMnSc1	Middle-English
Lexicography	Lexicographa	k1gFnSc2	Lexicographa
–	–	k?	–
publikováno	publikován	k2eAgNnSc1d1	Publikováno
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
čísle	číslo	k1gNnSc6	číslo
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
The	The	k1gMnSc1	The
Review	Review	k1gMnSc1	Review
of	of	k?	of
English	English	k1gMnSc1	English
Studies	Studies	k1gMnSc1	Studies
<g/>
,	,	kIx,	,
na	na	k7c6	na
stranách	strana	k1gFnPc6	strana
210	[number]	k4	210
<g/>
–	–	k?	–
<g/>
215	[number]	k4	215
<g/>
.	.	kIx.	.
1925	[number]	k4	1925
The	The	k1gMnSc1	The
Devil	Devil	k1gMnSc1	Devil
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Coach	Coach	k1gInSc1	Coach
Horses	Horses	k1gMnSc1	Horses
–	–	k?	–
esej	esej	k1gFnSc1	esej
<g/>
,	,	kIx,	,
publikována	publikovat	k5eAaBmNgFnS	publikovat
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
čísle	číslo	k1gNnSc6	číslo
1	[number]	k4	1
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
The	The	k1gMnSc1	The
Review	Review	k1gMnSc1	Review
of	of	k?	of
English	English	k1gMnSc1	English
Studies	Studies	k1gMnSc1	Studies
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
stranách	strana	k1gFnPc6	strana
331	[number]	k4	331
<g/>
–	–	k?	–
<g/>
336	[number]	k4	336
<g/>
.	.	kIx.	.
1929	[number]	k4	1929
Ancrene	Ancren	k1gInSc5	Ancren
Wisse	Wiss	k1gInSc5	Wiss
and	and	k?	and
Hali	hali	k0	hali
Meið	Meið	k1gFnPc3	Meið
–	–	k?	–
esej	esej	k1gFnSc4	esej
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dvou	dva	k4xCgNnPc2	dva
děl	dělo	k1gNnPc2	dělo
psaných	psaný	k2eAgFnPc2d1	psaná
střední	střední	k2eAgFnSc7d1	střední
angličtinou	angličtina	k1gFnSc7	angličtina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
dílu	díl	k1gInSc2	díl
Essays	Essays	k1gInSc1	Essays
and	and	k?	and
Studies	Studies	k1gInSc1	Studies
by	by	kYmCp3nS	by
members	members	k6eAd1	members
of	of	k?	of
the	the	k?	the
English	English	k1gInSc1	English
Association	Association	k1gInSc1	Association
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
stranách	strana	k1gFnPc6	strana
104	[number]	k4	104
<g/>
–	–	k?	–
<g/>
126	[number]	k4	126
<g/>
.	.	kIx.	.
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
Sigelwara	Sigelwara	k1gFnSc1	Sigelwara
Land	Landa	k1gFnPc2	Landa
–	–	k?	–
část	část	k1gFnSc4	část
I	I	kA	I
a	a	k8xC	a
II	II	kA	II
<g/>
,	,	kIx,	,
v	v	k7c4	v
Medium	medium	k1gNnSc4	medium
Aevum	Aevum	k1gInSc1	Aevum
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
čísle	číslo	k1gNnSc6	číslo
1	[number]	k4	1
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
<g/>
,	,	kIx,	,
na	na	k7c6	na
stranách	strana	k1gFnPc6	strana
183	[number]	k4	183
<g/>
–	–	k?	–
<g/>
196	[number]	k4	196
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
prosinec	prosinec	k1gInSc1	prosinec
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
čísle	číslo	k1gNnSc6	číslo
3	[number]	k4	3
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
stranách	strana	k1gFnPc6	strana
95	[number]	k4	95
<g/>
–	–	k?	–
<g/>
111	[number]	k4	111
<g/>
.	.	kIx.	.
1934	[number]	k4	1934
Chaucer	Chaucero	k1gNnPc2	Chaucero
as	as	k1gInSc1	as
a	a	k8xC	a
Philologist	Philologist	k1gInSc1	Philologist
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Reeve	Reeev	k1gFnSc2	Reeev
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tale	Tale	k1gInSc1	Tale
–	–	k?	–
znovuobjevení	znovuobjevení	k1gNnSc1	znovuobjevení
jazykového	jazykový	k2eAgInSc2d1	jazykový
humoru	humor	k1gInSc2	humor
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
originálním	originální	k2eAgMnSc7d1	originální
rukopisu	rukopis	k1gInSc2	rukopis
Chaucerových	Chaucerův	k2eAgFnPc2d1	Chaucerova
Canterburských	Canterburský	k2eAgFnPc2d1	Canterburská
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c4	v
Transactions	Transactions	k1gInSc4	Transactions
of	of	k?	of
the	the	k?	the
Philological	Philological	k1gFnSc2	Philological
Society	societa	k1gFnSc2	societa
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
na	na	k7c6	na
stranách	strana	k1gFnPc6	strana
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
1937	[number]	k4	1937
Beowulf	Beowulf	k1gMnSc1	Beowulf
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Monsters	Monstersa	k1gFnPc2	Monstersa
and	and	k?	and
the	the	k?	the
Critics	Critics	k1gInSc1	Critics
(	(	kIx(	(
<g/>
Béowulf	Béowulf	k1gInSc1	Béowulf
<g/>
:	:	kIx,	:
Netvoři	netvor	k1gMnPc1	netvor
a	a	k8xC	a
kritikové	kritik	k1gMnPc1	kritik
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
<g />
.	.	kIx.	.
</s>
<s>
Netvoři	netvor	k1gMnPc1	netvor
a	a	k8xC	a
kritikové	kritik	k1gMnPc1	kritik
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
eseje	esej	k1gFnPc1	esej
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
též	též	k9	též
uvádí	uvádět	k5eAaImIp3nS	uvádět
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Béowulf	Béowulf	k1gInSc1	Béowulf
<g/>
:	:	kIx,	:
Příšery	příšera	k1gFnPc1	příšera
a	a	k8xC	a
kritikové	kritik	k1gMnPc1	kritik
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
publikace	publikace	k1gFnSc2	publikace
jeho	jeho	k3xOp3gFnSc2	jeho
přednášky	přednáška	k1gFnSc2	přednáška
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g />
.	.	kIx.	.
</s>
<s>
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	se	k3xPyFc4	se
kritickým	kritický	k2eAgInSc7d1	kritický
rozborem	rozbor	k1gInSc7	rozbor
básně	báseň	k1gFnSc2	báseň
Beowulf	Beowulf	k1gInSc1	Beowulf
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
Humphrey	Humphrey	k1gInPc4	Humphrey
Milford	Milforda	k1gFnPc2	Milforda
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
56	[number]	k4	56
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
1939	[number]	k4	1939
The	The	k1gFnSc2	The
Reeve	Reeev	k1gFnSc2	Reeev
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tale	Tale	k1gFnSc7	Tale
–	–	k?	–
verze	verze	k1gFnSc1	verze
přizpůsobená	přizpůsobený	k2eAgFnSc1d1	přizpůsobená
recitacím	recitace	k1gFnPc3	recitace
při	při	k7c6	při
"	"	kIx"	"
<g/>
letních	letní	k2eAgFnPc6d1	letní
radovánkách	radovánka	k1gFnPc6	radovánka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
14	[number]	k4	14
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
1939	[number]	k4	1939
On	on	k3xPp3gMnSc1	on
<g />
.	.	kIx.	.
</s>
<s>
Fairy-Stories	Fairy-Stories	k1gInSc1	Fairy-Stories
(	(	kIx(	(
<g/>
O	o	k7c6	o
pohádkách	pohádka	k1gFnPc6	pohádka
<g/>
)	)	kIx)	)
–	–	k?	–
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
Tolkienovu	Tolkienův	k2eAgFnSc4d1	Tolkienova
filosofii	filosofie	k1gFnSc4	filosofie
fantasy	fantas	k1gInPc4	fantas
<g/>
,	,	kIx,	,
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
série	série	k1gFnSc2	série
přednášek	přednáška	k1gFnPc2	přednáška
Andrew	Andrew	k1gMnSc1	Andrew
Lang	Lang	k1gMnSc1	Lang
lectures	lectures	k1gMnSc1	lectures
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zkrácenou	zkrácený	k2eAgFnSc4d1	zkrácená
verzi	verze	k1gFnSc4	verze
větší	veliký	k2eAgInPc4d2	veliký
eseje	esej	k1gInPc4	esej
vydané	vydaný	k2eAgInPc4d1	vydaný
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
1944	[number]	k4	1944
Sir	sir	k1gMnSc1	sir
Orfeo	Orfeo	k1gMnSc1	Orfeo
–	–	k?	–
vydání	vydání	k1gNnSc4	vydání
středověké	středověký	k2eAgFnSc2d1	středověká
básně	báseň	k1gFnSc2	báseň
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Academic	Academic	k1gMnSc1	Academic
<g />
.	.	kIx.	.
</s>
<s>
Copying	Copying	k1gInSc1	Copying
Office	Office	kA	Office
<g/>
,	,	kIx,	,
18	[number]	k4	18
stran	strana	k1gFnPc2	strana
1947	[number]	k4	1947
On	on	k3xPp3gMnSc1	on
Fairy-Stories	Fairy-Stories	k1gMnSc1	Fairy-Stories
(	(	kIx(	(
<g/>
O	o	k7c6	o
pohádkách	pohádka	k1gFnPc6	pohádka
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
Winston	Winston	k1gInSc4	Winston
Smith	Smitha	k1gFnPc2	Smitha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
na	na	k7c6	na
stranách	strana	k1gFnPc6	strana
115	[number]	k4	115
<g/>
–	–	k?	–
<g/>
188	[number]	k4	188
<g/>
)	)	kIx)	)
–	–	k?	–
Tolkienova	Tolkienův	k2eAgFnSc1d1	Tolkienova
přednáška	přednáška	k1gFnSc1	přednáška
o	o	k7c6	o
povaze	povaha	k1gFnSc6	povaha
a	a	k8xC	a
smyslu	smysl	k1gInSc6	smysl
pohádek	pohádka	k1gFnPc2	pohádka
a	a	k8xC	a
fantastických	fantastický	k2eAgInPc2d1	fantastický
<g />
.	.	kIx.	.
</s>
<s>
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
v	v	k7c4	v
Essays	Essays	k1gInSc4	Essays
presented	presented	k1gMnSc1	presented
to	ten	k3xDgNnSc1	ten
Charles	Charles	k1gMnSc1	Charles
Williams	Williams	k1gInSc1	Williams
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
prezentována	prezentovat	k5eAaBmNgFnS	prezentovat
ve	v	k7c6	v
zkrácené	zkrácený	k2eAgFnSc6d1	zkrácená
verzi	verze	k1gFnSc6	verze
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
St.	st.	kA	st.
Andrews	Andrews	k1gInSc4	Andrews
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
1953	[number]	k4	1953
Ofermod	Ofermoda	k1gFnPc2	Ofermoda
a	a	k8xC	a
Beorhtnoth	Beorhtnotha	k1gFnPc2	Beorhtnotha
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Death	Death	k1gInSc1	Death
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
eseje	esej	k1gFnPc1	esej
publikované	publikovaný	k2eAgFnPc1d1	publikovaná
s	s	k7c7	s
básní	báseň	k1gFnSc7	báseň
The	The	k1gFnSc2	The
Homecoming	Homecoming	k1gInSc1	Homecoming
of	of	k?	of
<g />
.	.	kIx.	.
</s>
<s>
Beorhtnoth	Beorhtnoth	k1gMnSc1	Beorhtnoth
<g/>
,	,	kIx,	,
Beorhthelm	Beorhthelm	k1gMnSc1	Beorhthelm
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Son	son	k1gInSc1	son
(	(	kIx(	(
<g/>
Návrat	návrat	k1gInSc1	návrat
Beorhtnothe	Beorhtnoth	k1gMnSc2	Beorhtnoth
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
Beorhthelmova	Beorhthelmův	k2eAgMnSc2d1	Beorhthelmův
<g/>
)	)	kIx)	)
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
díle	díl	k1gInSc6	díl
Essays	Essays	k1gInSc4	Essays
and	and	k?	and
Studies	Studies	k1gInSc1	Studies
by	by	kYmCp3nS	by
members	members	k6eAd1	members
of	of	k?	of
the	the	k?	the
English	English	k1gInSc1	English
Association	Association	k1gInSc1	Association
na	na	k7c6	na
stranách	strana	k1gFnPc6	strana
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
1953	[number]	k4	1953
Middle	Middle	k1gMnSc1	Middle
English	English	k1gMnSc1	English
"	"	kIx"	"
<g/>
Losenger	Losenger	k1gMnSc1	Losenger
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Sketch	Sketch	k1gInSc1	Sketch
of	of	k?	of
an	an	k?	an
etymological	etymologicat	k5eAaPmAgMnS	etymologicat
and	and	k?	and
semantic	semantice	k1gFnPc2	semantice
enquiry	enquira	k1gFnSc2	enquira
–	–	k?	–
publikováno	publikován	k2eAgNnSc1d1	Publikováno
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Essais	Essais	k1gFnSc2	Essais
de	de	k?	de
philologie	philologie	k1gFnSc1	philologie
moderne	modernout	k5eAaPmIp3nS	modernout
<g/>
:	:	kIx,	:
Communications	Communications	k1gInSc1	Communications
présentées	présentéesa	k1gFnPc2	présentéesa
au	au	k0	au
Congrè	Congrè	k1gMnSc1	Congrè
1962	[number]	k4	1962
Ancrene	Ancren	k1gMnSc5	Ancren
Wisse	Wiss	k1gMnSc5	Wiss
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
English	English	k1gMnSc1	English
Text	text	k1gInSc1	text
of	of	k?	of
the	the	k?	the
Ancrene	Ancren	k1gMnSc5	Ancren
Riwle	Riwl	k1gMnSc5	Riwl
–	–	k?	–
pro	pro	k7c4	pro
Early	earl	k1gMnPc4	earl
English	English	k1gInSc4	English
Text	text	k1gInSc1	text
Society	societa	k1gFnSc2	societa
vydal	vydat	k5eAaPmAgInS	vydat
Oxford	Oxford	k1gInSc4	Oxford
University	universita	k1gFnSc2	universita
Press	Press	k1gInSc4	Press
1963	[number]	k4	1963
English	English	k1gMnSc1	English
and	and	k?	and
Welsh	Welsh	k1gMnSc1	Welsh
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
velština	velština	k1gFnSc1	velština
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Netvoři	netvor	k1gMnPc1	netvor
a	a	k8xC	a
kritikové	kritik	k1gMnPc1	kritik
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
eseje	esej	k1gFnPc1	esej
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
183	[number]	k4	183
<g/>
–	–	k?	–
<g/>
219	[number]	k4	219
<g/>
)	)	kIx)	)
–	–	k?	–
první	první	k4xOgFnSc1	první
přednáška	přednáška	k1gFnSc1	přednáška
z	z	k7c2	z
oxfordské	oxfordský	k2eAgFnSc2d1	Oxfordská
série	série	k1gFnSc2	série
O	o	k7c6	o
<g/>
'	'	kIx"	'
<g/>
Donnelových	Donnelův	k2eAgFnPc2d1	Donnelův
přednášek	přednáška	k1gFnPc2	přednáška
přednesená	přednesený	k2eAgFnSc1d1	přednesená
21	[number]	k4	21
<g />
.	.	kIx.	.
</s>
<s>
<g/>
října	říjen	k1gInSc2	říjen
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Angles	Angles	k1gMnSc1	Angles
and	and	k?	and
Britons	Britons	k1gInSc1	Britons
<g/>
:	:	kIx,	:
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Donnell	Donnell	k1gMnSc1	Donnell
Lectures	Lectures	k1gMnSc1	Lectures
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
Cardiff	Cardiff	k1gMnSc1	Cardiff
Press	Press	k1gInSc4	Press
1964	[number]	k4	1964
Úvod	úvod	k1gInSc1	úvod
ke	k	k7c3	k
knize	kniha	k1gFnSc3	kniha
Tree	Tre	k1gInSc2	Tre
and	and	k?	and
Leaf	Leaf	k1gInSc1	Leaf
(	(	kIx(	(
<g/>
Strom	strom	k1gInSc1	strom
a	a	k8xC	a
list	list	k1gInSc1	list
<g/>
)	)	kIx)	)
s	s	k7c7	s
podrobnostmi	podrobnost	k1gFnPc7	podrobnost
ke	k	k7c3	k
kompozici	kompozice	k1gFnSc3	kompozice
a	a	k8xC	a
historii	historie	k1gFnSc6	historie
tvorby	tvorba	k1gFnSc2	tvorba
děl	dít	k5eAaImAgMnS	dít
Leaf	Leaf	k1gMnSc1	Leaf
by	by	k9	by
<g />
.	.	kIx.	.
</s>
<s>
Niggle	Niggle	k1gNnSc1	Niggle
(	(	kIx(	(
<g/>
Nimralův	nimralův	k2eAgInSc1d1	nimralův
list	list	k1gInSc1	list
<g/>
)	)	kIx)	)
a	a	k8xC	a
On	on	k3xPp3gMnSc1	on
Fairy-Stories	Fairy-Stories	k1gMnSc1	Fairy-Stories
(	(	kIx(	(
<g/>
O	o	k7c6	o
pohádkách	pohádka	k1gFnPc6	pohádka
<g/>
)	)	kIx)	)
1966	[number]	k4	1966
příspěvky	příspěvek	k1gInPc4	příspěvek
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
Jerusalem	Jerusalem	k1gInSc4	Jerusalem
Bible	bible	k1gFnSc2	bible
–	–	k?	–
překlad	překlad	k1gInSc1	překlad
a	a	k8xC	a
poznámky	poznámka	k1gFnPc1	poznámka
1966	[number]	k4	1966
předmluva	předmluva	k1gFnSc1	předmluva
ke	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
edici	edice	k1gFnSc3	edice
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
s	s	k7c7	s
Tolkienovým	Tolkienův	k2eAgInSc7d1	Tolkienův
komentářem	komentář	k1gInSc7	komentář
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
reakce	reakce	k1gFnPc4	reakce
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
motivaci	motivace	k1gFnSc4	motivace
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
jeho	jeho	k3xOp3gInSc2	jeho
názoru	názor	k1gInSc2	názor
na	na	k7c6	na
alegorii	alegorie	k1gFnSc6	alegorie
<g/>
.	.	kIx.	.
1966	[number]	k4	1966
Tolkien	Tolkien	k2eAgInSc1d1	Tolkien
on	on	k3xPp3gInSc1	on
Tolkien	Tolkien	k1gInSc1	Tolkien
(	(	kIx(	(
<g/>
autobiografie	autobiografie	k1gFnSc1	autobiografie
<g/>
)	)	kIx)	)
1975	[number]	k4	1975
Sir	sir	k1gMnSc1	sir
Gawain	Gawain	k1gMnSc1	Gawain
and	and	k?	and
the	the	k?	the
Green	Green	k2eAgMnSc1d1	Green
Knight	Knight	k1gMnSc1	Knight
<g/>
,	,	kIx,	,
Pearl	Pearl	k1gMnSc1	Pearl
<g/>
,	,	kIx,	,
and	and	k?	and
Sir	sir	k1gMnSc1	sir
Orfeo	Orfeo	k1gMnSc1	Orfeo
–	–	k?	–
překlad	překlad	k1gInSc1	překlad
historických	historický	k2eAgNnPc2d1	historické
děl	dělo	k1gNnPc2	dělo
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
angličtiny	angličtina	k1gFnSc2	angličtina
(	(	kIx(	(
<g/>
Pearl	Pearl	k1gInSc1	Pearl
je	být	k5eAaImIp3nS	být
báseň	báseň	k1gFnSc4	báseň
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Sir	sir	k1gMnSc1	sir
<g />
.	.	kIx.	.
</s>
<s>
Orfeo	Orfeo	k6eAd1	Orfeo
výpravná	výpravný	k2eAgFnSc1d1	výpravná
báseň	báseň	k1gFnSc1	báseň
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydali	vydat	k5eAaPmAgMnP	vydat
George	George	k1gFnSc7	George
Allen	Allen	k1gMnSc1	Allen
&	&	k?	&
Unwin	Unwin	k1gInSc1	Unwin
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
;	;	kIx,	;
Houghton	Houghton	k1gInSc1	Houghton
Mifflin	Mifflin	k1gInSc1	Mifflin
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
<g/>
.	.	kIx.	.
1976	[number]	k4	1976
The	The	k1gFnSc2	The
Father	Fathra	k1gFnPc2	Fathra
Christmas	Christmasa	k1gFnPc2	Christmasa
Letters	Letters	k1gInSc1	Letters
(	(	kIx(	(
<g/>
Dopisy	dopis	k1gInPc1	dopis
Děda	děd	k1gMnSc2	děd
Mráze	Mráz	k1gMnSc2	Mráz
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
pohlednic	pohlednice	k1gFnPc2	pohlednice
a	a	k8xC	a
krátkých	krátký	k2eAgInPc2d1	krátký
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
posílal	posílat	k5eAaImAgMnS	posílat
svým	svůj	k3xOyFgInSc7	svůj
dětem	dítě	k1gFnPc3	dítě
1977	[number]	k4	1977
The	The	k1gMnSc1	The
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
(	(	kIx(	(
<g/>
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
320	[number]	k4	320
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
1979	[number]	k4	1979
Pictures	Picturesa	k1gFnPc2	Picturesa
by	by	k9	by
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkien	k1gInSc1	Tolkien
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
dříve	dříve	k6eAd2	dříve
<g />
.	.	kIx.	.
</s>
<s>
vyšlých	vyšlý	k2eAgFnPc2d1	vyšlá
maleb	malba	k1gFnPc2	malba
a	a	k8xC	a
kreseb	kresba	k1gFnPc2	kresba
v	v	k7c6	v
kalendářích	kalendář	k1gInPc6	kalendář
a	a	k8xC	a
knihách	kniha	k1gFnPc6	kniha
1980	[number]	k4	1980
Unfinished	Unfinished	k1gMnSc1	Unfinished
Tales	Tales	k1gMnSc1	Tales
of	of	k?	of
Númenor	Númenor	k1gMnSc1	Númenor
and	and	k?	and
Middle-earth	Middlearth	k1gInSc1	Middle-earth
(	(	kIx(	(
<g/>
Nedokončené	dokončený	k2eNgInPc1d1	nedokončený
příběhy	příběh	k1gInPc1	příběh
Númenoru	Númenor	k1gInSc2	Númenor
a	a	k8xC	a
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
512	[number]	k4	512
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
1981	[number]	k4	1981
The	The	k1gFnPc2	The
Letters	Letters	k1gInSc4	Letters
of	of	k?	of
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkien	k1gInSc1	Tolkien
–	–	k?	–
vydal	vydat	k5eAaPmAgInS	vydat
Christopher	Christophra	k1gFnPc2	Christophra
Tolkien	Tolkien	k1gInSc4	Tolkien
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Humphreyho	Humphrey	k1gMnSc2	Humphrey
Carpentera	Carpenter	k1gMnSc2	Carpenter
1981	[number]	k4	1981
The	The	k1gFnSc2	The
Old	Olda	k1gFnPc2	Olda
English	English	k1gInSc1	English
Exodus	Exodus	k1gInSc1	Exodus
Text	text	k1gInSc1	text
1982	[number]	k4	1982
Finn	Finn	k1gMnSc1	Finn
and	and	k?	and
Hengest	Hengest	k1gMnSc1	Hengest
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Fragment	fragment	k1gInSc1	fragment
and	and	k?	and
the	the	k?	the
Episode	Episod	k1gInSc5	Episod
1982	[number]	k4	1982
Mr	Mr	k1gFnPc3	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bliss	Bliss	k1gInSc1	Bliss
(	(	kIx(	(
<g/>
Pan	Pan	k1gMnSc1	Pan
Blahoš	Blahoš	k1gMnSc1	Blahoš
<g/>
,	,	kIx,	,
Argo	Argo	k1gMnSc1	Argo
<g/>
,	,	kIx,	,
připravováno	připravován	k2eAgNnSc4d1	připravováno
<g/>
)	)	kIx)	)
–	–	k?	–
příběh	příběh	k1gInSc1	příběh
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
o	o	k7c6	o
nekonvenční	konvenční	k2eNgFnSc6d1	nekonvenční
cestě	cesta	k1gFnSc6	cesta
pana	pan	k1gMnSc2	pan
Blahoše	Blahoš	k1gMnSc2	Blahoš
1983	[number]	k4	1983
The	The	k1gFnSc2	The
Monsters	Monstersa	k1gFnPc2	Monstersa
and	and	k?	and
the	the	k?	the
Critics	Critics	k1gInSc1	Critics
(	(	kIx(	(
<g/>
Netvoři	netvor	k1gMnPc1	netvor
a	a	k8xC	a
kritikové	kritik	k1gMnPc1	kritik
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
eseje	esej	k1gFnPc1	esej
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
270	[number]	k4	270
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
sbírka	sbírka	k1gFnSc1	sbírka
esejů	esej	k1gInPc2	esej
obsahující	obsahující	k2eAgFnSc1d1	obsahující
následující	následující	k2eAgFnSc1d1	následující
práce	práce	k1gFnSc1	práce
<g/>
:	:	kIx,	:
Beowulf	Beowulf	k1gInSc1	Beowulf
<g/>
:	:	kIx,	:
the	the	k?	the
Monsters	Monsters	k1gInSc1	Monsters
and	and	k?	and
the	the	k?	the
Critics	Critics	k1gInSc1	Critics
(	(	kIx(	(
<g/>
Béowulf	Béowulf	k1gInSc1	Béowulf
<g/>
:	:	kIx,	:
Netvoři	netvor	k1gMnPc1	netvor
a	a	k8xC	a
kritikové	kritik	k1gMnPc1	kritik
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
<g/>
)	)	kIx)	)
–	–	k?	–
přednáška	přednáška	k1gFnSc1	přednáška
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
On	on	k3xPp3gMnSc1	on
Translating	Translating	k1gInSc4	Translating
Beowulf	Beowulf	k1gInSc4	Beowulf
(	(	kIx(	(
<g/>
O	o	k7c6	o
překládání	překládání	k1gNnSc6	překládání
Béowulfa	Béowulf	k1gMnSc2	Béowulf
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
str	str	kA	str
<g/>
.	.	kIx.	.
61	[number]	k4	61
<g/>
–	–	k?	–
<g/>
86	[number]	k4	86
<g/>
)	)	kIx)	)
–	–	k?	–
text	text	k1gInSc1	text
původně	původně	k6eAd1	původně
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Prefatory	Prefator	k1gInPc4	Prefator
Remarks	Remarks	k1gInSc1	Remarks
on	on	k3xPp3gMnSc1	on
Prose	prosa	k1gFnSc3	prosa
Translation	Translation	k1gInSc1	Translation
of	of	k?	of
'	'	kIx"	'
<g/>
Beowulf	Beowulf	k1gMnSc1	Beowulf
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
Úvodní	úvodní	k2eAgFnSc1d1	úvodní
poznámka	poznámka	k1gFnSc1	poznámka
k	k	k7c3	k
prozaickému	prozaický	k2eAgInSc3d1	prozaický
překladu	překlad	k1gInSc3	překlad
"	"	kIx"	"
<g/>
Béowulfa	Béowulf	k1gMnSc4	Béowulf
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
knihy	kniha	k1gFnSc2	kniha
Beowulf	Beowulf	k1gInSc1	Beowulf
and	and	k?	and
the	the	k?	the
<g />
.	.	kIx.	.
</s>
<s>
Finnesburg	Finnesburg	k1gInSc1	Finnesburg
Fragment	fragment	k1gInSc1	fragment
(	(	kIx(	(
<g/>
Béowulf	Béowulf	k1gInSc1	Béowulf
a	a	k8xC	a
Finnsburský	Finnsburský	k2eAgInSc1d1	Finnsburský
zlomek	zlomek	k1gInSc1	zlomek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nového	nový	k2eAgNnSc2d1	nové
vydání	vydání	k1gNnSc2	vydání
překladu	překlad	k1gInSc2	překlad
C.	C.	kA	C.
L.	L.	kA	L.
Wrenna	Wrenna	k1gFnSc1	Wrenna
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
On	on	k3xPp3gMnSc1	on
Fairy-Stories	Fairy-Stories	k1gMnSc1	Fairy-Stories
(	(	kIx(	(
<g/>
O	o	k7c6	o
pohádkách	pohádka	k1gFnPc6	pohádka
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
131	[number]	k4	131
<g/>
–	–	k?	–
<g/>
182	[number]	k4	182
<g/>
)	)	kIx)	)
–	–	k?	–
text	text	k1gInSc1	text
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
celistvosti	celistvost	k1gFnSc6	celistvost
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
A	a	k8xC	a
Secret	Secret	k1gMnSc1	Secret
<g />
.	.	kIx.	.
</s>
<s>
Vice	vika	k1gFnSc3	vika
(	(	kIx(	(
<g/>
Tajná	tajný	k2eAgFnSc1d1	tajná
neřest	neřest	k1gFnSc1	neřest
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
221	[number]	k4	221
<g/>
–	–	k?	–
<g/>
248	[number]	k4	248
<g/>
)	)	kIx)	)
–	–	k?	–
esej	esej	k1gFnSc1	esej
o	o	k7c6	o
koníčku	koníček	k1gInSc6	koníček
vytváření	vytváření	k1gNnPc2	vytváření
vlastních	vlastní	k2eAgInPc2d1	vlastní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
proslovena	prosloven	k2eAgFnSc1d1	proslovena
na	na	k7c6	na
nějaké	nějaký	k3yIgFnSc6	nějaký
filologické	filologický	k2eAgFnSc6d1	filologická
společnosti	společnost	k1gFnSc6	společnost
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
English	English	k1gMnSc1	English
and	and	k?	and
Welsh	Welsh	k1gMnSc1	Welsh
(	(	kIx(	(
<g/>
Angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
velština	velština	k1gFnSc1	velština
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
183	[number]	k4	183
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
220	[number]	k4	220
<g/>
)	)	kIx)	)
–	–	k?	–
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Donnellovy	Donnellův	k2eAgFnSc2d1	Donnellova
řady	řada	k1gFnSc2	řada
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
proslovená	proslovený	k2eAgFnSc1d1	proslovená
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1955	[number]	k4	1955
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
The	The	k1gFnSc1	The
History	Histor	k1gInPc1	Histor
of	of	k?	of
Middle-earth	Middlearth	k1gInSc1	Middle-earth
–	–	k?	–
obsáhlý	obsáhlý	k2eAgInSc1d1	obsáhlý
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
Tolkienovy	Tolkienův	k2eAgFnSc2d1	Tolkienova
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgMnPc4d1	zabývající
se	se	k3xPyFc4	se
nejrůznějšími	různý	k2eAgInPc7d3	nejrůznější
aspekty	aspekt	k1gInPc4	aspekt
jeho	jeho	k3xOp3gInSc2	jeho
mythologického	mythologický	k2eAgInSc2d1	mythologický
světa	svět	k1gInSc2	svět
1983	[number]	k4	1983
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Book	Book	k1gInSc1	Book
of	of	k?	of
Lost	Lost	k2eAgInSc1d1	Lost
Tales	Tales	k1gInSc1	Tales
1	[number]	k4	1
(	(	kIx(	(
<g/>
Kniha	kniha	k1gFnSc1	kniha
ztracených	ztracený	k2eAgFnPc2d1	ztracená
pověstí	pověst	k1gFnPc2	pověst
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Winston	Winston	k1gInSc1	Winston
Smith	Smith	k1gInSc1	Smith
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
The	The	k1gMnSc1	The
Book	Book	k1gMnSc1	Book
of	of	k?	of
Lost	Lost	k1gMnSc1	Lost
Tales	Tales	k1gMnSc1	Tales
2	[number]	k4	2
1985	[number]	k4	1985
The	The	k1gFnPc2	The
Lays	Laysa	k1gFnPc2	Laysa
of	of	k?	of
Beleriand	Belerianda	k1gFnPc2	Belerianda
1986	[number]	k4	1986
The	The	k1gFnPc2	The
Shaping	Shaping	k1gInSc4	Shaping
of	of	k?	of
Middle-earth	Middlearth	k1gInSc1	Middle-earth
1987	[number]	k4	1987
The	The	k1gFnSc2	The
Lost	Losta	k1gFnPc2	Losta
Road	Road	k1gMnSc1	Road
and	and	k?	and
Other	Other	k1gInSc1	Other
Writings	Writings	k1gInSc1	Writings
1988	[number]	k4	1988
The	The	k1gFnSc2	The
Return	Returna	k1gFnPc2	Returna
of	of	k?	of
<g />
.	.	kIx.	.
</s>
<s>
the	the	k?	the
Shadow	Shadow	k1gFnSc1	Shadow
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
History	Histor	k1gInPc1	Histor
of	of	k?	of
The	The	k1gFnSc2	The
Lord	lord	k1gMnSc1	lord
of	of	k?	of
the	the	k?	the
Rings	Rings	k1gInSc1	Rings
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
The	The	k1gMnSc1	The
Treason	Treason	k1gMnSc1	Treason
of	of	k?	of
Isengard	Isengard	k1gMnSc1	Isengard
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
History	Histor	k1gInPc1	Histor
of	of	k?	of
The	The	k1gFnSc2	The
Lord	lord	k1gMnSc1	lord
of	of	k?	of
the	the	k?	the
Rings	Rings	k1gInSc1	Rings
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
)	)	kIx)	)
1990	[number]	k4	1990
The	The	k1gMnSc1	The
War	War	k1gMnSc1	War
of	of	k?	of
the	the	k?	the
Ring	ring	k1gInSc1	ring
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
History	Histor	k1gInPc1	Histor
of	of	k?	of
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Lord	lord	k1gMnSc1	lord
of	of	k?	of
the	the	k?	the
Rings	Rings	k1gInSc1	Rings
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
Sauron	Sauron	k1gMnSc1	Sauron
Defeated	Defeated	k1gMnSc1	Defeated
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
History	Histor	k1gInPc1	Histor
of	of	k?	of
The	The	k1gFnSc2	The
Lord	lord	k1gMnSc1	lord
of	of	k?	of
the	the	k?	the
Rings	Rings	k1gInSc1	Rings
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
)	)	kIx)	)
1993	[number]	k4	1993
Morgoth	Morgotha	k1gFnPc2	Morgotha
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Ring	ring	k1gInSc1	ring
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Later	Latra	k1gFnPc2	Latra
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
)	)	kIx)	)
1994	[number]	k4	1994
The	The	k1gFnSc2	The
<g />
.	.	kIx.	.
</s>
<s>
War	War	k?	War
of	of	k?	of
the	the	k?	the
Jewels	Jewels	k1gInSc1	Jewels
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Later	Latra	k1gFnPc2	Latra
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
)	)	kIx)	)
1996	[number]	k4	1996
The	The	k1gMnSc1	The
Peoples	Peoples	k1gMnSc1	Peoples
of	of	k?	of
Middle-earth	Middlearth	k1gInSc1	Middle-earth
Index	index	k1gInSc1	index
–	–	k?	–
vydán	vydat	k5eAaPmNgInS	vydat
2002	[number]	k4	2002
1995	[number]	k4	1995
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkina	k1gFnPc2	Tolkina
<g/>
:	:	kIx,	:
Artist	Artist	k1gInSc1	Artist
and	and	k?	and
Illustrator	Illustrator	k1gInSc1	Illustrator
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
Tolkienových	Tolkienův	k2eAgFnPc2d1	Tolkienova
ilustrací	ilustrace	k1gFnPc2	ilustrace
1998	[number]	k4	1998
Roverandom	Roverandom	k1gInSc1	Roverandom
(	(	kIx(	(
<g/>
Tulák	tulák	k1gMnSc1	tulák
Rover	rover	k1gMnSc1	rover
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
–	–	k?	–
příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
Tolkien	Tolkien	k1gInSc1	Tolkien
vyprávěl	vyprávět	k5eAaImAgInS	vyprávět
svému	svůj	k3xOyFgMnSc3	svůj
synovi	syn	k1gMnSc3	syn
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
se	se	k3xPyFc4	se
ztratila	ztratit	k5eAaPmAgFnS	ztratit
hračka	hračka	k1gFnSc1	hračka
–	–	k?	–
černobílý	černobílý	k2eAgMnSc1d1	černobílý
psík	psík	k1gMnSc1	psík
Rover	rover	k1gMnSc1	rover
2002	[number]	k4	2002
A	a	k8xC	a
Tolkien	Tolkien	k1gInSc1	Tolkien
Miscellany	Miscellana	k1gFnSc2	Miscellana
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
dříve	dříve	k6eAd2	dříve
publikovaného	publikovaný	k2eAgInSc2d1	publikovaný
materiálu	materiál	k1gInSc2	materiál
2002	[number]	k4	2002
Beowulf	Beowulf	k1gMnSc1	Beowulf
and	and	k?	and
the	the	k?	the
Critics	Critics	k1gInSc1	Critics
(	(	kIx(	(
<g/>
Béowulf	Béowulf	k1gInSc1	Béowulf
a	a	k8xC	a
kritikové	kritik	k1gMnPc1	kritik
<g/>
)	)	kIx)	)
–	–	k?	–
editovaná	editovaný	k2eAgFnSc1d1	editovaná
Michaelem	Michael	k1gMnSc7	Michael
<g />
.	.	kIx.	.
</s>
<s>
D.	D.	kA	D.
C.	C.	kA	C.
Droutem	Drout	k1gMnSc7	Drout
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Beowulf	Beowulf	k1gInSc1	Beowulf
<g/>
:	:	kIx,	:
netvoři	netvor	k1gMnPc1	netvor
a	a	k8xC	a
kritikové	kritik	k1gMnPc1	kritik
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
edicemi	edice	k1gFnPc7	edice
dvou	dva	k4xCgInPc2	dva
konceptů	koncept	k1gInPc2	koncept
delší	dlouhý	k2eAgFnSc2d2	delší
eseje	esej	k1gFnSc2	esej
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
zhuštěním	zhuštění	k1gNnSc7	zhuštění
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Names	Names	k1gInSc1	Names
in	in	k?	in
The	The	k1gFnSc2	The
Lord	lord	k1gMnSc1	lord
of	of	k?	of
the	the	k?	the
Rings	Rings	k1gInSc1	Rings
(	(	kIx(	(
<g/>
plná	plný	k2eAgFnSc1d1	plná
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
–	–	k?	–
publikována	publikovat	k5eAaBmNgFnS	publikovat
Waynem	Wayn	k1gMnSc7	Wayn
Hammondem	Hammond	k1gMnSc7	Hammond
a	a	k8xC	a
Christinou	Christin	k2eAgFnSc7d1	Christina
Skullovou	Skullová	k1gFnSc7	Skullová
v	v	k7c6	v
The	The	k1gFnSc6	The
Lord	lord	k1gMnSc1	lord
of	of	k?	of
the	the	k?	the
Rings	Rings	k1gInSc1	Rings
<g/>
:	:	kIx,	:
A	a	k9	a
Reader	Reader	k1gInSc1	Reader
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Companion	Companion	k1gInSc1	Companion
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
ji	on	k3xPp3gFnSc4	on
Tolkien	Tolkien	k1gInSc1	Tolkien
pro	pro	k7c4	pro
překladatele	překladatel	k1gMnSc4	překladatel
Pána	pán	k1gMnSc4	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
redigovaná	redigovaný	k2eAgFnSc1d1	redigovaná
verze	verze	k1gFnSc1	verze
od	od	k7c2	od
Jareda	Jared	k1gMnSc2	Jared
Lobdella	Lobdell	k1gMnSc2	Lobdell
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
v	v	k7c4	v
A	a	k9	a
Tolkien	Tolkien	k2eAgInSc4d1	Tolkien
Compass	Compass	k1gInSc4	Compass
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
The	The	k1gFnPc2	The
Children	Childrna	k1gFnPc2	Childrna
of	of	k?	of
Húrin	Húrina	k1gFnPc2	Húrina
(	(	kIx(	(
<g/>
Húrinovy	Húrinův	k2eAgFnPc1d1	Húrinův
děti	dítě	k1gFnPc1	dítě
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2007	[number]	k4	2007
2009	[number]	k4	2009
The	The	k1gFnPc2	The
Legend	legenda	k1gFnPc2	legenda
of	of	k?	of
Sigurd	Sigurd	k1gMnSc1	Sigurd
and	and	k?	and
Gudrún	Gudrún	k1gMnSc1	Gudrún
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
Sigurdovi	Sigurda	k1gMnSc6	Sigurda
a	a	k8xC	a
Gudrún	Gudrún	k1gInSc4	Gudrún
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc4	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
–	–	k?	–
Tolkienovo	Tolkienův	k2eAgNnSc4d1	Tolkienovo
podání	podání	k1gNnSc4	podání
staroseverské	staroseverský	k2eAgFnSc2d1	Staroseverská
Ságy	sága	k1gFnSc2	sága
o	o	k7c6	o
Völsunzích	Völsung	k1gInPc6	Völsung
2013	[number]	k4	2013
The	The	k1gFnPc2	The
Fall	Falla	k1gFnPc2	Falla
of	of	k?	of
Arthur	Arthura	k1gFnPc2	Arthura
(	(	kIx(	(
<g/>
Artušův	Artušův	k2eAgInSc1d1	Artušův
pád	pád	k1gInSc1	pád
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc4	Argo
<g/>
,	,	kIx,	,
připravováno	připravován	k2eAgNnSc4d1	připravováno
<g/>
)	)	kIx)	)
-	-	kIx~	-
Tolkienovo	Tolkienův	k2eAgNnSc1d1	Tolkienovo
podání	podání	k1gNnSc1	podání
legendy	legenda	k1gFnSc2	legenda
o	o	k7c6	o
králi	král	k1gMnSc6	král
Artušovi	Artuš	k1gMnSc6	Artuš
2014	[number]	k4	2014
Beowulf	Beowulf	k1gInSc1	Beowulf
<g/>
:	:	kIx,	:
A	a	k9	a
Translation	Translation	k1gInSc1	Translation
and	and	k?	and
Commentary	Commentara	k1gFnSc2	Commentara
<g/>
,	,	kIx,	,
together	togethra	k1gFnPc2	togethra
with	witha	k1gFnPc2	witha
Sellic	Sellice	k1gFnPc2	Sellice
Spell	Spell	k1gInSc1	Spell
-	-	kIx~	-
komentáře	komentář	k1gInPc1	komentář
k	k	k7c3	k
Tolkienovu	Tolkienův	k2eAgNnSc3d1	Tolkienovo
překládání	překládání	k1gNnSc3	překládání
Beowulfa	Beowulf	k1gMnSc2	Beowulf
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Tolkienovou	Tolkienový	k2eAgFnSc7d1	Tolkienový
vlastní	vlastní	k2eAgFnSc7d1	vlastní
legendou	legenda	k1gFnSc7	legenda
<g/>
,	,	kIx,	,
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Sellic	Sellice	k1gFnPc2	Sellice
Spell	Spell	k1gInSc4	Spell
</s>
