<p>
<s>
Holub	Holub	k1gMnSc1	Holub
písmenkový	písmenkový	k2eAgMnSc1d1	písmenkový
<g/>
,	,	kIx,	,
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
holoubek	holoubek	k1gMnSc1	holoubek
koroptví	koroptev	k1gFnPc2	koroptev
(	(	kIx(	(
<g/>
Geophaps	Geophaps	k1gInSc1	Geophaps
scripta	script	k1gInSc2	script
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Petrophassa	Petrophassa	k1gFnSc1	Petrophassa
scripta	scripta	k1gFnSc1	scripta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
měkkozobého	měkkozobý	k2eAgMnSc2d1	měkkozobý
ptáka	pták	k1gMnSc2	pták
náležící	náležící	k2eAgFnSc7d1	náležící
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
holubovití	holubovitý	k2eAgMnPc1d1	holubovitý
(	(	kIx(	(
<g/>
Columbidae	Columbidae	k1gNnSc7	Columbidae
<g/>
)	)	kIx)	)
a	a	k8xC	a
rodu	rod	k1gInSc2	rod
Geophaps	Geophapsa	k1gFnPc2	Geophapsa
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgMnSc4	tento
holuba	holub	k1gMnSc4	holub
popsal	popsat	k5eAaPmAgMnS	popsat
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
zoolog	zoolog	k1gMnSc1	zoolog
Coenraad	Coenraad	k1gInSc1	Coenraad
Jacob	Jacoba	k1gFnPc2	Jacoba
Temminck	Temmincko	k1gNnPc2	Temmincko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
<g/>
.	.	kIx.	.
</s>
<s>
Zaznamenány	zaznamenán	k2eAgFnPc1d1	zaznamenána
byly	být	k5eAaImAgFnP	být
celkem	celkem	k6eAd1	celkem
dva	dva	k4xCgInPc4	dva
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
:	:	kIx,	:
Geophaps	Geophaps	k1gInSc4	Geophaps
scripta	scripto	k1gNnSc2	scripto
peninsulae	peninsulae	k1gFnPc2	peninsulae
a	a	k8xC	a
Geophaps	Geophapsa	k1gFnPc2	Geophapsa
scripta	script	k1gMnSc4	script
scripta	script	k1gMnSc4	script
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
blízce	blízce	k6eAd1	blízce
příbuzný	příbuzný	k1gMnSc1	příbuzný
jinému	jiný	k2eAgMnSc3d1	jiný
holubovi	holub	k1gMnSc3	holub
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
holubovi	holub	k1gMnSc3	holub
koroptvímu	koroptví	k2eAgMnSc3d1	koroptví
(	(	kIx(	(
<g/>
Geophaps	Geophaps	k1gInSc4	Geophaps
smithii	smithie	k1gFnSc4	smithie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
v	v	k7c6	v
blahovičníkových	blahovičníkový	k2eAgFnPc6d1	blahovičníkový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
jiných	jiný	k2eAgInPc6d1	jiný
otevřených	otevřený	k2eAgInPc6d1	otevřený
porostech	porost	k1gInPc6	porost
australské	australský	k2eAgFnSc2d1	australská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
severovýchodních	severovýchodní	k2eAgFnPc6d1	severovýchodní
částech	část	k1gFnPc6	část
Austrálie	Austrálie	k1gFnSc2	Austrálie
na	na	k7c4	na
území	území	k1gNnSc4	území
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
asi	asi	k9	asi
1	[number]	k4	1
160	[number]	k4	160
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
poddruh	poddruh	k1gInSc1	poddruh
peninsulae	peninsulae	k6eAd1	peninsulae
obývá	obývat	k5eAaImIp3nS	obývat
severovýchod	severovýchod	k1gInSc1	severovýchod
australského	australský	k2eAgInSc2d1	australský
státu	stát	k1gInSc2	stát
Queensland	Queenslanda	k1gFnPc2	Queenslanda
<g/>
,	,	kIx,	,
subspecie	subspecie	k1gFnSc1	subspecie
scripta	scripto	k1gNnSc2	scripto
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
centrálním	centrální	k2eAgInSc6d1	centrální
Queenslandu	Queensland	k1gInSc6	Queensland
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
po	po	k7c4	po
Nový	nový	k2eAgInSc4d1	nový
Jižní	jižní	k2eAgInSc4d1	jižní
Wales	Wales	k1gInSc4	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Lokálně	lokálně	k6eAd1	lokálně
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
může	moct	k5eAaImIp3nS	moct
provádět	provádět	k5eAaImF	provádět
migrace	migrace	k1gFnSc1	migrace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Holub	Holub	k1gMnSc1	Holub
písmenkový	písmenkový	k2eAgMnSc1d1	písmenkový
měří	měřit	k5eAaImIp3nS	měřit
26	[number]	k4	26
až	až	k9	až
32	[number]	k4	32
cm	cm	kA	cm
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
200	[number]	k4	200
až	až	k9	až
225	[number]	k4	225
g.	g.	k?	g.
Poddruh	poddruh	k1gInSc1	poddruh
scripta	script	k1gMnSc2	script
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
subspecie	subspecie	k1gFnSc1	subspecie
peninsulae	peninsulae	k1gFnSc1	peninsulae
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
však	však	k9	však
tyto	tento	k3xDgInPc4	tento
dva	dva	k4xCgInPc4	dva
poddruhy	poddruh	k1gInPc4	poddruh
vzhledově	vzhledově	k6eAd1	vzhledově
podobné	podobný	k2eAgInPc4d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
si	se	k3xPyFc3	se
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
,	,	kIx,	,
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
dimorfismus	dimorfismus	k1gInSc4	dimorfismus
není	být	k5eNaImIp3nS	být
výrazný	výrazný	k2eAgInSc1d1	výrazný
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
šedohnědé	šedohnědý	k2eAgNnSc1d1	šedohnědé
s	s	k7c7	s
tmavými	tmavý	k2eAgNnPc7d1	tmavé
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
na	na	k7c6	na
spodku	spodek	k1gInSc6	spodek
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
holub	holub	k1gMnSc1	holub
zabarven	zabarven	k2eAgMnSc1d1	zabarven
šedě	šedě	k6eAd1	šedě
a	a	k8xC	a
bíle	bíle	k6eAd1	bíle
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
očí	oko	k1gNnPc2	oko
se	se	k3xPyFc4	se
u	u	k7c2	u
poddruhu	poddruh	k1gInSc2	poddruh
peninsulae	peninsulae	k6eAd1	peninsulae
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
kůže	kůže	k1gFnSc1	kůže
mající	mající	k2eAgFnSc4d1	mající
barvu	barva	k1gFnSc4	barva
do	do	k7c2	do
oranžova	oranžovo	k1gNnSc2	oranžovo
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
dalším	další	k2eAgInSc7d1	další
rozpoznávacím	rozpoznávací	k2eAgInSc7d1	rozpoznávací
znakem	znak	k1gInSc7	znak
mezi	mezi	k7c7	mezi
subspeciemi	subspecie	k1gFnPc7	subspecie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
druhý	druhý	k4xOgInSc1	druhý
poddruh	poddruh	k1gInSc1	poddruh
má	mít	k5eAaImIp3nS	mít
tuto	tento	k3xDgFnSc4	tento
kůži	kůže	k1gFnSc4	kůže
spíše	spíše	k9	spíše
šedivou	šedivý	k2eAgFnSc4d1	šedivá
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
výrazný	výrazný	k2eAgInSc4d1	výrazný
pruhovaný	pruhovaný	k2eAgInSc4d1	pruhovaný
vzor	vzor	k1gInSc4	vzor
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
záměna	záměna	k1gFnSc1	záměna
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
druhy	druh	k1gInPc7	druh
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
s	s	k7c7	s
holubem	holub	k1gMnSc7	holub
bronzovokřídlým	bronzovokřídlý	k2eAgNnSc7d1	bronzovokřídlý
(	(	kIx(	(
<g/>
Phaps	Phaps	k1gInSc1	Phaps
chalcoptera	chalcopter	k1gMnSc2	chalcopter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
jídelníčku	jídelníček	k1gInSc2	jídelníček
tohoto	tento	k3xDgMnSc2	tento
holuba	holub	k1gMnSc2	holub
tvoří	tvořit	k5eAaImIp3nS	tvořit
semena	semeno	k1gNnPc1	semeno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
živit	živit	k5eAaImF	živit
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
například	například	k6eAd1	například
také	také	k9	také
hmyzožravě	hmyzožravě	k6eAd1	hmyzožravě
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
vodního	vodní	k2eAgInSc2d1	vodní
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
kterého	který	k3yQgInSc2	který
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
alespoň	alespoň	k9	alespoň
malé	malý	k2eAgNnSc1d1	malé
otevřené	otevřený	k2eAgNnSc1d1	otevřené
prostranství	prostranství	k1gNnSc1	prostranství
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
poměrně	poměrně	k6eAd1	poměrně
tiše	tiš	k1gFnSc2	tiš
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
malé	malý	k2eAgFnPc4d1	malá
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
párech	pár	k1gInPc6	pár
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
probíhá	probíhat	k5eAaImIp3nS	probíhat
především	především	k9	především
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
období	období	k1gNnSc2	období
sucha	sucho	k1gNnSc2	sucho
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
dostatek	dostatek	k1gInSc4	dostatek
potravy	potrava	k1gFnSc2	potrava
−	−	k?	−
semen	semeno	k1gNnPc2	semeno
<g/>
,	,	kIx,	,
reprodukce	reprodukce	k1gFnSc2	reprodukce
jsou	být	k5eAaImIp3nP	být
nicméně	nicméně	k8xC	nicméně
ptáci	pták	k1gMnPc1	pták
schopni	schopen	k2eAgMnPc1d1	schopen
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
naklade	naklást	k5eAaPmIp3nS	naklást
dvě	dva	k4xCgNnPc4	dva
vajíčka	vajíčko	k1gNnPc4	vajíčko
do	do	k7c2	do
trávou	tráva	k1gFnSc7	tráva
vystlaného	vystlaný	k2eAgNnSc2d1	vystlané
hnízda	hnízdo	k1gNnSc2	hnízdo
umístěného	umístěný	k2eAgNnSc2d1	umístěné
mírně	mírně	k6eAd1	mírně
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
za	za	k7c4	za
17	[number]	k4	17
dní	den	k1gInPc2	den
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
rychlý	rychlý	k2eAgInSc1d1	rychlý
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
za	za	k7c4	za
9	[number]	k4	9
dní	den	k1gInPc2	den
již	již	k6eAd1	již
hnízdo	hnízdo	k1gNnSc4	hnízdo
mohou	moct	k5eAaImIp3nP	moct
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc4d1	jiný
zdroje	zdroj	k1gInPc4	zdroj
nicméně	nicméně	k8xC	nicméně
uvádějí	uvádět	k5eAaImIp3nP	uvádět
14	[number]	k4	14
až	až	k9	až
21	[number]	k4	21
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
není	být	k5eNaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
jedné	jeden	k4xCgFnSc2	jeden
generace	generace	k1gFnSc2	generace
je	být	k5eAaImIp3nS	být
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Status	status	k1gInSc1	status
ohrožení	ohrožení	k1gNnSc2	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
pro	pro	k7c4	pro
holuba	holub	k1gMnSc4	holub
písmenkového	písmenkový	k2eAgMnSc4d1	písmenkový
představuje	představovat	k5eAaImIp3nS	představovat
ztráta	ztráta	k1gFnSc1	ztráta
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
lov	lov	k1gInSc1	lov
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
nepůvodních	původní	k2eNgMnPc2d1	nepůvodní
predátorů	predátor	k1gMnPc2	predátor
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
svaz	svaz	k1gInSc4	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2017	[number]	k4	2017
populaci	populace	k1gFnSc6	populace
jako	jako	k8xC	jako
klesající	klesající	k2eAgFnSc7d1	klesající
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
neklesá	klesat	k5eNaImIp3nS	klesat
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
druh	druh	k1gInSc1	druh
zařazen	zařadit	k5eAaPmNgInS	zařadit
mezi	mezi	k7c4	mezi
zranitelné	zranitelný	k2eAgNnSc4d1	zranitelné
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
společně	společně	k6eAd1	společně
s	s	k7c7	s
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
jedinců	jedinec	k1gMnPc2	jedinec
a	a	k8xC	a
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
areálem	areál	k1gInSc7	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
svaz	svaz	k1gInSc1	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
vyhodnotil	vyhodnotit	k5eAaPmAgInS	vyhodnotit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
holub	holub	k1gMnSc1	holub
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
taxony	taxon	k1gInPc4	taxon
málo	málo	k6eAd1	málo
dotčené	dotčený	k2eAgInPc4d1	dotčený
<g/>
.	.	kIx.	.
</s>
<s>
Možnosti	možnost	k1gFnPc1	možnost
ochrany	ochrana	k1gFnSc2	ochrana
holubů	holub	k1gMnPc2	holub
písmenkových	písmenkový	k2eAgMnPc2d1	písmenkový
uvedené	uvedený	k2eAgInPc4d1	uvedený
na	na	k7c6	na
webu	web	k1gInSc6	web
Australian	Australian	k1gMnSc1	Australian
Governement	Governement	k1gMnSc1	Governement
(	(	kIx(	(
<g/>
zkoumán	zkoumán	k2eAgMnSc1d1	zkoumán
byl	být	k5eAaImAgMnS	být
poddruh	poddruh	k1gInSc4	poddruh
scripta	script	k1gInSc2	script
<g/>
)	)	kIx)	)
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
ochranu	ochrana	k1gFnSc4	ochrana
a	a	k8xC	a
obnovení	obnovení	k1gNnSc4	obnovení
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
odstranění	odstranění	k1gNnSc3	odstranění
nepůvodních	původní	k2eNgInPc2d1	nepůvodní
druhů	druh	k1gInPc2	druh
predátorů	predátor	k1gMnPc2	predátor
<g/>
,	,	kIx,	,
určení	určení	k1gNnSc3	určení
velikostí	velikost	k1gFnPc2	velikost
populace	populace	k1gFnSc2	populace
či	či	k8xC	či
další	další	k2eAgInPc1d1	další
výzkumy	výzkum	k1gInPc1	výzkum
ohledně	ohledně	k7c2	ohledně
chování	chování	k1gNnSc2	chování
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
holub	holub	k1gMnSc1	holub
písmenkový	písmenkový	k2eAgMnSc1d1	písmenkový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Geophaps	Geophapsa	k1gFnPc2	Geophapsa
scripta	script	k1gInSc2	script
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
