<s>
Rozmnožování	rozmnožování	k1gNnSc1
probíhá	probíhat	k5eAaImIp3nS
především	především	k9
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
období	období	k1gNnSc2
sucha	sucho	k1gNnSc2
a	a	k8xC
tedy	tedy	k9
i	i	k9
dostatek	dostatek	k1gInSc4
potravy	potrava	k1gFnSc2
−	−	k?
semen	semeno	k1gNnPc2
<g/>
,	,	kIx,
reprodukce	reprodukce	k1gFnSc2
jsou	být	k5eAaImIp3nP
nicméně	nicméně	k8xC
ptáci	pták	k1gMnPc1
schopni	schopen	k2eAgMnPc1d1
během	během	k7c2
celého	celý	k2eAgInSc2d1
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>