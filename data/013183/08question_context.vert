<s>
Holub	Holub	k1gMnSc1
písmenkový	písmenkový	k2eAgMnSc1d1
<g/>
,	,	kIx,
také	také	k9
známý	známý	k2eAgMnSc1d1
jako	jako	k8xC
holoubek	holoubek	k1gMnSc1
koroptví	koroptev	k1gFnPc2
(	(	kIx(
<g/>
Geophaps	Geophaps	k1gInSc1
scripta	script	k1gInSc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
také	také	k9
Petrophassa	Petrophassa	k1gFnSc1
scripta	scripta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
měkkozobého	měkkozobý	k2eAgMnSc2d1
ptáka	pták	k1gMnSc2
náležící	náležící	k2eAgFnSc7d1
do	do	k7c2
čeledi	čeleď	k1gFnSc2
holubovití	holubovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Columbidae	Columbidae	k1gNnSc7
<g/>
)	)	kIx)
a	a	k8xC
rodu	rod	k1gInSc2
Geophaps	Geophapsa	k1gFnPc2
<g/>
.	.	kIx.
</s>