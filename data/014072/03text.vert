<s>
Osmium	osmium	k1gNnSc1
</s>
<s>
Osmium	osmium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
5	#num#	k4
<g/>
d	d	k?
<g/>
6	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
190	#num#	k4
</s>
<s>
Os	Os	kA
</s>
<s>
76	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Krystaly	krystal	k1gInPc1
osmia	osmium	k1gNnSc2
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Osmium	osmium	k1gNnSc1
<g/>
,	,	kIx,
Os	osa	k1gFnPc2
<g/>
,	,	kIx,
76	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Osmium	osmium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
d	d	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
stříbromodrý	stříbromodrý	k2eAgInSc1d1
kov	kov	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-04-2	7440-04-2	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
190,23	190,23	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
135	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
144	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
5	#num#	k4
<g/>
d	d	k?
<g/>
6	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
III	III	kA
<g/>
,	,	kIx,
IV	IV	kA
<g/>
,	,	kIx,
VI	VI	kA
<g/>
,	,	kIx,
VIII	VIII	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2,2	2,2	k4
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Hexagonální	hexagonální	k2eAgFnSc1d1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
22,59	22,59	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
(	(	kIx(
<g/>
20	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
při	při	k7c6
teplotě	teplota	k1gFnSc6
tání	tání	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
7,0	7,0	k4
</s>
<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
</s>
<s>
4	#num#	k4
940	#num#	k4
m	m	kA
<g/>
·	·	k?
<g/>
s	s	k7c7
<g/>
−	−	k?
<g/>
1	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
87,6	87,6	k4
W	W	kA
<g/>
·	·	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
·	·	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
3033	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
3	#num#	k4
306,15	306,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
5012	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
5	#num#	k4
285,15	285,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
81,2	81,2	k4
nΩ	nΩ	k?
<g/>
·	·	k?
<g/>
m	m	kA
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
Paramagnetické	paramagnetický	k2eAgNnSc1d1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
5	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
neutrony	neutron	k1gInPc7
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
neutrony	neutron	k1gInPc7
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
neutrony	neutron	k1gInPc7
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ru	Ru	k?
<g/>
⋏	⋏	k?
</s>
<s>
Rhenium	rhenium	k1gNnSc1
≺	≺	k?
<g/>
Os	osa	k1gFnPc2
<g/>
≻	≻	k?
Iridium	iridium	k1gNnSc4
</s>
<s>
⋎	⋎	k?
<g/>
Hs	Hs	k1gFnSc1
</s>
<s>
Osmium	osmium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Os	osa	k1gFnPc2
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Osmium	osmium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
drahý	drahý	k2eAgInSc1d1
kov	kov	k1gInSc1
modro-šedé	modro-šedý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
a	a	k8xC
výskyt	výskyt	k1gInSc1
</s>
<s>
Vysoce	vysoce	k6eAd1
čisté	čistý	k2eAgNnSc4d1
osmium	osmium	k1gNnSc4
</s>
<s>
Osmium	osmium	k1gNnSc1
bylo	být	k5eAaImAgNnS
objeveno	objevit	k5eAaPmNgNnS
roku	rok	k1gInSc2
1804	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ušlechtilý	ušlechtilý	k2eAgInSc1d1
<g/>
,	,	kIx,
značně	značně	k6eAd1
tvrdý	tvrdý	k2eAgInSc1d1
a	a	k8xC
křehký	křehký	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
,	,	kIx,
elektricky	elektricky	k6eAd1
i	i	k9
tepelně	tepelně	k6eAd1
středně	středně	k6eAd1
dobře	dobře	k6eAd1
vodivý	vodivý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
iridiem	iridium	k1gNnSc7
a	a	k8xC
platinou	platina	k1gFnSc7
do	do	k7c2
triády	triáda	k1gFnSc2
těžkých	těžký	k2eAgInPc2d1
platinových	platinový	k2eAgInPc2d1
kovů	kov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
iridiem	iridium	k1gNnSc7
je	být	k5eAaImIp3nS
prvkem	prvek	k1gInSc7
s	s	k7c7
největší	veliký	k2eAgFnSc7d3
známou	známý	k2eAgFnSc7d1
hustotou	hustota	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přírodě	příroda	k1gFnSc6
doprovází	doprovázet	k5eAaImIp3nS
v	v	k7c6
rudách	ruda	k1gFnPc6
platinové	platinový	k2eAgInPc4d1
kovy	kov	k1gInPc4
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgNnSc4d1
naleziště	naleziště	k1gNnSc4
jsou	být	k5eAaImIp3nP
na	na	k7c6
Uralu	Ural	k1gInSc6
a	a	k8xC
v	v	k7c6
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Osmium	osmium	k1gNnSc1
je	být	k5eAaImIp3nS
prvkem	prvek	k1gInSc7
se	s	k7c7
značně	značně	k6eAd1
nízkým	nízký	k2eAgNnSc7d1
zastoupením	zastoupení	k1gNnSc7
na	na	k7c6
Zemi	zem	k1gFnSc6
i	i	k8xC
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
je	být	k5eAaImIp3nS
průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
osmia	osmium	k1gNnSc2
udáván	udáván	k2eAgInSc1d1
v	v	k7c4
rozmezí	rozmezí	k1gNnSc4
1,5	1,5	k4
–	–	k?
3	#num#	k4
μ	μ	k1gInSc1
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
koncentrace	koncentrace	k1gFnSc2
v	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
je	být	k5eAaImIp3nS
natolik	natolik	k6eAd1
nízká	nízký	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
současnými	současný	k2eAgInPc7d1
analytickými	analytický	k2eAgInPc7d1
postupy	postup	k1gInPc7
nelze	lze	k6eNd1
spolehlivě	spolehlivě	k6eAd1
odhadnout	odhadnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
osmia	osmium	k1gNnSc2
přibližně	přibližně	k6eAd1
50	#num#	k4
miliard	miliarda	k4xCgFnPc2
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Chemicky	chemicky	k6eAd1
je	být	k5eAaImIp3nS
značně	značně	k6eAd1
odolné	odolný	k2eAgNnSc1d1
a	a	k8xC
nejsnáze	snadno	k6eAd3
jej	on	k3xPp3gMnSc4
lze	lze	k6eAd1
převést	převést	k5eAaPmF
do	do	k7c2
roztoku	roztok	k1gInSc2
po	po	k7c6
oxidačním	oxidační	k2eAgNnSc6d1
alkalickém	alkalický	k2eAgNnSc6d1
tavení	tavení	k1gNnSc6
s	s	k7c7
hydroxidem	hydroxid	k1gInSc7
a	a	k8xC
peroxidem	peroxid	k1gInSc7
sodným	sodný	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavá	zajímavý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
také	také	k9
možnost	možnost	k1gFnSc1
oddělení	oddělení	k1gNnSc2
osmia	osmium	k1gNnSc2
od	od	k7c2
ostatních	ostatní	k2eAgInPc2d1
platinových	platinový	k2eAgInPc2d1
kovů	kov	k1gInPc2
destilací	destilace	k1gFnPc2
těkavého	těkavý	k2eAgInSc2d1
oxidu	oxid	k1gInSc2
osmičelého	osmičelý	k2eAgInSc2d1
OsO	osa	k1gFnSc5
<g/>
4	#num#	k4
ze	z	k7c2
silně	silně	k6eAd1
kyselých	kyselý	k2eAgInPc2d1
roztoků	roztok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
sloučenina	sloučenina	k1gFnSc1
je	být	k5eAaImIp3nS
značně	značně	k6eAd1
jedovatá	jedovatý	k2eAgFnSc1d1
a	a	k8xC
velmi	velmi	k6eAd1
nepříjemně	příjemně	k6eNd1
zapáchá	zapáchat	k5eAaImIp3nS
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
dalo	dát	k5eAaPmAgNnS
osmiu	osmium	k1gNnSc3
i	i	k9
jeho	jeho	k3xOp3gNnSc1
jméno	jméno	k1gNnSc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
osme	osmat	k5eAaPmIp3nS
=	=	kIx~
zápach	zápach	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Osmium	osmium	k1gNnSc1
bývá	bývat	k5eAaImIp3nS
někdy	někdy	k6eAd1
označováno	označovat	k5eAaImNgNnS
jako	jako	k8xC,k8xS
prvek	prvek	k1gInSc1
s	s	k7c7
největší	veliký	k2eAgFnSc7d3
hustotou	hustota	k1gFnSc7
neboli	neboli	k8xC
nejtěžší	těžký	k2eAgInSc1d3
prvek	prvek	k1gInSc1
(	(	kIx(
<g/>
těsně	těsně	k6eAd1
před	před	k7c7
iridiem	iridium	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
podle	podle	k7c2
přesných	přesný	k2eAgInPc2d1
teoretických	teoretický	k2eAgInPc2d1
výpočtů	výpočet	k1gInPc2
z	z	k7c2
krystalové	krystalový	k2eAgFnSc2d1
mřížky	mřížka	k1gFnSc2
by	by	kYmCp3nS
dokonale	dokonale	k6eAd1
čisté	čistý	k2eAgNnSc1d1
osmium	osmium	k1gNnSc1
mělo	mít	k5eAaImAgNnS
dosahovat	dosahovat	k5eAaImF
hustoty	hustota	k1gFnSc2
22,587	22,587	k4
±	±	k?
0,009	0,009	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
iridium	iridium	k1gNnSc1
22,562	22,562	k4
±	±	k?
0,009	0,009	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Vzhledem	vzhledem	k7c3
ke	k	k7c3
svým	svůj	k3xOyFgFnPc3
mechanickým	mechanický	k2eAgFnPc3d1
vlastnostem	vlastnost	k1gFnPc3
nemá	mít	k5eNaImIp3nS
ryzí	ryzí	k2eAgNnSc1d1
kovové	kovový	k2eAgNnSc1d1
osmium	osmium	k1gNnSc1
žádné	žádný	k3yNgNnSc4
praktické	praktický	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
pouze	pouze	k6eAd1
ve	v	k7c6
slitinách	slitina	k1gFnPc6
s	s	k7c7
ostatními	ostatní	k2eAgInPc7d1
platinovými	platinový	k2eAgInPc7d1
kovy	kov	k1gInPc7
např.	např.	kA
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
velmi	velmi	k6eAd1
odolných	odolný	k2eAgInPc2d1
hrotů	hrot	k1gInPc2
plnicích	plnicí	k2eAgNnPc2d1
per	pero	k1gNnPc2
nebo	nebo	k8xC
pro	pro	k7c4
některé	některý	k3yIgInPc4
chirurgické	chirurgický	k2eAgInPc4d1
implantáty	implantát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
chemickém	chemický	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
jsou	být	k5eAaImIp3nP
při	při	k7c6
organické	organický	k2eAgFnSc6d1
syntéze	syntéza	k1gFnSc6
poměrně	poměrně	k6eAd1
řídce	řídce	k6eAd1
užívány	užíván	k2eAgInPc1d1
katalyzátory	katalyzátor	k1gInPc1
na	na	k7c6
bázi	báze	k1gFnSc6
osmia	osmium	k1gNnSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
však	však	k9
také	také	k9
ve	v	k7c6
směsi	směs	k1gFnSc6
s	s	k7c7
dalšími	další	k2eAgInPc7d1
platinovými	platinový	k2eAgInPc7d1
kovy	kov	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Arblaster	Arblaster	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
W.	W.	kA
Osmium	osmium	k1gNnSc4
<g/>
,	,	kIx,
the	the	k?
Densest	Densest	k1gFnSc1
Metal	metat	k5eAaImAgMnS
Known	Known	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Platinum	Platinum	k1gNnSc1
Metals	Metalsa	k1gFnPc2
Review	Review	k1gFnPc2
<g/>
.	.	kIx.
1995	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
39	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
164	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Osmiridium	osmiridium	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
osmium	osmium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
osmium	osmium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4172906-7	4172906-7	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5777	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85095926	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85095926	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
