<s>
Národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
Soos	Soosa	k1gFnPc2	Soosa
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgFnPc4d1	unikátní
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Františkových	Františkův	k2eAgFnPc2d1	Františkova
Lázní	lázeň	k1gFnPc2	lázeň
<g/>
,	,	kIx,	,
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
na	na	k7c4	na
území	území	k1gNnSc4	území
Skalné	skalný	k2eAgFnSc2d1	Skalná
(	(	kIx(	(
<g/>
Kateřina	Kateřina	k1gFnSc1	Kateřina
–	–	k?	–
Hájek	hájek	k1gInSc1	hájek
–	–	k?	–
Nový	nový	k2eAgInSc1d1	nový
Drahov	Drahov	k1gInSc1	Drahov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
muzeum	muzeum	k1gNnSc1	muzeum
Františkovy	Františkův	k2eAgFnSc2d1	Františkova
Lázně	lázeň	k1gFnSc2	lázeň
<g/>
.	.	kIx.	.
</s>
