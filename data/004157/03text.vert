<s>
Národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
Soos	Soosa	k1gFnPc2	Soosa
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgFnPc4d1	unikátní
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Františkových	Františkův	k2eAgFnPc2d1	Františkova
Lázní	lázeň	k1gFnPc2	lázeň
<g/>
,	,	kIx,	,
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
na	na	k7c4	na
území	území	k1gNnSc4	území
Skalné	skalný	k2eAgFnSc2d1	Skalná
(	(	kIx(	(
<g/>
Kateřina	Kateřina	k1gFnSc1	Kateřina
–	–	k?	–
Hájek	hájek	k1gInSc1	hájek
–	–	k?	–
Nový	nový	k2eAgInSc1d1	nový
Drahov	Drahov	k1gInSc1	Drahov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
muzeum	muzeum	k1gNnSc1	muzeum
Františkovy	Františkův	k2eAgFnSc2d1	Františkova
Lázně	lázeň	k1gFnSc2	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
Soos	Soosa	k1gFnPc2	Soosa
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
rozloha	rozloha	k1gFnSc1	rozloha
činí	činit	k5eAaImIp3nS	činit
221	[number]	k4	221
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
Soos	Soos	k1gInSc1	Soos
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
egerlandském	egerlandský	k2eAgInSc6d1	egerlandský
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
chebském	chebský	k2eAgNnSc6d1	chebské
německém	německý	k2eAgNnSc6d1	německé
nářečí	nářečí	k1gNnSc6	nářečí
"	"	kIx"	"
<g/>
močál	močál	k1gInSc1	močál
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
NPR	NPR	kA	NPR
Soos	Soos	k1gInSc1	Soos
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Svazku	svazek	k1gInSc2	svazek
obcí	obec	k1gFnPc2	obec
Kamenné	kamenný	k2eAgInPc1d1	kamenný
Vrchy	vrch	k1gInPc1	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Naučná	naučný	k2eAgFnSc1d1	naučná
<g/>
,	,	kIx,	,
1,2	[number]	k4	1,2
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
stezka	stezka	k1gFnSc1	stezka
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
dřevěných	dřevěný	k2eAgInPc6d1	dřevěný
chodnících	chodník	k1gInPc6	chodník
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
vyschlého	vyschlý	k2eAgNnSc2d1	vyschlé
slaného	slaný	k2eAgNnSc2d1	slané
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
ze	z	k7c2	z
schránek	schránka	k1gFnPc2	schránka
jezerních	jezerní	k2eAgFnPc2d1	jezerní
řas	řasa	k1gFnPc2	řasa
rozsivek	rozsivka	k1gFnPc2	rozsivka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
evropská	evropský	k2eAgFnSc1d1	Evropská
rarita	rarita	k1gFnSc1	rarita
–	–	k?	–
tzv.	tzv.	kA	tzv.
křemelinový	křemelinový	k2eAgInSc4d1	křemelinový
štít	štít	k1gInSc4	štít
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
vlastně	vlastně	k9	vlastně
rozlehlé	rozlehlý	k2eAgNnSc4d1	rozlehlé
rašeliniště	rašeliniště	k1gNnSc4	rašeliniště
a	a	k8xC	a
slanisko	slanisko	k1gNnSc4	slanisko
s	s	k7c7	s
dozvuky	dozvuk	k1gInPc7	dozvuk
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
činnosti	činnost	k1gFnSc2	činnost
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
unikátních	unikátní	k2eAgFnPc2d1	unikátní
tzv.	tzv.	kA	tzv.
mofet	mofeta	k1gFnPc2	mofeta
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgInPc2	který
vybublává	vybublávat	k5eAaImIp3nS	vybublávat
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
vyvěrají	vyvěrat	k5eAaImIp3nP	vyvěrat
minerální	minerální	k2eAgInPc1d1	minerální
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Císařský	císařský	k2eAgInSc1d1	císařský
pramen	pramen	k1gInSc1	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pozdní	pozdní	k2eAgFnSc3d1	pozdní
vulkanické	vulkanický	k2eAgFnSc3d1	vulkanická
činnosti	činnost	k1gFnSc3	činnost
<g/>
,	,	kIx,	,
rašelině	rašelina	k1gFnSc3	rašelina
i	i	k8xC	i
dalším	další	k2eAgInPc3d1	další
vlivům	vliv	k1gInPc3	vliv
je	být	k5eAaImIp3nS	být
krajina	krajina	k1gFnSc1	krajina
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
rezervaci	rezervace	k1gFnSc6	rezervace
jako	jako	k9	jako
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
obdoby	obdoba	k1gFnSc2	obdoba
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
Soos	Soosa	k1gFnPc2	Soosa
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
navštěvovaná	navštěvovaný	k2eAgFnSc1d1	navštěvovaná
turisty	turist	k1gMnPc7	turist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rezervaci	rezervace	k1gFnSc6	rezervace
žijí	žít	k5eAaImIp3nP	žít
druhy	druh	k1gInPc1	druh
chráněných	chráněný	k2eAgMnPc2d1	chráněný
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
roste	růst	k5eAaImIp3nS	růst
zde	zde	k6eAd1	zde
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
významných	významný	k2eAgFnPc2d1	významná
mokřadních	mokřadní	k2eAgFnPc2d1	mokřadní
a	a	k8xC	a
slanomilných	slanomilný	k2eAgFnPc2d1	slanomilná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rezervaci	rezervace	k1gFnSc6	rezervace
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
31	[number]	k4	31
druhů	druh	k1gInPc2	druh
měkkýšů	měkkýš	k1gMnPc2	měkkýš
(	(	kIx(	(
<g/>
29	[number]	k4	29
druhů	druh	k1gInPc2	druh
plžů	plž	k1gMnPc2	plž
a	a	k8xC	a
2	[number]	k4	2
druhy	druh	k1gInPc7	druh
mlžů	mlž	k1gMnPc2	mlž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Císařský	císařský	k2eAgInSc4d1	císařský
pramen	pramen	k1gInSc4	pramen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Soos	Soosa	k1gFnPc2	Soosa
je	být	k5eAaImIp3nS	být
studená	studený	k2eAgFnSc1d1	studená
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
mineralizovaná	mineralizovaný	k2eAgFnSc1d1	mineralizovaná
železitá	železitý	k2eAgFnSc1d1	železitá
sírano-uhličitano-chloridová	síranohličitanohloridový	k2eAgFnSc1d1	sírano-uhličitano-chloridový
sodná	sodný	k2eAgFnSc1d1	sodná
kyselka	kyselka	k1gFnSc1	kyselka
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
obsahem	obsah	k1gInSc7	obsah
berylia	berylium	k1gNnSc2	berylium
a	a	k8xC	a
arsenu	arsen	k1gInSc2	arsen
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
jímce	jímka	k1gFnSc6	jímka
kolísá	kolísat	k5eAaImIp3nS	kolísat
podle	podle	k7c2	podle
ročního	roční	k2eAgNnSc2d1	roční
období	období	k1gNnSc2	období
mezi	mezi	k7c4	mezi
14	[number]	k4	14
-	-	kIx~	-
18	[number]	k4	18
°	°	k?	°
<g/>
C.	C.	kA	C.
Podle	podle	k7c2	podle
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
teploty	teplota	k1gFnSc2	teplota
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
termální	termální	k2eAgFnSc4d1	termální
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
hlubokého	hluboký	k2eAgNnSc2d1	hluboké
podloží	podloží	k1gNnSc2	podloží
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
podél	podél	k7c2	podél
zlomu	zlom	k1gInSc2	zlom
a	a	k8xC	a
cestou	cesta	k1gFnSc7	cesta
se	se	k3xPyFc4	se
mísí	mísit	k5eAaImIp3nP	mísit
se	s	k7c7	s
studenými	studený	k2eAgFnPc7d1	studená
kyselkami	kyselka	k1gFnPc7	kyselka
v	v	k7c6	v
třetihorní	třetihorní	k2eAgFnSc6d1	třetihorní
pánevní	pánevní	k2eAgFnSc6d1	pánevní
výplni	výplň	k1gFnSc6	výplň
<g/>
.	.	kIx.	.
</s>
<s>
Pramen	pramen	k1gInSc1	pramen
je	být	k5eAaImIp3nS	být
nejteplejším	teplý	k2eAgInSc7d3	nejteplejší
přírodním	přírodní	k2eAgInSc7d1	přírodní
vývěrem	vývěr	k1gInSc7	vývěr
v	v	k7c6	v
Chebské	chebský	k2eAgFnSc6d1	Chebská
pánvi	pánev	k1gFnSc6	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
v	v	k7c6	v
jímce	jímka	k1gFnSc6	jímka
je	být	k5eAaImIp3nS	být
znečistěna	znečistit	k5eAaPmNgFnS	znečistit
vodou	voda	k1gFnSc7	voda
z	z	k7c2	z
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
rašeliniště	rašeliniště	k1gNnSc2	rašeliniště
<g/>
,	,	kIx,	,
lignohumáty	lignohumáta	k1gFnSc2	lignohumáta
<g/>
.	.	kIx.	.
</s>
<s>
Geopark	Geopark	k1gInSc1	Geopark
<g/>
,	,	kIx,	,
muzeum	muzeum	k1gNnSc1	muzeum
s	s	k7c7	s
expozicí	expozice	k1gFnSc7	expozice
"	"	kIx"	"
<g/>
Příroda	příroda	k1gFnSc1	příroda
Chebska	Chebsko	k1gNnSc2	Chebsko
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Příroda	příroda	k1gFnSc1	příroda
Soosu	Soos	k1gInSc2	Soos
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pavilon	pavilon	k1gInSc1	pavilon
s	s	k7c7	s
expozicí	expozice	k1gFnSc7	expozice
"	"	kIx"	"
<g/>
Dějiny	dějiny	k1gFnPc1	dějiny
Země	zem	k1gFnSc2	zem
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
paleontologie	paleontologie	k1gFnSc1	paleontologie
<g/>
,	,	kIx,	,
velkoplošnými	velkoplošný	k2eAgFnPc7d1	velkoplošná
reprodukcemi	reprodukce	k1gFnPc7	reprodukce
obrazů	obraz	k1gInPc2	obraz
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Buriana	Burian	k1gMnSc2	Burian
a	a	k8xC	a
modely	model	k1gInPc1	model
prehistorických	prehistorický	k2eAgMnPc2d1	prehistorický
ještěrů	ještěr	k1gMnPc2	ještěr
v	v	k7c6	v
životní	životní	k2eAgFnSc6d1	životní
velikosti	velikost	k1gFnSc6	velikost
<g/>
,	,	kIx,	,
stanice	stanice	k1gFnSc1	stanice
pro	pro	k7c4	pro
záchranu	záchrana	k1gFnSc4	záchrana
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
expozice	expozice	k1gFnSc1	expozice
"	"	kIx"	"
<g/>
Ptačí	ptačí	k2eAgInSc1d1	ptačí
svět	svět	k1gInSc1	svět
Chebska	Chebsko	k1gNnSc2	Chebsko
<g/>
"	"	kIx"	"
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
stávajícího	stávající	k2eAgNnSc2d1	stávající
Muzea	muzeum	k1gNnSc2	muzeum
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
daňčí	daňčí	k2eAgFnSc1d1	daňčí
obora	obora	k1gFnSc1	obora
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
Muzea	muzeum	k1gNnSc2	muzeum
Soos	Soos	k1gInSc1	Soos
a	a	k8xC	a
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
geologického	geologický	k2eAgInSc2d1	geologický
parku	park	k1gInSc2	park
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přímo	přímo	k6eAd1	přímo
u	u	k7c2	u
železniční	železniční	k2eAgFnSc2d1	železniční
zastávky	zastávka	k1gFnSc2	zastávka
Nový	nový	k2eAgInSc1d1	nový
Drahov	Drahov	k1gInSc1	Drahov
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Tršnice	Tršnice	k1gFnSc1	Tršnice
–	–	k?	–
Luby	lub	k1gInPc1	lub
u	u	k7c2	u
Chebu	Cheb	k1gInSc2	Cheb
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
rezervace	rezervace	k1gFnSc2	rezervace
je	být	k5eAaImIp3nS	být
zpoplatněn	zpoplatněn	k2eAgInSc1d1	zpoplatněn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
červeně	červeně	k6eAd1	červeně
značené	značený	k2eAgFnSc6d1	značená
turistické	turistický	k2eAgFnSc6d1	turistická
stezce	stezka	k1gFnSc6	stezka
je	být	k5eAaImIp3nS	být
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
z	z	k7c2	z
nádraží	nádraží	k1gNnSc2	nádraží
ve	v	k7c6	v
Františkových	Františkův	k2eAgFnPc6d1	Františkova
Lázních	lázeň	k1gFnPc6	lázeň
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
necelých	celý	k2eNgInPc2d1	necelý
6	[number]	k4	6
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přilehlé	přilehlý	k2eAgFnSc6d1	přilehlá
komunikaci	komunikace	k1gFnSc6	komunikace
podél	podél	k7c2	podél
hranic	hranice	k1gFnPc2	hranice
rezervace	rezervace	k1gFnSc2	rezervace
vede	vést	k5eAaImIp3nS	vést
cyklotrasa	cyklotrasa	k1gFnSc1	cyklotrasa
č.	č.	k?	č.
2133	[number]	k4	2133
<g/>
.	.	kIx.	.
</s>
