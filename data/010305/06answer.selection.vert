<s>
Mongolské	mongolský	k2eAgInPc1d1	mongolský
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
altajské	altajský	k2eAgFnSc2d1	Altajská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
je	on	k3xPp3gNnSc4	on
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
jako	jako	k9	jako
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
