<p>
<s>
Mongolské	mongolský	k2eAgInPc1d1	mongolský
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
altajské	altajský	k2eAgFnSc2d1	Altajská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
je	on	k3xPp3gNnSc4	on
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
jako	jako	k9	jako
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
střední	střední	k2eAgInPc1d1	střední
mongolské	mongolský	k2eAgInPc1d1	mongolský
jazyky	jazyk	k1gInPc1	jazyk
</s>
</p>
<p>
<s>
mongolština	mongolština	k1gFnSc1	mongolština
</s>
</p>
<p>
<s>
ordoština	ordoština	k1gFnSc1	ordoština
</s>
</p>
<p>
<s>
západní	západní	k2eAgInPc4d1	západní
mongolské	mongolský	k2eAgInPc4d1	mongolský
jazyky	jazyk	k1gInPc4	jazyk
</s>
</p>
<p>
<s>
kalmyčtina	kalmyčtina	k1gFnSc1	kalmyčtina
</s>
</p>
<p>
<s>
darkhatština	darkhatština	k1gFnSc1	darkhatština
</s>
</p>
<p>
<s>
severní	severní	k2eAgInPc1d1	severní
mongolské	mongolský	k2eAgInPc1d1	mongolský
jazyky	jazyk	k1gInPc1	jazyk
</s>
</p>
<p>
<s>
burjatština	burjatština	k1gFnSc1	burjatština
</s>
</p>
<p>
<s>
chamniganština	chamniganština	k1gFnSc1	chamniganština
</s>
</p>
<p>
<s>
severovýchodní	severovýchodní	k2eAgInPc1d1	severovýchodní
mongolské	mongolský	k2eAgInPc1d1	mongolský
jazyky	jazyk	k1gInPc1	jazyk
</s>
</p>
<p>
<s>
dagurština	dagurština	k1gFnSc1	dagurština
</s>
</p>
<p>
<s>
jihovýchodní	jihovýchodní	k2eAgInPc1d1	jihovýchodní
mongolské	mongolský	k2eAgInPc1d1	mongolský
jazyky	jazyk	k1gInPc1	jazyk
</s>
</p>
<p>
<s>
monguor	monguor	k1gMnSc1	monguor
</s>
</p>
<p>
<s>
kangjia	kangjia	k1gFnSc1	kangjia
</s>
</p>
<p>
<s>
bonan	bonan	k1gMnSc1	bonan
</s>
</p>
<p>
<s>
dongxiang	dongxiang	k1gMnSc1	dongxiang
</s>
</p>
<p>
<s>
středojižní	středojižní	k2eAgInPc1d1	středojižní
mongolské	mongolský	k2eAgInPc1d1	mongolský
jazyky	jazyk	k1gInPc1	jazyk
</s>
</p>
<p>
<s>
východní	východní	k2eAgFnSc1d1	východní
yugurština	yugurština	k1gFnSc1	yugurština
</s>
</p>
<p>
<s>
jihozápadní	jihozápadní	k2eAgInPc1d1	jihozápadní
mongolské	mongolský	k2eAgInPc1d1	mongolský
jazyky	jazyk	k1gInPc1	jazyk
</s>
</p>
<p>
<s>
mogholština	mogholština	k1gFnSc1	mogholština
</s>
</p>
