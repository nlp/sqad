<s>
Ve	v	k7c6	v
Velkém	velký	k2eAgInSc6d1	velký
Oseku	Osek	k1gInSc6	Osek
žila	žít	k5eAaImAgFnS	žít
dvojnásobná	dvojnásobný	k2eAgFnSc1d1	dvojnásobná
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
trojnásobná	trojnásobný	k2eAgFnSc1d1	trojnásobná
olympijská	olympijský	k2eAgFnSc1d1	olympijská
medailistka	medailistka	k1gFnSc1	medailistka
ze	z	k7c2	z
ZOH	ZOH	kA	ZOH
2010	[number]	k4	2010
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
,	,	kIx,	,
rychlobruslařka	rychlobruslařka	k1gFnSc1	rychlobruslařka
Martina	Martina	k1gFnSc1	Martina
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
hrách	hra	k1gFnPc6	hra
získala	získat	k5eAaPmAgFnS	získat
zlato	zlato	k1gNnSc4	zlato
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
na	na	k7c4	na
3000	[number]	k4	3000
a	a	k8xC	a
5000	[number]	k4	5000
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
bronz	bronz	k1gInSc4	bronz
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
1500	[number]	k4	1500
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
