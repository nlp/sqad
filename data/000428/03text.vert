<s>
Obec	obec	k1gFnSc4	obec
Velký	velký	k2eAgInSc4d1	velký
Osek	Osek	k1gInSc4	Osek
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Středočeský	středočeský	k2eAgInSc1d1	středočeský
<g/>
,	,	kIx,	,
asi	asi	k9	asi
9	[number]	k4	9
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Kolína	Kolín	k1gInSc2	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
2135	[number]	k4	2135
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
evidováno	evidovat	k5eAaImNgNnS	evidovat
807	[number]	k4	807
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
Osek	Osek	k1gInSc1	Osek
je	být	k5eAaImIp3nS	být
také	také	k9	také
název	název	k1gInSc1	název
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
10,55	[number]	k4	10,55
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1052	[number]	k4	1052
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
územněsprávního	územněsprávní	k2eAgNnSc2d1	územněsprávní
začleňování	začleňování	k1gNnSc2	začleňování
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
období	období	k1gNnSc4	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chronologickém	chronologický	k2eAgInSc6d1	chronologický
přehledu	přehled	k1gInSc6	přehled
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
územně	územně	k6eAd1	územně
administrativní	administrativní	k2eAgFnSc4d1	administrativní
příslušnost	příslušnost	k1gFnSc4	příslušnost
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
:	:	kIx,	:
1850	[number]	k4	1850
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
1855	[number]	k4	1855
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Čáslav	Čáslav	k1gFnSc1	Čáslav
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
1868	[number]	k4	1868
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
1939	[number]	k4	1939
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
1942	[number]	k4	1942
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
1945	[number]	k4	1945
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
správní	správní	k2eAgFnSc1d1	správní
i	i	k9	i
soudní	soudní	k2eAgInSc4d1	soudní
okres	okres	k1gInSc4	okres
Kolín	Kolín	k1gInSc4	Kolín
1949	[number]	k4	1949
Pražský	pražský	k2eAgInSc1d1	pražský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
<g />
.	.	kIx.	.
</s>
<s>
1960	[number]	k4	1960
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
2003	[number]	k4	2003
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Kolín	Kolín	k1gInSc1	Kolín
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
Velký	velký	k2eAgInSc1d1	velký
Osek	Osek	k1gInSc1	Osek
(	(	kIx(	(
<g/>
2125	[number]	k4	2125
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
poštovní	poštovní	k2eAgInSc4d1	poštovní
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
telegrafní	telegrafní	k2eAgInSc1d1	telegrafní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
četnická	četnický	k2eAgFnSc1d1	četnická
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
četnická	četnický	k2eAgFnSc1d1	četnická
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
autodoprava	autodoprava	k1gFnSc1	autodoprava
<g/>
,	,	kIx,	,
biograf	biograf	k1gMnSc1	biograf
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
cementového	cementový	k2eAgNnSc2d1	cementové
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
cukrář	cukrář	k1gMnSc1	cukrář
<g/>
,	,	kIx,	,
drogerie	drogerie	k1gFnSc1	drogerie
<g/>
,	,	kIx,	,
obecní	obecní	k2eAgFnSc1d1	obecní
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
,	,	kIx,	,
elektrotechnický	elektrotechnický	k2eAgInSc1d1	elektrotechnický
závod	závod	k1gInSc1	závod
<g/>
,	,	kIx,	,
5	[number]	k4	5
holičů	holič	k1gMnPc2	holič
<g/>
,	,	kIx,	,
8	[number]	k4	8
hostinců	hostinec	k1gInPc2	hostinec
<g/>
,	,	kIx,	,
hotel	hotel	k1gInSc1	hotel
Veselka	veselka	k1gFnSc1	veselka
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
klempíř	klempíř	k1gMnSc1	klempíř
<g/>
,	,	kIx,	,
kolář	kolář	k1gMnSc1	kolář
<g/>
,	,	kIx,	,
kominík	kominík	k1gMnSc1	kominík
<g/>
,	,	kIx,	,
2	[number]	k4	2
konsumy	konsum	k1gInPc1	konsum
<g/>
,	,	kIx,	,
2	[number]	k4	2
kováři	kovář	k1gMnPc7	kovář
<g/>
,	,	kIx,	,
4	[number]	k4	4
krejčí	krejčí	k1gMnSc1	krejčí
<g/>
,	,	kIx,	,
lakýrník	lakýrník	k1gMnSc1	lakýrník
<g/>
,	,	kIx,	,
2	[number]	k4	2
malíři	malíř	k1gMnPc1	malíř
pokojů	pokoj	k1gInPc2	pokoj
<g/>
,	,	kIx,	,
mechanik	mechanika	k1gFnPc2	mechanika
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
nábytek	nábytek	k1gInSc4	nábytek
<g/>
,	,	kIx,	,
natěrač	natěrač	k1gMnSc1	natěrač
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
obuví	obuv	k1gFnSc7	obuv
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
4	[number]	k4	4
obuvníci	obuvník	k1gMnPc1	obuvník
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
ovocem	ovoce	k1gNnSc7	ovoce
a	a	k8xC	a
zeleninou	zelenina	k1gFnSc7	zelenina
<g/>
,	,	kIx,	,
5	[number]	k4	5
pekařů	pekař	k1gMnPc2	pekař
<g/>
,	,	kIx,	,
2	[number]	k4	2
pohřební	pohřební	k2eAgInPc1d1	pohřební
ústavy	ústav	k1gInPc1	ústav
<g/>
,	,	kIx,	,
2	[number]	k4	2
porodní	porodní	k2eAgFnPc1d1	porodní
asistentky	asistentka	k1gFnPc1	asistentka
<g/>
,	,	kIx,	,
Nádražní	nádražní	k2eAgFnSc1d1	nádražní
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
,	,	kIx,	,
4	[number]	k4	4
řezníci	řezník	k1gMnPc1	řezník
<g/>
,	,	kIx,	,
9	[number]	k4	9
obchodů	obchod	k1gInPc2	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
spořitelní	spořitelní	k2eAgFnPc1d1	spořitelní
a	a	k8xC	a
záložní	záložní	k2eAgInSc1d1	záložní
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
Velký	velký	k2eAgInSc4d1	velký
Osek	Osek	k1gInSc4	Osek
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc4	obchod
se	s	k7c7	s
střižním	střižní	k2eAgNnSc7d1	střižní
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
šrouby	šroub	k1gInPc4	šroub
do	do	k7c2	do
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
švadlena	švadlena	k1gFnSc1	švadlena
<g/>
,	,	kIx,	,
3	[number]	k4	3
trafiky	trafika	k1gFnSc2	trafika
<g/>
,	,	kIx,	,
4	[number]	k4	4
truhláři	truhlář	k1gMnPc7	truhlář
<g/>
,	,	kIx,	,
zahradník	zahradník	k1gMnSc1	zahradník
<g/>
,	,	kIx,	,
železárna	železárna	k1gFnSc1	železárna
Jouza	Jouza	k1gFnSc1	Jouza
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velkém	velký	k2eAgInSc6d1	velký
Oseku	Osek	k1gInSc6	Osek
žila	žít	k5eAaImAgFnS	žít
dvojnásobná	dvojnásobný	k2eAgFnSc1d1	dvojnásobná
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
trojnásobná	trojnásobný	k2eAgFnSc1d1	trojnásobná
olympijská	olympijský	k2eAgFnSc1d1	olympijská
medailistka	medailistka	k1gFnSc1	medailistka
ze	z	k7c2	z
ZOH	ZOH	kA	ZOH
2010	[number]	k4	2010
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
,	,	kIx,	,
rychlobruslařka	rychlobruslařka	k1gFnSc1	rychlobruslařka
Martina	Martina	k1gFnSc1	Martina
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
hrách	hra	k1gFnPc6	hra
získala	získat	k5eAaPmAgFnS	získat
zlato	zlato	k1gNnSc4	zlato
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
na	na	k7c4	na
3000	[number]	k4	3000
a	a	k8xC	a
5000	[number]	k4	5000
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
bronz	bronz	k1gInSc4	bronz
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
1500	[number]	k4	1500
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
pár	pár	k4xCyI	pár
hodin	hodina	k1gFnPc2	hodina
před	před	k7c7	před
příletem	přílet	k1gInSc7	přílet
olympijské	olympijský	k2eAgFnSc2d1	olympijská
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
přelepením	přelepení	k1gNnSc7	přelepení
všech	všecek	k3xTgFnPc2	všecek
dopravních	dopravní	k2eAgFnPc2d1	dopravní
značek	značka	k1gFnPc2	značka
s	s	k7c7	s
názvem	název	k1gInSc7	název
obce	obec	k1gFnSc2	obec
"	"	kIx"	"
<g/>
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
<g/>
"	"	kIx"	"
neznámý	známý	k2eNgMnSc1d1	neznámý
recesista	recesista	k1gMnSc1	recesista
obec	obec	k1gFnSc1	obec
na	na	k7c4	na
Sáblíkov	Sáblíkov	k1gInSc4	Sáblíkov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnových	říjnový	k2eAgFnPc6d1	říjnová
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
obecního	obecní	k2eAgNnSc2d1	obecní
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
kandidovala	kandidovat	k5eAaImAgFnS	kandidovat
dvě	dva	k4xCgFnPc4	dva
"	"	kIx"	"
<g/>
nezávislá	závislý	k2eNgNnPc4d1	nezávislé
<g/>
"	"	kIx"	"
uskupení	uskupení	k1gNnPc4	uskupení
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
"	"	kIx"	"
<g/>
Nezávislí	závislý	k2eNgMnPc1d1	nezávislý
pro	pro	k7c4	pro
Velký	velký	k2eAgInSc4d1	velký
Osek	Osek	k1gInSc4	Osek
<g/>
"	"	kIx"	"
s	s	k7c7	s
nově	nově	k6eAd1	nově
zvoleným	zvolený	k2eAgMnSc7d1	zvolený
starostou	starosta	k1gMnSc7	starosta
Mgr.	Mgr.	kA	Mgr.
Pavlem	Pavel	k1gMnSc7	Pavel
Drahovzalem	Drahovzal	k1gMnSc7	Drahovzal
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
Místostarosta	místostarosta	k1gMnSc1	místostarosta
RNDr.	RNDr.	kA	RNDr.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Matuš	Matuš	k1gMnSc1	Matuš
Členové	člen	k1gMnPc1	člen
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
Mgr.	Mgr.	kA	Mgr.
Michal	Michal	k1gMnSc1	Michal
Dupák	dupák	k1gInSc1	dupák
Jan	Jan	k1gMnSc1	Jan
Fagoš	Fagoš	k1gMnSc1	Fagoš
RNDr.	RNDr.	kA	RNDr.
Jakub	Jakub	k1gMnSc1	Jakub
Munzar	Munzar	k1gMnSc1	Munzar
Jiří	Jiří	k1gMnSc1	Jiří
Veselý	Veselý	k1gMnSc1	Veselý
MUDr.	MUDr.	kA	MUDr.
Jan	Jan	k1gMnSc1	Jan
Kulhánek	Kulhánek	k1gMnSc1	Kulhánek
Ing.	ing.	kA	ing.
Helena	Helena	k1gFnSc1	Helena
Erbenová	Erbenová	k1gFnSc1	Erbenová
Ing.	ing.	kA	ing.
Jiří	Jiří	k1gMnSc1	Jiří
Kurka	Kurka	k1gMnSc1	Kurka
Kostel	kostel	k1gInSc4	kostel
Nejsvětějšího	nejsvětější	k2eAgNnSc2d1	nejsvětější
Srdce	srdce	k1gNnSc2	srdce
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
Sousoší	sousoší	k1gNnSc2	sousoší
Kalvárie	Kalvárie	k1gFnSc2	Kalvárie
stojí	stát	k5eAaImIp3nS	stát
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
V	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
leží	ležet	k5eAaImIp3nP	ležet
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
Tonice-Bezedná	Tonice-Bezedný	k2eAgFnSc1d1	Tonice-Bezedný
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
síť	síť	k1gFnSc1	síť
Pozemní	pozemní	k2eAgFnSc2d1	pozemní
komunikace	komunikace	k1gFnSc2	komunikace
Územím	území	k1gNnSc7	území
obce	obec	k1gFnSc2	obec
prochází	procházet	k5eAaImIp3nS	procházet
dálnice	dálnice	k1gFnSc1	dálnice
D11	D11	k1gFnSc1	D11
s	s	k7c7	s
exitem	exit	k1gInSc7	exit
42	[number]	k4	42
-	-	kIx~	-
Libice	Libice	k1gFnPc1	Libice
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
-	-	kIx~	-
Velký	velký	k2eAgInSc1d1	velký
Osek	Osek	k1gInSc1	Osek
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
Obcí	obec	k1gFnSc7	obec
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
125	[number]	k4	125
Železnice	železnice	k1gFnSc1	železnice
Obec	obec	k1gFnSc1	obec
Velký	velký	k2eAgInSc4d1	velký
Osek	Osek	k1gInSc4	Osek
je	být	k5eAaImIp3nS	být
křižovatkou	křižovatka	k1gFnSc7	křižovatka
železniční	železniční	k2eAgFnSc2d1	železniční
tratě	trať	k1gFnSc2	trať
020	[number]	k4	020
Velký	velký	k2eAgInSc1d1	velký
Osek	Osek	k1gInSc1	Osek
-	-	kIx~	-
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
-	-	kIx~	-
Choceň	Choceň	k1gFnSc1	Choceň
a	a	k8xC	a
tratě	trať	k1gFnPc1	trať
231	[number]	k4	231
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Lysá	Lysá	k1gFnSc1	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
-	-	kIx~	-
Nymburk	Nymburk	k1gInSc1	Nymburk
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
Trať	trať	k1gFnSc1	trať
020	[number]	k4	020
Velký	velký	k2eAgInSc1d1	velký
Osek	Osek	k1gInSc1	Osek
-	-	kIx~	-
Chlumec	Chlumec	k1gInSc1	Chlumec
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
-	-	kIx~	-
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
-	-	kIx~	-
Choceň	Choceň	k1gFnSc1	Choceň
je	být	k5eAaImIp3nS	být
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
elektrizovaná	elektrizovaný	k2eAgFnSc1d1	elektrizovaná
celostátní	celostátní	k2eAgFnSc1d1	celostátní
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Velký	velký	k2eAgInSc1d1	velký
Osek	Osek	k1gInSc1	Osek
-	-	kIx~	-
Chlumec	Chlumec	k1gInSc1	Chlumec
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
Trať	trať	k1gFnSc1	trať
231	[number]	k4	231
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Čelákovice	Čelákovice	k1gFnPc1	Čelákovice
-	-	kIx~	-
Lysá	Lysá	k1gFnSc1	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
-	-	kIx~	-
Nymburk	Nymburk	k1gInSc1	Nymburk
-	-	kIx~	-
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
je	být	k5eAaImIp3nS	být
dvoukolejná	dvoukolejný	k2eAgFnSc1d1	dvoukolejná
elektrizovaná	elektrizovaný	k2eAgFnSc1d1	elektrizovaná
celostátní	celostátní	k2eAgFnSc1d1	celostátní
trať	trať	k1gFnSc1	trať
zařazená	zařazený	k2eAgFnSc1d1	zařazená
do	do	k7c2	do
evropského	evropský	k2eAgInSc2d1	evropský
železničního	železniční	k2eAgInSc2d1	železniční
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Nymburk	Nymburk	k1gInSc1	Nymburk
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
doprava	doprava	k1gFnSc1	doprava
2011	[number]	k4	2011
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
Obcí	obec	k1gFnPc2	obec
vedla	vést	k5eAaImAgFnS	vést
příměstská	příměstský	k2eAgFnSc1d1	příměstská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
linka	linka	k1gFnSc1	linka
Kolín-Velký	Kolín-Velký	k2eAgMnSc1d1	Kolín-Velký
Osek-Sány	Osek-Sána	k1gFnPc4	Osek-Sána
(	(	kIx(	(
<g/>
v	v	k7c6	v
pracovních	pracovní	k2eAgInPc6d1	pracovní
dnech	den	k1gInPc6	den
5	[number]	k4	5
spojů	spoj	k1gInPc2	spoj
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
dopravce	dopravce	k1gMnSc2	dopravce
Okresní	okresní	k2eAgFnSc1d1	okresní
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
Po	po	k7c6	po
trati	trať	k1gFnSc6	trať
231	[number]	k4	231
vede	vést	k5eAaImIp3nS	vést
linka	linka	k1gFnSc1	linka
S2	S2	k1gFnSc1	S2
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Nymburk	Nymburk	k1gInSc1	Nymburk
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
<g/>
)	)	kIx)	)
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pražského	pražský	k2eAgInSc2d1	pražský
systému	systém	k1gInSc2	systém
Esko	eska	k1gFnSc5	eska
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trati	trať	k1gFnSc6	trať
231	[number]	k4	231
ve	v	k7c6	v
Velkém	velký	k2eAgInSc6d1	velký
Oseku	Osek	k1gInSc6	Osek
denně	denně	k6eAd1	denně
zastavovalo	zastavovat	k5eAaImAgNnS	zastavovat
8	[number]	k4	8
párů	pár	k1gInPc2	pár
rychlíků	rychlík	k1gInPc2	rychlík
<g/>
,	,	kIx,	,
6	[number]	k4	6
párů	pár	k1gInPc2	pár
spěšných	spěšný	k2eAgInPc2d1	spěšný
vlaků	vlak	k1gInPc2	vlak
a	a	k8xC	a
25	[number]	k4	25
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
trati	trať	k1gFnSc6	trať
020	[number]	k4	020
v	v	k7c6	v
pracovních	pracovní	k2eAgInPc6d1	pracovní
dnech	den	k1gInPc6	den
jezdilo	jezdit	k5eAaImAgNnS	jezdit
7	[number]	k4	7
párů	pár	k1gInPc2	pár
spěšných	spěšný	k2eAgInPc2d1	spěšný
vlaků	vlak	k1gInPc2	vlak
a	a	k8xC	a
2	[number]	k4	2
páry	pár	k1gInPc1	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
6	[number]	k4	6
párů	pár	k1gInPc2	pár
spěšných	spěšný	k2eAgInPc2d1	spěšný
vlaků	vlak	k1gInPc2	vlak
a	a	k8xC	a
1	[number]	k4	1
pár	pár	k4xCyI	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Velký	velký	k2eAgInSc4d1	velký
Osek	Osek	k1gInSc4	Osek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
Velkého	velký	k2eAgInSc2d1	velký
Oseka	Osek	k1gInSc2	Osek
na	na	k7c6	na
webu	web	k1gInSc6	web
ČÚZK	ČÚZK	kA	ČÚZK
</s>
