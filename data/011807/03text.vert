<p>
<s>
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
(	(	kIx(	(
<g/>
PdF	PdF	k1gFnSc1	PdF
MU	MU	kA	MU
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
jako	jako	k9	jako
pátá	pátý	k4xOgFnSc1	pátý
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vzdělávací	vzdělávací	k2eAgFnSc4d1	vzdělávací
<g/>
,	,	kIx,	,
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
a	a	k8xC	a
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
instituci	instituce	k1gFnSc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Zaměření	zaměření	k1gNnSc1	zaměření
vědecké	vědecký	k2eAgFnSc2d1	vědecká
a	a	k8xC	a
umělecké	umělecký	k2eAgFnSc2d1	umělecká
činnosti	činnost	k1gFnSc2	činnost
fakulty	fakulta	k1gFnSc2	fakulta
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
ze	z	k7c2	z
specifik	specifikon	k1gNnPc2	specifikon
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kateder	katedra	k1gFnPc2	katedra
a	a	k8xC	a
ústavů	ústav	k1gInPc2	ústav
a	a	k8xC	a
soustředí	soustředit	k5eAaPmIp3nS	soustředit
se	se	k3xPyFc4	se
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
humanitní	humanitní	k2eAgInPc1d1	humanitní
<g/>
,	,	kIx,	,
přírodovědné	přírodovědný	k2eAgNnSc1d1	Přírodovědné
a	a	k8xC	a
umělecké	umělecký	k2eAgNnSc1d1	umělecké
(	(	kIx(	(
<g/>
skladatelská	skladatelský	k2eAgFnSc1d1	skladatelská
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc1	koncert
a	a	k8xC	a
výstavy	výstava	k1gFnPc1	výstava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Škola	škola	k1gFnSc1	škola
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
její	její	k3xOp3gFnPc1	její
tři	tři	k4xCgFnPc1	tři
sestry	sestra	k1gFnPc1	sestra
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
,	,	kIx,	,
Univerzitě	univerzita	k1gFnSc6	univerzita
Palackého	Palackého	k2eAgFnSc2d1	Palackého
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
100	[number]	k4	100
<g/>
/	/	kIx~	/
<g/>
1946	[number]	k4	1946
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
součást	součást	k1gFnSc4	součást
poválečných	poválečný	k2eAgFnPc2d1	poválečná
změn	změna	k1gFnPc2	změna
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
tehdejších	tehdejší	k2eAgFnPc6d1	tehdejší
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
Benešových	Benešových	k2eAgInPc2d1	Benešových
dekretů	dekret	k1gInPc2	dekret
č.	č.	k?	č.
132	[number]	k4	132
<g/>
/	/	kIx~	/
<g/>
1945	[number]	k4	1945
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
učitelstva	učitelstvo	k1gNnSc2	učitelstvo
z	z	k7c2	z
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Skladbu	skladba	k1gFnSc4	skladba
studia	studio	k1gNnSc2	studio
pedagogiky	pedagogika	k1gFnSc2	pedagogika
<g/>
,	,	kIx,	,
délku	délka	k1gFnSc4	délka
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
náležitosti	náležitost	k1gFnPc1	náležitost
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
upravovalo	upravovat	k5eAaImAgNnS	upravovat
nařízení	nařízení	k1gNnSc1	nařízení
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
<g/>
Výuka	výuka	k1gFnSc1	výuka
na	na	k7c6	na
pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
fakultě	fakulta	k1gFnSc6	fakulta
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
zahájena	zahájen	k2eAgFnSc1d1	zahájena
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
děkanem	děkan	k1gMnSc7	děkan
fakulty	fakulta	k1gFnSc2	fakulta
byl	být	k5eAaImAgMnS	být
známý	známý	k2eAgMnSc1d1	známý
český	český	k2eAgMnSc1d1	český
bohemista	bohemista	k1gMnSc1	bohemista
František	František	k1gMnSc1	František
Trávníček	Trávníček	k1gMnSc1	Trávníček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
byla	být	k5eAaImAgFnS	být
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1959	[number]	k4	1959
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
fungovala	fungovat	k5eAaImAgFnS	fungovat
jako	jako	k9	jako
pedagogický	pedagogický	k2eAgInSc4d1	pedagogický
institut	institut	k1gInSc4	institut
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
začlenění	začlenění	k1gNnSc3	začlenění
mezi	mezi	k7c4	mezi
fakulty	fakulta	k1gFnPc4	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Budovy	budova	k1gFnPc4	budova
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
budovách	budova	k1gFnPc6	budova
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Poříčí	Poříčí	k1gNnSc2	Poříčí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
dvě	dva	k4xCgFnPc1	dva
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c4	na
Poříčí	Poříčí	k1gNnSc4	Poříčí
7	[number]	k4	7
a	a	k8xC	a
9	[number]	k4	9
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
situován	situován	k2eAgInSc4d1	situován
děkanát	děkanát	k1gInSc4	děkanát
a	a	k8xC	a
studijní	studijní	k2eAgNnSc4d1	studijní
oddělení	oddělení	k1gNnSc4	oddělení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
budovy	budova	k1gFnPc4	budova
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
Poříčí	Poříčí	k1gNnSc4	Poříčí
31	[number]	k4	31
a	a	k8xC	a
31	[number]	k4	31
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
slavnostně	slavnostně	k6eAd1	slavnostně
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
Centrum	centrum	k1gNnSc1	centrum
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
institutů	institut	k1gInPc2	institut
a	a	k8xC	a
doktorských	doktorský	k2eAgNnPc2d1	doktorské
studií	studio	k1gNnPc2	studio
(	(	kIx(	(
<g/>
CVIDOS	CVIDOS	kA	CVIDOS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
pěti	pět	k4xCc2	pět
vědeckých	vědecký	k2eAgInPc2d1	vědecký
institutů	institut	k1gInPc2	institut
nacházejí	nacházet	k5eAaImIp3nP	nacházet
také	také	k9	také
moderní	moderní	k2eAgFnPc1d1	moderní
prostory	prostora	k1gFnPc1	prostora
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Studium	studium	k1gNnSc1	studium
==	==	k?	==
</s>
</p>
<p>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
studium	studium	k1gNnSc1	studium
je	být	k5eAaImIp3nS	být
členěno	členit	k5eAaImNgNnS	členit
na	na	k7c4	na
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
<g/>
,	,	kIx,	,
navazující	navazující	k2eAgInSc4d1	navazující
magisterský	magisterský	k2eAgInSc4d1	magisterský
a	a	k8xC	a
doktorský	doktorský	k2eAgInSc4d1	doktorský
stupeň	stupeň	k1gInSc4	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
poslání	poslání	k1gNnSc4	poslání
studia	studio	k1gNnSc2	studio
je	být	k5eAaImIp3nS	být
příprava	příprava	k1gFnSc1	příprava
učitelů	učitel	k1gMnPc2	učitel
pro	pro	k7c4	pro
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
oborů	obor	k1gInPc2	obor
rovněž	rovněž	k9	rovněž
pro	pro	k7c4	pro
školy	škola	k1gFnPc4	škola
střední	střední	k2eAgFnPc4d1	střední
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
učitelských	učitelský	k2eAgInPc2d1	učitelský
oborů	obor	k1gInPc2	obor
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
realizováno	realizovat	k5eAaBmNgNnS	realizovat
také	také	k9	také
studium	studium	k1gNnSc1	studium
sociální	sociální	k2eAgFnSc2d1	sociální
a	a	k8xC	a
speciální	speciální	k2eAgFnSc2d1	speciální
pedagogiky	pedagogika	k1gFnSc2	pedagogika
<g/>
.	.	kIx.	.
</s>
<s>
Skladbu	skladba	k1gFnSc4	skladba
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Struktura	struktura	k1gFnSc1	struktura
==	==	k?	==
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
katedry	katedra	k1gFnPc4	katedra
a	a	k8xC	a
výzkumné	výzkumný	k2eAgInPc4d1	výzkumný
ústavy	ústav	k1gInPc4	ústav
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
jsou	být	k5eAaImIp3nP	být
samostatnými	samostatný	k2eAgFnPc7d1	samostatná
rozpočtovými	rozpočtový	k2eAgFnPc7d1	rozpočtová
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Výzkumné	výzkumný	k2eAgInPc1d1	výzkumný
instituty	institut	k1gInPc1	institut
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Institut	institut	k1gInSc1	institut
výzkumu	výzkum	k1gInSc2	výzkum
inkluzivního	inkluzivní	k2eAgNnSc2d1	inkluzivní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
</s>
</p>
<p>
<s>
Institut	institut	k1gInSc1	institut
výzkumu	výzkum	k1gInSc2	výzkum
školního	školní	k2eAgInSc2d1	školní
vzděláváníMezi	vzděláváníMeze	k1gFnSc6	vzděláváníMeze
účelová	účelový	k2eAgNnPc1d1	účelové
zařízení	zařízení	k1gNnPc1	zařízení
patří	patřit	k5eAaImIp3nP	patřit
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
knihovna	knihovna	k1gFnSc1	knihovna
Pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
patronem	patron	k1gMnSc7	patron
je	být	k5eAaImIp3nS	být
brněnský	brněnský	k2eAgMnSc1d1	brněnský
básník	básník	k1gMnSc1	básník
Vít	Vít	k1gMnSc1	Vít
Slíva	Slíva	k1gMnSc1	Slíva
<g/>
.	.	kIx.	.
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
Technicko-provozní	technickorovozní	k2eAgNnSc4d1	technicko-provozní
oddělení	oddělení	k1gNnSc4	oddělení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
působí	působit	k5eAaImIp3nS	působit
ještě	ještě	k9	ještě
Úsek	úsek	k1gInSc1	úsek
edičních	ediční	k2eAgFnPc2d1	ediční
činností	činnost	k1gFnPc2	činnost
a	a	k8xC	a
fakultní	fakultní	k2eAgNnSc1d1	fakultní
oddělení	oddělení	k1gNnSc1	oddělení
Centra	centrum	k1gNnSc2	centrum
jazykového	jazykový	k2eAgNnSc2d1	jazykové
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
se	s	k7c7	s
2016	[number]	k4	2016
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
vyčlenilo	vyčlenit	k5eAaPmAgNnS	vyčlenit
samostatné	samostatný	k2eAgNnSc1d1	samostatné
Centrum	centrum	k1gNnSc1	centrum
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
výzkumné	výzkumný	k2eAgInPc1d1	výzkumný
instituty	institut	k1gInPc1	institut
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
restrukturalizace	restrukturalizace	k1gFnSc2	restrukturalizace
včleněny	včleněn	k2eAgFnPc1d1	včleněna
pod	pod	k7c4	pod
katedry	katedra	k1gFnPc4	katedra
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
někdejší	někdejší	k2eAgInSc1d1	někdejší
Institut	institut	k1gInSc1	institut
pedagogického	pedagogický	k2eAgInSc2d1	pedagogický
vývoje	vývoj	k1gInSc2	vývoj
do	do	k7c2	do
Katedry	katedra	k1gFnSc2	katedra
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
odborného	odborný	k2eAgNnSc2d1	odborné
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
Institutu	institut	k1gInSc2	institut
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
zdraví	zdraví	k1gNnSc2	zdraví
do	do	k7c2	do
Katedry	katedra	k1gFnSc2	katedra
speciální	speciální	k2eAgFnSc2d1	speciální
pedagogigky	pedagogigka	k1gFnSc2	pedagogigka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Děkani	děkan	k1gMnPc5	děkan
===	===	k?	===
</s>
</p>
<p>
<s>
Současným	současný	k2eAgMnSc7d1	současný
děkanem	děkan	k1gMnSc7	děkan
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
její	její	k3xOp3gFnSc4	její
bývalý	bývalý	k2eAgMnSc1d1	bývalý
absolvent	absolvent	k1gMnSc1	absolvent
doc.	doc.	kA	doc.
Jiří	Jiří	k1gMnSc1	Jiří
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
působící	působící	k2eAgMnSc1d1	působící
jako	jako	k8xS	jako
prorektor	prorektor	k1gMnSc1	prorektor
pro	pro	k7c4	pro
záležitosti	záležitost	k1gFnPc4	záležitost
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
děkana	děkan	k1gMnSc2	děkan
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
akademickým	akademický	k2eAgInSc7d1	akademický
senátem	senát	k1gInSc7	senát
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
všemi	všecek	k3xTgInPc7	všecek
21	[number]	k4	21
hlasy	hlas	k1gInPc7	hlas
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Významní	významný	k2eAgMnPc1d1	významný
absolventi	absolvent	k1gMnPc1	absolvent
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgMnPc4d1	významný
absolventy	absolvent	k1gMnPc4	absolvent
fakulty	fakulta	k1gFnSc2	fakulta
patří	patřit	k5eAaImIp3nS	patřit
hokejista	hokejista	k1gMnSc1	hokejista
Josef	Josef	k1gMnSc1	Josef
Augusta	Augusta	k1gMnSc1	Augusta
<g/>
,	,	kIx,	,
plzeňský	plzeňský	k2eAgMnSc1d1	plzeňský
politik	politik	k1gMnSc1	politik
Martin	Martin	k1gMnSc1	Martin
Baxa	Baxa	k1gMnSc1	Baxa
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Petr	Petr	k1gMnSc1	Petr
Gazdík	Gazdík	k1gMnSc1	Gazdík
<g/>
,	,	kIx,	,
krasobruslařka	krasobruslařka	k1gFnSc1	krasobruslařka
Gabriela	Gabriela	k1gFnSc1	Gabriela
Hrázská	Hrázský	k2eAgFnSc1d1	Hrázská
<g/>
,	,	kIx,	,
sportovkyně	sportovkyně	k1gFnSc1	sportovkyně
Šárka	Šárka	k1gFnSc1	Šárka
Kašpárková	Kašpárková	k1gFnSc1	Kašpárková
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
současný	současný	k2eAgMnSc1d1	současný
děkan	děkan	k1gMnSc1	děkan
fakulty	fakulta	k1gFnSc2	fakulta
Jiří	Jiří	k1gMnSc1	Jiří
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Ivo	Ivo	k1gMnSc1	Ivo
Vašíček	Vašíček	k1gMnSc1	Vašíček
či	či	k8xC	či
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
moderátor	moderátor	k1gMnSc1	moderátor
Petr	Petr	k1gMnSc1	Petr
Horký	Horký	k1gMnSc1	Horký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Symboly	symbol	k1gInPc1	symbol
fakulty	fakulta	k1gFnSc2	fakulta
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Logo	logo	k1gNnSc1	logo
a	a	k8xC	a
barva	barva	k1gFnSc1	barva
===	===	k?	===
</s>
</p>
<p>
<s>
Logo	logo	k1gNnSc1	logo
fakulty	fakulta	k1gFnSc2	fakulta
představuje	představovat	k5eAaImIp3nS	představovat
otevřená	otevřený	k2eAgFnSc1d1	otevřená
kniha	kniha	k1gFnSc1	kniha
symbolizující	symbolizující	k2eAgNnSc1d1	symbolizující
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
oranžová	oranžový	k2eAgFnSc1d1	oranžová
(	(	kIx(	(
<g/>
Pantone	Panton	k1gInSc5	Panton
1375	[number]	k4	1375
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Insignie	insignie	k1gFnPc4	insignie
===	===	k?	===
</s>
</p>
<p>
<s>
Insignie	insignie	k1gFnPc1	insignie
tvoří	tvořit	k5eAaImIp3nP	tvořit
medaile	medaile	k1gFnPc4	medaile
děkanského	děkanský	k2eAgInSc2d1	děkanský
řetězu	řetěz	k1gInSc2	řetěz
navržená	navržený	k2eAgFnSc1d1	navržená
Václavem	Václav	k1gMnSc7	Václav
Adonifem	Adonif	k1gMnSc7	Adonif
Kovaničem	Kovanič	k1gMnSc7	Kovanič
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
aversu	avers	k1gInSc6	avers
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazen	k2eAgInSc4d1	vyobrazen
profil	profil	k1gInSc4	profil
a	a	k8xC	a
hruď	hruď	k1gFnSc4	hruď
Jana	Jan	k1gMnSc2	Jan
Amose	Amos	k1gMnSc2	Amos
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
,	,	kIx,	,
revers	revers	k1gInSc1	revers
tvoří	tvořit	k5eAaImIp3nS	tvořit
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
rukou	ruka	k1gFnPc2	ruka
předávajících	předávající	k2eAgMnPc2d1	předávající
si	se	k3xPyFc3	se
pochodeň	pochodeň	k1gFnSc4	pochodeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
duchu	duch	k1gMnSc6	duch
hořící	hořící	k2eAgMnSc1d1	hořící
pochodně	pochodeň	k1gFnPc4	pochodeň
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
Eduard	Eduard	k1gMnSc1	Eduard
Milén	Milén	k1gInSc4	Milén
rovněž	rovněž	k9	rovněž
žezlo	žezlo	k1gNnSc4	žezlo
<g/>
.	.	kIx.	.
</s>
<s>
Insignie	insignie	k1gFnPc4	insignie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Heslem	heslo	k1gNnSc7	heslo
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
latinský	latinský	k2eAgInSc1d1	latinský
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
Traditio	Traditio	k1gNnSc1	Traditio
Lampadis	Lampadis	k1gFnSc2	Lampadis
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
předávání	předávání	k1gNnSc1	předávání
pochodně	pochodeň	k1gFnSc2	pochodeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
JANÍK	Janík	k1gMnSc1	Janík
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
CVIDOS	CVIDOS	kA	CVIDOS
<g/>
:	:	kIx,	:
Centrum	centrum	k1gNnSc1	centrum
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
institutů	institut	k1gInPc2	institut
a	a	k8xC	a
doktorských	doktorský	k2eAgNnPc2d1	doktorské
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
Univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
46	[number]	k4	46
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
6991	[number]	k4	6991
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JANÍK	Janík	k1gMnSc1	Janík
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
malby	malba	k1gFnSc2	malba
studentů	student	k1gMnPc2	student
a	a	k8xC	a
absolventů	absolvent	k1gMnPc2	absolvent
katedry	katedra	k1gFnSc2	katedra
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
výchovy	výchova	k1gFnSc2	výchova
Pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
:	:	kIx,	:
Centrum	centrum	k1gNnSc1	centrum
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
institutů	institut	k1gInPc2	institut
a	a	k8xC	a
doktorských	doktorský	k2eAgNnPc2d1	doktorské
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
61	[number]	k4	61
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
7030	[number]	k4	7030
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Katedra	katedra	k1gFnSc1	katedra
historie	historie	k1gFnSc2	historie
Pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
</s>
</p>
<p>
<s>
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
knihovna	knihovna	k1gFnSc1	knihovna
Pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
MU	MU	kA	MU
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
fakulty	fakulta	k1gFnSc2	fakulta
</s>
</p>
<p>
<s>
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
MU	MU	kA	MU
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Aktuality	aktualita	k1gFnPc1	aktualita
z	z	k7c2	z
pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
fakulty	fakulta	k1gFnSc2	fakulta
na	na	k7c6	na
zpravodajském	zpravodajský	k2eAgInSc6d1	zpravodajský
portálu	portál	k1gInSc6	portál
MU	MU	kA	MU
</s>
</p>
