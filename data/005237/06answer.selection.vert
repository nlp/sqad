<s>
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
administrativně	administrativně	k6eAd1	administrativně
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
31	[number]	k4	31
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
Distrito	Distrita	k1gFnSc5	Distrita
Nacional	Nacional	k1gMnPc7	Nacional
-	-	kIx~	-
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Santo	Sant	k2eAgNnSc4d1	Santo
Domingo	Domingo	k1gNnSc4	Domingo
<g/>
.	.	kIx.	.
</s>
