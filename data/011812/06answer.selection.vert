<s>
Písník	písník	k1gInSc1	písník
Mácháč	Mácháč	k1gInSc1	Mácháč
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
cca	cca	kA	cca
8	[number]	k4	8
ha	ha	kA	ha
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
po	po	k7c6	po
těžbě	těžba	k1gFnSc6	těžba
štěrkopísku	štěrkopísek	k1gInSc2	štěrkopísek
ukončené	ukončená	k1gFnSc2	ukončená
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
