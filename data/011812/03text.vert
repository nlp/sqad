<p>
<s>
Písník	písník	k1gInSc1	písník
Mácháč	Mácháč	k1gInSc1	Mácháč
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
cca	cca	kA	cca
8	[number]	k4	8
ha	ha	kA	ha
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
po	po	k7c6	po
těžbě	těžba	k1gFnSc6	těžba
štěrkopísku	štěrkopísek	k1gInSc2	štěrkopísek
ukončené	ukončená	k1gFnSc2	ukončená
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Písník	písník	k1gInSc1	písník
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
Čeperka	Čeperka	k1gFnSc1	Čeperka
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Pardubice	Pardubice	k1gInPc1	Pardubice
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Písník	písník	k1gInSc1	písník
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
soukromém	soukromý	k2eAgNnSc6d1	soukromé
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
majitelky	majitelka	k1gFnSc2	majitelka
restaurace	restaurace	k1gFnSc2	restaurace
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
jako	jako	k8xC	jako
soukromý	soukromý	k2eAgInSc1d1	soukromý
rybářský	rybářský	k2eAgInSc1d1	rybářský
revír	revír	k1gInSc1	revír
<g/>
.	.	kIx.	.
</s>
<s>
Chytá	chytat	k5eAaImIp3nS	chytat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pouze	pouze	k6eAd1	pouze
metodou	metoda	k1gFnSc7	metoda
chyť	chytit	k5eAaPmRp2nS	chytit
a	a	k8xC	a
pusť	pustit	k5eAaPmRp2nS	pustit
<g/>
,	,	kIx,	,
lovnou	lovný	k2eAgFnSc7d1	lovná
rybou	ryba	k1gFnSc7	ryba
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
trofejní	trofejní	k2eAgMnPc1d1	trofejní
kapři	kapr	k1gMnPc1	kapr
a	a	k8xC	a
jeseteři	jeseter	k1gMnPc1	jeseter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bezprostředním	bezprostřední	k2eAgNnSc6d1	bezprostřední
okolí	okolí	k1gNnSc6	okolí
písníku	písník	k1gInSc2	písník
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
další	další	k2eAgInPc1d1	další
písníky	písník	k1gInPc1	písník
-	-	kIx~	-
Malá	malý	k2eAgFnSc1d1	malá
Čeperka	Čeperka	k1gFnSc1	Čeperka
<g/>
,	,	kIx,	,
Jezero	jezero	k1gNnSc1	jezero
u	u	k7c2	u
Stéblové	stéblový	k2eAgFnSc2d1	Stéblová
<g/>
,	,	kIx,	,
Oplatil	oplatit	k5eAaPmAgMnS	oplatit
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
stránky	stránka	k1gFnPc4	stránka
restaurace	restaurace	k1gFnSc2	restaurace
a	a	k8xC	a
revíru	revír	k1gInSc2	revír
</s>
</p>
