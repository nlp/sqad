<p>
<s>
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
<g/>
,	,	kIx,
nepřechýleně	přechýleně	k6eNd1
plným	plný	k2eAgNnSc7d1	plné
jménem	jméno	k1gNnSc7	jméno
Greta	Gret	k1gInSc2	Gret
Tintin	Tintin	k1gInSc1	Tintin
Eleonora	Eleonora	k1gFnSc1	Eleonora
Ernman	Ernman	k1gMnSc1	Ernman
Thunberg	Thunberg	k1gMnSc1	Thunberg
(	(	kIx(
<g/>
*	*	kIx~
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2	leden
2003	#num#	k4
<g/>
,	,	kIx,
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
celosvětově	celosvětově	k6eAd1
známá	známý	k2eAgFnSc1d1	známá
švédská	švédský	k2eAgFnSc1d1	švédská
klimatická	klimatický	k2eAgFnSc1d1	klimatická
aktivistka	aktivistka	k1gFnSc1	aktivistka
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proslavila	proslavit	k5eAaPmAgFnS
se	s	k7c7
svými	svůj	k3xOyFgFnPc7
soustavnými	soustavný	k2eAgFnPc7d1	soustavná
aktivitami	aktivita	k1gFnPc7	aktivita
na	na	k7c4
podporu	podpora	k1gFnSc4	podpora
okamžité	okamžitý	k2eAgFnSc2d1	okamžitá
akce	akce	k1gFnSc2	akce
proti	proti	k7c3
globálnímu	globální	k2eAgNnSc3d1	globální
oteplování	oteplování	k1gNnSc3	oteplování
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
začala	začít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6	rok
2018	#num#	k4
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
15	#num#	k4
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
dala	dát	k5eAaPmAgFnS
podnět	podnět	k1gInSc4	podnět
pro	pro	k7c4
vznik	vznik	k1gInSc4	vznik
hnutí	hnutí	k1gNnSc2	hnutí
školních	školní	k2eAgFnPc2d1	školní
stávek	stávka	k1gFnPc2	stávka
pro	pro	k7c4
klima	klima	k1gNnSc4	klima
(	(	kIx(
<g/>
Fridays	Fridays	k1gInSc1	Fridays
for	forum	k1gNnPc2	forum
Future	Futur	k1gMnSc5	Futur
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
listopadu	listopad	k1gInSc6	listopad
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc2
vlastní	vlastní	k2eAgFnSc2d1	vlastní
aktivity	aktivita	k1gFnSc2	aktivita
začaly	začít	k5eAaPmAgFnP
již	již	k6eAd1
v	v	k7c6
srpnu	srpen	k1gInSc6	srpen
2018	#num#	k4
stávkou	stávka	k1gFnSc7	stávka
před	před	k7c7
švédským	švédský	k2eAgInSc7d1	švédský
Riksdagem	Riksdag	k1gInSc7	Riksdag
(	(	kIx(
<g/>
parlamentem	parlament	k1gInSc7	parlament
<g/>
)	)	kIx)
ve	v	k7c6
Stockholmu	Stockholm	k1gInSc6	Stockholm
pod	pod	k7c7
sloganem	slogan	k1gInSc7	slogan
Skolstrejk	Skolstrejk	k1gInSc4	Skolstrejk
för	för	k?
klimatet	klimatet	k1gInSc1	klimatet
(	(	kIx(
<g/>
školní	školní	k2eAgFnSc1d1	školní
stávka	stávka	k1gFnSc1	stávka
pro	pro	k7c4
klima	klima	k1gNnSc4	klima
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
chtěla	chtít	k5eAaImAgFnS
vzbudit	vzbudit	k5eAaPmF
větší	veliký	k2eAgFnSc4d2	veliký
pozornost	pozornost	k1gFnSc4	pozornost
pro	pro	k7c4
problémy	problém	k1gInPc4	problém
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
samotné	samotný	k2eAgNnSc1d1	samotné
Švédsko	Švédsko	k1gNnSc1	Švédsko
přijalo	přijmout	k5eAaPmAgNnS
podle	podle	k7c2
místních	místní	k2eAgMnPc2d1	místní
politiků	politik	k1gMnPc2	politik
„	„	k?
<g/>
nejambicióznější	ambiciózní	k2eAgFnSc4d3	nejambicióznější
klimatickou	klimatický	k2eAgFnSc4d1	klimatická
legislativu	legislativa	k1gFnSc4	legislativa
na	na	k7c6
světě	svět	k1gInSc6	svět
<g/>
“	“	k?
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
do	do	k7c2
roku	rok	k1gInSc2	rok
2045	#num#	k4
uhlíkově	uhlíkově	k6eAd1
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
,	,	kIx,
Greta	Greta	k1gFnSc1	Greta
poukazovala	poukazovat	k5eAaImAgFnS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
tak	tak	k6eAd1
bohatá	bohatý	k2eAgFnSc1d1	bohatá
země	země	k1gFnSc1	země
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
dělat	dělat	k5eAaImF
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4	ochrana
klimatu	klima	k1gNnSc2	klima
víc	hodně	k6eAd2
<g/>
.	.	kIx.
<g/>
V	v	k7c6
rámci	rámec	k1gInSc6	rámec
hnutí	hnutí	k1gNnSc2	hnutí
iniciovaného	iniciovaný	k2eAgNnSc2d1	iniciované
Gretou	Greta	k1gFnSc7	Greta
Thunbergovou	Thunbergův	k2eAgFnSc7d1	Thunbergův
proběhla	proběhnout	k5eAaPmAgFnS
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2	březen
2019	#num#	k4
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
školní	školní	k2eAgFnSc1d1	školní
stávka	stávka	k1gFnSc1	stávka
za	za	k7c4
záchranu	záchrana	k1gFnSc4	záchrana
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
odhadem	odhad	k1gInSc7	odhad
zúčastnilo	zúčastnit	k5eAaPmAgNnS
více	hodně	k6eAd2
než	než	k8xS
1,4	1,4	k4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.
studentů	student	k1gMnPc2	student
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
ve	v	k7c6
zhruba	zhruba	k6eAd1
300	#num#	k4
městech	město	k1gNnPc6	město
112	#num#	k4
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1	další
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
stávka	stávka	k1gFnSc1	stávka
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2	květen
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
Za	za	k7c4
své	svůj	k3xOyFgFnPc4
aktivity	aktivita	k1gFnPc4	aktivita
obdržela	obdržet	k5eAaPmAgFnS
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
různé	různý	k2eAgFnPc4d1	různá
ceny	cena	k1gFnPc4	cena
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2	den
27	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2	květen
2019	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
tehdy	tehdy	k6eAd1
16	#num#	k4
<g/>
letá	letý	k2eAgFnSc1d1	letá
Greta	Greta	k1gFnSc1	Greta
a	a	k8xC
její	její	k3xOp3gNnSc4
působení	působení	k1gNnSc4	působení
hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
vydání	vydání	k1gNnSc2	vydání
amerického	americký	k2eAgInSc2d1	americký
týdeníku	týdeník	k1gInSc2	týdeník
Time	Tim	k1gFnSc2	Tim
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
ve	v	k7c6
světě	svět	k1gInSc6	svět
popisují	popisovat	k5eAaImIp3nP
vliv	vliv	k1gInSc4	vliv
jejích	její	k3xOp3gFnPc2
aktivit	aktivita	k1gFnPc2	aktivita
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Efekt	efekt	k1gInSc1	efekt
Grety	Greta	k1gMnSc2	Greta
Thunbergové	Thunbergový	k2eAgFnSc2d1	Thunbergový
<g/>
“	“	k?
a	a	k8xC
spojují	spojovat	k5eAaImIp3nP
jej	on	k3xPp3gMnSc4
i	i	k9
s	s	k7c7
výrazně	výrazně	k6eAd1
proenvironmentálním	proenvironmentální	k2eAgInSc7d1	proenvironmentální
posunem	posun	k1gInSc7	posun
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7
tímto	tento	k3xDgInSc7
efektem	efekt	k1gInSc7	efekt
dávají	dávat	k5eAaImIp3nP
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
také	také	k6eAd1
výsledky	výsledek	k1gInPc4	výsledek
voleb	volba	k1gFnPc2	volba
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6
roce	rok	k1gInSc6	rok
2019	#num#	k4
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Život	život	k1gInSc1	život
==	==	k?
</s>
</p>
<p>
<s>
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2	leden
2003	#num#	k4
ve	v	k7c6
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
matka	matka	k1gFnSc1	matka
je	být	k5eAaImIp3nS
švédská	švédský	k2eAgFnSc1d1	švédská
operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
Malena	malena	k1gFnSc1	malena
Ernman	Ernman	k1gMnSc1	Ernman
<g/>
,	,	kIx,
jejím	její	k3xOp3gMnSc7
otcem	otec	k1gMnSc7	otec
je	být	k5eAaImIp3nS
herec	herec	k1gMnSc1	herec
Svante	Svant	k1gInSc5	Svant
Thunberg	Thunberg	k1gMnSc1	Thunberg
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otec	otec	k1gMnSc1	otec
dostal	dostat	k5eAaPmAgMnS
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6
svém	svůj	k3xOyFgInSc6
předkovi	předek	k1gMnSc3	předek
<g/>
,	,	kIx,
nositeli	nositel	k1gMnSc3	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4
chemii	chemie	k1gFnSc4	chemie
Svante	Svant	k1gInSc5	Svant
Arrheniusovi	Arrhenius	k1gMnSc6	Arrhenius
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6	rok
1896	#num#	k4
prvním	první	k4xOgMnSc7
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
vypočítal	vypočítat	k5eAaPmAgMnS
skleníkový	skleníkový	k2eAgInSc4d1	skleníkový
efekt	efekt	k1gInSc4	efekt
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
dědečkem	dědeček	k1gMnSc7	dědeček
je	být	k5eAaImIp3nS
herec	herec	k1gMnSc1	herec
a	a	k8xC
režisér	režisér	k1gMnSc1	režisér
Olof	Olof	k1gMnSc1	Olof
Thunberg	Thunberg	k1gMnSc1	Thunberg
<g/>
.	.	kIx.
<g/>
V	v	k7c6
pořadu	pořad	k1gInSc6	pořad
TEDx	TEDx	k1gInSc1	TEDx
v	v	k7c6
prosinci	prosinec	k1gInSc6	prosinec
2018	#num#	k4
prohlásila	prohlásit	k5eAaPmAgFnS
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
<g/>
,	,	kIx,
že	že	k8xS
o	o	k7c6
klimatické	klimatický	k2eAgFnSc6d1	klimatická
změně	změna	k1gFnSc6	změna
zaslechla	zaslechnout	k5eAaPmAgFnS
poprvé	poprvé	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
jí	on	k3xPp3gFnSc3
bylo	být	k5eAaImAgNnS
osm	osm	k4xCc4
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,
a	a	k8xC
když	když	k8xS
slyšela	slyšet	k5eAaImAgFnS
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
děje	dít	k5eAaImIp3nS
<g/>
,	,	kIx,
nemohla	moct	k5eNaImAgFnS
pochopit	pochopit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
s	s	k7c7
tím	ten	k3xDgInSc7
problémem	problém	k1gInSc7	problém
nic	nic	k3yNnSc1
nedělá	dělat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
věku	věk	k1gInSc6	věk
11	#num#	k4
let	léto	k1gNnPc2	léto
z	z	k7c2
tohoto	tento	k3xDgInSc2
poznatku	poznatek	k1gInSc2	poznatek
dostala	dostat	k5eAaPmAgFnS
deprese	deprese	k1gFnSc1	deprese
a	a	k8xC
přestala	přestat	k5eAaPmAgFnS
mluvit	mluvit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
jí	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
diagnostikovány	diagnostikovat	k5eAaBmNgFnP
Aspergerův	Aspergerův	k2eAgInSc1d1	Aspergerův
syndrom	syndrom	k1gInSc4	syndrom
<g/>
,	,	kIx,
obsedantně	obsedantně	k6eAd1
kompulzivní	kompulzivní	k2eAgFnSc1d1	kompulzivní
porucha	porucha	k1gFnSc1	porucha
a	a	k8xC
selektivní	selektivní	k2eAgInSc1d1	selektivní
mutismus	mutismus	k1gInSc1	mutismus
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sama	sám	k3xTgFnSc1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
díky	díky	k7c3
selektivnímu	selektivní	k2eAgInSc3d1	selektivní
mutismu	mutismus	k1gInSc3	mutismus
mluví	mluvit	k5eAaImIp3nS
<g/>
,	,	kIx,
jen	jen	k9
když	když	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
zjistila	zjistit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
nyní	nyní	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mluvit	mluvit	k5eAaImF
musí	muset	k5eAaImIp3nP
<g/>
“	“	k?
<g/>
,	,	kIx,
a	a	k8xC
vysvětluje	vysvětlovat	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Vidím	vidět	k5eAaImIp1nS
svět	svět	k1gInSc4	svět
trochu	trochu	k6eAd1
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
z	z	k7c2
jiné	jiný	k2eAgFnSc2d1	jiná
perspektivy	perspektiva	k1gFnSc2	perspektiva
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mám	mít	k5eAaImIp1nS
speciální	speciální	k2eAgInSc4d1	speciální
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
časté	častý	k2eAgNnSc1d1	časté
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1	člověk
na	na	k7c6
autistickém	autistický	k2eAgNnSc6d1	autistické
spektru	spektrum	k1gNnSc6	spektrum
ho	on	k3xPp3gInSc2
mají	mít	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
“	“	k?
Říká	říkat	k5eAaImIp3nS
také	také	k9
<g/>
:	:	kIx,
„	„	k?
<g/>
Cítím	cítit	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
umírám	umírat	k5eAaImIp1nS
zevnitř	zevnitř	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
neprotestuji	protestovat	k5eNaBmIp1nS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
svým	svůj	k3xOyFgInPc3
protestům	protest	k1gInPc3	protest
každý	každý	k3xTgInSc4
pátek	pátek	k1gInSc4	pátek
před	před	k7c7
švédským	švédský	k2eAgInSc7d1	švédský
Říšským	říšský	k2eAgInSc7d1	říšský
sněmem	sněm	k1gInSc7	sněm
říká	říkat	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Dělám	dělat	k5eAaImIp1nS
to	ten	k3xDgNnSc1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
vy	vy	k3xPp2nPc1
dospělí	dospělí	k1gMnPc1	dospělí
mi	já	k3xPp1nSc3
kradete	krást	k5eAaImIp2nP
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
klíčovým	klíčový	k2eAgInSc7d1	klíčový
postojem	postoj	k1gInSc7	postoj
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
politika	politika	k1gFnSc1	politika
dělá	dělat	k5eAaImIp3nS
příliš	příliš	k6eAd1
málo	málo	k6eAd1
na	na	k7c4
ochranu	ochrana	k1gFnSc4	ochrana
klimatu	klima	k1gNnSc2	klima
a	a	k8xC
funguje	fungovat	k5eAaImIp3nS
tudíž	tudíž	k8xC
nezodpovědně	zodpovědně	k6eNd1
<g/>
,	,	kIx,
zejména	zejména	k9
vůči	vůči	k7c3
mladým	mladý	k2eAgMnPc3d1	mladý
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyzývá	vyzývat	k5eAaImIp3nS
k	k	k7c3
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zrychlení	zrychlení	k1gNnSc3	zrychlení
ochrany	ochrana	k1gFnSc2	ochrana
klimatu	klima	k1gNnSc2	klima
na	na	k7c6
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC
chce	chtít	k5eAaImIp3nS
pokračovat	pokračovat	k5eAaImF
ve	v	k7c6
stávce	stávka	k1gFnSc6	stávka
<g/>
,	,	kIx,
dokud	dokud	k8xS
Švédsko	Švédsko	k1gNnSc1	Švédsko
nezačne	začít	k5eNaPmIp3nS
snižovat	snižovat	k5eAaImF
emise	emise	k1gFnPc4	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
o	o	k7c4
15	#num#	k4
%	%	kIx~
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Argumentuje	argumentovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
bohatá	bohatý	k2eAgFnSc1d1	bohatá
země	země	k1gFnSc1	země
<g/>
,	,	kIx,
má	můj	k3xOp1gFnSc1
povinnost	povinnost	k1gFnSc1	povinnost
snižovat	snižovat	k5eAaImF
emise	emise	k1gFnPc4	emise
rychleji	rychle	k6eAd2
než	než	k8xS
jiné	jiný	k2eAgInPc1d1	jiný
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vadí	vadit	k5eAaImIp3nS
jí	on	k3xPp3gFnSc3
především	především	k9
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
bohaté	bohatý	k2eAgNnSc1d1	bohaté
Švédsko	Švédsko	k1gNnSc1	Švédsko
se	s	k7c7
tváří	tvář	k1gFnSc7	tvář
jako	jako	k8xS,k8xC
velmi	velmi	k6eAd1
angažovaná	angažovaný	k2eAgFnSc1d1	angažovaná
země	země	k1gFnSc1	země
v	v	k7c6
boji	boj	k1gInSc6	boj
s	s	k7c7
klimatickou	klimatický	k2eAgFnSc7d1	klimatická
změnou	změna	k1gFnSc7	změna
<g/>
,	,	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ale	ale	k9
politici	politik	k1gMnPc1	politik
nic	nic	k3yNnSc1
reálného	reálný	k2eAgMnSc2d1	reálný
nedělají	dělat	k5eNaImIp3nP
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napsala	napsat	k5eAaPmAgFnS,k5eAaBmAgFnS
článek	článek	k1gInSc4	článek
„	„	k?
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
není	být	k5eNaImIp3nS
příkladem	příklad	k1gInSc7	příklad
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
Jejímu	její	k3xOp3gInSc3
otci	otec	k1gMnSc6	otec
se	se	k3xPyFc4
nelíbí	líbit	k5eNaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Greta	Greta	k1gFnSc1	Greta
stávkuje	stávkovat	k5eAaImIp3nS
místo	místo	k1gNnSc4	místo
docházky	docházka	k1gFnSc2	docházka
do	do	k7c2
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,
ale	ale	k8xC
říká	říkat	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Respektujeme	respektovat	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
chce	chtít	k5eAaImIp3nS
zaujmout	zaujmout	k5eAaPmF
stanovisko	stanovisko	k1gNnSc4	stanovisko
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buď	buď	k8xC
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
doma	doma	k6eAd1
a	a	k8xC
být	být	k5eAaImF
opravdu	opravdu	k6eAd1
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
<g/>
,	,	kIx,
nebo	nebo	k8xC
protestovat	protestovat	k5eAaBmF
a	a	k8xC
být	být	k5eAaImF
šťastná	šťastný	k2eAgFnSc1d1	šťastná
<g/>
.	.	kIx.
“	“	k?
</s>
<s>
Ke	k	k7c3
snížení	snížení	k1gNnSc3	snížení
uhlíkové	uhlíkový	k2eAgFnSc2d1	uhlíková
stopy	stopa	k1gFnSc2	stopa
přiměla	přimět	k5eAaPmAgFnS
celou	celý	k2eAgFnSc4d1	celá
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
přešli	přejít	k5eAaPmAgMnP
na	na	k7c4
veganskou	veganský	k2eAgFnSc4d1	veganská
stravu	strava	k1gFnSc4	strava
a	a	k8xC
přestali	přestat	k5eAaPmAgMnP
létat	létat	k5eAaImF
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
matka	matka	k1gFnSc1	matka
kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
omezila	omezit	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
kariéru	kariéra	k1gFnSc4	kariéra
operní	operní	k2eAgFnSc2d1	operní
pěvkyně	pěvkyně	k1gFnSc2	pěvkyně
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sama	Sam	k1gMnSc2	Sam
Greta	Gret	k1gMnSc2	Gret
odmítá	odmítat	k5eAaImIp3nS
pozvání	pozvání	k1gNnSc4	pozvání
na	na	k7c4
jakékoliv	jakýkoliv	k3yIgNnSc4
setkání	setkání	k1gNnSc4	setkání
<g/>
,	,	kIx,
kam	kam	k6eAd1
by	by	kYmCp3nS
musela	muset	k5eAaImAgFnS
letět	letět	k5eAaImF
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
.	.	kIx.
<g/>
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
je	být	k5eAaImIp3nS
si	se	k3xPyFc3
vědoma	vědom	k2eAgFnSc1d1	vědoma
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
její	její	k3xOp3gMnPc1
učitelé	učitel	k1gMnPc1	učitel
jsou	být	k5eAaImIp3nP
rozděleni	rozdělit	k5eAaPmNgMnP
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
názorech	názor	k1gInPc6	názor
na	na	k7c4
její	její	k3xOp3gFnPc4
absence	absence	k1gFnPc4	absence
ve	v	k7c6
škole	škola	k1gFnSc6	škola
kvůli	kvůli	k7c3
stávkám	stávka	k1gFnPc3	stávka
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říká	říkat	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Jako	jako	k8xS,k8xC
lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3
myslí	myslet	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
dělám	dělat	k5eAaImIp1nS
je	on	k3xPp3gMnPc4
dobré	dobrý	k2eAgMnPc4d1	dobrý
<g/>
,	,	kIx,
ale	ale	k8xC
jako	jako	k8xC,k8xS
učitelé	učitel	k1gMnPc1	učitel
říkají	říkat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
bych	by	kYmCp1nS
toho	ten	k3xDgMnSc4
měla	mít	k5eAaImAgFnS
nechat	nechat	k5eAaPmF
<g/>
.	.	kIx.
<g/>
“	“	k?
Jeden	jeden	k4xCgMnSc1
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
ji	on	k3xPp3gFnSc4
podporuje	podporovat	k5eAaImIp3nS
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Greta	Greta	k1gFnSc1	Greta
je	být	k5eAaImIp3nS
potížistka	potížistka	k1gFnSc1	potížistka
<g/>
,	,	kIx,
neposlouchá	poslouchat	k5eNaImIp3nS
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směřujeme	směřovat	k5eAaImIp1nP
však	však	k9
plnou	plný	k2eAgFnSc7d1	plná
rychlostí	rychlost	k1gFnSc7	rychlost
do	do	k7c2
katastrofy	katastrofa	k1gFnSc2	katastrofa
a	a	k8xC
za	za	k7c2
takové	takový	k3xDgFnSc2
situace	situace	k1gFnSc2	situace
je	být	k5eAaImIp3nS
jediná	jediný	k2eAgFnSc1d1	jediná
rozumná	rozumný	k2eAgFnSc1d1	rozumná
věc	věc	k1gFnSc1	věc
dělat	dělat	k5eAaImF
nerozumné	rozumný	k2eNgFnPc4d1	nerozumná
věci	věc	k1gFnPc4	věc
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
V	v	k7c6
květnu	květen	k1gInSc6	květen
2019	#num#	k4
vydalo	vydat	k5eAaPmAgNnS
anglické	anglický	k2eAgNnSc1d1	anglické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Penguin	Penguina	k1gFnPc2	Penguina
Books	Books	k1gInSc4	Books
sbírku	sbírka	k1gFnSc4	sbírka
projevů	projev	k1gInPc2	projev
Thunbergové	Thunbergový	k2eAgNnSc1d1	Thunbergový
pod	pod	k7c7
názvem	název	k1gInSc7	název
No	no	k9
One	One	k1gMnSc1	One
Is	Is	k1gMnSc1	Is
Too	Too	k1gMnSc1	Too
Small	Small	k1gMnSc1	Small
to	ten	k3xDgNnSc4
Make	Make	k1gNnSc4	Make
a	a	k8xC
Difference	Difference	k1gFnPc4	Difference
(	(	kIx(
<g/>
česky	česky	k6eAd1
Nikdo	nikdo	k3yNnSc1
není	být	k5eNaImIp3nS
příliš	příliš	k6eAd1
malý	malý	k2eAgInSc1d1	malý
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
něco	něco	k3yInSc4
změnil	změnit	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Penguin	Penguina	k1gFnPc2	Penguina
plánuje	plánovat	k5eAaImIp3nS
také	také	k9
vydat	vydat	k5eAaPmF
knihu	kniha	k1gFnSc4	kniha
Scény	scéna	k1gFnSc2	scéna
ze	z	k7c2
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
popisovat	popisovat	k5eAaImF
příběh	příběh	k1gInSc4	příběh
rodiny	rodina	k1gFnSc2	rodina
Thunbergových	Thunbergův	k2eAgMnPc2d1	Thunbergův
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výnosy	výnos	k1gInPc1	výnos
z	z	k7c2
těchto	tento	k3xDgInPc2
projektů	projekt	k1gInPc2	projekt
budou	být	k5eAaImBp3nP
věnovány	věnovat	k5eAaImNgFnP,k5eAaPmNgFnP
na	na	k7c4
charitu	charita	k1gFnSc4	charita
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6	květen
2019	#num#	k4
nakreslil	nakreslit	k5eAaPmAgMnS
Jody	jod	k1gInPc1	jod
Thomas	Thomas	k1gMnSc1	Thomas
na	na	k7c4
zeď	zeď	k1gFnSc4	zeď
v	v	k7c6
Bristolu	Bristol	k1gInSc6	Bristol
50	#num#	k4
metrů	metr	k1gInPc2	metr
vysokou	vysoký	k2eAgFnSc4d1	vysoká
nástěnnou	nástěnný	k2eAgFnSc4d1	nástěnná
kresbu	kresba	k1gFnSc4	kresba
Thunbergové	Thunbergový	k2eAgFnPc4d1	Thunbergový
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zobrazuje	zobrazovat	k5eAaImIp3nS
Gretu	Greta	k1gFnSc4	Greta
až	až	k9
po	po	k7c4
nos	nos	k1gInSc4	nos
ponořenou	ponořený	k2eAgFnSc4d1	ponořená
do	do	k7c2
stoupající	stoupající	k2eAgFnSc2d1	stoupající
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.
<g/>
V	v	k7c6
červnu	červen	k1gInSc6	červen
2019	#num#	k4
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
ukončení	ukončení	k1gNnSc6	ukončení
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
si	se	k3xPyFc3
vezme	vzít	k5eAaPmIp3nS
rok	rok	k1gInSc4	rok
volna	volno	k1gNnSc2	volno
od	od	k7c2
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4
mj.	mj.	kA
zúčastní	zúčastnit	k5eAaPmIp3nS
zasedání	zasedání	k1gNnSc1	zasedání
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
OSN	OSN	kA
v	v	k7c6
září	září	k1gNnSc6	září
2019	#num#	k4
v	v	k7c6
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC
Konference	konference	k1gFnSc2	konference
OSN	OSN	kA
o	o	k7c6
změně	změna	k1gFnSc6	změna
klimatu	klima	k1gNnSc2	klima
2019	#num#	k4
v	v	k7c6
Santiagu	Santiago	k1gNnSc6	Santiago
de	de	k?
Chile	Chile	k1gNnSc6	Chile
v	v	k7c6
prosinci	prosinec	k1gInSc6	prosinec
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oznámila	oznámit	k5eAaPmAgFnS
také	také	k9
<g/>
,	,	kIx,
že	že	k8xS
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
přes	přes	k7c4
Atlantský	atlantský	k2eAgInSc4d1	atlantský
oceán	oceán	k1gInSc4	oceán
vykoná	vykonat	k5eAaPmIp3nS
na	na	k7c6
závodní	závodní	k2eAgFnSc6d1	závodní
jachtě	jachta	k1gFnSc6	jachta
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
podle	podle	k7c2
ní	on	k3xPp3gFnSc2
nemá	mít	k5eNaImIp3nS
žádné	žádný	k3yNgFnPc4
emise	emise	k1gFnPc4	emise
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
má	mít	k5eAaImIp3nS
<g/>
,	,	kIx,
doprovázena	doprovázen	k2eAgFnSc1d1	doprovázena
svým	svůj	k1gMnSc7	svůj
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,
cestovat	cestovat	k5eAaImF
vlaky	vlak	k1gInPc1	vlak
a	a	k8xC
autobusy	autobus	k1gInPc1	autobus
na	na	k7c4
konferenci	konference	k1gFnSc4	konference
v	v	k7c6
Chile	Chile	k1gNnSc6	Chile
se	s	k7c7
zastávkami	zastávka	k1gFnPc7	zastávka
v	v	k7c6
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC
v	v	k7c6
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
plavba	plavba	k1gFnSc1	plavba
do	do	k7c2
Ameriky	Amerika	k1gFnSc2	Amerika
proběhla	proběhnout	k5eAaPmAgFnS
ve	v	k7c6
dnech	den	k1gInPc6	den
14	#num#	k4
<g/>
.	.	kIx.
až	až	k9
28	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2	srpen
2019	#num#	k4
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Školní	školní	k2eAgInPc1d1	školní
stávky	stávek	k1gInPc1	stávek
pro	pro	k7c4
klima	klima	k1gNnSc4	klima
==	==	k?
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
20	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2	srpen
2018	#num#	k4
se	se	k3xPyFc4
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
<g/>
,	,	kIx,
žákyně	žákyně	k1gFnPc1	žákyně
deváté	devátý	k4xOgFnSc2
třídy	třída	k1gFnSc2	třída
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,
rozhodla	rozhodnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
nebude	být	k5eNaImBp3nS
chodit	chodit	k5eAaImF
do	do	k7c2
školy	škola	k1gFnSc2	škola
až	až	k9
do	do	k7c2
švédských	švédský	k2eAgFnPc2d1	švédská
všeobecných	všeobecný	k2eAgFnPc2d1	všeobecná
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
dne	den	k1gInSc2	den
9	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodla	rozhodnout	k5eAaPmAgFnS
se	se	k3xPyFc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
Švédsko	Švédsko	k1gNnSc1	Švédsko
čelilo	čelit	k5eAaImAgNnS
v	v	k7c6
létě	léto	k1gNnSc6	léto
nebývalým	nebývalý	k2eAgFnPc3d1,k2eNgFnPc3d1	nebývalá
vlnám	vlna	k1gFnPc3	vlna
horka	horko	k1gNnSc2	horko
a	a	k8xC
požárů	požár	k1gInPc2	požár
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chtěla	chtít	k5eAaImAgFnS
po	po	k7c6
švédské	švédský	k2eAgFnSc6d1	švédská
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
snížila	snížit	k5eAaPmAgFnS
emise	emise	k1gFnSc1	emise
uhlíku	uhlík	k1gInSc2	uhlík
podle	podle	k7c2
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6
změně	změna	k1gFnSc6	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seděla	sedět	k5eAaImAgFnS
před	před	k7c7
Riksdagem	Riksdago	k1gNnSc7	Riksdago
každý	každý	k3xTgInSc4
den	den	k1gInSc4	den
během	během	k7c2
školních	školní	k2eAgFnPc2d1	školní
hodin	hodina	k1gFnPc2	hodina
se	s	k7c7
sloganem	slogan	k1gInSc7	slogan
„	„	k?
<g/>
Školní	školní	k2eAgFnSc1d1	školní
stávka	stávka	k1gFnSc1	stávka
pro	pro	k7c4
klima	klima	k1gNnSc4	klima
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
volbách	volba	k1gFnPc6	volba
pokračovala	pokračovat	k5eAaImAgFnS
v	v	k7c6
protestu	protest	k1gInSc6	protest
každý	každý	k3xTgInSc4
pátek	pátek	k1gInSc4	pátek
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
páteční	páteční	k2eAgFnSc1d1	páteční
stávka	stávka	k1gFnSc1	stávka
získala	získat	k5eAaPmAgFnS
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inspirovala	inspirovat	k5eAaBmAgFnS
školní	školní	k2eAgFnSc1d1	školní
studenty	student	k1gMnPc4	student
po	po	k7c6
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
ke	k	k7c3
klimatickým	klimatický	k2eAgFnPc3d1	klimatická
stávkám	stávka	k1gFnPc3	stávka
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6	prosinec
2018	#num#	k4
proběhly	proběhnout	k5eAaPmAgFnP
školní	školní	k2eAgFnPc1d1	školní
stávky	stávka	k1gFnPc1	stávka
nejméně	málo	k6eAd3
na	na	k7c6
270	#num#	k4
různých	různý	k2eAgInPc6d1	různý
místech	místo	k1gNnPc6	místo
a	a	k8xC
účastnilo	účastnit	k5eAaImAgNnS
se	se	k3xPyFc4
jich	on	k3xPp3gFnPc2
minimálně	minimálně	k6eAd1
20	#num#	k4
tisíc	tisíc	k4xCgInPc2
školáků	školák	k1gMnPc2	školák
<g/>
.	.	kIx.
</s>
<s desamb="1">
Myšlenku	myšlenka	k1gFnSc4	myšlenka
školních	školní	k2eAgFnPc2d1	školní
stávek	stávka	k1gFnPc2	stávka
pro	pro	k7c4
klima	klima	k1gNnSc4	klima
bezprostředně	bezprostředně	k6eAd1
před	před	k7c7
volbami	volba	k1gFnPc7	volba
do	do	k7c2
švédského	švédský	k2eAgInSc2d1	švédský
parlamentu	parlament	k1gInSc2	parlament
představil	představit	k5eAaPmAgInS
původně	původně	k6eAd1
Bo	Bo	k?
Thorén	Thorén	k1gInSc1	Thorén
během	během	k7c2
pořadů	pořad	k1gInPc2	pořad
o	o	k7c6
mobilizaci	mobilizace	k1gFnSc6	mobilizace
mládeže	mládež	k1gFnSc2	mládež
na	na	k7c4
ochranu	ochrana	k1gFnSc4	ochrana
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
se	se	k3xPyFc4
několika	několik	k4yIc2
těchto	tento	k3xDgNnPc2
setkání	setkání	k1gNnPc2	setkání
zúčastnila	zúčastnit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Thorén	Thorén	k1gInSc4	Thorén
byl	být	k5eAaImAgInS
inspirován	inspirovat	k5eAaBmNgInS
školními	školní	k2eAgFnPc7d1	školní
stávkami	stávka	k1gFnPc7	stávka
na	na	k7c4
zpřísnění	zpřísnění	k1gNnSc4	zpřísnění
zákona	zákon	k1gInSc2	zákon
o	o	k7c6
držení	držení	k1gNnSc6	držení
zbraní	zbraň	k1gFnPc2	zbraň
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1	další
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4
Gretu	Greta	k1gFnSc4	Greta
byli	být	k5eAaImAgMnP
mladí	mladý	k2eAgMnPc1d1	mladý
aktivisté	aktivista	k1gMnPc1	aktivista
ze	z	k7c2
Střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
Marjory	Marjora	k1gFnSc2	Marjora
Stoneman	Stoneman	k1gMnSc1	Stoneman
Douglasové	Douglasové	k2eAgMnSc1d1	Douglasové
v	v	k7c6
Parklandu	Parkland	k1gInSc6	Parkland
na	na	k7c6
Floridě	Florida	k1gFnSc6	Florida
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
organizovali	organizovat	k5eAaBmAgMnP
Pochod	pochod	k1gInSc4	pochod
pro	pro	k7c4
naše	náš	k3xOp1gInPc4
životy	život	k1gInPc4	život
<g/>
.	.	kIx.
<g/>
Ve	v	k7c6
svém	svůj	k3xOyFgInSc6
osobním	osobní	k2eAgInSc6d1	osobní
manifestu	manifest	k1gInSc6	manifest
říká	říkat	k5eAaImIp3nS
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
klimatoložkou	klimatoložka	k1gFnSc7	klimatoložka
<g/>
:	:	kIx,
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
poslem	posel	k1gMnSc7	posel
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
opakuje	opakovat	k5eAaImIp3nS
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
klimatologové	klimatolog	k1gMnPc1	klimatolog
říct	říct	k5eAaPmF
veřejnosti	veřejnost	k1gFnPc4	veřejnost
již	již	k6eAd1
desetiletí	desetiletí	k1gNnSc4	desetiletí
<g/>
,	,	kIx,
bohužel	bohužel	k9
však	však	k9
zatím	zatím	k6eAd1
neúspěšně	úspěšně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
studenti	student	k1gMnPc1	student
mohou	moct	k5eAaImIp3nP
ukončit	ukončit	k5eAaPmF
své	svůj	k3xOyFgInPc4
protesty	protest	k1gInPc4	protest
<g/>
,	,	kIx,
pokud	pokud	k8xS
začne	začít	k5eAaPmIp3nS
veřejnost	veřejnost	k1gFnSc4	veřejnost
konečně	konečně	k6eAd1
poslouchat	poslouchat	k5eAaImF
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
říkají	říkat	k5eAaImIp3nP
vědci	vědec	k1gMnPc1	vědec
<g/>
,	,	kIx,
ale	ale	k8xC
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
tento	tento	k3xDgInSc4
pokrok	pokrok	k1gInSc4	pokrok
ani	ani	k8xC
nenaznačuje	naznačovat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
Od	od	k7c2
října	říjen	k1gInSc2	říjen
2018	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
aktivismus	aktivismus	k1gInSc4	aktivismus
Thunbergové	Thunbergový	k2eAgFnSc2d1	Thunbergový
začal	začít	k5eAaPmAgMnS
osamělými	osamělý	k2eAgInPc7d1	osamělý
protesty	protest	k1gInPc7	protest
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,
se	se	k3xPyFc4
zúčastnila	zúčastnit	k5eAaPmAgFnS
demonstrací	demonstrace	k1gFnPc2	demonstrace
po	po	k7c6
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
několik	několik	k4yIc4
veřejných	veřejný	k2eAgNnPc2d1	veřejné
vystoupení	vystoupení	k1gNnPc2	vystoupení
a	a	k8xC
pokračuje	pokračovat	k5eAaImIp3nS
v	v	k7c6
mobilizaci	mobilizace	k1gFnSc6	mobilizace
svých	svůj	k3xOyFgMnPc2
vrstevníků	vrstevník	k1gMnPc2	vrstevník
na	na	k7c6
sociálních	sociální	k2eAgFnPc6d1	sociální
sítích	síť	k1gFnPc6	síť
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračuje	pokračovat	k5eAaImIp3nS
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
pátečních	páteční	k2eAgFnPc6d1	páteční
stávkách	stávka	k1gFnPc6	stávka
před	před	k7c7
švédským	švédský	k2eAgInSc7d1	švédský
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
stávky	stávka	k1gFnPc1	stávka
nenarušují	narušovat	k5eNaImIp3nP
její	její	k3xOp3gNnSc4
učení	učení	k1gNnSc4	učení
ve	v	k7c6
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,
ale	ale	k8xC
že	že	k8xS
má	mít	k5eAaImIp3nS
méně	málo	k6eAd2
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.
<g/>
Generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
OSN	OSN	kA
António	António	k1gMnSc1	António
Guterres	Guterres	k1gMnSc1	Guterres
podpořil	podpořit	k5eAaPmAgMnS
školní	školní	k2eAgInPc4d1	školní
stávky	stávek	k1gInPc4	stávek
iniciované	iniciovaný	k2eAgInPc4d1	iniciovaný
Gretou	Greta	k1gFnSc7	Greta
Thunbergovou	Thunbergův	k2eAgFnSc7d1	Thunbergův
a	a	k8xC
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Má	mít	k5eAaImIp3nS
generace	generace	k1gFnSc1	generace
selhala	selhat	k5eAaPmAgFnS
<g/>
,	,	kIx,
když	když	k8xS
měla	mít	k5eAaImAgFnS
správně	správně	k6eAd1
reagovat	reagovat	k5eAaBmF
na	na	k7c4
dramatické	dramatický	k2eAgFnPc4d1	dramatická
výzvy	výzva	k1gFnPc4	výzva
změny	změna	k1gFnSc2	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
hluboce	hluboko	k6eAd1
pociťováno	pociťovat	k5eAaImNgNnS
mladými	mladý	k2eAgMnPc7d1	mladý
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
divu	div	k1gInSc2	div
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
rozhněvaní	rozhněvaný	k2eAgMnPc1d1	rozhněvaný
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
</p>
<p>
<s>
==	==	k?
Požadavky	požadavek	k1gInPc1	požadavek
==	==	k?
</s>
</p>
<p>
<s>
Když	když	k8xS
zahájila	zahájit	k5eAaPmAgFnS
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
svůj	svůj	k3xOyFgInSc4
protest	protest	k1gInSc4	protest
před	před	k7c7
švédským	švédský	k2eAgInSc7d1	švédský
parlamentem	parlament	k1gInSc7	parlament
v	v	k7c6
roce	rok	k1gInSc6	rok
2018	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6	věk
15	#num#	k4
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
dvě	dva	k4xCgFnPc4
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
zprávy	zpráva	k1gFnPc4	zpráva
–	–	k?
kartonovou	kartonový	k2eAgFnSc4d1	kartonová
tabuli	tabule	k1gFnSc4	tabule
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
bylo	být	k5eAaImAgNnS
heslo	heslo	k1gNnSc1	heslo
„	„	k?
<g/>
školní	školní	k2eAgFnSc1d1	školní
stávka	stávka	k1gFnSc1	stávka
pro	pro	k7c4
klima	klima	k1gNnSc4	klima
<g/>
“	“	k?
a	a	k8xC
letáky	leták	k1gInPc1	leták
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
rozdávala	rozdávat	k5eAaImAgFnS
a	a	k8xC
na	na	k7c6
kterých	který	k3yRgInPc6,k3yQgInPc6,k3yIgInPc6
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Dělám	dělat	k5eAaImIp1nS
to	ten	k3xDgNnSc1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
vy	vy	k3xPp2nPc1
dospělí	dospělí	k1gMnPc1	dospělí
opomíjíte	opomíjet	k5eAaImIp2nP
mou	můj	k3xOp1gFnSc4
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.
<g/>
“	“	k?
Když	když	k8xS
její	její	k3xOp3gInSc1
protest	protest	k1gInSc1	protest
získal	získat	k5eAaPmAgInS
větší	veliký	k2eAgFnSc4d2	veliký
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
pozvána	pozvat	k5eAaPmNgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vystoupila	vystoupit	k5eAaPmAgFnS
na	na	k7c6
různých	různý	k2eAgInPc6d1	různý
fórech	fór	k1gInPc6	fór
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jí	on	k3xPp3gFnSc3
umožnilo	umožnit	k5eAaPmAgNnS
šířit	šířit	k5eAaImF
své	svůj	k3xOyFgInPc4
požadavky	požadavek	k1gInPc4	požadavek
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doposud	doposud	k6eAd1
obhajovala	obhajovat	k5eAaImAgFnS
čtyři	čtyři	k4xCgNnPc4
provázaná	provázaný	k2eAgNnPc4d1	provázané
témata	téma	k1gNnPc4	téma
<g/>
:	:	kIx,
</s>
</p>
<p>
<s>
Krize	krize	k1gFnSc1	krize
způsobená	způsobený	k2eAgFnSc1d1	způsobená
globálním	globální	k2eAgInSc7d1	globální
oteplováním	oteplování	k1gNnPc3	oteplování
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
závažná	závažný	k2eAgFnSc1d1	závažná
<g/>
,	,	kIx,
že	že	k8xS
lidstvo	lidstvo	k1gNnSc1	lidstvo
čelí	čelit	k5eAaImIp3nS
existenciální	existenciální	k2eAgFnSc4d1	existenciální
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,
„	„	k?
<g/>
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
pravděpodobně	pravděpodobně	k6eAd1
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
ke	k	k7c3
konci	konec	k1gInSc3	konec
naší	náš	k3xOp1gFnSc2
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,
tak	tak	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
ji	on	k3xPp3gFnSc4
dosud	dosud	k6eAd1
známe	znát	k5eAaImIp1nP
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
Za	za	k7c4
situaci	situace	k1gFnSc4	situace
je	být	k5eAaImIp3nS
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
současná	současný	k2eAgFnSc1d1	současná
generace	generace	k1gFnSc1	generace
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
prosazuje	prosazovat	k5eAaImIp3nS
výroky	výrok	k1gInPc4	výrok
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Kradete	krást	k5eAaImIp2nP
nám	my	k3xPp1nPc3
naši	náš	k3xOp1gFnSc4
budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obává	obávat	k5eAaImIp3nS
se	se	k3xPyFc4
zejména	zejména	k9
dopadu	dopad	k1gInSc3	dopad
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
klimatická	klimatický	k2eAgFnSc1d1	klimatická
krize	krize	k1gFnSc1	krize
na	na	k7c4
mladé	mladý	k2eAgMnPc4d1	mladý
lidi	člověk	k1gMnPc4	člověk
jako	jako	k9
je	být	k5eAaImIp3nS
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
britském	britský	k2eAgInSc6d1	britský
parlamentu	parlament	k1gInSc6	parlament
v	v	k7c6
Londýně	Londýn	k1gInSc6	Londýn
řekla	říct	k5eAaPmAgFnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Lhali	lhát	k5eAaImAgMnP
jste	být	k5eAaImIp2nP
nám.	nám.	k?
Dávali	dávat	k5eAaImAgMnP
jste	být	k5eAaImIp2nP
nám	my	k3xPp1nPc3
falešnou	falešný	k2eAgFnSc4d1	falešná
naději	naděje	k1gFnSc4	naděje
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říkali	říkat	k5eAaImAgMnP
jste	být	k5eAaImIp2nP
nám	my	k3xPp1nPc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
můžeme	moct	k5eAaImIp1nP
těšit	těšit	k5eAaImF
na	na	k7c4
naši	náš	k3xOp1gFnSc4
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
lidstvo	lidstvo	k1gNnSc1	lidstvo
probudilo	probudit	k5eAaPmAgNnS
a	a	k8xC
změnilo	změnit	k5eAaPmAgNnS
své	svůj	k3xOyFgNnSc1
chování	chování	k1gNnSc1	chování
<g/>
,	,	kIx,
protože	protože	k8xS
k	k	k7c3
vyřešení	vyřešení	k1gNnSc3	vyřešení
problému	problém	k1gInSc2	problém
se	se	k3xPyFc4
toho	ten	k3xDgNnSc2
zatím	zatím	k6eAd1
dělá	dělat	k5eAaImIp3nS
velmi	velmi	k6eAd1
málo	málo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
špatná	špatný	k2eAgFnSc1d1	špatná
<g/>
,	,	kIx,
že	že	k8xS
bychom	by	kYmCp1nP
měli	mít	k5eAaImAgMnP
všichni	všechen	k3xTgMnPc1
panikařit	panikařit	k5eAaImF
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
Politici	politik	k1gMnPc1	politik
a	a	k8xC
zodpovědní	zodpovědný	k2eAgMnPc1d1	zodpovědný
činitelé	činitel	k1gMnPc1	činitel
musí	muset	k5eAaImIp3nP
naslouchat	naslouchat	k5eAaImF
vědcům	vědec	k1gMnPc3	vědec
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
v	v	k7c6
roce	rok	k1gInSc6	rok
2019	#num#	k4
zdůraznili	zdůraznit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
podle	podle	k7c2
Mezivládního	mezivládní	k2eAgInSc2d1	mezivládní
panelu	panel	k1gInSc2	panel
pro	pro	k7c4
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
máme	mít	k5eAaImIp1nP
méně	málo	k6eAd2
než	než	k8xS
12	#num#	k4
let	léto	k1gNnPc2	léto
do	do	k7c2
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nebudeme	být	k5eNaImBp1nP
moci	moct	k5eAaImF
napravit	napravit	k5eAaPmF
naše	náš	k3xOp1gFnPc1
chyby	chyba	k1gFnPc1	chyba
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
používá	používat	k5eAaImIp3nS
často	často	k6eAd1
obrazné	obrazný	k2eAgFnPc4d1	obrazná
analogie	analogie	k1gFnPc4	analogie
k	k	k7c3
zdůraznění	zdůraznění	k1gNnSc3	zdůraznění
svých	svůj	k3xOyFgFnPc2
obav	obava	k1gFnPc2	obava
a	a	k8xC
otevřeně	otevřeně	k6eAd1
mluví	mluvit	k5eAaImIp3nS
k	k	k7c3
obchodním	obchodní	k2eAgMnPc3d1	obchodní
a	a	k8xC
politickým	politický	k2eAgMnPc3d1	politický
vůdcům	vůdce	k1gMnPc3	vůdce
<g/>
,	,	kIx,
často	často	k6eAd1
jim	on	k3xPp3gMnPc3
přímo	přímo	k6eAd1
vytýká	vytýkat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
nečinnost	nečinnost	k1gFnSc4	nečinnost
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
na	na	k7c6
panelu	panel	k1gInSc6	panel
významných	významný	k2eAgInPc2d1	významný
obchodních	obchodní	k2eAgInPc2d1	obchodní
a	a	k8xC
politických	politický	k2eAgMnPc2d1	politický
vůdců	vůdce	k1gMnPc2	vůdce
v	v	k7c6
Davosu	Davos	k1gInSc6	Davos
řekla	říct	k5eAaPmAgFnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc1
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,
zejména	zejména	k9
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
rozhodují	rozhodovat	k5eAaImIp3nP
<g/>
,	,	kIx,
přesně	přesně	k6eAd1
věděli	vědět	k5eAaImAgMnP
<g/>
,	,	kIx,
jaké	jaký	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
cenné	cenný	k2eAgFnPc4d1	cenná
hodnoty	hodnota	k1gFnPc4	hodnota
obětují	obětovat	k5eAaBmIp3nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
pokračovali	pokračovat	k5eAaImAgMnP
v	v	k7c6
získávání	získávání	k1gNnSc6	získávání
nepředstavitelného	představitelný	k2eNgNnSc2d1	nepředstavitelné
množství	množství	k1gNnSc2	množství
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
vy	vy	k3xPp2nPc1
zde	zde	k6eAd1
patříte	patřit	k5eAaImIp2nP
právě	právě	k9
do	do	k7c2
této	tento	k3xDgFnSc2
skupiny	skupina	k1gFnSc2	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekla	říct	k5eAaPmAgFnS
dále	daleko	k6eAd2
<g/>
:	:	kIx,
„	„	k?
<g/>
Chci	chtít	k5eAaImIp1nS
<g/>
,	,	kIx,
abyste	aby	kYmCp2nP
jednali	jednat	k5eAaImAgMnP
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
kdyby	kdyby	kYmCp3nS
byl	být	k5eAaImAgInS
dům	dům	k1gInSc1	dům
v	v	k7c6
plamenech	plamen	k1gInPc6	plamen
–	–	k?
protože	protože	k8xS
tomu	ten	k3xDgNnSc3
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6	říjen
2018	#num#	k4
řekla	říct	k5eAaPmAgFnS
v	v	k7c6
Londýně	Londýn	k1gInSc6	Londýn
<g/>
:	:	kIx,
„	„	k?
<g/>
Čelíme	čelit	k5eAaImIp1nP
okamžité	okamžitý	k2eAgFnSc3d1	okamžitá
bezprecedentní	bezprecedentní	k2eAgFnSc3d1	bezprecedentní
krizi	krize	k1gFnSc3	krize
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
nikdy	nikdy	k6eAd1
nebyla	být	k5eNaImAgFnS
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4
krizi	krize	k1gFnSc4	krize
a	a	k8xC
naši	náš	k3xOp1gMnPc1
vůdci	vůdce	k1gMnPc1	vůdce
se	se	k3xPyFc4
chovají	chovat	k5eAaImIp3nP
jako	jako	k8xC,k8xS
děti	dítě	k1gFnPc1	dítě
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
Zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
také	také	k9
<g/>
,	,	kIx,
že	že	k8xS
strategie	strategie	k1gFnSc1	strategie
přijaté	přijatý	k2eAgInPc1d1	přijatý
různými	různý	k2eAgFnPc7d1	různá
vládami	vláda	k1gFnPc7	vláda
k	k	k7c3
omezení	omezení	k1gNnSc3	omezení
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
na	na	k7c4
1,5	1,5	k4
°	°	k?
C	C	kA
v	v	k7c6
rámci	rámec	k1gInSc6	rámec
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
dohody	dohoda	k1gFnSc2	dohoda
jsou	být	k5eAaImIp3nP
nedostatečné	dostatečný	k2eNgFnPc1d1	nedostatečná
a	a	k8xC
křivka	křivka	k1gFnSc1	křivka
emisí	emise	k1gFnPc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
musí	muset	k5eAaImIp3nS
začít	začít	k5eAaPmF
prudce	prudko	k6eAd1
klesat	klesat	k5eAaImF
nejpozději	pozdě	k6eAd3
do	do	k7c2
roku	rok	k1gInSc2	rok
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6	leden
2019	#num#	k4
prohlásila	prohlásit	k5eAaPmAgFnS
před	před	k7c7
britským	britský	k2eAgInSc7d1	britský
parlamentem	parlament	k1gInSc7	parlament
<g/>
,	,	kIx,
že	že	k8xS
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
musí	muset	k5eAaImIp3nS
přestat	přestat	k5eAaPmF
mluvit	mluvit	k5eAaImF
o	o	k7c4
„	„	k?
<g/>
snižování	snižování	k1gNnSc4	snižování
<g/>
“	“	k?
emisí	emise	k1gFnPc2	emise
a	a	k8xC
začít	začít	k5eAaPmF
přemýšlet	přemýšlet	k5eAaImF
o	o	k7c6
jejich	jejich	k3xOp3gFnSc6
eliminaci	eliminace	k1gFnSc6	eliminace
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6	únor
2019	#num#	k4
na	na	k7c6
konferenci	konference	k1gFnSc6	konference
Evropského	evropský	k2eAgInSc2d1	evropský
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
a	a	k8xC
sociálního	sociální	k2eAgInSc2d1	sociální
výboru	výbor	k1gInSc2	výbor
uvedla	uvést	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
EU	EU	kA
musí	muset	k5eAaImIp3nS
snížit	snížit	k5eAaPmF
své	svůj	k3xOyFgFnPc4
CO2	CO2	k1gFnPc4	CO2
emise	emise	k1gFnSc2	emise
o	o	k7c4
80	#num#	k4
%	%	kIx~
do	do	k7c2
roku	rok	k1gInSc2	rok
2030	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
by	by	kYmCp3nP
byl	být	k5eAaImAgInS
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
cíle	cíl	k1gInSc2	cíl
40	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
stanoven	stanovit	k5eAaPmNgInS
v	v	k7c6
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.
<g/>
V	v	k7c6
prohlášení	prohlášení	k1gNnSc6	prohlášení
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
původně	původně	k6eAd1
zveřejnila	zveřejnit	k5eAaPmAgFnS
na	na	k7c6
své	svůj	k3xOyFgFnSc6
facebookové	facebookový	k2eAgFnSc6d1	facebooková
stránce	stránka	k1gFnSc6	stránka
<g/>
,	,	kIx,
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
vědcem	vědec	k1gMnSc7	vědec
v	v	k7c6
oblasti	oblast	k1gFnSc6	oblast
klimatu	klima	k1gNnSc2	klima
<g/>
:	:	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
pouze	pouze	k6eAd1
posel	posel	k1gMnSc1	posel
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
opakuje	opakovat	k5eAaImIp3nS
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
sdělují	sdělovat	k5eAaImIp3nP
vědci	vědec	k1gMnPc1	vědec
veřejnosti	veřejnost	k1gFnSc2	veřejnost
po	po	k7c4
celá	celý	k2eAgNnPc4d1	celé
desetiletí	desetiletí	k1gNnPc4	desetiletí
<g/>
,	,	kIx,
zatím	zatím	k6eAd1
bez	bez	k7c2
velkého	velký	k2eAgInSc2d1	velký
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
bude	být	k5eAaImBp3nS
každý	každý	k3xTgMnSc1
poslouchal	poslouchat	k5eAaImAgMnS
vědce	vědec	k1gMnPc4	vědec
a	a	k8xC
uzná	uznat	k5eAaPmIp3nS
fakta	faktum	k1gNnPc4	faktum
<g/>
,	,	kIx,
„	„	k?
<g/>
pak	pak	k6eAd1
jsme	být	k5eAaImIp1nP
se	se	k3xPyFc4
my	my	k3xPp1nPc1
(	(	kIx(
<g/>
studenti	student	k1gMnPc1	student
<g/>
)	)	kIx)
budeme	být	k5eAaImBp1nP
moci	moct	k5eAaImF
vrátit	vrátit	k5eAaPmF
do	do	k7c2
školy	škola	k1gFnSc2	škola
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomuto	tento	k3xDgNnSc3
tématu	téma	k1gNnSc3	téma
se	se	k3xPyFc4
vrátila	vrátit	k5eAaPmAgFnS
na	na	k7c6
své	svůj	k3xOyFgFnSc6
plavbě	plavba	k1gFnSc6	plavba
do	do	k7c2
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
na	na	k7c6
uhlíkově	uhlíkově	k6eAd1
neutrální	neutrální	k2eAgFnSc6d1	neutrální
plachetnici	plachetnice	k1gFnSc6	plachetnice
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
na	na	k7c6
plachtách	plachta	k1gFnPc6	plachta
velkými	velký	k2eAgNnPc7d1	velké
písmeny	písmeno	k1gNnPc7	písmeno
slova	slovo	k1gNnSc2	slovo
„	„	k?
<g/>
Spojme	spojit	k5eAaPmRp1nP
se	se	k3xPyFc4
za	za	k7c7
vědou	věda	k1gFnSc7	věda
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
jejích	její	k3xOp3gInPc2
prvních	první	k4xOgInPc2
výroků	výrok	k1gInPc2	výrok
<g/>
,	,	kIx,
když	když	k8xS
dorazila	dorazit	k5eAaPmAgFnS
na	na	k7c4
americkou	americký	k2eAgFnSc4d1	americká
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
zpráva	zpráva	k1gFnSc1	zpráva
pro	pro	k7c4
Donalda	Donald	k1gMnSc4	Donald
Trumpa	Trump	k1gMnSc4	Trump
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
ho	on	k3xPp3gNnSc4
žádá	žádat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
„	„	k?
<g/>
poslouchal	poslouchat	k5eAaImAgMnS
vědu	věda	k1gFnSc4	věda
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Veřejná	veřejný	k2eAgNnPc1d1	veřejné
vystoupení	vystoupení	k1gNnPc1	vystoupení
a	a	k8xC
další	další	k2eAgFnPc1d1	další
akce	akce	k1gFnPc1	akce
==	==	k?
</s>
</p>
<p>
<s>
===	===	k?
Účast	účast	k1gFnSc1	účast
na	na	k7c6
demonstracích	demonstrace	k1gFnPc6	demonstrace
===	===	k?
</s>
</p>
<p>
<s>
Zúčastnila	zúčastnit	k5eAaPmAgFnS
se	se	k3xPyFc4
demonstrace	demonstrace	k1gFnSc1	demonstrace
Rise	Ris	k1gInSc2	Ris
for	forum	k1gNnPc2	forum
Climate	Climat	k1gInSc5	Climat
(	(	kIx(
<g/>
Povstaňte	povstat	k5eAaPmRp2nP
pro	pro	k7c4
klima	klima	k1gNnSc4	klima
<g/>
)	)	kIx)
před	před	k7c7
Evropským	evropský	k2eAgInSc7d1	evropský
parlamentem	parlament	k1gInSc7	parlament
v	v	k7c6
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6	říjen
2018	#num#	k4
vystoupila	vystoupit	k5eAaPmAgFnS
v	v	k7c6
rámci	rámec	k1gInSc6	rámec
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
tzv.	tzv.	kA
Deklarace	deklarace	k1gFnSc2	deklarace
povstání	povstání	k1gNnSc2	povstání
(	(	kIx(
<g/>
Declaration	Declaration	k1gInSc1	Declaration
of	of	k?
Rebellion	Rebellion	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
událo	udát	k5eAaPmAgNnS
v	v	k7c6
Londýně	Londýn	k1gInSc6	Londýn
poblíž	poblíž	k7c2
Westminsterského	Westminsterský	k2eAgInSc2d1	Westminsterský
paláce	palác	k1gInSc2	palác
hnutím	hnutí	k1gNnSc7	hnutí
Extinction	Extinction	k1gInSc1	Extinction
Rebellion	Rebellion	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
vystoupení	vystoupení	k1gNnSc6	vystoupení
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1	jiné
prohlásila	prohlásit	k5eAaPmAgFnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Čelíme	čelit	k5eAaImIp1nP
okamžité	okamžitý	k2eAgFnSc3d1	okamžitá
bezprecedentní	bezprecedentní	k2eAgFnSc3d1	bezprecedentní
krizi	krize	k1gFnSc3	krize
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
přitom	přitom	k6eAd1
nikdo	nikdo	k3yNnSc1
za	za	k7c4
krizi	krize	k1gFnSc4	krize
nepovažuje	považovat	k5eNaImIp3nS
a	a	k8xC
naši	náš	k3xOp1gFnSc4
vůdci	vůdce	k1gMnPc1	vůdce
jednají	jednat	k5eAaImIp3nP
jako	jako	k9
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musíme	muset	k5eAaImIp1nP
se	se	k3xPyFc4
probudit	probudit	k5eAaPmF
a	a	k8xC
změnit	změnit	k5eAaPmF
všechno	všechen	k3xTgNnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
</p>
<p>
<s>
===	===	k?
TEDx	TEDx	k1gInSc1	TEDx
Stockholm	Stockholm	k1gInSc1	Stockholm
===	===	k?
</s>
</p>
<p>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6	prosinec
2018	#num#	k4
natočila	natočit	k5eAaBmAgFnS
v	v	k7c6
rámci	rámec	k1gInSc6	rámec
TEDx	TEDx	k1gInSc1	TEDx
Stockholm	Stockholm	k1gInSc1	Stockholm
přednášku	přednáška	k1gFnSc4	přednáška
„	„	k?
<g/>
School	School	k1gInSc1	School
strike	strike	k1gFnPc2	strike
for	forum	k1gNnPc2	forum
climate	climat	k1gInSc5	climat
–	–	k?
save	save	k1gNnSc6	save
the	the	k?
World	World	k1gInSc1	World
changing	changing	k1gInSc1	changing
the	the	k?
rules	rules	k1gInSc1	rules
<g/>
“	“	k?
(	(	kIx(
<g/>
Školní	školní	k2eAgFnSc1d1	školní
stávka	stávka	k1gFnSc1	stávka
pro	pro	k7c4
klima	klima	k1gNnSc4	klima
–	–	k?
zachraňme	zachránit	k5eAaPmRp1nP
svět	svět	k1gInSc4	svět
změnou	změna	k1gFnSc7	změna
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
vystoupení	vystoupení	k1gNnSc6	vystoupení
mluvila	mluvit	k5eAaImAgFnS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
o	o	k7c6
hrozící	hrozící	k2eAgFnSc6d1	hrozící
změně	změna	k1gFnSc6	změna
klimatu	klima	k1gNnSc2	klima
se	se	k3xPyFc4
dozvěděla	dozvědět	k5eAaPmAgFnS
v	v	k7c6
osmi	osm	k4xCc6
letech	léto	k1gNnPc6	léto
a	a	k8xC
přemýšlela	přemýšlet	k5eAaImAgFnS
<g/>
,	,	kIx,
proč	proč	k6eAd1
o	o	k7c6
takové	takový	k3xDgFnSc6
hrozbě	hrozba	k1gFnSc6	hrozba
nepíší	psát	k5eNaImIp3nP
denně	denně	k6eAd1
noviny	novina	k1gFnPc1	novina
a	a	k8xC
nemluví	mluvit	k5eNaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
ní	on	k3xPp3gFnSc6
na	na	k7c6
všech	všecek	k3xTgFnPc6
televizních	televizní	k2eAgFnPc6d1	televizní
stanicích	stanice	k1gFnPc6	stanice
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
v	v	k7c6
případě	případ	k1gInSc6	případ
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
různí	různý	k2eAgMnPc1d1	různý
lidé	člověk	k1gMnPc1	člověk
jí	on	k3xPp3gFnSc3
radí	radit	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
napřed	napřed	k6eAd1
místo	místo	k7c2
stávkování	stávkování	k1gNnSc2	stávkování
vystudovala	vystudovat	k5eAaPmAgFnS
klimatologii	klimatologie	k1gFnSc4	klimatologie
<g/>
,	,	kIx,
ale	ale	k8xC
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
má	mít	k5eAaImIp3nS
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,
že	že	k8xS
vědecké	vědecký	k2eAgInPc1d1	vědecký
výsledky	výsledek	k1gInPc1	výsledek
jsou	být	k5eAaImIp3nP
již	již	k6eAd1
dnes	dnes	k6eAd1
zcela	zcela	k6eAd1
jasné	jasný	k2eAgNnSc1d1	jasné
a	a	k8xC
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
překáží	překážet	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
popírání	popírání	k1gNnSc3	popírání
těchto	tento	k3xDgInPc2
výsledků	výsledek	k1gInPc2	výsledek
<g/>
,	,	kIx,
nevědomost	nevědomost	k1gFnSc1	nevědomost
a	a	k8xC
nečinnost	nečinnost	k1gFnSc1	nečinnost
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvažuje	uvažovat	k5eAaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
její	její	k3xOp3gFnPc1
děti	dítě	k1gFnPc1	dítě
a	a	k8xC
vnoučata	vnouče	k1gNnPc1	vnouče
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc3
budou	být	k5eAaImBp3nP
ptát	ptát	k5eAaImF
<g/>
,	,	kIx,
proč	proč	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6	rok
2018	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
ještě	ještě	k6eAd1
byl	být	k5eAaImAgInS
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,
nikdo	nikdo	k3yNnSc1
nic	nic	k3yNnSc1
neudělal	udělat	k5eNaPmAgMnS
a	a	k8xC
dospěla	dochvít	k5eAaPmAgFnS
k	k	k7c3
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
nemůžeme	moct	k5eNaImIp1nP
změnit	změnit	k5eAaPmF
svět	svět	k1gInSc4	svět
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
budeme	být	k5eAaImBp1nP
hrát	hrát	k5eAaImF
podle	podle	k7c2
současných	současný	k2eAgNnPc2d1	současné
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,
protože	protože	k8xS
pravidla	pravidlo	k1gNnPc1	pravidlo
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
změněna	změněn	k2eAgNnPc4d1	změněno
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Summit	summit	k1gInSc1	summit
COP24	COP24	k1gFnSc2	COP24
===	===	k?
</s>
</p>
<p>
<s>
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
vystoupila	vystoupit	k5eAaPmAgFnS
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2	prosinec
2018	#num#	k4
v	v	k7c6
rámci	rámec	k1gInSc6	rámec
Konference	konference	k1gFnSc2	konference
OSN	OSN	kA
o	o	k7c6
změně	změna	k1gFnSc6	změna
klimatu	klima	k1gNnSc2	klima
2018	#num#	k4
v	v	k7c6
Katovicích	Katovice	k1gFnPc6	Katovice
(	(	kIx(
<g/>
COP	cop	k1gInSc4	cop
<g/>
24	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
také	také	k6eAd1
vystoupila	vystoupit	k5eAaPmAgFnS
před	před	k7c7
plenárním	plenární	k2eAgNnSc7d1	plenární
zasedáním	zasedání	k1gNnSc7	zasedání
této	tento	k3xDgFnSc2
konference	konference	k1gFnSc2	konference
dne	den	k1gInSc2	den
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2	prosinec
2018	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
projevu	projev	k1gInSc6	projev
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1	jiné
prohlásila	prohlásit	k5eAaPmAgFnS
<g/>
:	:	kIx,
“	“	k?
<g/>
Mluvíte	mluvit	k5eAaImIp2nP
jen	jen	k9
o	o	k7c6
zeleném	zelený	k2eAgInSc6d1	zelený
trvalém	trvalý	k2eAgInSc6d1	trvalý
hospodářském	hospodářský	k2eAgInSc6d1	hospodářský
růstu	růst	k1gInSc6	růst
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
bojíte	bát	k5eAaImIp2nP
<g/>
,	,	kIx,
že	že	k8xS
byste	by	kYmCp2nP
ztratili	ztratit	k5eAaPmAgMnP
popularitu	popularita	k1gFnSc4	popularita
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mluvíte	mluvit	k5eAaImIp2nP
jen	jen	k9
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
pokračovat	pokračovat	k5eAaImF
ve	v	k7c6
špatných	špatný	k2eAgInPc6d1	špatný
nápadech	nápad	k1gInPc6	nápad
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
nás	my	k3xPp1nPc4
dostaly	dostat	k5eAaPmAgInP
do	do	k7c2
tohoto	tento	k3xDgInSc2
zmatku	zmatek	k1gInSc2	zmatek
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
jedinou	jediný	k2eAgFnSc7d1	jediná
smysluplnou	smysluplný	k2eAgFnSc7d1	smysluplná
věcí	věc	k1gFnSc7	věc
je	být	k5eAaImIp3nS
zatažení	zatažení	k1gNnSc1	zatažení
záchranné	záchranný	k2eAgFnSc2d1	záchranná
brzdy	brzda	k1gFnSc2	brzda
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejste	být	k5eNaImIp2nP
dostatečně	dostatečně	k6eAd1
zralí	zralý	k2eAgMnPc1d1	zralý
<g/>
,	,	kIx,
abyste	aby	kYmCp2nP
řekli	říct	k5eAaPmAgMnP
<g/>
,	,	kIx,
jaká	jaký	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
skutečně	skutečně	k6eAd1
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonce	dokonce	k9
i	i	k9
toto	tento	k3xDgNnSc4
břímě	břímě	k1gNnSc4	břímě
necháte	nechat	k5eAaPmIp2nP
na	na	k7c6
nás	my	k3xPp1nPc6
<g/>
,	,	kIx,
dětech	dítě	k1gFnPc6	dítě
<g/>
.	.	kIx.
<g/>
“	“	k?
Během	během	k7c2
summitu	summit	k1gInSc2	summit
se	se	k3xPyFc4
též	též	k9
zúčastnila	zúčastnit	k5eAaPmAgFnS
panelu	panel	k1gInSc6	panel
se	s	k7c7
členy	člen	k1gMnPc7	člen
vědecké	vědecký	k2eAgFnSc2d1	vědecká
iniciativy	iniciativa	k1gFnSc2	iniciativa
„	„	k?
<g/>
Nemáme	mít	k5eNaImIp1nP
čas	čas	k1gInSc4	čas
<g/>
“	“	k?
(	(	kIx(
<g/>
We	We	k1gMnSc1	We
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"
<g/>
t	t	k?
Have	Have	k1gInSc1	Have
Time	Time	k1gInSc1	Time
Foundation	Foundation	k1gInSc4	Foundation
<g/>
)	)	kIx)
<g/>
;	;	kIx,
zde	zde	k6eAd1
mluvila	mluvit	k5eAaImAgFnS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
začalo	začít	k5eAaPmAgNnS
hnutí	hnutí	k1gNnSc4	hnutí
školních	školní	k2eAgFnPc2d1	školní
stávek	stávka	k1gFnPc2	stávka
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Světové	světový	k2eAgNnSc1d1	světové
ekonomické	ekonomický	k2eAgNnSc1d1	ekonomické
fórum	fórum	k1gNnSc1	fórum
v	v	k7c6
Davosu	Davos	k1gInSc6	Davos
===	===	k?
</s>
</p>
<p>
<s>
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2	leden
2019	#num#	k4
dorazila	dorazit	k5eAaPmAgFnS
Greta	Greta	k1gFnSc1	Greta
na	na	k7c4
Světové	světový	k2eAgNnSc4d1	světové
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
fórum	fórum	k1gNnSc4	fórum
do	do	k7c2
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
Davosu	Davos	k1gInSc2	Davos
po	po	k7c6
32	#num#	k4
<g/>
hodinové	hodinový	k2eAgFnSc6d1	hodinová
cestě	cesta	k1gFnSc6	cesta
vlakem	vlak	k1gInSc7	vlak
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2
mnoha	mnoho	k4c2
delegátů	delegát	k1gMnPc2	delegát
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
dorazili	dorazit	k5eAaPmAgMnP
více	hodně	k6eAd2
než	než	k8xS
1	#num#	k4
500	#num#	k4
lety	léto	k1gNnPc7	léto
soukromých	soukromý	k2eAgNnPc2d1	soukromé
tryskových	tryskový	k2eAgNnPc2d1	tryskové
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Davosu	Davos	k1gInSc6	Davos
pokračovala	pokračovat	k5eAaImAgNnP
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
kampani	kampaň	k1gFnSc6	kampaň
za	za	k7c4
záchranu	záchrana	k1gFnSc4	záchrana
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejímu	její	k3xOp3gInSc3
příjezdu	příjezd	k1gInSc3	příjezd
do	do	k7c2
Davosu	Davos	k1gInSc2	Davos
věnovala	věnovat	k5eAaPmAgNnP,k5eAaImAgNnP
světová	světový	k2eAgNnPc1d1	světové
média	médium	k1gNnPc1	médium
větší	veliký	k2eAgFnSc1d2	veliký
pozornost	pozornost	k1gFnSc1	pozornost
než	než	k8xS
většině	většina	k1gFnSc3	většina
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6	rámec
panelu	panel	k1gInSc2	panel
v	v	k7c6
Davosu	Davos	k1gInSc6	Davos
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1	jiné
prohlásila	prohlásit	k5eAaPmAgFnS
před	před	k7c4
účastníky	účastník	k1gMnPc4	účastník
<g/>
:	:	kIx,
“	“	k?
<g/>
Někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc1
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,
zejména	zejména	k9
někteří	některý	k3yIgMnPc1
rozhodující	rozhodující	k2eAgMnPc1d1	rozhodující
činitelé	činitel	k1gMnPc1	činitel
<g/>
,	,	kIx,
přesně	přesně	k6eAd1
věděli	vědět	k5eAaImAgMnP
<g/>
,	,	kIx,
jaké	jaký	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
nedozírné	dozírný	k2eNgFnPc4d1	nedozírná
hodnoty	hodnota	k1gFnPc4	hodnota
obětují	obětovat	k5eAaBmIp3nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohli	moct	k5eAaImAgMnP
pokračovat	pokračovat	k5eAaImF
ve	v	k7c6
vydělávání	vydělávání	k1gNnSc6	vydělávání
nepředstavitelného	představitelný	k2eNgNnSc2d1	nepředstavitelné
množství	množství	k1gNnSc2	množství
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.
</s>
<s desamb="1">
Myslím	myslet	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2
vás	vy	k3xPp2nPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
zde	zde	k6eAd1
sedíte	sedit	k5eAaImIp2nP
<g/>
,	,	kIx,
do	do	k7c2
této	tento	k3xDgFnSc2
skupiny	skupina	k1gFnSc2	skupina
lidí	člověk	k1gMnPc2	člověk
patří	patřit	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
„	„	k?
<g/>
Později	pozdě	k6eAd2
v	v	k7c6
týdnu	týden	k1gInSc6	týden
varovala	varovat	k5eAaImAgFnS
globální	globální	k2eAgInSc4d1	globální
vůdce	vůdce	k1gMnPc4	vůdce
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,
„	„	k?
<g/>
Nechci	chtít	k5eNaImIp1nS
<g/>
,	,	kIx,
abyste	aby	kYmCp2nP
nám	my	k3xPp1nPc3
dávali	dávat	k5eAaImAgMnP
naději	naděje	k1gFnSc4	naděje
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chci	chtít	k5eAaImIp1nS
<g/>
,	,	kIx,
abyste	aby	kYmCp2nP
začali	začít	k5eAaPmAgMnP
panikařit	panikařit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chci	chtít	k5eAaImIp1nS
<g/>
,	,	kIx,
abyste	aby	kYmCp2nP
pocítili	pocítit	k5eAaPmAgMnP
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
cítím	cítit	k5eAaImIp1nS
každý	každý	k3xTgInSc4
den	den	k1gInSc4	den
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chci	chtít	k5eAaImIp1nS
<g/>
,	,	kIx,
abyste	aby	kYmCp2nP
jednali	jednat	k5eAaImAgMnP
tak	tak	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
v	v	k7c6
krizi	krize	k1gFnSc6	krize
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chci	chtít	k5eAaImIp1nS
<g/>
,	,	kIx,
abyste	aby	kYmCp2nP
jednali	jednat	k5eAaImAgMnP
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
když	když	k8xS
vám	vy	k3xPp2nPc3
hoří	hořet	k5eAaImIp3nS
dům	dům	k1gInSc1	dům
–	–	k?
protože	protože	k8xS
tomu	ten	k3xDgNnSc3
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
“	“	k?
V	v	k7c6
článku	článek	k1gInSc6	článek
pro	pro	k7c4
deník	deník	k1gInSc4	deník
The	The	k1gMnSc1	The
Guardian	Guardian	k1gMnSc1	Guardian
v	v	k7c6
lednu	leden	k1gInSc6	leden
2019	#num#	k4
napsala	napsat	k5eAaBmAgFnS,k5eAaPmAgFnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Podle	podle	k7c2
IPCC	IPCC	kA
(	(	kIx(
<g/>
Mezivládního	mezivládní	k2eAgInSc2d1	mezivládní
panelu	panel	k1gInSc2	panel
pro	pro	k7c4
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
<g/>
)	)	kIx)
máme	mít	k5eAaImIp1nP
méně	málo	k6eAd2
než	než	k8xS
12	#num#	k4
let	léto	k1gNnPc2	léto
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
abychom	aby	kYmCp1nP
odvrátili	odvrátit	k5eAaPmAgMnP
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nebudeme	být	k5eNaImBp1nP
schopni	schopen	k2eAgMnPc1d1	schopen
napravit	napravit	k5eAaPmF
naše	náš	k3xOp1gFnPc4
chyby	chyba	k1gFnPc4	chyba
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2	doba
musí	muset	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
k	k	k7c3
nebývalým	nebývalý	k2eAgFnPc3d1,k2eNgFnPc3d1	nebývalá
změnám	změna	k1gFnPc3	změna
ve	v	k7c6
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,
včetně	včetně	k7c2
snížení	snížení	k1gNnSc2	snížení
našich	náš	k3xOp1gFnPc2
emisí	emise	k1gFnPc2	emise
CO2	CO2	k1gMnPc1	CO2
nejméně	málo	k6eAd3
o	o	k7c4
50	#num#	k4
%	%	kIx~
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ubytování	ubytování	k1gNnSc4	ubytování
měla	mít	k5eAaImAgFnS
zařízeno	zařízen	k2eAgNnSc4d1	zařízeno
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1	další
klimatology	klimatolog	k1gMnPc7	klimatolog
<g/>
,	,	kIx,
ve	v	k7c6
stanu	stan	k1gInSc6	stan
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Evropský	evropský	k2eAgInSc1d1	evropský
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
a	a	k8xC
sociální	sociální	k2eAgInSc1d1	sociální
výbor	výbor	k1gInSc1	výbor
===	===	k?
</s>
</p>
<p>
<s>
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2	únor
2019	#num#	k4
mluvila	mluvit	k5eAaImAgFnS
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
na	na	k7c6
konferenci	konference	k1gFnSc6	konference
Evropského	evropský	k2eAgInSc2d1	evropský
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
a	a	k8xC
sociálního	sociální	k2eAgInSc2d1	sociální
výboru	výbor	k1gInSc2	výbor
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2	přítomnost
předsedy	předseda	k1gMnSc2	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Junckera	Junckero	k1gNnPc4	Junckero
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
vystoupení	vystoupení	k1gNnSc6	vystoupení
řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
má	mít	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
k	k	k7c3
omezení	omezení	k1gNnSc3	omezení
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
na	na	k7c6
méně	málo	k6eAd2
než	než	k8xS
dva	dva	k4xCgInPc1
stupně	stupeň	k1gInPc1	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
předpokládá	předpokládat	k5eAaImIp3nS
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
EU	EU	kA
snížit	snížit	k5eAaPmF
své	svůj	k3xOyFgFnPc4
emise	emise	k1gFnPc4	emise
CO2	CO2	k1gFnSc3	CO2
emise	emise	k1gFnSc2	emise
do	do	k7c2
roku	rok	k1gInSc2	rok
2030	#num#	k4
o	o	k7c4
80	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k8xS
<g />
.	.	kIx.
</s>
<s hack="1">
EU	EU	kA
slíbila	slíbit	k5eAaPmAgFnS
v	v	k7c6
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.
„	„	k?
<g/>
Pokud	pokud	k8xS
tak	tak	k6eAd1
neučiníme	učinit	k5eNaPmIp1nP
<g/>
,	,	kIx,
<g/>
“	“	k?
řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
„	„	k?
<g/>
vše	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
zůstane	zůstat	k5eAaPmIp3nS
z	z	k7c2
odkazu	odkaz	k1gInSc2	odkaz
našich	náš	k3xOp1gMnPc2
současných	současný	k2eAgMnPc2d1	současný
politických	politický	k2eAgMnPc2d1	politický
vůdců	vůdce	k1gMnPc2	vůdce
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
povědomí	povědomí	k1gNnSc1	povědomí
o	o	k7c6
největším	veliký	k2eAgNnSc6d3	veliký
selhání	selhání	k1gNnSc6	selhání
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1	lidská
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.
<g/>
“	“	k?
Po	po	k7c6
tomto	tento	k3xDgNnSc6
vystoupení	vystoupení	k1gNnSc6	vystoupení
se	se	k3xPyFc4
také	také	k9
účastnila	účastnit	k5eAaImAgFnS
klimatického	klimatický	k2eAgInSc2d1	klimatický
protestu	protest	k1gInSc2	protest
7	#num#	k4
500	#num#	k4
belgických	belgický	k2eAgMnPc2d1	belgický
studentů	student	k1gMnPc2	student
v	v	k7c6
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Návštěva	návštěva	k1gFnSc1	návštěva
Berlína	Berlín	k1gInSc2	Berlín
===	===	k?
</s>
</p>
<p>
<s>
O	o	k7c6
víkendu	víkend	k1gInSc6	víkend
29	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2	březen
2019	#num#	k4
navštívila	navštívit	k5eAaPmAgFnS
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
německé	německý	k2eAgNnSc4d1	německé
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Berlín	Berlín	k1gInSc4	Berlín
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
pátek	pátek	k1gInSc4	pátek
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2	březen
vystoupila	vystoupit	k5eAaPmAgFnS
před	před	k7c7
asi	asi	k9
25	#num#	k4
000	#num#	k4
lidmi	člověk	k1gMnPc7	člověk
v	v	k7c6
blízkosti	blízkost	k1gFnSc6	blízkost
Braniborské	braniborský	k2eAgFnSc2d1	Braniborská
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,
kde	kde	k6eAd1
prohlásila	prohlásit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
žijeme	žít	k5eAaImIp1nP
v	v	k7c6
podivném	podivný	k2eAgInSc6d1	podivný
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,
kde	kde	k6eAd1
děti	dítě	k1gFnPc1	dítě
musí	muset	k5eAaImIp3nP
obětovat	obětovat	k5eAaBmF
své	svůj	k3xOyFgNnSc4
vlastní	vlastní	k2eAgNnSc4d1	vlastní
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
protestovaly	protestovat	k5eAaBmAgInP
proti	proti	k7c3
zničení	zničení	k1gNnSc3	zničení
své	svůj	k3xOyFgFnSc2
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
přispěli	přispět	k5eAaPmAgMnP
k	k	k7c3
této	tento	k3xDgFnSc3
krizi	krize	k1gFnSc3	krize
nejméně	málo	k6eAd3
<g/>
,	,	kIx,
jí	on	k3xPp3gFnSc3
budou	být	k5eAaImBp3nP
v	v	k7c6
budoucnosti	budoucnost	k1gFnSc6	budoucnost
zasažení	zasažení	k1gNnSc2	zasažení
nejvíce	hodně	k6eAd3,k6eAd1
<g/>
.	.	kIx.
<g/>
“	“	k?
Po	po	k7c6
projevu	projev	k1gInSc6	projev
navštívila	navštívit	k5eAaPmAgFnS
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
spolu	spolu	k6eAd1
s	s	k7c7
klimatickou	klimatický	k2eAgFnSc7d1	klimatická
aktivistkou	aktivistka	k1gFnSc7	aktivistka
Luisou	Luisa	k1gFnSc7	Luisa
Neubauerovou	Neubauerová	k1gFnSc7	Neubauerová
Potsdamský	Potsdamský	k2eAgInSc4d1	Potsdamský
institut	institut	k1gInSc4	institut
pro	pro	k7c4
výzkum	výzkum	k1gInSc4	výzkum
klimatických	klimatický	k2eAgInPc2d1	klimatický
dopadů	dopad	k1gInPc2	dopad
a	a	k8xC
setkaly	setkat	k5eAaPmAgFnP
se	se	k3xPyFc4
tam	tam	k6eAd1
s	s	k7c7
předními	přední	k2eAgMnPc7d1	přední
vědci	vědec	k1gMnPc7	vědec
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
sobotu	sobota	k1gFnSc4	sobota
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2	březen
dostala	dostat	k5eAaPmAgFnS
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
speciální	speciální	k2eAgFnSc4d1	speciální
cenu	cena	k1gFnSc4	cena
„	„	k?
<g/>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
kamera	kamera	k1gFnSc1	kamera
<g/>
“	“	k?
na	na	k7c6
každoroční	každoroční	k2eAgFnSc6d1	každoroční
německé	německý	k2eAgFnSc6d1	německá
přehlídce	přehlídka	k1gFnSc6	přehlídka
filmů	film	k1gInPc2	film
a	a	k8xC
televizí	televize	k1gFnPc2	televize
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgInSc6
projevu	projev	k1gInSc6	projev
při	při	k7c6
přebírání	přebírání	k1gNnSc6	přebírání
této	tento	k3xDgFnSc2
ceny	cena	k1gFnSc2	cena
na	na	k7c6
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
galavečeru	galavečer	k1gInSc6	galavečer
vyzvala	vyzvat	k5eAaPmAgFnS
celebrity	celebrita	k1gFnPc1	celebrita
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
využily	využít	k5eAaPmAgFnP
svého	své	k1gNnSc2	své
vlivu	vliv	k1gInSc6	vliv
a	a	k8xC
pomohly	pomoct	k5eAaPmAgInP
jí	on	k3xPp3gFnSc3
v	v	k7c6
aktivitách	aktivita	k1gFnPc6	aktivita
na	na	k7c4
záchranu	záchrana	k1gFnSc4	záchrana
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Před	před	k7c7
vedoucími	vedoucí	k2eAgMnPc7d1	vedoucí
představiteli	představitel	k1gMnPc7	představitel
EU	EU	kA
===	===	k?
</s>
</p>
<p>
<s>
Na	na	k7c6
zasedání	zasedání	k1gNnSc6	zasedání
v	v	k7c6
Evropském	evropský	k2eAgInSc6d1	evropský
parlamentu	parlament	k1gInSc6	parlament
ve	v	k7c6
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
v	v	k7c6
dubnu	duben	k1gInSc6	duben
2019	#num#	k4
s	s	k7c7
poslanci	poslanec	k1gMnPc7	poslanec
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC
představiteli	představitel	k1gMnSc3	představitel
EU	EU	kA
vystoupila	vystoupit	k5eAaPmAgFnS
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
s	s	k7c7
kritickým	kritický	k2eAgInSc7d1	kritický
příspěvkem	příspěvek	k1gInSc7	příspěvek
<g/>
:	:	kIx,
„	„	k?
<g/>
Již	již	k6eAd1
na	na	k7c6
třech	tři	k4xCgInPc6
mimořádných	mimořádný	k2eAgInPc6d1	mimořádný
summitech	summit	k1gInPc6	summit
řešíte	řešit	k5eAaImIp2nP
brexit	brexit	k5eAaPmF,k5eAaImF,k5eAaBmF
a	a	k8xC
na	na	k7c6
žádném	žádný	k3yNgInSc6
summitu	summit	k1gInSc6	summit
jste	být	k5eAaImIp2nP
zatím	zatím	k6eAd1
neřešili	řešit	k5eNaImAgMnP
problém	problém	k1gInSc4	problém
klimatické	klimatický	k2eAgFnSc2d1	klimatická
krize	krize	k1gFnSc2	krize
a	a	k8xC
ohrožení	ohrožení	k1gNnSc2	ohrožení
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
ní	on	k3xPp3gFnSc2
nejsou	být	k5eNaImIp3nP
diskuse	diskuse	k1gFnPc1	diskuse
o	o	k7c6
klimatických	klimatický	k2eAgFnPc6d1	klimatická
změnách	změna	k1gFnPc6	změna
na	na	k7c6
summitech	summit	k1gInPc6	summit
EU	EU	kA
dominantní	dominantní	k2eAgFnPc1d1	dominantní
<g/>
,	,	kIx,
jiné	jiný	k2eAgFnPc1d1	jiná
otázky	otázka	k1gFnPc1	otázka
mají	mít	k5eAaImIp3nP
přednost	přednost	k1gFnSc4	přednost
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračovala	pokračovat	k5eAaImAgNnP
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
:	:	kIx,
„	„	k?
<g/>
Míra	Míra	k1gFnSc1	Míra
vymírání	vymírání	k1gNnPc2	vymírání
volně	volně	k6eAd1
žijících	žijící	k2eAgMnPc2d1	žijící
živočichů	živočich	k1gMnPc2	živočich
je	být	k5eAaImIp3nS
až	až	k9
10	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
krát	krát	k6eAd1
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
než	než	k8xS
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4
normální	normální	k2eAgFnSc4d1	normální
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
až	až	k9
200	#num#	k4
druhů	druh	k1gInPc2	druh
vyhyne	vyhynout	k5eAaPmIp3nS
každý	každý	k3xTgInSc1
den	den	k1gInSc1	den
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
„	„	k?
<g/>
degradace	degradace	k1gFnSc2	degradace
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,
odlesňování	odlesňování	k1gNnSc1	odlesňování
deštného	deštný	k2eAgInSc2d1	deštný
pralesa	prales	k1gInSc2	prales
<g/>
,	,	kIx,
toxické	toxický	k2eAgNnSc1d1	toxické
znečištění	znečištění	k1gNnSc1	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
,	,	kIx,
úbytek	úbytek	k1gInSc4	úbytek
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC
volně	volně	k6eAd1
žijících	žijící	k2eAgMnPc2d1	žijící
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,
acidifikace	acidifikace	k1gFnSc1	acidifikace
našich	náš	k3xOp1gInPc2
oceánů	oceán	k1gInPc2	oceán
...	...	k?
všechny	všechen	k3xTgInPc1
tyto	tento	k3xDgInPc1
faktory	faktor	k1gInPc1	faktor
ukazují	ukazovat	k5eAaImIp3nP
na	na	k7c4
katastrofální	katastrofální	k2eAgInPc4d1	katastrofální
trendy	trend	k1gInPc4	trend
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
řeč	řeč	k1gFnSc4	řeč
byla	být	k5eAaImAgFnS
na	na	k7c6
konci	konec	k1gInSc6	konec
oceněna	ocenit	k5eAaPmNgFnS
30	#num#	k4
vteřinami	vteřina	k1gFnPc7	vteřina
ovací	ovace	k1gFnPc2	ovace
vestoje	vestoje	k6eAd1
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Světový	světový	k2eAgInSc1d1	světový
summit	summit	k1gInSc1	summit
R20	R20	k1gFnSc2	R20
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6	Vídeň
===	===	k?
</s>
</p>
<p>
<s>
V	v	k7c6
květnu	květen	k1gInSc6	květen
2019	#num#	k4
se	se	k3xPyFc4
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
setkala	setkat	k5eAaPmAgFnS
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6	Vídeň
s	s	k7c7
americkým	americký	k2eAgMnSc7d1	americký
hercem	herec	k1gMnSc7	herec
a	a	k8xC
politikem	politik	k1gMnSc7	politik
Arnoldem	Arnold	k1gMnSc7	Arnold
Schwarzeneggerem	Schwarzenegger	k1gMnSc7	Schwarzenegger
(	(	kIx(
<g/>
původem	původ	k1gInSc7	původ
z	z	k7c2
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
)	)	kIx)
<g/>
,	,	kIx,
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
OSN	OSN	kA
Antóniem	Antónium	k1gNnSc7	Antónium
Guterresem	Guterres	k1gMnSc7	Guterres
a	a	k8xC
rakouským	rakouský	k2eAgMnSc7d1	rakouský
prezidentem	prezident	k1gMnSc7	prezident
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Van	van	k1gInSc4	van
der	drát	k5eAaImRp2nS
Bellen	Bellen	k1gInSc4	Bellen
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
při	při	k7c6
zahájení	zahájení	k1gNnSc6	zahájení
R20	R20	k1gFnSc2	R20
Austrian	Austriana	k1gFnPc2	Austriana
World	Worlda	k1gFnPc2	Worlda
Summit	summit	k1gInSc1	summit
<g/>
,	,	kIx,
konference	konference	k1gFnPc1	konference
pořádané	pořádaný	k2eAgFnPc1d1	pořádaná
Schwarzeneggerem	Schwarzeneggero	k1gNnSc7	Schwarzeneggero
s	s	k7c7
cílem	cíl	k1gInSc7	cíl
urychlit	urychlit	k5eAaPmF
pokrok	pokrok	k1gInSc4	pokrok
při	při	k7c6
plnění	plnění	k1gNnSc6	plnění
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6
klimatu	klima	k1gNnSc6	klima
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
vystoupení	vystoupení	k1gNnSc6	vystoupení
citovala	citovat	k5eAaBmAgFnS
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
nejnovější	nový	k2eAgFnSc4d3	nejnovější
zprávu	zpráva	k1gFnSc4	zpráva
IPCC	IPCC	kA
a	a	k8xC
uvedla	uvést	k5eAaPmAgFnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Pokud	pokud	k8xS
neprovedeme	provést	k5eNaPmIp1nP
požadované	požadovaný	k2eAgFnPc4d1	požadovaná
změny	změna	k1gFnPc4	změna
přibližně	přibližně	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2	rok
2030	#num#	k4
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
započne	započnout	k5eAaPmIp3nS
nevratná	vratný	k2eNgFnSc1d1	nevratná
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
už	už	k6eAd1
nebudou	být	k5eNaImBp3nP
lidé	člověk	k1gMnPc1	člověk
moci	moc	k1gFnSc2	moc
ovlivnit	ovlivnit	k5eAaPmF
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bude	být	k5eAaImBp3nS
katastrofální	katastrofální	k2eAgMnSc1d1	katastrofální
<g/>
.	.	kIx.
<g/>
“	“	k?
Akce	akce	k1gFnSc1	akce
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
17	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2
30	#num#	k4
různých	různý	k2eAgFnPc2d1	různá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Mluvené	mluvený	k2eAgNnSc1d1	mluvené
slovo	slovo	k1gNnSc1	slovo
pro	pro	k7c4
hudební	hudební	k2eAgFnSc4d1	hudební
skupinu	skupina	k1gFnSc4	skupina
The	The	k1gFnSc1	The
1975	#num#	k4
===	===	k?
</s>
</p>
<p>
<s>
V	v	k7c6
červenci	červenec	k1gInSc6	červenec
2019	#num#	k4
uzavřela	uzavřít	k5eAaPmAgFnS
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7
anglickou	anglický	k2eAgFnSc7d1	anglická
kapelou	kapela	k1gFnSc7	kapela
The	The	k1gFnSc2	The
1975	#num#	k4
a	a	k8xC
doprovodila	doprovodit	k5eAaPmAgFnS
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
mluveným	mluvený	k2eAgNnSc7d1	mluvené
slovem	slovo	k1gNnSc7	slovo
jejich	jejich	k3xOp3gFnSc4
novou	nový	k2eAgFnSc4d1	nová
skladbu	skladba	k1gFnSc4	skladba
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
závěr	závěr	k1gInSc4	závěr
Greta	Greta	k1gFnSc1	Greta
naléhala	naléhat	k5eAaBmAgFnS,k5eAaImAgFnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Takže	takže	k8xS
všichni	všechen	k3xTgMnPc1
povstaňte	povstat	k5eAaPmRp2nP
<g/>
,	,	kIx,
teď	teď	k6eAd1
je	být	k5eAaImIp3nS
čas	čas	k1gInSc4	čas
na	na	k7c4
občanskou	občanský	k2eAgFnSc4d1	občanská
neposlušnost	neposlušnost	k1gFnSc4	neposlušnost
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4
vzbouřit	vzbouřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
“	“	k?
Výtěžek	výtěžek	k1gInSc1	výtěžek
z	z	k7c2
prodeje	prodej	k1gInSc2	prodej
skladby	skladba	k1gFnSc2	skladba
půjde	jít	k5eAaImIp3nS
na	na	k7c4
žádost	žádost	k1gFnSc4	žádost
Thunbergové	Thunbergový	k2eAgFnSc2d1	Thunbergový
na	na	k7c4
podporu	podpora	k1gFnSc4	podpora
hnutí	hnutí	k1gNnSc2	hnutí
Extinction	Extinction	k1gInSc1	Extinction
Rebellion	Rebellion	k?
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2
Ameriky	Amerika	k1gFnSc2	Amerika
v	v	k7c6
roce	rok	k1gInSc6	rok
2019	#num#	k4
===	===	k?
</s>
</p>
<p>
<s>
Plavba	plavba	k1gFnSc1	plavba
do	do	k7c2
Ameriky	Amerika	k1gFnSc2	Amerika
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
ve	v	k7c6
dnech	den	k1gInPc6	den
14	#num#	k4
<g/>
.	.	kIx.
až	až	k9
28	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2	srpen
2019	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
z	z	k7c2
anglického	anglický	k2eAgInSc2d1	anglický
přístavu	přístav	k1gInSc2	přístav
Plymouth	Plymouth	k1gInSc1	Plymouth
do	do	k7c2
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
hledala	hledat	k5eAaImAgFnS
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
dopravit	dopravit	k5eAaPmF
na	na	k7c4
americký	americký	k2eAgInSc4d1	americký
kontinent	kontinent	k1gInSc4	kontinent
jinak	jinak	k6eAd1
než	než	k8xS
letecky	letecky	k6eAd1
<g/>
,	,	kIx,
nabídl	nabídnout	k5eAaPmAgMnS
jí	jíst	k5eAaImIp3nS
Team	team	k1gInSc4	team
Malizia	Malizium	k1gNnSc2	Malizium
z	z	k7c2
Monaka	Monako	k1gNnSc2	Monako
zdarma	zdarma	k6eAd1
dopravu	doprava	k1gFnSc4	doprava
závodní	závodní	k2eAgFnSc7d1	závodní
plachetnicí	plachetnice	k1gFnSc7	plachetnice
Malizia	Malizium	k1gNnSc2	Malizium
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
plavbu	plavba	k1gFnSc4	plavba
do	do	k7c2
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
měla	mít	k5eAaImAgFnS
loď	loď	k1gFnSc1	loď
Malizia	Malizium	k1gNnSc2	Malizium
II	II	kA
oplachtění	oplachtění	k1gNnSc1	oplachtění
se	s	k7c7
symboly	symbol	k1gInPc7	symbol
cílů	cíl	k1gInPc2	cíl
udržitelného	udržitelný	k2eAgInSc2d1	udržitelný
rozvoje	rozvoj	k1gInSc2	rozvoj
OSN	OSN	kA
a	a	k8xC
hesly	heslo	k1gNnPc7	heslo
„	„	k?
<g/>
Jednotně	jednotně	k6eAd1
za	za	k7c7
vědou	věda	k1gFnSc7	věda
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
FridaysForFuture	FridaysForFutur	k1gMnSc5	FridaysForFutur
<g/>
“	“	k?
a	a	k8xC
dalšími	další	k2eAgNnPc7d1	další
hesly	heslo	k1gNnPc7	heslo
na	na	k7c4
podporu	podpora	k1gFnSc4	podpora
ochrany	ochrana	k1gFnSc2	ochrana
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc1	loď
má	mít	k5eAaImIp3nS
Team	team	k1gInSc1	team
Malizia	Malizius	k1gMnSc2	Malizius
pronajatou	pronajatý	k2eAgFnSc7d1	pronajatá
od	od	k7c2
německého	německý	k2eAgMnSc2d1	německý
majitele	majitel	k1gMnSc2	majitel
Borise	Boris	k1gMnSc2	Boris
Herrmanna	Herrmann	k1gMnSc2	Herrmann
z	z	k7c2
Hamburku	Hamburk	k1gInSc2	Hamburk
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malizia	Malizia	k1gFnSc1	Malizia
II	II	kA
je	být	k5eAaImIp3nS
moderní	moderní	k2eAgFnSc1d1	moderní
18	#num#	k4
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
transoceánská	transoceánský	k2eAgFnSc1d1	transoceánská
závodní	závodní	k2eAgFnSc1d1	závodní
jachta	jachta	k1gFnSc1	jachta
třídy	třída	k1gFnSc2	třída
IMOCA	IMOCA	kA
60	#num#	k4
(	(	kIx(
<g/>
Open	Open	k1gInSc1	Open
60	#num#	k4
<g/>
)	)	kIx)
zhotovená	zhotovený	k2eAgFnSc1d1	zhotovená
z	z	k7c2
karbonu	karbon	k1gInSc2	karbon
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
podle	podle	k7c2
jejích	její	k3xOp3gMnPc2
provozovatelů	provozovatel	k1gMnPc2	provozovatel
nulové	nulový	k2eAgFnSc2d1	nulová
emise	emise	k1gFnSc2	emise
CO	co	k9
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
výrobce	výrobce	k1gMnSc2	výrobce
je	být	k5eAaImIp3nS
zhotovená	zhotovený	k2eAgFnSc1d1	zhotovená
částečně	částečně	k6eAd1
z	z	k7c2
recyklovaných	recyklovaný	k2eAgInPc2d1	recyklovaný
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
získávání	získávání	k1gNnSc4	získávání
potřebné	potřebný	k2eAgFnSc2d1	potřebná
energie	energie	k1gFnSc2	energie
jsou	být	k5eAaImIp3nP
na	na	k7c6
lodi	loď	k1gFnSc6	loď
využívány	využívat	k5eAaPmNgFnP,k5eAaImNgFnP
solární	solární	k2eAgFnPc1d1	solární
energie	energie	k1gFnPc1	energie
a	a	k8xC
vodní	vodní	k2eAgFnPc1d1	vodní
turbíny	turbína	k1gFnPc1	turbína
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
předpisů	předpis	k1gInPc2	předpis
je	být	k5eAaImIp3nS
loď	loď	k1gFnSc1	loď
vybavena	vybavit	k5eAaPmNgFnS
i	i	k9
dieselovým	dieselový	k2eAgInSc7d1	dieselový
motorem	motor	k1gInSc7	motor
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
na	na	k7c4
přání	přání	k1gNnSc4	přání
Thunbergové	Thunbergový	k2eAgNnSc4d1	Thunbergový
na	na	k7c4
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
zapečetěn	zapečetit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc1	loď
má	mít	k5eAaImIp3nS
velmi	velmi	k6eAd1
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
vybavení	vybavení	k1gNnSc1	vybavení
<g/>
,	,	kIx,
vše	všechen	k3xTgNnSc1
je	být	k5eAaImIp3nS
podřízeno	podřízen	k2eAgNnSc4d1	podřízeno
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
její	její	k3xOp3gFnSc1
váha	váha	k1gFnSc1	váha
co	co	k9
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
<g/>
;	;	kIx,
nemá	mít	k5eNaImIp3nS
ani	ani	k8xC
sociální	sociální	k2eAgNnSc4d1	sociální
zařízení	zařízení	k1gNnSc4	zařízení
či	či	k8xC
sprchu	sprcha	k1gFnSc4	sprcha
<g/>
,	,	kIx,
strava	strava	k1gFnSc1	strava
je	být	k5eAaImIp3nS
vyráběna	vyrábět	k5eAaImNgFnS
pouze	pouze	k6eAd1
ze	z	k7c2
suchých	suchý	k2eAgInPc2d1	suchý
polotovarů	polotovar	k1gInPc2	polotovar
a	a	k8xC
vařící	vařící	k2eAgFnSc2d1	vařící
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Thunbergovou	Thunbergový	k2eAgFnSc4d1	Thunbergový
byla	být	k5eAaImAgFnS
zajištěna	zajistit	k5eAaPmNgFnS
veganská	veganský	k2eAgFnSc1d1	veganská
suchá	suchý	k2eAgFnSc1d1	suchá
strava	strava	k1gFnSc1	strava
a	a	k8xC
lůžko	lůžko	k1gNnSc1	lůžko
bylo	být	k5eAaImAgNnS
vybaveno	vybavit	k5eAaPmNgNnS
závěsem	závěs	k1gInSc7	závěs
a	a	k8xC
měkčí	měkký	k2eAgFnSc7d2	měkčí
matrací	matrace	k1gFnSc7	matrace
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
byl	být	k5eAaImAgMnS
velitelem	velitel	k1gMnSc7	velitel
lodi	loď	k1gFnSc2	loď
skipper	skipper	k1gMnSc1	skipper
Boris	Boris	k1gMnSc1	Boris
Herrmann	Herrmann	k1gMnSc1	Herrmann
a	a	k8xC
doprovázel	doprovázet	k5eAaImAgMnS
ho	on	k3xPp3gMnSc4
vedoucí	vedoucí	k1gMnSc1	vedoucí
Teamu	team	k1gInSc2	team
Malizia	Malizium	k1gNnSc2	Malizium
Pierre	Pierr	k1gMnSc5	Pierr
Casiraghi	Casiragh	k1gMnSc5	Casiragh
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
synem	syn	k1gMnSc7	syn
princezny	princezna	k1gFnSc2	princezna
Caroline	Carolin	k1gInSc5	Carolin
Monacké	monacký	k2eAgFnSc2d1	monacká
a	a	k8xC
jejího	její	k3xOp3gNnSc2
zemřelého	zemřelý	k2eAgNnSc2d1	zemřelé
druhého	druhý	k4xOgMnSc4
manžela	manžel	k1gMnSc4	manžel
Stefana	Stefan	k1gMnSc4	Stefan
Casiraghiho	Casiraghi	k1gMnSc4	Casiraghi
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
samotné	samotný	k2eAgFnSc2d1	samotná
dvoučlenné	dvoučlenný	k2eAgFnSc2d1	dvoučlenná
posádky	posádka	k1gFnSc2	posádka
a	a	k8xC
Thunbergové	Thunbergový	k2eAgFnPc1d1	Thunbergový
se	se	k3xPyFc4
plavby	plavba	k1gFnSc2	plavba
účastnili	účastnit	k5eAaImAgMnP
její	její	k3xOp3gMnPc1
otec	otec	k1gMnSc1	otec
Svante	Svant	k1gInSc5	Svant
Thunberg	Thunberg	k1gMnSc1	Thunberg
a	a	k8xC
dokumentarista	dokumentarista	k1gMnSc1	dokumentarista
Nathan	Nathan	k1gMnSc1	Nathan
Grossman	Grossman	k1gMnSc1	Grossman
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
doprovázelo	doprovázet	k5eAaImAgNnS
loď	loď	k1gFnSc4	loď
17	#num#	k4
plachetnic	plachetnice	k1gFnPc2	plachetnice
<g/>
,	,	kIx,
s	s	k7c7
oplachtěním	oplachtění	k1gNnSc7	oplachtění
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
vyjadřovalo	vyjadřovat	k5eAaImAgNnS
barvou	barva	k1gFnSc7	barva
i	i	k9
nápisy	nápis	k1gInPc1	nápis
všech	všecek	k3xTgInPc2
sedmnáct	sedmnáct	k4xCc4
cílů	cíl	k1gInPc2	cíl
udržitelného	udržitelný	k2eAgInSc2d1	udržitelný
rozvoje	rozvoj	k1gInSc2	rozvoj
OSN	OSN	kA
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Vystoupení	vystoupení	k1gNnSc1	vystoupení
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgMnPc2d1	americký
v	v	k7c6
roce	rok	k1gInSc6	rok
2019	#num#	k4
===	===	k?
</s>
</p>
<p>
<s>
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6	září
vystoupila	vystoupit	k5eAaPmAgFnS
Greta	Greta	k1gFnSc1	Greta
Thunberg	Thunberg	k1gInSc1	Thunberg
před	před	k7c7
výborem	výbor	k1gInSc7	výbor
pro	pro	k7c4
klimatickou	klimatický	k2eAgFnSc4d1	klimatická
krizi	krize	k1gFnSc4	krize
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
USA	USA	kA
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6	září
vystoupila	vystoupit	k5eAaPmAgFnS
na	na	k7c4
Globální	globální	k2eAgInPc4d1	globální
klimatické	klimatický	k2eAgInPc4d1	klimatický
stávce	stávec	k1gInPc4	stávec
a	a	k8xC
to	ten	k3xDgNnSc4
na	na	k7c6
Manhattanu	Manhattan	k1gInSc6	Manhattan
v	v	k7c6
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,
kde	kde	k6eAd1
přišlo	přijít	k5eAaPmAgNnS
přibližně	přibližně	k6eAd1
250	#num#	k4
000	#num#	k4
protestujících	protestující	k2eAgMnPc2d1	protestující
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6	září
vystoupila	vystoupit	k5eAaPmAgFnS
na	na	k7c4
Youth	Youth	k1gInSc4	Youth
Climate	Climat	k1gInSc5	Climat
Summit	summit	k1gInSc1	summit
v	v	k7c6
budově	budova	k1gFnSc6	budova
OSN	OSN	kA
v	v	k7c6
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
</s>
</p>
<p>
<s>
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6	září
měla	mít	k5eAaImAgFnS
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1	další
dvěma	dva	k4xCgMnPc7
zástupci	zástupce	k1gMnPc7	zástupce
mládeže	mládež	k1gFnSc2	mládež
vystoupení	vystoupení	k1gNnSc1	vystoupení
v	v	k7c6
úvodní	úvodní	k2eAgFnSc6d1	úvodní
části	část	k1gFnSc6	část
Summitu	summit	k1gInSc2	summit
OSN	OSN	kA
pro	pro	k7c4
klimatickou	klimatický	k2eAgFnSc4d1	klimatická
akci	akce	k1gFnSc4	akce
<g/>
,	,	kIx,
před	před	k7c7
vrcholnými	vrcholný	k2eAgMnPc7d1	vrcholný
představiteli	představitel	k1gMnPc7	představitel
mnoha	mnoho	k4c2
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6	září
oznámila	oznámit	k5eAaPmAgFnS
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
<g/>
,	,	kIx,
že	že	k8xS
ona	onen	k3xDgNnPc1,k3xPp3gNnPc1
a	a	k8xC
15	#num#	k4
dalších	další	k2eAgMnPc2d1	další
zástupců	zástupce	k1gMnPc2	zástupce
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,
včetně	včetně	k7c2
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
Villaseñ	Villaseñ	k1gInSc1	Villaseñ
<g/>
,	,	kIx,
Catarina	Catarin	k2eAgFnSc5d1	Catarina
Lorenzo	Lorenza	k1gFnSc5	Lorenza
a	a	k8xC
Carl	Carl	k1gMnSc1	Carl
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,
podali	podat	k5eAaPmAgMnP
žalobu	žaloba	k1gFnSc4	žaloba
proti	proti	k7c3
pěti	pět	k4xCc2
zemím	zem	k1gFnPc3	zem
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
nejsou	být	k5eNaImIp3nP
na	na	k7c6
dobré	dobrý	k2eAgFnSc6d1	dobrá
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
splnily	splnit	k5eAaPmAgFnP
cíle	cíl	k1gInSc2	cíl
snížení	snížení	k1gNnSc4	snížení
emisí	emise	k1gFnPc2	emise
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yRgFnPc3,k3yIgFnPc3,k3yQgFnPc3
se	se	k3xPyFc4
zavázaly	zavázat	k5eAaPmAgFnP
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
závazcích	závazek	k1gInPc6	závazek
k	k	k7c3
Pařížské	pařížský	k2eAgFnSc3d1	Pařížská
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
:	:	kIx,
Argentina	Argentina	k1gFnSc1	Argentina
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudní	soudní	k2eAgInSc1d1	soudní
spor	spor	k1gInSc1	spor
napadá	napadat	k5eAaPmIp3nS,k5eAaBmIp3nS,k5eAaImIp3nS
národy	národ	k1gInPc4	národ
podle	podle	k7c2
Úmluvy	úmluva	k1gFnSc2	úmluva
OSN	OSN	kA
o	o	k7c6
právech	právo	k1gNnPc6	právo
dítěte	dítě	k1gNnSc2	dítě
(	(	kIx(
<g/>
konkrétně	konkrétně	k6eAd1
práva	právo	k1gNnSc2	právo
na	na	k7c4
život	život	k1gInSc4	život
<g/>
,	,	kIx,
zdraví	zdraví	k1gNnSc4	zdraví
a	a	k8xC
mír	mír	k1gInSc4	mír
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
bude	být	k5eAaImBp3nS
stížnost	stížnost	k1gFnSc1	stížnost
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,
země	zem	k1gFnPc1	zem
budou	být	k5eAaImBp3nP
vyzvány	vyzván	k2eAgFnPc1d1	vyzvána
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
odpověděly	odpovědět	k5eAaPmAgFnP
<g/>
,	,	kIx,
ale	ale	k8xC
žádné	žádný	k3yNgInPc1
výsledky	výsledek	k1gInPc1	výsledek
soudního	soudní	k2eAgInSc2d1	soudní
sporu	spor	k1gInSc2	spor
nejsou	být	k5eNaImIp3nP
právně	právně	k6eAd1
závazné	závazný	k2eAgFnPc1d1	závazná
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Globální	globální	k2eAgFnSc1d1	globální
klimatická	klimatický	k2eAgFnSc1d1	klimatická
stávka	stávka	k1gFnSc1	stávka
v	v	k7c6
kanadském	kanadský	k2eAgInSc6d1	kanadský
Montréalu	Montréal	k1gInSc6	Montréal
v	v	k7c6
roce	rok	k1gInSc6	rok
2019	#num#	k4
===	===	k?
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2	září
2019	#num#	k4
se	se	k3xPyFc4
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
účastnila	účastnit	k5eAaImAgFnS
v	v	k7c6
kanadském	kanadský	k2eAgInSc6d1	kanadský
Montréalu	Montréal	k1gInSc6	Montréal
Globální	globální	k2eAgFnSc2d1	globální
klimatické	klimatický	k2eAgFnSc2d1	klimatická
stávky	stávka	k1gFnSc2	stávka
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
manifestaci	manifestace	k1gFnSc4	manifestace
pozvala	pozvat	k5eAaPmAgFnS
zástupce	zástupce	k1gMnSc4	zástupce
mládeže	mládež	k1gFnSc2	mládež
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
komentovala	komentovat	k5eAaBmAgFnS
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,
„	„	k?
<g/>
Domorodí	domorodý	k2eAgMnPc1d1	domorodý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
po	po	k7c6
staletí	staletí	k1gNnSc6	staletí
a	a	k8xC
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
chránili	chránit	k5eAaImAgMnP
místní	místní	k2eAgNnSc4d1	místní
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC
samozřejmě	samozřejmě	k6eAd1
musíme	muset	k5eAaImIp1nP
<g/>
,	,	kIx,
protože	protože	k8xS
to	ten	k3xDgNnSc4
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
jsou	být	k5eAaImIp3nP
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
první	první	k4xOgFnSc6
linii	linie	k1gFnSc6	linie
<g/>
,	,	kIx,
takže	takže	k8xS
musíme	muset	k5eAaImIp1nP
být	být	k5eAaImF
schopni	schopen	k2eAgMnPc1d1	schopen
aby	aby	kYmCp3nP
si	se	k3xPyFc3
vyslechli	vyslechnout	k5eAaPmAgMnP
jejich	jejich	k3xOp3gInPc4
hlasy	hlas	k1gInPc4	hlas
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k6eAd1
si	se	k3xPyFc3
myslím	myslet	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
domorodí	domorodý	k2eAgMnPc1d1	domorodý
lidé	člověk	k1gMnPc1	člověk
vedly	vést	k5eAaImAgFnP
tento	tento	k3xDgInSc4
boj	boj	k1gInSc4	boj
<g/>
.	.	kIx.
<g/>
“	“	k?
Úvodní	úvodní	k2eAgFnSc1d1	úvodní
část	část	k1gFnSc1	část
svého	svůj	k3xOyFgInSc2
projevu	projev	k1gInSc2	projev
přednesla	přednést	k5eAaPmAgFnS
ve	v	k7c6
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
vlastní	vlastní	k2eAgFnSc7d1	vlastní
demonstrací	demonstrace	k1gFnSc7	demonstrace
se	se	k3xPyFc4
setkala	setkat	k5eAaPmAgFnS
také	také	k9
s	s	k7c7
kanadským	kanadský	k2eAgMnSc7d1	kanadský
premiérem	premiér	k1gMnSc7	premiér
Justinem	Justin	k1gMnSc7	Justin
Trudeau	Trudeaus	k1gInSc2	Trudeaus
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?
</s>
</p>
<p>
<s>
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
byla	být	k5eAaImAgFnS
v	v	k7c6
květnu	květen	k1gInSc6	květen
2018	#num#	k4
jedním	jeden	k4xCgMnSc7
z	z	k7c2
vítězů	vítěz	k1gMnPc2	vítěz
soutěže	soutěž	k1gFnSc2	soutěž
pro	pro	k7c4
mladé	mladý	k2eAgMnPc4d1	mladý
autory	autor	k1gMnPc4	autor
píšící	píšící	k2eAgMnPc4d1	píšící
o	o	k7c6
klimatu	klima	k1gNnSc6	klima
do	do	k7c2
deníku	deník	k1gInSc2	deník
Svenska	Svensko	k1gNnSc2	Svensko
Dagladet	Dagladeta	k1gFnPc2	Dagladeta
a	a	k8xC
byla	být	k5eAaImAgFnS
jedním	jeden	k4xCgInSc7
ze	z	k7c2
tří	tři	k4xCgInPc2
nominovaných	nominovaný	k2eAgInPc2d1	nominovaný
„	„	k?
<g/>
mladých	mladý	k2eAgMnPc2d1	mladý
hrdinů	hrdina	k1gMnPc2	hrdina
roku	rok	k1gInSc2	rok
2018	#num#	k4
<g/>
“	“	k?
na	na	k7c4
cenu	cena	k1gFnSc4	cena
World	Worlda	k1gFnPc2	Worlda
Nature	Natur	k1gMnSc5	Natur
Foundation	Foundation	k1gInSc1	Foundation
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
také	také	k9
nominována	nominovat	k5eAaBmNgFnS
na	na	k7c4
cenu	cena	k1gFnSc4	cena
pro	pro	k7c4
mladé	mladý	k1gMnPc4	mladý
prosazující	prosazující	k2eAgInSc1d1	prosazující
udržitelný	udržitelný	k2eAgInSc1d1	udržitelný
rozvoj	rozvoj	k1gInSc1	rozvoj
–	–	k?
„	„	k?
<g/>
Dětskou	dětský	k2eAgFnSc4d1	dětská
klimatickou	klimatický	k2eAgFnSc4d1	klimatická
cenu	cena	k1gFnSc4	cena
<g/>
“	“	k?
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
uděluje	udělovat	k5eAaImIp3nS
společnost	společnost	k1gFnSc1	společnost
Telge	Telge	k1gFnSc1	Telge
Energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
cenu	cena	k1gFnSc4	cena
však	však	k9
odmítla	odmítnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
protože	protože	k8xS
finalisté	finalista	k1gMnPc1	finalista
měli	mít	k5eAaImAgMnP
letět	letět	k5eAaImF
na	na	k7c4
udělování	udělování	k1gNnSc4	udělování
ceny	cena	k1gFnSc2	cena
do	do	k7c2
Stockholmu	Stockholm	k1gInSc2	Stockholm
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6	listopad
2018	#num#	k4
získala	získat	k5eAaPmAgFnS
za	za	k7c2
své	svůj	k3xOyFgFnSc2
aktivity	aktivita	k1gFnSc2	aktivita
stipendium	stipendium	k1gNnSc4	stipendium
švédské	švédský	k2eAgFnSc2d1	švédská
organizace	organizace	k1gFnSc2	organizace
Fryhuset	Fryhuseta	k1gFnPc2	Fryhuseta
pro	pro	k7c4
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
jsou	být	k5eAaImIp3nP
vzorem	vzor	k1gInSc7	vzor
mladým	mladý	k2eAgMnPc3d1	mladý
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6	prosinec
2018	#num#	k4
ji	on	k3xPp3gFnSc4
zařadil	zařadit	k5eAaPmAgInS
americký	americký	k2eAgInSc1d1	americký
týdeník	týdeník	k1gInSc1	týdeník
Time	Tim	k1gFnSc2	Tim
mezi	mezi	k7c7
25	#num#	k4
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
teenagerů	teenager	k1gMnPc2	teenager
roku	rok	k1gInSc2	rok
2018	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
dubnu	duben	k1gInSc6	duben
2019	#num#	k4
ji	on	k3xPp3gFnSc4
pak	pak	k6eAd1
zařadil	zařadit	k5eAaPmAgInS
mezi	mezi	k7c4
100	#num#	k4
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
lidí	člověk	k1gMnPc2	člověk
roku	rok	k1gInSc2	rok
2019	#num#	k4
a	a	k8xC
27	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2	květen
2019	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
tehdy	tehdy	k6eAd1
16	#num#	k4
<g/>
letá	letý	k2eAgFnSc1d1	letá
Greta	Greta	k1gFnSc1	Greta
a	a	k8xC
její	její	k3xOp3gNnSc4
působení	působení	k1gNnSc4	působení
hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
vydání	vydání	k1gNnSc2	vydání
tohoto	tento	k3xDgInSc2
časopisu	časopis	k1gInSc2	časopis
<g/>
;	;	kIx,
na	na	k7c6
obálce	obálka	k1gFnSc6	obálka
časopisu	časopis	k1gInSc2	časopis
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
její	její	k3xOp3gFnSc1
fotografie	fotografie	k1gFnSc1	fotografie
v	v	k7c6
zelených	zelený	k2eAgInPc6d1	zelený
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
šatech	šat	k1gInPc6	šat
<g/>
.	.	kIx.
<g/>
Při	při	k7c6
příležitosti	příležitost	k1gFnSc6	příležitost
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
dne	den	k1gInSc2	den
žen	žena	k1gFnPc2	žena
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6	rok
2019	#num#	k4
prohlášena	prohlásit	k5eAaPmNgFnS
za	za	k7c4
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
ženu	žena	k1gFnSc4	žena
roku	rok	k1gInSc2	rok
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocenění	ocenění	k1gNnSc1	ocenění
udělil	udělit	k5eAaPmAgInS
deník	deník	k1gInSc4	deník
Aftonbladet	Aftonbladeta	k1gFnPc2	Aftonbladeta
na	na	k7c6
základě	základ	k1gInSc6	základ
průzkumu	průzkum	k1gInSc2	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
si	se	k3xPyFc3
objednal	objednat	k5eAaPmAgMnS
u	u	k7c2
společnosti	společnost	k1gFnSc2	společnost
Inizio	Inizio	k1gNnSc1	Inizio
<g/>
,	,	kIx,
stejné	stejný	k2eAgNnSc1d1	stejné
ocenění	ocenění	k1gNnSc1	ocenění
jí	on	k3xPp3gFnSc2
udělil	udělit	k5eAaPmAgInS
konkurenční	konkurenční	k2eAgInSc1d1	konkurenční
deník	deník	k1gInSc1	deník
Expressen	Expressen	k2eAgInSc1d1	Expressen
na	na	k7c6
základě	základ	k1gInSc6	základ
hlasování	hlasování	k1gNnSc2	hlasování
poroty	porota	k1gFnSc2	porota
<g/>
.	.	kIx.
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2	březen
získala	získat	k5eAaPmAgFnS
speciální	speciální	k2eAgFnSc4d1	speciální
německou	německý	k2eAgFnSc4d1	německá
Cenu	cena	k1gFnSc4	cena
zlaté	zlatý	k2eAgFnSc2d1	zlatá
kamery	kamera	k1gFnSc2	kamera
za	za	k7c4
ochranu	ochrana	k1gFnSc4	ochrana
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2	duben
2019	#num#	k4
jí	on	k3xPp3gFnSc3
byla	být	k5eAaImAgFnS
udělena	udělit	k5eAaPmNgFnS
Prix	Prix	k1gInSc4	Prix
Liberté	Libertý	k2eAgNnSc1d1	Liberté
ve	v	k7c6
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2	duben
dostala	dostat	k5eAaPmAgFnS
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
organizací	organizace	k1gFnSc7	organizace
Natur	Natur	k1gMnSc1	Natur
og	og	k?
Undgdom	Undgdom	k1gInSc1	Undgdom
<g/>
,	,	kIx,
norskou	norský	k2eAgFnSc4d1	norská
cenu	cena	k1gFnSc4	cena
Fritt	Fritt	k2eAgInSc4d1	Fritt
Ords	Ords	k1gInSc4	Ords
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
cenu	cena	k1gFnSc4	cena
získala	získat	k5eAaPmAgFnS
za	za	k7c4
prosazování	prosazování	k1gNnSc4	prosazování
svobody	svoboda	k1gFnSc2	svoboda
vyjadřování	vyjadřování	k1gNnSc1	vyjadřování
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2	březen
2019	#num#	k4
ji	on	k3xPp3gFnSc4
tří	tři	k4xCgFnPc2
členové	člen	k1gMnPc1	člen
norského	norský	k2eAgInSc2d1	norský
parlamentu	parlament	k1gInSc2	parlament
nominovali	nominovat	k5eAaBmAgMnP
na	na	k7c4
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4
mír	mír	k1gInSc4	mír
za	za	k7c4
rok	rok	k1gInSc4	rok
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanci	poslanec	k1gMnPc1	poslanec
svou	svůj	k3xOyFgFnSc4
nominaci	nominace	k1gFnSc4	nominace
zdůvodnili	zdůvodnit	k5eAaPmAgMnP
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
důsledku	důsledek	k1gInSc6	důsledek
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
naroste	narůst	k5eAaPmIp3nS
počet	počet	k1gInSc1	počet
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,
konfliktů	konflikt	k1gInPc2	konflikt
a	a	k8xC
uprchlíků	uprchlík	k1gMnPc2	uprchlík
a	a	k8xC
nikdo	nikdo	k3yNnSc1
nedělá	dělat	k5eNaImIp3nS
nic	nic	k3yNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tomu	ten	k3xDgNnSc3
zabránil	zabránit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4
mír	mír	k1gInSc4	mír
však	však	k9
nakonec	nakonec	k6eAd1
získal	získat	k5eAaPmAgMnS
etiopský	etiopský	k2eAgMnSc1d1	etiopský
premiér	premiér	k1gMnSc1	premiér
Abiy	Abia	k1gFnSc2	Abia
Ahmed	Ahmed	k1gMnSc1	Ahmed
a	a	k8xC
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
obdržela	obdržet	k5eAaPmAgFnS
„	„	k?
<g/>
pouze	pouze	k6eAd1
<g/>
”	”	k?
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
Alternativní	alternativní	k2eAgFnSc4d1	alternativní
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
(	(	kIx(
<g/>
Right	Right	k2eAgInSc1d1	Right
Livelihood	Livelihood	k1gInSc1	Livelihood
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
)	)	kIx)
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
inspirovala	inspirovat	k5eAaBmAgFnS
a	a	k8xC
zesílila	zesílit	k5eAaPmAgFnS
požadavky	požadavek	k1gInPc7	požadavek
naléhavého	naléhavý	k2eAgNnSc2d1	naléhavé
řešení	řešení	k1gNnSc2	řešení
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
ze	z	k7c2
strany	strana	k1gFnSc2	strana
politiků	politik	k1gMnPc2	politik
v	v	k7c6
souladu	soulad	k1gInSc6	soulad
s	s	k7c7
vědeckými	vědecký	k2eAgInPc7d1	vědecký
fakty	fakt	k1gInPc7	fakt
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,
proč	proč	k6eAd1
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4
mír	mír	k1gInSc4	mír
neobdržela	obdržet	k5eNaPmAgFnS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
podle	podle	k7c2
expertů	expert	k1gMnPc2	expert
především	především	k9
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Norský	norský	k2eAgInSc1d1	norský
výbor	výbor	k1gInSc1	výbor
více	hodně	k6eAd2
držel	držet	k5eAaImAgInS
původního	původní	k2eAgNnSc2d1	původní
Nobelova	Nobelův	k2eAgNnSc2d1	Nobelovo
zadání	zadání	k1gNnSc2	zadání
<g/>
,	,	kIx,
za	za	k7c4
jaké	jaký	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
činy	čin	k1gInPc4	čin
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
toto	tento	k3xDgNnSc4
ocenění	ocenění	k1gNnSc4	ocenění
udělováno	udělován	k2eAgNnSc4d1	udělováno
<g/>
.	.	kIx.
<g/>
Dne	den	k1gInSc2	den
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2	červen
2019	#num#	k4
jí	on	k3xPp3gFnSc2
organizace	organizace	k1gFnSc2	organizace
Amnesty	Amnest	k1gInPc4	Amnest
International	International	k1gFnSc1	International
udělila	udělit	k5eAaPmAgFnS
své	svůj	k3xOyFgNnSc4
nejprestižnější	prestižní	k2eAgNnSc4d3	nejprestižnější
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
,	,	kIx,
cenu	cena	k1gFnSc4	cena
Ambassador	Ambassadora	k1gFnPc2	Ambassadora
of	of	k?
Conscience	Conscienec	k1gInSc2	Conscienec
Award	Awarda	k1gFnPc2	Awarda
(	(	kIx(
<g/>
Cenu	cena	k1gFnSc4	cena
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
svědomí	svědomí	k1gNnSc2	svědomí
<g/>
)	)	kIx)
<g/>
,	,	kIx,
za	za	k7c4
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6
oblasti	oblast	k1gFnSc6	oblast
klimatického	klimatický	k2eAgNnSc2d1	klimatické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocenění	ocenění	k1gNnSc4	ocenění
získala	získat	k5eAaPmAgFnS
spolu	spolu	k6eAd1
se	s	k7c7
studentským	studentský	k2eAgNnSc7d1	studentské
hnutím	hnutí	k1gNnSc7	hnutí
Fridays	Fridaysa	k1gFnPc2	Fridaysa
for	forum	k1gNnPc2	forum
Future	Futur	k1gInSc5	Futur
<g/>
.	.	kIx.
</s>
<s desamb="1">
Belgická	belgický	k2eAgFnSc1d1	belgická
Univerzita	univerzita	k1gFnSc1	univerzita
Mons	Monsa	k1gFnPc2	Monsa
ji	on	k3xPp3gFnSc4
udělila	udělit	k5eAaPmAgFnS
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
<g/>
.	.	kIx.
<g/>
Dne	den	k1gInSc2	den
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2	červenec
2019	#num#	k4
jí	on	k3xPp3gFnSc3
byla	být	k5eAaImAgFnS
Královskou	královský	k2eAgFnSc4d1	královská
skotskou	skotská	k1gFnSc4	skotská
geografickou	geografický	k2eAgFnSc7d1	geografická
společností	společnost	k1gFnSc7	společnost
udělena	udělen	k2eAgFnSc1d1	udělena
medaile	medaile	k1gFnSc1	medaile
Geddes	Geddes	k1gMnSc1	Geddes
Environment	Environment	k1gMnSc1	Environment
Medal	Medal	k1gMnSc1	Medal
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
získala	získat	k5eAaPmAgFnS
automaticky	automaticky	k6eAd1
také	také	k9
čestné	čestný	k2eAgNnSc4d1	čestné
stipendium	stipendium	k1gNnSc4	stipendium
<g/>
.	.	kIx.
<g/>
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
byla	být	k5eAaImAgFnS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
patnácti	patnáct	k4xCc2
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
vybrány	vybrat	k5eAaPmNgFnP
na	na	k7c4
obálku	obálka	k1gFnSc4	obálka
britského	britský	k2eAgInSc2d1	britský
časopisu	časopis	k1gInSc2	časopis
British	British	k1gMnSc1	British
Vogue	Vogue	k1gFnSc1	Vogue
ze	z	k7c2
září	září	k1gNnSc2	září
2019	#num#	k4
<g/>
;	;	kIx,
číslo	číslo	k1gNnSc4	číslo
připravovala	připravovat	k5eAaImAgFnS
hostující	hostující	k2eAgFnSc1d1	hostující
redaktorka	redaktorka	k1gFnSc1	redaktorka
Meghan	Meghan	k1gInSc1	Meghan
<g/>
,	,	kIx,
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
ze	z	k7c2
Sussexu	Sussex	k1gInSc2	Sussex
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6	květen
2019	#num#	k4
vydala	vydat	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1	společnost
Vice	vika	k1gFnSc6	vika
třicetiminutový	třicetiminutový	k2eAgInSc4d1	třicetiminutový
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
„	„	k?
<g/>
Make	Mak	k1gInSc2	Mak
the	the	k?
World	World	k1gMnSc1	World
Greta	Greta	k1gMnSc1	Greta
Again	Again	k1gMnSc1	Again
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnuje	zahrnovat	k5eAaImIp3nS
rozhovory	rozhovor	k1gInPc4	rozhovor
s	s	k7c7
řadou	řada	k1gFnSc7	řada
mladých	mladý	k2eAgMnPc2d1	mladý
vůdců	vůdce	k1gMnPc2	vůdce
klimatických	klimatický	k2eAgInPc2d1	klimatický
protestů	protest	k1gInPc2	protest
v	v	k7c6
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Ohlasy	ohlas	k1gInPc1	ohlas
==	==	k?
</s>
</p>
<p>
<s>
===	===	k?
„	„	k?
<g/>
Efekt	efekt	k1gInSc1	efekt
Grety	Greta	k1gMnSc2	Greta
Thunbergové	Thunbergový	k2eAgFnSc2d1	Thunbergový
<g/>
“	“	k?
===	===	k?
</s>
</p>
<p>
<s>
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
inspirovala	inspirovat	k5eAaBmAgFnS
řadu	řad	k1gInSc2	řad
svých	svůj	k3xOyFgMnPc2
vrstevníků	vrstevník	k1gMnPc2	vrstevník
ve	v	k7c6
školním	školní	k2eAgInSc6d1	školní
věku	věk	k1gInSc6	věk
k	k	k7c3
aktivitám	aktivita	k1gFnPc3	aktivita
na	na	k7c4
ochranu	ochrana	k1gFnSc4	ochrana
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,
často	často	k6eAd1
to	ten	k3xDgNnSc1
bývá	bývat	k5eAaImIp3nS
popisováno	popisován	k2eAgNnSc1d1	popisováno
jako	jako	k8xS,k8xC
„	„	k?
<g/>
efekt	efekt	k1gInSc1	efekt
Grety	Greta	k1gMnSc2	Greta
Thunbergové	Thunbergový	k2eAgFnSc2d1	Thunbergový
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4
její	její	k3xOp3gInSc4
otevřený	otevřený	k2eAgInSc4d1	otevřený
postoj	postoj	k1gInSc4	postoj
vyjádřili	vyjádřit	k5eAaPmAgMnP
různí	různý	k2eAgMnPc1d1	různý
politici	politik	k1gMnPc1	politik
ve	v	k7c6
světě	svět	k1gInSc6	svět
potřebu	potřeba	k1gFnSc4	potřeba
zaměřit	zaměřit	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c4
problematiku	problematika	k1gFnSc4	problematika
změny	změna	k1gFnSc2	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgMnSc1d1	britský
politik	politik	k1gMnSc1	politik
Konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,
člen	člen	k1gMnSc1	člen
vlády	vláda	k1gFnSc2	vláda
Borise	Boris	k1gMnSc2	Boris
Johnsona	Johnson	k1gMnSc2	Johnson
(	(	kIx(
<g/>
a	a	k8xC
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
ve	v	k7c6
vládě	vláda	k1gFnSc6	vláda
Theresy	Theresa	k1gFnSc2	Theresa
Mayové	Mayová	k1gFnSc2	Mayová
<g/>
)	)	kIx)
Michael	Michael	k1gMnSc1	Michael
Gove	Gove	k1gInSc4	Gove
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Když	když	k8xS
jsem	být	k5eAaImIp1nS
vás	vy	k3xPp2nPc4
poslouchal	poslouchat	k5eAaImAgMnS
<g/>
,	,	kIx,
cítil	cítit	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
velký	velký	k2eAgInSc4d1	velký
obdiv	obdiv	k1gInSc4	obdiv
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
a	a	k8xC
vinu	vina	k1gFnSc4	vina
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsem	být	k5eAaImIp1nS
z	z	k7c2
generace	generace	k1gFnSc2	generace
vašich	váš	k3xOp2gMnPc2
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC
uznávám	uznávat	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
jsme	být	k5eAaImIp1nP
neudělali	udělat	k5eNaPmAgMnP
dost	dost	k6eAd1
<g/>
,	,	kIx,
abychom	aby	kYmCp1nP
zabránili	zabránit	k5eAaPmAgMnP
změně	změna	k1gFnSc3	změna
klimatu	klima	k1gNnSc2	klima
a	a	k8xC
širší	široký	k2eAgFnSc6d2	širší
ekologické	ekologický	k2eAgFnSc6d1	ekologická
krizi	krize	k1gFnSc6	krize
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
jsme	být	k5eAaImIp1nP
pomohli	pomoct	k5eAaPmAgMnP
vytvořit	vytvořit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
“	“	k?
Politik	politik	k1gMnSc1	politik
Labouristické	labouristický	k2eAgFnSc2d1	labouristická
strany	strana	k1gFnSc2	strana
a	a	k8xC
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
pro	pro	k7c4
energii	energie	k1gFnSc4	energie
a	a	k8xC
klimatickou	klimatický	k2eAgFnSc4d1	klimatická
změnu	změna	k1gFnSc4	změna
ve	v	k7c6
vládě	vláda	k1gFnSc6	vláda
Gordona	Gordon	k1gMnSc2	Gordon
Browna	Browno	k1gNnSc2	Browno
<g/>
,	,	kIx,
Ed	Ed	k1gMnSc1	Ed
Miliband	Milibanda	k1gFnPc2	Milibanda
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
spoluautorem	spoluautor	k1gMnSc7	spoluautor
britského	britský	k2eAgInSc2d1	britský
zákona	zákon	k1gInSc2	zákon
o	o	k7c6
změně	změna	k1gFnSc6	změna
klimatu	klima	k1gNnSc2	klima
z	z	k7c2
roku	rok	k1gInSc2	rok
2008	#num#	k4
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Probudili	probudit	k5eAaPmAgMnP
jste	být	k5eAaImIp2nP
nás	my	k3xPp1nPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děkujeme	děkovat	k5eAaImIp1nP
vám	vy	k3xPp2nPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
šli	jít	k5eAaImAgMnP
stávkovat	stávkovat	k5eAaImF
<g/>
,	,	kIx,
nastavili	nastavit	k5eAaPmAgMnP
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
naší	náš	k3xOp1gFnSc2
společnosti	společnost	k1gFnSc2	společnost
...	...	k?
dali	dát	k5eAaPmAgMnP
jste	být	k5eAaImIp2nP
nám	my	k3xPp1nPc3
velmi	velmi	k6eAd1
důležitou	důležitý	k2eAgFnSc4d1	důležitá
lekci	lekce	k1gFnSc4	lekce
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystoupili	vystoupit	k5eAaPmAgMnP
jste	být	k5eAaImIp2nP
z	z	k7c2
davu	dav	k1gInSc2	dav
<g/>
.	.	kIx.
<g/>
“	“	k?
Dle	dle	k7c2
průzkumu	průzkum	k1gInSc2	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
agentury	agentura	k1gFnSc2	agentura
YouGov	YouGovo	k1gNnPc2	YouGovo
z	z	k7c2
června	červen	k1gInSc2	červen
2019	#num#	k4
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
obavy	obava	k1gFnPc1	obava
veřejnosti	veřejnost	k1gFnSc2	veřejnost
o	o	k7c4
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
v	v	k7c6
této	tento	k3xDgFnSc6
zemi	zem	k1gFnSc6	zem
stouply	stoupnout	k5eAaPmAgFnP
na	na	k7c4
rekordní	rekordní	k2eAgFnSc4d1	rekordní
úroveň	úroveň	k1gFnSc4	úroveň
od	od	k7c2
té	ten	k3xDgFnSc2
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
a	a	k8xC
hnutí	hnutí	k1gNnSc1	hnutí
Extinction	Extinction	k1gInSc1	Extinction
Rebellion	Rebellion	k?
„	„	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
propíchli	propíchnout	k5eAaPmAgMnP
bublinu	bublina	k1gFnSc4	bublina
popírání	popírání	k1gNnSc2	popírání
změny	změna	k1gFnSc2	změna
klimatu	klima	k1gNnSc2	klima
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
V	v	k7c6
srpnu	srpen	k1gInSc6	srpen
2019	#num#	k4
informovala	informovat	k5eAaBmAgFnS
britská	britský	k2eAgFnSc1d1	britská
vydavatelství	vydavatelství	k1gNnPc1	vydavatelství
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
stoupl	stoupnout	k5eAaPmAgInS
počet	počet	k1gInSc1	počet
vydaných	vydaný	k2eAgFnPc2d1	vydaná
dětských	dětský	k2eAgFnPc2d1	dětská
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
ze	z	k7c2
zabývají	zabývat	k5eAaImIp3nP
klimatickou	klimatický	k2eAgFnSc7d1	klimatická
krizí	krize	k1gFnSc7	krize
<g/>
,	,	kIx,
a	a	k8xC
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
narostl	narůst	k5eAaPmAgInS
také	také	k9
prodej	prodej	k1gInSc1	prodej
těchto	tento	k3xDgFnPc2
knih	kniha	k1gFnPc2	kniha
–	–	k?
díky	díky	k7c3
zvýšenému	zvýšený	k2eAgInSc3d1	zvýšený
zájmu	zájem	k1gInSc3	zájem
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
o	o	k7c4
záchranu	záchrana	k1gFnSc4	záchrana
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
vydavatelé	vydavatel	k1gMnPc1	vydavatel
to	ten	k3xDgNnSc4
nazvali	nazvat	k5eAaPmAgMnP,k5eAaBmAgMnP
„	„	k?
<g/>
Efekt	efekt	k1gInSc1	efekt
Grety	Greta	k1gMnSc2	Greta
Thunbergové	Thunbergový	k2eAgFnSc2d1	Thunbergový
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
Díky	dík	k1gInPc1	dík
inspirací	inspirace	k1gFnPc2	inspirace
Thunbergovou	Thunbergův	k2eAgFnSc7d1	Thunbergův
dali	dát	k5eAaPmAgMnP
bohatí	bohatý	k2eAgMnPc1d1	bohatý
filantropové	filantrop	k1gMnPc1	filantrop
a	a	k8xC
investoři	investor	k1gMnPc1	investor
ze	z	k7c2
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
téměř	téměř	k6eAd1
půl	půl	k1xP	půl
miliónu	milión	k4xCgInSc2
liber	libra	k1gFnPc2	libra
do	do	k7c2
Klimatického	klimatický	k2eAgInSc2d1	klimatický
záchranného	záchranný	k2eAgInSc2d1	záchranný
fondu	fond	k1gInSc2	fond
na	na	k7c4
podporu	podpora	k1gFnSc4	podpora
hnutí	hnutí	k1gNnSc2	hnutí
Extinction	Extinction	k1gInSc1	Extinction
Rebellion	Rebellion	k?
a	a	k8xC
na	na	k7c4
podporu	podpora	k1gFnSc4	podpora
školních	školní	k2eAgFnPc2d1	školní
stávek	stávka	k1gFnPc2	stávka
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgMnSc1
z	z	k7c2
filantropů	filantrop	k1gMnPc2	filantrop
<g/>
,	,	kIx,
Trevor	Trevor	k1gMnSc1	Trevor
Neilson	Neilson	k1gMnSc1	Neilson
<g/>
,	,	kIx,
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
tři	tři	k4xCgMnPc1
zakladatelé	zakladatel	k1gMnPc1	zakladatel
fondu	fond	k1gInSc2	fond
budou	být	k5eAaImBp3nP
kontaktovat	kontaktovat	k5eAaImF
své	svůj	k3xOyFgMnPc4
další	další	k2eAgMnPc4d1	další
přátele	přítel	k1gMnPc4	přítel
mezi	mezi	k7c7
nejbohatšími	bohatý	k2eAgMnPc7d3	nejbohatší
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
v	v	k7c6
následujících	následující	k2eAgInPc6d1	následující
týdnech	týden	k1gInPc6	týden
a	a	k8xC
měsících	měsíc	k1gInPc6	měsíc
darovali	darovat	k5eAaPmAgMnP
do	do	k7c2
fondu	fond	k1gInSc2	fond
„	„	k?
<g/>
stokrát	stokrát	k6eAd1
více	hodně	k6eAd2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
V	v	k7c6
únoru	únor	k1gInSc6	únor
2019	#num#	k4
představil	představit	k5eAaPmAgMnS
šéf	šéf	k1gMnSc1	šéf
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Juncker	Juncker	k1gInSc4	Juncker
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4
úsilí	úsilí	k1gNnSc4	úsilí
Thunbergové	Thunbergový	k2eAgFnSc2d1	Thunbergový
<g/>
,	,	kIx,
návrh	návrh	k1gInSc1	návrh
na	na	k7c4
investování	investování	k1gNnSc4	investování
stovek	stovka	k1gFnPc2	stovka
miliard	miliarda	k4xCgFnPc2
eur	euro	k1gNnPc2	euro
na	na	k7c4
zmírnění	zmírnění	k1gNnSc4	zmírnění
změny	změna	k1gFnSc2	změna
klimatu	klima	k1gNnSc2	klima
od	od	k7c2
roku	rok	k1gInSc2	rok
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klimatické	klimatický	k2eAgFnPc1d1	klimatická
otázky	otázka	k1gFnPc1	otázka
sehrály	sehrát	k5eAaPmAgFnP
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
ve	v	k7c6
volbách	volba	k1gFnPc6	volba
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6
květnu	květen	k1gInSc6	květen
2019	#num#	k4
<g/>
,	,	kIx,
zelené	zelený	k2eAgFnPc1d1	zelená
strany	strana	k1gFnPc1	strana
v	v	k7c6
nich	on	k3xPp3gInPc6
téměř	téměř	k6eAd1
zdvojnásobily	zdvojnásobit	k5eAaPmAgInP
počet	počet	k1gInSc4	počet
získaných	získaný	k2eAgInPc2d1	získaný
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC
získaly	získat	k5eAaPmAgFnP
celkem	celkem	k6eAd1
74	#num#	k4
křesel	křeslo	k1gNnPc2	křeslo
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
nových	nový	k2eAgInPc2d1	nový
hlasů	hlas	k1gInPc2	hlas
získaly	získat	k5eAaPmAgFnP
zelené	zelený	k2eAgFnPc1d1	zelená
strany	strana	k1gFnPc1	strana
v	v	k7c6
zemích	zem	k1gFnPc6	zem
severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,
kde	kde	k6eAd1
probíhaly	probíhat	k5eAaImAgFnP
nejsilnější	silný	k2eAgFnPc1d3	nejsilnější
studentské	studentský	k2eAgFnPc1d1	studentská
stávky	stávka	k1gFnPc1	stávka
inspirované	inspirovaný	k2eAgFnPc1d1	inspirovaná
Thunbergovou	Thunbergův	k2eAgFnSc7d1	Thunbergův
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledky	výsledek	k1gInPc1	výsledek
voleb	volba	k1gFnPc2	volba
daly	dát	k5eAaPmAgInP
šanci	šance	k1gFnSc4	šance
zeleným	zelený	k2eAgFnPc3d1	zelená
stranám	strana	k1gFnPc3	strana
stát	stát	k5eAaImF,k5eAaPmF
se	se	k3xPyFc4
vlivnou	vlivný	k2eAgFnSc7d1	vlivná
silou	síla	k1gFnSc7	síla
v	v	k7c6
Evropském	evropský	k2eAgInSc6d1	evropský
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.
<g/>
V	v	k7c6
červnu	červen	k1gInSc6	červen
2019	#num#	k4
ohlásily	ohlásit	k5eAaPmAgFnP
Švédské	švédský	k2eAgFnPc1d1	švédská
železnice	železnice	k1gFnPc1	železnice
<g/>
,	,	kIx,
že	že	k8xS
počet	počet	k1gInSc1	počet
cestujících	cestující	k1gMnPc2	cestující
vlaky	vlak	k1gInPc1	vlak
na	na	k7c6
domácích	domácí	k2eAgFnPc6d1	domácí
tratích	trať	k1gFnPc6	trať
narostl	narůst	k5eAaPmAgInS
meziročně	meziročně	k6eAd1
o	o	k7c4
8	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
a	a	k8xC
přičítají	přičítat	k5eAaImIp3nP
to	ten	k3xDgNnSc1
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP
obavy	obava	k1gFnPc4	obava
z	z	k7c2
emisí	emise	k1gFnPc2	emise
CO2	CO2	k1gMnPc2	CO2
při	při	k7c6
létání	létání	k1gNnSc6	létání
na	na	k7c6
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
odmítá	odmítat	k5eAaImIp3nS
při	při	k7c6
cestování	cestování	k1gNnSc6	cestování
používat	používat	k5eAaImF
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jsou	být	k5eAaImIp3nP
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6
rozpacích	rozpak	k1gInPc6	rozpak
nebo	nebo	k8xC
se	se	k3xPyFc4
stydí	stydět	k5eAaImIp3nP
létat	létat	k5eAaImF
letadly	letadlo	k1gNnPc7	letadlo
kvůli	kvůli	k7c3
jejich	jejich	k3xOp3gInSc3
dopadu	dopad	k1gInSc3	dopad
na	na	k7c4
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,
začaly	začít	k5eAaPmAgInP
být	být	k5eAaImF
používány	používán	k2eAgInPc1d1	používán
výrazy	výraz	k1gInPc1	výraz
„	„	k?
<g/>
Flygskam	Flygskam	k1gInSc1	Flygskam
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
Hanba	hanba	k6eAd1
z	z	k7c2
létání	létání	k1gNnSc2	létání
<g/>
“	“	k?
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
hashtagy	hashtag	k1gInPc7	hashtag
#	#	kIx~
<g/>
jagstannarpå	jagstannarpå	k1gInSc1	jagstannarpå
či	či	k8xC
#	#	kIx~
<g/>
istayontheground	istayontheground	k1gInSc1	istayontheground
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7
aktivitami	aktivita	k1gFnPc7	aktivita
Thunbergové	Thunbergový	k2eAgFnSc6d1	Thunbergový
je	být	k5eAaImIp3nS
dáván	dáván	k2eAgInSc1d1	dáván
také	také	k9
prudký	prudký	k2eAgInSc1d1	prudký
nárůst	nárůst	k1gInSc1	nárůst
zaplacených	zaplacený	k2eAgFnPc2d1	zaplacená
uhlíkových	uhlíkový	k2eAgFnPc2d1	uhlíková
kompenzací	kompenzace	k1gFnPc2	kompenzace
v	v	k7c6
letecké	letecký	k2eAgFnSc6d1	letecká
dopravě	doprava	k1gFnSc6	doprava
v	v	k7c6
roce	rok	k1gInSc6	rok
2019	#num#	k4
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Reakce	reakce	k1gFnSc1	reakce
OPEC	opéct	k5eAaPmRp2nSwK
===	===	k?
</s>
</p>
<p>
<s>
V	v	k7c6
červnu	červen	k1gInSc6	červen
2019	#num#	k4
informovala	informovat	k5eAaBmAgFnS
agentura	agentura	k1gFnSc1	agentura
France-Presse	France-Presse	k1gFnSc1	France-Presse
<g/>
,	,	kIx,
že	že	k8xS
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
Organizace	organizace	k1gFnSc2	organizace
zemí	zem	k1gFnPc2	zem
vyvážejících	vyvážející	k2eAgMnPc2d1	vyvážející
ropu	ropa	k1gFnSc4	ropa
(	(	kIx(
<g/>
OPEC	opéct	k5eAaPmRp2nSwK
<g/>
)	)	kIx)
Mohammed	Mohammed	k1gMnSc1	Mohammed
Barkindo	Barkindo	k6eAd1
si	se	k3xPyFc3
stěžoval	stěžovat	k5eAaImAgMnS
na	na	k7c4
„	„	k?
<g/>
nevědecké	vědecký	k2eNgInPc4d1	nevědecký
<g/>
“	“	k?
útoky	útok	k1gInPc4	útok
klimatických	klimatický	k2eAgMnPc2d1	klimatický
aktivistů	aktivista	k1gMnPc2	aktivista
na	na	k7c4
ropný	ropný	k2eAgInSc4d1	ropný
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC
nazval	nazvat	k5eAaPmAgMnS,k5eAaBmAgMnS
aktivisty	aktivista	k1gMnPc4	aktivista
„	„	k?
<g/>
pravděpodobně	pravděpodobně	k6eAd1
největším	veliký	k2eAgNnSc7d3	veliký
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
<g/>
,	,	kIx,
kterému	který	k3yQgInSc3,k3yRgInSc3,k3yIgInSc3
musí	muset	k5eAaImIp3nS
OPEC	opéct	k5eAaPmRp2nSwK
čelit	čelit	k5eAaImF
do	do	k7c2
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
“	“	k?
a	a	k8xC
jako	jako	k9
příčinu	příčina	k1gFnSc4	příčina
přímo	přímo	k6eAd1
uváděl	uvádět	k5eAaImAgInS
„	„	k?
<g/>
současnou	současný	k2eAgFnSc4d1	současná
vlnu	vlna	k1gFnSc4	vlna
školních	školní	k2eAgFnPc2d1	školní
stávek	stávka	k1gFnPc2	stávka
inspirované	inspirovaný	k2eAgInPc1d1	inspirovaný
švédskou	švédský	k2eAgFnSc7d1	švédská
školačkou	školačka	k1gFnSc7	školačka
Gretou	Gretý	k2eAgFnSc7d1	Gretý
Thunbergovou	Thunbergův	k2eAgFnSc7d1	Thunbergův
a	a	k8xC
hnutím	hnutí	k1gNnSc7	hnutí
Fridays	Fridaysa	k1gFnPc2	Fridaysa
for	forum	k1gNnPc2	forum
Future	Futur	k1gMnSc5	Futur
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
a	a	k8xC
další	další	k2eAgMnPc1d1	další
klimatičtí	klimatický	k2eAgMnPc1d1	klimatický
aktivisté	aktivista	k1gMnPc1	aktivista
toto	tento	k3xDgNnSc4
vyjádření	vyjádření	k1gNnSc4	vyjádření
označili	označit	k5eAaPmAgMnP
jako	jako	k8xS,k8xC
ocenění	ocenění	k1gNnSc4	ocenění
jejich	jejich	k3xOp3gFnSc2
dobré	dobrý	k2eAgFnSc2d1	dobrá
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Kontroverze	kontroverze	k1gFnSc2	kontroverze
==	==	k?
</s>
</p>
<p>
<s>
===	===	k?
Kritika	kritika	k1gFnSc1	kritika
===	===	k?
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
zvedla	zvednout	k5eAaPmAgFnS
vlna	vlna	k1gFnSc1	vlna
studentských	studentský	k2eAgFnPc2d1	studentská
klimatických	klimatický	k2eAgFnPc2d1	klimatická
stávek	stávka	k1gFnPc2	stávka
<g/>
,	,	kIx,
začali	začít	k5eAaPmAgMnP
se	se	k3xPyFc4
popírači	popírač	k1gMnPc1	popírač
klimatické	klimatický	k2eAgFnSc2d1	klimatická
změny	změna	k1gFnSc2	změna
snažit	snažit	k5eAaImF
ji	on	k3xPp3gFnSc4
zdiskreditovat	zdiskreditovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
redaktorky	redaktorka	k1gFnSc2	redaktorka
Aditya	Adityus	k1gMnSc2	Adityus
Chakrabortty	Chakrabortta	k1gMnSc2	Chakrabortta
z	z	k7c2
deníku	deník	k1gInSc2	deník
The	The	k1gMnSc1	The
Guardian	Guardian	k1gMnSc1	Guardian
„	„	k?
<g/>
spustili	spustit	k5eAaPmAgMnP
odpůrci	odpůrce	k1gMnPc7	odpůrce
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
nechutné	chutný	k2eNgNnSc1d1	nechutné
osobní	osobní	k2eAgInPc1d1	osobní
útoky	útok	k1gInPc1	útok
<g/>
“	“	k?
na	na	k7c6
Gretu	Gret	k1gInSc6	Gret
Thunbergovou	Thunbergový	k2eAgFnSc4d1	Thunbergový
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	nejvíce	k6eAd1,k6eAd3
útoků	útok	k1gInPc2	útok
je	být	k5eAaImIp3nS
na	na	k7c6
Gretu	Gret	k1gInSc6	Gret
vedeno	vést	k5eAaImNgNnS
ze	z	k7c2
strany	strana	k1gFnSc2	strana
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
popírají	popírat	k5eAaImIp3nP
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
často	často	k6eAd1
zmiňují	zmiňovat	k5eAaImIp3nP
nebo	nebo	k8xC
přímo	přímo	k6eAd1
zneužívají	zneužívat	k5eAaImIp3nP
její	její	k3xOp3gInSc4
autismus	autismus	k1gInSc4	autismus
<g/>
.	.	kIx.
<g/>
Švédská	švédský	k2eAgFnSc1d1	švédská
novinářka	novinářka	k1gFnSc1	novinářka
Paulina	Paulin	k2eAgFnSc1d1	Paulina
Neuding	Neuding	k1gInSc4	Neuding
napsala	napsat	k5eAaPmAgFnS,k5eAaBmAgFnS
pro	pro	k7c4
časopis	časopis	k1gInSc4	časopis
Quillette	Quillett	k1gInSc5	Quillett
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
uvažovala	uvažovat	k5eAaImAgFnS
<g/>
,	,	kIx,
zda	zda	k8xS
sláva	sláva	k1gFnSc1	sláva
a	a	k8xC
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
obdržela	obdržet	k5eAaPmAgFnS
<g/>
,	,	kIx,
na	na	k7c4
ni	on	k3xPp3gFnSc4
nevytváří	vytvářit	k5eNaPmIp3nS,k5eNaImIp3nS
nepřípustný	přípustný	k2eNgInSc1d1	nepřípustný
tlak	tlak	k1gInSc1	tlak
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
článku	článek	k1gInSc6	článek
dále	daleko	k6eAd2
rozvíjela	rozvíjet	k5eAaImAgFnS
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,
zda	zda	k8xS
by	by	kYmCp3nS
to	ten	k3xDgNnSc1
nemohl	moct	k5eNaImAgMnS
být	být	k5eAaImF
problém	problém	k1gInSc4	problém
s	s	k7c7
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4
její	její	k3xOp3gInPc4
mnohočetné	mnohočetný	k2eAgInPc4d1	mnohočetný
duševní	duševní	k2eAgInPc4d1	duševní
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,
a	a	k8xC
psala	psát	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
školní	školní	k2eAgFnSc1d1	školní
stávka	stávka	k1gFnSc1	stávka
<g/>
…	…	k?
představuje	představovat	k5eAaImIp3nS
formu	forma	k1gFnSc4	forma
sebepoškozování	sebepoškozování	k1gNnSc2	sebepoškozování
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
chce	chtít	k5eAaImIp3nS
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
přilákat	přilákat	k5eAaPmF
pozornost	pozornost	k1gFnSc4	pozornost
dospělých	dospělý	k2eAgMnPc2d1	dospělý
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1	další
redaktorka	redaktorka	k1gFnSc1	redaktorka
Quillette	Quillett	k1gInSc5	Quillett
Helen	Helena	k1gFnPc2	Helena
Deal	Dealum	k1gNnPc2	Dealum
napsala	napsat	k5eAaBmAgFnS,k5eAaPmAgFnS
tweet	tweet	k5eAaPmF,k5eAaBmF,k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
měla	mít	k5eAaImAgFnS
postoupit	postoupit	k5eAaPmF
interview	interview	k1gNnSc4	interview
s	s	k7c7
Andrewem	Andrew	k1gMnSc7	Andrew
Neilem	Neil	k1gInSc7	Neil
z	z	k7c2
BBC	BBC	kA
<g/>
,	,	kIx,
popíračem	popírač	k1gMnSc7	popírač
klimatické	klimatický	k2eAgFnSc2d1	klimatická
změny	změna	k1gFnSc2	změna
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračovala	pokračovat	k5eAaImAgFnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Mohli	moct	k5eAaImAgMnP
bychom	by	kYmCp1nP
být	být	k5eAaImF
svědky	svědek	k1gMnPc4	svědek
zhroucení	zhroucení	k1gNnSc1	zhroucení
Grety	Greta	k1gFnSc2	Greta
v	v	k7c6
národní	národní	k2eAgFnSc6d1	národní
televizi	televize	k1gFnSc6	televize
a	a	k8xC
už	už	k6eAd1
bychom	by	kYmCp1nP
o	o	k7c6
ní	on	k3xPp3gFnSc6
nikdy	nikdy	k6eAd1
neslyšeli	slyšet	k5eNaImAgMnP
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
byla	být	k5eAaImAgFnS
Dealová	Dealová	k1gFnSc1	Dealová
za	za	k7c4
tento	tento	k3xDgInSc4
tweet	tweet	k1gInSc4	tweet
kritizována	kritizovat	k5eAaImNgFnS
<g/>
,	,	kIx,
prohlásila	prohlásit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
je	být	k5eAaImIp3nS
snad	snad	k9
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
jen	jen	k9
vtip	vtip	k1gInSc1	vtip
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
extrémně	extrémně	k6eAd1
polarizovanou	polarizovaný	k2eAgFnSc4d1	polarizovaná
reakci	reakce	k1gFnSc4	reakce
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
na	na	k7c4
tweet	tweet	k1gInSc4	tweet
dostala	dostat	k5eAaPmAgFnS
<g/>
,	,	kIx,
nepřestala	přestat	k5eNaPmAgFnS
Dealová	Dealová	k1gFnSc1	Dealová
kritizovat	kritizovat	k5eAaImF
Thunbergovou	Thunbergový	k2eAgFnSc4d1	Thunbergový
jako	jako	k8xS,k8xC
obhájkyni	obhájkyně	k1gFnSc4	obhájkyně
povědomí	povědomí	k1gNnSc2	povědomí
o	o	k7c6
změně	změna	k1gFnSc6	změna
klimatu	klima	k1gNnSc2	klima
a	a	k8xC
argumentovala	argumentovat	k5eAaImAgFnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
děti	dítě	k1gFnPc1	dítě
„	„	k?
<g/>
nás	my	k3xPp1nPc2
nemohou	moct	k5eNaImIp3nP
vést	vést	k5eAaImF
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
OSN	OSN	kA
António	António	k1gMnSc1	António
Guterres	Guterres	k1gMnSc1	Guterres
řekl	říct	k5eAaPmAgMnS
na	na	k7c6
konferenci	konference	k1gFnSc6	konference
na	na	k7c6
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
v	v	k7c6
květnu	květen	k1gInSc6	květen
2019	#num#	k4
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc1
generace	generace	k1gFnSc1	generace
„	„	k?
<g/>
nevyhrává	vyhrávat	k5eNaImIp3nS
bitvu	bitva	k1gFnSc4	bitva
proti	proti	k7c3
klimatickým	klimatický	k2eAgFnPc3d1	klimatická
změnám	změna	k1gFnPc3	změna
<g/>
“	“	k?
a	a	k8xC
že	že	k8xS
je	být	k5eAaImIp3nS
na	na	k7c6
mladých	mladý	k1gMnPc6	mladý
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
„	„	k?
<g/>
zachránili	zachránit	k5eAaPmAgMnP
tuto	tento	k3xDgFnSc4
planetu	planeta	k1gFnSc4	planeta
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
Také	také	k9
Piers	Piersa	k1gFnPc2	Piersa
Corbyn	Corbyn	k1gMnSc1	Corbyn
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
známý	známý	k1gMnSc1	známý
popíráním	popírání	k1gNnSc7	popírání
klimatické	klimatický	k2eAgFnSc2d1	klimatická
změny	změna	k1gFnSc2	změna
(	(	kIx(
<g/>
bratr	bratr	k1gMnSc1	bratr
Jeremy	Jerema	k1gFnSc2	Jerema
Corbyna	Corbyna	k1gFnSc1	Corbyna
<g/>
)	)	kIx)
<g/>
,	,	kIx,
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
zmanipulovaná	zmanipulovaný	k2eAgFnSc1d1	zmanipulovaná
dospělými	dospělý	k2eAgFnPc7d1	dospělá
a	a	k8xC
„	„	k?
<g/>
nemá	mít	k5eNaImIp3nS
pravdu	pravda	k1gFnSc4	pravda
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
V	v	k7c6
období	období	k1gNnSc6	období
před	před	k7c7
volbami	volba	k1gFnPc7	volba
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6
květnu	květen	k1gInSc6	květen
2019	#num#	k4
zvýšili	zvýšit	k5eAaPmAgMnP
pravicoví	pravicový	k2eAgMnPc1d1	pravicový
populisté	populista	k1gMnPc1	populista
v	v	k7c6
Německu	Německo	k1gNnSc6	Německo
své	svůj	k3xOyFgInPc4
útoky	útok	k1gInPc4	útok
na	na	k7c4
vědecká	vědecký	k2eAgNnPc4d1	vědecké
pojednání	pojednání	k1gNnPc4	pojednání
o	o	k7c6
klimatu	klima	k1gNnSc6	klima
a	a	k8xC
začali	začít	k5eAaPmAgMnP
se	se	k3xPyFc4
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
zprávách	zpráva	k1gFnPc6	zpráva
zaměřovat	zaměřovat	k5eAaImF
na	na	k7c6
Gretu	Gret	k1gInSc6	Gret
Thunbergovou	Thunbergový	k2eAgFnSc4d1	Thunbergový
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1	strana
Alternativa	alternativa	k1gFnSc1	alternativa
pro	pro	k7c4
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
popírání	popírání	k1gNnSc3	popírání
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
programu	program	k1gInSc6	program
<g/>
,	,	kIx,
začala	začít	k5eAaPmAgFnS
v	v	k7c6
rámci	rámec	k1gInSc6	rámec
předvolební	předvolební	k2eAgFnSc2d1	předvolební
kampaně	kampaň	k1gFnSc2	kampaň
s	s	k7c7
osobními	osobní	k2eAgInPc7d1	osobní
útoky	útok	k1gInPc7	útok
na	na	k7c4
Thunbergovou	Thunbergový	k2eAgFnSc4d1	Thunbergový
<g/>
,	,	kIx,
vysmívali	vysmívat	k5eAaImAgMnP
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
jako	jako	k9
„	„	k?
<g/>
duševně	duševně	k6eAd1
postižené	postižený	k2eAgFnSc2d1	postižená
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
podvodnici	podvodnice	k1gFnSc4	podvodnice
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakob	Jakob	k1gMnSc1	Jakob
Guhl	Guhl	k1gMnSc1	Guhl
<g/>
,	,	kIx,
vědecký	vědecký	k2eAgMnSc1d1	vědecký
pracovník	pracovník	k1gMnSc1	pracovník
Institutu	institut	k1gInSc2	institut
pro	pro	k7c4
strategický	strategický	k2eAgInSc4d1	strategický
dialog	dialog	k1gInSc4	dialog
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
popírání	popírání	k1gNnSc1	popírání
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7	součást
politické	politický	k2eAgFnSc2d1	politická
taktiky	taktika	k1gFnSc2	taktika
strany	strana	k1gFnSc2	strana
a	a	k8xC
že	že	k8xS
„	„	k?
<g/>
útočit	útočit	k5eAaImF
na	na	k7c6
Gretu	Gret	k1gInSc6	Gret
<g/>
,	,	kIx,
občas	občas	k6eAd1
opravdu	opravdu	k6eAd1
zlovolnými	zlovolný	k2eAgInPc7d1	zlovolný
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,
včetně	včetně	k7c2
zesměšňování	zesměšňování	k1gNnSc2	zesměšňování
jejího	její	k3xOp3gInSc2
autismu	autismus	k1gInSc2	autismus
<g/>
,	,	kIx,
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
jak	jak	k6eAd1
prezentovat	prezentovat	k5eAaBmF
politické	politický	k2eAgMnPc4d1	politický
oponenty	oponent	k1gMnPc4	oponent
AfD	AfD	k1gMnSc4	AfD
jako	jako	k8xS,k8xC
iracionální	iracionální	k2eAgMnPc4d1	iracionální
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
Její	její	k3xOp3gFnSc4
kritici	kritik	k1gMnPc1	kritik
namítají	namítat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
sice	sice	k8xC
bojuje	bojovat	k5eAaImIp3nS
proti	proti	k7c3
změnám	změna	k1gFnPc3	změna
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,
sama	sám	k3xTgFnSc1
však	však	k9
při	při	k7c6
svých	svůj	k3xOyFgFnPc6
cestách	cesta	k1gFnPc6	cesta
spotřebovává	spotřebovávat	k5eAaImIp3nS
zbytečně	zbytečně	k6eAd1
plasty	plast	k1gInPc1	plast
<g/>
;	;	kIx,
tato	tento	k3xDgFnSc1
kritika	kritika	k1gFnSc1	kritika
byla	být	k5eAaImAgFnS
následně	následně	k6eAd1
zpochybněna	zpochybnit	k5eAaPmNgFnS
některými	některý	k3yIgFnPc7
komentátory	komentátor	k1gMnPc4	komentátor
<g/>
.	.	kIx.
<g/>
Novinář	novinář	k1gMnSc1	novinář
Christopher	Christophra	k1gFnPc2	Christophra
Caldwell	Caldwell	k1gMnSc1	Caldwell
tvrdí	tvrdit	k5eAaImIp3nS
v	v	k7c6
článku	článek	k1gInSc6	článek
pro	pro	k7c4
The	The	k1gFnSc4	The
New	New	k1gMnPc2	New
York	York	k1gInSc4	York
Times	Timesa	k1gFnPc2	Timesa
,	,	kIx,
že	že	k8xS
zjednodušený	zjednodušený	k2eAgInSc4d1	zjednodušený
<g/>
,	,	kIx,
přímočarý	přímočarý	k2eAgInSc4d1	přímočarý
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3
změně	změna	k1gFnSc3	změna
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
používá	používat	k5eAaImIp3nS
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
<g/>
,	,	kIx,
způsobí	způsobit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
protestující	protestující	k2eAgInPc4d1	protestující
proti	proti	k7c3
změně	změna	k1gFnSc3	změna
klimatu	klima	k1gNnSc2	klima
se	se	k3xPyFc4
dostanou	dostat	k5eAaPmIp3nP
do	do	k7c2
konfliktu	konflikt	k1gInSc2	konflikt
se	s	k7c7
složitostí	složitost	k1gFnSc7	složitost
rozhodování	rozhodování	k1gNnSc2	rozhodování
v	v	k7c6
západních	západní	k2eAgFnPc6d1	západní
demokraciích	demokracie	k1gFnPc6	demokracie
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
filozof	filozof	k1gMnSc1	filozof
Raphaël	Raphaël	k1gMnSc1	Raphaël
Enthoven	Enthoven	k2eAgMnSc1d1	Enthoven
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
mnoho	mnoho	k4c1
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3
podporou	podpora	k1gFnSc7	podpora
Grety	Greta	k1gMnSc2	Greta
„	„	k?
<g/>
nakupuje	nakupovat	k5eAaBmIp3nS
ctnost	ctnost	k1gFnSc1	ctnost
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6	skutečnost
neudělají	udělat	k5eNaPmIp3nP
nic	nic	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
pomohli	pomoct	k5eAaPmAgMnP
v	v	k7c6
boji	boj	k1gInSc6	boj
proti	proti	k7c3
změně	změna	k1gFnSc3	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Podpora	podpora	k1gFnSc1	podpora
===	===	k?
</s>
</p>
<p>
<s>
Komentátoři	komentátor	k1gMnPc1	komentátor
ostatních	ostatní	k2eAgNnPc2d1	ostatní
médií	médium	k1gNnPc2	médium
většinou	většinou	k6eAd1
Gretu	Gret	k2eAgFnSc4d1	Gret
Thunbergovou	Thunbergový	k2eAgFnSc4d1	Thunbergový
podporují	podporovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redaktor	redaktor	k1gMnSc1	redaktor
londýnského	londýnský	k2eAgInSc2d1	londýnský
deníku	deník	k1gInSc2	deník
The	The	k1gMnSc1	The
Guardian	Guardian	k1gMnSc1	Guardian
Charlie	Charlie	k1gMnSc1	Charlie
Hancock	Hancock	k1gMnSc1	Hancock
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Neurotické	neurotický	k2eAgInPc4d1	neurotický
útoky	útok	k1gInPc4	útok
na	na	k7c6
Gretu	Gret	k1gInSc6	Gret
jsou	být	k5eAaImIp3nP
ostré	ostrý	k2eAgInPc1d1	ostrý
a	a	k8xC
zlovolné	zlovolný	k2eAgInPc1d1	zlovolný
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9
znamenají	znamenat	k5eAaImIp3nP
uznání	uznání	k1gNnSc4	uznání
jejích	její	k3xOp3gInPc2
argumentů	argument	k1gInPc2	argument
<g/>
.	.	kIx.
<g/>
“	“	k?
Redaktor	redaktor	k1gMnSc1	redaktor
časopisu	časopis	k1gInSc2	časopis
Vox	Vox	k1gMnSc1	Vox
Steve	Steve	k1gMnSc1	Steve
Silberman	Silberman	k1gMnSc1	Silberman
také	také	k9
Gretu	Gret	k1gInSc3	Gret
obhajoval	obhajovat	k5eAaImAgMnS
a	a	k8xC
poukázal	poukázat	k5eAaPmAgMnS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
její	její	k3xOp3gNnSc4
autistické	autistický	k2eAgNnSc4d1	autistické
spektrum	spektrum	k1gNnSc4	spektrum
jí	on	k3xPp3gFnSc3
dává	dávat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4	možnost
neochvějně	neochvějně	k6eAd1
trvat	trvat	k5eAaImF
na	na	k7c6
svých	svůj	k3xOyFgInPc6
názorech	názor	k1gInPc6	názor
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporu	podpor	k1gInSc2	podpor
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
článku	článek	k1gInSc6	článek
vyjádřil	vyjádřit	k5eAaPmAgMnS
Gretě	Greť	k1gFnPc4	Greť
rovněž	rovněž	k9
Chris	Chris	k1gInSc1	Chris
Packham	Packham	k1gInSc1	Packham
<g/>
,	,	kIx,
moderátor	moderátor	k1gMnSc1	moderátor
televizního	televizní	k2eAgInSc2d1	televizní
pořadu	pořad	k1gInSc2	pořad
BBC	BBC	kA
Springwatch	Springwatch	k1gMnSc1	Springwatch
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
má	mít	k5eAaImIp3nS
také	také	k9
Aspergerův	Aspergerův	k2eAgInSc1d1	Aspergerův
syndrom	syndrom	k1gInSc1	syndrom
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Lidé	člověk	k1gMnPc1	člověk
jako	jako	k9
já	já	k3xPp1nSc1
<g/>
,	,	kIx,
s	s	k7c7
Aspergerovým	Aspergerův	k2eAgInSc7d1	Aspergerův
syndromem	syndrom	k1gInSc7	syndrom
<g/>
,	,	kIx,
takové	takový	k3xDgFnPc1
věci	věc	k1gFnPc1	věc
neovlivní	ovlivnit	k5eNaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nesníží	snížit	k5eNaPmIp3nS
to	ten	k3xDgNnSc4
naše	náš	k3xOp1gNnSc4
odhodlání	odhodlání	k1gNnSc4	odhodlání
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viděli	vidět	k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
to	ten	k3xDgNnSc1
tento	tento	k3xDgInSc4
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
proběhly	proběhnout	k5eAaPmAgFnP
trollingové	trollingový	k2eAgInPc4d1	trollingový
útoky	útok	k1gInPc4	útok
na	na	k7c4
Gretu	Greta	k1gFnSc4	Greta
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jen	jen	k9
ztráta	ztráta	k1gFnSc1	ztráta
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.
<g/>
“	“	k?
V	v	k7c6
článku	článek	k1gInSc6	článek
s	s	k7c7
názvem	název	k1gInSc7	název
"	"	kIx"
<g/>
Proč	proč	k6eAd1
se	se	k3xPyFc4
opravdu	opravdu	k6eAd1
bojí	bát	k5eAaImIp3nP
Grety	Greta	k1gFnPc1	Greta
Thunbergové	Thunbergový	k2eAgFnPc1d1	Thunbergový
<g/>
"	"	kIx"
<g/>
,	,	kIx,
internetová	internetový	k2eAgFnSc1d1	internetová
publikace	publikace	k1gFnSc1	publikace
Huffington	Huffington	k1gInSc4	Huffington
Post	post	k1gInSc1	post
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
„	„	k?
<g/>
straší	strašit	k5eAaImIp3nS
lidi	člověk	k1gMnPc4	člověk
středního	střední	k2eAgInSc2d1	střední
věku	věk	k1gInSc2	věk
a	a	k8xC
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
a	a	k8xC
jejich	jejich	k3xOp3gFnSc2
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4
ni	on	k3xPp3gFnSc4
je	být	k5eAaImIp3nS
řízena	řízen	k2eAgFnSc1d1	řízena
<g />
.	.	kIx.
</s>
<s hack="1">
strachem	strach	k1gInSc7	strach
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
díky	díky	k7c3
ní	on	k3xPp3gFnSc3
nevyhnutelně	vyhnutelně	k6eNd1
ztratí	ztratit	k5eAaPmIp3nS
svou	svůj	k3xOyFgFnSc4
pozici	pozice	k1gFnSc4	pozice
(	(	kIx(
<g/>
v	v	k7c6
politickém	politický	k2eAgInSc6d1	politický
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
V	v	k7c6
květnu	květen	k1gInSc6	květen
2019	#num#	k4
vyšel	vyjít	k5eAaPmAgInS
časopis	časopis	k1gInSc1	časopis
Time	Tim	k1gFnSc2	Tim
s	s	k7c7
Gretou	Greta	k1gFnSc7	Greta
Thunbergovou	Thunbergův	k2eAgFnSc7d1	Thunbergův
na	na	k7c6
obálce	obálka	k1gFnSc6	obálka
a	a	k8xC
v	v	k7c6
článku	článek	k1gInSc6	článek
uvnitř	uvnitř	k6eAd1
byla	být	k5eAaImAgFnS
popsána	popsat	k5eAaPmNgFnS
jako	jako	k8xC,k8xS
vzor	vzor	k1gInSc1	vzor
a	a	k8xC
jedna	jeden	k4xCgFnSc1
z	z	k7c2
„	„	k?
<g/>
příští	příští	k2eAgFnSc2d1	příští
generace	generace	k1gFnSc2	generace
vůdců	vůdce	k1gMnPc2	vůdce
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rozhovoru	rozhovor	k1gInSc6	rozhovor
se	s	k7c7
Suyinem	Suyin	k1gMnSc7	Suyin
Haynesem	Haynes	k1gMnSc7	Haynes
se	se	k3xPyFc4
zabývala	zabývat	k5eAaImAgFnS
kritikou	kritika	k1gFnSc7	kritika
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
obdržela	obdržet	k5eAaPmAgFnS
on-line	onin	k1gInSc5	on-lin
<g/>
,	,	kIx,
a	a	k8xC
řekla	říct	k5eAaPmAgFnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
docela	docela	k6eAd1
veselé	veselý	k2eAgNnSc1d1	veselé
<g/>
,	,	kIx,
když	když	k8xS
jediná	jediný	k2eAgFnSc1d1	jediná
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
proti	proti	k7c3
mně	já	k3xPp1nSc6
mohou	moct	k5eAaImIp3nP
lidé	člověk	k1gMnPc1	člověk
udělat	udělat	k5eAaPmF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
posmívat	posmívat	k5eAaImF
se	se	k3xPyFc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
mluvit	mluvit	k5eAaImF
o	o	k7c6
mém	můj	k3xOp1gInSc6
vzhledu	vzhled	k1gInSc6	vzhled
nebo	nebo	k8xC
osobnosti	osobnost	k1gFnSc3	osobnost
<g/>
,	,	kIx,
protože	protože	k8xS
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nemají	mít	k5eNaImIp3nP
žádné	žádný	k3yNgInPc1
reálné	reálný	k2eAgInPc1d1	reálný
argumenty	argument	k1gInPc1	argument
proti	proti	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
říkám	říkat	k5eAaImIp1nS
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
</p>
<p>
<s>
===	===	k?
Zneužití	zneužití	k1gNnSc1	zneužití
jejího	její	k3xOp3gNnSc2
jména	jméno	k1gNnSc2	jméno
===	===	k?
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
těžit	těžit	k5eAaImF
z	z	k7c2
vysoké	vysoký	k2eAgFnSc2d1	vysoká
popularity	popularita	k1gFnSc2	popularita
Grety	Greta	k1gFnSc2	Greta
Thunbergové	Thunbergový	k2eAgFnSc2d1	Thunbergový
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
k	k	k7c3
tomu	ten	k3xDgInSc3
dala	dát	k5eAaPmAgFnS
souhlas	souhlas	k1gInSc4	souhlas
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2	rok
2018	#num#	k4
Ingmar	Ingmar	k1gMnSc1	Ingmar
Rentzhog	Rentzhog	k1gMnSc1	Rentzhog
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1	zakladatel
neziskové	ziskový	k2eNgFnSc2d1	nezisková
nadace	nadace	k1gFnSc2	nadace
We	We	k1gMnSc1	We
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"
<g/>
t	t	k?
Have	Have	k1gInSc1	Have
Time	Time	k1gInSc1	Time
Foundation	Foundation	k1gInSc1	Foundation
(	(	kIx(
<g/>
WDHT	WDHT	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nabídl	nabídnout	k5eAaPmAgMnS
Thunbergové	Thunbergový	k2eAgNnSc4d1	Thunbergový
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
neplaceným	placený	k2eNgMnSc7d1	neplacený
poradcem	poradce	k1gMnSc7	poradce
pro	pro	k7c4
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,
a	a	k8xC
použil	použít	k5eAaPmAgMnS
její	její	k3xOp3gNnSc4
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC
podobiznu	podobizna	k1gFnSc4	podobizna
a	a	k8xC
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
měla	mít	k5eAaImAgFnS
velký	velký	k2eAgInSc4d1	velký
finanční	finanční	k2eAgInSc4d1	finanční
prospěch	prospěch	k1gInSc4	prospěch
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
We	We	k1gMnSc1	We
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"
<g/>
t	t	k?
Have	Have	k1gNnSc2	Have
Time	Time	k1gFnPc2	Time
AB	AB	kA
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
výkonným	výkonný	k2eAgMnSc7d1	výkonný
ředitelem	ředitel	k1gMnSc7	ředitel
byl	být	k5eAaImAgMnS
Rentzhog	Rentzhog	k1gMnSc1	Rentzhog
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
za	za	k7c4
to	ten	k3xDgNnSc4
nedostala	dostat	k5eNaPmAgFnS
žádné	žádný	k3yNgInPc4
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,
ve	v	k7c6
společností	společnost	k1gFnPc2	společnost
WDHT	WDHT	kA
ukončila	ukončit	k5eAaPmAgFnS
své	své	k1gNnSc4	své
angažmá	angažmá	k1gNnSc7	angažmá
dobrovolného	dobrovolný	k2eAgMnSc2d1	dobrovolný
poradce	poradce	k1gMnSc2	poradce
a	a	k8xC
uvedla	uvést	k5eAaPmAgFnS
<g/>
:	:	kIx,
„	„	k?
<g/>
nejsem	být	k5eNaImIp1nS
součástí	součást	k1gFnSc7	součást
žádné	žádný	k3yNgFnSc2
organizace	organizace	k1gFnSc2	organizace
<g/>
…	…	k?
jsem	být	k5eAaImIp1nS
naprosto	naprosto	k6eAd1
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
<g/>
…	…	k?
[	[	kIx(
<g/>
a	a	k8xC
<g/>
]	]	kIx)
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
dělám	dělat	k5eAaImIp1nS
<g/>
,	,	kIx,
dělám	dělat	k5eAaImIp1nS
úplně	úplně	k6eAd1
zdarma	zdarma	k6eAd1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
</p>
<p>
<s>
===	===	k?
Spor	spor	k1gInSc1	spor
o	o	k7c4
jadernou	jaderný	k2eAgFnSc4d1	jaderná
energetiku	energetika	k1gFnSc4	energetika
===	===	k?
</s>
</p>
<p>
<s>
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
se	se	k3xPyFc4
v	v	k7c6
březnu	březen	k1gInSc6	březen
2019	#num#	k4
vyjádřila	vyjádřit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
sama	sám	k3xTgFnSc1
nemá	mít	k5eNaImIp3nS
ráda	rád	k2eAgFnSc1d1	ráda
jadernou	jaderný	k2eAgFnSc7d1	jaderná
energetiku	energetika	k1gFnSc4	energetika
<g/>
,	,	kIx,
ale	ale	k8xC
že	že	k8xS
podle	podle	k7c2
IPCC	IPCC	kA
je	být	k5eAaImIp3nS
i	i	k9
jaderná	jaderný	k2eAgFnSc1d1	jaderná
energetika	energetika	k1gFnSc1	energetika
jedním	jeden	k4xCgMnSc7
z	z	k7c2
řešení	řešení	k1gNnSc2	řešení
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
použít	použít	k5eAaPmF
ke	k	k7c3
snížení	snížení	k1gNnSc3	snížení
emisí	emise	k1gFnPc2	emise
CO	co	k8xS
<g/>
2	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
by	by	kYmCp3nP
neměli	mít	k5eNaImAgMnP
ochránci	ochránce	k1gMnPc1	ochránce
klimatu	klima	k1gNnSc2	klima
vystupovat	vystupovat	k5eAaImF
proti	proti	k7c3
ní	on	k3xPp3gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
němečtí	německý	k2eAgMnPc1d1	německý
aktivisté	aktivista	k1gMnPc1	aktivista
toto	tento	k3xDgNnSc4
prohlášení	prohlášení	k1gNnSc4	prohlášení
odsoudili	odsoudit	k5eAaPmAgMnP
a	a	k8xC
vyjádřili	vyjádřit	k5eAaPmAgMnP
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7	součást
jaderné	jaderný	k2eAgFnSc2d1	jaderná
lobby	lobby	k1gFnSc2	lobby
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Spor	spor	k1gInSc1	spor
s	s	k7c7
novináři	novinář	k1gMnPc7	novinář
během	během	k7c2
studentského	studentský	k2eAgInSc2d1	studentský
summitu	summit	k1gInSc2	summit
v	v	k7c6
Lausanne	Lausanne	k1gNnSc6	Lausanne
===	===	k?
</s>
</p>
<p>
<s>
Na	na	k7c6
studentském	studentský	k2eAgInSc6d1	studentský
summitu	summit	k1gInSc6	summit
„	„	k?
<g/>
Úsměv	úsměv	k1gInSc1	úsměv
pro	pro	k7c4
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
“	“	k?
na	na	k7c4
podporu	podpora	k1gFnSc4	podpora
klimatu	klima	k1gNnSc2	klima
ve	v	k7c6
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
Lausanne	Lausanne	k1gNnSc6	Lausanne
došlo	dojít	k5eAaPmAgNnS
při	při	k7c6
jednání	jednání	k1gNnSc6	jednání
dne	den	k1gInSc2	den
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2	srpen
2019	#num#	k4
k	k	k7c3
závažným	závažný	k2eAgMnPc3d1	závažný
vnitřním	vnitřní	k2eAgMnPc3d1	vnitřní
sporům	spor	k1gInPc3	spor
studentského	studentský	k2eAgNnSc2d1	studentské
klimatického	klimatický	k2eAgNnSc2d1	klimatické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
souvisely	souviset	k5eAaImAgInP
především	především	k6eAd1
s	s	k7c7
nehierarchickou	hierarchický	k2eNgFnSc7d1	nehierarchická
koncepcí	koncepce	k1gFnSc7	koncepce
řízení	řízení	k1gNnSc2	řízení
tohoto	tento	k3xDgNnSc2
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
momentě	moment	k1gInSc6	moment
navrhla	navrhnout	k5eAaPmAgFnS
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
účastníci	účastník	k1gMnPc1	účastník
požádali	požádat	k5eAaPmAgMnP
novináře	novinář	k1gMnPc4	novinář
o	o	k7c4
opuštění	opuštění	k1gNnSc4	opuštění
sálu	sál	k1gInSc2	sál
<g/>
;	;	kIx,
nelíbilo	líbit	k5eNaImAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
<g/>
,	,	kIx,
jakým	jaký	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
způsobem	způsob	k1gInSc7	způsob
referují	referovat	k5eAaBmIp3nP
o	o	k7c6
těchto	tento	k3xDgInPc6
vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
sporech	spor	k1gInPc6	spor
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
účastníci	účastník	k1gMnPc1	účastník
odhlasovali	odhlasovat	k5eAaPmAgMnP
vyloučení	vyloučení	k1gNnSc4	vyloučení
novinářů	novinář	k1gMnPc2	novinář
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
ovšem	ovšem	k9
své	svůj	k3xOyFgNnSc4
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
změnili	změnit	k5eAaPmAgMnP
<g/>
,	,	kIx,
a	a	k8xC
novináři	novinář	k1gMnPc1	novinář
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
vrátit	vrátit	k5eAaPmF
zpět	zpět	k6eAd1
do	do	k7c2
sálu	sál	k1gInSc2	sál
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Plavba	plavba	k1gFnSc1	plavba
do	do	k7c2
USA	USA	kA
===	===	k?
</s>
</p>
<p>
<s>
Ve	v	k7c6
světových	světový	k2eAgInPc6d1	světový
i	i	k8xC
českých	český	k2eAgNnPc6d1	české
médiích	médium	k1gNnPc6	médium
proběhla	proběhnout	k5eAaPmAgFnS
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
a	a	k8xC
do	do	k7c2
jaké	jaký	k3yRgFnSc2,k3yQgFnSc2,k3yIgFnSc2
míry	míra	k1gFnSc2	míra
byla	být	k5eAaImAgFnS
plavba	plavba	k1gFnSc1	plavba
Grety	Greta	k1gFnSc2	Greta
Thunbergové	Thunberg	k1gMnPc1	Thunberg
do	do	k7c2
USA	USA	kA
opravdu	opravdu	k6eAd1
bezuhlíková	bezuhlíkový	k2eAgFnSc1d1	bezuhlíkový
<g/>
,	,	kIx,
respektive	respektive	k9
kolik	kolik	k4yQc4,k4yRc4,k4yIc4
tun	tuna	k1gFnPc2	tuna
emisí	emise	k1gFnPc2	emise
CO2	CO2	k1gMnPc2	CO2
nemuselo	muset	k5eNaImAgNnS
vzniknout	vzniknout	k5eAaPmF
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nS
Greta	Greta	k1gFnSc1	Greta
a	a	k8xC
její	její	k3xOp3gMnSc1
otec	otec	k1gMnSc1	otec
použili	použít	k5eAaPmAgMnP
pro	pro	k7c4
cestu	cesta	k1gFnSc4	cesta
normální	normální	k2eAgFnSc4d1	normální
letecké	letecký	k2eAgNnSc4d1	letecké
spojení	spojení	k1gNnSc4	spojení
nebo	nebo	k8xC
nákladní	nákladní	k2eAgFnSc4d1	nákladní
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
plavba	plavba	k1gFnSc1	plavba
byla	být	k5eAaImAgFnS
uspořádána	uspořádat	k5eAaPmNgFnS
krátkodobě	krátkodobě	k6eAd1
<g/>
,	,	kIx,
museli	muset	k5eAaImAgMnP
členové	člen	k1gMnPc1	člen
posádky	posádka	k1gFnSc2	posádka
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
přepravu	přeprava	k1gFnSc4	přeprava
do	do	k7c2
USA	USA	kA
resp.	resp.	kA
do	do	k7c2
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2
časových	časový	k2eAgInPc2d1	časový
důvodů	důvod	k1gInPc2	důvod
použít	použít	k5eAaPmF
letecká	letecký	k2eAgNnPc4d1	letecké
spojení	spojení	k1gNnPc4	spojení
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
proto	proto	k8xC
odletěli	odletět	k5eAaPmAgMnP
dva	dva	k4xCgMnPc1
organizátoři	organizátor	k1gMnPc1	organizátor
návratu	návrat	k1gInSc2	návrat
plachetnice	plachetnice	k1gFnSc2	plachetnice
do	do	k7c2
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC
po	po	k7c6
nich	on	k3xPp3gFnPc6
dva	dva	k4xCgMnPc1
další	další	k2eAgMnPc1d1	další
jachtaři	jachtař	k1gMnPc1	jachtař
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
zúčastnili	zúčastnit	k5eAaPmAgMnP
této	tento	k3xDgFnSc2
zpětné	zpětný	k2eAgFnSc2d1	zpětná
plavby	plavba	k1gFnSc2	plavba
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
majitel	majitel	k1gMnSc1	majitel
lodi	loď	k1gFnSc2	loď
Malizia	Malizia	k1gFnSc1	Malizia
II	II	kA
a	a	k8xC
zároveň	zároveň	k6eAd1
její	její	k3xOp3gFnSc4
tzv.	tzv.	kA
skipper	skipper	k1gMnSc1	skipper
Boris	Boris	k1gMnSc1	Boris
Herrmann	Herrmann	k1gMnSc1	Herrmann
(	(	kIx(
<g/>
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
vlastní	vlastnit	k5eAaImIp3nS
licenci	licence	k1gFnSc4	licence
pro	pro	k7c4
plavbu	plavba	k1gFnSc4	plavba
jachtou	jachta	k1gFnSc7	jachta
<g/>
)	)	kIx)
spolu	spolu	k6eAd1
s	s	k7c7
Pierrem	Pierro	k1gNnSc7	Pierro
Casiraghim	Casiraghim	k1gInSc1	Casiraghim
se	se	k3xPyFc4
po	po	k7c4
připlutí	připlutí	k1gNnSc4	připlutí
do	do	k7c2
USA	USA	kA
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
Monaka	Monako	k1gNnSc2	Monako
rovněž	rovněž	k9
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
v	v	k7c6
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7
plavbou	plavba	k1gFnSc7	plavba
proběhlo	proběhnout	k5eAaPmAgNnS
šest	šest	k4xCc4
letů	let	k1gInPc2	let
dopravními	dopravní	k2eAgNnPc7d1	dopravní
letadly	letadlo	k1gNnPc7	letadlo
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
manažerky	manažerka	k1gFnSc2	manažerka
Týmu	tým	k1gInSc2	tým
Malizia	Malizius	k1gMnSc2	Malizius
II	II	kA
všichni	všechen	k3xTgMnPc1
jeho	jeho	k3xOp3gMnPc1
členové	člen	k1gMnPc1	člen
zaplatili	zaplatit	k5eAaPmAgMnP
uhlíkovou	uhlíkový	k2eAgFnSc4d1	uhlíková
kompenzaci	kompenzace	k1gFnSc4	kompenzace
<g/>
,	,	kIx,
takže	takže	k8xS
celá	celý	k2eAgFnSc1d1	celá
cesta	cesta	k1gFnSc1	cesta
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
pro	pro	k7c4
Gretu	Gret	k2eAgFnSc4d1	Gret
Thunbergovou	Thunbergový	k2eAgFnSc4d1	Thunbergový
byla	být	k5eAaImAgFnS
údajně	údajně	k6eAd1
„	„	k?
<g/>
CO	co	k8xS
<g/>
2	#num#	k4
neutrální	neutrální	k2eAgFnSc4d1	neutrální
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
českých	český	k2eAgMnPc2d1	český
autorů	autor	k1gMnPc2	autor
se	se	k3xPyFc4
domnívalo	domnívat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c7
diskusí	diskuse	k1gFnSc7	diskuse
o	o	k7c6
uhlíkové	uhlíkový	k2eAgFnSc6d1	uhlíková
stopě	stopa	k1gFnSc6	stopa
plavby	plavba	k1gFnSc2	plavba
do	do	k7c2
USA	USA	kA
jsou	být	k5eAaImIp3nP
snahy	snaha	k1gFnPc1	snaha
odvrátit	odvrátit	k5eAaPmF
pozornost	pozornost	k1gFnSc4	pozornost
od	od	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
požaduje	požadovat	k5eAaImIp3nS
–	–	k?
tedy	tedy	k9
radikální	radikální	k2eAgNnSc4d1	radikální
opatření	opatření	k1gNnSc4	opatření
k	k	k7c3
ochraně	ochrana	k1gFnSc3	ochrana
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
českými	český	k2eAgMnPc7d1	český
jachtaři	jachtař	k1gMnPc7	jachtař
naopak	naopak	k6eAd1
proběhla	proběhnout	k5eAaPmAgFnS
diskuse	diskuse	k1gFnPc4	diskuse
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
daleké	daleký	k2eAgFnPc4d1	daleká
cesty	cesta	k1gFnPc4	cesta
plachetnicemi	plachetnice	k1gFnPc7	plachetnice
škodlivé	škodlivý	k2eAgFnPc4d1	škodlivá
pro	pro	k7c4
přírodu	příroda	k1gFnSc4	příroda
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?
</s>
</p>
<p>
<s>
==	==	k?
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?
</s>
</p>
<p>
<s>
===	===	k?
Reference	reference	k1gFnPc1	reference
===	===	k?
</s>
</p>
<p>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2
článků	článek	k1gInPc2	článek
Greta	Greta	k1gMnSc1	Greta
Thunberg	Thunberg	k1gMnSc1	Thunberg
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
,	,	kIx,
Greta	Greta	k1gMnSc1	Greta
Thunberg	Thunberg	k1gMnSc1	Thunberg
na	na	k7c6
švédské	švédský	k2eAgFnSc6d1	švédská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
,	,	kIx,
Greta	Greta	k1gMnSc1	Greta
Thunberg	Thunberg	k1gMnSc1	Thunberg
na	na	k7c6
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC
Malizia	Malizia	k1gFnSc1	Malizia
II	II	kA
na	na	k7c6
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Související	související	k2eAgFnPc4d1	související
stránky	stránka	k1gFnPc4	stránka
===	===	k?
</s>
</p>
<p>
<s>
Extinction	Extinction	k1gInSc1	Extinction
Rebellion	Rebellion	k?
</s>
</p>
<p>
<s>
Konference	konference	k1gFnSc1	konference
OSN	OSN	kA
o	o	k7c6
změně	změna	k1gFnSc6	změna
klimatu	klima	k1gNnSc2	klima
2018	#num#	k4
Katovice	Katovice	k1gFnPc5	Katovice
</s>
</p>
<p>
<s>
No	no	k9
One	One	k1gMnSc1	One
Is	Is	k1gMnSc1	Is
Too	Too	k1gMnSc1	Too
Small	Small	k1gMnSc1	Small
to	ten	k3xDgNnSc4
Make	Make	k1gNnSc4	Make
a	a	k8xC
Difference	Difference	k1gFnSc1	Difference
(	(	kIx(
<g/>
sbírka	sbírka	k1gFnSc1	sbírka
projevů	projev	k1gInPc2	projev
Grety	Gret	k1gInPc4	Gret
Thunbergové	Thunbergový	k2eAgInPc4d1	Thunbergový
<g/>
)	)	kIx)
</s>
</p>
<p>
<s>
===	===	k?
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC
videa	video	k1gNnSc2	video
k	k	k7c3
tématu	téma	k1gNnSc3	téma
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
ve	v	k7c4
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
na	na	k7c6
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
přednáší	přednášet	k5eAaImIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6	rámec
TeDx	TeDx	k1gInSc1	TeDx
Stockholm	Stockholm	k1gInSc1	Stockholm
</s>
</p>
<p>
<s>
Projev	projev	k1gInSc1	projev
Grety	Greta	k1gFnSc2	Greta
Thunbergové	Thunbergový	k2eAgFnSc2d1	Thunbergový
na	na	k7c6
konferenci	konference	k1gFnSc6	konference
OSN	OSN	kA
v	v	k7c6
Katovicích	Katovice	k1gFnPc6	Katovice
</s>
</p>
<p>
<s>
„	„	k?
<g/>
Nechováte	chovat	k5eNaImIp2nP
se	se	k3xPyFc4
jako	jako	k9
dospělí	dospělí	k1gMnPc1	dospělí
<g/>
.	.	kIx.
<g/>
“	“	k?
Studentka	studentka	k1gFnSc1	studentka
zahanbila	zahanbit	k5eAaPmAgFnS
lídry	lídr	k1gMnPc4	lídr
na	na	k7c6
klimatické	klimatický	k2eAgFnSc6d1	klimatická
konferenci	konference	k1gFnSc6	konference
–	–	k?
článek	článek	k1gInSc1	článek
a	a	k8xC
video	video	k1gNnSc1	video
na	na	k7c6
Aktualne	Aktualn	k1gInSc5	Aktualn
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
</p>
<p>
<s>
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
a	a	k8xC
její	její	k3xOp3gInSc1
projev	projev	k1gInSc1	projev
v	v	k7c6
Katowicích	Katowice	k1gFnPc6	Katowice
česky	česky	k6eAd1
-	-	kIx~
článek	článek	k1gInSc1	článek
na	na	k7c4
Ecoblog	Ecoblog	k1gInSc4	Ecoblog
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
</p>
