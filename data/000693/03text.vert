<s>
Město	město	k1gNnSc1	město
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Adlerkosteletz	Adlerkosteletz	k1gInSc1	Adlerkosteletz
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
7	[number]	k4	7
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Rychnova	Rychnov	k1gInSc2	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
a	a	k8xC	a
29	[number]	k4	29
km	km	kA	km
vjv	vjv	k?	vjv
od	od	k7c2	od
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
6	[number]	k4	6
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1316	[number]	k4	1316
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
"	"	kIx"	"
<g/>
tvrze	tvrz	k1gFnSc2	tvrz
kostelní	kostelní	k2eAgMnSc1d1	kostelní
<g/>
"	"	kIx"	"
zabit	zabit	k2eAgMnSc1d1	zabit
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Vartemberka	Vartemberka	k1gFnSc1	Vartemberka
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
dobývání	dobývání	k1gNnSc6	dobývání
románského	románský	k2eAgMnSc2d1	románský
předchůdce	předchůdce	k1gMnSc2	předchůdce
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
osada	osada	k1gFnSc1	osada
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
cihelny	cihelna	k1gFnSc2	cihelna
<g/>
,	,	kIx,	,
městečko	městečko	k1gNnSc1	městečko
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
plošiny	plošina	k1gFnSc2	plošina
nad	nad	k7c7	nad
údolím	údolí	k1gNnSc7	údolí
Divoké	divoký	k2eAgFnSc2d1	divoká
Orlice	Orlice	k1gFnSc2	Orlice
<g/>
.	.	kIx.	.
</s>
<s>
Kostelec	Kostelec	k1gInSc1	Kostelec
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
tržním	tržní	k2eAgInSc6d1	tržní
střediskem	středisko	k1gNnSc7	středisko
panství	panství	k1gNnSc4	panství
Potštejn	Potštejn	k1gInSc1	Potštejn
<g/>
,	,	kIx,	,
samostatné	samostatný	k2eAgNnSc1d1	samostatné
panství	panství	k1gNnSc1	panství
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
až	až	k9	až
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
na	na	k7c6	na
radnici	radnice	k1gFnSc6	radnice
kostelečtí	kostelecký	k2eAgMnPc1d1	kostelecký
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1585	[number]	k4	1585
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
tehdy	tehdy	k6eAd1	tehdy
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
sídlo	sídlo	k1gNnSc1	sídlo
majitelů	majitel	k1gMnPc2	majitel
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Starý	starý	k2eAgInSc4d1	starý
zámek	zámek	k1gInSc4	zámek
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
hospodářským	hospodářský	k2eAgInSc7d1	hospodářský
dvorem	dvůr	k1gInSc7	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
šlechtické	šlechtický	k2eAgNnSc1d1	šlechtické
vdovské	vdovský	k2eAgNnSc1d1	vdovské
sídlo	sídlo	k1gNnSc1	sídlo
tzv.	tzv.	kA	tzv.
Dvoreček	dvoreček	k1gInSc1	dvoreček
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1610	[number]	k4	1610
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1628	[number]	k4	1628
se	se	k3xPyFc4	se
kostelečtí	kostelecký	k2eAgMnPc1d1	kostelecký
postavili	postavit	k5eAaPmAgMnP	postavit
proti	proti	k7c3	proti
povstalým	povstalý	k2eAgInPc3d1	povstalý
sedlákům	sedlák	k1gInPc3	sedlák
<g/>
,	,	kIx,	,
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
zde	zde	k6eAd1	zde
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
rekatolizace	rekatolizace	k1gFnSc1	rekatolizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
soudního	soudní	k2eAgInSc2d1	soudní
okresu	okres	k1gInSc2	okres
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc4	sídlo
politického	politický	k2eAgInSc2d1	politický
okresu	okres	k1gInSc2	okres
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tohoto	tento	k3xDgInSc2	tento
kroku	krok	k1gInSc2	krok
stal	stát	k5eAaPmAgInS	stát
centrem	centr	k1gInSc7	centr
Podorlicka	Podorlicko	k1gNnSc2	Podorlicko
<g/>
.	.	kIx.	.
</s>
<s>
Pomalý	pomalý	k2eAgInSc1d1	pomalý
rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
akceleroval	akcelerovat	k5eAaImAgInS	akcelerovat
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Seykorova	Seykorův	k2eAgFnSc1d1	Seykorův
koželužna	koželužna	k1gFnSc1	koželužna
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k9	zejména
po	po	k7c6	po
zprovoznění	zprovoznění	k1gNnSc6	zprovoznění
železnice	železnice	k1gFnSc2	železnice
z	z	k7c2	z
Velkého	velký	k2eAgInSc2d1	velký
Oseka	Osek	k1gInSc2	Osek
do	do	k7c2	do
Lichkova	Lichkův	k2eAgInSc2d1	Lichkův
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
cukrovar	cukrovar	k1gInSc4	cukrovar
Kinských	Kinská	k1gFnPc2	Kinská
<g/>
,	,	kIx,	,
Kratochvílova	Kratochvílův	k2eAgFnSc1d1	Kratochvílova
strojírna	strojírna	k1gFnSc1	strojírna
<g/>
,	,	kIx,	,
Moravcova	Moravcův	k2eAgFnSc1d1	Moravcova
textilka	textilka	k1gFnSc1	textilka
a	a	k8xC	a
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
několik	několik	k4yIc1	několik
obuvnických	obuvnický	k2eAgFnPc2d1	obuvnická
továren	továrna	k1gFnPc2	továrna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
však	však	k9	však
většinou	většinou	k6eAd1	většinou
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
Baťova	Baťův	k2eAgInSc2d1	Baťův
koncernu	koncern	k1gInSc2	koncern
za	za	k7c4	za
velké	velká	k1gFnPc4	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
staré	starý	k2eAgFnSc2d1	stará
městské	městský	k2eAgFnSc2d1	městská
cihelny	cihelna	k1gFnSc2	cihelna
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
1884	[number]	k4	1884
kruhovka	kruhovka	k1gFnSc1	kruhovka
A.	A.	kA	A.
Moravce	Moravec	k1gMnSc4	Moravec
a	a	k8xC	a
J.	J.	kA	J.
Pišty	Pišt	k1gInPc1	Pišt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
od	od	k7c2	od
r.	r.	kA	r.
1898	[number]	k4	1898
konkurovala	konkurovat	k5eAaImAgFnS	konkurovat
kruhová	kruhový	k2eAgFnSc1d1	kruhová
cihelna	cihelna	k1gFnSc1	cihelna
velkostatku	velkostatek	k1gInSc2	velkostatek
Kinských	Kinské	k1gNnPc2	Kinské
za	za	k7c7	za
Dvorečkem	dvoreček	k1gInSc7	dvoreček
<g/>
.	.	kIx.	.
</s>
<s>
Velkostatek	velkostatek	k1gInSc4	velkostatek
pak	pak	k6eAd1	pak
koupil	koupit	k5eAaPmAgMnS	koupit
druhou	druhý	k4xOgFnSc4	druhý
kruhovku	kruhovka	k1gFnSc4	kruhovka
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
svoji	svůj	k3xOyFgFnSc4	svůj
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
stadion	stadion	k1gInSc1	stadion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1944-1947	[number]	k4	1944-1947
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
u	u	k7c2	u
Orlice	Orlice	k1gFnSc2	Orlice
Rojkova	Rojkův	k2eAgFnSc1d1	Rojkův
strojírna	strojírna	k1gFnSc1	strojírna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
Seykorova	Seykorův	k2eAgFnSc1d1	Seykorův
koželužna	koželužna	k1gFnSc1	koželužna
změněna	změnit	k5eAaPmNgFnS	změnit
ve	v	k7c4	v
výrobnu	výrobna	k1gFnSc4	výrobna
brzdových	brzdový	k2eAgNnPc2d1	brzdové
obložení	obložení	k1gNnPc2	obložení
Osinek	osinek	k1gInSc1	osinek
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Ferodo	Feroda	k1gMnSc5	Feroda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zaniklého	zaniklý	k2eAgInSc2d1	zaniklý
cukrovaru	cukrovar	k1gInSc2	cukrovar
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
komplex	komplex	k1gInSc1	komplex
budov	budova	k1gFnPc2	budova
Zemědělského	zemědělský	k2eAgNnSc2d1	zemědělské
zásobování	zásobování	k1gNnSc2	zásobování
a	a	k8xC	a
nákupu	nákup	k1gInSc2	nákup
se	s	k7c7	s
silem	silo	k1gNnSc7	silo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
první	první	k4xOgFnSc2	první
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
města	město	k1gNnSc2	město
vystavěna	vystavěn	k2eAgNnPc4d1	vystavěno
dělostřelecká	dělostřelecký	k2eAgNnPc4d1	dělostřelecké
kasárna	kasárna	k1gNnPc4	kasárna
díky	díky	k7c3	díky
činnosti	činnost	k1gFnSc3	činnost
starosty	starosta	k1gMnSc2	starosta
Josefa	Josef	k1gMnSc2	Josef
Krejčíka	Krejčík	k1gMnSc2	Krejčík
<g/>
,	,	kIx,	,
přezdívaného	přezdívaný	k2eAgMnSc2d1	přezdívaný
kostelecký	kostelecký	k2eAgMnSc1d1	kostelecký
Ulrych	Ulrych	k1gMnSc1	Ulrych
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Krejčíkovi	Krejčík	k1gMnSc3	Krejčík
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ve	v	k7c6	v
městě	město	k1gNnSc6	město
několik	několik	k4yIc1	několik
architektonicky	architektonicky	k6eAd1	architektonicky
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
staveb	stavba	k1gFnPc2	stavba
(	(	kIx(	(
<g/>
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
O.	O.	kA	O.
Liska	Liska	k1gMnSc1	Liska
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Spielmann	Spielmann	k1gInSc1	Spielmann
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
památný	památný	k2eAgInSc4d1	památný
den	den	k1gInSc4	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1926	[number]	k4	1926
v	v	k7c6	v
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
hodinách	hodina	k1gFnPc6	hodina
navštívil	navštívit	k5eAaPmAgInS	navštívit
tehdy	tehdy	k6eAd1	tehdy
okresní	okresní	k2eAgNnSc4d1	okresní
město	město	k1gNnSc4	město
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
delegací	delegace	k1gFnSc7	delegace
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgMnS	být
i	i	k8xC	i
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
přijel	přijet	k5eAaPmAgMnS	přijet
automobily	automobil	k1gInPc4	automobil
z	z	k7c2	z
Rychnova	Rychnov	k1gInSc2	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
odjel	odjet	k5eAaPmAgInS	odjet
připraveným	připravený	k2eAgInSc7d1	připravený
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
vlakem	vlak	k1gInSc7	vlak
z	z	k7c2	z
Kostelce	Kostelec	k1gInSc2	Kostelec
v	v	k7c4	v
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
18	[number]	k4	18
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
reálnou	reálný	k2eAgFnSc7d1	reálná
školou	škola	k1gFnSc7	škola
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
r.	r.	kA	r.
1935	[number]	k4	1935
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
socha	socha	k1gFnSc1	socha
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
zničená	zničený	k2eAgFnSc1d1	zničená
v	v	k7c6	v
r.	r.	kA	r.
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
presidentské	presidentský	k2eAgFnPc1d1	presidentská
návštěvy	návštěva	k1gFnPc1	návštěva
se	se	k3xPyFc4	se
Kostelec	Kostelec	k1gInSc4	Kostelec
dočkal	dočkat	k5eAaPmAgInS	dočkat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Kostelci	Kostelec	k1gInSc6	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
přestavěný	přestavěný	k2eAgInSc1d1	přestavěný
roku	rok	k1gInSc2	rok
1688	[number]	k4	1688
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
z	z	k7c2	z
tvrze	tvrz	k1gFnSc2	tvrz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
1777	[number]	k4	1777
přestavěn	přestavět	k5eAaPmNgMnS	přestavět
F.	F.	kA	F.
Kermerem	Kermero	k1gNnSc7	Kermero
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
trojkřídlé	trojkřídlý	k2eAgFnSc2d1	trojkřídlá
barokní	barokní	k2eAgFnSc2d1	barokní
stavby	stavba	k1gFnSc2	stavba
zbyla	zbýt	k5eAaPmAgFnS	zbýt
dvě	dva	k4xCgNnPc4	dva
dvoupatrová	dvoupatrový	k2eAgNnPc4d1	dvoupatrové
křídla	křídlo	k1gNnPc4	křídlo
s	s	k7c7	s
mansardovou	mansardový	k2eAgFnSc7d1	mansardová
střechou	střecha	k1gFnSc7	střecha
a	a	k8xC	a
ve	v	k7c6	v
dvoře	dvůr	k1gInSc6	dvůr
zazděnými	zazděný	k2eAgFnPc7d1	zazděná
arkádami	arkáda	k1gFnPc7	arkáda
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
zámkem	zámek	k1gInSc7	zámek
a	a	k8xC	a
východně	východně	k6eAd1	východně
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
byl	být	k5eAaImAgInS	být
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgInSc1d1	barokní
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
dvůr	dvůr	k1gInSc1	dvůr
se	s	k7c7	s
špýcharem	špýchar	k1gInSc7	špýchar
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
zbořený	zbořený	k2eAgMnSc1d1	zbořený
kvůli	kvůli	k7c3	kvůli
stavbě	stavba	k1gFnSc3	stavba
silničního	silniční	k2eAgInSc2d1	silniční
průtahu	průtah	k1gInSc2	průtah
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
empírová	empírový	k2eAgFnSc1d1	empírová
dvoupatrová	dvoupatrový	k2eAgFnSc1d1	dvoupatrová
vila	vila	k1gFnSc1	vila
se	s	k7c7	s
sloupovým	sloupový	k2eAgInSc7d1	sloupový
portikem	portikus	k1gInSc7	portikus
v	v	k7c6	v
krajinářském	krajinářský	k2eAgInSc6d1	krajinářský
parku	park	k1gInSc6	park
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1829-35	[number]	k4	1829-35
Heinrichem	Heinrich	k1gMnSc7	Heinrich
Kochem	Koch	k1gMnSc7	Koch
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zámeckou	zámecký	k2eAgFnSc4d1	zámecká
kapli	kaple	k1gFnSc4	kaple
byly	být	k5eAaImAgInP	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
obraz	obraz	k1gInSc1	obraz
P.	P.	kA	P.
Marie	Maria	k1gFnSc2	Maria
od	od	k7c2	od
Františka	František	k1gMnSc2	František
Tkadlíka	Tkadlík	k1gMnSc2	Tkadlík
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
a	a	k8xC	a
soška	soška	k1gFnSc1	soška
madony	madona	k1gFnSc2	madona
od	od	k7c2	od
Emanuela	Emanuel	k1gMnSc2	Emanuel
Maxe	Max	k1gMnSc2	Max
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
<g/>
.	.	kIx.	.
</s>
<s>
Přilehlý	přilehlý	k2eAgInSc1d1	přilehlý
krajinářský	krajinářský	k2eAgInSc1d1	krajinářský
park	park	k1gInSc1	park
z	z	k7c2	z
téže	týž	k3xTgFnSc2	týž
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
známý	známý	k2eAgMnSc1d1	známý
zejména	zejména	k9	zejména
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
výsadbami	výsadba	k1gFnPc7	výsadba
rododendronů	rododendron	k1gInPc2	rododendron
<g/>
,	,	kIx,	,
bledulí	bledule	k1gFnSc7	bledule
a	a	k8xC	a
skalkou	skalka	k1gFnSc7	skalka
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
druhů	druh	k1gInPc2	druh
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
od	od	k7c2	od
r.	r.	kA	r.
1928	[number]	k4	1928
stále	stále	k6eAd1	stále
využívaný	využívaný	k2eAgInSc4d1	využívaný
tenisový	tenisový	k2eAgInSc4d1	tenisový
kurt	kurt	k1gInSc4	kurt
<g/>
,	,	kIx,	,
zřízený	zřízený	k2eAgInSc4d1	zřízený
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
otevřené	otevřený	k2eAgFnSc2d1	otevřená
jízdárny	jízdárna	k1gFnSc2	jízdárna
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
přestavbami	přestavba	k1gFnPc7	přestavba
měšťanského	měšťanský	k2eAgInSc2d1	měšťanský
domu	dům	k1gInSc2	dům
(	(	kIx(	(
<g/>
nadpraží	nadpraží	k1gNnSc2	nadpraží
portálu	portál	k1gInSc2	portál
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1574	[number]	k4	1574
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pozdně	pozdně	k6eAd1	pozdně
barokně	barokně	k6eAd1	barokně
upravená	upravený	k2eAgFnSc1d1	upravená
F.	F.	kA	F.
Kermerem	Kermer	k1gMnSc7	Kermer
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
v	v	k7c6	v
r.	r.	kA	r.
1777	[number]	k4	1777
a	a	k8xC	a
doplněná	doplněný	k2eAgFnSc1d1	doplněná
o	o	k7c4	o
věž	věž	k1gFnSc4	věž
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
dvakrát	dvakrát	k6eAd1	dvakrát
přestavována	přestavovat	k5eAaImNgFnS	přestavovat
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
věžemi	věž	k1gFnPc7	věž
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
průčelí	průčelí	k1gNnSc6	průčelí
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1769-1773	[number]	k4	1769-1773
od	od	k7c2	od
hradeckého	hradecký	k2eAgMnSc2d1	hradecký
stavitele	stavitel	k1gMnSc2	stavitel
F.	F.	kA	F.
Kermera	Kermer	k1gMnSc2	Kermer
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
staršího	starý	k2eAgInSc2d2	starší
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Pozdně	pozdně	k6eAd1	pozdně
barokní	barokní	k2eAgFnSc1d1	barokní
stavba	stavba	k1gFnSc1	stavba
na	na	k7c6	na
půdorysu	půdorys	k1gInSc6	půdorys
kříže	kříž	k1gInSc2	kříž
s	s	k7c7	s
valenými	valený	k2eAgFnPc7d1	valená
klenbami	klenba	k1gFnPc7	klenba
a	a	k8xC	a
výsečemi	výseč	k1gFnPc7	výseč
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgNnSc1d1	barokní
zařízení	zařízení	k1gNnSc1	zařízení
silně	silně	k6eAd1	silně
upraveno	upravit	k5eAaPmNgNnS	upravit
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
oltáři	oltář	k1gInSc6	oltář
pozdně	pozdně	k6eAd1	pozdně
gotická	gotický	k2eAgFnSc1d1	gotická
soška	soška	k1gFnSc1	soška
Madony	Madona	k1gFnSc2	Madona
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1500	[number]	k4	1500
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cínová	cínový	k2eAgFnSc1d1	cínová
křtitelnice	křtitelnice	k1gFnSc1	křtitelnice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1540	[number]	k4	1540
<g/>
.	.	kIx.	.
</s>
<s>
Fara	fara	k1gFnSc1	fara
<g/>
,	,	kIx,	,
patrová	patrový	k2eAgFnSc1d1	patrová
obdélná	obdélný	k2eAgFnSc1d1	obdélná
budova	budova	k1gFnSc1	budova
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
gotickými	gotický	k2eAgInPc7d1	gotický
sklepy	sklep	k1gInPc7	sklep
<g/>
,	,	kIx,	,
patro	patro	k1gNnSc4	patro
z	z	k7c2	z
r.	r.	kA	r.
1831	[number]	k4	1831
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
<g/>
,	,	kIx,	,
hřbitovní	hřbitovní	k2eAgFnSc1d1	hřbitovní
<g/>
,	,	kIx,	,
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgFnSc1d1	barokní
obdélná	obdélný	k2eAgFnSc1d1	obdélná
stavba	stavba	k1gFnSc1	stavba
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
věžemi	věž	k1gFnPc7	věž
v	v	k7c4	v
průčelí	průčelí	k1gNnSc4	průčelí
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1686	[number]	k4	1686
<g/>
-	-	kIx~	-
<g/>
1691	[number]	k4	1691
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
zařízení	zařízení	k1gNnSc1	zařízení
z	z	k7c2	z
téže	týž	k3xTgFnSc2	týž
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
Jana	Jan	k1gMnSc2	Jan
Amose	Amos	k1gMnSc2	Amos
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
archanděla	archanděl	k1gMnSc2	archanděl
Michaela	Michael	k1gMnSc2	Michael
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgFnSc1d1	barokní
stavba	stavba	k1gFnSc1	stavba
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
renesančního	renesanční	k2eAgInSc2d1	renesanční
bratrského	bratrský	k2eAgInSc2d1	bratrský
sboru	sbor	k1gInSc2	sbor
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1580	[number]	k4	1580
<g/>
-	-	kIx~	-
<g/>
1586	[number]	k4	1586
<g/>
.	.	kIx.	.
</s>
<s>
Prostá	prostý	k2eAgFnSc1d1	prostá
obdélná	obdélný	k2eAgFnSc1d1	obdélná
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
půlkruhovým	půlkruhový	k2eAgInSc7d1	půlkruhový
závěrem	závěr	k1gInSc7	závěr
<g/>
,	,	kIx,	,
opravená	opravený	k2eAgFnSc1d1	opravená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Sloup	sloup	k1gInSc1	sloup
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1716	[number]	k4	1716
<g/>
.	.	kIx.	.
</s>
<s>
Kašna	kašna	k1gFnSc1	kašna
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
Orlice	Orlice	k1gFnSc1	Orlice
<g/>
,	,	kIx,	,
eklektická	eklektický	k2eAgFnSc1d1	eklektická
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
starší	starší	k1gMnPc1	starší
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Antonína	Antonín	k1gMnSc2	Antonín
Paduánského	paduánský	k2eAgMnSc2d1	paduánský
v	v	k7c6	v
Žofínské	žofínský	k2eAgFnSc6d1	Žofínská
ulici	ulice	k1gFnSc6	ulice
u	u	k7c2	u
čp.	čp.	k?	čp.
132	[number]	k4	132
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Floriána	Florián	k1gMnSc2	Florián
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
z	z	k7c2	z
Jirchářské	jirchářský	k2eAgFnSc2d1	Jirchářská
ulice	ulice	k1gFnSc2	ulice
na	na	k7c4	na
Příkopy	příkop	k1gInPc4	příkop
Socha	Socha	k1gMnSc1	Socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1693	[number]	k4	1693
u	u	k7c2	u
<g />
.	.	kIx.	.
</s>
<s>
silnice	silnice	k1gFnPc1	silnice
k	k	k7c3	k
Doudlebám	Doudleba	k1gFnPc3	Doudleba
Sloup	sloup	k1gInSc4	sloup
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1693	[number]	k4	1693
nad	nad	k7c7	nad
cihelnou	cihelna	k1gFnSc7	cihelna
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Vincence	Vincenc	k1gMnSc2	Vincenc
de	de	k?	de
Pauli	Paule	k1gFnSc4	Paule
u	u	k7c2	u
hřbitova	hřbitov	k1gInSc2	hřbitov
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1753	[number]	k4	1753
Pilíř	pilíř	k1gInSc1	pilíř
se	s	k7c7	s
sousoším	sousoší	k1gNnSc7	sousoší
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
<g/>
,	,	kIx,	,
pseudogotický	pseudogotický	k2eAgInSc1d1	pseudogotický
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
Socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
u	u	k7c2	u
býv.	býv.	k?	býv.
Velkého	velký	k2eAgInSc2d1	velký
mlýna	mlýn	k1gInSc2	mlýn
†	†	k?	†
Borovice	borovice	k1gFnSc1	borovice
Erbenka	Erbenka	k1gFnSc1	Erbenka
Buk	buk	k1gInSc1	buk
za	za	k7c7	za
dráhou	dráha	k1gFnSc7	dráha
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Červený	červený	k2eAgInSc1d1	červený
buk	buk	k1gInSc1	buk
u	u	k7c2	u
křižovatky	křižovatka	k1gFnSc2	křižovatka
(	(	kIx(	(
<g/>
)	)	kIx)	)
4	[number]	k4	4
lípy	lípa	k1gFnPc4	lípa
na	na	k7c4	na
Palackého	Palackého	k2eAgNnSc4d1	Palackého
náměstí	náměstí	k1gNnSc4	náměstí
(	(	kIx(	(
<g/>
)	)	kIx)	)
Lípa	lípa	k1gFnSc1	lípa
u	u	k7c2	u
památníku	památník	k1gInSc2	památník
"	"	kIx"	"
<g/>
Obětem	oběť	k1gFnPc3	oběť
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
obchodní	obchodní	k2eAgFnSc3d1	obchodní
akademii	akademie	k1gFnSc3	akademie
TGM	TGM	kA	TGM
<g/>
,	,	kIx,	,
)	)	kIx)	)
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Stradinské	Stradinský	k2eAgFnSc6d1	Stradinský
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
)	)	kIx)	)
Platan	platan	k1gInSc1	platan
u	u	k7c2	u
pošty	pošta	k1gFnSc2	pošta
(	(	kIx(	(
<g/>
)	)	kIx)	)
Platanové	platanový	k2eAgNnSc1d1	platanový
stromořadí	stromořadí	k1gNnSc1	stromořadí
Platanka	Platanka	k1gFnSc1	Platanka
(	(	kIx(	(
<g/>
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Pelclova	Pelclův	k2eAgFnSc1d1	Pelclova
<g/>
,	,	kIx,	,
až	až	k9	až
(	(	kIx(	(
<g/>
))	))	k?	))
Poblíž	poblíž	k7c2	poblíž
obce	obec	k1gFnSc2	obec
prý	prý	k9	prý
kdysi	kdysi	k6eAd1	kdysi
stávala	stávat	k5eAaImAgFnS	stávat
osada	osada	k1gFnSc1	osada
Grunda	Grunda	k1gFnSc1	Grunda
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k9	jednou
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
orali	orat	k5eAaImAgMnP	orat
dva	dva	k4xCgMnPc1	dva
čeledíni	čeledín	k1gMnPc1	čeledín
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
muž	muž	k1gMnSc1	muž
vyoral	vyorat	k5eAaPmAgMnS	vyorat
zvon	zvon	k1gInSc4	zvon
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
Ambrož	Ambrož	k1gMnSc1	Ambrož
<g/>
"	"	kIx"	"
a	a	k8xC	a
při	při	k7c6	při
zvonění	zvonění	k1gNnSc6	zvonění
volal	volat	k5eAaImAgMnS	volat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vyorali	vyorat	k5eAaPmAgMnP	vyorat
mě	já	k3xPp1nSc4	já
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Druhý	druhý	k4xOgMnSc1	druhý
čeledín	čeledín	k1gMnSc1	čeledín
také	také	k9	také
narazil	narazit	k5eAaPmAgMnS	narazit
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
na	na	k7c4	na
zvon	zvon	k1gInSc4	zvon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
to	ten	k3xDgNnSc4	ten
netušil	tušit	k5eNaImAgMnS	tušit
<g/>
,	,	kIx,	,
jen	jen	k9	jen
zlostně	zlostně	k6eAd1	zlostně
zaklel	zaklít	k5eAaPmAgInS	zaklít
a	a	k8xC	a
zvon	zvon	k1gInSc1	zvon
se	se	k3xPyFc4	se
propadl	propadnout	k5eAaPmAgInS	propadnout
pod	pod	k7c4	pod
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zvon	zvon	k1gInSc1	zvon
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgMnSc1d1	celý
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Býval	bývat	k5eAaImAgInS	bývat
zavěšen	zavěsit	k5eAaPmNgInS	zavěsit
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
v	v	k7c6	v
Grundě	Grunda	k1gFnSc6	Grunda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
schoval	schovat	k5eAaPmAgMnS	schovat
se	se	k3xPyFc4	se
do	do	k7c2	do
země	zem	k1gFnSc2	zem
před	před	k7c7	před
hrůzami	hrůza	k1gFnPc7	hrůza
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zvon	zvon	k1gInSc1	zvon
putuje	putovat	k5eAaImIp3nS	putovat
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
až	až	k6eAd1	až
dojde	dojít	k5eAaPmIp3nS	dojít
pod	pod	k7c4	pod
borovici	borovice	k1gFnSc4	borovice
Erbenku	Erbenka	k1gFnSc4	Erbenka
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
níž	nízce	k6eAd2	nízce
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
sedával	sedávat	k5eAaImAgMnS	sedávat
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svině	svině	k1gFnSc1	svině
jej	on	k3xPp3gMnSc4	on
vytrhne	vytrhnout	k5eAaPmIp3nS	vytrhnout
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zvon	zvon	k1gInSc1	zvon
zazvoní	zazvonit	k5eAaPmIp3nS	zazvonit
<g/>
,	,	kIx,	,
klášter	klášter	k1gInSc1	klášter
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
kráse	krása	k1gFnSc6	krása
a	a	k8xC	a
nastane	nastat	k5eAaPmIp3nS	nastat
nový	nový	k2eAgInSc4d1	nový
věk	věk	k1gInSc4	věk
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pověst	pověst	k1gFnSc1	pověst
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
úpravách	úprava	k1gFnPc6	úprava
převyprávěli	převyprávět	k5eAaPmAgMnP	převyprávět
i	i	k9	i
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
(	(	kIx(	(
<g/>
Staré	Staré	k2eAgFnPc1d1	Staré
pověsti	pověst	k1gFnPc1	pověst
české	český	k2eAgFnPc1d1	Česká
<g/>
)	)	kIx)	)
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
Věštkyně	věštkyně	k1gFnSc1	věštkyně
(	(	kIx(	(
<g/>
sbírka	sbírka	k1gFnSc1	sbírka
Kytice	kytice	k1gFnSc1	kytice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Albert	Albert	k1gMnSc1	Albert
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1890	[number]	k4	1890
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1952	[number]	k4	1952
Jeseník	Jeseník	k1gInSc1	Jeseník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
ředitel	ředitel	k1gMnSc1	ředitel
Baťovy	Baťův	k2eAgFnSc2d1	Baťova
nemocnice	nemocnice	k1gFnSc2	nemocnice
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
Ladislav	Ladislav	k1gMnSc1	Ladislav
Karel	Karel	k1gMnSc1	Karel
Feierabend	Feierabend	k1gMnSc1	Feierabend
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1891	[number]	k4	1891
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g />
.	.	kIx.	.
</s>
<s>
1969	[number]	k4	1969
Villach	Villach	k1gInSc1	Villach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
národohospodář	národohospodář	k1gMnSc1	národohospodář
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
<g/>
,	,	kIx,	,
odbojář	odbojář	k1gMnSc1	odbojář
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
Kinský	Kinský	k1gMnSc1	Kinský
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1879	[number]	k4	1879
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
kosteleckého	kostelecký	k2eAgInSc2d1	kostelecký
velkostatku	velkostatek	k1gInSc2	velkostatek
<g/>
,	,	kIx,	,
disident	disident	k1gMnSc1	disident
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kolářský	kolářský	k2eAgMnSc1d1	kolářský
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1931	[number]	k4	1931
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
medailér	medailér	k1gMnSc1	medailér
Vratislav	Vratislav	k1gMnSc1	Vratislav
Pasovský	Pasovský	k1gMnSc1	Pasovský
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1854	[number]	k4	1854
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
-	-	kIx~	-
24	[number]	k4	24
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1924	[number]	k4	1924
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
stavitel	stavitel	k1gMnSc1	stavitel
z	z	k7c2	z
místní	místní	k2eAgFnSc2d1	místní
stavitelské	stavitelský	k2eAgFnSc2d1	stavitelská
<g />
.	.	kIx.	.
</s>
<s>
rodiny	rodina	k1gFnPc1	rodina
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
a	a	k8xC	a
iniciátor	iniciátor	k1gMnSc1	iniciátor
stavby	stavba	k1gFnSc2	stavba
petřínské	petřínský	k2eAgFnSc2d1	Petřínská
rozhledny	rozhledna	k1gFnSc2	rozhledna
Ignác	Ignác	k1gMnSc1	Ignác
Josef	Josef	k1gMnSc1	Josef
Pešina	Pešina	k1gMnSc1	Pešina
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1766	[number]	k4	1766
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1808	[number]	k4	1808
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvěrolékař	zvěrolékař	k1gMnSc1	zvěrolékař
Václav	Václav	k1gMnSc1	Václav
Roštlapil	Roštlapil	k1gMnSc1	Roštlapil
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1856	[number]	k4	1856
Zlonice	Zlonice	k1gFnSc2	Zlonice
-	-	kIx~	-
23	[number]	k4	23
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1930	[number]	k4	1930
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
Strakova	Strakův	k2eAgFnSc1d1	Strakova
akademie	akademie	k1gFnSc1	akademie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Zemská	zemský	k2eAgFnSc1d1	zemská
léčebna	léčebna	k1gFnSc1	léčebna
choromyslných	choromyslný	k1gMnPc2	choromyslný
v	v	k7c6	v
Bohnicích	Bohnice	k1gInPc6	Bohnice
<g/>
)	)	kIx)	)
Šabatové	Šabatová	k1gFnPc1	Šabatová
z	z	k7c2	z
Vrbic	vrbice	k1gFnPc2	vrbice
-	-	kIx~	-
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
František	František	k1gMnSc1	František
Ignác	Ignác	k1gMnSc1	Ignác
Tůma	Tůma	k1gMnSc1	Tůma
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
října	říjen	k1gInSc2	říjen
1704	[number]	k4	1704
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1774	[number]	k4	1774
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgMnSc1d1	barokní
skladatel	skladatel	k1gMnSc1	skladatel
František	František	k1gMnSc1	František
Jan	Jan	k1gMnSc1	Jan
Zoubek	Zoubek	k1gMnSc1	Zoubek
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1832	[number]	k4	1832
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1890	[number]	k4	1890
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgMnSc1d1	regionální
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
komeniolog	komeniolog	k1gMnSc1	komeniolog
<g/>
,	,	kIx,	,
redaktor	redaktor	k1gMnSc1	redaktor
<g />
.	.	kIx.	.
</s>
<s>
časopisu	časopis	k1gInSc2	časopis
Památky	památka	k1gFnSc2	památka
archaeologické	archaeologický	k2eAgFnSc2d1	archaeologický
a	a	k8xC	a
místopisné	místopisný	k2eAgFnSc2d1	místopisná
Přes	přes	k7c4	přes
Kostelec	Kostelec	k1gInSc4	Kostelec
vede	vést	k5eAaImIp3nS	vést
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Týniště	týniště	k1gNnSc2	týniště
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
-	-	kIx~	-
Letohrad	letohrad	k1gInSc1	letohrad
(	(	kIx(	(
<g/>
Velký	velký	k2eAgInSc1d1	velký
Osek	Osek	k1gInSc1	Osek
-	-	kIx~	-
Kyšperk	Kyšperk	k1gInSc1	Kyšperk
<g/>
)	)	kIx)	)
se	s	k7c7	s
stanicí	stanice	k1gFnSc7	stanice
Kostelec	Kostelec	k1gInSc4	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
;	;	kIx,	;
v	v	k7c6	v
r.	r.	kA	r.
1935	[number]	k4	1935
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
ještě	ještě	k9	ještě
zastávka	zastávka	k1gFnSc1	zastávka
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
město	město	k1gNnSc1	město
blíže	blízce	k6eAd2	blízce
centra	centrum	k1gNnPc1	centrum
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
všechny	všechen	k3xTgInPc4	všechen
vlaky	vlak	k1gInPc4	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
(	(	kIx(	(
<g/>
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
-	-	kIx~	-
Vamberk	Vamberk	k1gInSc1	Vamberk
-	-	kIx~	-
Šumperk	Šumperk	k1gInSc1	Šumperk
<g/>
)	)	kIx)	)
prochází	procházet	k5eAaImIp3nS	procházet
středem	střed	k1gInSc7	střed
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jezdí	jezdit	k5eAaImIp3nS	jezdit
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc1d1	jiný
autobusové	autobusový	k2eAgInPc1d1	autobusový
spoje	spoj	k1gInPc1	spoj
spojující	spojující	k2eAgFnSc4d1	spojující
Prahu	Praha	k1gFnSc4	Praha
s	s	k7c7	s
Orlickými	orlický	k2eAgFnPc7d1	Orlická
horami	hora	k1gFnPc7	hora
a	a	k8xC	a
Jeseníky	Jeseník	k1gInPc7	Jeseník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
místních	místní	k2eAgFnPc2d1	místní
linek	linka	k1gFnPc2	linka
je	být	k5eAaImIp3nS	být
nejfrekventovanější	frekventovaný	k2eAgFnSc1d3	nejfrekventovanější
linka	linka	k1gFnSc1	linka
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
,	,	kIx,	,
Doudleby	Doudleby	k1gInPc7	Doudleby
<g/>
,	,	kIx,	,
Vamberk	Vamberk	k1gInSc1	Vamberk
a	a	k8xC	a
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
;	;	kIx,	;
několik	několik	k4yIc4	několik
spojů	spoj	k1gInPc2	spoj
jezdí	jezdit	k5eAaImIp3nP	jezdit
také	také	k9	také
do	do	k7c2	do
Chocně	Choceň	k1gFnSc2	Choceň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
r.	r.	kA	r.
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
provozována	provozován	k2eAgFnSc1d1	provozována
městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
zajišťující	zajišťující	k2eAgNnSc1d1	zajišťující
spojení	spojení	k1gNnSc1	spojení
centra	centrum	k1gNnSc2	centrum
s	s	k7c7	s
okrajovými	okrajový	k2eAgFnPc7d1	okrajová
částmi	část	k1gFnPc7	část
<g/>
.	.	kIx.	.
</s>
<s>
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
Koryta	koryto	k1gNnSc2	koryto
Kostelecká	kostelecký	k2eAgFnSc1d1	kostelecká
Lhota	Lhota	k1gFnSc1	Lhota
Kozodry	Kozodr	k1gInPc1	Kozodr
Dětský	dětský	k2eAgInSc1d1	dětský
domov	domov	k1gInSc4	domov
se	s	k7c7	s
školou	škola	k1gFnSc7	škola
a	a	k8xC	a
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Dětský	dětský	k2eAgInSc4d1	dětský
domov	domov	k1gInSc4	domov
<g/>
,	,	kIx,	,
ZŠ	ZŠ	kA	ZŠ
<g/>
,	,	kIx,	,
školní	školní	k2eAgFnSc1d1	školní
družina	družina	k1gFnSc1	družina
a	a	k8xC	a
školní	školní	k2eAgFnSc1d1	školní
jídelna	jídelna	k1gFnSc1	jídelna
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
VOŠ	VOŠ	kA	VOŠ
<g/>
,	,	kIx,	,
SOŠ	SOŠ	kA	SOŠ
a	a	k8xC	a
SOU	sou	k1gInSc1	sou
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Jiřího	Jiří	k1gMnSc2	Jiří
Gutha	Guth	k1gMnSc2	Guth
-	-	kIx~	-
Jarkovského	Jarkovský	k2eAgMnSc2d1	Jarkovský
Kostelec	Kostelec	k1gInSc4	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
Myjava	Myjava	k1gFnSc1	Myjava
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Zeulenroda-Triebes	Zeulenroda-Triebesa	k1gFnPc2	Zeulenroda-Triebesa
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Vale	val	k1gInSc6	val
Royal	Royal	k1gMnSc1	Royal
Borough	Borough	k1gMnSc1	Borough
Council	Council	k1gMnSc1	Council
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
E.	E.	kA	E.
Poche	Poch	k1gInSc2	Poch
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Umělecké	umělecký	k2eAgFnSc2d1	umělecká
památky	památka	k1gFnSc2	památka
Čech	Čechy	k1gFnPc2	Čechy
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
"	"	kIx"	"
ŠORM	Šorm	k1gMnSc1	Šorm
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Pověsti	pověst	k1gFnPc1	pověst
o	o	k7c6	o
českých	český	k2eAgInPc6d1	český
zvonech	zvon	k1gInPc6	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
V.	V.	kA	V.
Kotrba	kotrba	k1gFnSc1	kotrba
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
ZDRÁLEK	ZDRÁLEK	kA	ZDRÁLEK
<g/>
,	,	kIx,	,
Kamil	Kamil	k1gMnSc1	Kamil
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
v	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
