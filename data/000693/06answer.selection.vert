<s>
Město	město	k1gNnSc1	město
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Adlerkosteletz	Adlerkosteletz	k1gInSc1	Adlerkosteletz
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
7	[number]	k4	7
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Rychnova	Rychnov	k1gInSc2	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
a	a	k8xC	a
29	[number]	k4	29
km	km	kA	km
vjv	vjv	k?	vjv
od	od	k7c2	od
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
