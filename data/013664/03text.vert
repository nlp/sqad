<s>
Ředitelství	ředitelství	k1gNnSc1
silnic	silnice	k1gFnPc2
a	a	k8xC
dálnic	dálnice	k1gFnPc2
ČR	ČR	kA
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
nahradit	nahradit	k5eAaPmF
odkazy	odkaz	k1gInPc4
na	na	k7c4
intranet	intranet	k1gInSc4
</s>
<s>
Budova	budova	k1gFnSc1
ŘSD	ŘSD	kA
v	v	k7c6
Čerčanské	Čerčanský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
na	na	k7c6
Kačerově	Kačerov	k1gInSc6
v	v	k7c6
Praze-Krči	Praze-Krč	k1gFnSc6
</s>
<s>
Ředitelství	ředitelství	k1gNnSc1
silnic	silnice	k1gFnPc2
a	a	k8xC
dálnic	dálnice	k1gFnPc2
ČR	ČR	kA
(	(	kIx(
<g/>
zkratkou	zkratka	k1gFnSc7
ŘSD	ŘSD	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
státní	státní	k2eAgFnSc1d1
příspěvková	příspěvkový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
zřízená	zřízený	k2eAgFnSc1d1
Ministerstvem	ministerstvo	k1gNnSc7
dopravy	doprava	k1gFnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základním	základní	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
činnosti	činnost	k1gFnSc2
organizace	organizace	k1gFnSc2
je	být	k5eAaImIp3nS
výkon	výkon	k1gInSc4
vlastnických	vlastnický	k2eAgNnPc2d1
práv	právo	k1gNnPc2
státu	stát	k1gInSc2
k	k	k7c3
nemovitým	movitý	k2eNgFnPc3d1
věcem	věc	k1gFnPc3
tvořícím	tvořící	k2eAgFnPc3d1
dálnice	dálnice	k1gFnSc1
a	a	k8xC
silnice	silnice	k1gFnSc1
I.	I.	kA
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
zabezpečení	zabezpečení	k1gNnSc2
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
údržby	údržba	k1gFnSc2
a	a	k8xC
oprav	oprava	k1gFnPc2
dálnic	dálnice	k1gFnPc2
a	a	k8xC
silnic	silnice	k1gFnPc2
I.	I.	kA
třídy	třída	k1gFnSc2
a	a	k8xC
zabezpečení	zabezpečení	k1gNnSc2
výstavby	výstavba	k1gFnSc2
a	a	k8xC
modernizace	modernizace	k1gFnSc2
dálnic	dálnice	k1gFnPc2
a	a	k8xC
silnic	silnice	k1gFnPc2
I.	I.	kA
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
ŘSD	ŘSD	kA
vzniklo	vzniknout	k5eAaPmAgNnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
1997	#num#	k4
sloučením	sloučení	k1gNnSc7
dřívějšího	dřívější	k2eAgNnSc2d1
Ředitelství	ředitelství	k1gNnSc2
dálnic	dálnice	k1gFnPc2
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Ředitelství	ředitelství	k1gNnSc1
silnic	silnice	k1gFnPc2
ČR	ČR	kA
a	a	k8xC
jednotlivých	jednotlivý	k2eAgInPc2d1
silničních	silniční	k2eAgInPc2d1
investorských	investorský	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
(	(	kIx(
<g/>
SIÚ	SIÚ	kA
<g/>
)	)	kIx)
v	v	k7c6
sídelních	sídelní	k2eAgNnPc6d1
městech	město	k1gNnPc6
tehdejších	tehdejší	k2eAgMnPc2d1
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
<g/>
,	,	kIx,
Plzni	Plzeň	k1gFnSc6
<g/>
,	,	kIx,
Liberci	Liberec	k1gInSc6
<g/>
,	,	kIx,
Pardubicích	Pardubice	k1gInPc6
<g/>
,	,	kIx,
Brně	Brno	k1gNnSc6
a	a	k8xC
Ostravě	Ostrava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodnutí	rozhodnutí	k1gNnSc2
vydal	vydat	k5eAaPmAgMnS
ministr	ministr	k1gMnSc1
dopravy	doprava	k1gFnSc2
a	a	k8xC
spojů	spoj	k1gInPc2
ČR	ČR	kA
dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Představitelé	představitel	k1gMnPc1
organizace	organizace	k1gFnSc2
</s>
<s>
V	v	k7c6
čele	čelo	k1gNnSc6
předchůdce	předchůdce	k1gMnSc4
ŘSD	ŘSD	kA
<g/>
,	,	kIx,
jímž	jenž	k3xRgNnSc7
bylo	být	k5eAaImAgNnS
Ředitelství	ředitelství	k1gNnSc1
dálnic	dálnice	k1gFnPc2
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
stáli	stát	k5eAaImAgMnP
ředitelé	ředitel	k1gMnPc1
</s>
<s>
Josef	Josef	k1gMnSc1
Moc	moc	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1967	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1970	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Kopecký	Kopecký	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1971	#num#	k4
–	–	k?
19	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1975	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Milan	Milan	k1gMnSc1
Černý	Černý	k1gMnSc1
16	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1975	#num#	k4
–	–	k?
16	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1978	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Šikula	Šikula	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1978	#num#	k4
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1990	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Čipera	Čipera	k1gMnSc1
15	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1990	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
čele	čelo	k1gNnSc6
ŘSD	ŘSD	kA
stojí	stát	k5eAaImIp3nS
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
jimi	on	k3xPp3gMnPc7
<g/>
:	:	kIx,
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
Osoba	osoba	k1gFnSc1
</s>
<s>
Funkční	funkční	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Čipera	Čipera	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1997	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2000	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
-	-	kIx~
</s>
<s>
Petr	Petr	k1gMnSc1
Laušman	Laušman	k1gMnSc1
(	(	kIx(
<g/>
pověřený	pověřený	k2eAgMnSc1d1
řízením	řízení	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2001	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2001	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pavel	Pavel	k1gMnSc1
Havíř	havíř	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2001	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2002	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Petr	Petr	k1gMnSc1
Laušman	Laušman	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2002	#num#	k4
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
-	-	kIx~
</s>
<s>
Michal	Michal	k1gMnSc1
Hala	halo	k1gNnSc2
(	(	kIx(
<g/>
pověřený	pověřený	k2eAgInSc1d1
řízením	řízení	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2007	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Alfred	Alfred	k1gMnSc1
Brunclík	Brunclík	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2007	#num#	k4
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2010	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
Švorc	Švorc	k?
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2010	#num#	k4
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
-	-	kIx~
</s>
<s>
René	René	k1gFnSc1
Poruba	Poruba	k1gFnSc1
(	(	kIx(
<g/>
pověřený	pověřený	k2eAgMnSc1d1
řízením	řízení	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
David	David	k1gMnSc1
Čermák	Čermák	k1gMnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2012	#num#	k4
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pavel	Pavel	k1gMnSc1
Kočica	Kočica	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2014	#num#	k4
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
-	-	kIx~
</s>
<s>
Jiří	Jiří	k1gMnSc1
Mayer	Mayer	k1gMnSc1
(	(	kIx(
<g/>
pověřený	pověřený	k2eAgMnSc1d1
řízením	řízení	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2014	#num#	k4
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Kubiš	Kubiš	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
-	-	kIx~
</s>
<s>
Soňa	Soňa	k1gFnSc1
Křítková	Křítkový	k2eAgFnSc1d1
(	(	kIx(
<g/>
pověřena	pověřit	k5eAaPmNgFnS
řízením	řízení	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Kroupa	Kroupa	k1gMnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pavol	Pavol	k1gInSc1
Kováčik	Kováčika	k1gFnPc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
-	-	kIx~
</s>
<s>
Radek	Radek	k1gMnSc1
Mátl	mást	k5eAaImAgMnS
(	(	kIx(
<g/>
pověřen	pověřen	k2eAgMnSc1d1
řízením	řízení	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Radek	Radek	k1gMnSc1
Mátl	mást	k5eAaImAgMnS
</s>
<s>
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předražování	předražování	k1gNnSc1
staveb	stavba	k1gFnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Dálnice	dálnice	k1gFnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Kanálový	kanálový	k2eAgInSc1d1
poklop	poklop	k1gInSc1
s	s	k7c7
logem	log	k1gInSc7
ŘSD	ŘSD	kA
</s>
<s>
Na	na	k7c4
vysoké	vysoký	k2eAgFnPc4d1
ceny	cena	k1gFnPc4
dopravních	dopravní	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
upozornil	upozornit	k5eAaPmAgInS
Nejvyšší	vysoký	k2eAgInSc1d3
kontrolní	kontrolní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
–	–	k?
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
kontroloval	kontrolovat	k5eAaImAgMnS
435	#num#	k4
akcí	akce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
stály	stát	k5eAaImAgInP
423	#num#	k4
miliard	miliarda	k4xCgFnPc2
korun	koruna	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
došel	dojít	k5eAaPmAgInS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
stály	stát	k5eAaImAgFnP
o	o	k7c4
22	#num#	k4
procent	procento	k1gNnPc2
víc	hodně	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
měly	mít	k5eAaImAgFnP
<g/>
,	,	kIx,
a	a	k8xC
podle	podle	k7c2
harmonogramu	harmonogram	k1gInSc2
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
jen	jen	k9
třetina	třetina	k1gFnSc1
staveb	stavba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Bezpečnostní	bezpečnostní	k2eAgFnSc2d1
informační	informační	k2eAgFnSc2d1
služby	služba	k1gFnSc2
(	(	kIx(
<g/>
BIS	BIS	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
kolem	kolem	k7c2
výstavby	výstavba	k1gFnSc2
dálnic	dálnice	k1gFnPc2
angažují	angažovat	k5eAaBmIp3nP
vlivové	vlivový	k2eAgFnPc1d1
a	a	k8xC
nátlakové	nátlakový	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ředitel	ředitel	k1gMnSc1
ŘSD	ŘSD	kA
v	v	k7c6
letech	léto	k1gNnPc6
2007	#num#	k4
až	až	k9
2010	#num#	k4
Alfréd	Alfréd	k1gMnSc1
Brunclík	Brunclík	k1gMnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
aktivit	aktivita	k1gFnPc2
vedoucích	vedoucí	k1gMnPc2
ke	k	k7c3
zlevnění	zlevnění	k1gNnSc3
stavebních	stavební	k2eAgFnPc2d1
prací	práce	k1gFnPc2
ustanovil	ustanovit	k5eAaPmAgInS
ve	v	k7c6
struktuře	struktura	k1gFnSc6
ŘSD	ŘSD	kA
Technickou	technický	k2eAgFnSc4d1
radu	rada	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
seděli	sedět	k5eAaImAgMnP
nezávislí	závislý	k2eNgMnPc1d1
odborníci	odborník	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
hledali	hledat	k5eAaImAgMnP
možnosti	možnost	k1gFnPc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
stavět	stavět	k5eAaImF
dopravní	dopravní	k2eAgFnSc4d1
infrastrukturu	infrastruktura	k1gFnSc4
levněji	levně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
se	se	k3xPyFc4
přehodnocovaly	přehodnocovat	k5eAaImAgFnP
maximální	maximální	k2eAgFnPc1d1
sklony	sklona	k1gFnPc1
dálnic	dálnice	k1gFnPc2
či	či	k8xC
poloměry	poloměr	k1gInPc4
zatáček	zatáčka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdyby	kdyby	kYmCp3nS
totiž	totiž	k9
cesty	cesta	k1gFnPc1
více	hodně	k6eAd2
opisovaly	opisovat	k5eAaImAgFnP
terén	terén	k1gInSc4
<g/>
,	,	kIx,
ušetřilo	ušetřit	k5eAaPmAgNnS
by	by	kYmCp3nS
se	se	k3xPyFc4
nejen	nejen	k6eAd1
na	na	k7c6
výstavbě	výstavba	k1gFnSc6
přemostění	přemostění	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
na	na	k7c6
souvisejících	související	k2eAgFnPc6d1
činnostech	činnost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alfréd	Alfréd	k1gMnSc1
Brunclík	Brunclík	k1gMnSc1
do	do	k7c2
ŘSD	ŘSD	kA
přinesl	přinést	k5eAaPmAgInS
také	také	k9
tzv.	tzv.	kA
projektové	projektový	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Rozhodnutí	rozhodnutí	k1gNnSc1
č.	č.	k?
12	#num#	k4
164	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
-KM	-KM	k?
ze	z	k7c2
dne	den	k1gInSc2
4.12	4.12	k4
<g/>
.1996	.1996	k4
<g/>
,	,	kIx,
kterým	který	k3yRgNnSc7,k3yIgNnSc7,k3yQgNnSc7
se	se	k3xPyFc4
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
Zřizovací	zřizovací	k2eAgFnSc1d1
listina	listina	k1gFnSc1
Ředitelství	ředitelství	k1gNnSc2
silnic	silnice	k1gFnPc2
a	a	k8xC
dálnic	dálnice	k1gFnPc2
ČR1	ČR1	k1gFnSc4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
PRÁŠIL	Prášil	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Padesát	padesát	k4xCc1
let	léto	k1gNnPc2
1967-2017	1967-2017	k4
od	od	k7c2
založení	založení	k1gNnSc2
ŘSD	ŘSD	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ředitelství	ředitelství	k1gNnSc1
silnic	silnice	k1gFnPc2
a	a	k8xC
dálnic	dálnice	k1gFnPc2
ČR	ČR	kA
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
244	#num#	k4
s.	s.	k?
S.	S.	kA
118	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Novým	nový	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
ŘSD	ŘSD	kA
je	být	k5eAaImIp3nS
Alfred	Alfred	k1gMnSc1
Brunclík	Brunclík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Ministr	ministr	k1gMnSc1
Bárta	Bárta	k1gMnSc1
vyměnil	vyměnit	k5eAaPmAgMnS
šéfa	šéf	k1gMnSc2
českých	český	k2eAgFnPc2d1
dálnic	dálnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brunclíkovi	Brunclíkův	k2eAgMnPc5d1
nedůvěřoval	důvěřovat	k5eNaImAgInS
|	|	kIx~
Doprava	doprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-08-24	2010-08-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.mediafax.cz	www.mediafax.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.ceskatelevize.cz/ct24/domaci/176088-do-cela-rsd-prichazi-poradce-ministru-cermak/	http://www.ceskatelevize.cz/ct24/domaci/176088-do-cela-rsd-prichazi-poradce-ministru-cermak/	k4
<g/>
↑	↑	k?
Šéf	šéf	k1gMnSc1
ŘSD	ŘSD	kA
skončil	skončit	k5eAaPmAgMnS
<g/>
,	,	kIx,
ministr	ministr	k1gMnSc1
chce	chtít	k5eAaImIp3nS
tvrdého	tvrdý	k2eAgMnSc4d1
vyjednavače	vyjednavač	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2013-10-01	2013-10-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Funkce	funkce	k1gFnPc1
šéfa	šéf	k1gMnSc2
ŘSD	ŘSD	kA
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgMnS
Pavel	Pavel	k1gMnSc1
Kočica	Kočica	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahrazuje	nahrazovat	k5eAaImIp3nS
odvolaného	odvolaný	k1gMnSc2
Davida	David	k1gMnSc4
Čermáka	Čermák	k1gMnSc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ministr	ministr	k1gMnSc1
dopravy	doprava	k1gFnSc2
odvolal	odvolat	k5eAaPmAgMnS
šéfa	šéf	k1gMnSc4
Ředitelství	ředitelství	k1gNnSc2
silnic	silnice	k1gFnPc2
a	a	k8xC
dálnic	dálnice	k1gFnPc2
Kočicu	Kočicus	k1gInSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2014-02-11	2014-02-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ministr	ministr	k1gMnSc1
dopravy	doprava	k1gFnSc2
Prachař	Prachař	k1gMnSc1
odvolal	odvolat	k5eAaPmAgMnS
šéfa	šéf	k1gMnSc4
Ředitelství	ředitelství	k1gNnSc2
silnic	silnice	k1gFnPc2
a	a	k8xC
dálnic	dálnice	k1gFnPc2
Kočicu	Kočicus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
http://www.ceskatelevize.cz/ct24/ekonomika/268113-rsd-opet-meni-vedeni-misto-povereneho-mayera-ho-bude-definitivne-ridit-kubis/	http://www.ceskatelevize.cz/ct24/ekonomika/268113-rsd-opet-meni-vedeni-misto-povereneho-mayera-ho-bude-definitivne-ridit-kubis/	k4
<g/>
↑	↑	k?
Prachař	Prachař	k1gMnSc1
mění	měnit	k5eAaImIp3nS
ŘSD	ŘSD	kA
<g/>
,	,	kIx,
dočasného	dočasný	k2eAgMnSc4d1
šéfa	šéf	k1gMnSc4
Mayera	Mayer	k1gMnSc4
nahradil	nahradit	k5eAaPmAgMnS
Kubiš	Kubiš	k1gMnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2014-03-31	2014-03-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.financninoviny.cz	www.financninoviny.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KALENSKÝ	KALENSKÝ	kA
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šéf	šéf	k1gMnSc1
ŘSD	ŘSD	kA
Kubiš	Kubiš	k1gMnSc1
po	po	k7c6
čtyřech	čtyři	k4xCgInPc6
měsících	měsíc	k1gInPc6
končí	končit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahradí	nahradit	k5eAaPmIp3nS
ho	on	k3xPp3gInSc4
manažerka	manažerka	k1gFnSc1
z	z	k7c2
Babišova	Babišův	k2eAgInSc2d1
Agrofertu	Agrofert	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Silničáře	silničář	k1gMnSc2
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
Kroupa	Kroupa	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yQgMnSc4,k3yRgMnSc4
před	před	k7c4
půl	půl	k1xP
rokem	rok	k1gInSc7
vyhodil	vyhodit	k5eAaPmAgMnS
Prachař	Prachař	k1gMnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2014-12-08	2014-12-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nový	nový	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
ŘSD	ŘSD	kA
Jan	Jan	k1gMnSc1
Kroupa	Kroupa	k1gMnSc1
chce	chtít	k5eAaImIp3nS
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
modernizaci	modernizace	k1gFnSc6
dálnice	dálnice	k1gFnSc2
D	D	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Šéf	šéf	k1gMnSc1
ŘSD	ŘSD	kA
ve	v	k7c6
funkci	funkce	k1gFnSc6
přežil	přežít	k5eAaPmAgMnS
nového	nový	k2eAgMnSc4d1
ministra	ministr	k1gMnSc4
jen	jen	k9
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2019-05-03	2019-05-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ministr	ministr	k1gMnSc1
Kremlík	Kremlík	k1gMnSc1
odvolal	odvolat	k5eAaPmAgMnS
šéfa	šéf	k1gMnSc4
Ředitelství	ředitelství	k1gNnSc2
silnic	silnice	k1gFnPc2
a	a	k8xC
dálnic	dálnice	k1gFnPc2
Kroupu	Kroupa	k1gMnSc4
<g/>
,	,	kIx,
nahradí	nahradit	k5eAaPmIp3nS
ho	on	k3xPp3gInSc4
Kováčik	Kováčik	k1gInSc4
z	z	k7c2
dopravního	dopravní	k2eAgInSc2d1
fondu	fond	k1gInSc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
TESÁRKOVÁ	Tesárková	k1gFnSc1
<g/>
,	,	kIx,
Zdeňka	Zdeněk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Personální	personální	k2eAgFnSc1d1
změna	změna	k1gFnSc1
–	–	k?
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
ŘSD	ŘSD	kA
ČR	ČR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ředitelství	ředitelství	k1gNnSc1
silnic	silnice	k1gFnPc2
a	a	k8xC
dálnic	dálnice	k1gFnPc2
ČR	ČR	kA
<g/>
,	,	kIx,
2019-07-27	2019-07-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
TESÁRKOVÁ	Tesárková	k1gFnSc1
<g/>
,	,	kIx,
Zdeňka	Zdeněk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jmenování	jmenování	k1gNnSc1
Ing.	ing.	kA
Radka	Radek	k1gMnSc2
Mátla	mást	k5eAaImAgFnS
generálním	generální	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
ŘSD	ŘSD	kA
ČR	ČR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ředitelství	ředitelství	k1gNnSc1
silnic	silnice	k1gFnPc2
a	a	k8xC
dálnic	dálnice	k1gFnPc2
ČR	ČR	kA
<g/>
,	,	kIx,
2019-10-02	2019-10-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
POKORNÝ	Pokorný	k1gMnSc1
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
záhada	záhada	k1gFnSc1
<g/>
:	:	kIx,
Proč	proč	k6eAd1
jsou	být	k5eAaImIp3nP
silnice	silnice	k1gFnPc1
tak	tak	k6eAd1
drahé	drahý	k2eAgFnPc1d1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Stát	stát	k1gInSc1
vymýšlí	vymýšlet	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
stavět	stavět	k5eAaImF
levnější	levný	k2eAgFnSc1d2
dálnice	dálnice	k1gFnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2009-05-12	2009-05-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ředitelství	ředitelství	k1gNnSc2
silnic	silnice	k1gFnPc2
a	a	k8xC
dálnic	dálnice	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Ředitelství	ředitelství	k1gNnSc1
silnic	silnice	k1gFnPc2
a	a	k8xC
dálnic	dálnice	k1gFnPc2
ČR	ČR	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2005264388	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
124814933	#num#	k4
</s>
