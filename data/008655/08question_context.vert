<s>
Joule	joule	k1gInSc1	joule
[	[	kIx(	[
<g/>
džaul	džaul	k1gInSc1	džaul
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
J	J	kA	J
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	se	k3xPyFc3	se
patří	patřit	k5eAaImIp3nS	patřit
joule	joule	k1gInSc4	joule
mezi	mezi	k7c4	mezi
odvozené	odvozený	k2eAgFnPc4d1	odvozená
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
jednotek	jednotka	k1gFnPc2	jednotka
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
vztahem	vztah	k1gInSc7	vztah
<g/>
:	:	kIx,	:
J	J	kA	J
=	=	kIx~	=
kg	kg	kA	kg
·	·	k?	·
m	m	kA	m
<g/>
2	[number]	k4	2
·	·	k?	·
s	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
též	též	k9	též
N	N	kA	N
·	·	k?	·
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kg	kg	kA	kg
je	být	k5eAaImIp3nS	být
kilogram	kilogram	k1gInSc4	kilogram
<g/>
,	,	kIx,	,
m	m	kA	m
je	být	k5eAaImIp3nS	být
metr	metr	k1gInSc4	metr
<g/>
,	,	kIx,	,
s	s	k7c7	s
je	být	k5eAaImIp3nS	být
sekunda	sekunda	k1gFnSc1	sekunda
<g/>
,	,	kIx,	,
N	N	kA	N
je	být	k5eAaImIp3nS	být
newton	newton	k1gInSc4	newton
1	[number]	k4	1
Joule	joule	k1gInSc1	joule
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k8xC	jako
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
koná	konat	k5eAaImIp3nS	konat
síla	síla	k1gFnSc1	síla
1	[number]	k4	1
N	N	kA	N
působící	působící	k2eAgFnSc1d1	působící
po	po	k7c6	po
dráze	dráha	k1gFnSc6	dráha
1	[number]	k4	1
m	m	kA	m
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
joule	joule	k1gInSc1	joule
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
anglického	anglický	k2eAgMnSc2d1	anglický
fyzika	fyzik	k1gMnSc2	fyzik
Jamese	Jamese	k1gFnSc2	Jamese
P.	P.	kA	P.
Joulea	Jouleus	k1gMnSc2	Jouleus
<g/>
.	.	kIx.	.
</s>

