<p>
<s>
Studentská	studentský	k2eAgFnSc1d1	studentská
komora	komora	k1gFnSc1	komora
Rady	rada	k1gFnSc2	rada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
SK	Sk	kA	Sk
RVŠ	RVŠ	kA	RVŠ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc7d1	národní
reprezentací	reprezentace	k1gFnSc7	reprezentace
studentek	studentka	k1gFnPc2	studentka
a	a	k8xC	a
studentů	student	k1gMnPc2	student
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
součástí	součást	k1gFnPc2	součást
Rady	rada	k1gFnSc2	rada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poslání	poslání	k1gNnSc2	poslání
==	==	k?	==
</s>
</p>
<p>
<s>
Studentská	studentský	k2eAgFnSc1d1	studentská
komora	komora	k1gFnSc1	komora
RVŠ	RVŠ	kA	RVŠ
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
všechny	všechen	k3xTgMnPc4	všechen
studenty	student	k1gMnPc4	student
a	a	k8xC	a
studentky	studentka	k1gFnPc4	studentka
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
jejich	jejich	k3xOp3gInPc4	jejich
zájmy	zájem	k1gInPc4	zájem
na	na	k7c6	na
národní	národní	k2eAgFnSc6d1	národní
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
mezi	mezi	k7c7	mezi
studujícími	studující	k2eAgFnPc7d1	studující
a	a	k8xC	a
vyučujícími	vyučující	k2eAgFnPc7d1	vyučující
<g/>
,	,	kIx,	,
dialog	dialog	k1gInSc4	dialog
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
orgány	orgán	k1gInPc4	orgán
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
akademické	akademický	k2eAgFnSc2d1	akademická
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Struktura	struktura	k1gFnSc1	struktura
==	==	k?	==
</s>
</p>
<p>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
jsou	být	k5eAaImIp3nP	být
delegováni	delegován	k2eAgMnPc1d1	delegován
akademickými	akademický	k2eAgInPc7d1	akademický
senáty	senát	k1gInPc7	senát
nebo	nebo	k8xC	nebo
obdobnými	obdobný	k2eAgInPc7d1	obdobný
orgány	orgán	k1gInPc7	orgán
v	v	k7c6	v
případě	případ	k1gInSc6	případ
soukromých	soukromý	k2eAgFnPc2d1	soukromá
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
komory	komora	k1gFnSc2	komora
stojí	stát	k5eAaImIp3nS	stát
předseda	předseda	k1gMnSc1	předseda
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
místopředsedu	místopředseda	k1gMnSc4	místopředseda
Rady	rada	k1gFnSc2	rada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
<g/>
Předsedou	předseda	k1gMnSc7	předseda
SK	Sk	kA	Sk
RVŠ	RVŠ	kA	RVŠ
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
místopředsedou	místopředseda	k1gMnSc7	místopředseda
RVŠ	RVŠ	kA	RVŠ
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
ustavující	ustavující	k2eAgFnSc6d1	ustavující
schůzi	schůze	k1gFnSc6	schůze
SK	Sk	kA	Sk
RVŠ	RVŠ	kA	RVŠ
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Michal	Michal	k1gMnSc1	Michal
Zima	Zima	k1gMnSc1	Zima
<g/>
,	,	kIx,	,
delegát	delegát	k1gMnSc1	delegát
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Místopředsedou	místopředseda	k1gMnSc7	místopředseda
pro	pro	k7c4	pro
organizaci	organizace	k1gFnSc4	organizace
a	a	k8xC	a
řízení	řízení	k1gNnSc4	řízení
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Karel	Karel	k1gMnSc1	Karel
Doleček	doleček	k1gInSc4	doleček
z	z	k7c2	z
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
místopředsedou	místopředseda	k1gMnSc7	místopředseda
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
záležitosti	záležitost	k1gFnPc4	záležitost
Bronislav	Bronislav	k1gMnSc1	Bronislav
Kolář	Kolář	k1gMnSc1	Kolář
z	z	k7c2	z
Univerzity	univerzita	k1gFnSc2	univerzita
obrany	obrana	k1gFnSc2	obrana
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Činnost	činnost	k1gFnSc4	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
Komora	komora	k1gFnSc1	komora
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nS	scházet
několikrát	několikrát	k6eAd1	několikrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
dle	dle	k7c2	dle
předem	předem	k6eAd1	předem
stanovených	stanovený	k2eAgInPc2d1	stanovený
termínů	termín	k1gInPc2	termín
<g/>
.	.	kIx.	.
</s>
<s>
Celkovou	celkový	k2eAgFnSc7d1	celková
agendou	agenda	k1gFnSc7	agenda
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
plénum	plénum	k1gNnSc1	plénum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
komisí	komise	k1gFnPc2	komise
<g/>
,	,	kIx,	,
sekcí	sekce	k1gFnPc2	sekce
a	a	k8xC	a
pracovních	pracovní	k2eAgFnPc2d1	pracovní
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Dílčí	dílčí	k2eAgFnPc1d1	dílčí
části	část	k1gFnPc1	část
pléna	plénum	k1gNnSc2	plénum
připravují	připravovat	k5eAaImIp3nP	připravovat
podklady	podklad	k1gInPc1	podklad
a	a	k8xC	a
návrhy	návrh	k1gInPc1	návrh
usnesení	usnesení	k1gNnSc4	usnesení
pro	pro	k7c4	pro
plenární	plenární	k2eAgNnSc4d1	plenární
zasedání	zasedání	k1gNnSc4	zasedání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
usnesení	usnesení	k1gNnSc2	usnesení
se	se	k3xPyFc4	se
Studentská	studentský	k2eAgFnSc1d1	studentská
komora	komora	k1gFnSc1	komora
Rady	rada	k1gFnSc2	rada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
k	k	k7c3	k
aktuálnímu	aktuální	k2eAgNnSc3d1	aktuální
dění	dění	k1gNnSc3	dění
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
k	k	k7c3	k
situacím	situace	k1gFnPc3	situace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
nepříznivě	příznivě	k6eNd1	příznivě
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
situaci	situace	k1gFnSc4	situace
studentů	student	k1gMnPc2	student
VŠ	vš	k0	vš
(	(	kIx(	(
<g/>
např.	např.	kA	např.
novelizace	novelizace	k1gFnSc1	novelizace
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
problémy	problém	k1gInPc1	problém
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
akreditace	akreditace	k1gFnSc2	akreditace
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
myšlenkou	myšlenka	k1gFnSc7	myšlenka
SK	Sk	kA	Sk
RVŠ	RVŠ	kA	RVŠ
je	být	k5eAaImIp3nS	být
také	také	k9	také
sdílení	sdílení	k1gNnSc4	sdílení
zkušeností	zkušenost	k1gFnPc2	zkušenost
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
posilování	posilování	k1gNnSc4	posilování
vztahů	vztah	k1gInPc2	vztah
studentských	studentský	k2eAgMnPc2d1	studentský
akademických	akademický	k2eAgMnPc2d1	akademický
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
pořádá	pořádat	k5eAaImIp3nS	pořádat
SK	Sk	kA	Sk
RVŠ	RVŠ	kA	RVŠ
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
konference	konference	k1gFnPc4	konference
a	a	k8xC	a
workshopy	workshop	k1gInPc4	workshop
pro	pro	k7c4	pro
studentské	studentský	k2eAgMnPc4d1	studentský
akademické	akademický	k2eAgMnPc4d1	akademický
senátory	senátor	k1gMnPc4	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
akcí	akce	k1gFnSc7	akce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
SK	Sk	kA	Sk
RVŠ	RVŠ	kA	RVŠ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
pořádala	pořádat	k5eAaImAgFnS	pořádat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Studentský	studentský	k2eAgInSc1d1	studentský
Konvent	konvent	k1gInSc1	konvent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SK	Sk	kA	Sk
RVŠ	RVŠ	kA	RVŠ
pravidelně	pravidelně	k6eAd1	pravidelně
pořádá	pořádat	k5eAaImIp3nS	pořádat
vzpomínkové	vzpomínkový	k2eAgFnPc4d1	vzpomínková
akce	akce	k1gFnPc4	akce
během	během	k7c2	během
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
-	-	kIx~	-
zejména	zejména	k9	zejména
v	v	k7c6	v
Žitné	žitný	k2eAgFnSc6d1	Žitná
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Delegáti	delegát	k1gMnPc1	delegát
SK	Sk	kA	Sk
RVŠ	RVŠ	kA	RVŠ
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
akreditační	akreditační	k2eAgFnSc7d1	akreditační
komisí	komise	k1gFnSc7	komise
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zástupci	zástupce	k1gMnPc1	zástupce
komory	komora	k1gFnSc2	komora
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
účastní	účastnit	k5eAaImIp3nS	účastnit
hodnocení	hodnocení	k1gNnSc1	hodnocení
škol	škola	k1gFnPc2	škola
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
členy	člen	k1gMnPc7	člen
Akreditační	akreditační	k2eAgFnSc2d1	akreditační
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studentská	studentský	k2eAgFnSc1d1	studentská
komora	komora	k1gFnSc1	komora
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
i	i	k9	i
aktivně	aktivně	k6eAd1	aktivně
zastává	zastávat	k5eAaImIp3nS	zastávat
studentů	student	k1gMnPc2	student
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ohrožení	ohrožení	k1gNnSc2	ohrožení
jejich	jejich	k3xOp3gNnPc2	jejich
studentských	studentský	k2eAgNnPc2d1	studentské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
i	i	k9	i
další	další	k2eAgFnPc4d1	další
aktivity	aktivita	k1gFnPc4	aktivita
zlepšující	zlepšující	k2eAgNnSc4d1	zlepšující
studium	studium	k1gNnSc4	studium
a	a	k8xC	a
postavení	postavení	k1gNnSc4	postavení
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
například	například	k6eAd1	například
zasadila	zasadit	k5eAaPmAgFnS	zasadit
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
příspěvků	příspěvek	k1gInPc2	příspěvek
pro	pro	k7c4	pro
doktorandy	doktorand	k1gMnPc4	doktorand
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
a	a	k8xC	a
zrušení	zrušení	k1gNnSc4	zrušení
povinnosti	povinnost	k1gFnSc2	povinnost
platit	platit	k5eAaImF	platit
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
pro	pro	k7c4	pro
nepracující	pracující	k2eNgMnPc4d1	nepracující
studenty	student	k1gMnPc4	student
doktorských	doktorský	k2eAgNnPc2d1	doktorské
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
sekce	sekce	k1gFnSc2	sekce
a	a	k8xC	a
pracovní	pracovní	k2eAgFnSc2d1	pracovní
skupiny	skupina	k1gFnSc2	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
stálém	stálý	k2eAgNnSc6d1	stálé
zasedání	zasedání	k1gNnSc6	zasedání
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
2020	[number]	k4	2020
<g/>
)	)	kIx)	)
fungují	fungovat	k5eAaImIp3nP	fungovat
ve	v	k7c6	v
Studentské	studentský	k2eAgFnSc6d1	studentská
komoře	komora	k1gFnSc6	komora
Rady	rada	k1gFnSc2	rada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
tyto	tento	k3xDgFnPc1	tento
komise	komise	k1gFnSc1	komise
a	a	k8xC	a
pracovní	pracovní	k2eAgFnPc1d1	pracovní
skupiny	skupina	k1gFnPc1	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
vysokoškolskou	vysokoškolský	k2eAgFnSc4d1	vysokoškolská
samosprávu	samospráva	k1gFnSc4	samospráva
(	(	kIx(	(
<g/>
KVS	KVS	kA	KVS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
<g/>
:	:	kIx,	:
Naděžda	Naděžda	k1gFnSc1	Naděžda
Glincová	Glincový	k2eAgFnSc1d1	Glincový
<g/>
,	,	kIx,	,
delegátka	delegátka	k1gFnSc1	delegátka
Slezské	slezský	k2eAgFnSc2d1	Slezská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
OpavěKomise	OpavěKomis	k1gInSc6	OpavěKomis
pro	pro	k7c4	pro
doktorské	doktorský	k2eAgNnSc4d1	doktorské
studium	studium	k1gNnSc4	studium
(	(	kIx(	(
<g/>
KDS	KDS	kA	KDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
<g/>
:	:	kIx,	:
Iveta	Iveta	k1gFnSc1	Iveta
Kopřivová	Kopřivová	k1gFnSc1	Kopřivová
<g/>
,	,	kIx,	,
delegátka	delegátka	k1gFnSc1	delegátka
Mendelovy	Mendelův	k2eAgFnSc2d1	Mendelova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
Legislativní	legislativní	k2eAgFnSc1d1	legislativní
komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
LK	LK	kA	LK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Lukáš	Lukáš	k1gMnSc1	Lukáš
Hulínský	hulínský	k2eAgMnSc1d1	hulínský
<g/>
,	,	kIx,	,
delegát	delegát	k1gMnSc1	delegát
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
v	v	k7c4	v
PrazePředsedkyně	PrazePředsedkyně	k1gFnPc4	PrazePředsedkyně
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
komisí	komise	k1gFnPc2	komise
tvoří	tvořit	k5eAaImIp3nS	tvořit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
a	a	k8xC	a
místopředsedy	místopředseda	k1gMnPc7	místopředseda
členy	člen	k1gInPc4	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
SK	Sk	kA	Sk
RVŠ	RVŠ	kA	RVŠ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
skupina	skupina	k1gFnSc1	skupina
pro	pro	k7c4	pro
PR	pr	k0	pr
(	(	kIx(	(
<g/>
PRASK	prask	k0	prask
PR	pr	k0	pr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
skupina	skupina	k1gFnSc1	skupina
Zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
záležitosti	záležitost	k1gFnPc1	záležitost
(	(	kIx(	(
<g/>
PRASK	prask	k1gInSc1	prask
ZZ	ZZ	kA	ZZ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
skupina	skupina	k1gFnSc1	skupina
pro	pro	k7c4	pro
kvalitu	kvalita	k1gFnSc4	kvalita
v	v	k7c6	v
terciárním	terciární	k2eAgNnSc6d1	terciární
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
(	(	kIx(	(
<g/>
PRASK	prask	k1gInSc1	prask
Q	Q	kA	Q
<g/>
)	)	kIx)	)
<g/>
Pracovní	pracovní	k2eAgFnPc1d1	pracovní
skupiny	skupina	k1gFnPc1	skupina
jsou	být	k5eAaImIp3nP	být
uskupení	uskupení	k1gNnSc4	uskupení
spíše	spíše	k9	spíše
neformálního	formální	k2eNgInSc2d1	neformální
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
koordinátor	koordinátor	k1gMnSc1	koordinátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
stálém	stálý	k2eAgNnSc6d1	stálé
zasedání	zasedání	k1gNnSc6	zasedání
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
fungovaly	fungovat	k5eAaImAgInP	fungovat
ve	v	k7c6	v
Studentské	studentský	k2eAgFnSc6d1	studentská
komoře	komora	k1gFnSc6	komora
Rady	rada	k1gFnSc2	rada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
tyto	tento	k3xDgFnPc1	tento
komise	komise	k1gFnPc1	komise
<g/>
,	,	kIx,	,
sekce	sekce	k1gFnPc1	sekce
a	a	k8xC	a
pracovní	pracovní	k2eAgFnPc1d1	pracovní
skupiny	skupina	k1gFnPc1	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
sociální	sociální	k2eAgFnPc4d1	sociální
a	a	k8xC	a
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
záležitosti	záležitost	k1gFnPc4	záležitost
(	(	kIx(	(
<g/>
KSEZ	KSEZ	kA	KSEZ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
vysokoškolskou	vysokoškolský	k2eAgFnSc4d1	vysokoškolská
samosprávu	samospráva	k1gFnSc4	samospráva
(	(	kIx(	(
<g/>
KVS	KVS	kA	KVS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
doktorské	doktorský	k2eAgNnSc4d1	doktorské
studium	studium	k1gNnSc4	studium
(	(	kIx(	(
<g/>
KDS	KDS	kA	KDS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Legislativní	legislativní	k2eAgFnSc1d1	legislativní
komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
LK	LK	kA	LK
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sekce	sekce	k1gFnSc1	sekce
soukromých	soukromý	k2eAgFnPc2d1	soukromá
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
SSVŠ	SSVŠ	kA	SSVŠ
<g/>
)	)	kIx)	)
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
sekce	sekce	k1gFnSc1	sekce
má	mít	k5eAaImIp3nS	mít
výsadu	výsada	k1gFnSc4	výsada
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
hlasovat	hlasovat	k5eAaImF	hlasovat
o	o	k7c6	o
některých	některý	k3yIgNnPc6	některý
usneseních	usnesení	k1gNnPc6	usnesení
komory	komora	k1gFnSc2	komora
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
plénaPředsedové	plénaPředsedový	k2eAgFnPc1d1	plénaPředsedový
komisí	komise	k1gFnSc7	komise
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
Sekce	sekce	k1gFnSc2	sekce
soukromých	soukromý	k2eAgFnPc2d1	soukromá
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
tvořili	tvořit	k5eAaImAgMnP	tvořit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
a	a	k8xC	a
místopředsedy	místopředseda	k1gMnPc7	místopředseda
členy	člen	k1gInPc4	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
SK	Sk	kA	Sk
RVŠ	RVŠ	kA	RVŠ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pracovní	pracovní	k2eAgFnPc1d1	pracovní
skupiny	skupina	k1gFnPc1	skupina
pro	pro	k7c4	pro
organizaci	organizace	k1gFnSc4	organizace
Ceny	cena	k1gFnSc2	cena
Jana	Jan	k1gMnSc2	Jan
Opletala	Opletal	k1gMnSc2	Opletal
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
PRASK	prask	k1gInSc1	prask
CJO	CJO	kA	CJO
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
skupina	skupina	k1gFnSc1	skupina
pro	pro	k7c4	pro
PR	pr	k0	pr
(	(	kIx(	(
<g/>
PRASK	prask	k0	prask
PR	pr	k0	pr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
skupina	skupina	k1gFnSc1	skupina
Zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
záležitosti	záležitost	k1gFnPc1	záležitost
(	(	kIx(	(
<g/>
PRASK	prask	k1gInSc1	prask
ZZ	ZZ	kA	ZZ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Rada	rada	k1gFnSc1	rada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
RVŠ	RVŠ	kA	RVŠ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
studenti	student	k1gMnPc1	student
členy	člen	k1gMnPc4	člen
RVŠ	RVŠ	kA	RVŠ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
delegáti	delegát	k1gMnPc1	delegát
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Studentská	studentský	k2eAgFnSc1d1	studentská
komora	komora	k1gFnSc1	komora
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
nebyli	být	k5eNaImAgMnP	být
členové	člen	k1gMnPc1	člen
RVŠ	RVŠ	kA	RVŠ
<g/>
,	,	kIx,	,
až	až	k9	až
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
jsou	být	k5eAaImIp3nP	být
členové	člen	k1gMnPc1	člen
Komory	komora	k1gFnSc2	komora
plnoprávnými	plnoprávný	k2eAgInPc7d1	plnoprávný
členy	člen	k1gInPc7	člen
Rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cena	cena	k1gFnSc1	cena
Jana	Jan	k1gMnSc2	Jan
Opletala	Opletal	k1gMnSc2	Opletal
==	==	k?	==
</s>
</p>
<p>
<s>
Studentská	studentský	k2eAgFnSc1d1	studentská
komora	komora	k1gFnSc1	komora
Rady	rada	k1gFnSc2	rada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
udílí	udílet	k5eAaImIp3nS	udílet
Cenu	cena	k1gFnSc4	cena
Jana	Jan	k1gMnSc2	Jan
Opletala	Opletal	k1gMnSc2	Opletal
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
osobnosti	osobnost	k1gFnSc3	osobnost
nebo	nebo	k8xC	nebo
skupině	skupina	k1gFnSc3	skupina
osobností	osobnost	k1gFnPc2	osobnost
za	za	k7c4	za
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
přínos	přínos	k1gInSc4	přínos
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
akademické	akademický	k2eAgFnSc2d1	akademická
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
hájení	hájení	k1gNnSc1	hájení
akademických	akademický	k2eAgNnPc2d1	akademické
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
<g/>
,	,	kIx,	,
posílení	posílení	k1gNnSc1	posílení
studentských	studentský	k2eAgNnPc2d1	studentské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
<g/>
,	,	kIx,	,
významné	významný	k2eAgNnSc4d1	významné
přispění	přispění	k1gNnSc4	přispění
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
bezpráví	bezpráví	k1gNnSc3	bezpráví
<g/>
,	,	kIx,	,
za	za	k7c4	za
rovné	rovný	k2eAgNnSc4d1	rovné
zacházení	zacházení	k1gNnSc4	zacházení
a	a	k8xC	a
příležitosti	příležitost	k1gFnSc2	příležitost
nebo	nebo	k8xC	nebo
proti	proti	k7c3	proti
jakékoliv	jakýkoliv	k3yIgFnSc3	jakýkoliv
diskriminaci	diskriminace	k1gFnSc3	diskriminace
v	v	k7c6	v
uplynulém	uplynulý	k2eAgNnSc6d1	uplynulé
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
Cena	cena	k1gFnSc1	cena
uděluje	udělovat	k5eAaImIp3nS	udělovat
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Návrhy	návrh	k1gInPc1	návrh
na	na	k7c4	na
udělení	udělení	k1gNnSc4	udělení
Ceny	cena	k1gFnSc2	cena
mohou	moct	k5eAaImIp3nP	moct
předkládat	předkládat	k5eAaImF	předkládat
členové	člen	k1gMnPc1	člen
SK	Sk	kA	Sk
RVŠ	RVŠ	kA	RVŠ
<g/>
,	,	kIx,	,
akademické	akademický	k2eAgInPc1d1	akademický
senáty	senát	k1gInPc1	senát
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
fakult	fakulta	k1gFnPc2	fakulta
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc1	jejich
studentské	studentský	k2eAgFnPc1d1	studentská
části	část	k1gFnPc1	část
a	a	k8xC	a
orgány	orgán	k1gInPc1	orgán
soukromých	soukromý	k2eAgFnPc2d1	soukromá
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
plnící	plnící	k2eAgFnSc4d1	plnící
funkci	funkce	k1gFnSc4	funkce
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
rektor	rektor	k1gMnSc1	rektor
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
nebo	nebo	k8xC	nebo
děkan	děkan	k1gMnSc1	děkan
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
alespoň	alespoň	k9	alespoň
5	[number]	k4	5
studentů	student	k1gMnPc2	student
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
právnické	právnický	k2eAgFnPc1d1	právnická
osoby	osoba	k1gFnPc1	osoba
zřízené	zřízený	k2eAgFnPc1d1	zřízená
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
činnosti	činnost	k1gFnSc2	činnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
podpory	podpora	k1gFnSc2	podpora
rozvoje	rozvoj	k1gInSc2	rozvoj
vysokého	vysoký	k2eAgNnSc2d1	vysoké
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
akademické	akademický	k2eAgFnSc2d1	akademická
samosprávy	samospráva	k1gFnSc2	samospráva
nebo	nebo	k8xC	nebo
k	k	k7c3	k
obhajobě	obhajoba	k1gFnSc3	obhajoba
práv	právo	k1gNnPc2	právo
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
udělení	udělení	k1gNnSc6	udělení
Ceny	cena	k1gFnSc2	cena
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
Výbor	výbor	k1gInSc1	výbor
složený	složený	k2eAgInSc1d1	složený
ze	z	k7c2	z
studentských	studentská	k1gFnPc2	studentská
i	i	k8xC	i
akademických	akademický	k2eAgMnPc2d1	akademický
zástupců	zástupce	k1gMnPc2	zástupce
vylosovaných	vylosovaný	k2eAgFnPc2d1	vylosovaná
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
zvolených	zvolený	k2eAgMnPc2d1	zvolený
delegátu	delegát	k1gMnSc3	delegát
SK	Sk	kA	Sk
RVŠ	RVŠ	kA	RVŠ
<g/>
,	,	kIx,	,
třech	tři	k4xCgFnPc6	tři
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
zastupujících	zastupující	k2eAgFnPc2d1	zastupující
české	český	k2eAgNnSc4d1	české
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
předsedy	předseda	k1gMnSc2	předseda
Výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
předchozího	předchozí	k2eAgMnSc4d1	předchozí
laureáta	laureát	k1gMnSc4	laureát
Ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Laureáti	laureát	k1gMnPc1	laureát
Ceny	cena	k1gFnSc2	cena
Jana	Jan	k1gMnSc2	Jan
Opletala	Opletal	k1gMnSc2	Opletal
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
Fontana	Fontana	k1gFnSc1	Fontana
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgInSc2d1	Karlův
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
-	-	kIx~	-
Kateřina	Kateřina	k1gFnSc1	Kateřina
Mazánková	Mazánková	k1gFnSc1	Mazánková
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
Miklas	Miklas	k1gMnSc1	Miklas
a	a	k8xC	a
rektor	rektor	k1gMnSc1	rektor
Josef	Josef	k1gMnSc1	Josef
Salač	Salač	k1gMnSc1	Salač
<g/>
,	,	kIx,	,
Policejní	policejní	k2eAgFnSc1d1	policejní
akademie	akademie	k1gFnSc1	akademie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
-	-	kIx~	-
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Ručka	ručka	k1gFnSc1	ručka
<g/>
,	,	kIx,	,
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
-	-	kIx~	-
Kateřina	Kateřina	k1gFnSc1	Kateřina
Volná	volný	k2eAgFnSc1d1	volná
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgInSc2d1	Karlův
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
-	-	kIx~	-
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kuba	Kuba	k1gMnSc1	Kuba
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc3d1	Česká
vysoké	vysoká	k1gFnSc3	vysoká
učení	učení	k1gNnSc2	učení
technické	technický	k2eAgInPc4d1	technický
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
-	-	kIx~	-
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Švec	Švec	k1gMnSc1	Švec
<g/>
,	,	kIx,	,
Akademické	akademický	k2eAgNnSc1d1	akademické
centrum	centrum	k1gNnSc1	centrum
studentských	studentský	k2eAgFnPc2d1	studentská
aktivit	aktivita	k1gFnPc2	aktivita
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
David	David	k1gMnSc1	David
Tonzar	Tonzar	k1gMnSc1	Tonzar
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
-	-	kIx~	-
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hirnšal	Hirnšal	k1gMnSc1	Hirnšal
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Laštovička	Laštovička	k1gMnSc1	Laštovička
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Slezák	Slezák	k1gMnSc1	Slezák
<g/>
,	,	kIx,	,
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Rada	rada	k1gFnSc1	rada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
Studentské	studentský	k2eAgFnSc2d1	studentská
komory	komora	k1gFnSc2	komora
Rady	rada	k1gFnSc2	rada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
</s>
</p>
