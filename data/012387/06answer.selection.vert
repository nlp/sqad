<s>
Studentská	studentský	k2eAgFnSc1d1	studentská
komora	komora	k1gFnSc1	komora
Rady	rada	k1gFnSc2	rada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
SK	Sk	kA	Sk
RVŠ	RVŠ	kA	RVŠ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc7d1	národní
reprezentací	reprezentace	k1gFnSc7	reprezentace
studentek	studentka	k1gFnPc2	studentka
a	a	k8xC	a
studentů	student	k1gMnPc2	student
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
součástí	součást	k1gFnPc2	součást
Rady	rada	k1gFnSc2	rada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
