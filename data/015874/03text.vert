<s>
Ī	Ī	k?
<g/>
̌	̌	k?
</s>
<s>
Ī	Ī	k?
<g/>
̌	̌	k?
(	(	kIx(
<g/>
minuskule	minuskule	k1gFnSc1
<g/>
:	:	kIx,
ī	ī	k?
<g/>
̌	̌	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
speciální	speciální	k2eAgInSc4d1
znak	znak	k1gInSc4
latinky	latinka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nazývá	nazývat	k5eAaImIp3nS
se	se	k3xPyFc4
I	i	k9
s	s	k7c7
vodorovnou	vodorovný	k2eAgFnSc7d1
čárkou	čárka	k1gFnSc7
a	a	k8xC
háčkem	háček	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
toto	tento	k3xDgNnSc4
písmeno	písmeno	k1gNnSc4
používá	používat	k5eAaImIp3nS
je	on	k3xPp3gNnSc4
indiánský	indiánský	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
kaska	kaska	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
se	se	k3xPyFc4
do	do	k7c2
skupiny	skupina	k1gFnSc2
athabaských	athabaský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
a	a	k8xC
má	mít	k5eAaImIp3nS
asi	asi	k9
240	#num#	k4
rodilých	rodilý	k2eAgMnPc2d1
mluvčích	mluvčí	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
Severozápadních	severozápadní	k2eAgNnPc6d1
teritoriích	teritorium	k1gNnPc6
<g/>
,	,	kIx,
v	v	k7c6
Yukonu	Yukon	k1gInSc6
a	a	k8xC
v	v	k7c6
Britské	britský	k2eAgFnSc6d1
Kolumbii	Kolumbie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Unicode	Unicod	k1gMnSc5
</s>
<s>
V	v	k7c6
Unicode	Unicod	k1gInSc5
mají	mít	k5eAaImIp3nP
písmena	písmeno	k1gNnPc1
Ī	Ī	k?
<g/>
̌	̌	k?
a	a	k8xC
ī	ī	k?
<g/>
̌	̌	k?
tyto	tento	k3xDgInPc4
kódy	kód	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Ī	Ī	k?
<g/>
̌	̌	k?
<g/>
:	:	kIx,
buď	buď	k8xC
U	u	k7c2
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
12	#num#	k4
<g/>
A	A	kA
a	a	k8xC
U	u	k7c2
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
30	#num#	k4
<g/>
C	C	kA
nebo	nebo	k8xC
U	u	k7c2
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
49	#num#	k4
<g/>
,	,	kIx,
U	u	k7c2
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
304	#num#	k4
a	a	k8xC
U	u	k7c2
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
30	#num#	k4
<g/>
C	C	kA
</s>
<s>
ī	ī	k?
<g/>
̌	̌	k?
<g/>
:	:	kIx,
buď	buď	k8xC
U	u	k7c2
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
12	#num#	k4
<g/>
B	B	kA
a	a	k8xC
U	u	k7c2
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
30	#num#	k4
<g/>
C	C	kA
nebo	nebo	k8xC
U	u	k7c2
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
69	#num#	k4
<g/>
,	,	kIx,
U	u	k7c2
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
304	#num#	k4
a	a	k8xC
U	u	k7c2
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
30	#num#	k4
<g/>
C	C	kA
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Ī	Ī	k?
<g/>
̌	̌	k?
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
GOVERNMENT	GOVERNMENT	kA
OF	OF	kA
CANADA	CANADA	kA
<g/>
,	,	kIx,
Statistics	Statistics	k1gInSc1
Canada	Canada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aboriginal	Aboriginal	k1gFnSc1
Language	language	k1gFnPc2
Spoken	Spoken	k2eAgInSc1d1
at	at	k?
Home	Home	k1gInSc1
(	(	kIx(
<g/>
90	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Single	singl	k1gInSc5
and	and	k?
Multiple	multipl	k1gInSc5
Responses	Responses	k1gMnSc1
of	of	k?
Language	language	k1gFnSc1
Spoken	Spoken	k1gInSc1
at	at	k?
Home	Home	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Aboriginal	Aboriginal	k1gMnSc1
Identity	identita	k1gFnSc2
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Registered	Registered	k1gMnSc1
or	or	k?
Treaty	Treata	k1gFnSc2
Indian	Indiana	k1gFnPc2
Status	status	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
and	and	k?
Age	Age	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
12	#num#	k4
<g/>
)	)	kIx)
for	forum	k1gNnPc2
the	the	k?
Population	Population	k1gInSc1
in	in	k?
Private	Privat	k1gInSc5
Households	Households	k1gInSc1
of	of	k?
Canada	Canada	k1gFnSc1
<g/>
,	,	kIx,
Provinces	Provinces	k1gInSc1
and	and	k?
Territories	Territories	k1gInSc1
<g/>
,	,	kIx,
Census	census	k1gInSc1
Metropolitan	metropolitan	k1gInSc1
Areas	Areas	k1gInSc1
and	and	k?
Census	census	k1gInSc1
Agglomerations	Agglomerationsa	k1gFnPc2
<g/>
,	,	kIx,
2016	#num#	k4
Census	census	k1gInSc1
-	-	kIx~
25	#num#	k4
<g/>
%	%	kIx~
Sample	Sample	k1gFnSc1
Data	datum	k1gNnSc2
<g/>
.	.	kIx.
www	www	k?
<g/>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
statcan	statcana	k1gFnPc2
<g/>
.	.	kIx.
<g/>
gc	gc	k?
<g/>
.	.	kIx.
<g/>
ca	ca	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-03-28	2018-03-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Latinka	latinka	k1gFnSc1
Základní	základní	k2eAgFnSc1d1
písmena	písmeno	k1gNnPc4
dle	dle	k7c2
ISO	ISO	kA
<g/>
*	*	kIx~
</s>
<s>
Aa	Aa	k?
</s>
<s>
Bb	Bb	k?
</s>
<s>
Cc	Cc	k?
</s>
<s>
Dd	Dd	k?
</s>
<s>
Ee	Ee	k?
</s>
<s>
Ff	ff	kA
</s>
<s>
Gg	Gg	k?
</s>
<s>
Hh	Hh	k?
</s>
<s>
Ii	Ii	k?
</s>
<s>
Jj	Jj	k?
</s>
<s>
Kk	Kk	k?
</s>
<s>
Ll	Ll	k?
</s>
<s>
Mm	mm	kA
</s>
<s>
Nn	Nn	k?
</s>
<s>
Oo	Oo	k?
</s>
<s>
Pp	Pp	k?
</s>
<s>
Qq	Qq	k?
</s>
<s>
Rr	Rr	k?
</s>
<s>
Ss	Ss	k?
</s>
<s>
Tt	Tt	k?
</s>
<s>
Uu	Uu	k?
</s>
<s>
Vv	Vv	k?
</s>
<s>
Ww	Ww	k?
</s>
<s>
Xx	Xx	k?
</s>
<s>
Yy	Yy	k?
</s>
<s>
Zz	Zz	k?
Další	další	k2eAgFnSc1d1
písmena	písmeno	k1gNnPc4
</s>
<s>
Ə	Ə	k?
</s>
<s>
Ǝ	Ǝ	k?
</s>
<s>
Ɛ	Ɛ	k?
</s>
<s>
Ɣ	Ɣ	k?
</s>
<s>
Ɩ	Ɩ	k?
</s>
<s>
Ɐ	Ɐ	k?
</s>
<s>
Ꝺ	Ꝺ	k?
</s>
<s>
Þ	Þ	k?
</s>
<s>
Ꞇ	Ꞇ	k?
</s>
<s>
ʇ	ʇ	k?
</s>
<s>
Ŋ	Ŋ	k?
</s>
<s>
Ɔ	Ɔ	k?
</s>
<s>
ſ	ſ	k?
</s>
<s>
Ƨ	Ƨ	k?
</s>
<s>
Ʋ	Ʋ	k?
</s>
<s>
Ʊ	Ʊ	k?
</s>
<s>
Ꝩ	Ꝩ	k?
</s>
<s>
Ꝡ	Ꝡ	k?
</s>
<s>
ẜ	ẜ	k?
</s>
<s>
Ꞅ	Ꞅ	k?
</s>
<s>
Ꝭ	Ꝭ	k?
</s>
<s>
Ʒ	Ʒ	k?
</s>
<s>
Ƹ	Ƹ	k?
</s>
<s>
Ȣ	Ȣ	k?
</s>
<s>
Ʌ	Ʌ	k?
</s>
<s>
Ꞁ	Ꞁ	k?
</s>
<s>
Ỽ	Ỽ	k?
</s>
<s>
Ỿ	Ỿ	k?
</s>
<s>
ʚ	ʚ	k?
</s>
<s>
Ƣ	Ƣ	k?
</s>
<s>
Κ	Κ	k?
</s>
<s>
Ƿ	Ƿ	k?
</s>
<s>
Ⅎ	Ⅎ	k?
</s>
<s>
Ꝼ	Ꝼ	k?
</s>
<s>
Ꝛ	Ꝛ	k?
</s>
<s>
Ʀ	Ʀ	k?
</s>
<s>
Ᵹ	Ᵹ	k?
</s>
<s>
Ꝿ	Ꝿ	k?
</s>
<s>
Ɋ	Ɋ	k?
</s>
<s>
Ь	Ь	k?
</s>
<s>
ʞ	ʞ	k?
</s>
<s>
Ȝ	Ȝ	k?
</s>
<s>
Ꝫ	Ꝫ	k?
</s>
<s>
Ꜭ	Ꜭ	k?
</s>
<s>
ɥ	ɥ	k?
</s>
<s>
Ꝝ	Ꝝ	k?
</s>
<s>
Ƽ	Ƽ	k?
</s>
<s>
Ꝯ	Ꝯ	k?
</s>
<s>
Ɂ	Ɂ	k?
</s>
<s>
Ƕ	Ƕ	k?
</s>
<s>
Ꜧ	Ꜧ	k?
Spřežky	spřežka	k1gFnPc1
a	a	k8xC
ligatury	ligatura	k1gFnPc1
</s>
<s>
Æ	Æ	k?
</s>
<s>
Œ	Œ	k?
</s>
<s>
ẞ	ẞ	k?
</s>
<s>
Ỻ	Ỻ	k?
</s>
<s>
Ǆ	Ǆ	k?
</s>
<s>
Ǳ	Ǳ	k?
</s>
<s>
Ǉ	Ǉ	k?
</s>
<s>
Ǌ	Ǌ	k?
</s>
<s>
ȸ	ȸ	k?
</s>
<s>
ȹ	ȹ	k?
</s>
<s>
Ĳ	Ĳ	k?
</s>
<s>
ch	ch	k0
</s>
<s>
cz	cz	k?
</s>
<s>
gb	gb	k?
</s>
<s>
gh	gh	k?
</s>
<s>
gy	gy	k?
</s>
<s>
ll	ll	k?
</s>
<s>
ly	ly	k?
</s>
<s>
nh	nh	k?
</s>
<s>
ny	ny	k?
</s>
<s>
rr	rr	k?
</s>
<s>
sh	sh	k?
</s>
<s>
sz	sz	k?
</s>
<s>
th	th	k?
Varianty	varianta	k1gFnSc2
písmena	písmeno	k1gNnSc2
I	i	k9
</s>
<s>
Íí	Íí	k?
</s>
<s>
Îî	Îî	k?
</s>
<s>
Ì	Ì	k?
</s>
<s>
Ǐ	Ǐ	k?
</s>
<s>
Ĭ	Ĭ	k?
</s>
<s>
Ȋ	Ȋ	k?
</s>
<s>
Ỉ	Ỉ	k?
</s>
<s>
I	i	k9
<g/>
̋	̋	k?
<g/>
i	i	k8xC
<g/>
̋	̋	k?
</s>
<s>
Ȉ	Ȉ	k?
</s>
<s>
I	i	k9
<g/>
̍	̍	k?
<g/>
i	i	k8xC
<g/>
̍	̍	k?
</s>
<s>
Ī	Ī	k?
</s>
<s>
İ	İ	k6eAd1
</s>
<s>
Iı	Iı	k?
</s>
<s>
I	i	k9
<g/>
̊	̊	k?
<g/>
i	i	k8xC
<g/>
̊	̊	k?
</s>
<s>
I	i	k9
<g/>
̐	̐	k?
<g/>
i	i	k8xC
<g/>
̐	̐	k?
</s>
<s>
Ĩ	Ĩ	k?
</s>
<s>
Ï	Ï	k?
</s>
<s>
I	i	k9
<g/>
̓	̓	k?
<g/>
i	i	k8xC
<g/>
̓	̓	k?
</s>
<s>
Ɨ	Ɨ	k?
</s>
<s>
I	i	k9
<g/>
̭	̭	k?
<g/>
i	i	k8xC
<g/>
̭	̭	k?
</s>
<s>
I	i	k9
<g/>
̧	̧	k?
<g/>
i	i	k8xC
<g/>
̧	̧	k?
</s>
<s>
I	i	k9
<g/>
̱	̱	k?
<g/>
i	i	k8xC
<g/>
̱	̱	k?
</s>
<s>
Į	Į	k?
</s>
<s>
Ị	Ị	k?
</s>
<s>
Ḭ	Ḭ	k?
</s>
<s>
Î	Î	kA
<g/>
́	́	k?
<g/>
î	î	k?
<g/>
́	́	k?
</s>
<s>
Ɨ	Ɨ	k?
<g/>
́	́	k?
<g/>
ɨ	ɨ	k?
<g/>
́	́	k?
</s>
<s>
Ɨ	Ɨ	k?
<g/>
̀	̀	k?
<g/>
ɨ	ɨ	k?
<g/>
̀	̀	k?
</s>
<s>
Ɨ	Ɨ	k?
<g/>
̂	̂	k?
<g/>
ɨ	ɨ	k?
<g/>
̂	̂	k?
</s>
<s>
Ɨ	Ɨ	k?
<g/>
̌	̌	k?
<g/>
ɨ	ɨ	k?
<g/>
̌	̌	k?
</s>
<s>
Ɨ	Ɨ	k?
<g/>
̃	̃	k?
<g/>
ɨ	ɨ	k?
<g/>
̃	̃	k?
</s>
<s>
Ɨ	Ɨ	k?
<g/>
̈	̈	k?
<g/>
ɨ	ɨ	k?
<g/>
̈	̈	k?
</s>
<s>
Ɨ	Ɨ	k?
<g/>
̄	̄	k?
<g/>
ɨ	ɨ	k?
<g/>
̄	̄	k?
</s>
<s>
Ɨ	Ɨ	k?
<g/>
̧	̧	k?
<g/>
ɨ	ɨ	k?
<g/>
̧	̧	k?
</s>
<s>
Ɨ	Ɨ	k?
<g/>
̧	̧	k?
<g/>
̀	̀	k?
<g/>
ɨ	ɨ	k?
<g/>
̧	̧	k?
<g/>
̀	̀	k?
</s>
<s>
Ɨ	Ɨ	k?
<g/>
̧	̧	k?
<g/>
̂	̂	k?
<g/>
ɨ	ɨ	k?
<g/>
̧	̧	k?
<g/>
̂	̂	k?
</s>
<s>
Ɨ	Ɨ	k?
<g/>
̧	̧	k?
<g/>
̌	̌	k?
<g/>
ɨ	ɨ	k?
<g/>
̧	̧	k?
<g/>
̌	̌	k?
</s>
<s>
Ī	Ī	k?
<g/>
́	́	k?
<g/>
ī	ī	k?
<g/>
́	́	k?
</s>
<s>
Ī	Ī	k?
<g/>
̀	̀	k?
<g/>
ī	ī	k?
<g/>
̀	̀	k?
</s>
<s>
Ī	Ī	k?
<g/>
̂	̂	k?
<g/>
ī	ī	k?
<g/>
̂	̂	k?
</s>
<s>
Ī	Ī	k?
<g/>
̌	̌	k?
<g/>
ī	ī	k?
<g/>
̌	̌	k?
</s>
<s>
Ĩ	Ĩ	k?
<g/>
́	́	k?
<g/>
ĩ	ĩ	k?
<g/>
́	́	k?
</s>
<s>
Ĩ	Ĩ	k?
<g/>
̀	̀	k?
<g/>
ĩ	ĩ	k?
<g/>
̀	̀	k?
</s>
<s>
Ĩ	Ĩ	k?
<g/>
̂	̂	k?
<g/>
ĩ	ĩ	k?
<g/>
̂	̂	k?
</s>
<s>
Ĩ	Ĩ	k?
<g/>
̌	̌	k?
<g/>
ĩ	ĩ	k?
<g/>
̌	̌	k?
</s>
<s>
Ĩ	Ĩ	k?
<g/>
̍	̍	k?
<g/>
ĩ	ĩ	k?
<g/>
̍	̍	k?
</s>
<s>
Ḯ	Ḯ	k?
</s>
<s>
Í	Í	kA
<g/>
̧	̧	k?
<g/>
í	í	k0
<g/>
̧	̧	k?
</s>
<s>
Ì	Ì	k?
<g/>
̧	̧	k?
<g/>
ì	ì	k?
<g/>
̧	̧	k?
</s>
<s>
Î	Î	kA
<g/>
̧	̧	k?
<g/>
î	î	k?
<g/>
̧	̧	k?
</s>
<s>
Í	Í	kA
<g/>
̱	̱	k?
<g/>
í	í	k0
<g/>
̱	̱	k?
</s>
<s>
Ì	Ì	k?
<g/>
̱	̱	k?
<g/>
ì	ì	k?
<g/>
̱	̱	k?
</s>
<s>
Î	Î	kA
<g/>
̱	̱	k?
<g/>
î	î	k?
<g/>
̱	̱	k?
</s>
<s>
Ī	Ī	k?
<g/>
̱	̱	k?
<g/>
ī	ī	k?
<g/>
̱	̱	k?
</s>
<s>
Ī	Ī	k?
<g/>
̱	̱	k?
<g/>
́	́	k?
<g/>
ī	ī	k?
<g/>
̱	̱	k?
<g/>
́	́	k?
</s>
<s>
Ī	Ī	k?
<g/>
̱	̱	k?
<g/>
̀	̀	k?
<g/>
ī	ī	k?
<g/>
̱	̱	k?
<g/>
̀	̀	k?
</s>
<s>
Ī	Ī	k?
<g/>
̱	̱	k?
<g/>
̂	̂	k?
<g/>
ī	ī	k?
<g/>
̱	̱	k?
<g/>
̂	̂	k?
</s>
<s>
Į	Į	k?
<g/>
́	́	k?
<g/>
į	į	k?
<g/>
́	́	k?
</s>
<s>
Į	Į	k?
<g/>
̀	̀	k?
<g/>
į	į	k?
<g/>
̀	̀	k?
</s>
<s>
Į	Į	k?
<g/>
̂	̂	k?
<g/>
į	į	k?
<g/>
̂	̂	k?
</s>
<s>
Į	Į	k?
<g/>
̌	̌	k?
<g/>
į	į	k?
<g/>
̌	̌	k?
</s>
<s>
Į	Į	k?
<g/>
̃	̃	k?
<g/>
į	į	k?
<g/>
̃	̃	k?
</s>
<s>
Ị	Ị	k?
<g/>
́	́	k?
<g/>
ị	ị	k?
<g/>
́	́	k?
</s>
<s>
Ị	Ị	k?
<g/>
̂	̂	k?
<g/>
ị	ị	k?
<g/>
̂	̂	k?
</s>
<s>
Ị	Ị	k?
<g/>
̃	̃	k?
<g/>
ị	ị	k?
<g/>
̃	̃	k?
*	*	kIx~
tučně	tučně	k6eAd1
jsou	být	k5eAaImIp3nP
písmena	písmeno	k1gNnPc4
původní	původní	k2eAgFnSc2d1
latinské	latinský	k2eAgFnSc2d1
abecedy	abeceda	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
jazyků	jazyk	k1gInPc2
používajících	používající	k2eAgInPc2d1
latinku	latinka	k1gFnSc4
</s>
