<s desamb="1">
Jediným	jediný	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
toto	tento	k3xDgNnSc4
písmeno	písmeno	k1gNnSc4
používá	používat	k5eAaImIp3nS
je	on	k3xPp3gNnSc4
indiánský	indiánský	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
kaska	kaska	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
se	se	k3xPyFc4
do	do	k7c2
skupiny	skupina	k1gFnSc2
athabaských	athabaský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
a	a	k8xC
má	mít	k5eAaImIp3nS
asi	asi	k9
240	#num#	k4
rodilých	rodilý	k2eAgMnPc2d1
mluvčích	mluvčí	k1gMnPc2
<g/>
.	.	kIx.
</s>