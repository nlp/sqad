<s>
Frodo	Frodo	k1gNnSc1	Frodo
Pytlík	pytlík	k1gInSc1	pytlík
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
fiktivního	fiktivní	k2eAgInSc2d1	fiktivní
světa	svět	k1gInSc2	svět
anglického	anglický	k2eAgMnSc2d1	anglický
spisovatele	spisovatel	k1gMnSc2	spisovatel
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
.	.	kIx.	.
</s>
<s>
Frodo	Frodo	k1gNnSc1	Frodo
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
Tolkienova	Tolkienův	k2eAgInSc2d1	Tolkienův
nejznámějšího	známý	k2eAgInSc2d3	nejznámější
románu	román	k1gInSc2	román
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
třech	tři	k4xCgInPc6	tři
svazcích	svazek	k1gInPc6	svazek
Pána	pán	k1gMnSc4	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
ve	v	k7c6	v
Společenstvu	společenstvo	k1gNnSc6	společenstvo
Prstenu	prsten	k1gInSc2	prsten
<g/>
,	,	kIx,	,
Dvou	dva	k4xCgFnPc2	dva
věžích	věž	k1gFnPc6	věž
i	i	k8xC	i
Návratu	návrat	k1gInSc6	návrat
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
pak	pak	k6eAd1	pak
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
i	i	k9	i
v	v	k7c6	v
Tolkienových	Tolkienův	k2eAgInPc6d1	Tolkienův
Nedokončených	dokončený	k2eNgInPc6d1	nedokončený
příbězích	příběh	k1gInPc6	příběh
či	či	k8xC	či
Silmarillionu	Silmarillion	k1gInSc6	Silmarillion
<g/>
.	.	kIx.	.
</s>
<s>
Frodo	Frodo	k1gNnSc1	Frodo
patřící	patřící	k2eAgNnSc1d1	patřící
k	k	k7c3	k
rase	rasa	k1gFnSc3	rasa
hobitů	hobit	k1gMnPc2	hobit
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2968	[number]	k4	2968
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
bydlel	bydlet	k5eAaImAgMnS	bydlet
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
strýcem	strýc	k1gMnSc7	strýc
Bilbem	Bilb	k1gMnSc7	Bilb
Pytlíkem	pytlík	k1gMnSc7	pytlík
v	v	k7c6	v
Hobitíně	Hobitína	k1gFnSc6	Hobitína
ve	v	k7c6	v
Dně	dna	k1gFnSc6	dna
Pytle	pytel	k1gInSc2	pytel
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesl	nést	k5eAaImAgMnS	nést
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
v	v	k7c6	v
Hoře	hora	k1gFnSc6	hora
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bývá	bývat	k5eAaImIp3nS	bývat
nazýván	nazývat	k5eAaImNgInS	nazývat
Ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nese	nést	k5eAaImIp3nS	nést
Prsten	prsten	k1gInSc4	prsten
nebo	nebo	k8xC	nebo
Devítiprstý	Devítiprstý	k2eAgInSc4d1	Devítiprstý
.	.	kIx.	.
</s>
<s>
Frodo	Frodo	k1gNnSc1	Frodo
Pytlík	pytlík	k1gMnSc1	pytlík
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
Drogu	droga	k1gFnSc4	droga
Pytlíkovi	pytlík	k1gMnSc3	pytlík
a	a	k8xC	a
Primuli	primule	k1gFnSc3	primule
Brandorádové	Brandorádový	k2eAgFnSc2d1	Brandorádový
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2968	[number]	k4	2968
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2980	[number]	k4	2980
Frodo	Frodo	k1gNnSc1	Frodo
o	o	k7c4	o
rodiče	rodič	k1gMnPc4	rodič
přišel	přijít	k5eAaPmAgMnS	přijít
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
utopili	utopit	k5eAaPmAgMnP	utopit
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Brandyvíně	Brandyvína	k1gFnSc6	Brandyvína
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
starost	starost	k1gFnSc4	starost
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc4	on
poté	poté	k6eAd1	poté
vzali	vzít	k5eAaPmAgMnP	vzít
příbuzní	příbuzný	k1gMnPc1	příbuzný
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
roku	rok	k1gInSc2	rok
2989	[number]	k4	2989
přešel	přejít	k5eAaPmAgInS	přejít
Frodo	Frodo	k1gNnSc4	Frodo
do	do	k7c2	do
opatrovnictví	opatrovnictví	k1gNnSc2	opatrovnictví
svého	svůj	k3xOyFgMnSc2	svůj
příbuzného	příbuzný	k1gMnSc2	příbuzný
Bilba	Bilb	k1gMnSc2	Bilb
Pytlíka	pytlík	k1gMnSc2	pytlík
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
adoptoval	adoptovat	k5eAaPmAgMnS	adoptovat
jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc2	svůj
dědice	dědic	k1gMnSc2	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
Frodovi	Frodův	k2eAgMnPc1d1	Frodův
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hobiti	hobit	k1gMnPc1	hobit
docházejí	docházet	k5eAaImIp3nP	docházet
dospělosti	dospělost	k1gFnSc3	dospělost
až	až	k9	až
v	v	k7c6	v
33	[number]	k4	33
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Bilbovi	Bilbův	k2eAgMnPc1d1	Bilbův
111	[number]	k4	111
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přepadl	přepadnout	k5eAaPmAgMnS	přepadnout
ho	on	k3xPp3gInSc4	on
neklid	neklid	k1gInSc4	neklid
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
známeho	známeze	k6eAd1	známeze
jako	jako	k9	jako
Dno	dno	k1gNnSc1	dno
pytle	pytel	k1gInSc2	pytel
<g/>
,	,	kIx,	,
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
čaroděj	čaroděj	k1gMnSc1	čaroděj
Gandalf	Gandalf	k1gMnSc1	Gandalf
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
podezíral	podezírat	k5eAaImAgInS	podezírat
podivný	podivný	k2eAgInSc1d1	podivný
Bilbův	Bilbův	k2eAgInSc1d1	Bilbův
Prsten	prsten	k1gInSc1	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
Bilba	Bilba	k1gMnSc1	Bilba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Prsten	prsten	k1gInSc1	prsten
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
celé	celý	k2eAgNnSc4d1	celé
Dno	dno	k1gNnSc4	dno
pytle	pytel	k1gInSc2	pytel
přenechal	přenechat	k5eAaPmAgMnS	přenechat
Frodovi	Froda	k1gMnSc3	Froda
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Bilbo	Bilba	k1gFnSc5	Bilba
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Roklinky	roklinka	k1gFnSc2	roklinka
za	za	k7c4	za
elfy	elf	k1gMnPc4	elf
<g/>
.	.	kIx.	.
</s>
<s>
Gandalf	Gandalf	k1gMnSc1	Gandalf
nařídil	nařídit	k5eAaPmAgMnS	nařídit
Frodovi	Froda	k1gMnSc3	Froda
Prsten	prsten	k1gInSc4	prsten
uschovat	uschovat	k5eAaPmF	uschovat
a	a	k8xC	a
držet	držet	k5eAaImF	držet
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
tajnosti	tajnost	k1gFnSc6	tajnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
Gandalf	Gandalf	k1gInSc4	Gandalf
Froda	Froda	k1gFnSc1	Froda
čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
devítileté	devítiletý	k2eAgFnSc6d1	devítiletá
prodlevě	prodleva	k1gFnSc6	prodleva
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
posledního	poslední	k2eAgInSc2d1	poslední
příjezdu	příjezd	k1gInSc2	příjezd
do	do	k7c2	do
Kraje	kraj	k1gInSc2	kraj
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
3018	[number]	k4	3018
Gandalf	Gandalf	k1gMnSc1	Gandalf
k	k	k7c3	k
Frodovi	Froda	k1gMnSc3	Froda
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
a	a	k8xC	a
pověděl	povědět	k5eAaPmAgMnS	povědět
mu	on	k3xPp3gMnSc3	on
celý	celý	k2eAgInSc4d1	celý
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
Prstenu	prsten	k1gInSc6	prsten
a	a	k8xC	a
Sauronovi	Sauron	k1gMnSc6	Sauron
<g/>
.	.	kIx.	.
</s>
<s>
Frodo	Frodo	k1gNnSc4	Frodo
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
dál	daleko	k6eAd2	daleko
zůstat	zůstat	k5eAaPmF	zůstat
nemůže	moct	k5eNaImIp3nS	moct
a	a	k8xC	a
Gandalf	Gandalf	k1gMnSc1	Gandalf
mu	on	k3xPp3gMnSc3	on
poradil	poradit	k5eAaPmAgMnS	poradit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
společně	společně	k6eAd1	společně
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
zahradníkem	zahradník	k1gMnSc7	zahradník
Samvědem	Samvěd	k1gMnSc7	Samvěd
Křepelkou	křepelka	k1gFnSc7	křepelka
odešel	odejít	k5eAaPmAgMnS	odejít
hledat	hledat	k5eAaImF	hledat
bezpečí	bezpečí	k1gNnSc4	bezpečí
do	do	k7c2	do
Roklinky	roklinka	k1gFnSc2	roklinka
<g/>
.	.	kIx.	.
</s>
<s>
Gandalf	Gandalf	k1gMnSc1	Gandalf
chtěl	chtít	k5eAaImAgMnS	chtít
původně	původně	k6eAd1	původně
oba	dva	k4xCgMnPc4	dva
hobity	hobit	k1gMnPc4	hobit
doprovodit	doprovodit	k5eAaPmF	doprovodit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
vydal	vydat	k5eAaPmAgInS	vydat
pátrat	pátrat	k5eAaImF	pátrat
po	po	k7c6	po
novinkách	novinka	k1gFnPc6	novinka
a	a	k8xC	a
k	k	k7c3	k
plánovanému	plánovaný	k2eAgInSc3d1	plánovaný
dni	den	k1gInSc3	den
jejich	jejich	k3xOp3gInSc2	jejich
odjezdu	odjezd	k1gInSc2	odjezd
se	se	k3xPyFc4	se
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Frodo	Frodo	k1gNnSc4	Frodo
předstíral	předstírat	k5eAaImAgMnS	předstírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
domova	domov	k1gInSc2	domov
ve	v	k7c6	v
Dně	dna	k1gFnSc6	dna
pytle	pytel	k1gInSc2	pytel
za	za	k7c7	za
svými	svůj	k3xOyFgMnPc7	svůj
příbuznými	příbuzný	k1gMnPc7	příbuzný
do	do	k7c2	do
Rádovska	Rádovsko	k1gNnSc2	Rádovsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
Sama	Sam	k1gMnSc2	Sam
a	a	k8xC	a
svých	svůj	k3xOyFgMnPc2	svůj
příbuzných	příbuzný	k1gMnPc2	příbuzný
Peregrina	Peregrino	k1gNnSc2	Peregrino
Brala	brát	k5eAaImAgFnS	brát
a	a	k8xC	a
Smělmíra	Smělmíra	k1gFnSc1	Smělmíra
Brandoráda	Brandoráda	k1gFnSc1	Brandoráda
<g/>
,	,	kIx,	,
známých	známá	k1gFnPc2	známá
jako	jako	k8xS	jako
Pipin	pipina	k1gFnPc2	pipina
a	a	k8xC	a
Smíšek	smíšek	k1gInSc1	smíšek
<g/>
,	,	kIx,	,
vyrazil	vyrazit	k5eAaPmAgInS	vyrazit
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
než	než	k8xS	než
opustili	opustit	k5eAaPmAgMnP	opustit
Kraj	kraj	k1gInSc4	kraj
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
záhadnými	záhadný	k2eAgMnPc7d1	záhadný
Černými	černý	k2eAgMnPc7d1	černý
jezdci	jezdec	k1gMnPc7	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dobrodružstvích	dobrodružství	k1gNnPc6	dobrodružství
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
Hvozdu	hvozd	k1gInSc6	hvozd
a	a	k8xC	a
Mohylových	mohylový	k2eAgInPc6d1	mohylový
vrších	vrch	k1gInPc6	vrch
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jejich	jejich	k3xOp3gFnSc4	jejich
výpravu	výprava	k1gFnSc4	výprava
dvakrát	dvakrát	k6eAd1	dvakrát
zachraňoval	zachraňovat	k5eAaImAgMnS	zachraňovat
záhadný	záhadný	k2eAgMnSc1d1	záhadný
Tom	Tom	k1gMnSc1	Tom
Bombadil	Bombadil	k1gMnSc1	Bombadil
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
čtveřice	čtveřice	k1gFnSc1	čtveřice
hobitů	hobit	k1gMnPc2	hobit
šťastně	šťastně	k6eAd1	šťastně
dostala	dostat	k5eAaPmAgFnS	dostat
až	až	k9	až
do	do	k7c2	do
Hůrky	hůrka	k1gFnSc2	hůrka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hůrce	hůrka	k1gFnSc6	hůrka
dostal	dostat	k5eAaPmAgMnS	dostat
Frodo	Frodo	k1gNnSc4	Frodo
od	od	k7c2	od
zdejšího	zdejší	k2eAgMnSc2d1	zdejší
hostinského	hostinský	k1gMnSc2	hostinský
Ječmínka	Ječmínek	k1gMnSc2	Ječmínek
Máselníka	máselník	k1gMnSc2	máselník
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
zde	zde	k6eAd1	zde
zanechal	zanechat	k5eAaPmAgMnS	zanechat
Gandalf	Gandalf	k1gMnSc1	Gandalf
<g/>
.	.	kIx.	.
</s>
<s>
Vůdcem	vůdce	k1gMnSc7	vůdce
jejich	jejich	k3xOp3gFnSc2	jejich
výpravy	výprava	k1gFnSc2	výprava
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
čarodějův	čarodějův	k2eAgMnSc1d1	čarodějův
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
hraničář	hraničář	k1gMnSc1	hraničář
Aragorn	Aragorn	k1gMnSc1	Aragorn
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
Chodec	chodec	k1gMnSc1	chodec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Froda	Froda	k1gMnSc1	Froda
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc4	jeho
přátele	přítel	k1gMnPc4	přítel
zachránil	zachránit	k5eAaPmAgMnS	zachránit
při	při	k7c6	při
nočním	noční	k2eAgInSc6d1	noční
útoku	útok	k1gInSc6	útok
Černých	Černých	k2eAgMnPc2d1	Černých
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
další	další	k2eAgFnSc6d1	další
cestě	cesta	k1gFnSc6	cesta
na	na	k7c4	na
východ	východ	k1gInSc4	východ
se	se	k3xPyFc4	se
hobiti	hobit	k1gMnPc1	hobit
vedení	vedení	k1gNnSc2	vedení
hraničářem	hraničář	k1gMnSc7	hraničář
zastavili	zastavit	k5eAaPmAgMnP	zastavit
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
známém	známý	k2eAgInSc6d1	známý
jako	jako	k8xS	jako
Větrov	Větrov	k1gInSc4	Větrov
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
výpravu	výprava	k1gFnSc4	výprava
dohonili	dohonit	k5eAaPmAgMnP	dohonit
Černí	černý	k2eAgMnPc1d1	černý
jezdci	jezdec	k1gMnPc1	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Froda	Froda	k1gMnSc1	Froda
svým	svůj	k3xOyFgNnSc7	svůj
morgulským	morgulský	k2eAgNnSc7d1	morgulský
ostřím	ostří	k1gNnSc7	ostří
těžce	těžce	k6eAd1	těžce
zranil	zranit	k5eAaPmAgMnS	zranit
Pán	pán	k1gMnSc1	pán
nazgû	nazgû	k?	nazgû
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Aragornovi	Aragorn	k1gMnSc3	Aragorn
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
Sauronovi	Sauron	k1gMnSc3	Sauron
služebníky	služebník	k1gMnPc4	služebník
obrátit	obrátit	k5eAaPmF	obrátit
na	na	k7c4	na
ústup	ústup	k1gInSc4	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
výpravě	výprava	k1gFnSc3	výprava
i	i	k9	i
díky	díky	k7c3	díky
pomoci	pomoc	k1gFnSc3	pomoc
elfa	elf	k1gMnSc2	elf
Glorfindela	Glorfindel	k1gMnSc2	Glorfindel
podařilo	podařit	k5eAaPmAgNnS	podařit
Roklinky	roklinka	k1gFnPc4	roklinka
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Frodova	Frodův	k2eAgNnSc2d1	Frodovo
zranění	zranění	k1gNnSc2	zranění
pomohl	pomoct	k5eAaPmAgMnS	pomoct
vyléčit	vyléčit	k5eAaPmF	vyléčit
pán	pán	k1gMnSc1	pán
Roklinky	roklinka	k1gFnSc2	roklinka
Elrond	Elrond	k1gMnSc1	Elrond
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
sešla	sejít	k5eAaPmAgFnS	sejít
tzv.	tzv.	kA	tzv.
Elrondova	Elrondův	k2eAgFnSc1d1	Elrondova
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zástupci	zástupce	k1gMnPc1	zástupce
svobodných	svobodný	k2eAgInPc2d1	svobodný
národů	národ	k1gInPc2	národ
Středozemě	Středozem	k1gFnSc2	Středozem
řešili	řešit	k5eAaImAgMnP	řešit
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
udělat	udělat	k5eAaPmF	udělat
se	s	k7c7	s
Sauronovým	Sauronův	k2eAgInSc7d1	Sauronův
prstenem	prsten	k1gInSc7	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prsten	prsten	k1gInSc1	prsten
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zničen	zničit	k5eAaPmNgInS	zničit
v	v	k7c6	v
Hoře	hora	k1gFnSc6	hora
osudu	osud	k1gInSc2	osud
a	a	k8xC	a
Frodo	Frodo	k1gNnSc1	Frodo
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
jako	jako	k9	jako
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
Prsten	prsten	k1gInSc4	prsten
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
ponese	nést	k5eAaImIp3nS	nést
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gInPc1	člen
Společenstva	společenstvo	k1gNnSc2	společenstvo
prstenu	prsten	k1gInSc6	prsten
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
Froda	Frodo	k1gNnSc2	Frodo
stali	stát	k5eAaPmAgMnP	stát
Gandalf	Gandalf	k1gMnSc1	Gandalf
<g/>
,	,	kIx,	,
Aragorn	Aragorn	k1gMnSc1	Aragorn
<g/>
,	,	kIx,	,
elf	elf	k1gMnSc1	elf
Legolas	Legolas	k1gMnSc1	Legolas
<g/>
,	,	kIx,	,
trpaslík	trpaslík	k1gMnSc1	trpaslík
Gimli	Gimle	k1gFnSc4	Gimle
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
gondorského	gondorský	k2eAgMnSc2d1	gondorský
správce	správce	k1gMnSc2	správce
Denethora	Denethor	k1gMnSc2	Denethor
Boromir	Boromir	k1gMnSc1	Boromir
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
Frodovi	Frodův	k2eAgMnPc1d1	Frodův
přátelé	přítel	k1gMnPc1	přítel
Sam	Sam	k1gMnSc1	Sam
<g/>
,	,	kIx,	,
Smíšek	Smíšek	k1gMnSc1	Smíšek
a	a	k8xC	a
Pipin	pipina	k1gFnPc2	pipina
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
z	z	k7c2	z
Roklinky	roklinka	k1gFnSc2	roklinka
pak	pak	k6eAd1	pak
Bilbo	Bilba	k1gFnSc5	Bilba
daroval	darovat	k5eAaPmAgMnS	darovat
Frodovi	Froda	k1gMnSc3	Froda
svou	svůj	k3xOyFgFnSc4	svůj
mithrilovou	mithrilová	k1gFnSc4	mithrilová
kroužkovou	kroužkový	k2eAgFnSc4d1	kroužková
zbroj	zbroj	k1gFnSc4	zbroj
a	a	k8xC	a
meč	meč	k1gInSc1	meč
Žihadlo	žihadlo	k1gNnSc1	žihadlo
<g/>
.	.	kIx.	.
</s>
<s>
Společenstvo	společenstvo	k1gNnSc1	společenstvo
zamířilo	zamířit	k5eAaPmAgNnS	zamířit
podél	podél	k7c2	podél
Mlžných	mlžný	k2eAgFnPc2d1	mlžná
hor	hora	k1gFnPc2	hora
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
však	však	k9	však
výpravě	výprava	k1gFnSc3	výprava
nepodařilo	podařit	k5eNaPmAgNnS	podařit
překonat	překonat	k5eAaPmF	překonat
hory	hora	k1gFnPc4	hora
průsmykem	průsmyk	k1gInSc7	průsmyk
vedoucím	vedoucí	k1gMnSc7	vedoucí
okolo	okolo	k7c2	okolo
hory	hora	k1gFnSc2	hora
Caradhras	Caradhras	k1gInSc1	Caradhras
<g/>
,	,	kIx,	,
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
Gandalf	Gandalf	k1gMnSc1	Gandalf
provést	provést	k5eAaPmF	provést
své	svůj	k3xOyFgMnPc4	svůj
společníky	společník	k1gMnPc4	společník
opuštěnými	opuštěný	k2eAgInPc7d1	opuštěný
doly	dol	k1gInPc7	dol
Morie	Morie	k1gFnSc2	Morie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
tímto	tento	k3xDgNnSc7	tento
starým	starý	k2eAgNnSc7d1	staré
trpasličím	trpasličí	k2eAgNnSc7d1	trpasličí
městem	město	k1gNnSc7	město
však	však	k9	však
společenstvo	společenstvo	k1gNnSc4	společenstvo
napadli	napadnout	k5eAaPmAgMnP	napadnout
skřeti	skřet	k1gMnPc1	skřet
a	a	k8xC	a
Gandalf	Gandalf	k1gInSc1	Gandalf
padl	padnout	k5eAaPmAgInS	padnout
do	do	k7c2	do
propasti	propast	k1gFnSc2	propast
při	při	k7c6	při
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
balrogem	balrog	k1gInSc7	balrog
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
družiny	družina	k1gFnSc2	družina
se	se	k3xPyFc4	se
ze	z	k7c2	z
ztráty	ztráta	k1gFnSc2	ztráta
čaroděje	čaroděj	k1gMnSc2	čaroděj
vzpamatovával	vzpamatovávat	k5eAaImAgMnS	vzpamatovávat
v	v	k7c6	v
nedalekém	daleký	k2eNgInSc6d1	nedaleký
lese	les	k1gInSc6	les
Lothlórienu	Lothlórien	k1gInSc2	Lothlórien
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Frodo	Frodo	k1gNnSc1	Frodo
od	od	k7c2	od
paní	paní	k1gFnSc2	paní
Galadriel	Galadriel	k1gInSc1	Galadriel
obdržel	obdržet	k5eAaPmAgInS	obdržet
elfí	elfí	k2eAgInSc4d1	elfí
plášť	plášť	k1gInSc4	plášť
a	a	k8xC	a
lahvičku	lahvička	k1gFnSc4	lahvička
se	s	k7c7	s
světlem	světlo	k1gNnSc7	světlo
Eärendilovy	Eärendilův	k2eAgFnSc2d1	Eärendilova
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Lothlórienu	Lothlórien	k1gInSc2	Lothlórien
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
členové	člen	k1gMnPc1	člen
Společenstva	společenstvo	k1gNnSc2	společenstvo
prstenu	prsten	k1gInSc2	prsten
dál	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Anduině	Anduin	k2eAgFnSc6d1	Anduin
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
Rauroského	Rauroský	k2eAgInSc2d1	Rauroský
vodopádu	vodopád	k1gInSc2	vodopád
u	u	k7c2	u
Amon	Amona	k1gFnPc2	Amona
Henu	hena	k1gFnSc4	hena
se	se	k3xPyFc4	se
však	však	k9	však
Boromir	Boromir	k1gMnSc1	Boromir
pokusil	pokusit	k5eAaPmAgMnS	pokusit
Prstenu	prsten	k1gInSc3	prsten
zmocnit	zmocnit	k5eAaPmF	zmocnit
násilím	násilí	k1gNnSc7	násilí
a	a	k8xC	a
Frodo	Frodo	k1gNnSc1	Frodo
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
opustit	opustit	k5eAaPmF	opustit
zbytek	zbytek	k1gInSc4	zbytek
své	svůj	k3xOyFgFnSc2	svůj
družiny	družina	k1gFnSc2	družina
a	a	k8xC	a
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
sám	sám	k3xTgInSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
s	s	k7c7	s
sebou	se	k3xPyFc7	se
vzal	vzít	k5eAaPmAgMnS	vzít
pouze	pouze	k6eAd1	pouze
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
Sama	Sam	k1gMnSc4	Sam
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Společenstva	společenstvo	k1gNnSc2	společenstvo
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
Frodo	Frodo	k1gNnSc4	Frodo
doprovázený	doprovázený	k2eAgInSc1d1	doprovázený
Samem	Samos	k1gInSc7	Samos
do	do	k7c2	do
kopců	kopec	k1gInPc2	kopec
Emyn	Emyno	k1gNnPc2	Emyno
Muil	Muilum	k1gNnPc2	Muilum
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
oba	dva	k4xCgMnPc4	dva
hobity	hobit	k1gMnPc4	hobit
dohnal	dohnat	k5eAaPmAgMnS	dohnat
jejich	jejich	k3xOp3gMnSc1	jejich
pronásledovatel	pronásledovatel	k1gMnSc1	pronásledovatel
<g/>
,	,	kIx,	,
tvor	tvor	k1gMnSc1	tvor
známý	známý	k1gMnSc1	známý
jako	jako	k8xC	jako
Glum	Glum	k1gMnSc1	Glum
<g/>
.	.	kIx.	.
</s>
<s>
Frodovi	Frodův	k2eAgMnPc1d1	Frodův
se	se	k3xPyFc4	se
Samem	Samos	k1gInSc7	Samos
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
Gluma	Gluma	k1gNnSc1	Gluma
přemoci	přemoct	k5eAaPmF	přemoct
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
jej	on	k3xPp3gMnSc4	on
ušetřili	ušetřit	k5eAaPmAgMnP	ušetřit
za	za	k7c4	za
jeho	jeho	k3xOp3gInSc4	jeho
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
ukáže	ukázat	k5eAaPmIp3nS	ukázat
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
trojici	trojice	k1gFnSc3	trojice
nepodařilo	podařit	k5eNaPmAgNnS	podařit
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
Černou	černý	k2eAgFnSc7d1	černá
branou	brána	k1gFnSc7	brána
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
se	s	k7c7	s
Glum	Glu	k1gNnSc7	Glu
<g/>
,	,	kIx,	,
že	že	k8xS	že
provede	provést	k5eAaPmIp3nS	provést
oba	dva	k4xCgMnPc4	dva
hobity	hobit	k1gMnPc4	hobit
do	do	k7c2	do
Sauronovy	Sauronův	k2eAgFnSc2d1	Sauronova
země	zem	k1gFnSc2	zem
tajnou	tajný	k2eAgFnSc7d1	tajná
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Glum	Glum	k1gMnSc1	Glum
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
chtěl	chtít	k5eAaImAgMnS	chtít
zmocnit	zmocnit	k5eAaPmF	zmocnit
Frodova	Frodův	k2eAgInSc2d1	Frodův
prstenu	prsten	k1gInSc2	prsten
<g/>
,	,	kIx,	,
dovedl	dovést	k5eAaPmAgInS	dovést
oba	dva	k4xCgMnPc4	dva
nic	nic	k6eAd1	nic
netušící	tušící	k2eNgMnPc4d1	netušící
hobity	hobit	k1gMnPc4	hobit
až	až	k9	až
do	do	k7c2	do
doupěte	doupě	k1gNnSc2	doupě
pavoučice	pavoučice	k1gFnSc2	pavoučice
Oduly	odout	k5eAaPmAgInP	odout
<g/>
.	.	kIx.	.
</s>
<s>
Odula	odout	k5eAaPmAgFnS	odout
bodla	bodlo	k1gNnSc2	bodlo
Froda	Frodo	k1gNnSc2	Frodo
svým	svůj	k3xOyFgNnSc7	svůj
žihadlem	žihadlo	k1gNnSc7	žihadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Sam	Sam	k1gMnSc1	Sam
ji	on	k3xPp3gFnSc4	on
hrdinně	hrdinně	k6eAd1	hrdinně
zahnal	zahnat	k5eAaPmAgInS	zahnat
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
dojmu	dojem	k1gInSc3	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
bodnutý	bodnutý	k2eAgMnSc1d1	bodnutý
Frodo	Frodo	k1gNnSc4	Frodo
mrtvý	mrtvý	k1gMnSc1	mrtvý
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
připraven	připravit	k5eAaPmNgMnS	připravit
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
osamocen	osamocen	k2eAgInSc1d1	osamocen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
místo	místo	k1gNnSc4	místo
jejich	jejich	k3xOp3gInSc2	jejich
souboje	souboj	k1gInSc2	souboj
s	s	k7c7	s
obří	obří	k2eAgFnSc7d1	obří
pavoučicí	pavoučice	k1gFnSc7	pavoučice
však	však	k9	však
záhy	záhy	k6eAd1	záhy
přišli	přijít	k5eAaPmAgMnP	přijít
skřeti	skřet	k1gMnPc1	skřet
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
odnesli	odnést	k5eAaPmAgMnP	odnést
Frodovo	Frodův	k2eAgNnSc4d1	Frodovo
tělo	tělo	k1gNnSc4	tělo
do	do	k7c2	do
věže	věž	k1gFnSc2	věž
Cirith	Ciritha	k1gFnPc2	Ciritha
Ungol	Ungola	k1gFnPc2	Ungola
<g/>
.	.	kIx.	.
</s>
<s>
Sam	Sam	k1gMnSc1	Sam
odposlechem	odposlech	k1gInSc7	odposlech
jejich	jejich	k3xOp3gInSc2	jejich
rozhovoru	rozhovor	k1gInSc2	rozhovor
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Frodo	Frodo	k1gNnSc4	Frodo
byl	být	k5eAaImAgInS	být
pavoučím	pavoučí	k2eAgInSc7d1	pavoučí
jedem	jed	k1gInSc7	jed
pouze	pouze	k6eAd1	pouze
paralyzován	paralyzovat	k5eAaBmNgMnS	paralyzovat
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
dva	dva	k4xCgMnPc1	dva
skřetí	skřetý	k2eAgMnPc1d1	skřetý
oddíly	oddíl	k1gInPc4	oddíl
zaměstnal	zaměstnat	k5eAaPmAgInS	zaměstnat
spor	spor	k1gInSc1	spor
o	o	k7c4	o
Frodovy	Frodův	k2eAgFnPc4d1	Frodova
cennosti	cennost	k1gFnPc4	cennost
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
Samvědovi	Samvěda	k1gMnSc3	Samvěda
zajatého	zajatý	k2eAgNnSc2d1	zajaté
Froda	Frodo	k1gNnSc2	Frodo
ze	z	k7c2	z
skřetí	skřetí	k1gNnSc2	skřetí
věže	věž	k1gFnSc2	věž
v	v	k7c4	v
Cirith	Cirith	k1gInSc4	Cirith
Ungol	Ungol	k1gInSc4	Ungol
vysvobodit	vysvobodit	k5eAaPmF	vysvobodit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skřetím	skřetit	k5eAaPmIp1nS	skřetit
převleku	převléct	k5eAaPmIp1nSwK	převléct
poté	poté	k6eAd1	poté
Frodo	Frodo	k1gNnSc1	Frodo
a	a	k8xC	a
Sam	Sam	k1gMnSc1	Sam
doputovali	doputovat	k5eAaPmAgMnP	doputovat
až	až	k9	až
k	k	k7c3	k
puklinám	puklina	k1gFnPc3	puklina
Hory	hora	k1gFnSc2	hora
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jedině	jedině	k6eAd1	jedině
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
Sauronův	Sauronův	k2eAgInSc1d1	Sauronův
prsten	prsten	k1gInSc1	prsten
zničen	zničit	k5eAaPmNgInS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
Hory	Hora	k1gMnSc2	Hora
osudu	osud	k1gInSc2	osud
Frodo	Frodo	k1gNnSc4	Frodo
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
vůli	vůle	k1gFnSc4	vůle
Prstenu	prsten	k1gInSc2	prsten
a	a	k8xC	a
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
jej	on	k3xPp3gMnSc4	on
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byl	být	k5eAaImAgMnS	být
napaden	napadnout	k5eAaPmNgMnS	napadnout
Glumem	Glum	k1gInSc7	Glum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
Prsten	prsten	k1gInSc1	prsten
ukousnul	ukousnout	k5eAaPmAgInS	ukousnout
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
společně	společně	k6eAd1	společně
s	s	k7c7	s
prstem	prst	k1gInSc7	prst
<g/>
.	.	kIx.	.
</s>
<s>
Glum	Glum	k6eAd1	Glum
však	však	k9	však
i	i	k9	i
s	s	k7c7	s
Prstenem	prsten	k1gInSc7	prsten
padl	padnout	k5eAaPmAgMnS	padnout
do	do	k7c2	do
lávy	láva	k1gFnSc2	láva
a	a	k8xC	a
Vládnoucí	vládnoucí	k2eAgInSc4d1	vládnoucí
prsten	prsten	k1gInSc4	prsten
společně	společně	k6eAd1	společně
se	s	k7c7	s
Sauronovým	Sauronův	k2eAgMnSc7d1	Sauronův
duchem	duch	k1gMnSc7	duch
tak	tak	k8xC	tak
byl	být	k5eAaImAgInS	být
definitivně	definitivně	k6eAd1	definitivně
zničen	zničit	k5eAaPmNgInS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
v	v	k7c6	v
ohni	oheň	k1gInSc6	oheň
vybuchující	vybuchující	k2eAgFnSc2d1	vybuchující
Hory	hora	k1gFnSc2	hora
osudu	osud	k1gInSc2	osud
pak	pak	k6eAd1	pak
Froda	Frodo	k1gNnSc2	Frodo
a	a	k8xC	a
Sama	Sam	k1gMnSc4	Sam
zachránili	zachránit	k5eAaPmAgMnP	zachránit
velcí	velký	k2eAgMnPc1d1	velký
orli	orel	k1gMnPc1	orel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
je	on	k3xPp3gNnSc4	on
odnesli	odnést	k5eAaPmAgMnP	odnést
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
a	a	k8xC	a
hobiti	hobit	k1gMnPc1	hobit
tak	tak	k6eAd1	tak
znovu	znovu	k6eAd1	znovu
spatřili	spatřit	k5eAaPmAgMnP	spatřit
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oslavách	oslava	k1gFnPc6	oslava
vítězství	vítězství	k1gNnSc2	vítězství
a	a	k8xC	a
korunovaci	korunovace	k1gFnSc4	korunovace
krále	král	k1gMnSc2	král
Elessara	Elessar	k1gMnSc2	Elessar
se	se	k3xPyFc4	se
Frodo	Frodo	k1gNnSc4	Frodo
<g/>
,	,	kIx,	,
Sam	Sam	k1gMnSc1	Sam
<g/>
,	,	kIx,	,
Smíšek	Smíšek	k1gMnSc1	Smíšek
a	a	k8xC	a
Pipin	pipina	k1gFnPc2	pipina
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
domov	domov	k1gInSc1	domov
však	však	k9	však
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
obsadili	obsadit	k5eAaPmAgMnP	obsadit
zlí	zlý	k2eAgMnPc1d1	zlý
lidé	člověk	k1gMnPc1	člověk
sloužící	sloužící	k1gFnSc2	sloužící
čaroději	čaroděj	k1gMnSc3	čaroděj
Sarumanovi	Saruman	k1gMnSc3	Saruman
<g/>
.	.	kIx.	.
</s>
<s>
Frodo	Frodo	k1gNnSc4	Frodo
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
zorganizovali	zorganizovat	k5eAaPmAgMnP	zorganizovat
proti	proti	k7c3	proti
těmto	tento	k3xDgMnPc3	tento
vetřelcům	vetřelec	k1gMnPc3	vetřelec
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
vítězstvím	vítězství	k1gNnSc7	vítězství
hobitů	hobit	k1gMnPc2	hobit
nad	nad	k7c7	nad
Sarumanovými	Sarumanův	k2eAgMnPc7d1	Sarumanův
sluhy	sluha	k1gMnPc7	sluha
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Povodí	povodí	k1gNnSc2	povodí
<g/>
.	.	kIx.	.
</s>
<s>
Čarodějovou	čarodějův	k2eAgFnSc7d1	čarodějova
porážkou	porážka	k1gFnSc7	porážka
definitivně	definitivně	k6eAd1	definitivně
skončila	skončit	k5eAaPmAgFnS	skončit
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
a	a	k8xC	a
Vládnoucí	vládnoucí	k2eAgInSc1d1	vládnoucí
prsten	prsten	k1gInSc1	prsten
byl	být	k5eAaImAgInS	být
zničen	zničen	k2eAgInSc1d1	zničen
<g/>
,	,	kIx,	,
Frodo	Frodo	k1gNnSc1	Frodo
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nevypořádal	vypořádat	k5eNaPmAgMnS	vypořádat
se	s	k7c7	s
vším	všecek	k3xTgNnSc7	všecek
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
zažil	zažít	k5eAaPmAgMnS	zažít
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
zranění	zranění	k1gNnSc4	zranění
morgulským	morgulský	k2eAgNnSc7d1	morgulský
ostřím	ostří	k1gNnSc7	ostří
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
zcela	zcela	k6eAd1	zcela
nezahojilo	zahojit	k5eNaPmAgNnS	zahojit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
zničení	zničení	k1gNnSc2	zničení
Prstenu	prsten	k1gInSc2	prsten
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
3021	[number]	k4	3021
T.	T.	kA	T.
v.	v.	k?	v.
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Bilbem	Bilb	k1gInSc7	Bilb
<g/>
,	,	kIx,	,
Gandalfem	Gandalf	k1gInSc7	Gandalf
<g/>
,	,	kIx,	,
Elrondem	Elrond	k1gInSc7	Elrond
<g/>
,	,	kIx,	,
Galadriel	Galadriel	k1gInSc1	Galadriel
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
elfy	elf	k1gMnPc7	elf
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
Šedých	Šedých	k2eAgInPc6d1	Šedých
přístavech	přístav	k1gInPc6	přístav
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
opustil	opustit	k5eAaPmAgMnS	opustit
Středozem	Středozem	k1gFnSc4	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
odpluli	odplout	k5eAaPmAgMnP	odplout
přes	přes	k7c4	přes
moře	moře	k1gNnSc4	moře
na	na	k7c4	na
západ	západ	k1gInSc4	západ
k	k	k7c3	k
ostrovu	ostrov	k1gInSc3	ostrov
Tol	tol	k1gInSc4	tol
Eressëa	Eressëus	k1gMnSc2	Eressëus
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
skončil	skončit	k5eAaPmAgMnS	skončit
třetí	třetí	k4xOgInSc4	třetí
věk	věk	k1gInSc4	věk
<g/>
.	.	kIx.	.
</s>
<s>
Tolkienova	Tolkienův	k2eAgFnSc1d1	Tolkienova
trilogie	trilogie	k1gFnSc1	trilogie
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
mnoha	mnoho	k4c2	mnoho
filmových	filmový	k2eAgFnPc2d1	filmová
adaptací	adaptace	k1gFnPc2	adaptace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
animovaném	animovaný	k2eAgInSc6d1	animovaný
filmu	film	k1gInSc6	film
Ralpha	Ralph	k1gMnSc2	Ralph
Bakshiho	Bakshi	k1gMnSc2	Bakshi
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
namluvil	namluvit	k5eAaBmAgInS	namluvit
postavu	postava	k1gFnSc4	postava
Froda	Frodo	k1gNnSc2	Frodo
anglický	anglický	k2eAgMnSc1d1	anglický
herec	herec	k1gMnSc1	herec
Christopher	Christophra	k1gFnPc2	Christophra
Guard	Guard	k1gMnSc1	Guard
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
animovaném	animovaný	k2eAgInSc6d1	animovaný
filmu	film	k1gInSc6	film
The	The	k1gMnSc1	The
Return	Return	k1gMnSc1	Return
of	of	k?	of
the	the	k?	the
King	King	k1gMnSc1	King
zapůjčil	zapůjčit	k5eAaPmAgMnS	zapůjčit
této	tento	k3xDgFnSc3	tento
postavě	postava	k1gFnSc3	postava
hlas	hlas	k1gInSc4	hlas
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
Orson	Orson	k1gMnSc1	Orson
Bean	Bean	k1gMnSc1	Bean
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
filmové	filmový	k2eAgFnSc6d1	filmová
parodii	parodie	k1gFnSc6	parodie
Pár	pár	k4xCyI	pár
Pařmenů	Pařmen	k1gInPc2	Pařmen
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
filmu	film	k1gInSc2	film
autory	autor	k1gMnPc4	autor
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c6	na
Fritola	Fritola	k1gFnSc1	Fritola
Šourka	Šourek	k1gMnSc2	Šourek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
trilogii	trilogie	k1gFnSc6	trilogie
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
natočené	natočený	k2eAgFnSc2d1	natočená
režisérem	režisér	k1gMnSc7	režisér
Peterem	Peter	k1gMnSc7	Peter
Jacksonem	Jackson	k1gMnSc7	Jackson
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001	[number]	k4	2001
až	až	k9	až
2003	[number]	k4	2003
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
postavu	postava	k1gFnSc4	postava
Froda	Frodo	k1gNnSc2	Frodo
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
Elijah	Elijah	k1gMnSc1	Elijah
Wood	Wood	k1gMnSc1	Wood
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
jeho	jeho	k3xOp3gInSc2	jeho
postavu	postav	k1gInSc2	postav
nadaboval	nadabovat	k5eAaPmAgMnS	nadabovat
Jan	Jan	k1gMnSc1	Jan
Maxián	Maxián	k1gMnSc1	Maxián
<g/>
.	.	kIx.	.
</s>
