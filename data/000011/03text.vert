<s>
Angličtina	angličtina	k1gFnSc1	angličtina
je	být	k5eAaImIp3nS	být
západogermánský	západogermánský	k2eAgInSc4d1	západogermánský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
na	na	k7c6	na
území	území	k1gNnSc6	území
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třetím	třetí	k4xOgInSc7	třetí
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
anglicky	anglicky	k6eAd1	anglicky
mluví	mluvit	k5eAaImIp3nS	mluvit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
mateřština	mateřština	k1gFnSc1	mateřština
většiny	většina	k1gFnSc2	většina
populace	populace	k1gFnSc2	populace
figuruje	figurovat	k5eAaImIp3nS	figurovat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
<g/>
:	:	kIx,	:
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Irsko	Irsko	k1gNnSc1	Irsko
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
Austrálie	Austrálie	k1gFnSc2	Austrálie
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
Kanada	Kanada	k1gFnSc1	Kanada
a	a	k8xC	a
jiné	jiný	k2eAgNnSc1d1	jiné
Je	být	k5eAaImIp3nS	být
však	však	k9	však
používána	používán	k2eAgFnSc1d1	používána
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
nerozumí	rozumět	k5eNaImIp3nS	rozumět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
používají	používat	k5eAaImIp3nP	používat
ji	on	k3xPp3gFnSc4	on
jako	jako	k9	jako
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
dorozumívací	dorozumívací	k2eAgInSc4d1	dorozumívací
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
rozšíření	rozšíření	k1gNnSc3	rozšíření
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
mnoho	mnoho	k4c1	mnoho
mutací	mutace	k1gFnPc2	mutace
angličtiny	angličtina	k1gFnSc2	angličtina
mícháním	míchání	k1gNnSc7	míchání
s	s	k7c7	s
jazykem	jazyk	k1gInSc7	jazyk
dané	daný	k2eAgFnSc2d1	daná
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Japlish	Japlish	k1gInSc1	Japlish
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
učena	učen	k2eAgFnSc1d1	učena
ve	v	k7c6	v
znacích	znak	k1gInPc6	znak
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
přidávat	přidávat	k5eAaImF	přidávat
do	do	k7c2	do
slov	slovo	k1gNnPc2	slovo
samohlásky	samohláska	k1gFnSc2	samohláska
navíc	navíc	k6eAd1	navíc
(	(	kIx(	(
<g/>
Japlish	Japlish	k1gMnSc1	Japlish
-	-	kIx~	-
Japulish	Japulish	k1gMnSc1	Japulish
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Franglais	Franglais	k1gInSc1	Franglais
pro	pro	k7c4	pro
spojení	spojení	k1gNnSc4	spojení
francouzštiny	francouzština	k1gFnSc2	francouzština
a	a	k8xC	a
francouzského	francouzský	k2eAgNnSc2d1	francouzské
slova	slovo	k1gNnSc2	slovo
anglais	anglais	k1gFnSc1	anglais
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
Spanglish	Spanglish	k1gInSc1	Spanglish
pro	pro	k7c4	pro
latinskoamerické	latinskoamerický	k2eAgMnPc4d1	latinskoamerický
přistěhovalce	přistěhovalec	k1gMnPc4	přistěhovalec
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
Spanish	Spanisha	k1gFnPc2	Spanisha
English	Englisha	k1gFnPc2	Englisha
(	(	kIx(	(
<g/>
španělská	španělský	k2eAgFnSc1d1	španělská
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
Swenglish	Swenglish	k1gMnSc1	Swenglish
(	(	kIx(	(
<g/>
Swedish	Swedish	k1gMnSc1	Swedish
English	English	k1gMnSc1	English
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
švédštinu	švédština	k1gFnSc4	švédština
Singlish	Singlish	k1gInSc4	Singlish
pro	pro	k7c4	pro
singapurskou	singapurský	k2eAgFnSc4d1	Singapurská
angličtinu	angličtina	k1gFnSc4	angličtina
Neobratná	obratný	k2eNgFnSc1d1	neobratná
angličtina	angličtina	k1gFnSc1	angličtina
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zavlékají	zavlékat	k5eAaImIp3nP	zavlékat
českou	český	k2eAgFnSc4d1	Česká
větnou	větný	k2eAgFnSc4d1	větná
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
bohemismy	bohemismus	k1gInPc4	bohemismus
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
Czenglish	Czenglish	k1gInSc1	Czenglish
<g/>
.	.	kIx.	.
</s>
<s>
Anglicky	anglicky	k6eAd1	anglicky
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
hovoří	hovořit	k5eAaImIp3nS	hovořit
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
zemích	zem	k1gFnPc6	zem
<g/>
:	:	kIx,	:
Americká	americký	k2eAgFnSc1d1	americká
Samoa	Samoa	k1gFnSc1	Samoa
<g/>
,	,	kIx,	,
Americké	americký	k2eAgInPc1d1	americký
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Anguilla	Anguilla	k1gFnSc1	Anguilla
<g/>
,	,	kIx,	,
Antigua	Antigua	k1gFnSc1	Antigua
a	a	k8xC	a
Barbuda	Barbuda	k1gFnSc1	Barbuda
<g/>
,	,	kIx,	,
Aruba	Aruba	k1gFnSc1	Aruba
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Bahamy	Bahamy	k1gFnPc1	Bahamy
<g/>
,	,	kIx,	,
Barbados	Barbados	k1gInSc1	Barbados
<g/>
,	,	kIx,	,
Belize	Belize	k1gFnPc1	Belize
<g/>
,	,	kIx,	,
Bermudy	Bermudy	k1gFnPc1	Bermudy
<g/>
,	,	kIx,	,
Botswana	Botswana	k1gFnSc1	Botswana
<g/>
,	,	kIx,	,
Britské	britský	k2eAgNnSc1d1	Britské
indickooceánské	indickooceánský	k2eAgNnSc1d1	indickooceánský
teritorium	teritorium	k1gNnSc1	teritorium
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Britské	britský	k2eAgInPc1d1	britský
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Brunej	Brunej	k1gFnPc1	Brunej
<g/>
,	,	kIx,	,
Cookovy	Cookův	k2eAgInPc1d1	Cookův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Dominika	Dominik	k1gMnSc2	Dominik
<g/>
,	,	kIx,	,
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Eritrea	Eritrea	k1gFnSc1	Eritrea
<g/>
,	,	kIx,	,
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
,	,	kIx,	,
Falklandské	Falklandský	k2eAgInPc1d1	Falklandský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Fidži	Fidž	k1gFnSc6	Fidž
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc1	Filipíny
<g/>
,	,	kIx,	,
Gambie	Gambie	k1gFnSc1	Gambie
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
,	,	kIx,	,
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
<g/>
,	,	kIx,	,
Grenada	Grenada	k1gFnSc1	Grenada
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Guadeloupe	Guadeloupe	k1gFnSc5	Guadeloupe
<g/>
,	,	kIx,	,
Guam	Guam	k1gMnSc1	Guam
<g/>
,	,	kIx,	,
Guyana	Guyana	k1gFnSc1	Guyana
<g/>
,	,	kIx,	,
Honduras	Honduras	k1gInSc1	Honduras
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
Jamajka	Jamajka	k1gFnSc1	Jamajka
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Kajmanské	Kajmanský	k2eAgInPc1d1	Kajmanský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Kambodža	Kambodža	k1gFnSc1	Kambodža
<g/>
,	,	kIx,	,
Kamerun	Kamerun	k1gInSc1	Kamerun
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
,	,	kIx,	,
Kiribati	Kiribati	k1gFnSc1	Kiribati
<g/>
,	,	kIx,	,
Libanon	Libanon	k1gInSc1	Libanon
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Lesotho	Lesot	k1gMnSc4	Lesot
<g/>
,	,	kIx,	,
Libérie	Libérie	k1gFnSc1	Libérie
<g/>
,	,	kIx,	,
Malawi	Malawi	k1gNnSc1	Malawi
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc1	Malajsie
<g/>
,	,	kIx,	,
Malta	Malta	k1gFnSc1	Malta
<g/>
,	,	kIx,	,
Marshallovy	Marshallův	k2eAgInPc1d1	Marshallův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Mauricius	Mauricius	k1gInSc1	Mauricius
<g/>
,	,	kIx,	,
Mikronésie	Mikronésie	k1gFnSc1	Mikronésie
<g/>
,	,	kIx,	,
Montserrat	Montserrat	k1gInSc1	Montserrat
<g/>
,	,	kIx,	,
Namibie	Namibie	k1gFnSc1	Namibie
<g/>
,	,	kIx,	,
Nauru	Naura	k1gFnSc4	Naura
<g/>
,	,	kIx,	,
Nizozemské	nizozemský	k2eAgFnPc1d1	nizozemská
Antily	Antily	k1gFnPc1	Antily
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Nigérie	Nigérie	k1gFnSc1	Nigérie
<g/>
,	,	kIx,	,
Niue	Niue	k1gFnSc1	Niue
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Norfolk	Norfolk	k6eAd1	Norfolk
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc4	Pákistán
<g/>
,	,	kIx,	,
Palau	Palaa	k1gFnSc4	Palaa
<g/>
,	,	kIx,	,
Papua-Nová	Papua-Nový	k2eAgFnSc1d1	Papua-Nová
Guinea	Guinea	k1gFnSc1	Guinea
<g/>
,	,	kIx,	,
Pitcairn	Pitcairn	k1gNnSc1	Pitcairn
<g/>
,	,	kIx,	,
Portoriko	Portoriko	k1gNnSc1	Portoriko
<g/>
,	,	kIx,	,
Rwanda	Rwanda	k1gFnSc1	Rwanda
<g/>
,	,	kIx,	,
Saint-Pierre	Saint-Pierr	k1gInSc5	Saint-Pierr
a	a	k8xC	a
Miquelon	Miquelon	k1gInSc1	Miquelon
<g/>
,	,	kIx,	,
Samoa	Samoa	k1gFnSc1	Samoa
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnPc1d1	severní
Mariany	Mariana	k1gFnPc1	Mariana
<g/>
,	,	kIx,	,
Seychely	Seychely	k1gFnPc1	Seychely
<g/>
,	,	kIx,	,
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
<g/>
,	,	kIx,	,
Singapur	Singapur	k1gInSc4	Singapur
<g/>
,	,	kIx,	,
Somálsko	Somálsko	k1gNnSc4	Somálsko
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgFnSc2d1	americká
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Šrí	Šrí	k1gMnPc7	Šrí
Lanka	lanko	k1gNnSc2	lanko
<g/>
,	,	kIx,	,
Svatá	svatý	k2eAgFnSc1d1	svatá
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
Ascension	Ascension	k1gInSc1	Ascension
a	a	k8xC	a
Tristan	Tristan	k1gInSc1	Tristan
da	da	k?	da
Cunha	Cunha	k1gFnSc1	Cunha
<g/>
,	,	kIx,	,
Svatá	svatý	k2eAgFnSc1d1	svatá
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Kryštof	Kryštof	k1gMnSc1	Kryštof
a	a	k8xC	a
Nevis	viset	k5eNaImRp2nS	viset
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Vincenc	Vincenc	k1gMnSc1	Vincenc
a	a	k8xC	a
Grenadiny	grenadina	k1gFnPc1	grenadina
<g/>
,	,	kIx,	,
Svazijsko	Svazijsko	k1gNnSc1	Svazijsko
<g/>
,	,	kIx,	,
Šalamounovy	Šalamounův	k2eAgInPc1d1	Šalamounův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Tanzanie	Tanzanie	k1gFnPc1	Tanzanie
<g/>
,	,	kIx,	,
Tokelau	Tokelaus	k1gInSc6	Tokelaus
<g/>
,	,	kIx,	,
Tonga	Tonga	k1gFnSc1	Tonga
<g/>
,	,	kIx,	,
Trinidad	Trinidad	k1gInSc1	Trinidad
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Tobago	Tobago	k1gNnSc1	Tobago
<g/>
,	,	kIx,	,
Turks	Turks	k1gInSc1	Turks
a	a	k8xC	a
Caicos	Caicos	k1gInSc1	Caicos
<g/>
,	,	kIx,	,
Uganda	Uganda	k1gFnSc1	Uganda
<g/>
,	,	kIx,	,
Vanuatu	Vanuata	k1gFnSc4	Vanuata
<g/>
,	,	kIx,	,
Zambie	Zambie	k1gFnSc1	Zambie
<g/>
,	,	kIx,	,
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
<g/>
:	:	kIx,	:
primární	primární	k2eAgFnSc1d1	primární
-	-	kIx~	-
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
de	de	k?	de
facto	facto	k1gNnSc4	facto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
(	(	kIx(	(
<g/>
de	de	k?	de
facto	facto	k1gNnSc1	facto
<g/>
,	,	kIx,	,
de	de	k?	de
iure	iure	k1gInSc1	iure
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
některých	některý	k3yIgInPc2	některý
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
(	(	kIx(	(
<g/>
de	de	k?	de
facto	facto	k1gNnSc4	facto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Antigua	Antigua	k1gFnSc1	Antigua
a	a	k8xC	a
Barbuda	Barbuda	k1gFnSc1	Barbuda
<g/>
,	,	kIx,	,
Bahamy	Bahamy	k1gFnPc1	Bahamy
<g/>
,	,	kIx,	,
Barbados	Barbados	k1gInSc1	Barbados
<g/>
,	,	kIx,	,
Bermudy	Bermudy	k1gFnPc1	Bermudy
<g/>
,	,	kIx,	,
Dominika	Dominik	k1gMnSc4	Dominik
<g/>
,	,	kIx,	,
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
<g/>
,	,	kIx,	,
Grenada	Grenada	k1gFnSc1	Grenada
<g/>
,	,	kIx,	,
Guyana	Guyana	k1gFnSc1	Guyana
<g/>
,	,	kIx,	,
Jamajka	Jamajka	k1gFnSc1	Jamajka
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Svatá	svatý	k2eAgFnSc1d1	svatá
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Kryštof	Kryštof	k1gMnSc1	Kryštof
a	a	k8xC	a
Nevis	viset	k5eNaImRp2nS	viset
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Vincenc	Vincenc	k1gMnSc1	Vincenc
a	a	k8xC	a
Grenadiny	grenadina	k1gFnPc1	grenadina
<g/>
,	,	kIx,	,
Trinidad	Trinidad	k1gInSc1	Trinidad
a	a	k8xC	a
Tobago	Tobago	k6eAd1	Tobago
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
6	[number]	k4	6
jazyků	jazyk	k1gInPc2	jazyk
OSN	OSN	kA	OSN
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
-	-	kIx~	-
Belize	Belize	k1gFnSc1	Belize
<g/>
,	,	kIx,	,
Hongkong	Hongkong	k1gInSc1	Hongkong
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Singapur	Singapur	k1gInSc1	Singapur
<g/>
,	,	kIx,	,
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
doplňkový	doplňkový	k2eAgInSc4d1	doplňkový
-	-	kIx~	-
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Fidži	Fidž	k1gFnSc6	Fidž
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc1	Filipíny
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
,	,	kIx,	,
Gambie	Gambie	k1gFnSc1	Gambie
<g/>
,	,	kIx,	,
Kamerun	Kamerun	k1gInSc1	Kamerun
<g/>
,	,	kIx,	,
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
,	,	kIx,	,
Kiribati	Kiribati	k1gFnSc1	Kiribati
<g/>
,	,	kIx,	,
Kostarika	Kostarika	k1gFnSc1	Kostarika
<g/>
,	,	kIx,	,
Lesotho	Lesotha	k1gFnSc5	Lesotha
<g/>
,	,	kIx,	,
Libérie	Libérie	k1gFnSc5	Libérie
<g/>
,	,	kIx,	,
Mikronésie	Mikronésie	k1gFnSc5	Mikronésie
<g/>
,	,	kIx,	,
Namibie	Namibie	k1gFnSc5	Namibie
<g/>
,	,	kIx,	,
Nigérie	Nigérie	k1gFnSc1	Nigérie
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
<g/>
,	,	kIx,	,
Malta	Malta	k1gFnSc1	Malta
<g/>
,	,	kIx,	,
Marshallovy	Marshallův	k2eAgInPc1d1	Marshallův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
,	,	kIx,	,
Panama	Panama	k1gFnSc1	Panama
<g/>
,	,	kIx,	,
Papua-Nová	Papua-Nový	k2eAgFnSc1d1	Papua-Nová
Guinea	Guinea	k1gFnSc1	Guinea
<g/>
,	,	kIx,	,
Samoa	Samoa	k1gFnSc1	Samoa
<g/>
,	,	kIx,	,
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
<g/>
,	,	kIx,	,
Svazijsko	Svazijsko	k1gNnSc1	Svazijsko
<g/>
,	,	kIx,	,
Šalamounovy	Šalamounův	k2eAgInPc1d1	Šalamounův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Tanzanie	Tanzanie	k1gFnPc1	Tanzanie
<g/>
,	,	kIx,	,
Zambie	Zambie	k1gFnPc1	Zambie
<g/>
,	,	kIx,	,
Zimbabwe	Zimbabw	k1gInPc1	Zimbabw
Germánsky	germánsky	k6eAd1	germánsky
mluvící	mluvící	k2eAgInPc1d1	mluvící
kmeny	kmen	k1gInPc1	kmen
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
severozápadního	severozápadní	k2eAgNnSc2d1	severozápadní
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
Sasové	Sas	k1gMnPc1	Sas
<g/>
,	,	kIx,	,
Anglové	Angl	k1gMnPc1	Angl
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jutska	Jutsko	k1gNnPc4	Jutsko
(	(	kIx(	(
<g/>
Jutové	jutový	k2eAgNnSc4d1	jutové
<g/>
)	)	kIx)	)
v	v	k7c6	v
pátém	pátý	k4xOgNnSc6	pátý
století	století	k1gNnSc6	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
vpadly	vpadnout	k5eAaPmAgFnP	vpadnout
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
anglosaština	anglosaština	k1gFnSc1	anglosaština
<g/>
,	,	kIx,	,
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
bud	bouda	k1gFnPc2	bouda
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nově	nově	k6eAd1	nově
příchozí	příchozí	k1gFnSc1	příchozí
původní	původní	k2eAgFnSc1d1	původní
keltské	keltský	k2eAgNnSc4d1	keltské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
vytlačili	vytlačit	k5eAaPmAgMnP	vytlačit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
postupné	postupný	k2eAgFnSc3d1	postupná
asimilaci	asimilace	k1gFnSc3	asimilace
a	a	k8xC	a
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
postupně	postupně	k6eAd1	postupně
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
nové	nový	k2eAgFnSc2d1	nová
panující	panující	k2eAgFnSc2d1	panující
vrstvy	vrstva	k1gFnSc2	vrstva
přijali	přijmout	k5eAaPmAgMnP	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
svým	svůj	k3xOyFgInSc7	svůj
původem	původ	k1gInSc7	původ
anglofríský	anglofríský	k2eAgInSc4d1	anglofríský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
postup	postup	k1gInSc4	postup
jejího	její	k3xOp3gNnSc2	její
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
germánské	germánský	k2eAgInPc1d1	germánský
dialekty	dialekt	k1gInPc1	dialekt
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
sloučily	sloučit	k5eAaPmAgFnP	sloučit
do	do	k7c2	do
jazyka	jazyk	k1gInSc2	jazyk
dnes	dnes	k6eAd1	dnes
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
"	"	kIx"	"
<g/>
staroangličtina	staroangličtina	k1gFnSc1	staroangličtina
<g/>
"	"	kIx"	"
jež	jenž	k3xRgNnSc1	jenž
připomínal	připomínat	k5eAaImAgInS	připomínat
některá	některý	k3yIgNnPc1	některý
dnešní	dnešní	k2eAgNnPc1d1	dnešní
nářečí	nářečí	k1gNnPc1	nářečí
severozápadního	severozápadní	k2eAgNnSc2d1	severozápadní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Fríska	Frísko	k1gNnSc2	Frísko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Psaná	psaný	k2eAgFnSc1d1	psaná
angličtina	angličtina	k1gFnSc1	angličtina
si	se	k3xPyFc3	se
dlouho	dlouho	k6eAd1	dlouho
dále	daleko	k6eAd2	daleko
udržovala	udržovat	k5eAaImAgFnS	udržovat
svoji	svůj	k3xOyFgFnSc4	svůj
syntetickou	syntetický	k2eAgFnSc4d1	syntetická
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
víceméně	víceméně	k9	víceméně
byla	být	k5eAaImAgNnP	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
jedním	jeden	k4xCgInSc7	jeden
literárním	literární	k2eAgInSc7d1	literární
standardem	standard	k1gInSc7	standard
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
ní	on	k3xPp3gFnSc3	on
mluvená	mluvený	k2eAgFnSc1d1	mluvená
staroangličtina	staroangličtina	k1gFnSc1	staroangličtina
tuto	tento	k3xDgFnSc4	tento
strukturu	struktura	k1gFnSc4	struktura
postupem	postupem	k7c2	postupem
doby	doba	k1gFnSc2	doba
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
a	a	k8xC	a
stávala	stávat	k5eAaImAgFnS	stávat
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
analytickou	analytický	k2eAgFnSc7d1	analytická
<g/>
.	.	kIx.	.
</s>
<s>
Přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
komplexnější	komplexní	k2eAgInSc4d2	komplexnější
systém	systém	k1gInSc4	systém
práce	práce	k1gFnSc2	práce
s	s	k7c7	s
podstatnými	podstatný	k2eAgNnPc7d1	podstatné
jmény	jméno	k1gNnPc7	jméno
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
spoléhat	spoléhat	k5eAaImF	spoléhat
na	na	k7c4	na
předložky	předložka	k1gFnPc4	předložka
a	a	k8xC	a
pevné	pevný	k2eAgNnSc4d1	pevné
pořadí	pořadí	k1gNnSc4	pořadí
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
též	též	k9	též
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
vývojem	vývoj	k1gInSc7	vývoj
britonských	britonský	k2eAgInPc2d1	britonský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
i	i	k9	i
příbuznou	příbuzný	k2eAgFnSc7d1	příbuzná
staronorštinou	staronorština	k1gFnSc7	staronorština
patřící	patřící	k2eAgNnSc4d1	patřící
mezi	mezi	k7c4	mezi
severogermánské	severogermánský	k2eAgInPc4d1	severogermánský
jazyky	jazyk	k1gInPc4	jazyk
kterou	který	k3yIgFnSc4	který
používali	používat	k5eAaImAgMnP	používat
Vikingové	Viking	k1gMnPc1	Viking
usídlení	usídlení	k1gNnSc2	usídlení
především	především	k9	především
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
při	při	k7c6	při
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
až	až	k8xS	až
k	k	k7c3	k
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
Londýnu	Londýn	k1gInSc3	Londýn
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
oblasti	oblast	k1gFnSc6	oblast
dnes	dnes	k6eAd1	dnes
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
Danelaw	Danelaw	k1gFnPc1	Danelaw
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1066	[number]	k4	1066
si	se	k3xPyFc3	se
Anglii	Anglie	k1gFnSc4	Anglie
podmaňují	podmaňovat	k5eAaImIp3nP	podmaňovat
Normané	Norman	k1gMnPc1	Norman
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
tři	tři	k4xCgNnPc4	tři
sta	sto	k4xCgNnPc4	sto
let	léto	k1gNnPc2	léto
tak	tak	k6eAd1	tak
normanští	normanský	k2eAgMnPc1d1	normanský
králové	král	k1gMnPc1	král
a	a	k8xC	a
vysoká	vysoký	k2eAgFnSc1d1	vysoká
šlechta	šlechta	k1gFnSc1	šlechta
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
anglonormanštinu	anglonormanština	k1gFnSc4	anglonormanština
blízce	blízce	k6eAd1	blízce
příbuznou	příbuzná	k1gFnSc4	příbuzná
starofrancouzštině	starofrancouzština	k1gFnSc3	starofrancouzština
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
staroangličtiny	staroangličtina	k1gFnSc2	staroangličtina
přechází	přecházet	k5eAaImIp3nS	přecházet
mnoho	mnoho	k4c1	mnoho
normanských	normanský	k2eAgNnPc2d1	normanské
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
slov	slovo	k1gNnPc2	slovo
převzato	převzat	k2eAgNnSc4d1	převzato
i	i	k9	i
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
řečtiny	řečtina	k1gFnSc2	řečtina
a	a	k8xC	a
ve	v	k7c6	v
slovní	slovní	k2eAgFnSc6d1	slovní
zásobě	zásoba	k1gFnSc6	zásoba
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
až	až	k9	až
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Normanský	normanský	k2eAgInSc1d1	normanský
vliv	vliv	k1gInSc1	vliv
silně	silně	k6eAd1	silně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
vývoj	vývoj	k1gInSc4	vývoj
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
staletích	staletí	k1gNnPc6	staletí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyústil	vyústit	k5eAaPmAgInS	vyústit
do	do	k7c2	do
"	"	kIx"	"
<g/>
Střední	střední	k2eAgFnSc2d1	střední
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Middle	Middle	k1gMnSc1	Middle
English	English	k1gMnSc1	English
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
patnáctého	patnáctý	k4xOgNnSc2	patnáctý
století	století	k1gNnSc2	století
prošel	projít	k5eAaPmAgInS	projít
jazyk	jazyk	k1gInSc1	jazyk
velkými	velký	k2eAgFnPc7d1	velká
změnami	změna	k1gFnPc7	změna
výslovnosti	výslovnost	k1gFnSc2	výslovnost
samohlásek	samohláska	k1gFnPc2	samohláska
a	a	k8xC	a
londýnský	londýnský	k2eAgInSc1d1	londýnský
dialekt	dialekt	k1gInSc1	dialekt
angličtiny	angličtina	k1gFnSc2	angličtina
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
v	v	k7c6	v
administrativě	administrativa	k1gFnSc6	administrativa
a	a	k8xC	a
státní	státní	k2eAgFnSc3d1	státní
správě	správa	k1gFnSc3	správa
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
standardizaci	standardizace	k1gFnSc3	standardizace
jazyka	jazyk	k1gInSc2	jazyk
přispěl	přispět	k5eAaPmAgInS	přispět
i	i	k9	i
rozvoj	rozvoj	k1gInSc4	rozvoj
knihtisku	knihtisk	k1gInSc2	knihtisk
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
moderní	moderní	k2eAgFnSc2d1	moderní
angličtiny	angličtina	k1gFnSc2	angličtina
tak	tak	k6eAd1	tak
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaImF	vysledovat
v	v	k7c6	v
období	období	k1gNnSc6	období
života	život	k1gInSc2	život
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1755	[number]	k4	1755
Samuel	Samuel	k1gMnSc1	Samuel
Johnson	Johnson	k1gMnSc1	Johnson
vydává	vydávat	k5eAaImIp3nS	vydávat
první	první	k4xOgInSc4	první
slovník	slovník	k1gInSc4	slovník
angličtiny	angličtina	k1gFnSc2	angličtina
-	-	kIx~	-
Dictionary	Dictionara	k1gFnSc2	Dictionara
of	of	k?	of
the	the	k?	the
English	English	k1gInSc1	English
Language	language	k1gFnSc1	language
-	-	kIx~	-
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
jím	on	k3xPp3gInSc7	on
první	první	k4xOgMnSc1	první
jazykový	jazykový	k2eAgInSc1d1	jazykový
standard	standard	k1gInSc1	standard
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1828	[number]	k4	1828
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
Noah	Noah	k1gMnSc1	Noah
Webster	Webster	k1gMnSc1	Webster
vlastní	vlastní	k2eAgInSc4d1	vlastní
slovník	slovník	k1gInSc4	slovník
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
angličtinu	angličtina	k1gFnSc4	angličtina
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
a	a	k8xC	a
modernizuje	modernizovat	k5eAaBmIp3nS	modernizovat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
mnohé	mnohý	k2eAgInPc1d1	mnohý
jeho	jeho	k3xOp3gInPc1	jeho
novotvary	novotvar	k1gInPc1	novotvar
neuchytily	uchytit	k5eNaPmAgInP	uchytit
a	a	k8xC	a
v	v	k7c6	v
používání	používání	k1gNnSc6	používání
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
konzervativnější	konzervativní	k2eAgNnPc4d2	konzervativnější
Johnsonova	Johnsonův	k2eAgNnPc4d1	Johnsonovo
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
moderní	moderní	k2eAgFnSc4d1	moderní
angličtinu	angličtina	k1gFnSc4	angličtina
je	být	k5eAaImIp3nS	být
příznačné	příznačný	k2eAgNnSc1d1	příznačné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
národní	národní	k2eAgFnPc4d1	národní
varianty	varianta	k1gFnPc4	varianta
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
americká	americký	k2eAgFnSc1d1	americká
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
australská	australský	k2eAgFnSc1d1	australská
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
novozélandská	novozélandský	k2eAgFnSc1d1	novozélandská
angličtina	angličtina	k1gFnSc1	angličtina
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
díky	díky	k7c3	díky
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
obchodu	obchod	k1gInSc3	obchod
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
již	již	k9	již
bývalé	bývalý	k2eAgFnSc3d1	bývalá
koloniální	koloniální	k2eAgFnSc3d1	koloniální
říši	říš	k1gFnSc3	říš
a	a	k8xC	a
internacionalizaci	internacionalizace	k1gFnSc3	internacionalizace
opět	opět	k6eAd1	opět
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
mnoho	mnoho	k4c1	mnoho
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
industrializaci	industrializace	k1gFnSc3	industrializace
a	a	k8xC	a
velkým	velký	k2eAgFnPc3d1	velká
společenským	společenský	k2eAgFnPc3d1	společenská
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
době	doba	k1gFnSc6	doba
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
a	a	k8xC	a
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
dochází	docházet	k5eAaImIp3nS	docházet
především	především	k6eAd1	především
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
rozdílů	rozdíl	k1gInPc2	rozdíl
jazyka	jazyk	k1gInSc2	jazyk
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1	výrazný
vliv	vliv	k1gInSc1	vliv
má	mít	k5eAaImIp3nS	mít
zahájení	zahájení	k1gNnSc4	zahájení
rádiového	rádiový	k2eAgNnSc2d1	rádiové
vysílání	vysílání	k1gNnSc2	vysílání
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
nejenže	nejenže	k6eAd1	nejenže
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
BBC	BBC	kA	BBC
dnešní	dnešní	k2eAgInSc1d1	dnešní
jazykový	jazykový	k2eAgInSc1d1	jazykový
standard	standard	k1gInSc1	standard
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
uvolňování	uvolňování	k1gNnSc1	uvolňování
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
šíření	šíření	k1gNnSc2	šíření
a	a	k8xC	a
míchání	míchání	k1gNnSc2	míchání
jeho	jeho	k3xOp3gInPc2	jeho
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
akcentů	akcent	k1gInPc2	akcent
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Anglický	anglický	k2eAgInSc1d1	anglický
pravopis	pravopis	k1gInSc1	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
latinkou	latinka	k1gFnSc7	latinka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
některých	některý	k3yIgFnPc2	některý
cizích	cizí	k2eAgFnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
se	se	k3xPyFc4	se
obejde	obejít	k5eAaPmIp3nS	obejít
bez	bez	k7c2	bez
diakritických	diakritický	k2eAgNnPc2d1	diakritické
znamének	znaménko	k1gNnPc2	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
dominantnímu	dominantní	k2eAgNnSc3d1	dominantní
postavení	postavení	k1gNnSc3	postavení
angličtiny	angličtina	k1gFnSc2	angličtina
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
a	a	k8xC	a
též	též	k9	též
díky	díky	k7c3	díky
výpočetní	výpočetní	k2eAgFnSc3d1	výpočetní
technice	technika	k1gFnSc3	technika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zpočátku	zpočátku	k6eAd1	zpočátku
podporovala	podporovat	k5eAaImAgFnS	podporovat
především	především	k9	především
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
anglická	anglický	k2eAgFnSc1d1	anglická
abeceda	abeceda	k1gFnSc1	abeceda
stává	stávat	k5eAaImIp3nS	stávat
de	de	k?	de
facto	facto	k1gNnSc1	facto
standardem	standard	k1gInSc7	standard
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
základní	základní	k2eAgFnSc4d1	základní
latinku	latinka	k1gFnSc4	latinka
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
-	-	kIx~	-
přestože	přestože	k8xS	přestože
ne	ne	k9	ne
všechna	všechen	k3xTgNnPc1	všechen
její	její	k3xOp3gFnSc4	její
písmena	písmeno	k1gNnPc1	písmeno
byla	být	k5eAaImAgNnP	být
původně	původně	k6eAd1	původně
obsažena	obsáhnout	k5eAaPmNgNnP	obsáhnout
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
<g/>
.	.	kIx.	.
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
abeceda	abeceda	k1gFnSc1	abeceda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
písmena	písmeno	k1gNnPc4	písmeno
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
:	:	kIx,	:
1	[number]	k4	1
Irské	irský	k2eAgFnSc2d1	irská
<g/>
;	;	kIx,	;
oboje	oboj	k1gFnSc2	oboj
podoby	podoba	k1gFnSc2	podoba
rozšířené	rozšířený	k2eAgFnSc2d1	rozšířená
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
2	[number]	k4	2
Americké	americký	k2eAgFnSc2d1	americká
Angličtina	angličtina	k1gFnSc1	angličtina
používá	používat	k5eAaImIp3nS	používat
historický	historický	k2eAgInSc4d1	historický
pravopis	pravopis	k1gInSc4	pravopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
odráží	odrážet	k5eAaImIp3nS	odrážet
stav	stav	k1gInSc4	stav
jazyka	jazyk	k1gInSc2	jazyk
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
téměř	téměř	k6eAd1	téměř
nereflektuje	reflektovat	k5eNaImIp3nS	reflektovat
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
písemný	písemný	k2eAgInSc1d1	písemný
projev	projev	k1gInSc1	projev
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
rozchází	rozcházet	k5eAaImIp3nS	rozcházet
s	s	k7c7	s
výslovností	výslovnost	k1gFnSc7	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Pravopisná	pravopisný	k2eAgNnPc1d1	pravopisné
pravidla	pravidlo	k1gNnPc1	pravidlo
mají	mít	k5eAaImIp3nP	mít
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc4	množství
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jen	jen	k9	jen
zhruba	zhruba	k6eAd1	zhruba
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
které	který	k3yRgNnSc1	který
slovo	slovo	k1gNnSc1	slovo
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
<g/>
.	.	kIx.	.
</s>
<s>
Problematická	problematický	k2eAgFnSc1d1	problematická
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
výslovnost	výslovnost	k1gFnSc4	výslovnost
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
převzatých	převzatý	k2eAgMnPc2d1	převzatý
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
samohlásek	samohláska	k1gFnPc2	samohláska
závisí	záviset	k5eAaImIp3nS	záviset
především	především	k9	především
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
<g/>
)	)	kIx)	)
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
otevřených	otevřený	k2eAgFnPc6d1	otevřená
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hate	hate	k1gFnSc6	hate
[	[	kIx(	[
<g/>
hɛ	hɛ	k?	hɛ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
nenávidět	návidět	k5eNaImF	návidět
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zavřených	zavřený	k2eAgInPc2d1	zavřený
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hat	hat	k0	hat
[	[	kIx(	[
<g/>
hæ	hæ	k?	hæ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
klobouk	klobouk	k1gInSc1	klobouk
<g/>
)	)	kIx)	)
slabikách	slabika	k1gFnPc6	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fonologie	fonologie	k1gFnSc2	fonologie
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozsáhlému	rozsáhlý	k2eAgNnSc3d1	rozsáhlé
jazykovému	jazykový	k2eAgNnSc3d1	jazykové
území	území	k1gNnSc3	území
a	a	k8xC	a
velkému	velký	k2eAgInSc3d1	velký
počtu	počet	k1gInSc3	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
se	se	k3xPyFc4	se
výslovnost	výslovnost	k1gFnSc1	výslovnost
angličtiny	angličtina	k1gFnSc2	angličtina
regionálně	regionálně	k6eAd1	regionálně
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
všeobecně	všeobecně	k6eAd1	všeobecně
přijímané	přijímaný	k2eAgInPc1d1	přijímaný
standardy	standard	k1gInPc1	standard
anglické	anglický	k2eAgFnSc2d1	anglická
výslovnosti	výslovnost	k1gFnSc2	výslovnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
britský	britský	k2eAgMnSc1d1	britský
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
Received	Received	k1gInSc1	Received
Pronunciation	Pronunciation	k1gInSc1	Pronunciation
(	(	kIx(	(
<g/>
RP	RP	kA	RP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
americký	americký	k2eAgMnSc1d1	americký
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
General	General	k1gMnSc1	General
American	American	k1gMnSc1	American
(	(	kIx(	(
<g/>
GA	GA	kA	GA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
pohyblivý	pohyblivý	k2eAgMnSc1d1	pohyblivý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
germánského	germánský	k2eAgInSc2d1	germánský
původu	původ	k1gInSc2	původ
stojí	stát	k5eAaImIp3nS	stát
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
slov	slovo	k1gNnPc2	slovo
s	s	k7c7	s
nepřízvučnými	přízvučný	k2eNgFnPc7d1	nepřízvučná
předponami	předpona	k1gFnPc7	předpona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
četných	četný	k2eAgNnPc6d1	četné
přejatých	přejatý	k2eAgNnPc6d1	přejaté
slovech	slovo	k1gNnPc6	slovo
se	se	k3xPyFc4	se
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
nacházet	nacházet	k5eAaImF	nacházet
na	na	k7c6	na
kterékoliv	kterýkoliv	k3yIgFnSc6	kterýkoliv
slabice	slabika	k1gFnSc6	slabika
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
význam	význam	k1gInSc4	význam
stejně	stejně	k6eAd1	stejně
psaných	psaný	k2eAgNnPc2d1	psané
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
výrazně	výrazně	k6eAd1	výrazně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
výslovnost	výslovnost	k1gFnSc4	výslovnost
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Nepřízvučné	přízvučný	k2eNgFnPc1d1	nepřízvučná
samohlásky	samohláska	k1gFnPc1	samohláska
jsou	být	k5eAaImIp3nP	být
redukovány	redukován	k2eAgFnPc1d1	redukována
(	(	kIx(	(
<g/>
ɘ	ɘ	k?	ɘ
<g/>
,	,	kIx,	,
ɪ	ɪ	k?	ɪ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Anglická	anglický	k2eAgFnSc1d1	anglická
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
mluvnice	mluvnice	k1gFnSc1	mluvnice
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
podobných	podobný	k2eAgInPc2d1	podobný
principů	princip	k1gInPc2	princip
jako	jako	k8xS	jako
mluvnice	mluvnice	k1gFnSc1	mluvnice
ostatních	ostatní	k2eAgInPc2d1	ostatní
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
flexe	flexe	k1gFnSc1	flexe
dává	dávat	k5eAaImIp3nS	dávat
současné	současný	k2eAgFnSc3d1	současná
angličtině	angličtina	k1gFnSc3	angličtina
převažující	převažující	k2eAgInSc4d1	převažující
charakter	charakter	k1gInSc4	charakter
analytického	analytický	k2eAgInSc2d1	analytický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
rod	rod	k1gInSc4	rod
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
osobní	osobní	k2eAgNnPc1d1	osobní
a	a	k8xC	a
přivlastňovací	přivlastňovací	k2eAgNnPc1d1	přivlastňovací
zájmena	zájmeno	k1gNnPc1	zájmeno
ve	v	k7c4	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
(	(	kIx(	(
<g/>
he	he	k0	he
-	-	kIx~	-
on	on	k3xPp3gMnSc1	on
<g/>
;	;	kIx,	;
she	she	k?	she
-	-	kIx~	-
ona	onen	k3xDgFnSc1	onen
<g/>
;	;	kIx,	;
it	it	k?	it
-	-	kIx~	-
ono	onen	k3xDgNnSc1	onen
<g/>
;	;	kIx,	;
his	his	k1gNnSc1	his
-	-	kIx~	-
jeho	jeho	k3xOp3gFnPc2	jeho
<g/>
;	;	kIx,	;
her	hra	k1gFnPc2	hra
-	-	kIx~	-
její	její	k3xOp3gMnPc1	její
<g/>
;	;	kIx,	;
its	its	k?	its
-	-	kIx~	-
jeho	jeho	k3xOp3gFnSc4	jeho
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
přirozeného	přirozený	k2eAgInSc2d1	přirozený
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc1	žádný
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
mluvnické	mluvnický	k2eAgInPc4d1	mluvnický
tvary	tvar	k1gInPc4	tvar
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
determinována	determinovat	k5eAaBmNgFnS	determinovat
členem	člen	k1gInSc7	člen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
dvojí	dvojí	k4xRgMnSc1	dvojí
<g/>
:	:	kIx,	:
neurčitý	určitý	k2eNgMnSc1d1	neurčitý
<g/>
:	:	kIx,	:
a	a	k8xC	a
<g/>
,	,	kIx,	,
an	an	k?	an
[	[	kIx(	[
<g/>
ɘ	ɘ	k?	ɘ
<g/>
,	,	kIx,	,
ɘ	ɘ	k?	ɘ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc1d1	původní
význam	význam	k1gInSc1	význam
"	"	kIx"	"
<g/>
jeden	jeden	k4xCgInSc4	jeden
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
čísle	číslo	k1gNnSc6	číslo
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
delší	dlouhý	k2eAgInSc1d2	delší
tvar	tvar	k1gInSc1	tvar
an	an	k?	an
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
před	před	k7c7	před
slovem	slovo	k1gNnSc7	slovo
začínajícím	začínající	k2eAgNnSc7d1	začínající
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc2	výslovnost
samohláskou	samohláska	k1gFnSc7	samohláska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
an	an	k?	an
orange	orange	k1gInSc1	orange
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
nějaký	nějaký	k3yIgInSc1	nějaký
<g/>
)	)	kIx)	)
pomeranč	pomeranč	k1gInSc1	pomeranč
<g/>
;	;	kIx,	;
určitý	určitý	k2eAgInSc1d1	určitý
<g/>
:	:	kIx,	:
the	the	k?	the
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc4d1	původní
význam	význam	k1gInSc4	význam
"	"	kIx"	"
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
i	i	k8xC	i
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	se	k3xPyFc4	se
[	[	kIx(	[
<g/>
ð	ð	k?	ð
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
před	před	k7c7	před
samohláskou	samohláska	k1gFnSc7	samohláska
[	[	kIx(	[
<g/>
ð	ð	k?	ð
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Skloňování	skloňování	k1gNnSc1	skloňování
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
zjednodušené	zjednodušený	k2eAgNnSc1d1	zjednodušené
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
systému	systém	k1gInSc2	systém
4	[number]	k4	4
pádů	pád	k1gInPc2	pád
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
nominativ	nominativ	k1gInSc1	nominativ
a	a	k8xC	a
genitiv	genitiv	k1gInSc1	genitiv
(	(	kIx(	(
<g/>
zakončení	zakončení	k1gNnSc1	zakončení
-	-	kIx~	-
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
-s	-s	k?	-s
<g/>
'	'	kIx"	'
<g/>
;	;	kIx,	;
např.	např.	kA	např.
Peter	Peter	k1gMnSc1	Peter
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
car	car	k1gMnSc1	car
<g/>
,	,	kIx,	,
Petrovo	Petrův	k2eAgNnSc1d1	Petrovo
auto	auto	k1gNnSc1	auto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
výlučně	výlučně	k6eAd1	výlučně
přivlastňovací	přivlastňovací	k2eAgFnSc4d1	přivlastňovací
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
nominativu	nominativ	k1gInSc2	nominativ
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
větných	větný	k2eAgInPc2d1	větný
vztahů	vztah	k1gInPc2	vztah
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
předložky	předložka	k1gFnPc1	předložka
a	a	k8xC	a
pevný	pevný	k2eAgInSc1d1	pevný
slovosled	slovosled	k1gInSc1	slovosled
<g/>
.	.	kIx.	.
</s>
<s>
Množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
pomocí	pomocí	k7c2	pomocí
koncovky	koncovka	k1gFnSc2	koncovka
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
-s	-s	k?	-s
<g/>
,	,	kIx,	,
-z	-z	k?	-z
<g/>
,	,	kIx,	,
ɪ	ɪ	k?	ɪ
<g/>
]	]	kIx)	]
dle	dle	k7c2	dle
předcházející	předcházející	k2eAgFnSc2d1	předcházející
hlásky	hláska	k1gFnSc2	hláska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
cars	cars	k1gInSc1	cars
<g/>
,	,	kIx,	,
auta	auto	k1gNnPc1	auto
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgNnSc4d1	Malé
množství	množství	k1gNnSc4	množství
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
tvoří	tvořit	k5eAaImIp3nP	tvořit
množné	množný	k2eAgNnSc4d1	množné
číslo	číslo	k1gNnSc4	číslo
nepravidelně	pravidelně	k6eNd1	pravidelně
<g/>
,	,	kIx,	,
např.	např.	kA	např.
man	man	k1gMnSc1	man
-	-	kIx~	-
men	men	k?	men
<g/>
,	,	kIx,	,
muž	muž	k1gMnSc1	muž
-	-	kIx~	-
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
nesklonná	sklonný	k2eNgNnPc1d1	nesklonné
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
odvozovat	odvozovat	k5eAaImF	odvozovat
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
slovních	slovní	k2eAgInPc2d1	slovní
druhů	druh	k1gInPc2	druh
příponami	přípona	k1gFnPc7	přípona
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hunger	hunger	k1gInSc1	hunger
<g/>
,	,	kIx,	,
hlad	hlad	k1gInSc1	hlad
-	-	kIx~	-
<g/>
>	>	kIx)	>
hungry	hungr	k1gInPc1	hungr
<g/>
,	,	kIx,	,
hladový	hladový	k2eAgMnSc1d1	hladový
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
však	však	k9	však
přídavným	přídavný	k2eAgNnSc7d1	přídavné
jménem	jméno	k1gNnSc7	jméno
stává	stávat	k5eAaImIp3nS	stávat
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
v	v	k7c6	v
přívlastkové	přívlastkový	k2eAgFnSc6d1	přívlastková
pozici	pozice	k1gFnSc6	pozice
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
před	před	k7c7	před
jiným	jiný	k2eAgNnSc7d1	jiné
podstatným	podstatný	k2eAgNnSc7d1	podstatné
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
horse	horse	k1gFnPc1	horse
<g/>
,	,	kIx,	,
kůň	kůň	k1gMnSc1	kůň
-	-	kIx~	-
<g/>
>	>	kIx)	>
horse	horse	k6eAd1	horse
mane	manout	k5eAaImIp3nS	manout
<g/>
,	,	kIx,	,
koňská	koňský	k2eAgFnSc1d1	koňská
hříva	hříva	k1gFnSc1	hříva
<g/>
.	.	kIx.	.
</s>
<s>
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
se	se	k3xPyFc4	se
stupňují	stupňovat	k5eAaImIp3nP	stupňovat
některá	některý	k3yIgNnPc4	některý
analyticky	analyticky	k6eAd1	analyticky
(	(	kIx(	(
<g/>
složenými	složený	k2eAgInPc7d1	složený
tvary	tvar	k1gInPc7	tvar
s	s	k7c7	s
more	mor	k1gInSc5	mor
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiná	jiný	k2eAgFnSc1d1	jiná
synteticky	synteticky	k6eAd1	synteticky
koncovkami	koncovka	k1gFnPc7	koncovka
-er	r	k?	-er
<g/>
,	,	kIx,	,
-est	st	k1gInSc1	-est
<g/>
.	.	kIx.	.
</s>
<s>
Superlativ	superlativ	k1gInSc1	superlativ
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
pojí	pojíst	k5eAaPmIp3nS	pojíst
se	s	k7c7	s
členem	člen	k1gInSc7	člen
určitým	určitý	k2eAgInSc7d1	určitý
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgNnPc1d1	osobní
zájmena	zájmeno	k1gNnPc1	zájmeno
se	se	k3xPyFc4	se
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
tvary	tvar	k1gInPc1	tvar
pro	pro	k7c4	pro
podmět	podmět	k1gInSc4	podmět
(	(	kIx(	(
<g/>
nominativ	nominativ	k1gInSc4	nominativ
<g/>
)	)	kIx)	)
a	a	k8xC	a
předmět	předmět	k1gInSc1	předmět
(	(	kIx(	(
<g/>
dativ	dativ	k1gInSc1	dativ
<g/>
/	/	kIx~	/
<g/>
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
she	she	k?	she
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
-	-	kIx~	-
<g/>
>	>	kIx)	>
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
jí	jíst	k5eAaImIp3nS	jíst
<g/>
/	/	kIx~	/
<g/>
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Přivlastňovací	přivlastňovací	k2eAgNnPc1d1	přivlastňovací
zájmena	zájmeno	k1gNnPc1	zájmeno
mají	mít	k5eAaImIp3nP	mít
odlišné	odlišný	k2eAgInPc1d1	odlišný
tvary	tvar	k1gInPc1	tvar
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nestojí	stát	k5eNaImIp3nS	stát
bezprostředně	bezprostředně	k6eAd1	bezprostředně
před	před	k7c7	před
podstatným	podstatný	k2eAgNnSc7d1	podstatné
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
my	my	k3xPp1nPc1	my
house	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
můj	můj	k3xOp1gInSc1	můj
dům	dům	k1gInSc1	dům
×	×	k?	×
this	this	k6eAd1	this
house	house	k1gNnSc1	house
is	is	k?	is
mine	minout	k5eAaImIp3nS	minout
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
dům	dům	k1gInSc4	dům
je	být	k5eAaImIp3nS	být
můj	můj	k3xOp1gMnSc1	můj
<g/>
.	.	kIx.	.
</s>
<s>
Chybí	chybět	k5eAaImIp3nS	chybět
tvar	tvar	k1gInSc4	tvar
zvratných	zvratný	k2eAgNnPc2d1	zvratné
osobních	osobní	k2eAgNnPc2d1	osobní
a	a	k8xC	a
přivlastňovacích	přivlastňovací	k2eAgNnPc2d1	přivlastňovací
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
germánských	germánský	k2eAgInPc6d1	germánský
jazycích	jazyk	k1gInPc6	jazyk
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
he	he	k0	he
saw	saw	k?	saw
his	his	k1gNnSc1	his
car	car	k1gMnSc1	car
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
"	"	kIx"	"
<g/>
on	on	k3xPp3gMnSc1	on
viděl	vidět	k5eAaImAgMnS	vidět
své	svůj	k3xOyFgNnSc4	svůj
auto	auto	k1gNnSc4	auto
<g/>
"	"	kIx"	"
i	i	k8xC	i
"	"	kIx"	"
<g/>
on	on	k3xPp3gMnSc1	on
viděl	vidět	k5eAaImAgMnS	vidět
jeho	jeho	k3xOp3gNnSc4	jeho
auto	auto	k1gNnSc4	auto
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
existují	existovat	k5eAaImIp3nP	existovat
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
tvary	tvar	k1gInPc1	tvar
osobních	osobní	k2eAgNnPc2d1	osobní
zvratných	zvratný	k2eAgNnPc2d1	zvratné
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
I	i	k8xC	i
can	can	k?	can
see	see	k?	see
myself	myself	k1gInSc1	myself
in	in	k?	in
the	the	k?	the
mirror	mirror	k1gInSc1	mirror
<g/>
,	,	kIx,	,
vidím	vidět	k5eAaImIp1nS	vidět
se	se	k3xPyFc4	se
v	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Anglická	anglický	k2eAgNnPc4d1	anglické
slovesa	sloveso	k1gNnPc4	sloveso
a	a	k8xC	a
Anglická	anglický	k2eAgNnPc4d1	anglické
modální	modální	k2eAgNnPc4d1	modální
slovesa	sloveso	k1gNnPc4	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
sloves	sloveso	k1gNnPc2	sloveso
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
germánských	germánský	k2eAgInPc6d1	germánský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
tvary	tvar	k1gInPc1	tvar
v	v	k7c6	v
přítomném	přítomný	k2eAgInSc6d1	přítomný
a	a	k8xC	a
minulém	minulý	k2eAgInSc6d1	minulý
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
časy	čas	k1gInPc1	čas
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
pomocnými	pomocný	k2eAgNnPc7d1	pomocné
slovesy	sloveso	k1gNnPc7	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Slovesa	sloveso	k1gNnPc1	sloveso
se	se	k3xPyFc4	se
neobejdou	obejít	k5eNaPmIp3nP	obejít
bez	bez	k7c2	bez
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
skoro	skoro	k6eAd1	skoro
všech	všecek	k3xTgFnPc2	všecek
odlišujících	odlišující	k2eAgFnPc2d1	odlišující
osobních	osobní	k2eAgFnPc2d1	osobní
koncovek	koncovka	k1gFnPc2	koncovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přítomném	přítomný	k2eAgInSc6d1	přítomný
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
pouze	pouze	k6eAd1	pouze
koncovka	koncovka	k1gFnSc1	koncovka
-s	-s	k?	-s
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
he	he	k0	he
works	works	k6eAd1	works
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Infinitiv	infinitiv	k1gInSc4	infinitiv
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
zakončení	zakončení	k1gNnSc4	zakončení
nemá	mít	k5eNaImIp3nS	mít
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používán	používat	k5eAaImNgInS	používat
s	s	k7c7	s
částicí	částice	k1gFnSc7	částice
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
např.	např.	kA	např.
I	i	k8xC	i
want	want	k2eAgMnSc1d1	want
to	ten	k3xDgNnSc1	ten
be	be	k?	be
<g/>
,	,	kIx,	,
chci	chtít	k5eAaImIp1nS	chtít
být	být	k5eAaImF	být
<g/>
.	.	kIx.	.
</s>
<s>
Trpný	trpný	k2eAgInSc1d1	trpný
rod	rod	k1gInSc1	rod
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
opisně	opisně	k6eAd1	opisně
pomocí	pomocí	k7c2	pomocí
slovesa	sloveso	k1gNnSc2	sloveso
be	be	k?	be
(	(	kIx(	(
<g/>
být	být	k5eAaImF	být
<g/>
)	)	kIx)	)
a	a	k8xC	a
příčestí	příčestí	k1gNnSc2	příčestí
trpného	trpný	k2eAgNnSc2d1	trpné
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
všechny	všechen	k3xTgInPc4	všechen
germánské	germánský	k2eAgInPc4d1	germánský
jazyky	jazyk	k1gInPc4	jazyk
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
i	i	k9	i
angličtina	angličtina	k1gFnSc1	angličtina
slabá	slabý	k2eAgFnSc1d1	slabá
a	a	k8xC	a
silná	silný	k2eAgNnPc1d1	silné
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgNnPc1d1	silné
slovesa	sloveso	k1gNnPc1	sloveso
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
uváděná	uváděný	k2eAgFnSc1d1	uváděná
jako	jako	k8xS	jako
nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
čase	čas	k1gInSc6	čas
a	a	k8xC	a
příčestí	příčestí	k1gNnSc6	příčestí
trpném	trpný	k2eAgNnSc6d1	trpné
přehlásku	přehláska	k1gFnSc4	přehláska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
sing	sing	k1gMnSc1	sing
-	-	kIx~	-
sang	sang	k1gMnSc1	sang
-	-	kIx~	-
sung	sung	k1gMnSc1	sung
<g/>
,	,	kIx,	,
zpívat	zpívat	k5eAaImF	zpívat
<g/>
.	.	kIx.	.
</s>
<s>
Slabá	Slabá	k1gFnSc1	Slabá
slovesa	sloveso	k1gNnSc2	sloveso
(	(	kIx(	(
<g/>
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
čase	čas	k1gInSc6	čas
a	a	k8xC	a
příčestí	příčestí	k1gNnSc6	příčestí
trpném	trpný	k2eAgNnSc6d1	trpné
koncovku	koncovka	k1gFnSc4	koncovka
-ed	d	k?	-ed
<g/>
,	,	kIx,	,
např.	např.	kA	např.
work	work	k1gMnSc1	work
-	-	kIx~	-
worked	worked	k1gMnSc1	worked
-	-	kIx~	-
worked	worked	k1gMnSc1	worked
<g/>
,	,	kIx,	,
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
angličtiny	angličtina	k1gFnSc2	angličtina
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgInPc3d1	ostatní
germánským	germánský	k2eAgInPc3d1	germánský
jazykům	jazyk	k1gInPc3	jazyk
je	být	k5eAaImIp3nS	být
rozlišení	rozlišení	k1gNnSc1	rozlišení
aspektu	aspekt	k1gInSc2	aspekt
průběhovosti	průběhovost	k1gFnSc2	průběhovost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
I	i	k8xC	i
am	am	k?	am
going	going	k1gInSc1	going
<g/>
,	,	kIx,	,
jdu	jít	k5eAaImIp1nS	jít
(	(	kIx(	(
<g/>
právě	právě	k9	právě
nyní	nyní	k6eAd1	nyní
<g/>
)	)	kIx)	)
×	×	k?	×
I	i	k9	i
go	go	k?	go
<g/>
,	,	kIx,	,
chodím	chodit	k5eAaImIp1nS	chodit
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
má	mít	k5eAaImIp3nS	mít
pevný	pevný	k2eAgInSc4d1	pevný
slovosled	slovosled	k1gInSc4	slovosled
typu	typ	k1gInSc2	typ
SVO	SVO	kA	SVO
(	(	kIx(	(
<g/>
podmět	podmět	k1gInSc1	podmět
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
-	-	kIx~	-
přísudek	přísudek	k1gInSc1	přísudek
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
-	-	kIx~	-
předmět	předmět	k1gInSc1	předmět
(	(	kIx(	(
<g/>
O	O	kA	O
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
neplatí	platit	k5eNaImIp3nS	platit
zásada	zásada	k1gFnSc1	zásada
<g/>
,	,	kIx,	,
že	že	k8xS	že
sloveso	sloveso	k1gNnSc1	sloveso
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
tvaru	tvar	k1gInSc6	tvar
stojí	stát	k5eAaImIp3nS	stát
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
přesunutí	přesunutí	k1gNnSc2	přesunutí
příslovečného	příslovečný	k2eAgNnSc2d1	příslovečné
určení	určení	k1gNnSc2	určení
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
věty	věta	k1gFnSc2	věta
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
uvedené	uvedený	k2eAgNnSc4d1	uvedené
pořadí	pořadí	k1gNnSc4	pořadí
podmětu	podmět	k1gInSc2	podmět
a	a	k8xC	a
přísudku	přísudek	k1gInSc2	přísudek
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
tvoří	tvořit	k5eAaImIp3nP	tvořit
vždy	vždy	k6eAd1	vždy
pomocným	pomocný	k2eAgNnSc7d1	pomocné
slovesem	sloveso	k1gNnSc7	sloveso
<g/>
,	,	kIx,	,
v	v	k7c6	v
přítomném	přítomný	k2eAgInSc6d1	přítomný
a	a	k8xC	a
minulém	minulý	k2eAgInSc6d1	minulý
čase	čas	k1gInSc6	čas
specificky	specificky	k6eAd1	specificky
pomocí	pomocí	k7c2	pomocí
slovesa	sloveso	k1gNnSc2	sloveso
do	do	k7c2	do
(	(	kIx(	(
<g/>
dělat	dělat	k5eAaImF	dělat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
do	do	k7c2	do
you	you	k?	you
know	know	k?	know
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
víš	vědět	k5eAaImIp2nS	vědět
<g/>
?	?	kIx.	?
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
i	i	k9	i
slovesný	slovesný	k2eAgInSc1d1	slovesný
zápor	zápor	k1gInSc1	zápor
<g/>
,	,	kIx,	,
např.	např.	kA	např.
I	i	k9	i
do	do	k7c2	do
not	nota	k1gFnPc2	nota
know	know	k?	know
<g/>
,	,	kIx,	,
nevím	vědět	k5eNaImIp1nS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
větě	věta	k1gFnSc6	věta
zpravidla	zpravidla	k6eAd1	zpravidla
bývá	bývat	k5eAaImIp3nS	bývat
ve	v	k7c6	v
standardní	standardní	k2eAgFnSc6d1	standardní
angličtině	angličtina	k1gFnSc6	angličtina
jediný	jediný	k2eAgInSc1d1	jediný
záporný	záporný	k2eAgInSc1d1	záporný
výraz	výraz	k1gInSc1	výraz
(	(	kIx(	(
<g/>
např.	např.	kA	např.
I	i	k9	i
have	havat	k5eAaPmIp3nS	havat
nothing	nothing	k1gInSc1	nothing
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
ne	ne	k9	ne
<g/>
]	]	kIx)	]
<g/>
mám	mít	k5eAaImIp1nS	mít
nic	nic	k3yNnSc1	nic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zápor	zápor	k1gInSc4	zápor
sloves	sloveso	k1gNnPc2	sloveso
se	s	k7c7	s
tvoří	tvořit	k5eAaImIp3nP	tvořit
částicí	částice	k1gFnSc7	částice
not	nota	k1gFnPc2	nota
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
t.	t.	k?	t.
Přívlastky	přívlastek	k1gInPc1	přívlastek
vyjádřené	vyjádřený	k2eAgInPc1d1	vyjádřený
přídavnými	přídavný	k2eAgNnPc7d1	přídavné
jmény	jméno	k1gNnPc7	jméno
<g/>
,	,	kIx,	,
číslovkami	číslovka	k1gFnPc7	číslovka
<g/>
,	,	kIx,	,
ukazovacími	ukazovací	k2eAgFnPc7d1	ukazovací
a	a	k8xC	a
přivlastňovacími	přivlastňovací	k2eAgNnPc7d1	přivlastňovací
zájmeny	zájmeno	k1gNnPc7	zájmeno
a	a	k8xC	a
genitivem	genitiv	k1gInSc7	genitiv
stojí	stát	k5eAaImIp3nP	stát
před	před	k7c7	před
podstatným	podstatný	k2eAgNnSc7d1	podstatné
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
angličtiny	angličtina	k1gFnSc2	angličtina
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
největší	veliký	k2eAgInSc1d3	veliký
podíl	podíl	k1gInSc1	podíl
slov	slovo	k1gNnPc2	slovo
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
např.	např.	kA	např.
language	language	k1gFnSc1	language
-	-	kIx~	-
langue	langue	k1gFnSc1	langue
(	(	kIx(	(
<g/>
jazyk	jazyk	k1gInSc1	jazyk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
grand	grand	k1gMnSc1	grand
-	-	kIx~	-
grande	grand	k1gMnSc5	grand
(	(	kIx(	(
<g/>
veliký	veliký	k2eAgInSc4d1	veliký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
parrot	parrot	k1gMnSc1	parrot
-	-	kIx~	-
perroquet	perroquet	k1gMnSc1	perroquet
(	(	kIx(	(
<g/>
papoušek	papoušek	k1gMnSc1	papoušek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
germánského	germánský	k2eAgInSc2d1	germánský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
původní	původní	k2eAgFnSc1d1	původní
<g/>
"	"	kIx"	"
anglosaská	anglosaský	k2eAgFnSc1d1	anglosaská
nebo	nebo	k8xC	nebo
odvozená	odvozený	k2eAgFnSc1d1	odvozená
ze	z	k7c2	z
skandinávských	skandinávský	k2eAgInPc2d1	skandinávský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
)	)	kIx)	)
včetně	včetně	k7c2	včetně
mimo	mimo	k7c4	mimo
jiné	jiný	k1gMnPc4	jiný
všech	všecek	k3xTgNnPc2	všecek
zájmen	zájmeno	k1gNnPc2	zájmeno
a	a	k8xC	a
spojek	spojka	k1gFnPc2	spojka
bývají	bývat	k5eAaImIp3nP	bývat
kratší	krátký	k2eAgNnPc1d2	kratší
než	než	k8xS	než
latinská	latinský	k2eAgNnPc1d1	latinské
slova	slovo	k1gNnPc1	slovo
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
běžnější	běžný	k2eAgMnPc1d2	běžnější
v	v	k7c6	v
každodenní	každodenní	k2eAgFnSc6d1	každodenní
řeči	řeč	k1gFnSc6	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Delší	dlouhý	k2eAgNnPc1d2	delší
slova	slovo	k1gNnPc1	slovo
latinská	latinský	k2eAgNnPc1d1	latinské
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
elegantnější	elegantní	k2eAgInPc4d2	elegantnější
nebo	nebo	k8xC	nebo
učenější	učený	k2eAgFnPc4d2	učenější
alternativy	alternativa	k1gFnPc4	alternativa
<g/>
.	.	kIx.	.
</s>
<s>
Nadměrné	nadměrný	k2eAgNnSc1d1	nadměrné
používání	používání	k1gNnSc1	používání
těchto	tento	k3xDgNnPc2	tento
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
však	však	k9	však
obecně	obecně	k6eAd1	obecně
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
volit	volit	k5eAaImF	volit
mezi	mezi	k7c7	mezi
germánskými	germánský	k2eAgNnPc7d1	germánské
a	a	k8xC	a
latinskými	latinský	k2eAgNnPc7d1	latinské
synonymy	synonymum	k1gNnPc7	synonymum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
přidává	přidávat	k5eAaImIp3nS	přidávat
i	i	k9	i
varianta	varianta	k1gFnSc1	varianta
francouzská	francouzský	k2eAgFnSc1d1	francouzská
odvozená	odvozený	k2eAgFnSc1d1	odvozená
od	od	k7c2	od
stejného	stejný	k2eAgInSc2d1	stejný
latinského	latinský	k2eAgInSc2d1	latinský
základu	základ	k1gInSc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
Znalost	znalost	k1gFnSc1	znalost
těchto	tento	k3xDgFnPc2	tento
variant	varianta	k1gFnPc2	varianta
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
mluvčímu	mluvčí	k1gMnSc3	mluvčí
dobře	dobře	k6eAd1	dobře
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
formální	formální	k2eAgFnSc4d1	formální
úroveň	úroveň	k1gFnSc4	úroveň
svého	svůj	k3xOyFgInSc2	svůj
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
<g/>
-li	i	k?	-li
například	například	k6eAd1	například
v	v	k7c6	v
diskusi	diskuse	k1gFnSc6	diskuse
uvést	uvést	k5eAaPmF	uvést
argument	argument	k1gInSc4	argument
velmi	velmi	k6eAd1	velmi
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
zvolí	zvolit	k5eAaPmIp3nS	zvolit
germánské	germánský	k2eAgNnSc4d1	germánské
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
latinská	latinský	k2eAgNnPc1d1	latinské
a	a	k8xC	a
francouzská	francouzský	k2eAgNnPc1d1	francouzské
slova	slovo	k1gNnPc1	slovo
budou	být	k5eAaImBp3nP	být
většinou	většinou	k6eAd1	většinou
užívána	užívat	k5eAaImNgFnS	užívat
v	v	k7c6	v
psaném	psaný	k2eAgInSc6d1	psaný
projevu	projev	k1gInSc6	projev
nebo	nebo	k8xC	nebo
mluvené	mluvený	k2eAgFnSc3d1	mluvená
řeči	řeč	k1gFnSc3	řeč
formální	formální	k2eAgInSc1d1	formální
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Bohatství	bohatství	k1gNnSc1	bohatství
jazyka	jazyk	k1gInSc2	jazyk
pak	pak	k6eAd1	pak
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
různých	různý	k2eAgFnPc2d1	různá
variant	varianta	k1gFnPc2	varianta
rozdílných	rozdílný	k2eAgInPc2d1	rozdílný
významů	význam	k1gInPc2	význam
a	a	k8xC	a
odstínů	odstín	k1gInPc2	odstín
umožňujících	umožňující	k2eAgInPc2d1	umožňující
komplexně	komplexně	k6eAd1	komplexně
a	a	k8xC	a
přesně	přesně	k6eAd1	přesně
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
odstíny	odstín	k1gInPc4	odstín
popisovaných	popisovaný	k2eAgFnPc2d1	popisovaná
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
i	i	k9	i
svou	svůj	k3xOyFgFnSc7	svůj
celkově	celkově	k6eAd1	celkově
velkou	velký	k2eAgFnSc7d1	velká
slovní	slovní	k2eAgFnSc7d1	slovní
zásobou	zásoba	k1gFnSc7	zásoba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
aktivní	aktivní	k2eAgFnSc2d1	aktivní
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
přizpůsobivostí	přizpůsobivost	k1gFnSc7	přizpůsobivost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
běžného	běžný	k2eAgNnSc2d1	běžné
použití	použití	k1gNnSc2	použití
snadno	snadno	k6eAd1	snadno
přijímá	přijímat	k5eAaImIp3nS	přijímat
technické	technický	k2eAgInPc4d1	technický
termíny	termín	k1gInPc4	termín
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
dokonce	dokonce	k9	dokonce
celé	celý	k2eAgFnSc2d1	celá
nové	nový	k2eAgFnSc2d1	nová
fráze	fráze	k1gFnSc2	fráze
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc4d1	Nové
významy	význam	k1gInPc4	význam
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
a	a	k8xC	a
také	také	k9	také
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
starým	starý	k2eAgInPc3d1	starý
slovům	slovo	k1gNnPc3	slovo
slang	slang	k1gInSc4	slang
<g/>
.	.	kIx.	.
</s>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
má	mít	k5eAaImIp3nS	mít
bohatou	bohatý	k2eAgFnSc4d1	bohatá
slovní	slovní	k2eAgFnSc4d1	slovní
zásobu	zásoba	k1gFnSc4	zásoba
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
přijímat	přijímat	k5eAaImF	přijímat
nová	nový	k2eAgNnPc4d1	nové
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
však	však	k9	však
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
velkých	velký	k2eAgInPc2d1	velký
jazyků	jazyk	k1gInPc2	jazyk
neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc1	žádný
regulační	regulační	k2eAgInSc1d1	regulační
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
by	by	kYmCp3nP	by
nová	nový	k2eAgNnPc1d1	nové
slova	slovo	k1gNnPc1	slovo
"	"	kIx"	"
<g/>
přijímal	přijímat	k5eAaImAgInS	přijímat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
problematické	problematický	k2eAgNnSc1d1	problematické
určit	určit	k5eAaPmF	určit
jejich	jejich	k3xOp3gInSc4	jejich
počet	počet	k1gInSc4	počet
<g/>
.	.	kIx.	.
</s>
<s>
Novotvary	novotvar	k1gInPc1	novotvar
vznikají	vznikat	k5eAaImIp3nP	vznikat
především	především	k9	především
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
,	,	kIx,	,
vědě	věda	k1gFnSc6	věda
a	a	k8xC	a
technologiích	technologie	k1gFnPc6	technologie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
slangy	slang	k1gInPc1	slang
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
slova	slovo	k1gNnPc1	slovo
přinesená	přinesený	k2eAgNnPc1d1	přinesené
komunitami	komunita	k1gFnPc7	komunita
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
editace	editace	k1gFnSc1	editace
Oxford	Oxford	k1gInSc4	Oxford
English	English	k1gInSc1	English
Dictionary	Dictionara	k1gFnSc2	Dictionara
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
600	[number]	k4	600
000	[number]	k4	000
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
editoři	editor	k1gMnPc1	editor
Merriam	Merriam	k1gInSc4	Merriam
Webster	Webster	k1gInSc1	Webster
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Third	Third	k1gInSc1	Third
New	New	k1gMnSc4	New
International	International	k1gFnSc2	International
Dictionary	Dictionara	k1gFnSc2	Dictionara
<g/>
,	,	kIx,	,
Unabridged	Unabridged	k1gInSc4	Unabridged
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Global	globat	k5eAaImAgInS	globat
Language	language	k1gFnSc4	language
Monitor	monitor	k1gInSc1	monitor
po	po	k7c6	po
zkombinování	zkombinování	k1gNnSc6	zkombinování
oxfordského	oxfordský	k2eAgInSc2d1	oxfordský
slovníku	slovník	k1gInSc2	slovník
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
slovníkových	slovníkový	k2eAgInPc2d1	slovníkový
zdrojů	zdroj	k1gInPc2	zdroj
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
angličtina	angličtina	k1gFnSc1	angličtina
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přibližně	přibližně	k6eAd1	přibližně
990	[number]	k4	990
000	[number]	k4	000
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důsledků	důsledek	k1gInPc2	důsledek
normanskofrancouzského	normanskofrancouzský	k2eAgInSc2d1	normanskofrancouzský
vlivu	vliv	k1gInSc2	vliv
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
anglická	anglický	k2eAgFnSc1d1	anglická
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
slova	slovo	k1gNnPc4	slovo
germánská	germánský	k2eAgFnSc1d1	germánská
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
staroanglická	staroanglický	k2eAgFnSc1d1	staroanglická
<g/>
)	)	kIx)	)
a	a	k8xC	a
slova	slovo	k1gNnPc1	slovo
latinská	latinský	k2eAgNnPc1d1	latinské
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
starofrancouzská	starofrancouzský	k2eAgFnSc1d1	starofrancouzská
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
anglonormanská	anglonormanský	k2eAgFnSc1d1	anglonormanský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
již	již	k9	již
mnoho	mnoho	k4c1	mnoho
statistických	statistický	k2eAgInPc2d1	statistický
průzkumů	průzkum	k1gInPc2	průzkum
zachycující	zachycující	k2eAgInPc4d1	zachycující
vzájemné	vzájemný	k2eAgInPc4d1	vzájemný
poměry	poměr	k1gInPc4	poměr
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
nebyl	být	k5eNaImAgMnS	být
všemi	všecek	k3xTgMnPc7	všecek
lingvisty	lingvista	k1gMnPc7	lingvista
zcela	zcela	k6eAd1	zcela
přijat	přijat	k2eAgInSc1d1	přijat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Počítačový	počítačový	k2eAgInSc1d1	počítačový
průzkum	průzkum	k1gInSc1	průzkum
80	[number]	k4	80
000	[number]	k4	000
slov	slovo	k1gNnPc2	slovo
ze	z	k7c2	z
staršího	starý	k2eAgMnSc2d2	starší
Shorter	Shorter	k1gInSc1	Shorter
Oxford	Oxford	k1gInSc4	Oxford
Dictionary	Dictionara	k1gFnSc2	Dictionara
uveřejněný	uveřejněný	k2eAgInSc4d1	uveřejněný
v	v	k7c4	v
Ordered	Ordered	k1gInSc4	Ordered
Profusion	Profusion	k1gInSc4	Profusion
Thomasem	Thomas	k1gMnSc7	Thomas
Finkenstaedtem	Finkenstaedt	k1gMnSc7	Finkenstaedt
a	a	k8xC	a
Dieterem	Dieter	k1gMnSc7	Dieter
Wolffem	Wolff	k1gMnSc7	Wolff
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
například	například	k6eAd1	například
uvádí	uvádět	k5eAaImIp3nS	uvádět
původ	původ	k1gInSc4	původ
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
langues	languesa	k1gFnPc2	languesa
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
oï	oï	k?	oï
včetně	včetně	k7c2	včetně
francouzštiny	francouzština	k1gFnSc2	francouzština
a	a	k8xC	a
staronormanštiny	staronormanština	k1gFnSc2	staronormanština
28,3	[number]	k4	28,3
%	%	kIx~	%
<g/>
,	,	kIx,	,
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
včetně	včetně	k7c2	včetně
latiny	latina	k1gFnSc2	latina
moderní	moderní	k2eAgFnSc2d1	moderní
vědecké	vědecký	k2eAgFnPc4d1	vědecká
a	a	k8xC	a
technické	technický	k2eAgFnPc4d1	technická
28,24	[number]	k4	28,24
%	%	kIx~	%
<g/>
,	,	kIx,	,
z	z	k7c2	z
ostatních	ostatní	k2eAgInPc2d1	ostatní
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
slov	slovo	k1gNnPc2	slovo
přímo	přímo	k6eAd1	přímo
převzatých	převzatý	k2eAgMnPc2d1	převzatý
ze	z	k7c2	z
staroangličtiny	staroangličtina	k1gFnSc2	staroangličtina
<g/>
)	)	kIx)	)
25	[number]	k4	25
%	%	kIx~	%
<g/>
,	,	kIx,	,
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
5,32	[number]	k4	5,32
%	%	kIx~	%
<g/>
,	,	kIx,	,
z	z	k7c2	z
ostatních	ostatní	k2eAgInPc2d1	ostatní
jazyků	jazyk	k1gInPc2	jazyk
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jiný	jiný	k2eAgInSc1d1	jiný
průzkum	průzkum	k1gInSc1	průzkum
Josepha	Joseph	k1gMnSc2	Joseph
M.	M.	kA	M.
Williamse	Williamse	k1gFnSc2	Williamse
v	v	k7c4	v
Origins	Origins	k1gInSc4	Origins
of	of	k?	of
the	the	k?	the
English	English	k1gMnSc1	English
Language	language	k1gFnSc3	language
mezi	mezi	k7c7	mezi
deseti	deset	k4xCc7	deset
tisíci	tisíc	k4xCgInPc7	tisíc
slov	slovo	k1gNnPc2	slovo
převzatých	převzatý	k2eAgNnPc2d1	převzaté
z	z	k7c2	z
několika	několik	k4yIc2	několik
tisíců	tisíc	k4xCgInPc2	tisíc
obchodních	obchodní	k2eAgMnPc2d1	obchodní
dopisů	dopis	k1gInPc2	dopis
nalézá	nalézat	k5eAaImIp3nS	nalézat
francouzštinu	francouzština	k1gFnSc4	francouzština
(	(	kIx(	(
<g/>
langue	langue	k1gFnSc4	langue
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
oï	oï	k?	oï
<g/>
)	)	kIx)	)
41	[number]	k4	41
%	%	kIx~	%
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
původní	původní	k2eAgFnSc4d1	původní
<g/>
"	"	kIx"	"
angličtinu	angličtina	k1gFnSc4	angličtina
33	[number]	k4	33
%	%	kIx~	%
<g/>
,	,	kIx,	,
latinu	latina	k1gFnSc4	latina
15	[number]	k4	15
%	%	kIx~	%
<g/>
,	,	kIx,	,
dánštinu	dánština	k1gFnSc4	dánština
2	[number]	k4	2
%	%	kIx~	%
<g/>
,	,	kIx,	,
nizozemštinu	nizozemština	k1gFnSc4	nizozemština
1	[number]	k4	1
%	%	kIx~	%
a	a	k8xC	a
ostatní	ostatní	k2eAgMnSc1d1	ostatní
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jisté	jistý	k2eAgNnSc1d1	jisté
však	však	k9	však
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
83	[number]	k4	83
%	%	kIx~	%
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
tisíce	tisíc	k4xCgInSc2	tisíc
nejčastějších	častý	k2eAgNnPc2d3	nejčastější
anglických	anglický	k2eAgNnPc2d1	anglické
slov	slovo	k1gNnPc2	slovo
má	mít	k5eAaImIp3nS	mít
anglosaský	anglosaský	k2eAgInSc4d1	anglosaský
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
