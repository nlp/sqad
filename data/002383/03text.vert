<s>
Vix	Vix	k?	Vix
Pervenit	Pervenit	k1gFnSc1	Pervenit
(	(	kIx(	(
<g/>
O	o	k7c6	o
lichvě	lichva	k1gFnSc6	lichva
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
nepoctivých	poctivý	k2eNgInPc6d1	nepoctivý
ziscích	zisk	k1gInPc6	zisk
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
encyklika	encyklika	k1gFnSc1	encyklika
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
papežem	papež	k1gMnSc7	papež
Benediktem	Benedikt	k1gMnSc7	Benedikt
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1745	[number]	k4	1745
adresovaná	adresovaný	k2eAgFnSc1d1	adresovaná
italským	italský	k2eAgMnPc3d1	italský
biskupům	biskup	k1gMnPc3	biskup
odsuzující	odsuzující	k2eAgNnSc4d1	odsuzující
přijímání	přijímání	k1gNnSc4	přijímání
úroků	úrok	k1gInPc2	úrok
z	z	k7c2	z
půjčky	půjčka	k1gFnSc2	půjčka
a	a	k8xC	a
označujíc	označovat	k5eAaImSgFnS	označovat
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
lichvu	lichva	k1gFnSc4	lichva
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
katolickou	katolický	k2eAgFnSc4d1	katolická
církev	církev	k1gFnSc4	církev
bez	bez	k7c2	bez
územního	územní	k2eAgNnSc2d1	územní
omezení	omezení	k1gNnSc2	omezení
byla	být	k5eAaImAgFnS	být
encyklika	encyklika	k1gFnSc1	encyklika
znovuvydána	znovuvydán	k2eAgFnSc1d1	znovuvydán
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1836	[number]	k4	1836
papežem	papež	k1gMnSc7	papež
Řehořem	Řehoř	k1gMnSc7	Řehoř
XVI	XVI	kA	XVI
<g/>
..	..	k?	..
Encyklika	encyklika	k1gFnSc1	encyklika
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
učení	učení	k1gNnSc4	učení
církve	církev	k1gFnSc2	církev
s	s	k7c7	s
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
závěry	závěr	k1gInPc4	závěr
Nicejského	Nicejský	k2eAgInSc2d1	Nicejský
koncilu	koncil	k1gInSc2	koncil
z	z	k7c2	z
roku	rok	k1gInSc2	rok
325	[number]	k4	325
o	o	k7c6	o
nepřípustnusti	nepřípustnust	k1gFnSc6	nepřípustnust
úročení	úročení	k1gNnSc6	úročení
a	a	k8xC	a
Třetí	třetí	k4xOgInSc1	třetí
lateránský	lateránský	k2eAgInSc1d1	lateránský
koncil	koncil	k1gInSc1	koncil
ohledně	ohledně	k7c2	ohledně
neposkytování	neposkytování	k1gNnPc2	neposkytování
svátostí	svátost	k1gFnPc2	svátost
lichvářům	lichvář	k1gMnPc3	lichvář
včetně	včetně	k7c2	včetně
zádušní	zádušní	k2eAgFnSc2d1	zádušní
mše	mše	k1gFnSc2	mše
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
otázka	otázka	k1gFnSc1	otázka
vybírání	vybírání	k1gNnSc2	vybírání
úroků	úrok	k1gInPc2	úrok
dostávala	dostávat	k5eAaImAgFnS	dostávat
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
zájmu	zájem	k1gInSc2	zájem
teologů	teolog	k1gMnPc2	teolog
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
obchodních	obchodní	k2eAgFnPc2d1	obchodní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
se	s	k7c7	s
zněním	znění	k1gNnSc7	znění
Napoleonského	napoleonský	k2eAgInSc2d1	napoleonský
občanského	občanský	k2eAgInSc2d1	občanský
zákoníku	zákoník	k1gInSc2	zákoník
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
placení	placení	k1gNnSc6	placení
úroků	úrok	k1gInPc2	úrok
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
<g/>
.	.	kIx.	.
</s>
