<s>
Bitová	bitový	k2eAgFnSc1d1
chybovost	chybovost	k1gFnSc1
neboli	neboli	k8xC
Bit	bit	k2eAgInSc1d1
Error	Error	k1gInSc1
Rate	Rat	k1gInSc2
(	(	kIx(
<g/>
BER	brát	k5eAaImRp2nS
<g/>
)	)	kIx)
vyjadřuje	vyjadřovat	k5eAaImIp3nS
četnost	četnost	k1gFnSc4
chyb	chyba	k1gFnPc2
v	v	k7c6
telekomunikacích	telekomunikace	k1gFnPc6
a	a	k8xC
je	být	k5eAaImIp3nS
definována	definovat	k5eAaBmNgFnS
poměrem	poměr	k1gInSc7
chybně	chybně	k6eAd1
přijatých	přijatý	k2eAgInPc2d1
bitů	bit	k1gInPc2
bE	bE	k?
ku	k	k7c3
celkovému	celkový	k2eAgInSc3d1
počtu	počet	k1gInSc3
přijatých	přijatý	k2eAgInPc2d1
bitů	bit	k1gInPc2
za	za	k7c4
určitou	určitý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
měření	měření	k1gNnSc2
<g/>
.	.	kIx.
</s>