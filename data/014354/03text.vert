<s>
Bitová	bitový	k2eAgFnSc1d1
chybovost	chybovost	k1gFnSc1
</s>
<s>
Bitová	bitový	k2eAgFnSc1d1
chybovost	chybovost	k1gFnSc1
neboli	neboli	k8xC
Bit	bit	k2eAgInSc1d1
Error	Error	k1gInSc1
Rate	Rat	k1gInSc2
(	(	kIx(
<g/>
BER	brát	k5eAaImRp2nS
<g/>
)	)	kIx)
vyjadřuje	vyjadřovat	k5eAaImIp3nS
četnost	četnost	k1gFnSc4
chyb	chyba	k1gFnPc2
v	v	k7c6
telekomunikacích	telekomunikace	k1gFnPc6
a	a	k8xC
je	být	k5eAaImIp3nS
definována	definovat	k5eAaBmNgFnS
poměrem	poměr	k1gInSc7
chybně	chybně	k6eAd1
přijatých	přijatý	k2eAgInPc2d1
bitů	bit	k1gInPc2
bE	bE	k?
ku	k	k7c3
celkovému	celkový	k2eAgInSc3d1
počtu	počet	k1gInSc3
přijatých	přijatý	k2eAgInPc2d1
bitů	bit	k1gInPc2
za	za	k7c4
určitou	určitý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
měření	měření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
B	B	kA
</s>
<s>
E	E	kA
</s>
<s>
R	R	kA
</s>
<s>
=	=	kIx~
</s>
<s>
b	b	k?
</s>
<s>
E	E	kA
</s>
<s>
v	v	k7c6
</s>
<s>
p	p	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
t	t	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
BER	brát	k5eAaImRp2nS
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
bE	bE	k?
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
v_	v_	k?
<g/>
{	{	kIx(
<g/>
p	p	k?
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
t	t	k?
<g/>
}}}	}}}	k?
</s>
<s>
[	[	kIx(
─	─	k?
<g/>
;	;	kIx,
bit	bit	k1gInSc1
<g/>
;	;	kIx,
bit	bit	k1gInSc1
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
;	;	kIx,
s	s	k7c7
]	]	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
</s>
<s>
v	v	k7c6
</s>
<s>
p	p	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
v_	v_	k?
<g/>
{	{	kIx(
<g/>
p	p	k?
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
přenosová	přenosový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
a	a	k8xC
</s>
<s>
t	t	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
t	t	k?
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
celková	celkový	k2eAgFnSc1d1
doba	doba	k1gFnSc1
měření	měření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Chybovost	chybovost	k1gFnSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
vyjádřena	vyjádřit	k5eAaPmNgFnS
četností	četnost	k1gFnSc7
chyb	chyba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dána	dát	k5eAaPmNgFnS
poměrem	poměr	k1gInSc7
chybně	chybně	k6eAd1
přenesených	přenesený	k2eAgInPc2d1
elementů	element	k1gInPc2
digitálního	digitální	k2eAgInSc2d1
signálu	signál	k1gInSc2
k	k	k7c3
celkovému	celkový	k2eAgInSc3d1
počtu	počet	k1gInSc3
přenesených	přenesený	k2eAgInPc2d1
elementů	element	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
praxi	praxe	k1gFnSc6
zjišťují	zjišťovat	k5eAaImIp3nP
tyto	tento	k3xDgInPc4
druhy	druh	k1gInPc4
chybovosti	chybovost	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
chybovost	chybovost	k1gFnSc1
bitová	bitový	k2eAgFnSc1d1
(	(	kIx(
<g/>
symbolová	symbolový	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
chybovost	chybovost	k1gFnSc1
znaková	znakový	k2eAgFnSc1d1
</s>
<s>
chybovost	chybovost	k1gFnSc1
bloková	bloková	k1gFnSc1
</s>
