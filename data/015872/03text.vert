<s>
Connecticut	Connecticut	k1gInSc1
(	(	kIx(
<g/>
řeka	řeka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Connecticut	Connecticut	k2eAgInSc1d1
pohled	pohled	k1gInSc1
z	z	k7c2
French	Frencha	k1gFnPc2
King	Kinga	k1gFnPc2
Bridge	Bridg	k1gFnSc2
(	(	kIx(
<g/>
Mohawk	Mohawk	k1gMnSc1
Trail	Trail	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
552	#num#	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
29	#num#	k4
000	#num#	k4
km²	km²	k?
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
</s>
<s>
606	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Světadíl	světadíl	k1gInSc1
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
Pramen	pramen	k1gInSc1
</s>
<s>
Fourth	Fourth	k1gMnSc1
Connecticut	Connecticut	k1gMnSc1
Lake	Lake	k1gInSc4
45	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
71	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
Long	Long	k1gInSc1
Island	Island	k1gInSc1
Sound	Sound	k1gInSc1
41	#num#	k4
<g/>
°	°	k?
<g/>
16	#num#	k4
<g/>
′	′	k?
<g/>
20	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
72	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
USA	USA	kA
USA	USA	kA
(	(	kIx(
<g/>
Connecticut	Connecticut	k1gInSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
,	,	kIx,
Vermont	Vermont	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gMnSc5
Hampshire	Hampshir	k1gMnSc5
<g/>
)	)	kIx)
Úmoří	úmoří	k1gNnSc6
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc6
</s>
<s>
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
</s>
<s>
tok	tok	k1gInSc1
řeky	řeka	k1gFnSc2
na	na	k7c6
mapě	mapa	k1gFnSc6
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Connecticut	Connecticut	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Connecticut	Connecticut	k2eAgInSc1d1
River	River	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
USA	USA	kA
ve	v	k7c6
státech	stát	k1gInPc6
Connecticut	Connecticut	k1gInSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
,	,	kIx,
Vermont	Vermont	k1gInSc1
a	a	k8xC
New	New	k1gFnSc1
Hampshire	Hampshir	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
552	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povodí	povodí	k1gNnSc1
řeky	řeka	k1gFnSc2
zaujímá	zaujímat	k5eAaImIp3nS
plochu	plocha	k1gFnSc4
29	#num#	k4
000	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
toku	tok	k1gInSc2
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS
v	v	k7c6
jezeře	jezero	k1gNnSc6
Fourth	Fourth	k1gMnSc1
Connecticut	Connecticut	k1gMnSc1
Lake	Lake	k1gFnSc4
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
Appalačského	Appalačský	k2eAgNnSc2d1
pohoří	pohoří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teče	teč	k1gInSc2
na	na	k7c4
jih	jih	k1gInSc4
v	v	k7c6
hlubokém	hluboký	k2eAgNnSc6d1
údolí	údolí	k1gNnSc6
–	–	k?
grabenu	graben	k2eAgFnSc4d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
řadu	řada	k1gFnSc4
vodopádů	vodopád	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústí	ústí	k1gNnPc1
do	do	k7c2
průlivu	průliv	k1gInSc2
Long	Long	k1gInSc4
Island	Island	k1gInSc1
Sound	Sound	k1gInSc1
v	v	k7c6
Atlantském	atlantský	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
městy	město	k1gNnPc7
Gill	Gill	k1gMnSc1
a	a	k8xC
Erwing	Erwing	k1gInSc1
protéká	protékat	k5eAaImIp3nS
pod	pod	k7c7
mostem	most	k1gInSc7
French	French	k1gMnSc1
King	King	k1gMnSc1
Bridge	Bridge	k1gFnSc4
<g/>
,	,	kIx,
přes	přes	k7c4
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
vede	vést	k5eAaImIp3nS
spolkové	spolkový	k2eAgFnSc3d1
státní	státní	k2eAgFnSc3d1
silnici	silnice	k1gFnSc3
Massachusetts	Massachusetts	k1gNnSc2
Route	Rout	k1gInSc5
2	#num#	k4
a	a	k8xC
také	také	k9
známá	známý	k2eAgFnSc1d1
indiánská	indiánský	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Mohawk	Mohawk	k1gMnSc1
Trail	Trail	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
městy	město	k1gNnPc7
Stewartstown	Stewartstown	k1gNnSc1
(	(	kIx(
<g/>
poblíž	poblíž	k6eAd1
trojmezí	trojmezí	k1gNnSc1
Québecu	Québecus	k1gInSc2
<g/>
,	,	kIx,
Vermontu	Vermont	k1gInSc2
a	a	k8xC
New	New	k1gFnSc2
Hampshire	Hampshir	k1gInSc5
<g/>
)	)	kIx)
a	a	k8xC
Northfield	Northfield	k1gInSc1
(	(	kIx(
<g/>
trojmezí	trojmezí	k1gNnSc1
Vermontu	Vermont	k1gInSc2
<g/>
,	,	kIx,
New	New	k1gMnSc5
Hampshire	Hampshir	k1gMnSc5
a	a	k8xC
Massachusetts	Massachusetts	k1gNnPc2
<g/>
)	)	kIx)
tvoří	tvořit	k5eAaImIp3nS
hranici	hranice	k1gFnSc4
mezi	mezi	k7c7
Vermontem	Vermont	k1gMnSc7
a	a	k8xC
New	New	k1gMnSc7
Hampshire	Hampshir	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
stav	stav	k1gInSc1
</s>
<s>
Zdrojem	zdroj	k1gInSc7
vody	voda	k1gFnSc2
jsou	být	k5eAaImIp3nP
sněhové	sněhový	k2eAgFnPc1d1
a	a	k8xC
dešťové	dešťový	k2eAgFnPc1d1
srážky	srážka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
v	v	k7c6
ústí	ústí	k1gNnSc6
činí	činit	k5eAaImIp3nS
606	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Nejvyšších	vysoký	k2eAgInPc2d3
stavů	stav	k1gInPc2
dosahuje	dosahovat	k5eAaImIp3nS
v	v	k7c6
dubnu	duben	k1gInSc6
a	a	k8xC
v	v	k7c6
květnu	květen	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Peřeje	peřej	k1gInPc1
a	a	k8xC
vodopády	vodopád	k1gInPc1
na	na	k7c6
řece	řeka	k1gFnSc6
jsou	být	k5eAaImIp3nP
využívány	využívat	k5eAaImNgInP,k5eAaPmNgInP
vodními	vodní	k2eAgFnPc7d1
elektrárnami	elektrárna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
kanálům	kanál	k1gInPc3
vybudovaným	vybudovaný	k2eAgInPc3d1
kolem	kolem	k7c2
nesjízdných	sjízdný	k2eNgInPc2d1
úseků	úsek	k1gInPc2
je	být	k5eAaImIp3nS
možná	možná	k9
vodní	vodní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
pro	pro	k7c4
menší	malý	k2eAgFnPc4d2
námořní	námořní	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
do	do	k7c2
Hartfordu	Hartford	k1gInSc2
a	a	k8xC
pro	pro	k7c4
říční	říční	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
do	do	k7c2
Holyoke	Holyok	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
К	К	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Connecticut	Connecticut	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
136479638	#num#	k4
</s>
