<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
často	často	k6eAd1	často
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Temelín	Temelín	k1gInSc1	Temelín
(	(	kIx(	(
<g/>
zkratkou	zkratka	k1gFnSc7	zkratka
ETE	ETE	kA	ETE
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
také	také	k9	také
JETE	JETE	kA	JETE
nebo	nebo	k8xC	nebo
JET	jet	k2eAgMnSc1d1	jet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
elektrárna	elektrárna	k1gFnSc1	elektrárna
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
instalovaným	instalovaný	k2eAgInSc7d1	instalovaný
výkonem	výkon	k1gInSc7	výkon
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
českých	český	k2eAgFnPc2d1	Česká
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Temelín	Temelín	k1gInSc1	Temelín
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
terčem	terč	k1gInSc7	terč
kritiky	kritika	k1gFnSc2	kritika
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
českých	český	k2eAgFnPc2d1	Česká
i	i	k8xC	i
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
ekologických	ekologický	k2eAgFnPc2d1	ekologická
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
hornorakouské	hornorakouský	k2eAgFnSc2d1	Hornorakouská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
důležitých	důležitý	k2eAgNnPc2d1	důležité
témat	téma	k1gNnPc2	téma
česko-rakouské	českoakouský	k2eAgFnSc2d1	česko-rakouská
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
výstavba	výstavba	k1gFnSc1	výstavba
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
s	s	k7c7	s
datem	datum	k1gNnSc7	datum
dokončení	dokončení	k1gNnSc2	dokončení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
však	však	k9	však
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejnověji	nově	k6eAd3	nově
dostavěných	dostavěný	k2eAgFnPc2d1	dostavěná
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
<g/>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
má	mít	k5eAaImIp3nS	mít
instalované	instalovaný	k2eAgInPc4d1	instalovaný
dva	dva	k4xCgInPc4	dva
bloky	blok	k1gInPc4	blok
z	z	k7c2	z
původně	původně	k6eAd1	původně
plánovaných	plánovaný	k2eAgFnPc2d1	plánovaná
čtyř	čtyři	k4xCgFnPc2	čtyři
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
po	po	k7c6	po
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
s	s	k7c7	s
elektrickým	elektrický	k2eAgInSc7d1	elektrický
výkonem	výkon	k1gInSc7	výkon
1055	[number]	k4	1055
MW	MW	kA	MW
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc4d1	původní
výkon	výkon	k1gInSc4	výkon
1000	[number]	k4	1000
MW	MW	kA	MW
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
elektrárna	elektrárna	k1gFnSc1	elektrárna
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
přibližně	přibližně	k6eAd1	přibližně
12	[number]	k4	12
TWh	TWh	k1gFnPc2	TWh
(	(	kIx(	(
<g/>
43,2	[number]	k4	43,2
PJ	PJ	kA	PJ
<g/>
)	)	kIx)	)
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
přibližně	přibližně	k6eAd1	přibližně
14	[number]	k4	14
%	%	kIx~	%
výroby	výroba	k1gFnSc2	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
kolem	kolem	k7c2	kolem
23	[number]	k4	23
%	%	kIx~	%
výroby	výroba	k1gFnSc2	výroba
ČEZ	ČEZ	kA	ČEZ
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
instalovaném	instalovaný	k2eAgInSc6d1	instalovaný
výkonu	výkon	k1gInSc6	výkon
elektráren	elektrárna	k1gFnPc2	elektrárna
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
11,43	[number]	k4	11,43
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Ředitelem	ředitel	k1gMnSc7	ředitel
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
Bohdan	Bohdan	k1gMnSc1	Bohdan
Zronek	Zronek	k1gMnSc1	Zronek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umístění	umístění	k1gNnSc1	umístění
Temelína	Temelín	k1gInSc2	Temelín
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc4d1	ostatní
geografické	geografický	k2eAgFnPc4d1	geografická
informace	informace	k1gFnPc4	informace
==	==	k?	==
</s>
</p>
<p>
<s>
Temelín	Temelín	k1gInSc1	Temelín
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
asi	asi	k9	asi
5	[number]	k4	5
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
města	město	k1gNnSc2	město
Týn	Týn	k1gInSc1	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
a	a	k8xC	a
24	[number]	k4	24
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Temelín	Temelín	k1gInSc1	Temelín
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stála	stát	k5eAaImAgFnS	stát
vesnice	vesnice	k1gFnSc1	vesnice
Temelínec	Temelínec	k1gInSc1	Temelínec
<g/>
.	.	kIx.	.
</s>
<s>
JETE	JETE	kA	JETE
je	být	k5eAaImIp3nS	být
vzdálena	vzdálen	k2eAgFnSc1d1	vzdálena
asi	asi	k9	asi
45	[number]	k4	45
až	až	k9	až
50	[number]	k4	50
km	km	kA	km
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
.	.	kIx.	.
<g/>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
Temelína	Temelín	k1gInSc2	Temelín
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
500	[number]	k4	500
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Vltava	Vltava	k1gFnSc1	Vltava
je	být	k5eAaImIp3nS	být
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
km	km	kA	km
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
<g/>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
143	[number]	k4	143
ha	ha	kA	ha
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
123	[number]	k4	123
ha	ha	kA	ha
je	být	k5eAaImIp3nS	být
oploceno	oplocen	k2eAgNnSc1d1	oploceno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
ČEZ	ČEZ	kA	ČEZ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
místa	místo	k1gNnPc4	místo
i	i	k8xC	i
pro	pro	k7c4	pro
případné	případný	k2eAgNnSc4d1	případné
dostavění	dostavění	k1gNnSc4	dostavění
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
konkrétně	konkrétně	k6eAd1	konkrétně
o	o	k7c4	o
parcelu	parcela	k1gFnSc4	parcela
č.	č.	k?	č.
180	[number]	k4	180
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Křtěnov	Křtěnov	k1gInSc4	Křtěnov
<g/>
,	,	kIx,	,
parcelu	parcela	k1gFnSc4	parcela
č.	č.	k?	č.
1053	[number]	k4	1053
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
na	na	k7c4	na
k.ú.	k.ú.	k?	k.ú.
Březí	březí	k1gNnSc4	březí
u	u	k7c2	u
Týna	Týn	k1gInSc2	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
a	a	k8xC	a
parcelu	parcela	k1gFnSc4	parcela
parcela	parcela	k1gFnSc1	parcela
č.	č.	k?	č.
1044	[number]	k4	1044
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
v	v	k7c6	v
k.	k.	k?	k.
ú.	ú.	k?	ú.
Temelínec	Temelínec	k1gInSc1	Temelínec
<g/>
.	.	kIx.	.
<g/>
Lokalita	lokalita	k1gFnSc1	lokalita
Temelína	Temelín	k1gInSc2	Temelín
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
řady	řada	k1gFnSc2	řada
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
<g/>
,	,	kIx,	,
technických	technický	k2eAgNnPc2d1	technické
i	i	k8xC	i
ekonomických	ekonomický	k2eAgNnPc2d1	ekonomické
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
.	.	kIx.	.
</s>
<s>
Blízkost	blízkost	k1gFnSc1	blízkost
Vltavy	Vltava	k1gFnSc2	Vltava
a	a	k8xC	a
vodního	vodní	k2eAgNnSc2d1	vodní
díla	dílo	k1gNnSc2	dílo
Hněvkovice	Hněvkovice	k1gFnSc2	Hněvkovice
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
dostatek	dostatek	k1gInSc4	dostatek
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
chlazení	chlazení	k1gNnSc4	chlazení
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnSc3d1	vysoká
nadmořské	nadmořský	k2eAgFnSc3d1	nadmořská
výšce	výška	k1gFnSc3	výška
však	však	k9	však
elektrárna	elektrárna	k1gFnSc1	elektrárna
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
ohrožena	ohrozit	k5eAaPmNgFnS	ohrozit
povodní	povodeň	k1gFnSc7	povodeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
a	a	k8xC	a
energetického	energetický	k2eAgNnSc2d1	energetické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
umístění	umístění	k1gNnSc1	umístění
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
státu	stát	k1gInSc2	stát
–	–	k?	–
uhelné	uhelný	k2eAgFnSc2d1	uhelná
elektrárny	elektrárna	k1gFnSc2	elektrárna
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
poblíž	poblíž	k7c2	poblíž
ložisek	ložisko	k1gNnPc2	ložisko
uhlí	uhlí	k1gNnSc2	uhlí
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
umístění	umístění	k1gNnSc4	umístění
elektrárny	elektrárna	k1gFnSc2	elektrárna
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
tak	tak	k6eAd1	tak
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
a	a	k8xC	a
zlevňuje	zlevňovat	k5eAaImIp3nS	zlevňovat
přenos	přenos	k1gInSc1	přenos
elektřiny	elektřina	k1gFnSc2	elektřina
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
i	i	k9	i
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
umístění	umístění	k1gNnSc2	umístění
JE	být	k5eAaImIp3nS	být
Dukovany	Dukovany	k1gInPc4	Dukovany
na	na	k7c4	na
jižní	jižní	k2eAgFnSc4d1	jižní
Moravu	Morava	k1gFnSc4	Morava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Temelín	Temelín	k1gInSc1	Temelín
leží	ležet	k5eAaImIp3nS	ležet
z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
na	na	k7c6	na
moldanubickém	moldanubický	k2eAgInSc6d1	moldanubický
hřbetu	hřbet	k1gInSc6	hřbet
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
pokračováním	pokračování	k1gNnSc7	pokračování
Lišovského	Lišovský	k2eAgInSc2d1	Lišovský
prahu	práh	k1gInSc2	práh
<g/>
,	,	kIx,	,
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c4	v
seizmicky	seizmicky	k6eAd1	seizmicky
klidné	klidný	k2eAgFnPc4d1	klidná
a	a	k8xC	a
geologicky	geologicky	k6eAd1	geologicky
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
na	na	k7c6	na
skalním	skalní	k2eAgNnSc6d1	skalní
podloží	podloží	k1gNnSc6	podloží
a	a	k8xC	a
mimo	mimo	k7c4	mimo
geologické	geologický	k2eAgInPc4d1	geologický
zlomy	zlom	k1gInPc4	zlom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnPc1d1	jižní
Čechy	Čechy	k1gFnPc1	Čechy
obecně	obecně	k6eAd1	obecně
i	i	k9	i
okolí	okolí	k1gNnSc4	okolí
Temelína	Temelín	k1gInSc2	Temelín
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
i	i	k8xC	i
jinými	jiný	k2eAgFnPc7d1	jiná
oblastmi	oblast	k1gFnPc7	oblast
Česka	Česko	k1gNnSc2	Česko
řídce	řídce	k6eAd1	řídce
zalidněny	zalidněn	k2eAgFnPc1d1	zalidněna
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
Temelína	Temelín	k1gInSc2	Temelín
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zalesněno	zalesnit	k5eAaPmNgNnS	zalesnit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
výhodné	výhodný	k2eAgNnSc1d1	výhodné
z	z	k7c2	z
bezpečnostního	bezpečnostní	k2eAgNnSc2d1	bezpečnostní
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevýhodné	výhodný	k2eNgNnSc1d1	nevýhodné
pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
odpadního	odpadní	k2eAgNnSc2d1	odpadní
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
relativně	relativně	k6eAd1	relativně
nižší	nízký	k2eAgFnSc4d2	nižší
hustotu	hustota	k1gFnSc4	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
<g/>
,	,	kIx,	,
však	však	k9	však
kvůli	kvůli	k7c3	kvůli
výstavbě	výstavba	k1gFnSc3	výstavba
elektrárny	elektrárna	k1gFnSc2	elektrárna
bylo	být	k5eAaImAgNnS	být
zbouráno	zbourat	k5eAaPmNgNnS	zbourat
šest	šest	k4xCc1	šest
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
předběžné	předběžný	k2eAgFnSc6d1	předběžná
fázi	fáze	k1gFnSc6	fáze
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c4	o
umístění	umístění	k1gNnSc4	umístění
elektrárny	elektrárna	k1gFnSc2	elektrárna
k	k	k7c3	k
obci	obec	k1gFnSc3	obec
Malovice	Malovice	k1gFnSc2	Malovice
(	(	kIx(	(
<g/>
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
elektrárna	elektrárna	k1gFnSc1	elektrárna
byla	být	k5eAaImAgFnS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
JEMA	JEMA	kA	JEMA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
právě	právě	k9	právě
kvůli	kvůli	k7c3	kvůli
nedostatečné	dostatečný	k2eNgFnSc3d1	nedostatečná
geologické	geologický	k2eAgFnSc3d1	geologická
stabilitě	stabilita	k1gFnSc3	stabilita
zavržena	zavržen	k2eAgFnSc1d1	zavržena
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
zkoumána	zkoumán	k2eAgFnSc1d1	zkoumána
lokalita	lokalita	k1gFnSc1	lokalita
Dubenec	Dubenec	k1gMnSc1	Dubenec
(	(	kIx(	(
<g/>
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Dívčice	dívčice	k1gFnSc2	dívčice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
maximálně	maximálně	k6eAd1	maximálně
dvou	dva	k4xCgInPc2	dva
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc4	průzkum
prováděla	provádět	k5eAaImAgFnS	provádět
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
organizace	organizace	k1gFnSc1	organizace
Terplan	Terplan	k1gInSc1	Terplan
<g/>
.	.	kIx.	.
<g/>
Pásmo	pásmo	k1gNnSc1	pásmo
havarijní	havarijní	k2eAgFnSc2d1	havarijní
připravenosti	připravenost	k1gFnSc2	připravenost
bylo	být	k5eAaImAgNnS	být
SÚJB	SÚJB	kA	SÚJB
už	už	k9	už
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1997	[number]	k4	1997
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
na	na	k7c4	na
13	[number]	k4	13
km	km	kA	km
(	(	kIx(	(
<g/>
v	v	k7c6	v
určitých	určitý	k2eAgNnPc6d1	určité
místech	místo	k1gNnPc6	místo
sahá	sahat	k5eAaImIp3nS	sahat
ještě	ještě	k9	ještě
o	o	k7c4	o
trochu	trocha	k1gFnSc4	trocha
dál	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1989	[number]	k4	1989
===	===	k?	===
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
byla	být	k5eAaImAgFnS	být
plánována	plánovat	k5eAaImNgFnS	plánovat
ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
socialistického	socialistický	k2eAgNnSc2d1	socialistické
Československa	Československo	k1gNnSc2	Československo
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
staveb	stavba	k1gFnPc2	stavba
energetického	energetický	k2eAgInSc2d1	energetický
systému	systém	k1gInSc2	systém
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
bývalé	bývalý	k2eAgFnSc2d1	bývalá
RVHP	RVHP	kA	RVHP
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byla	být	k5eAaImAgNnP	být
už	už	k6eAd1	už
předtím	předtím	k6eAd1	předtím
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Jaslovské	Jaslovský	k2eAgFnPc1d1	Jaslovský
Bohunice	Bohunice	k1gFnPc1	Bohunice
(	(	kIx(	(
<g/>
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Dukovany	Dukovany	k1gInPc1	Dukovany
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
Temelínem	Temelín	k1gInSc7	Temelín
se	se	k3xPyFc4	se
plánovala	plánovat	k5eAaImAgFnS	plánovat
stavba	stavba	k1gFnSc1	stavba
Jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Mochovce	Mochovka	k1gFnSc6	Mochovka
(	(	kIx(	(
<g/>
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výhledově	výhledově	k6eAd1	výhledově
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Česko	Česko	k1gNnSc4	Česko
počítalo	počítat	k5eAaImAgNnS	počítat
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
Jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Blahutovice	Blahutovice	k1gFnSc2	Blahutovice
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
EBLA	EBLA	kA	EBLA
<g/>
)	)	kIx)	)
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
Jaderné	jaderný	k2eAgFnPc1d1	jaderná
elektrárny	elektrárna	k1gFnPc1	elektrárna
Tetov	Tetov	k1gInSc1	Tetov
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
JETO	jet	k5eAaImNgNnS	jet
<g/>
)	)	kIx)	)
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
v	v	k7c6	v
delším	dlouhý	k2eAgInSc6d2	delší
časovém	časový	k2eAgInSc6d1	časový
horizontu	horizont	k1gInSc6	horizont
i	i	k9	i
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xS	jako
ESEV	ESEV	kA	ESEV
(	(	kIx(	(
<g/>
elektrárna	elektrárna	k1gFnSc1	elektrárna
severní	severní	k2eAgFnPc1d1	severní
Čechy	Čechy	k1gFnPc1	Čechy
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
nahradit	nahradit	k5eAaPmF	nahradit
dosluhující	dosluhující	k2eAgFnSc2d1	dosluhující
uhelné	uhelný	k2eAgFnSc2d1	uhelná
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
<g/>
Investiční	investiční	k2eAgInSc1d1	investiční
záměr	záměr	k1gInSc1	záměr
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
elektrárny	elektrárna	k1gFnSc2	elektrárna
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
bloku	blok	k1gInSc2	blok
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
byla	být	k5eAaImAgFnS	být
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
bloku	blok	k1gInSc6	blok
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
Československem	Československo	k1gNnSc7	Československo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c4	o
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
blok	blok	k1gInSc1	blok
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
však	však	k9	však
byly	být	k5eAaImAgFnP	být
zvažovány	zvažován	k2eAgFnPc1d1	zvažována
jiné	jiný	k2eAgFnPc1d1	jiná
lokality	lokalita	k1gFnPc1	lokalita
než	než	k8xS	než
Temelín	Temelín	k1gInSc1	Temelín
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výš	vysoce	k6eAd2	vysoce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1986	[number]	k4	1986
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
stavební	stavební	k2eAgNnSc1d1	stavební
povolení	povolení	k1gNnSc1	povolení
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1987	[number]	k4	1987
začala	začít	k5eAaPmAgFnS	začít
stavba	stavba	k1gFnSc1	stavba
provozních	provozní	k2eAgInPc2d1	provozní
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
přípravné	přípravný	k2eAgFnPc1d1	přípravná
práce	práce	k1gFnPc1	práce
však	však	k9	však
začaly	začít	k5eAaPmAgFnP	začít
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
první	první	k4xOgFnSc3	první
z	z	k7c2	z
původně	původně	k6eAd1	původně
plánovaných	plánovaný	k2eAgFnPc2d1	plánovaná
chladicích	chladicí	k2eAgFnPc2d1	chladicí
věží	věž	k1gFnPc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
původních	původní	k2eAgInPc2d1	původní
plánů	plán	k1gInPc2	plán
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
blok	blok	k1gInSc1	blok
elektrárny	elektrárna	k1gFnSc2	elektrárna
dokončen	dokončit	k5eAaPmNgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
už	už	k6eAd1	už
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stavba	stavba	k1gFnSc1	stavba
začala	začít	k5eAaPmAgFnS	začít
výrazně	výrazně	k6eAd1	výrazně
protahovat	protahovat	k5eAaImF	protahovat
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
se	se	k3xPyFc4	se
ozývaly	ozývat	k5eAaImAgInP	ozývat
silné	silný	k2eAgInPc1d1	silný
protesty	protest	k1gInPc1	protest
proti	proti	k7c3	proti
dostavbě	dostavba	k1gFnSc3	dostavba
elektrárny	elektrárna	k1gFnSc2	elektrárna
a	a	k8xC	a
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
odpůrci	odpůrce	k1gMnPc7	odpůrce
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energetiky	energetika	k1gFnSc2	energetika
mj.	mj.	kA	mj.
často	často	k6eAd1	často
zpochybňovali	zpochybňovat	k5eAaImAgMnP	zpochybňovat
její	její	k3xOp3gFnSc4	její
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
akcí	akce	k1gFnPc2	akce
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1989	[number]	k4	1989
rozpracovaná	rozpracovaný	k2eAgFnSc1d1	rozpracovaná
kampaň	kampaň	k1gFnSc1	kampaň
hnutí	hnutí	k1gNnSc2	hnutí
Greenpeace	Greenpeace	k1gFnSc2	Greenpeace
Stop	stop	k2eAgInSc4d1	stop
ČSSRnobyl	ČSSRnobyl	k1gInSc4	ČSSRnobyl
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c6	na
základě	základ	k1gInSc6	základ
odborně	odborně	k6eAd1	odborně
nezpůsobilé	způsobilý	k2eNgFnSc2d1	nezpůsobilá
práce	práce	k1gFnSc2	práce
Ing.	ing.	kA	ing.
Emila	Emil	k1gMnSc2	Emil
Málka	Málek	k1gMnSc2	Málek
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
nedostatečné	dostatečný	k2eNgFnSc6d1	nedostatečná
bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
tehdy	tehdy	k6eAd1	tehdy
ji	on	k3xPp3gFnSc4	on
odborníci	odborník	k1gMnPc1	odborník
z	z	k7c2	z
Fakulty	fakulta	k1gFnSc2	fakulta
jaderné	jaderný	k2eAgFnSc2d1	jaderná
a	a	k8xC	a
fyzikálně	fyzikálně	k6eAd1	fyzikálně
inženýrské	inženýrský	k2eAgFnSc6d1	inženýrská
ČVUT	ČVUT	kA	ČVUT
zpochybnili	zpochybnit	k5eAaPmAgMnP	zpochybnit
a	a	k8xC	a
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
jako	jako	k9	jako
zcela	zcela	k6eAd1	zcela
nesmyslnou	smyslný	k2eNgFnSc7d1	nesmyslná
a	a	k8xC	a
vyvrácenou	vyvrácený	k2eAgFnSc7d1	vyvrácená
<g/>
.	.	kIx.	.
a	a	k8xC	a
příznivci	příznivec	k1gMnPc1	příznivec
naopak	naopak	k6eAd1	naopak
upozorňovali	upozorňovat	k5eAaImAgMnP	upozorňovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
výroba	výroba	k1gFnSc1	výroba
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
ekologičtější	ekologický	k2eAgNnSc1d2	ekologičtější
(	(	kIx(	(
<g/>
především	především	k9	především
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
ekologicky	ekologicky	k6eAd1	ekologicky
poničenými	poničený	k2eAgFnPc7d1	poničená
částmi	část	k1gFnPc7	část
severních	severní	k2eAgFnPc2d1	severní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
)	)	kIx)	)
MAAE	MAAE	kA	MAAE
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
přepracovávat	přepracovávat	k5eAaImF	přepracovávat
projekty	projekt	k1gInPc1	projekt
a	a	k8xC	a
původní	původní	k2eAgInSc1d1	původní
(	(	kIx(	(
<g/>
sovětský	sovětský	k2eAgInSc1d1	sovětský
resp.	resp.	kA	resp.
ruský	ruský	k2eAgInSc1d1	ruský
<g/>
)	)	kIx)	)
řídicí	řídicí	k2eAgInSc1d1	řídicí
a	a	k8xC	a
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
novým	nový	k2eAgInSc7d1	nový
(	(	kIx(	(
<g/>
kontrakt	kontrakt	k1gInSc1	kontrakt
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
udělen	udělen	k2eAgInSc1d1	udělen
americké	americký	k2eAgFnSc3d1	americká
společnosti	společnost	k1gFnSc3	společnost
Westinghouse	Westinghouse	k1gFnSc2	Westinghouse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc4d1	stejný
systém	systém	k1gInSc4	systém
firma	firma	k1gFnSc1	firma
instalovala	instalovat	k5eAaBmAgFnS	instalovat
i	i	k9	i
u	u	k7c2	u
britské	britský	k2eAgFnSc2d1	britská
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Sizewell	Sizewell	k1gMnSc1	Sizewell
B.	B.	kA	B.
</s>
</p>
<p>
<s>
Zároveň	zároveň	k6eAd1	zároveň
nebylo	být	k5eNaImAgNnS	být
kvůli	kvůli	k7c3	kvůli
společenským	společenský	k2eAgFnPc3d1	společenská
změnám	změna	k1gFnPc3	změna
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jaká	jaký	k3yQgFnSc1	jaký
bude	být	k5eAaImBp3nS	být
energetická	energetický	k2eAgFnSc1d1	energetická
potřeba	potřeba	k1gFnSc1	potřeba
státu	stát	k1gInSc2	stát
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
nebo	nebo	k8xC	nebo
restrukturalizaci	restrukturalizace	k1gFnSc4	restrukturalizace
řady	řada	k1gFnSc2	řada
podniků	podnik	k1gInPc2	podnik
energeticky	energeticky	k6eAd1	energeticky
náročného	náročný	k2eAgInSc2d1	náročný
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
elektrárně	elektrárna	k1gFnSc3	elektrárna
se	se	k3xPyFc4	se
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
její	její	k3xOp3gFnSc2	její
výstavby	výstavba	k1gFnSc2	výstavba
stavěla	stavět	k5eAaImAgFnS	stavět
řada	řada	k1gFnSc1	řada
českých	český	k2eAgFnPc2d1	Česká
i	i	k8xC	i
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
nevládních	vládní	k2eNgFnPc2d1	nevládní
protijaderných	protijaderný	k2eAgFnPc2d1	protijaderná
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1990	[number]	k4	1990
byly	být	k5eAaImAgFnP	být
pozastaveny	pozastavit	k5eAaPmNgFnP	pozastavit
práce	práce	k1gFnPc1	práce
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
Mariána	Marián	k1gMnSc2	Marián
Čalfy	Čalf	k1gMnPc7	Čalf
dostavět	dostavět	k5eAaPmF	dostavět
jenom	jenom	k9	jenom
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
blok	blok	k1gInSc1	blok
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
osudu	osud	k1gInSc6	osud
Temelína	Temelín	k1gInSc2	Temelín
jednala	jednat	k5eAaImAgFnS	jednat
i	i	k9	i
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1992	[number]	k4	1992
i	i	k8xC	i
vláda	vláda	k1gFnSc1	vláda
Petra	Petr	k1gMnSc2	Petr
Pitharta	Pithart	k1gMnSc2	Pithart
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
v	v	k7c6	v
předvolební	předvolební	k2eAgFnSc6d1	předvolební
době	doba	k1gFnSc6	doba
přenechala	přenechat	k5eAaPmAgFnS	přenechat
vládě	vláda	k1gFnSc3	vláda
následující	následující	k2eAgFnSc6d1	následující
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
doporučila	doporučit	k5eAaPmAgFnS	doporučit
opatření	opatření	k1gNnSc4	opatření
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
financování	financování	k1gNnSc4	financování
Temelína	Temelín	k1gInSc2	Temelín
a	a	k8xC	a
omezení	omezení	k1gNnSc2	omezení
monopolu	monopol	k1gInSc2	monopol
ČEZ	ČEZ	kA	ČEZ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1993	[number]	k4	1993
byla	být	k5eAaImAgFnS	být
dostavba	dostavba	k1gFnSc1	dostavba
prvního	první	k4xOgMnSc2	první
a	a	k8xC	a
druhého	druhý	k4xOgInSc2	druhý
bloku	blok	k1gInSc2	blok
znovu	znovu	k6eAd1	znovu
schválena	schválit	k5eAaPmNgFnS	schválit
vládou	vláda	k1gFnSc7	vláda
premiéra	premiér	k1gMnSc2	premiér
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
dostavbu	dostavba	k1gFnSc4	dostavba
hlasovala	hlasovat	k5eAaImAgFnS	hlasovat
vláda	vláda	k1gFnSc1	vláda
téměř	téměř	k6eAd1	téměř
jednomyslně	jednomyslně	k6eAd1	jednomyslně
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
ministr	ministr	k1gMnSc1	ministr
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
František	František	k1gMnSc1	František
Benda	Benda	k1gMnSc1	Benda
se	se	k3xPyFc4	se
zdržel	zdržet	k5eAaPmAgMnS	zdržet
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgMnSc1	třetí
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
blok	blok	k1gInSc1	blok
byl	být	k5eAaImAgInS	být
zakonzervován	zakonzervovat	k5eAaPmNgInS	zakonzervovat
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
kompletně	kompletně	k6eAd1	kompletně
dokončených	dokončený	k2eAgFnPc2d1	dokončená
zemních	zemní	k2eAgFnPc2d1	zemní
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
základové	základový	k2eAgFnSc6d1	základová
spáře	spára	k1gFnSc6	spára
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Protesty	protest	k1gInPc1	protest
Rakouska	Rakousko	k1gNnSc2	Rakousko
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
====	====	k?	====
</s>
</p>
<p>
<s>
Proti	proti	k7c3	proti
Temelínu	Temelín	k1gInSc3	Temelín
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
brojilo	brojit	k5eAaImAgNnS	brojit
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
vyostřila	vyostřit	k5eAaPmAgFnS	vyostřit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
přibližujícím	přibližující	k2eAgMnSc7d1	přibližující
se	se	k3xPyFc4	se
vstupem	vstup	k1gInSc7	vstup
Česka	Česko	k1gNnSc2	Česko
do	do	k7c2	do
EU	EU	kA	EU
a	a	k8xC	a
vstupem	vstup	k1gInSc7	vstup
krajně	krajně	k6eAd1	krajně
pravicové	pravicový	k2eAgFnSc6d1	pravicová
FPÖ	FPÖ	kA	FPÖ
do	do	k7c2	do
rakouské	rakouský	k2eAgFnSc2d1	rakouská
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
se	se	k3xPyFc4	se
jako	jako	k9	jako
jediná	jediný	k2eAgNnPc1d1	jediné
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
kandidátských	kandidátský	k2eAgFnPc2d1	kandidátská
zemí	zem	k1gFnPc2	zem
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
diplomatickému	diplomatický	k2eAgInSc3d1	diplomatický
bojkotu	bojkot	k1gInSc3	bojkot
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
FPÖ	FPÖ	kA	FPÖ
dokonce	dokonce	k9	dokonce
požadovala	požadovat	k5eAaImAgFnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Rakousko	Rakousko	k1gNnSc1	Rakousko
v	v	k7c6	v
případě	případ	k1gInSc6	případ
spuštění	spuštění	k1gNnSc2	spuštění
Temelína	Temelín	k1gInSc2	Temelín
zablokovalo	zablokovat	k5eAaPmAgNnS	zablokovat
vstup	vstup	k1gInSc4	vstup
Česka	Česko	k1gNnSc2	Česko
do	do	k7c2	do
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proti	proti	k7c3	proti
Temelínu	Temelín	k1gInSc3	Temelín
však	však	k9	však
vystupovaly	vystupovat	k5eAaImAgInP	vystupovat
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
méně	málo	k6eAd2	málo
ostře	ostro	k6eAd1	ostro
<g/>
)	)	kIx)	)
všechny	všechen	k3xTgFnPc4	všechen
významné	významný	k2eAgFnPc4d1	významná
rakouské	rakouský	k2eAgFnPc4d1	rakouská
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1999	[number]	k4	1999
se	se	k3xPyFc4	se
v	v	k7c6	v
rezoluci	rezoluce	k1gFnSc6	rezoluce
k	k	k7c3	k
dostavbě	dostavba	k1gFnSc3	dostavba
Temelína	Temelín	k1gInSc2	Temelín
dokonce	dokonce	k9	dokonce
kriticky	kriticky	k6eAd1	kriticky
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
Česka	Česko	k1gNnSc2	Česko
však	však	k9	však
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1999	[number]	k4	1999
většinou	většinou	k6eAd1	většinou
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
dostavbu	dostavba	k1gFnSc4	dostavba
Temelína	Temelín	k1gInSc2	Temelín
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
dostavbu	dostavba	k1gFnSc4	dostavba
Temelína	Temelín	k1gInSc2	Temelín
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
především	především	k9	především
ministr	ministr	k1gMnSc1	ministr
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grégr	Grégr	k1gMnSc1	Grégr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dokonce	dokonce	k9	dokonce
získal	získat	k5eAaPmAgMnS	získat
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
atomový	atomový	k2eAgMnSc1d1	atomový
dědek	dědek	k1gMnSc1	dědek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
získal	získat	k5eAaPmAgMnS	získat
později	pozdě	k6eAd2	pozdě
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
atomový	atomový	k2eAgMnSc1d1	atomový
kníže	kníže	k1gMnSc1	kníže
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
blokády	blokáda	k1gFnPc4	blokáda
hraničních	hraniční	k2eAgInPc2d1	hraniční
přechodů	přechod	k1gInPc2	přechod
Rakouska	Rakousko	k1gNnSc2	Rakousko
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
se	se	k3xPyFc4	se
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
dokonce	dokonce	k9	dokonce
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
tzv.	tzv.	kA	tzv.
podpisové	podpisový	k2eAgNnSc1d1	podpisové
referendum	referendum	k1gNnSc1	referendum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
celkem	celkem	k6eAd1	celkem
915	[number]	k4	915
220	[number]	k4	220
rakouských	rakouský	k2eAgMnPc2d1	rakouský
občanů	občan	k1gMnPc2	občan
(	(	kIx(	(
<g/>
15,53	[number]	k4	15,53
%	%	kIx~	%
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
<g/>
)	)	kIx)	)
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
pro	pro	k7c4	pro
podmínění	podmínění	k1gNnSc4	podmínění
vstupu	vstup	k1gInSc2	vstup
Česka	Česko	k1gNnSc2	Česko
do	do	k7c2	do
EU	EU	kA	EU
odstavením	odstavení	k1gNnSc7	odstavení
Temelína	Temelín	k1gInSc2	Temelín
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
uklidnila	uklidnit	k5eAaPmAgFnS	uklidnit
po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
Melkského	Melkský	k2eAgInSc2d1	Melkský
protokolu	protokol	k1gInSc2	protokol
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
k	k	k7c3	k
jehož	jehož	k3xOyRp3gNnSc3	jehož
sjednání	sjednání	k1gNnSc3	sjednání
mezi	mezi	k7c7	mezi
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
i	i	k9	i
úsilí	úsilí	k1gNnSc4	úsilí
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
především	především	k9	především
komisaře	komisař	k1gMnPc4	komisař
pro	pro	k7c4	pro
rozšíření	rozšíření	k1gNnSc4	rozšíření
Güntera	Günter	k1gMnSc2	Günter
Verheugena	Verheugen	k1gMnSc2	Verheugen
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zamítli	zamítnout	k5eAaPmAgMnP	zamítnout
zastupitelé	zastupitel	k1gMnPc1	zastupitel
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
další	další	k2eAgNnSc1d1	další
rozšíření	rozšíření	k1gNnSc1	rozšíření
elektrárny	elektrárna	k1gFnSc2	elektrárna
(	(	kIx(	(
<g/>
dostavbu	dostavba	k1gFnSc4	dostavba
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
bloku	blok	k1gInSc2	blok
<g/>
)	)	kIx)	)
<g/>
Po	po	k7c6	po
kolaudaci	kolaudace	k1gFnSc6	kolaudace
Temelína	Temelín	k1gInSc2	Temelín
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
spory	spor	k1gInPc4	spor
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
znova	znova	k6eAd1	znova
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
odstavec	odstavec	k1gInSc4	odstavec
Kritika	kritik	k1gMnSc2	kritik
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vývoj	vývoj	k1gInSc1	vývoj
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
si	se	k3xPyFc3	se
v	v	k7c6	v
koaliční	koaliční	k2eAgFnSc6d1	koaliční
smlouvě	smlouva	k1gFnSc6	smlouva
Strana	strana	k1gFnSc1	strana
zelených	zelená	k1gFnPc2	zelená
vymínila	vymínit	k5eAaPmAgFnS	vymínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vláda	vláda	k1gFnSc1	vláda
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
nebude	být	k5eNaImBp3nS	být
připravovat	připravovat	k5eAaImF	připravovat
stavbu	stavba	k1gFnSc4	stavba
dalších	další	k2eAgInPc2d1	další
jaderných	jaderný	k2eAgInPc2d1	jaderný
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
otázkám	otázka	k1gFnPc3	otázka
energetiky	energetika	k1gFnSc2	energetika
vláda	vláda	k1gFnSc1	vláda
ustavila	ustavit	k5eAaPmAgFnS	ustavit
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
tzv.	tzv.	kA	tzv.
Pačesovu	Pačesův	k2eAgFnSc4d1	Pačesova
komisi	komise	k1gFnSc4	komise
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
postavila	postavit	k5eAaPmAgFnS	postavit
za	za	k7c4	za
jadernou	jaderný	k2eAgFnSc4d1	jaderná
energetiku	energetika	k1gFnSc4	energetika
a	a	k8xC	a
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
zahájit	zahájit	k5eAaPmF	zahájit
proces	proces	k1gInSc4	proces
posuzování	posuzování	k1gNnSc2	posuzování
vlivu	vliv	k1gInSc2	vliv
dalších	další	k2eAgInPc2d1	další
reaktorů	reaktor	k1gInPc2	reaktor
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
(	(	kIx(	(
<g/>
EIA	EIA	kA	EIA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
ČEZ	ČEZ	kA	ČEZ
pak	pak	k6eAd1	pak
o	o	k7c4	o
proces	proces	k1gInSc4	proces
EIA	EIA	kA	EIA
11.7	[number]	k4	11.7
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
požádala	požádat	k5eAaPmAgFnS	požádat
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
SZ	SZ	kA	SZ
Martin	Martin	k1gMnSc1	Martin
Bursík	Bursík	k1gMnSc1	Bursík
komisi	komise	k1gFnSc4	komise
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
<g/>
,	,	kIx,	,
nehodlá	hodlat	k5eNaImIp3nS	hodlat
však	však	k9	však
EIA	EIA	kA	EIA
bránit	bránit	k5eAaImF	bránit
<g/>
.	.	kIx.	.
</s>
<s>
EIA	EIA	kA	EIA
však	však	k9	však
představuje	představovat	k5eAaImIp3nS	představovat
pouze	pouze	k6eAd1	pouze
první	první	k4xOgFnSc1	první
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
kroků	krok	k1gInPc2	krok
nutných	nutný	k2eAgInPc2d1	nutný
k	k	k7c3	k
povolení	povolení	k1gNnSc3	povolení
dostavby	dostavba	k1gFnSc2	dostavba
Temelína	Temelín	k1gInSc2	Temelín
<g/>
.	.	kIx.	.
</s>
<s>
ČEZ	ČEZ	kA	ČEZ
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stavba	stavba	k1gFnSc1	stavba
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
blok	blok	k1gInSc1	blok
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
spuštěn	spustit	k5eAaPmNgInS	spustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
udělil	udělit	k5eAaPmAgInS	udělit
Jihočeský	jihočeský	k2eAgInSc1d1	jihočeský
kraj	kraj	k1gInSc1	kraj
souhlas	souhlas	k1gInSc1	souhlas
s	s	k7c7	s
dostavbou	dostavba	k1gFnSc7	dostavba
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
bloku	blok	k1gInSc2	blok
elektrárny	elektrárna	k1gFnPc4	elektrárna
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
miliardy	miliarda	k4xCgFnSc2	miliarda
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
ČEZ	ČEZ	kA	ČEZ
do	do	k7c2	do
širšího	široký	k2eAgNnSc2d2	širší
okolí	okolí	k1gNnSc2	okolí
elektrárny	elektrárna	k1gFnSc2	elektrárna
investuje	investovat	k5eAaBmIp3nS	investovat
(	(	kIx(	(
<g/>
opravy	oprava	k1gFnPc4	oprava
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
dostavbu	dostavba	k1gFnSc4	dostavba
dálnice	dálnice	k1gFnSc2	dálnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ČEZ	ČEZ	kA	ČEZ
požádal	požádat	k5eAaPmAgInS	požádat
dokonce	dokonce	k9	dokonce
o	o	k7c4	o
postavení	postavení	k1gNnSc4	postavení
druhé	druhý	k4xOgFnSc2	druhý
železniční	železniční	k2eAgFnSc2d1	železniční
trasy	trasa	k1gFnSc2	trasa
do	do	k7c2	do
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
protestovali	protestovat	k5eAaBmAgMnP	protestovat
představitelé	představitel	k1gMnPc1	představitel
Strany	strana	k1gFnSc2	strana
zelených	zelený	k2eAgMnPc2d1	zelený
a	a	k8xC	a
Jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgFnSc3d1	jiná
absenci	absence	k1gFnSc3	absence
veřejné	veřejný	k2eAgFnSc2d1	veřejná
diskuze	diskuze	k1gFnSc2	diskuze
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
schvalování	schvalování	k1gNnSc2	schvalování
(	(	kIx(	(
<g/>
bylo	být	k5eAaImAgNnS	být
zahrnuto	zahrnout	k5eAaPmNgNnS	zahrnout
do	do	k7c2	do
programu	program	k1gInSc2	program
dodatečně	dodatečně	k6eAd1	dodatečně
<g/>
,	,	kIx,	,
až	až	k9	až
při	při	k7c6	při
zahájení	zahájení	k1gNnSc6	zahájení
jednání	jednání	k1gNnSc2	jednání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zastánce	zastánce	k1gMnPc4	zastánce
dostavby	dostavba	k1gFnSc2	dostavba
naopak	naopak	k6eAd1	naopak
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
sdružení	sdružení	k1gNnSc6	sdružení
Jihočeští	jihočeský	k2eAgMnPc1d1	jihočeský
taťkové	tatěk	k1gMnPc1	tatěk
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
několika	několik	k4yIc2	několik
nezávislých	závislý	k2eNgInPc2d1	nezávislý
výzkumů	výzkum	k1gInPc2	výzkum
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
okolo	okolo	k7c2	okolo
70	[number]	k4	70
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zajímavostem	zajímavost	k1gFnPc3	zajímavost
patří	patřit	k5eAaImIp3nP	patřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podporu	podpora	k1gFnSc4	podpora
nadpoloviční	nadpoloviční	k2eAgFnSc2d1	nadpoloviční
většiny	většina	k1gFnSc2	většina
má	mít	k5eAaImIp3nS	mít
tato	tento	k3xDgFnSc1	tento
otázka	otázka	k1gFnSc1	otázka
dokonce	dokonce	k9	dokonce
i	i	k9	i
mezi	mezi	k7c7	mezi
voliči	volič	k1gMnPc7	volič
Strany	strana	k1gFnSc2	strana
zelených	zelená	k1gFnPc2	zelená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spuštění	spuštění	k1gNnSc1	spuštění
Temelína	Temelín	k1gInSc2	Temelín
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
provoz	provoz	k1gInSc1	provoz
===	===	k?	===
</s>
</p>
<p>
<s>
Palivo	palivo	k1gNnSc1	palivo
pro	pro	k7c4	pro
první	první	k4xOgInSc4	první
blok	blok	k1gInSc4	blok
bylo	být	k5eAaImAgNnS	být
zavezeno	zavézt	k5eAaPmNgNnS	zavézt
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
aktivováno	aktivovat	k5eAaBmNgNnS	aktivovat
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2000	[number]	k4	2000
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
blok	blok	k1gInSc1	blok
poprvé	poprvé	k6eAd1	poprvé
připojen	připojit	k5eAaPmNgInS	připojit
do	do	k7c2	do
rozvodné	rozvodný	k2eAgFnSc2d1	rozvodná
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Zkušební	zkušební	k2eAgInSc1d1	zkušební
provoz	provoz	k1gInSc1	provoz
prvního	první	k4xOgInSc2	první
bloku	blok	k1gInSc2	blok
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2002	[number]	k4	2002
bylo	být	k5eAaImAgNnS	být
zavezeno	zavezen	k2eAgNnSc1d1	zavezeno
palivo	palivo	k1gNnSc1	palivo
pro	pro	k7c4	pro
druhý	druhý	k4xOgInSc4	druhý
blok	blok	k1gInSc4	blok
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
reakce	reakce	k1gFnSc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozvodné	rozvodný	k2eAgFnSc3d1	rozvodná
síti	síť	k1gFnSc3	síť
byl	být	k5eAaImAgInS	být
druhý	druhý	k4xOgInSc1	druhý
blok	blok	k1gInSc1	blok
poprvé	poprvé	k6eAd1	poprvé
připojen	připojit	k5eAaPmNgInS	připojit
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2002	[number]	k4	2002
a	a	k8xC	a
zkušební	zkušební	k2eAgInSc1d1	zkušební
provoz	provoz	k1gInSc1	provoz
začal	začít	k5eAaPmAgInS	začít
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
Temelín	Temelín	k1gInSc1	Temelín
zkolaudován	zkolaudovat	k5eAaPmNgInS	zkolaudovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
nabylo	nabýt	k5eAaPmAgNnS	nabýt
právní	právní	k2eAgNnSc1d1	právní
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
<g/>
Koeficient	koeficient	k1gInSc4	koeficient
ročního	roční	k2eAgNnSc2d1	roční
využití	využití	k1gNnSc2	využití
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
všechny	všechen	k3xTgInPc4	všechen
technologické	technologický	k2eAgInPc4d1	technologický
problémy	problém	k1gInPc4	problém
přiblížil	přiblížit	k5eAaPmAgInS	přiblížit
70	[number]	k4	70
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
dořešení	dořešení	k1gNnSc6	dořešení
začal	začít	k5eAaPmAgInS	začít
dosahovat	dosahovat	k5eAaImF	dosahovat
hodnot	hodnota	k1gFnPc2	hodnota
kolem	kolem	k7c2	kolem
85	[number]	k4	85
%	%	kIx~	%
a	a	k8xC	a
roční	roční	k2eAgFnSc1d1	roční
výroba	výroba	k1gFnSc1	výroba
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
hodnot	hodnota	k1gFnPc2	hodnota
15	[number]	k4	15
TWh	TWh	k1gFnPc2	TWh
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
v	v	k7c4	v
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
hodin	hodina	k1gFnPc2	hodina
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
celková	celkový	k2eAgFnSc1d1	celková
výroba	výroba	k1gFnSc1	výroba
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
od	od	k7c2	od
zahájení	zahájení	k1gNnSc2	zahájení
provozu	provoz	k1gInSc2	provoz
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
kWh	kwh	kA	kwh
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
množství	množství	k1gNnSc1	množství
elektřiny	elektřina	k1gFnSc2	elektřina
by	by	kYmCp3nS	by
pokrylo	pokrýt	k5eAaPmAgNnS	pokrýt
spotřebu	spotřeba	k1gFnSc4	spotřeba
celého	celý	k2eAgNnSc2d1	celé
Česka	Česko	k1gNnSc2	Česko
na	na	k7c4	na
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
Temelín	Temelín	k1gInSc1	Temelín
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
výrobu	výroba	k1gFnSc4	výroba
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
cca	cca	kA	cca
3500	[number]	k4	3500
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
aspekty	aspekt	k1gInPc1	aspekt
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Cena	cena	k1gFnSc1	cena
===	===	k?	===
</s>
</p>
<p>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
investiční	investiční	k2eAgInPc1d1	investiční
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
bloky	blok	k1gInPc4	blok
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
elektrickém	elektrický	k2eAgInSc6d1	elektrický
výkonu	výkon	k1gInSc6	výkon
2	[number]	k4	2
000	[number]	k4	000
MW	MW	kA	MW
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
i	i	k9	i
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zcela	zcela	k6eAd1	zcela
zásadních	zásadní	k2eAgFnPc2d1	zásadní
úprav	úprava	k1gFnPc2	úprava
projektu	projekt	k1gInSc2	projekt
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
výstavby	výstavba	k1gFnSc2	výstavba
98	[number]	k4	98
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
cena	cena	k1gFnSc1	cena
do	do	k7c2	do
konce	konec	k1gInSc2	konec
září	září	k1gNnSc4	září
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
investiční	investiční	k2eAgInPc4d1	investiční
náklady	náklad	k1gInPc4	náklad
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
cca	cca	kA	cca
50	[number]	k4	50
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
/	/	kIx~	/
<g/>
kW	kW	kA	kW
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
financovala	financovat	k5eAaBmAgFnS	financovat
společnost	společnost	k1gFnSc1	společnost
ČEZ	ČEZ	kA	ČEZ
pomocí	pomocí	k7c2	pomocí
úvěrů	úvěr	k1gInPc2	úvěr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
dvě	dva	k4xCgFnPc4	dva
bankovní	bankovní	k2eAgFnPc4d1	bankovní
konsorcia	konsorcium	k1gNnSc2	konsorcium
(	(	kIx(	(
<g/>
konsorcium	konsorcium	k1gNnSc1	konsorcium
bank	banka	k1gFnPc2	banka
vedené	vedený	k2eAgInPc4d1	vedený
Citibank	Citibank	k1gInSc4	Citibank
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
konsorcium	konsorcium	k1gNnSc1	konsorcium
bank	banka	k1gFnPc2	banka
vedené	vedený	k2eAgNnSc1d1	vedené
bankou	banka	k1gFnSc7	banka
Fortis	Fortis	k1gFnPc2	Fortis
bank	banka	k1gFnPc2	banka
(	(	kIx(	(
<g/>
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
za	za	k7c4	za
úvěry	úvěra	k1gFnPc4	úvěra
se	se	k3xPyFc4	se
zaručil	zaručit	k5eAaPmAgInS	zaručit
stát	stát	k1gInSc1	stát
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
Exim	Exim	k1gInSc1	Exim
Bank	bank	k1gInSc1	bank
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Čistě	čistě	k6eAd1	čistě
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
návratnost	návratnost	k1gFnSc1	návratnost
investice	investice	k1gFnSc1	investice
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c6	na
méně	málo	k6eAd2	málo
než	než	k8xS	než
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odvody	odvod	k1gInPc1	odvod
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
účet	účet	k1gInSc4	účet
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
vyrobenou	vyrobený	k2eAgFnSc4d1	vyrobená
kilowatthodinu	kilowatthodina	k1gFnSc4	kilowatthodina
elektřiny	elektřina	k1gFnSc2	elektřina
odvádí	odvádět	k5eAaImIp3nS	odvádět
ČEZ	ČEZ	kA	ČEZ
5	[number]	k4	5
haléřů	haléř	k1gInPc2	haléř
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
jaderný	jaderný	k2eAgInSc4d1	jaderný
účet	účet	k1gInSc4	účet
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
prostředky	prostředek	k1gInPc1	prostředek
na	na	k7c4	na
uložení	uložení	k1gNnSc4	uložení
vyprodukovaného	vyprodukovaný	k2eAgInSc2d1	vyprodukovaný
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
odpadu	odpad	k1gInSc2	odpad
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
přepracování	přepracování	k1gNnSc2	přepracování
<g/>
.	.	kIx.	.
</s>
<s>
ČEZ	ČEZ	kA	ČEZ
musí	muset	k5eAaImIp3nS	muset
vytvářet	vytvářet	k5eAaImF	vytvářet
i	i	k9	i
finanční	finanční	k2eAgFnPc4d1	finanční
rezervy	rezerva	k1gFnPc4	rezerva
na	na	k7c6	na
budoucí	budoucí	k2eAgFnSc6d1	budoucí
likvidaci	likvidace	k1gFnSc6	likvidace
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
vážné	vážný	k2eAgFnSc2d1	vážná
havárie	havárie	k1gFnSc2	havárie
je	být	k5eAaImIp3nS	být
Temelín	Temelín	k1gInSc1	Temelín
pojištěn	pojistit	k5eAaPmNgInS	pojistit
u	u	k7c2	u
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
poolu	pool	k1gInSc2	pool
pojišťoven	pojišťovna	k1gFnPc2	pojišťovna
<g/>
.	.	kIx.	.
</s>
<s>
Limit	limit	k1gInSc1	limit
plnění	plnění	k1gNnSc2	plnění
však	však	k9	však
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
ani	ani	k9	ani
1	[number]	k4	1
%	%	kIx~	%
předpokládaných	předpokládaný	k2eAgInPc2d1	předpokládaný
nákladů	náklad	k1gInPc2	náklad
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vážné	vážný	k2eAgFnSc2d1	vážná
havárie	havárie	k1gFnSc2	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
problém	problém	k1gInSc1	problém
jen	jen	k6eAd1	jen
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elekráren	elekrárna	k1gFnPc2	elekrárna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
většiny	většina	k1gFnSc2	většina
hydroelektráren	hydroelektrárna	k1gFnPc2	hydroelektrárna
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
velkých	velký	k2eAgInPc2d1	velký
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
komplexů	komplex	k1gInPc2	komplex
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výrobní	výrobní	k2eAgInPc1d1	výrobní
ukazatele	ukazatel	k1gInPc1	ukazatel
ETE	ETE	kA	ETE
===	===	k?	===
</s>
</p>
<p>
<s>
Tučně	tučně	k6eAd1	tučně
jsou	být	k5eAaImIp3nP	být
vyznačena	vyznačen	k2eAgNnPc4d1	vyznačeno
čísla	číslo	k1gNnPc4	číslo
u	u	k7c2	u
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
elektrárna	elektrárna	k1gFnSc1	elektrárna
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
nového	nový	k2eAgInSc2d1	nový
rekordu	rekord	k1gInSc2	rekord
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
je	být	k5eAaImIp3nS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koeficient	koeficient	k1gInSc1	koeficient
ročního	roční	k2eAgNnSc2d1	roční
využití	využití	k1gNnSc2	využití
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
87,1	[number]	k4	87,1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdroje	zdroj	k1gInPc1	zdroj
<g/>
:	:	kIx,	:
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
ČTK	ČTK	kA	ČTK
</s>
</p>
<p>
<s>
==	==	k?	==
Technické	technický	k2eAgFnPc1d1	technická
informace	informace	k1gFnPc1	informace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kombinace	kombinace	k1gFnSc1	kombinace
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
dodavatelské	dodavatelský	k2eAgFnPc4d1	dodavatelská
firmy	firma	k1gFnPc4	firma
===	===	k?	===
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
projekt	projekt	k1gInSc1	projekt
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
(	(	kIx(	(
<g/>
sovětské	sovětský	k2eAgFnSc6d1	sovětská
<g/>
)	)	kIx)	)
technologii	technologie	k1gFnSc6	technologie
<g/>
,	,	kIx,	,
řídicí	řídicí	k2eAgInSc1d1	řídicí
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kombinace	kombinace	k1gFnSc1	kombinace
"	"	kIx"	"
<g/>
západních	západní	k2eAgMnPc2d1	západní
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
východních	východní	k2eAgFnPc2d1	východní
<g/>
"	"	kIx"	"
technologií	technologie	k1gFnPc2	technologie
byla	být	k5eAaImAgFnS	být
důvodem	důvod	k1gInSc7	důvod
obav	obava	k1gFnPc2	obava
o	o	k7c4	o
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
však	však	k9	však
už	už	k6eAd1	už
vyzkoušena	vyzkoušet	k5eAaPmNgFnS	vyzkoušet
na	na	k7c6	na
Jaderné	jaderný	k2eAgFnSc6d1	jaderná
elektrárně	elektrárna	k1gFnSc6	elektrárna
Loviisa	Loviisa	k1gFnSc1	Loviisa
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnSc1	mise
MAAE	MAAE	kA	MAAE
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
konstatovala	konstatovat	k5eAaBmAgFnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kombinace	kombinace	k1gFnSc1	kombinace
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
techniky	technika	k1gFnSc2	technika
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
pečlivě	pečlivě	k6eAd1	pečlivě
zváženy	zvážen	k2eAgFnPc1d1	zvážena
a	a	k8xC	a
podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
mise	mise	k1gFnSc2	mise
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
kombinace	kombinace	k1gFnSc2	kombinace
obou	dva	k4xCgFnPc2	dva
technik	technika	k1gFnPc2	technika
dokonce	dokonce	k9	dokonce
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
<g/>
Většina	většina	k1gFnSc1	většina
vybavení	vybavení	k1gNnSc2	vybavení
elektrárny	elektrárna	k1gFnSc2	elektrárna
však	však	k9	však
byla	být	k5eAaImAgFnS	být
dodána	dodat	k5eAaPmNgFnS	dodat
českými	český	k2eAgFnPc7d1	Česká
firmami	firma	k1gFnPc7	firma
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ŠKODA	škoda	k1gFnSc1	škoda
PRAHA	Praha	k1gFnSc1	Praha
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
dodavatel	dodavatel	k1gMnSc1	dodavatel
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
,	,	kIx,	,
ŠKODA	škoda	k6eAd1	škoda
Jaderné	jaderný	k2eAgNnSc1d1	jaderné
strojírenství	strojírenství	k1gNnSc1	strojírenství
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
<g/>
,	,	kIx,	,
Sigma	sigma	k1gNnSc1	sigma
Lutín	Lutína	k1gFnPc2	Lutína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
===	===	k?	===
</s>
</p>
<p>
<s>
Bezpečnostní	bezpečnostní	k2eAgNnPc1d1	bezpečnostní
opatření	opatření	k1gNnPc1	opatření
v	v	k7c6	v
Temelíně	Temelín	k1gInSc6	Temelín
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
celou	celý	k2eAgFnSc4d1	celá
škálu	škála	k1gFnSc4	škála
opatření	opatření	k1gNnSc2	opatření
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
úrovní	úroveň	k1gFnPc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
systémů	systém	k1gInPc2	systém
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
na	na	k7c6	na
rozdílných	rozdílný	k2eAgInPc6d1	rozdílný
fyzikálních	fyzikální	k2eAgInPc6d1	fyzikální
a	a	k8xC	a
technických	technický	k2eAgInPc6d1	technický
principech	princip	k1gInPc6	princip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
z	z	k7c2	z
principů	princip	k1gInPc2	princip
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
princip	princip	k1gInSc1	princip
hloubkové	hloubkový	k2eAgFnSc2d1	hloubková
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
definuje	definovat	k5eAaBmIp3nS	definovat
5	[number]	k4	5
úrovní	úroveň	k1gFnPc2	úroveň
ochrany	ochrana	k1gFnSc2	ochrana
a	a	k8xC	a
5	[number]	k4	5
ochranných	ochranný	k2eAgFnPc2d1	ochranná
bariér	bariéra	k1gFnPc2	bariéra
stojících	stojící	k2eAgFnPc2d1	stojící
mezi	mezi	k7c7	mezi
aktivní	aktivní	k2eAgFnSc7d1	aktivní
zónou	zóna	k1gFnSc7	zóna
v	v	k7c6	v
reaktoru	reaktor	k1gInSc6	reaktor
a	a	k8xC	a
okolním	okolní	k2eAgNnSc7d1	okolní
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pevná	pevný	k2eAgFnSc1d1	pevná
keramická	keramický	k2eAgFnSc1d1	keramická
struktura	struktura	k1gFnSc1	struktura
samotného	samotný	k2eAgNnSc2d1	samotné
paliva	palivo	k1gNnSc2	palivo
</s>
</p>
<p>
<s>
pokrytí	pokrytí	k1gNnSc1	pokrytí
palivových	palivový	k2eAgInPc2d1	palivový
proutků	proutek	k1gInPc2	proutek
</s>
</p>
<p>
<s>
tlaková	tlakový	k2eAgFnSc1d1	tlaková
hranice	hranice	k1gFnSc1	hranice
primárního	primární	k2eAgInSc2d1	primární
okruhu	okruh	k1gInSc2	okruh
</s>
</p>
<p>
<s>
železobetonová	železobetonový	k2eAgFnSc1d1	železobetonová
šachta	šachta	k1gFnSc1	šachta
reaktoru	reaktor	k1gInSc2	reaktor
</s>
</p>
<p>
<s>
ochranná	ochranný	k2eAgFnSc1d1	ochranná
obálka	obálka	k1gFnSc1	obálka
(	(	kIx(	(
<g/>
kontejnment	kontejnment	k1gInSc1	kontejnment
<g/>
)	)	kIx)	)
<g/>
Aktivní	aktivní	k2eAgInPc1d1	aktivní
bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
systémy	systém	k1gInPc1	systém
jsou	být	k5eAaImIp3nP	být
zálohovány	zálohovat	k5eAaBmNgInP	zálohovat
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
jsou	být	k5eAaImIp3nP	být
nainstalovány	nainstalován	k2eAgFnPc1d1	nainstalována
třikrát	třikrát	k6eAd1	třikrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předností	přednost	k1gFnSc7	přednost
použitých	použitý	k2eAgInPc2d1	použitý
tlakovodních	tlakovodní	k2eAgInPc2d1	tlakovodní
reaktorů	reaktor	k1gInPc2	reaktor
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
inherentní	inherentní	k2eAgFnSc1d1	inherentní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
tj.	tj.	kA	tj.
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
daná	daný	k2eAgFnSc1d1	daná
fyzikálními	fyzikální	k2eAgInPc7d1	fyzikální
zákony	zákon	k1gInPc7	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Temelíně	Temelín	k1gInSc6	Temelín
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
mj.	mj.	kA	mj.
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
schopnost	schopnost	k1gFnSc1	schopnost
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
moderátor	moderátor	k1gMnSc1	moderátor
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
štěpná	štěpný	k2eAgFnSc1d1	štěpná
reakce	reakce	k1gFnSc1	reakce
vůbec	vůbec	k9	vůbec
mohla	moct	k5eAaImAgFnS	moct
probíhat	probíhat	k5eAaImF	probíhat
<g/>
)	)	kIx)	)
umožňovat	umožňovat	k5eAaImF	umožňovat
řetězovou	řetězový	k2eAgFnSc4d1	řetězová
reakci	reakce	k1gFnSc4	reakce
klesá	klesat	k5eAaImIp3nS	klesat
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
teplotou	teplota	k1gFnSc7	teplota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
vlastnost	vlastnost	k1gFnSc4	vlastnost
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
uran	uran	k1gInSc1	uran
238	[number]	k4	238
(	(	kIx(	(
<g/>
tvoří	tvořit	k5eAaImIp3nS	tvořit
kolem	kolem	k7c2	kolem
97	[number]	k4	97
%	%	kIx~	%
uranu	uran	k1gInSc2	uran
v	v	k7c6	v
palivu	palivo	k1gNnSc6	palivo
<g/>
,	,	kIx,	,
fakticky	fakticky	k6eAd1	fakticky
ale	ale	k9	ale
jako	jako	k9	jako
palivo	palivo	k1gNnSc4	palivo
nepůsobí	působit	k5eNaImIp3nS	působit
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
vyráběné	vyráběný	k2eAgFnSc2d1	vyráběná
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pohlcováním	pohlcování	k1gNnPc3	pohlcování
neutronů	neutron	k1gInPc2	neutron
reakci	reakce	k1gFnSc3	reakce
brzdí	brzdit	k5eAaImIp3nS	brzdit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
jeho	jeho	k3xOp3gFnSc1	jeho
vlastnost	vlastnost	k1gFnSc1	vlastnost
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
s	s	k7c7	s
růstem	růst	k1gInSc7	růst
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přílišném	přílišný	k2eAgInSc6d1	přílišný
vzrůstu	vzrůst	k1gInSc6	vzrůst
teploty	teplota	k1gFnSc2	teplota
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
tak	tak	k9	tak
reakce	reakce	k1gFnSc1	reakce
sama	sám	k3xTgFnSc1	sám
zastavila	zastavit	k5eAaPmAgFnS	zastavit
<g/>
.	.	kIx.	.
<g/>
Bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
systémů	systém	k1gInPc2	systém
využívajících	využívající	k2eAgInPc2d1	využívající
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
zákonitosti	zákonitost	k1gFnPc4	zákonitost
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Temelín	Temelín	k1gInSc1	Temelín
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
<g/>
,	,	kIx,	,
např.	např.	kA	např.
systém	systém	k1gInSc4	systém
tvořený	tvořený	k2eAgInSc1d1	tvořený
čtyřmi	čtyři	k4xCgInPc7	čtyři
hydroakumulátory	hydroakumulátor	k1gInPc7	hydroakumulátor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
zaplavily	zaplavit	k5eAaPmAgInP	zaplavit
aktivní	aktivní	k2eAgFnSc4d1	aktivní
zónu	zóna	k1gFnSc4	zóna
při	při	k7c6	při
mimořádných	mimořádný	k2eAgFnPc6d1	mimořádná
situacích	situace	k1gFnPc6	situace
spojených	spojený	k2eAgFnPc6d1	spojená
poklesem	pokles	k1gInSc7	pokles
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
primárním	primární	k2eAgInSc6d1	primární
okruhu	okruh	k1gInSc6	okruh
<g/>
,	,	kIx,	,
sprchový	sprchový	k2eAgInSc1d1	sprchový
systém	systém	k1gInSc1	systém
ochranné	ochranný	k2eAgFnSc2d1	ochranná
obálky	obálka	k1gFnSc2	obálka
<g/>
,	,	kIx,	,
systém	systém	k1gInSc4	systém
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
pád	pád	k1gInSc4	pád
souboru	soubor	k1gInSc2	soubor
absorpčních	absorpční	k2eAgFnPc2d1	absorpční
tyčí	tyč	k1gFnPc2	tyč
do	do	k7c2	do
aktivní	aktivní	k2eAgFnSc2d1	aktivní
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zajistí	zajistit	k5eAaPmIp3nS	zajistit
rychlé	rychlý	k2eAgNnSc1d1	rychlé
odstavení	odstavení	k1gNnSc1	odstavení
reaktoru	reaktor	k1gInSc2	reaktor
atd.	atd.	kA	atd.
<g/>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
ČEZ	ČEZ	kA	ČEZ
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
nevykazuje	vykazovat	k5eNaImIp3nS	vykazovat
Temelín	Temelín	k1gInSc1	Temelín
výrazně	výrazně	k6eAd1	výrazně
horší	horšit	k5eAaImIp3nS	horšit
výsledky	výsledek	k1gInPc4	výsledek
ohledně	ohledně	k7c2	ohledně
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
než	než	k8xS	než
francouzské	francouzský	k2eAgFnSc2d1	francouzská
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
horší	zlý	k2eAgInPc1d2	horší
výsledky	výsledek	k1gInPc1	výsledek
než	než	k8xS	než
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
elektráren	elektrárna	k1gFnPc2	elektrárna
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
nezjistil	zjistit	k5eNaPmAgInS	zjistit
ani	ani	k8xC	ani
tým	tým	k1gInSc1	tým
WANO	WANO	kA	WANO
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2017	[number]	k4	2017
elektrárna	elektrárna	k1gFnSc1	elektrárna
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc4	titul
Bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
podnik	podnik	k1gInSc4	podnik
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
jej	on	k3xPp3gNnSc4	on
již	již	k9	již
popáté	popáté	k4xO	popáté
a	a	k8xC	a
obhájila	obhájit	k5eAaPmAgFnS	obhájit
jeho	jeho	k3xOp3gInSc4	jeho
zisk	zisk	k1gInSc4	zisk
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reaktory	reaktor	k1gInPc4	reaktor
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Vlastní	vlastní	k2eAgInPc1d1	vlastní
reaktory	reaktor	k1gInPc1	reaktor
====	====	k?	====
</s>
</p>
<p>
<s>
JETE	JETE	kA	JETE
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
dvěma	dva	k4xCgInPc7	dva
tlakovodními	tlakovodní	k2eAgInPc7d1	tlakovodní
reaktory	reaktor	k1gInPc7	reaktor
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
o	o	k7c6	o
tepelném	tepelný	k2eAgInSc6d1	tepelný
výkonu	výkon	k1gInSc6	výkon
3120	[number]	k4	3120
MW	MW	kA	MW
a	a	k8xC	a
elektrickém	elektrický	k2eAgInSc6d1	elektrický
výkonu	výkon	k1gInSc6	výkon
připojeného	připojený	k2eAgInSc2d1	připojený
turboalternátoru	turboalternátor	k1gInSc2	turboalternátor
1055	[number]	k4	1055
MW	MW	kA	MW
(	(	kIx(	(
<g/>
VVER-	VVER-	k1gFnSc1	VVER-
<g/>
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
typ	typ	k1gInSc1	typ
V	v	k7c6	v
<g/>
320	[number]	k4	320
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc1d1	původní
projekt	projekt	k1gInSc1	projekt
počítal	počítat	k5eAaImAgInS	počítat
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
čtyř	čtyři	k4xCgInPc2	čtyři
bloků	blok	k1gInPc2	blok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
reaktoru	reaktor	k1gInSc2	reaktor
bez	bez	k7c2	bez
chladiva	chladivo	k1gNnSc2	chladivo
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
800	[number]	k4	800
t.	t.	k?	t.
Chladivo	chladivo	k1gNnSc1	chladivo
tvoří	tvořit	k5eAaImIp3nS	tvořit
slabý	slabý	k2eAgInSc4d1	slabý
roztok	roztok	k1gInSc4	roztok
kyseliny	kyselina	k1gFnSc2	kyselina
borité	boritý	k2eAgNnSc1d1	borité
(	(	kIx(	(
<g/>
kyselina	kyselina	k1gFnSc1	kyselina
boritá	boritý	k2eAgFnSc1d1	boritá
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
bor	bor	k1gInSc4	bor
10B	[number]	k4	10B
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
dlouhodobé	dlouhodobý	k2eAgFnSc3d1	dlouhodobá
regulaci	regulace	k1gFnSc3	regulace
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
výkon	výkon	k1gInSc1	výkon
reaktoru	reaktor	k1gInSc2	reaktor
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
pobytu	pobyt	k1gInSc2	pobyt
paliva	palivo	k1gNnSc2	palivo
totiž	totiž	k9	totiž
mírně	mírně	k6eAd1	mírně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
výměně	výměna	k1gFnSc6	výměna
je	být	k5eAaImIp3nS	být
koncentrace	koncentrace	k1gFnSc1	koncentrace
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
reakci	reakce	k1gFnSc4	reakce
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc4d2	veliký
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
výkon	výkon	k1gInSc1	výkon
byl	být	k5eAaImAgInS	být
konstantní	konstantní	k2eAgMnSc1d1	konstantní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Tlakové	tlakový	k2eAgFnPc1d1	tlaková
nádoby	nádoba	k1gFnPc1	nádoba
====	====	k?	====
</s>
</p>
<p>
<s>
Části	část	k1gFnPc1	část
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
vestavby	vestavba	k1gFnSc2	vestavba
reaktoru	reaktor	k1gInSc2	reaktor
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
v	v	k7c6	v
tlakové	tlakový	k2eAgFnSc6d1	tlaková
nádobě	nádoba	k1gFnSc6	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Tlaková	tlakový	k2eAgFnSc1d1	tlaková
nádoba	nádoba	k1gFnSc1	nádoba
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
11	[number]	k4	11
m	m	kA	m
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
její	její	k3xOp3gNnSc4	její
dno	dno	k1gNnSc4	dno
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
16	[number]	k4	16
m	m	kA	m
nad	nad	k7c7	nad
úrovní	úroveň	k1gFnSc7	úroveň
okolního	okolní	k2eAgInSc2d1	okolní
terénu	terén	k1gInSc2	terén
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInSc1d1	vnější
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
4,585	[number]	k4	4,585
m.	m.	k?	m.
Stěny	stěna	k1gFnSc2	stěna
válcové	válcový	k2eAgFnPc1d1	válcová
části	část	k1gFnPc1	část
mají	mít	k5eAaImIp3nP	mít
tloušťku	tloušťka	k1gFnSc4	tloušťka
193	[number]	k4	193
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Nádoba	nádoba	k1gFnSc1	nádoba
je	být	k5eAaImIp3nS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
vysoce	vysoce	k6eAd1	vysoce
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
nízkolegované	nízkolegovaný	k2eAgFnSc2d1	nízkolegovaná
chrom-nikl-molybden-vanadové	chromiklolybdenanadový	k2eAgFnSc2d1	chrom-nikl-molybden-vanadový
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
na	na	k7c4	na
tlak	tlak	k1gInSc4	tlak
17,6	[number]	k4	17,6
MPa	MPa	k1gFnPc2	MPa
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
350	[number]	k4	350
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
běžný	běžný	k2eAgInSc1d1	běžný
provozní	provozní	k2eAgInSc1d1	provozní
tlak	tlak	k1gInSc1	tlak
je	být	k5eAaImIp3nS	být
15,7	[number]	k4	15,7
MPa	MPa	k1gFnPc2	MPa
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
290	[number]	k4	290
<g/>
–	–	k?	–
<g/>
320	[number]	k4	320
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
===	===	k?	===
Palivo	palivo	k1gNnSc1	palivo
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
palivo	palivo	k1gNnSc1	palivo
slouží	sloužit	k5eAaImIp3nS	sloužit
oxid	oxid	k1gInSc4	oxid
uraničitý	uraničitý	k2eAgMnSc1d1	uraničitý
UO2	UO2	k1gMnSc1	UO2
s	s	k7c7	s
průměrně	průměrně	k6eAd1	průměrně
4,25	[number]	k4	4,25
%	%	kIx~	%
obohaceného	obohacený	k2eAgInSc2d1	obohacený
uranu	uran	k1gInSc2	uran
235	[number]	k4	235
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reaktoru	reaktor	k1gInSc6	reaktor
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
92	[number]	k4	92
tun	tuna	k1gFnPc2	tuna
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgFnSc1d1	aktivní
zóna	zóna	k1gFnSc1	zóna
reaktoru	reaktor	k1gInSc2	reaktor
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
3530	[number]	k4	3530
mm	mm	kA	mm
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
3160	[number]	k4	3160
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
v	v	k7c6	v
přesně	přesně	k6eAd1	přesně
stanovených	stanovený	k2eAgFnPc6d1	stanovená
pozicích	pozice	k1gFnPc6	pozice
umístěno	umístit	k5eAaPmNgNnS	umístit
163	[number]	k4	163
palivových	palivový	k2eAgInPc2d1	palivový
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
61	[number]	k4	61
regulačních	regulační	k2eAgFnPc2d1	regulační
tyčí	tyč	k1gFnPc2	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Palivové	palivový	k2eAgInPc1d1	palivový
soubory	soubor	k1gInPc1	soubor
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
v	v	k7c6	v
hexagonální	hexagonální	k2eAgFnSc6d1	hexagonální
mříži	mříž	k1gFnSc6	mříž
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
palivový	palivový	k2eAgInSc1d1	palivový
soubor	soubor	k1gInSc1	soubor
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
312	[number]	k4	312
palivových	palivový	k2eAgInPc2d1	palivový
proutků	proutek	k1gInPc2	proutek
<g/>
,	,	kIx,	,
18	[number]	k4	18
vodicích	vodicí	k2eAgFnPc2d1	vodicí
trubek	trubka	k1gFnPc2	trubka
a	a	k8xC	a
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
centrální	centrální	k2eAgFnSc2d1	centrální
měřicí	měřicí	k2eAgFnSc2d1	měřicí
trubky	trubka	k1gFnSc2	trubka
<g/>
.	.	kIx.	.
</s>
<s>
Palivový	palivový	k2eAgInSc1d1	palivový
cyklus	cyklus	k1gInSc1	cyklus
je	být	k5eAaImIp3nS	být
čtyřletý	čtyřletý	k2eAgMnSc1d1	čtyřletý
(	(	kIx(	(
<g/>
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vyměňuje	vyměňovat	k5eAaImIp3nS	vyměňovat
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
paliva	palivo	k1gNnSc2	palivo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
přechod	přechod	k1gInSc4	přechod
na	na	k7c4	na
pětiletý	pětiletý	k2eAgInSc4d1	pětiletý
cyklus	cyklus	k1gInSc4	cyklus
výměny	výměna	k1gFnSc2	výměna
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
dodávala	dodávat	k5eAaImAgFnS	dodávat
palivo	palivo	k1gNnSc4	palivo
společnost	společnost	k1gFnSc1	společnost
Westinghouse	Westinghouse	k1gFnSc2	Westinghouse
Electric	Electrice	k1gFnPc2	Electrice
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
na	na	k7c6	na
základě	základ	k1gInSc6	základ
proběhlého	proběhlý	k2eAgNnSc2d1	proběhlé
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
používat	používat	k5eAaImF	používat
v	v	k7c6	v
temelínských	temelínský	k2eAgInPc6d1	temelínský
reaktorech	reaktor	k1gInPc6	reaktor
palivové	palivový	k2eAgInPc4d1	palivový
soubory	soubor	k1gInPc4	soubor
ruské	ruský	k2eAgFnSc2d1	ruská
společnosti	společnost	k1gFnSc2	společnost
TVEL	TVEL	kA	TVEL
<g/>
.	.	kIx.	.
</s>
<s>
Technologie	technologie	k1gFnSc1	technologie
originálních	originální	k2eAgInPc2d1	originální
ruských	ruský	k2eAgInPc2d1	ruský
palivových	palivový	k2eAgInPc2d1	palivový
prutů	prut	k1gInPc2	prut
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
vhodnější	vhodný	k2eAgFnSc1d2	vhodnější
a	a	k8xC	a
provozně	provozně	k6eAd1	provozně
kvalitnější	kvalitní	k2eAgMnSc1d2	kvalitnější
než	než	k8xS	než
palivo	palivo	k1gNnSc1	palivo
dodávané	dodávaný	k2eAgNnSc1d1	dodávané
firmou	firma	k1gFnSc7	firma
Westinghouse	Westinghouse	k1gFnSc2	Westinghouse
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Palivo	palivo	k1gNnSc1	palivo
tvořilo	tvořit	k5eAaImAgNnS	tvořit
podíl	podíl	k1gInSc4	podíl
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
%	%	kIx~	%
na	na	k7c6	na
ceně	cena	k1gFnSc6	cena
elektřiny	elektřina	k1gFnSc2	elektřina
z	z	k7c2	z
Jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
<g/>
,	,	kIx,	,
zvyšující	zvyšující	k2eAgFnSc1d1	zvyšující
cena	cena	k1gFnSc1	cena
uranu	uran	k1gInSc2	uran
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
trhu	trh	k1gInSc6	trh
tento	tento	k3xDgInSc4	tento
podíl	podíl	k1gInSc4	podíl
neovlivní	ovlivnit	k5eNaPmIp3nP	ovlivnit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
palivo	palivo	k1gNnSc1	palivo
je	být	k5eAaImIp3nS	být
dodáváno	dodávat	k5eAaImNgNnS	dodávat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dlouhodobých	dlouhodobý	k2eAgFnPc2d1	dlouhodobá
smluv	smlouva	k1gFnPc2	smlouva
(	(	kIx(	(
<g/>
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
ceny	cena	k1gFnSc2	cena
paliva	palivo	k1gNnSc2	palivo
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
daná	daný	k2eAgFnSc1d1	daná
jeho	jeho	k3xOp3gNnSc4	jeho
obohacením	obohacení	k1gNnSc7	obohacení
<g/>
,	,	kIx,	,
přípravou	příprava	k1gFnSc7	příprava
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
jako	jako	k8xS	jako
paliva	palivo	k1gNnPc4	palivo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Parogenerátory	parogenerátor	k1gInPc1	parogenerátor
a	a	k8xC	a
turbogenerátory	turbogenerátor	k1gInPc1	turbogenerátor
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
reaktoru	reaktor	k1gInSc3	reaktor
jsou	být	k5eAaImIp3nP	být
připojeny	připojen	k2eAgInPc1d1	připojen
čtyři	čtyři	k4xCgInPc1	čtyři
parogenerátory	parogenerátor	k1gInPc1	parogenerátor
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
páry	pára	k1gFnSc2	pára
278	[number]	k4	278
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
výkonu	výkon	k1gInSc2	výkon
1470	[number]	k4	1470
tun	tuna	k1gFnPc2	tuna
páry	pár	k1gInPc1	pár
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
přicházející	přicházející	k2eAgFnSc1d1	přicházející
do	do	k7c2	do
parogenerátoru	parogenerátor	k1gInSc2	parogenerátor
má	mít	k5eAaImIp3nS	mít
teplotu	teplota	k1gFnSc4	teplota
320	[number]	k4	320
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
parogenerátoru	parogenerátor	k1gInSc6	parogenerátor
pak	pak	k6eAd1	pak
předává	předávat	k5eAaImIp3nS	předávat
teplo	teplo	k6eAd1	teplo
jiné	jiný	k2eAgFnSc6d1	jiná
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
vodě	voda	k1gFnSc6	voda
sekundárního	sekundární	k2eAgInSc2d1	sekundární
okruhu	okruh	k1gInSc2	okruh
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
sytou	sytý	k2eAgFnSc4d1	sytá
páru	pára	k1gFnSc4	pára
určenou	určený	k2eAgFnSc4d1	určená
už	už	k6eAd1	už
pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
turbíny	turbína	k1gFnSc2	turbína
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spojeného	spojený	k2eAgInSc2d1	spojený
elektrického	elektrický	k2eAgInSc2d1	elektrický
generátoru	generátor	k1gInSc2	generátor
<g/>
.	.	kIx.	.
</s>
<s>
Vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
sytá	sytý	k2eAgFnSc1d1	sytá
pára	pára	k1gFnSc1	pára
má	mít	k5eAaImIp3nS	mít
teplotu	teplota	k1gFnSc4	teplota
278,5	[number]	k4	278,5
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
6,3	[number]	k4	6,3
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
turbogenerátoru	turbogenerátor	k1gInSc6	turbogenerátor
(	(	kIx(	(
<g/>
skládajícím	skládající	k2eAgInSc6d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
parní	parní	k2eAgFnSc2d1	parní
turbíny	turbína	k1gFnSc2	turbína
<g/>
,	,	kIx,	,
elektrického	elektrický	k2eAgInSc2d1	elektrický
generátoru	generátor	k1gInSc2	generátor
<g/>
,	,	kIx,	,
budiče	budič	k1gInSc2	budič
a	a	k8xC	a
pomocného	pomocný	k2eAgInSc2d1	pomocný
budiče	budič	k1gInSc2	budič
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
tepelná	tepelný	k2eAgFnSc1d1	tepelná
energie	energie	k1gFnSc1	energie
páry	pára	k1gFnSc2	pára
převádí	převádět	k5eAaImIp3nS	převádět
na	na	k7c4	na
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
energii	energie	k1gFnSc4	energie
pohybu	pohyb	k1gInSc2	pohyb
turbíny	turbína	k1gFnSc2	turbína
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
následně	následně	k6eAd1	následně
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Parogenerátory	parogenerátor	k1gInPc1	parogenerátor
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
14,8	[number]	k4	14,8
m	m	kA	m
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
vnější	vnější	k2eAgInSc1d1	vnější
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
4,2	[number]	k4	4,2
<g/>
-	-	kIx~	-
<g/>
4,5	[number]	k4	4,5
m.	m.	k?	m.
Dodala	dodat	k5eAaPmAgFnS	dodat
je	on	k3xPp3gNnSc4	on
společnost	společnost	k1gFnSc1	společnost
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
<g/>
.	.	kIx.	.
<g/>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
zařízení	zařízení	k1gNnPc1	zařízení
sekundárního	sekundární	k2eAgInSc2d1	sekundární
okruhu	okruh	k1gInSc2	okruh
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
strojovně	strojovna	k1gFnSc6	strojovna
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
zařízením	zařízení	k1gNnSc7	zařízení
je	být	k5eAaImIp3nS	být
turbogenerátor	turbogenerátor	k1gInSc4	turbogenerátor
1055	[number]	k4	1055
MW	MW	kA	MW
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
parní	parní	k2eAgFnSc2d1	parní
turbíny	turbína	k1gFnSc2	turbína
<g/>
,	,	kIx,	,
elektrického	elektrický	k2eAgInSc2d1	elektrický
generátoru	generátor	k1gInSc2	generátor
<g/>
,	,	kIx,	,
budiče	budič	k1gInSc2	budič
a	a	k8xC	a
pomocného	pomocný	k2eAgMnSc2d1	pomocný
budiče	budič	k1gMnSc2	budič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
elektřina	elektřina	k1gFnSc1	elektřina
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
po	po	k7c6	po
zvýšení	zvýšení	k1gNnSc6	zvýšení
napětí	napětí	k1gNnSc2	napětí
blokovými	blokový	k2eAgInPc7d1	blokový
transformátory	transformátor	k1gInPc7	transformátor
z	z	k7c2	z
24	[number]	k4	24
kV	kV	k?	kV
na	na	k7c4	na
400	[number]	k4	400
kV	kV	k?	kV
odváděna	odváděn	k2eAgFnSc1d1	odváděna
do	do	k7c2	do
rozvodny	rozvodna	k1gFnSc2	rozvodna
Kočín	Kočína	k1gFnPc2	Kočína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kontejnment	Kontejnment	k1gInSc4	Kontejnment
===	===	k?	===
</s>
</p>
<p>
<s>
Mohutná	mohutný	k2eAgFnSc1d1	mohutná
konstrukce	konstrukce	k1gFnSc1	konstrukce
kontejnmentu	kontejnment	k1gInSc2	kontejnment
chrání	chránit	k5eAaImIp3nS	chránit
reaktor	reaktor	k1gInSc1	reaktor
a	a	k8xC	a
primární	primární	k2eAgInSc1d1	primární
okruh	okruh	k1gInSc1	okruh
před	před	k7c7	před
vnějšími	vnější	k2eAgInPc7d1	vnější
vlivy	vliv	k1gInPc7	vliv
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
elektrárny	elektrárna	k1gFnSc2	elektrárna
před	před	k7c7	před
následky	následek	k1gInPc7	následek
případné	případný	k2eAgFnSc2d1	případná
havárie	havárie	k1gFnSc2	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
45	[number]	k4	45
m	m	kA	m
<g/>
,	,	kIx,	,
výšku	výška	k1gFnSc4	výška
38	[number]	k4	38
m	m	kA	m
a	a	k8xC	a
tloušťku	tloušťka	k1gFnSc4	tloušťka
stěn	stěna	k1gFnPc2	stěna
1,2	[number]	k4	1,2
m	m	kA	m
<g/>
,	,	kIx,	,
konstrukce	konstrukce	k1gFnSc1	konstrukce
kupole	kupole	k1gFnSc2	kupole
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
10	[number]	k4	10
cm	cm	kA	cm
slabší	slabý	k2eAgNnSc1d2	slabší
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
ocelová	ocelový	k2eAgFnSc1d1	ocelová
výstelka	výstelka	k1gFnSc1	výstelka
má	mít	k5eAaImIp3nS	mít
tloušťku	tloušťka	k1gFnSc4	tloušťka
8	[number]	k4	8
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
kontejnmentu	kontejnment	k1gInSc2	kontejnment
je	být	k5eAaImIp3nS	být
trvale	trvale	k6eAd1	trvale
udržován	udržovat	k5eAaImNgInS	udržovat
mírný	mírný	k2eAgInSc1d1	mírný
podtlak	podtlak	k1gInSc1	podtlak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
únik	únik	k1gInSc4	únik
menších	malý	k2eAgNnPc2d2	menší
množství	množství	k1gNnPc2	množství
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
<g/>
Uvnitř	uvnitř	k7c2	uvnitř
kontejnmentu	kontejnment	k1gInSc2	kontejnment
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
umístěny	umístěn	k2eAgInPc1d1	umístěn
bazény	bazén	k1gInPc1	bazén
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	s	k7c7	s
10	[number]	k4	10
let	léto	k1gNnPc2	léto
uskladňuje	uskladňovat	k5eAaImIp3nS	uskladňovat
použité	použitý	k2eAgNnSc4d1	Použité
jaderné	jaderný	k2eAgNnSc4d1	jaderné
palivo	palivo	k1gNnSc4	palivo
před	před	k7c7	před
umístěním	umístění	k1gNnSc7	umístění
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
meziskladu	mezisklad	k1gInSc2	mezisklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chlazení	chlazení	k1gNnSc6	chlazení
primárního	primární	k2eAgInSc2d1	primární
okruhu	okruh	k1gInSc2	okruh
===	===	k?	===
</s>
</p>
<p>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
objem	objem	k1gInSc1	objem
chladiva	chladivo	k1gNnSc2	chladivo
v	v	k7c6	v
primárním	primární	k2eAgInSc6d1	primární
okruhu	okruh	k1gInSc6	okruh
je	být	k5eAaImIp3nS	být
337	[number]	k4	337
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
chladiva	chladivo	k1gNnSc2	chladivo
na	na	k7c6	na
vstupu	vstup	k1gInSc6	vstup
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
290	[number]	k4	290
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
na	na	k7c6	na
výstupu	výstup	k1gInSc6	výstup
asi	asi	k9	asi
320	[number]	k4	320
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
====	====	k?	====
Voda	voda	k1gFnSc1	voda
pro	pro	k7c4	pro
chlazení	chlazení	k1gNnSc4	chlazení
====	====	k?	====
</s>
</p>
<p>
<s>
Voda	voda	k1gFnSc1	voda
potřebná	potřebný	k2eAgFnSc1d1	potřebná
k	k	k7c3	k
chlazení	chlazení	k1gNnSc3	chlazení
se	se	k3xPyFc4	se
odebírá	odebírat	k5eAaImIp3nS	odebírat
z	z	k7c2	z
vltavské	vltavský	k2eAgFnSc2d1	Vltavská
nádrže	nádrž	k1gFnSc2	nádrž
Hněvkovice	Hněvkovice	k1gFnSc2	Hněvkovice
a	a	k8xC	a
zpětně	zpětně	k6eAd1	zpětně
se	se	k3xPyFc4	se
vypouští	vypouštět	k5eAaImIp3nS	vypouštět
do	do	k7c2	do
Vltavy	Vltava	k1gFnSc2	Vltava
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
nádrže	nádrž	k1gFnSc2	nádrž
Kořensko	Kořensko	k1gNnSc1	Kořensko
<g/>
.	.	kIx.	.
</s>
<s>
Soustava	soustava	k1gFnSc1	soustava
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgNnPc2	dva
vodních	vodní	k2eAgNnPc2d1	vodní
děl	dělo	k1gNnPc2	dělo
se	se	k3xPyFc4	se
budovala	budovat	k5eAaImAgFnS	budovat
současně	současně	k6eAd1	současně
s	s	k7c7	s
elektrárnou	elektrárna	k1gFnSc7	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
za	za	k7c2	za
plného	plný	k2eAgInSc2d1	plný
provozu	provoz	k1gInSc2	provoz
spotřebovává	spotřebovávat	k5eAaImIp3nS	spotřebovávat
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
elektrárna	elektrárna	k1gFnSc1	elektrárna
spotřebovala	spotřebovat	k5eAaPmAgFnS	spotřebovat
35,4	[number]	k4	35,4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
3	[number]	k4	3
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Čerpadla	čerpadlo	k1gNnSc2	čerpadlo
====	====	k?	====
</s>
</p>
<p>
<s>
Primární	primární	k2eAgInSc1d1	primární
okruh	okruh	k1gInSc1	okruh
je	být	k5eAaImIp3nS	být
chlazen	chladit	k5eAaImNgInS	chladit
čtyřmi	čtyři	k4xCgFnPc7	čtyři
vertikálními	vertikální	k2eAgFnPc7d1	vertikální
odstředivými	odstředivý	k2eAgFnPc7d1	odstředivá
čerpadly	čerpadlo	k1gNnPc7	čerpadlo
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
5,1	[number]	k4	5,1
MW	MW	kA	MW
umístěnými	umístěný	k2eAgInPc7d1	umístěný
na	na	k7c6	na
studených	studený	k2eAgFnPc6d1	studená
větvích	větev	k1gFnPc6	větev
cirkulačních	cirkulační	k2eAgFnPc2d1	cirkulační
smyček	smyčka	k1gFnPc2	smyčka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nominálních	nominální	k2eAgInPc6d1	nominální
parametrech	parametr	k1gInPc6	parametr
činí	činit	k5eAaImIp3nS	činit
průtok	průtok	k1gInSc1	průtok
jedním	jeden	k4xCgInSc7	jeden
čerpadlem	čerpadlo	k1gNnSc7	čerpadlo
21	[number]	k4	21
200	[number]	k4	200
m	m	kA	m
<g/>
3	[number]	k4	3
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
čerpadel	čerpadlo	k1gNnPc2	čerpadlo
je	být	k5eAaImIp3nS	být
11,9	[number]	k4	11,9
m.	m.	k?	m.
</s>
</p>
<p>
<s>
====	====	k?	====
Chladicí	chladicí	k2eAgFnPc1d1	chladicí
věže	věž	k1gFnPc1	věž
====	====	k?	====
</s>
</p>
<p>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
chladicí	chladicí	k2eAgFnSc2d1	chladicí
věže	věž	k1gFnSc2	věž
typu	typ	k1gInSc2	typ
Iterson	Iterson	k1gInSc1	Iterson
vysoké	vysoká	k1gFnSc2	vysoká
154,8	[number]	k4	154,8
m.	m.	k?	m.
Jejich	jejich	k3xOp3gInSc4	jejich
průměr	průměr	k1gInSc4	průměr
u	u	k7c2	u
paty	pata	k1gFnSc2	pata
je	být	k5eAaImIp3nS	být
130,7	[number]	k4	130,7
m	m	kA	m
<g/>
,	,	kIx,	,
povrch	povrch	k1gInSc4	povrch
vnější	vnější	k2eAgFnSc2d1	vnější
stěny	stěna	k1gFnSc2	stěna
činí	činit	k5eAaImIp3nS	činit
44	[number]	k4	44
000	[number]	k4	000
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgInSc1d1	maximální
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
17,4	[number]	k4	17,4
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
s	s	k7c7	s
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Chladicí	chladicí	k2eAgFnPc1d1	chladicí
věže	věž	k1gFnPc1	věž
elektrárny	elektrárna	k1gFnSc2	elektrárna
tvoří	tvořit	k5eAaImIp3nP	tvořit
dominantu	dominanta	k1gFnSc4	dominanta
celého	celý	k2eAgNnSc2d1	celé
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vidět	vidět	k5eAaImF	vidět
ze	z	k7c2	z
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
40	[number]	k4	40
km	km	kA	km
<g/>
,	,	kIx,	,
pára	pára	k1gFnSc1	pára
nad	nad	k7c7	nad
nimi	on	k3xPp3gMnPc7	on
až	až	k9	až
ze	z	k7c2	z
70	[number]	k4	70
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vliv	vliv	k1gInSc1	vliv
chlazení	chlazení	k1gNnSc1	chlazení
na	na	k7c4	na
okolí	okolí	k1gNnSc4	okolí
====	====	k?	====
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
pára	pára	k1gFnSc1	pára
odcházející	odcházející	k2eAgFnSc1d1	odcházející
z	z	k7c2	z
chladicích	chladicí	k2eAgFnPc2d1	chladicí
věží	věžit	k5eAaImIp3nS	věžit
zdaleka	zdaleka	k6eAd1	zdaleka
viditelná	viditelný	k2eAgFnSc1d1	viditelná
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
skutečný	skutečný	k2eAgInSc4d1	skutečný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
hydrometeorologické	hydrometeorologický	k2eAgNnSc4d1	hydrometeorologický
okolí	okolí	k1gNnSc4	okolí
je	být	k5eAaImIp3nS	být
nepatrný	patrný	k2eNgMnSc1d1	nepatrný
–	–	k?	–
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
5	[number]	k4	5
km	km	kA	km
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
jen	jen	k9	jen
o	o	k7c4	o
0,02	[number]	k4	0,02
<g/>
–	–	k?	–
<g/>
0,06	[number]	k4	0,06
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
0,006	[number]	k4	0,006
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
vyšší	vysoký	k2eAgInPc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
mírný	mírný	k2eAgInSc1d1	mírný
nárůst	nárůst	k1gInSc1	nárůst
tzv.	tzv.	kA	tzv.
hydrometeorů	hydrometeor	k1gInPc2	hydrometeor
(	(	kIx(	(
<g/>
námraza	námraza	k1gFnSc1	námraza
<g/>
,	,	kIx,	,
mlha	mlha	k1gFnSc1	mlha
<g/>
,	,	kIx,	,
jinovatka	jinovatka	k1gFnSc1	jinovatka
<g/>
)	)	kIx)	)
v	v	k7c6	v
bezprostředním	bezprostřední	k2eAgNnSc6d1	bezprostřední
okolí	okolí	k1gNnSc6	okolí
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
,	,	kIx,	,
meteorologická	meteorologický	k2eAgNnPc1d1	meteorologické
měření	měření	k1gNnPc1	měření
však	však	k9	však
zatím	zatím	k6eAd1	zatím
neprobíhají	probíhat	k5eNaImIp3nP	probíhat
dostatečně	dostatečně	k6eAd1	dostatečně
dlouho	dlouho	k6eAd1	dlouho
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc1	ten
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
ostatní	ostatní	k2eAgInPc1d1	ostatní
(	(	kIx(	(
<g/>
neekonomické	ekonomický	k2eNgInPc1d1	neekonomický
<g/>
)	)	kIx)	)
vlivy	vliv	k1gInPc1	vliv
elektrárny	elektrárna	k1gFnSc2	elektrárna
na	na	k7c6	na
okolí	okolí	k1gNnSc6	okolí
jsou	být	k5eAaImIp3nP	být
nepatrné	nepatrný	k2eAgFnPc1d1	nepatrná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Využití	využití	k1gNnSc1	využití
odpadního	odpadní	k2eAgNnSc2d1	odpadní
tepla	teplo	k1gNnSc2	teplo
====	====	k?	====
</s>
</p>
<p>
<s>
Při	při	k7c6	při
plánování	plánování	k1gNnSc6	plánování
elektrárny	elektrárna	k1gFnSc2	elektrárna
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c6	o
řadě	řada	k1gFnSc6	řada
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
využít	využít	k5eAaPmF	využít
odpadní	odpadní	k2eAgNnSc4d1	odpadní
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
většinu	většina	k1gFnSc4	většina
celkového	celkový	k2eAgInSc2d1	celkový
výkonu	výkon	k1gInSc2	výkon
elektrárny	elektrárna	k1gFnSc2	elektrárna
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
bez	bez	k7c2	bez
užitku	užitek	k1gInSc2	užitek
vypouštěno	vypouštěn	k2eAgNnSc1d1	vypouštěno
<g/>
.	.	kIx.	.
</s>
<s>
Uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c4	o
využití	využití	k1gNnSc4	využití
tepla	teplo	k1gNnSc2	teplo
pro	pro	k7c4	pro
zemědělství	zemědělství	k1gNnSc4	zemědělství
a	a	k8xC	a
rybníkářství	rybníkářství	k1gNnSc4	rybníkářství
o	o	k7c4	o
vytápění	vytápění	k1gNnSc4	vytápění
řady	řada	k1gFnSc2	řada
okolních	okolní	k2eAgFnPc2d1	okolní
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
především	především	k9	především
části	část	k1gFnPc1	část
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
Temelína	Temelín	k1gInSc2	Temelín
však	však	k9	však
kvůli	kvůli	k7c3	kvůli
nízké	nízký	k2eAgFnSc3d1	nízká
hustotě	hustota	k1gFnSc3	hustota
osídlení	osídlení	k1gNnSc2	osídlení
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
odpadního	odpadní	k2eAgNnSc2d1	odpadní
tepla	teplo	k1gNnSc2	teplo
k	k	k7c3	k
vytápění	vytápění	k1gNnSc3	vytápění
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
byla	být	k5eAaImAgFnS	být
EGP	EGP	kA	EGP
Praha	Praha	k1gFnSc1	Praha
zpracována	zpracován	k2eAgFnSc1d1	zpracována
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
reálné	reálný	k2eAgNnSc1d1	reálné
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc1	využití
jen	jen	k9	jen
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
dosažitelného	dosažitelný	k2eAgInSc2d1	dosažitelný
výkonu	výkon	k1gInSc2	výkon
Temelína	Temelín	k1gInSc2	Temelín
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
vysokých	vysoký	k2eAgInPc2d1	vysoký
investičních	investiční	k2eAgInPc2d1	investiční
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
<g/>
Prozatím	prozatím	k6eAd1	prozatím
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
podařilo	podařit	k5eAaPmAgNnS	podařit
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
pouze	pouze	k6eAd1	pouze
vytápění	vytápění	k1gNnSc4	vytápění
Týna	Týn	k1gInSc2	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c4	o
využití	využití	k1gNnSc4	využití
odpadního	odpadní	k2eAgNnSc2d1	odpadní
tepla	teplo	k1gNnSc2	teplo
pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
ryb	ryba	k1gFnPc2	ryba
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Temelín	Temelín	k1gInSc1	Temelín
začínal	začínat	k5eAaImAgInS	začínat
stavět	stavět	k5eAaImF	stavět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
mj.	mj.	kA	mj.
o	o	k7c6	o
využití	využití	k1gNnSc6	využití
odpadního	odpadní	k2eAgNnSc2d1	odpadní
tepla	teplo	k1gNnSc2	teplo
pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
Státní	státní	k2eAgInSc1d1	státní
rybářství	rybářství	k1gNnSc3	rybářství
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
tři	tři	k4xCgInPc4	tři
varianty	variant	k1gInPc4	variant
využití	využití	k1gNnSc2	využití
odpadního	odpadní	k2eAgNnSc2d1	odpadní
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
======	======	k?	======
Plány	plán	k1gInPc1	plán
na	na	k7c4	na
chov	chov	k1gInSc4	chov
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
varianta	varianta	k1gFnSc1	varianta
======	======	k?	======
</s>
</p>
<p>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
variantě	varianta	k1gFnSc3	varianta
se	se	k3xPyFc4	se
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
výstavba	výstavba	k1gFnSc1	výstavba
teplovodu	teplovod	k1gInSc2	teplovod
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
obce	obec	k1gFnSc2	obec
Lhota	Lhota	k1gFnSc1	Lhota
pod	pod	k7c7	pod
Horami	hora	k1gFnPc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
by	by	kYmCp3nS	by
voda	voda	k1gFnSc1	voda
teplá	teplý	k2eAgFnSc1d1	teplá
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
dodávaný	dodávaný	k2eAgInSc1d1	dodávaný
výkon	výkon	k1gInSc1	výkon
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
MW	MW	kA	MW
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
předávala	předávat	k5eAaImAgFnS	předávat
pomocí	pomocí	k7c2	pomocí
výměníků	výměník	k1gInPc2	výměník
teplo	teplo	k6eAd1	teplo
rybolovnému	rybolovný	k2eAgNnSc3d1	rybolovné
zařízení	zařízení	k1gNnSc3	zařízení
skládajícímu	skládající	k2eAgInSc3d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
haly	hala	k1gFnSc2	hala
(	(	kIx(	(
<g/>
800	[number]	k4	800
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
i	i	k9	i
z	z	k7c2	z
venkovních	venkovní	k2eAgInPc2d1	venkovní
zemních	zemní	k2eAgInPc2d1	zemní
rybníků	rybník	k1gInPc2	rybník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
hale	hala	k1gFnSc6	hala
by	by	kYmCp3nS	by
probíhal	probíhat	k5eAaImAgInS	probíhat
výkrm	výkrm	k1gInSc1	výkrm
tilápie	tilápie	k1gFnSc2	tilápie
nilské	nilský	k2eAgFnSc2d1	nilská
a	a	k8xC	a
sumečka	sumeček	k1gMnSc2	sumeček
afrického	africký	k2eAgMnSc2d1	africký
<g/>
,	,	kIx,	,
v	v	k7c6	v
rybnících	rybník	k1gInPc6	rybník
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
odchov	odchov	k1gInSc4	odchov
rychleného	rychlený	k2eAgInSc2d1	rychlený
plůdku	plůdek	k1gInSc2	plůdek
býložravých	býložravý	k2eAgFnPc2d1	býložravá
ryb	ryba	k1gFnPc2	ryba
následovaný	následovaný	k2eAgInSc1d1	následovaný
dokrmem	dokrm	k1gInSc7	dokrm
tilápie	tilápie	k1gFnSc2	tilápie
nebo	nebo	k8xC	nebo
sumečka	sumeček	k1gMnSc4	sumeček
do	do	k7c2	do
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
Dál	daleko	k6eAd2	daleko
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
odchovávány	odchováván	k2eAgFnPc1d1	odchovávána
jikry	jikra	k1gFnPc1	jikra
pstruha	pstruh	k1gMnSc2	pstruh
duhového	duhový	k2eAgMnSc2d1	duhový
a	a	k8xC	a
půlročka	půlročka	k1gFnSc1	půlročka
<g/>
.	.	kIx.	.
</s>
<s>
Výnos	výnos	k1gInSc1	výnos
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
50	[number]	k4	50
t	t	k?	t
ryb	ryba	k1gFnPc2	ryba
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
a	a	k8xC	a
50	[number]	k4	50
t	t	k?	t
v	v	k7c6	v
rybnících	rybník	k1gInPc6	rybník
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
s	s	k7c7	s
možným	možný	k2eAgNnSc7d1	možné
zvýšením	zvýšení	k1gNnSc7	zvýšení
produkce	produkce	k1gFnSc2	produkce
ještě	ještě	k9	ještě
o	o	k7c4	o
dalších	další	k2eAgInPc2d1	další
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
t.	t.	k?	t.
Rozpočtové	rozpočtový	k2eAgInPc1d1	rozpočtový
náklady	náklad	k1gInPc1	náklad
měly	mít	k5eAaImAgInP	mít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
40	[number]	k4	40
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
Kčs	Kčs	kA	Kčs
<g/>
,	,	kIx,	,
doba	doba	k1gFnSc1	doba
návratnosti	návratnost	k1gFnSc2	návratnost
investice	investice	k1gFnSc1	investice
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
13	[number]	k4	13
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
při	při	k7c6	při
produkci	produkce	k1gFnSc6	produkce
rychleného	rychlený	k2eAgInSc2d1	rychlený
plůdku	plůdek	k1gInSc2	plůdek
býložravých	býložravý	k2eAgFnPc2d1	býložravá
ryb	ryba	k1gFnPc2	ryba
dokonce	dokonce	k9	dokonce
jen	jen	k9	jen
8	[number]	k4	8
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
======	======	k?	======
Plány	plán	k1gInPc1	plán
na	na	k7c4	na
chov	chov	k1gInSc4	chov
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
varianta	varianta	k1gFnSc1	varianta
======	======	k?	======
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
variantě	varianta	k1gFnSc3	varianta
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
dovedení	dovedení	k1gNnSc3	dovedení
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
Vltavy	Vltava	k1gFnSc2	Vltava
do	do	k7c2	do
Dehtářského	Dehtářský	k2eAgInSc2d1	Dehtářský
a	a	k8xC	a
Soudného	soudný	k2eAgInSc2d1	soudný
potoka	potok	k1gInSc2	potok
buďto	buďto	k8xC	buďto
pomocí	pomocí	k7c2	pomocí
štoly	štola	k1gFnSc2	štola
(	(	kIx(	(
<g/>
Dívčí	dívčí	k2eAgInSc1d1	dívčí
kámen	kámen	k1gInSc1	kámen
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
čerpáním	čerpání	k1gNnSc7	čerpání
z	z	k7c2	z
Boršova	Boršův	k2eAgInSc2d1	Boršův
a	a	k8xC	a
touto	tento	k3xDgFnSc7	tento
vodou	voda	k1gFnSc7	voda
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
napájena	napájen	k2eAgFnSc1d1	napájena
soustava	soustava	k1gFnSc1	soustava
rybníku	rybník	k1gInSc3	rybník
Blatec	Blatec	k1gInSc4	Blatec
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
objekty	objekt	k1gInPc4	objekt
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
vesnic	vesnice	k1gFnPc2	vesnice
Dívčice	dívčice	k1gFnSc2	dívčice
<g/>
,	,	kIx,	,
Dubenec	Dubenec	k1gMnSc1	Dubenec
a	a	k8xC	a
Nákří	Nákří	k2eAgMnSc1d1	Nákří
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
varianta	varianta	k1gFnSc1	varianta
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
velké	velký	k2eAgInPc4d1	velký
náklady	náklad	k1gInPc4	náklad
<g/>
:	:	kIx,	:
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
250	[number]	k4	250
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
rybník	rybník	k1gInSc4	rybník
Dehtář	dehtář	k1gMnSc1	dehtář
<g/>
,	,	kIx,	,
50	[number]	k4	50
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
zajištění	zajištění	k1gNnSc4	zajištění
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
Blatec	Blatec	k1gInSc4	Blatec
u	u	k7c2	u
Dívčic	dívčice	k1gFnPc2	dívčice
–	–	k?	–
Bezdrev	Bezdrev	k1gInSc1	Bezdrev
<g/>
,	,	kIx,	,
výstavba	výstavba	k1gFnSc1	výstavba
modulů	modul	k1gInPc2	modul
základního	základní	k2eAgInSc2d1	základní
objektu	objekt	k1gInSc2	objekt
mohla	moct	k5eAaImAgFnS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
250	[number]	k4	250
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
,	,	kIx,	,
přívod	přívod	k1gInSc4	přívod
tepla	teplo	k1gNnSc2	teplo
550	[number]	k4	550
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
tedy	tedy	k9	tedy
mohla	moct	k5eAaImAgFnS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
celkem	celkem	k6eAd1	celkem
1,1	[number]	k4	1,1
mld.	mld.	k?	mld.
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
zajištěno	zajištěn	k2eAgNnSc1d1	zajištěno
využití	využití	k1gNnSc1	využití
2500	[number]	k4	2500
ha	ha	kA	ha
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
OZ	OZ	kA	OZ
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
a	a	k8xC	a
využití	využití	k1gNnSc1	využití
nízkopotenciálního	nízkopotenciální	k2eAgNnSc2d1	nízkopotenciální
tepla	teplo	k1gNnSc2	teplo
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
75	[number]	k4	75
MW	MW	kA	MW
<g/>
,	,	kIx,	,
i	i	k9	i
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Výnos	výnos	k1gInSc1	výnos
tak	tak	k9	tak
údajně	údajně	k6eAd1	údajně
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
zvýšen	zvýšit	k5eAaPmNgInS	zvýšit
o	o	k7c6	o
1000	[number]	k4	1000
t	t	k?	t
(	(	kIx(	(
<g/>
o	o	k7c4	o
0,25	[number]	k4	0,25
–	–	k?	–
0,4	[number]	k4	0,4
t	t	k?	t
<g/>
/	/	kIx~	/
<g/>
ha	ha	kA	ha
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
25	[number]	k4	25
000	[number]	k4	000
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
akvakultuře	akvakultura	k1gFnSc6	akvakultura
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
produkce	produkce	k1gFnSc1	produkce
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
o	o	k7c4	o
dalších	další	k2eAgInPc2d1	další
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
600	[number]	k4	600
t	t	k?	t
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
návratnost	návratnost	k1gFnSc1	návratnost
investic	investice	k1gFnPc2	investice
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgInPc1d1	další
podstatné	podstatný	k2eAgInPc1d1	podstatný
přínosy	přínos	k1gInPc1	přínos
mohly	moct	k5eAaImAgInP	moct
plynout	plynout	k5eAaImF	plynout
ze	z	k7c2	z
závlah	závlaha	k1gFnPc2	závlaha
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
======	======	k?	======
Plány	plán	k1gInPc1	plán
na	na	k7c4	na
chov	chov	k1gInSc4	chov
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
varianta	varianta	k1gFnSc1	varianta
======	======	k?	======
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
3	[number]	k4	3
<g/>
.	.	kIx.	.
varianty	varianta	k1gFnSc2	varianta
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
teplo	teplo	k1gNnSc4	teplo
z	z	k7c2	z
Temelína	Temelín	k1gInSc2	Temelín
přivedeno	přiveden	k2eAgNnSc1d1	přivedeno
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Hluboké	Hluboká	k1gFnSc2	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
–	–	k?	–
Hamr	hamr	k1gInSc4	hamr
<g/>
,	,	kIx,	,
plocha	plocha	k1gFnSc1	plocha
pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
ryb	ryba	k1gFnPc2	ryba
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
utvořena	utvořit	k5eAaPmNgFnS	utvořit
upravením	upravení	k1gNnSc7	upravení
koryta	koryto	k1gNnSc2	koryto
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Chovány	chovat	k5eAaImNgFnP	chovat
by	by	kYmCp3nP	by
tak	tak	k9	tak
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
i	i	k9	i
lososovité	lososovitý	k2eAgFnPc4d1	lososovitá
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
větev	větev	k1gFnSc1	větev
by	by	kYmCp3nS	by
vyhřívala	vyhřívat	k5eAaImAgFnS	vyhřívat
sádky	sádka	k1gFnPc4	sádka
OZ	OZ	kA	OZ
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
tato	tento	k3xDgFnSc1	tento
varianta	varianta	k1gFnSc1	varianta
měla	mít	k5eAaImAgFnS	mít
přinést	přinést	k5eAaPmF	přinést
výnos	výnos	k1gInSc4	výnos
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
150	[number]	k4	150
t	t	k?	t
pstruha	pstruh	k1gMnSc4	pstruh
duhového	duhový	k2eAgMnSc4d1	duhový
<g/>
,	,	kIx,	,
200	[number]	k4	200
t	t	k?	t
tilápie	tilápie	k1gFnSc2	tilápie
nilské	nilský	k2eAgFnPc1d1	nilská
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
t	t	k?	t
sumečka	sumeček	k1gMnSc4	sumeček
afrického	africký	k2eAgMnSc4d1	africký
<g/>
,	,	kIx,	,
býložravých	býložravý	k2eAgInPc2d1	býložravý
a	a	k8xC	a
říčních	říční	k2eAgInPc2d1	říční
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
raná	raný	k2eAgNnPc1d1	rané
stadia	stadion	k1gNnPc1	stadion
kapra	kapr	k1gMnSc2	kapr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Náklady	náklad	k1gInPc1	náklad
3	[number]	k4	3
<g/>
.	.	kIx.	.
varianty	varianta	k1gFnPc1	varianta
se	se	k3xPyFc4	se
předběžně	předběžně	k6eAd1	předběžně
odhadovaly	odhadovat	k5eAaImAgFnP	odhadovat
na	na	k7c4	na
125	[number]	k4	125
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
na	na	k7c4	na
přívod	přívod	k1gInSc4	přívod
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
80	[number]	k4	80
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
pro	pro	k7c4	pro
rybochovný	rybochovný	k2eAgInSc4d1	rybochovný
objekt	objekt	k1gInSc4	objekt
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
–	–	k?	–
Hamr	hamr	k1gInSc4	hamr
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
výměníkový	výměníkový	k2eAgInSc4d1	výměníkový
systém	systém	k1gInSc4	systém
v	v	k7c6	v
sádkách	sádka	k1gFnPc6	sádka
20	[number]	k4	20
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
tedy	tedy	k9	tedy
cca	cca	kA	cca
225	[number]	k4	225
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
návratnosti	návratnost	k1gFnSc2	návratnost
se	se	k3xPyFc4	se
odhadovala	odhadovat	k5eAaImAgFnS	odhadovat
na	na	k7c4	na
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
let	léto	k1gNnPc2	léto
bez	bez	k7c2	bez
započítání	započítání	k1gNnSc2	započítání
ekologických	ekologický	k2eAgInPc2d1	ekologický
přínosů	přínos	k1gInPc2	přínos
zarybňováním	zarybňování	k1gNnSc7	zarybňování
Hněvkovické	Hněvkovický	k2eAgFnSc2d1	Hněvkovická
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kritika	kritika	k1gFnSc1	kritika
obecně	obecně	k6eAd1	obecně
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Kritika	kritika	k1gFnSc1	kritika
nedostatečného	dostatečný	k2eNgNnSc2d1	nedostatečné
pojištění	pojištění	k1gNnSc2	pojištění
====	====	k?	====
</s>
</p>
<p>
<s>
Elektrárny	elektrárna	k1gFnSc2	elektrárna
Dukovany	Dukovany	k1gInPc1	Dukovany
a	a	k8xC	a
Temelín	Temelín	k1gInSc1	Temelín
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
pojištěny	pojistit	k5eAaPmNgInP	pojistit
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
havárie	havárie	k1gFnSc1	havárie
<g/>
,	,	kIx,	,
limit	limit	k1gInSc1	limit
plnění	plnění	k1gNnSc2	plnění
je	být	k5eAaImIp3nS	být
však	však	k9	však
pouze	pouze	k6eAd1	pouze
6	[number]	k4	6
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
například	například	k6eAd1	například
jen	jen	k9	jen
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
odškodnění	odškodnění	k1gNnSc4	odškodnění
obyvatel	obyvatel	k1gMnPc2	obyvatel
evakuovaných	evakuovaný	k2eAgMnPc2d1	evakuovaný
při	při	k7c6	při
havárii	havárie	k1gFnSc6	havárie
ve	v	k7c6	v
Fukušimě	Fukušima	k1gFnSc6	Fukušima
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
na	na	k7c4	na
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
1,8	[number]	k4	1,8
bilionu	bilion	k4xCgInSc2	bilion
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
600	[number]	k4	600
násobek	násobek	k1gInSc4	násobek
částky	částka	k1gFnSc2	částka
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
jsou	být	k5eAaImIp3nP	být
pojištěny	pojistit	k5eAaPmNgFnP	pojistit
obě	dva	k4xCgFnPc1	dva
české	český	k2eAgFnPc1d1	Česká
jaderné	jaderný	k2eAgFnPc1d1	jaderná
elektrárny	elektrárna	k1gFnPc1	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
problémem	problém	k1gInSc7	problém
podpojištění	podpojištění	k1gNnSc2	podpojištění
však	však	k9	však
na	na	k7c6	na
světě	svět	k1gInSc6	svět
trpí	trpět	k5eAaImIp3nP	trpět
i	i	k9	i
mnohé	mnohý	k2eAgInPc4d1	mnohý
jiné	jiný	k2eAgInPc4d1	jiný
velké	velký	k2eAgInPc4d1	velký
energetické	energetický	k2eAgInPc4d1	energetický
a	a	k8xC	a
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
komplexy	komplex	k1gInPc4	komplex
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Kritika	kritika	k1gFnSc1	kritika
opírající	opírající	k2eAgFnSc1d1	opírající
se	se	k3xPyFc4	se
o	o	k7c4	o
vývoz	vývoz	k1gInSc4	vývoz
elektřiny	elektřina	k1gFnSc2	elektřina
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
a	a	k8xC	a
neodstavování	neodstavování	k1gNnSc2	neodstavování
uhelných	uhelný	k2eAgFnPc2d1	uhelná
elektráren	elektrárna	k1gFnPc2	elektrárna
====	====	k?	====
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
předmětů	předmět	k1gInPc2	předmět
kritiky	kritika	k1gFnSc2	kritika
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Česko	Česko	k1gNnSc1	Česko
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
víc	hodně	k6eAd2	hodně
elektřiny	elektřina	k1gFnSc2	elektřina
než	než	k8xS	než
jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
vyprodukuje	vyprodukovat	k5eAaPmIp3nS	vyprodukovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
důvodů	důvod	k1gInPc2	důvod
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
Temelína	Temelín	k1gInSc2	Temelín
byla	být	k5eAaImAgFnS	být
energetická	energetický	k2eAgFnSc1d1	energetická
soběstačnost	soběstačnost	k1gFnSc1	soběstačnost
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
scénářů	scénář	k1gInPc2	scénář
hovořilo	hovořit	k5eAaImAgNnS	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
ČR	ČR	kA	ČR
bez	bez	k7c2	bez
další	další	k2eAgFnSc2d1	další
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
neobejde	obejde	k6eNd1	obejde
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
scénáře	scénář	k1gInPc1	scénář
se	se	k3xPyFc4	se
nenaplnily	naplnit	k5eNaPmAgInP	naplnit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
dnes	dnes	k6eAd1	dnes
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
18	[number]	k4	18
let	léto	k1gNnPc2	léto
po	po	k7c6	po
spuštění	spuštění	k1gNnSc6	spuštění
Temelína	Temelín	k1gInSc2	Temelín
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
vyváží	vyvážet	k5eAaImIp3nS	vyvážet
zhruba	zhruba	k6eAd1	zhruba
tolik	tolik	k4xDc4	tolik
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
Temelín	Temelín	k1gInSc1	Temelín
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
stavba	stavba	k1gFnSc1	stavba
dalších	další	k2eAgInPc2d1	další
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kritika	kritika	k1gFnSc1	kritika
však	však	k9	však
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
strukturu	struktura	k1gFnSc4	struktura
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
vývozu	vývoz	k1gInSc2	vývoz
–	–	k?	–
jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Temelína	Temelín	k1gInSc2	Temelín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
typický	typický	k2eAgInSc1d1	typický
zdroj	zdroj	k1gInSc1	zdroj
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
vykrývání	vykrývání	k1gNnSc4	vykrývání
základního	základní	k2eAgNnSc2d1	základní
zatížení	zatížení	k1gNnSc2	zatížení
soustavy	soustava	k1gFnSc2	soustava
zásobování	zásobování	k1gNnSc2	zásobování
elektřinou	elektřina	k1gFnSc7	elektřina
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
spotřeby	spotřeba	k1gFnSc2	spotřeba
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zdroj	zdroj	k1gInSc4	zdroj
s	s	k7c7	s
nízkými	nízký	k2eAgInPc7d1	nízký
provozními	provozní	k2eAgInPc7d1	provozní
náklady	náklad	k1gInPc7	náklad
<g/>
)	)	kIx)	)
snaha	snaha	k1gFnSc1	snaha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
elektrárna	elektrárna	k1gFnSc1	elektrárna
pracovala	pracovat	k5eAaImAgFnS	pracovat
co	co	k9	co
nejdéle	dlouho	k6eAd3	dlouho
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
i	i	k8xC	i
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Zdroje	zdroj	k1gInPc1	zdroj
(	(	kIx(	(
<g/>
především	především	k9	především
uhelné	uhelný	k2eAgFnSc2d1	uhelná
a	a	k8xC	a
plynové	plynový	k2eAgFnSc2d1	plynová
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
)	)	kIx)	)
s	s	k7c7	s
dražším	drahý	k2eAgInSc7d2	dražší
provozem	provoz	k1gInSc7	provoz
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
nasazovány	nasazován	k2eAgInPc1d1	nasazován
jen	jen	k9	jen
pro	pro	k7c4	pro
vykrytí	vykrytí	k1gNnSc4	vykrytí
špiček	špička	k1gFnPc2	špička
spotřeby	spotřeba	k1gFnSc2	spotřeba
elektřiny	elektřina	k1gFnSc2	elektřina
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
elektřina	elektřina	k1gFnSc1	elektřina
dražší	drahý	k2eAgFnSc1d2	dražší
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
však	však	k9	však
byla	být	k5eAaImAgFnS	být
cena	cena	k1gFnSc1	cena
elektřiny	elektřina	k1gFnSc2	elektřina
dostatečně	dostatečně	k6eAd1	dostatečně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyplatilo	vyplatit	k5eAaPmAgNnS	vyplatit
jejich	jejich	k3xOp3gFnSc4	jejich
produkci	produkce	k1gFnSc4	produkce
vyvážet	vyvážet	k5eAaImF	vyvážet
i	i	k9	i
mimo	mimo	k7c4	mimo
špičkový	špičkový	k2eAgInSc4d1	špičkový
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
ovšem	ovšem	k9	ovšem
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
cena	cena	k1gFnSc1	cena
elektřiny	elektřina	k1gFnSc2	elektřina
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
burze	burza	k1gFnSc6	burza
pod	pod	k7c4	pod
40	[number]	k4	40
EUR	euro	k1gNnPc2	euro
<g/>
/	/	kIx~	/
<g/>
MWh	MWh	k1gFnPc1	MWh
a	a	k8xC	a
mimošpičkové	mimošpičkový	k2eAgFnPc1d1	mimošpičkový
ceny	cena	k1gFnPc1	cena
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgMnSc1d2	nižší
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
efektivitu	efektivita	k1gFnSc4	efektivita
vývozu	vývoz	k1gInSc2	vývoz
z	z	k7c2	z
Temelína	Temelín	k1gInSc2	Temelín
výrazně	výrazně	k6eAd1	výrazně
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
i	i	k9	i
diskuse	diskuse	k1gFnPc1	diskuse
o	o	k7c6	o
dostavbě	dostavba	k1gFnSc6	dostavba
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
bloku	blok	k1gInSc2	blok
elektrárny	elektrárna	k1gFnSc2	elektrárna
poněkud	poněkud	k6eAd1	poněkud
utichly	utichnout	k5eAaPmAgFnP	utichnout
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
vývoz	vývoz	k1gInSc1	vývoz
elektřiny	elektřina	k1gFnSc2	elektřina
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
zakázán	zakázán	k2eAgMnSc1d1	zakázán
či	či	k8xC	či
omezen	omezen	k2eAgInSc1d1	omezen
<g/>
,	,	kIx,	,
výrobu	výroba	k1gFnSc4	výroba
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
hlediska	hledisko	k1gNnSc2	hledisko
výhodné	výhodný	k2eAgNnSc1d1	výhodné
snížit	snížit	k5eAaPmF	snížit
u	u	k7c2	u
uhelných	uhelný	k2eAgFnPc2d1	uhelná
a	a	k8xC	a
plynových	plynový	k2eAgFnPc2d1	plynová
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Srovnání	srovnání	k1gNnSc1	srovnání
vývozu	vývoz	k1gInSc2	vývoz
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
výroby	výroba	k1gFnSc2	výroba
v	v	k7c6	v
Jaderné	jaderný	k2eAgFnSc6d1	jaderná
elektrárně	elektrárna	k1gFnSc6	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tučně	tučně	k6eAd1	tučně
jsou	být	k5eAaImIp3nP	být
označena	označit	k5eAaPmNgNnP	označit
léta	léto	k1gNnPc1	léto
s	s	k7c7	s
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
rekordní	rekordní	k2eAgInSc4d1	rekordní
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
2000	[number]	k4	2000
ČR	ČR	kA	ČR
exportovala	exportovat	k5eAaBmAgFnS	exportovat
poměrně	poměrně	k6eAd1	poměrně
malá	malý	k2eAgNnPc4d1	malé
množství	množství	k1gNnPc4	množství
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
Temelín	Temelín	k1gInSc1	Temelín
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
jen	jen	k9	jen
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
)	)	kIx)	)
Česko	Česko	k1gNnSc1	Česko
vyváželo	vyvážet	k5eAaImAgNnS	vyvážet
přes	přes	k7c4	přes
10	[number]	k4	10
TWh	TWh	k1gFnPc2	TWh
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
cca	cca	kA	cca
2	[number]	k4	2
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
Temelín	Temelín	k1gInSc1	Temelín
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
přes	přes	k7c4	přes
12,6	[number]	k4	12,6
TWh	TWh	k1gFnPc2	TWh
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc4d1	podobný
skoky	skok	k1gInPc4	skok
v	v	k7c6	v
exportu	export	k1gInSc6	export
nastaly	nastat	k5eAaPmAgInP	nastat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
a	a	k8xC	a
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Vývoz	vývoz	k1gInSc1	vývoz
elektřiny	elektřina	k1gFnSc2	elektřina
společnosti	společnost	k1gFnSc2	společnost
ČEZ	ČEZ	kA	ČEZ
(	(	kIx(	(
<g/>
té	ten	k3xDgFnSc3	ten
Temelín	Temelín	k1gInSc1	Temelín
patří	patřit	k5eAaImIp3nS	patřit
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgMnS	mít
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
spíš	spíš	k9	spíš
klesající	klesající	k2eAgFnSc4d1	klesající
tendenci	tendence	k1gFnSc4	tendence
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ČEZ	ČEZ	kA	ČEZ
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
pokrytí	pokrytí	k1gNnSc4	pokrytí
českého	český	k2eAgInSc2d1	český
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
<g/>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zvyšováním	zvyšování	k1gNnSc7	zvyšování
spotřeby	spotřeba	k1gFnSc2	spotřeba
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
dožíváním	dožívání	k1gNnSc7	dožívání
starších	starý	k2eAgFnPc2d2	starší
elektráren	elektrárna	k1gFnPc2	elektrárna
očekávalo	očekávat	k5eAaImAgNnS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Česko	Česko	k1gNnSc1	Česko
přestane	přestat	k5eAaPmIp3nS	přestat
být	být	k5eAaImF	být
vývozcem	vývozce	k1gMnSc7	vývozce
elektřiny	elektřina	k1gFnSc2	elektřina
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
příprava	příprava	k1gFnSc1	příprava
a	a	k8xC	a
výstavba	výstavba	k1gFnSc1	výstavba
nových	nový	k2eAgInPc2d1	nový
zdrojů	zdroj	k1gInPc2	zdroj
je	být	k5eAaImIp3nS	být
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
panovaly	panovat	k5eAaImAgFnP	panovat
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrozí	hrozit	k5eAaImIp3nS	hrozit
v	v	k7c6	v
krátkodobé	krátkodobý	k2eAgFnSc6d1	krátkodobá
a	a	k8xC	a
střednědobé	střednědobý	k2eAgFnSc3d1	střednědobá
budoucnosti	budoucnost	k1gFnSc3	budoucnost
nedostatek	nedostatek	k1gInSc4	nedostatek
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
další	další	k2eAgNnSc4d1	další
zdražování	zdražování	k1gNnSc4	zdražování
<g/>
.	.	kIx.	.
<g/>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyly	být	k5eNaImAgFnP	být
odstaveny	odstaven	k2eAgFnPc1d1	odstavena
uhelné	uhelný	k2eAgFnPc1d1	uhelná
elektrárny	elektrárna	k1gFnPc1	elektrárna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
měl	mít	k5eAaImAgInS	mít
Temelín	Temelín	k1gInSc1	Temelín
nahradit	nahradit	k5eAaPmF	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991	[number]	k4	1991
až	až	k9	až
1998	[number]	k4	1998
ČEZ	ČEZ	kA	ČEZ
odstavil	odstavit	k5eAaPmAgInS	odstavit
2020	[number]	k4	2020
MW	MW	kA	MW
uhelných	uhelný	k2eAgFnPc2d1	uhelná
elektráren	elektrárna	k1gFnPc2	elektrárna
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
odsířování	odsířování	k1gNnSc2	odsířování
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nevyplatilo	vyplatit	k5eNaPmAgNnS	vyplatit
je	on	k3xPp3gFnPc4	on
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
by	by	kYmCp3nP	by
tedy	tedy	k9	tedy
odstaveny	odstaven	k2eAgFnPc1d1	odstavena
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Temelín	Temelín	k1gInSc1	Temelín
nebyl	být	k5eNaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
<g/>
;	;	kIx,	;
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
ČEZ	ČEZ	kA	ČEZ
zahájil	zahájit	k5eAaPmAgMnS	zahájit
obnovu	obnova	k1gFnSc4	obnova
(	(	kIx(	(
<g/>
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
výstavbu	výstavba	k1gFnSc4	výstavba
nových	nový	k2eAgFnPc2d1	nová
elektráren	elektrárna	k1gFnPc2	elektrárna
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
starých	stará	k1gFnPc2	stará
<g/>
)	)	kIx)	)
některých	některý	k3yIgFnPc2	některý
uhelných	uhelný	k2eAgFnPc2d1	uhelná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
v	v	k7c6	v
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
době	doba	k1gFnSc6	doba
skončí	skončit	k5eAaPmIp3nS	skončit
životnost	životnost	k1gFnSc1	životnost
<g/>
,	,	kIx,	,
zvažována	zvažovat	k5eAaImNgFnS	zvažovat
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
stavba	stavba	k1gFnSc1	stavba
paroplynových	paroplynový	k2eAgFnPc2d1	paroplynová
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgFnP	mít
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
plánu	plán	k1gInSc2	plán
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
být	být	k5eAaImF	být
zcela	zcela	k6eAd1	zcela
odstaveny	odstaven	k2eAgFnPc4d1	odstavena
elektrárny	elektrárna	k1gFnPc4	elektrárna
Prunéřov	Prunéřovo	k1gNnPc2	Prunéřovo
I	i	k8xC	i
a	a	k8xC	a
Mělník	Mělník	k1gInSc1	Mělník
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Další	další	k2eAgFnSc1d1	další
kritika	kritika	k1gFnSc1	kritika
<g/>
,	,	kIx,	,
veřejné	veřejný	k2eAgNnSc1d1	veřejné
mínění	mínění	k1gNnSc1	mínění
====	====	k?	====
</s>
</p>
<p>
<s>
Kritici	kritik	k1gMnPc1	kritik
dál	daleko	k6eAd2	daleko
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
na	na	k7c4	na
údajné	údajný	k2eAgInPc4d1	údajný
technické	technický	k2eAgInPc4d1	technický
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
názoru	názor	k1gInSc2	názor
z	z	k7c2	z
nedostatečně	dostatečně	k6eNd1	dostatečně
odzkoušené	odzkoušený	k2eAgFnSc2d1	odzkoušená
kombinace	kombinace	k1gFnSc2	kombinace
sovětských	sovětský	k2eAgMnPc2d1	sovětský
a	a	k8xC	a
"	"	kIx"	"
<g/>
západních	západní	k2eAgFnPc2d1	západní
<g/>
"	"	kIx"	"
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
elektrárny	elektrárna	k1gFnSc2	elektrárna
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
kombinace	kombinace	k1gFnSc1	kombinace
už	už	k6eAd1	už
byla	být	k5eAaImAgFnS	být
vyzkoušena	vyzkoušet	k5eAaPmNgFnS	vyzkoušet
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
elektrárně	elektrárna	k1gFnSc6	elektrárna
Loviisa	Loviisa	k1gFnSc1	Loviisa
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
podle	podle	k7c2	podle
západních	západní	k2eAgMnPc2d1	západní
odborníků	odborník	k1gMnPc2	odborník
je	být	k5eAaImIp3nS	být
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
tlakovodních	tlakovodní	k2eAgInPc2d1	tlakovodní
reaktorů	reaktor	k1gInPc2	reaktor
ruské	ruský	k2eAgFnSc2d1	ruská
konstrukce	konstrukce	k1gFnSc2	konstrukce
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zcela	zcela	k6eAd1	zcela
jinou	jiný	k2eAgFnSc4d1	jiná
koncepci	koncepce	k1gFnSc4	koncepce
<g/>
,	,	kIx,	,
než	než	k8xS	než
koncepce	koncepce	k1gFnSc1	koncepce
reaktoru	reaktor	k1gInSc2	reaktor
černobylského	černobylský	k2eAgInSc2d1	černobylský
typu	typ	k1gInSc2	typ
<g/>
)	)	kIx)	)
zcela	zcela	k6eAd1	zcela
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
obdobnými	obdobný	k2eAgInPc7d1	obdobný
západními	západní	k2eAgInPc7d1	západní
reaktory	reaktor	k1gInPc7	reaktor
<g/>
;	;	kIx,	;
ruská	ruský	k2eAgFnSc1d1	ruská
konstrukce	konstrukce	k1gFnSc1	konstrukce
mírně	mírně	k6eAd1	mírně
zaostávala	zaostávat	k5eAaImAgFnS	zaostávat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
řídicí	řídicí	k2eAgFnSc2d1	řídicí
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Temelíně	Temelín	k1gInSc6	Temelín
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
technikou	technika	k1gFnSc7	technika
západní	západní	k2eAgFnSc7d1	západní
<g/>
.	.	kIx.	.
</s>
<s>
Určitou	určitý	k2eAgFnSc4d1	určitá
roli	role	k1gFnSc4	role
hrají	hrát	k5eAaImIp3nP	hrát
i	i	k9	i
předsudky	předsudek	k1gInPc1	předsudek
či	či	k8xC	či
nedůvěra	nedůvěra	k1gFnSc1	nedůvěra
vůči	vůči	k7c3	vůči
jaderné	jaderný	k2eAgFnSc3d1	jaderná
energetice	energetika	k1gFnSc3	energetika
jako	jako	k8xS	jako
takové	takový	k3xDgNnSc1	takový
a	a	k8xC	a
také	také	k9	také
různé	různý	k2eAgFnPc4d1	různá
lobbistické	lobbistický	k2eAgFnPc4d1	lobbistická
skupiny	skupina	k1gFnPc4	skupina
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
rovněž	rovněž	k9	rovněž
odstavec	odstavec	k1gInSc4	odstavec
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Problémem	problém	k1gInSc7	problém
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
její	její	k3xOp3gInSc4	její
relativně	relativně	k6eAd1	relativně
vysoký	vysoký	k2eAgInSc4d1	vysoký
instalovaný	instalovaný	k2eAgInSc4d1	instalovaný
výkon	výkon	k1gInSc4	výkon
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
generátor	generátor	k1gInSc4	generátor
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgInPc3d1	ostatní
zdrojům	zdroj	k1gInPc3	zdroj
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
stavba	stavba	k1gFnSc1	stavba
menšího	malý	k2eAgNnSc2d2	menší
množství	množství	k1gNnSc2	množství
větších	veliký	k2eAgInPc2d2	veliký
bloků	blok	k1gInPc2	blok
místo	místo	k7c2	místo
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
menších	malý	k2eAgMnPc2d2	menší
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
kvůli	kvůli	k7c3	kvůli
snížení	snížení	k1gNnSc3	snížení
investičních	investiční	k2eAgInPc2d1	investiční
i	i	k8xC	i
provozních	provozní	k2eAgInPc2d1	provozní
nákladů	náklad	k1gInPc2	náklad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
náhlém	náhlý	k2eAgInSc6d1	náhlý
výpadku	výpadek	k1gInSc6	výpadek
dodávky	dodávka	k1gFnSc2	dodávka
elektřiny	elektřina	k1gFnSc2	elektřina
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
či	či	k8xC	či
druhého	druhý	k4xOgInSc2	druhý
bloku	blok	k1gInSc2	blok
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zajištěn	zajištěn	k2eAgInSc4d1	zajištěn
náhradní	náhradní	k2eAgInSc4d1	náhradní
zdroj	zdroj	k1gInSc4	zdroj
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
při	při	k7c6	při
vysokém	vysoký	k2eAgInSc6d1	vysoký
výkonu	výkon	k1gInSc6	výkon
obou	dva	k4xCgInPc2	dva
generátorů	generátor	k1gInPc2	generátor
znamená	znamenat	k5eAaImIp3nS	znamenat
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
situacích	situace	k1gFnPc6	situace
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
regulátor	regulátor	k1gInSc1	regulátor
přenosové	přenosový	k2eAgFnSc2d1	přenosová
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
ČEPS	ČEPS	kA	ČEPS
<g/>
)	)	kIx)	)
mít	mít	k5eAaImF	mít
omezené	omezený	k2eAgFnPc4d1	omezená
možnosti	možnost	k1gFnPc4	možnost
regulace	regulace	k1gFnSc2	regulace
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
musí	muset	k5eAaImIp3nP	muset
elektřinu	elektřina	k1gFnSc4	elektřina
nakupovat	nakupovat	k5eAaBmF	nakupovat
<g/>
.	.	kIx.	.
<g/>
Celkový	celkový	k2eAgInSc1d1	celkový
poměr	poměr	k1gInSc1	poměr
výkonu	výkon	k1gInSc2	výkon
obou	dva	k4xCgInPc2	dva
temelínských	temelínský	k2eAgInPc2d1	temelínský
generátorů	generátor	k1gInPc2	generátor
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
elektrárenské	elektrárenský	k2eAgFnSc2d1	elektrárenská
soustavy	soustava	k1gFnSc2	soustava
se	se	k3xPyFc4	se
však	však	k9	však
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
přijatelném	přijatelný	k2eAgNnSc6d1	přijatelné
rozmezí	rozmezí	k1gNnSc6	rozmezí
udávaném	udávaný	k2eAgNnSc6d1	udávané
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
,	,	kIx,	,
u	u	k7c2	u
Temelína	Temelín	k1gInSc2	Temelín
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
každý	každý	k3xTgInSc4	každý
z	z	k7c2	z
generátorů	generátor	k1gInPc2	generátor
přibližně	přibližně	k6eAd1	přibližně
7	[number]	k4	7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
všem	všecek	k3xTgFnPc3	všecek
peripetiím	peripetie	k1gFnPc3	peripetie
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
jaderná	jaderný	k2eAgFnSc1d1	jaderná
energetika	energetika	k1gFnSc1	energetika
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
prošla	projít	k5eAaPmAgNnP	projít
<g/>
,	,	kIx,	,
průzkumy	průzkum	k1gInPc1	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Česka	Česko	k1gNnSc2	Česko
jadernou	jaderný	k2eAgFnSc4d1	jaderná
energetiku	energetika	k1gFnSc4	energetika
a	a	k8xC	a
jadernou	jaderný	k2eAgFnSc4d1	jaderná
elektrárnu	elektrárna	k1gFnSc4	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
podporuje	podporovat	k5eAaImIp3nS	podporovat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kritika	kritika	k1gFnSc1	kritika
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Rakouska	Rakousko	k1gNnSc2	Rakousko
===	===	k?	===
</s>
</p>
<p>
<s>
Kritiku	kritika	k1gFnSc4	kritika
zpochybňující	zpochybňující	k2eAgFnSc4d1	zpochybňující
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
JE	být	k5eAaImIp3nS	být
Temelín	Temelín	k1gInSc1	Temelín
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
především	především	k9	především
zpoza	zpoza	k7c2	zpoza
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
bezprostředně	bezprostředně	k6eAd1	bezprostředně
sousedícím	sousedící	k2eAgNnSc6d1	sousedící
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
u	u	k7c2	u
části	část	k1gFnSc2	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
obavy	obava	k1gFnPc4	obava
z	z	k7c2	z
následků	následek	k1gInPc2	následek
případné	případný	k2eAgFnSc2d1	případná
havárie	havárie	k1gFnSc2	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
názory	názor	k1gInPc1	názor
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
podpořeny	podpořit	k5eAaPmNgInP	podpořit
i	i	k9	i
relativně	relativně	k6eAd1	relativně
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
Jadernou	jaderný	k2eAgFnSc7d1	jaderná
elektrárnou	elektrárna	k1gFnSc7	elektrárna
Dukovany	Dukovany	k1gInPc1	Dukovany
<g/>
)	)	kIx)	)
častými	častý	k2eAgFnPc7d1	častá
poruchami	porucha	k1gFnPc7	porucha
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
poruch	porucha	k1gFnPc2	porucha
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
nejaderné	jaderný	k2eNgFnSc6d1	nejaderná
části	část	k1gFnSc6	část
elektrárny	elektrárna	k1gFnSc2	elektrárna
a	a	k8xC	a
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nepřekročila	překročit	k5eNaPmAgFnS	překročit
stupeň	stupeň	k1gInSc1	stupeň
1	[number]	k4	1
stupnice	stupnice	k1gFnSc2	stupnice
INES	INES	kA	INES
(	(	kIx(	(
<g/>
Technická	technický	k2eAgFnSc1d1	technická
porucha	porucha	k1gFnSc1	porucha
nebo	nebo	k8xC	nebo
odchylka	odchylka	k1gFnSc1	odchylka
od	od	k7c2	od
schváleného	schválený	k2eAgInSc2d1	schválený
režimu	režim	k1gInSc2	režim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdánlivě	zdánlivě	k6eAd1	zdánlivě
vysoká	vysoký	k2eAgFnSc1d1	vysoká
frekvence	frekvence	k1gFnSc1	frekvence
poruch	porucha	k1gFnPc2	porucha
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
stupnice	stupnice	k1gFnSc2	stupnice
INES	INES	kA	INES
zatím	zatím	k6eAd1	zatím
pouhých	pouhý	k2eAgFnPc2d1	pouhá
odchylek	odchylka	k1gFnPc2	odchylka
<g/>
)	)	kIx)	)
na	na	k7c6	na
JETE	JETE	kA	JETE
je	být	k5eAaImIp3nS	být
zaviněna	zaviněn	k2eAgFnSc1d1	zaviněna
především	především	k6eAd1	především
nadstandardním	nadstandardní	k2eAgInSc7d1	nadstandardní
způsobem	způsob	k1gInSc7	způsob
informovanosti	informovanost	k1gFnSc2	informovanost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
velké	velký	k2eAgFnSc2d1	velká
většiny	většina	k1gFnSc2	většina
jiných	jiný	k2eAgFnPc2d1	jiná
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
veřejnost	veřejnost	k1gFnSc1	veřejnost
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
nejvýš	vysoce	k6eAd3	vysoce
o	o	k7c6	o
desítce	desítka	k1gFnSc6	desítka
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
vyzval	vyzvat	k5eAaPmAgInS	vyzvat
rakouský	rakouský	k2eAgInSc1d1	rakouský
parlament	parlament	k1gInSc1	parlament
vládu	vláda	k1gFnSc4	vláda
k	k	k7c3	k
právním	právní	k2eAgInPc3d1	právní
krokům	krok	k1gInPc3	krok
proti	proti	k7c3	proti
Temelínu	Temelín	k1gInSc3	Temelín
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
rakouských	rakouský	k2eAgMnPc2d1	rakouský
protijaderných	protijaderný	k2eAgMnPc2d1	protijaderný
aktivistů	aktivista	k1gMnPc2	aktivista
začaly	začít	k5eAaPmAgFnP	začít
opět	opět	k6eAd1	opět
blokády	blokáda	k1gFnPc1	blokáda
česko-rakouských	českoakouský	k2eAgInPc2d1	česko-rakouský
hraničních	hraniční	k2eAgInPc2d1	hraniční
přechodů	přechod	k1gInPc2	přechod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
události	událost	k1gFnPc4	událost
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2007	[number]	k4	2007
společnost	společnost	k1gFnSc1	společnost
ČEZ	ČEZ	kA	ČEZ
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
JETE	JETE	kA	JETE
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgInS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
srovnán	srovnán	k2eAgInSc4d1	srovnán
počet	počet	k1gInSc4	počet
poruch	porucha	k1gFnPc2	porucha
a	a	k8xC	a
odchylek	odchylka	k1gFnPc2	odchylka
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
jaderných	jaderný	k2eAgInPc6d1	jaderný
blocích	blok	k1gInPc6	blok
od	od	k7c2	od
běžného	běžný	k2eAgInSc2d1	běžný
režimu	režim	k1gInSc2	režim
jaderných	jaderný	k2eAgInPc2d1	jaderný
bloků	blok	k1gInPc2	blok
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Hodnocení	hodnocení	k1gNnSc1	hodnocení
zahrnulo	zahrnout	k5eAaPmAgNnS	zahrnout
výsledky	výsledek	k1gInPc4	výsledek
58	[number]	k4	58
francouzských	francouzský	k2eAgMnPc2d1	francouzský
jaderných	jaderný	k2eAgMnPc2d1	jaderný
bloků	blok	k1gInPc2	blok
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
až	až	k9	až
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
připadá	připadat	k5eAaImIp3nS	připadat
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
INES	INES	kA	INES
1	[number]	k4	1
zhruba	zhruba	k6eAd1	zhruba
1,1	[number]	k4	1,1
události	událost	k1gFnPc4	událost
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
blok	blok	k1gInSc4	blok
a	a	k8xC	a
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Temelín	Temelín	k1gInSc1	Temelín
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
sledovaném	sledovaný	k2eAgNnSc6d1	sledované
období	období	k1gNnSc6	období
hodnotu	hodnota	k1gFnSc4	hodnota
1,4	[number]	k4	1,4
události	událost	k1gFnSc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
INES	INES	kA	INES
0	[number]	k4	0
vychází	vycházet	k5eAaImIp3nS	vycházet
pro	pro	k7c4	pro
francouzské	francouzský	k2eAgFnPc4d1	francouzská
elektrárny	elektrárna	k1gFnPc4	elektrárna
7	[number]	k4	7
událostí	událost	k1gFnPc2	událost
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
blok	blok	k1gInSc4	blok
a	a	k8xC	a
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
Temelína	Temelín	k1gInSc2	Temelín
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
15	[number]	k4	15
událostí	událost	k1gFnPc2	událost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
zároveň	zároveň	k6eAd1	zároveň
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lepší	dobrý	k2eAgInPc1d2	lepší
výsledky	výsledek	k1gInPc1	výsledek
francouzských	francouzský	k2eAgFnPc2d1	francouzská
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
vysvětleny	vysvětlit	k5eAaPmNgInP	vysvětlit
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
už	už	k6eAd1	už
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
blokády	blokáda	k1gFnSc2	blokáda
hraničních	hraniční	k2eAgInPc2d1	hraniční
přechodů	přechod	k1gInPc2	přechod
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
nepřestanou	přestat	k5eNaPmIp3nP	přestat
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
by	by	kYmCp3nS	by
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
mohlo	moct	k5eAaImAgNnS	moct
vtáhnout	vtáhnout	k5eAaPmF	vtáhnout
EU	EU	kA	EU
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
blokády	blokáda	k1gFnPc1	blokáda
znamenají	znamenat	k5eAaImIp3nP	znamenat
porušení	porušení	k1gNnSc4	porušení
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
Melku	Melk	k1gInSc2	Melk
<g/>
.	.	kIx.	.
</s>
<s>
Nazval	nazvat	k5eAaBmAgMnS	nazvat
také	také	k9	také
odpůrce	odpůrce	k1gMnSc1	odpůrce
Temelína	Temelín	k1gInSc2	Temelín
"	"	kIx"	"
<g/>
magory	magor	k1gMnPc7	magor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgInPc3	svůj
postojům	postoj	k1gInPc3	postoj
k	k	k7c3	k
JETE	JETE	kA	JETE
si	se	k3xPyFc3	se
Schwarzenberg	Schwarzenberg	k1gInSc4	Schwarzenberg
u	u	k7c2	u
rakouských	rakouský	k2eAgMnPc2d1	rakouský
odpůrců	odpůrce	k1gMnPc2	odpůrce
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energie	energie	k1gFnSc2	energie
dokonce	dokonce	k9	dokonce
brzo	brzo	k6eAd1	brzo
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
přezdívku	přezdívka	k1gFnSc4	přezdívka
Atomfürst	Atomfürst	k1gFnSc4	Atomfürst
(	(	kIx(	(
<g/>
atomový	atomový	k2eAgMnSc1d1	atomový
kníže	kníže	k1gMnSc1	kníže
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
s	s	k7c7	s
blokádami	blokáda	k1gFnPc7	blokáda
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
už	už	k9	už
dřív	dříve	k6eAd2	dříve
i	i	k9	i
premiér	premiér	k1gMnSc1	premiér
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
a	a	k8xC	a
označil	označit	k5eAaPmAgMnS	označit
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
evropských	evropský	k2eAgFnPc2d1	Evropská
směrnic	směrnice	k1gFnPc2	směrnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2007	[number]	k4	2007
podala	podat	k5eAaPmAgFnS	podat
společnost	společnost	k1gFnSc1	společnost
ČEZ	ČEZ	kA	ČEZ
žalobu	žaloba	k1gFnSc4	žaloba
na	na	k7c4	na
Horní	horní	k2eAgNnSc4d1	horní
Rakousko	Rakousko	k1gNnSc4	Rakousko
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gFnSc3	jejich
neustálé	neustálý	k2eAgFnSc3d1	neustálá
snaze	snaha	k1gFnSc3	snaha
zastavit	zastavit	k5eAaPmF	zastavit
provoz	provoz	k1gInSc4	provoz
Temelína	Temelín	k1gInSc2	Temelín
<g/>
.	.	kIx.	.
</s>
<s>
ČEZ	ČEZ	kA	ČEZ
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
nucena	nutit	k5eAaImNgFnS	nutit
za	za	k7c4	za
soudní	soudní	k2eAgInPc4d1	soudní
spory	spor	k1gInPc4	spor
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
vydávat	vydávat	k5eAaPmF	vydávat
velké	velký	k2eAgFnPc4d1	velká
částky	částka	k1gFnPc4	částka
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
Evropský	evropský	k2eAgInSc1d1	evropský
soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
rakouské	rakouský	k2eAgInPc1d1	rakouský
soudy	soud	k1gInPc1	soud
nejsou	být	k5eNaImIp3nP	být
oprávněny	oprávněn	k2eAgFnPc4d1	oprávněna
žaloby	žaloba	k1gFnPc4	žaloba
proti	proti	k7c3	proti
ČEZ	ČEZ	kA	ČEZ
projednávat	projednávat	k5eAaImF	projednávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Plány	plán	k1gInPc1	plán
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
bloku	blok	k1gInSc2	blok
==	==	k?	==
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
ČEZ	ČEZ	kA	ČEZ
podala	podat	k5eAaPmAgFnS	podat
MŽP	MŽP	kA	MŽP
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
posouzení	posouzení	k1gNnSc4	posouzení
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
11	[number]	k4	11
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
zjišťovací	zjišťovací	k2eAgNnSc1d1	zjišťovací
řízení	řízení	k1gNnSc1	řízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2011	[number]	k4	2011
předal	předat	k5eAaPmAgMnS	předat
ČEZ	ČEZ	kA	ČEZ
zájemcům	zájemce	k1gMnPc3	zájemce
kvalifikovaným	kvalifikovaný	k2eAgMnSc7d1	kvalifikovaný
do	do	k7c2	do
zadávacího	zadávací	k2eAgNnSc2d1	zadávací
řízení	řízení	k1gNnSc2	řízení
veřejné	veřejný	k2eAgFnSc2d1	veřejná
zakázky	zakázka	k1gFnSc2	zakázka
Dostavba	dostavba	k1gFnSc1	dostavba
Jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
výzvu	výzva	k1gFnSc4	výzva
k	k	k7c3	k
podání	podání	k1gNnSc3	podání
nabídek	nabídka	k1gFnPc2	nabídka
včetně	včetně	k7c2	včetně
zadávací	zadávací	k2eAgFnSc2d1	zadávací
dokumentace	dokumentace	k1gFnSc2	dokumentace
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2012	[number]	k4	2012
otevřel	otevřít	k5eAaPmAgInS	otevřít
ČEZ	ČEZ	kA	ČEZ
nabídky	nabídka	k1gFnSc2	nabídka
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
zakázce	zakázka	k1gFnSc6	zakázka
na	na	k7c4	na
dostavbu	dostavba	k1gFnSc4	dostavba
Jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
za	za	k7c4	za
účasti	účast	k1gFnPc4	účast
jejich	jejich	k3xOp3gMnPc2	jejich
předkladatelů	předkladatel	k1gMnPc2	předkladatel
–	–	k?	–
společností	společnost	k1gFnSc7	společnost
Areva	Arevo	k1gNnSc2	Arevo
<g/>
,	,	kIx,	,
konsorcia	konsorcium	k1gNnSc2	konsorcium
společností	společnost	k1gFnPc2	společnost
<g />
.	.	kIx.	.
</s>
<s>
Westinghouse	Westinghouse	k1gFnSc1	Westinghouse
Electric	Electrice	k1gInPc2	Electrice
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
LLC	LLC	kA	LLC
a	a	k8xC	a
WESTINGHOUSE	WESTINGHOUSE	kA	WESTINGHOUSE
ELECTRIC	ELECTRIC	kA	ELECTRIC
ČR	ČR	kA	ČR
a	a	k8xC	a
konsorcia	konsorcium	k1gNnSc2	konsorcium
společností	společnost	k1gFnPc2	společnost
ŠKODA	škoda	k6eAd1	škoda
JS	JS	kA	JS
<g/>
,	,	kIx,	,
Atomstroyexport	Atomstroyexport	k1gInSc1	Atomstroyexport
a	a	k8xC	a
Gidropress	Gidropress	k1gInSc1	Gidropress
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
oznámil	oznámit	k5eAaPmAgInS	oznámit
ČEZ	ČEZ	kA	ČEZ
společnosti	společnost	k1gFnSc2	společnost
Areva	Areva	k1gFnSc1	Areva
<g/>
,	,	kIx,	,
že	že	k8xS	že
nenaplnila	naplnit	k5eNaPmAgFnS	naplnit
v	v	k7c6	v
nabídce	nabídka	k1gFnSc6	nabídka
zákonné	zákonný	k2eAgInPc4d1	zákonný
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
dvou	dva	k4xCgInPc2	dva
bloků	blok	k1gInPc2	blok
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Areva	Areva	k1gFnSc1	Areva
nesplnila	splnit	k5eNaPmAgFnS	splnit
ani	ani	k8xC	ani
další	další	k2eAgNnPc1d1	další
definovaná	definovaný	k2eAgNnPc1d1	definované
vylučovací	vylučovací	k2eAgNnPc1d1	vylučovací
kritéria	kritérion	k1gNnPc1	kritérion
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
tendr	tendr	k1gInSc1	tendr
probíhá	probíhat	k5eAaImIp3nS	probíhat
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
zadávání	zadávání	k1gNnSc6	zadávání
veřejných	veřejný	k2eAgFnPc2d1	veřejná
zakázek	zakázka	k1gFnPc2	zakázka
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnPc4	její
nabídka	nabídka	k1gFnSc1	nabídka
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
z	z	k7c2	z
dalšího	další	k2eAgNnSc2d1	další
řízení	řízení	k1gNnSc2	řízení
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
výroba	výroba	k1gFnSc1	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
fungovala	fungovat	k5eAaImAgFnS	fungovat
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
garantované	garantovaný	k2eAgFnSc2d1	garantovaná
výkupní	výkupní	k2eAgFnSc2d1	výkupní
ceny	cena	k1gFnSc2	cena
<g/>
.10	.10	k4	.10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
ukončení	ukončení	k1gNnSc1	ukončení
tendru	tendr	k1gInSc2	tendr
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
zrušení	zrušení	k1gNnSc1	zrušení
tendru	tendr	k1gInSc2	tendr
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
ČEZ	ČEZ	kA	ČEZ
neutěšená	utěšený	k2eNgFnSc1d1	neutěšená
situace	situace	k1gFnSc1	situace
na	na	k7c6	na
energetickém	energetický	k2eAgInSc6d1	energetický
trhu	trh	k1gInSc6	trh
a	a	k8xC	a
neochota	neochota	k1gFnSc1	neochota
české	český	k2eAgFnSc2d1	Česká
vlády	vláda	k1gFnSc2	vláda
výstavbu	výstavba	k1gFnSc4	výstavba
nových	nový	k2eAgInPc2d1	nový
bloků	blok	k1gInPc2	blok
finančně	finančně	k6eAd1	finančně
podpořit	podpořit	k5eAaPmF	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ekonomického	ekonomický	k2eAgMnSc2d1	ekonomický
analytika	analytik	k1gMnSc2	analytik
Tomáše	Tomáš	k1gMnSc2	Tomáš
Sýkory	Sýkora	k1gMnSc2	Sýkora
by	by	k9	by
výstavba	výstavba	k1gFnSc1	výstavba
nových	nový	k2eAgInPc2d1	nový
reaktorů	reaktor	k1gInPc2	reaktor
nebyla	být	k5eNaImAgFnS	být
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
cena	cena	k1gFnSc1	cena
elektřiny	elektřina	k1gFnSc2	elektřina
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
klesla	klesnout	k5eAaPmAgFnS	klesnout
ke	k	k7c3	k
32	[number]	k4	32
eurům	euro	k1gNnPc3	euro
za	za	k7c4	za
megawatthodinu	megawatthodina	k1gFnSc4	megawatthodina
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
nových	nový	k2eAgInPc2d1	nový
bloků	blok	k1gInPc2	blok
v	v	k7c6	v
Temelíně	Temelín	k1gInSc6	Temelín
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vyplatila	vyplatit	k5eAaPmAgFnS	vyplatit
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
cena	cena	k1gFnSc1	cena
elektřiny	elektřina	k1gFnSc2	elektřina
činila	činit	k5eAaImAgFnS	činit
přes	přes	k7c4	přes
70	[number]	k4	70
eur	euro	k1gNnPc2	euro
za	za	k7c4	za
megawatthodinu	megawatthodina	k1gFnSc4	megawatthodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
oploceného	oplocený	k2eAgInSc2d1	oplocený
areálu	areál	k1gInSc2	areál
elektrárny	elektrárna	k1gFnSc2	elektrárna
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
líbí	líbit	k5eAaImIp3nS	líbit
zajícům	zajíc	k1gMnPc3	zajíc
–	–	k?	–
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
Myslivecké	myslivecký	k2eAgNnSc4d1	Myslivecké
sdružení	sdružení	k1gNnSc4	sdružení
Háj	háj	k1gInSc4	háj
Temelín	Temelín	k1gInSc4	Temelín
provedlo	provést	k5eAaPmAgNnS	provést
jejich	jejich	k3xOp3gNnSc1	jejich
odchyt	odchyt	k1gInSc4	odchyt
<g/>
,	,	kIx,	,
30	[number]	k4	30
zajíců	zajíc	k1gMnPc2	zajíc
obojího	obojí	k4xRgMnSc2	obojí
pohlaví	pohlaví	k1gNnSc3	pohlaví
bylo	být	k5eAaImAgNnS	být
vypuštěno	vypustit	k5eAaPmNgNnS	vypustit
do	do	k7c2	do
honitby	honitba	k1gFnSc2	honitba
<g/>
.	.	kIx.	.
</s>
<s>
Zajíců	Zajíc	k1gMnPc2	Zajíc
však	však	k8xC	však
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
elektrárny	elektrárna	k1gFnSc2	elektrárna
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
i	i	k9	i
kolem	kolem	k7c2	kolem
80	[number]	k4	80
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
blízkosti	blízkost	k1gFnSc6	blízkost
JE	být	k5eAaImIp3nS	být
Temelín	Temelín	k1gInSc1	Temelín
stojí	stát	k5eAaImIp3nS	stát
renesanční	renesanční	k2eAgInSc4d1	renesanční
zámeček	zámeček	k1gInSc4	zámeček
Vysoký	vysoký	k2eAgInSc1d1	vysoký
Hrádek	hrádek	k1gInSc1	hrádek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
Informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
temelínské	temelínský	k2eAgFnSc2d1	temelínská
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zámečku	zámeček	k1gInSc6	zámeček
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
např.	např.	kA	např.
moderně	moderně	k6eAd1	moderně
vybavený	vybavený	k2eAgInSc4d1	vybavený
kinosál	kinosál	k1gInSc4	kinosál
<g/>
,	,	kIx,	,
mlžná	mlžný	k2eAgFnSc1d1	mlžná
komora	komora	k1gFnSc1	komora
<g/>
,	,	kIx,	,
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
expozice	expozice	k1gFnSc1	expozice
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Hodnocení	hodnocení	k1gNnSc1	hodnocení
vlivů	vliv	k1gInPc2	vliv
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
sborník	sborník	k1gInSc1	sborník
z	z	k7c2	z
konference	konference	k1gFnSc2	konference
<g/>
,	,	kIx,	,
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
Jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
editoři	editor	k1gMnPc1	editor
Emilie	Emilie	k1gFnSc1	Emilie
Pecharová	Pecharová	k1gFnSc1	Pecharová
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Broumová	Broumová	k1gFnSc1	Broumová
<g/>
;	;	kIx,	;
ISBN	ISBN	kA	ISBN
80-7040-710-7	[number]	k4	80-7040-710-7
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Rizika	riziko	k1gNnPc1	riziko
přesahující	přesahující	k2eAgFnSc2d1	přesahující
hranice	hranice	k1gFnSc2	hranice
<g/>
:	:	kIx,	:
případ	případ	k1gInSc1	případ
Temelín	Temelín	k1gInSc1	Temelín
<g/>
,	,	kIx,	,
Helmut	Helmut	k1gMnSc1	Helmut
Böck	Böck	k1gMnSc1	Böck
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
Drábová	Drábová	k1gFnSc1	Drábová
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
nukleární	nukleární	k2eAgFnSc1d1	nukleární
společnost	společnost	k1gFnSc1	společnost
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
;	;	kIx,	;
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1794	[number]	k4	1794
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
PDF	PDF	kA	PDF
</s>
</p>
<p>
<s>
Vliv	vliv	k1gInSc1	vliv
provozu	provoz	k1gInSc2	provoz
JE	být	k5eAaImIp3nS	být
Temelín	Temelín	k1gInSc1	Temelín
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
:	:	kIx,	:
předpoklady	předpoklad	k1gInPc1	předpoklad
a	a	k8xC	a
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
sborník	sborník	k1gInSc1	sborník
materiálů	materiál	k1gInPc2	materiál
ze	z	k7c2	z
semináře	seminář	k1gInSc2	seminář
pořádaného	pořádaný	k2eAgInSc2d1	pořádaný
Českou	český	k2eAgFnSc7d1	Česká
nukleární	nukleární	k2eAgFnSc7d1	nukleární
společností	společnost	k1gFnSc7	společnost
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Gomel	Gomela	k1gFnPc2	Gomela
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
(	(	kIx(	(
<g/>
sestavili	sestavit	k5eAaPmAgMnP	sestavit
Václav	Václav	k1gMnSc1	Václav
Bláha	Bláha	k1gMnSc1	Bláha
a	a	k8xC	a
Martina	Martina	k1gFnSc1	Martina
Kortanová	Kortanový	k2eAgFnSc1d1	Kortanová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
vědeckotechnických	vědeckotechnický	k2eAgFnPc2d1	vědeckotechnická
společností	společnost	k1gFnPc2	společnost
<g/>
;	;	kIx,	;
ISBN	ISBN	kA	ISBN
80-02-01871-0	[number]	k4	80-02-01871-0
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Zhodnocení	zhodnocení	k1gNnSc1	zhodnocení
výstavby	výstavba	k1gFnSc2	výstavba
a	a	k8xC	a
spouštění	spouštění	k1gNnSc2	spouštění
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
<g/>
,	,	kIx,	,
sborník	sborník	k1gInSc1	sborník
referátů	referát	k1gInPc2	referát
z	z	k7c2	z
konference	konference	k1gFnSc2	konference
v	v	k7c6	v
Srní	srní	k1gNnSc6	srní
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
nukleární	nukleární	k2eAgFnSc1d1	nukleární
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Využití	využití	k1gNnSc1	využití
energií	energie	k1gFnPc2	energie
z	z	k7c2	z
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
<g/>
,	,	kIx,	,
sborník	sborník	k1gInSc1	sborník
z	z	k7c2	z
konference	konference	k1gFnSc2	konference
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1989	[number]	k4	1989
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
99697	[number]	k4	99697
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Příprava	příprava	k1gFnSc1	příprava
<g/>
,	,	kIx,	,
realizace	realizace	k1gFnSc1	realizace
výstavby	výstavba	k1gFnSc2	výstavba
a	a	k8xC	a
provozu	provoz	k1gInSc2	provoz
JE	být	k5eAaImIp3nS	být
Temelín	Temelín	k1gInSc1	Temelín
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
techniky	technika	k1gFnSc2	technika
ČSVTS	ČSVTS	kA	ČSVTS
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
99062	[number]	k4	99062
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Technické	technický	k2eAgInPc1d1	technický
problémy	problém	k1gInPc1	problém
při	při	k7c6	při
uvádění	uvádění	k1gNnSc6	uvádění
Jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
František	františek	k1gInSc4	františek
Hezoučký	hezoučký	k2eAgInSc1d1	hezoučký
<g/>
,	,	kIx,	,
habilitační	habilitační	k2eAgFnSc1d1	habilitační
přednáška	přednáška	k1gFnSc1	přednáška
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
<g/>
,	,	kIx,	,
PDF	PDF	kA	PDF
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Vliv	vliv	k1gInSc1	vliv
vleček	vlečka	k1gFnPc2	vlečka
chladicích	chladicí	k2eAgFnPc2d1	chladicí
věží	věž	k1gFnPc2	věž
na	na	k7c4	na
počasí	počasí	k1gNnSc4	počasí
a	a	k8xC	a
klima	klima	k1gNnSc4	klima
<g/>
,	,	kIx,	,
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
78	[number]	k4	78
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
Daniela	Daniela	k1gFnSc1	Daniela
Řezáčová	Řezáčová	k1gFnSc1	Řezáčová
<g/>
,	,	kIx,	,
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
fyziky	fyzika	k1gFnSc2	fyzika
atmosféry	atmosféra	k1gFnSc2	atmosféra
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
květen	květen	k1gInSc4	květen
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
energie	energie	k1gFnSc1	energie
</s>
</p>
<p>
<s>
Temelín	Temelín	k1gInSc1	Temelín
</s>
</p>
<p>
<s>
Protokol	protokol	k1gInSc1	protokol
z	z	k7c2	z
Melku	Melk	k1gInSc2	Melk
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Hněvkovice	Hněvkovice	k1gFnSc2	Hněvkovice
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Dukovany	Dukovany	k1gInPc1	Dukovany
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Loviisa	Loviisa	k1gFnSc1	Loviisa
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Krško	Krška	k1gMnSc5	Krška
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Mochovce	Mochovka	k1gFnSc3	Mochovka
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Jaslovské	Jaslovský	k2eAgFnPc1d1	Jaslovský
Bohunice	Bohunice	k1gFnPc1	Bohunice
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Ignalina	Ignalina	k1gFnSc1	Ignalina
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Kozloduj	Kozloduj	k1gFnSc2	Kozloduj
</s>
</p>
<p>
<s>
MAPE	MAPE	kA	MAPE
Mydlovary	Mydlovary	k1gInPc4	Mydlovary
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Temelín	Temelín	k1gInSc4	Temelín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Protokol	protokol	k1gInSc1	protokol
z	z	k7c2	z
Melku	Melk	k1gInSc2	Melk
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Temelín	Temelín	k1gInSc1	Temelín
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
České	český	k2eAgFnSc2d1	Česká
nukleární	nukleární	k2eAgFnSc2d1	nukleární
společnosti	společnost	k1gFnSc2	společnost
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
často	často	k6eAd1	často
Temelínem	Temelín	k1gInSc7	Temelín
</s>
</p>
<p>
<s>
temelinky	temelinka	k1gFnPc1	temelinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
</s>
</p>
<p>
<s>
Stručné	stručný	k2eAgNnSc1d1	stručné
heslo	heslo	k1gNnSc1	heslo
na	na	k7c6	na
energyweb	energywba	k1gFnPc2	energywba
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
energie	energie	k1gFnSc1	energie
<g/>
"	"	kIx"	"
vydaná	vydaný	k2eAgFnSc1d1	vydaná
ČEZ	ČEZ	kA	ČEZ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obsáhlá	obsáhlý	k2eAgFnSc1d1	obsáhlá
reportáž	reportáž	k1gFnSc1	reportáž
webu	web	k1gInSc2	web
technet	techneta	k1gFnPc2	techneta
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
soustřeďující	soustřeďující	k2eAgMnSc1d1	soustřeďující
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
technickou	technický	k2eAgFnSc4d1	technická
stránku	stránka	k1gFnSc4	stránka
věci	věc	k1gFnSc2	věc
</s>
</p>
<p>
<s>
Obejde	obejít	k5eAaPmIp3nS	obejít
se	se	k3xPyFc4	se
Rakousko	Rakousko	k1gNnSc1	Rakousko
bez	bez	k7c2	bez
Temelína	Temelín	k1gInSc2	Temelín
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
;	;	kIx,	;
Neviditelný	viditelný	k2eNgMnSc1d1	Neviditelný
pes	pes	k1gMnSc1	pes
</s>
</p>
<p>
<s>
AKW	AKW	kA	AKW
Temelin	Temelin	k2eAgInSc1d1	Temelin
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
JE	být	k5eAaImIp3nS	být
Temelín	Temelín	k1gInSc1	Temelín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stránky	stránka	k1gFnSc2	stránka
rakouského	rakouský	k2eAgNnSc2d1	rakouské
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
s	s	k7c7	s
dokumentací	dokumentace	k1gFnSc7	dokumentace
k	k	k7c3	k
problematice	problematika	k1gFnSc3	problematika
JETE	JETE	kA	JETE
</s>
</p>
<p>
<s>
Jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
a	a	k8xC	a
Dukovany	Dukovany	k1gInPc1	Dukovany
–	–	k?	–
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
českých	český	k2eAgFnPc6d1	Česká
jaderných	jaderný	k2eAgFnPc6d1	jaderná
elektrárnách	elektrárna	k1gFnPc6	elektrárna
</s>
</p>
<p>
<s>
Rizika	riziko	k1gNnPc1	riziko
přesahující	přesahující	k2eAgFnSc2d1	přesahující
hranice	hranice	k1gFnSc2	hranice
<g/>
;	;	kIx,	;
Případ	případ	k1gInSc1	případ
Temelín	Temelín	k1gInSc1	Temelín
<g/>
,	,	kIx,	,
publikace	publikace	k1gFnSc1	publikace
<g/>
,	,	kIx,	,
PDF	PDF	kA	PDF
</s>
</p>
<p>
<s>
Obsáhlý	obsáhlý	k2eAgInSc1d1	obsáhlý
článek	článek	k1gInSc1	článek
Jiřího	Jiří	k1gMnSc2	Jiří
Pálky	Pálka	k1gMnSc2	Pálka
na	na	k7c4	na
blisty	blist	k1gInPc4	blist
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Dokončení	dokončení	k1gNnSc1	dokončení
Temelína	Temelín	k1gInSc2	Temelín
odloženo	odložit	k5eAaPmNgNnS	odložit
o	o	k7c6	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Virtuální	virtuální	k2eAgFnSc1d1	virtuální
prohlídka	prohlídka	k1gFnSc1	prohlídka
elektrárny	elektrárna	k1gFnSc2	elektrárna
</s>
</p>
