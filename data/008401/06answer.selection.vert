<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
často	často	k6eAd1	často
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Temelín	Temelín	k1gInSc1	Temelín
(	(	kIx(	(
<g/>
zkratkou	zkratka	k1gFnSc7	zkratka
ETE	ETE	kA	ETE
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
také	také	k9	také
JETE	JETE	kA	JETE
nebo	nebo	k8xC	nebo
JET	jet	k2eAgMnSc1d1	jet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
elektrárna	elektrárna	k1gFnSc1	elektrárna
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
instalovaným	instalovaný	k2eAgInSc7d1	instalovaný
výkonem	výkon	k1gInSc7	výkon
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
českých	český	k2eAgFnPc2d1	Česká
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
