<s>
Čerchov	Čerchov	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Schwarzkopf	Schwarzkopf	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hora	hora	k1gFnSc1
v	v	k7c6
jihozápadních	jihozápadní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
s	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
1041	#num#	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
Haltravské	Haltravský	k2eAgFnSc2d1
hornatiny	hornatina	k1gFnSc2
<g/>
,	,	kIx,
celého	celý	k2eAgInSc2d1
Českého	český	k2eAgInSc2d1
lesa	les	k1gInSc2
a	a	k8xC
okresu	okres	k1gInSc2
Domažlice	Domažlice	k1gFnPc1
<g/>
.	.	kIx.
</s>