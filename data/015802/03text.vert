<s>
Čerchov	Čerchov	k1gInSc1
</s>
<s>
Čerchov	Čerchov	k1gInSc1
Vojenská	vojenský	k2eAgFnSc1d1
věž	věž	k1gFnSc1
a	a	k8xC
Kurzova	Kurzův	k2eAgFnSc1d1
rozhledna	rozhledna	k1gFnSc1
na	na	k7c6
vrcholu	vrchol	k1gInSc6
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
1041	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
526	#num#	k4
m	m	kA
↓	↓	k?
u	u	k7c2
Mitterteichu	Mitterteich	k1gInSc2
Izolace	izolace	k1gFnSc2
</s>
<s>
19,4	19,4	k4
km	km	kA
→	→	k?
Schwarzriegel	Schwarzriegel	k1gInSc1
Seznamy	seznam	k1gInPc1
</s>
<s>
Tisícovky	tisícovka	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
#	#	kIx~
<g/>
287	#num#	k4
<g/>
Ultratisícovky	Ultratisícovka	k1gFnPc1
#	#	kIx~
<g/>
78	#num#	k4
<g/>
Nejprominentnější	prominentní	k2eAgFnPc1d3
hory	hora	k1gFnPc1
CZ	CZ	kA
#	#	kIx~
<g/>
10	#num#	k4
<g/>
Hory	hora	k1gFnSc2
a	a	k8xC
kopce	kopec	k1gInSc2
Českého	český	k2eAgInSc2d1
lesa	les	k1gInSc2
#	#	kIx~
<g/>
1	#num#	k4
Poznámka	poznámka	k1gFnSc1
</s>
<s>
rozhledna	rozhledna	k1gFnSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
/	/	kIx~
Čerchovský	Čerchovský	k2eAgInSc1d1
les	les	k1gInSc1
/	/	kIx~
Haltravská	Haltravský	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
/	/	kIx~
Čerchovský	Čerchovský	k2eAgInSc1d1
hřbet	hřbet	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
47	#num#	k4
<g/>
′	′	k?
<g/>
2	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Čerchov	Čerchov	k1gInSc1
</s>
<s>
Hornina	hornina	k1gFnSc1
</s>
<s>
břidličnatá	břidličnatý	k2eAgFnSc1d1
rula	rula	k1gFnSc1
Povodí	povodí	k1gNnSc2
</s>
<s>
Chladná	chladný	k2eAgFnSc1d1
Bystřice	Bystřice	k1gFnSc1
<g/>
,	,	kIx,
Teplá	teplý	k2eAgFnSc1d1
Bystřice	Bystřice	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Čerchov	Čerchov	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Schwarzkopf	Schwarzkopf	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hora	hora	k1gFnSc1
v	v	k7c6
jihozápadních	jihozápadní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
s	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
1041	#num#	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
Haltravské	Haltravský	k2eAgFnSc2d1
hornatiny	hornatina	k1gFnSc2
<g/>
,	,	kIx,
celého	celý	k2eAgInSc2d1
Českého	český	k2eAgInSc2d1
lesa	les	k1gInSc2
a	a	k8xC
okresu	okres	k1gInSc2
Domažlice	Domažlice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
prominencí	prominence	k1gFnSc7
526	#num#	k4
metrů	metr	k1gInPc2
jde	jít	k5eAaImIp3nS
o	o	k7c4
desátou	desátý	k4xOgFnSc4
nejprominentnější	prominentní	k2eAgFnSc4d3
horu	hora	k1gFnSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Vypíná	vypínat	k5eAaImIp3nS
se	se	k3xPyFc4
necelé	celý	k2eNgFnPc1d1
2	#num#	k4
km	km	kA
od	od	k7c2
německých	německý	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
přibližně	přibližně	k6eAd1
12	#num#	k4
km	km	kA
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Domažlic	Domažlice	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c4
trojmezí	trojmezí	k1gNnSc4
katastrů	katastr	k1gInPc2
obcí	obec	k1gFnPc2
Česká	český	k2eAgFnSc1d1
Kubice	kubika	k1gFnSc3
<g/>
,	,	kIx,
Pec	Pec	k1gFnSc4
a	a	k8xC
Chodov	Chodov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
roční	roční	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
vzduchu	vzduch	k1gInSc2
na	na	k7c6
Čerchově	Čerchův	k2eAgNnSc6d1
činí	činit	k5eAaImIp3nS
4,2	4,2	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
průměrný	průměrný	k2eAgInSc1d1
roční	roční	k2eAgInSc4d1
úhrn	úhrn	k1gInSc4
srážek	srážka	k1gFnPc2
1168	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dřívější	dřívější	k2eAgFnSc1d1
výška	výška	k1gFnSc1
1042	#num#	k4
m	m	kA
je	být	k5eAaImIp3nS
výškou	výška	k1gFnSc7
geodetického	geodetický	k2eAgInSc2d1
bodu	bod	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
kamenný	kamenný	k2eAgInSc1d1
hranol	hranol	k1gInSc1
ční	čnět	k5eAaImIp3nS
nad	nad	k7c7
terénem	terén	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stavby	stavba	k1gFnPc1
na	na	k7c6
vrcholu	vrchol	k1gInSc6
</s>
<s>
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
stojí	stát	k5eAaImIp3nS
vojenská	vojenský	k2eAgFnSc1d1
betonová	betonový	k2eAgFnSc1d1
věž	věž	k1gFnSc1
vysoká	vysoký	k2eAgFnSc1d1
přes	přes	k7c4
30	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
kamenná	kamenný	k2eAgFnSc1d1
Kurzova	Kurzův	k2eAgFnSc1d1
rozhledna	rozhledna	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1905	#num#	k4
<g/>
,	,	kIx,
vysoká	vysoký	k2eAgFnSc1d1
24	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
s	s	k7c7
výborným	výborný	k2eAgInSc7d1
výhledem	výhled	k1gInSc7
(	(	kIx(
<g/>
za	za	k7c2
ideálních	ideální	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
až	až	k9
na	na	k7c4
Alpy	Alpy	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
základní	základní	k2eAgInSc1d1
kámen	kámen	k1gInSc1
byl	být	k5eAaImAgInS
slavnostně	slavnostně	k6eAd1
položen	položit	k5eAaPmNgInS
23	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1904	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věž	věž	k1gFnSc1
byla	být	k5eAaImAgFnS
financována	financovat	k5eAaBmNgFnS
ze	z	k7c2
sbírek	sbírka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
prováděny	provádět	k5eAaImNgInP
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1902	#num#	k4
za	za	k7c7
účelem	účel	k1gInSc7
nahrazení	nahrazení	k1gNnSc2
původní	původní	k2eAgFnSc2d1
dřevěné	dřevěný	k2eAgFnSc2d1
věže	věž	k1gFnSc2
novou	nový	k2eAgFnSc7d1
věží	věž	k1gFnSc7
kamennou	kamenný	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předání	předání	k1gNnSc3
věže	věž	k1gFnSc2
veřejnosti	veřejnost	k1gFnSc2
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
16	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1905	#num#	k4
a	a	k8xC
byla	být	k5eAaImAgFnS
nazvána	nazvat	k5eAaBmNgFnS,k5eAaPmNgFnS
Kurzovou	Kurzův	k2eAgFnSc7d1
věží	věž	k1gFnSc7
na	na	k7c4
počest	počest	k1gFnSc4
svého	svůj	k3xOyFgMnSc2
příznivce	příznivec	k1gMnSc2
profesora	profesor	k1gMnSc2
Dr	dr	kA
<g/>
.	.	kIx.
Viléma	Vilém	k1gMnSc2
Kurze	kurz	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
však	však	k9
otevření	otevření	k1gNnSc3
již	již	k6eAd1
nedožil	dožít	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Přístup	přístup	k1gInSc1
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
byl	být	k5eAaImAgInS
přístup	přístup	k1gInSc4
nemožný	možný	k2eNgInSc4d1
<g/>
,	,	kIx,
vojenské	vojenský	k2eAgInPc4d1
objekty	objekt	k1gInPc4
na	na	k7c6
vrcholu	vrchol	k1gInSc6
byly	být	k5eAaImAgFnP
přísně	přísně	k6eAd1
tajné	tajný	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Byla	být	k5eAaImAgFnS
jen	jen	k9
přístupná	přístupný	k2eAgFnSc1d1
přes	přes	k7c4
pozemek	pozemek	k1gInSc4
vojenského	vojenský	k2eAgInSc2d1
útvaru	útvar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
války	válka	k1gFnSc2
používala	používat	k5eAaImAgFnS
objekty	objekt	k1gInPc4
na	na	k7c6
Čerchově	Čerchův	k2eAgFnSc6d1
německá	německý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
jako	jako	k8xS,k8xC
pozorovatelnu	pozorovatelna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
přístup	přístup	k1gInSc4
snadný	snadný	k2eAgInSc4d1
z	z	k7c2
rozcestí	rozcestí	k1gNnPc2
Pod	pod	k7c7
Čerchovem	Čerchov	k1gInSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
kam	kam	k6eAd1
se	se	k3xPyFc4
lze	lze	k6eAd1
dostat	dostat	k5eAaPmF
po	po	k7c6
červené	červený	k2eAgFnSc6d1
značce	značka	k1gFnSc6
z	z	k7c2
České	český	k2eAgFnPc1d1
Kubice	kubika	k1gFnSc3
(	(	kIx(
<g/>
6	#num#	k4
km	km	kA
<g/>
)	)	kIx)
a	a	k8xC
Capartic	Capartice	k1gFnPc2
(	(	kIx(
<g/>
4	#num#	k4
km	km	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
po	po	k7c6
modré	modrý	k2eAgFnSc6d1
značce	značka	k1gFnSc6
z	z	k7c2
Černé	Černé	k2eAgFnSc2d1
Řeky	řeka	k1gFnSc2
(	(	kIx(
<g/>
3	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
rozcestí	rozcestí	k1gNnSc2
vede	vést	k5eAaImIp3nS
zelená	zelený	k2eAgFnSc1d1
značka	značka	k1gFnSc1
vzhůru	vzhůru	k6eAd1
k	k	k7c3
jihu	jih	k1gInSc3
do	do	k7c2
sedla	sedlo	k1gNnSc2
mezi	mezi	k7c7
Čerchovem	Čerchovo	k1gNnSc7
a	a	k8xC
Skalkou	skalka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vrchol	vrchol	k1gInSc4
Čerchova	Čerchův	k2eAgNnSc2d1
vede	vést	k5eAaImIp3nS
od	od	k7c2
České	český	k2eAgFnSc2d1
studánky	studánka	k1gFnSc2
tzv.	tzv.	kA
Hanova	Hanův	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kamenné	kamenný	k2eAgInPc1d1
schody	schod	k1gInPc1
a	a	k8xC
upravený	upravený	k2eAgInSc1d1
terén	terén	k1gInSc1
této	tento	k3xDgFnSc2
stezky	stezka	k1gFnSc2
vinoucí	vinoucí	k2eAgFnSc2d1
se	se	k3xPyFc4
mezi	mezi	k7c7
skalkami	skalka	k1gFnPc7
je	být	k5eAaImIp3nS
dosud	dosud	k6eAd1
patrný	patrný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severovýchodně	severovýchodně	k6eAd1
vrcholu	vrchol	k1gInSc2
pramení	pramenit	k5eAaImIp3nS
v	v	k7c6
minulosti	minulost	k1gFnSc6
upravený	upravený	k2eAgInSc4d1
pramen	pramen	k1gInSc4
Emerichovy	Emerichův	k2eAgFnSc2d1
studny	studna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fauna	fauna	k1gFnSc1
a	a	k8xC
flora	flora	k1gFnSc1
</s>
<s>
Současná	současný	k2eAgFnSc1d1
skladba	skladba	k1gFnSc1
lesního	lesní	k2eAgInSc2d1
porostu	porost	k1gInSc2
je	být	k5eAaImIp3nS
převažující	převažující	k2eAgFnSc1d1
dřevina	dřevina	k1gFnSc1
smrk	smrk	k1gInSc1
<g/>
,	,	kIx,
z	z	k7c2
jehličnatých	jehličnatý	k2eAgFnPc2d1
dřevin	dřevina	k1gFnPc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
dále	daleko	k6eAd2
borovice	borovice	k1gFnSc1
<g/>
,	,	kIx,
jedle	jedle	k1gFnSc1
a	a	k8xC
modřín	modřín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
listnatých	listnatý	k2eAgFnPc2d1
dřevin	dřevina	k1gFnPc2
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc7d1
dřevinou	dřevina	k1gFnSc7
buk	buk	k1gInSc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
pak	pak	k6eAd1
bříza	bříza	k1gFnSc1
<g/>
,	,	kIx,
olše	olše	k1gFnSc1
<g/>
,	,	kIx,
javor	javor	k1gInSc1
<g/>
,	,	kIx,
dub	dub	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roste	růst	k5eAaImIp3nS
zde	zde	k6eAd1
mnoho	mnoho	k4c4
vzácných	vzácný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
<g/>
,	,	kIx,
česnek	česnek	k1gInSc4
medvědí	medvědí	k2eAgInSc4d1
<g/>
,	,	kIx,
rosnatka	rosnatka	k1gFnSc1
okrouhlolistá	okrouhlolistý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ojediněle	ojediněle	k6eAd1
i	i	k9
horské	horský	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
jako	jako	k8xS,k8xC
plavuň	plavuň	k1gFnSc1
pučivá	pučivý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
březích	březí	k2eAgInPc2d1
potoků	potok	k1gInPc2
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
ďáblík	ďáblík	k1gInSc1
bahenní	bahenní	k2eAgInSc1d1
<g/>
,	,	kIx,
oměj	oměj	k1gInSc1
pestrý	pestrý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Zachovalé	zachovalý	k2eAgInPc1d1
lesy	les	k1gInPc1
na	na	k7c6
úbočích	úbočí	k1gNnPc6
hory	hora	k1gFnPc1
jsou	být	k5eAaImIp3nP
chráněny	chránit	k5eAaImNgFnP
jako	jako	k9
národní	národní	k2eAgFnPc1d1
přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
Čerchovské	Čerchovský	k2eAgFnPc1d1
hvozdy	hvozd	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severovýchodním	severovýchodní	k2eAgNnSc6d1
úbočí	úbočí	k1gNnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
menší	malý	k2eAgFnPc4d2
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
Bystřice	Bystřice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
stálou	stálý	k2eAgFnSc4d1
sestavu	sestava	k1gFnSc4
živočichů	živočich	k1gMnPc2
patří	patřit	k5eAaImIp3nS
jelen	jelen	k1gMnSc1
lesní	lesní	k2eAgMnSc1d1
<g/>
,	,	kIx,
daněk	daněk	k1gMnSc1
evropský	evropský	k2eAgMnSc1d1
<g/>
,	,	kIx,
srnec	srnec	k1gMnSc1
<g/>
,	,	kIx,
prase	prase	k1gNnSc1
divoké	divoký	k2eAgNnSc1d1
<g/>
,	,	kIx,
zajíc	zajíc	k1gMnSc1
<g/>
,	,	kIx,
králík	králík	k1gMnSc1
divoký	divoký	k2eAgMnSc1d1
<g/>
,	,	kIx,
kuna	kuna	k1gFnSc1
lesní	lesní	k2eAgFnSc2d1
aj.	aj.	kA
K	k	k7c3
nejběžnějším	běžný	k2eAgMnPc3d3
dravcům	dravec	k1gMnPc3
náleží	náležet	k5eAaImIp3nS
káně	káně	k1gFnSc1
lesní	lesní	k2eAgFnSc1d1
<g/>
,	,	kIx,
poštolka	poštolka	k1gFnSc1
obecná	obecná	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
ČR	ČR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměměřický	zeměměřický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nejprominentnější	prominentní	k2eAgFnPc4d3
::	::	k?
Ultratisícovky	Ultratisícovka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.ultratisicovky.cz	www.ultratisicovky.cz	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bodová	bodový	k2eAgFnSc1d1
pole	pole	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměměřický	zeměměřický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Čerchov	Čerchovo	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.tisicovky.cz	www.tisicovky.cz	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Čerchov	Čerchovo	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Čerchov	Čerchov	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Čerchov	Čerchov	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
o	o	k7c6
státních	státní	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
a	a	k8xC
pohraniční	pohraniční	k2eAgFnSc3d1
turistice	turistika	k1gFnSc3
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Tisícovky	tisícovka	k1gFnSc2
Českého	český	k2eAgInSc2d1
lesa	les	k1gInSc2
Hlavní	hlavní	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čerchov	Čerchov	k1gInSc1
(	(	kIx(
<g/>
1041	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Skalka	skalka	k1gFnSc1
(	(	kIx(
<g/>
1008	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
