<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2002	[number]	k4	2002
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
Losos	losos	k1gMnSc1	losos
pochybnosti	pochybnost	k1gFnPc4	pochybnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
esejí	esej	k1gFnPc2	esej
<g/>
,	,	kIx,	,
dopisů	dopis	k1gInPc2	dopis
a	a	k8xC	a
pochvalných	pochvalný	k2eAgInPc2d1	pochvalný
článků	článek	k1gInPc2	článek
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
Richarda	Richard	k1gMnSc2	Richard
Dawkinse	Dawkins	k1gMnSc2	Dawkins
<g/>
,	,	kIx,	,
Stephena	Stephen	k1gMnSc2	Stephen
Frye	Fry	k1gMnSc2	Fry
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
vydání	vydání	k1gNnSc6	vydání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Christophera	Christopher	k1gMnSc4	Christopher
Cerfa	Cerf	k1gMnSc4	Cerf
(	(	kIx(	(
<g/>
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
vydání	vydání	k1gNnSc6	vydání
<g/>
)	)	kIx)	)
a	a	k8xC	a
Terryho	Terry	k1gMnSc2	Terry
Jonese	Jonese	k1gFnSc2	Jonese
(	(	kIx(	(
<g/>
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
brožovaném	brožovaný	k2eAgNnSc6d1	brožované
vydání	vydání	k1gNnSc6	vydání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
