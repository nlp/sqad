<p>
<s>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Běloruská	běloruský	k2eAgFnSc1d1	Běloruská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
bělorusky	bělorusky	k6eAd1	bělorusky
Р	Р	k?	Р
<g/>
́	́	k?	́
<g/>
б	б	k?	б
Б	Б	k?	Б
<g/>
́	́	k?	́
<g/>
с	с	k?	с
<g/>
,	,	kIx,	,
Respublika	Respublika	k1gFnSc1	Respublika
Belarus	Belarus	k1gMnSc1	Belarus
<g/>
;	;	kIx,	;
rusky	rusky	k6eAd1	rusky
Р	Р	k?	Р
<g/>
́	́	k?	́
<g/>
б	б	k?	б
Б	Б	k?	Б
<g/>
́	́	k?	́
<g/>
с	с	k?	с
<g/>
,	,	kIx,	,
Respublika	Respublika	k1gFnSc1	Respublika
Belarus	Belarus	k1gMnSc1	Belarus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
Litvou	Litva	k1gFnSc7	Litva
a	a	k8xC	a
Lotyšskem	Lotyšsko	k1gNnSc7	Lotyšsko
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
Ruskem	Rusko	k1gNnSc7	Rusko
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
východě	východ	k1gInSc6	východ
a	a	k8xC	a
s	s	k7c7	s
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Minsk	Minsk	k1gInSc1	Minsk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
necelých	celý	k2eNgInPc2d1	necelý
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
většinu	většina	k1gFnSc4	většina
tvoří	tvořit	k5eAaImIp3nP	tvořit
Bělorusové	Bělorus	k1gMnPc1	Bělorus
<g/>
,	,	kIx,	,
nejpoužívanějším	používaný	k2eAgInSc7d3	nejpoužívanější
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
běloruština	běloruština	k1gFnSc1	běloruština
úřední	úřední	k2eAgFnSc2d1	úřední
status	status	k1gInSc4	status
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
je	být	k5eAaImIp3nS	být
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nástupnických	nástupnický	k2eAgFnPc2d1	nástupnická
republik	republika	k1gFnPc2	republika
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
a	a	k8xC	a
zahraničně-politicky	zahraničněoliticky	k6eAd1	zahraničně-politicky
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
orientováno	orientovat	k5eAaBmNgNnS	orientovat
na	na	k7c4	na
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
roku	rok	k1gInSc3	rok
1997	[number]	k4	1997
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
je	být	k5eAaImIp3nS	být
nepřetržitě	přetržitě	k6eNd1	přetržitě
prezidentem	prezident	k1gMnSc7	prezident
země	zem	k1gFnSc2	zem
Alexandr	Alexandr	k1gMnSc1	Alexandr
Lukašenko	Lukašenka	k1gFnSc5	Lukašenka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
západními	západní	k2eAgFnPc7d1	západní
zeměmi	zem	k1gFnPc7	zem
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c4	za
nedemokratické	demokratický	k2eNgInPc4d1	nedemokratický
postupy	postup	k1gInPc4	postup
a	a	k8xC	a
potlačování	potlačování	k1gNnSc4	potlačování
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
stát	stát	k1gInSc1	stát
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
ho	on	k3xPp3gMnSc4	on
označil	označit	k5eAaPmAgMnS	označit
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
za	za	k7c4	za
"	"	kIx"	"
<g/>
poslední	poslední	k2eAgInSc1d1	poslední
totalitní	totalitní	k2eAgInSc1d1	totalitní
stát	stát	k1gInSc1	stát
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistiky	statistika	k1gFnSc2	statistika
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organizace	organizace	k1gFnSc2	organizace
je	být	k5eAaImIp3nS	být
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
první	první	k4xOgFnSc7	první
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
konzumací	konzumace	k1gFnSc7	konzumace
alkoholu	alkohol	k1gInSc2	alkohol
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
k	k	k7c3	k
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
Příchod	příchod	k1gInSc1	příchod
slovanských	slovanský	k2eAgInPc2d1	slovanský
kmenů	kmen	k1gInPc2	kmen
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
prvního	první	k4xOgNnSc2	první
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
usadily	usadit	k5eAaPmAgInP	usadit
tři	tři	k4xCgInPc1	tři
slovanské	slovanský	k2eAgInPc1d1	slovanský
kmenové	kmenový	k2eAgInPc1d1	kmenový
svazy	svaz	k1gInPc1	svaz
<g/>
:	:	kIx,	:
Kriviči	Krivič	k1gMnPc1	Krivič
<g/>
,	,	kIx,	,
Dregoviči	Dregovič	k1gMnPc1	Dregovič
a	a	k8xC	a
Radimiči	Radimič	k1gMnPc1	Radimič
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
běloruských	běloruský	k2eAgNnPc6d1	běloruské
knížectvích	knížectví	k1gNnPc6	knížectví
však	však	k9	však
pochází	pocházet	k5eAaImIp3nP	pocházet
až	až	k9	až
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
Polocké	Polocké	k2eAgNnSc6d1	Polocké
knížectví	knížectví	k1gNnSc6	knížectví
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
hlavních	hlavní	k2eAgInPc2d1	hlavní
celků	celek	k1gInPc2	celek
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
Rusi	Rus	k1gFnSc2	Rus
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
i	i	k9	i
další	další	k2eAgInPc1d1	další
mocné	mocný	k2eAgInPc1d1	mocný
útvary	útvar	k1gInPc1	útvar
<g/>
:	:	kIx,	:
Turovské	Turovský	k2eAgNnSc4d1	Turovský
<g/>
,	,	kIx,	,
Grodecké	Grodecký	k2eAgNnSc4d1	Grodecký
a	a	k8xC	a
Novogrodecké	Novogrodecký	k2eAgNnSc4d1	Novogrodecký
knížectví	knížectví	k1gNnSc4	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
992	[number]	k4	992
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
do	do	k7c2	do
běloruských	běloruský	k2eAgFnPc2d1	Běloruská
zemí	zem	k1gFnPc2	zem
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Misionáře	misionář	k1gMnSc4	misionář
vyslal	vyslat	k5eAaPmAgMnS	vyslat
z	z	k7c2	z
kyjevského	kyjevský	k2eAgNnSc2d1	Kyjevské
centra	centrum	k1gNnSc2	centrum
Vladimír	Vladimír	k1gMnSc1	Vladimír
I.	I.	kA	I.
Svatý	svatý	k1gMnSc1	svatý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
Rusi	Rus	k1gFnSc2	Rus
(	(	kIx(	(
<g/>
definitivně	definitivně	k6eAd1	definitivně
1240	[number]	k4	1240
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
náporem	nápor	k1gInSc7	nápor
Mongolů	Mongol	k1gMnPc2	Mongol
začala	začít	k5eAaPmAgFnS	začít
do	do	k7c2	do
mocensky	mocensky	k6eAd1	mocensky
rozdrobeného	rozdrobený	k2eAgInSc2d1	rozdrobený
prostoru	prostor	k1gInSc2	prostor
pronikat	pronikat	k5eAaImF	pronikat
litevská	litevský	k2eAgNnPc4d1	litevské
knížata	kníže	k1gNnPc4	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
tzv.	tzv.	kA	tzv.
Černé	Černá	k1gFnSc2	Černá
Rusi	Rus	k1gFnSc2	Rus
(	(	kIx(	(
<g/>
okolí	okolí	k1gNnSc2	okolí
Novogrodku	Novogrodek	k1gInSc2	Novogrodek
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
podřídil	podřídit	k5eAaPmAgMnS	podřídit
už	už	k9	už
kníže	kníže	k1gMnSc1	kníže
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
Mindaugas	Mindaugas	k1gMnSc1	Mindaugas
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1316	[number]	k4	1316
(	(	kIx(	(
<g/>
nástup	nástup	k1gInSc1	nástup
Gediminase	Gediminasa	k1gFnSc3	Gediminasa
<g/>
)	)	kIx)	)
už	už	k6eAd1	už
litevští	litevský	k2eAgMnPc1d1	litevský
panovníci	panovník	k1gMnPc1	panovník
systematicky	systematicky	k6eAd1	systematicky
připojovali	připojovat	k5eAaImAgMnP	připojovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
běloruské	běloruský	k2eAgFnPc4d1	Běloruská
a	a	k8xC	a
ukrajinské	ukrajinský	k2eAgFnPc4d1	ukrajinská
země	zem	k1gFnPc4	zem
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
soustátí	soustátí	k1gNnSc3	soustátí
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vzrůstající	vzrůstající	k2eAgInSc1d1	vzrůstající
tlak	tlak	k1gInSc1	tlak
německých	německý	k2eAgNnPc2d1	německé
vojsk	vojsko	k1gNnPc2	vojsko
(	(	kIx(	(
<g/>
Řád	řád	k1gInSc1	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
Řád	řád	k1gInSc1	řád
mečových	mečový	k2eAgMnPc2d1	mečový
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
)	)	kIx)	)
a	a	k8xC	a
nájezdy	nájezd	k1gInPc7	nájezd
Tatarů	Tatar	k1gMnPc2	Tatar
<g/>
/	/	kIx~	/
<g/>
Mongolů	Mongol	k1gMnPc2	Mongol
přispěl	přispět	k5eAaPmAgMnS	přispět
ke	k	k7c3	k
zformování	zformování	k1gNnSc3	zformování
litevsko-běloruské	litevskoěloruský	k2eAgFnSc2d1	litevsko-běloruský
jednoty	jednota	k1gFnSc2	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
Rus	Rus	k1gFnSc1	Rus
tvořila	tvořit	k5eAaImAgFnS	tvořit
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
Velikého	veliký	k2eAgNnSc2d1	veliké
knížectví	knížectví	k1gNnSc2	knížectví
litevského	litevský	k2eAgNnSc2d1	litevské
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
byl	být	k5eAaImAgInS	být
zpočátku	zpočátku	k6eAd1	zpočátku
Novogrodek	Novogrodek	k1gInSc1	Novogrodek
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1323	[number]	k4	1323
pak	pak	k9	pak
Vilnius	Vilnius	k1gInSc1	Vilnius
(	(	kIx(	(
<g/>
Vilno	Vilno	k1gNnSc1	Vilno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
éra	éra	k1gFnSc1	éra
tohoto	tento	k3xDgInSc2	tento
státního	státní	k2eAgInSc2d1	státní
útvaru	útvar	k1gInSc2	útvar
nastala	nastat	k5eAaPmAgFnS	nastat
zejména	zejména	k9	zejména
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Vitolda	Vitold	k1gMnSc2	Vitold
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Symboly	symbol	k1gInPc4	symbol
tohoto	tento	k3xDgInSc2	tento
státního	státní	k2eAgInSc2d1	státní
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
znak	znak	k1gInSc1	znak
zvaný	zvaný	k2eAgInSc1d1	zvaný
pahoňa	pahoňa	k6eAd1	pahoňa
a	a	k8xC	a
bílo-červeno-bílá	bílo-červenoílý	k2eAgFnSc1d1	bílo-červeno-bílá
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
v	v	k7c4	v
Bělorusku	Běloruska	k1gFnSc4	Běloruska
státními	státní	k2eAgInPc7d1	státní
orgány	orgán	k1gInPc7	orgán
vytlačovány	vytlačovat	k5eAaImNgInP	vytlačovat
z	z	k7c2	z
veřejného	veřejný	k2eAgInSc2d1	veřejný
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Rzeczypospolité	Rzeczypospolitý	k2eAgNnSc1d1	Rzeczypospolitý
(	(	kIx(	(
<g/>
raný	raný	k2eAgInSc1d1	raný
novověk	novověk	k1gInSc1	novověk
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Útoky	útok	k1gInPc1	útok
moskevských	moskevský	k2eAgNnPc2d1	moskevské
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c4	na
východní	východní	k2eAgFnPc4d1	východní
hranice	hranice	k1gFnPc4	hranice
země	zem	k1gFnSc2	zem
na	na	k7c6	na
konci	konec	k1gInSc6	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
si	se	k3xPyFc3	se
vynutily	vynutit	k5eAaPmAgFnP	vynutit
centralizaci	centralizace	k1gFnSc4	centralizace
státu	stát	k1gInSc2	stát
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
finančních	finanční	k2eAgInPc2d1	finanční
výdajů	výdaj	k1gInPc2	výdaj
na	na	k7c4	na
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ztratila	ztratit	k5eAaPmAgFnS	ztratit
slábnoucí	slábnoucí	k2eAgFnSc1d1	slábnoucí
Litva	Litva	k1gFnSc1	Litva
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
Bílé	bílý	k2eAgFnSc2d1	bílá
Rusi	Rus	k1gFnSc2	Rus
včetně	včetně	k7c2	včetně
města	město	k1gNnSc2	město
Smolenska	Smolensko	k1gNnSc2	Smolensko
(	(	kIx(	(
<g/>
1514	[number]	k4	1514
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1569	[number]	k4	1569
se	se	k3xPyFc4	se
Litva	Litva	k1gFnSc1	Litva
a	a	k8xC	a
sousední	sousední	k2eAgNnSc1d1	sousední
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
spojené	spojený	k2eAgFnPc4d1	spojená
pouze	pouze	k6eAd1	pouze
osobou	osoba	k1gFnSc7	osoba
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
sjednotily	sjednotit	k5eAaPmAgFnP	sjednotit
ve	v	k7c4	v
federativní	federativní	k2eAgInSc4d1	federativní
stát	stát	k1gInSc4	stát
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
Republika	republika	k1gFnSc1	republika
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Rzeczpospolita	Rzeczpospolit	k2eAgFnSc1d1	Rzeczpospolita
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Litvu	Litva	k1gFnSc4	Litva
a	a	k8xC	a
Bílou	bílý	k2eAgFnSc4d1	bílá
Rus	Rus	k1gFnSc4	Rus
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
ztrátu	ztráta	k1gFnSc4	ztráta
některých	některý	k3yIgNnPc2	některý
území	území	k1gNnPc2	území
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
a	a	k8xC	a
podřízení	podřízený	k1gMnPc1	podřízený
se	se	k3xPyFc4	se
svrchovanosti	svrchovanost	k1gFnSc2	svrchovanost
polského	polský	k2eAgNnSc2d1	polské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
také	také	k9	také
částečnou	částečný	k2eAgFnSc4d1	částečná
polonizaci	polonizace	k1gFnSc4	polonizace
(	(	kIx(	(
<g/>
především	především	k9	především
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
kléru	klér	k1gInSc2	klér
<g/>
)	)	kIx)	)
a	a	k8xC	a
katolizaci	katolizace	k1gFnSc4	katolizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1697	[number]	k4	1697
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
používat	používat	k5eAaImF	používat
běloruštinu	běloruština	k1gFnSc4	běloruština
v	v	k7c6	v
úředním	úřední	k2eAgInSc6d1	úřední
styku	styk	k1gInSc6	styk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
sjednocení	sjednocení	k1gNnSc4	sjednocení
tří	tři	k4xCgFnPc2	tři
historických	historický	k2eAgFnPc2d1	historická
zemí	zem	k1gFnPc2	zem
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
tlaku	tlak	k1gInSc3	tlak
Ruského	ruský	k2eAgNnSc2d1	ruské
carství	carství	k1gNnSc2	carství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Republiky	republika	k1gFnSc2	republika
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
byla	být	k5eAaImAgFnS	být
běloruština	běloruština	k1gFnSc1	běloruština
silně	silně	k6eAd1	silně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
polštinou	polština	k1gFnSc7	polština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1772	[number]	k4	1772
<g/>
–	–	k?	–
<g/>
1795	[number]	k4	1795
se	se	k3xPyFc4	se
dělením	dělení	k1gNnSc7	dělení
Polska	Polsko	k1gNnSc2	Polsko
země	zem	k1gFnSc2	zem
postupně	postupně	k6eAd1	postupně
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Běloruština	běloruština	k1gFnSc1	běloruština
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
omezována	omezován	k2eAgFnSc1d1	omezována
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
ruštiny	ruština	k1gFnSc2	ruština
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
pouze	pouze	k6eAd1	pouze
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
řeckokatolická	řeckokatolický	k2eAgFnSc1d1	řeckokatolická
církev	církev	k1gFnSc1	církev
uznávající	uznávající	k2eAgFnSc4d1	uznávající
autoritu	autorita	k1gFnSc4	autorita
papeže	papež	k1gMnSc2	papež
byla	být	k5eAaImAgFnS	být
potlačována	potlačovat	k5eAaImNgFnS	potlačovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1830	[number]	k4	1830
<g/>
/	/	kIx~	/
<g/>
31	[number]	k4	31
pak	pak	k6eAd1	pak
propuklo	propuknout	k5eAaPmAgNnS	propuknout
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Kongresovce	Kongresovka	k1gFnSc6	Kongresovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Litvě	Litva	k1gFnSc6	Litva
a	a	k8xC	a
Bílé	bílý	k2eAgFnSc6d1	bílá
Rusi	Rus	k1gFnSc6	Rus
protiruské	protiruský	k2eAgNnSc4d1	protiruské
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
tvrdě	tvrdě	k6eAd1	tvrdě
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
silná	silný	k2eAgFnSc1d1	silná
rusifikace	rusifikace	k1gFnSc1	rusifikace
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
zesílilo	zesílit	k5eAaPmAgNnS	zesílit
národní	národní	k2eAgNnSc1d1	národní
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
usilovalo	usilovat	k5eAaImAgNnS	usilovat
o	o	k7c4	o
samosprávu	samospráva	k1gFnSc4	samospráva
a	a	k8xC	a
opětovné	opětovný	k2eAgNnSc4d1	opětovné
zavedení	zavedení	k1gNnSc4	zavedení
běloruštiny	běloruština	k1gFnSc2	běloruština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zhroucení	zhroucení	k1gNnSc6	zhroucení
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
se	se	k3xPyFc4	se
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1917	[number]	k4	1917
konal	konat	k5eAaImAgInS	konat
Všeběloruský	Všeběloruský	k2eAgInSc1d1	Všeběloruský
sjezd	sjezd	k1gInSc1	sjezd
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
byla	být	k5eAaImAgFnS	být
zvolena	zvolen	k2eAgFnSc1d1	zvolena
vláda	vláda	k1gFnSc1	vláda
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jednání	jednání	k1gNnSc1	jednání
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
následně	následně	k6eAd1	následně
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1918	[number]	k4	1918
musela	muset	k5eAaImAgFnS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
armádě	armáda	k1gFnSc3	armáda
německé	německý	k2eAgFnPc1d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Političtí	politický	k2eAgMnPc1d1	politický
představitelé	představitel	k1gMnPc1	představitel
museli	muset	k5eAaImAgMnP	muset
uprchnout	uprchnout	k5eAaPmF	uprchnout
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Politického	politický	k2eAgInSc2d1	politický
zmatku	zmatek	k1gInSc2	zmatek
využila	využít	k5eAaPmAgFnS	využít
bolševická	bolševický	k2eAgFnSc1d1	bolševická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
volby	volba	k1gFnPc4	volba
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1918	[number]	k4	1918
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Běloruskou	běloruský	k2eAgFnSc4d1	Běloruská
lidovou	lidový	k2eAgFnSc4d1	lidová
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
prozatím	prozatím	k6eAd1	prozatím
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
na	na	k7c6	na
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
však	však	k9	však
brzy	brzy	k6eAd1	brzy
znovu	znovu	k6eAd1	znovu
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
Minska	Minsk	k1gInSc2	Minsk
a	a	k8xC	a
bolševici	bolševik	k1gMnPc1	bolševik
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
Běloruskou	běloruský	k2eAgFnSc4d1	Běloruská
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sovětsko-polské	sovětskoolský	k2eAgFnSc6d1	sovětsko-polská
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
připadla	připadnout	k5eAaPmAgFnS	připadnout
její	její	k3xOp3gFnSc1	její
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
–	–	k?	–
tzv.	tzv.	kA	tzv.
Kresy	Kresa	k1gFnSc2	Kresa
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
třetina	třetina	k1gFnSc1	třetina
území	území	k1gNnSc2	území
včetně	včetně	k7c2	včetně
Grodna	Grodno	k1gNnSc2	Grodno
a	a	k8xC	a
Brestu	Brest	k1gInSc2	Brest
Litevského	litevský	k2eAgInSc2d1	litevský
<g/>
)	)	kIx)	)
–	–	k?	–
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgFnSc1d1	zbývající
část	část	k1gFnSc1	část
včetně	včetně	k7c2	včetně
Minsku	Minsk	k1gInSc2	Minsk
pak	pak	k6eAd1	pak
zůstala	zůstat	k5eAaPmAgFnS	zůstat
Běloruské	běloruský	k2eAgFnPc4d1	Běloruská
SSR	SSR	kA	SSR
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Nacionalistická	nacionalistický	k2eAgFnSc1d1	nacionalistická
Druhá	druhý	k4xOgFnSc1	druhý
Polská	polský	k2eAgFnSc1d1	polská
republika	republika	k1gFnSc1	republika
zavírala	zavírat	k5eAaImAgFnS	zavírat
v	v	k7c6	v
Kresech	Kreso	k1gNnPc6	Kreso
běloruské	běloruský	k2eAgFnSc2d1	Běloruská
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
potlačovala	potlačovat	k5eAaImAgFnS	potlačovat
používání	používání	k1gNnSc4	používání
běloruštiny	běloruština	k1gFnSc2	běloruština
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
září	září	k1gNnSc6	září
1939	[number]	k4	1939
necelé	celý	k2eNgFnPc4d1	necelá
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
invazi	invaze	k1gFnSc6	invaze
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Polsko	Polsko	k1gNnSc1	Polsko
z	z	k7c2	z
východu	východ	k1gInSc2	východ
přepadeno	přepaden	k2eAgNnSc1d1	přepadeno
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
;	;	kIx,	;
polská	polský	k2eAgFnSc1d1	polská
část	část	k1gFnSc1	část
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
připojena	připojit	k5eAaPmNgFnS	připojit
k	k	k7c3	k
Běloruské	běloruský	k2eAgFnPc4d1	Běloruská
SSR	SSR	kA	SSR
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
zahájena	zahájen	k2eAgFnSc1d1	zahájena
kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
způsobila	způsobit	k5eAaPmAgFnS	způsobit
Bělorusku	Běloruska	k1gFnSc4	Běloruska
obrovské	obrovský	k2eAgFnSc2d1	obrovská
škody	škoda	k1gFnSc2	škoda
a	a	k8xC	a
ztrátu	ztráta	k1gFnSc4	ztráta
2,2	[number]	k4	2,2
miliónu	milión	k4xCgInSc2	milión
lidských	lidský	k2eAgInPc2d1	lidský
životů	život	k1gInPc2	život
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
předválečné	předválečný	k2eAgFnSc2d1	předválečná
populace	populace	k1gFnSc2	populace
(	(	kIx(	(
<g/>
okupační	okupační	k2eAgFnSc1d1	okupační
německá	německý	k2eAgFnSc1d1	německá
moc	moc	k1gFnSc1	moc
tu	tu	k6eAd1	tu
jako	jako	k8xS	jako
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgNnSc4d1	silné
partyzánské	partyzánský	k2eAgNnSc4d1	partyzánské
hnutí	hnutí	k1gNnSc4	hnutí
uplatňovala	uplatňovat	k5eAaImAgFnS	uplatňovat
taktiku	taktika	k1gFnSc4	taktika
spálené	spálený	k2eAgFnSc2d1	spálená
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
vyvražděny	vyvražděn	k2eAgFnPc1d1	vyvražděna
byly	být	k5eAaImAgFnP	být
stovky	stovka	k1gFnPc1	stovka
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
současná	současný	k2eAgFnSc1d1	současná
polsko-sovětská	polskoovětský	k2eAgFnSc1d1	polsko-sovětská
hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nově	nově	k6eAd1	nově
probíhala	probíhat	k5eAaImAgFnS	probíhat
asi	asi	k9	asi
o	o	k7c4	o
200	[number]	k4	200
km	km	kA	km
západněji	západně	k6eAd2	západně
někdejším	někdejší	k2eAgNnPc3d1	někdejší
polským	polský	k2eAgNnPc3d1	polské
vnitrozemím	vnitrozemí	k1gNnPc3	vnitrozemí
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
polské	polský	k2eAgFnSc2d1	polská
národnosti	národnost	k1gFnSc2	národnost
na	na	k7c6	na
získaných	získaný	k2eAgNnPc6d1	získané
západních	západní	k2eAgNnPc6d1	západní
územích	území	k1gNnPc6	území
Běloruské	běloruský	k2eAgFnSc2d1	Běloruská
SSR	SSR	kA	SSR
byla	být	k5eAaImAgFnS	být
nuceně	nuceně	k6eAd1	nuceně
repatriována	repatriovat	k5eAaBmNgFnS	repatriovat
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
poskytnuta	poskytnut	k2eAgFnSc1d1	poskytnuta
Běloruské	běloruský	k2eAgFnSc2d1	Běloruská
SSR	SSR	kA	SSR
centrálně	centrálně	k6eAd1	centrálně
řízená	řízený	k2eAgFnSc1d1	řízená
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
pomoc	pomoc	k1gFnSc1	pomoc
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
významně	významně	k6eAd1	významně
pomohla	pomoct	k5eAaPmAgFnS	pomoct
k	k	k7c3	k
obnově	obnova	k1gFnSc3	obnova
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
Běloruská	běloruský	k2eAgFnSc1d1	Běloruská
SSR	SSR	kA	SSR
dosídlována	dosídlován	k2eAgFnSc1d1	dosídlován
Rusy	Rus	k1gMnPc4	Rus
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
také	také	k9	také
dalšími	další	k2eAgInPc7d1	další
národy	národ	k1gInPc7	národ
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
;	;	kIx,	;
současně	současně	k6eAd1	současně
opět	opět	k6eAd1	opět
zesílila	zesílit	k5eAaPmAgFnS	zesílit
rusifikace	rusifikace	k1gFnSc1	rusifikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
bylo	být	k5eAaImAgNnS	být
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
postiženo	postihnout	k5eAaPmNgNnS	postihnout
černobylskou	černobylský	k2eAgFnSc7d1	Černobylská
jadernou	jaderný	k2eAgFnSc7d1	jaderná
havárií	havárie	k1gFnSc7	havárie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nezávislost	nezávislost	k1gFnSc4	nezávislost
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1990	[number]	k4	1990
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
svou	svůj	k3xOyFgFnSc4	svůj
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
a	a	k8xC	a
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
rozpadu	rozpad	k1gInSc2	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
nakonec	nakonec	k6eAd1	nakonec
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
i	i	k8xC	i
plnou	plný	k2eAgFnSc4d1	plná
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Bělorusku	Běloruska	k1gFnSc4	Běloruska
vládne	vládnout	k5eAaImIp3nS	vládnout
prezident	prezident	k1gMnSc1	prezident
Alexandr	Alexandr	k1gMnSc1	Alexandr
Lukašenko	Lukašenka	k1gFnSc5	Lukašenka
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nečekaně	nečekaně	k6eAd1	nečekaně
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
protikorupčnímu	protikorupční	k2eAgInSc3d1	protikorupční
programu	program	k1gInSc3	program
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
posílil	posílit	k5eAaPmAgInS	posílit
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
mu	on	k3xPp3gNnSc3	on
byli	být	k5eAaImAgMnP	být
protikandidáty	protikandidát	k1gMnPc4	protikandidát
společný	společný	k2eAgMnSc1d1	společný
kandidát	kandidát	k1gMnSc1	kandidát
běloruské	běloruský	k2eAgFnSc2d1	Běloruská
opozice	opozice	k1gFnSc2	opozice
Aljaksandar	Aljaksandar	k1gMnSc1	Aljaksandar
Milinkevič	Milinkevič	k1gMnSc1	Milinkevič
<g/>
,	,	kIx,	,
Sjarhej	Sjarhej	k1gMnSc1	Sjarhej
Hajdukevič	Hajdukevič	k1gMnSc1	Hajdukevič
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
kandidát	kandidát	k1gMnSc1	kandidát
opozice	opozice	k1gFnSc2	opozice
Aljaksandar	Aljaksandar	k1gMnSc1	Aljaksandar
Kazulin	Kazulin	k2eAgMnSc1d1	Kazulin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
další	další	k2eAgFnPc1d1	další
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
získali	získat	k5eAaPmAgMnP	získat
Lukašenkovi	Lukašenkův	k2eAgMnPc1d1	Lukašenkův
protikandidáti	protikandidát	k1gMnPc1	protikandidát
zisk	zisk	k1gInSc4	zisk
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
jednotek	jednotka	k1gFnPc2	jednotka
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Opozice	opozice	k1gFnSc1	opozice
označila	označit	k5eAaPmAgFnS	označit
volby	volba	k1gFnPc4	volba
za	za	k7c4	za
zmanipulované	zmanipulovaný	k2eAgNnSc4d1	zmanipulované
<g/>
,	,	kIx,	,
následovaly	následovat	k5eAaImAgFnP	následovat
pokojné	pokojný	k2eAgFnPc1d1	pokojná
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
režim	režim	k1gInSc4	režim
tvrdě	tvrdě	k6eAd1	tvrdě
rozehnal	rozehnat	k5eAaPmAgMnS	rozehnat
a	a	k8xC	a
hlavní	hlavní	k2eAgMnPc4d1	hlavní
představitele	představitel	k1gMnPc4	představitel
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
několika	několik	k4yIc2	několik
prezidentských	prezidentský	k2eAgMnPc2d1	prezidentský
protikandidátů	protikandidát	k1gMnPc2	protikandidát
<g/>
)	)	kIx)	)
uvěznil	uvěznit	k5eAaPmAgMnS	uvěznit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poloha	poloha	k1gFnSc1	poloha
===	===	k?	===
</s>
</p>
<p>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
207	[number]	k4	207
595	[number]	k4	595
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
84	[number]	k4	84
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
největším	veliký	k2eAgMnSc7d3	veliký
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
2,6	[number]	k4	2,6
<g/>
×	×	k?	×
větším	většit	k5eAaImIp1nS	většit
než	než	k8xS	než
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
má	mít	k5eAaImIp3nS	mít
418	[number]	k4	418
km	km	kA	km
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
640	[number]	k4	640
km	km	kA	km
s	s	k7c7	s
Litvou	Litva	k1gFnSc7	Litva
<g/>
,	,	kIx,	,
161	[number]	k4	161
km	km	kA	km
s	s	k7c7	s
Lotyšskem	Lotyšsko	k1gNnSc7	Lotyšsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
1	[number]	k4	1
312	[number]	k4	312
km	km	kA	km
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
1	[number]	k4	1
111	[number]	k4	111
km	km	kA	km
s	s	k7c7	s
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
nížinné	nížinný	k2eAgFnSc6d1	nížinná
oblasti	oblast	k1gFnSc6	oblast
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Východoevropské	východoevropský	k2eAgFnSc2d1	východoevropská
roviny	rovina	k1gFnSc2	rovina
s	s	k7c7	s
nevelkými	velký	k2eNgInPc7d1	nevelký
výškovými	výškový	k2eAgInPc7d1	výškový
rozdíly	rozdíl	k1gInPc7	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
345	[number]	k4	345
m.	m.	k?	m.
n.	n.	k?	n.
m.	m.	k?	m.
vrch	vrch	k1gInSc1	vrch
Dzjaržynskaja	Dzjaržynskaja	k1gMnSc1	Dzjaržynskaja
hara	hara	k1gMnSc1	hara
západně	západně	k6eAd1	západně
od	od	k7c2	od
Minsku	Minsk	k1gInSc2	Minsk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vodstvo	vodstvo	k1gNnSc1	vodstvo
===	===	k?	===
</s>
</p>
<p>
<s>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
povodí	povodí	k1gNnSc2	povodí
několika	několik	k4yIc2	několik
velkých	velký	k2eAgFnPc2d1	velká
evropských	evropský	k2eAgFnPc2d1	Evropská
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
současně	současně	k6eAd1	současně
na	na	k7c4	na
rozvodí	rozvodí	k1gNnSc4	rozvodí
Černého	Černého	k2eAgNnSc2d1	Černého
a	a	k8xC	a
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
řek	řeka	k1gFnPc2	řeka
(	(	kIx(	(
<g/>
delších	dlouhý	k2eAgFnPc2d2	delší
než	než	k8xS	než
5	[number]	k4	5
km	km	kA	km
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
51	[number]	k4	51
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
říční	říční	k2eAgFnSc2d1	říční
sítě	síť	k1gFnSc2	síť
je	být	k5eAaImIp3nS	být
0,2	[number]	k4	0,2
až	až	k9	až
0,28	[number]	k4	0,28
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
řeky	řeka	k1gFnPc1	řeka
jsou	být	k5eAaImIp3nP	být
Dněpr	Dněpr	k1gInSc4	Dněpr
(	(	kIx(	(
<g/>
s	s	k7c7	s
přítoky	přítok	k1gInPc7	přítok
Pripjať	Pripjať	k1gMnPc2	Pripjať
<g/>
,	,	kIx,	,
Sož	Sož	k1gMnPc2	Sož
a	a	k8xC	a
Berezina	Berezina	k1gFnSc1	Berezina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Západní	západní	k2eAgFnSc1d1	západní
Dvina	Dvina	k1gFnSc1	Dvina
<g/>
,	,	kIx,	,
Němen	Němen	k1gInSc1	Němen
(	(	kIx(	(
<g/>
s	s	k7c7	s
přítokem	přítok	k1gInSc7	přítok
Vilija	Vilij	k1gInSc2	Vilij
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
horní	horní	k2eAgInSc4d1	horní
tok	tok	k1gInSc4	tok
řeky	řeka	k1gFnSc2	řeka
Lovati	Lovat	k2eAgMnPc1d1	Lovat
(	(	kIx(	(
<g/>
povodí	povodit	k5eAaPmIp3nP	povodit
Něvy	Něva	k1gFnPc1	Něva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
řek	řeka	k1gFnPc2	řeka
je	být	k5eAaImIp3nS	být
smíšený	smíšený	k2eAgInSc1d1	smíšený
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
dešťového	dešťový	k2eAgInSc2d1	dešťový
a	a	k8xC	a
sněhového	sněhový	k2eAgInSc2d1	sněhový
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
řeky	řeka	k1gFnPc4	řeka
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
vysoké	vysoký	k2eAgInPc1d1	vysoký
stavy	stav	k1gInPc1	stav
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Západní	západní	k2eAgFnSc2d1	západní
Dviny	Dvina	k1gFnSc2	Dvina
a	a	k8xC	a
Němenu	Němen	k1gInSc2	Němen
i	i	k9	i
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
pro	pro	k7c4	pro
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
splavování	splavování	k1gNnSc4	splavování
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
i	i	k9	i
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
řeky	řeka	k1gFnPc1	řeka
přijímají	přijímat	k5eAaImIp3nP	přijímat
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
vysoušených	vysoušený	k2eAgFnPc2d1	vysoušená
bažin	bažina	k1gFnPc2	bažina
a	a	k8xC	a
mokřin	mokřina	k1gFnPc2	mokřina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
charakteristické	charakteristický	k2eAgNnSc4d1	charakteristické
<g/>
.	.	kIx.	.
</s>
<s>
Splavné	splavný	k2eAgFnPc1d1	splavná
jsou	být	k5eAaImIp3nP	být
Dněpr	Dněpr	k1gInSc1	Dněpr
<g/>
,	,	kIx,	,
Berezina	Berezina	k1gFnSc1	Berezina
a	a	k8xC	a
Němen	Němen	k1gInSc1	Němen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Pripjati	Pripjat	k2eAgMnPc1d1	Pripjat
lodě	loď	k1gFnPc1	loď
proplouvají	proplouvat	k5eAaImIp3nP	proplouvat
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Západní	západní	k2eAgFnSc2d1	západní
Bug	Bug	k1gFnSc2	Bug
přes	přes	k7c4	přes
Dněpersko-bugský	Dněperskougský	k2eAgInSc4d1	Dněpersko-bugský
kanál	kanál	k1gInSc4	kanál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
11	[number]	k4	11
000	[number]	k4	000
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Běloruská	běloruský	k2eAgNnPc1d1	běloruské
jezera	jezero	k1gNnPc1	jezero
jsou	být	k5eAaImIp3nP	být
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
po	po	k7c6	po
ustupujícím	ustupující	k2eAgInSc6d1	ustupující
ledovci	ledovec	k1gInSc6	ledovec
na	na	k7c6	na
konci	konec	k1gInSc6	konec
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgNnPc1d3	nejvýznamnější
hluboká	hluboký	k2eAgNnPc1d1	hluboké
jezera	jezero	k1gNnPc1	jezero
ledovcového	ledovcový	k2eAgInSc2d1	ledovcový
původu	původ	k1gInSc2	původ
rozmanitých	rozmanitý	k2eAgInPc2d1	rozmanitý
tvarů	tvar	k1gInPc2	tvar
pobřeží	pobřeží	k1gNnSc2	pobřeží
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
pro	pro	k7c4	pro
sever	sever	k1gInSc4	sever
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
jezero	jezero	k1gNnSc4	jezero
Narač	Narač	k1gInSc1	Narač
(	(	kIx(	(
<g/>
79,6	[number]	k4	79,6
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
Asvějské	Asvějský	k2eAgNnSc1d1	Asvějský
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
52,8	[number]	k4	52,8
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
jsou	být	k5eAaImIp3nP	být
jezera	jezero	k1gNnPc4	jezero
mělká	mělký	k2eAgNnPc4d1	mělké
a	a	k8xC	a
zarůstající	zarůstající	k2eAgNnPc4d1	zarůstající
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
říčního	říční	k2eAgInSc2d1	říční
a	a	k8xC	a
krasovo-sufozního	krasovoufozní	k2eAgInSc2d1	krasovo-sufozní
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
v	v	k7c6	v
Polesí	Polesí	k1gNnSc6	Polesí
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
mělkých	mělký	k2eAgNnPc2d1	mělké
jezer	jezero	k1gNnPc2	jezero
uprostřed	uprostřed	k7c2	uprostřed
bažin	bažina	k1gFnPc2	bažina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
Červené	červené	k1gNnSc4	červené
(	(	kIx(	(
<g/>
43,6	[number]	k4	43,6
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vyhanavské	Vyhanavský	k2eAgNnSc1d1	Vyhanavský
(	(	kIx(	(
<g/>
26	[number]	k4	26
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejhlubší	hluboký	k2eAgNnPc1d3	nejhlubší
jezera	jezero	k1gNnPc1	jezero
jsou	být	k5eAaImIp3nP	být
Dlouhé	Dlouhá	k1gFnPc1	Dlouhá
(	(	kIx(	(
<g/>
52,8	[number]	k4	52,8
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Riču	Riča	k1gFnSc4	Riča
(	(	kIx(	(
<g/>
51,9	[number]	k4	51,9
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hiňkava	Hiňkava	k1gFnSc1	Hiňkava
(	(	kIx(	(
<g/>
43,3	[number]	k4	43,3
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klima	klima	k1gNnSc1	klima
===	===	k?	===
</s>
</p>
<p>
<s>
Klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
mají	mít	k5eAaImIp3nP	mít
oproti	oproti	k7c3	oproti
střední	střední	k2eAgFnSc3d1	střední
Evropě	Evropa	k1gFnSc3	Evropa
výrazněji	výrazně	k6eAd2	výrazně
kontinentální	kontinentální	k2eAgInSc4d1	kontinentální
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
s	s	k7c7	s
chladnější	chladný	k2eAgFnSc7d2	chladnější
zimou	zima	k1gFnSc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
lednové	lednový	k2eAgFnPc1d1	lednová
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
-5,1	-5,1	k4	-5,1
na	na	k7c6	na
západě	západ	k1gInSc6	západ
do	do	k7c2	do
-7,5	-7,5	k4	-7,5
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
kolem	kolem	k7c2	kolem
18	[number]	k4	18
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
550	[number]	k4	550
<g/>
–	–	k?	–
<g/>
650	[number]	k4	650
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jaro	jaro	k1gNnSc4	jaro
a	a	k8xC	a
léto	léto	k1gNnSc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vlhkost	vlhkost	k1gFnSc1	vlhkost
podnebí	podnebí	k1gNnSc2	podnebí
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
vlivu	vliv	k1gInSc2	vliv
atlantského	atlantský	k2eAgNnSc2d1	Atlantské
proudění	proudění	k1gNnSc2	proudění
a	a	k8xC	a
blízkosti	blízkost	k1gFnSc2	blízkost
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Flóra	Flóra	k1gFnSc1	Flóra
a	a	k8xC	a
fauna	fauna	k1gFnSc1	fauna
===	===	k?	===
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
nadprůměrné	nadprůměrný	k2eAgNnSc4d1	nadprůměrné
zalesnění	zalesnění	k1gNnSc4	zalesnění
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
plochy	plocha	k1gFnSc2	plocha
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
převažuje	převažovat	k5eAaImIp3nS	převažovat
smrk	smrk	k1gInSc1	smrk
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
<g/>
,	,	kIx,	,
jedle	jedle	k1gFnSc1	jedle
<g/>
,	,	kIx,	,
bříza	bříza	k1gFnSc1	bříza
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
pak	pak	k6eAd1	pak
dub	dub	k1gInSc1	dub
<g/>
,	,	kIx,	,
buk	buk	k1gInSc1	buk
a	a	k8xC	a
jilm	jilm	k1gInSc1	jilm
<g/>
.	.	kIx.	.
</s>
<s>
Jehličnany	jehličnan	k1gInPc1	jehličnan
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
v	v	k7c4	v
Polesí	Polesí	k1gNnSc4	Polesí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fauna	fauna	k1gFnSc1	fauna
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
podstatně	podstatně	k6eAd1	podstatně
neliší	lišit	k5eNaImIp3nP	lišit
od	od	k7c2	od
středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
<g/>
.	.	kIx.	.
</s>
<s>
Běloruské	běloruský	k2eAgInPc1d1	běloruský
lesy	les	k1gInPc1	les
jsou	být	k5eAaImIp3nP	být
mimořádně	mimořádně	k6eAd1	mimořádně
bohaté	bohatý	k2eAgInPc1d1	bohatý
na	na	k7c4	na
živočišné	živočišný	k2eAgInPc4d1	živočišný
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
los	los	k1gInSc1	los
<g/>
,	,	kIx,	,
rys	rys	k1gInSc1	rys
<g/>
,	,	kIx,	,
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
pak	pak	k6eAd1	pak
vlk	vlk	k1gMnSc1	vlk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polsko-běloruském	polskoěloruský	k2eAgInSc6d1	polsko-běloruský
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Bělověžský	Bělověžský	k2eAgInSc4d1	Bělověžský
prales	prales	k1gInSc4	prales
lze	lze	k6eAd1	lze
pak	pak	k6eAd1	pak
spatřit	spatřit	k5eAaPmF	spatřit
zubry	zubr	k1gMnPc4	zubr
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
prales	prales	k1gInSc1	prales
zapsán	zapsat	k5eAaPmNgInS	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
přírodního	přírodní	k2eAgNnSc2d1	přírodní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Bělorusové	Bělorus	k1gMnPc1	Bělorus
tvoří	tvořit	k5eAaImIp3nP	tvořit
81,2	[number]	k4	81,2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
menšinou	menšina	k1gFnSc7	menšina
jsou	být	k5eAaImIp3nP	být
Rusové	Rus	k1gMnPc1	Rus
s	s	k7c7	s
11,4	[number]	k4	11,4
%	%	kIx~	%
<g/>
,	,	kIx,	,
následováni	následován	k2eAgMnPc1d1	následován
Poláky	Polák	k1gMnPc7	Polák
(	(	kIx(	(
<g/>
3,9	[number]	k4	3,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ukrajinci	Ukrajinec	k1gMnSc3	Ukrajinec
(	(	kIx(	(
<g/>
2,4	[number]	k4	2,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
v	v	k7c6	v
Běloruské	běloruský	k2eAgFnSc6d1	Běloruská
SSR	SSR	kA	SSR
tvořili	tvořit	k5eAaImAgMnP	tvořit
Bělorusové	Bělorus	k1gMnPc1	Bělorus
80,6	[number]	k4	80,6
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
Židé	Žid	k1gMnPc1	Žid
8,2	[number]	k4	8,2
%	%	kIx~	%
a	a	k8xC	a
Rusové	Rusové	k2eAgMnSc1d1	Rusové
7,7	[number]	k4	7,7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
situace	situace	k1gFnSc1	situace
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiálními	oficiální	k2eAgInPc7d1	oficiální
jazyky	jazyk	k1gInPc7	jazyk
státu	stát	k1gInSc2	stát
jsou	být	k5eAaImIp3nP	být
běloruština	běloruština	k1gFnSc1	běloruština
a	a	k8xC	a
ruština	ruština	k1gFnSc1	ruština
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
byla	být	k5eAaImAgFnS	být
však	však	k9	však
běloruština	běloruština	k1gFnSc1	běloruština
vystavena	vystaven	k2eAgFnSc1d1	vystavena
útlaku	útlak	k1gInSc2	útlak
a	a	k8xC	a
preferována	preferován	k2eAgFnSc1d1	preferována
byla	být	k5eAaImAgFnS	být
státními	státní	k2eAgMnPc7d1	státní
orgány	orgán	k1gInPc1	orgán
ruština	ruština	k1gFnSc1	ruština
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
běloruská	běloruský	k2eAgFnSc1d1	Běloruská
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
trendu	trend	k1gInSc6	trend
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
rusifikace	rusifikace	k1gFnSc1	rusifikace
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
praxi	praxe	k1gFnSc6	praxe
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
je	být	k5eAaImIp3nS	být
užívána	užíván	k2eAgFnSc1d1	užívána
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
tzv.	tzv.	kA	tzv.
trasjanka	trasjanka	k1gFnSc1	trasjanka
(	(	kIx(	(
<g/>
směs	směs	k1gFnSc1	směs
obou	dva	k4xCgInPc2	dva
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
obdoba	obdoba	k1gFnSc1	obdoba
suržyku	suržyku	k6eAd1	suržyku
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
spisovná	spisovný	k2eAgFnSc1d1	spisovná
běloruština	běloruština	k1gFnSc1	běloruština
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
předreformní	předreformní	k2eAgFnSc6d1	předreformní
podobě	podoba	k1gFnSc6	podoba
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
taraškevica	taraškevica	k1gFnSc1	taraškevica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
znakem	znak	k1gInSc7	znak
spíše	spíše	k9	spíše
národně	národně	k6eAd1	národně
orientované	orientovaný	k2eAgFnSc2d1	orientovaná
inteligence	inteligence	k1gFnSc2	inteligence
a	a	k8xC	a
protilukašenkovské	protilukašenkovský	k2eAgFnSc2d1	protilukašenkovský
opozice	opozice	k1gFnSc2	opozice
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
společenském	společenský	k2eAgInSc6d1	společenský
životě	život	k1gInSc6	život
a	a	k8xC	a
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
jsou	být	k5eAaImIp3nP	být
státním	státní	k2eAgInSc7d1	státní
aparátem	aparát	k1gInSc7	aparát
potlačovány	potlačovat	k5eAaImNgFnP	potlačovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Zhruba	zhruba	k6eAd1	zhruba
80	[number]	k4	80
%	%	kIx~	%
věřících	věřící	k2eAgMnPc2d1	věřící
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
pravoslaví	pravoslaví	k1gNnSc3	pravoslaví
<g/>
,	,	kIx,	,
10	[number]	k4	10
%	%	kIx~	%
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
příslušníci	příslušník	k1gMnPc1	příslušník
polské	polský	k2eAgFnSc2d1	polská
menšiny	menšina	k1gFnSc2	menšina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbylá	zbylý	k2eAgNnPc1d1	zbylé
procenta	procento	k1gNnPc1	procento
představují	představovat	k5eAaImIp3nP	představovat
protestanti	protestant	k1gMnPc1	protestant
<g/>
,	,	kIx,	,
řečtí	řecký	k2eAgMnPc1d1	řecký
katolíci	katolík	k1gMnPc1	katolík
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
uniaté	uniat	k1gMnPc1	uniat
<g/>
)	)	kIx)	)
,	,	kIx,	,
židé	žid	k1gMnPc1	žid
a	a	k8xC	a
muslimové	muslim	k1gMnPc1	muslim
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Lipkové	Lipková	k1gFnSc2	Lipková
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
20	[number]	k4	20
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Početná	početný	k2eAgFnSc1d1	početná
část	část	k1gFnSc1	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
<g/>
Původně	původně	k6eAd1	původně
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Bílé	bílý	k2eAgFnSc2d1	bílá
Rusi	Rus	k1gFnSc2	Rus
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
sílily	sílit	k5eAaImAgFnP	sílit
přestupy	přestup	k1gInPc4	přestup
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
)	)	kIx)	)
k	k	k7c3	k
protestantským	protestantský	k2eAgFnPc3d1	protestantská
církvím	církev	k1gFnPc3	církev
a	a	k8xC	a
ke	k	k7c3	k
katolicismu	katolicismus	k1gInSc3	katolicismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
brestlitevské	brestlitevský	k2eAgFnSc2d1	brestlitevský
unie	unie	k1gFnSc2	unie
roku	rok	k1gInSc2	rok
1596	[number]	k4	1596
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řeckokatolická	řeckokatolický	k2eAgFnSc1d1	řeckokatolická
církev	církev	k1gFnSc1	církev
(	(	kIx(	(
<g/>
a	a	k8xC	a
pravoslaví	pravoslaví	k1gNnSc1	pravoslaví
bylo	být	k5eAaImAgNnS	být
zároveň	zároveň	k6eAd1	zároveň
postaveno	postavit	k5eAaPmNgNnS	postavit
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrubé	Hrubé	k2eAgFnPc1d1	Hrubé
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
všeobecné	všeobecný	k2eAgNnSc1d1	všeobecné
prosazení	prosazení	k1gNnSc1	prosazení
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
ohromný	ohromný	k2eAgInSc4d1	ohromný
odpor	odpor	k1gInSc4	odpor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
povstání	povstání	k1gNnSc2	povstání
ve	v	k7c4	v
Vitebsku	Vitebska	k1gFnSc4	Vitebska
<g/>
)	)	kIx)	)
pravoslavného	pravoslavný	k2eAgNnSc2d1	pravoslavné
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
i	i	k8xC	i
většiny	většina	k1gFnSc2	většina
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nakonec	nakonec	k6eAd1	nakonec
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
roku	rok	k1gInSc2	rok
1635	[number]	k4	1635
pravoslaví	pravoslaví	k1gNnSc2	pravoslaví
opět	opět	k6eAd1	opět
oficiálně	oficiálně	k6eAd1	oficiálně
povoleno	povolit	k5eAaPmNgNnS	povolit
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
běloruský	běloruský	k2eAgInSc1d1	běloruský
lid	lid	k1gInSc1	lid
a	a	k8xC	a
nižší	nízký	k2eAgFnSc1d2	nižší
šlechta	šlechta	k1gFnSc1	šlechta
vnímali	vnímat	k5eAaImAgMnP	vnímat
řecké	řecký	k2eAgNnSc4d1	řecké
katolictví	katolictví	k1gNnSc4	katolictví
jako	jako	k8xS	jako
vnucovanou	vnucovaný	k2eAgFnSc4d1	vnucovaná
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
získávalo	získávat	k5eAaImAgNnS	získávat
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
staletích	staletí	k1gNnPc6	staletí
své	svůj	k3xOyFgMnPc4	svůj
stoupence	stoupenec	k1gMnPc4	stoupenec
i	i	k9	i
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
vrstvách	vrstva	k1gFnPc6	vrstva
a	a	k8xC	a
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
uniatství	uniatství	k1gNnSc1	uniatství
chápáno	chápat	k5eAaImNgNnS	chápat
a	a	k8xC	a
vyzdvihováno	vyzdvihován	k2eAgNnSc1d1	vyzdvihováno
jako	jako	k8xC	jako
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
rys	rys	k1gInSc1	rys
běloruského	běloruský	k2eAgInSc2d1	běloruský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vnucovaného	vnucovaný	k2eAgInSc2d1	vnucovaný
–	–	k?	–
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
–	–	k?	–
pravoslaví	pravoslaví	k1gNnSc2	pravoslaví
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
carské	carský	k2eAgFnSc2d1	carská
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
však	však	k9	však
uniaté	uniat	k1gMnPc1	uniat
nezískali	získat	k5eNaPmAgMnP	získat
tak	tak	k6eAd1	tak
silné	silný	k2eAgFnPc4d1	silná
pozice	pozice	k1gFnPc4	pozice
jako	jako	k8xC	jako
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Rusi	Rus	k1gFnSc6	Rus
<g/>
/	/	kIx~	/
<g/>
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
početná	početný	k2eAgFnSc1d1	početná
komunita	komunita	k1gFnSc1	komunita
běloruských	běloruský	k2eAgMnPc2d1	běloruský
židů	žid	k1gMnPc2	žid
byla	být	k5eAaImAgFnS	být
zdecimována	zdecimovat	k5eAaPmNgFnS	zdecimovat
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
odešel	odejít	k5eAaPmAgInS	odejít
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původně	původně	k6eAd1	původně
sedmisettisícové	sedmisettisícový	k2eAgFnSc2d1	sedmisettisícový
komunity	komunita	k1gFnSc2	komunita
tak	tak	k6eAd1	tak
zbývá	zbývat	k5eAaImIp3nS	zbývat
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
ateistů	ateista	k1gMnPc2	ateista
je	být	k5eAaImIp3nS	být
mj.	mj.	kA	mj.
důsledkem	důsledek	k1gInSc7	důsledek
omezení	omezení	k1gNnSc2	omezení
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
církevního	církevní	k2eAgInSc2d1	církevní
vlivu	vliv	k1gInSc2	vliv
na	na	k7c6	na
školství	školství	k1gNnSc6	školství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
==	==	k?	==
</s>
</p>
<p>
<s>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
je	být	k5eAaImIp3nS	být
průmyslově-zemědělský	průmyslověemědělský	k2eAgInSc4d1	průmyslově-zemědělský
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
jsou	být	k5eAaImIp3nP	být
strojírenství	strojírenství	k1gNnPc4	strojírenství
<g/>
,	,	kIx,	,
těžba	těžba	k1gFnSc1	těžba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc1	zpracování
draselných	draselný	k2eAgFnPc2d1	draselná
solí	sůl	k1gFnPc2	sůl
<g/>
,	,	kIx,	,
papírenský	papírenský	k2eAgInSc4d1	papírenský
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
a	a	k8xC	a
chemický	chemický	k2eAgInSc4d1	chemický
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
převažuje	převažovat	k5eAaImIp3nS	převažovat
živočišná	živočišný	k2eAgFnSc1d1	živočišná
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
chov	chov	k1gInSc1	chov
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
koní	kůň	k1gMnPc2	kůň
a	a	k8xC	a
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
výrobou	výroba	k1gFnSc7	výroba
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
<g/>
.	.	kIx.	.
</s>
<s>
Pěstují	pěstovat	k5eAaImIp3nP	pěstovat
se	se	k3xPyFc4	se
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
žito	žito	k1gNnSc1	žito
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
a	a	k8xC	a
len	len	k1gInSc1	len
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
za	za	k7c4	za
rok	rok	k1gInSc4	rok
je	být	k5eAaImIp3nS	být
18066	[number]	k4	18066
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převažuje	převažovat	k5eAaImIp3nS	převažovat
tepelná	tepelný	k2eAgFnSc1d1	tepelná
energetika	energetika	k1gFnSc1	energetika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nepovedené	povedený	k2eNgFnSc3d1	nepovedená
transformaci	transformace	k1gFnSc3	transformace
na	na	k7c4	na
volnotržní	volnotržní	k2eAgFnSc4d1	volnotržní
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991-1994	[number]	k4	1991-1994
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
o	o	k7c4	o
30	[number]	k4	30
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
míra	míra	k1gFnSc1	míra
inflace	inflace	k1gFnSc1	inflace
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
byla	být	k5eAaImAgFnS	být
2000	[number]	k4	2000
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
žilo	žít	k5eAaImAgNnS	žít
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
životního	životní	k2eAgNnSc2d1	životní
minima	minimum	k1gNnSc2	minimum
60	[number]	k4	60
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
využil	využít	k5eAaPmAgMnS	využít
Alexander	Alexandra	k1gFnPc2	Alexandra
Lukašeno	Lukašen	k2eAgNnSc1d1	Lukašen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
Bělorusům	Bělorus	k1gMnPc3	Bělorus
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
model	model	k1gInSc1	model
"	"	kIx"	"
<g/>
tržního	tržní	k2eAgInSc2d1	tržní
socialismu	socialismus	k1gInSc2	socialismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lukašenkův	Lukašenkův	k2eAgInSc1d1	Lukašenkův
režim	režim	k1gInSc1	režim
zastavil	zastavit	k5eAaPmAgInS	zastavit
privatizaci	privatizace	k1gFnSc4	privatizace
a	a	k8xC	a
zachoval	zachovat	k5eAaPmAgMnS	zachovat
si	se	k3xPyFc3	se
státní	státní	k2eAgFnSc4d1	státní
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
zhruba	zhruba	k6eAd1	zhruba
70	[number]	k4	70
procenty	procento	k1gNnPc7	procento
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
cenami	cena	k1gFnPc7	cena
a	a	k8xC	a
měnovou	měnový	k2eAgFnSc7d1	měnová
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
<s>
Lukašenkovi	Lukašenek	k1gMnSc3	Lukašenek
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
ekonomiku	ekonomika	k1gFnSc4	ekonomika
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
zastabilizovat	zastabilizovat	k5eAaBmF	zastabilizovat
a	a	k8xC	a
makroekonomické	makroekonomický	k2eAgInPc4d1	makroekonomický
ukazatele	ukazatel	k1gInPc4	ukazatel
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
zemí	zem	k1gFnPc2	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
úrovně	úroveň	k1gFnSc2	úroveň
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
začala	začít	k5eAaPmAgFnS	začít
i	i	k9	i
opatrná	opatrný	k2eAgFnSc1d1	opatrná
privatizace	privatizace	k1gFnSc1	privatizace
(	(	kIx(	(
<g/>
menších	malý	k2eAgInPc2d2	menší
podniků	podnik	k1gInPc2	podnik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
jen	jen	k9	jen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vzrostlo	vzrůst	k5eAaPmAgNnS	vzrůst
o	o	k7c4	o
9,2	[number]	k4	9,2
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Inflace	inflace	k1gFnSc1	inflace
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
klesla	klesnout	k5eAaPmAgFnS	klesnout
na	na	k7c4	na
8	[number]	k4	8
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
z	z	k7c2	z
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
trojciferných	trojciferný	k2eAgFnPc2d1	trojciferná
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výsledky	výsledek	k1gInPc1	výsledek
ostře	ostro	k6eAd1	ostro
kontrastovaly	kontrastovat	k5eAaImAgInP	kontrastovat
zejména	zejména	k9	zejména
se	s	k7c7	s
stagnací	stagnace	k1gFnSc7	stagnace
sousední	sousední	k2eAgFnSc2d1	sousední
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
Lukašenko	Lukašenka	k1gFnSc5	Lukašenka
také	také	k6eAd1	také
silně	silně	k6eAd1	silně
využíval	využívat	k5eAaImAgInS	využívat
v	v	k7c6	v
propagandě	propaganda	k1gFnSc6	propaganda
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInPc1d1	státní
zásahy	zásah	k1gInPc1	zásah
do	do	k7c2	do
ekonomiky	ekonomika	k1gFnSc2	ekonomika
ji	on	k3xPp3gFnSc4	on
nicméně	nicméně	k8xC	nicméně
stejně	stejně	k6eAd1	stejně
destabilizovaly	destabilizovat	k5eAaBmAgFnP	destabilizovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
měnové	měnový	k2eAgFnSc6d1	měnová
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
znovu	znovu	k6eAd1	znovu
rozjetou	rozjetý	k2eAgFnSc7d1	rozjetá
inflací	inflace	k1gFnSc7	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgInP	být
před	před	k7c7	před
prezidentskými	prezidentský	k2eAgFnPc7d1	prezidentská
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zvýšeny	zvýšen	k2eAgInPc4d1	zvýšen
platy	plat	k1gInPc4	plat
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
státních	státní	k2eAgInPc2d1	státní
podniků	podnik	k1gInPc2	podnik
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
(	(	kIx(	(
<g/>
na	na	k7c4	na
500	[number]	k4	500
$	$	kIx~	$
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
inflace	inflace	k1gFnSc1	inflace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
znovu	znovu	k6eAd1	znovu
trojciferné	trojciferný	k2eAgFnPc4d1	trojciferná
hodnoty	hodnota	k1gFnPc4	hodnota
-	-	kIx~	-
108,7	[number]	k4	108,7
%	%	kIx~	%
Stát	stát	k1gInSc1	stát
reagoval	reagovat	k5eAaBmAgMnS	reagovat
devalvací	devalvace	k1gFnPc2	devalvace
měny	měna	k1gFnSc2	měna
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
reálné	reálný	k2eAgFnPc1d1	reálná
mzdy	mzda	k1gFnPc1	mzda
poklesly	poklesnout	k5eAaPmAgFnP	poklesnout
z	z	k7c2	z
530	[number]	k4	530
<g/>
$	$	kIx~	$
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2010	[number]	k4	2010
na	na	k7c4	na
330	[number]	k4	330
<g/>
$	$	kIx~	$
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
příčinou	příčina	k1gFnSc7	příčina
krize	krize	k1gFnSc2	krize
byl	být	k5eAaImAgInS	být
deficit	deficit	k1gInSc1	deficit
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Výkon	výkon	k1gInSc1	výkon
běloruské	běloruský	k2eAgFnSc2d1	Běloruská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
se	se	k3xPyFc4	se
však	však	k9	však
znovu	znovu	k6eAd1	znovu
výrazně	výrazně	k6eAd1	výrazně
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
uznaly	uznat	k5eAaPmAgFnP	uznat
i	i	k9	i
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
ratingové	ratingový	k2eAgFnPc1d1	ratingová
agentury	agentura	k1gFnPc1	agentura
a	a	k8xC	a
OECD	OECD	kA	OECD
<g/>
.	.	kIx.	.
<g/>
Běloruská	běloruský	k2eAgFnSc1d1	Běloruská
ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
a	a	k8xC	a
je	on	k3xPp3gNnPc4	on
jí	jíst	k5eAaImIp3nS	jíst
specifickým	specifický	k2eAgInSc7d1	specifický
způsobem	způsob	k1gInSc7	způsob
i	i	k9	i
dotována	dotován	k2eAgFnSc1d1	dotována
-	-	kIx~	-
mezi	mezi	k7c7	mezi
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
existuje	existovat	k5eAaImIp3nS	existovat
bezcelní	bezcelní	k2eAgFnSc1d1	bezcelní
unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ropa	ropa	k1gFnSc1	ropa
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
na	na	k7c4	na
běloruské	běloruský	k2eAgNnSc4d1	běloruské
území	území	k1gNnSc4	území
teče	teč	k1gFnSc2	teč
ropovody	ropovod	k1gInPc4	ropovod
bez	bez	k7c2	bez
cla	clo	k1gNnSc2	clo
a	a	k8xC	a
Bělorusové	Bělorus	k1gMnPc1	Bělorus
clo	clo	k1gNnSc1	clo
za	za	k7c4	za
ruskou	ruský	k2eAgFnSc4d1	ruská
ropu	ropa	k1gFnSc4	ropa
vybírají	vybírat	k5eAaImIp3nP	vybírat
až	až	k9	až
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
hranicích	hranice	k1gFnPc6	hranice
se	s	k7c7	s
třetími	třetí	k4xOgFnPc7	třetí
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
de	de	k?	de
facto	facto	k1gNnSc1	facto
reexport	reexport	k1gInSc1	reexport
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
utužoval	utužovat	k5eAaImAgInS	utužovat
politické	politický	k2eAgFnPc4d1	politická
a	a	k8xC	a
geopolitické	geopolitický	k2eAgFnPc4d1	geopolitická
vazby	vazba	k1gFnPc4	vazba
na	na	k7c4	na
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
poněkud	poněkud	k6eAd1	poněkud
oslabily	oslabit	k5eAaPmAgFnP	oslabit
po	po	k7c6	po
ruském	ruský	k2eAgNnSc6d1	ruské
anektování	anektování	k1gNnSc6	anektování
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
Krymu	Krym	k1gInSc2	Krym
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
se	se	k3xPyFc4	se
zhoršují	zhoršovat	k5eAaImIp3nP	zhoršovat
i	i	k9	i
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
dopady	dopad	k1gInPc4	dopad
i	i	k9	i
na	na	k7c4	na
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
hrozí	hrozit	k5eAaImIp3nS	hrozit
rozpad	rozpad	k1gInSc1	rozpad
bezcelní	bezcelní	k2eAgFnSc2d1	bezcelní
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
posiluje	posilovat	k5eAaImIp3nS	posilovat
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
vazby	vazba	k1gFnPc4	vazba
s	s	k7c7	s
EU	EU	kA	EU
<g/>
.	.	kIx.	.
<g/>
Země	země	k1gFnSc1	země
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
minimální	minimální	k2eAgFnPc4d1	minimální
hodnoty	hodnota	k1gFnPc4	hodnota
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
pohlíží	pohlížet	k5eAaImIp3nS	pohlížet
jako	jako	k9	jako
na	na	k7c4	na
nepřípustný	přípustný	k2eNgInSc4d1	nepřípustný
sociální	sociální	k2eAgInSc4d1	sociální
jev	jev	k1gInSc4	jev
-	-	kIx~	-
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
platí	platit	k5eAaImIp3nS	platit
zákonná	zákonný	k2eAgFnSc1d1	zákonná
pracovní	pracovní	k2eAgFnSc1d1	pracovní
povinnost	povinnost	k1gFnSc1	povinnost
jako	jako	k9	jako
v	v	k7c6	v
sovětských	sovětský	k2eAgInPc6d1	sovětský
časech	čas	k1gInPc6	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
===	===	k?	===
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
turisticky	turisticky	k6eAd1	turisticky
atraktivní	atraktivní	k2eAgFnSc1d1	atraktivní
zemí	zem	k1gFnSc7	zem
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
však	však	k9	však
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
minimální	minimální	k2eAgFnSc7d1	minimální
propagací	propagace	k1gFnSc7	propagace
a	a	k8xC	a
omezenými	omezený	k2eAgFnPc7d1	omezená
podmínkami	podmínka	k1gFnPc7	podmínka
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
turistického	turistický	k2eAgInSc2d1	turistický
ruchu	ruch	k1gInSc2	ruch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
za	za	k7c4	za
pozornost	pozornost	k1gFnSc4	pozornost
určitě	určitě	k6eAd1	určitě
stojí	stát	k5eAaImIp3nS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Minsk	Minsk	k1gInSc1	Minsk
bylo	být	k5eAaImAgNnS	být
bohužel	bohužel	k9	bohužel
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zničeno	zničen	k2eAgNnSc4d1	zničeno
a	a	k8xC	a
jen	jen	k9	jen
nepatrná	patrný	k2eNgFnSc1d1	patrný
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
obnovy	obnova	k1gFnPc4	obnova
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
jen	jen	k9	jen
málo	málo	k1gNnSc4	málo
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
budovy	budova	k1gFnPc1	budova
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
,	,	kIx,	,
bernardinský	bernardinský	k2eAgInSc1d1	bernardinský
klášter	klášter	k1gInSc1	klášter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
návštěvníka	návštěvník	k1gMnSc2	návštěvník
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
upoutat	upoutat	k5eAaPmF	upoutat
především	především	k9	především
Trojické	trojický	k2eAgNnSc4d1	Trojické
předměstí	předměstí	k1gNnSc4	předměstí
na	na	k7c6	na
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
okraji	okraj	k1gInSc6	okraj
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
běloruská	běloruský	k2eAgFnSc1d1	Běloruská
metropole	metropole	k1gFnSc1	metropole
skvělou	skvělý	k2eAgFnSc7d1	skvělá
ukázkou	ukázka	k1gFnSc7	ukázka
sovětského	sovětský	k2eAgInSc2d1	sovětský
funkcionalistického	funkcionalistický	k2eAgInSc2d1	funkcionalistický
stavebního	stavební	k2eAgInSc2d1	stavební
stylu	styl	k1gInSc2	styl
a	a	k8xC	a
urbanismu	urbanismus	k1gInSc2	urbanismus
<g/>
:	:	kIx,	:
velké	velký	k2eAgFnPc1d1	velká
funkcionalistické	funkcionalistický	k2eAgFnPc1d1	funkcionalistická
budovy	budova	k1gFnPc1	budova
<g/>
,	,	kIx,	,
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
parky	park	k1gInPc1	park
a	a	k8xC	a
široké	široký	k2eAgInPc1d1	široký
prospekty	prospekt	k1gInPc1	prospekt
jsou	být	k5eAaImIp3nP	být
typickým	typický	k2eAgInSc7d1	typický
obrazem	obraz	k1gInSc7	obraz
Minska	Minsk	k1gInSc2	Minsk
<g/>
,	,	kIx,	,
vytvořeným	vytvořený	k2eAgNnSc7d1	vytvořené
v	v	k7c6	v
poválečných	poválečný	k2eAgNnPc6d1	poválečné
desetiletích	desetiletí	k1gNnPc6	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Minsk	Minsk	k1gInSc1	Minsk
je	být	k5eAaImIp3nS	být
také	také	k9	také
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
:	:	kIx,	:
najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
osm	osm	k4xCc4	osm
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
šest	šest	k4xCc4	šest
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
historicky	historicky	k6eAd1	historicky
či	či	k8xC	či
kulturně	kulturně	k6eAd1	kulturně
zajímavějších	zajímavý	k2eAgNnPc2d2	zajímavější
měst	město	k1gNnPc2	město
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaBmF	jmenovat
Grodno	Grodno	k1gNnSc4	Grodno
(	(	kIx(	(
<g/>
zachovaná	zachovaný	k2eAgFnSc1d1	zachovaná
část	část	k1gFnSc1	část
historické	historický	k2eAgFnSc2d1	historická
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vitebsk	Vitebsk	k1gInSc1	Vitebsk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
rodištěm	rodiště	k1gNnSc7	rodiště
Marka	Marek	k1gMnSc2	Marek
Chagalla	Chagall	k1gMnSc2	Chagall
<g/>
,	,	kIx,	,
či	či	k8xC	či
Polock	Polock	k1gInSc1	Polock
bohatý	bohatý	k2eAgInSc1d1	bohatý
na	na	k7c4	na
sakrální	sakrální	k2eAgFnPc4d1	sakrální
památky	památka	k1gFnPc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
a	a	k8xC	a
přitažlivou	přitažlivý	k2eAgFnSc7d1	přitažlivá
krajinou	krajina	k1gFnSc7	krajina
je	být	k5eAaImIp3nS	být
Polesí	Polesí	k1gNnSc1	Polesí
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zalesněnou	zalesněný	k2eAgFnSc4d1	zalesněná
nížinu	nížina	k1gFnSc4	nížina
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Pripjať	Pripjať	k1gFnSc2	Pripjať
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
leží	ležet	k5eAaImIp3nS	ležet
již	již	k6eAd1	již
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
nulovým	nulový	k2eAgInSc7d1	nulový
spádem	spád	k1gInSc7	spád
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
vytváření	vytváření	k1gNnSc1	vytváření
obrovských	obrovský	k2eAgFnPc2d1	obrovská
mokřin	mokřina	k1gFnPc2	mokřina
podél	podél	k6eAd1	podél
Pripjati	Pripjat	k2eAgMnPc1d1	Pripjat
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
přítoků	přítok	k1gInPc2	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Bělověžský	Bělověžský	k2eAgInSc1d1	Bělověžský
prales	prales	k1gInSc1	prales
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
láká	lákat	k5eAaImIp3nS	lákat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
běloruským	běloruský	k2eAgMnSc7d1	běloruský
spisovatelem	spisovatel	k1gMnSc7	spisovatel
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
Janka	Janka	k1gFnSc1	Janka
Kupala	Kupala	k1gFnSc1	Kupala
<g/>
.	.	kIx.	.
</s>
<s>
Světlana	Světlana	k1gFnSc1	Světlana
Alexijevičová	Alexijevičový	k2eAgFnSc1d1	Alexijevičový
získala	získat	k5eAaPmAgFnS	získat
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
hranice	hranice	k1gFnSc2	hranice
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
pronikli	proniknout	k5eAaPmAgMnP	proniknout
rovněž	rovněž	k9	rovněž
autoři	autor	k1gMnPc1	autor
jako	jako	k8xC	jako
Jakub	Jakub	k1gMnSc1	Jakub
Kolas	Kolas	k1gMnSc1	Kolas
<g/>
,	,	kIx,	,
Maksim	Maksim	k1gMnSc1	Maksim
Bahdanovič	Bahdanovič	k1gMnSc1	Bahdanovič
<g/>
,	,	kIx,	,
Vasil	Vasil	k1gMnSc1	Vasil
Bykav	Bykav	k1gInSc1	Bykav
<g/>
,	,	kIx,	,
Uladzimir	Uladzimir	k1gMnSc1	Uladzimir
Karatkevič	Karatkevič	k1gMnSc1	Karatkevič
<g/>
,	,	kIx,	,
Alěs	Alěs	k1gInSc1	Alěs
Adamovič	Adamovič	k1gInSc1	Adamovič
či	či	k8xC	či
Ryhor	Ryhor	k1gInSc1	Ryhor
Baradulin	Baradulin	k2eAgInSc1d1	Baradulin
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zakladatelům	zakladatel	k1gMnPc3	zakladatel
běloruského	běloruský	k2eAgNnSc2d1	běloruské
písemnictví	písemnictví	k1gNnSc2	písemnictví
patří	patřit	k5eAaImIp3nS	patřit
Cyril	Cyril	k1gMnSc1	Cyril
Turovský	Turovský	k1gMnSc1	Turovský
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Skoryna	Skoryno	k1gNnSc2	Skoryno
přeložil	přeložit	k5eAaPmAgMnS	přeložit
do	do	k7c2	do
běloruštiny	běloruština	k1gFnSc2	běloruština
Bibli	bible	k1gFnSc4	bible
<g/>
.	.	kIx.	.
</s>
<s>
Vyšla	vyjít	k5eAaPmAgFnS	vyjít
roku	rok	k1gInSc2	rok
1517	[number]	k4	1517
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
jako	jako	k9	jako
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
tištěná	tištěný	k2eAgFnSc1d1	tištěná
kniha	kniha	k1gFnSc1	kniha
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
Skoryna	Skoryn	k1gInSc2	Skoryn
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
císařský	císařský	k2eAgMnSc1d1	císařský
zahradník	zahradník	k1gMnSc1	zahradník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skoryna	Skoryna	k6eAd1	Skoryna
též	též	k9	též
kodifikoval	kodifikovat	k5eAaBmAgMnS	kodifikovat
staroběloruštinu	staroběloruština	k1gFnSc4	staroběloruština
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
moderní	moderní	k2eAgFnSc2d1	moderní
běloruštiny	běloruština	k1gFnSc2	běloruština
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
časopis	časopis	k1gInSc1	časopis
Naša	Naša	k?	Naša
niva	niva	k1gFnSc1	niva
založený	založený	k2eAgInSc1d1	založený
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
(	(	kIx(	(
<g/>
vychází	vycházet	k5eAaImIp3nS	vycházet
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
generace	generace	k1gFnPc1	generace
jeho	jeho	k3xOp3gMnPc2	jeho
autorů	autor	k1gMnPc2	autor
byla	být	k5eAaImAgFnS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
našanivská	našanivský	k2eAgFnSc1d1	našanivský
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
hudbě	hudba	k1gFnSc6	hudba
dokázal	dokázat	k5eAaPmAgInS	dokázat
získat	získat	k5eAaPmF	získat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
úspěch	úspěch	k1gInSc1	úspěch
houslista	houslista	k1gMnSc1	houslista
Alexander	Alexandra	k1gFnPc2	Alexandra
Rybak	Rybak	k1gMnSc1	Rybak
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
ovšem	ovšem	k9	ovšem
norské	norský	k2eAgNnSc4d1	norské
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
hudebním	hudební	k2eAgInSc7d1	hudební
festivalem	festival	k1gInSc7	festival
je	být	k5eAaImIp3nS	být
Slavjanski	Slavjansk	k1gMnSc3	Slavjansk
bazar	bazar	k1gInSc4	bazar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
ve	v	k7c6	v
Vitebsku	Vitebsek	k1gInSc6	Vitebsek
a	a	k8xC	a
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
primárně	primárně	k6eAd1	primárně
na	na	k7c4	na
populární	populární	k2eAgFnSc4d1	populární
hudbu	hudba	k1gFnSc4	hudba
ze	z	k7c2	z
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
soutěží	soutěžit	k5eAaImIp3nP	soutěžit
i	i	k9	i
umělci	umělec	k1gMnPc1	umělec
z	z	k7c2	z
neslovanských	slovanský	k2eNgInPc2d1	neslovanský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
vítězem	vítěz	k1gMnSc7	vítěz
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
již	již	k6eAd1	již
i	i	k9	i
zástupce	zástupce	k1gMnSc1	zástupce
Izraele	Izrael	k1gInSc2	Izrael
či	či	k8xC	či
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
<g/>
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
prestiž	prestiž	k1gFnSc4	prestiž
má	mít	k5eAaImIp3nS	mít
baletní	baletní	k2eAgInSc1d1	baletní
soubor	soubor	k1gInSc1	soubor
Národního	národní	k2eAgNnSc2d1	národní
akademického	akademický	k2eAgNnSc2d1	akademické
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Minsku	Minsk	k1gInSc6	Minsk
(	(	kIx(	(
<g/>
Н	Н	k?	Н
а	а	k?	а
В	В	k?	В
т	т	k?	т
о	о	k?	о
і	і	k?	і
б	б	k?	б
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
choreograf	choreograf	k1gMnSc1	choreograf
Valentin	Valentin	k1gMnSc1	Valentin
Elizarev	Elizarev	k1gFnSc4	Elizarev
získal	získat	k5eAaPmAgMnS	získat
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
prestižní	prestižní	k2eAgFnSc4d1	prestižní
Cenu	cena	k1gFnSc4	cena
Benois	Benois	k1gFnPc2	Benois
de	de	k?	de
la	la	k1gNnSc4	la
danse	danse	k1gFnSc2	danse
<g/>
.	.	kIx.	.
</s>
<s>
Hereckou	herecký	k2eAgFnSc7d1	herecká
hvězdou	hvězda	k1gFnSc7	hvězda
sovětského	sovětský	k2eAgInSc2d1	sovětský
filmu	film	k1gInSc2	film
byl	být	k5eAaImAgMnS	být
Innokentij	Innokentij	k1gMnSc1	Innokentij
Smoktunovskij	Smoktunovskij	k1gMnSc1	Smoktunovskij
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
současného	současný	k2eAgNnSc2d1	současné
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
i	i	k9	i
řada	řada	k1gFnSc1	řada
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
vnímány	vnímat	k5eAaImNgInP	vnímat
spíše	spíše	k9	spíše
jako	jako	k9	jako
Poláci	Polák	k1gMnPc1	Polák
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
spisovatel	spisovatel	k1gMnSc1	spisovatel
Adam	Adam	k1gMnSc1	Adam
Mickiewicz	Mickiewicz	k1gMnSc1	Mickiewicz
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Stanisław	Stanisław	k1gMnSc1	Stanisław
Moniuszko	Moniuszka	k1gFnSc5	Moniuszka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
kdysi	kdysi	k6eAd1	kdysi
sídlila	sídlit	k5eAaImAgFnS	sídlit
též	též	k9	též
významná	významný	k2eAgFnSc1d1	významná
židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
narození	narození	k1gNnSc2	narození
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
má	mít	k5eAaImIp3nS	mít
tudíž	tudíž	k8xC	tudíž
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životopise	životopis	k1gInSc6	životopis
mnoho	mnoho	k4c4	mnoho
židovských	židovský	k2eAgFnPc2d1	židovská
osobností	osobnost	k1gFnPc2	osobnost
–	–	k?	–
malíř	malíř	k1gMnSc1	malíř
Marc	Marc	k1gFnSc4	Marc
Chagall	Chagalla	k1gFnPc2	Chagalla
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Solomon	Solomona	k1gFnPc2	Solomona
Michoels	Michoels	k1gInSc1	Michoels
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Chaï	Chaï	k1gMnSc5	Chaï
Soutine	Soutin	k1gMnSc5	Soutin
<g/>
,	,	kIx,	,
sochař	sochařit	k5eAaImRp2nS	sochařit
Ossip	Ossip	k1gInSc4	Ossip
Zadkine	Zadkin	k1gMnSc5	Zadkin
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Irving	Irving	k1gInSc4	Irving
Berlin	berlina	k1gFnPc2	berlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
byly	být	k5eAaImAgFnP	být
zapsány	zapsat	k5eAaPmNgFnP	zapsat
dvě	dva	k4xCgFnPc1	dva
významné	významný	k2eAgFnPc1d1	významná
architektonické	architektonický	k2eAgFnPc1d1	architektonická
památky	památka	k1gFnPc1	památka
<g/>
:	:	kIx,	:
Mirský	Mirský	k2eAgInSc1d1	Mirský
zámek	zámek	k1gInSc1	zámek
a	a	k8xC	a
Zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
Ňasviži	Ňasviž	k1gFnSc6	Ňasviž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kuchyně	kuchyně	k1gFnSc2	kuchyně
==	==	k?	==
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
Češi	Čech	k1gMnPc1	Čech
využívají	využívat	k5eAaPmIp3nP	využívat
Bělorusové	Bělorus	k1gMnPc1	Bělorus
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kuchyni	kuchyně	k1gFnSc6	kuchyně
často	často	k6eAd1	často
houby	houba	k1gFnSc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Podávají	podávat	k5eAaImIp3nP	podávat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
jídlo	jídlo	k1gNnSc1	jídlo
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
hřiby	hřib	k1gInPc1	hřib
na	na	k7c6	na
smetaně	smetana	k1gFnSc6	smetana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
polévka	polévka	k1gFnSc1	polévka
<g/>
,	,	kIx,	,
omáčka	omáčka	k1gFnSc1	omáčka
i	i	k8xC	i
jako	jako	k9	jako
krém	krém	k1gInSc4	krém
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
základní	základní	k2eAgFnSc7d1	základní
surovinou	surovina	k1gFnSc7	surovina
jsou	být	k5eAaImIp3nP	být
brambory	brambor	k1gInPc1	brambor
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
je	být	k5eAaImIp3nS	být
připravováno	připravován	k2eAgNnSc1d1	připravováno
asi	asi	k9	asi
nejtypičtější	typický	k2eAgNnSc1d3	nejtypičtější
běloruské	běloruský	k2eAgNnSc1d1	běloruské
jídlo	jídlo	k1gNnSc1	jídlo
-	-	kIx~	-
draniki	draniki	k1gNnSc1	draniki
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
obdoba	obdoba	k1gFnSc1	obdoba
bramboráku	bramborák	k1gInSc2	bramborák
<g/>
.	.	kIx.	.
</s>
<s>
Palačinkám	palačinka	k1gFnPc3	palačinka
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgFnPc4d1	podobná
tzv.	tzv.	kA	tzv.
bliny	blina	k1gFnPc4	blina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ruské	ruský	k2eAgFnSc2d1	ruská
kuchyně	kuchyně	k1gFnSc2	kuchyně
Bělorusové	Bělorus	k1gMnPc1	Bělorus
převzali	převzít	k5eAaPmAgMnP	převzít
boršč	boršč	k1gInSc4	boršč
<g/>
.	.	kIx.	.
</s>
<s>
Minsk	Minsk	k1gInSc1	Minsk
převzal	převzít	k5eAaPmAgInS	převzít
více	hodně	k6eAd2	hodně
středoevropské	středoevropský	k2eAgInPc4d1	středoevropský
vlivy	vliv	k1gInPc4	vliv
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
poznat	poznat	k5eAaPmF	poznat
na	na	k7c6	na
oblibě	obliba	k1gFnSc6	obliba
řízku	řízek	k1gInSc2	řízek
či	či	k8xC	či
specialita	specialita	k1gFnSc1	specialita
filet	fileta	k1gFnPc2	fileta
á	á	k0	á
la	la	k1gNnSc3	la
Minsk	Minsk	k1gInSc1	Minsk
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
česnek	česnek	k1gInSc1	česnek
a	a	k8xC	a
kmín	kmín	k1gInSc1	kmín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
místní	místní	k2eAgFnSc7d1	místní
pálenkou	pálenka	k1gFnSc7	pálenka
je	být	k5eAaImIp3nS	být
kvas	kvas	k1gInSc1	kvas
<g/>
,	,	kIx,	,
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
ze	z	k7c2	z
sladu	slad	k1gInSc2	slad
<g/>
,	,	kIx,	,
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
máty	máta	k1gFnSc2	máta
peprné	peprný	k2eAgFnSc2d1	peprná
a	a	k8xC	a
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc7d1	místní
specialitou	specialita	k1gFnSc7	specialita
je	být	k5eAaImIp3nS	být
likér	likér	k1gInSc1	likér
Běloveskaja	Běloveskaj	k1gInSc2	Běloveskaj
<g/>
,	,	kIx,	,
vyráběný	vyráběný	k2eAgMnSc1d1	vyráběný
ze	z	k7c2	z
stovky	stovka	k1gFnSc2	stovka
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
se	se	k3xPyFc4	se
pije	pít	k5eAaImIp3nS	pít
také	také	k9	také
vodka	vodka	k1gFnSc1	vodka
<g/>
.	.	kIx.	.
</s>
<s>
Neblaze	blaze	k6eNd1	blaze
proslulé	proslulý	k2eAgNnSc1d1	proslulé
je	on	k3xPp3gNnSc4	on
levné	levný	k2eAgNnSc4d1	levné
jablečné	jablečný	k2eAgNnSc4d1	jablečné
víno	víno	k1gNnSc4	víno
čarnila	čarnit	k5eAaPmAgFnS	čarnit
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
konzumací	konzumace	k1gFnSc7	konzumace
v	v	k7c6	v
nejnižších	nízký	k2eAgFnPc6d3	nejnižší
vrstvách	vrstva	k1gFnPc6	vrstva
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
vláda	vláda	k1gFnSc1	vláda
snaží	snažit	k5eAaImIp3nS	snažit
bojovat	bojovat	k5eAaImF	bojovat
<g/>
.	.	kIx.	.
<g/>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
neslušné	slušný	k2eNgNnSc4d1	neslušné
jíst	jíst	k5eAaImF	jíst
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
také	také	k9	také
vůbec	vůbec	k9	vůbec
neprosadilo	prosadit	k5eNaPmAgNnS	prosadit
rychlé	rychlý	k2eAgNnSc4d1	rychlé
občerstvení	občerstvení	k1gNnSc4	občerstvení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
běloruským	běloruský	k2eAgFnPc3d1	Běloruská
vědeckým	vědecký	k2eAgFnPc3d1	vědecká
osobnostem	osobnost	k1gFnPc3	osobnost
patří	patřit	k5eAaImIp3nS	patřit
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
Žores	Žores	k1gMnSc1	Žores
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Alfjorov	Alfjorovo	k1gNnPc2	Alfjorovo
a	a	k8xC	a
letecký	letecký	k2eAgMnSc1d1	letecký
konstruktér	konstruktér	k1gMnSc1	konstruktér
Pavel	Pavel	k1gMnSc1	Pavel
Suchoj	Suchoj	k1gInSc4	Suchoj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
sovětských	sovětský	k2eAgFnPc2d1	sovětská
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
fyzik	fyzik	k1gMnSc1	fyzik
Jakov	Jakov	k1gInSc4	Jakov
Borisovič	Borisovič	k1gMnSc1	Borisovič
Zeldovič	Zeldovič	k1gMnSc1	Zeldovič
<g/>
.	.	kIx.	.
</s>
<s>
Běloruský	běloruský	k2eAgInSc1d1	běloruský
původ	původ	k1gInSc1	původ
měl	mít	k5eAaImAgInS	mít
rovněž	rovněž	k9	rovněž
psycholog	psycholog	k1gMnSc1	psycholog
Lev	Lev	k1gMnSc1	Lev
Vygotskij	Vygotskij	k1gMnSc1	Vygotskij
či	či	k8xC	či
matematik	matematik	k1gMnSc1	matematik
Otto	Otto	k1gMnSc1	Otto
Šmidt	Šmidt	k1gMnSc1	Šmidt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běloruském	běloruský	k2eAgInSc6d1	běloruský
Kobrynu	Kobryn	k1gInSc6	Kobryn
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
též	též	k9	též
americký	americký	k2eAgMnSc1d1	americký
matematik	matematik	k1gMnSc1	matematik
Oscar	Oscar	k1gMnSc1	Oscar
Zariski	Zariski	k1gNnPc2	Zariski
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otcem	otec	k1gMnSc7	otec
běloruské	běloruský	k2eAgFnSc2d1	Běloruská
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
je	být	k5eAaImIp3nS	být
kněz	kněz	k1gMnSc1	kněz
Cyril	Cyril	k1gMnSc1	Cyril
Turovský	Turovský	k1gMnSc1	Turovský
<g/>
.	.	kIx.	.
</s>
<s>
Lingvista	lingvista	k1gMnSc1	lingvista
Branislav	Branislav	k1gMnSc1	Branislav
Taraškievič	Taraškievič	k1gMnSc1	Taraškievič
je	být	k5eAaImIp3nS	být
tvůrcem	tvůrce	k1gMnSc7	tvůrce
spisovné	spisovný	k2eAgFnSc2d1	spisovná
běloruštiny	běloruština	k1gFnSc2	běloruština
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
-	-	kIx~	-
taraškievica	taraškievica	k6eAd1	taraškievica
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
získal	získat	k5eAaPmAgMnS	získat
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
běloruský	běloruský	k2eAgMnSc1d1	běloruský
rodák	rodák	k1gMnSc1	rodák
Simon	Simon	k1gMnSc1	Simon
Kuznets	Kuznets	k1gInSc4	Kuznets
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
marxistickým	marxistický	k2eAgMnSc7d1	marxistický
teoretikem	teoretik	k1gMnSc7	teoretik
byl	být	k5eAaImAgMnS	být
Alexandr	Alexandr	k1gMnSc1	Alexandr
Bogdanov	Bogdanov	k1gInSc4	Bogdanov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
je	být	k5eAaImIp3nS	být
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
běloruským	běloruský	k2eAgMnSc7d1	běloruský
fotbalistou	fotbalista	k1gMnSc7	fotbalista
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
Alexandr	Alexandr	k1gMnSc1	Alexandr
Hleb	Hleb	k1gMnSc1	Hleb
<g/>
,	,	kIx,	,
z	z	k7c2	z
časů	čas	k1gInPc2	čas
SSSR	SSSR	kA	SSSR
pak	pak	k8xC	pak
Sergej	Sergej	k1gMnSc1	Sergej
Alejnikov	Alejnikov	k1gInSc1	Alejnikov
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
běloruský	běloruský	k2eAgMnSc1d1	běloruský
fotbalovým	fotbalový	k2eAgInSc7d1	fotbalový
klubem	klub	k1gInSc7	klub
je	být	k5eAaImIp3nS	být
Bate	Bate	k1gFnSc1	Bate
Borisov	Borisovo	k1gNnPc2	Borisovo
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
účastník	účastník	k1gMnSc1	účastník
skupinové	skupinový	k2eAgFnSc3d1	skupinová
části	část	k1gFnSc3	část
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
sportem	sport	k1gInSc7	sport
je	být	k5eAaImIp3nS	být
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
<g/>
,	,	kIx,	,
běloruská	běloruský	k2eAgFnSc1d1	Běloruská
reprezentace	reprezentace	k1gFnSc1	reprezentace
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
úspěchu	úspěch	k1gInSc2	úspěch
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
HK	HK	kA	HK
Dynamo	dynamo	k1gNnSc1	dynamo
Minsk	Minsk	k1gInSc1	Minsk
hraje	hrát	k5eAaImIp3nS	hrát
Kontinentální	kontinentální	k2eAgFnSc4d1	kontinentální
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
hostilo	hostit	k5eAaImAgNnS	hostit
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
šampionát	šampionát	k1gInSc1	šampionát
má	mít	k5eAaImIp3nS	mít
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
uspořádat	uspořádat	k5eAaPmF	uspořádat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Lotyšskem	Lotyšsko	k1gNnSc7	Lotyšsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2021	[number]	k4	2021
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc7d2	veliký
plánovanou	plánovaný	k2eAgFnSc7d1	plánovaná
akcí	akce	k1gFnSc7	akce
jsou	být	k5eAaImIp3nP	být
Evropské	evropský	k2eAgFnPc1d1	Evropská
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
mají	mít	k5eAaImIp3nP	mít
konat	konat	k5eAaImF	konat
v	v	k7c6	v
Minsku	Minsk	k1gInSc6	Minsk
<g/>
.	.	kIx.	.
</s>
<s>
Dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
vládce	vládce	k1gMnSc1	vládce
Alexander	Alexandra	k1gFnPc2	Alexandra
Lukašenko	Lukašenka	k1gFnSc5	Lukašenka
klade	klást	k5eAaImIp3nS	klást
na	na	k7c4	na
sport	sport	k1gInSc4	sport
velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
předsedá	předsedat	k5eAaImIp3nS	předsedat
běloruském	běloruský	k2eAgInSc6d1	běloruský
olympijskému	olympijský	k2eAgNnSc3d1	Olympijské
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
<g/>
Nejúspěšnějšími	úspěšný	k2eAgNnPc7d3	nejúspěšnější
individuálními	individuální	k2eAgMnPc7d1	individuální
běloruskými	běloruský	k2eAgMnPc7d1	běloruský
sportovci	sportovec	k1gMnPc7	sportovec
jsou	být	k5eAaImIp3nP	být
sportovní	sportovní	k2eAgMnPc1d1	sportovní
gymnasté	gymnast	k1gMnPc1	gymnast
Vitalij	Vitalij	k1gMnSc5	Vitalij
Ščerbo	Ščerba	k1gMnSc5	Ščerba
<g/>
,	,	kIx,	,
s	s	k7c7	s
rekordními	rekordní	k2eAgInPc7d1	rekordní
osmi	osm	k4xCc7	osm
tituly	titul	k1gInPc1	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
šesti	šest	k4xCc7	šest
zlatými	zlatá	k1gFnPc7	zlatá
z	z	k7c2	z
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
,	,	kIx,	,
a	a	k8xC	a
Olga	Olga	k1gFnSc1	Olga
Korbutová	Korbutový	k2eAgFnSc1d1	Korbutová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
čtyři	čtyři	k4xCgFnPc4	čtyři
zlaté	zlatý	k2eAgFnPc4d1	zlatá
olympijské	olympijský	k2eAgFnPc4d1	olympijská
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Běloruskou	běloruský	k2eAgFnSc4d1	Běloruská
gymnastickou	gymnastický	k2eAgFnSc4d1	gymnastická
školu	škola	k1gFnSc4	škola
úspěšně	úspěšně	k6eAd1	úspěšně
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
též	též	k9	též
Světlana	Světlana	k1gFnSc1	Světlana
Boginská	Boginský	k2eAgFnSc1d1	Boginský
<g/>
.	.	kIx.	.
</s>
<s>
Zápasník	zápasník	k1gMnSc1	zápasník
Alexandr	Alexandr	k1gMnSc1	Alexandr
Medveď	Medveď	k1gMnSc1	Medveď
získal	získat	k5eAaPmAgMnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdoucích	jdoucí	k2eAgFnPc6d1	jdoucí
olympiádách	olympiáda	k1gFnPc6	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
běloruským	běloruský	k2eAgMnSc7d1	běloruský
sportovcem	sportovec	k1gMnSc7	sportovec
současnosti	současnost	k1gFnSc2	současnost
je	být	k5eAaImIp3nS	být
biatlonistka	biatlonistka	k1gFnSc1	biatlonistka
Darja	Darja	k1gFnSc1	Darja
Domračevová	Domračevová	k1gFnSc1	Domračevová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
již	již	k6eAd1	již
čtyři	čtyři	k4xCgInPc4	čtyři
zlaté	zlatý	k1gInPc4	zlatý
na	na	k7c6	na
OH	OH	kA	OH
<g/>
.	.	kIx.	.
</s>
<s>
Sprinterka	sprinterka	k1gFnSc1	sprinterka
Julija	Julija	k1gFnSc1	Julija
Něstěrenková	Něstěrenkový	k2eAgFnSc1d1	Něstěrenkový
dokázala	dokázat	k5eAaPmAgFnS	dokázat
i	i	k9	i
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
dominance	dominance	k1gFnSc2	dominance
černošských	černošský	k2eAgFnPc2d1	černošská
běžkyň	běžkyně	k1gFnPc2	běžkyně
vybojovat	vybojovat	k5eAaPmF	vybojovat
zlato	zlato	k1gNnSc4	zlato
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Bělorusové	Bělorus	k1gMnPc1	Bělorus
začali	začít	k5eAaPmAgMnP	začít
silně	silně	k6eAd1	silně
prosazovat	prosazovat	k5eAaImF	prosazovat
v	v	k7c6	v
akrobatickém	akrobatický	k2eAgNnSc6d1	akrobatické
lyžování	lyžování	k1gNnSc6	lyžování
<g/>
,	,	kIx,	,
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
zlato	zlato	k1gNnSc4	zlato
získali	získat	k5eAaPmAgMnP	získat
již	již	k9	již
čtyři	čtyři	k4xCgMnPc1	čtyři
akrobatičtí	akrobatický	k2eAgMnPc1d1	akrobatický
lyžaři	lyžař	k1gMnPc1	lyžař
<g/>
:	:	kIx,	:
Alexej	Alexej	k1gMnSc1	Alexej
Grišin	Grišin	k2eAgMnSc1d1	Grišin
<g/>
,	,	kIx,	,
Anton	Anton	k1gMnSc1	Anton
Kušnir	Kušnir	k1gMnSc1	Kušnir
<g/>
,	,	kIx,	,
Alla	Alla	k1gFnSc1	Alla
Cuperová	Cuperový	k2eAgFnSc1d1	Cuperový
a	a	k8xC	a
Hanna	Hanen	k2eAgFnSc1d1	Hanna
Husková	Husková	k1gFnSc1	Husková
<g/>
.	.	kIx.	.
</s>
<s>
Samostatné	samostatný	k2eAgNnSc1d1	samostatné
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
získalo	získat	k5eAaPmAgNnS	získat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
již	již	k6eAd1	již
20	[number]	k4	20
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
má	mít	k5eAaImIp3nS	mít
výborný	výborný	k2eAgInSc4d1	výborný
zvuk	zvuk	k1gInSc4	zvuk
běloruská	běloruský	k2eAgFnSc1d1	Běloruská
tenisová	tenisový	k2eAgFnSc1d1	tenisová
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
jména	jméno	k1gNnPc1	jméno
jako	jako	k8xS	jako
Viktoria	Viktoria	k1gFnSc1	Viktoria
Azarenková	Azarenkový	k2eAgFnSc1d1	Azarenková
(	(	kIx(	(
<g/>
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
a	a	k8xC	a
vítězka	vítězka	k1gFnSc1	vítězka
Australian	Australiana	k1gFnPc2	Australiana
Open	Opena	k1gFnPc2	Opena
<g/>
)	)	kIx)	)
či	či	k8xC	či
Nataša	Nataša	k1gFnSc1	Nataša
Zverevová	Zverevová	k1gFnSc1	Zverevová
<g/>
.	.	kIx.	.
</s>
<s>
Věhlas	věhlas	k1gInSc1	věhlas
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
šachista	šachista	k1gMnSc1	šachista
Lev	Lev	k1gMnSc1	Lev
Polugajevskij	Polugajevskij	k1gMnSc1	Polugajevskij
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ŘEZNÍK	řezník	k1gMnSc1	řezník
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Běloruská	běloruský	k2eAgFnSc1d1	Běloruská
státnost	státnost	k1gFnSc1	státnost
–	–	k?	–
novum	novum	k1gNnSc1	novum
nebo	nebo	k8xC	nebo
tradice	tradice	k1gFnSc1	tradice
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
118	[number]	k4	118
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŘEZNÍK	řezník	k1gMnSc1	řezník
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
183	[number]	k4	183
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SAHANOVIČ	SAHANOVIČ	kA	SAHANOVIČ
<g/>
,	,	kIx,	,
Hienadź	Hienadź	k1gFnSc1	Hienadź
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
828	[number]	k4	828
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
AKUDOVIČ	AKUDOVIČ	kA	AKUDOVIČ
<g/>
,	,	kIx,	,
Valancin	Valancin	k2eAgInSc1d1	Valancin
<g/>
.	.	kIx.	.
</s>
<s>
Neznámé	známý	k2eNgNnSc1d1	neznámé
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Dokořán	dokořán	k6eAd1	dokořán
<g/>
,	,	kIx,	,
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
-	-	kIx~	-
společnost	společnost	k1gFnSc1	společnost
při	při	k7c6	při
ČT	ČT	kA	ČT
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
903510	[number]	k4	903510
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HLAVÁČEK	Hlaváček	k1gMnSc1	Hlaváček
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
-	-	kIx~	-
KOTAU	KOTAU	kA	KOTAU
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
mimo	mimo	k7c4	mimo
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Běloruští	běloruský	k2eAgMnPc1d1	běloruský
intelektuálové	intelektuál	k1gMnPc1	intelektuál
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7308-687-9	[number]	k4	978-80-7308-687-9
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
Lukašenko	Lukašenka	k1gFnSc5	Lukašenka
</s>
</p>
<p>
<s>
Běloruské	běloruský	k2eAgNnSc1d1	běloruské
referendum	referendum	k1gNnSc1	referendum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
Bělověžský	Bělověžský	k2eAgInSc1d1	Bělověžský
prales	prales	k1gInSc1	prales
</s>
</p>
<p>
<s>
Černobyl	Černobyl	k1gInSc1	Černobyl
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
bělorusky	bělorusky	k6eAd1	bělorusky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
prezidenta	prezident	k1gMnSc2	prezident
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
bělorusky	bělorusky	k6eAd1	bělorusky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Rusko-běloruského	ruskoěloruský	k2eAgInSc2d1	rusko-běloruský
svazu	svaz	k1gInSc2	svaz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Svobodné	svobodný	k2eAgNnSc1d1	svobodné
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
–	–	k?	–
stránky	stránka	k1gFnSc2	stránka
prodemokratického	prodemokratický	k2eAgNnSc2d1	prodemokratické
občanského	občanský	k2eAgNnSc2d1	občanské
sdružení	sdružení	k1gNnSc2	sdružení
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Občanské	občanský	k2eAgNnSc4d1	občanské
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
(	(	kIx(	(
<g/>
stránky	stránka	k1gFnPc4	stránka
prodemokraticky	prodemokraticky	k6eAd1	prodemokraticky
zaměřené	zaměřený	k2eAgFnPc4d1	zaměřená
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
vše	všechen	k3xTgNnSc1	všechen
o	o	k7c6	o
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
cestopisů	cestopis	k1gInPc2	cestopis
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc1	Internet
Archive	archiv	k1gInSc5	archiv
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Aktuální	aktuální	k2eAgNnSc1d1	aktuální
počasí	počasí	k1gNnSc1	počasí
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
</s>
</p>
<p>
<s>
Belarus	Belarus	k1gInSc1	Belarus
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Belarus	Belarus	k1gInSc1	Belarus
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Belarus	Belarus	k1gInSc1	Belarus
Country	country	k2eAgInSc1d1	country
Report	report	k1gInSc1	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Belarus	Belarus	k1gInSc1	Belarus
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2010-08-27	[number]	k4	2010-08-27
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Belarus	Belarus	k1gInSc1	Belarus
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Minsku	Minsk	k1gInSc6	Minsk
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-04-15	[number]	k4	2011-04-15
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ADAMOVICH	ADAMOVICH	kA	ADAMOVICH
<g/>
,	,	kIx,	,
Anthony	Anthona	k1gFnSc2	Anthona
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Belarus	Belarus	k1gInSc1	Belarus
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
