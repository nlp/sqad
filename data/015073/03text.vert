<s>
Trikolóra	trikolóra	k1gFnSc1
hnutí	hnutí	k1gNnSc2
občanů	občan	k1gMnPc2
</s>
<s>
Trikolóra	trikolóra	k1gFnSc1
hnutí	hnutí	k1gNnSc2
občanů	občan	k1gMnPc2
Zkratka	zkratka	k1gFnSc1
</s>
<s>
Trikolóra	trikolóra	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pověřena	pověřit	k5eAaPmNgFnS
vedením	vedení	k1gNnSc7
</s>
<s>
Zuzana	Zuzana	k1gFnSc1
Majerová	Majerová	k1gFnSc1
Zahradníková	Zahradníková	k1gFnSc1
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
místopředsedkyně	místopředsedkyně	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Tiskový	tiskový	k2eAgMnSc1d1
mluvčí	mluvčí	k1gMnSc1
</s>
<s>
Ivana	Ivana	k1gFnSc1
Kerlesová	Kerlesový	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgNnSc4d2
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Freyova	Freyov	k1gInSc2
82	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
190	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Rozkol	rozkol	k1gInSc1
od	od	k7c2
</s>
<s>
ODS	ODS	kA
Ideologie	ideologie	k1gFnSc1
</s>
<s>
národní	národní	k2eAgInSc1d1
konzervatismus	konzervatismus	k1gInSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
sociální	sociální	k2eAgInSc1d1
konzervatismusekonomický	konzervatismusekonomický	k2eAgInSc1d1
nacionalismuspravicový	nacionalismuspravicový	k2eAgInSc1d1
populismusklausismus	populismusklausismus	k1gInSc1
Politická	politický	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
</s>
<s>
pravice	pravice	k1gFnSc1
Slogan	slogan	k1gInSc1
</s>
<s>
Vraťme	vrátit	k5eAaPmRp1nP
lidem	lid	k1gInSc7
jejich	jejich	k3xOp3gFnSc4
zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barvy	barva	k1gFnSc2
</s>
<s>
bílá	bílý	k2eAgFnSc1d1
červená	červený	k2eAgFnSc1d1
modrá	modrý	k2eAgFnSc1d1
IČO	IČO	kA
</s>
<s>
08288909	#num#	k4
(	(	kIx(
<g/>
PSH	PSH	kA
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
https://www.volimtrikoloru.cz/	https://www.volimtrikoloru.cz/	k?
Zisk	zisk	k1gInSc1
mandátů	mandát	k1gInPc2
ve	v	k7c6
volbách	volba	k1gFnPc6
Sněmovna	sněmovna	k1gFnSc1
<g/>
2017	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
200	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
p.	p.	k?
1	#num#	k4
<g/>
]	]	kIx)
Zastupitelstva	zastupitelstvo	k1gNnPc1
obcí	obec	k1gFnPc2
<g/>
2018	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
62300	#num#	k4
</s>
<s>
Symbolika	symbolika	k1gFnSc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
Členové	člen	k1gMnPc1
Trikolóry	trikolóra	k1gFnSc2
občas	občas	k6eAd1
používají	používat	k5eAaImIp3nP
(	(	kIx(
<g/>
zejm.	zejm.	k?
při	při	k7c6
fotografování	fotografování	k1gNnSc6
<g/>
)	)	kIx)
tříprsté	tříprstý	k2eAgNnSc4d1
gesto	gesto	k1gNnSc4
odkazující	odkazující	k2eAgNnSc4d1
na	na	k7c4
tři	tři	k4xCgFnPc4
barvy	barva	k1gFnPc4
trikolóry	trikolóra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Trikolóra	trikolóra	k1gFnSc1
hnutí	hnutí	k1gNnSc2
občanů	občan	k1gMnPc2
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
Trikolóra	trikolóra	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
české	český	k2eAgNnSc1d1
politické	politický	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
založené	založený	k2eAgNnSc1d1
v	v	k7c6
červnu	červen	k1gInSc6
2019	#num#	k4
Václavem	Václav	k1gMnSc7
Klausem	Klaus	k1gMnSc7
mladším	mladý	k2eAgMnSc7d2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
má	mít	k5eAaImIp3nS
některé	některý	k3yIgInPc1
charakteristické	charakteristický	k2eAgInPc1d1
prvky	prvek	k1gInPc1
pravice	pravice	k1gFnSc2
a	a	k8xC
profiluje	profilovat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
společensky	společensky	k6eAd1
konzervativní	konzervativní	k2eAgFnSc1d1
a	a	k8xC
nacionálně	nacionálně	k6eAd1
ekonomický	ekonomický	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
subjekt	subjekt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
se	se	k3xPyFc4
od	od	k7c2
ledna	leden	k1gInSc2
2021	#num#	k4
nebrání	bránit	k5eNaImIp3nS
referendu	referendum	k1gNnSc3
o	o	k7c6
členství	členství	k1gNnSc6
ČR	ČR	kA
v	v	k7c6
EU	EU	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hnutí	hnutí	k1gNnSc1
se	se	k3xPyFc4
staví	stavit	k5eAaBmIp3nS,k5eAaImIp3nS,k5eAaPmIp3nS
negativně	negativně	k6eAd1
k	k	k7c3
rozšiřování	rozšiřování	k1gNnSc3
práv	právo	k1gNnPc2
LGBT	LGBT	kA
(	(	kIx(
<g/>
uzákonění	uzákonění	k1gNnSc1
stejnopohlavního	stejnopohlavní	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
v	v	k7c6
ČR	ČR	kA
<g/>
,	,	kIx,
úřední	úřední	k2eAgFnSc3d1
změně	změna	k1gFnSc3
pohlaví	pohlaví	k1gNnSc2
pro	pro	k7c4
trans	trans	k1gInSc4
osoby	osoba	k1gFnSc2
bez	bez	k7c2
předchozího	předchozí	k2eAgNnSc2d1
podstoupení	podstoupení	k1gNnSc2
chirurgického	chirurgický	k2eAgInSc2d1
zákroku	zákrok	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
změnu	změna	k1gFnSc4
podmínkou	podmínka	k1gFnSc7
dle	dle	k7c2
současné	současný	k2eAgFnSc2d1
české	český	k2eAgFnSc2d1
právní	právní	k2eAgFnSc2d1
úpravy	úprava	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
staví	stavit	k5eAaBmIp3nS,k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
kriticky	kriticky	k6eAd1
k	k	k7c3
otázce	otázka	k1gFnSc3
lidského	lidský	k2eAgInSc2d1
vlivu	vliv	k1gInSc2
na	na	k7c4
změny	změna	k1gFnPc4
klimatu	klima	k1gNnSc2
či	či	k8xC
konkrétním	konkrétní	k2eAgNnSc7d1
uplatňovaným	uplatňovaný	k2eAgNnSc7d1
řešením	řešení	k1gNnSc7
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
také	také	k9
vůči	vůči	k7c3
multikulturalismu	multikulturalismus	k1gInSc3
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporuje	podporovat	k5eAaImIp3nS
zachování	zachování	k1gNnSc4
tzv.	tzv.	kA
tradičních	tradiční	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
,	,	kIx,
vlastenectví	vlastenectví	k1gNnSc4
<g/>
,	,	kIx,
volný	volný	k2eAgInSc4d1
trh	trh	k1gInSc4
<g/>
,	,	kIx,
stát	stát	k5eAaPmF,k5eAaImF
s	s	k7c7
minimálními	minimální	k2eAgFnPc7d1
kompetencemi	kompetence	k1gFnPc7
<g/>
,	,	kIx,
redukci	redukce	k1gFnSc4
sociálního	sociální	k2eAgInSc2d1
státu	stát	k1gInSc2
(	(	kIx(
<g/>
zrušení	zrušení	k1gNnSc4
minimální	minimální	k2eAgFnSc2d1
mzdy	mzda	k1gFnSc2
<g/>
)	)	kIx)
či	či	k8xC
zrušení	zrušení	k1gNnSc1
dotací	dotace	k1gFnPc2
pro	pro	k7c4
neziskové	ziskový	k2eNgFnPc4d1
organizace	organizace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
osobních	osobní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
ukončí	ukončit	k5eAaPmIp3nP
aktivity	aktivita	k1gFnPc4
v	v	k7c6
politice	politika	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
vzdal	vzdát	k5eAaPmAgMnS
kandidatury	kandidatura	k1gFnSc2
v	v	k7c6
říjnových	říjnový	k2eAgFnPc6d1
sněmovních	sněmovní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2021	#num#	k4
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
na	na	k7c6
mimořádném	mimořádný	k2eAgNnSc6d1
předsednictvu	předsednictvo	k1gNnSc6
rezignoval	rezignovat	k5eAaBmAgInS
na	na	k7c4
funkci	funkce	k1gFnSc4
předsedy	předseda	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
voleb	volba	k1gFnPc2
byla	být	k5eAaImAgFnS
vedením	vedení	k1gNnSc7
hnutí	hnutí	k1gNnSc2
pověřena	pověřen	k2eAgFnSc1d1
první	první	k4xOgFnSc1
místopředsedkyně	místopředsedkyně	k1gFnSc1
Zuzana	Zuzana	k1gFnSc1
Majerová	Majerová	k1gFnSc1
Zahradníková	Zahradníková	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
Vznik	vznik	k1gInSc1
hnutí	hnutí	k1gNnSc3
ohlásil	ohlásit	k5eAaPmAgInS
bývalý	bývalý	k2eAgMnSc1d1
poslanec	poslanec	k1gMnSc1
Občanské	občanský	k2eAgFnSc2d1
demokratické	demokratický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
ODS	ODS	kA
<g/>
)	)	kIx)
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
a	a	k8xC
předcházel	předcházet	k5eAaImAgInS
mu	on	k3xPp3gMnSc3
incident	incident	k1gInSc4
na	na	k7c6
půdě	půda	k1gFnSc6
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
při	při	k7c6
projednávání	projednávání	k1gNnSc6
zákona	zákon	k1gInSc2
o	o	k7c6
GDPR	GDPR	kA
přirovnal	přirovnat	k5eAaPmAgMnS
přejímání	přejímání	k1gNnSc4
a	a	k8xC
automatickou	automatický	k2eAgFnSc4d1
implementaci	implementace	k1gFnSc4
nařízení	nařízení	k1gNnSc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
Poslaneckou	poslanecký	k2eAgFnSc7d1
sněmovnou	sněmovna	k1gFnSc7
ČR	ČR	kA
k	k	k7c3
rozhodování	rozhodování	k1gNnSc3
židovských	židovský	k2eAgInPc2d1
výborů	výbor	k1gInPc2
o	o	k7c6
složení	složení	k1gNnSc6
transportů	transport	k1gInPc2
do	do	k7c2
nacistických	nacistický	k2eAgInPc2d1
koncentračních	koncentrační	k2eAgInPc2d1
táborů	tábor	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
poslanci	poslanec	k1gMnPc1
rozhodují	rozhodovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nařízení	nařízení	k1gNnSc1
ČR	ČR	kA
přijme	přijmout	k5eAaPmIp3nS
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
v	v	k7c6
jaké	jaký	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
formě	forma	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
„	„	k?
<g/>
kdybychom	kdyby	kYmCp1nP
hlasovali	hlasovat	k5eAaImAgMnP
nezávisle	závisle	k6eNd1
<g/>
,	,	kIx,
žádné	žádný	k3yNgNnSc4
GDPR	GDPR	kA
by	by	kYmCp3nS
nebylo	být	k5eNaImAgNnS
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
tomu	ten	k3xDgNnSc3
dodal	dodat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
57	#num#	k4
%	%	kIx~
případů	případ	k1gInPc2
jsou	být	k5eAaImIp3nP
zákony	zákon	k1gInPc1
přijímané	přijímaný	k2eAgInPc1d1
Poslaneckou	poslanecký	k2eAgFnSc7d1
sněmovnou	sněmovna	k1gFnSc7
pouze	pouze	k6eAd1
implementací	implementace	k1gFnSc7
nařízení	nařízení	k1gNnSc2
EU	EU	kA
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
poslanci	poslanec	k1gMnPc1
<g />
.	.	kIx.
</s>
<s hack="1">
bezmyšlenkovitě	bezmyšlenkovitě	k6eAd1
přijímají	přijímat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
použité	použitý	k2eAgNnSc4d1
přirovnání	přirovnání	k1gNnSc4
se	se	k3xPyFc4
před	před	k7c7
poslanci	poslanec	k1gMnPc7
omluvil	omluvit	k5eAaPmAgMnS
Petr	Petr	k1gMnSc1
Fiala	Fiala	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
ODS	ODS	kA
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
poslanecký	poslanecký	k2eAgInSc1d1
klub	klub	k1gInSc1
se	se	k3xPyFc4
následně	následně	k6eAd1
začal	začít	k5eAaPmAgInS
výrokem	výrok	k1gInSc7
zabývat	zabývat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Petr	Petr	k1gMnSc1
Fiala	Fiala	k1gMnSc1
Václava	Václav	k1gMnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Klause	Klaus	k1gMnSc4
mladšího	mladý	k2eAgMnSc2d2
ještě	ještě	k9
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
vyzval	vyzvat	k5eAaPmAgMnS
k	k	k7c3
odchodu	odchod	k1gInSc3
z	z	k7c2
poslaneckého	poslanecký	k2eAgInSc2d1
klubu	klub	k1gInSc2
ODS	ODS	kA
a	a	k8xC
označil	označit	k5eAaPmAgInS
jeho	jeho	k3xOp3gNnSc4
jednání	jednání	k1gNnSc4
za	za	k7c4
poškozování	poškozování	k1gNnSc4
ODS	ODS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
byl	být	k5eAaImAgMnS
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
vyloučen	vyloučen	k2eAgMnSc1d1
jak	jak	k8xS,k8xC
z	z	k7c2
ODS	ODS	kA
<g/>
,	,	kIx,
tak	tak	k9
z	z	k7c2
jejího	její	k3xOp3gInSc2
poslaneckého	poslanecký	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Dvaadvacetičlenná	dvaadvacetičlenný	k2eAgFnSc1d1
výkonná	výkonný	k2eAgFnSc1d1
rada	rada	k1gFnSc1
ho	on	k3xPp3gInSc4
vyloučila	vyloučit	k5eAaPmAgFnS
dvaceti	dvacet	k4xCc6
hlasy	hlas	k1gInPc1
přítomných	přítomný	k2eAgMnPc2d1
členů	člen	k1gMnPc2
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člen	člen	k1gMnSc1
výkonné	výkonný	k2eAgFnSc2d1
rady	rada	k1gFnSc2
ODS	ODS	kA
Jan	Jan	k1gMnSc1
Skopeček	skopeček	k1gMnSc1
hlasování	hlasování	k1gNnSc4
o	o	k7c4
vyloučení	vyloučení	k1gNnSc4
nebyl	být	k5eNaImAgMnS
přítomen	přítomen	k2eAgMnSc1d1
<g/>
,	,	kIx,
vyjádřil	vyjádřit	k5eAaPmAgMnS
se	se	k3xPyFc4
ale	ale	k9
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
hlasoval	hlasovat	k5eAaImAgMnS
proti	proti	k7c3
vyloučení	vyloučení	k1gNnSc3
Václava	Václav	k1gMnSc2
Klause	Klaus	k1gMnSc2
mladšího	mladý	k2eAgMnSc2d2
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
jeho	jeho	k3xOp3gNnSc3
vyloučení	vyloučení	k1gNnSc3
se	se	k3xPyFc4
vyjádřila	vyjádřit	k5eAaPmAgFnS
i	i	k9
tehdejší	tehdejší	k2eAgFnSc1d1
poslankyně	poslankyně	k1gFnSc1
ODS	ODS	kA
Zuzana	Zuzana	k1gFnSc1
Majerová	Majerová	k1gFnSc1
Zahradníková	Zahradníková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
vyloučení	vyloučení	k1gNnSc2
bylo	být	k5eAaImAgNnS
dle	dle	k7c2
vyjádření	vyjádření	k1gNnSc2
výkonné	výkonný	k2eAgFnSc2d1
rady	rada	k1gFnSc2
ODS	ODS	kA
dlouhodobé	dlouhodobý	k2eAgNnSc1d1
nerespektování	nerespektování	k1gNnSc1
hodnot	hodnota	k1gFnPc2
<g/>
,	,	kIx,
principů	princip	k1gInPc2
a	a	k8xC
programu	program	k1gInSc2
ODS	ODS	kA
<g/>
,	,	kIx,
veřejné	veřejný	k2eAgInPc4d1
útoky	útok	k1gInPc4
na	na	k7c4
poslance	poslanec	k1gMnPc4
ODS	ODS	kA
a	a	k8xC
další	další	k2eAgMnPc4d1
kolegy	kolega	k1gMnPc4
ze	z	k7c2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
protikandidáta	protikandidát	k1gMnSc4
ODS	ODS	kA
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
v	v	k7c6
senátních	senátní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
(	(	kIx(
<g/>
v	v	k7c6
senátním	senátní	k2eAgInSc6d1
obvodě	obvod	k1gInSc6
č.	č.	k?
26	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
podpořil	podpořit	k5eAaPmAgMnS
Ladislava	Ladislav	k1gMnSc4
Jakla	Jakl	k1gMnSc4
za	za	k7c2
SPD	SPD	kA
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
kontroverzní	kontroverzní	k2eAgInPc1d1
výroky	výrok	k1gInPc1
<g/>
,	,	kIx,
veřejné	veřejný	k2eAgInPc1d1
podporování	podporování	k1gNnSc4
czexitu	czexit	k1gInSc2
<g/>
,	,	kIx,
zpochybňování	zpochybňování	k1gNnSc4
účasti	účast	k1gFnSc2
českých	český	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
v	v	k7c6
zahraničních	zahraniční	k2eAgFnPc6d1
misích	mise	k1gFnPc6
(	(	kIx(
<g/>
především	především	k9
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
účast	účast	k1gFnSc1
na	na	k7c6
akcích	akce	k1gFnPc6
pořádaných	pořádaný	k2eAgFnPc2d1
problematickými	problematický	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
a	a	k8xC
deklarování	deklarování	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
nebude	být	k5eNaImBp3nS
volit	volit	k5eAaImF
ODS	ODS	kA
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Fiala	Fiala	k1gMnSc1
k	k	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
výkonné	výkonný	k2eAgFnSc2d1
rady	rada	k1gFnSc2
dodal	dodat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
byl	být	k5eAaImAgMnS
vyloučen	vyloučen	k2eAgMnSc1d1
za	za	k7c4
dlouhodobé	dlouhodobý	k2eAgNnSc4d1
poškozování	poškozování	k1gNnSc4
jména	jméno	k1gNnSc2
ODS	ODS	kA
navzdory	navzdory	k7c3
několika	několik	k4yIc3
varováním	varování	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
zmínku	zmínka	k1gFnSc4
stojí	stát	k5eAaImIp3nS
také	také	k9
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
plánoval	plánovat	k5eAaImAgMnS
kandidovat	kandidovat	k5eAaImF
do	do	k7c2
EP	EP	kA
spolu	spolu	k6eAd1
se	s	k7c7
Svobodnými	svobodný	k2eAgMnPc7d1
a	a	k8xC
Realisty	realista	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Představení	představení	k1gNnSc1
nového	nový	k2eAgInSc2d1
subjektu	subjekt	k1gInSc2
bylo	být	k5eAaImAgNnS
zprvu	zprvu	k6eAd1
ohlášeno	ohlásit	k5eAaPmNgNnS,k5eAaImNgNnS
na	na	k7c4
druhou	druhý	k4xOgFnSc4
polovinu	polovina	k1gFnSc4
května	květen	k1gInSc2
2019	#num#	k4
po	po	k7c6
evropských	evropský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Následně	následně	k6eAd1
bylo	být	k5eAaImAgNnS
ohlášeno	ohlásit	k5eAaPmNgNnS,k5eAaImNgNnS
na	na	k7c4
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2019	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
v	v	k7c6
kongresovém	kongresový	k2eAgNnSc6d1
centru	centrum	k1gNnSc6
Erbia	erbium	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akci	akce	k1gFnSc6
moderoval	moderovat	k5eAaBmAgMnS
herec	herec	k1gMnSc1
Marek	Marek	k1gMnSc1
Vašut	Vašut	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
uvedl	uvést	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Přišel	přijít	k5eAaPmAgMnS
čas	čas	k1gInSc4
vrátit	vrátit	k5eAaPmF
tuto	tento	k3xDgFnSc4
zemi	zem	k1gFnSc4
lidem	člověk	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
pracují	pracovat	k5eAaImIp3nP
<g/>
,	,	kIx,
platí	platit	k5eAaImIp3nP
daně	daň	k1gFnPc1
<g/>
,	,	kIx,
vychovávají	vychovávat	k5eAaImIp3nP
děti	dítě	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přišel	přijít	k5eAaPmAgMnS
čas	čas	k1gInSc4
vzít	vzít	k5eAaPmF
si	se	k3xPyFc3
naši	náš	k3xOp1gFnSc4
zemi	zem	k1gFnSc4
zpátky	zpátky	k6eAd1
<g/>
.	.	kIx.
<g/>
“	“	k?
Za	za	k7c4
prioritu	priorita	k1gFnSc4
označil	označit	k5eAaPmAgMnS
sněmovní	sněmovní	k2eAgFnPc4d1
volby	volba	k1gFnPc4
v	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
novinářů	novinář	k1gMnPc2
Jaroslava	Jaroslava	k1gFnSc1
Kmenty	Kment	k1gMnPc4
a	a	k8xC
Jana	Jan	k1gMnSc4
Novotného	Novotný	k1gMnSc4
si	se	k3xPyFc3
hnutí	hnutí	k1gNnSc1
určilo	určit	k5eAaPmAgNnS
za	za	k7c4
své	svůj	k3xOyFgNnSc4
sídlo	sídlo	k1gNnSc4
adresu	adresa	k1gFnSc4
Freyova	Freyov	k1gInSc2
82	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
,	,	kIx,
podle	podle	k7c2
katastru	katastr	k1gInSc2
nemovitostí	nemovitost	k1gFnPc2
patřící	patřící	k2eAgFnSc3d1
firmě	firma	k1gFnSc3
Yugra	Yugr	k1gMnSc2
Alliance	Allianec	k1gMnSc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gMnSc7
vlastníkem	vlastník	k1gMnSc7
a	a	k8xC
jednatelem	jednatel	k1gMnSc7
byl	být	k5eAaImAgMnS
moskevský	moskevský	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
Volodya	Volodya	k1gMnSc1
Elizbaryan	Elizbaryan	k1gMnSc1
<g/>
,	,	kIx,
provozující	provozující	k2eAgFnSc1d1
v	v	k7c6
Karlíně	Karlín	k1gInSc6
obchod	obchod	k1gInSc1
s	s	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
luxusním	luxusní	k2eAgInSc7d1
italským	italský	k2eAgInSc7d1
nábytkem	nábytek	k1gInSc7
Salon	salon	k1gInSc1
Cardinal	Cardinal	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
vysvětlil	vysvětlit	k5eAaPmAgMnS
<g/>
,	,	kIx,
proč	proč	k6eAd1
si	se	k3xPyFc3
vybral	vybrat	k5eAaPmAgMnS
pro	pro	k7c4
své	svůj	k3xOyFgNnSc4
sídlo	sídlo	k1gNnSc4
právě	právě	k9
tuto	tento	k3xDgFnSc4
budovu	budova	k1gFnSc4
<g/>
,	,	kIx,
takto	takto	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
levné	levný	k2eAgNnSc1d1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
na	na	k7c6
metru	metro	k1gNnSc6
a	a	k8xC
blízko	blízko	k7c2
mého	můj	k3xOp1gNnSc2
bydliště	bydliště	k1gNnSc2
<g/>
.	.	kIx.
<g/>
”	”	k?
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ke	k	k7c3
dni	den	k1gInSc3
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2019	#num#	k4
bylo	být	k5eAaImAgNnS
hnutí	hnutí	k1gNnSc1
zaregistrováno	zaregistrovat	k5eAaPmNgNnS
Ministerstvem	ministerstvo	k1gNnSc7
vnitra	vnitro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Model	model	k1gInSc1
Centra	centrum	k1gNnSc2
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
veřejného	veřejný	k2eAgNnSc2d1
mínění	mínění	k1gNnSc2
z	z	k7c2
konce	konec	k1gInSc2
června	červen	k1gInSc2
2019	#num#	k4
mu	on	k3xPp3gMnSc3
pro	pro	k7c4
hypotetické	hypotetický	k2eAgFnPc4d1
volby	volba	k1gFnPc4
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
přisuzoval	přisuzovat	k5eAaImAgMnS
volební	volební	k2eAgFnPc4d1
preference	preference	k1gFnPc4
kolem	kolem	k7c2
1	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Společnost	společnost	k1gFnSc1
Kantar	Kantar	k1gMnSc1
mu	on	k3xPp3gMnSc3
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
volebním	volební	k2eAgInSc6d1
modelu	model	k1gInSc6
z	z	k7c2
konce	konec	k1gInSc2
června	červen	k1gInSc2
2019	#num#	k4
přisuzovala	přisuzovat	k5eAaImAgFnS
3	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
volebním	volební	k2eAgInSc6d1
modelu	model	k1gInSc6
společnosti	společnost	k1gFnSc2
Kantar	Kantara	k1gFnPc2
z	z	k7c2
listopadu	listopad	k1gInSc2
2019	#num#	k4
by	by	kYmCp3nP
hnutí	hnutí	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
6,5	6,5	k4
%	%	kIx~
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
podle	podle	k7c2
průzkumu	průzkum	k1gInSc2
z	z	k7c2
dubna	duben	k1gInSc2
2020	#num#	k4
by	by	kYmCp3nS
obdrželo	obdržet	k5eAaPmAgNnS
2,5	2,5	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předsednictvo	předsednictvo	k1gNnSc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
Na	na	k7c6
ustavujícím	ustavující	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2019	#num#	k4
v	v	k7c6
Brně	Brno	k1gNnSc6
bylo	být	k5eAaImAgNnS
zvoleno	zvolit	k5eAaPmNgNnS
předsednictvo	předsednictvo	k1gNnSc1
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
úřad	úřad	k1gInSc1
předsedy	předseda	k1gMnSc2
hnutí	hnutí	k1gNnSc2
neobsazen	obsadit	k5eNaPmNgInS
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
rezignoval	rezignovat	k5eAaBmAgMnS
na	na	k7c6
funkci	funkce	k1gFnSc6
předsedy	předseda	k1gMnSc2
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
</s>
<s>
Zuzana	Zuzana	k1gFnSc1
Majerová	Majerová	k1gFnSc1
Zahradníková	Zahradníková	k1gFnSc1
–	–	k?
úřadující	úřadující	k2eAgFnSc1d1
předsedkyně	předsedkyně	k1gFnSc1
<g/>
,	,	kIx,
zvolená	zvolený	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
místopředsedkyně	místopředsedkyně	k1gFnSc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
Jan	Jan	k1gMnSc1
Tesař	Tesař	k1gMnSc1
–	–	k?
místopředseda	místopředseda	k1gMnSc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
Dalibor	Dalibor	k1gMnSc1
Uhlíř	Uhlíř	k1gMnSc1
–	–	k?
místopředseda	místopředseda	k1gMnSc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
Členstvo	členstvo	k1gNnSc1
a	a	k8xC
podpora	podpora	k1gFnSc1
</s>
<s>
Představení	představení	k1gNnSc1
úplné	úplný	k2eAgFnSc2d1
sestavy	sestava	k1gFnSc2
hnutí	hnutí	k1gNnSc1
bylo	být	k5eAaImAgNnS
ohlášeno	ohlášet	k5eAaImNgNnS,k5eAaPmNgNnS
na	na	k7c4
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předem	předem	k6eAd1
média	médium	k1gNnSc2
počítala	počítat	k5eAaImAgFnS
s	s	k7c7
určitostí	určitost	k1gFnSc7
pouze	pouze	k6eAd1
se	s	k7c7
zakladatelem	zakladatel	k1gMnSc7
Václavem	Václav	k1gMnSc7
Klausem	Klaus	k1gMnSc7
mladším	mladý	k2eAgMnSc7d2
a	a	k8xC
bývalou	bývalý	k2eAgFnSc7d1
poslankyní	poslankyně	k1gFnSc7
ODS	ODS	kA
Zuzanou	Zuzana	k1gFnSc7
Majerovou	Majerová	k1gFnSc7
Zahradníkovou	Zahradníková	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
médií	médium	k1gNnPc2
nicméně	nicméně	k8xC
unikly	uniknout	k5eAaPmAgFnP
zprávy	zpráva	k1gFnPc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
na	na	k7c6
chodu	chod	k1gInSc6
hnutí	hnutí	k1gNnSc2
mohli	moct	k5eAaImAgMnP
podílet	podílet	k5eAaImF
například	například	k6eAd1
kardiochirurg	kardiochirurg	k1gMnSc1
Jan	Jan	k1gMnSc1
Pirk	Pirk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
předsedou	předseda	k1gMnSc7
volebního	volební	k2eAgInSc2d1
výboru	výbor	k1gInSc2
prezidentského	prezidentský	k2eAgMnSc4d1
kandidáta	kandidát	k1gMnSc4
Jana	Jan	k1gMnSc2
Fischera	Fischer	k1gMnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
bývalý	bývalý	k2eAgMnSc1d1
poslanec	poslanec	k1gMnSc1
ODS	ODS	kA
a	a	k8xC
poradce	poradce	k1gMnSc2
prezidenta	prezident	k1gMnSc2
Václava	Václav	k1gMnSc2
Klause	Klaus	k1gMnSc2
Boris	Boris	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
<g/>
,	,	kIx,
ekonomická	ekonomický	k2eAgFnSc1d1
publicistka	publicistka	k1gFnSc1
Markéta	Markéta	k1gFnSc1
Šichtařová	Šichtařová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
v	v	k7c6
letech	léto	k1gNnPc6
2006	#num#	k4
a	a	k8xC
2007	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
týmu	tým	k1gInSc6
ekonomických	ekonomický	k2eAgMnPc2d1
poradců	poradce	k1gMnPc2
ministra	ministr	k1gMnSc2
financí	finance	k1gFnPc2
za	za	k7c2
ODS	ODS	kA
Vlastimila	Vlastimil	k1gMnSc4
Tlustého	tlusté	k1gNnSc2
nebo	nebo	k8xC
ředitel	ředitel	k1gMnSc1
firmy	firma	k1gFnSc2
Madeta	Madeta	k1gFnSc1
Jan	Jan	k1gMnSc1
Teplý	Teplý	k1gMnSc1
starší	starší	k1gMnSc1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Média	médium	k1gNnPc1
také	také	k9
očekávala	očekávat	k5eAaImAgNnP
pravděpodobné	pravděpodobný	k2eAgFnPc1d1
zapojení	zapojení	k1gNnSc4
bývalého	bývalý	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
a	a	k8xC
zakladatelova	zakladatelův	k2eAgMnSc2d1
otce	otec	k1gMnSc2
Václava	Václav	k1gMnSc2
Klause	Klaus	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
by	by	kYmCp3nS
podle	podle	k7c2
spekulací	spekulace	k1gFnPc2
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
stranickým	stranický	k2eAgMnSc7d1
expertem	expert	k1gMnSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Klaus	Klaus	k1gMnSc1
údajně	údajně	k6eAd1
oslovil	oslovit	k5eAaPmAgMnS
také	také	k9
spisovatele	spisovatel	k1gMnSc4
Vlastimila	Vlastimil	k1gMnSc4
Vondrušku	Vondruška	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
však	však	k9
účast	účast	k1gFnSc4
odmítl	odmítnout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
oficiálním	oficiální	k2eAgNnSc6d1
představení	představení	k1gNnSc6
Trikolóry	trikolóra	k1gFnSc2
přečetl	přečíst	k5eAaPmAgMnS
herec	herec	k1gMnSc1
Marek	Marek	k1gMnSc1
Vašut	Vašut	k1gMnSc1
přátelské	přátelský	k2eAgFnSc2d1
zdravice	zdravice	k1gFnSc2
dvou	dva	k4xCgInPc2
členů	člen	k1gInPc2
senátního	senátní	k2eAgInSc2d1
klubu	klub	k1gInSc2
ODS	ODS	kA
–	–	k?
Iva	Ivo	k1gMnSc2
Valenty	Valenta	k1gMnSc2
a	a	k8xC
Jaroslava	Jaroslav	k1gMnSc2
Zemana	Zeman	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Podporu	podpora	k1gFnSc4
vyjádřil	vyjádřit	k5eAaPmAgMnS
také	také	k9
bývalý	bývalý	k2eAgMnSc1d1
europoslanec	europoslanec	k1gMnSc1
za	za	k7c4
ODS	ODS	kA
Jaroslav	Jaroslav	k1gMnSc1
Zvěřina	Zvěřina	k1gMnSc1
a	a	k8xC
akce	akce	k1gFnSc1
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
i	i	k9
poslanec	poslanec	k1gMnSc1
vládního	vládní	k2eAgInSc2d1
ANO	ano	k9
Přemysl	Přemysl	k1gMnSc1
Mališ	Mališ	k1gMnSc1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Žádný	žádný	k1gMnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
však	však	k9
svůj	svůj	k3xOyFgInSc4
vstup	vstup	k1gInSc4
do	do	k7c2
Trikolóry	trikolóra	k1gFnSc2
neavizoval	avizovat	k5eNaBmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Podobně	podobně	k6eAd1
se	se	k3xPyFc4
k	k	k7c3
hnutí	hnutí	k1gNnSc3
stavěl	stavět	k5eAaImAgMnS
Václav	Václav	k1gMnSc1
Krása	krása	k1gFnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
Národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
osob	osoba	k1gFnPc2
se	s	k7c7
zdravotním	zdravotní	k2eAgNnSc7d1
postižením	postižení	k1gNnSc7
<g/>
,	,	kIx,
dřívější	dřívější	k2eAgMnSc1d1
poslanec	poslanec	k1gMnSc1
za	za	k7c4
ODS	ODS	kA
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
Unii	unie	k1gFnSc4
svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
neúspěšný	úspěšný	k2eNgMnSc1d1
kandidát	kandidát	k1gMnSc1
za	za	k7c4
KDU-ČSL	KDU-ČSL	k1gFnSc4
a	a	k8xC
naposledy	naposledy	k6eAd1
volební	volební	k2eAgMnSc1d1
podporovatel	podporovatel	k1gMnSc1
SPD	SPD	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Hnutí	hnutí	k1gNnSc1
uvádělo	uvádět	k5eAaImAgNnS
rovněž	rovněž	k9
podporu	podpora	k1gFnSc4
kardinála	kardinál	k1gMnSc2
Dominika	Dominik	k1gMnSc2
Duky	Duka	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Mluvčí	mluvčí	k1gMnSc1
kardinála	kardinál	k1gMnSc2
to	ten	k3xDgNnSc1
však	však	k9
odmítl	odmítnout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Hnutí	hnutí	k1gNnSc2
naopak	naopak	k6eAd1
vyjádřil	vyjádřit	k5eAaPmAgInS
sympatie	sympatie	k1gFnSc2
předseda	předseda	k1gMnSc1
ultrapravicové	ultrapravicový	k2eAgFnSc2d1
<g/>
,	,	kIx,
až	až	k6eAd1
neonacistické	neonacistický	k2eAgFnSc2d1
Dělnické	dělnický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
sociální	sociální	k2eAgFnSc2d1
spravedlnosti	spravedlnost	k1gFnSc2
Tomáš	Tomáš	k1gMnSc1
Vandas	Vandas	k1gInSc1
prohlášením	prohlášení	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Trikolóra	trikolóra	k1gFnSc1
shoduje	shodovat	k5eAaImIp3nS
s	s	k7c7
jeho	jeho	k3xOp3gInPc7
názory	názor	k1gInPc7
„	„	k?
<g/>
tak	tak	k6eAd1
na	na	k7c4
95	#num#	k4
procent	procento	k1gNnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Sympatie	sympatie	k1gFnPc1
hnutí	hnutí	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
vyjádřil	vyjádřit	k5eAaPmAgInS
před	před	k7c7
koncem	konec	k1gInSc7
června	červen	k1gInSc2
2019	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
ohlásil	ohlásit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
odchod	odchod	k1gInSc4
z	z	k7c2
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
advokát	advokát	k1gMnSc1
a	a	k8xC
bývalý	bývalý	k2eAgMnSc1d1
poslanec	poslanec	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Koudelka	Koudelka	k1gMnSc1
<g/>
,	,	kIx,
případný	případný	k2eAgInSc1d1
vstup	vstup	k1gInSc1
do	do	k7c2
Trikolóry	trikolóra	k1gFnSc2
však	však	k9
nepotvrdil	potvrdit	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červenci	červenec	k1gInSc6
2019	#num#	k4
hnutí	hnutí	k1gNnSc2
vyjádřil	vyjádřit	k5eAaPmAgMnS
podporu	podpora	k1gFnSc4
někdejší	někdejší	k2eAgMnSc1d1
europoslanec	europoslanec	k1gMnSc1
za	za	k7c4
Svobodné	svobodný	k2eAgMnPc4d1
Petr	Petr	k1gMnSc1
Mach	macha	k1gFnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Klaus	Klaus	k1gMnSc1
ml.	ml.	kA
pak	pak	k6eAd1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
ním	on	k3xPp3gInSc7
počítá	počítat	k5eAaImIp3nS
v	v	k7c6
„	„	k?
<g/>
ekonomické	ekonomický	k2eAgFnSc6d1
sekci	sekce	k1gFnSc6
<g/>
“	“	k?
hnutí	hnutí	k1gNnSc2
spolu	spolu	k6eAd1
s	s	k7c7
Šichtařovou	šichtařův	k2eAgFnSc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Počátkem	počátkem	k7c2
srpna	srpen	k1gInSc2
2019	#num#	k4
Deník	deník	k1gInSc1
N	N	kA
informoval	informovat	k5eAaBmAgInS
o	o	k7c6
dalších	další	k2eAgInPc6d1
nových	nový	k2eAgInPc6d1
registrovaných	registrovaný	k2eAgInPc6d1
příznivcích	příznivec	k1gMnPc6
hnutí	hnutí	k1gNnPc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
bývalém	bývalý	k2eAgInSc6d1
ministru	ministr	k1gMnSc3
školství	školství	k1gNnSc4
za	za	k7c4
Věci	věc	k1gFnPc4
veřejné	veřejný	k2eAgFnPc4d1
v	v	k7c6
Nečasově	Nečasův	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
Josefu	Josefa	k1gFnSc4
Dobešovi	Dobeš	k1gMnSc3
a	a	k8xC
někdejším	někdejší	k2eAgMnSc6d1
ministru	ministr	k1gMnSc6
zahraničí	zahraničí	k1gNnSc2
ve	v	k7c6
Fischerově	Fischerův	k2eAgFnSc6d1
a	a	k8xC
Rusnokově	Rusnokův	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
a	a	k8xC
poradci	poradce	k1gMnPc1
prezidenta	prezident	k1gMnSc2
Zemana	Zeman	k1gMnSc2
Janu	Jan	k1gMnSc3
Kohoutovi	Kohout	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
červenci	červenec	k1gInSc6
2019	#num#	k4
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgFnSc2d2
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
hnutí	hnutí	k1gNnSc1
disponuje	disponovat	k5eAaBmIp3nS
7	#num#	k4
200	#num#	k4
registrovanými	registrovaný	k2eAgMnPc7d1
příznivci	příznivec	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
by	by	kYmCp3nS
se	se	k3xPyFc4
výhledově	výhledově	k6eAd1
měli	mít	k5eAaImAgMnP
stát	stát	k5eAaImF,k5eAaPmF
jeho	jeho	k3xOp3gInPc4
členy	člen	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
srpnu	srpen	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
jich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
8,5	8,5	k4
tisíce	tisíc	k4xCgInSc2
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
v	v	k7c6
září	září	k1gNnSc6
2019	#num#	k4
poté	poté	k6eAd1
10	#num#	k4
tisíc	tisíc	k4xCgInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
prosinci	prosinec	k1gInSc3
2019	#num#	k4
bylo	být	k5eAaImAgNnS
členy	člen	k1gInPc4
hnutí	hnutí	k1gNnSc2
21	#num#	k4
starostů	starosta	k1gMnPc2
českých	český	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
červenci	červenec	k1gInSc6
2020	#num#	k4
navázala	navázat	k5eAaPmAgFnS
s	s	k7c7
hnutím	hnutí	k1gNnSc7
spolupráci	spolupráce	k1gFnSc4
poslankyně	poslankyně	k1gFnSc1
Tereza	Tereza	k1gFnSc1
Hyťhová	Hyťhová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
opustila	opustit	k5eAaPmAgFnS
SPD	SPD	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přípravný	přípravný	k2eAgInSc1d1
výbor	výbor	k1gInSc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
Devítičlenné	devítičlenný	k2eAgNnSc1d1
složení	složení	k1gNnSc1
přípravného	přípravný	k2eAgInSc2d1
výboru	výbor	k1gInSc2
hnutí	hnutí	k1gNnSc2
bylo	být	k5eAaImAgNnS
ohlášeno	ohlášen	k2eAgNnSc4d1
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2019	#num#	k4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
–	–	k?
poslanec	poslanec	k1gMnSc1
PČR	PČR	kA
</s>
<s>
Zuzana	Zuzana	k1gFnSc1
Majerová	Majerová	k1gFnSc1
Zahradníková	Zahradníková	k1gFnSc1
–	–	k?
poslankyně	poslankyně	k1gFnSc2
PČR	PČR	kA
</s>
<s>
Karel	Karel	k1gMnSc1
Rajchl	Rajchl	k1gMnSc1
–	–	k?
ředitel	ředitel	k1gMnSc1
ZŠ	ZŠ	kA
a	a	k8xC
MŠ	MŠ	kA
Děčín	Děčín	k1gInSc1
VIII	VIII	kA
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
člen	člen	k1gMnSc1
ČSSD	ČSSD	kA
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Norbert	Norbert	k1gMnSc1
Hlaváček	Hlaváček	k1gMnSc1
–	–	k?
podnikatel	podnikatel	k1gMnSc1
a	a	k8xC
starosta	starosta	k1gMnSc1
obce	obec	k1gFnSc2
Jenštejn	Jenštejn	k1gMnSc1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Fryč	Fryč	k1gMnSc1
–	–	k?
ředitel	ředitel	k1gMnSc1
společnosti	společnost	k1gFnSc2
Warex	Warex	k1gInSc1
Psáry	Psára	k1gFnSc2
<g/>
,	,	kIx,
dřívější	dřívější	k2eAgMnSc1d1
sponzor	sponzor	k1gMnSc1
KDU-ČSL	KDU-ČSL	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
později	pozdě	k6eAd2
spoluzakladatel	spoluzakladatel	k1gMnSc1
politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Realisté	realista	k1gMnPc1
<g/>
,	,	kIx,
za	za	k7c4
niž	jenž	k3xRgFnSc4
kandidoval	kandidovat	k5eAaImAgMnS
ve	v	k7c6
Středočeském	středočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
jako	jako	k8xC,k8xS
lídr	lídr	k1gMnSc1
ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ivana	Ivana	k1gFnSc1
Kerlesová	Kerlesový	k2eAgFnSc1d1
–	–	k?
majitelka	majitelka	k1gFnSc1
komunikační	komunikační	k2eAgFnPc1d1
agentury	agentura	k1gFnPc1
z	z	k7c2
Českých	český	k2eAgInPc2d1
Budějovic	Budějovice	k1gInPc2
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Grygar	Grygar	k1gMnSc1
–	–	k?
právník	právník	k1gMnSc1
z	z	k7c2
Přerova	Přerov	k1gInSc2
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
krajský	krajský	k2eAgMnSc1d1
místopředseda	místopředseda	k1gMnSc1
Svobodných	svobodný	k2eAgNnPc2d1
a	a	k8xC
asistent	asistent	k1gMnSc1
europoslance	europoslanec	k1gMnSc2
Petra	Petr	k1gMnSc2
Macha	Mach	k1gMnSc2
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jana	Jana	k1gFnSc1
Marková	markový	k2eAgFnSc1d1
–	–	k?
podnikatelka	podnikatelka	k1gFnSc1
z	z	k7c2
Mladé	mladá	k1gFnSc2
Boleslavi	Boleslaev	k1gFnSc3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2017	#num#	k4
kandidující	kandidující	k2eAgFnSc1d1
za	za	k7c4
Realisty	realista	k1gMnPc4
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Natálie	Natálie	k1gFnSc1
Vachatová	Vachatový	k2eAgFnSc1d1
–	–	k?
podnikatelka	podnikatelka	k1gFnSc1
a	a	k8xC
soudní	soudní	k2eAgFnSc1d1
tlumočnice	tlumočnice	k1gFnSc1
z	z	k7c2
Čelákovic	Čelákovice	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2014	#num#	k4
kandidující	kandidující	k2eAgInSc4d1
za	za	k7c4
Svobodné	svobodný	k2eAgInPc4d1
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Volební	volební	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
</s>
<s>
V	v	k7c6
opakovaných	opakovaný	k2eAgFnPc6d1
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
ve	v	k7c6
Strakonicích	Strakonice	k1gFnPc6
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2019	#num#	k4
získalo	získat	k5eAaPmAgNnS
hnutí	hnutí	k1gNnSc1
3,32	3,32	k4
%	%	kIx~
<g/>
,	,	kIx,
tedy	tedy	k9
5	#num#	k4
824	#num#	k4
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
p.	p.	k?
2	#num#	k4
<g/>
]	]	kIx)
nezískalo	získat	k5eNaPmAgNnS
žádný	žádný	k3yNgInSc4
mandát	mandát	k1gInSc4
v	v	k7c6
zastupitelstvu	zastupitelstvo	k1gNnSc6
a	a	k8xC
celkově	celkově	k6eAd1
skončilo	skončit	k5eAaPmAgNnS
v	v	k7c6
pořadí	pořadí	k1gNnSc6
čtvrté	čtvrtá	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgFnPc1
volby	volba	k1gFnPc1
byly	být	k5eAaImAgFnP
první	první	k4xOgFnPc1
<g/>
,	,	kIx,
kterých	který	k3yIgNnPc2,k3yQgNnPc2,k3yRgNnPc2
se	se	k3xPyFc4
Trikolóra	trikolóra	k1gFnSc1
zúčastnila	zúčastnit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgFnSc4d2
před	před	k7c7
volbami	volba	k1gFnPc7
sdílel	sdílet	k5eAaImAgInS
na	na	k7c6
sociálních	sociální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
průzkum	průzkum	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
přisuzoval	přisuzovat	k5eAaImAgInS
hnutí	hnutí	k1gNnSc4
15,8	15,8	k4
%	%	kIx~
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yRnSc4,k3yQnSc4
sklidil	sklidit	k5eAaPmAgInS
kritiku	kritika	k1gFnSc4
<g/>
,	,	kIx,
především	především	k6eAd1
protože	protože	k8xS
tento	tento	k3xDgInSc1
nebyl	být	k5eNaImAgInS
řádně	řádně	k6eAd1
zdrojován	zdrojovat	k5eAaPmNgInS,k5eAaBmNgInS,k5eAaImNgInS
<g/>
,	,	kIx,
na	na	k7c4
což	což	k3yQnSc4,k3yRnSc4
upozornil	upozornit	k5eAaPmAgInS
Deník	deník	k1gInSc1
N.	N.	kA
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
doplňovacích	doplňovací	k2eAgFnPc6d1
senátních	senátní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
ve	v	k7c6
volebním	volební	k2eAgInSc6d1
obvodu	obvod	k1gInSc6
32	#num#	k4
–	–	k?
Teplice	Teplice	k1gFnPc1
v	v	k7c6
červnu	červen	k1gInSc6
2020	#num#	k4
získal	získat	k5eAaPmAgMnS
kandidát	kandidát	k1gMnSc1
Trikolóry	trikolóra	k1gFnSc2
551	#num#	k4
hlasů	hlas	k1gInPc2
a	a	k8xC
umístil	umístit	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c6
předposledním	předposlední	k2eAgNnSc6d1
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Program	program	k1gInSc1
</s>
<s>
Základní	základní	k2eAgInSc1d1
program	program	k1gInSc1
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
ohlášený	ohlášený	k2eAgInSc1d1
v	v	k7c6
červnu	červen	k1gInSc6
2019	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
sdružený	sdružený	k2eAgMnSc1d1
ve	v	k7c6
třech	tři	k4xCgInPc6
bodech	bod	k1gInPc6
<g/>
/	/	kIx~
<g/>
pilířích	pilíř	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
nich	on	k3xPp3gMnPc2
se	se	k3xPyFc4
hnutí	hnutí	k1gNnSc2
chtělo	chtít	k5eAaImAgNnS
dívat	dívat	k5eAaImF
na	na	k7c4
svět	svět	k1gInSc4
českou	český	k2eAgFnSc7d1
perspektivou	perspektiva	k1gFnSc7
<g/>
,	,	kIx,
hájit	hájit	k5eAaImF
pracující	pracující	k2eAgMnPc4d1
občany	občan	k1gMnPc4
a	a	k8xC
„	„	k?
<g/>
bránit	bránit	k5eAaImF
normální	normální	k2eAgInSc4d1
svět	svět	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Představení	představení	k1gNnSc4
detailního	detailní	k2eAgInSc2d1
programu	program	k1gInSc2
bylo	být	k5eAaImAgNnS
ohlášeno	ohlášen	k2eAgNnSc1d1
na	na	k7c6
září	září	k1gNnSc6
2019	#num#	k4
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
ideovou	ideový	k2eAgFnSc7d1
konferenci	konference	k1gFnSc6
hnutí	hnutí	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Program	program	k1gInSc1
hnutí	hnutí	k1gNnSc2
je	být	k5eAaImIp3nS
uvozen	uvodit	k5eAaPmNgInS
heslem	heslo	k1gNnSc7
„	„	k?
<g/>
Vraťme	vrátit	k5eAaPmRp1nP
lidem	lid	k1gInSc7
jejich	jejich	k3xOp3gFnSc4
zemi	zem	k1gFnSc4
<g/>
“	“	k?
a	a	k8xC
člení	členit	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
tři	tři	k4xCgFnPc4
základní	základní	k2eAgFnPc4d1
části	část	k1gFnPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Braňme	bránit	k5eAaImRp1nP
normální	normální	k2eAgInSc4d1
svět	svět	k1gInSc4
</s>
<s>
Bohatství	bohatství	k1gNnSc1
vzniká	vznikat	k5eAaImIp3nS
z	z	k7c2
práce	práce	k1gFnSc2
</s>
<s>
Žijeme	žít	k5eAaImIp1nP
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Sám	sám	k3xTgMnSc1
Klaus	Klaus	k1gMnSc1
později	pozdě	k6eAd2
jako	jako	k9
některé	některý	k3yIgFnPc1
konkrétní	konkrétní	k2eAgFnPc1d1
priority	priorita	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
bude	být	k5eAaImBp3nS
jeho	jeho	k3xOp3gNnSc1
hnutí	hnutí	k1gNnSc1
prosazovat	prosazovat	k5eAaImF
<g/>
,	,	kIx,
označil	označit	k5eAaPmAgMnS
zrušení	zrušení	k1gNnSc3
zákazu	zákaz	k1gInSc2
kouření	kouření	k1gNnSc2
v	v	k7c6
restauracích	restaurace	k1gFnPc6
<g/>
,	,	kIx,
snížení	snížení	k1gNnSc6
daní	daň	k1gFnPc2
podnikatelům	podnikatel	k1gMnPc3
<g/>
,	,	kIx,
podporu	podpora	k1gFnSc4
automotorismu	automotorismus	k1gInSc2
<g/>
,	,	kIx,
podporu	podpor	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
obcí	obec	k1gFnPc2
v	v	k7c6
národních	národní	k2eAgInPc6d1
parcích	park	k1gInPc6
<g/>
,	,	kIx,
bez	bez	k7c2
jejichž	jejichž	k3xOyRp3gInSc2
souhlasu	souhlas	k1gInSc2
by	by	kYmCp3nP
podle	podle	k7c2
Klause	Klaus	k1gMnSc2
neměl	mít	k5eNaImAgInS
být	být	k5eAaImF
přijat	přijmout	k5eAaPmNgInS
žádný	žádný	k3yNgInSc4
zákon	zákon	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
týkal	týkat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klaus	Klaus	k1gMnSc1
se	se	k3xPyFc4
vyhranil	vyhranit	k5eAaPmAgMnS
i	i	k9
proti	proti	k7c3
systému	systém	k1gInSc3
sociálních	sociální	k2eAgFnPc2d1
dávek	dávka	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
kvůli	kvůli	k7c3
tomuto	tento	k3xDgInSc3
systému	systém	k1gInSc3
podle	podle	k7c2
něho	on	k3xPp3gInSc2
lidem	člověk	k1gMnPc3
nevyplatí	vyplatit	k5eNaPmIp3nS
pracovat	pracovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odmítl	odmítnout	k5eAaPmAgMnS
účast	účast	k1gFnSc4
na	na	k7c6
případném	případný	k2eAgInSc6d1
sudetoněmeckém	sudetoněmecký	k2eAgInSc6d1
sjezdu	sjezd	k1gInSc6
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Program	program	k1gInSc4
podle	podle	k7c2
svých	svůj	k3xOyFgNnPc2
slov	slovo	k1gNnPc2
hodlá	hodlat	k5eAaImIp3nS
prosazovat	prosazovat	k5eAaImF
s	s	k7c7
kýmkoli	kdokoli	k3yInSc7
<g/>
,	,	kIx,
nevyhranil	vyhranit	k5eNaPmAgInS
se	se	k3xPyFc4
tedy	tedy	k9
proti	proti	k7c3
v	v	k7c6
současnosti	současnost	k1gFnSc6
nejsilnějšímu	silný	k2eAgInSc3d3
českému	český	k2eAgInSc3d1
politickému	politický	k2eAgInSc3d1
subjektu	subjekt	k1gInSc3
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
je	být	k5eAaImIp3nS
hnutí	hnutí	k1gNnPc4
ANO	ano	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hnutí	hnutí	k1gNnSc1
zakládá	zakládat	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
program	program	k1gInSc4
na	na	k7c6
konzervativním	konzervativní	k2eAgInSc6d1
pravicovém	pravicový	k2eAgInSc6d1
nacionalismu	nacionalismus	k1gInSc6
<g/>
,	,	kIx,
popírání	popírání	k1gNnSc3
klimatických	klimatický	k2eAgFnPc2d1
změn	změna	k1gFnPc2
<g/>
,	,	kIx,
kritice	kritika	k1gFnSc6
sociálního	sociální	k2eAgInSc2d1
státu	stát	k1gInSc2
a	a	k8xC
prosazování	prosazování	k1gNnSc4
co	co	k9
nejnižších	nízký	k2eAgFnPc2d3
daní	daň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
zakladatele	zakladatel	k1gMnSc2
chce	chtít	k5eAaImIp3nS
hájit	hájit	k5eAaImF
konzervativní	konzervativní	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
<g/>
,	,	kIx,
„	„	k?
<g/>
zdravý	zdravý	k2eAgInSc4d1
rozum	rozum	k1gInSc4
<g/>
“	“	k?
a	a	k8xC
pracující	pracující	k2eAgMnPc4d1
lidi	člověk	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Komentátor	komentátor	k1gMnSc1
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
Jan	Jan	k1gMnSc1
Fingerland	Fingerlanda	k1gFnPc2
zpochybňuje	zpochybňovat	k5eAaImIp3nS
konzervatismus	konzervatismus	k1gInSc1
hnutí	hnutí	k1gNnSc2
a	a	k8xC
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
něm	on	k3xPp3gNnSc6
spíše	spíše	k9
jako	jako	k9
o	o	k7c6
radikálním	radikální	k2eAgNnSc6d1
než	než	k8xS
konzervativním	konzervativní	k2eAgNnSc6d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Komentátor	komentátor	k1gMnSc1
Info	Info	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Bohumil	Bohumila	k1gFnPc2
Pečinka	Pečinka	k1gFnSc1
vyhodnotil	vyhodnotit	k5eAaPmAgMnS
z	z	k7c2
prvotního	prvotní	k2eAgNnSc2d1
prohlášení	prohlášení	k1gNnSc2
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
nejspíš	nejspíš	k9
„	„	k?
<g/>
něčím	něčí	k3xOyIgInSc7
jako	jako	k9
lepší	dobrý	k2eAgMnSc1d2
ODS	ODS	kA
<g/>
,	,	kIx,
tedy	tedy	k9
pravicovou	pravicový	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
bude	být	k5eAaImBp3nS
jen	jen	k6eAd1
ostřejší	ostrý	k2eAgFnSc1d2
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
EU	EU	kA
a	a	k8xC
levicovému	levicový	k2eAgInSc3d1
progresivismu	progresivismus	k1gInSc3
v	v	k7c6
morálně-kulturních	morálně-kulturní	k2eAgFnPc6d1
otázkách	otázka	k1gFnPc6
<g/>
.	.	kIx.
<g/>
“	“	k?
Podle	podle	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
něho	on	k3xPp3gMnSc2
však	však	k9
voliči	volič	k1gMnPc1
ODS	ODS	kA
nesdílejí	sdílet	k5eNaImIp3nP
základní	základní	k2eAgFnPc4d1
názorové	názorový	k2eAgFnPc4d1
charakteristiky	charakteristika	k1gFnPc4
s	s	k7c7
Klausem	Klaus	k1gMnSc7
mladším	mladý	k2eAgMnSc7d2
a	a	k8xC
„	„	k?
<g/>
Trikolóra	trikolóra	k1gFnSc1
si	se	k3xPyFc3
musí	muset	k5eAaImIp3nS
projít	projít	k5eAaPmF
fází	fáze	k1gFnSc7
hry	hra	k1gFnSc2
na	na	k7c4
klasicky	klasicky	k6eAd1
pravicovou	pravicový	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
s	s	k7c7
liberálními	liberální	k2eAgInPc7d1
pohledy	pohled	k1gInPc7
na	na	k7c4
ekonomiku	ekonomika	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohla	moct	k5eAaImAgFnS
zakotvit	zakotvit	k5eAaPmF
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
její	její	k3xOp3gMnPc1
skuteční	skutečný	k2eAgMnPc1d1
voliči	volič	k1gMnPc1
a	a	k8xC
názory	názor	k1gInPc4
jejich	jejich	k3xOp3gMnSc2
předsedy	předseda	k1gMnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
Pečinky	Pečinka	k1gFnSc2
by	by	kYmCp3nS
Trikolóra	trikolóra	k1gFnSc1
v	v	k7c6
budoucnosti	budoucnost	k1gFnSc6
mohla	moct	k5eAaImAgFnS
„	„	k?
<g/>
jít	jít	k5eAaImF
cestou	cesta	k1gFnSc7
«	«	k?
<g/>
podmínečného	podmínečný	k2eAgInSc2d1
czexitu	czexit	k1gInSc2
<g/>
»	»	k?
<g/>
,	,	kIx,
zvednout	zvednout	k5eAaPmF
prapor	prapor	k1gInSc4
českého	český	k2eAgInSc2d1
nacionalismu	nacionalismus	k1gInSc2
<g/>
,	,	kIx,
vypracovat	vypracovat	k5eAaPmF
nějakou	nějaký	k3yIgFnSc4
verzi	verze	k1gFnSc4
protiimigrační	protiimigrační	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
a	a	k8xC
čekat	čekat	k5eAaImF
<g/>
,	,	kIx,
jestli	jestli	k8xS
se	se	k3xPyFc4
hnutí	hnutí	k1gNnSc1
ANO	ano	k9
nezačne	začít	k5eNaPmIp3nS
drolit	drolit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stručně	stručně	k6eAd1
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
být	být	k5eAaImF
hnutím	hnutí	k1gNnSc7
pro	pro	k7c4
zklamané	zklamaný	k2eAgMnPc4d1
okamurovce	okamurovec	k1gMnPc4
a	a	k8xC
anoisty	anoista	k1gMnPc4
s	s	k7c7
maturitou	maturita	k1gFnSc7
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sám	sám	k3xTgMnSc1
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
v	v	k7c6
pořadu	pořad	k1gInSc6
Rozstřel	rozstřel	k1gInSc1
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
zpochybnil	zpochybnit	k5eAaPmAgInS
jednoznačnost	jednoznačnost	k1gFnSc4
pravicové	pravicový	k2eAgFnSc2d1
pozice	pozice	k1gFnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
jeho	jeho	k3xOp3gNnPc2
slov	slovo	k1gNnPc2
„	„	k?
<g/>
[	[	kIx(
<g/>
nelze	lze	k6eNd1
<g/>
]	]	kIx)
říct	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
bychom	by	kYmCp1nP
patřili	patřit	k5eAaImAgMnP
vyloženě	vyloženě	k6eAd1
k	k	k7c3
pravici	pravice	k1gFnSc3
nebo	nebo	k8xC
levici	levice	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chceme	chtít	k5eAaImIp1nP
se	se	k3xPyFc4
zaměřovat	zaměřovat	k5eAaImF
na	na	k7c4
témata	téma	k1gNnPc4
z	z	k7c2
obou	dva	k4xCgFnPc2
oblastí	oblast	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomtéž	týž	k3xTgInSc6
pořadu	pořad	k1gInSc6
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
hnutí	hnutí	k1gNnSc1
Trikolóra	trikolóra	k1gFnSc1
mělo	mít	k5eAaImAgNnS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
programu	program	k1gInSc6
czexit	czexit	k5eAaImF,k5eAaPmF,k5eAaBmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
Už	už	k6eAd1
v	v	k7c6
dubnu	duben	k1gInSc6
2019	#num#	k4
v	v	k7c6
rozhovoru	rozhovor	k1gInSc6
pro	pro	k7c4
deník	deník	k1gInSc4
Právo	právo	k1gNnSc4
uvedl	uvést	k5eAaPmAgInS
<g/>
:	:	kIx,
“	“	k?
<g/>
nejde	jít	k5eNaImIp3nS
o	o	k7c4
ryze	ryze	k6eAd1
pravicový	pravicový	k2eAgInSc4d1
elektorát	elektorát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Myslím	myslet	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
pojmy	pojem	k1gInPc1
levice	levice	k1gFnSc2
a	a	k8xC
pravice	pravice	k1gFnSc2
jsou	být	k5eAaImIp3nP
zprofanované	zprofanovaný	k2eAgFnPc1d1
<g/>
,	,	kIx,
lidé	člověk	k1gMnPc1
jim	on	k3xPp3gMnPc3
nerozumí	rozumět	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
o	o	k7c6
nich	on	k3xPp3gFnPc6
nemá	mít	k5eNaImIp3nS
smysl	smysl	k1gInSc1
hovořit	hovořit	k5eAaImF
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
rozhovoru	rozhovor	k1gInSc6
pro	pro	k7c4
Xtv	Xtv	k1gFnSc4
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2019	#num#	k4
dále	daleko	k6eAd2
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
členem	člen	k1gMnSc7
přípravného	přípravný	k2eAgInSc2d1
výboru	výbor	k1gInSc2
hnutí	hnutí	k1gNnSc2
Trikolóra	trikolóra	k1gFnSc1
je	být	k5eAaImIp3nS
i	i	k9
sociální	sociální	k2eAgMnSc1d1
demokrat	demokrat	k1gMnSc1
<g/>
,	,	kIx,
blíže	blízce	k6eAd2
však	však	k9
osobu	osoba	k1gFnSc4
nespecifikoval	specifikovat	k5eNaBmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Financování	financování	k1gNnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
junior	junior	k1gMnSc1
v	v	k7c6
červnu	červen	k1gInSc6
2019	#num#	k4
uváděl	uvádět	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
financování	financování	k1gNnSc4
volebních	volební	k2eAgFnPc2d1
kampaní	kampaň	k1gFnPc2
nechce	chtít	k5eNaImIp3nS
hnutí	hnutí	k1gNnSc1
přijmout	přijmout	k5eAaPmF
žádné	žádný	k3yNgInPc4
peníze	peníz	k1gInPc4
ze	z	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
chtěl	chtít	k5eAaImAgMnS
by	by	kYmCp3nS
zajistit	zajistit	k5eAaPmF
financování	financování	k1gNnSc1
výhradně	výhradně	k6eAd1
z	z	k7c2
českých	český	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
k	k	k7c3
tomu	ten	k3xDgNnSc3
připomněly	připomnět	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
hlavním	hlavní	k2eAgMnSc7d1
sponzorem	sponzor	k1gMnSc7
Institutu	institut	k1gInSc2
Václava	Václav	k1gMnSc2
Klause	Klaus	k1gMnSc2
je	být	k5eAaImIp3nS
skupina	skupina	k1gFnSc1
PPF	PPF	kA
Petra	Petr	k1gMnSc2
Kellnera	Kellner	k1gMnSc2
<g/>
,	,	kIx,
masivně	masivně	k6eAd1
podnikající	podnikající	k2eAgFnSc1d1
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
Indii	Indie	k1gFnSc6
<g/>
,	,	kIx,
USA	USA	kA
a	a	k8xC
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klaus	Klaus	k1gMnSc1
junior	junior	k1gMnSc1
označil	označit	k5eAaPmAgMnS
Kellnerovu	Kellnerův	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
za	za	k7c4
českou	český	k2eAgFnSc4d1
<g/>
,	,	kIx,
potenciální	potenciální	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
však	však	k9
nijak	nijak	k6eAd1
nepotvrdil	potvrdit	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
červenci	červenec	k1gInSc3
2019	#num#	k4
patřil	patřit	k5eAaImAgInS
mezi	mezi	k7c4
největší	veliký	k2eAgMnPc4d3
sponzory	sponzor	k1gMnPc4
strany	strana	k1gFnSc2
podnikatel	podnikatel	k1gMnSc1
Antonín	Antonín	k1gMnSc1
Fryč	Fryč	k1gMnSc1
(	(	kIx(
<g/>
250	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
člen	člen	k1gMnSc1
přípravného	přípravný	k2eAgInSc2d1
výboru	výbor	k1gInSc2
<g/>
,	,	kIx,
podnikatel	podnikatel	k1gMnSc1
a	a	k8xC
bývalý	bývalý	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Státního	státní	k2eAgInSc2d1
fondu	fond	k1gInSc2
rozvoje	rozvoj	k1gInSc2
bydlení	bydlení	k1gNnSc1
Ľubomír	Ľubomír	k1gMnSc1
Vais	Vais	k1gMnSc1
(	(	kIx(
<g/>
100	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
)	)	kIx)
a	a	k8xC
Karel	Karel	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
(	(	kIx(
<g/>
100	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
transparentní	transparentní	k2eAgInSc4d1
účet	účet	k1gInSc4
strany	strana	k1gFnSc2
přišly	přijít	k5eAaPmAgInP
od	od	k7c2
založení	založení	k1gNnSc2
strany	strana	k1gFnSc2
do	do	k7c2
počátku	počátek	k1gInSc2
října	říjen	k1gInSc2
2020	#num#	k4
dary	dar	k1gInPc4
v	v	k7c6
celkové	celkový	k2eAgFnSc6d1
hodnotě	hodnota	k1gFnSc6
23,9	23,9	k4
milionu	milion	k4xCgInSc2
korun	koruna	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
17	#num#	k4
milionů	milion	k4xCgInPc2
získalo	získat	k5eAaPmAgNnS
hnutí	hnutí	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Politické	politický	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
v	v	k7c6
červnu	červen	k1gInSc6
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanci	poslanec	k1gMnPc1
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
a	a	k8xC
Zuzana	Zuzana	k1gFnSc1
Majerová	Majerová	k1gFnSc1
Zahradníková	Zahradníková	k1gFnSc1
byli	být	k5eAaImAgMnP
do	do	k7c2
Sněmovny	sněmovna	k1gFnSc2
zvoleni	zvolen	k2eAgMnPc1d1
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
za	za	k7c4
ODS	ODS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
2020	#num#	k4
navázala	navázat	k5eAaPmAgFnS
s	s	k7c7
Trikolórou	trikolóra	k1gFnSc7
spolupráci	spolupráce	k1gFnSc4
Tereza	Tereza	k1gFnSc1
Hyťhová	Hyťhová	k1gFnSc1
<g/>
,	,	kIx,
zvolená	zvolený	k2eAgFnSc1d1
na	na	k7c6
kandidátce	kandidátka	k1gFnSc6
SPD	SPD	kA
Tomia	Tomius	k1gMnSc2
Okamury	Okamura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
tak	tak	k6eAd1
reálně	reálně	k6eAd1
disponuje	disponovat	k5eAaBmIp3nS
3	#num#	k4
poslaneckými	poslanecký	k2eAgInPc7d1
mandáty	mandát	k1gInPc7
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
počet	počet	k1gInSc4
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
voličů	volič	k1gMnPc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
v	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
má	mít	k5eAaImIp3nS
volič	volič	k1gInSc4
více	hodně	k6eAd2
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Rejstřík	rejstřík	k1gInSc1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
a	a	k8xC
politických	politický	k2eAgNnPc2d1
hnutí	hnutí	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledek	výsledek	k1gInSc1
vyhledávání	vyhledávání	k1gNnSc2
podle	podle	k7c2
názvu	název	k1gInSc2
<g/>
,	,	kIx,
typ	typ	k1gInSc4
Politické	politický	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
<g/>
,	,	kIx,
poté	poté	k6eAd1
odkaz	odkaz	k1gInSc1
Historie	historie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Trikolóra	trikolóra	k1gFnSc1
hnutí	hnutí	k1gNnSc2
občanů	občan	k1gMnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trikolóra	trikolóra	k1gFnSc1
hnutí	hnutí	k1gNnSc2
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Předsednictvo	předsednictvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BERGER	Berger	k1gMnSc1
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miliardář	miliardář	k1gMnSc1
Valenta	Valenta	k1gMnSc1
přišel	přijít	k5eAaPmAgMnS
o	o	k7c4
senátorské	senátorský	k2eAgNnSc4d1
křeslo	křeslo	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
o	o	k7c4
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
odstřižením	odstřižení	k1gNnSc7
od	od	k7c2
médií	médium	k1gNnPc2
spěchat	spěchat	k5eAaImF
nemusí	muset	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
HlídacíPes	HlídacíPes	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-10-12	2020-10-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BARTONÍČEK	Bartoníček	k1gMnSc1
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klaus	Klaus	k1gMnSc1
ml.	ml.	kA
představil	představit	k5eAaPmAgMnS
nové	nový	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
Trikolóra	trikolóra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vezmeme	vzít	k5eAaPmIp1nP
si	se	k3xPyFc3
naši	náš	k3xOp1gFnSc4
zemi	zem	k1gFnSc4
zpátky	zpátky	k6eAd1
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2019-06-10	2019-06-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
REŽŇÁKOVÁ	REŽŇÁKOVÁ	kA
<g/>
,	,	kIx,
Lada	Lada	k1gFnSc1
(	(	kIx(
<g/>
lre	lre	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrum	centrum	k1gNnSc1
spjaté	spjatý	k2eAgFnSc2d1
s	s	k7c7
Klausem	Klaus	k1gMnSc7
ml.	ml.	kA
si	se	k3xPyFc3
zaregistrovalo	zaregistrovat	k5eAaPmAgNnS
značku	značka	k1gFnSc4
Trikolóra	trikolóra	k1gFnSc1
hnutí	hnutí	k1gNnSc2
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-06-04	2019-06-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JANÁKOVÁ	Janáková	k1gFnSc1
<g/>
,	,	kIx,
Barbora	Barbora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trikolóra	trikolóra	k1gFnSc1
hnutí	hnutí	k1gNnSc2
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgFnSc2d2
si	se	k3xPyFc3
zaregistroval	zaregistrovat	k5eAaPmAgInS
název	název	k1gInSc4
nové	nový	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
hledá	hledat	k5eAaImIp3nS
i	i	k9
vlivné	vlivný	k2eAgMnPc4d1
podporovatele	podporovatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
N	N	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-04	2019-06-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://denikn.cz/530917/referendum-o-eu-povinne-pri-kazdych-snemovnich-volbach-trikolora-trumfuje-spd/	https://denikn.cz/530917/referendum-o-eu-povinne-pri-kazdych-snemovnich-volbach-trikolora-trumfuje-spd/	k4
<g/>
↑	↑	k?
KAŠPÁREK	Kašpárek	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
LGBTQ	LGBTQ	kA
<g/>
+	+	kIx~
právech	právo	k1gNnPc6
se	se	k3xPyFc4
nikam	nikam	k6eAd1
neposouváme	posouvat	k5eNaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problematické	problematický	k2eAgFnPc4d1
zůstávají	zůstávat	k5eAaImIp3nP
sterilizace	sterilizace	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
Referendum	referendum	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-05-27	2020-05-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Poslankyně	poslankyně	k1gFnSc1
Zahradníková	Zahradníková	k1gFnSc1
o	o	k7c6
nové	nový	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
:	:	kIx,
Náš	náš	k3xOp1gInSc1
potenciál	potenciál	k1gInSc1
je	být	k5eAaImIp3nS
víc	hodně	k6eAd2
než	než	k8xS
14	#num#	k4
procent	procento	k1gNnPc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-05-20	2019-05-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
RYCHLÍKOVÁ	Rychlíková	k1gFnSc1
<g/>
,	,	kIx,
Apolena	Apolena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
bojí	bát	k5eAaImIp3nS
LGBT	LGBT	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2020-07-08	2020-07-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DEMAGOG	demagog	k1gMnSc1
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Demagog	demagog	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HEROLDOVÁ	HEROLDOVÁ	kA
<g/>
,	,	kIx,
Martina	Martina	k1gFnSc1
<g/>
;	;	kIx,
BARTONÍČEK	Bartoníček	k1gMnSc1
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
chce	chtít	k5eAaImIp3nS
s	s	k7c7
Klausem	Klaus	k1gMnSc7
hájit	hájit	k5eAaImF
barvy	barva	k1gFnSc2
Trikolóry	trikolóra	k1gFnSc2
<g/>
:	:	kIx,
podnikatelky	podnikatelka	k1gFnSc2
<g/>
,	,	kIx,
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
právník	právník	k1gMnSc1
či	či	k8xC
tlumočnice	tlumočnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2019-06-11	2019-06-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
RAMBOUSKOVÁ	Rambousková	k1gFnSc1
<g/>
,	,	kIx,
Michaela	Michaela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
končí	končit	k5eAaImIp3nS
v	v	k7c6
politice	politika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvody	důvod	k1gInPc7
jsou	být	k5eAaImIp3nP
osobní	osobní	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznamzpravy	Seznamzprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-03-23	2021-03-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Trikolóru	trikolóra	k1gFnSc4
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
Majerová	Majerová	k1gFnSc1
Zahradníková	Zahradníková	k1gFnSc1
<g/>
,	,	kIx,
Klaus	Klaus	k1gMnSc1
ml.	ml.	kA
rezignoval	rezignovat	k5eAaBmAgMnS
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
rezignoval	rezignovat	k5eAaBmAgInS
na	na	k7c4
předsedu	předseda	k1gMnSc4
Trikolóry	trikolóra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stranu	strana	k1gFnSc4
nově	nově	k6eAd1
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
Majerová	Majerová	k1gFnSc1
|	|	kIx~
Domov	domov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-03-23	2021-03-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ČTK	ČTK	kA
<g/>
;	;	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
ODS	ODS	kA
vyzvala	vyzvat	k5eAaPmAgFnS
Klause	Klaus	k1gMnSc4
k	k	k7c3
odchodu	odchod	k1gInSc3
z	z	k7c2
poslaneckého	poslanecký	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
to	ten	k3xDgNnSc4
odmítá	odmítat	k5eAaImIp3nS
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-03-13	2019-03-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klaus	Klaus	k1gMnSc1
srovnal	srovnat	k5eAaPmAgMnS
zákon	zákon	k1gInSc4
s	s	k7c7
jednáním	jednání	k1gNnSc7
Židů	Žid	k1gMnPc2
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
jde	jít	k5eAaImIp3nS
do	do	k7c2
transportu	transport	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fiala	Fiala	k1gMnSc1
se	se	k3xPyFc4
omluvil	omluvit	k5eAaPmAgMnS
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-03-12	2019-03-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Klausův	Klausův	k2eAgInSc4d1
příměr	příměr	k1gInSc4
zákona	zákon	k1gInSc2
k	k	k7c3
Židům	Žid	k1gMnPc3
do	do	k7c2
transportu	transport	k1gInSc2
za	za	k7c4
války	válek	k1gInPc4
bude	být	k5eAaImBp3nS
ODS	ODS	kA
dál	daleko	k6eAd2
řešit	řešit	k5eAaImF
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-03-13	2019-03-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klaus	Klaus	k1gMnSc1
se	se	k3xPyFc4
stále	stále	k6eAd1
více	hodně	k6eAd2
vzdaluje	vzdalovat	k5eAaImIp3nS
hodnotám	hodnota	k1gFnPc3
a	a	k8xC
kultuře	kultura	k1gFnSc3
ODS	ODS	kA
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
šéf	šéf	k1gMnSc1
strany	strana	k1gFnSc2
Fiala	Fiala	k1gMnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-03-14	2019-03-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ODS	ODS	kA
nadbíhá	nadbíhat	k5eAaImIp3nS
pravdě	pravda	k1gFnSc3
a	a	k8xC
lásce	láska	k1gFnSc3
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
vzniknout	vzniknout	k5eAaPmF
nová	nový	k2eAgFnSc1d1
pravicová	pravicový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
míní	mínit	k5eAaImIp3nS
Klaus	Klaus	k1gMnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-03-14	2019-03-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klause	Klaus	k1gMnSc4
vyloučili	vyloučit	k5eAaPmAgMnP
z	z	k7c2
ODS	ODS	kA
<g/>
,	,	kIx,
zaniklo	zaniknout	k5eAaPmAgNnS
mu	on	k3xPp3gMnSc3
i	i	k9
členství	členství	k1gNnSc3
v	v	k7c6
poslaneckém	poslanecký	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-03-16	2019-03-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
VAVERKOVÁ	Vaverková	k1gFnSc1
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ODS	ODS	kA
nesnesla	snést	k5eNaPmAgFnS
rebela	rebel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Názory	názor	k1gInPc4
nezměním	změnit	k5eNaPmIp1nS
<g/>
,	,	kIx,
hlásí	hlásit	k5eAaImIp3nS
Klaus	Klaus	k1gMnSc1
ml.	ml.	kA
po	po	k7c6
vyhazovu	vyhazov	k1gInSc6
<g/>
.	.	kIx.
echo	echo	k1gNnSc1
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-17	2019-03-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KRUTILEK	KRUTILEK	kA
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
„	„	k?
<g/>
Ty	ty	k3xPp2nSc5
seš	seš	k?
ten	ten	k3xDgMnSc1
Jakl	Jakl	k1gMnSc1
<g/>
,	,	kIx,
lídr	lídr	k1gMnSc1
SPD	SPD	kA
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
Útočníci	útočník	k1gMnPc1
v	v	k7c6
metru	metr	k1gInSc6
napadli	napadnout	k5eAaPmAgMnP
člena	člen	k1gMnSc4
vysílací	vysílací	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-05-28	2019-05-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOLÁŘ	Kolář	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voliči	volič	k1gMnPc1
důvěřují	důvěřovat	k5eAaImIp3nP
více	hodně	k6eAd2
Klausovi	Klaus	k1gMnSc3
mladšímu	mladý	k2eAgInSc3d2
než	než	k8xS
Fialovi	Fiala	k1gMnSc3
<g/>
,	,	kIx,
ukázal	ukázat	k5eAaPmAgMnS
průzkum	průzkum	k1gInSc4
ODS	ODS	kA
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-03-16	2019-03-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klausové	Klaus	k1gMnPc1
zakládají	zakládat	k5eAaImIp3nP
novou	nový	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budou	být	k5eAaImBp3nP
cílit	cílit	k5eAaImF
na	na	k7c4
voliče	volič	k1gInPc4
ODS	ODS	kA
a	a	k8xC
SPD	SPD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-04-10	2019-04-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klaus	Klaus	k1gMnSc1
ml.	ml.	kA
zakládá	zakládat	k5eAaImIp3nS
novou	nový	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
v	v	k7c6
ní	on	k3xPp3gFnSc6
působit	působit	k5eAaImF
i	i	k8xC
exprezident	exprezident	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víc	hodně	k6eAd2
po	po	k7c6
eurovolbách	eurovolba	k1gFnPc6
<g/>
,	,	kIx,
slibuje	slibovat	k5eAaImIp3nS
|	|	kIx~
Domov	domov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-04-10	2019-04-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FENDRYCH	FENDRYCH	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klaus	Klaus	k1gMnSc1
junior	junior	k1gMnSc1
staví	stavit	k5eAaBmIp3nS,k5eAaImIp3nS,k5eAaPmIp3nS
stranu	strana	k1gFnSc4
podle	podle	k7c2
zemanovsko-klausovského	zemanovsko-klausovský	k2eAgInSc2d1
vzoru	vzor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2019-04-09	2019-04-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
ČTK	ČTK	kA
<g/>
.	.	kIx.
‚	‚	k?
<g/>
Vrátit	vrátit	k5eAaPmF
tuto	tento	k3xDgFnSc4
zem	zem	k1gFnSc4
lidem	lid	k1gInSc7
<g/>
.	.	kIx.
<g/>
‘	‘	k?
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
po	po	k7c6
rozchodu	rozchod	k1gInSc6
s	s	k7c7
ODS	ODS	kA
založil	založit	k5eAaPmAgMnS
nové	nový	k2eAgNnSc4d1
konzervativní	konzervativní	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
Trikolóra	trikolóra	k1gFnSc1
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2019-06-10	2019-06-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KMENTA	Kment	k1gMnSc4
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
Trikolóra	trikolóra	k1gFnSc1
u	u	k7c2
Klausů	Klaus	k1gMnPc2
<g/>
.	.	kIx.
forum	forum	k1gNnSc1
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-21	2019-06-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
NOVOTNY	NOVOTNY	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
@	@	kIx~
<g/>
JanNovotny	JanNovotna	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-20	2019-06-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protesty	protest	k1gInPc7
důvěru	důvěra	k1gFnSc4
v	v	k7c6
ANO	ano	k9
nepodlomily	podlomit	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babišovo	Babišův	k2eAgNnSc1d1
hnutí	hnutí	k1gNnPc2
by	by	kYmCp3nP
ve	v	k7c6
volbách	volba	k1gFnPc6
získalo	získat	k5eAaPmAgNnS
29	#num#	k4
procent	procento	k1gNnPc2
<g/>
,	,	kIx,
druzí	druhý	k4xOgMnPc1
jsou	být	k5eAaImIp3nP
piráti	pirát	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-28	2019-06-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
mkk	mkk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Preference	preference	k1gFnSc1
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
klesají	klesat	k5eAaImIp3nP
<g/>
,	,	kIx,
v	v	k7c6
průzkumu	průzkum	k1gInSc6
má	mít	k5eAaImIp3nS
nejhorší	zlý	k2eAgInSc1d3
výsledek	výsledek	k1gInSc1
od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2019-06-30	2019-06-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
;	;	kIx,
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babišovo	Babišův	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
ANO	ano	k9
brutálně	brutálně	k6eAd1
posiluje	posilovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebo	nebo	k8xC
také	také	k9
ne	ne	k9
<g/>
,	,	kIx,
záleží	záležet	k5eAaImIp3nS
na	na	k7c6
průzkumu	průzkum	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-12-10	2019-12-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
kac	kac	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
prvním	první	k4xOgInSc6
místě	místo	k1gNnSc6
se	se	k3xPyFc4
drží	držet	k5eAaImIp3nP
ANO	ano	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
rostly	růst	k5eAaImAgInP
nejvíce	hodně	k6eAd3,k6eAd1
preference	preference	k1gFnSc2
Pirátům	pirát	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-05-10	2020-05-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BARTONÍČEK	Bartoníček	k1gMnSc1
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojďte	jít	k5eAaImRp2nP
si	se	k3xPyFc3
vzít	vzít	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
zemi	zem	k1gFnSc4
zpět	zpět	k6eAd1
<g/>
,	,	kIx,
vyzýval	vyzývat	k5eAaImAgMnS
Klaus	Klaus	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
zvolený	zvolený	k2eAgMnSc1d1
předsedou	předseda	k1gMnSc7
Trikolóry	trikolóra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-09-28	2019-09-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČERNÁ	Černá	k1gFnSc1
<g/>
,	,	kIx,
Monika	Monika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Václava	Václav	k1gMnSc2
Klause	Klaus	k1gMnSc2
ml.	ml.	kA
<g/>
:	:	kIx,
Napětí	napětí	k1gNnSc1
mezi	mezi	k7c7
otcem	otec	k1gMnSc7
a	a	k8xC
synem	syn	k1gMnSc7
roste	růst	k5eAaImIp3nS
<g/>
,	,	kIx,
exprezident	exprezident	k1gMnSc1
není	být	k5eNaImIp3nS
spokojen	spokojen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
EuroZprávy	EuroZpráva	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-04	2019-06-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šanci	šance	k1gFnSc4
vidí	vidět	k5eAaImIp3nS
Klaus	Klaus	k1gMnSc1
u	u	k7c2
více	hodně	k6eAd2
než	než	k8xS
deseti	deset	k4xCc2
nespokojených	spokojený	k2eNgMnPc2d1
poslanců	poslanec	k1gMnPc2
napříč	napříč	k7c7
stranami	strana	k1gFnPc7
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-06-11	2019-06-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
MARTINEK	Martinka	k1gFnPc2
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
ORHOLZ	ORHOLZ	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanec	poslanec	k1gMnSc1
ANO	ano	k9
podpořil	podpořit	k5eAaPmAgMnS
Klausovu	Klausův	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-12	2019-06-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PERKNEROVÁ	PERKNEROVÁ	kA
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klausovu	Klausův	k2eAgFnSc4d1
Trikolóru	trikolóra	k1gFnSc4
přivítal	přivítat	k5eAaPmAgMnS
i	i	k9
kardinál	kardinál	k1gMnSc1
Duka	Duka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-13	2019-06-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
POLÁK	Polák	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporuje	podporovat	k5eAaImIp3nS
nás	my	k3xPp1nPc4
i	i	k9
Duka	Duka	k1gMnSc1
<g/>
,	,	kIx,
chlubí	chlubit	k5eAaImIp3nS
se	se	k3xPyFc4
Trikolóra	trikolóra	k1gFnSc1
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kardinál	kardinál	k1gMnSc1
ale	ale	k9
o	o	k7c6
ničem	nic	k3yNnSc6
neví	vědět	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2019-06-14	2019-06-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BERNÁTH	BERNÁTH	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
‚	‚	k?
<g/>
S	s	k7c7
mými	můj	k3xOp1gInPc7
názory	názor	k1gInPc7
se	se	k3xPyFc4
shoduje	shodovat	k5eAaImIp3nS
tak	tak	k6eAd1
z	z	k7c2
95	#num#	k4
procent	procento	k1gNnPc2
<g/>
.	.	kIx.
<g/>
‘	‘	k?
Ultrapravicový	ultrapravicový	k2eAgMnSc1d1
politik	politik	k1gMnSc1
Vandas	Vandas	k1gMnSc1
pochválil	pochválit	k5eAaPmAgMnS
Klausovo	Klausův	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
Trikolóra	trikolóra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-06-13	2019-06-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právník	právník	k1gMnSc1
a	a	k8xC
exposlanec	exposlanec	k1gMnSc1
Koudelka	Koudelka	k1gMnSc1
končí	končit	k5eAaImIp3nS
v	v	k7c6
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
blízko	blízko	k6eAd1
k	k	k7c3
Trikolóře	trikolóra	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČeskéNoviny	ČeskéNovina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
tisková	tiskový	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
<g/>
,	,	kIx,
2019-06-26	2019-06-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HRABĚ	Hrabě	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
ml.	ml.	kA
poodkryl	poodkrýt	k5eAaPmAgMnS
zákulisí	zákulisí	k1gNnSc4
Trikolóry	trikolóra	k1gFnSc2
<g/>
:	:	kIx,
Promluvil	promluvit	k5eAaPmAgMnS
o	o	k7c6
nové	nový	k2eAgFnSc6d1
posile	posila	k1gFnSc6
i	i	k8xC
podpoře	podpora	k1gFnSc6
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
EuroZprávy	EuroZpráva	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Incorp	Incorp	k1gMnSc1
<g/>
,	,	kIx,
2019-07-13	2019-07-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
MAZANCOVÁ	MAZANCOVÁ	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trikolóra	trikolóra	k1gFnSc1
ulovila	ulovit	k5eAaPmAgFnS
„	„	k?
<g/>
nejlepšího	dobrý	k2eAgMnSc2d3
ministra	ministr	k1gMnSc2
školství	školství	k1gNnSc2
od	od	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
<g/>
“	“	k?
i	i	k8xC
Zemanova	Zemanův	k2eAgMnSc2d1
poradce	poradce	k1gMnSc2
a	a	k8xC
exšéfa	exšéf	k1gMnSc2
diplomacie	diplomacie	k1gFnSc2
Kohouta	Kohout	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
N	N	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-08-14	2019-08-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FOŘTOVÁ	Fořtová	k1gFnSc1
<g/>
,	,	kIx,
Klára	Klára	k1gFnSc1
(	(	kIx(
<g/>
kfo	kfo	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trikolóra	trikolóra	k1gFnSc1
přitahuje	přitahovat	k5eAaImIp3nS
<g/>
,	,	kIx,
Klause	Klaus	k1gMnSc4
podporují	podporovat	k5eAaImIp3nP
i	i	k9
exministr	exministr	k1gMnSc1
a	a	k8xC
poradce	poradce	k1gMnSc1
prezidenta	prezident	k1gMnSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-08-14	2019-08-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BRODNÍČKOVÁ	BRODNÍČKOVÁ	kA
<g/>
,	,	kIx,
Karolina	Karolinum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trikolóra	trikolóra	k1gFnSc1
má	mít	k5eAaImIp3nS
už	už	k6eAd1
víc	hodně	k6eAd2
fanoušků	fanoušek	k1gMnPc2
než	než	k8xS
STAN	stan	k1gInSc1
a	a	k8xC
Piráti	pirát	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Borgis	borgis	k1gInSc1
a.s.	a.s.	k?
<g/>
,	,	kIx,
2019-07-22	2019-07-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
předsedou	předseda	k1gMnSc7
Trikolóry	trikolóra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hloupí	hloupit	k5eAaImIp3nS
a	a	k8xC
líní	líní	k2eAgFnSc1d1
studují	studovat	k5eAaImIp3nP
pavědecké	pavědecký	k2eAgInPc1d1
obory	obor	k1gInPc1
<g/>
,	,	kIx,
místo	místo	k6eAd1
aby	aby	kYmCp3nP
pracovali	pracovat	k5eAaImAgMnP
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
v	v	k7c6
projevu	projev	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-09-28	2019-09-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Trikolóra	trikolóra	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trikolóra	trikolóra	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Naši	náš	k3xOp1gMnPc1
lidé	člověk	k1gMnPc1
<g/>
:	:	kIx,
Předsedové	předseda	k1gMnPc1
odborných	odborný	k2eAgFnPc2d1
komisí	komise	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JANÁKOVÁ	Janáková	k1gFnSc1
<g/>
,	,	kIx,
Barbora	Barbora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hyťhová	Hyťhová	k1gFnSc1
poznala	poznat	k5eAaPmAgFnS
„	„	k?
<g/>
skutečnou	skutečný	k2eAgFnSc4d1
podstatu	podstata	k1gFnSc4
SPD	SPD	kA
<g/>
“	“	k?
a	a	k8xC
šla	jít	k5eAaImAgFnS
do	do	k7c2
Trikolóry	trikolóra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Okamurou	Okamura	k1gFnSc7
jsme	být	k5eAaImIp1nP
se	se	k3xPyFc4
nehádali	hádat	k5eNaImAgMnP
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
poslankyně	poslankyně	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
N	N	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-07-27	2020-07-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MARTINEK	Martinek	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klaus	Klaus	k1gMnSc1
<g/>
:	:	kIx,
Chceme	chtít	k5eAaImIp1nP
statisíce	statisíce	k1gInPc4
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
od	od	k7c2
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
perou	prát	k5eAaImIp3nP
v	v	k7c6
hospodě	hospodě	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Borgis	borgis	k1gInSc1
a.s.	a.s.	k?
<g/>
,	,	kIx,
2019-06-15	2019-06-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BLÁHA	Bláha	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politik	politika	k1gFnPc2
Ing.	ing.	kA
Antonín	Antonín	k1gMnSc1
Fryč	Fryč	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
na	na	k7c4
hlidacstatu	hlidacstata	k1gFnSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlídač	hlídač	k1gInSc1
státu	stát	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Antonín	Antonín	k1gMnSc1
Fryč	Fryč	k1gMnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HOLAKOVSKÝ	HOLAKOVSKÝ	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antonín	Antonín	k1gMnSc1
Fryč	Fryč	k1gMnSc1
<g/>
:	:	kIx,
Podnikatelům	podnikatel	k1gMnPc3
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nP
vyplatit	vyplatit	k5eAaPmF
platit	platit	k5eAaImF
daně	daň	k1gFnPc4
u	u	k7c2
nás	my	k3xPp1nPc2
doma	doma	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-10-13	2017-10-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
chce	chtít	k5eAaImIp3nS
s	s	k7c7
Klausem	Klaus	k1gMnSc7
hájit	hájit	k5eAaImF
barvy	barva	k1gFnSc2
Trikolóry	trikolóra	k1gFnSc2
<g/>
:	:	kIx,
podnikatelky	podnikatelka	k1gFnSc2
<g/>
,	,	kIx,
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
právník	právník	k1gMnSc1
či	či	k8xC
tlumočnice	tlumočnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Ivana	Ivana	k1gFnSc1
Kerlesová	Kerlesový	k2eAgFnSc1d1
<g/>
,	,	kIx,
majitelka	majitelka	k1gFnSc1
komunikační	komunikační	k2eAgFnSc2d1
agentury	agentura	k1gFnSc2
z	z	k7c2
Českých	český	k2eAgInPc2d1
Budějovic	Budějovice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-11	2019-06-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HEROLDOVÁ	HEROLDOVÁ	kA
<g/>
,	,	kIx,
Martina	Martina	k1gFnSc1
<g/>
;	;	kIx,
BARTONÍČEK	Bartoníček	k1gMnSc1
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
chce	chtít	k5eAaImIp3nS
s	s	k7c7
Klausem	Klaus	k1gMnSc7
hájit	hájit	k5eAaImF
barvy	barva	k1gFnSc2
Trikolóry	trikolóra	k1gFnSc2
<g/>
:	:	kIx,
podnikatelky	podnikatelka	k1gFnSc2
<g/>
,	,	kIx,
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
právník	právník	k1gMnSc1
či	či	k8xC
tlumočnice	tlumočnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Tomáš	Tomáš	k1gMnSc1
Grygar	Grygar	k1gMnSc1
<g/>
,	,	kIx,
právník	právník	k1gMnSc1
z	z	k7c2
Přerova	Přerov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-11	2019-06-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
KAŠPÁREK	Kašpárek	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
(	(	kIx(
<g/>
jk	jk	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klausova	Klausův	k2eAgFnSc1d1
Trikolóra	trikolóra	k1gFnSc1
<g/>
:	:	kIx,
Za	za	k7c7
„	„	k?
<g/>
normální	normální	k2eAgInSc4d1
svět	svět	k1gInSc4
<g/>
“	“	k?
bez	bez	k7c2
ochrany	ochrana	k1gFnSc2
klimatu	klima	k1gNnSc2
a	a	k8xC
sňatků	sňatek	k1gInPc2
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
Referendum	referendum	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-10	2019-06-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
chce	chtít	k5eAaImIp3nS
s	s	k7c7
Klausem	Klaus	k1gMnSc7
hájit	hájit	k5eAaImF
barvy	barva	k1gFnSc2
Trikolóry	trikolóra	k1gFnSc2
<g/>
:	:	kIx,
podnikatelky	podnikatelka	k1gFnSc2
<g/>
,	,	kIx,
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
právník	právník	k1gMnSc1
či	či	k8xC
tlumočnice	tlumočnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Jana	Jana	k1gFnSc1
Marková	Marková	k1gFnSc1
<g/>
,	,	kIx,
podnikatelka	podnikatelka	k1gFnSc1
z	z	k7c2
Mladé	mladá	k1gFnSc2
Boleslavi	Boleslaev	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
,	,	kIx,
2019-06-11	2019-06-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MACEK	Macek	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
voleb	volba	k1gFnPc2
jde	jít	k5eAaImIp3nS
fotbalista	fotbalista	k1gMnSc1
<g/>
,	,	kIx,
psychiatr	psychiatr	k1gMnSc1
i	i	k8xC
skladnice	skladnice	k1gFnSc1
<g/>
.	.	kIx.
boleslavsky	boleslavsky	k6eAd1
<g/>
.	.	kIx.
<g/>
denik	denik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
chce	chtít	k5eAaImIp3nS
s	s	k7c7
Klausem	Klaus	k1gMnSc7
hájit	hájit	k5eAaImF
barvy	barva	k1gFnSc2
Trikolóry	trikolóra	k1gFnSc2
<g/>
:	:	kIx,
podnikatelky	podnikatelka	k1gFnSc2
<g/>
,	,	kIx,
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
právník	právník	k1gMnSc1
či	či	k8xC
tlumočnice	tlumočnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Natálie	Natálie	k1gFnSc1
Vachatová	Vachatový	k2eAgFnSc1d1
<g/>
,	,	kIx,
soudní	soudní	k2eAgFnSc1d1
tlumočnice	tlumočnice	k1gFnSc1
z	z	k7c2
Čelákovic	Čelákovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2019-06-11	2019-06-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Redakce	redakce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunální	komunální	k2eAgFnPc4d1
volby	volba	k1gFnPc4
2014	#num#	k4
<g/>
:	:	kIx,
Kandidátky	kandidátka	k1gFnSc2
politických	politický	k2eAgNnPc2d1
uskupení	uskupení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradecký	hradecký	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-08-07	2014-08-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
;	;	kIx,
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opakované	opakovaný	k2eAgFnPc4d1
volby	volba	k1gFnPc4
ve	v	k7c6
Strakonicích	Strakonice	k1gFnPc6
vyhráli	vyhrát	k5eAaPmAgMnP
vládci	vládce	k1gMnPc1
radnice	radnice	k1gFnSc2
<g/>
,	,	kIx,
Trikolóra	trikolóra	k1gFnSc1
padla	padnout	k5eAaPmAgFnS,k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-15-12	2019-15-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TVRDOŇ	Tvrdoň	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trikolóra	trikolóra	k1gFnSc1
se	se	k3xPyFc4
chlubí	chlubit	k5eAaImIp3nS
průzkumem	průzkum	k1gInSc7
s	s	k7c7
16	#num#	k4
procenty	procento	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Našel	najít	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
ho	on	k3xPp3gMnSc4
na	na	k7c6
Facebooku	Facebook	k1gInSc6
<g/>
,	,	kIx,
vysvětluje	vysvětlovat	k5eAaImIp3nS
Klaus	Klaus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
N	N	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-11-12	2019-11-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výsledky	výsledek	k1gInPc1
hlasování	hlasování	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgFnSc2d2
představil	představit	k5eAaPmAgInS
novou	nový	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
projevu	projev	k1gInSc6
zúčtoval	zúčtovat	k5eAaPmAgMnS
s	s	k7c7
lachtany	lachtan	k1gMnPc7
<g/>
,	,	kIx,
sexuálními	sexuální	k2eAgFnPc7d1
minoritami	minorita	k1gFnPc7
i	i	k8xC
švédskou	švédský	k2eAgFnSc7d1
aktivistkou	aktivistka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Euro	euro	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-10	2019-06-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Trikolóra	trikolóra	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trikolóra	trikolóra	k1gFnSc1
hnutí	hnutí	k1gNnSc2
občanů	občan	k1gMnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
XTV	XTV	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
han	hana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgFnSc2d2
představil	představit	k5eAaPmAgInS
své	svůj	k3xOyFgNnSc4
hnutí	hnutí	k1gNnSc4
Trikolóra	trikolóra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otec	otec	k1gMnSc1
mu	on	k3xPp3gMnSc3
garantuje	garantovat	k5eAaBmIp3nS
zahraniční	zahraniční	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2019-06-10	2019-06-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FINGERLAND	FINGERLAND	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Fingerland	Fingerland	k1gInSc1
<g/>
:	:	kIx,
Trikolóra	trikolóra	k1gFnSc1
<g/>
,	,	kIx,
spíše	spíše	k9
radikální	radikální	k2eAgInPc4d1
než	než	k8xS
konzervativní	konzervativní	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plus	plus	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-13	2019-06-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
PEČINKA	PEČINKA	kA
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pečinka	Pečinka	k1gFnSc1
<g/>
:	:	kIx,
Je	být	k5eAaImIp3nS
Klaus	Klaus	k1gMnSc1
fašista	fašista	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Trikolóra	trikolóra	k1gFnSc1
pod	pod	k7c7
palbou	palba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Info	Info	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-12	2019-06-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HORAŽĎOVSKÁ	HORAŽĎOVSKÁ	kA
<g/>
,	,	kIx,
Tereza	Tereza	k1gFnSc1
(	(	kIx(
<g/>
tho	tho	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czexit	Czexit	k1gInSc1
v	v	k7c6
programu	program	k1gInSc6
nemáme	mít	k5eNaImIp1nP
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
zakladatel	zakladatel	k1gMnSc1
hnutí	hnutí	k1gNnSc2
Trikolóra	trikolóra	k1gFnSc1
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-06-10	2019-06-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MARTINEK	Martinek	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
Právo	právo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
příštích	příští	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
budu	být	k5eAaImBp1nS
silnější	silný	k2eAgMnSc1d2
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
Klaus	Klaus	k1gMnSc1
ml.	ml.	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-04-22	2019-04-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
jr	jr	k?
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Na	na	k7c4
kampaň	kampaň	k1gFnSc4
Trikolóry	trikolóra	k1gFnSc2
chceme	chtít	k5eAaImIp1nP
jen	jen	k9
české	český	k2eAgInPc4d1
peníze	peníz	k1gInPc4
<g/>
,	,	kIx,
z	z	k7c2
ciziny	cizina	k1gFnSc2
nepřijmeme	přijmout	k5eNaPmIp1nP
ani	ani	k8xC
korunu	koruna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2019-06-20	2019-06-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HREBENAR	HREBENAR	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
čemu	co	k3yQnSc3,k3yRnSc3,k3yInSc3
je	být	k5eAaImIp3nS
Klausově	Klausův	k2eAgFnSc3d1
Trikoloře	trikolora	k1gFnSc3
transparentní	transparentní	k2eAgInSc4d1
účet	účet	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
na	na	k7c6
něm	on	k3xPp3gMnSc6
není	být	k5eNaImIp3nS
vidět	vidět	k5eAaImF
na	na	k7c4
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
byly	být	k5eAaImAgFnP
vybrané	vybraný	k2eAgInPc4d1
peníze	peníz	k1gInPc4
použity	použit	k2eAgInPc4d1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Demokratikon	Demokratikon	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2019-07-31	2019-07-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LEDERER	LEDERER	kA
<g/>
,	,	kIx,
Benedikt	Benedikt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klaus	Klaus	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
začal	začít	k5eAaPmAgMnS
vybírat	vybírat	k5eAaImF
peníze	peníz	k1gInPc4
<g/>
,	,	kIx,
během	během	k7c2
dvou	dva	k4xCgInPc2
týdnů	týden	k1gInPc2
získal	získat	k5eAaPmAgInS
přes	přes	k7c4
půl	půl	k1xP
milionu	milion	k4xCgInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
má	mít	k5eAaImIp3nS
od	od	k7c2
tří	tři	k4xCgMnPc2
dárců	dárce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-07-22	2019-07-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Internetové	internetový	k2eAgNnSc4d1
bankovnictví	bankovnictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fio	Fio	k1gFnSc1
banka	banka	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Trikolóra	trikolóra	k1gFnSc1
hnutí	hnutí	k1gNnSc2
občanů	občan	k1gMnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Podrobný	podrobný	k2eAgInSc1d1
program	program	k1gInSc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
a	a	k8xC
hnutí	hnutí	k1gNnPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
(	(	kIx(
<g/>
200	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ANO	ano	k9
2011	#num#	k4
(	(	kIx(
<g/>
78	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
63	#num#	k4
ANO	ano	k9
<g/>
,	,	kIx,
15	#num#	k4
bezpartijních	bezpartijní	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
21	#num#	k4
ODS	ODS	kA
<g/>
,	,	kIx,
2	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svoboda	Svoboda	k1gMnSc1
a	a	k8xC
přímá	přímý	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
18	#num#	k4
SPD	SPD	kA
<g/>
,	,	kIx,
1	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
13	#num#	k4
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
1	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
7	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
6	#num#	k4
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
nezařazení	nezařazení	k1gNnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
3	#num#	k4
JAP	JAP	kA
<g/>
,	,	kIx,
3	#num#	k4
Trikolóra	trikolóra	k1gFnSc1
<g/>
)	)	kIx)
Senát	senát	k1gInSc1
(	(	kIx(
<g/>
81	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ODS	ODS	kA
a	a	k8xC
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
26	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
16	#num#	k4
ODS	ODS	kA
<g/>
,	,	kIx,
4	#num#	k4
TOP	topit	k5eAaImRp2nS
09	#num#	k4
a	a	k8xC
6	#num#	k4
bezpartijní	bezpartijní	k2eAgFnSc4d1
–	–	k?
3	#num#	k4
za	za	k7c4
ODS	ODS	kA
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
SEN	sen	k1gInSc4
21	#num#	k4
a	a	k8xC
2	#num#	k4
nezávislí	závislý	k2eNgMnPc1d1
<g/>
)	)	kIx)
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
24	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
6	#num#	k4
STAN	stan	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
SLK	SLK	kA
<g/>
,	,	kIx,
1	#num#	k4
Ostravak	Ostravak	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
OPAT	opat	k1gMnSc1
<g/>
,	,	kIx,
1	#num#	k4
MHS	MHS	kA
a	a	k8xC
13	#num#	k4
bezpartijních	bezpartijní	k1gMnPc2
–	–	k?
11	#num#	k4
za	za	k7c7
STAN	stan	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
SD	SD	kA
<g/>
–	–	k?
<g/>
SN	SN	kA
a	a	k8xC
1	#num#	k4
za	za	k7c4
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
7	#num#	k4
KDU-ČSL	KDU-ČSL	k1gFnPc2
a	a	k8xC
5	#num#	k4
bezpartijní	bezpartijní	k2eAgFnSc4d1
–	–	k?
4	#num#	k4
za	za	k7c7
KDU-ČSL	KDU-ČSL	k1gFnSc7
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
NK	NK	kA
<g/>
)	)	kIx)
</s>
<s>
PROREGION	PROREGION	kA
(	(	kIx(
<g/>
9	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
2	#num#	k4
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
1	#num#	k4
ANO	ano	k9
a	a	k9
6	#num#	k4
bezpartijních	bezpartijní	k2eAgMnPc2d1
<g/>
,	,	kIx,
4	#num#	k4
za	za	k7c4
ANO	ano	k9
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
ČSSD	ČSSD	kA
a	a	k8xC
1	#num#	k4
za	za	k7c4
„	„	k?
<g/>
OSN	OSN	kA
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
SEN	sen	k1gInSc1
21	#num#	k4
a	a	k8xC
Piráti	pirát	k1gMnPc1
(	(	kIx(
<g/>
7	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
1	#num#	k4
SEN	sena	k1gFnPc2
21	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
Zelení	zeleň	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
HPP	HPP	kA
<g/>
11	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
a	a	k8xC
3	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
;	;	kIx,
1	#num#	k4
za	za	k7c4
SEN	sen	k1gInSc4
21	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
Piráty	pirát	k1gMnPc4
a	a	k8xC
1	#num#	k4
za	za	k7c4
HDK	HDK	kA
<g/>
)	)	kIx)
</s>
<s>
nezařazení	nezařazení	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
1	#num#	k4
S.	S.	kA
<g/>
cz	cz	k?
a	a	k8xC
2	#num#	k4
bezpartijní	bezpartijní	k2eAgFnSc4d1
–	–	k?
1	#num#	k4
nezávislí	závislý	k2eNgMnPc1d1
a	a	k8xC
1	#num#	k4
za	za	k7c4
Svobodní	svobodný	k2eAgMnPc1d1
<g/>
)	)	kIx)
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ANO	ano	k9
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
v	v	k7c4
RE	re	k9
</s>
<s>
ODS	ODS	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
ECR	ECR	kA
</s>
<s>
Piráti	pirát	k1gMnPc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
v	v	k7c4
Greens	Greens	k1gInSc4
<g/>
/	/	kIx~
<g/>
EFA	EFA	kA
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
STAN	stan	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
EPP	EPP	kA
</s>
<s>
SPD	SPD	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
ID	Ida	k1gFnPc2
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
EPP	EPP	kA
</s>
<s>
KSČM	KSČM	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
GUE-NGL	GUE-NGL	k1gFnSc6
Neparlamentní	parlamentní	k2eNgFnSc2d1
</s>
<s>
Alternativa	alternativa	k1gFnSc1
</s>
<s>
ANK	ANK	kA
2020	#num#	k4
</s>
<s>
APAČI	Apač	k1gMnSc3
2017	#num#	k4
</s>
<s>
ČP	ČP	kA
</s>
<s>
EU	EU	kA
TROLL	troll	k1gMnSc1
</s>
<s>
DSSS	DSSS	kA
</s>
<s>
DV	DV	kA
2016	#num#	k4
</s>
<s>
HLAS	hlas	k1gInSc1
</s>
<s>
IO	IO	kA
</s>
<s>
JsmePRO	JsmePRO	k?
<g/>
!	!	kIx.
</s>
<s>
M	M	kA
</s>
<s>
NBPLK	NBPLK	kA
</s>
<s>
Nezávislí	závislý	k2eNgMnPc1d1
</s>
<s>
NeKa	NeKa	k6eAd1
</s>
<s>
ODA	ODA	kA
</s>
<s>
HPP	HPP	kA
</s>
<s>
PV	PV	kA
</s>
<s>
PZS	PZS	kA
</s>
<s>
Rozumní	rozumný	k2eAgMnPc1d1
</s>
<s>
SNK	SNK	kA
ED	ED	kA
</s>
<s>
SNK1	SNK1	k4
</s>
<s>
SPOLEHNUTÍ	spolehnutí	k1gNnSc1
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
osobnosti	osobnost	k1gFnPc1
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
</s>
<s>
Strana	strana	k1gFnSc1
Práv	právo	k1gNnPc2
Občanů	občan	k1gMnPc2
</s>
<s>
Strana	strana	k1gFnSc1
soukromníků	soukromník	k1gMnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
T2020	T2020	k4
</s>
<s>
UFO	UFO	kA
</s>
<s>
USZ	USZ	kA
</s>
<s>
ZA	za	k7c4
OBČANY	občan	k1gMnPc4
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Z	z	k7c2
2020	#num#	k4
Seznam	seznam	k1gInSc1
neparlamentních	parlamentní	k2eNgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
a	a	k8xC
hnutí	hnutí	k1gNnSc2
není	být	k5eNaImIp3nS
úplný	úplný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
</s>
