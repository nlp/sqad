<p>
<s>
Vlkouš	vlkouš	k1gMnSc1	vlkouš
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Anarhichas	Anarhichas	k1gInSc1	Anarhichas
lupus	lupus	k1gInSc1	lupus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mořská	mořský	k2eAgFnSc1d1	mořská
ryba	ryba	k1gFnSc1	ryba
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
ostnoploutví	ostnoploutvý	k2eAgMnPc1d1	ostnoploutvý
(	(	kIx(	(
<g/>
Perciformes	Perciformes	k1gMnSc1	Perciformes
<g/>
)	)	kIx)	)
a	a	k8xC	a
čeledi	čeleď	k1gFnSc2	čeleď
vlkoušovití	vlkoušovitý	k2eAgMnPc1d1	vlkoušovitý
(	(	kIx(	(
<g/>
Anarhichadidae	Anarhichadidae	k1gNnSc7	Anarhichadidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc4	druh
popsal	popsat	k5eAaPmAgMnS	popsat
Carl	Carl	k1gMnSc1	Carl
Linné	Linné	k2eAgInSc2d1	Linné
roku	rok	k1gInSc2	rok
1758	[number]	k4	1758
<g/>
.	.	kIx.	.
</s>
<s>
Vlkouš	vlkouš	k1gMnSc1	vlkouš
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
Biskajském	biskajský	k2eAgInSc6d1	biskajský
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Barentsově	barentsově	k6eAd1	barentsově
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Islandu	Island	k1gInSc2	Island
<g/>
,	,	kIx,	,
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Grónska	Grónsko	k1gNnSc2	Grónsko
a	a	k8xC	a
přes	přes	k7c4	přes
západní	západní	k2eAgNnSc4d1	západní
pobřeží	pobřeží	k1gNnSc4	pobřeží
Atlantiku	Atlantik	k1gInSc2	Atlantik
od	od	k7c2	od
Labradoru	Labrador	k1gInSc2	Labrador
přes	přes	k7c4	přes
Newfoundland	Newfoundland	k1gInSc4	Newfoundland
až	až	k9	až
po	po	k7c6	po
Cape	capat	k5eAaImIp3nS	capat
Cod	coda	k1gFnPc2	coda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velká	velký	k2eAgFnSc1d1	velká
ryba	ryba	k1gFnSc1	ryba
měřící	měřící	k2eAgFnSc1d1	měřící
1,25	[number]	k4	1,25
až	až	k9	až
1,5	[number]	k4	1,5
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
s	s	k7c7	s
mohutnými	mohutný	k2eAgInPc7d1	mohutný
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jej	on	k3xPp3gMnSc4	on
člověk	člověk	k1gMnSc1	člověk
chytí	chytit	k5eAaPmIp3nS	chytit
<g/>
,	,	kIx,	,
vlkouš	vlkouš	k1gMnSc1	vlkouš
může	moct	k5eAaImIp3nS	moct
divoce	divoce	k6eAd1	divoce
kousat	kousat	k5eAaImF	kousat
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
černými	černý	k2eAgInPc7d1	černý
pruhy	pruh	k1gInPc7	pruh
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
znak	znak	k1gInSc1	znak
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
šedozelená	šedozelený	k2eAgFnSc1d1	šedozelená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyskytnou	vyskytnout	k5eAaPmIp3nP	vyskytnout
se	se	k3xPyFc4	se
i	i	k9	i
červenohnědí	červenohnědý	k2eAgMnPc1d1	červenohnědý
nebo	nebo	k8xC	nebo
černí	černit	k5eAaImIp3nP	černit
exempláři	exemplář	k1gInSc3	exemplář
<g/>
.	.	kIx.	.
</s>
<s>
Vlkouš	vlkouš	k1gMnSc1	vlkouš
obecný	obecný	k2eAgMnSc1d1	obecný
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
mořském	mořský	k2eAgNnSc6d1	mořské
dně	dno	k1gNnSc6	dno
ve	v	k7c6	v
skalnatých	skalnatý	k2eAgNnPc6d1	skalnaté
stanovištích	stanoviště	k1gNnPc6	stanoviště
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
na	na	k7c6	na
písčitém	písčitý	k2eAgNnSc6d1	písčité
nebo	nebo	k8xC	nebo
bahnitém	bahnitý	k2eAgNnSc6d1	bahnité
dně	dno	k1gNnSc6	dno
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
činí	činit	k5eAaImIp3nS	činit
20	[number]	k4	20
až	až	k9	až
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
vlkouše	vlkouš	k1gMnSc2	vlkouš
představují	představovat	k5eAaImIp3nP	představovat
krabi	krab	k1gMnPc1	krab
<g/>
,	,	kIx,	,
ježovky	ježovka	k1gFnPc1	ježovka
nebo	nebo	k8xC	nebo
mlži	mlž	k1gMnPc1	mlž
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
schránku	schránka	k1gFnSc4	schránka
drtí	drtit	k5eAaImIp3nS	drtit
svými	svůj	k3xOyFgFnPc7	svůj
čelistmi	čelist	k1gFnPc7	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
se	se	k3xPyFc4	se
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
klade	klást	k5eAaImIp3nS	klást
až	až	k9	až
25	[number]	k4	25
000	[number]	k4	000
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
průměr	průměr	k1gInSc4	průměr
5	[number]	k4	5
až	až	k9	až
6	[number]	k4	6
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
vylíhnutí	vylíhnutí	k1gNnSc2	vylíhnutí
jsou	být	k5eAaImIp3nP	být
vajíčka	vajíčko	k1gNnPc1	vajíčko
<g/>
,	,	kIx,	,
nakladená	nakladený	k2eAgFnSc1d1	nakladená
na	na	k7c4	na
mořské	mořský	k2eAgNnSc4d1	mořské
dno	dno	k1gNnSc4	dno
<g/>
,	,	kIx,	,
střežena	stříct	k5eAaPmNgFnS	stříct
samcem	samec	k1gMnSc7	samec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlkouš	vlkouš	k1gMnSc1	vlkouš
obecný	obecný	k2eAgMnSc1d1	obecný
je	být	k5eAaImIp3nS	být
jedlá	jedlý	k2eAgFnSc1d1	jedlá
a	a	k8xC	a
vyhledávaná	vyhledávaný	k2eAgFnSc1d1	vyhledávaná
ryba	ryba	k1gFnSc1	ryba
<g/>
,	,	kIx,	,
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
se	se	k3xPyFc4	se
na	na	k7c4	na
chlazené	chlazený	k2eAgFnPc4d1	chlazená
i	i	k8xC	i
mražené	mražený	k2eAgFnPc4d1	mražená
filety	fileta	k1gFnPc4	fileta
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nadměrnému	nadměrný	k2eAgInSc3d1	nadměrný
lovu	lov	k1gInSc3	lov
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
stává	stávat	k5eAaImIp3nS	stávat
vzácným	vzácný	k2eAgInSc7d1	vzácný
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Synonyma	synonymum	k1gNnSc2	synonymum
==	==	k?	==
</s>
</p>
<p>
<s>
Vlkouš	vlkouš	k1gMnSc1	vlkouš
severní	severní	k2eAgFnSc2d1	severní
</s>
</p>
<p>
<s>
Anarhichas	Anarhichas	k1gMnSc1	Anarhichas
strigosus	strigosus	k1gMnSc1	strigosus
<g/>
,	,	kIx,	,
Gmelin	Gmelin	k2eAgMnSc1d1	Gmelin
<g/>
,	,	kIx,	,
1789	[number]	k4	1789
</s>
</p>
<p>
<s>
Anarhichas	Anarhichas	k1gMnSc1	Anarhichas
vomerinus	vomerinus	k1gMnSc1	vomerinus
<g/>
,	,	kIx,	,
Agassiz	Agassiz	k1gMnSc1	Agassiz
<g/>
,	,	kIx,	,
1867	[number]	k4	1867
</s>
</p>
<p>
<s>
Anarhichas	Anarhichas	k1gMnSc1	Anarhichas
lupus	lupus	k1gInSc4	lupus
marisalbi	marisalbi	k1gNnSc2	marisalbi
<g/>
,	,	kIx,	,
Barsukov	Barsukov	k1gInSc1	Barsukov
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Gestreifter_Seewolf	Gestreifter_Seewolf	k1gInSc1	Gestreifter_Seewolf
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vlkouš	vlkouš	k1gMnSc1	vlkouš
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Anarhichas	Anarhichas	k1gInSc1	Anarhichas
lupus	lupus	k1gInSc1	lupus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
