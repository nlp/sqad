<s>
Vlkouš	vlkouš	k1gMnSc1	vlkouš
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Anarhichas	Anarhichas	k1gInSc1	Anarhichas
lupus	lupus	k1gInSc1	lupus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mořská	mořský	k2eAgFnSc1d1	mořská
ryba	ryba	k1gFnSc1	ryba
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
ostnoploutví	ostnoploutvý	k2eAgMnPc1d1	ostnoploutvý
(	(	kIx(	(
<g/>
Perciformes	Perciformes	k1gMnSc1	Perciformes
<g/>
)	)	kIx)	)
a	a	k8xC	a
čeledi	čeleď	k1gFnSc2	čeleď
vlkoušovití	vlkoušovitý	k2eAgMnPc1d1	vlkoušovitý
(	(	kIx(	(
<g/>
Anarhichadidae	Anarhichadidae	k1gNnSc7	Anarhichadidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
