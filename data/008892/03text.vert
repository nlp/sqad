<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
panarabských	panarabský	k2eAgFnPc2d1	panarabská
barev	barva	k1gFnPc2	barva
seřazených	seřazený	k2eAgFnPc2d1	seřazená
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
vodorovných	vodorovný	k2eAgInPc2d1	vodorovný
pruhů	pruh	k1gInPc2	pruh
–	–	k?	–
zeleného	zelený	k2eAgMnSc2d1	zelený
<g/>
,	,	kIx,	,
bílého	bílý	k2eAgInSc2d1	bílý
a	a	k8xC	a
červeného	červený	k2eAgInSc2d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žerdi	žerď	k1gFnSc2	žerď
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc4d1	černý
lichoběžník	lichoběžník	k1gInSc4	lichoběžník
<g/>
.	.	kIx.	.
<g/>
Symboliku	symbolika	k1gFnSc4	symbolika
barev	barva	k1gFnPc2	barva
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
oficiální	oficiální	k2eAgInSc4d1	oficiální
výklad	výklad	k1gInSc4	výklad
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Bílé	bílý	k2eAgInPc1d1	bílý
jsou	být	k5eAaImIp3nP	být
naše	náš	k3xOp1gInPc1	náš
činy	čin	k1gInPc1	čin
<g/>
,	,	kIx,	,
černé	černý	k2eAgInPc1d1	černý
jsou	být	k5eAaImIp3nP	být
naše	náš	k3xOp1gInPc1	náš
boje	boj	k1gInPc1	boj
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
jsou	být	k5eAaImIp3nP	být
naše	náš	k3xOp1gFnPc1	náš
louky	louka	k1gFnPc1	louka
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
napojená	napojený	k2eAgFnSc1d1	napojená
krví	krev	k1gFnSc7	krev
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
naše	náš	k3xOp1gFnSc1	náš
budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
také	také	k9	také
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
bílá	bílý	k2eAgFnSc1d1	bílá
znamená	znamenat	k5eAaImIp3nS	znamenat
ctnost	ctnost	k1gFnSc1	ctnost
<g/>
,	,	kIx,	,
černá	černat	k5eAaImIp3nS	černat
písek	písek	k1gInSc1	písek
zvířený	zvířený	k2eAgInSc1d1	zvířený
kuvajtskými	kuvajtský	k2eAgInPc7d1	kuvajtský
jezdci	jezdec	k1gInPc7	jezdec
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgNnPc1d1	zelené
pole	pole	k1gNnPc1	pole
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
statečnost	statečnost	k1gFnSc1	statečnost
<g/>
.	.	kIx.	.
</s>
<s>
Výklad	výklad	k1gInSc1	výklad
barev	barva	k1gFnPc2	barva
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
básně	báseň	k1gFnSc2	báseň
od	od	k7c2	od
Safie	Safie	k1gFnSc2	Safie
Al-Deen	Al-Deen	k2eAgMnSc1d1	Al-Deen
Al-Hali	Al-Hali	k1gMnSc1	Al-Hali
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kuvajtský	kuvajtský	k2eAgInSc1d1	kuvajtský
znak	znak	k1gInSc1	znak
</s>
</p>
<p>
<s>
Kuvajtská	kuvajtský	k2eAgFnSc1d1	kuvajtská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
