<s>
Tankový	tankový	k2eAgInSc1d1	tankový
prapor	prapor	k1gInSc1	prapor
je	být	k5eAaImIp3nS	být
československý	československý	k2eAgInSc1d1	československý
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Víta	Vít	k1gMnSc2	Vít
Olmera	Olmer	k1gMnSc2	Olmer
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
natočený	natočený	k2eAgInSc1d1	natočený
ve	v	k7c6	v
filmovém	filmový	k2eAgNnSc6d1	filmové
studiu	studio	k1gNnSc6	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
společností	společnost	k1gFnPc2	společnost
Bonton	bonton	k1gInSc1	bonton
a.s.	a.s.	k?	a.s.
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
literární	literární	k2eAgFnSc2d1	literární
předlohy	předloha	k1gFnSc2	předloha
Josefa	Josef	k1gMnSc2	Josef
Škvoreckého	Škvorecký	k2eAgMnSc2d1	Škvorecký
<g/>
.	.	kIx.	.
</s>
