<s>
Tankový	tankový	k2eAgInSc1d1	tankový
prapor	prapor	k1gInSc1	prapor
je	být	k5eAaImIp3nS	být
československý	československý	k2eAgInSc1d1	československý
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Víta	Vít	k1gMnSc2	Vít
Olmera	Olmer	k1gMnSc2	Olmer
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
natočený	natočený	k2eAgInSc1d1	natočený
ve	v	k7c6	v
filmovém	filmový	k2eAgNnSc6d1	filmové
studiu	studio	k1gNnSc6	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
společností	společnost	k1gFnPc2	společnost
Bonton	bonton	k1gInSc1	bonton
a.s.	a.s.	k?	a.s.
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
literární	literární	k2eAgFnSc2d1	literární
předlohy	předloha	k1gFnSc2	předloha
Josefa	Josef	k1gMnSc2	Josef
Škvoreckého	Škvorecký	k2eAgMnSc2d1	Škvorecký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Lukáš	Lukáš	k1gMnSc1	Lukáš
Vaculík	Vaculík	k1gMnSc1	Vaculík
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
diváků	divák	k1gMnPc2	divák
a	a	k8xC	a
tržby	tržba	k1gFnSc2	tržba
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
55	[number]	k4	55
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Lukáš	Lukáš	k1gMnSc1	Lukáš
Vaculík	Vaculík	k1gMnSc1	Vaculík
–	–	k?	–
rotný	rotný	k1gMnSc1	rotný
Daniel	Daniel	k1gMnSc1	Daniel
"	"	kIx"	"
<g/>
Danny	Dann	k1gInPc1	Dann
<g/>
"	"	kIx"	"
Smiřický	smiřický	k2eAgMnSc1d1	smiřický
Roman	Roman	k1gMnSc1	Roman
Skamene	Skamen	k1gInSc5	Skamen
–	–	k?	–
major	major	k1gMnSc1	major
Borovička	Borovička	k1gMnSc1	Borovička
Simona	Simona	k1gFnSc1	Simona
Chytrová	Chytrová	k1gFnSc1	Chytrová
–	–	k?	–
Janinka	Janinka	k1gFnSc1	Janinka
Pinkasová	Pinkasová	k1gFnSc1	Pinkasová
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Jandák	Jandák	k1gMnSc1	Jandák
–	–	k?	–
kapitán	kapitán	k1gMnSc1	kapitán
Matka	matka	k1gFnSc1	matka
Miroslav	Miroslav	k1gMnSc1	Miroslav
Donutil	donutit	k5eAaPmAgMnS	donutit
–	–	k?	–
nadporučík	nadporučík	k1gMnSc1	nadporučík
Růžička	Růžička	k1gMnSc1	Růžička
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Zavřel	Zavřel	k1gMnSc1	Zavřel
–	–	k?	–
podporučík	podporučík	k1gMnSc1	podporučík
Malina	Malina	k1gMnSc1	Malina
Martin	Martin	k1gMnSc1	Martin
Zounar	Zounar	k1gMnSc1	Zounar
–	–	k?	–
poručík	poručík	k1gMnSc1	poručík
Prouza	Prouz	k1gMnSc2	Prouz
Václav	Václav	k1gMnSc1	Václav
Vydra	Vydra	k1gMnSc1	Vydra
–	–	k?	–
svobodník	svobodník	k1gMnSc1	svobodník
Mlejnek	Mlejnek	k1gMnSc1	Mlejnek
Michal	Michal	k1gMnSc1	Michal
Suchánek	Suchánek	k1gMnSc1	Suchánek
–	–	k?	–
vojín	vojín	k1gMnSc1	vojín
Mengele	Mengel	k1gInSc2	Mengel
Milan	Milan	k1gMnSc1	Milan
Šimáček	Šimáček	k1gMnSc1	Šimáček
–	–	k?	–
rotný	rotný	k1gMnSc1	rotný
Soudek	soudek	k1gInSc1	soudek
Miroslav	Miroslav	k1gMnSc1	Miroslav
Táborský	Táborský	k1gMnSc1	Táborský
–	–	k?	–
desátník	desátník	k1gMnSc1	desátník
Plíhal	Plíhal	k1gMnSc1	Plíhal
Martina	Martina	k1gFnSc1	Martina
Adamcová	Adamcová	k1gFnSc1	Adamcová
–	–	k?	–
četařka	četařka	k1gFnSc1	četařka
Babinčáková	Babinčáková	k1gFnSc1	Babinčáková
a	a	k8xC	a
další	další	k2eAgFnSc7d1	další
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
doktor	doktor	k1gMnSc1	doktor
filosofie	filosofie	k1gFnSc2	filosofie
Danny	Danna	k1gFnSc2	Danna
Smiřický	smiřický	k2eAgMnSc1d1	smiřický
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
povinné	povinný	k2eAgFnSc2d1	povinná
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
přidělen	přidělit	k5eAaPmNgInS	přidělit
k	k	k7c3	k
tankovému	tankový	k2eAgInSc3d1	tankový
praporu	prapor	k1gInSc3	prapor
ve	v	k7c6	v
vojenském	vojenský	k2eAgInSc6d1	vojenský
prostoru	prostor	k1gInSc6	prostor
Kobylec	Kobylec	k1gInSc1	Kobylec
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
velitel	velitel	k1gMnSc1	velitel
tanku	tank	k1gInSc2	tank
T-	T-	k1gFnSc2	T-
<g/>
34	[number]	k4	34
<g/>
/	/	kIx~	/
<g/>
85	[number]	k4	85
prodělává	prodělávat	k5eAaImIp3nS	prodělávat
cvičení	cvičení	k1gNnSc4	cvičení
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgMnSc1d1	kulturní
pracovník	pracovník	k1gMnSc1	pracovník
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
plnění	plnění	k1gNnSc4	plnění
Fučíkova	Fučíkův	k2eAgInSc2d1	Fučíkův
odznaku	odznak	k1gInSc2	odznak
a	a	k8xC	a
zažívá	zažívat	k5eAaImIp3nS	zažívat
další	další	k2eAgFnPc4d1	další
příhody	příhoda	k1gFnPc4	příhoda
z	z	k7c2	z
vojenského	vojenský	k2eAgNnSc2d1	vojenské
prostředí	prostředí	k1gNnSc2	prostředí
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volných	volný	k2eAgFnPc6d1	volná
chvílích	chvíle	k1gFnPc6	chvíle
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
vdanou	vdaný	k2eAgFnSc4d1	vdaná
Lizetku	Lizetka	k1gFnSc4	Lizetka
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
civilním	civilní	k2eAgInSc6d1	civilní
životě	život	k1gInSc6	život
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
navázat	navázat	k5eAaPmF	navázat
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kasárnách	kasárny	k1gFnPc6	kasárny
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
ženy	žena	k1gFnSc2	žena
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
důstojníků	důstojník	k1gMnPc2	důstojník
Jany	Jana	k1gFnSc2	Jana
Pinkasové	Pinkasová	k1gFnSc2	Pinkasová
<g/>
.	.	kIx.	.
</s>
<s>
Zažije	zažít	k5eAaPmIp3nS	zažít
návštěvu	návštěva	k1gFnSc4	návštěva
sovětského	sovětský	k2eAgMnSc2d1	sovětský
vojenského	vojenský	k2eAgMnSc2d1	vojenský
poradce	poradce	k1gMnSc2	poradce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
svědkem	svědek	k1gMnSc7	svědek
utonutí	utonutí	k1gNnSc2	utonutí
v	v	k7c6	v
jímce	jímka	k1gFnSc6	jímka
velitele	velitel	k1gMnSc2	velitel
tábora	tábor	k1gInSc2	tábor
majora	major	k1gMnSc2	major
Borovičky	Borovička	k1gMnSc2	Borovička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konci	konec	k1gInSc6	konec
příběhu	příběh	k1gInSc2	příběh
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
civilu	civil	k1gMnSc3	civil
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
vojenském	vojenský	k2eAgInSc6d1	vojenský
prostoru	prostor	k1gInSc6	prostor
v	v	k7c6	v
Podbořanech	Podbořany	k1gInPc6	Podbořany
<g/>
,	,	kIx,	,
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
U	u	k7c2	u
Kasáren	kasárny	k1gFnPc2	kasárny
a	a	k8xC	a
Záhořanská	Záhořanský	k2eAgFnSc1d1	Záhořanský
v	v	k7c6	v
Berouně	Beroun	k1gInSc6	Beroun
<g/>
,	,	kIx,	,
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Valov	Valov	k1gInSc1	Valov
a	a	k8xC	a
na	na	k7c6	na
nádraží	nádraží	k1gNnSc6	nádraží
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
pod	pod	k7c7	pod
Pleší	pleš	k1gFnSc7	pleš
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
děj	děj	k1gInSc1	děj
románu	román	k1gInSc2	román
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
scenáristé	scenárista	k1gMnPc1	scenárista
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
celý	celý	k2eAgInSc4d1	celý
děj	děj	k1gInSc4	děj
filmu	film	k1gInSc2	film
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Danny	Danen	k2eAgInPc1d1	Danen
Smiřický	smiřický	k2eAgMnSc1d1	smiřický
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
románu	román	k1gInSc6	román
velitelem	velitel	k1gMnSc7	velitel
tanku	tank	k1gInSc2	tank
T-	T-	k1gFnSc1	T-
<g/>
34	[number]	k4	34
<g/>
/	/	kIx~	/
<g/>
85	[number]	k4	85
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
pětičlennou	pětičlenný	k2eAgFnSc4d1	pětičlenná
posádku	posádka	k1gFnSc4	posádka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
je	být	k5eAaImIp3nS	být
i	i	k9	i
vysvětleno	vysvětlen	k2eAgNnSc1d1	vysvětleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
pátý	pátý	k4xOgMnSc1	pátý
člen	člen	k1gMnSc1	člen
je	být	k5eAaImIp3nS	být
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
nemocný	nemocný	k1gMnSc1	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
posádky	posádka	k1gFnPc4	posádka
všech	všecek	k3xTgInPc2	všecek
tanků	tank	k1gInPc2	tank
pouze	pouze	k6eAd1	pouze
čtyřčlenné	čtyřčlenný	k2eAgNnSc1d1	čtyřčlenné
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
T-	T-	k1gFnSc2	T-
<g/>
34	[number]	k4	34
<g/>
/	/	kIx~	/
<g/>
85	[number]	k4	85
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tankový	tankový	k2eAgInSc1d1	tankový
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
http://www.extra.cz/tankovy-prapor-vas-vzdy-pobavi-7-veci-ktere-jste-o-legendarnim-filmu-nevedeli	[url]	k5eAaBmAgMnP	http://www.extra.cz/tankovy-prapor-vas-vzdy-pobavi-7-veci-ktere-jste-o-legendarnim-filmu-nevedeli
</s>
