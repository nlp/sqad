<s>
Placka	placka	k1gFnSc1
(	(	kIx(
<g/>
odznak	odznak	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Placka	placka	k1gFnSc1
jako	jako	k8xS,k8xC
špendlík	špendlík	k1gInSc1
nebo	nebo	k8xC
knoflík	knoflík	k1gInSc1
(	(	kIx(
<g/>
americký	americký	k2eAgInSc1d1
patent	patent	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1896	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dvě	dva	k4xCgFnPc1
sestavené	sestavený	k2eAgFnPc1d1
placky	placka	k1gFnPc1
(	(	kIx(
<g/>
nahoře	nahoře	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
rozebrané	rozebraný	k2eAgFnPc1d1
(	(	kIx(
<g/>
dole	dole	k6eAd1
<g/>
)	)	kIx)
s	s	k7c7
dvojím	dvojí	k4xRgInSc7
drátěným	drátěný	k2eAgInSc7d1
špendlíkem	špendlík	k1gInSc7
</s>
<s>
Placka	placka	k1gFnSc1
z	z	k7c2
prosince	prosinec	k1gInSc2
1989	#num#	k4
<g/>
,	,	kIx,
asi	asi	k9
první	první	k4xOgMnPc1
v	v	k7c6
ČSR	ČSR	kA
</s>
<s>
Reklamní	reklamní	k2eAgFnSc1d1
placka	placka	k1gFnSc1
nebo	nebo	k8xC
placka	placka	k1gFnSc1
se	s	k7c7
spínacím	spínací	k2eAgInSc7d1
špendlíkem	špendlík	k1gInSc7
<g/>
,	,	kIx,
slangově	slangově	k6eAd1
"	"	kIx"
<g/>
placka	placka	k1gFnSc1
se	s	k7c7
sichrhajckou	sichrhajcka	k1gFnSc7
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
předmět	předmět	k1gInSc4
kruhového	kruhový	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
(	(	kIx(
<g/>
jsou	být	k5eAaImIp3nP
i	i	k9
atypické	atypický	k2eAgInPc1d1
tvary	tvar	k1gInPc1
jako	jako	k8xC,k8xS
čtverce	čtverec	k1gInPc1
<g/>
,	,	kIx,
obdélníky	obdélník	k1gInPc1
<g/>
,	,	kIx,
ovály	ovál	k1gInPc1
<g/>
,	,	kIx,
srdíčka	srdíčko	k1gNnPc1
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
<g/>
)	)	kIx)
obvykle	obvykle	k6eAd1
kovový	kovový	k2eAgInSc1d1
nebo	nebo	k8xC
plastový	plastový	k2eAgInSc1d1
<g/>
,	,	kIx,
ze	z	k7c2
zadní	zadní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
opatřený	opatřený	k2eAgInSc1d1
svíracím	svírací	k2eAgInSc7d1
špendlíkem	špendlík	k1gInSc7
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
ji	on	k3xPp3gFnSc4
lze	lze	k6eAd1
připevnit	připevnit	k5eAaPmF
na	na	k7c4
oděv	oděv	k1gInSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
tašku	taška	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jeden	jeden	k4xCgInSc1
z	z	k7c2
předmětů	předmět	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
sběratelskou	sběratelský	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
Čechách	Čechy	k1gFnPc6
však	však	k9
sběratelská	sběratelský	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
prozatím	prozatím	k6eAd1
není	být	k5eNaImIp3nS
příliš	příliš	k6eAd1
aktivní	aktivní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
je	být	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
režim	režim	k1gInSc1
před	před	k7c7
rokem	rok	k1gInSc7
1989	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
tyto	tento	k3xDgInPc4
předměty	předmět	k1gInPc4
<g/>
,	,	kIx,
výrobou	výroba	k1gFnSc7
zejména	zejména	k9
z	z	k7c2
USA	USA	kA
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
dovážet	dovážet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnPc1
klasické	klasický	k2eAgFnPc1d1
kovové	kovový	k2eAgFnPc1d1
placky	placka	k1gFnPc1
se	se	k3xPyFc4
špendlíkem	špendlík	k1gInSc7
byly	být	k5eAaImAgInP
vyrobeny	vyroben	k2eAgInPc1d1
v	v	k7c6
USA	USA	kA
společností	společnost	k1gFnPc2
Whitehead	Whitehead	k1gInSc4
&	&	k?
Hoag	Hoag	k1gInSc1
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1896	#num#	k4
v	v	k7c6
New	New	k1gFnSc6
Jersey	Jersea	k1gFnSc2
<g/>
,	,	kIx,
původně	původně	k6eAd1
jako	jako	k9
levná	levný	k2eAgFnSc1d1
alternativa	alternativa	k1gFnSc1
k	k	k7c3
medailonkům	medailonek	k1gInPc3
<g/>
,	,	kIx,
přívěškům	přívěšek	k1gInPc3
a	a	k8xC
odznakům	odznak	k1gInPc3
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc4
výroba	výroba	k1gFnSc1
byla	být	k5eAaImAgFnS
nákladnější	nákladný	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
začínala	začínat	k5eAaImAgFnS
výrobu	výroba	k1gFnSc4
s	s	k7c7
nášivkami	nášivka	k1gFnPc7
přišívanými	přišívaný	k2eAgFnPc7d1
na	na	k7c6
bundy	bunda	k1gFnSc2
<g/>
,	,	kIx,
především	především	k9
u	u	k7c2
armády	armáda	k1gFnSc2
k	k	k7c3
určení	určení	k1gNnSc3
hodnosti	hodnost	k1gFnSc2
a	a	k8xC
identifikaci	identifikace	k1gFnSc4
personálu	personál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plechová	plechový	k2eAgFnSc1d1
obroučka	obroučka	k1gFnSc1
(	(	kIx(
<g/>
límec	límec	k1gInSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgNnP
nasazena	nasadit	k5eAaPmNgNnP
zezadu	zezadu	k6eAd1
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
držela	držet	k5eAaImAgFnS
výstřižek	výstřižek	k1gInSc4
látky	látka	k1gFnSc2
pevně	pevně	k6eAd1
napnutý	napnutý	k2eAgMnSc1d1
přes	přes	k7c4
přední	přední	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
odznaku	odznak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skautské	skautský	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
byly	být	k5eAaImAgFnP
z	z	k7c2
odznaků	odznak	k1gInPc2
tak	tak	k6eAd1
nadšené	nadšený	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
je	on	k3xPp3gInPc4
začaly	začít	k5eAaPmAgInP
používat	používat	k5eAaImF
pro	pro	k7c4
své	svůj	k3xOyFgMnPc4
členy	člen	k1gMnPc4
k	k	k7c3
označení	označení	k1gNnSc3
hodnosti	hodnost	k1gFnSc2
a	a	k8xC
příslušnosti	příslušnost	k1gFnSc2
k	k	k7c3
oddílu	oddíl	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
let	léto	k1gNnPc2
se	se	k3xPyFc4
dále	daleko	k6eAd2
objevily	objevit	k5eAaPmAgInP
různé	různý	k2eAgInPc1d1
typy	typ	k1gInPc1
odznaků	odznak	k1gInPc2
a	a	k8xC
staly	stát	k5eAaPmAgFnP
se	se	k3xPyFc4
velmi	velmi	k6eAd1
žádaným	žádaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
u	u	k7c2
sběratelů	sběratel	k1gMnPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
poštovní	poštovní	k2eAgFnSc2d1
známky	známka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgInPc1d1
placky	placek	k1gInPc1
o	o	k7c6
průměru	průměr	k1gInSc6
zhruba	zhruba	k6eAd1
25	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
v	v	k7c6
podstatě	podstata	k1gFnSc6
trochu	trochu	k6eAd1
vylepšený	vylepšený	k2eAgInSc4d1
oděvní	oděvní	k2eAgInSc4d1
knoflík	knoflík	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
se	se	k3xPyFc4
vyráběly	vyrábět	k5eAaImAgFnP
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
dodnes	dodnes	k6eAd1
v	v	k7c6
anglicky	anglicky	k6eAd1
mluvících	mluvící	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
nazývají	nazývat	k5eAaImIp3nP
button-badges	button-badges	k1gMnSc1
(	(	kIx(
<g/>
knoflíkové	knoflíkový	k2eAgInPc4d1
odznaky	odznak	k1gInPc4
<g/>
)	)	kIx)
nebo	nebo	k8xC
pins	pins	k6eAd1
(	(	kIx(
<g/>
špendlíky	špendlík	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
USA	USA	kA
jsou	být	k5eAaImIp3nP
jednoduše	jednoduše	k6eAd1
nazývány	nazývat	k5eAaImNgFnP
buttons	buttons	k6eAd1
(	(	kIx(
<g/>
knoflíky	knoflík	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1869	#num#	k4
John	John	k1gMnSc1
Wesley	Weslea	k1gMnSc2
Hyatt	Hyatt	k1gMnSc1
vynalezl	vynaleznout	k5eAaPmAgMnS
celuloid	celuloid	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
odolný	odolný	k2eAgInSc1d1
termoplastický	termoplastický	k2eAgInSc1d1
materiál	materiál	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
dost	dost	k6eAd1
pevný	pevný	k2eAgInSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
dal	dát	k5eAaPmAgInS
mechanicky	mechanicky	k6eAd1
vyrážet	vyrážet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1896	#num#	k4
společnost	společnost	k1gFnSc1
Whitehead	Whitehead	k1gInSc1
&	&	k?
Hoag	Hoag	k1gInSc1
vylepšila	vylepšit	k5eAaPmAgFnS
své	svůj	k3xOyFgInPc4
látkové	látkový	k2eAgInPc4d1
odznaky	odznak	k1gInPc4
výměnou	výměna	k1gFnSc7
látky	látka	k1gFnSc2
za	za	k7c4
celuloid	celuloid	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
vznikly	vzniknout	k5eAaPmAgFnP
první	první	k4xOgFnPc1
placky	placka	k1gFnPc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	on	k3xPp3gMnPc4
známe	znát	k5eAaImIp1nP
dnes	dnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tenká	tenký	k2eAgFnSc1d1
celuloidová	celuloidový	k2eAgFnSc1d1
fólie	fólie	k1gFnSc1
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
použita	použít	k5eAaPmNgFnS
k	k	k7c3
pokrytí	pokrytí	k1gNnSc3
papíru	papír	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
získání	získání	k1gNnSc1
matného	matný	k2eAgInSc2d1
vzhledu	vzhled	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
to	ten	k3xDgNnSc1
bez	bez	k7c2
zvýšení	zvýšení	k1gNnSc2
nákladů	náklad	k1gInPc2
a	a	k8xC
požadavků	požadavek	k1gInPc2
na	na	k7c4
pracovní	pracovní	k2eAgFnPc4d1
dovednosti	dovednost	k1gFnPc4
personálu	personál	k1gInSc2
<g/>
,	,	kIx,
potřebných	potřebný	k2eAgInPc2d1
při	při	k7c6
práci	práce	k1gFnSc6
s	s	k7c7
papírem	papír	k1gInSc7
či	či	k8xC
celuloidem	celuloid	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
to	ten	k3xDgNnSc1
znamenalo	znamenat	k5eAaImAgNnS
menší	malý	k2eAgFnSc4d2
spotřebu	spotřeba	k1gFnSc4
kovu	kov	k1gInSc2
při	při	k7c6
výrobě	výroba	k1gFnSc6
placek	placka	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
dále	daleko	k6eAd2
zde	zde	k6eAd1
již	již	k6eAd1
nebyla	být	k5eNaImAgFnS
potřeba	potřeba	k1gFnSc1
pájení	pájení	k1gNnSc2
nebo	nebo	k8xC
šroubování	šroubování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednoduchý	jednoduchý	k2eAgInSc1d1
mechanický	mechanický	k2eAgInSc1d1
lis	lis	k1gInSc1
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
ke	k	k7c3
stlačení	stlačení	k1gNnSc3
plechu	plech	k1gInSc2
<g/>
,	,	kIx,
papíru	papír	k1gInSc2
a	a	k8xC
celuloidu	celuloid	k1gInSc2
dohromady	dohromady	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kovový	kovový	k2eAgInSc1d1
kruh	kruh	k1gInSc1
byl	být	k5eAaImAgInS
poté	poté	k6eAd1
připevněn	připevnit	k5eAaPmNgInS
k	k	k7c3
zadní	zadní	k2eAgFnSc3d1
části	část	k1gFnSc3
placky	placka	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ji	on	k3xPp3gFnSc4
držel	držet	k5eAaImAgMnS
pohromadě	pohromadě	k6eAd1
<g/>
,	,	kIx,
opět	opět	k6eAd1
jednoduchým	jednoduchý	k2eAgNnSc7d1
vlisováním	vlisování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
byl	být	k5eAaImAgInS
na	na	k7c4
zadní	zadní	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
placky	placka	k1gFnSc2
připevněn	připevněn	k2eAgInSc4d1
spínací	spínací	k2eAgInSc4d1
špendlík	špendlík	k1gInSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
připíchnuta	připíchnout	k5eAaPmNgFnS
na	na	k7c4
oděv	oděv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Dnešní	dnešní	k2eAgMnPc1d1
výrobci	výrobce	k1gMnPc1
vyrábějí	vyrábět	k5eAaImIp3nP
placky	placek	k1gInPc4
stejným	stejný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
z	z	k7c2
lehce	lehko	k6eAd1
odlišných	odlišný	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
a	a	k8xC
s	s	k7c7
bezpečnějším	bezpečný	k2eAgNnSc7d2
uzavíráním	uzavírání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předtím	předtím	k6eAd1
byly	být	k5eAaImAgInP
kovové	kovový	k2eAgInPc1d1
smaltované	smaltovaný	k2eAgInPc1d1
placky	placek	k1gInPc1
jediným	jediný	k2eAgInSc7d1
dostupným	dostupný	k2eAgInSc7d1
druhem	druh	k1gInSc7
placek	placka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smaltované	smaltovaný	k2eAgInPc1d1
odznaky	odznak	k1gInPc1
připevňované	připevňovaný	k2eAgInPc1d1
na	na	k7c4
klopu	klopa	k1gFnSc4
kabátu	kabát	k1gInSc2
byly	být	k5eAaImAgInP
extrémně	extrémně	k6eAd1
populární	populární	k2eAgInPc1d1
<g/>
,	,	kIx,
jakkoliv	jakkoliv	k6eAd1
byly	být	k5eAaImAgFnP
drahé	drahý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
druh	druh	k1gInSc1
placek	placka	k1gFnPc2
byl	být	k5eAaImAgInS
docela	docela	k6eAd1
populární	populární	k2eAgInSc1d1
druh	druh	k1gInSc1
reklamního	reklamní	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
bylo	být	k5eAaImAgNnS
v	v	k7c6
průběhu	průběh	k1gInSc6
prvního	první	k4xOgInSc2
roku	rok	k1gInSc2
vyrobeno	vyroben	k2eAgNnSc1d1
přes	přes	k7c4
milion	milion	k4xCgInSc4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zpočátku	zpočátku	k6eAd1
byly	být	k5eAaImAgFnP
placky	placka	k1gFnPc1
používány	používat	k5eAaImNgFnP
k	k	k7c3
propagaci	propagace	k1gFnSc3
sportovců	sportovec	k1gMnPc2
<g/>
,	,	kIx,
herců	herec	k1gMnPc2
a	a	k8xC
politiků	politik	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dávaly	dávat	k5eAaImAgInP
se	se	k3xPyFc4
jako	jako	k8xC,k8xS
reklamní	reklamní	k2eAgInSc1d1
dárek	dárek	k1gInSc1
při	při	k7c6
koupi	koupě	k1gFnSc6
krabičky	krabička	k1gFnSc2
cigaret	cigareta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
byly	být	k5eAaImAgInP
rozdávány	rozdávat	k5eAaImNgInP
v	v	k7c6
Anglii	Anglie	k1gFnSc6
během	během	k7c2
Búrské	búrský	k2eAgFnSc2d1
války	válka	k1gFnSc2
k	k	k7c3
propagaci	propagace	k1gFnSc3
politických	politický	k2eAgNnPc2d1
sdělení	sdělení	k1gNnPc2
jako	jako	k8xS,k8xC
‘	‘	k?
<g/>
Anglie	Anglie	k1gFnSc1
očekává	očekávat	k5eAaImIp3nS
od	od	k7c2
každého	každý	k3xTgMnSc2
muže	muž	k1gMnSc2
<g/>
,	,	kIx,
že	že	k8xS
splní	splnit	k5eAaPmIp3nP
svou	svůj	k3xOyFgFnSc4
povinnost	povinnost	k1gFnSc4
<g/>
‘	‘	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedny	jeden	k4xCgFnPc1
z	z	k7c2
prvních	první	k4xOgFnPc2
placek	placka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
vytvořeny	vytvořit	k5eAaPmNgFnP
k	k	k7c3
oslavám	oslava	k1gFnPc3
diamantového	diamantový	k2eAgNnSc2d1
jubilea	jubileum	k1gNnSc2
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1897	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
je	on	k3xPp3gNnSc4
levně	levně	k6eAd1
vyrobit	vyrobit	k5eAaPmF
<g/>
,	,	kIx,
levně	levně	k6eAd1
koupit	koupit	k5eAaPmF
a	a	k8xC
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
vynikající	vynikající	k2eAgInSc1d1
suvenýr	suvenýr	k1gInSc1
pro	pro	k7c4
takto	takto	k6eAd1
velkou	velký	k2eAgFnSc4d1
událost	událost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1
popularita	popularita	k1gFnSc1
placek	placka	k1gFnPc2
se	se	k3xPyFc4
datuje	datovat	k5eAaImIp3nS
zpětně	zpětně	k6eAd1
k	k	k7c3
šedesátým	šedesátý	k4xOgInSc7
a	a	k8xC
sedmdesátým	sedmdesátý	k4xOgInSc7
létům	léto	k1gNnPc3
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
používány	používán	k2eAgMnPc4d1
studenty	student	k1gMnPc4
<g/>
,	,	kIx,
hippies	hippies	k1gInSc4
a	a	k8xC
hudebníky	hudebník	k1gMnPc4
jako	jako	k8xC,k8xS
symbol	symbol	k1gInSc4
protestu	protest	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
Lennon	Lennon	k1gMnSc1
placky	placka	k1gFnSc2
miloval	milovat	k5eAaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
je	být	k5eAaImIp3nS
prostě	prostě	k9
cool	cool	k1gInSc1
nosit	nosit	k5eAaImF
placky	placka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Ale	ale	k9
teprve	teprve	k6eAd1
příchod	příchod	k1gInSc1
Sex	sex	k1gInSc1
Pistols	Pistolsa	k1gFnPc2
a	a	k8xC
punku	punk	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
udělal	udělat	k5eAaPmAgMnS
z	z	k7c2
placek	placka	k1gFnPc2
nezbytnou	zbytný	k2eNgFnSc4d1,k2eAgFnSc4d1
součást	součást	k1gFnSc4
módního	módní	k2eAgNnSc2d1
sebevyjádření	sebevyjádření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
další	další	k2eAgFnSc6d1
dekádě	dekáda	k1gFnSc6
lidé	člověk	k1gMnPc1
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
projevovali	projevovat	k5eAaImAgMnP
svou	svůj	k3xOyFgFnSc4
náklonnost	náklonnost	k1gFnSc4
kapelám	kapela	k1gFnPc3
<g/>
,	,	kIx,
hudbě	hudba	k1gFnSc3
<g/>
,	,	kIx,
kultu	kult	k1gInSc6
mládí	mládí	k1gNnSc2
nebo	nebo	k8xC
čemukoliv	cokoliv	k3yInSc3
jinému	jiný	k2eAgMnSc3d1
nošením	nošení	k1gNnSc7
jedné	jeden	k4xCgFnSc2
nebo	nebo	k8xC
více	hodně	k6eAd2
placek	placka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
poklesu	pokles	k1gInSc3
jejich	jejich	k3xOp3gFnSc2
popularity	popularita	k1gFnSc2
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
placky	placka	k1gFnPc1
jsou	být	k5eAaImIp3nP
stále	stále	k6eAd1
oblíbené	oblíbený	k2eAgFnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
zcela	zcela	k6eAd1
jistě	jistě	k9
jsou	být	k5eAaImIp3nP
jedním	jeden	k4xCgInSc7
z	z	k7c2
nejoblíbenějších	oblíbený	k2eAgInPc2d3
propagačních	propagační	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
na	na	k7c6
trhu	trh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
placka	placka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
The	The	k?
Busy	bus	k1gInPc1
Beaver	Beaver	k1gInSc4
Button	Button	k1gInSc1
Museum	museum	k1gNnSc1
-	-	kIx~
http://buttonmuseum.org/	http://buttonmuseum.org/	k?
</s>
<s>
Political	Politicat	k5eAaPmAgInS
Pinbacks	Pinbacks	k1gInSc1
and	and	k?
Buttons	Buttons	k1gInSc1
<g/>
,	,	kIx,
.	.	kIx.
</s>
<s desamb="1">
Reviews	Reviews	k1gInSc1
&	&	k?
Guides	Guides	k1gInSc1
<g/>
,	,	kIx,
eBay	eBay	k1gInPc1
</s>
