spínacím	spínací	k2eAgInSc7d1
špendlíkem	špendlík	k1gInSc7
<g/>
,	,	kIx,
slangově	slangově	k6eAd1
"	"	kIx"
<g/>
placka	placka	k1gFnSc1
se	s	k7c7
sichrhajckou	sichrhajcka	k1gFnSc7
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
předmět	předmět	k1gInSc4
kruhového	kruhový	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
(	(	kIx(
<g/>
jsou	být	k5eAaImIp3nP
i	i	k9
atypické	atypický	k2eAgInPc1d1
tvary	tvar	k1gInPc1
jako	jako	k8xC,k8xS
čtverce	čtverec	k1gInPc1
<g/>
,	,	kIx,
obdélníky	obdélník	k1gInPc1
<g/>
,	,	kIx,
ovály	ovál	k1gInPc1
<g/>
,	,	kIx,
srdíčka	srdíčko	k1gNnPc1
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
<g/>
)	)	kIx)
obvykle	obvykle	k6eAd1
kovový	kovový	k2eAgInSc1d1
nebo	nebo	k8xC
plastový	plastový	k2eAgInSc1d1
<g/>
,	,	kIx,
ze	z	k7c2
zadní	zadní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
opatřený	opatřený	k2eAgInSc1d1
svíracím	svírací	k2eAgInSc7d1
špendlíkem	špendlík	k1gInSc7
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
ji	on	k3xPp3gFnSc4
lze	lze	k6eAd1
připevnit	připevnit	k5eAaPmF
na	na	k7c4
oděv	oděv	k1gInSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
tašku	taška	k1gFnSc4
