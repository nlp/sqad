<s>
Kanafas	kanafas	k1gInSc1
</s>
<s>
Bavlněný	bavlněný	k2eAgInSc1d1
kanafas	kanafas	k1gInSc1
(	(	kIx(
<g/>
cca	cca	kA
125	#num#	k4
<g/>
g	g	kA
<g/>
/	/	kIx~
<g/>
m²	m²	k?
<g/>
)	)	kIx)
</s>
<s>
Moderní	moderní	k2eAgInSc1d1
kanafas	kanafas	k1gInSc1
vzor	vzor	k1gInSc1
crocus	crocus	k1gInSc1
</s>
<s>
Moderní	moderní	k2eAgInSc1d1
kanafas	kanafas	k1gInSc1
s	s	k7c7
vazebnými	vazebný	k2eAgFnPc7d1
ozdobami	ozdoba	k1gFnPc7
vzor	vzor	k1gInSc1
yellow-chilli	yellow-chill	k1gMnPc7
</s>
<s>
Kanafas	kanafas	k1gInSc1
(	(	kIx(
<g/>
angl.	angl.	k?
<g/>
:	:	kIx,
bed	bed	k?
sheeting	sheeting	k1gInSc1
/	/	kIx~
<g/>
ve	v	k7c6
wikislovníku	wikislovník	k1gInSc6
coarse	coarse	k1gFnSc1
cotton	cotton	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
googlu	googlo	k1gNnSc6
canvas	canvasa	k1gFnPc2
<g/>
/	/	kIx~
<g/>
,	,	kIx,
něm.	něm.	k?
<g/>
:	:	kIx,
Züchen	Züchen	k1gInSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
</s>
<s>
bavlnářská	bavlnářský	k2eAgFnSc1d1
tkanina	tkanina	k1gFnSc1
střední	střední	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
<g/>
,	,	kIx,
ze	z	k7c2
středně	středně	k6eAd1
hrubých	hrubý	k2eAgFnPc2d1
přízí	příz	k1gFnPc2
(	(	kIx(
<g/>
barvených	barvený	k2eAgFnPc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tkaná	tkaný	k2eAgFnSc1d1
v	v	k7c6
plátnové	plátnový	k2eAgFnSc6d1
vazbě	vazba	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
povrchu	povrch	k1gInSc6
zřetelná	zřetelný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzory	vzor	k1gInPc1
kanafasu	kanafas	k1gInSc2
jsou	být	k5eAaImIp3nP
tvořeny	tvořit	k5eAaImNgFnP
buď	buď	k8xC
podélnými	podélný	k2eAgInPc7d1
nebo	nebo	k8xC
příčnými	příčný	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
kárem	káro	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
vzniká	vznikat	k5eAaImIp3nS
použitím	použití	k1gNnSc7
obojího	obojí	k4xRgMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známý	známý	k2eAgInSc1d1
<g/>
,	,	kIx,
jednoduchý	jednoduchý	k2eAgInSc1d1
vzor	vzor	k1gInSc1
jsou	být	k5eAaImIp3nP
drobné	drobný	k2eAgFnPc4d1
červené	červený	k2eAgFnPc4d1
či	či	k8xC
modré	modrý	k2eAgFnPc4d1
kostičky	kostička	k1gFnPc4
o	o	k7c6
velikosti	velikost	k1gFnSc6
cca	cca	kA
1	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
ale	ale	k8xC
možnosti	možnost	k1gFnPc1
kombinací	kombinace	k1gFnPc2
ve	v	k7c6
spojení	spojení	k1gNnSc6
s	s	k7c7
barvami	barva	k1gFnPc7
jsou	být	k5eAaImIp3nP
rozmanité	rozmanitý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
bývá	bývat	k5eAaImIp3nS
kanafas	kanafas	k1gInSc4
zdoben	zdoben	k2eAgInSc4d1
drobnými	drobné	k1gInPc7
vazebnými	vazebný	k2eAgInPc7d1
efekty	efekt	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
ložní	ložní	k2eAgNnSc4d1
a	a	k8xC
stolní	stolní	k2eAgNnSc4d1
prádlo	prádlo	k1gNnSc4
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
vzory	vzor	k1gInPc1
se	se	k3xPyFc4
uplatňují	uplatňovat	k5eAaImIp3nP
na	na	k7c4
oděvy	oděv	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Historický	historický	k2eAgInSc1d1
pohled	pohled	k1gInSc1
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1
kanafas	kanafas	k1gInSc1
se	se	k3xPyFc4
odvozuje	odvozovat	k5eAaImIp3nS
od	od	k7c2
latinského	latinský	k2eAgInSc2d1
"	"	kIx"
<g/>
canabis	canabis	k1gFnSc1
<g/>
"	"	kIx"
-	-	kIx~
konopí	konopí	k1gNnSc1
-	-	kIx~
neboť	neboť	k8xC
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
velmi	velmi	k6eAd1
dobrý	dobrý	k2eAgInSc1d1
pevný	pevný	k2eAgInSc1d1
textilní	textilní	k2eAgInSc1d1
materiál	materiál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
písemných	písemný	k2eAgFnPc6d1
zprávách	zpráva	k1gFnPc6
výraz	výraz	k1gInSc1
"	"	kIx"
<g/>
kanevač	kanevač	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
"	"	kIx"
<g/>
kanebáč	kanebáč	k1gInSc1
<g/>
"	"	kIx"
nebo	nebo	k8xC
"	"	kIx"
<g/>
kanawas	kanawas	k1gInSc1
<g/>
"	"	kIx"
už	už	k9
i	i	k9
s	s	k7c7
upřesněním	upřesnění	k1gNnSc7
"	"	kIx"
<g/>
vzorovaná	vzorovaný	k2eAgFnSc1d1
lněná	lněný	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Výroba	výroba	k1gFnSc1
plátna	plátno	k1gNnSc2
pro	pro	k7c4
vlastní	vlastní	k2eAgFnSc4d1
spotřebu	spotřeba	k1gFnSc4
byla	být	k5eAaImAgFnS
v	v	k7c6
Českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
běžná	běžný	k2eAgFnSc1d1
v	v	k7c6
každé	každý	k3xTgFnSc6
rodině	rodina	k1gFnSc6
již	již	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
-	-	kIx~
svědčí	svědčit	k5eAaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
archeologické	archeologický	k2eAgInPc1d1
nálezy	nález	k1gInPc1
kamenných	kamenný	k2eAgNnPc2d1
závaží	závaží	k1gNnPc2
používaných	používaný	k2eAgNnPc2d1
na	na	k7c6
vertikálních	vertikální	k2eAgInPc6d1
stavech	stav	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
Další	další	k2eAgNnPc4d1
staletí	staletí	k1gNnPc4
tkali	tkát	k5eAaImAgMnP
látky	látka	k1gFnPc4
pro	pro	k7c4
vlastní	vlastní	k2eAgFnSc4d1
spotřebu	spotřeba	k1gFnSc4
jak	jak	k8xC,k8xS
muži	muž	k1gMnPc1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
ženy	žena	k1gFnPc1
už	už	k9
na	na	k7c6
horizontálních	horizontální	k2eAgInPc6d1
stavech	stav	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
byli	být	k5eAaImAgMnP
ve	v	k7c6
spisech	spis	k1gInPc6
jmenováni	jmenován	k2eAgMnPc1d1
nejen	nejen	k6eAd1
pláteníci	pláteník	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
specialisté	specialista	k1gMnPc1
<g/>
,	,	kIx,
např.	např.	kA
šlojířníci	šlojířník	k1gMnPc1
-	-	kIx~
tkalci	tkadlec	k1gMnPc1
jemných	jemný	k2eAgFnPc2d1
látek	látka	k1gFnPc2
a	a	k8xC
závojů	závoj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
14	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byli	být	k5eAaImAgMnP
v	v	k7c6
městech	město	k1gNnPc6
organizováni	organizovat	k5eAaBmNgMnP
tkalci	tkadlec	k1gMnSc3
v	v	k7c6
ceších	cech	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
"	"	kIx"
<g/>
Vesnické	vesnický	k2eAgFnPc1d1
i	i	k8xC
městské	městský	k2eAgFnPc1d1
ženy	žena	k1gFnPc1
byly	být	k5eAaImAgFnP
ještě	ještě	k6eAd1
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
stálými	stálý	k2eAgFnPc7d1
konkurentkami	konkurentka	k1gFnPc7
tkalců	tkadlec	k1gMnPc2
<g/>
,	,	kIx,
tkajíce	tkát	k5eAaImSgFnP
pro	pro	k7c4
sebe	sebe	k3xPyFc4
trouby	trouba	k1gFnSc2
(	(	kIx(
<g/>
role	role	k1gFnPc1
<g/>
)	)	kIx)
plátna	plátno	k1gNnPc1
samy	sám	k3xTgFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výroba	výroba	k1gFnSc1
těchto	tento	k3xDgFnPc2
žen	žena	k1gFnPc2
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
vlastní	vlastní	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
bránila	bránit	k5eAaImAgFnS
vzrůstu	vzrůst	k1gInSc3
cechovních	cechovní	k2eAgMnPc2d1
pláteníků	pláteník	k1gMnPc2
ve	v	k7c6
městech	město	k1gNnPc6
právě	právě	k6eAd1
tak	tak	k6eAd1
jako	jako	k8xS,k8xC
značný	značný	k2eAgInSc1d1
počet	počet	k1gInSc1
venkovských	venkovský	k2eAgMnPc2d1
řemeslníků	řemeslník	k1gMnPc2
pracujících	pracující	k2eAgMnPc2d1
v	v	k7c6
horách	hora	k1gFnPc6
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
tkali	tkát	k5eAaImAgMnP
pro	pro	k7c4
manufaktury	manufaktura	k1gFnPc4
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Štučky	štuček	k1gInPc1
bílého	bílé	k1gNnSc2
<g/>
,	,	kIx,
modře	modř	k1gFnSc2
nebo	nebo	k8xC
červeně	červeň	k1gFnSc2
pruhovaného	pruhovaný	k2eAgNnSc2d1
plátna	plátno	k1gNnSc2
byly	být	k5eAaImAgInP
již	již	k6eAd1
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
odkazovány	odkazován	k2eAgFnPc1d1
v	v	k7c6
testamentech	testament	k1gInPc6
s	s	k7c7
doslovným	doslovný	k2eAgNnSc7d1
zněním	znění	k1gNnSc7
<g/>
:	:	kIx,
"	"	kIx"
<g/>
s	s	k7c7
pruhováním	pruhování	k1gNnSc7
tureckou	turecký	k2eAgFnSc7d1
přízí	příz	k1gFnSc7
červeně	červeň	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pruhovaných	pruhovaný	k2eAgNnPc2d1
i	i	k8xC
kostkovaných	kostkovaný	k2eAgNnPc2d1
pláten	plátno	k1gNnPc2
se	se	k3xPyFc4
již	již	k6eAd1
od	od	k7c2
středověku	středověk	k1gInSc2
šily	šít	k5eAaImAgInP
nejen	nejen	k6eAd1
povlaky	povlak	k1gInPc1
peřin	peřina	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
ženské	ženský	k2eAgFnPc4d1
sukně	sukně	k1gFnPc4
a	a	k8xC
zástěry	zástěra	k1gFnPc4
<g/>
,	,	kIx,
mužské	mužský	k2eAgFnPc4d1
vesty	vesta	k1gFnPc4
<g/>
;	;	kIx,
a	a	k8xC
na	na	k7c6
gotických	gotický	k2eAgInPc6d1
obrazech	obraz	k1gInPc6
<g/>
,	,	kIx,
plastikách	plastika	k1gFnPc6
a	a	k8xC
iluminacích	iluminace	k1gFnPc6
pergamenových	pergamenový	k2eAgInPc2d1
kodexů	kodex	k1gInPc2
jsou	být	k5eAaImIp3nP
zachyceny	zachycen	k2eAgInPc1d1
pruhované	pruhovaný	k2eAgInPc1d1
ubrusy	ubrus	k1gInPc1
a	a	k8xC
roušky	rouška	k1gFnPc1
na	na	k7c6
hlavách	hlava	k1gFnPc6
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
hospodářství	hospodářství	k1gNnSc6
se	se	k3xPyFc4
užívalo	užívat	k5eAaImAgNnS
také	také	k9
pruhovaných	pruhovaný	k2eAgFnPc2d1
látek	látka	k1gFnPc2
-	-	kIx~
např.	např.	kA
na	na	k7c6
pytlích	pytel	k1gInPc6
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgFnPc6
se	se	k3xPyFc4
vozilo	vozit	k5eAaImAgNnS
obilí	obilí	k1gNnSc1
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
každá	každý	k3xTgFnSc1
rodina	rodina	k1gFnSc1
jiný	jiný	k1gMnSc1
pruh	pruh	k1gInSc1
(	(	kIx(
<g/>
pruh	pruh	k1gInSc1
<g/>
=	=	kIx~
<g/>
jmenovka	jmenovka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
sláma	sláma	k1gFnSc1
v	v	k7c6
postelích	postel	k1gFnPc6
se	se	k3xPyFc4
každým	každý	k3xTgInSc7
rokem	rok	k1gInSc7
plnila	plnit	k5eAaImAgFnS
do	do	k7c2
pruhovaných	pruhovaný	k2eAgInPc2d1
slamníků	slamník	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
byly	být	k5eAaImAgFnP
Čechy	Čechy	k1gFnPc1
velkou	velká	k1gFnSc4
textilní	textilní	k2eAgFnSc4d1
výrobnou	výrobna	k1gFnSc7
<g/>
,	,	kIx,
rozmohlo	rozmoct	k5eAaPmAgNnS
se	se	k3xPyFc4
tkalcovství	tkalcovství	k1gNnSc1
manufakturní	manufakturní	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
vlivem	vlivem	k7c2
evropské	evropský	k2eAgFnSc2d1
konkurence	konkurence	k1gFnSc2
začaly	začít	k5eAaPmAgFnP
manufaktury	manufaktura	k1gFnSc2
své	svůj	k3xOyFgInPc4
provozy	provoz	k1gInPc4
uzavírat	uzavírat	k5eAaImF,k5eAaPmF
a	a	k8xC
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
začali	začít	k5eAaPmAgMnP
nezaměstnaní	zaměstnaný	k2eNgMnPc1d1
tkalci	tkadlec	k1gMnPc1
tkát	tkát	k5eAaImF
pro	pro	k7c4
místní	místní	k2eAgFnSc4d1
spotřebu	spotřeba	k1gFnSc4
vesnických	vesnický	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
doma	doma	k6eAd1
upředeného	upředený	k2eAgInSc2d1
lnu	len	k1gInSc2
(	(	kIx(
<g/>
na	na	k7c4
osnovu	osnova	k1gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
koupeného	koupený	k2eAgInSc2d1
bavlněného	bavlněný	k2eAgInSc2d1
útku	útek	k1gInSc2
tkali	tkát	k5eAaImAgMnP
pevné	pevný	k2eAgFnPc4d1
látky	látka	k1gFnPc4
na	na	k7c4
sukně	sukně	k1gFnPc4
<g/>
,	,	kIx,
zástěry	zástěra	k1gFnPc4
a	a	k8xC
ložní	ložní	k2eAgNnSc4d1
prádlo	prádlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
ženy-objednavatelky	ženy-objednavatelka	k1gFnPc1
byly	být	k5eAaImAgFnP
náročné	náročný	k2eAgInPc4d1
na	na	k7c4
vzory	vzor	k1gInPc4
nově	nova	k1gFnSc3
pořizovaných	pořizovaný	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
,	,	kIx,
snažili	snažit	k5eAaImAgMnP
se	se	k3xPyFc4
tkalci	tkadlec	k1gMnPc1
vymýšlet	vymýšlet	k5eAaImF
nové	nový	k2eAgFnPc4d1
a	a	k8xC
nové	nový	k2eAgFnPc4d1
kombinace	kombinace	k1gFnPc4
barev	barva	k1gFnPc2
<g/>
,	,	kIx,
pruhů	pruh	k1gInPc2
i	i	k8xC
kostek	kostka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc4d1
rčení	rčení	k1gNnSc4
„	„	k?
<g/>
hup	hup	k1gInSc1
do	do	k7c2
kanafasu	kanafas	k1gInSc2
<g/>
“	“	k?
nebo	nebo	k8xC
popěvek	popěvek	k1gInSc1
„	„	k?
<g/>
proto	proto	k8xC
jsem	být	k5eAaImIp1nS
si	se	k3xPyFc3
kanafasu	kanafas	k1gInSc2
koupila	koupit	k5eAaPmAgFnS
<g/>
,	,	kIx,
abych	aby	kYmCp1nS
se	se	k3xPyFc4
Ti	ty	k3xPp2nSc3
můj	můj	k3xOp1gInSc1
Jeníčku	Jeníček	k1gMnSc3
líbila	líbit	k5eAaImAgFnS
<g/>
“	“	k?
<g/>
,	,	kIx,
svědčí	svědčit	k5eAaImIp3nS
o	o	k7c4
jeho	jeho	k3xOp3gNnSc4
rozšíření	rozšíření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kanafasy	kanafas	k1gInPc1
se	se	k3xPyFc4
zařadily	zařadit	k5eAaPmAgInP
také	také	k9
do	do	k7c2
lidových	lidový	k2eAgInPc2d1
krojů	kroj	k1gInPc2
a	a	k8xC
díky	díky	k7c3
výtvarnému	výtvarný	k2eAgNnSc3d1
nadání	nadání	k1gNnSc3
tkalců	tkadlec	k1gMnPc2
se	se	k3xPyFc4
nám	my	k3xPp1nPc3
uchovaly	uchovat	k5eAaPmAgInP
desítky	desítka	k1gFnSc2
-	-	kIx~
stovky	stovka	k1gFnPc4
vzorů	vzor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vzory	vzor	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
zachovaly	zachovat	k5eAaPmAgInP
v	v	k7c6
rámci	rámec	k1gInSc6
etnografických	etnografický	k2eAgFnPc2d1
sbírek	sbírka	k1gFnPc2
<g/>
,	,	kIx,
mohly	moct	k5eAaImAgFnP
navázat	navázat	k5eAaPmF
návrhářky	návrhářka	k1gFnPc4
ÚLUV	ÚLUV	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Koloristickým	koloristický	k2eAgInSc7d1
základem	základ	k1gInSc7
českých	český	k2eAgInPc2d1
kanafasů	kanafas	k1gInPc2
v	v	k7c6
lidové	lidový	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
je	být	k5eAaImIp3nS
na	na	k7c6
bílém	bílý	k2eAgInSc6d1
nebo	nebo	k8xC
režném	režný	k2eAgInSc6d1
základu	základ	k1gInSc6
indigově	indigově	k6eAd1
modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
turecká	turecký	k2eAgFnSc1d1
červená	červená	k1gFnSc1
<g/>
,	,	kIx,
ke	k	k7c3
které	který	k3yRgFnSc3,k3yIgFnSc3,k3yQgFnSc3
přibyla	přibýt	k5eAaPmAgFnS
žlutá	žlutý	k2eAgFnSc1d1
<g/>
,	,	kIx,
černá	černý	k2eAgFnSc1d1
a	a	k8xC
teprve	teprve	k6eAd1
v	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
<g/>
pol.	pol.	k?
19	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
méně	málo	k6eAd2
často	často	k6eAd1
zelená	zelený	k2eAgFnSc1d1
<g/>
,	,	kIx,
či	či	k8xC
fialová	fialový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
přelomu	přelom	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
přešlo	přejít	k5eAaPmAgNnS
z	z	k7c2
tuzemské	tuzemský	k2eAgFnSc2d1
suroviny	surovina	k1gFnSc2
-	-	kIx~
lnu	lnout	k5eAaImIp1nS
-	-	kIx~
na	na	k7c4
snáze	snadno	k6eAd2
zpracovatelnou	zpracovatelný	k2eAgFnSc4d1
dováženou	dovážený	k2eAgFnSc4d1
bavlnu	bavlna	k1gFnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
přibývala	přibývat	k5eAaImAgFnS
barevnost	barevnost	k1gFnSc1
a	a	k8xC
bohatost	bohatost	k1gFnSc1
členění	členění	k1gNnSc2
pruhů	pruh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgFnPc1d1
barevnosti	barevnost	k1gFnPc1
se	se	k3xPyFc4
objevovaly	objevovat	k5eAaImAgFnP
v	v	k7c6
manufakturách	manufaktura	k1gFnPc6
a	a	k8xC
tradiční	tradiční	k2eAgFnSc3d1
místní	místní	k2eAgFnSc3d1
barevnosti	barevnost	k1gFnSc3
zcela	zcela	k6eAd1
rozrušila	rozrušit	k5eAaPmAgFnS
tovární	tovární	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
s	s	k7c7
masovým	masový	k2eAgInSc7d1
odbytem	odbyt	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
1946	#num#	k4
přebrala	přebrat	k5eAaPmAgFnS
státem	stát	k1gInSc7
zřízená	zřízený	k2eAgFnSc1d1
instituce	instituce	k1gFnSc1
Ústředí	ústředí	k1gNnSc2
lidové	lidový	k2eAgFnSc2d1
umělecké	umělecký	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
-	-	kIx~
ÚLUV	ÚLUV	kA
dohled	dohled	k1gInSc1
nad	nad	k7c7
„	„	k?
<g/>
lidovou	lidový	k2eAgFnSc7d1
výrobou	výroba	k1gFnSc7
<g/>
“	“	k?
kanafasu	kanafas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soustředila	soustředit	k5eAaPmAgFnS
výrobu	výroba	k1gFnSc4
do	do	k7c2
Postřekova	Postřekov	k1gInSc2
u	u	k7c2
Domažlic	Domažlice	k1gFnPc2
<g/>
,	,	kIx,
u	u	k7c2
Štěpána	Štěpán	k1gMnSc2
Kapice	Kapic	k1gMnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc1
dílnu	dílna	k1gFnSc4
postupně	postupně	k6eAd1
rozšířila	rozšířit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Kanafas	kanafas	k1gInSc1
už	už	k6eAd1
nebyl	být	k5eNaImAgInS
vyráběn	vyrábět	k5eAaImNgInS
ručně	ručně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
mechanicky	mechanicky	k6eAd1
a	a	k8xC
použitý	použitý	k2eAgInSc4d1
materiál	materiál	k1gInSc4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
%	%	kIx~
bavlněná	bavlněný	k2eAgFnSc1d1
příze	příze	k1gFnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
také	také	k9
zpracovávána	zpracovávat	k5eAaImNgFnS
strojově	strojově	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1981-1989	1981-1989	k4
se	se	k3xPyFc4
vyrábělo	vyrábět	k5eAaImAgNnS
kolem	kolem	k7c2
200	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
m	m	kA
kanafasu	kanafas	k1gInSc2
ročně	ročně	k6eAd1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
tkaniny	tkanina	k1gFnSc2
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
příze	příz	k1gFnSc2
<g/>
,	,	kIx,
dostava	dostava	k1gFnSc1
<g/>
,	,	kIx,
osnovní	osnovní	k2eAgFnSc1d1
i	i	k8xC
útková	útkový	k2eAgFnSc1d1
a	a	k8xC
gramáž	gramáž	k1gFnSc1
se	se	k3xPyFc4
„	„	k?
<g/>
usadila	usadit	k5eAaPmAgFnS
<g/>
“	“	k?
na	na	k7c4
cca	cca	kA
140-170	140-170	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Artikl	artikl	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
si	se	k3xPyFc3
povinně	povinně	k6eAd1
musel	muset	k5eAaImAgMnS
zachovávat	zachovávat	k5eAaImF
jisté	jistý	k2eAgInPc4d1
prvky	prvek	k1gInPc4
lidovosti	lidovost	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
přesto	přesto	k8xC
vyvíjel	vyvíjet	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
tradičních	tradiční	k2eAgInPc2d1
–	–	k?
klasických	klasický	k2eAgInPc2d1
vzorů	vzor	k1gInPc2
<g/>
,	,	kIx,
pravidelných	pravidelný	k2eAgInPc2d1
bílo-barevných	bílo-barevný	k2eAgInPc2d1
pruhů	pruh	k1gInPc2
a	a	k8xC
kostek	kostka	k1gFnPc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
vyráběny	vyrábět	k5eAaImNgInP
i	i	k9
vzory	vzor	k1gInPc4
podle	podle	k7c2
návrhů	návrh	k1gInPc2
výtvarníků	výtvarník	k1gMnPc2
profesionálně	profesionálně	k6eAd1
školených	školený	k2eAgMnPc2d1
na	na	k7c6
Vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc6d1
<g/>
,	,	kIx,
obor	obor	k1gInSc4
textilní	textilní	k2eAgNnPc4d1
výtvarnictví	výtvarnictví	k1gNnPc4
(	(	kIx(
<g/>
ak.	ak.	k?
mal	málit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslava	Jaroslava	k1gFnSc1
Bloudková	Bloudkový	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
do	do	k7c2
roku	rok	k1gInSc2
1981	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1981	#num#	k4
ak.	ak.	k?
mal	málit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eva	Eva	k1gFnSc1
Jandíková	Jandíková	k1gFnSc1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tradice	tradice	k1gFnSc2
tedy	tedy	k9
zůstal	zůstat	k5eAaPmAgMnS
vlastní	vlastní	k2eAgInSc4d1
charakter	charakter	k1gInSc4
tkaní	tkaní	k1gNnSc2
<g/>
,	,	kIx,
omezená	omezený	k2eAgFnSc1d1
barevná	barevný	k2eAgFnSc1d1
škála	škála	k1gFnSc1
(	(	kIx(
<g/>
ještě	ještě	k9
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
bylo	být	k5eAaImAgNnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
pouze	pouze	k6eAd1
12	#num#	k4
barev	barva	k1gFnPc2
včetně	včetně	k7c2
bílé	bílý	k2eAgFnSc2d1
a	a	k8xC
černé	černá	k1gFnSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
prodej	prodej	k1gInSc1
látky	látka	k1gFnSc2
bez	bez	k7c2
jakýchkoliv	jakýkoliv	k3yIgFnPc2
dalších	další	k2eAgFnPc2d1
úprav	úprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kanafasy	kanafas	k1gInPc1
byly	být	k5eAaImAgInP
přijímány	přijímat	k5eAaImNgInP
v	v	k7c6
rámci	rámec	k1gInSc6
úluváckého	úluvácký	k2eAgInSc2d1
stylu	styl	k1gInSc2
jako	jako	k8xC,k8xS
látky	látka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
patří	patřit	k5eAaImIp3nP
na	na	k7c4
chalupu	chalupa	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
některé	některý	k3yIgInPc1
nové	nový	k2eAgInPc1d1
vzory	vzor	k1gInPc1
vybočovaly	vybočovat	k5eAaImAgInP
z	z	k7c2
této	tento	k3xDgFnSc2
tradice	tradice	k1gFnSc2
a	a	k8xC
designově	designově	k6eAd1
zapadaly	zapadat	k5eAaPmAgFnP,k5eAaImAgFnP
do	do	k7c2
současného	současný	k2eAgInSc2d1
interiéru	interiér	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oděvní	oděvní	k2eAgInSc1d1
design	design	k1gInSc1
Úluvu	Úluv	k1gInSc2
se	se	k3xPyFc4
plánovaně	plánovaně	k6eAd1
snažil	snažit	k5eAaImAgMnS
držet	držet	k5eAaImF
krok	krok	k1gInSc4
s	s	k7c7
módou	móda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
devadesátých	devadesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozšíření	rozšíření	k1gNnSc3
barevné	barevný	k2eAgFnSc2d1
škály	škála	k1gFnSc2
a	a	k8xC
dodatečnému	dodatečný	k2eAgNnSc3d1
zušlechťování	zušlechťování	k1gNnSc3
mercerací	mercerace	k1gFnPc2
a	a	k8xC
sanforizací	sanforizace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Tkanina	tkanina	k1gFnSc1
tím	ten	k3xDgNnSc7
získávala	získávat	k5eAaImAgFnS
mnohem	mnohem	k6eAd1
příjemnější	příjemný	k2eAgFnPc4d2
uživatelské	uživatelský	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
,	,	kIx,
nesrážela	srážet	k5eNaImAgFnS
se	se	k3xPyFc4
tolik	tolik	k6eAd1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
splývavá	splývavý	k2eAgNnPc1d1
<g/>
,	,	kIx,
snadno	snadno	k6eAd1
se	se	k3xPyFc4
prala	prát	k5eAaImAgFnS
a	a	k8xC
žehlila	žehlit	k5eAaImAgFnS
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
barevně	barevně	k6eAd1
výraznější	výrazný	k2eAgFnSc1d2
<g/>
,	,	kIx,
nebyla	být	k5eNaImAgFnS
matná	matný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
pololesklá	pololesklý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
kanafasu	kanafas	k1gInSc6
objevil	objevit	k5eAaPmAgInS
projev	projev	k1gInSc1
šanžánování	šanžánování	k1gNnSc2
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
známý	známý	k1gMnSc1
dosud	dosud	k6eAd1
jen	jen	k9
z	z	k7c2
hedvábných	hedvábný	k2eAgFnPc2d1
tkanin	tkanina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
dochází	docházet	k5eAaImIp3nS
v	v	k7c6
konstrukci	konstrukce	k1gFnSc6
kanafasu	kanafas	k1gInSc2
ke	k	k7c3
změnám	změna	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
rozvětvují	rozvětvovat	k5eAaImIp3nP
tradiční	tradiční	k2eAgFnSc4d1
kvalitu	kvalita	k1gFnSc4
do	do	k7c2
více	hodně	k6eAd2
různých	různý	k2eAgNnPc2d1
provedení	provedení	k1gNnPc2
<g/>
:	:	kIx,
</s>
<s>
Gramáž	gramáž	k1gFnSc1
140	#num#	k4
<g/>
-	-	kIx~
<g/>
170	#num#	k4
<g/>
g	g	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
a	a	k8xC
více	hodně	k6eAd2
je	být	k5eAaImIp3nS
vhodná	vhodný	k2eAgFnSc1d1
pro	pro	k7c4
ubrusy	ubrus	k1gInPc4
<g/>
,	,	kIx,
polštáře	polštář	k1gInPc4
<g/>
,	,	kIx,
chňapky	chňapek	k1gInPc4
<g/>
,	,	kIx,
zástěry	zástěra	k1gFnPc4
apod.	apod.	kA
</s>
<s>
Gramáž	gramáž	k1gFnSc1
112	#num#	k4
<g/>
-	-	kIx~
<g/>
120	#num#	k4
<g/>
g	g	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
při	při	k7c6
použití	použití	k1gNnSc6
jemnější	jemný	k2eAgFnSc2d2
příze	příz	k1gFnSc2
a	a	k8xC
větší	veliký	k2eAgFnSc2d2
dostavy	dostava	k1gFnSc2
(	(	kIx(
<g/>
39	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
vhodná	vhodný	k2eAgFnSc1d1
pro	pro	k7c4
ložní	ložní	k2eAgNnSc4d1
povlečení	povlečení	k1gNnSc4
a	a	k8xC
blíží	blížit	k5eAaImIp3nS
se	se	k3xPyFc4
jemností	jemnost	k1gFnSc7
<g/>
,	,	kIx,
hustotou	hustota	k1gFnSc7
a	a	k8xC
měkkým	měkký	k2eAgInSc7d1
omakem	omak	k1gInSc7
perkálu	perkál	k1gInSc2
(	(	kIx(
<g/>
percale	percale	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
pro	pro	k7c4
kvalitní	kvalitní	k2eAgNnSc4d1
ložní	ložní	k2eAgNnSc4d1
prádlo	prádlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
perkálu	perkál	k1gInSc2
není	být	k5eNaImIp3nS
tkanina	tkanina	k1gFnSc1
vzorována	vzorován	k2eAgFnSc1d1
tiskem	tisk	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
"	"	kIx"
<g/>
jen	jen	k9
<g/>
"	"	kIx"
barevnou	barevný	k2eAgFnSc7d1
přízí	příz	k1gFnSc7
a	a	k8xC
pořád	pořád	k6eAd1
si	se	k3xPyFc3
drží	držet	k5eAaImIp3nS
složení	složení	k1gNnSc4
z	z	k7c2
přírodního	přírodní	k2eAgNnSc2d1
vlákna	vlákno	k1gNnSc2
(	(	kIx(
<g/>
100	#num#	k4
<g/>
%	%	kIx~
bavlny	bavlna	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
vyšší	vysoký	k2eAgInSc4d2
komfort	komfort	k1gInSc4
uživatele	uživatel	k1gMnSc2
je	být	k5eAaImIp3nS
vysrážená	vysrážený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tato	tento	k3xDgFnSc1
jemnější	jemný	k2eAgFnSc1d2
a	a	k8xC
splývavější	splývavý	k2eAgFnSc1d2
látka	látka	k1gFnSc1
nese	nést	k5eAaImIp3nS
název	název	k1gInSc1
kanafas	kanafas	k1gInSc1
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
převážně	převážně	k6eAd1
na	na	k7c4
ložní	ložní	k2eAgNnSc4d1
povlečení	povlečení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Gramáž	gramáž	k1gFnSc1
300	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
zachována	zachovat	k5eAaPmNgFnS
ve	v	k7c6
sbírce	sbírka	k1gFnSc6
historických	historický	k2eAgInPc2d1
kanafasů	kanafas	k1gInPc2
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
PhDr.	PhDr.	kA
Jitky	Jitka	k1gFnSc2
Staňkové	Staňková	k1gFnSc2
a	a	k8xC
dnes	dnes	k6eAd1
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nP
repliky	replika	k1gFnSc2
této	tento	k3xDgFnSc2
tkaniny	tkanina	k1gFnSc2
používané	používaný	k2eAgFnSc2d1
k	k	k7c3
čalounění	čalounění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barevnosti	barevnost	k1gFnPc1
a	a	k8xC
vzorování	vzorování	k1gNnSc1
současného	současný	k2eAgInSc2d1
kanafasu	kanafas	k1gInSc2
dovolují	dovolovat	k5eAaImIp3nP
i	i	k9
přesah	přesah	k1gInSc4
od	od	k7c2
praktického	praktický	k2eAgNnSc2d1
použití	použití	k1gNnSc2
až	až	k9
do	do	k7c2
uměleckých	umělecký	k2eAgFnPc2d1
expozic	expozice	k1gFnPc2
<g/>
,	,	kIx,
jak	jak	k6eAd1
to	ten	k3xDgNnSc1
ukázaly	ukázat	k5eAaPmAgInP
některé	některý	k3yIgInPc1
výstavy	výstav	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
kanafas	kanafas	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
kanafas	kanafas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Archivováno	archivován	k2eAgNnSc1d1
25	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
-	-	kIx~
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
kanafas	kanafas	k1gInSc4
a	a	k8xC
jak	jak	k6eAd1
se	se	k3xPyFc4
vyvíjel	vyvíjet	k5eAaImAgMnS
-	-	kIx~
úvodní	úvodní	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
PhDr.	PhDr.	kA
Jitky	Jitka	k1gFnSc2
Staňkové	Staňková	k1gFnSc2
<g/>
,	,	kIx,
CSc.	CSc.	kA
a	a	k8xC
prof.	prof.	kA
PhDr.	PhDr.	kA
Ludvíka	Ludvík	k1gMnSc2
Barana	Baran	k1gMnSc2
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
dr	dr	kA
<g/>
.	.	kIx.
h.	h.	k?
c.	c.	k?
k	k	k7c3
výstavě	výstava	k1gFnSc3
kanafasu	kanafas	k1gInSc2
v	v	k7c6
Jílovém	Jílové	k1gNnSc6
u	u	k7c2
Prahy	Praha	k1gFnSc2
</s>
<s>
T.	T.	kA
<g/>
Meyer	Meyer	k1gMnSc1
zur	zur	k?
Capellen	Capellen	k1gInSc1
<g/>
:	:	kIx,
Lexikon	lexikon	k1gInSc1
der	drát	k5eAaImRp2nS
Gewebe	Geweb	k1gMnSc5
<g/>
,	,	kIx,
Deutscher	Deutschra	k1gFnPc2
Fachverlag	Fachverlaga	k1gFnPc2
<g/>
,	,	kIx,
Frankfurt	Frankfurt	k1gInSc1
<g/>
/	/	kIx~
<g/>
Main	Main	k1gInSc1
2001	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
87150	#num#	k4
<g/>
-	-	kIx~
<g/>
725	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
378	#num#	k4
</s>
<s>
Designcabinet	Designcabinet	k5eAaBmF,k5eAaImF,k5eAaPmF
Barevný	barevný	k2eAgInSc4d1
absťák	absťák	k1gInSc4
Evy	Eva	k1gFnSc2
Jandíkové	Jandíková	k1gFnSc2
</s>
<s>
Krásná	krásný	k2eAgFnSc1d1
jizba	jizba	k1gFnSc1
byla	být	k5eAaImAgFnS
a	a	k8xC
je	být	k5eAaImIp3nS
pojmem	pojem	k1gInSc7
</s>
<s>
Adlerová	Adlerová	k1gFnSc1
Alena	Alena	k1gFnSc1
<g/>
,	,	kIx,
České	český	k2eAgNnSc1d1
užité	užitý	k2eAgNnSc1d1
umění	umění	k1gNnSc1
1918	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
,	,	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1983	#num#	k4
</s>
<s>
Privatizační	privatizační	k2eAgInSc1d1
projekt	projekt	k1gInSc1
Ing.	ing.	kA
Jiřího	Jiří	k1gMnSc2
Růžičky	Růžička	k1gMnSc2
na	na	k7c6
ÚLUV	ÚLUV	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1992	#num#	k4
</s>
<s>
Privatizační	privatizační	k2eAgInSc1d1
projekt	projekt	k1gInSc1
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s	s	k7c7
r.	r.	kA
o.	o.	k?
Krásná	krásný	k2eAgFnSc1d1
jizba	jizba	k1gFnSc1
<g/>
,	,	kIx,
1993	#num#	k4
</s>
<s>
Švácha	Švácha	k1gMnSc1
Rastislav	Rastislav	k1gMnSc1
<g/>
,	,	kIx,
Od	od	k7c2
moderny	moderna	k1gFnSc2
k	k	k7c3
funkcionalismu	funkcionalismus	k1gInSc3
<g/>
,	,	kIx,
Odeon	odeon	k1gInSc4
<g/>
,	,	kIx,
1985	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
textilní	textilní	k2eAgInSc1d1
výkladový	výkladový	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
↑	↑	k?
Staňková	Staňková	k1gFnSc1
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
<g/>
,	,	kIx,
Etnografické	etnografický	k2eAgFnPc1d1
marginálie	marginálie	k1gFnPc1
k	k	k7c3
textiliím	textilie	k1gFnPc3
z	z	k7c2
období	období	k1gNnSc2
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
lid	lid	k1gInSc1
1964	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
334	#num#	k4
<g/>
-	-	kIx~
<g/>
347	#num#	k4
<g/>
↑	↑	k?
Winter-Dějiny	Winter-Dějin	k2eAgInPc1d1
kroje	kroj	k1gInPc1
v	v	k7c6
zemích	zem	k1gFnPc6
českých	český	k2eAgFnPc2d1
od	od	k7c2
počátku	počátek	k1gInSc2
století	století	k1gNnSc2
XV	XV	kA
až	až	k6eAd1
po	po	k7c4
dobu	doba	k1gFnSc4
pobělohorské	pobělohorský	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1893	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
<g/>
617	#num#	k4
<g/>
↑	↑	k?
Staňková	Staňková	k1gFnSc1
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
<g/>
,	,	kIx,
Středověké	středověký	k2eAgInPc1d1
kanafasy	kanafas	k1gInPc1
<g/>
,	,	kIx,
Umění	umění	k1gNnSc1
a	a	k8xC
řemesla	řemeslo	k1gNnSc2
1975	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
58	#num#	k4
<g/>
-	-	kIx~
<g/>
63	#num#	k4
<g/>
↑	↑	k?
Staňková	Staňková	k1gFnSc1
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
<g/>
,	,	kIx,
Lidové	lidový	k2eAgFnPc1d1
tkaniny	tkanina	k1gFnPc1
v	v	k7c6
tradičním	tradiční	k2eAgMnSc6d1
<g />
.	.	kIx.
</s>
<s hack="1">
oděvu	oděv	k1gInSc2
a	a	k8xC
interiéru	interiér	k1gInSc2
<g/>
,	,	kIx,
Čsl	čsl	kA
<g/>
.	.	kIx.
etnografie	etnografie	k1gFnSc2
r.	r.	kA
<g/>
VIII	VIII	kA
1960	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
384	#num#	k4
<g/>
-	-	kIx~
<g/>
413	#num#	k4
<g/>
↑	↑	k?
Staňková	Staňková	k1gFnSc1
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
<g/>
,	,	kIx,
České	český	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
tkaniny	tkanina	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1989	#num#	k4
<g/>
↑	↑	k?
Staňková	Staňková	k1gFnSc1
Jitka	Jitka	k1gFnSc1
<g/>
:	:	kIx,
Textilní	textilní	k2eAgInPc1d1
vzorníky	vzorník	k1gInPc1
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
pol.	pol.	k?
19	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
ve	v	k7c6
Varnsdorfu	Varnsdorf	k1gInSc6
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
lid	lid	k1gInSc1
1992	#num#	k4
č.	č.	k?
<g/>
3	#num#	k4
<g/>
↑	↑	k?
Krásná	krásný	k2eAgFnSc1d1
Jizba	jizba	k1gFnSc1
a	a	k8xC
vše	všechen	k3xTgNnSc1
kolem	kolem	k6eAd1
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
Žižková	Žižková	k1gFnSc1
<g/>
,	,	kIx,
czech	czech	k1gInSc1
design	design	k1gInSc1
<g/>
↑	↑	k?
Jaroslava	Jaroslava	k1gFnSc1
Bloudková	Bloudkový	k2eAgFnSc1d1
<g/>
↑	↑	k?
Eva	Eva	k1gFnSc1
Jandíková	Jandíková	k1gFnSc1
<g/>
↑	↑	k?
Kanafas	kanafas	k1gInSc1
je	být	k5eAaImIp3nS
i	i	k9
pro	pro	k7c4
mladé	mladý	k1gMnPc4
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
Krausová	Krausová	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
časopis	časopis	k1gInSc1
Living	Living	k1gInSc1
6	#num#	k4
<g/>
/	/	kIx~
2008	#num#	k4
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
16	#num#	k4
<g/>
↑	↑	k?
Kanafas	kanafas	k1gInSc1
je	být	k5eAaImIp3nS
i	i	k9
pro	pro	k7c4
mladé	mladý	k1gMnPc4
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
Krausová	Krausová	k1gFnSc1
<g/>
,	,	kIx,
časopis	časopis	k1gInSc1
Living	Living	k1gInSc1
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
19	#num#	k4
<g/>
↑	↑	k?
Historická	historický	k2eAgFnSc1d1
část	část	k1gFnSc1
výstavy	výstava	k1gFnSc2
kanafasu	kanafas	k1gInSc2
v	v	k7c6
Itálii	Itálie	k1gFnSc6
se	se	k3xPyFc4
sbírky	sbírka	k1gFnSc2
etnografky	etnografka	k1gFnSc2
PhDr.	PhDr.	kA
Jitky	Jitka	k1gFnSc2
Staňkové	Staňková	k1gFnSc2
<g/>
.	.	kIx.
www.kanafas.cz	www.kanafas.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Fiori	Fior	k1gMnPc1
<g/>
,	,	kIx,
colori	color	k1gMnPc1
<g/>
,	,	kIx,
tessuti	tessut	k1gMnPc1
<g/>
,	,	kIx,
Villa	Villa	k1gMnSc1
Porta	porto	k1gNnSc2
Bozollo	Bozollo	k1gNnSc1
<g/>
,	,	kIx,
Calaslzuigno	Calaslzuigno	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
<g/>
.	.	kIx.
www.transfinito.eu	www.transfinito.eu	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4273553-1	4273553-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85012768	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85012768	#num#	k4
</s>
