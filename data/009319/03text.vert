<p>
<s>
Schisma	schisma	k1gFnSc1	schisma
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
σ	σ	k?	σ
rozdělení	rozdělení	k1gNnSc2	rozdělení
(	(	kIx(	(
<g/>
<	<	kIx(	<
σ	σ	k?	σ
rozdělovat	rozdělovat	k5eAaImF	rozdělovat
<g/>
,	,	kIx,	,
půlit	půlit	k5eAaImF	půlit
<g/>
)	)	kIx)	)
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
rozdělení	rozdělení	k1gNnSc4	rozdělení
či	či	k8xC	či
rozkol	rozkol	k1gInSc4	rozkol
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
či	či	k8xC	či
jiné	jiný	k2eAgFnSc3d1	jiná
náboženské	náboženský	k2eAgFnSc3d1	náboženská
instituci	instituce	k1gFnSc3	instituce
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
historickém	historický	k2eAgNnSc6d1	historické
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozkolu	rozkol	k1gInSc3	rozkol
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
jak	jak	k6eAd1	jak
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
věroučných	věroučný	k2eAgInPc2d1	věroučný
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
uznávání	uznávání	k1gNnSc2	uznávání
různých	různý	k2eAgMnPc2d1	různý
náboženských	náboženský	k2eAgMnPc2d1	náboženský
vůdců	vůdce	k1gMnPc2	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Schismatik	schismatik	k1gMnSc1	schismatik
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
nebo	nebo	k8xC	nebo
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS	zapříčiňovat
schisma	schisma	k1gFnSc1	schisma
anebo	anebo	k8xC	anebo
člen	člen	k1gInSc1	člen
odtrhující	odtrhující	k2eAgFnSc2d1	odtrhující
se	se	k3xPyFc4	se
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Užití	užití	k1gNnPc4	užití
v	v	k7c4	v
křesťanství	křesťanství	k1gNnSc4	křesťanství
==	==	k?	==
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
církve	církev	k1gFnSc2	církev
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
asi	asi	k9	asi
největší	veliký	k2eAgFnSc4d3	veliký
skupinu	skupina	k1gFnSc4	skupina
užití	užití	k1gNnSc2	užití
termínu	termín	k1gInSc2	termín
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zde	zde	k6eAd1	zde
schisma	schisma	k1gFnSc1	schisma
označuje	označovat	k5eAaImIp3nS	označovat
rozdělení	rozdělení	k1gNnSc4	rozdělení
<g/>
,	,	kIx,	,
rozštěpení	rozštěpení	k1gNnSc4	rozštěpení
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
<g/>
.	.	kIx.	.
</s>
<s>
Schismatik	schismatik	k1gMnSc1	schismatik
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
schisma	schisma	k1gNnSc4	schisma
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
anebo	anebo	k8xC	anebo
člen	člen	k1gMnSc1	člen
odtržené	odtržený	k2eAgFnSc2d1	odtržená
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
rozdělení	rozdělení	k1gNnSc1	rozdělení
mezi	mezi	k7c7	mezi
křesťany	křesťan	k1gMnPc7	křesťan
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zánik	zánik	k1gInSc1	zánik
<g/>
/	/	kIx~	/
<g/>
rozluka	rozluka	k1gFnSc1	rozluka
společenství	společenství	k1gNnSc2	společenství
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
skupinami	skupina	k1gFnPc7	skupina
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tito	tento	k3xDgMnPc1	tento
již	již	k6eAd1	již
neslouží	sloužit	k5eNaImIp3nP	sloužit
společně	společně	k6eAd1	společně
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Velké	velký	k2eAgNnSc1d1	velké
schizma	schizma	k1gNnSc1	schizma
značí	značit	k5eAaImIp3nS	značit
rozkol	rozkol	k1gInSc1	rozkol
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
porběhlo	porběhnout	k5eAaPmAgNnS	porběhnout
ve	v	k7c6	v
3	[number]	k4	3
fázích	fág	k1gInPc6	fág
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
stav	stav	k1gInSc1	stav
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
484	[number]	k4	484
<g/>
–	–	k?	–
<g/>
519	[number]	k4	519
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
roku	rok	k1gInSc2	rok
861	[number]	k4	861
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
vrcholem	vrchol	k1gInSc7	vrchol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1054	[number]	k4	1054
</s>
</p>
<p>
<s>
papežské	papežský	k2eAgNnSc1d1	papežské
schizma	schizma	k1gNnSc1	schizma
-	-	kIx~	-
čtyřicetileté	čtyřicetiletý	k2eAgNnSc1d1	čtyřicetileté
období	období	k1gNnSc1	období
dvojpapežství	dvojpapežství	k1gNnSc2	dvojpapežství
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
později	pozdě	k6eAd2	pozdě
trojpapežství	trojpapežství	k1gNnSc1	trojpapežství
<g/>
)	)	kIx)	)
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1378	[number]	k4	1378
a	a	k8xC	a
1417	[number]	k4	1417
</s>
</p>
<p>
<s>
odpadnutí	odpadnutí	k1gNnSc1	odpadnutí
kterékoli	kterýkoli	k3yIgFnSc2	kterýkoli
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
opustily	opustit	k5eAaPmAgFnP	opustit
katolickou	katolický	k2eAgFnSc4d1	katolická
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
hereze	hereze	k1gFnSc2	hereze
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vědomé	vědomý	k2eAgNnSc1d1	vědomé
zpochybnění	zpochybnění	k1gNnSc1	zpochybnění
ustanovené	ustanovený	k2eAgFnSc2d1	ustanovená
autority	autorita	k1gFnSc2	autorita
(	(	kIx(	(
<g/>
např	např	kA	např
<g/>
:	:	kIx,	:
papeže	papež	k1gMnSc2	papež
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
během	během	k7c2	během
schizmatu	schizma	k1gNnSc2	schizma
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
vědomy	vědom	k2eAgFnPc1d1	vědoma
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
sporu	spor	k1gInSc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katolickém	katolický	k2eAgNnSc6d1	katolické
kanonickém	kanonický	k2eAgNnSc6d1	kanonické
právu	právo	k1gNnSc6	právo
je	být	k5eAaImIp3nS	být
schisma	schisma	k1gFnSc1	schisma
<g/>
,	,	kIx,	,
odpadnutí	odpadnutí	k1gNnSc1	odpadnutí
nebo	nebo	k8xC	nebo
hereze	hereze	k1gFnSc1	hereze
trestáno	trestán	k2eAgNnSc1d1	trestáno
vyloučením	vyloučení	k1gNnSc7	vyloučení
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
exkomunikací	exkomunikace	k1gFnPc2	exkomunikace
z	z	k7c2	z
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozkol	rozkol	k1gInSc1	rozkol
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
církvi	církev	k1gFnSc6	církev
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
po	po	k7c6	po
reformách	reforma	k1gFnPc6	reforma
partiarchy	partiarcha	k1gMnSc2	partiarcha
Nikona	Nikon	k1gMnSc2	Nikon
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odštěpení	odštěpení	k1gNnSc3	odštěpení
tzv.	tzv.	kA	tzv.
starověrců	starověrec	k1gMnPc2	starověrec
(	(	kIx(	(
<g/>
zvaní	zvaní	k1gNnPc2	zvaní
též	též	k9	též
rozkolníci	rozkolník	k1gMnPc1	rozkolník
<g/>
)	)	kIx)	)
<g/>
Schisma	schisma	k1gFnSc1	schisma
je	být	k5eAaImIp3nS	být
antonymem	antonymum	k1gNnSc7	antonymum
ke	k	k7c3	k
společenství	společenství	k1gNnSc3	společenství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
vztah	vztah	k1gInSc1	vztah
jednoty	jednota	k1gFnSc2	jednota
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
církvi	církev	k1gFnSc6	církev
nebo	nebo	k8xC	nebo
mezi	mezi	k7c7	mezi
církvemi	církev	k1gFnPc7	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Užití	užití	k1gNnPc4	užití
v	v	k7c6	v
islámu	islám	k1gInSc6	islám
==	==	k?	==
</s>
</p>
<p>
<s>
Rozkol	rozkol	k1gInSc1	rozkol
v	v	k7c6	v
islámu	islám	k1gInSc6	islám
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
632	[number]	k4	632
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
ze	z	k7c2	z
sporu	spor	k1gInSc2	spor
o	o	k7c4	o
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
v	v	k7c6	v
muslimském	muslimský	k2eAgInSc6d1	muslimský
Chalífátu	chalífát	k1gInSc6	chalífát
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
ité	ité	k?	ité
a	a	k8xC	a
sunnité	sunnita	k1gMnPc1	sunnita
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
od	od	k7c2	od
Šiítů	Šiít	k1gInPc2	Šiít
odpadli	odpadnout	k5eAaPmAgMnP	odpadnout
Ibadhi	Ibadhe	k1gFnSc4	Ibadhe
<g/>
(	(	kIx(	(
<g/>
Khawarji	Khawarje	k1gFnSc4	Khawarje
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Hereze	hereze	k1gFnSc1	hereze
</s>
</p>
<p>
<s>
Papežské	papežský	k2eAgNnSc1d1	papežské
schizma	schizma	k1gNnSc1	schizma
</s>
</p>
