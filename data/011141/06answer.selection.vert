<s>
Rozsah	rozsah	k1gInSc1	rozsah
sopránu	soprán	k1gInSc2	soprán
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
malé	malý	k2eAgFnPc4d1	malá
a	a	k8xC	a
-	-	kIx~	-
c3	c3	k4	c3
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
áriích	árie	k1gFnPc6	árie
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
tóny	tón	k1gInPc1	tón
vyšší	vysoký	k2eAgInPc1d2	vyšší
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Mozartově	Mozartův	k2eAgFnSc6d1	Mozartova
opeře	opera	k1gFnSc6	opera
Kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
flétna	flétna	k1gFnSc1	flétna
zpívá	zpívat	k5eAaImIp3nS	zpívat
Královna	královna	k1gFnSc1	královna
noci	noc	k1gFnSc2	noc
v	v	k7c6	v
árii	árie	k1gFnSc6	árie
Der	drát	k5eAaImRp2nS	drát
Hölle	Hölle	k1gNnSc4	Hölle
Rache	Rache	k1gInSc1	Rache
kocht	kocht	k1gInSc1	kocht
in	in	k?	in
meinem	meino	k1gNnSc7	meino
Herzen	Herzna	k1gFnPc2	Herzna
až	až	k9	až
tón	tón	k1gInSc4	tón
f	f	k?	f
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
