<s>
Kadmium	kadmium	k1gNnSc1	kadmium
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Cd	cd	kA	cd
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Cadmium	Cadmium	k1gNnSc1	Cadmium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgMnSc1d1	měkký
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
tavitelný	tavitelný	k2eAgInSc1d1	tavitelný
<g/>
,	,	kIx,	,
toxický	toxický	k2eAgInSc1d1	toxický
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
různých	různý	k2eAgFnPc2d1	různá
slitin	slitina	k1gFnPc2	slitina
a	a	k8xC	a
k	k	k7c3	k
povrchové	povrchový	k2eAgFnSc3d1	povrchová
ochraně	ochrana	k1gFnSc3	ochrana
jiných	jiný	k2eAgInPc2d1	jiný
kovů	kov	k1gInPc2	kov
před	před	k7c7	před
korozí	koroze	k1gFnSc7	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
toxicitě	toxicita	k1gFnSc3	toxicita
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
omezováno	omezován	k2eAgNnSc4d1	omezováno
na	na	k7c4	na
nejnutnější	nutný	k2eAgNnSc4d3	nejnutnější
minimum	minimum	k1gNnSc4	minimum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
typický	typický	k2eAgInSc1d1	typický
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
bíle	bíle	k6eAd1	bíle
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c4	pod
0,517	[number]	k4	0,517
K	k	k7c3	k
je	být	k5eAaImIp3nS	být
supravodičem	supravodič	k1gInSc7	supravodič
I	i	k8xC	i
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
německým	německý	k2eAgMnSc7d1	německý
chemikem	chemik	k1gMnSc7	chemik
Friedrichem	Friedrich	k1gMnSc7	Friedrich
Stromeyerem	Stromeyer	k1gMnSc7	Stromeyer
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
přechodné	přechodný	k2eAgInPc4d1	přechodný
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
valenční	valenční	k2eAgInPc1d1	valenční
elektrony	elektron	k1gInPc1	elektron
v	v	k7c6	v
d-sféře	dféra	k1gFnSc6	d-sféra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
téměř	téměř	k6eAd1	téměř
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Cd	cd	kA	cd
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
sloučeniny	sloučenina	k1gFnPc1	sloučenina
Cd	cd	kA	cd
<g/>
+	+	kIx~	+
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
nestálé	stálý	k2eNgFnPc1d1	nestálá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
silných	silný	k2eAgFnPc6d1	silná
minerálních	minerální	k2eAgFnPc6d1	minerální
kyselinách	kyselina	k1gFnPc6	kyselina
je	být	k5eAaImIp3nS	být
kadmium	kadmium	k1gNnSc1	kadmium
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
za	za	k7c2	za
vývoje	vývoj	k1gInSc2	vývoj
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
je	být	k5eAaImIp3nS	být
kovové	kovový	k2eAgNnSc4d1	kovové
kadmium	kadmium	k1gNnSc4	kadmium
stálé	stálý	k2eAgNnSc4d1	stálé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
kyslíku	kyslík	k1gInSc2	kyslík
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jej	on	k3xPp3gInSc4	on
zapálit	zapálit	k5eAaPmF	zapálit
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
kademnatého	kademnatý	k2eAgInSc2d1	kademnatý
CdO	CdO	k1gFnPc7	CdO
<g/>
.	.	kIx.	.
</s>
<s>
Kadmium	kadmium	k1gNnSc1	kadmium
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
těkavých	těkavý	k2eAgFnPc2d1	těkavá
sloučenin	sloučenina	k1gFnPc2	sloučenina
již	již	k6eAd1	již
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
480	[number]	k4	480
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
kadmium	kadmium	k1gNnSc1	kadmium
vzácným	vzácný	k2eAgInSc7d1	vzácný
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
činí	činit	k5eAaImIp3nS	činit
kolem	kolem	k7c2	kolem
0,1	[number]	k4	0,1
<g/>
-	-	kIx~	-
<g/>
0,5	[number]	k4	0,5
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
značně	značně	k6eAd1	značně
nízká	nízký	k2eAgFnSc1d1	nízká
-	-	kIx~	-
0,11	[number]	k4	0,11
mikrogramu	mikrogram	k1gInSc2	mikrogram
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
kadmia	kadmium	k1gNnSc2	kadmium
přibližně	přibližně	k6eAd1	přibližně
36	[number]	k4	36
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
kadmium	kadmium	k1gNnSc1	kadmium
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
příměs	příměs	k1gFnSc1	příměs
rud	ruda	k1gFnPc2	ruda
zinku	zinek	k1gInSc2	zinek
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
olova	olovo	k1gNnPc1	olovo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
také	také	k9	také
společně	společně	k6eAd1	společně
získává	získávat	k5eAaImIp3nS	získávat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
kovů	kov	k1gInPc2	kov
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
poměrně	poměrně	k6eAd1	poměrně
nízkému	nízký	k2eAgInSc3d1	nízký
bodu	bod	k1gInSc3	bod
varu	var	k1gInSc2	var
požívá	požívat	k5eAaImIp3nS	požívat
destilace	destilace	k1gFnSc1	destilace
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
západoevropských	západoevropský	k2eAgFnPc6d1	západoevropská
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
přibližně	přibližně	k6eAd1	přibližně
350	[number]	k4	350
t	t	k?	t
ročně	ročně	k6eAd1	ročně
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc4	oblast
zvláště	zvláště	k9	zvláště
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
tímto	tento	k3xDgInSc7	tento
kovem	kov	k1gInSc7	kov
jsou	být	k5eAaImIp3nP	být
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
prokázané	prokázaný	k2eAgFnSc3d1	prokázaná
toxicitě	toxicita	k1gFnSc3	toxicita
kadmia	kadmium	k1gNnSc2	kadmium
převládá	převládat	k5eAaImIp3nS	převládat
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
tendence	tendence	k1gFnSc2	tendence
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
nahrazování	nahrazování	k1gNnSc1	nahrazování
jinými	jiný	k2eAgInPc7d1	jiný
kovy	kov	k1gInPc7	kov
všude	všude	k6eAd1	všude
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
technicky	technicky	k6eAd1	technicky
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Pokrytí	pokrytí	k1gNnSc1	pokrytí
povrchu	povrch	k1gInSc2	povrch
jiného	jiný	k2eAgInSc2d1	jiný
kovu	kov	k1gInSc2	kov
kadmiem	kadmium	k1gNnSc7	kadmium
bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
používáno	používat	k5eAaImNgNnS	používat
jako	jako	k8xC	jako
antikorozní	antikorozní	k2eAgFnSc1d1	antikorozní
ochrana	ochrana	k1gFnSc1	ochrana
především	především	k9	především
pro	pro	k7c4	pro
železo	železo	k1gNnSc4	železo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
slitiny	slitina	k1gFnPc4	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Galvanické	galvanický	k2eAgNnSc4d1	galvanické
kadmiování	kadmiování	k1gNnSc4	kadmiování
různých	různý	k2eAgInPc2d1	různý
pracovních	pracovní	k2eAgInPc2d1	pracovní
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
železných	železný	k2eAgFnPc2d1	železná
součástek	součástka	k1gFnPc2	součástka
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
vysoce	vysoce	k6eAd1	vysoce
účinná	účinný	k2eAgFnSc1d1	účinná
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
atmosférickou	atmosférický	k2eAgFnSc7d1	atmosférická
korozí	koroze	k1gFnSc7	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgNnSc1d1	významné
využití	využití	k1gNnSc1	využití
nachází	nacházet	k5eAaImIp3nS	nacházet
kadmium	kadmium	k1gNnSc4	kadmium
doposud	doposud	k6eAd1	doposud
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
pájek	pájka	k1gFnPc2	pájka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
o	o	k7c4	o
slitiny	slitina	k1gFnPc4	slitina
kadmia	kadmium	k1gNnSc2	kadmium
se	s	k7c7	s
stříbrem	stříbro	k1gNnSc7	stříbro
<g/>
,	,	kIx,	,
cínem	cín	k1gInSc7	cín
a	a	k8xC	a
zinkem	zinek	k1gInSc7	zinek
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgFnPc1d1	dobrá
mechanické	mechanický	k2eAgFnPc1d1	mechanická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
-	-	kIx~	-
pevnost	pevnost	k1gFnSc1	pevnost
a	a	k8xC	a
houževnatost	houževnatost	k1gFnSc1	houževnatost
sváru	svár	k1gInSc2	svár
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
vedou	vést	k5eAaImIp3nP	vést
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
přes	přes	k7c4	přes
nepříznivé	příznivý	k2eNgInPc4d1	nepříznivý
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
účinky	účinek	k1gInPc4	účinek
kadmia	kadmium	k1gNnSc2	kadmium
stále	stále	k6eAd1	stále
hojně	hojně	k6eAd1	hojně
využívány	využívat	k5eAaImNgInP	využívat
v	v	k7c6	v
elektronickém	elektronický	k2eAgInSc6d1	elektronický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
legislativa	legislativa	k1gFnSc1	legislativa
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
zakáže	zakázat	k5eAaPmIp3nS	zakázat
kompletně	kompletně	k6eAd1	kompletně
používání	používání	k1gNnSc1	používání
pájek	pájka	k1gFnPc2	pájka
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
kadmia	kadmium	k1gNnSc2	kadmium
v	v	k7c6	v
elektrotechnické	elektrotechnický	k2eAgFnSc6d1	elektrotechnická
výrobě	výroba	k1gFnSc6	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Direktiva	direktiva	k1gFnSc1	direktiva
unie	unie	k1gFnSc1	unie
RoHS	RoHS	k1gFnSc1	RoHS
(	(	kIx(	(
<g/>
Restriction	Restriction	k1gInSc1	Restriction
of	of	k?	of
Hazardous	Hazardous	k1gInSc1	Hazardous
Substances	Substances	k1gInSc1	Substances
<g/>
)	)	kIx)	)
použití	použití	k1gNnSc1	použití
kadmia	kadmium	k1gNnSc2	kadmium
v	v	k7c6	v
elektroprůmyslu	elektroprůmysl	k1gInSc6	elektroprůmysl
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
<g/>
.	.	kIx.	.
</s>
<s>
Kadmium	kadmium	k1gNnSc1	kadmium
je	být	k5eAaImIp3nS	být
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
nikl-kadmiových	nikladmiový	k2eAgInPc2d1	nikl-kadmiový
akumulátorů	akumulátor	k1gInPc2	akumulátor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
zápornou	záporný	k2eAgFnSc4d1	záporná
elektrodu	elektroda	k1gFnSc4	elektroda
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sloučenin	sloučenina	k1gFnPc2	sloučenina
kadmia	kadmium	k1gNnSc2	kadmium
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
sulfid	sulfid	k1gInSc1	sulfid
kademnatý	kademnatý	k2eAgInSc1d1	kademnatý
CdS	CdS	k1gFnSc3	CdS
<g/>
,	,	kIx,	,
intenzivně	intenzivně	k6eAd1	intenzivně
žlutá	žlutý	k2eAgFnSc1d1	žlutá
sloučenina	sloučenina	k1gFnSc1	sloučenina
slouží	sloužit	k5eAaImIp3nS	sloužit
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
malířských	malířský	k2eAgInPc2d1	malířský
pigmentů	pigment	k1gInPc2	pigment
jako	jako	k8xC	jako
kadmiová	kadmiový	k2eAgFnSc1d1	kadmiová
žluť	žluť	k1gFnSc1	žluť
<g/>
.	.	kIx.	.
</s>
<s>
Uplatnění	uplatnění	k1gNnSc1	uplatnění
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
CRT	CRT	kA	CRT
televizních	televizní	k2eAgFnPc2d1	televizní
obrazovek	obrazovka	k1gFnPc2	obrazovka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
luminoforů	luminofor	k1gInPc2	luminofor
v	v	k7c6	v
množstvích	množství	k1gNnPc6	množství
do	do	k7c2	do
5	[number]	k4	5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Kadmium	kadmium	k1gNnSc1	kadmium
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
lidského	lidský	k2eAgInSc2d1	lidský
organismu	organismus	k1gInSc2	organismus
je	být	k5eAaImIp3nS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
kuriózní	kuriózní	k2eAgFnSc1d1	kuriózní
například	například	k6eAd1	například
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
chemicky	chemicky	k6eAd1	chemicky
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgInPc1d1	podobný
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
nezbytnou	nezbytný	k2eAgFnSc7d1	nezbytná
součástí	součást	k1gFnSc7	součást
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
správný	správný	k2eAgInSc4d1	správný
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
lidského	lidský	k2eAgInSc2d1	lidský
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
chemická	chemický	k2eAgFnSc1d1	chemická
podobnost	podobnost	k1gFnSc1	podobnost
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
však	však	k9	však
působí	působit	k5eAaImIp3nS	působit
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kadmium	kadmium	k1gNnSc1	kadmium
může	moct	k5eAaImIp3nS	moct
snadno	snadno	k6eAd1	snadno
vstupovat	vstupovat	k5eAaImF	vstupovat
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
enzymatických	enzymatický	k2eAgFnPc2d1	enzymatická
reakcí	reakce	k1gFnPc2	reakce
místo	místo	k7c2	místo
zinku	zinek	k1gInSc2	zinek
a	a	k8xC	a
následné	následný	k2eAgInPc1d1	následný
biochemické	biochemický	k2eAgInPc1d1	biochemický
pochody	pochod	k1gInPc1	pochod
neproběhnou	proběhnout	k5eNaPmIp3nP	proběhnout
nebo	nebo	k8xC	nebo
probíhají	probíhat	k5eAaImIp3nP	probíhat
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
zablokování	zablokování	k1gNnSc4	zablokování
inzulínového	inzulínový	k2eAgInSc2d1	inzulínový
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
vážné	vážný	k2eAgFnPc4d1	vážná
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
komplikace	komplikace	k1gFnPc4	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
kumulace	kumulace	k1gFnSc1	kumulace
kadmia	kadmium	k1gNnSc2	kadmium
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
prostatě	prostata	k1gFnSc6	prostata
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
a	a	k8xC	a
toto	tento	k3xDgNnSc4	tento
kumulované	kumulovaný	k2eAgNnSc4d1	kumulované
kadmium	kadmium	k1gNnSc4	kadmium
zde	zde	k6eAd1	zde
může	moct	k5eAaImIp3nS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
velice	velice	k6eAd1	velice
rozšířenou	rozšířený	k2eAgFnSc4d1	rozšířená
rakovinu	rakovina	k1gFnSc4	rakovina
prostaty	prostata	k1gFnSc2	prostata
s	s	k7c7	s
následnými	následný	k2eAgFnPc7d1	následná
metastázami	metastáza	k1gFnPc7	metastáza
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
rizikovým	rizikový	k2eAgInSc7d1	rizikový
faktorem	faktor	k1gInSc7	faktor
u	u	k7c2	u
kadmia	kadmium	k1gNnSc2	kadmium
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
mimořádně	mimořádně	k6eAd1	mimořádně
kumulativní	kumulativní	k2eAgInSc4d1	kumulativní
jed	jed	k1gInSc4	jed
<g/>
.	.	kIx.	.
</s>
<s>
Přijaté	přijatý	k2eAgNnSc1d1	přijaté
kadmium	kadmium	k1gNnSc1	kadmium
se	se	k3xPyFc4	se
z	z	k7c2	z
organizmu	organizmus	k1gInSc2	organizmus
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
pozvolna	pozvolna	k6eAd1	pozvolna
a	a	k8xC	a
obtížně	obtížně	k6eAd1	obtížně
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
koncentruje	koncentrovat	k5eAaBmIp3nS	koncentrovat
především	především	k9	především
v	v	k7c6	v
ledvinách	ledvina	k1gFnPc6	ledvina
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
i	i	k8xC	i
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kadmium	kadmium	k1gNnSc1	kadmium
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
ledvinách	ledvina	k1gFnPc6	ledvina
setrvat	setrvat	k5eAaPmF	setrvat
až	až	k6eAd1	až
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
chronické	chronický	k2eAgFnSc6d1	chronická
otravě	otrava	k1gFnSc6	otrava
kadmiem	kadmium	k1gNnSc7	kadmium
nejvíce	hodně	k6eAd3	hodně
ohroženy	ohrozit	k5eAaPmNgFnP	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
zdravotními	zdravotní	k2eAgInPc7d1	zdravotní
projevy	projev	k1gInPc7	projev
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
(	(	kIx(	(
<g/>
chronické	chronický	k2eAgFnSc2d1	chronická
<g/>
)	)	kIx)	)
otravy	otrava	k1gFnSc2	otrava
kadmiem	kadmium	k1gNnSc7	kadmium
jsou	být	k5eAaImIp3nP	být
kromě	kromě	k7c2	kromě
poškození	poškození	k1gNnSc2	poškození
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
jater	játra	k1gNnPc2	játra
také	také	k9	také
osteoporóza	osteoporóza	k1gFnSc1	osteoporóza
-	-	kIx~	-
lidově	lidově	k6eAd1	lidově
řídnutí	řídnutí	k1gNnSc1	řídnutí
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
anémie	anémie	k1gFnSc1	anémie
neboli	neboli	k8xC	neboli
chudokrevnost	chudokrevnost	k1gFnSc1	chudokrevnost
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
i	i	k9	i
riziko	riziko	k1gNnSc1	riziko
srdečních	srdeční	k2eAgNnPc2d1	srdeční
a	a	k8xC	a
cévních	cévní	k2eAgNnPc2d1	cévní
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
obsah	obsah	k1gInSc1	obsah
kadmia	kadmium	k1gNnSc2	kadmium
totiž	totiž	k9	totiž
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
metabolismus	metabolismus	k1gInSc4	metabolismus
vápníku	vápník	k1gInSc2	vápník
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
jeho	jeho	k3xOp3gNnSc4	jeho
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
vylučování	vylučování	k1gNnSc4	vylučování
z	z	k7c2	z
organizmu	organizmus	k1gInSc2	organizmus
s	s	k7c7	s
následkem	následek	k1gInSc7	následek
zeslabení	zeslabení	k1gNnSc2	zeslabení
kostní	kostní	k2eAgFnSc2d1	kostní
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Kadmium	kadmium	k1gNnSc1	kadmium
je	být	k5eAaImIp3nS	být
také	také	k9	také
prokazatelně	prokazatelně	k6eAd1	prokazatelně
karcinogenní	karcinogenní	k2eAgInSc1d1	karcinogenní
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
rakovinného	rakovinný	k2eAgNnSc2d1	rakovinné
bujení	bujení	k1gNnSc2	bujení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jednorázové	jednorázový	k2eAgFnSc6d1	jednorázová
vysoké	vysoký	k2eAgFnSc6d1	vysoká
dávce	dávka	k1gFnSc6	dávka
kadmia	kadmium	k1gNnSc2	kadmium
se	se	k3xPyFc4	se
dostavují	dostavovat	k5eAaImIp3nP	dostavovat
bolesti	bolest	k1gFnPc1	bolest
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
průjmy	průjem	k1gInPc7	průjem
a	a	k8xC	a
zvracení	zvracení	k1gNnPc2	zvracení
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
se	se	k3xPyFc4	se
kadmium	kadmium	k1gNnSc1	kadmium
dostává	dostávat	k5eAaImIp3nS	dostávat
dvěma	dva	k4xCgFnPc7	dva
cestami	cesta	k1gFnPc7	cesta
-	-	kIx~	-
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
a	a	k8xC	a
dýcháním	dýchání	k1gNnSc7	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
potravin	potravina	k1gFnPc2	potravina
jsou	být	k5eAaImIp3nP	být
rizikovým	rizikový	k2eAgInSc7d1	rizikový
faktorem	faktor	k1gInSc7	faktor
především	především	k9	především
vnitřnosti	vnitřnost	k1gFnPc1	vnitřnost
(	(	kIx(	(
<g/>
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
ledviny	ledvina	k1gFnPc1	ledvina
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
kontaminovány	kontaminován	k2eAgFnPc1d1	kontaminována
kadmiem	kadmium	k1gNnSc7	kadmium
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
růstu	růst	k1gInSc6	růst
<g/>
.	.	kIx.	.
</s>
<s>
Rizikové	rizikový	k2eAgInPc1d1	rizikový
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
zemědělské	zemědělský	k2eAgFnPc4d1	zemědělská
plodiny	plodina	k1gFnPc4	plodina
<g/>
,	,	kIx,	,
pěstované	pěstovaný	k2eAgInPc4d1	pěstovaný
na	na	k7c4	na
kadmiem	kadmium	k1gNnSc7	kadmium
kontaminované	kontaminovaný	k2eAgFnSc6d1	kontaminovaná
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nízkému	nízký	k2eAgInSc3d1	nízký
bodu	bod	k1gInSc3	bod
varu	var	k1gInSc2	var
kadmia	kadmium	k1gNnSc2	kadmium
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
prvek	prvek	k1gInSc1	prvek
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
hutní	hutní	k2eAgInPc1d1	hutní
provozy	provoz	k1gInPc1	provoz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
s	s	k7c7	s
kadmiem	kadmium	k1gNnSc7	kadmium
pracují	pracovat	k5eAaImIp3nP	pracovat
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
důsledně	důsledně	k6eAd1	důsledně
dbaly	dbát	k5eAaImAgInP	dbát
o	o	k7c4	o
dokonalé	dokonalý	k2eAgNnSc4d1	dokonalé
čištění	čištění	k1gNnSc4	čištění
plynných	plynný	k2eAgFnPc2d1	plynná
exhalací	exhalace	k1gFnPc2	exhalace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
odcházejí	odcházet	k5eAaImIp3nP	odcházet
<g/>
.	.	kIx.	.
</s>
<s>
Ohroženi	ohrožen	k2eAgMnPc1d1	ohrožen
totiž	totiž	k9	totiž
nejsou	být	k5eNaImIp3nP	být
pouze	pouze	k6eAd1	pouze
přímo	přímo	k6eAd1	přímo
pracovníci	pracovník	k1gMnPc1	pracovník
v	v	k7c6	v
uvedených	uvedený	k2eAgInPc6d1	uvedený
provozech	provoz	k1gInPc6	provoz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kadmium	kadmium	k1gNnSc4	kadmium
nasorbované	nasorbovaný	k2eAgNnSc4d1	nasorbovaný
na	na	k7c4	na
prachové	prachový	k2eAgFnPc4d1	prachová
částice	částice	k1gFnPc4	částice
a	a	k8xC	a
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
aerosol	aerosol	k1gInSc4	aerosol
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
větrem	vítr	k1gInSc7	vítr
transportováno	transportován	k2eAgNnSc1d1	transportováno
na	na	k7c4	na
značně	značně	k6eAd1	značně
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejohroženější	ohrožený	k2eAgFnSc4d3	nejohroženější
skupinu	skupina	k1gFnSc4	skupina
osob	osoba	k1gFnPc2	osoba
však	však	k9	však
tvoří	tvořit	k5eAaImIp3nP	tvořit
kuřáci	kuřák	k1gMnPc1	kuřák
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
náhodně	náhodně	k6eAd1	náhodně
vybraném	vybraný	k2eAgInSc6d1	vybraný
vzorku	vzorek	k1gInSc6	vzorek
populace	populace	k1gFnSc2	populace
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
ledviny	ledvina	k1gFnPc1	ledvina
silného	silný	k2eAgMnSc2d1	silný
kuřáka	kuřák	k1gMnSc2	kuřák
minimálně	minimálně	k6eAd1	minimálně
10	[number]	k4	10
<g/>
×	×	k?	×
více	hodně	k6eAd2	hodně
kadmia	kadmium	k1gNnSc2	kadmium
než	než	k8xS	než
u	u	k7c2	u
nekuřáka	nekuřák	k1gMnSc2	nekuřák
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgInPc2d1	uvedený
faktů	fakt	k1gInPc2	fakt
pak	pak	k6eAd1	pak
jasně	jasně	k6eAd1	jasně
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kuřák	kuřák	k1gInSc1	kuřák
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
běžně	běžně	k6eAd1	běžně
uváděné	uváděný	k2eAgFnSc2d1	uváděná
rakoviny	rakovina	k1gFnSc2	rakovina
plic	plíce	k1gFnPc2	plíce
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
i	i	k9	i
rakovinou	rakovina	k1gFnSc7	rakovina
nebo	nebo	k8xC	nebo
chronickým	chronický	k2eAgNnSc7d1	chronické
selháním	selhání	k1gNnSc7	selhání
činnosti	činnost	k1gFnSc2	činnost
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Kadmium	kadmium	k1gNnSc1	kadmium
přijímané	přijímaný	k2eAgNnSc1d1	přijímané
potravou	potrava	k1gFnSc7	potrava
se	se	k3xPyFc4	se
vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
asi	asi	k9	asi
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
kolem	kolem	k7c2	kolem
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ukládá	ukládat	k5eAaImIp3nS	ukládat
se	se	k3xPyFc4	se
v	v	k7c6	v
ledvinách	ledvina	k1gFnPc6	ledvina
a	a	k8xC	a
játrech	játra	k1gNnPc6	játra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
protein	protein	k1gInSc4	protein
metalothionein	metalothionein	k2eAgInSc4d1	metalothionein
<g/>
.	.	kIx.	.
</s>
<s>
Poločas	poločas	k1gInSc1	poločas
jeho	on	k3xPp3gNnSc2	on
vylučování	vylučování	k1gNnSc2	vylučování
z	z	k7c2	z
organismu	organismus	k1gInSc2	organismus
je	být	k5eAaImIp3nS	být
až	až	k9	až
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kadmium	kadmium	k1gNnSc1	kadmium
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
funkci	funkce	k1gFnSc4	funkce
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
dlouhotrvajícím	dlouhotrvající	k2eAgInSc6d1	dlouhotrvající
příjmu	příjem	k1gInSc6	příjem
malých	malý	k2eAgNnPc2d1	malé
množství	množství	k1gNnSc2	množství
kadmia	kadmium	k1gNnSc2	kadmium
dochází	docházet	k5eAaImIp3nS	docházet
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
kolem	kolem	k7c2	kolem
50	[number]	k4	50
let	léto	k1gNnPc2	léto
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moči	moč	k1gFnSc6	moč
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
malé	malý	k2eAgInPc1d1	malý
peptidy	peptid	k1gInPc1	peptid
a	a	k8xC	a
cukr	cukr	k1gInSc1	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Nejvážnějším	vážní	k2eAgInSc7d3	nejvážnější
účinkem	účinek	k1gInSc7	účinek
kadmia	kadmium	k1gNnSc2	kadmium
je	být	k5eAaImIp3nS	být
ohrožení	ohrožení	k1gNnSc4	ohrožení
reprodukčních	reprodukční	k2eAgInPc2d1	reprodukční
orgánů	orgán	k1gInPc2	orgán
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kadmium	kadmium	k1gNnSc1	kadmium
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
funkčnost	funkčnost	k1gFnSc4	funkčnost
a	a	k8xC	a
kvalitu	kvalita	k1gFnSc4	kvalita
spermií	spermie	k1gFnPc2	spermie
<g/>
.	.	kIx.	.
</s>
<s>
Poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
zárodečný	zárodečný	k2eAgInSc1d1	zárodečný
epitel	epitel	k1gInSc1	epitel
varlat	varle	k1gNnPc2	varle
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
poševních	poševní	k2eAgInPc6d1	poševní
hlenech	hlen	k1gInPc6	hlen
<g/>
.	.	kIx.	.
</s>
<s>
Negativně	negativně	k6eAd1	negativně
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
na	na	k7c4	na
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
karcinogenní	karcinogenní	k2eAgInPc4d1	karcinogenní
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Porušuje	porušovat	k5eAaImIp3nS	porušovat
metabolismus	metabolismus	k1gInSc4	metabolismus
vápníku	vápník	k1gInSc2	vápník
-	-	kIx~	-
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
měknutí	měknutí	k1gNnSc1	měknutí
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
vypadávání	vypadávání	k1gNnPc2	vypadávání
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Užívání	užívání	k1gNnSc1	užívání
kadmia	kadmium	k1gNnSc2	kadmium
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
omezeno	omezit	k5eAaPmNgNnS	omezit
směrnice	směrnice	k1gFnSc2	směrnice
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
95	[number]	k4	95
<g/>
/	/	kIx~	/
<g/>
ES	ES	kA	ES
Restriction	Restriction	k1gInSc1	Restriction
of	of	k?	of
the	the	k?	the
use	usus	k1gInSc5	usus
of	of	k?	of
certain	certain	k2eAgMnSc1d1	certain
Hazardous	Hazardous	k1gMnSc1	Hazardous
Substances	Substances	k1gMnSc1	Substances
in	in	k?	in
electrical	electricat	k5eAaPmAgMnS	electricat
and	and	k?	and
electronic	electronice	k1gFnPc2	electronice
equipment	equipment	k1gMnSc1	equipment
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
Omezení	omezení	k1gNnSc1	omezení
užívání	užívání	k1gNnSc2	užívání
některých	některý	k3yIgFnPc2	některý
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
elektronických	elektronický	k2eAgInPc6d1	elektronický
a	a	k8xC	a
elektrických	elektrický	k2eAgNnPc6d1	elektrické
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
a	a	k8xC	a
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Omezení	omezení	k1gNnSc1	omezení
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
netýká	týkat	k5eNaImIp3nS	týkat
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
<g/>
,	,	kIx,	,
telekomunikačních	telekomunikační	k2eAgInPc2d1	telekomunikační
<g/>
,	,	kIx,	,
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
<g/>
,	,	kIx,	,
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
zařízení	zařízení	k1gNnPc2	zařízení
s	s	k7c7	s
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
životností	životnost	k1gFnSc7	životnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
v	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
množstvích	množství	k1gNnPc6	množství
a	a	k8xC	a
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
neskončí	skončit	k5eNaPmIp3nS	skončit
na	na	k7c6	na
skládce	skládka	k1gFnSc6	skládka
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc1	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bencko	Bencko	k1gNnSc1	Bencko
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Cikrt	Cikrt	k1gInSc1	Cikrt
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Lener	Lenra	k1gFnPc2	Lenra
<g/>
:	:	kIx,	:
Toxické	toxický	k2eAgInPc1d1	toxický
kovy	kov	k1gInPc1	kov
v	v	k7c6	v
životním	životní	k2eAgNnSc6d1	životní
a	a	k8xC	a
pracovním	pracovní	k2eAgNnSc6d1	pracovní
prostředí	prostředí	k1gNnSc6	prostředí
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
Grada	Grada	k1gFnSc1	Grada
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7169-150-X	[number]	k4	80-7169-150-X
Handbook	handbook	k1gInSc4	handbook
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Toxicology	Toxicolog	k1gMnPc4	Toxicolog
of	of	k?	of
Metals	Metalsa	k1gFnPc2	Metalsa
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
J.	J.	kA	J.
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Životní	životní	k2eAgNnSc1d1	životní
prostředí	prostředí	k1gNnSc1	prostředí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ročenka	ročenka	k1gFnSc1	ročenka
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
MŽP	MŽP	kA	MŽP
ČR	ČR	kA	ČR
a	a	k8xC	a
ČEÚ	ČEÚ	kA	ČEÚ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
J.	J.	kA	J.
Píša	Píša	k1gMnSc1	Píša
<g/>
:	:	kIx,	:
Narušení	narušení	k1gNnSc1	narušení
reprodukčních	reprodukční	k2eAgInPc2d1	reprodukční
procesů	proces	k1gInPc2	proces
působením	působení	k1gNnSc7	působení
kadmia	kadmium	k1gNnSc2	kadmium
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
a	a	k8xC	a
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
.	.	kIx.	.
in	in	k?	in
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Cibulka	Cibulka	k1gMnSc1	Cibulka
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
kadmia	kadmium	k1gNnSc2	kadmium
a	a	k8xC	a
rtuti	rtuť	k1gFnSc2	rtuť
v	v	k7c6	v
biosféře	biosféra	k1gFnSc6	biosféra
<g/>
.	.	kIx.	.
</s>
<s>
Akademia	Akademia	k1gFnSc1	Akademia
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kadmium	kadmium	k1gNnSc4	kadmium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kadmium	kadmium	k1gNnSc4	kadmium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
