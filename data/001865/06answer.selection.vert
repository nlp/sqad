<s>
Studiem	studio	k1gNnSc7	studio
mechanismů	mechanismus	k1gInPc2	mechanismus
a	a	k8xC	a
zákonitostí	zákonitost	k1gFnPc2	zákonitost
evoluce	evoluce	k1gFnSc2	evoluce
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
evoluční	evoluční	k2eAgFnSc2d1	evoluční
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
evolučními	evoluční	k2eAgInPc7d1	evoluční
vztahy	vztah	k1gInPc7	vztah
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
organismy	organismus	k1gInPc7	organismus
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
fylogenetika	fylogenetika	k1gFnSc1	fylogenetika
<g/>
.	.	kIx.	.
</s>
