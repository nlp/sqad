<s>
Oscar	Oscar	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
</s>
<s>
Oscar	Oscar	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Oscar	Oscar	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
August	August	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1849	#num#	k4
<g/>
Friedberg	Friedberg	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1922	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
73	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Berlín	Berlín	k1gInSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Grunewald	Grunewald	k6eAd1
Cemetery	Cemeter	k1gMnPc7
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Jena	Jen	k1gInSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
zoolog	zoolog	k1gMnSc1
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
anatom	anatom	k1gMnSc1
a	a	k8xC
biolog	biolog	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Friedricha	Friedrich	k1gMnSc2
Wilhelmse	Wilhelms	k1gMnSc2
v	v	k7c6
Berlíně	Berlín	k1gInSc6
(	(	kIx(
<g/>
od	od	k7c2
1888	#num#	k4
<g/>
)	)	kIx)
<g/>
Univerzita	univerzita	k1gFnSc1
JenaHumboldtova	JenaHumboldtův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Paula	Paula	k1gFnSc1
Hertwig	Hertwiga	k1gFnPc2
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Richard	Richard	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1
Oscara	Oscar	k1gMnSc2
Hertwiga	Hertwig	k1gMnSc2
z	z	k7c2
knihy	kniha	k1gFnSc2
Lehrbuch	Lehrbucha	k1gFnPc2
der	drát	k5eAaImRp2nS
Entwicklungsgeschichte	Entwicklungsgeschicht	k1gInSc5
des	des	k1gNnPc2
Menschen	Menschno	k1gNnPc2
und	und	k?
der	drát	k5eAaImRp2nS
Wirbeltiere	Wirbeltier	k1gMnSc5
(	(	kIx(
<g/>
Učebnice	učebnice	k1gFnSc1
z	z	k7c2
vývojové	vývojový	k2eAgFnSc2d1
biologie	biologie	k1gFnSc2
člověka	člověk	k1gMnSc2
a	a	k8xC
obratlovců	obratlovec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1906	#num#	k4
</s>
<s>
Oscar	Oscar	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1849	#num#	k4
<g/>
,	,	kIx,
Friedberg	Friedberg	k1gInSc1
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1922	#num#	k4
<g/>
,	,	kIx,
Berlín	Berlín	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
zoolog	zoolog	k1gMnSc1
a	a	k8xC
profesor	profesor	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgMnS
teorií	teorie	k1gFnSc7
evoluce	evoluce	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
směru	směr	k1gInSc6
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
také	také	k9
řadu	řada	k1gFnSc4
publikací	publikace	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nich	on	k3xPp3gMnPc2
nejvýznamnější	významný	k2eAgInSc1d3
publikoval	publikovat	k5eAaBmAgInS
asi	asi	k9
v	v	k7c6
letech	léto	k1gNnPc6
1916	#num#	k4
<g/>
,	,	kIx,
více	hodně	k6eAd2
než	než	k8xS
55	#num#	k4
let	léto	k1gNnPc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
známý	známý	k2eAgMnSc1d1
evoluční	evoluční	k2eAgMnSc1d1
biolog	biolog	k1gMnSc1
Charles	Charles	k1gMnSc1
Darwin	Darwin	k1gMnSc1
publikoval	publikovat	k5eAaBmAgMnS
svou	svůj	k3xOyFgFnSc4
knihu	kniha	k1gFnSc4
o	o	k7c6
Původu	původ	k1gInSc6
Druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Oscar	Oscar	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
byl	být	k5eAaImAgMnS
starší	starší	k1gMnPc4
z	z	k7c2
bratrů	bratr	k1gMnPc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
sourozenec	sourozenec	k1gMnSc1
(	(	kIx(
<g/>
taktéž	taktéž	k?
zoolog	zoolog	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
profesor	profesor	k1gMnSc1
Richard	Richard	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
(	(	kIx(
<g/>
1850	#num#	k4
<g/>
-	-	kIx~
<g/>
1937	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratři	bratr	k1gMnPc1
Hertwigové	Hertwigový	k2eAgFnSc6d1
byli	být	k5eAaImAgMnP
jedni	jeden	k4xCgMnPc1
z	z	k7c2
nejvýznamnějších	významný	k2eAgMnPc2d3
žáků	žák	k1gMnPc2
Ernsta	Ernst	k1gMnSc2
Haeckela	Haeckel	k1gMnSc2
(	(	kIx(
<g/>
a	a	k8xC
Carla	Carl	k1gMnSc2
Gegenbaura	Gegenbaur	k1gMnSc2
<g/>
)	)	kIx)
z	z	k7c2
University	universita	k1gFnSc2
v	v	k7c6
Jeně	Jena	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
počátku	počátek	k1gInSc2
byli	být	k5eAaImAgMnP
zcela	zcela	k6eAd1
nezávislí	závislý	k2eNgMnPc1d1
na	na	k7c6
Haeckelových	Haeckelův	k2eAgFnPc6d1
filozofických	filozofický	k2eAgFnPc6d1
spekulacích	spekulace	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
převzali	převzít	k5eAaPmAgMnP
jeho	jeho	k3xOp3gFnPc4
myšlenky	myšlenka	k1gFnPc4
v	v	k7c6
pozitivním	pozitivní	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
a	a	k8xC
rozšířili	rozšířit	k5eAaPmAgMnP
jejich	jejich	k3xOp3gInSc4
koncept	koncept	k1gInSc4
v	v	k7c6
zoologii	zoologie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Zpočátku	zpočátku	k6eAd1
<g/>
,	,	kIx,
mezi	mezi	k7c7
lety	let	k1gInPc7
1879	#num#	k4
<g/>
-	-	kIx~
<g/>
1883	#num#	k4
<g/>
,	,	kIx,
prováděli	provádět	k5eAaImAgMnP
embryologické	embryologický	k2eAgFnPc4d1
studie	studie	k1gFnPc4
<g/>
,	,	kIx,
zejména	zejména	k9
o	o	k7c4
teorii	teorie	k1gFnSc4
coelomu	coelom	k1gInSc2
(	(	kIx(
<g/>
1881	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tekutinou	tekutina	k1gFnSc7
naplněné	naplněný	k2eAgNnSc1d1
tělo	tělo	k1gNnSc1
dutiny	dutina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
úvahy	úvaha	k1gFnPc1
byly	být	k5eAaImAgFnP
založeny	založit	k5eAaPmNgFnP
na	na	k7c6
základě	základ	k1gInSc6
Heackelovy	Heackelův	k2eAgFnSc2d1
fylogenetické	fylogenetický	k2eAgFnSc2d1
věty	věta	k1gFnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
biogenní	biogenní	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
=	=	kIx~
biogenetisches	biogenetisches	k1gMnSc1
Grundgesetz	Grundgesetz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
"	"	kIx"
<g/>
teorie	teorie	k1gFnSc1
gastraea	gastraea	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
10	#num#	k4
letech	léto	k1gNnPc6
se	se	k3xPyFc4
oba	dva	k4xCgMnPc1
bratři	bratr	k1gMnPc1
rozdělili	rozdělit	k5eAaPmAgMnP
na	na	k7c4
sever	sever	k1gInSc4
a	a	k8xC
jih	jih	k1gInSc4
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Oscar	Oscar	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stal	stát	k5eAaPmAgMnS
profesorem	profesor	k1gMnSc7
anatomie	anatomie	k1gFnSc2
v	v	k7c6
Berlíně	Berlín	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1888	#num#	k4
<g/>
;	;	kIx,
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Richard	Richard	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
1885	#num#	k4
stal	stát	k5eAaPmAgMnS
profesorem	profesor	k1gMnSc7
zoologie	zoologie	k1gFnSc2
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
(	(	kIx(
<g/>
1885	#num#	k4
<g/>
-	-	kIx~
<g/>
1925	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c4
Ludwig	Ludwig	k1gInSc4
Maximilians	Maximilians	k1gInSc4
Universität	Universitäta	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
působil	působit	k5eAaImAgMnS
posledních	poslední	k2eAgNnPc2d1
40	#num#	k4
let	léto	k1gNnPc2
jeho	jeho	k3xOp3gFnSc2
50	#num#	k4
<g/>
leté	letý	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
jako	jako	k8xC,k8xS
profesor	profesor	k1gMnSc1
na	na	k7c6
čtyřech	čtyři	k4xCgFnPc6
univerzitách	univerzita	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Richardův	Richardův	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
je	být	k5eAaImIp3nS
zaměřen	zaměřit	k5eAaPmNgInS
na	na	k7c4
prvoky	prvok	k1gMnPc4
(	(	kIx(
<g/>
vztah	vztah	k1gInSc1
mezi	mezi	k7c7
jádrem	jádro	k1gNnSc7
a	a	k8xC
plazmou	plazma	k1gFnSc7
tzv	tzv	kA
Kern-Plasma-Relation	Kern-Plasma-Relation	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
i	i	k9
na	na	k7c4
vývojové	vývojový	k2eAgFnPc4d1
a	a	k8xC
fyziologické	fyziologický	k2eAgFnPc4d1
studie	studie	k1gFnPc4
o	o	k7c6
mořských	mořský	k2eAgMnPc6d1
ježcích	ježek	k1gMnPc6
čí	čí	k3xOyQgInPc4,k3xOyRgInPc4
žábách	žába	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
také	také	k9
světově	světově	k6eAd1
uznávané	uznávaný	k2eAgFnPc1d1
učebnice	učebnice	k1gFnPc1
zoologie	zoologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Oscar	Oscar	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
studiem	studio	k1gNnSc7
mořských	mořský	k2eAgMnPc2d1
ježků	ježek	k1gMnPc2
prokázal	prokázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
oplodnění	oplodnění	k1gNnSc3
dochází	docházet	k5eAaImIp3nS
v	v	k7c6
důsledku	důsledek	k1gInSc6
fúze	fúze	k1gFnSc2
spermie	spermie	k1gFnSc2
a	a	k8xC
vaječné	vaječný	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Poznal	poznat	k5eAaPmAgMnS
úlohu	úloha	k1gFnSc4
buněčného	buněčný	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
během	během	k7c2
dědičnosti	dědičnost	k1gFnSc2
a	a	k8xC
redukci	redukce	k1gFnSc4
chromozomů	chromozom	k1gInPc2
během	během	k7c2
meiózy	meióza	k1gFnSc2
<g/>
:	:	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1876	#num#	k4
publikoval	publikovat	k5eAaBmAgMnS
své	svůj	k3xOyFgNnSc4
zjištění	zjištění	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
proces	proces	k1gInSc1
oplodnění	oplodnění	k1gNnSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
pronikání	pronikání	k1gNnSc4
spermie	spermie	k1gFnSc2
do	do	k7c2
vaječné	vaječný	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
zjištění	zjištění	k1gNnSc1
patří	patřit	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
k	k	k7c3
velkým	velký	k2eAgInPc3d1
objevům	objev	k1gInPc3
v	v	k7c6
historii	historie	k1gFnSc6
biologie	biologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oscar	Oscar	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
taktéž	taktéž	k?
experimenty	experiment	k1gInPc1
s	s	k7c7
žabími	žabí	k2eAgNnPc7d1
vejci	vejce	k1gNnPc7
odhalil	odhalit	k5eAaPmAgMnS
„	„	k?
<g/>
pravidlo	pravidlo	k1gNnSc4
dlouhé	dlouhý	k2eAgFnSc2d1
osy	osa	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
nazývané	nazývaný	k2eAgNnSc1d1
Hertwigovo	Hertwigův	k2eAgNnSc1d1
pravidlo	pravidlo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
nejdůležitější	důležitý	k2eAgFnSc1d3
teoretická	teoretický	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
byla	být	k5eAaImAgFnS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Das	Das	k1gFnSc1
Werden	Werdna	k1gFnPc2
der	drát	k5eAaImRp2nS
Organismen	Organismen	k2eAgInSc1d1
<g/>
,	,	kIx,
eine	einat	k5eAaPmIp3nS
Widerlegung	Widerlegung	k1gMnSc1
der	drát	k5eAaImRp2nS
Darwinschen	Darwinschna	k1gFnPc2
Zufallslehre	Zufallslehr	k1gInSc5
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Jena	Jena	k1gMnSc1
<g/>
,	,	kIx,
1916	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Původ	původ	k1gInSc1
Organismů	organismus	k1gInPc2
–	–	k?
Vyvrácení	vyvrácení	k1gNnSc4
Darwinovy	Darwinův	k2eAgFnSc2d1
Teorie	teorie	k1gFnSc2
Šance	šance	k1gFnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hertwig	Hertwig	k1gMnSc1
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
členem	člen	k1gMnSc7
Královské	královský	k2eAgFnSc2d1
švédské	švédský	k2eAgFnSc2d1
Akademie	akademie	k1gFnSc2
Věd	věda	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1903	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Oscar	Oscar	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
je	být	k5eAaImIp3nS
známý	známý	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Oscar	Oscar	k1gMnSc1
Hedwig	Hedwig	k1gMnSc1
v	v	k7c6
knize	kniha	k1gFnSc6
"	"	kIx"
<g/>
Kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
objevil	objevit	k5eAaPmAgMnS
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
<g/>
"	"	kIx"
od	od	k7c2
Davida	David	k1gMnSc2
Ellyarda	Ellyard	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
objevu	objev	k1gInSc2
oplození	oplození	k1gNnSc2
u	u	k7c2
savců	savec	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
vědců	vědec	k1gMnPc2
jež	jenž	k3xRgNnSc1
za	za	k7c7
těmito	tento	k3xDgInPc7
objevy	objev	k1gInPc4
stáli	stát	k5eAaImAgMnP
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
byl	být	k5eAaImAgMnS
Hertwig	Hertwig	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnPc1d1
pracovníci	pracovník	k1gMnPc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zmíněna	zmínit	k5eAaPmNgFnS
v	v	k7c6
knize	kniha	k1gFnSc6
"	"	kIx"
<g/>
Savčí	savčí	k2eAgNnSc1d1
Vajíčko	vajíčko	k1gNnSc1
<g/>
"	"	kIx"
od	od	k7c2
spisovatele	spisovatel	k1gMnSc2
jménem	jméno	k1gNnSc7
Austin	Austin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Práce	práce	k1gFnSc1
</s>
<s>
Die	Die	k?
Elemente	element	k1gInSc5
der	drát	k5eAaImRp2nS
Entwicklungslehre	Entwicklungslehr	k1gInSc5
des	des	k1gNnPc2
Menschen	Menschno	k1gNnPc2
und	und	k?
der	drát	k5eAaImRp2nS
Wirbeltiere	Wirbeltier	k1gMnSc5
:	:	kIx,
Anleitung	Anleitunga	k1gFnPc2
und	und	k?
Repetitorium	repetitorium	k1gNnSc4
für	für	k?
Studierende	Studierend	k1gInSc5
und	und	k?
Lékařů	lékař	k1gMnPc2
.	.	kIx.
</s>
<s desamb="1">
Fischer	Fischer	k1gMnSc1
<g/>
,	,	kIx,
Jena	Jena	k1gMnSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
ed	ed	k?
<g/>
.	.	kIx.
1915	#num#	k4
Digitální	digitální	k2eAgInSc1d1
vydání	vydání	k1gNnSc4
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
Univerzita	univerzita	k1gFnSc1
a	a	k8xC
Státní	státní	k2eAgFnSc1d1
Knihovna	knihovna	k1gFnSc1
v	v	k7c6
Düsseldorfu	Düsseldorf	k1gInSc6
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Oscar	Oscar	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Clift	Cliftum	k1gNnPc2
D	D	kA
<g/>
,	,	kIx,
Schuh	Schuh	k1gInSc1
M.	M.	kA
Restarting	Restarting	k1gInSc1
life	life	k1gInSc1
<g/>
:	:	kIx,
fertilization	fertilization	k1gInSc1
and	and	k?
the	the	k?
transition	transition	k1gInSc1
from	from	k1gMnSc1
meiosis	meiosis	k1gInSc1
to	ten	k3xDgNnSc4
mitosis	mitosis	k1gFnSc1
(	(	kIx(
<g/>
Box	box	k1gInSc1
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Reviews	Reviews	k1gInSc4
Molecular	Molecular	k1gInSc4
Cell	cello	k1gNnPc2
Biology	biolog	k1gMnPc7
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
549	#num#	k4
<g/>
–	–	k?
<g/>
62	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nrm	nrm	k?
<g/>
3643	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
23942453	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Cobb	Cobb	k1gMnSc1
M.	M.	kA
An	An	k1gFnSc1
amazing	amazing	k1gInSc1
10	#num#	k4
years	yearsa	k1gFnPc2
<g/>
:	:	kIx,
the	the	k?
discovery	discovera	k1gFnSc2
of	of	k?
egg	egg	k?
and	and	k?
sperm	sperm	k1gInSc1
in	in	k?
the	the	k?
17	#num#	k4
<g/>
th	th	k?
century	centura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reprod	Reprod	k1gInSc1
Domest	Domest	k1gFnSc4
Anim	animo	k1gNnPc2
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
j	j	k?
<g/>
.1439	.1439	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
531.201	531.201	k4
<g/>
2.021	2.021	k4
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
x.	x.	k?
PMID	PMID	kA
22827343	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
GILL	GILL	kA
<g/>
,	,	kIx,
P.	P.	kA
Nature	Natur	k1gMnSc5
and	and	k?
nurture	nurtur	k1gMnSc5
<g/>
..	..	k?
Med	med	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Aust	Austa	k1gFnPc2
<g/>
..	..	k?
1990	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
386	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
2093819	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Citation	Citation	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
WEINDLING	WEINDLING	kA
<g/>
,	,	kIx,
P.	P.	kA
Social	Social	k1gInSc1
concepts	concepts	k1gInSc1
in	in	k?
anatomy	anatom	k1gMnPc7
<g/>
:	:	kIx,
theories	theories	k1gInSc1
of	of	k?
the	the	k?
cell	cello	k1gNnPc2
state	status	k1gInSc5
of	of	k?
Oscar	Oscar	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
(	(	kIx(
<g/>
1849	#num#	k4
<g/>
–	–	k?
<g/>
1922	#num#	k4
<g/>
)	)	kIx)
and	and	k?
Wilhelm	Wilhelm	k1gMnSc1
Waldeyer	Waldeyer	k1gMnSc1
(	(	kIx(
<g/>
1836	#num#	k4
<g/>
–	–	k?
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
<g/>
..	..	k?
The	The	k1gMnSc1
Society	societa	k1gFnSc2
for	forum	k1gNnPc2
the	the	k?
Social	Social	k1gInSc1
History	Histor	k1gInPc1
of	of	k?
Medicine	Medicin	k1gInSc5
bulletin	bulletin	k1gInSc1
<g/>
.	.	kIx.
1980	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
15	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
11610800	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Citation	Citation	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
GRAS	GRAS	kA
<g/>
,	,	kIx,
N	N	kA
<g/>
;	;	kIx,
VERCHERE	VERCHERE	kA
<g/>
,	,	kIx,
M	M	kA
<g/>
;	;	kIx,
SANTORO	SANTORO	kA
<g/>
,	,	kIx,
J	J	kA
P.	P.	kA
[	[	kIx(
<g/>
The	The	k1gMnSc1
Oscar	Oscar	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
centenary	centenara	k1gFnSc2
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revue	revue	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
odonto-stomatologie	odonto-stomatologie	k1gFnSc1
<g/>
.	.	kIx.
1975	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
135	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
1103253	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Citation	Citation	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
CHURCHILL	CHURCHILL	kA
<g/>
,	,	kIx,
F	F	kA
B.	B.	kA
Hertwig	Hertwig	k1gMnSc1
<g/>
,	,	kIx,
Weismann	Weismann	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
the	the	k?
meaning	meaning	k1gInSc1
of	of	k?
reduction	reduction	k1gInSc1
division	division	k1gInSc1
circa	circa	k1gFnSc1
1890	#num#	k4
<g/>
..	..	k?
Isis	Isis	k1gFnSc1
<g/>
;	;	kIx,
an	an	k?
international	internationat	k5eAaImAgMnS,k5eAaPmAgMnS
review	review	k?
devoted	devoted	k1gMnSc1
to	ten	k3xDgNnSc1
the	the	k?
history	histor	k1gInPc1
of	of	k?
science	scienec	k1gInSc2
and	and	k?
its	its	k?
cultural	culturat	k5eAaPmAgMnS,k5eAaImAgMnS
influences	influences	k1gMnSc1
<g/>
.	.	kIx.
1970	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
429	#num#	k4
<g/>
–	–	k?
<g/>
57	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
350680	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
4942056	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Citation	Citation	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Cremer	Cremer	k1gMnSc1
<g/>
,	,	kIx,
T.	T.	kA
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Von	von	k1gInSc1
der	drát	k5eAaImRp2nS
Zellenlehre	Zellenlehr	k1gInSc5
zur	zur	k?
Chromosomentheorie	Chromosomentheorie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Springer	Springer	k1gMnSc1
Vlg	Vlg	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Heidelberg	Heidelberg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
německý	německý	k2eAgMnSc1d1
knihu	kniha	k1gFnSc4
si	se	k3xPyFc3
můžete	moct	k5eAaImIp2nP
stáhnout	stáhnout	k5eAaPmF
zde	zde	k6eAd1
.	.	kIx.
</s>
<s>
Krafft	Krafft	k1gMnSc1
<g/>
,	,	kIx,
F.	F.	kA
<g/>
,	,	kIx,
a.	a.	k?
A.	A.	kA
Meyer-Abich	Meyer-Abich	k1gInSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
1970	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Große	Groß	k1gInSc2
Naturwissenschaftler	Naturwissenschaftler	k1gInSc1
–	–	k?
Biographisches	Biographisches	k1gInSc1
Lexikon	lexikon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fischer	Fischer	k1gMnSc1
Bücherei	Bücheree	k1gFnSc4
GmbH	GmbH	k1gFnPc2
<g/>
,	,	kIx,
Frankfurt	Frankfurt	k1gInSc1
nad	nad	k7c7
mohanem	mohan	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
M	M	kA
&	&	k?
Hamburg	Hamburg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Mol	mol	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mobil	mobil	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biol.	Biol.	k1gFnSc1
<g/>
-přednáška	-přednáška	k1gFnSc1
<g/>
,	,	kIx,
Heidelberg	Heidelberg	k1gInSc1
<g/>
,	,	kIx,
D.	D.	kA
<g/>
-	-	kIx~
<g/>
H.	H.	kA
Lankenau	Lankenaus	k1gInSc2
<g/>
.	.	kIx.
<g/>
Brzy	brzy	k6eAd1
na	na	k7c4
nedávné	dávný	k2eNgInPc4d1
klíčové	klíčový	k2eAgInPc4d1
objevy	objev	k1gInPc4
<g/>
:	:	kIx,
Od	od	k7c2
zárodečné	zárodečný	k2eAgFnSc2d1
linie	linie	k1gFnSc2
Teorie	teorie	k1gFnSc1
Moderní	moderní	k2eAgFnSc1d1
Genové	genový	k2eAgFnPc4d1
Modifikace	modifikace	k1gFnPc4
</s>
<s>
Weindling	Weindling	k1gInSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
.	.	kIx.
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Darwinismus	darwinismus	k1gInSc4
a	a	k8xC
Sociální	sociální	k2eAgInSc4d1
Darwinismus	darwinismus	k1gInSc4
v	v	k7c4
Císařské	císařský	k2eAgNnSc4d1
Německo	Německo	k1gNnSc4
<g/>
:	:	kIx,
Příspěvek	příspěvek	k1gInSc1
Buňka	buňka	k1gFnSc1
Biolog	biolog	k1gMnSc1
Oscar	Oscar	k1gMnSc1
Hertwig	Hertwig	k1gMnSc1
(	(	kIx(
<g/>
1849	#num#	k4
<g/>
-	-	kIx~
<g/>
1922	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forschungen	Forschungen	k1gInSc1
zur	zur	k?
Medizin	Medizin	k1gInSc1
–	–	k?
und	und	k?
Biologiegeschichte	Biologiegeschicht	k1gInSc5
obj	obj	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Stuttgart	Stuttgart	k1gInSc1
<g/>
:	:	kIx,
G.	G.	kA
Fischer	Fischer	k1gMnSc1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Akademie	akademie	k1gFnSc2
der	drát	k5eAaImRp2nS
Wissenschaften	Wissenschaftno	k1gNnPc2
und	und	k?
der	drát	k5eAaImRp2nS
Literatur	literatura	k1gFnPc2
Mainz	Mainz	k1gInSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
David	David	k1gMnSc1
Ellyard	Ellyard	k1gMnSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
objevil	objevit	k5eAaPmAgMnS
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
když	když	k8xS
<g/>
"	"	kIx"
<g/>
,	,	kIx,
New	New	k1gMnSc7
Holland	Hollanda	k1gFnPc2
Publishers	Publishers	k1gInSc1
(	(	kIx(
<g/>
Australia	Australia	k1gFnSc1
<g/>
)	)	kIx)
Private	Privat	k1gInSc5
Limited	limited	k2eAgMnPc2d1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
274	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
C.	C.	kA
R.	R.	kA
Austin	Austin	k1gMnSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
savčí	savčí	k2eAgNnSc1d1
vajíčko	vajíčko	k1gNnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Blackwell	Blackwell	k1gMnSc1
Scientific	Scientifice	k1gInPc2
Publications	Publications	k1gInSc1
<g/>
,	,	kIx,
Oxford	Oxford	k1gInSc1
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Epigenetika	Epigenetika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Evoluce	evoluce	k1gFnSc1
–	–	k?
pojmy	pojem	k1gInPc1
a	a	k8xC
historie	historie	k1gFnSc1
evolučního	evoluční	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Oscar	Oscar	k1gInSc1
Hertwig	Hertwig	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Plné	plný	k2eAgInPc1d1
texty	text	k1gInPc1
děl	dělo	k1gNnPc2
autora	autor	k1gMnSc2
Oscar	Oscara	k1gFnPc2
Hertwig	Hertwig	k1gMnSc1
na	na	k7c6
projektu	projekt	k1gInSc6
Gutenberg	Gutenberg	k1gMnSc1
</s>
<s>
Internet	Internet	k1gInSc1
Archive	archiv	k1gInSc5
</s>
<s>
První	první	k4xOgFnPc1
kapitoly	kapitola	k1gFnPc1
O.	O.	kA
Hertwig	Hertwig	k1gInSc4
knize	kniha	k1gFnSc3
Lehrbuch	Lehrbuch	k1gInSc1
der	drát	k5eAaImRp2nS
Entwicklungsgeschichte	Entwicklungsgeschicht	k1gInSc5
des	des	k1gNnPc2
Menschen	Menschno	k1gNnPc2
und	und	k?
der	drát	k5eAaImRp2nS
Wirbeltiere	Wirbeltier	k1gMnSc5
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
on-line	on-lin	k1gInSc5
zde	zde	k6eAd1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
nlk	nlk	k?
<g/>
20000088972	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118703919	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0980	#num#	k4
6041	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
85356422	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
64802006	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
85356422	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
