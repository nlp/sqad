<s>
Zoologie	zoologie	k1gFnSc1	zoologie
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnSc1d1	přírodní
věda	věda	k1gFnSc1	věda
spadající	spadající	k2eAgFnSc1d1	spadající
do	do	k7c2	do
okruhu	okruh	k1gInSc2	okruh
biologických	biologický	k2eAgFnPc2d1	biologická
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
studiem	studio	k1gNnSc7	studio
organismů	organismus	k1gInPc2	organismus
z	z	k7c2	z
říše	říš	k1gFnSc2	říš
živočichů	živočich	k1gMnPc2	živočich
(	(	kIx(	(
<g/>
Animalia	Animalia	k1gFnSc1	Animalia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
zoologie	zoologie	k1gFnSc1	zoologie
zabývá	zabývat	k5eAaImIp3nS	zabývat
také	také	k6eAd1	také
studiem	studio	k1gNnSc7	studio
prvoků	prvok	k1gMnPc2	prvok
(	(	kIx(	(
<g/>
protozoologií	protozoologie	k1gFnPc2	protozoologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
již	již	k6eAd1	již
ovšem	ovšem	k9	ovšem
nejsou	být	k5eNaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
:	:	kIx,	:
ζ	ζ	k?	ζ
(	(	kIx(	(
<g/>
zoon	zoon	k1gMnSc1	zoon
<g/>
)	)	kIx)	)
=	=	kIx~	=
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
živočich	živočich	k1gMnSc1	živočich
<g/>
,	,	kIx,	,
tvor	tvor	k1gMnSc1	tvor
<g/>
;	;	kIx,	;
λ	λ	k?	λ
=	=	kIx~	=
slovo	slovo	k1gNnSc1	slovo
Zoologii	zoologie	k1gFnSc4	zoologie
lze	lze	k6eAd1	lze
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
zoologii	zoologie	k1gFnSc4	zoologie
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgMnPc4d1	zabývající
se	se	k3xPyFc4	se
obecnými	obecný	k2eAgInPc7d1	obecný
jevy	jev	k1gInPc7	jev
a	a	k8xC	a
zákonitostmi	zákonitost	k1gFnPc7	zákonitost
vlastními	vlastní	k2eAgInPc7d1	vlastní
všem	všecek	k3xTgNnPc3	všecek
nebo	nebo	k8xC	nebo
mnoha	mnoho	k4c3	mnoho
skupinám	skupina	k1gFnPc3	skupina
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
a	a	k8xC	a
systematickou	systematický	k2eAgFnSc4d1	systematická
zoologii	zoologie	k1gFnSc4	zoologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
skupiny	skupina	k1gFnPc4	skupina
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
zoologie	zoologie	k1gFnSc1	zoologie
přenáší	přenášet	k5eAaImIp3nS	přenášet
teoretické	teoretický	k2eAgFnPc4d1	teoretická
znalosti	znalost	k1gFnPc4	znalost
zoologie	zoologie	k1gFnSc2	zoologie
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
(	(	kIx(	(
<g/>
např.	např.	kA	např.
lesnická	lesnický	k2eAgFnSc1d1	lesnická
zoologie	zoologie	k1gFnSc1	zoologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
zoologie	zoologie	k1gFnSc2	zoologie
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
zabývající	zabývající	k2eAgMnPc1d1	zabývající
se	s	k7c7	s
zoologií	zoologie	k1gFnSc7	zoologie
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
zoologové	zoolog	k1gMnPc1	zoolog
<g/>
.	.	kIx.	.
</s>
<s>
Anatomie	anatomie	k1gFnSc1	anatomie
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
stavbě	stavba	k1gFnSc6	stavba
těl	tělo	k1gNnPc2	tělo
živých	živý	k2eAgInPc2d1	živý
organizmů	organizmus	k1gInPc2	organizmus
Cytologie	cytologie	k1gFnSc2	cytologie
-	-	kIx~	-
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
buněčné	buněčný	k2eAgFnPc1d1	buněčná
organely	organela	k1gFnPc1	organela
Embryologie	embryologie	k1gFnSc2	embryologie
-	-	kIx~	-
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
vývoj	vývoj	k1gInSc4	vývoj
zárodku	zárodek	k1gInSc2	zárodek
(	(	kIx(	(
<g/>
embrya	embryo	k1gNnPc1	embryo
<g/>
)	)	kIx)	)
Etologie	etologie	k1gFnSc1	etologie
-	-	kIx~	-
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
chováním	chování	k1gNnSc7	chování
živočichů	živočich	k1gMnPc2	živočich
Fyziologie	fyziologie	k1gFnSc1	fyziologie
-	-	kIx~	-
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
životní	životní	k2eAgInPc4d1	životní
procesy	proces	k1gInPc4	proces
probíhající	probíhající	k2eAgInPc4d1	probíhající
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
živočichů	živočich	k1gMnPc2	živočich
Histologie	histologie	k1gFnSc1	histologie
-	-	kIx~	-
studuje	studovat	k5eAaImIp3nS	studovat
tělesné	tělesný	k2eAgFnPc4d1	tělesná
tkáně	tkáň	k1gFnPc4	tkáň
Morfologie	morfologie	k1gFnSc2	morfologie
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
vnější	vnější	k2eAgFnSc7d1	vnější
stavbou	stavba	k1gFnSc7	stavba
těl	tělo	k1gNnPc2	tělo
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
Organologie	organologie	k1gFnSc2	organologie
-	-	kIx~	-
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
orgány	orgán	k1gInPc4	orgán
Taxonomie	taxonomie	k1gFnSc2	taxonomie
(	(	kIx(	(
<g/>
systematika	systematik	k1gMnSc4	systematik
<g/>
)	)	kIx)	)
-	-	kIx~	-
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
tříděním	třídění	k1gNnSc7	třídění
živých	živý	k2eAgInPc2d1	živý
organizmů	organizmus	k1gInPc2	organizmus
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jejich	jejich	k3xOp3gFnSc2	jejich
příbuznosti	příbuznost	k1gFnSc2	příbuznost
Parazitologie	parazitologie	k1gFnSc2	parazitologie
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
cizopasnících	cizopasnící	k2eAgFnPc6d1	cizopasnící
Pedozoologie	Pedozoologie	k1gFnSc1	Pedozoologie
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
půdních	půdní	k2eAgMnPc6d1	půdní
živočiších	živočich	k1gMnPc6	živočich
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
zoologické	zoologický	k2eAgFnPc4d1	zoologická
obory	obora	k1gFnPc4	obora
vymezené	vymezený	k2eAgFnPc4d1	vymezená
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
určitou	určitý	k2eAgFnSc4d1	určitá
taxonomickou	taxonomický	k2eAgFnSc4d1	Taxonomická
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
již	již	k9	již
taxonomicky	taxonomicky	k6eAd1	taxonomicky
neplatnou	platný	k2eNgFnSc4d1	neplatná
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
prvoci	prvok	k1gMnPc1	prvok
nebo	nebo	k8xC	nebo
červi	červ	k1gMnPc1	červ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zoologie	zoologie	k1gFnSc1	zoologie
bezobratlých	bezobratlí	k1gMnPc2	bezobratlí
<g/>
:	:	kIx,	:
Protozoologie	Protozoologie	k1gFnSc1	Protozoologie
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
prvocích	prvok	k1gMnPc6	prvok
Helmintologie	Helmintologie	k1gFnSc2	Helmintologie
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
červech	červ	k1gMnPc6	červ
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
parazitických	parazitický	k2eAgFnPc2d1	parazitická
Entomologie	entomologie	k1gFnSc1	entomologie
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
hmyzu	hmyz	k1gInSc6	hmyz
Arachnologie	Arachnologie	k1gFnSc1	Arachnologie
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
pavoukovcích	pavoukovec	k1gInPc6	pavoukovec
Akarologie	Akarologie	k1gFnSc2	Akarologie
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
roztočích	roztoč	k1gMnPc6	roztoč
Malakologie	malakologie	k1gFnPc4	malakologie
(	(	kIx(	(
<g/>
malakozoologie	malakozoologie	k1gFnPc4	malakozoologie
<g/>
)	)	kIx)	)
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
měkkýších	měkkýší	k2eAgFnPc6d1	měkkýší
Zoologie	zoologie	k1gFnPc1	zoologie
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
:	:	kIx,	:
Ichtyologie	ichtyologie	k1gFnSc1	ichtyologie
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
<g />
.	.	kIx.	.
</s>
<s>
o	o	k7c6	o
rybách	ryba	k1gFnPc6	ryba
Batrachologie	Batrachologie	k1gFnSc2	Batrachologie
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
obojživelnících	obojživelník	k1gMnPc6	obojživelník
Herpetologie	herpetologie	k1gFnSc2	herpetologie
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
plazech	plaz	k1gInPc6	plaz
Ornitologie	ornitologie	k1gFnSc2	ornitologie
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
ptácích	pták	k1gMnPc6	pták
Mammaliologie	Mammaliologie	k1gFnSc2	Mammaliologie
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
savcích	savec	k1gMnPc6	savec
Primatologie	Primatologie	k1gFnSc2	Primatologie
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
primátech	primát	k1gInPc6	primát
Ekologie	ekologie	k1gFnSc2	ekologie
živočichů	živočich	k1gMnPc2	živočich
-	-	kIx~	-
studuje	studovat	k5eAaImIp3nS	studovat
vztah	vztah	k1gInSc4	vztah
živočicha	živočich	k1gMnSc2	živočich
k	k	k7c3	k
prostředí	prostředí	k1gNnSc3	prostředí
Gradologie	Gradologie	k1gFnSc2	Gradologie
-	-	kIx~	-
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
přemnožováním	přemnožování	k1gNnSc7	přemnožování
(	(	kIx(	(
<g/>
gradacemi	gradace	k1gFnPc7	gradace
<g/>
)	)	kIx)	)
škůdců	škůdce	k1gMnPc2	škůdce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
lesních	lesní	k2eAgInPc2d1	lesní
nebo	nebo	k8xC	nebo
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
<g/>
)	)	kIx)	)
Paleozoologie	paleozoologie	k1gFnSc1	paleozoologie
-	-	kIx~	-
zaobírá	zaobírat	k5eAaImIp3nS	zaobírat
se	se	k3xPyFc4	se
historickým	historický	k2eAgInSc7d1	historický
vývojem	vývoj	k1gInSc7	vývoj
živočichů	živočich	k1gMnPc2	živočich
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
Veterinární	veterinární	k2eAgNnSc4d1	veterinární
lékařství	lékařství	k1gNnSc4	lékařství
-	-	kIx~	-
zabývá	zabývat	k5eAaImIp3nS	zabývat
zdravotní	zdravotní	k2eAgFnSc7d1	zdravotní
problematikou	problematika	k1gFnSc7	problematika
<g/>
,	,	kIx,	,
léčbou	léčba	k1gFnSc7	léčba
nemocemi	nemoc	k1gFnPc7	nemoc
i	i	k8xC	i
prevencí	prevence	k1gFnSc7	prevence
nemocí	nemoc	k1gFnPc2	nemoc
domácích	domácí	k2eAgFnPc2d1	domácí
i	i	k8xC	i
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgNnPc2d1	žijící
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Zoogeografie	zoogeografie	k1gFnSc1	zoogeografie
-	-	kIx~	-
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
rozšíření	rozšíření	k1gNnSc1	rozšíření
živočichů	živočich	k1gMnPc2	živočich
Zootechnika	zootechnika	k1gFnSc1	zootechnika
-	-	kIx~	-
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
chovem	chov	k1gInSc7	chov
zvířat	zvíře	k1gNnPc2	zvíře
Kryptozoologie	Kryptozoologie	k1gFnSc1	Kryptozoologie
-	-	kIx~	-
pseudověda	pseudověda	k1gFnSc1	pseudověda
o	o	k7c6	o
zvířatech	zvíře	k1gNnPc6	zvíře
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejichž	jejichž	k3xOyRp3gFnSc6	jejichž
existenci	existence	k1gFnSc6	existence
nemáme	mít	k5eNaImIp1nP	mít
nezvratný	zvratný	k2eNgInSc4d1	nezvratný
důkaz	důkaz	k1gInSc4	důkaz
Seznam	seznam	k1gInSc1	seznam
zoologů	zoolog	k1gMnPc2	zoolog
dle	dle	k7c2	dle
zkratek	zkratka	k1gFnPc2	zkratka
Živočichové	živočich	k1gMnPc1	živočich
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zoologie	zoologie	k1gFnSc2	zoologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
zoologie	zoologie	k1gFnSc2	zoologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
