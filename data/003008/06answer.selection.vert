<s>
Zoologie	zoologie	k1gFnSc1	zoologie
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnSc1d1	přírodní
věda	věda	k1gFnSc1	věda
spadající	spadající	k2eAgFnSc1d1	spadající
do	do	k7c2	do
okruhu	okruh	k1gInSc2	okruh
biologických	biologický	k2eAgFnPc2d1	biologická
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
studiem	studio	k1gNnSc7	studio
organismů	organismus	k1gInPc2	organismus
z	z	k7c2	z
říše	říš	k1gFnSc2	říš
živočichů	živočich	k1gMnPc2	živočich
(	(	kIx(	(
<g/>
Animalia	Animalia	k1gFnSc1	Animalia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
