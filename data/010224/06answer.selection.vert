<s>
Pyrokatechol	Pyrokatechol	k1gInSc1	Pyrokatechol
(	(	kIx(	(
<g/>
též	též	k9	též
pyrokatechin	pyrokatechin	k1gInSc1	pyrokatechin
<g/>
;	;	kIx,	;
systematický	systematický	k2eAgInSc1d1	systematický
název	název	k1gInSc1	název
benzen-	benzen-	k?	benzen-
<g/>
1,2	[number]	k4	1,2
<g/>
-diol	iol	k1gInSc1	-diol
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
organická	organický	k2eAgFnSc1d1	organická
aromatická	aromatický	k2eAgFnSc1d1	aromatická
sloučenina	sloučenina	k1gFnSc1	sloučenina
(	(	kIx(	(
<g/>
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
fenolů	fenol	k1gInPc2	fenol
<g/>
)	)	kIx)	)
využívaná	využívaný	k2eAgFnSc1d1	využívaná
k	k	k7c3	k
organickým	organický	k2eAgFnPc3d1	organická
syntézám	syntéza	k1gFnPc3	syntéza
<g/>
.	.	kIx.	.
</s>
