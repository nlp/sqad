<p>
<s>
Pyrokatechol	Pyrokatechol	k1gInSc1	Pyrokatechol
(	(	kIx(	(
<g/>
též	též	k9	též
pyrokatechin	pyrokatechin	k1gInSc1	pyrokatechin
<g/>
;	;	kIx,	;
systematický	systematický	k2eAgInSc1d1	systematický
název	název	k1gInSc1	název
benzen-	benzen-	k?	benzen-
<g/>
1,2	[number]	k4	1,2
<g/>
-diol	iol	k1gInSc1	-diol
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
organická	organický	k2eAgFnSc1d1	organická
aromatická	aromatický	k2eAgFnSc1d1	aromatická
sloučenina	sloučenina	k1gFnSc1	sloučenina
(	(	kIx(	(
<g/>
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
fenolů	fenol	k1gInPc2	fenol
<g/>
)	)	kIx)	)
využívaná	využívaný	k2eAgFnSc1d1	využívaná
k	k	k7c3	k
organickým	organický	k2eAgFnPc3d1	organická
syntézám	syntéza	k1gFnPc3	syntéza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
využíval	využívat	k5eAaPmAgMnS	využívat
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
černobílých	černobílý	k2eAgFnPc2d1	černobílá
fotografických	fotografický	k2eAgFnPc2d1	fotografická
vývojek	vývojka	k1gFnPc2	vývojka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
studiích	studio	k1gNnPc6	studio
na	na	k7c6	na
zvířatech	zvíře	k1gNnPc6	zvíře
byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
jeho	jeho	k3xOp3gFnSc1	jeho
schopnost	schopnost	k1gFnSc1	schopnost
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
rakoviny	rakovina	k1gFnSc2	rakovina
(	(	kIx(	(
<g/>
IARC	IARC	kA	IARC
<g/>
)	)	kIx)	)
ho	on	k3xPp3gMnSc4	on
proto	proto	k6eAd1	proto
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
jako	jako	k9	jako
karcinogen	karcinogen	k1gInSc1	karcinogen
třídy	třída	k1gFnSc2	třída
2	[number]	k4	2
<g/>
B.	B.	kA	B.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Resorcinol	Resorcinol	k1gInSc1	Resorcinol
(	(	kIx(	(
<g/>
1,3	[number]	k4	1,3
<g/>
-benzendiol	enzendiola	k1gFnPc2	-benzendiola
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hydrochinon	hydrochinon	k1gInSc1	hydrochinon
(	(	kIx(	(
<g/>
1,4	[number]	k4	1,4
<g/>
-benzendiol	enzendiola	k1gFnPc2	-benzendiola
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vývojka	vývojka	k1gFnSc1	vývojka
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pyrokatechol	Pyrokatechol	k1gInSc4	Pyrokatechol
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
NIOSH	NIOSH	kA	NIOSH
Pocket	Pocket	k1gMnSc1	Pocket
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc4	ten
Chemical	Chemical	k1gFnPc1	Chemical
Hazards	Hazardsa	k1gFnPc2	Hazardsa
</s>
</p>
