<s>
Kapitál	kapitál	k1gInSc1	kapitál
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Das	Das	k1gMnSc1	Das
Kapital	Kapital	k1gMnSc1	Kapital
<g/>
)	)	kIx)	)
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
Kritika	kritika	k1gFnSc1	kritika
politické	politický	k2eAgFnSc2d1	politická
ekonomie	ekonomie	k1gFnSc2	ekonomie
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
dílo	dílo	k1gNnSc4	dílo
německého	německý	k2eAgMnSc2d1	německý
filozofa	filozof	k1gMnSc2	filozof
Karla	Karel	k1gMnSc2	Karel
Marxe	Marx	k1gMnSc2	Marx
<g/>
.	.	kIx.	.
</s>
