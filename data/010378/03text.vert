<p>
<s>
Kapitál	kapitál	k1gInSc1	kapitál
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Das	Das	k1gMnSc1	Das
Kapital	Kapital	k1gMnSc1	Kapital
<g/>
)	)	kIx)	)
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
Kritika	kritika	k1gFnSc1	kritika
politické	politický	k2eAgFnSc2d1	politická
ekonomie	ekonomie	k1gFnSc2	ekonomie
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
dílo	dílo	k1gNnSc4	dílo
německého	německý	k2eAgMnSc2d1	německý
filozofa	filozof	k1gMnSc2	filozof
Karla	Karel	k1gMnSc2	Karel
Marxe	Marx	k1gMnSc2	Marx
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
autor	autor	k1gMnSc1	autor
vydal	vydat	k5eAaPmAgMnS	vydat
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
díly	díl	k1gInPc4	díl
posmrtně	posmrtně	k6eAd1	posmrtně
zeditoval	zeditovat	k5eAaPmAgMnS	zeditovat
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
Marxův	Marxův	k2eAgMnSc1d1	Marxův
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
a	a	k8xC	a
přítel	přítel	k1gMnSc1	přítel
Friedrich	Friedrich	k1gMnSc1	Friedrich
Engels	Engels	k1gMnSc1	Engels
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1885	[number]	k4	1885
a	a	k8xC	a
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
<s>
Sporný	sporný	k2eAgInSc4d1	sporný
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
díl	díl	k1gInSc4	díl
<g/>
,	,	kIx,	,
Teorie	teorie	k1gFnSc1	teorie
nadhodnoty	nadhodnota	k1gFnSc2	nadhodnota
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nepovažovaný	považovaný	k2eNgMnSc1d1	nepovažovaný
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Karl	Karl	k1gMnSc1	Karl
Kautsky	Kautsky	k1gMnSc1	Kautsky
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1905	[number]	k4	1905
až	až	k9	až
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obsahem	obsah	k1gInSc7	obsah
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
analýza	analýza	k1gFnSc1	analýza
a	a	k8xC	a
kritika	kritika	k1gFnSc1	kritika
kapitalistické	kapitalistický	k2eAgFnSc2d1	kapitalistická
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
zejména	zejména	k9	zejména
kapitalistického	kapitalistický	k2eAgInSc2d1	kapitalistický
způsobu	způsob	k1gInSc2	způsob
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
distribuce	distribuce	k1gFnSc2	distribuce
statků	statek	k1gInPc2	statek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
<g/>
,	,	kIx,	,
Výrobní	výrobní	k2eAgInSc4d1	výrobní
proces	proces	k1gInSc4	proces
kapitálu	kapitál	k1gInSc2	kapitál
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
Produktionsprozess	Produktionsprozess	k1gInSc1	Produktionsprozess
des	des	k1gNnSc6	des
Kapitals	Kapitals	k1gInSc1	Kapitals
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
proces	proces	k1gInSc1	proces
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
tvorbu	tvorba	k1gFnSc4	tvorba
nadhodnoty	nadhodnota	k1gFnSc2	nadhodnota
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
moderního	moderní	k2eAgInSc2d1	moderní
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhý	druhý	k4xOgInSc1	druhý
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
Proces	proces	k1gInSc1	proces
oběhu	oběh	k1gInSc2	oběh
kapitálu	kapitál	k1gInSc2	kapitál
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
Zirkulationsprozess	Zirkulationsprozess	k1gInSc1	Zirkulationsprozess
des	des	k1gNnSc6	des
Kapitals	Kapitals	k1gInSc1	Kapitals
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
kapitalistické	kapitalistický	k2eAgInPc4d1	kapitalistický
zbožní	zbožní	k2eAgInPc4d1	zbožní
a	a	k8xC	a
peněžní	peněžní	k2eAgInPc4d1	peněžní
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgInSc1	třetí
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
Celkový	celkový	k2eAgInSc1d1	celkový
proces	proces	k1gInSc1	proces
kapitalistické	kapitalistický	k2eAgFnSc2d1	kapitalistická
výroby	výroba	k1gFnSc2	výroba
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
Gesamtprozess	Gesamtprozess	k1gInSc1	Gesamtprozess
der	drát	k5eAaImRp2nS	drát
kapitalistischen	kapitalistischen	k1gInSc4	kapitalistischen
Produktion	Produktion	k1gInSc1	Produktion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
analyzuje	analyzovat	k5eAaImIp3nS	analyzovat
tvorbu	tvorba	k1gFnSc4	tvorba
zisku	zisk	k1gInSc2	zisk
a	a	k8xC	a
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
celé	celý	k2eAgNnSc4d1	celé
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kapitál	kapitál	k1gInSc1	kapitál
–	–	k?	–
to	ten	k3xDgNnSc1	ten
nejsou	být	k5eNaImIp3nP	být
obyčejné	obyčejný	k2eAgInPc1d1	obyčejný
peníze	peníz	k1gInPc1	peníz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
peníze	peníz	k1gInPc1	peníz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nám	my	k3xPp1nPc3	my
vydělávají	vydělávat	k5eAaImIp3nP	vydělávat
další	další	k2eAgInPc4d1	další
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Citát	citát	k1gInSc1	citát
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
MARX	Marx	k1gMnSc1	Marx
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Kapitál	kapitál	k1gInSc1	kapitál
:	:	kIx,	:
kritika	kritika	k1gFnSc1	kritika
politické	politický	k2eAgFnSc2d1	politická
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
4	[number]	k4	4
svazky	svazek	k1gInPc4	svazek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
text	text	k1gInSc1	text
první	první	k4xOgFnSc2	první
knihy	kniha	k1gFnSc2	kniha
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
