<s>
Soprán	soprán	k1gInSc1	soprán
(	(	kIx(	(
<g/>
z	z	k7c2	z
ital	itala	k1gFnPc2	itala
<g/>
.	.	kIx.	.
sopra	sopra	k1gFnSc1	sopra
–	–	k?	–
nahoře	nahoře	k6eAd1	nahoře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
renesanci	renesance	k1gFnSc6	renesance
označován	označovat	k5eAaImNgInS	označovat
také	také	k9	také
jako	jako	k8xS	jako
diskant	diskant	k1gInSc4	diskant
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
zpěvních	zpěvní	k2eAgInPc2d1	zpěvní
lidských	lidský	k2eAgInPc2d1	lidský
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Sopránové	sopránový	k2eAgFnPc1d1	sopránová
party	parta	k1gFnPc1	parta
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
zpívány	zpíván	k2eAgFnPc1d1	zpívána
ženami	žena	k1gFnPc7	žena
nebo	nebo	k8xC	nebo
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
v	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
také	také	k9	také
kastráty	kastrát	k1gMnPc4	kastrát
<g/>
.	.	kIx.	.
</s>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
hlas	hlas	k1gInSc1	hlas
může	moct	k5eAaImIp3nS	moct
zpívat	zpívat	k5eAaImF	zpívat
soprán	soprán	k1gInSc1	soprán
pouze	pouze	k6eAd1	pouze
falzetem	falzet	k1gInSc7	falzet
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
sopránu	soprán	k1gInSc2	soprán
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
malé	malý	k2eAgFnPc4d1	malá
a	a	k8xC	a
-	-	kIx~	-
c3	c3	k4	c3
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
áriích	árie	k1gFnPc6	árie
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
tóny	tón	k1gInPc1	tón
vyšší	vysoký	k2eAgInPc1d2	vyšší
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Mozartově	Mozartův	k2eAgFnSc6d1	Mozartova
opeře	opera	k1gFnSc6	opera
Kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
flétna	flétna	k1gFnSc1	flétna
zpívá	zpívat	k5eAaImIp3nS	zpívat
Královna	královna	k1gFnSc1	královna
noci	noc	k1gFnSc2	noc
v	v	k7c6	v
árii	árie	k1gFnSc6	árie
Der	drát	k5eAaImRp2nS	drát
Hölle	Hölle	k1gNnSc4	Hölle
Rache	Rache	k1gInSc1	Rache
kocht	kocht	k1gInSc1	kocht
in	in	k?	in
meinem	meino	k1gNnSc7	meino
Herzen	Herzna	k1gFnPc2	Herzna
až	až	k9	až
tón	tón	k1gInSc4	tón
f	f	k?	f
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ustálilo	ustálit	k5eAaPmAgNnS	ustálit
několik	několik	k4yIc4	několik
typů	typ	k1gInPc2	typ
sopránů	soprán	k1gInPc2	soprán
<g/>
:	:	kIx,	:
lyrický	lyrický	k2eAgInSc1d1	lyrický
soprán	soprán	k1gInSc1	soprán
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
:	:	kIx,	:
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
,	,	kIx,	,
Čarostřelec	čarostřelec	k1gMnSc1	čarostřelec
<g/>
:	:	kIx,	:
Agáta	Agáta	k1gFnSc1	Agáta
<g/>
)	)	kIx)	)
–	–	k?	–
sladký	sladký	k2eAgInSc1d1	sladký
<g/>
,	,	kIx,	,
půvabný	půvabný	k2eAgInSc1d1	půvabný
hlas	hlas	k1gInSc1	hlas
podobný	podobný	k2eAgInSc1d1	podobný
subretnímu	subretní	k2eAgInSc3d1	subretní
sopránu	soprán	k1gInSc3	soprán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
více	hodně	k6eAd2	hodně
procítěný	procítěný	k2eAgInSc4d1	procítěný
dramatický	dramatický	k2eAgInSc4d1	dramatický
soprán	soprán	k1gInSc4	soprán
(	(	kIx(	(
<g/>
Tristan	Tristan	k1gInSc1	Tristan
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
Isolda	Isolda	k1gFnSc1	Isolda
<g/>
:	:	kIx,	:
Isolda	Isolda	k1gFnSc1	Isolda
<g/>
,	,	kIx,	,
Andrea	Andrea	k1gFnSc1	Andrea
Chénier	Chénier	k1gMnSc1	Chénier
<g/>
:	:	kIx,	:
Maddalena	Maddalen	k2eAgFnSc1d1	Maddalena
-	-	kIx~	-
Umberto	Umberta	k1gFnSc5	Umberta
Giordano	Giordana	k1gFnSc5	Giordana
<g/>
)	)	kIx)	)
–	–	k?	–
plný	plný	k2eAgInSc4d1	plný
a	a	k8xC	a
emotivní	emotivní	k2eAgInSc4d1	emotivní
hlas	hlas	k1gInSc4	hlas
koloraturní	koloraturní	k2eAgInSc1d1	koloraturní
soprán	soprán	k1gInSc1	soprán
(	(	kIx(	(
<g/>
Lucia	Lucia	k1gFnSc1	Lucia
di	di	k?	di
Lammermoor	Lammermoor	k1gInSc1	Lammermoor
<g/>
:	:	kIx,	:
Lucia	Lucia	k1gFnSc1	Lucia
<g/>
,	,	kIx,	,
Rigoletto	Rigoletto	k1gNnSc1	Rigoletto
<g/>
:	:	kIx,	:
Gilda	gilda	k1gFnSc1	gilda
<g/>
)	)	kIx)	)
–	–	k?	–
lehký	lehký	k2eAgInSc4d1	lehký
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
pohyblivý	pohyblivý	k2eAgMnSc1d1	pohyblivý
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
polohách	poloha	k1gFnPc6	poloha
<g />
.	.	kIx.	.
</s>
<s>
subretní	subretní	k2eAgInSc1d1	subretní
soprán	soprán	k1gInSc1	soprán
(	(	kIx(	(
<g/>
Čarostřelec	čarostřelec	k1gMnSc1	čarostřelec
<g/>
:	:	kIx,	:
Anička	Anička	k1gFnSc1	Anička
<g/>
)	)	kIx)	)
–	–	k?	–
sladký	sladký	k2eAgInSc1d1	sladký
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
hlas	hlas	k1gInSc1	hlas
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
v	v	k7c6	v
komediích	komedie	k1gFnPc6	komedie
nebo	nebo	k8xC	nebo
operetách	opereta	k1gFnPc6	opereta
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
soprán	soprán	k1gInSc1	soprán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Soprán	soprán	k1gInSc1	soprán
<g/>
,	,	kIx,	,
ukázka	ukázka	k1gFnSc1	ukázka
-	-	kIx~	-
Edita	Edita	k1gFnSc1	Edita
Gruberová	Gruberová	k1gFnSc1	Gruberová
-	-	kIx~	-
Rigoletto	Rigoletto	k1gNnSc1	Rigoletto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Caro	Caro	k6eAd1	Caro
nome	nomat	k5eAaPmIp3nS	nomat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Giuseppe	Giusepp	k1gMnSc5	Giusepp
Verdi	Verd	k1gMnPc5	Verd
<g/>
)	)	kIx)	)
</s>
