<s>
Soprán	soprán	k1gInSc1	soprán
(	(	kIx(	(
<g/>
z	z	k7c2	z
ital	itala	k1gFnPc2	itala
<g/>
.	.	kIx.	.
sopra	sopra	k1gFnSc1	sopra
–	–	k?	–
nahoře	nahoře	k6eAd1	nahoře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
renesanci	renesance	k1gFnSc6	renesance
označován	označovat	k5eAaImNgInS	označovat
také	také	k9	také
jako	jako	k8xS	jako
diskant	diskant	k1gInSc4	diskant
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
zpěvních	zpěvní	k2eAgInPc2d1	zpěvní
lidských	lidský	k2eAgInPc2d1	lidský
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
