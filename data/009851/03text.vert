<p>
<s>
Seftigen	Seftigen	k1gInSc4	Seftigen
je	být	k5eAaImIp3nS	být
švýcarské	švýcarský	k2eAgNnSc1d1	švýcarské
město	město	k1gNnSc1	město
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
Bern	Bern	k1gInSc1	Bern
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
centrum	centrum	k1gNnSc1	centrum
okresu	okres	k1gInSc2	okres
Seftigen	Seftigen	k1gInSc1	Seftigen
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
576	[number]	k4	576
m.	m.	k?	m.
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
7,5	[number]	k4	7,5
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Partnerská	partnerský	k2eAgFnSc1d1	partnerská
obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc4d1	český
Kovářov	Kovářov	k1gInSc4	Kovářov
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
2082	[number]	k4	2082
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
byla	být	k5eAaImAgFnS	být
529	[number]	k4	529
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Seftigen	Seftigen	k1gInSc1	Seftigen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
obce	obec	k1gFnSc2	obec
-	-	kIx~	-
Německy	německy	k6eAd1	německy
</s>
</p>
