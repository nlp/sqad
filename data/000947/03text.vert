<s>
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
(	(	kIx(	(
<g/>
chorvatsky	chorvatsky	k6eAd1	chorvatsky
<g/>
:	:	kIx,	:
Zagreb	Zagreb	k1gInSc1	Zagreb
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Agram	Agram	k1gInSc1	Agram
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
800	[number]	k4	800
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
pohoří	pohoří	k1gNnSc2	pohoří
Medvednica	Medvednic	k1gInSc2	Medvednic
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Sávy	Sáva	k1gFnSc2	Sáva
<g/>
,	,	kIx,	,
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
blízko	blízko	k7c2	blízko
slovinských	slovinský	k2eAgFnPc2d1	slovinská
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
(	(	kIx(	(
<g/>
železniční	železniční	k2eAgFnSc1d1	železniční
a	a	k8xC	a
dálniční	dálniční	k2eAgFnSc1d1	dálniční
křižovatka	křižovatka	k1gFnSc1	křižovatka
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
Jaderským	jaderský	k2eAgNnSc7d1	Jaderské
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
střední	střední	k2eAgFnSc7d1	střední
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
návrší	návrší	k1gNnSc6	návrší
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
Kaptol	Kaptol	k1gInSc4	Kaptol
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
zbytky	zbytek	k1gInPc1	zbytek
náhrobků	náhrobek	k1gInPc2	náhrobek
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
Ščitarjevu	Ščitarjevo	k1gNnSc6	Ščitarjevo
byla	být	k5eAaImAgFnS	být
osada	osada	k1gFnSc1	osada
Andautonia	Andautonium	k1gNnSc2	Andautonium
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c4	o
osídlení	osídlení	k1gNnSc4	osídlení
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1094	[number]	k4	1094
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
maďarský	maďarský	k2eAgMnSc1d1	maďarský
král	král	k1gMnSc1	král
Ladislav	Ladislav	k1gMnSc1	Ladislav
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
Kaptolu	Kaptol	k1gInSc2	Kaptol
biskupství	biskupství	k1gNnSc2	biskupství
a	a	k8xC	a
prvním	první	k4xOgMnSc7	první
biskupem	biskup	k1gMnSc7	biskup
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Čecha	Čech	k1gMnSc4	Čech
jménem	jméno	k1gNnSc7	jméno
Duch	duch	k1gMnSc1	duch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
vyvýšenině	vyvýšenina	k1gFnSc6	vyvýšenina
západněji	západně	k6eAd2	západně
<g/>
,	,	kIx,	,
oddělené	oddělený	k2eAgInPc4d1	oddělený
potokem	potok	k1gInSc7	potok
Medveščak	Medveščak	k1gMnSc1	Medveščak
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
obchodní	obchodní	k2eAgFnSc1d1	obchodní
osada	osada	k1gFnSc1	osada
Gradec	Gradec	k1gMnSc1	Gradec
(	(	kIx(	(
<g/>
Grič	Grič	k1gMnSc1	Grič
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgNnSc4d1	dnešní
Horní	horní	k2eAgNnSc4d1	horní
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
mongolské	mongolský	k2eAgFnPc4d1	mongolská
invaze	invaze	k1gFnPc4	invaze
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1242	[number]	k4	1242
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
král	král	k1gMnSc1	král
Béla	Béla	k1gMnSc1	Béla
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
z	z	k7c2	z
vděčnosti	vděčnost	k1gFnSc2	vděčnost
za	za	k7c4	za
poskytnutou	poskytnutý	k2eAgFnSc4d1	poskytnutá
ochranu	ochrana	k1gFnSc4	ochrana
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
město	město	k1gNnSc4	město
za	za	k7c4	za
královské	královský	k2eAgInPc4d1	královský
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
panovali	panovat	k5eAaImAgMnP	panovat
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
i	i	k8xC	i
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1557	[number]	k4	1557
se	se	k3xPyFc4	se
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
poprvé	poprvé	k6eAd1	poprvé
sešel	sejít	k5eAaPmAgMnS	sejít
chorvatský	chorvatský	k2eAgInSc4d1	chorvatský
sněm	sněm	k1gInSc4	sněm
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1607	[number]	k4	1607
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
pozvání	pozvání	k1gNnSc4	pozvání
do	do	k7c2	do
města	město	k1gNnSc2	město
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zde	zde	k6eAd1	zde
založili	založit	k5eAaPmAgMnP	založit
kolej	kolej	k1gFnSc4	kolej
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc4	předchůdce
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Záhřebské	záhřebský	k2eAgFnSc2d1	Záhřebská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1621	[number]	k4	1621
byl	být	k5eAaImAgInS	být
Záhřeb	Záhřeb	k1gInSc4	Záhřeb
sídlem	sídlo	k1gNnSc7	sídlo
chorvatského	chorvatský	k2eAgMnSc2d1	chorvatský
bána	bán	k1gMnSc2	bán
(	(	kIx(	(
<g/>
místokrále	místokrál	k1gMnSc2	místokrál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
osady	osada	k1gFnPc1	osada
<g/>
,	,	kIx,	,
Kaptol	Kaptol	k1gInSc1	Kaptol
jako	jako	k9	jako
církevní	církevní	k2eAgMnSc1d1	církevní
a	a	k8xC	a
Gradec	Gradec	k1gMnSc1	Gradec
jako	jako	k8xS	jako
politické	politický	k2eAgNnSc1d1	politické
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
stále	stále	k6eAd1	stále
svá	svůj	k3xOyFgNnPc4	svůj
samostatná	samostatný	k2eAgNnPc4d1	samostatné
opevnění	opevnění	k1gNnPc4	opevnění
a	a	k8xC	a
v	v	k7c6	v
určitých	určitý	k2eAgNnPc6d1	určité
obdobích	období	k1gNnPc6	období
stály	stát	k5eAaImAgFnP	stát
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
tato	tento	k3xDgNnPc4	tento
opevnění	opevnění	k1gNnPc4	opevnění
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Dolní	dolní	k2eAgNnSc1d1	dolní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgInS	přesunout
obchodní	obchodní	k2eAgInSc1d1	obchodní
a	a	k8xC	a
řemeslný	řemeslný	k2eAgInSc1d1	řemeslný
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
za	za	k7c2	za
bána	bán	k1gMnSc2	bán
Jelačiče	Jelačič	k1gInSc2	Jelačič
se	se	k3xPyFc4	se
Kaptol	Kaptol	k1gInSc1	Kaptol
<g/>
,	,	kIx,	,
Gradec	Gradec	k1gMnSc1	Gradec
i	i	k9	i
Donji	Donje	k1gFnSc4	Donje
Grad	grad	k1gInSc4	grad
spojily	spojit	k5eAaPmAgFnP	spojit
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc4	město
několikrát	několikrát	k6eAd1	několikrát
postiženo	postižen	k2eAgNnSc4d1	postiženo
požárem	požár	k1gInSc7	požár
a	a	k8xC	a
morem	mor	k1gInSc7	mor
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
do	do	k7c2	do
města	město	k1gNnSc2	město
železnice	železnice	k1gFnSc2	železnice
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nádražím	nádraží	k1gNnSc7	nádraží
a	a	k8xC	a
starou	starý	k2eAgFnSc7d1	stará
částí	část	k1gFnSc7	část
města	město	k1gNnSc2	město
začala	začít	k5eAaPmAgFnS	začít
vznikat	vznikat	k5eAaImF	vznikat
nová	nový	k2eAgFnSc1d1	nová
měšťanská	měšťanský	k2eAgFnSc1d1	měšťanská
výstavba	výstavba	k1gFnSc1	výstavba
<g/>
,	,	kIx,	,
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
se	se	k3xPyFc4	se
rozrůstal	rozrůstat	k5eAaImAgInS	rozrůstat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
tak	tak	k6eAd1	tak
rostl	růst	k5eAaImAgInS	růst
i	i	k9	i
jeho	jeho	k3xOp3gInSc4	jeho
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
letech	let	k1gInPc6	let
nakonec	nakonec	k6eAd1	nakonec
získalo	získat	k5eAaPmAgNnS	získat
město	město	k1gNnSc1	město
statut	statut	k1gInSc1	statut
centra	centrum	k1gNnSc2	centrum
chorvatského	chorvatský	k2eAgNnSc2d1	Chorvatské
obrozeneckého	obrozenecký	k2eAgNnSc2d1	obrozenecké
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
Matice	matice	k1gFnSc1	matice
Ilyrská	ilyrský	k2eAgFnSc1d1	ilyrská
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
Matice	matice	k1gFnSc1	matice
chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
<g/>
)	)	kIx)	)
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
již	již	k9	již
ke	k	k7c3	k
konci	konec	k1gInSc6	konec
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
postihlo	postihnout	k5eAaPmAgNnS	postihnout
město	město	k1gNnSc1	město
velké	velký	k2eAgNnSc1d1	velké
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
silně	silně	k6eAd1	silně
poškodilo	poškodit	k5eAaPmAgNnS	poškodit
katedrálu	katedrála	k1gFnSc4	katedrála
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
důkladně	důkladně	k6eAd1	důkladně
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konal	konat	k5eAaImAgInS	konat
sněm	sněm	k1gInSc1	sněm
jihoslovanských	jihoslovanský	k2eAgInPc2d1	jihoslovanský
národů	národ	k1gInPc2	národ
padlého	padlý	k2eAgNnSc2d1	padlé
mocnářství	mocnářství	k1gNnSc2	mocnářství
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
zástupci	zástupce	k1gMnPc1	zástupce
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
jednoho	jeden	k4xCgMnSc2	jeden
státu	stát	k1gInSc2	stát
-	-	kIx~	-
Království	království	k1gNnSc1	království
SHS	SHS	kA	SHS
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1929	[number]	k4	1929
a	a	k8xC	a
1939	[number]	k4	1939
tu	tu	k6eAd1	tu
bylo	být	k5eAaImAgNnS	být
sídlo	sídlo	k1gNnSc1	sídlo
Sávské	sávský	k2eAgFnSc2d1	sávský
bánoviny	bánovina	k1gFnSc2	bánovina
<g/>
,	,	kIx,	,
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pak	pak	k6eAd1	pak
ustašovského	ustašovský	k2eAgInSc2d1	ustašovský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
okolo	okolo	k7c2	okolo
185	[number]	k4	185
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
socialistické	socialistický	k2eAgFnSc2d1	socialistická
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
byl	být	k5eAaImAgInS	být
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
opět	opět	k6eAd1	opět
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
vystavěn	vystavěn	k2eAgInSc4d1	vystavěn
tzv.	tzv.	kA	tzv.
Novi	Nov	k1gFnSc2	Nov
Zagreb	Zagreb	k1gInSc1	Zagreb
-	-	kIx~	-
sídliště	sídliště	k1gNnSc1	sídliště
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Sávy	Sáva	k1gFnSc2	Sáva
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
život	život	k1gInSc4	život
město	město	k1gNnSc4	město
byla	být	k5eAaImAgFnS	být
významnou	významný	k2eAgFnSc7d1	významná
akcí	akce	k1gFnSc7	akce
Univerziáda	univerziáda	k1gFnSc1	univerziáda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
Záhřebu	Záhřeb	k1gInSc2	Záhřeb
přicestovalo	přicestovat	k5eAaPmAgNnS	přicestovat
6423	[number]	k4	6423
sportovců	sportovec	k1gMnPc2	sportovec
z	z	k7c2	z
122	[number]	k4	122
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
,	,	kIx,	,
za	za	k7c2	za
balkánské	balkánský	k2eAgFnSc2d1	balkánská
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
bombardován	bombardován	k2eAgInSc1d1	bombardován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgInS	být
vážně	vážně	k6eAd1	vážně
poškozen	poškodit	k5eAaPmNgInS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
okolo	okolo	k7c2	okolo
700	[number]	k4	700
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
s	s	k7c7	s
předměstími	předměstí	k1gNnPc7	předměstí
okolo	okolo	k7c2	okolo
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatský	chorvatský	k2eAgInSc1d1	chorvatský
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
tvoří	tvořit	k5eAaImIp3nS	tvořit
historické	historický	k2eAgNnSc4d1	historické
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
Horní	horní	k2eAgNnSc4d1	horní
město	město	k1gNnSc4	město
a	a	k8xC	a
Kaptol	Kaptol	k1gInSc4	Kaptol
<g/>
,	,	kIx,	,
a	a	k8xC	a
mladší	mladý	k2eAgNnSc1d2	mladší
Dolní	dolní	k2eAgNnSc1d1	dolní
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Malebný	malebný	k2eAgMnSc1d1	malebný
Gradec	Gradec	k1gMnSc1	Gradec
(	(	kIx(	(
<g/>
Horní	horní	k2eAgNnSc1d1	horní
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejlépe	dobře	k6eAd3	dobře
zachovaným	zachovaný	k2eAgInPc3d1	zachovaný
historickým	historický	k2eAgInPc3d1	historický
centrům	centr	k1gInPc3	centr
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
za	za	k7c7	za
údolím	údolí	k1gNnSc7	údolí
potoka	potok	k1gInSc2	potok
Medveščaku	Medveščak	k1gInSc2	Medveščak
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
středověký	středověký	k2eAgInSc1d1	středověký
Kaptol	Kaptol	k1gInSc1	Kaptol
s	s	k7c7	s
gotickou	gotický	k2eAgFnSc7d1	gotická
katedrálou	katedrála	k1gFnSc7	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
nejživější	živý	k2eAgFnSc1d3	nejživější
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgNnSc1d1	dolní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
mezi	mezi	k7c7	mezi
nádražím	nádraží	k1gNnSc7	nádraží
a	a	k8xC	a
historickými	historický	k2eAgNnPc7d1	historické
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
přibyl	přibýt	k5eAaPmAgInS	přibýt
Nový	nový	k2eAgInSc1d1	nový
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
sídliště	sídliště	k1gNnSc4	sídliště
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Sávy	Sáva	k1gFnSc2	Sáva
<g/>
..	..	k?	..
Historicky	historicky	k6eAd1	historicky
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
Horní	horní	k2eAgNnSc1d1	horní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
Vláda	vláda	k1gFnSc1	vláda
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
a	a	k8xC	a
parlament	parlament	k1gInSc1	parlament
a	a	k8xC	a
kde	kde	k6eAd1	kde
stojí	stát	k5eAaImIp3nS	stát
gotický	gotický	k2eAgInSc1d1	gotický
kostel	kostel	k1gInSc1	kostel
Sv.	sv.	kA	sv.
Marka	marka	k1gFnSc1	marka
s	s	k7c7	s
erby	erb	k1gInPc7	erb
Záhřebu	Záhřeb	k1gInSc2	Záhřeb
<g/>
,	,	kIx,	,
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
,	,	kIx,	,
Dalmácie	Dalmácie	k1gFnSc2	Dalmácie
a	a	k8xC	a
Slavonie	Slavonie	k1gFnSc2	Slavonie
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
z	z	k7c2	z
barevných	barevný	k2eAgFnPc2d1	barevná
glazovaných	glazovaný	k2eAgFnPc2d1	glazovaná
tašek	taška	k1gFnPc2	taška
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
Záhřebu	Záhřeb	k1gInSc2	Záhřeb
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
turistického	turistický	k2eAgNnSc2d1	turistické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgNnSc1d1	dolní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
centrem	centr	k1gInSc7	centr
kulturním	kulturní	k2eAgInSc7d1	kulturní
<g/>
,	,	kIx,	,
obchodním	obchodní	k2eAgInSc7d1	obchodní
a	a	k8xC	a
společenským	společenský	k2eAgInSc7d1	společenský
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
živé	živý	k2eAgFnPc1d1	živá
třídy	třída	k1gFnPc1	třída
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
velká	velký	k2eAgNnPc1d1	velké
náměstí	náměstí	k1gNnPc1	náměstí
<g/>
,	,	kIx,	,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
významných	významný	k2eAgFnPc6d1	významná
osobnostech	osobnost	k1gFnPc6	osobnost
chorvatské	chorvatský	k2eAgFnSc2d1	chorvatská
historie	historie	k1gFnSc2	historie
(	(	kIx(	(
<g/>
Bána	bán	k1gMnSc2	bán
Jelačiće	Jelačić	k1gMnSc2	Jelačić
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
Tomislava	Tomislav	k1gMnSc2	Tomislav
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
významné	významný	k2eAgFnPc1d1	významná
budovy	budova	k1gFnPc1	budova
školské	školský	k2eAgFnPc1d1	školská
<g/>
,	,	kIx,	,
či	či	k8xC	či
kulturní	kulturní	k2eAgInSc1d1	kulturní
<g/>
.	.	kIx.	.
</s>
<s>
Gradec	Gradec	k1gMnSc1	Gradec
a	a	k8xC	a
Kaptol	Kaptol	k1gInSc1	Kaptol
spojuje	spojovat	k5eAaImIp3nS	spojovat
Kamenná	kamenný	k2eAgFnSc1d1	kamenná
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
chorvatsky	chorvatsky	k6eAd1	chorvatsky
Kamenita	Kamenit	k2eAgNnPc4d1	Kamenit
vrata	vrata	k1gNnPc4	vrata
<g/>
)	)	kIx)	)
ze	z	k7c2	z
třináctého	třináctý	k4xOgNnSc2	třináctý
století	století	k1gNnSc2	století
s	s	k7c7	s
poutní	poutní	k2eAgFnSc7d1	poutní
svatyní	svatyně	k1gFnSc7	svatyně
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
odolal	odolat	k5eAaPmAgMnS	odolat
požáru	požár	k1gInSc2	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1731	[number]	k4	1731
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgNnSc1d1	horní
město	město	k1gNnSc1	město
s	s	k7c7	s
Dolním	dolní	k2eAgMnSc7d1	dolní
spojuje	spojovat	k5eAaImIp3nS	spojovat
také	také	k9	také
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc1	sto
let	léto	k1gNnPc2	léto
stará	starat	k5eAaImIp3nS	starat
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
její	její	k3xOp3gFnPc4	její
modré	modrý	k2eAgFnPc4d1	modrá
kabinky	kabinka	k1gFnPc4	kabinka
překonávají	překonávat	k5eAaImIp3nP	překonávat
výškový	výškový	k2eAgInSc4d1	výškový
rozdíl	rozdíl	k1gInSc4	rozdíl
30	[number]	k4	30
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
šestašedesát	šestašedesát	k4xCc1	šestašedesát
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
jízda	jízda	k1gFnSc1	jízda
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
konce	konec	k1gInSc2	konec
na	na	k7c6	na
druhý	druhý	k4xOgInSc1	druhý
netrvá	trvat	k5eNaImIp3nS	trvat
ani	ani	k9	ani
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Gradec	Gradec	k1gMnSc1	Gradec
(	(	kIx(	(
<g/>
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
Lotrščak	Lotrščak	k1gInSc1	Lotrščak
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
Horního	horní	k2eAgNnSc2d1	horní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
u	u	k7c2	u
horní	horní	k2eAgFnSc2d1	horní
stanice	stanice	k1gFnSc2	stanice
lanovky	lanovka	k1gFnSc2	lanovka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
z	z	k7c2	z
horního	horní	k2eAgNnSc2d1	horní
patra	patro	k1gNnSc2	patro
je	být	k5eAaImIp3nS	být
pozoruhodný	pozoruhodný	k2eAgInSc4d1	pozoruhodný
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Kamenná	kamenný	k2eAgFnSc1d1	kamenná
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
Kamenita	Kamenit	k2eAgNnPc1d1	Kamenit
vrata	vrata	k1gNnPc1	vrata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
zachovaná	zachovaný	k2eAgFnSc1d1	zachovaná
z	z	k7c2	z
původně	původně	k6eAd1	původně
pěti	pět	k4xCc2	pět
bran	brána	k1gFnPc2	brána
Gradce	Gradec	k1gMnPc4	Gradec
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
potoka	potok	k1gInSc2	potok
Medveščaku	Medveščak	k1gInSc2	Medveščak
a	a	k8xC	a
v	v	k7c6	v
bráně	brána	k1gFnSc6	brána
je	být	k5eAaImIp3nS	být
poutní	poutní	k2eAgFnSc1d1	poutní
kaple	kaple	k1gFnSc1	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Bánský	bánský	k2eAgInSc1d1	bánský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgInSc1d1	barokní
patrový	patrový	k2eAgInSc1d1	patrový
komplex	komplex	k1gInSc1	komplex
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
sídlo	sídlo	k1gNnSc4	sídlo
chorvatské	chorvatský	k2eAgFnSc2d1	chorvatská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatský	chorvatský	k2eAgInSc1d1	chorvatský
parlament	parlament	k1gInSc1	parlament
na	na	k7c6	na
protější	protější	k2eAgFnSc6d1	protější
straně	strana	k1gFnSc6	strana
Náměstí	náměstí	k1gNnSc1	náměstí
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Gotický	gotický	k2eAgInSc1d1	gotický
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Marka	Marek	k1gMnSc2	Marek
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgNnSc6d1	stejnojmenné
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
přestavěný	přestavěný	k2eAgInSc1d1	přestavěný
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
a	a	k8xC	a
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
1499	[number]	k4	1499
<g/>
.	.	kIx.	.
</s>
<s>
Jezuitský	jezuitský	k2eAgInSc1d1	jezuitský
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Kateřiny	Kateřina	k1gFnPc4	Kateřina
s	s	k7c7	s
kolejí	kolej	k1gFnSc7	kolej
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgInSc1d1	bývalý
klášter	klášter	k1gInSc1	klášter
klarisek	klariska	k1gFnPc2	klariska
<g/>
,	,	kIx,	,
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
konci	konec	k1gInSc6	konec
města	město	k1gNnSc2	město
s	s	k7c7	s
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
hradební	hradební	k2eAgFnSc7d1	hradební
věží	věž	k1gFnSc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kaptol	Kaptola	k1gFnPc2	Kaptola
(	(	kIx(	(
<g/>
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
sv.	sv.	kA	sv.
Štěpána	Štěpána	k1gFnSc1	Štěpána
<g/>
,	,	kIx,	,
vznikala	vznikat	k5eAaImAgFnS	vznikat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
starší	starý	k2eAgFnSc2d2	starší
katedrály	katedrála	k1gFnSc2	katedrála
ze	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
byli	být	k5eAaImAgMnP	být
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
dostavbě	dostavba	k1gFnSc3	dostavba
povoláni	povolán	k2eAgMnPc1d1	povolán
řemeslníci	řemeslník	k1gMnPc1	řemeslník
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
katedrálu	katedrála	k1gFnSc4	katedrála
silně	silně	k6eAd1	silně
poškodilo	poškodit	k5eAaPmAgNnS	poškodit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
a	a	k8xC	a
opatřena	opatřit	k5eAaPmNgFnS	opatřit
novogotickým	novogotický	k2eAgNnSc7d1	novogotické
průčelím	průčelí	k1gNnSc7	průčelí
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
105	[number]	k4	105
<g/>
m	m	kA	m
vysokými	vysoký	k2eAgFnPc7d1	vysoká
věžemi	věž	k1gFnPc7	věž
místo	místo	k7c2	místo
původní	původní	k2eAgFnSc2d1	původní
jedné	jeden	k4xCgFnSc2	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Arcibiskupský	arcibiskupský	k2eAgInSc1d1	arcibiskupský
palác	palác	k1gInSc1	palác
se	s	k7c7	s
třemi	tři	k4xCgNnPc7	tři
křídly	křídlo	k1gNnPc7	křídlo
kolem	kolem	k7c2	kolem
katedrály	katedrála	k1gFnSc2	katedrála
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
silně	silně	k6eAd1	silně
opevněn	opevněn	k2eAgMnSc1d1	opevněn
<g/>
,	,	kIx,	,
zbytky	zbytek	k1gInPc4	zbytek
hradby	hradba	k1gFnSc2	hradba
s	s	k7c7	s
baštami	bašta	k1gFnPc7	bašta
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Náměstí	náměstí	k1gNnSc1	náměstí
bána	bán	k1gMnSc2	bán
Jelačiće	Jelačić	k1gMnSc2	Jelačić
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
nejživější	živý	k2eAgNnSc4d3	nejživější
náměstí	náměstí	k1gNnSc4	náměstí
pod	pod	k7c7	pod
Kaptolem	Kaptol	k1gInSc7	Kaptol
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
středem	střed	k1gInSc7	střed
pěší	pěší	k2eAgFnSc2d1	pěší
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
hlavní	hlavní	k2eAgFnSc4d1	hlavní
obchodní	obchodní	k2eAgFnSc4d1	obchodní
ulici	ulice	k1gFnSc4	ulice
Ilica	Ilic	k1gInSc2	Ilic
<g/>
.	.	kIx.	.
</s>
<s>
Srbská	srbský	k2eAgFnSc1d1	Srbská
ortodoxní	ortodoxní	k2eAgFnSc1d1	ortodoxní
katedrála	katedrála	k1gFnSc1	katedrála
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Cvjetni	Cvjetni	k1gFnSc2	Cvjetni
trg	trg	k?	trg
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatské	chorvatský	k2eAgNnSc1d1	Chorvatské
národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
Zelená	zelený	k2eAgFnSc1d1	zelená
podkova	podkova	k1gFnSc1	podkova
<g/>
,	,	kIx,	,
komplex	komplex	k1gInSc1	komplex
parků	park	k1gInPc2	park
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Dolního	dolní	k2eAgNnSc2d1	dolní
města	město	k1gNnSc2	město
až	až	k9	až
k	k	k7c3	k
nádraží	nádraží	k1gNnSc3	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
navazuje	navazovat	k5eAaImIp3nS	navazovat
Botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
asi	asi	k9	asi
5	[number]	k4	5
ha	ha	kA	ha
s	s	k7c7	s
asi	asi	k9	asi
10	[number]	k4	10
tisíci	tisíc	k4xCgInSc6	tisíc
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hlavních	hlavní	k2eAgInPc2d1	hlavní
chorvatských	chorvatský	k2eAgInPc2d1	chorvatský
dopravních	dopravní	k2eAgInPc2d1	dopravní
tahů	tah	k1gInPc2	tah
prochází	procházet	k5eAaImIp3nS	procházet
přes	přes	k7c4	přes
území	území	k1gNnSc4	území
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Záhřebu	Záhřeb	k1gInSc2	Záhřeb
vede	vést	k5eAaImIp3nS	vést
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc4	pět
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
směřují	směřovat	k5eAaImIp3nP	směřovat
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
částí	část	k1gFnPc2	část
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
také	také	k9	také
procházejí	procházet	k5eAaImIp3nP	procházet
dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
celkem	celek	k1gInSc7	celek
tří	tři	k4xCgInPc2	tři
tzv.	tzv.	kA	tzv.
panevropských	panevropský	k2eAgInPc2d1	panevropský
koridorů	koridor	k1gInPc2	koridor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
přetínají	přetínat	k5eAaImIp3nP	přetínat
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
části	část	k1gFnSc6	část
chorvatské	chorvatský	k2eAgNnSc4d1	Chorvatské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
dálniční	dálniční	k2eAgInSc1d1	dálniční
obchvat	obchvat	k1gInSc1	obchvat
kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
sleduje	sledovat	k5eAaImIp3nS	sledovat
jižní	jižní	k2eAgFnSc4d1	jižní
trasu	trasa	k1gFnSc4	trasa
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
napojují	napojovat	k5eAaImIp3nP	napojovat
významné	významný	k2eAgFnPc4d1	významná
chorvatské	chorvatský	k2eAgFnPc4d1	chorvatská
dálnice	dálnice	k1gFnPc4	dálnice
<g/>
;	;	kIx,	;
A1	A1	k1gFnPc4	A1
do	do	k7c2	do
Dalmácie	Dalmácie	k1gFnSc2	Dalmácie
<g/>
,	,	kIx,	,
A3	A3	k1gFnSc2	A3
do	do	k7c2	do
Slavonie	Slavonie	k1gFnSc2	Slavonie
a	a	k8xC	a
také	také	k9	také
dva	dva	k4xCgInPc4	dva
dopravní	dopravní	k2eAgInPc4d1	dopravní
tahy	tah	k1gInPc4	tah
na	na	k7c4	na
sever	sever	k1gInSc4	sever
do	do	k7c2	do
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
tvoří	tvořit	k5eAaImIp3nP	tvořit
síť	síť	k1gFnSc4	síť
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
a	a	k8xC	a
autobusových	autobusový	k2eAgFnPc2d1	autobusová
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
doplněná	doplněný	k2eAgFnSc1d1	doplněná
příměstskými	příměstský	k2eAgInPc7d1	příměstský
železničními	železniční	k2eAgInPc7d1	železniční
spoji	spoj	k1gInPc7	spoj
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
druhy	druh	k1gInPc7	druh
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
taxi	taxe	k1gFnSc3	taxe
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Páteřním	páteřní	k2eAgInSc7d1	páteřní
druhem	druh	k1gInSc7	druh
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
záhřebský	záhřebský	k2eAgInSc1d1	záhřebský
podnik	podnik	k1gInSc1	podnik
Zagrebački	Zagrebačk	k1gFnSc2	Zagrebačk
električni	električnit	k5eAaPmRp2nS	električnit
tramvaj	tramvaj	k1gFnSc1	tramvaj
(	(	kIx(	(
<g/>
ZET	zet	k1gNnSc1	zet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
provozuje	provozovat	k5eAaImIp3nS	provozovat
i	i	k9	i
lanové	lanový	k2eAgFnPc4d1	lanová
dráhy	dráha	k1gFnPc4	dráha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
překonání	překonání	k1gNnSc2	překonání
výškových	výškový	k2eAgInPc2d1	výškový
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
elektrická	elektrický	k2eAgFnSc1d1	elektrická
tramvaj	tramvaj	k1gFnSc1	tramvaj
vyjela	vyjet	k5eAaPmAgFnS	vyjet
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
a	a	k8xC	a
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
místní	místní	k2eAgFnSc1d1	místní
dopravní	dopravní	k2eAgFnSc1d1	dopravní
síť	síť	k1gFnSc1	síť
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
na	na	k7c4	na
116	[number]	k4	116
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
linek	linka	k1gFnPc2	linka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
patnácti	patnáct	k4xCc2	patnáct
<g/>
,	,	kIx,	,
zajištěny	zajistit	k5eAaPmNgInP	zajistit
jsou	být	k5eAaImIp3nP	být
kromě	kromě	k7c2	kromě
denních	denní	k2eAgFnPc2d1	denní
i	i	k8xC	i
noční	noční	k2eAgInPc1d1	noční
spoje	spoj	k1gInPc1	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
síť	síť	k1gFnSc1	síť
tramvaje	tramvaj	k1gFnSc2	tramvaj
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
a	a	k8xC	a
počet	počet	k1gInSc1	počet
jejích	její	k3xOp3gFnPc2	její
linek	linka	k1gFnPc2	linka
překračuje	překračovat	k5eAaImIp3nS	překračovat
120	[number]	k4	120
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
pak	pak	k6eAd1	pak
slouží	sloužit	k5eAaImIp3nS	sloužit
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
historického	historický	k2eAgNnSc2d1	historické
města	město	k1gNnSc2	město
-	-	kIx~	-
Donji	Donj	k1gFnSc6	Donj
a	a	k8xC	a
Gorni	Goreň	k1gFnSc6	Goreň
grad	grad	k1gInSc1	grad
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
jejich	jejich	k3xOp3gFnSc1	jejich
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
výška	výška	k1gFnSc1	výška
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
město	město	k1gNnSc4	město
ikonická	ikonický	k2eAgFnSc1d1	ikonická
lanovka	lanovka	k1gFnSc1	lanovka
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
nejkratší	krátký	k2eAgFnSc7d3	nejkratší
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Příměstská	příměstský	k2eAgFnSc1d1	příměstská
železnice	železnice	k1gFnSc1	železnice
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
spojení	spojení	k1gNnSc4	spojení
chorvatské	chorvatský	k2eAgFnSc2d1	chorvatská
metropole	metropol	k1gFnSc2	metropol
s	s	k7c7	s
okolními	okolní	k2eAgNnPc7d1	okolní
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
ji	on	k3xPp3gFnSc4	on
používá	používat	k5eAaImIp3nS	používat
kolem	kolem	k7c2	kolem
70	[number]	k4	70
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dojíždějí	dojíždět	k5eAaImIp3nP	dojíždět
do	do	k7c2	do
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
,	,	kIx,	,
za	za	k7c7	za
kulturou	kultura	k1gFnSc7	kultura
<g/>
,	,	kIx,	,
či	či	k8xC	či
rekreací	rekreace	k1gFnSc7	rekreace
<g/>
.	.	kIx.	.
</s>
<s>
Nejvytíženější	vytížený	k2eAgFnSc7d3	nejvytíženější
železniční	železniční	k2eAgFnSc7d1	železniční
tratí	trať	k1gFnSc7	trať
je	být	k5eAaImIp3nS	být
úsek	úsek	k1gInSc1	úsek
Savski	Savski	k1gNnSc1	Savski
Marof	Marof	k1gInSc1	Marof
-	-	kIx~	-
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
a	a	k8xC	a
z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
na	na	k7c4	na
Dugo	Dugo	k1gNnSc4	Dugo
Selo	selo	k1gNnSc1	selo
<g/>
.	.	kIx.	.
</s>
<s>
Příměstské	příměstský	k2eAgInPc1d1	příměstský
spoje	spoj	k1gInPc1	spoj
pak	pak	k6eAd1	pak
také	také	k9	také
jezdí	jezdit	k5eAaImIp3nP	jezdit
do	do	k7c2	do
Veliké	veliký	k2eAgFnSc2d1	veliká
Gorice	Gorice	k1gFnSc2	Gorice
a	a	k8xC	a
do	do	k7c2	do
Jastrebarska	Jastrebarsk	k1gInSc2	Jastrebarsk
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
železniční	železniční	k2eAgFnSc2d1	železniční
sítě	síť	k1gFnSc2	síť
byly	být	k5eAaImAgInP	být
položeny	položen	k2eAgInPc1d1	položen
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
byl	být	k5eAaImAgInS	být
napojen	napojit	k5eAaPmNgInS	napojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
prodloužením	prodloužení	k1gNnSc7	prodloužení
trati	trať	k1gFnSc2	trať
ze	z	k7c2	z
slovinského	slovinský	k2eAgInSc2d1	slovinský
dopravního	dopravní	k2eAgInSc2d1	dopravní
uzlu	uzel	k1gInSc2	uzel
Zidani	Zidan	k1gMnPc1	Zidan
Most	most	k1gInSc4	most
přes	přes	k7c4	přes
chorvatskou	chorvatský	k2eAgFnSc4d1	chorvatská
metropoli	metropole	k1gFnSc4	metropole
do	do	k7c2	do
Sisku	Sisk	k1gInSc2	Sisk
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Rijeky	Rijeka	k1gFnSc2	Rijeka
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
koncem	koncem	k7c2	koncem
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
propojen	propojit	k5eAaPmNgInS	propojit
železničně	železničně	k6eAd1	železničně
s	s	k7c7	s
Koprivnicí	Koprivnice	k1gFnSc7	Koprivnice
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
a	a	k8xC	a
Varaždinem	Varaždino	k1gNnSc7	Varaždino
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
pak	pak	k9	pak
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
trať	trať	k1gFnSc1	trať
přes	přes	k7c4	přes
Liku	Lika	k1gFnSc4	Lika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zajistila	zajistit	k5eAaPmAgFnS	zajistit
spojení	spojení	k1gNnSc4	spojení
po	po	k7c6	po
dráze	dráha	k1gFnSc6	dráha
ze	z	k7c2	z
Záhřebu	Záhřeb	k1gInSc2	Záhřeb
až	až	k6eAd1	až
do	do	k7c2	do
Splitu	Split	k1gInSc2	Split
<g/>
.	.	kIx.	.
</s>
<s>
Záhřebské	záhřebský	k2eAgNnSc1d1	Záhřebské
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
(	(	kIx(	(
<g/>
IATA	IATA	kA	IATA
<g/>
:	:	kIx,	:
ZAG	ZAG	kA	ZAG
<g/>
,	,	kIx,	,
IKAO	IKAO	kA	IKAO
<g/>
:	:	kIx,	:
LDZA	LDZA	kA	LDZA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
přístavem	přístav	k1gInSc7	přístav
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Velika	Velik	k1gMnSc2	Velik
Gorica	Goricus	k1gMnSc2	Goricus
<g/>
,	,	kIx,	,
asi	asi	k9	asi
10	[number]	k4	10
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Záhřebu	Záhřeb	k1gInSc2	Záhřeb
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
jako	jako	k9	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
technické	technický	k2eAgNnSc4d1	technické
zázemí	zázemí	k1gNnSc4	zázemí
pro	pro	k7c4	pro
chorvatské	chorvatský	k2eAgFnPc4d1	chorvatská
aerolinie	aerolinie	k1gFnPc4	aerolinie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
ho	on	k3xPp3gMnSc4	on
využilo	využít	k5eAaPmAgNnS	využít
celkem	celkem	k6eAd1	celkem
1	[number]	k4	1
992	[number]	k4	992
455	[number]	k4	455
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hlavního	hlavní	k2eAgNnSc2d1	hlavní
letiště	letiště	k1gNnSc2	letiště
se	se	k3xPyFc4	se
u	u	k7c2	u
Záhřebu	Záhřeb	k1gInSc2	Záhřeb
nachází	nacházet	k5eAaImIp3nS	nacházet
ještě	ještě	k9	ještě
také	také	k9	také
menší	malý	k2eAgNnSc4d2	menší
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgNnSc4d1	sportovní
(	(	kIx(	(
<g/>
Zračno	Zračno	k1gNnSc4	Zračno
pristanište	pristanišit	k5eAaPmRp2nP	pristanišit
Lučko	Lučko	k1gNnSc4	Lučko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
sloužilo	sloužit	k5eAaImAgNnS	sloužit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945-1962	[number]	k4	1945-1962
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
záhřebské	záhřebský	k2eAgNnSc4d1	Záhřebské
letiště	letiště	k1gNnSc4	letiště
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc4d3	veliký
provoz	provoz	k1gInSc4	provoz
zažilo	zažít	k5eAaPmAgNnS	zažít
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gFnPc7	jeho
branami	brána	k1gFnPc7	brána
prošlo	projít	k5eAaPmAgNnS	projít
167	[number]	k4	167
000	[number]	k4	000
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Eugen	Eugen	k2eAgMnSc1d1	Eugen
Kumičić	Kumičić	k1gMnSc1	Kumičić
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Andrija	Andrija	k1gMnSc1	Andrija
Mohorovičić	Mohorovičić	k1gMnSc1	Mohorovičić
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
seismologie	seismologie	k1gFnSc2	seismologie
Marija	Marija	k1gFnSc1	Marija
Jurić	Jurić	k1gFnSc1	Jurić
Zagorka	Zagorka	k1gFnSc1	Zagorka
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
-	-	kIx~	-
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novinářka	novinářka	k1gFnSc1	novinářka
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Oskar	Oskar	k1gMnSc1	Oskar
Nedbal	Nedbal	k1gMnSc1	Nedbal
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
operetní	operetní	k2eAgMnSc1d1	operetní
skladatel	skladatel	k1gMnSc1	skladatel
Vladimir	Vladimir	k1gMnSc1	Vladimir
Nazor	Nazor	k1gMnSc1	Nazor
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
Mirko	Mirko	k1gMnSc1	Mirko
Kokotović	Kokotović	k1gMnSc1	Kokotović
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Ivica	Ivica	k1gMnSc1	Ivica
Račan	Račan	k1gMnSc1	Račan
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chorvatský	chorvatský	k2eAgMnSc1d1	chorvatský
politik	politik	k1gMnSc1	politik
Goran	Goran	k1gMnSc1	Goran
Prpić	Prpić	k1gMnSc1	Prpić
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
tenista	tenista	k1gMnSc1	tenista
Iva	Iva	k1gFnSc1	Iva
Majoliová	Majoliový	k2eAgFnSc1d1	Majoliová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalá	bývalý	k2eAgFnSc1d1	bývalá
tenistka	tenistka	k1gFnSc1	tenistka
Ivo	Ivo	k1gMnSc1	Ivo
Karlović	Karlović	k1gMnSc1	Karlović
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
Ivica	Ivica	k1gMnSc1	Ivica
Kostelić	Kostelić	k1gMnSc1	Kostelić
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lyžař	lyžař	k1gMnSc1	lyžař
Lovro	Lovro	k1gNnSc1	Lovro
Zovko	Zovko	k1gNnSc1	Zovko
(	(	kIx(	(
<g/>
*	*	kIx~	*
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
Janica	Janica	k1gMnSc1	Janica
Kostelićová	Kostelićová	k1gFnSc1	Kostelićová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalá	bývalý	k2eAgFnSc1d1	bývalá
lyžařka	lyžařka	k1gFnSc1	lyžařka
Sanda	Sanda	k1gFnSc1	Sanda
Mamićová	Mamićová	k1gFnSc1	Mamićová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenistka	tenistka	k1gFnSc1	tenistka
Nika	nika	k1gFnSc1	nika
Ožegovićová	Ožegovićová	k1gFnSc1	Ožegovićová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenistka	tenistka	k1gFnSc1	tenistka
Natko	Natko	k1gNnSc1	Natko
Zrnčić-Dim	Zrnčić-Dim	k1gInSc1	Zrnčić-Dim
(	(	kIx(	(
<g/>
*	*	kIx~	*
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lyžař	lyžař	k1gMnSc1	lyžař
Ajla	Ajla	k1gMnSc1	Ajla
Tomljanovićová	Tomljanovićová	k1gFnSc1	Tomljanovićová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenistka	tenistka	k1gFnSc1	tenistka
</s>
