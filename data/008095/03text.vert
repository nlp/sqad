<s>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
(	(	kIx(	(
<g/>
ČRo	ČRo	k1gFnSc1	ČRo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc4d1	český
veřejnoprávní	veřejnoprávní	k2eAgInSc4d1	veřejnoprávní
rozhlasový	rozhlasový	k2eAgInSc4d1	rozhlasový
subjekt	subjekt	k1gInSc4	subjekt
zřízený	zřízený	k2eAgInSc4d1	zřízený
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
provozuje	provozovat	k5eAaImIp3nS	provozovat
čtyři	čtyři	k4xCgFnPc4	čtyři
celoplošné	celoplošný	k2eAgFnPc4d1	celoplošná
stanice	stanice	k1gFnPc4	stanice
(	(	kIx(	(
<g/>
Radiožurnál	radiožurnál	k1gInSc1	radiožurnál
<g/>
,	,	kIx,	,
Dvojka	dvojka	k1gFnSc1	dvojka
<g/>
,	,	kIx,	,
Vltava	Vltava	k1gFnSc1	Vltava
a	a	k8xC	a
Plus	plus	k1gInSc1	plus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
krajích	kraj	k1gInPc6	kraj
také	také	k9	také
regionální	regionální	k2eAgNnSc4d1	regionální
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
,	,	kIx,	,
digitální	digitální	k2eAgFnSc1d1	digitální
stanice	stanice	k1gFnSc1	stanice
(	(	kIx(	(
<g/>
Rádio	rádio	k1gNnSc1	rádio
Junior	junior	k1gMnSc1	junior
<g/>
,	,	kIx,	,
Radio	radio	k1gNnSc1	radio
Wave	Wave	k1gFnPc2	Wave
<g/>
,	,	kIx,	,
D-dur	Dura	k1gFnPc2	D-dura
<g/>
,	,	kIx,	,
Jazz	jazz	k1gInSc1	jazz
<g/>
)	)	kIx)	)
a	a	k8xC	a
internetové	internetový	k2eAgInPc1d1	internetový
streamy	stream	k1gInPc1	stream
(	(	kIx(	(
<g/>
Rádio	rádio	k1gNnSc1	rádio
Junior	junior	k1gMnSc1	junior
–	–	k?	–
písničky	písnička	k1gFnSc2	písnička
a	a	k8xC	a
příležitostně	příležitostně	k6eAd1	příležitostně
Rádio	rádio	k1gNnSc1	rádio
Retro	retro	k1gNnSc2	retro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
provozuje	provozovat	k5eAaImIp3nS	provozovat
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
státu	stát	k1gInSc2	stát
také	také	k9	také
zahraniční	zahraniční	k2eAgNnSc1d1	zahraniční
vysílání	vysílání	k1gNnSc1	vysílání
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
–	–	k?	–
Radio	radio	k1gNnSc1	radio
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Signál	signál	k1gInSc1	signál
je	být	k5eAaImIp3nS	být
šířen	šířit	k5eAaImNgInS	šířit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
pozemních	pozemní	k2eAgInPc2d1	pozemní
vysílačů	vysílač	k1gInPc2	vysílač
analogového	analogový	k2eAgNnSc2d1	analogové
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
DVB-T	DVB-T	k1gFnPc2	DVB-T
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc4	všechen
celoplošné	celoplošný	k2eAgFnPc4d1	celoplošná
stanice	stanice	k1gFnPc4	stanice
a	a	k8xC	a
4	[number]	k4	4
digitální	digitální	k2eAgInPc1d1	digitální
jsou	být	k5eAaImIp3nP	být
vysílány	vysílat	k5eAaImNgInP	vysílat
v	v	k7c6	v
digitálním	digitální	k2eAgInSc6d1	digitální
multiplexu	multiplex	k1gInSc6	multiplex
č.	č.	k?	č.
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gNnSc1	jeho
stanice	stanice	k1gFnPc1	stanice
šířeny	šířen	k2eAgFnPc1d1	šířena
v	v	k7c6	v
kabelových	kabelový	k2eAgFnPc6d1	kabelová
televizích	televize	k1gFnPc6	televize
(	(	kIx(	(
<g/>
DVB-C	DVB-C	k1gFnSc1	DVB-C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
IPTV	IPTV	kA	IPTV
<g/>
,	,	kIx,	,
nekódovaně	kódovaně	k6eNd1	kódovaně
na	na	k7c6	na
satelitu	satelit	k1gInSc6	satelit
Astra	astra	k1gFnSc1	astra
3A	[number]	k4	3A
(	(	kIx(	(
<g/>
DVB-S	DVB-S	k1gFnSc2	DVB-S
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
a	a	k8xC	a
v	v	k7c6	v
DAB	DAB	kA	DAB
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
používá	používat	k5eAaImIp3nS	používat
jako	jako	k8xC	jako
logo	logo	k1gNnSc4	logo
symbol	symbol	k1gInSc1	symbol
R	R	kA	R
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
čtyřmi	čtyři	k4xCgInPc7	čtyři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
v	v	k7c6	v
tmavě	tmavě	k6eAd1	tmavě
modré	modrý	k2eAgFnSc6d1	modrá
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
stanice	stanice	k1gFnPc1	stanice
jsou	být	k5eAaImIp3nP	být
odlišeny	odlišit	k5eAaPmNgFnP	odlišit
barevně	barevně	k6eAd1	barevně
(	(	kIx(	(
<g/>
Radiožurnál	radiožurnál	k1gInSc1	radiožurnál
–	–	k?	–
červená	červená	k1gFnSc1	červená
<g/>
,	,	kIx,	,
Dvojka	dvojka	k1gFnSc1	dvojka
–	–	k?	–
fialová	fialový	k2eAgFnSc1d1	fialová
<g/>
,	,	kIx,	,
Vltava	Vltava	k1gFnSc1	Vltava
–	–	k?	–
světle	světle	k6eAd1	světle
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
Plus	plus	k6eAd1	plus
–	–	k?	–
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
,	,	kIx,	,
Radio	radio	k1gNnSc1	radio
Wave	Wav	k1gInSc2	Wav
–	–	k?	–
zlatá	zlatý	k2eAgFnSc1d1	zlatá
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgFnSc1d1	regionální
stanice	stanice	k1gFnSc1	stanice
–	–	k?	–
zelené	zelená	k1gFnSc2	zelená
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
stanice	stanice	k1gFnPc1	stanice
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgFnPc1d1	speciální
stanice	stanice	k1gFnPc1	stanice
–	–	k?	–
tmavě	tmavě	k6eAd1	tmavě
modré	modré	k1gNnSc1	modré
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vydává	vydávat	k5eAaImIp3nS	vydávat
periodikum	periodikum	k1gNnSc1	periodikum
Týdeník	týdeník	k1gInSc1	týdeník
Rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Radiojournal	radiojournal	k1gInSc1	radiojournal
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
pravidelně	pravidelně	k6eAd1	pravidelně
vysílat	vysílat	k5eAaImF	vysílat
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1923	[number]	k4	1923
ve	v	k7c4	v
20.15	[number]	k4	20.15
hod	hod	k1gInSc4	hod
<g/>
.	.	kIx.	.
ze	z	k7c2	z
stanu	stan	k1gInSc2	stan
u	u	k7c2	u
letiště	letiště	k1gNnSc2	letiště
v	v	k7c6	v
Kbelích	Kbely	k1gInPc6	Kbely
<g/>
.	.	kIx.	.
</s>
<s>
Déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
poslech	poslech	k1gInSc4	poslech
programu	program	k1gInSc2	program
nepříjemně	příjemně	k6eNd1	příjemně
rušen	rušit	k5eAaImNgInS	rušit
stálým	stálý	k2eAgInSc7d1	stálý
tónem	tón	k1gInSc7	tón
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
500	[number]	k4	500
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
technická	technický	k2eAgFnSc1d1	technická
závada	závada	k1gFnSc1	závada
byla	být	k5eAaImAgFnS	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
nedostatečnou	dostatečný	k2eNgFnSc7d1	nedostatečná
filtrací	filtrace	k1gFnSc7	filtrace
usměrněného	usměrněný	k2eAgInSc2d1	usměrněný
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
až	až	k9	až
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
vysílání	vysílání	k1gNnSc2	vysílání
několikrát	několikrát	k6eAd1	několikrát
změnilo	změnit	k5eAaPmAgNnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
improvizované	improvizovaný	k2eAgNnSc1d1	improvizované
studio	studio	k1gNnSc1	studio
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
do	do	k7c2	do
Hloubětína	Hloubětín	k1gInSc2	Hloubětín
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Kbel	Kbely	k1gInPc2	Kbely
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
do	do	k7c2	do
Poštovní	poštovní	k2eAgFnSc2d1	poštovní
nákupny	nákupna	k1gFnSc2	nákupna
na	na	k7c6	na
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Fochově	Fochova	k1gFnSc6	Fochova
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Vinohradské	vinohradský	k2eAgNnSc1d1	Vinohradské
<g/>
)	)	kIx)	)
ulici	ulice	k1gFnSc4	ulice
č.	č.	k?	č.
58	[number]	k4	58
–	–	k?	–
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc4	ten
první	první	k4xOgNnSc4	první
studio	studio	k1gNnSc4	studio
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
přišlo	přijít	k5eAaPmAgNnS	přijít
další	další	k2eAgNnSc1d1	další
stěhování	stěhování	k1gNnSc1	stěhování
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
do	do	k7c2	do
Orbisu	orbis	k1gInSc2	orbis
na	na	k7c6	na
Fochově	Fochův	k2eAgFnSc6d1	Fochův
ulici	ulice	k1gFnSc6	ulice
č.	č.	k?	č.
62	[number]	k4	62
(	(	kIx(	(
<g/>
poblíž	poblíž	k7c2	poblíž
Vinohradské	vinohradský	k2eAgFnSc2d1	Vinohradská
tržnice	tržnice	k1gFnSc2	tržnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
se	se	k3xPyFc4	se
vysílací	vysílací	k2eAgNnSc1d1	vysílací
studio	studio	k1gNnSc1	studio
přemístilo	přemístit	k5eAaPmAgNnS	přemístit
do	do	k7c2	do
Národního	národní	k2eAgInSc2d1	národní
domu	dům	k1gInSc2	dům
na	na	k7c6	na
Náměstí	náměstí	k1gNnSc6	náměstí
Míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
Radiojournal	radiojournal	k1gInSc1	radiojournal
vysílal	vysílat	k5eAaImAgInS	vysílat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
už	už	k6eAd1	už
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
přemístil	přemístit	k5eAaPmAgInS	přemístit
do	do	k7c2	do
dnešního	dnešní	k2eAgNnSc2d1	dnešní
sídla	sídlo	k1gNnSc2	sídlo
do	do	k7c2	do
Vinohradské	vinohradský	k2eAgFnSc2d1	Vinohradská
ulice	ulice	k1gFnSc2	ulice
č.	č.	k?	č.
12	[number]	k4	12
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
Ředitelství	ředitelství	k1gNnSc2	ředitelství
pošt	pošta	k1gFnPc2	pošta
a	a	k8xC	a
telegrafů	telegraf	k1gInPc2	telegraf
<g/>
.	.	kIx.	.
</s>
<s>
Památkově	památkově	k6eAd1	památkově
chráněná	chráněný	k2eAgFnSc1d1	chráněná
budova	budova	k1gFnSc1	budova
Československého	československý	k2eAgInSc2d1	československý
respektive	respektive	k9	respektive
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
na	na	k7c6	na
Vinohradské	vinohradský	k2eAgFnSc6d1	Vinohradská
třídě	třída	k1gFnSc6	třída
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
hrála	hrát	k5eAaImAgFnS	hrát
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
moderních	moderní	k2eAgFnPc6d1	moderní
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
svedeny	sveden	k2eAgInPc1d1	sveden
těžké	těžký	k2eAgInPc1d1	těžký
boje	boj	k1gInPc1	boj
během	během	k7c2	během
Pražského	pražský	k2eAgNnSc2d1	Pražské
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1968	[number]	k4	1968
během	během	k7c2	během
invaze	invaze	k1gFnSc2	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
došlo	dojít	k5eAaPmAgNnS	dojít
před	před	k7c7	před
rozhlasem	rozhlas	k1gInSc7	rozhlas
k	k	k7c3	k
masakru	masakr	k1gInSc3	masakr
cilistů	cilista	k1gMnPc2	cilista
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
zákonem	zákon	k1gInSc7	zákon
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
484	[number]	k4	484
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
subjekt	subjekt	k1gInSc1	subjekt
funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
rozpadu	rozpad	k1gInSc2	rozpad
Československé	československý	k2eAgFnSc2d1	Československá
federativní	federativní	k2eAgFnSc2d1	federativní
republiky	republika	k1gFnSc2	republika
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
nejstarší	starý	k2eAgInSc4d3	nejstarší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
po	po	k7c6	po
anglickém	anglický	k2eAgNnSc6d1	anglické
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
484	[number]	k4	484
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
orgánem	orgán	k1gInSc7	orgán
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
právo	právo	k1gNnSc1	právo
veřejnosti	veřejnost	k1gFnSc2	veřejnost
na	na	k7c4	na
kontrolu	kontrola	k1gFnSc4	kontrola
tvorby	tvorba	k1gFnSc2	tvorba
a	a	k8xC	a
šíření	šíření	k1gNnSc2	šíření
programů	program	k1gInPc2	program
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
organizační	organizační	k2eAgFnSc2d1	organizační
struktury	struktura	k1gFnSc2	struktura
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
9	[number]	k4	9
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
občanských	občanský	k2eAgNnPc2d1	občanské
sdružení	sdružení	k1gNnPc2	sdružení
<g/>
,	,	kIx,	,
spolků	spolek	k1gInPc2	spolek
a	a	k8xC	a
hnutí	hnutí	k1gNnSc2	hnutí
volí	volit	k5eAaImIp3nS	volit
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
však	však	k9	však
sněmovna	sněmovna	k1gFnSc1	sněmovna
nezvolila	zvolit	k5eNaPmAgFnS	zvolit
žádného	žádný	k3yNgMnSc4	žádný
člena	člen	k1gMnSc4	člen
Rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
proto	proto	k6eAd1	proto
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
nekompletním	kompletní	k2eNgInSc6d1	nekompletní
počtu	počet	k1gInSc6	počet
5	[number]	k4	5
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
šestým	šestý	k4xOgMnSc7	šestý
členem	člen	k1gMnSc7	člen
Rady	rada	k1gFnSc2	rada
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Milan	Milan	k1gMnSc1	Milan
Badal	badat	k5eAaImAgMnS	badat
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
počet	počet	k1gInSc1	počet
radních	radní	k1gMnPc2	radní
stoupl	stoupnout	k5eAaPmAgInS	stoupnout
na	na	k7c4	na
6	[number]	k4	6
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
hlasů	hlas	k1gInPc2	hlas
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
484	[number]	k4	484
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
minimální	minimální	k2eAgInSc4d1	minimální
počet	počet	k1gInSc4	počet
pro	pro	k7c4	pro
zvolení	zvolení	k1gNnSc4	zvolení
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Institut	institut	k1gInSc1	institut
ombudsmana	ombudsman	k1gMnSc2	ombudsman
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
smyslem	smysl	k1gInSc7	smysl
je	být	k5eAaImIp3nS	být
prohloubení	prohloubení	k1gNnSc1	prohloubení
dialogu	dialog	k1gInSc2	dialog
mezi	mezi	k7c7	mezi
posluchači	posluchač	k1gMnPc7	posluchač
a	a	k8xC	a
Českým	český	k2eAgInSc7d1	český
rozhlasem	rozhlas	k1gInSc7	rozhlas
jako	jako	k8xS	jako
médiem	médium	k1gNnSc7	médium
veřejné	veřejný	k2eAgFnSc2d1	veřejná
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Posluchači	posluchač	k1gMnPc1	posluchač
mohou	moct	k5eAaImIp3nP	moct
ombudsmanovi	ombudsmanův	k2eAgMnPc1d1	ombudsmanův
předávat	předávat	k5eAaImF	předávat
své	svůj	k3xOyFgInPc4	svůj
podněty	podnět	k1gInPc4	podnět
k	k	k7c3	k
činnosti	činnost	k1gFnSc3	činnost
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Ombudsman	ombudsman	k1gMnSc1	ombudsman
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
fungování	fungování	k1gNnSc4	fungování
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
jako	jako	k8xS	jako
veřejnoprávního	veřejnoprávní	k2eAgNnSc2d1	veřejnoprávní
média	médium	k1gNnSc2	médium
a	a	k8xC	a
hájí	hájit	k5eAaImIp3nP	hájit
oprávněné	oprávněný	k2eAgInPc4d1	oprávněný
zájmy	zájem	k1gInPc4	zájem
posluchačů	posluchač	k1gMnPc2	posluchač
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
post	post	k1gInSc4	post
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zastává	zastávat	k5eAaImIp3nS	zastávat
PhDr.	PhDr.	kA	PhDr.
Milan	Milan	k1gMnSc1	Milan
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
stojí	stát	k5eAaImIp3nS	stát
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
Radou	rada	k1gFnSc7	rada
ČRo	ČRo	k1gFnPc2	ČRo
na	na	k7c4	na
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
byl	být	k5eAaImAgInS	být
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Richard	Richard	k1gMnSc1	Richard
Medek	Medek	k1gMnSc1	Medek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pozici	pozice	k1gFnSc4	pozice
přechodně	přechodně	k6eAd1	přechodně
zastával	zastávat	k5eAaImAgMnS	zastávat
už	už	k6eAd1	už
od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
odvolán	odvolán	k2eAgMnSc1d1	odvolán
Václav	Václav	k1gMnSc1	Václav
Kasík	Kasík	k1gMnSc1	Kasík
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Richard	Richard	k1gMnSc1	Richard
Medek	Medek	k1gMnSc1	Medek
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
Rady	rada	k1gFnSc2	rada
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
že	že	k8xS	že
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
měsíce	měsíc	k1gInSc2	měsíc
rezignuje	rezignovat	k5eAaBmIp3nS	rezignovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
následně	následně	k6eAd1	následně
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
prozatímním	prozatímní	k2eAgMnSc7d1	prozatímní
ředitelem	ředitel	k1gMnSc7	ředitel
Petera	Peter	k1gMnSc2	Peter
Duhana	Duhan	k1gMnSc2	Duhan
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
mezitím	mezitím	k6eAd1	mezitím
vypršely	vypršet	k5eAaPmAgInP	vypršet
mandáty	mandát	k1gInPc4	mandát
některých	některý	k3yIgInPc2	některý
členů	člen	k1gInPc2	člen
Rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
klesl	klesnout	k5eAaPmAgInS	klesnout
počet	počet	k1gInSc1	počet
radních	radní	k1gMnPc2	radní
na	na	k7c4	na
5	[number]	k4	5
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemohla	moct	k5eNaImAgFnS	moct
přistoupit	přistoupit	k5eAaPmF	přistoupit
k	k	k7c3	k
volbě	volba	k1gFnSc3	volba
řádného	řádný	k2eAgMnSc2d1	řádný
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
(	(	kIx(	(
<g/>
k	k	k7c3	k
té	ten	k3xDgFnSc3	ten
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
alespoň	alespoň	k9	alespoň
6	[number]	k4	6
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
Peter	Peter	k1gMnSc1	Peter
Duhan	Duhan	k1gMnSc1	Duhan
zvolen	zvolen	k2eAgMnSc1d1	zvolen
již	již	k6eAd1	již
kompletní	kompletní	k2eAgFnSc7d1	kompletní
Radou	rada	k1gFnSc7	rada
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k2eAgFnPc1d1	vedoucí
osoby	osoba	k1gFnPc1	osoba
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
Richard	Richard	k1gMnSc1	Richard
Gemperle	Gemperl	k1gMnSc2	Gemperl
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1923	[number]	k4	1923
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
jednatelského	jednatelský	k2eAgInSc2d1	jednatelský
sboru	sbor	k1gInSc2	sbor
Radiojournalu	radiojournal	k1gInSc2	radiojournal
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šourek	Šourek	k1gMnSc1	Šourek
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1925	[number]	k4	1925
–	–	k?	–
listopad	listopad	k1gInSc1	listopad
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
jednatelského	jednatelský	k2eAgInSc2d1	jednatelský
sboru	sbor	k1gInSc2	sbor
<g />
.	.	kIx.	.
</s>
<s>
Radiojournalu	radiojournal	k1gInSc2	radiojournal
Jindřich	Jindřich	k1gMnSc1	Jindřich
Dobiáš	Dobiáš	k1gMnSc1	Dobiáš
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
1938	[number]	k4	1938
–	–	k?	–
květen	květen	k1gInSc1	květen
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
jednatelského	jednatelský	k2eAgInSc2d1	jednatelský
sboru	sbor	k1gInSc2	sbor
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
Hubert	Hubert	k1gMnSc1	Hubert
Masařík	Masařík	k1gMnSc1	Masařík
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1940	[number]	k4	1940
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
jednatelského	jednatelský	k2eAgInSc2d1	jednatelský
sboru	sbor	k1gInSc2	sbor
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Thürmer	Thürmer	k1gMnSc1	Thürmer
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
1942	[number]	k4	1942
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
–	–	k?	–
vedoucí	vedoucí	k1gMnPc1	vedoucí
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
skupiny	skupina	k1gFnSc2	skupina
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
Otakar	Otakar	k1gMnSc1	Otakar
Matoušek	Matoušek	k1gMnSc1	Matoušek
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
–	–	k?	–
pověřenec	pověřenec	k1gMnSc1	pověřenec
ČNR	ČNR	kA	ČNR
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
<g />
.	.	kIx.	.
</s>
<s>
Laštovička	laštovička	k1gFnSc1	laštovička
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
Kazimír	Kazimír	k1gMnSc1	Kazimír
Stahl	Stahl	k1gMnSc1	Stahl
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1948	[number]	k4	1948
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
Josef	Josef	k1gMnSc1	Josef
Věromír	Věromír	k1gMnSc1	Věromír
Pleva	Pleva	k1gMnSc1	Pleva
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1952	[number]	k4	1952
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
–	–	k?	–
pověřen	pověřit	k5eAaPmNgMnS	pověřit
řízením	řízení	k1gNnSc7	řízení
Václav	Václav	k1gMnSc1	Václav
Kopecký	Kopecký	k1gMnSc1	Kopecký
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1952	[number]	k4	1952
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1953	[number]	k4	1953
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Československého	československý	k2eAgInSc2d1	československý
rozhlasového	rozhlasový	k2eAgInSc2d1	rozhlasový
výboru	výbor	k1gInSc2	výbor
Jozef	Jozef	k1gMnSc1	Jozef
Vrabec	Vrabec	k1gMnSc1	Vrabec
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1952	[number]	k4	1952
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
–	–	k?	–
první	první	k4xOgFnSc6	první
náměstek	náměstek	k1gMnSc1	náměstek
předsedy	předseda	k1gMnSc2	předseda
Československého	československý	k2eAgInSc2d1	československý
rozhlasového	rozhlasový	k2eAgInSc2d1	rozhlasový
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
pověřený	pověřený	k2eAgMnSc1d1	pověřený
řízením	řízení	k1gNnSc7	řízení
ČsRo	ČsRo	k1gMnSc1	ČsRo
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1953	[number]	k4	1953
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
František	František	k1gMnSc1	František
Nečásek	Nečásek	k1gMnSc1	Nečásek
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1954	[number]	k4	1954
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
–	–	k?	–
ústřední	ústřední	k2eAgMnSc1d1	ústřední
ředitel	ředitel	k1gMnSc1	ředitel
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
a	a	k8xC	a
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
Jaromír	Jaromír	k1gMnSc1	Jaromír
Hřebík	hřebík	k1gInSc1	hřebík
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1958	[number]	k4	1958
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
–	–	k?	–
ústřední	ústřední	k2eAgMnSc1d1	ústřední
ředitel	ředitel	k1gMnSc1	ředitel
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
Karel	Karel	k1gMnSc1	Karel
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1959	[number]	k4	1959
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
–	–	k?	–
ústřední	ústřední	k2eAgMnSc1d1	ústřední
ředitel	ředitel	k1gMnSc1	ředitel
Miloš	Miloš	k1gMnSc1	Miloš
Marko	Marko	k1gMnSc1	Marko
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
–	–	k?	–
ústřední	ústřední	k2eAgMnSc1d1	ústřední
ředitel	ředitel	k1gMnSc1	ředitel
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hejzlar	Hejzlar	k1gMnSc1	Hejzlar
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1968	[number]	k4	1968
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
–	–	k?	–
ústřední	ústřední	k2eAgMnSc1d1	ústřední
ředitel	ředitel	k1gMnSc1	ředitel
Odon	Odon	k1gMnSc1	Odon
Závodský	Závodský	k2eAgMnSc1d1	Závodský
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
<g />
.	.	kIx.	.
</s>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
1968	[number]	k4	1968
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
–	–	k?	–
vládní	vládní	k2eAgMnSc1d1	vládní
zmocněnec	zmocněnec	k1gMnSc1	zmocněnec
pro	pro	k7c4	pro
Československý	československý	k2eAgInSc4d1	československý
rozhlas	rozhlas	k1gInSc4	rozhlas
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
–	–	k?	–
ústřední	ústřední	k2eAgMnSc1d1	ústřední
ředitel	ředitel	k1gMnSc1	ředitel
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
<g />
.	.	kIx.	.
</s>
<s>
Chňoupek	Chňoupek	k1gInSc1	Chňoupek
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1969	[number]	k4	1969
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
–	–	k?	–
ústřední	ústřední	k2eAgMnSc1d1	ústřední
ředitel	ředitel	k1gMnSc1	ředitel
Ján	Ján	k1gMnSc1	Ján
Riško	Riško	k1gNnSc4	Riško
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1970	[number]	k4	1970
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
–	–	k?	–
ústřední	ústřední	k2eAgMnSc1d1	ústřední
ředitel	ředitel	k1gMnSc1	ředitel
Karel	Karel	k1gMnSc1	Karel
Kvapil	Kvapil	k1gMnSc1	Kvapil
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
–	–	k?	–
ústřední	ústřední	k2eAgMnSc1d1	ústřední
ředitel	ředitel	k1gMnSc1	ředitel
Karel	Karel	k1gMnSc1	Karel
Starý	Starý	k1gMnSc1	Starý
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
–	–	k?	–
ústřední	ústřední	k2eAgMnSc1d1	ústřední
ředitel	ředitel	k1gMnSc1	ředitel
František	František	k1gMnSc1	František
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
–	–	k?	–
ústřední	ústřední	k2eAgMnSc1d1	ústřední
ředitel	ředitel	k1gMnSc1	ředitel
Richard	Richard	k1gMnSc1	Richard
Seemann	Seemann	k1gMnSc1	Seemann
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
–	–	k?	–
pověřen	pověřit	k5eAaPmNgInS	pověřit
výkonem	výkon	k1gInSc7	výkon
funkce	funkce	k1gFnSc2	funkce
ústředního	ústřední	k2eAgMnSc2d1	ústřední
ředitele	ředitel	k1gMnSc2	ředitel
Peter	Peter	k1gMnSc1	Peter
Duhan	Duhan	k1gMnSc1	Duhan
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
–	–	k?	–
ústřední	ústřední	k2eAgMnSc1d1	ústřední
ředitel	ředitel	k1gMnSc1	ředitel
Ředitelé	ředitel	k1gMnPc1	ředitel
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Rostislav	Rostislav	k1gMnSc1	Rostislav
Běhal	Běhal	k1gMnSc1	Běhal
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1969	[number]	k4	1969
–	–	k?	–
červen	červen	k1gInSc1	červen
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
–	–	k?	–
při	při	k7c6	při
konstituování	konstituování	k1gNnSc6	konstituování
federativního	federativní	k2eAgNnSc2d1	federativní
uspořádání	uspořádání	k1gNnSc2	uspořádání
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
pověřen	pověřit	k5eAaPmNgInS	pověřit
prozatímním	prozatímní	k2eAgNnSc7d1	prozatímní
vedením	vedení	k1gNnSc7	vedení
Českého	český	k2eAgInSc2d1	český
<g />
.	.	kIx.	.
</s>
<s>
rozhlasu	rozhlas	k1gInSc2	rozhlas
Karel	Karel	k1gMnSc1	Karel
Hrabal	Hrabal	k1gMnSc1	Hrabal
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
1969	[number]	k4	1969
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
–	–	k?	–
ředitel	ředitel	k1gMnSc1	ředitel
Karel	Karel	k1gMnSc1	Karel
Kvapil	Kvapil	k1gMnSc1	Kvapil
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1985	[number]	k4	1985
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
–	–	k?	–
ředitel	ředitel	k1gMnSc1	ředitel
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hacmac	Hacmac	k1gFnSc1	Hacmac
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1989	[number]	k4	1989
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
–	–	k?	–
ředitel	ředitel	k1gMnSc1	ředitel
Jan	Jan	k1gMnSc1	Jan
Czech	Czech	k1gMnSc1	Czech
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
–	–	k?	–
ředitel	ředitel	k1gMnSc1	ředitel
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Fridrich	Fridrich	k1gMnSc1	Fridrich
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
–	–	k?	–
ředitel	ředitel	k1gMnSc1	ředitel
Jiří	Jiří	k1gMnSc1	Jiří
Mejstřík	Mejstřík	k1gMnSc1	Mejstřík
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
1991	[number]	k4	1991
–	–	k?	–
květen	květen	k1gInSc1	květen
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
–	–	k?	–
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Ježek	Ježek	k1gMnSc1	Ježek
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Václav	Václav	k1gMnSc1	Václav
Kasík	Kasík	k1gMnSc1	Kasík
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Richard	Richard	k1gMnSc1	Richard
Medek	Medek	k1gMnSc1	Medek
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
–	–	k?	–
prozatímní	prozatímní	k2eAgMnSc1d1	prozatímní
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Peter	Peter	k1gMnSc1	Peter
Duhan	Duhan	k1gMnSc1	Duhan
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
–	–	k?	–
prozatímní	prozatímní	k2eAgMnSc1d1	prozatímní
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
)	)	kIx)	)
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Karel	Karel	k1gMnSc1	Karel
Zýka	Zýka	k1gMnSc1	Zýka
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
prozatímní	prozatímní	k2eAgMnSc1d1	prozatímní
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
René	René	k1gMnSc1	René
Zavoral	Zavoral	k1gMnSc1	Zavoral
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
–	–	k?	–
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Většinu	většina	k1gFnSc4	většina
příjmů	příjem	k1gInPc2	příjem
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
tvoří	tvořit	k5eAaImIp3nP	tvořit
rozhlasové	rozhlasový	k2eAgFnPc1d1	rozhlasová
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
koncesionářské	koncesionářský	k2eAgInPc4d1	koncesionářský
<g/>
)	)	kIx)	)
poplatky	poplatek	k1gInPc4	poplatek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
výše	výše	k1gFnSc1	výše
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
stanovena	stanovit	k5eAaPmNgFnS	stanovit
na	na	k7c4	na
45	[number]	k4	45
Kč	Kč	kA	Kč
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
</s>
<s>
Domácnosti	domácnost	k1gFnPc1	domácnost
platí	platit	k5eAaImIp3nP	platit
jeden	jeden	k4xCgInSc4	jeden
poplatek	poplatek	k1gInSc4	poplatek
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
počet	počet	k1gInSc4	počet
přijímačů	přijímač	k1gInPc2	přijímač
<g/>
,	,	kIx,	,
podnikatelé	podnikatel	k1gMnPc1	podnikatel
hradí	hradit	k5eAaImIp3nP	hradit
poplatek	poplatek	k1gInSc4	poplatek
za	za	k7c4	za
každý	každý	k3xTgInSc4	každý
jeden	jeden	k4xCgInSc4	jeden
přijímač	přijímač	k1gInSc4	přijímač
<g/>
.	.	kIx.	.
</s>
<s>
Rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
poplatek	poplatek	k1gInSc1	poplatek
není	být	k5eNaImIp3nS	být
příjmem	příjem	k1gInSc7	příjem
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
ho	on	k3xPp3gMnSc4	on
vybírá	vybírat	k5eAaImIp3nS	vybírat
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
České	český	k2eAgFnSc2d1	Česká
pošty	pošta	k1gFnSc2	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Zbylých	zbylý	k2eAgNnPc2d1	zbylé
několik	několik	k4yIc1	několik
procent	procento	k1gNnPc2	procento
příjmů	příjem	k1gInPc2	příjem
tvoří	tvořit	k5eAaImIp3nS	tvořit
výnosy	výnos	k1gInPc4	výnos
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
obchodní	obchodní	k2eAgFnSc2d1	obchodní
činnosti	činnost	k1gFnSc2	činnost
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
například	například	k6eAd1	například
patří	patřit	k5eAaImIp3nS	patřit
limitovaný	limitovaný	k2eAgInSc4d1	limitovaný
prodej	prodej	k1gInSc4	prodej
reklamního	reklamní	k2eAgInSc2d1	reklamní
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
výnosy	výnos	k1gInPc1	výnos
ze	z	k7c2	z
sponzoringu	sponzoring	k1gInSc2	sponzoring
nebo	nebo	k8xC	nebo
licencování	licencování	k1gNnSc2	licencování
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
předkládá	předkládat	k5eAaImIp3nS	předkládat
Poslanecké	poslanecký	k2eAgFnSc3d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
výroční	výroční	k2eAgInSc4d1	výroční
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c4	o
hospodaření	hospodaření	k1gNnSc4	hospodaření
ČRo	ČRo	k1gFnSc2	ČRo
v	v	k7c6	v
uplynulém	uplynulý	k2eAgInSc6d1	uplynulý
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc1	dokument
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
podrobný	podrobný	k2eAgInSc4d1	podrobný
rozbor	rozbor	k1gInSc4	rozbor
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
přehled	přehled	k1gInSc4	přehled
o	o	k7c6	o
peněžních	peněžní	k2eAgInPc6d1	peněžní
tocích	tok	k1gInPc6	tok
<g/>
,	,	kIx,	,
přehled	přehled	k1gInSc1	přehled
pohledávek	pohledávka	k1gFnPc2	pohledávka
<g/>
,	,	kIx,	,
mzdovou	mzdový	k2eAgFnSc4d1	mzdová
a	a	k8xC	a
honorářovou	honorářový	k2eAgFnSc4d1	Honorářová
analýzu	analýza	k1gFnSc4	analýza
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
auditorskou	auditorský	k2eAgFnSc4d1	auditorská
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovými	klíčový	k2eAgMnPc7d1	klíčový
ukazateli	ukazatel	k1gMnPc7	ukazatel
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
a	a	k8xC	a
výnosy	výnos	k1gInPc1	výnos
<g/>
,	,	kIx,	,
objem	objem	k1gInSc1	objem
vybraných	vybraný	k2eAgInPc2d1	vybraný
rozhlasových	rozhlasový	k2eAgInPc2d1	rozhlasový
poplatků	poplatek	k1gInPc2	poplatek
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
celkových	celkový	k2eAgInPc6d1	celkový
výnosech	výnos	k1gInPc6	výnos
<g/>
.	.	kIx.	.
</s>
<s>
Zprávu	zpráva	k1gFnSc4	zpráva
po	po	k7c4	po
doručení	doručení	k1gNnSc4	doručení
do	do	k7c2	do
sněmovny	sněmovna	k1gFnSc2	sněmovna
projednává	projednávat	k5eAaImIp3nS	projednávat
stálá	stálý	k2eAgFnSc1d1	stálá
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
sdělovací	sdělovací	k2eAgInPc4d1	sdělovací
prostředky	prostředek	k1gInPc4	prostředek
a	a	k8xC	a
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc4	školství
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
schválení	schválení	k1gNnSc6	schválení
nebo	nebo	k8xC	nebo
zamítnutí	zamítnutí	k1gNnSc6	zamítnutí
zprávy	zpráva	k1gFnSc2	zpráva
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
hlasuje	hlasovat	k5eAaImIp3nS	hlasovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
schůze	schůze	k1gFnSc2	schůze
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Radiožurnál	radiožurnál	k1gInSc1	radiožurnál
–	–	k?	–
"	"	kIx"	"
<g/>
moderní	moderní	k2eAgNnSc1d1	moderní
informační	informační	k2eAgNnSc1d1	informační
rádio	rádio	k1gNnSc1	rádio
<g/>
"	"	kIx"	"
Dvojka	dvojka	k1gFnSc1	dvojka
–	–	k?	–
generační	generační	k2eAgFnSc1d1	generační
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
stanice	stanice	k1gFnSc1	stanice
Vltava	Vltava	k1gFnSc1	Vltava
–	–	k?	–
stanice	stanice	k1gFnSc1	stanice
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
kulturu	kultura	k1gFnSc4	kultura
Plus	plus	k6eAd1	plus
–	–	k?	–
stanice	stanice	k1gFnSc1	stanice
mluveného	mluvený	k2eAgNnSc2d1	mluvené
slova	slovo	k1gNnSc2	slovo
Studia	studio	k1gNnSc2	studio
zajišťující	zajišťující	k2eAgNnSc4d1	zajišťující
regionální	regionální	k2eAgNnSc4d1	regionální
vysílání	vysílání	k1gNnSc4	vysílání
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
:	:	kIx,	:
ČRo	ČRo	k1gMnPc1	ČRo
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
vysílání	vysílání	k1gNnSc4	vysílání
pro	pro	k7c4	pro
Jihomoravský	jihomoravský	k2eAgInSc4d1	jihomoravský
i	i	k8xC	i
Zlínský	zlínský	k2eAgInSc4d1	zlínský
kraj	kraj	k1gInSc4	kraj
<g/>
)	)	kIx)	)
ČRo	ČRo	k1gFnSc4	ČRo
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
ČRo	ČRo	k1gFnSc2	ČRo
<g />
.	.	kIx.	.
</s>
<s>
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
ČRo	ČRo	k1gFnSc2	ČRo
Olomouc	Olomouc	k1gFnSc1	Olomouc
ČRo	ČRo	k1gFnSc1	ČRo
Ostrava	Ostrava	k1gFnSc1	Ostrava
ČRo	ČRo	k1gFnSc2	ČRo
Pardubice	Pardubice	k1gInPc1	Pardubice
ČRo	ČRo	k1gMnSc2	ČRo
Plzeň	Plzeň	k1gFnSc1	Plzeň
ČRo	ČRo	k1gMnSc1	ČRo
Region	region	k1gInSc1	region
<g/>
,	,	kIx,	,
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
(	(	kIx(	(
<g/>
Střední	střední	k2eAgFnPc1d1	střední
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
)	)	kIx)	)
ČRo	ČRo	k1gFnSc1	ČRo
Region	region	k1gInSc1	region
<g/>
,	,	kIx,	,
Vysočina	vysočina	k1gFnSc1	vysočina
ČRo	ČRo	k1gFnSc2	ČRo
Sever	sever	k1gInSc1	sever
ČRo	ČRo	k1gMnSc1	ČRo
Liberec	Liberec	k1gInSc1	Liberec
Rádio	rádio	k1gNnSc1	rádio
Junior	junior	k1gMnSc1	junior
–	–	k?	–
rádio	rádio	k1gNnSc4	rádio
pro	pro	k7c4	pro
dětské	dětský	k2eAgMnPc4d1	dětský
posluchače	posluchač	k1gMnPc4	posluchač
ČRo	ČRo	k1gFnSc2	ČRo
Radio	radio	k1gNnSc1	radio
Wave	Wav	k1gInSc2	Wav
–	–	k?	–
stanice	stanice	k1gFnSc2	stanice
pro	pro	k7c4	pro
mladé	mladý	k1gMnPc4	mladý
s	s	k7c7	s
alternativní	alternativní	k2eAgFnSc7d1	alternativní
hudbou	hudba	k1gFnSc7	hudba
ČRo	ČRo	k1gFnSc2	ČRo
Regina	Regina	k1gFnSc1	Regina
<g />
.	.	kIx.	.
</s>
<s>
DAB	DAB	kA	DAB
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
regionální	regionální	k2eAgFnPc4d1	regionální
stanice	stanice	k1gFnPc4	stanice
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
ČRo	ČRo	k1gMnSc1	ČRo
D-dur	Dur	k1gMnSc1	D-dur
–	–	k?	–
vysílá	vysílat	k5eAaImIp3nS	vysílat
nepřetržitě	přetržitě	k6eNd1	přetržitě
vážnou	vážný	k2eAgFnSc4d1	vážná
hudbu	hudba	k1gFnSc4	hudba
ČRo	ČRo	k1gFnSc2	ČRo
Jazz	jazz	k1gInSc1	jazz
–	–	k?	–
jazzové	jazzový	k2eAgNnSc4d1	jazzové
rádio	rádio	k1gNnSc4	rádio
s	s	k7c7	s
akcentem	akcent	k1gInSc7	akcent
na	na	k7c4	na
evropskou	evropský	k2eAgFnSc4d1	Evropská
tvorbu	tvorba	k1gFnSc4	tvorba
Rádio	rádio	k1gNnSc1	rádio
Junior	junior	k1gMnSc1	junior
–	–	k?	–
Písničky	písnička	k1gFnSc2	písnička
−	−	k?	−
doplňkový	doplňkový	k2eAgInSc4d1	doplňkový
stream	stream	k1gInSc4	stream
Rádia	rádius	k1gInSc2	rádius
Junior	junior	k1gMnSc1	junior
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
není	být	k5eNaImIp3nS	být
mluvené	mluvený	k2eAgNnSc1d1	mluvené
slovo	slovo	k1gNnSc1	slovo
Rádio	rádio	k1gNnSc1	rádio
Retro	retro	k1gNnSc2	retro
−	−	k?	−
příležitostný	příležitostný	k2eAgInSc4d1	příležitostný
stream	stream	k1gInSc4	stream
k	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
událostem	událost	k1gFnPc3	událost
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
90	[number]	k4	90
let	léto	k1gNnPc2	léto
pravidelného	pravidelný	k2eAgNnSc2d1	pravidelné
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
,	,	kIx,	,
45	[number]	k4	45
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
okupace	okupace	k1gFnSc2	okupace
Československa	Československo	k1gNnSc2	Československo
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
ČRo	ČRo	k1gMnSc1	ČRo
–	–	k?	–
Radio	radio	k1gNnSc4	radio
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
vysílání	vysílání	k1gNnSc1	vysílání
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
ruštině	ruština	k1gFnSc6	ruština
<g/>
,	,	kIx,	,
francouzštině	francouzština	k1gFnSc6	francouzština
a	a	k8xC	a
španělštině	španělština	k1gFnSc6	španělština
ČRo	ČRo	k1gFnSc1	ČRo
6	[number]	k4	6
–	–	k?	–
nástupce	nástupce	k1gMnSc4	nástupce
české	český	k2eAgFnSc2d1	Česká
redakce	redakce	k1gFnSc2	redakce
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
stanice	stanice	k1gFnSc2	stanice
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
vysílala	vysílat	k5eAaImAgFnS	vysílat
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
ČRo	ČRo	k1gFnPc2	ČRo
Rádio	rádio	k1gNnSc4	rádio
Česko	Česko	k1gNnSc1	Česko
–	–	k?	–
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
a	a	k8xC	a
publicistická	publicistický	k2eAgFnSc1d1	publicistická
stanice	stanice	k1gFnSc1	stanice
(	(	kIx(	(
<g/>
vysílá	vysílat	k5eAaImIp3nS	vysílat
také	také	k9	také
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
na	na	k7c6	na
FM	FM	kA	FM
kmitočtech	kmitočet	k1gInPc6	kmitočet
BBC	BBC	kA	BBC
World	World	k1gInSc1	World
Service	Service	k1gFnSc1	Service
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vysílala	vysílat	k5eAaImAgFnS	vysílat
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
ČRo	ČRo	k1gMnSc1	ČRo
Leonardo	Leonardo	k1gMnSc1	Leonardo
–	–	k?	–
populárně	populárně	k6eAd1	populárně
<g/>
–	–	k?	–
<g/>
naučná	naučný	k2eAgFnSc1d1	naučná
stanice	stanice	k1gFnSc1	stanice
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
technologie	technologie	k1gFnPc4	technologie
<g/>
,	,	kIx,	,
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
vysílala	vysílat	k5eAaImAgFnS	vysílat
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
ČRo	ČRo	k1gFnPc2	ČRo
Sport	sport	k1gInSc1	sport
–	–	k?	–
přímé	přímý	k2eAgFnPc4d1	přímá
reportáže	reportáž	k1gFnPc4	reportáž
ze	z	k7c2	z
sportovních	sportovní	k2eAgFnPc2d1	sportovní
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
vysílala	vysílat	k5eAaImAgFnS	vysílat
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
Archiv	archiv	k1gInSc1	archiv
ČRo	ČRo	k1gFnSc2	ČRo
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
zvukových	zvukový	k2eAgInPc2d1	zvukový
archivů	archiv	k1gInPc2	archiv
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
nejrozsáhlejším	rozsáhlý	k2eAgInSc7d3	nejrozsáhlejší
útvarem	útvar	k1gInSc7	útvar
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
depozitářích	depozitář	k1gInPc6	depozitář
tři	tři	k4xCgFnPc4	tři
patra	patro	k1gNnSc2	patro
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
je	být	k5eAaImIp3nS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
na	na	k7c4	na
80	[number]	k4	80
tisíc	tisíc	k4xCgInPc2	tisíc
hodin	hodina	k1gFnPc2	hodina
zvukových	zvukový	k2eAgInPc2d1	zvukový
záznamů	záznam	k1gInPc2	záznam
a	a	k8xC	a
25	[number]	k4	25
milionů	milion	k4xCgInPc2	milion
stran	strana	k1gFnPc2	strana
písemných	písemný	k2eAgInPc2d1	písemný
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Archiv	archiv	k1gInSc1	archiv
podporuje	podporovat	k5eAaImIp3nS	podporovat
rozhlasový	rozhlasový	k2eAgInSc4d1	rozhlasový
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
kulturní	kulturní	k2eAgNnSc4d1	kulturní
dědictví	dědictví	k1gNnSc4	dědictví
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
a	a	k8xC	a
zpřístupňuje	zpřístupňovat	k5eAaImIp3nS	zpřístupňovat
ho	on	k3xPp3gInSc4	on
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Bohoslužby	bohoslužba	k1gFnPc1	bohoslužba
(	(	kIx(	(
<g/>
Vltava	Vltava	k1gFnSc1	Vltava
–	–	k?	–
neděle	neděle	k1gFnSc2	neděle
<g/>
)	)	kIx)	)
Duchovní	duchovní	k2eAgFnSc1d1	duchovní
hudba	hudba	k1gFnSc1	hudba
(	(	kIx(	(
<g/>
Vltava	Vltava	k1gFnSc1	Vltava
–	–	k?	–
neděle	neděle	k1gFnSc2	neděle
<g/>
)	)	kIx)	)
Ranní	ranní	k2eAgNnSc1d1	ranní
slovo	slovo	k1gNnSc1	slovo
(	(	kIx(	(
<g/>
Vltava	Vltava	k1gFnSc1	Vltava
–	–	k?	–
neděle	neděle	k1gFnSc2	neděle
<g/>
)	)	kIx)	)
Ranní	ranní	k2eAgFnSc1d1	ranní
úvaha	úvaha	k1gFnSc1	úvaha
(	(	kIx(	(
<g/>
Vltava	Vltava	k1gFnSc1	Vltava
–	–	k?	–
neděle	neděle	k1gFnSc2	neděle
<g/>
)	)	kIx)	)
Liturgický	liturgický	k2eAgInSc1d1	liturgický
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
Vltava	Vltava	k1gFnSc1	Vltava
–	–	k?	–
neděle	neděle	k1gFnSc2	neděle
<g/>
)	)	kIx)	)
Vertikála	vertikála	k1gFnSc1	vertikála
(	(	kIx(	(
<g/>
Plus	plus	k1gInSc1	plus
–	–	k?	–
neděle	neděle	k1gFnSc2	neděle
<g/>
)	)	kIx)	)
Jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
vidí	vidět	k5eAaImIp3nS	vidět
(	(	kIx(	(
<g/>
Dvojka	dvojka	k1gFnSc1	dvojka
–	–	k?	–
neděle	neděle	k1gFnSc2	neděle
<g/>
)	)	kIx)	)
Mezi	mezi	k7c7	mezi
nebem	nebe	k1gNnSc7	nebe
a	a	k8xC	a
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc4	všechen
regionální	regionální	k2eAgFnPc4d1	regionální
stanice	stanice	k1gFnPc4	stanice
–	–	k?	–
sobota	sobota	k1gFnSc1	sobota
<g/>
)	)	kIx)	)
Hergot	hergot	k0	hergot
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Radio	radio	k1gNnSc4	radio
Wave	Wav	k1gFnSc2	Wav
–	–	k?	–
neděle	neděle	k1gFnSc2	neděle
<g/>
)	)	kIx)	)
Šalom	šalom	k0	šalom
(	(	kIx(	(
<g/>
Regina	Regina	k1gFnSc1	Regina
–	–	k?	–
neděle	neděle	k1gFnSc2	neděle
<g/>
,	,	kIx,	,
sudý	sudý	k2eAgInSc4d1	sudý
týden	týden	k1gInSc4	týden
<g/>
)	)	kIx)	)
Zelená	zelený	k2eAgFnSc1d1	zelená
vlna	vlna	k1gFnSc1	vlna
Všechny	všechen	k3xTgFnPc4	všechen
stanice	stanice	k1gFnPc4	stanice
ČRo	ČRo	k1gFnPc2	ČRo
vysílají	vysílat	k5eAaImIp3nP	vysílat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
internetového	internetový	k2eAgInSc2d1	internetový
streamu	stream	k1gInSc2	stream
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
digitálně	digitálně	k6eAd1	digitálně
přes	přes	k7c4	přes
DVB	DVB	kA	DVB
a	a	k8xC	a
DAB	DAB	kA	DAB
<g/>
/	/	kIx~	/
<g/>
DAB	DAB	kA	DAB
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
analogově	analogově	k6eAd1	analogově
přes	přes	k7c4	přes
pozemní	pozemní	k2eAgInPc4d1	pozemní
vysílače	vysílač	k1gInPc4	vysílač
<g/>
:	:	kIx,	:
Aktualizace	aktualizace	k1gFnSc1	aktualizace
<g/>
:	:	kIx,	:
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
Daruj	darovat	k5eAaPmRp2nS	darovat
krev	krev	k1gFnSc1	krev
-	-	kIx~	-
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
u	u	k7c2	u
posluchačů	posluchač	k1gMnPc2	posluchač
povědomí	povědomí	k1gNnSc2	povědomí
o	o	k7c4	o
dárcovství	dárcovství	k1gNnSc4	dárcovství
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
oslovuje	oslovovat	k5eAaImIp3nS	oslovovat
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dosud	dosud	k6eAd1	dosud
krev	krev	k1gFnSc4	krev
nedarovali	darovat	k5eNaPmAgMnP	darovat
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
prvodárce	prvodárka	k1gFnSc6	prvodárka
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
vody	voda	k1gFnSc2	voda
-	-	kIx~	-
programový	programový	k2eAgInSc4d1	programový
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
upozorňoval	upozorňovat	k5eAaImAgMnS	upozorňovat
na	na	k7c4	na
rostoucí	rostoucí	k2eAgInPc4d1	rostoucí
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
i	i	k8xC	i
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zapojily	zapojit	k5eAaPmAgInP	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
všechny	všechen	k3xTgFnPc4	všechen
stanice	stanice	k1gFnPc4	stanice
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
ČRo	ČRo	k?	ČRo
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
institucí	instituce	k1gFnPc2	instituce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
otevřela	otevřít	k5eAaPmAgNnP	otevřít
internetová	internetový	k2eAgNnPc1d1	internetové
vysílání	vysílání	k1gNnPc1	vysílání
i	i	k8xC	i
internetový	internetový	k2eAgInSc1d1	internetový
archiv	archiv	k1gInSc1	archiv
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Rádio	rádio	k1gNnSc1	rádio
na	na	k7c6	na
přání	přání	k1gNnSc6	přání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2004	[number]	k4	2004
zavedl	zavést	k5eAaPmAgInS	zavést
rozhlas	rozhlas	k1gInSc1	rozhlas
také	také	k9	také
podcast	podcast	k1gInSc1	podcast
pro	pro	k7c4	pro
pořady	pořad	k1gInPc4	pořad
ČRo	ČRo	k1gFnSc2	ČRo
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
podobnou	podobný	k2eAgFnSc4d1	podobná
službu	služba	k1gFnSc4	služba
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
stanice	stanice	k1gFnPc4	stanice
spustila	spustit	k5eAaPmAgFnS	spustit
BBC	BBC	kA	BBC
<g/>
.	.	kIx.	.
</s>
<s>
Webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
jsou	být	k5eAaImIp3nP	být
rozcestníkem	rozcestník	k1gInSc7	rozcestník
k	k	k7c3	k
webům	web	k1gInPc3	web
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
k	k	k7c3	k
živému	živý	k2eAgNnSc3d1	živé
vysílání	vysílání	k1gNnSc3	vysílání
i	i	k8xC	i
záznamům	záznam	k1gInPc3	záznam
pořadů	pořad	k1gInPc2	pořad
a	a	k8xC	a
informacím	informace	k1gFnPc3	informace
o	o	k7c6	o
činnosti	činnost	k1gFnSc6	činnost
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Zpravodajský	zpravodajský	k2eAgInSc1d1	zpravodajský
server	server	k1gInSc1	server
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
a	a	k8xC	a
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
odvysílané	odvysílaný	k2eAgFnPc4d1	odvysílaná
zprávy	zpráva	k1gFnPc4	zpráva
i	i	k8xC	i
vlastní	vlastní	k2eAgFnSc4d1	vlastní
agendu	agenda	k1gFnSc4	agenda
redaktorů	redaktor	k1gMnPc2	redaktor
webu	web	k1gInSc2	web
<g/>
.	.	kIx.	.
</s>
<s>
Služba	služba	k1gFnSc1	služba
v	v	k7c6	v
sobě	se	k3xPyFc3	se
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
živé	živý	k2eAgNnSc4d1	živé
vysílání	vysílání	k1gNnSc4	vysílání
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
audioarchiv	audioarchiva	k1gFnPc2	audioarchiva
a	a	k8xC	a
podcast	podcast	k1gFnSc4	podcast
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
audioarchivu	audioarchiv	k1gInSc2	audioarchiv
denně	denně	k6eAd1	denně
přibývají	přibývat	k5eAaImIp3nP	přibývat
stovky	stovka	k1gFnPc1	stovka
audií	audium	k1gNnPc2	audium
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
přes	přes	k7c4	přes
440	[number]	k4	440
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
lze	lze	k6eAd1	lze
vyhledat	vyhledat	k5eAaPmF	vyhledat
zvukový	zvukový	k2eAgInSc4d1	zvukový
záznam	záznam	k1gInSc4	záznam
podle	podle	k7c2	podle
řady	řada	k1gFnSc2	řada
různých	různý	k2eAgInPc2d1	různý
atributů	atribut	k1gInPc2	atribut
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
pořad	pořad	k1gInSc1	pořad
<g/>
,	,	kIx,	,
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
fulltext	fulltext	k1gInSc1	fulltext
atd.	atd.	kA	atd.
Vybraná	vybraná	k1gFnSc1	vybraná
literární	literární	k2eAgFnSc1d1	literární
a	a	k8xC	a
dramatická	dramatický	k2eAgNnPc1d1	dramatické
díla	dílo	k1gNnPc1	dílo
mohou	moct	k5eAaImIp3nP	moct
posluchači	posluchač	k1gMnSc3	posluchač
poslouchat	poslouchat	k5eAaImF	poslouchat
ve	v	k7c6	v
streamu	stream	k1gInSc6	stream
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
odvysílání	odvysílání	k1gNnSc6	odvysílání
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
časové	časový	k2eAgNnSc1d1	časové
omezení	omezení	k1gNnSc1	omezení
a	a	k8xC	a
technické	technický	k2eAgNnSc1d1	technické
omezení	omezení	k1gNnSc1	omezení
(	(	kIx(	(
<g/>
nelze	lze	k6eNd1	lze
stahovat	stahovat	k5eAaImF	stahovat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnPc4d3	nejznámější
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
romány	román	k1gInPc4	román
i	i	k8xC	i
povídky	povídka	k1gFnPc4	povídka
nabízí	nabízet	k5eAaImIp3nS	nabízet
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
zdarma	zdarma	k6eAd1	zdarma
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
předních	přední	k2eAgMnPc2d1	přední
českých	český	k2eAgMnPc2d1	český
herců	herec	k1gMnPc2	herec
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgFnSc1d1	kompletní
webová	webový	k2eAgFnSc1d1	webová
nabídka	nabídka	k1gFnSc1	nabídka
odvysílaných	odvysílaný	k2eAgFnPc2d1	odvysílaná
minutových	minutový	k2eAgFnPc2d1	minutová
her	hra	k1gFnPc2	hra
ve	v	k7c6	v
zvukové	zvukový	k2eAgFnSc6d1	zvuková
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Interaktivní	interaktivní	k2eAgFnSc1d1	interaktivní
časová	časový	k2eAgFnSc1d1	časová
osa	osa	k1gFnSc1	osa
s	s	k7c7	s
nejdůležitějšími	důležitý	k2eAgMnPc7d3	nejdůležitější
události	událost	k1gFnPc4	událost
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
vysílání	vysílání	k1gNnSc2	vysílání
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
aplikace	aplikace	k1gFnPc4	aplikace
pro	pro	k7c4	pro
systémy	systém	k1gInPc4	systém
iOS	iOS	k?	iOS
a	a	k8xC	a
Android	android	k1gInSc1	android
–	–	k?	–
iRadio	iRadio	k1gNnSc1	iRadio
<g/>
,	,	kIx,	,
Zprávy	zpráva	k1gFnPc1	zpráva
ČRo	ČRo	k1gFnPc1	ČRo
<g/>
,	,	kIx,	,
Radio	radio	k1gNnSc1	radio
Wave	Wave	k1gFnPc2	Wave
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
spustil	spustit	k5eAaPmAgInS	spustit
novou	nový	k2eAgFnSc4d1	nová
aplikaci	aplikace	k1gFnSc4	aplikace
s	s	k7c7	s
názvem	název	k1gInSc7	název
Radiotéka	Radiotéek	k1gInSc2	Radiotéek
<g/>
.	.	kIx.	.
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
audioknihy	audioknih	k1gInPc4	audioknih
a	a	k8xC	a
rozhlasové	rozhlasový	k2eAgFnPc4d1	rozhlasová
hry	hra	k1gFnPc4	hra
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
–	–	k?	–
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
především	především	k9	především
díky	díky	k7c3	díky
projektu	projekt	k1gInSc3	projekt
Světluška	světluška	k1gFnSc1	světluška
Radioservis	Radioservis	k1gFnSc2	Radioservis
–	–	k?	–
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Pronájem	pronájem	k1gInSc4	pronájem
studií	studio	k1gNnPc2	studio
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
</s>
