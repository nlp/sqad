<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
provozuje	provozovat	k5eAaImIp3nS	provozovat
čtyři	čtyři	k4xCgFnPc4	čtyři
celoplošné	celoplošný	k2eAgFnPc4d1	celoplošná
stanice	stanice	k1gFnPc4	stanice
(	(	kIx(	(
<g/>
Radiožurnál	radiožurnál	k1gInSc1	radiožurnál
<g/>
,	,	kIx,	,
Dvojka	dvojka	k1gFnSc1	dvojka
<g/>
,	,	kIx,	,
Vltava	Vltava	k1gFnSc1	Vltava
a	a	k8xC	a
Plus	plus	k1gInSc1	plus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
krajích	kraj	k1gInPc6	kraj
také	také	k9	také
regionální	regionální	k2eAgNnSc4d1	regionální
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
,	,	kIx,	,
digitální	digitální	k2eAgFnSc1d1	digitální
stanice	stanice	k1gFnSc1	stanice
(	(	kIx(	(
<g/>
Rádio	rádio	k1gNnSc1	rádio
Junior	junior	k1gMnSc1	junior
<g/>
,	,	kIx,	,
Radio	radio	k1gNnSc1	radio
Wave	Wave	k1gFnPc2	Wave
<g/>
,	,	kIx,	,
D-dur	Dura	k1gFnPc2	D-dura
<g/>
,	,	kIx,	,
Jazz	jazz	k1gInSc1	jazz
<g/>
)	)	kIx)	)
a	a	k8xC	a
internetové	internetový	k2eAgInPc1d1	internetový
streamy	stream	k1gInPc1	stream
(	(	kIx(	(
<g/>
Rádio	rádio	k1gNnSc1	rádio
Junior	junior	k1gMnSc1	junior
–	–	k?	–
písničky	písnička	k1gFnSc2	písnička
a	a	k8xC	a
příležitostně	příležitostně	k6eAd1	příležitostně
Rádio	rádio	k1gNnSc1	rádio
Retro	retro	k1gNnSc2	retro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
