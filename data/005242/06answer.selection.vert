<s>
Pařížský	pařížský	k2eAgInSc1d1	pařížský
syndrom	syndrom	k1gInSc1	syndrom
je	být	k5eAaImIp3nS	být
přechodná	přechodný	k2eAgFnSc1d1	přechodná
psychická	psychický	k2eAgFnSc1d1	psychická
porucha	porucha	k1gFnSc1	porucha
<g/>
,	,	kIx,	,
postihující	postihující	k2eAgNnSc1d1	postihující
některé	některý	k3yIgFnPc4	některý
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
navštíví	navštívit	k5eAaPmIp3nP	navštívit
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
,	,	kIx,	,
obecněji	obecně	k6eAd2	obecně
Francii	Francie	k1gFnSc4	Francie
nebo	nebo	k8xC	nebo
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
.	.	kIx.	.
</s>
