<s>
Pařížský	pařížský	k2eAgInSc1d1	pařížský
syndrom	syndrom	k1gInSc1	syndrom
je	být	k5eAaImIp3nS	být
přechodná	přechodný	k2eAgFnSc1d1	přechodná
psychická	psychický	k2eAgFnSc1d1	psychická
porucha	porucha	k1gFnSc1	porucha
<g/>
,	,	kIx,	,
postihující	postihující	k2eAgNnSc1d1	postihující
některé	některý	k3yIgFnPc4	některý
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
navštíví	navštívit	k5eAaPmIp3nP	navštívit
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
,	,	kIx,	,
obecněji	obecně	k6eAd2	obecně
Francii	Francie	k1gFnSc4	Francie
nebo	nebo	k8xC	nebo
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
podstatě	podstata	k1gFnSc6	podstata
obdobný	obdobný	k2eAgInSc4d1	obdobný
Jeruzalémskému	jeruzalémský	k2eAgMnSc3d1	jeruzalémský
a	a	k8xC	a
Stendhalovu	Stendhalův	k2eAgInSc3d1	Stendhalův
syndromu	syndrom	k1gInSc3	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
náchylní	náchylný	k2eAgMnPc1d1	náchylný
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
syndromu	syndrom	k1gInSc3	syndrom
návštěvníci	návštěvník	k1gMnPc1	návštěvník
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
publikována	publikovat	k5eAaBmNgFnS	publikovat
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
psychiatrickém	psychiatrický	k2eAgInSc6d1	psychiatrický
časopise	časopis	k1gInSc6	časopis
Nervure	Nervur	k1gMnSc5	Nervur
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
odhadovaných	odhadovaný	k2eAgInPc2d1	odhadovaný
8,8	[number]	k4	8,8
miliónů	milión	k4xCgInPc2	milión
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
za	za	k7c4	za
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc4d1	významný
počet	počet	k1gInSc4	počet
hlášených	hlášený	k2eAgInPc2d1	hlášený
případů	případ	k1gInPc2	případ
na	na	k7c6	na
japonském	japonský	k2eAgNnSc6d1	Japonské
velvyslanectví	velvyslanectví	k1gNnSc6	velvyslanectví
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgInSc7	tento
syndromem	syndrom	k1gInSc7	syndrom
ročně	ročně	k6eAd1	ročně
postiženo	postižen	k2eAgNnSc1d1	postiženo
asi	asi	k9	asi
dvacet	dvacet	k4xCc1	dvacet
japonských	japonský	k2eAgMnPc2d1	japonský
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Citlivost	citlivost	k1gFnSc1	citlivost
Japonců	Japonec	k1gMnPc2	Japonec
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
popularitou	popularita	k1gFnSc7	popularita
Paříže	Paříž	k1gFnSc2	Paříž
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
převládající	převládající	k2eAgInSc1d1	převládající
idealizovaný	idealizovaný	k2eAgInSc1d1	idealizovaný
obraz	obraz	k1gInSc1	obraz
Paříže	Paříž	k1gFnSc2	Paříž
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
realitě	realita	k1gFnSc3	realita
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
určité	určitý	k2eAgNnSc4d1	určité
zklamání	zklamání	k1gNnSc4	zklamání
a	a	k8xC	a
frustraci	frustrace	k1gFnSc4	frustrace
zažívají	zažívat	k5eAaImIp3nP	zažívat
během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
i	i	k9	i
turisté	turist	k1gMnPc1	turist
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pařížský	pařížský	k2eAgInSc1d1	pařížský
syndrom	syndrom	k1gInSc1	syndrom
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
řadou	řada	k1gFnSc7	řada
psychiatrických	psychiatrický	k2eAgInPc2d1	psychiatrický
symptomů	symptom	k1gInPc2	symptom
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
bludy	blud	k1gInPc1	blud
<g/>
,	,	kIx,	,
halucinace	halucinace	k1gFnPc1	halucinace
<g/>
,	,	kIx,	,
pocity	pocit	k1gInPc1	pocit
pronásledování	pronásledování	k1gNnSc2	pronásledování
(	(	kIx(	(
<g/>
dojem	dojem	k1gInSc1	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
je	být	k5eAaImIp3nS	být
obětí	oběť	k1gFnSc7	oběť
předsudků	předsudek	k1gInPc2	předsudek
<g/>
,	,	kIx,	,
agrese	agrese	k1gFnSc1	agrese
nebo	nebo	k8xC	nebo
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
derealizace	derealizace	k1gFnSc1	derealizace
<g/>
,	,	kIx,	,
depersonalizace	depersonalizace	k1gFnSc1	depersonalizace
<g/>
,	,	kIx,	,
úzkost	úzkost	k1gFnSc1	úzkost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
psychosomatické	psychosomatický	k2eAgInPc1d1	psychosomatický
projevy	projev	k1gInPc1	projev
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
závrať	závrať	k1gFnSc1	závrať
<g/>
,	,	kIx,	,
tachykardie	tachykardie	k1gFnSc1	tachykardie
<g/>
,	,	kIx,	,
pocení	pocení	k1gNnSc1	pocení
atd.	atd.	kA	atd.
Klinický	klinický	k2eAgInSc1d1	klinický
obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
velmi	velmi	k6eAd1	velmi
variabilní	variabilní	k2eAgMnSc1d1	variabilní
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
společnou	společný	k2eAgFnSc7d1	společná
charakteristikou	charakteristika	k1gFnSc7	charakteristika
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
během	během	k7c2	během
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
cestující	cestující	k1gMnSc1	cestující
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
věcmi	věc	k1gFnPc7	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
dříve	dříve	k6eAd2	dříve
nezažil	zažít	k5eNaPmAgMnS	zažít
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
neočekává	očekávat	k5eNaImIp3nS	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
neexistovaly	existovat	k5eNaImAgInP	existovat
před	před	k7c7	před
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
zmizí	zmizet	k5eAaPmIp3nS	zmizet
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
známého	známý	k2eAgNnSc2d1	známé
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgInPc7d1	základní
faktory	faktor	k1gInPc7	faktor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
spouštějí	spouštět	k5eAaImIp3nP	spouštět
syndrom	syndrom	k1gInSc4	syndrom
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
autorů	autor	k1gMnPc2	autor
článku	článek	k1gInSc2	článek
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
bariéra	bariéra	k1gFnSc1	bariéra
–	–	k?	–
jen	jen	k9	jen
málo	málo	k4c4	málo
Japonců	Japonec	k1gMnPc2	Japonec
hovoří	hovořit	k5eAaImIp3nS	hovořit
francouzsky	francouzsky	k6eAd1	francouzsky
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Neznalost	neznalost	k1gFnSc1	neznalost
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zjevných	zjevný	k2eAgInPc2d1	zjevný
rozdílů	rozdíl	k1gInPc2	rozdíl
mezi	mezi	k7c7	mezi
francouzštinou	francouzština	k1gFnSc7	francouzština
a	a	k8xC	a
japonštinou	japonština	k1gFnSc7	japonština
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c4	mnoho
každodenních	každodenní	k2eAgFnPc2d1	každodenní
frází	fráze	k1gFnPc2	fráze
a	a	k8xC	a
idiomů	idiom	k1gInPc2	idiom
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
<g/>
,	,	kIx,	,
zbaveny	zbavit	k5eAaPmNgFnP	zbavit
svého	svůj	k3xOyFgInSc2	svůj
významu	význam	k1gInSc2	význam
a	a	k8xC	a
obsahu	obsah	k1gInSc2	obsah
<g/>
,	,	kIx,	,
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
zmatek	zmatek	k1gInSc4	zmatek
při	při	k7c6	při
přímém	přímý	k2eAgInSc6d1	přímý
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
Francouzi	Francouz	k1gMnPc7	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
kulturní	kulturní	k2eAgFnSc6d1	kulturní
identitě	identita	k1gFnSc6	identita
–	–	k?	–
velké	velký	k2eAgFnPc1d1	velká
odlišnosti	odlišnost	k1gFnPc1	odlišnost
jsou	být	k5eAaImIp3nP	být
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ve	v	k7c6	v
zvycích	zvyk	k1gInPc6	zvyk
a	a	k8xC	a
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
více	hodně	k6eAd2	hodně
na	na	k7c6	na
neformální	formální	k2eNgFnSc6d1	neformální
úrovni	úroveň	k1gFnSc6	úroveň
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
přísně	přísně	k6eAd1	přísně
formální	formální	k2eAgFnSc7d1	formální
japonskou	japonský	k2eAgFnSc7d1	japonská
kulturou	kultura	k1gFnSc7	kultura
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
některé	některý	k3yIgMnPc4	některý
japonské	japonský	k2eAgMnPc4d1	japonský
návštěvníky	návštěvník	k1gMnPc4	návštěvník
velký	velký	k2eAgInSc4d1	velký
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Idealizovaný	idealizovaný	k2eAgInSc1d1	idealizovaný
obraz	obraz	k1gInSc1	obraz
Paříže	Paříž	k1gFnSc2	Paříž
–	–	k?	–
někteří	některý	k3yIgMnPc1	některý
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
uvést	uvést	k5eAaPmF	uvést
do	do	k7c2	do
souladu	soulad	k1gInSc2	soulad
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
populárním	populární	k2eAgInSc7d1	populární
virtuálním	virtuální	k2eAgInSc7d1	virtuální
obrazem	obraz	k1gInSc7	obraz
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
syrovou	syrový	k2eAgFnSc7d1	syrová
realitou	realita	k1gFnSc7	realita
<g/>
.	.	kIx.	.
</s>
<s>
Fyzické	fyzický	k2eAgNnSc1d1	fyzické
vyčerpání	vyčerpání	k1gNnSc1	vyčerpání
–	–	k?	–
vynaložený	vynaložený	k2eAgInSc1d1	vynaložený
čas	čas	k1gInSc1	čas
a	a	k8xC	a
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
na	na	k7c6	na
služební	služební	k2eAgFnSc6d1	služební
cestě	cesta	k1gFnSc6	cesta
nebo	nebo	k8xC	nebo
na	na	k7c6	na
dovolené	dovolená	k1gFnSc6	dovolená
a	a	k8xC	a
snaha	snaha	k1gFnSc1	snaha
stihnout	stihnout	k5eAaPmF	stihnout
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
zážitků	zážitek	k1gInPc2	zážitek
během	během	k7c2	během
krátkého	krátký	k2eAgInSc2d1	krátký
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
účinky	účinek	k1gInPc7	účinek
pásmové	pásmový	k2eAgFnSc2d1	pásmová
nemoci	nemoc	k1gFnSc2	nemoc
přispívá	přispívat	k5eAaImIp3nS	přispívat
rovněž	rovněž	k9	rovněž
k	k	k7c3	k
psychické	psychický	k2eAgFnSc3d1	psychická
destabilizaci	destabilizace	k1gFnSc3	destabilizace
některých	některý	k3yIgMnPc2	některý
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Hiroaki	Hiroaki	k1gNnSc2	Hiroaki
Ota	Ota	k1gMnSc1	Ota
<g/>
,	,	kIx,	,
japonský	japonský	k2eAgMnSc1d1	japonský
psychiatr	psychiatr	k1gMnSc1	psychiatr
působící	působící	k2eAgMnSc1d1	působící
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
prvního	první	k4xOgMnSc4	první
vědce	vědec	k1gMnSc4	vědec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
nemoc	nemoc	k1gFnSc4	nemoc
diagnostikoval	diagnostikovat	k5eAaBmAgInS	diagnostikovat
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnSc2d2	pozdější
práce	práce	k1gFnSc2	práce
Youcefa	Youcef	k1gMnSc2	Youcef
Mahmoudia	Mahmoudium	k1gNnSc2	Mahmoudium
<g/>
,	,	kIx,	,
lékaře	lékař	k1gMnSc4	lékař
z	z	k7c2	z
nemocnice	nemocnice	k1gFnSc2	nemocnice
Hôtel-Dieu	Hôtel-Dieus	k1gInSc2	Hôtel-Dieus
<g/>
,	,	kIx,	,
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pařížský	pařížský	k2eAgInSc1d1	pařížský
syndrom	syndrom	k1gInSc1	syndrom
je	být	k5eAaImIp3nS	být
psychopatologický	psychopatologický	k2eAgInSc1d1	psychopatologický
jev	jev	k1gInSc1	jev
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
cesty	cesta	k1gFnPc1	cesta
<g/>
,	,	kIx,	,
než	než	k8xS	než
cestujícího	cestující	k1gMnSc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Domnívá	domnívat	k5eAaImIp3nS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzrušení	vzrušení	k1gNnSc1	vzrušení
vyplývající	vyplývající	k2eAgNnSc1d1	vyplývající
z	z	k7c2	z
návštěvy	návštěva	k1gFnSc2	návštěva
Paříže	Paříž	k1gFnSc2	Paříž
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zrychlení	zrychlení	k1gNnSc1	zrychlení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
závratě	závrať	k1gFnPc4	závrať
a	a	k8xC	a
dušnost	dušnost	k1gFnSc4	dušnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
halucinace	halucinace	k1gFnSc2	halucinace
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xS	jako
u	u	k7c2	u
Stendhalova	Stendhalův	k2eAgInSc2d1	Stendhalův
syndromu	syndrom	k1gInSc2	syndrom
<g/>
.	.	kIx.	.
</s>
