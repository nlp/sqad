<s>
DLL	DLL	kA
</s>
<s>
DLL	DLL	kA
(	(	kIx(
<g/>
angl.	angl.	k?
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
ˌ	ˌ	k1gInSc1
<g/>
]	]	kIx)
<g/>
;	;	kIx,
Dynamic-link	Dynamic-link	k1gInSc1
library	librara	k1gFnSc2
<g/>
,	,	kIx,
dynamicky	dynamicky	k6eAd1
linkovaná	linkovaný	k2eAgFnSc1d1
[	[	kIx(
<g/>
připojovaná	připojovaný	k2eAgFnSc1d1
<g/>
]	]	kIx)
knihovna	knihovna	k1gFnSc1
<g/>
,	,	kIx,
knihovní	knihovní	k2eAgInSc1d1
modul	modul	k1gInSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
informatice	informatika	k1gFnSc6
implementace	implementace	k1gFnSc2
konceptu	koncept	k1gInSc2
sdílených	sdílený	k2eAgFnPc2d1
knihoven	knihovna	k1gFnPc2
společnosti	společnost	k1gFnSc2
Microsoft	Microsoft	kA
pro	pro	k7c4
operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
Microsoft	Microsoft	kA
Windows	Windows	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
též	též	k9
používán	používat	k5eAaImNgInS
v	v	k7c6
OS	OS	kA
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
jeden	jeden	k4xCgInSc4
ze	z	k7c2
základních	základní	k2eAgInPc2d1
prvků	prvek	k1gInPc2
struktury	struktura	k1gFnSc2
Windows	Windows	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Soubory	soubor	k1gInPc1
s	s	k7c7
knihovnami	knihovna	k1gFnPc7
obvykle	obvykle	k6eAd1
používají	používat	k5eAaImIp3nP
příponu	přípona	k1gFnSc4
.	.	kIx.
<g/>
dll	dll	k?
<g/>
,	,	kIx,
.	.	kIx.
<g/>
ocx	ocx	k?
(	(	kIx(
<g/>
pro	pro	k7c4
ActiveX	ActiveX	k1gFnSc4
prvky	prvek	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
.	.	kIx.
<g/>
drv	drvit	k5eAaImRp2nS
(	(	kIx(
<g/>
pro	pro	k7c4
staré	starý	k2eAgInPc4d1
systémové	systémový	k2eAgInPc4d1
ovladače	ovladač	k1gInPc4
<g/>
)	)	kIx)
nebo	nebo	k8xC
.	.	kIx.
<g/>
fon	fon	k?
(	(	kIx(
<g/>
pro	pro	k7c4
rastrová	rastrový	k2eAgNnPc4d1
písma	písmo	k1gNnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
<g/>
-li	-li	k?
knihovna	knihovna	k1gFnSc1
jinou	jiný	k2eAgFnSc4d1
příponu	přípona	k1gFnSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
ji	on	k3xPp3gFnSc4
program	program	k1gInSc1
musí	muset	k5eAaImIp3nS
nahrát	nahrát	k5eAaPmF,k5eAaBmF
explicitně	explicitně	k6eAd1
voláním	volání	k1gNnSc7
funkce	funkce	k1gFnSc2
LoadLibrary	LoadLibrara	k1gFnSc2
či	či	k8xC
LoadLibraryEx	LoadLibraryEx	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Formát	formát	k1gInSc1
DLL	DLL	kA
souborů	soubor	k1gInPc2
je	být	k5eAaImIp3nS
stejný	stejný	k2eAgInSc1d1
jako	jako	k9
v	v	k7c6
případě	případ	k1gInSc6
EXE	EXE	kA
souborů	soubor	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
Portable	portable	k1gInSc1
Executable	Executable	k1gFnSc2
(	(	kIx(
<g/>
PE	PE	kA
<g/>
)	)	kIx)
pro	pro	k7c4
32	#num#	k4
<g/>
bitové	bitový	k2eAgInPc1d1
a	a	k8xC
64	#num#	k4
<g/>
bitové	bitový	k2eAgInPc1d1
Windows	Windows	kA
nebo	nebo	k8xC
New	New	k1gMnSc1
Executable	Executable	k1gMnSc1
(	(	kIx(
<g/>
NE	Ne	kA
<g/>
)	)	kIx)
pro	pro	k7c4
16	#num#	k4
<g/>
bitové	bitový	k2eAgFnSc2d1
Windows	Windows	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
EXE	EXE	kA
soubory	soubor	k1gInPc7
mohou	moct	k5eAaImIp3nP
DLL	DLL	kA
obsahovat	obsahovat	k5eAaImF
kód	kód	k1gInSc4
<g/>
,	,	kIx,
data	datum	k1gNnPc4
a	a	k8xC
zdroje	zdroj	k1gInPc4
v	v	k7c6
libovolné	libovolný	k2eAgFnSc6d1
kombinaci	kombinace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
DLL	DLL	kA
souborů	soubor	k1gInPc2
</s>
<s>
V	v	k7c6
prvních	první	k4xOgFnPc6
verzích	verze	k1gFnPc6
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
Microsoft	Microsoft	kA
Windows	Windows	kA
byly	být	k5eAaImAgInP
všechny	všechen	k3xTgInPc1
procesy	proces	k1gInPc1
spouštěny	spouštěn	k2eAgInPc1d1
v	v	k7c6
jednom	jeden	k4xCgInSc6
adresovém	adresový	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
paměti	paměť	k1gFnSc2
počítače	počítač	k1gInSc2
a	a	k8xC
díky	díky	k7c3
kooperativnímu	kooperativní	k2eAgInSc3d1
multitaskingu	multitasking	k1gInSc3
se	s	k7c7
procesy	proces	k1gInPc7
explicitně	explicitně	k6eAd1
vzdávaly	vzdávat	k5eAaImAgInP
procesoru	procesor	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
funkce	funkce	k1gFnPc4
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
poskytoval	poskytovat	k5eAaImAgInS
MS-DOS	MS-DOS	k1gFnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
všechny	všechen	k3xTgFnPc1
vysokoúrovňové	vysokoúrovňový	k2eAgFnPc1d1
služby	služba	k1gFnPc1
byly	být	k5eAaImAgFnP
poskytovány	poskytovat	k5eAaImNgFnP
pomocí	pomocí	k7c2
DLL	DLL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aplikační	aplikační	k2eAgInSc4d1
rozhraní	rozhraní	k1gNnSc1
(	(	kIx(
<g/>
API	API	kA
<g/>
)	)	kIx)
pro	pro	k7c4
vykreslování	vykreslování	k1gNnSc4
(	(	kIx(
<g/>
GDI	GDI	kA
<g/>
,	,	kIx,
Graphics	Graphics	k1gInSc1
Device	device	k1gInSc1
Interface	interface	k1gInSc1
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
implementováno	implementovat	k5eAaImNgNnS
v	v	k7c6
DLL	DLL	kA
zvané	zvaný	k2eAgFnPc4d1
gdi	gdi	k?
<g/>
.	.	kIx.
<g/>
exe	exe	k?
a	a	k8xC
uživatelské	uživatelský	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
v	v	k7c6
user	usrat	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
exe	exe	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
vrstvy	vrstva	k1gFnPc1
nad	nad	k7c4
DOSem	DOSem	k1gInSc4
byly	být	k5eAaImAgFnP
sdílené	sdílený	k2eAgFnPc1d1
pro	pro	k7c4
všechny	všechen	k3xTgInPc4
běžící	běžící	k2eAgInPc4d1
procesy	proces	k1gInPc4
(	(	kIx(
<g/>
pod	pod	k7c7
podmínkou	podmínka	k1gFnSc7
provozu	provoz	k1gInSc2
na	na	k7c6
strojích	stroj	k1gInPc6
s	s	k7c7
alespoň	alespoň	k9
jedním	jeden	k4xCgInSc7
megabajtem	megabajt	k1gInSc7
paměti	paměť	k1gFnSc2
<g/>
)	)	kIx)
s	s	k7c7
cílem	cíl	k1gInSc7
umožnit	umožnit	k5eAaPmF
procesům	proces	k1gInPc3
vzájemně	vzájemně	k6eAd1
spolupracovat	spolupracovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kód	kód	k1gInSc1
v	v	k7c6
GDI	GDI	kA
překládal	překládat	k5eAaImAgMnS
příkazy	příkaz	k1gInPc4
pro	pro	k7c4
vykreslování	vykreslování	k1gNnSc4
do	do	k7c2
specifických	specifický	k2eAgFnPc2d1
instrukcí	instrukce	k1gFnPc2
různých	různý	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
:	:	kIx,
v	v	k7c6
případě	případ	k1gInSc6
displeje	displej	k1gInSc2
byly	být	k5eAaImAgFnP
měněny	měnit	k5eAaImNgInP
body	bod	k1gInPc1
ve	v	k7c6
framebufferu	framebuffero	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
tiskárny	tiskárna	k1gFnSc2
byly	být	k5eAaImAgInP
příkazy	příkaz	k1gInPc1
transformovány	transformován	k2eAgInPc1d1
na	na	k7c4
pokyny	pokyn	k1gInPc4
pro	pro	k7c4
tiskárnu	tiskárna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nP
GDI	GDI	kA
mohlo	moct	k5eAaImAgNnS
pracovat	pracovat	k5eAaImF
s	s	k7c7
různými	různý	k2eAgNnPc7d1
zařízeními	zařízení	k1gNnPc7
<g/>
,	,	kIx,
načítaly	načítat	k5eAaPmAgInP,k5eAaBmAgInP
se	se	k3xPyFc4
do	do	k7c2
paměti	paměť	k1gFnSc2
programy	program	k1gInPc1
zvané	zvaný	k2eAgInPc1d1
ovladače	ovladač	k1gInPc1
zařízení	zařízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Stejná	stejný	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
GDI	GDI	kA
načítalo	načítat	k5eAaBmAgNnS,k5eAaPmAgNnS
ovladače	ovladač	k1gInPc4
pro	pro	k7c4
různá	různý	k2eAgNnPc4d1
zařízení	zařízení	k1gNnSc4
<g/>
,	,	kIx,
umožnila	umožnit	k5eAaPmAgFnS
systému	systém	k1gInSc2
načíst	načíst	k5eAaPmF,k5eAaBmF
programy	program	k1gInPc1
tak	tak	k9
<g/>
,	,	kIx,
že	že	k8xS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
z	z	k7c2
těchto	tento	k3xDgInPc2
programů	program	k1gInPc2
volat	volat	k5eAaImF
API	API	kA
funkce	funkce	k1gFnSc1
knihoven	knihovna	k1gFnPc2
USER	usrat	k5eAaPmRp2nS
a	a	k8xC
GDI	GDI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
koncept	koncept	k1gInSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
dynamické	dynamický	k2eAgNnSc1d1
linkování	linkování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
nesdílených	sdílený	k2eNgNnPc2d1
<g/>
,	,	kIx,
tedy	tedy	k9
statických	statický	k2eAgFnPc6d1
<g/>
,	,	kIx,
knihoven	knihovna	k1gFnPc2
se	se	k3xPyFc4
jejich	jejich	k3xOp3gInSc4
kód	kód	k1gInSc4
jednoduše	jednoduše	k6eAd1
ve	v	k7c6
fázi	fáze	k1gFnSc6
linkování	linkování	k1gNnSc2
přidává	přidávat	k5eAaImIp3nS
do	do	k7c2
všech	všecek	k3xTgInPc2
jednotlivých	jednotlivý	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
příslušný	příslušný	k2eAgInSc4d1
kód	kód	k1gInSc4
potřebují	potřebovat	k5eAaImIp3nP
a	a	k8xC
volají	volat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
dynamického	dynamický	k2eAgNnSc2d1
linkování	linkování	k1gNnSc2
je	být	k5eAaImIp3nS
sdílený	sdílený	k2eAgInSc1d1
kód	kód	k1gInSc1
umístěn	umístit	k5eAaPmNgInS
do	do	k7c2
separátního	separátní	k2eAgInSc2d1
souboru	soubor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Programy	program	k1gInPc1
jsou	být	k5eAaImIp3nP
s	s	k7c7
těmito	tento	k3xDgFnPc7
soubory	soubor	k1gInPc1
spojeny	spojit	k5eAaPmNgInP
dynamicky	dynamicky	k6eAd1
pomocí	pomocí	k7c2
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
zlepšuje	zlepšovat	k5eAaImIp3nS
jak	jak	k6eAd1
využití	využití	k1gNnSc1
paměti	paměť	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
správu	správa	k1gFnSc4
těchto	tento	k3xDgNnPc2
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
DLL	DLL	kA
soubory	soubor	k1gInPc4
poskytují	poskytovat	k5eAaImIp3nP
standardní	standardní	k2eAgFnPc4d1
výhody	výhoda	k1gFnPc4
sdílených	sdílený	k2eAgFnPc2d1
knihoven	knihovna	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
patří	patřit	k5eAaImIp3nS
zejména	zejména	k9
modularita	modularita	k1gFnSc1
<g/>
;	;	kIx,
ta	ten	k3xDgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
provést	provést	k5eAaPmF
změny	změna	k1gFnPc4
v	v	k7c6
jedné	jeden	k4xCgFnSc6
knihovně	knihovna	k1gFnSc6
sdílené	sdílený	k2eAgFnPc1d1
více	hodně	k6eAd2
aplikacemi	aplikace	k1gFnPc7
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc2
tyto	tento	k3xDgFnPc4
aplikace	aplikace	k1gFnPc4
modifikovat	modifikovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
přínos	přínos	k1gInSc1
modularity	modularita	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
možnosti	možnost	k1gFnSc6
použití	použití	k1gNnSc2
generického	generický	k2eAgNnSc2d1
rozhraní	rozhraní	k1gNnSc2
pro	pro	k7c4
zásuvné	zásuvný	k2eAgInPc4d1
moduly	modul	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhraní	rozhraní	k1gNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
integraci	integrace	k1gFnSc4
různých	různý	k2eAgInPc2d1
modulů	modul	k1gInPc2
do	do	k7c2
již	již	k6eAd1
existujících	existující	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
opět	opět	k6eAd1
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc2
aplikace	aplikace	k1gFnSc2
modifikovat	modifikovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
koncept	koncept	k1gInSc1
dynamické	dynamický	k2eAgFnSc2d1
rozšiřitelnosti	rozšiřitelnost	k1gFnSc2
je	být	k5eAaImIp3nS
hojně	hojně	k6eAd1
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
v	v	k7c6
modelu	model	k1gInSc6
dílčích	dílčí	k2eAgInPc2d1
objektů	objekt	k1gInPc2
COM	COM	kA
(	(	kIx(
<g/>
Component	Component	k1gMnSc1
Object	Object	k1gMnSc1
Model	model	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
systémech	systém	k1gInPc6
Windows	Windows	kA
1	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
sdílejí	sdílet	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc4
aplikace	aplikace	k1gFnPc4
stejný	stejný	k2eAgInSc4d1
adresní	adresní	k2eAgInSc4d1
prostor	prostor	k1gInSc4
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
jsou	být	k5eAaImIp3nP
DLL	DLL	kA
načteny	načíst	k5eAaPmNgInP,k5eAaBmNgInP
pouze	pouze	k6eAd1
jednou	jeden	k4xCgFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Data	datum	k1gNnPc1
těchto	tento	k3xDgFnPc2
knihoven	knihovna	k1gFnPc2
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
sdílena	sdílen	k2eAgNnPc1d1
všemi	všecek	k3xTgFnPc7
aplikacemi	aplikace	k1gFnPc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
využito	využít	k5eAaPmNgNnS
k	k	k7c3
nepřímé	přímý	k2eNgFnSc3d1
meziprocesové	meziprocesový	k2eAgFnSc3d1
komunikaci	komunikace	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
to	ten	k3xDgNnSc1
mohlo	moct	k5eAaImAgNnS
vést	vést	k5eAaImF
k	k	k7c3
poškození	poškození	k1gNnSc3
nějaké	nějaký	k3yIgFnSc2
aplikace	aplikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Windows	Windows	kA
95	#num#	k4
měl	mít	k5eAaImAgInS
pak	pak	k6eAd1
každý	každý	k3xTgInSc1
proces	proces	k1gInSc1
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
adresní	adresní	k2eAgInSc4d1
prostor	prostor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
kód	kód	k1gInSc4
dynamických	dynamický	k2eAgFnPc2d1
knihoven	knihovna	k1gFnPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
sdílen	sdílen	k2eAgMnSc1d1
<g/>
,	,	kIx,
data	datum	k1gNnPc1
jsou	být	k5eAaImIp3nP
od	od	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
privátní	privátní	k2eAgFnSc1d1
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
explicitního	explicitní	k2eAgNnSc2d1
vyžádání	vyžádání	k1gNnSc2
ze	z	k7c2
strany	strana	k1gFnSc2
knihovny	knihovna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
když	když	k8xS
se	se	k3xPyFc4
DLL	DLL	kA
knihovny	knihovna	k1gFnPc1
staly	stát	k5eAaPmAgInP
jádrem	jádro	k1gNnSc7
architektury	architektura	k1gFnSc2
systému	systém	k1gInSc2
Windows	Windows	kA
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
mnoho	mnoho	k4c4
nedostatků	nedostatek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
souhrnně	souhrnně	k6eAd1
nazývají	nazývat	k5eAaImIp3nP
DLL	DLL	kA
peklo	peklo	k1gNnSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
DLL	DLL	kA
hell	hella	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
Microsoft	Microsoft	kA
poskytuje	poskytovat	k5eAaImIp3nS
několik	několik	k4yIc4
řešení	řešení	k1gNnPc2
tohoto	tento	k3xDgInSc2
problému	problém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
ně	on	k3xPp3gMnPc4
</s>
<s>
platforma	platforma	k1gFnSc1
Microsoft	Microsoft	kA
.	.	kIx.
<g/>
NET	NET	kA
nebo	nebo	k8xC
</s>
<s>
virtualizace	virtualizace	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c4
Microsoft	Microsoft	kA
Virtual	Virtual	k1gInSc4
PC	PC	kA
a	a	k8xC
Microsoft	Microsoft	kA
Application	Application	k1gInSc1
Virtualization	Virtualization	k1gInSc1
<g/>
,	,	kIx,
protože	protože	k8xS
nabízí	nabízet	k5eAaImIp3nS
vysoký	vysoký	k2eAgInSc1d1
stupeň	stupeň	k1gInSc1
izolace	izolace	k1gFnSc2
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgNnSc7d1
řešením	řešení	k1gNnSc7
pak	pak	k6eAd1
je	být	k5eAaImIp3nS
implementace	implementace	k1gFnSc1
Side-by-Side	Side-by-Sid	k1gMnSc5
Assembly	Assembly	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Funkce	funkce	k1gFnSc1
knihovny	knihovna	k1gFnSc2
DLL	DLL	kA
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
DLL	DLL	kA
knihovny	knihovna	k1gFnSc2
v	v	k7c6
podstatě	podstata	k1gFnSc6
shodné	shodný	k2eAgInPc1d1
s	s	k7c7
EXE	EXE	kA
soubory	soubor	k1gInPc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vhodné	vhodný	k2eAgNnSc1d1
použít	použít	k5eAaPmF
DLL	DLL	kA
z	z	k7c2
důvodu	důvod	k1gInSc2
větší	veliký	k2eAgFnSc2d2
přehlednosti	přehlednost	k1gFnSc2
programu	program	k1gInSc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc1
možnosti	možnost	k1gFnPc1
exportu	export	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
DLL	DLL	kA
soubory	soubor	k1gInPc4
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
přímo	přímo	k6eAd1
spouštět	spouštět	k5eAaImF
–	–	k?
ke	k	k7c3
spuštění	spuštění	k1gNnSc3
vyžadují	vyžadovat	k5eAaImIp3nP
EXE	EXE	kA
soubor	soubor	k1gInSc4
<g/>
,	,	kIx,
počínaje	počínaje	k7c7
Windows	Windows	kA
95	#num#	k4
jde	jít	k5eAaImIp3nS
o	o	k7c4
rundll	rundll	k1gInSc4
<g/>
32	#num#	k4
<g/>
.	.	kIx.
<g/>
exe	exe	k?
–	–	k?
a	a	k8xC
nepřijímají	přijímat	k5eNaImIp3nP
zprávy	zpráva	k1gFnPc4
<g/>
;	;	kIx,
zato	zato	k6eAd1
poskytují	poskytovat	k5eAaImIp3nP
mechanizmus	mechanizmus	k1gInSc4
pro	pro	k7c4
sdílení	sdílení	k1gNnSc4
kódu	kód	k1gInSc2
a	a	k8xC
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
umožňuje	umožňovat	k5eAaImIp3nS
vývojáři	vývojář	k1gMnPc7
aktualizaci	aktualizace	k1gFnSc4
funkcí	funkce	k1gFnPc2
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc2
aplikace	aplikace	k1gFnSc2
znovu	znovu	k6eAd1
linkovat	linkovat	k5eAaImF
nebo	nebo	k8xC
kompilovat	kompilovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
DLL	DLL	kA
(	(	kIx(
<g/>
např.	např.	kA
soubory	soubor	k1gInPc1
s	s	k7c7
písmy	písmo	k1gNnPc7
<g/>
)	)	kIx)
obsahují	obsahovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
data	datum	k1gNnSc2
a	a	k8xC
žádný	žádný	k3yNgInSc4
kód	kód	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
vývoje	vývoj	k1gInSc2
aplikací	aplikace	k1gFnPc2
si	se	k3xPyFc3
lze	lze	k6eAd1
Windows	Windows	kA
a	a	k8xC
OS	OS	kA
<g/>
/	/	kIx~
<g/>
2	#num#	k4
představit	představit	k5eAaPmF
jako	jako	k8xC,k8xS
kolekci	kolekce	k1gFnSc4
DLL	DLL	kA
souborů	soubor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
inovovány	inovován	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
aplikacím	aplikace	k1gFnPc3
jedné	jeden	k4xCgFnSc2
verze	verze	k1gFnSc2
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
fungovat	fungovat	k5eAaImF
i	i	k9
v	v	k7c6
novějších	nový	k2eAgFnPc6d2
verzích	verze	k1gFnPc6
OS	osa	k1gFnPc2
za	za	k7c2
předpokladu	předpoklad	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
vydavatel	vydavatel	k1gMnSc1
OS	OS	kA
zajistil	zajistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gNnSc1
rozhraní	rozhraní	k1gNnSc1
a	a	k8xC
funkcionalita	funkcionalita	k1gFnSc1
je	být	k5eAaImIp3nS
kompatibilní	kompatibilní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Knihovny	knihovna	k1gFnPc1
DLL	DLL	kA
jsou	být	k5eAaImIp3nP
spouštěny	spouštět	k5eAaImNgFnP
v	v	k7c6
rámci	rámec	k1gInSc6
volajícího	volající	k2eAgInSc2d1
procesu	proces	k1gInSc2
(	(	kIx(
<g/>
tj.	tj.	kA
probíhajícího	probíhající	k2eAgInSc2d1
programu	program	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
sdílejí	sdílet	k5eAaImIp3nP
s	s	k7c7
ním	on	k3xPp3gMnSc7
</s>
<s>
paměťový	paměťový	k2eAgInSc1d1
prostor	prostor	k1gInSc1
a	a	k8xC
</s>
<s>
přístupová	přístupový	k2eAgNnPc1d1
práva	právo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
zajištěna	zajištěn	k2eAgFnSc1d1
nízká	nízký	k2eAgFnSc1d1
režie	režie	k1gFnSc1
při	při	k7c6
použití	použití	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
pro	pro	k7c4
volající	volající	k2eAgInSc4d1
EXE	EXE	kA
soubor	soubor	k1gInSc4
není	být	k5eNaImIp3nS
zajištěna	zajištěn	k2eAgFnSc1d1
ochrana	ochrana	k1gFnSc1
proti	proti	k7c3
případným	případný	k2eAgFnPc3d1
chybám	chyba	k1gFnPc3
v	v	k7c6
DLL	DLL	kA
knihovně	knihovně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
DLL	DLL	kA
souborů	soubor	k1gInPc2
</s>
<s>
Správa	správa	k1gFnSc1
paměti	paměť	k1gFnSc2
</s>
<s>
Ve	v	k7c6
Windows	Windows	kA
API	API	kA
jsou	být	k5eAaImIp3nP
DLL	DLL	kA
soubory	soubor	k1gInPc4
organizovány	organizován	k2eAgInPc4d1
do	do	k7c2
sekcí	sekce	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
každá	každý	k3xTgFnSc1
sekce	sekce	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
atributy	atribut	k1gInPc4
informující	informující	k2eAgInPc1d1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
například	například	k6eAd1
v	v	k7c6
sekci	sekce	k1gFnSc6
povolen	povolit	k5eAaPmNgInS
zápis	zápis	k1gInSc1
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
pro	pro	k7c4
čtení	čtení	k1gNnSc4
a	a	k8xC
také	také	k9
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
vykonatelná	vykonatelný	k2eAgFnSc1d1
(	(	kIx(
<g/>
pro	pro	k7c4
kód	kód	k1gInSc4
<g/>
)	)	kIx)
nebo	nebo	k8xC
není	být	k5eNaImIp3nS
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
jen	jen	k9
pro	pro	k7c4
data	datum	k1gNnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kód	kód	k1gInSc1
v	v	k7c6
DLL	DLL	kA
knihovnách	knihovna	k1gFnPc6
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
sdílen	sdílen	k2eAgInSc1d1
všemi	všecek	k3xTgInPc7
procesy	proces	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
tyto	tento	k3xDgFnPc1
knihovny	knihovna	k1gFnPc1
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knihovny	knihovna	k1gFnPc1
jsou	být	k5eAaImIp3nP
tak	tak	k9
ve	v	k7c6
fyzické	fyzický	k2eAgFnSc6d1
paměti	paměť	k1gFnSc6
načteny	načíst	k5eAaBmNgInP,k5eAaPmNgInP
pouze	pouze	k6eAd1
jednou	jednou	k6eAd1
a	a	k8xC
nejsou	být	k5eNaImIp3nP
odkládány	odkládat	k5eAaImNgFnP
do	do	k7c2
stránkovacího	stránkovací	k2eAgInSc2d1
souboru	soubor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Datové	datový	k2eAgFnPc1d1
sekce	sekce	k1gFnPc1
jsou	být	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
privátní	privátní	k2eAgMnPc1d1
<g/>
,	,	kIx,
takže	takže	k8xS
procesy	proces	k1gInPc1
využívající	využívající	k2eAgFnSc2d1
DLL	DLL	kA
mají	mít	k5eAaImIp3nP
vlastní	vlastní	k2eAgFnSc4d1
kopii	kopie	k1gFnSc4
všech	všecek	k3xTgNnPc2
dat	datum	k1gNnPc2
knihovny	knihovna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Datové	datový	k2eAgFnPc4d1
sekce	sekce	k1gFnPc4
lze	lze	k6eAd1
ovšem	ovšem	k9
sdílet	sdílet	k5eAaImF
a	a	k8xC
využít	využít	k5eAaPmF
je	být	k5eAaImIp3nS
k	k	k7c3
meziprocesové	meziprocesový	k2eAgFnSc3d1
komunikaci	komunikace	k1gFnSc3
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
bezpečnostní	bezpečnostní	k2eAgInSc4d1
problém	problém	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
poškození	poškození	k1gNnSc1
sdílených	sdílený	k2eAgNnPc2d1
dat	datum	k1gNnPc2
může	moct	k5eAaImIp3nS
způsobit	způsobit	k5eAaPmF
nepřijatelné	přijatelný	k2eNgNnSc4d1
chování	chování	k1gNnSc4
jiných	jiný	k2eAgInPc2d1
procesů	proces	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
tato	tento	k3xDgNnPc1
data	datum	k1gNnPc4
používají	používat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
proces	proces	k1gInSc4
běžící	běžící	k2eAgInSc4d1
pod	pod	k7c7
účtem	účet	k1gInSc7
běžného	běžný	k2eAgMnSc2d1
uživatele	uživatel	k1gMnSc2
může	moct	k5eAaImIp3nS
pomocí	pomocí	k7c2
sdílených	sdílený	k2eAgNnPc2d1
dat	datum	k1gNnPc2
ovládnout	ovládnout	k5eAaPmF
jiný	jiný	k2eAgInSc4d1
proces	proces	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
vyšší	vysoký	k2eAgNnSc4d2
oprávnění	oprávnění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
důvod	důvod	k1gInSc1
<g/>
,	,	kIx,
proč	proč	k6eAd1
se	se	k3xPyFc4
později	pozdě	k6eAd2
upustilo	upustit	k5eAaPmAgNnS
od	od	k7c2
sdílených	sdílený	k2eAgFnPc2d1
datových	datový	k2eAgFnPc2d1
sekcí	sekce	k1gFnPc2
v	v	k7c6
DLL	DLL	kA
knihovnách	knihovna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Rozlišení	rozlišení	k1gNnSc1
a	a	k8xC
importování	importování	k1gNnSc1
funkcí	funkce	k1gFnPc2
</s>
<s>
Každá	každý	k3xTgFnSc1
funkce	funkce	k1gFnSc1
exportovaná	exportovaný	k2eAgFnSc1d1
DLL	DLL	kA
knihovnou	knihovna	k1gFnSc7
je	být	k5eAaImIp3nS
identifikována	identifikován	k2eAgFnSc1d1
číslem	číslo	k1gNnSc7
a	a	k8xC
volitelně	volitelně	k6eAd1
i	i	k9
jménem	jméno	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
funkce	funkce	k1gFnSc1
importována	importován	k2eAgFnSc1d1
z	z	k7c2
DLL	DLL	kA
buď	buď	k8xC
na	na	k7c6
základě	základ	k1gInSc6
čísla	číslo	k1gNnSc2
nebo	nebo	k8xC
jména	jméno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Číslo	číslo	k1gNnSc1
reprezentuje	reprezentovat	k5eAaImIp3nS
pozici	pozice	k1gFnSc4
ukazatele	ukazatel	k1gInSc2
funkce	funkce	k1gFnSc2
v	v	k7c6
tabulce	tabulka	k1gFnSc6
adres	adresa	k1gFnPc2
exportovaných	exportovaný	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
běžné	běžný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
interní	interní	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
jsou	být	k5eAaImIp3nP
exportované	exportovaný	k2eAgInPc1d1
pouze	pouze	k6eAd1
svým	svůj	k3xOyFgNnSc7
číslem	číslo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Windows	Windows	kA
API	API	kA
se	se	k3xPyFc4
pak	pak	k6eAd1
ve	v	k7c6
většině	většina	k1gFnSc6
případů	případ	k1gInPc2
zachovávají	zachovávat	k5eAaImIp3nP
mezi	mezi	k7c7
verzemi	verze	k1gFnPc7
názvy	název	k1gInPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
pořadí	pořadí	k1gNnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
měnit	měnit	k5eAaImF
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
nelze	lze	k6eNd1
na	na	k7c4
takový	takový	k3xDgInSc4
import	import	k1gInSc4
spoléhat	spoléhat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Import	import	k1gInSc1
funkcí	funkce	k1gFnPc2
pomocí	pomocí	k7c2
pořadí	pořadí	k1gNnSc2
není	být	k5eNaImIp3nS
o	o	k7c4
moc	moc	k6eAd1
rychlejší	rychlý	k2eAgInSc4d2
<g/>
,	,	kIx,
protože	protože	k8xS
tabulka	tabulka	k1gFnSc1
exportovaných	exportovaný	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
je	být	k5eAaImIp3nS
seřazena	seřadit	k5eAaPmNgFnS
podle	podle	k7c2
jména	jméno	k1gNnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
k	k	k7c3
nalezení	nalezení	k1gNnSc3
funkce	funkce	k1gFnSc2
lze	lze	k6eAd1
použít	použít	k5eAaPmF
binární	binární	k2eAgNnSc4d1
vyhledávání	vyhledávání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
16	#num#	k4
<g/>
bitových	bitový	k2eAgFnPc6d1
verzích	verze	k1gFnPc6
systému	systém	k1gInSc2
Windows	Windows	kA
nebyla	být	k5eNaImAgFnS
tabulka	tabulka	k1gFnSc1
řazena	řadit	k5eAaImNgFnS
<g/>
,	,	kIx,
takže	takže	k8xS
vyhledání	vyhledání	k1gNnSc1
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
o	o	k7c4
něco	něco	k3yInSc4
pomalejší	pomalý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Spustitelný	spustitelný	k2eAgInSc4d1
soubor	soubor	k1gInSc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
vázat	vázat	k5eAaImF
na	na	k7c4
konkrétní	konkrétní	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
knihovny	knihovna	k1gFnSc2
a	a	k8xC
zjistit	zjistit	k5eAaPmF
adresu	adresa	k1gFnSc4
importované	importovaný	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
již	již	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
kompilace	kompilace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
uloží	uložit	k5eAaPmIp3nS
linker	linker	k1gMnSc1
časovou	časový	k2eAgFnSc4d1
značku	značka	k1gFnSc4
a	a	k8xC
kontrolní	kontrolní	k2eAgInSc4d1
součet	součet	k1gInSc4
knihovny	knihovna	k1gFnSc2
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
se	se	k3xPyFc4
importuje	importovat	k5eAaBmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Windows	Windows	kA
pak	pak	k6eAd1
za	za	k7c2
běhu	běh	k1gInSc2
zkontrolují	zkontrolovat	k5eAaPmIp3nP
<g/>
,	,	kIx,
zda	zda	k8xS
se	se	k3xPyFc4
požadovaná	požadovaný	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
používá	používat	k5eAaImIp3nS
a	a	k8xC
pokud	pokud	k8xS
ano	ano	k9
<g/>
,	,	kIx,
obejde	obejít	k5eAaPmIp3nS
standardní	standardní	k2eAgInSc4d1
proces	proces	k1gInSc4
importování	importování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
opačném	opačný	k2eAgInSc6d1
případě	případ	k1gInSc6
proběhne	proběhnout	k5eAaPmIp3nS
import	import	k1gInSc4
klasickou	klasický	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Odložené	odložený	k2eAgNnSc1d1
načtení	načtení	k1gNnSc1
</s>
<s>
Normálně	normálně	k6eAd1
se	se	k3xPyFc4
aplikace	aplikace	k1gFnPc1
využívající	využívající	k2eAgFnSc4d1
jistou	jistý	k2eAgFnSc4d1
knihovnu	knihovna	k1gFnSc4
nespustí	spustit	k5eNaPmIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
tato	tento	k3xDgFnSc1
knihovna	knihovna	k1gFnSc1
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
nalezena	naleznout	k5eAaPmNgFnS,k5eAaBmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
aplikace	aplikace	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
linkována	linkovat	k5eAaImNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
systém	systém	k1gInSc1
nesnaží	snažit	k5eNaImIp3nS
nalézt	nalézt	k5eAaBmF,k5eAaPmF
knihovnu	knihovna	k1gFnSc4
ihned	ihned	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
až	až	k9
když	když	k8xS
je	být	k5eAaImIp3nS
zavolána	zavolán	k2eAgFnSc1d1
nějaká	nějaký	k3yIgFnSc1
funkce	funkce	k1gFnSc1
knihovny	knihovna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
knihovna	knihovna	k1gFnSc1
není	být	k5eNaImIp3nS
nalezena	nalézt	k5eAaBmNgFnS,k5eAaPmNgFnS
<g/>
,	,	kIx,
nelze	lze	k6eNd1
ji	on	k3xPp3gFnSc4
načíst	načíst	k5eAaBmF,k5eAaPmF
nebo	nebo	k8xC
konkrétní	konkrétní	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
neexistuje	existovat	k5eNaImIp3nS
<g/>
,	,	kIx,
vygeneruje	vygenerovat	k5eAaPmIp3nS
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
výjimku	výjimka	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
může	moct	k5eAaImIp3nS
aplikace	aplikace	k1gFnSc1
zachytit	zachytit	k5eAaPmF
a	a	k8xC
vhodně	vhodně	k6eAd1
ošetřit	ošetřit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
aplikace	aplikace	k1gFnSc1
výjimku	výjimek	k1gInSc2
nezachytí	zachytit	k5eNaPmIp3nS
<g/>
,	,	kIx,
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
aplikaci	aplikace	k1gFnSc4
ukončí	ukončit	k5eAaPmIp3nS
s	s	k7c7
hlášením	hlášení	k1gNnSc7
o	o	k7c6
chybě	chyba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Aspekty	aspekt	k1gInPc1
kompilátoru	kompilátor	k1gInSc2
a	a	k8xC
programovacího	programovací	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
</s>
<s>
Delphi	Delphi	k6eAd1
</s>
<s>
V	v	k7c6
hlavičce	hlavička	k1gFnSc6
zdrojového	zdrojový	k2eAgInSc2d1
kódu	kód	k1gInSc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
klíčové	klíčový	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
library	librara	k1gFnSc2
namísto	namísto	k7c2
program	program	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
souboru	soubor	k1gInSc2
je	být	k5eAaImIp3nS
výčet	výčet	k1gInSc4
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
budou	být	k5eAaImBp3nP
exportovány	exportován	k2eAgFnPc1d1
<g/>
,	,	kIx,
v	v	k7c6
klauzuli	klauzule	k1gFnSc6
exports	exportsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Delphi	Delphi	k6eAd1
nepotřebuje	potřebovat	k5eNaImIp3nS
LIB	Liba	k1gFnPc2
soubory	soubor	k1gInPc1
pro	pro	k7c4
import	import	k1gInSc4
funkcí	funkce	k1gFnPc2
z	z	k7c2
DLL	DLL	kA
<g/>
;	;	kIx,
k	k	k7c3
nalinkování	nalinkování	k1gNnSc3
DLL	DLL	kA
je	být	k5eAaImIp3nS
užíváno	užíván	k2eAgNnSc4d1
klíčové	klíčový	k2eAgNnSc4d1
slovo	slovo	k1gNnSc4
external	externat	k5eAaPmAgMnS,k5eAaImAgMnS
v	v	k7c6
deklaraci	deklarace	k1gFnSc6
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Microsoft	Microsoft	kA
Visual	Visual	k1gMnSc1
Basic	Basic	kA
</s>
<s>
Visual	Visual	k1gMnSc1
Basic	Basic	kA
(	(	kIx(
<g/>
VB	VB	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podporuje	podporovat	k5eAaImIp3nS
jen	jen	k9
run-time	run-timat	k5eAaPmIp3nS
nalinkování	nalinkování	k1gNnSc4
<g/>
;	;	kIx,
navíc	navíc	k6eAd1
ale	ale	k8xC
kromě	kromě	k7c2
používání	používání	k1gNnSc2
LoadLibrary	LoadLibrara	k1gFnSc2
a	a	k8xC
GetProcAddress	GetProcAddress	k1gInSc4
API	API	kA
funkcí	funkce	k1gFnSc7
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
povoleny	povolen	k2eAgFnPc1d1
deklarace	deklarace	k1gFnPc1
importovaných	importovaný	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
importu	import	k1gInSc6
DLL	DLL	kA
funkcí	funkce	k1gFnPc2
skrze	skrze	k?
deklarace	deklarace	k1gFnSc2
VB	VB	kA
vyhazuje	vyhazovat	k5eAaImIp3nS
run-time	run-timat	k5eAaPmIp3nS
error	error	k1gInSc1
(	(	kIx(
<g/>
chybu	chyba	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
nemohl	moct	k5eNaImAgMnS
být	být	k5eAaImF
DLL	DLL	kA
soubor	soubor	k1gInSc4
nalezen	nalezen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývojář	vývojář	k1gMnSc1
může	moct	k5eAaImIp3nS
zachytit	zachytit	k5eAaPmF
chybu	chyba	k1gFnSc4
a	a	k8xC
náležitě	náležitě	k6eAd1
ji	on	k3xPp3gFnSc4
ošetřit	ošetřit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
vytváření	vytváření	k1gNnSc6
DLL	DLL	kA
ve	v	k7c6
VB	VB	kA
<g/>
,	,	kIx,
IDE	IDE	kA
dovolí	dovolit	k5eAaPmIp3nS
vytvářet	vytvářet	k5eAaImF
pouze	pouze	k6eAd1
ActiveX	ActiveX	k1gMnPc1
DLL	DLL	kA
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
byly	být	k5eAaImAgFnP
vytvořeny	vytvořen	k2eAgFnPc4d1
metody	metoda	k1gFnPc4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
umožňující	umožňující	k2eAgFnSc1d1
programátorovi	programátorův	k2eAgMnPc1d1
explicitně	explicitně	k6eAd1
zahrnout	zahrnout	k5eAaPmF
.	.	kIx.
<g/>
DEF	DEF	kA
soubor	soubor	k1gInSc4
linkerem	linker	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
obsahuje	obsahovat	k5eAaImIp3nS
řadovou	řadový	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
a	a	k8xC
jméno	jméno	k1gNnSc4
každé	každý	k3xTgFnSc2
exportované	exportovaný	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
vývojáři	vývojář	k1gMnPc1
vytvoření	vytvoření	k1gNnSc2
standardních	standardní	k2eAgInPc2d1
Windows	Windows	kA
DLL	DLL	kA
(	(	kIx(
<g/>
užitím	užití	k1gNnSc7
Visual	Visual	k1gMnSc1
Basic	Basic	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
referencovány	referencován	k2eAgInPc4d1
„	„	k?
<g/>
deklaračním	deklarační	k2eAgFnPc3d1
<g/>
“	“	k?
vyjádřením	vyjádření	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
C	C	kA
a	a	k8xC
C	C	kA
<g/>
++	++	k?
</s>
<s>
Microsoft	Microsoft	kA
Visual	Visual	k1gMnSc1
C	C	kA
<g/>
++	++	k?
(	(	kIx(
<g/>
MSVC	MSVC	kA
<g/>
)	)	kIx)
nabízí	nabízet	k5eAaImIp3nS
několik	několik	k4yIc4
rozšíření	rozšíření	k1gNnPc2
oproti	oproti	k7c3
standardnímu	standardní	k2eAgInSc3d1
C	C	kA
<g/>
++	++	k?
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
funkcím	funkce	k1gFnPc3
být	být	k5eAaImF
specifikované	specifikovaný	k2eAgInPc4d1
jako	jako	k8xC,k8xS
importované	importovaný	k2eAgMnPc4d1
nebo	nebo	k8xC
exportované	exportovaný	k2eAgMnPc4d1
přímo	přímo	k6eAd1
v	v	k7c6
C	C	kA
<g/>
++	++	k?
kódu	kód	k1gInSc2
<g/>
;	;	kIx,
toto	tento	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
adoptováno	adoptovat	k5eAaPmNgNnS
i	i	k9
ostatními	ostatní	k2eAgFnPc7d1
Windows	Windows	kA
C	C	kA
a	a	k8xC
C	C	kA
<g/>
++	++	k?
kompilátory	kompilátor	k1gInPc1
<g/>
,	,	kIx,
zahrnujíc	zahrnovat	k5eAaImSgFnS
Windows	Windows	kA
verze	verze	k1gFnSc2
GCC	GCC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
rozšíření	rozšíření	k1gNnPc1
používají	používat	k5eAaImIp3nP
atribut	atribut	k1gInSc4
__declspec	__declspec	k1gInSc4
před	před	k7c7
deklarací	deklarace	k1gFnSc7
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
<g/>
-li	-li	k?
funkce	funkce	k1gFnSc1
jazyka	jazyk	k1gInSc2
C	C	kA
použity	použít	k5eAaPmNgInP
skrze	skrze	k?
jazyk	jazyk	k1gInSc1
C	C	kA
<g/>
++	++	k?
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
deklarovány	deklarovat	k5eAaBmNgFnP
jako	jako	k8xC,k8xS
extern	extern	k1gMnSc1
"	"	kIx"
<g/>
C	C	kA
<g/>
"	"	kIx"
v	v	k7c6
C	C	kA
<g/>
++	++	k?
kódu	kód	k1gInSc2
<g/>
,	,	kIx,
pro	pro	k7c4
informaci	informace	k1gFnSc4
kompilátoru	kompilátor	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
použito	použit	k2eAgNnSc1d1
nalinkování	nalinkování	k1gNnSc1
jazyka	jazyk	k1gInSc2
C.	C.	kA
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mimo	mimo	k7c4
specifikování	specifikování	k1gNnSc4
importovaných	importovaný	k2eAgFnPc2d1
a	a	k8xC
exportovaných	exportovaný	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
užitím	užití	k1gNnSc7
__declspec	__declspec	k1gInSc1
atributů	atribut	k1gInPc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
funkce	funkce	k1gFnPc1
vypsány	vypsat	k5eAaPmNgFnP
v	v	k7c4
IMPORT	import	k1gInSc4
nebo	nebo	k8xC
EXPORTS	EXPORTS	kA
sekci	sekce	k1gFnSc6
DEF	DEF	kA
souboru	soubor	k1gInSc6
použitém	použitý	k2eAgInSc6d1
v	v	k7c6
projektu	projekt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
DEF	DEF	kA
soubor	soubor	k1gInSc1
je	být	k5eAaImIp3nS
zpracován	zpracovat	k5eAaPmNgInS
linkerem	linker	k1gInSc7
namísto	namísto	k7c2
kompilátoru	kompilátor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Produktem	produkt	k1gInSc7
DLL	DLL	kA
kompilace	kompilace	k1gFnPc1
jsou	být	k5eAaImIp3nP
DLL	DLL	kA
a	a	k8xC
LIB	Liba	k1gFnPc2
soubory	soubor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
LIB	Liba	k1gFnPc2
soubor	soubor	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
objektová	objektový	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
použit	použít	k5eAaPmNgInS
pro	pro	k7c4
link	link	k1gInSc4
k	k	k7c3
DLL	DLL	kA
během	během	k7c2
kompilace	kompilace	k1gFnSc2
<g/>
;	;	kIx,
není	být	k5eNaImIp3nS
nezbytný	nezbytný	k2eAgInSc1d1,k2eNgInSc1d1
pro	pro	k7c4
run-time	run-tiit	k5eAaImRp1nP,k5eAaPmRp1nP,k5eAaBmRp1nP
linkování	linkování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k6eAd1
váš	váš	k3xOp2gInSc4
DLL	DLL	kA
je	být	k5eAaImIp3nS
Component	Component	k1gInSc1
Object	Object	k2eAgInSc1d1
Model	model	k1gInSc1
(	(	kIx(
<g/>
COM	COM	kA
<g/>
)	)	kIx)
server	server	k1gInSc1
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
DLL	DLL	kA
soubor	soubor	k1gInSc4
umístěn	umístit	k5eAaPmNgInS
v	v	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
adresářů	adresář	k1gInPc2
zapsaných	zapsaný	k2eAgInPc2d1
v	v	k7c6
systémové	systémový	k2eAgFnSc6d1
proměnné	proměnná	k1gFnSc6
PATH	PATH	kA
ve	v	k7c6
výchozím	výchozí	k2eAgInSc6d1
systémovém	systémový	k2eAgInSc6d1
adresáři	adresář	k1gInSc6
nebo	nebo	k8xC
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
adresáři	adresář	k1gInSc6
jako	jako	k9
je	být	k5eAaImIp3nS
program	program	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
COM	COM	kA
server	server	k1gInSc4
DLL	DLL	kA
jsou	být	k5eAaImIp3nP
registrovány	registrovat	k5eAaBmNgFnP
pomocí	pomocí	k7c2
regsvr	regsvra	k1gFnPc2
<g/>
32	#num#	k4
<g/>
.	.	kIx.
<g/>
exe	exe	k?
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
umístí	umístit	k5eAaPmIp3nS
lokaci	lokace	k1gFnSc4
DLL	DLL	kA
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
globální	globální	k2eAgFnPc1d1
unikátní	unikátní	k2eAgFnPc1d1
ID	ido	k1gNnPc2
(	(	kIx(
<g/>
GUID	GUID	kA
<g/>
)	)	kIx)
do	do	k7c2
registrů	registr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Programy	program	k1gInPc7
poté	poté	k6eAd1
mohou	moct	k5eAaImIp3nP
použít	použít	k5eAaPmF
DLL	DLL	kA
vyhledáním	vyhledání	k1gNnSc7
jeho	jeho	k3xOp3gNnPc2
GUID	GUID	kA
v	v	k7c6
registrech	registr	k1gInPc6
k	k	k7c3
nalezení	nalezení	k1gNnSc3
jeho	jeho	k3xOp3gFnSc2
lokace	lokace	k1gFnSc2
(	(	kIx(
<g/>
cesty	cesta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Petzold	Petzolda	k1gFnPc2
1999	#num#	k4
strana	strana	k1gFnSc1
<g/>
:	:	kIx,
10471	#num#	k4
2	#num#	k4
Petzold	Petzolda	k1gFnPc2
1999	#num#	k4
strana	strana	k1gFnSc1
<g/>
:	:	kIx,
1048	#num#	k4
<g/>
↑	↑	k?
</s>
<s>
Linker	Linker	k1gInSc1
Support	support	k1gInSc1
for	forum	k1gNnPc2
Delay-Loaded	Delay-Loaded	k1gMnSc1
DLLs	DLLs	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microsoft	Microsoft	kA
Corporation	Corporation	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PETRUSHA	PETRUSHA	kA
<g/>
,	,	kIx,
Ron	ron	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Creating	Creating	k1gInSc4
a	a	k8xC
Windows	Windows	kA
DLL	DLL	kA
with	with	k1gMnSc1
Visual	Visual	k1gMnSc1
Basic	Basic	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Reilly	Reill	k1gMnPc4
Media	medium	k1gNnSc2
<g/>
,	,	kIx,
2005-04-26	2005-04-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MSDN	MSDN	kA
<g/>
,	,	kIx,
Using	Using	k1gMnSc1
extern	extern	k1gMnSc1
to	ten	k3xDgNnSc4
Specify	Specif	k1gMnPc7
Linkage	Linkag	k1gMnPc4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
PETZOLD	PETZOLD	kA
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Programování	programování	k1gNnSc1
ve	v	k7c6
Windows	Windows	kA
<g/>
:	:	kIx,
legendární	legendární	k2eAgFnSc2d1
publikace	publikace	k1gFnSc2
o	o	k7c4
programování	programování	k1gNnSc4
Win	Win	k1gFnSc2
<g/>
32	#num#	k4
API	API	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Aleš	Aleš	k1gMnSc1
Polcar	Polcar	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Veselský	Veselský	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Computer	computer	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
1216	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Programování	programování	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7226	#num#	k4
<g/>
-	-	kIx~
<g/>
206	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
rejstřík	rejstřík	k1gInSc1
<g/>
.	.	kIx.
</s>
