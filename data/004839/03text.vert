<s>
Industrial	Industrial	k1gMnSc1	Industrial
Light	Light	k1gMnSc1	Light
&	&	k?	&
Magic	Magic	k1gMnSc1	Magic
(	(	kIx(	(
<g/>
ILM	ILM	kA	ILM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
zabývající	zabývající	k2eAgMnPc4d1	zabývající
se	se	k3xPyFc4	se
vizuálními	vizuální	k2eAgInPc7d1	vizuální
efekty	efekt	k1gInPc7	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
zakladatelem	zakladatel	k1gMnSc7	zakladatel
je	být	k5eAaImIp3nS	být
George	George	k1gFnSc1	George
Lucas	Lucas	k1gInSc1	Lucas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
snímek	snímek	k1gInSc4	snímek
ze	z	k7c2	z
série	série	k1gFnSc2	série
Hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
válek	válka	k1gFnPc2	válka
tuto	tento	k3xDgFnSc4	tento
společnost	společnost	k1gFnSc4	společnost
založil	založit	k5eAaPmAgMnS	založit
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
speciálních	speciální	k2eAgInPc2d1	speciální
efektů	efekt	k1gInPc2	efekt
ve	v	k7c6	v
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
si	se	k3xPyFc3	se
za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
do	do	k7c2	do
dobu	doba	k1gFnSc4	doba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
jméno	jméno	k1gNnSc4	jméno
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
jí	on	k3xPp3gFnSc3	on
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejlepším	dobrý	k2eAgNnPc3d3	nejlepší
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
oboru	obor	k1gInSc6	obor
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Za	za	k7c4	za
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
oceněna	ocenit	k5eAaPmNgFnS	ocenit
30	[number]	k4	30
cenami	cena	k1gFnPc7	cena
Akademie	akademie	k1gFnSc2	akademie
za	za	k7c4	za
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
efekty	efekt	k1gInPc4	efekt
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
