<s>
Jamajka	Jamajka	k1gFnSc1	Jamajka
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
ostrově	ostrov	k1gInSc6	ostrov
ve	v	k7c6	v
Velkých	velký	k2eAgFnPc6d1	velká
Antilách	Antily	k1gFnPc6	Antily
v	v	k7c6	v
Karibském	karibský	k2eAgNnSc6d1	Karibské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
anglicky	anglicky	k6eAd1	anglicky
mluvící	mluvící	k2eAgInSc4d1	mluvící
ostrov	ostrov	k1gInSc4	ostrov
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Jamajky	Jamajka	k1gFnSc2	Jamajka
<g/>
.	.	kIx.	.
</s>
<s>
Jamajku	Jamajka	k1gFnSc4	Jamajka
objevil	objevit	k5eAaPmAgMnS	objevit
roku	rok	k1gInSc2	rok
1494	[number]	k4	1494
Kryštof	Kryštof	k1gMnSc1	Kryštof
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1509	[number]	k4	1509
byla	být	k5eAaImAgFnS	být
kolonizována	kolonizovat	k5eAaBmNgFnS	kolonizovat
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1655	[number]	k4	1655
až	až	k9	až
1962	[number]	k4	1962
byla	být	k5eAaImAgFnS	být
korunní	korunní	k2eAgFnSc7d1	korunní
kolonií	kolonie	k1gFnSc7	kolonie
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
(	(	kIx(	(
<g/>
1674	[number]	k4	1674
až	až	k9	až
1688	[number]	k4	1688
byl	být	k5eAaImAgInS	být
guvernérem	guvernér	k1gMnSc7	guvernér
proslulý	proslulý	k2eAgMnSc1d1	proslulý
korzár	korzár	k1gMnSc1	korzár
Henry	Henry	k1gMnSc1	Henry
Morgan	morgan	k1gMnSc1	morgan
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
již	již	k9	již
povýšený	povýšený	k2eAgInSc4d1	povýšený
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
tvořil	tvořit	k5eAaImAgInS	tvořit
ostrov	ostrov	k1gInSc1	ostrov
součást	součást	k1gFnSc4	součást
Západoindické	západoindický	k2eAgFnSc2d1	Západoindická
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
Jamajka	Jamajka	k1gFnSc1	Jamajka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
(	(	kIx(	(
<g/>
čímž	což	k3yQnSc7	což
federace	federace	k1gFnSc1	federace
fakticky	fakticky	k6eAd1	fakticky
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
svou	svůj	k3xOyFgFnSc4	svůj
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
státu	stát	k1gInSc2	stát
stojí	stát	k5eAaImIp3nS	stát
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
zastupující	zastupující	k2eAgFnSc4d1	zastupující
britskou	britský	k2eAgFnSc4d1	britská
královnu	královna	k1gFnSc4	královna
<g/>
.	.	kIx.	.
</s>
<s>
Jamajská	jamajský	k2eAgFnSc1d1	jamajská
historie	historie	k1gFnSc1	historie
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
migrací	migrace	k1gFnSc7	migrace
indiánů	indián	k1gMnPc2	indián
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
650	[number]	k4	650
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
zemi	zem	k1gFnSc4	zem
"	"	kIx"	"
<g/>
Xaymaca	Xaymaca	k1gFnSc1	Xaymaca
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
země	zem	k1gFnPc4	zem
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
pár	pár	k4xCyI	pár
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
ostrovní	ostrovní	k2eAgInSc1d1	ostrovní
ráj	ráj	k1gInSc1	ráj
stal	stát	k5eAaPmAgInS	stát
známým	známý	k2eAgInSc7d1	známý
jako	jako	k9	jako
Jamajka	Jamajka	k1gFnSc1	Jamajka
<g/>
,	,	kIx,	,
klenot	klenot	k1gInSc1	klenot
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Arawakové	Arawakový	k2eAgNnSc1d1	Arawakový
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
indiáni	indián	k1gMnPc1	indián
jmenovali	jmenovat	k5eAaImAgMnP	jmenovat
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
mírumilovní	mírumilovný	k2eAgMnPc1d1	mírumilovný
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
harmonii	harmonie	k1gFnSc6	harmonie
až	až	k9	až
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
Španělů	Španěl	k1gMnPc2	Španěl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1494	[number]	k4	1494
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
stal	stát	k5eAaPmAgMnS	stát
osudným	osudný	k2eAgInSc7d1	osudný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
muzeích	muzeum	k1gNnPc6	muzeum
po	po	k7c6	po
nich	on	k3xPp3gMnPc6	on
zbylo	zbýt	k5eAaPmAgNnS	zbýt
jen	jen	k9	jen
nepatrné	nepatrný	k2eAgNnSc1d1	nepatrný
množství	množství	k1gNnSc1	množství
artefaktů	artefakt	k1gInPc2	artefakt
<g/>
.	.	kIx.	.
</s>
<s>
Jamajka	Jamajka	k1gFnSc1	Jamajka
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
španělskou	španělský	k2eAgFnSc7d1	španělská
nadvládou	nadvláda	k1gFnSc7	nadvláda
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1655	[number]	k4	1655
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
anglickou	anglický	k2eAgFnSc7d1	anglická
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
importu	import	k1gInSc6	import
otroků	otrok	k1gMnPc2	otrok
a	a	k8xC	a
produkci	produkce	k1gFnSc4	produkce
třtinového	třtinový	k2eAgInSc2d1	třtinový
cukru	cukr	k1gInSc2	cukr
a	a	k8xC	a
výrobě	výroba	k1gFnSc3	výroba
rumu	rum	k1gInSc2	rum
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
slavný	slavný	k2eAgMnSc1d1	slavný
anglický	anglický	k2eAgMnSc1d1	anglický
pirát	pirát	k1gMnSc1	pirát
Henry	Henry	k1gMnSc1	Henry
Morgan	morgan	k1gMnSc1	morgan
za	za	k7c4	za
plenění	plenění	k1gNnSc4	plenění
španělských	španělský	k2eAgFnPc2d1	španělská
kolonií	kolonie	k1gFnPc2	kolonie
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
guvernérem	guvernér	k1gMnSc7	guvernér
Jamajky	Jamajka	k1gFnSc2	Jamajka
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
zvěčněno	zvěčnit	k5eAaPmNgNnS	zvěčnit
v	v	k7c6	v
názvu	název	k1gInSc6	název
rumu	rum	k1gInSc2	rum
Captain	Captain	k1gMnSc1	Captain
Morgan	morgan	k1gMnSc1	morgan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
v	v	k7c6	v
r.	r.	kA	r.
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
však	však	k9	však
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
výsledného	výsledný	k2eAgInSc2d1	výsledný
produktu	produkt	k1gInSc2	produkt
přestěhována	přestěhovat	k5eAaPmNgFnS	přestěhovat
na	na	k7c4	na
Puerto	Puerta	k1gFnSc5	Puerta
Rico	Rico	k1gMnSc1	Rico
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1962	[number]	k4	1962
se	se	k3xPyFc4	se
Jamajka	Jamajka	k1gFnSc1	Jamajka
stala	stát	k5eAaPmAgFnS	stát
nezávislým	závislý	k2eNgInSc7d1	nezávislý
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Úderem	úder	k1gInSc7	úder
dvanácté	dvanáctý	k4xOgFnSc2	dvanáctý
hodiny	hodina	k1gFnSc2	hodina
toho	ten	k3xDgInSc2	ten
horkého	horký	k2eAgInSc2d1	horký
letního	letní	k2eAgInSc2d1	letní
večera	večer	k1gInSc2	večer
byla	být	k5eAaImAgFnS	být
britská	britský	k2eAgFnSc1d1	britská
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
spuštěna	spustit	k5eAaPmNgFnS	spustit
dolů	dolů	k6eAd1	dolů
a	a	k8xC	a
s	s	k7c7	s
velikým	veliký	k2eAgNnSc7d1	veliké
očekáváním	očekávání	k1gNnSc7	očekávání
a	a	k8xC	a
úlevou	úleva	k1gFnSc7	úleva
vytáhlo	vytáhnout	k5eAaPmAgNnS	vytáhnout
konečně	konečně	k6eAd1	konečně
jamajské	jamajský	k2eAgNnSc1d1	jamajské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
na	na	k7c4	na
stožár	stožár	k1gInSc4	stožár
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
barvy	barva	k1gFnSc2	barva
-	-	kIx~	-
černou	černý	k2eAgFnSc4d1	černá
<g/>
,	,	kIx,	,
zelenou	zelený	k2eAgFnSc4d1	zelená
a	a	k8xC	a
zlatou	zlatý	k2eAgFnSc4d1	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
nezávislosti	nezávislost	k1gFnSc3	nezávislost
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
byla	být	k5eAaImAgFnS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
trnitá	trnitý	k2eAgFnSc1d1	trnitá
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1492	[number]	k4	1492
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
připlul	připlout	k5eAaPmAgMnS	připlout
Kryštof	Kryštof	k1gMnSc1	Kryštof
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
do	do	k7c2	do
Nového	Nového	k2eAgInSc2d1	Nového
Světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Jamajka	Jamajka	k1gFnSc1	Jamajka
vedena	vést	k5eAaImNgFnS	vést
jako	jako	k9	jako
kolonie	kolonie	k1gFnSc1	kolonie
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
španělská	španělský	k2eAgFnSc1d1	španělská
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
britská	britský	k2eAgFnSc1d1	britská
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgNnPc1	tři
století	století	k1gNnPc2	století
mezi	mezi	k7c7	mezi
vyloděním	vylodění	k1gNnSc7	vylodění
Evropanů	Evropan	k1gMnPc2	Evropan
a	a	k8xC	a
nynějším	nynější	k2eAgInSc7d1	nynější
dnem	den	k1gInSc7	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc4	ostrov
neovládnutelný	ovládnutelný	k2eNgMnSc1d1	ovládnutelný
<g/>
,	,	kIx,	,
koloniální	koloniální	k2eAgFnPc1d1	koloniální
vlády	vláda	k1gFnPc1	vláda
byly	být	k5eAaImAgFnP	být
neustále	neustále	k6eAd1	neustále
napadány	napadán	k2eAgFnPc1d1	napadána
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
piráty	pirát	k1gMnPc7	pirát
lačnícími	lačnící	k2eAgMnPc7d1	lačnící
po	po	k7c6	po
zlatě	zlato	k1gNnSc6	zlato
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
skupinkami	skupinka	k1gFnPc7	skupinka
osvobozujícími	osvobozující	k2eAgFnPc7d1	osvobozující
africké	africký	k2eAgFnPc4d1	africká
válečníky	válečník	k1gMnPc7	válečník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
zbídačenými	zbídačený	k2eAgMnPc7d1	zbídačený
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
hlasitě	hlasitě	k6eAd1	hlasitě
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
násilně	násilně	k6eAd1	násilně
<g/>
)	)	kIx)	)
demonstrovali	demonstrovat	k5eAaBmAgMnP	demonstrovat
svou	svůj	k3xOyFgFnSc4	svůj
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Jamajky	Jamajka	k1gFnSc2	Jamajka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
lemované	lemovaný	k2eAgInPc4d1	lemovaný
korálovými	korálový	k2eAgInPc7d1	korálový
útesy	útes	k1gInPc7	útes
<g/>
,	,	kIx,	,
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
skalnaté	skalnatý	k2eAgNnSc1d1	skalnaté
<g/>
.	.	kIx.	.
</s>
<s>
Vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
hornaté	hornatý	k2eAgInPc1d1	hornatý
s	s	k7c7	s
vápencovými	vápencový	k2eAgFnPc7d1	vápencová
krasovými	krasový	k2eAgFnPc7d1	krasová
plošinami	plošina	k1gFnPc7	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vlhké	vlhký	k2eAgInPc1d1	vlhký
tropické	tropický	k2eAgInPc1d1	tropický
s	s	k7c7	s
občasnými	občasný	k2eAgInPc7d1	občasný
hurikány	hurikán	k1gInPc7	hurikán
<g/>
.	.	kIx.	.
</s>
<s>
Rostou	růst	k5eAaImIp3nP	růst
tu	tu	k6eAd1	tu
stále	stále	k6eAd1	stále
zelené	zelený	k2eAgInPc1d1	zelený
tropické	tropický	k2eAgInPc1d1	tropický
lesy	les	k1gInPc1	les
a	a	k8xC	a
savany	savana	k1gFnPc1	savana
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
hospodářsky	hospodářsky	k6eAd1	hospodářsky
relativně	relativně	k6eAd1	relativně
vyspělý	vyspělý	k2eAgInSc4d1	vyspělý
stát	stát	k1gInSc4	stát
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
negramotností	negramotnost	k1gFnSc7	negramotnost
a	a	k8xC	a
bohatým	bohatý	k2eAgInSc7d1	bohatý
cestovním	cestovní	k2eAgInSc7d1	cestovní
ruchem	ruch	k1gInSc7	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
zde	zde	k6eAd1	zde
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
198	[number]	k4	198
cm	cm	kA	cm
na	na	k7c4	na
metr	metr	k1gInSc4	metr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hornatých	hornatý	k2eAgFnPc6d1	hornatá
oblastech	oblast	k1gFnPc6	oblast
dokonce	dokonce	k9	dokonce
okolo	okolo	k7c2	okolo
762	[number]	k4	762
cm	cm	kA	cm
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
oblasti	oblast	k1gFnSc6	oblast
na	na	k7c6	na
západě	západ	k1gInSc6	západ
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
76,2	[number]	k4	76,2
cm	cm	kA	cm
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
je	být	k5eAaImIp3nS	být
27	[number]	k4	27
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Nejteplejší	teplý	k2eAgInPc1d3	nejteplejší
měsíce	měsíc	k1gInPc1	měsíc
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgNnSc1d1	zimní
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
prosinec	prosinec	k1gInSc1	prosinec
až	až	k8xS	až
březen	březen	k1gInSc1	březen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
znatelně	znatelně	k6eAd1	znatelně
chladnější	chladný	k2eAgMnSc1d2	chladnější
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
na	na	k7c6	na
Blue	Blue	k1gFnSc6	Blue
Mountain	Mountain	k2eAgInSc4d1	Mountain
Peak	Peak	k1gInSc4	Peak
(	(	kIx(	(
<g/>
Modrá	modrý	k2eAgFnSc1d1	modrá
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
teplota	teplota	k1gFnSc1	teplota
maximálně	maximálně	k6eAd1	maximálně
ke	k	k7c3	k
13	[number]	k4	13
stupňům	stupeň	k1gInPc3	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
významných	významný	k2eAgInPc2d1	významný
znaků	znak	k1gInPc2	znak
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
škála	škála	k1gFnSc1	škála
nadmořských	nadmořský	k2eAgFnPc2d1	nadmořská
výšek	výška	k1gFnPc2	výška
-	-	kIx~	-
od	od	k7c2	od
150	[number]	k4	150
m	m	kA	m
až	až	k8xS	až
k	k	k7c3	k
nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
bodu	bod	k1gInSc3	bod
ostrova	ostrov	k1gInSc2	ostrov
2256	[number]	k4	2256
m.	m.	k?	m.
Většina	většina	k1gFnSc1	většina
povrchu	povrch	k1gInSc2	povrch
ostrova	ostrov	k1gInSc2	ostrov
má	mít	k5eAaImIp3nS	mít
vápencové	vápencový	k2eAgNnSc1d1	vápencové
složení	složení	k1gNnSc1	složení
<g/>
,	,	kIx,	,
Blue	Blue	k1gNnSc1	Blue
Montains	Montainsa	k1gFnPc2	Montainsa
je	být	k5eAaImIp3nS	být
komplex	komplex	k1gInSc1	komplex
geologicky	geologicky	k6eAd1	geologicky
odrážející	odrážející	k2eAgFnSc4d1	odrážející
rozmanitou	rozmanitý	k2eAgFnSc4d1	rozmanitá
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
sopečnou	sopečný	k2eAgFnSc4d1	sopečná
činnost	činnost	k1gFnSc4	činnost
i	i	k8xC	i
metamorfní	metamorfní	k2eAgFnPc4d1	metamorfní
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgInSc1d1	zdejší
park	park	k1gInSc1	park
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
různá	různý	k2eAgNnPc4d1	různé
horská	horský	k2eAgNnPc4d1	horské
pásma	pásmo	k1gNnPc4	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hor	hora	k1gFnPc2	hora
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
krajinu	krajina	k1gFnSc4	krajina
také	také	k9	také
údolí	údolí	k1gNnPc1	údolí
a	a	k8xC	a
roviny	rovina	k1gFnPc1	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
hlavních	hlavní	k2eAgFnPc2d1	hlavní
nížin	nížina	k1gFnPc2	nížina
-	-	kIx~	-
Vere	Vere	k1gInSc1	Vere
<g/>
,	,	kIx,	,
St	St	kA	St
Jago	Jaga	k1gFnSc5	Jaga
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc4	Georg
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
Liguanea	Liguane	k1gInSc2	Liguane
a	a	k8xC	a
Pedro	Pedro	k1gNnSc4	Pedro
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
stěžejními	stěžejní	k2eAgNnPc7d1	stěžejní
místy	místo	k1gNnPc7	místo
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInPc4	jejich
především	především	k9	především
zemědělsky	zemědělsky	k6eAd1	zemědělsky
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Přírodních	přírodní	k2eAgFnPc2d1	přírodní
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
pramenů	pramen	k1gInPc2	pramen
najdeme	najít	k5eAaPmIp1nP	najít
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
hojně	hojně	k6eAd1	hojně
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
120	[number]	k4	120
řek	řeka	k1gFnPc2	řeka
protéká	protékat	k5eAaImIp3nS	protékat
zemí	zem	k1gFnPc2	zem
od	od	k7c2	od
hor	hora	k1gFnPc2	hora
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	Řek	k1gMnPc4	Řek
na	na	k7c6	na
severu	sever	k1gInSc6	sever
bývají	bývat	k5eAaImIp3nP	bývat
kratší	krátký	k2eAgFnPc1d2	kratší
a	a	k8xC	a
rychlejší	rychlý	k2eAgFnPc1d2	rychlejší
než	než	k8xS	než
řeky	řeka	k1gFnPc1	řeka
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
tekoucí	tekoucí	k2eAgFnPc1d1	tekoucí
řeky	řeka	k1gFnPc1	řeka
-	-	kIx~	-
Black	Black	k1gMnSc1	Black
River	River	k1gMnSc1	River
<g/>
,	,	kIx,	,
Rio	Rio	k1gMnSc5	Rio
Cobre	Cobr	k1gMnSc5	Cobr
<g/>
,	,	kIx,	,
Milk	Milk	k1gMnSc1	Milk
River	River	k1gMnSc1	River
<g/>
,	,	kIx,	,
Rio	Rio	k1gMnSc5	Rio
Grande	grand	k1gMnSc5	grand
a	a	k8xC	a
Martha	Marth	k1gMnSc4	Marth
Brae	Bra	k1gMnSc4	Bra
-	-	kIx~	-
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
k	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
k	k	k7c3	k
transportu	transport	k1gInSc3	transport
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
k	k	k7c3	k
zavlažování	zavlažování	k1gNnSc3	zavlažování
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
několik	několik	k4yIc1	několik
minerálních	minerální	k2eAgInPc2d1	minerální
pramenů	pramen	k1gInPc2	pramen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
svými	svůj	k3xOyFgInPc7	svůj
léčebnými	léčebný	k2eAgInPc7d1	léčebný
účinky	účinek	k1gInPc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
lázeňská	lázeňská	k1gFnSc1	lázeňská
střediska	středisko	k1gNnSc2	středisko
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Milk	Milk	k1gMnSc1	Milk
River	River	k1gMnSc1	River
Bath	Bath	k1gMnSc1	Bath
<g/>
,	,	kIx,	,
Bath	Bath	k1gMnSc1	Bath
Fountain	Fountain	k1gMnSc1	Fountain
<g/>
,	,	kIx,	,
Spa	Spa	k1gMnSc1	Spa
a	a	k8xC	a
Grand	grand	k1gMnSc1	grand
Lido	Lido	k1gNnSc4	Lido
Sans	Sans	k1gInSc1	Sans
Souci	Souce	k1gMnSc3	Souce
<g/>
,	,	kIx,	,
Rockfort	Rockfort	k1gInSc1	Rockfort
Mineral	Mineral	k1gMnSc1	Mineral
Bath	Bath	k1gMnSc1	Bath
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
málo	málo	k6eAd1	málo
známými	známý	k1gMnPc7	známý
místy	místy	k6eAd1	místy
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
komunitu	komunita	k1gFnSc4	komunita
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Příroda	příroda	k1gFnSc1	příroda
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
tropickým	tropický	k2eAgNnSc7d1	tropické
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
bílé	bílý	k2eAgFnPc1d1	bílá
pláže	pláž	k1gFnPc1	pláž
s	s	k7c7	s
korály	korál	k1gMnPc7	korál
<g/>
,	,	kIx,	,
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
tropické	tropický	k2eAgInPc4d1	tropický
lesy	les	k1gInPc4	les
a	a	k8xC	a
vápencové	vápencový	k2eAgFnPc4d1	vápencová
krasové	krasový	k2eAgFnPc4d1	krasová
plošiny	plošina	k1gFnPc4	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgInPc1d1	plný
cenných	cenný	k2eAgFnPc2d1	cenná
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
,	,	kIx,	,
různobarevně	různobarevně	k6eAd1	různobarevně
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
je	být	k5eAaImIp3nS	být
také	také	k9	také
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
endemických	endemický	k2eAgInPc2d1	endemický
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
organizmy	organizmus	k1gInPc1	organizmus
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jamajka	Jamajka	k1gFnSc1	Jamajka
nabízí	nabízet	k5eAaImIp3nS	nabízet
mnoho	mnoho	k4c4	mnoho
jedinečných	jedinečný	k2eAgInPc2d1	jedinečný
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
i	i	k9	i
několik	několik	k4yIc4	několik
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
např.	např.	kA	např.
krokodýly	krokodýl	k1gMnPc7	krokodýl
nebo	nebo	k8xC	nebo
rozmanité	rozmanitý	k2eAgInPc4d1	rozmanitý
druhy	druh	k1gInPc4	druh
netopýrů	netopýr	k1gMnPc2	netopýr
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
např.	např.	kA	např.
endemický	endemický	k2eAgInSc1d1	endemický
druh	druh	k1gInSc1	druh
hlodavce	hlodavec	k1gMnSc2	hlodavec
Geocapromys	Geocapromys	k1gInSc4	Geocapromys
brownii	brownium	k1gNnPc7	brownium
(	(	kIx(	(
<g/>
hutia	hutia	k1gFnSc1	hutia
prasečí	prasečí	k2eAgFnSc1d1	prasečí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejrozšířenější	rozšířený	k2eAgMnPc4d3	nejrozšířenější
savce	savec	k1gMnPc4	savec
patří	patřit	k5eAaImIp3nP	patřit
kozy	koza	k1gFnPc1	koza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
najdeme	najít	k5eAaPmIp1nP	najít
takřka	takřka	k6eAd1	takřka
všude	všude	k6eAd1	všude
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
žije	žít	k5eAaImIp3nS	žít
kolem	kolem	k7c2	kolem
250	[number]	k4	250
druhů	druh	k1gInPc2	druh
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Jamajka	Jamajka	k1gFnSc1	Jamajka
je	být	k5eAaImIp3nS	být
rájem	ráj	k1gInSc7	ráj
pro	pro	k7c4	pro
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
druhů	druh	k1gInPc2	druh
tropických	tropický	k2eAgMnPc2d1	tropický
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepšími	dobrý	k2eAgFnPc7d3	nejlepší
oblastmi	oblast	k1gFnPc7	oblast
jsou	být	k5eAaImIp3nP	být
Blue	Blue	k1gNnSc1	Blue
Mountains	Mountainsa	k1gFnPc2	Mountainsa
<g/>
,	,	kIx,	,
Cockpit	Cockpit	k1gFnSc1	Cockpit
Mountains	Mountains	k1gInSc1	Mountains
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
River	River	k1gMnSc1	River
nebo	nebo	k8xC	nebo
Great	Great	k2eAgInSc1d1	Great
Morass	Morass	k1gInSc1	Morass
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
křovinami	křovina	k1gFnPc7	křovina
najdeme	najít	k5eAaPmIp1nP	najít
hady	had	k1gMnPc4	had
<g/>
,	,	kIx,	,
ještěrky	ještěrka	k1gFnPc4	ještěrka
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
tropické	tropický	k2eAgMnPc4d1	tropický
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc1d1	místní
hmyz	hmyz	k1gInSc1	hmyz
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
různorodý	různorodý	k2eAgInSc1d1	různorodý
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
hmyzem	hmyz	k1gInSc7	hmyz
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
jsou	být	k5eAaImIp3nP	být
motýli	motýl	k1gMnPc1	motýl
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgNnSc3	svůj
pestrobarevnému	pestrobarevný	k2eAgNnSc3d1	pestrobarevné
zbarvení	zbarvení	k1gNnSc3	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
objevovat	objevovat	k5eAaImF	objevovat
hejna	hejno	k1gNnPc4	hejno
světlušek	světluška	k1gFnPc2	světluška
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
Jamajky	Jamajka	k1gFnSc2	Jamajka
jsou	být	k5eAaImIp3nP	být
přemnožení	přemnožený	k2eAgMnPc1d1	přemnožený
švábi	šváb	k1gMnPc1	šváb
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nepředstavují	představovat	k5eNaImIp3nP	představovat
žádný	žádný	k3yNgInSc4	žádný
větší	veliký	k2eAgInSc4d2	veliký
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
pestrý	pestrý	k2eAgInSc1d1	pestrý
je	být	k5eAaImIp3nS	být
také	také	k9	také
podmořský	podmořský	k2eAgInSc1d1	podmořský
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
korálech	korál	k1gInPc6	korál
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
narazíme	narazit	k5eAaPmIp1nP	narazit
na	na	k7c4	na
medúzy	medúza	k1gFnPc4	medúza
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgFnPc4d1	mořská
želvy	želva	k1gFnPc4	želva
<g/>
,	,	kIx,	,
tuleně	tuleň	k1gMnPc4	tuleň
<g/>
,	,	kIx,	,
delfíny	delfín	k1gMnPc4	delfín
a	a	k8xC	a
pestrobarevné	pestrobarevný	k2eAgFnPc4d1	pestrobarevná
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Blue	Blu	k1gFnSc2	Blu
and	and	k?	and
John	John	k1gMnSc1	John
Crow	Crow	k1gMnSc1	Crow
Mountains	Mountains	k1gInSc1	Mountains
(	(	kIx(	(
<g/>
BJCMNP	BJCMNP	kA	BJCMNP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
cípu	cíp	k1gInSc6	cíp
Jamajky	Jamajka	k1gFnSc2	Jamajka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
486	[number]	k4	486
km2	km2	k4	km2
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
4,4	[number]	k4	4,4
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
Jamajky	Jamajka	k1gFnSc2	Jamajka
<g/>
.	.	kIx.	.
</s>
<s>
Hory	hora	k1gFnPc1	hora
v	v	k7c6	v
parku	park	k1gInSc6	park
dominují	dominovat	k5eAaImIp3nP	dominovat
celému	celý	k2eAgInSc3d1	celý
obzoru	obzor	k1gInSc3	obzor
východní	východní	k2eAgFnSc2d1	východní
Jamajky	Jamajka	k1gFnSc2	Jamajka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
ostrova	ostrov	k1gInSc2	ostrov
-	-	kIx~	-
Blue	Blue	k1gFnSc1	Blue
Mountain	Mountain	k1gMnSc1	Mountain
Peak	Peak	k1gMnSc1	Peak
(	(	kIx(	(
<g/>
2256	[number]	k4	2256
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
BJCMNP	BJCMNP	kA	BJCMNP
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
horských	horský	k2eAgInPc2d1	horský
masivů	masiv	k1gInPc2	masiv
-	-	kIx~	-
Port	port	k1gInSc1	port
Royal	Royal	k1gMnSc1	Royal
Mountains	Mountains	k1gInSc1	Mountains
<g/>
,	,	kIx,	,
Blue	Blue	k1gInSc1	Blue
Mountains	Mountains	k1gInSc1	Mountains
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Crow	Crow	k1gFnSc2	Crow
Mountains	Mountains	k1gInSc1	Mountains
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
BJCMNP	BJCMNP	kA	BJCMNP
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
a	a	k8xC	a
nejsouvislejších	souvislý	k2eAgFnPc2d3	nejsouvislejší
ploch	plocha	k1gFnPc2	plocha
listnatých	listnatý	k2eAgInPc2d1	listnatý
lesů	les	k1gInPc2	les
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
horský	horský	k2eAgInSc1d1	horský
deštný	deštný	k2eAgInSc1d1	deštný
les	les	k1gInSc1	les
má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
biologickou	biologický	k2eAgFnSc4d1	biologická
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
uzavřeným	uzavřený	k2eAgInSc7d1	uzavřený
listnatým	listnatý	k2eAgInSc7d1	listnatý
lesem	les	k1gInSc7	les
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
les	les	k1gInSc1	les
upravený	upravený	k2eAgInSc1d1	upravený
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Blue	Blu	k1gFnSc2	Blu
a	a	k8xC	a
John	John	k1gMnSc1	John
Crow	Crow	k1gFnSc4	Crow
Mountains	Mountainsa	k1gFnPc2	Mountainsa
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
poslední	poslední	k2eAgFnPc1d1	poslední
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
známých	známý	k2eAgFnPc2d1	známá
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
motýl	motýl	k1gMnSc1	motýl
otakárek	otakárek	k1gMnSc1	otakárek
jamajský	jamajský	k2eAgMnSc1d1	jamajský
(	(	kIx(	(
<g/>
Papilio	Papilio	k1gMnSc1	Papilio
homerus	homerus	k1gMnSc1	homerus
<g/>
)	)	kIx)	)
-	-	kIx~	-
největší	veliký	k2eAgMnSc1d3	veliký
motýl	motýl	k1gMnSc1	motýl
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
důležitou	důležitý	k2eAgFnSc7d1	důležitá
lokalitou	lokalita	k1gFnSc7	lokalita
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
jamajských	jamajský	k2eAgMnPc2d1	jamajský
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgInPc2	všecek
endemických	endemický	k2eAgInPc2d1	endemický
druhů	druh	k1gInPc2	druh
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
jamajský	jamajský	k2eAgMnSc1d1	jamajský
černý	černý	k1gMnSc1	černý
pták	pták	k1gMnSc1	pták
(	(	kIx(	(
<g/>
Neospar	Neospar	k1gMnSc1	Neospar
nigerrimus	nigerrimus	k1gMnSc1	nigerrimus
<g/>
)	)	kIx)	)
a	a	k8xC	a
zimní	zimní	k2eAgFnSc7d1	zimní
lokalitou	lokalita	k1gFnSc7	lokalita
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
migrujících	migrující	k2eAgMnPc2d1	migrující
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
útočiště	útočiště	k1gNnSc4	útočiště
pro	pro	k7c4	pro
jamajskou	jamajský	k2eAgFnSc4d1	jamajská
divokou	divoký	k2eAgFnSc4d1	divoká
zvěř	zvěř	k1gFnSc4	zvěř
včetně	včetně	k7c2	včetně
jamajského	jamajský	k2eAgMnSc2d1	jamajský
hroznýše	hroznýš	k1gMnSc2	hroznýš
(	(	kIx(	(
<g/>
Epicrates	Epicrates	k1gMnSc1	Epicrates
subflavus	subflavus	k1gMnSc1	subflavus
<g/>
)	)	kIx)	)
a	a	k8xC	a
jamajského	jamajský	k2eAgMnSc2d1	jamajský
zajíce	zajíc	k1gMnSc2	zajíc
(	(	kIx(	(
<g/>
Geocapromys	Geocapromys	k1gInSc4	Geocapromys
brownii	brownie	k1gFnSc4	brownie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
domov	domov	k1gInSc4	domov
pro	pro	k7c4	pro
nespočet	nespočet	k1gInSc4	nespočet
<g />
.	.	kIx.	.
</s>
<s>
endemických	endemický	k2eAgFnPc2d1	endemická
orchidejí	orchidea	k1gFnPc2	orchidea
<g/>
,	,	kIx,	,
bromelií	bromelie	k1gFnPc2	bromelie
<g/>
,	,	kIx,	,
kapradin	kapradina	k1gFnPc2	kapradina
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
mnoha	mnoho	k4c2	mnoho
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
ohrožených	ohrožený	k2eAgFnPc2d1	ohrožená
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
důležitý	důležitý	k2eAgInSc4d1	důležitý
pro	pro	k7c4	pro
uchování	uchování	k1gNnSc4	uchování
vysoce	vysoce	k6eAd1	vysoce
erodující	erodující	k2eAgFnSc2d1	erodující
půdy	půda	k1gFnSc2	půda
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
-	-	kIx~	-
předcházení	předcházení	k1gNnSc1	předcházení
eroze	eroze	k1gFnSc2	eroze
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
lavinám	lavina	k1gFnPc3	lavina
<g/>
,	,	kIx,	,
zásadní	zásadní	k2eAgFnSc7d1	zásadní
pro	pro	k7c4	pro
poskytování	poskytování	k1gNnSc4	poskytování
vody	voda	k1gFnSc2	voda
-	-	kIx~	-
park	park	k1gInSc1	park
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
přes	přes	k7c4	přes
40	[number]	k4	40
%	%	kIx~	%
jamajské	jamajský	k2eAgFnSc2d1	jamajská
populace	populace	k1gFnSc2	populace
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
sociálně-kulturních	sociálněulturní	k2eAgFnPc2d1	sociálně-kulturní
tradic	tradice	k1gFnPc2	tradice
maroonského	maroonský	k2eAgInSc2d1	maroonský
kmene	kmen	k1gInSc2	kmen
a	a	k8xC	a
venkovských	venkovský	k2eAgFnPc2d1	venkovská
jamajských	jamajský	k2eAgFnPc2d1	jamajská
komunit	komunita	k1gFnPc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Jamajky	Jamajka	k1gFnSc2	Jamajka
<g/>
.	.	kIx.	.
</s>
<s>
Jamajka	Jamajka	k1gFnSc1	Jamajka
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
kraje	kraj	k1gInPc4	kraj
<g/>
,	,	kIx,	,
Cornwall	Cornwall	k1gInSc4	Cornwall
<g/>
,	,	kIx,	,
Middlesex	Middlesex	k1gInSc4	Middlesex
a	a	k8xC	a
Surrey	Surrea	k1gFnPc4	Surrea
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
kraje	kraj	k1gInPc1	kraj
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
na	na	k7c4	na
okrsky	okrsek	k1gInPc4	okrsek
<g/>
.	.	kIx.	.
</s>
<s>
Kingston	Kingston	k1gInSc1	Kingston
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgMnSc1d3	nejmenší
ze	z	k7c2	z
14	[number]	k4	14
okrsků	okrsek	k1gInPc2	okrsek
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
přístav	přístav	k1gInSc4	přístav
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
"	"	kIx"	"
<g/>
Palisadoes	Palisadoes	k1gInSc1	Palisadoes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
13	[number]	k4	13
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
písečná	písečný	k2eAgFnSc1d1	písečná
kosa	kosa	k1gFnSc1	kosa
<g/>
,	,	kIx,	,
vybíhající	vybíhající	k2eAgFnSc1d1	vybíhající
z	z	k7c2	z
pobřeží	pobřeží	k1gNnSc2	pobřeží
východně	východně	k6eAd1	východně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
této	tento	k3xDgFnSc2	tento
kosy	kosa	k1gFnSc2	kosa
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
hlavní	hlavní	k2eAgNnSc1d1	hlavní
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
a	a	k8xC	a
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
západním	západní	k2eAgInSc6d1	západní
konci	konec	k1gInSc6	konec
stálo	stát	k5eAaImAgNnS	stát
město	město	k1gNnSc4	město
Port	porta	k1gFnPc2	porta
Royal	Royal	k1gMnSc1	Royal
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Port	port	k1gInSc1	port
Royal	Royal	k1gInSc4	Royal
zničilo	zničit	k5eAaPmAgNnS	zničit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1692	[number]	k4	1692
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
,	,	kIx,	,
založili	založit	k5eAaPmAgMnP	založit
Britové	Brit	k1gMnPc1	Brit
Kingston	Kingston	k1gInSc4	Kingston
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
obchodním	obchodní	k2eAgInSc7d1	obchodní
centrem	centr	k1gInSc7	centr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
opět	opět	k6eAd1	opět
zničilo	zničit	k5eAaPmAgNnS	zničit
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
a	a	k8xC	a
následný	následný	k2eAgInSc1d1	následný
požár	požár	k1gInSc1	požár
mnoho	mnoho	k6eAd1	mnoho
městských	městský	k2eAgFnPc2d1	městská
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
moderními	moderní	k2eAgFnPc7d1	moderní
budovami	budova	k1gFnPc7	budova
se	se	k3xPyFc4	se
přesto	přesto	k6eAd1	přesto
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
několik	několik	k4yIc1	několik
architektonických	architektonický	k2eAgFnPc2d1	architektonická
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
leží	ležet	k5eAaImIp3nS	ležet
pevnost	pevnost	k1gFnSc1	pevnost
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
obehnaná	obehnaný	k2eAgFnSc1d1	obehnaná
příkopem	příkop	k1gInSc7	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Kingston	Kingston	k1gInSc1	Kingston
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
ostrova	ostrov	k1gInSc2	ostrov
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
°	°	k?	°
<g/>
59	[number]	k4	59
<g/>
'	'	kIx"	'
<g/>
N	N	kA	N
76	[number]	k4	76
<g/>
°	°	k?	°
<g/>
48	[number]	k4	48
<g/>
'	'	kIx"	'
<g/>
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
žije	žít	k5eAaImIp3nS	žít
okolo	okolo	k7c2	okolo
2700000	[number]	k4	2700000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
je	být	k5eAaImIp3nS	být
249	[number]	k4	249
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km	km	kA	km
čtvereční	čtvereční	k2eAgFnPc4d1	čtvereční
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Jamajky	Jamajka	k1gFnSc2	Jamajka
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
černoši	černoch	k1gMnPc1	černoch
a	a	k8xC	a
mulati	mulat	k1gMnPc1	mulat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
malé	malý	k2eAgFnPc1d1	malá
menšiny	menšina	k1gFnPc1	menšina
Indů	Ind	k1gMnPc2	Ind
<g/>
,	,	kIx,	,
Číňanů	Číňan	k1gMnPc2	Číňan
<g/>
,	,	kIx,	,
Libanonců	Libanonec	k1gMnPc2	Libanonec
a	a	k8xC	a
bělochů	běloch	k1gMnPc2	běloch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
nemělo	mít	k5eNaImAgNnS	mít
doma	doma	k6eAd1	doma
ani	ani	k8xC	ani
kohoutek	kohoutek	k1gInSc1	kohoutek
s	s	k7c7	s
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
23,7	[number]	k4	23,7
%	%	kIx~	%
se	se	k3xPyFc4	se
muselo	muset	k5eAaImAgNnS	muset
dělit	dělit	k5eAaImF	dělit
o	o	k7c4	o
záchod	záchod	k1gInSc4	záchod
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
rodinami	rodina	k1gFnPc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
na	na	k7c4	na
35	[number]	k4	35
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
městských	městský	k2eAgFnPc6d1	městská
částech	část	k1gFnPc6	část
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
57	[number]	k4	57
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
komunitu	komunita	k1gFnSc4	komunita
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
tvoří	tvořit	k5eAaImIp3nP	tvořit
Maroonové	Maroon	k1gMnPc1	Maroon
-	-	kIx~	-
potomci	potomek	k1gMnPc1	potomek
uprchlých	uprchlá	k1gFnPc2	uprchlá
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
v	v	k7c6	v
hornatém	hornatý	k2eAgNnSc6d1	hornaté
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
ostrova	ostrov	k1gInSc2	ostrov
udrželi	udržet	k5eAaPmAgMnP	udržet
až	až	k9	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
faktickou	faktický	k2eAgFnSc4d1	faktická
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Maroonové	Maroon	k1gMnPc1	Maroon
udrželi	udržet	k5eAaPmAgMnP	udržet
svou	svůj	k3xOyFgFnSc4	svůj
svrchovanou	svrchovaný	k2eAgFnSc4d1	svrchovaná
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
tradice	tradice	k1gFnPc4	tradice
včetně	včetně	k7c2	včetně
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
tance	tanec	k1gInSc2	tanec
<g/>
,	,	kIx,	,
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
náboženských	náboženský	k2eAgInPc2d1	náboženský
rituálů	rituál	k1gInPc2	rituál
a	a	k8xC	a
znalostí	znalost	k1gFnPc2	znalost
léčivých	léčivý	k2eAgFnPc2d1	léčivá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
je	on	k3xPp3gNnSc4	on
nemohli	moct	k5eNaImAgMnP	moct
porazit	porazit	k5eAaPmF	porazit
<g/>
,	,	kIx,	,
s	s	k7c7	s
Maroony	Maroon	k1gMnPc7	Maroon
podepsali	podepsat	k5eAaPmAgMnP	podepsat
roku	rok	k1gInSc2	rok
1739	[number]	k4	1739
mírovou	mírový	k2eAgFnSc4d1	mírová
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
uznala	uznat	k5eAaPmAgFnS	uznat
nezávislost	nezávislost	k1gFnSc4	nezávislost
Maroonů	Maroon	k1gInPc2	Maroon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
zakazovala	zakazovat	k5eAaImAgFnS	zakazovat
další	další	k2eAgNnSc4d1	další
přijímání	přijímání	k1gNnSc4	přijímání
uprchlých	uprchlý	k2eAgMnPc2d1	uprchlý
otroků	otrok	k1gMnPc2	otrok
do	do	k7c2	do
maroonských	maroonský	k2eAgFnPc2d1	maroonský
komunit	komunita	k1gFnPc2	komunita
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
dohodnuta	dohodnut	k2eAgFnSc1d1	dohodnuta
povinnost	povinnost	k1gFnSc1	povinnost
Maroonů	Maroon	k1gMnPc2	Maroon
vracet	vracet	k5eAaImF	vracet
otroky	otrok	k1gMnPc7	otrok
jejich	jejich	k3xOp3gMnPc3	jejich
britským	britský	k2eAgMnPc3d1	britský
pánům	pan	k1gMnPc3	pan
<g/>
.	.	kIx.	.
</s>
<s>
UNESCO	Unesco	k1gNnSc1	Unesco
uznalo	uznat	k5eAaPmAgNnS	uznat
"	"	kIx"	"
<g/>
dědictví	dědictví	k1gNnSc1	dědictví
Maroonů	Maroon	k1gInPc2	Maroon
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Moore	Moor	k1gInSc5	Moor
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Mistrovské	mistrovský	k2eAgNnSc1d1	mistrovské
dílo	dílo	k1gNnSc1	dílo
ústního	ústní	k2eAgNnSc2d1	ústní
a	a	k8xC	a
nehmotného	hmotný	k2eNgNnSc2d1	nehmotné
dědictví	dědictví	k1gNnSc2	dědictví
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
"	"	kIx"	"
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
Maroonským	Maroonský	k2eAgInSc7d1	Maroonský
koncilem	koncil	k1gInSc7	koncil
města	město	k1gNnSc2	město
Moore	Moor	k1gInSc5	Moor
na	na	k7c4	na
uchování	uchování	k1gNnSc4	uchování
tohoto	tento	k3xDgNnSc2	tento
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
udržování	udržování	k1gNnSc1	udržování
kulturních	kulturní	k2eAgInPc2d1	kulturní
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
Blue	Blue	k1gFnSc6	Blue
a	a	k8xC	a
John	John	k1gMnSc1	John
Crow	Crow	k1gFnSc2	Crow
Mountains	Mountains	k1gInSc1	Mountains
National	National	k1gMnSc1	National
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
událostem	událost	k1gFnPc3	událost
a	a	k8xC	a
živoucím	živoucí	k2eAgFnPc3d1	živoucí
tradicím	tradice	k1gFnPc3	tradice
kmenu	kmen	k1gInSc2	kmen
Windward	Windward	k1gMnSc1	Windward
Maroon	Maroon	k1gMnSc1	Maroon
<g/>
.	.	kIx.	.
</s>
<s>
Britské	britský	k2eAgInPc1d1	britský
dokumenty	dokument	k1gInPc1	dokument
a	a	k8xC	a
mapy	mapa	k1gFnPc1	mapa
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
těchto	tento	k3xDgMnPc2	tento
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc2	jejich
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc2	jejich
přínosu	přínos	k1gInSc2	přínos
jamajskému	jamajský	k2eAgNnSc3d1	jamajské
dědictví	dědictví	k1gNnSc3	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Dokládají	dokládat	k5eAaImIp3nP	dokládat
historii	historie	k1gFnSc4	historie
partyzánských	partyzánský	k2eAgFnPc2d1	Partyzánská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
Maroon	Maroon	k1gInSc4	Maroon
vedli	vést	k5eAaImAgMnP	vést
s	s	k7c7	s
britským	britský	k2eAgNnSc7d1	Britské
vojskem	vojsko	k1gNnSc7	vojsko
v	v	k7c6	v
BJCMNP	BJCMNP	kA	BJCMNP
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
mírovou	mírový	k2eAgFnSc4d1	mírová
dohodu	dohoda	k1gFnSc4	dohoda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1739	[number]	k4	1739
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Maroonům	Maroon	k1gMnPc3	Maroon
garantovala	garantovat	k5eAaBmAgFnS	garantovat
zem	zem	k1gFnSc1	zem
ne	ne	k9	ne
tak	tak	k6eAd1	tak
hluboko	hluboko	k6eAd1	hluboko
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
Maroonů	Maroon	k1gMnPc2	Maroon
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
udržování	udržování	k1gNnSc1	udržování
afrických	africký	k2eAgFnPc2d1	africká
tradic	tradice	k1gFnPc2	tradice
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c4	v
silnou	silný	k2eAgFnSc4d1	silná
afro-jamajskou	afroamajský	k2eAgFnSc4d1	afro-jamajský
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
především	především	k9	především
u	u	k7c2	u
skrytých	skrytý	k2eAgFnPc2d1	skrytá
<g/>
,	,	kIx,	,
venkovských	venkovský	k2eAgFnPc2d1	venkovská
komunit	komunita	k1gFnPc2	komunita
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Národního	národní	k2eAgInSc2d1	národní
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
na	na	k7c6	na
živoucích	živoucí	k2eAgFnPc6d1	živoucí
tradicích	tradice	k1gFnPc6	tradice
místních	místní	k2eAgFnPc2d1	místní
komunit	komunita	k1gFnPc2	komunita
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
náboženských	náboženský	k2eAgFnPc6d1	náboženská
a	a	k8xC	a
duchovních	duchovní	k2eAgFnPc6d1	duchovní
vírách	víra	k1gFnPc6	víra
<g/>
,	,	kIx,	,
tanci	tanec	k1gInSc6	tanec
<g/>
,	,	kIx,	,
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
pokrmu	pokrm	k1gInSc6	pokrm
<g/>
,	,	kIx,	,
příbězích	příběh	k1gInPc6	příběh
<g/>
,	,	kIx,	,
hospodářství	hospodářství	k1gNnSc6	hospodářství
i	i	k8xC	i
jiných	jiný	k2eAgInPc6d1	jiný
zvycích	zvyk	k1gInPc6	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Jamajka	Jamajka	k1gFnSc1	Jamajka
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
hudebních	hudební	k2eAgInPc2d1	hudební
stylů	styl	k1gInPc2	styl
Reggae	Regga	k1gFnSc2	Regga
a	a	k8xC	a
Ska	Ska	k1gFnSc2	Ska
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
reggae	reggae	k1gFnPc2	reggae
je	být	k5eAaImIp3nS	být
nejvýraznější	výrazný	k2eAgFnSc7d3	nejvýraznější
postavou	postava	k1gFnSc7	postava
Bob	Bob	k1gMnSc1	Bob
Marley	Marlea	k1gFnSc2	Marlea
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
právem	právem	k6eAd1	právem
označovaný	označovaný	k2eAgInSc1d1	označovaný
za	za	k7c4	za
učitele	učitel	k1gMnSc4	učitel
proroka	prorok	k1gMnSc4	prorok
-	-	kIx~	-
nejen	nejen	k6eAd1	nejen
Jamajčany	Jamajčan	k1gMnPc7	Jamajčan
<g/>
)	)	kIx)	)
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
kapelou	kapela	k1gFnSc7	kapela
The	The	k1gFnPc2	The
Wailers	Wailersa	k1gFnPc2	Wailersa
<g/>
,	,	kIx,	,
za	za	k7c4	za
ska	ska	k?	ska
pak	pak	k6eAd1	pak
jmenujme	jmenovat	k5eAaImRp1nP	jmenovat
např.	např.	kA	např.
hudební	hudební	k2eAgFnSc4d1	hudební
skupinu	skupina	k1gFnSc4	skupina
Skatalites	Skatalitesa	k1gFnPc2	Skatalitesa
<g/>
.	.	kIx.	.
</s>
<s>
Absolutně	absolutně	k6eAd1	absolutně
nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
náboženstvím	náboženství	k1gNnSc7	náboženství
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
je	být	k5eAaImIp3nS	být
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejznámější	známý	k2eAgNnSc1d3	nejznámější
náboženství	náboženství	k1gNnSc1	náboženství
spojované	spojovaný	k2eAgNnSc1d1	spojované
s	s	k7c7	s
Jamajkou	Jamajka	k1gFnSc7	Jamajka
je	být	k5eAaImIp3nS	být
rastafariánství	rastafariánství	k1gNnSc1	rastafariánství
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
např.	např.	kA	např.
vegetariánstvím	vegetariánství	k1gNnSc7	vegetariánství
<g/>
,	,	kIx,	,
odporem	odpor	k1gInSc7	odpor
k	k	k7c3	k
"	"	kIx"	"
<g/>
pohanské	pohanský	k2eAgFnSc6d1	pohanská
<g/>
"	"	kIx"	"
medicíně	medicína	k1gFnSc6	medicína
a	a	k8xC	a
nošením	nošení	k1gNnSc7	nošení
dredů	dred	k1gInPc2	dred
a	a	k8xC	a
pletených	pletený	k2eAgInPc2d1	pletený
rasta	rast	k1gMnSc4	rast
čepic	čepice	k1gFnPc2	čepice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Jamajkou	Jamajka	k1gFnSc7	Jamajka
se	se	k3xPyFc4	se
také	také	k9	také
setkáte	setkat	k5eAaPmIp2nP	setkat
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
barvami	barva	k1gFnPc7	barva
<g/>
,	,	kIx,	,
než	než	k8xS	než
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
,	,	kIx,	,
rastafariáni	rastafarián	k1gMnPc1	rastafarián
totiž	totiž	k9	totiž
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c7	za
svojí	svůj	k3xOyFgFnSc7	svůj
svatou	svatý	k2eAgFnSc7d1	svatá
zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
Zion	Zion	k1gInSc4	Zion
<g/>
)	)	kIx)	)
kromě	kromě	k7c2	kromě
Jamajky	Jamajka	k1gFnSc2	Jamajka
také	také	k9	také
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
,	,	kIx,	,
do	do	k7c2	do
"	"	kIx"	"
<g/>
rasta	rasto	k1gNnSc2	rasto
<g/>
"	"	kIx"	"
barev	barva	k1gFnPc2	barva
tedy	tedy	k8xC	tedy
patří	patřit	k5eAaImIp3nS	patřit
krom	krom	k7c2	krom
černé	černá	k1gFnSc2	černá
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc4d1	zelená
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
žluté	žlutý	k2eAgFnPc1d1	žlutá
také	také	k9	také
červená	červená	k1gFnSc1	červená
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
-	-	kIx~	-
vlajka	vlajka	k1gFnSc1	vlajka
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
)	)	kIx)	)
Na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
roste	růst	k5eAaImIp3nS	růst
marihuana	marihuana	k1gFnSc1	marihuana
jako	jako	k8xS	jako
téměř	téměř	k6eAd1	téměř
plevelná	plevelný	k2eAgFnSc1d1	plevelná
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
Jamajčané	Jamajčan	k1gMnPc1	Jamajčan
jí	on	k3xPp3gFnSc7	on
tedy	tedy	k8xC	tedy
používají	používat	k5eAaImIp3nP	používat
úplně	úplně	k6eAd1	úplně
ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
<g/>
,	,	kIx,	,
vaří	vařit	k5eAaImIp3nS	vařit
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
(	(	kIx(	(
<g/>
z	z	k7c2	z
částí	část	k1gFnPc2	část
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
méně	málo	k6eAd2	málo
THC	THC	kA	THC
-	-	kIx~	-
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dělají	dělat	k5eAaImIp3nP	dělat
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
lana	lano	k1gNnSc2	lano
<g/>
,	,	kIx,	,
odolné	odolný	k2eAgNnSc1d1	odolné
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
kouření	kouření	k1gNnSc1	kouření
marihuany	marihuana	k1gFnSc2	marihuana
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
Jamajčané	Jamajčan	k1gMnPc1	Jamajčan
jí	on	k3xPp3gFnSc3	on
kouří	kouřit	k5eAaImIp3nP	kouřit
hojně	hojně	k6eAd1	hojně
jako	jako	k8xC	jako
národní	národní	k2eAgInSc1d1	národní
sport	sport	k1gInSc1	sport
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
Tunisku	Tunisko	k1gNnSc6	Tunisko
se	se	k3xPyFc4	se
kouří	kouřit	k5eAaImIp3nS	kouřit
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
a	a	k8xC	a
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
pijí	pít	k5eAaImIp3nP	pít
kávu	káva	k1gFnSc4	káva
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
konopí	konopí	k1gNnSc4	konopí
uchytilo	uchytit	k5eAaPmAgNnS	uchytit
mnoho	mnoho	k4c1	mnoho
výrazů	výraz	k1gInPc2	výraz
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kaya	kaya	k1gFnSc1	kaya
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgNnPc4	dva
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
letiště	letiště	k1gNnPc4	letiště
<g/>
:	:	kIx,	:
Sangster	Sangster	k1gMnSc1	Sangster
International	International	k1gMnSc3	International
Airport	Airport	k1gInSc4	Airport
v	v	k7c4	v
Montego	Montego	k1gNnSc4	Montego
Bay	Bay	k1gFnSc2	Bay
a	a	k8xC	a
Norman	Norman	k1gMnSc1	Norman
Manley	Manlea	k1gFnSc2	Manlea
International	International	k1gMnSc1	International
Airport	Airport	k1gInSc1	Airport
v	v	k7c6	v
Kingstonu	Kingston	k1gInSc6	Kingston
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
Sangster	Sangstra	k1gFnPc2	Sangstra
International	International	k1gFnSc2	International
je	být	k5eAaImIp3nS	být
primárním	primární	k2eAgInSc7d1	primární
bodem	bod	k1gInSc7	bod
vstupu	vstup	k1gInSc2	vstup
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Jamajka	Jamajka	k1gFnSc1	Jamajka
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
vzdálena	vzdáleno	k1gNnSc2	vzdáleno
8670	[number]	k4	8670
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jamajka	Jamajka	k1gFnSc1	Jamajka
má	mít	k5eAaImIp3nS	mít
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
počet	počet	k1gInSc4	počet
vražd	vražda	k1gFnPc2	vražda
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
1500	[number]	k4	1500
lidí	člověk	k1gMnPc2	člověk
obětí	oběť	k1gFnPc2	oběť
kriminální	kriminální	k2eAgFnSc2d1	kriminální
vraždy	vražda	k1gFnSc2	vražda
a	a	k8xC	a
272	[number]	k4	272
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
policií	policie	k1gFnSc7	policie
-	-	kIx~	-
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
15	[number]	k4	15
%	%	kIx~	%
zabití	zabití	k1gNnSc1	zabití
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
1355	[number]	k4	1355
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
229	[number]	k4	229
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
1674	[number]	k4	1674
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
202	[number]	k4	202
zabito	zabít	k5eAaPmNgNnS	zabít
policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
50	[number]	k4	50
%	%	kIx~	%
vražd	vražda	k1gFnPc2	vražda
spácháno	spáchat	k5eAaPmNgNnS	spáchat
střelnou	střelný	k2eAgFnSc7d1	střelná
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
narostlo	narůst	k5eAaPmAgNnS	narůst
na	na	k7c4	na
61	[number]	k4	61
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
a	a	k8xC	a
okolo	okolo	k7c2	okolo
75	[number]	k4	75
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
oběťmi	oběť	k1gFnPc7	oběť
vražd	vražda	k1gFnPc2	vražda
byli	být	k5eAaImAgMnP	být
lidé	člověk	k1gMnPc1	člověk
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
extrémně	extrémně	k6eAd1	extrémně
chudých	chudý	k2eAgFnPc6d1	chudá
podmínkách	podmínka	k1gFnPc6	podmínka
v	v	k7c6	v
přelidněných	přelidněný	k2eAgInPc6d1	přelidněný
centrech	centr	k1gInPc6	centr
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
30	[number]	k4	30
%	%	kIx~	%
a	a	k8xC	a
45	[number]	k4	45
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
Kingstonu	Kingston	k1gInSc2	Kingston
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
komunitách	komunita	k1gFnPc6	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
vyšetřovaných	vyšetřovaný	k2eAgFnPc2d1	vyšetřovaná
a	a	k8xC	a
vyřešených	vyřešený	k2eAgFnPc2d1	vyřešená
vražd	vražda	k1gFnPc2	vražda
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
žalob	žaloba	k1gFnPc2	žaloba
a	a	k8xC	a
odsouzení	odsouzení	k1gNnSc1	odsouzení
je	být	k5eAaImIp3nS	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
chabá	chabý	k2eAgFnSc1d1	chabá
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgNnPc2d1	oficiální
dat	datum	k1gNnPc2	datum
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
vyšetřováno	vyšetřovat	k5eAaImNgNnS	vyšetřovat
a	a	k8xC	a
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
36,5	[number]	k4	36,5
<g/>
%	%	kIx~	%
zaznamenaných	zaznamenaný	k2eAgFnPc2d1	zaznamenaná
vražd	vražda	k1gFnPc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
drogové	drogový	k2eAgFnSc2d1	drogová
vraždy	vražda	k1gFnSc2	vražda
spadla	spadnout	k5eAaPmAgFnS	spadnout
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
vyšetřování	vyšetřování	k1gNnSc3	vyšetřování
na	na	k7c4	na
šokující	šokující	k2eAgFnPc4d1	šokující
0	[number]	k4	0
%	%	kIx~	%
a	a	k8xC	a
pro	pro	k7c4	pro
vraždy	vražda	k1gFnPc4	vražda
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
gangem	gang	k1gInSc7	gang
byla	být	k5eAaImAgFnS	být
objasněnost	objasněnost	k1gFnSc1	objasněnost
trestné	trestný	k2eAgFnSc2d1	trestná
činnosti	činnost	k1gFnSc2	činnost
27,2	[number]	k4	27,2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
provází	provázet	k5eAaImIp3nS	provázet
zničení	zničení	k1gNnSc4	zničení
důkazů	důkaz	k1gInPc2	důkaz
zločinu	zločin	k1gInSc2	zločin
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc1	nedostatek
zdrojů	zdroj	k1gInPc2	zdroj
(	(	kIx(	(
<g/>
forenzní	forenzní	k2eAgFnPc4d1	forenzní
a	a	k8xC	a
balistické	balistický	k2eAgFnPc4d1	balistická
expertizy	expertiza	k1gFnPc4	expertiza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neprůhlednost	neprůhlednost	k1gFnSc1	neprůhlednost
<g/>
,	,	kIx,	,
neadekvátní	adekvátní	k2eNgFnSc1d1	neadekvátní
síla	síla	k1gFnSc1	síla
při	při	k7c6	při
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
<g/>
,	,	kIx,	,
zpoždění	zpoždění	k1gNnSc4	zpoždění
a	a	k8xC	a
neúčinnost	neúčinnost	k1gFnSc4	neúčinnost
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
<g/>
.	.	kIx.	.
</s>
<s>
Metodistický	metodistický	k2eAgInSc1d1	metodistický
kostel	kostel	k1gInSc1	kostel
Kostel	kostel	k1gInSc1	kostel
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
Nazaretského	nazaretský	k2eAgMnSc2d1	nazaretský
Kostel	kostel	k1gInSc1	kostel
Prvního	první	k4xOgNnSc2	první
početí	početí	k1gNnSc2	početí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Islám	islám	k1gInSc1	islám
a	a	k8xC	a
židovství	židovství	k1gNnSc1	židovství
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rastafariánství	Rastafariánství	k1gNnSc2	Rastafariánství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
je	být	k5eAaImIp3nS	být
nápadně	nápadně	k6eAd1	nápadně
přítomen	přítomen	k2eAgInSc1d1	přítomen
vliv	vliv	k1gInSc1	vliv
afrického	africký	k2eAgNnSc2d1	africké
dědictví	dědictví	k1gNnSc2	dědictví
v	v	k7c6	v
sektách	sekta	k1gFnPc6	sekta
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
oblékají	oblékat	k5eAaImIp3nP	oblékat
do	do	k7c2	do
bílých	bílý	k2eAgInPc2d1	bílý
hávů	háv	k1gInPc2	háv
<g/>
,	,	kIx,	,
hlavy	hlava	k1gFnSc2	hlava
mají	mít	k5eAaImIp3nP	mít
zabalené	zabalený	k2eAgInPc1d1	zabalený
do	do	k7c2	do
modrých	modrý	k2eAgFnPc2d1	modrá
<g/>
,	,	kIx,	,
červených	červený	k2eAgFnPc2d1	červená
<g/>
,	,	kIx,	,
bílých	bílý	k2eAgFnPc2d1	bílá
nebo	nebo	k8xC	nebo
zelených	zelený	k2eAgFnPc2d1	zelená
šál	šála	k1gFnPc2	šála
a	a	k8xC	a
zpívají	zpívat	k5eAaImIp3nP	zpívat
a	a	k8xC	a
tančí	tančit	k5eAaImIp3nP	tančit
do	do	k7c2	do
rytmu	rytmus	k1gInSc2	rytmus
bubnů	buben	k1gInPc2	buben
a	a	k8xC	a
tamburín	tamburína	k1gFnPc2	tamburína
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
skoro	skoro	k6eAd1	skoro
hypnotická	hypnotický	k2eAgFnSc1d1	hypnotická
<g/>
,	,	kIx,	,
nutí	nutit	k5eAaImIp3nS	nutit
boky	boka	k1gFnPc4	boka
pohybovat	pohybovat	k5eAaImF	pohybovat
se	se	k3xPyFc4	se
v	v	k7c6	v
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
rytmu	rytmus	k1gInSc6	rytmus
<g/>
.	.	kIx.	.
</s>
<s>
Minulost	minulost	k1gFnSc1	minulost
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
příběhy	příběh	k1gInPc4	příběh
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc1d1	kulturní
pestrost	pestrost	k1gFnSc1	pestrost
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
vzestupu	vzestup	k1gInSc3	vzestup
náboženských	náboženský	k2eAgFnPc2d1	náboženská
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	on	k3xPp3gFnPc4	on
Rastafari	Rastafar	k1gFnPc4	Rastafar
a	a	k8xC	a
Pocomania	Pocomanium	k1gNnPc4	Pocomanium
<g/>
,	,	kIx,	,
široce	široko	k6eAd1	široko
vnímaná	vnímaný	k2eAgFnSc1d1	vnímaná
v	v	k7c6	v
tradičních	tradiční	k2eAgInPc6d1	tradiční
náboženských	náboženský	k2eAgInPc6d1	náboženský
kruzích	kruh	k1gInPc6	kruh
jako	jako	k8xC	jako
prostředek	prostředek	k1gInSc4	prostředek
rebelie	rebelie	k1gFnSc2	rebelie
za	za	k7c2	za
koloniálních	koloniální	k2eAgInPc2d1	koloniální
časů	čas	k1gInPc2	čas
a	a	k8xC	a
proti	proti	k7c3	proti
současnému	současný	k2eAgInSc3d1	současný
stavu	stav	k1gInSc3	stav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nákladných	nákladný	k2eAgFnPc6d1	nákladná
oslavách	oslava	k1gFnPc6	oslava
a	a	k8xC	a
hostinách	hostina	k1gFnPc6	hostina
se	se	k3xPyFc4	se
pokaždé	pokaždé	k6eAd1	pokaždé
stoly	stol	k1gInPc7	stol
nebo	nebo	k8xC	nebo
oltáře	oltář	k1gInPc1	oltář
prohýbají	prohýbat	k5eAaImIp3nP	prohýbat
pod	pod	k7c7	pod
ovocem	ovoce	k1gNnSc7	ovoce
<g/>
,	,	kIx,	,
sladkým	sladký	k2eAgInSc7d1	sladký
chlebem	chléb	k1gInSc7	chléb
<g/>
,	,	kIx,	,
potravinami	potravina	k1gFnPc7	potravina
a	a	k8xC	a
barevnými	barevný	k2eAgFnPc7d1	barevná
svíčkami	svíčka	k1gFnPc7	svíčka
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
k	k	k7c3	k
díkůvzdání	díkůvzdání	k1gNnSc3	díkůvzdání
nebo	nebo	k8xC	nebo
k	k	k7c3	k
prosbě	prosba	k1gFnSc3	prosba
o	o	k7c4	o
požehnání	požehnání	k1gNnSc4	požehnání
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
uzdravení	uzdravení	k1gNnSc2	uzdravení
nebo	nebo	k8xC	nebo
dodání	dodání	k1gNnSc2	dodání
síly	síla	k1gFnSc2	síla
k	k	k7c3	k
množení	množení	k1gNnSc3	množení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
ostrově	ostrov	k1gInSc6	ostrov
jsou	být	k5eAaImIp3nP	být
komunity	komunita	k1gFnPc1	komunita
rastafariánů	rastafarián	k1gMnPc2	rastafarián
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
bratři	bratr	k1gMnPc1	bratr
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
identifikováni	identifikován	k2eAgMnPc1d1	identifikován
díky	díky	k7c3	díky
dredům	dred	k1gInPc3	dred
na	na	k7c6	na
hlavách	hlava	k1gFnPc6	hlava
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc3	jejich
volným	volný	k2eAgInPc3d1	volný
slavnostním	slavnostní	k2eAgInPc3d1	slavnostní
hávům	háv	k1gInPc3	háv
<g/>
,	,	kIx,	,
turbanům	turban	k1gInPc3	turban
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
typickým	typický	k2eAgFnPc3d1	typická
pleteným	pletený	k2eAgFnPc3d1	pletená
čepicím	čepice	k1gFnPc3	čepice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Jamajkou	Jamajka	k1gFnSc7	Jamajka
se	se	k3xPyFc4	se
také	také	k9	také
setkáte	setkat	k5eAaPmIp2nP	setkat
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
barvami	barva	k1gFnPc7	barva
<g/>
,	,	kIx,	,
než	než	k8xS	než
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Rastafariáni	Rastafarián	k1gMnPc1	Rastafarián
totiž	totiž	k9	totiž
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c7	za
svojí	svůj	k3xOyFgFnSc7	svůj
svatou	svatý	k2eAgFnSc7d1	svatá
zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
Zion	Zion	k1gInSc4	Zion
<g/>
)	)	kIx)	)
kromě	kromě	k7c2	kromě
Jamajky	Jamajka	k1gFnSc2	Jamajka
také	také	k9	také
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
rasta	rast	k1gInSc2	rast
barev	barva	k1gFnPc2	barva
tedy	tedy	k9	tedy
patří	patřit	k5eAaImIp3nS	patřit
krom	krom	k7c2	krom
černé	černá	k1gFnSc2	černá
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
a	a	k8xC	a
žluté	žlutý	k2eAgFnPc1d1	žlutá
také	také	k9	také
červená	červená	k1gFnSc1	červená
(	(	kIx(	(
<g/>
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
-	-	kIx~	-
vlajka	vlajka	k1gFnSc1	vlajka
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
vnějším	vnější	k2eAgNnSc7d1	vnější
vyjádřením	vyjádření	k1gNnSc7	vyjádření
jejich	jejich	k3xOp3gFnSc2	jejich
identity	identita	k1gFnSc2	identita
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
neseny	nést	k5eAaImNgFnP	nést
v	v	k7c6	v
respektu	respekt	k1gInSc6	respekt
k	k	k7c3	k
matce	matka	k1gFnSc3	matka
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rastové	Rast	k1gMnPc1	Rast
bubnují	bubnovat	k5eAaImIp3nP	bubnovat
a	a	k8xC	a
zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
obnovili	obnovit	k5eAaPmAgMnP	obnovit
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
Ras	rasa	k1gFnPc2	rasa
Tafari	Tafari	k1gNnSc1	Tafari
(	(	kIx(	(
<g/>
etiopský	etiopský	k2eAgMnSc1d1	etiopský
vládce	vládce	k1gMnSc1	vládce
Haile	Haile	k1gFnSc2	Haile
Selassie	Selassie	k1gFnSc2	Selassie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Rastafariánství	Rastafariánství	k1gNnSc1	Rastafariánství
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
například	například	k6eAd1	například
vegetariánstvím	vegetariánství	k1gNnSc7	vegetariánství
<g/>
,	,	kIx,	,
odporem	odpor	k1gInSc7	odpor
k	k	k7c3	k
pohanské	pohanský	k2eAgFnSc3d1	pohanská
medicíně	medicína	k1gFnSc3	medicína
a	a	k8xC	a
mírumilovností	mírumilovnost	k1gFnPc2	mírumilovnost
<g/>
.	.	kIx.	.
</s>
<s>
Dredy	Dredy	k6eAd1	Dredy
na	na	k7c6	na
jejich	jejich	k3xOp3gFnPc6	jejich
hlavách	hlava	k1gFnPc6	hlava
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
hřívu	hříva	k1gFnSc4	hříva
Lva	lev	k1gInSc2	lev
z	z	k7c2	z
Judeje	Judea	k1gFnSc2	Judea
(	(	kIx(	(
<g/>
další	další	k2eAgNnSc1d1	další
jméno	jméno	k1gNnSc1	jméno
vládce	vládce	k1gMnSc2	vládce
Haile	Haile	k1gFnSc1	Haile
Selassie	Selassie	k1gFnSc1	Selassie
<g/>
)	)	kIx)	)
a	a	k8xC	a
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
nikdy	nikdy	k6eAd1	nikdy
ostříhány	ostříhán	k2eAgFnPc4d1	ostříhána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
je	být	k5eAaImIp3nS	být
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
atletika	atletika	k1gFnSc1	atletika
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
sprinter	sprinter	k1gMnSc1	sprinter
Usain	Usain	k1gMnSc1	Usain
Bolt	Bolt	k1gMnSc1	Bolt
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
držitel	držitel	k1gMnSc1	držitel
tří	tři	k4xCgMnPc2	tři
světových	světový	k2eAgMnPc2d1	světový
rekordů	rekord	k1gInPc2	rekord
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
běhu	běh	k1gInSc2	běh
na	na	k7c4	na
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
ve	v	k7c6	v
štafetě	štafeta	k1gFnSc6	štafeta
na	na	k7c4	na
4	[number]	k4	4
x	x	k?	x
100	[number]	k4	100
m	m	kA	m
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Jamajský	jamajský	k2eAgInSc4d1	jamajský
bobový	bobový	k2eAgInSc4d1	bobový
tým	tým	k1gInSc4	tým
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
a	a	k8xC	a
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
14	[number]	k4	14
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
před	před	k7c7	před
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
námětem	námět	k1gInSc7	námět
pro	pro	k7c4	pro
komediální	komediální	k2eAgInSc4d1	komediální
film	film	k1gInSc4	film
Kokosy	kokos	k1gInPc7	kokos
na	na	k7c6	na
sněhu	sníh	k1gInSc6	sníh
<g/>
.	.	kIx.	.
</s>
