<s>
Francisco	Francisco	k6eAd1	Francisco
je	být	k5eAaImIp3nS	být
měsíc	měsíc	k1gInSc4	měsíc
planety	planeta	k1gFnSc2	planeta
Uran	Uran	k1gInSc1	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
vnějších	vnější	k2eAgInPc2d1	vnější
Uranových	Uranův	k2eAgInPc2d1	Uranův
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
od	od	k7c2	od
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
4	[number]	k4	4
276	[number]	k4	276
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
22	[number]	k4	22
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
cca	cca	kA	cca
1,4	[number]	k4	1,4
<g/>
×	×	k?	×
<g/>
1015	[number]	k4	1015
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
objeven	objevit	k5eAaPmNgMnS	objevit
Matthewem	Matthew	k1gMnSc7	Matthew
J.	J.	kA	J.
Holmanem	Holman	k1gMnSc7	Holman
a	a	k8xC	a
Brettem	Brett	k1gMnSc7	Brett
J.	J.	kA	J.
Gladmanem	Gladman	k1gMnSc7	Gladman
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
ze	z	k7c2	z
snímků	snímek	k1gInPc2	snímek
získaných	získaný	k2eAgInPc2d1	získaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
nese	nést	k5eAaImIp3nS	nést
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
díla	dílo	k1gNnSc2	dílo
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
.	.	kIx.	.
</s>
