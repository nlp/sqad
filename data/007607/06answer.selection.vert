<s>
Největší	veliký	k2eAgFnSc7d3	veliký
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
galerií	galerie	k1gFnSc7	galerie
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
Moravská	moravský	k2eAgFnSc1d1	Moravská
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
druhým	druhý	k4xOgMnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
muzeem	muzeum	k1gNnSc7	muzeum
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
