<p>
<s>
Fytogeografie	fytogeografie	k1gFnSc1	fytogeografie
je	být	k5eAaImIp3nS	být
mezioborová	mezioborový	k2eAgFnSc1d1	mezioborová
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
studující	studující	k2eAgNnSc1d1	studující
rozšíření	rozšíření	k1gNnSc4	rozšíření
rostlin	rostlina	k1gFnPc2	rostlina
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
ztotožňována	ztotožňován	k2eAgFnSc1d1	ztotožňována
s	s	k7c7	s
geobotanikou	geobotanika	k1gFnSc7	geobotanika
neboli	neboli	k8xC	neboli
fytocenologií	fytocenologie	k1gFnSc7	fytocenologie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc1	obor
zkoumající	zkoumající	k2eAgInSc1d1	zkoumající
vegetaci	vegetace	k1gFnSc4	vegetace
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
rostlinných	rostlinný	k2eAgNnPc2d1	rostlinné
společenstev	společenstvo	k1gNnPc2	společenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
součástí	součást	k1gFnSc7	součást
fytogeografie	fytogeografie	k1gFnSc2	fytogeografie
jsou	být	k5eAaImIp3nP	být
botanika	botanika	k1gFnSc1	botanika
a	a	k8xC	a
geografie	geografie	k1gFnSc1	geografie
<g/>
.	.	kIx.	.
</s>
<s>
Fytografie	fytografie	k1gFnSc1	fytografie
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
biogeografie	biogeografie	k1gFnSc2	biogeografie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zakladatele	zakladatel	k1gMnPc4	zakladatel
tohoto	tento	k3xDgInSc2	tento
oboru	obor	k1gInSc2	obor
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Alexander	Alexandra	k1gFnPc2	Alexandra
von	von	k1gInSc1	von
Humboldt	Humboldt	k2eAgMnSc1d1	Humboldt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
fytos	fytos	k1gInSc1	fytos
=	=	kIx~	=
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
cenóza	cenóza	k1gFnSc1	cenóza
=	=	kIx~	=
společenstvo	společenstvo	k1gNnSc1	společenstvo
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
==	==	k?	==
</s>
</p>
<p>
<s>
Fytografie	fytografie	k1gFnSc1	fytografie
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
následující	následující	k2eAgInPc4d1	následující
podobory	podobor	k1gInPc4	podobor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Areálová	areálový	k2eAgFnSc1d1	areálová
fytogeografie	fytogeografie	k1gFnSc1	fytogeografie
neboli	neboli	k8xC	neboli
chorologie	chorologie	k1gFnSc1	chorologie
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
rozšířením	rozšíření	k1gNnSc7	rozšíření
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
areálů	areál	k1gInPc2	areál
<g/>
,	,	kIx,	,
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
jejich	jejich	k3xOp3gInSc4	jejich
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
je	on	k3xPp3gMnPc4	on
porovnává	porovnávat	k5eAaImIp3nS	porovnávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historická	historický	k2eAgFnSc1d1	historická
fytogeografie	fytogeografie	k1gFnSc1	fytogeografie
neboli	neboli	k8xC	neboli
genetická	genetický	k2eAgFnSc1d1	genetická
fytogeografie	fytogeografie	k1gFnSc1	fytogeografie
–	–	k?	–
studuje	studovat	k5eAaImIp3nS	studovat
flórogenezi	flórogeneze	k1gFnSc4	flórogeneze
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
změny	změna	k1gFnPc1	změna
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
flór	flóra	k1gFnPc2	flóra
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Regionální	regionální	k2eAgFnSc1d1	regionální
fytogeografie	fytogeografie	k1gFnSc1	fytogeografie
–	–	k?	–
porovnává	porovnávat	k5eAaImIp3nS	porovnávat
květeny	květena	k1gFnPc4	květena
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nP	řadit
je	on	k3xPp3gFnPc4	on
do	do	k7c2	do
květenných	květenný	k2eAgFnPc2d1	květenný
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
chorionů	chorion	k1gInPc2	chorion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Geografie	geografie	k1gFnSc1	geografie
vegetace	vegetace	k1gFnSc2	vegetace
–	–	k?	–
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
rostlinné	rostlinný	k2eAgFnPc4d1	rostlinná
formace	formace	k1gFnPc4	formace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tropický	tropický	k2eAgInSc1d1	tropický
deštný	deštný	k2eAgInSc1d1	deštný
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
savana	savana	k1gFnSc1	savana
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
fytogeografie	fytogeografie	k1gFnSc2	fytogeografie
považuje	považovat	k5eAaImIp3nS	považovat
Alexander	Alexandra	k1gFnPc2	Alexandra
von	von	k1gInSc1	von
Humboldt	Humboldt	k1gMnSc1	Humboldt
(	(	kIx(	(
<g/>
1769	[number]	k4	1769
<g/>
-	-	kIx~	-
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zmínit	zmínit	k5eAaPmF	zmínit
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byl	být	k5eAaImAgMnS	být
Theofrastos	Theofrastos	k1gMnSc1	Theofrastos
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
již	již	k6eAd1	již
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
popsal	popsat	k5eAaPmAgMnS	popsat
azonální	azonální	k2eAgFnSc4d1	azonální
formaci	formace	k1gFnSc4	formace
mangrovů	mangrovy	k1gInPc2	mangrovy
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1805	[number]	k4	1805
vydal	vydat	k5eAaPmAgMnS	vydat
Alexander	Alexandra	k1gFnPc2	Alexandra
von	von	k1gInSc1	von
Humboldt	Humboldt	k1gMnSc1	Humboldt
spis	spis	k1gInSc1	spis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
vymezil	vymezit	k5eAaPmAgInS	vymezit
fytogeografii	fytogeografie	k1gFnSc4	fytogeografie
jako	jako	k8xS	jako
vědu	věda	k1gFnSc4	věda
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnSc4	zakladatel
tohoto	tento	k3xDgInSc2	tento
vědního	vědní	k2eAgInSc2d1	vědní
oboru	obor	k1gInSc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zavedl	zavést	k5eAaPmAgInS	zavést
pojem	pojem	k1gInSc1	pojem
asociace	asociace	k1gFnSc2	asociace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
výrazný	výrazný	k2eAgInSc1d1	výrazný
typ	typ	k1gInSc1	typ
rostlinného	rostlinný	k2eAgNnSc2d1	rostlinné
společenstva	společenstvo	k1gNnSc2	společenstvo
s	s	k7c7	s
určitým	určitý	k2eAgNnSc7d1	určité
floristickým	floristický	k2eAgNnSc7d1	floristické
složením	složení	k1gNnSc7	složení
<g/>
;	;	kIx,	;
definoval	definovat	k5eAaBmAgMnS	definovat
pojmy	pojem	k1gInPc4	pojem
flóra	flóra	k1gFnSc1	flóra
a	a	k8xC	a
vegetace	vegetace	k1gFnSc1	vegetace
<g/>
;	;	kIx,	;
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
vliv	vliv	k1gInSc4	vliv
klimatu	klima	k1gNnSc2	klima
na	na	k7c4	na
utváření	utváření	k1gNnSc4	utváření
rostlinných	rostlinný	k2eAgFnPc2d1	rostlinná
formací	formace	k1gFnPc2	formace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
osmnáct	osmnáct	k4xCc4	osmnáct
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1823	[number]	k4	1823
vydává	vydávat	k5eAaPmIp3nS	vydávat
J.	J.	kA	J.
F.	F.	kA	F.
Schouw	Schouw	k1gFnSc1	Schouw
první	první	k4xOgFnSc4	první
příručku	příručka	k1gFnSc4	příručka
fytogeografie	fytogeografie	k1gFnSc2	fytogeografie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
spise	spis	k1gInSc6	spis
Shouw	Shouw	k1gFnSc2	Shouw
definoval	definovat	k5eAaBmAgInS	definovat
areálovou	areálový	k2eAgFnSc4d1	areálová
a	a	k8xC	a
regionální	regionální	k2eAgFnSc4d1	regionální
fytogeografii	fytogeografie	k1gFnSc4	fytogeografie
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
některé	některý	k3yIgInPc4	některý
abiotické	abiotický	k2eAgInPc4d1	abiotický
faktory	faktor	k1gInPc4	faktor
<g/>
,	,	kIx,	,
definoval	definovat	k5eAaBmAgInS	definovat
areál	areál	k1gInSc1	areál
(	(	kIx(	(
<g/>
ač	ač	k8xS	ač
výrazu	výraz	k1gInSc2	výraz
"	"	kIx"	"
<g/>
areál	areál	k1gInSc1	areál
<g/>
"	"	kIx"	"
nepoužil	použít	k5eNaPmAgInS	použít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vymezil	vymezit	k5eAaPmAgInS	vymezit
svět	svět	k1gInSc1	svět
na	na	k7c4	na
květenné	květenný	k2eAgInPc4d1	květenný
regiony	region	k1gInPc4	region
a	a	k8xC	a
zobrazil	zobrazit	k5eAaPmAgMnS	zobrazit
některé	některý	k3yIgInPc4	některý
fytogeografické	fytogeografický	k2eAgInPc4d1	fytogeografický
jevy	jev	k1gInPc4	jev
na	na	k7c6	na
geografických	geografický	k2eAgFnPc6d1	geografická
mapách	mapa	k1gFnPc6	mapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
jmen	jméno	k1gNnPc2	jméno
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
uvést	uvést	k5eAaPmF	uvést
F.	F.	kA	F.
J.	J.	kA	J.
Meyena	Meyen	k1gMnSc2	Meyen
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
použil	použít	k5eAaPmAgInS	použít
poprvé	poprvé	k6eAd1	poprvé
termín	termín	k1gInSc1	termín
areál	areál	k1gInSc1	areál
<g/>
,	,	kIx,	,
A.	A.	kA	A.
L.	L.	kA	L.
P.	P.	kA	P.
P.	P.	kA	P.
de	de	k?	de
Condolla	Condolla	k1gMnSc1	Condolla
(	(	kIx(	(
<g/>
1806	[number]	k4	1806
<g/>
-	-	kIx~	-
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zavedl	zavést	k5eAaPmAgInS	zavést
termíny	termín	k1gInPc4	termín
endemit	endemit	k1gInSc1	endemit
<g/>
,	,	kIx,	,
relikt	relikt	k1gInSc1	relikt
a	a	k8xC	a
disjunkce	disjunkce	k1gFnSc1	disjunkce
<g/>
,	,	kIx,	,
a	a	k8xC	a
A.	A.	kA	A.
H.	H.	kA	H.
R.	R.	kA	R.
Griselbacha	Griselbacha	k1gMnSc1	Griselbacha
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
přehled	přehled	k1gInSc4	přehled
vegetace	vegetace	k1gFnSc2	vegetace
Země	zem	k1gFnSc2	zem
podle	podle	k7c2	podle
vlivu	vliv	k1gInSc2	vliv
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
významnými	významný	k2eAgMnPc7d1	významný
vědci	vědec	k1gMnPc7	vědec
působícími	působící	k2eAgMnPc7d1	působící
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
fytogeografie	fytogeografie	k1gFnSc2	fytogeografie
byly	být	k5eAaImAgFnP	být
O.	O.	kA	O.
Heer	Heer	k1gInSc4	Heer
a	a	k8xC	a
A.	A.	kA	A.
Engler	Engler	k1gMnSc1	Engler
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
studoval	studovat	k5eAaImAgMnS	studovat
genezi	geneze	k1gFnSc4	geneze
flór	flóra	k1gFnPc2	flóra
od	od	k7c2	od
třetihor	třetihory	k1gFnPc2	třetihory
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Česká	český	k2eAgFnSc1d1	Česká
fytogeografie	fytogeografie	k1gFnSc1	fytogeografie
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
botanického	botanický	k2eAgInSc2d1	botanický
výzkumu	výzkum	k1gInSc2	výzkum
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
se	se	k3xPyFc4	se
zasloužilo	zasloužit	k5eAaPmAgNnS	zasloužit
především	především	k9	především
založení	založení	k1gNnSc1	založení
ústavu	ústav	k1gInSc2	ústav
botaniky	botanika	k1gFnSc2	botanika
na	na	k7c6	na
Karlově	Karlův	k2eAgFnSc6d1	Karlova
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
r.	r.	kA	r.
1770	[number]	k4	1770
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následně	následně	k6eAd1	následně
spousta	spousta	k1gFnSc1	spousta
odborníků	odborník	k1gMnPc2	odborník
i	i	k8xC	i
laiků	laik	k1gMnPc2	laik
zpracovávala	zpracovávat	k5eAaImAgFnS	zpracovávat
spisy	spis	k1gInPc4	spis
o	o	k7c6	o
květeně	květena	k1gFnSc6	květena
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
našeho	náš	k3xOp1gNnSc2	náš
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1819	[number]	k4	1819
předložili	předložit	k5eAaPmAgMnP	předložit
bratři	bratr	k1gMnPc1	bratr
Preslovi	Preslův	k2eAgMnPc1d1	Preslův
botanické	botanický	k2eAgFnPc4d1	botanická
veřejnosti	veřejnost	k1gFnSc6	veřejnost
dokončenou	dokončený	k2eAgFnSc4d1	dokončená
a	a	k8xC	a
vydanou	vydaný	k2eAgFnSc4d1	vydaná
českou	český	k2eAgFnSc4d1	Česká
květenu	květena	k1gFnSc4	květena
(	(	kIx(	(
<g/>
Flora	Flora	k1gFnSc1	Flora
Čechica	Čechica	k1gMnSc1	Čechica
<g/>
)	)	kIx)	)
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1500	[number]	k4	1500
rostlinnými	rostlinný	k2eAgInPc7d1	rostlinný
druhy	druh	k1gInPc7	druh
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
sjednotit	sjednotit	k5eAaPmF	sjednotit
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
květeně	květena	k1gFnSc6	květena
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
krajů	kraj	k1gInPc2	kraj
našeho	náš	k3xOp1gNnSc2	náš
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgMnSc7	první
průkopníkem	průkopník	k1gMnSc7	průkopník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
zabýval	zabývat	k5eAaImAgInS	zabývat
také	také	k9	také
jejich	jejich	k3xOp3gNnSc3	jejich
nalezišti	naleziště	k1gNnSc3	naleziště
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
florista	florista	k1gMnSc1	florista
Filip	Filip	k1gMnSc1	Filip
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Opiz	Opiz	k1gMnSc1	Opiz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
osobností	osobnost	k1gFnSc7	osobnost
botaniky	botanika	k1gFnSc2	botanika
Ladislav	Ladislava	k1gFnPc2	Ladislava
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Čelakovský	Čelakovský	k2eAgMnSc1d1	Čelakovský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
svým	svůj	k3xOyFgMnPc3	svůj
výzkumem	výzkum	k1gInSc7	výzkum
tzv.	tzv.	kA	tzv.
Květeny	květena	k1gFnPc1	květena
okolí	okolí	k1gNnSc2	okolí
Pražského	pražský	k2eAgNnSc2d1	Pražské
<g/>
,	,	kIx,	,
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
osobností	osobnost	k1gFnPc2	osobnost
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
tak	tak	k6eAd1	tak
vznikat	vznikat	k5eAaImF	vznikat
první	první	k4xOgFnPc4	první
fytogeografické	fytogeografický	k2eAgFnPc4d1	fytogeografická
studie	studie	k1gFnPc4	studie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgNnSc4d1	významné
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
práce	práce	k1gFnSc2	práce
Jana	Jan	k1gMnSc2	Jan
Palackého	Palacký	k1gMnSc2	Palacký
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
Fr.	Fr.	k1gMnSc2	Fr.
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Emanuela	Emanuel	k1gMnSc2	Emanuel
Purkyně	Purkyně	k1gFnSc2	Purkyně
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
J.	J.	kA	J.
E.	E.	kA	E.
Purkyně	Purkyně	k1gFnSc1	Purkyně
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pak	pak	k6eAd1	pak
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
botaniků	botanik	k1gMnPc2	botanik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
květenou	květena	k1gFnSc7	květena
konkrétních	konkrétní	k2eAgFnPc2d1	konkrétní
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
botanických	botanický	k2eAgInPc2d1	botanický
spolků	spolek	k1gInPc2	spolek
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
flóru	flóra	k1gFnSc4	flóra
daných	daný	k2eAgFnPc2d1	daná
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Spolky	spolek	k1gInPc1	spolek
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgInPc2	svůj
poznatků	poznatek	k1gInPc2	poznatek
vydávali	vydávat	k5eAaImAgMnP	vydávat
v	v	k7c6	v
odborných	odborný	k2eAgInPc6d1	odborný
časopisech	časopis	k1gInPc6	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významnější	významný	k2eAgFnPc4d2	významnější
osobnosti	osobnost	k1gFnPc4	osobnost
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
fytogeografického	fytogeografický	k2eAgInSc2d1	fytogeografický
výzkumu	výzkum	k1gInSc2	výzkum
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Josef	Josef	k1gMnSc1	Josef
Podpěra	podpěra	k1gFnSc1	podpěra
<g/>
,	,	kIx,	,
Anežka	Anežka	k1gFnSc1	Anežka
Hrabětová-Uhrová	Hrabětová-Uhrová	k1gFnSc1	Hrabětová-Uhrová
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Domin	domino	k1gNnPc2	domino
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
výzkum	výzkum	k1gInSc1	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
fytogeografie	fytogeografie	k1gFnSc2	fytogeografie
utlumen	utlumen	k2eAgInSc1d1	utlumen
<g/>
,	,	kIx,	,
zvrat	zvrat	k1gInSc1	zvrat
nastal	nastat	k5eAaPmAgInS	nastat
až	až	k9	až
po	po	k7c4	po
osvobození	osvobození	k1gNnSc4	osvobození
v	v	k7c6	v
r.	r.	kA	r.
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikala	vznikat	k5eAaImAgFnS	vznikat
velká	velký	k2eAgFnSc1d1	velká
řada	řada	k1gFnSc1	řada
disertačních	disertační	k2eAgFnPc2d1	disertační
prací	práce	k1gFnPc2	práce
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
fytogeografie	fytogeografie	k1gFnSc2	fytogeografie
určitých	určitý	k2eAgFnPc2d1	určitá
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
také	také	k9	také
další	další	k2eAgFnPc4d1	další
fytogeografické	fytogeografický	k2eAgFnPc4d1	fytogeografická
studie	studie	k1gFnPc4	studie
např.	např.	kA	např.
od	od	k7c2	od
Radovana	Radovan	k1gMnSc4	Radovan
Hendrycha	Hendrych	k1gMnSc2	Hendrych
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
byly	být	k5eAaImAgFnP	být
vydávány	vydáván	k2eAgInPc4d1	vydáván
odborné	odborný	k2eAgInPc4d1	odborný
články	článek	k1gInPc4	článek
např.	např.	kA	např.
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Preslia	Preslium	k1gNnSc2	Preslium
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vzniká	vznikat	k5eAaImIp3nS	vznikat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
publikace	publikace	k1gFnSc1	publikace
Květena	květena	k1gFnSc1	květena
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
přepracovaná	přepracovaný	k2eAgFnSc1d1	přepracovaná
jako	jako	k8xS	jako
květena	květena	k1gFnSc1	květena
ČR	ČR	kA	ČR
od	od	k7c2	od
Slavíka	slavík	k1gInSc2	slavík
a	a	k8xC	a
Hejného	Hejné	k1gNnSc2	Hejné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HENDRYCH	Hendrych	k1gMnSc1	Hendrych
<g/>
,	,	kIx,	,
Radovan	Radovan	k1gMnSc1	Radovan
<g/>
.	.	kIx.	.
</s>
<s>
Fytogeografie	fytogeografie	k1gFnSc1	fytogeografie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HEJNÝ	HEJNÝ	kA	HEJNÝ
S.	S.	kA	S.
<g/>
,	,	kIx,	,
SLAVÍK	slavík	k1gInSc1	slavík
B.	B.	kA	B.
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Eds	Eds	k1gFnSc1	Eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
KVĚTENA	květena	k1gFnSc1	květena
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
1	[number]	k4	1
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Fytocenologie	fytocenologie	k1gFnSc1	fytocenologie
</s>
</p>
<p>
<s>
Geobotanika	geobotanika	k1gFnSc1	geobotanika
</s>
</p>
<p>
<s>
Fytogeografické	fytogeografický	k2eAgNnSc1d1	fytogeografické
členění	členění	k1gNnSc1	členění
ČR	ČR	kA	ČR
</s>
</p>
