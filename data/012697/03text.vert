<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
MOP	mop	k1gInSc1	mop
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
International	International	k1gMnSc1	International
Labour	Laboura	k1gFnPc2	Laboura
Organization	Organization	k1gInSc1	Organization
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
ILO	ILO	kA	ILO
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
organizace	organizace	k1gFnSc1	organizace
OSN	OSN	kA	OSN
usilující	usilující	k2eAgFnSc1d1	usilující
o	o	k7c6	o
prosazování	prosazování	k1gNnSc6	prosazování
sociální	sociální	k2eAgFnSc2d1	sociální
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaných	uznávaný	k2eAgNnPc2d1	uznávané
pracovních	pracovní	k2eAgNnPc2d1	pracovní
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
jako	jako	k8xS	jako
stálé	stálý	k2eAgNnSc1d1	stálé
zřízení	zřízení	k1gNnSc1	zřízení
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
byla	být	k5eAaImAgFnS	být
podpora	podpora	k1gFnSc1	podpora
světového	světový	k2eAgInSc2d1	světový
míru	mír	k1gInSc2	mír
na	na	k7c6	na
základě	základ	k1gInSc6	základ
sociální	sociální	k2eAgFnSc2d1	sociální
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1946	[number]	k4	1946
je	být	k5eAaImIp3nS	být
specializovanou	specializovaný	k2eAgFnSc7d1	specializovaná
organizací	organizace	k1gFnSc7	organizace
OSN	OSN	kA	OSN
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
.	.	kIx.	.
</s>
<s>
MOP	mop	k1gInSc1	mop
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
187	[number]	k4	187
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
spolupráce	spolupráce	k1gFnSc2	spolupráce
při	při	k7c6	při
úpravě	úprava	k1gFnSc6	úprava
a	a	k8xC	a
regulaci	regulace	k1gFnSc6	regulace
pracovních	pracovní	k2eAgFnPc2d1	pracovní
podmínek	podmínka	k1gFnPc2	podmínka
pochází	pocházet	k5eAaImIp3nS	pocházet
už	už	k6eAd1	už
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
alsaský	alsaský	k2eAgMnSc1d1	alsaský
průmyslník	průmyslník	k1gMnSc1	průmyslník
Daniel	Daniel	k1gMnSc1	Daniel
Legrand	legranda	k1gFnPc2	legranda
apeloval	apelovat	k5eAaImAgMnS	apelovat
na	na	k7c4	na
evropské	evropský	k2eAgMnPc4d1	evropský
státníky	státník	k1gMnPc4	státník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jednali	jednat	k5eAaImAgMnP	jednat
na	na	k7c6	na
mezivládní	mezivládní	k2eAgFnSc6d1	mezivládní
úrovni	úroveň	k1gFnSc6	úroveň
o	o	k7c4	o
řešení	řešení	k1gNnSc4	řešení
zneužívání	zneužívání	k1gNnSc2	zneužívání
pracujících	pracující	k2eAgMnPc2d1	pracující
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
založena	založen	k2eAgFnSc1d1	založena
tzv.	tzv.	kA	tzv.
První	první	k4xOgFnSc1	první
internacionála	internacionála	k1gFnSc1	internacionála
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
nepřežila	přežít	k5eNaPmAgFnS	přežít
rok	rok	k1gInSc4	rok
1876	[number]	k4	1876
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
15	[number]	k4	15
evropských	evropský	k2eAgFnPc2d1	Evropská
vlád	vláda	k1gFnPc2	vláda
konference	konference	k1gFnSc2	konference
o	o	k7c6	o
podmínkách	podmínka	k1gFnPc6	podmínka
dětské	dětský	k2eAgFnSc2d1	dětská
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
navázala	navázat	k5eAaPmAgFnS	navázat
další	další	k2eAgNnSc4d1	další
jednání	jednání	k1gNnSc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
nevládní	vládní	k2eNgFnSc2d1	nevládní
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
asociace	asociace	k1gFnSc2	asociace
pro	pro	k7c4	pro
právní	právní	k2eAgFnSc4d1	právní
ochranu	ochrana	k1gFnSc4	ochrana
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gInPc7	jejíž
hlavními	hlavní	k2eAgInPc7d1	hlavní
iniciátory	iniciátor	k1gInPc7	iniciátor
byli	být	k5eAaImAgMnP	být
Welšan	Welšan	k1gMnSc1	Welšan
Robert	Robert	k1gMnSc1	Robert
Owen	Owen	k1gMnSc1	Owen
a	a	k8xC	a
Francouz	Francouz	k1gMnSc1	Francouz
Daniel	Daniel	k1gMnSc1	Daniel
Legrand	legranda	k1gFnPc2	legranda
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
založen	založit	k5eAaPmNgInS	založit
první	první	k4xOgInSc1	první
nadnárodní	nadnárodní	k2eAgInSc1d1	nadnárodní
odborový	odborový	k2eAgInSc1d1	odborový
orgán	orgán	k1gInSc1	orgán
–	–	k?	–
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
sekretariát	sekretariát	k1gInSc1	sekretariát
odborových	odborový	k2eAgFnPc2d1	odborová
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
přejmenován	přejmenovat	k5eAaPmNgMnS	přejmenovat
na	na	k7c4	na
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
federaci	federace	k1gFnSc4	federace
odborů	odbor	k1gInPc2	odbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Leedsu	Leeds	k1gInSc6	Leeds
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
odborový	odborový	k2eAgInSc1d1	odborový
kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
vzešel	vzejít	k5eAaPmAgMnS	vzejít
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c6	na
zřízení	zřízení	k1gNnSc6	zřízení
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
úřadu	úřad	k1gInSc2	úřad
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
sešli	sejít	k5eAaPmAgMnP	sejít
zástupci	zástupce	k1gMnPc1	zástupce
27	[number]	k4	27
zemí	zem	k1gFnPc2	zem
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
vypracování	vypracování	k1gNnSc1	vypracování
řady	řada	k1gFnSc2	řada
mírových	mírový	k2eAgFnPc2d1	mírová
smluv	smlouva	k1gFnPc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
Versailleská	versailleský	k2eAgFnSc1d1	Versailleská
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
zakládala	zakládat	k5eAaImAgFnS	zakládat
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
a	a	k8xC	a
také	také	k9	také
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
organizaci	organizace	k1gFnSc4	organizace
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
smlouvy	smlouva	k1gFnSc2	smlouva
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
původní	původní	k2eAgNnSc4d1	původní
znění	znění	k1gNnSc4	znění
Ústavy	ústava	k1gFnSc2	ústava
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Ústavu	ústava	k1gFnSc4	ústava
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
1919	[number]	k4	1919
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
pracovní	pracovní	k2eAgFnSc4d1	pracovní
legislativu	legislativa	k1gFnSc4	legislativa
zřízený	zřízený	k2eAgInSc1d1	zřízený
mírovou	mírový	k2eAgFnSc7d1	mírová
konferencí	konference	k1gFnSc7	konference
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
předsedal	předsedat	k5eAaImAgInS	předsedat
Samuel	Samuel	k1gMnSc1	Samuel
Gompers	Gompersa	k1gFnPc2	Gompersa
<g/>
,	,	kIx,	,
šéf	šéf	k1gMnSc1	šéf
Americké	americký	k2eAgFnSc2d1	americká
federace	federace	k1gFnSc2	federace
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
byl	být	k5eAaImAgInS	být
složený	složený	k2eAgInSc1d1	složený
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
devíti	devět	k4xCc2	devět
zemí	zem	k1gFnPc2	zem
<g/>
:	:	kIx,	:
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
,	,	kIx,	,
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
bylo	být	k5eAaImAgNnS	být
udržení	udržení	k1gNnSc1	udržení
míru	mír	k1gInSc2	mír
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
sociální	sociální	k2eAgFnSc2d1	sociální
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
se	se	k3xPyFc4	se
usnesl	usnést	k5eAaPmAgInS	usnést
na	na	k7c6	na
výsledné	výsledný	k2eAgFnSc6d1	výsledná
podobě	podoba	k1gFnSc6	podoba
Ústavy	ústava	k1gFnSc2	ústava
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
mírová	mírový	k2eAgFnSc1d1	mírová
konference	konference	k1gFnSc1	konference
ji	on	k3xPp3gFnSc4	on
bez	bez	k7c2	bez
výhrad	výhrada	k1gFnPc2	výhrada
přijala	přijmout	k5eAaPmAgFnS	přijmout
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
včlenila	včlenit	k5eAaPmAgFnS	včlenit
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
smlouvu	smlouva	k1gFnSc4	smlouva
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
ve	v	k7c6	v
Sbírce	sbírka	k1gFnSc6	sbírka
zákonů	zákon	k1gInPc2	zákon
pod	pod	k7c7	pod
číslem	číslo	k1gNnSc7	číslo
217	[number]	k4	217
<g/>
/	/	kIx~	/
<g/>
1921	[number]	k4	1921
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
první	první	k4xOgFnSc6	první
konferenci	konference	k1gFnSc6	konference
MOP	mop	k1gInSc1	mop
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1919	[number]	k4	1919
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
prvních	první	k4xOgInPc2	první
6	[number]	k4	6
úmluv	úmluva	k1gFnPc2	úmluva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
upravovaly	upravovat	k5eAaImAgFnP	upravovat
pracovní	pracovní	k2eAgFnSc4d1	pracovní
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
<g/>
,	,	kIx,	,
ochranu	ochrana	k1gFnSc4	ochrana
mateřství	mateřství	k1gNnSc2	mateřství
<g/>
,	,	kIx,	,
noční	noční	k2eAgFnSc4d1	noční
práci	práce	k1gFnSc4	práce
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
mladistvých	mladistvý	k1gMnPc2	mladistvý
<g/>
,	,	kIx,	,
a	a	k8xC	a
minimální	minimální	k2eAgFnSc4d1	minimální
věkovou	věkový	k2eAgFnSc4d1	věková
hranici	hranice	k1gFnSc4	hranice
pracujících	pracující	k1gMnPc2	pracující
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
dokumentů	dokument	k1gInPc2	dokument
(	(	kIx(	(
<g/>
16	[number]	k4	16
úmluv	úmluva	k1gFnPc2	úmluva
a	a	k8xC	a
18	[number]	k4	18
doporučení	doporučení	k1gNnPc2	doporučení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
kontrolu	kontrola	k1gFnSc4	kontrola
a	a	k8xC	a
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
jejich	jejich	k3xOp3gFnSc7	jejich
aplikací	aplikace	k1gFnSc7	aplikace
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
Komise	komise	k1gFnSc1	komise
expertů	expert	k1gMnPc2	expert
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1920	[number]	k4	1920
získala	získat	k5eAaPmAgFnS	získat
organizace	organizace	k1gFnSc1	organizace
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
a	a	k8xC	a
prvním	první	k4xOgMnSc6	první
ředitelem	ředitel	k1gMnSc7	ředitel
jejího	její	k3xOp3gInSc2	její
úřadu	úřad	k1gInSc2	úřad
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
francouzský	francouzský	k2eAgMnSc1d1	francouzský
zástupce	zástupce	k1gMnSc1	zástupce
Albert	Albert	k1gMnSc1	Albert
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
ho	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Brit	Brit	k1gMnSc1	Brit
Harold	Harold	k1gMnSc1	Harold
Butler	Butler	k1gMnSc1	Butler
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
následoval	následovat	k5eAaImAgMnS	následovat
Američan	Američan	k1gMnSc1	Američan
John	John	k1gMnSc1	John
Winant	Winant	k1gMnSc1	Winant
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgMnSc1d2	pozdější
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
USA	USA	kA	USA
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1940	[number]	k4	1940
se	se	k3xPyFc4	se
sídlo	sídlo	k1gNnSc4	sídlo
organizace	organizace	k1gFnSc2	organizace
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
kanadského	kanadský	k2eAgInSc2d1	kanadský
Montréalu	Montréal	k1gInSc2	Montréal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
irský	irský	k2eAgMnSc1d1	irský
ředitel	ředitel	k1gMnSc1	ředitel
Edward	Edward	k1gMnSc1	Edward
Phelan	Phelan	k1gMnSc1	Phelan
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
již	již	k6eAd1	již
26	[number]	k4	26
<g/>
.	.	kIx.	.
konferenci	konference	k1gFnSc4	konference
s	s	k7c7	s
účastí	účast	k1gFnSc7	účast
zástupců	zástupce	k1gMnPc2	zástupce
41	[number]	k4	41
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
vzešla	vzejít	k5eAaPmAgFnS	vzejít
Filadelfská	filadelfský	k2eAgFnSc1d1	Filadelfská
deklarace	deklarace	k1gFnSc1	deklarace
o	o	k7c6	o
cílech	cíl	k1gInPc6	cíl
a	a	k8xC	a
úkolech	úkol	k1gInPc6	úkol
MOP	mop	k1gInSc1	mop
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
doplnila	doplnit	k5eAaPmAgFnS	doplnit
Ústavu	ústava	k1gFnSc4	ústava
MOP	mop	k1gInSc1	mop
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
organizace	organizace	k1gFnSc2	organizace
jedinou	jediný	k2eAgFnSc7d1	jediná
přeživší	přeživší	k2eAgFnSc7d1	přeživší
organizací	organizace	k1gFnSc7	organizace
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
přijata	přijat	k2eAgFnSc1d1	přijata
rezoluce	rezoluce	k1gFnSc1	rezoluce
o	o	k7c4	o
navázání	navázání	k1gNnSc4	navázání
styku	styk	k1gInSc2	styk
s	s	k7c7	s
Organizací	organizace	k1gFnSc7	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
stvrdil	stvrdit	k5eAaPmAgInS	stvrdit
Generální	generální	k2eAgInSc1d1	generální
sekretariát	sekretariát	k1gInSc1	sekretariát
OSN	OSN	kA	OSN
spojení	spojení	k1gNnSc4	spojení
obou	dva	k4xCgFnPc2	dva
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
přijala	přijmout	k5eAaPmAgFnS	přijmout
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
pod	pod	k7c7	pod
Phelanovým	Phelanův	k2eAgNnSc7d1	Phelanův
vedením	vedení	k1gNnSc7	vedení
<g/>
,	,	kIx,	,
usnesení	usnesení	k1gNnSc4	usnesení
č.	č.	k?	č.
87	[number]	k4	87
o	o	k7c6	o
svobodě	svoboda	k1gFnSc6	svoboda
sdružování	sdružování	k1gNnSc6	sdružování
a	a	k8xC	a
právu	právo	k1gNnSc6	právo
na	na	k7c6	na
organizování	organizování	k1gNnSc6	organizování
<g/>
.	.	kIx.	.
<g/>
Už	už	k6eAd1	už
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
členy	člen	k1gInPc1	člen
MOP	mop	k1gInSc1	mop
i	i	k8xC	i
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
a	a	k8xC	a
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1948	[number]	k4	1948
a	a	k8xC	a
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
úřad	úřad	k1gInSc1	úřad
MOP	mop	k1gInSc1	mop
vedl	vést	k5eAaImAgMnS	vést
Američan	Američan	k1gMnSc1	Američan
David	David	k1gMnSc1	David
Morse	Morse	k1gMnSc1	Morse
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
úředníků	úředník	k1gMnPc2	úředník
organizace	organizace	k1gFnSc2	organizace
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
a	a	k8xC	a
rozpočet	rozpočet	k1gInSc4	rozpočet
pětkrát	pětkrát	k6eAd1	pětkrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
MOP	mop	k1gInSc1	mop
založila	založit	k5eAaPmAgFnS	založit
Ústav	ústav	k1gInSc4	ústav
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
školící	školící	k2eAgNnSc1d1	školící
středisko	středisko	k1gNnSc1	středisko
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
Turínu	Turín	k1gInSc3	Turín
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
padesátého	padesátý	k4xOgInSc2	padesátý
výročí	výročí	k1gNnSc3	výročí
MOP	mop	k1gInSc4	mop
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
obdržela	obdržet	k5eAaPmAgFnS	obdržet
organizace	organizace	k1gFnSc1	organizace
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
úřad	úřad	k1gInSc1	úřad
vedl	vést	k5eAaImAgMnS	vést
Brit	Brit	k1gMnSc1	Brit
Wilfred	Wilfred	k1gMnSc1	Wilfred
Jenks	Jenks	k1gInSc1	Jenks
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
Francouz	Francouz	k1gMnSc1	Francouz
Francis	Francis	k1gFnPc2	Francis
Blanchard	Blanchard	k1gMnSc1	Blanchard
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1977	[number]	k4	1977
až	až	k8xS	až
1980	[number]	k4	1980
se	se	k3xPyFc4	se
z	z	k7c2	z
MOP	mop	k1gInSc4	mop
stáhly	stáhnout	k5eAaPmAgInP	stáhnout
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
práce	práce	k1gFnSc2	práce
sehrála	sehrát	k5eAaPmAgFnS	sehrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
např.	např.	kA	např.
při	při	k7c6	při
emancipaci	emancipace	k1gFnSc6	emancipace
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podporovala	podporovat	k5eAaImAgFnS	podporovat
legitimitu	legitimita	k1gFnSc4	legitimita
odborových	odborový	k2eAgInPc2d1	odborový
svazů	svaz	k1gInPc2	svaz
Solidarity	solidarita	k1gFnSc2	solidarita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc3	vedení
úřadu	úřad	k1gInSc2	úřad
MOP	mop	k1gInSc1	mop
ujal	ujmout	k5eAaPmAgMnS	ujmout
Belgičan	Belgičan	k1gMnSc1	Belgičan
Michel	Michel	k1gMnSc1	Michel
Hansenne	Hansenn	k1gMnSc5	Hansenn
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Chilan	Chilan	k1gMnSc1	Chilan
Juan	Juan	k1gMnSc1	Juan
Somavía	Somavía	k1gMnSc1	Somavía
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Angličan	Angličan	k1gMnSc1	Angličan
Guy	Guy	k1gMnSc1	Guy
Ryder	Ryder	k1gMnSc1	Ryder
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Orgány	orgán	k1gInPc1	orgán
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
konference	konference	k1gFnSc1	konference
práce	práce	k1gFnSc2	práce
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
každoroční	každoroční	k2eAgNnSc4d1	každoroční
setkávání	setkávání	k1gNnSc4	setkávání
představitelů	představitel	k1gMnPc2	představitel
vlád	vláda	k1gFnPc2	vláda
<g/>
,	,	kIx,	,
zaměstnavatelů	zaměstnavatel	k1gMnPc2	zaměstnavatel
a	a	k8xC	a
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Konference	konference	k1gFnSc1	konference
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
rozpočet	rozpočet	k1gInSc4	rozpočet
a	a	k8xC	a
určuje	určovat	k5eAaImIp3nS	určovat
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
pracovní	pracovní	k2eAgInPc4d1	pracovní
standardy	standard	k1gInPc4	standard
<g/>
.	.	kIx.	.
</s>
<s>
Konference	konference	k1gFnSc1	konference
rovněž	rovněž	k9	rovněž
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
fórum	fórum	k1gNnSc1	fórum
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
jsou	být	k5eAaImIp3nP	být
diskutovány	diskutován	k2eAgFnPc4d1	diskutována
sociální	sociální	k2eAgFnPc4d1	sociální
a	a	k8xC	a
pracovní	pracovní	k2eAgFnPc4d1	pracovní
otázky	otázka	k1gFnPc4	otázka
globálního	globální	k2eAgInSc2d1	globální
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Správní	správní	k2eAgFnSc1d1	správní
rada	rada	k1gFnSc1	rada
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nS	scházet
třikrát	třikrát	k6eAd1	třikrát
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
operace	operace	k1gFnSc1	operace
MOP	mop	k1gInSc4	mop
<g/>
,	,	kIx,	,
formuluje	formulovat	k5eAaImIp3nS	formulovat
přístupy	přístup	k1gInPc4	přístup
a	a	k8xC	a
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
vypracovává	vypracovávat	k5eAaImIp3nS	vypracovávat
rozpočet	rozpočet	k1gInSc4	rozpočet
a	a	k8xC	a
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
případy	případ	k1gInPc4	případ
porušování	porušování	k1gNnSc4	porušování
standardů	standard	k1gInPc2	standard
určených	určený	k2eAgInPc2d1	určený
MOP	mop	k1gInSc4	mop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
úřad	úřad	k1gInSc1	úřad
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
BIT	bit	k1gInSc1	bit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stálým	stálý	k2eAgInSc7d1	stálý
sekretariátem	sekretariát	k1gInSc7	sekretariát
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MOP	mop	k1gInSc1	mop
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
2500	[number]	k4	2500
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
působících	působící	k2eAgMnPc2d1	působící
v	v	k7c6	v
ústředí	ústředí	k1gNnSc6	ústředí
a	a	k8xC	a
49	[number]	k4	49
kanceláří	kancelář	k1gFnPc2	kancelář
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úkoly	úkol	k1gInPc4	úkol
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
práce	práce	k1gFnSc2	práce
formuluje	formulovat	k5eAaImIp3nS	formulovat
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
přístupy	přístup	k1gInPc4	přístup
a	a	k8xC	a
programy	program	k1gInPc4	program
přispívající	přispívající	k2eAgFnSc2d1	přispívající
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
pracovních	pracovní	k2eAgFnPc2d1	pracovní
a	a	k8xC	a
životních	životní	k2eAgFnPc2d1	životní
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
pracovní	pracovní	k2eAgInPc4d1	pracovní
standardy	standard	k1gInPc4	standard
sloužící	sloužící	k2eAgMnSc1d1	sloužící
vládám	vláda	k1gFnPc3	vláda
států	stát	k1gInPc2	stát
jako	jako	k8xC	jako
model	model	k1gInSc1	model
při	při	k7c6	při
zavádění	zavádění	k1gNnSc6	zavádění
vlastních	vlastní	k2eAgInPc2d1	vlastní
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
organizuje	organizovat	k5eAaBmIp3nS	organizovat
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
program	program	k1gInSc4	program
technické	technický	k2eAgFnSc2d1	technická
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
pomoci	pomoct	k5eAaPmF	pomoct
vládám	vládat	k5eAaImIp1nS	vládat
zvýšit	zvýšit	k5eAaPmF	zvýšit
efektivnost	efektivnost	k1gFnSc4	efektivnost
nových	nový	k2eAgFnPc2d1	nová
strategií	strategie	k1gFnPc2	strategie
<g/>
,	,	kIx,	,
a	a	k8xC	a
angažuje	angažovat	k5eAaBmIp3nS	angažovat
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
školení	školení	k1gNnSc2	školení
<g/>
,	,	kIx,	,
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
výzkumu	výzkum	k1gInSc2	výzkum
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
dosažení	dosažení	k1gNnSc1	dosažení
pokroku	pokrok	k1gInSc2	pokrok
ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgFnPc6d1	uvedená
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
světových	světový	k2eAgFnPc2d1	světová
organizací	organizace	k1gFnPc2	organizace
hájících	hájící	k2eAgFnPc2d1	hájící
zájmy	zájem	k1gInPc7	zájem
pracujících	pracující	k1gMnPc2	pracující
vyniká	vynikat	k5eAaImIp3nS	vynikat
MOP	mop	k1gInSc4	mop
zejména	zejména	k9	zejména
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zástupci	zástupce	k1gMnPc1	zástupce
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
zaměstnavatelů	zaměstnavatel	k1gMnPc2	zaměstnavatel
mají	mít	k5eAaImIp3nP	mít
při	při	k7c6	při
formulování	formulování	k1gNnSc6	formulování
programů	program	k1gInPc2	program
stejnou	stejný	k2eAgFnSc4d1	stejná
důležitost	důležitost	k1gFnSc4	důležitost
jako	jako	k8xC	jako
vlády	vláda	k1gFnSc2	vláda
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nucená	nucený	k2eAgFnSc1d1	nucená
práce	práce	k1gFnSc1	práce
===	===	k?	===
</s>
</p>
<p>
<s>
Boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
nucené	nucený	k2eAgFnSc3d1	nucená
práci	práce	k1gFnSc3	práce
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
organizaci	organizace	k1gFnSc4	organizace
práce	práce	k1gFnSc2	práce
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
priorit	priorita	k1gFnPc2	priorita
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
byla	být	k5eAaImAgFnS	být
nucená	nucený	k2eAgFnSc1d1	nucená
práce	práce	k1gFnSc1	práce
považována	považován	k2eAgFnSc1d1	považována
zejména	zejména	k9	zejména
za	za	k7c4	za
znak	znak	k1gInSc4	znak
kolonialismu	kolonialismus	k1gInSc2	kolonialismus
a	a	k8xC	a
hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
organizace	organizace	k1gFnSc2	organizace
bylo	být	k5eAaImAgNnS	být
stanovení	stanovení	k1gNnSc1	stanovení
minimálních	minimální	k2eAgInPc2d1	minimální
standardů	standard	k1gInPc2	standard
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nS	by
ochránily	ochránit	k5eAaPmAgFnP	ochránit
původní	původní	k2eAgMnPc4d1	původní
obyvatele	obyvatel	k1gMnPc4	obyvatel
kolonií	kolonie	k1gFnPc2	kolonie
před	před	k7c7	před
ekonomickým	ekonomický	k2eAgNnSc7d1	ekonomické
zneužíváním	zneužívání	k1gNnSc7	zneužívání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
cíl	cíl	k1gInSc1	cíl
organizace	organizace	k1gFnSc2	organizace
změnil	změnit	k5eAaPmAgInS	změnit
–	–	k?	–
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
snažit	snažit	k5eAaImF	snažit
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
univerzálního	univerzální	k2eAgInSc2d1	univerzální
a	a	k8xC	a
jednotného	jednotný	k2eAgInSc2d1	jednotný
standardu	standard	k1gInSc2	standard
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
poznatků	poznatek	k1gInPc2	poznatek
o	o	k7c6	o
různých	různý	k2eAgFnPc6d1	různá
politicky	politicky	k6eAd1	politicky
či	či	k8xC	či
ekonomicky	ekonomicky	k6eAd1	ekonomicky
motivovaných	motivovaný	k2eAgInPc6d1	motivovaný
systémech	systém	k1gInPc6	systém
nucené	nucený	k2eAgFnSc2d1	nucená
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
získaných	získaný	k2eAgInPc2d1	získaný
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
–	–	k?	–
jeho	jeho	k3xOp3gFnSc3	jeho
realizaci	realizace	k1gFnSc3	realizace
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
jak	jak	k6eAd1	jak
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
mnohé	mnohý	k2eAgInPc1d1	mnohý
výjimky	výjimek	k1gInPc1	výjimek
vyžadované	vyžadovaný	k2eAgInPc1d1	vyžadovaný
koloniálními	koloniální	k2eAgFnPc7d1	koloniální
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
snaha	snaha	k1gFnSc1	snaha
zařadit	zařadit	k5eAaPmF	zařadit
pracovní	pracovní	k2eAgInPc4d1	pracovní
standardy	standard	k1gInPc4	standard
mezi	mezi	k7c4	mezi
základní	základní	k2eAgNnPc4d1	základní
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
znesnadňována	znesnadňován	k2eAgNnPc4d1	znesnadňováno
vládami	vláda	k1gFnPc7	vláda
nově	nově	k6eAd1	nově
samostatných	samostatný	k2eAgFnPc2d1	samostatná
postkoloniálních	postkoloniální	k2eAgFnPc2d1	postkoloniální
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
považují	považovat	k5eAaImIp3nP	považovat
možnost	možnost	k1gFnSc4	možnost
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
pracovní	pracovní	k2eAgFnSc4d1	pracovní
sílu	síla	k1gFnSc4	síla
za	za	k7c4	za
nezbytnou	nezbytný	k2eAgFnSc4d1	nezbytná
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
<g/>
-li	i	k?	-li
podporovat	podporovat	k5eAaImF	podporovat
rychlý	rychlý	k2eAgInSc4d1	rychlý
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
růst	růst	k5eAaImF	růst
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
InFocus	InFocus	k1gInSc4	InFocus
====	====	k?	====
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
konferenci	konference	k1gFnSc6	konference
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
konané	konaný	k2eAgFnSc6d1	konaná
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
Prohlášení	prohlášení	k1gNnSc1	prohlášení
MOP	mop	k1gInSc1	mop
o	o	k7c6	o
zásadách	zásada	k1gFnPc6	zásada
a	a	k8xC	a
právech	právo	k1gNnPc6	právo
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
prohlášením	prohlášení	k1gNnSc7	prohlášení
státy	stát	k1gInPc1	stát
přislíbily	přislíbit	k5eAaPmAgInP	přislíbit
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
sdružování	sdružování	k1gNnSc4	sdružování
a	a	k8xC	a
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
kolektivní	kolektivní	k2eAgNnSc4d1	kolektivní
vyjednávání	vyjednávání	k1gNnSc4	vyjednávání
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zavázaly	zavázat	k5eAaPmAgInP	zavázat
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
všech	všecek	k3xTgFnPc2	všecek
forem	forma	k1gFnPc2	forma
nucené	nucený	k2eAgFnSc2d1	nucená
či	či	k8xC	či
povinné	povinný	k2eAgFnSc2d1	povinná
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
zakázání	zakázání	k1gNnSc2	zakázání
dětské	dětský	k2eAgFnSc2d1	dětská
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
odstranění	odstranění	k1gNnSc1	odstranění
všech	všecek	k3xTgFnPc2	všecek
forem	forma	k1gFnPc2	forma
diskriminace	diskriminace	k1gFnSc2	diskriminace
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přijetím	přijetí	k1gNnSc7	přijetí
tohoto	tento	k3xDgNnSc2	tento
prohlášení	prohlášení	k1gNnSc2	prohlášení
zavedla	zavést	k5eAaPmAgFnS	zavést
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
práce	práce	k1gFnSc2	práce
také	také	k9	také
program	program	k1gInSc1	program
InFocus	InFocus	k1gInSc1	InFocus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
procesy	proces	k1gInPc4	proces
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
činnosti	činnost	k1gFnPc4	činnost
technické	technický	k2eAgFnSc2d1	technická
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
souvisí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
realizací	realizace	k1gFnSc7	realizace
tohoto	tento	k3xDgNnSc2	tento
prohlášení	prohlášení	k1gNnSc2	prohlášení
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
má	mít	k5eAaImIp3nS	mít
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
také	také	k9	také
zvyšování	zvyšování	k1gNnSc4	zvyšování
veřejného	veřejný	k2eAgNnSc2d1	veřejné
povědomí	povědomí	k1gNnSc2	povědomí
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
prohlášení	prohlášení	k1gNnSc6	prohlášení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
první	první	k4xOgFnSc1	první
globální	globální	k2eAgFnSc1d1	globální
zpráva	zpráva	k1gFnSc1	zpráva
programu	program	k1gInSc3	program
InFocus	InFocus	k1gMnSc1	InFocus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
základě	základ	k1gInSc6	základ
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Správní	správní	k2eAgFnSc1d1	správní
rada	rada	k1gFnSc1	rada
MOP	mop	k1gInSc4	mop
Speciální	speciální	k2eAgInSc1d1	speciální
akční	akční	k2eAgInSc1d1	akční
program	program	k1gInSc1	program
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
nucené	nucený	k2eAgFnSc3d1	nucená
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
počátku	počátek	k1gInSc2	počátek
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
akční	akční	k2eAgInSc1d1	akční
program	program	k1gInSc1	program
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
zvyšování	zvyšování	k1gNnSc4	zvyšování
veřejného	veřejný	k2eAgNnSc2d1	veřejné
povědomí	povědomí	k1gNnSc2	povědomí
o	o	k7c6	o
různých	různý	k2eAgFnPc6d1	různá
formách	forma	k1gFnPc6	forma
nucené	nucený	k2eAgFnSc2d1	nucená
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
potlačení	potlačení	k1gNnSc2	potlačení
jakýchkoli	jakýkoli	k3yIgFnPc2	jakýkoli
forem	forma	k1gFnPc2	forma
nucené	nucený	k2eAgFnSc2d1	nucená
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zavedení	zavedení	k1gNnSc2	zavedení
akčního	akční	k2eAgInSc2d1	akční
programu	program	k1gInSc2	program
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
několik	několik	k4yIc1	několik
tematicky	tematicky	k6eAd1	tematicky
či	či	k8xC	či
geograficky	geograficky	k6eAd1	geograficky
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
zabývaly	zabývat	k5eAaImAgFnP	zabývat
různými	různý	k2eAgInPc7d1	různý
aspekty	aspekt	k1gInPc7	aspekt
nucené	nucený	k2eAgFnSc2d1	nucená
práce	práce	k1gFnSc2	práce
–	–	k?	–
prací	práce	k1gFnPc2	práce
sloužící	sloužící	k2eAgFnSc1d1	sloužící
k	k	k7c3	k
umoření	umoření	k1gNnSc3	umoření
dluhu	dluh	k1gInSc2	dluh
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
bonded	bonded	k1gMnSc1	bonded
labour	labour	k1gMnSc1	labour
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nevolnictvím	nevolnictví	k1gNnSc7	nevolnictví
<g/>
,	,	kIx,	,
nucenými	nucený	k2eAgFnPc7d1	nucená
pracemi	práce	k1gFnPc7	práce
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
,	,	kIx,	,
nucenými	nucený	k2eAgFnPc7d1	nucená
vězeňskými	vězeňský	k2eAgFnPc7d1	vězeňská
pracemi	práce	k1gFnPc7	práce
<g/>
,	,	kIx,	,
obchodem	obchod	k1gInSc7	obchod
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
jinými	jiný	k2eAgMnPc7d1	jiný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Minimální	minimální	k2eAgFnPc1d1	minimální
mzdy	mzda	k1gFnPc1	mzda
===	===	k?	===
</s>
</p>
<p>
<s>
Právo	právo	k1gNnSc1	právo
pracovníků	pracovník	k1gMnPc2	pracovník
na	na	k7c4	na
ustanovení	ustanovení	k1gNnSc4	ustanovení
minimální	minimální	k2eAgFnSc2d1	minimální
mzdy	mzda	k1gFnSc2	mzda
je	být	k5eAaImIp3nS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
Úmluvami	úmluva	k1gFnPc7	úmluva
MOP	mop	k1gInSc4	mop
č.	č.	k?	č.
26	[number]	k4	26
a	a	k8xC	a
131	[number]	k4	131
o	o	k7c4	o
stanovení	stanovení	k1gNnSc4	stanovení
minimální	minimální	k2eAgFnSc2d1	minimální
mzdy	mzda	k1gFnSc2	mzda
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1928	[number]	k4	1928
a	a	k8xC	a
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Zákony	zákon	k1gInPc1	zákon
o	o	k7c6	o
minimální	minimální	k2eAgFnSc6d1	minimální
mzdě	mzda	k1gFnSc6	mzda
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
většina	většina	k1gFnSc1	většina
vyspělých	vyspělý	k2eAgFnPc2d1	vyspělá
a	a	k8xC	a
středně	středně	k6eAd1	středně
pokročilých	pokročilý	k2eAgFnPc2d1	pokročilá
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
však	však	k9	však
nabírají	nabírat	k5eAaImIp3nP	nabírat
různé	různý	k2eAgFnPc1d1	různá
podoby	podoba	k1gFnPc1	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgNnSc1d1	národní
<g/>
,	,	kIx,	,
vládou	vláda	k1gFnSc7	vláda
stanovená	stanovený	k2eAgFnSc1d1	stanovená
hranice	hranice	k1gFnSc1	hranice
minimální	minimální	k2eAgFnSc2d1	minimální
mzdy	mzda	k1gFnSc2	mzda
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgInSc7	druhý
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
národní	národní	k2eAgNnSc4d1	národní
minimum	minimum	k1gNnSc4	minimum
<g/>
,	,	kIx,	,
stanovené	stanovený	k2eAgInPc1d1	stanovený
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kolektivního	kolektivní	k2eAgNnSc2d1	kolektivní
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
<g/>
,	,	kIx,	,
a	a	k8xC	a
posledním	poslední	k2eAgInSc7d1	poslední
je	být	k5eAaImIp3nS	být
minimální	minimální	k2eAgFnSc1d1	minimální
mzda	mzda	k1gFnSc1	mzda
stanovená	stanovený	k2eAgFnSc1d1	stanovená
kolektivním	kolektivní	k2eAgNnPc3d1	kolektivní
jednáním	jednání	k1gNnPc3	jednání
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
však	však	k9	však
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
odvětví	odvětví	k1gNnSc6	odvětví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
===	===	k?	===
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
práce	práce	k1gFnSc2	práce
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
agenturou	agentura	k1gFnSc7	agentura
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
problematikou	problematika	k1gFnSc7	problematika
přítomnosti	přítomnost	k1gFnSc2	přítomnost
viru	vir	k1gInSc2	vir
HIV	HIV	kA	HIV
na	na	k7c6	na
pracovišti	pracoviště	k1gNnSc6	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
MOP	mop	k1gInSc1	mop
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
vědoma	vědom	k2eAgFnSc1d1	vědoma
<g/>
,	,	kIx,	,
že	že	k8xS	že
vir	vir	k1gInSc1	vir
HIV	HIV	kA	HIV
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
devastující	devastující	k2eAgInSc4d1	devastující
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
pracovní	pracovní	k2eAgFnSc4d1	pracovní
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
produktivitu	produktivita	k1gFnSc4	produktivita
<g/>
,	,	kIx,	,
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
tak	tak	k6eAd1	tak
představovat	představovat	k5eAaImF	představovat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
břemeno	břemeno	k1gNnSc4	břemeno
jak	jak	k8xC	jak
pro	pro	k7c4	pro
pracovníky	pracovník	k1gMnPc4	pracovník
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
rodiny	rodina	k1gFnPc4	rodina
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
MOP	mop	k1gInSc1	mop
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
zabývá	zabývat	k5eAaImIp3nS	zabývat
bojem	boj	k1gInSc7	boj
proti	proti	k7c3	proti
viru	vir	k1gInSc3	vir
HIV	HIV	kA	HIV
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2001	[number]	k4	2001
přijala	přijmout	k5eAaPmAgFnS	přijmout
správní	správní	k2eAgFnSc1d1	správní
rada	rada	k1gFnSc1	rada
MOP	mop	k1gInSc4	mop
Soubor	soubor	k1gInSc1	soubor
pravidel	pravidlo	k1gNnPc2	pravidlo
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
práce	práce	k1gFnSc2	práce
pro	pro	k7c4	pro
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
na	na	k7c6	na
speciálním	speciální	k2eAgNnSc6d1	speciální
zasedání	zasedání	k1gNnSc6	zasedání
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
MOP	mop	k1gInSc1	mop
stala	stát	k5eAaPmAgFnS	stát
spolusponzorem	spolusponzor	k1gMnSc7	spolusponzor
Společného	společný	k2eAgInSc2d1	společný
programu	program	k1gInSc2	program
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
k	k	k7c3	k
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
UNAIDS	UNAIDS	kA	UNAIDS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
99	[number]	k4	99
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
konferenci	konference	k1gFnSc4	konference
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
konané	konaný	k2eAgFnSc6d1	konaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
Doporučení	doporučení	k1gNnSc1	doporučení
MOP	mop	k1gInSc4	mop
k	k	k7c3	k
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
prvním	první	k4xOgInSc7	první
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
pracovním	pracovní	k2eAgInSc7d1	pracovní
standardem	standard	k1gInSc7	standard
<g/>
,	,	kIx,	,
týkajícím	týkající	k2eAgMnSc7d1	týkající
se	se	k3xPyFc4	se
HIV	HIV	kA	HIV
nebo	nebo	k8xC	nebo
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
doporučení	doporučení	k1gNnSc1	doporučení
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
soubor	soubor	k1gInSc4	soubor
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
ochránit	ochránit	k5eAaPmF	ochránit
práva	právo	k1gNnPc4	právo
HIV-pozitivních	HIVozitivní	k2eAgMnPc2d1	HIV-pozitivní
pracovníků	pracovník	k1gMnPc2	pracovník
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
návrhy	návrh	k1gInPc1	návrh
na	na	k7c4	na
zlepšení	zlepšení	k1gNnSc4	zlepšení
prevence	prevence	k1gFnSc2	prevence
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
ILOAIDS	ILOAIDS	kA	ILOAIDS
====	====	k?	====
</s>
</p>
<p>
<s>
Orgánem	orgán	k1gInSc7	orgán
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
tomuto	tento	k3xDgNnSc3	tento
tématu	téma	k1gNnSc3	téma
věnuje	věnovat	k5eAaPmIp3nS	věnovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
agentura	agentura	k1gFnSc1	agentura
ILOAIDS	ILOAIDS	kA	ILOAIDS
<g/>
.	.	kIx.	.
</s>
<s>
ILOAIDS	ILOAIDS	kA	ILOAIDS
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
řadu	řada	k1gFnSc4	řada
funkcí	funkce	k1gFnPc2	funkce
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
poradenství	poradenství	k1gNnSc2	poradenství
<g/>
,	,	kIx,	,
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
technické	technický	k2eAgFnSc2d1	technická
pomoci	pomoc	k1gFnSc2	pomoc
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
problematiky	problematika	k1gFnSc2	problematika
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
a	a	k8xC	a
trhu	trh	k1gInSc2	trh
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
rovněž	rovněž	k9	rovněž
na	na	k7c4	na
propagaci	propagace	k1gFnSc4	propagace
sociální	sociální	k2eAgFnSc2d1	sociální
ochrany	ochrana	k1gFnSc2	ochrana
jako	jako	k8xS	jako
prostředku	prostředek	k1gInSc2	prostředek
ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
nákazy	nákaza	k1gFnSc2	nákaza
virem	vir	k1gInSc7	vir
HIV	HIV	kA	HIV
a	a	k8xC	a
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
pracovníky	pracovník	k1gMnPc4	pracovník
nakažené	nakažený	k2eAgMnPc4d1	nakažený
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
ovlivněné	ovlivněný	k2eAgFnPc1d1	ovlivněná
virem	vir	k1gInSc7	vir
HIV	HIV	kA	HIV
<g/>
.	.	kIx.	.
</s>
<s>
Nynějším	nynější	k2eAgInSc7d1	nynější
projektem	projekt	k1gInSc7	projekt
ILOAIDS	ILOAIDS	kA	ILOAIDS
je	být	k5eAaImIp3nS	být
kampaň	kampaň	k1gFnSc1	kampaň
"	"	kIx"	"
<g/>
Dostaňme	dostat	k5eAaPmRp1nP	dostat
se	se	k3xPyFc4	se
k	k	k7c3	k
nule	nula	k1gFnSc3	nula
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
snížit	snížit	k5eAaPmF	snížit
počet	počet	k1gInSc4	počet
nově	nova	k1gFnSc3	nova
nakažených	nakažený	k2eAgMnPc2d1	nakažený
virem	vir	k1gInSc7	vir
HIV	HIV	kA	HIV
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
úmrtí	úmrť	k1gFnPc2	úmrť
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
a	a	k8xC	a
počet	počet	k1gInSc1	počet
diskriminace	diskriminace	k1gFnSc2	diskriminace
na	na	k7c6	na
základě	základ	k1gInSc6	základ
HIV	HIV	kA	HIV
na	na	k7c4	na
nulu	nula	k1gFnSc4	nula
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
kampaně	kampaň	k1gFnSc2	kampaň
ILOAIDS	ILOAIDS	kA	ILOAIDS
rovněž	rovněž	k9	rovněž
nabízí	nabízet	k5eAaImIp3nS	nabízet
dobrovolné	dobrovolný	k2eAgNnSc4d1	dobrovolné
a	a	k8xC	a
důvěrné	důvěrný	k2eAgNnSc4d1	důvěrné
testování	testování	k1gNnSc4	testování
na	na	k7c6	na
pracovišti	pracoviště	k1gNnSc6	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
iniciativa	iniciativa	k1gFnSc1	iniciativa
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
VTC	VTC	kA	VTC
<g/>
@	@	kIx~	@
<g/>
WORK	WORK	kA	WORK
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Migrující	migrující	k2eAgMnPc1d1	migrující
pracovníci	pracovník	k1gMnPc1	pracovník
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
práv	právo	k1gNnPc2	právo
migrujících	migrující	k2eAgMnPc2d1	migrující
pracovníků	pracovník	k1gMnPc2	pracovník
přijala	přijmout	k5eAaPmAgFnS	přijmout
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
práce	práce	k1gFnSc2	práce
několik	několik	k4yIc4	několik
úmluv	úmluva	k1gFnPc2	úmluva
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Úmluvu	úmluva	k1gFnSc4	úmluva
MOP	mop	k1gInSc4	mop
o	o	k7c6	o
migrujících	migrující	k2eAgMnPc6d1	migrující
pracovnících	pracovník	k1gMnPc6	pracovník
(	(	kIx(	(
<g/>
doplňková	doplňkový	k2eAgNnPc1d1	doplňkové
ustanovení	ustanovení	k1gNnPc1	ustanovení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Úmluvu	úmluva	k1gFnSc4	úmluva
OSN	OSN	kA	OSN
o	o	k7c6	o
právech	právo	k1gNnPc6	právo
migrujících	migrující	k2eAgMnPc2d1	migrující
pracovníků	pracovník	k1gMnPc2	pracovník
a	a	k8xC	a
členů	člen	k1gInPc2	člen
jejich	jejich	k3xOp3gFnPc2	jejich
rodin	rodina	k1gFnPc2	rodina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pracovníci	pracovník	k1gMnPc1	pracovník
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
===	===	k?	===
</s>
</p>
<p>
<s>
Pracovníci	pracovník	k1gMnPc1	pracovník
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
řadu	řada	k1gFnSc4	řada
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
úkolů	úkol	k1gInPc2	úkol
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
ostatních	ostatní	k2eAgMnPc2d1	ostatní
lidí	člověk	k1gMnPc2	člověk
–	–	k?	–
mohou	moct	k5eAaImIp3nP	moct
například	například	k6eAd1	například
vařit	vařit	k5eAaImF	vařit
<g/>
,	,	kIx,	,
uklízet	uklízet	k5eAaImF	uklízet
<g/>
,	,	kIx,	,
či	či	k8xC	či
pečovat	pečovat	k5eAaImF	pečovat
o	o	k7c4	o
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
a	a	k8xC	a
společenských	společenský	k2eAgInPc2d1	společenský
důvodů	důvod	k1gInPc2	důvod
–	–	k?	–
ženy	žena	k1gFnPc1	žena
tradičně	tradičně	k6eAd1	tradičně
vykonávaly	vykonávat	k5eAaImAgFnP	vykonávat
tyto	tento	k3xDgFnPc1	tento
činnosti	činnost	k1gFnPc1	činnost
bez	bez	k7c2	bez
nároku	nárok	k1gInSc2	nárok
na	na	k7c4	na
mzdu	mzda	k1gFnSc4	mzda
–	–	k?	–
tito	tento	k3xDgMnPc1	tento
pracovníci	pracovník	k1gMnPc1	pracovník
často	často	k6eAd1	často
nejsou	být	k5eNaImIp3nP	být
zahrnuti	zahrnout	k5eAaPmNgMnP	zahrnout
do	do	k7c2	do
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
projektů	projekt	k1gInPc2	projekt
sociální	sociální	k2eAgFnSc2d1	sociální
ochrany	ochrana	k1gFnSc2	ochrana
a	a	k8xC	a
ochrany	ochrana	k1gFnSc2	ochrana
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
práv	právo	k1gNnPc2	právo
pracovníků	pracovník	k1gMnPc2	pracovník
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
Úmluva	úmluva	k1gFnSc1	úmluva
MOP	mop	k1gInSc1	mop
o	o	k7c6	o
důstojné	důstojný	k2eAgFnSc6d1	důstojná
práci	práce	k1gFnSc6	práce
pro	pro	k7c4	pro
pracovníky	pracovník	k1gMnPc4	pracovník
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
,	,	kIx,	,
platná	platný	k2eAgFnSc1d1	platná
k	k	k7c3	k
16	[number]	k4	16
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
MOP	mop	k1gInSc1	mop
a	a	k8xC	a
globalizace	globalizace	k1gFnSc1	globalizace
===	===	k?	===
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
práce	práce	k1gFnSc2	práce
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
globalizace	globalizace	k1gFnSc2	globalizace
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
inkluzivní	inkluzivní	k2eAgInSc1d1	inkluzivní
a	a	k8xC	a
demokratický	demokratický	k2eAgInSc1d1	demokratický
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yIgMnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
příležitosti	příležitost	k1gFnPc4	příležitost
a	a	k8xC	a
hmatatelné	hmatatelný	k2eAgFnPc4d1	hmatatelná
výhody	výhoda	k1gFnPc4	výhoda
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
země	zem	k1gFnPc4	zem
a	a	k8xC	a
jedince	jedinec	k1gMnPc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odpovědi	odpověď	k1gFnSc6	odpověď
na	na	k7c4	na
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
multilaterálního	multilaterální	k2eAgInSc2d1	multilaterální
systému	systém	k1gInSc2	systém
MOP	mop	k1gInSc1	mop
neexistuje	existovat	k5eNaImIp3nS	existovat
orgán	orgán	k1gInSc4	orgán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
adekvátně	adekvátně	k6eAd1	adekvátně
pokrýval	pokrývat	k5eAaImAgInS	pokrývat
problematiku	problematika	k1gFnSc4	problematika
sociálního	sociální	k2eAgInSc2d1	sociální
rozměru	rozměr	k1gInSc2	rozměr
globalizace	globalizace	k1gFnSc2	globalizace
<g/>
,	,	kIx,	,
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2002	[number]	k4	2002
Správní	správní	k2eAgFnSc1d1	správní
rada	rada	k1gFnSc1	rada
MOP	mop	k1gInSc1	mop
Světovou	světový	k2eAgFnSc4d1	světová
komisi	komise	k1gFnSc4	komise
pro	pro	k7c4	pro
sociální	sociální	k2eAgInSc4d1	sociální
rozměr	rozměr	k1gInSc4	rozměr
globalizace	globalizace	k1gFnSc2	globalizace
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
této	tento	k3xDgFnSc2	tento
komise	komise	k1gFnSc2	komise
s	s	k7c7	s
názvem	název	k1gInSc7	název
Spravedlivá	spravedlivý	k2eAgFnSc1d1	spravedlivá
globalizace	globalizace	k1gFnSc1	globalizace
<g/>
:	:	kIx,	:
vytvoření	vytvoření	k1gNnSc1	vytvoření
příležitostí	příležitost	k1gFnPc2	příležitost
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
dialog	dialog	k1gInSc4	dialog
mezi	mezi	k7c7	mezi
zástupci	zástupce	k1gMnPc7	zástupce
různých	různý	k2eAgInPc2d1	různý
zájmů	zájem	k1gInPc2	zájem
a	a	k8xC	a
názorů	názor	k1gInPc2	názor
na	na	k7c4	na
sociální	sociální	k2eAgInSc4d1	sociální
rozměr	rozměr	k1gInSc4	rozměr
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zejména	zejména	k9	zejména
nalezení	nalezení	k1gNnSc1	nalezení
kompromisu	kompromis	k1gInSc2	kompromis
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
nejpalčivějších	palčivý	k2eAgFnPc2d3	nejpalčivější
otázek	otázka	k1gFnPc2	otázka
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc4d1	základní
informace	informace	k1gFnPc4	informace
==	==	k?	==
</s>
</p>
<p>
<s>
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
:	:	kIx,	:
Guy	Guy	k1gMnSc1	Guy
Ryder	Ryder	k1gMnSc1	Ryder
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sídlo	sídlo	k1gNnSc1	sídlo
<g/>
:	:	kIx,	:
4	[number]	k4	4
<g/>
,	,	kIx,	,
Route	Rout	k1gInSc5	Rout
des	des	k1gNnPc7	des
Morillons	Morillons	k1gInSc1	Morillons
<g/>
,	,	kIx,	,
CH-1211	CH-1211	k1gFnSc1	CH-1211
Geneva	Geneva	k1gFnSc1	Geneva
22	[number]	k4	22
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
</s>
</p>
<p>
<s>
telefon	telefon	k1gInSc1	telefon
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
41	[number]	k4	41
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
)	)	kIx)	)
799	[number]	k4	799
61	[number]	k4	61
11	[number]	k4	11
<g/>
;	;	kIx,	;
fax	fax	k1gInSc4	fax
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
41	[number]	k4	41
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
)	)	kIx)	)
798	[number]	k4	798
86	[number]	k4	86
85	[number]	k4	85
</s>
</p>
<p>
<s>
e-mail	eail	k1gInSc1	e-mail
<g/>
:	:	kIx,	:
webinfo@hql.ilo.ch	webinfo@hql.ilo.ch	k1gInSc1	webinfo@hql.ilo.ch
<g/>
Rozpočet	rozpočet	k1gInSc1	rozpočet
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2004	[number]	k4	2004
činil	činit	k5eAaImAgInS	činit
přibližně	přibližně	k6eAd1	přibližně
354	[number]	k4	354
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
poštovní	poštovní	k2eAgFnSc1d1	poštovní
správa	správa	k1gFnSc1	správa
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
sekretariátu	sekretariát	k1gInSc2	sekretariát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
vydala	vydat	k5eAaPmAgFnS	vydat
edice	edice	k1gFnSc1	edice
švýcarských	švýcarský	k2eAgFnPc2d1	švýcarská
poštovních	poštovní	k2eAgFnPc2d1	poštovní
známek	známka	k1gFnPc2	známka
s	s	k7c7	s
přetiskem	přetisk	k1gInSc7	přetisk
S.	S.	kA	S.
<g/>
D.N	D.N	k1gFnPc2	D.N
BURÉAU	BURÉAU	kA	BURÉAU
INTERNATIONAL	INTERNATIONAL	kA	INTERNATIONAL
DU	DU	k?	DU
TRAVAIL	TRAVAIL	kA	TRAVAIL
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
přetisků	přetisk	k1gInPc2	přetisk
se	se	k3xPyFc4	se
u	u	k7c2	u
dalších	další	k2eAgFnPc2d1	další
edic	edice	k1gFnPc2	edice
mírně	mírně	k6eAd1	mírně
měnily	měnit	k5eAaImAgFnP	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
byly	být	k5eAaImAgFnP	být
vydány	vydán	k2eAgFnPc1d1	vydána
edice	edice	k1gFnPc1	edice
známek	známka	k1gFnPc2	známka
jen	jen	k6eAd1	jen
tohoto	tento	k3xDgInSc2	tento
úřadu	úřad	k1gInSc2	úřad
–	–	k?	–
známky	známka	k1gFnSc2	známka
ve	v	k7c6	v
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
měně	měna	k1gFnSc6	měna
a	a	k8xC	a
provedení	provedení	k1gNnSc6	provedení
jsou	být	k5eAaImIp3nP	být
opatřeny	opatřen	k2eAgFnPc1d1	opatřena
nápisem	nápis	k1gInSc7	nápis
BUREAU	BUREAU	kA	BUREAU
INTERNATIONAL	INTERNATIONAL	kA	INTERNATIONAL
DU	DU	k?	DU
TRAVAIL	TRAVAIL	kA	TRAVAIL
a	a	k8xC	a
názvem	název	k1gInSc7	název
HELVETIA	Helvetia	k1gFnSc1	Helvetia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
International	International	k1gFnSc2	International
Labour	Labour	k1gMnSc1	Labour
Organization	Organization	k1gInSc1	Organization
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
práce	práce	k1gFnSc2	práce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
OSN	OSN	kA	OSN
</s>
</p>
