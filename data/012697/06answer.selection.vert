<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
MOP	mop	k1gInSc1	mop
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
International	International	k1gMnSc1	International
Labour	Laboura	k1gFnPc2	Laboura
Organization	Organization	k1gInSc1	Organization
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
ILO	ILO	kA	ILO
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
organizace	organizace	k1gFnSc1	organizace
OSN	OSN	kA	OSN
usilující	usilující	k2eAgFnSc1d1	usilující
o	o	k7c6	o
prosazování	prosazování	k1gNnSc6	prosazování
sociální	sociální	k2eAgFnSc2d1	sociální
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaných	uznávaný	k2eAgNnPc2d1	uznávané
pracovních	pracovní	k2eAgNnPc2d1	pracovní
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
