<s>
Glasgow	Glasgow	k1gNnSc1	Glasgow
(	(	kIx(	(
<g/>
Glaschu	Glasch	k1gInSc2	Glasch
skotskou	skotský	k2eAgFnSc7d1	skotská
gaelštinou	gaelština	k1gFnSc7	gaelština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
skotské	skotský	k2eAgNnSc4d1	skotské
město	město	k1gNnSc4	město
a	a	k8xC	a
po	po	k7c6	po
Londýnu	Londýn	k1gInSc3	Londýn
a	a	k8xC	a
Birminghamu	Birmingham	k1gInSc6	Birmingham
třetí	třetí	k4xOgFnSc1	třetí
největší	veliký	k2eAgFnSc1d3	veliký
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nejprůmyslovějším	průmyslový	k2eAgInSc6d3	průmyslový
pásu	pás	k1gInSc6	pás
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
zálivy	záliv	k1gInPc4	záliv
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
firths	firths	k1gInSc1	firths
<g/>
)	)	kIx)	)
Firth	Firth	k1gInSc1	Firth
of	of	k?	of
Clyde	Clyd	k1gInSc5	Clyd
a	a	k8xC	a
Firth	Firth	k1gMnSc1	Firth
of	of	k?	of
Forth	Forth	k1gMnSc1	Forth
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
podle	podle	k7c2	podle
posledních	poslední	k2eAgInPc2d1	poslední
údajů	údaj	k1gInPc2	údaj
620	[number]	k4	620
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Clyde	Clyd	k1gInSc5	Clyd
a	a	k8xC	a
menší	malý	k2eAgFnSc1d2	menší
řeka	řeka	k1gFnSc1	řeka
Kelvin	kelvin	k1gInSc1	kelvin
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Skotska	Skotsko	k1gNnSc2	Skotsko
Edinburghu	Edinburgh	k1gInSc2	Edinburgh
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1175	[number]	k4	1175
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
království	království	k1gNnSc2	království
Strathclyde	Strathclyd	k1gInSc5	Strathclyd
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
fungovalo	fungovat	k5eAaImAgNnS	fungovat
jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
správní	správní	k2eAgInSc1d1	správní
celek	celek	k1gInSc1	celek
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
kraje	kraj	k1gInPc1	kraj
Strathclyde	Strathclyd	k1gInSc5	Strathclyd
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
získává	získávat	k5eAaImIp3nS	získávat
opět	opět	k6eAd1	opět
podle	podle	k7c2	podle
nového	nový	k2eAgNnSc2d1	nové
členění	členění	k1gNnSc2	členění
status	status	k1gInSc1	status
městské	městský	k2eAgFnSc2d1	městská
správní	správní	k2eAgFnSc2d1	správní
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
Glasgow	Glasgow	k1gInSc4	Glasgow
velice	velice	k6eAd1	velice
turisticky	turisticky	k6eAd1	turisticky
populární	populární	k2eAgFnPc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Londýnu	Londýn	k1gInSc3	Londýn
a	a	k8xC	a
Edinburghu	Edinburgha	k1gMnSc4	Edinburgha
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
třetí	třetí	k4xOgNnSc1	třetí
nejnavštěvovanější	navštěvovaný	k2eAgNnSc1d3	nejnavštěvovanější
město	město	k1gNnSc1	město
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
výchozí	výchozí	k2eAgNnSc1d1	výchozí
město	město	k1gNnSc1	město
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
směřují	směřovat	k5eAaImIp3nP	směřovat
za	za	k7c7	za
cíli	cíl	k1gInPc7	cíl
do	do	k7c2	do
celého	celý	k2eAgNnSc2d1	celé
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
vyspělá	vyspělý	k2eAgFnSc1d1	vyspělá
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgInSc1d1	prvotní
Robin	Robina	k1gFnPc2	Robina
počátky	počátek	k1gInPc1	počátek
osady	osada	k1gFnSc2	osada
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Clyde	Clyd	k1gInSc5	Clyd
se	se	k3xPyFc4	se
nesou	nést	k5eAaImIp3nP	nést
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
rybáři	rybář	k1gMnPc7	rybář
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
zde	zde	k6eAd1	zde
založili	založit	k5eAaPmAgMnP	založit
osadu	osada	k1gFnSc4	osada
a	a	k8xC	a
původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
věnovali	věnovat	k5eAaPmAgMnP	věnovat
jen	jen	k9	jen
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
rybolovu	rybolov	k1gInSc2	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
osadníci	osadník	k1gMnPc1	osadník
pustili	pustit	k5eAaPmAgMnP	pustit
do	do	k7c2	do
zemědělství	zemědělství	k1gNnSc2	zemědělství
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
obchodování	obchodování	k1gNnSc6	obchodování
a	a	k8xC	a
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc3	tento
rybářské	rybářský	k2eAgFnSc3d1	rybářská
osadě	osada	k1gFnSc3	osada
ani	ani	k8xC	ani
zdaleka	zdaleka	k6eAd1	zdaleka
nedalo	dát	k5eNaPmAgNnS	dát
říkat	říkat	k5eAaImF	říkat
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
cca	cca	kA	cca
400	[number]	k4	400
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Skotsku	Skotsko	k1gNnSc6	Skotsko
méně	málo	k6eAd2	málo
než	než	k8xS	než
400	[number]	k4	400
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
roztroušených	roztroušený	k2eAgMnPc2d1	roztroušený
po	po	k7c6	po
venkově	venkov	k1gInSc6	venkov
a	a	k8xC	a
živící	živící	k2eAgMnSc1d1	živící
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
a	a	k8xC	a
rybolovem	rybolov	k1gInSc7	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
kusé	kusý	k2eAgInPc1d1	kusý
nálezy	nález	k1gInPc1	nález
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ovšem	ovšem	k9	ovšem
již	již	k6eAd1	již
vyčíst	vyčíst	k5eAaPmF	vyčíst
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
okolo	okolo	k7c2	okolo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Glasgow	Glasgow	k1gNnSc2	Glasgow
uměli	umět	k5eAaImAgMnP	umět
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
zdatně	zdatně	k6eAd1	zdatně
jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pít	pít	k5eAaImF	pít
alkoholické	alkoholický	k2eAgInPc4d1	alkoholický
nápoje	nápoj	k1gInPc4	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Založení	založení	k1gNnSc1	založení
samotného	samotný	k2eAgNnSc2d1	samotné
města	město	k1gNnSc2	město
mělo	mít	k5eAaImAgNnS	mít
nastat	nastat	k5eAaPmF	nastat
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
místní	místní	k2eAgFnSc2d1	místní
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
příchodem	příchod	k1gInSc7	příchod
Svatého	svatý	k1gMnSc2	svatý
Kentigerna	Kentigerna	k1gFnSc1	Kentigerna
(	(	kIx(	(
<g/>
Munga	mungo	k1gMnSc4	mungo
<g/>
)	)	kIx)	)
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
Ten	ten	k3xDgMnSc1	ten
zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgInS	mít
vystavět	vystavět	k5eAaPmF	vystavět
kostel	kostel	k1gInSc1	kostel
u	u	k7c2	u
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
gotická	gotický	k2eAgFnSc1d1	gotická
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
zde	zde	k6eAd1	zde
stojí	stát	k5eAaImIp3nS	stát
od	od	k7c2	od
třináctého	třináctý	k4xOgNnSc2	třináctý
století	století	k1gNnSc2	století
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
originální	originální	k2eAgFnSc1d1	originální
(	(	kIx(	(
<g/>
tématem	téma	k1gNnSc7	téma
<g/>
)	)	kIx)	)
skotská	skotský	k2eAgFnSc1d1	skotská
výzdoba	výzdoba	k1gFnSc1	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
mezníkem	mezník	k1gInSc7	mezník
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Glasgow	Glasgow	k1gInSc1	Glasgow
je	být	k5eAaImIp3nS	být
rok	rok	k1gInSc4	rok
1451	[number]	k4	1451
-	-	kIx~	-
vznik	vznik	k1gInSc1	vznik
místní	místní	k2eAgFnSc2d1	místní
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
také	také	k9	také
nejstarší	starý	k2eAgInSc1d3	nejstarší
dům	dům	k1gInSc1	dům
v	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
stojí	stát	k5eAaImIp3nS	stát
naproti	naproti	k7c3	naproti
katedrále	katedrála	k1gFnSc3	katedrála
a	a	k8xC	a
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zachránit	zachránit	k5eAaPmF	zachránit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
před	před	k7c7	před
demolicí	demolice	k1gFnSc7	demolice
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
vzniku	vznik	k1gInSc2	vznik
univerzity	univerzita	k1gFnSc2	univerzita
po	po	k7c4	po
konec	konec	k1gInSc4	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
víceméně	víceméně	k9	víceméně
stagnuje	stagnovat	k5eAaImIp3nS	stagnovat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
poté	poté	k6eAd1	poté
začíná	začínat	k5eAaImIp3nS	začínat
vzkvétat	vzkvétat	k5eAaImF	vzkvétat
zámořský	zámořský	k2eAgInSc4d1	zámořský
obchod	obchod	k1gInSc4	obchod
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
předzvěstí	předzvěst	k1gFnSc7	předzvěst
budoucího	budoucí	k2eAgInSc2d1	budoucí
rozmachu	rozmach	k1gInSc2	rozmach
<g/>
.	.	kIx.	.
</s>
<s>
Glasgow	Glasgow	k1gNnSc1	Glasgow
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
pozornosti	pozornost	k1gFnSc2	pozornost
světa	svět	k1gInSc2	svět
jako	jako	k8xS	jako
centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
cukrem	cukr	k1gInSc7	cukr
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
se	s	k7c7	s
Severní	severní	k2eAgFnSc7d1	severní
Amerikou	Amerika	k1gFnSc7	Amerika
a	a	k8xC	a
Západní	západní	k2eAgFnSc7d1	západní
Indií	Indie	k1gFnSc7	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
tehdy	tehdy	k6eAd1	tehdy
neméně	málo	k6eNd2	málo
zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
obchodním	obchodní	k2eAgInSc7d1	obchodní
artiklem	artikl	k1gInSc7	artikl
pro	pro	k7c4	pro
Glasgow	Glasgow	k1gInSc4	Glasgow
byl	být	k5eAaImAgMnS	být
tabák	tabák	k1gInSc4	tabák
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Obchodu	obchod	k1gInSc3	obchod
se	s	k7c7	s
zámořím	zámoří	k1gNnSc7	zámoří
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
přizpůsobilo	přizpůsobit	k5eAaPmAgNnS	přizpůsobit
výstavbou	výstavba	k1gFnSc7	výstavba
velkého	velký	k2eAgInSc2d1	velký
přístavu	přístav	k1gInSc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozmohl	rozmoct	k5eAaPmAgInS	rozmoct
i	i	k9	i
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
textilem	textil	k1gInSc7	textil
<g/>
,	,	kIx,	,
barvami	barva	k1gFnPc7	barva
a	a	k8xC	a
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
staví	stavit	k5eAaPmIp3nS	stavit
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
mnoho	mnoho	k4c1	mnoho
obchodnických	obchodnický	k2eAgInPc2d1	obchodnický
domů	dům	k1gInPc2	dům
v	v	k7c6	v
klasickém	klasický	k2eAgInSc6d1	klasický
viktoriánském	viktoriánský	k2eAgInSc6d1	viktoriánský
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
již	již	k9	již
ovládá	ovládat	k5eAaImIp3nS	ovládat
Glasgow	Glasgow	k1gInSc4	Glasgow
zejména	zejména	k9	zejména
těžký	těžký	k2eAgInSc4d1	těžký
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
a	a	k8xC	a
cukrovarnických	cukrovarnický	k2eAgNnPc6d1	cukrovarnické
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
i	i	k9	i
další	další	k2eAgFnPc4d1	další
věci	věc	k1gFnPc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Glasgow	Glasgow	k1gNnSc1	Glasgow
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
nejvíce	hodně	k6eAd3	hodně
rozvíjejícím	rozvíjející	k2eAgMnSc7d1	rozvíjející
se	se	k3xPyFc4	se
viktoriánským	viktoriánský	k2eAgNnSc7d1	viktoriánské
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
mnoho	mnoho	k4c1	mnoho
imigračních	imigrační	k2eAgFnPc2d1	imigrační
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
<g/>
,	,	kIx,	,
z	z	k7c2	z
tehdy	tehdy	k6eAd1	tehdy
chudé	chudý	k2eAgFnSc2d1	chudá
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přicházejí	přicházet	k5eAaImIp3nP	přicházet
také	také	k9	také
početné	početný	k2eAgFnPc1d1	početná
skupiny	skupina	k1gFnPc1	skupina
Židovských	židovský	k2eAgFnPc2d1	židovská
rodin	rodina	k1gFnPc2	rodina
z	z	k7c2	z
východu	východ	k1gInSc2	východ
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nemalá	malý	k2eNgFnSc1d1	nemalá
je	být	k5eAaImIp3nS	být
i	i	k9	i
populace	populace	k1gFnSc1	populace
Irů	Ir	k1gMnPc2	Ir
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
usazuje	usazovat	k5eAaImIp3nS	usazovat
zejména	zejména	k9	zejména
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
East	East	k1gMnSc1	East
End	End	k1gMnSc1	End
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
města	město	k1gNnSc2	město
přichází	přicházet	k5eAaImIp3nS	přicházet
rozmach	rozmach	k1gInSc1	rozmach
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
,	,	kIx,	,
nemocí	nemoc	k1gFnPc2	nemoc
a	a	k8xC	a
sociálních	sociální	k2eAgInPc2d1	sociální
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
chudé	chudý	k2eAgMnPc4d1	chudý
dělníky	dělník	k1gMnPc4	dělník
se	se	k3xPyFc4	se
staví	stavit	k5eAaBmIp3nS	stavit
narychlo	narychlo	k6eAd1	narychlo
vybudované	vybudovaný	k2eAgInPc4d1	vybudovaný
domy	dům	k1gInPc4	dům
a	a	k8xC	a
celé	celý	k2eAgFnPc4d1	celá
městské	městský	k2eAgFnPc4d1	městská
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
kulturního	kulturní	k2eAgNnSc2d1	kulturní
a	a	k8xC	a
sociálního	sociální	k2eAgNnSc2d1	sociální
zázemí	zázemí	k1gNnSc2	zázemí
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
zejména	zejména	k9	zejména
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
východ	východ	k1gInSc4	východ
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Bohatší	bohatý	k2eAgMnPc1d2	bohatší
obyvatelé	obyvatel	k1gMnPc1	obyvatel
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
soustřeďují	soustřeďovat	k5eAaImIp3nP	soustřeďovat
okolo	okolo	k7c2	okolo
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
v	v	k7c6	v
části	část	k1gFnSc6	část
Kelvinside	Kelvinsid	k1gInSc5	Kelvinsid
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
řeky	řeka	k1gFnSc2	řeka
Kelvin	kelvin	k1gInSc4	kelvin
a	a	k8xC	a
Glasgowské	glasgowský	k2eAgFnPc4d1	Glasgowská
botanické	botanický	k2eAgFnPc4d1	botanická
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnPc4d1	založená
roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
a	a	k8xC	a
dostavěné	dostavěný	k2eAgInPc1d1	dostavěný
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
éře	éra	k1gFnSc6	éra
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
populace	populace	k1gFnSc1	populace
až	až	k9	až
miliónu	milión	k4xCgInSc2	milión
(	(	kIx(	(
<g/>
1	[number]	k4	1
088	[number]	k4	088
000	[number]	k4	000
r.	r.	kA	r.
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInSc1d2	pozdější
úbytek	úbytek	k1gInSc1	úbytek
obyvatel	obyvatel	k1gMnPc2	obyvatel
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
výstavba	výstavba	k1gFnSc1	výstavba
tzv.	tzv.	kA	tzv.
satelitních	satelitní	k2eAgNnPc2d1	satelitní
měst	město	k1gNnPc2	město
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Glasgow	Glasgow	k1gNnSc2	Glasgow
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
stavby	stavba	k1gFnPc4	stavba
jako	jako	k8xC	jako
kanál	kanál	k1gInSc4	kanál
Clyde	Clyd	k1gInSc5	Clyd
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
muzea	muzeum	k1gNnSc2	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
po	po	k7c6	po
tříleté	tříletý	k2eAgFnSc6d1	tříletá
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
znovu	znovu	k6eAd1	znovu
otevřená	otevřený	k2eAgNnPc1d1	otevřené
(	(	kIx(	(
<g/>
v	v	k7c6	v
r.	r.	kA	r.
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
galerie	galerie	k1gFnSc2	galerie
Kelvingrove	Kelvingrov	k1gInSc5	Kelvingrov
a	a	k8xC	a
také	také	k9	také
náměstí	náměstí	k1gNnSc1	náměstí
se	s	k7c7	s
zdejší	zdejší	k2eAgFnSc7d1	zdejší
radnicí	radnice	k1gFnSc7	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
místního	místní	k2eAgMnSc2d1	místní
rodáka	rodák	k1gMnSc2	rodák
Charlese	Charles	k1gMnSc2	Charles
Mackintoshe	Mackintosh	k1gFnSc2	Mackintosh
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
nezaměnitelného	zaměnitelný	k2eNgInSc2d1	nezaměnitelný
moderního	moderní	k2eAgInSc2d1	moderní
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
architektury	architektura	k1gFnSc2	architektura
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
připomenout	připomenout	k5eAaPmF	připomenout
ještě	ještě	k9	ještě
moderní	moderní	k2eAgFnSc2d1	moderní
stavby	stavba	k1gFnSc2	stavba
jako	jako	k8xC	jako
SECC	secco	k1gNnPc2	secco
-	-	kIx~	-
kulturní	kulturní	k2eAgFnSc1d1	kulturní
a	a	k8xC	a
konferenční	konferenční	k2eAgNnSc1d1	konferenční
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
glasgowské	glasgowský	k2eAgNnSc1d1	glasgowské
letiště	letiště	k1gNnSc1	letiště
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgNnSc1d1	dnešní
Glasgow	Glasgow	k1gNnSc1	Glasgow
je	být	k5eAaImIp3nS	být
kosmopolitním	kosmopolitní	k2eAgNnSc7d1	kosmopolitní
místem	místo	k1gNnSc7	místo
mnoha	mnoho	k4c2	mnoho
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
staví	stavit	k5eAaImIp3nS	stavit
mnoho	mnoho	k4c1	mnoho
moderních	moderní	k2eAgFnPc2d1	moderní
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Glasgow	Glasgow	k1gNnSc6	Glasgow
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgInPc1d1	patrný
rozdíly	rozdíl	k1gInPc1	rozdíl
z	z	k7c2	z
předešlých	předešlý	k2eAgNnPc2d1	předešlé
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
<g/>
zmizel	zmizet	k5eAaPmAgInS	zmizet
špinavý	špinavý	k2eAgInSc4d1	špinavý
přístav	přístav	k1gInSc4	přístav
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
něhož	jenž	k3xRgInSc2	jenž
přichází	přicházet	k5eAaImIp3nS	přicházet
moderní	moderní	k2eAgFnSc1d1	moderní
zástavba	zástavba	k1gFnSc1	zástavba
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
kulturně	kulturně	k6eAd1	kulturně
vzkvétá	vzkvétat	k5eAaImIp3nS	vzkvétat
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
kvalita	kvalita	k1gFnSc1	kvalita
vzdělání	vzdělání	k1gNnSc2	vzdělání
atd.	atd.	kA	atd.
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
problémů	problém	k1gInPc2	problém
současnosti	současnost	k1gFnSc2	současnost
je	být	k5eAaImIp3nS	být
vysoké	vysoký	k2eAgNnSc1d1	vysoké
procento	procento	k1gNnSc1	procento
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Bohatá	bohatý	k2eAgFnSc1d1	bohatá
historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgInSc1d1	kulturní
potenciál	potenciál	k1gInSc1	potenciál
<g/>
,	,	kIx,	,
vyspělé	vyspělý	k2eAgNnSc1d1	vyspělé
finančnictví	finančnictví	k1gNnSc1	finančnictví
a	a	k8xC	a
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
frekvencí	frekvence	k1gFnSc7	frekvence
modernizace	modernizace	k1gFnSc2	modernizace
<g/>
,	,	kIx,	,
dělá	dělat	k5eAaImIp3nS	dělat
z	z	k7c2	z
Glasgow	Glasgow	k1gNnSc2	Glasgow
atraktivní	atraktivní	k2eAgNnSc1d1	atraktivní
město	město	k1gNnSc1	město
západního	západní	k2eAgInSc2d1	západní
stylu	styl	k1gInSc2	styl
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
odvětvích	odvětví	k1gNnPc6	odvětví
druhé	druhý	k4xOgFnSc6	druhý
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
po	po	k7c6	po
Londýnu	Londýn	k1gInSc3	Londýn
<g/>
.	.	kIx.	.
15,4	[number]	k4	15,4
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
minority	minorita	k1gFnSc2	minorita
<g/>
.	.	kIx.	.
8,1	[number]	k4	8,1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nS	tvořit
Asiaté	Asiat	k1gMnPc1	Asiat
(	(	kIx(	(
<g/>
Pákistánci	Pákistánec	k1gMnPc1	Pákistánec
<g/>
,	,	kIx,	,
Indové	Ind	k1gMnPc1	Ind
<g/>
,	,	kIx,	,
Bangladéšané	Bangladéšan	k1gMnPc1	Bangladéšan
<g/>
)	)	kIx)	)
2.4	[number]	k4	2.4
<g/>
%	%	kIx~	%
lidé	člověk	k1gMnPc1	člověk
tmavé	tmavý	k2eAgFnSc2d1	tmavá
pleti	pleť	k1gFnSc2	pleť
<g/>
,	,	kIx,	,
Afričané	Afričan	k1gMnPc1	Afričan
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
Karibiku	Karibik	k1gInSc2	Karibik
1.42	[number]	k4	1.42
<g/>
%	%	kIx~	%
Poláci	Polák	k1gMnPc1	Polák
0,64	[number]	k4	0,64
<g/>
%	%	kIx~	%
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
ostatní	ostatní	k2eAgInPc1d1	ostatní
národy	národ	k1gInPc1	národ
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Arabů	Arab	k1gMnPc2	Arab
Glasgow	Glasgow	k1gInSc1	Glasgow
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
přívržencem	přívrženec	k1gMnSc7	přívrženec
sociální	sociální	k2eAgFnSc2d1	sociální
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
30	[number]	k4	30
let	léto	k1gNnPc2	léto
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
'	'	kIx"	'
<g/>
Labouristů	labourista	k1gMnPc2	labourista
<g/>
'	'	kIx"	'
-	-	kIx~	-
Labour	Labour	k1gMnSc1	Labour
Party	parta	k1gFnSc2	parta
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
sociálního	sociální	k2eAgNnSc2d1	sociální
smýšlení	smýšlení	k1gNnSc2	smýšlení
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
přívrženců	přívrženec	k1gMnPc2	přívrženec
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
obyčejných	obyčejný	k2eAgMnPc2d1	obyčejný
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
docházelo	docházet	k5eAaImAgNnS	docházet
často	často	k6eAd1	často
k	k	k7c3	k
politickým	politický	k2eAgFnPc3d1	politická
manifestacím	manifestace	k1gFnPc3	manifestace
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Velké	velký	k2eAgFnSc2d1	velká
říjnové	říjnový	k2eAgFnSc2d1	říjnová
revoluce	revoluce	k1gFnSc2	revoluce
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
příčinou	příčina	k1gFnSc7	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
pak	pak	k6eAd1	pak
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1919	[number]	k4	1919
a	a	k8xC	a
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byly	být	k5eAaImAgFnP	být
rozepře	rozepře	k1gFnPc1	rozepře
mezi	mezi	k7c7	mezi
nájemníky	nájemník	k1gMnPc7	nájemník
a	a	k8xC	a
majiteli	majitel	k1gMnPc7	majitel
domů	dům	k1gInPc2	dům
o	o	k7c4	o
výši	výše	k1gFnSc4	výše
nájemného	nájemné	k1gNnSc2	nájemné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
zasahovala	zasahovat	k5eAaImAgFnS	zasahovat
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
79	[number]	k4	79
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
politické	politický	k2eAgNnSc4d1	politické
členění	členění	k1gNnSc4	členění
rady	rada	k1gFnSc2	rada
následující	následující	k2eAgFnSc2d1	následující
:	:	kIx,	:
45	[number]	k4	45
Labouristů	labourista	k1gMnPc2	labourista
<g/>
,	,	kIx,	,
22	[number]	k4	22
Skotská	skotský	k2eAgFnSc1d1	skotská
národní	národní	k2eAgFnSc1d1	národní
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
5	[number]	k4	5
Zelení	zeleň	k1gFnPc2	zeleň
<g/>
,	,	kIx,	,
5	[number]	k4	5
Liberální	liberální	k2eAgMnPc1d1	liberální
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
,	,	kIx,	,
1	[number]	k4	1
Konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
a	a	k8xC	a
1	[number]	k4	1
zástupce	zástupce	k1gMnSc1	zástupce
Solidarity	solidarita	k1gFnSc2	solidarita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
krom	krom	k7c2	krom
Glasgowské	glasgowský	k2eAgFnSc2d1	Glasgowská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
Kaledonské	Kaledonský	k2eAgFnSc2d1	Kaledonská
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
Strathclydské	Strathclydský	k2eAgFnSc2d1	Strathclydský
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
několik	několik	k4yIc4	několik
středních	střední	k2eAgFnPc2d1	střední
soukromých	soukromý	k2eAgFnPc2d1	soukromá
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgInPc6	který
lze	lze	k6eAd1	lze
studovat	studovat	k5eAaImF	studovat
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
škálu	škála	k1gFnSc4	škála
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Glasgow	Glasgow	k1gNnSc1	Glasgow
je	být	k5eAaImIp3nS	být
také	také	k9	také
častým	častý	k2eAgInSc7d1	častý
cílem	cíl	k1gInSc7	cíl
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
studentů	student	k1gMnPc2	student
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třeba	třeba	k6eAd1	třeba
i	i	k9	i
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělanost	vzdělanost	k1gFnSc1	vzdělanost
Glasgowčanů	Glasgowčan	k1gMnPc2	Glasgowčan
je	být	k5eAaImIp3nS	být
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
netýká	týkat	k5eNaImIp3nS	týkat
většiny	většina	k1gFnSc2	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
ze	z	k7c2	z
sociálně	sociálně	k6eAd1	sociálně
slabších	slabý	k2eAgFnPc2d2	slabší
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
nejenom	nejenom	k6eAd1	nejenom
jich	on	k3xPp3gInPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Imigrace	imigrace	k1gFnSc1	imigrace
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
Glasgow	Glasgow	k1gInSc4	Glasgow
velmi	velmi	k6eAd1	velmi
aktuální	aktuální	k2eAgNnSc4d1	aktuální
téma	téma	k1gNnSc4	téma
nejméně	málo	k6eAd3	málo
200	[number]	k4	200
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sem	sem	k6eAd1	sem
proudily	proudit	k5eAaPmAgInP	proudit
již	již	k6eAd1	již
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
skupiny	skupina	k1gFnSc2	skupina
Italů	Ital	k1gMnPc2	Ital
<g/>
,	,	kIx,	,
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
Irů	Ir	k1gMnPc2	Ir
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
imigrantů	imigrant	k1gMnPc2	imigrant
z	z	k7c2	z
asijských	asijský	k2eAgFnPc2d1	asijská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
první	první	k4xOgFnSc2	první
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
víceméně	víceméně	k9	víceméně
dokonale	dokonale	k6eAd1	dokonale
asimilovala	asimilovat	k5eAaBmAgFnS	asimilovat
<g/>
,	,	kIx,	,
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
své	svůj	k3xOyFgFnSc2	svůj
komunity	komunita	k1gFnSc2	komunita
a	a	k8xC	a
kromě	kromě	k7c2	kromě
uchování	uchování	k1gNnSc2	uchování
mateřského	mateřský	k2eAgInSc2d1	mateřský
jazyka	jazyk	k1gInSc2	jazyk
si	se	k3xPyFc3	se
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
i	i	k9	i
své	svůj	k3xOyFgInPc4	svůj
zvyky	zvyk	k1gInPc4	zvyk
a	a	k8xC	a
obyčeje	obyčej	k1gInPc4	obyčej
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgInSc7	ten
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
i	i	k9	i
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
a	a	k8xC	a
provozování	provozování	k1gNnSc1	provozování
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
obchodů	obchod	k1gInPc2	obchod
a	a	k8xC	a
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
právě	právě	k9	právě
na	na	k7c4	na
speciality	specialita	k1gFnPc4	specialita
uvedených	uvedený	k2eAgFnPc2d1	uvedená
komunit	komunita	k1gFnPc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
přišlo	přijít	k5eAaPmAgNnS	přijít
do	do	k7c2	do
města	město	k1gNnSc2	město
mnoho	mnoho	k6eAd1	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
bývalého	bývalý	k2eAgInSc2d1	bývalý
Východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
Pobaltských	pobaltský	k2eAgInPc2d1	pobaltský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Česka	Česko	k1gNnSc2	Česko
a	a	k8xC	a
od	od	k7c2	od
vstupu	vstup	k1gInSc2	vstup
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
do	do	k7c2	do
EU	EU	kA	EU
<g/>
,	,	kIx,	,
také	také	k9	také
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
příchozích	příchozí	k1gMnPc2	příchozí
z	z	k7c2	z
tehdy	tehdy	k6eAd1	tehdy
nových	nový	k2eAgInPc2d1	nový
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
jsou	být	k5eAaImIp3nP	být
víceméně	víceméně	k9	víceméně
migranti	migrant	k1gMnPc1	migrant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
dominují	dominovat	k5eAaImIp3nP	dominovat
především	především	k9	především
drogová	drogový	k2eAgFnSc1d1	drogová
kriminalita	kriminalita	k1gFnSc1	kriminalita
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
tzn.	tzn.	kA	tzn.
'	'	kIx"	'
<g/>
knife	knife	k?	knife
crime	crimat	k5eAaPmIp3nS	crimat
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
-	-	kIx~	-
kriminalita	kriminalita	k1gFnSc1	kriminalita
nožů	nůž	k1gInPc2	nůž
a	a	k8xC	a
vandalismus	vandalismus	k1gInSc4	vandalismus
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
nošení	nošení	k1gNnSc4	nošení
nožů	nůž	k1gInPc2	nůž
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
zakázáno	zakázán	k2eAgNnSc4d1	zakázáno
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc4	množství
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
mladých	mladý	k2eAgMnPc2d1	mladý
<g/>
)	)	kIx)	)
lidí	člověk	k1gMnPc2	člověk
toho	ten	k3xDgNnSc2	ten
nedbá	nedbat	k5eAaImIp3nS	nedbat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
médiích	médium	k1gNnPc6	médium
probíhají	probíhat	k5eAaImIp3nP	probíhat
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
kampaně	kampaň	k1gFnPc1	kampaň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
snížit	snížit	k5eAaPmF	snížit
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
kriminálních	kriminální	k2eAgInPc2d1	kriminální
činů	čin	k1gInPc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Pouliční	pouliční	k2eAgFnSc1d1	pouliční
prostituce	prostituce	k1gFnSc1	prostituce
je	být	k5eAaImIp3nS	být
zakázána	zakázán	k2eAgFnSc1d1	zakázána
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
nabídka	nabídka	k1gFnSc1	nabídka
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
poptávka	poptávka	k1gFnSc1	poptávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
kriminalita	kriminalita	k1gFnSc1	kriminalita
v	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
velice	velice	k6eAd1	velice
závažným	závažný	k2eAgInSc7d1	závažný
problémem	problém	k1gInSc7	problém
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
války	válka	k1gFnSc2	válka
gangů	gang	k1gInPc2	gang
byly	být	k5eAaImAgInP	být
takřka	takřka	k6eAd1	takřka
na	na	k7c6	na
denním	denní	k2eAgInSc6d1	denní
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Glasgow	Glasgow	k1gNnSc1	Glasgow
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
Chicago	Chicago	k1gNnSc4	Chicago
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
části	část	k1gFnSc2	část
jako	jako	k8xC	jako
Govan	Govana	k1gFnPc2	Govana
<g/>
,	,	kIx,	,
Tollcross	Tollcrossa	k1gFnPc2	Tollcrossa
<g/>
,	,	kIx,	,
či	či	k8xC	či
Bridgeton	Bridgeton	k1gInSc4	Bridgeton
neměly	mít	k5eNaImAgFnP	mít
o	o	k7c4	o
časté	častý	k2eAgInPc4d1	častý
jevy	jev	k1gInPc4	jev
kriminality	kriminalita	k1gFnSc2	kriminalita
nouzi	nouze	k1gFnSc3	nouze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztržkách	roztržka	k1gFnPc6	roztržka
skupin	skupina	k1gFnPc2	skupina
se	se	k3xPyFc4	se
často	často	k6eAd1	často
angažovali	angažovat	k5eAaBmAgMnP	angažovat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
vydobyli	vydobýt	k5eAaPmAgMnP	vydobýt
práva	práv	k2eAgMnSc4d1	práv
vůdce	vůdce	k1gMnSc4	vůdce
a	a	k8xC	a
gangy	gang	k1gInPc4	gang
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
měly	mít	k5eAaImAgFnP	mít
svou	svůj	k3xOyFgFnSc4	svůj
hierarchii	hierarchie	k1gFnSc4	hierarchie
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
namátkou	namátkou	k6eAd1	namátkou
lze	lze	k6eAd1	lze
zmínit	zmínit	k5eAaPmF	zmínit
Billyho	Billy	k1gMnSc4	Billy
Fullertona	Fullerton	k1gMnSc4	Fullerton
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
dát	dát	k5eAaPmF	dát
v	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dohromady	dohromady	k6eAd1	dohromady
celou	celý	k2eAgFnSc4d1	celá
malou	malý	k2eAgFnSc4d1	malá
armádu	armáda	k1gFnSc4	armáda
gangsterů	gangster	k1gMnPc2	gangster
z	z	k7c2	z
chudých	chudý	k2eAgFnPc2d1	chudá
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Brigton	Brigton	k1gInSc1	Brigton
Billy	Bill	k1gMnPc4	Bill
Boys	boy	k1gMnPc2	boy
a	a	k8xC	a
zaměstnávat	zaměstnávat	k5eAaImF	zaměstnávat
tak	tak	k9	tak
na	na	k7c4	na
dlouho	dlouho	k6eAd1	dlouho
místní	místní	k2eAgFnSc4d1	místní
policii	policie	k1gFnSc4	policie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
potyček	potyčka	k1gFnPc2	potyčka
byli	být	k5eAaImAgMnP	být
angažováni	angažován	k2eAgMnPc1d1	angažován
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
i	i	k9	i
nezletilí	zletilý	k2eNgMnPc1d1	nezletilý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
doslova	doslova	k6eAd1	doslova
multikulturní	multikulturní	k2eAgMnSc1d1	multikulturní
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
zde	zde	k6eAd1	zde
nalézt	nalézt	k5eAaBmF	nalézt
mnoho	mnoho	k4c1	mnoho
typů	typ	k1gInPc2	typ
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
kin	kino	k1gNnPc2	kino
i	i	k8xC	i
speciálních	speciální	k2eAgFnPc2d1	speciální
kulturních	kulturní	k2eAgFnPc2d1	kulturní
kaváren	kavárna	k1gFnPc2	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Glasgow	Glasgow	k1gNnSc2	Glasgow
přilétává	přilétávat	k5eAaImIp3nS	přilétávat
koncertovat	koncertovat	k5eAaImF	koncertovat
nemálo	málo	k6eNd1	málo
slavných	slavný	k2eAgFnPc2d1	slavná
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
zpěvaček	zpěvačka	k1gFnPc2	zpěvačka
a	a	k8xC	a
zpěváků	zpěvák	k1gMnPc2	zpěvák
<g/>
,	,	kIx,	,
zastupující	zastupující	k2eAgInPc1d1	zastupující
celou	celý	k2eAgFnSc4d1	celá
škálu	škála	k1gFnSc4	škála
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
koncerty	koncert	k1gInPc1	koncert
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
konají	konat	k5eAaImIp3nP	konat
ve	v	k7c6	v
zmiňovaném	zmiňovaný	k2eAgNnSc6d1	zmiňované
centru	centrum	k1gNnSc6	centrum
SECC	secco	k1gNnPc2	secco
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
otevřené	otevřený	k2eAgFnSc6d1	otevřená
scéně	scéna	k1gFnSc6	scéna
na	na	k7c6	na
Národním	národní	k2eAgInSc6d1	národní
stadiónu	stadión	k1gInSc6	stadión
Hampden	Hampdna	k1gFnPc2	Hampdna
Park	park	k1gInSc4	park
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
představily	představit	k5eAaPmAgFnP	představit
skupiny	skupina	k1gFnPc1	skupina
jako	jako	k9	jako
třeba	třeba	k6eAd1	třeba
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
sále	sál	k1gInSc6	sál
Barrowland	Barrowlanda	k1gFnPc2	Barrowlanda
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
svá	svůj	k3xOyFgNnPc4	svůj
centra	centrum	k1gNnPc4	centrum
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
Čínské	čínský	k2eAgNnSc1d1	čínské
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
China	China	k1gFnSc1	China
town	town	k1gMnSc1	town
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
několik	několik	k4yIc4	několik
mešit	mešita	k1gFnPc2	mešita
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Glasgowská	glasgowský	k2eAgFnSc1d1	Glasgowská
ústřední	ústřední	k2eAgFnSc1d1	ústřední
mešita	mešita	k1gFnSc1	mešita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
galerií	galerie	k1gFnPc2	galerie
<g/>
,	,	kIx,	,
jako	jako	k9	jako
např.	např.	kA	např.
zmiňované	zmiňovaný	k2eAgNnSc1d1	zmiňované
Muzeum	muzeum	k1gNnSc1	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Kelvingrove	Kelvingrov	k1gInSc5	Kelvingrov
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
(	(	kIx(	(
<g/>
St.	st.	kA	st.
<g/>
Mungo	mungo	k1gMnSc1	mungo
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Religious	Religious	k1gMnSc1	Religious
Life	Lif	k1gFnSc2	Lif
and	and	k?	and
Art	Art	k1gFnSc2	Art
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Transport	transport	k1gInSc1	transport
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
je	být	k5eAaImIp3nS	být
také	také	k9	také
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
udržovaných	udržovaný	k2eAgInPc2d1	udržovaný
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
konají	konat	k5eAaImIp3nP	konat
kulturní	kulturní	k2eAgFnPc4d1	kulturní
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
všechny	všechen	k3xTgInPc4	všechen
aspoň	aspoň	k9	aspoň
letní	letní	k2eAgInSc1d1	letní
Dudácký	dudácký	k2eAgInSc1d1	dudácký
festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
Pipe	Pipe	k1gInSc1	Pipe
Festival	festival	k1gInSc1	festival
on	on	k3xPp3gMnSc1	on
Glasgow	Glasgow	k1gNnSc1	Glasgow
Green	Greno	k1gNnPc2	Greno
<g/>
)	)	kIx)	)
v	v	k7c6	v
parku	park	k1gInSc6	park
Glasgow	Glasgow	k1gNnSc1	Glasgow
green	green	k1gInSc1	green
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
další	další	k2eAgNnSc1d1	další
kulturní	kulturní	k2eAgNnSc1d1	kulturní
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
Hydro	hydra	k1gFnSc5	hydra
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
SECC	secco	k1gNnPc2	secco
které	který	k3yQgInPc4	který
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
využito	využít	k5eAaPmNgNnS	využít
jako	jako	k8xC	jako
sportovní	sportovní	k2eAgFnSc1d1	sportovní
aréna	aréna	k1gFnSc1	aréna
při	při	k7c6	při
úspěšných	úspěšný	k2eAgFnPc6d1	úspěšná
Commonwealth	Commonwealth	k1gInSc1	Commonwealth
Games	Games	k1gInSc1	Games
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
:	:	kIx,	:
Do	do	k7c2	do
všech	všecek	k3xTgNnPc2	všecek
glasgowských	glasgowský	k2eAgNnPc2d1	glasgowské
muzeí	muzeum	k1gNnPc2	muzeum
je	být	k5eAaImIp3nS	být
vstup	vstup	k1gInSc4	vstup
zcela	zcela	k6eAd1	zcela
zdarma	zdarma	k6eAd1	zdarma
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
muzeích	muzeum	k1gNnPc6	muzeum
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
The	The	k1gFnSc1	The
Burell	Burell	k1gMnSc1	Burell
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
Muzeum	muzeum	k1gNnSc1	muzeum
Sira	sir	k1gMnSc2	sir
Williama	William	k1gMnSc2	William
Burella	Burell	k1gMnSc2	Burell
<g/>
)	)	kIx)	)
Fossil	Fossil	k1gFnSc1	Fossil
Grove	Groev	k1gFnSc2	Groev
(	(	kIx(	(
<g/>
Muzeum	muzeum	k1gNnSc1	muzeum
fosílií	fosílie	k1gFnPc2	fosílie
-	-	kIx~	-
ZAVŘENO	zavřít	k5eAaPmNgNnS	zavřít
do	do	k7c2	do
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Gallery	Galler	k1gInPc1	Galler
of	of	k?	of
Modern	Modern	k1gInSc1	Modern
Art	Art	k1gFnSc1	Art
(	(	kIx(	(
<g/>
Galerie	galerie	k1gFnSc1	galerie
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
)	)	kIx)	)
Kelvingrove	Kelvingrov	k1gInSc5	Kelvingrov
Art	Art	k1gFnSc1	Art
Gallery	Galler	k1gInPc1	Galler
and	and	k?	and
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
Galerie	galerie	k1gFnSc1	galerie
a	a	k8xC	a
muzeum	muzeum	k1gNnSc1	muzeum
Kelvingrove	Kelvingrov	k1gInSc5	Kelvingrov
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Martyr	martyr	k1gMnSc1	martyr
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
School	Schoola	k1gFnPc2	Schoola
(	(	kIx(	(
<g/>
Muzeum	muzeum	k1gNnSc1	muzeum
raných	raný	k2eAgNnPc2d1	rané
děl	dělo	k1gNnPc2	dělo
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
R.	R.	kA	R.
MacKintoshe	MacKintoshe	k1gFnSc1	MacKintoshe
<g/>
)	)	kIx)	)
St.	st.	kA	st.
Mungo	mungo	k1gMnSc1	mungo
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Religious	Religious	k1gMnSc1	Religious
Life	Lif	k1gFnSc2	Lif
and	and	k?	and
Art	Art	k1gFnSc1	Art
(	(	kIx(	(
<g/>
Muzeum	muzeum	k1gNnSc1	muzeum
náboženství	náboženství	k1gNnSc1	náboženství
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
sv.	sv.	kA	sv.
Munga	mungo	k1gMnSc2	mungo
<g/>
)	)	kIx)	)
Pollok	Pollok	k1gInSc1	Pollok
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
Venkovské	venkovský	k2eAgNnSc1d1	venkovské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
)	)	kIx)	)
People	People	k1gFnSc1	People
<g/>
'	'	kIx"	'
<g/>
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
Palace	Palace	k1gFnPc4	Palace
and	and	k?	and
Winter	Winter	k1gMnSc1	Winter
Gardens	Gardens	k1gInSc1	Gardens
(	(	kIx(	(
<g/>
Muzeum	muzeum	k1gNnSc1	muzeum
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
fotografií	fotografia	k1gFnPc2	fotografia
+	+	kIx~	+
zimní	zimní	k2eAgFnSc2d1	zimní
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
)	)	kIx)	)
Provand	Provand	k1gInSc1	Provand
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lordship	Lordship	k1gInSc1	Lordship
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
dům	dům	k1gInSc1	dům
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
)	)	kIx)	)
Open	Open	k1gInSc1	Open
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
Muzeum	muzeum	k1gNnSc1	muzeum
se	se	k3xPyFc4	se
sbírkou	sbírka	k1gFnSc7	sbírka
k	k	k7c3	k
volnému	volný	k2eAgNnSc3d1	volné
užívání	užívání	k1gNnSc3	užívání
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
)	)	kIx)	)
Museum	museum	k1gNnSc1	museum
of	of	k?	of
<g />
.	.	kIx.	.
</s>
<s>
Transport	transport	k1gInSc1	transport
(	(	kIx(	(
<g/>
Muzeum	muzeum	k1gNnSc1	muzeum
dopravy	doprava	k1gFnSc2	doprava
<g/>
)	)	kIx)	)
Mc	Mc	k1gMnSc1	Mc
Lellan	Lellan	k1gMnSc1	Lellan
Galleries	Galleries	k1gMnSc1	Galleries
(	(	kIx(	(
<g/>
Galerie	galerie	k1gFnSc1	galerie
Mc	Mc	k1gMnSc1	Mc
Lellan	Lellan	k1gInSc1	Lellan
<g/>
)	)	kIx)	)
Glasgow	Glasgow	k1gNnSc1	Glasgow
museums	museums	k6eAd1	museums
Recource	Recourka	k1gFnSc3	Recourka
Centre	centr	k1gInSc5	centr
(	(	kIx(	(
<g/>
Sklad	sklad	k1gInSc1	sklad
s	s	k7c7	s
povoleným	povolený	k2eAgInSc7d1	povolený
přístupem	přístup	k1gInSc7	přístup
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
muzejní	muzejní	k2eAgInPc4d1	muzejní
artefakty	artefakt	k1gInPc4	artefakt
všech	všecek	k3xTgNnPc2	všecek
městských	městský	k2eAgNnPc2d1	Městské
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
)	)	kIx)	)
Scotland	Scotland	k1gInSc1	Scotland
Streets	Streets	k1gInSc1	Streets
Schol	Schol	k1gInSc1	Schol
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
Muzeum	muzeum	k1gNnSc1	muzeum
vyučování	vyučování	k1gNnPc2	vyučování
a	a	k8xC	a
škol	škola	k1gFnPc2	škola
<g/>
)	)	kIx)	)
V	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
několik	několik	k4yIc1	několik
festivalů	festival	k1gInPc2	festival
a	a	k8xC	a
přehlídek	přehlídka	k1gFnPc2	přehlídka
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Byť	byť	k8xS	byť
nejznámější	známý	k2eAgInSc1d3	nejznámější
kulturní	kulturní	k2eAgInSc1d1	kulturní
festival	festival	k1gInSc1	festival
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
(	(	kIx(	(
<g/>
Edinburský	Edinburský	k2eAgInSc4d1	Edinburský
festival	festival	k1gInSc4	festival
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
v	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
velkou	velký	k2eAgFnSc4d1	velká
popularitu	popularita	k1gFnSc4	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ty	ten	k3xDgInPc4	ten
nejznámější	známý	k2eAgInPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
dudáckých	dudácký	k2eAgFnPc2d1	dudácká
kapel	kapela	k1gFnPc2	kapela
(	(	kIx(	(
<g/>
World	World	k1gInSc1	World
Pipe	Pipe	k1gNnSc1	Pipe
Band	banda	k1gFnPc2	banda
Championships	Championshipsa	k1gFnPc2	Championshipsa
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Festival	festival	k1gInSc1	festival
jazzu	jazz	k1gInSc2	jazz
(	(	kIx(	(
<g/>
Jazz	jazz	k1gInSc1	jazz
Festival	festival	k1gInSc1	festival
<g/>
)	)	kIx)	)
Glasgowský	glasgowský	k2eAgInSc1d1	glasgowský
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
West	West	k1gMnSc1	West
End	End	k1gMnSc1	End
festival	festival	k1gInSc4	festival
Keltský	keltský	k2eAgInSc4d1	keltský
festival	festival	k1gInSc4	festival
(	(	kIx(	(
<g/>
Celtic	Celtice	k1gFnPc2	Celtice
Connections	Connections	k1gInSc1	Connections
<g/>
)	)	kIx)	)
Merchant	Merchant	k1gInSc1	Merchant
City	city	k1gNnSc1	city
festival	festival	k1gInSc1	festival
Glasgowský	glasgowský	k2eAgInSc4d1	glasgowský
festival	festival	k1gInSc4	festival
komedie	komedie	k1gFnSc2	komedie
(	(	kIx(	(
<g/>
Glasgow	Glasgow	k1gInSc1	Glasgow
Comedy	Comeda	k1gMnSc2	Comeda
Festival	festival	k1gInSc1	festival
<g/>
)	)	kIx)	)
Glasgow	Glasgow	k1gInSc1	Glasgow
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
pořádají	pořádat	k5eAaImIp3nP	pořádat
koncerty	koncert	k1gInPc1	koncert
velkých	velký	k2eAgFnPc2d1	velká
i	i	k8xC	i
malých	malý	k2eAgFnPc2d1	malá
kapel	kapela	k1gFnPc2	kapela
a	a	k8xC	a
sólistů	sólista	k1gMnPc2	sólista
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
k	k	k7c3	k
'	'	kIx"	'
<g/>
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
'	'	kIx"	'
několik	několik	k4yIc4	několik
velkých	velký	k2eAgFnPc2d1	velká
koncertních	koncertní	k2eAgFnPc2d1	koncertní
síní	síň	k1gFnPc2	síň
<g/>
,	,	kIx,	,
hal	hala	k1gFnPc2	hala
a	a	k8xC	a
veřejných	veřejný	k2eAgNnPc2d1	veřejné
prostranství	prostranství	k1gNnPc2	prostranství
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
SECC	secco	k1gNnPc2	secco
(	(	kIx(	(
<g/>
Scottish	Scottish	k1gInSc1	Scottish
Exhibition	Exhibition	k1gInSc1	Exhibition
and	and	k?	and
Conference	Conference	k1gFnSc2	Conference
Centre	centr	k1gInSc5	centr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Glasgowská	glasgowský	k2eAgFnSc1d1	Glasgowská
královská	královský	k2eAgFnSc1d1	královská
koncertní	koncertní	k2eAgFnSc1d1	koncertní
hala	hala	k1gFnSc1	hala
(	(	kIx(	(
<g/>
Glasgow	Glasgow	k1gInSc1	Glasgow
Royal	Royal	k1gMnSc1	Royal
Concert	Concert	k1gMnSc1	Concert
Hall	Hall	k1gMnSc1	Hall
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Barrowlands	Barrowlands	k1gInSc1	Barrowlands
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
King	King	k1gMnSc1	King
Tut	tut	k1gInSc1	tut
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wah	Wah	k1gMnSc7	Wah
Wah	Wah	k1gMnSc7	Wah
Hut	Hut	k1gMnSc7	Hut
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
již	již	k6eAd1	již
zmiňovaný	zmiňovaný	k2eAgInSc1d1	zmiňovaný
Hampden	Hampdna	k1gFnPc2	Hampdna
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Glasgow	Glasgow	k1gNnSc1	Glasgow
je	být	k5eAaImIp3nS	být
také	také	k9	také
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
'	'	kIx"	'
<g/>
líhní	líheň	k1gFnSc7	líheň
<g/>
'	'	kIx"	'
světoznámých	světoznámý	k2eAgMnPc2d1	světoznámý
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
neúplný	úplný	k2eNgInSc1d1	neúplný
výčet	výčet	k1gInSc1	výčet
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
<g/>
:	:	kIx,	:
<g/>
Seznam	seznam	k1gInSc1	seznam
hudebníků	hudebník	k1gMnPc2	hudebník
a	a	k8xC	a
skupin	skupina	k1gFnPc2	skupina
z	z	k7c2	z
Glasgow	Glasgow	k1gNnSc2	Glasgow
Doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
a	a	k8xC	a
do	do	k7c2	do
Glasgow	Glasgow	k1gNnSc2	Glasgow
možná	možná	k9	možná
jak	jak	k6eAd1	jak
letecky	letecky	k6eAd1	letecky
(	(	kIx(	(
<g/>
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Glasgow	Glasgow	k1gNnSc1	Glasgow
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Glasgow	Glasgow	k1gInSc1	Glasgow
Prestwick	Prestwick	k1gMnSc1	Prestwick
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
tak	tak	k9	tak
po	po	k7c6	po
železnici	železnice	k1gFnSc6	železnice
(	(	kIx(	(
<g/>
historické	historický	k2eAgNnSc1d1	historické
nádraží	nádraží	k1gNnSc1	nádraží
Glasgow	Glasgow	k1gInSc1	Glasgow
Central	Central	k1gMnSc1	Central
Station	station	k1gInSc1	station
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
hustá	hustý	k2eAgFnSc1d1	hustá
síť	síť	k1gFnSc1	síť
železnic	železnice	k1gFnPc2	železnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInPc1d1	hlavní
tahy	tah	k1gInPc1	tah
dálnice	dálnice	k1gFnSc2	dálnice
M	M	kA	M
<g/>
74	[number]	k4	74
<g/>
-jih	iha	k1gFnPc2	-jiha
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
8	[number]	k4	8
<g/>
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
-východ	ýchod	k1gInSc1	-východ
<g/>
,	,	kIx,	,
A	a	k9	a
<g/>
8	[number]	k4	8
<g/>
-západ	ápad	k6eAd1	-západ
<g/>
,	,	kIx,	,
A	a	k9	a
<g/>
80	[number]	k4	80
<g/>
-severovýchod	everovýchod	k1gInSc1	-severovýchod
<g/>
,	,	kIx,	,
A	a	k9	a
<g/>
82	[number]	k4	82
<g/>
-sever	evra	k1gFnPc2	-sevra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Případně	případně	k6eAd1	případně
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
kombinovaně	kombinovaně	k6eAd1	kombinovaně
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
instituce	instituce	k1gFnSc2	instituce
Strathclyde	Strathclyd	k1gInSc5	Strathclyd
Partnership	Partnership	k1gInSc1	Partnership
for	forum	k1gNnPc2	forum
Transport	transport	k1gInSc1	transport
(	(	kIx(	(
<g/>
SPT	SPT	kA	SPT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
Strathclyde	Strathclyd	k1gInSc5	Strathclyd
Passenger	Passenger	k1gMnSc1	Passenger
Transport	transport	k1gInSc1	transport
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
instituce	instituce	k1gFnSc1	instituce
spravuje	spravovat	k5eAaImIp3nS	spravovat
veškerou	veškerý	k3xTgFnSc4	veškerý
městskou	městský	k2eAgFnSc4d1	městská
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
<g/>
,	,	kIx,	,
lodní	lodní	k2eAgFnSc4d1	lodní
<g/>
,	,	kIx,	,
vlakovou	vlakový	k2eAgFnSc4d1	vlaková
i	i	k8xC	i
podzemní	podzemní	k2eAgFnSc4d1	podzemní
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
financována	financovat	k5eAaBmNgFnS	financovat
z	z	k7c2	z
několika	několik	k4yIc2	několik
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
radnice	radnice	k1gFnSc2	radnice
v	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
<g/>
.	.	kIx.	.
</s>
<s>
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
<g/>
,	,	kIx,	,
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
smluvních	smluvní	k2eAgMnPc2d1	smluvní
partnerů	partner	k1gMnPc2	partner
SPT	SPT	kA	SPT
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
je	být	k5eAaImIp3nS	být
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
Buchanan	Buchanana	k1gFnPc2	Buchanana
(	(	kIx(	(
<g/>
Buchanan	Buchanan	k1gInSc1	Buchanan
bus	bus	k1gInSc1	bus
station	station	k1gInSc1	station
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
směřují	směřovat	k5eAaImIp3nP	směřovat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
hlavní	hlavní	k2eAgInPc4d1	hlavní
spoje	spoj	k1gInPc4	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
síť	síť	k1gFnSc1	síť
železnic	železnice	k1gFnPc2	železnice
(	(	kIx(	(
<g/>
ve	v	k7c6	v
VB	VB	kA	VB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
Londýnu	Londýn	k1gInSc3	Londýn
<g/>
,	,	kIx,	,
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
flexibilní	flexibilní	k2eAgNnSc1d1	flexibilní
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
jak	jak	k6eAd1	jak
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgNnPc4	dva
větší	veliký	k2eAgNnPc4d2	veliký
nádraží	nádraží	k1gNnPc4	nádraží
:	:	kIx,	:
Central	Central	k1gFnSc1	Central
Station	station	k1gInSc1	station
<g/>
,	,	kIx,	,
Queen	Queen	k2eAgInSc1d1	Queen
Street	Street	k1gInSc1	Street
Station	station	k1gInSc1	station
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
menších	malý	k2eAgFnPc2d2	menší
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
způsobem	způsob	k1gInSc7	způsob
řešený	řešený	k2eAgInSc1d1	řešený
systém	systém	k1gInSc1	systém
metra	metro	k1gNnSc2	metro
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
poměrně	poměrně	k6eAd1	poměrně
rychlou	rychlý	k2eAgFnSc4d1	rychlá
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
Metro	metro	k1gNnSc1	metro
v	v	k7c4	v
Glasgow	Glasgow	k1gInSc4	Glasgow
<g/>
.	.	kIx.	.
</s>
<s>
Glasgowský	glasgowský	k2eAgInSc1d1	glasgowský
průmysl	průmysl	k1gInSc1	průmysl
těchto	tento	k3xDgInPc2	tento
dní	den	k1gInPc2	den
už	už	k9	už
zdaleka	zdaleka	k6eAd1	zdaleka
není	být	k5eNaImIp3nS	být
těžký	těžký	k2eAgInSc1d1	těžký
<g/>
,	,	kIx,	,
převládá	převládat	k5eAaImIp3nS	převládat
výroba	výroba	k1gFnSc1	výroba
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
,	,	kIx,	,
strojírenství	strojírenství	k1gNnSc2	strojírenství
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgFnSc1d1	speciální
oděvní	oděvní	k2eAgFnSc1d1	oděvní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
technologie	technologie	k1gFnSc1	technologie
<g/>
,	,	kIx,	,
potravinářství	potravinářství	k1gNnSc1	potravinářství
a	a	k8xC	a
chemický	chemický	k2eAgInSc1d1	chemický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
financí	finance	k1gFnPc2	finance
patří	patřit	k5eAaImIp3nS	patřit
Glasgow	Glasgow	k1gInSc1	Glasgow
mezi	mezi	k7c4	mezi
16	[number]	k4	16
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
finančních	finanční	k2eAgNnPc2d1	finanční
center	centrum	k1gNnPc2	centrum
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
největší	veliký	k2eAgFnPc4d3	veliký
skotské	skotský	k2eAgFnPc4d1	skotská
instituce	instituce	k1gFnPc4	instituce
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
firem	firma	k1gFnPc2	firma
podnikajících	podnikající	k2eAgFnPc2d1	podnikající
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
odvětví	odvětví	k1gNnSc6	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
samotném	samotný	k2eAgNnSc6d1	samotné
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
část	část	k1gFnSc1	část
nazývána	nazýván	k2eAgFnSc1d1	nazývána
"	"	kIx"	"
<g/>
čtveveční	čtvevečnit	k5eAaPmIp3nS	čtvevečnit
kilometr	kilometr	k1gInSc4	kilometr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nashromáždily	nashromáždit	k5eAaPmAgFnP	nashromáždit
pobočky	pobočka	k1gFnPc1	pobočka
i	i	k8xC	i
sídla	sídlo	k1gNnPc1	sídlo
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
podnikajících	podnikající	k2eAgFnPc2d1	podnikající
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
finančnictví	finančnictví	k1gNnSc6	finančnictví
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
Blythswood	Blythswooda	k1gFnPc2	Blythswooda
Hill	Hill	k1gInSc1	Hill
<g/>
,	,	kIx,	,
Anderston	Anderston	k1gInSc1	Anderston
a	a	k8xC	a
Broomielaw	Broomielaw	k1gFnSc1	Broomielaw
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
třetí	třetí	k4xOgNnSc4	třetí
největší	veliký	k2eAgNnSc4d3	veliký
finanční	finanční	k2eAgNnSc4d1	finanční
centrum	centrum	k1gNnSc4	centrum
po	po	k7c6	po
Londýnu	Londýn	k1gInSc3	Londýn
a	a	k8xC	a
Edinburghu	Edinburgh	k1gInSc3	Edinburgh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
deseti	deset	k4xCc2	deset
největších	veliký	k2eAgFnPc2d3	veliký
pojišťoven	pojišťovna	k1gFnPc2	pojišťovna
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
osm	osm	k4xCc1	osm
hlavní	hlavní	k2eAgNnSc1d1	hlavní
sídlo	sídlo	k1gNnSc1	sídlo
právě	právě	k9	právě
v	v	k7c4	v
Glasgow	Glasgow	k1gInSc4	Glasgow
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
fotbalu	fotbal	k1gInSc2	fotbal
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
populární	populární	k2eAgNnPc1d1	populární
ragby	ragby	k1gNnPc1	ragby
<g/>
,	,	kIx,	,
bowling	bowling	k1gInSc1	bowling
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
golf	golf	k1gInSc4	golf
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
nechybí	chybět	k5eNaImIp3nS	chybět
ani	ani	k9	ani
jiné	jiný	k2eAgInPc4d1	jiný
sporty	sport	k1gInPc4	sport
<g/>
:	:	kIx,	:
atletika	atletika	k1gFnSc1	atletika
(	(	kIx(	(
<g/>
pořadatelské	pořadatelský	k2eAgNnSc1d1	pořadatelské
město	město	k1gNnSc1	město
pro	pro	k7c4	pro
Hry	hra	k1gFnPc4	hra
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bojové	bojový	k2eAgInPc4d1	bojový
sporty	sport	k1gInPc4	sport
<g/>
,	,	kIx,	,
plavání	plavání	k1gNnSc1	plavání
atd	atd	kA	atd
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
zahájila	zahájit	k5eAaPmAgFnS	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
'	'	kIx"	'
<g/>
Národní	národní	k2eAgFnSc1d1	národní
akademie	akademie	k1gFnSc1	akademie
badmintonu	badminton	k1gInSc2	badminton
<g/>
'	'	kIx"	'
a	a	k8xC	a
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
získalo	získat	k5eAaPmAgNnS	získat
Glasgow	Glasgow	k1gNnSc1	Glasgow
titul	titul	k1gInSc1	titul
'	'	kIx"	'
<g/>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
sportu	sport	k1gInSc2	sport
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Sport	sport	k1gInSc1	sport
je	být	k5eAaImIp3nS	být
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
z	z	k7c2	z
fotbalových	fotbalový	k2eAgMnPc2d1	fotbalový
fanoušků	fanoušek	k1gMnPc2	fanoušek
by	by	kYmCp3nS	by
neznal	znát	k5eNaImAgMnS	znát
kluby	klub	k1gInPc4	klub
jako	jako	k8xS	jako
Glasgow	Glasgow	k1gInSc4	Glasgow
Rangers	Rangersa	k1gFnPc2	Rangersa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Celtic	Celtice	k1gFnPc2	Celtice
Glasgow	Glasgow	k1gNnSc4	Glasgow
<g/>
.	.	kIx.	.
</s>
<s>
Fotbal	fotbal	k1gInSc1	fotbal
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
Glasgow	Glasgow	k1gInSc4	Glasgow
opravdovým	opravdový	k2eAgInSc7d1	opravdový
svátkem	svátek	k1gInSc7	svátek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
zápasu	zápas	k1gInSc2	zápas
svého	svůj	k3xOyFgInSc2	svůj
klubu	klub	k1gInSc2	klub
se	se	k3xPyFc4	se
fanoušci	fanoušek	k1gMnPc1	fanoušek
oblečou	obléct	k5eAaPmIp3nP	obléct
do	do	k7c2	do
odpovídajícího	odpovídající	k2eAgInSc2d1	odpovídající
oděvu	oděv	k1gInSc2	oděv
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
jejich	jejich	k3xOp3gInSc4	jejich
klub	klub	k1gInSc4	klub
a	a	k8xC	a
vyrazí	vyrazit	k5eAaPmIp3nP	vyrazit
nejčastěji	často	k6eAd3	často
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
oblíbené	oblíbený	k2eAgFnSc2d1	oblíbená
hospůdky	hospůdka	k1gFnSc2	hospůdka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
s	s	k7c7	s
kamarády	kamarád	k1gMnPc7	kamarád
před	před	k7c7	před
vlastním	vlastní	k2eAgNnSc7d1	vlastní
utkáním	utkání	k1gNnSc7	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
chodí	chodit	k5eAaImIp3nS	chodit
ne	ne	k9	ne
výjimečně	výjimečně	k6eAd1	výjimečně
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
zájem	zájem	k1gInSc1	zájem
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc1d1	tradiční
duel	duel	k1gInSc1	duel
dvou	dva	k4xCgMnPc2	dva
odvěkých	odvěký	k2eAgMnPc2d1	odvěký
rivalů	rival	k1gMnPc2	rival
Celticu	Celticus	k1gInSc2	Celticus
a	a	k8xC	a
Rangers	Rangers	k1gInSc1	Rangers
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
nejméně	málo	k6eAd3	málo
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
a	a	k8xC	a
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
hraje	hrát	k5eAaImIp3nS	hrát
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
roli	role	k1gFnSc4	role
také	také	k9	také
náboženské	náboženský	k2eAgNnSc4d1	náboženské
cítění	cítění	k1gNnSc4	cítění
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
stadióny	stadión	k1gInPc1	stadión
v	v	k7c6	v
Glasgow	Glasgow	k1gNnPc6	Glasgow
jsou	být	k5eAaImIp3nP	být
Národní	národní	k2eAgFnSc1d1	národní
Hampden	Hampdno	k1gNnPc2	Hampdno
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
52	[number]	k4	52
670	[number]	k4	670
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
5	[number]	k4	5
hvězdiček	hvězdička	k1gFnPc2	hvězdička
UEFA	UEFA	kA	UEFA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Celtic	Celtice	k1gFnPc2	Celtice
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
60	[number]	k4	60
832	[number]	k4	832
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ibrox	Ibrox	k1gInSc1	Ibrox
(	(	kIx(	(
<g/>
Rangers-	Rangers-	k1gFnSc1	Rangers-
<g/>
51	[number]	k4	51
0	[number]	k4	0
<g/>
82	[number]	k4	82
<g/>
,	,	kIx,	,
5	[number]	k4	5
hvězdiček	hvězdička	k1gFnPc2	hvězdička
UEFA	UEFA	kA	UEFA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Firhill	Firhill	k1gMnSc1	Firhill
(	(	kIx(	(
<g/>
Partick	Partick	k1gMnSc1	Partick
Thistle	Thistle	k1gMnSc1	Thistle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stadión	stadión	k1gInSc1	stadión
Hampden	Hampdna	k1gFnPc2	Hampdna
Park	park	k1gInSc1	park
'	'	kIx"	'
<g/>
drží	držet	k5eAaImIp3nS	držet
<g/>
'	'	kIx"	'
evropský	evropský	k2eAgInSc4d1	evropský
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
návštěvnosti	návštěvnost	k1gFnSc6	návštěvnost
fotbalového	fotbalový	k2eAgNnSc2d1	fotbalové
utkání	utkání	k1gNnSc2	utkání
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
ještě	ještě	k9	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgInPc4d1	možný
zápasy	zápas	k1gInPc4	zápas
sledovat	sledovat	k5eAaImF	sledovat
ve	v	k7c4	v
stoje	stoj	k1gInPc4	stoj
<g/>
.	.	kIx.	.
</s>
<s>
Tenkrát	tenkrát	k6eAd1	tenkrát
se	se	k3xPyFc4	se
na	na	k7c6	na
utkání	utkání	k1gNnSc6	utkání
mezi	mezi	k7c7	mezi
Skotskem	Skotsko	k1gNnSc7	Skotsko
a	a	k8xC	a
Anglií	Anglie	k1gFnSc7	Anglie
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přišlo	přijít	k5eAaPmAgNnS	přijít
podívat	podívat	k5eAaPmF	podívat
neuvěřitelných	uvěřitelný	k2eNgInPc2d1	neuvěřitelný
149	[number]	k4	149
547	[number]	k4	547
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgInPc4	tři
profesionální	profesionální	k2eAgInPc4d1	profesionální
fotbalové	fotbalový	k2eAgInPc4d1	fotbalový
kluby	klub	k1gInPc4	klub
<g/>
:	:	kIx,	:
Celtic	Celtice	k1gFnPc2	Celtice
a	a	k8xC	a
Rangers	Rangersa	k1gFnPc2	Rangersa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
nazývány	nazýván	k2eAgFnPc1d1	nazývána
'	'	kIx"	'
<g/>
Staré	Staré	k2eAgFnPc1d1	Staré
gardy	garda	k1gFnPc1	garda
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
'	'	kIx"	'
<g/>
firmy	firma	k1gFnSc2	firma
<g/>
'	'	kIx"	'
[	[	kIx(	[
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
</s>
<s>
Old	Olda	k1gFnPc2	Olda
Firm	Firma	k1gFnPc2	Firma
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
a	a	k8xC	a
Partick	Partick	k1gMnSc1	Partick
Thistle	Thistle	k1gMnSc1	Thistle
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Queen	Queen	k1gInSc1	Queen
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
amatérský	amatérský	k2eAgInSc1d1	amatérský
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
nalézt	nalézt	k5eAaBmF	nalézt
ve	v	k7c6	v
'	'	kIx"	'
<g/>
Skotském	skotský	k2eAgInSc6d1	skotský
profesionálním	profesionální	k2eAgInSc6d1	profesionální
ligovém	ligový	k2eAgInSc6d1	ligový
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
pol.	pol.	k?	pol.
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
také	také	k9	také
působily	působit	k5eAaImAgInP	působit
kluby	klub	k1gInPc1	klub
<g/>
:	:	kIx,	:
Clyde	Clyd	k1gMnSc5	Clyd
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
přemístil	přemístit	k5eAaPmAgInS	přemístit
do	do	k7c2	do
města	město	k1gNnSc2	město
Cumbernauld	Cumbernaulda	k1gFnPc2	Cumbernaulda
<g/>
,	,	kIx,	,
a	a	k8xC	a
Third	Third	k1gMnSc1	Third
Lanark	Lanark	k1gInSc4	Lanark
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zbankrotoval	zbankrotovat	k5eAaPmAgInS	zbankrotovat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
několik	několik	k4yIc1	několik
klubů	klub	k1gInPc2	klub
patřících	patřící	k2eAgInPc2d1	patřící
do	do	k7c2	do
'	'	kIx"	'
<g/>
Skotské	skotský	k2eAgFnSc2d1	skotská
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
asociace	asociace	k1gFnSc2	asociace
juniorů	junior	k1gMnPc2	junior
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
<g/>
:	:	kIx,	:
Pollok	Pollok	k1gInSc1	Pollok
<g/>
,	,	kIx,	,
Maryhill	Maryhill	k1gInSc1	Maryhill
a	a	k8xC	a
Petershill	Petershill	k1gInSc1	Petershill
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
množství	množství	k1gNnSc1	množství
klubů	klub	k1gInPc2	klub
amatérských	amatérský	k2eAgInPc2d1	amatérský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
sídlí	sídlet	k5eAaImIp3nS	sídlet
také	také	k9	také
profesionální	profesionální	k2eAgNnSc4d1	profesionální
ragby	ragby	k1gNnSc4	ragby
tým	tým	k1gInSc1	tým
-	-	kIx~	-
Glasgow	Glasgow	k1gNnSc1	Glasgow
Warriors	Warriorsa	k1gFnPc2	Warriorsa
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgMnPc1d1	hrající
Celtic	Celtice	k1gFnPc2	Celtice
League	League	k1gInSc1	League
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgNnPc7d1	ostatní
družstvy	družstvo	k1gNnPc7	družstvo
ze	z	k7c2	z
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
Irska	Irsko	k1gNnSc2	Irsko
a	a	k8xC	a
Walesu	Wales	k1gInSc2	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
je	být	k5eAaImIp3nS	být
také	také	k9	také
mnoho	mnoho	k6eAd1	mnoho
mládežnických	mládežnický	k2eAgInPc2d1	mládežnický
-	-	kIx~	-
juniorských	juniorský	k2eAgInPc2d1	juniorský
a	a	k8xC	a
amatérských	amatérský	k2eAgInPc2d1	amatérský
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgMnSc1d1	hrající
v	v	k7c6	v
různých	různý	k2eAgFnPc2d1	různá
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Bowls	Bowls	k1gInSc1	Bowls
<g/>
,	,	kIx,	,
venkovní	venkovní	k2eAgFnSc1d1	venkovní
obdoba	obdoba	k1gFnSc1	obdoba
bowlingu	bowling	k1gInSc2	bowling
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
zejména	zejména	k9	zejména
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
populární	populární	k2eAgFnSc1d1	populární
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
starší	starý	k2eAgFnSc2d2	starší
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
i	i	k9	i
zde	zde	k6eAd1	zde
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
množství	množství	k1gNnSc4	množství
bowlingových	bowlingový	k2eAgNnPc2d1	bowlingové
hřišť	hřiště	k1gNnPc2	hřiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
schází	scházet	k5eAaImIp3nP	scházet
členové	člen	k1gMnPc1	člen
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
hrají	hrát	k5eAaImIp3nP	hrát
na	na	k7c6	na
dokonale	dokonale	k6eAd1	dokonale
upravených	upravený	k2eAgInPc6d1	upravený
trávnících	trávník	k1gInPc6	trávník
tuto	tento	k3xDgFnSc4	tento
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
v	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
skotské	skotský	k2eAgNnSc1d1	skotské
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
hlavně	hlavně	k6eAd1	hlavně
nepředpověditelné	předpověditelný	k2eNgNnSc4d1	nepředpověditelné
<g/>
.	.	kIx.	.
</s>
<s>
Skoti	Skot	k1gMnPc1	Skot
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
four	four	k1gInSc1	four
seasons	seasons	k1gInSc1	seasons
in	in	k?	in
one	one	k?	one
day	day	k?	day
<g/>
'	'	kIx"	'
<g/>
--	--	k?	--
<g/>
'	'	kIx"	'
<g/>
čtyři	čtyři	k4xCgMnPc1	čtyři
roční	roční	k2eAgMnPc1d1	roční
období	období	k1gNnSc2	období
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
dni	den	k1gInSc6	den
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
lze	lze	k6eAd1	lze
také	také	k9	také
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
zažít	zažít	k5eAaPmF	zažít
doslova	doslova	k6eAd1	doslova
do	do	k7c2	do
písmene	písmeno	k1gNnSc2	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
ovlivňováno	ovlivňovat	k5eAaImNgNnS	ovlivňovat
zejména	zejména	k9	zejména
Golfským	golfský	k2eAgInSc7d1	golfský
proudem	proud	k1gInSc7	proud
od	od	k7c2	od
západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
přináší	přinášet	k5eAaImIp3nS	přinášet
srážky	srážka	k1gFnPc4	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Rána	ráno	k1gNnPc1	ráno
bývají	bývat	k5eAaImIp3nP	bývat
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
mlhou	mlha	k1gFnSc7	mlha
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
den	den	k1gInSc4	den
je	být	k5eAaImIp3nS	být
nezřídka	nezřídka	k6eAd1	nezřídka
větrno	větrno	k6eAd1	větrno
<g/>
.	.	kIx.	.
</s>
<s>
Pozdní	pozdní	k2eAgNnSc1d1	pozdní
jaro	jaro	k1gNnSc1	jaro
a	a	k8xC	a
léto	léto	k1gNnSc1	léto
bývá	bývat	k5eAaImIp3nS	bývat
slunečné	slunečný	k2eAgNnSc1d1	slunečné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zdaleka	zdaleka	k6eAd1	zdaleka
ne	ne	k9	ne
tolik	tolik	k4xDc1	tolik
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Zimy	zima	k1gFnPc1	zima
bývají	bývat	k5eAaImIp3nP	bývat
vlhké	vlhký	k2eAgFnPc1d1	vlhká
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
teplejší	teplý	k2eAgFnSc1d2	teplejší
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
srážek	srážka	k1gFnPc2	srážka
připadá	připadat	k5eAaImIp3nS	připadat
právě	právě	k9	právě
na	na	k7c4	na
zimní	zimní	k2eAgInPc4d1	zimní
měsíce	měsíc	k1gInPc4	měsíc
-	-	kIx~	-
leden	leden	k1gInSc4	leden
a	a	k8xC	a
únor	únor	k1gInSc4	únor
<g/>
.	.	kIx.	.
sir	sir	k1gMnSc1	sir
Alex	Alex	k1gMnSc1	Alex
Ferguson	Ferguson	k1gMnSc1	Ferguson
-	-	kIx~	-
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
trenér	trenér	k1gMnSc1	trenér
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
Billy	Bill	k1gMnPc7	Bill
Connoly	Connola	k1gFnSc2	Connola
-	-	kIx~	-
komediální	komediální	k2eAgMnSc1d1	komediální
herec	herec	k1gMnSc1	herec
Robert	Robert	k1gMnSc1	Robert
Carlyle	Carlyl	k1gInSc5	Carlyl
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
Trainspotting	Trainspotting	k1gInSc1	Trainspotting
<g/>
,	,	kIx,	,
Full	Full	k1gInSc1	Full
Monty	Monta	k1gFnSc2	Monta
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
World	Worlda	k1gFnPc2	Worlda
is	is	k?	is
Not	nota	k1gFnPc2	nota
Enough	Enough	k1gMnSc1	Enough
<g/>
)	)	kIx)	)
Charles	Charles	k1gMnSc1	Charles
Rennie	Rennie	k1gFnSc2	Rennie
Mackintosh	Mackintosh	k1gMnSc1	Mackintosh
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
interiérista	interiérista	k1gMnSc1	interiérista
Angus	Angus	k1gMnSc1	Angus
Young	Young	k1gMnSc1	Young
-	-	kIx~	-
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
kapely	kapela	k1gFnSc2	kapela
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
Malcolm	Malcolm	k1gMnSc1	Malcolm
Young	Young	k1gMnSc1	Young
-	-	kIx~	-
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
kapely	kapela	k1gFnSc2	kapela
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
Havana	Havana	k1gFnSc1	Havana
(	(	kIx(	(
<g/>
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
)	)	kIx)	)
Rostov	Rostov	k1gInSc1	Rostov
na	na	k7c6	na
Donu	Don	k1gInSc6	Don
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
Norimberk	Norimberk	k1gInSc1	Norimberk
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
Turín	Turín	k1gInSc1	Turín
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
Talien	Talien	k1gInSc1	Talien
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
Řím	Řím	k1gInSc1	Řím
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
Marseille	Marseille	k1gFnSc1	Marseille
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
Láhaur	Láhaur	k1gInSc1	Láhaur
(	(	kIx(	(
<g/>
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
)	)	kIx)	)
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
