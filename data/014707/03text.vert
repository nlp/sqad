<s>
Žinčice	žinčice	k1gFnSc1
</s>
<s>
Žinčice	žinčice	k1gFnSc1
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
žinčica	žinčica	k6eAd1
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
żentyca	żentyca	k6eAd1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
tradiční	tradiční	k2eAgInSc1d1
slovenský	slovenský	k2eAgInSc1d1
nápoj	nápoj	k1gInSc1
z	z	k7c2
ovčího	ovčí	k2eAgNnSc2d1
mléka	mléko	k1gNnSc2
podobný	podobný	k2eAgInSc4d1
kefíru	kefír	k1gInSc2
<g/>
,	,	kIx,
vzniká	vznikat	k5eAaImIp3nS
jako	jako	k9
vedlejší	vedlejší	k2eAgInSc1d1
produkt	produkt	k1gInSc1
při	při	k7c6
výrobě	výroba	k1gFnSc6
brynzy	brynza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původ	původ	k1gInSc1
slova	slovo	k1gNnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
rumunském	rumunský	k2eAgInSc6d1
jîntiţa	jîntiţa	k6eAd1
<g/>
,	,	kIx,
nápoji	nápoj	k1gInSc6
valašských	valašský	k2eAgMnPc2d1
pastýřů	pastýř	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údajně	údajně	k6eAd1
nejlepší	dobrý	k2eAgInSc4d3
druh	druh	k1gInSc4
žinčice	žinčice	k1gFnSc2
je	být	k5eAaImIp3nS
urda	urda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
hustší	hustý	k2eAgFnSc1d2
než	než	k8xS
klasická	klasický	k2eAgFnSc1d1
slaná	slaný	k2eAgFnSc1d1
nebo	nebo	k8xC
kyselá	kyselý	k2eAgFnSc1d1
žinčice	žinčice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přinejmenším	přinejmenším	k6eAd1
v	v	k7c6
létě	léto	k1gNnSc6
je	být	k5eAaImIp3nS
příjemným	příjemný	k2eAgInSc7d1
osvěžujícím	osvěžující	k2eAgInSc7d1
nápojem	nápoj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
autentickému	autentický	k2eAgInSc3d1
zážitku	zážitek	k1gInSc3
návštěvy	návštěva	k1gFnSc2
salaše	salaš	k1gInSc2
patří	patřit	k5eAaImIp3nS
pití	pití	k1gNnSc4
žinčice	žinčice	k1gFnSc2
z	z	k7c2
typických	typický	k2eAgFnPc2d1
pastýřských	pastýřská	k1gFnPc2
črpáků	črpáků	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Žinčice	žinčice	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
typicky	typicky	k6eAd1
servírována	servírován	k2eAgFnSc1d1
k	k	k7c3
bryndzovým	bryndzův	k2eAgFnPc3d1
haluškám	haluška	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Žinčice	žinčice	k1gFnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
bohatá	bohatý	k2eAgFnSc1d1
co	co	k9
do	do	k7c2
počtu	počet	k1gInSc2
druhů	druh	k1gInPc2
mikroorganismů	mikroorganismus	k1gInPc2
<g/>
,	,	kIx,
kterých	který	k3yIgInPc2,k3yQgInPc2,k3yRgInPc2
obsahuje	obsahovat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
známých	známý	k2eAgNnPc2d1
až	až	k9
1	#num#	k4
700	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jednom	jeden	k4xCgInSc6
gramu	gram	k1gInSc6
produktu	produkt	k1gInSc2
jich	on	k3xPp3gInPc2
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
až	až	k9
1	#num#	k4
miliardu	miliarda	k4xCgFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejpočetněji	početně	k6eAd3
zastoupenými	zastoupený	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
bakterií	bakterie	k1gFnPc2
a	a	k8xC
kvasinek	kvasinka	k1gFnPc2
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
Lactobacillus	Lactobacillus	k1gInSc1
<g/>
,	,	kIx,
Lactococcus	Lactococcus	k1gInSc1
<g/>
,	,	kIx,
Leuconostoc	Leuconostoc	k1gInSc1
<g/>
,	,	kIx,
Bifidobacterium	Bifidobacterium	k1gNnSc1
<g/>
,	,	kIx,
Enterococcus	Enterococcus	k1gMnSc1
<g/>
,	,	kIx,
Saccharomyces	Saccharomyces	k1gMnSc1
<g/>
,	,	kIx,
Kluyveromyces	Kluyveromyces	k1gMnSc1
<g/>
,	,	kIx,
Streptococcus	Streptococcus	k1gMnSc1
<g/>
,	,	kIx,
Candida	Candida	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Žinčica	Žinčicum	k1gNnSc2
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Žinčica	Žinčica	k1gFnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
SMATANA	SMATANA	kA
<g/>
,	,	kIx,
Ľubomír	Ľubomír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naberte	nabrat	k5eAaPmRp2nP
si	se	k3xPyFc3
žinčici	žinčice	k1gFnSc4
do	do	k7c2
črpáku	črpáku	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radiožurnál	radiožurnál	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-07-04	2009-07-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://vscht.cz/tmt/prehlidky/2007/souhrn_MaS_2007.pdf	https://vscht.cz/tmt/prehlidky/2007/souhrn_MaS_2007.pdf	k1gInSc1
Mikroflóra	mikroflóra	k1gFnSc1
žinčice	žinčice	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
žinčice	žinčice	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
