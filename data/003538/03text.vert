<s>
Blesk	blesk	k1gInSc1	blesk
je	být	k5eAaImIp3nS	být
silný	silný	k2eAgInSc4d1	silný
přírodní	přírodní	k2eAgInSc4d1	přírodní
elektrostatický	elektrostatický	k2eAgInSc4d1	elektrostatický
výboj	výboj	k1gInSc4	výboj
(	(	kIx(	(
<g/>
electrostatic	electrostatice	k1gFnPc2	electrostatice
discharge	discharg	k1gFnSc2	discharg
–	–	k?	–
ESD	ESD	kA	ESD
<g/>
)	)	kIx)	)
produkovaný	produkovaný	k2eAgMnSc1d1	produkovaný
během	během	k7c2	během
bouřky	bouřka	k1gFnSc2	bouřka
<g/>
.	.	kIx.	.
</s>
<s>
Bleskový	bleskový	k2eAgInSc1d1	bleskový
elektrický	elektrický	k2eAgInSc1d1	elektrický
výboj	výboj	k1gInSc1	výboj
je	být	k5eAaImIp3nS	být
provázen	provázet	k5eAaImNgInS	provázet
emisí	emise	k1gFnSc7	emise
světla	světlo	k1gNnPc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Elektřina	elektřina	k1gFnSc1	elektřina
procházející	procházející	k2eAgFnSc1d1	procházející
kanály	kanál	k1gInPc1	kanál
výboje	výboj	k1gInSc2	výboj
rychle	rychle	k6eAd1	rychle
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
okolní	okolní	k2eAgInSc4d1	okolní
vzduch	vzduch	k1gInSc4	vzduch
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
díky	díky	k7c3	díky
expanzi	expanze	k1gFnSc3	expanze
produkuje	produkovat	k5eAaImIp3nS	produkovat
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
zvuk	zvuk	k1gInSc4	zvuk
hromu	hrom	k1gInSc2	hrom
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
mají	mít	k5eAaImIp3nP	mít
blesky	blesk	k1gInPc4	blesk
modro-bílé	modroílý	k2eAgNnSc4d1	modro-bílé
zabarvení	zabarvení	k1gNnSc4	zabarvení
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
dusíku	dusík	k1gInSc2	dusík
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Přetvářejí	přetvářet	k5eAaImIp3nP	přetvářet
horniny	hornina	k1gFnPc4	hornina
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
fulgurit	fulgurit	k1gInSc4	fulgurit
<g/>
.	.	kIx.	.
</s>
<s>
Přeskoková	přeskokový	k2eAgFnSc1d1	přeskoková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
elektrického	elektrický	k2eAgInSc2d1	elektrický
výboje	výboj	k1gInSc2	výboj
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jistě	jistě	k9	jistě
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
vlhkosti	vlhkost	k1gFnSc6	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
tu	tu	k6eAd1	tu
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yQnSc7	co
je	být	k5eAaImIp3nS	být
vzduch	vzduch	k1gInSc1	vzduch
vlhčí	vlhký	k2eAgInSc1d2	vlhčí
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
nasycený	nasycený	k2eAgMnSc1d1	nasycený
vodními	vodní	k2eAgFnPc7d1	vodní
parami	para	k1gFnPc7	para
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
přeskoková	přeskokový	k2eAgFnSc1d1	přeskoková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
zase	zase	k9	zase
silně	silně	k6eAd1	silně
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
také	také	k6eAd1	také
tlaku	tlak	k1gInSc2	tlak
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgNnPc2	tento
měření	měření	k1gNnPc2	měření
je	být	k5eAaImIp3nS	být
prováděna	provádět	k5eAaImNgFnS	provádět
za	za	k7c2	za
konstantní	konstantní	k2eAgFnSc2d1	konstantní
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
,	,	kIx,	,
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1	přírodní
blesky	blesk	k1gInPc1	blesk
se	se	k3xPyFc4	se
ovšemže	ovšemže	k9	ovšemže
ve	v	k7c6	v
stabilních	stabilní	k2eAgFnPc6d1	stabilní
laboratorních	laboratorní	k2eAgFnPc6d1	laboratorní
podmínkách	podmínka	k1gFnPc6	podmínka
nikdy	nikdy	k6eAd1	nikdy
nepohybují	pohybovat	k5eNaImIp3nP	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Běžná	běžný	k2eAgFnSc1d1	běžná
technická	technický	k2eAgFnSc1d1	technická
hodnota	hodnota	k1gFnSc1	hodnota
u	u	k7c2	u
vedení	vedení	k1gNnSc2	vedení
velmi	velmi	k6eAd1	velmi
vysokého	vysoký	k2eAgNnSc2d1	vysoké
napětí	napětí	k1gNnSc2	napětí
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
minimální	minimální	k2eAgFnSc7d1	minimální
přeskokovou	přeskokový	k2eAgFnSc7d1	přeskoková
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
asi	asi	k9	asi
1	[number]	k4	1
centimetr	centimetr	k1gInSc4	centimetr
při	při	k7c6	při
rozdílu	rozdíl	k1gInSc6	rozdíl
napětí	napětí	k1gNnSc1	napětí
10	[number]	k4	10
kilovoltů	kilovolt	k1gInPc2	kilovolt
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
minimálně	minimálně	k6eAd1	minimálně
1	[number]	k4	1
metr	metr	k1gInSc4	metr
při	při	k7c6	při
rozdílu	rozdíl	k1gInSc6	rozdíl
napětí	napětí	k1gNnSc2	napětí
jeden	jeden	k4xCgInSc1	jeden
milión	milión	k4xCgInSc1	milión
voltů	volt	k1gInPc2	volt
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
bouře	bouř	k1gFnPc4	bouř
bývá	bývat	k5eAaImIp3nS	bývat
relativní	relativní	k2eAgFnSc1d1	relativní
vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
extrémně	extrémně	k6eAd1	extrémně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
fakticky	fakticky	k6eAd1	fakticky
se	se	k3xPyFc4	se
často	často	k6eAd1	často
blíží	blížit	k5eAaImIp3nS	blížit
ke	k	k7c3	k
100	[number]	k4	100
%	%	kIx~	%
<g/>
,	,	kIx,	,
teploty	teplota	k1gFnPc4	teplota
a	a	k8xC	a
tlaky	tlak	k1gInPc4	tlak
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
bouřkových	bouřkový	k2eAgInPc6d1	bouřkový
mracích	mrak	k1gInPc6	mrak
však	však	k9	však
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
kolísají	kolísat	k5eAaImIp3nP	kolísat
(	(	kIx(	(
<g/>
samotná	samotný	k2eAgFnSc1d1	samotná
bouře	bouře	k1gFnSc1	bouře
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
důsledek	důsledek	k1gInSc4	důsledek
tohoto	tento	k3xDgNnSc2	tento
klimatického	klimatický	k2eAgNnSc2d1	klimatické
kolísání	kolísání	k1gNnSc2	kolísání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzduch	vzduch	k1gInSc1	vzduch
se	se	k3xPyFc4	se
při	při	k7c6	při
úderu	úder	k1gInSc6	úder
ohřeje	ohřát	k5eAaPmIp3nS	ohřát
až	až	k9	až
na	na	k7c4	na
30	[number]	k4	30
000	[number]	k4	000
°	°	k?	°
<g/>
C.	C.	kA	C.
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
výboj	výboj	k1gInSc1	výboj
vydává	vydávat	k5eAaImIp3nS	vydávat
několika	několik	k4yIc7	několik
drahami	draha	k1gFnPc7	draha
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
"	"	kIx"	"
<g/>
rozvětvený	rozvětvený	k2eAgInSc4d1	rozvětvený
blesk	blesk	k1gInSc4	blesk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Blesky	blesk	k1gInPc1	blesk
uvnitř	uvnitř	k7c2	uvnitř
jednoho	jeden	k4xCgInSc2	jeden
mraku	mrak	k1gInSc2	mrak
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
plošné	plošný	k2eAgFnPc1d1	plošná
<g/>
"	"	kIx"	"
a	a	k8xC	a
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
jen	jen	k6eAd1	jen
jako	jako	k8xC	jako
světelné	světelný	k2eAgInPc1d1	světelný
záblesky	záblesk	k1gInPc1	záblesk
<g/>
.	.	kIx.	.
</s>
<s>
Blesky	blesk	k1gInPc1	blesk
mohou	moct	k5eAaImIp3nP	moct
za	za	k7c4	za
bouřky	bouřka	k1gFnPc4	bouřka
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
nabývat	nabývat	k5eAaImF	nabývat
velmi	velmi	k6eAd1	velmi
podivných	podivný	k2eAgInPc2d1	podivný
tvarů	tvar	k1gInPc2	tvar
i	i	k8xC	i
neobvyklých	obvyklý	k2eNgInPc2d1	neobvyklý
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
konkrétních	konkrétní	k2eAgFnPc6d1	konkrétní
fyzikálních	fyzikální	k2eAgFnPc6d1	fyzikální
a	a	k8xC	a
klimatických	klimatický	k2eAgFnPc6d1	klimatická
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Blesk	blesk	k1gInSc1	blesk
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
i	i	k9	i
několik	několik	k4yIc4	několik
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvotního	prvotní	k2eAgInSc2d1	prvotní
výzkumu	výzkum	k1gInSc2	výzkum
elektřiny	elektřina	k1gFnSc2	elektřina
pomocí	pomocí	k7c2	pomocí
leydenských	leydenský	k2eAgFnPc2d1	leydenská
láhví	láhev	k1gFnPc2	láhev
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
instrumentů	instrument	k1gInPc2	instrument
si	se	k3xPyFc3	se
mnoho	mnoho	k6eAd1	mnoho
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Wall	Wall	k1gMnSc1	Wall
<g/>
,	,	kIx,	,
Gray	Graa	k1gFnPc1	Graa
<g/>
,	,	kIx,	,
Abbé	abbé	k1gMnSc1	abbé
Nollet	Nollet	k1gInSc1	Nollet
<g/>
)	)	kIx)	)
myslelo	myslet	k5eAaImAgNnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
krátké	krátký	k2eAgFnPc1d1	krátká
jiskry	jiskra	k1gFnPc1	jiskra
sdílejí	sdílet	k5eAaImIp3nP	sdílet
s	s	k7c7	s
bleskem	blesk	k1gInSc7	blesk
určitou	určitý	k2eAgFnSc4d1	určitá
podobnost	podobnost	k1gFnSc4	podobnost
<g/>
.	.	kIx.	.
</s>
<s>
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklina	k1gFnPc2	Franklina
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
testovat	testovat	k5eAaImF	testovat
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
použitím	použití	k1gNnSc7	použití
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
tyče	tyč	k1gFnSc2	tyč
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
vztyčena	vztyčit	k5eAaPmNgFnS	vztyčit
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
čekání	čekání	k1gNnSc2	čekání
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
dokončení	dokončení	k1gNnSc4	dokončení
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgInS	dostat
nápad	nápad	k1gInSc1	nápad
použít	použít	k5eAaPmF	použít
létající	létající	k2eAgInSc4d1	létající
objekt	objekt	k1gInSc4	objekt
–	–	k?	–
např.	např.	kA	např.
papírový	papírový	k2eAgInSc4d1	papírový
drak	drak	k1gInSc4	drak
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následující	následující	k2eAgFnSc2d1	následující
bouřky	bouřka	k1gFnSc2	bouřka
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
1752	[number]	k4	1752
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
jako	jako	k8xS	jako
asistentem	asistent	k1gMnSc7	asistent
vypustili	vypustit	k5eAaPmAgMnP	vypustit
draka	drak	k1gMnSc4	drak
do	do	k7c2	do
bouřkových	bouřkový	k2eAgInPc2d1	bouřkový
mraků	mrak	k1gInPc2	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
konec	konec	k1gInSc4	konec
jeho	jeho	k3xOp3gNnSc2	jeho
lanka	lanko	k1gNnSc2	lanko
připevnili	připevnit	k5eAaPmAgMnP	připevnit
klíč	klíč	k1gInSc4	klíč
a	a	k8xC	a
uvázali	uvázat	k5eAaPmAgMnP	uvázat
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
kolík	kolík	k1gInSc4	kolík
s	s	k7c7	s
hedvábnou	hedvábný	k2eAgFnSc7d1	hedvábná
nití	nit	k1gFnSc7	nit
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
si	se	k3xPyFc3	se
Franklin	Franklin	k1gInSc1	Franklin
všiml	všimnout	k5eAaPmAgInS	všimnout
ztráty	ztráta	k1gFnPc4	ztráta
vláken	vlákno	k1gNnPc2	vlákno
na	na	k7c6	na
lanku	lanko	k1gNnSc6	lanko
vlivem	vlivem	k7c2	vlivem
napínání	napínání	k1gNnSc2	napínání
<g/>
;	;	kIx,	;
zkusil	zkusit	k5eAaPmAgMnS	zkusit
dát	dát	k5eAaPmF	dát
svou	svůj	k3xOyFgFnSc4	svůj
ruku	ruka	k1gFnSc4	ruka
dost	dost	k6eAd1	dost
blízko	blízko	k6eAd1	blízko
ke	k	k7c3	k
klíči	klíč	k1gInSc3	klíč
a	a	k8xC	a
mezerou	mezera	k1gFnSc7	mezera
přeskočila	přeskočit	k5eAaPmAgFnS	přeskočit
jiskra	jiskra	k1gFnSc1	jiskra
<g/>
.	.	kIx.	.
</s>
<s>
Padající	padající	k2eAgInSc1d1	padající
déšť	déšť	k1gInSc1	déšť
namočil	namočit	k5eAaPmAgInS	namočit
lanko	lanko	k1gNnSc4	lanko
a	a	k8xC	a
udělal	udělat	k5eAaPmAgInS	udělat
ho	on	k3xPp3gMnSc4	on
vodivým	vodivý	k2eAgInSc7d1	vodivý
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
i	i	k9	i
jiní	jiný	k2eAgMnPc1d1	jiný
(	(	kIx(	(
<g/>
Dalibard	Dalibard	k1gMnSc1	Dalibard
a	a	k8xC	a
De	De	k?	De
Lors	Lors	k1gInSc1	Lors
<g/>
)	)	kIx)	)
dělali	dělat	k5eAaImAgMnP	dělat
podobné	podobný	k2eAgInPc4d1	podobný
experimenty	experiment	k1gInPc4	experiment
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Franklin	Franklin	k1gInSc1	Franklin
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
původní	původní	k2eAgInSc1d1	původní
nápad	nápad	k1gInSc1	nápad
s	s	k7c7	s
vyvýšeným	vyvýšený	k2eAgInSc7d1	vyvýšený
objektem	objekt	k1gInSc7	objekt
a	a	k8xC	a
jiskrovou	jiskrový	k2eAgFnSc7d1	Jiskrová
mezerou	mezera	k1gFnSc7	mezera
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
i	i	k8xC	i
oni	onen	k3xDgMnPc1	onen
použili	použít	k5eAaPmAgMnP	použít
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
autora	autor	k1gMnSc4	autor
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
experimentu	experiment	k1gInSc6	experiment
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc6	jeho
podrobnostech	podrobnost	k1gFnPc6	podrobnost
rozšiřovaly	rozšiřovat	k5eAaImAgFnP	rozšiřovat
<g/>
,	,	kIx,	,
vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
se	se	k3xPyFc4	se
pokusy	pokus	k1gInPc7	pokus
o	o	k7c4	o
napodobení	napodobení	k1gNnPc4	napodobení
<g/>
.	.	kIx.	.
</s>
<s>
Experimenty	experiment	k1gInPc1	experiment
s	s	k7c7	s
bleskem	blesk	k1gInSc7	blesk
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
extrémně	extrémně	k6eAd1	extrémně
rizikové	rizikový	k2eAgFnPc1d1	riziková
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
často	často	k6eAd1	často
smrtelné	smrtelný	k2eAgFnPc1d1	smrtelná
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
oběť	oběť	k1gFnSc1	oběť
z	z	k7c2	z
mnohých	mnohý	k2eAgMnPc2d1	mnohý
imitátorů	imitátor	k1gMnPc2	imitátor
Franklina	Franklina	k1gFnSc1	Franklina
byl	být	k5eAaImAgMnS	být
profesor	profesor	k1gMnSc1	profesor
Richman	Richman	k1gMnSc1	Richman
ze	z	k7c2	z
Sankt	Sankt	k1gInSc1	Sankt
Petersburgu	Petersburg	k1gInSc2	Petersburg
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
podobnou	podobný	k2eAgFnSc4d1	podobná
sestavu	sestava	k1gFnSc4	sestava
jako	jako	k8xS	jako
Franklin	Franklin	k1gInSc4	Franklin
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
když	když	k8xS	když
uslyšel	uslyšet	k5eAaPmAgMnS	uslyšet
bouřku	bouřka	k1gFnSc4	bouřka
<g/>
.	.	kIx.	.
</s>
<s>
Utíkal	utíkat	k5eAaImAgMnS	utíkat
domů	domů	k6eAd1	domů
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
rytcem	rytec	k1gMnSc7	rytec
na	na	k7c4	na
zachycení	zachycení	k1gNnSc4	zachycení
události	událost	k1gFnSc2	událost
pro	pro	k7c4	pro
potomstvo	potomstvo	k1gNnSc4	potomstvo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
experimentu	experiment	k1gInSc2	experiment
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
velký	velký	k2eAgInSc1d1	velký
kulový	kulový	k2eAgInSc1d1	kulový
blesk	blesk	k1gInSc1	blesk
<g/>
,	,	kIx,	,
srazil	srazit	k5eAaPmAgMnS	srazit
se	se	k3xPyFc4	se
s	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
Richmana	Richman	k1gMnSc2	Richman
zanechav	zanechat	k5eAaPmDgInS	zanechat
červenou	červený	k2eAgFnSc4d1	červená
skvrnu	skvrna	k1gFnSc4	skvrna
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
skonal	skonat	k5eAaPmAgMnS	skonat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
boty	bota	k1gFnPc1	bota
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
kusy	kus	k1gInPc4	kus
rozpadlé	rozpadlý	k2eAgInPc4d1	rozpadlý
a	a	k8xC	a
ohořelé	ohořelý	k2eAgInPc4d1	ohořelý
<g/>
,	,	kIx,	,
části	část	k1gFnPc1	část
oděvu	oděv	k1gInSc2	oděv
byly	být	k5eAaImAgFnP	být
připáleny	připálen	k2eAgFnPc1d1	připálen
<g/>
,	,	kIx,	,
rytec	rytec	k1gMnSc1	rytec
byl	být	k5eAaImAgMnS	být
odhozen	odhozen	k2eAgMnSc1d1	odhozen
<g/>
,	,	kIx,	,
rám	rám	k1gInSc1	rám
dveří	dveře	k1gFnPc2	dveře
místnosti	místnost	k1gFnSc2	místnost
se	se	k3xPyFc4	se
roztrhl	roztrhnout	k5eAaPmAgInS	roztrhnout
a	a	k8xC	a
samotné	samotný	k2eAgFnPc1d1	samotná
dveře	dveře	k1gFnPc1	dveře
vypadly	vypadnout	k5eAaPmAgFnP	vypadnout
ze	z	k7c2	z
závěsu	závěs	k1gInSc2	závěs
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
proces	proces	k1gInSc1	proces
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
blesku	blesk	k1gInSc2	blesk
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
separace	separace	k1gFnSc1	separace
kladných	kladný	k2eAgInPc2d1	kladný
a	a	k8xC	a
záporných	záporný	k2eAgInPc2d1	záporný
nábojů	náboj	k1gInPc2	náboj
v	v	k7c6	v
mraku	mrak	k1gInSc6	mrak
nebo	nebo	k8xC	nebo
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
procesu	proces	k1gInSc2	proces
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
objektem	objekt	k1gInSc7	objekt
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jedna	jeden	k4xCgFnSc1	jeden
široce	široko	k6eAd1	široko
akceptovaná	akceptovaný	k2eAgFnSc1d1	akceptovaná
teorie	teorie	k1gFnSc1	teorie
je	být	k5eAaImIp3nS	být
polarizační	polarizační	k2eAgInSc4d1	polarizační
mechanismus	mechanismus	k1gInSc4	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mechanismus	mechanismus	k1gInSc1	mechanismus
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
složky	složka	k1gFnSc2	složka
<g/>
:	:	kIx,	:
první	první	k4xOgMnSc1	první
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
padající	padající	k2eAgFnPc1d1	padající
kapky	kapka	k1gFnPc1	kapka
ledu	led	k1gInSc2	led
a	a	k8xC	a
deště	dešť	k1gInSc2	dešť
se	se	k3xPyFc4	se
elektricky	elektricky	k6eAd1	elektricky
polarizují	polarizovat	k5eAaImIp3nP	polarizovat
během	během	k7c2	během
průchodu	průchod	k1gInSc2	průchod
přírodním	přírodní	k2eAgNnSc7d1	přírodní
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
polem	pole	k1gNnSc7	pole
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
srážející	srážející	k2eAgMnSc1d1	srážející
se	se	k3xPyFc4	se
ledové	ledový	k2eAgFnPc1d1	ledová
částice	částice	k1gFnPc1	částice
se	se	k3xPyFc4	se
nabíjejí	nabíjet	k5eAaImIp3nP	nabíjet
elektrostatickou	elektrostatický	k2eAgFnSc7d1	elektrostatická
indukcí	indukce	k1gFnSc7	indukce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
nabití	nabití	k1gNnSc4	nabití
částic	částice	k1gFnPc2	částice
ledu	led	k1gInSc2	led
nebo	nebo	k8xC	nebo
kapek	kapka	k1gFnPc2	kapka
jakýmkoli	jakýkoli	k3yIgInSc7	jakýkoli
mechanismem	mechanismus	k1gInSc7	mechanismus
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc1	práce
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
<g/>
,	,	kIx,	,
když	když	k8xS	když
protikladné	protikladný	k2eAgInPc1d1	protikladný
náboje	náboj	k1gInPc1	náboj
jsou	být	k5eAaImIp3nP	být
odděleny	oddělen	k2eAgInPc1d1	oddělen
a	a	k8xC	a
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
elektrických	elektrický	k2eAgNnPc6d1	elektrické
polích	pole	k1gNnPc6	pole
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Kladně	kladně	k6eAd1	kladně
nabité	nabitý	k2eAgInPc1d1	nabitý
krystaly	krystal	k1gInPc1	krystal
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
stoupat	stoupat	k5eAaImF	stoupat
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
kladný	kladný	k2eAgInSc4d1	kladný
náboj	náboj	k1gInSc4	náboj
vrcholu	vrchol	k1gInSc2	vrchol
mraku	mrak	k1gInSc2	mrak
a	a	k8xC	a
záporně	záporně	k6eAd1	záporně
nabité	nabitý	k2eAgInPc1d1	nabitý
krystaly	krystal	k1gInPc1	krystal
a	a	k8xC	a
kroupy	kroupa	k1gFnPc1	kroupa
padají	padat	k5eAaImIp3nP	padat
do	do	k7c2	do
středních	střední	k2eAgFnPc2d1	střední
a	a	k8xC	a
spodních	spodní	k2eAgFnPc2d1	spodní
vrstev	vrstva	k1gFnPc2	vrstva
mraku	mrak	k1gInSc2	mrak
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
oblast	oblast	k1gFnSc1	oblast
se	s	k7c7	s
záporným	záporný	k2eAgInSc7d1	záporný
nábojem	náboj	k1gInSc7	náboj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
blesk	blesk	k1gInSc4	blesk
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
mraky	mrak	k1gInPc7	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Blesk	blesk	k1gInSc1	blesk
mezi	mezi	k7c7	mezi
mrakem	mrak	k1gInSc7	mrak
a	a	k8xC	a
zemí	zem	k1gFnSc7	zem
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
častý	častý	k2eAgInSc1d1	častý
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
kupovité	kupovitý	k2eAgInPc1d1	kupovitý
mraky	mrak	k1gInPc1	mrak
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
cumulonimbus	cumulonimbus	k1gInSc1	cumulonimbus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
neprodukují	produkovat	k5eNaImIp3nP	produkovat
dost	dost	k6eAd1	dost
ledových	ledový	k2eAgInPc2d1	ledový
krystalů	krystal	k1gInPc2	krystal
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
nejsou	být	k5eNaImIp3nP	být
s	s	k7c7	s
to	ten	k3xDgNnSc4	ten
vytvořit	vytvořit	k5eAaPmF	vytvořit
dost	dost	k6eAd1	dost
nábojové	nábojový	k2eAgFnPc4d1	nábojová
separace	separace	k1gFnPc4	separace
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
blesku	blesk	k1gInSc2	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
nahromadí	nahromadit	k5eAaPmIp3nS	nahromadit
dostatek	dostatek	k1gInSc1	dostatek
kladných	kladný	k2eAgInPc2d1	kladný
a	a	k8xC	a
záporných	záporný	k2eAgInPc2d1	záporný
nábojů	náboj	k1gInPc2	náboj
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
elektrické	elektrický	k2eAgNnSc1d1	elektrické
pole	pole	k1gNnSc1	pole
stane	stanout	k5eAaPmIp3nS	stanout
dostatečně	dostatečně	k6eAd1	dostatečně
silným	silný	k2eAgInSc7d1	silný
<g/>
,	,	kIx,	,
nastane	nastat	k5eAaPmIp3nS	nastat
elektrický	elektrický	k2eAgInSc4d1	elektrický
výboj	výboj	k1gInSc4	výboj
mezi	mezi	k7c7	mezi
mraky	mrak	k1gInPc7	mrak
nebo	nebo	k8xC	nebo
mezi	mezi	k7c7	mezi
mrakem	mrak	k1gInSc7	mrak
a	a	k8xC	a
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
produkuje	produkovat	k5eAaImIp3nS	produkovat
hrom	hrom	k1gInSc1	hrom
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
zvukový	zvukový	k2eAgInSc1d1	zvukový
jev	jev	k1gInSc1	jev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
elektrické	elektrický	k2eAgInPc1d1	elektrický
náboje	náboj	k1gInPc1	náboj
(	(	kIx(	(
<g/>
pocházející	pocházející	k2eAgFnPc1d1	pocházející
i	i	k8xC	i
z	z	k7c2	z
kosmických	kosmický	k2eAgInPc2d1	kosmický
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
urychlovány	urychlovat	k5eAaImNgFnP	urychlovat
elektrickými	elektrický	k2eAgNnPc7d1	elektrické
poli	pole	k1gNnPc7	pole
(	(	kIx(	(
<g/>
neviditelná	viditelný	k2eNgFnSc1d1	neviditelná
fáze	fáze	k1gFnSc1	fáze
blesku	blesk	k1gInSc2	blesk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ionizují	ionizovat	k5eAaBmIp3nP	ionizovat
vzduchové	vzduchový	k2eAgFnPc1d1	vzduchová
molekuly	molekula	k1gFnPc1	molekula
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
srážejí	srážet	k5eAaImIp3nP	srážet
dělajíce	dělat	k5eAaImSgFnP	dělat
vzduch	vzduch	k1gInSc4	vzduch
vodivým	vodivý	k2eAgInSc7d1	vodivý
a	a	k8xC	a
začínají	začínat	k5eAaImIp3nP	začínat
bleskové	bleskový	k2eAgInPc4d1	bleskový
výboje	výboj	k1gInPc4	výboj
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výbojů	výboj	k1gInPc2	výboj
se	se	k3xPyFc4	se
následující	následující	k2eAgFnPc1d1	následující
části	část	k1gFnPc1	část
vzduchu	vzduch	k1gInSc2	vzduch
stávají	stávat	k5eAaImIp3nP	stávat
vodivými	vodivý	k2eAgMnPc7d1	vodivý
<g/>
,	,	kIx,	,
když	když	k8xS	když
elektrony	elektron	k1gInPc4	elektron
a	a	k8xC	a
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
ionty	ion	k1gInPc4	ion
molekul	molekula	k1gFnPc2	molekula
vzduchu	vzduch	k1gInSc2	vzduch
jsou	být	k5eAaImIp3nP	být
odtaženy	odtáhnout	k5eAaPmNgFnP	odtáhnout
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
nuceny	nucen	k2eAgFnPc1d1	nucena
proudit	proudit	k5eAaImF	proudit
v	v	k7c6	v
opačných	opačný	k2eAgInPc6d1	opačný
směrech	směr	k1gInPc6	směr
(	(	kIx(	(
<g/>
krokové	krokový	k2eAgInPc4d1	krokový
kanály	kanál	k1gInPc4	kanál
zvané	zvaný	k2eAgInPc4d1	zvaný
vodič	vodič	k1gInSc4	vodič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vodivá	vodivý	k2eAgNnPc1d1	vodivé
vlákna	vlákno	k1gNnPc1	vlákno
rostou	růst	k5eAaImIp3nP	růst
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
elektrická	elektrický	k2eAgFnSc1d1	elektrická
energie	energie	k1gFnSc1	energie
uložená	uložený	k2eAgFnSc1d1	uložená
v	v	k7c6	v
elektrickém	elektrický	k2eAgNnSc6d1	elektrické
poli	pole	k1gNnSc6	pole
proudí	proudit	k5eAaPmIp3nS	proudit
radiálně	radiálně	k6eAd1	radiálně
dovnitř	dovnitř	k6eAd1	dovnitř
do	do	k7c2	do
vodivého	vodivý	k2eAgNnSc2d1	vodivé
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
nabitý	nabitý	k2eAgInSc1d1	nabitý
krokový	krokový	k2eAgInSc1d1	krokový
kanál	kanál	k1gInSc1	kanál
blízko	blízko	k7c2	blízko
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
protikladné	protikladný	k2eAgInPc1d1	protikladný
náboje	náboj	k1gInPc1	náboj
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nP	objevit
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
elektrické	elektrický	k2eAgNnSc4d1	elektrické
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Elektrické	elektrický	k2eAgNnSc1d1	elektrické
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgNnSc1d2	vyšší
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc6d1	vysoká
budovách	budova	k1gFnPc6	budova
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
elektrické	elektrický	k2eAgNnSc4d1	elektrické
pole	pole	k1gNnSc4	pole
dost	dost	k6eAd1	dost
velké	velký	k2eAgFnPc1d1	velká
<g/>
,	,	kIx,	,
výboj	výboj	k1gInSc1	výboj
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
iniciován	iniciovat	k5eAaBmNgInS	iniciovat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
a	a	k8xC	a
eventuálně	eventuálně	k6eAd1	eventuálně
se	se	k3xPyFc4	se
napojit	napojit	k5eAaPmF	napojit
na	na	k7c4	na
sestupný	sestupný	k2eAgInSc4d1	sestupný
výboj	výboj	k1gInSc4	výboj
z	z	k7c2	z
mraku	mrak	k1gInSc2	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Blesk	blesk	k1gInSc1	blesk
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
též	též	k9	též
v	v	k7c6	v
mracích	mrak	k1gInPc6	mrak
z	z	k7c2	z
popelu	popel	k1gInSc2	popel
při	při	k7c6	při
sopečných	sopečný	k2eAgFnPc6d1	sopečná
erupcích	erupce	k1gFnPc6	erupce
(	(	kIx(	(
<g/>
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
výboje	výboj	k1gInSc2	výboj
až	až	k9	až
100	[number]	k4	100
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
písečných	písečný	k2eAgFnPc6d1	písečná
bouřích	bouř	k1gFnPc6	bouř
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
výboje	výboj	k1gInSc2	výboj
až	až	k9	až
1	[number]	k4	1
m	m	kA	m
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
silnými	silný	k2eAgInPc7d1	silný
lesními	lesní	k2eAgInPc7d1	lesní
požáry	požár	k1gInPc7	požár
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyprodukují	vyprodukovat	k5eAaPmIp3nP	vyprodukovat
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
prachu	prach	k1gInSc2	prach
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
statického	statický	k2eAgInSc2d1	statický
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
blesku	blesk	k1gInSc2	blesk
při	při	k7c6	při
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
(	(	kIx(	(
<g/>
vycházející	vycházející	k2eAgFnPc1d1	vycházející
z	z	k7c2	z
elektrických	elektrický	k2eAgFnPc2d1	elektrická
polí	pole	k1gFnPc2	pole
vytvářených	vytvářený	k2eAgInPc2d1	vytvářený
seizmickým	seizmický	k2eAgNnSc7d1	seizmické
napětím	napětí	k1gNnSc7	napětí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
explozích	exploze	k1gFnPc6	exploze
termonukleárních	termonukleární	k2eAgFnPc2d1	termonukleární
zbraní	zbraň	k1gFnPc2	zbraň
-	-	kIx~	-
např.	např.	kA	např.
vodíková	vodíkový	k2eAgFnSc1d1	vodíková
bomba	bomba	k1gFnSc1	bomba
(	(	kIx(	(
<g/>
jaderné	jaderný	k2eAgInPc1d1	jaderný
bleskové	bleskový	k2eAgInPc1d1	bleskový
výboje	výboj	k1gInPc1	výboj
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
až	až	k9	až
1	[number]	k4	1
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Blesk	blesk	k1gInSc1	blesk
obvykle	obvykle	k6eAd1	obvykle
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
neviditelný	viditelný	k2eNgInSc1d1	neviditelný
negativně	negativně	k6eAd1	negativně
nabitý	nabitý	k2eAgInSc1d1	nabitý
impuls	impuls	k1gInSc1	impuls
z	z	k7c2	z
krokového	krokový	k2eAgInSc2d1	krokový
kanálu	kanál	k1gInSc2	kanál
vyslán	vyslat	k5eAaPmNgInS	vyslat
z	z	k7c2	z
mraku	mrak	k1gInSc2	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
pozitivně	pozitivně	k6eAd1	pozitivně
nabitý	nabitý	k2eAgInSc1d1	nabitý
krokový	krokový	k2eAgInSc1d1	krokový
kanál	kanál	k1gInSc1	kanál
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vyslán	vyslán	k2eAgInSc1d1	vyslán
z	z	k7c2	z
pozitivně	pozitivně	k6eAd1	pozitivně
nabité	nabitý	k2eAgFnSc2d1	nabitá
země	zem	k1gFnSc2	zem
nebo	nebo	k8xC	nebo
mraku	mrak	k1gInSc2	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
2	[number]	k4	2
kanály	kanál	k1gInPc7	kanál
střetnou	střetnout	k5eAaPmIp3nP	střetnout
<g/>
,	,	kIx,	,
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
značně	značně	k6eAd1	značně
vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
vysokého	vysoký	k2eAgInSc2d1	vysoký
proudu	proud	k1gInSc2	proud
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
zpětně	zpětně	k6eAd1	zpětně
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
krokový	krokový	k2eAgInSc1d1	krokový
kanál	kanál	k1gInSc1	kanál
do	do	k7c2	do
mraku	mrak	k1gInSc2	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
"	"	kIx"	"
<g/>
zpětný	zpětný	k2eAgInSc1d1	zpětný
impuls	impuls	k1gInSc1	impuls
<g/>
"	"	kIx"	"
tvoří	tvořit	k5eAaImIp3nP	tvořit
nejjasnější	jasný	k2eAgFnSc4d3	nejjasnější
část	část	k1gFnSc4	část
výboje	výboj	k1gInSc2	výboj
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
opravdu	opravdu	k6eAd1	opravdu
viditelná	viditelný	k2eAgFnSc1d1	viditelná
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
bleskových	bleskový	k2eAgInPc2d1	bleskový
výbojů	výboj	k1gInPc2	výboj
trvá	trvat	k5eAaImIp3nS	trvat
obvykle	obvykle	k6eAd1	obvykle
asi	asi	k9	asi
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
několik	několik	k4yIc1	několik
výbojů	výboj	k1gInPc2	výboj
prochází	procházet	k5eAaImIp3nS	procházet
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
dolů	dolů	k6eAd1	dolů
stejným	stejný	k2eAgInSc7d1	stejný
kanálem	kanál	k1gInSc7	kanál
<g/>
,	,	kIx,	,
způsobujíce	způsobovat	k5eAaImSgFnP	způsobovat
efekt	efekt	k1gInSc4	efekt
blikání	blikání	k1gNnSc3	blikání
<g/>
.	.	kIx.	.
</s>
<s>
Hrom	hrom	k1gInSc1	hrom
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
,	,	kIx,	,
když	když	k8xS	když
výboj	výboj	k1gInSc1	výboj
rychle	rychle	k6eAd1	rychle
zahřeje	zahřát	k5eAaPmIp3nS	zahřát
vodící	vodící	k2eAgInSc4d1	vodící
kanál	kanál	k1gInSc4	kanál
a	a	k8xC	a
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
rázová	rázový	k2eAgFnSc1d1	rázová
vlna	vlna	k1gFnSc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
proudnice	proudnice	k1gFnPc1	proudnice
jsou	být	k5eAaImIp3nP	být
vyslány	vyslat	k5eAaPmNgFnP	vyslat
z	z	k7c2	z
několika	několik	k4yIc2	několik
různých	různý	k2eAgInPc2d1	různý
objektů	objekt	k1gInPc2	objekt
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
a	a	k8xC	a
jen	jen	k8xS	jen
jedna	jeden	k4xCgFnSc1	jeden
se	se	k3xPyFc4	se
spojí	spojit	k5eAaPmIp3nS	spojit
s	s	k7c7	s
vodičem	vodič	k1gInSc7	vodič
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
cestu	cesta	k1gFnSc4	cesta
výboje	výboj	k1gInSc2	výboj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
blesku	blesk	k1gInSc2	blesk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
negativní	negativní	k2eAgInSc1d1	negativní
blesk	blesk	k1gInSc1	blesk
pro	pro	k7c4	pro
vybití	vybití	k1gNnSc4	vybití
negativního	negativní	k2eAgInSc2d1	negativní
náboje	náboj	k1gInSc2	náboj
z	z	k7c2	z
mraku	mrak	k1gInSc2	mrak
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
přes	přes	k7c4	přes
95	[number]	k4	95
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
blesků	blesk	k1gInPc2	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
blesk	blesk	k1gInSc1	blesk
nese	nést	k5eAaImIp3nS	nést
proud	proud	k1gInSc4	proud
30	[number]	k4	30
kA	kA	k?	kA
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
potenciálový	potenciálový	k2eAgInSc4d1	potenciálový
rozdíl	rozdíl	k1gInSc4	rozdíl
asi	asi	k9	asi
100	[number]	k4	100
MV	MV	kA	MV
až	až	k9	až
1	[number]	k4	1
GV	GV	kA	GV
(	(	kIx(	(
<g/>
miliarda	miliarda	k4xCgFnSc1	miliarda
voltů	volt	k1gInPc2	volt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přemístí	přemístit	k5eAaPmIp3nS	přemístit
se	se	k3xPyFc4	se
jím	on	k3xPp3gInSc7	on
náboj	náboj	k1gInSc4	náboj
asi	asi	k9	asi
15	[number]	k4	15
C.	C.	kA	C.
Pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
blesk	blesk	k1gInSc1	blesk
tvoří	tvořit	k5eAaImIp3nS	tvořit
méně	málo	k6eAd2	málo
než	než	k8xS	než
5	[number]	k4	5
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
blesků	blesk	k1gInPc2	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
krokový	krokový	k2eAgInSc1d1	krokový
vodič	vodič	k1gInSc1	vodič
formuje	formovat	k5eAaImIp3nS	formovat
při	při	k7c6	při
pozitivně	pozitivně	k6eAd1	pozitivně
nabitých	nabitý	k2eAgInPc6d1	nabitý
vrcholech	vrchol	k1gInPc6	vrchol
mraků	mrak	k1gInPc2	mrak
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
důsledkem	důsledek	k1gInSc7	důsledek
<g/>
,	,	kIx,	,
že	že	k8xS	že
negativně	negativně	k6eAd1	negativně
nabitá	nabitý	k2eAgFnSc1d1	nabitá
proudnice	proudnice	k1gFnSc1	proudnice
je	být	k5eAaImIp3nS	být
vyslána	vyslat	k5eAaPmNgFnS	vyslat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Celkovým	celkový	k2eAgInSc7d1	celkový
efektem	efekt	k1gInSc7	efekt
je	být	k5eAaImIp3nS	být
vybití	vybití	k1gNnSc1	vybití
pozitivních	pozitivní	k2eAgInPc2d1	pozitivní
nábojů	náboj	k1gInPc2	náboj
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
vedený	vedený	k2eAgInSc1d1	vedený
po	po	k7c6	po
objevu	objev	k1gInSc6	objev
pozitivního	pozitivní	k2eAgInSc2d1	pozitivní
blesku	blesk	k1gInSc2	blesk
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
létech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
blesky	blesk	k1gInPc1	blesk
jsou	být	k5eAaImIp3nP	být
typicky	typicky	k6eAd1	typicky
6	[number]	k4	6
–	–	k?	–
10	[number]	k4	10
<g/>
krát	krát	k6eAd1	krát
silnější	silný	k2eAgInSc4d2	silnější
než	než	k8xS	než
negativní	negativní	k2eAgInSc4d1	negativní
blesky	blesk	k1gInPc4	blesk
<g/>
,	,	kIx,	,
trvají	trvat	k5eAaImIp3nP	trvat
asi	asi	k9	asi
10	[number]	k4	10
<g/>
krát	krát	k6eAd1	krát
déle	dlouho	k6eAd2	dlouho
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
udeřit	udeřit	k5eAaPmF	udeřit
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
mraku	mrak	k1gInSc2	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pozitivního	pozitivní	k2eAgInSc2d1	pozitivní
blesku	blesk	k1gInSc2	blesk
vzniká	vznikat	k5eAaImIp3nS	vznikat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
rádiových	rádiový	k2eAgFnPc2d1	rádiová
vln	vlna	k1gFnPc2	vlna
o	o	k7c6	o
extrémně	extrémně	k6eAd1	extrémně
nízké	nízký	k2eAgFnSc6d1	nízká
frekvenci	frekvence	k1gFnSc6	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
sílu	síla	k1gFnSc4	síla
jsou	být	k5eAaImIp3nP	být
pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
blesky	blesk	k1gInPc1	blesk
mnohem	mnohem	k6eAd1	mnohem
nebezpečnější	bezpečný	k2eNgMnSc1d2	nebezpečnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejsou	být	k5eNaImIp3nP	být
letadla	letadlo	k1gNnPc1	letadlo
navržena	navrhnout	k5eAaPmNgNnP	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odolala	odolat	k5eAaPmAgFnS	odolat
tomuto	tento	k3xDgInSc3	tento
blesku	blesk	k1gInSc3	blesk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
existence	existence	k1gFnSc1	existence
byla	být	k5eAaImAgFnS	být
neznámá	známý	k2eNgFnSc1d1	neznámá
v	v	k7c6	v
době	doba	k1gFnSc6	doba
tvorby	tvorba	k1gFnSc2	tvorba
standardů	standard	k1gInPc2	standard
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
riziko	riziko	k1gNnSc1	riziko
nebylo	být	k5eNaImAgNnS	být
doceněno	docenit	k5eAaPmNgNnS	docenit
až	až	k9	až
do	do	k7c2	do
destrukce	destrukce	k1gFnSc2	destrukce
větroně	větroň	k1gInSc2	větroň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
blesk	blesk	k1gInSc1	blesk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobil	způsobit	k5eAaPmAgInS	způsobit
pád	pád	k1gInSc4	pád
letu	let	k1gInSc2	let
Pan	Pan	k1gMnSc1	Pan
Am	Am	k1gFnSc1	Am
číslo	číslo	k1gNnSc1	číslo
214	[number]	k4	214
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
blesk	blesk	k1gInSc1	blesk
je	být	k5eAaImIp3nS	být
teď	teď	k6eAd1	teď
též	též	k9	též
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
původce	původce	k1gMnPc4	původce
mnohých	mnohý	k2eAgInPc2d1	mnohý
lesních	lesní	k2eAgInPc2d1	lesní
požárů	požár	k1gInPc2	požár
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
blesk	blesk	k1gInSc1	blesk
byl	být	k5eAaImAgInS	být
též	též	k9	též
viděn	vidět	k5eAaImNgMnS	vidět
jak	jak	k8xS	jak
spouští	spouštět	k5eAaImIp3nS	spouštět
výskyt	výskyt	k1gInSc4	výskyt
horních	horní	k2eAgInPc2d1	horní
atmosferických	atmosferický	k2eAgInPc2d1	atmosferický
blesků	blesk	k1gInPc2	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
v	v	k7c6	v
zimních	zimní	k2eAgFnPc6d1	zimní
bouřkách	bouřka	k1gFnPc6	bouřka
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
bouřky	bouřka	k1gFnSc2	bouřka
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
případem	případ	k1gInSc7	případ
vzniku	vznik	k1gInSc2	vznik
blesků	blesk	k1gInPc2	blesk
je	být	k5eAaImIp3nS	být
výbuch	výbuch	k1gInSc1	výbuch
sopky	sopka	k1gFnSc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výbuchu	výbuch	k1gInSc6	výbuch
narážejí	narážet	k5eAaImIp3nP	narážet
částice	částice	k1gFnSc2	částice
chaoticky	chaoticky	k6eAd1	chaoticky
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Popel	popel	k1gInSc1	popel
je	být	k5eAaImIp3nS	být
chrlen	chrlit	k5eAaImNgInS	chrlit
vysokými	vysoký	k2eAgFnPc7d1	vysoká
rychlostmi	rychlost	k1gFnPc7	rychlost
vzhůru	vzhůru	k6eAd1	vzhůru
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
100	[number]	k4	100
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
ionizaci	ionizace	k1gFnSc3	ionizace
<g/>
.	.	kIx.	.
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
nerovnováha	nerovnováha	k1gFnSc1	nerovnováha
se	se	k3xPyFc4	se
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
kratšími	krátký	k2eAgInPc7d2	kratší
elektrickými	elektrický	k2eAgInPc7d1	elektrický
výboji	výboj	k1gInPc7	výboj
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
blesků	blesk	k1gInPc2	blesk
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
desítky	desítka	k1gFnPc4	desítka
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fenomén	fenomén	k1gInSc1	fenomén
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vědci	vědec	k1gMnPc7	vědec
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sledovali	sledovat	k5eAaImAgMnP	sledovat
sopku	sopka	k1gFnSc4	sopka
St.	st.	kA	st.
Augustine	Augustin	k1gMnSc5	Augustin
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
blesky	blesk	k1gInPc1	blesk
byly	být	k5eAaImAgInP	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
činnými	činný	k2eAgFnPc7d1	činná
sopkami	sopka	k1gFnPc7	sopka
Redoubt	Redoubt	k1gInSc1	Redoubt
<g/>
,	,	kIx,	,
Sakurajima	Sakurajima	k1gFnSc1	Sakurajima
nebo	nebo	k8xC	nebo
Eyjafjallajökull	Eyjafjallajökull	k1gInSc1	Eyjafjallajökull
<g/>
.	.	kIx.	.
</s>
<s>
Naposled	naposled	k6eAd1	naposled
byly	být	k5eAaImAgInP	být
vulkanické	vulkanický	k2eAgInPc1d1	vulkanický
blesky	blesk	k1gInPc1	blesk
zaznamenány	zaznamenán	k2eAgInPc1d1	zaznamenán
4.12	[number]	k4	4.12
<g/>
.2015	.2015	k4	.2015
při	při	k7c6	při
výbuchu	výbuch	k1gInSc6	výbuch
Etny	Etna	k1gFnSc2	Etna
<g/>
.	.	kIx.	.
</s>
<s>
Blesk	blesk	k1gInSc1	blesk
může	moct	k5eAaImIp3nS	moct
uhodit	uhodit	k5eAaPmF	uhodit
z	z	k7c2	z
mraku	mrak	k1gInSc2	mrak
vzhůru	vzhůru	k6eAd1	vzhůru
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
barevná	barevný	k2eAgFnSc1d1	barevná
světla	světlo	k1gNnSc2	světlo
šlehající	šlehající	k2eAgNnSc4d1	šlehající
z	z	k7c2	z
vrchní	vrchní	k2eAgFnSc2d1	vrchní
části	část	k1gFnSc2	část
bouřkových	bouřkový	k2eAgInPc2d1	bouřkový
mraků	mrak	k1gInPc2	mrak
–	–	k?	–
oranžové	oranžový	k2eAgInPc4d1	oranžový
kruhy	kruh	k1gInPc4	kruh
s	s	k7c7	s
modrými	modrý	k2eAgNnPc7d1	modré
rameny	rameno	k1gNnPc7	rameno
<g/>
,	,	kIx,	,
záblesky	záblesk	k1gInPc1	záblesk
modrých	modrý	k2eAgNnPc2d1	modré
světel	světlo	k1gNnPc2	světlo
a	a	k8xC	a
obrovské	obrovský	k2eAgFnPc4d1	obrovská
rudé	rudý	k2eAgFnPc4d1	rudá
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
světelné	světelný	k2eAgInPc1d1	světelný
úkazy	úkaz	k1gInPc1	úkaz
vystřelují	vystřelovat	k5eAaImIp3nP	vystřelovat
až	až	k9	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
95	[number]	k4	95
kilometrů	kilometr	k1gInPc2	kilometr
buď	buď	k8xC	buď
po	po	k7c6	po
jednom	jeden	k4xCgMnSc6	jeden
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
celých	celý	k2eAgFnPc6d1	celá
sériích	série	k1gFnPc6	série
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ještě	ještě	k6eAd1	ještě
vzácnější	vzácný	k2eAgInPc4d2	vzácnější
úkazy	úkaz	k1gInPc4	úkaz
pak	pak	k6eAd1	pak
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
modré	modrý	k2eAgInPc1d1	modrý
záblesky	záblesk	k1gInPc1	záblesk
<g/>
,	,	kIx,	,
putující	putující	k2eAgMnSc1d1	putující
asi	asi	k9	asi
stokilometrovou	stokilometrový	k2eAgFnSc7d1	stokilometrová
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
explodující	explodující	k2eAgInPc1d1	explodující
světelné	světelný	k2eAgInPc1d1	světelný
kotouče	kotouč	k1gInPc1	kotouč
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
"	"	kIx"	"
<g/>
skřítky	skřítek	k1gMnPc7	skřítek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
atmosférické	atmosférický	k2eAgInPc1d1	atmosférický
výboje	výboj	k1gInPc1	výboj
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
silném	silný	k2eAgNnSc6d1	silné
elektrickém	elektrický	k2eAgNnSc6d1	elektrické
poli	pole	k1gNnSc6	pole
nad	nad	k7c7	nad
bouřkovými	bouřkový	k2eAgInPc7d1	bouřkový
oblaky	oblak	k1gInPc7	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
A.	A.	kA	A.
Theurer	Theurer	k1gMnSc1	Theurer
<g/>
:	:	kIx,	:
Ochrana	ochrana	k1gFnSc1	ochrana
budov	budova	k1gFnPc2	budova
proti	proti	k7c3	proti
blesku	blesk	k1gInSc3	blesk
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Topič	topič	k1gMnSc1	topič
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1918	[number]	k4	1918
Peter	Peter	k1gMnSc1	Peter
Hasse	Hasse	k1gFnSc1	Hasse
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
a	a	k8xC	a
vnější	vnější	k2eAgFnSc1d1	vnější
ochrana	ochrana	k1gFnSc1	ochrana
zařízení	zařízení	k1gNnSc2	zařízení
před	před	k7c7	před
účinky	účinek	k1gInPc7	účinek
blesku	blesk	k1gInSc2	blesk
a	a	k8xC	a
přepětím	přepětí	k1gNnSc7	přepětí
<g/>
,	,	kIx,	,
Elektromanagement	Elektromanagement	k1gInSc1	Elektromanagement
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1994	[number]	k4	1994
Elektrický	elektrický	k2eAgInSc1d1	elektrický
výboj	výboj	k1gInSc1	výboj
Blesk	blesk	k1gInSc1	blesk
(	(	kIx(	(
<g/>
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
blesk	blesk	k1gInSc1	blesk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
blesk	blesk	k1gInSc1	blesk
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Czech	Czech	k1gInSc1	Czech
Thunderstorm	Thunderstorm	k1gInSc1	Thunderstorm
Research	Researcha	k1gFnPc2	Researcha
Association	Association	k1gInSc1	Association
Amateur	Amateur	k1gMnSc1	Amateur
Stormchasing	Stormchasing	k1gInSc1	Stormchasing
Society	societa	k1gFnSc2	societa
–	–	k?	–
počasí	počasí	k1gNnSc1	počasí
<g/>
:	:	kIx,	:
bouřky	bouřka	k1gFnPc1	bouřka
<g/>
,	,	kIx,	,
blesky	blesk	k1gInPc1	blesk
<g/>
,	,	kIx,	,
oblaky	oblak	k1gInPc1	oblak
a	a	k8xC	a
podobné	podobný	k2eAgInPc1d1	podobný
jevy	jev	k1gInPc1	jev
Detekce	detekce	k1gFnSc2	detekce
blesků	blesk	k1gInPc2	blesk
ČHMÚ	ČHMÚ	kA	ČHMÚ
Videa	video	k1gNnSc2	video
a	a	k8xC	a
fotografie	fotografia	k1gFnSc2	fotografia
z	z	k7c2	z
bouřek	bouřka	k1gFnPc2	bouřka
–	–	k?	–
Lovci	lovec	k1gMnPc1	lovec
bouřek	bouřka	k1gFnPc2	bouřka
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
ČR	ČR	kA	ČR
Video	video	k1gNnSc1	video
<g/>
:	:	kIx,	:
300	[number]	k4	300
<g/>
×	×	k?	×
zpomalený	zpomalený	k2eAgInSc1d1	zpomalený
blesk	blesk	k1gInSc1	blesk
Video	video	k1gNnSc1	video
<g/>
:	:	kIx,	:
úder	úder	k1gInSc1	úder
blesku	blesk	k1gInSc2	blesk
do	do	k7c2	do
země	zem	k1gFnSc2	zem
Online	Onlin	k1gInSc5	Onlin
přednáška	přednáška	k1gFnSc1	přednáška
-	-	kIx~	-
Ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
se	se	k3xPyFc4	se
blýská	blýskat	k5eAaImIp3nS	blýskat
-	-	kIx~	-
Ing.	ing.	kA	ing.
Ivana	Ivana	k1gFnSc1	Ivana
Kolmašová	Kolmašová	k1gFnSc1	Kolmašová
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
</s>
