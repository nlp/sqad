<s>
Japonština	japonština	k1gFnSc1	japonština
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
日	日	k?	日
<g/>
,	,	kIx,	,
nihongo	nihongo	k1gMnSc1	nihongo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
mluví	mluvit	k5eAaImIp3nS	mluvit
přibližně	přibližně	k6eAd1	přibližně
130	[number]	k4	130
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
pak	pak	k6eAd1	pak
v	v	k7c6	v
komunitách	komunita	k1gFnPc6	komunita
japonských	japonský	k2eAgMnPc2d1	japonský
emigrantů	emigrant	k1gMnPc2	emigrant
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Japonština	japonština	k1gFnSc1	japonština
je	být	k5eAaImIp3nS	být
aglutinační	aglutinační	k2eAgInSc4d1	aglutinační
jazyk	jazyk	k1gInSc4	jazyk
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
úrovněmi	úroveň	k1gFnPc7	úroveň
zdvořilosti	zdvořilost	k1gFnPc4	zdvořilost
promluvy	promluva	k1gFnPc4	promluva
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
japonského	japonský	k2eAgNnSc2d1	Japonské
písma	písmo	k1gNnSc2	písmo
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejsložitějším	složitý	k2eAgNnPc3d3	nejsložitější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
kombinace	kombinace	k1gFnSc1	kombinace
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
(	(	kIx(	(
<g/>
kandži	kandzat	k5eAaPmIp1nS	kandzat
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
fonetické	fonetický	k2eAgFnPc1d1	fonetická
slabičné	slabičný	k2eAgFnPc1d1	slabičná
abecedy	abeceda	k1gFnPc1	abeceda
(	(	kIx(	(
<g/>
hiragana	hiragana	k1gFnSc1	hiragana
a	a	k8xC	a
katakana	katakana	k1gFnSc1	katakana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k8xC	i
latinka	latinka	k1gFnSc1	latinka
(	(	kIx(	(
<g/>
rómadži	rómadzat	k5eAaPmIp1nS	rómadzat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přejímána	přejímán	k2eAgNnPc1d1	přejímáno
slova	slovo	k1gNnPc1	slovo
z	z	k7c2	z
čínštiny	čínština	k1gFnSc2	čínština
(	(	kIx(	(
<g/>
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
rozsahu	rozsah	k1gInSc6	rozsah
z	z	k7c2	z
korejštiny	korejština	k1gFnSc2	korejština
a	a	k8xC	a
ainštiny	ainština	k1gFnSc2	ainština
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prvních	první	k4xOgInPc2	první
kontaktů	kontakt	k1gInPc2	kontakt
s	s	k7c7	s
Evropany	Evropan	k1gMnPc7	Evropan
(	(	kIx(	(
<g/>
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
do	do	k7c2	do
japonštiny	japonština	k1gFnSc2	japonština
pronikají	pronikat	k5eAaImIp3nP	pronikat
i	i	k9	i
výrazy	výraz	k1gInPc1	výraz
z	z	k7c2	z
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
gairaigo	gairaigo	k1gNnSc4	gairaigo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
z	z	k7c2	z
portugalštiny	portugalština	k1gFnSc2	portugalština
a	a	k8xC	a
nizozemštiny	nizozemština	k1gFnSc2	nizozemština
a	a	k8xC	a
od	od	k7c2	od
reforem	reforma	k1gFnPc2	reforma
Meidži	Meidž	k1gFnSc3	Meidž
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
francouzštiny	francouzština	k1gFnSc2	francouzština
a	a	k8xC	a
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
drtivě	drtivě	k6eAd1	drtivě
převažuje	převažovat	k5eAaImIp3nS	převažovat
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Japonština	japonština	k1gFnSc1	japonština
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
japonsko-rjúkjúských	japonskojúkjúský	k2eAgInPc2d1	japonsko-rjúkjúský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
rodiny	rodina	k1gFnSc2	rodina
patří	patřit	k5eAaImIp3nP	patřit
i	i	k8xC	i
jazyky	jazyk	k1gInPc1	jazyk
souostroví	souostroví	k1gNnSc2	souostroví
Rjúkjú	Rjúkjú	k1gNnSc2	Rjúkjú
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
považovány	považován	k2eAgInPc4d1	považován
za	za	k7c4	za
dialekty	dialekt	k1gInPc4	dialekt
(	(	kIx(	(
<g/>
ač	ač	k8xS	ač
nesrozumitelné	srozumitelný	k2eNgFnPc1d1	nesrozumitelná
<g/>
)	)	kIx)	)
japonštiny	japonština	k1gFnPc1	japonština
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
o	o	k7c6	o
japonštině	japonština	k1gFnSc6	japonština
často	často	k6eAd1	často
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c6	o
jazykovém	jazykový	k2eAgInSc6d1	jazykový
izolátu	izolát	k1gInSc6	izolát
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
totiž	totiž	k9	totiž
nebyl	být	k5eNaImAgInS	být
prokazatelně	prokazatelně	k6eAd1	prokazatelně
doložen	doložen	k2eAgInSc1d1	doložen
prajazyk	prajazyk	k1gInSc1	prajazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
japonštinu	japonština	k1gFnSc4	japonština
genealogicky	genealogicky	k6eAd1	genealogicky
vázat	vázat	k5eAaImF	vázat
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
jazykům	jazyk	k1gInPc3	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgFnPc4d3	nejčastější
teorie	teorie	k1gFnPc4	teorie
o	o	k7c6	o
příbuznosti	příbuznost	k1gFnSc6	příbuznost
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Příbuznost	příbuznost	k1gFnSc1	příbuznost
s	s	k7c7	s
jazyky	jazyk	k1gInPc7	jazyk
severní	severní	k2eAgFnSc2d1	severní
Asie	Asie	k1gFnSc2	Asie
Příbuznost	příbuznost	k1gFnSc1	příbuznost
s	s	k7c7	s
korejštinou	korejština	k1gFnSc7	korejština
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
přiřazována	přiřazovat	k5eAaImNgFnS	přiřazovat
k	k	k7c3	k
altajské	altajský	k2eAgFnSc3d1	Altajská
větvi	větev	k1gFnSc3	větev
<g/>
)	)	kIx)	)
Altajská	altajský	k2eAgFnSc1d1	Altajská
nebo	nebo	k8xC	nebo
uralo-altajská	uraloltajský	k2eAgFnSc1d1	uralo-altajský
větev	větev	k1gFnSc1	větev
Předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
japonština	japonština	k1gFnSc1	japonština
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
austronéského	austronéský	k2eAgMnSc2d1	austronéský
substrata	substrat	k1gMnSc2	substrat
a	a	k8xC	a
altajského	altajský	k2eAgMnSc2d1	altajský
superstrata	superstrat	k1gMnSc2	superstrat
Rjúkjúština	Rjúkjúština	k1gFnSc1	Rjúkjúština
–	–	k?	–
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
jazyk	jazyk	k1gInSc4	jazyk
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
původně	původně	k6eAd1	původně
totožný	totožný	k2eAgInSc1d1	totožný
s	s	k7c7	s
japonštinou	japonština	k1gFnSc7	japonština
(	(	kIx(	(
<g/>
dialekt	dialekt	k1gInSc1	dialekt
<g/>
)	)	kIx)	)
Příbuznost	příbuznost	k1gFnSc1	příbuznost
s	s	k7c7	s
jazyky	jazyk	k1gInPc7	jazyk
jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
Malajsko-polynéská	malajskoolynéský	k2eAgFnSc1d1	malajsko-polynéský
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
austro-asiatická	austrosiatický	k2eAgFnSc1d1	austro-asiatický
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
tibeto-burmská	tibetourmský	k2eAgFnSc1d1	tibeto-burmský
hypotéza	hypotéza	k1gFnSc1	hypotéza
Hypotézy	hypotéza	k1gFnSc2	hypotéza
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
japonštinu	japonština	k1gFnSc4	japonština
spojují	spojovat	k5eAaImIp3nP	spojovat
s	s	k7c7	s
indoevropskými	indoevropský	k2eAgInPc7d1	indoevropský
jazyky	jazyk	k1gInPc7	jazyk
Ostatní	ostatní	k2eAgFnSc2d1	ostatní
hypotézy	hypotéza	k1gFnSc2	hypotéza
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
věnovaly	věnovat	k5eAaImAgInP	věnovat
komparaci	komparace	k1gFnSc4	komparace
japonštiny	japonština	k1gFnSc2	japonština
s	s	k7c7	s
perštinou	perština	k1gFnSc7	perština
<g/>
,	,	kIx,	,
řečtinou	řečtina	k1gFnSc7	řečtina
<g/>
,	,	kIx,	,
baskičtinou	baskičtina	k1gFnSc7	baskičtina
a	a	k8xC	a
sumerštinou	sumerština	k1gFnSc7	sumerština
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
dvě	dva	k4xCgFnPc1	dva
teorie	teorie	k1gFnPc1	teorie
jsou	být	k5eAaImIp3nP	být
převážnou	převážný	k2eAgFnSc7d1	převážná
většinou	většina	k1gFnSc7	většina
lingvistů	lingvista	k1gMnPc2	lingvista
odmítané	odmítaný	k2eAgNnSc1d1	odmítané
<g/>
.	.	kIx.	.
</s>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
diskutovanou	diskutovaný	k2eAgFnSc7d1	diskutovaná
a	a	k8xC	a
nejsystematičtější	systematický	k2eAgFnSc7d3	nejsystematičtější
je	být	k5eAaImIp3nS	být
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
řadí	řadit	k5eAaImIp3nS	řadit
japonštinu	japonština	k1gFnSc4	japonština
k	k	k7c3	k
altajským	altajský	k2eAgInPc3d1	altajský
jazykům	jazyk	k1gInPc3	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
japonštinu	japonština	k1gFnSc4	japonština
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
korejštinou	korejština	k1gFnSc7	korejština
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
definitivně	definitivně	k6eAd1	definitivně
prokázána	prokázat	k5eAaPmNgFnS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Příbuznost	příbuznost	k1gFnSc1	příbuznost
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
gramatická	gramatický	k2eAgFnSc1d1	gramatická
<g/>
)	)	kIx)	)
moderní	moderní	k2eAgFnSc2d1	moderní
japonštiny	japonština	k1gFnSc2	japonština
a	a	k8xC	a
korejštiny	korejština	k1gFnSc2	korejština
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
tak	tak	k6eAd1	tak
blízká	blízký	k2eAgFnSc1d1	blízká
<g/>
,	,	kIx,	,
že	že	k8xS	že
často	často	k6eAd1	často
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
doslovný	doslovný	k2eAgInSc4d1	doslovný
překlad	překlad	k1gInSc4	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Předmoderní	Předmoderní	k2eAgNnPc1d1	Předmoderní
jazyková	jazykový	k2eAgNnPc1d1	jazykové
stadia	stadion	k1gNnPc1	stadion
ovšem	ovšem	k9	ovšem
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
velké	velký	k2eAgInPc1d1	velký
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
starou	starý	k2eAgFnSc7d1	stará
japonštinou	japonština	k1gFnSc7	japonština
a	a	k8xC	a
korejštinou	korejština	k1gFnSc7	korejština
a	a	k8xC	a
napovídají	napovídat	k5eAaBmIp3nP	napovídat
ke	k	k7c3	k
genealogické	genealogický	k2eAgFnSc3d1	genealogická
nepříbuznosti	nepříbuznost	k1gFnSc3	nepříbuznost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
moderní	moderní	k2eAgFnSc1d1	moderní
teorie	teorie	k1gFnSc1	teorie
stavící	stavící	k2eAgFnSc1d1	stavící
na	na	k7c6	na
altajských	altajský	k2eAgFnPc6d1	Altajská
hypotézách	hypotéza	k1gFnPc6	hypotéza
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
japonština	japonština	k1gFnSc1	japonština
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
austronéského	austronéský	k2eAgMnSc2d1	austronéský
substrata	substrat	k1gMnSc2	substrat
a	a	k8xC	a
altajského	altajský	k2eAgMnSc2d1	altajský
superstrata	superstrat	k1gMnSc2	superstrat
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
zastánců	zastánce	k1gMnPc2	zastánce
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
je	být	k5eAaImIp3nS	být
Susumu	Susum	k1gInSc2	Susum
Óno	Óno	k1gMnSc2	Óno
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
období	období	k1gNnSc2	období
Džómon	Džómona	k1gFnPc2	Džómona
(	(	kIx(	(
<g/>
8000	[number]	k4	8000
–	–	k?	–
400	[number]	k4	400
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
na	na	k7c6	na
japonských	japonský	k2eAgInPc6d1	japonský
ostrovech	ostrov	k1gInPc6	ostrov
mluvilo	mluvit	k5eAaImAgNnS	mluvit
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
fonologický	fonologický	k2eAgInSc1d1	fonologický
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
4	[number]	k4	4
vokály	vokál	k1gInPc4	vokál
<g/>
,	,	kIx,	,
otevřené	otevřený	k2eAgFnPc4d1	otevřená
slabiky	slabika	k1gFnPc4	slabika
<g/>
)	)	kIx)	)
podobný	podobný	k2eAgInSc4d1	podobný
polynéským	polynéský	k2eAgMnPc3d1	polynéský
jazykům	jazyk	k1gInPc3	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
300	[number]	k4	300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
kultura	kultura	k1gFnSc1	kultura
Jajoi	Jajo	k1gFnSc2	Jajo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
na	na	k7c6	na
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
pevnině	pevnina	k1gFnSc6	pevnina
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
jazyk	jazyk	k1gInSc1	jazyk
s	s	k7c7	s
gramatickou	gramatický	k2eAgFnSc7d1	gramatická
strukturou	struktura	k1gFnSc7	struktura
altajských	altajský	k2eAgInPc2d1	altajský
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
vokálovou	vokálový	k2eAgFnSc7d1	vokálová
harmonií	harmonie	k1gFnSc7	harmonie
začal	začít	k5eAaPmAgMnS	začít
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
mluvilo	mluvit	k5eAaImAgNnS	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Migrace	migrace	k1gFnSc1	migrace
probíhala	probíhat	k5eAaImAgFnS	probíhat
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nový	nový	k2eAgInSc1d1	nový
jazyk	jazyk	k1gInSc1	jazyk
nevyřadil	vyřadit	k5eNaPmAgInS	vyřadit
existující	existující	k2eAgFnPc4d1	existující
lexikální	lexikální	k2eAgFnPc4d1	lexikální
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgMnSc1d1	schopen
změnit	změnit	k5eAaPmF	změnit
jeho	jeho	k3xOp3gFnSc4	jeho
gramatickou	gramatický	k2eAgFnSc4d1	gramatická
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Geneticky	geneticky	k6eAd1	geneticky
tedy	tedy	k9	tedy
japonštinu	japonština	k1gFnSc4	japonština
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
altajským	altajský	k2eAgInPc3d1	altajský
jazykům	jazyk	k1gInPc3	jazyk
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zbytky	zbytek	k1gInPc4	zbytek
austronéského	austronéský	k2eAgNnSc2d1	austronéský
lexika	lexikon	k1gNnSc2	lexikon
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
odpovědět	odpovědět	k5eAaPmF	odpovědět
i	i	k9	i
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
altajských	altajský	k2eAgMnPc2d1	altajský
lexikálních	lexikální	k2eAgMnPc2d1	lexikální
kognátů	kognát	k1gMnPc2	kognát
tak	tak	k6eAd1	tak
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Polivanov	Polivanov	k1gInSc1	Polivanov
uvádí	uvádět	k5eAaImIp3nS	uvádět
několik	několik	k4yIc1	několik
znaků	znak	k1gInPc2	znak
polynéských	polynéský	k2eAgInPc2d1	polynéský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
podporují	podporovat	k5eAaImIp3nP	podporovat
austronéskou	austronéský	k2eAgFnSc4d1	austronéský
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
:	:	kIx,	:
Typická	typický	k2eAgFnSc1d1	typická
dvouslabičnost	dvouslabičnost	k1gFnSc1	dvouslabičnost
(	(	kIx(	(
<g/>
bisylabicita	bisylabicita	k1gFnSc1	bisylabicita
<g/>
)	)	kIx)	)
lexikálního	lexikální	k2eAgInSc2d1	lexikální
morfému	morfém	k1gInSc2	morfém
(	(	kIx(	(
<g/>
kata	kata	k9	kata
<g/>
,	,	kIx,	,
naka	naka	k1gFnSc1	naka
<g/>
)	)	kIx)	)
Přítomnost	přítomnost	k1gFnSc1	přítomnost
prefixů	prefix	k1gInPc2	prefix
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
japonštinu	japonština	k1gFnSc4	japonština
od	od	k7c2	od
plně	plně	k6eAd1	plně
sufixálních	sufixální	k2eAgInPc2d1	sufixální
altajských	altajský	k2eAgInPc2d1	altajský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
)	)	kIx)	)
–	–	k?	–
japonská	japonský	k2eAgFnSc1d1	japonská
sufixace	sufixace	k1gFnSc1	sufixace
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
původu	původ	k1gInSc2	původ
Reduplikace	reduplikace	k1gFnSc2	reduplikace
Jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
vokalický	vokalický	k2eAgInSc1d1	vokalický
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
absence	absence	k1gFnSc1	absence
vokálové	vokálový	k2eAgFnSc2d1	vokálová
harmonie	harmonie	k1gFnSc2	harmonie
Otevřené	otevřený	k2eAgFnSc2d1	otevřená
slabiky	slabika	k1gFnSc2	slabika
Postupná	postupný	k2eAgFnSc1d1	postupná
ztráta	ztráta	k1gFnSc1	ztráta
účasti	účast	k1gFnSc2	účast
rtů	ret	k1gInPc2	ret
<g/>
.	.	kIx.	.
</s>
<s>
Japonština	japonština	k1gFnSc1	japonština
<g/>
:	:	kIx,	:
*	*	kIx~	*
<g/>
p	p	k?	p
<g/>
:	:	kIx,	:
p	p	k?	p
>	>	kIx)	>
f	f	k?	f
(	(	kIx(	(
<g/>
ɸ	ɸ	k?	ɸ
<g/>
)	)	kIx)	)
>	>	kIx)	>
h	h	k?	h
<g/>
;	;	kIx,	;
stejný	stejný	k2eAgInSc1d1	stejný
proces	proces	k1gInSc1	proces
v	v	k7c6	v
polynéských	polynéský	k2eAgInPc6d1	polynéský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
:	:	kIx,	:
*	*	kIx~	*
apui	apui	k1gNnSc1	apui
>	>	kIx)	>
api	api	k?	api
>	>	kIx)	>
afi	afi	k?	afi
>	>	kIx)	>
ahi	ahi	k?	ahi
Počet	počet	k1gInSc1	počet
samohlásek	samohláska	k1gFnPc2	samohláska
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
dialektu	dialekt	k1gInSc2	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
systémy	systém	k1gInPc4	systém
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vokály	vokál	k1gInPc7	vokál
/	/	kIx~	/
<g/>
i	i	k9	i
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
a	a	k8xC	a
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
dialekt	dialekt	k1gInSc1	dialekt
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Jonaguni	Jonagun	k1gMnPc1	Jonagun
<g/>
)	)	kIx)	)
a	a	k8xC	a
systémy	systém	k1gInPc1	systém
až	až	k9	až
s	s	k7c7	s
osmi	osm	k4xCc7	osm
prvky	prvek	k1gInPc7	prvek
/	/	kIx~	/
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
æ	æ	k?	æ
<g/>
,	,	kIx,	,
centralizované	centralizovaný	k2eAgFnSc2d1	centralizovaná
ö	ö	k?	ö
<g/>
,	,	kIx,	,
ü	ü	k?	ü
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
dialekt	dialekt	k1gInSc1	dialekt
města	město	k1gNnSc2	město
Nagoja	Nagoj	k1gInSc2	Nagoj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Standardizovaný	standardizovaný	k2eAgInSc1d1	standardizovaný
tokijský	tokijský	k2eAgInSc1d1	tokijský
dialekt	dialekt	k1gInSc1	dialekt
má	mít	k5eAaImIp3nS	mít
pětičlenný	pětičlenný	k2eAgInSc4d1	pětičlenný
systém	systém	k1gInSc4	systém
/	/	kIx~	/
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
o	o	k7c4	o
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
/	/	kIx~	/
<g/>
u	u	k7c2	u
<g/>
/	/	kIx~	/
je	být	k5eAaImIp3nS	být
realizováno	realizovat	k5eAaBmNgNnS	realizovat
jako	jako	k8xS	jako
nezaokrouhlený	zaokrouhlený	k2eNgInSc1d1	zaokrouhlený
zadní	zadní	k2eAgInSc1d1	zadní
vysoký	vysoký	k2eAgInSc1d1	vysoký
vokál	vokál	k1gInSc1	vokál
[	[	kIx(	[
<g/>
ɯ	ɯ	k?	ɯ
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Vokalická	vokalický	k2eAgFnSc1d1	vokalický
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
fonologicky	fonologicky	k6eAd1	fonologicky
distinktivní	distinktivní	k2eAgNnSc1d1	distinktivní
<g/>
:	:	kIx,	:
月	月	k?	月
cuki	cuke	k1gFnSc4	cuke
'	'	kIx"	'
<g/>
měsíc	měsíc	k1gInSc4	měsíc
<g/>
'	'	kIx"	'
a	a	k8xC	a
通	通	k?	通
cúki	cúki	k1gNnSc1	cúki
'	'	kIx"	'
<g/>
proudění	proudění	k1gNnSc1	proudění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgInPc1d1	vysoký
vokály	vokál	k1gInPc1	vokál
/	/	kIx~	/
<g/>
i	i	k9	i
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
/	/	kIx~	/
podléhají	podléhat	k5eAaImIp3nP	podléhat
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
dialektech	dialekt	k1gInPc6	dialekt
desonorizaci	desonorizace	k1gFnSc4	desonorizace
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
stávají	stávat	k5eAaImIp3nP	stávat
se	se	k3xPyFc4	se
neznělými	znělý	k2eNgMnPc7d1	neznělý
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
znělosti	znělost	k1gFnSc2	znělost
dochází	docházet	k5eAaImIp3nS	docházet
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
v	v	k7c6	v
neznělém	znělý	k2eNgInSc6d1	neznělý
hláskovém	hláskový	k2eAgInSc6d1	hláskový
kontextu	kontext	k1gInSc6	kontext
nebo	nebo	k8xC	nebo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
[	[	kIx(	[
<g/>
kɯ	kɯ	k?	kɯ
<g/>
̥	̥	k?	̥
<g/>
t	t	k?	t
<g/>
͡	͡	k?	͡
<g/>
sɯ	sɯ	k?	sɯ
<g/>
]	]	kIx)	]
靴	靴	k?	靴
kucu	kucus	k1gInSc2	kucus
'	'	kIx"	'
<g/>
boty	bota	k1gFnPc1	bota
<g/>
'	'	kIx"	'
nebo	nebo	k8xC	nebo
[	[	kIx(	[
<g/>
haɕ	haɕ	k?	haɕ
<g/>
̥	̥	k?	̥
<g/>
]	]	kIx)	]
箸	箸	k?	箸
haši	haše	k1gFnSc4	haše
'	'	kIx"	'
<g/>
hůlky	hůlka	k1gFnSc2	hůlka
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
příklady	příklad	k1gInPc1	příklad
frekventovaných	frekventovaný	k2eAgNnPc2d1	frekventované
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
podléhají	podléhat	k5eAaImIp3nP	podléhat
desonorizaci	desonorizace	k1gFnSc4	desonorizace
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
desɯ	desɯ	k?	desɯ
<g/>
̥	̥	k?	̥
<g/>
]	]	kIx)	]
で	で	k?	で
desu	desu	k5eAaPmIp1nS	desu
slovesná	slovesný	k2eAgFnSc1d1	slovesná
spona	spona	k1gFnSc1	spona
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
gozaimasɯ	gozaimasɯ	k?	gozaimasɯ
<g/>
̥	̥	k?	̥
<g/>
]	]	kIx)	]
ご	ご	k?	ご
gozaimasu	gozaimas	k1gInSc2	gozaimas
'	'	kIx"	'
<g/>
existovat	existovat	k5eAaImF	existovat
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
zdvořile	zdvořile	k6eAd1	zdvořile
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
doː	doː	k?	doː
<g/>
̥	̥	k?	̥
<g/>
te	te	k?	te
<g/>
]	]	kIx)	]
ど	ど	k?	ど
dóšite	dóšit	k1gInSc5	dóšit
'	'	kIx"	'
<g/>
proč	proč	k6eAd1	proč
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Souhlásky	souhláska	k1gFnPc1	souhláska
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mírou	míra	k1gFnSc7	míra
alofonie	alofonie	k1gFnSc2	alofonie
<g/>
:	:	kIx,	:
/	/	kIx~	/
<g/>
t	t	k?	t
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
/	/	kIx~	/
jsou	být	k5eAaImIp3nP	být
před	před	k7c4	před
/	/	kIx~	/
<g/>
i	i	k8xC	i
<g/>
/	/	kIx~	/
realizovány	realizovat	k5eAaBmNgFnP	realizovat
jako	jako	k8xC	jako
alveo-palatální	alveoalatální	k2eAgFnSc1d1	alveo-palatální
afrikáta	afrikáta	k1gFnSc1	afrikáta
[	[	kIx(	[
<g/>
t	t	k?	t
<g/>
͡	͡	k?	͡
<g/>
ɕ	ɕ	k?	ɕ
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
͡	͡	k?	͡
<g/>
ʑ	ʑ	k?	ʑ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
před	před	k7c7	před
/	/	kIx~	/
<g/>
u	u	k7c2	u
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
jako	jako	k8xS	jako
alveolární	alveolární	k2eAgFnSc1d1	alveolární
afrikáta	afrikáta	k1gFnSc1	afrikáta
[	[	kIx(	[
<g/>
t	t	k?	t
<g/>
͡	͡	k?	͡
<g/>
sɯ	sɯ	k?	sɯ
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
͡	͡	k?	͡
<g/>
zɯ	zɯ	k?	zɯ
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
/	/	kIx~	/
<g/>
N	N	kA	N
<g/>
/	/	kIx~	/
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
slov	slovo	k1gNnPc2	slovo
realizováno	realizován	k2eAgNnSc4d1	realizováno
jako	jako	k9	jako
uvulární	uvulární	k2eAgFnSc1d1	uvulární
nazála	nazála	k1gFnSc1	nazála
/	/	kIx~	/
<g/>
hoN	hon	k1gInSc1	hon
<g/>
/	/	kIx~	/
→	→	k?	→
[	[	kIx(	[
<g/>
hoɴ	hoɴ	k?	hoɴ
<g/>
]	]	kIx)	]
本	本	k?	本
hon	hon	k1gInSc1	hon
'	'	kIx"	'
<g/>
kniha	kniha	k1gFnSc1	kniha
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
také	také	k9	také
realizován	realizovat	k5eAaBmNgInS	realizovat
jako	jako	k8xC	jako
nazalizace	nazalizace	k1gFnSc1	nazalizace
předchozího	předchozí	k2eAgInSc2d1	předchozí
vokálu	vokál	k1gInSc2	vokál
[	[	kIx(	[
<g/>
hõ	hõ	k?	hõ
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
po	po	k7c6	po
/	/	kIx~	/
<g/>
N	N	kA	N
<g/>
/	/	kIx~	/
následuje	následovat	k5eAaImIp3nS	následovat
kontinuant	kontinuant	k1gMnSc1	kontinuant
<g/>
,	,	kIx,	,
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc3	jeho
artikulačním	artikulační	k2eAgInPc3d1	artikulační
pohybům	pohyb	k1gInPc3	pohyb
/	/	kIx~	/
<g/>
hoN-jaku	hoNak	k1gInSc6	hoN-jak
<g/>
/	/	kIx~	/
→	→	k?	→
[	[	kIx(	[
<g/>
hoj	hojit	k5eAaImRp2nS	hojit
<g/>
̃	̃	k?	̃
<g/>
jakɯ	jakɯ	k?	jakɯ
<g/>
]	]	kIx)	]
翻	翻	k?	翻
honjaku	honjak	k1gInSc2	honjak
'	'	kIx"	'
<g/>
překlad	překlad	k1gInSc1	překlad
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
realizován	realizovat	k5eAaBmNgInS	realizovat
jako	jako	k9	jako
[	[	kIx(	[
<g/>
m	m	kA	m
<g/>
]	]	kIx)	]
před	před	k7c7	před
bilabiálními	bilabiální	k2eAgInPc7d1	bilabiální
konsonanty	konsonant	k1gInPc7	konsonant
/	/	kIx~	/
<g/>
siNbuN	siNbuN	k?	siNbuN
<g/>
/	/	kIx~	/
→	→	k?	→
[	[	kIx(	[
<g/>
ɕ	ɕ	k?	ɕ
<g/>
]	]	kIx)	]
新	新	k?	新
<g/>
]	]	kIx)	]
šinbun	šinbun	k1gInSc1	šinbun
'	'	kIx"	'
<g/>
noviny	novina	k1gFnPc1	novina
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jako	jako	k9	jako
[	[	kIx(	[
<g/>
ŋ	ŋ	k?	ŋ
<g/>
]	]	kIx)	]
před	před	k7c7	před
/	/	kIx~	/
<g/>
k	k	k7c3	k
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
/	/	kIx~	/
<g/>
kaNkei	kaNkei	k6eAd1	kaNkei
<g/>
/	/	kIx~	/
→	→	k?	→
[	[	kIx(	[
<g/>
kaŋ	kaŋ	k?	kaŋ
<g/>
]	]	kIx)	]
関	関	k?	関
kankei	kankei	k1gNnSc1	kankei
'	'	kIx"	'
<g/>
souvislost	souvislost	k1gFnSc1	souvislost
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
/	/	kIx~	/
<g/>
d	d	k?	d
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
/	/	kIx~	/
je	být	k5eAaImIp3nS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
jako	jako	k8xS	jako
[	[	kIx(	[
<g/>
n	n	k0	n
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
/	/	kIx~	/
<g/>
hoNdana	hoNdana	k1gFnSc1	hoNdana
<g/>
/	/	kIx~	/
→	→	k?	→
[	[	kIx(	[
<g/>
hondana	hondana	k1gFnSc1	hondana
<g/>
]	]	kIx)	]
本	本	k?	本
hondana	hondana	k1gFnSc1	hondana
'	'	kIx"	'
<g/>
police	police	k1gFnSc1	police
na	na	k7c4	na
knihy	kniha	k1gFnPc4	kniha
<g/>
/	/	kIx~	/
<g/>
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
<g/>
/	/	kIx~	/
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
individuálně	individuálně	k6eAd1	individuálně
relizováno	relizován	k2eAgNnSc1d1	relizován
jako	jako	k8xC	jako
[	[	kIx(	[
<g/>
d	d	k?	d
<g/>
͡	͡	k?	͡
<g/>
z	z	k7c2	z
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
/	/	kIx~	/
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
/	/	kIx~	/
<g/>
i	i	k9	i
<g/>
/	/	kIx~	/
realizováno	realizován	k2eAgNnSc1d1	realizováno
jako	jako	k8xS	jako
alveolopalatální	alveolopalatální	k2eAgFnSc1d1	alveolopalatální
frikativa	frikativa	k1gFnSc1	frikativa
[	[	kIx(	[
<g/>
ɕ	ɕ	k?	ɕ
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
/	/	kIx~	/
<g/>
r	r	kA	r
<g/>
/	/	kIx~	/
je	být	k5eAaImIp3nS	být
apikální	apikální	k2eAgFnSc7d1	apikální
post-alveolární	postlveolární	k2eAgFnSc7d1	post-alveolární
<g />
.	.	kIx.	.
</s>
<s>
švih	švih	k1gInSc1	švih
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
reprezentován	reprezentován	k2eAgMnSc1d1	reprezentován
i	i	k9	i
laterální	laterální	k2eAgFnSc7d1	laterální
variantou	varianta	k1gFnSc7	varianta
<g/>
.	.	kIx.	.
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
/	/	kIx~	/
je	být	k5eAaImIp3nS	být
realizováno	realizován	k2eAgNnSc1d1	realizováno
jako	jako	k8xS	jako
[	[	kIx(	[
<g/>
ç	ç	k?	ç
<g/>
]	]	kIx)	]
před	před	k7c7	před
/	/	kIx~	/
<g/>
i	i	k9	i
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
j	j	k?	j
<g/>
/	/	kIx~	/
a	a	k8xC	a
jako	jako	k9	jako
[	[	kIx(	[
<g/>
ɸ	ɸ	k?	ɸ
<g/>
]	]	kIx)	]
před	před	k7c7	před
/	/	kIx~	/
<g/>
u	u	k7c2	u
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
je	být	k5eAaImIp3nS	být
i	i	k9	i
délka	délka	k1gFnSc1	délka
konsonantu	konsonant	k1gInSc2	konsonant
distinktivní	distinktivní	k2eAgInSc1d1	distinktivní
<g/>
:	:	kIx,	:
来	来	k?	来
kita	kita	k1gMnSc1	kita
'	'	kIx"	'
<g/>
přišel	přijít	k5eAaPmAgInS	přijít
<g/>
'	'	kIx"	'
vs	vs	k?	vs
<g/>
.	.	kIx.	.
切	切	k?	切
kitta	kitta	k1gMnSc1	kitta
'	'	kIx"	'
<g/>
uříznul	uříznout	k5eAaPmAgInS	uříznout
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Fonologicky	fonologicky	k6eAd1	fonologicky
je	být	k5eAaImIp3nS	být
geminace	geminace	k1gFnSc1	geminace
reprezentována	reprezentovat	k5eAaImNgFnS	reprezentovat
pomocí	pomoc	k1gFnSc7	pomoc
/	/	kIx~	/
<g/>
Q	Q	kA	Q
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
moraického	moraický	k2eAgInSc2d1	moraický
orálního	orální	k2eAgInSc2d1	orální
obstruentu	obstruent	k1gInSc2	obstruent
s	s	k7c7	s
nespecifikovaným	specifikovaný	k2eNgNnSc7d1	nespecifikované
místem	místo	k1gNnSc7	místo
artikulace	artikulace	k1gFnSc2	artikulace
<g/>
.	.	kIx.	.
</s>
<s>
Foneticky	foneticky	k6eAd1	foneticky
je	být	k5eAaImIp3nS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
jako	jako	k8xS	jako
prodloužení	prodloužení	k1gNnSc1	prodloužení
konsonantické	konsonantický	k2eAgFnSc2d1	konsonantický
artikulace	artikulace	k1gFnSc2	artikulace
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
vlastní	vlastní	k2eAgFnSc4d1	vlastní
artikulaci	artikulace	k1gFnSc4	artikulace
udává	udávat	k5eAaImIp3nS	udávat
následující	následující	k2eAgInSc1d1	následující
konsonant	konsonant	k1gInSc1	konsonant
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
Q	Q	kA	Q
<g/>
/	/	kIx~	/
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
trvání	trvání	k1gNnSc4	trvání
následujícího	následující	k2eAgInSc2d1	následující
konsonantu	konsonant	k1gInSc2	konsonant
(	(	kIx(	(
<g/>
frikativy	frikativa	k1gFnPc4	frikativa
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
delší	dlouhý	k2eAgNnSc4d2	delší
trvání	trvání	k1gNnSc4	trvání
<g/>
,	,	kIx,	,
plozivy	ploziv	k1gInPc4	ploziv
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
delší	dlouhý	k2eAgInSc4d2	delší
závěr	závěr	k1gInSc4	závěr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
je	být	k5eAaImIp3nS	být
zapisován	zapisovat	k5eAaImNgInS	zapisovat
pomocí	pomoc	k1gFnSc7	pomoc
malého	malý	k2eAgMnSc4d1	malý
っ	っ	k?	っ
Geminují	geminovat	k5eAaImIp3nP	geminovat
zejména	zejména	k9	zejména
konsonanty	konsonant	k1gInPc1	konsonant
/	/	kIx~	/
<g/>
p	p	k?	p
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
japonských	japonský	k2eAgNnPc2d1	Japonské
a	a	k8xC	a
sino-japonských	sinoaponský	k2eAgNnPc2d1	sino-japonský
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Znělé	znělý	k2eAgFnPc1d1	znělá
gemináty	gemináta	k1gFnPc1	gemináta
/	/	kIx~	/
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
z	z	k7c2	z
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
/	/	kIx~	/
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
moderních	moderní	k2eAgFnPc2d1	moderní
výpůjček	výpůjčka	k1gFnPc2	výpůjčka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
河	河	k?	河
[	[	kIx(	[
<g/>
カ	カ	k?	カ
<g/>
]	]	kIx)	]
kappa	kappa	k1gNnSc1	kappa
/	/	kIx~	/
<g/>
kaQpa	kaQpa	k1gFnSc1	kaQpa
<g/>
/	/	kIx~	/
→	→	k?	→
[	[	kIx(	[
<g/>
kap	kap	k1gInSc1	kap
<g/>
̚	̚	k?	̚
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
pa	pa	k0	pa
<g/>
]	]	kIx)	]
'	'	kIx"	'
<g/>
vodník	vodník	k1gMnSc1	vodník
<g/>
'	'	kIx"	'
バ	バ	k?	バ
baggu	bagg	k1gInSc2	bagg
/	/	kIx~	/
<g/>
baQgu	baQg	k1gInSc2	baQg
<g/>
/	/	kIx~	/
→	→	k?	→
[	[	kIx(	[
<g/>
bag	bag	k?	bag
<g/>
̚	̚	k?	̚
<g/>
.	.	kIx.	.
<g/>
gɯ	gɯ	k?	gɯ
<g/>
]	]	kIx)	]
'	'	kIx"	'
<g/>
taška	taška	k1gFnSc1	taška
<g/>
'	'	kIx"	'
喫	喫	k?	喫
kissaten	kissatno	k1gNnPc2	kissatno
/	/	kIx~	/
<g/>
kiQsateN	kiQsateN	k?	kiQsateN
<g/>
/	/	kIx~	/
→	→	k?	→
[	[	kIx(	[
<g/>
kisː	kisː	k?	kisː
<g/>
]	]	kIx)	]
'	'	kIx"	'
<g/>
čajovna	čajovna	k1gFnSc1	čajovna
<g />
.	.	kIx.	.
</s>
<s>
<g/>
'	'	kIx"	'
察	察	k?	察
sačči	sačč	k1gFnSc6	sačč
/	/	kIx~	/
<g/>
saQti	saQť	k1gFnSc6	saQť
<g/>
/	/	kIx~	/
→	→	k?	→
[	[	kIx(	[
<g/>
sat	sat	k?	sat
<g/>
̚	̚	k?	̚
<g/>
.	.	kIx.	.
<g/>
t	t	k?	t
<g/>
͡	͡	k?	͡
<g/>
ɕ	ɕ	k?	ɕ
<g/>
]	]	kIx)	]
'	'	kIx"	'
<g/>
závěr	závěr	k1gInSc1	závěr
<g/>
,	,	kIx,	,
úsudek	úsudek	k1gInSc1	úsudek
<g/>
'	'	kIx"	'
Japonština	japonština	k1gFnSc1	japonština
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c7	mezi
jazyky	jazyk	k1gInPc7	jazyk
s	s	k7c7	s
tonálním	tonální	k2eAgInSc7d1	tonální
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navzájem	navzájem	k6eAd1	navzájem
kontrastují	kontrastovat	k5eAaImIp3nP	kontrastovat
dva	dva	k4xCgInPc1	dva
tóny	tón	k1gInPc1	tón
–	–	k?	–
vysoký	vysoký	k2eAgInSc1d1	vysoký
a	a	k8xC	a
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgInSc1d1	standardní
tokijský	tokijský	k2eAgInSc1d1	tokijský
dialekt	dialekt	k1gInSc1	dialekt
je	být	k5eAaImIp3nS	být
popisován	popisovat	k5eAaImNgInS	popisovat
jako	jako	k9	jako
downstep	downstep	k1gInSc1	downstep
(	(	kIx(	(
<g/>
tonální	tonální	k2eAgInSc1d1	tonální
pokles	pokles	k1gInSc1	pokles
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tonálnímu	tonální	k2eAgInSc3d1	tonální
poklesu	pokles	k1gInSc3	pokles
dochází	docházet	k5eAaImIp3nS	docházet
po	po	k7c6	po
moře	mora	k1gFnSc6	mora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
lexikálně	lexikálně	k6eAd1	lexikálně
specifikována	specifikován	k2eAgFnSc1d1	specifikována
jako	jako	k8xS	jako
přízvučná	přízvučný	k2eAgFnSc1d1	přízvučná
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
definována	definovat	k5eAaBmNgFnS	definovat
vysokým	vysoký	k2eAgInSc7d1	vysoký
tónem	tón	k1gInSc7	tón
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátcích	počátek	k1gInPc6	počátek
slov	slovo	k1gNnPc2	slovo
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
tónu	tón	k1gInSc2	tón
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
pokud	pokud	k8xS	pokud
nenesou	nést	k5eNaImIp3nP	nést
přízvuk	přízvuk	k1gInSc4	přízvuk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
bez	bez	k7c2	bez
přízvuku	přízvuk	k1gInSc2	přízvuk
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
tonálnímu	tonální	k2eAgInSc3d1	tonální
poklesu	pokles	k1gInSc3	pokles
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
japonštiny	japonština	k1gFnSc2	japonština
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
směs	směs	k1gFnSc1	směs
tří	tři	k4xCgInPc2	tři
znakových	znakový	k2eAgMnPc2d1	znakový
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
kandži	kandzat	k5eAaPmIp1nS	kandzat
(	(	kIx(	(
<g/>
漢	漢	k?	漢
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hiragana	hiragana	k1gFnSc1	hiragana
(	(	kIx(	(
<g/>
ひ	ひ	k?	ひ
<g/>
)	)	kIx)	)
a	a	k8xC	a
katakana	katakana	k1gFnSc1	katakana
(	(	kIx(	(
<g/>
カ	カ	k?	カ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hiragana	Hiragana	k1gFnSc1	Hiragana
a	a	k8xC	a
katakana	katakana	k1gFnSc1	katakana
se	se	k3xPyFc4	se
též	též	k9	též
označují	označovat	k5eAaImIp3nP	označovat
společným	společný	k2eAgInSc7d1	společný
názvem	název	k1gInSc7	název
kana	kanout	k5eAaImSgInS	kanout
<g/>
.	.	kIx.	.
</s>
<s>
Kandži	Kandž	k1gFnSc3	Kandž
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
kořenu	kořen	k1gInSc2	kořen
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
koncovky	koncovka	k1gFnSc2	koncovka
a	a	k8xC	a
partikule	partikule	k1gFnPc1	partikule
jsou	být	k5eAaImIp3nP	být
doplněny	doplnit	k5eAaPmNgFnP	doplnit
hiraganou	hiragana	k1gFnSc7	hiragana
<g/>
.	.	kIx.	.
</s>
<s>
Katakana	Katakana	k1gFnSc1	Katakana
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
fonetický	fonetický	k2eAgInSc4d1	fonetický
přepis	přepis	k1gInSc4	přepis
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
slov	slovo	k1gNnPc2	slovo
převzatých	převzatý	k2eAgMnPc2d1	převzatý
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
nějakou	nějaký	k3yIgFnSc4	nějaký
část	část	k1gFnSc4	část
textu	text	k1gInSc2	text
zvýraznit	zvýraznit	k5eAaPmF	zvýraznit
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
kurzíva	kurzíva	k1gFnSc1	kurzíva
<g/>
.	.	kIx.	.
</s>
<s>
Hiragana	Hiragana	k1gFnSc1	Hiragana
a	a	k8xC	a
katakana	katakana	k1gFnSc1	katakana
jsou	být	k5eAaImIp3nP	být
slabičné	slabičný	k2eAgFnPc4d1	slabičná
abecedy	abeceda	k1gFnPc4	abeceda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kandži	kandzat	k5eAaPmIp1nS	kandzat
je	on	k3xPp3gNnSc4	on
morfemografické	morfemografický	k2eAgNnSc4d1	morfemografický
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
znaků	znak	k1gInPc2	znak
kandži	kandž	k1gFnSc3	kandž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
číst	číst	k5eAaImF	číst
dvěma	dva	k4xCgFnPc7	dva
i	i	k9	i
více	hodně	k6eAd2	hodně
způsoby	způsob	k1gInPc4	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
specifikovat	specifikovat	k5eAaBmF	specifikovat
výslovnost	výslovnost	k1gFnSc4	výslovnost
nějakého	nějaký	k3yIgInSc2	nějaký
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
použije	použít	k5eAaPmIp3nS	použít
se	se	k3xPyFc4	se
hiragana	hiragan	k1gMnSc2	hiragan
<g/>
.	.	kIx.	.
</s>
<s>
Kandži	Kandzat	k5eAaPmIp1nS	Kandzat
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rozlišit	rozlišit	k5eAaPmF	rozlišit
význam	význam	k1gInSc4	význam
u	u	k7c2	u
homonym	homonymum	k1gNnPc2	homonymum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
velmi	velmi	k6eAd1	velmi
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
mezer	mezera	k1gFnPc2	mezera
mezi	mezi	k7c7	mezi
slovy	slovo	k1gNnPc7	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
jsou	být	k5eAaImIp3nP	být
znaky	znak	k1gInPc1	znak
uspořádány	uspořádán	k2eAgInPc1d1	uspořádán
do	do	k7c2	do
sloupců	sloupec	k1gInPc2	sloupec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
sloupci	sloupec	k1gInSc6	sloupec
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
shora	shora	k6eAd1	shora
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
sloupce	sloupec	k1gInPc1	sloupec
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgInP	řadit
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
.	.	kIx.	.
</s>
<s>
Japonské	japonský	k2eAgFnPc1d1	japonská
knihy	kniha	k1gFnPc1	kniha
tedy	tedy	k9	tedy
mají	mít	k5eAaImIp3nP	mít
stránky	stránka	k1gFnPc1	stránka
řazené	řazený	k2eAgFnPc1d1	řazená
z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
pohledu	pohled	k1gInSc2	pohled
"	"	kIx"	"
<g/>
odzadu	odzadu	k6eAd1	odzadu
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Alternativně	alternativně	k6eAd1	alternativně
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgInPc4d1	možný
znaky	znak	k1gInPc4	znak
psát	psát	k5eAaImF	psát
do	do	k7c2	do
řádku	řádek	k1gInSc2	řádek
zleva	zleva	k6eAd1	zleva
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
řádky	řádka	k1gFnPc1	řádka
jsou	být	k5eAaImIp3nP	být
řazené	řazený	k2eAgFnPc1d1	řazená
shora	shora	k6eAd1	shora
dolů	dolů	k6eAd1	dolů
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Diakritickým	diakritický	k2eAgNnSc7d1	diakritické
znaménkem	znaménko	k1gNnSc7	znaménko
dakuten	dakuten	k2eAgMnSc1d1	dakuten
(	(	kIx(	(
<g/>
濁	濁	k?	濁
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
dvě	dva	k4xCgFnPc1	dva
krátké	krátký	k2eAgFnPc1d1	krátká
čárky	čárka	k1gFnPc1	čárka
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
znaku	znak	k1gInSc2	znak
<g/>
;	;	kIx,	;
jiný	jiný	k2eAgInSc1d1	jiný
název	název	k1gInSc1	název
<g/>
:	:	kIx,	:
nigori	nigori	k1gNnSc1	nigori
(	(	kIx(	(
<g/>
濁	濁	k?	濁
<g/>
))	))	k?	))
se	se	k3xPyFc4	se
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
značí	značit	k5eAaImIp3nP	značit
slabiky	slabika	k1gFnPc1	slabika
se	s	k7c7	s
znělou	znělý	k2eAgFnSc7d1	znělá
párovou	párový	k2eAgFnSc7d1	párová
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Souhláska	souhláska	k1gFnSc1	souhláska
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
/	/	kIx~	/
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
/	/	kIx~	/
<g/>
b	b	k?	b
<g/>
/	/	kIx~	/
<g/>
;	;	kIx,	;
/	/	kIx~	/
<g/>
k	k	k7c3	k
<g/>
/	/	kIx~	/
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
/	/	kIx~	/
<g/>
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
;	;	kIx,	;
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
/	/	kIx~	/
na	na	k7c6	na
/	/	kIx~	/
<g/>
z	z	k7c2	z
<g/>
/	/	kIx~	/
<g/>
;	;	kIx,	;
/	/	kIx~	/
<g/>
š	š	k?	š
<g/>
/	/	kIx~	/
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
dž	dž	k?	dž
<g/>
/	/	kIx~	/
<g/>
;	;	kIx,	;
/	/	kIx~	/
<g/>
t	t	k?	t
<g/>
/	/	kIx~	/
na	na	k7c4	na
/	/	kIx~	/
<g/>
d	d	k?	d
<g/>
/	/	kIx~	/
<g/>
;	;	kIx,	;
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
/	/	kIx~	/
na	na	k7c6	na
/	/	kIx~	/
<g/>
(	(	kIx(	(
<g/>
d	d	k?	d
<g/>
)	)	kIx)	)
<g/>
z	z	k7c2	z
<g/>
/	/	kIx~	/
<g/>
;	;	kIx,	;
/	/	kIx~	/
<g/>
č	č	k0	č
<g/>
/	/	kIx~	/
na	na	k7c6	na
/	/	kIx~	/
<g/>
dž	dž	k?	dž
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Znaménkem	znaménko	k1gNnSc7	znaménko
handakuten	handakuten	k2eAgMnSc1d1	handakuten
(	(	kIx(	(
<g/>
半	半	k?	半
<g/>
;	;	kIx,	;
kroužek	kroužek	k1gInSc1	kroužek
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
znaku	znak	k1gInSc2	znak
<g/>
;	;	kIx,	;
jiný	jiný	k2eAgInSc1d1	jiný
název	název	k1gInSc1	název
<g/>
:	:	kIx,	:
marunigori	marunigori	k1gNnSc1	marunigori
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
změna	změna	k1gFnSc1	změna
výlovnosti	výlovnost	k1gFnSc2	výlovnost
z	z	k7c2	z
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
/	/	kIx~	/
na	na	k7c4	na
/	/	kIx~	/
<g/>
p	p	k?	p
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
z	z	k7c2	z
/	/	kIx~	/
<g/>
f	f	k?	f
<g/>
/	/	kIx~	/
na	na	k7c4	na
/	/	kIx~	/
<g/>
p	p	k?	p
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
samohlásky	samohláska	k1gFnSc2	samohláska
/	/	kIx~	/
<g/>
u	u	k7c2	u
<g/>
/	/	kIx~	/
se	s	k7c7	s
znaménkem	znaménko	k1gNnSc7	znaménko
nigori	nigori	k6eAd1	nigori
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
slabika	slabika	k1gFnSc1	slabika
vu	vu	k?	vu
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
přepis	přepis	k1gInSc4	přepis
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
katakanou	katakaný	k2eAgFnSc7d1	katakaný
<g/>
:	:	kIx,	:
např.	např.	kA	např.
ヴ	ヴ	k?	ヴ
vácurafu	vácuraf	k1gInSc2	vácuraf
hiroba	hiroba	k1gFnSc1	hiroba
'	'	kIx"	'
<g/>
Václavské	václavský	k2eAgNnSc1d1	Václavské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
písmena	písmeno	k1gNnPc1	písmeno
mají	mít	k5eAaImIp3nP	mít
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
napíší	napsat	k5eAaPmIp3nP	napsat
malá	malý	k2eAgNnPc1d1	malé
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
normální	normální	k2eAgFnSc6d1	normální
velikosti	velikost	k1gFnSc6	velikost
<g/>
,	,	kIx,	,
v	v	k7c6	v
dolním	dolní	k2eAgInSc6d1	dolní
indexu	index	k1gInSc6	index
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slabika	slabika	k1gFnSc1	slabika
se	s	k7c7	s
samohláskou	samohláska	k1gFnSc7	samohláska
/	/	kIx~	/
<g/>
i	i	k9	i
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
následovaná	následovaný	k2eAgNnPc4d1	následované
malým	malý	k2eAgInSc7d1	malý
znakem	znak	k1gInSc7	znak
pro	pro	k7c4	pro
slabiky	slabika	k1gFnPc4	slabika
ja	ja	k?	ja
<g/>
,	,	kIx,	,
ju	ju	k0	ju
nebo	nebo	k8xC	nebo
jo	jo	k0	jo
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
složené	složený	k2eAgFnSc2d1	složená
slabiky	slabika	k1gFnSc2	slabika
se	s	k7c7	s
změkčenou	změkčený	k2eAgFnSc7d1	změkčená
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
:	:	kIx,	:
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
složenou	složený	k2eAgFnSc7d1	složená
slabikou	slabika	k1gFnSc7	slabika
(	(	kIx(	(
<g/>
malé	malý	k2eAgFnPc4d1	malá
ja	ja	k?	ja
/	/	kIx~	/
ju	ju	k0	ju
/	/	kIx~	/
jo	jo	k9	jo
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
slabikami	slabika	k1gFnPc7	slabika
(	(	kIx(	(
<g/>
normální	normální	k2eAgMnSc1d1	normální
ja	ja	k?	ja
/	/	kIx~	/
ju	ju	k0	ju
/	/	kIx~	/
jo	jo	k9	jo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
srov.	srov.	kA	srov.
např.	např.	kA	např.
び	び	k?	び
(	(	kIx(	(
<g/>
病	病	k?	病
<g/>
)	)	kIx)	)
bjóin	bjóin	k1gMnSc1	bjóin
"	"	kIx"	"
<g/>
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
"	"	kIx"	"
a	a	k8xC	a
び	び	k?	び
(	(	kIx(	(
<g/>
美	美	k?	美
<g/>
)	)	kIx)	)
bijóin	bijóin	k1gInSc1	bijóin
"	"	kIx"	"
<g/>
kadeřnictví	kadeřnictví	k1gNnSc1	kadeřnictví
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
lze	lze	k6eAd1	lze
pomocí	pomocí	k7c2	pomocí
malých	malý	k2eAgNnPc2d1	malé
písmen	písmeno	k1gNnPc2	písmeno
pro	pro	k7c4	pro
samohlásky	samohláska	k1gFnPc4	samohláska
zapsat	zapsat	k5eAaPmF	zapsat
slabiky	slabika	k1gFnPc4	slabika
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
sadě	sada	k1gFnSc6	sada
nejsou	být	k5eNaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
možnost	možnost	k1gFnSc1	možnost
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
přepisu	přepis	k1gInSc6	přepis
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
katakanou	katakaný	k2eAgFnSc7d1	katakaný
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgFnPc1d1	Dlouhé
samohlásky	samohláska	k1gFnPc1	samohláska
se	se	k3xPyFc4	se
v	v	k7c6	v
hiraganě	hiragana	k1gFnSc6	hiragana
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
zdvojením	zdvojení	k1gNnSc7	zdvojení
krátké	krátká	k1gFnSc2	krátká
samohlásky	samohláska	k1gFnSc2	samohláska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
あ	あ	k?	あ
=	=	kIx~	=
á	á	k0	á
<g/>
,	,	kIx,	,
か	か	k?	か
=	=	kIx~	=
ká	ká	k0	ká
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgNnSc1d1	Dlouhé
ó	ó	k0	ó
se	se	k3xPyFc4	se
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
japonského	japonský	k2eAgInSc2d1	japonský
původu	původ	k1gInSc2	původ
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
jako	jako	k8xC	jako
zdvojené	zdvojený	k2eAgNnSc4d1	zdvojené
oo	oo	k?	oo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
お	お	k?	お
ókina	ókina	k1gFnSc1	ókina
(	(	kIx(	(
<g/>
ookina	ookina	k1gFnSc1	ookina
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
velký	velký	k2eAgInSc1d1	velký
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
častěji	často	k6eAd2	často
(	(	kIx(	(
<g/>
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
sinojaponského	sinojaponský	k2eAgInSc2d1	sinojaponský
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
ou	ou	k0	ou
<g/>
,	,	kIx,	,
např.	např.	kA	např.
と	と	k?	と
Tókjó	Tókjó	k1gNnSc1	Tókjó
(	(	kIx(	(
<g/>
Toukijou	Toukijou	k1gFnSc1	Toukijou
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
však	však	k9	však
u	u	k7c2	u
po	po	k7c6	po
/	/	kIx~	/
<g/>
o	o	k0	o
<g/>
/	/	kIx~	/
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
tvary	tvar	k1gInPc4	tvar
sloves	sloveso	k1gNnPc2	sloveso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
問	問	k?	問
[	[	kIx(	[
<g/>
tou	ten	k3xDgFnSc7	ten
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
追	追	k?	追
[	[	kIx(	[
<g/>
ou	ou	k0	ou
<g/>
]	]	kIx)	]
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katakaně	katakaně	k6eAd1	katakaně
se	se	k3xPyFc4	se
k	k	k7c3	k
vyznačení	vyznačení	k1gNnSc3	vyznačení
délky	délka	k1gFnSc2	délka
používá	používat	k5eAaImIp3nS	používat
znak	znak	k1gInSc1	znak
ー	ー	k?	ー
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ビ	ビ	k?	ビ
bíru	bíra	k1gFnSc4	bíra
"	"	kIx"	"
<g/>
pivo	pivo	k1gNnSc4	pivo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zmenšený	zmenšený	k2eAgInSc1d1	zmenšený
znak	znak	k1gInSc1	znak
slabiky	slabika	k1gFnSc2	slabika
cu	cu	k?	cu
zdvojuje	zdvojovat	k5eAaImIp3nS	zdvojovat
následující	následující	k2eAgFnSc4d1	následující
souhlásku	souhláska	k1gFnSc4	souhláska
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
促	促	k?	促
-	-	kIx~	-
sokuon	sokuon	k1gMnSc1	sokuon
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
též	též	k9	též
cumaruon	cumaruon	k1gInSc1	cumaruon
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
např.	např.	kA	např.
が	が	k?	が
(	(	kIx(	(
<g/>
学	学	k?	学
<g/>
)	)	kIx)	)
gakkó	gakkó	k?	gakkó
"	"	kIx"	"
<g/>
škola	škola	k1gFnSc1	škola
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
パ	パ	k?	パ
panfuretto	panfuretto	k1gNnSc1	panfuretto
"	"	kIx"	"
<g/>
brožura	brožura	k1gFnSc1	brožura
<g/>
,	,	kIx,	,
pamflet	pamflet	k1gInSc1	pamflet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slabiky	slabika	k1gFnSc2	slabika
končící	končící	k2eAgFnSc2d1	končící
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
/	/	kIx~	/
<g/>
u	u	k7c2	u
<g/>
/	/	kIx~	/
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
při	při	k7c6	při
přepisu	přepis	k1gInSc6	přepis
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
katakanou	katakaný	k2eAgFnSc7d1	katakaný
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
v	v	k7c6	v
originále	originál	k1gInSc6	originál
byly	být	k5eAaImAgInP	být
dvě	dva	k4xCgFnPc4	dva
souhlásky	souhláska	k1gFnPc4	souhláska
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
transkripci	transkripce	k1gFnSc6	transkripce
se	se	k3xPyFc4	se
k	k	k7c3	k
zápisu	zápis	k1gInSc2	zápis
první	první	k4xOgFnSc2	první
souhlásky	souhláska	k1gFnSc2	souhláska
použije	použít	k5eAaPmIp3nS	použít
nejčastěji	často	k6eAd3	často
právě	právě	k9	právě
slabika	slabika	k1gFnSc1	slabika
se	s	k7c7	s
samohláskou	samohláska	k1gFnSc7	samohláska
u	u	k7c2	u
<g/>
:	:	kIx,	:
プ	プ	k?	プ
Puraha	Puraha	k1gFnSc1	Puraha
"	"	kIx"	"
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ト	ト	k?	ト
tósuto	tósut	k2eAgNnSc1d1	tósut
"	"	kIx"	"
<g/>
toast	toast	k1gInSc1	toast
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ゴ	ゴ	k?	ゴ
gorufu	goruf	k1gInSc6	goruf
"	"	kIx"	"
<g/>
golf	golf	k1gInSc4	golf
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
dochází	docházet	k5eAaImIp3nS	docházet
často	často	k6eAd1	často
k	k	k7c3	k
desonorizaci	desonorizace	k1gFnSc3	desonorizace
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
vnímají	vnímat	k5eAaImIp3nP	vnímat
souhlásky	souhláska	k1gFnPc4	souhláska
r	r	kA	r
a	a	k8xC	a
l	l	kA	l
jako	jako	k8xS	jako
téměř	téměř	k6eAd1	téměř
totožné	totožný	k2eAgFnPc1d1	totožná
<g/>
.	.	kIx.	.
</s>
<s>
Slabika	slabika	k1gFnSc1	slabika
は	は	k?	は
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
tematické	tematický	k2eAgFnSc2d1	tematická
částice	částice	k1gFnSc2	částice
se	se	k3xPyFc4	se
nečte	číst	k5eNaImIp3nS	číst
*	*	kIx~	*
<g/>
[	[	kIx(	[
<g/>
ha	ha	kA	ha
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
[	[	kIx(	[
<g/>
wa	wa	k?	wa
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Slabika	slabika	k1gFnSc1	slabika
を	を	k?	を
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
akuzativní	akuzativní	k2eAgFnSc2d1	akuzativní
částice	částice	k1gFnSc2	částice
se	se	k3xPyFc4	se
nečte	číst	k5eNaImIp3nS	číst
*	*	kIx~	*
<g/>
[	[	kIx(	[
<g/>
wo	wo	k?	wo
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
[	[	kIx(	[
<g/>
o	o	k0	o
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Slabika	slabika	k1gFnSc1	slabika
へ	へ	k?	へ
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
směrové	směrový	k2eAgFnSc2d1	směrová
částice	částice	k1gFnSc2	částice
se	se	k3xPyFc4	se
nečte	číst	k5eNaImIp3nS	číst
*	*	kIx~	*
<g/>
[	[	kIx(	[
<g/>
he	he	k0	he
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
[	[	kIx(	[
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Slabiky	slabika	k1gFnPc1	slabika
<g/>
,	,	kIx,	,
zakončené	zakončený	k2eAgNnSc1d1	zakončené
/	/	kIx~	/
<g/>
e	e	k0	e
<g/>
/	/	kIx~	/
a	a	k8xC	a
následované	následovaný	k2eAgNnSc1d1	následované
い	い	k?	い
(	(	kIx(	(
<g/>
/	/	kIx~	/
<g/>
i	i	k9	i
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nečtou	číst	k5eNaImIp3nP	číst
*	*	kIx~	*
<g/>
[	[	kIx(	[
<g/>
-ei	-ei	k?	-ei
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
[	[	kIx(	[
<g/>
é	é	k0	é
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
へ	へ	k?	へ
[	[	kIx(	[
<g/>
hé	hé	k0	hé
<g/>
]	]	kIx)	]
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
平	平	k?	平
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc1	tento
však	však	k9	však
neplatí	platit	k5eNaImIp3nS	platit
pro	pro	k7c4	pro
oddělené	oddělený	k2eAgInPc4d1	oddělený
významy	význam	k1gInPc4	význam
<g/>
/	/	kIx~	/
<g/>
slabiky	slabika	k1gFnPc4	slabika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
会	会	k?	会
[	[	kIx(	[
<g/>
ein	ein	k?	ein
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
毛	毛	k?	毛
[	[	kIx(	[
<g/>
keiro	keiro	k6eAd1	keiro
<g/>
]	]	kIx)	]
a	a	k8xC	a
podobných	podobný	k2eAgInPc6d1	podobný
vzácných	vzácný	k2eAgInPc6d1	vzácný
případech	případ	k1gInPc6	případ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc4	znak
kandži	kandzat	k5eAaPmIp1nS	kandzat
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
Japonci	Japonec	k1gMnPc1	Japonec
převzali	převzít	k5eAaPmAgMnP	převzít
z	z	k7c2	z
čínštiny	čínština	k1gFnSc2	čínština
<g/>
,	,	kIx,	,
nezachycují	zachycovat	k5eNaImIp3nP	zachycovat
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pojmy	pojem	k1gInPc1	pojem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Číňan	Číňan	k1gMnSc1	Číňan
i	i	k9	i
Japonec	Japonec	k1gMnSc1	Japonec
přečetli	přečíst	k5eAaPmAgMnP	přečíst
tentýž	týž	k3xTgInSc4	týž
text	text	k1gInSc4	text
každý	každý	k3xTgInSc4	každý
svým	svůj	k3xOyFgInSc7	svůj
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
porozuměli	porozumět	k5eAaPmAgMnP	porozumět
mu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Úplnému	úplný	k2eAgNnSc3d1	úplné
porozumění	porozumění	k1gNnSc3	porozumění
ovšem	ovšem	k9	ovšem
brání	bránit	k5eAaImIp3nS	bránit
několik	několik	k4yIc4	několik
faktorů	faktor	k1gInPc2	faktor
<g/>
:	:	kIx,	:
Gramatická	gramatický	k2eAgFnSc1d1	gramatická
struktura	struktura	k1gFnSc1	struktura
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
slovosled	slovosled	k1gInSc1	slovosled
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Čínština	čínština	k1gFnSc1	čínština
a	a	k8xC	a
japonština	japonština	k1gFnSc1	japonština
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
odlišné	odlišný	k2eAgInPc1d1	odlišný
jazyky	jazyk	k1gInPc1	jazyk
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
jazykových	jazykový	k2eAgFnPc2d1	jazyková
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Japonské	japonský	k2eAgInPc1d1	japonský
texty	text	k1gInPc1	text
nikdy	nikdy	k6eAd1	nikdy
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
pouze	pouze	k6eAd1	pouze
kandži	kandž	k1gFnSc3	kandž
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
směs	směs	k1gFnSc4	směs
kandži	kandzat	k5eAaPmIp1nS	kandzat
a	a	k8xC	a
kany	kany	k?	kany
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
slabičných	slabičný	k2eAgFnPc2d1	slabičná
abeced	abeceda	k1gFnPc2	abeceda
hiragany	hiragana	k1gFnSc2	hiragana
a	a	k8xC	a
katakany	katakana	k1gFnSc2	katakana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
čínština	čínština	k1gFnSc1	čínština
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
drtivou	drtivý	k2eAgFnSc4d1	drtivá
většinu	většina	k1gFnSc4	většina
znaků	znak	k1gInPc2	znak
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
má	mít	k5eAaImIp3nS	mít
nebo	nebo	k8xC	nebo
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
repertoáru	repertoár	k1gInSc6	repertoár
i	i	k9	i
čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
lišit	lišit	k5eAaImF	lišit
jejich	jejich	k3xOp3gNnSc4	jejich
preferované	preferovaný	k2eAgNnSc4d1	preferované
užití	užití	k1gNnSc4	užití
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
znak	znak	k1gInSc1	znak
pro	pro	k7c4	pro
řeku	řeka	k1gFnSc4	řeka
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
čínských	čínský	k2eAgInPc2d1	čínský
názvů	název	k1gInPc2	název
řek	řeka	k1gFnPc2	řeka
je	být	k5eAaImIp3nS	být
江	江	k?	江
(	(	kIx(	(
<g/>
jiā	jiā	k?	jiā
<g/>
,	,	kIx,	,
ťiang	ťiang	k1gMnSc1	ťiang
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
河	河	k?	河
(	(	kIx(	(
<g/>
hé	hé	k0	hé
<g/>
,	,	kIx,	,
che	che	k0	che
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
川	川	k?	川
(	(	kIx(	(
<g/>
kawa	kawa	k1gMnSc1	kawa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
znak	znak	k1gInSc1	znak
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
v	v	k7c6	v
čínštině	čínština	k1gFnSc6	čínština
<g/>
,	,	kIx,	,
čte	číst	k5eAaImIp3nS	číst
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
chuan	chuan	k1gMnSc1	chuan
<g/>
,	,	kIx,	,
čchuan	čchuan	k1gMnSc1	čchuan
<g/>
)	)	kIx)	)
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
názvech	název	k1gInPc6	název
řek	řeka	k1gFnPc2	řeka
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
často	často	k6eAd1	často
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
ho	on	k3xPp3gInSc4	on
např.	např.	kA	např.
v	v	k7c6	v
názvu	název	k1gInSc6	název
provincie	provincie	k1gFnSc2	provincie
S	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
-čchuan	-čchuana	k1gFnPc2	-čchuana
(	(	kIx(	(
<g/>
四	四	k?	四
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Čtyřříčí	Čtyřříčí	k2eAgFnPc4d1	Čtyřříčí
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
čínštiny	čínština	k1gFnSc2	čínština
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
každý	každý	k3xTgInSc4	každý
znak	znak	k1gInSc4	znak
několik	několik	k4yIc4	několik
možných	možný	k2eAgNnPc2d1	možné
čtení	čtení	k1gNnPc2	čtení
<g/>
,	,	kIx,	,
správná	správný	k2eAgFnSc1d1	správná
výslovnost	výslovnost	k1gFnSc1	výslovnost
se	se	k3xPyFc4	se
určí	určit	k5eAaPmIp3nS	určit
podle	podle	k7c2	podle
kontextu	kontext	k1gInSc2	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
čtení	čtení	k1gNnPc1	čtení
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
sinojaponská	sinojaponský	k2eAgFnSc1d1	sinojaponský
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zkomolenou	zkomolený	k2eAgFnSc4d1	zkomolená
původní	původní	k2eAgFnSc4d1	původní
čínskou	čínský	k2eAgFnSc4d1	čínská
výslovnost	výslovnost	k1gFnSc4	výslovnost
daného	daný	k2eAgInSc2d1	daný
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
čínština	čínština	k1gFnSc1	čínština
masivně	masivně	k6eAd1	masivně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
slovní	slovní	k2eAgFnSc4d1	slovní
zásobu	zásoba	k1gFnSc4	zásoba
japonštiny	japonština	k1gFnSc2	japonština
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
Čína	Čína	k1gFnSc1	Čína
kulturním	kulturní	k2eAgInSc7d1	kulturní
vzorem	vzor	k1gInSc7	vzor
a	a	k8xC	a
čínština	čínština	k1gFnSc1	čínština
jazykem	jazyk	k1gInSc7	jazyk
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
latina	latina	k1gFnSc1	latina
a	a	k8xC	a
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
sanskrt	sanskrt	k1gInSc4	sanskrt
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
mají	mít	k5eAaImIp3nP	mít
znaky	znak	k1gInPc4	znak
i	i	k8xC	i
japonské	japonský	k2eAgNnSc4d1	Japonské
čtení	čtení	k1gNnSc4	čtení
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jedno	jeden	k4xCgNnSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
japonských	japonský	k2eAgNnPc2d1	Japonské
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
odvozených	odvozený	k2eAgFnPc2d1	odvozená
od	od	k7c2	od
pojmu	pojem	k1gInSc2	pojem
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
daný	daný	k2eAgInSc4d1	daný
znak	znak	k1gInSc4	znak
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
<g/>
.	.	kIx.	.
</s>
<s>
Sinojaponské	Sinojaponský	k2eAgNnSc1d1	Sinojaponský
čtení	čtení	k1gNnSc1	čtení
bývá	bývat	k5eAaImIp3nS	bývat
preferováno	preferovat	k5eAaImNgNnS	preferovat
ve	v	k7c6	v
složených	složený	k2eAgInPc6d1	složený
slovech	slovo	k1gNnPc6	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
výslovnost	výslovnost	k1gFnSc1	výslovnost
už	už	k6eAd1	už
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
mnohdy	mnohdy	k6eAd1	mnohdy
není	být	k5eNaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
(	(	kIx(	(
<g/>
buď	buď	k8xC	buď
kvůli	kvůli	k7c3	kvůli
příliš	příliš	k6eAd1	příliš
velkým	velký	k2eAgFnPc3d1	velká
hláskovým	hláskový	k2eAgFnPc3d1	hlásková
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kvůli	kvůli	k7c3	kvůli
vývoji	vývoj	k1gInSc3	vývoj
čínské	čínský	k2eAgFnSc2d1	čínská
výslovnosti	výslovnost	k1gFnSc2	výslovnost
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
znak	znak	k1gInSc1	znak
přejat	přejmout	k5eAaPmNgInS	přejmout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
v	v	k7c6	v
čínštině	čínština	k1gFnSc6	čínština
znak	znak	k1gInSc1	znak
vždy	vždy	k6eAd1	vždy
označuje	označovat	k5eAaImIp3nS	označovat
právě	právě	k9	právě
jednu	jeden	k4xCgFnSc4	jeden
slabiku	slabika	k1gFnSc4	slabika
<g/>
,	,	kIx,	,
sinojaponské	sinojaponský	k2eAgNnSc4d1	sinojaponské
čtení	čtení	k1gNnSc4	čtení
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dvojslabičné	dvojslabičný	k2eAgInPc1d1	dvojslabičný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
znak	znak	k1gInSc1	znak
木	木	k?	木
"	"	kIx"	"
<g/>
strom	strom	k1gInSc1	strom
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
čínštině	čínština	k1gFnSc6	čínština
čte	číst	k5eAaImIp3nS	číst
mù	mù	k?	mù
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc3	jeho
sinojaponské	sinojaponský	k2eAgNnSc1d1	sinojaponské
čtení	čtení	k1gNnSc1	čtení
je	být	k5eAaImIp3nS	být
moku	moka	k1gFnSc4	moka
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
boku	bok	k1gInSc2	bok
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Příklad	příklad	k1gInSc1	příklad
japonského	japonský	k2eAgInSc2d1	japonský
zápisu	zápis	k1gInSc2	zápis
<g/>
:	:	kIx,	:
私	私	k?	私
<g/>
。	。	k?	。
Wataši	Wataše	k1gFnSc3	Wataše
wa	wa	k?	wa
bíru	bíra	k1gFnSc4	bíra
o	o	k7c4	o
nomimasu	nomimasa	k1gFnSc4	nomimasa
<g/>
.	.	kIx.	.
</s>
<s>
Japonština	japonština	k1gFnSc1	japonština
používá	používat	k5eAaImIp3nS	používat
dva	dva	k4xCgInPc4	dva
způsoby	způsob	k1gInPc4	způsob
řazení	řazení	k1gNnPc2	řazení
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovnících	slovník	k1gInPc6	slovník
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
používán	používat	k5eAaImNgInS	používat
systém	systém	k1gInSc1	systém
vycházející	vycházející	k2eAgInSc1d1	vycházející
ze	z	k7c2	z
slabičné	slabičný	k2eAgFnSc2d1	slabičná
abecedy	abeceda	k1gFnSc2	abeceda
godžúon	godžúona	k1gFnPc2	godžúona
(	(	kIx(	(
<g/>
五	五	k?	五
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
padesát	padesát	k4xCc4	padesát
znaků	znak	k1gInPc2	znak
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
znaků	znak	k1gInPc2	znak
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tabulce	tabulka	k1gFnSc3	tabulka
uvedené	uvedený	k2eAgFnPc4d1	uvedená
výše	výše	k1gFnPc4	výše
<g/>
.	.	kIx.	.
</s>
<s>
Sobě	se	k3xPyFc3	se
odpovídající	odpovídající	k2eAgInPc4d1	odpovídající
znaky	znak	k1gInPc4	znak
hiragany	hiragana	k1gFnSc2	hiragana
a	a	k8xC	a
katakany	katakana	k1gFnSc2	katakana
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
při	při	k7c6	při
řazení	řazení	k1gNnSc6	řazení
rovnocenné	rovnocenný	k2eAgInPc1d1	rovnocenný
<g/>
,	,	kIx,	,
slova	slovo	k1gNnPc4	slovo
psaná	psaný	k2eAgNnPc4d1	psané
kandži	kandž	k1gFnSc3	kandž
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
výslovnosti	výslovnost	k1gFnSc2	výslovnost
zapsané	zapsaný	k2eAgInPc1d1	zapsaný
hiraganou	hiragana	k1gFnSc7	hiragana
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
také	také	k9	také
používán	používat	k5eAaImNgInS	používat
starší	starý	k2eAgInSc1d2	starší
systém	systém	k1gInSc1	systém
iroha	iroha	k1gFnSc1	iroha
(	(	kIx(	(
<g/>
い	い	k?	い
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
podle	podle	k7c2	podle
mnemotechnické	mnemotechnický	k2eAgFnSc2d1	mnemotechnická
básně	báseň	k1gFnSc2	báseň
připisované	připisovaný	k2eAgFnSc2d1	připisovaná
mnichu	mnich	k1gMnSc3	mnich
Kúkaiovi	Kúkaius	k1gMnSc3	Kúkaius
<g/>
.	.	kIx.	.
</s>
<s>
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
(	(	kIx(	(
<g/>
substantiva	substantivum	k1gNnPc1	substantivum
<g/>
)	)	kIx)	)
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
rod	rod	k1gInSc4	rod
ani	ani	k8xC	ani
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
pádu	pád	k1gInSc2	pád
přebírají	přebírat	k5eAaImIp3nP	přebírat
částice	částice	k1gFnPc1	částice
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
připojené	připojený	k2eAgInPc4d1	připojený
za	za	k7c4	za
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
lze	lze	k6eAd1	lze
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
vnímat	vnímat	k5eAaImF	vnímat
jako	jako	k8xC	jako
pádové	pádový	k2eAgFnPc4d1	pádová
koncovky	koncovka	k1gFnPc4	koncovka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
používá	používat	k5eAaImIp3nS	používat
japonština	japonština	k1gFnSc1	japonština
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
již	již	k6eAd1	již
neproduktivní	produktivní	k2eNgFnSc3d1	neproduktivní
reduplikaci	reduplikace	k1gFnSc3	reduplikace
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
nově	nově	k6eAd1	nově
přejatá	přejatý	k2eAgNnPc1d1	přejaté
slova	slovo	k1gNnPc1	slovo
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
tvořena	tvořit	k5eAaImNgFnS	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
této	tento	k3xDgFnSc2	tento
operace	operace	k1gFnSc2	operace
není	být	k5eNaImIp3nS	být
plurál	plurál	k1gInSc1	plurál
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kolektivní	kolektivní	k2eAgNnSc4d1	kolektivní
množství	množství	k1gNnSc4	množství
<g/>
.	.	kIx.	.
人	人	k?	人
hito	hito	k1gMnSc1	hito
'	'	kIx"	'
<g/>
člověk	člověk	k1gMnSc1	člověk
<g/>
'	'	kIx"	'
人	人	k?	人
hitobito	hitobit	k2eAgNnSc1d1	hitobit
'	'	kIx"	'
<g/>
skupina	skupina	k1gFnSc1	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dva	dva	k4xCgMnPc1	dva
členové	člen	k1gMnPc1	člen
<g/>
)	)	kIx)	)
Na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
principu	princip	k1gInSc6	princip
fungují	fungovat	k5eAaImIp3nP	fungovat
i	i	k9	i
kolektivní	kolektivní	k2eAgInPc1d1	kolektivní
sufixy	sufix	k1gInPc1	sufix
-tači	-tači	k1gFnSc3	-tač
(	(	kIx(	(
<g/>
達	達	k?	達
<g/>
)	)	kIx)	)
a	a	k8xC	a
-ra	-ra	k?	-ra
(	(	kIx(	(
<g/>
等	等	k?	等
<g/>
)	)	kIx)	)
–	–	k?	–
太	太	k?	太
taró-tači	taróač	k1gMnPc1	taró-tač
neznačí	značit	k5eNaImIp3nP	značit
'	'	kIx"	'
<g/>
skupinu	skupina	k1gFnSc4	skupina
mužů	muž	k1gMnPc2	muž
jménem	jméno	k1gNnSc7	jméno
Taró	Taró	k1gFnSc2	Taró
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
'	'	kIx"	'
<g/>
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
Taró	Taró	k1gFnSc1	Taró
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
tohoto	tento	k3xDgNnSc2	tento
spojení	spojení	k1gNnSc2	spojení
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
kontextu	kontext	k1gInSc2	kontext
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
Taró	Taró	k1gMnSc1	Taró
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Taró	Taró	k1gFnSc1	Taró
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Taró	Taró	k1gFnSc1	Taró
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
sourozenci	sourozenec	k1gMnPc1	sourozenec
<g/>
'	'	kIx"	'
atp.	atp.	kA	atp.
Tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
i	i	k9	i
ze	z	k7c2	z
zájmen	zájmeno	k1gNnPc2	zájmeno
wataši-tači	watašiač	k1gInPc7	wataši-tač
nebo	nebo	k8xC	nebo
boku-ra	bokuo	k1gNnPc4	boku-ro
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
překládána	překládán	k2eAgNnPc1d1	překládáno
jako	jako	k8xC	jako
'	'	kIx"	'
<g/>
my	my	k3xPp1nPc1	my
<g/>
'	'	kIx"	'
,	,	kIx,	,
doslovný	doslovný	k2eAgInSc1d1	doslovný
překlad	překlad	k1gInSc1	překlad
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
jsem	být	k5eAaImIp1nS	být
já	já	k3xPp1nSc1	já
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Substantiva	substantivum	k1gNnPc1	substantivum
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
reflektovat	reflektovat	k5eAaImF	reflektovat
zdvořilost	zdvořilost	k1gFnSc4	zdvořilost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
rozlišení	rozlišení	k1gNnSc3	rozlišení
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dvou	dva	k4xCgInPc2	dva
zdvořilostních	zdvořilostní	k2eAgInPc2d1	zdvořilostní
sufixů	sufix	k1gInPc2	sufix
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
hiraganovým	hiraganův	k2eAgInSc7d1	hiraganův
お	お	k?	お
o-	o-	k?	o-
nebo	nebo	k8xC	nebo
ご	ご	k?	ご
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
御	御	k?	御
<g/>
)	)	kIx)	)
go-	go-	k?	go-
<g/>
.	.	kIx.	.
結	結	k?	結
kekkon	kekkon	k1gInSc1	kekkon
'	'	kIx"	'
<g/>
svatba	svatba	k1gFnSc1	svatba
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
ご	ご	k?	ご
go-kekkon	goekkon	k1gInSc1	go-kekkon
'	'	kIx"	'
<g/>
svatba	svatba	k1gFnSc1	svatba
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
zdvořile	zdvořile	k6eAd1	zdvořile
<g/>
;	;	kIx,	;
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
<g />
.	.	kIx.	.
</s>
<s>
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
Vaše	váš	k3xOp2gFnSc1	váš
svatba	svatba	k1gFnSc1	svatba
<g/>
)	)	kIx)	)
金	金	k?	金
kane	kanout	k5eAaImIp3nS	kanout
'	'	kIx"	'
<g/>
peníze	peníz	k1gInPc4	peníz
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
též	též	k9	též
'	'	kIx"	'
<g/>
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
<g/>
'	'	kIx"	'
<g/>
;	;	kIx,	;
お	お	k?	お
o-kane	oanout	k5eAaPmIp3nS	o-kanout
'	'	kIx"	'
<g/>
peníze	peníz	k1gInPc4	peníz
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
zdvořile	zdvořile	k6eAd1	zdvořile
<g/>
)	)	kIx)	)
Přídavná	přídavný	k2eAgNnPc4d1	přídavné
jména	jméno	k1gNnPc4	jméno
(	(	kIx(	(
<g/>
adjektiva	adjektivum	k1gNnSc2	adjektivum
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
pravá	pravý	k2eAgFnSc1d1	pravá
a	a	k8xC	a
nepravá	pravý	k2eNgFnSc1d1	nepravá
<g/>
.	.	kIx.	.
</s>
<s>
Pravá	pravý	k2eAgNnPc1d1	pravé
adjektiva	adjektivum	k1gNnPc1	adjektivum
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
.	.	kIx.	.
車	車	k?	車
<g/>
。	。	k?	。
Kuruma	Kurumum	k1gNnSc2	Kurumum
ga	ga	k?	ga
akai	akai	k1gNnSc2	akai
<g/>
.	.	kIx.	.
</s>
<s>
Auto	auto	k1gNnSc1	auto
je	být	k5eAaImIp3nS	být
červené	červený	k2eAgNnSc1d1	červené
<g/>
.	.	kIx.	.
車	車	k?	車
<g/>
。	。	k?	。
Kuruma	Kurumum	k1gNnSc2	Kurumum
ga	ga	k?	ga
akakunai	akakunai	k1gNnSc2	akakunai
<g/>
.	.	kIx.	.
</s>
<s>
Auto	auto	k1gNnSc1	auto
není	být	k5eNaImIp3nS	být
červené	červený	k2eAgNnSc1d1	červené
<g/>
.	.	kIx.	.
車	車	k?	車
<g/>
。	。	k?	。
Kuruma	Kurum	k1gMnSc2	Kurum
ga	ga	k?	ga
akakatta	akakatt	k1gMnSc2	akakatt
<g/>
.	.	kIx.	.
</s>
<s>
Auto	auto	k1gNnSc1	auto
bylo	být	k5eAaImAgNnS	být
červené	červený	k2eAgNnSc1d1	červené
<g/>
.	.	kIx.	.
車	車	k?	車
<g/>
。	。	k?	。
Kuruma	Kurum	k1gMnSc2	Kurum
ga	ga	k?	ga
akakunakatta	akakunakatt	k1gMnSc2	akakunakatt
<g/>
.	.	kIx.	.
</s>
<s>
Auto	auto	k1gNnSc1	auto
nebylo	být	k5eNaImAgNnS	být
červené	červený	k2eAgNnSc1d1	červené
<g/>
.	.	kIx.	.
</s>
<s>
Nepravá	pravý	k2eNgNnPc1d1	nepravé
adjektiva	adjektivum	k1gNnPc1	adjektivum
se	se	k3xPyFc4	se
připojují	připojovat	k5eAaImIp3nP	připojovat
pomocí	pomocí	k7c2	pomocí
partikule	partikule	k1gFnSc2	partikule
na	na	k7c4	na
(	(	kIx(	(
<g/>
な	な	k?	な
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
no	no	k9	no
(	(	kIx(	(
<g/>
の	の	k?	の
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
adjektivum	adjektivum	k1gNnSc4	adjektivum
tvořené	tvořený	k2eAgNnSc4d1	tvořené
ze	z	k7c2	z
substantiva	substantivum	k1gNnSc2	substantivum
<g/>
.	.	kIx.	.
会	会	k?	会
kaiša	kaiša	k6eAd1	kaiša
no	no	k9	no
kuruma	kuruma	k1gNnSc1	kuruma
podnikové	podnikový	k2eAgNnSc1d1	podnikové
auto	auto	k1gNnSc1	auto
Osobní	osobní	k2eAgNnSc1d1	osobní
zájmena	zájmeno	k1gNnPc1	zájmeno
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
poměrně	poměrně	k6eAd1	poměrně
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jejich	jejich	k3xOp3gInSc4	jejich
význam	význam	k1gInSc4	význam
lze	lze	k6eAd1	lze
vyrozumět	vyrozumět	k5eAaPmF	vyrozumět
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
<g/>
,	,	kIx,	,
vypouštějí	vypouštět	k5eAaImIp3nP	vypouštět
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Samostatná	samostatný	k2eAgFnSc1d1	samostatná
sada	sada	k1gFnSc1	sada
zájmen	zájmeno	k1gNnPc2	zájmeno
existuje	existovat	k5eAaImIp3nS	existovat
pro	pro	k7c4	pro
důvěrný	důvěrný	k2eAgInSc4d1	důvěrný
stupeň	stupeň	k1gInSc4	stupeň
a	a	k8xC	a
samostatná	samostatný	k2eAgFnSc1d1	samostatná
pro	pro	k7c4	pro
zdvořilý	zdvořilý	k2eAgInSc4d1	zdvořilý
stupeň	stupeň	k1gInSc4	stupeň
promluvy	promluva	k1gFnPc4	promluva
<g/>
.	.	kIx.	.
*	*	kIx~	*
Zájmena	zájmeno	k1gNnPc1	zájmeno
zdvořilého	zdvořilý	k2eAgInSc2d1	zdvořilý
stupně	stupeň	k1gInSc2	stupeň
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
rod	rod	k1gInSc1	rod
a	a	k8xC	a
samostatná	samostatný	k2eAgFnSc1d1	samostatná
sada	sada	k1gFnSc1	sada
zájmen	zájmeno	k1gNnPc2	zájmeno
končících	končící	k2eAgNnPc2d1	končící
na	na	k7c6	na
-ち	-ち	k?	-ち
-čira	-čira	k6eAd1	-čira
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
ani	ani	k8xC	ani
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
osobě	osoba	k1gFnSc6	osoba
jde	jít	k5eAaImIp3nS	jít
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
o	o	k7c4	o
spojení	spojení	k1gNnSc4	spojení
ukazovacího	ukazovací	k2eAgNnSc2d1	ukazovací
zájmena	zájmeno	k1gNnSc2	zájmeno
a	a	k8xC	a
podstatného	podstatný	k2eAgNnSc2d1	podstatné
jména	jméno	k1gNnSc2	jméno
人	人	k?	人
hito	hito	k1gMnSc1	hito
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
předpona	předpona	k1gFnSc1	předpona
こ	こ	k?	こ
kono	kono	k6eAd1	kono
nahrazena	nahrazen	k2eAgFnSc1d1	nahrazena
předponami	předpona	k1gFnPc7	předpona
そ	そ	k?	そ
sono	sono	k1gMnSc1	sono
nebo	nebo	k8xC	nebo
あ	あ	k?	あ
ano	ano	k9	ano
<g/>
.	.	kIx.	.
*	*	kIx~	*
Sada	sada	k1gFnSc1	sada
zájmen	zájmeno	k1gNnPc2	zájmeno
bez	bez	k7c2	bez
rozlišení	rozlišení	k1gNnSc2	rozlišení
čísla	číslo	k1gNnSc2	číslo
<g/>
:	:	kIx,	:
*	*	kIx~	*
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ani	ani	k8xC	ani
zde	zde	k6eAd1	zde
japonština	japonština	k1gFnSc1	japonština
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
mezi	mezi	k7c7	mezi
jednotným	jednotný	k2eAgNnSc7d1	jednotné
a	a	k8xC	a
množným	množný	k2eAgNnSc7d1	množné
číslem	číslo	k1gNnSc7	číslo
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
彼	彼	k?	彼
[	[	kIx(	[
<g/>
kare	kar	k1gInSc5	kar
<g/>
]	]	kIx)	]
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
stejně	stejně	k6eAd1	stejně
dobře	dobře	k6eAd1	dobře
on	on	k3xPp3gMnSc1	on
jako	jako	k8xS	jako
oni	onen	k3xDgMnPc1	onen
<g/>
;	;	kIx,	;
彼	彼	k?	彼
[	[	kIx(	[
<g/>
karera	karer	k1gMnSc2	karer
<g/>
]	]	kIx)	]
spíše	spíše	k9	spíše
nežli	nežli	k8xS	nežli
oni	onen	k3xDgMnPc1	onen
znamená	znamenat	k5eAaImIp3nS	znamenat
on	on	k3xPp3gInSc1	on
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
druzí	druhý	k4xOgMnPc1	druhý
<g/>
/	/	kIx~	/
<g/>
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
podobně	podobně	k6eAd1	podobně
i	i	k9	i
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
příkladech	příklad	k1gInPc6	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Japonština	japonština	k1gFnSc1	japonština
nemá	mít	k5eNaImIp3nS	mít
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
výrazy	výraz	k1gInPc4	výraz
pro	pro	k7c4	pro
přivlastňovací	přivlastňovací	k2eAgNnPc4d1	přivlastňovací
zájmena	zájmeno	k1gNnPc4	zájmeno
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
za	za	k7c4	za
tvar	tvar	k1gInSc4	tvar
osobního	osobní	k2eAgNnSc2d1	osobní
zájmena	zájmeno	k1gNnSc2	zájmeno
připojí	připojit	k5eAaPmIp3nS	připojit
genitivní	genitivní	k2eAgFnSc1d1	genitivní
částice	částice	k1gFnSc1	částice
の	の	k?	の
no	no	k9	no
<g/>
.	.	kIx.	.
私	私	k?	私
watakuši	watakuše	k1gFnSc4	watakuše
no	no	k9	no
okane	okanout	k5eAaPmIp3nS	okanout
"	"	kIx"	"
<g/>
mé	můj	k3xOp1gInPc1	můj
peníze	peníz	k1gInPc1	peníz
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
já	já	k3xPp1nSc1	já
od	od	k7c2	od
peníze	peníz	k1gInSc2	peníz
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
彼	彼	k?	彼
kare	kar	k1gInSc5	kar
no	no	k9	no
isu	isu	k?	isu
"	"	kIx"	"
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
židle	židle	k1gFnSc1	židle
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
on	on	k3xPp3gMnSc1	on
od	od	k7c2	od
židle	židle	k1gFnSc2	židle
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
zvratné	zvratný	k2eAgNnSc4d1	zvratné
zájmeno	zájmeno	k1gNnSc4	zájmeno
自	自	k?	自
džibun	džibun	k1gInSc1	džibun
<g/>
:	:	kIx,	:
自	自	k?	自
džibun	džibun	k1gInSc1	džibun
no	no	k9	no
šigoto	šigota	k1gFnSc5	šigota
"	"	kIx"	"
<g/>
svá	svůj	k3xOyFgFnSc1	svůj
práce	práce	k1gFnSc1	práce
<g/>
"	"	kIx"	"
Ukazovací	ukazovací	k2eAgNnPc1d1	ukazovací
zájmena	zájmeno	k1gNnPc1	zájmeno
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgInPc1d1	různý
tvary	tvar	k1gInPc1	tvar
podle	podle	k7c2	podle
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
mluvčího	mluvčí	k1gMnSc2	mluvčí
a	a	k8xC	a
posluchače	posluchač	k1gMnSc2	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
tvary	tvar	k1gInPc1	tvar
jmenné	jmenný	k2eAgInPc1d1	jmenný
(	(	kIx(	(
<g/>
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přívlastkové	přívlastkový	k2eAgInPc1d1	přívlastkový
(	(	kIx(	(
<g/>
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
výběrem	výběr	k1gInSc7	výběr
z	z	k7c2	z
množství	množství	k1gNnSc2	množství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpřídavnělé	zpřídavnělý	k2eAgInPc1d1	zpřídavnělý
(	(	kIx(	(
<g/>
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
určením	určení	k1gNnSc7	určení
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
lokální	lokální	k2eAgNnSc4d1	lokální
(	(	kIx(	(
<g/>
příslovečné	příslovečný	k2eAgNnSc4d1	příslovečné
určení	určení	k1gNnSc4	určení
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
mluvnici	mluvnice	k1gFnSc6	mluvnice
jsou	být	k5eAaImIp3nP	být
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
příslovce	příslovce	k1gNnSc4	příslovce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Japonština	japonština	k1gFnSc1	japonština
používá	používat	k5eAaImIp3nS	používat
dva	dva	k4xCgInPc4	dva
číselné	číselný	k2eAgInPc4d1	číselný
systémy	systém	k1gInPc4	systém
<g/>
:	:	kIx,	:
japonský	japonský	k2eAgMnSc1d1	japonský
pro	pro	k7c4	pro
čísla	číslo	k1gNnPc4	číslo
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
do	do	k7c2	do
deseti	deset	k4xCc2	deset
a	a	k8xC	a
sinojaponský	sinojaponský	k2eAgMnSc1d1	sinojaponský
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k8xS	takže
číslo	číslo	k1gNnSc1	číslo
123	[number]	k4	123
456	[number]	k4	456
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
kandži	kandž	k1gFnSc6	kandž
zapíše	zapsat	k5eAaPmIp3nS	zapsat
十	十	k?	十
(	(	kIx(	(
<g/>
džú	džú	k?	džú
ni	on	k3xPp3gFnSc4	on
man	mana	k1gFnPc2	mana
san	san	k?	san
zen	zen	k2eAgFnSc2d1	zen
jon	jon	k?	jon
hjaku	hjak	k1gInSc2	hjak
go	go	k?	go
džú	džú	k?	džú
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kandži	Kandzat	k5eAaPmIp1nS	Kandzat
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
styku	styk	k1gInSc6	styk
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
čísel	číslo	k1gNnPc2	číslo
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
požívají	požívat	k5eAaImIp3nP	požívat
se	se	k3xPyFc4	se
arabské	arabský	k2eAgFnSc2d1	arabská
číslice	číslice	k1gFnSc2	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
zatímco	zatímco	k8xS	zatímco
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
je	být	k5eAaImIp3nS	být
výchozí	výchozí	k2eAgMnSc1d1	výchozí
počítání	počítání	k1gNnSc4	počítání
v	v	k7c6	v
násobcích	násobek	k1gInPc6	násobek
1	[number]	k4	1
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Japonci	Japonec	k1gMnPc1	Japonec
počítají	počítat	k5eAaImIp3nP	počítat
po	po	k7c4	po
10	[number]	k4	10
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
je	on	k3xPp3gNnPc4	on
tedy	tedy	k9	tedy
doslova	doslova	k6eAd1	doslova
100	[number]	k4	100
0000	[number]	k4	0000
(	(	kIx(	(
<g/>
百	百	k?	百
hjaku	hjak	k1gInSc2	hjak
man	mana	k1gFnPc2	mana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
100	[number]	k4	100
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
sumy	suma	k1gFnPc1	suma
peněz	peníze	k1gInPc2	peníze
se	se	k3xPyFc4	se
často	často	k6eAd1	často
počítají	počítat	k5eAaImIp3nP	počítat
v	v	k7c6	v
desetitisících	desetitisíce	k1gInPc6	desetitisíce
(	(	kIx(	(
<g/>
万	万	k?	万
man	man	k1gMnSc1	man
<g/>
'	'	kIx"	'
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sinojaponský	Sinojaponský	k2eAgInSc1d1	Sinojaponský
systém	systém	k1gInSc1	systém
používá	používat	k5eAaImIp3nS	používat
tzv.	tzv.	kA	tzv.
numerativy	numerativ	k1gInPc1	numerativ
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
counter	counter	k1gMnSc1	counter
word	word	k1gMnSc1	word
<g/>
/	/	kIx~	/
<g/>
measure	measur	k1gMnSc5	measur
word	word	k1gMnSc1	word
<g/>
/	/	kIx~	/
<g/>
classifier	classifier	k1gMnSc1	classifier
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
jakési	jakýsi	k3yIgFnPc4	jakýsi
"	"	kIx"	"
<g/>
měrné	měrný	k2eAgFnPc4d1	měrná
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
počítaný	počítaný	k2eAgInSc4d1	počítaný
předmět	předmět	k1gInSc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
jeden	jeden	k4xCgMnSc1	jeden
pták	pták	k1gMnSc1	pták
je	být	k5eAaImIp3nS	být
鳥	鳥	k?	鳥
tori	tori	k6eAd1	tori
ičiwa	ičiwa	k6eAd1	ičiwa
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
znak	znak	k1gInSc1	znak
wa	wa	k?	wa
(	(	kIx(	(
<g/>
sinojaponské	sinojaponský	k2eAgNnSc1d1	sinojaponské
čtení	čtení	k1gNnSc1	čtení
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc1	význam
"	"	kIx"	"
<g/>
pero	pero	k1gNnSc1	pero
<g/>
,	,	kIx,	,
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Doslovný	doslovný	k2eAgInSc1d1	doslovný
překlad	překlad	k1gInSc1	překlad
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
pták	pták	k1gMnSc1	pták
jedno	jeden	k4xCgNnSc1	jeden
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Japonština	japonština	k1gFnSc1	japonština
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
tvarově	tvarově	k6eAd1	tvarově
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
množství	množství	k1gNnSc4	množství
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
si	se	k3xPyFc3	se
příponami	přípona	k1gFnPc7	přípona
-tači	-tač	k1gFnSc5	-tač
<g/>
,	,	kIx,	,
<g/>
-domo	-doma	k1gFnSc5	-doma
<g/>
,	,	kIx,	,
<g/>
-gata	-gata	k1gMnSc1	-gata
<g/>
,	,	kIx,	,
<g/>
-ra	-ra	k?	-ra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
私	私	k?	私
watašitači	watašitač	k1gMnSc3	watašitač
–	–	k?	–
my	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
彼	彼	k?	彼
karera	karera	k1gFnSc1	karera
–	–	k?	–
oni	onen	k3xDgMnPc1	onen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zdvojením	zdvojení	k1gNnSc7	zdvojení
(	(	kIx(	(
<g/>
我	我	k?	我
wareware	warewar	k1gMnSc5	warewar
–	–	k?	–
my	my	k3xPp1nPc1	my
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
vyjádřením	vyjádření	k1gNnSc7	vyjádření
počtu	počet	k1gInSc2	počet
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgNnPc1d1	Japonské
slovesa	sloveso	k1gNnPc1	sloveso
nevyjadřují	vyjadřovat	k5eNaImIp3nP	vyjadřovat
časováním	časování	k1gNnPc3	časování
osobu	osoba	k1gFnSc4	osoba
ani	ani	k8xC	ani
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
(	(	kIx(	(
<g/>
trpný	trpný	k2eAgMnSc1d1	trpný
<g/>
,	,	kIx,	,
činný	činný	k2eAgMnSc1d1	činný
<g/>
)	)	kIx)	)
a	a	k8xC	a
úroveň	úroveň	k1gFnSc4	úroveň
zdvořilosti	zdvořilost	k1gFnSc2	zdvořilost
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
tvary	tvar	k1gInPc1	tvar
má	mít	k5eAaImIp3nS	mít
minulý	minulý	k2eAgInSc1d1	minulý
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tvary	tvar	k1gInPc1	tvar
přítomného	přítomný	k2eAgMnSc2d1	přítomný
a	a	k8xC	a
budoucího	budoucí	k2eAgInSc2d1	budoucí
času	čas	k1gInSc2	čas
jsou	být	k5eAaImIp3nP	být
společné	společný	k2eAgInPc1d1	společný
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
příslovečným	příslovečný	k2eAgNnSc7d1	příslovečné
určením	určení	k1gNnSc7	určení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
časování	časování	k1gNnSc2	časování
se	se	k3xPyFc4	se
slovesa	sloveso	k1gNnPc1	sloveso
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
samohlásková	samohláskový	k2eAgNnPc4d1	samohláskové
a	a	k8xC	a
souhlásková	souhláskový	k2eAgNnPc4d1	souhláskové
<g/>
.	.	kIx.	.
</s>
<s>
Infinitiv	infinitiv	k1gInSc1	infinitiv
samohláskových	samohláskový	k2eAgNnPc2d1	samohláskové
sloves	sloveso	k1gNnPc2	sloveso
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
končícího	končící	k2eAgInSc2d1	končící
samohláskou	samohláska	k1gFnSc7	samohláska
e	e	k0	e
nebo	nebo	k8xC	nebo
i	i	k8xC	i
a	a	k8xC	a
z	z	k7c2	z
přípony	přípona	k1gFnSc2	přípona
-ru	-ru	k?	-ru
(	(	kIx(	(
<g/>
-る	-る	k?	-る
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Infinitiv	infinitiv	k1gInSc1	infinitiv
souhláskových	souhláskový	k2eAgNnPc2d1	souhláskové
sloves	sloveso	k1gNnPc2	sloveso
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
končícího	končící	k2eAgInSc2d1	končící
souhláskou	souhláska	k1gFnSc7	souhláska
nebo	nebo	k8xC	nebo
samohláskou	samohláska	k1gFnSc7	samohláska
a	a	k8xC	a
a	a	k8xC	a
z	z	k7c2	z
přípony	přípona	k1gFnSc2	přípona
-	-	kIx~	-
<g/>
u.	u.	k?	u.
Z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
pět	pět	k4xCc4	pět
tzv.	tzv.	kA	tzv.
slovesných	slovesný	k2eAgInPc2d1	slovesný
základů	základ	k1gInPc2	základ
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
tvoření	tvoření	k1gNnSc3	tvoření
slovesných	slovesný	k2eAgInPc2d1	slovesný
tvarů	tvar	k1gInPc2	tvar
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
.	.	kIx.	.
základ	základ	k1gInSc1	základ
(	(	kIx(	(
<g/>
končící	končící	k2eAgInSc1d1	končící
na	na	k7c4	na
-u	-u	k?	-u
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
základní	základní	k2eAgInSc4d1	základní
<g/>
,	,	kIx,	,
citační	citační	k2eAgInSc4d1	citační
tvar	tvar	k1gInSc4	tvar
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
má	mít	k5eAaImIp3nS	mít
společnou	společný	k2eAgFnSc4d1	společná
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
infinitivem	infinitiv	k1gInSc7	infinitiv
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
ho	on	k3xPp3gNnSc2	on
při	při	k7c6	při
důvěrném	důvěrný	k2eAgInSc6d1	důvěrný
projevu	projev	k1gInSc6	projev
použít	použít	k5eAaPmF	použít
jako	jako	k8xS	jako
samostatné	samostatný	k2eAgNnSc4d1	samostatné
plnovýznamové	plnovýznamový	k2eAgNnSc4d1	plnovýznamové
sloveso	sloveso	k1gNnSc4	sloveso
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
つ	つ	k?	つ
<g/>
。	。	k?	。
Cugi	Cuge	k1gFnSc4	Cuge
no	no	k9	no
eki	eki	k?	eki
de	de	k?	de
oriru	orir	k1gInSc2	orir
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vystoupím	vystoupit	k5eAaPmIp1nS	vystoupit
v	v	k7c6	v
příští	příští	k2eAgFnSc6d1	příští
stanici	stanice	k1gFnSc6	stanice
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Příště	příště	k6eAd1	příště
od	od	k7c2	od
stanice	stanice	k1gFnSc2	stanice
v	v	k7c6	v
vystoupit	vystoupit	k5eAaPmF	vystoupit
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
První	první	k4xOgFnSc1	první
osoba	osoba	k1gFnSc1	osoba
vyplyne	vyplynout	k5eAaPmIp3nS	vyplynout
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
žádnými	žádný	k3yNgInPc7	žádný
jazykovými	jazykový	k2eAgInPc7d1	jazykový
prostředky	prostředek	k1gInPc7	prostředek
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
<g/>
,	,	kIx,	,
použilo	použít	k5eAaPmAgNnS	použít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
osobní	osobní	k2eAgNnSc1d1	osobní
zájmeno	zájmeno	k1gNnSc1	zájmeno
s	s	k7c7	s
částicí	částice	k1gFnSc7	částice
は	は	k?	は
nebo	nebo	k8xC	nebo
が	が	k?	が
<g/>
)	)	kIx)	)
Další	další	k2eAgInPc1d1	další
slovesné	slovesný	k2eAgInPc1d1	slovesný
tvary	tvar	k1gInPc1	tvar
(	(	kIx(	(
<g/>
poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
při	při	k7c6	při
výslovnosti	výslovnost	k1gFnSc6	výslovnost
se	se	k3xPyFc4	se
přípona	přípona	k1gFnSc1	přípona
-masu	-mas	k1gInSc2	-mas
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
na	na	k7c4	na
[	[	kIx(	[
<g/>
-mas	-mas	k1gInSc4	-mas
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
přípona	přípona	k1gFnSc1	přípona
-šita	-šita	k1gFnSc1	-šita
na	na	k7c4	na
[	[	kIx(	[
<g/>
-šta	-šta	k6eAd1	-šta
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
nejsou	být	k5eNaImIp3nP	být
japonské	japonský	k2eAgFnPc1d1	japonská
částice	částice	k1gFnPc1	částice
(	(	kIx(	(
<g/>
partikule	partikule	k1gFnPc1	partikule
<g/>
)	)	kIx)	)
pouze	pouze	k6eAd1	pouze
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
zbytkovým	zbytkový	k2eAgInSc7d1	zbytkový
slovním	slovní	k2eAgInSc7d1	slovní
druhem	druh	k1gInSc7	druh
bez	bez	k7c2	bez
jasně	jasně	k9	jasně
vymezené	vymezený	k2eAgFnSc2d1	vymezená
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgNnPc4d1	důležité
gramatická	gramatický	k2eAgNnPc4d1	gramatické
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
určují	určovat	k5eAaImIp3nP	určovat
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
větnými	větný	k2eAgMnPc7d1	větný
členy	člen	k1gMnPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Kladou	klást	k5eAaImIp3nP	klást
se	se	k3xPyFc4	se
za	za	k7c4	za
slova	slovo	k1gNnPc4	slovo
nebo	nebo	k8xC	nebo
fráze	fráze	k1gFnPc4	fráze
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
jmenné	jmenný	k2eAgFnPc1d1	jmenná
fráze	fráze	k1gFnPc1	fráze
<g/>
)	)	kIx)	)
a	a	k8xC	a
určují	určovat	k5eAaImIp3nP	určovat
jejich	jejich	k3xOp3gFnSc4	jejich
funkci	funkce	k1gFnSc4	funkce
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
předložky	předložka	k1gFnSc2	předložka
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
záložky	záložka	k1gFnSc2	záložka
<g/>
,	,	kIx,	,
postpozice	postpozice	k1gFnSc2	postpozice
<g/>
)	)	kIx)	)
a	a	k8xC	a
pádové	pádový	k2eAgFnSc2d1	pádová
koncovky	koncovka	k1gFnSc2	koncovka
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Lingvisté	lingvista	k1gMnPc1	lingvista
je	on	k3xPp3gInPc4	on
převážně	převážně	k6eAd1	převážně
popisují	popisovat	k5eAaImIp3nP	popisovat
jako	jako	k9	jako
samostatná	samostatný	k2eAgNnPc4d1	samostatné
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
chybějícímu	chybějící	k2eAgNnSc3d1	chybějící
grafickému	grafický	k2eAgNnSc3d1	grafické
vyznačení	vyznačení	k1gNnSc3	vyznačení
hranic	hranice	k1gFnPc2	hranice
slov	slovo	k1gNnPc2	slovo
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
by	by	kYmCp3nS	by
je	on	k3xPp3gNnSc4	on
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
chápat	chápat	k5eAaImF	chápat
i	i	k9	i
jako	jako	k9	jako
pádové	pádový	k2eAgFnSc2d1	pádová
koncovky	koncovka	k1gFnSc2	koncovka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
samostatnost	samostatnost	k1gFnSc4	samostatnost
nicméně	nicméně	k8xC	nicméně
mluví	mluvit	k5eAaImIp3nS	mluvit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
připojují	připojovat	k5eAaImIp3nP	připojovat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
frázi	fráze	k1gFnSc4	fráze
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
za	za	k7c4	za
každé	každý	k3xTgNnSc4	každý
slovo	slovo	k1gNnSc4	slovo
fráze	fráze	k1gFnSc2	fráze
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
は	は	k?	は
se	se	k3xPyFc4	se
nečte	číst	k5eNaImIp3nS	číst
ha	ha	kA	ha
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
jejímu	její	k3xOp3gInSc3	její
zápisu	zápis	k1gInSc3	zápis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
wa	wa	k?	wa
<g/>
.	.	kIx.	.
</s>
<s>
Zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
téma	téma	k1gNnSc4	téma
promluvy	promluva	k1gFnSc2	promluva
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
označovat	označovat	k5eAaImF	označovat
podmět	podmět	k1gInSc4	podmět
nebo	nebo	k8xC	nebo
předmět	předmět	k1gInSc4	předmět
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
bývá	bývat	k5eAaImIp3nS	bývat
japonština	japonština	k1gFnSc1	japonština
uváděna	uvádět	k5eAaImNgFnS	uvádět
jako	jako	k8xC	jako
příklad	příklad	k1gInSc1	příklad
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
aktuální	aktuální	k2eAgNnSc4d1	aktuální
členění	členění	k1gNnSc4	členění
vyjádřené	vyjádřený	k2eAgNnSc4d1	vyjádřené
morfologicky	morfologicky	k6eAd1	morfologicky
<g/>
.	.	kIx.	.
大	大	k?	大
<g/>
。	。	k?	。
Ósaka	Ósaka	k1gFnSc1	Ósaka
wa	wa	k?	wa
ókina	ókino	k1gNnSc2	ókino
mači	mač	k1gInSc3	mač
desu	desus	k1gInSc2	desus
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Ósaka	Ósaka	k1gFnSc1	Ósaka
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc4d1	velké
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Ósaka	Ósaka	k1gFnSc1	Ósaka
téma	téma	k1gFnSc1	téma
velký	velký	k2eAgInSc4d1	velký
město	město	k1gNnSc1	město
být	být	k5eAaImF	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Částice	částice	k1gFnSc1	částice
が	が	k?	が
ga	ga	k?	ga
označuje	označovat	k5eAaImIp3nS	označovat
předcházející	předcházející	k2eAgFnSc4d1	předcházející
frázi	fráze	k1gFnSc4	fráze
jako	jako	k8xC	jako
podmět	podmět	k1gInSc4	podmět
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
podmět	podmět	k1gInSc1	podmět
označen	označit	k5eAaPmNgInS	označit
částicí	částice	k1gFnSc7	částice
は	は	k?	は
jako	jako	k8xS	jako
téma	téma	k1gNnSc1	téma
<g/>
,	,	kIx,	,
částice	částice	k1gFnSc1	částice
が	が	k?	が
už	už	k6eAd1	už
se	se	k3xPyFc4	se
nepřidává	přidávat	k5eNaImIp3nS	přidávat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
můžou	můžou	k?	můžou
obě	dva	k4xCgFnPc1	dva
částice	částice	k1gFnPc1	částice
objevit	objevit	k5eAaPmF	objevit
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
souvětí	souvětí	k1gNnSc6	souvětí
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
částice	částice	k1gFnSc1	částice
は	は	k?	は
označuje	označovat	k5eAaImIp3nS	označovat
podmět	podmět	k1gInSc1	podmět
hlavní	hlavní	k2eAgInSc1d1	hlavní
a	a	k8xC	a
částice	částice	k1gFnSc1	částice
が	が	k?	が
podmět	podmět	k1gInSc4	podmět
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
高	高	k?	高
takai	takai	k6eAd1	takai
kagu	kagu	k6eAd1	kagu
"	"	kIx"	"
<g/>
drahý	drahý	k2eAgInSc1d1	drahý
nábytek	nábytek	k1gInSc1	nábytek
<g/>
"	"	kIx"	"
家	家	k?	家
<g/>
。	。	k?	。
Kagu	Kagus	k1gInSc2	Kagus
ga	ga	k?	ga
takai	taka	k1gFnSc2	taka
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nábytek	nábytek	k1gInSc1	nábytek
je	být	k5eAaImIp3nS	být
drahý	drahý	k2eAgInSc1d1	drahý
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
nábytek	nábytek	k1gInSc1	nábytek
podmět	podmět	k1gInSc1	podmět
drahý	drahý	k2eAgInSc1d1	drahý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
山	山	k?	山
<g/>
。	。	k?	。
Jama	Jam	k1gInSc2	Jam
no	no	k9	no
ue	ue	k?	ue
wa	wa	k?	wa
kešiki	kešik	k1gFnSc2	kešik
ga	ga	k?	ga
ii	ii	k?	ii
desu	desus	k1gInSc2	desus
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Na	na	k7c6	na
horách	hora	k1gFnPc6	hora
je	být	k5eAaImIp3nS	být
krásná	krásný	k2eAgFnSc1d1	krásná
krajina	krajina	k1gFnSc1	krajina
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
hora	hora	k1gFnSc1	hora
genitiv	genitiv	k1gInSc1	genitiv
nahoře	nahoře	k6eAd1	nahoře
téma	téma	k1gNnSc4	téma
krajina	krajina	k1gFnSc1	krajina
podmět	podmět	k1gInSc1	podmět
<g />
.	.	kIx.	.
</s>
<s>
krásný	krásný	k2eAgInSc1d1	krásný
být	být	k5eAaImF	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Částice	částice	k1gFnSc1	částice
を	を	k?	を
se	se	k3xPyFc4	se
nečte	číst	k5eNaImIp3nS	číst
wo	wo	k?	wo
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
jejímu	její	k3xOp3gInSc3	její
zápisu	zápis	k1gInSc3	zápis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o.	o.	k?	o.
Má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
akuzativu	akuzativ	k1gInSc2	akuzativ
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
označuje	označovat	k5eAaImIp3nS	označovat
přímý	přímý	k2eAgInSc4d1	přímý
předmět	předmět	k1gInSc4	předmět
<g/>
.	.	kIx.	.
本	本	k?	本
hon	hon	k1gInSc1	hon
o	o	k7c4	o
jomu	joma	k1gFnSc4	joma
"	"	kIx"	"
<g/>
číst	číst	k5eAaImF	číst
knihu	kniha	k1gFnSc4	kniha
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
kniha	kniha	k1gFnSc1	kniha
předmět	předmět	k1gInSc1	předmět
číst	číst	k5eAaImF	číst
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
絵	絵	k?	絵
e	e	k0	e
o	o	k7c4	o
kaku	kaka	k1gMnSc4	kaka
"	"	kIx"	"
<g/>
malovat	malovat	k5eAaImF	malovat
obraz	obraz	k1gInSc4	obraz
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
obraz	obraz	k1gInSc1	obraz
předmět	předmět	k1gInSc1	předmět
malovat	malovat	k5eAaImF	malovat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Částice	částice	k1gFnSc1	částice
の	の	k?	の
no	no	k9	no
má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
genitivu	genitiv	k1gInSc2	genitiv
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
přivlastňovací	přivlastňovací	k2eAgInPc4d1	přivlastňovací
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
vnímat	vnímat	k5eAaImF	vnímat
jako	jako	k9	jako
genitivní	genitivní	k2eAgFnSc4d1	genitivní
pádovou	pádový	k2eAgFnSc4d1	pádová
koncovku	koncovka	k1gFnSc4	koncovka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
jako	jako	k9	jako
jakousi	jakýsi	k3yIgFnSc4	jakýsi
podřadící	podřadící	k2eAgFnSc4d1	podřadící
spojku	spojka	k1gFnSc4	spojka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
frází	fráze	k1gFnSc7	fráze
vlastníka	vlastník	k1gMnSc2	vlastník
a	a	k8xC	a
frází	fráze	k1gFnSc7	fráze
vlastněného	vlastněný	k2eAgMnSc2d1	vlastněný
<g/>
.	.	kIx.	.
雨	雨	k?	雨
ame	ame	k?	ame
no	no	k9	no
oto	oto	k?	oto
"	"	kIx"	"
<g/>
zvuk	zvuk	k1gInSc1	zvuk
deště	dešť	k1gInSc2	dešť
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
déšť	déšť	k1gInSc1	déšť
od	od	k7c2	od
zvuk	zvuk	k1gInSc4	zvuk
<g/>
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
海	海	k?	海
umi	umi	k?	umi
no	no	k9	no
sakana	sakana	k1gFnSc1	sakana
"	"	kIx"	"
<g/>
mořská	mořský	k2eAgFnSc1d1	mořská
ryba	ryba	k1gFnSc1	ryba
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
moře	moře	k1gNnSc1	moře
od	od	k7c2	od
ryba	ryba	k1gFnSc1	ryba
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
子	子	k?	子
kodomo	kodomo	k6eAd1	kodomo
no	no	k9	no
nacu	nacu	k5eAaPmIp1nS	nacu
no	no	k9	no
kucu	kucu	k6eAd1	kucu
"	"	kIx"	"
<g/>
dětské	dětský	k2eAgFnPc1d1	dětská
letní	letní	k2eAgFnPc1d1	letní
boty	bota	k1gFnPc1	bota
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
dítě	dítě	k1gNnSc1	dítě
od	od	k7c2	od
léto	léto	k1gNnSc4	léto
od	od	k7c2	od
bota	bota	k1gFnSc1	bota
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
へ	へ	k?	へ
se	se	k3xPyFc4	se
nečte	číst	k5eNaImIp3nS	číst
he	he	k0	he
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
jejímu	její	k3xOp3gInSc3	její
zápisu	zápis	k1gInSc3	zápis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
e.	e.	k?	e.
Označuje	označovat	k5eAaImIp3nS	označovat
směr	směr	k1gInSc4	směr
pohybu	pohyb	k1gInSc2	pohyb
nebo	nebo	k8xC	nebo
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
海	海	k?	海
umi	umi	k?	umi
e	e	k0	e
iku	iku	k?	iku
"	"	kIx"	"
<g/>
jet	jet	k5eAaImF	jet
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
moře	moře	k1gNnSc4	moře
k	k	k7c3	k
jet	jet	k5eAaImNgMnS	jet
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
父	父	k?	父
čiči	čiči	k0	čiči
e	e	k0	e
no	no	k9	no
tegami	tega	k1gFnPc7	tega
"	"	kIx"	"
<g/>
dopis	dopis	k1gInSc1	dopis
otci	otec	k1gMnSc3	otec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
k	k	k7c3	k
od	od	k7c2	od
dopis	dopis	k1gInSc4	dopis
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Částice	částice	k1gFnSc1	částice
に	に	k?	に
ni	on	k3xPp3gFnSc4	on
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
funkci	funkce	k1gFnSc4	funkce
dativu	dativ	k1gInSc2	dativ
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
označovat	označovat	k5eAaImF	označovat
místo	místo	k1gNnSc4	místo
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
účel	účel	k1gInSc4	účel
nebo	nebo	k8xC	nebo
cíl	cíl	k1gInSc4	cíl
děje	děj	k1gInSc2	děj
nebo	nebo	k8xC	nebo
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
částice	částice	k1gFnSc2	částice
へ	へ	k?	へ
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
určuje	určovat	k5eAaImIp3nS	určovat
cíl	cíl	k1gInSc4	cíl
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
へ	へ	k?	へ
určuje	určovat	k5eAaImIp3nS	určovat
směr	směr	k1gInSc4	směr
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
totéž	týž	k3xTgNnSc4	týž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
母	母	k?	母
<g/>
。	。	k?	。
Haha	Haha	k?	Haha
ni	on	k3xPp3gFnSc4	on
ageru	agera	k1gFnSc4	agera
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Dát	dát	k5eAaPmF	dát
matce	matka	k1gFnSc3	matka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
matka	matka	k1gFnSc1	matka
k	k	k7c3	k
dát	dát	k5eAaPmF	dát
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
友	友	k?	友
<g/>
。	。	k?	。
Tomodači	Tomodač	k1gInSc3	Tomodač
ga	ga	k?	ga
Hirošima	Hirošima	k1gFnSc1	Hirošima
ni	on	k3xPp3gFnSc4	on
sumu	suma	k1gFnSc4	suma
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Přítel	přítel	k1gMnSc1	přítel
bydlí	bydlet	k5eAaImIp3nS	bydlet
v	v	k7c6	v
Hirošimě	Hirošima	k1gFnSc6	Hirošima
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
přítel	přítel	k1gMnSc1	přítel
podmět	podmět	k1gInSc4	podmět
Hirošima	Hirošima	k1gFnSc1	Hirošima
v	v	k7c6	v
bydlet	bydlet	k5eAaImF	bydlet
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
部	部	k?	部
<g/>
。	。	k?	。
Heja	heja	k0	heja
ni	on	k3xPp3gFnSc4	on
<g />
.	.	kIx.	.
</s>
<s>
hairu	haira	k1gFnSc4	haira
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
místnost	místnost	k1gFnSc1	místnost
do	do	k7c2	do
vstoupit	vstoupit	k5eAaPmF	vstoupit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
夏	夏	k?	夏
nacu	nacu	k5eAaPmIp1nS	nacu
ni	on	k3xPp3gFnSc4	on
"	"	kIx"	"
<g/>
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
léto	léto	k1gNnSc1	léto
v	v	k7c6	v
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
学	学	k?	学
<g/>
。	。	k?	。
Gakkó	Gakkó	k1gMnSc3	Gakkó
ni	on	k3xPp3gFnSc4	on
iku	iku	k?	iku
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
Jdu	jít	k5eAaImIp1nS	jít
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
škola	škola	k1gFnSc1	škola
do	do	k7c2	do
jít	jít	k5eAaImF	jít
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
学	学	k?	学
<g/>
。	。	k?	。
Gakkó	Gakkó	k1gFnSc2	Gakkó
e	e	k0	e
iku	iku	k?	iku
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jdu	jít	k5eAaImIp1nS	jít
ke	k	k7c3	k
škole	škola	k1gFnSc3	škola
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
škola	škola	k1gFnSc1	škola
k	k	k7c3	k
jít	jít	k5eAaImF	jít
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Tato	tento	k3xDgFnSc1	tento
částice	částice	k1gFnSc1	částice
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgNnPc2d1	další
použití	použití	k1gNnPc2	použití
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
nepravých	pravý	k2eNgNnPc2d1	nepravé
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
příslovce	příslovce	k1gNnSc1	příslovce
<g/>
:	:	kIx,	:
静	静	k?	静
šizukani	šizukaň	k1gFnSc6	šizukaň
"	"	kIx"	"
<g/>
tiše	tiš	k1gFnSc2	tiš
<g/>
"	"	kIx"	"
Částice	částice	k1gFnSc1	částice
で	で	k?	で
de	de	k?	de
má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
instrumentálu	instrumentál	k1gInSc2	instrumentál
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
prostředek	prostředek	k1gInSc4	prostředek
nebo	nebo	k8xC	nebo
způsob	způsob	k1gInSc4	způsob
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
船	船	k?	船
fune	fune	k1gInSc1	fune
de	de	k?	de
iku	iku	k?	iku
"	"	kIx"	"
<g/>
jet	jet	k5eAaImF	jet
lodí	loď	k1gFnSc7	loď
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
loď	loď	k1gFnSc1	loď
s	s	k7c7	s
jet	jet	k5eAaImNgMnS	jet
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
英	英	k?	英
eigo	eigo	k1gNnSc4	eigo
de	de	k?	de
hanasu	hanas	k1gInSc2	hanas
"	"	kIx"	"
<g/>
mluvit	mluvit	k5eAaImF	mluvit
anglicky	anglicky	k6eAd1	anglicky
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
s	s	k7c7	s
mluvit	mluvit	k5eAaImF	mluvit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Částice	částice	k1gFnSc1	částice
か	か	k?	か
kara	kara	k1gFnSc1	kara
má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
ablativu	ablativ	k1gInSc2	ablativ
<g/>
,	,	kIx,	,
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
místní	místní	k2eAgNnSc4d1	místní
nebo	nebo	k8xC	nebo
časové	časový	k2eAgNnSc4d1	časové
východisko	východisko	k1gNnSc4	východisko
nebo	nebo	k8xC	nebo
příčinu	příčina	k1gFnSc4	příčina
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
českým	český	k2eAgFnPc3d1	Česká
předložkám	předložka	k1gFnPc3	předložka
z	z	k7c2	z
<g/>
,	,	kIx,	,
od	od	k7c2	od
<g/>
.	.	kIx.	.
船	船	k?	船
fune	fune	k1gInSc1	fune
wa	wa	k?	wa
minato	minato	k6eAd1	minato
kara	kara	k1gFnSc1	kara
deru	drát	k5eAaImIp1nS	drát
"	"	kIx"	"
<g/>
lodě	loď	k1gFnPc1	loď
odjíždějí	odjíždět	k5eAaImIp3nP	odjíždět
z	z	k7c2	z
přístavu	přístav	k1gInSc2	přístav
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
loď	lodit	k5eAaImRp2nS	lodit
téma	téma	k1gNnSc4	téma
přístav	přístav	k1gInSc1	přístav
z	z	k7c2	z
odjíždět	odjíždět	k5eAaImF	odjíždět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
月	月	k?	月
gecujóbi	gecujóbi	k6eAd1	gecujóbi
kara	kara	k1gFnSc1	kara
hataraku	hatarak	k1gInSc2	hatarak
"	"	kIx"	"
<g/>
pracovat	pracovat	k5eAaImF	pracovat
od	od	k7c2	od
pondělí	pondělí	k1gNnSc2	pondělí
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
pondělí	pondělí	k1gNnSc4	pondělí
od	od	k7c2	od
pracovat	pracovat	k5eAaImF	pracovat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
酒	酒	k?	酒
sake	sake	k1gInSc1	sake
wa	wa	k?	wa
kome	komat	k5eAaPmIp3nS	komat
kara	kara	k1gFnSc1	kara
cukuru	cukur	k1gInSc2	cukur
"	"	kIx"	"
<g/>
sake	sake	k1gFnSc1	sake
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
rýže	rýže	k1gFnSc2	rýže
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
sake	sakat	k5eAaPmIp3nS	sakat
téma	téma	k1gNnSc4	téma
rýže	rýže	k1gFnSc2	rýže
z	z	k7c2	z
vyrábět	vyrábět	k5eAaImF	vyrábět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Částice	částice	k1gFnSc1	částice
ま	ま	k?	ま
made	madat	k5eAaPmIp3nS	madat
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
místní	místní	k2eAgFnSc4d1	místní
nebo	nebo	k8xC	nebo
časovou	časový	k2eAgFnSc4d1	časová
krajní	krajní	k2eAgFnSc4d1	krajní
mez	mez	k1gFnSc4	mez
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
českému	český	k2eAgMnSc3d1	český
až	až	k6eAd1	až
do	do	k7c2	do
<g/>
.	.	kIx.	.
京	京	k?	京
Kjóto	Kjóto	k1gNnSc1	Kjóto
made	mad	k1gInSc2	mad
iku	iku	k?	iku
"	"	kIx"	"
<g/>
jet	jet	k5eAaImF	jet
až	až	k9	až
do	do	k7c2	do
Kjóta	Kjóto	k1gNnSc2	Kjóto
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Kjóto	Kjót	k2eAgNnSc1d1	Kjóto
až-do	ažo	k1gNnSc1	až-do
jet	jet	k5eAaImF	jet
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
朝	朝	k?	朝
asa	asa	k?	asa
made	made	k1gInSc4	made
okiru	okira	k1gMnSc4	okira
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
vzhůru	vzhůru	k6eAd1	vzhůru
do	do	k7c2	do
rána	ráno	k1gNnSc2	ráno
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
ráno	ráno	k6eAd1	ráno
až-do	ažo	k1gNnSc1	až-do
bdít	bdít	k5eAaImF	bdít
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Částice	částice	k1gFnSc1	částice
と	と	k?	と
to	ten	k3xDgNnSc1	ten
nemá	mít	k5eNaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
pádové	pádový	k2eAgFnSc2d1	pádová
koncovky	koncovka	k1gFnSc2	koncovka
ani	ani	k8xC	ani
předložky	předložka	k1gFnSc2	předložka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
souřadící	souřadící	k2eAgFnPc4d1	souřadící
spojky	spojka	k1gFnPc4	spojka
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
u	u	k7c2	u
úplného	úplný	k2eAgInSc2d1	úplný
výčtu	výčet	k1gInSc2	výčet
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
funkci	funkce	k1gFnSc4	funkce
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
částice	částice	k1gFnSc1	částice
や	や	k?	や
ja	ja	k?	ja
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
u	u	k7c2	u
výčtu	výčet	k1gInSc2	výčet
neúplného	úplný	k2eNgInSc2d1	neúplný
<g/>
.	.	kIx.	.
大	大	k?	大
ókí	ókí	k?	ókí
ki	ki	k?	ki
to	ten	k3xDgNnSc4	ten
čísai	čísai	k6eAd1	čísai
iši	iši	k?	iši
"	"	kIx"	"
<g/>
velký	velký	k2eAgInSc1d1	velký
strom	strom	k1gInSc1	strom
a	a	k8xC	a
malý	malý	k2eAgInSc1d1	malý
kámen	kámen	k1gInSc1	kámen
<g/>
"	"	kIx"	"
女	女	k?	女
onnanoko	onnanoko	k6eAd1	onnanoko
to	ten	k3xDgNnSc1	ten
otokonoko	otokonoko	k6eAd1	otokonoko
"	"	kIx"	"
<g/>
děvčata	děvče	k1gNnPc1	děvče
a	a	k8xC	a
chlapci	chlapec	k1gMnPc1	chlapec
<g/>
"	"	kIx"	"
庭	庭	k?	庭
<g/>
。	。	k?	。
Niwa	Niwum	k1gNnSc2	Niwum
ni	on	k3xPp3gFnSc4	on
neko	ko	k6eNd1	ko
ja	ja	k?	ja
<g />
.	.	kIx.	.
</s>
<s>
inu	inu	k9	inu
ga	ga	k?	ga
iru	iru	k?	iru
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
je	být	k5eAaImIp3nS	být
kočka	kočka	k1gFnSc1	kočka
a	a	k8xC	a
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
částice	částice	k1gFnSc1	částice
や	や	k?	や
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tam	tam	k6eAd1	tam
i	i	k9	i
další	další	k2eAgNnPc1d1	další
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
)	)	kIx)	)
Částice	částice	k1gFnSc1	částice
も	も	k?	も
mo	mo	k?	mo
zhruba	zhruba	k6eAd1	zhruba
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
českým	český	k2eAgMnPc3d1	český
slovům	slovo	k1gNnPc3	slovo
i	i	k9	i
<g/>
,	,	kIx,	,
také	také	k9	také
<g/>
,	,	kIx,	,
v	v	k7c6	v
záporu	zápor	k1gInSc6	zápor
ani	ani	k9	ani
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vyskytne	vyskytnout	k5eAaPmIp3nS	vyskytnout
společně	společně	k6eAd1	společně
s	s	k7c7	s
částicí	částice	k1gFnSc7	částice
を	を	k?	を
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
dokonce	dokonce	k9	dokonce
i	i	k9	i
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
地	地	k?	地
čizu	čizu	k5eAaPmIp1nS	čizu
o	o	k7c6	o
kau	kau	k?	kau
"	"	kIx"	"
<g/>
koupím	koupit	k5eAaPmIp1nS	koupit
si	se	k3xPyFc3	se
mapu	mapa	k1gFnSc4	mapa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
mapa	mapa	k1gFnSc1	mapa
předmět	předmět	k1gInSc1	předmět
koupit	koupit	k5eAaPmF	koupit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
雑	雑	k?	雑
zašši	zašsat	k5eAaPmIp1nSwK	zašsat
mo	mo	k?	mo
kau	kau	k?	kau
"	"	kIx"	"
<g/>
koupím	koupit	k5eAaPmIp1nS	koupit
si	se	k3xPyFc3	se
i	i	k9	i
<g />
.	.	kIx.	.
</s>
<s>
časopis	časopis	k1gInSc1	časopis
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
časopis	časopis	k1gInSc1	časopis
i	i	k8xC	i
koupit	koupit	k5eAaPmF	koupit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
本	本	k?	本
hon	hon	k1gInSc1	hon
o	o	k7c4	o
mo	mo	k?	mo
kau	kau	k?	kau
"	"	kIx"	"
<g/>
koupím	koupit	k5eAaPmIp1nS	koupit
si	se	k3xPyFc3	se
dokonce	dokonce	k9	dokonce
i	i	k9	i
knihu	kniha	k1gFnSc4	kniha
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
kniha	kniha	k1gFnSc1	kniha
předmět	předmět	k1gInSc1	předmět
i	i	k9	i
koupit	koupit	k5eAaPmF	koupit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Částice	částice	k1gFnSc1	částice
か	か	k?	か
ka	ka	k?	ka
se	se	k3xPyFc4	se
klade	klást	k5eAaImIp3nS	klást
na	na	k7c4	na
konec	konec	k1gInSc4	konec
věty	věta	k1gFnSc2	věta
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
tázací	tázací	k2eAgFnSc4d1	tázací
větu	věta	k1gFnSc4	věta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
tázací	tázací	k2eAgFnSc1d1	tázací
věta	věta	k1gFnSc1	věta
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
nevyznačuje	vyznačovat	k5eNaImIp3nS	vyznačovat
otazníkem	otazník	k1gInSc7	otazník
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nemění	měnit	k5eNaImIp3nS	měnit
slovosled	slovosled	k1gInSc1	slovosled
<g/>
.	.	kIx.	.
枝	枝	k?	枝
<g/>
。	。	k?	。
Eda	Eda	k1gMnSc1	Eda
ga	ga	k?	ga
aoi	aoi	k?	aoi
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Větev	větev	k1gFnSc1	větev
je	být	k5eAaImIp3nS	být
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
枝	枝	k?	枝
<g/>
。	。	k?	。
Eda	Eda	k1gMnSc1	Eda
ga	ga	k?	ga
aoi	aoi	k?	aoi
ka	ka	k?	ka
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
větev	větev	k1gFnSc1	větev
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Struktura	struktura	k1gFnSc1	struktura
japonské	japonský	k2eAgFnSc2d1	japonská
věty	věta	k1gFnSc2	věta
je	být	k5eAaImIp3nS	být
SOV	sova	k1gFnPc2	sova
(	(	kIx(	(
<g/>
podmět-předmět-sloveso	podmětředmětlovesa	k1gFnSc5	podmět-předmět-slovesa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Japonština	japonština	k1gFnSc1	japonština
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
gramatickou	gramatický	k2eAgFnSc4d1	gramatická
roli	role	k1gFnSc4	role
a	a	k8xC	a
pád	pád	k1gInSc4	pád
slova	slovo	k1gNnSc2	slovo
pomocí	pomocí	k7c2	pomocí
nesamostatných	samostatný	k2eNgFnPc2d1	nesamostatná
významových	významový	k2eAgFnPc2d1	významová
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
partikulí	partikule	k1gFnSc7	partikule
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
prvky	prvek	k1gInPc1	prvek
věty	věta	k1gFnSc2	věta
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vynechány	vynechat	k5eAaPmNgInP	vynechat
<g/>
,	,	kIx,	,
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
<g/>
-li	-li	k?	-li
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
<g/>
;	;	kIx,	;
a	a	k8xC	a
v	v	k7c6	v
praktické	praktický	k2eAgFnSc3d1	praktická
mluvě	mluva	k1gFnSc3	mluva
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
je	být	k5eAaImIp3nS	být
–	–	k?	–
důvtipná	důvtipný	k2eAgNnPc1d1	důvtipné
vynechání	vynechání	k1gNnPc1	vynechání
např.	např.	kA	např.
některých	některý	k3yIgNnPc2	některý
zájmen	zájmeno	k1gNnPc2	zájmeno
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
zdrojem	zdroj	k1gInSc7	zdroj
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
záměrných	záměrný	k2eAgInPc2d1	záměrný
<g/>
)	)	kIx)	)
dvojsmyslů	dvojsmysl	k1gInPc2	dvojsmysl
a	a	k8xC	a
narážek	narážka	k1gFnPc2	narážka
<g/>
.	.	kIx.	.
</s>
<s>
Japonština	japonština	k1gFnSc1	japonština
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
zdvořilost	zdvořilost	k1gFnSc4	zdvořilost
promluvy	promluva	k1gFnSc2	promluva
na	na	k7c6	na
gramatické	gramatický	k2eAgFnSc6d1	gramatická
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Zdvořilost	zdvořilost	k1gFnSc1	zdvořilost
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
různými	různý	k2eAgInPc7d1	různý
prostředky	prostředek	k1gInPc7	prostředek
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
obecnou	obecný	k2eAgFnSc4d1	obecná
volbu	volba	k1gFnSc4	volba
výrazů	výraz	k1gInPc2	výraz
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
s	s	k7c7	s
kým	kdo	k3yQnSc7	kdo
nebo	nebo	k8xC	nebo
o	o	k7c6	o
kom	kdo	k3yInSc6	kdo
mluvíme	mluvit	k5eAaImIp1nP	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Neomezuje	omezovat	k5eNaImIp3nS	omezovat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
obracíme	obracet	k5eAaImIp1nP	obracet
na	na	k7c4	na
respektovanou	respektovaný	k2eAgFnSc4d1	respektovaná
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgMnSc3	ten
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
vykání	vykání	k1gNnSc6	vykání
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenou	vzdálený	k2eAgFnSc7d1	vzdálená
analogií	analogie	k1gFnSc7	analogie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
vykání	vykání	k1gNnSc2	vykání
a	a	k8xC	a
tykání	tykání	k1gNnSc2	tykání
<g/>
,	,	kIx,	,
také	také	k9	také
rozlišování	rozlišování	k1gNnSc1	rozlišování
knižního	knižní	k2eAgInSc2d1	knižní
<g/>
,	,	kIx,	,
hovorového	hovorový	k2eAgInSc2d1	hovorový
a	a	k8xC	a
vulgárního	vulgární	k2eAgInSc2d1	vulgární
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvládnutí	zvládnutí	k1gNnSc1	zvládnutí
jazykové	jazykový	k2eAgFnSc2d1	jazyková
zdvořilosti	zdvořilost	k1gFnSc2	zdvořilost
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Japonce	Japonec	k1gMnPc4	Japonec
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
a	a	k8xC	a
Japonec	Japonec	k1gMnSc1	Japonec
jiného	jiný	k2eAgMnSc2d1	jiný
člověka	člověk	k1gMnSc2	člověk
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
kontaktu	kontakt	k1gInSc6	kontakt
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
právě	právě	k6eAd1	právě
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
jazykové	jazykový	k2eAgFnSc2d1	jazyková
zdvořilosti	zdvořilost	k1gFnSc2	zdvořilost
<g/>
.	.	kIx.	.
</s>
<s>
Rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
tyto	tento	k3xDgFnPc4	tento
úrovně	úroveň	k1gFnPc4	úroveň
zdvořilosti	zdvořilost	k1gFnSc2	zdvořilost
<g/>
:	:	kIx,	:
丁	丁	k?	丁
teineigo	teineigo	k6eAd1	teineigo
–	–	k?	–
zdvořilá	zdvořilý	k2eAgFnSc1d1	zdvořilá
řeč	řeč	k1gFnSc1	řeč
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
polite	polit	k1gInSc5	polit
language	language	k1gFnPc3	language
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
např.	např.	kA	např.
masu	masa	k1gFnSc4	masa
<g/>
,	,	kIx,	,
desu	desu	k5eAaPmIp1nS	desu
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
尊	尊	k?	尊
sonkeigo	sonkeigo	k6eAd1	sonkeigo
–	–	k?	–
uctivá	uctivý	k2eAgFnSc1d1	uctivá
řeč	řeč	k1gFnSc1	řeč
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
honorific	honorific	k1gMnSc1	honorific
language	language	k1gFnSc2	language
<g/>
)	)	kIx)	)
謙	謙	k?	謙
kendžógo	kendžógo	k6eAd1	kendžógo
–	–	k?	–
skromná	skromný	k2eAgFnSc1d1	skromná
řeč	řeč	k1gFnSc1	řeč
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
humble	humble	k6eAd1	humble
language	language	k1gFnSc2	language
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
např.	např.	kA	např.
itadaku	itadak	k1gInSc2	itadak
<g/>
)	)	kIx)	)
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
různých	různý	k2eAgFnPc2d1	různá
úrovní	úroveň	k1gFnPc2	úroveň
zdvořilosti	zdvořilost	k1gFnSc2	zdvořilost
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
úroveň	úroveň	k1gFnSc1	úroveň
důvěrná	důvěrný	k2eAgFnSc1d1	důvěrná
neboli	neboli	k8xC	neboli
prostá	prostý	k2eAgFnSc1d1	prostá
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
zdvořilostních	zdvořilostní	k2eAgInPc2d1	zdvořilostní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvěrná	důvěrný	k2eAgFnSc1d1	důvěrná
<g/>
,	,	kIx,	,
prostá	prostý	k2eAgFnSc1d1	prostá
nebo	nebo	k8xC	nebo
také	také	k9	také
"	"	kIx"	"
<g/>
dětská	dětský	k2eAgFnSc1d1	dětská
<g/>
"	"	kIx"	"
úroveň	úroveň	k1gFnSc1	úroveň
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC	a
vůči	vůči	k7c3	vůči
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
naučí	naučit	k5eAaPmIp3nS	naučit
zdvořilé	zdvořilý	k2eAgFnSc3d1	zdvořilá
úrovni	úroveň	k1gFnSc3	úroveň
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
mezi	mezi	k7c7	mezi
členy	člen	k1gMnPc7	člen
rodiny	rodina	k1gFnSc2	rodina
nebo	nebo	k8xC	nebo
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
užší	úzký	k2eAgFnSc2d2	užší
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
party	party	k1gFnSc1	party
kamarádů	kamarád	k1gMnPc2	kamarád
nebo	nebo	k8xC	nebo
úzkého	úzký	k2eAgInSc2d1	úzký
pracovního	pracovní	k2eAgInSc2d1	pracovní
kolektivu	kolektiv	k1gInSc2	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důvěrné	důvěrný	k2eAgFnSc6d1	důvěrná
úrovni	úroveň	k1gFnSc6	úroveň
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
psány	psán	k2eAgInPc1d1	psán
neosobní	osobní	k2eNgInPc1d1	neosobní
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
např.	např.	kA	např.
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
</s>
<s>
Cizinec	cizinec	k1gMnSc1	cizinec
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
nikdy	nikdy	k6eAd1	nikdy
nedostane	dostat	k5eNaPmIp3nS	dostat
do	do	k7c2	do
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
úrovni	úroveň	k1gFnSc6	úroveň
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
Japonec	Japonec	k1gMnSc1	Japonec
za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
okolností	okolnost	k1gFnPc2	okolnost
vůči	vůči	k7c3	vůči
cizinci	cizinec	k1gMnPc1	cizinec
tuto	tento	k3xDgFnSc4	tento
úroveň	úroveň	k1gFnSc4	úroveň
nepoužije	použít	k5eNaPmIp3nS	použít
<g/>
.	.	kIx.	.
</s>
<s>
Znalost	znalost	k1gFnSc1	znalost
důvěrného	důvěrný	k2eAgInSc2d1	důvěrný
stupně	stupeň	k1gInSc2	stupeň
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvoření	tvoření	k1gNnSc1	tvoření
zdvořilých	zdvořilý	k2eAgInPc2d1	zdvořilý
tvarů	tvar	k1gInPc2	tvar
přímo	přímo	k6eAd1	přímo
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
tvarů	tvar	k1gInPc2	tvar
důvěrných	důvěrný	k2eAgInPc2d1	důvěrný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
důvěrnou	důvěrný	k2eAgFnSc4d1	důvěrná
úroveň	úroveň	k1gFnSc4	úroveň
patří	patřit	k5eAaImIp3nS	patřit
mj.	mj.	kA	mj.
koncovka	koncovka	k1gFnSc1	koncovka
slovesného	slovesný	k2eAgInSc2d1	slovesný
záporu	zápor	k1gInSc2	zápor
-な	-な	k?	-な
-nai	-nai	k1gFnSc2	-na
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
nepůjdu	jít	k5eNaImIp1nS	jít
<g/>
"	"	kIx"	"
důvěrně	důvěrně	k6eAd1	důvěrně
行	行	k?	行
ikanai	ikana	k1gFnPc4	ikana
<g/>
,	,	kIx,	,
zdvořile	zdvořile	k6eAd1	zdvořile
行	行	k?	行
ikimasen	ikimasit	k5eAaPmNgMnS	ikimasit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
úroveň	úroveň	k1gFnSc1	úroveň
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
nejčastěji	často	k6eAd3	často
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
úroveň	úroveň	k1gFnSc1	úroveň
<g/>
,	,	kIx,	,
slušná	slušný	k2eAgFnSc1d1	slušná
za	za	k7c2	za
všech	všecek	k3xTgFnPc2	všecek
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
vůči	vůči	k7c3	vůči
dospělým	dospělí	k1gMnPc3	dospělí
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
částí	část	k1gFnSc7	část
stejné	stejný	k2eAgFnSc2d1	stejná
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
cizincům	cizinec	k1gMnPc3	cizinec
apod.	apod.	kA	apod.
Nevyjadřuje	vyjadřovat	k5eNaImIp3nS	vyjadřovat
důvěrnost	důvěrnost	k1gFnSc4	důvěrnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
společenskou	společenský	k2eAgFnSc4d1	společenská
nerovnost	nerovnost	k1gFnSc4	nerovnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zdvořilou	zdvořilý	k2eAgFnSc4d1	zdvořilá
úroveň	úroveň	k1gFnSc4	úroveň
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
tyto	tento	k3xDgFnPc1	tento
slovesné	slovesný	k2eAgFnPc1d1	slovesná
koncovky	koncovka	k1gFnPc1	koncovka
<g/>
:	:	kIx,	:
-ま	-ま	k?	-ま
-masu	-masa	k1gFnSc4	-masa
(	(	kIx(	(
<g/>
budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
kladný	kladný	k2eAgInSc4d1	kladný
<g/>
)	)	kIx)	)
-ま	-ま	k?	-ま
-masen	-masen	k1gInSc1	-masen
(	(	kIx(	(
<g/>
budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
záporný	záporný	k2eAgInSc4d1	záporný
<g/>
)	)	kIx)	)
-ま	-ま	k?	-ま
-mašita	-mašit	k2eAgMnSc4d1	-mašit
(	(	kIx(	(
<g/>
minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
kladný	kladný	k2eAgInSc4d1	kladný
<g/>
)	)	kIx)	)
-ま	-ま	k?	-ま
-masendešita	asendešit	k2eAgMnSc4d1	-masendešit
(	(	kIx(	(
<g/>
minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
záporný	záporný	k2eAgInSc4d1	záporný
<g/>
)	)	kIx)	)
Uctivá	uctivý	k2eAgFnSc1d1	uctivá
(	(	kIx(	(
<g/>
formální	formální	k2eAgFnSc1d1	formální
<g/>
,	,	kIx,	,
honorifická	honorifický	k2eAgFnSc1d1	honorifický
<g/>
)	)	kIx)	)
úroveň	úroveň	k1gFnSc1	úroveň
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
vůči	vůči	k7c3	vůči
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
společenském	společenský	k2eAgInSc6d1	společenský
žebříčku	žebříček	k1gInSc6	žebříček
výrazně	výrazně	k6eAd1	výrazně
výše	výše	k1gFnSc1	výše
než	než	k8xS	než
mluvčí	mluvčí	k1gFnSc1	mluvčí
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
nebo	nebo	k8xC	nebo
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Honorifické	Honorifický	k2eAgInPc1d1	Honorifický
tvary	tvar	k1gInPc1	tvar
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nS	mluvit
<g/>
-li	-li	k?	-li
například	například	k6eAd1	například
řadový	řadový	k2eAgMnSc1d1	řadový
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
společnosti	společnost	k1gFnSc2	společnost
nebo	nebo	k8xC	nebo
běžný	běžný	k2eAgMnSc1d1	běžný
Japonec	Japonec	k1gMnSc1	Japonec
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Uctivá	uctivý	k2eAgFnSc1d1	uctivá
mluva	mluva	k1gFnSc1	mluva
je	být	k5eAaImIp3nS	být
také	také	k9	také
používána	používat	k5eAaImNgFnS	používat
vůči	vůči	k7c3	vůči
výrazně	výrazně	k6eAd1	výrazně
starším	starý	k2eAgMnPc3d2	starší
<g/>
,	,	kIx,	,
významným	významný	k2eAgMnPc3d1	významný
umělcům	umělec	k1gMnPc3	umělec
a	a	k8xC	a
všeobecně	všeobecně	k6eAd1	všeobecně
úctyhodným	úctyhodný	k2eAgMnPc3d1	úctyhodný
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
úrovně	úroveň	k1gFnSc2	úroveň
komunikace	komunikace	k1gFnSc2	komunikace
spadá	spadat	k5eAaPmIp3nS	spadat
jak	jak	k6eAd1	jak
výše	vysoce	k6eAd2	vysoce
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
uctivá	uctivý	k2eAgFnSc1d1	uctivá
řeč	řeč	k1gFnSc1	řeč
(	(	kIx(	(
<g/>
sonkeigo	sonkeigo	k1gNnSc1	sonkeigo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
skromná	skromný	k2eAgFnSc1d1	skromná
řeč	řeč	k1gFnSc1	řeč
(	(	kIx(	(
<g/>
kendžógo	kendžógo	k1gNnSc1	kendžógo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
úctu	úcta	k1gFnSc4	úcta
ke	k	k7c3	k
společensky	společensky	k6eAd1	společensky
výše	vysoce	k6eAd2	vysoce
postavené	postavený	k2eAgFnSc3d1	postavená
osobě	osoba	k1gFnSc3	osoba
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
povýšit	povýšit	k5eAaPmF	povýšit
ji	on	k3xPp3gFnSc4	on
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
naopak	naopak	k6eAd1	naopak
snižuje	snižovat	k5eAaImIp3nS	snižovat
význam	význam	k1gInSc4	význam
mluvčího	mluvčí	k1gMnSc2	mluvčí
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
ponížit	ponížit	k5eAaPmF	ponížit
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
skromné	skromný	k2eAgFnSc2d1	skromná
řeči	řeč	k1gFnSc2	řeč
je	být	k5eAaImIp3nS	být
pomocné	pomocný	k2eAgNnSc4d1	pomocné
sloveso	sloveso	k1gNnSc4	sloveso
い	い	k?	い
itadaku	itadak	k1gInSc2	itadak
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gInSc7	jeho
neutrálním	neutrální	k2eAgInSc7d1	neutrální
protějškem	protějšek	k1gInSc7	protějšek
je	být	k5eAaImIp3nS	být
sloveso	sloveso	k1gNnSc1	sloveso
も	も	k?	も
morau	moraus	k1gInSc2	moraus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
空	空	k?	空
<g/>
。	。	k?	。
kúkó	kúkó	k?	kúkó
made	madat	k5eAaPmIp3nS	madat
itte	itte	k1gFnSc1	itte
moraemasu	moraemas	k1gInSc2	moraemas
ka	ka	k?	ka
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Zavezl	zavézt	k5eAaPmAgMnS	zavézt
byste	by	kYmCp2nP	by
mne	já	k3xPp1nSc4	já
až	až	k9	až
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Neutrální	neutrální	k2eAgFnSc1d1	neutrální
zdvořilá	zdvořilý	k2eAgFnSc1d1	zdvořilá
úroveň	úroveň	k1gFnSc1	úroveň
<g/>
.	.	kIx.	.
行	行	k?	行
itte	itte	k1gInSc1	itte
je	být	k5eAaImIp3nS	být
přechodník	přechodník	k1gInSc4	přechodník
slovesa	sloveso	k1gNnSc2	sloveso
行	行	k?	行
iku	iku	k?	iku
"	"	kIx"	"
<g/>
jít	jít	k5eAaImF	jít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
も	も	k?	も
moraemasu	moraemas	k1gInSc2	moraemas
je	být	k5eAaImIp3nS	být
budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
slovesa	sloveso	k1gNnSc2	sloveso
も	も	k?	も
morau	morau	k5eAaPmIp1nS	morau
"	"	kIx"	"
<g/>
dostat	dostat	k5eAaPmF	dostat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnSc1	kombinace
těchto	tento	k3xDgInPc2	tento
slovesných	slovesný	k2eAgInPc2d1	slovesný
tvarů	tvar	k1gInPc2	tvar
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
dostat	dostat	k5eAaPmF	dostat
jízdu	jízda	k1gFnSc4	jízda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
zavezen	zavezen	k2eAgMnSc1d1	zavezen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
友	友	k?	友
<g/>
。	。	k?	。
tomodači	tomodač	k1gInPc7	tomodač
no	no	k9	no
otósan	otósan	k1gInSc1	otósan
ni	on	k3xPp3gFnSc4	on
kúkó	kúkó	k?	kúkó
made	madat	k5eAaPmIp3nS	madat
itte	itte	k6eAd1	itte
itadakimašita	itadakimašit	k2eAgFnSc1d1	itadakimašit
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Přítelův	přítelův	k2eAgMnSc1d1	přítelův
otec	otec	k1gMnSc1	otec
mne	já	k3xPp1nSc4	já
zavezl	zavézt	k5eAaPmAgMnS	zavézt
až	až	k6eAd1	až
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Skromná	skromný	k2eAgFnSc1d1	skromná
úroveň	úroveň	k1gFnSc1	úroveň
<g/>
.	.	kIx.	.
い	い	k?	い
itadakimašita	itadakimašit	k2eAgFnSc1d1	itadakimašit
je	být	k5eAaImIp3nS	být
minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
skromného	skromný	k2eAgNnSc2d1	skromné
slovesa	sloveso	k1gNnSc2	sloveso
い	い	k?	い
itadaku	itadak	k1gInSc2	itadak
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
poníženě	poníženě	k6eAd1	poníženě
<g/>
,	,	kIx,	,
s	s	k7c7	s
povděkem	povděk	k1gInSc7	povděk
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
odvezení	odvezení	k1gNnSc2	odvezení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
věta	věta	k1gFnSc1	věta
tak	tak	k9	tak
dostává	dostávat	k5eAaImIp3nS	dostávat
nádech	nádech	k1gInSc1	nádech
"	"	kIx"	"
<g/>
Přítelův	přítelův	k2eAgMnSc1d1	přítelův
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
tak	tak	k6eAd1	tak
velice	velice	k6eAd1	velice
laskav	laskav	k2eAgMnSc1d1	laskav
<g/>
,	,	kIx,	,
že	že	k8xS	že
mne	já	k3xPp1nSc4	já
zavezl	zavézt	k5eAaPmAgInS	zavézt
až	až	k9	až
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Uctivé	uctivý	k2eAgInPc1d1	uctivý
výrazy	výraz	k1gInPc1	výraz
se	se	k3xPyFc4	se
často	často	k6eAd1	často
poznají	poznat	k5eAaPmIp3nP	poznat
podle	podle	k7c2	podle
honorifické	honorifický	k2eAgFnSc2d1	honorifický
předpony	předpona	k1gFnSc2	předpona
お	お	k?	お
o-	o-	k?	o-
<g/>
,	,	kIx,	,
ご	ご	k?	ご
go-	go-	k?	go-
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
jak	jak	k6eAd1	jak
před	před	k7c7	před
slovesy	sloveso	k1gNnPc7	sloveso
<g/>
,	,	kIx,	,
tak	tak	k9	tak
před	před	k7c7	před
podstatnými	podstatný	k2eAgNnPc7d1	podstatné
a	a	k8xC	a
přídavnými	přídavný	k2eAgNnPc7d1	přídavné
jmény	jméno	k1gNnPc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Slovesa	sloveso	k1gNnPc1	sloveso
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
opět	opět	k6eAd1	opět
mění	měnit	k5eAaImIp3nS	měnit
i	i	k9	i
koncovku	koncovka	k1gFnSc4	koncovka
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
sloveso	sloveso	k1gNnSc1	sloveso
読	読	k?	読
jomu	jomus	k1gInSc2	jomus
"	"	kIx"	"
<g/>
číst	číst	k5eAaImF	číst
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
uctivý	uctivý	k2eAgInSc4d1	uctivý
tvar	tvar	k1gInSc4	tvar
お	お	k?	お
ojomi	ojo	k1gFnPc7	ojo
ni	on	k3xPp3gFnSc4	on
naru	nara	k1gFnSc4	nara
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
skromné	skromný	k2eAgInPc1d1	skromný
tvary	tvar	k1gInPc1	tvar
jsou	být	k5eAaImIp3nP	být
お	お	k?	お
ojomi	ojo	k1gFnPc7	ojo
itasu	itas	k1gInSc2	itas
nebo	nebo	k8xC	nebo
お	お	k?	お
ojomi	ojo	k1gFnPc7	ojo
suru	surus	k1gInSc2	surus
<g/>
.	.	kIx.	.
</s>
<s>
Uctivé	uctivý	k2eAgInPc1d1	uctivý
tvary	tvar	k1gInPc1	tvar
také	také	k6eAd1	také
pronikly	proniknout	k5eAaPmAgInP	proniknout
do	do	k7c2	do
ostatních	ostatní	k2eAgFnPc2d1	ostatní
úrovní	úroveň	k1gFnPc2	úroveň
komunikace	komunikace	k1gFnSc2	komunikace
jako	jako	k8xS	jako
součásti	součást	k1gFnSc2	součást
zdvořilostních	zdvořilostní	k2eAgFnPc2d1	zdvořilostní
frází	fráze	k1gFnPc2	fráze
nebo	nebo	k8xC	nebo
výrazů	výraz	k1gInPc2	výraz
pro	pro	k7c4	pro
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgMnPc3	který
mají	mít	k5eAaImIp3nP	mít
Japonci	Japonec	k1gMnPc1	Japonec
úctu	úcta	k1gFnSc4	úcta
<g/>
:	:	kIx,	:
gohan	gohan	k1gInSc4	gohan
(	(	kIx(	(
<g/>
御	御	k?	御
<g/>
)	)	kIx)	)
–	–	k?	–
vařená	vařený	k2eAgFnSc1d1	vařená
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
okane	okanout	k5eAaPmIp3nS	okanout
(	(	kIx(	(
<g/>
お	お	k?	お
<g/>
)	)	kIx)	)
–	–	k?	–
peníze	peníz	k1gInPc1	peníz
<g/>
,	,	kIx,	,
arigató	arigató	k?	arigató
gozaimasu	gozaimasa	k1gFnSc4	gozaimasa
あ	あ	k?	あ
–	–	k?	–
děkuji	děkovat	k5eAaImIp1nS	děkovat
mnohokrát	mnohokrát	k6eAd1	mnohokrát
<g/>
,	,	kIx,	,
ohajó	ohajó	k?	ohajó
gozaimasu	gozaimasa	k1gFnSc4	gozaimasa
お	お	k?	お
–	–	k?	–
dobré	dobrý	k2eAgNnSc4d1	dobré
jitro	jitro	k1gNnSc4	jitro
(	(	kIx(	(
<g/>
tučně	tučně	k6eAd1	tučně
jsou	být	k5eAaImIp3nP	být
vyznačeny	vyznačen	k2eAgInPc4d1	vyznačen
honorifické	honorifický	k2eAgInPc4d1	honorifický
prefixy	prefix	k1gInPc4	prefix
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
japonština	japonština	k1gFnSc1	japonština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
japonština	japonština	k1gFnSc1	japonština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Online	Onlin	k1gInSc5	Onlin
japonsko-anglický	japonskonglický	k2eAgInSc4d1	japonsko-anglický
slovník	slovník	k1gInSc4	slovník
Kim	Kim	k1gMnSc1	Kim
Allen	Allen	k1gMnSc1	Allen
<g/>
:	:	kIx,	:
<g/>
Politeness	Politeness	k1gInSc1	Politeness
Levels	Levels	k1gInSc1	Levels
–	–	k?	–
O	o	k7c6	o
zdvořilostních	zdvořilostní	k2eAgFnPc6d1	zdvořilostní
úrovních	úroveň	k1gFnPc6	úroveň
Logical	Logical	k1gFnSc2	Logical
Japanese	Japanese	k1gFnSc2	Japanese
Grammar	Grammara	k1gFnPc2	Grammara
–	–	k?	–
Ohýbání	ohýbání	k1gNnSc4	ohýbání
sloves	sloveso	k1gNnPc2	sloveso
Japanese	Japanese	k1gFnSc2	Japanese
for	forum	k1gNnPc2	forum
western	western	k1gInSc4	western
brain	brain	k1gInSc1	brain
-	-	kIx~	-
Zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
na	na	k7c4	na
anglicky	anglicky	k6eAd1	anglicky
mluvící	mluvící	k2eAgNnSc4d1	mluvící
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
mnoho	mnoho	k4c1	mnoho
užitečných	užitečný	k2eAgFnPc2d1	užitečná
informací	informace	k1gFnPc2	informace
i	i	k9	i
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgNnSc4d1	ostatní
AKSER	AKSER	kA	AKSER
Brasil	Brasil	k1gFnSc2	Brasil
-	-	kIx~	-
Japanese	Japanese	k1gFnSc1	Japanese
Video	video	k1gNnSc1	video
Course	Course	k1gFnSc2	Course
Základy	základ	k1gInPc1	základ
japonštiny	japonština	k1gFnSc2	japonština
pro	pro	k7c4	pro
zájemce	zájemce	k1gMnPc4	zájemce
o	o	k7c6	o
anime	animat	k5eAaPmIp3nS	animat
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
lidi	člověk	k1gMnPc4	člověk
Česko-japonský	českoaponský	k2eAgInSc1d1	česko-japonský
online	onlinout	k5eAaPmIp3nS	onlinout
slovník	slovník	k1gInSc1	slovník
Analýza	analýza	k1gFnSc1	analýza
nejnovějších	nový	k2eAgFnPc2d3	nejnovější
tendencí	tendence	k1gFnPc2	tendence
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
zdvořilostní	zdvořilostní	k2eAgFnSc2d1	zdvořilostní
japonštiny	japonština	k1gFnSc2	japonština
Japonština	japonština	k1gFnSc1	japonština
AKAMACU	AKAMACU	kA	AKAMACU
<g/>
,	,	kIx,	,
Cutomu	Cutom	k1gInSc2	Cutom
<g/>
.	.	kIx.	.
</s>
<s>
Japanese	Japanést	k5eAaPmIp3nS	Japanést
phonology	phonolog	k1gMnPc4	phonolog
<g/>
:	:	kIx,	:
A	a	k9	a
functional	functionat	k5eAaPmAgInS	functionat
approach	approach	k1gInSc1	approach
<g/>
.	.	kIx.	.
</s>
<s>
Lincom	Lincom	k1gInSc1	Lincom
Europa	Europa	k1gFnSc1	Europa
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
89586	[number]	k4	89586
<g/>
-	-	kIx~	-
<g/>
544	[number]	k4	544
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
KROUSKÝ	KROUSKÝ	kA	KROUSKÝ
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Učebnice	učebnice	k1gFnSc1	učebnice
japonštiny	japonština	k1gFnSc2	japonština
<g/>
.	.	kIx.	.
229	[number]	k4	229
pp	pp	k?	pp
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
H	H	kA	H
&	&	k?	&
H	H	kA	H
Vyšehradská	vyšehradský	k2eAgFnSc1d1	Vyšehradská
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-86022-98-6	[number]	k4	80-86022-98-6
LABRUNE	LABRUNE	kA	LABRUNE
<g/>
,	,	kIx,	,
Laurence	Laurence	k1gFnSc1	Laurence
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Phonology	Phonolog	k1gMnPc4	Phonolog
of	of	k?	of
Japanese	Japanese	k1gFnSc1	Japanese
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
<g/>
:	:	kIx,	:
9780199545834	[number]	k4	9780199545834
<g/>
.	.	kIx.	.
</s>
<s>
ŠIBATANI	ŠIBATANI	kA	ŠIBATANI
<g/>
,	,	kIx,	,
Masajoši	Masajoš	k1gMnPc1	Masajoš
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Languages	Languages	k1gMnSc1	Languages
of	of	k?	of
Japan	japan	k1gInSc1	japan
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-521-36070-6	[number]	k4	0-521-36070-6
</s>
