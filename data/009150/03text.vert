<p>
<s>
Svařovací	svařovací	k2eAgInSc4d1	svařovací
zdroj	zdroj	k1gInSc4	zdroj
nebo	nebo	k8xC	nebo
svařovací	svařovací	k2eAgInSc4d1	svařovací
agregát	agregát	k1gInSc4	agregát
(	(	kIx(	(
<g/>
slangově	slangově	k6eAd1	slangově
svářečka	svářečka	k1gFnSc1	svářečka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
elektrické	elektrický	k2eAgNnSc4d1	elektrické
zařízení	zařízení	k1gNnSc4	zařízení
používané	používaný	k2eAgNnSc4d1	používané
pro	pro	k7c4	pro
generování	generování	k1gNnSc4	generování
svařovacího	svařovací	k2eAgInSc2d1	svařovací
proudu	proud	k1gInSc2	proud
při	při	k7c6	při
svařování	svařování	k1gNnSc6	svařování
metodami	metoda	k1gFnPc7	metoda
obloukového	obloukový	k2eAgNnSc2d1	obloukové
svařování	svařování	k1gNnSc2	svařování
nebo	nebo	k8xC	nebo
při	při	k7c6	při
odporovém	odporový	k2eAgNnSc6d1	odporové
svařování	svařování	k1gNnSc6	svařování
<g/>
.	.	kIx.	.
</s>
<s>
Svařovací	svařovací	k2eAgInSc1d1	svařovací
zdroj	zdroj	k1gInSc1	zdroj
musí	muset	k5eAaImIp3nS	muset
splnit	splnit	k5eAaPmF	splnit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
požadavků	požadavek	k1gInPc2	požadavek
vyžadovaných	vyžadovaný	k2eAgInPc2d1	vyžadovaný
pro	pro	k7c4	pro
bezproblémové	bezproblémový	k2eAgNnSc4d1	bezproblémové
svařování	svařování	k1gNnSc4	svařování
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
regulace	regulace	k1gFnSc1	regulace
elektrického	elektrický	k2eAgNnSc2d1	elektrické
napětí	napětí	k1gNnSc2	napětí
a	a	k8xC	a
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
odolnost	odolnost	k1gFnSc4	odolnost
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
krátkodobých	krátkodobý	k2eAgInPc2d1	krátkodobý
zkratů	zkrat	k1gInPc2	zkrat
<g/>
,	,	kIx,	,
zapálení	zapálení	k1gNnSc4	zapálení
a	a	k8xC	a
stabilní	stabilní	k2eAgNnSc4d1	stabilní
hoření	hoření	k1gNnSc4	hoření
elektrického	elektrický	k2eAgInSc2d1	elektrický
oblouku	oblouk	k1gInSc2	oblouk
<g/>
,	,	kIx,	,
stálost	stálost	k1gFnSc4	stálost
výkonu	výkon	k1gInSc2	výkon
a	a	k8xC	a
dostatečná	dostatečný	k2eAgFnSc1d1	dostatečná
účinnost	účinnost	k1gFnSc1	účinnost
i	i	k8xC	i
zatěžovatel	zatěžovatel	k1gMnSc1	zatěžovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svařovací	svařovací	k2eAgInPc1d1	svařovací
zdroje	zdroj	k1gInPc1	zdroj
mohou	moct	k5eAaImIp3nP	moct
dodávat	dodávat	k5eAaImF	dodávat
stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
proud	proud	k1gInSc1	proud
(	(	kIx(	(
<g/>
svařovací	svařovací	k2eAgNnPc1d1	svařovací
dynama	dynamo	k1gNnPc1	dynamo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
usměrněný	usměrněný	k2eAgInSc1d1	usměrněný
proud	proud	k1gInSc1	proud
(	(	kIx(	(
<g/>
svařovací	svařovací	k2eAgInPc1d1	svařovací
usměrňovače	usměrňovač	k1gInPc1	usměrňovač
a	a	k8xC	a
invertory	invertor	k1gInPc1	invertor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střídavý	střídavý	k2eAgInSc4d1	střídavý
proud	proud	k1gInSc4	proud
(	(	kIx(	(
<g/>
svařovací	svařovací	k2eAgInPc4d1	svařovací
transformátory	transformátor	k1gInPc4	transformátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristiky	charakteristika	k1gFnPc4	charakteristika
svařovacího	svařovací	k2eAgInSc2d1	svařovací
zdroje	zdroj	k1gInSc2	zdroj
==	==	k?	==
</s>
</p>
<p>
<s>
Dostupné	dostupný	k2eAgInPc1d1	dostupný
svařovací	svařovací	k2eAgInPc1d1	svařovací
zdroje	zdroj	k1gInPc1	zdroj
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
generování	generování	k1gNnSc4	generování
svařovacího	svařovací	k2eAgInSc2d1	svařovací
proudu	proud	k1gInSc2	proud
v	v	k7c6	v
hodnotách	hodnota	k1gFnPc6	hodnota
od	od	k7c2	od
0,05	[number]	k4	0,05
až	až	k9	až
20	[number]	k4	20
A	A	kA	A
pro	pro	k7c4	pro
svařování	svařování	k1gNnSc4	svařování
mikroplasmové	mikroplasmová	k1gFnSc2	mikroplasmová
<g/>
,	,	kIx,	,
od	od	k7c2	od
30	[number]	k4	30
do	do	k7c2	do
1	[number]	k4	1
000	[number]	k4	000
A	A	kA	A
pro	pro	k7c4	pro
svařování	svařování	k1gNnSc1	svařování
obloukovými	obloukový	k2eAgFnPc7d1	oblouková
metodami	metoda	k1gFnPc7	metoda
a	a	k8xC	a
hodnoty	hodnota	k1gFnPc1	hodnota
v	v	k7c6	v
desítkách	desítka	k1gFnPc6	desítka
až	až	k8xS	až
stovkách	stovka	k1gFnPc6	stovka
kA	kA	k?	kA
pro	pro	k7c4	pro
odporové	odporový	k2eAgNnSc4d1	odporové
svařování	svařování	k1gNnSc4	svařování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zatěžovatel	zatěžovatel	k1gMnSc1	zatěžovatel
svařovacího	svařovací	k2eAgInSc2d1	svařovací
zdroje	zdroj	k1gInSc2	zdroj
===	===	k?	===
</s>
</p>
<p>
<s>
Zatěžovatel	zatěžovatel	k1gMnSc1	zatěžovatel
(	(	kIx(	(
<g/>
DZ	DZ	kA	DZ
<g/>
)	)	kIx)	)
svařovacího	svařovací	k2eAgInSc2d1	svařovací
zdroje	zdroj	k1gInSc2	zdroj
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
parametrů	parametr	k1gInPc2	parametr
svařovacího	svařovací	k2eAgInSc2d1	svařovací
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
poměr	poměr	k1gInSc1	poměr
maximální	maximální	k2eAgFnSc2d1	maximální
možné	možný	k2eAgFnSc2d1	možná
doby	doba	k1gFnSc2	doba
svařování	svařování	k1gNnSc1	svařování
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
sv	sv	kA	sv
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
k	k	k7c3	k
celkovému	celkový	k2eAgInSc3d1	celkový
pracovnímu	pracovní	k2eAgInSc3d1	pracovní
času	čas	k1gInSc3	čas
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
sm	sm	k?	sm
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
100	[number]	k4	100
</s>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
<s>
%	%	kIx~	%
</s>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
DZ	DZ	kA	DZ
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
sv	sv	kA	sv
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
sm	sm	k?	sm
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
100	[number]	k4	100
<g/>
[	[	kIx(	[
<g/>
\	\	kIx~	\
<g/>
%	%	kIx~	%
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
PříkladPokud	PříkladPokud	k6eAd1	PříkladPokud
při	při	k7c6	při
celkové	celkový	k2eAgFnSc6d1	celková
době	doba	k1gFnSc6	doba
práce	práce	k1gFnSc2	práce
se	s	k7c7	s
svářečkou	svářečka	k1gFnSc7	svářečka
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
efektivně	efektivně	k6eAd1	efektivně
používat	používat	k5eAaImF	používat
svářečku	svářečka	k1gFnSc4	svářečka
4	[number]	k4	4
minuty	minuta	k1gFnSc2	minuta
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
zatěžovatel	zatěžovatel	k1gMnSc1	zatěžovatel
svařovacího	svařovací	k2eAgInSc2d1	svařovací
zdroje	zdroj	k1gInSc2	zdroj
lze	lze	k6eAd1	lze
vypočítat	vypočítat	k5eAaPmF	vypočítat
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
100	[number]	k4	100
</s>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
<s>
%	%	kIx~	%
</s>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
100	[number]	k4	100
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
40	[number]	k4	40
</s>
</p>
<p>
<s>
%	%	kIx~	%
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
DZ	DZ	kA	DZ
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
sv	sv	kA	sv
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
sm	sm	k?	sm
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
100	[number]	k4	100
<g/>
[	[	kIx(	[
<g/>
\	\	kIx~	\
<g/>
%	%	kIx~	%
<g/>
]	]	kIx)	]
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
10	[number]	k4	10
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
100	[number]	k4	100
<g/>
=	=	kIx~	=
<g/>
40	[number]	k4	40
<g/>
\	\	kIx~	\
<g/>
%	%	kIx~	%
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Při	při	k7c6	při
ručním	ruční	k2eAgNnSc6d1	ruční
svařování	svařování	k1gNnSc6	svařování
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
DZ	DZ	kA	DZ
být	být	k5eAaImF	být
minimálně	minimálně	k6eAd1	minimálně
40	[number]	k4	40
%	%	kIx~	%
u	u	k7c2	u
automatických	automatický	k2eAgFnPc2d1	automatická
metod	metoda	k1gFnPc2	metoda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
svařování	svařování	k1gNnSc1	svařování
pod	pod	k7c7	pod
tavidlem	tavidlo	k1gNnSc7	tavidlo
<g/>
)	)	kIx)	)
100	[number]	k4	100
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stabilita	stabilita	k1gFnSc1	stabilita
oblouku	oblouk	k1gInSc2	oblouk
===	===	k?	===
</s>
</p>
<p>
<s>
Stabilita	stabilita	k1gFnSc1	stabilita
oblouku	oblouk	k1gInSc2	oblouk
má	mít	k5eAaImIp3nS	mít
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
svařovaného	svařovaný	k2eAgInSc2d1	svařovaný
spoje	spoj	k1gInSc2	spoj
a	a	k8xC	a
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
rozdíl	rozdíl	k1gInSc1	rozdíl
gradientu	gradient	k1gInSc2	gradient
zatěžovací	zatěžovací	k2eAgFnSc2d1	zatěžovací
charakteristiky	charakteristika	k1gFnSc2	charakteristika
zdroje	zdroj	k1gInSc2	zdroj
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
a	a	k8xC	a
voltampérové	voltampérový	k2eAgFnSc2d1	voltampérová
charakteristiky	charakteristika	k1gFnSc2	charakteristika
oblouku	oblouk	k1gInSc2	oblouk
v	v	k7c6	v
pracovní	pracovní	k2eAgFnSc6d1	pracovní
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
resp.	resp.	kA	resp.
(	(	kIx(	(
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
>	>	kIx)	>
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
>	>	kIx)	>
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
U_	U_	k1gFnSc1	U_
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
I	i	k9	i
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
U_	U_	k1gFnSc1	U_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
I	i	k9	i
<g/>
}}	}}	k?	}}
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
\	\	kIx~	\
resp.	resp.	kA	resp.
<g/>
\	\	kIx~	\
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
U_	U_	k1gFnSc1	U_
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
I	i	k9	i
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
U_	U_	k1gFnSc1	U_
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
I	i	k9	i
<g/>
}}	}}	k?	}}
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Statická	statický	k2eAgFnSc1d1	statická
charakteristika	charakteristika	k1gFnSc1	charakteristika
===	===	k?	===
</s>
</p>
<p>
<s>
Statickou	statický	k2eAgFnSc4d1	statická
charakteristiku	charakteristika	k1gFnSc4	charakteristika
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
voltampérovou	voltampérový	k2eAgFnSc4d1	voltampérová
charakteristiku	charakteristika	k1gFnSc4	charakteristika
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
závislostí	závislost	k1gFnSc7	závislost
mezi	mezi	k7c7	mezi
pracovním	pracovní	k2eAgNnSc7d1	pracovní
napětím	napětí	k1gNnSc7	napětí
a	a	k8xC	a
svařovacím	svařovací	k2eAgInSc7d1	svařovací
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
<g/>
Zdroje	zdroj	k1gInPc1	zdroj
se	s	k7c7	s
strmou	strmý	k2eAgFnSc7d1	strmá
charakteristikou	charakteristika	k1gFnSc7	charakteristika
regulují	regulovat	k5eAaImIp3nP	regulovat
hodnotu	hodnota	k1gFnSc4	hodnota
napětí	napětí	k1gNnSc2	napětí
v	v	k7c6	v
elektrickém	elektrický	k2eAgInSc6d1	elektrický
obvodu	obvod	k1gInSc6	obvod
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
odporu	odpor	k1gInSc6	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Tzn.	tzn.	kA	tzn.
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
odpor	odpor	k1gInSc1	odpor
<g/>
,	,	kIx,	,
elektrický	elektrický	k2eAgInSc1d1	elektrický
oblouk	oblouk	k1gInSc1	oblouk
se	se	k3xPyFc4	se
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
napětí	napětí	k1gNnSc4	napětí
při	při	k7c6	při
konstantní	konstantní	k2eAgFnSc6d1	konstantní
hodnotě	hodnota	k1gFnSc6	hodnota
svařovacího	svařovací	k2eAgInSc2d1	svařovací
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
charakteristika	charakteristika	k1gFnSc1	charakteristika
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaImNgFnS	využívat
pro	pro	k7c4	pro
svařování	svařování	k1gNnSc4	svařování
metodami	metoda	k1gFnPc7	metoda
ruční	ruční	k2eAgNnSc4d1	ruční
obloukové	obloukový	k2eAgNnSc4d1	obloukové
svařování	svařování	k1gNnSc4	svařování
obalenou	obalený	k2eAgFnSc7d1	obalená
elektrodou	elektroda	k1gFnSc7	elektroda
nebo	nebo	k8xC	nebo
svařování	svařování	k1gNnSc4	svařování
netavící	tavící	k2eNgFnSc2d1	netavící
se	se	k3xPyFc4	se
elektrodou	elektroda	k1gFnSc7	elektroda
v	v	k7c6	v
inertním	inertní	k2eAgInSc6d1	inertní
plynu	plyn	k1gInSc6	plyn
<g/>
.	.	kIx.	.
<g/>
Zdroje	zdroj	k1gInPc1	zdroj
s	s	k7c7	s
plochou	plochý	k2eAgFnSc7d1	plochá
charakteristikou	charakteristika	k1gFnSc7	charakteristika
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
odporu	odpor	k1gInSc2	odpor
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
prodloužení	prodloužení	k1gNnSc1	prodloužení
elektrického	elektrický	k2eAgInSc2d1	elektrický
oblouku	oblouk	k1gInSc2	oblouk
<g/>
,	,	kIx,	,
zvýšením	zvýšení	k1gNnSc7	zvýšení
svařovacího	svařovací	k2eAgInSc2d1	svařovací
proudu	proud	k1gInSc2	proud
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
konstantního	konstantní	k2eAgNnSc2d1	konstantní
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
při	při	k7c6	při
svařování	svařování	k1gNnSc6	svařování
svařování	svařování	k1gNnSc2	svařování
s	s	k7c7	s
tavící	tavící	k2eAgFnSc7d1	tavící
se	se	k3xPyFc4	se
elektrodou	elektroda	k1gFnSc7	elektroda
jak	jak	k8xS	jak
v	v	k7c6	v
ochranném	ochranný	k2eAgInSc6d1	ochranný
plynu	plyn	k1gInSc6	plyn
tak	tak	k6eAd1	tak
i	i	k9	i
při	při	k7c6	při
svařování	svařování	k1gNnSc6	svařování
pod	pod	k7c7	pod
tavidlem	tavidlo	k1gNnSc7	tavidlo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zdroj	zdroj	k1gInSc1	zdroj
umožní	umožnit	k5eAaPmIp3nS	umožnit
zachovávat	zachovávat	k5eAaImF	zachovávat
konstantní	konstantní	k2eAgFnSc4d1	konstantní
délku	délka	k1gFnSc4	délka
oblouku	oblouk	k1gInSc2	oblouk
–	–	k?	–
tedy	tedy	k9	tedy
napětí	napětí	k1gNnSc1	napětí
–	–	k?	–
regulací	regulace	k1gFnSc7	regulace
rychlosti	rychlost	k1gFnSc2	rychlost
podávání	podávání	k1gNnSc2	podávání
se	se	k3xPyFc4	se
svařovacího	svařovací	k2eAgInSc2d1	svařovací
drátu	drát	k1gInSc2	drát
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
odtavování	odtavování	k1gNnSc2	odtavování
svařovacího	svařovací	k2eAgNnSc2d1	svařovací
drátuje	drátovat	k5eAaImIp3nS	drátovat
úměrná	úměrný	k2eAgFnSc1d1	úměrná
velikosti	velikost	k1gFnSc3	velikost
svařovacího	svařovací	k2eAgInSc2d1	svařovací
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
zdrojů	zdroj	k1gInPc2	zdroj
s	s	k7c7	s
polostrmou	polostrmý	k2eAgFnSc7d1	polostrmý
charakteristikou	charakteristika	k1gFnSc7	charakteristika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
také	také	k9	také
nazývají	nazývat	k5eAaImIp3nP	nazývat
zdroje	zdroj	k1gInPc1	zdroj
s	s	k7c7	s
konstantním	konstantní	k2eAgInSc7d1	konstantní
výkonem	výkon	k1gInSc7	výkon
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
takovou	takový	k3xDgFnSc4	takový
charakteristiku	charakteristika	k1gFnSc4	charakteristika
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
konstantním	konstantní	k2eAgInSc7d1	konstantní
výkonem	výkon	k1gInSc7	výkon
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
součinem	součin	k1gInSc7	součin
napětí	napětí	k1gNnSc2	napětí
a	a	k8xC	a
proudu	proud	k1gInSc2	proud
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
konst	konst	k5eAaPmF	konst
<g/>
.	.	kIx.	.
<g/>
=	=	kIx~	=
<g/>
U.I	U.I	k1gFnSc1	U.I
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
Zdroje	zdroj	k1gInPc1	zdroj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
disponují	disponovat	k5eAaBmIp3nP	disponovat
regulací	regulace	k1gFnSc7	regulace
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
nastavení	nastavení	k1gNnPc1	nastavení
svařovacího	svařovací	k2eAgNnSc2d1	svařovací
normalizovaného	normalizovaný	k2eAgNnSc2d1	normalizované
napětí	napětí	k1gNnSc2	napětí
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pracovní	pracovní	k2eAgFnPc4d1	pracovní
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
svařovacím	svařovací	k2eAgInSc6d1	svařovací
proudu	proud	k1gInSc6	proud
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
I_	I_	k1gFnSc6	I_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
tj.	tj.	kA	tj.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
20	[number]	k4	20
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
04	[number]	k4	04
</s>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
20	[number]	k4	20
<g/>
+	+	kIx~	+
<g/>
0,04	[number]	k4	0,04
<g/>
I_	I_	k1gFnPc2	I_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Dynamická	dynamický	k2eAgFnSc1d1	dynamická
charakteristika	charakteristika	k1gFnSc1	charakteristika
===	===	k?	===
</s>
</p>
<p>
<s>
Dynamická	dynamický	k2eAgFnSc1d1	dynamická
charakteristika	charakteristika	k1gFnSc1	charakteristika
zdroje	zdroj	k1gInSc2	zdroj
popisuje	popisovat	k5eAaImIp3nS	popisovat
schopnost	schopnost	k1gFnSc1	schopnost
zdroje	zdroj	k1gInPc4	zdroj
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
s	s	k7c7	s
náhlými	náhlý	k2eAgFnPc7d1	náhlá
krátkodobými	krátkodobý	k2eAgFnPc7d1	krátkodobá
změnami	změna	k1gFnPc7	změna
napětí	napětí	k1gNnSc2	napětí
při	při	k7c6	při
zapalování	zapalování	k1gNnSc6	zapalování
oblouku	oblouk	k1gInSc2	oblouk
<g/>
,	,	kIx,	,
při	při	k7c6	při
zkratu	zkrat	k1gInSc2	zkrat
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Synergický	synergický	k2eAgInSc1d1	synergický
zdroj	zdroj	k1gInSc1	zdroj
===	===	k?	===
</s>
</p>
<p>
<s>
Synergický	synergický	k2eAgInSc1d1	synergický
zdroj	zdroj	k1gInSc1	zdroj
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
synergický	synergický	k2eAgInSc1d1	synergický
režim	režim	k1gInSc1	režim
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
takový	takový	k3xDgInSc1	takový
svařovací	svařovací	k2eAgInSc1d1	svařovací
zdroj	zdroj	k1gInSc1	zdroj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
svařovací	svařovací	k2eAgInPc1d1	svařovací
parametry	parametr	k1gInPc1	parametr
jsou	být	k5eAaImIp3nP	být
natolik	natolik	k6eAd1	natolik
optimalizovány	optimalizován	k2eAgFnPc1d1	optimalizována
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
změně	změna	k1gFnSc6	změna
jednoho	jeden	k4xCgInSc2	jeden
parametru	parametr	k1gInSc2	parametr
–	–	k?	–
většinou	většina	k1gFnSc7	většina
rychlosti	rychlost	k1gFnSc2	rychlost
podávání	podávání	k1gNnSc2	podávání
svařovacího	svařovací	k2eAgInSc2d1	svařovací
drátu	drát	k1gInSc2	drát
–	–	k?	–
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
automatickému	automatický	k2eAgNnSc3d1	automatické
přenastavení	přenastavení	k1gNnSc3	přenastavení
dalších	další	k2eAgInPc2d1	další
závislých	závislý	k2eAgInPc2d1	závislý
parametrů	parametr	k1gInPc2	parametr
<g/>
,	,	kIx,	,
např.	např.	kA	např.
svařovacího	svařovací	k2eAgInSc2d1	svařovací
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
průtok	průtok	k1gInSc1	průtok
ochranného	ochranný	k2eAgInSc2d1	ochranný
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc4	rychlost
pojezdu	pojezd	k1gInSc2	pojezd
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Touto	tento	k3xDgFnSc7	tento
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
vybavena	vybaven	k2eAgFnSc1d1	vybavena
většina	většina	k1gFnSc1	většina
současných	současný	k2eAgMnPc2d1	současný
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
(	(	kIx(	(
<g/>
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
<g/>
)	)	kIx)	)
svářeček	svářečka	k1gFnPc2	svářečka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnostní	bezpečnostní	k2eAgNnSc1d1	bezpečnostní
hledisko	hledisko	k1gNnSc1	hledisko
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Napětí	napětí	k1gNnSc4	napětí
naprázdno	naprázdno	k6eAd1	naprázdno
===	===	k?	===
</s>
</p>
<p>
<s>
Napětí	napětí	k1gNnSc1	napětí
naprázdno	naprázdno	k6eAd1	naprázdno
je	být	k5eAaImIp3nS	být
maximální	maximální	k2eAgNnSc4d1	maximální
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
svařovací	svařovací	k2eAgNnSc1d1	svařovací
zdroj	zdroj	k1gInSc4	zdroj
dodávat	dodávat	k5eAaImF	dodávat
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
v	v	k7c4	v
danou	daný	k2eAgFnSc4d1	daná
chvíli	chvíle	k1gFnSc4	chvíle
nesvařuje	svařovat	k5eNaImIp3nS	svařovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
maximální	maximální	k2eAgFnSc1d1	maximální
hodnota	hodnota	k1gFnSc1	hodnota
napětí	napětí	k1gNnSc2	napětí
omezuje	omezovat	k5eAaImIp3nS	omezovat
na	na	k7c4	na
113	[number]	k4	113
V	V	kA	V
při	při	k7c6	při
používání	používání	k1gNnSc6	používání
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
80	[number]	k4	80
V	V	kA	V
při	při	k7c6	při
používání	používání	k1gNnSc6	používání
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Uvedené	uvedený	k2eAgFnPc1d1	uvedená
hodnoty	hodnota	k1gFnPc1	hodnota
jsou	být	k5eAaImIp3nP	být
platné	platný	k2eAgInPc1d1	platný
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
používání	používání	k1gNnSc6	používání
svařovacího	svařovací	k2eAgInSc2d1	svařovací
zdroje	zdroj	k1gInSc2	zdroj
mimo	mimo	k7c4	mimo
zakryté	zakrytý	k2eAgFnPc4d1	zakrytá
prostory	prostora	k1gFnPc4	prostora
platí	platit	k5eAaImIp3nS	platit
přísnější	přísný	k2eAgFnPc4d2	přísnější
regule	regule	k1gFnPc4	regule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stupeň	stupeň	k1gInSc1	stupeň
krytí	krytí	k1gNnSc1	krytí
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
elektrický	elektrický	k2eAgInSc4d1	elektrický
přístroj	přístroj	k1gInSc4	přístroj
je	být	k5eAaImIp3nS	být
vyžadováno	vyžadován	k2eAgNnSc1d1	vyžadováno
podle	podle	k7c2	podle
elektrotechnických	elektrotechnický	k2eAgFnPc2d1	elektrotechnická
norem	norma	k1gFnPc2	norma
ochrana	ochrana	k1gFnSc1	ochrana
obsluhy	obsluha	k1gFnSc2	obsluha
před	před	k7c7	před
nebezpečným	bezpečný	k2eNgInSc7d1	nebezpečný
dotykem	dotyk	k1gInSc7	dotyk
živých	živý	k2eAgFnPc2d1	živá
částí	část	k1gFnPc2	část
krytím	krytí	k1gNnSc7	krytí
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
normou	norma	k1gFnSc7	norma
ČSN	ČSN	kA	ČSN
33	[number]	k4	33
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
41	[number]	k4	41
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svařovací	svařovací	k2eAgInPc4d1	svařovací
zdroje	zdroj	k1gInPc4	zdroj
se	se	k3xPyFc4	se
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
stupeň	stupeň	k1gInSc1	stupeň
krytí	krytí	k1gNnSc2	krytí
IP	IP	kA	IP
21	[number]	k4	21
až	až	k8xS	až
IP	IP	kA	IP
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
svařovacích	svařovací	k2eAgInPc2d1	svařovací
zdrojů	zdroj	k1gInPc2	zdroj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Svařovací	svařovací	k2eAgNnSc1d1	svařovací
rotační	rotační	k2eAgNnSc1d1	rotační
dynamo	dynamo	k1gNnSc1	dynamo
===	===	k?	===
</s>
</p>
<p>
<s>
Nejstaršími	starý	k2eAgInPc7d3	nejstarší
zdroji	zdroj	k1gInPc7	zdroj
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
rotační	rotační	k2eAgMnPc1d1	rotační
dynama	dynamo	k1gNnSc2	dynamo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
generují	generovat	k5eAaImIp3nP	generovat
stejnosměrný	stejnosměrný	k2eAgInSc4d1	stejnosměrný
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Svařovací	svařovací	k2eAgNnSc1d1	svařovací
dynamo	dynamo	k1gNnSc1	dynamo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
poháněno	poháněn	k2eAgNnSc4d1	poháněno
buď	buď	k8xC	buď
elektrickým	elektrický	k2eAgInSc7d1	elektrický
nebo	nebo	k8xC	nebo
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
s	s	k7c7	s
dynamem	dynamo	k1gNnSc7	dynamo
tvoří	tvořit	k5eAaImIp3nP	tvořit
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
svařovací	svařovací	k2eAgFnSc2d1	svařovací
agregát	agregát	k1gInSc4	agregát
<g/>
.	.	kIx.	.
</s>
<s>
Svařovací	svařovací	k2eAgNnSc1d1	svařovací
dynamo	dynamo	k1gNnSc1	dynamo
disponuje	disponovat	k5eAaBmIp3nS	disponovat
strmou	strmý	k2eAgFnSc7d1	strmá
statickou	statický	k2eAgFnSc7d1	statická
charakteristikou	charakteristika	k1gFnSc7	charakteristika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
svařování	svařování	k1gNnSc4	svařování
obalenou	obalený	k2eAgFnSc7d1	obalená
elektrodou	elektroda	k1gFnSc7	elektroda
nebo	nebo	k8xC	nebo
svařování	svařování	k1gNnSc1	svařování
TIG	TIG	kA	TIG
případně	případně	k6eAd1	případně
MIG	mig	k1gInSc4	mig
<g/>
/	/	kIx~	/
<g/>
MAG	Maga	k1gFnPc2	Maga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svařovací	svařovací	k2eAgInSc1d1	svařovací
proud	proud	k1gInSc1	proud
se	se	k3xPyFc4	se
indukuje	indukovat	k5eAaBmIp3nS	indukovat
ve	v	k7c6	v
vodičích	vodič	k1gInPc6	vodič
kotvy	kotva	k1gFnSc2	kotva
rotoru	rotor	k1gInSc2	rotor
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
otáčejí	otáčet	k5eAaImIp3nP	otáčet
v	v	k7c6	v
elektromagnetickém	elektromagnetický	k2eAgNnSc6d1	elektromagnetické
poli	pole	k1gNnSc6	pole
statorového	statorový	k2eAgNnSc2d1	statorové
vinutí	vinutí	k1gNnSc2	vinutí
<g/>
.	.	kIx.	.
</s>
<s>
Regulace	regulace	k1gFnSc1	regulace
svařovacího	svařovací	k2eAgInSc2d1	svařovací
proudu	proud	k1gInSc2	proud
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
plynulá	plynulý	k2eAgFnSc1d1	plynulá
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
se	se	k3xPyFc4	se
změnou	změna	k1gFnSc7	změna
buzení	buzení	k1gNnSc2	buzení
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
statoru	stator	k1gInSc2	stator
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
patří	patřit	k5eAaImIp3nS	patřit
velká	velký	k2eAgFnSc1d1	velká
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
hlučnost	hlučnost	k1gFnSc1	hlučnost
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
spotřeba	spotřeba	k1gFnSc1	spotřeba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojená	spojený	k2eAgFnSc1d1	spojená
nízká	nízký	k2eAgFnSc1d1	nízká
efektivita	efektivita	k1gFnSc1	efektivita
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
hodnota	hodnota	k1gFnSc1	hodnota
zatěžovatele	zatěžovatel	k1gMnSc2	zatěžovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svařovací	svařovací	k2eAgInSc1d1	svařovací
transformátor	transformátor	k1gInSc1	transformátor
===	===	k?	===
</s>
</p>
<p>
<s>
Svařovací	svařovací	k2eAgInSc1d1	svařovací
transformátor	transformátor	k1gInSc1	transformátor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
generuje	generovat	k5eAaImIp3nS	generovat
jednofázový	jednofázový	k2eAgInSc1d1	jednofázový
střídavý	střídavý	k2eAgInSc1d1	střídavý
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
tvořeného	tvořený	k2eAgInSc2d1	tvořený
křemíkem	křemík	k1gInSc7	křemík
legovanými	legovaný	k2eAgInPc7d1	legovaný
ocelovými	ocelový	k2eAgInPc7d1	ocelový
plechy	plech	k1gInPc7	plech
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
rámu	rám	k1gInSc2	rám
<g/>
,	,	kIx,	,
primární	primární	k2eAgFnSc2d1	primární
a	a	k8xC	a
sekundární	sekundární	k2eAgFnSc2d1	sekundární
cívky	cívka	k1gFnSc2	cívka
<g/>
.	.	kIx.	.
</s>
<s>
Vinutí	vinutí	k1gNnSc1	vinutí
cívek	cívka	k1gFnPc2	cívka
je	být	k5eAaImIp3nS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
z	z	k7c2	z
měděných	měděný	k2eAgInPc2d1	měděný
nebo	nebo	k8xC	nebo
hliníkových	hliníkový	k2eAgInPc2d1	hliníkový
vodičů	vodič	k1gInPc2	vodič
<g/>
.	.	kIx.	.
</s>
<s>
Transformátor	transformátor	k1gInSc1	transformátor
je	být	k5eAaImIp3nS	být
napájen	napájet	k5eAaImNgInS	napájet
střídavým	střídavý	k2eAgInSc7d1	střídavý
proudem	proud	k1gInSc7	proud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
vinutím	vinutí	k1gNnSc7	vinutí
primární	primární	k2eAgFnSc2d1	primární
cívky	cívka	k1gFnSc2	cívka
a	a	k8xC	a
indukuje	indukovat	k5eAaBmIp3nS	indukovat
střídavé	střídavý	k2eAgNnSc1d1	střídavé
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
Elektromagnetickou	elektromagnetický	k2eAgFnSc7d1	elektromagnetická
indukcí	indukce	k1gFnSc7	indukce
vzniká	vznikat	k5eAaImIp3nS	vznikat
ve	v	k7c6	v
vinutí	vinutí	k1gNnSc6	vinutí
sekundární	sekundární	k2eAgFnSc2d1	sekundární
cívky	cívka	k1gFnSc2	cívka
střídavé	střídavý	k2eAgNnSc4d1	střídavé
napětí	napětí	k1gNnSc4	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zapálení	zapálení	k1gNnSc6	zapálení
elektrického	elektrický	k2eAgInSc2d1	elektrický
oblouku	oblouk	k1gInSc2	oblouk
při	při	k7c6	při
svařování	svařování	k1gNnSc6	svařování
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
uzavřením	uzavření	k1gNnSc7	uzavření
svařovacího	svařovací	k2eAgInSc2d1	svařovací
obvodu	obvod	k1gInSc2	obvod
střídavý	střídavý	k2eAgInSc1d1	střídavý
svařovací	svařovací	k2eAgInSc1d1	svařovací
proud	proud	k1gInSc1	proud
<g/>
.	.	kIx.	.
</s>
<s>
Svařovací	svařovací	k2eAgInPc4d1	svařovací
transformátory	transformátor	k1gInPc4	transformátor
disponují	disponovat	k5eAaBmIp3nP	disponovat
polostrmou	polostrmý	k2eAgFnSc7d1	polostrmý
statickou	statický	k2eAgFnSc7d1	statická
charakteristikou	charakteristika	k1gFnSc7	charakteristika
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
ruční	ruční	k2eAgNnSc4d1	ruční
svařování	svařování	k1gNnSc4	svařování
obalenou	obalený	k2eAgFnSc7d1	obalená
elektrodou	elektroda	k1gFnSc7	elektroda
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pro	pro	k7c4	pro
TIG	TIG	kA	TIG
svařování	svařování	k1gNnSc1	svařování
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
zdroj	zdroj	k1gInSc1	zdroj
vysokonapěťových	vysokonapěťový	k2eAgInPc2d1	vysokonapěťový
impulsů	impuls	k1gInPc2	impuls
–	–	k?	–
vysokofrekvenční	vysokofrekvenční	k2eAgInSc4d1	vysokofrekvenční
ionizátor	ionizátor	k1gInSc4	ionizátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svařovací	svařovací	k2eAgInPc1d1	svařovací
transformátory	transformátor	k1gInPc1	transformátor
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgFnSc4d2	nižší
spotřebu	spotřeba	k1gFnSc4	spotřeba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nižší	nízký	k2eAgFnSc4d2	nižší
hodnotu	hodnota	k1gFnSc4	hodnota
zatěžovatele	zatěžovatel	k1gMnSc2	zatěžovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svařovací	svařovací	k2eAgInSc1d1	svařovací
usměrňovač	usměrňovač	k1gInSc1	usměrňovač
===	===	k?	===
</s>
</p>
<p>
<s>
Svařovací	svařovací	k2eAgInSc1d1	svařovací
usměrňovač	usměrňovač	k1gInSc1	usměrňovač
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
generuje	generovat	k5eAaImIp3nS	generovat
stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
resp.	resp.	kA	resp.
usměrněný	usměrněný	k2eAgInSc1d1	usměrněný
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
síťového	síťový	k2eAgInSc2d1	síťový
transformátoru	transformátor	k1gInSc2	transformátor
a	a	k8xC	a
usměrňovacích	usměrňovací	k2eAgInPc2d1	usměrňovací
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
sekundárním	sekundární	k2eAgInSc6d1	sekundární
obvodu	obvod	k1gInSc6	obvod
transformátoru	transformátor	k1gInSc2	transformátor
<g/>
.	.	kIx.	.
</s>
<s>
Usměrňovacími	usměrňovací	k2eAgInPc7d1	usměrňovací
prvky	prvek	k1gInPc7	prvek
jsou	být	k5eAaImIp3nP	být
polovodičové	polovodičový	k2eAgFnPc4d1	polovodičová
křemíkové	křemíkový	k2eAgFnPc4d1	křemíková
diody	dioda	k1gFnPc4	dioda
nebo	nebo	k8xC	nebo
tyristory	tyristor	k1gInPc4	tyristor
<g/>
.	.	kIx.	.
</s>
<s>
Použitím	použití	k1gNnSc7	použití
transformátoru	transformátor	k1gInSc2	transformátor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jak	jak	k6eAd1	jak
jednofázový	jednofázový	k2eAgInSc1d1	jednofázový
tak	tak	k9	tak
i	i	k9	i
třífázový	třífázový	k2eAgMnSc1d1	třífázový
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
měnit	měnit	k5eAaImF	měnit
výstupní	výstupní	k2eAgInSc4d1	výstupní
proud	proud	k1gInSc4	proud
jak	jak	k8xC	jak
na	na	k7c4	na
střídavý	střídavý	k2eAgInSc4d1	střídavý
tak	tak	k8xC	tak
i	i	k9	i
na	na	k7c4	na
usměrněný	usměrněný	k2eAgInSc4d1	usměrněný
<g/>
.	.	kIx.	.
</s>
<s>
Svařovací	svařovací	k2eAgInPc1d1	svařovací
usměrňovače	usměrňovač	k1gInPc1	usměrňovač
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgFnSc4d2	nižší
spotřebu	spotřeba	k1gFnSc4	spotřeba
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
svařovacími	svařovací	k2eAgNnPc7d1	svařovací
dynamy	dynamo	k1gNnPc7	dynamo
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc4d2	vyšší
účinnost	účinnost	k1gFnSc4	účinnost
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgFnSc1d2	nižší
hmotnost	hmotnost	k1gFnSc1	hmotnost
a	a	k8xC	a
hlučnost	hlučnost	k1gFnSc1	hlučnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svařovací	svařovací	k2eAgInSc1d1	svařovací
invertor	invertor	k1gInSc1	invertor
===	===	k?	===
</s>
</p>
<p>
<s>
Svařovací	svařovací	k2eAgInPc1d1	svařovací
invertorové	invertorový	k2eAgInPc1d1	invertorový
zdroje	zdroj	k1gInPc1	zdroj
jsou	být	k5eAaImIp3nP	být
moderní	moderní	k2eAgInPc1d1	moderní
svařovací	svařovací	k2eAgInPc1d1	svařovací
zdroje	zdroj	k1gInPc1	zdroj
využívané	využívaný	k2eAgInPc1d1	využívaný
zejména	zejména	k9	zejména
v	v	k7c6	v
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
výrobě	výroba	k1gFnSc6	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
zdroje	zdroj	k1gInPc1	zdroj
řízené	řízený	k2eAgInPc4d1	řízený
výkonovými	výkonový	k2eAgInPc7d1	výkonový
tranzistory	tranzistor	k1gInPc7	tranzistor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
středofrekvenčních	středofrekvenční	k2eAgInPc2d1	středofrekvenční
měničů	měnič	k1gInPc2	měnič
s	s	k7c7	s
frekvencemi	frekvence	k1gFnPc7	frekvence
od	od	k7c2	od
20	[number]	k4	20
do	do	k7c2	do
100	[number]	k4	100
kHz	khz	kA	khz
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jejich	jejich	k3xOp3gNnSc3	jejich
uspořádání	uspořádání	k1gNnSc3	uspořádání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
pracovní	pracovní	k2eAgFnSc4d1	pracovní
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
,	,	kIx,	,
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
transformátory	transformátor	k1gInPc1	transformátor
menších	malý	k2eAgInPc2d2	menší
rozměrů	rozměr	k1gInPc2	rozměr
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
frekvence	frekvence	k1gFnSc1	frekvence
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
z	z	k7c2	z
usměrněného	usměrněný	k2eAgInSc2d1	usměrněný
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
zdroje	zdroj	k1gInPc1	zdroj
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
centrální	centrální	k2eAgFnSc4d1	centrální
řídící	řídící	k2eAgFnSc4d1	řídící
jednotku	jednotka	k1gFnSc4	jednotka
umožňující	umožňující	k2eAgFnSc4d1	umožňující
mj.	mj.	kA	mj.
i	i	k8xC	i
synergický	synergický	k2eAgInSc1d1	synergický
režim	režim	k1gInSc1	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
AMBROŽ	Ambrož	k1gMnSc1	Ambrož
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
<g/>
;	;	kIx,	;
KANDUS	KANDUS	kA	KANDUS
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
<g/>
;	;	kIx,	;
KUBÍČEK	Kubíček	k1gMnSc1	Kubíček
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Technologie	technologie	k1gFnSc1	technologie
svařování	svařování	k1gNnSc2	svařování
a	a	k8xC	a
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Recenzent	recenzent	k1gMnSc1	recenzent
Václav	Václav	k1gMnSc1	Václav
Minařík	Minařík	k1gMnSc1	Minařík
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
svářečská	svářečský	k2eAgFnSc1d1	svářečská
společnost	společnost	k1gFnSc1	společnost
ANB	ANB	kA	ANB
<g/>
,	,	kIx,	,
ZEROSS	ZEROSS	kA	ZEROSS
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
395	[number]	k4	395
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85771	[number]	k4	85771
<g/>
-	-	kIx~	-
<g/>
81	[number]	k4	81
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
210	[number]	k4	210
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
reference	reference	k1gFnSc1	reference
viz	vidět	k5eAaImRp2nS	vidět
Ambrož	Ambrož	k1gMnSc1	Ambrož
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KUBÍČEK	Kubíček	k1gMnSc1	Kubíček
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Technologie	technologie	k1gFnSc1	technologie
svařování	svařování	k1gNnSc2	svařování
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
ust	ust	k?	ust
<g/>
.	.	kIx.	.
<g/>
fme	fme	k?	fme
<g/>
.	.	kIx.	.
<g/>
vutbr	vutbr	k1gMnSc1	vutbr
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
reference	reference	k1gFnSc1	reference
viz	vidět	k5eAaImRp2nS	vidět
Kubíček	Kubíček	k1gMnSc1	Kubíček
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Zkrat	zkrat	k1gInSc1	zkrat
</s>
</p>
<p>
<s>
Úraz	úraz	k1gInSc4	úraz
elektrickým	elektrický	k2eAgInSc7d1	elektrický
proudem	proud	k1gInSc7	proud
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Svařovací	svařovací	k2eAgInSc4d1	svařovací
zdroj	zdroj	k1gInSc4	zdroj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
