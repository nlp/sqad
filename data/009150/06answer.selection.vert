<s>
Svařovací	svařovací	k2eAgInSc4d1	svařovací
zdroj	zdroj	k1gInSc4	zdroj
nebo	nebo	k8xC	nebo
svařovací	svařovací	k2eAgInSc4d1	svařovací
agregát	agregát	k1gInSc4	agregát
(	(	kIx(	(
<g/>
slangově	slangově	k6eAd1	slangově
svářečka	svářečka	k1gFnSc1	svářečka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
elektrické	elektrický	k2eAgNnSc4d1	elektrické
zařízení	zařízení	k1gNnSc4	zařízení
používané	používaný	k2eAgNnSc4d1	používané
pro	pro	k7c4	pro
generování	generování	k1gNnSc4	generování
svařovacího	svařovací	k2eAgInSc2d1	svařovací
proudu	proud	k1gInSc2	proud
při	při	k7c6	při
svařování	svařování	k1gNnSc6	svařování
metodami	metoda	k1gFnPc7	metoda
obloukového	obloukový	k2eAgNnSc2d1	obloukové
svařování	svařování	k1gNnSc2	svařování
nebo	nebo	k8xC	nebo
při	při	k7c6	při
odporovém	odporový	k2eAgNnSc6d1	odporové
svařování	svařování	k1gNnSc6	svařování
<g/>
.	.	kIx.	.
</s>
