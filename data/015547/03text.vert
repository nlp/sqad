<s>
Lvíče	lvíče	k1gNnSc1
(	(	kIx(
<g/>
román	román	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
LvíčeAutor	LvíčeAutor	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Škvorecký	Škvorecký	k2eAgMnSc1d1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lvíče	lvíče	k1gNnSc1
je	být	k5eAaImIp3nS
román	román	k1gInSc1
Josefa	Josef	k1gMnSc2
Škvoreckého	Škvorecký	k2eAgMnSc2d1
psaný	psaný	k2eAgInSc4d1
v	v	k7c6
letech	let	k1gInPc6
1963	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
a	a	k8xC
vydaný	vydaný	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Román	román	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
tří	tři	k4xCgFnPc2
žánrových	žánrový	k2eAgFnPc2d1
linií	linie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgFnSc7d2
částí	část	k1gFnSc7
knihy	kniha	k1gFnSc2
se	se	k3xPyFc4
vzájemně	vzájemně	k6eAd1
proplétají	proplétat	k5eAaImIp3nP
milostný	milostný	k2eAgInSc4d1
román	román	k1gInSc4
a	a	k8xC
satira	satira	k1gFnSc1
(	(	kIx(
<g/>
s	s	k7c7
prvky	prvek	k1gInPc7
klíčového	klíčový	k2eAgInSc2d1
románu	román	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
závěru	závěr	k1gInSc6
se	se	k3xPyFc4
vše	všechen	k3xTgNnSc1
proměňuje	proměňovat	k5eAaImIp3nS
v	v	k7c4
detektivní	detektivní	k2eAgInSc4d1
příběh	příběh	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
plně	plně	k6eAd1
rozveden	rozveden	k2eAgInSc1d1
až	až	k9
v	v	k7c6
posledních	poslední	k2eAgFnPc6d1
kapitolách	kapitola	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnSc1
české	český	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
knihy	kniha	k1gFnSc2
nese	nést	k5eAaImIp3nS
podtitul	podtitul	k1gInSc1
Koncové	koncový	k2eAgFnSc2d1
detektivní	detektivní	k2eAgFnSc2d1
melodrama	melodrama	k?
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc4
<g/>
,	,	kIx,
rozvržený	rozvržený	k2eAgInSc4d1
do	do	k7c2
17	#num#	k4
kapitol	kapitola	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
převážně	převážně	k6eAd1
v	v	k7c6
pražském	pražský	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Lednovy	Lednovy	k?
cesty	cesta	k1gFnSc2
do	do	k7c2
Medzihorence	Medzihorence	k1gFnSc2
<g/>
)	)	kIx)
na	na	k7c6
přelomu	přelom	k1gInSc6
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Próza	próza	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
také	také	k9
tematiku	tematika	k1gFnSc4
židovskou	židovský	k2eAgFnSc4d1
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
zevrubně	zevrubně	k6eAd1
představuje	představovat	k5eAaImIp3nS
také	také	k9
soudobé	soudobý	k2eAgInPc4d1
poměry	poměr	k1gInPc4
v	v	k7c6
knižním	knižní	k2eAgNnSc6d1
nakladatelství	nakladatelství	k1gNnSc6
na	na	k7c6
přelomu	přelom	k1gInSc6
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
též	též	k9
nástinem	nástin	k1gInSc7
dobové	dobový	k2eAgFnSc2d1
atmosféry	atmosféra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
závažná	závažný	k2eAgNnPc1d1
sdělení	sdělení	k1gNnPc1
autor	autor	k1gMnSc1
záměrně	záměrně	k6eAd1
skrývá	skrývat	k5eAaImIp3nS
za	za	k7c4
melodrama	melodrama	k?
či	či	k8xC
detektivku	detektivka	k1gFnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
žánry	žánr	k1gInPc1
vnímané	vnímaný	k2eAgInPc1d1
jako	jako	k8xC,k8xS
pokleslé	pokleslý	k2eAgInPc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
z	z	k7c2
důvodu	důvod	k1gInSc2
možného	možný	k2eAgNnSc2d1
uniknutí	uniknutí	k1gNnSc2
dobové	dobový	k2eAgFnSc6d1
cenzuře	cenzura	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Filmově	filmově	k6eAd1
byl	být	k5eAaImAgInS
román	román	k1gInSc1
zpracován	zpracován	k2eAgInSc1d1
režisérem	režisér	k1gMnSc7
Václavem	Václav	k1gMnSc7
Gajerem	Gajer	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
Flirt	flirt	k1gInSc1
se	s	k7c7
slečnou	slečna	k1gFnSc7
Stříbrnou	stříbrná	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
</s>
