<s>
Kyslík	kyslík	k1gInSc1	kyslík
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
O	O	kA	O
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Oxygenium	oxygenium	k1gNnSc1	oxygenium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plynný	plynný	k2eAgInSc1d1	plynný
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnPc1d1	tvořící
druhou	druhý	k4xOgFnSc4	druhý
hlavní	hlavní	k2eAgFnSc4d1	hlavní
složku	složka	k1gFnSc4	složka
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
