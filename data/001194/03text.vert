<s>
Kyslík	kyslík	k1gInSc1	kyslík
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
O	O	kA	O
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Oxygenium	oxygenium	k1gNnSc1	oxygenium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plynný	plynný	k2eAgInSc1d1	plynný
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnPc1d1	tvořící
druhou	druhý	k4xOgFnSc4	druhý
hlavní	hlavní	k2eAgFnSc4d1	hlavní
složku	složka	k1gFnSc4	složka
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
biogenním	biogenní	k2eAgInSc7d1	biogenní
prvkem	prvek	k1gInSc7	prvek
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
je	být	k5eAaImIp3nS	být
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
pro	pro	k7c4	pro
existenci	existence	k1gFnSc4	existence
většiny	většina	k1gFnSc2	většina
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
jeho	on	k3xPp3gInSc2	on
českého	český	k2eAgInSc2d1	český
názvu	název	k1gInSc2	název
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Presl	Presl	k1gInSc4	Presl
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dýchání	dýchání	k1gNnSc6	dýchání
vzduchu	vzduch	k1gInSc2	vzduch
o	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
kyslíku	kyslík	k1gInSc2	kyslík
větším	většit	k5eAaImIp1nS	většit
než	než	k8xS	než
75	[number]	k4	75
%	%	kIx~	%
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
většinou	většinou	k6eAd1	většinou
nenávratnému	návratný	k2eNgNnSc3d1	nenávratné
poškození	poškození	k1gNnSc3	poškození
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Alotropické	alotropický	k2eAgFnSc2d1	alotropický
modifikace	modifikace	k1gFnSc2	modifikace
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
řadu	řada	k1gFnSc4	řada
alotropických	alotropický	k2eAgFnPc2d1	alotropický
modifikací	modifikace	k1gFnPc2	modifikace
<g/>
:	:	kIx,	:
volné	volný	k2eAgInPc4d1	volný
kyslíkové	kyslíkový	k2eAgInPc4d1	kyslíkový
radikály	radikál	k1gInPc4	radikál
dikyslík	dikyslík	k1gMnSc1	dikyslík
trikyslík	trikyslík	k1gMnSc1	trikyslík
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
ozon	ozon	k1gInSc1	ozon
<g/>
)	)	kIx)	)
tetrakyslík	tetrakyslík	k1gInSc1	tetrakyslík
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
oxozon	oxozon	k1gInSc4	oxozon
<g/>
)	)	kIx)	)
pevný	pevný	k2eAgInSc4d1	pevný
kyslík	kyslík	k1gInSc4	kyslík
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
sledoval	sledovat	k5eAaImAgMnS	sledovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Určil	určit	k5eAaPmAgMnS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
složek	složka	k1gFnPc2	složka
podporuje	podporovat	k5eAaImIp3nS	podporovat
hoření	hoření	k1gNnSc1	hoření
<g/>
.	.	kIx.	.
1608	[number]	k4	1608
–	–	k?	–
Cornelius	Cornelius	k1gMnSc1	Cornelius
Drebbel	Drebbel	k1gMnSc1	Drebbel
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
výrobu	výroba	k1gFnSc4	výroba
kyslíku	kyslík	k1gInSc2	kyslík
zahřátím	zahřátí	k1gNnSc7	zahřátí
salnytru	salnytr	k1gInSc2	salnytr
(	(	kIx(	(
<g/>
ledku	ledek	k1gInSc2	ledek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
1772	[number]	k4	1772
–	–	k?	–
</s>
<s>
Carl	Carl	k1gMnSc1	Carl
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Scheele	Scheel	k1gInSc2	Scheel
objevil	objevit	k5eAaPmAgMnS	objevit
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
ho	on	k3xPp3gInSc4	on
"	"	kIx"	"
<g/>
ohnivý	ohnivý	k2eAgInSc4d1	ohnivý
vzduch	vzduch	k1gInSc4	vzduch
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
byl	být	k5eAaImAgInS	být
však	však	k9	však
publikován	publikovat	k5eAaBmNgInS	publikovat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1777	[number]	k4	1777
<g/>
.	.	kIx.	.
</s>
<s>
1774	[number]	k4	1774
–	–	k?	–
Joseph	Joseph	k1gInSc1	Joseph
Priestley	Priestlea	k1gFnSc2	Priestlea
objevuje	objevovat	k5eAaImIp3nS	objevovat
kyslík	kyslík	k1gInSc1	kyslík
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
nezávisle	závisle	k6eNd1	závisle
po	po	k7c6	po
Scheeleovi	Scheeleus	k1gMnSc6	Scheeleus
<g/>
.	.	kIx.	.
</s>
<s>
Publikuje	publikovat	k5eAaBmIp3nS	publikovat
však	však	k9	však
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
<g/>
.	.	kIx.	.
1779	[number]	k4	1779
–	–	k?	–
Antoine	Antoin	k1gInSc5	Antoin
Lavoisier	Lavoisira	k1gFnPc2	Lavoisira
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
oxygen	oxygen	k1gInSc1	oxygen
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kyselinu	kyselina	k1gFnSc4	kyselina
tvořící	tvořící	k2eAgFnSc4d1	tvořící
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
dýchatelnou	dýchatelný	k2eAgFnSc4d1	dýchatelná
<g/>
"	"	kIx"	"
část	část	k1gFnSc4	část
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
hoření	hoření	k1gNnSc1	hoření
<g/>
.	.	kIx.	.
1781	[number]	k4	1781
–	–	k?	–
Henry	Henry	k1gMnSc1	Henry
Cavendish	Cavendish	k1gMnSc1	Cavendish
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
<g />
.	.	kIx.	.
</s>
<s>
je	být	k5eAaImIp3nS	být
sloučeninou	sloučenina	k1gFnSc7	sloučenina
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
1785	[number]	k4	1785
–	–	k?	–
van	van	k1gInSc1	van
Marun	Maruna	k1gFnPc2	Maruna
popisuje	popisovat	k5eAaImIp3nS	popisovat
pach	pach	k1gInSc1	pach
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
mylně	mylně	k6eAd1	mylně
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
unikátní	unikátní	k2eAgFnSc3d1	unikátní
formě	forma	k1gFnSc3	forma
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
ozonu	ozon	k1gInSc2	ozon
O3	O3	k1gFnSc2	O3
1833	[number]	k4	1833
–	–	k?	–
Paul	Paul	k1gMnSc1	Paul
Bert	Berta	k1gFnPc2	Berta
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
na	na	k7c4	na
výhodu	výhoda	k1gFnSc4	výhoda
dýchání	dýchání	k1gNnSc2	dýchání
čistého	čistý	k2eAgInSc2d1	čistý
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
dekomprese	dekomprese	k1gFnSc2	dekomprese
-	-	kIx~	-
základy	základ	k1gInPc1	základ
kyslíkové	kyslíkový	k2eAgFnSc2d1	kyslíková
rekompresní	rekompresní	k2eAgFnSc2d1	rekompresní
lečby	lečba	k1gFnSc2	lečba
1840	[number]	k4	1840
–	–	k?	–
Christian	Christian	k1gMnSc1	Christian
Schönbein	Schönbein	k1gMnSc1	Schönbein
objevuje	objevovat	k5eAaImIp3nS	objevovat
<g />
.	.	kIx.	.
</s>
<s>
ozón	ozón	k1gInSc1	ozón
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
charakteristickému	charakteristický	k2eAgInSc3d1	charakteristický
zápachu	zápach	k1gInSc3	zápach
při	při	k7c6	při
používání	používání	k1gNnSc6	používání
elektrických	elektrický	k2eAgInPc2d1	elektrický
přístrojů	přístroj	k1gInPc2	přístroj
ve	v	k7c6	v
špatně	špatně	k6eAd1	špatně
větrané	větraný	k2eAgFnSc6d1	větraná
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
.	.	kIx.	.
1858	[number]	k4	1858
–	–	k?	–
Werner	Werner	k1gMnSc1	Werner
von	von	k1gInSc4	von
Siemens	siemens	k1gInSc1	siemens
konstruuje	konstruovat	k5eAaImIp3nS	konstruovat
první	první	k4xOgInSc1	první
přístroj	přístroj	k1gInSc1	přístroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
využívá	využívat	k5eAaImIp3nS	využívat
tichého	tichý	k2eAgInSc2d1	tichý
elektrického	elektrický	k2eAgInSc2d1	elektrický
výboje	výboj	k1gInSc2	výboj
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
ozónu	ozón	k1gInSc2	ozón
<g/>
.	.	kIx.	.
1861	[number]	k4	1861
–	–	k?	–
William	William	k1gInSc1	William
Odling	Odling	k1gInSc1	Odling
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
vzorec	vzorec	k1gInSc4	vzorec
O3	O3	k1gFnPc2	O3
pro	pro	k7c4	pro
ozón	ozón	k1gInSc4	ozón
po	po	k7c6	po
reakci	reakce	k1gFnSc6	reakce
ozónu	ozón	k1gInSc2	ozón
s	s	k7c7	s
jodidem	jodid	k1gInSc7	jodid
<g />
.	.	kIx.	.
</s>
<s>
draselným	draselný	k2eAgInSc7d1	draselný
<g/>
.	.	kIx.	.
1868	[number]	k4	1868
–	–	k?	–
J.	J.	kA	J.
L.	L.	kA	L.
Soret	Soret	k1gMnSc1	Soret
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
vzorec	vzorec	k1gInSc4	vzorec
ozonu	ozon	k1gInSc2	ozon
O3	O3	k1gMnPc2	O3
difuzními	difuzní	k2eAgFnPc7d1	difuzní
studiemi	studie	k1gFnPc7	studie
<g/>
.	.	kIx.	.
1877	[number]	k4	1877
–	–	k?	–
Kyslík	kyslík	k1gInSc1	kyslík
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
zkapalněn	zkapalnit	k5eAaPmNgInS	zkapalnit
nezávisle	závisle	k6eNd1	závisle
L.	L.	kA	L.
Cailletetem	Cailletet	k1gMnSc7	Cailletet
a	a	k8xC	a
R.	R.	kA	R.
Picketem	Picket	k1gMnSc7	Picket
<g/>
.	.	kIx.	.
1882	[number]	k4	1882
–	–	k?	–
J.	J.	kA	J.
W.	W.	kA	W.
Strutt	Strutt	k1gMnSc1	Strutt
objevuje	objevovat	k5eAaImIp3nS	objevovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
atomová	atomový	k2eAgFnSc1d1	atomová
hmotnost	hmotnost	k1gFnSc1	hmotnost
kyslíku	kyslík	k1gInSc2	kyslík
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
16,00	[number]	k4	16,00
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
15,872	[number]	k4	15,872
<g/>
.	.	kIx.	.
</s>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
permanentní	permanentní	k2eAgInSc1d1	permanentní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
nezbytný	zbytný	k2eNgInSc1d1	zbytný
pro	pro	k7c4	pro
existenci	existence	k1gFnSc4	existence
života	život	k1gInSc2	život
na	na	k7c6	na
naší	náš	k3xOp1gFnSc6	náš
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Slučování	slučování	k1gNnSc1	slučování
kyslíku	kyslík	k1gInSc2	kyslík
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
prvky	prvek	k1gInPc7	prvek
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hoření	hoření	k2eAgNnSc1d1	hoření
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
zahřátá	zahřátý	k2eAgFnSc1d1	zahřátá
na	na	k7c4	na
zápalnou	zápalný	k2eAgFnSc4d1	zápalná
teplotu	teplota	k1gFnSc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
prakticky	prakticky	k6eAd1	prakticky
vždy	vždy	k6eAd1	vždy
o	o	k7c4	o
exotermní	exotermní	k2eAgFnSc4d1	exotermní
reakci	reakce	k1gFnSc4	reakce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
značného	značný	k2eAgNnSc2d1	značné
množství	množství	k1gNnSc2	množství
tepelné	tepelný	k2eAgFnSc2d1	tepelná
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Produkty	produkt	k1gInPc1	produkt
hoření	hoření	k1gNnSc2	hoření
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
oxidy	oxid	k1gInPc1	oxid
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
kysličníky	kysličník	k1gInPc1	kysličník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
kyslík	kyslík	k1gInSc1	kyslík
velmi	velmi	k6eAd1	velmi
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
tvoří	tvořit	k5eAaImIp3nS	tvořit
plynný	plynný	k2eAgInSc1d1	plynný
kyslík	kyslík	k1gInSc1	kyslík
21	[number]	k4	21
objemových	objemový	k2eAgFnPc2d1	objemová
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hmotnostně	hmotnostně	k6eAd1	hmotnostně
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
kyslík	kyslík	k1gInSc4	kyslík
majoritním	majoritní	k2eAgInSc7d1	majoritní
prvkem	prvek	k1gInSc7	prvek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
horninách	hornina	k1gFnPc6	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
je	být	k5eAaImIp3nS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
46	[number]	k4	46
–	–	k?	–
50	[number]	k4	50
hmotnostních	hmotnostní	k2eAgFnPc2d1	hmotnostní
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlubších	hluboký	k2eAgFnPc6d2	hlubší
vrstvách	vrstva	k1gFnPc6	vrstva
zemského	zemský	k2eAgNnSc2d1	zemské
tělesa	těleso	k1gNnSc2	těleso
zastoupení	zastoupení	k1gNnSc2	zastoupení
kyslíku	kyslík	k1gInSc2	kyslík
klesá	klesat	k5eAaImIp3nS	klesat
a	a	k8xC	a
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zemském	zemský	k2eAgNnSc6d1	zemské
jádře	jádro	k1gNnSc6	jádro
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgInSc1d1	přítomen
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
zastoupení	zastoupení	k1gNnSc1	zastoupení
kyslíku	kyslík	k1gInSc2	kyslík
podstatně	podstatně	k6eAd1	podstatně
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
1	[number]	k4	1
000	[number]	k4	000
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
zde	zde	k6eAd1	zde
připadá	připadat	k5eAaPmIp3nS	připadat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
kyslík	kyslík	k1gInSc1	kyslík
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
O	o	k7c6	o
<g/>
−	−	k?	−
<g/>
II	II	kA	II
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
pak	pak	k6eAd1	pak
jako	jako	k8xC	jako
O	o	k7c6	o
<g/>
−	−	k?	−
<g/>
I	i	k9	i
a	a	k8xC	a
O	o	k7c4	o
<g/>
+	+	kIx~	+
<g/>
Ia	ia	k0	ia
také	také	k9	také
O	o	k7c4	o
−	−	k?	−
2	[number]	k4	2
v	v	k7c6	v
superoxidech	superoxid	k1gInPc6	superoxid
(	(	kIx(	(
<g/>
KO	KO	kA	KO
<g/>
2	[number]	k4	2
superoxid	superoxid	k1gInSc1	superoxid
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
)	)	kIx)	)
a	a	k8xC	a
O	o	k7c4	o
−	−	k?	−
3	[number]	k4	3
v	v	k7c6	v
ozonidech	ozonid	k1gInPc6	ozonid
<g/>
.	.	kIx.	.
</s>
<s>
Záporně	záporně	k6eAd1	záporně
dvojmocný	dvojmocný	k2eAgInSc1d1	dvojmocný
kyslík	kyslík	k1gInSc1	kyslík
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
široké	široký	k2eAgFnSc6d1	široká
škále	škála	k1gFnSc6	škála
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
oxidy	oxid	k1gInPc1	oxid
<g/>
,	,	kIx,	,
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
sloučenin	sloučenina	k1gFnPc2	sloučenina
jsou	být	k5eAaImIp3nP	být
detailněji	detailně	k6eAd2	detailně
popsány	popsat	k5eAaPmNgInP	popsat
v	v	k7c6	v
kapitolách	kapitola	k1gFnPc6	kapitola
příslušných	příslušný	k2eAgInPc2d1	příslušný
jednotlivým	jednotlivý	k2eAgMnPc3d1	jednotlivý
prvkům	prvek	k1gInPc3	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
anorganických	anorganický	k2eAgFnPc2d1	anorganická
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těch	ten	k3xDgFnPc2	ten
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
jmenovat	jmenovat	k5eAaBmF	jmenovat
uhličitany	uhličitan	k1gInPc4	uhličitan
(	(	kIx(	(
<g/>
CO	co	k6eAd1	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
−	−	k?	−
<g/>
II	II	kA	II
<g/>
,	,	kIx,	,
křemičitany	křemičitan	k1gInPc1	křemičitan
(	(	kIx(	(
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
−	−	k?	−
<g/>
II	II	kA	II
<g/>
,	,	kIx,	,
sírany	síran	k1gInPc1	síran
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
−	−	k?	−
<g/>
II	II	kA	II
<g/>
,	,	kIx,	,
dusičnany	dusičnan	k1gInPc1	dusičnan
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
−	−	k?	−
a	a	k8xC	a
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
−	−	k?	−
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1	alkalický
sloučeny	sloučen	k2eAgInPc1d1	sloučen
hydroxidy	hydroxid	k1gInPc1	hydroxid
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
přítomnosti	přítomnost	k1gFnSc2	přítomnost
skupiny	skupina	k1gFnSc2	skupina
-OH	-OH	k?	-OH
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
hydroxid	hydroxid	k1gInSc4	hydroxid
sodný	sodný	k2eAgMnSc1d1	sodný
NaOH	NaOH	k1gMnSc1	NaOH
<g/>
,	,	kIx,	,
draselný	draselný	k2eAgMnSc1d1	draselný
KOH	KOH	kA	KOH
a	a	k8xC	a
vápenatý	vápenatý	k2eAgMnSc1d1	vápenatý
<g/>
,	,	kIx,	,
hašené	hašený	k2eAgNnSc1d1	hašené
vápno	vápno	k1gNnSc1	vápno
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
valenci	valence	k1gFnSc6	valence
O	o	k7c6	o
<g/>
−	−	k?	−
<g/>
I	i	k9	i
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
kyslík	kyslík	k1gInSc4	kyslík
v	v	k7c6	v
peroxidech	peroxid	k1gInPc6	peroxid
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgInPc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
bezesporu	bezesporu	k9	bezesporu
peroxid	peroxid	k1gInSc1	peroxid
vodíku	vodík	k1gInSc2	vodík
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kapalná	kapalný	k2eAgFnSc1d1	kapalná
sloučenina	sloučenina	k1gFnSc1	sloučenina
má	mít	k5eAaImIp3nS	mít
silné	silný	k2eAgInPc4d1	silný
oxidační	oxidační	k2eAgInPc4d1	oxidační
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
svých	svůj	k3xOyFgInPc2	svůj
vodných	vodný	k2eAgInPc2d1	vodný
roztoků	roztok	k1gInPc2	roztok
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
pro	pro	k7c4	pro
desinfekci	desinfekce	k1gFnSc4	desinfekce
a	a	k8xC	a
v	v	k7c4	v
chemii	chemie	k1gFnSc4	chemie
jako	jako	k8xC	jako
oxidační	oxidační	k2eAgNnSc4d1	oxidační
činidlo	činidlo	k1gNnSc4	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Peroxid	peroxid	k1gInSc4	peroxid
sodný	sodný	k2eAgInSc4d1	sodný
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
,	,	kIx,	,
hygroskopická	hygroskopický	k2eAgFnSc1d1	hygroskopická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nachází	nacházet	k5eAaImIp3nS	nacházet
uplatnění	uplatnění	k1gNnSc4	uplatnění
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
energetické	energetický	k2eAgNnSc4d1	energetické
oxidační	oxidační	k2eAgNnSc4d1	oxidační
činidlo	činidlo	k1gNnSc4	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
fluor	fluor	k1gInSc1	fluor
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
větší	veliký	k2eAgFnSc4d2	veliký
elektronegativitu	elektronegativita	k1gFnSc4	elektronegativita
než	než	k8xS	než
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
několik	několik	k4yIc1	několik
fluoridů	fluorid	k1gInPc2	fluorid
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
se	se	k3xPyFc4	se
kyslík	kyslík	k1gInSc1	kyslík
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
O	O	kA	O
<g/>
+	+	kIx~	+
<g/>
I	I	kA	I
i	i	k8xC	i
O	O	kA	O
<g/>
+	+	kIx~	+
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
fluoridy	fluorid	k1gInPc1	fluorid
kyslíku	kyslík	k1gInSc2	kyslík
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
nestálé	stálý	k2eNgFnPc1d1	nestálá
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
existuje	existovat	k5eAaImIp3nS	existovat
reálná	reálný	k2eAgFnSc1d1	reálná
možnost	možnost	k1gFnSc1	možnost
jejich	jejich	k3xOp3gNnSc2	jejich
využití	využití	k1gNnSc2	využití
jako	jako	k8xS	jako
raketového	raketový	k2eAgNnSc2d1	raketové
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
těchto	tento	k3xDgFnPc2	tento
sloučenin	sloučenina	k1gFnPc2	sloučenina
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
všech	všecek	k3xTgInPc2	všecek
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kyslík	kyslík	k1gInSc1	kyslík
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
základní	základní	k2eAgInPc4d1	základní
biogenní	biogenní	k2eAgInPc4d1	biogenní
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc1d1	základní
skupiny	skupina	k1gFnPc1	skupina
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
kyslíku	kyslík	k1gInSc2	kyslík
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
alkoholy	alkohol	k1gInPc1	alkohol
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc1d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
C-OH	C-OH	k1gFnSc2	C-OH
fenoly	fenol	k1gInPc1	fenol
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
skupinu	skupina	k1gFnSc4	skupina
-OH	-OH	k?	-OH
mají	mít	k5eAaImIp3nP	mít
připojenu	připojen	k2eAgFnSc4d1	připojena
k	k	k7c3	k
aromatickému	aromatický	k2eAgNnSc3d1	aromatické
jádru	jádro	k1gNnSc3	jádro
ethery	ether	k1gInPc4	ether
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
C-O-C	C-O-C	k1gFnSc2	C-O-C
peroxidy	peroxid	k1gInPc4	peroxid
<g/>
,	,	kIx,	,
<g/>
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
C-O-O-C	C-O-O-C	k1gFnSc2	C-O-O-C
aldehydy	aldehyd	k1gInPc4	aldehyd
<g/>
,	,	kIx,	,
<g/>
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
HC	HC	kA	HC
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
ketony	keton	k1gInPc4	keton
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
C-CO-C	C-CO-C	k1gFnSc2	C-CO-C
karboxylové	karboxylový	k2eAgFnSc2d1	karboxylová
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
-COOH	-COOH	k?	-COOH
estery	ester	k1gInPc4	ester
<g/>
,	,	kIx,	,
<g/>
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
R-C-OOR	R-C-OOR	k1gFnPc2	R-C-OOR
z	z	k7c2	z
heterocyklických	heterocyklický	k2eAgFnPc2d1	heterocyklická
sloučenin	sloučenina	k1gFnPc2	sloučenina
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
uvést	uvést	k5eAaPmF	uvést
např.	např.	kA	např.
furan	furan	k1gInSc4	furan
<g/>
:	:	kIx,	:
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
neviditelnou	viditelný	k2eNgFnSc4d1	neviditelná
složku	složka	k1gFnSc4	složka
vzduchu	vzduch	k1gInSc2	vzduch
nutnou	nutný	k2eAgFnSc7d1	nutná
pro	pro	k7c4	pro
spalování	spalování	k1gNnSc4	spalování
prakticky	prakticky	k6eAd1	prakticky
každého	každý	k3xTgNnSc2	každý
fosilního	fosilní	k2eAgNnSc2d1	fosilní
paliva	palivo	k1gNnSc2	palivo
(	(	kIx(	(
<g/>
technologická	technologický	k2eAgFnSc1d1	technologická
oxidace	oxidace	k1gFnSc1	oxidace
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
výroba	výroba	k1gFnSc1	výroba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
–	–	k?	–
spalování	spalování	k1gNnSc1	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
v	v	k7c6	v
tepelných	tepelný	k2eAgFnPc6d1	tepelná
elektrárnách	elektrárna	k1gFnPc6	elektrárna
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
technologického	technologický	k2eAgNnSc2d1	Technologické
tepla	teplo	k1gNnSc2	teplo
<g/>
)	)	kIx)	)
výroba	výroba	k1gFnSc1	výroba
technologického	technologický	k2eAgNnSc2d1	Technologické
tepla	teplo	k1gNnSc2	teplo
–	–	k?	–
spalování	spalování	k1gNnSc1	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
v	v	k7c6	v
teplárnách	teplárna	k1gFnPc6	teplárna
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
pohon	pohon	k1gInSc1	pohon
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
turbín	turbína	k1gFnPc2	turbína
–	–	k?	–
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
<g />
.	.	kIx.	.
</s>
<s>
druzích	druh	k1gInPc6	druh
spalovacích	spalovací	k2eAgInPc2d1	spalovací
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
turbín	turbína	k1gFnPc2	turbína
vytápění	vytápění	k1gNnSc2	vytápění
domácností	domácnost	k1gFnPc2	domácnost
v	v	k7c6	v
domovních	domovní	k2eAgFnPc6d1	domovní
kotelnách	kotelna	k1gFnPc6	kotelna
<g/>
,	,	kIx,	,
kamnech	kamna	k1gNnPc6	kamna
či	či	k8xC	či
v	v	k7c6	v
krbech	krb	k1gInPc6	krb
příprava	příprava	k1gFnSc1	příprava
pokrmů	pokrm	k1gInPc2	pokrm
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
plynové	plynový	k2eAgInPc1d1	plynový
sporáky	sporák	k1gInPc1	sporák
<g/>
)	)	kIx)	)
nouzové	nouzový	k2eAgNnSc4d1	nouzové
osvětlování	osvětlování	k1gNnSc4	osvětlování
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
svíčky	svíčka	k1gFnPc1	svíčka
<g/>
,	,	kIx,	,
petrolejové	petrolejový	k2eAgFnPc1d1	petrolejová
lampy	lampa	k1gFnPc1	lampa
<g/>
)	)	kIx)	)
Nežádoucí	žádoucí	k2eNgInSc1d1	nežádoucí
chemicko-technologický	chemickoechnologický	k2eAgInSc1d1	chemicko-technologický
či	či	k8xC	či
fyzikálně-chemický	fyzikálněhemický	k2eAgInSc1d1	fyzikálně-chemický
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
koroze	koroze	k1gFnSc1	koroze
kovů	kov	k1gInPc2	kov
je	být	k5eAaImIp3nS	být
způsobená	způsobený	k2eAgFnSc1d1	způsobená
nežádoucí	žádoucí	k2eNgFnSc7d1	nežádoucí
oxidací	oxidace	k1gFnSc7	oxidace
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
doprovodnými	doprovodný	k2eAgFnPc7d1	doprovodná
chemickými	chemický	k2eAgFnPc7d1	chemická
reakcemi	reakce	k1gFnPc7	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
výlučně	výlučně	k6eAd1	výlučně
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
destilací	destilace	k1gFnSc7	destilace
zkapalněného	zkapalněný	k2eAgInSc2d1	zkapalněný
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
kyslík	kyslík	k1gInSc1	kyslík
se	se	k3xPyFc4	se
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
buď	buď	k8xC	buď
ve	v	k7c6	v
zkapalněném	zkapalněný	k2eAgInSc6d1	zkapalněný
stavu	stav	k1gInSc6	stav
ve	v	k7c6	v
speciálních	speciální	k2eAgFnPc6d1	speciální
Dewarových	Dewarův	k2eAgFnPc6d1	Dewarova
nádobách	nádoba	k1gFnPc6	nádoba
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
plynný	plynný	k2eAgInSc1d1	plynný
v	v	k7c6	v
ocelových	ocelový	k2eAgFnPc6d1	ocelová
tlakových	tlakový	k2eAgFnPc6d1	tlaková
lahvích	lahev	k1gFnPc6	lahev
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
reaktivitě	reaktivita	k1gFnSc3	reaktivita
čistého	čistý	k2eAgInSc2d1	čistý
kyslíku	kyslík	k1gInSc2	kyslík
je	být	k5eAaImIp3nS	být
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nedostal	dostat	k5eNaPmAgMnS	dostat
do	do	k7c2	do
přímého	přímý	k2eAgInSc2d1	přímý
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
organickými	organický	k2eAgFnPc7d1	organická
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
žádné	žádný	k3yNgFnPc1	žádný
součásti	součást	k1gFnPc1	součást
aparatury	aparatura	k1gFnSc2	aparatura
pro	pro	k7c4	pro
uchovávání	uchovávání	k1gNnSc4	uchovávání
a	a	k8xC	a
manipulaci	manipulace	k1gFnSc4	manipulace
s	s	k7c7	s
kapalným	kapalný	k2eAgInSc7d1	kapalný
nebo	nebo	k8xC	nebo
stlačeným	stlačený	k2eAgInSc7d1	stlačený
kyslíkem	kyslík	k1gInSc7	kyslík
nesmí	smět	k5eNaImIp3nS	smět
mazat	mazat	k5eAaImF	mazat
organickými	organický	k2eAgInPc7d1	organický
tuky	tuk	k1gInPc7	tuk
nebo	nebo	k8xC	nebo
oleji	olej	k1gInPc7	olej
<g/>
.	.	kIx.	.
</s>
<s>
Kyslíkové	kyslíkový	k2eAgInPc1d1	kyslíkový
koncentrátory	koncentrátor	k1gInPc1	koncentrátor
jsou	být	k5eAaImIp3nP	být
přístroje	přístroj	k1gInPc1	přístroj
<g/>
,	,	kIx,	,
<g/>
které	který	k3yIgInPc1	který
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
žádnou	žádný	k3yNgFnSc4	žádný
zásobu	zásoba	k1gFnSc4	zásoba
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
lahví	lahev	k1gFnPc2	lahev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vyvíjení	vyvíjení	k1gNnSc4	vyvíjení
vyšší	vysoký	k2eAgFnSc2d2	vyšší
koncentrace	koncentrace	k1gFnSc2	koncentrace
neomezeně	omezeně	k6eNd1	omezeně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dle	dle	k7c2	dle
nastavení	nastavení	k1gNnSc2	nastavení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
se	se	k3xPyFc4	se
čistý	čistý	k2eAgInSc1d1	čistý
kyslík	kyslík	k1gInSc1	kyslík
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
operacích	operace	k1gFnPc6	operace
a	a	k8xC	a
traumatických	traumatický	k2eAgInPc6d1	traumatický
stavech	stav	k1gInPc6	stav
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
pacientova	pacientův	k2eAgNnSc2d1	pacientovo
dýchání	dýchání	k1gNnSc2	dýchání
a	a	k8xC	a
lepšímu	dobrý	k2eAgNnSc3d2	lepší
okysličení	okysličení	k1gNnSc3	okysličení
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Směsi	směs	k1gFnPc1	směs
kyslíku	kyslík	k1gInSc2	kyslík
s	s	k7c7	s
inertními	inertní	k2eAgInPc7d1	inertní
plyny	plyn	k1gInPc7	plyn
slouží	sloužit	k5eAaImIp3nS	sloužit
potápěčům	potápěč	k1gMnPc3	potápěč
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
dekompresní	dekompresní	k2eAgFnSc2d1	dekompresní
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
i	i	k8xC	i
všech	všecek	k3xTgInPc2	všecek
ostatních	ostatní	k2eAgInPc2d1	ostatní
dýchacích	dýchací	k2eAgInPc2d1	dýchací
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
potápění	potápění	k1gNnSc4	potápění
do	do	k7c2	do
velkých	velký	k2eAgFnPc2d1	velká
hloubek	hloubka	k1gFnPc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vysokohorští	vysokohorský	k2eAgMnPc1d1	vysokohorský
horolezci	horolezec	k1gMnPc1	horolezec
a	a	k8xC	a
letci	letec	k1gMnPc1	letec
se	se	k3xPyFc4	se
v	v	k7c6	v
nutných	nutný	k2eAgInPc6d1	nutný
případech	případ	k1gInPc6	případ
uchylují	uchylovat	k5eAaImIp3nP	uchylovat
k	k	k7c3	k
dýchání	dýchání	k1gNnSc3	dýchání
čistého	čistý	k2eAgInSc2d1	čistý
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
piloti	pilot	k1gMnPc1	pilot
stíhacích	stíhací	k2eAgNnPc2d1	stíhací
letadel	letadlo	k1gNnPc2	letadlo
jsou	být	k5eAaImIp3nP	být
vybaveni	vybaven	k2eAgMnPc1d1	vybaven
směsmi	směs	k1gFnPc7	směs
stlačených	stlačený	k2eAgFnPc2d1	stlačená
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
základní	základní	k2eAgFnSc7d1	základní
složkou	složka	k1gFnSc7	složka
je	být	k5eAaImIp3nS	být
kyslík	kyslík	k1gInSc1	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvýšením	zvýšení	k1gNnSc7	zvýšení
koncentrace	koncentrace	k1gFnSc2	koncentrace
kyslíku	kyslík	k1gInSc2	kyslík
se	se	k3xPyFc4	se
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
jeho	jeho	k3xOp3gInSc1	jeho
parciální	parciální	k2eAgInSc1d1	parciální
tlak	tlak	k1gInSc1	tlak
a	a	k8xC	a
ulehčí	ulehčit	k5eAaPmIp3nS	ulehčit
se	se	k3xPyFc4	se
tak	tak	k9	tak
dýchání	dýchání	k1gNnSc2	dýchání
v	v	k7c6	v
řídké	řídký	k2eAgFnSc6d1	řídká
atmosféře	atmosféra	k1gFnSc6	atmosféra
a	a	k8xC	a
předejde	předejít	k5eAaPmIp3nS	předejít
vysokohorské	vysokohorský	k2eAgFnPc4d1	vysokohorská
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Američtí	americký	k2eAgMnPc1d1	americký
astronauti	astronaut	k1gMnPc1	astronaut
programu	program	k1gInSc2	program
Apollo	Apollo	k1gMnSc1	Apollo
dýchali	dýchat	k5eAaImAgMnP	dýchat
také	také	k9	také
atmosféru	atmosféra	k1gFnSc4	atmosféra
z	z	k7c2	z
téměř	téměř	k6eAd1	téměř
čistého	čistý	k2eAgInSc2d1	čistý
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
snížit	snížit	k5eAaPmF	snížit
tlak	tlak	k1gInSc4	tlak
v	v	k7c6	v
kabině	kabina	k1gFnSc6	kabina
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c4	na
třetinu	třetina	k1gFnSc4	třetina
běžné	běžný	k2eAgFnSc2d1	běžná
hodnoty	hodnota	k1gFnSc2	hodnota
a	a	k8xC	a
tak	tak	k6eAd1	tak
odlehčit	odlehčit	k5eAaPmF	odlehčit
její	její	k3xOp3gFnSc4	její
hermetickou	hermetický	k2eAgFnSc4d1	hermetická
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
stalo	stát	k5eAaPmAgNnS	stát
osudným	osudný	k2eAgInPc3d1	osudný
posádce	posádka	k1gFnSc6	posádka
Apolla	Apollo	k1gNnSc2	Apollo
1	[number]	k4	1
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ve	v	k7c6	v
vysoce	vysoce	k6eAd1	vysoce
hořlavé	hořlavý	k2eAgFnSc6d1	hořlavá
atmosféře	atmosféra	k1gFnSc6	atmosféra
uhořela	uhořet	k5eAaPmAgFnS	uhořet
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hoření	hoření	k1gNnSc6	hoření
směsi	směs	k1gFnSc2	směs
kyslíku	kyslík	k1gInSc2	kyslík
s	s	k7c7	s
acetylenem	acetylen	k1gInSc7	acetylen
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
teploty	teplota	k1gFnPc4	teplota
cca	cca	kA	cca
3	[number]	k4	3
150	[number]	k4	150
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
200	[number]	k4	200
°	°	k?	°
<g/>
C.	C.	kA	C.
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
kyslíko-acetylenový	kyslíkocetylenový	k2eAgInSc1d1	kyslíko-acetylenový
plamen	plamen	k1gInSc1	plamen
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
řezání	řezání	k1gNnSc3	řezání
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
tavení	tavení	k1gNnSc2	tavení
kovů	kov	k1gInPc2	kov
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
bodem	bod	k1gInSc7	bod
tání	tání	k1gNnSc2	tání
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
platinových	platinový	k2eAgInPc2d1	platinový
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
oceli	ocel	k1gFnSc2	ocel
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
především	především	k6eAd1	především
odstranit	odstranit	k5eAaPmF	odstranit
z	z	k7c2	z
matrice	matrice	k1gFnSc2	matrice
železa	železo	k1gNnSc2	železo
přebytečný	přebytečný	k2eAgInSc1d1	přebytečný
uhlík	uhlík	k1gInSc1	uhlík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
karbidu	karbid	k1gInSc2	karbid
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přebytečný	přebytečný	k2eAgInSc1d1	přebytečný
uhlík	uhlík	k1gInSc1	uhlík
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
příměsmi	příměs	k1gFnPc7	příměs
se	se	k3xPyFc4	se
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
spálením	spálení	k1gNnSc7	spálení
obvyle	obvyl	k1gInSc5	obvyl
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
konvertoru	konvertor	k1gInSc2	konvertor
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vháněním	vhánění	k1gNnSc7	vhánění
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
Bessemerově	Bessemerův	k2eAgInSc6d1	Bessemerův
a	a	k8xC	a
Thomasově	Thomasův	k2eAgInSc6d1	Thomasův
konvertoru	konvertor	k1gInSc6	konvertor
nebo	nebo	k8xC	nebo
vháněním	vhánění	k1gNnSc7	vhánění
čistého	čistý	k2eAgInSc2d1	čistý
kyslíku	kyslík	k1gInSc2	kyslík
do	do	k7c2	do
roztaveného	roztavený	k2eAgNnSc2d1	roztavené
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
kyslíkovém	kyslíkový	k2eAgInSc6d1	kyslíkový
konvertoru	konvertor	k1gInSc6	konvertor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
za	za	k7c4	za
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
taveniny	tavenina	k1gFnSc2	tavenina
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oxidaci	oxidace	k1gFnSc3	oxidace
přítomného	přítomný	k2eAgInSc2d1	přítomný
uhlíku	uhlík	k1gInSc2	uhlík
na	na	k7c4	na
plynné	plynný	k2eAgInPc4d1	plynný
oxidy	oxid	k1gInPc4	oxid
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
odcházejí	odcházet	k5eAaImIp3nP	odcházet
jako	jako	k9	jako
spaliny	spaliny	k1gFnPc1	spaliny
<g/>
.	.	kIx.	.
</s>
<s>
Kapalný	kapalný	k2eAgInSc1d1	kapalný
kyslík	kyslík	k1gInSc1	kyslík
většinou	většinou	k6eAd1	většinou
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
okysličovadlo	okysličovadlo	k1gNnSc1	okysličovadlo
raketových	raketový	k2eAgInPc2d1	raketový
motorů	motor	k1gInPc2	motor
při	při	k7c6	při
letech	let	k1gInPc6	let
kosmických	kosmický	k2eAgFnPc2d1	kosmická
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
složek	složka	k1gFnPc2	složka
pro	pro	k7c4	pro
náplň	náplň	k1gFnSc4	náplň
některých	některý	k3yIgInPc2	některý
typů	typ	k1gInPc2	typ
palivových	palivový	k2eAgInPc2d1	palivový
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kyslík	kyslík	k1gInSc4	kyslík
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
kyslík	kyslík	k1gInSc4	kyslík
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kyslík	kyslík	k1gInSc1	kyslík
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oxidizing	Oxidizing	k1gInSc1	Oxidizing
Agents	Agents	k1gInSc1	Agents
>	>	kIx)	>
Oxygen	oxygen	k1gInSc1	oxygen
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oxygen	oxygen	k1gInSc1	oxygen
(	(	kIx(	(
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Properties	Properties	k1gInSc1	Properties
<g/>
,	,	kIx,	,
Uses	Uses	k1gInSc1	Uses
<g/>
,	,	kIx,	,
Applications	Applications	k1gInSc1	Applications
</s>
