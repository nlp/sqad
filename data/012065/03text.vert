<p>
<s>
Zámecká	zámecký	k2eAgFnSc1d1	zámecká
Dyje	Dyje	k1gFnSc1	Dyje
je	být	k5eAaImIp3nS	být
jižní	jižní	k2eAgNnSc4d1	jižní
rameno	rameno	k1gNnSc4	rameno
řeky	řeka	k1gFnSc2	řeka
Dyje	Dyje	k1gFnSc2	Dyje
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
11	[number]	k4	11
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Dyje	Dyje	k1gFnSc2	Dyje
se	se	k3xPyFc4	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
na	na	k7c6	na
39,7	[number]	k4	39,7
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
a	a	k8xC	a
protéká	protékat	k5eAaImIp3nS	protékat
Lednickým	lednický	k2eAgInSc7d1	lednický
zámeckým	zámecký	k2eAgInSc7d1	zámecký
parkem	park	k1gInSc7	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Trasa	trasa	k1gFnSc1	trasa
==	==	k?	==
</s>
</p>
<p>
<s>
Zámecká	zámecký	k2eAgFnSc1d1	zámecká
Dyje	Dyje	k1gFnSc1	Dyje
začíná	začínat	k5eAaImIp3nS	začínat
kousek	kousek	k1gInSc4	kousek
od	od	k7c2	od
mostu	most	k1gInSc2	most
v	v	k7c6	v
Bulharech	Bulhar	k1gMnPc6	Bulhar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
od	od	k7c2	od
hlavního	hlavní	k2eAgInSc2d1	hlavní
toku	tok	k1gInSc2	tok
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
stavidlo	stavidlo	k1gNnSc1	stavidlo
a	a	k8xC	a
protipovodňová	protipovodňový	k2eAgFnSc1d1	protipovodňová
hráz	hráz	k1gFnSc1	hráz
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
cesty	cesta	k1gFnSc2	cesta
lemují	lemovat	k5eAaImIp3nP	lemovat
popadané	popadaný	k2eAgInPc1d1	popadaný
stromy	strom	k1gInPc1	strom
a	a	k8xC	a
větve	větev	k1gFnPc1	větev
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Nejdku	Nejdek	k1gInSc2	Nejdek
si	se	k3xPyFc3	se
situace	situace	k1gFnSc1	situace
trochu	trochu	k6eAd1	trochu
vylepšuje	vylepšovat	k5eAaImIp3nS	vylepšovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c2	za
obcí	obec	k1gFnPc2	obec
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
úsek	úsek	k1gInSc1	úsek
podobný	podobný	k2eAgInSc1d1	podobný
tomu	ten	k3xDgNnSc3	ten
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lednici	Lednice	k1gFnSc6	Lednice
přes	přes	k7c4	přes
Zámeckou	zámecký	k2eAgFnSc4d1	zámecká
Dyji	Dyje	k1gFnSc4	Dyje
vede	vést	k5eAaImIp3nS	vést
několik	několik	k4yIc1	několik
mostů	most	k1gInPc2	most
a	a	k8xC	a
stavidlo	stavidlo	k1gNnSc1	stavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
protéká	protékat	k5eAaImIp3nS	protékat
zámeckým	zámecký	k2eAgInSc7d1	zámecký
parkem	park	k1gInSc7	park
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
náhon	náhon	k1gInSc1	náhon
rybníků	rybník	k1gInPc2	rybník
<g/>
,	,	kIx,	,
s	s	k7c7	s
výhledem	výhled	k1gInSc7	výhled
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
minaret	minaret	k1gInSc4	minaret
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
1,5	[number]	k4	1,5
km	km	kA	km
od	od	k7c2	od
zámeckého	zámecký	k2eAgInSc2d1	zámecký
parku	park	k1gInSc2	park
ústí	ústit	k5eAaImIp3nS	ústit
Zámecká	zámecký	k2eAgFnSc1d1	zámecká
Dyje	Dyje	k1gFnSc1	Dyje
do	do	k7c2	do
Černé	Černé	k2eAgFnSc2d1	Černé
Dyje	Dyje	k1gFnSc2	Dyje
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Zámecká	zámecký	k2eAgFnSc1d1	zámecká
Dyje	Dyje	k1gFnSc1	Dyje
na	na	k7c4	na
tourism	tourism	k1gInSc4	tourism
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
vodáků	vodák	k1gMnPc2	vodák
</s>
</p>
