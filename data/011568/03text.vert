<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Cory	Cora	k1gFnSc2	Cora
Bryar	Bryar	k1gMnSc1	Bryar
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1979	[number]	k4	1979
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gFnSc1	Illinois
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
bubeník	bubeník	k1gMnSc1	bubeník
skupiny	skupina	k1gFnSc2	skupina
My	my	k3xPp1nPc1	my
Chemical	Chemical	k1gMnPc6	Chemical
Romance	romance	k1gFnSc2	romance
<g/>
.	.	kIx.	.
</s>
<s>
Bob	Bob	k1gMnSc1	Bob
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
My	my	k3xPp1nPc1	my
Chemical	Chemical	k1gFnSc1	Chemical
Romance	romance	k1gFnPc4	romance
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
odešel	odejít	k5eAaPmAgMnS	odejít
bývalý	bývalý	k2eAgMnSc1d1	bývalý
bubeník	bubeník	k1gMnSc1	bubeník
Matt	Matt	k1gMnSc1	Matt
Pelissier	Pelissier	k1gMnSc1	Pelissier
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Frankie	Frankie	k1gFnSc1	Frankie
Iero	Iero	k6eAd1	Iero
není	být	k5eNaImIp3nS	být
členem	člen	k1gMnSc7	člen
My	my	k3xPp1nPc1	my
Chemical	Chemical	k1gMnSc3	Chemical
Romance	romance	k1gFnSc1	romance
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Matta	Matta	k1gMnSc1	Matta
Pelissiera	Pelissier	k1gMnSc2	Pelissier
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
dělal	dělat	k5eAaImAgMnS	dělat
zvukového	zvukový	k2eAgMnSc4d1	zvukový
technika	technik	k1gMnSc4	technik
skupinám	skupina	k1gFnPc3	skupina
jako	jako	k9	jako
The	The	k1gMnSc1	The
Used	Used	k1gMnSc1	Used
atd.	atd.	kA	atd.
Podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
vydání	vydání	k1gNnSc4	vydání
alba	album	k1gNnSc2	album
MCR	MCR	kA	MCR
Three	Three	k1gFnSc4	Three
Cheers	Cheersa	k1gFnPc2	Cheersa
for	forum	k1gNnPc2	forum
the	the	k?	the
Sweet	Sweet	k1gMnSc1	Sweet
Revenge	Reveng	k1gInSc2	Reveng
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ještě	ještě	k9	ještě
nebyl	být	k5eNaImAgInS	být
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
členem	člen	k1gMnSc7	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jejich	jejich	k3xOp3gNnPc6	jejich
videích	videum	k1gNnPc6	videum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
kapelu	kapela	k1gFnSc4	kapela
My	my	k3xPp1nPc1	my
Chemical	Chemical	k1gMnPc3	Chemical
Romance	romance	k1gFnSc2	romance
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybavení	vybavení	k1gNnSc1	vybavení
==	==	k?	==
</s>
</p>
<p>
<s>
Bob	Bob	k1gMnSc1	Bob
používá	používat	k5eAaImIp3nS	používat
bubny	buben	k1gInPc4	buben
C	C	kA	C
&	&	k?	&
C	C	kA	C
Custom	Custom	k1gInSc1	Custom
Drums	Drums	k1gInSc1	Drums
s	s	k7c7	s
činely	činel	k1gInPc7	činel
Sabian	Sabiana	k1gFnPc2	Sabiana
ze	z	k7c2	z
série	série	k1gFnSc2	série
Sabian	Sabian	k1gInSc1	Sabian
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Paragon	paragon	k1gInSc1	paragon
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
americké	americký	k2eAgFnSc2d1	americká
klasické	klasický	k2eAgFnSc2d1	klasická
rockové	rockový	k2eAgFnSc2d1	rocková
paličky	palička	k1gFnSc2	palička
Vic	Vic	k1gMnSc1	Vic
Firth	Firth	k1gMnSc1	Firth
s	s	k7c7	s
Vic	Vic	k1gFnSc7	Vic
Firth	Firtha	k1gFnPc2	Firtha
Victape	Victap	k1gMnSc5	Victap
<g/>
.	.	kIx.	.
</s>
<s>
Bob	Bob	k1gMnSc1	Bob
Bryar	Bryar	k1gMnSc1	Bryar
používá	používat	k5eAaImIp3nS	používat
dvě	dva	k4xCgFnPc1	dva
soupravy	souprava	k1gFnPc1	souprava
bicích	bicí	k2eAgMnPc2d1	bicí
<g/>
.	.	kIx.	.
</s>
</p>
