<s>
Rovečné	Rovečný	k2eAgNnSc1d1	Rovečné
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Rowetschin	Rowetschin	k2eAgInSc1d1	Rowetschin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
649	[number]	k4	649
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
obce	obec	k1gFnSc2	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
zřejmě	zřejmě	k6eAd1	zřejmě
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
rovec	rovec	k1gInSc1	rovec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zdrobnělina	zdrobnělina	k1gFnSc1	zdrobnělina
ke	k	k7c3	k
slovu	slovo	k1gNnSc3	slovo
"	"	kIx"	"
<g/>
rov	rov	k1gInSc4	rov
<g/>
"	"	kIx"	"
,	,	kIx,	,
které	který	k3yRgNnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
místo	místo	k1gNnSc4	místo
nerovné	rovný	k2eNgNnSc4d1	nerovné
s	s	k7c7	s
výmoly	výmol	k1gInPc7	výmol
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
poloze	poloha	k1gFnSc3	poloha
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Srstka	srstka	k1gFnSc1	srstka
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kronice	kronika	k1gFnSc6	kronika
uvádí	uvádět	k5eAaImIp3nS	uvádět
ještě	ještě	k6eAd1	ještě
jiný	jiný	k2eAgInSc1d1	jiný
výklad	výklad	k1gInSc1	výklad
původu	původ	k1gInSc2	původ
jména	jméno	k1gNnSc2	jméno
-	-	kIx~	-
"	"	kIx"	"
<g/>
při	při	k7c6	při
přestavbě	přestavba	k1gFnSc6	přestavba
některých	některý	k3yIgInPc2	některý
domů	dům	k1gInPc2	dům
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
staré	starý	k2eAgFnPc1d1	stará
lidské	lidský	k2eAgFnPc1d1	lidská
kosti	kost	k1gFnPc1	kost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kolem	kolem	k7c2	kolem
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
usuzovali	usuzovat	k5eAaImAgMnP	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nalézalo	nalézat	k5eAaImAgNnS	nalézat
pohřebiště	pohřebiště	k1gNnSc1	pohřebiště
-	-	kIx~	-
pole	pole	k1gNnSc1	pole
s	s	k7c7	s
hroby	hrob	k1gInPc7	hrob
-	-	kIx~	-
rovy	rov	k1gInPc7	rov
-	-	kIx~	-
rovečné	rovečný	k2eAgFnPc4d1	rovečný
pole	pole	k1gFnPc4	pole
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yRgInSc2	který
pochází	pocházet	k5eAaImIp3nS	pocházet
první	první	k4xOgFnSc1	první
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Rovečném	Rovečný	k2eAgMnSc6d1	Rovečný
<g/>
,	,	kIx,	,
patřila	patřit	k5eAaImAgFnS	patřit
ves	ves	k1gFnSc4	ves
Pernštejnům	Pernštejn	k1gInPc3	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Rovečné	Rovečný	k2eAgFnPc1d1	Rovečný
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Kunštátského	kunštátský	k2eAgNnSc2d1	kunštátské
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
soudního	soudní	k2eAgInSc2d1	soudní
okresu	okres	k1gInSc2	okres
Kunštát	Kunštát	k1gInSc1	Kunštát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
1	[number]	k4	1
<g/>
.	.	kIx.	.
československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
během	během	k7c2	během
Protektorátu	protektorát	k1gInSc2	protektorát
bylo	být	k5eAaImAgNnS	být
Rovečné	Rovečný	k2eAgFnPc4d1	Rovečný
součástí	součást	k1gFnSc7	součást
okresu	okres	k1gInSc2	okres
Boskovice	Boskovice	k1gInPc4	Boskovice
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
byla	být	k5eAaImAgFnS	být
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
Bystřice	Bystřice	k1gFnSc2	Bystřice
nad	nad	k7c7	nad
Pernštejnem	Pernštejn	k1gInSc7	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
územní	územní	k2eAgFnSc6d1	územní
reorganizaci	reorganizace	k1gFnSc6	reorganizace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
se	se	k3xPyFc4	se
obec	obec	k1gFnSc1	obec
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
okresu	okres	k1gInSc2	okres
Žďár	Žďár	k1gInSc4	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Rovečné	Rovečný	k2eAgFnPc1d1	Rovečný
Malé	Malé	k2eAgFnPc1d1	Malé
Tresné	Tresný	k2eAgFnPc1d1	Tresný
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1335	[number]	k4	1335
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Rovečné	Rovečný	k2eAgFnSc2d1	Rovečný
Dětský	dětský	k2eAgInSc4d1	dětský
domov	domov	k1gInSc4	domov
Evangelický	evangelický	k2eAgInSc4d1	evangelický
kostel	kostel	k1gInSc4	kostel
sboru	sbor	k1gInSc2	sbor
Českobratrské	českobratrský	k2eAgFnSc2d1	Českobratrská
církve	církev	k1gFnSc2	církev
evangelické	evangelický	k2eAgFnSc2d1	evangelická
Vila	vila	k1gFnSc1	vila
čp.	čp.	k?	čp.
174	[number]	k4	174
(	(	kIx(	(
<g/>
evangelická	evangelický	k2eAgFnSc1d1	evangelická
fara	fara	k1gFnSc1	fara
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Martina	Martin	k1gMnSc2	Martin
Budova	budova	k1gFnSc1	budova
pošty	pošta	k1gFnSc2	pošta
č.	č.	k?	č.
<g/>
p.	p.	k?	p.
<g/>
120	[number]	k4	120
<g/>
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
200	[number]	k4	200
<g/>
let	léto	k1gNnPc2	léto
stará	stará	k1gFnSc1	stará
<g/>
)	)	kIx)	)
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Bauer	Bauer	k1gMnSc1	Bauer
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
<g/>
:	:	kIx,	:
bohemista	bohemista	k1gMnSc1	bohemista
<g/>
,	,	kIx,	,
rusista	rusista	k1gMnSc1	rusista
<g/>
,	,	kIx,	,
syntaktik	syntaktik	k1gMnSc1	syntaktik
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc4	farnost
Rovečné	Rovečný	k2eAgInPc4d1	Rovečný
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rovečné	Rovečný	k2eAgFnPc4d1	Rovečný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
