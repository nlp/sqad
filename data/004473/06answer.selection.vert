<s>
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc4	Building
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
William	William	k1gInSc1	William
F.	F.	kA	F.
Lamb	Lamb	k1gInSc1	Lamb
z	z	k7c2	z
architektonické	architektonický	k2eAgFnSc2d1	architektonická
firmy	firma	k1gFnSc2	firma
Shreve	Shreev	k1gFnSc2	Shreev
<g/>
,	,	kIx,	,
Lamb	Lamba	k1gFnPc2	Lamba
a	a	k8xC	a
Harmon	Harmona	k1gFnPc2	Harmona
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgInPc1d1	stavební
výkresy	výkres	k1gInPc1	výkres
byly	být	k5eAaImAgInP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
za	za	k7c4	za
pouhé	pouhý	k2eAgInPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
ovšem	ovšem	k9	ovšem
inspirovány	inspirován	k2eAgInPc4d1	inspirován
předchozími	předchozí	k2eAgInPc7d1	předchozí
plány	plán	k1gInPc7	plán
pro	pro	k7c4	pro
Reynolds	Reynolds	k1gInSc4	Reynolds
Building	Building	k1gInSc4	Building
ve	v	k7c6	v
Winston-Salem	Winston-Sal	k1gMnSc7	Winston-Sal
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Karolíně	Karolína	k1gFnSc6	Karolína
a	a	k8xC	a
Carew	Carew	k1gFnSc6	Carew
Tower	Tower	k1gInSc1	Tower
v	v	k7c6	v
Cincinnati	Cincinnati	k1gFnPc6	Cincinnati
<g/>
,	,	kIx,	,
stát	stát	k5eAaPmF	stát
Ohio	Ohio	k1gNnSc4	Ohio
<g/>
.	.	kIx.	.
</s>
