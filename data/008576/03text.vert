<p>
<s>
Hodinky	hodinka	k1gFnPc1	hodinka
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
přenosné	přenosný	k2eAgFnPc1d1	přenosná
hodiny	hodina	k1gFnPc1	hodina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
další	další	k2eAgFnPc4d1	další
funkce	funkce	k1gFnPc4	funkce
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hodinkové	hodinkový	k2eAgFnPc4d1	hodinková
komplikace	komplikace	k1gFnPc4	komplikace
<g/>
)	)	kIx)	)
–	–	k?	–
nejčastěji	často	k6eAd3	často
datumovník	datumovník	k1gMnSc1	datumovník
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
též	též	k9	též
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
a	a	k8xC	a
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
celé	celý	k2eAgNnSc4d1	celé
datum	datum	k1gNnSc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
i	i	k9	i
zařízením	zařízení	k1gNnSc7	zařízení
na	na	k7c4	na
odměřování	odměřování	k1gNnSc4	odměřování
kratších	krátký	k2eAgInPc2d2	kratší
časů	čas	k1gInPc2	čas
(	(	kIx(	(
<g/>
chronograf	chronograf	k1gInSc1	chronograf
<g/>
,	,	kIx,	,
stopky	stopka	k1gFnPc1	stopka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
otočný	otočný	k2eAgInSc4d1	otočný
číselník	číselník	k1gInSc4	číselník
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Hodinky	hodinka	k1gFnPc1	hodinka
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
módním	módní	k2eAgInSc7d1	módní
doplňkem	doplněk	k1gInSc7	doplněk
<g/>
,	,	kIx,	,
předmětem	předmět	k1gInSc7	předmět
společenské	společenský	k2eAgFnSc2d1	společenská
reprezentace	reprezentace	k1gFnSc2	reprezentace
a	a	k8xC	a
prestiže	prestiž	k1gFnSc2	prestiž
<g/>
.	.	kIx.	.
</s>
<s>
Drahocenné	drahocenný	k2eAgFnPc1d1	drahocenná
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
z	z	k7c2	z
titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
zdobené	zdobený	k2eAgInPc4d1	zdobený
jako	jako	k9	jako
šperky	šperk	k1gInPc4	šperk
a	a	k8xC	a
podléhají	podléhat	k5eAaImIp3nP	podléhat
proměnám	proměna	k1gFnPc3	proměna
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
hodinky	hodinka	k1gFnPc4	hodinka
speciální	speciální	k2eAgFnPc4d1	speciální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
hodinek	hodinka	k1gFnPc2	hodinka
==	==	k?	==
</s>
</p>
<p>
<s>
Hodinky	hodinka	k1gFnPc4	hodinka
dělíme	dělit	k5eAaImIp1nP	dělit
podle	podle	k7c2	podle
různých	různý	k2eAgNnPc2d1	různé
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
nošení	nošení	k1gNnSc2	nošení
</s>
</p>
<p>
<s>
kapesní	kapesní	k2eAgFnPc1d1	kapesní
hodinky	hodinka	k1gFnPc1	hodinka
(	(	kIx(	(
<g/>
s	s	k7c7	s
mechanickým	mechanický	k2eAgInSc7d1	mechanický
pohonem	pohon	k1gInSc7	pohon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
v	v	k7c6	v
kapse	kapsa	k1gFnSc6	kapsa
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
v	v	k7c6	v
kapsičce	kapsička	k1gFnSc6	kapsička
pánské	pánský	k2eAgFnSc2d1	pánská
vesty	vesta	k1gFnSc2	vesta
<g/>
,	,	kIx,	,
ke	k	k7c3	k
knoflíku	knoflík	k1gInSc3	knoflík
vesty	vesta	k1gFnSc2	vesta
byly	být	k5eAaImAgInP	být
často	často	k6eAd1	často
připevněny	připevněn	k2eAgInPc1d1	připevněn
řetízkem	řetízek	k1gInSc7	řetízek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
náramkové	náramkový	k2eAgFnPc1d1	náramková
hodinky	hodinka	k1gFnPc1	hodinka
s	s	k7c7	s
náramkem	náramek	k1gInSc7	náramek
nebo	nebo	k8xC	nebo
řemínkem	řemínek	k1gInSc7	řemínek
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
<g/>
,	,	kIx,	,
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
kovu	kov	k1gInSc2	kov
či	či	k8xC	či
textilie	textilie	k1gFnSc2	textilie
<g/>
,	,	kIx,	,
upevněným	upevněný	k2eAgNnSc7d1	upevněné
na	na	k7c4	na
hodinky	hodinka	k1gFnPc4	hodinka
pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgFnPc2	dva
stěžejek	stěžejka	k1gFnPc2	stěžejka
nebo	nebo	k8xC	nebo
provléknutým	provléknutý	k2eAgNnSc7d1	provléknutý
pod	pod	k7c7	pod
oušky	ouško	k1gNnPc7	ouško
pláště	plášť	k1gInPc4	plášť
hodinek	hodinka	k1gFnPc2	hodinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
závěsné	závěsný	k2eAgFnPc1d1	závěsná
nebo	nebo	k8xC	nebo
nákrční	nákrční	k2eAgFnPc1d1	nákrční
–	–	k?	–
dámské	dámský	k2eAgFnPc1d1	dámská
<g/>
,	,	kIx,	,
nošené	nošený	k2eAgFnPc1d1	nošená
na	na	k7c6	na
řetízku	řetízek	k1gInSc6	řetízek
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
</s>
</p>
<p>
<s>
hodinky	hodinka	k1gFnPc1	hodinka
v	v	k7c6	v
prstenu	prsten	k1gInSc6	prsten
–	–	k?	–
navléknuté	navléknutý	k2eAgFnSc2d1	navléknutá
na	na	k7c4	na
prstpodle	prstpodle	k6eAd1	prstpodle
nositelů	nositel	k1gMnPc2	nositel
</s>
</p>
<p>
<s>
pánské	pánský	k2eAgNnSc1d1	pánské
</s>
</p>
<p>
<s>
dámské	dámský	k2eAgInPc1d1	dámský
–	–	k?	–
bývají	bývat	k5eAaImIp3nP	bývat
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
klenotnickém	klenotnický	k2eAgNnSc6d1	klenotnické
provedení	provedení	k1gNnSc6	provedení
</s>
</p>
<p>
<s>
dětské	dětský	k2eAgFnPc1d1	dětská
–	–	k?	–
odolné	odolný	k2eAgFnPc1d1	odolná
konstrukce	konstrukce	k1gFnPc1	konstrukce
i	i	k9	i
materiálů	materiál	k1gInPc2	materiál
</s>
</p>
<p>
<s>
unisexpodle	unisexpodle	k6eAd1	unisexpodle
strojku	strojek	k1gInSc2	strojek
</s>
</p>
<p>
<s>
mechanické	mechanický	k2eAgNnSc1d1	mechanické
(	(	kIx(	(
<g/>
s	s	k7c7	s
pružinou	pružina	k1gFnSc7	pružina
<g/>
,	,	kIx,	,
natahují	natahovat	k5eAaImIp3nP	natahovat
se	se	k3xPyFc4	se
korunkou	korunka	k1gFnSc7	korunka
nebo	nebo	k8xC	nebo
samočinně	samočinně	k6eAd1	samočinně
excentrem	excentr	k1gInSc7	excentr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
s	s	k7c7	s
denním	denní	k2eAgNnSc7d1	denní
datem	datum	k1gNnSc7	datum
</s>
</p>
<p>
<s>
s	s	k7c7	s
chronometrem	chronometr	k1gInSc7	chronometr
či	či	k8xC	či
stopkami	stopka	k1gFnPc7	stopka
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
krátkých	krátký	k2eAgInPc2d1	krátký
intervalů	interval	k1gInPc2	interval
</s>
</p>
<p>
<s>
námořní	námořní	k2eAgInPc1d1	námořní
chronometry	chronometr	k1gInPc1	chronometr
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
a	a	k8xC	a
dlouhodobou	dlouhodobý	k2eAgFnSc7d1	dlouhodobá
přesností	přesnost	k1gFnSc7	přesnost
chodu	chod	k1gInSc2	chod
</s>
</p>
<p>
<s>
s	s	k7c7	s
mechanickým	mechanický	k2eAgInSc7d1	mechanický
budíkem	budík	k1gInSc7	budík
<g/>
,	,	kIx,	,
bitím	bití	k1gNnSc7	bití
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
komplikacemi	komplikace	k1gFnPc7	komplikace
</s>
</p>
<p>
<s>
elektronické	elektronický	k2eAgInPc1d1	elektronický
</s>
</p>
<p>
<s>
s	s	k7c7	s
indikací	indikace	k1gFnSc7	indikace
analogovou	analogový	k2eAgFnSc7d1	analogová
(	(	kIx(	(
<g/>
ručičky	ručička	k1gFnSc2	ručička
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
s	s	k7c7	s
indikací	indikace	k1gFnSc7	indikace
číslicovou	číslicový	k2eAgFnSc7d1	číslicová
(	(	kIx(	(
<g/>
digitální	digitální	k2eAgFnSc7d1	digitální
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
se	s	k7c7	s
zvukovým	zvukový	k2eAgInSc7d1	zvukový
signálem	signál	k1gInSc7	signál
(	(	kIx(	(
<g/>
budíkem	budík	k1gInSc7	budík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
bitím	bití	k1gNnSc7	bití
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
s	s	k7c7	s
"	"	kIx"	"
<g/>
komplikacemi	komplikace	k1gFnPc7	komplikace
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
chytré	chytrý	k2eAgFnPc1d1	chytrá
hodinky	hodinka	k1gFnPc1	hodinka
</s>
</p>
<p>
<s>
s	s	k7c7	s
kalkulačkou	kalkulačka	k1gFnSc7	kalkulačka
</s>
</p>
<p>
<s>
s	s	k7c7	s
databází	databáze	k1gFnSc7	databáze
a	a	k8xC	a
plánovačem	plánovač	k1gMnSc7	plánovač
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
sport	sport	k1gInSc4	sport
s	s	k7c7	s
krokoměrem	krokoměr	k1gInSc7	krokoměr
<g/>
,	,	kIx,	,
GPS	GPS	kA	GPS
<g/>
,	,	kIx,	,
registrací	registrace	k1gFnSc7	registrace
pohybu	pohyb	k1gInSc2	pohyb
</s>
</p>
<p>
<s>
s	s	k7c7	s
funkcemi	funkce	k1gFnPc7	funkce
mobilního	mobilní	k2eAgInSc2d1	mobilní
telefonupodle	telefonupodle	k6eAd1	telefonupodle
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
výzdoby	výzdoba	k1gFnSc2	výzdoba
pláště	plášť	k1gInSc2	plášť
</s>
</p>
<p>
<s>
tříplášťové	tříplášťový	k2eAgNnSc1d1	tříplášťový
<g/>
,	,	kIx,	,
ciferník	ciferník	k1gInSc1	ciferník
je	být	k5eAaImIp3nS	být
kryt	kryt	k2eAgInSc1d1	kryt
odklápěcím	odklápěcí	k2eAgNnSc7d1	odklápěcí
víčkem	víčko	k1gNnSc7	víčko
</s>
</p>
<p>
<s>
dvouplášťové	dvouplášťový	k2eAgFnPc4d1	dvouplášťová
(	(	kIx(	(
<g/>
zlaté	zlatý	k2eAgFnPc4d1	zlatá
<g/>
,	,	kIx,	,
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
<g/>
,	,	kIx,	,
ryté	rytý	k2eAgFnPc4d1	rytá
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
přátelské	přátelský	k2eAgNnSc4d1	přátelské
<g/>
,	,	kIx,	,
pamětní	pamětní	k2eAgFnPc4d1	pamětní
–	–	k?	–
s	s	k7c7	s
miniaturním	miniaturní	k2eAgInSc7d1	miniaturní
rytým	rytý	k2eAgInSc7d1	rytý
nebo	nebo	k8xC	nebo
smaltovaným	smaltovaný	k2eAgInSc7d1	smaltovaný
portrétem	portrét	k1gInSc7	portrét
</s>
</p>
<p>
<s>
módní	módní	k2eAgFnPc1d1	módní
hodinky	hodinka	k1gFnPc1	hodinka
–	–	k?	–
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
fantastickým	fantastický	k2eAgInSc7d1	fantastický
designem	design	k1gInSc7	design
</s>
</p>
<p>
<s>
luxusní	luxusní	k2eAgFnPc1d1	luxusní
hodinky	hodinka	k1gFnPc1	hodinka
–	–	k?	–
přinášejí	přinášet	k5eAaImIp3nP	přinášet
nositeli	nositel	k1gMnSc3	nositel
prestižpodle	prestižpodle	k6eAd1	prestižpodle
zaměřenívojenské	zaměřenívojenský	k2eAgFnPc1d1	zaměřenívojenský
hodinky	hodinka	k1gFnPc1	hodinka
–	–	k?	–
s	s	k7c7	s
chronometrem	chronometr	k1gInSc7	chronometr
a	a	k8xC	a
dodatečnými	dodatečný	k2eAgFnPc7d1	dodatečná
komplikacemi	komplikace	k1gFnPc7	komplikace
</s>
</p>
<p>
<s>
hodinky	hodinka	k1gFnPc1	hodinka
pilotů	pilot	k1gInPc2	pilot
–	–	k?	–
s	s	k7c7	s
chronometrem	chronometr	k1gInSc7	chronometr
a	a	k8xC	a
dodatečnými	dodatečný	k2eAgFnPc7d1	dodatečná
komplikacemi	komplikace	k1gFnPc7	komplikace
<g/>
,	,	kIx,	,
otřesuvzdorné	otřesuvzdorný	k2eAgFnPc1d1	otřesuvzdorná
</s>
</p>
<p>
<s>
hodinky	hodinka	k1gFnPc1	hodinka
potápěčů	potápěč	k1gInPc2	potápěč
–	–	k?	–
vodotěsné	vodotěsný	k2eAgFnPc1d1	vodotěsná
<g/>
,	,	kIx,	,
stopky	stopka	k1gFnPc1	stopka
odměřují	odměřovat	k5eAaImIp3nP	odměřovat
zbývající	zbývající	k2eAgInSc4d1	zbývající
čas	čas	k1gInSc4	čas
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
</s>
</p>
<p>
<s>
hodinky	hodinka	k1gFnPc1	hodinka
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
se	s	k7c7	s
specifickými	specifický	k2eAgFnPc7d1	specifická
funkcemi	funkce	k1gFnPc7	funkce
</s>
</p>
<p>
<s>
párové	párový	k2eAgFnSc2d1	párová
hodinky	hodinka	k1gFnSc2	hodinka
–	–	k?	–
pánské	pánský	k2eAgNnSc4d1	pánské
a	a	k8xC	a
dámské	dámský	k2eAgNnSc4d1	dámské
<g/>
,	,	kIx,	,
vhodné	vhodný	k2eAgNnSc4d1	vhodné
jako	jako	k8xC	jako
dárek	dárek	k1gInSc4	dárek
</s>
</p>
<p>
<s>
hodinky	hodinka	k1gFnPc4	hodinka
s	s	k7c7	s
extra	extra	k2eAgInPc7d1	extra
čitelnými	čitelný	k2eAgInPc7d1	čitelný
ciferníky	ciferník	k1gInPc7	ciferník
a	a	k8xC	a
vysokým	vysoký	k2eAgInSc7d1	vysoký
kontrastem	kontrast	k1gInSc7	kontrast
</s>
</p>
<p>
<s>
slepecké	slepecký	k2eAgFnPc4d1	slepecká
hodinky	hodinka	k1gFnPc4	hodinka
s	s	k7c7	s
hmatovým	hmatový	k2eAgInSc7d1	hmatový
ciferníkem	ciferník	k1gInSc7	ciferník
a	a	k8xC	a
víčkem	víčko	k1gNnSc7	víčko
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
části	část	k1gFnPc1	část
==	==	k?	==
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
hodiny	hodina	k1gFnPc1	hodina
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
hodinky	hodinka	k1gFnPc4	hodinka
několik	několik	k4yIc4	několik
hlavních	hlavní	k2eAgFnPc2d1	hlavní
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pohon	pohon	k1gInSc1	pohon
(	(	kIx(	(
<g/>
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
oscilátor	oscilátor	k1gInSc1	oscilátor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odměřuje	odměřovat	k5eAaImIp3nS	odměřovat
krátké	krátký	k2eAgInPc4d1	krátký
časové	časový	k2eAgInPc4d1	časový
intervaly	interval	k1gInPc4	interval
<g/>
;	;	kIx,	;
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
závisí	záviset	k5eAaImIp3nS	záviset
přesnost	přesnost	k1gFnSc4	přesnost
(	(	kIx(	(
<g/>
chod	chod	k1gInSc4	chod
<g/>
)	)	kIx)	)
hodinek	hodinka	k1gFnPc2	hodinka
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
převody	převod	k1gInPc4	převod
a	a	k8xC	a
indikaci	indikace	k1gFnSc4	indikace
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
počítají	počítat	k5eAaImIp3nP	počítat
kmity	kmit	k1gInPc4	kmit
oscilátoru	oscilátor	k1gInSc2	oscilátor
a	a	k8xC	a
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
výsledek	výsledek	k1gInSc4	výsledek
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
pouzdro	pouzdro	k1gNnSc1	pouzdro
<g/>
,	,	kIx,	,
řemínek	řemínek	k1gInSc1	řemínek
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
pomocné	pomocný	k2eAgFnPc1d1	pomocná
části	část	k1gFnPc1	část
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
mechanických	mechanický	k2eAgFnPc2d1	mechanická
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
spirálová	spirálový	k2eAgFnSc1d1	spirálová
pružina	pružina	k1gFnSc1	pružina
<g/>
,	,	kIx,	,
u	u	k7c2	u
lepších	dobrý	k2eAgFnPc2d2	lepší
hodinek	hodinka	k1gFnPc2	hodinka
uložená	uložený	k2eAgFnSc1d1	uložená
v	v	k7c6	v
pérovníku	pérovník	k1gInSc6	pérovník
<g/>
.	.	kIx.	.
</s>
<s>
Oscilátorem	oscilátor	k1gInSc7	oscilátor
nejstarších	starý	k2eAgFnPc2d3	nejstarší
hodinek	hodinka	k1gFnPc2	hodinka
byl	být	k5eAaImAgInS	být
lihýř	lihýř	k1gInSc1	lihýř
<g/>
,	,	kIx,	,
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
podstatně	podstatně	k6eAd1	podstatně
lepší	dobrý	k2eAgFnSc1d2	lepší
setrvačka	setrvačka	k1gFnSc1	setrvačka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nepokoj	nepokoj	k1gInSc4	nepokoj
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
moderních	moderní	k2eAgFnPc2d1	moderní
hodinek	hodinka	k1gFnPc2	hodinka
s	s	k7c7	s
odpruženým	odpružený	k2eAgNnSc7d1	odpružené
uložením	uložení	k1gNnSc7	uložení
čepů	čep	k1gInPc2	čep
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
hodinky	hodinka	k1gFnPc1	hodinka
vydržely	vydržet	k5eAaPmAgFnP	vydržet
i	i	k9	i
menší	malý	k2eAgInPc4d2	menší
nárazy	náraz	k1gInPc4	náraz
<g/>
.	.	kIx.	.
</s>
<s>
Kmity	kmit	k1gInPc1	kmit
oscilátoru	oscilátor	k1gInSc2	oscilátor
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
krok	krok	k1gInSc1	krok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
naopak	naopak	k6eAd1	naopak
oscilátoru	oscilátor	k1gInSc2	oscilátor
dodává	dodávat	k5eAaImIp3nS	dodávat
impulz	impulz	k1gInSc4	impulz
<g/>
.	.	kIx.	.
</s>
<s>
Krok	krok	k1gInSc1	krok
propustí	propustit	k5eAaPmIp3nS	propustit
při	při	k7c6	při
každém	každý	k3xTgInSc6	každý
kmitu	kmit	k1gInSc6	kmit
oscilátoru	oscilátor	k1gInSc2	oscilátor
jeden	jeden	k4xCgInSc4	jeden
zub	zub	k1gInSc4	zub
stoupacího	stoupací	k2eAgNnSc2d1	stoupací
kola	kolo	k1gNnSc2	kolo
a	a	k8xC	a
soustava	soustava	k1gFnSc1	soustava
ozubených	ozubený	k2eAgInPc2d1	ozubený
převodů	převod	k1gInPc2	převod
převádí	převádět	k5eAaImIp3nS	převádět
tento	tento	k3xDgInSc1	tento
pohyb	pohyb	k1gInSc1	pohyb
na	na	k7c4	na
minutové	minutový	k2eAgNnSc4d1	minutové
kolo	kolo	k1gNnSc4	kolo
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
připevněna	připevněn	k2eAgFnSc1d1	připevněna
minutová	minutový	k2eAgFnSc1d1	minutová
ručka	ručka	k1gFnSc1	ručka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
pohybu	pohyb	k1gInSc2	pohyb
minutového	minutový	k2eAgNnSc2d1	minutové
kola	kolo	k1gNnSc2	kolo
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
i	i	k9	i
pohyb	pohyb	k1gInSc1	pohyb
hodinové	hodinový	k2eAgFnSc2d1	hodinová
ručky	ručka	k1gFnSc2	ručka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mechanické	mechanický	k2eAgFnPc1d1	mechanická
hodinky	hodinka	k1gFnPc1	hodinka
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
natahovací	natahovací	k2eAgNnPc1d1	natahovací
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
u	u	k7c2	u
starých	starý	k2eAgFnPc2d1	stará
hodinek	hodinka	k1gFnPc2	hodinka
klíčkem	klíček	k1gInSc7	klíček
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
korunkou	korunka	k1gFnSc7	korunka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
k	k	k7c3	k
nastavení	nastavení	k1gNnSc3	nastavení
správného	správný	k2eAgInSc2d1	správný
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
automatické	automatický	k2eAgNnSc1d1	automatické
natahování	natahování	k1gNnSc1	natahování
excentrickým	excentrický	k2eAgInSc7d1	excentrický
rotorem	rotor	k1gInSc7	rotor
<g/>
.	.	kIx.	.
</s>
<s>
Ručky	ručka	k1gFnPc1	ručka
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
nad	nad	k7c7	nad
ciferníkem	ciferník	k1gInSc7	ciferník
s	s	k7c7	s
dělením	dělení	k1gNnSc7	dělení
na	na	k7c4	na
hodiny	hodina	k1gFnPc4	hodina
a	a	k8xC	a
minuty	minuta	k1gFnPc4	minuta
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
natřeny	natřen	k2eAgMnPc4d1	natřen
světélkujícím	světélkující	k2eAgInSc7d1	světélkující
nátěrem	nátěr	k1gInSc7	nátěr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hřídeli	hřídel	k1gInSc6	hřídel
druhého	druhý	k4xOgNnSc2	druhý
převodového	převodový	k2eAgNnSc2d1	převodové
kola	kolo	k1gNnSc2	kolo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
upevněna	upevněn	k2eAgFnSc1d1	upevněna
i	i	k8xC	i
sekundová	sekundový	k2eAgFnSc1d1	sekundová
ručka	ručka	k1gFnSc1	ručka
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
umístěná	umístěný	k2eAgFnSc1d1	umístěná
uprostřed	uprostřed	k7c2	uprostřed
ciferníku	ciferník	k1gInSc2	ciferník
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
centrální	centrální	k2eAgFnSc1d1	centrální
<g/>
"	"	kIx"	"
sekundová	sekundový	k2eAgFnSc1d1	sekundová
ručka	ručka	k1gFnSc1	ručka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
elektronických	elektronický	k2eAgFnPc2d1	elektronická
hodinek	hodinka	k1gFnPc2	hodinka
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
článek	článek	k1gInSc1	článek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
baterie	baterie	k1gFnSc1	baterie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
oscilátor	oscilátor	k1gInSc1	oscilátor
bývá	bývat	k5eAaImIp3nS	bývat
řízen	řídit	k5eAaImNgInS	řídit
křemenným	křemenný	k2eAgInSc7d1	křemenný
výbrusem	výbrus	k1gInSc7	výbrus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
quartz	quartz	k1gInSc1	quartz
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
frekvence	frekvence	k1gFnSc1	frekvence
oscilátoru	oscilátor	k1gInSc2	oscilátor
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
32	[number]	k4	32
kHz	khz	kA	khz
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
elektronicky	elektronicky	k6eAd1	elektronicky
dělí	dělit	k5eAaImIp3nS	dělit
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
indikuje	indikovat	k5eAaBmIp3nS	indikovat
na	na	k7c6	na
číslicovém	číslicový	k2eAgInSc6d1	číslicový
displeji	displej	k1gInSc6	displej
(	(	kIx(	(
<g/>
digitální	digitální	k2eAgFnSc1d1	digitální
indikace	indikace	k1gFnSc1	indikace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
krokovým	krokový	k2eAgInSc7d1	krokový
motorkem	motorek	k1gInSc7	motorek
převádí	převádět	k5eAaImIp3nS	převádět
na	na	k7c4	na
ozubená	ozubený	k2eAgNnPc4d1	ozubené
kola	kolo	k1gNnPc4	kolo
a	a	k8xC	a
ručky	ručka	k1gFnPc4	ručka
(	(	kIx(	(
<g/>
analogová	analogový	k2eAgFnSc1d1	analogová
indikace	indikace	k1gFnSc1	indikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Korunka	korunka	k1gFnSc1	korunka
slouží	sloužit	k5eAaImIp3nS	sloužit
jen	jen	k9	jen
pro	pro	k7c4	pro
nastavení	nastavení	k1gNnSc4	nastavení
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Elektronické	elektronický	k2eAgFnPc1d1	elektronická
hodinky	hodinka	k1gFnPc1	hodinka
mají	mít	k5eAaImIp3nP	mít
podstatně	podstatně	k6eAd1	podstatně
pravidelnější	pravidelní	k2eAgInSc4d2	pravidelní
chod	chod	k1gInSc4	chod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
poloze	poloha	k1gFnSc6	poloha
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
se	se	k3xPyFc4	se
natahovat	natahovat	k5eAaImF	natahovat
a	a	k8xC	a
při	při	k7c6	při
masové	masový	k2eAgFnSc6d1	masová
výrobě	výroba	k1gFnSc6	výroba
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
levné	levný	k2eAgFnPc1d1	levná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
kapesní	kapesní	k2eAgFnPc1d1	kapesní
hodinky	hodinka	k1gFnPc1	hodinka
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
z	z	k7c2	z
potřeb	potřeba	k1gFnPc2	potřeba
přesnější	přesný	k2eAgFnSc2d2	přesnější
časové	časový	k2eAgFnSc2d1	časová
orientace	orientace	k1gFnSc2	orientace
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1600	[number]	k4	1600
<g/>
.	.	kIx.	.
</s>
<s>
Měly	mít	k5eAaImAgFnP	mít
vejčitý	vejčitý	k2eAgInSc4d1	vejčitý
tvar	tvar	k1gInSc4	tvar
vnějšího	vnější	k2eAgInSc2d1	vnější
bronzového	bronzový	k2eAgInSc2d1	bronzový
pláště	plášť	k1gInSc2	plášť
<g/>
,	,	kIx,	,
rytý	rytý	k2eAgInSc4d1	rytý
ciferník	ciferník	k1gInSc4	ciferník
a	a	k8xC	a
jedinou	jediný	k2eAgFnSc4d1	jediná
hodinovou	hodinový	k2eAgFnSc4d1	hodinová
ručičku	ručička	k1gFnSc4	ručička
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
tvaru	tvar	k1gInSc2	tvar
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
Norimberská	norimberský	k2eAgNnPc4d1	norimberské
vejce	vejce	k1gNnPc4	vejce
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
mylně	mylně	k6eAd1	mylně
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
norimberský	norimberský	k2eAgMnSc1d1	norimberský
hodinář	hodinář	k1gMnSc1	hodinář
Peter	Peter	k1gMnSc1	Peter
Henlein	Henlein	k1gMnSc1	Henlein
<g/>
;	;	kIx,	;
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
patrně	patrně	k6eAd1	patrně
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
až	až	k9	až
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
1542	[number]	k4	1542
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
bohatě	bohatě	k6eAd1	bohatě
zdobené	zdobený	k2eAgFnPc1d1	zdobená
a	a	k8xC	a
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
natažení	natažení	k1gNnSc4	natažení
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
jít	jít	k5eAaImF	jít
až	až	k9	až
40	[number]	k4	40
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Poháněla	pohánět	k5eAaImAgFnS	pohánět
je	být	k5eAaImIp3nS	být
ocelová	ocelový	k2eAgFnSc1d1	ocelová
pružina	pružina	k1gFnSc1	pružina
a	a	k8xC	a
oscilátorem	oscilátor	k1gInSc7	oscilátor
byl	být	k5eAaImAgMnS	být
lihýř	lihýř	k1gMnSc1	lihýř
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
chod	chod	k1gInSc1	chod
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
přesnost	přesnost	k1gFnSc1	přesnost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
velmi	velmi	k6eAd1	velmi
nerovnoměrný	rovnoměrný	k2eNgMnSc1d1	nerovnoměrný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hnací	hnací	k2eAgFnSc1d1	hnací
síla	síla	k1gFnSc1	síla
pružiny	pružina	k1gFnSc2	pružina
postupně	postupně	k6eAd1	postupně
klesá	klesat	k5eAaImIp3nS	klesat
a	a	k8xC	a
protože	protože	k8xS	protože
frekvence	frekvence	k1gFnSc1	frekvence
jejich	jejich	k3xOp3gInSc2	jejich
oscilátoru	oscilátor	k1gInSc2	oscilátor
<g/>
,	,	kIx,	,
lihýře	lihýř	k1gInSc2	lihýř
<g/>
,	,	kIx,	,
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
silně	silně	k6eAd1	silně
závisí	záviset	k5eAaImIp3nS	záviset
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
hodináři	hodinář	k1gMnPc1	hodinář
naučili	naučit	k5eAaPmAgMnP	naučit
tuto	tento	k3xDgFnSc4	tento
vadu	vada	k1gFnSc4	vada
zčásti	zčásti	k6eAd1	zčásti
kompenzovat	kompenzovat	k5eAaBmF	kompenzovat
nesmírně	smírně	k6eNd1	smírně
pracným	pracný	k2eAgInSc7d1	pracný
miniaturním	miniaturní	k2eAgInSc7d1	miniaturní
řetízkem	řetízek	k1gInSc7	řetízek
a	a	k8xC	a
šnekem	šnek	k1gInSc7	šnek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
pružina	pružina	k1gFnSc1	pružina
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
a	a	k8xC	a
slábla	slábnout	k5eAaImAgFnS	slábnout
zmenšoval	zmenšovat	k5eAaImAgMnS	zmenšovat
rameno	rameno	k1gNnSc4	rameno
působící	působící	k2eAgFnSc2d1	působící
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Radikální	radikální	k2eAgNnSc4d1	radikální
zlepšení	zlepšení	k1gNnSc4	zlepšení
přinesla	přinést	k5eAaPmAgFnS	přinést
až	až	k9	až
setrvačka	setrvačka	k1gFnSc1	setrvačka
a	a	k8xC	a
klidové	klidový	k2eAgInPc1d1	klidový
kroky	krok	k1gInPc1	krok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1541	[number]	k4	1541
se	s	k7c7	s
zavedením	zavedení	k1gNnSc7	zavedení
svých	svůj	k3xOyFgFnPc2	svůj
reforem	reforma	k1gFnPc2	reforma
zakázal	zakázat	k5eAaPmAgMnS	zakázat
Jan	Jan	k1gMnSc1	Jan
Kalvín	Kalvín	k1gMnSc1	Kalvín
ve	v	k7c6	v
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
Ženevě	Ženeva	k1gFnSc6	Ženeva
nošení	nošení	k1gNnSc2	nošení
ozdob	ozdoba	k1gFnPc2	ozdoba
a	a	k8xC	a
zlatých	zlatý	k2eAgInPc2d1	zlatý
šperků	šperk	k1gInPc2	šperk
<g/>
.	.	kIx.	.
</s>
<s>
Ženevští	ženevský	k2eAgMnPc1d1	ženevský
zlatníci	zlatník	k1gMnPc1	zlatník
byli	být	k5eAaImAgMnP	být
tak	tak	k6eAd1	tak
nuceni	nutit	k5eAaImNgMnP	nutit
svou	svůj	k3xOyFgFnSc4	svůj
výrobu	výroba	k1gFnSc4	výroba
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
něco	něco	k3yInSc4	něco
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
svým	svůj	k3xOyFgMnPc3	svůj
zákazníkům	zákazník	k1gMnPc3	zákazník
jako	jako	k8xS	jako
alternativu	alternativa	k1gFnSc4	alternativa
šperků	šperk	k1gInPc2	šperk
začali	začít	k5eAaPmAgMnP	začít
nabízet	nabízet	k5eAaImF	nabízet
právě	právě	k9	právě
přenosné	přenosný	k2eAgFnPc4d1	přenosná
hodinky	hodinka	k1gFnPc4	hodinka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
jejich	jejich	k3xOp3gFnSc2	jejich
první	první	k4xOgFnSc2	první
sériové	sériový	k2eAgFnSc2d1	sériová
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1587	[number]	k4	1587
<g/>
,	,	kIx,	,
stál	stát	k5eAaImAgMnS	stát
Francouz	Francouz	k1gMnSc1	Francouz
Charles	Charles	k1gMnSc1	Charles
Cusin	Cusin	k1gMnSc1	Cusin
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
zaregistrováno	zaregistrovat	k5eAaPmNgNnS	zaregistrovat
asi	asi	k9	asi
100	[number]	k4	100
hodinářských	hodinářský	k2eAgMnPc2d1	hodinářský
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
produkovali	produkovat	k5eAaImAgMnP	produkovat
téměř	téměř	k6eAd1	téměř
5000	[number]	k4	5000
hodinek	hodinka	k1gFnPc2	hodinka
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
zaměstnávali	zaměstnávat	k5eAaImAgMnP	zaměstnávat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
kapesní	kapesní	k2eAgFnPc4d1	kapesní
hodinky	hodinka	k1gFnPc4	hodinka
<g/>
,	,	kIx,	,
opatřené	opatřený	k2eAgFnSc2d1	opatřená
řetízkem	řetízek	k1gInSc7	řetízek
<g/>
.	.	kIx.	.
</s>
<s>
Ciferník	ciferník	k1gInSc1	ciferník
již	již	k6eAd1	již
býval	bývat	k5eAaImAgInS	bývat
krytý	krytý	k2eAgInSc1d1	krytý
broušeným	broušený	k2eAgInSc7d1	broušený
křišťálem	křišťál	k1gInSc7	křišťál
<g/>
,	,	kIx,	,
berylem	beryl	k1gInSc7	beryl
nebo	nebo	k8xC	nebo
sklem	sklo	k1gNnSc7	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
pokrok	pokrok	k1gInSc1	pokrok
znamenal	znamenat	k5eAaImAgInS	znamenat
vynález	vynález	k1gInSc4	vynález
setrvačky	setrvačka	k1gFnSc2	setrvačka
(	(	kIx(	(
<g/>
Christian	Christian	k1gMnSc1	Christian
Huygens	Huygensa	k1gFnPc2	Huygensa
<g/>
,	,	kIx,	,
před	před	k7c7	před
1675	[number]	k4	1675
<g/>
)	)	kIx)	)
s	s	k7c7	s
lépe	dobře	k6eAd2	dobře
definovanou	definovaný	k2eAgFnSc7d1	definovaná
vlastní	vlastní	k2eAgFnSc7d1	vlastní
frekvencí	frekvence	k1gFnSc7	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Hodináři	hodinář	k1gMnPc1	hodinář
ale	ale	k9	ale
dlouho	dlouho	k6eAd1	dlouho
preferovali	preferovat	k5eAaImAgMnP	preferovat
pracnější	pracný	k2eAgNnSc4d2	pracnější
řešení	řešení	k1gNnSc4	řešení
–	–	k?	–
vyrovnávání	vyrovnávání	k1gNnSc2	vyrovnávání
síly	síla	k1gFnSc2	síla
pružiny	pružina	k1gFnSc2	pružina
šnekem	šnek	k1gInSc7	šnek
a	a	k8xC	a
řetízkem	řetízek	k1gInSc7	řetízek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
tření	tření	k1gNnSc2	tření
v	v	k7c6	v
čepech	čep	k1gInPc6	čep
se	se	k3xPyFc4	se
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc3	století
dělají	dělat	k5eAaImIp3nP	dělat
ložiska	ložisko	k1gNnPc1	ložisko
z	z	k7c2	z
rubínů	rubín	k1gInPc2	rubín
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
kameny	kámen	k1gInPc1	kámen
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
mořeplavby	mořeplavba	k1gFnSc2	mořeplavba
a	a	k8xC	a
navigace	navigace	k1gFnSc2	navigace
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
naléhavá	naléhavý	k2eAgFnSc1d1	naléhavá
potřeba	potřeba	k1gFnSc1	potřeba
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
přesných	přesný	k2eAgFnPc2d1	přesná
a	a	k8xC	a
spolehlivých	spolehlivý	k2eAgFnPc2d1	spolehlivá
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
námořních	námořní	k2eAgMnPc2d1	námořní
chronometrů	chronometr	k1gInPc2	chronometr
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
umožnily	umožnit	k5eAaPmAgFnP	umožnit
přesnější	přesný	k2eAgNnSc4d2	přesnější
určení	určení	k1gNnSc4	určení
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1730	[number]	k4	1730
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
lodní	lodní	k2eAgMnSc1d1	lodní
tesař	tesař	k1gMnSc1	tesař
John	John	k1gMnSc1	John
Harrison	Harrison	k1gMnSc1	Harrison
model	model	k1gInSc4	model
chronometru	chronometr	k1gInSc2	chronometr
s	s	k7c7	s
úplně	úplně	k6eAd1	úplně
novým	nový	k2eAgInSc7d1	nový
krokem	krok	k1gInSc7	krok
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
chod	chod	k1gInSc1	chod
radikálně	radikálně	k6eAd1	radikálně
zlepšil	zlepšit	k5eAaPmAgInS	zlepšit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
použitelný	použitelný	k2eAgInSc1d1	použitelný
chronometr	chronometr	k1gInSc1	chronometr
sestrojil	sestrojit	k5eAaPmAgInS	sestrojit
roku	rok	k1gInSc2	rok
1760	[number]	k4	1760
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
vývoji	vývoj	k1gInSc6	vývoj
přesných	přesný	k2eAgFnPc2d1	přesná
hodin	hodina	k1gFnPc2	hodina
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
tzv.	tzv.	kA	tzv.
klidové	klidový	k2eAgInPc1d1	klidový
kroky	krok	k1gInPc1	krok
(	(	kIx(	(
<g/>
Grahamův	Grahamův	k2eAgInSc1d1	Grahamův
krok	krok	k1gInSc1	krok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
podstatně	podstatně	k6eAd1	podstatně
méně	málo	k6eAd2	málo
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
oscilátor	oscilátor	k1gInSc4	oscilátor
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
také	také	k6eAd1	také
lepší	dobrý	k2eAgNnSc1d2	lepší
tvarování	tvarování	k1gNnSc1	tvarování
vlásku	vlásek	k1gInSc2	vlásek
(	(	kIx(	(
<g/>
Abraham	Abraham	k1gMnSc1	Abraham
Louis	Louis	k1gMnSc1	Louis
Breguet	Breguet	k1gMnSc1	Breguet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
frekvence	frekvence	k1gFnSc1	frekvence
méně	málo	k6eAd2	málo
závisela	záviset	k5eAaImAgFnS	záviset
na	na	k7c6	na
hnací	hnací	k2eAgFnSc6d1	hnací
síle	síla	k1gFnSc6	síla
i	i	k8xC	i
na	na	k7c6	na
poloze	poloha	k1gFnSc6	poloha
hodinek	hodinka	k1gFnPc2	hodinka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
stejným	stejný	k2eAgInSc7d1	stejný
účelem	účel	k1gInSc7	účel
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
už	už	k6eAd1	už
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tourbillon	tourbillon	k1gInSc1	tourbillon
<g/>
,	,	kIx,	,
složité	složitý	k2eAgNnSc1d1	složité
a	a	k8xC	a
důmyslné	důmyslný	k2eAgNnSc1d1	důmyslné
uspořádání	uspořádání	k1gNnSc1	uspořádání
setrvačky	setrvačka	k1gFnSc2	setrvačka
v	v	k7c6	v
jakési	jakýsi	k3yIgFnSc6	jakýsi
kleci	klec	k1gFnSc6	klec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
pomalu	pomalu	k6eAd1	pomalu
otáčí	otáčet	k5eAaImIp3nS	otáčet
a	a	k8xC	a
tak	tak	k6eAd1	tak
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
nerovnoměrnost	nerovnoměrnost	k1gFnSc1	nerovnoměrnost
chodu	chod	k1gInSc2	chod
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
polohách	poloha	k1gFnPc6	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitní	kvalitní	k2eAgFnPc4d1	kvalitní
hodinky	hodinka	k1gFnPc4	hodinka
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
tenkými	tenký	k2eAgInPc7d1	tenký
čepy	čep	k1gInPc7	čep
setrvačky	setrvačka	k1gFnSc2	setrvačka
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
0,12	[number]	k4	0,12
mm	mm	kA	mm
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
choulostivé	choulostivý	k2eAgFnPc1d1	choulostivá
vůči	vůči	k7c3	vůči
nárazům	náraz	k1gInPc3	náraz
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
zavádí	zavádět	k5eAaImIp3nS	zavádět
pružné	pružný	k2eAgNnSc4d1	pružné
uložení	uložení	k1gNnSc4	uložení
ložisek	ložisko	k1gNnPc2	ložisko
setrvačky	setrvačka	k1gFnSc2	setrvačka
(	(	kIx(	(
<g/>
anti-choc	antihoc	k1gInSc1	anti-choc
<g/>
,	,	kIx,	,
Incabloc	Incabloc	k1gInSc1	Incabloc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
rozběhla	rozběhnout	k5eAaPmAgFnS	rozběhnout
tovární	tovární	k2eAgFnSc1d1	tovární
výroba	výroba	k1gFnSc1	výroba
spolehlivých	spolehlivý	k2eAgFnPc2d1	spolehlivá
a	a	k8xC	a
stále	stále	k6eAd1	stále
levnějších	levný	k2eAgFnPc2d2	levnější
hodinek	hodinka	k1gFnPc2	hodinka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
(	(	kIx(	(
<g/>
Patek	patka	k1gFnPc2	patka
Philippe	Philipp	k1gInSc5	Philipp
<g/>
,	,	kIx,	,
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
i	i	k9	i
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
je	on	k3xPp3gNnSc4	on
všichni	všechen	k3xTgMnPc1	všechen
železničáři	železničář	k1gMnPc1	železničář
<g/>
,	,	kIx,	,
pošťáci	pošťák	k1gMnPc1	pošťák
<g/>
,	,	kIx,	,
důstojníci	důstojník	k1gMnPc1	důstojník
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
zlevnění	zlevnění	k1gNnSc1	zlevnění
přinesl	přinést	k5eAaPmAgInS	přinést
Roskopfův	Roskopfův	k2eAgInSc1d1	Roskopfův
kolíčkový	kolíčkový	k2eAgInSc1d1	kolíčkový
krok	krok	k1gInSc1	krok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
dámské	dámský	k2eAgFnPc4d1	dámská
hodinky	hodinka	k1gFnPc4	hodinka
<g/>
,	,	kIx,	,
nošené	nošený	k2eAgMnPc4d1	nošený
na	na	k7c6	na
šňůrce	šňůrka	k1gFnSc6	šňůrka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
užíval	užívat	k5eAaImAgInS	užívat
cylindrový	cylindrový	k2eAgInSc1d1	cylindrový
krok	krok	k1gInSc1	krok
<g/>
.	.	kIx.	.
</s>
<s>
Lepší	dobrý	k2eAgFnPc1d2	lepší
hodinky	hodinka	k1gFnPc1	hodinka
měly	mít	k5eAaImAgFnP	mít
ještě	ještě	k9	ještě
druhý	druhý	k4xOgInSc4	druhý
plášť	plášť	k1gInSc4	plášť
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zlatý	zlatý	k1gInSc1	zlatý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
chránil	chránit	k5eAaImAgInS	chránit
sklo	sklo	k1gNnSc4	sklo
na	na	k7c6	na
ciferníku	ciferník	k1gInSc6	ciferník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náramkové	náramkový	k2eAgFnPc4d1	náramková
hodinky	hodinka	k1gFnPc4	hodinka
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
firma	firma	k1gFnSc1	firma
Cartier	Cartier	k1gInSc4	Cartier
pro	pro	k7c4	pro
brazilského	brazilský	k2eAgMnSc4d1	brazilský
letce	letec	k1gMnSc4	letec
Santos-Dumonta	Santos-Dumont	k1gInSc2	Santos-Dumont
první	první	k4xOgFnSc2	první
náramkové	náramkový	k2eAgFnSc2d1	náramková
hodinky	hodinka	k1gFnSc2	hodinka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
od	od	k7c2	od
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
rychle	rychle	k6eAd1	rychle
nahradily	nahradit	k5eAaPmAgFnP	nahradit
hodinky	hodinka	k1gFnPc1	hodinka
kapesní	kapesní	k2eAgFnPc1d1	kapesní
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
v	v	k7c6	v
nejrůznějších	různý	k2eAgInPc6d3	nejrůznější
tvarech	tvar	k1gInPc6	tvar
a	a	k8xC	a
designu	design	k1gInSc6	design
<g/>
,	,	kIx,	,
od	od	k7c2	od
levných	levný	k2eAgInPc2d1	levný
kolíčkových	kolíčkový	k2eAgInPc2d1	kolíčkový
až	až	k9	až
po	po	k7c4	po
velmi	velmi	k6eAd1	velmi
drahé	drahý	k2eAgFnPc4d1	drahá
a	a	k8xC	a
technicky	technicky	k6eAd1	technicky
dokonalé	dokonalý	k2eAgInPc1d1	dokonalý
stroje	stroj	k1gInPc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
hodinky	hodinka	k1gFnPc1	hodinka
s	s	k7c7	s
automatickým	automatický	k2eAgNnSc7d1	automatické
natahováním	natahování	k1gNnSc7	natahování
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
excentrickým	excentrický	k2eAgInSc7d1	excentrický
segmentem	segment	k1gInSc7	segment
(	(	kIx(	(
<g/>
rotor	rotor	k1gInSc1	rotor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
ruky	ruka	k1gFnSc2	ruka
otáčí	otáčet	k5eAaImIp3nS	otáčet
a	a	k8xC	a
natahuje	natahovat	k5eAaImIp3nS	natahovat
pero	pero	k1gNnSc1	pero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Náramkové	náramkový	k2eAgFnPc1d1	náramková
hodinky	hodinka	k1gFnPc1	hodinka
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nP	nosit
na	na	k7c4	na
zápěstí	zápěstí	k1gNnSc4	zápěstí
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
náramek	náramek	k1gInSc1	náramek
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
opatřeny	opatřen	k2eAgInPc1d1	opatřen
páskem	pásek	k1gInSc7	pásek
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
tahem	tah	k1gInSc7	tah
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
připnou	připnout	k5eAaPmIp3nP	připnout
na	na	k7c4	na
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
levou	levý	k2eAgFnSc4d1	levá
<g/>
)	)	kIx)	)
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Pásek	pásek	k1gMnSc1	pásek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
materiálů	materiál	k1gInPc2	materiál
(	(	kIx(	(
<g/>
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
umělá	umělý	k2eAgFnSc1d1	umělá
hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
kaučuk	kaučuk	k1gInSc1	kaučuk
<g/>
,	,	kIx,	,
nerezová	rezový	k2eNgFnSc1d1	nerezová
ocel	ocel	k1gFnSc1	ocel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
otevřený	otevřený	k2eAgInSc1d1	otevřený
(	(	kIx(	(
<g/>
rozpojitelný	rozpojitelný	k2eAgInSc1d1	rozpojitelný
<g/>
)	)	kIx)	)
či	či	k8xC	či
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
(	(	kIx(	(
<g/>
pružný	pružný	k2eAgInSc1d1	pružný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nastavit	nastavit	k5eAaPmF	nastavit
délku	délka	k1gFnSc4	délka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vůle	vůle	k1gFnSc1	vůle
hodinek	hodinka	k1gFnPc2	hodinka
na	na	k7c6	na
zápěstí	zápěstí	k1gNnSc6	zápěstí
vyhovovala	vyhovovat	k5eAaImAgNnP	vyhovovat
nositeli	nositel	k1gMnSc3	nositel
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
velikostí	velikost	k1gFnSc7	velikost
a	a	k8xC	a
hmotností	hmotnost	k1gFnSc7	hmotnost
jsou	být	k5eAaImIp3nP	být
uzpůsobeny	uzpůsobit	k5eAaPmNgInP	uzpůsobit
k	k	k7c3	k
nošení	nošení	k1gNnSc3	nošení
na	na	k7c4	na
zápěstí	zápěstí	k1gNnSc4	zápěstí
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgInPc4d2	menší
a	a	k8xC	a
lehčí	lehký	k2eAgInPc4d2	lehčí
než	než	k8xS	než
jiné	jiný	k2eAgInPc4d1	jiný
typy	typ	k1gInPc4	typ
hodinek	hodinka	k1gFnPc2	hodinka
<g/>
,	,	kIx,	,
odborníci	odborník	k1gMnPc1	odborník
zejména	zejména	k9	zejména
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
hodně	hodně	k6eAd1	hodně
tenké	tenký	k2eAgInPc1d1	tenký
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
stacionárními	stacionární	k2eAgMnPc7d1	stacionární
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
třeba	třeba	k9	třeba
kapesními	kapesní	k2eAgFnPc7d1	kapesní
<g/>
)	)	kIx)	)
hodinkami	hodinka	k1gFnPc7	hodinka
jsou	být	k5eAaImIp3nP	být
konstruovány	konstruovat	k5eAaImNgInP	konstruovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odolávaly	odolávat	k5eAaImAgFnP	odolávat
silnějším	silný	k2eAgInPc3d2	silnější
otřesům	otřes	k1gInPc3	otřes
a	a	k8xC	a
dalším	další	k2eAgInPc3d1	další
mechanickým	mechanický	k2eAgInPc3d1	mechanický
vlivům	vliv	k1gInPc3	vliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hodinky	hodinka	k1gFnPc4	hodinka
dnes	dnes	k6eAd1	dnes
==	==	k?	==
</s>
</p>
<p>
<s>
Elektronické	elektronický	k2eAgFnPc4d1	elektronická
(	(	kIx(	(
<g/>
digitální	digitální	k2eAgInSc4d1	digitální
<g/>
,	,	kIx,	,
quartzové	quartzové	k2eAgInSc4d1	quartzové
<g/>
)	)	kIx)	)
náramkové	náramkový	k2eAgFnPc1d1	náramková
hodinky	hodinka	k1gFnPc1	hodinka
s	s	k7c7	s
ručkovou	ručkový	k2eAgFnSc7d1	Ručková
indikací	indikace	k1gFnSc7	indikace
představují	představovat	k5eAaImIp3nP	představovat
dnes	dnes	k6eAd1	dnes
nejběžnější	běžný	k2eAgFnSc4d3	nejběžnější
podobu	podoba	k1gFnSc4	podoba
hodinek	hodinka	k1gFnPc2	hodinka
pro	pro	k7c4	pro
běžné	běžný	k2eAgNnSc4d1	běžné
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Lepší	dobrý	k2eAgFnPc1d2	lepší
hodinky	hodinka	k1gFnPc1	hodinka
mají	mít	k5eAaImIp3nP	mít
vodotěsné	vodotěsný	k2eAgNnSc4d1	vodotěsné
pouzdro	pouzdro	k1gNnSc4	pouzdro
<g/>
,	,	kIx,	,
safírové	safírový	k2eAgNnSc4d1	safírové
sklo	sklo	k1gNnSc4	sklo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nepoškrábe	poškrábat	k5eNaPmIp3nS	poškrábat
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
certifikát	certifikát	k1gInSc4	certifikát
přesnosti	přesnost	k1gFnSc2	přesnost
chodu	chod	k1gInSc2	chod
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
však	však	k9	však
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
množství	množství	k1gNnSc1	množství
luxusních	luxusní	k2eAgFnPc2d1	luxusní
hodinek	hodinka	k1gFnPc2	hodinka
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ve	v	k7c6	v
zlatém	zlatý	k2eAgNnSc6d1	Zlaté
pouzdře	pouzdro	k1gNnSc6	pouzdro
s	s	k7c7	s
diamanty	diamant	k1gInPc7	diamant
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dávají	dávat	k5eAaImIp3nP	dávat
najevo	najevo	k6eAd1	najevo
společenský	společenský	k2eAgInSc4d1	společenský
status	status	k1gInSc4	status
svého	svůj	k3xOyFgMnSc2	svůj
nositele	nositel	k1gMnSc2	nositel
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitní	kvalitní	k2eAgFnPc1d1	kvalitní
mechanické	mechanický	k2eAgFnPc1d1	mechanická
hodinky	hodinka	k1gFnPc1	hodinka
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nP	těšit
velké	velký	k2eAgFnSc3d1	velká
oblibě	obliba	k1gFnSc3	obliba
sběratelů	sběratel	k1gMnPc2	sběratel
a	a	k8xC	a
milovníků	milovník	k1gMnPc2	milovník
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
těší	těšit	k5eAaImIp3nS	těšit
technická	technický	k2eAgFnSc1d1	technická
dokonalost	dokonalost	k1gFnSc1	dokonalost
a	a	k8xC	a
krása	krása	k1gFnSc1	krása
strojků	strojek	k1gInPc2	strojek
<g/>
,	,	kIx,	,
důmyslná	důmyslný	k2eAgFnSc1d1	důmyslná
konstrukce	konstrukce	k1gFnSc1	konstrukce
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
i	i	k9	i
výroba	výroba	k1gFnSc1	výroba
replik	replika	k1gFnPc2	replika
a	a	k8xC	a
"	"	kIx"	"
<g/>
nostalgií	nostalgie	k1gFnSc7	nostalgie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
napodobenin	napodobenina	k1gFnPc2	napodobenina
starších	starý	k2eAgFnPc2d2	starší
hodinek	hodinka	k1gFnPc2	hodinka
s	s	k7c7	s
vynikajícími	vynikající	k2eAgInPc7d1	vynikající
parametry	parametr	k1gInPc7	parametr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vracejí	vracet	k5eAaImIp3nP	vracet
i	i	k9	i
tzv.	tzv.	kA	tzv.
komplikace	komplikace	k1gFnPc1	komplikace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Ukazují	ukazovat	k5eAaImIp3nP	ukazovat
třeba	třeba	k6eAd1	třeba
fáze	fáze	k1gFnPc4	fáze
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
budík	budík	k1gInSc4	budík
<g/>
,	,	kIx,	,
hodinové	hodinový	k2eAgInPc1d1	hodinový
nebo	nebo	k8xC	nebo
i	i	k9	i
minutové	minutový	k2eAgNnSc4d1	minutové
bití	bití	k1gNnSc4	bití
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Segment	segment	k1gInSc1	segment
luxusních	luxusní	k2eAgFnPc2d1	luxusní
hodinek	hodinka	k1gFnPc2	hodinka
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
patří	patřit	k5eAaImIp3nS	patřit
převážně	převážně	k6eAd1	převážně
švýcarským	švýcarský	k2eAgFnPc3d1	švýcarská
značkám	značka	k1gFnPc3	značka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyšší	vysoký	k2eAgFnSc6d2	vyšší
třídě	třída	k1gFnSc6	třída
luxusních	luxusní	k2eAgFnPc2d1	luxusní
hodinek	hodinka	k1gFnPc2	hodinka
se	se	k3xPyFc4	se
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
žádné	žádný	k3yNgInPc1	žádný
digitální	digitální	k2eAgInPc1d1	digitální
displeje	displej	k1gInPc1	displej
ani	ani	k8xC	ani
quartz	quartz	k1gMnSc1	quartz
strojky	strojek	k1gInPc4	strojek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
mechanické	mechanický	k2eAgFnPc4d1	mechanická
hodinky	hodinka	k1gFnPc4	hodinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Automaty	automat	k1gInPc1	automat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
slangově	slangově	k6eAd1	slangově
hodinkám	hodinka	k1gFnPc3	hodinka
s	s	k7c7	s
automatickým	automatický	k2eAgInSc7d1	automatický
nátahem	nátah	k1gInSc7	nátah
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
variantou	varianta	k1gFnSc7	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Opravdu	opravdu	k6eAd1	opravdu
luxusní	luxusní	k2eAgFnPc1d1	luxusní
hodinky	hodinka	k1gFnPc1	hodinka
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
mechanismem	mechanismus	k1gInSc7	mechanismus
tourbillon	tourbillon	k1gInSc1	tourbillon
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
inovovanou	inovovaný	k2eAgFnSc7d1	inovovaná
verzí	verze	k1gFnSc7	verze
letájícím	letájící	k1gMnSc7	letájící
tourbillonem	tourbillon	k1gInSc7	tourbillon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokrok	pokrok	k1gInSc1	pokrok
se	se	k3xPyFc4	se
nedotýká	dotýkat	k5eNaImIp3nS	dotýkat
jen	jen	k9	jen
technologie	technologie	k1gFnSc1	technologie
výroby	výroba	k1gFnSc2	výroba
hodinových	hodinový	k2eAgInPc2d1	hodinový
strojků	strojek	k1gInPc2	strojek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnoho	mnoho	k4c1	mnoho
výrobců	výrobce	k1gMnPc2	výrobce
zkouší	zkoušet	k5eAaImIp3nS	zkoušet
využívat	využívat	k5eAaPmF	využívat
nové	nový	k2eAgInPc4d1	nový
materiály	materiál	k1gInPc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
titan	titan	k1gInSc1	titan
na	na	k7c4	na
pouzdra	pouzdro	k1gNnPc4	pouzdro
a	a	k8xC	a
tahy	tah	k1gInPc4	tah
<g/>
,	,	kIx,	,
kompozit	kompozitum	k1gNnPc2	kompozitum
uhlíkových	uhlíkový	k2eAgFnPc2d1	uhlíková
vláken	vlákna	k1gFnPc2	vlákna
<g/>
,	,	kIx,	,
směs	směsa	k1gFnPc2	směsa
plastu	plast	k1gInSc2	plast
a	a	k8xC	a
skelných	skelný	k2eAgNnPc2d1	skelné
vláken	vlákno	k1gNnPc2	vlákno
nebo	nebo	k8xC	nebo
mořené	mořený	k2eAgNnSc1d1	mořené
exotické	exotický	k2eAgNnSc1d1	exotické
dřevo	dřevo	k1gNnSc1	dřevo
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
pouzder	pouzdro	k1gNnPc2	pouzdro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chytré	chytrý	k2eAgFnPc4d1	chytrá
hodinky	hodinka	k1gFnPc4	hodinka
===	===	k?	===
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
novinkou	novinka	k1gFnSc7	novinka
byly	být	k5eAaImAgFnP	být
křemenné	křemenný	k2eAgFnPc1d1	křemenná
hodinky	hodinka	k1gFnPc1	hodinka
(	(	kIx(	(
<g/>
quartz	quartz	k1gInSc1	quartz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
japonskou	japonský	k2eAgFnSc7d1	japonská
firmou	firma	k1gFnSc7	firma
Seiko	Seiko	k1gNnSc4	Seiko
<g/>
.	.	kIx.	.
</s>
<s>
Indikace	indikace	k1gFnSc1	indikace
byla	být	k5eAaImAgFnS	být
zprvu	zprvu	k6eAd1	zprvu
také	také	k9	také
číslicová	číslicový	k2eAgFnSc1d1	číslicová
<g/>
,	,	kIx,	,
displejem	displej	k1gInSc7	displej
se	s	k7c7	s
svítícími	svítící	k2eAgFnPc7d1	svítící
diodami	dioda	k1gFnPc7	dioda
(	(	kIx(	(
<g/>
LED	LED	kA	LED
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
kapalnými	kapalný	k2eAgInPc7d1	kapalný
krystaly	krystal	k1gInPc7	krystal
(	(	kIx(	(
<g/>
LCD	LCD	kA	LCD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
více	hodně	k6eAd2	hodně
prosadily	prosadit	k5eAaPmAgFnP	prosadit
elektronické	elektronický	k2eAgFnPc1d1	elektronická
hodinky	hodinka	k1gFnPc1	hodinka
s	s	k7c7	s
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
indikací	indikace	k1gFnSc7	indikace
ručkami	ručka	k1gFnPc7	ručka
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc7d1	poslední
novinkou	novinka	k1gFnSc7	novinka
jsou	být	k5eAaImIp3nP	být
elektronické	elektronický	k2eAgFnPc1d1	elektronická
hodinky	hodinka	k1gFnPc1	hodinka
s	s	k7c7	s
automatickou	automatický	k2eAgFnSc7d1	automatická
rádiovou	rádiový	k2eAgFnSc7d1	rádiová
synchronizací	synchronizace	k1gFnSc7	synchronizace
(	(	kIx(	(
<g/>
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	s	k7c7	s
stanicí	stanice	k1gFnSc7	stanice
DCF	DCF	kA	DCF
<g/>
77	[number]	k4	77
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
automatickou	automatický	k2eAgFnSc7d1	automatická
synchronizací	synchronizace	k1gFnSc7	synchronizace
pomocí	pomocí	k7c2	pomocí
signálu	signál	k1gInSc2	signál
GPS	GPS	kA	GPS
(	(	kIx(	(
<g/>
Seiko	Seiko	k1gNnSc1	Seiko
Astron	Astron	k1gInSc1	Astron
<g/>
,	,	kIx,	,
Citizen	Citizen	kA	Citizen
Satellite	Satellit	k1gInSc5	Satellit
Wave	Wave	k1gNnPc6	Wave
<g/>
,	,	kIx,	,
Casio	Casio	k6eAd1	Casio
GPS	GPS	kA	GPS
HYBRID	hybrida	k1gFnPc2	hybrida
WAVE	WAVE	kA	WAVE
CEPTOR	CEPTOR	kA	CEPTOR
–	–	k?	–
kombinace	kombinace	k1gFnSc2	kombinace
rádiové	rádiový	k2eAgFnSc2d1	rádiová
synchronizace	synchronizace	k1gFnSc2	synchronizace
a	a	k8xC	a
GPS	GPS	kA	GPS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
chyby	chyba	k1gFnPc1	chyba
chodu	chod	k1gInSc2	chod
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
nanosekund	nanosekunda	k1gFnPc2	nanosekunda
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
japonské	japonský	k2eAgInPc1d1	japonský
modely	model	k1gInPc1	model
hodinek	hodinka	k1gFnPc2	hodinka
mají	mít	k5eAaImIp3nP	mít
napájení	napájení	k1gNnPc1	napájení
pomocí	pomocí	k7c2	pomocí
solární	solární	k2eAgFnSc2d1	solární
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dobíjí	dobíjet	k5eAaImIp3nS	dobíjet
vestavěný	vestavěný	k2eAgInSc4d1	vestavěný
akumulátor	akumulátor	k1gInSc4	akumulátor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
životnost	životnost	k1gFnSc4	životnost
bývá	bývat	k5eAaImIp3nS	bývat
kolem	kolem	k7c2	kolem
10	[number]	k4	10
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
Seiko	Seiko	k1gNnSc1	Seiko
Solar	Solara	k1gFnPc2	Solara
<g/>
,	,	kIx,	,
Citizen	Citizen	kA	Citizen
Eco-drive	Ecoriev	k1gFnPc1	Eco-driev
<g/>
,	,	kIx,	,
Casio	Casio	k1gMnSc1	Casio
Tough-solar	Tougholar	k1gMnSc1	Tough-solar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Seiko	Seiko	k1gNnSc1	Seiko
taktéž	taktéž	k?	taktéž
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
variantu	varianta	k1gFnSc4	varianta
quartzových	quartzův	k2eAgFnPc2d1	quartzův
hodinek	hodinka	k1gFnPc2	hodinka
s	s	k7c7	s
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
dobíjením	dobíjení	k1gNnSc7	dobíjení
(	(	kIx(	(
<g/>
Seiko	Seiko	k1gNnSc1	Seiko
Kinetic	Kinetice	k1gFnPc2	Kinetice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
zlomovým	zlomový	k2eAgInSc7d1	zlomový
pro	pro	k7c4	pro
klasické	klasický	k2eAgFnPc4d1	klasická
hodinářské	hodinářský	k2eAgFnPc4d1	hodinářská
společnosti	společnost	k1gFnPc4	společnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
představeny	představit	k5eAaPmNgFnP	představit
hodinky	hodinka	k1gFnPc1	hodinka
Apple	Apple	kA	Apple
Watch	Watch	k1gInSc1	Watch
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
tradiční	tradiční	k2eAgMnPc1d1	tradiční
výrobci	výrobce	k1gMnPc1	výrobce
luxusních	luxusní	k2eAgFnPc2d1	luxusní
hodinek	hodinka	k1gFnPc2	hodinka
nemusejí	muset	k5eNaImIp3nP	muset
o	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
obávat	obávat	k5eAaImF	obávat
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
značek	značka	k1gFnPc2	značka
se	se	k3xPyFc4	se
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
trhu	trh	k1gInSc2	trh
moderních	moderní	k2eAgFnPc2d1	moderní
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
nabídnout	nabídnout	k5eAaPmF	nabídnout
luxusní	luxusní	k2eAgFnPc4d1	luxusní
hodinky	hodinka	k1gFnPc4	hodinka
s	s	k7c7	s
moderními	moderní	k2eAgFnPc7d1	moderní
funkcemi	funkce	k1gFnPc7	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
byla	být	k5eAaImAgFnS	být
hodinářská	hodinářský	k2eAgFnSc1d1	hodinářská
společnost	společnost	k1gFnSc1	společnost
Frédérique	Frédérique	k1gFnPc2	Frédérique
Constant	Constant	k1gMnSc1	Constant
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Breitling	Breitling	k1gInSc1	Breitling
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
modelem	model	k1gInSc7	model
chytrých	chytrá	k1gFnPc2	chytrá
hodinek	hodinka	k1gFnPc2	hodinka
Breitling	Breitling	k1gInSc1	Breitling
B55	B55	k1gMnSc1	B55
Connected	Connected	k1gMnSc1	Connected
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
M.	M.	kA	M.
Hajn	Hajn	k1gMnSc1	Hajn
<g/>
,	,	kIx,	,
Základy	základ	k1gInPc1	základ
jemné	jemný	k2eAgFnSc2d1	jemná
mechaniky	mechanika	k1gFnSc2	mechanika
a	a	k8xC	a
hodinářství	hodinářství	k1gNnSc2	hodinářství
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
1953	[number]	k4	1953
</s>
</p>
<p>
<s>
Radko	Radka	k1gFnSc5	Radka
Kynčl	Kynčl	k1gMnSc1	Kynčl
<g/>
,	,	kIx,	,
Hodiny	hodina	k1gFnPc1	hodina
a	a	k8xC	a
hodinky	hodinka	k1gFnPc1	hodinka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Radko	Radka	k1gFnSc5	Radka
Kynčl	Kynčl	k1gMnSc1	Kynčl
<g/>
,	,	kIx,	,
Illustriertes	Illustriertes	k1gMnSc1	Illustriertes
Lexikon	lexikon	k1gInSc1	lexikon
der	drát	k5eAaImRp2nS	drát
Uhren	Uhren	k1gInSc4	Uhren
<g/>
.	.	kIx.	.
</s>
<s>
Egglofsheim	Egglofsheim	k6eAd1	Egglofsheim
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Michal	Michal	k1gMnSc1	Michal
<g/>
,	,	kIx,	,
Hodiny	hodina	k1gFnPc1	hodina
<g/>
.	.	kIx.	.
</s>
<s>
SNTL	SNTL	kA	SNTL
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
Čas	čas	k1gInSc1	čas
a	a	k8xC	a
rytmus	rytmus	k1gInSc1	rytmus
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Oikúmené	Oikúmený	k2eAgNnSc1d1	Oikúmené
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kap	kap	k1gInSc1	kap
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
Urešová	Urešová	k1gFnSc1	Urešová
<g/>
,	,	kIx,	,
Hodiny	hodina	k1gFnPc1	hodina
a	a	k8xC	a
hodinky	hodinka	k1gFnPc1	hodinka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
DCF77	DCF77	k4	DCF77
</s>
</p>
<p>
<s>
Hodiny	hodina	k1gFnPc1	hodina
</s>
</p>
<p>
<s>
Chronograf	chronograf	k1gMnSc1	chronograf
</s>
</p>
<p>
<s>
Chronometr	chronometr	k1gInSc1	chronometr
</s>
</p>
<p>
<s>
Měření	měření	k1gNnSc1	měření
času	čas	k1gInSc2	čas
</s>
</p>
<p>
<s>
Setrvačka	setrvačka	k1gFnSc1	setrvačka
</s>
</p>
<p>
<s>
Stopky	stopka	k1gFnPc1	stopka
</s>
</p>
<p>
<s>
Smartwatch	Smartwatch	k1gMnSc1	Smartwatch
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hodinky	hodinka	k1gFnSc2	hodinka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hodinky	hodinka	k1gFnSc2	hodinka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Rozebírání	rozebírání	k1gNnPc1	rozebírání
hodinek	hodinka	k1gFnPc2	hodinka
<g/>
,	,	kIx,	,
Horlogerie-Suisse	Horlogerie-Suisse	k1gFnSc1	Horlogerie-Suisse
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Sestavování	sestavování	k1gNnPc1	sestavování
hodinek	hodinka	k1gFnPc2	hodinka
<g/>
,	,	kIx,	,
Horlogerie-Suisse	Horlogerie-Suisse	k1gFnSc1	Horlogerie-Suisse
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Funkce	funkce	k1gFnSc1	funkce
mechanických	mechanický	k2eAgFnPc2d1	mechanická
hodinek	hodinka	k1gFnPc2	hodinka
<g/>
,	,	kIx,	,
Horlogerie-Suisse	Horlogerie-Suisse	k1gFnSc1	Horlogerie-Suisse
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Explanations	Explanations	k1gInSc1	Explanations
Of	Of	k1gFnSc2	Of
The	The	k1gFnSc2	The
Mechanical	Mechanical	k1gFnPc1	Mechanical
Movements	Movements	k1gInSc4	Movements
In	In	k1gMnPc2	In
A	a	k8xC	a
Watch	Watcha	k1gFnPc2	Watcha
<g/>
,	,	kIx,	,
TimeZone	TimeZon	k1gMnSc5	TimeZon
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
