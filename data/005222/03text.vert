<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
obdobím	období	k1gNnSc7	období
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Nového	Nového	k2eAgInSc2d1	Nového
světa	svět	k1gInSc2	svět
Španěly	Španěly	k1gInPc1	Španěly
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
období	období	k1gNnSc6	období
staré	starý	k2eAgFnSc2d1	stará
mayské	mayský	k2eAgFnSc2d1	mayská
civilizace	civilizace	k1gFnSc2	civilizace
čtěte	číst	k5eAaImRp2nP	číst
článek	článek	k1gInSc1	článek
Mayská	mayský	k2eAgFnSc1d1	mayská
civilizace	civilizace	k1gFnSc1	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Mayové	May	k1gMnPc1	May
jsou	být	k5eAaImIp3nP	být
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
jižního	jižní	k2eAgNnSc2d1	jižní
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
maya	maya	k6eAd1	maya
označuje	označovat	k5eAaImIp3nS	označovat
obyvatele	obyvatel	k1gMnPc4	obyvatel
regionu	region	k1gInSc2	region
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
určité	určitý	k2eAgFnPc4d1	určitá
podobné	podobný	k2eAgFnPc4d1	podobná
kulturní	kulturní	k2eAgFnPc4d1	kulturní
a	a	k8xC	a
jazykové	jazykový	k2eAgFnPc4d1	jazyková
tradice	tradice	k1gFnPc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
mnoho	mnoho	k4c1	mnoho
rozličných	rozličný	k2eAgFnPc2d1	rozličná
populací	populace	k1gFnPc2	populace
a	a	k8xC	a
komunit	komunita	k1gFnPc2	komunita
i	i	k8xC	i
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
v	v	k7c6	v
regionu	region	k1gInSc6	region
Mezoameriky	Mezoamerika	k1gFnSc2	Mezoamerika
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
částečně	částečně	k6eAd1	částečně
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
kulturu	kultura	k1gFnSc4	kultura
či	či	k8xC	či
historickou	historický	k2eAgFnSc4d1	historická
identitu	identita	k1gFnSc4	identita
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
okolo	okolo	k7c2	okolo
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
potomků	potomek	k1gMnPc2	potomek
Mayů	May	k1gMnPc2	May
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
integrovali	integrovat	k5eAaBmAgMnP	integrovat
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	on	k3xPp3gInPc4	on
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
tradicích	tradice	k1gFnPc6	tradice
a	a	k8xC	a
často	často	k6eAd1	často
mají	mít	k5eAaImIp3nP	mít
jako	jako	k9	jako
mateřštinu	mateřština	k1gFnSc4	mateřština
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mayských	mayský	k2eAgInPc2d1	mayský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
současné	současný	k2eAgFnSc2d1	současná
mayské	mayský	k2eAgFnSc2d1	mayská
populace	populace	k1gFnSc2	populace
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
mexických	mexický	k2eAgInPc6d1	mexický
státech	stát	k1gInPc6	stát
Yucatán	Yucatán	k2eAgMnSc1d1	Yucatán
<g/>
,	,	kIx,	,
Campeche	Campeche	k1gFnSc1	Campeche
<g/>
,	,	kIx,	,
Quintana	Quintana	k1gFnSc1	Quintana
Roo	Roo	k1gFnSc1	Roo
<g/>
,	,	kIx,	,
Tabasco	Tabasco	k1gNnSc1	Tabasco
a	a	k8xC	a
Chiapas	Chiapas	k1gInSc1	Chiapas
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
Mayové	Mayová	k1gFnPc1	Mayová
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
Belize	Belize	k1gFnSc2	Belize
<g/>
,	,	kIx,	,
Guatemala	Guatemala	k1gFnSc1	Guatemala
a	a	k8xC	a
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
provinciích	provincie	k1gFnPc6	provincie
Hondurasu	Honduras	k1gInSc2	Honduras
a	a	k8xC	a
Salvadoru	Salvador	k1gInSc2	Salvador
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jistých	jistý	k2eAgFnPc2d1	jistá
verzí	verze	k1gFnPc2	verze
skončil	skončit	k5eAaPmAgInS	skončit
cyklus	cyklus	k1gInSc1	cyklus
mayského	mayský	k2eAgInSc2d1	mayský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
tento	tento	k3xDgInSc1	tento
fenomén	fenomén	k1gInSc1	fenomén
byl	být	k5eAaImAgInS	být
relativně	relativně	k6eAd1	relativně
často	často	k6eAd1	často
zmiňován	zmiňován	k2eAgInSc1d1	zmiňován
médii	médium	k1gNnPc7	médium
a	a	k8xC	a
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
skupina	skupina	k1gFnSc1	skupina
moderních	moderní	k2eAgMnPc2d1	moderní
Mayů	May	k1gMnPc2	May
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Yucatán	Yucatán	k2eAgInSc1d1	Yucatán
<g/>
.	.	kIx.	.
</s>
<s>
Sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c4	za
Maye	May	k1gMnPc4	May
<g/>
.	.	kIx.	.
</s>
<s>
Nepohlížejí	pohlížet	k5eNaImIp3nP	pohlížet
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
jako	jako	k8xS	jako
na	na	k7c4	na
kmenovou	kmenový	k2eAgFnSc4d1	kmenová
komunitu	komunita	k1gFnSc4	komunita
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgInSc3	ten
běžné	běžný	k2eAgFnPc4d1	běžná
například	například	k6eAd1	například
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Guatemale	Guatemala	k1gFnSc6	Guatemala
a	a	k8xC	a
mluví	mluvit	k5eAaImIp3nS	mluvit
yucateckou	yucatecký	k2eAgFnSc7d1	yucatecký
mayštinou	mayština	k1gFnSc7	mayština
<g/>
.	.	kIx.	.
</s>
<s>
Označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
za	za	k7c4	za
yucatecos	yucatecos	k1gInSc4	yucatecos
nebo	nebo	k8xC	nebo
jednodušeji	jednoduše	k6eAd2	jednoduše
mayas	mayasa	k1gFnPc2	mayasa
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
také	také	k9	také
mluví	mluvit	k5eAaImIp3nS	mluvit
španělsky	španělsky	k6eAd1	španělsky
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
částech	část	k1gFnPc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
postupně	postupně	k6eAd1	postupně
jazyk	jazyk	k1gInSc4	jazyk
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
a	a	k8xC	a
maysky	maysky	k6eAd1	maysky
mluví	mluvit	k5eAaImIp3nS	mluvit
řídce	řídce	k6eAd1	řídce
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
identifikují	identifikovat	k5eAaBmIp3nP	identifikovat
jako	jako	k9	jako
míšenci	míšenec	k1gMnPc1	míšenec
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
probíhá	probíhat	k5eAaImIp3nS	probíhat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
větší	veliký	k2eAgFnSc1d2	veliký
asimilace	asimilace	k1gFnSc1	asimilace
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
mexickou	mexický	k2eAgFnSc7d1	mexická
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
Mayů	May	k1gMnPc2	May
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
poloostrova	poloostrov	k1gInSc2	poloostrov
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
integrována	integrovat	k5eAaBmNgFnS	integrovat
s	s	k7c7	s
hispánskou	hispánský	k2eAgFnSc7d1	hispánská
kulturou	kultura	k1gFnSc7	kultura
než	než	k8xS	než
ta	ten	k3xDgFnSc1	ten
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
revolta	revolta	k1gFnSc1	revolta
yucateckých	yucatecký	k2eAgMnPc2d1	yucatecký
Mayů	May	k1gMnPc2	May
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
Válka	válka	k1gFnSc1	válka
kast	kasta	k1gFnPc2	kasta
(	(	kIx(	(
<g/>
Guerra	Guerra	k1gFnSc1	Guerra
de	de	k?	de
castas	castas	k1gInSc1	castas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
revolt	revolta	k1gFnPc2	revolta
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
dočasná	dočasný	k2eAgFnSc1d1	dočasná
existence	existence	k1gFnSc1	existence
mayského	mayský	k2eAgInSc2d1	mayský
státu	stát	k1gInSc2	stát
Chan	Chan	k1gMnSc1	Chan
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
<g/>
,	,	kIx,	,
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
jako	jako	k8xC	jako
nezávislý	závislý	k2eNgInSc1d1	nezávislý
národ	národ	k1gInSc1	národ
Britským	britský	k2eAgNnSc7d1	Britské
impériem	impérium	k1gNnSc7	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
turismu	turismus	k1gInSc2	turismus
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Karibského	karibský	k2eAgNnSc2d1	Karibské
moře	moře	k1gNnSc2	moře
v	v	k7c6	v
resortu	resort	k1gInSc6	resort
Quintana	Quintan	k1gMnSc2	Quintan
Roo	Roo	k1gMnSc2	Roo
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Cancún	Cancún	k1gInSc1	Cancún
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
cílené	cílený	k2eAgFnSc2d1	cílená
imigrace	imigrace	k1gFnSc2	imigrace
z	z	k7c2	z
jiných	jiný	k2eAgNnPc2d1	jiné
míst	místo	k1gNnPc2	místo
Mexika	Mexiko	k1gNnSc2	Mexiko
do	do	k7c2	do
regionu	region	k1gInSc2	region
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
kritice	kritika	k1gFnSc3	kritika
mexické	mexický	k2eAgFnSc2d1	mexická
vlády	vláda	k1gFnSc2	vláda
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vláda	vláda	k1gFnSc1	vláda
chce	chtít	k5eAaImIp3nS	chtít
snížit	snížit	k5eAaPmF	snížit
mayskou	mayský	k2eAgFnSc4d1	mayská
populaci	populace	k1gFnSc4	populace
a	a	k8xC	a
identitu	identita	k1gFnSc4	identita
<g/>
.	.	kIx.	.
</s>
<s>
Region	region	k1gInSc1	region
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
více	hodně	k6eAd2	hodně
mexickým	mexický	k2eAgInSc7d1	mexický
a	a	k8xC	a
pro	pro	k7c4	pro
vládu	vláda	k1gFnSc4	vláda
méně	málo	k6eAd2	málo
problémovým	problémový	k2eAgInSc7d1	problémový
<g/>
.	.	kIx.	.
</s>
<s>
Chiapas	Chiapas	k1gInSc1	Chiapas
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
poslední	poslední	k2eAgFnSc7d1	poslední
částí	část	k1gFnSc7	část
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nedotkly	dotknout	k5eNaPmAgFnP	dotknout
reformy	reforma	k1gFnPc1	reforma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
přinesla	přinést	k5eAaPmAgFnS	přinést
mexická	mexický	k2eAgFnSc1d1	mexická
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Mayů	May	k1gMnPc2	May
zde	zde	k6eAd1	zde
podporuje	podporovat	k5eAaImIp3nS	podporovat
Zapatovu	Zapatův	k2eAgFnSc4d1	Zapatova
armádu	armáda	k1gFnSc4	armáda
národního	národní	k2eAgNnSc2d1	národní
osvobození	osvobození	k1gNnSc2	osvobození
(	(	kIx(	(
<g/>
Ejército	Ejércit	k2eAgNnSc1d1	Ejército
Zapatista	zapatista	k1gMnSc1	zapatista
de	de	k?	de
Liberación	Liberación	k1gMnSc1	Liberación
Nacional	Nacional	k1gMnSc1	Nacional
<g/>
,	,	kIx,	,
EZLN	EZLN	kA	EZLN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mayské	mayský	k2eAgFnPc1d1	mayská
skupiny	skupina	k1gFnPc1	skupina
v	v	k7c6	v
Chiapasu	Chiapas	k1gInSc6	Chiapas
jsou	být	k5eAaImIp3nP	být
Tzotzilové	Tzotzil	k1gMnPc1	Tzotzil
a	a	k8xC	a
Tzeltalové	Tzeltal	k1gMnPc1	Tzeltal
<g/>
,	,	kIx,	,
v	v	k7c6	v
džungli	džungle	k1gFnSc6	džungle
pak	pak	k6eAd1	pak
Ch	Ch	kA	Ch
<g/>
'	'	kIx"	'
<g/>
olové	olový	k2eAgFnPc1d1	olový
a	a	k8xC	a
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
komunity	komunita	k1gFnSc2	komunita
Tojolabal	Tojolabal	k1gInSc1	Tojolabal
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
a	a	k8xC	a
s	s	k7c7	s
největšími	veliký	k2eAgFnPc7d3	veliký
tradicemi	tradice	k1gFnPc7	tradice
jsou	být	k5eAaImIp3nP	být
Mayové	May	k1gMnPc1	May
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Guatemaly	Guatemala	k1gFnSc2	Guatemala
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Guatemale	Guatemala	k1gFnSc6	Guatemala
vedla	vést	k5eAaImAgFnS	vést
španělská	španělský	k2eAgFnSc1d1	španělská
koloniální	koloniální	k2eAgFnSc1d1	koloniální
politika	politika	k1gFnSc1	politika
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
legální	legální	k2eAgFnSc4d1	legální
segregaci	segregace	k1gFnSc4	segregace
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
Mayové	May	k1gMnPc1	May
udrželi	udržet	k5eAaPmAgMnP	udržet
mnoho	mnoho	k4c4	mnoho
tradic	tradice	k1gFnPc2	tradice
a	a	k8xC	a
ovlivnění	ovlivnění	k1gNnSc4	ovlivnění
hispánskou	hispánský	k2eAgFnSc7d1	hispánská
kulturou	kultura	k1gFnSc7	kultura
zde	zde	k6eAd1	zde
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
velké	velký	k2eAgNnSc1d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
kmenová	kmenový	k2eAgFnSc1d1	kmenová
identifikace	identifikace	k1gFnSc1	identifikace
často	často	k6eAd1	často
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
předkolumbovským	předkolumbovský	k2eAgInPc3d1	předkolumbovský
státům	stát	k1gInPc3	stát
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
zde	zde	k6eAd1	zde
pořád	pořád	k6eAd1	pořád
nosí	nosit	k5eAaImIp3nS	nosit
tradiční	tradiční	k2eAgInSc1d1	tradiční
oděv	oděv	k1gInSc1	oděv
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
místní	místní	k2eAgFnSc4d1	místní
příslušnost	příslušnost	k1gFnSc4	příslušnost
<g/>
.	.	kIx.	.
</s>
<s>
Oblečení	oblečení	k1gNnSc1	oblečení
žen	žena	k1gFnPc2	žena
je	být	k5eAaImIp3nS	být
podstatně	podstatně	k6eAd1	podstatně
tradičnější	tradiční	k2eAgNnSc1d2	tradičnější
než	než	k8xS	než
oblečení	oblečení	k1gNnSc1	oblečení
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
muži	muž	k1gMnPc1	muž
přicházejí	přicházet	k5eAaImIp3nP	přicházet
více	hodně	k6eAd2	hodně
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
hispánskou	hispánský	k2eAgFnSc7d1	hispánská
kulturou	kultura	k1gFnSc7	kultura
a	a	k8xC	a
komercí	komerce	k1gFnSc7	komerce
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
nosí	nosit	k5eAaImIp3nP	nosit
šat	šat	k1gInSc4	šat
zvaný	zvaný	k2eAgInSc4d1	zvaný
huipil	huipit	k5eAaPmAgMnS	huipit
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
ovšem	ovšem	k9	ovšem
z	z	k7c2	z
aztéckého	aztécký	k2eAgInSc2d1	aztécký
jazyka	jazyk	k1gInSc2	jazyk
nahuatlu	nahuatl	k1gMnSc3	nahuatl
huipilli	huipill	k1gMnSc3	huipill
-	-	kIx~	-
blusa	blusa	k1gFnSc1	blusa
-	-	kIx~	-
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc4	dva
kusy	kus	k1gInPc4	kus
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgInPc1d1	spojený
na	na	k7c6	na
ramenou	rameno	k1gNnPc6	rameno
a	a	k8xC	a
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
cca	cca	kA	cca
pod	pod	k7c4	pod
kolena	koleno	k1gNnPc4	koleno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mayové	Mayové	k2eAgFnPc1d1	Mayové
Guatemaly	Guatemala	k1gFnPc1	Guatemala
jsou	být	k5eAaImIp3nP	být
Quiché	Quichý	k2eAgFnPc1d1	Quichý
<g/>
,	,	kIx,	,
Mamesové	Mamesový	k2eAgFnPc1d1	Mamesový
<g/>
,	,	kIx,	,
Pocomam	Pocomam	k1gInSc1	Pocomam
<g/>
,	,	kIx,	,
Kakčikelové	Kakčikelové	k2eAgInSc1d1	Kakčikelové
<g/>
,	,	kIx,	,
Ixil	Ixil	k1gInSc1	Ixil
<g/>
,	,	kIx,	,
Kekchi	Kekchi	k1gNnSc1	Kekchi
<g/>
,	,	kIx,	,
Tzutujil	Tzutujil	k1gMnSc1	Tzutujil
<g/>
,	,	kIx,	,
Jacaltec	Jacaltec	k1gMnSc1	Jacaltec
a	a	k8xC	a
Xinca	Xinca	k1gMnSc1	Xinca
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
mayskou	mayský	k2eAgFnSc7d1	mayská
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
Lakandonové	Lakandon	k1gMnPc1	Lakandon
−	−	k?	−
malá	malý	k2eAgFnSc1d1	malá
populace	populace	k1gFnSc1	populace
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
skupinách	skupina	k1gFnPc6	skupina
v	v	k7c6	v
deštných	deštný	k2eAgInPc6d1	deštný
pralesích	prales	k1gInPc6	prales
Chiapasu	Chiapas	k1gInSc2	Chiapas
a	a	k8xC	a
Guatemaly	Guatemala	k1gFnSc2	Guatemala
<g/>
.	.	kIx.	.
</s>
<s>
Mayská	mayský	k2eAgFnSc1d1	mayská
architektura	architektura	k1gFnSc1	architektura
Mayská	mayský	k2eAgFnSc1d1	mayská
civilizace	civilizace	k1gFnSc2	civilizace
Mayské	mayský	k2eAgFnPc4d1	mayská
dějiny	dějiny	k1gFnPc4	dějiny
Mayská	mayský	k2eAgFnSc1d1	mayská
dvacítková	dvacítkový	k2eAgFnSc1d1	dvacítková
soustava	soustava	k1gFnSc1	soustava
Mayské	mayský	k2eAgMnPc4d1	mayský
jazyky	jazyk	k1gMnPc4	jazyk
Mayský	mayský	k2eAgInSc4d1	mayský
kalendář	kalendář	k1gInSc4	kalendář
Mayská	mayský	k2eAgFnSc1d1	mayská
literatura	literatura	k1gFnSc1	literatura
Mayská	mayský	k2eAgFnSc1d1	mayská
mytologie	mytologie	k1gFnSc1	mytologie
Copán	Copán	k1gInSc1	Copán
<g/>
,	,	kIx,	,
Chichén	Chichén	k1gInSc1	Chichén
Itzá	Itzá	k1gFnSc1	Itzá
<g/>
,	,	kIx,	,
Palenque	Palenque	k1gFnSc1	Palenque
<g/>
,	,	kIx,	,
Tikal	tikal	k1gInSc1	tikal
<g/>
,	,	kIx,	,
Quiriguá	Quiriguá	k1gFnSc1	Quiriguá
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mayové	Mayová	k1gFnSc2	Mayová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
jsou	být	k5eAaImIp3nP	být
Mayové	May	k1gMnPc1	May
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Kompletní	kompletní	k2eAgInSc1d1	kompletní
seznam	seznam	k1gInSc1	seznam
národností	národnost	k1gFnPc2	národnost
a	a	k8xC	a
jazyků	jazyk	k1gMnPc2	jazyk
Guatemaly	Guatemala	k1gFnSc2	Guatemala
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Mayové	Mayové	k2eAgNnSc1d1	Mayové
dnes	dnes	k6eAd1	dnes
(	(	kIx(	(
<g/>
Guatemala	Guatemala	k1gFnSc1	Guatemala
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
</s>
