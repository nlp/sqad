<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
líčidlo	líčidlo	k1gNnSc1	líčidlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
zvýraznění	zvýraznění	k1gNnSc4	zvýraznění
a	a	k8xC	a
zpevnění	zpevnění	k1gNnSc4	zpevnění
nehtů	nehet	k1gInPc2	nehet
<g/>
?	?	kIx.	?
</s>
