<s>
Lak	lak	k1gInSc1	lak
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
kosmetického	kosmetický	k2eAgNnSc2d1	kosmetické
líčidla	líčidlo	k1gNnSc2	líčidlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
zvýraznění	zvýraznění	k1gNnSc4	zvýraznění
a	a	k8xC	a
zpevnění	zpevnění	k1gNnSc4	zpevnění
nehtů	nehet	k1gInPc2	nehet
<g/>
,	,	kIx,	,
vytvoření	vytvoření	k1gNnSc3	vytvoření
umělého	umělý	k2eAgInSc2d1	umělý
lesku	lesk	k1gInSc2	lesk
<g/>
,	,	kIx,	,
změně	změna	k1gFnSc3	změna
barvy	barva	k1gFnSc2	barva
či	či	k8xC	či
vytvoření	vytvoření	k1gNnSc2	vytvoření
okrasného	okrasný	k2eAgInSc2d1	okrasný
ornamentu	ornament	k1gInSc2	ornament
<g/>
.	.	kIx.	.
</s>
