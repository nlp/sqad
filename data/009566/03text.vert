<p>
<s>
Lak	lak	k1gInSc1	lak
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
kosmetického	kosmetický	k2eAgNnSc2d1	kosmetické
líčidla	líčidlo	k1gNnSc2	líčidlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
zvýraznění	zvýraznění	k1gNnSc4	zvýraznění
a	a	k8xC	a
zpevnění	zpevnění	k1gNnSc4	zpevnění
nehtů	nehet	k1gInPc2	nehet
<g/>
,	,	kIx,	,
vytvoření	vytvoření	k1gNnSc3	vytvoření
umělého	umělý	k2eAgInSc2d1	umělý
lesku	lesk	k1gInSc2	lesk
<g/>
,	,	kIx,	,
změně	změna	k1gFnSc3	změna
barvy	barva	k1gFnSc2	barva
či	či	k8xC	či
vytvoření	vytvoření	k1gNnSc2	vytvoření
okrasného	okrasný	k2eAgInSc2d1	okrasný
ornamentu	ornament	k1gInSc2	ornament
<g/>
.	.	kIx.	.
</s>
<s>
Aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
malého	malý	k2eAgInSc2d1	malý
štětečku	štěteček	k1gInSc2	štěteček
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
prsty	prst	k1gInPc4	prst
na	na	k7c6	na
rukou	ruka	k1gFnPc6	ruka
i	i	k8xC	i
nohou	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
tmavě	tmavě	k6eAd1	tmavě
rudé	rudý	k2eAgFnPc4d1	rudá
barvy	barva	k1gFnPc4	barva
laku	lak	k1gInSc2	lak
<g/>
,	,	kIx,	,
či	či	k8xC	či
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
varianta	varianta	k1gFnSc1	varianta
sloužící	sloužící	k2eAgFnSc1d1	sloužící
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
lesku	lesk	k1gInSc2	lesk
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ale	ale	k9	ale
celá	celý	k2eAgFnSc1d1	celá
škála	škála	k1gFnSc1	škála
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
na	na	k7c6	na
nehtech	nehet	k1gInPc6	nehet
i	i	k9	i
kombinovat	kombinovat	k5eAaImF	kombinovat
od	od	k7c2	od
průhledné	průhledný	k2eAgFnSc2d1	průhledná
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnSc2d1	žlutá
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnSc2d1	červená
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnSc2d1	zelená
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnSc2d1	modrá
až	až	k9	až
po	po	k7c4	po
černou	černá	k1gFnSc4	černá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
aplikací	aplikace	k1gFnSc7	aplikace
laku	laka	k1gFnSc4	laka
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
nehty	nehet	k1gInPc4	nehet
pořádně	pořádně	k6eAd1	pořádně
umyté	umytý	k2eAgNnSc4d1	umyté
<g/>
,	,	kIx,	,
vyčištěné	vyčištěný	k2eAgNnSc4d1	vyčištěné
a	a	k8xC	a
usušené	usušený	k2eAgNnSc4d1	usušené
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
lak	lak	k1gInSc1	lak
dobře	dobře	k6eAd1	dobře
přilnul	přilnout	k5eAaPmAgInS	přilnout
a	a	k8xC	a
neodloupával	odloupávat	k5eNaImAgInS	odloupávat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
delší	dlouhý	k2eAgFnSc4d2	delší
životnost	životnost	k1gFnSc4	životnost
laku	lak	k1gInSc2	lak
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
využívat	využívat	k5eAaImF	využívat
i	i	k9	i
podkladový	podkladový	k2eAgInSc4d1	podkladový
lak	lak	k1gInSc4	lak
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
nanášet	nanášet	k5eAaImF	nanášet
lak	lak	k1gInSc4	lak
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
vrstvách	vrstva	k1gFnPc6	vrstva
s	s	k7c7	s
intervalem	interval	k1gInSc7	interval
okolo	okolo	k7c2	okolo
jedné	jeden	k4xCgFnSc2	jeden
minuty	minuta	k1gFnSc2	minuta
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
jeho	jeho	k3xOp3gNnPc2	jeho
přilnutí	přilnutí	k1gNnPc2	přilnutí
a	a	k8xC	a
výsledný	výsledný	k2eAgInSc1d1	výsledný
vzhled	vzhled	k1gInSc1	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
aplikace	aplikace	k1gFnSc2	aplikace
se	se	k3xPyFc4	se
lak	lak	k1gInSc1	lak
roztírá	roztírat	k5eAaImIp3nS	roztírat
pravidelně	pravidelně	k6eAd1	pravidelně
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
nehtu	nehet	k1gInSc6	nehet
až	až	k9	až
ke	k	k7c3	k
kůži	kůže	k1gFnSc3	kůže
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
nechává	nechávat	k5eAaImIp3nS	nechávat
volně	volně	k6eAd1	volně
zaschnout	zaschnout	k5eAaPmF	zaschnout
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
již	již	k6eAd1	již
částečně	částečně	k6eAd1	částečně
zatuhl	zatuhnout	k5eAaPmAgMnS	zatuhnout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
urychlit	urychlit	k5eAaPmF	urychlit
zasychání	zasychání	k1gNnSc1	zasychání
kmitavými	kmitavý	k2eAgInPc7d1	kmitavý
pohyby	pohyb	k1gInPc7	pohyb
končetiny	končetina	k1gFnSc2	končetina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
nalakované	nalakovaný	k2eAgInPc1d1	nalakovaný
nehty	nehet	k1gInPc1	nehet
přetřou	přetřít	k5eAaPmIp3nP	přetřít
ještě	ještě	k6eAd1	ještě
ochranným	ochranný	k2eAgInSc7d1	ochranný
lakem	lak	k1gInSc7	lak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
prodlouží	prodloužit	k5eAaPmIp3nS	prodloužit
životnost	životnost	k1gFnSc4	životnost
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
vrstvu	vrstva	k1gFnSc4	vrstva
před	před	k7c7	před
okolním	okolní	k2eAgNnSc7d1	okolní
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Lak	lak	k1gInSc1	lak
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
se	se	k3xPyFc4	se
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
z	z	k7c2	z
nehtů	nehet	k1gInPc2	nehet
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
vaty	vata	k1gFnSc2	vata
a	a	k8xC	a
kosmetického	kosmetický	k2eAgInSc2d1	kosmetický
odlakovače	odlakovač	k1gInSc2	odlakovač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zdobení	zdobení	k1gNnSc1	zdobení
nehtů	nehet	k1gInPc2	nehet
probíhalo	probíhat	k5eAaImAgNnS	probíhat
již	již	k6eAd1	již
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
k	k	k7c3	k
líčení	líčení	k1gNnSc3	líčení
nevyužíval	využívat	k5eNaImAgInS	využívat
lak	lak	k1gInSc1	lak
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
henna	henna	k6eAd1	henna
<g/>
,	,	kIx,	,
či	či	k8xC	či
kombinace	kombinace	k1gFnSc1	kombinace
různých	různý	k2eAgInPc2d1	různý
obarvených	obarvený	k2eAgInPc2d1	obarvený
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
prameny	pramen	k1gInPc1	pramen
ale	ale	k9	ale
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
používání	používání	k1gNnSc1	používání
je	být	k5eAaImIp3nS	být
staršího	starší	k1gMnSc4	starší
data	datum	k1gNnSc2	datum
a	a	k8xC	a
probíhalo	probíhat	k5eAaImAgNnS	probíhat
již	již	k6eAd1	již
ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
či	či	k8xC	či
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
využívalo	využívat	k5eAaImAgNnS	využívat
vstřikování	vstřikování	k1gNnSc4	vstřikování
organických	organický	k2eAgNnPc2d1	organické
barviv	barvivo	k1gNnPc2	barvivo
do	do	k7c2	do
nehtového	nehtový	k2eAgNnSc2d1	nehtové
lůžka	lůžko	k1gNnSc2	lůžko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
barevných	barevný	k2eAgInPc2d1	barevný
nehtů	nehet	k1gInPc2	nehet
<g/>
.	.	kIx.	.
<g/>
Moderní	moderní	k2eAgInPc1d1	moderní
laky	lak	k1gInPc1	lak
jsou	být	k5eAaImIp3nP	být
datovány	datovat	k5eAaImNgInP	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
první	první	k4xOgInSc1	první
růžový	růžový	k2eAgInSc1d1	růžový
a	a	k8xC	a
červený	červený	k2eAgInSc1d1	červený
lak	lak	k1gInSc1	lak
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
<g/>
Následoval	následovat	k5eAaImAgInS	následovat
rychlý	rychlý	k2eAgInSc4d1	rychlý
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
je	být	k5eAaImIp3nS	být
lak	lak	k1gInSc4	lak
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
již	již	k6eAd1	již
široce	široko	k6eAd1	široko
prodáván	prodávat	k5eAaImNgInS	prodávat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
lékárnách	lékárna	k1gFnPc6	lékárna
a	a	k8xC	a
drogeriích	drogerie	k1gFnPc6	drogerie
<g/>
.	.	kIx.	.
</s>
<s>
Nástup	nástup	k1gInSc1	nástup
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
snížení	snížení	k1gNnSc4	snížení
produkce	produkce	k1gFnSc2	produkce
laků	lak	k1gInPc2	lak
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
látky	látka	k1gFnPc1	látka
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
výrobu	výroba	k1gFnSc4	výroba
byly	být	k5eAaImAgFnP	být
potřeba	potřeba	k6eAd1	potřeba
ve	v	k7c6	v
vojenství	vojenství	k1gNnSc6	vojenství
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
nedostatek	nedostatek	k1gInSc1	nedostatek
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
hledání	hledání	k1gNnSc3	hledání
alternativních	alternativní	k2eAgFnPc2d1	alternativní
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
nových	nový	k2eAgFnPc2d1	nová
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
v	v	k7c6	v
moderních	moderní	k2eAgInPc6d1	moderní
lacích	lak	k1gInPc6	lak
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lak	laka	k1gFnPc2	laka
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Šuta	šuta	k1gFnSc1	šuta
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šťovíček	Šťovíček	k1gMnSc1	Šťovíček
<g/>
:	:	kIx,	:
Laky	lak	k1gInPc1	lak
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
</s>
</p>
