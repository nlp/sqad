<p>
<s>
Tropický	tropický	k2eAgInSc1d1	tropický
podnebný	podnebný	k2eAgInSc1d1	podnebný
pás	pás	k1gInSc1	pás
neboli	neboli	k8xC	neboli
tropy	trop	k1gInPc1	trop
či	či	k8xC	či
tropické	tropický	k2eAgFnPc1d1	tropická
šířky	šířka	k1gFnPc1	šířka
jsou	být	k5eAaImIp3nP	být
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
tropickým	tropický	k2eAgNnSc7d1	tropické
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
tropy	trop	k1gInPc7	trop
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
řeckém	řecký	k2eAgInSc6d1	řecký
tropos	tropos	k1gInSc1	tropos
=	=	kIx~	=
obrat	obrat	k1gInSc1	obrat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tropy	trop	k1gInPc4	trop
dle	dle	k7c2	dle
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
vymezeny	vymezen	k2eAgInPc1d1	vymezen
obratníky	obratník	k1gInPc1	obratník
Raka	rak	k1gMnSc2	rak
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
°	°	k?	°
<g/>
26	[number]	k4	26
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
°	°	k?	°
<g/>
26	[number]	k4	26
<g/>
'	'	kIx"	'
j.	j.	k?	j.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
tvoří	tvořit	k5eAaImIp3nP	tvořit
tropy	trop	k1gInPc1	trop
jeden	jeden	k4xCgMnSc1	jeden
souvislý	souvislý	k2eAgInSc1d1	souvislý
pás	pás	k1gInSc1	pás
obepínající	obepínající	k2eAgFnSc4d1	obepínající
Zemi	zem	k1gFnSc4	zem
podél	podél	k7c2	podél
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Tropy	trop	k1gInPc1	trop
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
označovány	označován	k2eAgInPc1d1	označován
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
horký	horký	k2eAgInSc4d1	horký
pás	pás	k1gInSc4	pás
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c4	mezi
obratníky	obratník	k1gInPc4	obratník
Raka	rak	k1gMnSc4	rak
a	a	k8xC	a
Kozoroha	Kozoroh	k1gMnSc4	Kozoroh
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgFnPc1d1	další
definice	definice	k1gFnPc1	definice
o	o	k7c6	o
tropech	trop	k1gInPc6	trop
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
oblastmi	oblast	k1gFnPc7	oblast
se	s	k7c7	s
subtropickým	subtropický	k2eAgNnSc7d1	subtropické
podnebím	podnebí	k1gNnSc7	podnebí
a	a	k8xC	a
oblastmi	oblast	k1gFnPc7	oblast
s	s	k7c7	s
podnebím	podnebí	k1gNnSc7	podnebí
rovníkových	rovníkový	k2eAgInPc2d1	rovníkový
monzunů	monzun	k1gInPc2	monzun
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
jsou	být	k5eAaImIp3nP	být
vymezeny	vymezit	k5eAaPmNgInP	vymezit
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
oblastmi	oblast	k1gFnPc7	oblast
s	s	k7c7	s
rovníkovým	rovníkový	k2eAgNnSc7d1	rovníkové
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
tomuto	tento	k3xDgInSc3	tento
pásu	pás	k1gInSc3	pás
říká	říkat	k5eAaImIp3nS	říkat
teplý	teplý	k2eAgInSc4d1	teplý
pás	pás	k1gInSc4	pás
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
připojena	připojen	k2eAgFnSc1d1	připojena
půlka	půlka	k1gFnSc1	půlka
pásu	pás	k1gInSc2	pás
subtropického	subtropický	k2eAgInSc2d1	subtropický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Köppenova	Köppenův	k2eAgFnSc1d1	Köppenova
klasifikace	klasifikace	k1gFnSc1	klasifikace
podnebí	podnebí	k1gNnSc2	podnebí
==	==	k?	==
</s>
</p>
<p>
<s>
Tropy	trop	k1gInPc1	trop
mají	mít	k5eAaImIp3nP	mít
tropické	tropický	k2eAgNnSc4d1	tropické
podnebí	podnebí	k1gNnSc4	podnebí
–	–	k?	–
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
vlastní	vlastní	k2eAgInPc4d1	vlastní
tropy	trop	k1gInPc4	trop
také	také	k9	také
definovány	definovat	k5eAaBmNgFnP	definovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
velké	velký	k2eAgNnSc4d1	velké
teplo	teplo	k1gNnSc4	teplo
a	a	k8xC	a
střídá	střídat	k5eAaImIp3nS	střídat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
hodně	hodně	k6eAd1	hodně
počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
tropický	tropický	k2eAgInSc1d1	tropický
pás	pás	k1gInSc1	pás
ztotožňován	ztotožňován	k2eAgInSc1d1	ztotožňován
s	s	k7c7	s
rovníkovým	rovníkový	k2eAgNnSc7d1	rovníkové
deštným	deštný	k2eAgNnSc7d1	Deštné
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
Köppenově	Köppenův	k2eAgFnSc6d1	Köppenova
klasifikaci	klasifikace	k1gFnSc6	klasifikace
podnebí	podnebí	k1gNnSc2	podnebí
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pásům	pás	k1gInPc3	pás
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
A	a	k9	a
<g/>
,	,	kIx,	,
Af	Af	k1gMnSc1	Af
–	–	k?	–
podnebí	podnebí	k1gNnSc2	podnebí
tropického	tropický	k2eAgInSc2d1	tropický
deštného	deštný	k2eAgInSc2d1	deštný
pralesa	prales	k1gInSc2	prales
<g/>
,	,	kIx,	,
Am	Am	k1gFnPc2	Am
–	–	k?	–
podnebí	podnebí	k1gNnSc4	podnebí
monzunové	monzunový	k2eAgFnSc2d1	monzunová
<g/>
,	,	kIx,	,
Aw	Aw	k1gMnSc1	Aw
–	–	k?	–
podnebí	podnebí	k1gNnSc2	podnebí
savanové	savanová	k1gFnSc2	savanová
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
nearidní	aridní	k2eNgFnSc1d1	aridní
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
arid	arid	k1gInSc1	arid
–	–	k?	–
vyprahlý	vyprahlý	k2eAgInSc1d1	vyprahlý
<g/>
)	)	kIx)	)
oblasti	oblast	k1gFnPc1	oblast
a	a	k8xC	a
průměrné	průměrný	k2eAgFnPc1d1	průměrná
měsíční	měsíční	k2eAgFnPc1d1	měsíční
teploty	teplota	k1gFnPc1	teplota
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
18	[number]	k4	18
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
==	==	k?	==
Alisova	Alisův	k2eAgFnSc1d1	Alisův
klasifikace	klasifikace	k1gFnSc1	klasifikace
podnebí	podnebí	k1gNnSc2	podnebí
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Alisově	Alisův	k2eAgFnSc6d1	Alisův
klasifikaci	klasifikace	k1gFnSc6	klasifikace
podnebí	podnebí	k1gNnSc2	podnebí
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
definici	definice	k1gFnSc3	definice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
hranice	hranice	k1gFnPc4	hranice
vymezeny	vymezen	k2eAgInPc1d1	vymezen
klimatickými	klimatický	k2eAgFnPc7d1	klimatická
polohami	poloha	k1gFnPc7	poloha
tropické	tropický	k2eAgFnSc2d1	tropická
fronty	fronta	k1gFnSc2	fronta
a	a	k8xC	a
polární	polární	k2eAgFnSc2d1	polární
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
severní	severní	k2eAgFnSc4d1	severní
i	i	k8xC	i
jižní	jižní	k2eAgFnSc4d1	jižní
polokouli	polokoule	k1gFnSc4	polokoule
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tropická	tropický	k2eAgFnSc1d1	tropická
vzduchová	vzduchový	k2eAgFnSc1d1	vzduchová
hmota	hmota	k1gFnSc1	hmota
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tropické	tropický	k2eAgNnSc1d1	tropické
podnebí	podnebí	k1gNnSc1	podnebí
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
4	[number]	k4	4
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
pevninské	pevninský	k2eAgInPc4d1	pevninský
(	(	kIx(	(
<g/>
kontinentální	kontinentální	k2eAgInPc4d1	kontinentální
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgInPc1d1	mořský
(	(	kIx(	(
<g/>
maritimní	maritimní	k2eAgInSc1d1	maritimní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
západních	západní	k2eAgInPc2d1	západní
břehů	břeh	k1gInPc2	břeh
pevnin	pevnina	k1gFnPc2	pevnina
(	(	kIx(	(
<g/>
středomořské	středomořský	k2eAgInPc1d1	středomořský
<g/>
)	)	kIx)	)
a	a	k8xC	a
východních	východní	k2eAgInPc2d1	východní
břehů	břeh	k1gInPc2	břeh
pevnin	pevnina	k1gFnPc2	pevnina
(	(	kIx(	(
<g/>
monzunové	monzunový	k2eAgInPc4d1	monzunový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
pevninský	pevninský	k2eAgInSc4d1	pevninský
tropický	tropický	k2eAgInSc4d1	tropický
typ	typ	k1gInSc4	typ
podnebí	podnebí	k1gNnSc2	podnebí
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
suché	suchý	k2eAgNnSc1d1	suché
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
teplé	teplý	k2eAgNnSc4d1	teplé
klima	klima	k1gNnSc4	klima
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
denními	denní	k2eAgInPc7d1	denní
výkyvy	výkyv	k1gInPc7	výkyv
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
leží	ležet	k5eAaImIp3nP	ležet
pouště	poušť	k1gFnPc1	poušť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jiná	jiný	k2eAgFnSc1d1	jiná
klasifikace	klasifikace	k1gFnSc1	klasifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
ryze	ryze	k6eAd1	ryze
astronomického	astronomický	k2eAgNnSc2d1	astronomické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
tropické	tropický	k2eAgNnSc4d1	tropické
pásmo	pásmo	k1gNnSc4	pásmo
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
alespoň	alespoň	k9	alespoň
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
rok	rok	k1gInSc4	rok
dopadají	dopadat	k5eAaImIp3nP	dopadat
sluneční	sluneční	k2eAgInPc1d1	sluneční
paprsky	paprsek	k1gInPc1	paprsek
kolmo	kolmo	k6eAd1	kolmo
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zenitu	zenit	k1gInSc6	zenit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
tropického	tropický	k2eAgNnSc2d1	tropické
pásma	pásmo	k1gNnSc2	pásmo
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
dána	dát	k5eAaPmNgFnS	dát
sklonem	sklon	k1gInSc7	sklon
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
obecná	obecný	k2eAgFnSc1d1	obecná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
aplikovat	aplikovat	k5eAaBmF	aplikovat
i	i	k9	i
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
planety	planeta	k1gFnPc4	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oceán	oceán	k1gInSc1	oceán
==	==	k?	==
</s>
</p>
<p>
<s>
Tropický	tropický	k2eAgInSc1d1	tropický
pás	pás	k1gInSc1	pás
oceánů	oceán	k1gInPc2	oceán
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
stálou	stálý	k2eAgFnSc7d1	stálá
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
svrchních	svrchní	k2eAgFnPc2d1	svrchní
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
roční	roční	k2eAgNnSc1d1	roční
kolísání	kolísání	k1gNnSc1	kolísání
nepřevyšuje	převyšovat	k5eNaImIp3nS	převyšovat
2	[number]	k4	2
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
mořích	moře	k1gNnPc6	moře
a	a	k8xC	a
oceánech	oceán	k1gInPc6	oceán
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
velká	velký	k2eAgFnSc1d1	velká
druhová	druhový	k2eAgFnSc1d1	druhová
biodiverzita	biodiverzita	k1gFnSc1	biodiverzita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nízká	nízký	k2eAgFnSc1d1	nízká
početnost	početnost	k1gFnSc1	početnost
jedinců	jedinec	k1gMnPc2	jedinec
(	(	kIx(	(
<g/>
abundance	abundance	k1gFnSc1	abundance
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biota	Bioto	k1gNnSc2	Bioto
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
první	první	k4xOgFnSc2	první
definice	definice	k1gFnSc2	definice
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
tropy	trop	k1gInPc1	trop
oblasti	oblast	k1gFnSc2	oblast
s	s	k7c7	s
tropickými	tropický	k2eAgInPc7d1	tropický
deštnými	deštný	k2eAgInPc7d1	deštný
lesy	les	k1gInPc7	les
<g/>
,	,	kIx,	,
savanami	savana	k1gFnPc7	savana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
druhé	druhý	k4xOgFnSc2	druhý
definice	definice	k1gFnSc2	definice
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
tropů	trop	k1gInPc2	trop
především	především	k6eAd1	především
velké	velký	k2eAgFnSc2d1	velká
pouště	poušť	k1gFnSc2	poušť
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Sahara	Sahara	k1gFnSc1	Sahara
s	s	k7c7	s
typickou	typický	k2eAgFnSc7d1	typická
pouští	poušť	k1gFnSc7	poušť
vegetací	vegetace	k1gFnSc7	vegetace
a	a	k8xC	a
živočišstvem	živočišstvo	k1gNnSc7	živočišstvo
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
zde	zde	k6eAd1	zde
chovají	chovat	k5eAaImIp3nP	chovat
velbloudy	velbloud	k1gMnPc4	velbloud
a	a	k8xC	a
v	v	k7c6	v
oázách	oáza	k1gFnPc6	oáza
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
palmy	palma	k1gFnPc1	palma
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc4	ovoce
a	a	k8xC	a
zeleninu	zelenina	k1gFnSc4	zelenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Sobíšek	Sobíšek	k1gMnSc1	Sobíšek
<g/>
,	,	kIx,	,
B.	B.	kA	B.
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Meteorologický	meteorologický	k2eAgInSc1d1	meteorologický
slovník	slovník	k1gInSc1	slovník
výkladový	výkladový	k2eAgInSc1d1	výkladový
a	a	k8xC	a
terminologický	terminologický	k2eAgInSc1d1	terminologický
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
cizojazyčnými	cizojazyčný	k2eAgInPc7d1	cizojazyčný
názvy	název	k1gInPc7	název
hesel	heslo	k1gNnPc2	heslo
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
,	,	kIx,	,
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
francouzštině	francouzština	k1gFnSc6	francouzština
a	a	k8xC	a
ruštině	ruština	k1gFnSc6	ruština
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
594	[number]	k4	594
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-85368-45-5	[number]	k4	80-85368-45-5
</s>
</p>
<p>
<s>
Netopil	topit	k5eNaImAgMnS	topit
<g/>
,	,	kIx,	,
R.	R.	kA	R.
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
geografie	geografie	k1gFnSc1	geografie
I.	I.	kA	I.
Státní	státní	k2eAgFnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc4d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
273	[number]	k4	273
s.	s.	k?	s.
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
84	[number]	k4	84
<g/>
,	,	kIx,	,
strana	strana	k1gFnSc1	strana
128	[number]	k4	128
</s>
</p>
<p>
<s>
Horník	Horník	k1gMnSc1	Horník
<g/>
,	,	kIx,	,
S.	S.	kA	S.
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
geografie	geografie	k1gFnSc1	geografie
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
320	[number]	k4	320
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Hanzlík	Hanzlík	k1gMnSc1	Hanzlík
<g/>
,	,	kIx,	,
S.	S.	kA	S.
<g/>
:	:	kIx,	:
Základy	základ	k1gInPc1	základ
meteorologie	meteorologie	k1gFnSc2	meteorologie
a	a	k8xC	a
klimatologie	klimatologie	k1gFnSc2	klimatologie
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
321	[number]	k4	321
s.	s.	k?	s.
<g/>
,	,	kIx,	,
strana	strana	k1gFnSc1	strana
194	[number]	k4	194
</s>
</p>
