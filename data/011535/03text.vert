<p>
<s>
Ánanda	Ánanda	k1gFnSc1	Ánanda
byl	být	k5eAaImAgMnS	být
Buddhův	Buddhův	k2eAgMnSc1d1	Buddhův
bratranec	bratranec	k1gMnSc1	bratranec
a	a	k8xC	a
asistent	asistent	k1gMnSc1	asistent
(	(	kIx(	(
<g/>
v	v	k7c6	v
sanskrtu	sanskrt	k1gInSc6	sanskrt
upasthájaka	upasthájak	k1gMnSc2	upasthájak
<g/>
,	,	kIx,	,
v	v	k7c6	v
pálí	pálit	k5eAaImIp3nS	pálit
upattháka	upatthák	k1gMnSc4	upatthák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
deset	deset	k4xCc4	deset
nejvýznačnějších	význačný	k2eAgMnPc2d3	nejvýznačnější
Buddhových	Buddhův	k2eAgMnPc2d1	Buddhův
žáků	žák	k1gMnPc2	žák
(	(	kIx(	(
<g/>
v	v	k7c6	v
sanskrtu	sanskrt	k1gInSc6	sanskrt
dašašišja	dašašišj	k1gInSc2	dašašišj
<g/>
,	,	kIx,	,
v	v	k7c6	v
pálí	pálit	k5eAaImIp3nS	pálit
dasasissa	dasasissa	k1gFnSc1	dasasissa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zenové	zenový	k2eAgFnSc2d1	zenová
tradice	tradice	k1gFnSc2	tradice
byl	být	k5eAaImAgMnS	být
druhým	druhý	k4xOgMnSc7	druhý
patriarchou	patriarcha	k1gMnSc7	patriarcha
zenu	zenu	k5eAaPmIp1nS	zenu
po	po	k7c6	po
Mahákássapovi	Mahákássap	k1gMnSc6	Mahákássap
(	(	kIx(	(
<g/>
sa	sa	k?	sa
Mahákášjapa	Mahákášjapa	k1gFnSc1	Mahákášjapa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Buddha	Buddha	k1gMnSc1	Buddha
přebýval	přebývat	k5eAaImAgMnS	přebývat
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
zrozením	zrození	k1gNnSc7	zrození
v	v	k7c6	v
nebesích	nebesa	k1gNnPc6	nebesa
Tušita	Tušitum	k1gNnSc2	Tušitum
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
jako	jako	k8xS	jako
Buddha	Buddha	k1gMnSc1	Buddha
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
Amitódana	Amitódan	k1gMnSc4	Amitódan
Sákja	Sákjus	k1gMnSc4	Sákjus
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Suddhódany	Suddhódana	k1gFnSc2	Suddhódana
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
otcem	otec	k1gMnSc7	otec
Buddhy	Buddha	k1gMnSc2	Buddha
<g/>
.	.	kIx.	.
</s>
<s>
Mahánáma	Mahánáma	k1gFnSc1	Mahánáma
a	a	k8xC	a
Anuruddha	Anuruddha	k1gFnSc1	Anuruddha
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
budoucí	budoucí	k2eAgMnPc1d1	budoucí
významní	významný	k2eAgMnPc1d1	významný
mniši	mnich	k1gMnPc1	mnich
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Ánandovi	Ánandův	k2eAgMnPc1d1	Ánandův
bratři	bratr	k1gMnPc1	bratr
(	(	kIx(	(
<g/>
možná	možná	k9	možná
nevlastní	vlastnit	k5eNaImIp3nS	vlastnit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
byl	být	k5eAaImAgInS	být
Ánanda	Ánand	k1gMnSc2	Ánand
synem	syn	k1gMnSc7	syn
Suklódany	Suklódana	k1gFnSc2	Suklódana
a	a	k8xC	a
bratrem	bratr	k1gMnSc7	bratr
Dévadatty	Dévadatta	k1gFnSc2	Dévadatta
a	a	k8xC	a
Upadhány	Upadhán	k2eAgInPc1d1	Upadhán
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
byla	být	k5eAaImAgFnS	být
Mrgí	Mrgí	k1gFnSc1	Mrgí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
mnišské	mnišský	k2eAgFnSc2d1	mnišská
sanghy	sangha	k1gFnSc2	sangha
byl	být	k5eAaImAgMnS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
Buddhově	Buddhův	k2eAgNnSc6d1	Buddhovo
osvícení	osvícení	k1gNnSc6	osvícení
<g/>
,	,	kIx,	,
když	když	k8xS	když
Buddha	Buddha	k1gMnSc1	Buddha
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Kapilavastu	Kapilavast	k1gMnSc3	Kapilavast
<g/>
.	.	kIx.	.
</s>
<s>
Vynikal	vynikat	k5eAaImAgMnS	vynikat
svou	svůj	k3xOyFgFnSc7	svůj
neobvyklou	obvyklý	k2eNgFnSc7d1	neobvyklá
pamětí	paměť	k1gFnSc7	paměť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
celý	celý	k2eAgInSc4d1	celý
druhý	druhý	k4xOgInSc4	druhý
koš	koš	k1gInSc4	koš
(	(	kIx(	(
<g/>
p.	p.	k?	p.
Sutta-pitaka	Suttaitak	k1gMnSc2	Sutta-pitak
<g/>
,	,	kIx,	,
<g/>
sa	sa	k?	sa
<g/>
.	.	kIx.	.
</s>
<s>
Sútrapitaka	Sútrapitak	k1gMnSc4	Sútrapitak
<g/>
)	)	kIx)	)
původního	původní	k2eAgInSc2d1	původní
buddhistického	buddhistický	k2eAgInSc2d1	buddhistický
kánonu	kánon	k1gInSc2	kánon
(	(	kIx(	(
<g/>
p.	p.	k?	p.
Tipitaka	Tipitak	k1gMnSc2	Tipitak
<g/>
,	,	kIx,	,
sa	sa	k?	sa
<g/>
.	.	kIx.	.
</s>
<s>
Tripitaka	Tripitak	k1gMnSc2	Tripitak
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
na	na	k7c4	na
Buddhovy	Buddhův	k2eAgFnPc4d1	Buddhova
rozpravy	rozprava	k1gFnPc4	rozprava
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
Buddhova	Buddhův	k2eAgMnSc2d1	Buddhův
pomocníka	pomocník	k1gMnSc2	pomocník
přijal	přijmout	k5eAaPmAgInS	přijmout
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
ho	on	k3xPp3gInSc4	on
Buddha	Buddha	k1gMnSc1	Buddha
ujistil	ujistit	k5eAaPmAgMnS	ujistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
z	z	k7c2	z
ní	on	k3xPp3gFnSc6	on
nebudou	být	k5eNaImBp3nP	být
plynout	plynout	k5eAaImF	plynout
žádné	žádný	k3yNgFnPc4	žádný
výhody	výhoda	k1gFnPc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
službě	služba	k1gFnSc3	služba
Buddhovi	Buddhův	k2eAgMnPc1d1	Buddhův
<g/>
,	,	kIx,	,
zanedbával	zanedbávat	k5eAaImAgMnS	zanedbávat
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
Buddhově	Buddhův	k2eAgInSc6d1	Buddhův
skonu	skon	k1gInSc6	skon
první	první	k4xOgMnSc1	první
buddhistický	buddhistický	k2eAgInSc1d1	buddhistický
koncil	koncil	k1gInSc1	koncil
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
"	"	kIx"	"
<g/>
pouze	pouze	k6eAd1	pouze
<g/>
"	"	kIx"	"
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
probuzení	probuzení	k1gNnSc2	probuzení
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
roztržce	roztržka	k1gFnSc3	roztržka
s	s	k7c7	s
Mahákášjapou	Mahákášjapou	k1gMnPc7	Mahákášjapou
se	se	k3xPyFc4	se
odebral	odebrat	k5eAaPmAgInS	odebrat
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
usilovně	usilovně	k6eAd1	usilovně
věnoval	věnovat	k5eAaImAgMnS	věnovat
praxi	praxe	k1gFnSc4	praxe
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
k	k	k7c3	k
ránu	ráno	k1gNnSc3	ráno
cítil	cítit	k5eAaImAgInS	cítit
unavený	unavený	k2eAgMnSc1d1	unavený
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
odpočine	odpočinout	k5eAaPmIp3nS	odpočinout
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
když	když	k8xS	když
ulehal	ulehat	k5eAaImAgInS	ulehat
ke	k	k7c3	k
spánku	spánek	k1gInSc3	spánek
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
plného	plný	k2eAgNnSc2d1	plné
probuzení	probuzení	k1gNnSc2	probuzení
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
arhatem	arhat	k1gInSc7	arhat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
mu	on	k3xPp3gMnSc3	on
už	už	k6eAd1	už
následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
jako	jako	k9	jako
arahantovi	arahant	k1gMnSc3	arahant
byla	být	k5eAaImAgFnS	být
povolena	povolit	k5eAaPmNgFnS	povolit
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
koncilu	koncil	k1gInSc6	koncil
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přednesl	přednést	k5eAaPmAgMnS	přednést
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
celý	celý	k2eAgInSc4d1	celý
druhý	druhý	k4xOgInSc4	druhý
koš	koš	k1gInSc4	koš
kánonu	kánon	k1gInSc2	kánon
<g/>
.	.	kIx.	.
</s>
<s>
Ánadovým	Ánadův	k2eAgInSc7d1	Ánadův
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
recitovat	recitovat	k5eAaImF	recitovat
všechny	všechen	k3xTgFnPc4	všechen
Buddhovy	Buddhův	k2eAgFnPc4d1	Buddhova
rozpravy	rozprava	k1gFnPc4	rozprava
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
si	se	k3xPyFc3	se
zapamatoval	zapamatovat	k5eAaPmAgMnS	zapamatovat
během	během	k7c2	během
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
staral	starat	k5eAaImAgMnS	starat
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
mniši	mnich	k1gMnPc1	mnich
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pak	pak	k6eAd1	pak
opakovali	opakovat	k5eAaImAgMnP	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tradice	tradice	k1gFnSc1	tradice
předávání	předávání	k1gNnSc2	předávání
Buddhových	Buddhův	k2eAgFnPc2d1	Buddhova
rozprav	rozprava	k1gFnPc2	rozprava
se	se	k3xPyFc4	se
v	v	k7c6	v
théravádě	théraváda	k1gFnSc6	théraváda
zachovala	zachovat	k5eAaPmAgFnS	zachovat
až	až	k9	až
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ánanda	Ánand	k1gMnSc4	Ánand
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jako	jako	k8xC	jako
Buddhův	Buddhův	k2eAgMnSc1d1	Buddhův
pomocník	pomocník	k1gMnSc1	pomocník
slyšel	slyšet	k5eAaImAgMnS	slyšet
nejvíce	nejvíce	k6eAd1	nejvíce
rozprav	rozprava	k1gFnPc2	rozprava
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
také	také	k9	také
nejpřednější	přední	k2eAgNnSc4d3	nejpřednější
mezi	mezi	k7c4	mezi
mnichy	mnich	k1gMnPc4	mnich
ve	v	k7c6	v
schopnosti	schopnost	k1gFnSc6	schopnost
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
si	se	k3xPyFc3	se
Buddhovo	Buddhův	k2eAgNnSc4d1	Buddhovo
učení	učení	k1gNnSc4	učení
<g/>
.	.	kIx.	.
</s>
<s>
Ánanda	Ánand	k1gMnSc4	Ánand
byl	být	k5eAaImAgMnS	být
také	také	k9	také
znám	znám	k2eAgInSc1d1	znám
svým	svůj	k3xOyFgInSc7	svůj
shovívavým	shovívavý	k2eAgInSc7d1	shovívavý
postojem	postoj	k1gInSc7	postoj
vůči	vůči	k7c3	vůči
ženám	žena	k1gFnPc3	žena
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
přemluvil	přemluvit	k5eAaPmAgMnS	přemluvit
Buddhu	Buddha	k1gMnSc4	Buddha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
svolil	svolit	k5eAaPmAgMnS	svolit
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
řádu	řád	k1gInSc2	řád
mnišek	mniška	k1gFnPc2	mniška
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
120	[number]	k4	120
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
buddhistický	buddhistický	k2eAgInSc1d1	buddhistický
koncil	koncil	k1gInSc1	koncil
</s>
</p>
<p>
<s>
Mahákášjapa	Mahákášjapa	k1gFnSc1	Mahákášjapa
</s>
</p>
