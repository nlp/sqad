<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
má	mít	k5eAaImIp3nS	mít
nezřetelný	zřetelný	k2eNgInSc4d1	nezřetelný
systém	systém	k1gInSc4	systém
planetárních	planetární	k2eAgInPc2d1	planetární
prstenců	prstenec	k1gInPc2	prstenec
skládajících	skládající	k2eAgInPc2d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
torusu	torus	k1gInSc2	torus
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
jasného	jasný	k2eAgInSc2d1	jasný
hlavního	hlavní	k2eAgInSc2d1	hlavní
prstence	prstenec	k1gInSc2	prstenec
a	a	k8xC	a
vnějšího	vnější	k2eAgInSc2d1	vnější
slabšího	slabý	k2eAgInSc2d2	slabší
prstence	prstenec	k1gInSc2	prstenec
<g/>
.	.	kIx.	.
</s>
