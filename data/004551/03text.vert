<s>
Prezident	prezident	k1gMnSc1	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
hlava	hlava	k1gFnSc1	hlava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
přímo	přímo	k6eAd1	přímo
českými	český	k2eAgMnPc7d1	český
občany	občan	k1gMnPc7	občan
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
oprávněnými	oprávněný	k2eAgInPc7d1	oprávněný
voliči	volič	k1gInPc7	volič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
volen	volit	k5eAaImNgInS	volit
oběma	dva	k4xCgFnPc7	dva
komorami	komora	k1gFnPc7	komora
Parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
poslanci	poslanec	k1gMnPc1	poslanec
a	a	k8xC	a
senátory	senátor	k1gMnPc4	senátor
<g/>
,	,	kIx,	,
na	na	k7c6	na
společné	společný	k2eAgFnSc6d1	společná
schůzi	schůze	k1gFnSc6	schůze
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
výkonu	výkon	k1gInSc2	výkon
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
není	být	k5eNaImIp3nS	být
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
velezrady	velezrada	k1gFnSc2	velezrada
a	a	k8xC	a
hrubého	hrubý	k2eAgNnSc2d1	hrubé
porušení	porušení	k1gNnSc2	porušení
Ústavy	ústava	k1gFnSc2	ústava
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
součásti	součást	k1gFnSc2	součást
ústavního	ústavní	k2eAgInSc2d1	ústavní
pořádku	pořádek	k1gInSc2	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
se	se	k3xPyFc4	se
ujímal	ujímat	k5eAaImAgMnS	ujímat
úřadu	úřad	k1gInSc2	úřad
složením	složení	k1gNnSc7	složení
slibu	slib	k1gInSc2	slib
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
předsedy	předseda	k1gMnSc2	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
skládá	skládat	k5eAaImIp3nS	skládat
slib	slib	k1gInSc4	slib
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
předsedy	předseda	k1gMnSc2	předseda
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentovo	prezidentův	k2eAgNnSc1d1	prezidentovo
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
trvá	trvat	k5eAaImIp3nS	trvat
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
může	moct	k5eAaImIp3nS	moct
též	též	k9	též
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
předsedy	předseda	k1gMnSc2	předseda
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Výkon	výkon	k1gInSc1	výkon
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
,	,	kIx,	,
protokolární	protokolární	k2eAgFnPc4d1	protokolární
povinnosti	povinnost	k1gFnPc4	povinnost
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
funkce	funkce	k1gFnPc4	funkce
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
prezidenta	prezident	k1gMnSc2	prezident
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Kancelář	kancelář	k1gFnSc1	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
ochranu	ochrana	k1gFnSc4	ochrana
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
specializovaný	specializovaný	k2eAgInSc1d1	specializovaný
policejní	policejní	k2eAgInSc1d1	policejní
útvar	útvar	k1gInSc1	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
součástí	součást	k1gFnSc7	součást
pilíře	pilíř	k1gInPc1	pilíř
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
s	s	k7c7	s
omezenými	omezený	k2eAgFnPc7d1	omezená
pravomocemi	pravomoc	k1gFnPc7	pravomoc
v	v	k7c6	v
rozhodovacích	rozhodovací	k2eAgInPc6d1	rozhodovací
procesech	proces	k1gInPc6	proces
(	(	kIx(	(
<g/>
pasivní	pasivní	k2eAgFnSc1d1	pasivní
složka	složka	k1gFnSc1	složka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
většinově	většinově	k6eAd1	většinově
náleží	náležet	k5eAaImIp3nS	náležet
předsedovi	předseda	k1gMnSc3	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
kabinetu	kabinet	k1gInSc2	kabinet
(	(	kIx(	(
<g/>
aktivní	aktivní	k2eAgFnSc1d1	aktivní
složka	složka	k1gFnSc1	složka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
dění	dění	k1gNnSc2	dění
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
napětí	napětí	k1gNnSc2	napětí
a	a	k8xC	a
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
zemi	zem	k1gFnSc4	zem
navenek	navenek	k6eAd1	navenek
i	i	k9	i
ve	v	k7c6	v
vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
vztazích	vztah	k1gInPc6	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Slib	slib	k1gInSc1	slib
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Slibuji	slibovat	k5eAaImIp1nS	slibovat
věrnost	věrnost	k1gFnSc4	věrnost
České	český	k2eAgFnSc3d1	Česká
republice	republika	k1gFnSc3	republika
<g/>
.	.	kIx.	.
</s>
<s>
Slibuji	slibovat	k5eAaImIp1nS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
budu	být	k5eAaImBp1nS	být
zachovávat	zachovávat	k5eAaImF	zachovávat
její	její	k3xOp3gFnSc4	její
Ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC	a
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Slibuji	slibovat	k5eAaImIp1nS	slibovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
čest	čest	k1gFnSc4	čest
<g/>
,	,	kIx,	,
že	že	k8xS	že
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
budu	být	k5eAaImBp1nS	být
zastávat	zastávat	k5eAaImF	zastávat
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
všeho	všecek	k3xTgInSc2	všecek
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
nejlepšího	dobrý	k2eAgNnSc2d3	nejlepší
vědomí	vědomí	k1gNnSc2	vědomí
a	a	k8xC	a
svědomí	svědomí	k1gNnSc2	svědomí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
schůzí	schůze	k1gFnPc2	schůze
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
Parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc2	jejich
výborů	výbor	k1gInPc2	výbor
a	a	k8xC	a
komisí	komise	k1gFnPc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
uděleno	udělen	k2eAgNnSc1d1	uděleno
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
kdykoliv	kdykoliv	k6eAd1	kdykoliv
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
požádá	požádat	k5eAaPmIp3nS	požádat
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
účastnit	účastnit	k5eAaImF	účastnit
se	s	k7c7	s
schůzí	schůze	k1gFnSc7	schůze
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
vyžádat	vyžádat	k5eAaPmF	vyžádat
si	se	k3xPyFc3	se
od	od	k7c2	od
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
jejích	její	k3xOp3gInPc2	její
členů	člen	k1gInPc2	člen
zprávy	zpráva	k1gFnSc2	zpráva
a	a	k8xC	a
projednávat	projednávat	k5eAaImF	projednávat
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
nebo	nebo	k8xC	nebo
s	s	k7c7	s
jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
otázky	otázka	k1gFnSc2	otázka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
jejich	jejich	k3xOp3gFnSc2	jejich
působnosti	působnost	k1gFnSc2	působnost
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
také	také	k9	také
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
disponuje	disponovat	k5eAaBmIp3nS	disponovat
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
klíčů	klíč	k1gInPc2	klíč
od	od	k7c2	od
dveří	dveře	k1gFnPc2	dveře
do	do	k7c2	do
Korunní	korunní	k2eAgFnSc2d1	korunní
komory	komora	k1gFnSc2	komora
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
uloženy	uložen	k2eAgInPc4d1	uložen
české	český	k2eAgInPc4d1	český
korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnPc1d1	Česká
republiky	republika	k1gFnPc1	republika
<g/>
,	,	kIx,	,
Volby	volba	k1gFnPc1	volba
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
a	a	k8xC	a
Volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnPc1d1	Česká
republiky	republika	k1gFnPc1	republika
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
občan	občan	k1gMnSc1	občan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
volit	volit	k5eAaImF	volit
a	a	k8xC	a
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
věku	věk	k1gInSc2	věk
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvakrát	dvakrát	k6eAd1	dvakrát
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Zvolen	Zvolen	k1gInSc1	Zvolen
také	také	k9	také
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
občan	občan	k1gMnSc1	občan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
za	za	k7c4	za
velezradu	velezrada	k1gFnSc4	velezrada
nebo	nebo	k8xC	nebo
za	za	k7c4	za
hrubé	hrubý	k2eAgNnSc4d1	hrubé
porušení	porušení	k1gNnSc4	porušení
Ústavy	ústava	k1gFnSc2	ústava
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
součásti	součást	k1gFnSc2	součást
ústavního	ústavní	k2eAgInSc2d1	ústavní
pořádku	pořádek	k1gInSc2	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Navrhovat	navrhovat	k5eAaImF	navrhovat
kandidáta	kandidát	k1gMnSc4	kandidát
může	moct	k5eAaImIp3nS	moct
nejméně	málo	k6eAd3	málo
dvacet	dvacet	k4xCc4	dvacet
poslanců	poslanec	k1gMnPc2	poslanec
nebo	nebo	k8xC	nebo
deset	deset	k4xCc1	deset
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
každý	každý	k3xTgMnSc1	každý
občan	občan	k1gMnSc1	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
věku	věk	k1gInSc2	věk
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
podpoří	podpořit	k5eAaPmIp3nS	podpořit
<g/>
-li	i	k?	-li
jeho	on	k3xPp3gInSc4	on
návrh	návrh	k1gInSc4	návrh
petice	petice	k1gFnSc1	petice
podepsaná	podepsaný	k2eAgFnSc1d1	podepsaná
nejméně	málo	k6eAd3	málo
50	[number]	k4	50
000	[number]	k4	000
občany	občan	k1gMnPc7	občan
<g/>
.	.	kIx.	.
</s>
<s>
Volbu	volba	k1gFnSc4	volba
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
tajným	tajný	k2eAgNnSc7d1	tajné
hlasováním	hlasování	k1gNnSc7	hlasování
a	a	k8xC	a
právo	právo	k1gNnSc1	právo
volit	volit	k5eAaImF	volit
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgMnSc1	každý
český	český	k2eAgMnSc1d1	český
občan	občan	k1gMnSc1	občan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
věku	věk	k1gInSc2	věk
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získá	získat	k5eAaPmIp3nS	získat
nadpoloviční	nadpoloviční	k2eAgFnSc4d1	nadpoloviční
většinu	většina	k1gFnSc4	většina
všech	všecek	k3xTgInPc2	všecek
platných	platný	k2eAgInPc2d1	platný
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nikdo	nikdo	k3yNnSc1	nikdo
nadpoloviční	nadpoloviční	k2eAgFnSc4d1	nadpoloviční
většinu	většina	k1gFnSc4	většina
nezíská	získat	k5eNaPmIp3nS	získat
<g/>
,	,	kIx,	,
koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
do	do	k7c2	do
14	[number]	k4	14
dnů	den	k1gInPc2	den
kolo	kolo	k1gNnSc1	kolo
druhé	druhý	k4xOgFnPc4	druhý
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
postupují	postupovat	k5eAaImIp3nP	postupovat
dva	dva	k4xCgMnPc1	dva
nejúspěšnější	úspěšný	k2eAgMnPc1d3	nejúspěšnější
kandidáti	kandidát	k1gMnPc1	kandidát
z	z	k7c2	z
kola	kolo	k1gNnSc2	kolo
prvního	první	k4xOgInSc2	první
(	(	kIx(	(
<g/>
při	při	k7c6	při
teoretické	teoretický	k2eAgFnSc6d1	teoretická
rovnosti	rovnost	k1gFnSc6	rovnost
hlasů	hlas	k1gInPc2	hlas
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
takový	takový	k3xDgInSc4	takový
počet	počet	k1gInSc4	počet
hlasů	hlas	k1gInPc2	hlas
získají	získat	k5eAaPmIp3nP	získat
<g/>
,	,	kIx,	,
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
alespoň	alespoň	k9	alespoň
dva	dva	k4xCgInPc4	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
pak	pak	k6eAd1	pak
bude	být	k5eAaImBp3nS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
ten	ten	k3xDgMnSc1	ten
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získá	získat	k5eAaPmIp3nS	získat
hlasů	hlas	k1gInPc2	hlas
více	hodně	k6eAd2	hodně
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
při	při	k7c6	při
teoretické	teoretický	k2eAgFnSc6d1	teoretická
rovnosti	rovnost	k1gFnSc6	rovnost
hlasů	hlas	k1gInPc2	hlas
se	se	k3xPyFc4	se
do	do	k7c2	do
deseti	deset	k4xCc2	deset
dnů	den	k1gInPc2	den
vyhlásí	vyhlásit	k5eAaPmIp3nS	vyhlásit
zcela	zcela	k6eAd1	zcela
nová	nový	k2eAgFnSc1d1	nová
volba	volba	k1gFnSc1	volba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prezidenta	prezident	k1gMnSc4	prezident
nelze	lze	k6eNd1	lze
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
výkonu	výkon	k1gInSc2	výkon
jeho	jeho	k3xOp3gFnSc2	jeho
funkce	funkce	k1gFnSc2	funkce
zadržet	zadržet	k5eAaPmF	zadržet
<g/>
,	,	kIx,	,
trestně	trestně	k6eAd1	trestně
stíhat	stíhat	k5eAaImF	stíhat
ani	ani	k8xC	ani
stíhat	stíhat	k5eAaImF	stíhat
pro	pro	k7c4	pro
přestupek	přestupek	k1gInSc4	přestupek
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc4d1	jiný
správní	správní	k2eAgInSc4d1	správní
delikt	delikt	k1gInSc4	delikt
<g/>
.	.	kIx.	.
</s>
<s>
Nestíhatelnost	Nestíhatelnost	k1gFnSc1	Nestíhatelnost
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
jak	jak	k6eAd1	jak
na	na	k7c4	na
trestné	trestný	k2eAgInPc4d1	trestný
činy	čin	k1gInPc4	čin
<g/>
,	,	kIx,	,
přestupky	přestupek	k1gInPc4	přestupek
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
správní	správní	k2eAgInPc4d1	správní
delikty	delikt	k1gInPc4	delikt
spáchané	spáchaný	k2eAgInPc4d1	spáchaný
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
mandátu	mandát	k1gInSc2	mandát
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
spáchané	spáchaný	k2eAgNnSc1d1	spáchané
během	během	k7c2	během
jeho	jeho	k3xOp3gInSc2	jeho
výkonu	výkon	k1gInSc2	výkon
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yIgFnPc4	který
nemohl	moct	k5eNaImAgMnS	moct
být	být	k5eAaImF	být
prezident	prezident	k1gMnSc1	prezident
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
mandátu	mandát	k1gInSc2	mandát
stíhán	stíhat	k5eAaImNgMnS	stíhat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
možné	možný	k2eAgNnSc1d1	možné
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
jeho	on	k3xPp3gInSc2	on
mandátu	mandát	k1gInSc2	mandát
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
běh	běh	k1gInSc1	běh
promlčecí	promlčecí	k2eAgFnSc2d1	promlčecí
lhůty	lhůta	k1gFnSc2	lhůta
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
protiprávní	protiprávní	k2eAgInPc4d1	protiprávní
činy	čin	k1gInPc4	čin
staví	stavit	k5eAaBmIp3nS	stavit
<g/>
.	.	kIx.	.
</s>
<s>
Procesní	procesní	k2eAgFnSc1d1	procesní
imunita	imunita	k1gFnSc1	imunita
se	se	k3xPyFc4	se
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
na	na	k7c4	na
civilní	civilní	k2eAgInPc4d1	civilní
ani	ani	k8xC	ani
ústavní	ústavní	k2eAgInPc4d1	ústavní
delikty	delikt	k1gInPc4	delikt
<g/>
.	.	kIx.	.
</s>
<s>
Velezrada	velezrada	k1gFnSc1	velezrada
je	být	k5eAaImIp3nS	být
ústavní	ústavní	k2eAgInSc4d1	ústavní
delikt	delikt	k1gInSc4	delikt
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yRgInSc4	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
prezident	prezident	k1gMnSc1	prezident
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jej	on	k3xPp3gMnSc4	on
odsoudit	odsoudit	k5eAaPmF	odsoudit
také	také	k9	také
pro	pro	k7c4	pro
hrubé	hrubý	k2eAgNnSc4d1	hrubé
porušení	porušení	k1gNnSc4	porušení
Ústavy	ústava	k1gFnSc2	ústava
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
součásti	součást	k1gFnSc2	součást
ústavního	ústavní	k2eAgInSc2d1	ústavní
pořádku	pořádek	k1gInSc2	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Velezradou	velezrada	k1gFnSc7	velezrada
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
rozumí	rozumět	k5eAaImIp3nS	rozumět
jednání	jednání	k1gNnSc4	jednání
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
směřující	směřující	k2eAgFnSc2d1	směřující
proti	proti	k7c3	proti
svrchovanosti	svrchovanost	k1gFnSc3	svrchovanost
a	a	k8xC	a
celistvosti	celistvost	k1gFnSc3	celistvost
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
proti	proti	k7c3	proti
jejímu	její	k3xOp3gInSc3	její
demokratickému	demokratický	k2eAgInSc3d1	demokratický
řádu	řád	k1gInSc3	řád
<g/>
.	.	kIx.	.
</s>
<s>
Žalobu	žaloba	k1gFnSc4	žaloba
může	moct	k5eAaImIp3nS	moct
podat	podat	k5eAaPmF	podat
jen	jen	k9	jen
Senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
k	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
získat	získat	k5eAaPmF	získat
souhlas	souhlas	k1gInSc4	souhlas
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Řízení	řízení	k1gNnSc1	řízení
probíhá	probíhat	k5eAaImIp3nS	probíhat
před	před	k7c7	před
Ústavním	ústavní	k2eAgInSc7d1	ústavní
soudem	soud	k1gInSc7	soud
a	a	k8xC	a
jediným	jediný	k2eAgInSc7d1	jediný
trestem	trest	k1gInSc7	trest
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ztráta	ztráta	k1gFnSc1	ztráta
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
způsobilosti	způsobilost	k1gFnSc2	způsobilost
jej	on	k3xPp3gMnSc4	on
znovu	znovu	k6eAd1	znovu
nabýt	nabýt	k5eAaPmF	nabýt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
lidovecký	lidovecký	k2eAgMnSc1d1	lidovecký
senátor	senátor	k1gMnSc1	senátor
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Bárta	Bárta	k1gMnSc1	Bárta
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
připravený	připravený	k2eAgInSc4d1	připravený
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
ústavní	ústavní	k2eAgFnSc4d1	ústavní
žalobu	žaloba	k1gFnSc4	žaloba
proti	proti	k7c3	proti
prezidentu	prezident	k1gMnSc3	prezident
Klausovi	Klaus	k1gMnSc3	Klaus
pro	pro	k7c4	pro
velezradu	velezrada	k1gFnSc4	velezrada
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
obviňoval	obviňovat	k5eAaImAgMnS	obviňovat
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
svým	svůj	k3xOyFgMnPc3	svůj
počínám	počínat	k5eAaImIp1nS	počínat
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
fungování	fungování	k1gNnSc1	fungování
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nenavrhuje	navrhovat	k5eNaImIp3nS	navrhovat
dostatečně	dostatečně	k6eAd1	dostatečně
rychle	rychle	k6eAd1	rychle
Senátu	senát	k1gInSc2	senát
na	na	k7c6	na
schválení	schválení	k1gNnSc6	schválení
kandidáty	kandidát	k1gMnPc4	kandidát
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
jeho	jeho	k3xOp3gMnPc2	jeho
soudců	soudce	k1gMnPc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
však	však	k9	však
mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgMnPc7d1	ostatní
senátory	senátor	k1gMnPc7	senátor
podporu	podpor	k1gInSc2	podpor
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
zájem	zájem	k1gInSc1	zájem
obecně	obecně	k6eAd1	obecně
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
velezradě	velezrada	k1gFnSc6	velezrada
také	také	k9	také
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1	sociálnědemokratická
senátorka	senátorka	k1gFnSc1	senátorka
Alena	Alena	k1gFnSc1	Alena
Gajdůšková	Gajdůšková	k1gFnSc1	Gajdůšková
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
ratifikací	ratifikace	k1gFnSc7	ratifikace
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
však	však	k9	však
k	k	k7c3	k
podání	podání	k1gNnSc3	podání
ústavní	ústavní	k2eAgFnSc2d1	ústavní
žaloby	žaloba	k1gFnSc2	žaloba
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
podepsal	podepsat	k5eAaPmAgInS	podepsat
potřebný	potřebný	k2eAgInSc1d1	potřebný
počet	počet	k1gInSc1	počet
senátorů	senátor	k1gMnPc2	senátor
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
)	)	kIx)	)
návrh	návrh	k1gInSc4	návrh
žaloby	žaloba	k1gFnSc2	žaloba
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Václava	Václav	k1gMnSc4	Václav
Klause	Klaus	k1gMnSc4	Klaus
především	především	k9	především
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
amnestií	amnestie	k1gFnSc7	amnestie
vyhlášenou	vyhlášený	k2eAgFnSc7d1	vyhlášená
v	v	k7c6	v
novoročním	novoroční	k2eAgInSc6d1	novoroční
projevu	projev	k1gInSc6	projev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
dalšími	další	k2eAgInPc7d1	další
kroky	krok	k1gInPc7	krok
během	během	k7c2	během
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
nedokončování	nedokončování	k1gNnSc1	nedokončování
ratifikace	ratifikace	k1gFnSc2	ratifikace
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
<g/>
,	,	kIx,	,
nenavrhování	nenavrhování	k1gNnSc1	nenavrhování
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
soudce	soudce	k1gMnPc4	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
nerozhodnutí	rozhodnutý	k2eNgMnPc1d1	nerozhodnutý
o	o	k7c6	o
jmenování	jmenování	k1gNnSc6	jmenování
či	či	k8xC	či
nejmenování	nejmenování	k1gNnSc6	nejmenování
Petra	Petr	k1gMnSc2	Petr
Langera	Langer	k1gMnSc2	Langer
soudcem	soudce	k1gMnSc7	soudce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
dopustit	dopustit	k5eAaPmF	dopustit
jednání	jednání	k1gNnSc4	jednání
směřujícího	směřující	k2eAgInSc2d1	směřující
proti	proti	k7c3	proti
svrchovanosti	svrchovanost	k1gFnSc3	svrchovanost
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
proti	proti	k7c3	proti
jejímu	její	k3xOp3gInSc3	její
demokratickému	demokratický	k2eAgInSc3d1	demokratický
řádu	řád	k1gInSc3	řád
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
Senátu	senát	k1gInSc2	senát
doručena	doručit	k5eAaPmNgFnS	doručit
petice	petice	k1gFnSc1	petice
73	[number]	k4	73
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
výzvou	výzva	k1gFnSc7	výzva
k	k	k7c3	k
podání	podání	k1gNnSc3	podání
žaloby	žaloba	k1gFnSc2	žaloba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
návrhu	návrh	k1gInSc2	návrh
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
schůze	schůze	k1gFnSc1	schůze
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
podání	podání	k1gNnPc1	podání
ústavní	ústavní	k2eAgFnSc2d1	ústavní
žaloby	žaloba	k1gFnSc2	žaloba
k	k	k7c3	k
Ústavnímu	ústavní	k2eAgInSc3d1	ústavní
soudu	soud	k1gInSc3	soud
schválila	schválit	k5eAaPmAgFnS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
však	však	k9	však
řízení	řízení	k1gNnSc1	řízení
o	o	k7c6	o
žalobě	žaloba	k1gFnSc6	žaloba
z	z	k7c2	z
procesních	procesní	k2eAgInPc2d1	procesní
důvodů	důvod	k1gInPc2	důvod
zastavil	zastavit	k5eAaPmAgInS	zastavit
<g/>
,	,	kIx,	,
když	když	k8xS	když
dovodil	dovodit	k5eAaPmAgMnS	dovodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
právní	právní	k2eAgFnSc2d1	právní
úpravy	úprava	k1gFnSc2	úprava
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
řízení	řízení	k1gNnSc6	řízení
účinné	účinný	k2eAgNnSc1d1	účinné
před	před	k7c4	před
7	[number]	k4	7
<g/>
.	.	kIx.	.
březnem	březen	k1gInSc7	březen
2013	[number]	k4	2013
nešlo	jít	k5eNaImAgNnS	jít
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
řízení	řízení	k1gNnSc6	řízení
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
žalobě	žaloba	k1gFnSc6	žaloba
po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
pravomoci	pravomoc	k1gFnPc4	pravomoc
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
prezident	prezident	k1gMnSc1	prezident
zcela	zcela	k6eAd1	zcela
nezávisle	závisle	k6eNd1	závisle
<g/>
,	,	kIx,	,
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
vydaná	vydaný	k2eAgNnPc1d1	vydané
jsou	být	k5eAaImIp3nP	být
bezprostředně	bezprostředně	k6eAd1	bezprostředně
platná	platný	k2eAgFnSc1d1	platná
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
podle	podle	k7c2	podle
Ústavy	ústava	k1gFnSc2	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
předsedu	předseda	k1gMnSc4	předseda
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
členy	člen	k1gMnPc4	člen
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
přijímá	přijímat	k5eAaImIp3nS	přijímat
jejich	jejich	k3xOp3gFnSc4	jejich
demisi	demise	k1gFnSc4	demise
<g/>
,	,	kIx,	,
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
přijímá	přijímat	k5eAaImIp3nS	přijímat
její	její	k3xOp3gFnSc4	její
demisi	demise	k1gFnSc4	demise
<g/>
,	,	kIx,	,
svolává	svolávat	k5eAaImIp3nS	svolávat
zasedání	zasedání	k1gNnSc1	zasedání
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
Poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
sněmovnu	sněmovna	k1gFnSc4	sněmovna
<g/>
,	,	kIx,	,
pověřuje	pověřovat	k5eAaImIp3nS	pověřovat
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
demisi	demise	k1gFnSc4	demise
přijal	přijmout	k5eAaPmAgMnS	přijmout
nebo	nebo	k8xC	nebo
kterou	který	k3yRgFnSc4	který
odvolal	odvolat	k5eAaPmAgInS	odvolat
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
vykonáváním	vykonávání	k1gNnSc7	vykonávání
jejích	její	k3xOp3gFnPc2	její
funkcí	funkce	k1gFnPc2	funkce
prozatímně	prozatímně	k6eAd1	prozatímně
až	až	k9	až
do	do	k7c2	do
jmenování	jmenování	k1gNnSc2	jmenování
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
soudce	soudce	k1gMnSc1	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc4	jeho
předsedu	předseda	k1gMnSc4	předseda
a	a	k8xC	a
místopředsedy	místopředseda	k1gMnPc4	místopředseda
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
předsedu	předseda	k1gMnSc4	předseda
a	a	k8xC	a
místopředsedy	místopředseda	k1gMnPc7	místopředseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
soudců	soudce	k1gMnPc2	soudce
tohoto	tento	k3xDgInSc2	tento
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
odpouští	odpouštět	k5eAaImIp3nS	odpouštět
a	a	k8xC	a
zmírňuje	zmírňovat	k5eAaImIp3nS	zmírňovat
tresty	trest	k1gInPc1	trest
uložené	uložený	k2eAgInPc1d1	uložený
soudem	soud	k1gInSc7	soud
(	(	kIx(	(
<g/>
agraciace	agraciace	k1gFnSc1	agraciace
<g/>
)	)	kIx)	)
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
zahlazuje	zahlazovat	k5eAaImIp3nS	zahlazovat
odsouzení	odsouzení	k1gNnSc1	odsouzení
(	(	kIx(	(
<g/>
rehabilitace	rehabilitace	k1gFnSc1	rehabilitace
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
vrátit	vrátit	k5eAaPmF	vrátit
Parlamentu	parlament	k1gInSc3	parlament
přijatý	přijatý	k2eAgInSc1d1	přijatý
zákon	zákon	k1gInSc1	zákon
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
zákona	zákon	k1gInSc2	zákon
ústavního	ústavní	k2eAgInSc2d1	ústavní
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podepisuje	podepisovat	k5eAaImIp3nS	podepisovat
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
prezidenta	prezident	k1gMnSc4	prezident
a	a	k8xC	a
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
členy	člen	k1gMnPc4	člen
Bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
jmenování	jmenování	k1gNnSc3	jmenování
soudců	soudce	k1gMnPc2	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
však	však	k9	však
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
souhlas	souhlas	k1gInSc4	souhlas
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Milost	milost	k1gFnSc1	milost
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
pravomoc	pravomoc	k1gFnSc4	pravomoc
<g/>
,	,	kIx,	,
ideově	ideově	k6eAd1	ideově
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
prezidentovi	prezident	k1gMnSc3	prezident
v	v	k7c6	v
individuálních	individuální	k2eAgInPc6d1	individuální
případech	případ	k1gInPc6	případ
odpouštět	odpouštět	k5eAaImF	odpouštět
a	a	k8xC	a
zmírňovat	zmírňovat	k5eAaImF	zmírňovat
tresty	trest	k1gInPc1	trest
uložené	uložený	k2eAgInPc1d1	uložený
soudem	soud	k1gInSc7	soud
<g/>
;	;	kIx,	;
zahlazovat	zahlazovat	k5eAaImF	zahlazovat
odsouzení	odsouzení	k1gNnSc4	odsouzení
<g/>
;	;	kIx,	;
a	a	k8xC	a
s	s	k7c7	s
kontrasignací	kontrasignace	k1gFnSc7	kontrasignace
nařizovat	nařizovat	k5eAaImF	nařizovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
trestní	trestní	k2eAgNnSc1d1	trestní
řízení	řízení	k1gNnSc1	řízení
nezahajovalo	zahajovat	k5eNaImAgNnS	zahajovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
<g/>
-li	i	k?	-li
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
nepokračovalo	pokračovat	k5eNaImAgNnS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
tuto	tento	k3xDgFnSc4	tento
pravomoc	pravomoc	k1gFnSc4	pravomoc
prezident	prezident	k1gMnSc1	prezident
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
soud	soud	k1gInSc1	soud
pochybil	pochybit	k5eAaPmAgInS	pochybit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
humanitárních	humanitární	k2eAgInPc6d1	humanitární
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
rozdělení	rozdělení	k1gNnSc4	rozdělení
rodiny	rodina	k1gFnSc2	rodina
nebo	nebo	k8xC	nebo
přihlíží	přihlížet	k5eAaImIp3nS	přihlížet
k	k	k7c3	k
vážnému	vážný	k2eAgInSc3d1	vážný
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
stavu	stav	k1gInSc3	stav
odsouzených	odsouzený	k1gMnPc2	odsouzený
nebo	nebo	k8xC	nebo
členů	člen	k1gMnPc2	člen
jejich	jejich	k3xOp3gFnSc2	jejich
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
bývají	bývat	k5eAaImIp3nP	bývat
některá	některý	k3yIgNnPc1	některý
udělení	udělení	k1gNnPc1	udělení
milosti	milost	k1gFnSc2	milost
veřejností	veřejnost	k1gFnSc7	veřejnost
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
<g/>
.	.	kIx.	.
</s>
<s>
Suspenzivní	suspenzivní	k2eAgNnSc1d1	suspenzivní
veto	veto	k1gNnSc1	veto
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
pravomoci	pravomoc	k1gFnPc1	pravomoc
českého	český	k2eAgMnSc2d1	český
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
mu	on	k3xPp3gMnSc3	on
vrátit	vrátit	k5eAaPmF	vrátit
Parlamentem	parlament	k1gInSc7	parlament
schválený	schválený	k2eAgInSc1d1	schválený
návrh	návrh	k1gInSc1	návrh
zákona	zákon	k1gInSc2	zákon
zpět	zpět	k6eAd1	zpět
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
tak	tak	k6eAd1	tak
učinit	učinit	k5eAaPmF	učinit
ve	v	k7c6	v
lhůtě	lhůta	k1gFnSc6	lhůta
15	[number]	k4	15
dnů	den	k1gInPc2	den
počínající	počínající	k2eAgInSc4d1	počínající
den	den	k1gInSc4	den
po	po	k7c6	po
předložení	předložení	k1gNnSc6	předložení
zákona	zákon	k1gInSc2	zákon
k	k	k7c3	k
podpisu	podpis	k1gInSc3	podpis
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
tak	tak	k6eAd1	tak
učinit	učinit	k5eAaImF	učinit
u	u	k7c2	u
ústavních	ústavní	k2eAgInPc2d1	ústavní
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Přehlasovat	přehlasovat	k5eAaBmF	přehlasovat
veto	veto	k1gNnSc4	veto
může	moct	k5eAaImIp3nS	moct
nadpoloviční	nadpoloviční	k2eAgFnSc1d1	nadpoloviční
většina	většina	k1gFnSc1	většina
všech	všecek	k3xTgMnPc2	všecek
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
prezident	prezident	k1gMnSc1	prezident
není	být	k5eNaImIp3nS	být
jistý	jistý	k2eAgMnSc1d1	jistý
ústavností	ústavnost	k1gFnPc2	ústavnost
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
podat	podat	k5eAaPmF	podat
Ústavnímu	ústavní	k2eAgInSc3d1	ústavní
soudu	soud	k1gInSc3	soud
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
zrušení	zrušení	k1gNnSc4	zrušení
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
ovšem	ovšem	k9	ovšem
nabude	nabýt	k5eAaPmIp3nS	nabýt
platnost	platnost	k1gFnSc4	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
prvním	první	k4xOgNnSc6	první
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
použil	použít	k5eAaPmAgInS	použít
veto	veto	k1gNnSc4	veto
jen	jen	k6eAd1	jen
devětkrát	devětkrát	k6eAd1	devětkrát
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
byl	být	k5eAaImAgMnS	být
poslanci	poslanec	k1gMnSc3	poslanec
přehlasován	přehlasován	k2eAgMnSc1d1	přehlasován
<g/>
,	,	kIx,	,
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
se	se	k3xPyFc4	se
tak	tak	k9	tak
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
případě	případ	k1gInSc6	případ
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
platného	platný	k2eAgInSc2d1	platný
zákona	zákon	k1gInSc2	zákon
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
prezidenta	prezident	k1gMnSc2	prezident
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
použil	použít	k5eAaPmAgInS	použít
veto	veto	k1gNnSc4	veto
sedmnáctkrát	sedmnáctkrát	k6eAd1	sedmnáctkrát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
případech	případ	k1gInPc6	případ
však	však	k9	však
byl	být	k5eAaImAgInS	být
poslanci	poslanec	k1gMnSc3	poslanec
přehlasován	přehlasován	k2eAgMnSc1d1	přehlasován
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnější	úspěšný	k2eAgInSc1d2	úspěšnější
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
návrhům	návrh	k1gInPc3	návrh
prezidenta	prezident	k1gMnSc4	prezident
vyhověl	vyhovět	k5eAaPmAgInS	vyhovět
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
veto	veto	k1gNnSc4	veto
používal	používat	k5eAaImAgMnS	používat
častěji	často	k6eAd2	často
<g/>
,	,	kIx,	,
vetoval	vetovat	k5eAaBmAgMnS	vetovat
celkem	celek	k1gInSc7	celek
62	[number]	k4	62
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
uspěl	uspět	k5eAaPmAgMnS	uspět
jen	jen	k9	jen
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
sedm	sedm	k4xCc4	sedm
jeho	jeho	k3xOp3gNnPc2	jeho
vet	veto	k1gNnPc2	veto
již	již	k6eAd1	již
nebylo	být	k5eNaImAgNnS	být
projednáno	projednat	k5eAaPmNgNnS	projednat
<g/>
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
vetoval	vetovat	k5eAaBmAgMnS	vetovat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
prvních	první	k4xOgInPc2	první
19	[number]	k4	19
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
3	[number]	k4	3
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
veto	veto	k1gNnSc1	veto
přehlasováno	přehlasován	k2eAgNnSc1d1	přehlasováno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
hodlal	hodlat	k5eAaImAgMnS	hodlat
obrátit	obrátit	k5eAaPmF	obrátit
na	na	k7c4	na
Ústavní	ústavní	k2eAgInSc4d1	ústavní
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
vydaná	vydaný	k2eAgFnSc1d1	vydaná
podle	podle	k7c2	podle
těchto	tento	k3xDgFnPc2	tento
pravomocí	pravomoc	k1gFnPc2	pravomoc
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
platnosti	platnost	k1gFnSc3	platnost
spolupodpis	spolupodpis	k1gInSc4	spolupodpis
(	(	kIx(	(
<g/>
kontrasignaci	kontrasignace	k1gFnSc4	kontrasignace
<g/>
)	)	kIx)	)
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
nebo	nebo	k8xC	nebo
jím	jíst	k5eAaImIp1nS	jíst
pověřeného	pověřený	k2eAgMnSc4d1	pověřený
člena	člen	k1gMnSc4	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tato	tento	k3xDgNnPc4	tento
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
podle	podle	k7c2	podle
Ústavy	ústava	k1gFnSc2	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
stát	stát	k5eAaImF	stát
navenek	navenek	k6eAd1	navenek
<g/>
,	,	kIx,	,
sjednává	sjednávat	k5eAaImIp3nS	sjednávat
a	a	k8xC	a
ratifikuje	ratifikovat	k5eAaBmIp3nS	ratifikovat
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
;	;	kIx,	;
sjednávání	sjednávání	k1gNnSc4	sjednávání
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
může	moct	k5eAaImIp3nS	moct
přenést	přenést	k5eAaPmF	přenést
na	na	k7c4	na
vládu	vláda	k1gFnSc4	vláda
nebo	nebo	k8xC	nebo
s	s	k7c7	s
jejím	její	k3xOp3gInSc7	její
souhlasem	souhlas	k1gInSc7	souhlas
na	na	k7c4	na
její	její	k3xOp3gMnPc4	její
jednotlivé	jednotlivý	k2eAgMnPc4d1	jednotlivý
členy	člen	k1gMnPc4	člen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
přijímá	přijímat	k5eAaImIp3nS	přijímat
vedoucí	vedoucí	k1gFnSc1	vedoucí
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
zastupitelských	zastupitelský	k2eAgFnPc2d1	zastupitelská
misí	mise	k1gFnPc2	mise
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
pověřuje	pověřovat	k5eAaImIp3nS	pověřovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
vedoucí	vedoucí	k1gFnSc1	vedoucí
zastupitelských	zastupitelský	k2eAgFnPc2d1	zastupitelská
misí	mise	k1gFnPc2	mise
<g/>
,	,	kIx,	,
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
a	a	k8xC	a
povyšuje	povyšovat	k5eAaImIp3nS	povyšovat
generály	generál	k1gMnPc4	generál
<g/>
,	,	kIx,	,
propůjčuje	propůjčovat	k5eAaImIp3nS	propůjčovat
a	a	k8xC	a
uděluje	udělovat	k5eAaImIp3nS	udělovat
státní	státní	k2eAgNnSc1d1	státní
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
<g/>
,	,	kIx,	,
nezmocní	zmocnit	k5eNaPmIp3nS	zmocnit
<g/>
-li	i	k?	-li
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
jiný	jiný	k2eAgInSc4d1	jiný
orgán	orgán	k1gInSc4	orgán
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
nařizuje	nařizovat	k5eAaImIp3nS	nařizovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
trestní	trestní	k2eAgNnSc1d1	trestní
řízení	řízení	k1gNnSc1	řízení
nezahajovalo	zahajovat	k5eNaImAgNnS	zahajovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
<g/>
-li	i	k?	-li
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
nepokračovalo	pokračovat	k5eNaImAgNnS	pokračovat
(	(	kIx(	(
<g/>
abolice	abolice	k1gFnSc1	abolice
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
shora	shora	k6eAd1	shora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
udělovat	udělovat	k5eAaImF	udělovat
amnestii	amnestie	k1gFnSc4	amnestie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
pravomoci	pravomoc	k1gFnPc1	pravomoc
určují	určovat	k5eAaImIp3nP	určovat
běžné	běžný	k2eAgInPc1d1	běžný
zákony	zákon	k1gInPc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
prezident	prezident	k1gMnSc1	prezident
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
pečetidlo	pečetidlo	k1gNnSc1	pečetidlo
státní	státní	k2eAgFnSc2d1	státní
pečeti	pečeť	k1gFnSc2	pečeť
<g/>
,	,	kIx,	,
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
krajů	kraj	k1gInPc2	kraj
a	a	k8xC	a
do	do	k7c2	do
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
předsedu	předseda	k1gMnSc4	předseda
a	a	k8xC	a
místopředsedu	místopředseda	k1gMnSc4	místopředseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
soudců	soudce	k1gMnPc2	soudce
tohoto	tento	k3xDgInSc2	tento
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
ze	z	k7c2	z
soudců	soudce	k1gMnPc2	soudce
předsedy	předseda	k1gMnSc2	předseda
vrchních	vrchní	k1gFnPc2	vrchní
a	a	k8xC	a
krajských	krajský	k2eAgInPc2d1	krajský
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
předsedu	předseda	k1gMnSc4	předseda
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
předsedu	předseda	k1gMnSc4	předseda
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
osobních	osobní	k2eAgInPc2d1	osobní
údajů	údaj	k1gInPc2	údaj
nebo	nebo	k8xC	nebo
předsedu	předseda	k1gMnSc4	předseda
Českého	český	k2eAgInSc2d1	český
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
rektory	rektor	k1gMnPc4	rektor
veřejných	veřejný	k2eAgFnPc2d1	veřejná
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
profesory	profesor	k1gMnPc4	profesor
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
předsedu	předseda	k1gMnSc4	předseda
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Amnestie	amnestie	k1gFnSc2	amnestie
<g/>
.	.	kIx.	.
</s>
<s>
Amnestie	amnestie	k1gFnSc1	amnestie
je	být	k5eAaImIp3nS	být
pravomoc	pravomoc	k1gFnSc4	pravomoc
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hromadně	hromadně	k6eAd1	hromadně
promíjet	promíjet	k5eAaImF	promíjet
a	a	k8xC	a
snižovat	snižovat	k5eAaImF	snižovat
tresty	trest	k1gInPc4	trest
a	a	k8xC	a
právní	právní	k2eAgInPc4d1	právní
následky	následek	k1gInPc4	následek
odsouzení	odsouzení	k1gNnSc2	odsouzení
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
nařizovat	nařizovat	k5eAaImF	nařizovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
trestní	trestní	k2eAgNnSc1d1	trestní
řízení	řízení	k1gNnSc1	řízení
nezahajovalo	zahajovat	k5eNaImAgNnS	zahajovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
<g/>
-li	i	k?	-li
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
nepokračovalo	pokračovat	k5eNaImAgNnS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Amnestie	amnestie	k1gFnSc1	amnestie
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vztahovat	vztahovat	k5eAaImF	vztahovat
i	i	k9	i
na	na	k7c4	na
přestupky	přestupek	k1gInPc4	přestupek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
na	na	k7c4	na
přestupky	přestupek	k1gInPc4	přestupek
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
konkrétně	konkrétně	k6eAd1	konkrétně
o	o	k7c4	o
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
přestupků	přestupek	k1gInPc2	přestupek
a	a	k8xC	a
uděluje	udělovat	k5eAaImIp3nS	udělovat
se	se	k3xPyFc4	se
celoplošně	celoplošně	k6eAd1	celoplošně
<g/>
.	.	kIx.	.
</s>
<s>
Amnestie	amnestie	k1gFnSc1	amnestie
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
může	moct	k5eAaImIp3nS	moct
určit	určit	k5eAaPmF	určit
okruhy	okruh	k1gInPc4	okruh
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
amnestie	amnestie	k1gFnSc1	amnestie
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
okruhy	okruh	k1gInPc4	okruh
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
se	se	k3xPyFc4	se
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
platnosti	platnost	k1gFnSc3	platnost
amnestie	amnestie	k1gFnSc2	amnestie
je	být	k5eAaImIp3nS	být
třeba	třeba	k9	třeba
spolupodpis	spolupodpis	k1gInSc1	spolupodpis
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
nebo	nebo	k8xC	nebo
pověřeného	pověřený	k2eAgMnSc2d1	pověřený
ministra	ministr	k1gMnSc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
jako	jako	k8xC	jako
prezident	prezident	k1gMnSc1	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
udělil	udělit	k5eAaPmAgMnS	udělit
amnestie	amnestie	k1gFnPc4	amnestie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1993	[number]	k4	1993
a	a	k8xC	a
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgInP	týkat
jen	jen	k9	jen
nižšího	nízký	k2eAgInSc2d2	nižší
počtu	počet	k1gInSc2	počet
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
130	[number]	k4	130
a	a	k8xC	a
955	[number]	k4	955
<g/>
)	)	kIx)	)
a	a	k8xC	a
nevzbudily	vzbudit	k5eNaPmAgFnP	vzbudit
takové	takový	k3xDgFnPc1	takový
kontroverze	kontroverze	k1gFnPc1	kontroverze
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
amnestie	amnestie	k1gFnSc1	amnestie
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
prezidenta	prezident	k1gMnSc2	prezident
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
nebylo	být	k5eNaImAgNnS	být
třeba	třeba	k6eAd1	třeba
premiérova	premiérův	k2eAgInSc2d1	premiérův
spolupodpisu	spolupodpis	k1gInSc2	spolupodpis
<g/>
.	.	kIx.	.
</s>
<s>
Amnestie	amnestie	k1gFnSc1	amnestie
byla	být	k5eAaImAgFnS	být
nejrozsáhlejší	rozsáhlý	k2eAgFnSc1d3	nejrozsáhlejší
a	a	k8xC	a
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
mnoho	mnoho	k4c4	mnoho
ohlasů	ohlas	k1gInPc2	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Propuštěno	propustit	k5eAaPmNgNnS	propustit
bylo	být	k5eAaImAgNnS	být
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
,	,	kIx,	,
16	[number]	k4	16
tisícům	tisíc	k4xCgInPc3	tisíc
se	se	k3xPyFc4	se
trest	trest	k1gInSc4	trest
zkrátil	zkrátit	k5eAaPmAgInS	zkrátit
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
Havlovi	Havlův	k2eAgMnPc1d1	Havlův
vyčítají	vyčítat	k5eAaImIp3nP	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tímto	tento	k3xDgNnSc7	tento
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
výrazně	výrazně	k6eAd1	výrazně
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
míru	míra	k1gFnSc4	míra
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
.	.	kIx.	.
</s>
<s>
Trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
propuštěnými	propuštěný	k2eAgInPc7d1	propuštěný
na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
amnestie	amnestie	k1gFnSc2	amnestie
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
amnestovanými	amnestovaný	k2eAgMnPc7d1	amnestovaný
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
9	[number]	k4	9
procent	procento	k1gNnPc2	procento
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
kriminality	kriminalita	k1gFnSc2	kriminalita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
možnosti	možnost	k1gFnSc2	možnost
amnestie	amnestie	k1gFnSc2	amnestie
využil	využít	k5eAaPmAgInS	využít
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
výročí	výročí	k1gNnSc2	výročí
20	[number]	k4	20
let	léto	k1gNnPc2	léto
od	od	k7c2	od
rozdělení	rozdělení	k1gNnSc2	rozdělení
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
posledním	poslední	k2eAgInSc6d1	poslední
prezidentském	prezidentský	k2eAgInSc6d1	prezidentský
novoročním	novoroční	k2eAgInSc6d1	novoroční
projevu	projev	k1gInSc6	projev
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
oznámil	oznámit	k5eAaPmAgMnS	oznámit
amnestii	amnestie	k1gFnSc4	amnestie
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
propuštěno	propustit	k5eAaPmNgNnS	propustit
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
500	[number]	k4	500
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Protesty	protest	k1gInPc1	protest
zejména	zejména	k9	zejména
opozičních	opoziční	k2eAgMnPc2d1	opoziční
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
občanů	občan	k1gMnPc2	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
zejména	zejména	k9	zejména
rozsah	rozsah	k1gInSc1	rozsah
amnestie	amnestie	k1gFnSc2	amnestie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
i	i	k9	i
řady	řada	k1gFnPc1	řada
kauz	kauza	k1gFnPc2	kauza
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
trestné	trestný	k2eAgFnSc2d1	trestná
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
známých	známý	k2eAgFnPc2d1	známá
osobností	osobnost	k1gFnPc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
prezidenta	prezident	k1gMnSc2	prezident
může	moct	k5eAaImIp3nS	moct
zůstat	zůstat	k5eAaPmF	zůstat
neobsazen	obsazen	k2eNgMnSc1d1	neobsazen
<g/>
:	:	kIx,	:
prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
za	za	k7c4	za
velezradu	velezrada	k1gFnSc4	velezrada
nebo	nebo	k8xC	nebo
za	za	k7c4	za
hrubé	hrubý	k2eAgNnSc4d1	hrubé
porušení	porušení	k1gNnSc4	porušení
Ústavy	ústava	k1gFnSc2	ústava
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
součásti	součást	k1gFnSc2	součást
ústavního	ústavní	k2eAgInSc2d1	ústavní
pořádku	pořádek	k1gInSc2	pořádek
<g/>
,	,	kIx,	,
vzdá	vzdát	k5eAaPmIp3nS	vzdát
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
předsedy	předseda	k1gMnSc2	předseda
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
prezidenta	prezident	k1gMnSc2	prezident
skončí	skončit	k5eAaPmIp3nS	skončit
a	a	k8xC	a
nový	nový	k2eAgMnSc1d1	nový
prezident	prezident	k1gMnSc1	prezident
ještě	ještě	k6eAd1	ještě
nesložil	složit	k5eNaPmAgMnS	složit
slib	slib	k1gInSc4	slib
či	či	k8xC	či
složil	složit	k5eAaPmAgMnS	složit
slib	slib	k1gInSc4	slib
s	s	k7c7	s
výhradou	výhrada	k1gFnSc7	výhrada
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgInSc4d1	schopen
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
ze	z	k7c2	z
závažných	závažný	k2eAgInPc2d1	závažný
důvodů	důvod	k1gInPc2	důvod
vykonávat	vykonávat	k5eAaImF	vykonávat
(	(	kIx(	(
<g/>
usnáší	usnášet	k5eAaImIp3nS	usnášet
se	se	k3xPyFc4	se
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
a	a	k8xC	a
Senát	senát	k1gInSc1	senát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
pravomoci	pravomoc	k1gFnSc2	pravomoc
s	s	k7c7	s
kontrasignací	kontrasignace	k1gFnSc7	kontrasignace
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
pravomocí	pravomoc	k1gFnPc2	pravomoc
jmenovat	jmenovat	k5eAaImF	jmenovat
a	a	k8xC	a
povyšovat	povyšovat	k5eAaImF	povyšovat
generály	generál	k1gMnPc4	generál
a	a	k8xC	a
vyhlašovat	vyhlašovat	k5eAaImF	vyhlašovat
parlamentní	parlamentní	k2eAgFnPc4d1	parlamentní
volby	volba	k1gFnPc4	volba
<g/>
)	)	kIx)	)
přecházejí	přecházet	k5eAaImIp3nP	přecházet
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Pravomoci	pravomoc	k1gFnPc1	pravomoc
bez	bez	k7c2	bez
kontrasignace	kontrasignace	k1gFnSc2	kontrasignace
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
jmenování	jmenování	k1gNnSc2	jmenování
předsedy	předseda	k1gMnSc2	předseda
a	a	k8xC	a
místopředsedů	místopředseda	k1gMnPc2	místopředseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
milosti	milost	k1gFnSc2	milost
<g/>
,	,	kIx,	,
suspenzivního	suspenzivní	k2eAgNnSc2d1	suspenzivní
veta	veto	k1gNnSc2	veto
<g/>
,	,	kIx,	,
podepisování	podepisování	k1gNnSc2	podepisování
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
jmenování	jmenování	k1gNnSc4	jmenování
předsedy	předseda	k1gMnSc2	předseda
a	a	k8xC	a
místopředsedy	místopředseda	k1gMnSc2	místopředseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
<g/>
)	)	kIx)	)
přecházejí	přecházet	k5eAaImIp3nP	přecházet
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pravomoci	pravomoc	k1gFnSc2	pravomoc
vyhlašovat	vyhlašovat	k5eAaImF	vyhlašovat
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
(	(	kIx(	(
<g/>
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
v	v	k7c6	v
takových	takový	k3xDgInPc6	takový
případech	případ	k1gInPc6	případ
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
vždy	vždy	k6eAd1	vždy
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
<g/>
,	,	kIx,	,
přísluší	příslušet	k5eAaImIp3nS	příslušet
výkon	výkon	k1gInSc4	výkon
těchto	tento	k3xDgFnPc2	tento
pravomocí	pravomoc	k1gFnPc2	pravomoc
předsedovi	předseda	k1gMnSc3	předseda
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
stavu	stav	k1gInSc2	stav
ohrožení	ohrožení	k1gNnSc2	ohrožení
státu	stát	k1gInSc2	stát
nebo	nebo	k8xC	nebo
válečného	válečný	k2eAgInSc2d1	válečný
stavu	stav	k1gInSc2	stav
může	moct	k5eAaImIp3nS	moct
vláda	vláda	k1gFnSc1	vláda
požádat	požádat	k5eAaPmF	požádat
Parlament	parlament	k1gInSc4	parlament
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
projednal	projednat	k5eAaPmAgInS	projednat
vládní	vládní	k2eAgInSc4d1	vládní
návrh	návrh	k1gInSc4	návrh
zákona	zákon	k1gInSc2	zákon
ve	v	k7c6	v
zkráceném	zkrácený	k2eAgNnSc6d1	zkrácené
jednání	jednání	k1gNnSc6	jednání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
prezident	prezident	k1gMnSc1	prezident
nemá	mít	k5eNaImIp3nS	mít
právo	právo	k1gNnSc4	právo
veta	veto	k1gNnSc2	veto
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
nouzového	nouzový	k2eAgInSc2d1	nouzový
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
stavu	stav	k1gInSc2	stav
ohrožení	ohrožení	k1gNnSc2	ohrožení
státu	stát	k1gInSc2	stát
nebo	nebo	k8xC	nebo
válečného	válečný	k2eAgInSc2d1	válečný
stavu	stav	k1gInSc2	stav
podmínky	podmínka	k1gFnPc4	podmínka
na	na	k7c4	na
území	území	k1gNnSc4	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
neumožní	umožnit	k5eNaPmIp3nS	umožnit
konat	konat	k5eAaImF	konat
volby	volba	k1gFnPc4	volba
ve	v	k7c6	v
lhůtách	lhůta	k1gFnPc6	lhůta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
pro	pro	k7c4	pro
pravidelná	pravidelný	k2eAgNnPc4d1	pravidelné
volební	volební	k2eAgNnPc4d1	volební
období	období	k1gNnPc4	období
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
zákonem	zákon	k1gInSc7	zákon
lhůty	lhůta	k1gFnSc2	lhůta
prodloužit	prodloužit	k5eAaPmF	prodloužit
<g/>
,	,	kIx,	,
nejdéle	dlouho	k6eAd3	dlouho
však	však	k9	však
o	o	k7c4	o
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
prodloužení	prodloužení	k1gNnSc1	prodloužení
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
provést	provést	k5eAaPmF	provést
i	i	k9	i
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
tak	tak	k6eAd1	tak
prodloužit	prodloužit	k5eAaPmF	prodloužit
i	i	k9	i
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Plat	plat	k1gInSc1	plat
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
činí	činit	k5eAaImIp3nS	činit
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
219	[number]	k4	219
tisíc	tisíc	k4xCgInSc4	tisíc
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
složkou	složka	k1gFnSc7	složka
jeho	jeho	k3xOp3gInSc2	jeho
příjmu	příjem	k1gInSc2	příjem
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
víceúčelová	víceúčelový	k2eAgFnSc1d1	víceúčelová
paušální	paušální	k2eAgFnSc1d1	paušální
náhrada	náhrada	k1gFnSc1	náhrada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
(	(	kIx(	(
<g/>
ke	k	k7c3	k
stejnému	stejný	k2eAgNnSc3d1	stejné
datu	datum	k1gNnSc3	datum
<g/>
)	)	kIx)	)
činí	činit	k5eAaImIp3nS	činit
cca	cca	kA	cca
204	[number]	k4	204
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
sloužit	sloužit	k5eAaImF	sloužit
například	například	k6eAd1	například
na	na	k7c4	na
pokrytí	pokrytí	k1gNnSc4	pokrytí
výdajů	výdaj	k1gInPc2	výdaj
na	na	k7c4	na
reprezentaci	reprezentace	k1gFnSc4	reprezentace
<g/>
,	,	kIx,	,
odborné	odborný	k2eAgFnPc4d1	odborná
a	a	k8xC	a
administrativní	administrativní	k2eAgFnPc4d1	administrativní
práce	práce	k1gFnPc4	práce
a	a	k8xC	a
odbornou	odborný	k2eAgFnSc4d1	odborná
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
funkce	funkce	k1gFnSc2	funkce
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
48	[number]	k4	48
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
rentu	renta	k1gFnSc4	renta
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
</s>
<s>
Renta	renta	k1gFnSc1	renta
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
splatná	splatný	k2eAgFnSc1d1	splatná
předem	předem	k6eAd1	předem
vždy	vždy	k6eAd1	vždy
do	do	k7c2	do
pěti	pět	k4xCc2	pět
pracovních	pracovní	k2eAgInPc2d1	pracovní
dnů	den	k1gInPc2	den
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
příslušného	příslušný	k2eAgInSc2d1	příslušný
kalendářního	kalendářní	k2eAgInSc2d1	kalendářní
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
má	mít	k5eAaImIp3nS	mít
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
paušální	paušální	k2eAgFnSc4d1	paušální
náhradu	náhrada	k1gFnSc4	náhrada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kryje	krýt	k5eAaImIp3nS	krýt
zejména	zejména	k9	zejména
nájemné	nájemné	k1gNnSc1	nájemné
kanceláře	kancelář	k1gFnSc2	kancelář
a	a	k8xC	a
odměnu	odměna	k1gFnSc4	odměna
pro	pro	k7c4	pro
asistenta	asistent	k1gMnSc4	asistent
<g/>
,	,	kIx,	,
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
výši	výše	k1gFnSc6	výše
dalších	další	k2eAgInPc2d1	další
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
</s>
<s>
Paušální	paušální	k2eAgFnSc1d1	paušální
náhrada	náhrada	k1gFnSc1	náhrada
je	být	k5eAaImIp3nS	být
splatná	splatný	k2eAgFnSc1d1	splatná
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rentou	renta	k1gFnSc7	renta
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
má	mít	k5eAaImIp3nS	mít
nárok	nárok	k1gInSc4	nárok
i	i	k9	i
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
výhody	výhoda	k1gFnPc4	výhoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xC	jako
požitky	požitek	k1gInPc1	požitek
naturální	naturální	k2eAgInPc1d1	naturální
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
má	mít	k5eAaImIp3nS	mít
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
užívání	užívání	k1gNnSc4	užívání
služebního	služební	k2eAgNnSc2d1	služební
vozidla	vozidlo	k1gNnSc2	vozidlo
s	s	k7c7	s
řidičem	řidič	k1gMnSc7	řidič
i	i	k8xC	i
bez	bez	k7c2	bez
něho	on	k3xPp3gMnSc2	on
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
funkce	funkce	k1gFnSc2	funkce
i	i	k9	i
k	k	k7c3	k
osobní	osobní	k2eAgFnSc3d1	osobní
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
má	mít	k5eAaImIp3nS	mít
nárok	nárok	k1gInSc4	nárok
k	k	k7c3	k
užívání	užívání	k1gNnSc3	užívání
nemovitostí	nemovitost	k1gFnPc2	nemovitost
tvořících	tvořící	k2eAgFnPc2d1	tvořící
sídlo	sídlo	k1gNnSc4	sídlo
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zejména	zejména	k9	zejména
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
Zámek	zámek	k1gInSc4	zámek
Lány	lán	k1gInPc1	lán
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
úhradu	úhrada	k1gFnSc4	úhrada
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
služeb	služba	k1gFnPc2	služba
apod.	apod.	kA	apod.
Doživotní	doživotní	k2eAgFnSc4d1	doživotní
ochranu	ochrana	k1gFnSc4	ochrana
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
i	i	k9	i
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
funkce	funkce	k1gFnSc2	funkce
<g/>
)	)	kIx)	)
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnSc2	republika
Útvar	útvar	k1gInSc4	útvar
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ochranné	ochranný	k2eAgFnSc2d1	ochranná
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
