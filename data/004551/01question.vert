<s>
Jakým	jaký	k3yQgNnSc7	jaký
slovem	slovo	k1gNnSc7	slovo
je	být	k5eAaImIp3nS	být
označována	označován	k2eAgFnSc1d1	označována
pravomoc	pravomoc	k1gFnSc1	pravomoc
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hromadně	hromadně	k6eAd1	hromadně
promíjet	promíjet	k5eAaImF	promíjet
a	a	k8xC	a
snižovat	snižovat	k5eAaImF	snižovat
tresty	trest	k1gInPc4	trest
a	a	k8xC	a
právní	právní	k2eAgInPc4d1	právní
následky	následek	k1gInPc4	následek
odsouzení	odsouzení	k1gNnSc2	odsouzení
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
nařizovat	nařizovat	k5eAaImF	nařizovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
trestní	trestní	k2eAgNnSc1d1	trestní
řízení	řízení	k1gNnSc1	řízení
nezahajovalo	zahajovat	k5eNaImAgNnS	zahajovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
<g/>
-li	i	k?	-li
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
nepokračovalo	pokračovat	k5eNaImAgNnS	pokračovat
<g/>
?	?	kIx.	?
</s>
