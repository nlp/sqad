<s>
Aken	Aken	k1gMnSc1
</s>
<s>
Akenv	Akenv	k1gMnSc1
hieroglyfickém	hieroglyfický	k2eAgInSc6d1
zápisu	zápis	k1gInSc3
</s>
<s>
Aken	Aken	k1gMnSc1
neboli	neboli	k8xC
Aqen	Aqen	k1gMnSc1
byl	být	k5eAaImAgMnS
zřídka	zřídka	k6eAd1
zmiňovaným	zmiňovaný	k2eAgMnSc7d1
staroegyptským	staroegyptský	k2eAgMnSc7d1
bohem	bůh	k1gMnSc7
podsvětí	podsvětí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgInS
zmíněn	zmínit	k5eAaPmNgInS
v	v	k7c6
knize	kniha	k1gFnSc6
mrtvých	mrtvý	k1gMnPc2
z	z	k7c2
období	období	k1gNnSc2
střední	střední	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podsvětí	podsvětí	k1gNnSc6
byl	být	k5eAaImAgInS
ochráncem	ochránce	k1gMnSc7
a	a	k8xC
průvodcem	průvodce	k1gMnSc7
boha	bůh	k1gMnSc2
Slunce	slunce	k1gNnSc2
Rea	Rea	k1gFnSc1
na	na	k7c6
Reově	Reova	k1gFnSc6
nebeské	nebeský	k2eAgFnSc3d1
cestě	cesta	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k9
popisován	popisován	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
“	“	k?
<g/>
ústa	ústa	k1gNnPc4
času	čas	k1gInSc2
<g/>
”	”	k?
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yQgInPc2,k3yRgInPc2,k3yIgInPc2
bohové	bůh	k1gMnPc1
a	a	k8xC
démoni	démon	k1gMnPc1
vytáhli	vytáhnout	k5eAaPmAgMnP
“	“	k?
<g/>
lano	lano	k1gNnSc1
času	čas	k1gInSc2
<g/>
”	”	k?
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
popsáno	popsat	k5eAaPmNgNnS
v	v	k7c6
hrobce	hrobka	k1gFnSc6
krále	král	k1gMnSc4
Setiho	Seti	k1gMnSc4
I.	I.	kA
Byl	být	k5eAaImAgMnS
také	také	k9
patronem	patron	k1gMnSc7
a	a	k8xC
ochráncem	ochránce	k1gMnSc7
lodi	loď	k1gFnSc2
Mesektet	Mesekteta	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
převážela	převážet	k5eAaImAgFnS
mrtvé	mrtvý	k2eAgNnSc4d1
do	do	k7c2
podsvětí	podsvětí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mrtví	mrtvý	k2eAgMnPc1d1
jej	on	k3xPp3gMnSc4
volají	volat	k5eAaImIp3nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
je	on	k3xPp3gInPc4
převezl	převézt	k5eAaPmAgInS
do	do	k7c2
podsvětí	podsvětí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
převozník	převozník	k1gMnSc1
mrtvých	mrtvý	k1gMnPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
předchůdcem	předchůdce	k1gMnSc7
řeckého	řecký	k2eAgNnSc2d1
Chárona	Cháron	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
známa	znám	k2eAgNnPc1d1
další	další	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
<g/>
/	/	kIx~
<g/>
epitetony	epiteton	k1gInPc1
a	a	k8xC
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
všechna	všechen	k3xTgNnPc1
označují	označovat	k5eAaImIp3nP
Akena	Akeen	k2eAgNnPc1d1
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
Jehož	jehož	k3xOyRp3gInSc4
pohled	pohled	k1gInSc4
je	být	k5eAaImIp3nS
za	za	k7c7
ním	on	k3xPp3gInSc7
<g/>
“	“	k?
(	(	kIx(
<g/>
mȝ	mȝ	k?
<g/>
.	.	kIx.
<g/>
f	f	k?
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Jenž	jenž	k3xRgInSc1
hledí	hledět	k5eAaImIp3nS
svým	svůj	k3xOyFgInSc7
obličejem	obličej	k1gInSc7
<g/>
“	“	k?
(	(	kIx(
<g/>
mȝ	mȝ	k1gMnSc1
<g/>
.	.	kIx.
<g/>
f	f	k?
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Jehož	jehož	k3xOyRp3gFnSc4
tvář	tvář	k1gFnSc4
je	být	k5eAaImIp3nS
za	za	k7c7
ním	on	k3xPp3gInSc7
<g/>
“	“	k?
(	(	kIx(
<g/>
ḥ	ḥ	k1gMnSc1
<g/>
.	.	kIx.
<g/>
f-ḥ	f-ḥ	k?
<g/>
.	.	kIx.
<g/>
f	f	k?
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Jehož	jehož	k3xOyRp3gInSc4
obličej	obličej	k1gInSc4
je	být	k5eAaImIp3nS
vpředu	vpředu	k6eAd1
a	a	k8xC
jehož	jehož	k3xOyRp3gInSc4
obličej	obličej	k1gInSc4
je	být	k5eAaImIp3nS
vzadu	vzadu	k6eAd1
<g/>
.	.	kIx.
<g/>
“	“	k?
(	(	kIx(
<g/>
ḥ	ḥ	k1gMnSc1
<g/>
.	.	kIx.
<g/>
f-m-ḫ	f-m-ḫ	k1gMnSc1
<g/>
.	.	kIx.
<g/>
f-ḥ	f-ḥ	k1gMnSc1
<g/>
.	.	kIx.
<g/>
f-m-mḥ	f-m-mḥ	k?
<g/>
.	.	kIx.
<g/>
f	f	k?
<g/>
)	)	kIx)
</s>
<s>
Aken	Aken	k1gMnSc1
neměl	mít	k5eNaImAgMnS
vlastní	vlastní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
kultu	kult	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
by	by	kYmCp3nS
byl	být	k5eAaImAgMnS
uctíván	uctívat	k5eAaImNgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
mnohokrát	mnohokrát	k6eAd1
zmíněn	zmínit	k5eAaPmNgInS
v	v	k7c6
textech	text	k1gInPc6
pyramid	pyramida	k1gFnPc2
<g/>
,	,	kIx,
textech	text	k1gInPc6
rakví	rakev	k1gFnPc2
a	a	k8xC
knize	kniha	k1gFnSc3
mrtvých	mrtvý	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohdy	mnohdy	k6eAd1
je	být	k5eAaImIp3nS
asociován	asociován	k2eAgMnSc1d1
s	s	k7c7
bohem	bůh	k1gMnSc7
Chertim	Chertima	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Egyptští	egyptský	k2eAgMnPc1d1
bohové	bůh	k1gMnPc1
Osmero	Osmero	k1gNnSc1
</s>
<s>
Amon	Amon	k1gMnSc1
•	•	k?
Amaunet	Amaunet	k1gMnSc1
•	•	k?
Heh	heh	k0
•	•	k?
Hauhet	Hauhet	k1gInSc1
•	•	k?
Kuk	kuk	k1gInSc1
•	•	k?
Kauket	Kauket	k1gMnSc1
•	•	k?
Nun	Nun	k1gMnSc1
•	•	k?
Naunet	Naunet	k1gMnSc1
Devatero	devatero	k1gNnSc1
</s>
<s>
Atum	Atum	k1gMnSc1
•	•	k?
Šu	Šu	k1gMnSc1
•	•	k?
Tefnut	Tefnut	k1gMnSc1
•	•	k?
Geb	Geb	k1gMnSc1
•	•	k?
Nút	Nút	k1gMnSc1
•	•	k?
Usir	Usir	k1gMnSc1
•	•	k?
Eset	Eset	k1gMnSc1
•	•	k?
Sutech	Sutech	k1gInSc1
•	•	k?
Nebthet	Nebthet	k1gInSc1
</s>
<s>
Aken	Aken	k1gMnSc1
•	•	k?
Aker	Aker	k1gMnSc1
•	•	k?
Achty	acht	k1gInPc1
•	•	k?
Amentet	Amentet	k1gInSc1
•	•	k?
Amenre	Amenr	k1gInSc5
•	•	k?
Amemait	Amemait	k1gMnSc1
•	•	k?
Am-Heh	Am-Heh	k1gMnSc1
•	•	k?
Anat	Anat	k1gMnSc1
•	•	k?
Andžety	Andžeta	k1gFnSc2
•	•	k?
Anput	Anput	k1gMnSc1
•	•	k?
Anti	Ant	k1gFnSc2
•	•	k?
Anuket	Anuket	k1gMnSc1
•	•	k?
Anup	Anup	k1gMnSc1
•	•	k?
Apet	Apet	k1gMnSc1
•	•	k?
Apis	Apis	k1gInSc1
•	•	k?
Apop	Apop	k1gInSc1
•	•	k?
Aš	Aš	k1gInSc1
•	•	k?
Aštoret	Aštoret	k1gInSc1
•	•	k?
Aton	Aton	k1gInSc1
•	•	k?
Ba-pef	Ba-pef	k1gInSc1
•	•	k?
Babi	Babi	k?
•	•	k?
Banebdžedet	Banebdžedet	k1gMnSc1
•	•	k?
Bat	Bat	k1gMnSc1
•	•	k?
Bata	Bata	k1gMnSc1
•	•	k?
Bastet	Bastet	k1gMnSc1
•	•	k?
Behdetej	Behdetej	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Benu	Ben	k1gInSc2
•	•	k?
Bes	Bes	k1gMnSc1
•	•	k?
Cenenet	Cenenet	k1gMnSc1
•	•	k?
Dedun	Dedun	k1gMnSc1
•	•	k?
Dua	duo	k1gNnSc2
•	•	k?
Duamutef	Duamutef	k1gMnSc1
•	•	k?
Dunanuej	Dunanuej	k1gInSc1
•	•	k?
Fetektej	Fetektej	k1gFnSc2
•	•	k?
Gerh	Gerh	k1gInSc1
•	•	k?
Ha	ha	kA
•	•	k?
Hapi	Hap	k1gFnSc2
(	(	kIx(
<g/>
bůh	bůh	k1gMnSc1
záplav	záplava	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
Hapi	Hap	k1gMnPc1
(	(	kIx(
<g/>
syn	syn	k1gMnSc1
Hora	Hora	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Harachtej	Harachtej	k1gMnSc1
•	•	k?
Haremachet	Haremachet	k1gMnSc1
•	•	k?
Hathor	Hathor	k1gMnSc1
•	•	k?
Hatmehit	Hatmehit	k1gMnSc1
•	•	k?
Hededet	Hededet	k1gMnSc1
•	•	k?
Hedžhotep	Hedžhotep	k1gMnSc1
•	•	k?
Heka	Heka	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Heket	Heket	k1gMnSc1
•	•	k?
Hemen	Hemen	k1gInSc1
•	•	k?
Hemsut	Hemsut	k1gInSc1
•	•	k?
Hermanubis	Hermanubis	k1gInSc1
•	•	k?
Herišef	Herišef	k1gInSc1
•	•	k?
Hesat	Hesat	k1gInSc1
•	•	k?
Hor	hora	k1gFnPc2
•	•	k?
Huh	huh	k0
•	•	k?
Chentiamenti	Chentiament	k1gMnPc1
•	•	k?
Cheprer	Cheprer	k1gInSc1
•	•	k?
Chnum	Chnum	k1gInSc1
•	•	k?
Chonsu	Chons	k1gInSc2
•	•	k?
Ihi	ihi	k0
•	•	k?
Imhotep	Imhotep	k1gMnSc1
•	•	k?
Imset	Imset	k1gMnSc1
•	•	k?
Inmutef	Inmutef	k1gMnSc1
•	•	k?
Isdes	Isdes	k1gMnSc1
•	•	k?
Jah	Jah	k1gFnSc2
•	•	k?
Jat	jat	k2eAgInSc4d1
•	•	k?
Kadeš	Kadeš	k1gMnSc7
•	•	k?
Kamutef	Kamutef	k1gMnSc1
•	•	k?
Kebehsenuf	Kebehsenuf	k1gMnSc1
•	•	k?
Kebhut	Kebhut	k1gMnSc1
•	•	k?
Kematef	Kematef	k1gMnSc1
•	•	k?
Maat	Maat	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Mahes	Mahes	k1gMnSc1
•	•	k?
Mandulis	Mandulis	k1gFnSc2
•	•	k?
Medžed	Medžed	k1gMnSc1
•	•	k?
Mefdet	Mefdet	k1gMnSc1
•	•	k?
Mehen	Mehen	k1gInSc1
•	•	k?
Mehet	Mehet	k1gMnSc1
•	•	k?
Mehetveret	Mehetveret	k1gMnSc1
•	•	k?
Menhit	Menhit	k1gMnSc1
•	•	k?
Menket	Menket	k1gMnSc1
•	•	k?
Merseger	Merseger	k1gMnSc1
•	•	k?
Mert	Mert	k1gMnSc1
•	•	k?
Mešent	Mešent	k1gMnSc1
•	•	k?
Min	min	kA
•	•	k?
Moncu	Moncus	k1gInSc2
•	•	k?
Mut	Mut	k?
•	•	k?
Nechbet	Nechbet	k1gMnSc1
•	•	k?
Neit	Neit	k1gMnSc1
•	•	k?
Neper	prát	k5eNaImRp2nS
•	•	k?
Niau	Nia	k1gMnSc3
•	•	k?
Onhuret	Onhuret	k1gMnSc1
•	•	k?
Osmero	Osmero	k1gNnSc4
•	•	k?
Pachet	Pachet	k1gMnSc1
•	•	k?
Pataikos	Pataikos	k1gMnSc1
•	•	k?
Ptah	Ptah	k1gMnSc1
•	•	k?
Re	re	k9
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Renenutet	Renenutet	k1gMnSc1
•	•	k?
Rešef	Rešef	k1gMnSc1
•	•	k?
Rutej	Rutej	k1gInSc1
•	•	k?
Sah	Sah	k1gFnSc2
•	•	k?
Sachmet	Sachmet	k1gMnSc1
•	•	k?
Satet	Satet	k1gMnSc1
•	•	k?
Selket	Selket	k1gMnSc1
•	•	k?
Sep	Sep	k1gMnSc1
•	•	k?
Serapis	Serapis	k1gFnSc2
•	•	k?
Seret	Seret	k1gMnSc1
•	•	k?
Sešet	Sešet	k1gMnSc1
•	•	k?
Sia	Sia	k1gMnSc1
•	•	k?
Sobek	Sobek	k1gMnSc1
•	•	k?
Sokar	Sokar	k1gMnSc1
•	•	k?
Sopd	Sopd	k1gMnSc1
•	•	k?
Sopdet	Sopdet	k1gInSc1
•	•	k?
Sopdu	Sopd	k1gInSc2
•	•	k?
Šaj	Šaj	k1gMnSc1
•	•	k?
Šed	šedo	k1gNnPc2
•	•	k?
Šesmu	Šesm	k1gInSc6
•	•	k?
Tait	Tait	k1gInSc1
•	•	k?
Tatenen	Tatenen	k1gInSc1
•	•	k?
Tenemu	Tenem	k1gInSc2
•	•	k?
Tenenet	Tenenet	k1gMnSc1
•	•	k?
Thovt	Thovt	k1gMnSc1
•	•	k?
Tveret	Tveret	k1gMnSc1
•	•	k?
Vadžet	Vadžet	k1gMnSc1
•	•	k?
Veneg	Veneg	k1gMnSc1
•	•	k?
Venvet	Venvet	k1gMnSc1
•	•	k?
Vepvovet	Vepvovet	k1gInSc1
•	•	k?
Verethekau	Verethekaus	k1gInSc2
•	•	k?
Vosret	Vosret	k1gInSc1
Seznam	seznam	k1gInSc1
bohů	bůh	k1gMnPc2
•	•	k?
Kategorie	kategorie	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Starověký	starověký	k2eAgInSc1d1
Egypt	Egypt	k1gInSc1
</s>
