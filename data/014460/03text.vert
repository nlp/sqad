<s>
Stlačený	stlačený	k2eAgInSc1d1
zemní	zemní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
dlouhá	dlouhý	k2eAgFnSc1d1
úvodní	úvodní	k2eAgFnSc1d1
sekce	sekce	k1gFnSc1
(	(	kIx(
<g/>
rozdělit	rozdělit	k5eAaPmF
<g/>
)	)	kIx)
</s>
<s>
Irisbus	Irisbus	k1gInSc1
Citelis	Citelis	k1gFnSc2
12M	12M	k4
s	s	k7c7
pohonem	pohon	k1gInSc7
na	na	k7c6
CNG	CNG	kA
(	(	kIx(
<g/>
všimněte	všimnout	k5eAaPmRp2nP
si	se	k3xPyFc3
nádrží	nádrž	k1gFnPc2
na	na	k7c4
CNG	CNG	kA
na	na	k7c6
střeše	střecha	k1gFnSc6
vozu	vůz	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
CNG	CNG	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Compressed	Compressed	k1gMnSc1
Natural	Natural	k?
Gas	Gas	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
stlačený	stlačený	k2eAgInSc1d1
zemní	zemní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
(	(	kIx(
<g/>
metan	metan	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
palivo	palivo	k1gNnSc1
pro	pro	k7c4
pohon	pohon	k1gInSc4
motorových	motorový	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
relativně	relativně	k6eAd1
čistější	čistý	k2eAgFnSc4d2
alternativu	alternativa	k1gFnSc4
k	k	k7c3
benzínu	benzín	k1gInSc3
a	a	k8xC
motorové	motorový	k2eAgFnSc3d1
naftě	nafta	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
k	k	k7c3
LPG	LPG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Variantou	varianta	k1gFnSc7
metanu	metan	k1gInSc2
jako	jako	k8xC,k8xS
paliva	palivo	k1gNnSc2
je	být	k5eAaImIp3nS
LNG	LNG	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
zkapalněný	zkapalněný	k2eAgInSc1d1
zemní	zemní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
CNG	CNG	kA
je	být	k5eAaImIp3nS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
LPG	LPG	kA
součástí	součást	k1gFnPc2
koncepce	koncepce	k1gFnSc1
ministerstva	ministerstvo	k1gNnSc2
dopravy	doprava	k1gFnSc2
ČR	ČR	kA
na	na	k7c4
podporu	podpora	k1gFnSc4
ekologických	ekologický	k2eAgNnPc2d1
paliv	palivo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podpora	podpora	k1gFnSc1
a	a	k8xC
rozvoj	rozvoj	k1gInSc1
infrastruktury	infrastruktura	k1gFnSc2
pro	pro	k7c4
CNG	CNG	kA
bude	být	k5eAaImBp3nS
v	v	k7c6
nejbližších	blízký	k2eAgInPc6d3
letech	let	k1gInPc6
směřovat	směřovat	k5eAaImF
k	k	k7c3
masivnějšímu	masivní	k2eAgNnSc3d2
nasazení	nasazení	k1gNnSc3
osobních	osobní	k2eAgInPc2d1
<g/>
,	,	kIx,
nákladních	nákladní	k2eAgInPc2d1
aj.	aj.	kA
užitkových	užitkový	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
(	(	kIx(
<g/>
autobusy	autobus	k1gInPc1
<g/>
,	,	kIx,
manipulační	manipulační	k2eAgFnSc1d1
technika	technika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dle	dle	k7c2
současného	současný	k2eAgNnSc2d1
stanoviska	stanovisko	k1gNnSc2
až	až	k6eAd1
k	k	k7c3
10	#num#	k4
<g/>
%	%	kIx~
podílu	podíl	k1gInSc2
CNG	CNG	kA
vozidel	vozidlo	k1gNnPc2
na	na	k7c6
celkovém	celkový	k2eAgInSc6d1
počtu	počet	k1gInSc6
vozidel	vozidlo	k1gNnPc2
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
zdroje	zdroj	k1gInPc4
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
koncepce	koncepce	k1gFnSc1
je	být	k5eAaImIp3nS
zmanipulovaná	zmanipulovaný	k2eAgFnSc1d1
a	a	k8xC
znevýhodňuje	znevýhodňovat	k5eAaImIp3nS
účelově	účelově	k6eAd1
dosud	dosud	k6eAd1
rozšířenější	rozšířený	k2eAgMnSc1d2
LPG	LPG	kA
(	(	kIx(
<g/>
propan-butan	propan-butan	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jiné	jiný	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
odkazují	odkazovat	k5eAaImIp3nP
více	hodně	k6eAd2
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
důsledkem	důsledek	k1gInSc7
spojitosti	spojitost	k1gFnSc2
zpracování	zpracování	k1gNnSc2
ropy	ropa	k1gFnSc2
a	a	k8xC
výroby	výroba	k1gFnSc2
LPG	LPG	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přestavba	přestavba	k1gFnSc1
motorů	motor	k1gInPc2
na	na	k7c6
CNG	CNG	kA
z	z	k7c2
dieselových	dieselový	k2eAgInPc2d1
motorů	motor	k1gInPc2
(	(	kIx(
<g/>
ze	z	k7c2
vznětových	vznětový	k2eAgMnPc2d1
na	na	k7c4
zážehové	zážehový	k2eAgMnPc4d1
<g/>
)	)	kIx)
snižuje	snižovat	k5eAaImIp3nS
emise	emise	k1gFnSc1
na	na	k7c4
úroveň	úroveň	k1gFnSc4
normy	norma	k1gFnSc2
Euro	euro	k1gNnSc1
5	#num#	k4
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc2
dodatečného	dodatečný	k2eAgNnSc2d1
čištění	čištění	k1gNnSc2
výfukových	výfukový	k2eAgInPc2d1
plynů	plyn	k1gInPc2
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
nespálených	spálený	k2eNgInPc2d1
uhlovodíků	uhlovodík	k1gInPc2
(	(	kIx(
<g/>
NMHC	NMHC	kA
-	-	kIx~
Non-Methane	Non-Methan	k1gMnSc5
Hydrocarbons	Hydrocarbonsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
obsah	obsah	k1gInSc1
naopak	naopak	k6eAd1
naroste	narůst	k5eAaPmIp3nS
a	a	k8xC
bývá	bývat	k5eAaImIp3nS
proto	proto	k8xC
pro	pro	k7c4
takové	takový	k3xDgInPc4
motory	motor	k1gInPc4
legislativně	legislativně	k6eAd1
omezen	omezen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
paliva	palivo	k1gNnSc2
je	být	k5eAaImIp3nS
o	o	k7c4
něco	něco	k3yInSc4
nižší	nízký	k2eAgFnSc1d2
než	než	k8xS
cena	cena	k1gFnSc1
nafty	nafta	k1gFnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
provoz	provoz	k1gInSc4
na	na	k7c6
CNG	CNG	kA
ekonomicky	ekonomicky	k6eAd1
výhodnější	výhodný	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měrná	měrný	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
CNG	CNG	kA
na	na	k7c6
plnicích	plnicí	k2eAgFnPc6d1
stanicích	stanice	k1gFnPc6
je	být	k5eAaImIp3nS
kilogram	kilogram	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
kg	kg	kA
CNG	CNG	kA
odpovídá	odpovídat	k5eAaImIp3nS
cca	cca	kA
1,4	1,4	k4
l	l	kA
benzínu	benzín	k1gInSc2
a	a	k8xC
cca	cca	kA
1,3	1,3	k4
l	l	kA
motorové	motorový	k2eAgFnSc2d1
nafty	nafta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
přepočtu	přepočet	k1gInSc6
na	na	k7c4
stejné	stejný	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
(	(	kIx(
<g/>
převod	převod	k1gInSc4
1	#num#	k4
kg	kg	kA
CNG	CNG	kA
na	na	k7c4
cca	cca	kA
1,4	1,4	k4
l	l	kA
benzínu	benzín	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
CNG	CNG	kA
zatím	zatím	k6eAd1
nejlevnějším	levný	k2eAgNnSc7d3
palivem	palivo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
je	být	k5eAaImIp3nS
CNG	CNG	kA
zatíženo	zatížit	k5eAaPmNgNnS
nižší	nízký	k2eAgFnSc7d2
spotřební	spotřební	k2eAgFnSc7d1
daní	daň	k1gFnSc7
než	než	k8xS
jiná	jiný	k2eAgNnPc1d1
paliva	palivo	k1gNnPc1
a	a	k8xC
vozidla	vozidlo	k1gNnPc1
a	a	k8xC
celé	celý	k2eAgFnPc4d1
flotily	flotila	k1gFnPc4
s	s	k7c7
pohonem	pohon	k1gInSc7
na	na	k7c6
CNG	CNG	kA
mohou	moct	k5eAaImIp3nP
uplatňovat	uplatňovat	k5eAaImF
osvobození	osvobození	k1gNnSc4
od	od	k7c2
silniční	silniční	k2eAgFnSc2d1
daně	daň	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spotřební	spotřební	k2eAgFnSc1d1
daň	daň	k1gFnSc1
z	z	k7c2
CNG	CNG	kA
<g/>
:	:	kIx,
</s>
<s>
2007	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
sazba	sazba	k1gFnSc1
0	#num#	k4
Kč	Kč	kA
<g/>
/	/	kIx~
<g/>
t	t	k?
(	(	kIx(
<g/>
0	#num#	k4
Kč	Kč	kA
<g/>
/	/	kIx~
<g/>
m³	m³	k?
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
sazba	sazba	k1gFnSc1
500	#num#	k4
Kč	Kč	kA
<g/>
/	/	kIx~
<g/>
t	t	k?
(	(	kIx(
<g/>
0,36	0,36	k4
Kč	Kč	kA
<g/>
/	/	kIx~
<g/>
m³	m³	k?
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
2015	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
sazba	sazba	k1gFnSc1
1	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
/	/	kIx~
<g/>
t	t	k?
(	(	kIx(
<g/>
0,7	0,7	k4
Kč	Kč	kA
<g/>
/	/	kIx~
<g/>
m³	m³	k?
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
2018	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
sazba	sazba	k1gFnSc1
2	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
/	/	kIx~
<g/>
t	t	k?
(	(	kIx(
<g/>
1,4	1,4	k4
Kč	Kč	kA
<g/>
/	/	kIx~
<g/>
m³	m³	k?
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
od	od	k7c2
2020	#num#	k4
-	-	kIx~
sazba	sazba	k1gFnSc1
3	#num#	k4
355	#num#	k4
Kč	Kč	kA
<g/>
/	/	kIx~
<g/>
t	t	k?
(	(	kIx(
<g/>
2,36	2,36	k4
Kč	Kč	kA
<g/>
/	/	kIx~
<g/>
m³	m³	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
resp.	resp.	kA
minimální	minimální	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
dle	dle	k7c2
EU	EU	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
provozu	provoz	k1gInSc2
z	z	k7c2
rozdílu	rozdíl	k1gInSc2
ceny	cena	k1gFnSc2
paliva	palivo	k1gNnSc2
je	být	k5eAaImIp3nS
proměnlivá	proměnlivý	k2eAgFnSc1d1
(	(	kIx(
<g/>
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
vývoji	vývoj	k1gInSc6
ceny	cena	k1gFnSc2
paliv	palivo	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
další	další	k2eAgNnPc4d1
kritéria	kritérion	k1gNnPc4
pro	pro	k7c4
posouzení	posouzení	k1gNnSc4
vhodnosti	vhodnost	k1gFnSc2
změny	změna	k1gFnSc2
paliva	palivo	k1gNnSc2
na	na	k7c6
CNG	CNG	kA
je	být	k5eAaImIp3nS
rozdíl	rozdíl	k1gInSc1
mezi	mezi	k7c7
průměrnou	průměrný	k2eAgFnSc7d1
spotřebou	spotřeba	k1gFnSc7
původního	původní	k2eAgNnSc2d1
paliva	palivo	k1gNnSc2
a	a	k8xC
CNG	CNG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spotřeba	spotřeba	k1gFnSc1
benzínu	benzín	k1gInSc2
nebo	nebo	k8xC
motorové	motorový	k2eAgFnSc2d1
nafty	nafta	k1gFnSc2
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
vyšší	vysoký	k2eAgFnSc1d2
o	o	k7c4
několik	několik	k4yIc4
procent	procento	k1gNnPc2
(	(	kIx(
<g/>
zde	zde	k6eAd1
hraje	hrát	k5eAaImIp3nS
roli	role	k1gFnSc4
více	hodně	k6eAd2
faktorů	faktor	k1gInPc2
<g/>
,	,	kIx,
mj.	mj.	kA
i	i	k9
styl	styl	k1gInSc4
jízdy	jízda	k1gFnSc2
a	a	k8xC
kvalita	kvalita	k1gFnSc1
péče	péče	k1gFnSc2
o	o	k7c4
motor	motor	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
než	než	k8xS
spotřeba	spotřeba	k1gFnSc1
CNG	CNG	kA
<g/>
.	.	kIx.
</s>
<s>
Nevýhodami	nevýhoda	k1gFnPc7
provozu	provoz	k1gInSc2
CNG	CNG	kA
vozidla	vozidlo	k1gNnPc1
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
zmenšení	zmenšení	k1gNnSc1
zavazadlového	zavazadlový	k2eAgInSc2d1
nebo	nebo	k8xC
nákladového	nákladový	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgFnP
nádrže	nádrž	k1gFnPc1
na	na	k7c4
CNG	CNG	kA
<g/>
,	,	kIx,
</s>
<s>
řidší	řídký	k2eAgFnSc1d2
síť	síť	k1gFnSc1
čerpacích	čerpací	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
v	v	k7c6
Česku	Česko	k1gNnSc6
je	být	k5eAaImIp3nS
povolen	povolen	k2eAgInSc1d1
vjezd	vjezd	k1gInSc1
vozidlům	vozidlo	k1gNnPc3
s	s	k7c7
nádržemi	nádrž	k1gFnPc7
na	na	k7c6
CNG	CNG	kA
do	do	k7c2
podzemních	podzemní	k2eAgFnPc2d1
garáží	garáž	k1gFnPc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
mají	mít	k5eAaImIp3nP
garáže	garáž	k1gFnSc2
nainstalována	nainstalován	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
pro	pro	k7c4
detekci	detekce	k1gFnSc4
úniku	únik	k1gInSc2
rizikových	rizikový	k2eAgInPc2d1
plynů	plyn	k1gInPc2
a	a	k8xC
systém	systém	k1gInSc1
pro	pro	k7c4
odvětrávání	odvětrávání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinak	jinak	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vjezd	vjezd	k1gInSc1
LPG	LPG	kA
<g/>
/	/	kIx~
<g/>
CNG	CNG	kA
vozidel	vozidlo	k1gNnPc2
do	do	k7c2
těchto	tento	k3xDgInPc2
objektů	objekt	k1gInPc2
zakázán	zakázán	k2eAgMnSc1d1
<g/>
,	,	kIx,
</s>
<s>
výkon	výkon	k1gInSc1
motoru	motor	k1gInSc2
při	při	k7c6
pohonu	pohon	k1gInSc6
na	na	k7c4
CNG	CNG	kA
může	moct	k5eAaImIp3nS
při	při	k7c6
dodatečné	dodatečný	k2eAgFnSc6d1
montáži	montáž	k1gFnSc6
klesnout	klesnout	k5eAaPmF
o	o	k7c4
5	#num#	k4
–	–	k?
10	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
zejména	zejména	k9
u	u	k7c2
atmosférických	atmosférický	k2eAgInPc2d1
motorů	motor	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
jízdní	jízdní	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
vozidla	vozidlo	k1gNnSc2
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
díky	díky	k7c3
těžkým	těžký	k2eAgFnPc3d1
nádržím	nádrž	k1gFnPc3
významně	významně	k6eAd1
změnit	změnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roli	role	k1gFnSc4
zde	zde	k6eAd1
hrají	hrát	k5eAaImIp3nP
umístění	umístění	k1gNnSc4
nádrží	nádrž	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
zatížení	zatížení	k1gNnPc4
zadní	zadní	k2eAgFnSc2d1
nápravy	náprava	k1gFnSc2
při	při	k7c6
umístění	umístění	k1gNnSc6
nádrží	nádrž	k1gFnPc2
do	do	k7c2
kufru	kufr	k1gInSc2
nebo	nebo	k8xC
místo	místo	k7c2
rezervního	rezervní	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
vede	vést	k5eAaImIp3nS
k	k	k7c3
horší	zlý	k2eAgFnSc3d2
přilnavosti	přilnavost	k1gFnSc3
předních	přední	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
jsou	být	k5eAaImIp3nP
většinou	většinou	k6eAd1
hnaná	hnaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
typ	typ	k1gInSc1
použité	použitý	k2eAgFnSc2d1
nádrže	nádrž	k1gFnSc2
(	(	kIx(
<g/>
ocelová	ocelový	k2eAgFnSc1d1
-	-	kIx~
těžší	těžký	k2eAgFnSc1d2
<g/>
,	,	kIx,
kompozitní	kompozitní	k2eAgInSc1d1
-	-	kIx~
lehčí	lehký	k2eAgMnSc1d2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
ekonomicky	ekonomicky	k6eAd1
výhodný	výhodný	k2eAgInSc1d1
dojezd	dojezd	k1gInSc1
na	na	k7c6
CNG	CNG	kA
je	být	k5eAaImIp3nS
kratší	krátký	k2eAgNnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
dojezd	dojezd	k1gInSc1
na	na	k7c4
původní	původní	k2eAgNnSc4d1
palivo	palivo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
častěji	často	k6eAd2
tankovat	tankovat	k5eAaImF
(	(	kIx(
<g/>
tato	tento	k3xDgFnSc1
nevýhoda	nevýhoda	k1gFnSc1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
odstranit	odstranit	k5eAaPmF
instalací	instalace	k1gFnPc2
objemnějších	objemný	k2eAgFnPc2d2
nádrží	nádrž	k1gFnPc2
na	na	k7c6
CNG	CNG	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
mírně	mírně	k6eAd1
dražší	drahý	k2eAgInSc1d2
servis	servis	k1gInSc1
–	–	k?
periodicky	periodicky	k6eAd1
jednou	jeden	k4xCgFnSc7
za	za	k7c4
2	#num#	k4
roky	rok	k1gInPc4
je	být	k5eAaImIp3nS
povinná	povinný	k2eAgFnSc1d1
kontrola	kontrola	k1gFnSc1
těsnosti	těsnost	k1gFnSc2
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
kontroly	kontrola	k1gFnSc2
se	se	k3xPyFc4
různí	různit	k5eAaImIp3nP
dle	dle	k7c2
servisního	servisní	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
pokynů	pokyn	k1gInPc2
výrobce	výrobce	k1gMnSc2
tlakového	tlakový	k2eAgInSc2d1
zásobníku	zásobník	k1gInSc2
je	být	k5eAaImIp3nS
povinná	povinný	k2eAgFnSc1d1
také	také	k9
revizní	revizní	k2eAgFnSc1d1
tlaková	tlakový	k2eAgFnSc1d1
zkouška	zkouška	k1gFnSc1
(	(	kIx(
<g/>
interval	interval	k1gInSc1
je	být	k5eAaImIp3nS
10	#num#	k4
až	až	k9
20	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozsah	rozsah	k1gInSc1
běžného	běžný	k2eAgInSc2d1
servisu	servis	k1gInSc2
vozidel	vozidlo	k1gNnPc2
je	být	k5eAaImIp3nS
jinak	jinak	k6eAd1
shodný	shodný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
u	u	k7c2
téhož	týž	k3xTgNnSc2
vozidla	vozidlo	k1gNnSc2
s	s	k7c7
benzínovou	benzínový	k2eAgFnSc7d1
motorizací	motorizace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
u	u	k7c2
některých	některý	k3yIgInPc2
modelů	model	k1gInPc2
továrních	tovární	k2eAgInPc2d1
CNG	CNG	kA
zástaveb	zástavba	k1gFnPc2
VW	VW	kA
group	group	k1gInSc1
je	být	k5eAaImIp3nS
známa	znám	k2eAgFnSc1d1
výrobní	výrobní	k2eAgFnSc1d1
vada	vada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konkrétní	konkrétní	k2eAgFnSc1d1
šarže	šarže	k1gFnSc1
modelů	model	k1gInPc2
VW	VW	kA
Caddy	Cadda	k1gFnSc2
<g/>
/	/	kIx~
<g/>
Passat	Passat	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Touran	Touran	k1gInSc1
byly	být	k5eAaImAgFnP
navrženy	navrhnout	k5eAaPmNgInP
a	a	k8xC
vyrobeny	vyrobit	k5eAaPmNgInP
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
mezi	mezi	k7c7
ocelovým	ocelový	k2eAgInSc7d1
zásobníkem	zásobník	k1gInSc7
a	a	k8xC
objímkou	objímka	k1gFnSc7
<g/>
,	,	kIx,
držící	držící	k2eAgInSc1d1
zásobník	zásobník	k1gInSc1
na	na	k7c6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
galvanické	galvanický	k2eAgFnSc3d1
korozi	koroze	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důsledkem	důsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
zeslabení	zeslabení	k1gNnSc1
pláště	plášť	k1gInSc2
tlakového	tlakový	k2eAgInSc2d1
zásobníku	zásobník	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
mohlo	moct	k5eAaImAgNnS
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
mělo	mít	k5eAaImAgNnS
<g/>
,	,	kIx,
za	za	k7c4
následek	následek	k1gInSc4
roztržení	roztržení	k1gNnSc2
během	během	k7c2
tankování	tankování	k1gNnSc2
(	(	kIx(
<g/>
tlak	tlak	k1gInSc1
v	v	k7c6
nádrži	nádrž	k1gFnSc6
může	moct	k5eAaImIp3nS
běžně	běžně	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
hodnot	hodnota	k1gFnPc2
přes	přes	k7c4
200	#num#	k4
bar	bar	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
několika	několik	k4yIc2
případech	případ	k1gInPc6
byly	být	k5eAaImAgFnP
tyto	tento	k3xDgFnPc1
nehody	nehoda	k1gFnPc1
smrtelné	smrtelný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Automobilka	automobilka	k1gFnSc1
spustila	spustit	k5eAaPmAgFnS
u	u	k7c2
dotčených	dotčený	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
svolávací	svolávací	k2eAgFnSc4d1
akci	akce	k1gFnSc4
a	a	k8xC
čerpací	čerpací	k2eAgFnPc1d1
stanice	stanice	k1gFnPc1
v	v	k7c6
některých	některý	k3yIgInPc6
státech	stát	k1gInPc6
zavedly	zavést	k5eAaPmAgFnP
preventivní	preventivní	k2eAgNnSc4d1
opatření	opatření	k1gNnSc4
<g/>
:	:	kIx,
</s>
<s>
před	před	k7c7
zahájením	zahájení	k1gNnSc7
tankování	tankování	k1gNnSc2
musí	muset	k5eAaImIp3nS
řidič	řidič	k1gMnSc1
CNG	CNG	kA
vozidla	vozidlo	k1gNnSc2
u	u	k7c2
obsluhy	obsluha	k1gFnSc2
identifikovat	identifikovat	k5eAaBmF
typ	typ	k1gInSc4
a	a	k8xC
model	model	k1gInSc4
vozidla	vozidlo	k1gNnSc2
<g/>
,	,	kIx,
u	u	k7c2
dotčených	dotčený	k2eAgInPc2d1
modelů	model	k1gInPc2
se	se	k3xPyFc4
pak	pak	k6eAd1
musí	muset	k5eAaImIp3nS
prokázat	prokázat	k5eAaPmF
protokolem	protokol	k1gInSc7
o	o	k7c6
provedené	provedený	k2eAgFnSc6d1
opravě	oprava	k1gFnSc6
vadného	vadný	k2eAgInSc2d1
tlakového	tlakový	k2eAgInSc2d1
zásobníku	zásobník	k1gInSc2
CNG	CNG	kA
<g/>
,	,	kIx,
</s>
<s>
na	na	k7c6
výdejních	výdejní	k2eAgInPc6d1
stojanech	stojan	k1gInPc6
je	být	k5eAaImIp3nS
snížen	snížit	k5eAaPmNgInS
plnící	plnící	k2eAgInSc1d1
tlak	tlak	k1gInSc1
z	z	k7c2
dřívějších	dřívější	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
220	#num#	k4
bar	bar	k1gInSc1
na	na	k7c4
180	#num#	k4
až	až	k9
200	#num#	k4
bar	bar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
opatření	opatření	k1gNnSc1
má	mít	k5eAaImIp3nS
mj.	mj.	kA
za	za	k7c4
následek	následek	k1gInSc4
pokles	pokles	k1gInSc4
maximální	maximální	k2eAgFnSc2d1
možné	možný	k2eAgFnSc2d1
kapacity	kapacita	k1gFnSc2
naplnění	naplnění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
u	u	k7c2
zásobníku	zásobník	k1gInSc2
vozidel	vozidlo	k1gNnPc2
s	s	k7c7
max	max	kA
<g/>
.	.	kIx.
kapacitou	kapacita	k1gFnSc7
15	#num#	k4
kg	kg	kA
je	být	k5eAaImIp3nS
po	po	k7c6
snížení	snížení	k1gNnSc6
tlaku	tlak	k1gInSc2
možné	možný	k2eAgNnSc1d1
naplnit	naplnit	k5eAaPmF
zásobník	zásobník	k1gInSc4
jen	jen	k9
na	na	k7c4
13	#num#	k4
až	až	k9
14	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
důsledkem	důsledek	k1gInSc7
je	být	k5eAaImIp3nS
snížení	snížení	k1gNnSc1
už	už	k6eAd1
tak	tak	k6eAd1
omezeného	omezený	k2eAgInSc2d1
dojezdu	dojezd	k1gInSc2
čistě	čistě	k6eAd1
na	na	k7c6
CNG	CNG	kA
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
výhody	výhoda	k1gFnPc4
provozu	provoz	k1gInSc2
CNG	CNG	kA
vozidla	vozidlo	k1gNnPc1
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Zemní	zemní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
je	být	k5eAaImIp3nS
kvalitní	kvalitní	k2eAgNnSc4d1
vysokooktanové	vysokooktanový	k2eAgNnSc4d1
motorové	motorový	k2eAgNnSc4d1
palivo	palivo	k1gNnSc4
(	(	kIx(
<g/>
okt	okt	k?
<g/>
.	.	kIx.
č.	č.	k?
128	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc7
předností	přednost	k1gFnSc7
je	být	k5eAaImIp3nS
tak	tak	k9
snížené	snížený	k2eAgNnSc1d1
riziko	riziko	k1gNnSc1
opotřebení	opotřebení	k1gNnSc2
motoru	motor	k1gInSc2
"	"	kIx"
<g/>
klepáním	klepání	k1gNnSc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
spalování	spalování	k1gNnSc6
CNG	CNG	kA
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
lepšímu	dobrý	k2eAgNnSc3d2
směšování	směšování	k1gNnSc3
se	s	k7c7
vzduchem	vzduch	k1gInSc7
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
k	k	k7c3
rovnoměrnějšímu	rovnoměrný	k2eAgNnSc3d2
složení	složení	k1gNnSc3
palivové	palivový	k2eAgFnSc2d1
směsi	směs	k1gFnSc2
a	a	k8xC
plnění	plnění	k1gNnSc2
válců	válec	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
projevit	projevit	k5eAaPmF
vyšším	vysoký	k2eAgInSc7d2
výkonem	výkon	k1gInSc7
motoru	motor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
CNG	CNG	kA
auta	auto	k1gNnSc2
vykazují	vykazovat	k5eAaImIp3nP
snížení	snížení	k1gNnSc4
hladiny	hladina	k1gFnSc2
hluku	hluk	k1gInSc2
o	o	k7c4
10	#num#	k4
–	–	k?
15	#num#	k4
dB	db	kA
<g/>
,	,	kIx,
podle	podle	k7c2
stáří	stáří	k1gNnSc2
motoru	motor	k1gInSc2
a	a	k8xC
údržby	údržba	k1gFnSc2
vozidla	vozidlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Výhodou	výhoda	k1gFnSc7
CNG	CNG	kA
je	být	k5eAaImIp3nS
nízká	nízký	k2eAgFnSc1d1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
nižší	nízký	k2eAgInPc1d2
provozní	provozní	k2eAgInPc1d1
náklady	náklad	k1gInPc1
a	a	k8xC
výrazná	výrazný	k2eAgFnSc1d1
úspora	úspora	k1gFnSc1
za	za	k7c2
pohonné	pohonný	k2eAgFnSc2d1
hmoty	hmota	k1gFnSc2
(	(	kIx(
<g/>
cca	cca	kA
o	o	k7c4
40	#num#	k4
až	až	k9
50	#num#	k4
%	%	kIx~
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
benzínem	benzín	k1gInSc7
Natural	Natural	k?
95	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
veřejných	veřejný	k2eAgFnPc6d1
plnicích	plnicí	k2eAgFnPc6d1
stanicích	stanice	k1gFnPc6
se	se	k3xPyFc4
cena	cena	k1gFnSc1
udává	udávat	k5eAaImIp3nS
v	v	k7c6
Kč	Kč	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spotřeba	spotřeba	k1gFnSc1
paliva	palivo	k1gNnSc2
se	se	k3xPyFc4
udává	udávat	k5eAaImIp3nS
také	také	k9
v	v	k7c6
m³	m³	k?
(	(	kIx(
<g/>
1	#num#	k4
m³	m³	k?
CNG	CNG	kA
odpovídá	odpovídat	k5eAaImIp3nS
z	z	k7c2
energetického	energetický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
cca	cca	kA
1	#num#	k4
l	l	kA
benzínu	benzín	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
CNG	CNG	kA
je	být	k5eAaImIp3nS
zatíženo	zatížit	k5eAaPmNgNnS
minimální	minimální	k2eAgFnSc7d1
spotřební	spotřební	k2eAgFnSc7d1
daní	daň	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
ale	ale	k9
v	v	k7c6
budoucích	budoucí	k2eAgNnPc6d1
letech	léto	k1gNnPc6
poroste	porůst	k5eAaPmIp3nS,k5eAaImIp3nS
na	na	k7c4
minimální	minimální	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
stanovenou	stanovený	k2eAgFnSc4d1
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s>
Vozidla	vozidlo	k1gNnPc1
na	na	k7c4
CNG	CNG	kA
jsou	být	k5eAaImIp3nP
osvobozena	osvobodit	k5eAaPmNgNnP
od	od	k7c2
placení	placení	k1gNnPc2
silniční	silniční	k2eAgFnSc2d1
daně	daň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
výhoda	výhoda	k1gFnSc1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
až	až	k9
u	u	k7c2
těžších	těžký	k2eAgNnPc2d2
vozidel	vozidlo	k1gNnPc2
s	s	k7c7
větším	veliký	k2eAgNnSc7d2
množstvím	množství	k1gNnSc7
náprav	náprava	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
roční	roční	k2eAgFnSc1d1
silniční	silniční	k2eAgFnSc1d1
daň	daň	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
výše	vysoce	k6eAd2
až	až	k9
44	#num#	k4
100	#num#	k4
Kč	Kč	kA
(	(	kIx(
<g/>
vizte	vidět	k5eAaImRp2nP
zákon	zákon	k1gInSc4
o	o	k7c6
dani	daň	k1gFnSc6
silniční	silniční	k2eAgFnSc6d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspory	úspor	k1gInPc7
u	u	k7c2
osobních	osobní	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
jsou	být	k5eAaImIp3nP
zanedbatelné	zanedbatelný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
stlačeného	stlačený	k2eAgInSc2d1
zemního	zemní	k2eAgInSc2d1
plynu	plyn	k1gInSc2
(	(	kIx(
<g/>
CNG	CNG	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Díky	díky	k7c3
vysokému	vysoký	k2eAgNnSc3d1
oktanovému	oktanový	k2eAgNnSc3d1
číslu	číslo	k1gNnSc3
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
čisté	čistý	k2eAgNnSc4d1
palivo	palivo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
nemá	mít	k5eNaImIp3nS
problémy	problém	k1gInPc4
se	s	k7c7
současnými	současný	k2eAgInPc7d1
i	i	k8xC
budoucími	budoucí	k2eAgInPc7d1
emisními	emisní	k2eAgInPc7d1
limity	limit	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
využíván	využívat	k5eAaPmNgInS,k5eAaImNgInS
v	v	k7c6
klasických	klasický	k2eAgInPc6d1
benzínových	benzínový	k2eAgInPc6d1
motorech	motor	k1gInPc6
a	a	k8xC
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
jej	on	k3xPp3gInSc4
kombinovat	kombinovat	k5eAaImF
s	s	k7c7
původním	původní	k2eAgNnSc7d1
palivem	palivo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc2
vlastnosti	vlastnost	k1gFnSc2
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
i	i	k9
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
vozidlo	vozidlo	k1gNnSc1
vybaveno	vybavit	k5eAaPmNgNnS
nádržemi	nádrž	k1gFnPc7
na	na	k7c4
obě	dva	k4xCgNnPc4
paliva	palivo	k1gNnPc4
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
dojezdy	dojezd	k1gInPc1
se	se	k3xPyFc4
sčítají	sčítat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
vozidla	vozidlo	k1gNnSc2
využívající	využívající	k2eAgInSc4d1
motor	motor	k1gInSc4
koncernu	koncern	k1gInSc2
VW	VW	kA
Group	Group	k1gMnSc1
1.4	1.4	k4
TGI	TGI	kA
81	#num#	k4
<g/>
kW	kW	kA
jsou	být	k5eAaImIp3nP
většinou	většinou	k6eAd1
vybavena	vybavit	k5eAaPmNgFnS
nádrží	nádrž	k1gFnSc7
na	na	k7c4
50	#num#	k4
l	l	kA
benzínu	benzín	k1gInSc2
N95	N95	k1gFnSc2
a	a	k8xC
nádrží	nádrž	k1gFnPc2
na	na	k7c4
15	#num#	k4
kg	kg	kA
CNG	CNG	kA
–	–	k?
kombinovaný	kombinovaný	k2eAgInSc4d1
dojezd	dojezd	k1gInSc4
na	na	k7c4
obě	dva	k4xCgNnPc4
paliva	palivo	k1gNnPc4
je	být	k5eAaImIp3nS
cca	cca	kA
1300	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praktický	praktický	k2eAgInSc1d1
rekordní	rekordní	k2eAgInSc1d1
dojezd	dojezd	k1gInSc1
dvoučlenné	dvoučlenný	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
továrního	tovární	k2eAgNnSc2d1
vozidla	vozidlo	k1gNnSc2
vybaveného	vybavený	k2eAgInSc2d1
tímto	tento	k3xDgInSc7
motorem	motor	k1gInSc7
je	být	k5eAaImIp3nS
1700	#num#	k4
km	km	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Jeden	jeden	k4xCgInSc1
m³	m³	k?
CNG	CNG	kA
dodává	dodávat	k5eAaImIp3nS
10,6	10,6	k4
kWh	kwh	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
složení	složení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS
96	#num#	k4
<g/>
–	–	k?
<g/>
98	#num#	k4
%	%	kIx~
metanu	metan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
skladování	skladování	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Plynné	plynný	k2eAgNnSc1d1
skupenství	skupenství	k1gNnSc1
v	v	k7c6
tlakových	tlakový	k2eAgFnPc6d1
nádobách	nádoba	k1gFnPc6
</s>
<s>
hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Lehčí	lehký	k2eAgFnSc1d2
než	než	k8xS
vzduch	vzduch	k1gInSc1
<g/>
,	,	kIx,
lze	lze	k6eAd1
snadno	snadno	k6eAd1
odvětrat	odvětrat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
kvalita	kvalita	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Jednotná	jednotný	k2eAgNnPc1d1
díky	díky	k7c3
propojené	propojený	k2eAgFnSc3d1
síti	síť	k1gFnSc3
plynovodů	plynovod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíl	rozdíl	k1gInSc1
v	v	k7c6
základní	základní	k2eAgFnSc6d1
surovině	surovina	k1gFnSc6
je	být	k5eAaImIp3nS
dán	dát	k5eAaPmNgMnS
geomorfologií	geomorfologie	k1gFnSc7
místa	místo	k1gNnSc2
vzniku	vznik	k1gInSc2
plynu	plyn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
parkování	parkování	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Lze	lze	k6eAd1
parkovat	parkovat	k5eAaImF
v	v	k7c6
moderních	moderní	k2eAgFnPc6d1
odvětrávaných	odvětrávaný	k2eAgFnPc6d1
podzemních	podzemní	k2eAgFnPc6d1
garážích	garáž	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Dříve	dříve	k6eAd2
vyjmenované	vyjmenovaný	k2eAgFnPc1d1
nevýhody	nevýhoda	k1gFnPc1
určují	určovat	k5eAaImIp3nP
v	v	k7c6
Česku	Česko	k1gNnSc6
CNG	CNG	kA
na	na	k7c4
nejčastější	častý	k2eAgNnSc4d3
využití	využití	k1gNnSc4
k	k	k7c3
pohonu	pohon	k1gInSc3
městských	městský	k2eAgInPc2d1
autobusů	autobus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autobusoví	autobusový	k2eAgMnPc1d1
dopravci	dopravce	k1gMnPc1
mají	mít	k5eAaImIp3nP
většinou	většina	k1gFnSc7
své	svůj	k3xOyFgFnPc4
vlastní	vlastní	k2eAgFnPc4d1
plnicí	plnicí	k2eAgFnPc4d1
stanice	stanice	k1gFnPc4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
přístupné	přístupný	k2eAgInPc1d1
i	i	k9
pro	pro	k7c4
ostatní	ostatní	k2eAgMnPc4d1
uživatele	uživatel	k1gMnPc4
CNG	CNG	kA
osobních	osobní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
v	v	k7c6
okolí	okolí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Zemní	zemní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
byl	být	k5eAaImAgInS
jako	jako	k8xS,k8xC
palivo	palivo	k1gNnSc1
do	do	k7c2
motorových	motorový	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
používán	používat	k5eAaImNgInS
už	už	k6eAd1
ve	v	k7c6
třicátých	třicátý	k4xOgNnPc2
<g/>
,	,	kIx,
čtyřicátých	čtyřicátý	k4xOgNnPc2
a	a	k8xC
padesátých	padesátý	k4xOgNnPc2
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většímu	veliký	k2eAgNnSc3d2
rozšíření	rozšíření	k1gNnSc3
však	však	k9
tehdy	tehdy	k6eAd1
bránila	bránit	k5eAaImAgFnS
omezená	omezený	k2eAgFnSc1d1
dostupnost	dostupnost	k1gFnSc1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
proběhla	proběhnout	k5eAaPmAgFnS
25.11	25.11	k4
<g/>
.2015	.2015	k4
oslava	oslava	k1gFnSc1
překročení	překročení	k1gNnSc2
počtu	počet	k1gInSc2
100	#num#	k4
plnicích	plnicí	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
na	na	k7c6
CNG	CNG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
11	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2021	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
ČR	ČR	kA
v	v	k7c6
provozu	provoz	k1gInSc6
217	#num#	k4
veřejných	veřejný	k2eAgFnPc2d1
CNG	CNG	kA
stanic	stanice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Evropě	Evropa	k1gFnSc6
bylo	být	k5eAaImAgNnS
v	v	k7c6
lednu	leden	k1gInSc6
2016	#num#	k4
v	v	k7c6
provozu	provoz	k1gInSc6
celkem	celkem	k6eAd1
3060	#num#	k4
plnicích	plnicí	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zemní	zemní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
(	(	kIx(
<g/>
NG	NG	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
u	u	k7c2
zážehových	zážehový	k2eAgInPc2d1
motorů	motor	k1gInPc2
(	(	kIx(
<g/>
ve	v	k7c6
vznětovém	vznětový	k2eAgInSc6d1
cyklu	cyklus	k1gInSc6
nelze	lze	k6eNd1
směs	směs	k1gFnSc1
zapálit	zapálit	k5eAaPmF
-	-	kIx~
má	mít	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc4d2
teplotu	teplota	k1gFnSc4
vzplanutí	vzplanutí	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrábějí	vyrábět	k5eAaImIp3nP
se	se	k3xPyFc4
motory	motor	k1gInPc7
přímo	přímo	k6eAd1
pro	pro	k7c4
CNG	CNG	kA
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
např.	např.	kA
Irisbus	Irisbus	k1gInSc1
na	na	k7c6
obrázku	obrázek	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
konstrukční	konstrukční	k2eAgFnSc7d1
úpravou	úprava	k1gFnSc7
vznětových	vznětový	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zážehový	zážehový	k2eAgInSc1d1
(	(	kIx(
<g/>
benzínový	benzínový	k2eAgInSc1d1
<g/>
)	)	kIx)
motor	motor	k1gInSc1
se	se	k3xPyFc4
nepřestavuje	přestavovat	k5eNaImIp3nS
<g/>
,	,	kIx,
změní	změnit	k5eAaPmIp3nS
se	se	k3xPyFc4
(	(	kIx(
<g/>
nebo	nebo	k8xC
přidá	přidat	k5eAaPmIp3nS
<g/>
)	)	kIx)
palivový	palivový	k2eAgInSc1d1
systém	systém	k1gInSc1
pro	pro	k7c4
zemní	zemní	k2eAgInSc4d1
plyn	plyn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Auta	auto	k1gNnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
dvoupalivová	dvoupalivový	k2eAgFnSc1d1
(	(	kIx(
<g/>
většina	většina	k1gFnSc1
modelů	model	k1gInPc2
na	na	k7c6
českém	český	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
dvě	dva	k4xCgFnPc4
nádrže	nádrž	k1gFnPc4
–	–	k?
na	na	k7c4
CNG	CNG	kA
a	a	k8xC
benzín	benzín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
motor	motor	k1gInSc1
konstruovaný	konstruovaný	k2eAgInSc1d1
přímo	přímo	k6eAd1
pro	pro	k7c4
zemní	zemní	k2eAgInSc4d1
plyn	plyn	k1gInSc4
(	(	kIx(
<g/>
Dedicated	Dedicated	k1gInSc1
Natural	Natural	k?
Gas	Gas	k1gMnSc5
Engine	Engin	k1gMnSc5
<g/>
,	,	kIx,
DNGE	DNGE	kA
<g/>
)	)	kIx)
dosahuje	dosahovat	k5eAaImIp3nS
o	o	k7c4
něco	něco	k6eAd1
lepších	dobrý	k2eAgInPc2d2
parametrů	parametr	k1gInPc2
než	než	k8xS
motor	motor	k1gInSc1
původně	původně	k6eAd1
benzínový	benzínový	k2eAgMnSc1d1
<g/>
,	,	kIx,
přizpůsobený	přizpůsobený	k2eAgMnSc1d1
spalování	spalování	k1gNnSc1
zemního	zemní	k2eAgInSc2d1
plynu	plyn	k1gInSc2
(	(	kIx(
<g/>
jiný	jiný	k2eAgInSc1d1
kompresní	kompresní	k2eAgInSc1d1
poměr	poměr	k1gInSc1
<g/>
,	,	kIx,
časování	časování	k1gNnSc1
rozvodů	rozvod	k1gInPc2
<g/>
,	,	kIx,
chlazení	chlazení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rovněž	rovněž	k9
nádrže	nádrž	k1gFnPc1
na	na	k7c4
CNG	CNG	kA
u	u	k7c2
současných	současný	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
(	(	kIx(
<g/>
Fiat	fiat	k1gInSc1
<g/>
,	,	kIx,
Opel	opel	k1gInSc1
<g/>
,	,	kIx,
Volkswagen	volkswagen	k1gInSc1
<g/>
,	,	kIx,
Mercedes	mercedes	k1gInSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
zabudovány	zabudovat	k5eAaPmNgInP
v	v	k7c6
podvozku	podvozek	k1gInSc6
vozidla	vozidlo	k1gNnSc2
a	a	k8xC
nezmenšují	zmenšovat	k5eNaImIp3nP
zavazadlový	zavazadlový	k2eAgInSc4d1
prostor	prostor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc4
užitné	užitný	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
se	se	k3xPyFc4
vinou	vina	k1gFnSc7
umístění	umístění	k1gNnSc2
rozměrných	rozměrný	k2eAgFnPc2d1
nádrží	nádrž	k1gFnPc2
na	na	k7c6
CNG	CNG	kA
mohou	moct	k5eAaImIp3nP
změnit	změnit	k5eAaPmF
(	(	kIx(
<g/>
např.	např.	kA
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
mít	mít	k5eAaImF
ve	v	k7c6
vozidle	vozidlo	k1gNnSc6
rezervní	rezervní	k2eAgNnSc4d1
kolo	kolo	k1gNnSc4
<g/>
,	,	kIx,
úložný	úložný	k2eAgInSc4d1
prostor	prostor	k1gInSc4
pro	pro	k7c4
roletu	roleta	k1gFnSc4
zavazadlového	zavazadlový	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
v	v	k7c6
nabídce	nabídka	k1gFnSc6
několik	několik	k4yIc4
motorizací	motorizace	k1gFnPc2
a	a	k8xC
typů	typ	k1gInPc2
vozidel	vozidlo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
jsou	být	k5eAaImIp3nP
určena	určit	k5eAaPmNgNnP
pro	pro	k7c4
spalování	spalování	k1gNnSc4
CNG	CNG	kA
už	už	k6eAd1
z	z	k7c2
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
pro	pro	k7c4
pořízení	pořízení	k1gNnSc4
takového	takový	k3xDgInSc2
vozu	vůz	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
mj.	mj.	kA
záruka	záruka	k1gFnSc1
od	od	k7c2
výrobce	výrobce	k1gMnSc2
na	na	k7c4
vozidlo	vozidlo	k1gNnSc4
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
ztráty	ztráta	k1gFnSc2
záruky	záruka	k1gFnSc2
při	při	k7c6
dodatečné	dodatečný	k2eAgFnSc6d1
přestavbě	přestavba	k1gFnSc6
na	na	k7c6
CNG	CNG	kA
svépomocí	svépomoc	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přizpůsobení	přizpůsobení	k1gNnSc2
vozidla	vozidlo	k1gNnSc2
na	na	k7c4
spalování	spalování	k1gNnSc4
CNG	CNG	kA
už	už	k6eAd1
z	z	k7c2
výroby	výroba	k1gFnSc2
(	(	kIx(
<g/>
nastavení	nastavení	k1gNnSc1
spalovacího	spalovací	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
,	,	kIx,
jiné	jiný	k2eAgFnPc1d1
součástky	součástka	k1gFnPc1
pro	pro	k7c4
zvýšení	zvýšení	k1gNnSc4
odolnosti	odolnost	k1gFnSc2
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejznámější	známý	k2eAgNnPc1d3
vozidla	vozidlo	k1gNnPc1
s	s	k7c7
již	již	k9
hotovou	hotový	k2eAgFnSc7d1
zástavbou	zástavba	k1gFnSc7
CNG	CNG	kA
z	z	k7c2
výroby	výroba	k1gFnSc2
patří	patřit	k5eAaImIp3nS
(	(	kIx(
<g/>
seznam	seznam	k1gInSc1
není	být	k5eNaImIp3nS
řazený	řazený	k2eAgInSc1d1
chronologicky	chronologicky	k6eAd1
od	od	k7c2
prvního	první	k4xOgInSc2
vozu	vůz	k1gInSc2
uvedeného	uvedený	k2eAgInSc2d1
na	na	k7c4
trh	trh	k1gInSc4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
koncern	koncern	k1gInSc1
VW	VW	kA
Group	Group	k1gInSc1
–	–	k?
VW	VW	kA
Passat	Passat	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
VW	VW	kA
Golf	golf	k1gInSc1
a	a	k8xC
sesterské	sesterský	k2eAgInPc4d1
vozy	vůz	k1gInPc4
Škoda	škoda	k1gFnSc1
Octavia	octavia	k1gFnSc1
<g/>
,	,	kIx,
Seat	Seat	k1gInSc1
Leon	Leona	k1gFnPc2
<g/>
,	,	kIx,
Audi	Audi	k1gNnSc1
A	a	k8xC
<g/>
3	#num#	k4
<g/>
;	;	kIx,
v	v	k7c6
nižších	nízký	k2eAgFnPc6d2
kategoriích	kategorie	k1gFnPc6
SEAT	SEAT	kA
Ibiza	Ibiza	k1gFnSc1
a	a	k8xC
crossover	crossover	k1gMnSc1
<g/>
/	/	kIx~
<g/>
SUV	SUV	kA
SEAT	SEAT	kA
Arona	Arona	k1gFnSc1
<g/>
;	;	kIx,
dále	daleko	k6eAd2
pak	pak	k6eAd1
VW	VW	kA
up	up	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
a	a	k8xC
sesterské	sesterský	k2eAgFnPc1d1
Škoda	škoda	k1gFnSc1
citigo	citigo	k1gMnSc1
<g/>
,	,	kIx,
Seat	Seat	k1gMnSc1
Mii	Mii	k1gMnSc1
<g/>
;	;	kIx,
mezi	mezi	k7c7
užitnými	užitný	k2eAgInPc7d1
vozy	vůz	k1gInPc7
VW	VW	kA
Caddy	Cadd	k1gInPc4
<g/>
;	;	kIx,
</s>
<s>
Fiat	fiat	k1gInSc4
Punto	punto	k1gNnSc1
<g/>
,	,	kIx,
Multipla	Multipla	k1gFnSc1
<g/>
,	,	kIx,
Panda	panda	k1gFnSc1
<g/>
,	,	kIx,
Qubo	Quba	k1gFnSc5
<g/>
,	,	kIx,
Ducato	Ducata	k1gFnSc5
<g/>
,	,	kIx,
Doblo	Dobla	k1gFnSc5
</s>
<s>
Opel	opel	k1gInSc1
Zafira	Zafir	k1gInSc2
<g/>
,	,	kIx,
Combo	Comba	k1gFnSc5
<g/>
,	,	kIx,
</s>
<s>
aj.	aj.	kA
</s>
<s>
Budoucnost	budoucnost	k1gFnSc1
pohonu	pohon	k1gInSc2
CNG	CNG	kA
<g/>
/	/	kIx~
<g/>
LNG	LNG	kA
</s>
<s>
Vývoj	vývoj	k1gInSc1
současných	současný	k2eAgFnPc2d1
a	a	k8xC
budoucích	budoucí	k2eAgFnPc2d1
koncepcí	koncepce	k1gFnPc2
pohonu	pohon	k1gInSc2
vozidel	vozidlo	k1gNnPc2
a	a	k8xC
strojů	stroj	k1gInPc2
lze	lze	k6eAd1
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
několik	několik	k4yIc4
odlišitelných	odlišitelný	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
podle	podle	k7c2
zdroje	zdroj	k1gInSc2
energie	energie	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
elektřina	elektřina	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
baterií	baterie	k1gFnPc2
i	i	k8xC
solárních	solární	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
vodík	vodík	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
stlačený	stlačený	k2eAgInSc1d1
vzduch	vzduch	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
CNG	CNG	kA
<g/>
/	/	kIx~
<g/>
LNG	LNG	kA
(	(	kIx(
<g/>
metan	metan	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
LPG	LPG	kA
(	(	kIx(
<g/>
propan-butan	propan-butan	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
konvenční	konvenční	k2eAgNnPc1d1
paliva	palivo	k1gNnPc1
(	(	kIx(
<g/>
benzín	benzín	k1gInSc1
<g/>
,	,	kIx,
motorová	motorový	k2eAgFnSc1d1
nafta	nafta	k1gFnSc1
a	a	k8xC
jejich	jejich	k3xOp3gFnPc1
směsi	směs	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
vícezdrojové	vícezdrojový	k2eAgInPc1d1
pohony	pohon	k1gInPc1
<g/>
,	,	kIx,
vč.	vč.	k?
rekuperačních	rekuperační	k2eAgInPc2d1
(	(	kIx(
<g/>
např.	např.	kA
hybridní	hybridní	k2eAgInSc1d1
pohon	pohon	k1gInSc1
eletřina	eletřina	k1gFnSc1
+	+	kIx~
vodík	vodík	k1gInSc1
<g/>
,	,	kIx,
CNG	CNG	kA
+	+	kIx~
benzín	benzín	k1gInSc1
<g/>
,	,	kIx,
LPG	LPG	kA
+	+	kIx~
benzín	benzín	k1gInSc1
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
Protože	protože	k8xS
vývoj	vývoj	k1gInSc4
více	hodně	k6eAd2
technologií	technologie	k1gFnPc2
najednou	najednou	k6eAd1
vyžaduje	vyžadovat	k5eAaImIp3nS
vysoké	vysoký	k2eAgInPc4d1
náklady	náklad	k1gInPc4
a	a	k8xC
investice	investice	k1gFnPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zájmem	zájem	k1gInSc7
automobilových	automobilový	k2eAgMnPc2d1
výrobců	výrobce	k1gMnPc2
zvolit	zvolit	k5eAaPmF
jen	jen	k6eAd1
některé	některý	k3yIgInPc4
z	z	k7c2
uvedených	uvedený	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
pohonů	pohon	k1gInPc2
a	a	k8xC
ostatní	ostatní	k2eAgMnSc1d1
upozadit	upozadit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgMnSc7
bude	být	k5eAaImBp3nS
možné	možný	k2eAgNnSc1d1
alokovat	alokovat	k5eAaImF
vývojové	vývojový	k2eAgFnPc4d1
kapacity	kapacita	k1gFnPc4
a	a	k8xC
finanční	finanční	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
do	do	k7c2
koncepčně	koncepčně	k6eAd1
podporovaných	podporovaný	k2eAgNnPc2d1
paliv	palivo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgInSc1d1
akční	akční	k2eAgInSc1d1
plán	plán	k1gInSc1
(	(	kIx(
<g/>
NAP	napa	k1gFnPc2
<g/>
)	)	kIx)
čisté	čistý	k2eAgFnSc2d1
mobility	mobilita	k1gFnSc2
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
je	být	k5eAaImIp3nS
podle	podle	k7c2
poslední	poslední	k2eAgFnSc2d1
známé	známý	k2eAgFnSc2d1
koncepce	koncepce	k1gFnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
"	"	kIx"
<g/>
Národního	národní	k2eAgInSc2d1
akčního	akční	k2eAgInSc2d1
plánu	plán	k1gInSc2
(	(	kIx(
<g/>
NAP	napa	k1gFnPc2
<g/>
)	)	kIx)
čisté	čistý	k2eAgFnSc2d1
mobility	mobilita	k1gFnSc2
<g/>
"	"	kIx"
do	do	k7c2
budoucna	budoucno	k1gNnSc2
podporováno	podporován	k2eAgNnSc1d1
palivo	palivo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
nebude	být	k5eNaImBp3nS
zvyšovat	zvyšovat	k5eAaImF
emise	emise	k1gFnPc4
a	a	k8xC
sníží	snížit	k5eAaPmIp3nS
závislost	závislost	k1gFnSc4
dopravy	doprava	k1gFnSc2
na	na	k7c6
klasických	klasický	k2eAgNnPc6d1
kapalných	kapalný	k2eAgNnPc6d1
palivech	palivo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
podobném	podobný	k2eAgInSc6d1
cíli	cíl	k1gInSc6
staví	stavit	k5eAaPmIp3nP,k5eAaBmIp3nP,k5eAaImIp3nP
i	i	k9
evropské	evropský	k2eAgInPc1d1
strategické	strategický	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
<g/>
,	,	kIx,
mj.	mj.	kA
směrnice	směrnice	k1gFnSc2
o	o	k7c4
zavedení	zavedení	k1gNnSc4
infrastruktury	infrastruktura	k1gFnSc2
pro	pro	k7c4
alternativní	alternativní	k2eAgNnPc4d1
paliva	palivo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Budoucím	budoucí	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
může	moct	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
nemusí	muset	k5eNaImIp3nS
dojít	dojít	k5eAaPmF
ke	k	k7c3
změně	změna	k1gFnSc3
stávajících	stávající	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
<g/>
,	,	kIx,
výrobu	výroba	k1gFnSc4
a	a	k8xC
provoz	provoz	k1gInSc4
vozidel	vozidlo	k1gNnPc2
na	na	k7c4
paliva	palivo	k1gNnPc4
různá	různý	k2eAgNnPc4d1
od	od	k7c2
benzínu	benzín	k1gInSc2
<g/>
,	,	kIx,
motorové	motorový	k2eAgFnSc2d1
nafty	nafta	k1gFnSc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
směsí	směs	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
současném	současný	k2eAgInSc6d1
trhu	trh	k1gInSc6
jsou	být	k5eAaImIp3nP
díky	díky	k7c3
podpoře	podpora	k1gFnSc3
růstu	růst	k1gInSc2
infrastruktury	infrastruktura	k1gFnSc2
<g/>
,	,	kIx,
nízkým	nízký	k2eAgFnPc3d1
cenám	cena	k1gFnPc3
paliva	palivo	k1gNnSc2
i	i	k8xC
dostupnosti	dostupnost	k1gFnSc2
vozidel	vozidlo	k1gNnPc2
perspektivní	perspektivní	k2eAgNnSc4d1
zejména	zejména	k9
pohony	pohon	k1gInPc1
na	na	k7c4
CNG	CNG	kA
a	a	k8xC
hybridy	hybrida	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
Toyota	toyota	k1gFnSc1
Auris	Auris	k1gFnSc1
514	#num#	k4
900	#num#	k4
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektromobily	elektromobil	k1gInPc4
(	(	kIx(
<g/>
vozidla	vozidlo	k1gNnSc2
s	s	k7c7
výlučně	výlučně	k6eAd1
elektrickým	elektrický	k2eAgInSc7d1
pohonem	pohon	k1gInSc7
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
jsou	být	k5eAaImIp3nP
s	s	k7c7
cenou	cena	k1gFnSc7
začínající	začínající	k2eAgFnSc7d1
na	na	k7c4
715	#num#	k4
tis	tis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
vč.	vč.	k?
DPH	DPH	kA
(	(	kIx(
<g/>
Nissan	nissan	k1gInSc1
Leaf	Leaf	k1gInSc1
<g/>
)	)	kIx)
jen	jen	k9
o	o	k7c4
10	#num#	k4
<g/>
%	%	kIx~
dražší	drahý	k2eAgNnPc1d2
než	než	k8xS
CNG	CNG	kA
modely	model	k1gInPc1
(	(	kIx(
<g/>
VW	VW	kA
Golf	golf	k1gInSc1
TGI	TGI	kA
stojí	stát	k5eAaImIp3nS
658	#num#	k4
400	#num#	k4
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Postoj	postoj	k1gInSc1
automobilek	automobilka	k1gFnPc2
k	k	k7c3
vývoji	vývoj	k1gInSc3
motorů	motor	k1gInPc2
na	na	k7c4
CNG	CNG	kA
</s>
<s>
Dva	dva	k4xCgMnPc1
největší	veliký	k2eAgMnPc1d3
producenti	producent	k1gMnPc1
CNG	CNG	kA
vozidel	vozidlo	k1gNnPc2
(	(	kIx(
<g/>
s	s	k7c7
továrními	tovární	k2eAgInPc7d1
motory	motor	k1gInPc7
pro	pro	k7c4
CNG	CNG	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k8xC
Volkswagen	volkswagen	k1gInSc1
<g/>
,	,	kIx,
AG	AG	kA
a	a	k8xC
FIAT	fiat	k1gInSc1
<g/>
,	,	kIx,
SpA	SpA	k1gFnSc1
<g/>
,	,	kIx,
volí	volit	k5eAaImIp3nS
každý	každý	k3xTgMnSc1
jinou	jiný	k2eAgFnSc4d1
koncepci	koncepce	k1gFnSc4
budoucích	budoucí	k2eAgInPc2d1
pohonů	pohon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
VW	VW	kA
již	již	k6eAd1
potvrdil	potvrdit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
produkci	produkce	k1gFnSc6
CNG	CNG	kA
vozidel	vozidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
současné	současný	k2eAgFnSc2d1
generace	generace	k1gFnSc2
motoru	motor	k1gInSc2
1.4	1.4	k4
TGI	TGI	kA
a	a	k8xC
1.0	1.0	k4
MPI	MPI	kA
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
budou	být	k5eAaImBp3nP
i	i	k9
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
pokračovat	pokračovat	k5eAaImF
<g/>
,	,	kIx,
oznámil	oznámit	k5eAaPmAgMnS
i	i	k9
nasazení	nasazení	k1gNnSc4
motoru	motor	k1gInSc2
1.5	1.5	k4
TSI	TSI	kA
a	a	k8xC
1.0	1.0	k4
TSI	TSI	kA
<g/>
,	,	kIx,
takéž	takéž	k?
v	v	k7c6
úpravě	úprava	k1gFnSc6
pro	pro	k7c4
spalování	spalování	k1gNnSc4
CNG	CNG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
motory	motor	k1gInPc1
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
i	i	k9
ve	v	k7c6
vozidlech	vozidlo	k1gNnPc6
automobilky	automobilka	k1gFnSc2
Škoda	škoda	k6eAd1
auto	auto	k1gNnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
údajně	údajně	k6eAd1
bude	být	k5eAaImBp3nS
během	během	k7c2
příštích	příští	k2eAgNnPc2d1
let	léto	k1gNnPc2
dodávat	dodávat	k5eAaImF
jeden	jeden	k4xCgInSc4
z	z	k7c2
těchto	tento	k3xDgInPc2
2	#num#	k4
agregátů	agregát	k1gInPc2
v	v	k7c6
úpravě	úprava	k1gFnSc6
pro	pro	k7c4
spalování	spalování	k1gNnSc4
CNG	CNG	kA
do	do	k7c2
každého	každý	k3xTgInSc2
modelu	model	k1gInSc2
v	v	k7c6
nabídce	nabídka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Směr	směr	k1gInSc1
vývoje	vývoj	k1gInSc2
největších	veliký	k2eAgMnPc2d3
producentů	producent	k1gMnPc2
osobních	osobní	k2eAgInPc2d1
a	a	k8xC
užitkových	užitkový	k2eAgInPc2d1
vozů	vůz	k1gInPc2
se	se	k3xPyFc4
však	však	k9
více	hodně	k6eAd2
ubírá	ubírat	k5eAaImIp3nS
k	k	k7c3
elektrickému	elektrický	k2eAgInSc3d1
pohonu	pohon	k1gInSc3
a	a	k8xC
hybridním	hybridní	k2eAgFnPc3d1
technologiím	technologie	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zpráva	zpráva	k1gFnSc1
o	o	k7c6
stavu	stav	k1gInSc6
koncepce	koncepce	k1gFnSc2
MD	MD	kA
k	k	k7c3
alternativním	alternativní	k2eAgNnPc3d1
palivům	palivo	k1gNnPc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
MAFRA	MAFRA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
2030	#num#	k4
<g/>
:	:	kIx,
až	až	k9
10	#num#	k4
%	%	kIx~
CNG	CNG	kA
<g/>
/	/	kIx~
<g/>
LNG	LNG	kA
v	v	k7c6
ČR	ČR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hybrid	hybrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
Chamanne	Chamann	k1gMnSc5
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.businessinfo.cz/cs/clanky/dan-silnicni-3537.html#ds02	http://www.businessinfo.cz/cs/clanky/dan-silnicni-3537.html#ds02	k4
<g/>
↑	↑	k?
Legislativa	legislativa	k1gFnSc1
o	o	k7c6
CNG	CNG	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNG	CNG	kA
plus	plus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
CNG	CNG	kA
(	(	kIx(
<g/>
Compressed	Compressed	k1gMnSc1
Natural	Natural	k?
Gas	Gas	k1gMnSc1
<g/>
)	)	kIx)
|	|	kIx~
autolexicon	autolexicon	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-08-11	2016-08-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rekord	rekord	k1gInSc1
dojezdu	dojezd	k1gInSc2
kombinovanou	kombinovaný	k2eAgFnSc7d1
spotřebou	spotřeba	k1gFnSc7
benzínu	benzín	k1gInSc2
a	a	k8xC
CNG	CNG	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
magazín	magazín	k1gInSc1
Úsporně	úsporně	k6eAd1
<g/>
.	.	kIx.
<g/>
info	info	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vlastnosti	vlastnost	k1gFnPc4
CNG	CNG	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
http://www.tukas.cz	http://www.tukas.cz	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Historie	historie	k1gFnSc1
metanu	metan	k1gInSc2
a	a	k8xC
propan-butanu	propan-butan	k1gInSc2
v	v	k7c6
dopravě	doprava	k1gFnSc6
<g/>
↑	↑	k?
Tomíšek	Tomíšek	k1gMnSc1
Marek	Marek	k1gMnSc1
<g/>
,	,	kIx,
Usporne	Usporn	k1gInSc5
<g/>
.	.	kIx.
<g/>
info	info	k1gNnSc1
<g/>
:	:	kIx,
Oslava	oslava	k1gFnSc1
překročení	překročení	k1gNnSc2
počtu	počet	k1gInSc2
100	#num#	k4
plnicích	plnicí	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
[	[	kIx(
<g/>
28.11	28.11	k4
<g/>
.2015	.2015	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Mapa	mapa	k1gFnSc1
CNG	CNG	kA
stanic	stanice	k1gFnPc2
|	|	kIx~
CNG	CNG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNG	CNG	kA
<g/>
+	+	kIx~
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://cngeurope.com/	http://cngeurope.com/	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
stlačený	stlačený	k2eAgInSc4d1
zemní	zemní	k2eAgInSc4d1
plyn	plyn	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
CNG	CNG	kA
<g/>
+	+	kIx~
-	-	kIx~
kompletní	kompletní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
o	o	k7c4
CNG	CNG	kA
v	v	k7c6
ČR	ČR	kA
</s>
<s>
CNG	CNG	kA
<g/>
4	#num#	k4
<g/>
you	you	k?
-	-	kIx~
informace	informace	k1gFnPc1
o	o	k7c6
CNG	CNG	kA
od	od	k7c2
Českého	český	k2eAgInSc2d1
plynárenského	plynárenský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Fosilní	fosilní	k2eAgNnPc1d1
paliva	palivo	k1gNnPc1
uhlí	uhlí	k1gNnSc2
</s>
<s>
rašelina	rašelina	k1gFnSc1
•	•	k?
gagát	gagát	k1gInSc1
•	•	k?
hnědé	hnědý	k2eAgNnSc4d1
uhlí	uhlí	k1gNnSc4
•	•	k?
černé	černá	k1gFnSc2
uhlí	uhlí	k1gNnSc2
•	•	k?
antracit	antracit	k1gInSc1
</s>
<s>
uhelné	uhelný	k2eAgInPc4d1
produkty	produkt	k1gInPc4
</s>
<s>
briketa	briketa	k1gFnSc1
•	•	k?
saze	saze	k1gFnSc2
•	•	k?
koks	koks	k1gInSc1
•	•	k?
oxid	oxid	k1gInSc1
uhelnatý	uhelnatý	k2eAgInSc1d1
-	-	kIx~
generátorový	generátorový	k2eAgInSc1d1
plyn	plyn	k1gInSc1
(	(	kIx(
<g/>
vodní	vodní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
•	•	k?
koksárenský	koksárenský	k2eAgInSc1d1
plyn	plyn	k1gInSc1
•	•	k?
svítiplyn	svítiplyn	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ropa	ropa	k1gFnSc1
</s>
<s>
konvenční	konvenční	k2eAgFnSc1d1
ropa	ropa	k1gFnSc1
</s>
<s>
lehká-sladká	lehká-sladký	k2eAgFnSc1d1
•	•	k?
těžká-kyselá	těžká-kyselý	k2eAgFnSc1d1
nekonvenční	konvenční	k2eNgFnSc1d1
ropa	ropa	k1gFnSc1
</s>
<s>
živice	živice	k1gFnSc1
<g/>
:	:	kIx,
dehtové	dehtový	k2eAgInPc1d1
písky	písek	k1gInPc1
•	•	k?
asfalt	asfalt	k1gInSc1
•	•	k?
kerogen	kerogen	k1gInSc1
<g/>
:	:	kIx,
ropné	ropný	k2eAgFnSc2d1
břidlice	břidlice	k1gFnSc2
ropné	ropný	k2eAgFnSc2d1
produkty	produkt	k1gInPc1
</s>
<s>
minerální	minerální	k2eAgInSc1d1
olej	olej	k1gInSc1
•	•	k?
nafta	nafta	k1gFnSc1
•	•	k?
benzín	benzín	k1gInSc1
•	•	k?
petrolej	petrolej	k1gInSc1
•	•	k?
parafín	parafín	k1gInSc1
•	•	k?
dehet	dehet	k1gInSc1
•	•	k?
LPG	LPG	kA
(	(	kIx(
<g/>
propan	propan	k1gInSc1
•	•	k?
butan	butan	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
zemní	zemní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
</s>
<s>
stlačený	stlačený	k2eAgMnSc1d1
(	(	kIx(
<g/>
CNG	CNG	kA
<g/>
)	)	kIx)
•	•	k?
zkapalněný	zkapalněný	k2eAgMnSc1d1
(	(	kIx(
<g/>
LNG	LNG	kA
<g/>
)	)	kIx)
•	•	k?
methan	methan	k1gInSc1
</s>
<s>
přírodní	přírodní	k2eAgInPc1d1
vývěry	vývěr	k1gInPc1
</s>
<s>
Chiméra	chiméra	k1gFnSc1
v	v	k7c6
Turecku	Turecko	k1gNnSc6
•	•	k?
Focul	Focul	k1gInSc1
viu	viu	k?
•	•	k?
Brána	brána	k1gFnSc1
do	do	k7c2
pekla	peklo	k1gNnSc2
•	•	k?
Yanar	Yanar	k1gMnSc1
Dağ	Dağ	k1gMnSc1
</s>
