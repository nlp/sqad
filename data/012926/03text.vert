<p>
<s>
Lovochemie	Lovochemie	k1gFnSc1	Lovochemie
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
vyrábějící	vyrábějící	k2eAgNnPc1d1	vyrábějící
minerální	minerální	k2eAgNnPc1d1	minerální
hnojiva	hnojivo	k1gNnPc1	hnojivo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
dusičnan	dusičnan	k1gInSc1	dusičnan
amonný	amonný	k2eAgInSc1d1	amonný
a	a	k8xC	a
dusičnan	dusičnan	k1gInSc1	dusičnan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
prodávala	prodávat	k5eAaImAgFnS	prodávat
Lovochemie	Lovochemie	k1gFnPc4	Lovochemie
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
produkce	produkce	k1gFnSc2	produkce
přes	přes	k7c4	přes
společnost	společnost	k1gFnSc4	společnost
Agrofert	Agrofert	k1gInSc1	Agrofert
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
jejím	její	k3xOp3gMnSc7	její
jediným	jediný	k2eAgMnSc7d1	jediný
vlastníkem	vlastník	k1gMnSc7	vlastník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1993	[number]	k4	1993
byla	být	k5eAaImAgFnS	být
Lovochemie	Lovochemie	k1gFnSc1	Lovochemie
zapsána	zapsat	k5eAaPmNgFnS	zapsat
do	do	k7c2	do
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
jako	jako	k8xS	jako
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
<g/>
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1995	[number]	k4	1995
oznámil	oznámit	k5eAaPmAgInS	oznámit
Fond	fond	k1gInSc1	fond
národního	národní	k2eAgInSc2d1	národní
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
že	že	k8xS	že
svůj	svůj	k3xOyFgInSc4	svůj
majoritní	majoritní	k2eAgInSc4d1	majoritní
podíl	podíl	k1gInSc4	podíl
prodá	prodat	k5eAaPmIp3nS	prodat
české	český	k2eAgFnSc3d1	Česká
společnosti	společnost	k1gFnSc3	společnost
Proferta	Proferta	k1gFnSc1	Proferta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
měla	mít	k5eAaImAgFnS	mít
menšinový	menšinový	k2eAgInSc4d1	menšinový
podíl	podíl	k1gInSc4	podíl
německá	německý	k2eAgFnSc1d1	německá
skupina	skupina	k1gFnSc1	skupina
Beiselen	Beiselen	k2eAgInSc4d1	Beiselen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
