<s>
Kanton	Kanton	k1gInSc1
Gros-Morne	Gros-Morn	k1gInSc5
</s>
<s>
Kanton	Kanton	k1gInSc1
Gros-Morne	Gros-Morn	k1gInSc5
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
Region	region	k1gInSc1
</s>
<s>
Martinik	Martinik	k1gInSc1
Departement	departement	k1gInSc1
</s>
<s>
Martinik	Martinik	k1gMnSc1
Arrondissement	Arrondissement	k1gMnSc1
</s>
<s>
La	la	k1gNnSc1
Trinité	Trinitý	k2eAgFnPc1d1
Počet	počet	k1gInSc4
obcí	obec	k1gFnPc2
</s>
<s>
1	#num#	k4
Sídlo	sídlo	k1gNnSc4
správy	správa	k1gFnSc2
</s>
<s>
Gros-Morne	Gros-Mornout	k5eAaPmIp3nS
Rozloha	rozloha	k1gFnSc1
</s>
<s>
54,25	54,25	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
10	#num#	k4
686	#num#	k4
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
197	#num#	k4
ob.	ob.	k?
<g/>
/	/	kIx~
<g/>
km²	km²	k?
</s>
<s>
Kanton	Kanton	k1gInSc1
Gros-Morne	Gros-Morne	k1gInSc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
Canton	Canton	k1gInSc1
de	de	k?
Gros-Morne	Gros-Morn	k1gInSc5
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
francouzský	francouzský	k2eAgInSc1d1
kanton	kanton	k1gInSc1
v	v	k7c6
departementu	departement	k1gInSc6
Martinik	Martinik	k1gInSc1
v	v	k7c6
regionu	region	k1gInSc6
Martinik	Martinik	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacházela	nacházet	k5eAaImAgFnS
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gNnSc6
pouze	pouze	k6eAd1
obec	obec	k1gFnSc1
Gros-Morne	Gros-Morn	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrušen	zrušen	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Bývalé	bývalý	k2eAgInPc4d1
kantony	kanton	k1gInPc4
v	v	k7c6
departementu	departement	k1gInSc6
Martinik	Martinik	k1gMnSc1
</s>
<s>
L	L	kA
<g/>
'	'	kIx"
<g/>
Ajoupa-Bouillon	Ajoupa-Bouillon	k1gInSc1
•	•	k?
Les	les	k1gInSc1
Anses-d	Anses-da	k1gFnPc2
<g/>
'	'	kIx"
<g/>
Arlet	Arleta	k1gFnPc2
•	•	k?
Basse-Pointe	Basse-Point	k1gInSc5
•	•	k?
Le	Le	k1gMnSc1
Carbet	Carbet	k1gMnSc1
•	•	k?
Case-Pilote-Bellefontaine	Case-Pilote-Bellefontain	k1gInSc5
•	•	k?
Le	Le	k1gFnSc1
Diamant	diamant	k1gInSc1
•	•	k?
Ducos	Ducos	k1gInSc1
•	•	k?
Fort-de-France-	Fort-de-France-	k1gFnSc1
<g/>
1	#num#	k4
•	•	k?
Fort-de-France-	Fort-de-France-	k1gFnPc2
<g/>
2	#num#	k4
•	•	k?
Fort-de-France-	Fort-de-France-	k1gFnPc2
<g/>
3	#num#	k4
•	•	k?
Fort-de-France-	Fort-de-France-	k1gFnPc2
<g/>
4	#num#	k4
•	•	k?
Fort-de-France-	Fort-de-France-	k1gFnPc2
<g/>
5	#num#	k4
•	•	k?
Fort-de-France-	Fort-de-France-	k1gFnPc2
<g/>
6	#num#	k4
•	•	k?
Fort-de-France-	Fort-de-France-	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
7	#num#	k4
•	•	k?
Fort-de-France-	Fort-de-France-	k1gFnPc2
<g/>
8	#num#	k4
•	•	k?
Fort-de-France-	Fort-de-France-	k1gFnPc2
<g/>
9	#num#	k4
•	•	k?
Fort-de-France-	Fort-de-France-	k1gFnPc2
<g/>
10	#num#	k4
•	•	k?
Le	Le	k1gMnSc4
François-	François-	k1gFnSc2
<g/>
1	#num#	k4
Nord	Nord	k1gMnSc1
•	•	k?
Le	Le	k1gMnSc1
François-	François-	k1gMnSc1
<g/>
2	#num#	k4
Sud	sud	k1gInSc1
•	•	k?
Gros-Morne	Gros-Morn	k1gInSc5
•	•	k?
Le	Le	k1gMnPc4
Lamentin-	Lamentin-	k1gFnSc2
<g/>
1	#num#	k4
Sud-Bourg	Sud-Bourg	k1gMnSc1
•	•	k?
Le	Le	k1gMnSc1
Lamentin-	Lamentin-	k1gFnSc2
<g/>
2	#num#	k4
Nord	Nord	k1gMnSc1
•	•	k?
Le	Le	k1gMnSc1
Lamentin-	Lamentin-	k1gFnSc2
<g/>
3	#num#	k4
Est	Est	k1gMnSc1
•	•	k?
Le	Le	k1gMnSc1
Lorrain	Lorrain	k1gMnSc1
•	•	k?
Macouba	Macouba	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Le	Le	k1gMnSc1
Marigot	Marigot	k1gMnSc1
•	•	k?
Le	Le	k1gFnSc2
Marin	Marina	k1gFnPc2
•	•	k?
Le	Le	k1gMnSc1
Morne-Rouge	Morne-Roug	k1gFnSc2
•	•	k?
Le	Le	k1gMnSc1
Prê	Prê	k1gMnSc1
•	•	k?
Riviè	Riviè	k1gInSc5
•	•	k?
Riviè	Riviè	k1gFnSc2
•	•	k?
Le	Le	k1gMnSc1
Robert-	Robert-	k1gMnSc1
<g/>
1	#num#	k4
Sud	sud	k1gInSc1
•	•	k?
Le	Le	k1gFnSc1
Robert-	Robert-	k1gFnSc2
<g/>
2	#num#	k4
Nord	Nord	k1gMnSc1
•	•	k?
Sainte-Anne	Sainte-Ann	k1gInSc5
•	•	k?
Sainte-Luce	Sainte-Luce	k1gFnSc2
•	•	k?
Sainte-Marie-	Sainte-Marie-	k1gFnSc2
<g/>
1	#num#	k4
Nord	Nord	k1gMnSc1
•	•	k?
Sainte-Marie-	Sainte-Marie-	k1gMnSc1
<g/>
2	#num#	k4
Sud	sud	k1gInSc1
•	•	k?
Saint-Esprit	Saint-Esprit	k1gInSc1
•	•	k?
Saint-Joseph	Saint-Joseph	k1gInSc1
•	•	k?
Saint-Pierre	Saint-Pierr	k1gInSc5
•	•	k?
Schœ	Schœ	k1gFnSc7
<g/>
1	#num#	k4
•	•	k?
Schœ	Schœ	k1gFnPc2
<g/>
2	#num#	k4
•	•	k?
La	la	k1gNnSc4
Trinité	Trinitý	k2eAgFnSc2d1
•	•	k?
Les	les	k1gInSc1
Trois-Îlets	Trois-Îlets	k1gInSc1
•	•	k?
Le	Le	k1gFnSc2
Vauclin	Vauclina	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
</s>
