<s>
Kriket	kriket	k1gInSc1	kriket
(	(	kIx(	(
<g/>
Cricket	Cricket	k1gInSc1	Cricket
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kolektivní	kolektivní	k2eAgFnSc1d1	kolektivní
pálkovací	pálkovací	k2eAgFnSc1d1	pálkovací
míčová	míčový	k2eAgFnSc1d1	Míčová
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
dvě	dva	k4xCgNnPc4	dva
družstva	družstvo	k1gNnPc4	družstvo
po	po	k7c6	po
jedenácti	jedenáct	k4xCc6	jedenáct
hráčích	hráč	k1gMnPc6	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
na	na	k7c6	na
oválném	oválný	k2eAgNnSc6d1	oválné
hřišti	hřiště	k1gNnSc6	hřiště
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
obdélníkový	obdélníkový	k2eAgInSc1d1	obdélníkový
pruh	pruh	k1gInSc1	pruh
(	(	kIx(	(
<g/>
pitch	pitch	k1gInSc1	pitch
<g/>
)	)	kIx)	)
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
22	[number]	k4	22
yardů	yard	k1gInPc2	yard
(	(	kIx(	(
<g/>
20,12	[number]	k4	20,12
metru	metr	k1gInSc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kriketu	kriket	k1gInSc2	kriket
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
120	[number]	k4	120
milionů	milion	k4xCgInPc2	milion
hráčů	hráč	k1gMnPc2	hráč
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
druhý	druhý	k4xOgInSc1	druhý
nejpopulárnější	populární	k2eAgInSc1d3	nejpopulárnější
sport	sport	k1gInSc1	sport
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nadhazovač	nadhazovač	k1gMnSc1	nadhazovač
(	(	kIx(	(
<g/>
bowler	bowler	k1gMnSc1	bowler
<g/>
)	)	kIx)	)
hází	házet	k5eAaImIp3nS	házet
kriketový	kriketový	k2eAgInSc1d1	kriketový
míč	míč	k1gInSc1	míč
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
pálkaři	pálkař	k1gMnPc1	pálkař
(	(	kIx(	(
<g/>
batsman	batsman	k1gMnSc1	batsman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
snaží	snažit	k5eAaImIp3nS	snažit
odpálit	odpálit	k5eAaPmF	odpálit
mimo	mimo	k7c4	mimo
dosah	dosah	k1gInSc4	dosah
polařů	polař	k1gMnPc2	polař
(	(	kIx(	(
<g/>
fielders	fielders	k6eAd1	fielders
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
běžet	běžet	k5eAaImF	běžet
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
konec	konec	k1gInSc4	konec
pruhu	pruh	k1gInSc2	pruh
a	a	k8xC	a
bodovat	bodovat	k5eAaImF	bodovat
<g/>
.	.	kIx.	.
</s>
<s>
Pálkaři	pálkař	k1gMnPc1	pálkař
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nejsou	být	k5eNaImIp3nP	být
vyautováni	vyautován	k2eAgMnPc1d1	vyautován
(	(	kIx(	(
<g/>
out	out	k?	out
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
pálce	pálka	k1gFnSc6	pálka
<g/>
,	,	kIx,	,
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
není	být	k5eNaImIp3nS	být
vyautováno	vyautovat	k5eAaImNgNnS	vyautovat
deset	deset	k4xCc1	deset
pálkařů	pálkař	k1gMnPc2	pálkař
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dokud	dokud	k8xS	dokud
nadhazovači	nadhazovač	k1gMnSc3	nadhazovač
nedokončí	dokončit	k5eNaPmIp3nS	dokončit
předem	předem	k6eAd1	předem
určený	určený	k2eAgInSc1d1	určený
počet	počet	k1gInSc1	počet
sad	sada	k1gFnPc2	sada
(	(	kIx(	(
<g/>
overs	overs	k6eAd1	overs
<g/>
)	)	kIx)	)
po	po	k7c6	po
šesti	šest	k4xCc6	šest
nadhozech	nadhoz	k1gInPc6	nadhoz
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k8xC	potom
si	se	k3xPyFc3	se
oba	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
prohodí	prohodit	k5eAaPmIp3nP	prohodit
role	role	k1gFnPc4	role
–	–	k?	–
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
pálkovat	pálkovat	k5eAaImF	pálkovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
profesionálním	profesionální	k2eAgInSc6d1	profesionální
kriketu	kriket	k1gInSc6	kriket
může	moct	k5eAaImIp3nS	moct
jedno	jeden	k4xCgNnSc4	jeden
utkání	utkání	k1gNnSc4	utkání
trvat	trvat	k5eAaImF	trvat
od	od	k7c2	od
20	[number]	k4	20
sad	sada	k1gFnPc2	sada
(	(	kIx(	(
<g/>
overs	overs	k6eAd1	overs
<g/>
)	)	kIx)	)
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
až	až	k9	až
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
Testovací	testovací	k2eAgInSc1d1	testovací
zápas	zápas	k1gInSc1	zápas
(	(	kIx(	(
<g/>
Test	test	k1gInSc1	test
match	match	k1gInSc1	match
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
směny	směna	k1gFnPc4	směna
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
pěti	pět	k4xCc2	pět
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
kriketu	kriket	k1gInSc6	kriket
spadají	spadat	k5eAaImIp3nP	spadat
před	před	k7c7	před
nebo	nebo	k8xC	nebo
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
kriket	kriket	k1gInSc1	kriket
stal	stát	k5eAaPmAgInS	stát
anglickým	anglický	k2eAgInSc7d1	anglický
národním	národní	k2eAgInSc7d1	národní
sportem	sport	k1gInSc7	sport
<g/>
.	.	kIx.	.
</s>
<s>
Expanze	expanze	k1gFnSc1	expanze
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
znamenala	znamenat	k5eAaImAgFnS	znamenat
rovněž	rovněž	k9	rovněž
rozšíření	rozšíření	k1gNnSc4	rozšíření
kriketu	kriket	k1gInSc2	kriket
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
první	první	k4xOgNnSc1	první
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
utkání	utkání	k1gNnSc1	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
nejpopulárnější	populární	k2eAgFnSc1d3	nejpopulárnější
v	v	k7c6	v
Australasii	Australasie	k1gFnSc6	Australasie
<g/>
,	,	kIx,	,
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Indickém	indický	k2eAgInSc6d1	indický
subkontinentě	subkontinent	k1gInSc6	subkontinent
<g/>
,	,	kIx,	,
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc3d1	jižní
Africe	Afrika	k1gFnSc3	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc2d1	národní
kriketové	kriketový	k2eAgFnSc2d1	kriketová
federace	federace	k1gFnSc2	federace
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kriketová	kriketový	k2eAgFnSc1d1	kriketová
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
ICC	ICC	kA	ICC
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
International	International	k1gMnSc1	International
Cricket	Cricket	k1gMnSc1	Cricket
Council	Council	k1gMnSc1	Council
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
této	tento	k3xDgFnSc2	tento
sportovní	sportovní	k2eAgFnSc2d1	sportovní
asociace	asociace	k1gFnSc2	asociace
je	být	k5eAaImIp3nS	být
Dave	Dav	k1gInSc5	Dav
Richardson	Richardson	k1gNnSc1	Richardson
<g/>
,	,	kIx,	,
ústředí	ústředí	k1gNnSc1	ústředí
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
arabských	arabský	k2eAgInPc6d1	arabský
emirátech	emirát	k1gInPc6	emirát
<g/>
.	.	kIx.	.
</s>
<s>
ICC	ICC	kA	ICC
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Asociace	asociace	k1gFnSc2	asociace
MOV	MOV	kA	MOV
uznaných	uznaný	k2eAgFnPc2d1	uznaná
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
sportovních	sportovní	k2eAgFnPc2d1	sportovní
federací	federace	k1gFnPc2	federace
(	(	kIx(	(
<g/>
ARISF	ARISF	kA	ARISF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kriket	kriket	k1gInSc1	kriket
je	být	k5eAaImIp3nS	být
uznaný	uznaný	k2eAgInSc1d1	uznaný
sport	sport	k1gInSc1	sport
(	(	kIx(	(
<g/>
MOV	MOV	kA	MOV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
hry	hra	k1gFnSc2	hra
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
pálce	pálka	k1gFnSc6	pálka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
odpalů	odpal	k1gInPc2	odpal
snaží	snažit	k5eAaImIp3nP	snažit
získat	získat	k5eAaPmF	získat
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
runs	runsit	k5eAaPmRp2nS	runsit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nadhazuje	nadhazovat	k5eAaImIp3nS	nadhazovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
bodování	bodování	k1gNnSc4	bodování
zamezit	zamezit	k5eAaPmF	zamezit
a	a	k8xC	a
pálkaře	pálkař	k1gMnSc4	pálkař
vyautovat	vyautovat	k5eAaPmF	vyautovat
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
získat	získat	k5eAaPmF	získat
více	hodně	k6eAd2	hodně
bodů	bod	k1gInPc2	bod
než	než	k8xS	než
soupeř	soupeř	k1gMnSc1	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
formátech	formát	k1gInPc6	formát
kriketu	kriket	k1gInSc2	kriket
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rovněž	rovněž	k9	rovněž
důležité	důležitý	k2eAgNnSc1d1	důležité
vyautovat	vyautovat	k5eAaBmF	vyautovat
všechny	všechen	k3xTgMnPc4	všechen
soupeřovy	soupeřův	k2eAgMnPc4d1	soupeřův
pálkaře	pálkař	k1gMnPc4	pálkař
<g/>
,	,	kIx,	,
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
zápas	zápas	k1gInSc4	zápas
skončil	skončit	k5eAaPmAgMnS	skončit
remízou	remíza	k1gFnSc7	remíza
<g/>
.	.	kIx.	.
</s>
<s>
Kriketový	kriketový	k2eAgInSc1d1	kriketový
zápas	zápas	k1gInSc1	zápas
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
směny	směna	k1gFnPc4	směna
(	(	kIx(	(
<g/>
innings	innings	k6eAd1	innings
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
směny	směna	k1gFnSc2	směna
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
tým	tým	k1gInSc4	tým
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
tým	tým	k1gInSc1	tým
na	na	k7c6	na
pálce	pálka	k1gFnSc6	pálka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
směny	směna	k1gFnSc2	směna
si	se	k3xPyFc3	se
týmy	tým	k1gInPc4	tým
úlohy	úloha	k1gFnSc2	úloha
vymění	vyměnit	k5eAaPmIp3nP	vyměnit
<g/>
.	.	kIx.	.
11	[number]	k4	11
hráčů	hráč	k1gMnPc2	hráč
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozmístí	rozmístit	k5eAaPmIp3nS	rozmístit
po	po	k7c6	po
hřišti	hřiště	k1gNnSc6	hřiště
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
družstva	družstvo	k1gNnSc2	družstvo
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgMnPc1	dva
pálkaři	pálkař	k1gMnPc1	pálkař
<g/>
.	.	kIx.	.
</s>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
pálkařů	pálkař	k1gMnPc2	pálkař
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
ohlášeno	ohlášen	k2eAgNnSc1d1	ohlášeno
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
utkání	utkání	k1gNnSc1	utkání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
průběžně	průběžně	k6eAd1	průběžně
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Kriketové	kriketový	k2eAgNnSc1d1	kriketové
hřiště	hřiště	k1gNnSc1	hřiště
je	být	k5eAaImIp3nS	být
oválné	oválný	k2eAgNnSc1d1	oválné
<g/>
,	,	kIx,	,
s	s	k7c7	s
obdélníkovým	obdélníkový	k2eAgInSc7d1	obdélníkový
pruhem	pruh	k1gInSc7	pruh
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	Hranice	k1gFnPc1	Hranice
hřiště	hřiště	k1gNnSc2	hřiště
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgFnP	označit
většinou	většinou	k6eAd1	většinou
provazem	provaz	k1gInSc7	provaz
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
plotem	plot	k1gInSc7	plot
nebo	nebo	k8xC	nebo
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
pruhu	pruh	k1gInSc2	pruh
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
22	[number]	k4	22
yardů	yard	k1gInPc2	yard
(	(	kIx(	(
<g/>
20,12	[number]	k4	20,12
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
terč	terč	k1gInSc4	terč
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
branka	branka	k1gFnSc1	branka
(	(	kIx(	(
<g/>
wicket	wicket	k1gInSc1	wicket
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pruh	pruh	k1gInSc1	pruh
je	být	k5eAaImIp3nS	být
vymezen	vymezit	k5eAaPmNgInS	vymezit
bílými	bílý	k2eAgFnPc7d1	bílá
lajnami	lajna	k1gFnPc7	lajna
<g/>
:	:	kIx,	:
čarou	čára	k1gFnSc7	čára
pro	pro	k7c4	pro
nadhazovače	nadhazovač	k1gMnPc4	nadhazovač
(	(	kIx(	(
<g/>
bowling	bowling	k1gInSc1	bowling
crease	crease	k6eAd1	crease
<g/>
)	)	kIx)	)
a	a	k8xC	a
čarou	čára	k1gFnSc7	čára
pro	pro	k7c4	pro
pálkaře	pálkař	k1gMnPc4	pálkař
(	(	kIx(	(
<g/>
batting	batting	k1gInSc1	batting
<g/>
/	/	kIx~	/
<g/>
popping	popping	k1gInSc1	popping
crease	crease	k6eAd1	crease
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
4	[number]	k4	4
yardů	yard	k1gInPc2	yard
(	(	kIx(	(
<g/>
3,66	[number]	k4	3,66
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
od	od	k7c2	od
branky	branka	k1gFnSc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Branku	branka	k1gFnSc4	branka
tvoří	tvořit	k5eAaImIp3nP	tvořit
tři	tři	k4xCgFnPc4	tři
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
tyčky	tyčka	k1gFnPc4	tyčka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
položená	položený	k2eAgFnSc1d1	položená
dvě	dva	k4xCgNnPc1	dva
vyřezávaná	vyřezávaný	k2eAgNnPc1d1	vyřezávané
dřívka	dřívko	k1gNnPc1	dřívko
(	(	kIx(	(
<g/>
bails	bails	k1gInSc1	bails
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Branka	branka	k1gFnSc1	branka
je	být	k5eAaImIp3nS	být
zbořená	zbořený	k2eAgFnSc1d1	zbořená
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
spadlo	spadnout	k5eAaPmAgNnS	spadnout
alespoň	alespoň	k9	alespoň
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
dřívek	dřívko	k1gNnPc2	dřívko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
míč	míč	k1gInSc1	míč
branky	branka	k1gFnSc2	branka
dotkne	dotknout	k5eAaPmIp3nS	dotknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dřívka	dřívko	k1gNnPc1	dřívko
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pálkař	pálkař	k1gMnSc1	pálkař
nebyl	být	k5eNaImAgMnS	být
vyautován	vyautovat	k5eAaBmNgMnS	vyautovat
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgNnSc3	každý
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
pálkařů	pálkař	k1gMnPc2	pálkař
vždy	vždy	k6eAd1	vždy
patří	patřit	k5eAaImIp3nS	patřit
jedna	jeden	k4xCgFnSc1	jeden
branka	branka	k1gFnSc1	branka
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
ta	ten	k3xDgNnPc1	ten
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
je	být	k5eAaImIp3nS	být
blíže	blízce	k6eAd2	blízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
právě	právě	k9	právě
odpaluje	odpalovat	k5eAaImIp3nS	odpalovat
<g/>
,	,	kIx,	,
nehrozí	hrozit	k5eNaImIp3nS	hrozit
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
blízkosti	blízkost	k1gFnSc6	blízkost
–	–	k?	–
za	za	k7c7	za
pálkovací	pálkovací	k2eAgFnSc7d1	pálkovací
čarou	čára	k1gFnSc7	čára
–	–	k?	–
žádné	žádný	k3yNgNnSc4	žádný
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
branka	branka	k1gFnSc1	branka
zničena	zničit	k5eAaPmNgFnS	zničit
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pálkař	pálkař	k1gMnSc1	pálkař
běží	běžet	k5eAaImIp3nS	běžet
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
konec	konec	k1gInSc4	konec
pruhu	pruh	k1gInSc2	pruh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyautován	vyautován	k2eAgMnSc1d1	vyautován
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
nadhozu	nadhoz	k1gInSc2	nadhoz
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
pálkař	pálkař	k1gMnSc1	pálkař
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
pálkaři	pálkař	k1gMnPc1	pálkař
zaujmou	zaujmout	k5eAaPmIp3nP	zaujmout
pozici	pozice	k1gFnSc4	pozice
na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
pruhu	pruh	k1gInSc2	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
označený	označený	k2eAgMnSc1d1	označený
hráč	hráč	k1gMnSc1	hráč
týmu	tým	k1gInSc2	tým
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
nadhazovač	nadhazovač	k1gMnSc1	nadhazovač
<g/>
,	,	kIx,	,
nadhazuje	nadhazovat	k5eAaImIp3nS	nadhazovat
míč	míč	k1gInSc4	míč
z	z	k7c2	z
opačné	opačný	k2eAgFnSc2d1	opačná
strany	strana	k1gFnSc2	strana
pruhu	pruh	k1gInSc2	pruh
na	na	k7c4	na
odpalujícího	odpalující	k2eAgMnSc4d1	odpalující
pálkaře	pálkař	k1gMnSc4	pálkař
<g/>
.	.	kIx.	.
</s>
<s>
Pálkaři	pálkař	k1gMnSc3	pálkař
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
právě	právě	k6eAd1	právě
není	být	k5eNaImIp3nS	být
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
ne-pálkař	neálkař	k1gMnSc1	ne-pálkař
(	(	kIx(	(
<g/>
non-striker	nontriker	k1gMnSc1	non-striker
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
stojí	stát	k5eAaImIp3nS	stát
vedle	vedle	k7c2	vedle
branky	branka	k1gFnSc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
překročit	překročit	k5eAaPmF	překročit
svou	svůj	k3xOyFgFnSc4	svůj
čáru	čára	k1gFnSc4	čára
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
rizika	riziko	k1gNnSc2	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
hráč	hráč	k1gMnSc1	hráč
týmu	tým	k1gInSc2	tým
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
brankář	brankář	k1gMnSc1	brankář
(	(	kIx(	(
<g/>
wicket	wicket	k1gMnSc1	wicket
keeper	keeper	k1gMnSc1	keeper
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
brankou	branka	k1gFnSc7	branka
aktivního	aktivní	k2eAgMnSc2d1	aktivní
pálkaře	pálkař	k1gMnSc2	pálkař
<g/>
.	.	kIx.	.
</s>
<s>
Zbývajících	zbývající	k2eAgMnPc2d1	zbývající
devět	devět	k4xCc1	devět
hráčů	hráč	k1gMnPc2	hráč
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
je	být	k5eAaImIp3nS	být
strategicky	strategicky	k6eAd1	strategicky
rozmístěno	rozmístit	k5eAaPmNgNnS	rozmístit
po	po	k7c6	po
hřišti	hřiště	k1gNnSc6	hřiště
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
centrální	centrální	k2eAgInSc4d1	centrální
pruh	pruh	k1gInSc4	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
polařů	polař	k1gMnPc2	polař
průběžně	průběžně	k6eAd1	průběžně
mění	měnit	k5eAaImIp3nS	měnit
jejich	jejich	k3xOp3gFnPc4	jejich
pozice	pozice	k1gFnPc4	pozice
podle	podle	k7c2	podle
vývoje	vývoj	k1gInSc2	vývoj
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
pruhu	pruh	k1gInSc2	pruh
vždy	vždy	k6eAd1	vždy
stojí	stát	k5eAaImIp3nS	stát
jeden	jeden	k4xCgMnSc1	jeden
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
<g/>
.	.	kIx.	.
</s>
<s>
Nadhazovač	nadhazovač	k1gMnSc1	nadhazovač
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vzdálí	vzdálit	k5eAaPmIp3nS	vzdálit
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
za	za	k7c4	za
branku	branka	k1gFnSc4	branka
<g/>
,	,	kIx,	,
rozběhne	rozběhnout	k5eAaPmIp3nS	rozběhnout
se	se	k3xPyFc4	se
a	a	k8xC	a
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
vypustí	vypustit	k5eAaPmIp3nS	vypustit
míč	míč	k1gInSc4	míč
z	z	k7c2	z
natažené	natažený	k2eAgFnSc2d1	natažená
ruky	ruka	k1gFnSc2	ruka
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlápne	došlápnout	k5eAaPmIp3nS	došlápnout
na	na	k7c4	na
čáru	čára	k1gFnSc4	čára
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
dopustí	dopustit	k5eAaPmIp3nS	dopustit
přešlapu	přešlap	k1gInSc2	přešlap
nebo	nebo	k8xC	nebo
když	když	k8xS	když
ruku	ruka	k1gFnSc4	ruka
během	během	k7c2	během
nadhozu	nadhoz	k1gInSc2	nadhoz
pokrčí	pokrčit	k5eAaPmIp3nS	pokrčit
v	v	k7c6	v
lokti	loket	k1gInSc6	loket
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
pokus	pokus	k1gInSc1	pokus
je	být	k5eAaImIp3nS	být
neplatný	platný	k2eNgInSc1d1	neplatný
(	(	kIx(	(
<g/>
no	no	k9	no
ball	ball	k1gInSc1	ball
<g/>
)	)	kIx)	)
a	a	k8xC	a
tým	tým	k1gInSc1	tým
na	na	k7c6	na
pálce	pálka	k1gFnSc6	pálka
automaticky	automaticky	k6eAd1	automaticky
získává	získávat	k5eAaImIp3nS	získávat
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
samé	samý	k3xTgNnSc1	samý
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
míč	míč	k1gInSc1	míč
letí	letět	k5eAaImIp3nS	letět
mimo	mimo	k7c4	mimo
dosah	dosah	k1gInSc4	dosah
pálkaře	pálkař	k1gMnSc2	pálkař
(	(	kIx(	(
<g/>
wide	wide	k6eAd1	wide
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Míč	míč	k1gInSc1	míč
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hozen	hodit	k5eAaPmNgMnS	hodit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
pruhu	pruh	k1gInSc6	pruh
odrazí	odrazit	k5eAaPmIp3nS	odrazit
od	od	k7c2	od
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
čáru	čára	k1gFnSc4	čára
(	(	kIx(	(
<g/>
a	a	k8xC	a
yorker	yorker	k1gInSc1	yorker
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
letí	letět	k5eAaImIp3nS	letět
vzduchem	vzduch	k1gInSc7	vzduch
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
branku	branka	k1gFnSc4	branka
(	(	kIx(	(
<g/>
a	a	k8xC	a
full	full	k1gInSc1	full
toss	tossa	k1gFnPc2	tossa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pálkař	pálkař	k1gMnSc1	pálkař
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
chránit	chránit	k5eAaImF	chránit
svou	svůj	k3xOyFgFnSc4	svůj
branku	branka	k1gFnSc4	branka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
míče	míč	k1gInSc2	míč
odpaluje	odpalovat	k5eAaImIp3nS	odpalovat
pálkou	pálka	k1gFnSc7	pálka
(	(	kIx(	(
<g/>
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
rukojeť	rukojeť	k1gFnSc4	rukojeť
a	a	k8xC	a
rukavice	rukavice	k1gFnPc4	rukavice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nadhazovači	nadhazovač	k1gMnPc1	nadhazovač
podaří	podařit	k5eAaPmIp3nP	podařit
branku	branka	k1gFnSc4	branka
zbořit	zbořit	k5eAaPmF	zbořit
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
pálkař	pálkař	k1gMnSc1	pálkař
vyautován	vyautován	k2eAgMnSc1d1	vyautován
(	(	kIx(	(
<g/>
bowled	bowled	k1gInSc1	bowled
out	out	k?	out
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pálkař	pálkař	k1gMnSc1	pálkař
míč	míč	k1gInSc4	míč
mine	minout	k5eAaImIp3nS	minout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zboření	zboření	k1gNnSc1	zboření
branky	branka	k1gFnSc2	branka
zabrání	zabránit	k5eAaPmIp3nS	zabránit
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
jiná	jiný	k2eAgFnSc1d1	jiná
část	část	k1gFnSc1	část
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
podle	podle	k7c2	podle
pravidla	pravidlo	k1gNnSc2	pravidlo
noha	noh	k1gMnSc2	noh
před	před	k7c7	před
brankou	branka	k1gFnSc7	branka
(	(	kIx(	(
<g/>
leg	lego	k1gNnPc2	lego
before	befor	k1gInSc5	befor
wicket	wicket	k1gInSc4	wicket
–	–	k?	–
LBW	LBW	kA	LBW
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pálkař	pálkař	k1gMnSc1	pálkař
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyautován	vyautován	k2eAgInSc1d1	vyautován
také	také	k9	také
když	když	k8xS	když
odpálí	odpálit	k5eAaPmIp3nS	odpálit
míč	míč	k1gInSc4	míč
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
chycen	chytit	k5eAaPmNgInS	chytit
polařem	polař	k1gMnSc7	polař
přímo	přímo	k6eAd1	přímo
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
caught	caught	k1gInSc1	caught
out	out	k?	out
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
míč	míč	k1gInSc4	míč
chytí	chytit	k5eAaPmIp3nS	chytit
nadhazovač	nadhazovač	k1gMnSc1	nadhazovač
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
caught	caught	k2eAgInSc1d1	caught
and	and	k?	and
bowled	bowled	k1gInSc1	bowled
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
brankáře	brankář	k1gMnSc2	brankář
caugh	caugh	k1gMnSc1	caugh
behind	behind	k1gMnSc1	behind
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pálkaři	pálkař	k1gMnPc1	pálkař
podaří	podařit	k5eAaPmIp3nP	podařit
odpálit	odpálit	k5eAaPmF	odpálit
míč	míč	k1gInSc4	míč
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
není	být	k5eNaImIp3nS	být
chycen	chytit	k5eAaPmNgInS	chytit
přímo	přímo	k6eAd1	přímo
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
pálkaři	pálkař	k1gMnPc1	pálkař
pokusit	pokusit	k5eAaPmF	pokusit
získat	získat	k5eAaPmF	získat
body	bod	k1gInPc4	bod
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
běží	běžet	k5eAaImIp3nP	běžet
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
konec	konec	k1gInSc4	konec
pruhu	pruh	k1gInSc2	pruh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pálkou	pálka	k1gFnSc7	pálka
dotknou	dotknout	k5eAaPmIp3nP	dotknout
příslušné	příslušný	k2eAgFnPc1d1	příslušná
čáry	čára	k1gFnPc1	čára
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
každý	každý	k3xTgInSc4	každý
takový	takový	k3xDgInSc4	takový
dokončený	dokončený	k2eAgInSc4d1	dokončený
přeběh	přeběh	k1gInSc4	přeběh
získávají	získávat	k5eAaImIp3nP	získávat
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
pokusit	pokusit	k5eAaPmF	pokusit
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
o	o	k7c4	o
více	hodně	k6eAd2	hodně
bodů	bod	k1gInPc2	bod
nebo	nebo	k8xC	nebo
neběžet	běžet	k5eNaImF	běžet
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
přeběh	přeběh	k1gInSc4	přeběh
hrozí	hrozit	k5eAaImIp3nS	hrozit
pálkařům	pálkař	k1gMnPc3	pálkař
vyřazení	vyřazení	k1gNnSc2	vyřazení
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dojde	dojít	k5eAaPmIp3nS	dojít
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
hráči	hráč	k1gMnPc1	hráč
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
zmocní	zmocnit	k5eAaPmIp3nP	zmocnit
míče	míč	k1gInPc4	míč
a	a	k8xC	a
srazí	srazit	k5eAaPmIp3nP	srazit
dřívka	dřívko	k1gNnPc1	dřívko
branky	branka	k1gFnSc2	branka
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
buď	buď	k8xC	buď
hodem	hod	k1gInSc7	hod
nebo	nebo	k8xC	nebo
s	s	k7c7	s
míčem	míč	k1gInSc7	míč
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
než	než	k8xS	než
pálkaři	pálkař	k1gMnPc1	pálkař
doběhnou	doběhnout	k5eAaPmIp3nP	doběhnout
k	k	k7c3	k
čáře	čára	k1gFnSc3	čára
<g/>
.	.	kIx.	.
</s>
<s>
Takovému	takový	k3xDgInSc3	takový
způsobu	způsob	k1gInSc3	způsob
vyautování	vyautování	k1gNnSc1	vyautování
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
run	run	k1gInSc1	run
out	out	k?	out
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pálkaři	pálkař	k1gMnPc1	pálkař
často	často	k6eAd1	často
vyběhnou	vyběhnout	k5eAaPmIp3nP	vyběhnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
původní	původní	k2eAgFnSc2d1	původní
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pálkař	pálkař	k1gMnSc1	pálkař
odpálí	odpálit	k5eAaPmIp3nS	odpálit
míč	míč	k1gInSc4	míč
vzduchem	vzduch	k1gInSc7	vzduch
až	až	k8xS	až
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
,	,	kIx,	,
získává	získávat	k5eAaImIp3nS	získávat
jeho	jeho	k3xOp3gInSc4	jeho
tým	tým	k1gInSc4	tým
šest	šest	k4xCc4	šest
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
míč	míč	k1gInSc1	míč
před	před	k7c7	před
opuštěním	opuštění	k1gNnSc7	opuštění
hřiště	hřiště	k1gNnSc2	hřiště
dotkne	dotknout	k5eAaPmIp3nS	dotknout
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
odpal	odpal	k1gInSc4	odpal
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Pálkaři	pálkař	k1gMnPc1	pálkař
mohou	moct	k5eAaImIp3nP	moct
začít	začít	k5eAaPmF	začít
běhat	běhat	k5eAaImF	běhat
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
míč	míč	k1gInSc1	míč
opustí	opustit	k5eAaPmIp3nS	opustit
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgInPc1	tento
body	bod	k1gInPc1	bod
se	se	k3xPyFc4	se
nepočítají	počítat	k5eNaImIp3nP	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
pálkař	pálkař	k1gMnSc1	pálkař
míč	míč	k1gInSc4	míč
netrefí	trefit	k5eNaPmIp3nS	trefit
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pokusit	pokusit	k5eAaPmF	pokusit
získat	získat	k5eAaPmF	získat
nějaké	nějaký	k3yIgInPc4	nějaký
body	bod	k1gInPc4	bod
přeběhnutím	přeběhnutí	k1gNnSc7	přeběhnutí
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pálkař	pálkař	k1gMnSc1	pálkař
opustí	opustit	k5eAaPmIp3nS	opustit
svou	svůj	k3xOyFgFnSc4	svůj
čáru	čára	k1gFnSc4	čára
a	a	k8xC	a
mine	minout	k5eAaImIp3nS	minout
míč	míč	k1gInSc4	míč
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ho	on	k3xPp3gMnSc4	on
brankář	brankář	k1gMnSc1	brankář
chytit	chytit	k5eAaPmF	chytit
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
branku	branka	k1gFnSc4	branka
rozbít	rozbít	k5eAaPmF	rozbít
(	(	kIx(	(
<g/>
stumped	stumped	k1gInSc1	stumped
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
přešlapu	přešlap	k1gInSc2	přešlap
(	(	kIx(	(
<g/>
no	no	k9	no
ball	ball	k1gInSc1	ball
<g/>
)	)	kIx)	)
a	a	k8xC	a
širokého	široký	k2eAgInSc2d1	široký
nadhozu	nadhoz	k1gInSc2	nadhoz
(	(	kIx(	(
<g/>
wide	wide	k1gInSc1	wide
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pálkař	pálkař	k1gMnSc1	pálkař
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
míč	míč	k1gInSc4	míč
odpálit	odpálit	k5eAaPmF	odpálit
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
extra	extra	k2eAgInPc4d1	extra
body	bod	k1gInPc4	bod
navíc	navíc	k6eAd1	navíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyautován	vyautovat	k5eAaBmNgMnS	vyautovat
pouze	pouze	k6eAd1	pouze
<g/>
,	,	kIx,	,
když	když	k8xS	když
včas	včas	k6eAd1	včas
nedoběhne	doběhnout	k5eNaPmIp3nS	doběhnout
za	za	k7c4	za
čáru	čára	k1gFnSc4	čára
(	(	kIx(	(
<g/>
run	run	k1gInSc1	run
out	out	k?	out
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pálkaři	pálkař	k1gMnPc1	pálkař
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
ukončit	ukončit	k5eAaPmF	ukončit
přeběhy	přeběh	k1gInPc1	přeběh
<g/>
,	,	kIx,	,
míč	míč	k1gInSc1	míč
je	být	k5eAaImIp3nS	být
označen	označit	k5eAaPmNgInS	označit
jako	jako	k9	jako
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
(	(	kIx(	(
<g/>
dead	dead	k1gInSc1	dead
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vrácen	vrátit	k5eAaPmNgMnS	vrátit
nadhazovači	nadhazovač	k1gMnPc7	nadhazovač
<g/>
.	.	kIx.	.
</s>
<s>
Živým	živý	k1gMnPc3	živý
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nadhazovač	nadhazovač	k1gMnSc1	nadhazovač
rozběhne	rozběhnout	k5eAaPmIp3nS	rozběhnout
k	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
nadhozu	nadhoz	k1gInSc3	nadhoz
<g/>
.	.	kIx.	.
</s>
<s>
Nadhazovač	nadhazovač	k1gMnSc1	nadhazovač
během	během	k7c2	během
sady	sada	k1gFnSc2	sada
(	(	kIx(	(
<g/>
over	over	k1gInSc1	over
<g/>
)	)	kIx)	)
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
nadhazování	nadhazování	k1gNnSc6	nadhazování
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
branku	branka	k1gFnSc4	branka
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
změnu	změna	k1gFnSc4	změna
pozic	pozice	k1gFnPc2	pozice
pálkařů	pálkař	k1gMnPc2	pálkař
<g/>
.	.	kIx.	.
</s>
<s>
Vyřazený	vyřazený	k2eAgMnSc1d1	vyřazený
pálkař	pálkař	k1gMnSc1	pálkař
odchází	odcházet	k5eAaImIp3nS	odcházet
z	z	k7c2	z
hřiště	hřiště	k1gNnSc2	hřiště
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
novým	nový	k2eAgMnSc7d1	nový
pálkařem	pálkař	k1gMnSc7	pálkař
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
třeba	třeba	k6eAd1	třeba
nadhazovač	nadhazovač	k1gMnSc1	nadhazovač
viditelně	viditelně	k6eAd1	viditelně
zbořil	zbořit	k5eAaPmAgMnS	zbořit
branku	branka	k1gFnSc4	branka
<g/>
,	,	kIx,	,
pálkař	pálkař	k1gMnSc1	pálkař
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
vyřazen	vyřazen	k2eAgMnSc1d1	vyřazen
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
tým	tým	k1gInSc1	tým
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
nepožádá	požádat	k5eNaPmIp3nS	požádat
rozhodčího	rozhodčí	k1gMnSc4	rozhodčí
o	o	k7c4	o
vyjádření	vyjádření	k1gNnSc4	vyjádření
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
formulováno	formulovat	k5eAaImNgNnS	formulovat
slovy	slovo	k1gNnPc7	slovo
How	How	k1gMnSc2	How
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
that	that	k1gInSc1	that
nebo	nebo	k8xC	nebo
Howzat	Howzat	k1gFnSc1	Howzat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šesti	šest	k4xCc6	šest
nadhozech	nadhoz	k1gInPc6	nadhoz
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc6	jeden
sadě	sada	k1gFnSc6	sada
(	(	kIx(	(
<g/>
over	over	k1gInSc1	over
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
polařů	polař	k1gMnPc2	polař
mění	měnit	k5eAaImIp3nS	měnit
nadhazovač	nadhazovač	k1gMnSc1	nadhazovač
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
hází	házet	k5eAaImIp3nS	házet
na	na	k7c4	na
opačnou	opačný	k2eAgFnSc4d1	opačná
branku	branka	k1gFnSc4	branka
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Směna	směna	k1gFnSc1	směna
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
konce	konec	k1gInSc2	konec
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
vyautovat	vyautovat	k5eAaPmF	vyautovat
10	[number]	k4	10
z	z	k7c2	z
11	[number]	k4	11
pálkařů	pálkař	k1gMnPc2	pálkař
(	(	kIx(	(
<g/>
all	all	k?	all
out	out	k?	out
–	–	k?	–
přičemž	přičemž	k6eAd1	přičemž
vždy	vždy	k6eAd1	vždy
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
pálkařů	pálkař	k1gMnPc2	pálkař
zůstane	zůstat	k5eAaPmIp3nS	zůstat
"	"	kIx"	"
<g/>
not	nota	k1gFnPc2	nota
out	out	k?	out
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
variantou	varianta	k1gFnSc7	varianta
konce	konec	k1gInSc2	konec
směny	směna	k1gFnSc2	směna
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
odehrán	odehrát	k5eAaPmNgInS	odehrát
dopředu	dopředu	k6eAd1	dopředu
dohodnutý	dohodnutý	k2eAgInSc1d1	dohodnutý
počet	počet	k1gInSc1	počet
sad	sada	k1gFnPc2	sada
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
když	když	k8xS	když
tým	tým	k1gInSc1	tým
na	na	k7c6	na
pálce	pálka	k1gFnSc6	pálka
deklaruje	deklarovat	k5eAaBmIp3nS	deklarovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
získal	získat	k5eAaPmAgMnS	získat
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
pohledu	pohled	k1gInSc2	pohled
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
počet	počet	k1gInSc1	počet
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
odehrány	odehrát	k5eAaPmNgFnP	odehrát
všechny	všechen	k3xTgFnPc1	všechen
směny	směna	k1gFnPc1	směna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
špatného	špatný	k2eAgNnSc2d1	špatné
počasí	počasí	k1gNnSc2	počasí
mohou	moct	k5eAaImIp3nP	moct
zápas	zápas	k1gInSc4	zápas
ukončit	ukončit	k5eAaPmF	ukončit
také	také	k9	také
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
utkání	utkání	k1gNnSc2	utkání
konči	končit	k5eAaImRp2nS	končit
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
první	první	k4xOgInSc1	první
tým	tým	k1gInSc1	tým
už	už	k6eAd1	už
má	mít	k5eAaImIp3nS	mít
odehrány	odehrát	k5eAaPmNgFnP	odehrát
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
směny	směna	k1gFnPc4	směna
na	na	k7c6	na
pálce	pálka	k1gFnSc6	pálka
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
tým	tým	k1gInSc1	tým
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápasech	zápas	k1gInPc6	zápas
hraných	hraný	k2eAgInPc6d1	hraný
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
směny	směna	k1gFnPc4	směna
nemusí	muset	k5eNaImIp3nS	muset
druhý	druhý	k4xOgInSc1	druhý
tým	tým	k1gInSc1	tým
svou	svůj	k3xOyFgFnSc4	svůj
druhou	druhý	k4xOgFnSc4	druhý
směnu	směna	k1gFnSc4	směna
ani	ani	k8xC	ani
začínat	začínat	k5eAaImF	začínat
–	–	k?	–
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
že	že	k8xS	že
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
o	o	k7c4	o
směnu	směna	k1gFnSc4	směna
(	(	kIx(	(
<g/>
win	win	k?	win
by	by	k9	by
an	an	k?	an
innings	innings	k1gInSc1	innings
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
druhý	druhý	k4xOgInSc1	druhý
tým	tým	k1gInSc1	tým
nedokončil	dokončit	k5eNaPmAgInS	dokončit
svou	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
směnu	směna	k1gFnSc4	směna
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ještě	ještě	k9	ještě
pět	pět	k4xCc1	pět
pálkařů	pálkař	k1gMnPc2	pálkař
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
nedostali	dostat	k5eNaPmAgMnP	dostat
na	na	k7c4	na
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
o	o	k7c4	o
pět	pět	k4xCc4	pět
branek	branka	k1gFnPc2	branka
(	(	kIx(	(
<g/>
win	win	k?	win
by	by	kYmCp3nS	by
five	five	k6eAd1	five
wickets	wickets	k6eAd1	wickets
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
poslední	poslední	k2eAgInSc1d1	poslední
tým	tým	k1gInSc1	tým
na	na	k7c6	na
pálce	pálka	k1gFnSc6	pálka
prohrává	prohrávat	k5eAaImIp3nS	prohrávat
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
pálkaři	pálkař	k1gMnPc1	pálkař
jsou	být	k5eAaImIp3nP	být
vyautovaní	vyautovaný	k2eAgMnPc1d1	vyautovaný
<g/>
,	,	kIx,	,
a	a	k8xC	a
tým	tým	k1gInSc1	tým
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
deset	deset	k4xCc4	deset
bodů	bod	k1gInPc2	bod
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vítězný	vítězný	k2eAgInSc1d1	vítězný
tým	tým	k1gInSc1	tým
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
o	o	k7c4	o
deset	deset	k4xCc4	deset
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
win	win	k?	win
by	by	k9	by
10	[number]	k4	10
runs	runsa	k1gFnPc2	runsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
oba	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
odehrají	odehrát	k5eAaPmIp3nP	odehrát
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
směny	směna	k1gFnPc4	směna
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
remízu	remíza	k1gFnSc4	remíza
(	(	kIx(	(
<g/>
a	a	k8xC	a
tie	tie	k?	tie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čtyřsměnných	čtyřsměnný	k2eAgNnPc6d1	čtyřsměnný
utkáních	utkání	k1gNnPc6	utkání
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
jiná	jiný	k2eAgFnSc1d1	jiná
možnost	možnost	k1gFnSc1	možnost
remízy	remíza	k1gFnSc2	remíza
(	(	kIx(	(
<g/>
a	a	k8xC	a
draw	draw	k?	draw
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
tým	tým	k1gInSc1	tým
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
počtem	počet	k1gInSc7	počet
bodů	bod	k1gInPc2	bod
na	na	k7c6	na
konci	konec	k1gInSc6	konec
posledního	poslední	k2eAgInSc2d1	poslední
dne	den	k1gInSc2	den
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
stále	stále	k6eAd1	stále
alespoň	alespoň	k9	alespoň
dva	dva	k4xCgMnPc4	dva
pálkaře	pálkař	k1gMnPc4	pálkař
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
strategii	strategie	k1gFnSc4	strategie
<g/>
.	.	kIx.	.
</s>
<s>
Týmy	tým	k1gInPc1	tým
často	často	k6eAd1	často
deklarují	deklarovat	k5eAaBmIp3nP	deklarovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ve	v	k7c6	v
stanoveném	stanovený	k2eAgInSc6d1	stanovený
čase	čas	k1gInSc6	čas
stačily	stačit	k5eAaBmAgInP	stačit
vyautovat	vyautovat	k5eAaBmF	vyautovat
všechny	všechen	k3xTgMnPc4	všechen
soupeřovy	soupeřův	k2eAgMnPc4d1	soupeřův
pálkaře	pálkař	k1gMnPc4	pálkař
<g/>
.	.	kIx.	.
</s>
<s>
Rizikem	riziko	k1gNnSc7	riziko
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
nepodaří	podařit	k5eNaPmIp3nS	podařit
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
tým	tým	k1gInSc1	tým
naopak	naopak	k6eAd1	naopak
získá	získat	k5eAaPmIp3nS	získat
potřebný	potřebný	k2eAgInSc1d1	potřebný
počet	počet	k1gInSc1	počet
bodů	bod	k1gInPc2	bod
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgNnSc4d1	vlastní
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
