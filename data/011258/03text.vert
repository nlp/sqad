<p>
<s>
Gyrostemon	Gyrostemon	k1gMnSc1	Gyrostemon
ramulosus	ramulosus	k1gMnSc1	ramulosus
je	být	k5eAaImIp3nS	být
endemická	endemický	k2eAgFnSc1d1	endemická
rostlina	rostlina	k1gFnSc1	rostlina
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
v	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
oblastech	oblast	k1gFnPc6	oblast
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přísluší	příslušet	k5eAaImIp3nS	příslušet
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
Gyrostemonaceae	Gyrostemonacea	k1gFnSc2	Gyrostemonacea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
teplomilná	teplomilný	k2eAgFnSc1d1	teplomilná
a	a	k8xC	a
suchomilná	suchomilný	k2eAgFnSc1d1	suchomilná
rostlina	rostlina	k1gFnSc1	rostlina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
Australském	australský	k2eAgInSc6d1	australský
kontinentu	kontinent	k1gInSc6	kontinent
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
ve	v	k7c6	v
spolkových	spolkový	k2eAgInPc6d1	spolkový
státech	stát	k1gInPc6	stát
Queensland	Queenslanda	k1gFnPc2	Queenslanda
a	a	k8xC	a
Západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Lokálně	lokálně	k6eAd1	lokálně
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
hojně	hojně	k6eAd1	hojně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
nebo	nebo	k8xC	nebo
vnitrozemních	vnitrozemní	k2eAgFnPc6d1	vnitrozemní
písčitých	písčitý	k2eAgFnPc6d1	písčitá
dunách	duna	k1gFnPc6	duna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Dvoudomá	dvoudomý	k2eAgFnSc1d1	dvoudomá
rostlina	rostlina	k1gFnSc1	rostlina
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
buď	buď	k8xC	buď
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
keře	keř	k1gInSc2	keř
do	do	k7c2	do
0,5	[number]	k4	0,5
m	m	kA	m
nebo	nebo	k8xC	nebo
nízkého	nízký	k2eAgInSc2d1	nízký
stromu	strom	k1gInSc2	strom
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
5	[number]	k4	5
m	m	kA	m
<g/>
,	,	kIx,	,
kůra	kůra	k1gFnSc1	kůra
starších	starý	k2eAgInPc2d2	starší
stromů	strom	k1gInPc2	strom
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stává	stávat	k5eAaImIp3nS	stávat
korkovitou	korkovitý	k2eAgFnSc4d1	korkovitá
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnPc1d1	mladá
větve	větev	k1gFnPc1	větev
jsou	být	k5eAaImIp3nP	být
pružné	pružný	k2eAgFnPc1d1	pružná
<g/>
,	,	kIx,	,
rostou	růst	k5eAaImIp3nP	růst
metlovitě	metlovitě	k6eAd1	metlovitě
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Střídavé	střídavý	k2eAgInPc1d1	střídavý
listy	list	k1gInPc1	list
bez	bez	k7c2	bez
řapíků	řapík	k1gInPc2	řapík
jsou	být	k5eAaImIp3nP	být
štíhlé	štíhlý	k2eAgFnPc1d1	štíhlá
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1	[number]	k4	1
cm	cm	kA	cm
široké	široký	k2eAgNnSc1d1	široké
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
délky	délka	k1gFnSc2	délka
až	až	k9	až
7	[number]	k4	7
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světle	světle	k6eAd1	světle
žluté	žlutý	k2eAgInPc1d1	žlutý
nebo	nebo	k8xC	nebo
bílé	bílý	k2eAgInPc1d1	bílý
drobné	drobný	k2eAgInPc1d1	drobný
květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
jednopohlavné	jednopohlavný	k2eAgInPc1d1	jednopohlavný
<g/>
.	.	kIx.	.
</s>
<s>
Samčí	samčí	k2eAgFnPc1d1	samčí
ani	ani	k8xC	ani
samičí	samičí	k2eAgFnPc1d1	samičí
nemají	mít	k5eNaImIp3nP	mít
koruny	koruna	k1gFnPc1	koruna
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
kalichy	kalich	k1gInPc1	kalich
se	s	k7c7	s
4	[number]	k4	4
lístky	lístek	k1gInPc7	lístek
mají	mít	k5eAaImIp3nP	mít
zřetelné	zřetelný	k2eAgInPc1d1	zřetelný
laloky	lalok	k1gInPc1	lalok
<g/>
.	.	kIx.	.
</s>
<s>
Samčí	samčí	k2eAgInPc1d1	samčí
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
mm	mm	kA	mm
velké	velká	k1gFnSc2	velká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
veliký	veliký	k2eAgInSc4d1	veliký
počet	počet	k1gInSc4	počet
tyčinek	tyčinka	k1gFnPc2	tyčinka
<g/>
.	.	kIx.	.
</s>
<s>
Samičí	samičí	k2eAgInPc1d1	samičí
květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
stopkách	stopka	k1gFnPc6	stopka
až	až	k9	až
7	[number]	k4	7
mm	mm	kA	mm
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
plodolistů	plodolist	k1gInPc2	plodolist
se	s	k7c7	s
synkarpními	synkarpní	k2eAgInPc7d1	synkarpní
pestíky	pestík	k1gInPc7	pestík
<g/>
.	.	kIx.	.
</s>
<s>
Opylovány	opylován	k2eAgInPc1d1	opylován
jsou	být	k5eAaImIp3nP	být
hlavně	hlavně	k9	hlavně
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Pukavé	pukavý	k2eAgInPc1d1	pukavý
plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
kulovité	kulovitý	k2eAgFnPc1d1	kulovitá
<g/>
,	,	kIx,	,
asi	asi	k9	asi
5	[number]	k4	5
mm	mm	kA	mm
velké	velká	k1gFnSc2	velká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámka	poznámka	k1gFnSc1	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
Pokusy	pokus	k1gInPc4	pokus
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
stimulátorem	stimulátor	k1gInSc7	stimulátor
pro	pro	k7c4	pro
vyklíčení	vyklíčení	k1gNnSc4	vyklíčení
semen	semeno	k1gNnPc2	semeno
Gyrostemon	Gyrostemona	k1gFnPc2	Gyrostemona
ramulosus	ramulosus	k1gInSc4	ramulosus
zapadlých	zapadlý	k2eAgFnPc2d1	zapadlá
do	do	k7c2	do
půdy	půda	k1gFnSc2	půda
je	být	k5eAaImIp3nS	být
stepní	stepní	k2eAgInSc1d1	stepní
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
klíčení	klíčení	k1gNnSc1	klíčení
nastává	nastávat	k5eAaImIp3nS	nastávat
zhruba	zhruba	k6eAd1	zhruba
až	až	k9	až
za	za	k7c4	za
rok	rok	k1gInSc4	rok
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Xerofyt	xerofyt	k1gInSc1	xerofyt
Gyrostemon	Gyrostemon	k1gMnSc1	Gyrostemon
ramulosus	ramulosus	k1gMnSc1	ramulosus
nemá	mít	k5eNaImIp3nS	mít
praktického	praktický	k2eAgNnSc2d1	praktické
využití	využití	k1gNnSc2	využití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gyrostemon	Gyrostemon	k1gInSc1	Gyrostemon
ramulosus	ramulosus	k1gInSc4	ramulosus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Gyrostemon	Gyrostemon	k1gInSc1	Gyrostemon
ramulosus	ramulosus	k1gInSc1	ramulosus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
FOTO	foto	k1gNnSc1	foto
Gyrostemon	Gyrostemon	k1gMnSc1	Gyrostemon
ramulosus	ramulosus	k1gMnSc1	ramulosus
</s>
</p>
