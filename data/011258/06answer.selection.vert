<s>
Gyrostemon	Gyrostemon	k1gMnSc1	Gyrostemon
ramulosus	ramulosus	k1gMnSc1	ramulosus
je	být	k5eAaImIp3nS	být
endemická	endemický	k2eAgFnSc1d1	endemická
rostlina	rostlina	k1gFnSc1	rostlina
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
v	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
oblastech	oblast	k1gFnPc6	oblast
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přísluší	příslušet	k5eAaImIp3nS	příslušet
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
Gyrostemonaceae	Gyrostemonacea	k1gFnSc2	Gyrostemonacea
<g/>
.	.	kIx.	.
</s>
