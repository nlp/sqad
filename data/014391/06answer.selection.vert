<s>
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gFnPc4
nejznámější	známý	k2eAgFnPc4d3
stavby	stavba	k1gFnPc4
patří	patřit	k5eAaImIp3nS
Německý	německý	k2eAgInSc1d1
pavilon	pavilon	k1gInSc1
na	na	k7c6
Mezinárodní	mezinárodní	k2eAgFnSc6d1
výstavě	výstava	k1gFnSc6
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1929	#num#	k4
a	a	k8xC
elegantní	elegantní	k2eAgFnSc1d1
brněnská	brněnský	k2eAgFnSc1d1
Vila	vila	k1gFnSc1
Tugendhat	Tugendhat	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1930	#num#	k4
<g/>
.	.	kIx.
</s>