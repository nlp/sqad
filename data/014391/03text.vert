<s>
Ludwig	Ludwig	k1gMnSc1
Mies	Miesa	k1gFnPc2
van	van	k1gInSc4
der	drát	k5eAaImRp2nS
Rohe	Roh	k1gMnSc4
</s>
<s>
Ludwig	Ludwig	k1gMnSc1
Mies	Miesa	k1gFnPc2
van	van	k1gInSc4
der	drát	k5eAaImRp2nS
Rohe	Rohe	k1gFnSc6
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Ludwig	Ludwig	k1gMnSc1
Mies	Mies	k1gInSc4
Narození	narození	k1gNnSc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1886	#num#	k4
Cáchy	Cáchy	k1gFnPc4
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1969	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
83	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Chicago	Chicago	k1gNnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Hřbitov	hřbitov	k1gInSc1
Graceland	Graceland	k1gInSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Unterrichtsanstalt	Unterrichtsanstalt	k5eAaPmF
des	des	k1gNnPc4
Kunstgewerbemuseums	Kunstgewerbemuseumsa	k1gFnPc2
BerlinUniverzita	BerlinUniverzita	k1gFnSc1
umění	umění	k1gNnSc2
v	v	k7c6
Berlíně	Berlín	k1gInSc6
Povolání	povolání	k1gNnSc2
</s>
<s>
architekt	architekt	k1gMnSc1
<g/>
,	,	kIx,
designér	designér	k1gMnSc1
a	a	k8xC
učitel	učitel	k1gMnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Ada	Ada	kA
Mies	Mies	k1gInSc1
(	(	kIx(
<g/>
1913	#num#	k4
<g/>
–	–	k?
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
Mia	Mia	k?
Moore	Moor	k1gMnSc5
Významná	významný	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
Toronto-Dominion	Toronto-Dominion	k1gInSc1
CentreWestmount	CentreWestmounta	k1gFnPc2
Squarevila	Squarevila	k1gFnSc2
TugendhatSeagram	TugendhatSeagram	k1gInSc1
Building	Building	k1gInSc1
Ovlivněný	ovlivněný	k2eAgInSc1d1
</s>
<s>
de	de	k?
Stijl	Stijl	k1gInSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Royal	Royal	k1gMnSc1
Gold	Gold	k1gMnSc1
Medal	Medal	k1gMnSc1
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
)	)	kIx)
<g/>
Berliner	Berliner	k1gInSc1
Kunstpreis	Kunstpreis	k1gFnSc2
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
<g/>
medaile	medaile	k1gFnSc2
Ernsta	Ernst	k1gMnSc2
Reutera	Reuter	k1gMnSc2
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
<g/>
Prezidentská	prezidentský	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
svobodyŘád	svobodyŘáda	k1gFnPc2
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
umění	umění	k1gNnSc2
a	a	k8xC
vědvelkokříž	vědvelkokříž	k1gFnSc4
Řádu	řád	k1gInSc2
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
Spolkové	spolkový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Německo	Německo	k1gNnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
citáty	citát	k1gInPc1
na	na	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ludwig	Ludwig	k1gMnSc1
Mies	Mies	k1gMnSc1
van	van	k7
der	der	kA
Rohe	Rohe	k1gMnSc1
<g/>
,	,	kIx,
narozený	narozený	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Maria	Maria	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
Michael	Michael	k1gMnSc1
Mies	Mies	k1gMnSc1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1886	#num#	k4
<g/>
,	,	kIx,
Cáchy	Cáchy	k1gFnPc1
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1969	#num#	k4
<g/>
,	,	kIx,
Chicago	Chicago	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
žijící	žijící	k2eAgFnSc2d1
od	od	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
tvorbu	tvorba	k1gFnSc4
po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
výrazně	výrazně	k6eAd1
ovlivnily	ovlivnit	k5eAaPmAgFnP
osobnosti	osobnost	k1gFnPc1
jako	jako	k8xS,k8xC
Walter	Walter	k1gMnSc1
Gropius	Gropius	k1gMnSc1
či	či	k8xC
funkcionalista	funkcionalista	k1gMnSc1
Le	Le	k1gMnSc1
Corbusier	Corbusier	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1930	#num#	k4
<g/>
–	–	k?
<g/>
1933	#num#	k4
působil	působit	k5eAaImAgMnS
jako	jako	k9
poslední	poslední	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
výtvarné	výtvarný	k2eAgFnSc2d1
školy	škola	k1gFnSc2
Bauhaus	Bauhaus	k1gInSc1
<g/>
,	,	kIx,
tuto	tento	k3xDgFnSc4
funkci	funkce	k1gFnSc4
převzal	převzít	k5eAaPmAgMnS
od	od	k7c2
Hannese	Hannese	k1gFnSc2
Meyera	Meyero	k1gNnSc2
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
před	před	k7c7
ním	on	k3xPp3gNnSc7
byl	být	k5eAaImAgMnS
ředitelem	ředitel	k1gMnSc7
Bauhausu	Bauhaus	k1gInSc2
Walter	Walter	k1gMnSc1
Gropius	Gropius	k1gMnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
Mies	Mies	k1gInSc1
ujal	ujmout	k5eAaPmAgInS
vedení	vedení	k1gNnSc4
oddělení	oddělení	k1gNnSc2
architektury	architektura	k1gFnSc2
na	na	k7c6
Illinois	Illinois	k1gFnSc6
Institute	institut	k1gInSc5
of	of	k?
Technology	technolog	k1gMnPc7
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
IIT	IIT	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
technické	technický	k2eAgFnSc3d1
vysoké	vysoký	k2eAgFnSc3d1
škole	škola	k1gFnSc3
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
rozpracoval	rozpracovat	k5eAaPmAgMnS
tzv.	tzv.	kA
druhou	druhý	k4xOgFnSc4
chicagskou	chicagský	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
Le	Le	k1gMnSc7
Corbusierem	Corbusier	k1gMnSc7
<g/>
,	,	kIx,
Alvarem	Alvar	k1gMnSc7
Aaltou	Aalta	k1gMnSc7
nebo	nebo	k8xC
Frankem	Frank	k1gMnSc7
Lloydem	Lloyd	k1gMnSc7
Wrightem	Wright	k1gMnSc7
je	být	k5eAaImIp3nS
všeobecně	všeobecně	k6eAd1
považován	považován	k2eAgInSc1d1
za	za	k7c4
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
průkopníků	průkopník	k1gMnPc2
moderní	moderní	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mies	Mies	k6eAd1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
mnoho	mnoho	k4c1
jeho	jeho	k3xOp3gMnPc2
současníků	současník	k1gMnPc2
<g/>
,	,	kIx,
usiloval	usilovat	k5eAaImAgMnS
o	o	k7c6
vytvoření	vytvoření	k1gNnSc6
nového	nový	k2eAgInSc2d1
architektonického	architektonický	k2eAgInSc2d1
stylu	styl	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
by	by	kYmCp3nS
ztělesňoval	ztělesňovat	k5eAaImAgMnS
moderní	moderní	k2eAgFnSc4d1
<g/>
,	,	kIx,
poválečnou	poválečný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
právě	právě	k9
tak	tak	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
ztělesněním	ztělesnění	k1gNnSc7
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
byla	být	k5eAaImAgFnS
například	například	k6eAd1
architektura	architektura	k1gFnSc1
antická	antický	k2eAgFnSc1d1
nebo	nebo	k8xC
gotická	gotický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stál	stát	k5eAaImAgMnS
u	u	k7c2
vzniku	vznik	k1gInSc2
architektonického	architektonický	k2eAgInSc2d1
stylu	styl	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
vyznačoval	vyznačovat	k5eAaImAgInS
maximální	maximální	k2eAgFnSc7d1
čistotou	čistota	k1gFnSc7
a	a	k8xC
jednoduchostí	jednoduchost	k1gFnSc7
a	a	k8xC
ovlivnil	ovlivnit	k5eAaPmAgInS
celé	celý	k2eAgNnSc4d1
dvacáté	dvacátý	k4xOgNnSc4
století	století	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavby	stavba	k1gFnSc2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
vrcholného	vrcholný	k2eAgNnSc2d1
období	období	k1gNnSc2
používaly	používat	k5eAaImAgInP
pro	pro	k7c4
vymezení	vymezení	k1gNnSc4
vnitřních	vnitřní	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
moderní	moderní	k2eAgInPc1d1
materiály	materiál	k1gInPc1
jako	jako	k8xS,k8xC
například	například	k6eAd1
průmyslovou	průmyslový	k2eAgFnSc4d1
ocel	ocel	k1gFnSc4
nebo	nebo	k8xC
tabulkové	tabulkový	k2eAgNnSc4d1
sklo	sklo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mise	mise	k1gFnSc1
usiloval	usilovat	k5eAaImAgInS
o	o	k7c4
architekturu	architektura	k1gFnSc4
s	s	k7c7
minimalistickou	minimalistický	k2eAgFnSc7d1
nosnou	nosný	k2eAgFnSc7d1
konstrukcí	konstrukce	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
bude	být	k5eAaImBp3nS
v	v	k7c6
rovnováze	rovnováha	k1gFnSc6
s	s	k7c7
volně	volně	k6eAd1
plynoucím	plynoucí	k2eAgInSc7d1
otevřeným	otevřený	k2eAgInSc7d1
prostorem	prostor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Architekturu	architektura	k1gFnSc4
svých	svůj	k3xOyFgFnPc2
staveb	stavba	k1gFnPc2
nazýval	nazývat	k5eAaImAgInS
architekturou	architektura	k1gFnSc7
„	„	k?
<g/>
kosti	kost	k1gFnSc2
a	a	k8xC
kůže	kůže	k1gFnSc2
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snažil	snažit	k5eAaImAgMnS
se	se	k3xPyFc4
o	o	k7c4
racionální	racionální	k2eAgInSc4d1
přístup	přístup	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
by	by	kYmCp3nS
řídil	řídit	k5eAaImAgInS
tvůrčí	tvůrčí	k2eAgInSc1d1
proces	proces	k1gInSc1
architektonického	architektonický	k2eAgInSc2d1
návrhu	návrh	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
především	především	k9
jej	on	k3xPp3gMnSc4
zajímalo	zajímat	k5eAaImAgNnS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
vyjádřit	vyjádřit	k5eAaPmF
ducha	duch	k1gMnSc4
moderní	moderní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mies	Mies	k1gInSc1
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
spojován	spojovat	k5eAaImNgMnS
se	s	k7c7
svými	svůj	k3xOyFgInPc7
krátkými	krátký	k2eAgInPc7d1
aforismy	aforismus	k1gInPc7
„	„	k?
<g/>
méně	málo	k6eAd2
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
<g/>
”	”	k?
a	a	k8xC
„	„	k?
<g/>
Bůh	bůh	k1gMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
detailech	detail	k1gInPc6
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gFnPc4
nejznámější	známý	k2eAgFnPc4d3
stavby	stavba	k1gFnPc4
patří	patřit	k5eAaImIp3nS
Německý	německý	k2eAgInSc1d1
pavilon	pavilon	k1gInSc1
na	na	k7c6
Mezinárodní	mezinárodní	k2eAgFnSc6d1
výstavě	výstava	k1gFnSc6
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1929	#num#	k4
a	a	k8xC
elegantní	elegantní	k2eAgFnSc1d1
brněnská	brněnský	k2eAgFnSc1d1
Vila	vila	k1gFnSc1
Tugendhat	Tugendhat	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1930	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Raná	raný	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
</s>
<s>
Mies	Mies	k1gInSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
v	v	k7c6
německých	německý	k2eAgFnPc6d1
Cáchách	Cáchy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předtím	předtím	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
přestěhoval	přestěhovat	k5eAaPmAgInS
do	do	k7c2
Berlína	Berlín	k1gInSc2
<g/>
,	,	kIx,
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c4
kamenictví	kamenictví	k1gNnSc4
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Berlíně	Berlín	k1gInSc6
pak	pak	k6eAd1
nastoupil	nastoupit	k5eAaPmAgMnS
do	do	k7c2
ateliéru	ateliér	k1gInSc2
interiérového	interiérový	k2eAgMnSc2d1
designéra	designér	k1gMnSc2
Bruna	Bruno	k1gMnSc2
Paula	Paul	k1gMnSc2
<g/>
,	,	kIx,
1908	#num#	k4
<g/>
–	–	k?
<g/>
1911	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
učeň	učeň	k1gMnSc1
u	u	k7c2
Petera	Peter	k1gMnSc2
Behrense	Behrens	k1gMnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
započal	započnout	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
dráhu	dráha	k1gFnSc4
architekta	architekt	k1gMnSc2
a	a	k8xC
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
vystaven	vystavit	k5eAaPmNgInS
nejnovějším	nový	k2eAgFnPc3d3
teoriím	teorie	k1gFnPc3
designu	design	k1gInSc2
a	a	k8xC
moderní	moderní	k2eAgFnSc3d1
německé	německý	k2eAgFnSc3d1
kultuře	kultura	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
Behrense	Behrense	k1gFnSc2
měl	mít	k5eAaImAgMnS
příležitost	příležitost	k1gFnSc4
pracovat	pracovat	k5eAaImF
po	po	k7c6
boku	bok	k1gInSc6
Waltera	Walter	k1gMnSc2
Gropia	Gropius	k1gMnSc2
a	a	k8xC
Le	Le	k1gMnSc2
Corbusiera	Corbusier	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
také	také	k9
podíleli	podílet	k5eAaImAgMnP
na	na	k7c6
vývoji	vývoj	k1gInSc6
Bauhausu	Bauhaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
jako	jako	k8xS,k8xC
spolupracovník	spolupracovník	k1gMnSc1
Behrense	Behrense	k1gFnSc2
působil	působit	k5eAaImAgMnS
Mies	Mies	k1gInSc4
jako	jako	k9
stavbyvedoucí	stavbyvedoucí	k1gMnPc1
německého	německý	k2eAgNnSc2d1
velvyslanectví	velvyslanectví	k1gNnSc2
v	v	k7c6
Petrohradě	Petrohrad	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
se	se	k3xPyFc4
poznalo	poznat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
talentovaný	talentovaný	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
Mies	Mies	k1gInSc1
brzy	brzy	k6eAd1
začal	začít	k5eAaPmAgInS
přijímat	přijímat	k5eAaImF
samostatné	samostatný	k2eAgFnPc4d1
zakázky	zakázka	k1gFnPc4
<g/>
,	,	kIx,
navzdory	navzdory	k7c3
nedostatku	nedostatek	k1gInSc3
patřičného	patřičný	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znám	znát	k5eAaImIp1nS
jako	jako	k9
fyzicky	fyzicky	k6eAd1
impozantní	impozantní	k2eAgMnSc1d1
a	a	k8xC
uvážlivý	uvážlivý	k2eAgMnSc1d1
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
toho	ten	k3xDgNnSc2
mnoho	mnoho	k6eAd1
nenamluví	namluvit	k5eNaPmIp3nP,k5eNaBmIp3nP
<g/>
,	,	kIx,
Ludwig	Ludwig	k1gInSc1
Mies	Mies	k1gInSc1
si	se	k3xPyFc3
brzy	brzy	k6eAd1
změnil	změnit	k5eAaPmAgMnS
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tak	tak	k6eAd1
dosáhl	dosáhnout	k5eAaPmAgMnS
rychlé	rychlý	k2eAgFnPc4d1
přeměny	přeměna	k1gFnPc4
ze	z	k7c2
syna	syn	k1gMnSc2
kameníka	kameník	k1gMnSc2
v	v	k7c4
architekta	architekt	k1gMnSc4
pracujícího	pracující	k1gMnSc4
s	s	k7c7
berlínskou	berlínský	k2eAgFnSc7d1
smetánkou	smetánka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
jménu	jméno	k1gNnSc3
Mies	Mies	k1gInSc4
připojil	připojit	k5eAaPmAgInS
„	„	k?
<g/>
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
<g/>
”	”	k?
a	a	k8xC
dívčí	dívčí	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
své	svůj	k3xOyFgFnSc2
holandské	holandský	k2eAgFnSc2d1
matky	matka	k1gFnSc2
„	„	k?
<g/>
Rohe	Rohe	k1gInSc1
<g/>
”	”	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
–	–	k?
použil	použít	k5eAaPmAgMnS
raději	rád	k6eAd2
holandského	holandský	k2eAgInSc2d1
„	„	k?
<g/>
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
<g/>
”	”	k?
než	než	k8xS
německého	německý	k2eAgNnSc2d1
„	„	k?
<g/>
von	von	k1gInSc1
<g/>
”	”	k?
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
ze	z	k7c2
zákona	zákon	k1gInSc2
vztahovalo	vztahovat	k5eAaImAgNnS
pouze	pouze	k6eAd1
na	na	k7c4
členy	člen	k1gMnPc4
pravé	pravý	k2eAgFnSc2d1
aristokracie	aristokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4
samostatnou	samostatný	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
architekta	architekt	k1gMnSc2
začal	začít	k5eAaPmAgMnS
Mies	Mies	k1gInSc4
navrhováním	navrhování	k1gNnSc7
domů	dům	k1gInPc2
vyšší	vysoký	k2eAgFnSc2d2
vrstvy	vrstva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Usiloval	usilovat	k5eAaImAgMnS
o	o	k7c4
návrat	návrat	k1gInSc4
k	k	k7c3
čistotě	čistota	k1gFnSc3
germánské	germánský	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
počátku	počátek	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdivoval	obdivovat	k5eAaImAgMnS
Karla	Karel	k1gMnSc4
Friedricha	Friedrich	k1gMnSc4
Schinkela	Schinkel	k1gMnSc4
<g/>
,	,	kIx,
pruského	pruský	k2eAgMnSc2d1
klasicistního	klasicistní	k2eAgMnSc2d1
architekta	architekt	k1gMnSc2
počátku	počátek	k1gInSc6
devatenáctého	devatenáctý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblíbil	oblíbit	k5eAaPmAgMnS
si	se	k3xPyFc3
velké	velký	k2eAgInPc4d1
rozměry	rozměr	k1gInPc4
<g/>
,	,	kIx,
pravidelnost	pravidelnost	k1gFnSc4
rytmických	rytmický	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
pozornost	pozornost	k1gFnSc1
vztahu	vztah	k1gInSc2
mezi	mezi	k7c7
uměním	umění	k1gNnSc7
a	a	k8xC
přírodou	příroda	k1gFnSc7
a	a	k8xC
kompozici	kompozice	k1gFnSc4
využívající	využívající	k2eAgInPc4d1
jednoduché	jednoduchý	k2eAgInPc4d1
kubistické	kubistický	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
pro	pro	k7c4
Schinkela	Schinkel	k1gMnSc4
typické	typický	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odmítnul	Odmítnul	k1gMnSc1
eklektické	eklektický	k2eAgInPc4d1
a	a	k8xC
přeplácané	přeplácaný	k2eAgInPc4d1
tradiční	tradiční	k2eAgInPc4d1
styly	styl	k1gInPc4
přelomu	přelom	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jako	jako	k9
pro	pro	k7c4
moderní	moderní	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
bezvýznamné	bezvýznamný	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Soukromý	soukromý	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
se	se	k3xPyFc4
Mies	Mies	k1gInSc1
oženil	oženit	k5eAaPmAgInS
s	s	k7c7
Adele	Adel	k1gMnSc5
Auguste	August	k1gMnSc5
(	(	kIx(
<g/>
Adou	Ado	k1gMnSc6
<g/>
)	)	kIx)
Bruhnovou	Bruhnová	k1gFnSc7
(	(	kIx(
<g/>
1885	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
zámožného	zámožný	k2eAgMnSc4d1
průmyslníka	průmyslník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
od	od	k7c2
ženy	žena	k1gFnSc2
odchází	odcházet	k5eAaImIp3nS
<g/>
,	,	kIx,
z	z	k7c2
manželství	manželství	k1gNnSc2
ale	ale	k8xC
vzešly	vzejít	k5eAaPmAgFnP
3	#num#	k4
dcery	dcera	k1gFnPc1
<g/>
:	:	kIx,
Dorothea	Dorothea	k1gFnSc1
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
,	,	kIx,
herečka	herečka	k1gFnSc1
a	a	k8xC
tanečnice	tanečnice	k1gFnSc1
známá	známý	k2eAgFnSc1d1
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Georgia	Georgium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Marianne	Mariann	k1gMnSc5
(	(	kIx(
<g/>
1915	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Waltraut	Waltraut	k1gMnSc1
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
–	–	k?
<g/>
1959	#num#	k4
<g/>
,	,	kIx,
akademička	akademička	k1gFnSc1
a	a	k8xC
kurátorka	kurátorka	k1gFnSc1
Institutu	institut	k1gInSc2
umění	umění	k1gNnSc2
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
své	svůj	k3xOyFgFnSc2
vojenské	vojenský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
zplodil	zplodit	k5eAaPmAgMnS
nemanželského	manželský	k2eNgMnSc4d1
syna	syn	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
započal	započnout	k5eAaPmAgInS
vztah	vztah	k1gInSc1
s	s	k7c7
designérkou	designérka	k1gFnSc7
Lilly	Lilla	k1gFnSc2
Reichovou	Reichová	k1gFnSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
trval	trvat	k5eAaImAgInS
až	až	k6eAd1
do	do	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
odchodu	odchod	k1gInSc2
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
se	se	k3xPyFc4
setkává	setkávat	k5eAaImIp3nS
s	s	k7c7
Lorou	Lora	k1gFnSc7
Marxovou	Marxová	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc7
partnerkou	partnerka	k1gFnSc7
až	až	k9
do	do	k7c2
jeho	jeho	k3xOp3gFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mies	Mies	k1gInSc4
také	také	k9
udržoval	udržovat	k5eAaImAgInS
milostný	milostný	k2eAgInSc1d1
vztah	vztah	k1gInSc1
se	s	k7c7
sochařkou	sochařka	k1gFnSc7
a	a	k8xC
sběratelkou	sběratelka	k1gFnSc7
umění	umění	k1gNnSc2
Mary	Mary	k1gFnSc2
Calleryovou	Calleryová	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
navrhl	navrhnout	k5eAaPmAgInS
ateliér	ateliér	k1gInSc1
v	v	k7c6
Huntingtonu	Huntington	k1gInSc6
v	v	k7c6
New	New	k1gFnPc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Údajně	údajně	k6eAd1
měl	mít	k5eAaImAgInS
i	i	k9
krátký	krátký	k2eAgInSc1d1
románek	románek	k1gInSc1
s	s	k7c7
Edith	Edith	k1gMnSc1
Farnsworthovou	Farnsworthová	k1gFnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
navrhl	navrhnout	k5eAaPmAgInS
dům	dům	k1gInSc1
Farnsworth	Farnsworth	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
vnuk	vnuk	k1gMnSc1
Dirk	Dirk	k1gMnSc1
Lohan	Lohan	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1938	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc7
studentem	student	k1gMnSc7
a	a	k8xC
později	pozdě	k6eAd2
pro	pro	k7c4
něho	on	k3xPp3gMnSc4
i	i	k9
pracoval	pracovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
tradičních	tradiční	k2eAgInPc2d1
stylů	styl	k1gInPc2
k	k	k7c3
moderně	moderna	k1gFnSc3
</s>
<s>
Vila	vila	k1gFnSc1
Tugendhat	Tugendhat	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
Replika	replika	k1gFnSc1
Německého	německý	k2eAgInSc2d1
pavilonu	pavilon	k1gInSc2
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
</s>
<s>
Po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
staví	stavit	k5eAaPmIp3nS,k5eAaBmIp3nS,k5eAaImIp3nS
Mies	Mies	k1gInSc4
i	i	k9
nadále	nadále	k6eAd1
v	v	k7c6
tradičním	tradiční	k2eAgInSc6d1
neoklasicistním	neoklasicistní	k2eAgInSc6d1
stylu	styl	k1gInSc6
<g/>
,	,	kIx,
současně	současně	k6eAd1
však	však	k9
začíná	začínat	k5eAaImIp3nS
experimentovat	experimentovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
jeho	jeho	k3xOp3gMnPc1
avantgardní	avantgardní	k2eAgMnPc1d1
kolegové	kolega	k1gMnPc1
se	se	k3xPyFc4
dlouhodobě	dlouhodobě	k6eAd1
pokoušel	pokoušet	k5eAaImAgMnS
najít	najít	k5eAaPmF
nový	nový	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
by	by	kYmCp3nS
odpovídal	odpovídat	k5eAaImAgInS
moderní	moderní	k2eAgInSc1d1
průmyslové	průmyslový	k2eAgFnSc3d1
době	doba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
od	od	k7c2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byly	být	k5eAaImAgInP
tradiční	tradiční	k2eAgInPc1d1
styly	styl	k1gInPc1
terčem	terč	k1gInSc7
kritiky	kritik	k1gMnPc7
progresivních	progresivní	k2eAgMnPc2d1
teoretiků	teoretik	k1gMnPc2
–	–	k?
jejich	jejich	k3xOp3gInSc4
nedostatek	nedostatek	k1gInSc4
spatřovali	spatřovat	k5eAaImAgMnP
především	především	k9
v	v	k7c6
rozporu	rozpor	k1gInSc6
mezi	mezi	k7c7
moderní	moderní	k2eAgFnSc7d1
stavební	stavební	k2eAgFnSc7d1
technologií	technologie	k1gFnSc7
a	a	k8xC
jejím	její	k3xOp3gNnPc3
zakrytím	zakrytí	k1gNnPc3
vnější	vnější	k2eAgInPc4d1
fasádou	fasáda	k1gFnSc7
s	s	k7c7
tradičními	tradiční	k2eAgInPc7d1
ornamenty	ornament	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Rostoucí	rostoucí	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
historizujících	historizující	k2eAgInPc2d1
stylů	styl	k1gInPc2
získala	získat	k5eAaPmAgFnS
po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
značně	značně	k6eAd1
na	na	k7c6
kulturní	kulturní	k2eAgFnSc6d1
důvěryhodnosti	důvěryhodnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
byla	být	k5eAaImAgFnS
obecně	obecně	k6eAd1
vnímána	vnímat	k5eAaImNgFnS
jako	jako	k8xC,k8xS
selhání	selhání	k1gNnSc1
starého	starý	k2eAgInSc2d1
světového	světový	k2eAgInSc2d1
řádu	řád	k1gInSc2
vedeného	vedený	k2eAgInSc2d1
císařskou	císařský	k2eAgFnSc7d1
Evropou	Evropa	k1gFnSc7
a	a	k8xC
tradiční	tradiční	k2eAgInPc1d1
aristokratické	aristokratický	k2eAgInPc1d1
historizující	historizující	k2eAgInPc1d1
styly	styl	k1gInPc1
upadly	upadnout	k5eAaPmAgInP
v	v	k7c4
nelibost	nelibost	k1gFnSc4
právě	právě	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
představovaly	představovat	k5eAaImAgFnP
onen	onen	k3xDgInSc4
zastaralý	zastaralý	k2eAgInSc4d1
sociální	sociální	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
ztratil	ztratit	k5eAaPmAgInS
důvěru	důvěra	k1gFnSc4
a	a	k8xC
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
považován	považován	k2eAgInSc1d1
za	za	k7c4
překonaný	překonaný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgMnPc1d1
myslitelé	myslitel	k1gMnPc1
vyzývali	vyzývat	k5eAaImAgMnP
k	k	k7c3
novému	nový	k2eAgNnSc3d1
zpracovávání	zpracovávání	k1gNnSc3
architektonického	architektonický	k2eAgInSc2d1
návrhu	návrh	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
by	by	kYmCp3nS
upřednostnil	upřednostnit	k5eAaPmAgInS
racionální	racionální	k2eAgNnSc4d1
řešení	řešení	k1gNnSc4
problému	problém	k1gInSc2
a	a	k8xC
vnější	vnější	k2eAgNnSc4d1
vyjádření	vyjádření	k1gNnSc4
moderního	moderní	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
a	a	k8xC
struktury	struktura	k1gFnSc2
před	před	k7c7
vnějším	vnější	k2eAgNnSc7d1
použitím	použití	k1gNnSc7
tradičních	tradiční	k2eAgFnPc2d1
fasád	fasáda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vedle	vedle	k7c2
tradičních	tradiční	k2eAgInPc2d1
neoklasicistních	neoklasicistní	k2eAgInPc2d1
projektů	projekt	k1gInPc2
se	se	k3xPyFc4
zároveň	zároveň	k6eAd1
začínají	začínat	k5eAaImIp3nP
i	i	k9
objevovat	objevovat	k5eAaImF
utopistické	utopistický	k2eAgInPc4d1
projekty	projekt	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
(	(	kIx(
<g/>
i	i	k8xC
když	když	k8xS
většina	většina	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
nerealizovaná	realizovaný	k2eNgFnSc1d1
<g/>
)	)	kIx)
z	z	k7c2
Miese	Miese	k1gFnSc2
udělaly	udělat	k5eAaPmAgInP
proslulého	proslulý	k2eAgMnSc2d1
architekta	architekt	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
dokázal	dokázat	k5eAaPmAgMnS
tvořit	tvořit	k5eAaImF
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
duchem	duch	k1gMnSc7
rodící	rodící	k2eAgFnSc2d1
se	se	k3xPyFc4
moderní	moderní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mies	Mies	k1gInSc1
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
uvedl	uvést	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
se	se	k3xPyFc4
svým	svůj	k3xOyFgInSc7
ohromujícím	ohromující	k2eAgInSc7d1
soutěžním	soutěžní	k2eAgInSc7d1
návrhem	návrh	k1gInSc7
fasetového	fasetový	k2eAgInSc2d1
proskleného	prosklený	k2eAgInSc2d1
mrakodrapu	mrakodrap	k1gInSc2
na	na	k7c4
berlínské	berlínský	k2eAgFnPc4d1
Friedrichstrasse	Friedrichstrasse	k1gFnPc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
odvážně	odvážně	k6eAd1
oprostil	oprostit	k5eAaPmAgMnS
od	od	k7c2
jakéhokoli	jakýkoli	k3yIgInSc2
ornamentu	ornament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
následoval	následovat	k5eAaImAgInS
návrh	návrh	k1gInSc1
nového	nový	k2eAgInSc2d1
mrakodrapu	mrakodrap	k1gInSc2
s	s	k7c7
názvem	název	k1gInSc7
Glass	Glass	k1gInSc1
Scyscraper	Scyscrapra	k1gFnPc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
ještě	ještě	k6eAd1
vyšší	vysoký	k2eAgMnSc1d2
a	a	k8xC
zaoblený	zaoblený	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Mies	Mies	k6eAd1
s	s	k7c7
průkopnickými	průkopnický	k2eAgInPc7d1
projekty	projekt	k1gInPc7
pokračoval	pokračovat	k5eAaImAgInS
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc7
vrcholem	vrchol	k1gInSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
dvě	dva	k4xCgFnPc4
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
mistrovská	mistrovský	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
:	:	kIx,
stavba	stavba	k1gFnSc1
dočasného	dočasný	k2eAgInSc2d1
německého	německý	k2eAgInSc2d1
pavilonu	pavilon	k1gInSc2
pro	pro	k7c4
Mezinárodní	mezinárodní	k2eAgFnSc4d1
výstavu	výstava	k1gFnSc4
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
(	(	kIx(
<g/>
často	často	k6eAd1
nazýván	nazýván	k2eAgInSc1d1
Pavilon	pavilon	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1929	#num#	k4
(	(	kIx(
<g/>
na	na	k7c6
původním	původní	k2eAgNnSc6d1
místě	místo	k1gNnSc6
byl	být	k5eAaImAgInS
pavilon	pavilon	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
znovuvybudován	znovuvybudovat	k5eAaPmNgMnS
<g/>
)	)	kIx)
a	a	k8xC
slavná	slavný	k2eAgFnSc1d1
vila	vila	k1gFnSc1
Tugendhat	Tugendhat	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
dokončená	dokončený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Připojil	připojit	k5eAaPmAgMnS
se	se	k3xPyFc4
k	k	k7c3
německé	německý	k2eAgFnSc3d1
avantgardě	avantgarda	k1gFnSc3
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	s	k7c7
spoluzakladatelem	spoluzakladatel	k1gMnSc7
moderního	moderní	k2eAgInSc2d1
designového	designový	k2eAgInSc2d1
časopisu	časopis	k1gInSc2
G	G	kA
(	(	kIx(
<g/>
Gestaltung	Gestaltung	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
první	první	k4xOgNnSc1
číslo	číslo	k1gNnSc1
vyšlo	vyjít	k5eAaPmAgNnS
v	v	k7c6
červenci	červenec	k1gInSc6
1923	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Význačného	význačný	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
dosáhl	dosáhnout	k5eAaPmAgMnS
jako	jako	k9
ředitel	ředitel	k1gMnSc1
Werkbundu	Werkbund	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgInS
na	na	k7c4
organizaci	organizace	k1gFnSc4
vybudování	vybudování	k1gNnSc2
sídliště	sídliště	k1gNnSc2
Weissenhof	Weissenhof	k1gMnSc1
ve	v	k7c6
Stuttgartu	Stuttgart	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
vystavěno	vystavět	k5eAaPmNgNnS
pro	pro	k7c4
výstavu	výstava	k1gFnSc4
moderní	moderní	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
a	a	k8xC
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
získalo	získat	k5eAaPmAgNnS
široký	široký	k2eAgInSc4d1
ohlas	ohlas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
zakladatelů	zakladatel	k1gMnPc2
architektonické	architektonický	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Der	drát	k5eAaImRp2nS
Ring	ring	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijal	přijmout	k5eAaPmAgMnS
pozici	pozice	k1gFnSc4
vedoucího	vedoucí	k1gMnSc2
katedry	katedra	k1gFnSc2
architektury	architektura	k1gFnSc2
na	na	k7c6
avantgardní	avantgardní	k2eAgFnSc6d1
škole	škola	k1gFnSc6
umění	umění	k1gNnSc2
a	a	k8xC
designu	design	k1gInSc2
Bauhausu	Bauhaus	k1gInSc2
<g/>
,	,	kIx,
jehož	jenž	k3xRgNnSc4,k3xOyRp3gNnSc4
funkcionalistické	funkcionalistický	k2eAgNnSc4d1
používání	používání	k1gNnSc4
jednoduchých	jednoduchý	k2eAgInPc2d1
geometrických	geometrický	k2eAgInPc2d1
tvarů	tvar	k1gInPc2
při	při	k7c6
návrhu	návrh	k1gInSc6
užitkových	užitkový	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
plně	plně	k6eAd1
přijal	přijmout	k5eAaPmAgInS
a	a	k8xC
rozvíjel	rozvíjet	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
jeho	jeho	k3xOp3gMnSc7
posledním	poslední	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k9
jiní	jiný	k2eAgMnPc1d1
avantgardní	avantgardní	k2eAgMnPc1d1
architekti	architekt	k1gMnPc1
i	i	k8xC
Mies	Mies	k1gInSc1
založil	založit	k5eAaPmAgInS
své	svůj	k3xOyFgNnSc4
architektonické	architektonický	k2eAgNnSc4d1
poslání	poslání	k1gNnSc4
a	a	k8xC
zásady	zásada	k1gFnPc4
na	na	k7c6
chápání	chápání	k1gNnSc6
a	a	k8xC
výkladu	výklad	k1gInSc6
myšlenek	myšlenka	k1gFnPc2
těch	ten	k3xDgMnPc2
teoretiků	teoretik	k1gMnPc2
a	a	k8xC
kritiků	kritik	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
uvažovali	uvažovat	k5eAaImAgMnP
o	o	k7c6
klesajícím	klesající	k2eAgInSc6d1
významu	význam	k1gInSc6
tradičních	tradiční	k2eAgInPc2d1
stylů	styl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
teoretické	teoretický	k2eAgFnSc2d1
myšlenky	myšlenka	k1gFnSc2
přijal	přijmout	k5eAaPmAgInS
za	za	k7c4
vlastní	vlastní	k2eAgNnSc4d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
estetické	estetický	k2eAgInPc4d1
principy	princip	k1gInPc4
ruského	ruský	k2eAgInSc2d1
konstruktivismu	konstruktivismus	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
ideologii	ideologie	k1gFnSc4
„	„	k?
<g/>
účelné	účelný	k2eAgNnSc1d1
<g/>
“	“	k?
skulpturální	skulpturální	k2eAgFnSc2d1
montáže	montáž	k1gFnSc2
moderních	moderní	k2eAgInPc2d1
průmyslových	průmyslový	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mies	Mies	k1gInSc1
našel	najít	k5eAaPmAgInS
inspiraci	inspirace	k1gFnSc4
u	u	k7c2
nizozemského	nizozemský	k2eAgNnSc2d1
uměleckého	umělecký	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
De	De	k?
Stijl	Stijla	k1gFnPc2
a	a	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gInPc6
jednoduchých	jednoduchý	k2eAgInPc6d1
lineárních	lineární	k2eAgInPc6d1
tvarech	tvar	k1gInPc6
a	a	k8xC
plochách	plocha	k1gFnPc6
<g/>
,	,	kIx,
přesných	přesný	k2eAgFnPc6d1
liniích	linie	k1gFnPc6
<g/>
,	,	kIx,
čistém	čistý	k2eAgNnSc6d1
využití	využití	k1gNnSc6
barev	barva	k1gFnPc2
a	a	k8xC
rozšíření	rozšíření	k1gNnSc4
prostoru	prostor	k1gInSc2
kolem	kolem	k6eAd1
a	a	k8xC
vně	vně	k6eAd1
vnitřních	vnitřní	k2eAgFnPc2d1
stěn	stěna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miesovi	Miesův	k2eAgMnPc1d1
se	se	k3xPyFc4
zejména	zejména	k9
zamlouvalo	zamlouvat	k5eAaImAgNnS
Rietveldovo	Rietveldův	k2eAgNnSc1d1
vrstvení	vrstvení	k1gNnSc1
dílčích	dílčí	k2eAgInPc2d1
funkčních	funkční	k2eAgInPc2d1
prostorů	prostor	k1gInPc2
uvnitř	uvnitř	k7c2
celkového	celkový	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
a	a	k8xC
zdůraznění	zdůraznění	k1gNnSc4
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1
Adolfa	Adolf	k1gMnSc2
Loose	Loose	k1gFnSc2
našly	najít	k5eAaPmAgFnP
u	u	k7c2
Miese	Miese	k1gFnSc2
odezvu	odezva	k1gFnSc4
<g/>
,	,	kIx,
především	především	k9
myšlenka	myšlenka	k1gFnSc1
nahrazení	nahrazení	k1gNnSc2
složitých	složitý	k2eAgInPc2d1
užitých	užitý	k2eAgInPc2d1
uměleckých	umělecký	k2eAgInPc2d1
ornamentů	ornament	k1gInPc2
přímým	přímý	k2eAgNnSc7d1
zobrazením	zobrazení	k1gNnSc7
vlastních	vlastní	k2eAgFnPc2d1
vizuálních	vizuální	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
materiálu	materiál	k1gInSc2
a	a	k8xC
tvarů	tvar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loos	Loos	k1gInSc1
navrhoval	navrhovat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
umění	umění	k1gNnSc2
a	a	k8xC
řemesla	řemeslo	k1gNnSc2
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
na	na	k7c6
architektuře	architektura	k1gFnSc6
zcela	zcela	k6eAd1
nezávislá	závislý	k2eNgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
architekt	architekt	k1gMnSc1
by	by	kYmCp3nS
už	už	k6eAd1
neměl	mít	k5eNaImAgMnS
řídit	řídit	k5eAaImF
ty	ten	k3xDgInPc4
ornamenty	ornament	k1gInPc4
<g/>
,	,	kIx,
jak	jak	k6eAd1
diktovala	diktovat	k5eAaImAgFnS
pařížská	pařížský	k2eAgFnSc1d1
École	École	k1gFnSc1
des	des	k1gNnSc2
Beaux-Arts	Beaux-Artsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mies	Mies	k1gInSc4
také	také	k9
obdivoval	obdivovat	k5eAaImAgMnS
jeho	jeho	k3xOp3gFnPc4
myšlenky	myšlenka	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
v	v	k7c6
anonymitě	anonymita	k1gFnSc6
moderního	moderní	k2eAgInSc2d1
života	život	k1gInSc2
lze	lze	k6eAd1
najít	najít	k5eAaPmF
ušlechtilost	ušlechtilost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Evropští	evropský	k2eAgMnPc1d1
architekti	architekt	k1gMnPc1
vzhlíželi	vzhlížet	k5eAaImAgMnP
ke	k	k7c3
smělým	smělý	k2eAgInPc3d1
projektům	projekt	k1gInPc3
amerických	americký	k2eAgMnPc2d1
architektů	architekt	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
další	další	k2eAgMnPc1d1
architekti	architekt	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
zhlédli	zhlédnout	k5eAaPmAgMnP
výstavu	výstava	k1gFnSc4
Wrightových	Wrightův	k2eAgNnPc2d1
portfolií	portfolio	k1gNnPc2
uspořádanou	uspořádaný	k2eAgFnSc7d1
německým	německý	k2eAgMnSc7d1
nakladatelem	nakladatel	k1gMnSc7
Wasmuthem	Wasmuth	k1gInSc7
<g/>
,	,	kIx,
uchvátil	uchvátit	k5eAaPmAgMnS
Miese	Miese	k1gFnPc4
tzv.	tzv.	kA
volně	volně	k6eAd1
plynoucí	plynoucí	k2eAgInSc1d1
prostor	prostor	k1gInSc1
vzájemně	vzájemně	k6eAd1
propojených	propojený	k2eAgFnPc2d1
místností	místnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
obklopují	obklopovat	k5eAaImIp3nP
vnější	vnější	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
ukazují	ukazovat	k5eAaImIp3nP
plány	plán	k1gInPc4
otevřeného	otevřený	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
Wrightových	Wrightův	k2eAgFnPc2d1
tzv.	tzv.	kA
Prairie	Prairie	k1gFnSc1
Houses	Houses	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
</s>
<s>
Mies	Mies	k6eAd1
se	se	k3xPyFc4
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
náročnému	náročný	k2eAgInSc3d1
úkolu	úkol	k1gInSc3
<g/>
:	:	kIx,
vytvořit	vytvořit	k5eAaPmF
nový	nový	k2eAgInSc4d1
architektonický	architektonický	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
reprezentovat	reprezentovat	k5eAaImF
novou	nový	k2eAgFnSc4d1
éru	éra	k1gFnSc4
technologie	technologie	k1gFnSc2
a	a	k8xC
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cítil	cítit	k5eAaImAgMnS
potřebu	potřeba	k1gFnSc4
architektury	architektura	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
vyjadřovala	vyjadřovat	k5eAaImAgFnS
a	a	k8xC
zároveň	zároveň	k6eAd1
byla	být	k5eAaImAgFnS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
dobou	doba	k1gFnSc7
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
se	se	k3xPyFc4
například	například	k6eAd1
spiritualismus	spiritualismus	k1gInSc1
našel	najít	k5eAaPmAgInS
v	v	k7c6
gotické	gotický	k2eAgFnSc6d1
architektuře	architektura	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
dosáhl	dosáhnout	k5eAaPmAgMnS
svého	svůj	k3xOyFgInSc2
duchovního	duchovní	k2eAgInSc2d1
cíle	cíl	k1gInSc2
<g/>
,	,	kIx,
aplikoval	aplikovat	k5eAaBmAgInS
disciplinovaný	disciplinovaný	k2eAgInSc1d1
proces	proces	k1gInSc1
návrhu	návrh	k1gInSc2
s	s	k7c7
použitím	použití	k1gNnSc7
racionálního	racionální	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věřil	věřit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
tvar	tvar	k1gInSc1
a	a	k8xC
uspořádání	uspořádání	k1gNnSc1
každého	každý	k3xTgInSc2
konstrukčního	konstrukční	k2eAgInSc2d1
prvku	prvek	k1gInSc2
<g/>
,	,	kIx,
především	především	k6eAd1
zahrnutí	zahrnutí	k1gNnSc1
uzavřeného	uzavřený	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
,	,	kIx,
přispívá	přispívat	k5eAaImIp3nS
k	k	k7c3
jednotnému	jednotný	k2eAgInSc3d1
výrazu	výraz	k1gInSc3
stavby	stavba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mies	Mies	k6eAd1
byl	být	k5eAaImAgMnS
samouk	samouk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
důkladně	důkladně	k6eAd1
studoval	studovat	k5eAaImAgMnS
slavné	slavný	k2eAgMnPc4d1
filozofy	filozof	k1gMnPc4
a	a	k8xC
myslitele	myslitel	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
lépe	dobře	k6eAd2
pochopil	pochopit	k5eAaPmAgMnS
povahu	povaha	k1gFnSc4
a	a	k8xC
základní	základní	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
technické	technický	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
žil	žít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
děl	dělo	k1gNnPc2
filozofů	filozof	k1gMnPc2
a	a	k8xC
myslitelů	myslitel	k1gMnPc2
těžil	těžit	k5eAaImAgMnS
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
jakýkoli	jakýkoli	k3yIgMnSc1
jiný	jiný	k2eAgMnSc1d1
průkopník	průkopník	k1gMnSc1
modernismu	modernismus	k1gInSc2
<g/>
,	,	kIx,
načerpal	načerpat	k5eAaPmAgMnS
tak	tak	k6eAd1
nápady	nápad	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
měly	mít	k5eAaImAgFnP
význam	význam	k1gInSc4
pro	pro	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
architektonický	architektonický	k2eAgInSc4d1
cíl	cíl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miesova	Miesův	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
se	se	k3xPyFc4
řídila	řídit	k5eAaImAgFnS
vysoce	vysoce	k6eAd1
abstrakčními	abstrakční	k2eAgFnPc7d1
zásadami	zásada	k1gFnPc7
a	a	k8xC
jeho	jeho	k3xOp3gInPc1
zobecněné	zobecněný	k2eAgInPc1d1
popisy	popis	k1gInPc1
těchto	tento	k3xDgFnPc2
zásad	zásada	k1gFnPc2
záměrně	záměrně	k6eAd1
nechávají	nechávat	k5eAaImIp3nP
prostor	prostor	k1gInSc4
pro	pro	k7c4
interpretaci	interpretace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
jsou	být	k5eAaImIp3nP
jeho	jeho	k3xOp3gFnPc4
stavby	stavba	k1gFnPc4
výtvorem	výtvor	k1gInSc7
krásy	krása	k1gFnSc2
a	a	k8xC
řemeslné	řemeslný	k2eAgFnSc2d1
zručnosti	zručnost	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
na	na	k7c4
ně	on	k3xPp3gFnPc4
člověk	člověk	k1gMnSc1
dívá	dívat	k5eAaImIp3nS
na	na	k7c4
vlastní	vlastní	k2eAgNnPc4d1
oči	oko	k1gNnPc4
<g/>
,	,	kIx,
působí	působit	k5eAaImIp3nS
velmi	velmi	k6eAd1
bezprostředně	bezprostředně	k6eAd1
a	a	k8xC
jednoduše	jednoduše	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
všech	všecek	k3xTgFnPc6
stránkách	stránka	k1gFnPc6
potvrzují	potvrzovat	k5eAaImIp3nP
jeho	jeho	k3xOp3gNnSc1
stavby	stavba	k1gFnPc1
snahu	snaha	k1gFnSc4
o	o	k7c4
vyjádření	vyjádření	k1gNnSc4
moderní	moderní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
od	od	k7c2
celkové	celkový	k2eAgFnSc2d1
koncepce	koncepce	k1gFnSc2
až	až	k9
po	po	k7c4
nejmenší	malý	k2eAgInPc4d3
detaily	detail	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hloubka	hloubka	k1gFnSc1
významu	význam	k1gInSc2
nad	nad	k7c4
rámec	rámec	k1gInSc4
estetických	estetický	k2eAgFnPc2d1
kvalit	kvalita	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
jeho	jeho	k3xOp3gNnSc4
dílo	dílo	k1gNnSc4
vyjadřuje	vyjadřovat	k5eAaImIp3nS
<g/>
,	,	kIx,
budila	budit	k5eAaImAgFnS
pozornost	pozornost	k1gFnSc1
mnoha	mnoho	k4c2
současných	současný	k2eAgMnPc2d1
filozofů	filozof	k1gMnPc2
a	a	k8xC
teoretiků	teoretik	k1gMnPc2
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
pokračovali	pokračovat	k5eAaImAgMnP
v	v	k7c6
poznávání	poznávání	k1gNnSc6
jeho	jeho	k3xOp3gFnSc2
architektury	architektura	k1gFnSc2
a	a	k8xC
v	v	k7c6
úvahách	úvaha	k1gFnPc6
nad	nad	k7c7
ní	on	k3xPp3gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Emigrace	emigrace	k1gFnSc1
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
Třicátá	třicátý	k4xOgNnPc1
léta	léto	k1gNnPc1
se	se	k3xPyFc4
nesla	nést	k5eAaImAgNnP
ve	v	k7c6
znamení	znamení	k1gNnSc6
celosvětové	celosvětový	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
krize	krize	k1gFnSc2
a	a	k8xC
úbytku	úbytek	k1gInSc3
zakázek	zakázka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
se	se	k3xPyFc4
Mies	Mies	k1gInSc1
na	na	k7c4
žádost	žádost	k1gFnSc4
svého	svůj	k3xOyFgMnSc2
kolegy	kolega	k1gMnSc2
Waltera	Walter	k1gMnSc2
Gropia	Gropius	k1gMnSc2
stal	stát	k5eAaPmAgMnS
posledním	poslední	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
živořícího	živořící	k2eAgInSc2d1
Bauhausu	Bauhaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
byla	být	k5eAaImAgFnS
státem	stát	k1gInSc7
podporovaná	podporovaný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
nacisty	nacista	k1gMnSc2
donucena	donucen	k2eAgFnSc1d1
opustit	opustit	k5eAaPmF
areál	areál	k1gInSc4
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Dessau	Dessaus	k1gInSc6
<g/>
,	,	kIx,
přesídlil	přesídlit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
Mies	Mies	k1gInSc4
do	do	k7c2
opuštěné	opuštěný	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
na	na	k7c4
telefony	telefon	k1gInPc4
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
byl	být	k5eAaImAgInS
provoz	provoz	k1gInSc1
univerzity	univerzita	k1gFnSc2
neudržitelný	udržitelný	k2eNgMnSc1d1
(	(	kIx(
<g/>
v	v	k7c6
dubnu	duben	k1gInSc6
provedlo	provést	k5eAaPmAgNnS
gestapo	gestapo	k1gNnSc1
na	na	k7c6
škole	škola	k1gFnSc6
razii	razie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
červenci	červenec	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
Mies	Mies	k1gInSc1
spolu	spolu	k6eAd1
s	s	k7c7
vedením	vedení	k1gNnSc7
fakulty	fakulta	k1gFnSc2
tedy	tedy	k9
odhlasoval	odhlasovat	k5eAaPmAgInS
uzavření	uzavření	k1gNnSc4
Bauhausu	Bauhaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
zakázky	zakázka	k1gFnPc1
příliš	příliš	k6eAd1
nehrnuly	hrnout	k5eNaImAgFnP
a	a	k8xC
Mies	Mies	k1gInSc4
stavěl	stavět	k5eAaImAgMnS
velmi	velmi	k6eAd1
málo	málo	k6eAd1
(	(	kIx(
<g/>
jednou	jednou	k6eAd1
z	z	k7c2
provedených	provedený	k2eAgFnPc2d1
zakázek	zakázka	k1gFnPc2
byl	být	k5eAaImAgInS
byt	byt	k1gInSc1
architekta	architekt	k1gMnSc2
Philipa	Philip	k1gMnSc2
Johnsona	Johnson	k1gMnSc2
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
nacisté	nacista	k1gMnPc1
Miesův	Miesův	k2eAgInSc4d1
styl	styl	k1gInSc4
odmítli	odmítnout	k5eAaPmAgMnP
a	a	k8xC
označili	označit	k5eAaPmAgMnP
za	za	k7c7
„	„	k?
<g/>
neněmecký	německý	k2eNgInSc4d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Protože	protože	k8xS
byl	být	k5eAaImAgInS
znechucen	znechutit	k5eAaPmNgInS
poměry	poměr	k1gInPc7
v	v	k7c6
Německu	Německo	k1gNnSc6
a	a	k8xC
cítil	cítit	k5eAaImAgMnS
se	se	k3xPyFc4
nešťastný	šťastný	k2eNgMnSc1d1
<g/>
,	,	kIx,
opustil	opustit	k5eAaPmAgMnS
<g/>
,	,	kIx,
ač	ač	k8xS
neochotně	ochotně	k6eNd1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
svou	svůj	k3xOyFgFnSc4
vlast	vlast	k1gFnSc4
–	–	k?
viděl	vidět	k5eAaImAgMnS
totiž	totiž	k9
<g/>
,	,	kIx,
že	že	k8xS
vyhlídky	vyhlídka	k1gFnPc1
na	na	k7c4
stavební	stavební	k2eAgFnPc4d1
zakázky	zakázka	k1gFnPc4
jsou	být	k5eAaImIp3nP
téměř	téměř	k6eAd1
nulové	nulový	k2eAgFnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
přijal	přijmout	k5eAaPmAgMnS
zakázku	zakázka	k1gFnSc4
ve	v	k7c6
Wyomingu	Wyoming	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
nabídku	nabídka	k1gFnSc4
stát	stát	k5eAaPmF,k5eAaImF
se	se	k3xPyFc4
vedoucím	vedoucí	k1gMnPc3
oddělení	oddělení	k1gNnSc2
architektury	architektura	k1gFnSc2
na	na	k7c6
nově	nov	k1gInSc6
založeném	založený	k2eAgInSc6d1
Illinois	Illinois	k1gFnSc4
Institute	institut	k1gInSc5
of	of	k?
Technology	technolog	k1gMnPc7
(	(	kIx(
<g/>
IIT	IIT	kA
<g/>
)	)	kIx)
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
také	také	k9
představil	představit	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
způsob	způsob	k1gInSc1
vzdělávání	vzdělávání	k1gNnSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
známý	známý	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
druhá	druhý	k4xOgFnSc1
chicagská	chicagský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
ovlivnil	ovlivnit	k5eAaPmAgInS
Severní	severní	k2eAgFnSc4d1
Ameriku	Amerika	k1gFnSc4
a	a	k8xC
Evropu	Evropa	k1gFnSc4
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
</s>
<s>
Mies	Mies	k1gInSc1
se	se	k3xPyFc4
usadil	usadit	k5eAaPmAgInS
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
vedoucím	vedoucí	k1gMnSc7
oddělení	oddělení	k1gNnSc2
architektury	architektura	k1gFnSc2
na	na	k7c6
Illinois	Illinois	k1gFnSc6
Institute	institut	k1gInSc5
of	of	k?
Technology	technolog	k1gMnPc7
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
IIT	IIT	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jmenování	jmenování	k1gNnPc1
do	do	k7c2
této	tento	k3xDgFnSc2
funkce	funkce	k1gFnSc2
mu	on	k3xPp3gMnSc3
přineslo	přinést	k5eAaPmAgNnS
několik	několik	k4yIc4
výhod	výhoda	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgMnS
zadán	zadán	k2eAgInSc4d1
návrh	návrh	k1gInSc4
nových	nový	k2eAgFnPc2d1
budov	budova	k1gFnPc2
a	a	k8xC
hlavní	hlavní	k2eAgInSc4d1
plán	plán	k1gInSc4
kampusu	kampus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
budovy	budova	k1gFnPc1
v	v	k7c6
areálu	areál	k1gInSc6
dodnes	dodnes	k6eAd1
najdeme	najít	k5eAaPmIp1nP
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
ně	on	k3xPp3gMnPc4
Alumni	Alumni	k1gMnPc4
Hall	Hall	k1gInSc1
<g/>
,	,	kIx,
Kaple	kaple	k1gFnSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
vrcholné	vrcholný	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
Crown	Crowna	k1gFnPc2
Hall	Halla	k1gFnPc2
<g/>
,	,	kIx,
navržené	navržený	k2eAgInPc4d1
jako	jako	k8xC,k8xS
sídlo	sídlo	k1gNnSc4
oddělení	oddělení	k1gNnSc2
architektury	architektura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Crown	Crown	k1gMnSc1
Hall	Hall	k1gMnSc1
je	být	k5eAaImIp3nS
obecně	obecně	k6eAd1
považován	považován	k2eAgInSc1d1
za	za	k7c4
Miesovo	Miesův	k2eAgNnSc4d1
nejdokonalejší	dokonalý	k2eAgNnSc4d3
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
za	za	k7c4
formulaci	formulace	k1gFnSc4
miesovské	miesovský	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
získal	získat	k5eAaPmAgMnS
americké	americký	k2eAgNnSc4d1
občanství	občanství	k1gNnSc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
přetrhal	přetrhat	k5eAaPmAgMnS
jakékoli	jakýkoli	k3yIgFnPc4
vazby	vazba	k1gFnPc4
s	s	k7c7
rodným	rodný	k2eAgNnSc7d1
Německem	Německo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
zbylých	zbylý	k2eAgNnPc2d1
30	#num#	k4
let	léto	k1gNnPc2
života	život	k1gInSc2
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
americká	americký	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
značí	značit	k5eAaImIp3nS
konstrukčním	konstrukční	k2eAgInSc7d1
a	a	k8xC
čistým	čistý	k2eAgInSc7d1
přístupem	přístup	k1gInSc7
k	k	k7c3
dosažení	dosažení	k1gNnSc3
nové	nový	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměřil	zaměřit	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
uzavřít	uzavřít	k5eAaPmF
volné	volný	k2eAgInPc4d1
a	a	k8xC
upravitelné	upravitelný	k2eAgInPc4d1
univerzální	univerzální	k2eAgInPc4d1
prostory	prostor	k1gInPc4
jasně	jasně	k6eAd1
uspořádanou	uspořádaný	k2eAgFnSc7d1
konstrukcí	konstrukce	k1gFnSc7
z	z	k7c2
průmyslové	průmyslový	k2eAgFnSc2d1
oceli	ocel	k1gFnSc2
vyplněné	vyplněný	k2eAgInPc1d1
velkými	velký	k2eAgInPc7d1
tabulkovými	tabulkový	k2eAgInPc7d1
skly	sklo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1
rané	raný	k2eAgInPc1d1
projekty	projekt	k1gInPc1
v	v	k7c6
areálu	areál	k1gInSc6
IIT	IIT	kA
a	a	k8xC
projekty	projekt	k1gInPc1
pro	pro	k7c4
stavebního	stavební	k2eAgMnSc4d1
investora	investor	k1gMnSc4
Herba	Herb	k1gMnSc2
Greenwalda	Greenwald	k1gMnSc2
představily	představit	k5eAaPmAgFnP
americké	americký	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
styl	styl	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
zdál	zdát	k5eAaImAgInS
být	být	k5eAaImF
přirozeným	přirozený	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
téměř	téměř	k6eAd1
zapomenutého	zapomenutý	k2eAgInSc2d1
stylu	styl	k1gInSc2
chicagské	chicagský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
devatenáctého	devatenáctý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
mnohé	mnohý	k2eAgInPc4d1
americké	americký	k2eAgInPc4d1
kulturní	kulturní	k2eAgInPc4d1
a	a	k8xC
vzdělávací	vzdělávací	k2eAgFnPc4d1
instituce	instituce	k1gFnPc4
<g/>
,	,	kIx,
investory	investor	k1gMnPc4
<g/>
,	,	kIx,
veřejné	veřejný	k2eAgInPc4d1
úřady	úřad	k1gInPc4
či	či	k8xC
velké	velký	k2eAgFnPc4d1
společnosti	společnost	k1gFnPc4
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
architektura	architektura	k1gFnSc1
<g/>
,	,	kIx,
s	s	k7c7
kořeny	kořen	k1gInPc7
v	v	k7c6
německém	německý	k2eAgInSc6d1
Bauhausu	Bauhaus	k1gInSc6
a	a	k8xC
západoevropském	západoevropský	k2eAgInSc6d1
internacionálním	internacionální	k2eAgInSc6d1
stylu	styl	k1gInSc6
<g/>
,	,	kIx,
stala	stát	k5eAaPmAgFnS
uznávanou	uznávaný	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
v	v	k7c6
Americe	Amerika	k1gFnSc6
</s>
<s>
Mies	Mies	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
ateliéru	ateliér	k1gInSc6
v	v	k7c6
centru	centrum	k1gNnSc6
Chicaga	Chicago	k1gNnSc2
po	po	k7c4
celých	celý	k2eAgInPc2d1
31	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gInPc4
významné	významný	k2eAgInPc4d1
projekty	projekt	k1gInPc4
na	na	k7c6
území	území	k1gNnSc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
patří	patřit	k5eAaImIp3nP
<g/>
:	:	kIx,
výškové	výškový	k2eAgInPc1d1
obytné	obytný	k2eAgInPc1d1
domy	dům	k1gInPc1
Lake	Lak	k1gFnSc2
Shore	Shor	k1gInSc5
Drive	drive	k1gInSc1
Apartments	Apartments	k1gInSc1
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
<g/>
,	,	kIx,
skleněný	skleněný	k2eAgInSc4d1
pavilon	pavilon	k1gInSc4
Farnsworth	Farnswortha	k1gFnPc2
House	house	k1gNnSc1
<g/>
,	,	kIx,
budova	budova	k1gFnSc1
fakulty	fakulta	k1gFnSc2
architektury	architektura	k1gFnSc2
Crown	Crown	k1gMnSc1
Hall	Hall	k1gMnSc1
a	a	k8xC
další	další	k2eAgFnPc4d1
budovy	budova	k1gFnPc4
univerzitního	univerzitní	k2eAgInSc2d1
areálu	areál	k1gInSc2
IIT	IIT	kA
nebo	nebo	k8xC
administrativní	administrativní	k2eAgInSc4d1
mrakodrap	mrakodrap	k1gInSc4
Seagram	Seagram	k1gInSc1
Building	Building	k1gInSc1
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
stavby	stavba	k1gFnPc4
se	se	k3xPyFc4
staly	stát	k5eAaPmAgInP
modely	model	k1gInPc1
pro	pro	k7c4
jeho	jeho	k3xOp3gInPc4
další	další	k2eAgInPc4d1
projekty	projekt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
projektoval	projektovat	k5eAaBmAgInS
domy	dům	k1gInPc4
pro	pro	k7c4
zámožné	zámožný	k2eAgMnPc4d1
klienty	klient	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Farnsworth	Farnsworth	k1gInSc1
House	house	k1gNnSc1
</s>
<s>
Farnsworth	Farnsworth	k1gInSc1
House	house	k1gNnSc1
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1946	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
navrhl	navrhnout	k5eAaPmAgInS
Mies	Mies	k1gInSc1
Farnsworth	Farnswortha	k1gFnPc2
House	house	k1gNnSc1
<g/>
,	,	kIx,
víkendové	víkendový	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
nedaleko	nedaleko	k7c2
Chicaga	Chicago	k1gNnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
nezávislou	závislý	k2eNgFnSc4d1
a	a	k8xC
úspěšnou	úspěšný	k2eAgFnSc4d1
lékařku	lékařka	k1gFnSc4
Edith	Edith	k1gMnSc1
Farnsworth	Farnsworth	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skleněný	skleněný	k2eAgInSc1d1
pavilon	pavilon	k1gInSc1
stojí	stát	k5eAaImIp3nS
dva	dva	k4xCgInPc4
metry	metr	k1gInPc4
nad	nad	k7c7
nivou	niva	k1gFnSc7
řeky	řeka	k1gFnSc2
Fox	fox	k1gInSc1
River	River	k1gInSc1
<g/>
,	,	kIx,
obklopený	obklopený	k2eAgInSc1d1
zalesněným	zalesněný	k2eAgInSc7d1
pozemkem	pozemek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Jednoduchý	jednoduchý	k2eAgInSc1d1
lineární	lineární	k2eAgInSc1d1
vnitřní	vnitřní	k2eAgInSc1d1
prostor	prostor	k1gInSc1
je	být	k5eAaImIp3nS
vymezen	vymezit	k5eAaPmNgInS
dokonalou	dokonalý	k2eAgFnSc7d1
bílou	bílý	k2eAgFnSc7d1
konstrukcí	konstrukce	k1gFnSc7
a	a	k8xC
prosklenými	prosklený	k2eAgFnPc7d1
stěnami	stěna	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
dovolují	dovolovat	k5eAaImIp3nP
přírodě	příroda	k1gFnSc3
a	a	k8xC
světlu	světlo	k1gNnSc3
vnitřní	vnitřní	k2eAgInSc4d1
prostor	prostor	k1gInSc4
obklopit	obklopit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dřevěná	dřevěný	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
s	s	k7c7
krbem	krb	k1gInSc7
(	(	kIx(
<g/>
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
také	také	k9
zabudované	zabudovaný	k2eAgNnSc1d1
mechanické	mechanický	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
<g/>
,	,	kIx,
kuchyňská	kuchyňský	k2eAgFnSc1d1
linka	linka	k1gFnSc1
či	či	k8xC
toaleta	toaleta	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
umístěna	umístit	k5eAaPmNgFnS
uvnitř	uvnitř	k7c2
otevřeného	otevřený	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tak	tak	k6eAd1
naznačila	naznačit	k5eAaPmAgFnS
obytný	obytný	k2eAgInSc4d1
prostor	prostor	k1gInSc4
<g/>
,	,	kIx,
jídelnu	jídelna	k1gFnSc4
či	či	k8xC
ložnici	ložnice	k1gFnSc4
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
musel	muset	k5eAaImAgMnS
být	být	k5eAaImF
prostor	prostor	k1gInSc4
rozdělen	rozdělen	k2eAgInSc4d1
zdí	zeď	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masivní	masivní	k2eAgFnSc2d1
vnější	vnější	k2eAgFnSc2d1
zdi	zeď	k1gFnSc2
jsou	být	k5eAaImIp3nP
nahrazeny	nahradit	k5eAaPmNgInP
skleněnými	skleněný	k2eAgFnPc7d1
stěnami	stěna	k1gFnPc7
–	–	k?
dlouhé	dlouhý	k2eAgFnPc1d1
závěsy	závěsa	k1gFnPc1
po	po	k7c6
obvodu	obvod	k1gInSc6
umožňují	umožňovat	k5eAaImIp3nP
volnost	volnost	k1gFnSc4
a	a	k8xC
na	na	k7c4
přání	přání	k1gNnSc4
poskytují	poskytovat	k5eAaImIp3nP
částečné	částečný	k2eAgInPc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
úplné	úplný	k2eAgNnSc1d1
soukromí	soukromí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dům	dům	k1gInSc1
byl	být	k5eAaImAgInS
označen	označit	k5eAaPmNgInS
za	za	k7c4
impozantní	impozantní	k2eAgFnSc4d1
<g/>
,	,	kIx,
za	za	k7c4
chrám	chrám	k1gInSc4
vznášející	vznášející	k2eAgFnSc2d1
se	se	k3xPyFc4
mezi	mezi	k7c7
zemí	zem	k1gFnSc7
a	a	k8xC
nebem	nebe	k1gNnSc7
<g/>
,	,	kIx,
za	za	k7c4
báseň	báseň	k1gFnSc4
<g/>
,	,	kIx,
za	za	k7c4
umělecké	umělecký	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
byl	být	k5eAaImAgInS
dům	dům	k1gInSc1
a	a	k8xC
60	#num#	k4
akrů	akr	k1gInPc2
přilehlé	přilehlý	k2eAgFnSc2d1
zalesněné	zalesněný	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
zakoupen	zakoupen	k2eAgMnSc1d1
památkáři	památkář	k1gMnPc1
v	v	k7c6
aukci	aukce	k1gFnSc6
za	za	k7c4
7,5	7,5	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
jej	on	k3xPp3gMnSc4
vlastní	vlastnit	k5eAaImIp3nS
a	a	k8xC
spravuje	spravovat	k5eAaImIp3nS
národní	národní	k2eAgFnSc1d1
nadace	nadace	k1gFnSc1
National	National	k1gFnSc2
Trust	trust	k1gInSc1
for	forum	k1gNnPc2
Historic	Historice	k1gInPc2
Preservation	Preservation	k1gInSc1
jako	jako	k8xC,k8xS
veřejně	veřejně	k6eAd1
přístupné	přístupný	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
ovlivnila	ovlivnit	k5eAaPmAgFnS
vznik	vznik	k1gInSc4
stovek	stovka	k1gFnPc2
dalších	další	k2eAgInPc2d1
modernistických	modernistický	k2eAgInPc2d1
skleněných	skleněný	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
,	,	kIx,
především	především	k9
Glass	Glass	k1gInSc1
House	house	k1gNnSc1
Philipa	Philip	k1gMnSc2
Johnsona	Johnson	k1gMnSc2
poblíž	poblíž	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
stejně	stejně	k6eAd1
tak	tak	k6eAd1
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
nadace	nadace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dům	dům	k1gInSc1
je	být	k5eAaImIp3nS
ztělesněním	ztělesnění	k1gNnSc7
Miesových	Miesův	k2eAgFnPc2d1
zralých	zralý	k2eAgFnPc2d1
představ	představa	k1gFnPc2
o	o	k7c6
moderní	moderní	k2eAgFnSc6d1
architektuře	architektura	k1gFnSc6
v	v	k7c6
nové	nový	k2eAgFnSc6d1
technické	technický	k2eAgFnSc6d1
éře	éra	k1gFnSc6
<g/>
:	:	kIx,
jeden	jeden	k4xCgInSc4
volný	volný	k2eAgInSc4d1
prostor	prostor	k1gInSc4
s	s	k7c7
minimální	minimální	k2eAgFnSc7d1
konstrukcí	konstrukce	k1gFnSc7
„	„	k?
<g/>
kosti	kost	k1gFnSc2
a	a	k8xC
kůže	kůže	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
zřetelné	zřetelný	k2eAgNnSc1d1
uspořádání	uspořádání	k1gNnSc1
architektonických	architektonický	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
představy	představa	k1gFnPc1
jsou	být	k5eAaImIp3nP
vyjádřeny	vyjádřit	k5eAaPmNgFnP
srozumitelně	srozumitelně	k6eAd1
a	a	k8xC
jednoduše	jednoduše	k6eAd1
<g/>
,	,	kIx,
za	za	k7c4
použití	použití	k1gNnSc4
materiálů	materiál	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
sestaveny	sestavit	k5eAaPmNgInP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
vyjádřily	vyjádřit	k5eAaPmAgFnP
svůj	svůj	k3xOyFgInSc4
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Mrakodrap	mrakodrap	k1gInSc1
IBM	IBM	kA
</s>
<s>
Edith	Edith	k1gInSc1
Farnsworth	Farnswortha	k1gFnPc2
House	house	k1gNnSc1
<g/>
,	,	kIx,
Plano	Plano	k?
<g/>
,	,	kIx,
Illinois	Illinois	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
I.B.	I.B.	k?
<g/>
M.	M.	kA
–	–	k?
regionální	regionální	k2eAgFnSc1d1
kancelářská	kancelářský	k2eAgFnSc1d1
budova	budova	k1gFnSc1
(	(	kIx(
<g/>
spol	spol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
C.	C.	kA
<g/>
F.	F.	kA
Murphy	Murpha	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Chicago	Chicago	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Weissenhof	Weissenhof	k1gInSc1
<g/>
,	,	kIx,
Stuttgart	Stuttgart	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
Barcelonský	barcelonský	k2eAgInSc1d1
pavilon	pavilon	k1gInSc1
–	–	k?
pavilon	pavilon	k1gInSc4
německé	německý	k2eAgFnSc2d1
expozice	expozice	k1gFnSc2
pro	pro	k7c4
světovou	světový	k2eAgFnSc4d1
výstavu	výstava	k1gFnSc4
<g/>
,	,	kIx,
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Vila	vila	k1gFnSc1
Tugendhat	Tugendhat	k1gFnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
Seagram	Seagram	k1gInSc1
Building	Building	k1gInSc1
<g/>
,	,	kIx,
Manhattan	Manhattan	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
City	City	k1gFnSc2
<g/>
,	,	kIx,
USA	USA	kA
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
AIA	AIA	kA
<g/>
,	,	kIx,
1960	#num#	k4
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
RIBA	RIBA	kA
<g/>
,	,	kIx,
1959	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Ludwig	Ludwig	k1gInSc4
Mies	Mies	k1gInSc1
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Rohe	Rohe	k1gFnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ludwig	Ludwig	k1gInSc1
Mies	Miesa	k1gFnPc2
van	vana	k1gFnPc2
der	drát	k5eAaImRp2nS
Rohe	Roh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavění	stavění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Arbor	Arbor	k1gInSc1
vitae	vita	k1gInSc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
90	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
German	German	k1gMnSc1
Embassy	Embassa	k1gFnSc2
Building	Building	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopaedia	Encyclopaedium	k1gNnSc2
of	of	k?
Saint	Saint	k1gMnSc1
Petersburg	Petersburg	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Vila	vila	k1gFnSc1
Tugendhat	Tugendhat	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
In	In	k1gMnSc4
Memoriam	Memoriam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Art	Art	k1gFnSc1
Institute	institut	k1gInSc5
of	of	k?
Chicago	Chicago	k1gNnSc4
Quarterly	Quarterla	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Art	Art	k1gFnSc1
Institute	institut	k1gInSc5
of	of	k?
Chicago	Chicago	k1gNnSc1
<g/>
,	,	kIx,
February	Februar	k1gInPc1
1960	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
February	Februara	k1gFnSc2
22	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vila	vila	k1gFnSc1
Tugendhat	Tugendhat	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WELCH	WELCH	kA
<g/>
,	,	kIx,
Frank	Frank	k1gMnSc1
D.	D.	kA
<g/>
,	,	kIx,
Ray	Ray	k1gFnPc1
<g/>
,	,	kIx,
Landry	Landr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Photographs	Photographs	k1gInSc4
by	by	kYmCp3nS
Paul	Paul	k1gMnSc1
Hester	Hester	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foreword	Foreword	k1gInSc4
by	by	kYmCp3nS
Philip	Philip	k1gMnSc1
Johnson	Johnson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drawings	Drawings	k1gInSc1
by	by	kYmCp3nS
Brian	Brian	k1gInSc4
Fitzsimmons	Fitzsimmonsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philip	Philip	k1gMnSc1
Johnson	Johnson	k1gMnSc1
&	&	k?
Texas	Texas	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austin	Austin	k1gInSc1
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
Texas	Texas	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
292791348	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
318	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Compare	Compar	k1gMnSc5
Arthur	Arthura	k1gFnPc2
Lubow	Lubow	k1gMnPc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
"	"	kIx"
<g/>
The	The	k1gMnSc1
Contextualizer	Contextualizer	k1gMnSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
April	April	k1gInSc1
6	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
4	#num#	k4
<g/>
;	;	kIx,
excerpt	excerpt	k1gInSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
...	...	k?
<g/>
a	a	k8xC
skyscraper	skyscraper	k1gMnSc1
that	that	k1gMnSc1
Nouvel	Nouvel	k1gMnSc1
(	(	kIx(
<g/>
adapting	adapting	k1gInSc1
a	a	k8xC
term	term	k1gInSc1
from	from	k1gMnSc1
the	the	k?
artist	artist	k1gMnSc1
Brâncuș	Brâncuș	k1gFnSc2
<g/>
)	)	kIx)
called	called	k1gInSc1
the	the	k?
“	“	k?
<g/>
tour	tour	k1gMnSc1
sans	sans	k6eAd1
fins	fins	k6eAd1
<g/>
,	,	kIx,
<g/>
”	”	k?
or	or	k?
endless	endless	k1gInSc1
tower	tower	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Conceived	Conceived	k1gInSc1
as	as	k1gNnSc2
a	a	k8xC
kind	kind	k6eAd1
of	of	k?
minaret	minaret	k1gInSc1
alongside	alongsid	k1gInSc5
the	the	k?
squat	squat	k1gInSc4
<g/>
,	,	kIx,
monumental	monumentat	k5eAaImAgMnS,k5eAaPmAgMnS
Grande	grand	k1gMnSc5
Arche	Arche	k1gFnSc3
de	de	k?
La	la	k1gNnSc4
Défense	Défense	k1gFnSc2
<g/>
,	,	kIx,
the	the	k?
endless	endless	k1gInSc1
tower	tower	k1gInSc1
has	hasit	k5eAaImRp2nS
taken	taken	k2eAgInSc1d1
on	on	k3xPp3gInSc1
some	some	k1gInSc1
of	of	k?
the	the	k?
mystique	mystique	k1gInSc1
of	of	k?
Mies	Mies	k1gInSc1
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Rohe	Rohe	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
unbuilt	unbuilt	k1gMnSc1
Friedrichstrasse	Friedrichstrassa	k1gFnSc3
glass	glass	k6eAd1
skyscraper	skyscraper	k1gInSc4
of	of	k?
1921	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
obscure	obscur	k1gMnSc5
its	its	k?
lower	lower	k1gInSc1
end	end	k?
<g/>
,	,	kIx,
the	the	k?
tower	tower	k1gMnSc1
was	was	k?
designed	designed	k1gMnSc1
to	ten	k3xDgNnSc1
sit	sit	k?
within	within	k1gInSc1
a	a	k8xC
crater	crater	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Its	Its	k1gMnSc1
facade	facást	k5eAaPmIp3nS
<g/>
,	,	kIx,
appearing	appearing	k1gInSc1
to	ten	k3xDgNnSc1
vanish	vanish	k1gInSc1
in	in	k?
the	the	k?
sky	sky	k?
<g/>
,	,	kIx,
changed	changed	k1gMnSc1
as	as	k1gNnSc2
it	it	k?
rose	rosa	k1gFnSc6
<g/>
,	,	kIx,
from	from	k6eAd1
charcoal-colored	charcoal-colored	k1gInSc1
granite	granit	k1gInSc5
to	ten	k3xDgNnSc1
paler	paler	k1gInSc1
stone	ston	k1gInSc5
<g/>
,	,	kIx,
then	then	k1gNnSc4
to	ten	k3xDgNnSc4
aluminum	aluminum	k1gNnSc4
and	and	k?
finally	finalla	k1gFnSc2
to	ten	k3xDgNnSc1
glass	glass	k6eAd1
that	that	k2eAgInSc1d1
became	becam	k1gInSc5
increasingly	increasingl	k1gInPc1
reflective	reflectiv	k1gInSc5
<g/>
,	,	kIx,
all	all	k?
to	ten	k3xDgNnSc1
enhance	enhance	k1gFnPc1
the	the	k?
illusion	illusion	k1gInSc1
of	of	k?
dematerialization	dematerialization	k1gInSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SAPÁK	SAPÁK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tugendhat	Tugendhat	k1gFnSc1
–	–	k?
Dům	dům	k1gInSc1
na	na	k7c4
rozhraní	rozhraní	k1gNnSc4
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
K-Public	K-Publice	k1gFnPc2
<g/>
,	,	kIx,
1913	#num#	k4
<g/>
.	.	kIx.
240	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87028	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PUENTE	PUENTE	kA
<g/>
,	,	kIx,
Moisés	Moisés	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhovory	rozhovor	k1gInPc4
s	s	k7c7
Miesem	Mieso	k1gNnSc7
van	vana	k1gFnPc2
der	drát	k5eAaImRp2nS
Rohe	Roh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlín	Zlín	k1gInSc1
<g/>
:	:	kIx,
Archa	archa	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
904514	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TEGETHOFF	TEGETHOFF	kA
<g/>
,	,	kIx,
Wolf	Wolf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mies	Mies	k1gInSc1
van	van	k1gInSc4
der	drát	k5eAaImRp2nS
Rohe	Roh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnSc1
Villen	Villna	k1gFnPc2
und	und	k?
Landhausprojekte	Landhausprojekt	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krefeld	Krefeld	k1gInSc1
–	–	k?
Essen	Essen	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ludwig	Ludwiga	k1gFnPc2
Mies	Miesa	k1gFnPc2
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Rohe	Rohe	k1gNnSc3
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Ludwig	Ludwiga	k1gFnPc2
Mies	Miesa	k1gFnPc2
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Rohe	Rohe	k1gFnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20010523006	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118582291	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2144	#num#	k4
125X	125X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80057077	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500006293	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
95153318	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80057077	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Německo	Německo	k1gNnSc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
