<s>
Italský	italský	k2eAgMnSc1d1	italský
badatel	badatel	k1gMnSc1	badatel
E.	E.	kA	E.
Emanuele	Emanuela	k1gFnSc3	Emanuela
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
hladinou	hladina	k1gFnSc7	hladina
tzv.	tzv.	kA	tzv.
nervového	nervový	k2eAgInSc2d1	nervový
růstového	růstový	k2eAgInSc2d1	růstový
faktoru	faktor	k1gInSc2	faktor
(	(	kIx(	(
<g/>
NGF	NGF	kA	NGF
<g/>
)	)	kIx)	)
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
vztahu	vztah	k1gInSc2	vztah
<g/>
.	.	kIx.	.
</s>
