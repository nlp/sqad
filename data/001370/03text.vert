<s>
Thallium	Thallium	k1gNnSc1	Thallium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Tl	Tl	k1gFnSc1	Tl
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Thallium	Thallium	k1gNnSc1	Thallium
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
toxický	toxický	k2eAgInSc1d1	toxický
měkký	měkký	k2eAgInSc1d1	měkký
<g/>
,	,	kIx,	,
lesklý	lesklý	k2eAgInSc1d1	lesklý
kov	kov	k1gInSc1	kov
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
řídce	řídce	k6eAd1	řídce
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInSc1d1	vyskytující
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
nalézající	nalézající	k2eAgMnSc1d1	nalézající
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
jako	jako	k9	jako
příměs	příměs	k1gFnSc1	příměs
v	v	k7c6	v
sulfidických	sulfidický	k2eAgFnPc6d1	sulfidická
rudách	ruda	k1gFnPc6	ruda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Tl	Tl	k1gFnSc2	Tl
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
a	a	k8xC	a
Tl	Tl	k1gMnSc1	Tl
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c4	pod
2,39	[number]	k4	2,39
K	K	kA	K
je	být	k5eAaImIp3nS	být
supravodivý	supravodivý	k2eAgInSc1d1	supravodivý
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgMnS	objevit
jej	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
sir	sir	k1gMnSc1	sir
William	William	k1gInSc1	William
Crookes	Crookes	k1gInSc1	Crookes
při	při	k7c6	při
spektroskopickém	spektroskopický	k2eAgNnSc6d1	spektroskopické
zkoumání	zkoumání	k1gNnSc6	zkoumání
obsahu	obsah	k1gInSc2	obsah
telluru	tellur	k1gInSc2	tellur
ve	v	k7c6	v
zbytcích	zbytek	k1gInPc6	zbytek
po	po	k7c6	po
zpracování	zpracování	k1gNnSc6	zpracování
sulfidických	sulfidický	k2eAgFnPc2d1	sulfidická
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Thallium	Thallium	k1gNnSc1	Thallium
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
značně	značně	k6eAd1	značně
vzácným	vzácný	k2eAgInSc7d1	vzácný
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
činí	činit	k5eAaImIp3nS	činit
pouze	pouze	k6eAd1	pouze
0,5	[number]	k4	0,5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
natolik	natolik	k6eAd1	natolik
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
nelze	lze	k6eNd1	lze
změřit	změřit	k5eAaPmF	změřit
ani	ani	k9	ani
nejcitlivějšími	citlivý	k2eAgFnPc7d3	nejcitlivější
analytickými	analytický	k2eAgFnPc7d1	analytická
technikami	technika	k1gFnPc7	technika
<g/>
,	,	kIx,	,
udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
0,01	[number]	k4	0,01
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
thalia	thalium	k1gNnSc2	thalium
přibližně	přibližně	k6eAd1	přibližně
300	[number]	k4	300
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
příměs	příměs	k1gFnSc4	příměs
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
sulfidických	sulfidický	k2eAgFnPc6d1	sulfidická
rudách	ruda	k1gFnPc6	ruda
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
a	a	k8xC	a
zinku	zinek	k1gInSc2	zinek
<g/>
.	.	kIx.	.
</s>
<s>
Odpad	odpad	k1gInSc1	odpad
po	po	k7c6	po
zpracování	zpracování	k1gNnSc6	zpracování
těchto	tento	k3xDgFnPc2	tento
rud	ruda	k1gFnPc2	ruda
slouží	sloužit	k5eAaImIp3nS	sloužit
pak	pak	k6eAd1	pak
jako	jako	k9	jako
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
čistého	čistý	k2eAgNnSc2d1	čisté
thallia	thallium	k1gNnSc2	thallium
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
lokalitách	lokalita	k1gFnPc6	lokalita
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
koncentraci	koncentrace	k1gFnSc6	koncentrace
v	v	k7c6	v
jílových	jílový	k2eAgInPc6d1	jílový
minerálech	minerál	k1gInPc6	minerál
a	a	k8xC	a
žule	žula	k1gFnSc6	žula
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
produkce	produkce	k1gFnSc2	produkce
se	se	k3xPyFc4	se
spotřebovává	spotřebovávat	k5eAaImIp3nS	spotřebovávat
v	v	k7c6	v
elektronice	elektronika	k1gFnSc6	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
prvkem	prvek	k1gInSc7	prvek
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
některých	některý	k3yIgInPc2	některý
polovodičů	polovodič	k1gInPc2	polovodič
<g/>
,	,	kIx,	,
např.	např.	kA	např.
tranzistorů	tranzistor	k1gInPc2	tranzistor
<g/>
,	,	kIx,	,
fotočlánků	fotočlánek	k1gInPc2	fotočlánek
s	s	k7c7	s
citlivostí	citlivost	k1gFnSc7	citlivost
v	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
a	a	k8xC	a
supravodičů	supravodič	k1gInPc2	supravodič
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
thallia	thallium	k1gNnSc2	thallium
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
toxické	toxický	k2eAgFnPc1d1	toxická
<g/>
,	,	kIx,	,
a	a	k8xC	a
používalo	používat	k5eAaImAgNnS	používat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
jako	jako	k8xS	jako
aktivní	aktivní	k2eAgFnSc1d1	aktivní
látka	látka	k1gFnSc1	látka
některých	některý	k3yIgInPc2	některý
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c6	na
hubení	hubení	k1gNnSc6	hubení
krys	krysa	k1gFnPc2	krysa
<g/>
,	,	kIx,	,
dalších	další	k2eAgMnPc2d1	další
hlodavců	hlodavec	k1gMnPc2	hlodavec
i	i	k8xC	i
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Thallium	Thallium	k1gNnSc1	Thallium
nachází	nacházet	k5eAaImIp3nS	nacházet
uplatnění	uplatnění	k1gNnSc4	uplatnění
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
speciálních	speciální	k2eAgNnPc2d1	speciální
skel	sklo	k1gNnPc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
slitiny	slitina	k1gFnPc4	slitina
se	s	k7c7	s
sírou	síra	k1gFnSc7	síra
<g/>
,	,	kIx,	,
selenem	selen	k1gInSc7	selen
a	a	k8xC	a
arzenem	arzen	k1gInSc7	arzen
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
velmi	velmi	k6eAd1	velmi
těžká	těžký	k2eAgNnPc1d1	těžké
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snadno	snadno	k6eAd1	snadno
tavitelná	tavitelný	k2eAgFnSc1d1	tavitelná
(	(	kIx(	(
<g/>
teplota	teplota	k1gFnSc1	teplota
tání	tání	k1gNnSc1	tání
125-150	[number]	k4	125-150
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
skla	sklo	k1gNnSc2	sklo
s	s	k7c7	s
mimořádně	mimořádně	k6eAd1	mimořádně
velkou	velký	k2eAgFnSc7d1	velká
odrazivostí	odrazivost	k1gFnSc7	odrazivost
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
lomem	lom	k1gInSc7	lom
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Přidává	přidávat	k5eAaImIp3nS	přidávat
se	se	k3xPyFc4	se
do	do	k7c2	do
lehkotavitelných	lehkotavitelný	k2eAgFnPc2d1	lehkotavitelný
slitin	slitina	k1gFnPc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Výbojky	výbojka	k1gFnPc1	výbojka
plněné	plněný	k2eAgFnPc1d1	plněná
parami	para	k1gFnPc7	para
thallia	thallium	k1gNnSc2	thallium
vydávají	vydávat	k5eAaImIp3nP	vydávat
zelené	zelený	k2eAgNnSc4d1	zelené
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc7	jeho
slabými	slabý	k2eAgInPc7d1	slabý
roztoky	roztok	k1gInPc7	roztok
se	se	k3xPyFc4	se
impregnuje	impregnovat	k5eAaBmIp3nS	impregnovat
dřevo	dřevo	k1gNnSc1	dřevo
a	a	k8xC	a
moří	mořit	k5eAaImIp3nS	mořit
obilí	obilí	k1gNnSc1	obilí
před	před	k7c7	před
výsevem	výsev	k1gInSc7	výsev
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
detekčních	detekční	k2eAgInPc2d1	detekční
členů	člen	k1gInPc2	člen
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
úrovně	úroveň	k1gFnSc2	úroveň
gama	gama	k1gNnSc1	gama
radiace	radiace	k1gFnSc2	radiace
v	v	k7c6	v
atomových	atomový	k2eAgFnPc6d1	atomová
elektrárnách	elektrárna	k1gFnPc6	elektrárna
a	a	k8xC	a
jaderném	jaderný	k2eAgInSc6d1	jaderný
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Radioizotop	radioizotop	k1gInSc1	radioizotop
201	[number]	k4	201
<g/>
Tl	Tl	k1gMnPc2	Tl
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
radionuklidovém	radionuklidový	k2eAgNnSc6d1	radionuklidové
vyšetření	vyšetření	k1gNnSc6	vyšetření
průtoku	průtok	k1gInSc6	průtok
krve	krev	k1gFnSc2	krev
koronárním	koronární	k2eAgNnSc7d1	koronární
řečištěm	řečiště	k1gNnSc7	řečiště
<g/>
.	.	kIx.	.
vodné	vodný	k2eAgFnSc2d1	vodná
roztoky	roztoka	k1gFnSc2	roztoka
solí	sůl	k1gFnPc2	sůl
thallia	thallium	k1gNnSc2	thallium
s	s	k7c7	s
organickými	organický	k2eAgFnPc7d1	organická
kyselinami	kyselina	k1gFnPc7	kyselina
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hustotu	hustota	k1gFnSc4	hustota
a	a	k8xC	a
užívají	užívat	k5eAaImIp3nP	užívat
se	se	k3xPyFc4	se
v	v	k7c4	v
mineralogii	mineralogie	k1gFnSc4	mineralogie
mj.	mj.	kA	mj.
pro	pro	k7c4	pro
orientační	orientační	k2eAgNnSc4d1	orientační
stanovení	stanovení	k1gNnSc4	stanovení
hustoty	hustota	k1gFnSc2	hustota
nerostů	nerost	k1gInPc2	nerost
(	(	kIx(	(
<g/>
Clericiho	Clerici	k1gMnSc2	Clerici
roztok	roztok	k1gInSc1	roztok
-	-	kIx~	-
mravenčan	mravenčan	k1gInSc1	mravenčan
a	a	k8xC	a
malonan	malonan	k1gInSc1	malonan
thallný	thallný	k2eAgInSc1d1	thallný
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
4.25	[number]	k4	4.25
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
při	při	k7c6	při
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
Thallium	Thallium	k1gNnSc1	Thallium
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
mimořádně	mimořádně	k6eAd1	mimořádně
toxický	toxický	k2eAgInSc4d1	toxický
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
státech	stát	k1gInPc6	stát
světa	svět	k1gInSc2	svět
proto	proto	k8xC	proto
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
používat	používat	k5eAaImF	používat
jej	on	k3xPp3gMnSc4	on
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
nástrah	nástraha	k1gFnPc2	nástraha
na	na	k7c4	na
krysy	krysa	k1gFnPc4	krysa
a	a	k8xC	a
mravence	mravenec	k1gMnPc4	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
Thallné	Thallný	k2eAgFnPc1d1	Thallný
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
prudkými	prudký	k2eAgInPc7d1	prudký
jedy	jed	k1gInPc7	jed
pro	pro	k7c4	pro
teplokrevná	teplokrevný	k2eAgNnPc4d1	teplokrevné
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Buněčný	buněčný	k2eAgInSc1d1	buněčný
jed	jed	k1gInSc1	jed
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
nervovou	nervový	k2eAgFnSc4d1	nervová
tkáň	tkáň	k1gFnSc4	tkáň
a	a	k8xC	a
vylučovací	vylučovací	k2eAgInPc4d1	vylučovací
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Otrava	otrava	k1gFnSc1	otrava
nastane	nastat	k5eAaPmIp3nS	nastat
po	po	k7c6	po
dávkách	dávka	k1gFnPc6	dávka
0,1	[number]	k4	0,1
<g/>
-	-	kIx~	-
<g/>
0,5	[number]	k4	0,5
g	g	kA	g
<g/>
;	;	kIx,	;
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
dávka	dávka	k1gFnSc1	dávka
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
g.	g.	k?	g.
Soli	sůl	k1gFnPc1	sůl
thallia	thallius	k1gMnSc2	thallius
jsou	být	k5eAaImIp3nP	být
pokládány	pokládán	k2eAgFnPc1d1	pokládána
za	za	k7c4	za
potenciálně	potenciálně	k6eAd1	potenciálně
karcinogenní	karcinogenní	k2eAgFnPc4d1	karcinogenní
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
pro	pro	k7c4	pro
obtížnou	obtížný	k2eAgFnSc4d1	obtížná
detekovatelnost	detekovatelnost	k1gFnSc4	detekovatelnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
thallium	thallium	k1gNnSc1	thallium
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
mezi	mezi	k7c7	mezi
traviči	travič	k1gMnPc7	travič
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
příznaky	příznak	k1gInPc7	příznak
otravy	otrava	k1gFnSc2	otrava
mj.	mj.	kA	mj.
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
vypadávání	vypadávání	k1gNnSc1	vypadávání
vlasů	vlas	k1gInPc2	vlas
a	a	k8xC	a
chlupů	chlup	k1gInPc2	chlup
<g/>
,	,	kIx,	,
bolest	bolest	k1gFnSc1	bolest
<g/>
/	/	kIx~	/
<g/>
znecitlivění	znecitlivění	k1gNnSc1	znecitlivění
nervů	nerv	k1gInPc2	nerv
končetin	končetina	k1gFnPc2	končetina
(	(	kIx(	(
<g/>
periferální	periferální	k2eAgFnSc1d1	periferální
neuropatie	neuropatie	k1gFnSc1	neuropatie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poškození	poškození	k1gNnSc1	poškození
trávicí	trávicí	k2eAgFnSc2d1	trávicí
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
krvavé	krvavý	k2eAgNnSc1d1	krvavé
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnPc1	bolest
břicha	břicho	k1gNnSc2	břicho
(	(	kIx(	(
<g/>
doprovázené	doprovázený	k2eAgNnSc1d1	doprovázené
i	i	k8xC	i
zácpou	zácpa	k1gFnSc7	zácpa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prsou	prsa	k1gNnPc2	prsa
a	a	k8xC	a
kloubů	kloub	k1gInPc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
otravu	otrava	k1gFnSc4	otrava
thalliem	thallium	k1gNnSc7	thallium
existuje	existovat	k5eAaImIp3nS	existovat
účinný	účinný	k2eAgInSc1d1	účinný
protijed	protijed	k1gInSc1	protijed
-	-	kIx~	-
barvivo	barvivo	k1gNnSc4	barvivo
berlínská	berlínský	k2eAgFnSc1d1	Berlínská
modř	modř	k1gFnSc1	modř
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
orálně	orálně	k6eAd1	orálně
<g/>
;	;	kIx,	;
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
tělem	tělo	k1gNnSc7	tělo
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
váže	vázat	k5eAaImIp3nS	vázat
molekuly	molekula	k1gFnSc2	molekula
Tl	Tl	k1gFnSc2	Tl
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
stolicí	stolice	k1gFnSc7	stolice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hutním	hutní	k2eAgInSc6d1	hutní
průmyslu	průmysl	k1gInSc6	průmysl
hrozí	hrozit	k5eAaImIp3nS	hrozit
expozice	expozice	k1gFnSc1	expozice
pracovníků	pracovník	k1gMnPc2	pracovník
thalliem	thallium	k1gNnSc7	thallium
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
polétavého	polétavý	k2eAgInSc2d1	polétavý
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
maximální	maximální	k2eAgFnSc1d1	maximální
přípustná	přípustný	k2eAgFnSc1d1	přípustná
dávka	dávka	k1gFnSc1	dávka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vystavena	vystaven	k2eAgFnSc1d1	vystavena
pokožka	pokožka	k1gFnSc1	pokožka
anebo	anebo	k8xC	anebo
dýchací	dýchací	k2eAgInPc1d1	dýchací
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
určeno	určit	k5eAaPmNgNnS	určit
množství	množství	k1gNnSc1	množství
0,1	[number]	k4	0,1
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
vzduchu	vzduch	k1gInSc2	vzduch
za	za	k7c4	za
pracovní	pracovní	k2eAgInPc4d1	pracovní
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
hodinovou	hodinový	k2eAgFnSc4d1	hodinová
<g/>
)	)	kIx)	)
směnu	směna	k1gFnSc4	směna
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
thallium	thallium	k1gNnSc4	thallium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
thallium	thallium	k1gNnSc4	thallium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
