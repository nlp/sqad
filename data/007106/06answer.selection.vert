<s>
Geometrická	geometrický	k2eAgFnSc1d1	geometrická
optika	optika	k1gFnSc1	optika
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
částí	část	k1gFnSc7	část
optiky	optika	k1gFnSc2	optika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
rozměry	rozměr	k1gInPc4	rozměr
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
něž	jenž	k3xRgMnPc4	jenž
nebo	nebo	k8xC	nebo
okolo	okolo	k7c2	okolo
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
světlo	světlo	k1gNnSc1	světlo
šíří	šíř	k1gFnPc2	šíř
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vlnová	vlnový	k2eAgFnSc1d1	vlnová
povaha	povaha	k1gFnSc1	povaha
světla	světlo	k1gNnSc2	světlo
jen	jen	k9	jen
slabě	slabě	k6eAd1	slabě
rozeznatelná	rozeznatelný	k2eAgFnSc1d1	rozeznatelná
<g/>
.	.	kIx.	.
</s>
