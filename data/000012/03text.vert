<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
na	na	k7c6	na
valném	valný	k2eAgNnSc6d1	Valné
shromáždění	shromáždění	k1gNnSc6	shromáždění
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
den	den	k1gInSc4	den
rodiny	rodina	k1gFnSc2	rodina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
International	International	k1gFnSc1	International
Day	Day	k1gFnSc2	Day
of	of	k?	of
Families	Families	k1gMnSc1	Families
<g/>
,	,	kIx,	,
United	United	k1gMnSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
Programme	Programme	k1gMnSc1	Programme
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Family	Famila	k1gFnPc4	Famila
<g/>
,	,	kIx,	,
Division	Division	k1gInSc4	Division
for	forum	k1gNnPc2	forum
Social	Social	k1gMnSc1	Social
Policy	Polica	k1gFnSc2	Polica
and	and	k?	and
Development	Development	k1gMnSc1	Development
<g/>
,	,	kIx,	,
UNDESA	UNDESA	kA	UNDESA
OSN	OSN	kA	OSN
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Kalendář	kalendář	k1gInSc1	kalendář
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
dní	den	k1gInPc2	den
a	a	k8xC	a
týdnů	týden	k1gInPc2	týden
</s>
