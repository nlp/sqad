<p>
<s>
Úval	úval	k1gInSc1	úval
je	být	k5eAaImIp3nS	být
sníženina	sníženina	k1gFnSc1	sníženina
obvykle	obvykle	k6eAd1	obvykle
protáhlého	protáhlý	k2eAgInSc2d1	protáhlý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
nebo	nebo	k8xC	nebo
obou	dva	k4xCgInPc6	dva
koncích	konec	k1gInPc6	konec
otevřená	otevřený	k2eAgFnSc1d1	otevřená
<g/>
.	.	kIx.	.
</s>
<s>
Dno	dno	k1gNnSc1	dno
úvalu	úval	k1gInSc2	úval
je	být	k5eAaImIp3nS	být
široké	široký	k2eAgNnSc1d1	široké
s	s	k7c7	s
převládající	převládající	k2eAgFnSc7d1	převládající
výškovou	výškový	k2eAgFnSc7d1	výšková
členitostí	členitost	k1gFnSc7	členitost
do	do	k7c2	do
75	[number]	k4	75
m	m	kA	m
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Hornomoravský	hornomoravský	k2eAgInSc1d1	hornomoravský
úval	úval	k1gInSc1	úval
<g/>
,	,	kIx,	,
Dolnomoravský	dolnomoravský	k2eAgInSc1d1	dolnomoravský
úval	úval	k1gInSc1	úval
<g/>
,	,	kIx,	,
Dyjsko-svratecký	dyjskovratecký	k2eAgInSc1d1	dyjsko-svratecký
úval	úval	k1gInSc1	úval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úval	úval	k1gInSc1	úval
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
konkávní	konkávní	k2eAgFnSc7d1	konkávní
(	(	kIx(	(
<g/>
vhloubené	vhloubený	k2eAgInPc1d1	vhloubený
<g/>
,	,	kIx,	,
vkleslé	vkleslý	k2eAgInPc1d1	vkleslý
<g/>
)	)	kIx)	)
tvary	tvar	k1gInPc1	tvar
georeliéfu	georeliéf	k1gInSc2	georeliéf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
Typy	typ	k1gInPc1	typ
a	a	k8xC	a
tvary	tvar	k1gInPc1	tvar
reliéfu	reliéf	k1gInSc2	reliéf
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
průhon	průhon	k1gInSc1	průhon
</s>
</p>
<p>
<s>
úvoz	úvoz	k1gInSc1	úvoz
</s>
</p>
