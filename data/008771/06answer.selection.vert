<s>
Megalodon	Megalodon	k1gMnSc1	Megalodon
(	(	kIx(	(
<g/>
Carcharocles	Carcharocles	k1gMnSc1	Carcharocles
megalodon	megalodon	k1gMnSc1	megalodon
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
velký	velký	k2eAgInSc4d1	velký
zub	zub	k1gInSc4	zub
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyhynulý	vyhynulý	k2eAgInSc4d1	vyhynulý
druh	druh	k1gInSc4	druh
žraloka	žralok	k1gMnSc2	žralok
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
před	před	k7c7	před
asi	asi	k9	asi
23	[number]	k4	23
až	až	k9	až
3,6	[number]	k4	3,6
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
během	během	k7c2	během
období	období	k1gNnSc2	období
od	od	k7c2	od
raného	raný	k2eAgInSc2d1	raný
miocénu	miocén	k1gInSc2	miocén
do	do	k7c2	do
pliocénu	pliocén	k1gInSc2	pliocén
<g/>
.	.	kIx.	.
</s>
