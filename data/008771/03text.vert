<p>
<s>
Megalodon	Megalodon	k1gMnSc1	Megalodon
(	(	kIx(	(
<g/>
Carcharocles	Carcharocles	k1gMnSc1	Carcharocles
megalodon	megalodon	k1gMnSc1	megalodon
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
velký	velký	k2eAgInSc4d1	velký
zub	zub	k1gInSc4	zub
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyhynulý	vyhynulý	k2eAgInSc4d1	vyhynulý
druh	druh	k1gInSc4	druh
žraloka	žralok	k1gMnSc2	žralok
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
před	před	k7c7	před
asi	asi	k9	asi
23	[number]	k4	23
až	až	k9	až
3,6	[number]	k4	3,6
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
během	během	k7c2	během
období	období	k1gNnSc2	období
od	od	k7c2	od
raného	raný	k2eAgInSc2d1	raný
miocénu	miocén	k1gInSc2	miocén
do	do	k7c2	do
pliocénu	pliocén	k1gInSc2	pliocén
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
systematice	systematika	k1gFnSc6	systematika
tohoto	tento	k3xDgMnSc2	tento
tvora	tvor	k1gMnSc2	tvor
probíhaly	probíhat	k5eAaImAgFnP	probíhat
debaty	debata	k1gFnPc1	debata
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jedněch	jeden	k4xCgInPc2	jeden
názorů	názor	k1gInPc2	názor
patřil	patřit	k5eAaImAgInS	patřit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
lamnovití	lamnovitý	k2eAgMnPc1d1	lamnovitý
(	(	kIx(	(
<g/>
Lamnidae	Lamnidae	k1gNnSc7	Lamnidae
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
blízkým	blízký	k2eAgMnPc3d1	blízký
příbuzným	příbuzný	k1gMnPc3	příbuzný
velkého	velký	k2eAgMnSc2d1	velký
žraloka	žralok	k1gMnSc2	žralok
bílého	bílý	k1gMnSc2	bílý
(	(	kIx(	(
<g/>
Carcharodon	Carcharodon	k1gMnSc1	Carcharodon
carcharias	carcharias	k1gMnSc1	carcharias
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
druhých	druhý	k4xOgInPc2	druhý
do	do	k7c2	do
vyhynulé	vyhynulý	k2eAgFnSc2d1	vyhynulá
čeledi	čeleď	k1gFnSc2	čeleď
Otodontidae	Otodontida	k1gInSc2	Otodontida
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
předpoklad	předpoklad	k1gInSc1	předpoklad
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
správný	správný	k2eAgInSc1d1	správný
<g/>
.	.	kIx.	.
</s>
<s>
Diskutováno	diskutován	k2eAgNnSc1d1	diskutováno
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
rodu	rod	k1gInSc6	rod
Carcharocles	Carcharocles	k1gMnSc1	Carcharocles
<g/>
,	,	kIx,	,
Megaselachus	Megaselachus	k1gMnSc1	Megaselachus
<g/>
,	,	kIx,	,
Otodus	Otodus	k1gMnSc1	Otodus
nebo	nebo	k8xC	nebo
Procarcharodon	Procarcharodon	k1gMnSc1	Procarcharodon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Megalodon	Megalodon	k1gInSc1	Megalodon
mohl	moct	k5eAaImAgInS	moct
vzhledem	vzhled	k1gInSc7	vzhled
připomínat	připomínat	k5eAaImF	připomínat
mohutnějšího	mohutný	k2eAgMnSc4d2	mohutnější
žraloka	žralok	k1gMnSc4	žralok
bílého	bílý	k2eAgMnSc4d1	bílý
či	či	k8xC	či
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
podobat	podobat	k5eAaImF	podobat
žraloku	žralok	k1gMnSc3	žralok
velikému	veliký	k2eAgInSc3d1	veliký
(	(	kIx(	(
<g/>
Cetorhinus	Cetorhinus	k1gMnSc1	Cetorhinus
maximus	maximus	k1gMnSc1	maximus
<g/>
)	)	kIx)	)
či	či	k8xC	či
písečnému	písečný	k2eAgMnSc3d1	písečný
(	(	kIx(	(
<g/>
Carcharias	Carcharias	k1gMnSc1	Carcharias
taurus	taurus	k1gMnSc1	taurus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fosilní	fosilní	k2eAgInPc1d1	fosilní
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgMnSc1	tento
žralok	žralok	k1gMnSc1	žralok
mohl	moct	k5eAaImAgMnS	moct
měřit	měřit	k5eAaImF	měřit
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
až	až	k9	až
18	[number]	k4	18
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
velké	velký	k2eAgFnPc1d1	velká
čelisti	čelist	k1gFnPc1	čelist
<g/>
,	,	kIx,	,
vybavené	vybavený	k2eAgInPc1d1	vybavený
silnými	silný	k2eAgInPc7d1	silný
a	a	k8xC	a
robustními	robustní	k2eAgInPc7d1	robustní
zuby	zub	k1gInPc7	zub
sloužícími	sloužící	k2eAgInPc7d1	sloužící
k	k	k7c3	k
zabíjení	zabíjení	k1gNnSc3	zabíjení
a	a	k8xC	a
trhání	trhání	k1gNnSc3	trhání
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgFnP	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
při	při	k7c6	při
skousnutí	skousnutí	k1gNnSc6	skousnutí
sílu	síl	k1gInSc2	síl
108	[number]	k4	108
500	[number]	k4	500
až	až	k9	až
182	[number]	k4	182
200	[number]	k4	200
newtonů	newton	k1gInPc2	newton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Megalodoni	Megalodon	k1gMnPc1	Megalodon
měli	mít	k5eAaImAgMnP	mít
asi	asi	k9	asi
silný	silný	k2eAgInSc4d1	silný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
strukturu	struktura	k1gFnSc4	struktura
mořských	mořský	k2eAgNnPc2d1	mořské
společenstev	společenstvo	k1gNnPc2	společenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
fosilií	fosilie	k1gFnPc2	fosilie
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
kosmopolita	kosmopolit	k1gMnSc4	kosmopolit
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
druh	druh	k1gInSc1	druh
žijící	žijící	k2eAgInSc1d1	žijící
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
oceánech	oceán	k1gInPc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
se	se	k3xPyFc4	se
zaměřoval	zaměřovat	k5eAaImAgInS	zaměřovat
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byly	být	k5eAaImAgFnP	být
velryby	velryba	k1gFnPc1	velryba
<g/>
,	,	kIx,	,
ploutvonožci	ploutvonožec	k1gMnPc1	ploutvonožec
a	a	k8xC	a
velké	velký	k2eAgInPc1d1	velký
druhy	druh	k1gInPc1	druh
želv	želva	k1gFnPc2	želva
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
žraloka	žralok	k1gMnSc2	žralok
bílého	bílý	k1gMnSc2	bílý
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
útočí	útočit	k5eAaImIp3nS	útočit
zespodu	zespodu	k6eAd1	zespodu
<g/>
,	,	kIx,	,
megalodon	megalodon	k1gInSc1	megalodon
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
využíval	využívat	k5eAaPmAgInS	využívat
své	svůj	k3xOyFgFnPc4	svůj
silné	silný	k2eAgFnPc4d1	silná
čelisti	čelist	k1gFnPc4	čelist
k	k	k7c3	k
prokousnutí	prokousnutí	k1gNnSc3	prokousnutí
hrudní	hrudní	k2eAgFnSc2d1	hrudní
dutiny	dutina	k1gFnSc2	dutina
oběti	oběť	k1gFnSc2	oběť
a	a	k8xC	a
poškození	poškození	k1gNnSc2	poškození
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
orgánů	orgán	k1gInPc2	orgán
−	−	k?	−
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Megalodon	Megalodon	k1gInSc1	Megalodon
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
kompetičním	kompetiční	k2eAgInSc6d1	kompetiční
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
kytovci	kytovec	k1gMnPc7	kytovec
lovícími	lovící	k2eAgMnPc7d1	lovící
jiné	jiný	k2eAgFnSc2d1	jiná
velryby	velryba	k1gFnSc2	velryba
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
možná	možná	k9	možná
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vyhynutí	vyhynutí	k1gNnSc3	vyhynutí
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žil	žít	k5eAaImAgMnS	žít
primárně	primárně	k6eAd1	primárně
v	v	k7c6	v
teplých	teplý	k2eAgFnPc6d1	teplá
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
vyhynutí	vyhynutí	k1gNnSc4	vyhynutí
mohl	moct	k5eAaImAgInS	moct
i	i	k9	i
nástup	nástup	k1gInSc1	nástup
ledové	ledový	k2eAgFnSc2d1	ledová
doby	doba	k1gFnSc2	doba
či	či	k8xC	či
ochlazení	ochlazení	k1gNnSc3	ochlazení
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
hladina	hladina	k1gFnSc1	hladina
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
žraloci	žralok	k1gMnPc1	žralok
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c6	o
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
jejich	jejich	k3xOp3gNnPc4	jejich
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
počet	počet	k1gInSc1	počet
druhů	druh	k1gInPc2	druh
kosticovců	kosticovec	k1gInPc2	kosticovec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
začali	začít	k5eAaPmAgMnP	začít
přesouvat	přesouvat	k5eAaImF	přesouvat
do	do	k7c2	do
polárních	polární	k2eAgFnPc2d1	polární
oblastí	oblast	k1gFnPc2	oblast
<g/>
;	;	kIx,	;
tím	ten	k3xDgNnSc7	ten
megalodon	megalodon	k1gInSc1	megalodon
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
hlavní	hlavní	k2eAgInSc4d1	hlavní
zdroj	zdroj	k1gInSc4	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Vyhynutí	vyhynutí	k1gNnSc4	vyhynutí
tohoto	tento	k3xDgMnSc2	tento
velkého	velký	k2eAgMnSc2d1	velký
žraloka	žralok	k1gMnSc2	žralok
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
ostatní	ostatní	k2eAgMnPc4d1	ostatní
mořské	mořský	k2eAgMnPc4d1	mořský
tvory	tvor	k1gMnPc4	tvor
<g/>
;	;	kIx,	;
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc1	velikost
kosticovců	kosticovec	k1gMnPc2	kosticovec
výrazně	výrazně	k6eAd1	výrazně
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pojmenování	pojmenování	k1gNnPc1	pojmenování
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
renesance	renesance	k1gFnSc2	renesance
byly	být	k5eAaImAgInP	být
obrovské	obrovský	k2eAgInPc1d1	obrovský
trojúhelníkové	trojúhelníkový	k2eAgInPc1d1	trojúhelníkový
fosilní	fosilní	k2eAgInPc1d1	fosilní
zuby	zub	k1gInPc1	zub
<g/>
,	,	kIx,	,
nalézané	nalézaný	k2eAgFnPc1d1	nalézaná
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
skalních	skalní	k2eAgInPc6d1	skalní
útvarech	útvar	k1gInPc6	útvar
<g/>
,	,	kIx,	,
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
zkamenělé	zkamenělý	k2eAgInPc4d1	zkamenělý
jazyky	jazyk	k1gInPc4	jazyk
draků	drak	k1gMnPc2	drak
a	a	k8xC	a
hadů	had	k1gMnPc2	had
a	a	k8xC	a
nazývány	nazýván	k2eAgMnPc4d1	nazýván
jako	jako	k8xC	jako
glossopetrae	glossopetra	k1gMnPc4	glossopetra
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1667	[number]	k4	1667
tento	tento	k3xDgMnSc1	tento
názor	názor	k1gInSc4	názor
vyvrátil	vyvrátit	k5eAaPmAgMnS	vyvrátit
dánský	dánský	k2eAgMnSc1d1	dánský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Nicolas	Nicolas	k1gMnSc1	Nicolas
Steno	Steno	k6eAd1	Steno
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
správně	správně	k6eAd1	správně
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k8xS	jako
zuby	zub	k1gInPc4	zub
žraloka	žralok	k1gMnSc2	žralok
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
rovněž	rovněž	k9	rovněž
realistický	realistický	k2eAgInSc1d1	realistický
nákres	nákres	k1gInSc1	nákres
hlavy	hlava	k1gFnSc2	hlava
žraloka	žralok	k1gMnSc2	žralok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
takové	takový	k3xDgInPc4	takový
zuby	zub	k1gInPc4	zub
mohl	moct	k5eAaImAgInS	moct
vlastnit	vlastnit	k5eAaImF	vlastnit
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
poznatky	poznatek	k1gInPc4	poznatek
popsal	popsat	k5eAaPmAgMnS	popsat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
monografii	monografie	k1gFnSc6	monografie
De	De	k?	De
glossopetris	glossopetris	k1gFnPc2	glossopetris
dissertatio	dissertatio	k1gMnSc1	dissertatio
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
i	i	k9	i
ilustraci	ilustrace	k1gFnSc4	ilustrace
tohoto	tento	k3xDgInSc2	tento
zubu	zub	k1gInSc2	zub
<g/>
.	.	kIx.	.
<g/>
Poprvé	poprvé	k6eAd1	poprvé
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
popsal	popsat	k5eAaPmAgMnS	popsat
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Louis	Louis	k1gMnSc1	Louis
Agassiz	Agassiz	k1gMnSc1	Agassiz
pod	pod	k7c7	pod
vědeckým	vědecký	k2eAgNnSc7d1	vědecké
jménem	jméno	k1gNnSc7	jméno
Carcharodon	Carcharodon	k1gMnSc1	Carcharodon
megalodon	megalodon	k1gMnSc1	megalodon
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Carcharodon	Carcharodon	k1gInSc1	Carcharodon
jej	on	k3xPp3gMnSc4	on
zařadil	zařadit	k5eAaPmAgInS	zařadit
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
podobnosti	podobnost	k1gFnSc3	podobnost
zubů	zub	k1gInPc2	zub
se	s	k7c7	s
zuby	zub	k1gInPc7	zub
žraloka	žralok	k1gMnSc2	žralok
bílého	bílý	k1gMnSc2	bílý
(	(	kIx(	(
<g/>
Carcharodon	Carcharodon	k1gMnSc1	Carcharodon
carcharias	carcharias	k1gMnSc1	carcharias
<g/>
)	)	kIx)	)
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
popis	popis	k1gInSc4	popis
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
zbytcích	zbytek	k1gInPc6	zbytek
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
Recherches	Recherches	k1gMnSc1	Recherches
sur	sur	k?	sur
les	les	k1gInSc1	les
poissons	poissonsa	k1gFnPc2	poissonsa
fossiles	fossiles	k1gInSc4	fossiles
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
paleontolog	paleontolog	k1gMnSc1	paleontolog
Edward	Edward	k1gMnSc1	Edward
Charlesworth	Charlesworth	k1gMnSc1	Charlesworth
však	však	k9	však
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
již	již	k6eAd1	již
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
určil	určit	k5eAaPmAgInS	určit
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
název	název	k1gInSc1	název
Carcharias	Carchariasa	k1gFnPc2	Carchariasa
megalodon	megalodona	k1gFnPc2	megalodona
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
citoval	citovat	k5eAaBmAgMnS	citovat
Agassize	Agassize	k1gFnSc1	Agassize
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Agassiz	Agassiz	k1gInSc1	Agassiz
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
megalodona	megalodon	k1gMnSc2	megalodon
popsal	popsat	k5eAaPmAgMnS	popsat
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1843	[number]	k4	1843
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
předchozích	předchozí	k2eAgInPc6d1	předchozí
článcích	článek	k1gInPc6	článek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
<g/>
.	.	kIx.	.
<g/>
Název	název	k1gInSc1	název
megalodon	megalodon	k1gInSc1	megalodon
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
velký	velký	k2eAgInSc4d1	velký
zub	zub	k1gInSc4	zub
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc4	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
starořeckých	starořecký	k2eAgNnPc2d1	starořecké
slov	slovo	k1gNnPc2	slovo
<g/>
:	:	kIx,	:
μ	μ	k?	μ
(	(	kIx(	(
<g/>
mégas	mégas	k1gMnSc1	mégas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgNnSc4d1	znamenající
velký	velký	k2eAgInSc4d1	velký
či	či	k8xC	či
mocný	mocný	k2eAgInSc4d1	mocný
<g/>
,	,	kIx,	,
a	a	k8xC	a
ὀ	ὀ	k?	ὀ
(	(	kIx(	(
<g/>
odoús	odoús	k1gInSc1	odoús
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
zub	zub	k1gInSc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojmenování	pojmenování	k1gNnSc1	pojmenování
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
pouze	pouze	k6eAd1	pouze
neformální	formální	k2eNgFnPc1d1	neformální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Evoluce	evoluce	k1gFnSc2	evoluce
===	===	k?	===
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
megalodona	megalodona	k1gFnSc1	megalodona
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
pozdního	pozdní	k2eAgInSc2d1	pozdní
oligocénu	oligocén	k1gInSc2	oligocén
před	před	k7c7	před
zhruba	zhruba	k6eAd1	zhruba
28	[number]	k4	28
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgInPc4d1	další
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
druh	druh	k1gInSc1	druh
objevil	objevit	k5eAaPmAgInS	objevit
například	například	k6eAd1	například
před	před	k7c7	před
16	[number]	k4	16
nebo	nebo	k8xC	nebo
23	[number]	k4	23
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Megalodon	Megalodon	k1gMnSc1	Megalodon
údajně	údajně	k6eAd1	údajně
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
během	během	k7c2	během
konce	konec	k1gInSc2	konec
pliocénu	pliocén	k1gInSc2	pliocén
před	před	k7c7	před
asi	asi	k9	asi
2,6	[number]	k4	2,6
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc4	výzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
zasadil	zasadit	k5eAaPmAgInS	zasadit
vyhynutí	vyhynutí	k1gNnSc4	vyhynutí
megalodona	megalodon	k1gMnSc2	megalodon
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
3,6	[number]	k4	3,6
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
prý	prý	k9	prý
i	i	k9	i
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
zubech	zub	k1gInPc6	zub
z	z	k7c2	z
následujícího	následující	k2eAgInSc2d1	následující
období	období	k1gNnSc6	období
pleistocén	pleistocén	k1gInSc4	pleistocén
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
pochybné	pochybný	k2eAgNnSc4d1	pochybné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Megalodon	Megalodon	k1gInSc1	Megalodon
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
zástupce	zástupce	k1gMnPc4	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
Otodontidae	Otodontida	k1gInSc2	Otodontida
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
tvoří	tvořit	k5eAaImIp3nS	tvořit
část	část	k1gFnSc1	část
rodu	rod	k1gInSc2	rod
Carcharocles	Carcharoclesa	k1gFnPc2	Carcharoclesa
<g/>
;	;	kIx,	;
na	na	k7c6	na
základě	základ	k1gInSc6	základ
předchozí	předchozí	k2eAgFnSc2d1	předchozí
systematiky	systematika	k1gFnSc2	systematika
byl	být	k5eAaImAgInS	být
nicméně	nicméně	k8xC	nicméně
řazen	řadit	k5eAaImNgInS	řadit
mezi	mezi	k7c4	mezi
lamnovité	lamnovitý	k2eAgNnSc4d1	lamnovitý
(	(	kIx(	(
<g/>
Lamnidae	Lamnidae	k1gNnSc4	Lamnidae
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
zástupce	zástupce	k1gMnSc1	zástupce
rodu	rod	k1gInSc2	rod
Carcharodon	Carcharodon	k1gMnSc1	Carcharodon
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podobnosti	podobnost	k1gFnSc2	podobnost
zubů	zub	k1gInPc2	zub
se	s	k7c7	s
žralokem	žralok	k1gMnSc7	žralok
bílým	bílý	k1gMnSc7	bílý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
pravděpodobnější	pravděpodobný	k2eAgMnSc1d2	pravděpodobnější
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
paryby	paryba	k1gFnPc1	paryba
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
konvergenčně	konvergenčně	k6eAd1	konvergenčně
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pod	pod	k7c7	pod
stejnými	stejný	k2eAgInPc7d1	stejný
selekčními	selekční	k2eAgInPc7d1	selekční
tlaky	tlak	k1gInPc7	tlak
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
podobný	podobný	k2eAgInSc1d1	podobný
vzhled	vzhled	k1gInSc1	vzhled
obou	dva	k4xCgNnPc2	dva
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
společně	společně	k6eAd1	společně
nemusela	muset	k5eNaImAgFnS	muset
býti	být	k5eAaImF	být
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
<g/>
.	.	kIx.	.
</s>
<s>
Žralok	žralok	k1gMnSc1	žralok
bílý	bílý	k1gMnSc1	bílý
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
považován	považován	k2eAgInSc1d1	považován
spíše	spíše	k9	spíše
za	za	k7c4	za
příbuzného	příbuzný	k2eAgMnSc4d1	příbuzný
žraloka	žralok	k1gMnSc4	žralok
druhu	druh	k1gInSc2	druh
Isurus	Isurus	k1gInSc1	Isurus
hastalis	hastalis	k1gFnSc1	hastalis
(	(	kIx(	(
<g/>
zuby	zub	k1gInPc7	zub
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
jsou	být	k5eAaImIp3nP	být
podobnější	podobný	k2eAgInPc1d2	podobnější
<g/>
,	,	kIx,	,
megalodon	megalodon	k1gInSc1	megalodon
měl	mít	k5eAaImAgInS	mít
jemnější	jemný	k2eAgNnSc4d2	jemnější
vroubkování	vroubkování	k1gNnSc4	vroubkování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společný	společný	k2eAgInSc1d1	společný
předek	předek	k1gInSc1	předek
žraloka	žralok	k1gMnSc2	žralok
bílého	bílý	k1gMnSc2	bílý
a	a	k8xC	a
rodu	rod	k1gInSc2	rod
Isurus	Isurus	k1gMnSc1	Isurus
žil	žít	k5eAaImAgMnS	žít
před	před	k7c7	před
asi	asi	k9	asi
4	[number]	k4	4
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
dle	dle	k7c2	dle
teorie	teorie	k1gFnSc2	teorie
příbuznosti	příbuznost	k1gFnSc2	příbuznost
megalodona	megalodon	k1gMnSc2	megalodon
a	a	k8xC	a
žraloka	žralok	k1gMnSc2	žralok
bílého	bílý	k1gMnSc2	bílý
jsou	být	k5eAaImIp3nP	být
prý	prý	k9	prý
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
zubech	zub	k1gInPc6	zub
nepatrné	nepatrný	k2eAgFnPc1d1	nepatrná
a	a	k8xC	a
nejasné	jasný	k2eNgFnPc1d1	nejasná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
Carcharocles	Carcharocles	k1gInSc1	Carcharocles
je	být	k5eAaImIp3nS	být
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
čtyřmi	čtyři	k4xCgInPc7	čtyři
druhy	druh	k1gInPc7	druh
<g/>
:	:	kIx,	:
Carcharocles	Carcharocles	k1gMnSc1	Carcharocles
auriculatus	auriculatus	k1gMnSc1	auriculatus
<g/>
,	,	kIx,	,
Carcharocles	Carcharocles	k1gMnSc1	Carcharocles
angustidens	angustidens	k1gInSc1	angustidens
<g/>
,	,	kIx,	,
Carcharocles	Carcharocles	k1gInSc1	Carcharocles
chubutensis	chubutensis	k1gFnSc2	chubutensis
a	a	k8xC	a
Carcharocles	Carcharocles	k1gMnSc1	Carcharocles
megalodon	megalodon	k1gMnSc1	megalodon
<g/>
.	.	kIx.	.
</s>
<s>
Určili	určit	k5eAaPmAgMnP	určit
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
výzkumníci	výzkumník	k1gMnPc1	výzkumník
D.	D.	kA	D.
S.	S.	kA	S.
Jordan	Jordan	k1gMnSc1	Jordan
a	a	k8xC	a
H.	H.	kA	H.
Hannibal	Hannibal	k1gInSc1	Hannibal
a	a	k8xC	a
prvotně	prvotně	k6eAd1	prvotně
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
pouze	pouze	k6eAd1	pouze
druh	druh	k1gInSc1	druh
C.	C.	kA	C.
auriculatus	auriculatus	k1gInSc1	auriculatus
<g/>
,	,	kIx,	,
megalodon	megalodon	k1gInSc1	megalodon
sem	sem	k6eAd1	sem
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
až	až	k9	až
v	v	k7c4	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
ichtyolog	ichtyolog	k1gMnSc1	ichtyolog
Edgard	Edgard	k1gMnSc1	Edgard
Casier	Casier	k1gMnSc1	Casier
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
rod	rod	k1gInSc4	rod
Procarcharodon	Procarcharodona	k1gFnPc2	Procarcharodona
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
zařadil	zařadit	k5eAaPmAgMnS	zařadit
tyto	tento	k3xDgInPc4	tento
čtyři	čtyři	k4xCgInPc4	čtyři
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
nepříbuzné	příbuzný	k2eNgNnSc4d1	nepříbuzné
se	s	k7c7	s
žralokem	žralok	k1gMnSc7	žralok
bílým	bílý	k1gMnSc7	bílý
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
rod	rod	k1gInSc1	rod
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
mladším	mladý	k2eAgNnSc7d2	mladší
synonymem	synonymum	k1gNnSc7	synonymum
rodu	rod	k1gInSc2	rod
Carcharocles	Carcharoclesa	k1gFnPc2	Carcharoclesa
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
rod	rod	k1gInSc1	rod
Palaeocarcharodon	Palaeocarcharodon	k1gInSc1	Palaeocarcharodon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
začátkem	začátkem	k7c2	začátkem
této	tento	k3xDgFnSc2	tento
vývojové	vývojový	k2eAgFnSc2d1	vývojová
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
teorie	teorie	k1gFnSc2	teorie
o	o	k7c6	o
příbuznosti	příbuznost	k1gFnSc6	příbuznost
žraloka	žralok	k1gMnSc2	žralok
bílého	bílý	k1gMnSc2	bílý
a	a	k8xC	a
megalodona	megalodon	k1gMnSc2	megalodon
potom	potom	k6eAd1	potom
považují	považovat	k5eAaImIp3nP	považovat
Paleocarcharodon	Paleocarcharodon	k1gInSc4	Paleocarcharodon
za	za	k7c4	za
posledního	poslední	k2eAgMnSc4d1	poslední
společného	společný	k2eAgMnSc4d1	společný
předka	předek	k1gMnSc4	předek
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
odpůrců	odpůrce	k1gMnPc2	odpůrce
teorie	teorie	k1gFnSc2	teorie
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
slepou	slepý	k2eAgFnSc4d1	slepá
vývojovou	vývojový	k2eAgFnSc4d1	vývojová
větev	větev	k1gFnSc4	větev
<g/>
.	.	kIx.	.
<g/>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
další	další	k2eAgInSc1d1	další
model	model	k1gInSc1	model
evoluce	evoluce	k1gFnSc2	evoluce
megalodona	megalodon	k1gMnSc2	megalodon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
také	také	k9	také
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Casier	Casier	k1gInSc4	Casier
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
je	být	k5eAaImIp3nS	být
předkem	předek	k1gInSc7	předek
rodu	rod	k1gInSc2	rod
Carcharocles	Carcharoclesa	k1gFnPc2	Carcharoclesa
žralok	žralok	k1gMnSc1	žralok
Otodus	Otodus	k1gMnSc1	Otodus
obliquus	obliquus	k1gMnSc1	obliquus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
během	během	k7c2	během
paleocénu	paleocén	k1gInSc2	paleocén
až	až	k8xS	až
miocénu	miocén	k1gInSc2	miocén
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
na	na	k7c4	na
druh	druh	k1gInSc4	druh
Otodus	Otodus	k1gMnSc1	Otodus
aksuaticus	aksuaticus	k1gMnSc1	aksuaticus
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
skrze	skrze	k?	skrze
druhy	druh	k1gInPc4	druh
Carcharocles	Carcharocles	k1gMnSc1	Carcharocles
auriculatus	auriculatus	k1gMnSc1	auriculatus
<g/>
,	,	kIx,	,
Carcharocles	Carcharocles	k1gMnSc1	Carcharocles
angustidens	angustidens	k1gInSc1	angustidens
a	a	k8xC	a
Carcharocles	Carcharocles	k1gInSc1	Carcharocles
chubutensis	chubutensis	k1gFnPc2	chubutensis
až	až	k9	až
po	po	k7c4	po
megalodona	megalodon	k1gMnSc4	megalodon
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
této	tento	k3xDgFnSc2	tento
linie	linie	k1gFnSc2	linie
je	být	k5eAaImIp3nS	být
charakterizován	charakterizován	k2eAgInSc1d1	charakterizován
postupným	postupný	k2eAgNnSc7d1	postupné
narůstáním	narůstání	k1gNnSc7	narůstání
vroubkování	vroubkování	k1gNnSc2	vroubkování
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
rozšiřováním	rozšiřování	k1gNnSc7	rozšiřování
jejich	jejich	k3xOp3gFnSc2	jejich
korunky	korunka	k1gFnSc2	korunka
<g/>
,	,	kIx,	,
nabýváním	nabývání	k1gNnSc7	nabývání
trojúhelníkového	trojúhelníkový	k2eAgInSc2d1	trojúhelníkový
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
ztrátou	ztráta	k1gFnSc7	ztráta
bočních	boční	k2eAgInPc2d1	boční
hrotů	hrot	k1gInPc2	hrot
<g/>
.	.	kIx.	.
</s>
<s>
Předky	předek	k1gInPc1	předek
rodu	rod	k1gInSc2	rod
Otodus	Otodus	k1gInSc4	Otodus
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
žraloci	žralok	k1gMnPc1	žralok
Cretolamna	Cretolamno	k1gNnSc2	Cretolamno
z	z	k7c2	z
období	období	k1gNnSc2	období
křídy	křída	k1gFnSc2	křída
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgInSc1d1	další
model	model	k1gInSc1	model
evoluce	evoluce	k1gFnSc2	evoluce
rodu	rod	k1gInSc2	rod
Carcharocles	Carcharocles	k1gInSc1	Carcharocles
představil	představit	k5eAaPmAgInS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
paleontolog	paleontolog	k1gMnSc1	paleontolog
Michael	Michael	k1gMnSc1	Michael
Benton	Benton	k1gInSc4	Benton
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
Carcharocles	Carcharoclesa	k1gFnPc2	Carcharoclesa
auriculatus	auriculatus	k1gInSc1	auriculatus
<g/>
,	,	kIx,	,
Carcharocles	Carcharocles	k1gInSc1	Carcharocles
angustidens	angustidensa	k1gFnPc2	angustidensa
a	a	k8xC	a
Carcharocles	Carcharoclesa	k1gFnPc2	Carcharoclesa
chubutensis	chubutensis	k1gFnPc2	chubutensis
jsou	být	k5eAaImIp3nP	být
dle	dle	k7c2	dle
něj	on	k3xPp3gNnSc2	on
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jediným	jediný	k2eAgInSc7d1	jediný
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
dalších	další	k2eAgNnPc2d1	další
studií	studio	k1gNnPc2	studio
by	by	kYmCp3nS	by
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
měl	mít	k5eAaImAgInS	mít
patřit	patřit	k5eAaImF	patřit
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Otodus	Otodus	k1gInSc4	Otodus
<g/>
,	,	kIx,	,
megalodon	megalodon	k1gInSc4	megalodon
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
byl	být	k5eAaImAgInS	být
jediným	jediný	k2eAgMnSc7d1	jediný
zástupcem	zástupce	k1gMnSc7	zástupce
rodu	rod	k1gInSc2	rod
Carcharocles	Carcharoclesa	k1gFnPc2	Carcharoclesa
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
Carcharocles	Carcharoclesa	k1gFnPc2	Carcharoclesa
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
neplatným	platný	k2eNgInSc7d1	neplatný
taxonem	taxon	k1gInSc7	taxon
a	a	k8xC	a
megalodon	megalodon	k1gNnSc1	megalodon
může	moct	k5eAaImIp3nS	moct
patřit	patřit	k5eAaImF	patřit
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Otodus	Otodus	k1gInSc4	Otodus
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Otodus	Otodus	k1gMnSc1	Otodus
megalodon	megalodon	k1gMnSc1	megalodon
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
Henriho	Henri	k1gMnSc2	Henri
Cappety	Cappeta	k1gFnSc2	Cappeta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
zkoumající	zkoumající	k2eAgMnPc4d1	zkoumající
paleogénní	paleogénní	k2eAgMnPc4d1	paleogénní
žraloky	žralok	k1gMnPc4	žralok
<g/>
,	,	kIx,	,
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
podrod	podrod	k1gInSc4	podrod
Megaselachus	Megaselachus	k1gInSc4	Megaselachus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
byl	být	k5eAaImAgInS	být
megalodon	megalodon	k1gInSc1	megalodon
zařazen	zařadit	k5eAaPmNgInS	zařadit
jako	jako	k8xC	jako
Otodus	Otodus	k1gInSc1	Otodus
(	(	kIx(	(
<g/>
Megaselachus	Megaselachus	k1gInSc1	Megaselachus
<g/>
)	)	kIx)	)
megalodon	megalodon	k1gInSc1	megalodon
společně	společně	k6eAd1	společně
s	s	k7c7	s
druhem	druh	k1gInSc7	druh
O.	O.	kA	O.
(	(	kIx(	(
<g/>
M.	M.	kA	M.
<g/>
)	)	kIx)	)
chubutensis	chubutensis	k1gInSc1	chubutensis
<g/>
.	.	kIx.	.
</s>
<s>
Revize	revize	k1gFnSc1	revize
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
poté	poté	k6eAd1	poté
povýšila	povýšit	k5eAaPmAgFnS	povýšit
Megaselachus	Megaselachus	k1gInSc4	Megaselachus
na	na	k7c4	na
rod	rod	k1gInSc4	rod
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nS	by
megalodon	megalodon	k1gInSc1	megalodon
patřil	patřit	k5eAaImAgInS	patřit
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
jako	jako	k8xC	jako
Megaselachus	Megaselachus	k1gMnSc1	Megaselachus
megalodon	megalodon	k1gMnSc1	megalodon
<g/>
.	.	kIx.	.
<g/>
Objev	objev	k1gInSc1	objev
fosilií	fosilie	k1gFnSc7	fosilie
přiřazených	přiřazený	k2eAgMnPc2d1	přiřazený
rodu	rod	k1gInSc2	rod
Megalolamna	Megalolamno	k1gNnSc2	Megalolamno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
zkoumáním	zkoumání	k1gNnSc7	zkoumání
rodu	rod	k1gInSc2	rod
Otodus	Otodus	k1gInSc4	Otodus
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
parafyletický	parafyletický	k2eAgMnSc1d1	parafyletický
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
posledního	poslední	k2eAgMnSc4d1	poslední
společného	společný	k2eAgMnSc4d1	společný
předka	předek	k1gMnSc4	předek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
všechny	všechen	k3xTgMnPc4	všechen
jeho	jeho	k3xOp3gMnPc4	jeho
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
zařazen	zařazen	k2eAgInSc1d1	zařazen
současný	současný	k2eAgInSc1d1	současný
rod	rod	k1gInSc1	rod
Carcharocles	Carcharoclesa	k1gFnPc2	Carcharoclesa
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
by	by	kYmCp3nS	by
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
celá	celý	k2eAgFnSc1d1	celá
fylogenetická	fylogenetický	k2eAgFnSc1d1	fylogenetická
linie	linie	k1gFnSc1	linie
(	(	kIx(	(
<g/>
monofyletismus	monofyletismus	k1gInSc1	monofyletismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Megalolamna	Megalolamn	k1gInSc2	Megalolamn
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
stal	stát	k5eAaPmAgMnS	stát
sesterskou	sesterský	k2eAgFnSc7d1	sesterská
linií	linie	k1gFnSc7	linie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biologie	biologie	k1gFnSc2	biologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vzhled	vzhled	k1gInSc4	vzhled
===	===	k?	===
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
možných	možný	k2eAgFnPc2d1	možná
teorií	teorie	k1gFnPc2	teorie
popisujících	popisující	k2eAgFnPc2d1	popisující
vzhled	vzhled	k1gInSc4	vzhled
megalodona	megalodon	k1gMnSc2	megalodon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
jedné	jeden	k4xCgFnSc2	jeden
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
mohutného	mohutný	k2eAgMnSc4d1	mohutný
žraloka	žralok	k1gMnSc4	žralok
podobného	podobný	k2eAgMnSc4d1	podobný
žralokovi	žralok	k1gMnSc3	žralok
bílému	bílý	k1gMnSc3	bílý
(	(	kIx(	(
<g/>
Carcharodon	Carcharodon	k1gMnSc1	Carcharodon
carcharias	carcharias	k1gMnSc1	carcharias
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
většími	veliký	k2eAgFnPc7d2	veliký
a	a	k8xC	a
širšími	široký	k2eAgFnPc7d2	širší
čelistmi	čelist	k1gFnPc7	čelist
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Ploutve	ploutev	k1gFnPc1	ploutev
byly	být	k5eAaImAgFnP	být
dle	dle	k7c2	dle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
u	u	k7c2	u
megalodona	megalodona	k1gFnSc1	megalodona
podobné	podobný	k2eAgFnPc1d1	podobná
jako	jako	k9	jako
u	u	k7c2	u
žraloka	žralok	k1gMnSc2	žralok
bílého	bílý	k1gMnSc2	bílý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
silnější	silný	k2eAgMnSc1d2	silnější
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
hluboké	hluboký	k2eAgFnPc1d1	hluboká
drobné	drobný	k2eAgFnPc1d1	drobná
oči	oko	k1gNnPc4	oko
mohly	moct	k5eAaImAgInP	moct
připomínat	připomínat	k5eAaImF	připomínat
prasečí	prasečí	k2eAgFnSc7d1	prasečí
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgFnSc7d1	další
teorií	teorie	k1gFnSc7	teorie
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
megalodon	megalodon	k1gInSc1	megalodon
připomínal	připomínat	k5eAaImAgInS	připomínat
žraloka	žralok	k1gMnSc2	žralok
obrovského	obrovský	k2eAgMnSc2d1	obrovský
(	(	kIx(	(
<g/>
Rhincodon	Rhincodon	k1gInSc1	Rhincodon
typus	typus	k?	typus
<g/>
)	)	kIx)	)
či	či	k8xC	či
velikého	veliký	k2eAgMnSc2d1	veliký
(	(	kIx(	(
<g/>
Cetorhinus	Cetorhinus	k1gInSc1	Cetorhinus
maximus	maximus	k1gInSc1	maximus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
asi	asi	k9	asi
ocasní	ocasní	k2eAgFnSc1d1	ocasní
ploutev	ploutev	k1gFnSc1	ploutev
měla	mít	k5eAaImAgFnS	mít
tvar	tvar	k1gInSc4	tvar
půlměsíce	půlměsíc	k1gInSc2	půlměsíc
a	a	k8xC	a
žralok	žralok	k1gMnSc1	žralok
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
vybaven	vybavit	k5eAaPmNgMnS	vybavit
malou	malý	k2eAgFnSc7d1	malá
řitní	řitní	k2eAgFnSc7d1	řitní
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
hřbetní	hřbetní	k2eAgFnSc7d1	hřbetní
ploutví	ploutev	k1gFnSc7	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
ocasu	ocas	k1gInSc2	ocas
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
mohl	moct	k5eAaImAgInS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
vodorovný	vodorovný	k2eAgInSc1d1	vodorovný
kýl	kýl	k1gInSc1	kýl
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
velkých	velký	k2eAgMnPc2d1	velký
vodních	vodní	k2eAgMnPc2d1	vodní
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
velryby	velryba	k1gFnPc1	velryba
<g/>
,	,	kIx,	,
tuňáci	tuňák	k1gMnPc1	tuňák
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
žraloci	žralok	k1gMnPc1	žralok
a	a	k8xC	a
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
plavání	plavání	k1gNnSc1	plavání
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
hlavy	hlava	k1gFnSc2	hlava
však	však	k9	však
nelze	lze	k6eNd1	lze
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
druhy	druh	k1gInPc7	druh
liší	lišit	k5eAaImIp3nS	lišit
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
tělesných	tělesný	k2eAgFnPc2d1	tělesná
adaptací	adaptace	k1gFnPc2	adaptace
<g/>
,	,	kIx,	,
majících	mající	k2eAgMnPc2d1	mající
snižovat	snižovat	k5eAaImF	snižovat
odpor	odpor	k1gInSc4	odpor
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
spíše	spíše	k9	spíše
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
ocasu	ocas	k1gInSc3	ocas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rod	rod	k1gInSc1	rod
Carcharocles	Carcharoclesa	k1gFnPc2	Carcharoclesa
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Otodus	Otodus	k1gInSc4	Otodus
<g/>
,	,	kIx,	,
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
mají	mít	k5eAaImIp3nP	mít
zuby	zub	k1gInPc1	zub
podobné	podobný	k2eAgInPc1d1	podobný
žralokům	žralok	k1gMnPc3	žralok
písečným	písečný	k2eAgMnPc3d1	písečný
(	(	kIx(	(
<g/>
Carcharias	Carcharias	k1gMnSc1	Carcharias
taurus	taurus	k1gMnSc1	taurus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
nepravděpodobné	pravděpodobný	k2eNgFnSc2d1	nepravděpodobná
teorie	teorie	k1gFnSc2	teorie
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
megalodon	megalodon	k1gInSc1	megalodon
vypadal	vypadat	k5eAaImAgInS	vypadat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
tento	tento	k3xDgMnSc1	tento
žralok	žralok	k1gMnSc1	žralok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozměry	rozměr	k1gInPc4	rozměr
===	===	k?	===
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
megalodona	megalodon	k1gMnSc2	megalodon
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
pouze	pouze	k6eAd1	pouze
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
obratle	obratel	k1gInPc4	obratel
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c4	mnoho
protikladných	protikladný	k2eAgInPc2d1	protikladný
odhadů	odhad	k1gInPc2	odhad
jeho	jeho	k3xOp3gFnSc2	jeho
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
fosilního	fosilní	k2eAgInSc2d1	fosilní
materiálu	materiál	k1gInSc2	materiál
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInSc4	jeho
popis	popis	k1gInSc4	popis
a	a	k8xC	a
velikost	velikost	k1gFnSc1	velikost
odhadovány	odhadovat	k5eAaImNgFnP	odhadovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
žraloka	žralok	k1gMnSc2	žralok
bílého	bílý	k1gMnSc2	bílý
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
tvora	tvor	k1gMnSc4	tvor
jemu	on	k3xPp3gMnSc3	on
nejpodobnějšího	podobný	k2eAgInSc2d3	nejpodobnější
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
americký	americký	k2eAgMnSc1d1	americký
zoolog	zoolog	k1gMnSc1	zoolog
Bashford	Bashford	k1gMnSc1	Bashford
Dean	Dean	k1gMnSc1	Dean
poprvé	poprvé	k6eAd1	poprvé
rekontruoval	rekontruovat	k5eAaPmAgMnS	rekontruovat
čelisti	čelist	k1gFnSc3	čelist
megalodona	megalodon	k1gMnSc2	megalodon
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
sbírek	sbírka	k1gFnPc2	sbírka
Amerického	americký	k2eAgNnSc2d1	americké
přírodovědného	přírodovědný	k2eAgNnSc2d1	Přírodovědné
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozměrů	rozměr	k1gInPc2	rozměr
této	tento	k3xDgFnSc2	tento
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
megalodon	megalodon	k1gInSc1	megalodon
měřil	měřit	k5eAaImAgInS	měřit
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
asi	asi	k9	asi
30	[number]	k4	30
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
však	však	k9	však
Dean	Dean	k1gMnSc1	Dean
přecenil	přecenit	k5eAaPmAgMnS	přecenit
velikost	velikost	k1gFnSc4	velikost
čelistí	čelist	k1gFnPc2	čelist
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
tento	tento	k3xDgInSc4	tento
přehnaný	přehnaný	k2eAgInSc4d1	přehnaný
údaj	údaj	k1gInSc4	údaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ichtyolog	ichtyolog	k1gMnSc1	ichtyolog
John	John	k1gMnSc1	John
E.	E.	kA	E.
Randall	Randall	k1gMnSc1	Randall
využil	využít	k5eAaPmAgMnS	využít
k	k	k7c3	k
odhadnutí	odhadnutí	k1gNnSc3	odhadnutí
velikosti	velikost	k1gFnSc2	velikost
megalodona	megalodon	k1gMnSc2	megalodon
výšku	výška	k1gFnSc4	výška
zubní	zubní	k2eAgFnSc2d1	zubní
skloviny	sklovina	k1gFnSc2	sklovina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
ní	on	k3xPp3gFnSc2	on
mu	on	k3xPp3gMnSc3	on
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
<g/>
,	,	kIx,	,
že	že	k8xS	že
žralok	žralok	k1gMnSc1	žralok
měřil	měřit	k5eAaImAgMnS	měřit
okolo	okolo	k7c2	okolo
třinácti	třináct	k4xCc2	třináct
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
skloviny	sklovina	k1gFnSc2	sklovina
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
nemusí	muset	k5eNaImIp3nS	muset
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
v	v	k7c6	v
přesném	přesný	k2eAgInSc6d1	přesný
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
celkové	celkový	k2eAgFnSc3d1	celková
délce	délka	k1gFnSc3	délka
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
<g/>
Odborníci	odborník	k1gMnPc1	odborník
na	na	k7c6	na
žraloky	žralok	k1gMnPc4	žralok
Michael	Michael	k1gMnSc1	Michael
D.	D.	kA	D.
Gottfried	Gottfried	k1gMnSc1	Gottfried
<g/>
,	,	kIx,	,
Leonard	Leonard	k1gMnSc1	Leonard
Compagno	Compagno	k6eAd1	Compagno
a	a	k8xC	a
S.	S.	kA	S.
Curtis	Curtis	k1gInSc1	Curtis
Bowman	Bowman	k1gMnSc1	Bowman
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
jiný	jiný	k2eAgInSc4d1	jiný
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
lineární	lineární	k2eAgInSc1d1	lineární
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
celkovou	celkový	k2eAgFnSc7d1	celková
délkou	délka	k1gFnSc7	délka
žraloka	žralok	k1gMnSc2	žralok
a	a	k8xC	a
výškou	výška	k1gFnSc7	výška
největšího	veliký	k2eAgInSc2d3	veliký
předního	přední	k2eAgInSc2d1	přední
zubu	zub	k1gInSc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
největšího	veliký	k2eAgInSc2d3	veliký
objeveného	objevený	k2eAgInSc2d1	objevený
zubu	zub	k1gInSc2	zub
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
délka	délka	k1gFnSc1	délka
samic	samice	k1gFnPc2	samice
asi	asi	k9	asi
na	na	k7c4	na
15,6	[number]	k4	15,6
metrů	metr	k1gInPc2	metr
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
existovat	existovat	k5eAaImF	existovat
mohou	moct	k5eAaImIp3nP	moct
i	i	k9	i
větší	veliký	k2eAgInPc1d2	veliký
zuby	zub	k1gInPc1	zub
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
dalším	další	k2eAgInSc7d1	další
návrhem	návrh	k1gInSc7	návrh
výzkumník	výzkumník	k1gMnSc1	výzkumník
žraloků	žralok	k1gMnPc2	žralok
Clifford	Clifford	k1gMnSc1	Clifford
Jeremiah	Jeremiah	k1gMnSc1	Jeremiah
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
byla	být	k5eAaImAgFnS	být
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
přímo	přímo	k6eAd1	přímo
uměrná	uměrný	k2eAgFnSc1d1	uměrná
s	s	k7c7	s
šířkou	šířka	k1gFnSc7	šířka
kořenu	kořen	k1gInSc2	kořen
předního	přední	k2eAgInSc2d1	přední
zubu	zub	k1gInSc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
měl	mít	k5eAaImAgInS	mít
1	[number]	k4	1
cm	cm	kA	cm
šířky	šířka	k1gFnSc2	šířka
kořenu	kořen	k1gInSc2	kořen
odpovídat	odpovídat	k5eAaImF	odpovídat
přibližně	přibližně	k6eAd1	přibližně
1,4	[number]	k4	1,4
m	m	kA	m
délky	délka	k1gFnSc2	délka
žraloka	žralok	k1gMnSc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Jeremiah	Jeremiah	k1gInSc1	Jeremiah
svou	svůj	k3xOyFgFnSc4	svůj
teorii	teorie	k1gFnSc4	teorie
podporoval	podporovat	k5eAaImAgMnS	podporovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obvod	obvod	k1gInSc1	obvod
vnějších	vnější	k2eAgFnPc2d1	vnější
čelistí	čelist	k1gFnPc2	čelist
žraloků	žralok	k1gMnPc2	žralok
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
uměrný	uměrný	k2eAgInSc1d1	uměrný
celkové	celkový	k2eAgFnSc3d1	celková
délce	délka	k1gFnSc3	délka
paryby	paryba	k1gFnSc2	paryba
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pomocí	pomocí	k7c2	pomocí
šířky	šířka	k1gFnSc2	šířka
kořenů	kořen	k1gInPc2	kořen
zubů	zub	k1gInPc2	zub
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
přibližná	přibližný	k2eAgFnSc1d1	přibližná
délka	délka	k1gFnSc1	délka
obvodu	obvod	k1gInSc2	obvod
čelistí	čelist	k1gFnPc2	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
Jeremiah	Jeremiaha	k1gFnPc2	Jeremiaha
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nejdelšího	dlouhý	k2eAgInSc2d3	nejdelší
megalodonova	megalodonův	k2eAgInSc2d1	megalodonův
zubu	zub	k1gInSc2	zub
(	(	kIx(	(
<g/>
12	[number]	k4	12
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
<g/>
,	,	kIx,	,
odhadnul	odhadnout	k5eAaPmAgInS	odhadnout
délku	délka	k1gFnSc4	délka
megalodona	megalodon	k1gMnSc2	megalodon
na	na	k7c4	na
16,5	[number]	k4	16,5
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
paleontolog	paleontolog	k1gMnSc1	paleontolog
Kenshu	Kensh	k1gInSc2	Kensh
Shimada	Shimada	k1gFnSc1	Shimada
z	z	k7c2	z
DePaulovy	DePaulův	k2eAgFnSc2d1	DePaulův
univerzity	univerzita	k1gFnSc2	univerzita
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
výškou	výška	k1gFnSc7	výška
zubní	zubní	k2eAgFnSc2d1	zubní
korunky	korunka	k1gFnSc2	korunka
a	a	k8xC	a
celkovou	celkový	k2eAgFnSc7d1	celková
délkou	délka	k1gFnSc7	délka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
anatomické	anatomický	k2eAgFnPc1d1	anatomická
analýzy	analýza	k1gFnPc1	analýza
několika	několik	k4yIc2	několik
vzorků	vzorek	k1gInPc2	vzorek
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
vztah	vztah	k1gInSc4	vztah
univerzální	univerzální	k2eAgMnSc1d1	univerzální
pro	pro	k7c4	pro
kterýkoli	kterýkoli	k3yIgInSc4	kterýkoli
zub	zub	k1gInSc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Shimada	Shimada	k1gFnSc1	Shimada
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
dříve	dříve	k6eAd2	dříve
navrhované	navrhovaný	k2eAgFnPc1d1	navrhovaná
metody	metoda	k1gFnPc1	metoda
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
méně	málo	k6eAd2	málo
spolehlivém	spolehlivý	k2eAgNnSc6d1	spolehlivé
homologickém	homologický	k2eAgNnSc6d1	homologické
porovnávání	porovnávání	k1gNnSc6	porovnávání
mezi	mezi	k7c7	mezi
megalodonem	megalodon	k1gMnSc7	megalodon
a	a	k8xC	a
žralokem	žralok	k1gMnSc7	žralok
bílým	bílý	k1gMnSc7	bílý
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
růstu	růst	k1gInSc2	růst
zubu	zub	k1gInSc2	zub
mezi	mezi	k7c7	mezi
korunou	koruna	k1gFnSc7	koruna
a	a	k8xC	a
kořenem	kořen	k1gInSc7	kořen
není	být	k5eNaImIp3nS	být
izometrická	izometrický	k2eAgFnSc1d1	izometrická
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
určil	určit	k5eAaPmAgInS	určit
pro	pro	k7c4	pro
Gottfriedův	Gottfriedův	k2eAgInSc4d1	Gottfriedův
zub	zub	k1gInSc4	zub
velikost	velikost	k1gFnSc4	velikost
žraloka	žralok	k1gMnSc2	žralok
asi	asi	k9	asi
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
<g/>
Havajský	havajský	k2eAgMnSc1d1	havajský
ichtyolog	ichtyolog	k1gMnSc1	ichtyolog
John	John	k1gMnSc1	John
E.	E.	kA	E.
Randall	Randall	k1gMnSc1	Randall
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
maximální	maximální	k2eAgFnSc1d1	maximální
délka	délka	k1gFnSc1	délka
megalodona	megalodona	k1gFnSc1	megalodona
činila	činit	k5eAaImAgFnS	činit
asi	asi	k9	asi
13	[number]	k4	13
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dva	dva	k4xCgMnPc1	dva
mořští	mořský	k2eAgMnPc1d1	mořský
biologové	biolog	k1gMnPc1	biolog
Patrick	Patricka	k1gFnPc2	Patricka
J.	J.	kA	J.
Schembri	Schembr	k1gFnSc2	Schembr
a	a	k8xC	a
Stephen	Stephen	k2eAgInSc4d1	Stephen
Papson	Papson	k1gInSc4	Papson
určili	určit	k5eAaPmAgMnP	určit
megalodonovu	megalodonův	k2eAgFnSc4d1	megalodonův
velikost	velikost	k1gFnSc4	velikost
až	až	k9	až
na	na	k7c4	na
24	[number]	k4	24
<g/>
−	−	k?	−
<g/>
25	[number]	k4	25
metrů	metr	k1gInPc2	metr
<g/>
;	;	kIx,	;
naopak	naopak	k6eAd1	naopak
Gottfried	Gottfried	k1gMnSc1	Gottfried
&	&	k?	&
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
zastávali	zastávat	k5eAaImAgMnP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
měřil	měřit	k5eAaImAgInS	měřit
maximálně	maximálně	k6eAd1	maximálně
20,3	[number]	k4	20,3
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
posledních	poslední	k2eAgNnPc2d1	poslední
studií	studio	k1gNnPc2	studio
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2014	[number]	k4	2014
<g/>
−	−	k?	−
<g/>
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
všeobecně	všeobecně	k6eAd1	všeobecně
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
velikost	velikost	k1gFnSc1	velikost
maximálně	maximálně	k6eAd1	maximálně
18	[number]	k4	18
metrů	metr	k1gInPc2	metr
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
10,5	[number]	k4	10,5
metru	metr	k1gInSc2	metr
(	(	kIx(	(
<g/>
největší	veliký	k2eAgMnPc1d3	veliký
žraloci	žralok	k1gMnPc1	žralok
bílí	bílý	k2eAgMnPc1d1	bílý
měřili	měřit	k5eAaImAgMnP	měřit
6,1	[number]	k4	6,1
metru	metr	k1gInSc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
různé	různý	k2eAgFnPc1d1	různá
populace	populace	k1gFnPc1	populace
megalodonů	megalodon	k1gMnPc2	megalodon
dosahovaly	dosahovat	k5eAaImAgFnP	dosahovat
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
územích	území	k1gNnPc6	území
odlišných	odlišný	k2eAgFnPc2d1	odlišná
velikostí	velikost	k1gFnPc2	velikost
kvůli	kvůli	k7c3	kvůli
různým	různý	k2eAgInPc3d1	různý
ekologickým	ekologický	k2eAgInPc3d1	ekologický
tlakům	tlak	k1gInPc3	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
žralok	žralok	k1gMnSc1	žralok
dosahoval	dosahovat	k5eAaImAgMnS	dosahovat
větší	veliký	k2eAgFnPc4d2	veliký
velikosti	velikost	k1gFnPc4	velikost
než	než	k8xS	než
16	[number]	k4	16
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgFnSc1d3	veliký
známá	známá	k1gFnSc1	známá
(	(	kIx(	(
<g/>
pa	pa	k0	pa
<g/>
)	)	kIx)	)
<g/>
ryba	ryba	k1gFnSc1	ryba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kdy	kdy	k6eAd1	kdy
žila	žít	k5eAaImAgFnS	žít
<g/>
,	,	kIx,	,
a	a	k8xC	a
překonal	překonat	k5eAaPmAgMnS	překonat
by	by	kYmCp3nS	by
i	i	k9	i
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
jurskou	jurský	k2eAgFnSc4d1	jurská
rybu	ryba	k1gFnSc4	ryba
Leedsichthys	Leedsichthysa	k1gFnPc2	Leedsichthysa
<g/>
.	.	kIx.	.
<g/>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
megalodona	megalodon	k1gMnSc4	megalodon
mohli	moct	k5eAaImAgMnP	moct
vážit	vážit	k5eAaImF	vážit
mezi	mezi	k7c7	mezi
12,6	[number]	k4	12,6
až	až	k9	až
33,9	[number]	k4	33,9
tun	tuna	k1gFnPc2	tuna
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
mezi	mezi	k7c7	mezi
27,4	[number]	k4	27,4
a	a	k8xC	a
59,4	[number]	k4	59,4
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
byly	být	k5eAaImAgFnP	být
celkově	celkově	k6eAd1	celkově
větší	veliký	k2eAgFnPc1d2	veliký
<g/>
,	,	kIx,	,
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
asi	asi	k9	asi
13,3	[number]	k4	13,3
až	až	k9	až
17	[number]	k4	17
m	m	kA	m
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
samci	samec	k1gMnPc1	samec
měřili	měřit	k5eAaImAgMnP	měřit
mezi	mezi	k7c7	mezi
10,5	[number]	k4	10,5
až	až	k9	až
14,3	[number]	k4	14,3
m.	m.	k?	m.
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
studie	studie	k1gFnSc2	studie
vydané	vydaný	k2eAgFnSc2d1	vydaná
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
rychlost	rychlost	k1gFnSc4	rychlost
žraloků	žralok	k1gMnPc2	žralok
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jejich	jejich	k3xOp3gFnSc2	jejich
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
odhadnuto	odhadnut	k2eAgNnSc1d1	odhadnuto
<g/>
,	,	kIx,	,
že	že	k8xS	že
megalodon	megalodon	k1gInSc1	megalodon
plaval	plavat	k5eAaImAgInS	plavat
rychlostí	rychlost	k1gFnSc7	rychlost
asi	asi	k9	asi
18	[number]	k4	18
kilometrů	kilometr	k1gInPc2	kilometr
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jeho	jeho	k3xOp3gFnSc2	jeho
průměrné	průměrný	k2eAgFnSc2d1	průměrná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
48	[number]	k4	48
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
rychlosti	rychlost	k1gFnSc3	rychlost
jiných	jiný	k1gMnPc2	jiný
vodních	vodní	k2eAgMnPc2d1	vodní
tvorů	tvor	k1gMnPc2	tvor
o	o	k7c4	o
podobné	podobný	k2eAgFnPc4d1	podobná
velikosti	velikost	k1gFnPc4	velikost
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
podobně	podobně	k6eAd1	podobně
velký	velký	k2eAgMnSc1d1	velký
plejtvák	plejtvák	k1gMnSc1	plejtvák
myšok	myšok	k1gInSc1	myšok
(	(	kIx(	(
<g/>
Balaenoptera	Balaenopter	k1gMnSc2	Balaenopter
physalus	physalus	k1gMnSc1	physalus
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
mezi	mezi	k7c4	mezi
14,5	[number]	k4	14,5
<g/>
−	−	k?	−
<g/>
21,5	[number]	k4	21,5
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
<g/>
Obří	obří	k2eAgFnSc1d1	obří
velikost	velikost	k1gFnSc1	velikost
megalodona	megalodona	k1gFnSc1	megalodona
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
klimatickými	klimatický	k2eAgInPc7d1	klimatický
faktory	faktor	k1gInPc7	faktor
<g/>
,	,	kIx,	,
množstvím	množství	k1gNnSc7	množství
velké	velká	k1gFnSc2	velká
kořisti	kořist	k1gFnSc2	kořist
či	či	k8xC	či
vývojem	vývoj	k1gInSc7	vývoj
mezotermie	mezotermie	k1gFnSc2	mezotermie
−	−	k?	−
přechodem	přechod	k1gInSc7	přechod
mezi	mezi	k7c7	mezi
teplokrevností	teplokrevnost	k1gFnSc7	teplokrevnost
a	a	k8xC	a
studenokrevností	studenokrevnost	k1gFnSc7	studenokrevnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
rychlosti	rychlost	k1gFnSc2	rychlost
metabolismu	metabolismus	k1gInSc2	metabolismus
megalodona	megalodona	k1gFnSc1	megalodona
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
by	by	kYmCp3nP	by
pak	pak	k6eAd1	pak
dovedl	dovést	k5eAaPmAgInS	dovést
také	také	k9	také
rychleji	rychle	k6eAd2	rychle
plavat	plavat	k5eAaImF	plavat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
žraloci	žralok	k1gMnPc1	žralok
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Otodontidae	Otodontidae	k1gFnPc2	Otodontidae
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
studenokrevné	studenokrevný	k2eAgNnSc4d1	studenokrevné
a	a	k8xC	a
megalodon	megalodon	k1gNnSc4	megalodon
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
jejich	jejich	k3xOp3gMnSc2	jejich
blízkého	blízký	k2eAgMnSc2d1	blízký
příbuzného	příbuzný	k1gMnSc2	příbuzný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgInS	být
studenokrevný	studenokrevný	k2eAgMnSc1d1	studenokrevný
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
možnost	možnost	k1gFnSc1	možnost
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1	nepravděpodobná
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
současných	současný	k2eAgMnPc2d1	současný
studenokrevných	studenokrevný	k2eAgMnPc2d1	studenokrevný
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
žralok	žralok	k1gMnSc1	žralok
obrovský	obrovský	k2eAgMnSc1d1	obrovský
<g/>
,	,	kIx,	,
živí	živit	k5eAaImIp3nP	živit
převážně	převážně	k6eAd1	převážně
filtrováním	filtrování	k1gNnSc7	filtrování
potravy	potrava	k1gFnSc2	potrava
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k8xC	tedy
nejsou	být	k5eNaImIp3nP	být
adaptováni	adaptovat	k5eAaBmNgMnP	adaptovat
na	na	k7c4	na
dravý	dravý	k2eAgInSc4d1	dravý
styl	styl	k1gInSc4	styl
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zuby	zub	k1gInPc1	zub
a	a	k8xC	a
síla	síla	k1gFnSc1	síla
skusu	skus	k1gInSc2	skus
===	===	k?	===
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
zachované	zachovaný	k2eAgFnPc1d1	zachovaná
fosilie	fosilie	k1gFnPc1	fosilie
megalodona	megalodon	k1gMnSc2	megalodon
jsou	být	k5eAaImIp3nP	být
zuby	zub	k1gInPc1	zub
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgFnPc1d1	velká
a	a	k8xC	a
robustní	robustní	k2eAgFnPc1d1	robustní
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
trojúhelníkový	trojúhelníkový	k2eAgInSc4d1	trojúhelníkový
tvar	tvar	k1gInSc4	tvar
s	s	k7c7	s
napojením	napojení	k1gNnSc7	napojení
kořene	kořen	k1gInSc2	kořen
na	na	k7c4	na
korunku	korunka	k1gFnSc4	korunka
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
V	V	kA	V
<g/>
,	,	kIx,	,
malé	malý	k2eAgNnSc1d1	malé
vroubkování	vroubkování	k1gNnSc1	vroubkování
a	a	k8xC	a
postrádají	postrádat	k5eAaImIp3nP	postrádat
postranní	postranní	k2eAgInPc1d1	postranní
hroty	hrot	k1gInPc1	hrot
<g/>
.	.	kIx.	.
</s>
<s>
Zub	zub	k1gInSc1	zub
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
čelisti	čelist	k1gFnSc6	čelist
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
žraloka	žralok	k1gMnSc2	žralok
bílého	bílé	k1gNnSc2	bílé
zasazen	zasazen	k2eAgInSc1d1	zasazen
strmě	strmě	k6eAd1	strmě
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
čelisti	čelist	k1gFnSc6	čelist
ukotveny	ukotven	k2eAgFnPc4d1	ukotvena
vlákny	vlákna	k1gFnPc4	vlákna
kolagenu	kolagen	k1gInSc2	kolagen
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc4	jejich
základna	základna	k1gFnSc1	základna
byla	být	k5eAaImAgFnS	být
drsná	drsný	k2eAgFnSc1d1	drsná
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přidávalo	přidávat	k5eAaImAgNnS	přidávat
na	na	k7c6	na
pevnosti	pevnost	k1gFnSc6	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
zubu	zub	k1gInSc2	zub
směřující	směřující	k2eAgFnSc1d1	směřující
k	k	k7c3	k
jazyku	jazyk	k1gInSc3	jazyk
byla	být	k5eAaImAgFnS	být
vypouklá	vypouklý	k2eAgFnSc1d1	vypouklá
(	(	kIx(	(
<g/>
konvexní	konvexní	k2eAgFnSc1d1	konvexní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
strana	strana	k1gFnSc1	strana
zubu	zub	k1gInSc2	zub
pouze	pouze	k6eAd1	pouze
mírně	mírně	k6eAd1	mírně
konvexní	konvexní	k2eAgFnSc1d1	konvexní
až	až	k9	až
skoro	skoro	k6eAd1	skoro
plochá	plochý	k2eAgFnSc1d1	plochá
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgInPc1d1	přední
zuby	zub	k1gInPc1	zub
byly	být	k5eAaImAgInP	být
téměř	téměř	k6eAd1	téměř
kolmé	kolmý	k2eAgFnPc1d1	kolmá
k	k	k7c3	k
čelisti	čelist	k1gFnSc3	čelist
a	a	k8xC	a
symetrické	symetrický	k2eAgFnSc3d1	symetrická
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgFnSc3d1	zadní
zasazeny	zasazen	k2eAgFnPc4d1	zasazena
šikmo	šikmo	k6eAd1	šikmo
a	a	k8xC	a
asymetrické	asymetrický	k2eAgInPc1d1	asymetrický
<g/>
.	.	kIx.	.
<g/>
Zuby	zub	k1gInPc1	zub
megalodona	megalodona	k1gFnSc1	megalodona
mohou	moct	k5eAaImIp3nP	moct
měřit	měřit	k5eAaImF	měřit
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
sklonu	sklon	k1gInSc2	sklon
(	(	kIx(	(
<g/>
diagonální	diagonální	k2eAgFnSc1d1	diagonální
délka	délka	k1gFnSc1	délka
<g/>
)	)	kIx)	)
i	i	k9	i
přes	přes	k7c4	přes
180	[number]	k4	180
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
čelistí	čelist	k1gFnPc2	čelist
od	od	k7c2	od
lovce	lovec	k1gMnSc2	lovec
fosilií	fosilie	k1gFnSc7	fosilie
Vita	vit	k2eAgMnSc4d1	vit
Bertucciho	Bertucci	k1gMnSc4	Bertucci
je	být	k5eAaImIp3nS	být
obsažen	obsažen	k2eAgInSc1d1	obsažen
i	i	k8xC	i
zub	zub	k1gInSc1	zub
o	o	k7c6	o
údajné	údajný	k2eAgFnSc6d1	údajná
délce	délka	k1gFnSc6	délka
více	hodně	k6eAd2	hodně
než	než	k8xS	než
180	[number]	k4	180
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
rozměry	rozměr	k1gInPc7	rozměr
jsou	být	k5eAaImIp3nP	být
zuby	zub	k1gInPc1	zub
megalodona	megalodon	k1gMnSc2	megalodon
největšími	veliký	k2eAgMnPc7d3	veliký
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgInPc7	všecek
doposud	doposud	k6eAd1	doposud
známými	známý	k2eAgInPc7d1	známý
druhy	druh	k1gInPc7	druh
žraloků	žralok	k1gMnPc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
Saitamě	Saitama	k1gFnSc6	Saitama
objevena	objevit	k5eAaPmNgNnP	objevit
jejich	jejich	k3xOp3gFnPc7	jejich
téměř	téměř	k6eAd1	téměř
kompletní	kompletní	k2eAgFnSc1d1	kompletní
sada	sada	k1gFnSc1	sada
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
sada	sada	k1gFnSc1	sada
zubů	zub	k1gInPc2	zub
megalodona	megalodona	k1gFnSc1	megalodona
byla	být	k5eAaImAgFnS	být
vykopána	vykopat	k5eAaPmNgFnS	vykopat
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
formaci	formace	k1gFnSc6	formace
Yorktown	Yorktown	k1gInSc1	Yorktown
(	(	kIx(	(
<g/>
Maryland	Maryland	k1gInSc1	Maryland
<g/>
)	)	kIx)	)
a	a	k8xC	a
posloužila	posloužit	k5eAaPmAgFnS	posloužit
jako	jako	k9	jako
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
čelistí	čelist	k1gFnPc2	čelist
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
přírodovědeckém	přírodovědecký	k2eAgNnSc6d1	Přírodovědecké
muzeu	muzeum	k1gNnSc6	muzeum
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgMnPc2	tento
objevů	objev	k1gInPc2	objev
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
určen	určen	k2eAgInSc1d1	určen
celkový	celkový	k2eAgInSc1d1	celkový
zubní	zubní	k2eAgInSc1d1	zubní
vzorec	vzorec	k1gInSc1	vzorec
megalodona	megalodona	k1gFnSc1	megalodona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ten	ten	k3xDgInSc1	ten
činí	činit	k5eAaImIp3nS	činit
2.1.7.43	[number]	k4	2.1.7.43
<g/>
.0	.0	k4	.0
<g/>
.8	.8	k4	.8
<g/>
.4	.4	k4	.4
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
ze	z	k7c2	z
vzorce	vzorec	k1gInSc2	vzorec
<g/>
,	,	kIx,	,
megalodon	megalodon	k1gInSc1	megalodon
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
čelistech	čelist	k1gFnPc6	čelist
čtyři	čtyři	k4xCgInPc4	čtyři
druhy	druh	k1gInPc1	druh
zubů	zub	k1gInPc2	zub
<g/>
:	:	kIx,	:
přední	přední	k2eAgMnSc1d1	přední
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc6d1	střední
<g/>
,	,	kIx,	,
boční	boční	k2eAgFnSc6d1	boční
a	a	k8xC	a
zadní	zadní	k2eAgFnSc6d1	zadní
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
velmi	velmi	k6eAd1	velmi
robustní	robustní	k2eAgInSc4d1	robustní
chrup	chrup	k1gInSc4	chrup
<g/>
,	,	kIx,	,
čelisti	čelist	k1gFnPc1	čelist
byly	být	k5eAaImAgFnP	být
osazeny	osadit	k5eAaPmNgFnP	osadit
až	až	k6eAd1	až
250	[number]	k4	250
zuby	zub	k1gInPc4	zub
v	v	k7c6	v
pěti	pět	k4xCc6	pět
řadách	řada	k1gFnPc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Zoubkování	zoubkování	k1gNnSc1	zoubkování
jejich	jejich	k3xOp3gInSc2	jejich
nositeli	nositel	k1gMnSc3	nositel
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
prořezávání	prořezávání	k1gNnPc2	prořezávání
masem	maso	k1gNnSc7	maso
a	a	k8xC	a
kostmi	kost	k1gFnPc7	kost
<g/>
.	.	kIx.	.
</s>
<s>
Čelist	čelist	k1gFnSc1	čelist
u	u	k7c2	u
velkých	velký	k2eAgMnPc2d1	velký
jedinců	jedinec	k1gMnPc2	jedinec
měřila	měřit	k5eAaImAgFnS	měřit
napříč	napříč	k6eAd1	napříč
až	až	k8xS	až
dva	dva	k4xCgInPc4	dva
metry	metr	k1gInPc4	metr
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
se	se	k3xPyFc4	se
rozevřít	rozevřít	k5eAaPmF	rozevřít
až	až	k9	až
do	do	k7c2	do
75	[number]	k4	75
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
přírodovědeckém	přírodovědecký	k2eAgNnSc6d1	Přírodovědecké
muzeu	muzeum	k1gNnSc6	muzeum
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
i	i	k9	i
100	[number]	k4	100
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
tým	tým	k1gInSc1	tým
vědců	vědec	k1gMnPc2	vědec
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
S.	S.	kA	S.
Wroea	Wroeus	k1gMnSc2	Wroeus
pokus	pokus	k1gInSc1	pokus
mající	mající	k2eAgInSc1d1	mající
zjistit	zjistit	k5eAaPmF	zjistit
sílu	síla	k1gFnSc4	síla
skusu	skus	k1gInSc2	skus
velkého	velký	k2eAgMnSc2d1	velký
bílého	bílý	k1gMnSc2	bílý
žraloka	žralok	k1gMnSc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
s	s	k7c7	s
2,5	[number]	k4	2,5
<g/>
metrovým	metrový	k2eAgInSc7d1	metrový
exemplářem	exemplář	k1gInSc7	exemplář
a	a	k8xC	a
výsledky	výsledek	k1gInPc1	výsledek
poté	poté	k6eAd1	poté
přepočteny	přepočíst	k5eAaPmNgInP	přepočíst
na	na	k7c4	na
maximální	maximální	k2eAgFnSc4d1	maximální
potvrzenou	potvrzený	k2eAgFnSc4d1	potvrzená
velikost	velikost	k1gFnSc4	velikost
žraloka	žralok	k1gMnSc2	žralok
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
na	na	k7c4	na
minimální	minimální	k2eAgFnSc4d1	minimální
a	a	k8xC	a
maximální	maximální	k2eAgFnSc4d1	maximální
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
hmotnost	hmotnost	k1gFnSc4	hmotnost
megalodona	megalodon	k1gMnSc2	megalodon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
mohl	moct	k5eAaImAgInS	moct
megalodon	megalodon	k1gInSc1	megalodon
vyvinout	vyvinout	k5eAaPmF	vyvinout
sílu	síla	k1gFnSc4	síla
o	o	k7c4	o
108	[number]	k4	108
514	[number]	k4	514
až	až	k9	až
182	[number]	k4	182
201	[number]	k4	201
newtonech	newton	k1gInPc6	newton
<g/>
,	,	kIx,	,
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
18	[number]	k4	18
216	[number]	k4	216
newtony	newton	k1gInPc7	newton
u	u	k7c2	u
žraloka	žralok	k1gMnSc2	žralok
bílého	bílé	k1gNnSc2	bílé
a	a	k8xC	a
5	[number]	k4	5
300	[number]	k4	300
až	až	k9	až
7	[number]	k4	7
400	[number]	k4	400
u	u	k7c2	u
vyhynulé	vyhynulý	k2eAgFnSc2d1	vyhynulá
ryby	ryba	k1gFnSc2	ryba
Dunkleosteus	Dunkleosteus	k1gMnSc1	Dunkleosteus
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
tým	tým	k1gInSc1	tým
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
žraloci	žralok	k1gMnPc1	žralok
sebou	se	k3xPyFc7	se
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
trhají	trhat	k5eAaImIp3nP	trhat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
udanou	udaný	k2eAgFnSc4d1	udaná
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
bude	být	k5eAaImBp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
celková	celkový	k2eAgFnSc1d1	celková
působená	působený	k2eAgFnSc1d1	působená
síla	síla	k1gFnSc1	síla
ještě	ještě	k6eAd1	ještě
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
odhad	odhad	k1gInSc1	odhad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
popis	popis	k1gInSc1	popis
===	===	k?	===
</s>
</p>
<p>
<s>
Fosilie	fosilie	k1gFnSc1	fosilie
megalodona	megalodona	k1gFnSc1	megalodona
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
pouze	pouze	k6eAd1	pouze
zuby	zub	k1gInPc7	zub
<g/>
,	,	kIx,	,
obratli	obratel	k1gInPc7	obratel
a	a	k8xC	a
koprolity	koprolit	k1gInPc1	koprolit
−	−	k?	−
fosilním	fosilní	k2eAgInSc7d1	fosilní
trusem	trus	k1gInSc7	trus
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
i	i	k8xC	i
kostra	kostra	k1gFnSc1	kostra
megalodona	megalodona	k1gFnSc1	megalodona
nebyla	být	k5eNaImAgFnS	být
tvořena	tvořen	k2eAgFnSc1d1	tvořena
kostmi	kost	k1gFnPc7	kost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozpadající	rozpadající	k2eAgFnSc7d1	rozpadající
chrupavkou	chrupavka	k1gFnSc7	chrupavka
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
fosilních	fosilní	k2eAgInPc2d1	fosilní
vzorků	vzorek	k1gInPc2	vzorek
špatně	špatně	k6eAd1	špatně
zachována	zachován	k2eAgFnSc1d1	zachována
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
jeho	jeho	k3xOp3gInPc2	jeho
mohutných	mohutný	k2eAgInPc2d1	mohutný
zubů	zub	k1gInPc2	zub
by	by	kYmCp3nP	by
musely	muset	k5eAaImAgInP	muset
být	být	k5eAaImF	být
čelisti	čelist	k1gFnPc4	čelist
megalodona	megalodona	k1gFnSc1	megalodona
mnohem	mnohem	k6eAd1	mnohem
masivnější	masivní	k2eAgFnSc1d2	masivnější
<g/>
,	,	kIx,	,
pevnější	pevný	k2eAgFnSc1d2	pevnější
a	a	k8xC	a
silnější	silný	k2eAgFnSc1d2	silnější
než	než	k8xS	než
čelisti	čelist	k1gFnPc1	čelist
žraloka	žralok	k1gMnSc2	žralok
bílého	bílý	k1gMnSc2	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
jeho	jeho	k3xOp3gFnSc1	jeho
chrupavčitá	chrupavčitý	k2eAgFnSc1d1	chrupavčitá
lebka	lebka	k1gFnSc1	lebka
<g/>
,	,	kIx,	,
chondrokranium	chondrokranium	k1gNnSc1	chondrokranium
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
mohutnější	mohutný	k2eAgFnSc1d2	mohutnější
než	než	k8xS	než
u	u	k7c2	u
žraloka	žralok	k1gMnSc2	žralok
bílého	bílý	k1gMnSc2	bílý
<g/>
,	,	kIx,	,
i	i	k8xC	i
ploutve	ploutev	k1gFnPc1	ploutev
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
u	u	k7c2	u
megalodona	megalodon	k1gMnSc2	megalodon
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
velké	velká	k1gFnPc1	velká
<g/>
.	.	kIx.	.
<g/>
Fosilní	fosilní	k2eAgInPc1d1	fosilní
obratle	obratel	k1gInPc1	obratel
megalodona	megalodona	k1gFnSc1	megalodona
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
například	například	k6eAd1	například
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Antverp	Antverpy	k1gFnPc2	Antverpy
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pozoruhodný	pozoruhodný	k2eAgInSc1d1	pozoruhodný
nález	nález	k1gInSc1	nález
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
150	[number]	k4	150
částečně	částečně	k6eAd1	částečně
zachovaných	zachovaný	k2eAgInPc2d1	zachovaný
obratlů	obratel	k1gInPc2	obratel
<g/>
,	,	kIx,	,
měřících	měřící	k2eAgMnPc2d1	měřící
mezi	mezi	k7c4	mezi
55	[number]	k4	55
až	až	k9	až
155	[number]	k4	155
milimetry	milimetr	k1gInPc4	milimetr
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
obratle	obratel	k1gInSc2	obratel
megalodona	megalodon	k1gMnSc2	megalodon
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
i	i	k9	i
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
výzkum	výzkum	k1gInSc1	výzkum
tohoto	tento	k3xDgInSc2	tento
vzorku	vzorek	k1gInSc2	vzorek
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
megalodon	megalodon	k1gMnSc1	megalodon
měl	mít	k5eAaImAgMnS	mít
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
počet	počet	k1gInSc4	počet
obratlů	obratel	k1gInPc2	obratel
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
žraloky	žralok	k1gMnPc7	žralok
<g/>
,	,	kIx,	,
možná	možná	k9	možná
přes	přes	k7c4	přes
200	[number]	k4	200
obratlů	obratel	k1gInPc2	obratel
<g/>
;	;	kIx,	;
podobnému	podobný	k2eAgInSc3d1	podobný
počtu	počet	k1gInSc3	počet
se	se	k3xPyFc4	se
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
pouze	pouze	k6eAd1	pouze
žralok	žralok	k1gMnSc1	žralok
bílý	bílý	k1gMnSc1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
podobně	podobně	k6eAd1	podobně
zachovalé	zachovalý	k2eAgInPc1d1	zachovalý
obratle	obratel	k1gInPc1	obratel
byly	být	k5eAaImAgInP	být
vyzvednuty	vyzvednout	k5eAaPmNgInP	vyzvednout
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
ve	v	k7c6	v
formaci	formace	k1gFnSc6	formace
Gram	gram	k1gInSc4	gram
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
celkem	celkem	k6eAd1	celkem
dvacet	dvacet	k4xCc1	dvacet
obratlů	obratel	k1gInPc2	obratel
<g/>
,	,	kIx,	,
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
mezi	mezi	k7c7	mezi
100	[number]	k4	100
až	až	k9	až
230	[number]	k4	230
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koprolity	koprolit	k1gInPc1	koprolit
megalodona	megalodona	k1gFnSc1	megalodona
byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
například	například	k6eAd1	například
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Karolíně	Karolína	k1gFnSc6	Karolína
a	a	k8xC	a
datovány	datovat	k5eAaImNgFnP	datovat
do	do	k7c2	do
období	období	k1gNnSc2	období
miocén	miocén	k1gInSc1	miocén
<g/>
.	.	kIx.	.
</s>
<s>
Měřily	měřit	k5eAaImAgFnP	měřit
14	[number]	k4	14
cm	cm	kA	cm
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
spirálovité	spirálovitý	k2eAgFnPc1d1	spirálovitá
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
megalodon	megalodon	k1gInSc1	megalodon
vybaven	vybavit	k5eAaPmNgInS	vybavit
takzvanou	takzvaný	k2eAgFnSc7d1	takzvaná
spirální	spirální	k2eAgFnSc7d1	spirální
řasou	řasa	k1gFnSc7	řasa
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc7d1	spodní
stočenou	stočený	k2eAgFnSc7d1	stočená
částí	část	k1gFnSc7	část
střev	střevo	k1gNnPc2	střevo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
lepší	dobrý	k2eAgFnSc4d2	lepší
absorpci	absorpce	k1gFnSc4	absorpce
živin	živina	k1gFnPc2	živina
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
adaptaci	adaptace	k1gFnSc4	adaptace
mají	mít	k5eAaImIp3nP	mít
například	například	k6eAd1	například
současní	současný	k2eAgMnPc1d1	současný
lamnovití	lamnovitý	k2eAgMnPc1d1	lamnovitý
<g/>
.	.	kIx.	.
<g/>
Kompletní	kompletní	k2eAgFnSc1d1	kompletní
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
kostry	kostra	k1gFnSc2	kostra
megalodona	megalodona	k1gFnSc1	megalodona
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vystavována	vystavovat	k5eAaImNgFnS	vystavovat
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Calvert	Calvert	k1gInSc1	Calvert
Marine	Marin	k1gInSc5	Marin
Museum	museum	k1gNnSc1	museum
a	a	k8xC	a
jihoafrickém	jihoafrický	k2eAgNnSc6d1	jihoafrické
Iziko	Iziko	k1gNnSc4	Iziko
South	South	k1gMnSc1	South
African	African	k1gMnSc1	African
Museum	museum	k1gNnSc4	museum
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
vědcem	vědec	k1gMnSc7	vědec
Michaelem	Michael	k1gMnSc7	Michael
Gottfriedem	Gottfried	k1gMnSc7	Gottfried
a	a	k8xC	a
kolektivem	kolektiv	k1gInSc7	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
11,3	[number]	k4	11,3
metrů	metr	k1gInPc2	metr
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
exponát	exponát	k1gInSc1	exponát
má	mít	k5eAaImIp3nS	mít
představovat	představovat	k5eAaImF	představovat
dospělého	dospělý	k2eAgMnSc4d1	dospělý
samce	samec	k1gMnSc4	samec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Paleobiologie	paleobiologie	k1gFnSc2	paleobiologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Výskyt	výskyt	k1gInSc1	výskyt
===	===	k?	===
</s>
</p>
<p>
<s>
Megalodon	Megalodon	k1gMnSc1	Megalodon
byl	být	k5eAaImAgMnS	být
kosmopolitem	kosmopolit	k1gMnSc7	kosmopolit
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnPc1	jeho
fosilie	fosilie	k1gFnPc1	fosilie
byly	být	k5eAaImAgFnP	být
vykopány	vykopán	k2eAgFnPc1d1	vykopána
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
v	v	k7c6	v
subtropických	subtropický	k2eAgFnPc6d1	subtropická
až	až	k8xS	až
mírných	mírný	k2eAgFnPc6d1	mírná
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
šířkách	šířka	k1gFnPc6	šířka
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
do	do	k7c2	do
55	[number]	k4	55
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Toleroval	tolerovat	k5eAaImAgMnS	tolerovat
vody	voda	k1gFnPc4	voda
o	o	k7c6	o
teplotách	teplota	k1gFnPc6	teplota
mezi	mezi	k7c7	mezi
1	[number]	k4	1
<g/>
−	−	k?	−
<g/>
24	[number]	k4	24
°	°	k?	°
<g/>
C.	C.	kA	C.
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobně	podobně	k6eAd1	podobně
nízké	nízký	k2eAgFnPc4d1	nízká
teploty	teplota	k1gFnPc4	teplota
snášel	snášet	k5eAaImAgMnS	snášet
díky	díky	k7c3	díky
mezotermii	mezotermie	k1gFnSc3	mezotermie
<g/>
,	,	kIx,	,
fyziologické	fyziologický	k2eAgFnPc4d1	fyziologická
schopnosti	schopnost	k1gFnPc4	schopnost
velkých	velký	k2eAgMnPc2d1	velký
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
které	který	k3yIgFnSc2	který
dovedou	dovést	k5eAaPmIp3nP	dovést
udržovat	udržovat	k5eAaImF	udržovat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
teplotu	teplota	k1gFnSc4	teplota
těla	tělo	k1gNnSc2	tělo
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
metabolického	metabolický	k2eAgNnSc2d1	metabolické
tepla	teplo	k1gNnSc2	teplo
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
právě	právě	k9	právě
například	například	k6eAd1	například
u	u	k7c2	u
žraloků	žralok	k1gMnPc2	žralok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Megalodon	Megalodon	k1gMnSc1	Megalodon
obýval	obývat	k5eAaImAgMnS	obývat
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
mořských	mořský	k2eAgFnPc2d1	mořská
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
mělké	mělký	k2eAgFnPc1d1	mělká
pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
vody	voda	k1gFnPc1	voda
<g/>
,	,	kIx,	,
bažinaté	bažinatý	k2eAgFnPc1d1	bažinatá
laguny	laguna	k1gFnPc1	laguna
<g/>
,	,	kIx,	,
písčitý	písčitý	k2eAgInSc1d1	písčitý
litorál	litorál	k1gInSc1	litorál
i	i	k8xC	i
hluboké	hluboký	k2eAgNnSc1d1	hluboké
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
a	a	k8xC	a
vykazoval	vykazovat	k5eAaImAgInS	vykazovat
přechodný	přechodný	k2eAgInSc4d1	přechodný
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
megalodoni	megalodon	k1gMnPc1	megalodon
nebyli	být	k5eNaImAgMnP	být
hojní	hojný	k2eAgMnPc1d1	hojný
v	v	k7c6	v
mělkých	mělký	k2eAgFnPc6d1	mělká
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
obývali	obývat	k5eAaImAgMnP	obývat
pobřežní	pobřežní	k2eAgFnPc4d1	pobřežní
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
fázích	fáze	k1gFnPc6	fáze
životního	životní	k2eAgInSc2d1	životní
cyklu	cyklus	k1gInSc2	cyklus
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
megalodon	megalodon	k1gInSc1	megalodon
pohybovat	pohybovat	k5eAaImF	pohybovat
mezi	mezi	k7c7	mezi
pobřežními	pobřežní	k2eAgFnPc7d1	pobřežní
a	a	k8xC	a
vzdálenějšími	vzdálený	k2eAgFnPc7d2	vzdálenější
oceánskými	oceánský	k2eAgFnPc7d1	oceánská
vodami	voda	k1gFnPc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
fosilie	fosilie	k1gFnPc1	fosilie
byly	být	k5eAaImAgFnP	být
pak	pak	k6eAd1	pak
objeveny	objevit	k5eAaPmNgInP	objevit
i	i	k9	i
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
u	u	k7c2	u
Marianského	Marianský	k2eAgInSc2d1	Marianský
příkopu	příkop	k1gInSc2	příkop
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
<g/>
Fosilní	fosilní	k2eAgInPc1d1	fosilní
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
megalodoni	megalodon	k1gMnPc1	megalodon
žijící	žijící	k2eAgMnPc1d1	žijící
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
poloukouli	poloukoulit	k5eAaPmRp2nS	poloukoulit
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
megalodoni	megalodon	k1gMnPc1	megalodon
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
potom	potom	k6eAd1	potom
byli	být	k5eAaImAgMnP	být
větší	veliký	k2eAgMnPc1d2	veliký
než	než	k8xS	než
megalodoni	megalodon	k1gMnPc1	megalodon
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
Atlantském	atlantský	k2eAgInSc6d1	atlantský
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
však	však	k9	však
předpokládán	předpokládán	k2eAgInSc4d1	předpokládán
žádný	žádný	k3yNgInSc4	žádný
trend	trend	k1gInSc4	trend
zvětšování	zvětšování	k1gNnSc4	zvětšování
velikosti	velikost	k1gFnSc2	velikost
těla	tělo	k1gNnSc2	tělo
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
šířce	šířka	k1gFnSc6	šířka
či	či	k8xC	či
změna	změna	k1gFnSc1	změna
velikosti	velikost	k1gFnSc2	velikost
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
linie	linie	k1gFnPc1	linie
Carcharocles	Carcharoclesa	k1gFnPc2	Carcharoclesa
tuto	tento	k3xDgFnSc4	tento
vlastnost	vlastnost	k1gFnSc4	vlastnost
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Modus	modus	k1gInSc1	modus
všech	všecek	k3xTgFnPc2	všecek
velikostí	velikost	k1gFnPc2	velikost
byl	být	k5eAaImAgInS	být
odhadnut	odhadnut	k2eAgInSc1d1	odhadnut
na	na	k7c4	na
asi	asi	k9	asi
10,5	[number]	k4	10,5
m	m	kA	m
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
větší	veliký	k2eAgMnPc1d2	veliký
jedinci	jedinec	k1gMnPc1	jedinec
obývají	obývat	k5eAaImIp3nP	obývat
více	hodně	k6eAd2	hodně
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
ekologickou	ekologický	k2eAgFnSc4d1	ekologická
nebo	nebo	k8xC	nebo
konkurenční	konkurenční	k2eAgFnSc4d1	konkurenční
výhodu	výhoda	k1gFnSc4	výhoda
pro	pro	k7c4	pro
větší	veliký	k2eAgFnSc4d2	veliký
velikost	velikost	k1gFnSc4	velikost
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rozmístění	rozmístění	k1gNnPc1	rozmístění
fosilií	fosilie	k1gFnPc2	fosilie
====	====	k?	====
</s>
</p>
<p>
<s>
Megalodon	Megalodon	k1gMnSc1	Megalodon
se	se	k3xPyFc4	se
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
objeveno	objeven	k2eAgNnSc1d1	objeveno
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
fosilií	fosilie	k1gFnPc2	fosilie
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
oceánech	oceán	k1gInPc6	oceán
neogénu	neogén	k1gInSc2	neogén
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
Žraloci	žralok	k1gMnPc1	žralok
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
živí	živit	k5eAaImIp3nS	živit
oportunisticky	oportunisticky	k6eAd1	oportunisticky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
megalodon	megalodon	k1gInSc1	megalodon
byl	být	k5eAaImAgInS	být
obzvláště	obzvláště	k6eAd1	obzvláště
dravý	dravý	k2eAgInSc1d1	dravý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
velká	velký	k2eAgFnSc1d1	velká
velikost	velikost	k1gFnSc1	velikost
společně	společně	k6eAd1	společně
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
rychlého	rychlý	k2eAgNnSc2d1	rychlé
plavání	plavání	k1gNnSc2	plavání
a	a	k8xC	a
mocnými	mocný	k2eAgFnPc7d1	mocná
čelistmi	čelist	k1gFnPc7	čelist
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činila	činit	k5eAaImAgFnS	činit
vrcholového	vrcholový	k2eAgMnSc4d1	vrcholový
predátora	predátor	k1gMnSc4	predátor
schopného	schopný	k2eAgMnSc4d1	schopný
zabíjet	zabíjet	k5eAaImF	zabíjet
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnPc1	studie
zkoumající	zkoumající	k2eAgInPc4d1	zkoumající
izotopy	izotop	k1gInPc4	izotop
vápníku	vápník	k1gInSc2	vápník
vyhynulých	vyhynulý	k2eAgFnPc2d1	vyhynulá
a	a	k8xC	a
žijících	žijící	k2eAgFnPc2d1	žijící
paryb	paryba	k1gFnPc2	paryba
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
megalodon	megalodon	k1gMnSc1	megalodon
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
vyšší	vysoký	k2eAgFnSc6d2	vyšší
trofické	trofický	k2eAgFnSc6d1	trofická
úrovni	úroveň	k1gFnSc6	úroveň
než	než	k8xS	než
žralok	žralok	k1gMnSc1	žralok
bílý	bílý	k1gMnSc1	bílý
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
potravním	potravní	k2eAgInSc6d1	potravní
řetězci	řetězec	k1gInSc6	řetězec
ještě	ještě	k9	ještě
výše	výše	k1gFnSc1	výše
<g/>
.	.	kIx.	.
<g/>
Fosilní	fosilní	k2eAgInPc1d1	fosilní
důkazy	důkaz	k1gInPc1	důkaz
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
živil	živit	k5eAaImAgInS	živit
mnoha	mnoho	k4c3	mnoho
druhy	druh	k1gInPc1	druh
kytovců	kytovec	k1gMnPc2	kytovec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
delfíni	delfín	k1gMnPc1	delfín
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnPc1d1	malá
velryby	velryba	k1gFnPc1	velryba
<g/>
,	,	kIx,	,
velryby	velryba	k1gFnPc1	velryba
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
cetotheridů	cetotherid	k1gInPc2	cetotherid
či	či	k8xC	či
squalodontidů	squalodontid	k1gInPc2	squalodontid
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zabíjel	zabíjet	k5eAaImAgMnS	zabíjet
vorvaně	vorvaň	k1gMnSc2	vorvaň
<g/>
,	,	kIx,	,
velryby	velryba	k1gFnSc2	velryba
grónské	grónský	k2eAgFnSc2d1	grónská
či	či	k8xC	či
plejtvákovité	plejtvákovitý	k2eAgFnSc2d1	plejtvákovitý
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
sirénami	siréna	k1gFnPc7	siréna
<g/>
,	,	kIx,	,
velkými	velký	k2eAgFnPc7d1	velká
mořskými	mořský	k2eAgFnPc7d1	mořská
želvami	želva	k1gFnPc7	želva
<g/>
,	,	kIx,	,
nepohrdl	pohrdnout	k5eNaPmAgMnS	pohrdnout
ani	ani	k8xC	ani
menšími	malý	k2eAgFnPc7d2	menší
rybami	ryba	k1gFnPc7	ryba
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
druhy	druh	k1gInPc7	druh
žraloků	žralok	k1gMnPc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
mnoho	mnoho	k4c1	mnoho
velrybích	velrybí	k2eAgFnPc2d1	velrybí
kostí	kost	k1gFnPc2	kost
s	s	k7c7	s
hlubokými	hluboký	k2eAgInPc7d1	hluboký
kousanci	kousanec	k1gInPc7	kousanec
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
od	od	k7c2	od
megalodonů	megalodon	k1gInPc2	megalodon
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
zuby	zub	k1gInPc1	zub
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
někdy	někdy	k6eAd1	někdy
našly	najít	k5eAaPmAgFnP	najít
blízko	blízko	k6eAd1	blízko
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
velrybích	velrybí	k2eAgInPc6d1	velrybí
ostatcích	ostatek	k1gInPc6	ostatek
<g/>
.	.	kIx.	.
<g/>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
složení	složení	k1gNnSc1	složení
potravy	potrava	k1gFnSc2	potrava
megalodonů	megalodon	k1gInPc2	megalodon
se	se	k3xPyFc4	se
měnilo	měnit	k5eAaImAgNnS	měnit
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
a	a	k8xC	a
stanovišti	stanoviště	k1gNnPc7	stanoviště
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
současných	současný	k2eAgMnPc2d1	současný
žraloků	žralok	k1gMnPc2	žralok
bílých	bílý	k2eAgMnPc2d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
megalodoni	megalodon	k1gMnPc1	megalodon
žijící	žijící	k2eAgMnPc1d1	žijící
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
dnešního	dnešní	k2eAgInSc2d1	dnešní
státu	stát	k1gInSc2	stát
Peru	Peru	k1gNnSc2	Peru
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zaměřovali	zaměřovat	k5eAaImAgMnP	zaměřovat
na	na	k7c4	na
lov	lov	k1gInSc4	lov
2,5	[number]	k4	2,5
až	až	k9	až
7	[number]	k4	7
<g/>
metrových	metrový	k2eAgInPc2d1	metrový
cetotheridů	cetotherid	k1gInPc2	cetotherid
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
kořisti	kořist	k1gFnSc2	kořist
menší	malý	k2eAgNnSc4d2	menší
než	než	k8xS	než
byli	být	k5eAaImAgMnP	být
oni	onen	k3xDgMnPc1	onen
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInPc1d2	veliký
druhy	druh	k1gInPc1	druh
velryb	velryba	k1gFnPc2	velryba
zabíjeli	zabíjet	k5eAaImAgMnP	zabíjet
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
jedinci	jedinec	k1gMnPc1	jedinec
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
konzumovali	konzumovat	k5eAaBmAgMnP	konzumovat
spíše	spíše	k9	spíše
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kompetiční	Kompetiční	k2eAgInPc1d1	Kompetiční
vztahy	vztah	k1gInPc1	vztah
===	===	k?	===
</s>
</p>
<p>
<s>
Megalodon	Megalodon	k1gMnSc1	Megalodon
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
vysoce	vysoce	k6eAd1	vysoce
konkurenčním	konkurenční	k2eAgNnSc6d1	konkurenční
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gNnSc4	jeho
postavení	postavení	k1gNnSc4	postavení
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
potravního	potravní	k2eAgInSc2d1	potravní
řetězce	řetězec	k1gInSc2	řetězec
mělo	mít	k5eAaImAgNnS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
strukturu	struktura	k1gFnSc4	struktura
mořských	mořský	k2eAgNnPc2d1	mořské
společenstev	společenstvo	k1gNnPc2	společenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Fosilní	fosilní	k2eAgInPc1d1	fosilní
důkazy	důkaz	k1gInPc1	důkaz
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
existencí	existence	k1gFnSc7	existence
tohoto	tento	k3xDgMnSc2	tento
mořského	mořský	k2eAgMnSc2d1	mořský
predátora	predátor	k1gMnSc2	predátor
a	a	k8xC	a
rozvojem	rozvoj	k1gInSc7	rozvoj
kytovců	kytovec	k1gMnPc2	kytovec
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
velkých	velký	k2eAgMnPc2d1	velký
mořských	mořský	k2eAgMnPc2d1	mořský
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
megalodoni	megalodon	k1gMnPc1	megalodon
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
preferovali	preferovat	k5eAaImAgMnP	preferovat
stanoviště	stanoviště	k1gNnSc4	stanoviště
s	s	k7c7	s
dostatkem	dostatek	k1gInSc7	dostatek
malých	malý	k2eAgInPc2d1	malý
druhů	druh	k1gInPc2	druh
kytovců	kytovec	k1gMnPc2	kytovec
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
dospělci	dospělec	k1gMnPc1	dospělec
vyhledávali	vyhledávat	k5eAaImAgMnP	vyhledávat
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
dostatkem	dostatek	k1gInSc7	dostatek
kytovců	kytovec	k1gMnPc2	kytovec
velkých	velká	k1gFnPc2	velká
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
preference	preference	k1gFnPc1	preference
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
megalodon	megalodon	k1gMnSc1	megalodon
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
oligocénu	oligocén	k1gInSc6	oligocén
<g/>
.	.	kIx.	.
<g/>
Megalodon	Megalodon	k1gMnSc1	Megalodon
koexistoval	koexistovat	k5eAaImAgMnS	koexistovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
ozubenými	ozubený	k2eAgMnPc7d1	ozubený
kytovci	kytovec	k1gMnPc7	kytovec
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
squalodontidé	squalodontidý	k2eAgFnSc3d1	squalodontidý
a	a	k8xC	a
vorvani	vorvaň	k1gMnSc3	vorvaň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
vrcholovými	vrcholový	k2eAgMnPc7d1	vrcholový
predátory	predátor	k1gMnPc7	predátor
<g/>
,	,	kIx,	,
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
tedy	tedy	k9	tedy
s	s	k7c7	s
megalodonem	megalodon	k1gInSc7	megalodon
v	v	k7c6	v
kompetičním	kompetiční	k2eAgInSc6d1	kompetiční
vztahu	vztah	k1gInSc6	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Konkurence	konkurence	k1gFnSc1	konkurence
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
u	u	k7c2	u
ozubených	ozubený	k2eAgInPc2d1	ozubený
evoluční	evoluční	k2eAgInPc4d1	evoluční
závody	závod	k1gInPc4	závod
ve	v	k7c6	v
zbrojení	zbrojení	k1gNnSc6	zbrojení
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
kosatky	kosatka	k1gFnPc1	kosatka
Orcinus	Orcinus	k1gInSc4	Orcinus
citoniensis	citoniensis	k1gFnSc2	citoniensis
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
lovit	lovit	k5eAaImF	lovit
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
narostly	narůst	k5eAaPmAgFnP	narůst
do	do	k7c2	do
obří	obří	k2eAgFnSc2d1	obří
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
kytovec	kytovec	k1gMnSc1	kytovec
Livyatan	Livyatan	k1gInSc4	Livyatan
melvillei	melvillei	k1gNnSc2	melvillei
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měřil	měřit	k5eAaImAgInS	měřit
mezi	mezi	k7c7	mezi
13,5	[number]	k4	13,5
až	až	k9	až
17,5	[number]	k4	17,5
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
miocénu	miocén	k1gInSc2	miocén
<g/>
,	,	kIx,	,
před	před	k7c7	před
asi	asi	k9	asi
11	[number]	k4	11
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
významně	významně	k6eAd1	významně
klesat	klesat	k5eAaImF	klesat
množství	množství	k1gNnSc4	množství
a	a	k8xC	a
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
vorvaňů	vorvaň	k1gMnPc2	vorvaň
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jiní	jiný	k2eAgMnPc1d1	jiný
draví	dravý	k2eAgMnPc1d1	dravý
kytovci	kytovec	k1gMnPc1	kytovec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kosatky	kosatka	k1gFnPc1	kosatka
<g/>
,	,	kIx,	,
během	během	k7c2	během
pliocénu	pliocén	k1gInSc2	pliocén
postupně	postupně	k6eAd1	postupně
vyplňovali	vyplňovat	k5eAaImAgMnP	vyplňovat
tuto	tento	k3xDgFnSc4	tento
uvolněnou	uvolněný	k2eAgFnSc4d1	uvolněná
ekologickou	ekologický	k2eAgFnSc4d1	ekologická
niku	nika	k1gFnSc4	nika
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
fosilních	fosilní	k2eAgInPc2d1	fosilní
důkazů	důkaz	k1gInPc2	důkaz
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
i	i	k9	i
žralok	žralok	k1gMnSc1	žralok
bílý	bílý	k1gMnSc1	bílý
<g/>
,	,	kIx,	,
reagovaly	reagovat	k5eAaBmAgInP	reagovat
na	na	k7c4	na
konkurenční	konkurenční	k2eAgInSc4d1	konkurenční
tlak	tlak	k1gInSc4	tlak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
megalodonů	megalodon	k1gInPc2	megalodon
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
začaly	začít	k5eAaPmAgFnP	začít
obývat	obývat	k5eAaImF	obývat
převážně	převážně	k6eAd1	převážně
chladnější	chladný	k2eAgFnPc1d2	chladnější
vody	voda	k1gFnPc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
společně	společně	k6eAd1	společně
koexistovaly	koexistovat	k5eAaImAgFnP	koexistovat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
během	během	k7c2	během
pliocénu	pliocén	k1gInSc2	pliocén
u	u	k7c2	u
Baja	Baj	k1gInSc2	Baj
California	Californium	k1gNnSc2	Californium
<g/>
,	,	kIx,	,
možná	možná	k9	možná
střídavě	střídavě	k6eAd1	střídavě
žily	žít	k5eAaImAgFnP	žít
během	během	k7c2	během
různých	různý	k2eAgNnPc2d1	různé
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
současné	současný	k2eAgInPc4d1	současný
druhy	druh	k1gInPc4	druh
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
i	i	k8xC	i
megalodon	megalodon	k1gInSc1	megalodon
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
uchyloval	uchylovat	k5eAaImAgMnS	uchylovat
ke	k	k7c3	k
kanibalismu	kanibalismus	k1gInSc3	kanibalismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lovecké	lovecký	k2eAgFnPc4d1	lovecká
strategie	strategie	k1gFnPc4	strategie
===	===	k?	===
</s>
</p>
<p>
<s>
Žraloci	žralok	k1gMnPc1	žralok
často	často	k6eAd1	často
využívají	využívat	k5eAaImIp3nP	využívat
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
velké	velký	k2eAgFnSc2d1	velká
kořisti	kořist	k1gFnSc2	kořist
různých	různý	k2eAgInPc2d1	různý
způsobů	způsob	k1gInPc2	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Megalodon	Megalodon	k1gMnSc1	Megalodon
mohl	moct	k5eAaImAgMnS	moct
používat	používat	k5eAaImF	používat
podobné	podobný	k2eAgFnSc2d1	podobná
lovecké	lovecký	k2eAgFnSc2d1	lovecká
techniky	technika	k1gFnSc2	technika
jako	jako	k8xS	jako
současný	současný	k2eAgMnSc1d1	současný
žralok	žralok	k1gMnSc1	žralok
bílý	bílý	k1gMnSc1	bílý
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
otisků	otisk	k1gInPc2	otisk
zubů	zub	k1gInPc2	zub
ve	v	k7c6	v
velrybích	velrybí	k2eAgFnPc6d1	velrybí
fosiliích	fosilie	k1gFnPc6	fosilie
se	se	k3xPyFc4	se
však	však	k9	však
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zabít	zabít	k5eAaPmF	zabít
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
využíval	využívat	k5eAaPmAgMnS	využívat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
současný	současný	k2eAgMnSc1d1	současný
bílý	bílý	k1gMnSc1	bílý
žralok	žralok	k1gMnSc1	žralok
<g/>
.	.	kIx.	.
<g/>
Jedním	jeden	k4xCgInSc7	jeden
konkrétním	konkrétní	k2eAgInSc7d1	konkrétní
exemplářem	exemplář	k1gInSc7	exemplář
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
devítimetrový	devítimetrový	k2eAgInSc1d1	devítimetrový
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
miocénní	miocénní	k2eAgFnSc2d1	miocénní
kosticovité	kosticovitý	k2eAgFnSc2d1	kosticovitý
velryby	velryba	k1gFnSc2	velryba
neznámého	známý	k2eNgInSc2d1	neznámý
taxonu	taxon	k1gInSc2	taxon
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
lze	lze	k6eAd1	lze
analyzovat	analyzovat	k5eAaImF	analyzovat
útočné	útočný	k2eAgNnSc4d1	útočné
chování	chování	k1gNnSc4	chování
megalodona	megalodon	k1gMnSc2	megalodon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
žraloků	žralok	k1gMnPc2	žralok
bílých	bílý	k2eAgMnPc2d1	bílý
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
břicho	břicho	k1gNnSc4	břicho
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
megalodon	megalodon	k1gMnSc1	megalodon
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
útočil	útočit	k5eAaImAgMnS	útočit
na	na	k7c4	na
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
plíce	plíce	k1gFnPc4	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
silnými	silný	k2eAgInPc7d1	silný
zuby	zub	k1gInPc7	zub
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterých	který	k3yIgFnPc2	který
uměl	umět	k5eAaImAgMnS	umět
překousnout	překousnout	k5eAaPmF	překousnout
i	i	k9	i
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
kosti	kost	k1gFnPc4	kost
<g/>
,	,	kIx,	,
prokousl	prokousnout	k5eAaPmAgInS	prokousnout
kořisti	kořist	k1gFnSc2	kořist
hrudní	hrudní	k2eAgInSc1d1	hrudní
koš	koš	k1gInSc1	koš
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
odolné	odolný	k2eAgFnPc1d1	odolná
kosti	kost	k1gFnPc1	kost
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
útoku	útok	k1gInSc2	útok
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
také	také	k9	také
odlišovat	odlišovat	k5eAaImF	odlišovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
velikosti	velikost	k1gFnSc2	velikost
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
fosilní	fosilní	k2eAgInPc1d1	fosilní
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
menších	malý	k2eAgMnPc2d2	menší
kytovců	kytovec	k1gMnPc2	kytovec
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
zástupci	zástupce	k1gMnPc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
Cetotheriidae	Cetotheriida	k1gFnSc2	Cetotheriida
<g/>
,	,	kIx,	,
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
nich	on	k3xPp3gFnPc2	on
megalodon	megalodona	k1gFnPc2	megalodona
prudce	prudko	k6eAd1	prudko
narazil	narazit	k5eAaPmAgMnS	narazit
zespodu	zespodu	k6eAd1	zespodu
a	a	k8xC	a
tak	tak	k9	tak
jim	on	k3xPp3gMnPc3	on
způsobil	způsobit	k5eAaPmAgMnS	způsobit
kompresní	kompresní	k2eAgFnSc4d1	kompresní
zlomeninu	zlomenina	k1gFnSc4	zlomenina
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
pliocénu	pliocén	k1gInSc2	pliocén
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
větší	veliký	k2eAgInPc1d2	veliký
a	a	k8xC	a
vyspělejší	vyspělý	k2eAgInPc1d2	vyspělejší
druhy	druh	k1gInPc1	druh
kytovců	kytovec	k1gMnPc2	kytovec
<g/>
.	.	kIx.	.
</s>
<s>
Megalodon	Megalodon	k1gMnSc1	Megalodon
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
asi	asi	k9	asi
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
nové	nový	k2eAgFnPc4d1	nová
lovecké	lovecký	k2eAgFnPc4d1	lovecká
strategie	strategie	k1gFnPc4	strategie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tyto	tento	k3xDgFnPc4	tento
větší	veliký	k2eAgFnPc4d2	veliký
velryby	velryba	k1gFnPc4	velryba
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
nalezena	naleznout	k5eAaPmNgFnS	naleznout
řada	řada	k1gFnSc1	řada
zkamenělých	zkamenělý	k2eAgFnPc2d1	zkamenělá
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
kdysi	kdysi	k6eAd1	kdysi
tvořily	tvořit	k5eAaImAgFnP	tvořit
prsní	prsní	k2eAgFnPc1d1	prsní
ploutve	ploutev	k1gFnPc1	ploutev
či	či	k8xC	či
ocasní	ocasní	k2eAgInPc1d1	ocasní
obratle	obratel	k1gInPc1	obratel
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
pliocénním	pliocénní	k2eAgNnSc7d1	pliocénní
druhům	druh	k1gMnPc3	druh
velryb	velryba	k1gFnPc2	velryba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
pozůstatcích	pozůstatek	k1gInPc6	pozůstatek
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
otisky	otisk	k1gInPc1	otisk
po	po	k7c6	po
zubech	zub	k1gInPc6	zub
megalodonů	megalodon	k1gInPc2	megalodon
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgMnSc1	tento
žralok	žralok	k1gMnSc1	žralok
znehybnil	znehybnit	k5eAaPmAgMnS	znehybnit
větší	veliký	k2eAgFnSc2d2	veliký
velryby	velryba	k1gFnSc2	velryba
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
poškodil	poškodit	k5eAaPmAgInS	poškodit
ploutve	ploutev	k1gFnPc4	ploutev
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
potom	potom	k6eAd1	potom
je	být	k5eAaImIp3nS	být
zabil	zabít	k5eAaPmAgMnS	zabít
a	a	k8xC	a
sežral	sežrat	k5eAaPmAgMnS	sežrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
mláďat	mládě	k1gNnPc2	mládě
===	===	k?	===
</s>
</p>
<p>
<s>
Mláďata	mládě	k1gNnPc1	mládě
megalodona	megalodona	k1gFnSc1	megalodona
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
současných	současný	k2eAgMnPc2d1	současný
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
žila	žít	k5eAaImAgFnS	žít
ve	v	k7c6	v
"	"	kIx"	"
<g/>
školkách	školka	k1gFnPc6	školka
<g/>
"	"	kIx"	"
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jim	on	k3xPp3gMnPc3	on
zároveň	zároveň	k6eAd1	zároveň
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c4	před
predátory	predátor	k1gMnPc4	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
fosilních	fosilní	k2eAgInPc2d1	fosilní
důkazů	důkaz	k1gInPc2	důkaz
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
areály	areál	k1gInPc1	areál
nácházely	nácházet	k5eAaImAgInP	nácházet
v	v	k7c6	v
teplých	teplý	k2eAgFnPc6d1	teplá
vodách	voda	k1gFnPc6	voda
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
jak	jak	k6eAd1	jak
méně	málo	k6eAd2	málo
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
dostatek	dostatek	k1gInSc4	dostatek
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
například	například	k6eAd1	například
v	v	k7c6	v
panamské	panamský	k2eAgFnSc6d1	Panamská
formaci	formace	k1gFnSc6	formace
Gatun	Gatun	k1gInSc4	Gatun
<g/>
,	,	kIx,	,
v	v	k7c6	v
marylandské	marylandský	k2eAgFnSc6d1	marylandská
formaci	formace	k1gFnSc6	formace
Calvert	Calvert	k1gInSc4	Calvert
<g/>
,	,	kIx,	,
na	na	k7c6	na
Kanárských	kanárský	k2eAgInPc6d1	kanárský
ostrovech	ostrov	k1gInPc6	ostrov
či	či	k8xC	či
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
(	(	kIx(	(
<g/>
souvrství	souvrství	k1gNnSc1	souvrství
Bone	bon	k1gInSc5	bon
Valley	Valle	k2eAgInPc1d1	Valle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
existující	existující	k2eAgMnPc1d1	existující
lamnovití	lamnovitý	k2eAgMnPc1d1	lamnovitý
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
megalodon	megalodon	k1gMnSc1	megalodon
někdy	někdy	k6eAd1	někdy
řadí	řadit	k5eAaImIp3nS	řadit
<g/>
)	)	kIx)	)
rodí	rodit	k5eAaImIp3nS	rodit
živá	živý	k2eAgNnPc4d1	živé
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
i	i	k9	i
u	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
živorodost	živorodost	k1gFnSc1	živorodost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedněch	jeden	k4xCgInPc2	jeden
odhadů	odhad	k1gInPc2	odhad
mláďata	mládě	k1gNnPc4	mládě
měřila	měřit	k5eAaImAgFnS	měřit
asi	asi	k9	asi
3,5	[number]	k4	3,5
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
asi	asi	k9	asi
dvoumetrová	dvoumetrový	k2eAgFnSc1d1	dvoumetrová
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
je	on	k3xPp3gMnPc4	on
mohlo	moct	k5eAaImAgNnS	moct
vystavovat	vystavovat	k5eAaImF	vystavovat
hrozbě	hrozba	k1gFnSc3	hrozba
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
kladivoun	kladivoun	k1gMnSc1	kladivoun
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Sphyrna	Sphyrna	k1gFnSc1	Sphyrna
mokarran	mokarrana	k1gFnPc2	mokarrana
<g/>
)	)	kIx)	)
či	či	k8xC	či
vyhynulý	vyhynulý	k2eAgInSc4d1	vyhynulý
miocénní	miocénní	k2eAgInSc4d1	miocénní
druh	druh	k1gInSc4	druh
Hemipristis	Hemipristis	k1gFnSc2	Hemipristis
serra	serr	k1gInSc2	serr
<g/>
.	.	kIx.	.
</s>
<s>
Potrava	potrava	k1gFnSc1	potrava
mláďat	mládě	k1gNnPc2	mládě
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
měnila	měnit	k5eAaImAgFnS	měnit
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
lovila	lovit	k5eAaImAgFnS	lovit
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgFnPc4d1	mořská
želvy	želva	k1gFnPc4	želva
<g/>
,	,	kIx,	,
dugongy	dugong	k1gMnPc4	dugong
<g/>
,	,	kIx,	,
či	či	k8xC	či
malé	malý	k2eAgMnPc4d1	malý
kytovce	kytovec	k1gMnPc4	kytovec
<g/>
,	,	kIx,	,
na	na	k7c4	na
větší	veliký	k2eAgFnPc4d2	veliký
velryby	velryba	k1gFnPc4	velryba
se	se	k3xPyFc4	se
přeorientovala	přeorientovat	k5eAaPmAgFnS	přeorientovat
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
<g/>
.	.	kIx.	.
<g/>
Nicméně	nicméně	k8xC	nicméně
ve	v	k7c6	v
fosilních	fosilní	k2eAgInPc6d1	fosilní
pozůstatcích	pozůstatek	k1gInPc6	pozůstatek
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
i	i	k9	i
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mladí	mladý	k2eAgMnPc1d1	mladý
megalodoni	megalodon	k1gMnPc1	megalodon
občas	občas	k6eAd1	občas
napadli	napadnout	k5eAaPmAgMnP	napadnout
i	i	k9	i
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgMnPc4d2	veliký
kytovce	kytovec	k1gMnPc4	kytovec
než	než	k8xS	než
byli	být	k5eAaImAgMnP	být
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
na	na	k7c6	na
žebrech	žebr	k1gInPc6	žebr
pliocéního	pliocéní	k1gMnSc2	pliocéní
keporkaka	keporkak	k1gMnSc2	keporkak
či	či	k8xC	či
plejtváka	plejtvák	k1gMnSc2	plejtvák
byly	být	k5eAaImAgFnP	být
objeveny	objeven	k2eAgFnPc1d1	objevena
kousance	kousanec	k1gInPc4	kousanec
od	od	k7c2	od
4	[number]	k4	4
až	až	k9	až
7	[number]	k4	7
<g/>
metrového	metrový	k2eAgMnSc2d1	metrový
žraloka	žralok	k1gMnSc2	žralok
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vykazovaly	vykazovat	k5eAaImAgFnP	vykazovat
známky	známka	k1gFnPc4	známka
hojení	hojení	k1gNnSc2	hojení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
velrybě	velryba	k1gFnSc3	velryba
toto	tento	k3xDgNnSc4	tento
zranění	zranění	k1gNnSc4	zranění
způsobil	způsobit	k5eAaPmAgMnS	způsobit
nedospělý	nedospělý	k1gMnSc1	nedospělý
megalodon	megalodon	k1gMnSc1	megalodon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyhynutí	vyhynutí	k1gNnSc2	vyhynutí
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Klimatické	klimatický	k2eAgFnPc1d1	klimatická
změny	změna	k1gFnPc1	změna
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
žil	žít	k5eAaImAgMnS	žít
megalodon	megalodon	k1gMnSc1	megalodon
<g/>
,	,	kIx,	,
Zemi	zem	k1gFnSc4	zem
postihla	postihnout	k5eAaPmAgFnS	postihnout
řada	řada	k1gFnSc1	řada
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
mořský	mořský	k2eAgInSc4d1	mořský
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Postupné	postupný	k2eAgNnSc1d1	postupné
ochlazování	ochlazování	k1gNnSc1	ochlazování
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
oligocénu	oligocén	k1gInSc6	oligocén
před	před	k7c7	před
asi	asi	k9	asi
35	[number]	k4	35
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
zamrznutí	zamrznutí	k1gNnSc3	zamrznutí
pólů	pól	k1gInPc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Geologické	geologický	k2eAgFnPc1d1	geologická
události	událost	k1gFnPc1	událost
změnily	změnit	k5eAaPmAgFnP	změnit
pohyb	pohyb	k1gInSc4	pohyb
mořských	mořský	k2eAgInPc2d1	mořský
proudů	proud	k1gInPc2	proud
a	a	k8xC	a
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
rovníkové	rovníkový	k2eAgFnSc2d1	Rovníková
cirkumglobální	cirkumglobální	k2eAgFnSc2d1	cirkumglobální
vodní	vodní	k2eAgFnSc2d1	vodní
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
Amerikami	Amerika	k1gFnPc7	Amerika
a	a	k8xC	a
napříč	napříč	k7c7	napříč
mořem	moře	k1gNnSc7	moře
Tethys	Tethysa	k1gFnPc2	Tethysa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
ochlazování	ochlazování	k1gNnSc3	ochlazování
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Zastavením	zastavení	k1gNnSc7	zastavení
Golfského	golfský	k2eAgInSc2d1	golfský
proudu	proud	k1gInSc2	proud
přestala	přestat	k5eAaPmAgFnS	přestat
zásobovat	zásobovat	k5eAaImF	zásobovat
na	na	k7c4	na
živiny	živina	k1gFnPc4	živina
bohatá	bohatý	k2eAgFnSc1d1	bohatá
voda	voda	k1gFnSc1	voda
velké	velká	k1gFnSc2	velká
mořské	mořský	k2eAgInPc1d1	mořský
ekosystémy	ekosystém	k1gInPc1	ekosystém
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mohlo	moct	k5eAaImAgNnS	moct
způsobit	způsobit	k5eAaPmF	způsobit
úbytek	úbytek	k1gInSc4	úbytek
megalodonovy	megalodonův	k2eAgFnSc2d1	megalodonův
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
megalodon	megalodon	k1gMnSc1	megalodon
možná	možná	k9	možná
nevyskytoval	vyskytovat	k5eNaImAgMnS	vyskytovat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
chladnější	chladný	k2eAgFnSc7d2	chladnější
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
udržet	udržet	k5eAaPmF	udržet
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
metabolického	metabolický	k2eAgNnSc2d1	metabolické
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
areál	areál	k1gInSc1	areál
výskytu	výskyt	k1gInSc2	výskyt
mohl	moct	k5eAaImAgInS	moct
zmenšit	zmenšit	k5eAaPmF	zmenšit
na	na	k7c6	na
stále	stále	k6eAd1	stále
ubývající	ubývající	k2eAgFnSc6d1	ubývající
oblasti	oblast	k1gFnSc6	oblast
s	s	k7c7	s
teplou	teplý	k2eAgFnSc7d1	teplá
vodou	voda	k1gFnSc7	voda
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
fosilních	fosilní	k2eAgInPc2d1	fosilní
důkazů	důkaz	k1gInPc2	důkaz
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
pliocénu	pliocén	k1gInSc6	pliocén
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
výrazně	výrazně	k6eAd1	výrazně
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
megalodon	megalodon	k1gMnSc1	megalodon
vymizel	vymizet	k5eAaPmAgMnS	vymizet
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
oblastí	oblast	k1gFnPc2	oblast
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
pliocénem	pliocén	k1gInSc7	pliocén
až	až	k8xS	až
pleistocénem	pleistocén	k1gInSc7	pleistocén
před	před	k7c7	před
5	[number]	k4	5
miliony	milion	k4xCgInPc7	milion
až	až	k8xS	až
12	[number]	k4	12
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
kolísání	kolísání	k1gNnSc3	kolísání
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
ledovců	ledovec	k1gInPc2	ledovec
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
negativní	negativní	k2eAgInSc4d1	negativní
dopad	dopad	k1gInSc4	dopad
na	na	k7c6	na
pobřežní	pobřežní	k2eAgFnSc6d1	pobřežní
oblasti	oblast	k1gFnSc6	oblast
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
mohla	moct	k5eAaImAgFnS	moct
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
vyhynutí	vyhynutí	k1gNnSc3	vyhynutí
megalodona	megalodon	k1gMnSc2	megalodon
a	a	k8xC	a
několika	několik	k4yIc2	několik
jiných	jiný	k2eAgFnPc2d1	jiná
velkých	velká	k1gFnPc2	velká
mořských	mořský	k2eAgInPc2d1	mořský
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
mohly	moct	k5eAaImAgFnP	moct
způsobit	způsobit	k5eAaPmF	způsobit
úbytek	úbytek	k1gInSc4	úbytek
vhodných	vhodný	k2eAgFnPc2d1	vhodná
mělčin	mělčina	k1gFnPc2	mělčina
s	s	k7c7	s
teplou	teplý	k2eAgFnSc7d1	teplá
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
mláďata	mládě	k1gNnPc4	mládě
<g/>
;	;	kIx,	;
oblastí	oblast	k1gFnPc2	oblast
klíčových	klíčový	k2eAgFnPc2d1	klíčová
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
řady	řada	k1gFnSc2	řada
druhů	druh	k1gInPc2	druh
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
i	i	k8xC	i
proto	proto	k8xC	proto
že	že	k8xS	že
chrání	chránit	k5eAaImIp3nS	chránit
jejich	jejich	k3xOp3gNnPc4	jejich
mláďata	mládě	k1gNnPc4	mládě
před	před	k7c4	před
útoky	útok	k1gInPc4	útok
predátorů	predátor	k1gMnPc2	predátor
<g/>
.	.	kIx.	.
<g/>
Nicméně	nicméně	k8xC	nicméně
analýza	analýza	k1gFnSc1	analýza
výskytu	výskyt	k1gInSc2	výskyt
megalodona	megalodona	k1gFnSc1	megalodona
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
změna	změna	k1gFnSc1	změna
teploty	teplota	k1gFnSc2	teplota
nehrála	hrát	k5eNaImAgFnS	hrát
přímou	přímý	k2eAgFnSc4d1	přímá
roli	role	k1gFnSc4	role
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
vyhynutí	vyhynutí	k1gNnSc6	vyhynutí
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
rozšíření	rozšíření	k1gNnSc1	rozšíření
během	během	k7c2	během
miocénu	miocén	k1gInSc2	miocén
a	a	k8xC	a
pliocénu	pliocén	k1gInSc2	pliocén
nekoreluje	korelovat	k5eNaImIp3nS	korelovat
s	s	k7c7	s
oteplováním	oteplování	k1gNnSc7	oteplování
a	a	k8xC	a
ochlazováním	ochlazování	k1gNnSc7	ochlazování
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přestože	přestože	k8xS	přestože
populace	populace	k1gFnSc1	populace
megalodonů	megalodon	k1gMnPc2	megalodon
během	během	k7c2	během
pliocénu	pliocén	k1gInSc2	pliocén
klesala	klesat	k5eAaImAgFnS	klesat
<g/>
,	,	kIx,	,
žralok	žralok	k1gMnSc1	žralok
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
schopen	schopen	k2eAgMnSc1d1	schopen
obývat	obývat	k5eAaImF	obývat
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
než	než	k8xS	než
tropické	tropický	k2eAgFnPc1d1	tropická
oblasti	oblast	k1gFnPc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
fosilie	fosilie	k1gFnPc1	fosilie
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
panovaly	panovat	k5eAaImAgFnP	panovat
průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
mezi	mezi	k7c7	mezi
12	[number]	k4	12
až	až	k9	až
27	[number]	k4	27
<g/>
°	°	k?	°
C	C	kA	C
<g/>
,	,	kIx,	,
s	s	k7c7	s
celkovým	celkový	k2eAgNnSc7d1	celkové
rozpětím	rozpětí	k1gNnSc7	rozpětí
teplot	teplota	k1gFnPc2	teplota
1	[number]	k4	1
až	až	k9	až
33	[number]	k4	33
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozsah	rozsah	k1gInSc1	rozsah
vodních	vodní	k2eAgNnPc2d1	vodní
stanovišť	stanoviště	k1gNnPc2	stanoviště
nebyl	být	k5eNaImAgInS	být
výrazně	výrazně	k6eAd1	výrazně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
změnami	změna	k1gFnPc7	změna
teploty	teplota	k1gFnPc4	teplota
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
rovněž	rovněž	k9	rovněž
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgMnSc1	tento
žralok	žralok	k1gMnSc1	žralok
byl	být	k5eAaImAgMnS	být
mezotermní	mezotermní	k2eAgMnSc1d1	mezotermní
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
spekulací	spekulace	k1gFnPc2	spekulace
mohli	moct	k5eAaImAgMnP	moct
tito	tento	k3xDgMnPc1	tento
obří	obří	k2eAgMnPc1d1	obří
žraloci	žralok	k1gMnPc1	žralok
i	i	k8xC	i
další	další	k2eAgMnPc1d1	další
velcí	velký	k2eAgMnPc1d1	velký
živočichové	živočich	k1gMnPc1	živočich
vyhynout	vyhynout	k5eAaPmF	vyhynout
asi	asi	k9	asi
před	před	k7c7	před
2,6	[number]	k4	2,6
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
vinou	vinou	k7c2	vinou
exploze	exploze	k1gFnSc2	exploze
supernovy	supernova	k1gFnSc2	supernova
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
asi	asi	k9	asi
150	[number]	k4	150
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
novější	nový	k2eAgNnSc1d2	novější
datování	datování	k1gNnSc1	datování
vymření	vymření	k1gNnSc2	vymření
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
před	před	k7c7	před
3,6	[number]	k4	3,6
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
vyvrací	vyvracet	k5eAaImIp3nS	vyvracet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Změny	změna	k1gFnSc2	změna
ekosystému	ekosystém	k1gInSc2	ekosystém
===	===	k?	===
</s>
</p>
<p>
<s>
Mořští	mořský	k2eAgMnPc1d1	mořský
savci	savec	k1gMnPc1	savec
dosahovali	dosahovat	k5eAaImAgMnP	dosahovat
největší	veliký	k2eAgFnPc4d3	veliký
diverzity	diverzita	k1gFnPc4	diverzita
během	během	k7c2	během
miocénu	miocén	k1gInSc2	miocén
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
kosticovců	kosticovec	k1gInPc2	kosticovec
tehdy	tehdy	k6eAd1	tehdy
existovalo	existovat	k5eAaImAgNnS	existovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvacet	dvacet	k4xCc4	dvacet
rodů	rod	k1gInPc2	rod
<g/>
;	;	kIx,	;
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
jich	on	k3xPp3gNnPc2	on
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc1	šest
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
kořisti	kořist	k1gFnSc2	kořist
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
superpredátory	superpredátor	k1gMnPc4	superpredátor
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
řadil	řadit	k5eAaImAgMnS	řadit
i	i	k9	i
megalodon	megalodon	k1gMnSc1	megalodon
<g/>
,	,	kIx,	,
ideální	ideální	k2eAgMnSc1d1	ideální
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
období	období	k1gNnSc2	období
miocénu	miocén	k1gInSc2	miocén
však	však	k9	však
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
kosticovců	kosticovec	k1gMnPc2	kosticovec
vyhynulo	vyhynout	k5eAaPmAgNnS	vyhynout
a	a	k8xC	a
žijící	žijící	k2eAgInPc1d1	žijící
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
v	v	k7c6	v
rychlejší	rychlý	k2eAgFnSc6d2	rychlejší
plavce	plavka	k1gFnSc6	plavka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ztěžovalo	ztěžovat	k5eAaImAgNnS	ztěžovat
jejich	jejich	k3xOp3gFnSc4	jejich
ulovení	ulovení	k1gNnSc1	ulovení
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
průlivu	průliv	k1gInSc2	průliv
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
a	a	k8xC	a
četnost	četnost	k1gFnSc4	četnost
tropických	tropický	k2eAgInPc2d1	tropický
druhů	druh	k1gInPc2	druh
velryb	velryba	k1gFnPc2	velryba
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
vyhynutí	vyhynutí	k1gNnSc2	vyhynutí
megalodona	megalodon	k1gMnSc2	megalodon
koreluje	korelovat	k5eAaImIp3nS	korelovat
s	s	k7c7	s
poklesem	pokles	k1gInSc7	pokles
počtu	počet	k1gInSc2	počet
mnoha	mnoho	k4c2	mnoho
vývojových	vývojový	k2eAgFnPc2d1	vývojová
linií	linie	k1gFnPc2	linie
kosticovců	kosticovec	k1gInPc2	kosticovec
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
na	na	k7c6	na
zdroji	zdroj	k1gInSc6	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
závislý	závislý	k2eAgInSc1d1	závislý
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
během	během	k7c2	během
pliocénu	pliocén	k1gInSc2	pliocén
vyhynulo	vyhynout	k5eAaPmAgNnS	vyhynout
36	[number]	k4	36
%	%	kIx~	%
velkých	velký	k2eAgInPc2d1	velký
mořských	mořský	k2eAgInPc2d1	mořský
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
55	[number]	k4	55
%	%	kIx~	%
mořských	mořský	k2eAgMnPc2d1	mořský
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
35	[number]	k4	35
%	%	kIx~	%
mořských	mořský	k2eAgMnPc2d1	mořský
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
9	[number]	k4	9
%	%	kIx~	%
žraloků	žralok	k1gMnPc2	žralok
a	a	k8xC	a
43	[number]	k4	43
%	%	kIx~	%
mořských	mořský	k2eAgFnPc2d1	mořská
želv	želva	k1gFnPc2	želva
<g/>
.	.	kIx.	.
</s>
<s>
Vyhynutí	vyhynutí	k1gNnSc1	vyhynutí
postihlo	postihnout	k5eAaPmAgNnS	postihnout
především	především	k9	především
živočichy	živočich	k1gMnPc4	živočich
endotermní	endotermní	k2eAgFnSc1d1	endotermní
a	a	k8xC	a
mezotermní	mezotermní	k2eAgFnSc1d1	mezotermní
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
nedostatek	nedostatek	k1gInSc4	nedostatek
potravy	potrava	k1gFnSc2	potrava
(	(	kIx(	(
<g/>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
podporuje	podporovat	k5eAaImIp3nS	podporovat
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
megalodon	megalodon	k1gInSc1	megalodon
byl	být	k5eAaImAgInS	být
mezotermní	mezotermní	k2eAgMnSc1d1	mezotermní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obřímu	obří	k2eAgMnSc3d1	obří
žralokovi	žralok	k1gMnSc3	žralok
nemusely	muset	k5eNaImAgInP	muset
stačit	stačit	k5eAaBmF	stačit
ztenčující	ztenčující	k2eAgFnSc4d1	ztenčující
se	se	k3xPyFc4	se
zdroje	zdroj	k1gInSc2	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Ochlazování	ochlazování	k1gNnSc1	ochlazování
během	během	k7c2	během
pliocénu	pliocén	k1gInSc2	pliocén
mu	on	k3xPp3gMnSc3	on
pak	pak	k6eAd1	pak
mohlo	moct	k5eAaImAgNnS	moct
zabránit	zabránit	k5eAaPmF	zabránit
v	v	k7c6	v
přístupu	přístup	k1gInSc6	přístup
do	do	k7c2	do
polárních	polární	k2eAgFnPc2d1	polární
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
stěhovaly	stěhovat	k5eAaImAgFnP	stěhovat
velké	velký	k2eAgFnPc1d1	velká
velryby	velryba	k1gFnPc1	velryba
<g/>
.	.	kIx.	.
<g/>
Rovněž	rovněž	k9	rovněž
konkurence	konkurence	k1gFnSc1	konkurence
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
nových	nový	k2eAgMnPc2d1	nový
predátorů	predátor	k1gMnPc2	predátor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
vorvaňů	vorvaň	k1gMnPc2	vorvaň
v	v	k7c6	v
miocénu	miocén	k1gInSc6	miocén
<g/>
,	,	kIx,	,
a	a	k8xC	a
kosatek	kosatka	k1gFnPc2	kosatka
a	a	k8xC	a
žraloků	žralok	k1gMnPc2	žralok
bílých	bílý	k2eAgMnPc2d1	bílý
v	v	k7c6	v
pliocénu	pliocén	k1gInSc6	pliocén
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
stavů	stav	k1gInPc2	stav
megalodona	megalodon	k1gMnSc2	megalodon
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vyvinutí	vyvinutý	k2eAgMnPc1d1	vyvinutý
kytovci	kytovec	k1gMnPc1	kytovec
živící	živící	k2eAgFnSc4d1	živící
se	se	k3xPyFc4	se
jinými	jiný	k2eAgFnPc7d1	jiná
velrybami	velryba	k1gFnPc7	velryba
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
mohli	moct	k5eAaImAgMnP	moct
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
i	i	k9	i
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
rozšiřující	rozšiřující	k2eAgFnSc7d1	rozšiřující
studenou	studený	k2eAgFnSc7d1	studená
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
obývali	obývat	k5eAaImAgMnP	obývat
i	i	k9	i
tropické	tropický	k2eAgFnPc4d1	tropická
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
<g/>
Vyhynutí	vyhynutí	k1gNnSc1	vyhynutí
megalodona	megalodona	k1gFnSc1	megalodona
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
další	další	k2eAgFnPc4d1	další
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
mořských	mořský	k2eAgNnPc6d1	mořské
společenstvech	společenstvo	k1gNnPc6	společenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
zvětšili	zvětšit	k5eAaPmAgMnP	zvětšit
kosticovci	kosticovec	k1gMnPc1	kosticovec
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
možná	možná	k9	možná
kvůli	kvůli	k7c3	kvůli
klimatickým	klimatický	k2eAgFnPc3d1	klimatická
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvětšení	zvětšení	k1gNnSc1	zvětšení
velikosti	velikost	k1gFnSc2	velikost
kosticovců	kosticovec	k1gInPc2	kosticovec
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
vyhynutí	vyhynutí	k1gNnSc1	vyhynutí
megalodona	megalodona	k1gFnSc1	megalodona
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ten	ten	k3xDgMnSc1	ten
zabíjel	zabíjet	k5eAaImAgMnS	zabíjet
převážně	převážně	k6eAd1	převážně
menší	malý	k2eAgInPc4d2	menší
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vyhynutí	vyhynutí	k1gNnSc1	vyhynutí
megalodona	megalodona	k1gFnSc1	megalodona
mělo	mít	k5eAaImAgNnS	mít
také	také	k9	také
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
další	další	k2eAgMnPc4d1	další
velké	velký	k2eAgMnPc4d1	velký
predátory	predátor	k1gMnPc4	predátor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
na	na	k7c4	na
velkého	velký	k2eAgMnSc4d1	velký
bílého	bílý	k2eAgMnSc4d1	bílý
žraloka	žralok	k1gMnSc4	žralok
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
obsadil	obsadit	k5eAaPmAgMnS	obsadit
oblasti	oblast	k1gFnSc2	oblast
uvolněné	uvolněný	k2eAgFnSc2d1	uvolněná
megalodonem	megalodon	k1gInSc7	megalodon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
kryptozoologii	kryptozoologie	k1gFnSc6	kryptozoologie
==	==	k?	==
</s>
</p>
<p>
<s>
Megalodon	Megalodon	k1gInSc1	Megalodon
je	být	k5eAaImIp3nS	být
postavou	postava	k1gFnSc7	postava
některých	některý	k3yIgInPc2	některý
děl	dít	k5eAaImAgInS	dít
včetně	včetně	k7c2	včetně
filmů	film	k1gInPc2	film
a	a	k8xC	a
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zobrazován	zobrazován	k2eAgInSc1d1	zobrazován
jako	jako	k8xC	jako
mořská	mořský	k2eAgFnSc1d1	mořská
příšera	příšera	k1gFnSc1	příšera
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
jedinci	jedinec	k1gMnPc1	jedinec
(	(	kIx(	(
<g/>
dva	dva	k4xCgMnPc1	dva
dospělci	dospělec	k1gMnPc1	dospělec
a	a	k8xC	a
mládě	mládě	k1gNnSc1	mládě
<g/>
)	)	kIx)	)
megalodona	megalodon	k1gMnSc4	megalodon
byli	být	k5eAaImAgMnP	být
představeni	představen	k2eAgMnPc1d1	představen
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
BBC	BBC	kA	BBC
Putování	putování	k1gNnSc1	putování
s	s	k7c7	s
dinosaury	dinosaurus	k1gMnPc7	dinosaurus
<g/>
:	:	kIx,	:
Monstra	monstrum	k1gNnSc2	monstrum
pravěkých	pravěký	k2eAgInPc2d1	pravěký
oceánů	oceán	k1gInPc2	oceán
s	s	k7c7	s
Nigelem	Nigel	k1gMnSc7	Nigel
Marvenem	Marven	k1gMnSc7	Marven
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
popisován	popisován	k2eAgMnSc1d1	popisován
jako	jako	k8xC	jako
vrcholový	vrcholový	k2eAgMnSc1d1	vrcholový
predátor	predátor	k1gMnSc1	predátor
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dokumentárním	dokumentární	k2eAgInSc6d1	dokumentární
cyklu	cyklus	k1gInSc6	cyklus
Jurské	jurský	k2eAgNnSc4d1	jurské
bojiště	bojiště	k1gNnSc4	bojiště
byl	být	k5eAaImAgInS	být
zobrazen	zobrazen	k2eAgInSc1d1	zobrazen
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
velryby	velryba	k1gFnPc4	velryba
Brygmophyseter	Brygmophysetra	k1gFnPc2	Brygmophysetra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
součástí	součást	k1gFnSc7	součást
některých	některý	k3yIgInPc2	některý
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Žralok	žralok	k1gMnSc1	žralok
útočí	útočit	k5eAaImIp3nS	útočit
3	[number]	k4	3
<g/>
:	:	kIx,	:
Lidožrout	lidožrout	k1gMnSc1	lidožrout
<g/>
,	,	kIx,	,
Megažralok	Megažralok	k1gInSc1	Megažralok
vs	vs	k?	vs
<g/>
.	.	kIx.	.
obří	obří	k2eAgFnSc1d1	obří
chobotnice	chobotnice	k1gFnSc1	chobotnice
<g/>
,	,	kIx,	,
Megažralok	Megažralok	k1gInSc1	Megažralok
versus	versus	k7c1	versus
crocosaurus	crocosaurus	k1gInSc1	crocosaurus
či	či	k8xC	či
DCU	DCU	kA	DCU
<g/>
:	:	kIx,	:
Liga	liga	k1gFnSc1	liga
spravedlivých	spravedlivý	k2eAgFnPc2d1	spravedlivá
<g/>
:	:	kIx,	:
Trůn	trůn	k1gInSc1	trůn
Atlantidy	Atlantida	k1gFnSc2	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
příbězích	příběh	k1gInPc6	příběh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Tedford	Tedford	k1gInSc1	Tedford
and	and	k?	and
the	the	k?	the
Megalodon	Megalodon	k1gInSc1	Megalodon
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vykresleno	vykreslen	k2eAgNnSc4d1	vykresleno
znovuobjevení	znovuobjevení	k1gNnSc4	znovuobjevení
žraloka	žralok	k1gMnSc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
Meg	Meg	k1gFnSc2	Meg
<g/>
:	:	kIx,	:
Teror	teror	k1gInSc1	teror
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hlubin	hlubina	k1gFnPc2	hlubina
od	od	k7c2	od
Stevea	Steveus	k1gMnSc2	Steveus
Altena	Alten	k1gMnSc2	Alten
je	být	k5eAaImIp3nS	být
žralok	žralok	k1gMnSc1	žralok
datován	datovat	k5eAaImNgMnS	datovat
do	do	k7c2	do
období	období	k1gNnSc2	období
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
a	a	k8xC	a
na	na	k7c6	na
původním	původní	k2eAgInSc6d1	původní
americkém	americký	k2eAgInSc6d1	americký
přebalu	přebal	k1gInSc6	přebal
je	být	k5eAaImIp3nS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
tyranosaura	tyranosaura	k1gFnSc1	tyranosaura
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
nesmysl	nesmysl	k1gInSc1	nesmysl
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tato	tento	k3xDgNnPc1	tento
zvířata	zvíře	k1gNnPc1	zvíře
žila	žít	k5eAaImAgNnP	žít
ve	v	k7c6	v
zcela	zcela	k6eAd1	zcela
jiných	jiný	k2eAgNnPc6d1	jiné
geologických	geologický	k2eAgNnPc6d1	geologické
obdobích	období	k1gNnPc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
pak	pak	k6eAd1	pak
napsal	napsat	k5eAaPmAgMnS	napsat
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
navazujících	navazující	k2eAgInPc2d1	navazující
románů	román	k1gInPc2	román
s	s	k7c7	s
megalodony	megalodon	k1gMnPc7	megalodon
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
natočil	natočit	k5eAaBmAgInS	natočit
Discovery	Discovera	k1gFnSc2	Discovera
Channel	Channel	k1gInSc1	Channel
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
Megalodon	Megalodon	k1gInSc1	Megalodon
<g/>
:	:	kIx,	:
Obří	obří	k2eAgInSc1d1	obří
superžralok	superžralok	k1gInSc1	superžralok
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kontroverzní	kontroverzní	k2eAgInSc1d1	kontroverzní
dokument	dokument	k1gInSc1	dokument
předkládal	předkládat	k5eAaImAgInS	předkládat
údajné	údajný	k2eAgInPc4d1	údajný
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
naznačovaly	naznačovat	k5eAaImAgInP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
megalodon	megalodon	k1gMnSc1	megalodon
nevyhynul	vyhynout	k5eNaPmAgMnS	vyhynout
a	a	k8xC	a
stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
film	film	k1gInSc4	film
se	se	k3xPyFc4	se
snesla	snést	k5eAaPmAgFnS	snést
vlna	vlna	k1gFnSc1	vlna
kritiky	kritika	k1gFnSc2	kritika
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
zcela	zcela	k6eAd1	zcela
smyšlený	smyšlený	k2eAgInSc1d1	smyšlený
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
všechny	všechen	k3xTgMnPc4	všechen
údajné	údajný	k2eAgMnPc4d1	údajný
vědce	vědec	k1gMnPc4	vědec
představovali	představovat	k5eAaImAgMnP	představovat
herci	herec	k1gMnSc3	herec
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
tento	tento	k3xDgInSc4	tento
pseudodokument	pseudodokument	k1gInSc4	pseudodokument
znovu	znovu	k6eAd1	znovu
DC	DC	kA	DC
odvysílal	odvysílat	k5eAaPmAgInS	odvysílat
společně	společně	k6eAd1	společně
s	s	k7c7	s
novými	nový	k2eAgInPc7d1	nový
hodinovými	hodinový	k2eAgInPc7d1	hodinový
filmy	film	k1gInPc7	film
Megalodon	Megalodon	k1gNnSc1	Megalodon
<g/>
:	:	kIx,	:
Nové	Nové	k2eAgInPc1d1	Nové
důkazy	důkaz	k1gInPc1	důkaz
a	a	k8xC	a
Žralok	žralok	k1gMnSc1	žralok
v	v	k7c6	v
hlubinách	hlubina	k1gFnPc6	hlubina
<g/>
:	:	kIx,	:
Zuřivá	zuřivý	k2eAgFnSc1d1	zuřivá
ponorka	ponorka	k1gFnSc1	ponorka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
opět	opět	k6eAd1	opět
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
kritiku	kritika	k1gFnSc4	kritika
médií	médium	k1gNnPc2	médium
i	i	k8xC	i
vědecké	vědecký	k2eAgFnSc2d1	vědecká
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
existuje	existovat	k5eAaImIp3nS	existovat
rovněž	rovněž	k9	rovněž
několik	několik	k4yIc4	několik
údajných	údajný	k2eAgNnPc2d1	údajné
pozorování	pozorování	k1gNnPc2	pozorování
megalodonů	megalodon	k1gMnPc2	megalodon
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neověřených	ověřený	k2eNgInPc2d1	neověřený
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgNnPc2	tento
pozorování	pozorování	k1gNnPc2	pozorování
měli	mít	k5eAaImAgMnP	mít
žraloci	žralok	k1gMnPc1	žralok
měřit	měřit	k5eAaImF	měřit
mezi	mezi	k7c7	mezi
10	[number]	k4	10
a	a	k8xC	a
90	[number]	k4	90
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
chybně	chybně	k6eAd1	chybně
identifikované	identifikovaný	k2eAgMnPc4d1	identifikovaný
žraloky	žralok	k1gMnPc4	žralok
velrybí	velrybí	k2eAgNnSc1d1	velrybí
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
polynéský	polynéský	k2eAgInSc1d1	polynéský
mýtus	mýtus	k1gInSc1	mýtus
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
třicetimetrovém	třicetimetrový	k2eAgMnSc6d1	třicetimetrový
žralokovi	žralok	k1gMnSc6	žralok
<g/>
,	,	kIx,	,
kterého	který	k3yIgInSc2	který
nazývá	nazývat	k5eAaImIp3nS	nazývat
pánem	pán	k1gMnSc7	pán
hlubin	hlubina	k1gFnPc2	hlubina
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
však	však	k9	však
také	také	k9	také
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
žraloka	žralok	k1gMnSc4	žralok
velrybího	velrybí	k2eAgMnSc4d1	velrybí
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
údajně	údajně	k6eAd1	údajně
nových	nový	k2eAgInPc6d1	nový
zubech	zub	k1gInPc6	zub
megalodona	megalodon	k1gMnSc2	megalodon
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
například	například	k6eAd1	například
objevila	objevit	k5eAaPmAgFnS	objevit
posádka	posádka	k1gFnSc1	posádka
lodi	loď	k1gFnSc2	loď
HMS	HMS	kA	HMS
Challenger	Challenger	k1gInSc4	Challenger
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
<g/>
,	,	kIx,	,
a	a	k8xC	a
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
chybně	chybně	k6eAd1	chybně
datovány	datovat	k5eAaImNgFnP	datovat
do	do	k7c2	do
období	období	k1gNnSc2	období
před	před	k7c7	před
24	[number]	k4	24
000	[number]	k4	000
až	až	k9	až
11	[number]	k4	11
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
chybné	chybný	k2eAgNnSc4d1	chybné
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
dobře	dobře	k6eAd1	dobře
zakonzervované	zakonzervovaný	k2eAgInPc4d1	zakonzervovaný
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
si	se	k3xPyFc3	se
při	při	k7c6	při
fosilizaci	fosilizace	k1gFnSc6	fosilizace
zachovaly	zachovat	k5eAaPmAgInP	zachovat
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
zuby	zub	k1gInPc1	zub
megalodona	megalodona	k1gFnSc1	megalodona
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zbarveny	zbarven	k2eAgInPc4d1	zbarven
různými	různý	k2eAgInPc7d1	různý
odstíny	odstín	k1gInPc7	odstín
<g/>
,	,	kIx,	,
od	od	k7c2	od
špinavě	špinavě	k6eAd1	špinavě
bílé	bílý	k2eAgFnSc2d1	bílá
přes	přes	k7c4	přes
tmavě	tmavě	k6eAd1	tmavě
hnědou	hnědý	k2eAgFnSc4d1	hnědá
až	až	k6eAd1	až
po	po	k7c4	po
šedou	šedý	k2eAgFnSc4d1	šedá
<g/>
,	,	kIx,	,
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
vyzdvihnuty	vyzdvihnout	k5eAaPmNgFnP	vyzdvihnout
do	do	k7c2	do
mladší	mladý	k2eAgFnSc2d2	mladší
geologické	geologický	k2eAgFnSc2d1	geologická
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
megalodon	megalodon	k1gInSc1	megalodon
mohl	moct	k5eAaImAgInS	moct
dodnes	dodnes	k6eAd1	dodnes
přežívat	přežívat	k5eAaImF	přežívat
v	v	k7c6	v
mořských	mořský	k2eAgFnPc6d1	mořská
hlubinách	hlubina	k1gFnPc6	hlubina
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
objevený	objevený	k2eAgMnSc1d1	objevený
žralok	žralok	k1gMnSc1	žralok
velkoústý	velkoústý	k2eAgMnSc1d1	velkoústý
(	(	kIx(	(
<g/>
Megachasma	Megachasma	k1gNnSc1	Megachasma
pelagios	pelagiosa	k1gFnPc2	pelagiosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
megalodon	megalodon	k1gMnSc1	megalodon
žil	žít	k5eAaImAgMnS	žít
primárně	primárně	k6eAd1	primárně
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
teplých	teplý	k2eAgFnPc2d1	teplá
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
vod	voda	k1gFnPc2	voda
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
by	by	kYmCp3nS	by
nepřežil	přežít	k5eNaPmAgMnS	přežít
v	v	k7c6	v
chladném	chladný	k2eAgInSc6d1	chladný
a	a	k8xC	a
na	na	k7c4	na
potravu	potrava	k1gFnSc4	potrava
chudém	chudý	k2eAgNnSc6d1	chudé
prostředí	prostředí	k1gNnSc6	prostředí
hlubokého	hluboký	k2eAgNnSc2d1	hluboké
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Megalodon	Megalodona	k1gFnPc2	Megalodona
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
NOVÁK	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
A.	A.	kA	A.
Megalodon	Megalodon	k1gMnSc1	Megalodon
<g/>
.	.	kIx.	.
</s>
<s>
Pátrání	pátrání	k1gNnSc4	pátrání
po	po	k7c6	po
nejstrašnějším	strašný	k2eAgMnSc6d3	nejstrašnější
mořském	mořský	k2eAgMnSc6d1	mořský
predátorovi	predátor	k1gMnSc6	predátor
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
XYZ	XYZ	kA	XYZ
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
308	[number]	k4	308
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7388	[number]	k4	7388
<g/>
-	-	kIx~	-
<g/>
535	[number]	k4	535
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Megalodon	Megalodona	k1gFnPc2	Megalodona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Carcharocles	Carcharocles	k1gInSc1	Carcharocles
megalodon	megalodon	k1gInSc1	megalodon
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
vědeckém	vědecký	k2eAgInSc6d1	vědecký
výzkumu	výzkum	k1gInSc6	výzkum
druhu	druh	k1gInSc2	druh
C.	C.	kA	C.
megalodon	megalodon	k1gMnSc1	megalodon
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
megalodonovi	megalodon	k1gMnSc6	megalodon
na	na	k7c6	na
webu	web	k1gInSc6	web
časopisu	časopis	k1gInSc2	časopis
100	[number]	k4	100
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
