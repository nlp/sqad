<s>
Objektivní	objektivní	k2eAgNnSc1d1	objektivní
rozdělení	rozdělení	k1gNnSc1	rozdělení
a	a	k8xC	a
popsání	popsání	k1gNnSc1	popsání
všech	všecek	k3xTgInPc2	všecek
významů	význam	k1gInPc2	význam
lásky	láska	k1gFnSc2	láska
prakticky	prakticky	k6eAd1	prakticky
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vnímání	vnímání	k1gNnSc1	vnímání
lásky	láska	k1gFnSc2	láska
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
osobních	osobní	k2eAgFnPc6d1	osobní
vlastnostech	vlastnost	k1gFnPc6	vlastnost
a	a	k8xC	a
zkušenostech	zkušenost	k1gFnPc6	zkušenost
každého	každý	k3xTgMnSc4	každý
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
