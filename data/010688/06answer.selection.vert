<s>
Letní	letní	k2eAgInSc1d1	letní
slunovrat	slunovrat	k1gInSc1	slunovrat
nastává	nastávat	k5eAaImIp3nS	nastávat
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
obvykle	obvykle	k6eAd1	obvykle
okolo	okolo	k7c2	okolo
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
zimní	zimní	k2eAgInSc1d1	zimní
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jejich	jejich	k3xOp3gInSc1	jejich
přesný	přesný	k2eAgInSc1d1	přesný
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
mírně	mírně	k6eAd1	mírně
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
