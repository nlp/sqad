<p>
<s>
Slunovrat	slunovrat	k1gInSc1	slunovrat
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
solstitium	solstitium	k1gNnSc1	solstitium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
astronomický	astronomický	k2eAgInSc4d1	astronomický
termín	termín	k1gInSc4	termín
pro	pro	k7c4	pro
okamžik	okamžik	k1gInSc4	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Slunce	slunce	k1gNnSc1	slunce
má	mít	k5eAaImIp3nS	mít
vůči	vůči	k7c3	vůči
světovému	světový	k2eAgInSc3d1	světový
rovníku	rovník	k1gInSc2	rovník
největší	veliký	k2eAgMnPc4d3	veliký
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
letního	letní	k2eAgInSc2d1	letní
slunovratu	slunovrat	k1gInSc2	slunovrat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
nejmenší	malý	k2eAgInPc1d3	nejmenší
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zimního	zimní	k2eAgNnSc2d1	zimní
<g/>
)	)	kIx)	)
deklinaci	deklinace	k1gFnSc4	deklinace
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgInSc1d1	letní
slunovrat	slunovrat	k1gInSc1	slunovrat
nastává	nastávat	k5eAaImIp3nS	nastávat
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
obvykle	obvykle	k6eAd1	obvykle
okolo	okolo	k7c2	okolo
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
zimní	zimní	k2eAgInSc1d1	zimní
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jejich	jejich	k3xOp3gInSc1	jejich
přesný	přesný	k2eAgInSc1d1	přesný
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
mírně	mírně	k6eAd1	mírně
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristiky	charakteristika	k1gFnPc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
Slunovraty	slunovrat	k1gInPc1	slunovrat
mají	mít	k5eAaImIp3nP	mít
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
náklonu	náklon	k1gInSc3	náklon
zemské	zemský	k2eAgFnSc2d1	zemská
osy	osa	k1gFnSc2	osa
od	od	k7c2	od
roviny	rovina	k1gFnSc2	rovina
oběhu	oběh	k1gInSc2	oběh
Země	zem	k1gFnSc2	zem
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
Země	zem	k1gFnSc2	zem
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
často	často	k6eAd1	často
věří	věřit	k5eAaImIp3nS	věřit
(	(	kIx(	(
<g/>
srovnej	srovnat	k5eAaPmRp2nS	srovnat
s	s	k7c7	s
aféliem	afélium	k1gNnSc7	afélium
a	a	k8xC	a
perihéliem	perihélium	k1gNnSc7	perihélium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
definice	definice	k1gFnPc1	definice
slunovratu	slunovrat	k1gInSc2	slunovrat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
okamžiky	okamžik	k1gInPc1	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Slunce	slunce	k1gNnSc1	slunce
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
nejsevernějšího	severní	k2eAgInSc2d3	nejsevernější
nebo	nebo	k8xC	nebo
nejjižnějšího	jižní	k2eAgInSc2d3	nejjižnější
bodu	bod	k1gInSc2	bod
své	svůj	k3xOyFgFnSc2	svůj
pomyslné	pomyslný	k2eAgFnSc2d1	pomyslná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
obratníků	obratník	k1gInPc2	obratník
Raka	rak	k1gMnSc2	rak
(	(	kIx(	(
<g/>
sever	sever	k1gInSc1	sever
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
(	(	kIx(	(
<g/>
jih	jih	k1gInSc1	jih
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nastávají	nastávat	k5eAaImIp3nP	nastávat
<g/>
,	,	kIx,	,
když	když	k8xS	když
den	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
nebo	nebo	k8xC	nebo
nejkratší	krátký	k2eAgInSc1d3	nejkratší
(	(	kIx(	(
<g/>
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
zeměpisnou	zeměpisný	k2eAgFnSc4d1	zeměpisná
šířku	šířka	k1gFnSc4	šířka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
neplatí	platit	k5eNaImIp3nS	platit
třeba	třeba	k6eAd1	třeba
pro	pro	k7c4	pro
póly	pól	k1gInPc4	pól
nebo	nebo	k8xC	nebo
území	území	k1gNnSc4	území
mezi	mezi	k7c7	mezi
obratníky	obratník	k1gInPc7	obratník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nastávají	nastávat	k5eAaImIp3nP	nastávat
ve	v	k7c6	v
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Slunce	slunce	k1gNnSc1	slunce
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
nejvýše	vysoce	k6eAd3	vysoce
resp.	resp.	kA	resp.
nejníže	nízce	k6eAd3	nízce
(	(	kIx(	(
<g/>
neplatí	platit	k5eNaImIp3nS	platit
pro	pro	k7c4	pro
území	území	k1gNnSc4	území
mezi	mezi	k7c7	mezi
obratníky	obratník	k1gInPc7	obratník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nastávají	nastávat	k5eAaImIp3nP	nastávat
ve	v	k7c6	v
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
úhel	úhel	k1gInSc4	úhel
střed	středa	k1gFnPc2	středa
Slunce	slunce	k1gNnSc2	slunce
–	–	k?	–
střed	střed	k1gInSc4	střed
Země	zem	k1gFnSc2	zem
–	–	k?	–
geografický	geografický	k2eAgInSc4d1	geografický
(	(	kIx(	(
<g/>
severní	severní	k2eAgMnSc1d1	severní
či	či	k8xC	či
jižní	jižní	k2eAgInSc1d1	jižní
<g/>
)	)	kIx)	)
pól	pól	k1gInSc1	pól
největšíData	největšíDat	k1gMnSc2	největšíDat
letního	letní	k2eAgMnSc2d1	letní
a	a	k8xC	a
zimního	zimní	k2eAgInSc2d1	zimní
slunovratu	slunovrat	k1gInSc2	slunovrat
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
severní	severní	k2eAgFnSc4d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
opačná	opačný	k2eAgFnSc1d1	opačná
<g/>
.	.	kIx.	.
</s>
<s>
Čistě	čistě	k6eAd1	čistě
teoreticky	teoreticky	k6eAd1	teoreticky
<g/>
,	,	kIx,	,
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
mezi	mezi	k7c7	mezi
slunovraty	slunovrat	k1gInPc7	slunovrat
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
i	i	k9	i
precese	precese	k1gFnSc1	precese
zemské	zemský	k2eAgFnSc2d1	zemská
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
pomalý	pomalý	k2eAgInSc1d1	pomalý
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oslavy	oslava	k1gFnPc4	oslava
slunovratů	slunovrat	k1gInPc2	slunovrat
==	==	k?	==
</s>
</p>
<p>
<s>
Slunovraty	slunovrat	k1gInPc1	slunovrat
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
oslav	oslava	k1gFnPc2	oslava
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
kulturách	kultura	k1gFnPc6	kultura
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
které	který	k3yQgFnSc6	který
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
mající	mající	k2eAgInPc4d1	mající
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
oslav	oslava	k1gFnPc2	oslava
zimního	zimní	k2eAgInSc2d1	zimní
slunovratu	slunovrat	k1gInSc2	slunovrat
bylo	být	k5eAaImAgNnS	být
vítání	vítání	k1gNnSc1	vítání
delšího	dlouhý	k2eAgInSc2d2	delší
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
delší	dlouhý	k2eAgFnSc2d2	delší
doby	doba	k1gFnSc2	doba
během	během	k7c2	během
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
po	po	k7c4	po
kterou	který	k3yRgFnSc4	který
svítí	svítit	k5eAaImIp3nS	svítit
Slunce	slunce	k1gNnSc1	slunce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ústup	ústup	k1gInSc1	ústup
zimy	zima	k1gFnSc2	zima
a	a	k8xC	a
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
příchod	příchod	k1gInSc1	příchod
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
lepších	dobrý	k2eAgFnPc2d2	lepší
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
potažmo	potažmo	k6eAd1	potažmo
více	hodně	k6eAd2	hodně
úrody	úroda	k1gFnSc2	úroda
a	a	k8xC	a
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
kultury	kultura	k1gFnPc1	kultura
si	se	k3xPyFc3	se
tyto	tento	k3xDgInPc1	tento
cykly	cyklus	k1gInPc1	cyklus
vysvětlovaly	vysvětlovat	k5eAaImAgInP	vysvětlovat
jako	jako	k9	jako
přízeň	přízeň	k1gFnSc4	přízeň
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
odpovídajícím	odpovídající	k2eAgInSc7d1	odpovídající
způsobem	způsob	k1gInSc7	způsob
je	on	k3xPp3gNnSc4	on
oslavovaly	oslavovat	k5eAaImAgFnP	oslavovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
slunovratů	slunovrat	k1gInPc2	slunovrat
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Letní	letní	k2eAgInSc1d1	letní
slunovrat	slunovrat	k1gInSc1	slunovrat
===	===	k?	===
</s>
</p>
<p>
<s>
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
je	být	k5eAaImIp3nS	být
zimní	zimní	k2eAgInSc1d1	zimní
slunovrat	slunovrat	k1gInSc1	slunovrat
</s>
</p>
<p>
<s>
Nastává	nastávat	k5eAaImIp3nS	nastávat
obvykle	obvykle	k6eAd1	obvykle
20	[number]	k4	20
<g/>
.	.	kIx.	.
či	či	k8xC	či
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzácně	vzácně	k6eAd1	vzácně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgInSc1d1	poslední
výskyt	výskyt	k1gInSc1	výskyt
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
příští	příští	k2eAgInSc1d1	příští
výskyt	výskyt	k1gInSc1	výskyt
2203	[number]	k4	2203
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
(	(	kIx(	(
<g/>
příští	příští	k2eAgInSc1d1	příští
výskyt	výskyt	k1gInSc1	výskyt
2487	[number]	k4	2487
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
Slunce	slunce	k1gNnSc2	slunce
vychází	vycházet	k5eAaImIp3nS	vycházet
asi	asi	k9	asi
24	[number]	k4	24
<g/>
°	°	k?	°
severně	severně	k6eAd1	severně
(	(	kIx(	(
<g/>
nalevo	nalevo	k6eAd1	nalevo
<g/>
)	)	kIx)	)
od	od	k7c2	od
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
jde	jít	k5eAaImIp3nS	jít
po	po	k7c6	po
severní	severní	k2eAgFnSc6d1	severní
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
maximální	maximální	k2eAgFnSc2d1	maximální
výšky	výška	k1gFnSc2	výška
66	[number]	k4	66
<g/>
°	°	k?	°
<g/>
33	[number]	k4	33
<g/>
'	'	kIx"	'
a	a	k8xC	a
zapadne	zapadnout	k5eAaPmIp3nS	zapadnout
24	[number]	k4	24
<g/>
°	°	k?	°
severně	severně	k6eAd1	severně
(	(	kIx(	(
<g/>
napravo	napravo	k6eAd1	napravo
<g/>
)	)	kIx)	)
od	od	k7c2	od
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
obratníku	obratník	k1gInSc6	obratník
Raka	rak	k1gMnSc2	rak
Slunce	slunce	k1gNnSc2	slunce
vychází	vycházet	k5eAaImIp3nS	vycházet
asi	asi	k9	asi
26	[number]	k4	26
<g/>
°	°	k?	°
severně	severně	k6eAd1	severně
(	(	kIx(	(
<g/>
nalevo	nalevo	k6eAd1	nalevo
<g/>
)	)	kIx)	)
od	od	k7c2	od
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dne	den	k1gInSc2	den
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
zenitu	zenit	k1gInSc3	zenit
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
zapadá	zapadat	k5eAaPmIp3nS	zapadat
asi	asi	k9	asi
26	[number]	k4	26
<g/>
°	°	k?	°
severně	severně	k6eAd1	severně
(	(	kIx(	(
<g/>
napravo	napravo	k6eAd1	napravo
<g/>
)	)	kIx)	)
od	od	k7c2	od
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
obratníku	obratník	k1gInSc6	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
Slunce	slunce	k1gNnSc2	slunce
vychází	vycházet	k5eAaImIp3nS	vycházet
asi	asi	k9	asi
26	[number]	k4	26
<g/>
°	°	k?	°
severně	severně	k6eAd1	severně
(	(	kIx(	(
<g/>
nalevo	nalevo	k6eAd1	nalevo
<g/>
)	)	kIx)	)
od	od	k7c2	od
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dne	den	k1gInSc2	den
přechází	přecházet	k5eAaImIp3nS	přecházet
přes	přes	k7c4	přes
severní	severní	k2eAgFnSc4d1	severní
oblohu	obloha	k1gFnSc4	obloha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
výšky	výška	k1gFnPc4	výška
až	až	k9	až
43	[number]	k4	43
<g/>
°	°	k?	°
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
zapadá	zapadat	k5eAaPmIp3nS	zapadat
asi	asi	k9	asi
26	[number]	k4	26
<g/>
°	°	k?	°
severně	severně	k6eAd1	severně
(	(	kIx(	(
<g/>
napravo	napravo	k6eAd1	napravo
<g/>
)	)	kIx)	)
od	od	k7c2	od
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
polárním	polární	k2eAgInSc6d1	polární
kruhu	kruh	k1gInSc6	kruh
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc1	slunce
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
dotkne	dotknout	k5eAaPmIp3nS	dotknout
horizontu	horizont	k1gInSc2	horizont
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
zapadlo	zapadnout	k5eAaPmAgNnS	zapadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
polárním	polární	k2eAgInSc6d1	polární
kruhu	kruh	k1gInSc6	kruh
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc1	slunce
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
dotkne	dotknout	k5eAaPmIp3nS	dotknout
horizontu	horizont	k1gInSc3	horizont
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
pólu	pól	k1gInSc6	pól
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
+23	+23	k4	+23
<g/>
°	°	k?	°
<g/>
29	[number]	k4	29
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
horizontem	horizont	k1gInSc7	horizont
<g/>
)	)	kIx)	)
–	–	k?	–
probíhá	probíhat	k5eAaImIp3nS	probíhat
polární	polární	k2eAgInSc4d1	polární
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
−	−	k?	−
<g/>
°	°	k?	°
<g/>
27	[number]	k4	27
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
horizontem	horizont	k1gInSc7	horizont
<g/>
)	)	kIx)	)
–	–	k?	–
probíhá	probíhat	k5eAaImIp3nS	probíhat
polární	polární	k2eAgFnSc4d1	polární
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zimní	zimní	k2eAgInSc1d1	zimní
slunovrat	slunovrat	k1gInSc1	slunovrat
===	===	k?	===
</s>
</p>
<p>
<s>
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
je	být	k5eAaImIp3nS	být
letní	letní	k2eAgInSc4d1	letní
slunovrat	slunovrat	k1gInSc4	slunovrat
</s>
</p>
<p>
<s>
Nastává	nastávat	k5eAaImIp3nS	nastávat
obvykle	obvykle	k6eAd1	obvykle
21	[number]	k4	21
<g/>
.	.	kIx.	.
či	či	k8xC	či
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzácně	vzácně	k6eAd1	vzácně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgInSc1d1	poslední
výskyt	výskyt	k1gInSc1	výskyt
roku	rok	k1gInSc2	rok
1697	[number]	k4	1697
<g/>
,	,	kIx,	,
příští	příští	k2eAgInSc1d1	příští
výskyt	výskyt	k1gInSc1	výskyt
2080	[number]	k4	2080
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgInSc1d1	poslední
výskyt	výskyt	k1gInSc1	výskyt
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
příští	příští	k2eAgInSc1d1	příští
výskyt	výskyt	k1gInSc1	výskyt
2303	[number]	k4	2303
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
Slunce	slunce	k1gNnSc2	slunce
vychází	vycházet	k5eAaImIp3nS	vycházet
asi	asi	k9	asi
24	[number]	k4	24
<g/>
°	°	k?	°
jižně	jižně	k6eAd1	jižně
(	(	kIx(	(
<g/>
napravo	napravo	k6eAd1	napravo
<g/>
)	)	kIx)	)
od	od	k7c2	od
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
jde	jít	k5eAaImIp3nS	jít
po	po	k7c6	po
jižní	jižní	k2eAgFnSc6d1	jižní
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
maximální	maximální	k2eAgFnSc2d1	maximální
výšky	výška	k1gFnSc2	výška
66	[number]	k4	66
<g/>
°	°	k?	°
<g/>
33	[number]	k4	33
<g/>
'	'	kIx"	'
a	a	k8xC	a
zapadne	zapadnout	k5eAaPmIp3nS	zapadnout
24	[number]	k4	24
<g/>
°	°	k?	°
jižně	jižně	k6eAd1	jižně
(	(	kIx(	(
<g/>
nalevo	nalevo	k6eAd1	nalevo
<g/>
)	)	kIx)	)
od	od	k7c2	od
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
obratníku	obratník	k1gInSc6	obratník
Raka	rak	k1gMnSc2	rak
Slunce	slunce	k1gNnSc2	slunce
vychází	vycházet	k5eAaImIp3nS	vycházet
asi	asi	k9	asi
26	[number]	k4	26
<g/>
°	°	k?	°
jižně	jižně	k6eAd1	jižně
(	(	kIx(	(
<g/>
napravo	napravo	k6eAd1	napravo
<g/>
)	)	kIx)	)
od	od	k7c2	od
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dne	den	k1gInSc2	den
přechází	přecházet	k5eAaImIp3nS	přecházet
přes	přes	k7c4	přes
jižní	jižní	k2eAgFnSc4d1	jižní
oblohu	obloha	k1gFnSc4	obloha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
výšky	výška	k1gFnPc4	výška
až	až	k9	až
43	[number]	k4	43
<g/>
°	°	k?	°
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
zapadá	zapadat	k5eAaPmIp3nS	zapadat
asi	asi	k9	asi
26	[number]	k4	26
<g/>
°	°	k?	°
jižně	jižně	k6eAd1	jižně
(	(	kIx(	(
<g/>
nalevo	nalevo	k6eAd1	nalevo
<g/>
)	)	kIx)	)
od	od	k7c2	od
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
obratníku	obratník	k1gInSc6	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
Slunce	slunce	k1gNnSc2	slunce
vychází	vycházet	k5eAaImIp3nS	vycházet
asi	asi	k9	asi
26	[number]	k4	26
<g/>
°	°	k?	°
jižně	jižně	k6eAd1	jižně
(	(	kIx(	(
<g/>
napravo	napravo	k6eAd1	napravo
<g/>
)	)	kIx)	)
od	od	k7c2	od
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dne	den	k1gInSc2	den
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
zenitu	zenit	k1gInSc3	zenit
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
zapadá	zapadat	k5eAaPmIp3nS	zapadat
asi	asi	k9	asi
26	[number]	k4	26
<g/>
°	°	k?	°
jižně	jižně	k6eAd1	jižně
(	(	kIx(	(
<g/>
nalevo	nalevo	k6eAd1	nalevo
<g/>
)	)	kIx)	)
od	od	k7c2	od
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
polárním	polární	k2eAgInSc6d1	polární
kruhu	kruh	k1gInSc6	kruh
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc1	slunce
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
dotkne	dotknout	k5eAaPmIp3nS	dotknout
horizontu	horizont	k1gInSc3	horizont
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
polárním	polární	k2eAgInSc6d1	polární
kruhu	kruh	k1gInSc6	kruh
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc1	slunce
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
dotkne	dotknout	k5eAaPmIp3nS	dotknout
horizontu	horizont	k1gInSc2	horizont
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
zapadlo	zapadnout	k5eAaPmAgNnS	zapadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
pólu	pól	k1gInSc6	pól
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
−	−	k?	−
<g/>
°	°	k?	°
<g/>
27	[number]	k4	27
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
horizontem	horizont	k1gInSc7	horizont
<g/>
)	)	kIx)	)
–	–	k?	–
probíhá	probíhat	k5eAaImIp3nS	probíhat
polární	polární	k2eAgFnSc4d1	polární
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
+23	+23	k4	+23
<g/>
°	°	k?	°
<g/>
27	[number]	k4	27
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
horizontem	horizont	k1gInSc7	horizont
<g/>
)	)	kIx)	)
–	–	k?	–
probíhá	probíhat	k5eAaImIp3nS	probíhat
polární	polární	k2eAgInSc4d1	polární
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Data	datum	k1gNnSc2	datum
slunovratů	slunovrat	k1gInPc2	slunovrat
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
a	a	k8xC	a
pro	pro	k7c4	pro
nejbližší	blízký	k2eAgInPc4d3	Nejbližší
roky	rok	k1gInPc4	rok
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
</s>
</p>
<p>
<s>
Precese	precese	k1gFnSc1	precese
zemské	zemský	k2eAgFnSc2d1	zemská
osy	osa	k1gFnSc2	osa
</s>
</p>
<p>
<s>
Ekliptika	ekliptika	k1gFnSc1	ekliptika
</s>
</p>
<p>
<s>
Slavnosti	slavnost	k1gFnPc1	slavnost
slunovratu	slunovrat	k1gInSc2	slunovrat
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
slunovrat	slunovrat	k1gInSc1	slunovrat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
slunovrat	slunovrat	k1gInSc1	slunovrat
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Zimní	zimní	k2eAgInSc1d1	zimní
slunovrat	slunovrat	k1gInSc1	slunovrat
a	a	k8xC	a
staří	starý	k2eAgMnPc1d1	starý
Slované	Slovan	k1gMnPc1	Slovan
</s>
</p>
<p>
<s>
Začátky	začátek	k1gInPc1	začátek
ročních	roční	k2eAgFnPc2d1	roční
dob	doba	k1gFnPc2	doba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2999	[number]	k4	2999
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
Převod	převod	k1gInSc1	převod
času	čas	k1gInSc2	čas
GMT	GMT	kA	GMT
<g/>
/	/	kIx~	/
<g/>
UTC	UTC	kA	UTC
do	do	k7c2	do
CET	ceta	k1gFnPc2	ceta
(	(	kIx(	(
<g/>
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
slunovratové	slunovratový	k2eAgNnSc1d1	slunovratové
mystérium	mystérium	k1gNnSc1	mystérium
</s>
</p>
