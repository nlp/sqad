<p>
<s>
Zavraždění	zavraždění	k1gNnSc1	zavraždění
carské	carský	k2eAgFnSc2d1	carská
rodiny	rodina	k1gFnSc2	rodina
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
na	na	k7c4	na
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
ve	v	k7c6	v
sklepě	sklep	k1gInSc6	sklep
Ipaťjevova	Ipaťjevův	k2eAgInSc2d1	Ipaťjevův
domu	dům	k1gInSc2	dům
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
Jekatěrinburgu	Jekatěrinburg	k1gInSc6	Jekatěrinburg
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zde	zde	k6eAd1	zde
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
někdejší	někdejší	k2eAgMnSc1d1	někdejší
car	car	k1gMnSc1	car
ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
civilním	civilní	k2eAgNnSc7d1	civilní
jménem	jméno	k1gNnSc7	jméno
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Romanov	Romanov	k1gInSc1	Romanov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
a	a	k8xC	a
nejbližší	blízký	k2eAgNnSc1d3	nejbližší
služebnictvo	služebnictvo	k1gNnSc1	služebnictvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
zadržení	zadržení	k1gNnSc6	zadržení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
po	po	k7c6	po
únorové	únorový	k2eAgFnSc6d1	únorová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
,	,	kIx,	,
svržení	svržení	k1gNnSc4	svržení
z	z	k7c2	z
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
po	po	k7c6	po
období	období	k1gNnSc6	období
domácího	domácí	k2eAgNnSc2d1	domácí
věznění	věznění	k1gNnSc2	věznění
v	v	k7c6	v
Carském	carský	k2eAgNnSc6d1	carské
Selu	selo	k1gNnSc6	selo
nedaleko	nedaleko	k7c2	nedaleko
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Puškin	Puškin	k1gInSc1	Puškin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
někdejší	někdejší	k2eAgMnPc1d1	někdejší
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
podle	podle	k7c2	podle
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
ruské	ruský	k2eAgFnSc2d1	ruská
vlády	vláda	k1gFnSc2	vláda
posláni	poslat	k5eAaPmNgMnP	poslat
do	do	k7c2	do
Tobolsku	Tobolsek	k1gInSc2	Tobolsek
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
žili	žít	k5eAaImAgMnP	žít
ve	v	k7c6	v
vile	vila	k1gFnSc6	vila
bývalého	bývalý	k2eAgMnSc4d1	bývalý
guvernéra	guvernér	k1gMnSc4	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
bolševiků	bolševik	k1gMnPc2	bolševik
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1918	[number]	k4	1918
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
povolení	povolení	k1gNnSc1	povolení
prezídia	prezídium	k1gNnSc2	prezídium
(	(	kIx(	(
<g/>
VCIK	VCIK	kA	VCIK
<g/>
)	)	kIx)	)
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
sjezdu	sjezd	k1gInSc2	sjezd
o	o	k7c6	o
převozu	převoz	k1gInSc6	převoz
Romanovců	Romanovec	k1gMnPc2	Romanovec
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
byli	být	k5eAaImAgMnP	být
Romanovci	Romanovec	k1gMnPc1	Romanovec
deportováni	deportovat	k5eAaBmNgMnP	deportovat
vlakem	vlak	k1gInSc7	vlak
z	z	k7c2	z
Tobolsku	Tobolsek	k1gInSc2	Tobolsek
do	do	k7c2	do
Jekatěrinburgu	Jekatěrinburg	k1gInSc2	Jekatěrinburg
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
zabaven	zabaven	k2eAgInSc1d1	zabaven
dům	dům	k1gInSc1	dům
patřící	patřící	k2eAgFnSc2d1	patřící
důlnímu	důlní	k2eAgMnSc3d1	důlní
inženýrovi	inženýr	k1gMnSc3	inženýr
Ipaťjevovi	Ipaťjeva	k1gMnSc3	Ipaťjeva
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
července	červenec	k1gInSc2	červenec
1918	[number]	k4	1918
se	se	k3xPyFc4	se
blížily	blížit	k5eAaImAgInP	blížit
k	k	k7c3	k
Jekatěrinburgu	Jekatěrinburg	k1gInSc3	Jekatěrinburg
Československé	československý	k2eAgFnSc2d1	Československá
legie	legie	k1gFnSc2	legie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
udržely	udržet	k5eAaPmAgFnP	udržet
transsibiřskou	transsibiřský	k2eAgFnSc4d1	Transsibiřská
magistrálu	magistrála	k1gFnSc4	magistrála
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
měly	mít	k5eAaImAgFnP	mít
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
bolševici	bolševik	k1gMnPc1	bolševik
zpanikařili	zpanikařit	k5eAaPmAgMnP	zpanikařit
a	a	k8xC	a
proto	proto	k8xC	proto
rodinu	rodina	k1gFnSc4	rodina
popravili	popravit	k5eAaPmAgMnP	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Legie	legie	k1gFnSc1	legie
dorazila	dorazit	k5eAaPmAgFnS	dorazit
o	o	k7c4	o
necelý	celý	k2eNgInSc4d1	necelý
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
obsadila	obsadit	k5eAaPmAgFnS	obsadit
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Popravu	poprava	k1gFnSc4	poprava
patrně	patrně	k6eAd1	patrně
nařídil	nařídit	k5eAaPmAgInS	nařídit
místní	místní	k2eAgInSc1d1	místní
Výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
Uralu	Ural	k1gInSc2	Ural
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
plným	plný	k2eAgNnSc7d1	plné
vědomím	vědomí	k1gNnSc7	vědomí
Lenina	Lenin	k1gMnSc2	Lenin
<g/>
,	,	kIx,	,
Jakova	jakův	k2eAgInSc2d1	jakův
Sverdlova	Sverdlův	k2eAgInSc2d1	Sverdlův
a	a	k8xC	a
Felixe	Felix	k1gMnSc2	Felix
Dzeržinského	Dzeržinský	k2eAgMnSc2d1	Dzeržinský
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vraždou	vražda	k1gFnSc7	vražda
carské	carský	k2eAgFnSc2d1	carská
rodiny	rodina	k1gFnSc2	rodina
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zavraždění	zavraždění	k1gNnSc6	zavraždění
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgInPc2d1	další
členů	člen	k1gInPc2	člen
carské	carský	k2eAgFnSc2d1	carská
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
zachránily	zachránit	k5eAaPmAgFnP	zachránit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
carova	carův	k2eAgFnSc1d1	carova
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
dvě	dva	k4xCgFnPc1	dva
sestry	sestra	k1gFnPc1	sestra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poprava	poprava	k1gFnSc1	poprava
==	==	k?	==
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
půlnoci	půlnoc	k1gFnSc2	půlnoc
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
na	na	k7c4	na
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1918	[number]	k4	1918
Jakov	Jakovo	k1gNnPc2	Jakovo
Jurovskij	Jurovskij	k1gMnPc2	Jurovskij
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
domu	dům	k1gInSc2	dům
pro	pro	k7c4	pro
speciální	speciální	k2eAgInSc4d1	speciální
účel	účel	k1gInSc4	účel
<g/>
,	,	kIx,	,
nařídil	nařídit	k5eAaPmAgMnS	nařídit
lékaři	lékař	k1gMnPc1	lékař
Romanovců	Romanovec	k1gMnPc2	Romanovec
<g/>
,	,	kIx,	,
doktoru	doktor	k1gMnSc3	doktor
Evženu	Evžen	k1gMnSc3	Evžen
Botkinovi	Botkin	k1gMnSc3	Botkin
<g/>
,	,	kIx,	,
probudit	probudit	k5eAaPmF	probudit
spící	spící	k2eAgFnSc4d1	spící
rodinu	rodina	k1gFnSc4	rodina
a	a	k8xC	a
požádat	požádat	k5eAaPmF	požádat
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
oblékli	obléct	k5eAaPmAgMnP	obléct
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
<g/>
,	,	kIx,	,
že	že	k8xS	že
rodina	rodina	k1gFnSc1	rodina
bude	být	k5eAaImBp3nS	být
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
na	na	k7c4	na
bezpečnější	bezpečný	k2eAgNnSc4d2	bezpečnější
místo	místo	k1gNnSc4	místo
kvůli	kvůli	k7c3	kvůli
hrozícímu	hrozící	k2eAgInSc3d1	hrozící
chaosu	chaos	k1gInSc3	chaos
v	v	k7c6	v
Jekatěrinburgu	Jekatěrinburg	k1gInSc6	Jekatěrinburg
<g/>
.	.	kIx.	.
</s>
<s>
Romanovci	Romanovec	k1gMnPc1	Romanovec
byli	být	k5eAaImAgMnP	být
přemístěni	přemístit	k5eAaPmNgMnP	přemístit
do	do	k7c2	do
sklepní	sklepní	k2eAgFnSc2d1	sklepní
místnosti	místnost	k1gFnSc2	místnost
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
x	x	k?	x
<g/>
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
a	a	k8xC	a
vězňům	vězeň	k1gMnPc3	vězeň
bylo	být	k5eAaImAgNnS	být
poté	poté	k6eAd1	poté
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zde	zde	k6eAd1	zde
počkali	počkat	k5eAaPmAgMnP	počkat
<g/>
,	,	kIx,	,
než	než	k8xS	než
bude	být	k5eAaImBp3nS	být
k	k	k7c3	k
domu	dům	k1gInSc2	dům
přistaven	přistaven	k2eAgInSc4d1	přistaven
nákladní	nákladní	k2eAgInSc4d1	nákladní
automobil	automobil	k1gInSc4	automobil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nicméně	nicméně	k8xC	nicméně
o	o	k7c4	o
několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
později	pozdě	k6eAd2	pozdě
vešla	vejít	k5eAaPmAgFnS	vejít
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
popravčí	popravčí	k2eAgFnSc1d1	popravčí
četa	četa	k1gFnSc1	četa
tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Ivana	Ivan	k1gMnSc2	Ivan
Plotnikova	Plotnikův	k2eAgMnSc2d1	Plotnikův
<g/>
,	,	kIx,	,
profesora	profesor	k1gMnSc2	profesor
historie	historie	k1gFnSc2	historie
na	na	k7c6	na
Uralské	uralský	k2eAgFnSc6d1	Uralská
státní	státní	k2eAgFnSc6d1	státní
univerzitě	univerzita	k1gFnSc6	univerzita
A.	A.	kA	A.
M.	M.	kA	M.
Gorkého	Gorkij	k1gMnSc2	Gorkij
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
popravy	poprava	k1gFnPc4	poprava
<g/>
:	:	kIx,	:
Jakov	Jakov	k1gInSc1	Jakov
Jurovskij	Jurovskij	k1gFnSc2	Jurovskij
<g/>
,	,	kIx,	,
G.	G.	kA	G.
P.	P.	kA	P.
Nikulin	Nikulin	k2eAgMnSc1d1	Nikulin
<g/>
,	,	kIx,	,
M.	M.	kA	M.
A.	A.	kA	A.
Medvěděv	Medvěděv	k1gMnSc1	Medvěděv
(	(	kIx(	(
<g/>
Kudrin	Kudrin	k1gInSc1	Kudrin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Jermakov	Jermakov	k1gInSc1	Jermakov
<g/>
,	,	kIx,	,
S.	S.	kA	S.
P.	P.	kA	P.
Vaganov	Vaganov	k1gInSc1	Vaganov
<g/>
,	,	kIx,	,
A.	A.	kA	A.
G.	G.	kA	G.
Kabanov	Kabanov	k1gInSc1	Kabanov
<g/>
,	,	kIx,	,
P.	P.	kA	P.
S.	S.	kA	S.
Medvěděv	Medvěděv	k1gMnSc1	Medvěděv
<g/>
,	,	kIx,	,
V.	V.	kA	V.
N.	N.	kA	N.
Nětrebin	Nětrebina	k1gFnPc2	Nětrebina
a	a	k8xC	a
J.	J.	kA	J.
M.	M.	kA	M.
Celms	Celmsa	k1gFnPc2	Celmsa
<g/>
.	.	kIx.	.
</s>
<s>
Jurovskij	Jurovskít	k5eAaPmRp2nS	Jurovskít
přečetl	přečíst	k5eAaPmAgInS	přečíst
nahlas	nahlas	k6eAd1	nahlas
rozkaz	rozkaz	k1gInSc1	rozkaz
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikolaji	Nikolaj	k1gMnSc5	Nikolaj
Alexandroviči	Alexandrovič	k1gMnSc5	Alexandrovič
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vaši	váš	k3xOp2gMnPc1	váš
příbuzní	příbuzný	k1gMnPc1	příbuzný
nadále	nadále	k6eAd1	nadále
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
v	v	k7c6	v
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Sovětské	sovětský	k2eAgNnSc4d1	sovětské
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
Výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
Uralu	Ural	k1gInSc2	Ural
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
vás	vy	k3xPp2nPc4	vy
popravit	popravit	k5eAaPmF	popravit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
pohlédl	pohlédnout	k5eAaPmAgMnS	pohlédnout
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
otočil	otočit	k5eAaPmAgMnS	otočit
se	se	k3xPyFc4	se
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Co	co	k3yQnSc1	co
<g/>
?	?	kIx.	?
</s>
<s>
Co	co	k3yInSc1	co
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jurovskij	Jurovskít	k5eAaPmRp2nS	Jurovskít
rozkaz	rozkaz	k1gInSc4	rozkaz
rychle	rychle	k6eAd1	rychle
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
a	a	k8xC	a
zamířil	zamířit	k5eAaPmAgMnS	zamířit
pistolí	pistol	k1gFnSc7	pistol
na	na	k7c4	na
tělo	tělo	k1gNnSc4	tělo
Nikolaje	Nikolaj	k1gMnSc2	Nikolaj
a	a	k8xC	a
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
padl	padnout	k5eAaPmAgMnS	padnout
mrtev	mrtev	k2eAgMnSc1d1	mrtev
<g/>
.	.	kIx.	.
</s>
<s>
Jurovskij	Jurovskít	k5eAaPmRp2nS	Jurovskít
pak	pak	k6eAd1	pak
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
na	na	k7c4	na
Alexeje	Alexej	k1gMnSc4	Alexej
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
popravčí	popravčí	k1gMnPc1	popravčí
poté	poté	k6eAd1	poté
začali	začít	k5eAaPmAgMnP	začít
střílet	střílet	k5eAaImF	střílet
chaoticky	chaoticky	k6eAd1	chaoticky
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nepadly	padnout	k5eNaPmAgInP	padnout
všechny	všechen	k3xTgFnPc4	všechen
zamýšlené	zamýšlený	k2eAgFnPc4d1	zamýšlená
oběti	oběť	k1gFnPc4	oběť
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Otevřely	otevřít	k5eAaPmAgFnP	otevřít
se	se	k3xPyFc4	se
dveře	dveře	k1gFnPc1	dveře
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
rozptýlil	rozptýlit	k5eAaPmAgInS	rozptýlit
kouř	kouř	k1gInSc1	kouř
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
přeživší	přeživší	k2eAgFnPc1d1	přeživší
ještě	ještě	k9	ještě
ubodal	ubodat	k5eAaPmAgMnS	ubodat
bolševik	bolševik	k1gMnSc1	bolševik
Pjotr	Pjotr	k1gMnSc1	Pjotr
Jermakov	Jermakov	k1gInSc4	Jermakov
bajonetem	bajonet	k1gInSc7	bajonet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
střelba	střelba	k1gFnSc1	střelba
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
slyšet	slyšet	k5eAaImF	slyšet
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
zemřely	zemřít	k5eAaPmAgFnP	zemřít
Taťána	Taťána	k1gFnSc1	Taťána
<g/>
,	,	kIx,	,
Anastázie	Anastázie	k1gFnSc1	Anastázie
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
měly	mít	k5eAaImAgFnP	mít
přes	přes	k7c4	přes
1	[number]	k4	1
kg	kg	kA	kg
diamantů	diamant	k1gInPc2	diamant
všitých	všitý	k2eAgInPc2d1	všitý
do	do	k7c2	do
jejich	jejich	k3xOp3gInPc2	jejich
oděvů	oděv	k1gInPc2	oděv
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
chráněny	chráněn	k2eAgFnPc4d1	chráněna
před	před	k7c7	před
střelami	střela	k1gFnPc7	střela
<g/>
.	.	kIx.	.
<g/>
Popraven	popravit	k5eAaPmNgMnS	popravit
byl	být	k5eAaImAgMnS	být
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Alexandra	Alexandr	k1gMnSc2	Alexandr
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc2	jejich
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
Olga	Olga	k1gFnSc1	Olga
<g/>
,	,	kIx,	,
Taťána	Taťána	k1gFnSc1	Taťána
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
Anastázie	Anastázie	k1gFnSc1	Anastázie
a	a	k8xC	a
Alexej	Alexej	k1gMnSc1	Alexej
<g/>
)	)	kIx)	)
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
jít	jít	k5eAaImF	jít
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
do	do	k7c2	do
exilu	exil	k1gInSc6	exil
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
lékař	lékař	k1gMnSc1	lékař
Evžen	Evžen	k1gMnSc1	Evžen
Botkin	Botkin	k1gMnSc1	Botkin
<g/>
,	,	kIx,	,
služebná	služebný	k2eAgFnSc1d1	služebná
Anna	Anna	k1gFnSc1	Anna
Demidová	Demidový	k2eAgFnSc1d1	Demidový
<g/>
,	,	kIx,	,
Alexej	Alexej	k1gMnSc1	Alexej
Trupp	Trupp	k1gMnSc1	Trupp
a	a	k8xC	a
Ivan	Ivan	k1gMnSc1	Ivan
Charitonov	Charitonov	k1gInSc1	Charitonov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Následky	následek	k1gInPc4	následek
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
oznámení	oznámení	k1gNnPc1	oznámení
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgNnP	objevit
v	v	k7c6	v
celostátním	celostátní	k2eAgInSc6d1	celostátní
tisku	tisk	k1gInSc6	tisk
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
ohlášeno	ohlášen	k2eAgNnSc1d1	ohlášeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
monarcha	monarcha	k1gMnSc1	monarcha
byl	být	k5eAaImAgMnS	být
popraven	popravit	k5eAaPmNgMnS	popravit
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
Uralu	Ural	k1gInSc2	Ural
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
představovali	představovat	k5eAaImAgMnP	představovat
blížící	blížící	k2eAgFnSc3d1	blížící
se	se	k3xPyFc4	se
Čechoslováci	Čechoslovák	k1gMnPc5	Čechoslovák
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
verze	verze	k1gFnSc2	verze
bylo	být	k5eAaImAgNnS	být
zavraždění	zavraždění	k1gNnSc1	zavraždění
vykonáno	vykonat	k5eAaPmNgNnS	vykonat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
"	"	kIx"	"
<g/>
tajného	tajný	k2eAgNnSc2d1	tajné
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
"	"	kIx"	"
sovětského	sovětský	k2eAgNnSc2d1	sovětské
politbyra	politbyro	k1gNnSc2	politbyro
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Leninem	Lenin	k1gMnSc7	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
oficiální	oficiální	k2eAgInSc1d1	oficiální
sovět	sovět	k1gInSc1	sovět
vinil	vinit	k5eAaImAgInS	vinit
z	z	k7c2	z
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
za	za	k7c4	za
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
Uralský	uralský	k2eAgInSc1d1	uralský
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
Lev	Lev	k1gMnSc1	Lev
Davidovič	Davidovič	k1gMnSc1	Davidovič
Trockij	Trockij	k1gMnSc1	Trockij
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
deníku	deník	k1gInSc6	deník
údajně	údajně	k6eAd1	údajně
odkazoval	odkazovat	k5eAaImAgMnS	odkazovat
na	na	k7c4	na
Lenina	Lenin	k1gMnSc4	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Trockij	Trockít	k5eAaPmRp2nS	Trockít
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
:	:	kIx,	:
K	k	k7c3	k
mojí	můj	k3xOp1gFnSc3	můj
další	další	k2eAgFnSc3d1	další
návštěvě	návštěva	k1gFnSc3	návštěva
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Jekatěrinburgu	Jekatěrinburg	k1gInSc2	Jekatěrinburg
<g/>
.	.	kIx.	.
</s>
<s>
Mluvil	mluvit	k5eAaImAgMnS	mluvit
jsem	být	k5eAaImIp1nS	být
se	s	k7c7	s
Sverdlovem	Sverdlovo	k1gNnSc7	Sverdlovo
a	a	k8xC	a
zeptal	zeptat	k5eAaPmAgMnS	zeptat
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
mimochodem	mimochodem	k6eAd1	mimochodem
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Ach	ach	k0	ach
ano	ano	k9	ano
<g/>
,	,	kIx,	,
a	a	k8xC	a
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
car	car	k1gMnSc1	car
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Byl	být	k5eAaImAgMnS	být
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
A	a	k9	a
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Byla	být	k5eAaImAgFnS	být
zastřelena	zastřelit	k5eAaPmNgFnS	zastřelit
i	i	k9	i
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Zeptal	zeptat	k5eAaPmAgMnS	zeptat
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
s	s	k7c7	s
nádechem	nádech	k1gInSc7	nádech
překvapení	překvapení	k1gNnSc2	překvapení
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
"	"	kIx"	"
odpověděl	odpovědět	k5eAaPmAgInS	odpovědět
Jakov	Jakov	k1gInSc1	Jakov
Sverdlov	Sverdlov	k1gInSc4	Sverdlov
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Co	co	k9	co
ty	ty	k3xPp2nSc1	ty
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Čekal	čekat	k5eAaImAgInS	čekat
na	na	k7c4	na
mou	můj	k3xOp1gFnSc4	můj
reakci	reakce	k1gFnSc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Neodpověděl	odpovědět	k5eNaPmAgMnS	odpovědět
jsem	být	k5eAaImIp1nS	být
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
A	a	k8xC	a
kdo	kdo	k3yRnSc1	kdo
udělal	udělat	k5eAaPmAgMnS	udělat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Ptal	ptat	k5eAaImAgMnS	ptat
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
jsme	být	k5eAaImIp1nP	být
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
my	my	k3xPp1nPc1	my
tady	tady	k6eAd1	tady
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Iljič	Iljič	k1gMnSc1	Iljič
Lenin	Lenin	k1gMnSc1	Lenin
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bychom	by	kYmCp1nP	by
neměli	mít	k5eNaImAgMnP	mít
bílým	bílý	k1gMnSc7	bílý
ponechávat	ponechávat	k5eAaImF	ponechávat
živoucí	živoucí	k2eAgInSc4d1	živoucí
symbol	symbol	k1gInSc4	symbol
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
kterého	který	k3yQgNnSc2	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
soustřeďovali	soustřeďovat	k5eAaImAgMnP	soustřeďovat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nynějším	nynější	k2eAgInPc3d1	nynější
složitým	složitý	k2eAgInPc3d1	složitý
poměrům	poměr	k1gInPc3	poměr
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejněn	k2eAgInSc1d1	zveřejněn
report	report	k1gInSc1	report
Jakova	jakův	k2eAgMnSc2d1	jakův
Jurovského	Jurovský	k1gMnSc2	Jurovský
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
reportu	report	k1gInSc2	report
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
se	se	k3xPyFc4	se
jednotky	jednotka	k1gFnPc1	jednotka
Československé	československý	k2eAgFnSc2d1	Československá
legie	legie	k1gFnSc2	legie
blížily	blížit	k5eAaImAgFnP	blížit
k	k	k7c3	k
Jekatěrinburgu	Jekatěrinburg	k1gInSc3	Jekatěrinburg
<g/>
,	,	kIx,	,
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
Legie	legie	k1gFnPc1	legie
dobyly	dobýt	k5eAaPmAgFnP	dobýt
město	město	k1gNnSc4	město
a	a	k8xC	a
osvobodily	osvobodit	k5eAaPmAgFnP	osvobodit
cara	car	k1gMnSc2	car
<g/>
,	,	kIx,	,
Jakov	Jakov	k1gInSc1	Jakov
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
bolševičtí	bolševický	k2eAgMnPc1d1	bolševický
věznitelé	věznitel	k1gMnPc1	věznitel
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
popravili	popravit	k5eAaPmAgMnP	popravit
Nikolaje	Nikolaj	k1gMnPc4	Nikolaj
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
odjel	odjet	k5eAaPmAgMnS	odjet
Jurovskij	Jurovskij	k1gMnSc1	Jurovskij
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
se	s	k7c7	s
zprávou	zpráva	k1gFnSc7	zpráva
pro	pro	k7c4	pro
Sverdlova	Sverdlův	k2eAgMnSc4d1	Sverdlův
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Čechoslováci	Čechoslovák	k1gMnPc1	Čechoslovák
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Jekatěrinburg	Jekatěrinburg	k1gInSc4	Jekatěrinburg
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
byt	byt	k1gInSc1	byt
vydrancován	vydrancován	k2eAgInSc1d1	vydrancován
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
jistá	jistý	k2eAgFnSc1d1	jistá
Anna	Anna	k1gFnSc1	Anna
Andersonová	Andersonová	k1gFnSc1	Andersonová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
o	o	k7c6	o
sobě	se	k3xPyFc3	se
prohlašovala	prohlašovat	k5eAaImAgFnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nejmladší	mladý	k2eAgFnSc7d3	nejmladší
dcerou	dcera	k1gFnSc7	dcera
carského	carský	k2eAgInSc2d1	carský
páru	pár	k1gInSc2	pár
<g/>
,	,	kIx,	,
Anastázií	Anastázie	k1gFnPc2	Anastázie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
říčního	říční	k2eAgInSc2d1	říční
berlínského	berlínský	k2eAgInSc2d1	berlínský
kanálu	kanál	k1gInSc2	kanál
byla	být	k5eAaImAgFnS	být
zachráněna	zachráněn	k2eAgFnSc1d1	zachráněna
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byla	být	k5eAaImAgFnS	být
převezena	převézt	k5eAaPmNgFnS	převézt
na	na	k7c4	na
psychiatrickou	psychiatrický	k2eAgFnSc4d1	psychiatrická
kliniku	klinika	k1gFnSc4	klinika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dívka	dívka	k1gFnSc1	dívka
personálu	personál	k1gInSc2	personál
svěřila	svěřit	k5eAaPmAgFnS	svěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
velkokněžna	velkokněžna	k1gFnSc1	velkokněžna
Anastázie	Anastázie	k1gFnSc1	Anastázie
<g/>
,	,	kIx,	,
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
verzi	verze	k1gFnSc4	verze
podpořila	podpořit	k5eAaPmAgFnS	podpořit
vyprávěním	vyprávění	k1gNnSc7	vyprávění
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
úniku	únik	k1gInSc6	únik
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
katů	kat	k1gMnPc2	kat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
z	z	k7c2	z
ústavu	ústav	k1gInSc2	ústav
si	se	k3xPyFc3	se
říkala	říkat	k5eAaImAgFnS	říkat
Anastázie	Anastázie	k1gFnSc1	Anastázie
Čajkovská	Čajkovský	k2eAgFnSc1d1	Čajkovský
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Anna	Anna	k1gFnSc1	Anna
Andersonová	Andersonová	k1gFnSc1	Andersonová
a	a	k8xC	a
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
léta	léto	k1gNnPc1	léto
soudně	soudně	k6eAd1	soudně
usilovala	usilovat	k5eAaImAgNnP	usilovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jí	on	k3xPp3gFnSc3	on
byla	být	k5eAaImAgFnS	být
vrácena	vrácen	k2eAgFnSc1d1	vrácena
"	"	kIx"	"
<g/>
pravá	pravý	k2eAgFnSc1d1	pravá
identita	identita	k1gFnSc1	identita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
svědčilo	svědčit	k5eAaImAgNnS	svědčit
i	i	k9	i
tvrzení	tvrzení	k1gNnSc1	tvrzení
jisté	jistý	k2eAgNnSc1d1	jisté
paní	paní	k1gFnSc7	paní
Wingender	Wingendra	k1gFnPc2	Wingendra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
poznala	poznat	k5eAaPmAgFnS	poznat
polskou	polský	k2eAgFnSc4d1	polská
tovární	tovární	k2eAgFnSc4d1	tovární
dělnici	dělnice	k1gFnSc4	dělnice
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Spekulace	spekulace	k1gFnPc1	spekulace
o	o	k7c6	o
živé	živý	k2eAgFnSc6d1	živá
Anastázii	Anastázie	k1gFnSc6	Anastázie
utichly	utichnout	k5eAaPmAgFnP	utichnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
její	její	k3xOp3gInPc1	její
ostatky	ostatek	k1gInPc1	ostatek
nalezeny	nalézt	k5eAaBmNgInP	nalézt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
většiny	většina	k1gFnSc2	většina
členů	člen	k1gMnPc2	člen
carské	carský	k2eAgFnSc2d1	carská
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
služebných	služebná	k1gFnPc2	služebná
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1979	[number]	k4	1979
amatérskými	amatérský	k2eAgMnPc7d1	amatérský
nadšenci	nadšenec	k1gMnPc7	nadšenec
<g/>
,	,	kIx,	,
objev	objev	k1gInSc1	objev
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
uchován	uchován	k2eAgInSc1d1	uchován
v	v	k7c6	v
tajnosti	tajnost	k1gFnSc6	tajnost
až	až	k9	až
do	do	k7c2	do
rozpadu	rozpad	k1gInSc2	rozpad
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Exhumovány	exhumován	k2eAgFnPc1d1	exhumována
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1991	[number]	k4	1991
pod	pod	k7c7	pod
náspem	násep	k1gInSc7	násep
Staré	Staré	k2eAgFnSc2d1	Staré
koptjakovské	koptjakovský	k2eAgFnSc2d1	koptjakovský
cesty	cesta	k1gFnSc2	cesta
19	[number]	k4	19
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Jekatěrinburgu	Jekatěrinburg	k1gInSc2	Jekatěrinburg
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
polité	politý	k2eAgFnSc6d1	politá
kyselinou	kyselina	k1gFnSc7	kyselina
a	a	k8xC	a
spálené	spálený	k2eAgNnSc1d1	spálené
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vedla	vést	k5eAaImAgFnS	vést
Prokuratura	prokuratura	k1gFnSc1	prokuratura
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Romanovci	Romanovec	k1gMnPc1	Romanovec
identifikováni	identifikovat	k5eAaBmNgMnP	identifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Vzorky	vzorek	k1gInPc1	vzorek
DNA	DNA	kA	DNA
také	také	k6eAd1	také
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
příbuzenský	příbuzenský	k2eAgInSc4d1	příbuzenský
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Princem	princ	k1gMnSc7	princ
Philipem	Philip	k1gMnSc7	Philip
<g/>
,	,	kIx,	,
vévodou	vévoda	k1gMnSc7	vévoda
z	z	k7c2	z
Edinburghu	Edinburgh	k1gInSc2	Edinburgh
<g/>
,	,	kIx,	,
manželem	manžel	k1gMnSc7	manžel
britské	britský	k2eAgFnSc2d1	britská
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1998	[number]	k4	1998
byly	být	k5eAaImAgInP	být
ostatky	ostatek	k1gInPc1	ostatek
členů	člen	k1gMnPc2	člen
carské	carský	k2eAgFnSc2d1	carská
rodiny	rodina	k1gFnSc2	rodina
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
Sankt-Petěrburgu	Sankt-Petěrburg	k1gInSc2	Sankt-Petěrburg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
pochovány	pochovat	k5eAaPmNgInP	pochovat
v	v	k7c6	v
Petropavlovském	petropavlovský	k2eAgInSc6d1	petropavlovský
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbu	pohřeb	k1gInSc2	pohřeb
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
prezident	prezident	k1gMnSc1	prezident
Boris	Boris	k1gMnSc1	Boris
Jelcin	Jelcin	k1gMnSc1	Jelcin
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
příbuznými	příbuzná	k1gFnPc7	příbuzná
Romanovců	Romanovec	k1gMnPc2	Romanovec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
prince	princ	k1gMnSc2	princ
Michaela	Michael	k1gMnSc2	Michael
z	z	k7c2	z
Kentu	Kent	k1gInSc2	Kent
(	(	kIx(	(
<g/>
bratranec	bratranec	k1gMnSc1	bratranec
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgNnPc4d1	zbývající
dvě	dva	k4xCgNnPc4	dva
těla	tělo	k1gNnPc4	tělo
prince	princ	k1gMnSc2	princ
Alexeje	Alexej	k1gMnSc2	Alexej
a	a	k8xC	a
princezny	princezna	k1gFnSc2	princezna
Marie	Maria	k1gFnSc2	Maria
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
nedaleko	nedaleko	k7c2	nedaleko
nálezu	nález	k1gInSc2	nález
ostatků	ostatek	k1gInPc2	ostatek
rodiny	rodina	k1gFnSc2	rodina
<g/>
.14	.14	k4	.14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
ruská	ruský	k2eAgFnSc1d1	ruská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
cara	car	k1gMnSc2	car
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
členy	člen	k1gMnPc7	člen
carské	carský	k2eAgFnSc2d1	carská
rodiny	rodina	k1gFnSc2	rodina
za	za	k7c4	za
svaté	svatá	k1gFnPc4	svatá
(	(	kIx(	(
<g/>
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
strastotěrpec	strastotěrpec	k1gMnSc1	strastotěrpec
-	-	kIx~	-
trpitel	trpitel	k1gMnSc1	trpitel
<g/>
,	,	kIx,	,
mučedník	mučedník	k1gMnSc1	mučedník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ipaťjevův	Ipaťjevův	k2eAgInSc1d1	Ipaťjevův
dům	dům	k1gInSc1	dům
byl	být	k5eAaImAgInS	být
zdemolován	zdemolovat	k5eAaPmNgInS	zdemolovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
místě	místo	k1gNnSc6	místo
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
85	[number]	k4	85
let	léto	k1gNnPc2	léto
po	po	k7c6	po
popravě	poprava	k1gFnSc6	poprava
carské	carský	k2eAgFnSc2d1	carská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
vzpomínkový	vzpomínkový	k2eAgInSc1d1	vzpomínkový
Chrám	chrám	k1gInSc1	chrám
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
na	na	k7c6	na
krvi	krev	k1gFnSc6	krev
věnovaný	věnovaný	k2eAgInSc4d1	věnovaný
rodině	rodina	k1gFnSc3	rodina
Romanovových	Romanovových	k2eAgMnPc1d1	Romanovových
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Romanovci	Romanovec	k1gMnPc1	Romanovec
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
příbuzní	příbuzný	k2eAgMnPc1d1	příbuzný
zabití	zabitý	k1gMnPc1	zabitý
bolševiky	bolševik	k1gMnPc7	bolševik
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
rituální	rituální	k2eAgFnSc6d1	rituální
vraždě	vražda	k1gFnSc6	vražda
==	==	k?	==
</s>
</p>
<p>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
se	se	k3xPyFc4	se
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
znovu	znovu	k6eAd1	znovu
přezkoumat	přezkoumat	k5eAaPmF	přezkoumat
ostatky	ostatek	k1gInPc4	ostatek
Mikuláše	mikuláš	k1gInSc2	mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
rodiny	rodina	k1gFnPc1	rodina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgFnP	uložit
v	v	k7c6	v
petrohradském	petrohradský	k2eAgInSc6d1	petrohradský
Petropavlovském	petropavlovský	k2eAgInSc6d1	petropavlovský
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
církvi	církev	k1gFnSc6	církev
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
objevily	objevit	k5eAaPmAgInP	objevit
hlasy	hlas	k1gInPc1	hlas
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečné	skutečný	k2eAgInPc1d1	skutečný
ostatky	ostatek	k1gInPc1	ostatek
se	se	k3xPyFc4	se
nezachovaly	zachovat	k5eNaPmAgInP	zachovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	on	k3xPp3gMnPc4	on
vrazi	vrah	k1gMnPc1	vrah
zničili	zničit	k5eAaPmAgMnP	zničit
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zahladili	zahladit	k5eAaPmAgMnP	zahladit
stopy	stopa	k1gFnPc4	stopa
rituální	rituální	k2eAgFnSc2d1	rituální
židovské	židovský	k2eAgFnSc2d1	židovská
vraždy	vražda	k1gFnSc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
stoupenci	stoupenec	k1gMnPc1	stoupenec
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jekatěrinburgská	jekatěrinburgský	k2eAgFnSc1d1	jekatěrinburgský
židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
"	"	kIx"	"
<g/>
těla	tělo	k1gNnPc1	tělo
Romanovců	Romanovec	k1gMnPc2	Romanovec
spálila	spálit	k5eAaPmAgFnS	spálit
a	a	k8xC	a
popíjela	popíjet	k5eAaImAgFnS	popíjet
čaj	čaj	k1gInSc4	čaj
s	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
popelem	popel	k1gInSc7	popel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2017	[number]	k4	2017
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
biskup	biskup	k1gMnSc1	biskup
Tichon	Tichon	k1gMnSc1	Tichon
Ševkunov	Ševkunov	k1gInSc4	Ševkunov
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
církevní	církevní	k2eAgFnSc2d1	církevní
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
vraždu	vražda	k1gFnSc4	vražda
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
této	tento	k3xDgFnSc2	tento
komise	komise	k1gFnSc2	komise
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
rituální	rituální	k2eAgFnSc1d1	rituální
vražda	vražda	k1gFnSc1	vražda
byla	být	k5eAaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Marina	Marina	k1gFnSc1	Marina
Molodcova	molodcův	k2eAgFnSc1d1	molodcův
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
vyšetřovatelů	vyšetřovatel	k1gMnPc2	vyšetřovatel
speciální	speciální	k2eAgFnSc2d1	speciální
komise	komise	k1gFnSc2	komise
misterstva	misterstvo	k1gNnSc2	misterstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gInSc1	její
tým	tým	k1gInSc1	tým
provede	provést	k5eAaPmIp3nS	provést
"	"	kIx"	"
<g/>
psychologicko-historický	psychologickoistorický	k2eAgInSc1d1	psychologicko-historický
výzkum	výzkum	k1gInSc1	výzkum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
prozkoumá	prozkoumat	k5eAaPmIp3nS	prozkoumat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nebyla	být	k5eNaImAgFnS	být
vražda	vražda	k1gFnSc1	vražda
cara	car	k1gMnSc2	car
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
rituální	rituální	k2eAgFnSc2d1	rituální
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
názory	názor	k1gInPc1	názor
vycházejí	vycházet	k5eAaImIp3nP	vycházet
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
bolševické	bolševický	k2eAgFnSc2d1	bolševická
strany	strana	k1gFnSc2	strana
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
při	při	k7c6	při
popravě	poprava	k1gFnSc6	poprava
carské	carský	k2eAgFnSc2d1	carská
rodiny	rodina	k1gFnSc2	rodina
měli	mít	k5eAaImAgMnP	mít
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
bolševici	bolševik	k1gMnPc1	bolševik
s	s	k7c7	s
židovskými	židovský	k2eAgInPc7d1	židovský
kořeny	kořen	k1gInPc7	kořen
(	(	kIx(	(
<g/>
Jakov	Jakov	k1gInSc1	Jakov
Jurovskij	Jurovskij	k1gFnSc2	Jurovskij
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Federace	federace	k1gFnSc1	federace
židovských	židovský	k2eAgFnPc2d1	židovská
komunit	komunita	k1gFnPc2	komunita
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
vydala	vydat	k5eAaPmAgFnS	vydat
27	[number]	k4	27
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
prohlášení	prohlášení	k1gNnPc2	prohlášení
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
tyto	tento	k3xDgInPc4	tento
výroky	výrok	k1gInPc4	výrok
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
šokující	šokující	k2eAgNnSc4d1	šokující
vyjádření	vyjádření	k1gNnSc4	vyjádření
antisemitské	antisemitský	k2eAgFnSc2d1	antisemitská
pověry	pověra	k1gFnSc2	pověra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
tyto	tento	k3xDgInPc4	tento
výroky	výrok	k1gInPc4	výrok
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
i	i	k9	i
Evropsko-asijský	evropskosijský	k2eAgInSc4d1	evropsko-asijský
židovský	židovský	k2eAgInSc4d1	židovský
kongres	kongres	k1gInSc4	kongres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
proti	proti	k7c3	proti
pověře	pověra	k1gFnSc3	pověra
o	o	k7c6	o
židovské	židovský	k2eAgFnSc6d1	židovská
rituální	rituální	k2eAgFnSc6d1	rituální
vraždě	vražda	k1gFnSc6	vražda
ostře	ostro	k6eAd1	ostro
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gInSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
v	v	k7c6	v
období	období	k1gNnSc6	období
tzv.	tzv.	kA	tzv.
hilsneriády	hilsneriáda	k1gFnSc2	hilsneriáda
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SOKOLOV	Sokolov	k1gInSc1	Sokolov
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Aleksejevič	Aleksejevič	k1gMnSc1	Aleksejevič
<g/>
.	.	kIx.	.
</s>
<s>
Zavraždění	zavraždění	k1gNnSc1	zavraždění
carské	carský	k2eAgFnSc2d1	carská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
Konstantin	Konstantin	k1gMnSc1	Konstantin
Kessler	Kessler	k1gMnSc1	Kessler
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
298	[number]	k4	298
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
