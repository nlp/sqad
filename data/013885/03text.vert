<s>
Massa	Massa	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
italské	italský	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
Massa-Carrara	Massa-Carrara	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Massa	Massa	k1gFnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Massa	Massa	k1gFnSc1
Tuscany	Tuscana	k1gFnSc2
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
44	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
10	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
65	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
region	region	k1gInSc1
</s>
<s>
Toskánsko	Toskánsko	k1gNnSc1
Provincie	provincie	k1gFnSc2
</s>
<s>
Massa-Carrara	Massa-Carrara	k1gFnSc1
Administrativní	administrativní	k2eAgFnSc1d1
dělení	dělení	k1gNnSc4
</s>
<s>
Massa	Massa	k1gFnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
94	#num#	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
66	#num#	k4
097	#num#	k4
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
703,2	703,2	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.comune.massa.ms.it	www.comune.massa.ms.it	k1gInSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+39	+39	k4
585	#num#	k4
PSČ	PSČ	kA
</s>
<s>
54100	#num#	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
MS	MS	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Massa	Massa	k1gFnSc1
je	být	k5eAaImIp3nS
italské	italský	k2eAgNnSc1d1
město	město	k1gNnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Toskánsko	Toskánsko	k1gNnSc1
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc1
ze	z	k7c2
dvou	dva	k4xCgNnPc2
center	centrum	k1gNnPc2
provincie	provincie	k1gFnSc2
Massa-Carrara	Massa-Carrara	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Massa	Mass	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Toskánsko	Toskánsko	k1gNnSc1
•	•	k?
Obce	obec	k1gFnSc2
v	v	k7c6
provincii	provincie	k1gFnSc6
Massa-Carrara	Massa-Carrar	k1gMnSc2
</s>
<s>
Aulla	Aulla	k1gFnSc1
•	•	k?
Bagnone	Bagnon	k1gInSc5
•	•	k?
Carrara	Carrara	k1gFnSc1
•	•	k?
Casola	Casola	k1gFnSc1
in	in	k?
Lunigiana	Lunigiana	k1gFnSc1
•	•	k?
Comano	Comana	k1gFnSc5
•	•	k?
Filattiera	Filattier	k1gMnSc2
•	•	k?
Fivizzano	Fivizzana	k1gFnSc5
•	•	k?
Fosdinovo	Fosdinův	k2eAgNnSc1d1
•	•	k?
Licciana	Liccian	k1gMnSc2
Nardi	Nard	k1gMnPc1
•	•	k?
Massa	Massa	k1gFnSc1
•	•	k?
Montignoso	Montignosa	k1gFnSc5
•	•	k?
Mulazzo	Mulazza	k1gFnSc5
•	•	k?
Podenzana	Podenzan	k1gMnSc2
•	•	k?
Pontremoli	Pontremole	k1gFnSc4
•	•	k?
Tresana	Tresana	k1gFnSc1
•	•	k?
Villafranca	Villafrancus	k1gMnSc2
in	in	k?
Lunigiana	Lunigian	k1gMnSc2
•	•	k?
Zeri	Zer	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
671386	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4037857-3	4037857-3	k4
</s>
