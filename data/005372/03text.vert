<s>
Žralok	žralok	k1gMnSc1	žralok
bílý	bílý	k1gMnSc1	bílý
(	(	kIx(	(
<g/>
Carcharodon	Carcharodon	k1gMnSc1	Carcharodon
carcharias	carcharias	k1gMnSc1	carcharias
<g/>
)	)	kIx)	)
či	či	k8xC	či
velký	velký	k2eAgMnSc1d1	velký
bílý	bílý	k1gMnSc1	bílý
žralok	žralok	k1gMnSc1	žralok
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
často	často	k6eAd1	často
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
žralok	žralok	k1gMnSc1	žralok
lidožravý	lidožravý	k2eAgMnSc1d1	lidožravý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
žralok	žralok	k1gMnSc1	žralok
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lamnovití	lamnovitý	k2eAgMnPc1d1	lamnovitý
vyskytující	vyskytující	k2eAgFnSc7d1	vyskytující
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
vodách	voda	k1gFnPc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maximální	maximální	k2eAgFnPc4d1	maximální
délky	délka	k1gFnPc4	délka
přes	přes	k7c4	přes
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
i	i	k9	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
dospělý	dospělý	k2eAgMnSc1d1	dospělý
bývá	bývat	k5eAaImIp3nS	bývat
až	až	k9	až
po	po	k7c6	po
26	[number]	k4	26
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
dožívá	dožívat	k5eAaImIp3nS	dožívat
se	se	k3xPyFc4	se
i	i	k9	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejdéle	dlouho	k6eAd3	dlouho
žijících	žijící	k2eAgFnPc2d1	žijící
chrupavčitých	chrupavčitý	k2eAgFnPc2d1	chrupavčitá
paryb	paryba	k1gFnPc2	paryba
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejrychlejším	rychlý	k2eAgMnPc3d3	nejrychlejší
žralokům	žralok	k1gMnPc3	žralok
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
nakrátko	nakrátko	k6eAd1	nakrátko
zrychlit	zrychlit	k5eAaPmF	zrychlit
až	až	k9	až
na	na	k7c4	na
56	[number]	k4	56
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
člověku	člověk	k1gMnSc6	člověk
velmi	velmi	k6eAd1	velmi
nebezpečného	bezpečný	k2eNgMnSc4d1	nebezpečný
predátora	predátor	k1gMnSc4	predátor
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
nejvíce	nejvíce	k6eAd1	nejvíce
nevyprovokovaných	vyprovokovaný	k2eNgInPc2d1	nevyprovokovaný
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
žraloky	žralok	k1gMnPc7	žralok
a	a	k8xC	a
jako	jako	k8xC	jako
takový	takový	k3xDgInSc1	takový
byl	být	k5eAaImAgInS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
systematicky	systematicky	k6eAd1	systematicky
huben	huben	k2eAgInSc1d1	huben
námořníky	námořník	k1gMnPc7	námořník
a	a	k8xC	a
rybáři	rybář	k1gMnPc7	rybář
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
pronásledování	pronásledování	k1gNnSc1	pronásledování
ještě	ještě	k6eAd1	ještě
zesílilo	zesílit	k5eAaPmAgNnS	zesílit
po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
filmu	film	k1gInSc2	film
Čelisti	čelist	k1gFnSc2	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
tito	tento	k3xDgMnPc1	tento
žraloci	žralok	k1gMnPc1	žralok
člověka	člověk	k1gMnSc4	člověk
systematicky	systematicky	k6eAd1	systematicky
neloví	lovit	k5eNaImIp3nS	lovit
<g/>
,	,	kIx,	,
smrtelných	smrtelný	k2eAgInPc2d1	smrtelný
útoků	útok	k1gInPc2	útok
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
jen	jen	k9	jen
1,5	[number]	k4	1,5
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
především	především	k9	především
mořskými	mořský	k2eAgFnPc7d1	mořská
rybami	ryba	k1gFnPc7	ryba
a	a	k8xC	a
savci	savec	k1gMnPc7	savec
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
ploutvonožci	ploutvonožec	k1gMnPc1	ploutvonožec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
i	i	k9	i
plazy	plaz	k1gMnPc4	plaz
a	a	k8xC	a
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
jediným	jediný	k2eAgMnSc7d1	jediný
predátorem	predátor	k1gMnSc7	predátor
mimo	mimo	k7c4	mimo
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
kosatka	kosatka	k1gFnSc1	kosatka
dravá	dravý	k2eAgFnSc1d1	dravá
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kritérií	kritérion	k1gNnPc2	kritérion
IUCN	IUCN	kA	IUCN
je	být	k5eAaImIp3nS	být
veden	veden	k2eAgInSc1d1	veden
jako	jako	k8xS	jako
zranitelný	zranitelný	k2eAgInSc1d1	zranitelný
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
brzké	brzký	k2eAgFnSc6d1	brzká
době	doba	k1gFnSc6	doba
čelit	čelit	k5eAaImF	čelit
hrozbě	hrozba	k1gFnSc3	hrozba
vyhubení	vyhubení	k1gNnSc2	vyhubení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1758	[number]	k4	1758
dal	dát	k5eAaPmAgInS	dát
Carl	Carl	k1gInSc4	Carl
Linné	Linné	k2eAgMnSc3d1	Linné
velkému	velký	k2eAgMnSc3d1	velký
bílému	bílý	k1gMnSc3	bílý
žralokovi	žralok	k1gMnSc3	žralok
první	první	k4xOgNnSc4	první
vědecké	vědecký	k2eAgNnSc4d1	vědecké
pojmenování	pojmenování	k1gNnSc4	pojmenování
Squalus	Squalus	k1gMnSc1	Squalus
carcharias	carcharias	k1gMnSc1	carcharias
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
změnil	změnit	k5eAaPmAgMnS	změnit
zoolog	zoolog	k1gMnSc1	zoolog
Andrew	Andrew	k1gMnSc1	Andrew
Smith	Smith	k1gMnSc1	Smith
rodové	rodový	k2eAgNnSc4d1	rodové
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
Carcharodon	Carcharodon	k1gInSc4	Carcharodon
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
roztřepený	roztřepený	k2eAgInSc4d1	roztřepený
zub	zub	k1gInSc4	zub
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
druh	druh	k1gInSc1	druh
se	se	k3xPyFc4	se
velký	velký	k2eAgMnSc1d1	velký
bílý	bílý	k1gMnSc1	bílý
žralok	žralok	k1gMnSc1	žralok
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
miocénu	miocén	k1gInSc6	miocén
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
známý	známý	k2eAgInSc1d1	známý
fosilní	fosilní	k2eAgInSc1d1	fosilní
nález	nález	k1gInSc1	nález
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
16	[number]	k4	16
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
starý	starý	k2eAgInSc1d1	starý
<g/>
,	,	kIx,	,
stáří	stáří	k1gNnSc1	stáří
druhu	druh	k1gInSc2	druh
pak	pak	k6eAd1	pak
bude	být	k5eAaImBp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Fylogeneze	fylogeneze	k1gFnSc1	fylogeneze
je	být	k5eAaImIp3nS	být
však	však	k9	však
poněkud	poněkud	k6eAd1	poněkud
nejasná	jasný	k2eNgFnSc1d1	nejasná
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
obřím	obří	k2eAgInSc7d1	obří
druhem	druh	k1gInSc7	druh
Carcharodon	Carcharodon	k1gNnSc4	Carcharodon
megalodon	megalodona	k1gFnPc2	megalodona
sdíleli	sdílet	k5eAaImAgMnP	sdílet
společného	společný	k2eAgMnSc4d1	společný
předka	předek	k1gMnSc4	předek
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
zuby	zub	k1gInPc1	zub
jsou	být	k5eAaImIp3nP	být
tvarově	tvarově	k6eAd1	tvarově
podobné	podobný	k2eAgInPc1d1	podobný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
megalodon	megalodon	k1gInSc1	megalodon
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
podstatně	podstatně	k6eAd1	podstatně
větších	veliký	k2eAgInPc2d2	veliký
rozměrů	rozměr	k1gInPc2	rozměr
(	(	kIx(	(
<g/>
až	až	k9	až
17	[number]	k4	17
-	-	kIx~	-
18	[number]	k4	18
m	m	kA	m
a	a	k8xC	a
59	[number]	k4	59
tun	tuna	k1gFnPc2	tuna
<g/>
)	)	kIx)	)
než	než	k8xS	než
velký	velký	k2eAgMnSc1d1	velký
bílý	bílý	k1gMnSc1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
rozdílným	rozdílný	k2eAgInPc3d1	rozdílný
rozměrům	rozměr	k1gInPc3	rozměr
jsou	být	k5eAaImIp3nP	být
řazeni	řadit	k5eAaImNgMnP	řadit
do	do	k7c2	do
stejného	stejný	k2eAgInSc2d1	stejný
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
hypotéza	hypotéza	k1gFnSc1	hypotéza
nicméně	nicméně	k8xC	nicméně
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
příbuzní	příbuzný	k2eAgMnPc1d1	příbuzný
jen	jen	k9	jen
vzdáleně	vzdáleně	k6eAd1	vzdáleně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
čeledi	čeleď	k1gFnSc2	čeleď
Lamnidae	Lamnida	k1gFnSc2	Lamnida
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
blízkým	blízký	k2eAgMnPc3d1	blízký
příbuzným	příbuzný	k1gMnPc3	příbuzný
velkého	velký	k2eAgNnSc2d1	velké
bílého	bílé	k1gNnSc2	bílé
byl	být	k5eAaImAgMnS	být
archaický	archaický	k2eAgMnSc1d1	archaický
žralok	žralok	k1gMnSc1	žralok
mako	mako	k1gNnSc1	mako
Isurus	Isurus	k1gMnSc1	Isurus
hastalis	hastalis	k1gFnSc1	hastalis
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
podporovat	podporovat	k5eAaImF	podporovat
nález	nález	k1gInSc4	nález
222	[number]	k4	222
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
45	[number]	k4	45
obratlů	obratel	k1gInPc2	obratel
přechodného	přechodný	k2eAgInSc2d1	přechodný
druhu	druh	k1gInSc2	druh
Carcharodon	Carcharodona	k1gFnPc2	Carcharodona
hubbelli	hubbelnout	k5eAaImAgMnP	hubbelnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
publikace	publikace	k1gFnSc1	publikace
výsledků	výsledek	k1gInPc2	výsledek
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
hypotéza	hypotéza	k1gFnSc1	hypotéza
řadí	řadit	k5eAaImIp3nS	řadit
megalodona	megalodona	k1gFnSc1	megalodona
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Carcharocles	Carcharoclesa	k1gFnPc2	Carcharoclesa
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jsou	být	k5eAaImIp3nP	být
řazeni	řazen	k2eAgMnPc1d1	řazen
další	další	k2eAgMnPc1d1	další
velkozubí	velkozubý	k2eAgMnPc1d1	velkozubý
žraloci	žralok	k1gMnPc1	žralok
jako	jako	k9	jako
např.	např.	kA	např.
Otodus	Otodus	k1gMnSc1	Otodus
obliquus	obliquus	k1gMnSc1	obliquus
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
současných	současný	k2eAgInPc2d1	současný
druhů	druh	k1gInPc2	druh
má	mít	k5eAaImIp3nS	mít
bílý	bílý	k1gMnSc1	bílý
žralok	žralok	k1gMnSc1	žralok
nejblíže	blízce	k6eAd3	blízce
k	k	k7c3	k
žralokovi	žralok	k1gMnSc3	žralok
mako	mako	k1gNnSc1	mako
(	(	kIx(	(
<g/>
Isurus	Isurus	k1gMnSc1	Isurus
oxyrinchus	oxyrinchus	k1gMnSc1	oxyrinchus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žralokovi	žralok	k1gMnSc3	žralok
východnímu	východní	k2eAgMnSc3d1	východní
(	(	kIx(	(
<g/>
Isurus	Isurus	k1gMnSc1	Isurus
paucus	paucus	k1gMnSc1	paucus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žralokovi	žralok	k1gMnSc3	žralok
sleďovému	sleďový	k2eAgMnSc3d1	sleďový
(	(	kIx(	(
<g/>
Lamna	Lamn	k1gInSc2	Lamn
nasus	nasus	k1gInSc1	nasus
<g/>
)	)	kIx)	)
a	a	k8xC	a
žralokovi	žralok	k1gMnSc3	žralok
tichooceánskému	tichooceánský	k2eAgMnSc3d1	tichooceánský
(	(	kIx(	(
<g/>
Lamna	Lamn	k1gInSc2	Lamn
ditropis	ditropis	k1gInSc1	ditropis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgMnSc1d1	velký
bílý	bílý	k1gMnSc1	bílý
žralok	žralok	k1gMnSc1	žralok
žije	žít	k5eAaImIp3nS	žít
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
vodách	voda	k1gFnPc6	voda
rozličných	rozličný	k2eAgInPc2d1	rozličný
oceánů	oceán	k1gInPc2	oceán
a	a	k8xC	a
moří	moře	k1gNnPc2	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c4	mezi
12	[number]	k4	12
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
24	[number]	k4	24
°	°	k?	°
<g/>
C.	C.	kA	C.
Vyšší	vysoký	k2eAgInSc1d2	vyšší
výskyt	výskyt	k1gInSc1	výskyt
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
severovýchodu	severovýchod	k1gInSc2	severovýchod
USA	USA	kA	USA
a	a	k8xC	a
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc2d1	jižní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
některých	některý	k3yIgInPc2	některý
ostrovů	ostrov	k1gInPc2	ostrov
Oceánie	Oceánie	k1gFnSc2	Oceánie
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc6	Chile
a	a	k8xC	a
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
koncentrace	koncentrace	k1gFnSc1	koncentrace
je	být	k5eAaImIp3nS	být
poblíže	poblíže	k7c2	poblíže
ostrova	ostrov	k1gInSc2	ostrov
Dyer	Dyer	k1gInSc1	Dyer
nedaleko	nedaleko	k7c2	nedaleko
Kapského	kapský	k2eAgNnSc2d1	Kapské
města	město	k1gNnSc2	město
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
provádí	provádět	k5eAaImIp3nS	provádět
většina	většina	k1gFnSc1	většina
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgMnSc1d1	bílý
žralok	žralok	k1gMnSc1	žralok
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
epipelagickou	epipelagický	k2eAgFnSc4d1	epipelagický
parybu	paryba	k1gFnSc4	paryba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
hloubkách	hloubka	k1gFnPc6	hloubka
do	do	k7c2	do
200	[number]	k4	200
m.	m.	k?	m.
Žije	žít	k5eAaImIp3nS	žít
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jeho	jeho	k3xOp3gFnSc4	jeho
hlavní	hlavní	k2eAgFnSc4d1	hlavní
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
jsou	být	k5eAaImIp3nP	být
ploutvonožci	ploutvonožec	k1gMnPc1	ploutvonožec
<g/>
,	,	kIx,	,
menší	malý	k2eAgMnPc1d2	menší
kytovci	kytovec	k1gMnPc1	kytovec
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
žraloci	žralok	k1gMnPc1	žralok
a	a	k8xC	a
velké	velký	k2eAgFnPc1d1	velká
ryby	ryba	k1gFnPc1	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
otevřeném	otevřený	k2eAgInSc6d1	otevřený
oceánu	oceán	k1gInSc6	oceán
byli	být	k5eAaImAgMnP	být
žraloci	žralok	k1gMnPc1	žralok
bílí	bílit	k5eAaImIp3nP	bílit
zaznamenáni	zaznamenat	k5eAaPmNgMnP	zaznamenat
v	v	k7c6	v
hloubkách	hloubka	k1gFnPc6	hloubka
až	až	k9	až
1	[number]	k4	1
200	[number]	k4	200
m	m	kA	m
a	a	k8xC	a
tato	tento	k3xDgNnPc1	tento
pozorování	pozorování	k1gNnPc1	pozorování
zahýbala	zahýbat	k5eAaPmAgNnP	zahýbat
obecně	obecně	k6eAd1	obecně
přijímaným	přijímaný	k2eAgInSc7d1	přijímaný
předpokladem	předpoklad	k1gInSc7	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pobřežní	pobřežní	k2eAgInSc4d1	pobřežní
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
studie	studie	k1gFnPc1	studie
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žralok	žralok	k1gMnSc1	žralok
bílý	bílý	k1gMnSc1	bílý
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
migrací	migrace	k1gFnSc7	migrace
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornští	kalifornský	k2eAgMnPc1d1	kalifornský
jedinci	jedinec	k1gMnPc1	jedinec
se	se	k3xPyFc4	se
na	na	k7c6	na
asi	asi	k9	asi
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
měsíce	měsíc	k1gInSc2	měsíc
přesunují	přesunovat	k5eAaImIp3nP	přesunovat
k	k	k7c3	k
Havajskému	havajský	k2eAgNnSc3d1	havajské
souostroví	souostroví	k1gNnSc3	souostroví
a	a	k8xC	a
pak	pak	k6eAd1	pak
plují	plout	k5eAaImIp3nP	plout
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
potápějí	potápět	k5eAaImIp3nP	potápět
se	se	k3xPyFc4	se
do	do	k7c2	do
hloubek	hloubka	k1gFnPc2	hloubka
až	až	k9	až
900	[number]	k4	900
m.	m.	k?	m.
Podobně	podobně	k6eAd1	podobně
pendlují	pendlovat	k5eAaImIp3nP	pendlovat
mnohé	mnohý	k2eAgInPc1d1	mnohý
kusy	kus	k1gInPc1	kus
mezi	mezi	k7c7	mezi
Austrálií	Austrálie	k1gFnSc7	Austrálie
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc7d1	jižní
Afrikou	Afrika	k1gFnSc7	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
žralok	žralok	k1gMnSc1	žralok
takto	takto	k6eAd1	takto
urazil	urazit	k5eAaPmAgMnS	urazit
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
km	km	kA	km
za	za	k7c4	za
méně	málo	k6eAd2	málo
než	než	k8xS	než
9	[number]	k4	9
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Důvody	důvod	k1gInPc1	důvod
těchto	tento	k3xDgInPc2	tento
přesunů	přesun	k1gInPc2	přesun
neznáme	znát	k5eNaImIp1nP	znát
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
potravě	potrava	k1gFnSc3	potrava
či	či	k8xC	či
páření	páření	k1gNnSc3	páření
<g/>
.	.	kIx.	.
</s>
<s>
Žralok	žralok	k1gMnSc1	žralok
bílý	bílý	k1gMnSc1	bílý
má	mít	k5eAaImIp3nS	mít
zavalité	zavalitý	k2eAgNnSc4d1	zavalité
kónické	kónický	k2eAgNnSc4d1	kónické
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Ocasní	ocasní	k2eAgFnSc1d1	ocasní
ploutev	ploutev	k1gFnSc1	ploutev
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
symetrická	symetrický	k2eAgFnSc1d1	symetrická
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
přeci	přeci	k?	přeci
jen	jen	k6eAd1	jen
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
dolní	dolní	k2eAgFnSc1d1	dolní
<g/>
.	.	kIx.	.
</s>
<s>
Prsní	prsní	k2eAgFnPc1d1	prsní
ploutve	ploutev	k1gFnPc1	ploutev
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgFnPc1d1	velká
a	a	k8xC	a
široké	široký	k2eAgFnPc1d1	široká
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
zespodu	zespodu	k6eAd1	zespodu
bílá	bílý	k2eAgFnSc1d1	bílá
s	s	k7c7	s
černými	černý	k2eAgInPc7d1	černý
konci	konec	k1gInPc7	konec
<g/>
.	.	kIx.	.
</s>
<s>
Hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
ploutev	ploutev	k1gFnSc1	ploutev
má	mít	k5eAaImIp3nS	mít
klasický	klasický	k2eAgInSc4d1	klasický
trojúhelníkovitý	trojúhelníkovitý	k2eAgInSc4d1	trojúhelníkovitý
tvar	tvar	k1gInSc4	tvar
s	s	k7c7	s
mírným	mírný	k2eAgNnSc7d1	mírné
vykrojením	vykrojení	k1gNnSc7	vykrojení
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Mezery	mezera	k1gFnPc1	mezera
mezi	mezi	k7c7	mezi
žábrami	žábry	k1gFnPc7	žábry
se	se	k3xPyFc4	se
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
prsním	prsní	k2eAgFnPc3d1	prsní
ploutvím	ploutev	k1gFnPc3	ploutev
zužují	zužovat	k5eAaImIp3nP	zužovat
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgFnPc1d1	černá
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
opatřeny	opatřen	k2eAgInPc1d1	opatřen
mžurkou	mžurka	k1gFnSc7	mžurka
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
kůže	kůže	k1gFnSc2	kůže
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
a	a	k8xC	a
bocích	bok	k1gInPc6	bok
šedé	šedá	k1gFnSc2	šedá
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
nebo	nebo	k8xC	nebo
hnědým	hnědý	k2eAgInSc7d1	hnědý
nádechem	nádech	k1gInSc7	nádech
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
zřetelně	zřetelně	k6eAd1	zřetelně
odděleno	oddělit	k5eAaPmNgNnS	oddělit
od	od	k7c2	od
bílé	bílý	k2eAgFnSc2d1	bílá
spodní	spodní	k2eAgFnSc2d1	spodní
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zbarvení	zbarvení	k1gNnSc1	zbarvení
dělá	dělat	k5eAaImIp3nS	dělat
žraloka	žralok	k1gMnSc4	žralok
špatně	špatně	k6eAd1	špatně
viditelným	viditelný	k2eAgMnPc3d1	viditelný
zespodu	zespodu	k6eAd1	zespodu
(	(	kIx(	(
<g/>
splývá	splývat	k5eAaImIp3nS	splývat
se	se	k3xPyFc4	se
světlou	světlý	k2eAgFnSc7d1	světlá
hladinou	hladina	k1gFnSc7	hladina
<g/>
)	)	kIx)	)
a	a	k8xC	a
seshora	seshora	k6eAd1	seshora
(	(	kIx(	(
<g/>
splývá	splývat	k5eAaImIp3nS	splývat
se	s	k7c7	s
dnem	den	k1gInSc7	den
či	či	k8xC	či
tmavou	tmavý	k2eAgFnSc7d1	tmavá
modří	modř	k1gFnSc7	modř
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozbíjí	rozbíjet	k5eAaImIp3nS	rozbíjet
jeho	jeho	k3xOp3gFnSc4	jeho
siluetu	silueta	k1gFnSc4	silueta
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
boku	bok	k1gInSc2	bok
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
žraloci	žralok	k1gMnPc1	žralok
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
velký	velký	k2eAgMnSc1d1	velký
bílý	bílý	k1gMnSc1	bílý
řady	řada	k1gFnSc2	řada
jemně	jemně	k6eAd1	jemně
vroubkovaných	vroubkovaný	k2eAgInPc2d1	vroubkovaný
trojúhelníkovitých	trojúhelníkovitý	k2eAgInPc2d1	trojúhelníkovitý
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
obměňují	obměňovat	k5eAaImIp3nP	obměňovat
a	a	k8xC	a
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
zuby	zub	k1gInPc1	zub
vypadlé	vypadlý	k2eAgInPc1d1	vypadlý
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
žralok	žralok	k1gMnSc1	žralok
zakousne	zakousnout	k5eAaPmIp3nS	zakousnout
<g/>
,	,	kIx,	,
třese	třást	k5eAaImIp3nS	třást
hlavou	hlava	k1gFnSc7	hlava
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
používá	používat	k5eAaImIp3nS	používat
čelisti	čelist	k1gFnSc2	čelist
jako	jako	k8xC	jako
ozubenou	ozubený	k2eAgFnSc4d1	ozubená
pilu	pila	k1gFnSc4	pila
oddělující	oddělující	k2eAgNnSc1d1	oddělující
maso	maso	k1gNnSc1	maso
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
bílých	bílý	k2eAgInPc2d1	bílý
žraloků	žralok	k1gMnPc2	žralok
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
zralosti	zralost	k1gFnPc1	zralost
při	při	k7c6	při
velikosti	velikost	k1gFnSc6	velikost
3,5	[number]	k4	3,5
-	-	kIx~	-
4	[number]	k4	4
m	m	kA	m
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samice	samice	k1gFnSc1	samice
při	při	k7c6	při
délce	délka	k1gFnSc6	délka
4,5	[number]	k4	4,5
-	-	kIx~	-
5	[number]	k4	5
m.	m.	k?	m.
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
bývají	bývat	k5eAaImIp3nP	bývat
nejčastěji	často	k6eAd3	často
4	[number]	k4	4
-	-	kIx~	-
5,4	[number]	k4	5,4
m	m	kA	m
dlouzí	dlouhý	k2eAgMnPc1d1	dlouhý
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
váží	vážit	k5eAaImIp3nS	vážit
680	[number]	k4	680
-	-	kIx~	-
1	[number]	k4	1
100	[number]	k4	100
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
větší	veliký	k2eAgMnPc1d2	veliký
než	než	k8xS	než
samci	samec	k1gMnPc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
největší	veliký	k2eAgFnSc4d3	veliký
změřenou	změřený	k2eAgFnSc4d1	změřená
a	a	k8xC	a
ověřenou	ověřený	k2eAgFnSc4d1	ověřená
velikost	velikost	k1gFnSc4	velikost
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
délka	délka	k1gFnSc1	délka
6,4	[number]	k4	6,4
m	m	kA	m
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
3	[number]	k4	3
324	[number]	k4	324
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žraloci	žralok	k1gMnPc1	žralok
dosahující	dosahující	k2eAgFnSc2d1	dosahující
délky	délka	k1gFnSc2	délka
okolo	okolo	k7c2	okolo
7	[number]	k4	7
m	m	kA	m
existují	existovat	k5eAaImIp3nP	existovat
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
stálých	stálý	k2eAgInPc2d1	stálý
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
zprávy	zpráva	k1gFnPc1	zpráva
a	a	k8xC	a
údaje	údaj	k1gInPc1	údaj
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
hrubými	hrubý	k2eAgInPc7d1	hrubý
odhady	odhad	k1gInPc7	odhad
a	a	k8xC	a
spekulacemi	spekulace	k1gFnPc7	spekulace
<g/>
,	,	kIx,	,
vzniklými	vzniklý	k2eAgFnPc7d1	vzniklá
za	za	k7c4	za
pochybných	pochybný	k2eAgNnPc2d1	pochybné
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
parybami	paryba	k1gFnPc7	paryba
existují	existovat	k5eAaImIp3nP	existovat
jen	jen	k6eAd1	jen
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
větších	veliký	k2eAgInPc2d2	veliký
rozměrů	rozměr	k1gInPc2	rozměr
než	než	k8xS	než
bílý	bílý	k1gMnSc1	bílý
žralok	žralok	k1gMnSc1	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
žraloka	žralok	k1gMnSc4	žralok
obrovského	obrovský	k2eAgNnSc2d1	obrovské
(	(	kIx(	(
<g/>
Rhincodon	Rhincodon	k1gNnSc1	Rhincodon
typus	typus	k?	typus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žraloka	žralok	k1gMnSc2	žralok
velikého	veliký	k2eAgMnSc2d1	veliký
(	(	kIx(	(
<g/>
Cetorhinus	Cetorhinus	k1gInSc1	Cetorhinus
maximus	maximus	k1gInSc1	maximus
<g/>
)	)	kIx)	)
a	a	k8xC	a
mantu	manto	k1gNnSc6	manto
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
(	(	kIx(	(
<g/>
Manta	manto	k1gNnPc1	manto
birostris	birostris	k1gFnPc2	birostris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
dva	dva	k4xCgInPc1	dva
jsou	být	k5eAaImIp3nP	být
delší	dlouhý	k2eAgMnSc1d2	delší
i	i	k8xC	i
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
,	,	kIx,	,
manta	manto	k1gNnSc2	manto
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
delší	dlouhý	k2eAgFnSc1d2	delší
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
lépe	dobře	k6eAd2	dobře
řečeno	říct	k5eAaPmNgNnS	říct
širší	široký	k2eAgNnSc4d2	širší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
tito	tento	k3xDgMnPc1	tento
obři	obr	k1gMnPc1	obr
jsou	být	k5eAaImIp3nP	být
mírumilovní	mírumilovný	k2eAgMnPc1d1	mírumilovný
a	a	k8xC	a
klidní	klidný	k2eAgMnPc1d1	klidný
tvorové	tvor	k1gMnPc1	tvor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
malými	malý	k2eAgMnPc7d1	malý
živočichy	živočich	k1gMnPc7	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
množství	množství	k1gNnSc1	množství
záznamů	záznam	k1gInPc2	záznam
o	o	k7c6	o
skutečně	skutečně	k6eAd1	skutečně
velkých	velký	k2eAgInPc6d1	velký
exemplářích	exemplář	k1gInPc6	exemplář
bílých	bílý	k2eAgMnPc2d1	bílý
žraloků	žralok	k1gMnPc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
desetiletí	desetiletí	k1gNnPc2	desetiletí
držely	držet	k5eAaImAgFnP	držet
oficiální	oficiální	k2eAgInSc4d1	oficiální
rekordy	rekord	k1gInPc4	rekord
žraloci	žralok	k1gMnPc1	žralok
velcí	velký	k2eAgMnPc1d1	velký
11,1	[number]	k4	11,1
m	m	kA	m
a	a	k8xC	a
11,3	[number]	k4	11,3
m.	m.	k?	m.
První	první	k4xOgInSc1	první
byl	být	k5eAaImAgInS	být
chycen	chytit	k5eAaPmNgInS	chytit
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
jižní	jižní	k2eAgFnSc2d1	jižní
Austrálie	Austrálie	k1gFnSc2	Austrálie
poblíž	poblíž	k6eAd1	poblíž
Port	port	k1gInSc4	port
Fairy	Faira	k1gFnSc2	Faira
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Nového	Nového	k2eAgInSc2d1	Nového
Brunšviku	Brunšvik	k1gInSc2	Brunšvik
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
postupem	postupem	k7c2	postupem
doby	doba	k1gFnSc2	doba
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
zpochybňovali	zpochybňovat	k5eAaImAgMnP	zpochybňovat
tyto	tento	k3xDgInPc4	tento
údaje	údaj	k1gInPc4	údaj
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
daleko	daleko	k6eAd1	daleko
převyšovaly	převyšovat	k5eAaImAgFnP	převyšovat
veškerá	veškerý	k3xTgNnPc1	veškerý
další	další	k2eAgNnPc1d1	další
pozorování	pozorování	k1gNnPc1	pozorování
a	a	k8xC	a
měření	měření	k1gNnPc1	měření
<g/>
.	.	kIx.	.
11,3	[number]	k4	11,3
<g/>
metrový	metrový	k2eAgInSc1d1	metrový
exemplář	exemplář	k1gInSc1	exemplář
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zaměněn	zaměnit	k5eAaPmNgInS	zaměnit
za	za	k7c2	za
žraloka	žralok	k1gMnSc2	žralok
velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
příbřežních	příbřežní	k2eAgFnPc6d1	příbřežní
kanadských	kanadský	k2eAgFnPc6d1	kanadská
vodách	voda	k1gFnPc6	voda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
a	a	k8xC	a
vzhledově	vzhledově	k6eAd1	vzhledově
se	se	k3xPyFc4	se
velkému	velký	k2eAgNnSc3d1	velké
bílému	bílé	k1gNnSc3	bílé
poněkud	poněkud	k6eAd1	poněkud
podobá	podobat	k5eAaImIp3nS	podobat
<g/>
.	.	kIx.	.
</s>
<s>
Věrohodnost	věrohodnost	k1gFnSc1	věrohodnost
australského	australský	k2eAgInSc2d1	australský
exempláře	exemplář	k1gInSc2	exemplář
byla	být	k5eAaImAgFnS	být
vyřešena	vyřešit	k5eAaPmNgFnS	vyřešit
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
J.	J.	kA	J.
E.	E.	kA	E.
Randall	Randall	k1gMnSc1	Randall
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
jeho	jeho	k3xOp3gFnPc4	jeho
čelisti	čelist	k1gFnPc4	čelist
a	a	k8xC	a
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
délku	délka	k1gFnSc4	délka
žraloka	žralok	k1gMnSc2	žralok
na	na	k7c4	na
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
chybě	chyba	k1gFnSc3	chyba
došlo	dojít	k5eAaPmAgNnS	dojít
zřejmě	zřejmě	k6eAd1	zřejmě
při	při	k7c6	při
zápisu	zápis	k1gInSc6	zápis
délky	délka	k1gFnSc2	délka
do	do	k7c2	do
záznamu	záznam	k1gInSc2	záznam
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Randalla	Randallo	k1gNnSc2	Randallo
měřil	měřit	k5eAaImAgMnS	měřit
největší	veliký	k2eAgMnSc1d3	veliký
ověřený	ověřený	k2eAgMnSc1d1	ověřený
žralok	žralok	k1gMnSc1	žralok
6	[number]	k4	6
m	m	kA	m
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
chycen	chytit	k5eAaPmNgInS	chytit
u	u	k7c2	u
Ledge	Ledg	k1gInSc2	Ledg
Point	pointa	k1gFnPc2	pointa
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Austrálii	Austrálie	k1gFnSc6	Austrálie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
byla	být	k5eAaImAgFnS	být
Davidem	David	k1gMnSc7	David
McKendrickem	McKendrick	k1gInSc7	McKendrick
u	u	k7c2	u
Ostrova	ostrov	k1gInSc2	ostrov
prince	princ	k1gMnSc2	princ
Edwarda	Edward	k1gMnSc2	Edward
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
Sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
chycena	chycen	k2eAgFnSc1d1	chycena
samice	samice	k1gFnSc1	samice
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
6,1	[number]	k4	6,1
m.	m.	k?	m.
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
experti	expert	k1gMnPc1	expert
však	však	k9	však
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
spolehlivý	spolehlivý	k2eAgInSc4d1	spolehlivý
údaj	údaj	k1gInSc4	údaj
o	o	k7c4	o
samici	samice	k1gFnSc4	samice
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měřila	měřit	k5eAaImAgFnS	měřit
6,4	[number]	k4	6,4
m	m	kA	m
a	a	k8xC	a
vážila	vážit	k5eAaImAgFnS	vážit
3324	[number]	k4	3324
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
exemplář	exemplář	k1gInSc1	exemplář
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
největší	veliký	k2eAgInSc4d3	veliký
změřený	změřený	k2eAgInSc4d1	změřený
kus	kus	k1gInSc4	kus
žraloka	žralok	k1gMnSc2	žralok
bílého	bílý	k1gMnSc2	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgNnPc2d1	další
tvrzení	tvrzení	k1gNnPc2	tvrzení
<g/>
,	,	kIx,	,
pozorování	pozorování	k1gNnPc2	pozorování
a	a	k8xC	a
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
žralocích	žralok	k1gMnPc6	žralok
6	[number]	k4	6
-	-	kIx~	-
7	[number]	k4	7
<g/>
+	+	kIx~	+
m	m	kA	m
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
věrohodnějších	věrohodný	k2eAgMnPc2d2	věrohodnější
šlo	jít	k5eAaImAgNnS	jít
především	především	k6eAd1	především
o	o	k7c4	o
žraloky	žralok	k1gMnPc4	žralok
chycené	chycený	k2eAgMnPc4d1	chycený
u	u	k7c2	u
Kangaroo	Kangaroo	k1gMnSc1	Kangaroo
Island	Island	k1gInSc1	Island
u	u	k7c2	u
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Malty	Malta	k1gFnSc2	Malta
<g/>
.	.	kIx.	.
</s>
<s>
Shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
byli	být	k5eAaImAgMnP	být
oba	dva	k4xCgMnPc1	dva
odchyceni	odchycen	k2eAgMnPc1d1	odchycen
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1987	[number]	k4	1987
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
měřili	měřit	k5eAaImAgMnP	měřit
přibližně	přibližně	k6eAd1	přibližně
7	[number]	k4	7
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
první	první	k4xOgMnPc1	první
23	[number]	k4	23
stopy	stopa	k1gFnPc4	stopa
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
7,13	[number]	k4	7,13
metru	metro	k1gNnSc6	metro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
metody	metoda	k1gFnPc1	metoda
měření	měření	k1gNnSc2	měření
a	a	k8xC	a
způsoby	způsoba	k1gFnPc1	způsoba
ověření	ověření	k1gNnSc2	ověření
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
pochybnosti	pochybnost	k1gFnPc1	pochybnost
<g/>
,	,	kIx,	,
zkoumání	zkoumání	k1gNnSc1	zkoumání
dokumentace	dokumentace	k1gFnSc2	dokumentace
a	a	k8xC	a
zbytků	zbytek	k1gInPc2	zbytek
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
považují	považovat	k5eAaImIp3nP	považovat
údaje	údaj	k1gInPc1	údaj
za	za	k7c4	za
možné	možný	k2eAgNnSc4d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Důkladná	důkladný	k2eAgFnSc1d1	důkladná
analýza	analýza	k1gFnSc1	analýza
všech	všecek	k3xTgInPc2	všecek
dostupných	dostupný	k2eAgInPc2d1	dostupný
zdrojů	zdroj	k1gInPc2	zdroj
v	v	k7c6	v
případě	případ	k1gInSc6	případ
maltského	maltský	k2eAgMnSc2d1	maltský
žraloka	žralok	k1gMnSc2	žralok
spíše	spíše	k9	spíše
hovoří	hovořit	k5eAaImIp3nS	hovořit
pro	pro	k7c4	pro
délku	délka	k1gFnSc4	délka
zhruba	zhruba	k6eAd1	zhruba
6	[number]	k4	6
-	-	kIx~	-
6,8	[number]	k4	6,8
m.	m.	k?	m.
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ve	v	k7c6	v
False	Fals	k1gInSc6	Fals
Bay	Bay	k1gFnSc2	Bay
u	u	k7c2	u
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
údajně	údajně	k6eAd1	údajně
několikrát	několikrát	k6eAd1	několikrát
objevil	objevit	k5eAaPmAgMnS	objevit
obrovský	obrovský	k2eAgMnSc1d1	obrovský
žralok	žralok	k1gMnSc1	žralok
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
získal	získat	k5eAaPmAgMnS	získat
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
The	The	k1gFnSc4	The
Submarine	Submarin	k1gInSc5	Submarin
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ponorka	ponorka	k1gFnSc1	ponorka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
měl	mít	k5eAaImAgInS	mít
přes	přes	k7c4	přes
7	[number]	k4	7
m	m	kA	m
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
odchytit	odchytit	k5eAaPmF	odchytit
a	a	k8xC	a
změřit	změřit	k5eAaPmF	změřit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
existence	existence	k1gFnSc1	existence
tohoto	tento	k3xDgMnSc2	tento
žraloka	žralok	k1gMnSc2	žralok
Ponorky	ponorka	k1gFnSc2	ponorka
je	být	k5eAaImIp3nS	být
přetrvávající	přetrvávající	k2eAgInSc1d1	přetrvávající
mýtus	mýtus	k1gInSc1	mýtus
uvedený	uvedený	k2eAgInSc1d1	uvedený
v	v	k7c4	v
život	život	k1gInSc4	život
novináři	novinář	k1gMnPc1	novinář
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
přiživovaný	přiživovaný	k2eAgInSc1d1	přiživovaný
pseudodokumentárními	pseudodokumentární	k2eAgInPc7d1	pseudodokumentární
filmy	film	k1gInPc7	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2013	[number]	k4	2013
-	-	kIx~	-
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
ostrova	ostrov	k1gInSc2	ostrov
Guadalupe	Guadalup	k1gInSc5	Guadalup
(	(	kIx(	(
<g/>
západně	západně	k6eAd1	západně
od	od	k7c2	od
Kalifornského	kalifornský	k2eAgInSc2d1	kalifornský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
)	)	kIx)	)
opakovaně	opakovaně	k6eAd1	opakovaně
nafilmována	nafilmován	k2eAgFnSc1d1	nafilmována
a	a	k8xC	a
vyfotografována	vyfotografován	k2eAgFnSc1d1	vyfotografována
obrovská	obrovský	k2eAgFnSc1d1	obrovská
samice	samice	k1gFnSc1	samice
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
"	"	kIx"	"
<g/>
Deep	Deep	k1gInSc1	Deep
Blue	Blu	k1gInSc2	Blu
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
a	a	k8xC	a
nepřímých	přímý	k2eNgNnPc2d1	nepřímé
měření	měření	k1gNnPc2	měření
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
délky	délka	k1gFnPc4	délka
6,5	[number]	k4	6,5
-	-	kIx~	-
7	[number]	k4	7
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
stáří	stáří	k1gNnSc1	stáří
je	být	k5eAaImIp3nS	být
odhadováno	odhadovat	k5eAaImNgNnS	odhadovat
na	na	k7c4	na
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Bílí	bílý	k2eAgMnPc1d1	bílý
žraloci	žralok	k1gMnPc1	žralok
nepřestávají	přestávat	k5eNaImIp3nP	přestávat
růst	růst	k1gInSc4	růst
ani	ani	k8xC	ani
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
zralosti	zralost	k1gFnSc2	zralost
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jejich	jejich	k3xOp3gInSc1	jejich
růst	růst	k1gInSc1	růst
se	se	k3xPyFc4	se
zpomalí	zpomalet	k5eAaPmIp3nS	zpomalet
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
exempláře	exemplář	k1gInPc1	exemplář
tak	tak	k9	tak
bývají	bývat	k5eAaImIp3nP	bývat
zároveň	zároveň	k6eAd1	zároveň
největšími	veliký	k2eAgMnPc7d3	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dravými	dravý	k2eAgMnPc7d1	dravý
žraloky	žralok	k1gMnPc7	žralok
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
bílý	bílý	k2eAgInSc1d1	bílý
největší	veliký	k2eAgInSc1d3	veliký
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
žralok	žralok	k1gMnSc1	žralok
tygří	tygří	k2eAgMnSc1d1	tygří
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k4c4	málo
menších	malý	k2eAgInPc2d2	menší
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
měří	měřit	k5eAaImIp3nP	měřit
dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
3,25	[number]	k4	3,25
-	-	kIx~	-
4,25	[number]	k4	4,25
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
přes	přes	k7c4	přes
400	[number]	k4	400
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
potvrzený	potvrzený	k2eAgMnSc1d1	potvrzený
žralok	žralok	k1gMnSc1	žralok
tygří	tygří	k2eAgMnSc1d1	tygří
měřil	měřit	k5eAaImAgMnS	měřit
5,5	[number]	k4	5,5
metru	metr	k1gInSc2	metr
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
neověřený	ověřený	k2eNgMnSc1d1	neověřený
jedinec	jedinec	k1gMnSc1	jedinec
dokonce	dokonce	k9	dokonce
7,4	[number]	k4	7,4
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
při	při	k7c6	při
hmotnosti	hmotnost	k1gFnSc6	hmotnost
3110	[number]	k4	3110
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgMnSc1d1	velký
bílý	bílý	k1gMnSc1	bílý
žralok	žralok	k1gMnSc1	žralok
má	mít	k5eAaImIp3nS	mít
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
žraloci	žralok	k1gMnPc1	žralok
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
smysl	smysl	k1gInSc4	smysl
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Lorenziniho	Lorenzini	k1gMnSc4	Lorenzini
ampule	ampule	k1gFnSc2	ampule
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mu	on	k3xPp3gMnSc3	on
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
detekovat	detekovat	k5eAaImF	detekovat
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vzniká	vznikat	k5eAaImIp3nS	vznikat
pohybem	pohyb	k1gInSc7	pohyb
živých	živý	k2eAgNnPc2d1	živé
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgMnSc1d1	bílý
žralok	žralok	k1gMnSc1	žralok
dokáže	dokázat	k5eAaPmIp3nS	dokázat
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
napětí	napětí	k1gNnSc4	napětí
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
půl	půl	k6eAd1	půl
miliardtiny	miliardtina	k1gFnSc2	miliardtina
voltu	volt	k1gInSc2	volt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
tak	tak	k9	tak
dokáže	dokázat	k5eAaPmIp3nS	dokázat
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
i	i	k9	i
nepohybujícího	pohybující	k2eNgMnSc4d1	nepohybující
se	se	k3xPyFc4	se
živočicha	živočich	k1gMnSc4	živočich
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tlukotu	tlukot	k1gInSc2	tlukot
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
ryb	ryba	k1gFnPc2	ryba
má	mít	k5eAaImIp3nS	mít
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
méně	málo	k6eAd2	málo
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
mysl	mysl	k1gFnSc4	mysl
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
jejich	jejich	k3xOp3gFnSc2	jejich
postranní	postranní	k2eAgFnSc2d1	postranní
čáry	čára	k1gFnSc2	čára
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
lépe	dobře	k6eAd2	dobře
lovit	lovit	k5eAaImF	lovit
rychlou	rychlý	k2eAgFnSc4d1	rychlá
a	a	k8xC	a
hbitou	hbitý	k2eAgFnSc4d1	hbitá
kořist	kořist	k1gFnSc4	kořist
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
lachtani	lachtan	k1gMnPc1	lachtan
<g/>
,	,	kIx,	,
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
se	se	k3xPyFc4	se
u	u	k7c2	u
bílého	bílý	k1gMnSc2	bílý
žraloka	žralok	k1gMnSc2	žralok
schopnost	schopnost	k1gFnSc4	schopnost
udržovat	udržovat	k5eAaImF	udržovat
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
teplotu	teplota	k1gFnSc4	teplota
vyšší	vysoký	k2eAgFnSc4d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
okolní	okolní	k2eAgFnSc2d1	okolní
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
adaptaci	adaptace	k1gFnSc4	adaptace
zvanou	zvaný	k2eAgFnSc4d1	zvaná
rete	ret	k1gInSc5	ret
mirabile	mirabile	k6eAd1	mirabile
(	(	kIx(	(
<g/>
úžasná	úžasný	k2eAgFnSc1d1	úžasná
síť	síť	k1gFnSc1	síť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
studenější	studený	k2eAgFnSc1d2	studenější
tepenná	tepenný	k2eAgFnSc1d1	tepenná
krev	krev	k1gFnSc1	krev
v	v	k7c6	v
pletenci	pletenec	k1gInSc6	pletenec
cév	céva	k1gFnPc2	céva
ohřívána	ohřívat	k5eAaImNgFnS	ohřívat
teplejší	teplý	k2eAgFnSc7d2	teplejší
žilní	žilní	k2eAgFnSc7d1	žilní
krví	krev	k1gFnSc7	krev
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zahřátá	zahřátý	k2eAgFnSc1d1	zahřátá
díky	díky	k7c3	díky
svalové	svalový	k2eAgFnSc3d1	svalová
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mechanismus	mechanismus	k1gInSc1	mechanismus
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
udržet	udržet	k5eAaPmF	udržet
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
těla	tělo	k1gNnSc2	tělo
až	až	k9	až
o	o	k7c4	o
14	[number]	k4	14
°	°	k?	°
<g/>
C	C	kA	C
teplejší	teplý	k2eAgNnSc1d2	teplejší
než	než	k8xS	než
okolní	okolní	k2eAgNnSc1d1	okolní
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
žábry	žábry	k1gFnPc1	žábry
a	a	k8xC	a
srdce	srdce	k1gNnSc1	srdce
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
teploty	teplota	k1gFnSc2	teplota
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Bílí	bílý	k2eAgMnPc1d1	bílý
žraloci	žralok	k1gMnPc1	žralok
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
spoléhají	spoléhat	k5eAaImIp3nP	spoléhat
na	na	k7c4	na
zásoby	zásoba	k1gFnPc4	zásoba
tuku	tuk	k1gInSc2	tuk
a	a	k8xC	a
oleje	olej	k1gInSc2	olej
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
játrech	játra	k1gNnPc6	játra
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mu	on	k3xPp3gMnSc3	on
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
překonávat	překonávat	k5eAaImF	překonávat
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
přes	přes	k7c4	přes
potravně	potravně	k6eAd1	potravně
chudé	chudý	k2eAgFnPc4d1	chudá
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Játra	játra	k1gNnPc1	játra
jim	on	k3xPp3gMnPc3	on
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
při	při	k7c6	při
potápění	potápění	k1gNnSc6	potápění
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
stisku	stisk	k1gInSc2	stisk
čelistí	čelist	k1gFnPc2	čelist
bílého	bílý	k1gMnSc2	bílý
žraloka	žralok	k1gMnSc2	žralok
byla	být	k5eAaImAgFnS	být
dlouho	dlouho	k6eAd1	dlouho
zahalena	zahalen	k2eAgFnSc1d1	zahalena
mýty	mýtus	k1gInPc7	mýtus
a	a	k8xC	a
dohady	dohad	k1gInPc7	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgInPc1d3	nejnovější
vědecké	vědecký	k2eAgInPc1d1	vědecký
výzkumy	výzkum	k1gInPc1	výzkum
se	se	k3xPyFc4	se
přiklánějí	přiklánět	k5eAaImIp3nP	přiklánět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
myslelo	myslet	k5eAaImAgNnS	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
predátor	predátor	k1gMnSc1	predátor
se	se	k3xPyFc4	se
nespoléhá	spoléhat	k5eNaImIp3nS	spoléhat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
na	na	k7c4	na
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
na	na	k7c4	na
pilovité	pilovitý	k2eAgInPc4d1	pilovitý
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
pohyb	pohyb	k1gInSc1	pohyb
hlavou	hlava	k1gFnSc7	hlava
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
efektivně	efektivně	k6eAd1	efektivně
oddělovat	oddělovat	k5eAaImF	oddělovat
maso	maso	k1gNnSc4	maso
od	od	k7c2	od
těla	tělo	k1gNnSc2	tělo
oběti	oběť	k1gFnSc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Vědecký	vědecký	k2eAgInSc1d1	vědecký
experiment	experiment	k1gInSc1	experiment
sice	sice	k8xC	sice
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
s	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedinec	jedinec	k1gMnSc1	jedinec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vážil	vážit	k5eAaImAgMnS	vážit
3324	[number]	k4	3324
kg	kg	kA	kg
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
sílu	síla	k1gFnSc4	síla
asi	asi	k9	asi
18	[number]	k4	18
000	[number]	k4	000
N	N	kA	N
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1800	[number]	k4	1800
kg	kg	kA	kg
na	na	k7c4	na
cm2	cm2	k4	cm2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
značná	značný	k2eAgFnSc1d1	značná
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skutečné	skutečný	k2eAgInPc4d1	skutečný
výsledky	výsledek	k1gInPc4	výsledek
in	in	k?	in
natura	natura	k1gFnSc1	natura
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
nemáme	mít	k5eNaImIp1nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Chování	chování	k1gNnSc1	chování
a	a	k8xC	a
sociální	sociální	k2eAgFnSc1d1	sociální
struktura	struktura	k1gFnSc1	struktura
tohoto	tento	k3xDgMnSc2	tento
žraloka	žralok	k1gMnSc2	žralok
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
dobře	dobře	k6eAd1	dobře
prozkoumána	prozkoumat	k5eAaPmNgNnP	prozkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
je	být	k5eAaImIp3nS	být
hierarchie	hierarchie	k1gFnSc1	hierarchie
dána	dán	k2eAgFnSc1d1	dána
velikostí	velikost	k1gFnSc7	velikost
<g/>
,	,	kIx,	,
pohlavím	pohlaví	k1gNnSc7	pohlaví
a	a	k8xC	a
teritoriálností	teritoriálnost	k1gFnSc7	teritoriálnost
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
větší	veliký	k2eAgMnPc1d2	veliký
jedinci	jedinec	k1gMnPc1	jedinec
mají	mít	k5eAaImIp3nP	mít
převahu	převaha	k1gFnSc4	převaha
nad	nad	k7c7	nad
menšími	malý	k2eAgInPc7d2	menší
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
nad	nad	k7c7	nad
samci	samec	k1gMnPc7	samec
a	a	k8xC	a
rezidenti	rezident	k1gMnPc1	rezident
nad	nad	k7c7	nad
nově	nově	k6eAd1	nově
připluvšími	připluvší	k2eAgFnPc7d1	připluvší
<g/>
.	.	kIx.	.
</s>
<s>
Čili	čili	k8xC	čili
velká	velký	k2eAgFnSc1d1	velká
místní	místní	k2eAgFnSc1d1	místní
samice	samice	k1gFnSc1	samice
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
jednoznačně	jednoznačně	k6eAd1	jednoznačně
navrch	navrch	k6eAd1	navrch
nad	nad	k7c7	nad
malým	malý	k2eAgInSc7d1	malý
"	"	kIx"	"
<g/>
přivandrovalým	přivandrovalý	k2eAgInSc7d1	přivandrovalý
<g/>
"	"	kIx"	"
samcem	samec	k1gInSc7	samec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
lovu	lov	k1gInSc6	lov
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
většinou	většinou	k6eAd1	většinou
solitérně	solitérně	k6eAd1	solitérně
a	a	k8xC	a
konflikty	konflikt	k1gInPc1	konflikt
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
urovnat	urovnat	k5eAaPmF	urovnat
rituálními	rituální	k2eAgInPc7d1	rituální
pohyby	pohyb	k1gInPc7	pohyb
a	a	k8xC	a
ukazováním	ukazování	k1gNnSc7	ukazování
své	svůj	k3xOyFgFnSc2	svůj
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
pokoušou	pokousat	k5eAaPmIp3nP	pokousat
<g/>
.	.	kIx.	.
</s>
<s>
Pokousaní	pokousaný	k2eAgMnPc1d1	pokousaný
jedinci	jedinec	k1gMnPc1	jedinec
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
často	často	k6eAd1	často
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
komunikace	komunikace	k1gFnSc2	komunikace
zřejmě	zřejmě	k6eAd1	zřejmě
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
běžné	běžný	k2eAgFnSc3d1	běžná
vnitrodruhové	vnitrodruhový	k2eAgFnSc3d1	vnitrodruhová
sociální	sociální	k2eAgFnSc3d1	sociální
interakci	interakce	k1gFnSc3	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgMnSc1d1	bílý
žralok	žralok	k1gMnSc1	žralok
je	být	k5eAaImIp3nS	být
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
žraloků	žralok	k1gMnPc2	žralok
znám	znát	k5eAaImIp1nS	znát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vystrkuje	vystrkovat	k5eAaImIp3nS	vystrkovat
hlavu	hlava	k1gFnSc4	hlava
nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
a	a	k8xC	a
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
různé	různý	k2eAgInPc4d1	různý
objekty	objekt	k1gInPc4	objekt
či	či	k8xC	či
svou	svůj	k3xOyFgFnSc4	svůj
potenciální	potenciální	k2eAgFnSc4d1	potenciální
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
bílí	bílý	k1gMnPc1	bílý
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
velmi	velmi	k6eAd1	velmi
zvědavá	zvědavý	k2eAgNnPc4d1	zvědavé
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
jistou	jistý	k2eAgFnSc4d1	jistá
"	"	kIx"	"
<g/>
nadstandardní	nadstandardní	k2eAgFnSc4d1	nadstandardní
<g/>
"	"	kIx"	"
inteligenci	inteligence	k1gFnSc4	inteligence
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
socializace	socializace	k1gFnSc2	socializace
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Seal	Seala	k1gFnPc2	Seala
Islandu	Island	k1gInSc2	Island
byli	být	k5eAaImAgMnP	být
pozorováni	pozorovat	k5eAaImNgMnP	pozorovat
jak	jak	k8xC	jak
připlouvají	připlouvat	k5eAaImIp3nP	připlouvat
a	a	k8xC	a
odplouvají	odplouvat	k5eAaImIp3nP	odplouvat
v	v	k7c6	v
klanech	klan	k1gInPc6	klan
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
2	[number]	k4	2
-	-	kIx~	-
6	[number]	k4	6
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
příbuzné	příbuzný	k1gMnPc4	příbuzný
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
podobná	podobný	k2eAgFnSc1d1	podobná
vlčím	vlčet	k5eAaImIp1nS	vlčet
smečkám	smečka	k1gFnPc3	smečka
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
má	mít	k5eAaImIp3nS	mít
určité	určitý	k2eAgNnSc4d1	určité
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
klan	klan	k1gInSc1	klan
je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgInS	vést
alfa	alfa	k1gNnSc7	alfa
jedincem	jedinec	k1gMnSc7	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
nebývají	bývat	k5eNaImIp3nP	bývat
agresivní	agresivní	k2eAgNnSc4d1	agresivní
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
klanům	klan	k1gInPc3	klan
<g/>
.	.	kIx.	.
</s>
<s>
Bílí	bílý	k2eAgMnPc1d1	bílý
žraloci	žralok	k1gMnPc1	žralok
jsou	být	k5eAaImIp3nP	být
predátoři	predátor	k1gMnPc1	predátor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
rybami	ryba	k1gFnPc7	ryba
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
tuňáky	tuňák	k1gMnPc7	tuňák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinými	jiný	k2eAgMnPc7d1	jiný
žraloky	žralok	k1gMnPc7	žralok
<g/>
,	,	kIx,	,
rejnoky	rejnok	k1gMnPc7	rejnok
<g/>
,	,	kIx,	,
mořskými	mořský	k2eAgMnPc7d1	mořský
savci	savec	k1gMnPc7	savec
(	(	kIx(	(
<g/>
kytovci	kytovec	k1gMnPc7	kytovec
<g/>
,	,	kIx,	,
ploutvonožci	ploutvonožec	k1gMnPc7	ploutvonožec
<g/>
,	,	kIx,	,
vydrami	vydra	k1gFnPc7	vydra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mořskými	mořský	k2eAgFnPc7d1	mořská
želvami	želva	k1gFnPc7	želva
a	a	k8xC	a
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
napadají	napadat	k5eAaPmIp3nP	napadat
i	i	k9	i
jinou	jiný	k2eAgFnSc4d1	jiná
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
krokodýli	krokodýl	k1gMnPc1	krokodýl
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
jejich	jejich	k3xOp3gInPc2	jejich
žaludků	žaludek	k1gInPc2	žaludek
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
jídelníčku	jídelníček	k1gInSc6	jídelníček
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
ocitnout	ocitnout	k5eAaPmF	ocitnout
i	i	k9	i
žraloci	žralok	k1gMnPc1	žralok
velrybí	velrybí	k2eAgMnPc1d1	velrybí
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
aktivní	aktivní	k2eAgInSc4d1	aktivní
lov	lov	k1gInSc4	lov
či	či	k8xC	či
pouhé	pouhý	k2eAgNnSc4d1	pouhé
mrchožroutství	mrchožroutství	k1gNnSc4	mrchožroutství
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
objekty	objekt	k1gInPc1	objekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nemohou	moct	k5eNaImIp3nP	moct
strávit	strávit	k5eAaPmF	strávit
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
v	v	k7c6	v
takovém	takový	k3xDgNnSc6	takový
množství	množství	k1gNnSc6	množství
jako	jako	k9	jako
žraloci	žralok	k1gMnPc1	žralok
tygří	tygří	k2eAgMnPc1d1	tygří
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
jedinci	jedinec	k1gMnPc1	jedinec
bílých	bílý	k2eAgMnPc2d1	bílý
žraloků	žralok	k1gMnPc2	žralok
útočí	útočit	k5eAaImIp3nS	útočit
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gFnPc1	jejich
čelisti	čelist	k1gFnPc1	čelist
a	a	k8xC	a
zuby	zub	k1gInPc1	zub
nejsou	být	k5eNaImIp3nP	být
ještě	ještě	k9	ještě
tak	tak	k6eAd1	tak
mineralizované	mineralizovaný	k2eAgNnSc1d1	mineralizované
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
efektivně	efektivně	k6eAd1	efektivně
zdolávat	zdolávat	k5eAaImF	zdolávat
větší	veliký	k2eAgMnPc4d2	veliký
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Hranicí	hranice	k1gFnSc7	hranice
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
délka	délka	k1gFnSc1	délka
3	[number]	k4	3
metry	metr	k1gInPc4	metr
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
když	když	k8xS	když
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
<g/>
,	,	kIx,	,
začínají	začínat	k5eAaImIp3nP	začínat
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
napadat	napadat	k5eAaPmF	napadat
mořské	mořský	k2eAgMnPc4d1	mořský
savce	savec	k1gMnPc4	savec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
délky	délka	k1gFnSc2	délka
4	[number]	k4	4
metrů	metr	k1gInPc2	metr
se	se	k3xPyFc4	se
savci	savec	k1gMnPc1	savec
stávají	stávat	k5eAaImIp3nP	stávat
jejich	jejich	k3xOp3gFnSc7	jejich
hlavní	hlavní	k2eAgFnSc7d1	hlavní
kořistí	kořist	k1gFnSc7	kořist
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
žraloci	žralok	k1gMnPc1	žralok
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
specializovat	specializovat	k5eAaBmF	specializovat
na	na	k7c4	na
různé	různý	k2eAgMnPc4d1	různý
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
jsou	být	k5eAaImIp3nP	být
potravními	potravní	k2eAgMnPc7d1	potravní
oportunisty	oportunista	k1gMnPc7	oportunista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
využívají	využívat	k5eAaImIp3nP	využívat
jakékoliv	jakýkoliv	k3yIgNnSc1	jakýkoliv
potenciální	potenciální	k2eAgFnPc1d1	potenciální
nabídky	nabídka	k1gFnPc1	nabídka
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
<g/>
,	,	kIx,	,
přednost	přednost	k1gFnSc4	přednost
dostává	dostávat	k5eAaImIp3nS	dostávat
potrava	potrava	k1gFnSc1	potrava
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
pokusu	pokus	k1gInSc6	pokus
byly	být	k5eAaImAgFnP	být
žralokům	žralok	k1gMnPc3	žralok
nabídnuty	nabídnut	k2eAgFnPc4d1	nabídnuta
mrtvoly	mrtvola	k1gFnPc4	mrtvola
tuleně	tuleň	k1gMnSc2	tuleň
<g/>
,	,	kIx,	,
prasete	prase	k1gNnSc2	prase
a	a	k8xC	a
ovce	ovce	k1gFnSc2	ovce
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
ochutnal	ochutnat	k5eAaPmAgMnS	ochutnat
veškerou	veškerý	k3xTgFnSc4	veškerý
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
,	,	kIx,	,
ovci	ovce	k1gFnSc4	ovce
následně	následně	k6eAd1	následně
nekonzumoval	konzumovat	k5eNaBmAgMnS	konzumovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
žraloci	žralok	k1gMnPc1	žralok
imobilizují	imobilizovat	k5eAaBmIp3nP	imobilizovat
rypouše	rypouš	k1gMnPc4	rypouš
severní	severní	k2eAgNnSc1d1	severní
(	(	kIx(	(
<g/>
Mirounga	Mirounga	k1gFnSc1	Mirounga
angustirostris	angustirostris	k1gFnSc2	angustirostris
<g/>
)	)	kIx)	)
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
zasadí	zasadit	k5eAaPmIp3nS	zasadit
hluboký	hluboký	k2eAgInSc1d1	hluboký
kousanec	kousanec	k1gInSc1	kousanec
do	do	k7c2	do
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
nechají	nechat	k5eAaPmIp3nP	nechat
oběť	oběť	k1gFnSc4	oběť
vykrvácet	vykrvácet	k5eAaPmF	vykrvácet
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
používaná	používaný	k2eAgFnSc1d1	používaná
hlavně	hlavně	k9	hlavně
vůči	vůči	k7c3	vůči
dospělým	dospělý	k2eAgInPc3d1	dospělý
samcům	samec	k1gInPc3	samec
rypoušů	rypouš	k1gMnPc2	rypouš
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
mohutnější	mohutný	k2eAgMnPc1d2	mohutnější
než	než	k8xS	než
žraloci	žralok	k1gMnPc1	žralok
(	(	kIx(	(
<g/>
váží	vážit	k5eAaImIp3nP	vážit
často	často	k6eAd1	často
mezi	mezi	k7c7	mezi
1500	[number]	k4	1500
-	-	kIx~	-
2000	[number]	k4	2000
kg	kg	kA	kg
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
potenciálně	potenciálně	k6eAd1	potenciálně
nebezpečnými	bezpečný	k2eNgMnPc7d1	nebezpečný
protivníky	protivník	k1gMnPc7	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
nicméně	nicméně	k8xC	nicméně
napadají	napadat	k5eAaPmIp3nP	napadat
mladé	mladý	k2eAgMnPc4d1	mladý
jedince	jedinec	k1gMnPc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlédnutá	vyhlédnutý	k2eAgFnSc1d1	vyhlédnutá
kořist	kořist	k1gFnSc1	kořist
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
napadána	napadat	k5eAaPmNgFnS	napadat
rychlým	rychlý	k2eAgInSc7d1	rychlý
výpadem	výpad	k1gInSc7	výpad
z	z	k7c2	z
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Tuleni	tuleň	k1gMnPc1	tuleň
obecní	obecnět	k5eAaImIp3nP	obecnět
(	(	kIx(	(
<g/>
Phoca	Phoca	k1gFnSc1	Phoca
vitulina	vitulin	k2eAgFnSc1d1	vitulina
<g/>
)	)	kIx)	)
bývají	bývat	k5eAaImIp3nP	bývat
po	po	k7c6	po
útoku	útok	k1gInSc6	útok
staženi	stažen	k2eAgMnPc1d1	stažen
pod	pod	k7c4	pod
hladinu	hladina	k1gFnSc4	hladina
až	až	k9	až
do	do	k7c2	do
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
přestanou	přestat	k5eAaPmIp3nP	přestat
bránit	bránit	k5eAaImF	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
jsou	být	k5eAaImIp3nP	být
zkonzumováni	zkonzumován	k2eAgMnPc1d1	zkonzumován
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Lachtani	lachtan	k1gMnPc1	lachtan
kalifornští	kalifornský	k2eAgMnPc1d1	kalifornský
(	(	kIx(	(
<g/>
Zalophus	Zalophus	k1gMnSc1	Zalophus
californianus	californianus	k1gMnSc1	californianus
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
napadáni	napadán	k2eAgMnPc1d1	napadán
prudkým	prudký	k2eAgInSc7d1	prudký
výpadem	výpad	k1gInSc7	výpad
zespodu	zespodu	k6eAd1	zespodu
a	a	k8xC	a
kousnuti	kousnout	k5eAaPmNgMnP	kousnout
doprostřed	doprostřed	k7c2	doprostřed
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Žraloci	žralok	k1gMnPc1	žralok
napadají	napadat	k5eAaBmIp3nP	napadat
delfíny	delfín	k1gMnPc4	delfín
<g/>
,	,	kIx,	,
plískavice	plískavice	k1gFnPc4	plískavice
a	a	k8xC	a
sviňuchy	sviňucha	k1gFnPc4	sviňucha
ze	z	k7c2	z
shora	shora	k6eAd1	shora
<g/>
,	,	kIx,	,
zezadu	zezadu	k6eAd1	zezadu
či	či	k8xC	či
zespodu	zespodu	k6eAd1	zespodu
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vyhnuli	vyhnout	k5eAaPmAgMnP	vyhnout
detekci	detekce	k1gFnSc3	detekce
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
jejich	jejich	k3xOp3gNnSc2	jejich
echolokačního	echolokační	k2eAgNnSc2d1	echolokační
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
zaznamenané	zaznamenaný	k2eAgInPc1d1	zaznamenaný
útoky	útok	k1gInPc1	útok
na	na	k7c4	na
plískavice	plískavice	k1gFnPc4	plískavice
tmavé	tmavý	k2eAgFnPc4d1	tmavá
(	(	kIx(	(
<g/>
Lagenorhynchus	Lagenorhynchus	k1gMnSc1	Lagenorhynchus
obscurus	obscurus	k1gMnSc1	obscurus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plískavice	plískavice	k1gFnSc1	plískavice
šedé	šedá	k1gFnSc2	šedá
(	(	kIx(	(
<g/>
Grampus	Grampus	k1gMnSc1	Grampus
griseus	griseus	k1gMnSc1	griseus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
delfíny	delfín	k1gMnPc4	delfín
(	(	kIx(	(
<g/>
rody	rod	k1gInPc1	rod
Tursiops	Tursiopsa	k1gFnPc2	Tursiopsa
a	a	k8xC	a
Sousa	Sousa	k1gFnSc1	Sousa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sviňuchy	sviňucha	k1gFnPc1	sviňucha
obecné	obecná	k1gFnPc1	obecná
(	(	kIx(	(
<g/>
Phocoena	Phocoen	k2eAgFnSc1d1	Phocoena
phocoena	phocoena	k1gFnSc1	phocoena
<g/>
)	)	kIx)	)
a	a	k8xC	a
sviňuchy	sviňucha	k1gFnPc1	sviňucha
běloploutvé	běloploutvý	k2eAgFnPc1d1	běloploutvý
(	(	kIx(	(
<g/>
Phocoenoides	Phocoenoides	k1gInSc1	Phocoenoides
dalli	dalle	k1gFnSc4	dalle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skupiny	skupina	k1gFnPc1	skupina
delfínů	delfín	k1gMnPc2	delfín
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
brání	bránit	k5eAaImIp3nP	bránit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žraloky	žralok	k1gMnPc4	žralok
hromadně	hromadně	k6eAd1	hromadně
obtěžují	obtěžovat	k5eAaImIp3nP	obtěžovat
<g/>
.	.	kIx.	.
</s>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
bílí	bílý	k1gMnPc1	bílý
napadají	napadat	k5eAaImIp3nP	napadat
i	i	k9	i
další	další	k2eAgMnPc4d1	další
kytovce	kytovec	k1gMnPc4	kytovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
nalezen	nalezen	k2eAgMnSc1d1	nalezen
mladý	mladý	k2eAgMnSc1d1	mladý
samec	samec	k1gMnSc1	samec
kogie	kogie	k1gFnSc2	kogie
tuponosé	tuponosý	k2eAgFnSc2d1	tuponosá
(	(	kIx(	(
<g/>
Kogia	Kogium	k1gNnPc1	Kogium
breviceps	brevicepsa	k1gFnPc2	brevicepsa
<g/>
)	)	kIx)	)
s	s	k7c7	s
vykousnutou	vykousnutý	k2eAgFnSc7d1	vykousnutá
ocasní	ocasní	k2eAgFnSc7d1	ocasní
ploutví	ploutev	k1gFnSc7	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
také	také	k9	také
vorvaňovcovité	vorvaňovcovitý	k2eAgNnSc1d1	vorvaňovcovitý
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vorvaňovce	vorvaňovec	k1gInPc1	vorvaňovec
Stejnegerovy	Stejnegerův	k2eAgInPc1d1	Stejnegerův
(	(	kIx(	(
<g/>
Mesoplodon	Mesoplodon	k1gInSc1	Mesoplodon
stejnegeri	stejneger	k1gFnSc2	stejneger
<g/>
)	)	kIx)	)
a	a	k8xC	a
vorvaňovce	vorvaňovec	k1gInPc1	vorvaňovec
zobaté	zobatý	k2eAgInPc1d1	zobatý
(	(	kIx(	(
<g/>
Ziphius	Ziphius	k1gInSc1	Ziphius
cavirostris	cavirostris	k1gFnSc2	cavirostris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mořským	mořský	k2eAgFnPc3d1	mořská
želvám	želva	k1gFnPc3	želva
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
prokousnout	prokousnout	k5eAaPmF	prokousnout
krunýře	krunýř	k1gInPc4	krunýř
okolo	okolo	k7c2	okolo
ploutví	ploutev	k1gFnPc2	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgFnPc7d3	veliký
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
jim	on	k3xPp3gMnPc3	on
padají	padat	k5eAaImIp3nP	padat
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
měsíčníci	měsíčník	k1gMnPc1	měsíčník
svítiví	svítivý	k2eAgMnPc1d1	svítivý
(	(	kIx(	(
<g/>
Mola	mol	k1gMnSc4	mol
mola	mol	k1gMnSc4	mol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Seal	Seal	k1gInSc4	Seal
Islandu	Island	k1gInSc2	Island
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
False	False	k1gFnSc2	False
Bay	Bay	k1gFnSc2	Bay
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
napadají	napadat	k5eAaImIp3nP	napadat
místní	místní	k2eAgMnPc1d1	místní
žraloci	žralok	k1gMnPc1	žralok
především	především	k6eAd1	především
lachtany	lachtan	k1gMnPc4	lachtan
jihoafrické	jihoafrický	k2eAgNnSc1d1	jihoafrické
(	(	kIx(	(
<g/>
Arctocephalus	Arctocephalus	k1gInSc1	Arctocephalus
pusillus	pusillus	k1gInSc1	pusillus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
je	on	k3xPp3gFnPc4	on
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
zespodu	zespodu	k6eAd1	zespodu
vysokou	vysoký	k2eAgFnSc7d1	vysoká
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
přesahující	přesahující	k2eAgFnSc4d1	přesahující
40	[number]	k4	40
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
většinou	většinou	k6eAd1	většinou
vyletí	vyletět	k5eAaPmIp3nS	vyletět
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
ať	ať	k8xS	ať
už	už	k6eAd1	už
s	s	k7c7	s
lachtanem	lachtan	k1gMnSc7	lachtan
v	v	k7c6	v
tlamě	tlama	k1gFnSc6	tlama
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
neuspějí	uspět	k5eNaPmIp3nP	uspět
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
přepadu	přepad	k1gInSc6	přepad
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
pronásledují	pronásledovat	k5eAaImIp3nP	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
především	především	k9	především
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
kvůli	kvůli	k7c3	kvůli
snížené	snížený	k2eAgFnSc3d1	snížená
viditelnosti	viditelnost	k1gFnSc3	viditelnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
rozednění	rozednění	k1gNnSc6	rozednění
(	(	kIx(	(
<g/>
55	[number]	k4	55
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
klesne	klesnout	k5eAaPmIp3nS	klesnout
na	na	k7c4	na
40	[number]	k4	40
%	%	kIx~	%
a	a	k8xC	a
žraloci	žralok	k1gMnPc1	žralok
s	s	k7c7	s
lovem	lov	k1gInSc7	lov
nejpozději	pozdě	k6eAd3	pozdě
dopoledne	dopoledne	k6eAd1	dopoledne
přestanou	přestat	k5eAaPmIp3nP	přestat
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvoly	mrtvola	k1gFnPc1	mrtvola
velryb	velryba	k1gFnPc2	velryba
představují	představovat	k5eAaImIp3nP	představovat
důležitou	důležitý	k2eAgFnSc4d1	důležitá
součást	součást	k1gFnSc4	součást
jídelníčku	jídelníček	k1gInSc2	jídelníček
bílých	bílý	k2eAgMnPc2d1	bílý
žraloků	žralok	k1gMnPc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
30	[number]	k4	30
kg	kg	kA	kg
velrybího	velrybí	k2eAgInSc2d1	velrybí
tuku	tuk	k1gInSc2	tuk
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zasytit	zasytit	k5eAaPmF	zasytit
4,5	[number]	k4	4,5
metru	metr	k1gInSc2	metr
dlouhého	dlouhý	k2eAgMnSc2d1	dlouhý
žraloka	žralok	k1gMnSc2	žralok
na	na	k7c4	na
1,5	[number]	k4	1,5
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
několikrát	několikrát	k6eAd1	několikrát
detailně	detailně	k6eAd1	detailně
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
chování	chování	k1gNnSc1	chování
žraloků	žralok	k1gMnPc2	žralok
u	u	k7c2	u
velrybích	velrybí	k2eAgFnPc2d1	velrybí
zdechlin	zdechlina	k1gFnPc2	zdechlina
poblíž	poblíž	k7c2	poblíž
False	False	k1gFnSc2	False
Bay	Bay	k1gFnSc2	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Žraloci	žralok	k1gMnPc1	žralok
nalákaní	nalákaný	k2eAgMnPc1d1	nalákaný
zápachem	zápach	k1gInSc7	zápach
rozkládající	rozkládající	k2eAgFnSc2d1	rozkládající
se	se	k3xPyFc4	se
mršiny	mršina	k1gFnSc2	mršina
nejprve	nejprve	k6eAd1	nejprve
ohryzávali	ohryzávat	k5eAaImAgMnP	ohryzávat
ploutve	ploutev	k1gFnPc4	ploutev
a	a	k8xC	a
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
mrtvolu	mrtvola	k1gFnSc4	mrtvola
pomalým	pomalý	k2eAgNnSc7d1	pomalé
obeplouváním	obeplouvání	k1gNnSc7	obeplouvání
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pokusně	pokusně	k6eAd1	pokusně
kousali	kousat	k5eAaImAgMnP	kousat
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
až	až	k9	až
do	do	k7c2	do
objevení	objevení	k1gNnSc2	objevení
na	na	k7c4	na
tuk	tuk	k1gInSc4	tuk
bohatých	bohatý	k2eAgFnPc2d1	bohatá
zón	zóna	k1gFnPc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
pak	pak	k6eAd1	pak
oddělovali	oddělovat	k5eAaImAgMnP	oddělovat
pohybem	pohyb	k1gInSc7	pohyb
hlavy	hlava	k1gFnSc2	hlava
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
zavírali	zavírat	k5eAaImAgMnP	zavírat
mžurky	mžurka	k1gFnSc2	mžurka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dělají	dělat	k5eAaImIp3nP	dělat
jen	jen	k9	jen
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
živou	živý	k2eAgFnSc4d1	živá
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
potom	potom	k6eAd1	potom
vyvrhovali	vyvrhovat	k5eAaImAgMnP	vyvrhovat
již	již	k9	již
spolknuté	spolknutý	k2eAgMnPc4d1	spolknutý
a	a	k8xC	a
zjevně	zjevně	k6eAd1	zjevně
málo	málo	k6eAd1	málo
výživné	výživný	k2eAgInPc4d1	výživný
kousky	kousek	k1gInPc4	kousek
masa	maso	k1gNnSc2	maso
z	z	k7c2	z
tlamy	tlama	k1gFnSc2	tlama
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
vraceli	vracet	k5eAaImAgMnP	vracet
pozřít	pozřít	k5eAaPmF	pozřít
více	hodně	k6eAd2	hodně
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
hodinách	hodina	k1gFnPc6	hodina
krmení	krmení	k1gNnSc2	krmení
se	se	k3xPyFc4	se
žraloci	žralok	k1gMnPc1	žralok
stávali	stávat	k5eAaImAgMnP	stávat
letargickými	letargický	k2eAgFnPc7d1	letargická
a	a	k8xC	a
přesycenými	přesycený	k2eAgFnPc7d1	přesycená
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
zakusovali	zakusovat	k5eAaImAgMnP	zakusovat
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
sousto	sousto	k1gNnSc1	sousto
od	od	k7c2	od
mršiny	mršina	k1gFnSc2	mršina
oddělit	oddělit	k5eAaPmF	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
pomalu	pomalu	k6eAd1	pomalu
odplouvali	odplouvat	k5eAaImAgMnP	odplouvat
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mršině	mršina	k1gFnSc6	mršina
se	se	k3xPyFc4	se
živilo	živit	k5eAaImAgNnS	živit
až	až	k9	až
osm	osm	k4xCc1	osm
jedinců	jedinec	k1gMnPc2	jedinec
naráz	naráz	k6eAd1	naráz
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nebyly	být	k5eNaImAgInP	být
patrné	patrný	k2eAgInPc1d1	patrný
žádné	žádný	k3yNgInPc1	žádný
stopy	stop	k1gInPc1	stop
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
agrese	agrese	k1gFnSc2	agrese
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
některý	některý	k3yIgMnSc1	některý
žralok	žralok	k1gMnSc1	žralok
omylem	omylem	k6eAd1	omylem
pokousal	pokousat	k5eAaPmAgMnS	pokousat
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgMnPc1d2	menší
jedinci	jedinec	k1gMnPc1	jedinec
obvykle	obvykle	k6eAd1	obvykle
plavali	plavat	k5eAaImAgMnP	plavat
okolo	okolo	k6eAd1	okolo
a	a	k8xC	a
pojídali	pojídat	k5eAaImAgMnP	pojídat
oddělené	oddělený	k2eAgInPc4d1	oddělený
plavající	plavající	k2eAgInPc4d1	plavající
zbytky	zbytek	k1gInPc4	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
velrybích	velrybí	k2eAgFnPc2d1	velrybí
mrtvol	mrtvola	k1gFnPc2	mrtvola
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zahlédnout	zahlédnout	k5eAaPmF	zahlédnout
ty	ten	k3xDgInPc4	ten
největší	veliký	k2eAgInPc4d3	veliký
kusy	kus	k1gInPc4	kus
žraloků	žralok	k1gMnPc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
této	tento	k3xDgFnSc2	tento
výživné	výživný	k2eAgFnSc2d1	výživná
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
snadno	snadno	k6eAd1	snadno
dosažitelné	dosažitelný	k2eAgFnSc2d1	dosažitelná
kořisti	kořist	k1gFnSc2	kořist
před	před	k7c7	před
namáhavým	namáhavý	k2eAgInSc7d1	namáhavý
lovem	lov	k1gInSc7	lov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
mrštnost	mrštnost	k1gFnSc4	mrštnost
a	a	k8xC	a
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
oni	onen	k3xDgMnPc1	onen
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
velikosti	velikost	k1gFnSc3	velikost
už	už	k6eAd1	už
zčásti	zčásti	k6eAd1	zčásti
ztratili	ztratit	k5eAaPmAgMnP	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
zde	zde	k6eAd1	zde
také	také	k9	také
hledají	hledat	k5eAaImIp3nP	hledat
partnery	partner	k1gMnPc4	partner
na	na	k7c4	na
případné	případný	k2eAgNnSc4d1	případné
páření	páření	k1gNnSc4	páření
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
na	na	k7c6	na
velrybích	velrybí	k2eAgFnPc6d1	velrybí
mršinách	mršina	k1gFnPc6	mršina
živí	živit	k5eAaImIp3nP	živit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
tygří	tygří	k2eAgMnPc1d1	tygří
žraloci	žralok	k1gMnPc1	žralok
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
rozmnožování	rozmnožování	k1gNnSc6	rozmnožování
bílých	bílý	k2eAgMnPc2d1	bílý
žraloků	žralok	k1gMnPc2	žralok
nevíme	vědět	k5eNaImIp1nP	vědět
téměř	téměř	k6eAd1	téměř
nic	nic	k3yNnSc4	nic
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgInPc1d1	jistý
důkazy	důkaz	k1gInPc1	důkaz
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodování	hodování	k1gNnSc1	hodování
na	na	k7c6	na
velkých	velký	k2eAgFnPc6d1	velká
mršinách	mršina	k1gFnPc6	mršina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spouštěčem	spouštěč	k1gInSc7	spouštěč
následného	následný	k2eAgNnSc2d1	následné
páření	páření	k1gNnSc2	páření
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žraloci	žralok	k1gMnPc1	žralok
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
pohlavní	pohlavní	k2eAgFnSc3d1	pohlavní
zralosti	zralost	k1gFnSc3	zralost
okolo	okolo	k7c2	okolo
15	[number]	k4	15
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejnovější	nový	k2eAgInPc1d3	nejnovější
výzkumy	výzkum	k1gInPc1	výzkum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dochází	docházet	k5eAaImIp3nS	docházet
podstatně	podstatně	k6eAd1	podstatně
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
sexuálně	sexuálně	k6eAd1	sexuálně
dospívají	dospívat	k5eAaImIp3nP	dospívat
v	v	k7c6	v
asi	asi	k9	asi
26	[number]	k4	26
a	a	k8xC	a
samice	samice	k1gFnPc1	samice
ve	v	k7c6	v
33	[number]	k4	33
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
něco	něco	k3yInSc4	něco
přes	přes	k7c4	přes
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
studie	studie	k1gFnSc1	studie
Woods	Woodsa	k1gFnPc2	Woodsa
Hole	hole	k6eAd1	hole
Oceanographic	Oceanographice	k1gInPc2	Oceanographice
Institution	Institution	k1gInSc4	Institution
jí	on	k3xPp3gFnSc2	on
posunuly	posunout	k5eAaPmAgFnP	posunout
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
zřejmě	zřejmě	k6eAd1	zřejmě
žijí	žít	k5eAaImIp3nP	žít
déle	dlouho	k6eAd2	dlouho
a	a	k8xC	a
rostou	růst	k5eAaImIp3nP	růst
pomaleji	pomale	k6eAd2	pomale
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zjištění	zjištění	k1gNnPc1	zjištění
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
i	i	k9	i
na	na	k7c4	na
zranitelnost	zranitelnost	k1gFnSc4	zranitelnost
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
zástupci	zástupce	k1gMnPc1	zástupce
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
život	život	k1gInSc4	život
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
stihli	stihnout	k5eAaPmAgMnP	stihnout
rozmnožit	rozmnožit	k5eAaPmF	rozmnožit
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
víme	vědět	k5eAaImIp1nP	vědět
o	o	k7c4	o
chování	chování	k1gNnSc4	chování
při	při	k7c6	při
páření	páření	k1gNnSc6	páření
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mají	mít	k5eAaImIp3nP	mít
u	u	k7c2	u
břišních	břišní	k2eAgFnPc2d1	břišní
ploutví	ploutev	k1gFnPc2	ploutev
párový	párový	k2eAgInSc1d1	párový
pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
orgán	orgán	k1gInSc1	orgán
(	(	kIx(	(
<g/>
clasper	clasper	k1gInSc1	clasper
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
musejí	muset	k5eAaImIp3nP	muset
samici	samice	k1gFnSc4	samice
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
uchopit	uchopit	k5eAaPmF	uchopit
čelistmi	čelist	k1gFnPc7	čelist
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
jí	on	k3xPp3gFnSc7	on
mohli	moct	k5eAaImAgMnP	moct
přidržet	přidržet	k5eAaPmF	přidržet
a	a	k8xC	a
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
orgán	orgán	k1gInSc4	orgán
zasunout	zasunout	k5eAaPmF	zasunout
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dospělé	dospělý	k2eAgFnPc1d1	dospělá
samice	samice	k1gFnPc1	samice
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
prsních	prsní	k2eAgFnPc2d1	prsní
ploutví	ploutev	k1gFnPc2	ploutev
stopy	stopa	k1gFnSc2	stopa
zubů	zub	k1gInPc2	zub
jiných	jiný	k2eAgMnPc2d1	jiný
žraloků	žralok	k1gMnPc2	žralok
a	a	k8xC	a
vědci	vědec	k1gMnPc1	vědec
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
právě	právě	k9	právě
o	o	k7c4	o
kousance	kousanec	k1gInPc4	kousanec
samců	samec	k1gMnPc2	samec
uštědřené	uštědřený	k2eAgInPc4d1	uštědřený
během	během	k7c2	během
páření	páření	k1gNnSc2	páření
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
spatřen	spatřit	k5eAaPmNgInS	spatřit
porod	porod	k1gInSc1	porod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
březí	březí	k2eAgFnPc1d1	březí
samice	samice	k1gFnPc1	samice
byly	být	k5eAaImAgFnP	být
zkoumány	zkoumán	k2eAgFnPc1d1	zkoumána
a	a	k8xC	a
vyšetřovány	vyšetřován	k2eAgFnPc1d1	vyšetřována
<g/>
.	.	kIx.	.
</s>
<s>
Žraloci	žralok	k1gMnPc1	žralok
jsou	být	k5eAaImIp3nP	být
vejcoživorodí	vejcoživorodý	k2eAgMnPc1d1	vejcoživorodý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vajíčka	vajíčko	k1gNnPc1	vajíčko
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
v	v	k7c6	v
děloze	děloha	k1gFnSc6	děloha
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
vyklubání	vyklubání	k1gNnSc2	vyklubání
mláděte	mládě	k1gNnSc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
březosti	březost	k1gFnSc2	březost
trvá	trvat	k5eAaImIp3nS	trvat
zřejmě	zřejmě	k6eAd1	zřejmě
11	[number]	k4	11
-	-	kIx~	-
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
v	v	k7c6	v
děloze	děloha	k1gFnSc6	děloha
požírají	požírat	k5eAaImIp3nP	požírat
neoplodněná	oplodněný	k2eNgNnPc1d1	neoplodněné
vajíčka	vajíčko	k1gNnPc1	vajíčko
produkovaná	produkovaný	k2eAgFnSc1d1	produkovaná
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
mláďat	mládě	k1gNnPc2	mládě
při	při	k7c6	při
narození	narození	k1gNnSc6	narození
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
metr	metr	k1gInSc1	metr
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
120	[number]	k4	120
-	-	kIx~	-
150	[number]	k4	150
cm	cm	kA	cm
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
poněkud	poněkud	k6eAd1	poněkud
menší	malý	k2eAgFnSc1d2	menší
(	(	kIx(	(
<g/>
85	[number]	k4	85
cm	cm	kA	cm
<g/>
)	)	kIx)	)
či	či	k8xC	či
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Vrh	vrh	k1gInSc1	vrh
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
2	[number]	k4	2
-	-	kIx~	-
10	[number]	k4	10
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Vyskakování	vyskakování	k1gNnSc1	vyskakování
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
žraloky	žralok	k1gMnPc4	žralok
lovící	lovící	k2eAgMnPc4d1	lovící
lachtany	lachtan	k1gMnPc4	lachtan
jihoafrické	jihoafrický	k2eAgNnSc1d1	jihoafrické
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
sice	sice	k8xC	sice
o	o	k7c4	o
nepředvídatelné	předvídatelný	k2eNgInPc4d1	nepředvídatelný
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
obtížně	obtížně	k6eAd1	obtížně
zdokumentovatelné	zdokumentovatelný	k2eAgInPc4d1	zdokumentovatelný
skoky	skok	k1gInPc4	skok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existuje	existovat	k5eAaImIp3nS	existovat
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
již	již	k6eAd1	již
množství	množství	k1gNnSc1	množství
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
filmového	filmový	k2eAgInSc2d1	filmový
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Žraloci	žralok	k1gMnPc1	žralok
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
okolo	okolo	k7c2	okolo
40	[number]	k4	40
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
vyskakují	vyskakovat	k5eAaImIp3nP	vyskakovat
až	až	k9	až
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
polovina	polovina	k1gFnSc1	polovina
těchto	tento	k3xDgInPc2	tento
skokových	skokový	k2eAgInPc2d1	skokový
útoků	útok	k1gInPc2	útok
končí	končit	k5eAaImIp3nS	končit
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
skočil	skočit	k5eAaPmAgMnS	skočit
třímetrový	třímetrový	k2eAgMnSc1d1	třímetrový
žralok	žralok	k1gMnSc1	žralok
do	do	k7c2	do
výzkumného	výzkumný	k2eAgNnSc2d1	výzkumné
plavidla	plavidlo	k1gNnSc2	plavidlo
v	v	k7c4	v
Mosel	Mosel	k1gInSc4	Mosel
Bay	Bay	k1gFnSc2	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
incident	incident	k1gInSc1	incident
byl	být	k5eAaImAgInS	být
vyhodnocen	vyhodnotit	k5eAaPmNgInS	vyhodnotit
nikoliv	nikoliv	k9	nikoliv
jako	jako	k8xC	jako
útok	útok	k1gInSc1	útok
ale	ale	k8xC	ale
nehoda	nehoda	k1gFnSc1	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Nešlo	jít	k5eNaImAgNnS	jít
však	však	k9	však
o	o	k7c4	o
ojedinělý	ojedinělý	k2eAgInSc4d1	ojedinělý
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
žraloci	žralok	k1gMnPc1	žralok
bílí	bílý	k2eAgMnPc1d1	bílý
jsou	být	k5eAaImIp3nP	být
pověstní	pověstný	k2eAgMnPc1d1	pověstný
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokážou	dokázat	k5eAaPmIp3nP	dokázat
skočit	skočit	k5eAaPmF	skočit
do	do	k7c2	do
malých	malý	k2eAgFnPc2d1	malá
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
člunů	člun	k1gInPc2	člun
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
maximální	maximální	k2eAgFnSc2d1	maximální
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
jsou	být	k5eAaImIp3nP	být
žraloci	žralok	k1gMnPc1	žralok
schopni	schopen	k2eAgMnPc1d1	schopen
vyvinout	vyvinout	k5eAaPmF	vyvinout
<g/>
,	,	kIx,	,
přesné	přesný	k2eAgInPc1d1	přesný
údaje	údaj	k1gInPc1	údaj
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
že	že	k8xS	že
40	[number]	k4	40
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
je	být	k5eAaImIp3nS	být
víceméně	víceméně	k9	víceméně
jistá	jistý	k2eAgFnSc1d1	jistá
dolní	dolní	k2eAgFnSc1d1	dolní
hranice	hranice	k1gFnSc1	hranice
maximální	maximální	k2eAgFnSc2d1	maximální
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
uvažují	uvažovat	k5eAaImIp3nP	uvažovat
o	o	k7c4	o
nejméně	málo	k6eAd3	málo
56	[number]	k4	56
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
při	při	k7c6	při
krátkodobém	krátkodobý	k2eAgInSc6d1	krátkodobý
maximálním	maximální	k2eAgInSc6d1	maximální
výkonu	výkon	k1gInSc6	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
žralok	žralok	k1gMnSc1	žralok
bílý	bílý	k1gMnSc1	bílý
dostává	dostávat	k5eAaImIp3nS	dostávat
mezi	mezi	k7c4	mezi
nejrychlejší	rychlý	k2eAgMnPc4d3	nejrychlejší
žraloky	žralok	k1gMnPc4	žralok
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
ho	on	k3xPp3gMnSc4	on
překonávají	překonávat	k5eAaImIp3nP	překonávat
jenom	jenom	k9	jenom
jeho	jeho	k3xOp3gMnPc1	jeho
bratranci	bratranec	k1gMnPc1	bratranec
žraloci	žralok	k1gMnPc1	žralok
mako	mako	k1gNnSc1	mako
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c6	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Mezidruhová	mezidruhový	k2eAgFnSc1d1	mezidruhová
kompetice	kompetika	k1gFnSc3	kompetika
s	s	k7c7	s
kosatkou	kosatka	k1gFnSc7	kosatka
dravou	dravý	k2eAgFnSc7d1	dravá
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
oba	dva	k4xCgMnPc1	dva
predátoři	predátor	k1gMnPc1	predátor
loví	lovit	k5eAaImIp3nP	lovit
stejnou	stejný	k2eAgFnSc4d1	stejná
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
interakce	interakce	k1gFnPc1	interakce
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgMnPc7	tento
dvěma	dva	k4xCgMnPc7	dva
predátory	predátor	k1gMnPc7	predátor
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
prozkoumána	prozkoumat	k5eAaPmNgNnP	prozkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Kosatky	kosatka	k1gFnSc2	kosatka
dravé	dravý	k2eAgFnSc2d1	dravá
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c2	za
jediného	jediný	k2eAgMnSc2d1	jediný
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
zřejmě	zřejmě	k6eAd1	zřejmě
jen	jen	k9	jen
občasného	občasný	k2eAgMnSc2d1	občasný
predátora	predátor	k1gMnSc2	predátor
bílých	bílý	k2eAgMnPc2d1	bílý
žraloků	žralok	k1gMnPc2	žralok
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1997	[number]	k4	1997
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
Farallon	Farallon	k1gInSc1	Farallon
Islands	Islands	k1gInSc1	Islands
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
asi	asi	k9	asi
5	[number]	k4	5
<g/>
metrová	metrový	k2eAgFnSc1d1	metrová
samice	samice	k1gFnSc1	samice
kosatky	kosatka	k1gFnSc2	kosatka
znehybnila	znehybnit	k5eAaPmAgFnS	znehybnit
3	[number]	k4	3
-	-	kIx~	-
4	[number]	k4	4
metry	metr	k1gInPc1	metr
dlouhého	dlouhý	k2eAgMnSc2d1	dlouhý
žraloka	žralok	k1gMnSc2	žralok
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
že	že	k9	že
ho	on	k3xPp3gNnSc4	on
převrátila	převrátit	k5eAaPmAgFnS	převrátit
na	na	k7c4	na
záda	záda	k1gNnPc4	záda
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
ho	on	k3xPp3gInSc4	on
ochromila	ochromit	k5eAaPmAgFnS	ochromit
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
prudkým	prudký	k2eAgInSc7d1	prudký
nárazem	náraz	k1gInSc7	náraz
anebo	anebo	k8xC	anebo
právě	právě	k9	právě
převrácením	převrácení	k1gNnSc7	převrácení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nedošlo	dojít	k5eNaPmAgNnS	dojít
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
oběti	oběť	k1gFnSc2	oběť
čelistmi	čelist	k1gFnPc7	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Žraloka	žralok	k1gMnSc4	žralok
následně	následně	k6eAd1	následně
udržovala	udržovat	k5eAaImAgFnS	udržovat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
poloze	poloha	k1gFnSc6	poloha
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
udusil	udusit	k5eAaPmAgMnS	udusit
<g/>
.	.	kIx.	.
</s>
<s>
Kosatka	kosatka	k1gFnSc1	kosatka
mu	on	k3xPp3gMnSc3	on
pak	pak	k6eAd1	pak
zčásti	zčásti	k6eAd1	zčásti
sežrala	sežrat	k5eAaPmAgNnP	sežrat
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
bohatý	bohatý	k2eAgInSc1d1	bohatý
zdroj	zdroj	k1gInSc1	zdroj
tuků	tuk	k1gInPc2	tuk
a	a	k8xC	a
olejů	olej	k1gInPc2	olej
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
útok	útok	k1gInSc1	útok
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
výsledek	výsledek	k1gInSc1	výsledek
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zmíněných	zmíněný	k2eAgInPc6d1	zmíněný
útocích	útok	k1gInPc6	útok
místní	místní	k2eAgFnSc2d1	místní
populace	populace	k1gFnSc2	populace
odhadovaná	odhadovaný	k2eAgFnSc1d1	odhadovaná
na	na	k7c4	na
100	[number]	k4	100
žraloků	žralok	k1gMnPc2	žralok
zmizela	zmizet	k5eAaPmAgFnS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
opatřen	opatřit	k5eAaPmNgInS	opatřit
vysílačkou	vysílačka	k1gFnSc7	vysílačka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
potopil	potopit	k5eAaPmAgInS	potopit
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
odplaval	odplavat	k5eAaPmAgMnS	odplavat
k	k	k7c3	k
Havaji	Havaj	k1gFnSc3	Havaj
<g/>
.	.	kIx.	.
</s>
<s>
Žraloci	žralok	k1gMnPc1	žralok
vycítí	vycítit	k5eAaPmIp3nP	vycítit
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
a	a	k8xC	a
opuštění	opuštění	k1gNnSc4	opuštění
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
následný	následný	k2eAgInSc1d1	následný
obranný	obranný	k2eAgInSc1d1	obranný
mechanismus	mechanismus	k1gInSc1	mechanismus
vůči	vůči	k7c3	vůči
přítomnosti	přítomnost	k1gFnSc3	přítomnost
svého	svůj	k3xOyFgMnSc2	svůj
predátora	predátor	k1gMnSc2	predátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
napadlo	napadnout	k5eAaPmAgNnS	napadnout
a	a	k8xC	a
zabilo	zabít	k5eAaPmAgNnS	zabít
stádo	stádo	k1gNnSc4	stádo
kosatek	kosatka	k1gFnPc2	kosatka
bílého	bílý	k1gMnSc2	bílý
žraloka	žralok	k1gMnSc2	žralok
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
jižní	jižní	k2eAgFnSc2d1	jižní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Nemoci	nemoc	k1gFnSc3	nemoc
žraloků	žralok	k1gMnPc2	žralok
bílých	bílý	k2eAgMnPc2d1	bílý
nebyly	být	k5eNaImAgFnP	být
zdokumentovány	zdokumentovat	k5eAaPmNgFnP	zdokumentovat
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Občasné	občasný	k2eAgInPc1d1	občasný
nálezy	nález	k1gInPc1	nález
mrtvých	mrtvý	k1gMnPc2	mrtvý
kusů	kus	k1gInPc2	kus
na	na	k7c6	na
plážích	pláž	k1gFnPc6	pláž
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nevykazovaly	vykazovat	k5eNaImAgFnP	vykazovat
žádné	žádný	k3yNgFnPc1	žádný
známky	známka	k1gFnPc1	známka
zranění	zranění	k1gNnSc2	zranění
či	či	k8xC	či
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgInP	být
zatím	zatím	k6eAd1	zatím
uspokojivě	uspokojivě	k6eAd1	uspokojivě
vysvětleny	vysvětlit	k5eAaPmNgFnP	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
zaznamenaným	zaznamenaný	k2eAgMnSc7d1	zaznamenaný
ektoparazitem	ektoparazit	k1gMnSc7	ektoparazit
je	být	k5eAaImIp3nS	být
žraloček	žraločka	k1gFnPc2	žraločka
brazilský	brazilský	k2eAgInSc1d1	brazilský
(	(	kIx(	(
<g/>
Isistius	Isistius	k1gInSc1	Isistius
brasiliensis	brasiliensis	k1gFnSc2	brasiliensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
kromě	kromě	k7c2	kromě
bílých	bílý	k2eAgMnPc2d1	bílý
žraloků	žralok	k1gMnPc2	žralok
napadá	napadat	k5eAaPmIp3nS	napadat
i	i	k8xC	i
další	další	k2eAgMnPc4d1	další
větší	veliký	k2eAgMnPc4d2	veliký
mořské	mořský	k2eAgMnPc4d1	mořský
živočichy	živočich	k1gMnPc4	živočich
včetně	včetně	k7c2	včetně
dalších	další	k2eAgMnPc2d1	další
žraloků	žralok	k1gMnPc2	žralok
a	a	k8xC	a
kytovců	kytovec	k1gMnPc2	kytovec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
maximálně	maximálně	k6eAd1	maximálně
šedesáti	šedesát	k4xCc7	šedesát
centimetrová	centimetrový	k2eAgFnSc1d1	centimetrová
paryba	paryba	k1gFnSc1	paryba
doutníkovitého	doutníkovitý	k2eAgInSc2d1	doutníkovitý
tvaru	tvar	k1gInSc2	tvar
se	se	k3xPyFc4	se
přisaje	přisát	k5eAaPmIp3nS	přisát
na	na	k7c4	na
tělo	tělo	k1gNnSc4	tělo
své	svůj	k3xOyFgFnSc2	svůj
oběti	oběť	k1gFnSc2	oběť
a	a	k8xC	a
rotačním	rotační	k2eAgInSc7d1	rotační
pohybem	pohyb	k1gInSc7	pohyb
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
pomocí	pomocí	k7c2	pomocí
svých	svůj	k3xOyFgFnPc2	svůj
čelistí	čelist	k1gFnPc2	čelist
vyřízne	vyříznout	k5eAaPmIp3nS	vyříznout
kus	kus	k1gInSc1	kus
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
masa	maso	k1gNnSc2	maso
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
několika	několik	k4yIc2	několik
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Zranění	zranění	k1gNnSc1	zranění
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jich	on	k3xPp3gMnPc2	on
není	být	k5eNaImIp3nS	být
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
nebývají	bývat	k5eNaImIp3nP	bývat
fatální	fatální	k2eAgFnPc1d1	fatální
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
žraloků	žralok	k1gMnPc2	žralok
jsou	být	k5eAaImIp3nP	být
velcí	velký	k2eAgMnPc1d1	velký
bílí	bílý	k1gMnPc1	bílý
zodpovědní	zodpovědný	k2eAgMnPc1d1	zodpovědný
za	za	k7c2	za
jednoznačně	jednoznačně	k6eAd1	jednoznačně
nejvíce	nejvíce	k6eAd1	nejvíce
nevyprovokovaných	vyprovokovaný	k2eNgInPc2d1	nevyprovokovaný
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1580	[number]	k4	1580
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
statisticky	statisticky	k6eAd1	statisticky
evidováno	evidovat	k5eAaImNgNnS	evidovat
celkem	celek	k1gInSc7	celek
314	[number]	k4	314
případů	případ	k1gInPc2	případ
napadení	napadení	k1gNnPc2	napadení
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
útocích	útok	k1gInPc6	útok
zabili	zabít	k5eAaPmAgMnP	zabít
žraloci	žralok	k1gMnPc1	žralok
již	již	k6eAd1	již
nejméně	málo	k6eAd3	málo
80	[number]	k4	80
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
kořist	kořist	k1gFnSc4	kořist
a	a	k8xC	a
jen	jen	k9	jen
málo	málo	k4c1	málo
napadení	napadení	k1gNnPc2	napadení
končí	končit	k5eAaImIp3nS	končit
fatálně	fatálně	k6eAd1	fatálně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
-	-	kIx~	-
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
153	[number]	k4	153
útoků	útok	k1gInPc2	útok
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
35	[number]	k4	35
smrtelných	smrtelný	k2eAgFnPc2d1	smrtelná
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
jen	jen	k9	jen
1,5	[number]	k4	1,5
úmrtí	úmrtí	k1gNnPc2	úmrtí
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
došlo	dojít	k5eAaPmAgNnS	dojít
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
200	[number]	k4	200
let	léto	k1gNnPc2	léto
jen	jen	k9	jen
k	k	k7c3	k
31	[number]	k4	31
potvrzeným	potvrzený	k2eAgInPc3d1	potvrzený
útokům	útok	k1gInPc3	útok
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
měla	mít	k5eAaImAgFnS	mít
charakter	charakter	k1gInSc4	charakter
zkušebních	zkušební	k2eAgNnPc2d1	zkušební
kousnutí	kousnutí	k1gNnPc2	kousnutí
<g/>
.	.	kIx.	.
</s>
<s>
Žraloci	žralok	k1gMnPc1	žralok
takto	takto	k6eAd1	takto
hryžou	hryzat	k5eAaImIp3nP	hryzat
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgInPc2d1	různý
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
o	o	k7c4	o
co	co	k3yQnSc4	co
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
všeobecnému	všeobecný	k2eAgNnSc3d1	všeobecné
mínění	mínění	k1gNnSc3	mínění
není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
pletli	plést	k5eAaImAgMnP	plést
lidi	člověk	k1gMnPc4	člověk
s	s	k7c7	s
lachtany	lachtan	k1gMnPc7	lachtan
a	a	k8xC	a
jinými	jiný	k2eAgMnPc7d1	jiný
ploutvonožci	ploutvonožec	k1gMnPc7	ploutvonožec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
napadením	napadení	k1gNnPc3	napadení
lidí	člověk	k1gMnPc2	člověk
dochází	docházet	k5eAaImIp3nS	docházet
úplně	úplně	k6eAd1	úplně
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
než	než	k8xS	než
ploutvonožců	ploutvonožec	k1gMnPc2	ploutvonožec
-	-	kIx~	-
nebyl	být	k5eNaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
na	na	k7c6	na
plavce	plavka	k1gFnSc6	plavka
útočili	útočit	k5eAaImAgMnP	útočit
prudkým	prudký	k2eAgInSc7d1	prudký
výpadem	výpad	k1gInSc7	výpad
zespodu	zespodu	k6eAd1	zespodu
s	s	k7c7	s
následným	následný	k2eAgInSc7d1	následný
výskokem	výskok	k1gInSc7	výskok
nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
se	se	k3xPyFc4	se
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
koušou	kousat	k5eAaImIp3nP	kousat
jakoby	jakoby	k8xS	jakoby
bez	bez	k7c2	bez
zásadního	zásadní	k2eAgNnSc2d1	zásadní
úsilí	úsilí	k1gNnSc2	úsilí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
většině	většina	k1gFnSc3	většina
útoků	útok	k1gInPc2	útok
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
kalných	kalný	k2eAgFnPc6d1	kalná
vodách	voda	k1gFnPc6	voda
či	či	k8xC	či
při	při	k7c6	při
nízké	nízký	k2eAgFnSc6d1	nízká
viditelnosti	viditelnost	k1gFnSc6	viditelnost
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
žralokům	žralok	k1gMnPc3	žralok
buď	buď	k8xC	buď
nechutnají	chutnat	k5eNaImIp3nP	chutnat
nebo	nebo	k8xC	nebo
jim	on	k3xPp3gInPc3	on
chutnají	chutnat	k5eAaImIp3nP	chutnat
nezvykle	zvykle	k6eNd1	zvykle
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
možná	možná	k9	možná
příliš	příliš	k6eAd1	příliš
kostnaté	kostnatý	k2eAgNnSc1d1	kostnaté
a	a	k8xC	a
málo	málo	k6eAd1	málo
tučné	tučný	k2eAgNnSc4d1	tučné
<g/>
.	.	kIx.	.
</s>
<s>
Málokdy	málokdy	k6eAd1	málokdy
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
po	po	k7c6	po
zkušebním	zkušební	k2eAgInSc6d1	zkušební
kousnutí	kousnutý	k2eAgMnPc1d1	kousnutý
svůj	svůj	k3xOyFgInSc4	svůj
útok	útok	k1gInSc4	útok
opakovali	opakovat	k5eAaImAgMnP	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Nevhodný	vhodný	k2eNgInSc1d1	nevhodný
výživový	výživový	k2eAgInSc1d1	výživový
poměr	poměr	k1gInSc1	poměr
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
pro	pro	k7c4	pro
žraločí	žraločí	k2eAgNnSc4d1	žraločí
zažívání	zažívání	k1gNnSc4	zažívání
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
tyto	tento	k3xDgFnPc1	tento
paryby	paryba	k1gFnPc1	paryba
při	při	k7c6	při
prvním	první	k4xOgNnSc6	první
kousnutí	kousnutí	k1gNnSc6	kousnutí
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zanalyzovat	zanalyzovat	k5eAaPmF	zanalyzovat
<g/>
.	.	kIx.	.
</s>
<s>
Smrtelné	smrtelný	k2eAgInPc1d1	smrtelný
útoky	útok	k1gInPc1	útok
jsou	být	k5eAaImIp3nP	být
tudíž	tudíž	k8xC	tudíž
většinou	většinou	k6eAd1	většinou
výsledkem	výsledek	k1gInSc7	výsledek
značné	značný	k2eAgFnSc2d1	značná
ztráty	ztráta	k1gFnSc2	ztráta
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
jen	jen	k9	jen
málokdy	málokdy	k6eAd1	málokdy
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
vyrvání	vyrvání	k1gNnSc3	vyrvání
velkého	velký	k2eAgInSc2d1	velký
kusu	kus	k1gInSc2	kus
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
už	už	k9	už
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
nohy	noha	k1gFnSc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
výzkumníci	výzkumník	k1gMnPc1	výzkumník
nicméně	nicméně	k8xC	nicméně
přišli	přijít	k5eAaPmAgMnP	přijít
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tak	tak	k6eAd1	tak
málo	málo	k6eAd1	málo
opakovaným	opakovaný	k2eAgMnPc3d1	opakovaný
útokům	útok	k1gInPc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
údajně	údajně	k6eAd1	údajně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidské	lidský	k2eAgFnPc1d1	lidská
oběti	oběť	k1gFnPc1	oběť
dokážou	dokázat	k5eAaPmIp3nP	dokázat
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
útoku	útok	k1gInSc6	útok
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
někdo	někdo	k3yInSc1	někdo
vytáhne	vytáhnout	k5eAaPmIp3nS	vytáhnout
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Žraloci	žralok	k1gMnPc1	žralok
bílí	bílit	k5eAaImIp3nP	bílit
totiž	totiž	k9	totiž
používají	používat	k5eAaImIp3nP	používat
metodu	metoda	k1gFnSc4	metoda
jednoho	jeden	k4xCgNnSc2	jeden
kousnutí	kousnutí	k1gNnSc2	kousnutí
a	a	k8xC	a
následného	následný	k2eAgNnSc2d1	následné
vyčkávání	vyčkávání	k1gNnSc2	vyčkávání
<g/>
,	,	kIx,	,
až	až	k9	až
jejich	jejich	k3xOp3gFnSc4	jejich
kořist	kořist	k1gFnSc4	kořist
vykrvácí	vykrvácet	k5eAaPmIp3nS	vykrvácet
nebo	nebo	k8xC	nebo
zeslábne	zeslábnout	k5eAaPmIp3nS	zeslábnout
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
bez	bez	k7c2	bez
rizika	riziko	k1gNnSc2	riziko
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
poraněné	poraněný	k2eAgMnPc4d1	poraněný
lidi	člověk	k1gMnPc4	člověk
někdo	někdo	k3yInSc1	někdo
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
vytáhne	vytáhnout	k5eAaPmIp3nS	vytáhnout
<g/>
,	,	kIx,	,
žraloci	žralok	k1gMnPc1	žralok
nepředpokládají	předpokládat	k5eNaImIp3nP	předpokládat
a	a	k8xC	a
u	u	k7c2	u
jejich	jejich	k3xOp3gFnSc2	jejich
běžné	běžný	k2eAgFnSc2d1	běžná
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
ploutvonožci	ploutvonožec	k1gMnPc1	ploutvonožec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
nestává	stávat	k5eNaImIp3nS	stávat
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
výše	vysoce	k6eAd2	vysoce
řečené	řečený	k2eAgNnSc4d1	řečené
představují	představovat	k5eAaImIp3nP	představovat
žraloci	žralok	k1gMnPc1	žralok
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
oblastech	oblast	k1gFnPc6	oblast
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
některé	některý	k3yIgMnPc4	některý
úřady	úřada	k1gMnPc4	úřada
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
jednají	jednat	k5eAaImIp3nP	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Západní	západní	k2eAgFnSc2d1	západní
Austrálie	Austrálie	k1gFnSc2	Austrálie
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
nařízení	nařízení	k1gNnSc2	nařízení
o	o	k7c6	o
selektivním	selektivní	k2eAgInSc6d1	selektivní
lovu	lov	k1gInSc6	lov
velkých	velký	k2eAgMnPc2d1	velký
žraloků	žralok	k1gMnPc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rizikových	rizikový	k2eAgNnPc6d1	rizikové
místech	místo	k1gNnPc6	místo
byly	být	k5eAaImAgFnP	být
nataženy	natažen	k2eAgFnPc4d1	natažena
sítě	síť	k1gFnPc4	síť
s	s	k7c7	s
háky	hák	k1gInPc7	hák
a	a	k8xC	a
návnadami	návnada	k1gFnPc7	návnada
<g/>
,	,	kIx,	,
chycení	chycený	k2eAgMnPc1d1	chycený
žraloci	žralok	k1gMnPc1	žralok
jsou	být	k5eAaImIp3nP	být
stříleni	střílen	k2eAgMnPc1d1	střílen
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
těla	tělo	k1gNnSc2	tělo
házena	házet	k5eAaImNgFnS	házet
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Bílí	bílý	k2eAgMnPc1d1	bílý
žraloci	žralok	k1gMnPc1	žralok
občas	občas	k6eAd1	občas
napadají	napadat	k5eAaImIp3nP	napadat
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
potápějí	potápět	k5eAaImIp3nP	potápět
malé	malý	k2eAgFnPc1d1	malá
lodě	loď	k1gFnPc1	loď
či	či	k8xC	či
čluny	člun	k1gInPc1	člun
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
108	[number]	k4	108
potvrzených	potvrzený	k2eAgInPc2d1	potvrzený
útoků	útok	k1gInPc2	útok
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jen	jen	k9	jen
5	[number]	k4	5
týkalo	týkat	k5eAaImAgNnS	týkat
napadení	napadení	k1gNnSc1	napadení
kajakářů	kajakář	k1gMnPc2	kajakář
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
délka	délka	k1gFnSc1	délka
napadených	napadený	k2eAgFnPc2d1	napadená
lodí	loď	k1gFnPc2	loď
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
narážejí	narážet	k5eAaPmIp3nP	narážet
do	do	k7c2	do
zádi	záď	k1gFnSc2	záď
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
občas	občas	k6eAd1	občas
loď	loď	k1gFnSc1	loď
poškodí	poškodit	k5eAaPmIp3nS	poškodit
a	a	k8xC	a
můžou	můžou	k?	můžou
shodit	shodit	k5eAaPmF	shodit
posádku	posádka	k1gFnSc4	posádka
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
jeden	jeden	k4xCgMnSc1	jeden
velký	velký	k2eAgMnSc1d1	velký
žralok	žralok	k1gMnSc1	žralok
skočil	skočit	k5eAaPmAgMnS	skočit
do	do	k7c2	do
rybářského	rybářský	k2eAgInSc2d1	rybářský
člunu	člun	k1gInSc2	člun
Lucky	lucky	k6eAd1	lucky
Jim	on	k3xPp3gMnPc3	on
a	a	k8xC	a
katapultoval	katapultovat	k5eAaBmAgMnS	katapultovat
rybáře	rybář	k1gMnSc4	rybář
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
lodí	loď	k1gFnPc2	loď
láká	lákat	k5eAaImIp3nS	lákat
žraloky	žralok	k1gMnPc4	žralok
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
polem	pole	k1gNnSc7	pole
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
statistické	statistický	k2eAgInPc4d1	statistický
údaje	údaj	k1gInPc4	údaj
a	a	k8xC	a
vědecká	vědecký	k2eAgNnPc4d1	vědecké
bádání	bádání	k1gNnPc4	bádání
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
nazírání	nazírání	k1gNnSc1	nazírání
na	na	k7c4	na
tohoto	tento	k3xDgMnSc4	tento
predátora	predátor	k1gMnSc4	predátor
vydání	vydání	k1gNnSc2	vydání
knihy	kniha	k1gFnSc2	kniha
Čelisti	čelist	k1gFnSc2	čelist
od	od	k7c2	od
Petera	Peter	k1gMnSc2	Peter
Benchleye	Benchley	k1gFnSc2	Benchley
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
následné	následný	k2eAgNnSc4d1	následné
zfilmování	zfilmování	k1gNnSc4	zfilmování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgMnSc1d1	bílý
žralok	žralok	k1gMnSc1	žralok
dostal	dostat	k5eAaPmAgMnS	dostat
mezi	mezi	k7c7	mezi
veřejností	veřejnost	k1gFnSc7	veřejnost
nálepku	nálepek	k1gInSc2	nálepek
extrémně	extrémně	k6eAd1	extrémně
nebezpečného	bezpečný	k2eNgMnSc4d1	nebezpečný
<g/>
,	,	kIx,	,
krvelačného	krvelačný	k2eAgMnSc4d1	krvelačný
<g/>
,	,	kIx,	,
ba	ba	k9	ba
přímo	přímo	k6eAd1	přímo
děsivého	děsivý	k2eAgMnSc4d1	děsivý
lidožrouta	lidožrout	k1gMnSc4	lidožrout
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
filmu	film	k1gInSc2	film
začal	začít	k5eAaPmAgInS	začít
masivní	masivní	k2eAgInSc1d1	masivní
hon	hon	k1gInSc1	hon
na	na	k7c4	na
žraloky	žralok	k1gMnPc4	žralok
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
premiéře	premiéra	k1gFnSc6	premiéra
Čelistí	čelist	k1gFnPc2	čelist
počet	počet	k1gInSc1	počet
velkých	velký	k2eAgMnPc2d1	velký
bílých	bílý	k2eAgMnPc2d1	bílý
žraloků	žralok	k1gMnPc2	žralok
u	u	k7c2	u
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
o	o	k7c4	o
50	[number]	k4	50
%	%	kIx~	%
Populace	populace	k1gFnSc1	populace
žraloků	žralok	k1gMnPc2	žralok
bílých	bílý	k2eAgInPc2d1	bílý
<g/>
,	,	kIx,	,
tygřích	tygří	k2eAgInPc2d1	tygří
a	a	k8xC	a
kladivounů	kladivoun	k1gMnPc2	kladivoun
spadly	spadnout	k5eAaPmAgFnP	spadnout
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
až	až	k6eAd1	až
o	o	k7c4	o
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Peter	Peter	k1gMnSc1	Peter
Benchley	Benchlea	k1gFnSc2	Benchlea
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
věnoval	věnovat	k5eAaPmAgInS	věnovat
ochraně	ochrana	k1gFnSc3	ochrana
žraloků	žralok	k1gMnPc2	žralok
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
by	by	kYmCp3nS	by
takovou	takový	k3xDgFnSc4	takový
knihu	kniha	k1gFnSc4	kniha
nikdy	nikdy	k6eAd1	nikdy
nenapsal	napsat	k5eNaPmAgMnS	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byli	být	k5eAaImAgMnP	být
žraloci	žralok	k1gMnPc1	žralok
bílí	bílit	k5eAaImIp3nP	bílit
chováni	chovat	k5eAaImNgMnP	chovat
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
akváriích	akvárium	k1gNnPc6	akvárium
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Vždycky	vždycky	k6eAd1	vždycky
brzy	brzy	k6eAd1	brzy
uhynuli	uhynout	k5eAaPmAgMnP	uhynout
anebo	anebo	k8xC	anebo
byli	být	k5eAaImAgMnP	být
puštěni	pustit	k5eAaPmNgMnP	pustit
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
volné	volný	k2eAgFnSc6d1	volná
moře	mora	k1gFnSc6	mora
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
udržet	udržet	k5eAaPmF	udržet
žraloka	žralok	k1gMnSc4	žralok
v	v	k7c6	v
akváriu	akvárium	k1gNnSc6	akvárium
téměř	téměř	k6eAd1	téměř
200	[number]	k4	200
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
samici	samice	k1gFnSc4	samice
chycenou	chycený	k2eAgFnSc4d1	chycená
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
drženou	držený	k2eAgFnSc7d1	držená
v	v	k7c6	v
Monterey	Monterey	k1gInPc1	Monterey
Bay	Bay	k1gFnSc7	Bay
Aquarium	Aquarium	k1gNnSc4	Aquarium
o	o	k7c6	o
objemu	objem	k1gInSc6	objem
3	[number]	k4	3
800	[number]	k4	800
000	[number]	k4	000
litrů	litr	k1gInPc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgNnSc2	svůj
zajetí	zajetí	k1gNnSc2	zajetí
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
o	o	k7c4	o
35	[number]	k4	35
cm	cm	kA	cm
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
napadala	napadat	k5eAaBmAgFnS	napadat
ostatní	ostatní	k2eAgMnPc4d1	ostatní
žraloky	žralok	k1gMnPc4	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
měsíc	měsíc	k1gInSc4	měsíc
sledována	sledovat	k5eAaImNgFnS	sledovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
několika	několik	k4yIc6	několik
jiných	jiný	k2eAgNnPc6d1	jiné
akváriích	akvárium	k1gNnPc6	akvárium
drženo	držet	k5eAaImNgNnS	držet
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
kusů	kus	k1gInPc2	kus
podobně	podobně	k6eAd1	podobně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
chovat	chovat	k5eAaImF	chovat
tyto	tento	k3xDgMnPc4	tento
velké	velký	k2eAgMnPc4d1	velký
predátory	predátor	k1gMnPc4	predátor
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
odmítají	odmítat	k5eAaImIp3nP	odmítat
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
narážejí	narážet	k5eAaPmIp3nP	narážet
do	do	k7c2	do
stěn	stěna	k1gFnPc2	stěna
akvária	akvárium	k1gNnSc2	akvárium
<g/>
.	.	kIx.	.
</s>
<s>
Potápění	potápění	k1gNnSc1	potápění
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInSc1d1	populární
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
výskytu	výskyt	k1gInSc3	výskyt
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
praxí	praxe	k1gFnSc7	praxe
je	být	k5eAaImIp3nS	být
nalákat	nalákat	k5eAaPmF	nalákat
žraloky	žralok	k1gMnPc4	žralok
směsí	směs	k1gFnPc2	směs
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
tuku	tuk	k1gInSc2	tuk
a	a	k8xC	a
masa	maso	k1gNnSc2	maso
házené	házená	k1gFnSc2	házená
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
tahání	tahání	k1gNnSc4	tahání
návnady	návnada	k1gFnSc2	návnada
před	před	k7c7	před
klecí	klec	k1gFnSc7	klec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
žraloci	žralok	k1gMnPc1	žralok
pluli	plout	k5eAaImAgMnP	plout
co	co	k3yRnSc1	co
nejblíže	blízce	k6eAd3	blízce
k	k	k7c3	k
turistům	turist	k1gMnPc3	turist
<g/>
.	.	kIx.	.
</s>
<s>
Závažným	závažný	k2eAgInSc7d1	závažný
negativem	negativ	k1gInSc7	negativ
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žraloci	žralok	k1gMnPc1	žralok
si	se	k3xPyFc3	se
zafixují	zafixovat	k5eAaPmIp3nP	zafixovat
asociaci	asociace	k1gFnSc4	asociace
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
rovná	rovnat	k5eAaImIp3nS	rovnat
se	se	k3xPyFc4	se
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
potenciálně	potenciálně	k6eAd1	potenciálně
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
situace	situace	k1gFnPc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
byl	být	k5eAaImAgMnS	být
proto	proto	k8xC	proto
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
turistiky	turistika	k1gFnSc2	turistika
zakázán	zakázán	k2eAgInSc1d1	zakázán
<g/>
.	.	kIx.	.
</s>
<s>
Společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
toto	tento	k3xDgNnSc1	tento
podmořské	podmořský	k2eAgNnSc1d1	podmořské
safari	safari	k1gNnSc1	safari
provozují	provozovat	k5eAaImIp3nP	provozovat
se	s	k7c7	s
brání	bránit	k5eAaImIp3nS	bránit
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
dostatek	dostatek	k1gInSc4	dostatek
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
by	by	kYmCp3nP	by
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
změnu	změna	k1gFnSc4	změna
chování	chování	k1gNnSc2	chování
žraloků	žralok	k1gMnPc2	žralok
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
pravděpodobnější	pravděpodobný	k2eAgMnSc1d2	pravděpodobnější
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověka	člověk	k1gMnSc4	člověk
zabije	zabít	k5eAaPmIp3nS	zabít
blesk	blesk	k1gInSc1	blesk
než	než	k8xS	než
žralok	žralok	k1gMnSc1	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Kompromisem	kompromis	k1gInSc7	kompromis
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
používání	používání	k1gNnSc2	používání
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
pouze	pouze	k6eAd1	pouze
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
žraloci	žralok	k1gMnPc1	žralok
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
každopádně	každopádně	k6eAd1	každopádně
(	(	kIx(	(
<g/>
a	a	k8xC	a
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
pláží	pláž	k1gFnPc2	pláž
a	a	k8xC	a
turistických	turistický	k2eAgNnPc2d1	turistické
středisek	středisko	k1gNnPc2	středisko
<g/>
)	)	kIx)	)
a	a	k8xC	a
následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
nepřikrmovat	přikrmovat	k5eNaImF	přikrmovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
lákadla	lákadlo	k1gNnSc2	lákadlo
s	s	k7c7	s
jídlem	jídlo	k1gNnSc7	jídlo
nespojovali	spojovat	k5eNaImAgMnP	spojovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Neptune	Neptun	k1gInSc5	Neptun
Bay	Bay	k1gMnSc4	Bay
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Austrálii	Austrálie	k1gFnSc6	Austrálie
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
přilákání	přilákání	k1gNnSc4	přilákání
žraloků	žralok	k1gMnPc2	žralok
experimentálně	experimentálně	k6eAd1	experimentálně
pouštěna	pouštěn	k2eAgFnSc1d1	pouštěna
i	i	k8xC	i
rocková	rockový	k2eAgFnSc1d1	rocková
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
především	především	k9	především
od	od	k7c2	od
domácí	domácí	k2eAgFnSc2d1	domácí
legendy	legenda	k1gFnSc2	legenda
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
to	ten	k3xDgNnSc1	ten
fungovalo	fungovat	k5eAaImAgNnS	fungovat
a	a	k8xC	a
žraloky	žralok	k1gMnPc7	žralok
přitahovaly	přitahovat	k5eAaImAgFnP	přitahovat
především	především	k9	především
skladby	skladba	k1gFnPc1	skladba
"	"	kIx"	"
<g/>
Shook	Shook	k1gInSc1	Shook
Me	Me	k1gMnSc1	Me
All	All	k1gMnSc1	All
Night	Night	k1gMnSc1	Night
Long	Long	k1gMnSc1	Long
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Back	Back	k1gInSc1	Back
in	in	k?	in
Black	Black	k1gInSc1	Black
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Žraločí	žraločí	k2eAgFnSc1d1	žraločí
turistika	turistika	k1gFnSc1	turistika
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
sobě	se	k3xPyFc3	se
určitě	určitě	k6eAd1	určitě
i	i	k9	i
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
potenciál	potenciál	k1gInSc4	potenciál
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgInSc1d2	veliký
finanční	finanční	k2eAgInSc1d1	finanční
přínos	přínos	k1gInSc1	přínos
než	než	k8xS	než
rybářství	rybářství	k1gNnSc1	rybářství
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
tohoto	tento	k3xDgMnSc2	tento
predátora	predátor	k1gMnSc2	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Potápění	potápění	k1gNnSc1	potápění
v	v	k7c6	v
klecích	klek	k1gInPc6	klek
vydělá	vydělat	k5eAaPmIp3nS	vydělat
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
9000	[number]	k4	9000
-	-	kIx~	-
27	[number]	k4	27
000	[number]	k4	000
liber	libra	k1gFnPc2	libra
denně	denně	k6eAd1	denně
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bílí	bílý	k2eAgMnPc1d1	bílý
žraloci	žralok	k1gMnPc1	žralok
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
synonymem	synonymum	k1gNnSc7	synonymum
všech	všecek	k3xTgMnPc2	všecek
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
mají	mít	k5eAaImIp3nP	mít
silný	silný	k2eAgInSc4d1	silný
potenciál	potenciál	k1gInSc4	potenciál
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
i	i	k9	i
v	v	k7c6	v
reklamě	reklama	k1gFnSc6	reklama
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
zdůrazňována	zdůrazňovat	k5eAaImNgFnS	zdůrazňovat
buď	buď	k8xC	buď
jejich	jejich	k3xOp3gFnSc4	jejich
žravost	žravost	k1gFnSc4	žravost
a	a	k8xC	a
lačnost	lačnost	k1gFnSc4	lačnost
po	po	k7c6	po
krvi	krev	k1gFnSc6	krev
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
krása	krása	k1gFnSc1	krása
<g/>
,	,	kIx,	,
elegance	elegance	k1gFnSc1	elegance
a	a	k8xC	a
možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
získat	získat	k5eAaPmF	získat
vrcholné	vrcholný	k2eAgInPc4d1	vrcholný
filmové	filmový	k2eAgInPc4d1	filmový
zážitky	zážitek	k1gInPc4	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
moc	moc	k6eAd1	moc
působí	působit	k5eAaImIp3nS	působit
rozvoj	rozvoj	k1gInSc4	rozvoj
rybaření	rybařený	k2eAgMnPc1d1	rybařený
na	na	k7c6	na
populaci	populace	k1gFnSc6	populace
bílého	bílý	k1gMnSc2	bílý
žraloka	žralok	k1gMnSc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
nejsou	být	k5eNaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
žádná	žádný	k3yNgNnPc4	žádný
přesná	přesný	k2eAgNnPc4d1	přesné
čísla	číslo	k1gNnPc4	číslo
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
početnosti	početnost	k1gFnSc6	početnost
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
svaz	svaz	k1gInSc1	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
ho	on	k3xPp3gMnSc4	on
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
jako	jako	k9	jako
zranitelný	zranitelný	k2eAgInSc4d1	zranitelný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
žraloci	žralok	k1gMnPc1	žralok
chyceni	chycen	k2eAgMnPc1d1	chycen
mezi	mezi	k7c7	mezi
nerozením	nerození	k1gNnSc7	nerození
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
26	[number]	k4	26
<g/>
.	.	kIx.	.
rokem	rok	k1gInSc7	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
nestihli	stihnout	k5eNaPmAgMnP	stihnout
rozmnožit	rozmnožit	k5eAaPmF	rozmnožit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
činí	činit	k5eAaImIp3nS	činit
jejich	jejich	k3xOp3gFnSc4	jejich
populaci	populace	k1gFnSc4	populace
obecně	obecně	k6eAd1	obecně
velmi	velmi	k6eAd1	velmi
zranitelnou	zranitelný	k2eAgFnSc4d1	zranitelná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Úmluvě	úmluva	k1gFnSc6	úmluva
o	o	k7c4	o
obchodování	obchodování	k1gNnSc4	obchodování
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
(	(	kIx(	(
<g/>
CITES	CITES	kA	CITES
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgInS	vést
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
II	II	kA	II
(	(	kIx(	(
<g/>
Appendix	Appendix	k1gInSc1	Appendix
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
povolením	povolení	k1gNnSc7	povolení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
zařazen	zařazen	k2eAgInSc1d1	zařazen
do	do	k7c2	do
"	"	kIx"	"
<g/>
Konvence	konvence	k1gFnSc2	konvence
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
migrujících	migrující	k2eAgInPc2d1	migrující
druhů	druh	k1gInPc2	druh
divokých	divoký	k2eAgNnPc2d1	divoké
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Convention	Convention	k1gInSc1	Convention
on	on	k3xPp3gInSc1	on
the	the	k?	the
Conservation	Conservation	k1gInSc1	Conservation
of	of	k?	of
Migratory	Migrator	k1gMnPc4	Migrator
Species	species	k1gFnSc2	species
of	of	k?	of
Wild	Wild	k1gInSc1	Wild
Animals	Animals	k1gInSc1	Animals
<g/>
)	)	kIx)	)
konkrétně	konkrétně	k6eAd1	konkrétně
do	do	k7c2	do
"	"	kIx"	"
<g/>
Memoranda	memorandum	k1gNnPc4	memorandum
o	o	k7c4	o
pochopení	pochopení	k1gNnSc4	pochopení
ochrany	ochrana	k1gFnSc2	ochrana
migrujících	migrující	k2eAgMnPc2d1	migrující
žraloků	žralok	k1gMnPc2	žralok
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Memorandum	memorandum	k1gNnSc4	memorandum
of	of	k?	of
Understanding	Understanding	k1gInSc1	Understanding
on	on	k3xPp3gInSc1	on
the	the	k?	the
Conservation	Conservation	k1gInSc1	Conservation
of	of	k?	of
Migratory	Migrator	k1gInPc1	Migrator
Sharks	Sharks	k1gInSc1	Sharks
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2010	[number]	k4	2010
vyšla	vyjít	k5eAaPmAgFnS	vyjít
studie	studie	k1gFnSc1	studie
Barbary	Barbara	k1gFnSc2	Barbara
Block	Blocka	k1gFnPc2	Blocka
ze	z	k7c2	z
Stanfordovy	Stanfordův	k2eAgFnSc2d1	Stanfordova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
žraločí	žraločí	k2eAgFnSc1d1	žraločí
populace	populace	k1gFnSc1	populace
na	na	k7c4	na
méně	málo	k6eAd2	málo
než	než	k8xS	než
3500	[number]	k4	3500
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
počet	počet	k1gInSc1	počet
s	s	k7c7	s
žralokem	žralok	k1gMnSc7	žralok
tygřím	tygří	k2eAgMnSc7d1	tygří
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
několik	několik	k4yIc1	několik
jiných	jiný	k2eAgNnPc2d1	jiné
studií	studio	k1gNnPc2	studio
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
přišlo	přijít	k5eAaPmAgNnS	přijít
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
2000	[number]	k4	2000
-	-	kIx~	-
2400	[number]	k4	2400
bílých	bílý	k2eAgMnPc2d1	bílý
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
desetkrát	desetkrát	k6eAd1	desetkrát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
číslo	číslo	k1gNnSc1	číslo
udávané	udávaný	k2eAgNnSc1d1	udávané
Barbarou	Barbara	k1gFnSc7	Barbara
Block	Blocka	k1gFnPc2	Blocka
(	(	kIx(	(
<g/>
219	[number]	k4	219
<g/>
)	)	kIx)	)
a	a	k8xC	a
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
na	na	k7c6	na
vzestupu	vzestup	k1gInSc6	vzestup
<g/>
.	.	kIx.	.
</s>
<s>
Rybáři	rybář	k1gMnPc1	rybář
žraloky	žralok	k1gMnPc7	žralok
loví	lovit	k5eAaImIp3nP	lovit
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnPc4	jejich
čelisti	čelist	k1gFnPc4	čelist
<g/>
,	,	kIx,	,
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
ploutve	ploutev	k1gFnPc4	ploutev
či	či	k8xC	či
jako	jako	k9	jako
trofejní	trofejní	k2eAgFnSc4d1	trofejní
rybu	ryba	k1gFnSc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
málokdy	málokdy	k6eAd1	málokdy
je	být	k5eAaImIp3nS	být
objektem	objekt	k1gInSc7	objekt
zájmů	zájem	k1gInPc2	zájem
komerčního	komerční	k2eAgNnSc2d1	komerční
rybářství	rybářství	k1gNnSc2	rybářství
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
hodnotné	hodnotný	k2eAgInPc4d1	hodnotný
<g/>
.	.	kIx.	.
</s>
<s>
Australská	australský	k2eAgFnSc1d1	australská
vláda	vláda	k1gFnSc1	vláda
vede	vést	k5eAaImIp3nS	vést
žraloka	žralok	k1gMnSc4	žralok
bílého	bílý	k1gMnSc4	bílý
jako	jako	k8xC	jako
zranitelný	zranitelný	k2eAgInSc4d1	zranitelný
druh	druh	k1gInSc4	druh
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
státy	stát	k1gInPc1	stát
Australského	australský	k2eAgInSc2d1	australský
svazu	svaz	k1gInSc2	svaz
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
trochu	trochu	k6eAd1	trochu
odlišný	odlišný	k2eAgInSc1d1	odlišný
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
lovu	lov	k1gInSc3	lov
či	či	k8xC	či
ochraně	ochrana	k1gFnSc3	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
zabíjení	zabíjení	k1gNnSc1	zabíjení
žraloků	žralok	k1gMnPc2	žralok
na	na	k7c6	na
vzestupu	vzestup	k1gInSc6	vzestup
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zónu	zóna	k1gFnSc4	zóna
370	[number]	k4	370
km	km	kA	km
okolo	okolo	k7c2	okolo
souostroví	souostroví	k1gNnSc2	souostroví
za	za	k7c4	za
oblast	oblast	k1gFnSc4	oblast
plné	plný	k2eAgFnSc2d1	plná
ochrany	ochrana	k1gFnSc2	ochrana
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
lodě	loď	k1gFnPc4	loď
plující	plující	k2eAgFnPc4d1	plující
pod	pod	k7c7	pod
novozélandskou	novozélandský	k2eAgFnSc7d1	novozélandská
vlajkou	vlajka	k1gFnSc7	vlajka
nemůžou	nemůžou	k?	nemůžou
žraloky	žralok	k1gMnPc4	žralok
bílé	bílý	k2eAgFnPc1d1	bílá
zabíjet	zabíjet	k5eAaImF	zabíjet
ani	ani	k8xC	ani
mimo	mimo	k7c4	mimo
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
nařízení	nařízení	k1gNnSc2	nařízení
hrozí	hrozit	k5eAaImIp3nS	hrozit
pokuta	pokuta	k1gFnSc1	pokuta
250	[number]	k4	250
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
až	až	k9	až
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
vydávají	vydávat	k5eAaPmIp3nP	vydávat
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
nařízení	nařízení	k1gNnPc4	nařízení
k	k	k7c3	k
zacházení	zacházení	k1gNnSc3	zacházení
s	s	k7c7	s
tímto	tento	k3xDgMnSc7	tento
žralokem	žralok	k1gMnSc7	žralok
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
žraloků	žralok	k1gMnPc2	žralok
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Pacifiku	Pacifik	k1gInSc6	Pacifik
tvořená	tvořený	k2eAgFnSc1d1	tvořená
jen	jen	k9	jen
asi	asi	k9	asi
340	[number]	k4	340
jedinci	jedinec	k1gMnPc7	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
geneticky	geneticky	k6eAd1	geneticky
odlišní	odlišný	k2eAgMnPc1d1	odlišný
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
členů	člen	k1gInPc2	člen
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
do	do	k7c2	do
California	Californium	k1gNnSc2	Californium
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Endangered	Endangered	k1gInSc1	Endangered
Species	species	k1gFnSc2	species
Act	Act	k1gFnSc2	Act
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
chytání	chytání	k1gNnSc1	chytání
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
a	a	k8xC	a
potápění	potápění	k1gNnSc1	potápění
za	za	k7c7	za
nimi	on	k3xPp3gFnPc7	on
v	v	k7c6	v
klecích	klec	k1gFnPc6	klec
<g/>
,	,	kIx,	,
lákání	lákání	k1gNnSc6	lákání
na	na	k7c4	na
návnadu	návnada	k1gFnSc4	návnada
a	a	k8xC	a
podobné	podobný	k2eAgFnPc4d1	podobná
aktivity	aktivita	k1gFnPc4	aktivita
jsou	být	k5eAaImIp3nP	být
možné	možný	k2eAgInPc1d1	možný
jen	jen	k9	jen
na	na	k7c4	na
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
povolení	povolení	k1gNnSc4	povolení
<g/>
.	.	kIx.	.
</s>
