<s>
Brno-jih	Brnoih	k1gInSc1	Brno-jih
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
29	[number]	k4	29
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
města	město	k1gNnSc2	město
po	po	k7c6	po
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vlévá	vlévat	k5eAaImIp3nS	vlévat
řeka	řeka	k1gFnSc1	řeka
Svitava	Svitava	k1gFnSc1	Svitava
<g/>
,	,	kIx,	,
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
celá	celý	k2eAgFnSc1d1	celá
katastrální	katastrální	k2eAgFnSc1d1	katastrální
území	území	k1gNnSc4	území
Komárov	Komárov	k1gInSc1	Komárov
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnPc1d1	horní
Heršpice	Heršpice	k1gFnPc1	Heršpice
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgFnPc1d1	dolní
Heršpice	Heršpice	k1gFnPc1	Heršpice
a	a	k8xC	a
Přízřenice	Přízřenice	k1gFnSc1	Přízřenice
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
část	část	k1gFnSc1	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Trnitá	trnitý	k2eAgFnSc1d1	trnitá
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
1277	[number]	k4	1277
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
úřadu	úřad	k1gInSc2	úřad
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Komárově	komárově	k6eAd1	komárově
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
senátních	senátní	k2eAgFnPc2d1	senátní
voleb	volba	k1gFnPc2	volba
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc1	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-jih	Brnoiha	k1gFnPc2	Brno-jiha
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
volebního	volební	k2eAgInSc2d1	volební
obvodu	obvod	k1gInSc2	obvod
číslo	číslo	k1gNnSc1	číslo
58	[number]	k4	58
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Brno-jih	Brnoiha	k1gFnPc2	Brno-jiha
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
městem	město	k1gNnSc7	město
Modřicemi	Modřice	k1gFnPc7	Modřice
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
obcí	obec	k1gFnSc7	obec
Moravany	Moravan	k1gMnPc7	Moravan
a	a	k8xC	a
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Brno-Bohunice	Brno-Bohunice	k1gFnSc2	Brno-Bohunice
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
městskou	městský	k2eAgFnSc7d1	městská
části	část	k1gFnPc1	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
městskými	městský	k2eAgFnPc7d1	městská
částmi	část	k1gFnPc7	část
Brno-Černovice	Brno-Černovice	k1gFnPc4	Brno-Černovice
a	a	k8xC	a
Brno-Tuřany	Brno-Tuřan	k1gMnPc4	Brno-Tuřan
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnPc1d1	původní
části	část	k1gFnPc1	část
Dolních	dolní	k2eAgFnPc2d1	dolní
Heršpic	Heršpice	k1gFnPc2	Heršpice
a	a	k8xC	a
Přízřenic	Přízřenice	k1gFnPc2	Přízřenice
si	se	k3xPyFc3	se
dosud	dosud	k6eAd1	dosud
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
vesnický	vesnický	k2eAgInSc4d1	vesnický
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
představují	představovat	k5eAaImIp3nP	představovat
jedny	jeden	k4xCgInPc1	jeden
z	z	k7c2	z
nejzachovalejších	zachovalý	k2eAgInPc2d3	nejzachovalejší
vesnických	vesnický	k2eAgInPc2d1	vesnický
celků	celek	k1gInPc2	celek
na	na	k7c6	na
území	území	k1gNnSc6	území
moderního	moderní	k2eAgNnSc2d1	moderní
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnPc1d1	horní
Heršpice	Heršpice	k1gFnPc1	Heršpice
<g/>
,	,	kIx,	,
Komárov	Komárov	k1gInSc1	Komárov
a	a	k8xC	a
Trnitá	trnitý	k2eAgNnPc1d1	trnité
mají	mít	k5eAaImIp3nP	mít
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
spíše	spíše	k9	spíše
městský	městský	k2eAgInSc4d1	městský
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
území	území	k1gNnSc2	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
Vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
ulice	ulice	k1gFnSc2	ulice
nachází	nacházet	k5eAaImIp3nS	nacházet
významná	významný	k2eAgFnSc1d1	významná
obchodně	obchodně	k6eAd1	obchodně
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
také	také	k9	také
významná	významný	k2eAgNnPc1d1	významné
obchodní	obchodní	k2eAgNnPc1d1	obchodní
centra	centrum	k1gNnPc1	centrum
Futurum	futurum	k1gNnSc1	futurum
a	a	k8xC	a
Avion	avion	k1gInSc1	avion
Shopping	shopping	k1gInSc1	shopping
Park	park	k1gInSc1	park
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
i	i	k8xC	i
řada	řada	k1gFnSc1	řada
jiných	jiný	k2eAgInPc2d1	jiný
obchodů	obchod	k1gInPc2	obchod
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Europamöbel	Europamöbel	k1gInSc4	Europamöbel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
katastru	katastr	k1gInSc2	katastr
Přízřenic	Přízřenice	k1gFnPc2	Přízřenice
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
původně	původně	k6eAd1	původně
modřicemodřické	modřicemodřický	k2eAgNnSc1d1	modřicemodřický
obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
prochází	procházet	k5eAaImIp3nS	procházet
i	i	k9	i
několik	několik	k4yIc1	několik
důležitých	důležitý	k2eAgFnPc2d1	důležitá
dopravních	dopravní	k2eAgFnPc2d1	dopravní
tepen	tepna	k1gFnPc2	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
dálnice	dálnice	k1gFnPc4	dálnice
D1	D1	k1gFnSc2	D1
a	a	k8xC	a
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
a	a	k8xC	a
rychlostní	rychlostní	k2eAgFnSc4d1	rychlostní
komunikaci	komunikace	k1gFnSc4	komunikace
E	E	kA	E
<g/>
461	[number]	k4	461
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
dálnice	dálnice	k1gFnSc2	dálnice
D1	D1	k1gFnSc2	D1
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
však	však	k9	však
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
plochy	plocha	k1gFnPc1	plocha
nezastavěné	zastavěný	k2eNgFnSc2d1	nezastavěná
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgNnSc1d1	dopravní
spojení	spojení	k1gNnSc1	spojení
se	s	k7c7	s
středem	střed	k1gInSc7	střed
města	město	k1gNnSc2	město
i	i	k8xC	i
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
městskými	městský	k2eAgFnPc7d1	městská
částmi	část	k1gFnPc7	část
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
linek	linka	k1gFnPc2	linka
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
9	[number]	k4	9
a	a	k8xC	a
12	[number]	k4	12
<g/>
,	,	kIx,	,
a	a	k8xC	a
několika	několik	k4yIc2	několik
autobusových	autobusový	k2eAgFnPc2d1	autobusová
a	a	k8xC	a
trolejbusových	trolejbusový	k2eAgFnPc2d1	trolejbusová
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc4	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
připojeno	připojit	k5eAaPmNgNnS	připojit
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
fázích	fáze	k1gFnPc6	fáze
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1850	[number]	k4	1850
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
připojena	připojen	k2eAgNnPc1d1	připojeno
katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
Trnitá	trnitý	k2eAgNnPc1d1	trnité
(	(	kIx(	(
<g/>
její	její	k3xOp3gInSc4	její
katastr	katastr	k1gInSc4	katastr
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
také	také	k9	také
severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
část	část	k1gFnSc4	část
současného	současný	k2eAgInSc2d1	současný
katastru	katastr	k1gInSc2	katastr
Komárova	komárův	k2eAgInSc2d1	komárův
<g/>
)	)	kIx)	)
a	a	k8xC	a
Křenová	křenový	k2eAgFnSc1d1	Křenová
(	(	kIx(	(
<g/>
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
patřily	patřit	k5eAaImAgFnP	patřit
z	z	k7c2	z
území	území	k1gNnSc2	území
současné	současný	k2eAgFnSc2d1	současná
<g />
.	.	kIx.	.
</s>
<s>
městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
Brno-jih	Brnoiha	k1gFnPc2	Brno-jiha
jen	jen	k6eAd1	jen
malé	malý	k2eAgFnPc4d1	malá
části	část	k1gFnPc4	část
dnešních	dnešní	k2eAgFnPc2d1	dnešní
parcel	parcela	k1gFnPc2	parcela
425	[number]	k4	425
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
a	a	k8xC	a
560	[number]	k4	560
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
a	a	k8xC	a
krátký	krátký	k2eAgInSc4d1	krátký
úsek	úsek	k1gInSc4	úsek
Ponávky	Ponávka	k1gFnSc2	Ponávka
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
pozemky	pozemka	k1gFnPc1	pozemka
při	při	k7c6	při
první	první	k4xOgFnSc6	první
katastrální	katastrální	k2eAgFnSc6d1	katastrální
reformě	reforma	k1gFnSc6	reforma
Brna	Brno	k1gNnSc2	Brno
připojeny	připojit	k5eAaPmNgInP	připojit
ke	k	k7c3	k
katastrálnímu	katastrální	k2eAgNnSc3d1	katastrální
území	území	k1gNnSc3	území
Trnitá	trnitý	k2eAgFnSc1d1	trnitá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1919	[number]	k4	1919
<g />
.	.	kIx.	.
</s>
<s>
následovalo	následovat	k5eAaImAgNnS	následovat
připojení	připojení	k1gNnSc1	připojení
obcí	obec	k1gFnSc7	obec
Dolní	dolní	k2eAgFnPc1d1	dolní
Heršpice	Heršpice	k1gFnPc1	Heršpice
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnPc1d1	horní
Heršpice	Heršpice	k1gFnPc1	Heršpice
<g/>
,	,	kIx,	,
Komárova	komárův	k2eAgFnSc1d1	Komárova
<g/>
,	,	kIx,	,
a	a	k8xC	a
Přízřenic	Přízřenice	k1gFnPc2	Přízřenice
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
Černovic	Černovice	k1gFnPc2	Černovice
a	a	k8xC	a
Brněnských	brněnský	k2eAgFnPc2d1	brněnská
Ivanovic	Ivanovice	k1gFnPc2	Ivanovice
(	(	kIx(	(
<g/>
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
původní	původní	k2eAgInPc4d1	původní
katastry	katastr	k1gInPc4	katastr
na	na	k7c6	na
území	území	k1gNnSc6	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
také	také	k6eAd1	také
zasahovaly	zasahovat	k5eAaImAgInP	zasahovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1960	[number]	k4	1960
také	také	k6eAd1	také
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
část	část	k1gFnSc1	část
katastru	katastr	k1gInSc2	katastr
obce	obec	k1gFnSc2	obec
Moravany	Moravan	k1gMnPc4	Moravan
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
Nové	Nové	k2eAgFnSc1d1	Nové
Moravany	Moravan	k1gMnPc7	Moravan
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
začleněno	začlenit	k5eAaPmNgNnS	začlenit
do	do	k7c2	do
katastru	katastr	k1gInSc2	katastr
Horních	horní	k2eAgFnPc2d1	horní
Heršpic	Heršpice	k1gFnPc2	Heršpice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
obec	obec	k1gFnSc1	obec
Holásky	Holásek	k1gMnPc4	Holásek
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
původní	původní	k2eAgInSc4d1	původní
katastr	katastr	k1gInSc4	katastr
sem	sem	k6eAd1	sem
také	také	k9	také
zasahoval	zasahovat	k5eAaImAgMnS	zasahovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
mělo	mít	k5eAaImAgNnS	mít
území	území	k1gNnSc1	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
převážně	převážně	k6eAd1	převážně
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
charakter	charakter	k1gInSc1	charakter
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
zde	zde	k6eAd1	zde
nastal	nastat	k5eAaPmAgInS	nastat
rozvoj	rozvoj	k1gInSc4	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
negativní	negativní	k2eAgInSc1d1	negativní
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
se	se	k3xPyFc4	se
i	i	k9	i
území	území	k1gNnSc1	území
této	tento	k3xDgFnSc2	tento
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
dotkla	dotknout	k5eAaPmAgFnS	dotknout
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
katastrální	katastrální	k2eAgFnSc1d1	katastrální
reforma	reforma	k1gFnSc1	reforma
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
například	například	k6eAd1	například
patřila	patřit	k5eAaImAgFnS	patřit
nejvýchodnější	východní	k2eAgFnSc1d3	nejvýchodnější
nezastavěná	zastavěný	k2eNgFnSc1d1	nezastavěná
část	část	k1gFnSc1	část
katastru	katastr	k1gInSc2	katastr
Komárova	komárův	k2eAgFnSc1d1	Komárova
k	k	k7c3	k
historické	historický	k2eAgFnSc3d1	historická
obci	obec	k1gFnSc3	obec
Černovice	Černovice	k1gFnSc2	Černovice
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
část	část	k1gFnSc1	část
komárovského	komárovský	k2eAgInSc2d1	komárovský
katastru	katastr	k1gInSc2	katastr
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
i	i	k9	i
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Svitavy	Svitava	k1gFnSc2	Svitava
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
součást	součást	k1gFnSc4	součást
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
Černovice	Černovice	k1gFnSc2	Černovice
a	a	k8xC	a
Brněnské	brněnský	k2eAgFnPc4d1	brněnská
Ivanovice	Ivanovice	k1gFnPc4	Ivanovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1976	[number]	k4	1976
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-jih	Brnoiha	k1gFnPc2	Brno-jiha
začleněno	začlenit	k5eAaPmNgNnS	začlenit
do	do	k7c2	do
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
městského	městský	k2eAgInSc2d1	městský
obvodu	obvod	k1gInSc2	obvod
Brno	Brno	k1gNnSc4	Brno
IV	IV	kA	IV
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
novodobé	novodobý	k2eAgFnSc2d1	novodobá
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-jih	Brnoiha	k1gFnPc2	Brno-jiha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zde	zde	k6eAd1	zde
také	také	k9	také
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
postupné	postupný	k2eAgFnSc3d1	postupná
obnově	obnova	k1gFnSc3	obnova
a	a	k8xC	a
zlepšování	zlepšování	k1gNnSc3	zlepšování
dopravní	dopravní	k2eAgFnSc2d1	dopravní
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
a	a	k8xC	a
budování	budování	k1gNnSc4	budování
sportovišť	sportoviště	k1gNnPc2	sportoviště
<g/>
,	,	kIx,	,
a	a	k8xC	a
opravám	oprava	k1gFnPc3	oprava
školních	školní	k2eAgFnPc2d1	školní
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
se	se	k3xPyFc4	se
v	v	k7c6	v
Dolních	dolní	k2eAgFnPc6d1	dolní
Heršpicích	Heršpice	k1gFnPc6	Heršpice
a	a	k8xC	a
Přízřenicích	Přízřenice	k1gFnPc6	Přízřenice
konalo	konat	k5eAaImAgNnS	konat
místní	místní	k2eAgNnSc1d1	místní
referendum	referendum	k1gNnSc1	referendum
o	o	k7c6	o
oddělení	oddělení	k1gNnSc6	oddělení
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc2	vytvoření
nové	nový	k2eAgFnSc2d1	nová
obce	obec	k1gFnSc2	obec
Dolní	dolní	k2eAgFnSc2d1	dolní
Heršpice-Přízřenice	Heršpice-Přízřenice	k1gFnSc2	Heršpice-Přízřenice
<g/>
,	,	kIx,	,
o	o	k7c6	o
těchto	tento	k3xDgFnPc6	tento
snahách	snaha	k1gFnPc6	snaha
podrobněji	podrobně	k6eAd2	podrobně
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
článek	článek	k1gInSc1	článek
Dolní	dolní	k2eAgFnSc2d1	dolní
Heršpice-Přízřenice	Heršpice-Přízřenice	k1gFnSc2	Heršpice-Přízřenice
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Brno-jih	Brnoiha	k1gFnPc2	Brno-jiha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgInSc4d1	oficiální
web	web	k1gInSc4	web
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Drobná	drobný	k2eAgFnSc1d1	drobná
sakrální	sakrální	k2eAgFnSc1d1	sakrální
architektura	architektura	k1gFnSc1	architektura
v	v	k7c6	v
MČ	MČ	kA	MČ
Brno-jih	Brnoih	k1gMnSc1	Brno-jih
</s>
