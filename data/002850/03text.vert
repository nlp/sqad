<s>
Indy	Indus	k1gInPc1	Indus
&	&	k?	&
Wich	Wicha	k1gFnPc2	Wicha
je	být	k5eAaImIp3nS	být
pražská	pražský	k2eAgFnSc1d1	Pražská
hip	hip	k0	hip
hopová	hopový	k2eAgFnSc1d1	hopová
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
rapper	rapper	k1gInSc4	rapper
Indy	Indus	k1gInPc4	Indus
a	a	k8xC	a
DJ	DJ	kA	DJ
Wich	Wich	k1gInSc1	Wich
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
také	také	k9	také
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
s	s	k7c7	s
rapperem	rapper	k1gInSc7	rapper
La	la	k1gNnPc2	la
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
PSH	PSH	kA	PSH
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
základním	základní	k2eAgFnPc3d1	základní
kapelám	kapela	k1gFnPc3	kapela
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
nového	nový	k2eAgNnSc2d1	nové
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
formovaly	formovat	k5eAaImAgInP	formovat
podobu	podoba	k1gFnSc4	podoba
českého	český	k2eAgMnSc2d1	český
hip	hip	k0	hip
hopu	hopu	k6eAd1	hopu
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
domácí	domácí	k2eAgMnPc4d1	domácí
hiphopové	hiphopový	k2eAgMnPc4d1	hiphopový
umělce	umělec	k1gMnPc4	umělec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
úspěšně	úspěšně	k6eAd1	úspěšně
debutovali	debutovat	k5eAaBmAgMnP	debutovat
deskou	deska	k1gFnSc7	deska
My	my	k3xPp1nPc1	my
3	[number]	k4	3
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dodnes	dodnes	k6eAd1	dodnes
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
domácí	domácí	k2eAgFnPc4d1	domácí
hiphopové	hiphopový	k2eAgFnPc4d1	hiphopová
desky	deska	k1gFnPc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
Hádej	hádat	k5eAaImRp2nS	hádat
kdo	kdo	k3yInSc1	kdo
<g/>
...	...	k?	...
vydané	vydaný	k2eAgFnSc2d1	vydaná
opět	opět	k6eAd1	opět
na	na	k7c6	na
vlastním	vlastní	k2eAgInSc6d1	vlastní
labelu	label	k1gInSc6	label
Mad	Mad	k1gFnSc4	Mad
Drum	Drumo	k1gNnPc2	Drumo
Records	Recordsa	k1gFnPc2	Recordsa
bylo	být	k5eAaImAgNnS	být
nominováno	nominovat	k5eAaBmNgNnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Anděl	Anděla	k1gFnPc2	Anděla
2006	[number]	k4	2006
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Hip	hip	k0	hip
Hop	hop	k0	hop
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Filter	Filter	k1gInSc1	Filter
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Hádej	hádat	k5eAaImRp2nS	hádat
kdo	kdo	k3yQnSc1	kdo
<g/>
...	...	k?	...
deskou	deska	k1gFnSc7	deska
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
má	mít	k5eAaImIp3nS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
stovky	stovka	k1gFnPc4	stovka
koncertů	koncert	k1gInPc2	koncert
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
ČR	ČR	kA	ČR
a	a	k8xC	a
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2005	[number]	k4	2005
také	také	k6eAd1	také
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
koncert	koncert	k1gInSc4	koncert
odehráli	odehrát	k5eAaPmAgMnP	odehrát
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1998	[number]	k4	1998
ve	v	k7c6	v
strahovském	strahovský	k2eAgInSc6d1	strahovský
klubu	klub	k1gInSc6	klub
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
přichází	přicházet	k5eAaImIp3nS	přicházet
druhý	druhý	k4xOgInSc4	druhý
rapper	rapper	k1gInSc4	rapper
LA	la	k1gNnSc4	la
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
postava	postava	k1gFnSc1	postava
graffiti	graffiti	k1gNnSc2	graffiti
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
rappeři	rapper	k1gMnPc1	rapper
Indy	Indus	k1gInPc4	Indus
a	a	k8xC	a
LA4	LA4	k1gFnPc4	LA4
s	s	k7c7	s
DJ	DJ	kA	DJ
Wichem	Wich	k1gInSc7	Wich
výrazně	výrazně	k6eAd1	výrazně
upozornili	upozornit	k5eAaPmAgMnP	upozornit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
již	již	k6eAd1	již
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
ikonou	ikona	k1gFnSc7	ikona
pražské	pražský	k2eAgFnSc2d1	Pražská
scény	scéna	k1gFnSc2	scéna
Penery	Penera	k1gFnSc2	Penera
Strýčka	strýček	k1gMnSc2	strýček
Homeboye	Homeboy	k1gMnSc2	Homeboy
natočili	natočit	k5eAaBmAgMnP	natočit
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Hlasuju	hlasovat	k5eAaImIp1nS	hlasovat
proti	proti	k7c3	proti
<g/>
/	/	kIx~	/
<g/>
Cesta	cesta	k1gFnSc1	cesta
štrýtu	štrýt	k1gInSc2	štrýt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
debutové	debutový	k2eAgFnSc6d1	debutová
desce	deska	k1gFnSc6	deska
PSH	PSH	kA	PSH
-	-	kIx~	-
Repertoár	repertoár	k1gInSc1	repertoár
<g/>
.	.	kIx.	.
</s>
<s>
Wich	Wich	k1gMnSc1	Wich
se	se	k3xPyFc4	se
také	také	k6eAd1	také
ujal	ujmout	k5eAaPmAgMnS	ujmout
produkce	produkce	k1gFnSc2	produkce
celé	celý	k2eAgFnSc2d1	celá
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
její	její	k3xOp3gInSc4	její
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
mu	on	k3xPp3gMnSc3	on
vynesl	vynést	k5eAaPmAgMnS	vynést
cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
Anděl	Anděl	k1gMnSc1	Anděl
za	za	k7c7	za
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Dance	Danka	k1gFnSc6	Danka
a	a	k8xC	a
Hip	hip	k0	hip
hop	hop	k0	hop
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
deskách	deska	k1gFnPc6	deska
Teritorium	teritorium	k1gNnSc1	teritorium
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
rappera	rappera	k1gFnSc1	rappera
Oriona	Oriona	k1gFnSc1	Oriona
<g/>
.	.	kIx.	.
</s>
<s>
Indy	Indus	k1gInPc1	Indus
&	&	k?	&
Wich	Wich	k1gInSc1	Wich
sami	sám	k3xTgMnPc1	sám
debutovali	debutovat	k5eAaBmAgMnP	debutovat
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
albem	album	k1gNnSc7	album
My	my	k3xPp1nPc1	my
3	[number]	k4	3
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c6	na
vlastní	vlastní	k2eAgFnSc6d1	vlastní
značce	značka	k1gFnSc6	značka
Mad	Mad	k1gMnSc1	Mad
Drum	Druma	k1gFnPc2	Druma
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
velké	velký	k2eAgNnSc1d1	velké
turné	turné	k1gNnSc1	turné
v	v	k7c6	v
klubech	klub	k1gInPc6	klub
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
letních	letní	k2eAgInPc6d1	letní
festivalech	festival	k1gInPc6	festival
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jedni	jeden	k4xCgMnPc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
českých	český	k2eAgMnPc2d1	český
rapperů	rapper	k1gMnPc2	rapper
pronikají	pronikat	k5eAaImIp3nP	pronikat
i	i	k9	i
na	na	k7c4	na
sousední	sousední	k2eAgNnSc4d1	sousední
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
vydali	vydat	k5eAaPmAgMnP	vydat
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Ještě	ještě	k9	ještě
Pořád	pořád	k6eAd1	pořád
<g/>
/	/	kIx~	/
<g/>
Originál	originál	k1gMnSc1	originál
Pilsner	Pilsner	k1gMnSc1	Pilsner
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
už	už	k9	už
byl	být	k5eAaImAgInS	být
rapper	rapper	k1gInSc1	rapper
LA4	LA4	k1gFnSc2	LA4
na	na	k7c6	na
sólové	sólový	k2eAgFnSc6d1	sólová
dráze	dráha	k1gFnSc6	dráha
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
s	s	k7c7	s
Indy	Indus	k1gInPc7	Indus
&	&	k?	&
Wich	Wich	k1gInSc1	Wich
jezdí	jezdit	k5eAaImIp3nS	jezdit
jen	jen	k9	jen
v	v	k7c6	v
roli	role	k1gFnSc6	role
hosta	host	k1gMnSc2	host
<g/>
.	.	kIx.	.
</s>
<s>
Wich	Wich	k1gInSc1	Wich
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vydal	vydat	k5eAaPmAgMnS	vydat
sólovku	sólovka	k1gFnSc4	sólovka
Time	Tim	k1gMnSc2	Tim
Is	Is	k1gMnSc2	Is
Now	Now	k1gMnSc2	Now
<g/>
,	,	kIx,	,
za	za	k7c7	za
kterou	který	k3yRgFnSc7	který
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
Anděl	Anděla	k1gFnPc2	Anděla
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
i	i	k8xC	i
písně	píseň	k1gFnPc4	píseň
Indyho	Indy	k1gMnSc2	Indy
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
1000	[number]	k4	1000
MCs	MCs	k1gFnPc2	MCs
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
LA4	LA4	k1gFnSc1	LA4
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2006	[number]	k4	2006
Indy	Indus	k1gInPc7	Indus
&	&	k?	&
Wich	Wich	k1gInSc4	Wich
vydávají	vydávat	k5eAaImIp3nP	vydávat
druhé	druhý	k4xOgFnPc4	druhý
album	album	k1gNnSc4	album
Hádej	hádat	k5eAaImRp2nS	hádat
kdo	kdo	k3yRnSc1	kdo
<g/>
...	...	k?	...
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
opět	opět	k6eAd1	opět
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
labelu	label	k1gInSc6	label
<g/>
,	,	kIx,	,
se	s	k7c7	s
záštitou	záštita	k1gFnSc7	záštita
EMI	EMI	kA	EMI
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
fanoušků	fanoušek	k1gMnPc2	fanoušek
je	být	k5eAaImIp3nS	být
značný	značný	k2eAgInSc1d1	značný
<g/>
,	,	kIx,	,
kapela	kapela	k1gFnSc1	kapela
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
album	album	k1gNnSc4	album
nominována	nominován	k2eAgFnSc1d1	nominována
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Anděl	Anděla	k1gFnPc2	Anděla
2006	[number]	k4	2006
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Hip	hip	k0	hip
hop	hop	k0	hop
<g/>
.	.	kIx.	.
</s>
<s>
Nominaci	nominace	k1gFnSc4	nominace
však	však	k9	však
neproměnila	proměnit	k5eNaPmAgFnS	proměnit
<g/>
.	.	kIx.	.
</s>
<s>
Satisfakce	satisfakce	k1gFnSc2	satisfakce
se	se	k3xPyFc4	se
dočkali	dočkat	k5eAaPmAgMnP	dočkat
alespoň	alespoň	k9	alespoň
od	od	k7c2	od
časopisu	časopis	k1gInSc2	časopis
Filter	Filter	k1gInSc1	Filter
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Hádej	hádat	k5eAaImRp2nS	hádat
kdo	kdo	k3yQnSc1	kdo
<g/>
...	...	k?	...
deskou	deska	k1gFnSc7	deska
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
vydali	vydat	k5eAaPmAgMnP	vydat
DVD	DVD	kA	DVD
Kids	Kids	k1gInSc4	Kids
On	on	k3xPp3gMnSc1	on
The	The	k1gMnSc1	The
Click	Click	k1gMnSc1	Click
Tour	Tour	k1gInSc4	Tour
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc2	první
české	český	k2eAgFnSc2d1	Česká
hiphopové	hiphopový	k2eAgFnSc2d1	hiphopová
koncertní	koncertní	k2eAgFnSc2d1	koncertní
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc1	dokument
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Michala	Michal	k1gMnSc2	Michal
Dvořáka	Dvořák	k1gMnSc2	Dvořák
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
kapelu	kapela	k1gFnSc4	kapela
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
turné	turné	k1gNnSc6	turné
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
také	také	k9	také
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
velkých	velký	k2eAgInPc6d1	velký
letních	letní	k2eAgInPc6d1	letní
festivalech	festival	k1gInPc6	festival
-	-	kIx~	-
Hip	hip	k0	hip
Hop	hop	k0	hop
Kempu	kemp	k1gInSc6	kemp
<g/>
,	,	kIx,	,
Hip	hip	k0	hip
Hop	hop	k0	hop
Jamu	jam	k1gInSc2	jam
a	a	k8xC	a
Hip	hip	k0	hip
Hop	hop	k0	hop
Allstars	Allstarsa	k1gFnPc2	Allstarsa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cenách	cena	k1gFnPc6	cena
Óčka	Óčk	k1gInSc2	Óčk
2008	[number]	k4	2008
byli	být	k5eAaImAgMnP	být
nominováni	nominován	k2eAgMnPc1d1	nominován
na	na	k7c4	na
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
hiphopové	hiphopový	k2eAgMnPc4d1	hiphopový
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
v	v	k7c6	v
ostré	ostrý	k2eAgFnSc6d1	ostrá
konkurenci	konkurence	k1gFnSc6	konkurence
Kanye	Kany	k1gMnSc2	Kany
Westa	West	k1gMnSc2	West
<g/>
,	,	kIx,	,
Rihanny	Rihanna	k1gMnSc2	Rihanna
a	a	k8xC	a
Timbalanda	Timbalanda	k1gFnSc1	Timbalanda
se	se	k3xPyFc4	se
však	však	k9	však
neprosadili	prosadit	k5eNaPmAgMnP	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
hudebního	hudební	k2eAgInSc2d1	hudební
časopisu	časopis	k1gInSc2	časopis
REPORT	report	k1gInSc1	report
si	se	k3xPyFc3	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
cen	cena	k1gFnPc2	cena
Žebřík	žebřík	k1gInSc4	žebřík
2008	[number]	k4	2008
vysloužili	vysloužit	k5eAaPmAgMnP	vysloužit
dvě	dva	k4xCgFnPc4	dva
nominace	nominace	k1gFnPc4	nominace
-	-	kIx~	-
Hudební	hudební	k2eAgFnSc2d1	hudební
DVD	DVD	kA	DVD
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
Kids	Kids	k1gInSc4	Kids
On	on	k3xPp3gMnSc1	on
The	The	k1gMnSc1	The
Click	Click	k1gMnSc1	Click
Tour	Tour	k1gMnSc1	Tour
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
a	a	k8xC	a
DJ	DJ	kA	DJ
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
DJ	DJ	kA	DJ
Wich	Wich	k1gInSc1	Wich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
v	v	k7c6	v
plzeňském	plzeňský	k2eAgInSc6d1	plzeňský
Domě	dům	k1gInSc6	dům
kultury	kultura	k1gFnSc2	kultura
Inwest	Inwest	k1gFnSc1	Inwest
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
-	-	kIx~	-
Pohyb	pohyb	k1gInSc1	pohyb
Nehybnýho	Nehybnýho	k?	Nehybnýho
2002	[number]	k4	2002
-	-	kIx~	-
My	my	k3xPp1nPc1	my
3	[number]	k4	3
2006	[number]	k4	2006
-	-	kIx~	-
Hádej	hádat	k5eAaImRp2nS	hádat
kdo	kdo	k3yRnSc1	kdo
<g/>
...	...	k?	...
2004	[number]	k4	2004
-	-	kIx~	-
Time	Tim	k1gInSc2	Tim
is	is	k?	is
now	now	k?	now
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
Wich	Wich	k1gInSc1	Wich
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
-	-	kIx~	-
The	The	k1gMnSc1	The
Chronicles	Chronicles	k1gMnSc1	Chronicles
of	of	k?	of
Nomad	Nomad	k1gInSc1	Nomad
-	-	kIx~	-
DJ	DJ	kA	DJ
Wich	Wich	k1gInSc4	Wich
presents	presentsa	k1gFnPc2	presentsa
Nironic	Nironice	k1gFnPc2	Nironice
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
Wich	Wich	k1gInSc1	Wich
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
-	-	kIx~	-
Golden	Goldna	k1gFnPc2	Goldna
Touch	Touch	k1gInSc1	Touch
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
Wich	Wich	k1gInSc1	Wich
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
-	-	kIx~	-
Human	Human	k1gMnSc1	Human
Writes	Writes	k1gMnSc1	Writes
-	-	kIx~	-
Hi-Def	Hi-Def	k1gMnSc1	Hi-Def
&	&	k?	&
DJ	DJ	kA	DJ
Wich	Wich	k1gInSc1	Wich
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
Wich	Wich	k1gInSc1	Wich
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
Al	ala	k1gFnPc2	ala
Capone	Capon	k1gInSc5	Capon
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Vault	Vaulta	k1gFnPc2	Vaulta
-	-	kIx~	-
Rasco	Rasco	k1gNnSc1	Rasco
&	&	k?	&
DJ	DJ	kA	DJ
Wich	Wich	k1gInSc1	Wich
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
Wich	Wich	k1gInSc1	Wich
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
2011	[number]	k4	2011
-	-	kIx~	-
Nomad	Nomad	k1gInSc1	Nomad
2	[number]	k4	2
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Long	Long	k1gMnSc1	Long
Way	Way	k1gMnSc1	Way
Home	Home	k1gFnSc1	Home
<g/>
)	)	kIx)	)
-	-	kIx~	-
DJ	DJ	kA	DJ
Wich	Wich	k1gInSc1	Wich
&	&	k?	&
Nironic	Nironice	k1gFnPc2	Nironice
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
Wich	Wich	k1gInSc1	Wich
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
KMBL	KMBL	kA	KMBL
-	-	kIx~	-
Indy	Ind	k1gMnPc4	Ind
&	&	k?	&
Rae	Rae	k1gFnSc1	Rae
(	(	kIx(	(
<g/>
Clou	clou	k1gNnSc1	clou
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
Indy	Indus	k1gInPc1	Indus
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
-	-	kIx~	-
From	From	k1gInSc1	From
Amsterdam	Amsterdam	k1gInSc4	Amsterdam
To	to	k9	to
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Nejbr	Nejbr	k1gInSc1	Nejbr
HipHop	hiphop	k1gInSc1	hiphop
mix	mix	k1gInSc1	mix
1	[number]	k4	1
2006	[number]	k4	2006
-	-	kIx~	-
Nejbr	Nejbr	k1gInSc1	Nejbr
HipHop	hiphop	k1gInSc1	hiphop
mix	mix	k1gInSc1	mix
2	[number]	k4	2
2007	[number]	k4	2007
-	-	kIx~	-
Nejbr	Nejbr	k1gInSc1	Nejbr
HipHop	hiphop	k1gInSc1	hiphop
mix	mix	k1gInSc1	mix
3	[number]	k4	3
2008	[number]	k4	2008
-	-	kIx~	-
20	[number]	k4	20
<g/>
ers	ers	k?	ers
2007	[number]	k4	2007
-	-	kIx~	-
Kids	Kids	k1gInSc4	Kids
On	on	k3xPp3gMnSc1	on
The	The	k1gMnSc1	The
Click	Click	k1gMnSc1	Click
Tour	Tour	k1gMnSc1	Tour
2007	[number]	k4	2007
2008	[number]	k4	2008
-	-	kIx~	-
Making	Making	k1gInSc1	Making
of	of	k?	of
20	[number]	k4	20
<g/>
ers	ers	k?	ers
2008	[number]	k4	2008
-	-	kIx~	-
Česká	český	k2eAgFnSc1d1	Česká
RAPublika	RAPublika	k1gFnSc1	RAPublika
2000	[number]	k4	2000
-	-	kIx~	-
"	"	kIx"	"
<g/>
Léto	léto	k1gNnSc1	léto
<g/>
/	/	kIx~	/
<g/>
East	East	k2eAgInSc1d1	East
Side	Side	k1gInSc1	Side
Unia	Unium	k1gNnSc2	Unium
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
"	"	kIx"	"
2001	[number]	k4	2001
-	-	kIx~	-
"	"	kIx"	"
<g/>
Hlasuju	hlasovat	k5eAaImIp1nS	hlasovat
proti	proti	k7c3	proti
<g/>
/	/	kIx~	/
<g/>
Cesta	cesta	k1gFnSc1	cesta
štrýtu	štrýt	k1gInSc2	štrýt
<g/>
"	"	kIx"	"
2003	[number]	k4	2003
-	-	kIx~	-
"	"	kIx"	"
<g/>
Ještě	ještě	k9	ještě
Pořád	pořád	k6eAd1	pořád
<g/>
/	/	kIx~	/
<g/>
Originál	originál	k1gMnSc1	originál
Pilsner	Pilsner	k1gMnSc1	Pilsner
<g/>
"	"	kIx"	"
2005	[number]	k4	2005
-	-	kIx~	-
"	"	kIx"	"
<g/>
Velrybí	velrybí	k2eAgNnSc4d1	velrybí
hovězí	hovězí	k1gNnSc4	hovězí
<g/>
"	"	kIx"	"
*	*	kIx~	*
<g/>
nebylo	být	k5eNaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
</s>
