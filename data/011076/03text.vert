<p>
<s>
Amplituda	amplituda	k1gFnSc1	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
komplexní	komplexní	k2eAgNnSc4d1	komplexní
číslo	číslo	k1gNnSc4	číslo
přiřazené	přiřazený	k2eAgFnSc2d1	přiřazená
neurčitému	určitý	k2eNgMnSc3d1	neurčitý
nebo	nebo	k8xC	nebo
neznámému	známý	k2eNgInSc3d1	neznámý
procesu	proces	k1gInSc3	proces
nebo	nebo	k8xC	nebo
veličině	veličina	k1gFnSc3	veličina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
daný	daný	k2eAgInSc1d1	daný
proces	proces	k1gInSc1	proces
nastane	nastat	k5eAaPmIp3nS	nastat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
udána	udán	k2eAgFnSc1d1	udána
jako	jako	k8xS	jako
kvadrát	kvadrát	k1gInSc1	kvadrát
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
mocnina	mocnina	k1gFnSc1	mocnina
<g/>
)	)	kIx)	)
absolutní	absolutní	k2eAgFnSc2d1	absolutní
hodnoty	hodnota	k1gFnSc2	hodnota
amplitudy	amplituda	k1gFnSc2	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Amplitudu	amplituda	k1gFnSc4	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
nelze	lze	k6eNd1	lze
přímo	přímo	k6eAd1	přímo
měřit	měřit	k5eAaImF	měřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Amplituda	amplituda	k1gFnSc1	amplituda
a	a	k8xC	a
hustota	hustota	k1gFnSc1	hustota
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
kvantovému	kvantový	k2eAgInSc3d1	kvantový
stavu	stav	k1gInSc3	stav
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
funkcí	funkce	k1gFnSc7	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ψ	ψ	k?	ψ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
}	}	kIx)	}
</s>
</p>
<p>
<s>
lze	lze	k6eAd1	lze
přiřadit	přiřadit	k5eAaPmF	přiřadit
hustotu	hustota	k1gFnSc4	hustota
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
jako	jako	k8xS	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ψ	ψ	k?	ψ
</s>
</p>
<p>
</p>
<p>
<s>
⋆	⋆	k?	⋆
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ψ	ψ	k?	ψ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
star	star	k1gFnSc1	star
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
<s>
ψ	ψ	k?	ψ
</s>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
veličina	veličina	k1gFnSc1	veličina
se	se	k3xPyFc4	se
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
fyzice	fyzika	k1gFnSc6	fyzika
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
hustota	hustota	k1gFnSc1	hustota
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
amplituda	amplituda	k1gFnSc1	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ψ	ψ	k?	ψ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
hustota	hustota	k1gFnSc1	hustota
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
<s>
ψ	ψ	k?	ψ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
ψ	ψ	k?	ψ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
∗	∗	k?	∗
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ψ	ψ	k?	ψ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
*	*	kIx~	*
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Amplituda	amplituda	k1gFnSc1	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
pro	pro	k7c4	pro
polohu	poloha	k1gFnSc4	poloha
===	===	k?	===
</s>
</p>
<p>
<s>
Amplitudou	amplituda	k1gFnSc7	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
pro	pro	k7c4	pro
polohu	poloha	k1gFnSc4	poloha
nějaké	nějaký	k3yIgFnSc2	nějaký
částice	částice	k1gFnSc2	částice
je	být	k5eAaImIp3nS	být
komplexní	komplexní	k2eAgFnSc1d1	komplexní
funkce	funkce	k1gFnSc1	funkce
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gFnPc7	jejíž
hodnotami	hodnota	k1gFnPc7	hodnota
jsou	být	k5eAaImIp3nP	být
komplexní	komplexní	k2eAgNnPc4d1	komplexní
čísla	číslo	k1gNnPc4	číslo
<g/>
)	)	kIx)	)
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
označujeme	označovat	k5eAaImIp1nP	označovat
ji	on	k3xPp3gFnSc4	on
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hustotu	hustota	k1gFnSc4	hustota
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ρ	ρ	k?	ρ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varrho	varr	k1gMnSc2	varr
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
výskytu	výskyt	k1gInSc3	výskyt
zkoumané	zkoumaný	k2eAgFnSc2d1	zkoumaná
částice	částice	k1gFnSc2	částice
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
bodě	bod	k1gInSc6	bod
prostoru	prostor	k1gInSc2	prostor
se	s	k7c7	s
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Platí	platit	k5eAaImIp3nS	platit
tedy	tedy	k9	tedy
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ρ	ρ	k?	ρ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
</p>
<p>
<s>
∗	∗	k?	∗
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varrho	varr	k1gMnSc2	varr
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
*	*	kIx~	*
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
hvězdičkou	hvězdička	k1gFnSc7	hvězdička
je	být	k5eAaImIp3nS	být
označeno	označit	k5eAaPmNgNnS	označit
komplexně	komplexně	k6eAd1	komplexně
sdružené	sdružený	k2eAgNnSc1d1	sdružené
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
funkci	funkce	k1gFnSc4	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc1	název
vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
alternativně	alternativně	k6eAd1	alternativně
tedy	tedy	k9	tedy
vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
ψ	ψ	k?	ψ
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
každé	každý	k3xTgFnSc6	každý
možné	možný	k2eAgFnSc6d1	možná
poloze	poloha	k1gFnSc6	poloha
částice	částice	k1gFnSc2	částice
v	v	k7c4	v
prostoru	prostora	k1gFnSc4	prostora
hustotu	hustota	k1gFnSc4	hustota
amplitudy	amplituda	k1gFnSc2	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zkoumaná	zkoumaný	k2eAgFnSc1d1	zkoumaná
částice	částice	k1gFnSc1	částice
se	se	k3xPyFc4	se
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
musí	muset	k5eAaImIp3nS	muset
nacházet	nacházet	k5eAaImF	nacházet
<g/>
,	,	kIx,	,
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
prostor	prostor	k1gInSc4	prostor
rozdělíme	rozdělit	k5eAaPmIp1nP	rozdělit
na	na	k7c6	na
malé	malý	k2eAgFnSc6d1	malá
části	část	k1gFnSc6	část
a	a	k8xC	a
sčítáme	sčítat	k5eAaImIp1nP	sčítat
pravděpodobnosti	pravděpodobnost	k1gFnPc4	pravděpodobnost
nalezení	nalezení	k1gNnSc2	nalezení
částice	částice	k1gFnSc2	částice
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
malých	malý	k2eAgFnPc2d1	malá
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
suma	suma	k1gFnSc1	suma
těchto	tento	k3xDgFnPc2	tento
pravděpodobností	pravděpodobnost	k1gFnPc2	pravděpodobnost
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
rovna	roven	k2eAgFnSc1d1	rovna
1	[number]	k4	1
(	(	kIx(	(
<g/>
jistou	jistý	k2eAgFnSc4d1	jistá
událost	událost	k1gFnSc4	událost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvedenému	uvedený	k2eAgInSc3d1	uvedený
součtu	součet	k1gInSc3	součet
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
integrál	integrál	k1gInSc1	integrál
přes	přes	k7c4	přes
celý	celý	k2eAgInSc4d1	celý
prostor	prostor	k1gInSc4	prostor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Omega	omega	k1gNnPc3	omega
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rovný	rovný	k2eAgInSc4d1	rovný
jedné	jeden	k4xCgFnSc2	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
tedy	tedy	k9	tedy
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
normovací	normovací	k2eAgFnSc1d1	normovací
podmínka	podmínka	k1gFnSc1	podmínka
pro	pro	k7c4	pro
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
funkci	funkce	k1gFnSc4	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Amplituda	amplituda	k1gFnSc1	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
a	a	k8xC	a
princip	princip	k1gInSc1	princip
superpozice	superpozice	k1gFnSc2	superpozice
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
principu	princip	k1gInSc2	princip
superpozice	superpozice	k1gFnSc2	superpozice
se	se	k3xPyFc4	se
kvantově-mechanická	kvantověechanický	k2eAgFnSc1d1	kvantově-mechanická
soustava	soustava	k1gFnSc1	soustava
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
elektron	elektron	k1gInSc1	elektron
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
nacházet	nacházet	k5eAaImF	nacházet
v	v	k7c6	v
superpozici	superpozice	k1gFnSc6	superpozice
dvou	dva	k4xCgFnPc2	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
superpozici	superpozice	k1gFnSc4	superpozice
stavů	stav	k1gInPc2	stav
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
potom	potom	k6eAd1	potom
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
=	=	kIx~	=
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
komplexní	komplexní	k2eAgFnPc1d1	komplexní
konstanty	konstanta	k1gFnPc1	konstanta
a	a	k8xC	a
nazýváme	nazývat	k5eAaImIp1nP	nazývat
je	on	k3xPp3gFnPc4	on
amplitudy	amplituda	k1gFnPc4	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgFnPc1	druhý
mocniny	mocnina	k1gFnPc1	mocnina
jejich	jejich	k3xOp3gFnPc2	jejich
absolutních	absolutní	k2eAgFnPc2d1	absolutní
hodnot	hodnota	k1gFnPc2	hodnota
mají	mít	k5eAaImIp3nP	mít
opět	opět	k6eAd1	opět
význam	význam	k1gInSc4	význam
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
|	|	kIx~	|
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
nalezení	nalezení	k1gNnSc2	nalezení
soustavy	soustava	k1gFnSc2	soustava
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
|	|	kIx~	|
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
nalezení	nalezení	k1gNnSc2	nalezení
soustavy	soustava	k1gFnSc2	soustava
v	v	k7c6	v
stavu	stav	k1gInSc6	stav
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
superpozici	superpozice	k1gFnSc6	superpozice
stavů	stav	k1gInPc2	stav
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ψ	Ψ	k?	Ψ
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
jiném	jiný	k2eAgInSc6d1	jiný
stavu	stav	k1gInSc6	stav
ho	on	k3xPp3gNnSc4	on
najít	najít	k5eAaPmF	najít
nemůžeme	moct	k5eNaImIp1nP	moct
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
součet	součet	k1gInSc1	součet
obou	dva	k4xCgFnPc2	dva
pravděpodobností	pravděpodobnost	k1gFnPc2	pravděpodobnost
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
roven	roven	k2eAgInSc1d1	roven
jedné	jeden	k4xCgFnSc2	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnPc1	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
libovolná	libovolný	k2eAgNnPc1d1	libovolné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
svázána	svázat	k5eAaPmNgFnS	svázat
normovací	normovací	k2eAgFnSc7d1	normovací
podmínkou	podmínka	k1gFnSc7	podmínka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
|	|	kIx~	|
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
|	|	kIx~	|
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Amplituda	amplituda	k1gFnSc1	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
procesu	proces	k1gInSc2	proces
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Feynmanova	Feynmanův	k2eAgInSc2d1	Feynmanův
přístupu	přístup	k1gInSc2	přístup
ke	k	k7c3	k
kvantové	kvantový	k2eAgFnSc3d1	kvantová
mechanice	mechanika	k1gFnSc3	mechanika
lze	lze	k6eAd1	lze
přiřadit	přiřadit	k5eAaPmF	přiřadit
amplitudy	amplituda	k1gFnPc4	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
jednotlivým	jednotlivý	k2eAgInPc3d1	jednotlivý
procesům	proces	k1gInPc3	proces
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
experimentu	experiment	k1gInSc6	experiment
nastat	nastat	k5eAaPmF	nastat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
může	moct	k5eAaImIp3nS	moct
daný	daný	k2eAgInSc4d1	daný
proces	proces	k1gInSc4	proces
nastat	nastat	k5eAaPmF	nastat
více	hodně	k6eAd2	hodně
způsoby	způsob	k1gInPc4	způsob
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každý	každý	k3xTgInSc1	každý
způsob	způsob	k1gInSc1	způsob
má	mít	k5eAaImIp3nS	mít
přiřazenu	přiřazen	k2eAgFnSc4d1	přiřazena
amplitudu	amplituda	k1gFnSc4	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
principu	princip	k1gInSc6	princip
nemůžeme	moct	k5eNaImIp1nP	moct
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
ze	z	k7c2	z
způsobů	způsob	k1gInPc2	způsob
proces	proces	k1gInSc4	proces
nastal	nastat	k5eAaPmAgInS	nastat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
amplituda	amplituda	k1gFnSc1	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
procesu	proces	k1gInSc2	proces
dána	dát	k5eAaPmNgFnS	dát
jako	jako	k8xC	jako
součet	součet	k1gInSc1	součet
amplitud	amplituda	k1gFnPc2	amplituda
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
způsobů	způsob	k1gInPc2	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsme	být	k5eAaImIp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
způsoby	způsob	k1gInPc4	způsob
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
procesu	proces	k1gInSc2	proces
je	být	k5eAaImIp3nS	být
součtem	součet	k1gInSc7	součet
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
způsobů	způsob	k1gInPc2	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
dvouštěrbinovém	dvouštěrbinový	k2eAgInSc6d1	dvouštěrbinový
experimentu	experiment	k1gInSc6	experiment
pokud	pokud	k8xS	pokud
neumíme	umět	k5eNaImIp1nP	umět
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
štěrbinou	štěrbina	k1gFnSc7	štěrbina
částice	částice	k1gFnSc2	částice
proletěla	proletět	k5eAaPmAgFnS	proletět
<g/>
,	,	kIx,	,
sčítáme	sčítat	k5eAaImIp1nP	sčítat
amplitudy	amplituda	k1gFnPc1	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
průletu	průlet	k1gInSc2	průlet
každou	každý	k3xTgFnSc7	každý
štěrbinou	štěrbina	k1gFnSc7	štěrbina
a	a	k8xC	a
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
interferenci	interference	k1gFnSc4	interference
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rozlišit	rozlišit	k5eAaPmF	rozlišit
umíme	umět	k5eAaImIp1nP	umět
<g/>
,	,	kIx,	,
sčítáme	sčítat	k5eAaImIp1nP	sčítat
pravděpodobnosti	pravděpodobnost	k1gFnPc1	pravděpodobnost
průletu	průlet	k1gInSc2	průlet
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
štěrbinami	štěrbina	k1gFnPc7	štěrbina
a	a	k8xC	a
interferenci	interference	k1gFnSc4	interference
nepozorujeme	pozorovat	k5eNaImIp1nP	pozorovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
představuje	představovat	k5eAaImIp3nS	představovat
amplitudu	amplituda	k1gFnSc4	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
čtverec	čtverec	k1gInSc4	čtverec
hustotu	hustota	k1gFnSc4	hustota
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Maxe	Max	k1gMnSc2	Max
Borna	Born	k1gMnSc2	Born
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
oceněn	ocenit	k5eAaPmNgInS	ocenit
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Amplituda	amplituda	k1gFnSc1	amplituda
</s>
</p>
<p>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
</s>
</p>
<p>
<s>
Vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
</s>
</p>
<p>
<s>
Hustota	hustota	k1gFnSc1	hustota
toku	tok	k1gInSc2	tok
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Amplitúda	Amplitúdo	k1gNnSc2	Amplitúdo
pravdepodobnosti	pravdepodobnost	k1gFnSc2	pravdepodobnost
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
