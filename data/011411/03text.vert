<p>
<s>
Parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
je	být	k5eAaImIp3nS	být
pístový	pístový	k2eAgInSc1d1	pístový
tepelný	tepelný	k2eAgInSc1d1	tepelný
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
spalovací	spalovací	k2eAgInSc1d1	spalovací
motor	motor	k1gInSc1	motor
s	s	k7c7	s
vnějším	vnější	k2eAgNnSc7d1	vnější
spalováním	spalování	k1gNnSc7	spalování
<g/>
,	,	kIx,	,
přeměňující	přeměňující	k2eAgInPc1d1	přeměňující
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
energii	energie	k1gFnSc4	energie
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
rotační	rotační	k2eAgInSc4d1	rotační
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
mechanický	mechanický	k2eAgInSc1d1	mechanický
stroj	stroj	k1gInSc1	stroj
poháněný	poháněný	k2eAgInSc1d1	poháněný
parou	para	k1gFnSc7	para
sestrojil	sestrojit	k5eAaPmAgInS	sestrojit
Hérón	Hérón	k1gInSc1	Hérón
Alexandrijský	alexandrijský	k2eAgInSc1d1	alexandrijský
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Považoval	považovat	k5eAaImAgMnS	považovat
jej	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
jen	jen	k9	jen
za	za	k7c4	za
hračku	hračka	k1gFnSc4	hračka
a	a	k8xC	a
na	na	k7c4	na
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
vynález	vynález	k1gInSc4	vynález
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
obvykle	obvykle	k6eAd1	obvykle
připisován	připisovat	k5eAaImNgInS	připisovat
skotskému	skotský	k1gInSc3	skotský
vynálezci	vynálezce	k1gMnPc1	vynálezce
Jamesi	Jamese	k1gFnSc4	Jamese
Wattovi	Watt	k1gMnSc3	Watt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1765	[number]	k4	1765
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Watt	watt	k1gInSc1	watt
"	"	kIx"	"
<g/>
pouze	pouze	k6eAd1	pouze
<g/>
"	"	kIx"	"
významně	významně	k6eAd1	významně
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
stroje	stroj	k1gInSc2	stroj
Thomase	Thomas	k1gMnSc2	Thomas
Saveryho	Savery	k1gMnSc2	Savery
a	a	k8xC	a
Thomase	Thomas	k1gMnSc2	Thomas
Newcomena	Newcomen	k2eAgFnSc1d1	Newcomena
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
stroje	stroj	k1gInPc4	stroj
na	na	k7c6	na
principu	princip	k1gInSc6	princip
kondenzace	kondenzace	k1gFnSc2	kondenzace
syté	sytý	k2eAgFnSc2d1	sytá
páry	pára	k1gFnSc2	pára
ve	v	k7c6	v
válci	válec	k1gInSc6	válec
a	a	k8xC	a
využití	využití	k1gNnSc1	využití
síly	síla	k1gFnSc2	síla
vyvolané	vyvolaný	k2eAgFnSc2d1	vyvolaná
podtlakem	podtlak	k1gInSc7	podtlak
k	k	k7c3	k
čerpání	čerpání	k1gNnSc3	čerpání
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
atmosferického	atmosferický	k2eAgInSc2d1	atmosferický
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
Thomase	Thomas	k1gMnSc2	Thomas
Newcomena	Newcomen	k2eAgFnSc1d1	Newcomena
byl	být	k5eAaImAgInS	být
prostor	prostor	k1gInSc4	prostor
pod	pod	k7c7	pod
pístem	píst	k1gInSc7	píst
naplněn	naplnit	k5eAaPmNgInS	naplnit
párou	pára	k1gFnSc7	pára
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
píst	píst	k1gInSc4	píst
zvedla	zvednout	k5eAaPmAgFnS	zvednout
do	do	k7c2	do
horní	horní	k2eAgFnSc2d1	horní
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
pod	pod	k7c4	pod
píst	píst	k1gInSc4	píst
vstříkla	vstříknout	k5eAaPmAgFnS	vstříknout
studená	studený	k2eAgFnSc1d1	studená
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
kondenzaci	kondenzace	k1gFnSc4	kondenzace
páry	pára	k1gFnSc2	pára
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podtlak	podtlak	k1gInSc1	podtlak
<g/>
.	.	kIx.	.
</s>
<s>
Píst	píst	k1gInSc1	píst
byl	být	k5eAaImAgInS	být
tlačen	tlačit	k5eAaImNgInS	tlačit
atmosferickým	atmosferický	k2eAgInSc7d1	atmosferický
tlakem	tlak	k1gInSc7	tlak
dolů	dol	k1gInPc2	dol
a	a	k8xC	a
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
táhla	táhlo	k1gNnSc2	táhlo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
bylo	být	k5eAaImAgNnS	být
pevně	pevně	k6eAd1	pevně
spojeno	spojit	k5eAaPmNgNnS	spojit
<g/>
,	,	kIx,	,
prováděl	provádět	k5eAaImAgInS	provádět
práci	práce	k1gFnSc4	práce
kývavým	kývavý	k2eAgInSc7d1	kývavý
pohybem	pohyb	k1gInSc7	pohyb
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
dolů	dolů	k6eAd1	dolů
jako	jako	k9	jako
zahradní	zahradní	k2eAgFnSc1d1	zahradní
pumpa	pumpa	k1gFnSc1	pumpa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1722-1724	[number]	k4	1722-1724
postavil	postavit	k5eAaPmAgInS	postavit
Newcomenův	Newcomenův	k2eAgInSc1d1	Newcomenův
atmosferický	atmosferický	k2eAgInSc1d1	atmosferický
parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
-	-	kIx~	-
první	první	k4xOgFnSc1	první
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentě	kontinent	k1gInSc6	kontinent
-	-	kIx~	-
anglický	anglický	k2eAgMnSc1d1	anglický
mechanik	mechanik	k1gMnSc1	mechanik
Issak	Issak	k1gMnSc1	Issak
Potter	Potter	k1gMnSc1	Potter
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Bani	baně	k1gFnSc6	baně
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1736	[number]	k4	1736
získal	získat	k5eAaPmAgMnS	získat
Jonathan	Jonathan	k1gMnSc1	Jonathan
Hull	Hull	k1gMnSc1	Hull
či	či	k8xC	či
Hulls	Hulls	k1gInSc1	Hulls
první	první	k4xOgFnSc6	první
patent	patent	k1gInSc1	patent
na	na	k7c4	na
kolesovou	kolesový	k2eAgFnSc4d1	Kolesová
loď	loď	k1gFnSc4	loď
poháněnou	poháněný	k2eAgFnSc4d1	poháněná
Newcomenovým	Newcomenův	k2eAgInSc7d1	Newcomenův
atmosferickým	atmosferický	k2eAgInSc7d1	atmosferický
parním	parní	k2eAgInSc7d1	parní
strojem	stroj	k1gInSc7	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
Wattovým	wattový	k2eAgInSc7d1	wattový
zlepšením	zlepšení	k1gNnSc7	zlepšení
bylo	být	k5eAaImAgNnS	být
oddělení	oddělení	k1gNnSc1	oddělení
kondenzace	kondenzace	k1gFnSc2	kondenzace
do	do	k7c2	do
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
prostoru	prostor	k1gInSc2	prostor
mimo	mimo	k7c4	mimo
válec	válec	k1gInSc4	válec
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	s	k7c7	s
stěny	stěn	k1gInPc7	stěn
válce	válka	k1gFnSc6	válka
neochlazovaly	ochlazovat	k5eNaImAgFnP	ochlazovat
a	a	k8xC	a
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
se	se	k3xPyFc4	se
účinnost	účinnost	k1gFnSc1	účinnost
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
Watt	watt	k1gInSc1	watt
nechal	nechat	k5eAaPmAgInS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
řadu	řada	k1gFnSc4	řada
vynálezů	vynález	k1gInPc2	vynález
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
dále	daleko	k6eAd2	daleko
využívány	využívat	k5eAaPmNgInP	využívat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
éru	éra	k1gFnSc4	éra
parních	parní	k2eAgInPc2d1	parní
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Dvojčinný	dvojčinný	k2eAgInSc1d1	dvojčinný
parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
pára	pára	k1gFnSc1	pára
vháněla	vhánět	k5eAaImAgFnS	vhánět
střídavě	střídavě	k6eAd1	střídavě
pod	pod	k7c4	pod
píst	píst	k1gInSc4	píst
a	a	k8xC	a
nad	nad	k7c4	nad
píst	píst	k1gInSc4	píst
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
zpětný	zpětný	k2eAgInSc1d1	zpětný
pohyb	pohyb	k1gInSc1	pohyb
pístu	píst	k1gInSc2	píst
byl	být	k5eAaImAgInS	být
</s>
</p>
<p>
<s>
poháněn	poháněn	k2eAgInSc1d1	poháněn
tlakem	tlak	k1gInSc7	tlak
páry	pára	k1gFnSc2	pára
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
podstatně	podstatně	k6eAd1	podstatně
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
výkon	výkon	k1gInSc4	výkon
i	i	k8xC	i
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
stroj	stroj	k1gInSc1	stroj
mohl	moct	k5eAaImAgInS	moct
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
párou	pára	k1gFnSc7	pára
o	o	k7c6	o
vyšším	vysoký	k2eAgInSc6d2	vyšší
než	než	k8xS	než
atmosférickém	atmosférický	k2eAgInSc6d1	atmosférický
tlaku	tlak	k1gInSc6	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
vynálezů	vynález	k1gInPc2	vynález
jmenujme	jmenovat	k5eAaImRp1nP	jmenovat
převod	převod	k1gInSc4	převod
přímočarého	přímočarý	k2eAgInSc2d1	přímočarý
pohybu	pohyb	k1gInSc2	pohyb
pístu	píst	k1gInSc2	píst
na	na	k7c4	na
otáčivý	otáčivý	k2eAgInSc4d1	otáčivý
pomocí	pomocí	k7c2	pomocí
klikového	klikový	k2eAgInSc2d1	klikový
mechanismu	mechanismus	k1gInSc2	mechanismus
nebo	nebo	k8xC	nebo
Wattův	Wattův	k2eAgInSc4d1	Wattův
odstředivý	odstředivý	k2eAgInSc4d1	odstředivý
regulátor	regulátor	k1gInSc4	regulátor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
udržoval	udržovat	k5eAaImAgInS	udržovat
stálé	stálý	k2eAgFnPc4d1	stálá
otáčky	otáčka	k1gFnPc4	otáčka
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
stal	stát	k5eAaPmAgInS	stát
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
jak	jak	k8xS	jak
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
tomuto	tento	k3xDgMnSc3	tento
století	století	k1gNnSc1	století
také	také	k9	také
říká	říkat	k5eAaImIp3nS	říkat
století	století	k1gNnSc4	století
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Dopravě	doprava	k1gFnSc3	doprava
kralovaly	kralovat	k5eAaImAgInP	kralovat
vlaky	vlak	k1gInPc1	vlak
tažené	tažený	k2eAgInPc1d1	tažený
parními	parní	k2eAgFnPc7d1	parní
lokomotivami	lokomotiva	k1gFnPc7	lokomotiva
<g/>
,	,	kIx,	,
vody	voda	k1gFnPc1	voda
brázdily	brázdit	k5eAaImAgFnP	brázdit
parníky	parník	k1gInPc4	parník
<g/>
,	,	kIx,	,
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
podniky	podnik	k1gInPc4	podnik
měly	mít	k5eAaImAgInP	mít
stroje	stroj	k1gInPc1	stroj
poháněné	poháněný	k2eAgInPc1d1	poháněný
transmisemi	transmise	k1gFnPc7	transmise
od	od	k7c2	od
centrálního	centrální	k2eAgInSc2d1	centrální
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
parní	parní	k2eAgFnPc1d1	parní
oračky	oračka	k1gFnPc1	oračka
<g/>
,	,	kIx,	,
parní	parní	k2eAgFnPc1d1	parní
mlátičky	mlátička	k1gFnPc1	mlátička
a	a	k8xC	a
parní	parní	k2eAgFnPc1d1	parní
lokomobily	lokomobila	k1gFnPc1	lokomobila
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
silnic	silnice	k1gFnPc2	silnice
pak	pak	k6eAd1	pak
upravovaly	upravovat	k5eAaImAgFnP	upravovat
parní	parní	k2eAgFnPc1d1	parní
válcovačky	válcovačka	k1gFnPc1	válcovačka
resp.	resp.	kA	resp.
parní	parní	k2eAgInPc1d1	parní
válce	válec	k1gInPc4	válec
<g/>
.	.	kIx.	.
</s>
</p>
<s>
První	první	k4xOgInSc1
parní	parní	k2eAgInSc1d1
stroj	stroj	k1gInSc1
a	a	k8xC
parní	parní	k2eAgInSc1d1
automobil	automobil	k1gInSc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
vznikl	vzniknout	k5eAaPmAgInS
na	na	k7c6
počátku	počátek	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ho	on	k3xPp3gMnSc4
sestrojil	sestrojit	k5eAaPmAgMnS
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
Gerstner	Gerstner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<p>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
význam	význam	k1gInSc4	význam
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
postupně	postupně	k6eAd1	postupně
upadal	upadat	k5eAaPmAgInS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dopravy	doprava	k1gFnSc2	doprava
byl	být	k5eAaImAgInS	být
vytlačen	vytlačit	k5eAaPmNgInS	vytlačit
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
s	s	k7c7	s
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
spalováním	spalování	k1gNnSc7	spalování
a	a	k8xC	a
z	z	k7c2	z
průmyslu	průmysl	k1gInSc2	průmysl
elektrickými	elektrický	k2eAgInPc7d1	elektrický
stroji	stroj	k1gInPc7	stroj
a	a	k8xC	a
parní	parní	k2eAgFnSc7d1	parní
turbínou	turbína	k1gFnSc7	turbína
<g/>
.	.	kIx.	.
</s>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
sloužily	sloužit	k5eAaImAgInP	sloužit
těžní	těžní	k2eAgInPc1d1	těžní
parní	parní	k2eAgInPc1d1	parní
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
dolech	dol	k1gInPc6	dol
vydržely	vydržet	k5eAaPmAgFnP	vydržet
až	až	k6eAd1	až
do	do	k7c2	do
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Muzeum	muzeum	k1gNnSc4	muzeum
Krásno	krásno	k1gNnSc1	krásno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
s	s	k7c7	s
provozem	provoz	k1gInSc7	provoz
parních	parní	k2eAgInPc2d1	parní
strojů	stroj	k1gInPc2	stroj
setkat	setkat	k5eAaPmF	setkat
především	především	k9	především
u	u	k7c2	u
nostalgických	nostalgický	k2eAgFnPc2d1	nostalgická
jízd	jízda	k1gFnPc2	jízda
parních	parní	k2eAgFnPc2d1	parní
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
také	také	k9	také
používaly	používat	k5eAaImAgInP	používat
speciální	speciální	k2eAgInPc1d1	speciální
stroje	stroj	k1gInPc1	stroj
podobné	podobný	k2eAgInPc1d1	podobný
či	či	k8xC	či
konstrukčně	konstrukčně	k6eAd1	konstrukčně
příbuzné	příbuzná	k1gFnSc2	příbuzná
parnímu	parní	k2eAgInSc3d1	parní
stroji	stroj	k1gInSc3	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
využívána	využívat	k5eAaPmNgFnS	využívat
přímočará	přímočarý	k2eAgFnSc1d1	přímočará
hnací	hnací	k2eAgFnSc1d1	hnací
síla	síla	k1gFnSc1	síla
přenášená	přenášený	k2eAgFnSc1d1	přenášená
na	na	k7c4	na
pracovní	pracovní	k2eAgInSc4d1	pracovní
nástroj	nástroj	k1gInSc4	nástroj
z	z	k7c2	z
pístu	píst	k1gInSc2	píst
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
parní	parní	k2eAgNnPc4d1	parní
kladiva	kladivo	k1gNnPc4	kladivo
<g/>
,	,	kIx,	,
parní	parní	k2eAgInPc4d1	parní
buchary	buchar	k1gInPc4	buchar
či	či	k8xC	či
parní	parní	k2eAgInPc4d1	parní
lisy	lis	k1gInPc4	lis
a	a	k8xC	a
podobná	podobný	k2eAgNnPc4d1	podobné
strojní	strojní	k2eAgNnPc4d1	strojní
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
jinými	jiný	k2eAgInPc7d1	jiný
technickými	technický	k2eAgInPc7d1	technický
principy	princip	k1gInPc7	princip
či	či	k8xC	či
jinými	jiný	k2eAgInPc7d1	jiný
technologickými	technologický	k2eAgInPc7d1	technologický
postupy	postup	k1gInPc7	postup
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
letadlových	letadlový	k2eAgFnPc6d1	letadlová
lodích	loď	k1gFnPc6	loď
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
využívá	využívat	k5eAaImIp3nS	využívat
parní	parní	k2eAgInSc1d1	parní
katapult	katapult	k1gInSc1	katapult
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
speciální	speciální	k2eAgInSc1d1	speciální
parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
s	s	k7c7	s
několika	několik	k4yIc7	několik
desítkami	desítka	k1gFnPc7	desítka
metrů	metr	k1gInPc2	metr
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
válcem	válec	k1gInSc7	válec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Účinnost	účinnost	k1gFnSc4	účinnost
==	==	k?	==
</s>
</p>
<p>
<s>
Proti	proti	k7c3	proti
současným	současný	k2eAgInPc3d1	současný
tepelným	tepelný	k2eAgInPc3d1	tepelný
strojům	stroj	k1gInPc3	stroj
má	mít	k5eAaImIp3nS	mít
parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
nízkou	nízký	k2eAgFnSc4d1	nízká
účinnost	účinnost	k1gFnSc4	účinnost
přeměny	přeměna	k1gFnSc2	přeměna
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
maximálně	maximálně	k6eAd1	maximálně
30	[number]	k4	30
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kotlem	kotel	k1gInSc7	kotel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
účinnost	účinnost	k1gFnSc4	účinnost
okolo	okolo	k7c2	okolo
50	[number]	k4	50
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
výsledná	výsledný	k2eAgFnSc1d1	výsledná
účinnost	účinnost	k1gFnSc1	účinnost
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
stroje	stroj	k1gInSc2	stroj
a	a	k8xC	a
kotle	kotel	k1gInSc2	kotel
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
5	[number]	k4	5
%	%	kIx~	%
–	–	k?	–
15	[number]	k4	15
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
spalovacím	spalovací	k2eAgInPc3d1	spalovací
motorům	motor	k1gInPc3	motor
s	s	k7c7	s
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
spalováním	spalování	k1gNnSc7	spalování
s	s	k7c7	s
běžně	běžně	k6eAd1	běžně
dosahovanou	dosahovaný	k2eAgFnSc7d1	dosahovaná
účinností	účinnost	k1gFnSc7	účinnost
okolo	okolo	k7c2	okolo
35	[number]	k4	35
%	%	kIx~	%
nedostačující	dostačující	k2eNgFnSc2d1	nedostačující
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dostatek	dostatek	k1gInSc1	dostatek
levného	levný	k2eAgNnSc2d1	levné
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ale	ale	k9	ale
může	moct	k5eAaImIp3nS	moct
ekonomicky	ekonomicky	k6eAd1	ekonomicky
užívat	užívat	k5eAaImF	užívat
i	i	k9	i
dnes	dnes	k6eAd1	dnes
-	-	kIx~	-
proti	proti	k7c3	proti
jiným	jiný	k2eAgInPc3d1	jiný
typům	typ	k1gInPc3	typ
strojů	stroj	k1gInPc2	stroj
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
spolehlivostí	spolehlivost	k1gFnSc7	spolehlivost
a	a	k8xC	a
schopností	schopnost	k1gFnSc7	schopnost
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
pracovních	pracovní	k2eAgInPc6d1	pracovní
režimech	režim	k1gInPc6	režim
(	(	kIx(	(
<g/>
různé	různý	k2eAgFnPc4d1	různá
otáčky	otáčka	k1gFnPc4	otáčka
i	i	k8xC	i
výkony	výkon	k1gInPc1	výkon
<g/>
,	,	kIx,	,
snadná	snadný	k2eAgFnSc1d1	snadná
reverzace	reverzace	k1gFnSc1	reverzace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nespornou	sporný	k2eNgFnSc7d1	nesporná
výhodou	výhoda	k1gFnSc7	výhoda
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
nevadí	vadit	k5eNaImIp3nS	vadit
přetížení	přetížení	k1gNnSc4	přetížení
<g/>
,	,	kIx,	,
a	a	k8xC	a
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
poškození	poškození	k1gNnSc3	poškození
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
když	když	k8xS	když
se	se	k3xPyFc4	se
přetížením	přetížení	k1gNnSc7	přetížení
zcela	zcela	k6eAd1	zcela
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
práce	práce	k1gFnSc2	práce
==	==	k?	==
</s>
</p>
<p>
<s>
Pára	pára	k1gFnSc1	pára
z	z	k7c2	z
kotle	kotel	k1gInSc2	kotel
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
regulátor	regulátor	k1gInSc4	regulátor
vedena	vést	k5eAaImNgFnS	vést
do	do	k7c2	do
ústrojí	ústrojí	k1gNnSc2	ústrojí
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
rozvodu	rozvod	k1gInSc2	rozvod
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
šoupátkové	šoupátkový	k2eAgFnSc2d1	šoupátková
komory	komora	k1gFnSc2	komora
<g/>
)	)	kIx)	)
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
je	být	k5eAaImIp3nS	být
rozdělována	rozdělovat	k5eAaImNgFnS	rozdělovat
do	do	k7c2	do
válce	válec	k1gInSc2	válec
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
svým	svůj	k3xOyFgInSc7	svůj
tlakem	tlak	k1gInSc7	tlak
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
pohyb	pohyb	k1gInSc1	pohyb
pístu	píst	k1gInSc2	píst
<g/>
.	.	kIx.	.
</s>
<s>
Použitá	použitý	k2eAgFnSc1d1	použitá
pára	pára	k1gFnSc1	pára
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
přes	přes	k7c4	přes
šoupátkovou	šoupátkový	k2eAgFnSc4d1	šoupátková
komoru	komora	k1gFnSc4	komora
vypouštěna	vypouštěn	k2eAgMnSc4d1	vypouštěn
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Posuvný	posuvný	k2eAgInSc1d1	posuvný
pohyb	pohyb	k1gInSc1	pohyb
pístu	píst	k1gInSc2	píst
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
pístní	pístní	k2eAgFnSc4d1	pístní
tyč	tyč	k1gFnSc4	tyč
<g/>
,	,	kIx,	,
křižák	křižák	k1gInSc4	křižák
a	a	k8xC	a
ojnici	ojnice	k1gFnSc4	ojnice
přenášen	přenášet	k5eAaImNgInS	přenášet
na	na	k7c6	na
kliku	klik	k1gInSc6	klik
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
posuvný	posuvný	k2eAgInSc4d1	posuvný
pohyb	pohyb	k1gInSc4	pohyb
převádí	převádět	k5eAaImIp3nS	převádět
na	na	k7c4	na
otáčivý	otáčivý	k2eAgInSc4d1	otáčivý
(	(	kIx(	(
<g/>
rotační	rotační	k2eAgInSc4d1	rotační
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Část	část	k1gFnSc1	část
výkonu	výkon	k1gInSc2	výkon
stroje	stroj	k1gInSc2	stroj
je	být	k5eAaImIp3nS	být
odebírána	odebírat	k5eAaImNgFnS	odebírat
ústrojím	ústrojí	k1gNnSc7	ústrojí
vnějšího	vnější	k2eAgInSc2d1	vnější
rozvodu	rozvod	k1gInSc2	rozvod
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
nucený	nucený	k2eAgInSc1d1	nucený
pohyb	pohyb	k1gInSc1	pohyb
částí	část	k1gFnPc2	část
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
rozvodu	rozvod	k1gInSc2	rozvod
(	(	kIx(	(
<g/>
šoupátka	šoupátko	k1gNnSc2	šoupátko
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
ventilů	ventil	k1gInPc2	ventil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
rozběh	rozběh	k1gInSc4	rozběh
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nastavit	nastavit	k5eAaPmF	nastavit
stupeň	stupeň	k1gInSc4	stupeň
plnění	plnění	k1gNnSc2	plnění
válce	válec	k1gInSc2	válec
a	a	k8xC	a
také	také	k9	také
změnu	změna	k1gFnSc4	změna
směru	směr	k1gInSc2	směr
otáčení	otáčení	k1gNnSc2	otáčení
(	(	kIx(	(
<g/>
reverzaci	reverzace	k1gFnSc4	reverzace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
požadována	požadovat	k5eAaImNgFnS	požadovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Animované	animovaný	k2eAgNnSc1d1	animované
schéma	schéma	k1gNnSc1	schéma
práce	práce	k1gFnSc2	práce
stroje	stroj	k1gInSc2	stroj
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lokomotivní	lokomotivní	k2eAgInSc1d1	lokomotivní
parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reverzace	reverzace	k1gFnSc2	reverzace
===	===	k?	===
</s>
</p>
<p>
<s>
Reverzace	reverzace	k1gFnSc1	reverzace
chodu	chod	k1gInSc2	chod
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
za	za	k7c2	za
klidu	klid	k1gInSc2	klid
stroje	stroj	k1gInSc2	stroj
přestavením	přestavení	k1gNnSc7	přestavení
rozvodu	rozvod	k1gInSc2	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Stroj	stroj	k1gInSc1	stroj
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
rozběhne	rozběhnout	k5eAaPmIp3nS	rozběhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
pára	pára	k1gFnSc1	pára
je	být	k5eAaImIp3nS	být
přiváděna	přivádět	k5eAaImNgFnS	přivádět
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
pístu	píst	k1gInSc2	píst
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byla	být	k5eAaImAgFnS	být
předtím	předtím	k6eAd1	předtím
vytlačována	vytlačovat	k5eAaImNgFnS	vytlačovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Animované	animovaný	k2eAgNnSc1d1	animované
schéma	schéma	k1gNnSc1	schéma
reverzace	reverzace	k1gFnSc2	reverzace
stroje	stroj	k1gInSc2	stroj
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hesle	heslo	k1gNnSc6	heslo
Lokomotivní	lokomotivní	k2eAgInSc1d1	lokomotivní
parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
pV	pV	k?	pV
diagram	diagram	k1gInSc1	diagram
==	==	k?	==
</s>
</p>
<p>
<s>
pV	pV	k?	pV
diagram	diagram	k1gInSc1	diagram
popisuje	popisovat	k5eAaImIp3nS	popisovat
práci	práce	k1gFnSc4	práce
tepelného	tepelný	k2eAgInSc2d1	tepelný
stroje	stroj	k1gInSc2	stroj
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jednoho	jeden	k4xCgInSc2	jeden
pracovního	pracovní	k2eAgInSc2d1	pracovní
cyklu	cyklus	k1gInSc2	cyklus
(	(	kIx(	(
<g/>
vizte	vidět	k5eAaImRp2nP	vidět
také	také	k9	také
Carnotův	Carnotův	k2eAgInSc1d1	Carnotův
cyklus	cyklus	k1gInSc1	cyklus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skutečný	skutečný	k2eAgInSc4d1	skutečný
tvar	tvar	k1gInSc4	tvar
tohoto	tento	k3xDgInSc2	tento
diagramu	diagram	k1gInSc2	diagram
<g/>
,	,	kIx,	,
malovaný	malovaný	k2eAgInSc1d1	malovaný
speciálním	speciální	k2eAgInSc7d1	speciální
přístrojem	přístroj	k1gInSc7	přístroj
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
indikátorový	indikátorový	k2eAgInSc1d1	indikátorový
diagram	diagram	k1gInSc1	diagram
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Plnotlaké	Plnotlaký	k2eAgInPc4d1	Plnotlaký
a	a	k8xC	a
expanzní	expanzní	k2eAgInPc4d1	expanzní
stroje	stroj	k1gInPc4	stroj
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pára	pára	k1gFnSc1	pára
přiváděna	přiváděn	k2eAgFnSc1d1	přiváděna
do	do	k7c2	do
válce	válec	k1gInSc2	válec
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
pohybu	pohyb	k1gInSc2	pohyb
pístu	píst	k1gInSc2	píst
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
plnotlakém	plnotlaký	k2eAgInSc6d1	plnotlaký
parním	parní	k2eAgInSc6d1	parní
stroji	stroj	k1gInSc6	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
byly	být	k5eAaImAgInP	být
především	především	k6eAd1	především
první	první	k4xOgInPc1	první
parní	parní	k2eAgInPc1d1	parní
stroje	stroj	k1gInPc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pára	pára	k1gFnSc1	pára
vpouštěna	vpouštěn	k2eAgFnSc1d1	vpouštěna
jen	jen	k9	jen
po	po	k7c4	po
část	část	k1gFnSc4	část
pohybu	pohyb	k1gInSc2	pohyb
pístu	píst	k1gInSc2	píst
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
expanzních	expanzní	k2eAgInPc6d1	expanzní
strojích	stroj	k1gInPc6	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
mezi	mezi	k7c7	mezi
celkovým	celkový	k2eAgInSc7d1	celkový
objemem	objem	k1gInSc7	objem
válce	válec	k1gInSc2	válec
a	a	k8xC	a
objemem	objem	k1gInSc7	objem
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
uzavření	uzavření	k1gNnSc2	uzavření
přívodu	přívod	k1gInSc2	přívod
páry	pára	k1gFnSc2	pára
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
plněním	plnění	k1gNnSc7	plnění
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgNnPc1d1	obvyklé
plnění	plnění	k1gNnPc1	plnění
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
100	[number]	k4	100
%	%	kIx~	%
(	(	kIx(	(
<g/>
plnotlaké	plnotlaký	k2eAgInPc1d1	plnotlaký
stroje	stroj	k1gInPc1	stroj
<g/>
)	)	kIx)	)
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
–	–	k?	–
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
expanze	expanze	k1gFnSc2	expanze
páry	pára	k1gFnSc2	pára
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
několikanásobně	několikanásobně	k6eAd1	několikanásobně
zvýšit	zvýšit	k5eAaPmF	zvýšit
účinnost	účinnost	k1gFnSc4	účinnost
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
vývojovým	vývojový	k2eAgNnSc7d1	vývojové
stádiem	stádium	k1gNnSc7	stádium
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
využití	využití	k1gNnSc1	využití
několikanásobné	několikanásobný	k2eAgFnSc2d1	několikanásobná
expanze	expanze	k1gFnSc2	expanze
ve	v	k7c6	v
sdružených	sdružený	k2eAgInPc6d1	sdružený
parních	parní	k2eAgInPc6d1	parní
strojích	stroj	k1gInPc6	stroj
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
==	==	k?	==
</s>
</p>
<p>
<s>
Stabilní	stabilní	k2eAgInSc1d1	stabilní
parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
</s>
</p>
<p>
<s>
Parní	parní	k2eAgFnSc1d1	parní
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
</s>
</p>
<p>
<s>
Lokomobila	lokomobila	k1gFnSc1	lokomobila
</s>
</p>
<p>
<s>
Parník	parník	k1gInSc1	parník
(	(	kIx(	(
<g/>
paroloď	paroloď	k1gFnSc1	paroloď
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Parní	parní	k2eAgInSc1d1	parní
válec	válec	k1gInSc1	válec
</s>
</p>
<p>
<s>
Parní	parní	k2eAgInSc1d1	parní
automobil	automobil	k1gInSc1	automobil
</s>
</p>
<p>
<s>
Parostrojní	parostrojní	k2eAgFnSc1d1	parostrojní
elektrárna	elektrárna	k1gFnSc1	elektrárna
</s>
</p>
<p>
<s>
Parní	parní	k2eAgFnSc1d1	parní
stříkačka	stříkačka	k1gFnSc1	stříkačka
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BOURNE	BOURNE	kA	BOURNE
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
C.E.	C.E.	k1gMnSc1	C.E.
A	a	k8xC	a
Catechism	Catechism	k1gMnSc1	Catechism
of	of	k?	of
the	the	k?	the
Steam	Steam	k1gInSc1	Steam
Engine	Engin	k1gInSc5	Engin
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Project	Project	k2eAgInSc1d1	Project
Gutenberg	Gutenberg	k1gInSc1	Gutenberg
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
také	také	k9	také
na	na	k7c4	na
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
10998	[number]	k4	10998
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Parní	parní	k2eAgFnSc1d1	parní
turbína	turbína	k1gFnSc1	turbína
</s>
</p>
<p>
<s>
Termodynamika	termodynamika	k1gFnSc1	termodynamika
</s>
</p>
<p>
<s>
Mazací	mazací	k2eAgInSc1d1	mazací
lis	lis	k1gInSc1	lis
</s>
</p>
<p>
<s>
Odvodňovací	odvodňovací	k2eAgInSc1d1	odvodňovací
ventil	ventil	k1gInSc1	ventil
</s>
</p>
<p>
<s>
Parní	parní	k2eAgInSc1d1	parní
regulátor	regulátor	k1gInSc1	regulátor
</s>
</p>
<p>
<s>
Píst	píst	k1gInSc1	píst
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
</s>
</p>
<p>
<s>
Sdružený	sdružený	k2eAgInSc1d1	sdružený
parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
</s>
</p>
<p>
<s>
Ucpávka	ucpávka	k1gFnSc1	ucpávka
</s>
</p>
<p>
<s>
Lokomotivní	lokomotivní	k2eAgInSc1d1	lokomotivní
parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
</s>
</p>
<p>
<s>
Steampunk	Steampunk	k6eAd1	Steampunk
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
parní	parní	k2eAgInSc4d1	parní
stroj	stroj	k1gInSc4	stroj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc4d1	encyklopedické
heslo	heslo	k1gNnSc4	heslo
Parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Animace	animace	k1gFnPc1	animace
a	a	k8xC	a
obrázky	obrázek	k1gInPc1	obrázek
</s>
</p>
