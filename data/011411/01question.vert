<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pístový	pístový	k2eAgInSc1d1	pístový
tepelný	tepelný	k2eAgInSc1d1	tepelný
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
spalovací	spalovací	k2eAgInSc1d1	spalovací
motor	motor	k1gInSc1	motor
s	s	k7c7	s
vnějším	vnější	k2eAgNnSc7d1	vnější
spalováním	spalování	k1gNnSc7	spalování
<g/>
,	,	kIx,	,
přeměňující	přeměňující	k2eAgInPc1d1	přeměňující
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
energii	energie	k1gFnSc4	energie
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
<g/>
?	?	kIx.	?
</s>
