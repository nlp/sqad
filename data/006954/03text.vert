<s>
Žert	žert	k1gInSc1	žert
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
román	román	k1gInSc1	román
Milana	Milan	k1gMnSc2	Milan
Kundery	Kundera	k1gFnSc2	Kundera
napsaný	napsaný	k2eAgInSc1d1	napsaný
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
vydaný	vydaný	k2eAgInSc1d1	vydaný
roku	rok	k1gInSc3	rok
1967	[number]	k4	1967
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
Ludvík	Ludvík	k1gMnSc1	Ludvík
Jahn	Jahn	k1gMnSc1	Jahn
<g/>
,	,	kIx,	,
v	v	k7c6	v
raných	raný	k2eAgNnPc6d1	rané
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
jako	jako	k9	jako
student	student	k1gMnSc1	student
horlivý	horlivý	k2eAgMnSc1d1	horlivý
komunista	komunista	k1gMnSc1	komunista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
kvůli	kvůli	k7c3	kvůli
žertovné	žertovný	k2eAgFnSc3d1	žertovná
pohlednici	pohlednice	k1gFnSc3	pohlednice
(	(	kIx(	(
<g/>
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
"	"	kIx"	"
<g/>
Ať	ať	k9	ať
žije	žít	k5eAaImIp3nS	žít
Trockij	Trockij	k1gFnSc1	Trockij
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslané	poslaný	k2eAgFnSc3d1	poslaná
spolužačce	spolužačka	k1gFnSc3	spolužačka
Markétě	Markéta	k1gFnSc3	Markéta
na	na	k7c4	na
socialistické	socialistický	k2eAgNnSc4d1	socialistické
školení	školení	k1gNnSc4	školení
<g/>
,	,	kIx,	,
vyhozen	vyhozen	k2eAgInSc4d1	vyhozen
z	z	k7c2	z
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
i	i	k8xC	i
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
pomstít	pomstít	k5eAaPmF	pomstít
bývalému	bývalý	k2eAgMnSc3d1	bývalý
spolužákovi	spolužák	k1gMnSc3	spolužák
Zemánkovi	Zemánek	k1gMnSc3	Zemánek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
rozhodující	rozhodující	k2eAgFnSc6d1	rozhodující
chvíli	chvíle	k1gFnSc6	chvíle
podrazil	podrazit	k5eAaPmAgMnS	podrazit
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
svede	svést	k5eAaPmIp3nS	svést
Zemánkovu	Zemánkův	k2eAgFnSc4d1	Zemánkova
ženu	žena	k1gFnSc4	žena
Helenu	Helena	k1gFnSc4	Helena
<g/>
.	.	kIx.	.
</s>
<s>
Zjistí	zjistit	k5eAaPmIp3nS	zjistit
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
Zemánkovi	Zemánek	k1gMnSc3	Zemánek
spolu	spolu	k6eAd1	spolu
již	již	k6eAd1	již
stejně	stejně	k6eAd1	stejně
nežijí	žít	k5eNaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
textu	text	k1gInSc2	text
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
retrospektivní	retrospektivní	k2eAgNnSc1d1	retrospektivní
vyprávění	vyprávění	k1gNnSc1	vyprávění
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
začíná	začínat	k5eAaImIp3nS	začínat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
rodného	rodný	k2eAgNnSc2d1	rodné
moravského	moravský	k2eAgNnSc2d1	Moravské
města	město	k1gNnSc2	město
provést	provést	k5eAaPmF	provést
svoji	svůj	k3xOyFgFnSc4	svůj
pomstu	pomsta	k1gFnSc4	pomsta
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
důvod	důvod	k1gInSc4	důvod
a	a	k8xC	a
Ludvíkův	Ludvíkův	k2eAgInSc4d1	Ludvíkův
předchozí	předchozí	k2eAgInSc4d1	předchozí
život	život	k1gInSc4	život
je	být	k5eAaImIp3nS	být
čtenáři	čtenář	k1gMnSc3	čtenář
osvětlován	osvětlovat	k5eAaImNgInS	osvětlovat
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
kapitolách	kapitola	k1gFnPc6	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
sice	sice	k8xC	sice
komentuje	komentovat	k5eAaBmIp3nS	komentovat
politickou	politický	k2eAgFnSc4d1	politická
atmosféru	atmosféra	k1gFnSc4	atmosféra
doby	doba	k1gFnSc2	doba
padesátých	padesátý	k4xOgNnPc2	padesátý
a	a	k8xC	a
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
autorova	autorův	k2eAgFnSc1d1	autorova
pozornost	pozornost	k1gFnSc1	pozornost
je	být	k5eAaImIp3nS	být
obrácena	obrátit	k5eAaPmNgFnS	obrátit
spíše	spíše	k9	spíše
na	na	k7c4	na
Ludvíkovy	Ludvíkův	k2eAgInPc4d1	Ludvíkův
vztahy	vztah	k1gInPc4	vztah
a	a	k8xC	a
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k6eAd1	také
části	část	k1gFnPc4	část
věnované	věnovaný	k2eAgFnSc3d1	věnovaná
moravské	moravský	k2eAgFnSc3d1	Moravská
lidové	lidový	k2eAgFnSc3d1	lidová
písni	píseň	k1gFnSc3	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Jaromil	Jaromil	k1gMnSc1	Jaromil
Jireš	Jireš	k1gMnSc1	Jireš
natočil	natočit	k5eAaBmAgMnS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
oceňován	oceňovat	k5eAaImNgMnS	oceňovat
filmovou	filmový	k2eAgFnSc7d1	filmová
kritikou	kritika	k1gFnSc7	kritika
i	i	k8xC	i
diváctvem	diváctvo	k1gNnSc7	diváctvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
západní	západní	k2eAgFnSc2d1	západní
filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
jeho	jeho	k3xOp3gFnSc2	jeho
Nesnesitelné	snesitelný	k2eNgFnSc2d1	nesnesitelná
lehkosti	lehkost	k1gFnSc2	lehkost
bytí	bytí	k1gNnSc2	bytí
<g/>
)	)	kIx)	)
líbil	líbit	k5eAaImAgMnS	líbit
i	i	k8xC	i
Kunderovi	Kunderův	k2eAgMnPc1d1	Kunderův
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
