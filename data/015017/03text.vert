<s>
Polyethylen	polyethylen	k1gInSc1
</s>
<s>
polyethylen	polyethylen	k1gInSc1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Polymethandiyl	Polymethandiyl	k1gInSc1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
С	С	k?
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
n	n	k0
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
9002-88-4	9002-88-4	k4
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Označení	označení	k1gNnSc1
vysokohustotního	vysokohustotní	k2eAgInSc2d1
polyethylenu	polyethylen	k1gInSc2
</s>
<s>
Označení	označení	k1gNnSc1
nízkohustotního	nízkohustotní	k2eAgInSc2d1
polyethylenu	polyethylen	k1gInSc2
</s>
<s>
Polyethylen	polyethylen	k1gInSc1
(	(	kIx(
<g/>
PE	PE	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
termoplast	termoplast	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vzniká	vznikat	k5eAaImIp3nS
polymerací	polymerace	k1gFnSc7
ethenu	ethen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Poprvé	poprvé	k6eAd1
jej	on	k3xPp3gMnSc4
syntetizoval	syntetizovat	k5eAaImAgMnS
Hans	Hans	k1gMnSc1
von	von	k1gInSc4
Pechmann	Pechmann	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1898	#num#	k4
zahříváním	zahřívání	k1gNnSc7
diazomethanu	diazomethan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
byla	být	k5eAaImAgFnS
zvládnuta	zvládnut	k2eAgFnSc1d1
průmyslová	průmyslový	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
průmyslová	průmyslový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
nízkohustotního	nízkohustotní	k2eAgInSc2d1
–	–	k?
rozvětveného	rozvětvený	k2eAgInSc2d1
polyetylenu	polyetylen	k1gInSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
syntetizován	syntetizován	k2eAgInSc1d1
lineární	lineární	k2eAgInSc1d1
vysokohustotní	vysokohustotní	k2eAgInSc1d1
polyetylen	polyetylen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrobu	výrob	k1gInSc2
lineárního	lineární	k2eAgInSc2d1
stereoregulárního	stereoregulární	k2eAgInSc2d1
polyethylenu	polyethylen	k1gInSc2
umožnil	umožnit	k5eAaPmAgMnS
objev	objev	k1gInSc4
polyinzerce	polyinzerka	k1gFnSc3
pomocí	pomocí	k7c2
Ziegler-Nattova	Ziegler-Nattův	k2eAgInSc2d1
katalyzátoru	katalyzátor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
polyethylenu	polyethylen	k1gInSc2
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
názevZkratka	názevZkratka	k1gFnSc1
</s>
<s>
Ultra	ultra	k2eAgMnSc1d1
high	high	k1gMnSc1
molecular	molecular	k1gMnSc1
weight	weight	k1gMnSc1
polyethylenePE-UHMW	polyethylenePE-UHMW	k?
</s>
<s>
Ultra	ultra	k2eAgMnSc1d1
low	low	k?
molecular	molecular	k1gMnSc1
weight	weight	k1gMnSc1
polyethylenePE-ULMW	polyethylenePE-ULMW	k?
nebo	nebo	k8xC
PE-WAX	PE-WAX	k1gFnSc1
</s>
<s>
High	High	k1gMnSc1
molecular	molecular	k1gMnSc1
weight	weight	k1gMnSc1
polyethylenePE-HMW	polyethylenePE-HMW	k?
</s>
<s>
High	High	k1gInSc1
density	densit	k1gInPc1
polyethylenePE-HD	polyethylenePE-HD	k?
</s>
<s>
High	High	k1gInSc1
density	densit	k1gInPc1
cross-linked	cross-linked	k1gInSc1
polyethylenePE-HDXL	polyethylenePE-HDXL	k?
</s>
<s>
Cross-linked	Cross-linked	k1gInSc1
polyethylenePE-X	polyethylenePE-X	k?
nebo	nebo	k8xC
XLPE	XLPE	kA
</s>
<s>
Medium	medium	k1gNnSc4
density	densit	k1gInPc1
polyethylenePE-MD	polyethylenePE-MD	k?
</s>
<s>
Low	Low	k?
density	densit	k1gInPc1
polyethylenePE-LD	polyethylenePE-LD	k?
</s>
<s>
Linear	Linear	k1gMnSc1
low	low	k?
density	densit	k1gInPc1
polyethylenePE-LLD	polyethylenePE-LLD	k?
</s>
<s>
Very	Ver	k2eAgInPc1d1
low	low	k?
density	densit	k1gInPc1
polyethylenePE-VLD	polyethylenePE-VLD	k?
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Lze	lze	k6eAd1
využít	využít	k5eAaPmF
polymerace	polymerace	k1gFnPc4
za	za	k7c2
nízkého	nízký	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
(	(	kIx(
<g/>
vznikne	vzniknout	k5eAaPmIp3nS
polymer	polymer	k1gInSc1
s	s	k7c7
lineárním	lineární	k2eAgInSc7d1
řetězcem	řetězec	k1gInSc7
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
lPE	lPE	k?
–	–	k?
liten	liten	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
za	za	k7c2
vysokého	vysoký	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
(	(	kIx(
<g/>
vznikne	vzniknout	k5eAaPmIp3nS
polymer	polymer	k1gInSc1
s	s	k7c7
rozvětveným	rozvětvený	k2eAgInSc7d1
řetězcem	řetězec	k1gInSc7
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
rPE	rPE	k?
–	–	k?
bralen	bralen	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
výrobě	výroba	k1gFnSc6
lze	lze	k6eAd1
využít	využít	k5eAaPmF
vstřikování	vstřikování	k1gNnSc4
a	a	k8xC
vytlačování	vytlačování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
odolný	odolný	k2eAgMnSc1d1
vůči	vůči	k7c3
kyselinám	kyselina	k1gFnPc3
i	i	k8xC
zásadám	zásada	k1gFnPc3
<g/>
,	,	kIx,
použitelný	použitelný	k2eAgInSc1d1
do	do	k7c2
teploty	teplota	k1gFnSc2
kolem	kolem	k7c2
80	#num#	k4
stupňů	stupeň	k1gInPc2
Celsia	Celsius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gInSc2
smrštitelné	smrštitelný	k2eAgFnPc4d1
fólie	fólie	k1gFnPc4
<g/>
,	,	kIx,
roury	roura	k1gFnPc4
<g/>
,	,	kIx,
ozubená	ozubený	k2eAgNnPc1d1
kola	kolo	k1gNnPc1
<g/>
,	,	kIx,
ložiska	ložisko	k1gNnPc1
<g/>
,	,	kIx,
textilní	textilní	k2eAgNnPc1d1
vlákna	vlákno	k1gNnPc1
<g/>
,	,	kIx,
nejrůznější	různý	k2eAgFnPc1d3
hračky	hračka	k1gFnPc1
<g/>
,	,	kIx,
sáčky	sáček	k1gInPc1
(	(	kIx(
<g/>
mikroten	mikroten	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
izolace	izolace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP
se	se	k3xPyFc4
dva	dva	k4xCgInPc1
základní	základní	k2eAgInPc1d1
druhy	druh	k1gInPc1
polyethylenu	polyethylen	k1gInSc2
<g/>
:	:	kIx,
PE-LD	PE-LD	k1gFnSc1
(	(	kIx(
<g/>
s	s	k7c7
nízkou	nízký	k2eAgFnSc7d1
hustotou	hustota	k1gFnSc7
<g/>
)	)	kIx)
a	a	k8xC
PE-HD	PE-HD	k1gMnSc1
(	(	kIx(
<g/>
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
hustotou	hustota	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
PE-HD	PE-HD	k1gMnSc1
má	mít	k5eAaImIp3nS
vysoký	vysoký	k2eAgInSc4d1
stupeň	stupeň	k1gInSc4
krystality	krystalit	k1gInPc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
způsobuje	způsobovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
vysokou	vysoký	k2eAgFnSc4d1
chemickou	chemický	k2eAgFnSc4d1
odolnost	odolnost	k1gFnSc4
a	a	k8xC
odolnost	odolnost	k1gFnSc4
proti	proti	k7c3
rozpouštědlům	rozpouštědlo	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podmínkou	podmínka	k1gFnSc7
vysokého	vysoký	k2eAgInSc2d1
stupně	stupeň	k1gInSc2
krystality	krystalit	k1gInPc4
je	být	k5eAaImIp3nS
linearita	linearita	k1gFnSc1
a	a	k8xC
stereoregularita	stereoregularita	k1gFnSc1
řetězců	řetězec	k1gInPc2
<g/>
,	,	kIx,
čehož	což	k3yRnSc2,k3yQnSc2
se	se	k3xPyFc4
dosahuje	dosahovat	k5eAaImIp3nS
při	při	k7c6
polyinzerci	polyinzerec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
PE-HD	PE-HD	k?
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
i	i	k9
při	při	k7c6
výrobě	výroba	k1gFnSc6
kompozitního	kompozitní	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
na	na	k7c6
bázi	báze	k1gFnSc6
dřeva	dřevo	k1gNnSc2
–	–	k?
woodplastic	woodplastice	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
náhrada	náhrada	k1gFnSc1
dřeva	dřevo	k1gNnSc2
v	v	k7c6
mnoha	mnoho	k4c2
oborech	obor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Polyethylen	polyethylen	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
nejpoužívanějším	používaný	k2eAgInSc7d3
polymerem	polymer	k1gInSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
zásluhou	zásluha	k1gFnSc7
předčila	předčit	k5eAaBmAgFnS,k5eAaPmAgFnS
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
produkce	produkce	k1gFnSc2
plastů	plast	k1gInPc2
celosvětovou	celosvětový	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
oceli	ocel	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
roční	roční	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
je	být	k5eAaImIp3nS
odhadována	odhadovat	k5eAaImNgFnS
na	na	k7c6
více	hodně	k6eAd2
než	než	k8xS
60	#num#	k4
milionů	milion	k4xCgInPc2
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Miroslav	Miroslava	k1gFnPc2
Šuta	šuta	k1gFnSc1
<g/>
:	:	kIx,
Chemické	chemický	k2eAgFnPc1d1
látky	látka	k1gFnPc1
v	v	k7c6
životním	životní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
a	a	k8xC
zdraví	zdraví	k1gNnSc6
<g/>
,	,	kIx,
Ekologický	ekologický	k2eAgInSc1d1
institut	institut	k1gInSc1
Veronica	Veronica	k1gFnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
2008	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87308	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
↑	↑	k?
Nejpevnější	pevný	k2eAgNnSc1d3
syntetické	syntetický	k2eAgNnSc1d1
vlákno	vlákno	k1gNnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
http://www.dsm.com/en_US/html/hpf/home_dyneema.htm	http://www.dsm.com/en_US/html/hpf/home_dyneema.htm	k6eAd1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Recyklační	recyklační	k2eAgInSc1d1
symbol	symbol	k1gInSc1
</s>
<s>
Polyethylenová	polyethylenový	k2eAgNnPc1d1
vlákna	vlákno	k1gNnPc1
</s>
<s>
Mikroten	mikroten	k1gInSc1
</s>
<s>
Polypropylen	polypropylen	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
polyethylen	polyethylen	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Polythene	polythen	k1gInSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
story	story	k1gFnSc7
<g/>
:	:	kIx,
The	The	k1gFnSc1
accidental	accidental	k1gMnSc1
birth	birth	k1gInSc1
of	of	k?
plastic	plastice	k1gFnPc2
bags	bags	k6eAd1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Polythene	polythen	k1gInSc5
Technical	Technical	k1gFnPc3
Properties	Properties	k1gInSc1
&	&	k?
Applications	Applications	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Plasty	plast	k1gInPc1
</s>
<s>
Polyethylen	polyethylen	k1gInSc1
(	(	kIx(
<g/>
PE	PE	kA
<g/>
)	)	kIx)
•	•	k?
Polyethylen	polyethylen	k1gInSc1
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
hustotou	hustota	k1gFnSc7
(	(	kIx(
<g/>
HDPE	HDPE	kA
<g/>
,	,	kIx,
HD-PE	HD-PE	k1gFnSc1
<g/>
,	,	kIx,
PE-HD	PE-HD	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Polyethylentereftalát	Polyethylentereftalát	k1gInSc1
(	(	kIx(
<g/>
PET	PET	kA
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
PETP	PETP	kA
či	či	k8xC
PERF	PERF	kA
<g/>
)	)	kIx)
•	•	k?
Polyvinylchlorid	polyvinylchlorid	k1gInSc1
(	(	kIx(
<g/>
PVC	PVC	kA
<g/>
)	)	kIx)
•	•	k?
Polyvinylidenchlorid	Polyvinylidenchlorid	k1gInSc1
(	(	kIx(
<g/>
PVDC	PVDC	kA
<g/>
)	)	kIx)
•	•	k?
Polybuten	Polybutno	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
PB	PB	kA
<g/>
)	)	kIx)
•	•	k?
Polypropylen	polypropylen	k1gInSc1
(	(	kIx(
<g/>
PP	PP	kA
<g/>
)	)	kIx)
•	•	k?
Polyamidy	polyamid	k1gInPc4
(	(	kIx(
<g/>
PA	Pa	kA
<g/>
)	)	kIx)
•	•	k?
Polykarbonát	polykarbonát	k1gInSc1
(	(	kIx(
<g/>
PC	PC	kA
<g/>
)	)	kIx)
•	•	k?
Polytetrafluorethylen	Polytetrafluorethylen	k1gInSc1
(	(	kIx(
<g/>
PTFE	PTFE	kA
<g/>
)	)	kIx)
•	•	k?
Polyvinylacetát	polyvinylacetát	k1gInSc1
(	(	kIx(
<g/>
PVAC	PVAC	kA
<g/>
)	)	kIx)
•	•	k?
Polystyren	polystyren	k1gInSc1
(	(	kIx(
<g/>
PS	PS	kA
<g/>
)	)	kIx)
•	•	k?
Polyestery	polyester	k1gInPc1
•	•	k?
Akrylonitrilbutadienstyren	Akrylonitrilbutadienstyrna	k1gFnPc2
(	(	kIx(
<g/>
ABS	ABS	kA
<g/>
)	)	kIx)
•	•	k?
Plexisklo	plexisklo	k1gNnSc4
(	(	kIx(
<g/>
PMMA	PMMA	kA
<g/>
)	)	kIx)
•	•	k?
Polyvinylalkohol	Polyvinylalkohol	k1gInSc1
(	(	kIx(
<g/>
PVAL	PVAL	kA
<g/>
)	)	kIx)
</s>
<s>
Bakelit	bakelit	k1gInSc1
•	•	k?
Mikroten	mikroten	k1gInSc1
•	•	k?
Novodur	novodur	k1gInSc1
•	•	k?
Nylon	nylon	k1gInSc1
•	•	k?
Silon	silon	k1gInSc1
•	•	k?
Teflon	teflon	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
6092	#num#	k4
</s>
