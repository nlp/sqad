<p>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Kladen	kladen	k2eAgInSc1d1	kladen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgMnSc1d1	statutární
město	město	k1gNnSc4	město
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
69	[number]	k4	69
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
třináctým	třináctý	k4xOgMnPc3	třináctý
největším	veliký	k2eAgMnPc3d3	veliký
městem	město	k1gNnSc7	město
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
těžbu	těžba	k1gFnSc4	těžba
černého	černý	k2eAgNnSc2d1	černé
uhlí	uhlí	k1gNnSc2	uhlí
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
a	a	k8xC	a
výchovu	výchova	k1gFnSc4	výchova
hokejistů	hokejista	k1gMnPc2	hokejista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
25	[number]	k4	25
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
Kladenské	kladenský	k2eAgFnSc6d1	kladenská
tabuli	tabule	k1gFnSc6	tabule
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Pražské	pražský	k2eAgFnSc2d1	Pražská
plošiny	plošina	k1gFnSc2	plošina
<g/>
,	,	kIx,	,
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
s	s	k7c7	s
Křivoklátskou	křivoklátský	k2eAgFnSc7d1	Křivoklátská
vrchovinou	vrchovina	k1gFnSc7	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
poměrně	poměrně	k6eAd1	poměrně
rovinatém	rovinatý	k2eAgNnSc6d1	rovinaté
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
rozvodí	rozvodí	k1gNnSc4	rozvodí
mezi	mezi	k7c7	mezi
Vltavou	Vltava	k1gFnSc7	Vltava
a	a	k8xC	a
Berounkou	Berounka	k1gFnSc7	Berounka
<g/>
;	;	kIx,	;
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
severovýchodu	severovýchod	k1gInSc3	severovýchod
se	se	k3xPyFc4	se
odtud	odtud	k6eAd1	odtud
začíná	začínat	k5eAaImIp3nS	začínat
zahlubovat	zahlubovat	k5eAaImF	zahlubovat
několik	několik	k4yIc1	několik
mělkých	mělký	k2eAgNnPc2d1	mělké
údolí	údolí	k1gNnPc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgInPc1	žádný
větší	veliký	k2eAgInPc1d2	veliký
vodní	vodní	k2eAgInPc1d1	vodní
toky	tok	k1gInPc1	tok
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
potoků	potok	k1gInPc2	potok
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
potok	potok	k1gInSc4	potok
Dřetovický	Dřetovický	k2eAgInSc4d1	Dřetovický
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
několika	několik	k4yIc2	několik
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pramení	pramenit	k5eAaImIp3nP	pramenit
pod	pod	k7c7	pod
vlakovou	vlakový	k2eAgFnSc7d1	vlaková
zastávkou	zastávka	k1gFnSc7	zastávka
Kladno	Kladno	k1gNnSc1	Kladno
–	–	k?	–
město	město	k1gNnSc1	město
v	v	k7c6	v
Sítenském	Sítenský	k2eAgNnSc6d1	Sítenské
údolí	údolí	k1gNnSc6	údolí
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
bývalým	bývalý	k2eAgInSc7d1	bývalý
pivovarem	pivovar	k1gInSc7	pivovar
a	a	k8xC	a
v	v	k7c6	v
lese	les	k1gInSc6	les
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
Boroviny	borovina	k1gFnSc2	borovina
stéká	stékat	k5eAaImIp3nS	stékat
na	na	k7c6	na
území	území	k1gNnSc6	území
Dubí	dubí	k1gNnSc2	dubí
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
potoky	potok	k1gInPc7	potok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
povodí	povodí	k1gNnSc2	povodí
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
potok	potok	k1gInSc4	potok
Týnecký	týnecký	k2eAgInSc4d1	týnecký
(	(	kIx(	(
<g/>
pramen	pramen	k1gInSc4	pramen
v	v	k7c6	v
k.	k.	k?	k.
ú.	ú.	k?	ú.
Motyčín	Motyčín	k1gInSc1	Motyčín
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lidický	lidický	k2eAgInSc4d1	lidický
(	(	kIx(	(
<g/>
pramen	pramen	k1gInSc4	pramen
v	v	k7c6	v
k.	k.	k?	k.
ú.	ú.	k?	ú.
Kročehlavy	Kročehlava	k1gFnSc2	Kročehlava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
povodí	povodí	k1gNnSc2	povodí
Berounky	Berounka	k1gFnSc2	Berounka
přísluší	příslušet	k5eAaImIp3nS	příslušet
potok	potok	k1gInSc1	potok
Rozdělovský	Rozdělovský	k2eAgInSc1d1	Rozdělovský
<g/>
.	.	kIx.	.
</s>
<s>
Rozvodí	rozvodí	k1gNnSc1	rozvodí
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
ulic	ulice	k1gFnPc2	ulice
Doberská	Doberský	k2eAgFnSc1d1	Doberská
a	a	k8xC	a
Vašíčkova	Vašíčkův	k2eAgFnSc1d1	Vašíčkova
<g/>
.	.	kIx.	.
</s>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
obklopeno	obklopen	k2eAgNnSc4d1	obklopeno
lesy	les	k1gInPc1	les
–	–	k?	–
nejzazší	zadní	k2eAgInSc4d3	nejzazší
okraj	okraj	k1gInSc4	okraj
lesnatého	lesnatý	k2eAgNnSc2d1	lesnaté
pásma	pásmo	k1gNnSc2	pásmo
Křivoklátska	Křivoklátsko	k1gNnSc2	Křivoklátsko
a	a	k8xC	a
Džbánu	džbán	k1gInSc2	džbán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
města	město	k1gNnSc2	město
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
severním	severní	k2eAgNnSc6d1	severní
až	až	k6eAd1	až
západním	západní	k2eAgNnSc6d1	západní
okolí	okolí	k1gNnSc6	okolí
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
ložiska	ložisko	k1gNnSc2	ložisko
černého	černý	k2eAgNnSc2d1	černé
uhlí	uhlí	k1gNnSc2	uhlí
karbonského	karbonský	k2eAgNnSc2d1	karbonské
stáří	stáří	k1gNnSc2	stáří
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
zčásti	zčásti	k6eAd1	zčásti
vytěžená	vytěžený	k2eAgFnSc1d1	vytěžená
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
je	být	k5eAaImIp3nS	být
místy	místy	k6eAd1	místy
poznamenána	poznamenat	k5eAaPmNgFnS	poznamenat
stopami	stopa	k1gFnPc7	stopa
těžební	těžební	k2eAgFnSc2d1	těžební
a	a	k8xC	a
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
činnosti	činnost	k1gFnSc2	činnost
uplynulých	uplynulý	k2eAgNnPc2d1	uplynulé
dvou	dva	k4xCgNnPc2	dva
století	století	k1gNnPc2	století
–	–	k?	–
zarůstající	zarůstající	k2eAgFnSc2d1	zarůstající
haldy	halda	k1gFnSc2	halda
důlní	důlní	k2eAgFnSc2d1	důlní
hlušiny	hlušina	k1gFnSc2	hlušina
a	a	k8xC	a
železárenské	železárenský	k2eAgFnSc2d1	Železárenská
strusky	struska	k1gFnSc2	struska
nejsou	být	k5eNaImIp3nP	být
k	k	k7c3	k
přehlédnutí	přehlédnutí	k1gNnSc3	přehlédnutí
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
Turyňský	Turyňský	k2eAgInSc4d1	Turyňský
rybník	rybník	k1gInSc4	rybník
pod	pod	k7c7	pod
obcí	obec	k1gFnSc7	obec
Srby	Srba	k1gMnSc2	Srba
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
propadu	propad	k1gInSc6	propad
části	část	k1gFnSc2	část
území	území	k1gNnSc4	území
(	(	kIx(	(
<g/>
zapříčiněném	zapříčiněný	k2eAgInSc6d1	zapříčiněný
důlní	důlní	k2eAgFnSc1d1	důlní
činností	činnost	k1gFnSc7	činnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
zaplavení	zaplavení	k1gNnSc2	zaplavení
tzv.	tzv.	kA	tzv.
Panské	panská	k1gFnSc2	panská
cesty	cesta	k1gFnSc2	cesta
získal	získat	k5eAaPmAgMnS	získat
lidové	lidový	k2eAgNnSc1d1	lidové
označení	označení	k1gNnSc1	označení
Záplavy	záplava	k1gFnPc1	záplava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
podmínky	podmínka	k1gFnPc1	podmínka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
===	===	k?	===
</s>
</p>
<p>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
rozlohu	rozloha	k1gFnSc4	rozloha
36,96	[number]	k4	36,96
km	km	kA	km
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
12,75	[number]	k4	12,75
km	km	kA	km
<g/>
2	[number]	k4	2
představují	představovat	k5eAaImIp3nP	představovat
lesy	les	k1gInPc1	les
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
náměstí	náměstí	k1gNnSc1	náměstí
Starosty	Starosta	k1gMnSc2	Starosta
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
381	[number]	k4	381
m	m	kA	m
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
Kožová	Kožová	k1gFnSc1	Kožová
hora	hora	k1gFnSc1	hora
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
456	[number]	k4	456
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejnižším	nízký	k2eAgInSc7d3	nejnižší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Dřetovický	Dřetovický	k2eAgInSc1d1	Dřetovický
potok	potok	k1gInSc1	potok
za	za	k7c7	za
Vrapicemi	Vrapice	k1gInPc7	Vrapice
opouští	opouštět	k5eAaImIp3nS	opouštět
území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
283	[number]	k4	283
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chráněná	chráněný	k2eAgNnPc1d1	chráněné
území	území	k1gNnPc1	území
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Vrapickém	Vrapický	k2eAgInSc6d1	Vrapický
lese	les	k1gInSc6	les
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
Kladna	Kladno	k1gNnSc2	Kladno
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Žraločí	žraločí	k2eAgFnSc1d1	žraločí
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
další	další	k2eAgNnPc1d1	další
chráněná	chráněný	k2eAgNnPc1d1	chráněné
území	území	k1gNnPc1	území
(	(	kIx(	(
<g/>
Vinařická	Vinařický	k2eAgFnSc1d1	Vinařická
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Pašijová	pašijový	k2eAgFnSc1d1	pašijová
draha	draha	k1gFnSc1	draha
<g/>
,	,	kIx,	,
Záplavy	záplava	k1gFnPc1	záplava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgInSc1d1	přírodní
park	park	k1gInSc1	park
Džbán	džbán	k1gInSc1	džbán
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodné	pozoruhodný	k2eAgInPc1d1	pozoruhodný
jsou	být	k5eAaImIp3nP	být
revitalizované	revitalizovaný	k2eAgInPc1d1	revitalizovaný
mokřady	mokřad	k1gInPc1	mokřad
na	na	k7c6	na
Týneckém	týnecký	k2eAgInSc6d1	týnecký
potoce	potok	k1gInSc6	potok
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Vodního	vodní	k2eAgInSc2d1	vodní
parku	park	k1gInSc2	park
Čabárna	Čabárna	k1gFnSc1	Čabárna
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
soustavu	soustava	k1gFnSc4	soustava
rybníků	rybník	k1gInPc2	rybník
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
přírodní	přírodní	k2eAgInSc4d1	přírodní
ekosystém	ekosystém	k1gInSc4	ekosystém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
park	park	k1gInSc1	park
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
za	za	k7c4	za
významný	významný	k2eAgInSc4d1	významný
krajinný	krajinný	k2eAgInSc4d1	krajinný
prvek	prvek	k1gInSc4	prvek
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
území	území	k1gNnSc4	území
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
navštěvované	navštěvovaný	k2eAgNnSc1d1	navštěvované
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
environmentální	environmentální	k2eAgFnSc2d1	environmentální
výchovy	výchova	k1gFnSc2	výchova
nebo	nebo	k8xC	nebo
prostého	prostý	k2eAgInSc2d1	prostý
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poddanské	poddanský	k2eAgNnSc1d1	poddanské
městečko	městečko	k1gNnSc1	městečko
===	===	k?	===
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
Kladno	Kladno	k1gNnSc1	Kladno
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
v	v	k7c6	v
zemských	zemský	k2eAgFnPc6d1	zemská
deskách	deska	k1gFnPc6	deska
roku	rok	k1gInSc2	rok
1318	[number]	k4	1318
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
sám	sám	k3xTgInSc1	sám
název	název	k1gInSc1	název
(	(	kIx(	(
<g/>
související	související	k2eAgMnSc1d1	související
se	se	k3xPyFc4	se
slovem	slovem	k6eAd1	slovem
kláda	kláda	k1gFnSc1	kláda
<g/>
)	)	kIx)	)
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
lesnatou	lesnatý	k2eAgFnSc4d1	lesnatá
polohu	poloha	k1gFnSc4	poloha
někdejší	někdejší	k2eAgFnSc2d1	někdejší
osady	osada	k1gFnSc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
několik	několik	k4yIc4	několik
staletí	staletí	k1gNnPc2	staletí
byla	být	k5eAaImAgFnS	být
ves	ves	k1gFnSc1	ves
Kladno	Kladno	k1gNnSc4	Kladno
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
rozvětveného	rozvětvený	k2eAgInSc2d1	rozvětvený
rytířského	rytířský	k2eAgInSc2d1	rytířský
rodu	rod	k1gInSc2	rod
Kladenských	kladenský	k2eAgInPc2d1	kladenský
z	z	k7c2	z
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
měli	mít	k5eAaImAgMnP	mít
dokonce	dokonce	k9	dokonce
tři	tři	k4xCgFnPc4	tři
tvrze	tvrz	k1gFnPc4	tvrz
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
tvrz	tvrz	k1gFnSc1	tvrz
poblíž	poblíž	k7c2	poblíž
dnešní	dnešní	k2eAgFnSc2d1	dnešní
hlavní	hlavní	k2eAgFnSc2d1	hlavní
pošty	pošta	k1gFnSc2	pošta
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Horní	horní	k2eAgFnSc1d1	horní
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
výstavná	výstavný	k2eAgFnSc1d1	výstavná
a	a	k8xC	a
podobala	podobat	k5eAaImAgFnS	podobat
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
hrádku	hrádek	k1gInSc3	hrádek
<g/>
;	;	kIx,	;
jejím	její	k3xOp3gMnSc7	její
pokračovatelem	pokračovatel	k1gMnSc7	pokračovatel
je	být	k5eAaImIp3nS	být
dnešní	dnešní	k2eAgInSc1d1	dnešní
kladenský	kladenský	k2eAgInSc1d1	kladenský
zámek	zámek	k1gInSc1	zámek
<g/>
;	;	kIx,	;
a	a	k8xC	a
</s>
</p>
<p>
<s>
Vlaškovu	Vlaškův	k2eAgFnSc4d1	Vlaškův
tvrz	tvrz	k1gFnSc4	tvrz
na	na	k7c6	na
Ostrovci	Ostrovec	k1gMnSc6	Ostrovec
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
na	na	k7c4	na
Rozdělov	Rozdělov	k1gInSc4	Rozdělov
<g/>
.	.	kIx.	.
<g/>
Posledním	poslední	k2eAgMnSc7d1	poslední
příslušníkem	příslušník	k1gMnSc7	příslušník
hlavní	hlavní	k2eAgFnSc2d1	hlavní
linie	linie	k1gFnSc2	linie
rodu	rod	k1gInSc2	rod
Kladenských	kladenský	k2eAgFnPc2d1	kladenská
byl	být	k5eAaImAgInS	být
rytíř	rytíř	k1gMnSc1	rytíř
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kladenský	kladenský	k2eAgMnSc1d1	kladenský
z	z	k7c2	z
Kladna	Kladno	k1gNnSc2	Kladno
(	(	kIx(	(
<g/>
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
1450	[number]	k4	1450
<g/>
–	–	k?	–
<g/>
1543	[number]	k4	1543
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgNnSc4d1	velké
panství	panství	k1gNnSc4	panství
odkázal	odkázat	k5eAaPmAgInS	odkázat
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
čtyřem	čtyři	k4xCgInPc3	čtyři
<g/>
)	)	kIx)	)
potomkům	potomek	k1gMnPc3	potomek
svého	svůj	k3xOyFgMnSc2	svůj
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
synovce	synovec	k1gMnSc2	synovec
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Žďárského	Žďárský	k1gMnSc2	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
(	(	kIx(	(
<g/>
1481	[number]	k4	1481
<g/>
–	–	k?	–
<g/>
1542	[number]	k4	1542
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poněvadž	poněvadž	k8xS	poněvadž
však	však	k9	však
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
prasynovců	prasynovec	k1gMnPc2	prasynovec
(	(	kIx(	(
<g/>
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
)	)	kIx)	)
zemřel	zemřít	k5eAaPmAgMnS	zemřít
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
otec	otec	k1gMnSc1	otec
Oldřich	Oldřich	k1gMnSc1	Oldřich
<g/>
)	)	kIx)	)
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Kladenským	kladenský	k2eAgMnSc7d1	kladenský
<g/>
,	,	kIx,	,
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
vlastními	vlastní	k2eAgMnPc7d1	vlastní
dědici	dědic	k1gMnPc7	dědic
kladenského	kladenský	k2eAgNnSc2d1	kladenské
panství	panství	k1gNnSc2	panství
tři	tři	k4xCgMnPc1	tři
Oldřichovi	Oldřichův	k2eAgMnPc1d1	Oldřichův
pozůstalí	pozůstalý	k2eAgMnPc1d1	pozůstalý
synové	syn	k1gMnPc1	syn
<g/>
,	,	kIx,	,
kterými	který	k3yRgMnPc7	který
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
starší	starší	k1gMnSc1	starší
(	(	kIx(	(
<g/>
1502	[number]	k4	1502
<g/>
–	–	k?	–
<g/>
1578	[number]	k4	1578
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
(	(	kIx(	(
<g/>
1517	[number]	k4	1517
<g/>
–	–	k?	–
<g/>
1574	[number]	k4	1574
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
(	(	kIx(	(
<g/>
1519	[number]	k4	1519
–	–	k?	–
asi	asi	k9	asi
1557	[number]	k4	1557
<g/>
)	)	kIx)	)
rytíři	rytíř	k1gMnPc1	rytíř
Žďárští	žďárský	k2eAgMnPc1d1	žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc4d1	nový
rod	rod	k1gInSc4	rod
vlastníků	vlastník	k1gMnPc2	vlastník
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
středních	střední	k2eAgFnPc2d1	střední
Čech	Čechy	k1gFnPc2	Čechy
již	již	k6eAd1	již
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
ze	z	k7c2	z
vzdáleného	vzdálený	k2eAgNnSc2d1	vzdálené
a	a	k8xC	a
hornatého	hornatý	k2eAgNnSc2d1	hornaté
západočeského	západočeský	k2eAgNnSc2d1	Západočeské
Doupovska	Doupovsko	k1gNnSc2	Doupovsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obýval	obývat	k5eAaImAgMnS	obývat
tvrz	tvrz	k1gFnSc4	tvrz
a	a	k8xC	a
poté	poté	k6eAd1	poté
zámek	zámek	k1gInSc1	zámek
Žďár	Žďár	k1gInSc1	Žďár
u	u	k7c2	u
Doupova	Doupův	k2eAgMnSc2d1	Doupův
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
uchovávala	uchovávat	k5eAaImAgFnS	uchovávat
rodina	rodina	k1gFnSc1	rodina
místních	místní	k2eAgMnPc2d1	místní
Žďárských	Žďárský	k1gMnPc2	Žďárský
s	s	k7c7	s
rodným	rodný	k2eAgNnSc7d1	rodné
Doupovskem	Doupovsko	k1gNnSc7	Doupovsko
nadále	nadále	k6eAd1	nadále
četné	četný	k2eAgInPc4d1	četný
(	(	kIx(	(
<g/>
rodové	rodový	k2eAgInPc4d1	rodový
i	i	k8xC	i
majetkové	majetkový	k2eAgInPc4d1	majetkový
<g/>
)	)	kIx)	)
kontakty	kontakt	k1gInPc4	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
zmínění	zmíněný	k2eAgMnPc1d1	zmíněný
tři	tři	k4xCgMnPc1	tři
bratři	bratr	k1gMnPc1	bratr
Žďárští	žďárský	k2eAgMnPc1d1	žďárský
drželi	držet	k5eAaImAgMnP	držet
Kladno	Kladno	k1gNnSc4	Kladno
zprvu	zprvu	k6eAd1	zprvu
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
ale	ale	k8xC	ale
statek	statek	k1gInSc4	statek
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
a	a	k8xC	a
vlastní	vlastní	k2eAgNnSc4d1	vlastní
Kladensko	Kladensko	k1gNnSc4	Kladensko
získal	získat	k5eAaPmAgInS	získat
roku	rok	k1gInSc2	rok
1548	[number]	k4	1548
nejmladší	mladý	k2eAgMnPc1d3	nejmladší
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
1557	[number]	k4	1557
jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgMnSc1d1	nový
majitel	majitel	k1gMnSc1	majitel
Kladna	Kladno	k1gNnSc2	Kladno
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Žďárský	Žďárský	k1gMnSc1	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
(	(	kIx(	(
<g/>
1517	[number]	k4	1517
<g/>
–	–	k?	–
<g/>
1574	[number]	k4	1574
<g/>
)	)	kIx)	)
zůstal	zůstat	k5eAaPmAgInS	zůstat
společně	společně	k6eAd1	společně
se	s	k7c7	s
sourozenci	sourozenec	k1gMnPc7	sourozenec
katolíkem	katolík	k1gMnSc7	katolík
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
dobrým	dobrý	k2eAgMnSc7d1	dobrý
hospodářem	hospodář	k1gMnSc7	hospodář
a	a	k8xC	a
kladenské	kladenský	k2eAgNnSc4d1	kladenské
panství	panství	k1gNnPc4	panství
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
vlivným	vlivný	k2eAgMnSc7d1	vlivný
dvořanem	dvořan	k1gMnSc7	dvořan
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
I	i	k8xC	i
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
dát	dát	k5eAaPmF	dát
povýšit	povýšit	k5eAaPmF	povýšit
Kladno	Kladno	k1gNnSc4	Kladno
na	na	k7c4	na
městečko	městečko	k1gNnSc4	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Panovníkovým	panovníkův	k2eAgNnSc7d1	panovníkovo
privilegiem	privilegium	k1gNnSc7	privilegium
o	o	k7c6	o
povýšení	povýšení	k1gNnSc6	povýšení
ze	z	k7c2	z
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1561	[number]	k4	1561
dostalo	dostat	k5eAaPmAgNnS	dostat
Kladno	Kladno	k1gNnSc1	Kladno
mj.	mj.	kA	mj.
právo	právo	k1gNnSc4	právo
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
pořádat	pořádat	k5eAaImF	pořádat
dva	dva	k4xCgInPc1	dva
výroční	výroční	k2eAgInPc1d1	výroční
osmidenní	osmidenní	k2eAgInPc1d1	osmidenní
trhy	trh	k1gInPc1	trh
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pořádat	pořádat	k5eAaImF	pořádat
jeden	jeden	k4xCgInSc4	jeden
týdenní	týdenní	k2eAgInSc4d1	týdenní
trh	trh	k1gInSc4	trh
a	a	k8xC	a
městečku	městečko	k1gNnSc6	městečko
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc4d1	udělen
i	i	k8xC	i
pěkný	pěkný	k2eAgInSc4d1	pěkný
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polceném	polcený	k2eAgInSc6d1	polcený
štítě	štít	k1gInSc6	štít
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
polovina	polovina	k1gFnSc1	polovina
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
orlice	orlice	k1gFnSc2	orlice
pocházející	pocházející	k2eAgFnSc2d1	pocházející
ze	z	k7c2	z
znaku	znak	k1gInSc2	znak
vrchnosti	vrchnost	k1gFnSc2	vrchnost
–	–	k?	–
tj.	tj.	kA	tj.
rodu	rod	k1gInSc2	rod
Žďárských	Žďárských	k2eAgInSc2d1	Žďárských
–	–	k?	–
a	a	k8xC	a
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
rys	rys	k1gInSc1	rys
v	v	k7c6	v
přirozených	přirozený	k2eAgFnPc6d1	přirozená
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
snad	snad	k9	snad
měl	mít	k5eAaImAgMnS	mít
připomínat	připomínat	k5eAaImF	připomínat
zvířenu	zvířena	k1gFnSc4	zvířena
ve	v	k7c6	v
zdejších	zdejší	k2eAgInPc6d1	zdejší
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
hlubokých	hluboký	k2eAgFnPc6d1	hluboká
<g/>
,	,	kIx,	,
lesích	les	k1gInPc6	les
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
upozorňovat	upozorňovat	k5eAaImF	upozorňovat
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Horní	horní	k2eAgFnSc1d1	horní
tvrz	tvrz	k1gFnSc1	tvrz
se	se	k3xPyFc4	se
za	za	k7c2	za
Žďárských	Žďárská	k1gFnPc2	Žďárská
dočkala	dočkat	k5eAaPmAgFnS	dočkat
honosné	honosný	k2eAgFnPc4d1	honosná
přestavby	přestavba	k1gFnPc4	přestavba
na	na	k7c4	na
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc4d1	velký
výstavný	výstavný	k2eAgInSc4d1	výstavný
renesanční	renesanční	k2eAgInSc4d1	renesanční
zámek	zámek	k1gInSc4	zámek
(	(	kIx(	(
<g/>
1566	[number]	k4	1566
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pro	pro	k7c4	pro
středočeskou	středočeský	k2eAgFnSc4d1	Středočeská
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
též	též	k9	též
katolickou	katolický	k2eAgFnSc4d1	katolická
<g/>
)	)	kIx)	)
linii	linie	k1gFnSc4	linie
Žďárských	Žďárská	k1gFnPc2	Žďárská
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
hlavní	hlavní	k2eAgFnSc7d1	hlavní
rodovou	rodový	k2eAgFnSc7d1	rodová
residencí	residence	k1gFnSc7	residence
<g/>
.	.	kIx.	.
</s>
<s>
Městys	městys	k1gInSc1	městys
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
oděl	odět	k5eAaPmAgMnS	odět
v	v	k7c6	v
několika	několik	k4yIc6	několik
následujících	následující	k2eAgNnPc6d1	následující
desetiletích	desetiletí	k1gNnPc6	desetiletí
do	do	k7c2	do
nepříliš	příliš	k6eNd1	příliš
pevného	pevný	k2eAgNnSc2d1	pevné
okruží	okruží	k1gNnSc2	okruží
městských	městský	k2eAgFnPc2d1	městská
zdí	zeď	k1gFnPc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
dominovaly	dominovat	k5eAaImAgInP	dominovat
tři	tři	k4xCgFnPc4	tři
věžové	věžový	k2eAgFnPc4d1	věžová
brány	brána	k1gFnPc4	brána
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zámkem	zámek	k1gInSc7	zámek
<g/>
,	,	kIx,	,
starším	starý	k2eAgInSc7d2	starší
gotickým	gotický	k2eAgInSc7d1	gotický
kostelem	kostel	k1gInSc7	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
sousední	sousední	k2eAgInPc1d1	sousední
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
zvonicí	zvonice	k1gFnSc7	zvonice
a	a	k8xC	a
s	s	k7c7	s
pozdější	pozdní	k2eAgFnSc7d2	pozdější
radnicí	radnice	k1gFnSc7	radnice
a	a	k8xC	a
kaplí	kaple	k1gFnSc7	kaple
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc2	Florián
tvořily	tvořit	k5eAaImAgFnP	tvořit
malebnou	malebný	k2eAgFnSc4d1	malebná
siluetu	silueta	k1gFnSc4	silueta
městečka	městečko	k1gNnSc2	městečko
až	až	k9	až
do	do	k7c2	do
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
stavební	stavební	k2eAgInPc1d1	stavební
podniky	podnik	k1gInPc1	podnik
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
výrazem	výraz	k1gInSc7	výraz
snah	snaha	k1gFnPc2	snaha
rodu	rod	k1gInSc2	rod
Žďárských	Žďárská	k1gFnPc2	Žďárská
přebudovat	přebudovat	k5eAaPmF	přebudovat
Kladno	Kladno	k1gNnSc4	Kladno
na	na	k7c4	na
důstojnou	důstojný	k2eAgFnSc4d1	důstojná
rodovou	rodový	k2eAgFnSc4d1	rodová
rezidenci	rezidence	k1gFnSc4	rezidence
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
hodna	hoden	k2eAgFnSc1d1	hodna
jejich	jejich	k3xOp3gNnSc2	jejich
společenského	společenský	k2eAgNnSc2d1	společenské
postavení	postavení	k1gNnSc2	postavení
a	a	k8xC	a
nemalého	malý	k2eNgNnSc2d1	nemalé
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
Jiříkův	Jiříkův	k2eAgMnSc1d1	Jiříkův
synovec	synovec	k1gMnSc1	synovec
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1574	[number]	k4	1574
dědic	dědic	k1gMnSc1	dědic
Kladna	Kladno	k1gNnSc2	Kladno
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
Jiřího	Jiří	k1gMnSc2	Jiří
bratra	bratr	k1gMnSc2	bratr
Jana	Jan	k1gMnSc2	Jan
staršího	starý	k2eAgMnSc2d2	starší
<g/>
)	)	kIx)	)
–	–	k?	–
rytíř	rytíř	k1gMnSc1	rytíř
Ctibor	Ctibor	k1gMnSc1	Ctibor
Tiburcí	Tiburcí	k1gMnSc1	Tiburcí
Žďárský	Žďárský	k1gMnSc1	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
(	(	kIx(	(
<g/>
1545	[number]	k4	1545
<g/>
–	–	k?	–
<g/>
1615	[number]	k4	1615
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
hospodářem	hospodář	k1gMnSc7	hospodář
a	a	k8xC	a
hodnostářem	hodnostář	k1gMnSc7	hodnostář
(	(	kIx(	(
<g/>
zemský	zemský	k2eAgMnSc1d1	zemský
soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
hejtman	hejtman	k1gMnSc1	hejtman
Menšího	malý	k2eAgNnSc2d2	menší
města	město	k1gNnSc2	město
Pražského	pražský	k2eAgNnSc2d1	Pražské
<g/>
,	,	kIx,	,
purkrabí	purkrabí	k1gMnSc1	purkrabí
karlštejnský	karlštejnský	k2eAgMnSc1d1	karlštejnský
<g/>
)	)	kIx)	)
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
místodržící	místodržící	k1gMnSc1	místodržící
císaře	císař	k1gMnSc2	císař
Matyáše	Matyáš	k1gMnSc2	Matyáš
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
prožívalo	prožívat	k5eAaImAgNnS	prožívat
Kladno	Kladno	k1gNnSc1	Kladno
další	další	k2eAgFnSc2d1	další
vzestup	vzestup	k1gInSc4	vzestup
<g/>
.	.	kIx.	.
</s>
<s>
Ctibor	Ctibor	k1gMnSc1	Ctibor
Tiburcí	Tiburcí	k1gMnSc1	Tiburcí
zbudoval	zbudovat	k5eAaPmAgMnS	zbudovat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
chotí	choť	k1gFnSc7	choť
Sibylou	Sibyla	k1gFnSc7	Sibyla
Hradišťskou	hradišťský	k2eAgFnSc4d1	Hradišťská
z	z	k7c2	z
Hořovic	Hořovice	k1gFnPc2	Hořovice
zřídil	zřídit	k5eAaPmAgInS	zřídit
špitál	špitál	k1gInSc1	špitál
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
lazaret	lazaret	k1gInSc4	lazaret
<g/>
,	,	kIx,	,
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Floriánské	Floriánský	k2eAgFnSc2d1	Floriánská
kaple	kaple	k1gFnSc2	kaple
<g/>
)	)	kIx)	)
a	a	k8xC	a
chudobinec	chudobinec	k1gInSc4	chudobinec
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
založil	založit	k5eAaPmAgInS	založit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
roku	rok	k1gInSc2	rok
1586	[number]	k4	1586
společný	společný	k2eAgInSc1d1	společný
cech	cech	k1gInSc1	cech
řemeslníků	řemeslník	k1gMnPc2	řemeslník
a	a	k8xC	a
i	i	k9	i
jinak	jinak	k6eAd1	jinak
podporoval	podporovat	k5eAaImAgInS	podporovat
hospodářství	hospodářství	k1gNnSc4	hospodářství
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
panstvích	panství	k1gNnPc6	panství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Ctibora	Ctibor	k1gMnSc2	Ctibor
Tiburcího	Tiburcí	k2eAgMnSc2d1	Tiburcí
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1615	[number]	k4	1615
<g/>
)	)	kIx)	)
přešlo	přejít	k5eAaPmAgNnS	přejít
panství	panství	k1gNnSc1	panství
na	na	k7c4	na
jeho	jeho	k3xOp3gMnSc4	jeho
jediného	jediný	k2eAgMnSc4d1	jediný
syna	syn	k1gMnSc4	syn
Jana	Jan	k1gMnSc2	Jan
Jiřího	Jiří	k1gMnSc2	Jiří
Žďárského	Žďárský	k1gMnSc2	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
a	a	k8xC	a
na	na	k7c6	na
Kladně	Kladno	k1gNnSc6	Kladno
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1581	[number]	k4	1581
<g/>
–	–	k?	–
<g/>
1626	[number]	k4	1626
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předků	předek	k1gInPc2	předek
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
příliš	příliš	k6eAd1	příliš
neangažoval	angažovat	k5eNaBmAgMnS	angažovat
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
ponejvíce	ponejvíce	k6eAd1	ponejvíce
na	na	k7c6	na
zámcích	zámek	k1gInPc6	zámek
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
<g/>
,	,	kIx,	,
v	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
Újezdě	Újezd	k1gInSc6	Újezd
či	či	k8xC	či
v	v	k7c6	v
rodovém	rodový	k2eAgInSc6d1	rodový
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Kladno	Kladno	k1gNnSc4	Kladno
příznivá	příznivý	k2eAgFnSc1d1	příznivá
<g/>
.	.	kIx.	.
</s>
<s>
Městečko	městečko	k1gNnSc1	městečko
bylo	být	k5eAaImAgNnS	být
sice	sice	k8xC	sice
malé	malý	k2eAgNnSc1d1	malé
(	(	kIx(	(
<g/>
čítalo	čítat	k5eAaImAgNnS	čítat
jen	jen	k9	jen
něco	něco	k6eAd1	něco
přes	přes	k7c4	přes
třicet	třicet	k4xCc4	třicet
domů	dům	k1gInPc2	dům
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
sídlem	sídlo	k1gNnSc7	sídlo
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
dominia	dominion	k1gNnSc2	dominion
Žďárských	Žďárská	k1gFnPc2	Žďárská
<g/>
,	,	kIx,	,
sahajícího	sahající	k2eAgInSc2d1	sahající
především	především	k6eAd1	především
východním	východní	k2eAgInSc7d1	východní
a	a	k8xC	a
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
přípražské	přípražský	k2eAgNnSc4d1	přípražský
Červenoújezdecko	Červenoújezdecko	k1gNnSc4	Červenoújezdecko
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
u	u	k7c2	u
Hostivice	Hostivice	k1gFnSc2	Hostivice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
rodině	rodina	k1gFnSc3	rodina
taktéž	taktéž	k?	taktéž
patřilo	patřit	k5eAaImAgNnS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
katolíci	katolík	k1gMnPc1	katolík
<g/>
,	,	kIx,	,
připojil	připojit	k5eAaPmAgInS	připojit
za	za	k7c2	za
českého	český	k2eAgNnSc2d1	české
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
roku	rok	k1gInSc2	rok
1619	[number]	k4	1619
kladenský	kladenský	k2eAgMnSc1d1	kladenský
pán	pán	k1gMnSc1	pán
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
Žďárský	Žďárský	k1gMnSc1	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1581	[number]	k4	1581
<g/>
–	–	k?	–
<g/>
1626	[number]	k4	1626
<g/>
)	)	kIx)	)
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc1	jeho
"	"	kIx"	"
<g/>
synovec	synovec	k1gMnSc1	synovec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
jeho	jeho	k3xOp3gNnSc1	jeho
bratrance	bratranec	k1gMnSc4	bratranec
Jana	Jan	k1gMnSc4	Jan
ml.	ml.	kA	ml.
Žďárského	Žďárský	k1gMnSc4	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
a	a	k8xC	a
na	na	k7c6	na
Tachlovicích	Tachlovice	k1gFnPc6	Tachlovice
(	(	kIx(	(
<g/>
kol	kol	k7c2	kol
<g/>
.	.	kIx.	.
1566	[number]	k4	1566
<g/>
–	–	k?	–
<g/>
1598	[number]	k4	1598
<g/>
))	))	k?	))
a	a	k8xC	a
dědic	dědic	k1gMnSc1	dědic
kladenského	kladenský	k2eAgInSc2d1	kladenský
a	a	k8xC	a
červenoújezdeckého	červenoújezdecký	k2eAgInSc2d1	červenoújezdecký
velkostatku	velkostatek	k1gInSc2	velkostatek
Florián	Florián	k1gMnSc1	Florián
Jetřich	Jetřich	k1gMnSc1	Jetřich
Žďárský	Žďárský	k1gMnSc1	Žďárský
(	(	kIx(	(
<g/>
1598	[number]	k4	1598
<g/>
–	–	k?	–
<g/>
1653	[number]	k4	1653
<g/>
)	)	kIx)	)
svůj	svůj	k3xOyFgInSc4	svůj
podpis	podpis	k1gInSc4	podpis
pod	pod	k7c4	pod
stavovskou	stavovský	k2eAgFnSc4d1	stavovská
konfederaci	konfederace	k1gFnSc4	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
však	však	k9	však
Florián	Florián	k1gMnSc1	Florián
Jetřich	Jetřich	k1gMnSc1	Jetřich
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1620	[number]	k4	1620
oženil	oženit	k5eAaPmAgMnS	oženit
v	v	k7c6	v
Pasově	pasově	k6eAd1	pasově
s	s	k7c7	s
Alžbětou	Alžběta	k1gFnSc7	Alžběta
Koronou	Korona	k1gFnSc7	Korona
Bořitovou	Bořitový	k2eAgFnSc4d1	Bořitový
z	z	k7c2	z
Martinic	Martinice	k1gFnPc2	Martinice
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1603	[number]	k4	1603
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
1649	[number]	k4	1649
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
představitelů	představitel	k1gMnPc2	představitel
císařské	císařský	k2eAgFnSc2d1	císařská
strany	strana	k1gFnSc2	strana
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Bořity	Bořita	k1gMnSc2	Bořita
z	z	k7c2	z
Martinic	Martinice	k1gFnPc2	Martinice
<g/>
,	,	kIx,	,
pána	pán	k1gMnSc2	pán
na	na	k7c4	na
sousedním	sousední	k2eAgFnPc3d1	sousední
Smečně	Smečně	k1gFnPc3	Smečně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
českého	český	k2eAgNnSc2d1	české
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
postihlo	postihnout	k5eAaPmAgNnS	postihnout
Kladno	Kladno	k1gNnSc1	Kladno
velké	velká	k1gFnSc2	velká
neštěstí	neštěstí	k1gNnSc2	neštěstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
bitvy	bitva	k1gFnSc2	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
–	–	k?	–
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1620	[number]	k4	1620
–	–	k?	–
bylo	být	k5eAaImAgNnS	být
totiž	totiž	k9	totiž
městečko	městečko	k1gNnSc1	městečko
vydrancováno	vydrancován	k2eAgNnSc1d1	vydrancováno
obávanými	obávaný	k2eAgMnPc7d1	obávaný
polskými	polský	k2eAgMnPc7d1	polský
kozáky	kozák	k1gMnPc7	kozák
v	v	k7c6	v
císařských	císařský	k2eAgFnPc6d1	císařská
službách	služba	k1gFnPc6	služba
–	–	k?	–
tzv.	tzv.	kA	tzv.
Lisovčíky	Lisovčík	k1gInPc7	Lisovčík
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
tehdy	tehdy	k6eAd1	tehdy
táhli	táhnout	k5eAaImAgMnP	táhnout
s	s	k7c7	s
císařským	císařský	k2eAgNnSc7d1	císařské
vojskem	vojsko	k1gNnSc7	vojsko
od	od	k7c2	od
Rakovníka	Rakovník	k1gInSc2	Rakovník
ku	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
císařské	císařský	k2eAgFnSc2d1	císařská
jednotky	jednotka	k1gFnSc2	jednotka
střetly	střetnout	k5eAaPmAgFnP	střetnout
se	se	k3xPyFc4	se
stavovskými	stavovský	k2eAgInPc7d1	stavovský
v	v	k7c6	v
legendární	legendární	k2eAgFnSc6d1	legendární
bělohorské	bělohorský	k2eAgFnSc6d1	Bělohorská
bitvě	bitva	k1gFnSc6	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přepadu	přepad	k1gInSc6	přepad
za	za	k7c2	za
své	svůj	k3xOyFgFnSc2	svůj
vzal	vzít	k5eAaPmAgMnS	vzít
i	i	k9	i
archiv	archiv	k1gInSc4	archiv
ostrovsko-svatojánského	ostrovskovatojánský	k2eAgInSc2d1	ostrovsko-svatojánský
kláštera	klášter	k1gInSc2	klášter
benediktinů	benediktin	k1gMnPc2	benediktin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
tamější	tamější	k2eAgMnSc1d1	tamější
opat	opat	k1gMnSc1	opat
Martin	Martin	k1gMnSc1	Martin
Bytomský	Bytomský	k2eAgMnSc1d1	Bytomský
na	na	k7c6	na
bytelném	bytelný	k2eAgInSc6d1	bytelný
kladenském	kladenský	k2eAgInSc6d1	kladenský
zámku	zámek	k1gInSc6	zámek
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
katolického	katolický	k2eAgMnSc2d1	katolický
souseda	soused	k1gMnSc2	soused
uschoval	uschovat	k5eAaPmAgMnS	uschovat
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
část	část	k1gFnSc4	část
rodového	rodový	k2eAgInSc2d1	rodový
archivu	archiv	k1gInSc2	archiv
rytířů	rytíř	k1gMnPc2	rytíř
Žďárských	Žďárská	k1gFnPc2	Žďárská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
stavů	stav	k1gInPc2	stav
byly	být	k5eAaImAgInP	být
rytíři	rytíř	k1gMnSc3	rytíř
Floriánu	Florián	k1gMnSc3	Florián
Jetřichovi	Jetřich	k1gMnSc3	Jetřich
Žďárskému	Žďárský	k1gMnSc3	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc3	jeho
příbuznému	příbuzný	k1gMnSc3	příbuzný
–	–	k?	–
kladenskému	kladenský	k2eAgMnSc3d1	kladenský
pánu	pán	k1gMnSc3	pán
Janu	Jan	k1gMnSc3	Jan
Jiřímu	Jiří	k1gMnSc3	Jiří
Žďárskému	Žďárský	k1gMnSc3	Žďárský
dřívější	dřívější	k2eAgInPc4d1	dřívější
poklesky	poklesek	k1gInPc1	poklesek
prominuty	prominout	k5eAaPmNgInP	prominout
<g/>
,	,	kIx,	,
ba	ba	k9	ba
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
již	již	k9	již
roku	rok	k1gInSc2	rok
1622	[number]	k4	1622
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
dočkali	dočkat	k5eAaPmAgMnP	dočkat
povýšení	povýšení	k1gNnPc4	povýšení
do	do	k7c2	do
panského	panské	k1gNnSc2	panské
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
hrdě	hrdě	k6eAd1	hrdě
začali	začít	k5eAaPmAgMnP	začít
užívat	užívat	k5eAaImF	užívat
titul	titul	k1gInSc4	titul
baronů	baron	k1gMnPc2	baron
(	(	kIx(	(
<g/>
s	s	k7c7	s
pouhým	pouhý	k2eAgInSc7d1	pouhý
predikátem	predikát	k1gInSc7	predikát
<g/>
:	:	kIx,	:
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1626	[number]	k4	1626
zdědil	zdědit	k5eAaPmAgInS	zdědit
Kladno	Kladno	k1gNnSc4	Kladno
po	po	k7c6	po
Janu	Jan	k1gMnSc6	Jan
Jiřím	Jiří	k1gMnSc7	Jiří
pan	pan	k1gMnSc1	pan
Florián	Florián	k1gMnSc1	Florián
Jetřich	Jetřich	k1gMnSc1	Jetřich
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
stoupal	stoupat	k5eAaImAgInS	stoupat
v	v	k7c6	v
hodnostech	hodnost	k1gFnPc6	hodnost
a	a	k8xC	a
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
u	u	k7c2	u
panovnického	panovnický	k2eAgInSc2d1	panovnický
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
již	již	k6eAd1	již
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c4	po
převzetí	převzetí	k1gNnSc4	převzetí
Kladna	Kladno	k1gNnSc2	Kladno
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1628	[number]	k4	1628
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
císařem	císař	k1gMnSc7	císař
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
říšského	říšský	k2eAgInSc2d1	říšský
hraběcího	hraběcí	k2eAgInSc2d1	hraběcí
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1631	[number]	k4	1631
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
dokonce	dokonce	k9	dokonce
velkým	velký	k2eAgMnSc7d1	velký
palatinem	palatin	k1gMnSc7	palatin
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zástupcem	zástupce	k1gMnSc7	zástupce
císaře	císař	k1gMnSc4	císař
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jeho	jeho	k3xOp3gNnPc6	jeho
panovnických	panovnický	k2eAgNnPc6d1	panovnické
právech	právo	k1gNnPc6	právo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1630	[number]	k4	1630
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
na	na	k7c4	na
popud	popud	k1gInSc4	popud
hraběte	hrabě	k1gMnSc2	hrabě
Floriána	Florián	k1gMnSc2	Florián
Jetřicha	Jetřich	k1gMnSc2	Jetřich
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
kladenské	kladenský	k2eAgFnSc3d1	kladenská
městské	městský	k2eAgFnSc3d1	městská
radě	rada	k1gFnSc3	rada
výsady	výsada	k1gFnSc2	výsada
a	a	k8xC	a
rozmnožil	rozmnožit	k5eAaPmAgInS	rozmnožit
počet	počet	k1gInSc1	počet
výročních	výroční	k2eAgInPc2d1	výroční
trhů	trh	k1gInPc2	trh
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Florián	Florián	k1gMnSc1	Florián
Jetřich	Jetřich	k1gMnSc1	Jetřich
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
chotí	choť	k1gFnSc7	choť
Alžbětou	Alžběta	k1gFnSc7	Alžběta
Koronou	Korona	k1gFnSc7	Korona
Bořitovou	Bořitový	k2eAgFnSc4d1	Bořitový
z	z	k7c2	z
Martinic	Martinice	k1gFnPc2	Martinice
<g/>
)	)	kIx)	)
dal	dát	k5eAaPmAgInS	dát
roku	rok	k1gInSc2	rok
1623	[number]	k4	1623
zbudovat	zbudovat	k5eAaPmF	zbudovat
v	v	k7c6	v
oboře	obora	k1gFnSc6	obora
Hájku	hájek	k1gInSc2	hájek
u	u	k7c2	u
Unhoště	Unhošť	k1gFnSc2	Unhošť
loretánskou	loretánský	k2eAgFnSc4d1	Loretánská
kapli	kaple	k1gFnSc4	kaple
(	(	kIx(	(
<g/>
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
poděkování	poděkování	k1gNnSc4	poděkování
za	za	k7c4	za
narození	narození	k1gNnSc4	narození
syna	syn	k1gMnSc2	syn
Františka	František	k1gMnSc2	František
Adama	Adam	k1gMnSc2	Adam
Eusebia	Eusebius	k1gMnSc2	Eusebius
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1623	[number]	k4	1623
či	či	k8xC	či
1624	[number]	k4	1624
vytouženého	vytoužený	k2eAgNnSc2d1	vytoužené
potomka	potomek	k1gMnSc4	potomek
dočkali	dočkat	k5eAaPmAgMnP	dočkat
<g/>
,	,	kIx,	,
nemohli	moct	k5eNaImAgMnP	moct
tušit	tušit	k5eAaImF	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
František	František	k1gMnSc1	František
Adam	Adam	k1gMnSc1	Adam
Eusebius	Eusebius	k1gMnSc1	Eusebius
druhý	druhý	k4xOgMnSc1	druhý
říšský	říšský	k2eAgMnSc1d1	říšský
hrabě	hrabě	k1gMnSc1	hrabě
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
zemře	zemřít	k5eAaPmIp3nS	zemřít
roku	rok	k1gInSc2	rok
1670	[number]	k4	1670
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
a	a	k8xC	a
odkáže	odkázat	k5eAaPmIp3nS	odkázat
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
kladensko-červenoújezdecké	kladensko-červenoújezdecký	k2eAgNnSc4d1	kladensko-červenoújezdecký
dominium	dominium	k1gNnSc4	dominium
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
42	[number]	k4	42
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
,	,	kIx,	,
vedlejší	vedlejší	k2eAgFnSc3d1	vedlejší
luteránské	luteránský	k2eAgFnSc3d1	luteránská
větvi	větev	k1gFnSc3	větev
rodu	rod	k1gInSc2	rod
Žďárských	Žďárských	k2eAgInSc2d1	Žďárských
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
žila	žít	k5eAaImAgFnS	žít
po	po	k7c6	po
prohraném	prohraný	k2eAgNnSc6d1	prohrané
stavovském	stavovský	k2eAgNnSc6d1	Stavovské
povstání	povstání	k1gNnSc6	povstání
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1621	[number]	k4	1621
pod	pod	k7c7	pod
poněmčeným	poněmčený	k2eAgNnSc7d1	poněmčené
jménem	jméno	k1gNnSc7	jméno
Sahrer	Sahrer	k1gInSc1	Sahrer
von	von	k1gInSc1	von
Sahr	Sahr	k1gInSc1	Sahr
v	v	k7c6	v
Sasku	Sasko	k1gNnSc6	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Zesnulý	zesnulý	k2eAgMnSc1d1	zesnulý
hrabě	hrabě	k1gMnSc1	hrabě
František	František	k1gMnSc1	František
Adam	Adam	k1gMnSc1	Adam
Eusebius	Eusebius	k1gMnSc1	Eusebius
převzetí	převzetí	k1gNnSc2	převzetí
dominia	dominion	k1gNnSc2	dominion
podmínil	podmínit	k5eAaPmAgMnS	podmínit
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
němečtí	německý	k2eAgMnPc1d1	německý
Žďárští	Žďárský	k1gMnPc1	Žďárský
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Sahrerové	Sahrerová	k1gFnSc6	Sahrerová
<g/>
)	)	kIx)	)
přestoupí	přestoupit	k5eAaPmIp3nS	přestoupit
na	na	k7c4	na
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
a	a	k8xC	a
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
usadí	usadit	k5eAaPmIp3nP	usadit
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Saské	saský	k2eAgNnSc4d1	Saské
příbuzenstvo	příbuzenstvo	k1gNnSc4	příbuzenstvo
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
podmínku	podmínka	k1gFnSc4	podmínka
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
a	a	k8xC	a
po	po	k7c6	po
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
letech	léto	k1gNnPc6	léto
sporů	spor	k1gInPc2	spor
bylo	být	k5eAaImAgNnS	být
smlouvou	smlouva	k1gFnSc7	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1688	[number]	k4	1688
vyplaceno	vyplatit	k5eAaPmNgNnS	vyplatit
slušnou	slušný	k2eAgFnSc7d1	slušná
částkou	částka	k1gFnSc7	částka
53	[number]	k4	53
000	[number]	k4	000
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
kladensko-červenoújezdecké	kladensko-červenoújezdecký	k2eAgNnSc1d1	kladensko-červenoújezdecký
panství	panství	k1gNnSc1	panství
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
pět	pět	k4xCc4	pět
(	(	kIx(	(
<g/>
doposud	doposud	k6eAd1	doposud
žijících	žijící	k2eAgFnPc2d1	žijící
<g/>
)	)	kIx)	)
sester	sestra	k1gFnPc2	sestra
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
hraběte	hrabě	k1gMnSc2	hrabě
Františka	František	k1gMnSc2	František
Adama	Adam	k1gMnSc2	Adam
Eusebia	Eusebius	k1gMnSc2	Eusebius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
Kročehlavy	Kročehlava	k1gFnPc1	Kročehlava
<g/>
,	,	kIx,	,
Újezd	Újezd	k1gInSc1	Újezd
pod	pod	k7c7	pod
Kladnem	Kladno	k1gNnSc7	Kladno
<g/>
,	,	kIx,	,
Dubí	dubí	k1gNnSc2	dubí
<g/>
,	,	kIx,	,
Hnidousy	hnidous	k1gMnPc4	hnidous
<g/>
,	,	kIx,	,
Motyčín	Motyčín	k1gInSc1	Motyčín
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
získala	získat	k5eAaPmAgFnS	získat
Marie	Marie	k1gFnSc1	Marie
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
Eva	Eva	k1gFnSc1	Eva
Terezie	Terezie	k1gFnSc1	Terezie
hraběnka	hraběnka	k1gFnSc1	hraběnka
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
(	(	kIx(	(
<g/>
1633	[number]	k4	1633
<g/>
–	–	k?	–
<g/>
1690	[number]	k4	1690
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
baronka	baronka	k1gFnSc1	baronka
Slavatová	Slavatový	k2eAgFnSc1d1	Slavatová
z	z	k7c2	z
Chlumu	chlum	k1gInSc2	chlum
a	a	k8xC	a
Košumberka	Košumberka	k1gFnSc1	Košumberka
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1656	[number]	k4	1656
<g/>
)	)	kIx)	)
Hýzrlová	Hýzrlová	k1gFnSc1	Hýzrlová
z	z	k7c2	z
Chodů	chod	k1gInPc2	chod
<g/>
.	.	kIx.	.
</s>
<s>
Hraběnka	hraběnka	k1gFnSc1	hraběnka
na	na	k7c6	na
Kladně	Kladno	k1gNnSc6	Kladno
nesídlila	sídlit	k5eNaImAgFnS	sídlit
a	a	k8xC	a
kladenské	kladenský	k2eAgNnSc1d1	kladenské
panství	panství	k1gNnSc1	panství
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zámkem	zámek	k1gInSc7	zámek
upadalo	upadat	k5eAaPmAgNnS	upadat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
i	i	k9	i
za	za	k7c2	za
jejich	jejich	k3xOp3gMnPc2	jejich
nástupců	nástupce	k1gMnPc2	nástupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
hraběnčině	hraběnčin	k2eAgFnSc6d1	hraběnčina
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
†	†	k?	†
1690	[number]	k4	1690
<g/>
)	)	kIx)	)
její	její	k3xOp3gMnPc1	její
dědicové	dědic	k1gMnPc1	dědic
–	–	k?	–
tj.	tj.	kA	tj.
její	její	k3xOp3gInPc4	její
vnoučata	vnouče	k1gNnPc1	vnouče
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
hrabat	hrabat	k5eAaImF	hrabat
z	z	k7c2	z
Lambergu	Lamberg	k1gInSc2	Lamberg
–	–	k?	–
upadající	upadající	k2eAgNnSc1d1	upadající
panství	panství	k1gNnSc1	panství
roku	rok	k1gInSc2	rok
1701	[number]	k4	1701
obratem	obrat	k1gInSc7	obrat
prodala	prodat	k5eAaPmAgFnS	prodat
za	za	k7c4	za
132	[number]	k4	132
000	[number]	k4	000
zlatých	zlatý	k1gInPc2	zlatý
Anně	Anna	k1gFnSc6	Anna
Marii	Maria	k1gFnSc3	Maria
Františce	Františka	k1gFnSc3	Františka
velkovévodkyni	velkovévodkyně	k1gFnSc3	velkovévodkyně
toskánské	toskánský	k2eAgFnSc2d1	Toskánská
(	(	kIx(	(
<g/>
1672	[number]	k4	1672
<g/>
–	–	k?	–
<g/>
1741	[number]	k4	1741
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
choť	choť	k1gFnSc1	choť
posledního	poslední	k2eAgMnSc2d1	poslední
velkovévody	velkovévoda	k1gMnSc2	velkovévoda
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
di	di	k?	di
Medici	medik	k1gMnPc1	medik
–	–	k?	–
Giana	Giana	k1gFnSc1	Giana
Gastona	Gastona	k1gFnSc1	Gastona
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
majitelkou	majitelka	k1gFnSc7	majitelka
velkých	velký	k2eAgNnPc2d1	velké
severočeských	severočeský	k2eAgNnPc2d1	Severočeské
panství	panství	k1gNnPc2	panství
a	a	k8xC	a
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
sousedního	sousední	k2eAgInSc2d1	sousední
buštěhradského	buštěhradský	k2eAgInSc2d1	buštěhradský
velkostatku	velkostatek	k1gInSc2	velkostatek
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
kladenské	kladenský	k2eAgFnPc4d1	kladenská
panství	panství	k1gNnPc2	panství
chtěla	chtít	k5eAaImAgFnS	chtít
připojit	připojit	k5eAaPmF	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
držení	držení	k1gNnSc6	držení
však	však	k9	však
Kladno	Kladno	k1gNnSc1	Kladno
nezůstalo	zůstat	k5eNaPmAgNnS	zůstat
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
velkovévodkyně	velkovévodkyně	k1gFnSc1	velkovévodkyně
nutně	nutně	k6eAd1	nutně
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
hotové	hotový	k2eAgInPc4d1	hotový
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
1705	[number]	k4	1705
koupili	koupit	k5eAaPmAgMnP	koupit
kladenské	kladenský	k2eAgNnSc4d1	kladenské
panství	panství	k1gNnSc4	panství
benediktini	benediktin	k1gMnPc1	benediktin
z	z	k7c2	z
bohatého	bohatý	k2eAgNnSc2d1	bohaté
a	a	k8xC	a
starobylého	starobylý	k2eAgNnSc2d1	starobylé
břevnovsko-broumovského	břevnovskoroumovský	k2eAgNnSc2d1	břevnovsko-broumovský
opatství	opatství	k1gNnSc2	opatství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
uměnímilovných	uměnímilovný	k2eAgMnPc2d1	uměnímilovný
břevnovských	břevnovský	k2eAgMnPc2d1	břevnovský
opatů	opat	k1gMnPc2	opat
Otmara	Otmar	k1gMnSc2	Otmar
Zinckeho	Zincke	k1gMnSc2	Zincke
a	a	k8xC	a
Benno	Benno	k1gNnSc1	Benno
Löbla	Löblo	k1gNnSc2	Löblo
se	se	k3xPyFc4	se
Kladno	Kladno	k1gNnSc1	Kladno
opět	opět	k6eAd1	opět
pozvedlo	pozvednout	k5eAaPmAgNnS	pozvednout
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
Benno	Benno	k6eAd1	Benno
Löbl	Löbl	k1gMnSc1	Löbl
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
věhlasným	věhlasný	k2eAgMnSc7d1	věhlasný
českým	český	k2eAgMnSc7d1	český
architektem	architekt	k1gMnSc7	architekt
Kiliánem	Kilián	k1gMnSc7	Kilián
Ignácem	Ignác	k1gMnSc7	Ignác
Dientzenhoferem	Dientzenhofer	k1gMnSc7	Dientzenhofer
vtiskli	vtisknout	k5eAaPmAgMnP	vtisknout
městu	město	k1gNnSc3	město
barokní	barokní	k2eAgFnSc1d1	barokní
tvář	tvář	k1gFnSc1	tvář
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgInPc1d1	patrný
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
radikálně	radikálně	k6eAd1	radikálně
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
a	a	k8xC	a
zmenšen	zmenšen	k2eAgInSc1d1	zmenšen
zchátralý	zchátralý	k2eAgInSc1d1	zchátralý
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
starší	starý	k2eAgFnSc1d2	starší
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgFnSc1d1	barokní
Floriánská	Floriánský	k2eAgFnSc1d1	Floriánská
kaple	kaple	k1gFnSc1	kaple
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
v	v	k7c4	v
elegantní	elegantní	k2eAgInPc4d1	elegantní
(	(	kIx(	(
<g/>
vrcholně	vrcholně	k6eAd1	vrcholně
barokní	barokní	k2eAgFnSc4d1	barokní
<g/>
)	)	kIx)	)
dvouvěžovou	dvouvěžový	k2eAgFnSc4d1	dvouvěžová
centrálu	centrála	k1gFnSc4	centrála
s	s	k7c7	s
kupolí	kupole	k1gFnSc7	kupole
a	a	k8xC	a
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
bylo	být	k5eAaImAgNnS	být
vztyčeno	vztyčen	k2eAgNnSc1d1	vztyčeno
velkolepé	velkolepý	k2eAgNnSc1d1	velkolepé
mariánské	mariánský	k2eAgNnSc1d1	Mariánské
sousoší	sousoší	k1gNnSc1	sousoší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
válek	válka	k1gFnPc2	válka
o	o	k7c4	o
rakouské	rakouský	k2eAgNnSc4d1	rakouské
dědictví	dědictví	k1gNnSc4	dědictví
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1741	[number]	k4	1741
a	a	k8xC	a
1742	[number]	k4	1742
břevnovští	břevnovský	k2eAgMnPc1d1	břevnovský
benediktinští	benediktinský	k2eAgMnPc1d1	benediktinský
mniši	mnich	k1gMnPc1	mnich
opakovaně	opakovaně	k6eAd1	opakovaně
uchylovali	uchylovat	k5eAaImAgMnP	uchylovat
před	před	k7c7	před
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
akcemi	akce	k1gFnPc7	akce
z	z	k7c2	z
exponovaného	exponovaný	k2eAgInSc2d1	exponovaný
kláštera	klášter	k1gInSc2	klášter
sv.	sv.	kA	sv.
Markéty	Markéta	k1gFnSc2	Markéta
v	v	k7c6	v
Břevnově	Břevnov	k1gInSc6	Břevnov
do	do	k7c2	do
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
odlehlé	odlehlý	k2eAgFnPc4d1	odlehlá
a	a	k8xC	a
utopené	utopený	k2eAgFnPc4d1	utopená
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1780	[number]	k4	1780
nechal	nechat	k5eAaPmAgMnS	nechat
opat	opat	k1gMnSc1	opat
Stephan	Stephan	k1gMnSc1	Stephan
Rautenstrauch	Rautenstrauch	k1gMnSc1	Rautenstrauch
založit	založit	k5eAaPmF	založit
dvě	dva	k4xCgFnPc4	dva
nové	nový	k2eAgFnPc4d1	nová
osady	osada	k1gFnPc4	osada
–	–	k?	–
Štěpánov	Štěpánov	k1gInSc1	Štěpánov
u	u	k7c2	u
Kročehlav	Kročehlav	k1gFnSc2	Kročehlav
a	a	k8xC	a
Rozdělov	Rozdělov	k1gInSc4	Rozdělov
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInSc4d1	ležící
západně	západně	k6eAd1	západně
od	od	k7c2	od
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Noví	nový	k2eAgMnPc1d1	nový
osadníci	osadník	k1gMnPc1	osadník
byli	být	k5eAaImAgMnP	být
povoláni	povolán	k2eAgMnPc1d1	povolán
z	z	k7c2	z
německy	německy	k6eAd1	německy
hovořícího	hovořící	k2eAgNnSc2d1	hovořící
Broumovska	Broumovsko	k1gNnSc2	Broumovsko
<g/>
.	.	kIx.	.
</s>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
prosperovalo	prosperovat	k5eAaImAgNnS	prosperovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
prahu	práh	k1gInSc6	práh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
jen	jen	k6eAd1	jen
malým	malý	k2eAgNnSc7d1	malé
bezvýznamným	bezvýznamný	k2eAgNnSc7d1	bezvýznamné
městečkem	městečko	k1gNnSc7	městečko
na	na	k7c6	na
řídce	řídce	k6eAd1	řídce
osídleném	osídlený	k2eAgInSc6d1	osídlený
zemědělském	zemědělský	k2eAgInSc6d1	zemědělský
venkově	venkov	k1gInSc6	venkov
<g/>
;	;	kIx,	;
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
postihl	postihnout	k5eAaPmAgInS	postihnout
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
jen	jen	k9	jen
kolem	kolem	k7c2	kolem
650	[number]	k4	650
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
město	město	k1gNnSc1	město
===	===	k?	===
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Kladně	Kladno	k1gNnSc6	Kladno
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
tehdy	tehdy	k6eAd1	tehdy
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
převoz	převoz	k1gInSc4	převoz
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Vedla	vést	k5eAaImAgFnS	vést
z	z	k7c2	z
Brusky	bruska	k1gFnSc2	bruska
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Praha-Dejvice	Praha-Dejvice	k1gFnPc4	Praha-Dejvice
<g/>
)	)	kIx)	)
na	na	k7c6	na
Vejhybku	Vejhybek	k1gInSc6	Vejhybek
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Kladno-Výhybka	Kladno-Výhybek	k1gMnSc2	Kladno-Výhybek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
železnici	železnice	k1gFnSc4	železnice
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
první	první	k4xOgInSc1	první
důl	důl	k1gInSc1	důl
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
ocelárny	ocelárna	k1gFnSc2	ocelárna
Poldi	Poldi	k1gFnSc2	Poldi
<g/>
.	.	kIx.	.
</s>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
center	centrum	k1gNnPc2	centrum
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
rostlo	růst	k5eAaImAgNnS	růst
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
rozlohou	rozloha	k1gFnSc7	rozloha
i	i	k8xC	i
významem	význam	k1gInSc7	význam
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgMnSc1d1	aktivní
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
dělnické	dělnický	k2eAgNnSc4d1	dělnické
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
zde	zde	k6eAd1	zde
ke	k	k7c3	k
stávkám	stávka	k1gFnPc3	stávka
a	a	k8xC	a
demonstracím	demonstrace	k1gFnPc3	demonstrace
a	a	k8xC	a
Kladno	Kladno	k1gNnSc1	Kladno
se	se	k3xPyFc4	se
také	také	k9	také
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
ohnisek	ohnisko	k1gNnPc2	ohnisko
českého	český	k2eAgNnSc2d1	české
komunistického	komunistický	k2eAgNnSc2d1	komunistické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
zde	zde	k6eAd1	zde
dvakrát	dvakrát	k6eAd1	dvakrát
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
studentského	studentský	k2eAgInSc2d1	studentský
spolku	spolek	k1gInSc2	spolek
přednášel	přednášet	k5eAaImAgMnS	přednášet
profesor	profesor	k1gMnSc1	profesor
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
socha	socha	k1gFnSc1	socha
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
jako	jako	k9	jako
prezidenta	prezident	k1gMnSc4	prezident
Osvoboditele	osvoboditel	k1gMnSc4	osvoboditel
<g/>
,	,	kIx,	,
také	také	k9	také
stanula	stanout	k5eAaPmAgFnS	stanout
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
legionářského	legionářský	k2eAgInSc2d1	legionářský
pomníku	pomník	k1gInSc2	pomník
v	v	k7c6	v
době	doba	k1gFnSc6	doba
První	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Hutnictví	hutnictví	k1gNnSc1	hutnictví
a	a	k8xC	a
těžký	těžký	k2eAgInSc1d1	těžký
průmysl	průmysl	k1gInSc1	průmysl
také	také	k9	také
znamenal	znamenat	k5eAaImAgInS	znamenat
poškození	poškození	k1gNnSc4	poškození
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ocele	ocel	k1gFnPc1	ocel
Poldi	Poldi	k1gFnSc2	Poldi
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
světoznámými	světoznámý	k2eAgInPc7d1	světoznámý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
socialismu	socialismus	k1gInSc2	socialismus
byl	být	k5eAaImAgInS	být
zdejší	zdejší	k2eAgInSc1d1	zdejší
průmysl	průmysl	k1gInSc1	průmysl
poškozen	poškodit	k5eAaPmNgInS	poškodit
neúspěšnou	úspěšný	k2eNgFnSc7d1	neúspěšná
privatizací	privatizace	k1gFnSc7	privatizace
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Stehlík	Stehlík	k1gMnSc1	Stehlík
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
skrze	skrze	k?	skrze
svou	svůj	k3xOyFgFnSc4	svůj
firmu	firma	k1gFnSc4	firma
Bohemia	bohemia	k1gFnSc1	bohemia
Art	Art	k1gMnPc2	Art
většinu	většina	k1gFnSc4	většina
ocelárny	ocelárna	k1gFnSc2	ocelárna
převzal	převzít	k5eAaPmAgMnS	převzít
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
podnik	podnik	k1gInSc1	podnik
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
znovu	znovu	k6eAd1	znovu
konkurenceschopný	konkurenceschopný	k2eAgInSc1d1	konkurenceschopný
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
však	však	k9	však
za	za	k7c2	za
podezřelých	podezřelý	k2eAgFnPc2d1	podezřelá
okolností	okolnost	k1gFnPc2	okolnost
dostal	dostat	k5eAaPmAgInS	dostat
podnik	podnik	k1gInSc1	podnik
ke	k	k7c3	k
krachu	krach	k1gInSc3	krach
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
ukončení	ukončení	k1gNnSc1	ukončení
řady	řada	k1gFnSc2	řada
provozů	provoz	k1gInPc2	provoz
a	a	k8xC	a
rozdělení	rozdělení	k1gNnSc4	rozdělení
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
s	s	k7c7	s
ekologickými	ekologický	k2eAgInPc7d1	ekologický
i	i	k8xC	i
ekonomickými	ekonomický	k2eAgInPc7d1	ekonomický
následky	následek	k1gInPc7	následek
tohoto	tento	k3xDgInSc2	tento
případu	případ	k1gInSc2	případ
potýkalo	potýkat	k5eAaImAgNnS	potýkat
ještě	ještě	k9	ještě
do	do	k7c2	do
nedávné	dávný	k2eNgFnSc2d1	nedávná
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
Kladno	Kladno	k1gNnSc1	Kladno
stabilizovalo	stabilizovat	k5eAaBmAgNnS	stabilizovat
a	a	k8xC	a
žádné	žádný	k3yNgInPc4	žádný
větší	veliký	k2eAgInPc4d2	veliký
problémy	problém	k1gInPc4	problém
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Primátorem	primátor	k1gMnSc7	primátor
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledků	výsledek	k1gInPc2	výsledek
komunálních	komunální	k2eAgFnPc2d1	komunální
voleb	volba	k1gFnPc2	volba
konaných	konaný	k2eAgFnPc2d1	konaná
ten	ten	k3xDgMnSc1	ten
rok	rok	k1gInSc4	rok
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
opoziční	opoziční	k2eAgFnSc2d1	opoziční
koaliční	koaliční	k2eAgFnSc2d1	koaliční
dohody	dohoda	k1gFnSc2	dohoda
(	(	kIx(	(
<g/>
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
Milan	Milan	k1gMnSc1	Milan
Volf	Volf	k1gMnSc1	Volf
s	s	k7c7	s
z	z	k7c2	z
uskupení	uskupení	k1gNnSc2	uskupení
Volba	volba	k1gFnSc1	volba
pro	pro	k7c4	pro
Kladno	Kladno	k1gNnSc4	Kladno
<g/>
)	)	kIx)	)
opět	opět	k6eAd1	opět
Dan	Dan	k1gMnSc1	Dan
Jiránek	Jiránek	k1gMnSc1	Jiránek
z	z	k7c2	z
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
již	již	k6eAd1	již
podruhé	podruhé	k6eAd1	podruhé
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Milana	Milan	k1gMnSc4	Milan
Volfa	Volf	k1gMnSc4	Volf
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
1740	[number]	k4	1740
leží	ležet	k5eAaImIp3nS	ležet
severně	severně	k6eAd1	severně
od	od	k7c2	od
náměstí	náměstí	k1gNnSc2	náměstí
starosty	starosta	k1gMnSc2	starosta
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgFnSc1d1	barokní
trojkřídlá	trojkřídlý	k2eAgFnSc1d1	trojkřídlá
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
starší	starý	k2eAgFnSc2d2	starší
budovy	budova	k1gFnSc2	budova
(	(	kIx(	(
<g/>
východní	východní	k2eAgNnSc1d1	východní
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
)	)	kIx)	)
s	s	k7c7	s
věžičkou	věžička	k1gFnSc7	věžička
<g/>
,	,	kIx,	,
snad	snad	k9	snad
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
K.	K.	kA	K.
I.	I.	kA	I.
Dienzenhofera	Dienzenhofer	k1gMnSc2	Dienzenhofer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
křídle	křídlo	k1gNnSc6	křídlo
je	být	k5eAaImIp3nS	být
oválná	oválný	k2eAgFnSc1d1	oválná
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
,	,	kIx,	,
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
náhrobní	náhrobní	k2eAgInPc1d1	náhrobní
kameny	kámen	k1gInPc1	kámen
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
ze	z	k7c2	z
staršího	starý	k2eAgInSc2d2	starší
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
patře	patro	k1gNnSc6	patro
kachlová	kachlový	k2eAgNnPc1d1	kachlové
kamna	kamna	k1gNnPc1	kamna
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
sídlo	sídlo	k1gNnSc4	sídlo
městské	městský	k2eAgFnSc2d1	městská
galerie	galerie	k1gFnSc2	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
zámkem	zámek	k1gInSc7	zámek
park	park	k1gInSc4	park
obehnaný	obehnaný	k2eAgInSc4d1	obehnaný
zdí	zeď	k1gFnSc7	zeď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc2	Florián
na	na	k7c6	na
Floriánském	Floriánský	k2eAgNnSc6d1	Floriánské
náměstí	náměstí	k1gNnSc6	náměstí
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1751	[number]	k4	1751
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
K.	K.	kA	K.
I.	I.	kA	I.
Dienzenhofera	Dienzenhofer	k1gMnSc2	Dienzenhofer
<g/>
.	.	kIx.	.
</s>
<s>
Pozdně	pozdně	k6eAd1	pozdně
barokní	barokní	k2eAgFnSc1d1	barokní
centrální	centrální	k2eAgFnSc1d1	centrální
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
stavba	stavba	k1gFnSc1	stavba
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
apsidami	apsida	k1gFnPc7	apsida
<g/>
,	,	kIx,	,
kopulí	kopule	k1gFnSc7	kopule
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
na	na	k7c4	na
koso	kosa	k1gFnSc5	kosa
postavenými	postavený	k2eAgFnPc7d1	postavená
věžemi	věž	k1gFnPc7	věž
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
průčelí	průčelí	k1gNnSc6	průčelí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
na	na	k7c6	na
Náměstí	náměstí	k1gNnSc6	náměstí
starosty	starosta	k1gMnSc2	starosta
Pavla	Pavel	k1gMnSc2	Pavel
od	od	k7c2	od
K.	K.	kA	K.
J.	J.	kA	J.
Hiernleho	Hiernle	k1gMnSc4	Hiernle
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
K.	K.	kA	K.
I.	I.	kA	I.
Dienzenhofera	Dienzenhofer	k1gMnSc2	Dienzenhofer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
synagoga	synagoga	k1gFnSc1	synagoga
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
kostel	kostel	k1gInSc1	kostel
CČH	CČH	kA	CČH
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
plk.	plk.	kA	plk.
Stříbrného	stříbrný	k1gInSc2	stříbrný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Radnice	radnice	k1gFnSc1	radnice
na	na	k7c6	na
Náměstí	náměstí	k1gNnSc6	náměstí
starosty	starosta	k1gMnSc2	starosta
Pavla	Pavel	k1gMnSc2	Pavel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
J.	J.	kA	J.
Vejrycha	Vejrycha	k1gFnSc1	Vejrycha
s	s	k7c7	s
malbami	malba	k1gFnPc7	malba
A.	A.	kA	A.
Liebschera	Liebschero	k1gNnSc2	Liebschero
na	na	k7c4	na
průčelí	průčelí	k1gNnSc4	průčelí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děkanství	děkanství	k1gNnSc1	děkanství
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
<g/>
,	,	kIx,	,
prostá	prostý	k2eAgFnSc1d1	prostá
patrová	patrový	k2eAgFnSc1d1	patrová
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
mansardovou	mansardový	k2eAgFnSc7d1	mansardová
střechou	střecha	k1gFnSc7	střecha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
novorománský	novorománský	k2eAgInSc1d1	novorománský
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Ludvíka	Ludvík	k1gMnSc4	Ludvík
Láblera	Lábler	k1gMnSc4	Lábler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
lékárna	lékárna	k1gFnSc1	lékárna
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
novorenesanční	novorenesanční	k2eAgFnSc1d1	novorenesanční
se	s	k7c7	s
sgrafity	sgrafito	k1gNnPc7	sgrafito
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Mikoláše	Mikoláš	k1gMnSc2	Mikoláš
Alše	Aleš	k1gMnSc2	Aleš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
a	a	k8xC	a
tiskárna	tiskárna	k1gFnSc1	tiskárna
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Šnajdra	Šnajdr	k1gMnSc2	Šnajdr
<g/>
,	,	kIx,	,
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
Saskova	Saskův	k2eAgFnSc1d1	Saskova
ulice	ulice	k1gFnSc1	ulice
1495	[number]	k4	1495
<g/>
,	,	kIx,	,
návrh	návrh	k1gInSc4	návrh
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rössler	Rössler	k1gMnSc1	Rössler
(	(	kIx(	(
<g/>
architekt	architekt	k1gMnSc1	architekt
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Rösslera	Rössler	k1gMnSc2	Rössler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Secesní	secesní	k2eAgInSc1d1	secesní
Okresní	okresní	k2eAgInSc1d1	okresní
dům	dům	k1gInSc1	dům
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
architektů	architekt	k1gMnPc2	architekt
Josefa	Josef	k1gMnSc2	Josef
Maříka	Mařík	k1gMnSc2	Mařík
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Šidlíka	Šidlík	k1gMnSc2	Šidlík
s	s	k7c7	s
přednáškovou	přednáškový	k2eAgFnSc7d1	přednášková
síní	síň	k1gFnSc7	síň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
sídlo	sídlo	k1gNnSc1	sídlo
Středočeské	středočeský	k2eAgFnSc2d1	Středočeská
vědecké	vědecký	k2eAgFnSc2d1	vědecká
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
1	[number]	k4	1
828	[number]	k4	828
domech	dům	k1gInPc6	dům
19	[number]	k4	19
111	[number]	k4	111
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
9	[number]	k4	9
944	[number]	k4	944
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
18	[number]	k4	18
289	[number]	k4	289
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
551	[number]	k4	551
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
a	a	k8xC	a
7	[number]	k4	7
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
10	[number]	k4	10
877	[number]	k4	877
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
315	[number]	k4	315
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
523	[number]	k4	523
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
257	[number]	k4	257
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
2	[number]	k4	2
391	[number]	k4	391
domech	dům	k1gInPc6	dům
20	[number]	k4	20
751	[number]	k4	751
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
19	[number]	k4	19
705	[number]	k4	705
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
789	[number]	k4	789
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
11	[number]	k4	11
981	[number]	k4	981
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
492	[number]	k4	492
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
1702	[number]	k4	1702
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
210	[number]	k4	210
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Školství	školství	k1gNnSc2	školství
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Kladně	Kladno	k1gNnSc6	Kladno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
19	[number]	k4	19
škol	škola	k1gFnPc2	škola
základních	základní	k2eAgMnPc2d1	základní
<g/>
,	,	kIx,	,
13	[number]	k4	13
středních	střední	k1gMnPc2	střední
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2	[number]	k4	2
gymnázia	gymnázium	k1gNnPc4	gymnázium
<g/>
,	,	kIx,	,
3	[number]	k4	3
vyšší	vysoký	k2eAgFnPc1d2	vyšší
odborné	odborný	k2eAgFnPc1d1	odborná
a	a	k8xC	a
2	[number]	k4	2
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Firmy	firma	k1gFnPc1	firma
==	==	k?	==
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
nkt	nkt	k?	nkt
cables	cables	k1gMnSc1	cables
dnes	dnes	k6eAd1	dnes
působí	působit	k5eAaImIp3nS	působit
výrobce	výrobce	k1gMnSc4	výrobce
kabelů	kabel	k1gInPc2	kabel
Kablo	Kablo	k1gNnSc4	Kablo
Kladno	Kladno	k1gNnSc1	Kladno
s	s	k7c7	s
historií	historie	k1gFnSc7	historie
sahající	sahající	k2eAgFnSc7d1	sahající
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
<s>
Pokračovatelem	pokračovatel	k1gMnSc7	pokračovatel
tradice	tradice	k1gFnSc2	tradice
značky	značka	k1gFnSc2	značka
Poldi	Poldi	k1gFnSc2	Poldi
jsou	být	k5eAaImIp3nP	být
Strojírny	strojírna	k1gFnPc1	strojírna
Poldi	Poldi	k1gFnSc2	Poldi
(	(	kIx(	(
<g/>
hřídele	hřídel	k1gFnSc2	hřídel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výrobní	výrobní	k2eAgInSc4d1	výrobní
program	program	k1gInSc4	program
někdejší	někdejší	k2eAgFnSc2d1	někdejší
Poldi	Poldi	k1gFnSc2	Poldi
navazují	navazovat	k5eAaImIp3nP	navazovat
též	též	k9	též
Třinecké	třinecký	k2eAgFnPc1d1	třinecká
železárny	železárna	k1gFnPc1	železárna
(	(	kIx(	(
<g/>
sochorová	sochorový	k2eAgFnSc1d1	Sochorová
válcovna	válcovna	k1gFnSc1	válcovna
Dříň	Dříň	k1gFnSc1	Dříň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
POLDI	Poldi	k1gMnSc5	Poldi
Hütte	Hütt	k1gMnSc5	Hütt
(	(	kIx(	(
<g/>
ocelárna	ocelárna	k1gFnSc1	ocelárna
<g/>
,	,	kIx,	,
kovárna	kovárna	k1gFnSc1	kovárna
<g/>
)	)	kIx)	)
a	a	k8xC	a
Beznoska	beznoska	k1gFnSc1	beznoska
(	(	kIx(	(
<g/>
chirurgické	chirurgický	k2eAgInPc1d1	chirurgický
nástroje	nástroj	k1gInPc1	nástroj
a	a	k8xC	a
implantáty	implantát	k1gInPc1	implantát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
zóně	zóna	k1gFnSc6	zóna
Kladno-jih	Kladnoiha	k1gFnPc2	Kladno-jiha
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Velkému	velký	k2eAgNnSc3d1	velké
Přítočnu	Přítočno	k1gNnSc3	Přítočno
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
výrobních	výrobní	k2eAgInPc2d1	výrobní
závodů	závod	k1gInPc2	závod
dánské	dánský	k2eAgFnSc2d1	dánská
hračkářské	hračkářský	k2eAgFnSc2d1	hračkářská
společnosti	společnost	k1gFnSc2	společnost
Lego	lego	k1gNnSc1	lego
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
volný	volný	k2eAgInSc1d1	volný
čas	čas	k1gInSc1	čas
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c4	o
sportovní	sportovní	k2eAgNnSc4d1	sportovní
a	a	k8xC	a
volnočasové	volnočasový	k2eAgNnSc4d1	volnočasové
využití	využití	k1gNnSc4	využití
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
společnost	společnost	k1gFnSc4	společnost
Sportovní	sportovní	k2eAgInPc1d1	sportovní
areály	areál	k1gInPc1	areál
města	město	k1gNnSc2	město
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
samotném	samotný	k2eAgNnSc6d1	samotné
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
sportovních	sportovní	k2eAgInPc2d1	sportovní
areálů	areál	k1gInPc2	areál
<g/>
,	,	kIx,	,
bazénů	bazén	k1gInPc2	bazén
<g/>
,	,	kIx,	,
specializovaných	specializovaný	k2eAgNnPc2d1	specializované
hřišť	hřiště	k1gNnPc2	hřiště
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
míst	místo	k1gNnPc2	místo
pro	pro	k7c4	pro
sport	sport	k1gInSc4	sport
a	a	k8xC	a
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Aquapark	aquapark	k1gInSc1	aquapark
Kladno	Kladno	k1gNnSc1	Kladno
</s>
</p>
<p>
<s>
Letní	letní	k2eAgNnSc1d1	letní
koupaliště	koupaliště	k1gNnSc1	koupaliště
u	u	k7c2	u
Sletiště	sletiště	k1gNnSc2	sletiště
</s>
</p>
<p>
<s>
Letní	letní	k2eAgNnSc1d1	letní
koupaliště	koupaliště	k1gNnSc1	koupaliště
Bažantnice	bažantnice	k1gFnSc2	bažantnice
</s>
</p>
<p>
<s>
Trampolínový	Trampolínový	k2eAgInSc1d1	Trampolínový
park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
u	u	k7c2	u
letního	letní	k2eAgNnSc2d1	letní
koupaliště	koupaliště	k1gNnSc2	koupaliště
u	u	k7c2	u
Sletiště	sletiště	k1gNnSc2	sletiště
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Krytý	krytý	k2eAgInSc1d1	krytý
bazén	bazén	k1gInSc1	bazén
při	při	k7c6	při
ZŠ	ZŠ	kA	ZŠ
v	v	k7c6	v
Norské	norský	k2eAgFnSc6d1	norská
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
</s>
</p>
<p>
<s>
Stadion	stadion	k1gInSc1	stadion
Františka	František	k1gMnSc2	František
Kloze	Kloze	k1gFnSc2	Kloze
</s>
</p>
<p>
<s>
Kladenská	kladenský	k2eAgFnSc1d1	kladenská
sportovní	sportovní	k2eAgFnSc1d1	sportovní
hala	hala	k1gFnSc1	hala
</s>
</p>
<p>
<s>
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
hala	hala	k1gFnSc1	hala
BIOS	BIOS	kA	BIOS
</s>
</p>
<p>
<s>
Přetlaková	přetlakový	k2eAgFnSc1d1	přetlaková
sportovní	sportovní	k2eAgFnSc1d1	sportovní
hala	hala	k1gFnSc1	hala
při	při	k7c6	při
ZŠ	ZŠ	kA	ZŠ
v	v	k7c6	v
Brjanské	Brjanský	k2eAgFnSc6d1	Brjanská
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
</s>
</p>
<p>
<s>
In-line	Inin	k1gInSc5	In-lin
dráha	dráha	k1gFnSc1	dráha
</s>
</p>
<p>
<s>
Městská	městský	k2eAgFnSc1d1	městská
hokejbalová	hokejbalový	k2eAgFnSc1d1	hokejbalová
aréna	aréna	k1gFnSc1	aréna
</s>
</p>
<p>
<s>
Městský	městský	k2eAgInSc1d1	městský
stadion	stadion	k1gInSc1	stadion
Sletiště	sletiště	k1gNnSc2	sletiště
</s>
</p>
<p>
<s>
ČEZ	ČEZ	kA	ČEZ
stadion	stadion	k1gInSc1	stadion
Kladno	Kladno	k1gNnSc1	Kladno
</s>
</p>
<p>
<s>
Víceúčelový	víceúčelový	k2eAgInSc1d1	víceúčelový
sportovní	sportovní	k2eAgInSc1d1	sportovní
areál	areál	k1gInSc1	areál
</s>
</p>
<p>
<s>
Finská	finský	k2eAgFnSc1d1	finská
sauna	sauna	k1gFnSc1	sauna
</s>
</p>
<p>
<s>
Turistická	turistický	k2eAgFnSc1d1	turistická
ubytovna	ubytovna	k1gFnSc1	ubytovna
(	(	kIx(	(
<g/>
za	za	k7c7	za
Aquaparkem	aquapark	k1gInSc7	aquapark
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tenisové	tenisový	k2eAgFnPc1d1	tenisová
kurty	kurta	k1gFnPc1	kurta
SK	Sk	kA	Sk
</s>
</p>
<p>
<s>
Lanový	lanový	k2eAgInSc1d1	lanový
park	park	k1gInSc1	park
</s>
</p>
<p>
<s>
Bowlingové	Bowlingový	k2eAgNnSc1d1	Bowlingové
centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
OC	OC	kA	OC
Central	Central	k1gFnSc1	Central
<g/>
)	)	kIx)	)
<g/>
Městský	městský	k2eAgInSc1d1	městský
stadion	stadion	k1gInSc1	stadion
Sletiště	sletiště	k1gNnSc2	sletiště
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
hostil	hostit	k5eAaImAgMnS	hostit
několik	několik	k4yIc4	několik
mistrovství	mistrovství	k1gNnPc2	mistrovství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
desetiboji	desetiboj	k1gInSc6	desetiboj
a	a	k8xC	a
atletických	atletický	k2eAgInPc2d1	atletický
mítinků	mítink	k1gInPc2	mítink
<g/>
;	;	kIx,	;
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
každoročně	každoročně	k6eAd1	každoročně
pořádá	pořádat	k5eAaImIp3nS	pořádat
vícebojařský	vícebojařský	k2eAgInSc1d1	vícebojařský
TNT	TNT	kA	TNT
Express	express	k1gInSc1	express
Meeting	meeting	k1gInSc1	meeting
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Sletiště	sletiště	k1gNnSc2	sletiště
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
několika	několik	k4yIc6	několik
letech	let	k1gInPc6	let
zbudována	zbudován	k2eAgFnSc1d1	zbudována
síť	síť	k1gFnSc1	síť
cyklostezek	cyklostezka	k1gFnPc2	cyklostezka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
též	též	k9	též
registruje	registrovat	k5eAaBmIp3nS	registrovat
skoro	skoro	k6eAd1	skoro
tři	tři	k4xCgFnPc1	tři
desítky	desítka	k1gFnPc1	desítka
sportovních	sportovní	k2eAgInPc2d1	sportovní
klubů	klub	k1gInPc2	klub
<g/>
:	:	kIx,	:
aerobic	aerobic	k1gInSc1	aerobic
<g/>
,	,	kIx,	,
alkampf	alkampf	k1gInSc1	alkampf
–	–	k?	–
jitsu	jits	k1gInSc2	jits
<g/>
,	,	kIx,	,
atletika	atletika	k1gFnSc1	atletika
<g/>
,	,	kIx,	,
badminton	badminton	k1gInSc1	badminton
<g/>
,	,	kIx,	,
basketbal	basketbal	k1gInSc1	basketbal
<g/>
,	,	kIx,	,
basball	basball	k1gMnSc1	basball
<g/>
,	,	kIx,	,
bojové	bojový	k2eAgInPc4d1	bojový
sporty	sport	k1gInPc4	sport
<g/>
,	,	kIx,	,
curling	curling	k1gInSc1	curling
<g/>
,	,	kIx,	,
cyklistika	cyklistika	k1gFnSc1	cyklistika
<g/>
,	,	kIx,	,
florbal	florbal	k1gInSc1	florbal
<g/>
,	,	kIx,	,
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
futsal	futsat	k5eAaImAgMnS	futsat
<g/>
,	,	kIx,	,
hokejbal	hokejbal	k1gInSc1	hokejbal
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
jiu-jitsu	jiuits	k1gInSc6	jiu-jits
<g/>
,	,	kIx,	,
judo	judo	k1gNnSc1	judo
<g/>
,	,	kIx,	,
kanoistika	kanoistika	k1gFnSc1	kanoistika
<g/>
,	,	kIx,	,
karate	karate	k1gNnSc1	karate
<g/>
,	,	kIx,	,
kick	kick	k1gInSc1	kick
–	–	k?	–
box	box	k1gInSc1	box
<g/>
,	,	kIx,	,
krasobruslení	krasobruslení	k1gNnSc1	krasobruslení
<g/>
,	,	kIx,	,
plavání	plavání	k1gNnSc1	plavání
záchranářů	záchranář	k1gMnPc2	záchranář
<g/>
,	,	kIx,	,
squash	squash	k1gInSc1	squash
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgNnSc1d1	sportovní
plavání	plavání	k1gNnSc1	plavání
<g/>
,	,	kIx,	,
stolní	stolní	k2eAgInSc1d1	stolní
tenis	tenis	k1gInSc1	tenis
<g/>
,	,	kIx,	,
tenis	tenis	k1gInSc1	tenis
<g/>
,	,	kIx,	,
tanec	tanec	k1gInSc1	tanec
<g/>
,	,	kIx,	,
volejbal	volejbal	k1gInSc1	volejbal
a	a	k8xC	a
všeobecné	všeobecný	k2eAgNnSc1d1	všeobecné
plavání	plavání	k1gNnSc1	plavání
dětí	dítě	k1gFnPc2	dítě
od	od	k7c2	od
7	[number]	k4	7
do	do	k7c2	do
11	[number]	k4	11
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
některé	některý	k3yIgFnPc4	některý
sportovní	sportovní	k2eAgFnPc4d1	sportovní
haly	hala	k1gFnPc4	hala
(	(	kIx(	(
<g/>
např.	např.	kA	např.
squash	squash	k1gInSc1	squash
na	na	k7c6	na
Sítné	Sítná	k1gFnSc6	Sítná
<g/>
)	)	kIx)	)
a	a	k8xC	a
hřiště	hřiště	k1gNnSc1	hřiště
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
soukromníků	soukromník	k1gMnPc2	soukromník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hokej	hokej	k1gInSc1	hokej
zde	zde	k6eAd1	zde
na	na	k7c6	na
kladenském	kladenský	k2eAgInSc6d1	kladenský
domácím	domácí	k2eAgInSc6d1	domácí
hokejovém	hokejový	k2eAgInSc6d1	hokejový
stadionu	stadion	k1gInSc6	stadion
ČEZ	ČEZ	kA	ČEZ
stadion	stadion	k1gInSc1	stadion
Kladno	Kladno	k1gNnSc1	Kladno
hraje	hrát	k5eAaImIp3nS	hrát
klub	klub	k1gInSc4	klub
Rytíři	Rytíř	k1gMnPc7	Rytíř
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozloha	rozloha	k1gFnSc1	rozloha
==	==	k?	==
</s>
</p>
<p>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
podobě	podoba	k1gFnSc6	podoba
je	být	k5eAaImIp3nS	být
aglomerací	aglomerace	k1gFnSc7	aglomerace
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
prošly	projít	k5eAaPmAgInP	projít
prudkým	prudký	k2eAgInSc7d1	prudký
rozvojem	rozvoj	k1gInSc7	rozvoj
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
Újezd	Újezd	k1gInSc1	Újezd
pod	pod	k7c7	pod
Kladnem	Kladno	k1gNnSc7	Kladno
<g/>
,	,	kIx,	,
Dubí	dubí	k1gNnSc1	dubí
<g/>
,	,	kIx,	,
Dříň	Dříň	k1gFnSc1	Dříň
<g/>
,	,	kIx,	,
Vrapice	Vrapice	k1gFnSc1	Vrapice
<g/>
,	,	kIx,	,
Kročehlavy	Kročehlava	k1gFnPc1	Kročehlava
<g/>
,	,	kIx,	,
Štěpánov	Štěpánov	k1gInSc1	Štěpánov
<g/>
,	,	kIx,	,
Rozdělov	Rozdělov	k1gInSc1	Rozdělov
<g/>
,	,	kIx,	,
Motyčín	Motyčín	k1gInSc1	Motyčín
<g/>
,	,	kIx,	,
Hnidousy	hnidous	k1gMnPc4	hnidous
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
6	[number]	k4	6
částí	část	k1gFnPc2	část
a	a	k8xC	a
7	[number]	k4	7
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
(	(	kIx(	(
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
základní	základní	k2eAgFnPc4d1	základní
sídelní	sídelní	k2eAgFnPc4d1	sídelní
jednotky	jednotka	k1gFnPc4	jednotka
Kladno-střed	Kladnotřed	k1gInSc4	Kladno-střed
<g/>
,	,	kIx,	,
Bresson	Bresson	k1gInSc1	Bresson
<g/>
,	,	kIx,	,
Cikánka	cikánka	k1gFnSc1	cikánka
<g/>
,	,	kIx,	,
Habešovna	Habešovna	k1gFnSc1	Habešovna
<g/>
,	,	kIx,	,
Nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
Ostrovec	Ostrovec	k1gMnSc1	Ostrovec
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
Bezruče	Bezruč	k1gFnSc2	Bezruč
<g/>
,	,	kIx,	,
Podprůhon	Podprůhon	k1gMnSc1	Podprůhon
<g/>
,	,	kIx,	,
Rozdělov-jih	Rozdělovih	k1gMnSc1	Rozdělov-jih
<g/>
,	,	kIx,	,
Rozdělov-sever	Rozdělovever	k1gMnSc1	Rozdělov-sever
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna-západ	zónaápad	k6eAd1	zóna-západ
<g/>
,	,	kIx,	,
Štěpánka	Štěpánka	k1gFnSc1	Štěpánka
<g/>
,	,	kIx,	,
U	u	k7c2	u
zimního	zimní	k2eAgInSc2d1	zimní
stadionu	stadion	k1gInSc2	stadion
<g/>
,	,	kIx,	,
Západní	západní	k2eAgInPc1d1	západní
lesy	les	k1gInPc1	les
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rozdělov	Rozdělov	k1gInSc1	Rozdělov
(	(	kIx(	(
<g/>
zahrhuje	zahrhovat	k5eAaImIp3nS	zahrhovat
ZSJ	ZSJ	kA	ZSJ
Starý	starý	k2eAgInSc1d1	starý
Rozdělov	Rozdělov	k1gInSc1	Rozdělov
<g/>
,	,	kIx,	,
Lapák	lapák	k1gInSc1	lapák
<g/>
,	,	kIx,	,
Bílé	bílý	k2eAgInPc1d1	bílý
vršky	vršek	k1gInPc1	vršek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kročehlavy	Kročehlava	k1gFnPc1	Kročehlava
(	(	kIx(	(
<g/>
ZSJ	ZSJ	kA	ZSJ
Staré	Stará	k1gFnSc2	Stará
Kročehlavy	Kročehlava	k1gFnSc2	Kročehlava
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgInSc1d1	Staré
Kročehlavy-sever	Kročehlavyever	k1gInSc1	Kročehlavy-sever
<g/>
,	,	kIx,	,
Bažantnice	bažantnice	k1gFnSc1	bažantnice
<g/>
,	,	kIx,	,
K	k	k7c3	k
Přítočnu	Přítočno	k1gNnSc3	Přítočno
<g/>
,	,	kIx,	,
Milíř	milíř	k1gInSc1	milíř
<g/>
,	,	kIx,	,
Nádraží	nádraží	k1gNnPc1	nádraží
<g/>
,	,	kIx,	,
Norská	norský	k2eAgNnPc1d1	norské
<g/>
,	,	kIx,	,
Sídliště	sídliště	k1gNnSc1	sídliště
Sítná	Sítná	k1gFnSc1	Sítná
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna-jih	zónaih	k1gInSc1	zóna-jih
<g/>
,	,	kIx,	,
U	u	k7c2	u
Bažantnice	bažantnice	k1gFnSc2	bažantnice
<g/>
,	,	kIx,	,
U	u	k7c2	u
Hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
U	u	k7c2	u
Kročehlavského	Kročehlavský	k2eAgInSc2d1	Kročehlavský
rybníka	rybník	k1gInSc2	rybník
<g/>
,	,	kIx,	,
U	u	k7c2	u
masokombinátu	masokombinát	k1gInSc2	masokombinát
<g/>
,	,	kIx,	,
u	u	k7c2	u
Pražské	pražský	k2eAgFnSc2d1	Pražská
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
U	u	k7c2	u
rozvodny	rozvodna	k1gFnSc2	rozvodna
<g/>
,	,	kIx,	,
U	u	k7c2	u
tržnice	tržnice	k1gFnSc2	tržnice
<g/>
,	,	kIx,	,
Václavské	václavský	k2eAgNnSc1d1	Václavské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
Výhybka	výhybka	k1gFnSc1	výhybka
<g/>
,	,	kIx,	,
Zabitý	zabitý	k1gMnSc1	zabitý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dubí	dubí	k1gNnSc1	dubí
–	–	k?	–
k.	k.	k?	k.
ú.	ú.	k?	ú.
Dubí	dubí	k1gNnSc1	dubí
u	u	k7c2	u
Kladna	Kladno	k1gNnSc2	Kladno
(	(	kIx(	(
<g/>
ZSJ	ZSJ	kA	ZSJ
Dubí	dubí	k1gNnSc1	dubí
<g/>
,	,	kIx,	,
Újezd	Újezd	k1gInSc1	Újezd
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna-sever	zónaever	k1gInSc1	zóna-sever
<g/>
,	,	kIx,	,
Na	na	k7c6	na
vysokém	vysoký	k2eAgInSc6d1	vysoký
<g/>
,	,	kIx,	,
U	U	kA	U
Jana	Jana	k1gFnSc1	Jana
<g/>
,	,	kIx,	,
Dříň	Dříň	k1gFnSc1	Dříň
<g/>
,	,	kIx,	,
Dříň-průmyslový	Dříňrůmyslový	k2eAgInSc1d1	Dříň-průmyslový
obvod	obvod	k1gInSc1	obvod
<g/>
,	,	kIx,	,
Dlouhé	Dlouhé	k2eAgFnPc1d1	Dlouhé
boroviny	borovina	k1gFnPc1	borovina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vrapice	Vrapice	k1gFnSc1	Vrapice
(	(	kIx(	(
<g/>
ZSJ	ZSJ	kA	ZSJ
Vrapice	Vrapika	k1gFnSc6	Vrapika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Švermov	Švermov	k1gInSc1	Švermov
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Motyčín	Motyčín	k1gMnSc1	Motyčín
(	(	kIx(	(
<g/>
ZSJ	ZSJ	kA	ZSJ
Motyčín	Motyčín	k1gInSc1	Motyčín
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Hnidousy	hnidous	k1gMnPc7	hnidous
(	(	kIx(	(
<g/>
ZSJ	ZSJ	kA	ZSJ
Hnidousy	hnidous	k1gMnPc4	hnidous
<g/>
,	,	kIx,	,
Hnidousy-jih	Hnidousyih	k1gInSc1	Hnidousy-jih
<g/>
,	,	kIx,	,
U	u	k7c2	u
dolu	dol	k1gInSc2	dol
Ronna	Ronn	k1gInSc2	Ronn
<g/>
,	,	kIx,	,
Za	za	k7c7	za
borkem	borek	k1gInSc7	borek
<g/>
)	)	kIx)	)
<g/>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	on	k3xPp3gNnSc4	on
Kladno	Kladno	k1gNnSc4	Kladno
statutárním	statutární	k2eAgNnSc7d1	statutární
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
nečlení	členit	k5eNaImIp3nP	členit
se	se	k3xPyFc4	se
na	na	k7c6	na
samosprávné	samosprávný	k2eAgFnSc6d1	samosprávná
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
ani	ani	k8xC	ani
městské	městský	k2eAgInPc1d1	městský
obvody	obvod	k1gInPc1	obvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
po	po	k7c6	po
Pardubicích	Pardubice	k1gInPc6	Pardubice
o	o	k7c4	o
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
české	český	k2eAgNnSc4d1	české
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
domy	dům	k1gInPc1	dům
neměly	mít	k5eNaImAgInP	mít
přidělena	přidělen	k2eAgNnPc4d1	přiděleno
orientační	orientační	k2eAgNnPc4d1	orientační
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Správní	správní	k2eAgNnSc4d1	správní
území	území	k1gNnSc4	území
a	a	k8xC	a
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
územních	územní	k2eAgInPc6d1	územní
celcích	celek	k1gInPc6	celek
==	==	k?	==
</s>
</p>
<p>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
kromě	kromě	k7c2	kromě
působnosti	působnost	k1gFnSc2	působnost
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
je	být	k5eAaImIp3nS	být
též	též	k9	též
obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Kladno	Kladno	k1gNnSc1	Kladno
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
100	[number]	k4	100
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
ORP	ORP	kA	ORP
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
48	[number]	k4	48
obcí	obec	k1gFnPc2	obec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
existencí	existence	k1gFnSc7	existence
však	však	k9	však
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
spjata	spjat	k2eAgFnSc1d1	spjata
žádná	žádný	k3yNgFnSc1	žádný
působnost	působnost	k1gFnSc1	působnost
obecné	obecný	k2eAgFnSc2d1	obecná
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1	specifické
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kladno	Kladno	k1gNnSc1	Kladno
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc7	jeho
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
krajským	krajský	k2eAgNnSc7d1	krajské
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
však	však	k9	však
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Územněsprávní	územněsprávní	k2eAgNnSc4d1	územněsprávní
začlenění	začlenění	k1gNnSc4	začlenění
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
územněsprávního	územněsprávní	k2eAgNnSc2d1	územněsprávní
začleňování	začleňování	k1gNnSc2	začleňování
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
období	období	k1gNnSc4	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chronologickém	chronologický	k2eAgInSc6d1	chronologický
přehledu	přehled	k1gInSc6	přehled
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
územně	územně	k6eAd1	územně
administrativní	administrativní	k2eAgFnSc4d1	administrativní
příslušnost	příslušnost	k1gFnSc4	příslušnost
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1850	[number]	k4	1850
<g/>
:	:	kIx,	:
země	zem	k1gFnPc1	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
okres	okres	k1gInSc1	okres
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Unhošť	Unhošť	k1gFnSc1	Unhošť
</s>
</p>
<p>
<s>
1855	[number]	k4	1855
<g/>
:	:	kIx,	:
země	zem	k1gFnPc1	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Unhošť	Unhošť	k1gFnSc1	Unhošť
</s>
</p>
<p>
<s>
1868	[number]	k4	1868
<g/>
:	:	kIx,	:
země	zem	k1gFnPc1	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
okres	okres	k1gInSc1	okres
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Unhošť	Unhošť	k1gFnSc1	Unhošť
</s>
</p>
<p>
<s>
1878	[number]	k4	1878
<g/>
:	:	kIx,	:
země	zem	k1gFnPc1	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
okres	okres	k1gInSc1	okres
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kladno	Kladno	k1gNnSc1	Kladno
</s>
</p>
<p>
<s>
1893	[number]	k4	1893
<g/>
:	:	kIx,	:
země	zem	k1gFnPc1	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kladno	Kladno	k1gNnSc1	Kladno
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
<g/>
:	:	kIx,	:
země	zem	k1gFnPc1	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gMnSc1	Oberlandrat
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kladno	Kladno	k1gNnSc1	Kladno
</s>
</p>
<p>
<s>
1942	[number]	k4	1942
<g/>
:	:	kIx,	:
země	zem	k1gFnPc1	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kladno	Kladno	k1gNnSc1	Kladno
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
<g/>
:	:	kIx,	:
země	zem	k1gFnPc1	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
správní	správní	k2eAgInSc1d1	správní
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kladno	Kladno	k1gNnSc1	Kladno
</s>
</p>
<p>
<s>
1949	[number]	k4	1949
<g/>
:	:	kIx,	:
Pražský	pražský	k2eAgInSc1d1	pražský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Kladno	Kladno	k1gNnSc1	Kladno
</s>
</p>
<p>
<s>
od	od	k7c2	od
1960	[number]	k4	1960
<g/>
:	:	kIx,	:
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Kladno	Kladno	k1gNnSc1	Kladno
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Čech	Čechy	k1gFnPc2	Čechy
má	mít	k5eAaImIp3nS	mít
dobré	dobrý	k2eAgNnSc1d1	dobré
dopravní	dopravní	k2eAgNnSc1d1	dopravní
spojení	spojení	k1gNnSc1	spojení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
silniční	silniční	k2eAgNnSc1d1	silniční
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgFnSc1d1	železniční
i	i	k9	i
letecké	letecký	k2eAgFnSc3d1	letecká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
blízkosti	blízkost	k1gFnSc6	blízkost
Kladna	Kladno	k1gNnSc2	Kladno
vedou	vést	k5eAaImIp3nP	vést
důležité	důležitý	k2eAgFnPc1d1	důležitá
dálnice	dálnice	k1gFnPc1	dálnice
D6	D6	k1gFnSc2	D6
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Karlových	Karlův	k2eAgInPc2d1	Karlův
Varů	Vary	k1gInPc2	Vary
a	a	k8xC	a
D7	D7	k1gMnPc3	D7
spojující	spojující	k2eAgFnSc4d1	spojující
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Rakovníka	Rakovník	k1gInSc2	Rakovník
<g/>
,	,	kIx,	,
Kladno	Kladno	k1gNnSc1	Kladno
je	být	k5eAaImIp3nS	být
zahrnuto	zahrnout	k5eAaPmNgNnS	zahrnout
do	do	k7c2	do
pražského	pražský	k2eAgInSc2d1	pražský
systému	systém	k1gInSc2	systém
příměstské	příměstský	k2eAgFnSc2d1	příměstská
železnice	železnice	k1gFnSc2	železnice
Esko	eska	k1gFnSc5	eska
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Praha-Ruzyně	Praha-Ruzyně	k1gFnSc2	Praha-Ruzyně
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Kladna	Kladno	k1gNnSc2	Kladno
vzdáleno	vzdálit	k5eAaPmNgNnS	vzdálit
jen	jen	k9	jen
16	[number]	k4	16
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
protíná	protínat	k5eAaImIp3nS	protínat
silnice	silnice	k1gFnPc4	silnice
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
61	[number]	k4	61
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
město	město	k1gNnSc4	město
s	s	k7c7	s
dálnicemi	dálnice	k1gFnPc7	dálnice
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Slaný	Slaný	k1gInSc1	Slaný
–	–	k?	–
Louny	Louny	k1gInPc1	Louny
–	–	k?	–
Chomutov	Chomutov	k1gInSc1	Chomutov
a	a	k8xC	a
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Nové	Nové	k2eAgNnSc4d1	Nové
Strašecí	Strašecí	k1gNnSc4	Strašecí
–	–	k?	–
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
–	–	k?	–
Cheb	Cheb	k1gInSc1	Cheb
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
vedou	vést	k5eAaImIp3nP	vést
silnice	silnice	k1gFnPc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
118	[number]	k4	118
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Beroun	Beroun	k1gInSc1	Beroun
–	–	k?	–
Kladno	Kladno	k1gNnSc4	Kladno
–	–	k?	–
Slaný	Slaný	k1gInSc1	Slaný
a	a	k8xC	a
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
101	[number]	k4	101
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Kladno	Kladno	k1gNnSc1	Kladno
–	–	k?	–
Kralupy	Kralupy	k1gInPc1	Kralupy
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
–	–	k?	–
Neratovice	Neratovice	k1gFnSc2	Neratovice
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
238	[number]	k4	238
spojuje	spojovat	k5eAaImIp3nS	spojovat
Kladno	Kladno	k1gNnSc4	Kladno
s	s	k7c7	s
Kamennými	kamenný	k2eAgFnPc7d1	kamenná
Žehrovicemi	Žehrovice	k1gFnPc7	Žehrovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
120	[number]	k4	120
vedoucí	vedoucí	k1gFnSc1	vedoucí
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Rakovníka	Rakovník	k1gInSc2	Rakovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
093	[number]	k4	093
do	do	k7c2	do
Kralup	Kralupy	k1gInPc2	Kralupy
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Rakovník	Rakovník	k1gInSc1	Rakovník
====	====	k?	====
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
120	[number]	k4	120
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Kladno	Kladno	k1gNnSc4	Kladno
<g/>
–	–	k?	–
<g/>
Rakovník	Rakovník	k1gInSc1	Rakovník
je	být	k5eAaImIp3nS	být
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
celostátní	celostátní	k2eAgFnSc1d1	celostátní
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Kladna	Kladno	k1gNnSc2	Kladno
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
(	(	kIx(	(
<g/>
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
(	(	kIx(	(
<g/>
z	z	k7c2	z
Kladna	Kladno	k1gNnSc2	Kladno
do	do	k7c2	do
Stochova	Stochův	k2eAgInSc2d1	Stochův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
trati	trať	k1gFnSc6	trať
vede	vést	k5eAaImIp3nS	vést
linka	linka	k1gFnSc1	linka
S5	S5	k1gFnSc1	S5
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
)	)	kIx)	)
a	a	k8xC	a
R24	R24	k1gFnSc1	R24
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Kladno	Kladno	k1gNnSc4	Kladno
<g/>
–	–	k?	–
<g/>
Rakovník	Rakovník	k1gInSc1	Rakovník
<g/>
)	)	kIx)	)
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pražského	pražský	k2eAgInSc2d1	pražský
systému	systém	k1gInSc2	systém
Esko	eska	k1gFnSc5	eska
<g/>
.	.	kIx.	.
</s>
<s>
Přepravní	přepravní	k2eAgNnSc1d1	přepravní
zatížení	zatížení	k1gNnSc1	zatížení
tratě	trať	k1gFnSc2	trať
směrem	směr	k1gInSc7	směr
na	na	k7c6	na
Prahu	práh	k1gInSc6	práh
v	v	k7c6	v
pracovních	pracovní	k2eAgInPc6d1	pracovní
dnech	den	k1gInPc6	den
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
činilo	činit	k5eAaImAgNnS	činit
obousměrně	obousměrně	k6eAd1	obousměrně
8	[number]	k4	8
rychlíků	rychlík	k1gInPc2	rychlík
<g/>
,	,	kIx,	,
11	[number]	k4	11
spěšných	spěšný	k2eAgInPc2d1	spěšný
vlaků	vlak	k1gInPc2	vlak
a	a	k8xC	a
21	[number]	k4	21
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
do	do	k7c2	do
Rakovníka	Rakovník	k1gInSc2	Rakovník
7	[number]	k4	7
rychlíků	rychlík	k1gInPc2	rychlík
<g/>
,	,	kIx,	,
1	[number]	k4	1
spěšný	spěšný	k2eAgInSc1d1	spěšný
vlak	vlak	k1gInSc1	vlak
a	a	k8xC	a
10	[number]	k4	10
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
odbočná	odbočný	k2eAgFnSc1d1	odbočná
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Kladno	Kladno	k1gNnSc1	Kladno
a	a	k8xC	a
mezilehlá	mezilehlý	k2eAgFnSc1d1	mezilehlá
železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
Kladno-Rozdělov	Kladno-Rozdělov	k1gInSc1	Kladno-Rozdělov
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
</s>
</p>
<p>
<s>
====	====	k?	====
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Kralupy	Kralupy	k1gInPc4	Kralupy
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
–	–	k?	–
Kladno	Kladno	k1gNnSc1	Kladno
====	====	k?	====
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
093	[number]	k4	093
Kladno	Kladno	k1gNnSc4	Kladno
–	–	k?	–
Kralupy	Kralupy	k1gInPc1	Kralupy
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
je	být	k5eAaImIp3nS	být
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
celostátní	celostátní	k2eAgFnSc1d1	celostátní
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
odbočná	odbočný	k2eAgFnSc1d1	odbočná
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
zastávka	zastávka	k1gFnSc1	zastávka
Kladno	Kladno	k1gNnSc1	Kladno
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
stanice	stanice	k1gFnSc1	stanice
Kladno-Ostrovec	Kladno-Ostrovec	k1gMnSc1	Kladno-Ostrovec
<g/>
,	,	kIx,	,
zastávka	zastávka	k1gFnSc1	zastávka
Kladno-Švermov	Kladno-Švermov	k1gInSc1	Kladno-Švermov
<g/>
,	,	kIx,	,
stanice	stanice	k1gFnSc1	stanice
Kladno-Dubí	Kladno-Dubí	k1gNnSc1	Kladno-Dubí
a	a	k8xC	a
zastávka	zastávka	k1gFnSc1	zastávka
Kladno-Vrapice	Kladno-Vrapice	k1gFnSc1	Kladno-Vrapice
<g/>
.	.	kIx.	.
</s>
<s>
Přepravní	přepravní	k2eAgNnSc1d1	přepravní
zatížení	zatížení	k1gNnSc1	zatížení
tratě	trať	k1gFnSc2	trať
v	v	k7c6	v
pracovních	pracovní	k2eAgInPc6d1	pracovní
dnech	den	k1gInPc6	den
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
činilo	činit	k5eAaImAgNnS	činit
obousměrně	obousměrně	k6eAd1	obousměrně
15	[number]	k4	15
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úseku	úsek	k1gInSc6	úsek
z	z	k7c2	z
Kladna	Kladno	k1gNnSc2	Kladno
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Ostrovec	Ostrovec	k1gMnSc1	Ostrovec
jezdily	jezdit	k5eAaImAgInP	jezdit
navíc	navíc	k6eAd1	navíc
i	i	k9	i
přímé	přímý	k2eAgInPc1d1	přímý
vlaky	vlak	k1gInPc1	vlak
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
jezdilo	jezdit	k5eAaImAgNnS	jezdit
denně	denně	k6eAd1	denně
9	[number]	k4	9
spěšných	spěšný	k2eAgInPc2d1	spěšný
vlaků	vlak	k1gInPc2	vlak
a	a	k8xC	a
32	[number]	k4	32
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Zrušené	zrušený	k2eAgFnPc4d1	zrušená
tratě	trať	k1gFnPc4	trať
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Kladno-Dubí	Kladno-Dubí	k1gNnSc1	Kladno-Dubí
vedla	vést	k5eAaImAgFnS	vést
i	i	k9	i
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Zvoleněvse	Zvoleněvse	k1gFnSc2	Zvoleněvse
(	(	kIx(	(
<g/>
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
110	[number]	k4	110
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zrušená	zrušený	k2eAgFnSc1d1	zrušená
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
byla	být	k5eAaImAgFnS	být
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
lokální	lokální	k2eAgFnSc1d1	lokální
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
postavená	postavený	k2eAgFnSc1d1	postavená
jako	jako	k8xC	jako
vlečka	vlečka	k1gFnSc1	vlečka
z	z	k7c2	z
Kladna-Dubí	Kladna-Dubí	k1gNnSc2	Kladna-Dubí
do	do	k7c2	do
Vinařic	Vinařice	k1gFnPc2	Vinařice
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1875	[number]	k4	1875
až	až	k9	až
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
osobní	osobní	k2eAgFnSc1d1	osobní
doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
provozována	provozovat	k5eAaImNgFnS	provozovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
trati	trať	k1gFnSc6	trať
jezdilo	jezdit	k5eAaImAgNnS	jezdit
jen	jen	k9	jen
4	[number]	k4	4
až	až	k9	až
7	[number]	k4	7
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Úsek	úsek	k1gInSc1	úsek
Kladno-Dubí	Kladno-Dubí	k1gNnSc2	Kladno-Dubí
-	-	kIx~	-
Mayrau	Mayraus	k1gInSc2	Mayraus
(	(	kIx(	(
<g/>
skanzen	skanzen	k1gInSc1	skanzen
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
částečně	částečně	k6eAd1	částečně
obnoven	obnovit	k5eAaPmNgInS	obnovit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pojížděn	pojížděn	k2eAgInSc1d1	pojížděn
příležitostnými	příležitostný	k2eAgInPc7d1	příležitostný
zážitkovými	zážitkový	k2eAgInPc7d1	zážitkový
vlaky	vlak	k1gInPc7	vlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Letecká	letecký	k2eAgFnSc1d1	letecká
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Přibližně	přibližně	k6eAd1	přibližně
12	[number]	k4	12
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Kladna	Kladno	k1gNnSc2	Kladno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
největší	veliký	k2eAgNnSc4d3	veliký
letiště	letiště	k1gNnSc4	letiště
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
Letiště	letiště	k1gNnSc4	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
2017	[number]	k4	2017
letiště	letiště	k1gNnSc2	letiště
odbavilo	odbavit	k5eAaPmAgNnS	odbavit
15,4	[number]	k4	15,4
miliónů	milión	k4xCgInPc2	milión
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
němu	on	k3xPp3gInSc3	on
se	se	k3xPyFc4	se
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
dostanete	dostat	k5eAaPmIp2nP	dostat
na	na	k7c4	na
jednak	jednak	k8xC	jednak
největší	veliký	k2eAgFnSc4d3	veliký
přestupní	přestupní	k2eAgFnSc4d1	přestupní
uzle	uzel	k1gInSc5	uzel
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
<g/>
,	,	kIx,	,
Londýn-Heathrow	Londýn-Heathrow	k1gFnSc1	Londýn-Heathrow
<g/>
,	,	kIx,	,
Paříž-CDG	Paříž-CDG	k1gFnSc1	Paříž-CDG
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
kontinentů	kontinent	k1gInPc2	kontinent
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
Dubai	Dubai	k1gNnSc1	Dubai
<g/>
,	,	kIx,	,
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
,	,	kIx,	,
Tel	tel	kA	tel
Aviv	Aviv	k1gInSc1	Aviv
<g/>
,	,	kIx,	,
Soul	Soul	k1gInSc1	Soul
<g/>
,	,	kIx,	,
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
<g/>
,	,	kIx,	,
Čcheng-tu	Čchenga	k1gFnSc4	Čcheng-ta
<g/>
,	,	kIx,	,
Peking	Peking	k1gInSc4	Peking
+	+	kIx~	+
nově	nově	k6eAd1	nově
Dauhá	Dauhý	k2eAgFnSc1d1	Dauhá
<g/>
,	,	kIx,	,
Hanoj	Hanoj	k1gFnSc1	Hanoj
<g/>
,	,	kIx,	,
Xi	Xi	k1gFnSc1	Xi
<g/>
'	'	kIx"	'
<g/>
an	an	k?	an
<g/>
)	)	kIx)	)
a	a	k8xC	a
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York-JFK	York-JFK	k1gFnSc1	York-JFK
<g/>
,	,	kIx,	,
Atlanta	Atlanta	k1gFnSc1	Atlanta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Lanna	Lann	k1gMnSc2	Lann
(	(	kIx(	(
<g/>
1805	[number]	k4	1805
<g/>
–	–	k?	–
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
průmyslník	průmyslník	k1gMnSc1	průmyslník
<g/>
,	,	kIx,	,
loďmistr	loďmistr	k1gMnSc1	loďmistr
a	a	k8xC	a
stavitel	stavitel	k1gMnSc1	stavitel
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Braniš	Braniš	k1gMnSc1	Braniš
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc2	umění
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Suk	Suk	k1gMnSc1	Suk
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
houslista	houslista	k1gMnSc1	houslista
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
působící	působící	k2eAgMnSc1d1	působící
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
</s>
</p>
<p>
<s>
Alfons	Alfons	k1gMnSc1	Alfons
Breska	Bresek	k1gMnSc2	Bresek
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
</s>
</p>
<p>
<s>
Anton	Anton	k1gMnSc1	Anton
Joseph	Joseph	k1gMnSc1	Joseph
Cermak	Cermak	k1gMnSc1	Cermak
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Chicaga	Chicago	k1gNnSc2	Chicago
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Majerová	Majerová	k1gFnSc1	Majerová
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
prozaička	prozaička	k1gFnSc1	prozaička
<g/>
,	,	kIx,	,
komunistická	komunistický	k2eAgFnSc1d1	komunistická
novinářka	novinářka	k1gFnSc1	novinářka
a	a	k8xC	a
národní	národní	k2eAgFnSc1d1	národní
umělkyně	umělkyně	k1gFnSc1	umělkyně
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Seidl	Seidl	k1gMnSc1	Seidl
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rössler	Rössler	k1gMnSc1	Rössler
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
kladenský	kladenský	k2eAgMnSc1d1	kladenský
rodák	rodák	k1gMnSc1	rodák
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Raymond	Raymond	k1gMnSc1	Raymond
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgMnSc1d1	moderní
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
stavěl	stavět	k5eAaImAgMnS	stavět
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
</s>
</p>
<p>
<s>
Cyril	Cyril	k1gMnSc1	Cyril
Bouda	Bouda	k1gMnSc1	Bouda
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Kloz	Kloz	k1gMnSc1	Kloz
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
historicky	historicky	k6eAd1	historicky
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgMnPc2d3	nejslavnější
sportovců	sportovec	k1gMnPc2	sportovec
Kladenska	Kladensko	k1gNnSc2	Kladensko
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Kobr	Kobr	k1gMnSc1	Kobr
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Vella	Vella	k1gMnSc1	Vella
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
pilot	pilot	k1gMnSc1	pilot
RAF	raf	k0	raf
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kolář	Kolář	k1gMnSc1	Kolář
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
výtvarník	výtvarník	k1gMnSc1	výtvarník
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kárný	kárný	k2eAgMnSc1d1	kárný
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Miler	Miler	k1gMnSc1	Miler
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
výtvarník	výtvarník	k1gMnSc1	výtvarník
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Souček	Souček	k1gMnSc1	Souček
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
</s>
</p>
<p>
<s>
Květa	Květa	k1gFnSc1	Květa
Válová	Válová	k1gFnSc1	Válová
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
výtvarnice	výtvarnice	k1gFnSc1	výtvarnice
</s>
</p>
<p>
<s>
Jitka	Jitka	k1gFnSc1	Jitka
Válová	Válová	k1gFnSc1	Válová
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
výtvarnice	výtvarnice	k1gFnSc1	výtvarnice
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Battěk	Battěk	k1gMnSc1	Battěk
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
disident	disident	k1gMnSc1	disident
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Bóra	bóra	k1gFnSc1	bóra
Kříž	Kříž	k1gMnSc1	Kříž
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
jazzový	jazzový	k2eAgMnSc1d1	jazzový
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
multiinstrumentalista	multiinstrumentalista	k1gMnSc1	multiinstrumentalista
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Brandejs	Brandejs	k1gMnSc1	Brandejs
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
houslista	houslista	k1gMnSc1	houslista
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
člen	člen	k1gMnSc1	člen
Symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
rozhlasu	rozhlas	k1gInSc2	rozhlas
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Stavinoha	Stavinoha	k1gMnSc1	Stavinoha
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
kladenský	kladenský	k2eAgMnSc1d1	kladenský
horník	horník	k1gMnSc1	horník
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Olejník	Olejník	k1gMnSc1	Olejník
(	(	kIx(	(
<g/>
*	*	kIx~	*
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nevidomý	nevidomý	k1gMnSc1	nevidomý
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
flétnista	flétnista	k1gMnSc1	flétnista
a	a	k8xC	a
klavírista	klavírista	k1gMnSc1	klavírista
</s>
</p>
<p>
<s>
Ota	Ota	k1gMnSc1	Ota
Pavel	Pavel	k1gMnSc1	Pavel
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
sportovní	sportovní	k2eAgMnSc1d1	sportovní
reportér	reportér	k1gMnSc1	reportér
</s>
</p>
<p>
<s>
Marta	Marta	k1gFnSc1	Marta
Jiráčková	Jiráčková	k1gFnSc1	Jiráčková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
hudební	hudební	k2eAgFnSc1d1	hudební
skladatelka	skladatelka	k1gFnSc1	skladatelka
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
(	(	kIx(	(
<g/>
Wabi	Wabi	k1gNnSc1	Wabi
<g/>
)	)	kIx)	)
Ryvola	Ryvola	k1gFnSc1	Ryvola
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
trampský	trampský	k2eAgMnSc1d1	trampský
písničkář	písničkář	k1gMnSc1	písničkář
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Dienstbier	Dienstbier	k1gMnSc1	Dienstbier
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
disident	disident	k1gMnSc1	disident
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Fousek	Fousek	k1gMnSc1	Fousek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
písničkář	písničkář	k1gMnSc1	písničkář
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Pithart	Pitharta	k1gFnPc2	Pitharta
(	(	kIx(	(
<g/>
*	*	kIx~	*
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
české	český	k2eAgFnSc2d1	Česká
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
a	a	k8xC	a
místopředseda	místopředseda	k1gMnSc1	místopředseda
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Mirko	Mirko	k1gMnSc1	Mirko
(	(	kIx(	(
<g/>
Miki	Miki	k1gNnSc1	Miki
<g/>
)	)	kIx)	)
Ryvola	Ryvola	k1gFnSc1	Ryvola
(	(	kIx(	(
<g/>
*	*	kIx~	*
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
trampský	trampský	k2eAgMnSc1d1	trampský
písničkář	písničkář	k1gMnSc1	písničkář
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
</s>
</p>
<p>
<s>
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Karásek	Karásek	k1gMnSc1	Karásek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
písničkář	písničkář	k1gMnSc1	písničkář
a	a	k8xC	a
evangelický	evangelický	k2eAgMnSc1d1	evangelický
duchovní	duchovní	k1gMnSc1	duchovní
</s>
</p>
<p>
<s>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Hort	Hort	k1gMnSc1	Hort
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
a	a	k8xC	a
po	po	k7c6	po
emigraci	emigrace	k1gFnSc6	emigrace
1985	[number]	k4	1985
německý	německý	k2eAgMnSc1d1	německý
šachový	šachový	k2eAgMnSc1d1	šachový
velmistr	velmistr	k1gMnSc1	velmistr
a	a	k8xC	a
teoretik	teoretik	k1gMnSc1	teoretik
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchomel	Suchomel	k1gMnSc1	Suchomel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
děkan	děkan	k1gMnSc1	děkan
<g/>
,	,	kIx,	,
proděkan	proděkan	k1gMnSc1	proděkan
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hošek	Hošek	k1gMnSc1	Hošek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
autokrosový	autokrosový	k2eAgMnSc1d1	autokrosový
závodník	závodník	k1gMnSc1	závodník
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Valta	Valta	k1gMnSc1	Valta
(	(	kIx(	(
<g/>
*	*	kIx~	*
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
klavírista	klavírista	k1gMnSc1	klavírista
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Jamek	jamka	k1gFnPc2	jamka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Panocha	Panoch	k1gMnSc2	Panoch
(	(	kIx(	(
<g/>
*	*	kIx~	*
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
houslista	houslista	k1gMnSc1	houslista
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
Panochova	Panochův	k2eAgNnSc2d1	Panochovo
kvarteta	kvarteto	k1gNnSc2	kvarteto
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Nový	nový	k2eAgMnSc1d1	nový
(	(	kIx(	(
<g/>
*	*	kIx~	*
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
legenda	legenda	k1gFnSc1	legenda
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
českého	český	k2eAgInSc2d1	český
hokeje	hokej	k1gInSc2	hokej
</s>
</p>
<p>
<s>
Jana	Jana	k1gFnSc1	Jana
Boušková	Boušková	k1gFnSc1	Boušková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Ilona	Ilona	k1gFnSc1	Ilona
Svobodová	Svobodová	k1gFnSc1	Svobodová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Luboš	Luboš	k1gMnSc1	Luboš
Patera	Patera	k1gMnSc1	Patera
(	(	kIx(	(
<g/>
*	*	kIx~	*
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
občanský	občanský	k2eAgMnSc1d1	občanský
aktivista	aktivista	k1gMnSc1	aktivista
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Sládek	Sládek	k1gMnSc1	Sládek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
editor	editor	k1gInSc1	editor
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Pivoňka	Pivoňka	k1gMnSc1	Pivoňka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Suchopárek	Suchopárek	k1gMnSc1	Suchopárek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
obránce	obránce	k1gMnSc1	obránce
<g/>
,	,	kIx,	,
vicemistr	vicemistr	k1gMnSc1	vicemistr
z	z	k7c2	z
ME	ME	kA	ME
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Patera	Patera	k1gMnSc1	Patera
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
střední	střední	k2eAgMnSc1d1	střední
útočník	útočník	k1gMnSc1	útočník
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
proslulé	proslulý	k2eAgFnSc2d1	proslulá
BlueLine	BlueLin	k1gMnSc5	BlueLin
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
(	(	kIx(	(
<g/>
*	*	kIx~	*
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
reprezentant	reprezentant	k1gMnSc1	reprezentant
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
světových	světový	k2eAgMnPc2d1	světový
hokejistů	hokejista	k1gMnPc2	hokejista
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
několika	několik	k4yIc2	několik
klubů	klub	k1gInPc2	klub
v	v	k7c6	v
NHL	NHL	kA	NHL
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Kaberle	Kaberle	k1gFnSc2	Kaberle
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Révai	Réva	k1gFnSc2	Réva
(	(	kIx(	(
<g/>
*	*	kIx~	*
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
tanečník	tanečník	k1gMnSc1	tanečník
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Huml	Huml	k1gMnSc1	Huml
(	(	kIx(	(
<g/>
*	*	kIx~	*
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
klub	klub	k1gInSc1	klub
Kärpät	Kärpäta	k1gFnPc2	Kärpäta
Oulu	Oulus	k1gInSc2	Oulus
(	(	kIx(	(
<g/>
finská	finský	k2eAgFnSc1d1	finská
SM-liiga	SMiiga	k1gFnSc1	SM-liiga
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Plekanec	Plekanec	k1gMnSc1	Plekanec
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
střední	střední	k2eAgMnSc1d1	střední
útočník	útočník	k1gMnSc1	útočník
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Montreal	Montreal	k1gInSc1	Montreal
Canadiens	Canadiens	k1gInSc1	Canadiens
v	v	k7c6	v
NHL	NHL	kA	NHL
</s>
</p>
<p>
<s>
Roman	Roman	k1gMnSc1	Roman
Štabrňák	Štabrňák	k1gMnSc1	Štabrňák
(	(	kIx(	(
<g/>
*	*	kIx~	*
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
divadelní	divadelní	k2eAgMnSc1d1	divadelní
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
dabér	dabér	k1gMnSc1	dabér
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Růžička	Růžička	k1gMnSc1	Růžička
(	(	kIx(	(
<g/>
*	*	kIx~	*
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Lukáš	Lukáš	k1gMnSc1	Lukáš
M.	M.	kA	M.
Vytlačil	Vytlačil	k1gMnSc1	Vytlačil
(	(	kIx(	(
<g/>
*	*	kIx~	*
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
český	český	k2eAgMnSc1d1	český
flétnista	flétnista	k1gMnSc1	flétnista
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
muzikolog	muzikolog	k1gMnSc1	muzikolog
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Pavelec	Pavelec	k1gMnSc1	Pavelec
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hokejový	hokejový	k2eAgMnSc1d1	hokejový
brankář	brankář	k1gMnSc1	brankář
<g/>
,	,	kIx,	,
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Winnipeg	Winnipeg	k1gInSc1	Winnipeg
Jets	Jets	k1gInSc1	Jets
v	v	k7c6	v
NHL	NHL	kA	NHL
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Frolík	Frolík	k1gMnSc1	Frolík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
střední	střední	k2eAgMnSc1d1	střední
útočník	útočník	k1gMnSc1	útočník
v	v	k7c6	v
zámořské	zámořský	k2eAgFnSc6d1	zámořská
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Chicago	Chicago	k1gNnSc4	Chicago
Blackhawks	Blackhawksa	k1gFnPc2	Blackhawksa
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Voráček	Voráček	k1gMnSc1	Voráček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
pravé	pravý	k2eAgNnSc4d1	pravé
útočné	útočný	k2eAgNnSc4d1	útočné
křídlo	křídlo	k1gNnSc4	křídlo
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Flyers	Flyers	k1gInSc1	Flyers
v	v	k7c6	v
NHL	NHL	kA	NHL
</s>
</p>
<p>
<s>
Ivana	Ivana	k1gFnSc1	Ivana
Recmanová	Recmanová	k1gFnSc1	Recmanová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
publicistka	publicistka	k1gFnSc1	publicistka
<g/>
,	,	kIx,	,
lidskoprávní	lidskoprávní	k2eAgFnSc1d1	lidskoprávní
aktivistka	aktivistka	k1gFnSc1	aktivistka
a	a	k8xC	a
umělkyně	umělkyně	k1gFnSc1	umělkyně
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Bellevue	bellevue	k1gFnSc1	bellevue
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Cáchy	Cáchy	k1gFnPc1	Cáchy
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Vitry-sur-Seine	Vitryur-Seinout	k5eAaPmIp3nS	Vitry-sur-Seinout
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BAĎURA	Baďura	k1gMnSc1	Baďura
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgFnSc2d1	literární
toulky	toulka	k1gFnSc2	toulka
Kladenskem	Kladensko	k1gNnSc7	Kladensko
<g/>
.	.	kIx.	.
</s>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
:	:	kIx,	:
Halda	halda	k1gFnSc1	halda
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Kladenské	kladenský	k2eAgFnPc1d1	kladenská
zajímavosti	zajímavost	k1gFnPc1	zajímavost
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
905992	[number]	k4	905992
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dobývání	dobývání	k1gNnSc1	dobývání
uhlí	uhlí	k1gNnSc2	uhlí
na	na	k7c6	na
Kladensku	Kladensko	k1gNnSc6	Kladensko
<g/>
:	:	kIx,	:
historie	historie	k1gFnSc1	historie
kladensko-slánsko-rakovnické	kladenskolánskoakovnický	k2eAgFnSc2d1	kladensko-slánsko-rakovnický
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
OKD	OKD	kA	OKD
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLAPKA	Klapka	k1gMnSc1	Klapka
<g/>
,	,	kIx,	,
Č.	Č.	kA	Č.
Dějiny	dějiny	k1gFnPc1	dějiny
města	město	k1gNnSc2	město
Kladna	Kladno	k1gNnSc2	Kladno
a	a	k8xC	a
životopis	životopis	k1gInSc1	životopis
povzbuditelů	povzbuditel	k1gMnPc2	povzbuditel
jeho	jeho	k3xOp3gMnPc2	jeho
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
vl	vl	k?	vl
<g/>
.	.	kIx.	.
<g/>
n.	n.	k?	n.
<g/>
,	,	kIx,	,
1876	[number]	k4	1876
<g/>
.	.	kIx.	.
15	[number]	k4	15
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KUCHYŇKA	Kuchyňka	k1gMnSc1	Kuchyňka
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
DOBNER	DOBNER	kA	DOBNER
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
JANKOVEC	JANKOVEC	kA	JANKOVEC
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
současnost	současnost	k1gFnSc1	současnost
podnikání	podnikání	k1gNnSc2	podnikání
na	na	k7c6	na
Kladensku	Kladensko	k1gNnSc6	Kladensko
a	a	k8xC	a
Slánsku	Slánsko	k1gNnSc6	Slánsko
<g/>
.	.	kIx.	.
</s>
<s>
Žehušice	Žehušice	k1gFnSc1	Žehušice
<g/>
:	:	kIx,	:
Městské	městský	k2eAgFnSc2d1	městská
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86699	[number]	k4	86699
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MATĚJ	Matěj	k1gMnSc1	Matěj
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgNnSc1d1	kulturní
dědictví	dědictví	k1gNnSc1	dědictví
Centrálního	centrální	k2eAgInSc2d1	centrální
kladenského	kladenský	k2eAgInSc2d1	kladenský
kamenouhelného	kamenouhelný	k2eAgInSc2d1	kamenouhelný
revíru	revír	k1gInSc2	revír
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgInSc1d1	státní
ústav	ústav	k1gInSc1	ústav
památkové	památkový	k2eAgFnSc2d1	památková
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86234	[number]	k4	86234
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OLIVERIUS	OLIVERIUS	kA	OLIVERIUS
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Rukověť	rukověť	k1gFnSc1	rukověť
k	k	k7c3	k
symbolům	symbol	k1gInPc3	symbol
měst	město	k1gNnPc2	město
a	a	k8xC	a
obcí	obec	k1gFnSc7	obec
Kladenska	Kladensko	k1gNnSc2	Kladensko
a	a	k8xC	a
Slánska	Slánsko	k1gNnSc2	Slánsko
<g/>
.	.	kIx.	.
</s>
<s>
Slaný	Slaný	k1gInSc1	Slaný
<g/>
:	:	kIx,	:
Patria	Patrium	k1gNnSc2	Patrium
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
910	[number]	k4	910
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SEIFERT	Seifert	k1gMnSc1	Seifert
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
.	.	kIx.	.
</s>
<s>
Doly	dol	k1gInPc1	dol
<g/>
,	,	kIx,	,
hutě	huť	k1gFnPc1	huť
a	a	k8xC	a
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
:	:	kIx,	:
Statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
905388	[number]	k4	905388
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠKORPIL	Škorpil	k1gMnSc1	Škorpil
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Bohumil	Bohumil	k1gMnSc1	Bohumil
<g/>
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc1	popis
hejtmanství	hejtmanství	k1gNnSc2	hejtmanství
Kladenského	kladenský	k2eAgNnSc2d1	kladenské
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
GARN	GARN	kA	GARN
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Monografie	monografie	k1gFnSc1	monografie
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
městeček	městečko	k1gNnPc2	městečko
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
172	[number]	k4	172
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIRTH	WIRTH	kA	WIRTH
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Manchester	Manchester	k1gInSc1	Manchester
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
svět	svět	k1gInSc1	svět
<g/>
:	:	kIx,	:
illustrovaný	illustrovaný	k2eAgInSc1d1	illustrovaný
čtrnáctidenník	čtrnáctidenník	k1gInSc1	čtrnáctidenník
<g/>
.	.	kIx.	.
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
70	[number]	k4	70
<g/>
-	-	kIx~	-
<g/>
72	[number]	k4	72
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kladno	Kladno	k1gNnSc4	Kladno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Kladno	Kladno	k1gNnSc4	Kladno
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Kladno	Kladno	k1gNnSc1	Kladno
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
http://kladnominule.cz/	[url]	k?	http://kladnominule.cz/
</s>
</p>
