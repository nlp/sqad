<p>
<s>
Reformace	reformace	k1gFnSc1	reformace
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
obnovení	obnovení	k1gNnSc1	obnovení
či	či	k8xC	či
oprava	oprava	k1gFnSc1	oprava
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
proces	proces	k1gInSc1	proces
v	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
církvi	církev	k1gFnSc6	církev
s	s	k7c7	s
těžištěm	těžiště	k1gNnSc7	těžiště
v	v	k7c4	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
nápravy	náprava	k1gFnSc2	náprava
poměrů	poměr	k1gInPc2	poměr
a	a	k8xC	a
návratu	návrat	k1gInSc2	návrat
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
nezatíženému	zatížený	k2eNgInSc3d1	nezatížený
tradicí	tradice	k1gFnSc7	tradice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
reformace	reformace	k1gFnSc2	reformace
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
protestantské	protestantský	k2eAgFnPc1d1	protestantská
církve	církev	k1gFnPc1	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Příčina	příčina	k1gFnSc1	příčina
===	===	k?	===
</s>
</p>
<p>
<s>
Teologickou	teologický	k2eAgFnSc7d1	teologická
příčinou	příčina	k1gFnSc7	příčina
reformace	reformace	k1gFnSc2	reformace
byl	být	k5eAaImAgInS	být
rozpor	rozpor	k1gInSc1	rozpor
mezi	mezi	k7c7	mezi
Biblí	bible	k1gFnSc7	bible
a	a	k8xC	a
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
církevní	církevní	k2eAgFnSc7d1	církevní
praxí	praxe	k1gFnSc7	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Papežství	papežství	k1gNnSc1	papežství
ignorovalo	ignorovat	k5eAaImAgNnS	ignorovat
nebo	nebo	k8xC	nebo
potlačovalo	potlačovat	k5eAaImAgNnS	potlačovat
kritiky	kritik	k1gMnPc4	kritik
poměrů	poměr	k1gInPc2	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Sporná	sporný	k2eAgFnSc1d1	sporná
byla	být	k5eAaImAgFnS	být
otázka	otázka	k1gFnSc1	otázka
odpustků	odpustek	k1gInPc2	odpustek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
jejich	jejich	k3xOp3gMnPc1	jejich
kritici	kritik	k1gMnPc1	kritik
označovali	označovat	k5eAaImAgMnP	označovat
za	za	k7c4	za
kupčení	kupčení	k1gNnSc4	kupčení
se	s	k7c7	s
spásou	spása	k1gFnSc7	spása
<g/>
.	.	kIx.	.
</s>
<s>
Nákladná	nákladný	k2eAgFnSc1d1	nákladná
správa	správa	k1gFnSc1	správa
majetku	majetek	k1gInSc2	majetek
církve	církev	k1gFnSc2	církev
měla	mít	k5eAaImAgFnS	mít
svůj	svůj	k3xOyFgInSc4	svůj
odraz	odraz	k1gInSc4	odraz
v	v	k7c6	v
zavádění	zavádění	k1gNnSc6	zavádění
stále	stále	k6eAd1	stále
nových	nový	k2eAgInPc2d1	nový
církevních	církevní	k2eAgInPc2d1	církevní
poplatků	poplatek	k1gInPc2	poplatek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
setkávalo	setkávat	k5eAaImAgNnS	setkávat
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
reformátoři	reformátor	k1gMnPc1	reformátor
šli	jít	k5eAaImAgMnP	jít
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
že	že	k8xS	že
obviňovali	obviňovat	k5eAaImAgMnP	obviňovat
preláty	prelát	k1gInPc4	prelát
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
církevních	církevní	k2eAgInPc2d1	církevní
úřadů	úřad	k1gInPc2	úřad
(	(	kIx(	(
<g/>
simonie	simonie	k1gFnSc2	simonie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Směry	směr	k1gInPc1	směr
reformace	reformace	k1gFnSc2	reformace
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
center	centrum	k1gNnPc2	centrum
reformace	reformace	k1gFnSc2	reformace
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
německé	německý	k2eAgFnSc6d1	německá
reformaci	reformace	k1gFnSc6	reformace
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
reformátorem	reformátor	k1gMnSc7	reformátor
Martinem	Martin	k1gMnSc7	Martin
Lutherem	Luther	k1gMnSc7	Luther
(	(	kIx(	(
<g/>
Wittenberg	Wittenberg	k1gMnSc1	Wittenberg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
základem	základ	k1gInSc7	základ
pozdější	pozdní	k2eAgFnSc2d2	pozdější
lutherské	lutherský	k2eAgFnSc2d1	lutherská
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
a	a	k8xC	a
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
reformaci	reformace	k1gFnSc6	reformace
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Ulricha	Ulrich	k1gMnSc2	Ulrich
Zwingliho	Zwingli	k1gMnSc2	Zwingli
(	(	kIx(	(
<g/>
Curych	Curych	k1gInSc1	Curych
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Kalvína	Kalvín	k1gMnSc2	Kalvín
(	(	kIx(	(
<g/>
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
reformované	reformovaný	k2eAgFnSc2d1	reformovaná
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společným	společný	k2eAgInSc7d1	společný
jmenovatelem	jmenovatel	k1gInSc7	jmenovatel
evangelických	evangelický	k2eAgFnPc2d1	evangelická
církví	církev	k1gFnPc2	církev
lutherských	lutherský	k2eAgFnPc2d1	lutherská
i	i	k8xC	i
reformovaných	reformovaný	k2eAgFnPc2d1	reformovaná
jsou	být	k5eAaImIp3nP	být
3	[number]	k4	3
sola	solum	k1gNnSc2	solum
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
pouze	pouze	k6eAd1	pouze
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
pouze	pouze	k6eAd1	pouze
milost	milost	k1gFnSc4	milost
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
víra	víra	k1gFnSc1	víra
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
Písmo	písmo	k1gNnSc1	písmo
<g/>
"	"	kIx"	"
Martina	Martina	k1gFnSc1	Martina
Luthera	Luther	k1gMnSc2	Luther
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozdílnou	rozdílný	k2eAgFnSc7d1	rozdílná
mírou	míra	k1gFnSc7	míra
jejich	jejich	k3xOp3gInPc2	jejich
důsledků	důsledek	k1gInPc2	důsledek
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Věroučné	věroučný	k2eAgInPc1d1	věroučný
rozdíly	rozdíl	k1gInPc1	rozdíl
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
např.	např.	kA	např.
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
Svaté	svatý	k2eAgFnSc2d1	svatá
večeře	večeře	k1gFnSc2	večeře
a	a	k8xC	a
učení	učení	k1gNnSc2	učení
o	o	k7c4	o
predestinaci	predestinace	k1gFnSc4	predestinace
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
evangelíci	evangelík	k1gMnPc1	evangelík
myšlenkově	myšlenkově	k6eAd1	myšlenkově
blízcí	blízký	k2eAgMnPc1d1	blízký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Předchůdci	předchůdce	k1gMnPc5	předchůdce
===	===	k?	===
</s>
</p>
<p>
<s>
Proti	proti	k7c3	proti
poměrům	poměr	k1gInPc3	poměr
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
veřejně	veřejně	k6eAd1	veřejně
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
již	již	k6eAd1	již
předchůdci	předchůdce	k1gMnPc1	předchůdce
reformace	reformace	k1gFnSc2	reformace
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
kritika	kritika	k1gFnSc1	kritika
však	však	k9	však
byla	být	k5eAaImAgFnS	být
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
za	za	k7c4	za
kacířství	kacířství	k1gNnSc4	kacířství
(	(	kIx(	(
<g/>
Ockham	Ockham	k1gInSc1	Ockham
<g/>
,	,	kIx,	,
Viklef	Viklef	k1gMnSc1	Viklef
<g/>
,	,	kIx,	,
Hus	Hus	k1gMnSc1	Hus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zakonzervování	zakonzervování	k1gNnSc3	zakonzervování
poměrů	poměr	k1gInPc2	poměr
<g/>
,	,	kIx,	,
nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
kumulovala	kumulovat	k5eAaImAgFnS	kumulovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Anglická	anglický	k2eAgFnSc1d1	anglická
reformace	reformace	k1gFnSc1	reformace
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
reformační	reformační	k2eAgFnSc2d1	reformační
myšlenky	myšlenka	k1gFnSc2	myšlenka
Angličan	Angličan	k1gMnSc1	Angličan
John	John	k1gMnSc1	John
Wycliffe	Wycliff	k1gInSc5	Wycliff
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1320	[number]	k4	1320
<g/>
/	/	kIx~	/
<g/>
1330	[number]	k4	1330
-	-	kIx~	-
1384	[number]	k4	1384
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vystupoval	vystupovat	k5eAaImAgInS	vystupovat
proti	proti	k7c3	proti
moci	moc	k1gFnSc3	moc
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
odpustkům	odpustek	k1gInPc3	odpustek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
postojích	postoj	k1gInPc6	postoj
důsledně	důsledně	k6eAd1	důsledně
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgFnSc7d1	hlavní
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
autoritou	autorita	k1gFnSc7	autorita
nejsou	být	k5eNaImIp3nP	být
církevní	církevní	k2eAgMnPc1d1	církevní
hodnostáři	hodnostář	k1gMnPc1	hodnostář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Bible	bible	k1gFnSc1	bible
<g/>
,	,	kIx,	,
a	a	k8xC	a
odmítá	odmítat	k5eAaImIp3nS	odmítat
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
světskou	světský	k2eAgFnSc4d1	světská
moc	moc	k1gFnSc4	moc
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
přemíru	přemíra	k1gFnSc4	přemíra
a	a	k8xC	a
pompéznost	pompéznost	k1gFnSc4	pompéznost
obřadů	obřad	k1gInPc2	obřad
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
===	===	k?	===
Německá	německý	k2eAgFnSc1d1	německá
reformace	reformace	k1gFnSc1	reformace
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
reformačního	reformační	k2eAgInSc2d1	reformační
procesu	proces	k1gInSc2	proces
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
31	[number]	k4	31
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
1517	[number]	k4	1517
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Martin	Martin	k1gInSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
svých	svůj	k3xOyFgFnPc2	svůj
95	[number]	k4	95
tezí	teze	k1gFnPc2	teze
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vyvolat	vyvolat	k5eAaPmF	vyvolat
akademickou	akademický	k2eAgFnSc4d1	akademická
diskuzi	diskuze	k1gFnSc4	diskuze
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
myšlenky	myšlenka	k1gFnSc2	myšlenka
se	se	k3xPyFc4	se
však	však	k9	však
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
roznesly	roznést	k5eAaPmAgInP	roznést
a	a	k8xC	a
vzbudily	vzbudit	k5eAaPmAgInP	vzbudit
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
označil	označit	k5eAaPmAgInS	označit
tyto	tento	k3xDgFnPc4	tento
teze	teze	k1gFnPc4	teze
za	za	k7c4	za
kacířské	kacířský	k2eAgFnPc4d1	kacířská
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Luthera	Luther	k1gMnSc4	Luther
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
odvolání	odvolání	k1gNnSc3	odvolání
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
spisy	spis	k1gInPc1	spis
byly	být	k5eAaImAgInP	být
páleny	pálen	k2eAgInPc1d1	pálen
<g/>
,	,	kIx,	,
Luther	Luthra	k1gFnPc2	Luthra
reagoval	reagovat	k5eAaBmAgMnS	reagovat
spálením	spálení	k1gNnSc7	spálení
papežské	papežský	k2eAgFnSc2d1	Papežská
buly	bula	k1gFnSc2	bula
a	a	k8xC	a
vydáním	vydání	k1gNnSc7	vydání
dalších	další	k2eAgInPc2d1	další
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
odmítal	odmítat	k5eAaImAgInS	odmítat
autoritu	autorita	k1gFnSc4	autorita
papeže	papež	k1gMnSc2	papež
a	a	k8xC	a
koncilů	koncil	k1gInPc2	koncil
a	a	k8xC	a
žádal	žádat	k5eAaImAgMnS	žádat
církevní	církevní	k2eAgFnPc4d1	církevní
reformy	reforma	k1gFnPc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1521	[number]	k4	1521
<g/>
,	,	kIx,	,
po	po	k7c6	po
uvalení	uvalení	k1gNnSc6	uvalení
církevní	církevní	k2eAgFnSc2d1	církevní
klatby	klatba	k1gFnSc2	klatba
<g/>
,	,	kIx,	,
následovalo	následovat	k5eAaImAgNnS	následovat
předvolání	předvolání	k1gNnSc4	předvolání
před	před	k7c4	před
říšský	říšský	k2eAgInSc4d1	říšský
sněm	sněm	k1gInSc4	sněm
ve	v	k7c6	v
Wormsu	Worms	k1gInSc6	Worms
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
Luther	Luthra	k1gFnPc2	Luthra
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
své	svůj	k3xOyFgNnSc4	svůj
učení	učení	k1gNnSc4	učení
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nebude	být	k5eNaImBp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
svědectvím	svědectví	k1gNnSc7	svědectví
Písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
vydal	vydat	k5eAaPmAgInS	vydat
Wormský	Wormský	k2eAgInSc1d1	Wormský
edikt	edikt	k1gInSc1	edikt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
reformátorova	reformátorův	k2eAgInSc2d1	reformátorův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
obsahem	obsah	k1gInSc7	obsah
je	být	k5eAaImIp3nS	být
říšská	říšský	k2eAgFnSc1d1	říšská
klatba	klatba	k1gFnSc1	klatba
proti	proti	k7c3	proti
Lutherovi	Luther	k1gMnSc3	Luther
a	a	k8xC	a
zákaz	zákaz	k1gInSc4	zákaz
jeho	jeho	k3xOp3gNnSc2	jeho
učení	učení	k1gNnSc2	učení
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
se	se	k3xPyFc4	se
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
Wartburg	Wartburg	k1gInSc1	Wartburg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Lutherových	Lutherův	k2eAgInPc2d1	Lutherův
spisů	spis	k1gInPc2	spis
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
vystoupení	vystoupení	k1gNnPc2	vystoupení
ve	v	k7c6	v
Wormsu	Worms	k1gInSc6	Worms
propukly	propuknout	k5eAaPmAgFnP	propuknout
od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
1521	[number]	k4	1521
prakticky	prakticky	k6eAd1	prakticky
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Německu	Německo	k1gNnSc6	Německo
náboženské	náboženský	k2eAgInPc4d1	náboženský
nepokoje	nepokoj	k1gInPc4	nepokoj
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
učení	učení	k1gNnSc1	učení
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
šířilo	šířit	k5eAaImAgNnS	šířit
i	i	k9	i
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Lutherova	Lutherův	k2eAgInSc2d1	Lutherův
překladu	překlad	k1gInSc2	překlad
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
veřejnosti	veřejnost	k1gFnSc2	veřejnost
prakticky	prakticky	k6eAd1	prakticky
nedostupná	dostupný	k2eNgNnPc1d1	nedostupné
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
přecházeli	přecházet	k5eAaImAgMnP	přecházet
houfně	houfně	k6eAd1	houfně
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
mniši	mnich	k1gMnPc1	mnich
a	a	k8xC	a
jeptišky	jeptiška	k1gFnSc2	jeptiška
opouštěli	opouštět	k5eAaImAgMnP	opouštět
kláštery	klášter	k1gInPc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
radikálním	radikální	k2eAgInPc3d1	radikální
projevům	projev	k1gInPc3	projev
reformace	reformace	k1gFnSc2	reformace
(	(	kIx(	(
<g/>
obrazoborectví	obrazoborectví	k1gNnSc1	obrazoborectví
a	a	k8xC	a
selským	selský	k2eAgInPc3d1	selský
nepokojům	nepokoj	k1gInPc3	nepokoj
<g/>
)	)	kIx)	)
reformátor	reformátor	k1gMnSc1	reformátor
ostře	ostro	k6eAd1	ostro
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
politické	politický	k2eAgFnSc6d1	politická
úrovni	úroveň	k1gFnSc6	úroveň
došlo	dojít	k5eAaPmAgNnS	dojít
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1526	[number]	k4	1526
ve	v	k7c6	v
Špýru	Špýr	k1gInSc6	Špýr
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
volba	volba	k1gFnSc1	volba
náboženství	náboženství	k1gNnSc2	náboženství
přenechává	přenechávat	k5eAaImIp3nS	přenechávat
autonomii	autonomie	k1gFnSc4	autonomie
zeměpánů	zeměpán	k1gMnPc2	zeměpán
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
říšském	říšský	k2eAgInSc6d1	říšský
sněmu	sněm	k1gInSc6	sněm
1529	[number]	k4	1529
toto	tento	k3xDgNnSc1	tento
právo	právo	k1gNnSc1	právo
opět	opět	k6eAd1	opět
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
,	,	kIx,	,
evangeličtí	evangelický	k2eAgMnPc1d1	evangelický
stavové	stavový	k2eAgNnSc4d1	stavové
protestovali	protestovat	k5eAaBmAgMnP	protestovat
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
protestanti	protestant	k1gMnPc1	protestant
<g/>
)	)	kIx)	)
opuštěním	opuštění	k1gNnPc3	opuštění
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1530	[number]	k4	1530
předložili	předložit	k5eAaPmAgMnP	předložit
evangelíci	evangelík	k1gMnPc1	evangelík
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
Augsburgu	Augsburg	k1gInSc6	Augsburg
Melanchthonovo	Melanchthonův	k2eAgNnSc4d1	Melanchthonův
Augsburské	augsburský	k2eAgNnSc4d1	Augsburské
vyznání	vyznání	k1gNnSc4	vyznání
<g/>
,	,	kIx,	,
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
přiblížení	přiblížení	k1gNnSc4	přiblížení
katolické	katolický	k2eAgFnSc3d1	katolická
straně	strana	k1gFnSc3	strana
(	(	kIx(	(
<g/>
zdůraznění	zdůraznění	k1gNnSc4	zdůraznění
společných	společný	k2eAgInPc2d1	společný
rysů	rys	k1gInPc2	rys
víry	víra	k1gFnSc2	víra
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
první	první	k4xOgNnSc4	první
evangelické	evangelický	k2eAgNnSc4d1	evangelické
vyznání	vyznání	k1gNnSc4	vyznání
nové	nový	k2eAgFnSc2d1	nová
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Katolické	katolický	k2eAgInPc1d1	katolický
stavy	stav	k1gInPc1	stav
toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
odmítly	odmítnout	k5eAaPmAgFnP	odmítnout
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
hrozil	hrozit	k5eAaImAgMnS	hrozit
protestantům	protestant	k1gMnPc3	protestant
represemi	represe	k1gFnPc7	represe
<g/>
.	.	kIx.	.
</s>
<s>
Evangelické	evangelický	k2eAgInPc4d1	evangelický
stavy	stav	k1gInPc4	stav
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
obranu	obrana	k1gFnSc4	obrana
založily	založit	k5eAaPmAgFnP	založit
šmalkaldský	šmalkaldský	k2eAgInSc4d1	šmalkaldský
spolek	spolek	k1gInSc4	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
protestantů	protestant	k1gMnPc2	protestant
ve	v	k7c6	v
šmalkaldské	šmalkaldský	k2eAgFnSc6d1	šmalkaldská
válce	válka	k1gFnSc6	válka
1546	[number]	k4	1546
-	-	kIx~	-
1547	[number]	k4	1547
následovalo	následovat	k5eAaImAgNnS	následovat
pronásledování	pronásledování	k1gNnSc1	pronásledování
protestantů	protestant	k1gMnPc2	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odstoupení	odstoupení	k1gNnSc6	odstoupení
císaře	císař	k1gMnSc2	císař
byl	být	k5eAaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1555	[number]	k4	1555
augsburský	augsburský	k2eAgInSc1d1	augsburský
náboženský	náboženský	k2eAgInSc1d1	náboženský
mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
obsahem	obsah	k1gInSc7	obsah
bylo	být	k5eAaImAgNnS	být
zrovnoprávnění	zrovnoprávnění	k1gNnSc2	zrovnoprávnění
obou	dva	k4xCgNnPc2	dva
náboženství	náboženství	k1gNnPc2	náboženství
na	na	k7c6	na
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
znamenalo	znamenat	k5eAaImAgNnS	znamenat
faktické	faktický	k2eAgNnSc1d1	faktické
uznání	uznání	k1gNnSc1	uznání
rozdělení	rozdělení	k1gNnSc2	rozdělení
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
reformace	reformace	k1gFnSc1	reformace
===	===	k?	===
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Ulricha	Ulrich	k1gMnSc2	Ulrich
Zwingliho	Zwingli	k1gMnSc2	Zwingli
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1522	[number]	k4	1522
začala	začít	k5eAaPmAgFnS	začít
reformace	reformace	k1gFnSc1	reformace
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
s	s	k7c7	s
evangelickým	evangelický	k2eAgNnSc7d1	evangelické
kázáním	kázání	k1gNnSc7	kázání
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
domněle	domněle	k6eAd1	domněle
nebiblických	biblický	k2eNgInPc2d1	nebiblický
elementů	element	k1gInPc2	element
v	v	k7c6	v
církevním	církevní	k2eAgInSc6d1	církevní
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
doprovázeno	doprovázet	k5eAaImNgNnS	doprovázet
i	i	k9	i
obrazoborectvím	obrazoborectví	k1gNnSc7	obrazoborectví
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
totéž	týž	k3xTgNnSc4	týž
východisko	východisko	k1gNnSc4	východisko
se	se	k3xPyFc4	se
teologické	teologický	k2eAgInPc1d1	teologický
názory	názor	k1gInPc1	názor
Zwingliho	Zwingli	k1gMnSc2	Zwingli
zčásti	zčásti	k6eAd1	zčásti
lišily	lišit	k5eAaImAgFnP	lišit
od	od	k7c2	od
Lutherových	Lutherová	k1gFnPc2	Lutherová
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
nalezení	nalezení	k1gNnSc4	nalezení
společného	společný	k2eAgNnSc2d1	společné
stanoviska	stanovisko	k1gNnSc2	stanovisko
při	při	k7c6	při
Marburských	Marburský	k2eAgInPc6d1	Marburský
náboženských	náboženský	k2eAgInPc6d1	náboženský
rozhovorech	rozhovor	k1gInPc6	rozhovor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1529	[number]	k4	1529
ztroskotalo	ztroskotat	k5eAaPmAgNnS	ztroskotat
na	na	k7c6	na
otázce	otázka	k1gFnSc6	otázka
Svaté	svatý	k2eAgFnSc2d1	svatá
večeře	večeře	k1gFnSc2	večeře
<g/>
.	.	kIx.	.
</s>
<s>
Napjatá	napjatý	k2eAgFnSc1d1	napjatá
náboženská	náboženský	k2eAgFnSc1d1	náboženská
situace	situace	k1gFnSc1	situace
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
1531	[number]	k4	1531
Zwingli	Zwingl	k1gMnPc7	Zwingl
umírá	umírat	k5eAaImIp3nS	umírat
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kappelu	Kappel	k1gInSc2	Kappel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
následovník	následovník	k1gMnSc1	následovník
Heinrich	Heinrich	k1gMnSc1	Heinrich
Bullinger	Bullinger	k1gMnSc1	Bullinger
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c4	na
formulování	formulování	k1gNnSc1	formulování
Prvního	první	k4xOgNnSc2	první
helvétského	helvétský	k2eAgNnSc2d1	helvétský
vyznání	vyznání	k1gNnSc2	vyznání
(	(	kIx(	(
<g/>
1536	[number]	k4	1536
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1533	[number]	k4	1533
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
Jan	Jan	k1gMnSc1	Jan
Kalvín	Kalvín	k1gMnSc1	Kalvín
<g/>
,	,	kIx,	,
pronásledovaný	pronásledovaný	k2eAgMnSc1d1	pronásledovaný
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
protestantské	protestantský	k2eAgInPc4d1	protestantský
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
učení	učení	k1gNnSc4	učení
shrnul	shrnout	k5eAaPmAgMnS	shrnout
v	v	k7c6	v
publikaci	publikace	k1gFnSc6	publikace
Instituce	instituce	k1gFnSc2	instituce
(	(	kIx(	(
<g/>
1535	[number]	k4	1535
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1536	[number]	k4	1536
působil	působit	k5eAaImAgMnS	působit
Kalvín	Kalvín	k1gMnSc1	Kalvín
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zavedl	zavést	k5eAaPmAgMnS	zavést
radikální	radikální	k2eAgFnSc3d1	radikální
reformaci	reformace	k1gFnSc3	reformace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dotýkala	dotýkat	k5eAaImAgFnS	dotýkat
života	život	k1gInSc2	život
všech	všecek	k3xTgInPc2	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Nakrátko	nakrátko	k6eAd1	nakrátko
byl	být	k5eAaImAgInS	být
vypovězen	vypovědět	k5eAaPmNgInS	vypovědět
<g/>
,	,	kIx,	,
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
budování	budování	k1gNnSc6	budování
teokracie	teokracie	k1gFnSc2	teokracie
(	(	kIx(	(
<g/>
propojení	propojení	k1gNnSc2	propojení
náboženství	náboženství	k1gNnSc2	náboženství
s	s	k7c7	s
politikou	politika	k1gFnSc7	politika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kalvínovo	Kalvínův	k2eAgNnSc1d1	Kalvínovo
učení	učení	k1gNnSc1	učení
o	o	k7c6	o
predestinaci	predestinace	k1gFnSc6	predestinace
(	(	kIx(	(
<g/>
předurčení	předurčení	k1gNnSc1	předurčení
člověka	člověk	k1gMnSc2	člověk
ke	k	k7c3	k
spáse	spása	k1gFnSc3	spása
nebo	nebo	k8xC	nebo
zavržení	zavržení	k1gNnSc4	zavržení
<g/>
)	)	kIx)	)
Lutheráni	Lutherán	k2eAgMnPc1d1	Lutherán
neuznávali	uznávat	k5eNaImAgMnP	uznávat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gFnSc4	jeho
pozici	pozice	k1gFnSc4	pozice
ke	k	k7c3	k
Svaté	svatý	k2eAgFnSc3d1	svatá
večeři	večeře	k1gFnSc3	večeře
<g/>
,	,	kIx,	,
společné	společný	k2eAgNnSc1d1	společné
s	s	k7c7	s
Zwinglim	Zwingli	k1gNnSc7	Zwingli
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1549	[number]	k4	1549
Kalvín	Kalvín	k1gMnSc1	Kalvín
s	s	k7c7	s
Bullingerem	Bullinger	k1gInSc7	Bullinger
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
společné	společný	k2eAgNnSc4d1	společné
vyznání	vyznání	k1gNnSc4	vyznání
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jednotná	jednotný	k2eAgFnSc1d1	jednotná
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
reformovaná	reformovaný	k2eAgFnSc1d1	reformovaná
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1566	[number]	k4	1566
požádal	požádat	k5eAaPmAgMnS	požádat
německý	německý	k2eAgMnSc1d1	německý
reformovaný	reformovaný	k2eAgMnSc1d1	reformovaný
falcký	falcký	k2eAgMnSc1d1	falcký
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
Bullingera	Bullingero	k1gNnSc2	Bullingero
o	o	k7c6	o
zaslání	zaslání	k1gNnSc6	zaslání
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
teology	teolog	k1gMnPc4	teolog
švýcarských	švýcarský	k2eAgInPc2d1	švýcarský
kantonů	kanton	k1gInPc2	kanton
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Druhé	druhý	k4xOgNnSc1	druhý
helvétské	helvétský	k2eAgNnSc1d1	helvétský
vyznání	vyznání	k1gNnSc1	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stálo	stát	k5eAaImAgNnS	stát
základem	základ	k1gInSc7	základ
reformovaných	reformovaný	k2eAgFnPc2d1	reformovaná
církví	církev	k1gFnPc2	církev
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Česká	český	k2eAgFnSc1d1	Česká
reformace	reformace	k1gFnSc1	reformace
===	===	k?	===
</s>
</p>
<p>
<s>
Reformace	reformace	k1gFnSc1	reformace
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
celoevropského	celoevropský	k2eAgNnSc2d1	celoevropské
pojetí	pojetí	k1gNnSc2	pojetí
svá	svůj	k3xOyFgNnPc4	svůj
specifika	specifikon	k1gNnPc4	specifikon
<g/>
.	.	kIx.	.
</s>
<s>
Reformační	reformační	k2eAgInSc1d1	reformační
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
či	či	k8xC	či
alespoň	alespoň	k9	alespoň
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
datuje	datovat	k5eAaImIp3nS	datovat
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
celospolečenským	celospolečenský	k2eAgFnPc3d1	celospolečenská
událostem	událost	k1gFnPc3	událost
díky	díky	k7c3	díky
působení	působení	k1gNnSc3	působení
mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
celoevropské	celoevropský	k2eAgInPc4d1	celoevropský
důsledky	důsledek	k1gInPc4	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
probíhal	probíhat	k5eAaImAgInS	probíhat
již	již	k6eAd1	již
v	v	k7c6	v
období	období	k1gNnSc6	období
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
i	i	k9	i
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
skončení	skončení	k1gNnSc6	skončení
<g/>
,	,	kIx,	,
České	český	k2eAgNnSc1d1	české
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
království	království	k1gNnSc3	království
dvojího	dvojí	k4xRgInSc2	dvojí
lidu	lid	k1gInSc2	lid
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
rozdělené	rozdělený	k2eAgFnPc4d1	rozdělená
na	na	k7c4	na
katolíky	katolík	k1gMnPc4	katolík
a	a	k8xC	a
utrakvisty	utrakvista	k1gMnPc4	utrakvista
<g/>
.	.	kIx.	.
</s>
<s>
Reformní	reformní	k2eAgInSc1d1	reformní
proces	proces	k1gInSc1	proces
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
byl	být	k5eAaImAgInS	být
sledován	sledovat	k5eAaImNgInS	sledovat
jak	jak	k8xC	jak
utrakvisty	utrakvista	k1gMnPc7	utrakvista
<g/>
,	,	kIx,	,
tak	tak	k9	tak
Jednotou	jednota	k1gFnSc7	jednota
bratrskou	bratrský	k2eAgFnSc7d1	bratrská
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
zásady	zásada	k1gFnPc4	zásada
luteránství	luteránství	k1gNnSc2	luteránství
začala	začít	k5eAaPmAgFnS	začít
opírat	opírat	k5eAaImF	opírat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Jagellonského	jagellonský	k2eAgMnSc2d1	jagellonský
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
zvolen	zvolit	k5eAaPmNgMnS	zvolit
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
slíbil	slíbit	k5eAaPmAgMnS	slíbit
zachovávat	zachovávat	k5eAaImF	zachovávat
náboženské	náboženský	k2eAgFnSc2d1	náboženská
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
uvolnění	uvolnění	k1gNnSc3	uvolnění
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přispělo	přispět	k5eAaPmAgNnS	přispět
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
sekty	sekta	k1gFnSc2	sekta
habrovanských	habrovanský	k2eAgNnPc2d1	habrovanský
a	a	k8xC	a
k	k	k7c3	k
příchodu	příchod	k1gInSc3	příchod
novokřtěnců	novokřtěnec	k1gMnPc2	novokřtěnec
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
Rakous	Rakousy	k1gInPc2	Rakousy
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1555	[number]	k4	1555
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
přijata	přijmout	k5eAaPmNgFnS	přijmout
zásada	zásada	k1gFnSc1	zásada
"	"	kIx"	"
<g/>
čí	čí	k3xOyQgNnSc1	čí
je	být	k5eAaImIp3nS	být
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
toho	ten	k3xDgMnSc2	ten
je	být	k5eAaImIp3nS	být
i	i	k9	i
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Cuius	Cuius	k1gMnSc1	Cuius
regio	regio	k1gMnSc1	regio
<g/>
,	,	kIx,	,
eius	eius	k6eAd1	eius
religio	religio	k6eAd1	religio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
zásady	zásada	k1gFnPc1	zásada
platily	platit	k5eAaImAgFnP	platit
i	i	k9	i
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
šlechtických	šlechtický	k2eAgNnPc2d1	šlechtické
panství	panství	k1gNnPc2	panství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
fary	fara	k1gFnPc1	fara
a	a	k8xC	a
kostely	kostel	k1gInPc1	kostel
byly	být	k5eAaImAgInP	být
obsazovány	obsazován	k2eAgMnPc4d1	obsazován
kněžími	kněz	k1gMnPc7	kněz
té	ten	k3xDgFnSc2	ten
náboženské	náboženský	k2eAgFnSc2d1	náboženská
orientace	orientace	k1gFnSc2	orientace
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vyznával	vyznávat	k5eAaImAgMnS	vyznávat
majitel	majitel	k1gMnSc1	majitel
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Poddaní	poddaný	k1gMnPc1	poddaný
pak	pak	k6eAd1	pak
museli	muset	k5eAaImAgMnP	muset
prakticky	prakticky	k6eAd1	prakticky
přijmout	přijmout	k5eAaPmF	přijmout
víru	víra	k1gFnSc4	víra
svého	svůj	k3xOyFgMnSc2	svůj
pána	pán	k1gMnSc2	pán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
reformace	reformace	k1gFnSc2	reformace
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
řada	řada	k1gFnSc1	řada
českých	český	k2eAgFnPc2d1	Česká
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vydávaly	vydávat	k5eAaImAgFnP	vydávat
jak	jak	k6eAd1	jak
reformní	reformní	k2eAgFnPc1d1	reformní
církve	církev	k1gFnPc1	církev
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
sblížit	sblížit	k5eAaPmF	sblížit
reformační	reformační	k2eAgInPc4d1	reformační
směry	směr	k1gInPc4	směr
a	a	k8xC	a
dovést	dovést	k5eAaPmF	dovést
církev	církev	k1gFnSc4	církev
ne	ne	k9	ne
k	k	k7c3	k
"	"	kIx"	"
<g/>
reformaci	reformace	k1gFnSc3	reformace
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
ale	ale	k8xC	ale
ke	k	k7c3	k
skutečné	skutečný	k2eAgFnSc3d1	skutečná
reformě	reforma	k1gFnSc3	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1575	[number]	k4	1575
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
Česká	český	k2eAgFnSc1d1	Česká
konfese	konfese	k1gFnSc1	konfese
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
potvrzovala	potvrzovat	k5eAaImAgFnS	potvrzovat
a	a	k8xC	a
upravovala	upravovat	k5eAaImAgFnS	upravovat
náboženské	náboženský	k2eAgFnPc4d1	náboženská
záležitosti	záležitost	k1gFnPc4	záležitost
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Náboženské	náboženský	k2eAgFnPc1d1	náboženská
svobody	svoboda	k1gFnPc1	svoboda
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc3	rok
1609	[number]	k4	1609
upraveny	upravit	k5eAaPmNgFnP	upravit
vydáním	vydání	k1gNnSc7	vydání
Rudolfova	Rudolfův	k2eAgInSc2d1	Rudolfův
Majestátu	majestát	k1gInSc2	majestát
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
Majestátem	majestát	k1gInSc7	majestát
se	se	k3xPyFc4	se
evangeličtí	evangelický	k2eAgMnPc1d1	evangelický
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
katoličtí	katolický	k2eAgMnPc1d1	katolický
šlechtici	šlechtic	k1gMnPc1	šlechtic
usnesli	usnést	k5eAaPmAgMnP	usnést
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Porovnání	porovnání	k1gNnSc1	porovnání
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
článcích	článek	k1gInPc6	článek
odporovalo	odporovat	k5eAaImAgNnS	odporovat
Majestátu	majestát	k1gInSc3	majestát
a	a	k8xC	a
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
různé	různý	k2eAgInPc4d1	různý
výklady	výklad	k1gInPc4	výklad
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
spor	spor	k1gInSc1	spor
v	v	k7c6	v
Broumově	Broumov	k1gInSc6	Broumov
a	a	k8xC	a
v	v	k7c6	v
Hrobech	hrob	k1gInPc6	hrob
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
evangeličtí	evangelický	k2eAgMnPc1d1	evangelický
občané	občan	k1gMnPc1	občan
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
vystavěli	vystavět	k5eAaPmAgMnP	vystavět
na	na	k7c6	na
statcích	statek	k1gInPc6	statek
broumovského	broumovský	k2eAgMnSc2d1	broumovský
opata	opat	k1gMnSc2	opat
a	a	k8xC	a
pražského	pražský	k2eAgMnSc2d1	pražský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
dva	dva	k4xCgInPc1	dva
protestantské	protestantský	k2eAgInPc1d1	protestantský
kostely	kostel	k1gInPc1	kostel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
rozepře	rozepře	k1gFnSc2	rozepře
<g/>
.	.	kIx.	.
</s>
<s>
Předáci	předák	k1gMnPc1	předák
broumovských	broumovský	k2eAgMnPc2d1	broumovský
měšťanů	měšťan	k1gMnPc2	měšťan
byli	být	k5eAaImAgMnP	být
zatčeni	zatčen	k2eAgMnPc1d1	zatčen
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1618	[number]	k4	1618
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
uvězněni	uvěznit	k5eAaPmNgMnP	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
nastartovala	nastartovat	k5eAaPmAgFnS	nastartovat
další	další	k2eAgInPc4d1	další
nepokoje	nepokoj	k1gInPc4	nepokoj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
Stavovským	stavovský	k2eAgNnSc7d1	Stavovské
povstáním	povstání	k1gNnSc7	povstání
proti	proti	k7c3	proti
králi	král	k1gMnSc3	král
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
bitvou	bitva	k1gFnSc7	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
a	a	k8xC	a
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
CHAUNU	CHAUNU	kA	CHAUNU
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gMnSc5	Pierr
<g/>
.	.	kIx.	.
</s>
<s>
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
reformace	reformace	k1gFnSc2	reformace
:	:	kIx,	:
svět	svět	k1gInSc1	svět
Jana	Jan	k1gMnSc2	Jan
Kalvína	Kalvín	k1gMnSc2	Kalvín
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
124	[number]	k4	124
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85959	[number]	k4	85959
<g/>
-	-	kIx~	-
<g/>
84	[number]	k4	84
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VRABEC	Vrabec	k1gMnSc1	Vrabec
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Popravy	poprava	k1gFnPc1	poprava
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1621	[number]	k4	1621
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Kočí	Kočí	k1gMnSc1	Kočí
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Prvních	první	k4xOgFnPc2	první
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
utraquismu	utraquismus	k1gInSc2	utraquismus
a	a	k8xC	a
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
novoutraquismu	novoutraquismus	k1gInSc2	novoutraquismus
k	k	k7c3	k
Bílé	bílý	k2eAgFnSc3d1	bílá
Hoře	hora	k1gFnSc3	hora
<g/>
,	,	kIx,	,
s.	s.	k?	s.
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
48	[number]	k4	48
<g/>
.	.	kIx.	.
</s>
<s>
Nástin	nástin	k1gInSc1	nástin
200	[number]	k4	200
let	léto	k1gNnPc2	léto
dějin	dějiny	k1gFnPc2	dějiny
české	český	k2eAgFnSc2d1	Česká
reformace	reformace	k1gFnSc2	reformace
(	(	kIx(	(
<g/>
od	od	k7c2	od
Husových	Husových	k2eAgMnPc2d1	Husových
předchůdců	předchůdce	k1gMnPc2	předchůdce
do	do	k7c2	do
r.	r.	kA	r.
1621	[number]	k4	1621
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WOLGAST	WOLGAST	kA	WOLGAST
<g/>
,	,	kIx,	,
Eike	Eike	k1gNnSc2	Eike
<g/>
.	.	kIx.	.
</s>
<s>
Religion	religion	k1gInSc1	religion
und	und	k?	und
Gewalt	Gewalt	k1gInSc1	Gewalt
in	in	k?	in
der	drát	k5eAaImRp2nS	drát
Reformation	Reformation	k1gInSc4	Reformation
<g/>
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gFnSc1	Prague
Papers	Papersa	k1gFnPc2	Papersa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
History	Histor	k1gInPc4	Histor
of	of	k?	of
International	International	k1gFnSc2	International
Relations	Relationsa	k1gFnPc2	Relationsa
<g/>
.	.	kIx.	.
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
59	[number]	k4	59
<g/>
-	-	kIx~	-
<g/>
78	[number]	k4	78
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85899	[number]	k4	85899
<g/>
-	-	kIx~	-
<g/>
49	[number]	k4	49
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Protestantismus	protestantismus	k1gInSc1	protestantismus
</s>
</p>
<p>
<s>
Novokřtěnectví	novokřtěnectví	k1gNnSc1	novokřtěnectví
</s>
</p>
<p>
<s>
Rekatolizace	rekatolizace	k1gFnSc1	rekatolizace
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
Reformace	reformace	k1gFnSc1	reformace
a	a	k8xC	a
protireformace	protireformace	k1gFnSc1	protireformace
(	(	kIx(	(
<g/>
1550	[number]	k4	1550
<g/>
-	-	kIx~	-
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
</s>
</p>
