<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
zvonek	zvonek	k1gInSc1	zvonek
jesenický	jesenický	k2eAgInSc1d1	jesenický
kriticky	kriticky	k6eAd1	kriticky
ohroženým	ohrožený	k2eAgInSc7d1	ohrožený
druhem	druh	k1gInSc7	druh
a	a	k8xC	a
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
vyhynutí	vyhynutí	k1gNnSc2	vyhynutí
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
populace	populace	k1gFnSc1	populace
zdá	zdát	k5eAaPmIp3nS	zdát
stabilizovaná	stabilizovaný	k2eAgFnSc1d1	stabilizovaná
<g/>
.	.	kIx.	.
</s>
