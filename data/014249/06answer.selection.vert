<s desamb="1">
Při	při	k7c6
délce	délka	k1gFnSc6
kolem	kolem	k7c2
2	#num#	k4
metrů	metr	k1gInPc2
(	(	kIx(
<g/>
i	i	k9
s	s	k7c7
dlouhým	dlouhý	k2eAgInSc7d1
ocasem	ocas	k1gInSc7
<g/>
)	)	kIx)
dosahoval	dosahovat	k5eAaImAgInS
hmotnosti	hmotnost	k1gFnSc2
maximálně	maximálně	k6eAd1
kolem	kolem	k7c2
10	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Fosilie	fosilie	k1gFnSc1
tohoto	tento	k3xDgMnSc2
dinosaura	dinosaurus	k1gMnSc2
jsou	být	k5eAaImIp3nP
nicméně	nicméně	k8xC
spíše	spíše	k9
fragmentární	fragmentární	k2eAgFnPc1d1
<g/>
,	,	kIx,
poukazují	poukazovat	k5eAaImIp3nP
však	však	k9
na	na	k7c4
schopnost	schopnost	k1gFnSc4
rychlého	rychlý	k2eAgInSc2d1
pohybu	pohyb	k1gInSc2
a	a	k8xC
celkovou	celkový	k2eAgFnSc4d1
mrštnost	mrštnost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
To	to	k9
byly	být	k5eAaImAgFnP
pro	pro	k7c4
menší	malý	k2eAgMnPc4d2
dinosaury	dinosaurus	k1gMnPc4
v	v	k7c6
ekosystémech	ekosystém	k1gInPc6
morrisonského	morrisonský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
nezbytné	zbytný	k2eNgFnSc1d1
vlastnosti	vlastnost	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
zde	zde	k6eAd1
nacházelo	nacházet	k5eAaImAgNnS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
dravých	dravý	k2eAgMnPc2d1
teropodů	teropod	k1gMnPc2
všech	všecek	k3xTgFnPc2
velikostních	velikostní	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
<g/>
.	.	kIx.
</s>