<s>
Nanosaurus	Nanosaurus	k1gMnSc1
</s>
<s>
NanosaurusStratigrafický	NanosaurusStratigrafický	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
<g/>
:	:	kIx,
Svrchní	svrchní	k2eAgFnSc1d1
jura	jura	k1gFnSc1
<g/>
,	,	kIx,
před	před	k7c7
155	#num#	k4
až	až	k8xS
148	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
Rekonstrukce	rekonstrukce	k1gFnSc2
kostry	kostra	k1gFnSc2
nanosaura	nanosaura	k1gFnSc1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
plazi	plaz	k1gMnPc1
(	(	kIx(
<g/>
Sauropsida	Sauropsida	k1gFnSc1
<g/>
)	)	kIx)
Nadřád	nadřád	k1gInSc1
</s>
<s>
dinosauři	dinosaurus	k1gMnPc1
(	(	kIx(
<g/>
Dinosauria	Dinosaurium	k1gNnPc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
ptakopánví	ptakopánví	k1gNnSc1
(	(	kIx(
<g/>
Ornithischia	Ornithischia	k1gFnSc1
<g/>
)	)	kIx)
Podřád	podřád	k1gInSc1
</s>
<s>
Cerapoda	Cerapoda	k1gFnSc1
Infrařád	Infrařáda	k1gFnPc2
</s>
<s>
Ornithopoda	Ornithopoda	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Čeleď	čeleď	k1gFnSc1
</s>
<s>
Nanosauridae	Nanosauridae	k6eAd1
Rod	rod	k1gInSc1
</s>
<s>
Nanosaurus	Nanosaurus	k1gInSc1
Typový	typový	k2eAgInSc4d1
druh	druh	k1gInSc4
</s>
<s>
Nanosaurus	Nanosaurus	k1gMnSc1
agilisMarsh	agilisMarsh	k1gMnSc1
<g/>
,	,	kIx,
1877	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nanosaurus	Nanosaurus	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
malinký	malinký	k1gMnSc1
ještěr	ještěr	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
rod	rod	k1gInSc1
malého	malý	k2eAgMnSc2d1
ptakopánvého	ptakopánvý	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
z	z	k7c2
kladu	klad	k1gInSc2
Neornithischia	Neornithischius	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
žil	žít	k5eAaImAgMnS
v	v	k7c6
období	období	k1gNnSc6
svrchní	svrchní	k2eAgFnSc2d1
jury	jura	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
dnešních	dnešní	k2eAgInPc2d1
států	stát	k1gInPc2
Wyoming	Wyoming	k1gInSc1
<g/>
,	,	kIx,
Colorado	Colorado	k1gNnSc1
a	a	k8xC
pravděpodobně	pravděpodobně	k6eAd1
i	i	k9
Utah	Utah	k1gInSc4
v	v	k7c6
USA	USA	kA
(	(	kIx(
<g/>
souvrství	souvrství	k1gNnSc1
Morrison	Morrisona	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
však	však	k9
o	o	k7c4
pochybný	pochybný	k2eAgInSc4d1
taxon	taxon	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
vědecky	vědecky	k6eAd1
platný	platný	k2eAgInSc4d1
(	(	kIx(
<g/>
a	a	k8xC
může	moct	k5eAaImIp3nS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
spadat	spadat	k5eAaPmF,k5eAaImF
do	do	k7c2
rodu	rod	k1gInSc2
Othnielosaurus	Othnielosaurus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Porovnání	porovnání	k1gNnSc1
velikosti	velikost	k1gFnSc2
nanosaura	nanosaur	k1gMnSc2
a	a	k8xC
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Nanosaurus	Nanosaurus	k1gMnSc1
byl	být	k5eAaImAgMnS
malým	malý	k2eAgMnSc7d1
býložravým	býložravý	k2eAgMnSc7d1
dinosaurem	dinosaurus	k1gMnSc7
<g/>
,	,	kIx,
běhajícím	běhající	k2eAgMnSc7d1
po	po	k7c6
dvou	dva	k4xCgFnPc6
zadních	zadní	k2eAgFnPc6d1
končetinách	končetina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
délce	délka	k1gFnSc6
kolem	kolem	k7c2
2	#num#	k4
metrů	metr	k1gInPc2
(	(	kIx(
<g/>
i	i	k9
s	s	k7c7
dlouhým	dlouhý	k2eAgInSc7d1
ocasem	ocas	k1gInSc7
<g/>
)	)	kIx)
dosahoval	dosahovat	k5eAaImAgInS
hmotnosti	hmotnost	k1gFnSc2
maximálně	maximálně	k6eAd1
kolem	kolem	k7c2
10	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Fosilie	fosilie	k1gFnSc1
tohoto	tento	k3xDgMnSc2
dinosaura	dinosaurus	k1gMnSc2
jsou	být	k5eAaImIp3nP
nicméně	nicméně	k8xC
spíše	spíše	k9
fragmentární	fragmentární	k2eAgFnPc1d1
<g/>
,	,	kIx,
poukazují	poukazovat	k5eAaImIp3nP
však	však	k9
na	na	k7c4
schopnost	schopnost	k1gFnSc4
rychlého	rychlý	k2eAgInSc2d1
pohybu	pohyb	k1gInSc2
a	a	k8xC
celkovou	celkový	k2eAgFnSc4d1
mrštnost	mrštnost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
To	to	k9
byly	být	k5eAaImAgFnP
pro	pro	k7c4
menší	malý	k2eAgMnPc4d2
dinosaury	dinosaurus	k1gMnPc4
v	v	k7c6
ekosystémech	ekosystém	k1gInPc6
morrisonského	morrisonský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
nezbytné	zbytný	k2eNgFnSc1d1
vlastnosti	vlastnost	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
zde	zde	k6eAd1
nacházelo	nacházet	k5eAaImAgNnS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
dravých	dravý	k2eAgMnPc2d1
teropodů	teropod	k1gMnPc2
všech	všecek	k3xTgFnPc2
velikostních	velikostní	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinou	jediný	k2eAgFnSc7d1
nadějí	naděje	k1gFnSc7
na	na	k7c4
záchranu	záchrana	k1gFnSc4
tak	tak	k6eAd1
byl	být	k5eAaImAgInS
pro	pro	k7c4
tyto	tento	k3xDgMnPc4
malé	malý	k2eAgMnPc4d1
ptakopánvé	ptakopánvý	k2eAgMnPc4d1
dinosaury	dinosaurus	k1gMnPc4
rychlý	rychlý	k2eAgInSc4d1
útěk	útěk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Blízce	blízce	k6eAd1
příbuznými	příbuzný	k2eAgInPc7d1
taxony	taxon	k1gInPc7
k	k	k7c3
nanosaurovi	nanosaur	k1gMnSc3
mohl	moct	k5eAaImAgMnS
být	být	k5eAaImF
severoamerický	severoamerický	k2eAgInSc4d1
rod	rod	k1gInSc4
Drinker	Drinkra	k1gFnPc2
a	a	k8xC
portugalský	portugalský	k2eAgInSc4d1
rod	rod	k1gInSc4
Phyllodon	Phyllodon	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
ale	ale	k9
známý	známý	k2eAgMnSc1d1
pouze	pouze	k6eAd1
podle	podle	k7c2
izolovaných	izolovaný	k2eAgInPc2d1
fosilních	fosilní	k2eAgInPc2d1
zubů	zub	k1gInPc2
a	a	k8xC
několika	několik	k4yIc2
fragmentů	fragment	k1gInPc2
dolní	dolní	k2eAgFnSc2d1
čelisti	čelist	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Příbuzným	příbuzný	k2eAgInSc7d1
taxonem	taxon	k1gInSc7
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
také	také	k9
pochybný	pochybný	k2eAgInSc4d1
rod	rod	k1gInSc4
Laosaurus	Laosaurus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Galton	Galton	k1gInSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
Jensen	Jensen	k1gInSc1
<g/>
,	,	kIx,
James	James	k1gMnSc1
A.	A.	kA
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
„	„	k?
<g/>
Skeleton	skeleton	k1gInSc1
of	of	k?
a	a	k8xC
hypsilophodontid	hypsilophodontid	k1gInSc1
dinosaur	dinosaura	k1gFnPc2
(	(	kIx(
<g/>
Nanosaurus	Nanosaurus	k1gInSc1
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
rex	rex	k?
<g/>
)	)	kIx)
from	from	k1gMnSc1
the	the	k?
Upper	Upper	k1gMnSc1
Jurassic	Jurassic	k1gMnSc1
of	of	k?
Utah	Utah	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brigham	Brigham	k1gInSc1
Young	Young	k1gInSc4
University	universita	k1gFnSc2
Geology	geolog	k1gMnPc4
Series	Seriesa	k1gFnPc2
<g/>
.	.	kIx.
20	#num#	k4
<g/>
:	:	kIx,
137	#num#	k4
<g/>
–	–	k?
<g/>
157	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Penélope	Penélop	k1gInSc5
Cruzado-Caballero	Cruzado-Caballera	k1gFnSc5
<g/>
,	,	kIx,
Ignacio	Ignacio	k1gMnSc1
Díaz-Martínez	Díaz-Martínez	k1gMnSc1
<g/>
,	,	kIx,
Bruce	Bruce	k1gMnSc1
Rothschild	Rothschild	k1gMnSc1
<g/>
,	,	kIx,
Malcolm	Malcolm	k1gMnSc1
Bedell	Bedell	k1gMnSc1
&	&	k?
Xabier	Xabier	k1gInSc1
Pereda-Suberbiola	Pereda-Suberbiola	k1gFnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
limping	limping	k1gInSc1
dinosaur	dinosaur	k1gMnSc1
in	in	k?
the	the	k?
Late	lat	k1gInSc5
Jurassic	Jurassic	k1gMnSc1
<g/>
:	:	kIx,
Pathologies	Pathologies	k1gMnSc1
in	in	k?
the	the	k?
pes	pes	k1gMnSc1
of	of	k?
the	the	k?
neornithischian	neornithischian	k1gInSc1
Othnielosaurus	Othnielosaurus	k1gMnSc1
consors	consorsa	k1gFnPc2
from	from	k1gMnSc1
the	the	k?
Morrison	Morrison	k1gInSc1
Formation	Formation	k1gInSc1
(	(	kIx(
<g/>
Upper	Upper	k1gMnSc1
Jurassic	Jurassic	k1gMnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historical	Historical	k1gFnSc1
Biology	biolog	k1gMnPc4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1080/08912963.2020.1734589	https://doi.org/10.1080/08912963.2020.1734589	k4
<g/>
↑	↑	k?
Foster	Foster	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
R.	R.	kA
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
„	„	k?
<g/>
Paleoecological	Paleoecological	k1gMnSc1
Analysis	Analysis	k1gFnSc2
of	of	k?
the	the	k?
Vertebrate	Vertebrat	k1gInSc5
Fauna	Faun	k1gMnSc4
of	of	k?
the	the	k?
Morrison	Morrison	k1gInSc1
Formation	Formation	k1gInSc1
(	(	kIx(
<g/>
Upper	Upper	k1gMnSc1
Jurassic	Jurassic	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Rocky	rock	k1gInPc1
Mountain	Mountain	k2eAgInSc1d1
Region	region	k1gInSc4
<g/>
,	,	kIx,
U.	U.	kA
<g/>
S.	S.	kA
<g/>
A.	A.	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
Mexico	Mexico	k1gMnSc1
Museum	museum	k1gNnSc1
of	of	k?
Natural	Natural	k?
History	Histor	k1gInPc4
and	and	k?
Science	Science	k1gFnSc1
Bulletin	bulletin	k1gInSc1
<g/>
.	.	kIx.
23	#num#	k4
<g/>
:	:	kIx,
29	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Norman	Norman	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
B.	B.	kA
<g/>
;	;	kIx,
Sues	Sues	k1gInSc1
<g/>
,	,	kIx,
Hans-Dieter	Hans-Dieter	k1gInSc1
<g/>
;	;	kIx,
Witmer	Witmer	k1gInSc1
<g/>
,	,	kIx,
Larry	Larr	k1gInPc1
M.	M.	kA
<g/>
;	;	kIx,
Coria	Corius	k1gMnSc2
<g/>
,	,	kIx,
Rodolfo	Rodolfo	k6eAd1
A.	A.	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
„	„	k?
<g/>
Basal	Basal	k1gMnSc1
Ornithopoda	Ornithopoda	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
Weishampel	Weishampel	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
B.	B.	kA
<g/>
;	;	kIx,
Dodson	Dodson	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
;	;	kIx,
Osmólska	Osmólska	k1gFnSc1
<g/>
,	,	kIx,
Halszka	Halszka	k1gFnSc1
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Dinosauria	Dinosaurium	k1gNnSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
nd	nd	k?
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berkeley	Berkelea	k1gFnPc1
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
California	Californium	k1gNnSc2
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
pp	pp	k?
<g/>
.	.	kIx.
393	#num#	k4
<g/>
–	–	k?
<g/>
412	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
520	#num#	k4
<g/>
-	-	kIx~
<g/>
24209	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Thulborn	Thulborn	k1gInSc1
<g/>
,	,	kIx,
R.	R.	kA
A.	A.	kA
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
„	„	k?
<g/>
Teeth	Teeth	k1gInSc1
of	of	k?
ornithischian	ornithischian	k1gInSc1
dinosaurs	dinosaursa	k1gFnPc2
from	from	k1gMnSc1
the	the	k?
Upper	Upper	k1gMnSc1
Jurassic	Jurassic	k1gMnSc1
of	of	k?
Portugal	portugal	k1gInSc1
<g/>
,	,	kIx,
with	with	k1gInSc1
description	description	k1gInSc1
of	of	k?
a	a	k8xC
hypsilophodontid	hypsilophodontid	k1gInSc1
(	(	kIx(
<g/>
Phyllodon	Phyllodon	k1gNnSc1
henkeli	henkel	k1gInPc7
gen.	gen.	kA
et	et	k?
sp	sp	k?
<g/>
.	.	kIx.
nov	nov	k1gInSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
from	from	k6eAd1
the	the	k?
Guimarota	Guimarota	k1gFnSc1
lignite	lignit	k1gInSc5
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Memória	Memórium	k1gNnPc1
Serivoços	Serivoços	k1gMnSc1
Geológicos	Geológicos	k1gMnSc1
de	de	k?
Portugal	portugal	k1gInSc1
(	(	kIx(
<g/>
Nova	nova	k1gFnSc1
Série	série	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
22	#num#	k4
<g/>
:	:	kIx,
89	#num#	k4
<g/>
–	–	k?
<g/>
134	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Galton	Galton	k1gInSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
M.	M.	kA
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
„	„	k?
<g/>
Teeth	Teeth	k1gInSc1
of	of	k?
ornithischian	ornithischian	k1gInSc1
dinosaurs	dinosaurs	k1gInSc1
(	(	kIx(
<g/>
mostly	mostnout	k5eAaPmAgFnP
Ornithopoda	Ornithopoda	k1gFnSc1
<g/>
)	)	kIx)
from	from	k1gInSc1
the	the	k?
Morrison	Morrison	k1gInSc1
Formation	Formation	k1gInSc1
(	(	kIx(
<g/>
Upper	Upper	k1gMnSc1
Jurassic	Jurassic	k1gMnSc1
<g/>
)	)	kIx)
of	of	k?
the	the	k?
western	western	k1gInSc1
United	United	k1gMnSc1
States	States	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
Carpenter	Carpenter	k1gMnSc1
<g/>
,	,	kIx,
Kenneth	Kenneth	k1gMnSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horns	Horns	k1gInSc1
and	and	k?
Beaks	Beaks	k1gInSc1
<g/>
:	:	kIx,
Ceratopsian	Ceratopsian	k1gInSc1
and	and	k?
Ornithopod	Ornithopod	k1gInSc1
Dinosaurs	Dinosaurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bloomington	Bloomington	k1gInSc1
and	and	k?
Indianapolis	Indianapolis	k1gInSc1
<g/>
:	:	kIx,
Indiana	Indiana	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
pp	pp	k?
<g/>
.	.	kIx.
17	#num#	k4
<g/>
–	–	k?
<g/>
47	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
253	#num#	k4
<g/>
-	-	kIx~
<g/>
34817	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Marsh	Marsh	k1gMnSc1
<g/>
,	,	kIx,
O.	O.	kA
C.	C.	kA
(	(	kIx(
<g/>
1877	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Notice	Notice	k?
of	of	k?
new	new	k?
dinosaurian	dinosaurian	k1gMnSc1
reptiles	reptiles	k1gMnSc1
from	from	k1gMnSc1
the	the	k?
Jurassic	Jurassic	k1gMnSc1
formations	formations	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Sciences	Sciences	k1gMnSc1
(	(	kIx(
<g/>
Series	Series	k1gInSc1
3	#num#	k4
<g/>
)	)	kIx)
14	#num#	k4
<g/>
:	:	kIx,
514	#num#	k4
<g/>
-	-	kIx~
<g/>
516	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Galton	Galton	k1gInSc1
<g/>
,	,	kIx,
P.	P.	kA
M.	M.	kA
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
ornithopod	ornithopoda	k1gFnPc2
dinosaur	dinosaur	k1gMnSc1
Dryosaurus	Dryosaurus	k1gMnSc1
and	and	k?
a	a	k8xC
Laurasia-Gondwanaland	Laurasia-Gondwanaland	k1gInSc1
connection	connection	k1gInSc1
in	in	k?
the	the	k?
Upper	Upper	k1gMnSc1
Jurassic	Jurassic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
268	#num#	k4
<g/>
:	:	kIx,
230	#num#	k4
<g/>
-	-	kIx~
<g/>
232	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
nanosaurus	nanosaurus	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Článek	článek	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
Equatorial	Equatorial	k1gInSc1
Minnesota	Minnesota	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Profil	profil	k1gInSc1
na	na	k7c4
databázi	databáze	k1gFnSc4
Fossilworks	Fossilworksa	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
