<p>
<s>
Anyone	Anyon	k1gMnSc5	Anyon
Who	Who	k1gMnSc1	Who
Had	had	k1gMnSc1	had
a	a	k8xC	a
Heart	Heart	k1gInSc1	Heart
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
americké	americký	k2eAgFnSc2d1	americká
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Dionne	Dionn	k1gInSc5	Dionn
Warwick	Warwicka	k1gFnPc2	Warwicka
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
ve	v	k7c6	v
vydavatelství	vydavatelství	k1gNnSc6	vydavatelství
Scepter	sceptrum	k1gNnPc2	sceptrum
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Producenty	producent	k1gMnPc4	producent
byl	být	k5eAaImAgInS	být
Burt	Burt	k1gInSc1	Burt
Bacharach	Bacharacha	k1gFnPc2	Bacharacha
a	a	k8xC	a
Hal	hala	k1gFnPc2	hala
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
pozoruhodné	pozoruhodný	k2eAgNnSc1d1	pozoruhodné
pro	pro	k7c4	pro
začlenění	začlenění	k1gNnSc4	začlenění
titulní	titulní	k2eAgFnSc2d1	titulní
písně	píseň	k1gFnSc2	píseň
Anyone	Anyon	k1gInSc5	Anyon
Who	Who	k1gMnPc1	Who
Had	had	k1gMnSc1	had
a	a	k8xC	a
Heart	Heart	k1gInSc1	Heart
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
písní	píseň	k1gFnSc7	píseň
v	v	k7c6	v
desítce	desítka	k1gFnSc6	desítka
v	v	k7c4	v
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
Strana	strana	k1gFnSc1	strana
A	A	kA	A
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Anyone	Anyon	k1gMnSc5	Anyon
Who	Who	k1gMnSc1	Who
Had	had	k1gMnSc1	had
a	a	k8xC	a
Heart	Heart	k1gInSc1	Heart
(	(	kIx(	(
<g/>
Burt	Burt	k1gInSc1	Burt
Bacharach	Bacharacha	k1gFnPc2	Bacharacha
<g/>
/	/	kIx~	/
<g/>
Hal	halit	k5eAaImRp2nS	halit
David	David	k1gMnSc1	David
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
11	[number]	k4	11
</s>
</p>
<p>
<s>
Shall	Shalnout	k5eAaPmAgInS	Shalnout
I	i	k9	i
Tell	Tell	k1gInSc1	Tell
Her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
Doc	doc	kA	doc
Pomus	Pomus	k1gMnSc1	Pomus
<g/>
/	/	kIx~	/
<g/>
Mort	Mort	k1gMnSc1	Mort
Shuman	Shuman	k1gMnSc1	Shuman
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
33	[number]	k4	33
</s>
</p>
<p>
<s>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Make	Make	k1gInSc1	Make
Me	Me	k1gMnSc1	Me
Over	Over	k1gMnSc1	Over
(	(	kIx(	(
<g/>
Burt	Burt	k1gInSc1	Burt
Bacharach	Bacharacha	k1gFnPc2	Bacharacha
<g/>
/	/	kIx~	/
<g/>
Hal	halit	k5eAaImRp2nS	halit
David	David	k1gMnSc1	David
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
46	[number]	k4	46
</s>
</p>
<p>
<s>
I	i	k9	i
Cry	Cry	k1gMnSc5	Cry
Alone	Alon	k1gMnSc5	Alon
(	(	kIx(	(
<g/>
Burt	Burt	k2eAgInSc4d1	Burt
Bacharach	Bacharach	k1gInSc4	Bacharach
<g/>
/	/	kIx~	/
<g/>
Hal	halit	k5eAaImRp2nS	halit
David	David	k1gMnSc1	David
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
37	[number]	k4	37
</s>
</p>
<p>
<s>
Getting	Getting	k1gInSc1	Getting
Ready	ready	k0	ready
For	forum	k1gNnPc2	forum
The	The	k1gMnSc1	The
Heartbreak	Heartbreak	k1gMnSc1	Heartbreak
(	(	kIx(	(
<g/>
Lockie	Lockie	k1gFnSc1	Lockie
Edwards	Edwards	k1gInSc1	Edwards
Jr	Jr	k1gFnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
Larry	Larra	k1gMnSc2	Larra
Weiss	Weiss	k1gMnSc1	Weiss
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
</s>
</p>
<p>
<s>
Oh	oh	k0	oh
Lord	lord	k1gMnSc1	lord
<g/>
,	,	kIx,	,
What	What	k1gMnSc1	What
Are	ar	k1gInSc5	ar
You	You	k1gMnPc1	You
Doing	Doinga	k1gFnPc2	Doinga
to	ten	k3xDgNnSc4	ten
Me	Me	k1gMnSc1	Me
(	(	kIx(	(
<g/>
Luther	Luthra	k1gFnPc2	Luthra
Dixon	Dixona	k1gFnPc2	Dixona
<g/>
/	/	kIx~	/
<g/>
Bert	Berta	k1gFnPc2	Berta
Keyes	Keyes	k1gInSc1	Keyes
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
14	[number]	k4	14
<g/>
Strana	strana	k1gFnSc1	strana
B	B	kA	B
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Any	Any	k?	Any
Old	Olda	k1gFnPc2	Olda
Time	Time	k1gFnSc1	Time
of	of	k?	of
Day	Day	k1gFnSc2	Day
(	(	kIx(	(
<g/>
Burt	Burt	k2eAgInSc4d1	Burt
Bacharach	Bacharach	k1gInSc4	Bacharach
<g/>
/	/	kIx~	/
<g/>
Hal	halit	k5eAaImRp2nS	halit
David	David	k1gMnSc1	David
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
</s>
</p>
<p>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Heartbreak	Heartbreak	k1gInSc1	Heartbreak
(	(	kIx(	(
<g/>
Barbara	Barbara	k1gFnSc1	Barbara
English	Englisha	k1gFnPc2	Englisha
<g/>
/	/	kIx~	/
<g/>
Al	ala	k1gFnPc2	ala
Cleveland	Cleveland	k1gInSc1	Cleveland
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
33	[number]	k4	33
</s>
</p>
<p>
<s>
Put	puta	k1gFnPc2	puta
Yourself	Yourself	k1gMnSc1	Yourself
in	in	k?	in
My	my	k3xPp1nPc1	my
Place	plac	k1gInSc6	plac
(	(	kIx(	(
<g/>
Reggie	Reggie	k1gFnSc1	Reggie
Obrecht	Obrecht	k1gMnSc1	Obrecht
<g/>
/	/	kIx~	/
<g/>
William	William	k1gInSc1	William
Drain	Drain	k1gInSc1	Drain
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
</s>
</p>
<p>
<s>
I	i	k9	i
Could	Could	k1gInSc1	Could
Make	Mak	k1gMnSc2	Mak
You	You	k1gMnSc2	You
Mine	minout	k5eAaImIp3nS	minout
(	(	kIx(	(
<g/>
Burt	Burt	k2eAgInSc4d1	Burt
Bacharach	Bacharach	k1gInSc4	Bacharach
<g/>
/	/	kIx~	/
<g/>
Hal	halit	k5eAaImRp2nS	halit
David	David	k1gMnSc1	David
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
</s>
</p>
<p>
<s>
This	This	k1gInSc1	This
Empty	Empta	k1gFnSc2	Empta
Place	plac	k1gInSc6	plac
(	(	kIx(	(
<g/>
Burt	Burt	k2eAgInSc4d1	Burt
Bacharach	Bacharach	k1gInSc4	Bacharach
<g/>
/	/	kIx~	/
<g/>
Hal	halit	k5eAaImRp2nS	halit
David	David	k1gMnSc1	David
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
</s>
</p>
<p>
<s>
Please	Please	k6eAd1	Please
Make	Make	k1gFnSc1	Make
Him	Him	k1gFnSc2	Him
Love	lov	k1gInSc5	lov
Me	Me	k1gFnSc3	Me
(	(	kIx(	(
<g/>
Burt	Burt	k2eAgInSc4d1	Burt
Bacharach	Bacharach	k1gInSc4	Bacharach
<g/>
/	/	kIx~	/
<g/>
Hal	halit	k5eAaImRp2nS	halit
David	David	k1gMnSc1	David
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
33	[number]	k4	33
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Discogs	Discogs	k1gInSc1	Discogs
</s>
</p>
