<s>
Ján	Ján	k1gMnSc1	Ján
Levoslav	Levoslav	k1gMnSc1	Levoslav
Bella	Bella	k1gMnSc1	Bella
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1843	[number]	k4	1843
Liptovský	liptovský	k2eAgInSc1d1	liptovský
Mikuláš	mikuláš	k1gInSc1	mikuláš
<g/>
,	,	kIx,	,
Uhersko	Uhersko	k1gNnSc1	Uhersko
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1936	[number]	k4	1936
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
duchovní	duchovní	k1gMnSc1	duchovní
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
sbormistr	sbormistr	k1gMnSc1	sbormistr
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
publicista	publicista	k1gMnSc1	publicista
a	a	k8xC	a
varhaník	varhaník	k1gMnSc1	varhaník
<g/>
.	.	kIx.	.
</s>
<s>
Užíval	užívat	k5eAaImAgMnS	užívat
pseudonymy	pseudonym	k1gInPc7	pseudonym
Janko	Janko	k1gMnSc1	Janko
Pravdomil	Pravdomil	k1gMnSc1	Pravdomil
a	a	k8xC	a
Poludničan	Poludničan	k1gMnSc1	Poludničan
<g/>
.	.	kIx.	.
</s>

