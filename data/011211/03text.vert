<p>
<s>
Ján	Ján	k1gMnSc1	Ján
Levoslav	Levoslav	k1gMnSc1	Levoslav
Bella	Bella	k1gMnSc1	Bella
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1843	[number]	k4	1843
Liptovský	liptovský	k2eAgInSc1d1	liptovský
Mikuláš	mikuláš	k1gInSc1	mikuláš
<g/>
,	,	kIx,	,
Uhersko	Uhersko	k1gNnSc1	Uhersko
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1936	[number]	k4	1936
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
duchovní	duchovní	k1gMnSc1	duchovní
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
sbormistr	sbormistr	k1gMnSc1	sbormistr
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
publicista	publicista	k1gMnSc1	publicista
a	a	k8xC	a
varhaník	varhaník	k1gMnSc1	varhaník
<g/>
.	.	kIx.	.
</s>
<s>
Užíval	užívat	k5eAaImAgMnS	užívat
pseudonymy	pseudonym	k1gInPc7	pseudonym
Janko	Janko	k1gMnSc1	Janko
Pravdomil	Pravdomil	k1gMnSc1	Pravdomil
a	a	k8xC	a
Poludničan	Poludničan	k1gMnSc1	Poludničan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bodový	bodový	k2eAgInSc4d1	bodový
životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
1853	[number]	k4	1853
<g/>
–	–	k?	–
<g/>
1861	[number]	k4	1861
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Levoči	Levoča	k1gFnSc6	Levoča
a	a	k8xC	a
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
</s>
</p>
<p>
<s>
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1863	[number]	k4	1863
studia	studio	k1gNnSc2	studio
teologie	teologie	k1gFnSc2	teologie
na	na	k7c6	na
semináři	seminář	k1gInSc6	seminář
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
</s>
</p>
<p>
<s>
1863	[number]	k4	1863
<g/>
–	–	k?	–
<g/>
1865	[number]	k4	1865
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
</s>
</p>
<p>
<s>
1866	[number]	k4	1866
přijal	přijmout	k5eAaPmAgMnS	přijmout
kněžské	kněžský	k2eAgNnSc4d1	kněžské
svěcení	svěcení	k1gNnSc4	svěcení
</s>
</p>
<p>
<s>
1928	[number]	k4	1928
dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
h.c.	h.c.	k?	h.c.
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
UK	UK	kA	UK
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
</s>
</p>
<p>
<s>
1865	[number]	k4	1865
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
</s>
</p>
<p>
<s>
1866	[number]	k4	1866
regenschori	regenschori	k1gMnSc1	regenschori
katedrálního	katedrální	k2eAgInSc2d1	katedrální
chrámu	chrám	k1gInSc2	chrám
</s>
</p>
<p>
<s>
1869	[number]	k4	1869
městský	městský	k2eAgInSc1d1	městský
kapelník	kapelník	k1gMnSc1	kapelník
v	v	k7c6	v
Kremnici	Kremnica	k1gFnSc6	Kremnica
</s>
</p>
<p>
<s>
1881	[number]	k4	1881
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
varhaník	varhaník	k1gMnSc1	varhaník
v	v	k7c6	v
rumunském	rumunský	k2eAgNnSc6d1	rumunské
Sibiu	Sibium	k1gNnSc6	Sibium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
konvertoval	konvertovat	k5eAaBmAgMnS	konvertovat
na	na	k7c4	na
protestantismus	protestantismus	k1gInSc4	protestantismus
a	a	k8xC	a
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
</s>
</p>
<p>
<s>
profesor	profesor	k1gMnSc1	profesor
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
hudebního	hudební	k2eAgInSc2d1	hudební
spolku	spolek	k1gInSc2	spolek
</s>
</p>
<p>
<s>
1901	[number]	k4	1901
sbormistr	sbormistr	k1gMnSc1	sbormistr
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
pěveckého	pěvecký	k2eAgInSc2d1	pěvecký
sboru	sbor	k1gInSc2	sbor
Hermania	Hermanium	k1gNnSc2	Hermanium
</s>
</p>
<p>
<s>
1916	[number]	k4	1916
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
</s>
</p>
<p>
<s>
1917	[number]	k4	1917
žil	žít	k5eAaImAgMnS	žít
Sibiu	Sibius	k1gMnSc3	Sibius
</s>
</p>
<p>
<s>
1921	[number]	k4	1921
opět	opět	k6eAd1	opět
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
</s>
</p>
<p>
<s>
1928	[number]	k4	1928
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
</s>
</p>
<p>
<s>
==	==	k?	==
Stručná	stručný	k2eAgFnSc1d1	stručná
charakteristika	charakteristika	k1gFnSc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
výrazného	výrazný	k2eAgMnSc4d1	výrazný
reprezentanta	reprezentant	k1gMnSc4	reprezentant
slovenské	slovenský	k2eAgFnSc2d1	slovenská
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
svojí	svojit	k5eAaImIp3nS	svojit
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
tvorbou	tvorba	k1gFnSc7	tvorba
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
emancipaci	emancipace	k1gFnSc3	emancipace
jakožto	jakožto	k8xS	jakožto
národní	národní	k2eAgFnSc2d1	národní
hudby	hudba	k1gFnSc2	hudba
na	na	k7c6	na
profesionální	profesionální	k2eAgFnSc6d1	profesionální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
hudebního	hudební	k2eAgNnSc2d1	hudební
vzdělání	vzdělání	k1gNnSc2	vzdělání
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
prohloubil	prohloubit	k5eAaPmAgMnS	prohloubit
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
na	na	k7c6	na
levočském	levočský	k2eAgNnSc6d1	Levočské
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
osvojil	osvojit	k5eAaPmAgMnS	osvojit
jak	jak	k8xC	jak
základy	základ	k1gInPc4	základ
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
některé	některý	k3yIgInPc4	některý
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
hudební	hudební	k2eAgFnSc4d1	hudební
teorii	teorie	k1gFnSc4	teorie
a	a	k8xC	a
teorii	teorie	k1gFnSc4	teorie
skladby	skladba	k1gFnSc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělával	vzdělávat	k5eAaImAgInS	vzdělávat
se	se	k3xPyFc4	se
také	také	k9	také
u	u	k7c2	u
Jána	Ján	k1gMnSc2	Ján
Egryho	Egry	k1gMnSc2	Egry
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
sbor	sbor	k1gInSc4	sbor
a	a	k8xC	a
smyčcové	smyčcový	k2eAgNnSc1d1	smyčcové
kvarteto	kvarteto	k1gNnSc1	kvarteto
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgMnPc4	který
komponoval	komponovat	k5eAaImAgMnS	komponovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svých	svůj	k3xOyFgNnPc2	svůj
teologických	teologický	k2eAgNnPc2d1	teologické
studií	studio	k1gNnPc2	studio
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
instrumentální	instrumentální	k2eAgFnSc2d1	instrumentální
mše	mše	k1gFnSc2	mše
<g/>
,	,	kIx,	,
moteta	moteto	k1gNnSc2	moteto
a	a	k8xC	a
světskou	světský	k2eAgFnSc4d1	světská
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
apod.	apod.	kA	apod.
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Štefana	Štefan	k1gMnSc2	Štefan
Moyzese	Moyzese	k1gFnSc2	Moyzese
ukončil	ukončit	k5eAaPmAgInS	ukončit
teologické	teologický	k2eAgNnSc4d1	teologické
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
semináři	seminář	k1gInSc6	seminář
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
varhaníkem	varhaník	k1gMnSc7	varhaník
a	a	k8xC	a
dirigentem	dirigent	k1gMnSc7	dirigent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
reprezentantů	reprezentant	k1gMnPc2	reprezentant
slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
hnutí	hnutí	k1gNnSc2	hnutí
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předním	přední	k2eAgMnSc7d1	přední
bojovníkem	bojovník	k1gMnSc7	bojovník
za	za	k7c4	za
národní	národní	k2eAgNnSc4d1	národní
osvobození	osvobození	k1gNnSc4	osvobození
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
autora	autor	k1gMnSc2	autor
první	první	k4xOgFnSc2	první
slovenské	slovenský	k2eAgFnSc2d1	slovenská
opery	opera	k1gFnSc2	opera
Kovář	Kovář	k1gMnSc1	Kovář
Wieland	Wieland	k1gInSc1	Wieland
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komorní	komorní	k2eAgFnSc2d1	komorní
hudby	hudba	k1gFnSc2	hudba
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
smyčcová	smyčcový	k2eAgNnPc1d1	smyčcové
kvarteta	kvarteto	k1gNnPc1	kvarteto
<g/>
,	,	kIx,	,
varhanní	varhanní	k2eAgFnPc1d1	varhanní
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
orchestrální	orchestrální	k2eAgFnPc1d1	orchestrální
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
písně	píseň	k1gFnPc1	píseň
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
sbory	sbor	k1gInPc1	sbor
<g/>
,	,	kIx,	,
světské	světský	k2eAgFnPc4d1	světská
kantáty	kantáta	k1gFnPc4	kantáta
<g/>
.	.	kIx.	.
</s>
<s>
Publikoval	publikovat	k5eAaBmAgMnS	publikovat
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
články	článek	k1gInPc4	článek
o	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
hudebně	hudebně	k6eAd1	hudebně
estetické	estetický	k2eAgInPc1d1	estetický
eseje	esej	k1gInPc1	esej
a	a	k8xC	a
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
národní	národní	k2eAgFnSc6d1	národní
a	a	k8xC	a
církevní	církevní	k2eAgFnSc6d1	církevní
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Rudolf	Rudolf	k1gMnSc1	Rudolf
Bella	Bella	k1gMnSc1	Bella
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
rovněž	rovněž	k9	rovněž
hudebním	hudební	k2eAgMnSc7d1	hudební
skladatelem	skladatel	k1gMnSc7	skladatel
a	a	k8xC	a
dirigentem	dirigent	k1gMnSc7	dirigent
<g/>
.	.	kIx.	.
</s>
<s>
Vnučka	vnučka	k1gFnSc1	vnučka
Dagmar	Dagmar	k1gFnSc1	Dagmar
Bellová	Bellová	k1gFnSc1	Bellová
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
koncertní	koncertní	k2eAgFnSc7d1	koncertní
klavíristkou	klavíristka	k1gFnSc7	klavíristka
a	a	k8xC	a
hudební	hudební	k2eAgFnSc7d1	hudební
pedagožkou	pedagožka	k1gFnSc7	pedagožka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ján	Ján	k1gMnSc1	Ján
Levoslav	Levoslav	k1gMnSc1	Levoslav
Bella	Bella	k1gMnSc1	Bella
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Kdo	kdo	k3yQnSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yQnSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
–	–	k?	–
<g/>
M	M	kA	M
/	/	kIx~	/
Milan	Milan	k1gMnSc1	Milan
Churaň	Churaň	k1gMnSc1	Churaň
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
..	..	k?	..
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
467	[number]	k4	467
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československý	československý	k2eAgInSc1d1	československý
hudební	hudební	k2eAgInSc1d1	hudební
slovník	slovník	k1gInSc1	slovník
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
institucí	instituce	k1gFnPc2	instituce
I.	I.	kA	I.
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
–	–	k?	–
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Státní	státní	k2eAgNnSc1d1	státní
hudební	hudební	k2eAgNnSc1d1	hudební
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
s.	s.	k?	s.
71	[number]	k4	71
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Godár	Godár	k1gMnSc1	Godár
<g/>
:	:	kIx,	:
Ján	Ján	k1gMnSc1	Ján
Levoslav	Levoslav	k1gMnSc1	Levoslav
Bella	Bella	k1gMnSc1	Bella
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
100	[number]	k4	100
slovenských	slovenský	k2eAgNnPc2d1	slovenské
skladateľov	skladateľovo	k1gNnPc2	skladateľovo
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
<g/>
.	.	kIx.	.
</s>
<s>
Marián	Marián	k1gMnSc1	Marián
Jurík	Jurík	k1gMnSc1	Jurík
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Zagar	Zagar	k1gMnSc1	Zagar
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
:	:	kIx,	:
Národné	Národný	k2eAgNnSc1d1	Národné
hudobné	hudobný	k2eAgNnSc1d1	hudobný
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
s.	s.	k?	s.
35	[number]	k4	35
–	–	k?	–
36	[number]	k4	36
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Dostupné	dostupný	k2eAgFnPc1d1	dostupná
ZDE	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ján	Ján	k1gMnSc1	Ján
Levoslav	Levoslav	k1gMnSc1	Levoslav
Bella	Bella	k1gMnSc1	Bella
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Ján	Ján	k1gMnSc1	Ján
Levoslav	Levoslav	k1gMnSc1	Levoslav
Bella	Bella	k1gMnSc1	Bella
–	–	k?	–
Prvý	prvý	k4xOgInSc1	prvý
slovenský	slovenský	k2eAgMnSc1d1	slovenský
skladateľ	skladateľ	k?	skladateľ
–	–	k?	–
profesionál	profesionál	k1gMnSc1	profesionál
(	(	kIx(	(
<g/>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národná	národný	k2eAgFnSc1d1	národná
knižnica	knižnica	k1gFnSc1	knižnica
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
životopis	životopis	k1gInSc1	životopis
(	(	kIx(	(
<g/>
Osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Svátek	svátek	k1gInSc4	svátek
panenky	panenka	k1gFnSc2	panenka
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
ruce	ruka	k1gFnPc4	ruka
komponovaná	komponovaný	k2eAgNnPc1d1	komponované
pro	pro	k7c4	pro
vnučku	vnučka	k1gFnSc4	vnučka
Dagmar	Dagmar	k1gFnSc4	Dagmar
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
skladeb	skladba	k1gFnPc2	skladba
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Slovenský	slovenský	k2eAgInSc1d1	slovenský
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
</s>
</p>
