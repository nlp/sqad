<s>
Sklep	sklep	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
podzemním	podzemní	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Divadlo	divadlo	k1gNnSc1
Sklep	sklep	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vinný	vinný	k2eAgInSc1d1
sklep	sklep	k1gInSc1
v	v	k7c6
</s>
<s>
Čejkovicích	Čejkovice	k1gFnPc6
s	s	k7c7
klenutým	klenutý	k2eAgInSc7d1
stropem	strop	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sklep	sklep	k1gInSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
i	i	k9
sklepy	sklep	k1gInPc4
či	či	k8xC
sklepení	sklepení	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
prostor	prostor	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
podlaha	podlaha	k1gFnSc1
se	se	k3xPyFc4
celá	celý	k2eAgFnSc1d1
nebo	nebo	k8xC
z	z	k7c2
větší	veliký	k2eAgFnSc2d2
části	část	k1gFnSc2
nachází	nacházet	k5eAaImIp3nS
pod	pod	k7c7
úrovní	úroveň	k1gFnSc7
terénu	terén	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sklep	sklep	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
umístěn	umístit	k5eAaPmNgInS
pod	pod	k7c7
budovou	budova	k1gFnSc7
nebo	nebo	k8xC
stát	stát	k5eAaPmF,k5eAaImF
samostatně	samostatně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sklepy	sklep	k1gInPc7
jsou	být	k5eAaImIp3nP
vhodné	vhodný	k2eAgInPc1d1
pro	pro	k7c4
uskladnění	uskladnění	k1gNnSc4
potravin	potravina	k1gFnPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
nápojů	nápoj	k1gInPc2
(	(	kIx(
<g/>
pivo	pivo	k1gNnSc1
<g/>
,	,	kIx,
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
medovina	medovina	k1gFnSc1
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
v	v	k7c6
nich	on	k3xPp3gInPc6
drží	držet	k5eAaImIp3nS
stálá	stálý	k2eAgFnSc1d1
<g/>
,	,	kIx,
poměrně	poměrně	k6eAd1
nízká	nízký	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Užívají	užívat	k5eAaImIp3nP
se	se	k3xPyFc4
též	též	k9
k	k	k7c3
uskladnění	uskladnění	k1gNnSc3
materiálu	materiál	k1gInSc2
<g/>
,	,	kIx,
topiva	topivo	k1gNnSc2
<g/>
,	,	kIx,
přebytečných	přebytečný	k2eAgInPc2d1
věcí	věc	k1gFnSc7
apod.	apod.	kA
Při	při	k7c6
bombardování	bombardování	k1gNnSc6
nebo	nebo	k8xC
během	během	k7c2
přírodní	přírodní	k2eAgFnSc2d1
pohromy	pohroma	k1gFnSc2
(	(	kIx(
<g/>
tornádo	tornádo	k1gNnSc1
<g/>
)	)	kIx)
mohou	moct	k5eAaImIp3nP
sloužit	sloužit	k5eAaImF
též	též	k9
jako	jako	k9
úkryt	úkryt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Podsklepení	podsklepení	k1gNnSc1
obytného	obytný	k2eAgInSc2d1
domu	dům	k1gInSc2
obvykle	obvykle	k6eAd1
pomáhá	pomáhat	k5eAaImIp3nS
k	k	k7c3
udržení	udržení	k1gNnSc3
vnitřní	vnitřní	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
během	během	k7c2
zimního	zimní	k2eAgNnSc2d1
období	období	k1gNnSc2
a	a	k8xC
často	často	k6eAd1
také	také	k9
omezuje	omezovat	k5eAaImIp3nS
vlhkost	vlhkost	k1gFnSc1
zdiva	zdivo	k1gNnSc2
ve	v	k7c6
vyšších	vysoký	k2eAgNnPc6d2
podlažích	podlaží	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
slova	slovo	k1gNnSc2
a	a	k8xC
terminologie	terminologie	k1gFnSc2
</s>
<s>
Slovo	slovo	k1gNnSc1
sklep	sklep	k1gInSc1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
výrazem	výraz	k1gInSc7
klenba	klenba	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
původní	původní	k2eAgInPc4d1
sklepy	sklep	k1gInPc4
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
byly	být	k5eAaImAgFnP
podzemní	podzemní	k2eAgFnPc1d1
prostory	prostora	k1gFnPc1
s	s	k7c7
klenutým	klenutý	k2eAgInSc7d1
stropem	strop	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinak	jinak	k6eAd1
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
podzemní	podzemní	k2eAgInPc4d1
prostory	prostor	k1gInPc4
mívaly	mívat	k5eAaImAgInP
stropy	strop	k1gInPc1
rovné	rovný	k2eAgInPc1d1
<g/>
,	,	kIx,
nazývaly	nazývat	k5eAaImAgInP
se	se	k3xPyFc4
(	(	kIx(
<g/>
a	a	k8xC
dosud	dosud	k6eAd1
nazývají	nazývat	k5eAaImIp3nP
<g/>
)	)	kIx)
pivnice	pivnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Terminologicky	terminologicky	k6eAd1
tedy	tedy	k9
není	být	k5eNaImIp3nS
zcela	zcela	k6eAd1
přesné	přesný	k2eAgNnSc1d1
říkat	říkat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
pivnice	pivnice	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
původním	původní	k2eAgMnSc6d1
<g/>
,	,	kIx,
slovenském	slovenský	k2eAgInSc6d1
významu	význam	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
sklep	sklep	k1gInSc1
je	být	k5eAaImIp3nS
totéž	týž	k3xTgNnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
polštině	polština	k1gFnSc6
slovo	slovo	k1gNnSc1
sklep	sklep	k1gInSc1
znamená	znamenat	k5eAaImIp3nS
obchod	obchod	k1gInSc4
(	(	kIx(
<g/>
jakožto	jakožto	k8xS
provozovna	provozovna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
sklep	sklep	k1gInSc1
se	se	k3xPyFc4
řekne	říct	k5eAaPmIp3nS
piwnica	piwnica	k1gFnSc1
a	a	k8xC
sklepienie	sklepienie	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
klenbu	klenba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Správný	správný	k2eAgInSc1d1
termín	termín	k1gInSc1
pro	pro	k7c4
chodbový	chodbový	k2eAgInSc4d1
sklep	sklep	k1gInSc4
<g/>
,	,	kIx,
sklepní	sklepní	k2eAgFnSc4d1
chodbu	chodba	k1gFnSc4
je	být	k5eAaImIp3nS
loch	loch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.vinarskydum.cz/index.php?Itemid=41&	http://www.vinarskydum.cz/index.php?Itemid=41&	k?
Stavba	stavba	k1gFnSc1
vinného	vinný	k2eAgInSc2d1
sklepa	sklep	k1gInSc2
obrazem	obraz	k1gInSc7
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Vinný	vinný	k2eAgInSc1d1
sklep	sklep	k1gInSc1
</s>
<s>
Aktivní	aktivní	k2eAgInSc1d1
vinný	vinný	k2eAgInSc1d1
sklep	sklep	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
sklep	sklep	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
sklep	sklep	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Vinné	vinný	k2eAgInPc4d1
sklepy	sklep	k1gInPc4
a	a	k8xC
skladování	skladování	k1gNnSc4
vína	víno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
sklepa	sklep	k1gInSc2
a	a	k8xC
odvětrání	odvětrání	k1gNnSc1
<g/>
,	,	kIx,
vinobraní	vinobraní	k1gNnSc1
na	na	k7c6
lidova-architektura	lidova-architektura	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Několik	několik	k4yIc1
druhů	druh	k1gInPc2
vinných	vinný	k2eAgInPc2d1
sklepů	sklep	k1gInPc2
<g/>
,	,	kIx,
fotografie	fotografia	k1gFnPc1
ze	z	k7c2
stavby	stavba	k1gFnSc2
vinných	vinný	k2eAgInPc2d1
sklepů	sklep	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vinné	vinný	k2eAgInPc4d1
sklepy	sklep	k1gInPc4
</s>
<s>
Obrázky	obrázek	k1gInPc1
sklepů	sklep	k1gInPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4030194-1	4030194-1	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
234	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85012053	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85012053	#num#	k4
</s>
