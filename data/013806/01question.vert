<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
prostor	prostor	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
podlaha	podlaha	k1gFnSc1
se	se	k3xPyFc4
celá	celý	k2eAgFnSc1d1
nebo	nebo	k8xC
z	z	k7c2
větší	veliký	k2eAgFnSc2d2
části	část	k1gFnSc2
nachází	nacházet	k5eAaImIp3nS
pod	pod	k7c7
úrovní	úroveň	k1gFnSc7
terénu	terén	k1gInSc2
<g/>
?	?	kIx.
</s>