<s>
Kollárovci	Kollárovec	k1gMnPc1
</s>
<s>
KollárovciZákladní	KollárovciZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc2
Původ	původ	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
Kolačkov	Kolačkov	k1gInSc1
okres	okres	k1gInSc4
Stará	starat	k5eAaImIp3nS
Ľubovňa	Ľubovňa	k1gMnSc1
Žánry	žánr	k1gInPc4
</s>
<s>
Východoslovenský	východoslovenský	k2eAgInSc1d1
folklór	folklór	k1gInSc1
Aktivní	aktivní	k2eAgInSc1d1
roky	rok	k1gInPc4
</s>
<s>
1997	#num#	k4
—	—	k?
současnost	současnost	k1gFnSc1
Web	web	k1gInSc1
</s>
<s>
kollarovci	kollarovec	k1gMnPc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
Současní	současný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Kollár	Kollár	k1gMnSc1
Štefan	Štefan	k1gMnSc1
Kollár	Kollár	k1gMnSc1
Marek	Marek	k1gMnSc1
Kolár	Kolár	k1gMnSc1
Štefan	Štefan	k1gMnSc1
Repka	Repka	k1gMnSc1
Peter	Peter	k1gMnSc1
Rončík	Rončík	k1gMnSc1
Juliús	Juliúsa	k1gFnPc2
Michal	Michal	k1gMnSc1
Hudi	Hud	k1gFnSc2
Patrik	Patrik	k1gMnSc1
Červeňák	Červeňák	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kollárovci	Kollárovec	k1gMnPc7
jsou	být	k5eAaImIp3nP
hudební	hudební	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
hrající	hrající	k2eAgFnSc1d1
od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
první	první	k4xOgFnSc6
CD	CD	kA
vydali	vydat	k5eAaPmAgMnP
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrají	hrát	k5eAaImIp3nP
a	a	k8xC
skládají	skládat	k5eAaImIp3nP
hlavně	hlavně	k9
folklórní	folklórní	k2eAgFnPc1d1
písně	píseň	k1gFnPc1
<g/>
,	,	kIx,
blízké	blízký	k2eAgFnPc1d1
folklóru	folklór	k1gInSc6
východního	východní	k2eAgNnSc2d1
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zpěvu	zpěv	k1gInSc6
používají	používat	k5eAaImIp3nP
často	často	k6eAd1
goralské	goralský	k2eAgNnSc4d1
nářečí	nářečí	k1gNnSc4
(	(	kIx(
<g/>
nářečí	nářečí	k1gNnSc4
slovenštiny	slovenština	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
směsí	směs	k1gFnSc7
slovenštiny	slovenština	k1gFnSc2
<g/>
,	,	kIx,
polštiny	polština	k1gFnSc2
a	a	k8xC
ukrajinštiny	ukrajinština	k1gFnSc2
podobající	podobající	k2eAgFnSc2d1
se	se	k3xPyFc4
těšínskému	těšínské	k1gNnSc3
nářečí	nářečí	k1gNnSc1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
zpěvu	zpěv	k1gInSc6
se	se	k3xPyFc4
členové	člen	k1gMnPc1
skupiny	skupina	k1gFnSc2
střídají	střídat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
mimořádný	mimořádný	k2eAgInSc4d1
cit	cit	k1gInSc4
pro	pro	k7c4
melodii	melodie	k1gFnSc4
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
vydali	vydat	k5eAaPmAgMnP
10	#num#	k4
hudebních	hudební	k2eAgMnPc2d1
CD	CD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupinu	skupina	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nP
především	především	k9
3	#num#	k4
bratři	bratr	k1gMnPc1
z	z	k7c2
rodiny	rodina	k1gFnSc2
Kollárovců	Kollárovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několikrát	několikrát	k6eAd1
se	se	k3xPyFc4
proslavili	proslavit	k5eAaPmAgMnP
na	na	k7c6
hudebních	hudební	k2eAgInPc6d1
festivalech	festival	k1gInPc6
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
folklórní	folklórní	k2eAgInPc4d1
festival	festival	k1gInSc4
v	v	k7c6
Chambly	Chambly	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hrají	hrát	k5eAaImIp3nP
i	i	k9
na	na	k7c6
soukromých	soukromý	k2eAgInPc6d1
koncertech	koncert	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
mimo	mimo	k7c4
Slovensko	Slovensko	k1gNnSc4
koncertují	koncertovat	k5eAaImIp3nP
v	v	k7c6
Polsku	Polsko	k1gNnSc6
a	a	k8xC
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
pravidelně	pravidelně	k6eAd1
hrají	hrát	k5eAaImIp3nP
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
pořadu	pořad	k1gInSc6
Šlágr	šlágr	k1gInSc1
TV	TV	kA
<g/>
.	.	kIx.
</s>
<s>
Členové	člen	k1gMnPc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Kollár	Kollár	k1gMnSc1
—	—	k?
Houslista	houslista	k1gMnSc1
</s>
<s>
Štefan	Štefan	k1gMnSc1
Kollár	Kollár	k1gMnSc1
—	—	k?
saxofon	saxofon	k1gInSc1
<g/>
,	,	kIx,
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
akordeon	akordeon	k1gInSc1
</s>
<s>
Marek	Marek	k1gMnSc1
Kollár	Kollár	k1gMnSc1
—	—	k?
Druhý	druhý	k4xOgInSc1
houslista	houslista	k1gMnSc1
</s>
<s>
Štefan	Štefan	k1gMnSc1
Repka	Repka	k1gMnSc1
—	—	k?
Basa	basa	k1gFnSc1
</s>
<s>
Patrik	Patrik	k1gMnSc1
Červenak-Bicie	Červenak-Bicie	k1gFnSc2
nástroje	nástroj	k1gInPc4
</s>
<s>
Juliús	Juliús	k6eAd1
Michal	Michal	k1gMnSc1
Hudi	Hud	k1gFnSc2
cimbálista	cimbálista	k1gMnSc1
</s>
<s>
Peter	Peter	k1gMnSc1
Rončík	Rončík	k1gMnSc1
-	-	kIx~
Gitara	Gitara	k1gFnSc1
</s>
<s>
CD	CD	kA
</s>
<s>
Z	z	k7c2
Kolačkova	Kolačkův	k2eAgInSc2d1
parobci	parobek	k1gMnPc5
</s>
<s>
Keď	Keď	k?
chlapci	chlapec	k1gMnPc1
hrajú	hrajú	k?
</s>
<s>
Hej	hej	k6eAd1
tam	tam	k6eAd1
od	od	k7c2
Tater	Tatra	k1gFnPc2
</s>
<s>
Pozdrav	pozdrav	k1gInSc1
zo	zo	k?
Slovenska	Slovensko	k1gNnSc2
</s>
<s>
Dievčatá	Dievčatat	k5eAaPmIp3nS
</s>
<s>
Vianoce	Vianoko	k6eAd1
s	s	k7c7
Kollárovcami	Kollárovca	k1gFnPc7
</s>
<s>
Goraľu	Goraľu	k6eAd1
cy	cy	k?
či	či	k8xC
ne	ne	k9
žaľ	žaľ	k?
</s>
<s>
Môj	Môj	k?
život	život	k1gInSc1
je	být	k5eAaImIp3nS
muzika	muzika	k1gFnSc1
</s>
<s>
Neúprosný	úprosný	k2eNgInSc1d1
čas	čas	k1gInSc1
</s>
<s>
Vlasy	vlas	k1gInPc4
čierne	čiernout	k5eAaPmIp3nS
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
skupiny	skupina	k1gFnSc2
</s>
<s>
Kollárovci	Kollárovec	k1gMnPc1
po	po	k7c6
siedmykrát	siedmykrát	k6eAd1
a	a	k8xC
stále	stále	k6eAd1
originálni	originálnit	k5eAaPmRp2nS
</s>
<s>
Kollárovci	Kollárovec	k1gMnPc1
<g/>
:	:	kIx,
Muzikanti	muzikant	k1gMnPc1
z	z	k7c2
Kolačkova	Kolačkův	k2eAgNnSc2d1
dobýjajú	dobýjajú	k?
srdcia	srdcium	k1gNnSc2
doma	doma	k6eAd1
aj	aj	kA
za	za	k7c4
hranicami	hranica	k1gFnPc7
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
