<p>
<s>
Skalník	skalník	k1gInSc1	skalník
zpěvný	zpěvný	k2eAgInSc1d1	zpěvný
(	(	kIx(	(
<g/>
Monticola	Monticola	k1gFnSc1	Monticola
saxatilis	saxatilis	k1gFnSc1	saxatilis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tažný	tažný	k2eAgMnSc1d1	tažný
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
drozdovitých	drozdovitý	k2eAgMnPc2d1	drozdovitý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
přibližně	přibližně	k6eAd1	přibližně
19	[number]	k4	19
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
rezavou	rezavý	k2eAgFnSc4d1	rezavá
hruď	hruď	k1gFnSc4	hruď
a	a	k8xC	a
modře	modro	k6eAd1	modro
zbarvenou	zbarvený	k2eAgFnSc4d1	zbarvená
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
nenápadné	nápadný	k2eNgFnPc1d1	nenápadná
<g/>
,	,	kIx,	,
hnědě	hnědě	k6eAd1	hnědě
kropenaté	kropenatý	k2eAgInPc1d1	kropenatý
se	s	k7c7	s
světlejším	světlý	k2eAgNnSc7d2	světlejší
břichem	břicho	k1gNnSc7	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
kropenatí	kropenatý	k2eAgMnPc1d1	kropenatý
a	a	k8xC	a
zbarvením	zbarvení	k1gNnSc7	zbarvení
připomínají	připomínat	k5eAaImIp3nP	připomínat
samičky	samička	k1gFnPc4	samička
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
potravy	potrava	k1gFnSc2	potrava
se	se	k3xPyFc4	se
skalníci	skalník	k1gMnPc1	skalník
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
nápadnými	nápadný	k2eAgInPc7d1	nápadný
přískoky	přískok	k1gInPc7	přískok
s	s	k7c7	s
úklony	úklon	k1gInPc7	úklon
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yRgFnPc6	který
charakteristicky	charakteristicky	k6eAd1	charakteristicky
švihají	švihat	k5eAaImIp3nP	švihat
ocasy	ocas	k1gInPc1	ocas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
krajině	krajina	k1gFnSc6	krajina
do	do	k7c2	do
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
3	[number]	k4	3
000	[number]	k4	000
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
centrální	centrální	k2eAgFnSc4d1	centrální
Asii	Asie	k1gFnSc4	Asie
až	až	k9	až
po	po	k7c4	po
sever	sever	k1gInSc4	sever
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nepříliš	příliš	k6eNd1	příliš
často	často	k6eAd1	často
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
lesnaté	lesnatý	k2eAgFnSc6d1	lesnatá
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
pahorkatinách	pahorkatina	k1gFnPc6	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
vyhynutí	vyhynutí	k1gNnSc6	vyhynutí
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
svou	svůj	k3xOyFgFnSc4	svůj
zásluhu	zásluha	k1gFnSc4	zásluha
tehdy	tehdy	k6eAd1	tehdy
populární	populární	k2eAgNnSc1d1	populární
chytání	chytání	k1gNnSc1	chytání
těchto	tento	k3xDgMnPc2	tento
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
chov	chov	k1gInSc1	chov
na	na	k7c4	na
zpěv	zpěv	k1gInSc4	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgNnSc1d1	poslední
hnízdění	hnízdění	k1gNnSc1	hnízdění
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
během	během	k7c2	během
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
skalách	skála	k1gFnPc6	skála
nebo	nebo	k8xC	nebo
mezi	mezi	k7c7	mezi
velkými	velký	k2eAgInPc7d1	velký
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
ne	ne	k9	ne
výš	vysoce	k6eAd2	vysoce
než	než	k8xS	než
sedm	sedm	k4xCc4	sedm
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
si	se	k3xPyFc3	se
staví	stavit	k5eAaBmIp3nS	stavit
neupravené	upravený	k2eNgNnSc4d1	neupravené
a	a	k8xC	a
samička	samička	k1gFnSc1	samička
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
snáší	snášet	k5eAaImIp3nS	snášet
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
bývá	bývat	k5eAaImIp3nS	bývat
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
dní	den	k1gInPc2	den
a	a	k8xC	a
za	za	k7c4	za
zhruba	zhruba	k6eAd1	zhruba
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
mláďata	mládě	k1gNnPc4	mládě
opouštějí	opouštět	k5eAaImIp3nP	opouštět
hnízdo	hnízdo	k1gNnSc1	hnízdo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Především	především	k9	především
nejrůznější	různý	k2eAgInSc4d3	nejrůznější
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepohrdne	pohrdnout	k5eNaPmIp3nS	pohrdnout
i	i	k9	i
různými	různý	k2eAgFnPc7d1	různá
bobulemi	bobule	k1gFnPc7	bobule
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
plody	plod	k1gInPc7	plod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
skalník	skalník	k1gMnSc1	skalník
zpěvný	zpěvný	k2eAgMnSc1d1	zpěvný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
skalník	skalník	k1gInSc4	skalník
zpěvný	zpěvný	k2eAgInSc4d1	zpěvný
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
