<s>
Bach	Bach	k1gMnSc1	Bach
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
světských	světský	k2eAgFnPc6d1	světská
i	i	k8xC	i
církevních	církevní	k2eAgFnPc6d1	církevní
službách	služba	k1gFnPc6	služba
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
jeho	on	k3xPp3gNnSc2	on
působiště	působiště	k1gNnSc2	působiště
byla	být	k5eAaImAgFnS	být
Výmar	Výmar	k1gInSc4	Výmar
<g/>
,	,	kIx,	,
Köthen	Köthen	k1gInSc4	Köthen
a	a	k8xC	a
Lipsko	Lipsko	k1gNnSc4	Lipsko
<g/>
.	.	kIx.	.
</s>
