<p>
<s>
Claude	Claud	k1gMnSc5	Claud
Arabo	Araba	k1gMnSc5	Araba
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1937	[number]	k4	1937
Nice	Nice	k1gFnSc2	Nice
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
Villefranche-sur-Mer	Villefrancheur-Mra	k1gFnPc2	Villefranche-sur-Mra
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
sportovní	sportovní	k2eAgMnSc1d1	sportovní
šermíř	šermíř	k1gMnSc1	šermíř
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
specializoval	specializovat	k5eAaBmAgMnS	specializovat
na	na	k7c4	na
šerm	šerm	k1gInSc4	šerm
šavlí	šavle	k1gFnPc2	šavle
<g/>
.	.	kIx.	.
</s>
<s>
Francii	Francie	k1gFnSc4	Francie
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
startoval	startovat	k5eAaBmAgInS	startovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
a	a	k8xC	a
1968	[number]	k4	1968
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
a	a	k8xC	a
družstev	družstvo	k1gNnPc2	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1964	[number]	k4	1964
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
obsadil	obsadit	k5eAaPmAgMnS	obsadit
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
francouzským	francouzský	k2eAgNnSc7d1	francouzské
družstvem	družstvo	k1gNnSc7	družstvo
kordistů	kordista	k1gMnPc2	kordista
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
a	a	k8xC	a
1967	[number]	k4	1967
třetí	třetí	k4xOgInSc4	třetí
místo	místo	k6eAd1	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Claude	Claud	k1gInSc5	Claud
Arabo	Araba	k1gFnSc5	Araba
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc3d1	francouzská
Wikipedii	Wikipedie	k1gFnSc3	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
