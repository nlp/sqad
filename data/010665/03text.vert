<p>
<s>
Zpěvy	zpěv	k1gInPc1	zpěv
páteční	páteční	k2eAgInPc1d1	páteční
jsou	být	k5eAaImIp3nP	být
poslední	poslední	k2eAgInPc1d1	poslední
básnickou	básnický	k2eAgFnSc7d1	básnická
sbírkou	sbírka	k1gFnSc7	sbírka
Jana	Jan	k1gMnSc2	Jan
Nerudy	Neruda	k1gMnSc2	Neruda
<g/>
.	.	kIx.	.
</s>
<s>
Vznikaly	vznikat	k5eAaImAgFnP	vznikat
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
desetiletí	desetiletí	k1gNnSc6	desetiletí
autorova	autorův	k2eAgInSc2d1	autorův
života	život	k1gInSc2	život
a	a	k8xC	a
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
nedokončeny	dokončit	k5eNaPmNgFnP	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
vyšly	vyjít	k5eAaPmAgFnP	vyjít
až	až	k9	až
po	po	k7c6	po
Nerudově	Nerudův	k2eAgFnSc6d1	Nerudova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
připravil	připravit	k5eAaPmAgMnS	připravit
básník	básník	k1gMnSc1	básník
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
10	[number]	k4	10
lyrických	lyrický	k2eAgFnPc2d1	lyrická
básní	báseň	k1gFnPc2	báseň
s	s	k7c7	s
národnostní	národnostní	k2eAgFnSc7d1	národnostní
a	a	k8xC	a
vlasteneckou	vlastenecký	k2eAgFnSc7d1	vlastenecká
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
patosem	patos	k1gInSc7	patos
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
autor	autor	k1gMnSc1	autor
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
obraz	obraz	k1gInSc4	obraz
trpícího	trpící	k2eAgInSc2d1	trpící
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
zápasu	zápas	k1gInSc2	zápas
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
existenci	existence	k1gFnSc4	existence
i	i	k9	i
naděje	naděje	k1gFnPc4	naděje
na	na	k7c4	na
lepší	dobrý	k2eAgFnSc4d2	lepší
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
bojovat	bojovat	k5eAaImF	bojovat
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
odkazu	odkaz	k1gInSc2	odkaz
slavných	slavný	k2eAgMnPc2d1	slavný
předků	předek	k1gMnPc2	předek
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
básně	báseň	k1gFnPc1	báseň
jsou	být	k5eAaImIp3nP	být
psány	psán	k2eAgInPc1d1	psán
jako	jako	k8xC	jako
žalozpěvy	žalozpěv	k1gInPc1	žalozpěv
nebo	nebo	k8xC	nebo
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
básně	báseň	k1gFnPc1	báseň
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Moje	můj	k3xOp1gFnSc1	můj
barva	barva	k1gFnSc1	barva
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
===	===	k?	===
</s>
</p>
<p>
<s>
Oslavná	oslavný	k2eAgFnSc1d1	oslavná
hymna	hymna	k1gFnSc1	hymna
na	na	k7c4	na
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
český	český	k2eAgInSc4d1	český
prapor	prapor	k1gInSc4	prapor
tvořený	tvořený	k2eAgInSc4d1	tvořený
červeným	červený	k2eAgInSc7d1	červený
a	a	k8xC	a
bílým	bílý	k2eAgInSc7d1	bílý
pruhem	pruh	k1gInSc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
krásu	krása	k1gFnSc4	krása
vlajícího	vlající	k2eAgInSc2d1	vlající
praporu	prapor	k1gInSc2	prapor
<g/>
,	,	kIx,	,
spojení	spojení	k1gNnSc1	spojení
barev	barva	k1gFnPc2	barva
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
ke	k	k7c3	k
krvi	krev	k1gFnSc3	krev
a	a	k8xC	a
vroucí	vroucí	k2eAgFnSc3d1	vroucí
bílé	bílý	k2eAgFnSc3d1	bílá
pěně	pěna	k1gFnSc3	pěna
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ke	k	k7c3	k
sněhové	sněhový	k2eAgFnSc3d1	sněhová
návěji	návěj	k1gFnSc3	návěj
pokropené	pokropený	k2eAgNnSc1d1	pokropené
krví	krev	k1gFnSc7	krev
<g/>
,	,	kIx,	,
k	k	k7c3	k
barvě	barva	k1gFnSc3	barva
života	život	k1gInSc2	život
a	a	k8xC	a
barvě	barva	k1gFnSc3	barva
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
připodobňuje	připodobňovat	k5eAaImIp3nS	připodobňovat
prapor	prapor	k1gInSc4	prapor
k	k	k7c3	k
národu	národ	k1gInSc3	národ
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc3	jeho
proměnlivému	proměnlivý	k2eAgInSc3d1	proměnlivý
osudu	osud	k1gInSc3	osud
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
barvy	barva	k1gFnPc4	barva
praporu	prapor	k1gInSc2	prapor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vedly	vést	k5eAaImAgFnP	vést
jako	jako	k9	jako
světla	světlo	k1gNnSc2	světlo
národ	národ	k1gInSc1	národ
bojem	boj	k1gInSc7	boj
i	i	k8xC	i
mírem	mír	k1gInSc7	mír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ecce	Eccus	k1gMnSc5	Eccus
homo	homa	k1gMnSc5	homa
===	===	k?	===
</s>
</p>
<p>
<s>
Báseň	báseň	k1gFnSc1	báseň
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
protiklady	protiklad	k1gInPc4	protiklad
postihující	postihující	k2eAgInSc4d1	postihující
český	český	k2eAgInSc4d1	český
národ	národ	k1gInSc4	národ
a	a	k8xC	a
vyzývající	vyzývající	k2eAgInPc1d1	vyzývající
k	k	k7c3	k
jednotě	jednota	k1gFnSc3	jednota
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Neruda	Neruda	k1gMnSc1	Neruda
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
nad	nad	k7c7	nad
dvěma	dva	k4xCgInPc7	dva
obrazy	obraz	k1gInPc7	obraz
za	za	k7c7	za
výkladní	výkladní	k2eAgFnSc7d1	výkladní
skříní	skříň	k1gFnSc7	skříň
–	–	k?	–
"	"	kIx"	"
<g/>
Ecce	Ecce	k1gFnSc1	Ecce
homo	homo	k1gMnSc1	homo
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Když	když	k8xS	když
na	na	k7c4	na
lid	lid	k1gInSc4	lid
český	český	k2eAgInSc4d1	český
padla	padnout	k5eAaImAgFnS	padnout
persekuce	persekuce	k1gFnSc1	persekuce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
Boha	bůh	k1gMnSc4	bůh
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vraždí	vraždit	k5eAaImIp3nP	vraždit
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
jménu	jméno	k1gNnSc6	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
nesrovnalostí	nesrovnalost	k1gFnSc7	nesrovnalost
a	a	k8xC	a
klade	klást	k5eAaImIp3nS	klást
si	se	k3xPyFc3	se
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
bude	být	k5eAaImBp3nS	být
vždy	vždy	k6eAd1	vždy
<g/>
.	.	kIx.	.
</s>
<s>
Hledá	hledat	k5eAaImIp3nS	hledat
odpověď	odpověď	k1gFnSc4	odpověď
vzpomínkou	vzpomínka	k1gFnSc7	vzpomínka
na	na	k7c4	na
mládí	mládí	k1gNnSc4	mládí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
národ	národ	k1gInSc1	národ
to	ten	k3xDgNnSc1	ten
se	s	k7c7	s
svobodou	svoboda	k1gFnSc7	svoboda
a	a	k8xC	a
bratrstvím	bratrství	k1gNnSc7	bratrství
myslí	myslet	k5eAaImIp3nS	myslet
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
teď	teď	k6eAd1	teď
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
"	"	kIx"	"
<g/>
bludná	bludný	k2eAgFnSc1d1	bludná
náhoda	náhoda	k1gFnSc1	náhoda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Končí	končit	k5eAaImIp3nS	končit
konstatování	konstatování	k1gNnSc1	konstatování
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdo	nikdo	k3yNnSc1	nikdo
nepronesl	pronést	k5eNaPmAgMnS	pronést
trpčí	trpký	k2eAgNnSc1d2	trpčí
slovo	slovo	k1gNnSc1	slovo
než	než	k8xS	než
"	"	kIx"	"
<g/>
Ecce	Ecce	k1gFnSc1	Ecce
homo	homo	k1gMnSc1	homo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Láska	láska	k1gFnSc1	láska
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
básni	báseň	k1gFnSc6	báseň
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
vřelém	vřelý	k2eAgInSc6d1	vřelý
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
českému	český	k2eAgInSc3d1	český
národu	národ	k1gInSc3	národ
<g/>
.	.	kIx.	.
</s>
<s>
Nevadí	vadit	k5eNaImIp3nS	vadit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
malému	malý	k2eAgMnSc3d1	malý
a	a	k8xC	a
chudému	chudý	k2eAgInSc3d1	chudý
národu	národ	k1gInSc3	národ
se	se	k3xPyFc4	se
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
každý	každý	k3xTgMnSc1	každý
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
<g/>
,	,	kIx,	,
on	on	k3xPp3gInSc1	on
jej	on	k3xPp3gMnSc4	on
přesto	přesto	k8xC	přesto
miluje	milovat	k5eAaImIp3nS	milovat
jako	jako	k9	jako
dívčici	dívčice	k1gFnSc4	dívčice
a	a	k8xC	a
nehledí	hledět	k5eNaImIp3nS	hledět
přitom	přitom	k6eAd1	přitom
na	na	k7c4	na
drobné	drobný	k2eAgInPc4d1	drobný
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Nenechá	nechat	k5eNaPmIp3nS	nechat
se	se	k3xPyFc4	se
strhnout	strhnout	k5eAaPmF	strhnout
názory	názor	k1gInPc4	názor
některých	některý	k3yIgMnPc2	některý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
věří	věřit	k5eAaImIp3nS	věřit
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
naučila	naučit	k5eAaPmAgFnS	naučit
lásce	láska	k1gFnSc3	láska
k	k	k7c3	k
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
národu	národ	k1gInSc3	národ
<g/>
.	.	kIx.	.
</s>
<s>
Nikoho	nikdo	k3yNnSc4	nikdo
jiného	jiný	k2eAgNnSc2d1	jiné
nechce	chtít	k5eNaImIp3nS	chtít
milovat	milovat	k5eAaImF	milovat
v	v	k7c6	v
širém	širý	k2eAgInSc6d1	širý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
zatímco	zatímco	k8xS	zatímco
matku	matka	k1gFnSc4	matka
přežil	přežít	k5eAaPmAgMnS	přežít
a	a	k8xC	a
drží	držet	k5eAaImIp3nS	držet
si	se	k3xPyFc3	se
její	její	k3xOp3gFnSc4	její
památku	památka	k1gFnSc4	památka
národ	národ	k1gInSc4	národ
by	by	kYmCp3nS	by
nepřežil	přežít	k5eNaPmAgMnS	přežít
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
jej	on	k3xPp3gMnSc4	on
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jen	jen	k9	jen
dál	daleko	k6eAd2	daleko
<g/>
!	!	kIx.	!
</s>
<s>
===	===	k?	===
</s>
</p>
<p>
<s>
Hymna	hymna	k1gFnSc1	hymna
vyzývající	vyzývající	k2eAgFnSc2d1	vyzývající
k	k	k7c3	k
velkým	velký	k2eAgInPc3d1	velký
činům	čin	k1gInPc3	čin
celý	celý	k2eAgInSc4d1	celý
národ	národ	k1gInSc4	národ
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
mu	on	k3xPp3gNnSc3	on
budou	být	k5eAaImBp3nP	být
padat	padat	k5eAaImF	padat
do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
překážky	překážka	k1gFnSc2	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
k	k	k7c3	k
novým	nový	k2eAgInPc3d1	nový
činům	čin	k1gInPc3	čin
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
čase	čas	k1gInSc6	čas
a	a	k8xC	a
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
dávnou	dávný	k2eAgFnSc4d1	dávná
slávu	sláva	k1gFnSc4	sláva
k	k	k7c3	k
brázdě	brázda	k1gFnSc3	brázda
za	za	k7c7	za
korábem	koráb	k1gInSc7	koráb
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
k	k	k7c3	k
napnutí	napnutí	k1gNnSc3	napnutí
plachet	plachta	k1gFnPc2	plachta
a	a	k8xC	a
pokračování	pokračování	k1gNnSc4	pokračování
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
umlčet	umlčet	k5eAaPmF	umlčet
hlasy	hlas	k1gInPc4	hlas
hovořící	hovořící	k2eAgInPc4d1	hovořící
o	o	k7c6	o
nepřízni	nepřízeň	k1gFnSc6	nepřízeň
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
odstranit	odstranit	k5eAaPmF	odstranit
klímání	klímání	k1gNnSc4	klímání
u	u	k7c2	u
kormidla	kormidlo	k1gNnSc2	kormidlo
a	a	k8xC	a
nutí	nutit	k5eAaImIp3nS	nutit
k	k	k7c3	k
okamžitému	okamžitý	k2eAgInSc3d1	okamžitý
činu	čin	k1gInSc3	čin
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
kdo	kdo	k3yRnSc1	kdo
chvíli	chvíle	k1gFnSc6	chvíle
stál	stát	k5eAaImAgInS	stát
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
stojí	stát	k5eAaImIp3nS	stát
opodál	opodál	k6eAd1	opodál
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Burcuje	burcovat	k5eAaImIp3nS	burcovat
k	k	k7c3	k
činům	čin	k1gInPc3	čin
celých	celá	k1gFnPc2	celá
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
nevíme	vědět	k5eNaImIp1nP	vědět
co	co	k8xS	co
přinese	přinést	k5eAaPmIp3nS	přinést
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
využít	využít	k5eAaPmF	využít
všeho	všecek	k3xTgNnSc2	všecek
co	co	k9	co
máme	mít	k5eAaImIp1nP	mít
k	k	k7c3	k
dalším	další	k2eAgNnPc3d1	další
vítězstvím	vítězství	k1gNnPc3	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
jsme	být	k5eAaImIp1nP	být
součástí	součást	k1gFnSc7	součást
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
společně	společně	k6eAd1	společně
držíme	držet	k5eAaImIp1nP	držet
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
,	,	kIx,	,
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
nemáme	mít	k5eNaImIp1nP	mít
znát	znát	k5eAaImF	znát
odpočinku	odpočinek	k1gInSc2	odpočinek
a	a	k8xC	a
máme	mít	k5eAaImIp1nP	mít
se	se	k3xPyFc4	se
mít	mít	k5eAaImF	mít
stále	stále	k6eAd1	stále
k	k	k7c3	k
činu	čin	k1gInSc3	čin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
online	onlinout	k5eAaPmIp3nS	onlinout
==	==	k?	==
</s>
</p>
<p>
<s>
NERUDA	Neruda	k1gMnSc1	Neruda
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvy	zpěv	k1gInPc1	zpěv
páteční	páteční	k2eAgInPc1d1	páteční
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Viktor	Viktor	k1gMnSc1	Viktor
Oliva	Oliva	k1gMnSc1	Oliva
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
F.	F.	kA	F.
Topič	topič	k1gMnSc1	topič
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
25	[number]	k4	25
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc4	dílo
Zpěvy	zpěv	k1gInPc1	zpěv
páteční	páteční	k2eAgInPc1d1	páteční
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
