<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
je	být	k5eAaImIp3nS	být
objevení	objevení	k1gNnSc1	objevení
čaje	čaj	k1gInSc2	čaj
připisováno	připisovat	k5eAaImNgNnS	připisovat
čínskému	čínský	k2eAgMnSc3d1	čínský
císaři	císař	k1gMnSc3	císař
Šen-nungovi	Šenung	k1gMnSc3	Šen-nung
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
při	při	k7c6	při
ohřívání	ohřívání	k1gNnSc6	ohřívání
vody	voda	k1gFnSc2	voda
spadly	spadnout	k5eAaPmAgFnP	spadnout
do	do	k7c2	do
kotlíku	kotlík	k1gInSc2	kotlík
náhodně	náhodně	k6eAd1	náhodně
i	i	k9	i
lístky	lístek	k1gInPc1	lístek
čajovníku	čajovník	k1gInSc2	čajovník
<g/>
.	.	kIx.	.
</s>
