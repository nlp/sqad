<s>
Alfons	Alfons	k1gMnSc1	Alfons
Maria	Mario	k1gMnSc2	Mario
Mucha	Mucha	k1gMnSc1	Mucha
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1860	[number]	k4	1860
Ivančice	Ivančice	k1gFnPc1	Ivančice
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1939	[number]	k4	1939
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
designér	designér	k1gMnSc1	designér
období	období	k1gNnSc2	období
secese	secese	k1gFnSc2	secese
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
a	a	k8xC	a
francouzštině	francouzština	k1gFnSc6	francouzština
známo	znám	k2eAgNnSc1d1	známo
pod	pod	k7c7	pod
francouzským	francouzský	k2eAgNnSc7d1	francouzské
označením	označení	k1gNnSc7	označení
Art	Art	k1gFnSc2	Art
nouveau	nouveaus	k1gInSc2	nouveaus
(	(	kIx(	(
<g/>
nové	nový	k2eAgNnSc1d1	nové
umění	umění	k1gNnSc1	umění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
