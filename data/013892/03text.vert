<s>
René	René	k1gMnSc1
Fasel	Fasel	k1gMnSc1
</s>
<s>
René	René	k1gMnSc1
Fasel	Fasel	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1950	#num#	k4
(	(	kIx(
<g/>
71	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Fribourg	Fribourg	k1gInSc1
<g/>
,	,	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
švýcarská	švýcarský	k2eAgFnSc1d1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Bernská	bernský	k2eAgFnSc1d1
univerzitaFreiburská	univerzitaFreiburský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
funkcionář	funkcionář	k1gMnSc1
IIHF	IIHF	kA
Titul	titul	k1gInSc1
</s>
<s>
prezident	prezident	k1gMnSc1
IIHF	IIHF	kA
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc1
přátelství	přátelství	k1gNnSc2
mezi	mezi	k7c7
národy	národ	k1gInPc7
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
rytíř	rytíř	k1gMnSc1
Čestné	čestný	k2eAgFnSc2d1
legieŘád	legieŘáda	k1gFnPc2
přátelstvíKříž	přátelstvíKříž	k1gFnSc4
uznání	uznání	k1gNnSc2
Období	období	k1gNnSc2
</s>
<s>
od	od	k7c2
1994	#num#	k4
Předchůdce	předchůdce	k1gMnPc4
</s>
<s>
Günther	Günthra	k1gFnPc2
Sabetzki	Sabetzk	k1gFnSc2
Představenstva	představenstvo	k1gNnSc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
Funkce	funkce	k1gFnSc1
</s>
<s>
prezident	prezident	k1gMnSc1
(	(	kIx(
<g/>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
<g/>
;	;	kIx,
od	od	k7c2
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
René	René	k1gMnSc1
Fasel	Fasel	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1950	#num#	k4
Fribourg	Fribourg	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
prezident	prezident	k1gMnSc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
hokejové	hokejový	k2eAgFnSc2d1
federace	federace	k1gFnSc2
(	(	kIx(
<g/>
IIHF	IIHF	kA
<g/>
)	)	kIx)
a	a	k8xC
bývalý	bývalý	k2eAgMnSc1d1
švýcarský	švýcarský	k2eAgMnSc1d1
hokejista	hokejista	k1gMnSc1
a	a	k8xC
rozhodčí	rozhodčí	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystudoval	vystudovat	k5eAaPmAgMnS
stomatologii	stomatologie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Lednímu	lední	k2eAgInSc3d1
hokeji	hokej	k1gInSc3
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
věnovat	věnovat	k5eAaImF,k5eAaPmF
v	v	k7c6
žákovském	žákovský	k2eAgInSc6d1
věku	věk	k1gInSc6
<g/>
;	;	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
hrál	hrát	k5eAaImAgInS
za	za	k7c4
HC	HC	kA
Fribourg-Gottéron	Fribourg-Gottéron	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
však	však	k9
dal	dát	k5eAaPmAgMnS
přednost	přednost	k1gFnSc4
nejprve	nejprve	k6eAd1
kariéře	kariéra	k1gFnSc6
rozhodčího	rozhodčí	k1gMnSc2
a	a	k8xC
následně	následně	k6eAd1
hokejového	hokejový	k2eAgMnSc2d1
funkcionáře	funkcionář	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
René	René	k1gMnSc1
Fasel	Fasel	k1gMnSc1
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
švýcarského	švýcarský	k2eAgNnSc2d1
města	město	k1gNnSc2
Fribourg	Fribourg	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
také	také	k9
roku	rok	k1gInSc2
1960	#num#	k4
začal	začít	k5eAaPmAgMnS
hrát	hrát	k5eAaImF
lední	lední	k2eAgInSc4d1
hokej	hokej	k1gInSc4
v	v	k7c6
klubu	klub	k1gInSc6
HC	HC	kA
Fribourg-Gottéron	Fribourg-Gottéron	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Klub	klub	k1gInSc1
opustil	opustit	k5eAaPmAgInS
roku	rok	k1gInSc2
1972	#num#	k4
a	a	k8xC
dal	dát	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
dráhu	dráha	k1gFnSc4
rozhodčího	rozhodčí	k1gMnSc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgMnSc7
zůstal	zůstat	k5eAaPmAgMnS
do	do	k7c2
roku	rok	k1gInSc2
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
řídil	řídit	k5eAaImAgInS
mj.	mj.	kA
37	#num#	k4
mezinárodních	mezinárodní	k2eAgNnPc2d1
utkání	utkání	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1982	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
předsedou	předseda	k1gMnSc7
komise	komise	k1gFnSc2
rozhodčích	rozhodčí	k1gMnPc2
Švýcarské	švýcarský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1985	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prezidentem	prezident	k1gMnSc7
Švýcarské	švýcarský	k2eAgFnSc2d1
federace	federace	k1gFnSc2
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
a	a	k8xC
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
také	také	k9
členem	člen	k1gMnSc7
rady	rada	k1gFnSc2
IIHF	IIHF	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
působil	působit	k5eAaImAgMnS
v	v	k7c6
komisi	komise	k1gFnSc6
rozhodčích	rozhodčí	k1gMnPc2
a	a	k8xC
v	v	k7c6
marketingové	marketingový	k2eAgFnSc6d1
komisi	komise	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
roku	rok	k1gInSc2
1994	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
prezidentem	prezident	k1gMnSc7
IIHF	IIHF	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
vystřídal	vystřídat	k5eAaPmAgInS
Němce	Němec	k1gMnSc4
Günthera	Günther	k1gMnSc4
Sabetzkiho	Sabetzki	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
funkci	funkce	k1gFnSc6
začal	začít	k5eAaPmAgMnS
usilovat	usilovat	k5eAaImF
o	o	k7c4
vybudování	vybudování	k1gNnSc4
pevnějších	pevný	k2eAgInPc2d2
vztahů	vztah	k1gInPc2
mezi	mezi	k7c7
IIHF	IIHF	kA
a	a	k8xC
kanadsko-americkou	kanadsko-americký	k2eAgFnSc7d1
Národní	národní	k2eAgFnSc7d1
hokejovou	hokejový	k2eAgFnSc7d1
ligou	liga	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
1995	#num#	k4
pomohl	pomoct	k5eAaPmAgMnS
vyjednat	vyjednat	k5eAaPmF
dohodu	dohoda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
umožnila	umožnit	k5eAaPmAgFnS
účast	účast	k1gFnSc4
hráčů	hráč	k1gMnPc2
z	z	k7c2
NHL	NHL	kA
na	na	k7c6
Zimních	zimní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
Naganu	Nagano	k1gNnSc6
roku	rok	k1gInSc2
1998	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Velmi	velmi	k6eAd1
ostře	ostro	k6eAd1
vystupuje	vystupovat	k5eAaImIp3nS
proti	proti	k7c3
hokejovým	hokejový	k2eAgFnPc3d1
rvačkám	rvačka	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1992	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
členem	člen	k1gMnSc7
Švýcarské	švýcarský	k2eAgFnSc2d1
olympijské	olympijský	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
a	a	k8xC
roku	rok	k1gInSc2
1995	#num#	k4
členem	člen	k1gInSc7
Mezinárodního	mezinárodní	k2eAgInSc2d1
olympijského	olympijský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
první	první	k4xOgMnSc1
zástupce	zástupce	k1gMnSc1
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
v	v	k7c6
historii	historie	k1gFnSc6
této	tento	k3xDgFnSc2
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
svého	svůj	k3xOyFgNnSc2
členství	členství	k1gNnSc2
v	v	k7c6
MOV	MOV	kA
byl	být	k5eAaImAgInS
jmenován	jmenován	k2eAgInSc1d1
do	do	k7c2
různých	různý	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
předsednictví	předsednictví	k1gNnSc2
Asociace	asociace	k1gFnSc2
mezinárodních	mezinárodní	k2eAgFnPc2d1
federací	federace	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
zimních	zimní	k2eAgMnPc2d1
olympijských	olympijský	k2eAgMnPc2d1
sportů	sport	k1gInPc2
a	a	k8xC
předsednictví	předsednictví	k1gNnSc2
koordinační	koordinační	k2eAgFnSc2d1
komise	komise	k1gFnSc2
MOV	MOV	kA
pro	pro	k7c4
Zimní	zimní	k2eAgFnPc4d1
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
7	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2008	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c4
120	#num#	k4
<g/>
.	.	kIx.
zasedání	zasedání	k1gNnSc6
Mezinárodního	mezinárodní	k2eAgInSc2d1
olympijského	olympijský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
jmenován	jmenován	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
zástupce	zástupce	k1gMnSc1
zimních	zimní	k2eAgInPc2d1
sportů	sport	k1gInPc2
na	na	k7c4
čtyřleté	čtyřletý	k2eAgNnSc4d1
funkční	funkční	k2eAgNnSc4d1
období	období	k1gNnSc4
do	do	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
představenstva	představenstvo	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
kde	kde	k6eAd1
nahradil	nahradit	k5eAaPmAgInS
Itala	Ital	k1gMnSc4
Ottavia	Ottavius	k1gMnSc4
Cinquantu	Cinquant	k1gInSc2
<g/>
,	,	kIx,
prezidenta	prezident	k1gMnSc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
bruslařské	bruslařský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
René	René	k1gMnSc1
Fasel	Fasel	k1gMnSc1
je	být	k5eAaImIp3nS
ženatý	ženatý	k2eAgMnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
čtyři	čtyři	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
univerzitě	univerzita	k1gFnSc6
ve	v	k7c6
Freiburgu	Freiburg	k1gInSc6
a	a	k8xC
v	v	k7c6
Bernu	Bern	k1gInSc6
vystudoval	vystudovat	k5eAaPmAgMnS
stomatologii	stomatologie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studia	studio	k1gNnSc2
úspěšně	úspěšně	k6eAd1
ukončil	ukončit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1977	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1997	#num#	k4
jej	on	k3xPp3gMnSc4
Mezinárodní	mezinárodní	k2eAgInSc1d1
olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
pověřil	pověřit	k5eAaPmAgInS
vypracováním	vypracování	k1gNnSc7
studie	studie	k1gFnSc2
zabývající	zabývající	k2eAgFnSc4d1
se	se	k3xPyFc4
zubní	zubní	k2eAgFnSc7d1
léčbou	léčba	k1gFnSc7
olympijských	olympijský	k2eAgMnPc2d1
sportovců	sportovec	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
publikována	publikován	k2eAgFnSc1d1
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Uznání	uznání	k1gNnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
René	René	k1gMnSc1
Fasel	Fasel	k1gMnSc1
obdržel	obdržet	k5eAaPmAgMnS
Řád	řád	k1gInSc4
čestné	čestný	k2eAgFnSc2d1
legie	legie	k1gFnSc2
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgNnSc1d3
francouzské	francouzský	k2eAgNnSc1d1
vyznamenání	vyznamenání	k1gNnSc1
udělované	udělovaný	k2eAgNnSc1d1
za	za	k7c4
výjimečnou	výjimečný	k2eAgFnSc4d1
službu	služba	k1gFnSc4
Francii	Francie	k1gFnSc4
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
národnost	národnost	k1gFnSc4
oceněného	oceněný	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
mu	on	k3xPp3gMnSc3
byla	být	k5eAaImAgFnS
udělena	udělit	k5eAaPmNgFnS
také	také	k9
ukrajinská	ukrajinský	k2eAgFnSc1d1
cena	cena	k1gFnSc1
VIZHIBU	VIZHIBU	kA
za	za	k7c4
významný	významný	k2eAgInSc4d1
přínos	přínos	k1gInSc4
rozvoji	rozvoj	k1gInSc3
a	a	k8xC
popularizaci	popularizace	k1gFnSc3
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
Síně	síň	k1gFnSc2
slávy	sláva	k1gFnSc2
slovinského	slovinský	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2008	#num#	k4
mu	on	k3xPp3gMnSc3
Švýcarská	švýcarský	k2eAgFnSc1d1
federace	federace	k1gFnSc1
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
právě	právě	k6eAd1
oslavovala	oslavovat	k5eAaImAgFnS
sto	sto	k4xCgNnSc4
let	let	k1gInSc4
svého	svůj	k3xOyFgNnSc2
trvání	trvání	k1gNnSc2
<g/>
,	,	kIx,
udělila	udělit	k5eAaPmAgFnS
Zvláštní	zvláštní	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
výjimečný	výjimečný	k2eAgInSc4d1
přínos	přínos	k1gInSc4
švýcarskému	švýcarský	k2eAgInSc3d1
hokeji	hokej	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Louis	Louis	k1gMnSc1
Magnus	Magnus	k1gMnSc1
</s>
<s>
John	John	k1gMnSc1
Francis	Francis	k1gFnSc2
Ahearne	Ahearn	k1gInSc5
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
René	René	k1gMnSc1
Fasel	Fasel	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Mr	Mr	k1gMnSc1
René	René	k1gMnSc1
Fasel	Fasel	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gMnSc1
Olympic	Olympic	k1gMnSc1
Committee	Committee	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
IIHF	IIHF	kA
Council	Councila	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gFnSc1
Ice	Ice	k1gFnSc2
Hockey	Hockea	k1gFnSc2
Federation	Federation	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LAPOINTE	LAPOINTE	kA
<g/>
,	,	kIx,
Joe	Joe	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
HOCKEY	HOCKEY	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
N.	N.	kA
<g/>
H.L.	H.L.	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Olympic	Olympic	k1gMnSc1
Gamble	Gamble	k1gMnSc1
<g/>
;	;	kIx,
Stars	Stars	k1gInSc1
<g/>
'	'	kIx"
Participation	Participation	k1gInSc1
in	in	k?
Nagano	Nagano	k1gNnSc1
Could	Could	k1gInSc4
Raise	Raisa	k1gFnSc3
Sport	sport	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Profile	profil	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
C	C	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
362	#num#	k4
<g/>
-	-	kIx~
<g/>
4331	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bass	Bass	k1gMnSc1
<g/>
,	,	kIx,
Alan	Alan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beyond	Beyond	k1gInSc1
the	the	k?
Bleachers	Bleachers	k1gInSc1
with	with	k1gMnSc1
IIHF	IIHF	kA
President	president	k1gMnSc1
Rene	Ren	k1gFnSc2
Fasel	Fasel	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bleacher	Bleachra	k1gFnPc2
Report	report	k1gInSc1
<g/>
,	,	kIx,
2009-01-19	2009-01-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Associated	Associated	k1gInSc1
Press	Press	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moroccan	Moroccan	k1gMnSc1
woman	woman	k1gMnSc1
set	set	k1gInSc4
to	ten	k3xDgNnSc1
join	join	k1gNnSc1
IOC	IOC	kA
rule	rula	k1gFnSc6
making	making	k1gInSc4
body	bod	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olympic	Olympice	k1gInPc2
Sports	Sportsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Fasel	Fasel	k1gInSc1
set	sto	k4xCgNnPc2
to	ten	k3xDgNnSc4
join	join	k1gNnSc1
the	the	k?
IOC	IOC	kA
executive	executiv	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gFnSc1
Ice	Ice	k1gFnSc2
Hockey	Hockea	k1gFnSc2
Federation	Federation	k1gInSc1
<g/>
,	,	kIx,
2008-5-6	2008-5-6	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FASEL	FASEL	kA
<g/>
,	,	kIx,
René	René	k1gFnSc5
<g/>
,	,	kIx,
Piccininni	Piccinineň	k1gFnSc5
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
M.	M.	kA
Sports	Sports	k1gInSc4
Dentistry	Dentistra	k1gFnSc2
and	and	k?
the	the	k?
Olympic	Olympic	k1gMnSc1
Games	Games	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
California	Californium	k1gNnSc2
Dental	Dental	k1gMnSc1
Association	Association	k1gInSc1
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
33	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
471	#num#	k4
<g/>
–	–	k?
<g/>
483	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1043	#num#	k4
<g/>
-	-	kIx~
<g/>
2256	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
16060340	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
25	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
René	René	k1gFnSc2
Fasel	Fasel	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Prezidenti	prezident	k1gMnPc1
IIHF	IIHF	kA
</s>
<s>
1908	#num#	k4
<g/>
–	–	k?
<g/>
1912	#num#	k4
<g/>
:	:	kIx,
Louis	louis	k1gInSc2
Magnus	Magnus	k1gInSc1
•	•	k?
1912	#num#	k4
<g/>
–	–	k?
<g/>
1914	#num#	k4
<g/>
:	:	kIx,
Henri	Henr	k1gFnSc6
van	vana	k1gFnPc2
den	den	k1gInSc1
Bulcke	Bulck	k1gInSc2
•	•	k?
1914	#num#	k4
<g/>
:	:	kIx,
Peter	Petra	k1gFnPc2
Patton	Patton	k1gInSc1
•	•	k?
1914	#num#	k4
<g/>
:	:	kIx,
Louis	louis	k1gInSc2
Magnus	Magnus	k1gInSc1
•	•	k?
1914	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
<g/>
:	:	kIx,
Henri	Henr	k1gFnSc6
van	vana	k1gFnPc2
den	den	k1gInSc1
Bulcke	Bulck	k1gInSc2
•	•	k?
1920	#num#	k4
<g/>
–	–	k?
<g/>
1922	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Max	max	kA
Sillig	Sillig	k1gInSc1
•	•	k?
1922	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
<g/>
:	:	kIx,
Paul	Paul	k1gMnSc1
Loicq	Loicq	k1gFnSc2
•	•	k?
1947	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
:	:	kIx,
Fritz	Fritz	k1gInSc1
Kraatz	Kraatz	k1gInSc1
•	•	k?
1948	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
<g/>
:	:	kIx,
George	Georg	k1gInSc2
Hardy	Harda	k1gFnSc2
•	•	k?
1951	#num#	k4
<g/>
–	–	k?
<g/>
1954	#num#	k4
<g/>
:	:	kIx,
Fritz	Fritz	k1gInSc1
Kraatz	Kraatz	k1gInSc1
•	•	k?
1954	#num#	k4
<g/>
–	–	k?
<g/>
1957	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Walter	Walter	k1gMnSc1
Brown	Brown	k1gMnSc1
•	•	k?
1957	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
<g/>
:	:	kIx,
John	John	k1gMnSc1
Francis	Francis	k1gFnSc2
Ahearne	Ahearn	k1gInSc5
•	•	k?
1960	#num#	k4
<g/>
–	–	k?
<g/>
1963	#num#	k4
<g/>
:	:	kIx,
Robert	Robert	k1gMnSc1
Lebel	Lebela	k1gFnPc2
•	•	k?
1963	#num#	k4
<g/>
–	–	k?
<g/>
1966	#num#	k4
<g/>
:	:	kIx,
John	John	k1gMnSc1
Francis	Francis	k1gFnSc2
Ahearne	Ahearn	k1gInSc5
•	•	k?
1966	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
:	:	kIx,
William	William	k1gInSc1
Thayer	Thayer	k1gInSc1
Tutt	Tutt	k1gInSc4
•	•	k?
1969	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g/>
:	:	kIx,
John	John	k1gMnSc1
Francis	Francis	k1gFnSc2
Ahearne	Ahearn	k1gInSc5
•	•	k?
1975	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
:	:	kIx,
Günther	Günthra	k1gFnPc2
Sabetzki	Sabetzki	k1gNnPc2
•	•	k?
1994	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
René	René	k1gMnSc1
Fasel	Fasel	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
