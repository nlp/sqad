<s>
Heathen	Heathen	k1gInSc1
Chemistry	Chemistr	k1gMnPc4
</s>
<s>
Heathen	Heathen	k2eAgInSc4d1
ChemistryInterpretOasisDruh	ChemistryInterpretOasisDruh	k1gInSc4
albastudiové	albastudiový	k2eAgNnSc4d1
albumVydáno	albumVydán	k2eAgNnSc4d1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2002	#num#	k4
<g/>
Nahránosrpen	Nahránosrpen	k2eAgInSc1d1
2001	#num#	k4
–	–	k?
únor	únor	k1gInSc1
2002	#num#	k4
ve	v	k7c4
Wheeler	Wheeler	k1gInSc4
End	End	k1gFnSc2
v	v	k7c6
Buckinghamshire	Buckinghamshir	k1gInSc5
a	a	k8xC
v	v	k7c4
Olympic	Olympic	k1gMnSc1
Studios	Studios	k?
v	v	k7c4
LondýněŽánryalternativní	LondýněŽánryalternativní	k2eAgInSc4d1
rock	rock	k1gInSc4
<g/>
,	,	kIx,
britpopDélka	britpopDélka	k1gFnSc1
<g/>
76	#num#	k4
<g/>
:	:	kIx,
<g/>
48	#num#	k4
<g/>
JazykangličtinaVydavatelstvíBig	JazykangličtinaVydavatelstvíBig	k1gInSc1
Brother	Brother	kA
<g/>
,	,	kIx,
EpicProducentOasisProfesionální	EpicProducentOasisProfesionální	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
</s>
<s>
Allmusic	Allmusic	k1gMnSc1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
The	The	k?
Guardian	Guardian	k1gInSc1
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
NME	NME	kA
(	(	kIx(
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pitchfork	Pitchfork	k1gInSc1
Media	medium	k1gNnSc2
(	(	kIx(
<g/>
1.2	1.2	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rolling	Rolling	k1gInSc1
Stone	ston	k1gInSc5
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oasis	Oasis	k1gFnSc1
chronologicky	chronologicky	k6eAd1
</s>
<s>
Familiar	Familiar	k1gMnSc1
to	ten	k3xDgNnSc4
Millions	Millionsa	k1gFnPc2
<g/>
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Heathen	Heathit	k5eAaBmNgMnS,k5eAaImNgMnS,k5eAaPmNgMnS
Chemistry	Chemistr	k1gMnPc7
<g/>
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Believe	Belieev	k1gFnSc2
the	the	k?
Truth	Truth	k1gMnSc1
<g/>
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Heathen	Heathen	k1nSc1
Chemistry	chemistry	k1nSc1
je	být	k5eAaImIp3nS
páté	pátý	k4xOgNnSc1
studiové	studiový	k2eAgNnSc1d1
album	album	k1gNnSc1
anglické	anglický	k2eAgFnSc2d1
rockové	rockový	k2eAgFnSc2d1
kapely	kapela	k1gFnSc2
Oasis	Oasis	k1nSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
u	u	k7c2
Big	Big	k1gFnSc2
Brother	Brother	kA
Records	Records	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
skladeb	skladba	k1gFnPc2
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
NázevAutor	NázevAutor	k1gMnSc1
</s>
<s>
Délka	délka	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
The	The	k1gMnSc3
Hindu	hind	k1gMnSc3
Times	Times	k1gInSc4
<g/>
“	“	k?
Noel	Noel	k1gInSc1
Gallagher	Gallaghra	k1gFnPc2
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
46	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Force	force	k1gFnSc1
of	of	k?
Nature	Natur	k1gMnSc5
<g/>
“	“	k?
N.	N.	kA
Gallagher	Gallaghra	k1gFnPc2
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
51	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Hung	Hung	k1gInSc1
in	in	k?
a	a	k8xC
Bad	Bad	k1gMnSc1
Place	plac	k1gInSc6
<g/>
“	“	k?
Gem	gem	k1gInSc1
Archer	Archra	k1gFnPc2
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
28	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Stop	stop	k1gInSc1
Crying	Crying	k1gInSc1
Your	Youra	k1gFnPc2
Heart	Hearta	k1gFnPc2
Out	Out	k1gFnSc2
<g/>
“	“	k?
N.	N.	kA
Gallagher	Gallaghra	k1gFnPc2
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Songbird	Songbird	k1gInSc1
<g/>
“	“	k?
Liam	Liam	k1gInSc1
Gallagher	Gallaghra	k1gFnPc2
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Little	Little	k1gFnSc2
by	by	kYmCp3nP
Little	Little	k1gFnSc1
<g/>
“	“	k?
N.	N.	kA
Gallagher	Gallaghra	k1gFnPc2
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
53	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
A	a	k9
Quick	Quick	k1gMnSc1
Peep	Peep	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
instrumentální	instrumentální	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
Andy	Anda	k1gFnSc2
Bell	bell	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
(	(	kIx(
<g/>
Probably	Probably	k1gMnSc1
<g/>
)	)	kIx)
All	All	k1gMnSc1
in	in	k?
the	the	k?
Mind	Mind	k1gInSc1
<g/>
“	“	k?
N.	N.	kA
Gallagher	Gallaghra	k1gFnPc2
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
She	She	k1gMnSc1
Is	Is	k1gMnSc1
Love	lov	k1gInSc5
<g/>
“	“	k?
N.	N.	kA
Gallagher	Gallaghra	k1gFnPc2
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Born	Born	k1gMnSc1
on	on	k3xPp3gMnSc1
a	a	k8xC
Different	Different	k1gMnSc1
Cloud	Cloud	k1gMnSc1
<g/>
“	“	k?
L.	L.	kA
Gallagher	Gallaghra	k1gFnPc2
</s>
<s>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Better	Better	k1gMnSc1
Man	Man	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS
skrytou	skrytý	k2eAgFnSc4d1
skladbu	skladba	k1gFnSc4
„	„	k?
<g/>
The	The	k1gFnSc1
Cage	Cage	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
L.	L.	kA
Gallagher	Gallaghra	k1gFnPc2
<g/>
/	/	kIx~
<g/>
N.	N.	kA
Gallagher	Gallaghra	k1gFnPc2
</s>
<s>
38	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Oasis	Oasis	k1gFnSc1
</s>
<s>
Liam	Liam	k1gMnSc1
Gallagher	Gallaghra	k1gFnPc2
–	–	k?
zpěv	zpěv	k1gInSc1
<g/>
,	,	kIx,
tamburína	tamburína	k1gFnSc1
<g/>
,	,	kIx,
akustická	akustický	k2eAgFnSc1d1
kytara	kytara	k1gFnSc1
v	v	k7c4
"	"	kIx"
<g/>
Songbird	Songbird	k1gInSc4
<g/>
"	"	kIx"
</s>
<s>
Noel	Noel	k1gMnSc1
Gallagher	Gallaghra	k1gFnPc2
–	–	k?
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
klávesy	klávesa	k1gFnPc1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
</s>
<s>
Gem	gem	k1gInSc1
Archer	Archra	k1gFnPc2
–	–	k?
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
klávesy	klávesa	k1gFnSc2
<g/>
,	,	kIx,
klavír	klavír	k1gInSc4
v	v	k7c6
„	„	k?
<g/>
Stop	stop	k2eAgInSc4d1
Crying	Crying	k1gInSc4
Your	Youra	k1gFnPc2
Heart	Hearta	k1gFnPc2
Out	Out	k1gFnSc2
<g/>
“	“	k?
</s>
<s>
Andy	Anda	k1gFnPc1
Bell	bell	k1gInSc1
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Alan	Alan	k1gMnSc1
White	Whit	k1gInSc5
–	–	k?
bicí	bicí	k2eAgFnSc7d1
</s>
<s>
Další	další	k2eAgMnPc1d1
hudebníci	hudebník	k1gMnPc1
</s>
<s>
Paul	Paul	k1gMnSc1
Stacey	Stacea	k1gFnSc2
–	–	k?
mellotron	mellotron	k1gInSc1
v	v	k7c6
„	„	k?
<g/>
The	The	k1gMnSc3
Hindu	hind	k1gMnSc3
Times	Times	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
klavír	klavír	k1gInSc4
ve	v	k7c6
„	„	k?
<g/>
Force	force	k1gFnSc6
of	of	k?
Nature	Natur	k1gMnSc5
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Hung	Hung	k1gInSc1
in	in	k?
a	a	k8xC
Bad	Bad	k1gMnSc1
Place	plac	k1gInSc6
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
Born	Born	k1gMnSc1
on	on	k3xPp3gMnSc1
a	a	k8xC
Different	Different	k1gMnSc1
Cloud	Cloud	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
Hammondovy	Hammondův	k2eAgInPc1d1
varhany	varhany	k1gInPc1
v	v	k7c4
„	„	k?
<g/>
Little	Little	k1gFnSc2
by	by	kYmCp3nP
Little	Little	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
Mike	Mike	k6eAd1
Rowe	Rowe	k1gInSc1
–	–	k?
klavír	klavír	k1gInSc4
v	v	k7c6
„	„	k?
<g/>
Stop	stop	k2eAgInSc4d1
Crying	Crying	k1gInSc4
Your	Youra	k1gFnPc2
Heart	Hearta	k1gFnPc2
Out	Out	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
Born	Born	k1gMnSc1
on	on	k3xPp3gMnSc1
a	a	k8xC
Different	Different	k1gMnSc1
Cloud	Cloud	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
harmonium	harmonium	k1gNnSc1
v	v	k7c6
„	„	k?
<g/>
She	She	k1gMnSc1
Is	Is	k1gMnSc1
Love	lov	k1gInSc5
<g/>
“	“	k?
<g/>
,	,	kIx,
Hammondovy	Hammondův	k2eAgFnPc1d1
varhany	varhany	k1gFnPc1
v	v	k7c6
„	„	k?
<g/>
(	(	kIx(
<g/>
Probably	Probably	k1gMnSc1
<g/>
)	)	kIx)
All	All	k1gMnSc1
in	in	k?
the	the	k?
Mind	Mind	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
She	She	k1gMnSc1
Is	Is	k1gMnSc1
Love	lov	k1gInSc5
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
Born	Born	k1gMnSc1
on	on	k3xPp3gMnSc1
a	a	k8xC
Different	Different	k1gMnSc1
Cloud	Cloud	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
Johnny	Johnn	k1gInPc1
Marr	Marra	k1gFnPc2
–	–	k?
kytarové	kytarový	k2eAgNnSc4d1
sólo	sólo	k1gNnSc4
v	v	k7c6
„	„	k?
<g/>
(	(	kIx(
<g/>
Probably	Probably	k1gMnSc1
<g/>
)	)	kIx)
All	All	k1gMnSc1
in	in	k?
the	the	k?
Mind	Mind	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
slide	slide	k1gInSc1
guitar	guitar	k1gInSc1
v	v	k7c6
„	„	k?
<g/>
Born	Born	k1gMnSc1
on	on	k3xPp3gMnSc1
a	a	k8xC
Different	Different	k1gMnSc1
Cloud	Cloud	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
kytara	kytara	k1gFnSc1
a	a	k8xC
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
v	v	k7c6
„	„	k?
<g/>
Better	Better	k1gMnSc1
Man	Man	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
London	London	k1gMnSc1
Session	Session	k1gInSc4
Orchestra	orchestra	k1gFnSc1
–	–	k?
strunné	strunný	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
v	v	k7c6
„	„	k?
<g/>
Stop	stop	k2eAgInSc4d1
Crying	Crying	k1gInSc4
Your	Youra	k1gFnPc2
Heart	Hearta	k1gFnPc2
Out	Out	k1gFnSc2
<g/>
“	“	k?
</s>
<s>
Jay	Jay	k?
Darlington	Darlington	k1gInSc1
-	-	kIx~
klávesy	klávesa	k1gFnSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Heathen	Heathen	k2eAgInSc1d1
Chemistry	Chemistr	k1gMnPc7
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
http://www.allmusic.com/album/heathen-chemistry-mw0000219095	http://www.allmusic.com/album/heathen-chemistry-mw0000219095	k4
<g/>
↑	↑	k?
http://www.theguardian.com/music/2002/jun/21/popandrock.shopping	http://www.theguardian.com/music/2002/jun/21/popandrock.shopping	k1gInSc1
<g/>
↑	↑	k?
http://www.nme.com/reviews//6492	http://www.nme.com/reviews//6492	k4
<g/>
↑	↑	k?
http://pitchfork.com/reviews/albums/5931-heathen-chemistry/	http://pitchfork.com/reviews/albums/5931-heathen-chemistry/	k4
<g/>
↑	↑	k?
http://www.rollingstone.com/music/albumreviews/heathen-chemistry-20020606	http://www.rollingstone.com/music/albumreviews/heathen-chemistry-20020606	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Oasis	Oasis	k1gInSc1
Liam	Liam	k1gMnSc1
Gallagher	Gallaghra	k1gFnPc2
•	•	k?
Noel	Noel	k1gInSc1
Gallagher	Gallaghra	k1gFnPc2
•	•	k?
Gem	gem	k1gInSc1
Archer	Archra	k1gFnPc2
•	•	k?
Andy	Anda	k1gFnSc2
Bell	bell	k1gInSc1
•	•	k?
Chris	Chris	k1gFnSc2
Sharrock	Sharrock	k1gMnSc1
Paul	Paul	k1gMnSc1
"	"	kIx"
<g/>
Bonehead	Bonehead	k1gInSc1
<g/>
"	"	kIx"
Arthurs	Arthurs	k1gInSc1
•	•	k?
Tony	Tony	k1gMnSc1
McCarroll	McCarroll	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
"	"	kIx"
<g/>
Guigsy	Guigs	k1gInPc1
<g/>
"	"	kIx"
McGuigan	McGuigan	k1gMnSc1
•	•	k?
Zak	Zak	k1gMnSc1
Starkey	Starkea	k1gFnSc2
•	•	k?
Alan	Alan	k1gMnSc1
White	Whit	k1gInSc5
Studiová	studiový	k2eAgFnSc5d1
alba	album	k1gNnPc4
</s>
<s>
Definitely	Definitela	k1gFnPc1
Maybe	Mayb	k1gInSc5
•	•	k?
(	(	kIx(
<g/>
What	What	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
the	the	k?
Story	story	k1gFnSc1
<g/>
)	)	kIx)
Morning	Morning	k1gInSc1
Glory	Glora	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
•	•	k?
Be	Be	k1gMnSc1
Here	Her	k1gFnSc2
Now	Now	k1gMnSc1
•	•	k?
Standing	Standing	k1gInSc1
on	on	k3xPp3gInSc1
the	the	k?
Shoulder	Shoulder	k1gInSc1
of	of	k?
Giants	Giants	k1gInSc1
•	•	k?
Heathen	Heathen	k1gInSc1
Chemistry	Chemistr	k1gMnPc4
•	•	k?
Don	dona	k1gFnPc2
<g/>
'	'	kIx"
<g/>
t	t	k?
Believe	Belieev	k1gFnSc2
the	the	k?
Truth	Truth	k1gMnSc1
•	•	k?
Dig	Dig	k1gMnSc1
Out	Out	k1gMnSc1
Your	Your	k1gMnSc1
Soul	Soul	k1gInSc4
Koncertní	koncertní	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
Familiar	Familiar	k1gInSc1
to	ten	k3xDgNnSc1
Millions	Millions	k1gInSc1
•	•	k?
Live	Live	k1gInSc1
from	from	k1gMnSc1
The	The	k1gFnSc1
Roundhouse	Roundhouse	k1gFnSc2
Kompilační	kompilační	k2eAgNnSc4d1
alba	album	k1gNnPc4
</s>
<s>
Definitely	Definitela	k1gFnPc1
Maybe	Mayb	k1gInSc5
(	(	kIx(
<g/>
singles	singlesa	k1gFnPc2
box	box	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Morning	Morning	k1gInSc1
Glory	Glora	k1gFnSc2
(	(	kIx(
<g/>
singles	singles	k1gInSc1
box	box	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
The	The	k1gMnSc1
Masterplan	Masterplan	k1gMnSc1
•	•	k?
Stop	stop	k1gInSc1
the	the	k?
Clocks	Clocks	k1gInSc1
•	•	k?
Time	Time	k1gInSc1
Flies	Flies	k1gInSc1
<g/>
...	...	k?
1994	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
EP	EP	kA
</s>
<s>
Live	Live	k6eAd1
Demonstration	Demonstration	k1gInSc1
•	•	k?
Stop	stop	k1gInSc1
the	the	k?
Clocks	Clocks	k1gInSc1
•	•	k?
Boy	boa	k1gFnSc2
with	witha	k1gFnPc2
the	the	k?
Blues	blues	k1gNnSc1
Videa	video	k1gNnSc2
</s>
<s>
Live	Live	k6eAd1
by	by	kYmCp3nS
the	the	k?
Sea	Sea	k1gMnSc3
•	•	k?
…	…	k?
<g/>
There	Ther	k1gInSc5
and	and	k?
Then	Then	k1gMnSc1
•	•	k?
Familiar	Familiar	k1gMnSc1
to	ten	k3xDgNnSc1
Millions	Millions	k1gInSc1
•	•	k?
Definitely	Definitela	k1gFnSc2
Maybe	Mayb	k1gInSc5
•	•	k?
Lord	lord	k1gMnSc1
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Slow	Slow	k1gMnSc1
Me	Me	k1gMnSc1
Down	Down	k1gMnSc1
•	•	k?
Time	Tim	k1gMnSc4
Flies	Flies	k1gMnSc1
<g/>
...	...	k?
1994	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
Turné	turné	k1gNnSc2
</s>
<s>
Definitely	Definitela	k1gFnPc1
Maybe	Mayb	k1gInSc5
Tour	Tour	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
What	What	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
the	the	k?
Story	story	k1gFnSc1
<g/>
)	)	kIx)
Morning	Morning	k1gInSc1
Glory	Glora	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Tour	Tour	k1gMnSc1
•	•	k?
Be	Be	k1gMnSc1
Here	Her	k1gFnSc2
Now	Now	k1gMnSc1
Tour	Tour	k1gMnSc1
•	•	k?
Standing	Standing	k1gInSc1
on	on	k3xPp3gInSc1
the	the	k?
Shoulder	Shoulder	k1gInSc1
of	of	k?
Giants	Giants	k1gInSc1
Tour	Tour	k1gMnSc1
•	•	k?
The	The	k1gMnSc1
Tour	Tour	k1gMnSc1
of	of	k?
Brotherly	Brotherla	k1gFnSc2
Love	lov	k1gInSc5
•	•	k?
Heathen	Heathna	k1gFnPc2
Chemistry	Chemistr	k1gMnPc4
Tour	Toura	k1gFnPc2
•	•	k?
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Believe	Belieev	k1gFnSc2
the	the	k?
Truth	Truth	k1gMnSc1
Tour	Tour	k1gMnSc1
•	•	k?
Dig	Dig	k1gMnSc1
Out	Out	k1gMnSc1
Your	Youra	k1gFnPc2
Soul	Soul	k1gInSc1
Tour	Tour	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
