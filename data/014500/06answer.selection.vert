<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
některým	některý	k3yIgMnPc3	některý
Spojencům	spojenec	k1gMnPc3	spojenec
v	v	k7c6	v
počínající	počínající	k2eAgFnSc6d1	počínající
válce	válka	k1gFnSc6	válka
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
aktivně	aktivně	k6eAd1	aktivně
do	do	k7c2	do
války	válka	k1gFnSc2	válka
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1941	[number]	k4	1941
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Japonsko	Japonsko	k1gNnSc1	Japonsko
udeřilo	udeřit	k5eAaPmAgNnS	udeřit
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
námořní	námořní	k2eAgFnSc4d1	námořní
základnu	základna	k1gFnSc4	základna
v	v	k7c6	v
Pearl	Pearla	k1gFnPc2	Pearla
Harboru	Harbor	k1gInSc2	Harbor
<g/>
.	.	kIx.	.
</s>
