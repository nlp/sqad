<s>
Hélios	Hélios	k1gMnSc1	Hélios
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Ἥ	Ἥ	k?	Ἥ
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Sol	sol	k1gNnSc1	sol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Titána	Titán	k1gMnSc2	Titán
Hyperíona	Hyperíon	k1gMnSc2	Hyperíon
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
a	a	k8xC	a
sestry	sestra	k1gFnSc2	sestra
Theie	Theie	k1gFnSc2	Theie
byl	být	k5eAaImAgMnS	být
bůh	bůh	k1gMnSc1	bůh
a	a	k8xC	a
zosobnění	zosobnění	k1gNnSc1	zosobnění
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
sestrou	sestra	k1gFnSc7	sestra
byla	být	k5eAaImAgFnS	být
Seléné	Seléné	k1gNnSc4	Seléné
<g/>
,	,	kIx,	,
bohyně	bohyně	k1gFnSc1	bohyně
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
Éós	Éós	k1gFnSc2	Éós
<g/>
,	,	kIx,	,
bohyně	bohyně	k1gFnSc1	bohyně
jitra	jitro	k1gNnSc2	jitro
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
Persa	Persa	k1gFnSc1	Persa
<g/>
,	,	kIx,	,
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
měl	mít	k5eAaImAgMnS	mít
syna	syn	k1gMnSc4	syn
Aiéta	Aiét	k1gMnSc4	Aiét
<g/>
,	,	kIx,	,
pozdějšího	pozdní	k2eAgMnSc4d2	pozdější
krále	král	k1gMnSc4	král
v	v	k7c6	v
Kolchidě	Kolchida	k1gFnSc6	Kolchida
a	a	k8xC	a
dcery	dcera	k1gFnSc2	dcera
Kirké	Kirká	k1gFnSc2	Kirká
<g/>
,	,	kIx,	,
proslulou	proslulý	k2eAgFnSc4d1	proslulá
kouzelnici	kouzelnice	k1gFnSc4	kouzelnice
a	a	k8xC	a
Pásifaé	Pásifaé	k1gNnSc4	Pásifaé
<g/>
,	,	kIx,	,
manželku	manželka	k1gFnSc4	manželka
krále	král	k1gMnSc2	král
Mínoa	Mínous	k1gMnSc2	Mínous
<g/>
.	.	kIx.	.
</s>
<s>
Klymené	Klymený	k2eAgFnPc1d1	Klymený
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Ókeanova	Ókeanův	k2eAgFnSc1d1	Ókeanův
mu	on	k3xPp3gMnSc3	on
porodila	porodit	k5eAaPmAgFnS	porodit
sedm	sedm	k4xCc4	sedm
dcer	dcera	k1gFnPc2	dcera
<g/>
,	,	kIx,	,
zvaných	zvaný	k2eAgFnPc2d1	zvaná
Héliovny	Héliovna	k1gFnSc2	Héliovna
<g/>
,	,	kIx,	,
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
Faethonta	Faethón	k1gMnSc4	Faethón
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
zabil	zabít	k5eAaPmAgMnS	zabít
bleskem	blesk	k1gInSc7	blesk
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
bůh	bůh	k1gMnSc1	bůh
Zeus	Zeusa	k1gFnPc2	Zeusa
<g/>
,	,	kIx,	,
když	když	k8xS	když
Faethón	Faethón	k1gMnSc1	Faethón
nezvládal	zvládat	k5eNaImAgMnS	zvládat
jízdu	jízda	k1gFnSc4	jízda
slunečním	sluneční	k2eAgInSc7d1	sluneční
vozem	vůz	k1gInSc7	vůz
a	a	k8xC	a
hrozilo	hrozit	k5eAaImAgNnS	hrozit
zničení	zničení	k1gNnSc1	zničení
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
milenkou	milenka	k1gFnSc7	milenka
Aiglé	Aigl	k1gMnPc1	Aigl
<g/>
,	,	kIx,	,
prý	prý	k9	prý
nejkrásnější	krásný	k2eAgInPc1d3	nejkrásnější
z	z	k7c2	z
Najád	najáda	k1gFnPc2	najáda
<g/>
,	,	kIx,	,
zplodil	zplodit	k5eAaPmAgMnS	zplodit
Charitky	Charitka	k1gFnPc4	Charitka
<g/>
,	,	kIx,	,
bohyně	bohyně	k1gFnPc4	bohyně
půvabu	půvab	k1gInSc2	půvab
a	a	k8xC	a
krásy	krása	k1gFnSc2	krása
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
jako	jako	k9	jako
jejich	jejich	k3xOp3gMnSc1	jejich
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
verzích	verze	k1gFnPc6	verze
uvádí	uvádět	k5eAaImIp3nS	uvádět
sám	sám	k3xTgMnSc1	sám
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
bůh	bůh	k1gMnSc1	bůh
Zeus	Zeusa	k1gFnPc2	Zeusa
a	a	k8xC	a
matkou	matka	k1gFnSc7	matka
prý	prý	k9	prý
je	být	k5eAaImIp3nS	být
mořská	mořský	k2eAgFnSc1d1	mořská
bohyně	bohyně	k1gFnSc1	bohyně
Eurynomé	Eurynomý	k2eAgFnSc2d1	Eurynomý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hélios	Hélios	k1gMnSc1	Hélios
je	být	k5eAaImIp3nS	být
zosobněním	zosobnění	k1gNnSc7	zosobnění
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jeho	on	k3xPp3gInSc2	on
jasu	jas	k1gInSc2	jas
a	a	k8xC	a
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyobrazován	vyobrazován	k2eAgMnSc1d1	vyobrazován
jako	jako	k8xS	jako
výrazná	výrazný	k2eAgFnSc1d1	výrazná
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
zlaté	zlatý	k2eAgInPc4d1	zlatý
vlasy	vlas	k1gInPc4	vlas
jsou	být	k5eAaImIp3nP	být
korunovány	korunován	k2eAgInPc4d1	korunován
oslňujícími	oslňující	k2eAgInPc7d1	oslňující
paprsky	paprsek	k1gInPc7	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
každodenní	každodenní	k2eAgFnSc4d1	každodenní
povinnost	povinnost	k1gFnSc4	povinnost
plnil	plnit	k5eAaImAgInS	plnit
ve	v	k7c6	v
zlatém	zlatý	k2eAgInSc6d1	zlatý
voze	vůz	k1gInSc6	vůz
<g/>
,	,	kIx,	,
taženém	tažený	k2eAgInSc6d1	tažený
čtyřmi	čtyři	k4xCgMnPc7	čtyři
okřídlenými	okřídlený	k2eAgMnPc7d1	okřídlený
koňmi	kůň	k1gMnPc7	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
kohout	kohout	k1gInSc1	kohout
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
ranní	ranní	k2eAgFnSc4d1	ranní
cestu	cesta	k1gFnSc4	cesta
budil	budit	k5eAaImAgMnS	budit
<g/>
,	,	kIx,	,
před	před	k7c7	před
Héliem	hélium	k1gNnSc7	hélium
vycházela	vycházet	k5eAaImAgFnS	vycházet
Éós	Éós	k1gFnSc1	Éós
-	-	kIx~	-
Jitřenka	Jitřenka	k1gFnSc1	Jitřenka
a	a	k8xC	a
za	za	k7c4	za
ní	on	k3xPp3gFnSc3	on
na	na	k7c6	na
východě	východ	k1gInSc6	východ
vyjelo	vyjet	k5eAaPmAgNnS	vyjet
spřežení	spřežení	k1gNnSc1	spřežení
boha	bůh	k1gMnSc2	bůh
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
cesta	cesta	k1gFnSc1	cesta
začínala	začínat	k5eAaImAgFnS	začínat
na	na	k7c6	na
dalekém	daleký	k2eAgInSc6d1	daleký
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
prý	prý	k9	prý
někde	někde	k6eAd1	někde
v	v	k7c6	v
Kolchidě	Kolchida	k1gFnSc6	Kolchida
a	a	k8xC	a
směřovala	směřovat	k5eAaImAgFnS	směřovat
nad	nad	k7c7	nad
celou	celý	k2eAgFnSc7d1	celá
zemí	zem	k1gFnSc7	zem
až	až	k9	až
k	k	k7c3	k
dalekému	daleký	k2eAgInSc3d1	daleký
západu	západ	k1gInSc3	západ
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Blažených	blažený	k2eAgMnPc2d1	blažený
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc1	jeho
koně	kůň	k1gMnPc1	kůň
vykoupali	vykoupat	k5eAaPmAgMnP	vykoupat
a	a	k8xC	a
napásli	napást	k5eAaPmAgMnP	napást
a	a	k8xC	a
Hélios	Hélios	k1gMnSc1	Hélios
potom	potom	k6eAd1	potom
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
nádherném	nádherný	k2eAgInSc6d1	nádherný
člunu	člun	k1gInSc6	člun
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
sám	sám	k3xTgMnSc1	sám
božský	božský	k2eAgMnSc1d1	božský
kovář	kovář	k1gMnSc1	kovář
Héfaistos	Héfaistos	k1gMnSc1	Héfaistos
<g/>
,	,	kIx,	,
odplouval	odplouvat	k5eAaImAgMnS	odplouvat
po	po	k7c6	po
vodách	voda	k1gFnPc6	voda
oceánu	oceán	k1gInSc2	oceán
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
jeho	jeho	k3xOp3gFnSc4	jeho
pouť	pouť	k1gFnSc4	pouť
opět	opět	k6eAd1	opět
začínala	začínat	k5eAaImAgFnS	začínat
<g/>
.	.	kIx.	.
</s>
<s>
Hélios	Hélios	k1gMnSc1	Hélios
dával	dávat	k5eAaImAgMnS	dávat
svým	svůj	k3xOyFgNnSc7	svůj
světlem	světlo	k1gNnSc7	světlo
a	a	k8xC	a
teplem	teplo	k1gNnSc7	teplo
zemi	zem	k1gFnSc6	zem
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
pouti	pouť	k1gFnSc6	pouť
všechno	všechen	k3xTgNnSc4	všechen
viděl	vidět	k5eAaImAgMnS	vidět
-	-	kIx~	-
a	a	k8xC	a
mnohé	mnohé	k1gNnSc4	mnohé
si	se	k3xPyFc3	se
nenechal	nechat	k5eNaPmAgMnS	nechat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bájích	báj	k1gFnPc6	báj
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
svědkem	svědek	k1gMnSc7	svědek
nevěry	nevěra	k1gFnSc2	nevěra
bohyně	bohyně	k1gFnSc2	bohyně
Afrodíté	Afrodítý	k2eAgFnPc4d1	Afrodítý
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
války	válka	k1gFnSc2	válka
Areem	Ares	k1gMnSc7	Ares
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
prozradil	prozradit	k5eAaPmAgMnS	prozradit
podvedenému	podvedený	k2eAgMnSc3d1	podvedený
manželovi	manžel	k1gMnSc3	manžel
Héfaistovi	Héfaista	k1gMnSc3	Héfaista
<g/>
.	.	kIx.	.
</s>
<s>
Viděl	vidět	k5eAaImAgMnS	vidět
také	také	k9	také
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bůh	bůh	k1gMnSc1	bůh
podsvětí	podsvětí	k1gNnSc2	podsvětí
Hádés	Hádésa	k1gFnPc2	Hádésa
unesl	unést	k5eAaPmAgMnS	unést
Persefonu	Persefona	k1gFnSc4	Persefona
a	a	k8xC	a
prozradil	prozradit	k5eAaPmAgMnS	prozradit
to	ten	k3xDgNnSc4	ten
její	její	k3xOp3gFnSc3	její
nešťastné	šťastný	k2eNgFnSc3d1	nešťastná
matce	matka	k1gFnSc3	matka
Démétér	Démétér	k1gFnSc2	Démétér
<g/>
.	.	kIx.	.
</s>
<s>
Héliovou	Héliův	k2eAgFnSc7d1	Héliova
pýchou	pýcha	k1gFnSc7	pýcha
bylo	být	k5eAaImAgNnS	být
sedm	sedm	k4xCc1	sedm
stád	stádo	k1gNnPc2	stádo
krav	kráva	k1gFnPc2	kráva
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
stád	stádo	k1gNnPc2	stádo
beránků	beránek	k1gMnPc2	beránek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Thrínakii	Thrínakie	k1gFnSc4	Thrínakie
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
stádo	stádo	k1gNnSc1	stádo
čítalo	čítat	k5eAaImAgNnS	čítat
50	[number]	k4	50
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Hlídaly	hlídat	k5eAaImAgFnP	hlídat
mu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc2	jeho
dcery	dcera	k1gFnSc2	dcera
Faethúsa	Faethús	k1gMnSc2	Faethús
a	a	k8xC	a
Lampetia	Lampetius	k1gMnSc2	Lampetius
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mu	on	k3xPp3gNnSc3	on
porodila	porodit	k5eAaPmAgFnS	porodit
nymfa	nymfa	k1gFnSc1	nymfa
Neaira	Neaira	k1gFnSc1	Neaira
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
těchto	tento	k3xDgNnPc6	tento
stádech	stádo	k1gNnPc6	stádo
se	se	k3xPyFc4	se
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
několik	několik	k4yIc4	několik
kusů	kus	k1gInPc2	kus
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zabili	zabít	k5eAaPmAgMnP	zabít
Odysseovi	Odysseův	k2eAgMnPc1d1	Odysseův
druhové	druh	k1gMnPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Hélios	Hélios	k1gMnSc1	Hélios
nepatřil	patřit	k5eNaImAgMnS	patřit
k	k	k7c3	k
hlavním	hlavní	k2eAgMnPc3d1	hlavní
bohům	bůh	k1gMnPc3	bůh
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
pravomoci	pravomoc	k1gFnSc6	pravomoc
trestat	trestat	k5eAaImF	trestat
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
proto	proto	k8xC	proto
o	o	k7c4	o
potrestání	potrestání	k1gNnSc4	potrestání
zlodějů	zloděj	k1gMnPc2	zloděj
požádat	požádat	k5eAaPmF	požádat
Dia	Dia	k1gFnSc3	Dia
<g/>
.	.	kIx.	.
</s>
<s>
Héliovým	Héliův	k2eAgInSc7d1	Héliův
osobním	osobní	k2eAgInSc7d1	osobní
majetkem	majetek	k1gInSc7	majetek
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
Rhodos	Rhodos	k1gInSc1	Rhodos
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
vyzvedl	vyzvednout	k5eAaPmAgMnS	vyzvednout
z	z	k7c2	z
hlubin	hlubina	k1gFnPc2	hlubina
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgMnS	získat
ho	on	k3xPp3gMnSc4	on
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
si	se	k3xPyFc3	se
bohové	bůh	k1gMnPc1	bůh
rozdělovali	rozdělovat	k5eAaImAgMnP	rozdělovat
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
na	na	k7c4	na
Hélia	hélium	k1gNnPc4	hélium
zapomněli	zapomnět	k5eAaImAgMnP	zapomnět
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
jemu	on	k3xPp3gMnSc3	on
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
zbudovali	zbudovat	k5eAaPmAgMnP	zbudovat
Rhodský	rhodský	k2eAgInSc4d1	rhodský
kolos	kolos	k1gInSc4	kolos
<g/>
,	,	kIx,	,
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
sochu	socha	k1gFnSc4	socha
nad	nad	k7c7	nad
vjezdem	vjezd	k1gInSc7	vjezd
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
divů	div	k1gMnPc2	div
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
při	při	k7c6	při
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
v	v	k7c6	v
r.	r.	kA	r.
225	[number]	k4	225
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Slovník	slovník	k1gInSc4	slovník
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
,	,	kIx,	,
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
Graves	Graves	k1gMnSc1	Graves
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
Řecké	řecký	k2eAgInPc4d1	řecký
mýty	mýtus	k1gInPc4	mýtus
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7309-153-4	[number]	k4	80-7309-153-4
Houtzager	Houtzagra	k1gFnPc2	Houtzagra
<g/>
,	,	kIx,	,
Guus	Guusa	k1gFnPc2	Guusa
<g/>
,	,	kIx,	,
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
ISBN80-7234-287-8	ISBN80-7234-287-8	k1gFnSc1	ISBN80-7234-287-8
Gerhard	Gerhard	k1gMnSc1	Gerhard
Löwe	Löwe	k1gInSc1	Löwe
<g/>
,	,	kIx,	,
Heindrich	Heindrich	k1gMnSc1	Heindrich
Alexander	Alexandra	k1gFnPc2	Alexandra
Stoll	Stoll	k1gMnSc1	Stoll
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
Antiky	antika	k1gFnPc1	antika
Apollón	Apollón	k1gMnSc1	Apollón
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hélios	Hélios	k1gMnSc1	Hélios
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
