<p>
<s>
Kilt	Kilt	k1gInSc1	Kilt
je	být	k5eAaImIp3nS	být
výhradně	výhradně	k6eAd1	výhradně
mužská	mužský	k2eAgFnSc1d1	mužská
součást	součást	k1gFnSc1	součást
skotského	skotský	k2eAgInSc2d1	skotský
národního	národní	k2eAgInSc2d1	národní
kroje	kroj	k1gInSc2	kroj
(	(	kIx(	(
<g/>
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
kilt	kilt	k1gInSc1	kilt
nenosí	nosit	k5eNaImIp3nS	nosit
<g/>
)	)	kIx)	)
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
mužské	mužský	k2eAgFnSc2d1	mužská
zavinovací	zavinovací	k2eAgFnSc2d1	zavinovací
skládané	skládaný	k2eAgFnSc2d1	skládaná
suknice	suknice	k1gFnSc2	suknice
s	s	k7c7	s
barevným	barevný	k2eAgNnSc7d1	barevné
kárem	káro	k1gNnSc7	káro
<g/>
,	,	kIx,	,
ozdobnými	ozdobný	k2eAgFnPc7d1	ozdobná
třásněmi	třáseň	k1gFnPc7	třáseň
a	a	k8xC	a
ozdobnou	ozdobný	k2eAgFnSc7d1	ozdobná
broží	brož	k1gFnSc7	brož
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
zavíracího	zavírací	k2eAgInSc2d1	zavírací
špendlíku	špendlík	k1gInSc2	špendlík
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
kiltpin	kiltpin	k1gInSc1	kiltpin
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
kiltů	kilt	k1gInPc2	kilt
<g/>
,	,	kIx,	,
lišících	lišící	k2eAgInPc2d1	lišící
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
vzorem	vzor	k1gInSc7	vzor
a	a	k8xC	a
také	také	k9	také
původem	původ	k1gInSc7	původ
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jiných	jiný	k1gMnPc2	jiný
existují	existovat	k5eAaImIp3nP	existovat
ještě	ještě	k6eAd1	ještě
irské	irský	k2eAgFnPc4d1	irská
a	a	k8xC	a
velšské	velšský	k2eAgFnPc4d1	velšská
kilty	kilta	k1gFnPc4	kilta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kilt	Kilt	k1gInSc1	Kilt
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
skotská	skotský	k2eAgFnSc1d1	skotská
sukně	sukně	k1gFnPc1	sukně
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
tradičního	tradiční	k2eAgInSc2d1	tradiční
skotského	skotský	k2eAgInSc2d1	skotský
mužského	mužský	k2eAgInSc2d1	mužský
kroje	kroj	k1gInSc2	kroj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zhotovena	zhotovit	k5eAaPmNgFnS	zhotovit
z	z	k7c2	z
vlněné	vlněný	k2eAgFnSc2d1	vlněná
kostkované	kostkovaný	k2eAgFnSc2d1	kostkovaná
látky	látka	k1gFnSc2	látka
zvané	zvaný	k2eAgInPc1d1	zvaný
tartan	tartan	k1gInSc1	tartan
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
vzory	vzor	k1gInPc1	vzor
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
asociovány	asociovat	k5eAaBmNgInP	asociovat
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
skotskými	skotský	k2eAgInPc7d1	skotský
rody	rod	k1gInPc7	rod
-	-	kIx~	-
klany	klan	k1gInPc7	klan
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
novodobou	novodobý	k2eAgFnSc4d1	novodobá
asociaci	asociace	k1gFnSc4	asociace
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
přiřazování	přiřazování	k1gNnSc1	přiřazování
vzorů	vzor	k1gInPc2	vzor
nemá	mít	k5eNaImIp3nS	mít
historický	historický	k2eAgInSc4d1	historický
podklad	podklad	k1gInSc4	podklad
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
teprve	teprve	k6eAd1	teprve
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
skotského	skotský	k2eAgNnSc2d1	skotské
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kilt	Kilt	k1gInSc1	Kilt
je	být	k5eAaImIp3nS	být
dominantní	dominantní	k2eAgFnSc7d1	dominantní
součástí	součást	k1gFnSc7	součást
skotského	skotský	k2eAgInSc2d1	skotský
kroje	kroj	k1gInSc2	kroj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
další	další	k2eAgInSc1d1	další
součástmi	součást	k1gFnPc7	součást
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
zmíněný	zmíněný	k2eAgInSc4d1	zmíněný
kiltpin	kiltpin	k1gInSc4	kiltpin
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
vlněné	vlněný	k2eAgFnPc4d1	vlněná
podkolenky	podkolenka	k1gFnPc4	podkolenka
(	(	kIx(	(
<g/>
jednak	jednak	k8xC	jednak
jednobarevné	jednobarevný	k2eAgNnSc1d1	jednobarevné
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
tartanové	tartanový	k2eAgFnPc1d1	tartanová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sporran	sporran	k1gInSc1	sporran
(	(	kIx(	(
<g/>
kapsář	kapsář	k1gInSc1	kapsář
na	na	k7c6	na
řetízku	řetízek	k1gInSc6	řetízek
kolem	kolem	k7c2	kolem
pasu	pas	k1gInSc2	pas
<g/>
)	)	kIx)	)
a	a	k8xC	a
sgin	sgin	k1gMnSc1	sgin
dubh	dubh	k1gMnSc1	dubh
-	-	kIx~	-
dýka	dýka	k1gFnSc1	dýka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
zastrčená	zastrčený	k2eAgFnSc1d1	zastrčená
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
podkolence	podkolenka	k1gFnSc6	podkolenka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
základ	základ	k1gInSc1	základ
bývá	bývat	k5eAaImIp3nS	bývat
doplňován	doplňovat	k5eAaImNgInS	doplňovat
jednak	jednak	k8xC	jednak
slavnostním	slavnostní	k2eAgInSc7d1	slavnostní
oblekem	oblek	k1gInSc7	oblek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nosí	nosit	k5eAaImIp3nS	nosit
např.	např.	kA	např.
skotští	skotský	k2eAgMnPc1d1	skotský
dudáci	dudák	k1gMnPc1	dudák
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
slavnostním	slavnostní	k2eAgInSc7d1	slavnostní
civilním	civilní	k2eAgInSc7d1	civilní
oblekem	oblek	k1gInSc7	oblek
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Bonnie	Bonnie	k1gFnSc1	Bonnie
Prince	princa	k1gFnSc3	princa
Charlie	Charlie	k1gMnSc1	Charlie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
skotského	skotský	k2eAgInSc2d1	skotský
krátkého	krátký	k2eAgInSc2d1	krátký
fráčku	fráček	k1gInSc2	fráček
s	s	k7c7	s
vestou	vesta	k1gFnSc7	vesta
<g/>
,	,	kIx,	,
smokingové	smokingový	k2eAgFnSc2d1	smokingová
košile	košile	k1gFnSc2	košile
a	a	k8xC	a
motýlku	motýlek	k1gInSc2	motýlek
<g/>
,	,	kIx,	,
či	či	k8xC	či
neformálního	formální	k2eNgNnSc2d1	neformální
tweedového	tweedový	k2eAgNnSc2d1	tweedový
saka	sako	k1gNnSc2	sako
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnSc2d1	bílá
košile	košile	k1gFnSc2	košile
a	a	k8xC	a
kravaty	kravata	k1gFnSc2	kravata
<g/>
.	.	kIx.	.
</s>
<s>
Variant	variant	k1gInSc1	variant
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
-	-	kIx~	-
od	od	k7c2	od
pouhého	pouhý	k2eAgInSc2d1	pouhý
svetru	svetr	k1gInSc2	svetr
oblékaného	oblékaný	k2eAgInSc2d1	oblékaný
ke	k	k7c3	k
kiltu	kilto	k1gNnSc3	kilto
například	například	k6eAd1	například
po	po	k7c4	po
saka	sako	k1gNnPc4	sako
typu	typ	k1gInSc2	typ
Argylle	Argylle	k1gFnSc2	Argylle
<g/>
,	,	kIx,	,
Montrose	Montrosa	k1gFnSc6	Montrosa
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skotský	skotský	k2eAgInSc1d1	skotský
kilt	kilt	k1gInSc1	kilt
==	==	k?	==
</s>
</p>
<p>
<s>
Skotský	skotský	k2eAgInSc1d1	skotský
kilt	kilt	k1gInSc1	kilt
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
konstrukci	konstrukce	k1gFnSc4	konstrukce
a	a	k8xC	a
konvence	konvence	k1gFnPc4	konvence
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jej	on	k3xPp3gMnSc4	on
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
ostatní	ostatní	k2eAgFnSc2d1	ostatní
typů	typ	k1gInPc2	typ
kiltu	kilt	k1gInSc2	kilt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
šitá	šitý	k2eAgFnSc1d1	šitá
část	část	k1gFnSc1	část
oděvu	oděv	k1gInSc2	oděv
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
pase	pas	k1gInSc6	pas
omotává	omotávat	k5eAaImIp3nS	omotávat
kolem	kolem	k7c2	kolem
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Kilt	Kilt	k1gInSc1	Kilt
se	se	k3xPyFc4	se
zapíná	zapínat	k5eAaImIp3nS	zapínat
pomocí	pomocí	k7c2	pomocí
řemínků	řemínek	k1gInPc2	řemínek
a	a	k8xC	a
přezek	přezka	k1gFnPc2	přezka
<g/>
.	.	kIx.	.
</s>
<s>
Řemínky	řemínek	k1gInPc1	řemínek
jsou	být	k5eAaImIp3nP	být
připevněny	připevněn	k2eAgInPc1d1	připevněn
jak	jak	k8xC	jak
na	na	k7c6	na
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
konci	konec	k1gInSc6	konec
kiltu	kilt	k1gInSc2	kilt
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc1	jeden
řemínek	řemínek	k1gInSc1	řemínek
<g/>
)	)	kIx)	)
a	a	k8xC	a
otvory	otvor	k1gInPc1	otvor
procházejí	procházet	k5eAaImIp3nP	procházet
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zapínají	zapínat	k5eAaImIp3nP	zapínat
do	do	k7c2	do
přezek	přezka	k1gFnPc2	přezka
na	na	k7c6	na
vnějším	vnější	k2eAgInSc6d1	vnější
konci	konec	k1gInSc6	konec
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
vnějším	vnější	k2eAgInSc6d1	vnější
okraji	okraj	k1gInSc6	okraj
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
dva	dva	k4xCgInPc4	dva
řemínky	řemínek	k1gInPc4	řemínek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kilt	Kilt	k1gInSc1	Kilt
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
pase	pas	k1gInSc6	pas
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
kolen	koleno	k1gNnPc2	koleno
<g/>
.	.	kIx.	.
</s>
<s>
Vepředu	vepředu	k6eAd1	vepředu
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
překrývající	překrývající	k2eAgFnSc1d1	překrývající
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
apron	apron	k1gInSc4	apron
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
zástěra	zástěra	k1gFnSc1	zástěra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
kiltu	kilt	k1gInSc2	kilt
je	být	k5eAaImIp3nS	být
naskládaná	naskládaný	k2eAgFnSc1d1	naskládaná
do	do	k7c2	do
hustých	hustý	k2eAgInPc2d1	hustý
skladů	sklad	k1gInPc2	sklad
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
pleats	pleats	k1gInSc1	pleats
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
buď	buď	k8xC	buď
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzor	vzor	k1gInSc1	vzor
tartanu	tartan	k1gInSc2	tartan
i	i	k9	i
nadále	nadále	k6eAd1	nadále
nepřerušovaně	přerušovaně	k6eNd1	přerušovaně
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
civilní	civilní	k2eAgNnSc1d1	civilní
skládání	skládání	k1gNnSc1	skládání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
opakuje	opakovat	k5eAaImIp3nS	opakovat
dominantní	dominantní	k2eAgInSc4d1	dominantní
proužek	proužek	k1gInSc4	proužek
z	z	k7c2	z
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
dominantní	dominantní	k2eAgFnPc1d1	dominantní
proužky	proužka	k1gFnPc1	proužka
střídají	střídat	k5eAaImIp3nP	střídat
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
military	militara	k1gFnSc2	militara
style	styl	k1gInSc5	styl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
těmto	tento	k3xDgFnPc3	tento
hlubokým	hluboký	k2eAgFnPc3d1	hluboká
a	a	k8xC	a
hustým	hustý	k2eAgInPc3d1	hustý
skladům	sklad	k1gInPc3	sklad
je	být	k5eAaImIp3nS	být
délka	délka	k1gFnSc1	délka
materiálu	materiál	k1gInSc2	materiál
použitého	použitý	k2eAgInSc2d1	použitý
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
kvalitního	kvalitní	k2eAgInSc2d1	kvalitní
kiltu	kilt	k1gInSc2	kilt
šitého	šitý	k2eAgInSc2d1	šitý
na	na	k7c6	na
míru	mír	k1gInSc6	mír
cca	cca	kA	cca
7,5	[number]	k4	7,5
m.	m.	k?	m.
Tradičním	tradiční	k2eAgInSc7d1	tradiční
zvykem	zvyk	k1gInSc7	zvyk
je	být	k5eAaImIp3nS	být
nenosit	nosit	k5eNaImF	nosit
pod	pod	k7c7	pod
kiltem	kilt	k1gInSc7	kilt
spodní	spodní	k2eAgNnSc4d1	spodní
prádlo	prádlo	k1gNnSc4	prádlo
(	(	kIx(	(
<g/>
u	u	k7c2	u
vojenských	vojenský	k2eAgFnPc2d1	vojenská
jednotek	jednotka	k1gFnPc2	jednotka
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
vojenským	vojenský	k2eAgInSc7d1	vojenský
řádem	řád	k1gInSc7	řád
vyžadováno	vyžadovat	k5eAaImNgNnS	vyžadovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
tanečníci	tanečník	k1gMnPc1	tanečník
a	a	k8xC	a
sportovci	sportovec	k1gMnPc1	sportovec
oblékající	oblékající	k2eAgMnPc1d1	oblékající
kilt	kilt	k5eAaPmF	kilt
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
ryze	ryze	k6eAd1	ryze
praktických	praktický	k2eAgInPc2d1	praktický
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Materiál	materiál	k1gInSc1	materiál
===	===	k?	===
</s>
</p>
<p>
<s>
Typický	typický	k2eAgInSc1d1	typický
moderní	moderní	k2eAgInSc1d1	moderní
kilt	kilt	k1gInSc1	kilt
je	být	k5eAaImIp3nS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
z	z	k7c2	z
kepru	kepr	k1gInSc2	kepr
<g/>
,	,	kIx,	,
utkaného	utkaný	k2eAgInSc2d1	utkaný
z	z	k7c2	z
česané	česaný	k2eAgFnSc2d1	česaná
ovčí	ovčí	k2eAgFnSc2d1	ovčí
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
tkána	tkát	k5eAaImNgFnS	tkát
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
diagonální	diagonální	k2eAgFnSc4d1	diagonální
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
na	na	k7c6	na
látce	látka	k1gFnSc6	látka
ještě	ještě	k9	ještě
barevný	barevný	k2eAgInSc1d1	barevný
vzor	vzor	k1gInSc1	vzor
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
tkanina	tkanina	k1gFnSc1	tkanina
tartan	tartana	k1gFnPc2	tartana
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgInSc1d1	tradiční
irský	irský	k2eAgInSc1d1	irský
kilt	kilt	k1gInSc1	kilt
se	se	k3xPyFc4	se
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
z	z	k7c2	z
jednobarevné	jednobarevný	k2eAgFnSc2d1	jednobarevná
tkaniny	tkanina	k1gFnSc2	tkanina
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
šafránově	šafránově	k6eAd1	šafránově
nebo	nebo	k8xC	nebo
zeleně	zeleně	k6eAd1	zeleně
obarvené	obarvený	k2eAgInPc4d1	obarvený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kepr	kepr	k1gInSc1	kepr
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
tloušťce	tloušťka	k1gFnSc6	tloušťka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jaké	jaký	k3yIgNnSc4	jaký
použití	použití	k1gNnSc4	použití
je	být	k5eAaImIp3nS	být
finální	finální	k2eAgInSc1d1	finální
výrobek	výrobek	k1gInSc1	výrobek
určen	určit	k5eAaPmNgInS	určit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
aktivní	aktivní	k2eAgInSc4d1	aktivní
pohyb	pohyb	k1gInSc4	pohyb
(	(	kIx(	(
<g/>
tradiční	tradiční	k2eAgInPc4d1	tradiční
tance	tanec	k1gInPc4	tanec
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
tenčí	tenký	k2eAgInSc1d2	tenčí
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
než	než	k8xS	než
pro	pro	k7c4	pro
kilt	kilt	k1gInSc4	kilt
určený	určený	k2eAgInSc4d1	určený
pro	pro	k7c4	pro
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
chladném	chladný	k2eAgNnSc6d1	chladné
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzorek	vzorek	k1gInSc4	vzorek
===	===	k?	===
</s>
</p>
<p>
<s>
Nejcharakterističtějším	charakteristický	k2eAgInSc7d3	nejcharakterističtější
prvkem	prvek	k1gInSc7	prvek
skotského	skotský	k2eAgInSc2d1	skotský
kiltu	kilt	k1gInSc2	kilt
jsou	být	k5eAaImIp3nP	být
barevné	barevný	k2eAgInPc1d1	barevný
vzory	vzor	k1gInPc1	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
těchto	tento	k3xDgInPc2	tento
vzorů	vzor	k1gInPc2	vzor
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
určitými	určitý	k2eAgInPc7d1	určitý
klany	klan	k1gInPc7	klan
a	a	k8xC	a
rodinami	rodina	k1gFnPc7	rodina
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
neexistují	existovat	k5eNaImIp3nP	existovat
historické	historický	k2eAgInPc1d1	historický
podklady	podklad	k1gInPc1	podklad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
tartany	tartan	k1gInPc1	tartan
s	s	k7c7	s
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
vzorkem	vzorek	k1gInSc7	vzorek
pro	pro	k7c4	pro
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
hrabství	hrabství	k1gNnPc4	hrabství
<g/>
,	,	kIx,	,
podniky	podnik	k1gInPc1	podnik
<g/>
,	,	kIx,	,
školy	škola	k1gFnPc1	škola
a	a	k8xC	a
univerzity	univerzita	k1gFnPc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Vzorek	vzorek	k1gInSc1	vzorek
je	být	k5eAaImIp3nS	být
orientován	orientovat	k5eAaBmNgInS	orientovat
svisle	svisle	k6eAd1	svisle
a	a	k8xC	a
vodorovně	vodorovně	k6eAd1	vodorovně
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
ne	ne	k9	ne
úhlopříčně	úhlopříčně	k6eAd1	úhlopříčně
<g/>
.	.	kIx.	.
</s>
<s>
Vzorky	vzorek	k1gInPc1	vzorek
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgInP	označovat
podle	podle	k7c2	podle
souslednosti	souslednost	k1gFnSc2	souslednost
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
tartan	tartan	k1gInSc1	tartan
Wallace	Wallace	k1gFnSc2	Wallace
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
K	k	k7c3	k
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
R32	R32	k1gMnPc1	R32
K32	K32	k1gFnPc2	K32
Y	Y	kA	Y
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
K	K	kA	K
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
R	R	kA	R
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
Y	Y	kA	Y
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kód	kód	k1gInSc1	kód
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
černá	černat	k5eAaImIp3nS	černat
v	v	k7c6	v
šířce	šířka	k1gFnSc6	šířka
4	[number]	k4	4
jednotek	jednotka	k1gFnPc2	jednotka
je	být	k5eAaImIp3nS	být
následována	následovat	k5eAaImNgFnS	následovat
červenou	červená	k1gFnSc7	červená
v	v	k7c4	v
32	[number]	k4	32
jednotkách	jednotka	k1gFnPc6	jednotka
atd.	atd.	kA	atd.
Jednotka	jednotka	k1gFnSc1	jednotka
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
počet	počet	k1gInSc1	počet
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Vzory	vzor	k1gInPc1	vzor
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
charakterizovány	charakterizovat	k5eAaBmNgInP	charakterizovat
délkou	délka	k1gFnSc7	délka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
měří	měřit	k5eAaImIp3nS	měřit
celá	celý	k2eAgFnSc1d1	celá
jedna	jeden	k4xCgFnSc1	jeden
sekvence	sekvence	k1gFnSc1	sekvence
barevných	barevný	k2eAgNnPc2d1	barevné
polí	pole	k1gNnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
v	v	k7c6	v
centimetrech	centimetr	k1gInPc6	centimetr
(	(	kIx(	(
<g/>
palcích	palec	k1gInPc6	palec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzory	vzor	k1gInPc1	vzor
tartanu	tartan	k1gInSc2	tartan
bývaly	bývat	k5eAaImAgFnP	bývat
registrovány	registrován	k2eAgMnPc4d1	registrován
Skotským	skotský	k2eAgInSc7d1	skotský
úřadem	úřad	k1gInSc7	úřad
pro	pro	k7c4	pro
tartan	tartan	k1gInSc4	tartan
(	(	kIx(	(
<g/>
Scottish	Scottish	k1gInSc1	Scottish
Tartans	Tartansa	k1gFnPc2	Tartansa
Authority	Authorita	k1gFnSc2	Authorita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
uchovával	uchovávat	k5eAaImAgInS	uchovávat
vzorky	vzorek	k1gInPc4	vzorek
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
kód	kód	k1gInSc4	kód
označující	označující	k2eAgFnSc4d1	označující
charakteristiku	charakteristika	k1gFnSc4	charakteristika
barevného	barevný	k2eAgInSc2d1	barevný
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
registrace	registrace	k1gFnSc1	registrace
tartanů	tartan	k1gInPc2	tartan
přešla	přejít	k5eAaPmAgFnS	přejít
pod	pod	k7c4	pod
Skotský	skotský	k2eAgInSc4d1	skotský
národní	národní	k2eAgInSc4d1	národní
archiv	archiv	k1gInSc4	archiv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
registrováno	registrovat	k5eAaBmNgNnS	registrovat
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInPc2	tisíc
vzorů	vzor	k1gInPc2	vzor
tartanu	tartan	k1gInSc2	tartan
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
národní	národní	k2eAgInSc4d1	národní
tartan	tartan	k1gInSc4	tartan
(	(	kIx(	(
<g/>
Czech	Czech	k1gInSc1	Czech
National	National	k1gMnSc1	National
Tartan	tartan	k1gInSc1	tartan
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
registrován	registrovat	k5eAaBmNgInS	registrovat
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
registrují	registrovat	k5eAaBmIp3nP	registrovat
nové	nový	k2eAgInPc1d1	nový
vzory	vzor	k1gInPc1	vzor
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
jich	on	k3xPp3gInPc2	on
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
narůstalo	narůstat	k5eAaImAgNnS	narůstat
romantické	romantický	k2eAgNnSc1d1	romantické
vnímání	vnímání	k1gNnSc1	vnímání
Highlands	Highlandsa	k1gFnPc2	Highlandsa
a	a	k8xC	a
během	během	k7c2	během
viktoriánského	viktoriánský	k2eAgNnSc2d1	viktoriánské
období	období	k1gNnSc2	období
byla	být	k5eAaImAgFnS	být
skotská	skotský	k2eAgFnSc1d1	skotská
kultura	kultura	k1gFnSc1	kultura
silně	silně	k6eAd1	silně
poangličťována	poangličťován	k2eAgFnSc1d1	poangličťován
<g/>
.	.	kIx.	.
</s>
<s>
Reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
procesy	proces	k1gInPc4	proces
byla	být	k5eAaImAgFnS	být
registrace	registrace	k1gFnSc1	registrace
tartanů	tartan	k1gInPc2	tartan
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
klanovými	klanový	k2eAgNnPc7d1	klanové
jmény	jméno	k1gNnPc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
většinou	většinou	k6eAd1	většinou
nebyly	být	k5eNaImAgInP	být
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
vzory	vzor	k1gInPc1	vzor
s	s	k7c7	s
klany	klan	k1gInPc1	klan
spojovány	spojovat	k5eAaImNgInP	spojovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velikosti	velikost	k1gFnSc6	velikost
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
levnější	levný	k2eAgFnPc1d2	levnější
kilty	kilta	k1gFnPc1	kilta
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
ve	v	k7c6	v
standardních	standardní	k2eAgFnPc6d1	standardní
velikostech	velikost	k1gFnPc6	velikost
<g/>
,	,	kIx,	,
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
kilt	kilt	k1gInSc1	kilt
je	být	k5eAaImIp3nS	být
šitý	šitý	k2eAgInSc1d1	šitý
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
potřeba	potřeba	k6eAd1	potřeba
nejméně	málo	k6eAd3	málo
tři	tři	k4xCgInPc4	tři
rozměry	rozměr	k1gInPc4	rozměr
–	–	k?	–
pas	pas	k1gInSc4	pas
<g/>
,	,	kIx,	,
boky	boka	k1gFnPc1	boka
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
kiltu	kilt	k1gInSc2	kilt
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
kilt	kilt	k1gInSc1	kilt
objednáván	objednávat	k5eAaImNgInS	objednávat
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
,	,	kIx,	,
výrobci	výrobce	k1gMnPc1	výrobce
zájemcům	zájemce	k1gMnPc3	zájemce
obvykle	obvykle	k6eAd1	obvykle
zasílají	zasílat	k5eAaImIp3nP	zasílat
instrukce	instrukce	k1gFnPc4	instrukce
a	a	k8xC	a
schéma	schéma	k1gNnSc4	schéma
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
míry	míra	k1gFnSc2	míra
správně	správně	k6eAd1	správně
určit	určit	k5eAaPmF	určit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Northumbrijský	Northumbrijský	k2eAgInSc1d1	Northumbrijský
kilt	kilt	k1gInSc1	kilt
==	==	k?	==
</s>
</p>
<p>
<s>
Northumbrijský	Northumbrijský	k2eAgInSc1d1	Northumbrijský
kilt	kilt	k1gInSc1	kilt
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
shodný	shodný	k2eAgInSc1d1	shodný
se	s	k7c7	s
skotským	skotský	k2eAgNnSc7d1	skotské
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
z	z	k7c2	z
obyčejnějšího	obyčejný	k2eAgInSc2d2	obyčejnější
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
barevný	barevný	k2eAgInSc1d1	barevný
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
tkána	tkát	k5eAaImNgFnS	tkát
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pravý	pravý	k2eAgInSc4d1	pravý
tartan	tartan	k1gInSc4	tartan
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
jednobarevné	jednobarevný	k2eAgFnPc1d1	jednobarevná
kilty	kilta	k1gFnPc1	kilta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Irský	irský	k2eAgInSc1d1	irský
kilt	kilt	k1gInSc1	kilt
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
skotského	skotský	k2eAgInSc2d1	skotský
kiltu	kilt	k1gInSc2	kilt
<g/>
,	,	kIx,	,
irský	irský	k2eAgInSc1d1	irský
kilt	kilt	k1gInSc1	kilt
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
jednobarevný	jednobarevný	k2eAgInSc1d1	jednobarevný
<g/>
,	,	kIx,	,
obarvený	obarvený	k2eAgInSc1d1	obarvený
nejčastěji	často	k6eAd3	často
šafránovou	šafránový	k2eAgFnSc7d1	šafránová
nebo	nebo	k8xC	nebo
zelenou	zelený	k2eAgFnSc7d1	zelená
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
můžeme	moct	k5eAaImIp1nP	moct
často	často	k6eAd1	často
vidět	vidět	k5eAaImF	vidět
lidi	člověk	k1gMnPc4	člověk
mající	mající	k2eAgInSc4d1	mající
jednobarevný	jednobarevný	k2eAgInSc4d1	jednobarevný
kilt	kilt	k1gInSc4	kilt
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
na	na	k7c6	na
politických	politický	k2eAgFnPc6d1	politická
a	a	k8xC	a
hudebních	hudební	k2eAgNnPc6d1	hudební
shromážděních	shromáždění	k1gNnPc6	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
kilt	kilt	k1gInSc1	kilt
používal	používat	k5eAaImAgInS	používat
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
galského	galský	k2eAgNnSc2d1	galské
národního	národní	k2eAgNnSc2d1	národní
cítění	cítění	k1gNnSc2	cítění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
kilt	kilt	k5eAaPmF	kilt
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
vnímán	vnímat	k5eAaImNgInS	vnímat
spíše	spíše	k9	spíše
jako	jako	k8xS	jako
skotská	skotský	k2eAgFnSc1d1	skotská
specialita	specialita	k1gFnSc1	specialita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velšský	velšský	k2eAgInSc1d1	velšský
kilt	kilt	k1gInSc1	kilt
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
kilt	kilt	k1gInSc1	kilt
není	být	k5eNaImIp3nS	být
tradiční	tradiční	k2eAgFnSc7d1	tradiční
součástí	součást	k1gFnSc7	součást
velšského	velšský	k2eAgInSc2d1	velšský
oděvu	oděv	k1gInSc2	oděv
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
u	u	k7c2	u
Velšanů	Velšan	k1gMnPc2	Velšan
velmi	velmi	k6eAd1	velmi
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
národů	národ	k1gInPc2	národ
keltského	keltský	k2eAgInSc2d1	keltský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kiltem	kilt	k1gInSc7	kilt
a	a	k8xC	a
tartanem	tartan	k1gInSc7	tartan
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
setkáme	setkat	k5eAaPmIp1nP	setkat
téměř	téměř	k6eAd1	téměř
stejně	stejně	k6eAd1	stejně
často	často	k6eAd1	často
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
jako	jako	k8xC	jako
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
<g/>
,	,	kIx,	,
Cornwallu	Cornwall	k1gInSc6	Cornwall
<g/>
,	,	kIx,	,
Devonu	devon	k1gInSc6	devon
či	či	k8xC	či
Galicii	Galicie	k1gFnSc6	Galicie
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgInPc2d3	nejpopulárnější
tartanů	tartan	k1gInPc2	tartan
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
je	být	k5eAaImIp3nS	být
tartan	tartan	k1gInSc1	tartan
svatého	svatý	k2eAgMnSc2d1	svatý
Davida	David	k1gMnSc2	David
<g/>
,	,	kIx,	,
velšského	velšský	k2eAgMnSc2d1	velšský
národního	národní	k2eAgMnSc2d1	národní
patrona	patron	k1gMnSc2	patron
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
i	i	k8xC	i
individuální	individuální	k2eAgInPc1d1	individuální
tartany	tartan	k1gInPc1	tartan
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
minimum	minimum	k1gNnSc1	minimum
důkazů	důkaz	k1gInPc2	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
rodiny	rodina	k1gFnPc1	rodina
identifikovaly	identifikovat	k5eAaBmAgFnP	identifikovat
s	s	k7c7	s
konkrétním	konkrétní	k2eAgInSc7d1	konkrétní
tartanem	tartan	k1gInSc7	tartan
<g/>
.	.	kIx.	.
</s>
<s>
Velšský	velšský	k2eAgInSc1d1	velšský
národní	národní	k2eAgInSc1d1	národní
tartan	tartan	k1gInSc1	tartan
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
D.	D.	kA	D.
M.	M.	kA	M.
Richards	Richardsa	k1gFnPc2	Richardsa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
demonstroval	demonstrovat	k5eAaBmAgInS	demonstrovat
spojení	spojení	k1gNnSc3	spojení
Velšanů	Velšan	k1gMnPc2	Velšan
s	s	k7c7	s
keltským	keltský	k2eAgInSc7d1	keltský
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tartan	tartan	k1gInSc1	tartan
používá	používat	k5eAaImIp3nS	používat
barvy	barva	k1gFnPc4	barva
velšské	velšský	k2eAgFnSc2d1	velšská
národní	národní	k2eAgFnSc2d1	národní
vlajky	vlajka	k1gFnSc2	vlajka
–	–	k?	–
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
,	,	kIx,	,
červenou	červený	k2eAgFnSc4d1	červená
a	a	k8xC	a
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Quilt	Quilt	k1gMnSc1	Quilt
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kilt	Kilta	k1gFnPc2	Kilta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
tradicích	tradice	k1gFnPc6	tradice
a	a	k8xC	a
současnosti	současnost	k1gFnSc6	současnost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
akcí	akce	k1gFnPc2	akce
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
</s>
</p>
