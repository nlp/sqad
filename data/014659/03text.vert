<s>
Bukovské	Bukovské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Bukové	bukový	k2eAgFnPc1d1
hory	hora	k1gFnPc1
(	(	kIx(
<g/>
rezervace	rezervace	k1gFnPc1
ve	v	k7c6
Vizovických	vizovický	k2eAgInPc6d1
vrších	vrch	k1gInPc6
<g/>
)	)	kIx)
nebo	nebo	k8xC
Bukové	bukový	k2eAgFnPc1d1
hory	hora	k1gFnPc1
(	(	kIx(
<g/>
pohoří	pohořet	k5eAaPmIp3nP
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Bukovské	Bukovské	k2eAgFnSc1d1
vrchyBieszczadyВ	vrchyBieszczadyВ	k?
х	х	k?
Polská	polský	k2eAgFnSc1d1
část	část	k1gFnSc1
Bieszczad	Bieszczad	k1gInSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
1408	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
(	(	kIx(
<g/>
Pikuj	Pikuj	k1gMnSc1
<g/>
)	)	kIx)
Délka	délka	k1gFnSc1
</s>
<s>
100	#num#	k4
km	km	kA
</s>
<s>
Nadřazená	nadřazený	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Poloniny	polonina	k1gFnPc1
Sousedníjednotky	Sousedníjednotka	k1gFnSc2
</s>
<s>
Beskydské	beskydský	k2eAgFnPc1d1
predhorie	predhorie	k1gFnPc1
<g/>
,	,	kIx,
Laborecká	Laborecký	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
,	,	kIx,
Góry	Gór	k2eAgFnPc1d1
Sanocko-Turczańskie	Sanocko-Turczańskie	k1gFnPc1
<g/>
,	,	kIx,
Verchn	Verchn	k1gInSc1
<g/>
'	'	kIx"
<g/>
odnistrovs	odnistrovs	k1gInSc1
<g/>
'	'	kIx"
<g/>
ki	ki	k?
Beskydy	Beskydy	k1gFnPc1
<g/>
,	,	kIx,
Skolivs	Skolivs	k1gInSc1
<g/>
'	'	kIx"
<g/>
ki	ki	k?
Beskydy	Beskydy	k1gFnPc1
<g/>
,	,	kIx,
Horhany	Horhan	k1gInPc1
<g/>
,	,	kIx,
Polonyna	Polonyen	k2eAgFnSc1d1
Rivna	Rivna	k1gFnSc1
Podřazenéjednotky	Podřazenéjednotka	k1gFnSc2
</s>
<s>
Nastaz	Nastaz	k1gInSc1
<g/>
,	,	kIx,
Uličská	Uličský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
<g/>
,	,	kIx,
Bukovce	bukovka	k1gFnSc6
<g/>
,	,	kIx,
Ruská	ruský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
<g/>
,	,	kIx,
Runinská	Runinský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
<g/>
,	,	kIx,
Sedlická	sedlický	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
<g/>
,	,	kIx,
Połonyna	Połonyen	k2eAgFnSc1d1
Caryńska	Caryńska	k1gFnSc1
<g/>
,	,	kIx,
Połonyna	Połonyen	k2eAgFnSc1d1
Wetlińska	Wetlińska	k1gFnSc1
<g/>
,	,	kIx,
Połonyna	Połonyen	k2eAgFnSc1d1
Bukowska	Bukowska	k1gFnSc1
<g/>
,	,	kIx,
Połonyna	Połonyna	k1gFnSc1
Dźwiniacz	Dźwiniacza	k1gFnPc2
</s>
<s>
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
SlovenskoPolsko	SlovenskoPolsko	k1gNnSc4
PolskoUkrajina	PolskoUkrajin	k2eAgFnSc1d1
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Bukovské	Bukovské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
v	v	k7c6
rámci	rámec	k1gInSc6
Karpat	Karpaty	k1gInPc2
<g/>
,	,	kIx,
vyznačeny	vyznačit	k5eAaPmNgFnP
červeně	červeň	k1gFnPc1
</s>
<s>
Horniny	hornina	k1gFnPc1
</s>
<s>
flyš	flyš	k1gInSc1
<g/>
,	,	kIx,
břidlice	břidlice	k1gFnSc1
<g/>
,	,	kIx,
sopečný	sopečný	k2eAgInSc1d1
materiál	materiál	k1gInSc1
Povodí	povodí	k1gNnSc2
</s>
<s>
Cirocha	Ciroch	k1gMnSc4
<g/>
,	,	kIx,
San	San	k1gMnSc4
<g/>
,	,	kIx,
Osława	Osławus	k1gMnSc4
<g/>
,	,	kIx,
Wisłok	Wisłok	k1gInSc4
<g/>
,	,	kIx,
Už	už	k6eAd1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
22	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bukovské	Bukovské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
Bieszczady	Bieszczad	k1gInPc4
<g/>
,	,	kIx,
ukrajinsky	ukrajinsky	k6eAd1
В	В	k?
В	В	k?
х	х	k?
<g/>
,	,	kIx,
Verchovynskyj	Verchovynskyj	k1gMnSc1
Vododilnyj	Vododilnyj	k1gMnSc1
hrebet	hrebet	k1gMnSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
pohoří	pohoří	k1gNnSc4
a	a	k8xC
geomorfologický	geomorfologický	k2eAgInSc4d1
celek	celek	k1gInSc4
ve	v	k7c6
východokarpatské	východokarpatský	k2eAgFnSc6d1
geomorfologické	geomorfologický	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Poloniny	polonina	k1gFnSc2
v	v	k7c6
Beskydech	Beskyd	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
tří	tři	k4xCgInPc2
států	stát	k1gInPc2
<g/>
:	:	kIx,
Slovenska	Slovensko	k1gNnSc2
(	(	kIx(
<g/>
Prešovský	prešovský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Polska	Polsko	k1gNnSc2
(	(	kIx(
<g/>
Podkarpatské	podkarpatský	k2eAgNnSc1d1
vojvodství	vojvodství	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
Ukrajiny	Ukrajina	k1gFnSc2
(	(	kIx(
<g/>
Zakarpatská	zakarpatský	k2eAgFnSc1d1
a	a	k8xC
Lvovská	lvovský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
</s>
<s>
Pohoří	pohoří	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
severovýchodním	severovýchodní	k2eAgInSc6d1
cípu	cíp	k1gInSc6
Slovenska	Slovensko	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
jihovýchodním	jihovýchodní	k2eAgInSc6d1
cípu	cíp	k1gInSc6
Polska	Polsko	k1gNnSc2
a	a	k8xC
v	v	k7c6
jihozápadním	jihozápadní	k2eAgInSc6d1
cípu	cíp	k1gInSc6
Ukrajiny	Ukrajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jediná	jediný	k2eAgFnSc1d1
část	část	k1gFnSc1
Polonin	polonina	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zasahuje	zasahovat	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západě	západ	k1gInSc6
a	a	k8xC
jihozápadě	jihozápad	k1gInSc6
hraničí	hraničit	k5eAaImIp3nP
s	s	k7c7
Nízkými	nízký	k2eAgInPc7d1
Beskydy	Beskyd	k1gInPc7
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
Laboreckou	Laborecký	k2eAgFnSc7d1
vrchovinou	vrchovina	k1gFnSc7
a	a	k8xC
Beskydským	beskydský	k2eAgNnSc7d1
podhůřím	podhůří	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
hraničním	hraniční	k2eAgInSc6d1
polsko-slovenském	polsko-slovenský	k2eAgInSc6d1
hřebeni	hřeben	k1gInSc6
se	se	k3xPyFc4
rozhraní	rozhraní	k1gNnSc1
mezi	mezi	k7c7
Bukovskými	Bukovská	k1gFnPc7
vrchy	vrch	k1gInPc4
a	a	k8xC
Laboreckou	Laborecký	k2eAgFnSc7d1
vrchovinou	vrchovina	k1gFnSc7
podle	podle	k7c2
různých	různý	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
považuje	považovat	k5eAaImIp3nS
buď	buď	k8xC
už	už	k6eAd1
pramenná	pramenný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
řeky	řeka	k1gFnSc2
Udavy	Udava	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
až	až	k9
Lupkovský	Lupkovský	k2eAgInSc4d1
průsmyk	průsmyk	k1gInSc4
dále	daleko	k6eAd2
na	na	k7c4
západ	západ	k1gInSc4
(	(	kIx(
<g/>
na	na	k7c6
polské	polský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
se	se	k3xPyFc4
Bukovské	Bukovské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
vymezují	vymezovat	k5eAaImIp3nP
právě	právě	k9
až	až	k9
po	po	k7c4
Lupkovský	Lupkovský	k2eAgInSc4d1
průsmyk	průsmyk	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc4
slovenské	slovenský	k2eAgFnSc2d1
části	část	k1gFnSc2
</s>
<s>
Oblast	oblast	k1gFnSc1
Bukovských	Bukovských	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c7
nejpozději	pozdě	k6eAd3
a	a	k8xC
nejméně	málo	k6eAd3
osídlené	osídlený	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
obyvatelstva	obyvatelstvo	k1gNnSc2
Bukovských	Bukovských	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
tvoří	tvořit	k5eAaImIp3nP
příslušníci	příslušník	k1gMnPc1
rusínské	rusínský	k2eAgFnSc2d1
menšiny	menšina	k1gFnSc2
–	–	k?
Rusíni	Rusín	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převážnou	převážný	k2eAgFnSc4d1
část	část	k1gFnSc4
v	v	k7c6
tomto	tento	k3xDgInSc6
regionu	region	k1gInSc6
představují	představovat	k5eAaImIp3nP
Rusíni	Rusín	k1gMnPc1
východnější	východní	k2eAgFnSc2d2
řečové	řečový	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
–	–	k?
Pujďáci	Pujďák	k1gMnPc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
Laborecké	Laborecký	k2eAgFnSc6d1
vrchovině	vrchovina	k1gFnSc6
dominují	dominovat	k5eAaImIp3nP
spíše	spíše	k9
Pijďaki	Pijďak	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Dominantní	dominantní	k2eAgNnSc1d1
vyznání	vyznání	k1gNnSc1
v	v	k7c6
regionu	region	k1gInSc6
jsou	být	k5eAaImIp3nP
řeckokatolické	řeckokatolický	k2eAgFnPc1d1
a	a	k8xC
pravoslavné	pravoslavný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Více	hodně	k6eAd2
než	než	k8xS
60	#num#	k4
%	%	kIx~
povrchu	povrch	k1gInSc2
Bukovských	Bukovských	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
pokrývají	pokrývat	k5eAaImIp3nP
lesy	les	k1gInPc1
<g/>
,	,	kIx,
zejména	zejména	k9
jedlo-bukové	jedlo-bukový	k2eAgNnSc1d1
a	a	k8xC
bukové	bukový	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
byly	být	k5eAaImAgInP
Bukovské	Bukovské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
zahrnuty	zahrnut	k2eAgInPc1d1
do	do	k7c2
celosvětové	celosvětový	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
biosférických	biosférický	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
pohoří	pohoří	k1gNnPc2
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Bieszczadského	Bieszczadský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
(	(	kIx(
<g/>
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
Poloniny	polonina	k1gFnSc2
(	(	kIx(
<g/>
Slovensko	Slovensko	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
Užanského	Užanský	k1gMnSc2
národního	národní	k2eAgInSc2d1
přírodního	přírodní	k2eAgInSc2d1
parku	park	k1gInSc2
(	(	kIx(
<g/>
У	У	k?
н	н	k?
п	п	k?
п	п	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Ukrajina	Ukrajina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Členění	členění	k1gNnSc1
</s>
<s>
Pohoří	pohoří	k1gNnSc1
se	se	k3xPyFc4
člení	členit	k5eAaImIp3nS
na	na	k7c4
podcelky	podcelka	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Bukovské	Bukovské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
</s>
<s>
Na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
na	na	k7c6
území	území	k1gNnSc6
okresů	okres	k1gInPc2
Medzilaborce	Medzilaborka	k1gFnSc3
a	a	k8xC
Snina	Snina	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Bukovce	bukovka	k1gFnSc3
(	(	kIx(
<g/>
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Kremenec	Kremenec	k1gInSc1
1221	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
</s>
<s>
Runinská	Runinský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
</s>
<s>
Sedlická	sedlický	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
</s>
<s>
Uličská	Uličský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
</s>
<s>
Nastaz	Nastaz	k1gInSc1
(	(	kIx(
<g/>
801	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Polské	polský	k2eAgFnPc1d1
Bieszczady	Bieszczada	k1gFnPc1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
Polska	Polsko	k1gNnSc2
<g/>
,	,	kIx,
Podkarpatské	podkarpatský	k2eAgNnSc1d1
vojvodství	vojvodství	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Połonyna	Połonyen	k2eAgFnSc1d1
Caryńska	Caryńska	k1gFnSc1
(	(	kIx(
<g/>
1297	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Połonyna	Połonyen	k2eAgFnSc1d1
Wetlińska	Wetlińska	k1gFnSc1
(	(	kIx(
<g/>
Roh	roh	k1gInSc1
1255	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Połonyna	Połonyen	k2eAgFnSc1d1
Bukowska	Bukowska	k1gFnSc1
(	(	kIx(
<g/>
Halicz	Halicz	k1gInSc1
1333	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Połonyna	Połonyna	k1gFnSc1
Dźwiniacz	Dźwiniacza	k1gFnPc2
(	(	kIx(
<g/>
Bukowe	Bukowe	k1gInSc1
Berdo	berdo	k1gNnSc1
1312	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Ukrajinské	ukrajinský	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
Ukrajiny	Ukrajina	k1gFnSc2
(	(	kIx(
<g/>
ukrajinsky	ukrajinsky	k6eAd1
В	В	k?
х	х	k?
<g/>
,	,	kIx,
Vododilnyj	Vododilnyj	k1gMnSc1
hrebet	hrebet	k1gMnSc1
<g/>
)	)	kIx)
v	v	k7c6
Zakarpatské	zakarpatský	k2eAgFnSc6d1
a	a	k8xC
Lvovské	lvovský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
:	:	kIx,
</s>
<s>
Chrebet	Chrebet	k1gMnSc1
Stynka	Stynka	k1gMnSc1
</s>
<s>
Bukivs	Bukivs	k1gInSc1
<g/>
'	'	kIx"
<g/>
ka	ka	k?
Polonyna	Polonyna	k1gFnSc1
(	(	kIx(
<g/>
Pikuj	Pikuj	k1gFnSc1
1408	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Velyka	Velyka	k1gFnSc1
Hranka	hranka	k1gFnSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgInPc4d3
vrcholy	vrchol	k1gInPc4
</s>
<s>
Pikuj	Pikovat	k5eAaImRp2nS,k5eAaPmRp2nS,k5eAaBmRp2nS
-	-	kIx~
1405	#num#	k4
m	m	kA
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
vrcholů	vrchol	k1gInPc2
v	v	k7c6
Bukovských	Bukovských	k2eAgInPc6d1
vrších	vrch	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Pikuj	Pikovat	k5eAaPmRp2nS,k5eAaImRp2nS,k5eAaBmRp2nS
(	(	kIx(
<g/>
1408	#num#	k4
m	m	kA
<g/>
,	,	kIx,
Ukrajina	Ukrajina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Tarnica	Tarnica	k1gFnSc1
(	(	kIx(
<g/>
1346	#num#	k4
m	m	kA
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Kremenec	Kremenec	k1gInSc1
(	(	kIx(
<g/>
1221	#num#	k4
m	m	kA
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Bieszczadská	Bieszczadský	k2eAgFnSc1d1
lesní	lesní	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
</s>
<s>
V	v	k7c6
Bieszczadech	Bieszczad	k1gInPc6
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
znovuobnovený	znovuobnovený	k2eAgInSc1d1
provoz	provoz	k1gInSc1
na	na	k7c6
bývalé	bývalý	k2eAgFnSc6d1
lesní	lesní	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
zvané	zvaný	k2eAgFnPc1d1
Bieszczadská	Bieszczadský	k2eAgFnSc1d1
lesní	lesní	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
(	(	kIx(
<g/>
Bieszczadzkiej	Bieszczadzkiej	k1gInSc1
kolejce	kolejka	k1gFnSc3
leśnej	leśnat	k5eAaPmRp2nS,k5eAaImRp2nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
budovat	budovat	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
1890	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Bieszczadská	Bieszczadský	k2eAgFnSc1d1
lesní	lesní	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
na	na	k7c4
karpaty	karpat	k1gMnPc4
<g/>
.	.	kIx.
<g/>
net	net	k?
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Geografický	geografický	k2eAgInSc1d1
místopisný	místopisný	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
445	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Dělení	dělení	k1gNnSc1
Karpat	Karpaty	k1gInPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bukovské	Bukovské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Přechod	přechod	k1gInSc1
bukovských	bukovský	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
v	v	k7c6
létě	léto	k1gNnSc6
na	na	k7c4
Horydoly	Horydola	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Přechod	přechod	k1gInSc1
bukovských	bukovský	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
a	a	k8xC
Bieszczad	Bieszczad	k1gInSc1
zimě	zima	k1gFnSc3
na	na	k7c4
Horydoly	Horydola	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Bieszczady	Bieszczada	k1gFnPc4
–	–	k?
fotografie	fotografia	k1gFnPc4
a	a	k8xC
informace	informace	k1gFnPc4
z	z	k7c2
polské	polský	k2eAgFnSc2d1
části	část	k1gFnSc2
hor	hora	k1gFnPc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
450608	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4731487-4	4731487-4	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
234794036	#num#	k4
</s>
