<s>
Vila	vila	k1gFnSc1	vila
Tugendhat	Tugendhat	k1gFnSc2	Tugendhat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
ojedinělým	ojedinělý	k2eAgInSc7d1	ojedinělý
funkcionalistickým	funkcionalistický	k2eAgInSc7d1	funkcionalistický
dílem	díl	k1gInSc7	díl
německého	německý	k2eAgMnSc2d1	německý
architekta	architekt	k1gMnSc2	architekt
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Miese	Mies	k1gMnSc2	Mies
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Rohe	Roh	k1gFnSc2	Roh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
návrh	návrh	k1gInSc1	návrh
stavby	stavba	k1gFnSc2	stavba
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
manželů	manžel	k1gMnPc2	manžel
Grety	Greta	k1gFnSc2	Greta
a	a	k8xC	a
Fritze	Fritze	k1gFnSc2	Fritze
Tugendhatových	Tugendhatův	k2eAgMnPc2d1	Tugendhatův
<g/>
.	.	kIx.	.
</s>
