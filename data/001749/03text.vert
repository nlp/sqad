<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Hrabal	Hrabal	k1gMnSc1	Hrabal
<g/>
,	,	kIx,	,
rozený	rozený	k2eAgMnSc1d1	rozený
Bohumil	Bohumil	k1gMnSc1	Bohumil
František	František	k1gMnSc1	František
Kilian	Kilian	k1gMnSc1	Kilian
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1914	[number]	k4	1914
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1997	[number]	k4	1997
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
a	a	k8xC	a
nejosobitějších	osobitý	k2eAgMnPc2d3	nejosobitější
spisovatelů	spisovatel	k1gMnPc2	spisovatel
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejpřekládanějším	překládaný	k2eAgMnSc7d3	nejpřekládanější
českým	český	k2eAgMnSc7d1	český
autorem	autor	k1gMnSc7	autor
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Stálý	stálý	k2eAgMnSc1d1	stálý
host	host	k1gMnSc1	host
a	a	k8xC	a
důležitý	důležitý	k2eAgMnSc1d1	důležitý
člen	člen	k1gMnSc1	člen
hospody	hospody	k?	hospody
U	u	k7c2	u
Zlatého	zlatý	k2eAgMnSc2d1	zlatý
Tygra	tygr	k1gMnSc2	tygr
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
tzv.	tzv.	kA	tzv.
Hrabalovy	Hrabalův	k2eAgFnSc2d1	Hrabalova
Sorbonny	Sorbonna	k1gFnSc2	Sorbonna
(	(	kIx(	(
<g/>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Hrabal	Hrabal	k1gMnSc1	Hrabal
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Tretera	Treter	k1gMnSc2	Treter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
prezident	prezident	k1gMnSc1	prezident
hospodské	hospodský	k2eAgFnSc2d1	hospodská
přístolní	přístolní	k2eAgFnSc2d1	přístolní
společnosti	společnost	k1gFnSc2	společnost
zvané	zvaný	k2eAgFnPc1d1	zvaná
Zlatá	zlatá	k1gFnSc1	zlatá
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně-Židenicích	Brně-Židenice	k1gFnPc6	Brně-Židenice
(	(	kIx(	(
<g/>
Balbínova	Balbínův	k2eAgFnSc1d1	Balbínova
ul	ul	kA	ul
<g/>
.	.	kIx.	.
47	[number]	k4	47
<g/>
,	,	kIx,	,
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
)	)	kIx)	)
svobodné	svobodný	k2eAgFnSc3d1	svobodná
matce	matka	k1gFnSc3	matka
Marii	Maria	k1gFnSc3	Maria
Kilianové	Kilianová	k1gFnSc3	Kilianová
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
a	a	k8xC	a
důstojníkovi	důstojník	k1gMnSc3	důstojník
rakouské	rakouský	k2eAgFnSc2d1	rakouská
armády	armáda	k1gFnSc2	armáda
Bohumilu	Bohumil	k1gMnSc3	Bohumil
Blechovi	Blecha	k1gMnSc3	Blecha
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1893	[number]	k4	1893
Jevišovice	Jevišovice	k1gFnSc1	Jevišovice
-	-	kIx~	-
1970	[number]	k4	1970
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
k	k	k7c3	k
otcovství	otcovství	k1gNnSc3	otcovství
nehlásil	hlásit	k5eNaImAgMnS	hlásit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
pokřtěn	pokřtěn	k2eAgMnSc1d1	pokřtěn
Bohumil	Bohumil	k1gMnSc1	Bohumil
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
žil	žít	k5eAaImAgInS	žít
u	u	k7c2	u
prarodičů	prarodič	k1gMnPc2	prarodič
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
pomocná	pomocný	k2eAgFnSc1d1	pomocná
účetní	účetní	k1gFnSc1	účetní
v	v	k7c6	v
městském	městský	k2eAgInSc6d1	městský
pivovaru	pivovar	k1gInSc6	pivovar
v	v	k7c6	v
Polné	Polná	k1gFnSc6	Polná
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
budoucím	budoucí	k2eAgMnSc7d1	budoucí
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgMnSc7d1	hlavní
účetním	účetní	k2eAgMnSc7d1	účetní
Františkem	František	k1gMnSc7	František
Hrabalem	Hrabal	k1gMnSc7	Hrabal
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1966	[number]	k4	1966
<g/>
;	;	kIx,	;
předobraz	předobraz	k1gInSc1	předobraz
literární	literární	k2eAgFnSc2d1	literární
postavy	postava	k1gFnSc2	postava
Francina	Francina	k1gMnSc1	Francina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterého	který	k3yRgMnSc4	který
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
v	v	k7c6	v
děkanském	děkanský	k2eAgInSc6d1	děkanský
chrámu	chrám	k1gInSc6	chrám
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Polné	Polné	k1gNnSc1	Polné
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
dal	dát	k5eAaPmAgMnS	dát
František	František	k1gMnSc1	František
Hrabal	Hrabal	k1gMnSc1	Hrabal
písemný	písemný	k2eAgInSc4d1	písemný
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
syn	syn	k1gMnSc1	syn
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
užívat	užívat	k5eAaImF	užívat
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
Hrabalův	Hrabalův	k2eAgMnSc1d1	Hrabalův
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
Břetislav	Břetislav	k1gMnSc1	Břetislav
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
věnovali	věnovat	k5eAaPmAgMnP	věnovat
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
malý	malý	k2eAgMnSc1d1	malý
Bohumil	Bohumil	k1gMnSc1	Bohumil
<g/>
,	,	kIx,	,
když	když	k8xS	když
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1918	[number]	k4	1918
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
Jiráskově	Jiráskův	k2eAgFnSc6d1	Jiráskova
Vojnarce	Vojnarka	k1gFnSc6	Vojnarka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1919	[number]	k4	1919
se	se	k3xPyFc4	se
čtyřčlenná	čtyřčlenný	k2eAgFnSc1d1	čtyřčlenná
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Nymburka	Nymburk	k1gInSc2	Nymburk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
na	na	k7c6	na
reálce	reálka	k1gFnSc6	reálka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
s	s	k7c7	s
obtížemi	obtíž	k1gFnPc7	obtíž
dokončil	dokončit	k5eAaPmAgMnS	dokončit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
několikrát	několikrát	k6eAd1	několikrát
propadl	propadnout	k5eAaPmAgMnS	propadnout
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
Hrabal	Hrabal	k1gMnSc1	Hrabal
na	na	k7c6	na
právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
však	však	k9	však
rovněž	rovněž	k9	rovněž
přednášky	přednáška	k1gFnPc4	přednáška
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Vinou	vinou	k7c2	vinou
uzavření	uzavření	k1gNnSc2	uzavření
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
období	období	k1gNnSc6	období
okupace	okupace	k1gFnSc2	okupace
mohl	moct	k5eAaImAgMnS	moct
svá	svůj	k3xOyFgNnPc4	svůj
studia	studio	k1gNnPc4	studio
dokončit	dokončit	k5eAaPmF	dokončit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
JUDr.	JUDr.	kA	JUDr.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
železniční	železniční	k2eAgMnSc1d1	železniční
dělník	dělník	k1gMnSc1	dělník
a	a	k8xC	a
výpravčí	výpravčí	k1gMnSc1	výpravčí
v	v	k7c6	v
Kostomlatech	Kostomle	k1gNnPc6	Kostomle
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
literární	literární	k2eAgFnSc6d1	literární
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
i	i	k9	i
profese	profes	k1gFnSc2	profes
jako	jako	k8xC	jako
pojišťovací	pojišťovací	k2eAgMnSc1d1	pojišťovací
agent	agent	k1gMnSc1	agent
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgMnSc1d1	obchodní
cestující	cestující	k1gMnSc1	cestující
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
brigádník	brigádník	k1gMnSc1	brigádník
v	v	k7c6	v
kladenských	kladenský	k2eAgFnPc6d1	kladenská
ocelárnách	ocelárna	k1gFnPc6	ocelárna
a	a	k8xC	a
po	po	k7c6	po
těžkém	těžký	k2eAgInSc6d1	těžký
úrazu	úraz	k1gInSc6	úraz
pracoval	pracovat	k5eAaImAgInS	pracovat
ve	v	k7c6	v
sběrných	sběrný	k2eAgFnPc6d1	sběrná
surovinách	surovina	k1gFnPc6	surovina
ve	v	k7c6	v
Spálené	spálený	k2eAgFnSc6d1	spálená
ulici	ulice	k1gFnSc6	ulice
č.	č.	k?	č.
10	[number]	k4	10
jako	jako	k8xC	jako
balič	balič	k1gMnSc1	balič
starého	starý	k2eAgInSc2d1	starý
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
kulisák	kulisák	k1gMnSc1	kulisák
<g/>
.	.	kIx.	.
</s>
<s>
Nemalou	malý	k2eNgFnSc4d1	nemalá
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Libni	Libeň	k1gFnSc6	Libeň
-	-	kIx~	-
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
Na	na	k7c6	na
hrázi	hráz	k1gFnSc6	hráz
326	[number]	k4	326
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
byl	být	k5eAaImAgInS	být
zbourán	zbourán	k2eAgMnSc1d1	zbourán
<g/>
,	,	kIx,	,
malířka	malířka	k1gFnSc1	malířka
Svatošová	Svatošová	k1gFnSc1	Svatošová
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
(	(	kIx(	(
<g/>
u	u	k7c2	u
východu	východ	k1gInSc2	východ
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
Palmovka	Palmovka	k1gFnSc1	Palmovka
<g/>
)	)	kIx)	)
ovšem	ovšem	k9	ovšem
vyzdobila	vyzdobit	k5eAaPmAgFnS	vyzdobit
stěnu	stěna	k1gFnSc4	stěna
na	na	k7c4	na
hrabalovské	hrabalovský	k2eAgNnSc4d1	hrabalovské
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
Elišku	Eliška	k1gFnSc4	Eliška
Plevovou	plevový	k2eAgFnSc4d1	plevová
na	na	k7c6	na
zámečku	zámeček	k1gInSc6	zámeček
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
-	-	kIx~	-
<g/>
Libni	Libeň	k1gFnSc6	Libeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
si	se	k3xPyFc3	se
manželé	manžel	k1gMnPc1	manžel
Hrabalovi	Hrabalův	k2eAgMnPc1d1	Hrabalův
koupili	koupit	k5eAaPmAgMnP	koupit
v	v	k7c6	v
Kersku	Kersek	k1gInSc6	Kersek
u	u	k7c2	u
Nymburka	Nymburk	k1gInSc2	Nymburk
chatu	chata	k1gFnSc4	chata
če	če	k?	če
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
274	[number]	k4	274
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
se	se	k3xPyFc4	se
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
do	do	k7c2	do
Kobylis	Kobylisy	k1gInPc2	Kobylisy
<g/>
,	,	kIx,	,
jezdí	jezdit	k5eAaImIp3nP	jezdit
do	do	k7c2	do
Kerska	Kersk	k1gInSc2	Kersk
na	na	k7c4	na
víkendy	víkend	k1gInPc4	víkend
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatelem	spisovatel	k1gMnSc7	spisovatel
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
členem	člen	k1gInSc7	člen
Svazu	svaz	k1gInSc2	svaz
československých	československý	k2eAgMnPc2d1	československý
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
redakční	redakční	k2eAgFnSc2d1	redakční
rady	rada	k1gFnSc2	rada
Literárních	literární	k2eAgFnPc2d1	literární
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
nesměl	smět	k5eNaImAgMnS	smět
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
oficiálně	oficiálně	k6eAd1	oficiálně
publikovat	publikovat	k5eAaBmF	publikovat
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgInS	psát
tedy	tedy	k9	tedy
do	do	k7c2	do
samizdatových	samizdatový	k2eAgNnPc2d1	samizdatové
a	a	k8xC	a
exilových	exilový	k2eAgNnPc2d1	exilové
periodik	periodikum	k1gNnPc2	periodikum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
Libně	Libeň	k1gFnSc2	Libeň
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc2	ulice
Na	na	k7c6	na
Hrázi	hráz	k1gFnSc6	hráz
326	[number]	k4	326
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
odstraněn	odstranit	k5eAaPmNgInS	odstranit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
na	na	k7c6	na
chodníku	chodník	k1gInSc6	chodník
<g/>
)	)	kIx)	)
do	do	k7c2	do
družstevního	družstevní	k2eAgInSc2d1	družstevní
bytu	byt	k1gInSc2	byt
v	v	k7c6	v
panelovém	panelový	k2eAgInSc6d1	panelový
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Kobylisích	Kobylisy	k1gInPc6	Kobylisy
na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
Sokolniky	Sokolnik	k1gInPc4	Sokolnik
(	(	kIx(	(
<g/>
Koštálkova	Koštálkův	k2eAgFnSc1d1	Koštálkův
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Jodasova	Jodasův	k2eAgFnSc1d1	Jodasův
<g/>
,	,	kIx,	,
1105	[number]	k4	1105
<g/>
/	/	kIx~	/
<g/>
37	[number]	k4	37
v	v	k7c4	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Tvorba	tvorba	k1gFnSc1	tvorba
krátké	krátká	k1gFnSc2	krátká
sebekritické	sebekritický	k2eAgNnSc4d1	sebekritické
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
částečně	částečně	k6eAd1	částečně
a	a	k8xC	a
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
cenzury	cenzura	k1gFnSc2	cenzura
opět	opět	k6eAd1	opět
umožněno	umožněn	k2eAgNnSc1d1	umožněno
publikovat	publikovat	k5eAaBmF	publikovat
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
vycházela	vycházet	k5eAaImAgFnS	vycházet
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Pražská	pražský	k2eAgFnSc1d1	Pražská
imaginace	imaginace	k1gFnSc1	imaginace
<g/>
,	,	kIx,	,
vydávajícím	vydávající	k2eAgNnSc7d1	vydávající
Hrabala	hrabat	k5eAaImAgFnS	hrabat
samizdatem	samizdat	k1gInSc7	samizdat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
vydalo	vydat	k5eAaPmAgNnS	vydat
pod	pod	k7c7	pod
editorským	editorský	k2eAgInSc7d1	editorský
dohledem	dohled	k1gInSc7	dohled
Václava	Václav	k1gMnSc2	Václav
Kadlece	Kadlec	k1gMnSc2	Kadlec
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
1997	[number]	k4	1997
Sebrané	sebraný	k2eAgInPc4d1	sebraný
spisy	spis	k1gInPc4	spis
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hrabala	Hrabal	k1gMnSc2	Hrabal
v	v	k7c6	v
19	[number]	k4	19
svazcích	svazek	k1gInPc6	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Hrabal	Hrabal	k1gMnSc1	Hrabal
se	se	k3xPyFc4	se
znal	znát	k5eAaImAgMnS	znát
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Kolářem	Kolář	k1gMnSc7	Kolář
<g/>
,	,	kIx,	,
obdivoval	obdivovat	k5eAaImAgInS	obdivovat
svérázného	svérázný	k2eAgMnSc4d1	svérázný
a	a	k8xC	a
předčasně	předčasně	k6eAd1	předčasně
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
malíře	malíř	k1gMnSc2	malíř
<g/>
,	,	kIx,	,
grafika	grafik	k1gMnSc2	grafik
a	a	k8xC	a
průmyslového	průmyslový	k2eAgMnSc2d1	průmyslový
výtvarníka	výtvarník	k1gMnSc2	výtvarník
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Boudníka	Boudník	k1gMnSc2	Boudník
a	a	k8xC	a
přátelil	přátelit	k5eAaImAgMnS	přátelit
se	se	k3xPyFc4	se
s	s	k7c7	s
filosofem	filosof	k1gMnSc7	filosof
Egonem	Egon	k1gMnSc7	Egon
Bondym	Bondym	k1gInSc4	Bondym
<g/>
.	.	kIx.	.
</s>
<s>
Hrabalovy	Hrabalův	k2eAgFnPc1d1	Hrabalova
knihy	kniha	k1gFnPc1	kniha
byly	být	k5eAaImAgFnP	být
mnohokráte	mnohokráte	k6eAd1	mnohokráte
úspěšně	úspěšně	k6eAd1	úspěšně
zfilmovány	zfilmován	k2eAgFnPc4d1	zfilmována
a	a	k8xC	a
obdržel	obdržet	k5eAaPmAgMnS	obdržet
také	také	k9	také
spoustu	spousta	k1gFnSc4	spousta
nakladatelských	nakladatelský	k2eAgFnPc2d1	nakladatelská
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
Vaculíkem	Vaculík	k1gMnSc7	Vaculík
<g/>
,	,	kIx,	,
Karlem	Karel	k1gMnSc7	Karel
Peckou	Pecka	k1gMnSc7	Pecka
<g/>
,	,	kIx,	,
Janem	Jan	k1gMnSc7	Jan
Kameníčkem	kameníček	k1gInSc7	kameníček
<g/>
,	,	kIx,	,
Libuší	Libuše	k1gFnSc7	Libuše
Moníkovou	moníkův	k2eAgFnSc7d1	moníkův
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
tvůrci	tvůrce	k1gMnPc7	tvůrce
je	být	k5eAaImIp3nS	být
Hrabal	hrabat	k5eAaImAgMnS	hrabat
-	-	kIx~	-
především	především	k9	především
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
Příliš	příliš	k6eAd1	příliš
hlučnou	hlučný	k2eAgFnSc4d1	hlučná
samotu	samota	k1gFnSc4	samota
-	-	kIx~	-
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
v	v	k7c6	v
dílčí	dílčí	k2eAgFnSc6d1	dílčí
míře	míra	k1gFnSc6	míra
pokračovatele	pokračovatel	k1gMnSc2	pokračovatel
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
hospodu	hospodu	k?	hospodu
U	u	k7c2	u
Zlatého	zlatý	k2eAgMnSc2d1	zlatý
tygra	tygr	k1gMnSc2	tygr
(	(	kIx(	(
<g/>
Husova	Husův	k2eAgFnSc1d1	Husova
ulice	ulice	k1gFnSc1	ulice
228	[number]	k4	228
<g/>
/	/	kIx~	/
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
-	-	kIx~	-
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
setkal	setkat	k5eAaPmAgInS	setkat
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Havlem	Havel	k1gMnSc7	Havel
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
Billem	Bill	k1gMnSc7	Bill
Clintonem	Clinton	k1gMnSc7	Clinton
(	(	kIx(	(
<g/>
*	*	kIx~	*
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
a	a	k8xC	a
Madeleine	Madeleine	k1gFnSc1	Madeleine
Albrightovou	Albrightová	k1gFnSc7	Albrightová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrabal	Hrabal	k1gMnSc1	Hrabal
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
v	v	k7c6	v
pátém	pátý	k4xOgNnSc6	pátý
patře	patro	k1gNnSc6	patro
Ortopedické	ortopedický	k2eAgFnSc2d1	ortopedická
kliniky	klinika	k1gFnSc2	klinika
Nemocnice	nemocnice	k1gFnSc2	nemocnice
Na	na	k7c6	na
Bulovce	Bulovka	k1gFnSc6	Bulovka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
léčil	léčit	k5eAaImAgMnS	léčit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
rodinném	rodinný	k2eAgInSc6d1	rodinný
hrobě	hrob	k1gInSc6	hrob
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Hradištku	Hradištek	k1gInSc6	Hradištek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
hrobě	hrob	k1gInSc6	hrob
byli	být	k5eAaImAgMnP	být
pohřbeni	pohřben	k2eAgMnPc1d1	pohřben
také	také	k9	také
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
"	"	kIx"	"
<g/>
Maryška	Maryška	k1gFnSc1	Maryška
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nevlastní	vlastnit	k5eNaImIp3nS	vlastnit
otec	otec	k1gMnSc1	otec
"	"	kIx"	"
<g/>
Francin	Francin	k1gMnSc1	Francin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
strýc	strýc	k1gMnSc1	strýc
"	"	kIx"	"
<g/>
Pepin	Pepina	k1gFnPc2	Pepina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
žena	žena	k1gFnSc1	žena
"	"	kIx"	"
<g/>
Pipsi	Pipse	k1gFnSc4	Pipse
<g/>
"	"	kIx"	"
a	a	k8xC	a
bratr	bratr	k1gMnSc1	bratr
"	"	kIx"	"
<g/>
Slávek	Slávek	k1gMnSc1	Slávek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
těžké	těžký	k2eAgFnSc6d1	těžká
dubové	dubový	k2eAgFnSc6d1	dubová
rakvi	rakev	k1gFnSc6	rakev
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
PIVOVAR	pivovar	k1gInSc1	pivovar
POLNÁ	POLNÁ	kA	POLNÁ
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
oceněn	ocenit	k5eAaPmNgInS	ocenit
<g/>
,	,	kIx,	,
za	za	k7c4	za
román	román	k1gInSc4	román
Příliš	příliš	k6eAd1	příliš
hlučná	hlučný	k2eAgFnSc1d1	hlučná
samota	samota	k1gFnSc1	samota
získal	získat	k5eAaPmAgMnS	získat
italskou	italský	k2eAgFnSc4d1	italská
literární	literární	k2eAgFnSc4d1	literární
cenu	cena	k1gFnSc4	cena
Premio	Premio	k1gMnSc1	Premio
Elba	Elba	k1gMnSc1	Elba
-	-	kIx~	-
Raffaello	Raffaello	k1gNnSc1	Raffaello
Brignetti	Brignetť	k1gFnSc2	Brignetť
<g/>
,	,	kIx,	,
maďarskou	maďarský	k2eAgFnSc4d1	maďarská
cenu	cena	k1gFnSc4	cena
I.	I.	kA	I.
Bethlena	Bethlen	k2eAgFnSc1d1	Bethlena
<g/>
,	,	kIx,	,
za	za	k7c4	za
knihu	kniha	k1gFnSc4	kniha
Obsluhoval	obsluhovat	k5eAaImAgMnS	obsluhovat
jsem	být	k5eAaImIp1nS	být
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
Národní	národní	k2eAgFnSc4d1	národní
cenu	cena	k1gFnSc4	cena
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
francouzské	francouzský	k2eAgNnSc1d1	francouzské
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
Officier	Officier	k1gInSc1	Officier
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ordre	ordre	k1gInSc1	ordre
des	des	k1gNnSc2	des
arts	artsa	k1gFnPc2	artsa
et	et	k?	et
des	des	k1gNnSc1	des
lettres	lettres	k1gMnSc1	lettres
(	(	kIx(	(
<g/>
Rytíř	Rytíř	k1gMnSc1	Rytíř
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
písemnictví	písemnictví	k1gNnSc4	písemnictví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
anglické	anglický	k2eAgNnSc4d1	anglické
vydání	vydání	k1gNnSc4	vydání
Příliš	příliš	k6eAd1	příliš
hlučné	hlučný	k2eAgFnSc2d1	hlučná
samoty	samota	k1gFnSc2	samota
obdržel	obdržet	k5eAaPmAgMnS	obdržet
cenu	cena	k1gFnSc4	cena
Georgie	Georgie	k1gFnSc2	Georgie
Theinera	Theiner	k1gMnSc2	Theiner
a	a	k8xC	a
cenu	cena	k1gFnSc4	cena
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Seiferta	Seifert	k1gMnSc2	Seifert
za	za	k7c4	za
trilogii	trilogie	k1gFnSc4	trilogie
Svatby	svatba	k1gFnSc2	svatba
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
Vita	vit	k2eAgNnPc4d1	Vito
nuova	nuovo	k1gNnPc4	nuovo
<g/>
,	,	kIx,	,
Proluky	proluka	k1gFnPc4	proluka
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1968	[number]	k4	1968
obdržel	obdržet	k5eAaPmAgInS	obdržet
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Menzelem	Menzel	k1gMnSc7	Menzel
státní	státní	k2eAgFnSc4d1	státní
cenu	cena	k1gFnSc4	cena
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
za	za	k7c4	za
film	film	k1gInSc4	film
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
vlaky	vlak	k1gInPc1	vlak
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1989	[number]	k4	1989
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc4d1	udělen
titul	titul	k1gInSc4	titul
zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1996	[number]	k4	1996
jej	on	k3xPp3gInSc4	on
jmenovali	jmenovat	k5eAaImAgMnP	jmenovat
doktorem	doktor	k1gMnSc7	doktor
honoris	honoris	k1gFnSc2	honoris
causa	causa	k1gFnSc1	causa
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
získal	získat	k5eAaPmAgInS	získat
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
medaili	medaile	k1gFnSc4	medaile
"	"	kIx"	"
<g/>
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
je	být	k5eAaImIp3nS	být
jasný	jasný	k2eAgInSc1d1	jasný
prvek	prvek	k1gInSc1	prvek
autobiografičnosti	autobiografičnost	k1gFnSc2	autobiografičnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hojně	hojně	k6eAd1	hojně
čerpal	čerpat	k5eAaImAgInS	čerpat
z	z	k7c2	z
autentických	autentický	k2eAgFnPc2d1	autentická
životních	životní	k2eAgFnPc2d1	životní
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
s	s	k7c7	s
nevšední	všední	k2eNgFnSc7d1	nevšední
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
imaginací	imaginace	k1gFnSc7	imaginace
a	a	k8xC	a
fantazií	fantazie	k1gFnSc7	fantazie
transformoval	transformovat	k5eAaBmAgMnS	transformovat
do	do	k7c2	do
svébytné	svébytný	k2eAgFnSc2d1	svébytná
grotesknosti	grotesknost	k1gFnSc2	grotesknost
<g/>
,	,	kIx,	,
nadsázky	nadsázka	k1gFnSc2	nadsázka
a	a	k8xC	a
komična	komično	k1gNnSc2	komično
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
poznamenaných	poznamenaný	k2eAgInPc2d1	poznamenaný
i	i	k8xC	i
skepsí	skepse	k1gFnSc7	skepse
a	a	k8xC	a
černým	černý	k2eAgInSc7d1	černý
humorem	humor	k1gInSc7	humor
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
literární	literární	k2eAgNnPc4d1	literární
díla	dílo	k1gNnPc4	dílo
triviální	triviálnět	k5eAaImIp3nS	triviálnět
i	i	k9	i
vysoce	vysoce	k6eAd1	vysoce
kultivovaná	kultivovaný	k2eAgFnSc1d1	kultivovaná
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
nevyhýbal	vyhýbat	k5eNaImAgInS	vyhýbat
ani	ani	k8xC	ani
slangu	slang	k1gInSc2	slang
a	a	k8xC	a
poetismům	poetismus	k1gInPc3	poetismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdních	pozdní	k2eAgFnPc6d1	pozdní
prózách	próza	k1gFnPc6	próza
nechal	nechat	k5eAaPmAgMnS	nechat
zaznít	zaznít	k5eAaPmF	zaznít
i	i	k9	i
silné	silný	k2eAgInPc1d1	silný
melancholické	melancholický	k2eAgInPc1d1	melancholický
tóny	tón	k1gInPc1	tón
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
depresivní	depresivní	k2eAgFnSc1d1	depresivní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInSc1d1	populární
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
28	[number]	k4	28
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
vyšly	vyjít	k5eAaPmAgFnP	vyjít
(	(	kIx(	(
<g/>
jako	jako	k9	jako
příloha	příloha	k1gFnSc1	příloha
časopisu	časopis	k1gInSc2	časopis
Zprávy	zpráva	k1gFnSc2	zpráva
Spolku	spolek	k1gInSc2	spolek
českých	český	k2eAgMnPc2d1	český
bibliofilů	bibliofil	k1gMnPc2	bibliofil
<g/>
)	)	kIx)	)
Hovory	hovor	k1gInPc1	hovor
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Ztracená	ztracený	k2eAgFnSc1d1	ztracená
ulička	ulička	k1gFnSc1	ulička
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
Hovory	hovor	k1gInPc1	hovor
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
-	-	kIx~	-
dvě	dva	k4xCgFnPc1	dva
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ve	v	k7c6	v
Spolku	spolek	k1gInSc6	spolek
českých	český	k2eAgMnPc2d1	český
bibliofilů	bibliofil	k1gMnPc2	bibliofil
Skřivánek	Skřivánek	k1gMnSc1	Skřivánek
na	na	k7c6	na
niti	nit	k1gFnSc6	nit
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
(	(	kIx(	(
<g/>
připraveno	připravit	k5eAaPmNgNnS	připravit
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
neuskutečnilo	uskutečnit	k5eNaPmAgNnS	uskutečnit
<g/>
)	)	kIx)	)
Perlička	perlička	k1gFnSc1	perlička
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
Pábitelé	pábitel	k1gMnPc1	pábitel
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1964	[number]	k4	1964
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
;	;	kIx,	;
Pábiteli	pábitel	k1gMnSc3	pábitel
nazýval	nazývat	k5eAaImAgInS	nazývat
osobité	osobitý	k2eAgNnSc4d1	osobité
vypravěče	vypravěč	k1gMnPc4	vypravěč
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
způsobem	způsob	k1gInSc7	způsob
milující	milující	k2eAgInSc1d1	milující
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
navenek	navenek	k6eAd1	navenek
zdají	zdát	k5eAaImIp3nP	zdát
obhroublí	obhroublý	k2eAgMnPc1d1	obhroublý
<g/>
;	;	kIx,	;
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
vyprávění	vyprávění	k1gNnSc6	vyprávění
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
Hrabal	Hrabal	k1gMnSc1	Hrabal
nazýval	nazývat	k5eAaImAgMnS	nazývat
pábení	pábení	k1gNnSc4	pábení
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
prvky	prvek	k1gInPc4	prvek
poetismu	poetismus	k1gInSc2	poetismus
i	i	k8xC	i
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
;	;	kIx,	;
sám	sám	k3xTgMnSc1	sám
Hrabal	Hrabal	k1gMnSc1	Hrabal
byl	být	k5eAaImAgMnS	být
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
pábitel	pábitel	k1gMnSc1	pábitel
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
vlaky	vlak	k1gInPc1	vlak
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
-	-	kIx~	-
novela	novela	k1gFnSc1	novela
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
okupace	okupace	k1gFnSc2	okupace
<g/>
,	,	kIx,	,
zfilmováno	zfilmován	k2eAgNnSc4d1	zfilmováno
(	(	kIx(	(
<g/>
film	film	k1gInSc4	film
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
vlaky	vlak	k1gInPc1	vlak
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
oceněn	oceněn	k2eAgMnSc1d1	oceněn
Oscarem	Oscar	k1gMnSc7	Oscar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Taneční	taneční	k2eAgFnSc2d1	taneční
hodiny	hodina	k1gFnSc2	hodina
pro	pro	k7c4	pro
starší	starší	k1gMnPc4	starší
a	a	k8xC	a
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
-	-	kIx~	-
novela	novela	k1gFnSc1	novela
skládající	skládající	k2eAgFnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
věty	věta	k1gFnSc2	věta
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc2d1	literární
experiment	experiment	k1gInSc4	experiment
<g/>
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
nákladu	náklad	k1gInSc2	náklad
kolem	kolem	k6eAd1	kolem
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
výtisků	výtisk	k1gInPc2	výtisk
Inzerát	inzerát	k1gInSc4	inzerát
na	na	k7c4	na
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
už	už	k6eAd1	už
nechci	chtít	k5eNaImIp1nS	chtít
bydlet	bydlet	k5eAaImF	bydlet
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
–	–	k?	–
1969	[number]	k4	1969
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
Jiřím	Jiří	k1gMnSc7	Jiří
Menzelem	Menzel	k1gMnSc7	Menzel
jako	jako	k8xS	jako
Skřivánci	Skřivánek	k1gMnPc1	Skřivánek
na	na	k7c6	na
niti	nit	k1gFnSc6	nit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
okamžitě	okamžitě	k6eAd1	okamžitě
putovalo	putovat	k5eAaImAgNnS	putovat
"	"	kIx"	"
<g/>
do	do	k7c2	do
trezoru	trezor	k1gInSc2	trezor
<g/>
"	"	kIx"	"
Kopretina	kopretina	k1gFnSc1	kopretina
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
Automat	automat	k1gInSc1	automat
Svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1966	[number]	k4	1966
-	-	kIx~	-
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
již	již	k9	již
dříve	dříve	k6eAd2	dříve
vydaných	vydaný	k2eAgFnPc2d1	vydaná
povídek	povídka	k1gFnPc2	povídka
Bohumil	Bohumil	k1gMnSc1	Bohumil
Hrabal	Hrabal	k1gMnSc1	Hrabal
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
-	-	kIx~	-
antologie	antologie	k1gFnSc1	antologie
jeho	jeho	k3xOp3gMnPc2	jeho
oblíbených	oblíbený	k2eAgMnPc2d1	oblíbený
autorů	autor	k1gMnPc2	autor
Toto	tento	k3xDgNnSc1	tento
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
společné	společný	k2eAgFnSc6d1	společná
péči	péče	k1gFnSc6	péče
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
-	-	kIx~	-
textová	textový	k2eAgFnSc1d1	textová
koláž	koláž	k1gFnSc1	koláž
k	k	k7c3	k
fotografiím	fotografia	k1gFnPc3	fotografia
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Toto	tento	k3xDgNnSc4	tento
město	město	k1gNnSc4	město
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
společné	společný	k2eAgFnSc6d1	společná
péči	péče	k1gFnSc6	péče
nájemníků	nájemník	k1gMnPc2	nájemník
Morytáty	morytát	k1gInPc4	morytát
a	a	k8xC	a
legendy	legenda	k1gFnPc4	legenda
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
Domácí	domácí	k2eAgInPc1d1	domácí
úkoly	úkol	k1gInPc1	úkol
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
Poupata	poupě	k1gNnPc4	poupě
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
básně	báseň	k1gFnPc1	báseň
a	a	k8xC	a
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
náklad	náklad	k1gInSc1	náklad
byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
ve	v	k7c6	v
skladech	sklad	k1gInPc6	sklad
<g/>
.	.	kIx.	.
</s>
<s>
Něžný	něžný	k2eAgMnSc1d1	něžný
barbar	barbar	k1gMnSc1	barbar
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
trilogie	trilogie	k1gFnSc2	trilogie
Městečko	městečko	k1gNnSc1	městečko
u	u	k7c2	u
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
vzpomínková	vzpomínkový	k2eAgFnSc1d1	vzpomínková
próza	próza	k1gFnSc1	próza
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Postřižiny	postřižiny	k1gFnPc4	postřižiny
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
-	-	kIx~	-
vzpomínková	vzpomínkový	k2eAgFnSc1d1	vzpomínková
próza	próza	k1gFnSc1	próza
Krasosmutnění	krasosmutnění	k1gNnSc2	krasosmutnění
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
Harlekýnovy	harlekýnův	k2eAgFnPc1d1	harlekýnův
milióny	milión	k4xCgInPc7	milión
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
Bambini	Bambin	k2eAgMnPc1d1	Bambin
di	di	k?	di
Praga	Prag	k1gMnSc2	Prag
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
-	-	kIx~	-
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Andělské	andělský	k2eAgFnSc2d1	Andělská
oči	oko	k1gNnPc4	oko
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
režie	režie	k1gFnSc1	režie
Dušan	Dušan	k1gMnSc1	Dušan
Klein	Klein	k1gMnSc1	Klein
<g/>
)	)	kIx)	)
Městečko	městečko	k1gNnSc1	městečko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zastavil	zastavit	k5eAaPmAgInS	zastavit
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
-	-	kIx~	-
vzpomínková	vzpomínkový	k2eAgFnSc1d1	vzpomínková
próza	próza	k1gFnSc1	próza
Každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
zázrak	zázrak	k1gInSc1	zázrak
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
Slavnosti	slavnost	k1gFnSc2	slavnost
sněženek	sněženka	k1gFnPc2	sněženka
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
-	-	kIx~	-
povídky	povídka	k1gFnSc2	povídka
Obsluhoval	obsluhovat	k5eAaImAgMnS	obsluhovat
jsem	být	k5eAaImIp1nS	být
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1982	[number]	k4	1982
v	v	k7c6	v
Jazzové	jazzový	k2eAgFnSc6d1	jazzová
sekci	sekce	k1gFnSc6	sekce
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
až	až	k9	až
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
2006	[number]	k4	2006
Příliš	příliš	k6eAd1	příliš
hlučná	hlučný	k2eAgFnSc1d1	hlučná
samota	samota	k1gFnSc1	samota
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
Kluby	klub	k1gInPc1	klub
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
Něžný	něžný	k2eAgMnSc1d1	něžný
barbar	barbar	k1gMnSc1	barbar
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
až	až	k9	až
1991	[number]	k4	1991
Domácí	domácí	k2eAgInPc1d1	domácí
úkoly	úkol	k1gInPc1	úkol
z	z	k7c2	z
pilnosti	pilnost	k1gFnSc2	pilnost
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
Listování	listování	k1gNnSc2	listování
ve	v	k7c6	v
stínech	stín	k1gInPc6	stín
grafických	grafický	k2eAgMnPc2d1	grafický
<g />
.	.	kIx.	.
</s>
<s>
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
Domácí	domácí	k2eAgInPc1d1	domácí
úkoly	úkol	k1gInPc1	úkol
z	z	k7c2	z
poetiky	poetika	k1gFnSc2	poetika
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
trilogie	trilogie	k1gFnPc1	trilogie
<g/>
:	:	kIx,	:
Vita	vit	k2eAgFnSc1d1	Vita
nuova	nuova	k1gFnSc1	nuova
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
vyprávění	vyprávění	k1gNnSc1	vyprávění
o	o	k7c6	o
Hrabalovi	Hrabal	k1gMnSc6	Hrabal
samém	samý	k3xTgMnSc6	samý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc3	jeho
ženě	žena	k1gFnSc3	žena
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
přátelích	přítel	k1gMnPc6	přítel
z	z	k7c2	z
období	období	k1gNnSc2	období
života	život	k1gInSc2	život
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
-	-	kIx~	-
očima	oko	k1gNnPc7	oko
jeho	jeho	k3xOp3gNnSc1	jeho
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
psáno	psán	k2eAgNnSc1d1	psáno
ich-formou	ichormý	k2eAgFnSc7d1	ich-formý
v	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
rodě	rod	k1gInSc6	rod
Proluky	proluka	k1gFnSc2	proluka
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
až	až	k8xS	až
v	v	k7c6	v
r.	r.	kA	r.
1991	[number]	k4	1991
Svatby	svatba	k1gFnSc2	svatba
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
až	až	k9	až
1991	[number]	k4	1991
-	-	kIx~	-
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
próza	próza	k1gFnSc1	próza
Život	život	k1gInSc1	život
bez	bez	k7c2	bez
smokingu	smoking	k1gInSc2	smoking
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
Chcete	chtít	k5eAaImIp2nP	chtít
vidět	vidět	k5eAaImF	vidět
zlatou	zlatý	k2eAgFnSc4d1	zlatá
Prahu	Praha	k1gFnSc4	Praha
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
-	-	kIx~	-
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
editor	editor	k1gInSc1	editor
Jaromír	Jaromír	k1gMnSc1	Jaromír
Pelc	Pelc	k1gMnSc1	Pelc
Kličky	klička	k1gFnSc2	klička
na	na	k7c6	na
kapesníku	kapesník	k1gInSc6	kapesník
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
Můj	můj	k3xOp1gInSc1	můj
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
Československy	československy	k6eAd1	československy
spisovatel	spisovatel	k1gMnSc1	spisovatel
1988	[number]	k4	1988
Tři	tři	k4xCgFnPc4	tři
novely	novela	k1gFnPc4	novela
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
Obsluhoval	obsluhovat	k5eAaImAgMnS	obsluhovat
jsem	být	k5eAaImIp1nS	být
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Dopisy	dopis	k1gInPc1	dopis
Dubence	Dubence	k1gFnSc2	Dubence
(	(	kIx(	(
<g/>
tetralogie	tetralogie	k1gFnSc1	tetralogie
<g/>
)	)	kIx)	)
-	-	kIx~	-
proud	proud	k1gInSc1	proud
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
propojený	propojený	k2eAgMnSc1d1	propojený
filosofickými	filosofický	k2eAgFnPc7d1	filosofická
úvahami	úvaha	k1gFnPc7	úvaha
a	a	k8xC	a
politickými	politický	k2eAgInPc7d1	politický
komentáři	komentář	k1gInPc7	komentář
Barvotisky	barvotisk	k1gInPc1	barvotisk
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
Krásná	krásný	k2eAgFnSc1d1	krásná
Poldi	Poldi	k1gFnSc1	Poldi
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
Listopadový	listopadový	k2eAgInSc1d1	listopadový
uragán	uragán	k1gInSc1	uragán
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
Schizofrenické	schizofrenický	k2eAgNnSc4d1	schizofrenické
evangelium	evangelium	k1gNnSc4	evangelium
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
Kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
flétna	flétna	k1gFnSc1	flétna
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Ponorné	ponorný	k2eAgFnSc2d1	ponorná
říčky	říčka	k1gFnSc2	říčka
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1991	[number]	k4	1991
Růžový	růžový	k2eAgMnSc1d1	růžový
kavalír	kavalír	k1gMnSc1	kavalír
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
Aurora	Aurora	k1gFnSc1	Aurora
na	na	k7c6	na
mělčině	mělčina	k1gFnSc6	mělčina
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
Večerníčky	večerníček	k1gInPc1	večerníček
pro	pro	k7c4	pro
Cassia	Cassium	k1gNnPc4	Cassium
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
Texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
-	-	kIx~	-
úvahy	úvaha	k1gFnSc2	úvaha
Annons	Annons	k1gInSc1	Annons
om	om	k?	om
huset	huset	k1gInSc1	huset
där	där	k?	där
jag	jaga	k1gFnPc2	jaga
inte	intat	k5eAaPmIp3nS	intat
längre	längr	k1gInSc5	längr
vill	villit	k5eAaPmRp2nS	villit
bo	bo	k?	bo
(	(	kIx(	(
<g/>
Inzerát	inzerát	k1gInSc4	inzerát
na	na	k7c4	na
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
už	už	k6eAd1	už
nechci	chtít	k5eNaImIp1nS	chtít
bydlet	bydlet	k5eAaImF	bydlet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g />
.	.	kIx.	.
</s>
<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
,	,	kIx,	,
Bonnier	Bonnier	k1gInSc1	Bonnier
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Gunilla	Gunilla	k1gMnSc1	Gunilla
Nilsson	Nilsson	k1gMnSc1	Nilsson
<g/>
,	,	kIx,	,
Mimi	Mimi	k1gFnSc1	Mimi
Rönnow	Rönnow	k1gMnSc1	Rönnow
<g/>
,	,	kIx,	,
Göran	Göran	k1gMnSc1	Göran
Skogmar	Skogmar	k1gMnSc1	Skogmar
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Radlová-Jensen	Radlová-Jensna	k1gFnPc2	Radlová-Jensna
Jag	Jaga	k1gFnPc2	Jaga
har	har	k?	har
betjänat	betjänat	k5eAaPmF	betjänat
kungen	kungen	k1gInSc4	kungen
av	av	k?	av
England	England	k1gInSc1	England
(	(	kIx(	(
<g/>
Obsluhoval	obsluhovat	k5eAaImAgInS	obsluhovat
jsem	být	k5eAaImIp1nS	být
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
,	,	kIx,	,
Askelin	Askelin	k2eAgInSc1d1	Askelin
&	&	k?	&
Hägglund	Hägglund	k1gInSc1	Hägglund
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
7684	[number]	k4	7684
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Karin	Karina	k1gFnPc2	Karina
Mossdal	Mossdal	k1gFnPc2	Mossdal
När	När	k1gFnSc2	När
seklet	seklet	k5eAaImF	seklet
var	var	k1gInSc1	var
kort	korta	k1gFnPc2	korta
(	(	kIx(	(
<g/>
Postřižiny	postřižiny	k1gFnPc1	postřižiny
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
Lund	Lunda	k1gFnPc2	Lunda
<g/>
,	,	kIx,	,
Pegas	Pegas	k1gMnSc1	Pegas
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
7784	[number]	k4	7784
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Mats	Matsa	k1gFnPc2	Matsa
Larsson	Larsson	k1gMnSc1	Larsson
Lå	Lå	k1gMnSc1	Lå
tå	tå	k?	tå
gå	gå	k?	gå
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
vlaky	vlak	k1gInPc1	vlak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
Lund	Lunda	k1gFnPc2	Lunda
<g/>
,	,	kIx,	,
Pegas	Pegas	k1gMnSc1	Pegas
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
7784	[number]	k4	7784
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
von	von	k1gInSc1	von
Hofsten	Hofsten	k2eAgInSc4d1	Hofsten
Novemberorkanen	Novemberorkanen	k1gInSc4	Novemberorkanen
(	(	kIx(	(
<g/>
Listopadový	listopadový	k2eAgInSc4d1	listopadový
uragán	uragán	k1gInSc4	uragán
+	+	kIx~	+
Živoucí	živoucí	k2eAgInPc4d1	živoucí
řetězy	řetěz	k1gInPc4	řetěz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1990	[number]	k4	1990
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
,	,	kIx,	,
Charta	charta	k1gFnSc1	charta
77	[number]	k4	77
<g/>
-stiftelsen	tiftelsna	k1gFnPc2	-stiftelsna
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Mats	Matsa	k1gFnPc2	Matsa
Larsson	Larsson	k1gMnSc1	Larsson
Danslektioner	Danslektioner	k1gMnSc1	Danslektioner
för	för	k?	för
äldre	äldr	k1gInSc5	äldr
och	och	k0	och
försigkomna	försigkomna	k6eAd1	försigkomna
(	(	kIx(	(
<g/>
Taneční	taneční	k2eAgFnPc4d1	taneční
hodiny	hodina	k1gFnPc4	hodina
pro	pro	k7c4	pro
starší	starší	k1gMnPc4	starší
a	a	k8xC	a
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
Lund	Lunda	k1gFnPc2	Lunda
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
Pictus	Pictus	k1gInSc1	Pictus
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
88283	[number]	k4	88283
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Mats	Mats	k1gInSc1	Mats
Larsson	Larsson	k1gInSc1	Larsson
Harlekins	Harlekinsa	k1gFnPc2	Harlekinsa
millioner	millionra	k1gFnPc2	millionra
(	(	kIx(	(
<g/>
Harlekýnovy	harlekýnův	k2eAgFnSc2d1	harlekýnův
milióny	milión	k4xCgInPc4	milión
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Östlings	Östlings	k1gInSc1	Östlings
Bokförlag	Bokförlaga	k1gFnPc2	Bokförlaga
Symposion	symposion	k1gNnSc1	symposion
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
Höör	Hööra	k1gFnPc2	Hööra
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
7139	[number]	k4	7139
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Mats	Matsa	k1gFnPc2	Matsa
Larsson	Larssona	k1gFnPc2	Larssona
Den	den	k1gInSc1	den
ljuva	ljuva	k6eAd1	ljuva
sorgen	sorgen	k1gInSc1	sorgen
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Krasosmutnění	krasosmutnění	k1gNnPc2	krasosmutnění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Höör	Hööra	k1gFnPc2	Hööra
<g/>
,	,	kIx,	,
Östlings	Östlings	k1gInSc4	Östlings
Bokförlag	Bokförlaga	k1gFnPc2	Bokförlaga
Symposion	symposion	k1gNnSc1	symposion
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
7139	[number]	k4	7139
<g/>
-	-	kIx~	-
<g/>
752	[number]	k4	752
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Mats	Matsa	k1gFnPc2	Matsa
Larsson	Larsson	k1gMnSc1	Larsson
En	En	k1gMnSc1	En
alltför	alltför	k1gMnSc1	alltför
högljudd	högljudd	k1gMnSc1	högljudd
ensamhet	ensamhet	k1gMnSc1	ensamhet
(	(	kIx(	(
<g/>
Příliš	příliš	k6eAd1	příliš
hlučná	hlučný	k2eAgFnSc1d1	hlučná
samota	samota	k1gFnSc1	samota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Stockholm	Stockholm	k1gInSc1	Stockholm
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Ruin	ruina	k1gFnPc2	ruina
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
85191	[number]	k4	85191
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Karin	Karina	k1gFnPc2	Karina
Mossdal	Mossdal	k1gFnPc2	Mossdal
Breven	Breven	k2eAgInSc1d1	Breven
till	till	k1gInSc1	till
Dubenka	Dubenka	k1gFnSc1	Dubenka
(	(	kIx(	(
<g/>
Listopadový	listopadový	k2eAgInSc1d1	listopadový
uragán	uragán	k1gInSc1	uragán
+	+	kIx~	+
Ponorné	ponorný	k2eAgFnSc2d1	ponorná
říčky	říčka	k1gFnSc2	říčka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
,	,	kIx,	,
Ruin	ruina	k1gFnPc2	ruina
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
<g />
.	.	kIx.	.
</s>
<s>
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
85191	[number]	k4	85191
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Mats	Matsa	k1gFnPc2	Matsa
Larsson	Larssona	k1gFnPc2	Larssona
Den	den	k1gInSc1	den
lilla	lilla	k1gMnSc1	lilla
staden	stadna	k1gFnPc2	stadna
där	där	k?	där
tiden	tiden	k1gInSc1	tiden
stannade	stannást	k5eAaPmIp3nS	stannást
(	(	kIx(	(
<g/>
Městečko	městečko	k1gNnSc1	městečko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zastavil	zastavit	k5eAaPmAgInS	zastavit
čas	čas	k1gInSc1	čas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
,	,	kIx,	,
Aspekt	aspekt	k1gInSc1	aspekt
förlag	förlag	k1gInSc1	förlag
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
979100	[number]	k4	979100
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Mats	Matsa	k1gFnPc2	Matsa
Larsson	Larsson	k1gNnSc1	Larsson
Bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
veřejné	veřejný	k2eAgNnSc1d1	veřejné
prostranství	prostranství	k1gNnSc1	prostranství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
8	[number]	k4	8
na	na	k7c6	na
Palmovce	Palmovka	k1gFnSc6	Palmovka
-	-	kIx~	-
náměstí	náměstí	k1gNnSc1	náměstí
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hrabala	Hrabal	k1gMnSc2	Hrabal
a	a	k8xC	a
také	také	k9	také
nedaleká	daleký	k2eNgFnSc1d1	nedaleká
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hrabala	Hrabal	k1gMnSc2	Hrabal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
Hrabalovu	Hrabalův	k2eAgFnSc4d1	Hrabalova
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
ulice	ulice	k1gFnSc1	ulice
v	v	k7c6	v
Brně-Židenicích	Brně-Židenice	k1gFnPc6	Brně-Židenice
nedaleko	nedaleko	k7c2	nedaleko
Balbínovy	Balbínův	k2eAgFnSc2d1	Balbínova
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stále	stále	k6eAd1	stále
stojí	stát	k5eAaImIp3nS	stát
Hrabalův	Hrabalův	k2eAgInSc1d1	Hrabalův
rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nymburce	Nymburk	k1gInSc6	Nymburk
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
bylo	být	k5eAaImAgNnS	být
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hrabala	Hrabal	k1gMnSc2	Hrabal
v	v	k7c6	v
Nymburce	Nymburk	k1gInSc6	Nymburk
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
gymnáziu	gymnázium	k1gNnSc6	gymnázium
studoval	studovat	k5eAaImAgMnS	studovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
absolventů	absolvent	k1gMnPc2	absolvent
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Hrabalovi	Hrabal	k1gMnSc6	Hrabal
byla	být	k5eAaImAgFnS	být
také	také	k9	také
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
planetka	planetka	k1gFnSc1	planetka
4112	[number]	k4	4112
Hrabal	Hrabal	k1gMnSc1	Hrabal
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nymburských	nymburský	k2eAgFnPc2d1	Nymburská
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
