<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Hrabal	Hrabal	k1gMnSc1	Hrabal
<g/>
,	,	kIx,	,
rozený	rozený	k2eAgMnSc1d1	rozený
Bohumil	Bohumil	k1gMnSc1	Bohumil
František	František	k1gMnSc1	František
Kilian	Kilian	k1gMnSc1	Kilian
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1914	[number]	k4	1914
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1997	[number]	k4	1997
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
a	a	k8xC	a
nejosobitějších	osobitý	k2eAgMnPc2d3	nejosobitější
spisovatelů	spisovatel	k1gMnPc2	spisovatel
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
