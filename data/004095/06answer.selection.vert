<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Homuta	Homut	k1gMnSc2	Homut
byl	být	k5eAaImAgInS	být
český	český	k2eAgMnSc1d1	český
elektrotechnik	elektrotechnik	k1gMnSc1	elektrotechnik
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
remosky	remoska	k1gFnSc2	remoska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
menšími	malý	k2eAgFnPc7d2	menší
modifikacemi	modifikace	k1gFnPc7	modifikace
používána	používat	k5eAaImNgFnS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
