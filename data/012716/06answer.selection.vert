<s>
Zoroastrismus	Zoroastrismus	k1gInSc1	Zoroastrismus
či	či	k8xC	či
zarathuštrismus	zarathuštrismus	k1gInSc1	zarathuštrismus
je	být	k5eAaImIp3nS	být
íránské	íránský	k2eAgNnSc4d1	íránské
dualistické	dualistický	k2eAgNnSc4d1	dualistické
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
zakladatelem	zakladatel	k1gMnSc7	zakladatel
prorok	prorok	k1gMnSc1	prorok
Zarathuštra	Zarathuštr	k1gMnSc2	Zarathuštr
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Zoroaster	Zoroaster	k1gInSc1	Zoroaster
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
