<p>
<s>
Zoroastrismus	Zoroastrismus	k1gInSc1	Zoroastrismus
či	či	k8xC	či
zarathuštrismus	zarathuštrismus	k1gInSc1	zarathuštrismus
je	být	k5eAaImIp3nS	být
íránské	íránský	k2eAgNnSc4d1	íránské
dualistické	dualistický	k2eAgNnSc4d1	dualistické
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
zakladatelem	zakladatel	k1gMnSc7	zakladatel
prorok	prorok	k1gMnSc1	prorok
Zarathuštra	Zarathuštr	k1gMnSc2	Zarathuštr
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Zoroaster	Zoroaster	k1gInSc1	Zoroaster
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
nazýván	nazývat	k5eAaImNgInS	nazývat
také	také	k9	také
jako	jako	k8xS	jako
mazdaismus	mazdaismus	k1gInSc4	mazdaismus
podle	podle	k7c2	podle
jména	jméno	k1gNnSc2	jméno
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
boha	bůh	k1gMnSc2	bůh
či	či	k8xC	či
pársismus	pársismus	k1gInSc4	pársismus
podle	podle	k7c2	podle
názvu	název	k1gInSc2	název
jeho	jeho	k3xOp3gMnPc2	jeho
vyznavačů	vyznavač	k1gMnPc2	vyznavač
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
(	(	kIx(	(
<g/>
Pársové	Párs	k1gMnPc1	Párs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
historikové	historik	k1gMnPc1	historik
považují	považovat	k5eAaImIp3nP	považovat
zoroastrismus	zoroastrismus	k1gInSc4	zoroastrismus
za	za	k7c4	za
historicky	historicky	k6eAd1	historicky
první	první	k4xOgNnSc4	první
nejstarší	starý	k2eAgNnSc4d3	nejstarší
světové	světový	k2eAgNnSc4d1	světové
náboženství	náboženství	k1gNnSc4	náboženství
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
byl	být	k5eAaImAgInS	být
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
náboženstvím	náboženství	k1gNnSc7	náboženství
Perské	perský	k2eAgFnSc2d1	perská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	dík	k1gInPc1	dík
vlivu	vliv	k1gInSc2	vliv
této	tento	k3xDgFnSc2	tento
civilizace	civilizace	k1gFnSc2	civilizace
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
mimo	mimo	k7c4	mimo
historické	historický	k2eAgFnPc4d1	historická
země	zem	k1gFnPc4	zem
Persie	Persie	k1gFnSc2	Persie
(	(	kIx(	(
<g/>
dnešního	dnešní	k2eAgInSc2d1	dnešní
Íránu	Írán	k1gInSc2	Írán
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc4d1	možný
vystopovat	vystopovat	k5eAaPmF	vystopovat
v	v	k7c6	v
Arménii	Arménie	k1gFnSc6	Arménie
<g/>
,	,	kIx,	,
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
Ázerbájdžánu	Ázerbájdžán	k1gInSc6	Ázerbájdžán
<g/>
,	,	kIx,	,
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
Pákistánu	Pákistán	k1gInSc6	Pákistán
<g/>
,	,	kIx,	,
Tádžikistánu	Tádžikistán	k1gInSc2	Tádžikistán
a	a	k8xC	a
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
středověku	středověk	k1gInSc2	středověk
nahrazen	nahradit	k5eAaPmNgInS	nahradit
islámem	islám	k1gInSc7	islám
nebo	nebo	k8xC	nebo
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
soupeřil	soupeřit	k5eAaImAgInS	soupeřit
také	také	k6eAd1	také
s	s	k7c7	s
manicheismem	manicheismus	k1gInSc7	manicheismus
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
je	být	k5eAaImIp3nS	být
zoroastrismus	zoroastrismus	k1gInSc1	zoroastrismus
činný	činný	k2eAgInSc1d1	činný
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
a	a	k8xC	a
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
málo	málo	k1gNnSc1	málo
jeho	jeho	k3xOp3gMnPc2	jeho
vyznavačů	vyznavač	k1gMnPc2	vyznavač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stáří	stáří	k1gNnSc1	stáří
a	a	k8xC	a
původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Doba	doba	k1gFnSc1	doba
vzniku	vznik	k1gInSc2	vznik
zoroastrismu	zoroastrismus	k1gInSc2	zoroastrismus
je	být	k5eAaImIp3nS	být
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
žil	žít	k5eAaImAgMnS	žít
historický	historický	k2eAgMnSc1d1	historický
prorok	prorok	k1gMnSc1	prorok
Zarathuštra	Zarathuštrum	k1gNnSc2	Zarathuštrum
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
donedávna	donedávna	k6eAd1	donedávna
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
600	[number]	k4	600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
na	na	k7c6	na
území	území	k1gNnSc6	území
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
filologický	filologický	k2eAgInSc4d1	filologický
rozbor	rozbor	k1gInSc4	rozbor
nejstarších	starý	k2eAgInPc2d3	nejstarší
posvátných	posvátný	k2eAgInPc2d1	posvátný
textů	text	k1gInPc2	text
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
počátky	počátek	k1gInPc1	počátek
nauky	nauka	k1gFnSc2	nauka
sahají	sahat	k5eAaImIp3nP	sahat
snad	snad	k9	snad
až	až	k9	až
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
To	ten	k3xDgNnSc1	ten
činí	činit	k5eAaImIp3nS	činit
ze	z	k7c2	z
zoroastrismu	zoroastrismus	k1gInSc2	zoroastrismus
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
náboženství	náboženství	k1gNnPc2	náboženství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zoroastrismus	Zoroastrismus	k1gInSc1	Zoroastrismus
se	se	k3xPyFc4	se
nejúspěšněji	úspěšně	k6eAd3	úspěšně
ujal	ujmout	k5eAaPmAgInS	ujmout
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc4	tento
území	území	k1gNnPc4	území
obývali	obývat	k5eAaImAgMnP	obývat
Protoíránci	Protoíránek	k1gMnPc1	Protoíránek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tu	tu	k6eAd1	tu
zůstali	zůstat	k5eAaPmAgMnP	zůstat
při	při	k7c6	při
přesunu	přesun	k1gInSc6	přesun
kmenů	kmen	k1gInPc2	kmen
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
původním	původní	k2eAgNnSc6d1	původní
náboženství	náboženství	k1gNnSc6	náboženství
se	se	k3xPyFc4	se
nezachovalo	zachovat	k5eNaPmAgNnS	zachovat
mnoho	mnoho	k4c1	mnoho
poznatků	poznatek	k1gInPc2	poznatek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
podobností	podobnost	k1gFnPc2	podobnost
védského	védský	k2eAgNnSc2d1	védské
náboženství	náboženství	k1gNnSc2	náboženství
se	s	k7c7	s
zoroastrismem	zoroastrismus	k1gInSc7	zoroastrismus
lze	lze	k6eAd1	lze
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
védy	véda	k1gFnPc1	véda
i	i	k8xC	i
zoroastrismus	zoroastrismus	k1gInSc1	zoroastrismus
čerpají	čerpat	k5eAaImIp3nP	čerpat
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
pramene	pramen	k1gInSc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zoroastrismu	zoroastrismus	k1gInSc2	zoroastrismus
pronikají	pronikat	k5eAaImIp3nP	pronikat
jména	jméno	k1gNnSc2	jméno
některých	některý	k3yIgNnPc2	některý
původních	původní	k2eAgNnPc2d1	původní
božstev	božstvo	k1gNnPc2	božstvo
<g/>
,	,	kIx,	,
jen	jen	k9	jen
zde	zde	k6eAd1	zde
sehrávají	sehrávat	k5eAaImIp3nP	sehrávat
jinou	jiný	k2eAgFnSc4d1	jiná
úlohu	úloha	k1gFnSc4	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
protoíránské	protoíránský	k2eAgNnSc1d1	protoíránský
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
bohy	bůh	k1gMnPc4	bůh
<g/>
,	,	kIx,	,
daivové	daivová	k1gFnPc4	daivová
<g/>
,	,	kIx,	,
nabývá	nabývat	k5eAaImIp3nS	nabývat
v	v	k7c6	v
posvátném	posvátný	k2eAgInSc6d1	posvátný
spise	spis	k1gInSc6	spis
Avesta	Avesta	k1gFnSc1	Avesta
významu	význam	k1gInSc2	význam
démoni	démon	k1gMnPc5	démon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zarathuštrovo	Zarathuštrův	k2eAgNnSc1d1	Zarathuštrovo
dílo	dílo	k1gNnSc1	dílo
a	a	k8xC	a
náboženská	náboženský	k2eAgFnSc1d1	náboženská
reforma	reforma	k1gFnSc1	reforma
==	==	k?	==
</s>
</p>
<p>
<s>
Zarathuštra	Zarathuštra	k1gFnSc1	Zarathuštra
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
nejstarších	starý	k2eAgFnPc2d3	nejstarší
částí	část	k1gFnPc2	část
Jasny	jasno	k1gNnPc7	jasno
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
zpěvů	zpěv	k1gInPc2	zpěv
<g/>
"	"	kIx"	"
Gáth	Gátha	k1gFnPc2	Gátha
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
jazyk	jazyk	k1gInSc1	jazyk
nejspíš	nejspíš	k9	nejspíš
nebyl	být	k5eNaImAgInS	být
přímo	přímo	k6eAd1	přímo
jeho	jeho	k3xOp3gFnSc7	jeho
mateřštinou	mateřština	k1gFnSc7	mateřština
<g/>
,	,	kIx,	,
značné	značný	k2eAgFnSc2d1	značná
shody	shoda	k1gFnSc2	shoda
v	v	k7c6	v
dikci	dikce	k1gFnSc6	dikce
se	s	k7c7	s
staroindickými	staroindický	k2eAgFnPc7d1	staroindická
památkami	památka	k1gFnPc7	památka
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
existenci	existence	k1gFnSc4	existence
"	"	kIx"	"
<g/>
kněžského	kněžský	k2eAgInSc2d1	kněžský
<g/>
"	"	kIx"	"
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Zarathuštrova	Zarathuštrův	k2eAgFnSc1d1	Zarathuštrova
reforma	reforma	k1gFnSc1	reforma
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odvrhl	odvrhnout	k5eAaPmAgMnS	odvrhnout
starší	starý	k2eAgInSc4d2	starší
polyteismus	polyteismus	k1gInSc4	polyteismus
a	a	k8xC	a
za	za	k7c4	za
jediného	jediný	k2eAgMnSc4d1	jediný
svrchovaného	svrchovaný	k2eAgMnSc4d1	svrchovaný
boha	bůh	k1gMnSc4	bůh
označil	označit	k5eAaPmAgMnS	označit
"	"	kIx"	"
<g/>
vládce-moudrost	vládceoudrost	k1gFnSc4	vládce-moudrost
<g/>
"	"	kIx"	"
Ahuru	Ahura	k1gFnSc4	Ahura
Mazdu	Mazda	k1gFnSc4	Mazda
<g/>
.	.	kIx.	.
</s>
<s>
Zarathuštra	Zarathuštrum	k1gNnPc4	Zarathuštrum
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zjevil	zjevit	k5eAaPmAgInS	zjevit
jako	jako	k9	jako
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
,	,	kIx,	,
spravedlivý	spravedlivý	k2eAgMnSc1d1	spravedlivý
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
odměňující	odměňující	k2eAgNnSc1d1	odměňující
dobro	dobro	k1gNnSc1	dobro
a	a	k8xC	a
trestající	trestající	k2eAgNnSc1d1	trestající
zlo	zlo	k1gNnSc1	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Ahura	Ahura	k6eAd1	Ahura
Mazda	Mazda	k1gFnSc1	Mazda
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
pravdou	pravda	k1gFnSc7	pravda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nesmrtelný	smrtelný	k2eNgMnSc1d1	nesmrtelný
a	a	k8xC	a
nedělitelný	dělitelný	k2eNgMnSc1d1	nedělitelný
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
základní	základní	k2eAgInPc4d1	základní
božské	božský	k2eAgInPc4d1	božský
atributy	atribut	k1gInPc4	atribut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dualismus	dualismus	k1gInSc1	dualismus
dobra	dobro	k1gNnSc2	dobro
a	a	k8xC	a
zla	zlo	k1gNnSc2	zlo
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
takovéto	takovýto	k3xDgFnSc6	takovýto
koncepci	koncepce	k1gFnSc6	koncepce
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
bytosti	bytost	k1gFnSc2	bytost
se	se	k3xPyFc4	se
Zarathuštra	Zarathuštra	k1gFnSc1	Zarathuštra
musel	muset	k5eAaImAgInS	muset
nutně	nutně	k6eAd1	nutně
střetnout	střetnout	k5eAaPmF	střetnout
s	s	k7c7	s
problémem	problém	k1gInSc7	problém
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
zlo	zlo	k1gNnSc4	zlo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ahura	Ahura	k1gFnSc1	Ahura
Mazda	Mazda	k1gFnSc1	Mazda
jako	jako	k8xC	jako
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
dobro	dobro	k1gNnSc1	dobro
<g/>
,	,	kIx,	,
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
a	a	k8xC	a
moudrý	moudrý	k2eAgMnSc1d1	moudrý
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
být	být	k5eAaImF	být
původcem	původce	k1gMnSc7	původce
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Zarathuštrových	Zarathuštrův	k2eAgFnPc2d1	Zarathuštrova
představ	představa	k1gFnPc2	představa
stvořil	stvořit	k5eAaPmAgMnS	stvořit
Ahura	Ahura	k1gMnSc1	Ahura
Mazda	Mazda	k1gFnSc1	Mazda
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
dvě	dva	k4xCgFnPc1	dva
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
dvojčata	dvojče	k1gNnPc1	dvojče
Spenta	Spenta	k1gMnSc1	Spenta
Mainju	Mainju	k1gMnSc1	Mainju
a	a	k8xC	a
Angra	Angra	k1gMnSc1	Angra
Mainju	Mainju	k1gMnSc1	Mainju
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgFnP	být
plodem	plod	k1gInSc7	plod
jeho	jeho	k3xOp3gFnSc2	jeho
mysli	mysl	k1gFnSc2	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Spenta	Spenta	k1gMnSc1	Spenta
Mainju	Mainju	k1gMnSc1	Mainju
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
konat	konat	k5eAaImF	konat
dobro	dobro	k1gNnSc4	dobro
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Ahura	Ahura	k1gMnSc1	Ahura
Mazdovým	Mazdův	k2eAgMnSc7d1	Mazdův
duchem	duch	k1gMnSc7	duch
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Angra	Angra	k1gFnSc1	Angra
Mainju	Mainju	k1gFnPc2	Mainju
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
v	v	k7c4	v
představitele	představitel	k1gMnSc4	představitel
a	a	k8xC	a
původce	původce	k1gMnSc4	původce
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jeho	jeho	k3xOp3gFnSc1	jeho
povaha	povaha	k1gFnSc1	povaha
byla	být	k5eAaImAgFnS	být
zlá	zlý	k2eAgFnSc1d1	zlá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
zvolil	zvolit	k5eAaPmAgMnS	zvolit
takovou	takový	k3xDgFnSc4	takový
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Spenta	Spenta	k1gFnSc1	Spenta
Mainju	Mainju	k1gFnSc2	Mainju
a	a	k8xC	a
Angra	Angro	k1gNnSc2	Angro
Mainju	Mainju	k1gFnSc2	Mainju
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
spolu	spolu	k6eAd1	spolu
neustále	neustále	k6eAd1	neustále
zápasí	zápasit	k5eAaImIp3nS	zápasit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
nutné	nutný	k2eAgNnSc1d1	nutné
je	on	k3xPp3gInPc4	on
chápat	chápat	k5eAaImF	chápat
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
celistvosti	celistvost	k1gFnSc6	celistvost
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
existovat	existovat	k5eAaImF	existovat
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Představují	představovat	k5eAaImIp3nP	představovat
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
tmu	tma	k1gFnSc4	tma
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
a	a	k8xC	a
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dualismus	dualismus	k1gInSc1	dualismus
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
světě	svět	k1gInSc6	svět
stále	stále	k6eAd1	stále
objevují	objevovat	k5eAaImIp3nP	objevovat
nové	nový	k2eAgFnPc4d1	nová
pozitivní	pozitivní	k2eAgFnPc4d1	pozitivní
i	i	k8xC	i
negativní	negativní	k2eAgFnPc4d1	negativní
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
stojí	stát	k5eAaImIp3nP	stát
buď	buď	k8xC	buď
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Spenta	Spent	k1gMnSc2	Spent
Mainju	Mainju	k1gMnSc2	Mainju
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
Angra	Angra	k1gMnSc1	Angra
Mainju	Mainju	k1gMnSc1	Mainju
a	a	k8xC	a
navzájem	navzájem	k6eAd1	navzájem
spolu	spolu	k6eAd1	spolu
bojují	bojovat	k5eAaImIp3nP	bojovat
<g/>
.	.	kIx.	.
</s>
<s>
Dobří	dobrý	k2eAgMnPc1d1	dobrý
duchové	duch	k1gMnPc1	duch
jsou	být	k5eAaImIp3nP	být
souborně	souborně	k6eAd1	souborně
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
améša	améša	k6eAd1	améša
spentové	spent	k1gMnPc1	spent
(	(	kIx(	(
<g/>
nesmrtelní	smrtelní	k2eNgMnPc1d1	nesmrtelní
svatí	svatý	k1gMnPc1	svatý
<g/>
)	)	kIx)	)
a	a	k8xC	a
zosobňují	zosobňovat	k5eAaImIp3nP	zosobňovat
Ahura	Ahuro	k1gNnPc1	Ahuro
Mazdovy	Mazdův	k2eAgFnSc2d1	Mazdova
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
boku	bok	k1gInSc6	bok
Angra	Angr	k1gMnSc2	Angr
Mainju	Mainju	k1gMnSc2	Mainju
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
daivové	daiva	k1gMnPc1	daiva
(	(	kIx(	(
<g/>
démoni	démon	k1gMnPc1	démon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zarathuštra	Zarathuštra	k1gFnSc1	Zarathuštra
nabádá	nabádat	k5eAaBmIp3nS	nabádat
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
konat	konat	k5eAaImF	konat
dobro	dobro	k1gNnSc4	dobro
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
činí	činit	k5eAaImIp3nS	činit
nepravosti	nepravost	k1gFnPc4	nepravost
<g/>
,	,	kIx,	,
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
s	s	k7c7	s
Angra	Angr	k1gMnSc2	Angr
Mainjou	Mainjý	k2eAgFnSc7d1	Mainjý
a	a	k8xC	a
opakují	opakovat	k5eAaImIp3nP	opakovat
jeho	jeho	k3xOp3gFnSc4	jeho
volbu	volba	k1gFnSc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
momentem	moment	k1gInSc7	moment
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zoroastrismu	zoroastrismus	k1gInSc6	zoroastrismus
právě	právě	k9	právě
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
nutnost	nutnost	k1gFnSc4	nutnost
svobodné	svobodný	k2eAgFnSc2d1	svobodná
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Člověku	člověk	k1gMnSc3	člověk
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostává	dostávat	k5eAaImIp3nS	dostávat
morální	morální	k2eAgFnSc3d1	morální
zodpovědnosti	zodpovědnost	k1gFnSc3	zodpovědnost
za	za	k7c7	za
jeho	jeho	k3xOp3gInPc7	jeho
činy	čin	k1gInPc7	čin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
sil	síla	k1gFnPc2	síla
dobra	dobro	k1gNnSc2	dobro
a	a	k8xC	a
zla	zlo	k1gNnSc2	zlo
==	==	k?	==
</s>
</p>
<p>
<s>
Ahura	Ahura	k1gFnSc1	Ahura
Mazda	Mazda	k1gFnSc1	Mazda
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
božstvo	božstvo	k1gNnSc1	božstvo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spenta	Spenta	k1gMnSc1	Spenta
Mainju	Mainju	k1gMnSc1	Mainju
Angra	Angra	k1gMnSc1	Angra
Mainju	Mainju	k1gMnSc1	Mainju
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
svatý	svatý	k2eAgMnSc1d1	svatý
duch	duch	k1gMnSc1	duch
Ahura	Ahur	k1gInSc2	Ahur
Mazdy	Mazda	k1gFnSc2	Mazda
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
zlý	zlý	k2eAgMnSc1d1	zlý
duch	duch	k1gMnSc1	duch
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Améša	Améša	k1gFnSc1	Améša
spentové	spentový	k2eAgFnSc2d1	spentový
Daivové	Daivová	k1gFnSc2	Daivová
</s>
</p>
<p>
<s>
•	•	k?	•
Kšathra	Kšathra	k1gFnSc1	Kšathra
Vairja	Vairja	k1gMnSc1	Vairja
-	-	kIx~	-
duch	duch	k1gMnSc1	duch
moci	moc	k1gFnSc2	moc
•	•	k?	•
Aka	Aka	k1gMnSc1	Aka
Manah	Manah	k1gMnSc1	Manah
-	-	kIx~	-
démon	démon	k1gMnSc1	démon
zlé	zlý	k2eAgFnSc2d1	zlá
mysli	mysl	k1gFnSc2	mysl
</s>
</p>
<p>
<s>
•	•	k?	•
Aša	Aša	k1gMnSc1	Aša
Vahišta	Vahišta	k1gMnSc1	Vahišta
-	-	kIx~	-
duch	duch	k1gMnSc1	duch
práva	právo	k1gNnSc2	právo
•	•	k?	•
Indra	Indra	k1gMnSc1	Indra
-	-	kIx~	-
démon	démon	k1gMnSc1	démon
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
•	•	k?	•
Haurvatád	Haurvatáda	k1gFnPc2	Haurvatáda
-	-	kIx~	-
duch	duch	k1gMnSc1	duch
zdraví	zdraví	k1gNnSc2	zdraví
•	•	k?	•
Saurva	Saurva	k1gFnSc1	Saurva
-	-	kIx~	-
démon	démon	k1gMnSc1	démon
destrukce	destrukce	k1gFnSc2	destrukce
</s>
</p>
<p>
<s>
•	•	k?	•
Vohu	Voha	k1gMnSc4	Voha
Manah	Manah	k1gMnSc1	Manah
-	-	kIx~	-
duch	duch	k1gMnSc1	duch
dobré	dobrý	k2eAgFnSc2d1	dobrá
mysli	mysl	k1gFnSc2	mysl
•	•	k?	•
Náonhaitja	Náonhaitja	k1gMnSc1	Náonhaitja
-	-	kIx~	-
démon	démon	k1gMnSc1	démon
nájezdnictví	nájezdnictví	k1gNnSc4	nájezdnictví
</s>
</p>
<p>
<s>
•	•	k?	•
Spenta	Spenta	k1gMnSc1	Spenta
Ármaiti	Ármait	k2eAgMnPc1d1	Ármait
-	-	kIx~	-
duch	duch	k1gMnSc1	duch
zbožnosti	zbožnost	k1gFnSc2	zbožnost
•	•	k?	•
Tauru	Taur	k1gInSc2	Taur
-	-	kIx~	-
démon	démon	k1gMnSc1	démon
temnoty	temnota	k1gFnSc2	temnota
</s>
</p>
<p>
<s>
•	•	k?	•
Ameretát	Ameretát	k1gInSc1	Ameretát
-	-	kIx~	-
duch	duch	k1gMnSc1	duch
nesmrtelnosti	nesmrtelnost	k1gFnSc2	nesmrtelnost
•	•	k?	•
Zairi	Zair	k1gFnSc2	Zair
-	-	kIx~	-
démon	démon	k1gMnSc1	démon
opilství	opilství	k1gNnSc4	opilství
</s>
</p>
<p>
<s>
•	•	k?	•
Jazatové	Jazatový	k2eAgFnPc1d1	Jazatový
-	-	kIx~	-
nižší	nízký	k2eAgFnPc1d2	nižší
božské	božský	k2eAgFnPc1d1	božská
bytosti	bytost	k1gFnPc1	bytost
•	•	k?	•
nižší	nízký	k2eAgFnPc1d2	nižší
daivové	daivová	k1gFnPc1	daivová
</s>
</p>
<p>
<s>
==	==	k?	==
Eschatologie	eschatologie	k1gFnSc2	eschatologie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
zoroastrismu	zoroastrismus	k1gInSc6	zoroastrismus
se	se	k3xPyFc4	se
život	život	k1gInSc1	život
lidí	člověk	k1gMnPc2	člověk
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tělesný	tělesný	k2eAgInSc4d1	tělesný
a	a	k8xC	a
duševní	duševní	k2eAgInSc4d1	duševní
<g/>
.	.	kIx.	.
</s>
<s>
Duše	duše	k1gFnSc1	duše
(	(	kIx(	(
<g/>
urván	urván	k2eAgInSc1d1	urván
<g/>
)	)	kIx)	)
přežije	přežít	k5eAaPmIp3nS	přežít
i	i	k9	i
zánik	zánik	k1gInSc1	zánik
tělesné	tělesný	k2eAgFnSc2d1	tělesná
schránky	schránka	k1gFnSc2	schránka
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
další	další	k2eAgInSc1d1	další
úděl	úděl	k1gInSc1	úděl
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
žila	žít	k5eAaImAgFnS	žít
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
volila	volit	k5eAaImAgFnS	volit
mezi	mezi	k7c7	mezi
dobrem	dobro	k1gNnSc7	dobro
a	a	k8xC	a
zlem	zlo	k1gNnSc7	zlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spravedlivý	spravedlivý	k2eAgMnSc1d1	spravedlivý
člověk	člověk	k1gMnSc1	člověk
smí	smět	k5eAaImIp3nS	smět
vstoupit	vstoupit	k5eAaPmF	vstoupit
přes	přes	k7c4	přes
most	most	k1gInSc4	most
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
čhinvad-púl	čhinvadúl	k1gInSc1	čhinvad-púl
<g/>
)	)	kIx)	)
do	do	k7c2	do
domu	dům	k1gInSc2	dům
hymnů	hymnus	k1gInPc2	hymnus
(	(	kIx(	(
<g/>
garó-demána	garóemán	k2eAgFnSc1d1	garó-demán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obývaném	obývaný	k2eAgInSc6d1	obývaný
Ahura	Ahuro	k1gNnSc2	Ahuro
Mazdou	Mazda	k1gFnSc7	Mazda
i	i	k9	i
ostatními	ostatní	k2eAgInPc7d1	ostatní
spravedlivými	spravedlivý	k2eAgInPc7d1	spravedlivý
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
domu	dům	k1gInSc2	dům
hymnů	hymnus	k1gInPc2	hymnus
je	být	k5eAaImIp3nS	být
provázen	provázet	k5eAaImNgInS	provázet
krásnou	krásný	k2eAgFnSc7d1	krásná
Daénou	Daéna	k1gFnSc7	Daéna
<g/>
,	,	kIx,	,
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
dobrých	dobrý	k2eAgFnPc2d1	dobrá
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Duše	duše	k1gFnSc1	duše
nespravedlivého	spravedlivý	k2eNgMnSc2d1	nespravedlivý
člověka	člověk	k1gMnSc2	člověk
bude	být	k5eAaImBp3nS	být
naopak	naopak	k6eAd1	naopak
provázena	provázet	k5eAaImNgFnS	provázet
ošklivou	ošklivý	k2eAgFnSc7d1	ošklivá
dívkou	dívka	k1gFnSc7	dívka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
výtvorem	výtvor	k1gInSc7	výtvor
jeho	jeho	k3xOp3gFnPc2	jeho
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Daéná	Daéná	k1gFnSc1	Daéná
představuje	představovat	k5eAaImIp3nS	představovat
lidské	lidský	k2eAgNnSc4d1	lidské
svědomí	svědomí	k1gNnSc4	svědomí
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
dobré	dobrý	k2eAgNnSc1d1	dobré
či	či	k8xC	či
zlé	zlý	k2eAgNnSc1d1	zlé
<g/>
,	,	kIx,	,
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
vlastní	vlastní	k2eAgNnSc1d1	vlastní
Já	já	k3xPp1nSc1	já
té	ten	k3xDgFnSc2	ten
které	který	k3yQgFnSc2	který
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddělení	oddělení	k1gNnSc1	oddělení
dobrých	dobrý	k2eAgFnPc2d1	dobrá
duší	duše	k1gFnPc2	duše
od	od	k7c2	od
špatných	špatný	k2eAgInPc2d1	špatný
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
mostě	most	k1gInSc6	most
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
který	který	k3yQgInSc4	který
jsou	být	k5eAaImIp3nP	být
převáděny	převáděn	k2eAgFnPc1d1	převáděna
<g/>
.	.	kIx.	.
</s>
<s>
Utrpení	utrpení	k1gNnSc1	utrpení
či	či	k8xC	či
blaženost	blaženost	k1gFnSc1	blaženost
jsou	být	k5eAaImIp3nP	být
přímo	přímo	k6eAd1	přímo
úměrné	úměrná	k1gFnSc2	úměrná
skutkům	skutek	k1gInPc3	skutek
<g/>
,	,	kIx,	,
vykonaným	vykonaný	k2eAgFnPc3d1	vykonaná
během	během	k7c2	během
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
skutky	skutek	k1gInPc4	skutek
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc1	stav
blaženosti	blaženost	k1gFnSc2	blaženost
a	a	k8xC	a
utrpení	utrpení	k1gNnSc2	utrpení
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
pramenů	pramen	k1gInPc2	pramen
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
duše	duše	k1gFnSc1	duše
zemřelého	zemřelý	k1gMnSc4	zemřelý
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stavu	stav	k1gInSc6	stav
setrvávat	setrvávat	k5eAaImF	setrvávat
věčně	věčně	k6eAd1	věčně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poslední	poslední	k2eAgInSc1d1	poslední
soud	soud	k1gInSc1	soud
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
zoroastrismu	zoroastrismus	k1gInSc6	zoroastrismus
nacházíme	nacházet	k5eAaImIp1nP	nacházet
též	též	k9	též
popis	popis	k1gInSc4	popis
posledního	poslední	k2eAgInSc2d1	poslední
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
bude	být	k5eAaImBp3nS	být
předcházet	předcházet	k5eAaImF	předcházet
souboj	souboj	k1gInSc1	souboj
sil	síla	k1gFnPc2	síla
dobra	dobro	k1gNnSc2	dobro
<g/>
,	,	kIx,	,
zastoupených	zastoupený	k2eAgFnPc2d1	zastoupená
spasitelem	spasitel	k1gMnSc7	spasitel
Saošjantem	Saošjant	k1gInSc7	Saošjant
<g/>
,	,	kIx,	,
se	s	k7c7	s
silami	síla	k1gFnPc7	síla
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
Saošjanta	Saošjant	k1gMnSc2	Saošjant
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
vzkříšení	vzkříšení	k1gNnSc3	vzkříšení
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
spravedlivých	spravedlivý	k2eAgMnPc2d1	spravedlivý
i	i	k8xC	i
nespravedlivých	spravedlivý	k2eNgMnPc2d1	nespravedlivý
<g/>
.	.	kIx.	.
</s>
<s>
Nespravedliví	spravedlivý	k2eNgMnPc1d1	nespravedlivý
budou	být	k5eAaImBp3nP	být
očištěni	očistit	k5eAaPmNgMnP	očistit
ohněm	oheň	k1gInSc7	oheň
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
vstoupit	vstoupit	k5eAaPmF	vstoupit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
spravedlivými	spravedlivý	k2eAgMnPc7d1	spravedlivý
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
blaženosti	blaženost	k1gFnSc2	blaženost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zoroastrismu	zoroastrismus	k1gInSc6	zoroastrismus
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
až	až	k9	až
o	o	k7c6	o
čtyřech	čtyři	k4xCgMnPc6	čtyři
spasitelích	spasitel	k1gMnPc6	spasitel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
postupně	postupně	k6eAd1	postupně
vykonávat	vykonávat	k5eAaImF	vykonávat
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Spasení	spasení	k1gNnSc1	spasení
završí	završit	k5eAaPmIp3nS	završit
teprve	teprve	k6eAd1	teprve
poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
narodit	narodit	k5eAaPmF	narodit
z	z	k7c2	z
panny	panna	k1gFnSc2	panna
oplodněné	oplodněný	k2eAgFnSc2d1	oplodněná
Zarathuštrovým	Zarathuštrův	k2eAgNnSc7d1	Zarathuštrovo
semenem	semeno	k1gNnSc7	semeno
<g/>
,	,	kIx,	,
uloženém	uložený	k2eAgNnSc6d1	uložené
v	v	k7c6	v
jistém	jistý	k2eAgNnSc6d1	jisté
jezeře	jezero	k1gNnSc6	jezero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
panna	panna	k1gFnSc1	panna
bude	být	k5eAaImBp3nS	být
koupat	koupat	k5eAaImF	koupat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Učení	učení	k1gNnSc2	učení
zoroastrismu	zoroastrismus	k1gInSc2	zoroastrismus
==	==	k?	==
</s>
</p>
<p>
<s>
Učení	učení	k1gNnSc1	učení
zoroastrismu	zoroastrismus	k1gInSc2	zoroastrismus
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nejpůvodnější	původní	k2eAgFnSc6d3	nejpůvodnější
formě	forma	k1gFnSc6	forma
podáno	podat	k5eAaPmNgNnS	podat
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
Avesty	Avesta	k1gFnSc2	Avesta
<g/>
.	.	kIx.	.
</s>
<s>
Skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dala	dát	k5eAaPmAgFnS	dát
počátek	počátek	k1gInSc4	počátek
všemu	všecek	k3xTgNnSc3	všecek
jsoucnu	jsoucno	k1gNnSc3	jsoucno
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
praprincip	praprincip	k1gInSc1	praprincip
chápaný	chápaný	k2eAgInSc1d1	chápaný
jako	jako	k8xC	jako
čistě	čistě	k6eAd1	čistě
duchovní	duchovní	k2eAgFnSc4d1	duchovní
bytost	bytost	k1gFnSc4	bytost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
princip	princip	k1gInSc1	princip
rozdvojený	rozdvojený	k2eAgInSc1d1	rozdvojený
v	v	k7c6	v
sebeafirmaci	sebeafirmace	k1gFnSc6	sebeafirmace
(	(	kIx(	(
<g/>
Ahura	Ahura	k1gFnSc1	Ahura
Mazda	Mazda	k1gFnSc1	Mazda
<g/>
)	)	kIx)	)
a	a	k8xC	a
sebenegaci	sebenegace	k1gFnSc4	sebenegace
(	(	kIx(	(
<g/>
Angra	Angra	k1gMnSc1	Angra
Mainju	Mainju	k1gMnSc1	Mainju
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
dva	dva	k4xCgInPc1	dva
zabsolutizované	zabsolutizovaný	k2eAgInPc1d1	zabsolutizovaný
protiklady	protiklad	k1gInPc1	protiklad
a	a	k8xC	a
rozpory	rozpor	k1gInPc1	rozpor
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
působení	působení	k1gNnSc1	působení
praprincipu	praprincip	k1gInSc2	praprincip
a	a	k8xC	a
jimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
podmíněna	podmíněn	k2eAgFnSc1d1	podmíněna
existence	existence	k1gFnSc1	existence
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Ahura	Ahura	k1gFnSc1	Ahura
Mazda	Mazda	k1gFnSc1	Mazda
neboli	neboli	k8xC	neboli
Óhrmazd	Óhrmazd	k1gInSc1	Óhrmazd
podmiňuje	podmiňovat	k5eAaImIp3nS	podmiňovat
bytí	bytí	k1gNnSc1	bytí
<g/>
,	,	kIx,	,
Angra	Angra	k1gFnSc1	Angra
Mainju	Mainju	k1gMnSc1	Mainju
čili	čili	k8xC	čili
Ahriman	Ahriman	k1gMnSc1	Ahriman
ztělesňuje	ztělesňovat	k5eAaImIp3nS	ztělesňovat
jeho	jeho	k3xOp3gNnSc4	jeho
popření	popření	k1gNnSc4	popření
<g/>
;	;	kIx,	;
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
staví	stavit	k5eAaBmIp3nS	stavit
Óhrmazd	Óhrmazd	k1gInSc1	Óhrmazd
a	a	k8xC	a
z	z	k7c2	z
negace	negace	k1gFnSc2	negace
popření	popření	k1gNnSc2	popření
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
další	další	k2eAgNnSc1d1	další
bytí	bytí	k1gNnSc1	bytí
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
bytí	bytí	k1gNnSc3	bytí
znovu	znovu	k6eAd1	znovu
působí	působit	k5eAaImIp3nS	působit
Ahriman	Ahriman	k1gMnSc1	Ahriman
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
donekonečna	donekonečna	k6eAd1	donekonečna
srážejí	srážet	k5eAaImIp3nP	srážet
protiklady	protiklad	k1gInPc4	protiklad
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
udržují	udržovat	k5eAaImIp3nP	udržovat
existenci	existence	k1gFnSc4	existence
neosobní	osobní	k2eNgInSc1d1	neosobní
–	–	k?	–
později	pozdě	k6eAd2	pozdě
personifikované	personifikovaný	k2eAgFnSc2d1	personifikovaná
–	–	k?	–
praskutečnosti	praskutečnost	k1gFnSc2	praskutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
vykládají	vykládat	k5eAaImIp3nP	vykládat
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
náboženské	náboženský	k2eAgInPc1d1	náboženský
proudy	proud	k1gInPc1	proud
v	v	k7c6	v
zoroastrismu	zoroastrismus	k1gInSc6	zoroastrismus
jako	jako	k8xC	jako
odvěký	odvěký	k2eAgInSc1d1	odvěký
boj	boj	k1gInSc1	boj
protikladných	protikladný	k2eAgNnPc2d1	protikladné
božstev	božstvo	k1gNnPc2	božstvo
–	–	k?	–
dobrého	dobrý	k2eAgNnSc2d1	dobré
a	a	k8xC	a
zlého	zlý	k2eAgNnSc2d1	zlé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oslavy	oslava	k1gFnPc4	oslava
svátků	svátek	k1gInPc2	svátek
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
historickému	historický	k2eAgInSc3d1	historický
a	a	k8xC	a
geografickému	geografický	k2eAgInSc3d1	geografický
vývoji	vývoj	k1gInSc3	vývoj
zoroastrismu	zoroastrismus	k1gInSc2	zoroastrismus
existují	existovat	k5eAaImIp3nP	existovat
3	[number]	k4	3
různé	různý	k2eAgInPc1d1	různý
kalendáře	kalendář	k1gInPc1	kalendář
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
i	i	k9	i
3	[number]	k4	3
různé	různý	k2eAgFnSc2d1	různá
doby	doba	k1gFnSc2	doba
oslavy	oslava	k1gFnSc2	oslava
svátků	svátek	k1gInPc2	svátek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kalendář	kalendář	k1gInSc4	kalendář
Faslí	Faslí	k1gNnSc2	Faslí
</s>
</p>
<p>
<s>
vždy	vždy	k6eAd1	vždy
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Džamšédí	Džamšédí	k1gNnSc2	Džamšédí
Nourúz	Nourúza	k1gFnPc2	Nourúza
<g/>
,	,	kIx,	,
v	v	k7c6	v
kalendáři	kalendář	k1gInSc6	kalendář
Faslí	Faslí	k1gNnSc2	Faslí
zoroastrijský	zoroastrijský	k2eAgInSc4d1	zoroastrijský
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
</s>
</p>
<p>
<s>
vždy	vždy	k6eAd1	vždy
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Chordád	Chordáda	k1gFnPc2	Chordáda
Sal	Sal	k1gFnSc1	Sal
<g/>
,	,	kIx,	,
v	v	k7c6	v
kalendáři	kalendář	k1gInSc6	kalendář
Faslí	Faslí	k1gNnSc2	Faslí
den	den	k1gInSc4	den
narození	narození	k1gNnSc2	narození
proroka	prorok	k1gMnSc2	prorok
Zarathuštry	Zarathuštr	k1gInPc4	Zarathuštr
</s>
</p>
<p>
<s>
kalendář	kalendář	k1gInSc4	kalendář
Qadímí	Qadímí	k1gNnSc2	Qadímí
</s>
</p>
<p>
<s>
vždy	vždy	k6eAd1	vždy
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Džamšédí	Džamšédí	k1gNnSc2	Džamšédí
Nourúz	Nourúza	k1gFnPc2	Nourúza
<g/>
,	,	kIx,	,
v	v	k7c6	v
kalendáři	kalendář	k1gInSc6	kalendář
Qadímí	Qadímí	k1gNnSc2	Qadímí
zoroastrijský	zoroastrijský	k2eAgInSc4d1	zoroastrijský
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
</s>
</p>
<p>
<s>
vždy	vždy	k6eAd1	vždy
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Chordád	Chordáda	k1gFnPc2	Chordáda
Sal	Sal	k1gFnSc1	Sal
<g/>
,	,	kIx,	,
v	v	k7c6	v
kalendáři	kalendář	k1gInSc6	kalendář
Qadímí	Qadímí	k1gNnSc2	Qadímí
den	den	k1gInSc4	den
narození	narození	k1gNnSc2	narození
proroka	prorok	k1gMnSc4	prorok
Zarathuštry	Zarathuštr	k1gInPc1	Zarathuštr
</s>
</p>
<p>
<s>
kalendář	kalendář	k1gInSc4	kalendář
Rasmí	Rasmí	k1gNnSc2	Rasmí
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
–	–	k?	–
Džamšédí	Džamšédí	k1gNnSc2	Džamšédí
Nourúz	Nourúza	k1gFnPc2	Nourúza
<g/>
,	,	kIx,	,
v	v	k7c6	v
kalendáři	kalendář	k1gInSc6	kalendář
Rasmí	Rasmí	k1gNnSc2	Rasmí
zoroastrijský	zoroastrijský	k2eAgInSc4d1	zoroastrijský
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
vždy	vždy	k6eAd1	vždy
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Chordád	Chordáda	k1gFnPc2	Chordáda
Sal	Sal	k1gFnSc1	Sal
<g/>
,	,	kIx,	,
v	v	k7c6	v
kalendáři	kalendář	k1gInSc6	kalendář
Rasmí	Rasmí	k1gNnSc2	Rasmí
den	den	k1gInSc4	den
narození	narození	k1gNnSc2	narození
proroka	prorok	k1gMnSc4	prorok
Zarathuštry	Zarathuštrum	k1gNnPc7	Zarathuštrum
</s>
</p>
<p>
<s>
==	==	k?	==
Zoroastrismus	Zoroastrismus	k1gInSc1	Zoroastrismus
dnes	dnes	k6eAd1	dnes
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
vyznavačů	vyznavač	k1gMnPc2	vyznavač
zoroastrismu	zoroastrismus	k1gInSc2	zoroastrismus
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
pronásledováním	pronásledování	k1gNnSc7	pronásledování
a	a	k8xC	a
postupnou	postupný	k2eAgFnSc7d1	postupná
konverzí	konverze	k1gFnSc7	konverze
k	k	k7c3	k
islámu	islám	k1gInSc3	islám
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
světě	svět	k1gInSc6	svět
žije	žít	k5eAaImIp3nS	žít
mezi	mezi	k7c4	mezi
124	[number]	k4	124
000	[number]	k4	000
a	a	k8xC	a
190	[number]	k4	190
000	[number]	k4	000
zoroastristů	zoroastrista	k1gMnPc2	zoroastrista
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
většina	většina	k1gFnSc1	většina
nežije	žít	k5eNaImIp3nS	žít
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Sásanovské	Sásanovský	k2eAgFnSc2d1	Sásanovský
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
známí	známý	k1gMnPc1	známý
jako	jako	k8xC	jako
Pársové	Párs	k1gMnPc1	Párs
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
vlasti	vlast	k1gFnSc6	vlast
Íránu	Írán	k1gInSc2	Írán
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Teherán	Teherán	k1gInSc1	Teherán
<g/>
,	,	kIx,	,
Jazd	Jazd	k1gInSc1	Jazd
<g/>
,	,	kIx,	,
Kermán	Kermán	k2eAgInSc1d1	Kermán
a	a	k8xC	a
Kermánšáh	Kermánšáh	k1gInSc1	Kermánšáh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mluví	mluvit	k5eAaImIp3nS	mluvit
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
dialektem	dialekt	k1gInSc7	dialekt
perštiny	perština	k1gFnSc2	perština
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Komunity	komunita	k1gFnPc1	komunita
zoroastristů	zoroastrista	k1gMnPc2	zoroastrista
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
KLÍMA	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
<g/>
.	.	kIx.	.
</s>
<s>
Zarathuštra	Zarathuštra	k1gFnSc1	Zarathuštra
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
220	[number]	k4	220
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7198	[number]	k4	7198
<g/>
-	-	kIx~	-
<g/>
532	[number]	k4	532
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oběti	oběť	k1gFnPc1	oběť
ohňům	oheň	k1gInPc3	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Otakar	Otakar	k1gMnSc1	Otakar
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
346	[number]	k4	346
s.	s.	k?	s.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WIESEHÖFER	WIESEHÖFER	kA	WIESEHÖFER
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Das	Das	k?	Das
antike	antikat	k5eAaPmIp3nS	antikat
Persien	Persien	k1gInSc1	Persien
<g/>
.	.	kIx.	.
</s>
<s>
Von	von	k1gInSc1	von
550	[number]	k4	550
v.	v.	k?	v.
Chr	chr	k0	chr
<g/>
.	.	kIx.	.
bis	bis	k?	bis
650	[number]	k4	650
n.	n.	k?	n.
Chr	chr	k0	chr
<g/>
..	..	k?	..
München	München	k2eAgInSc4d1	München
;	;	kIx,	;
Zürich	Zürich	k1gInSc4	Zürich
<g/>
:	:	kIx,	:
Artemis	Artemis	k1gFnSc1	Artemis
und	und	k?	und
Winkler	Winkler	k1gMnSc1	Winkler
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
426	[number]	k4	426
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7608	[number]	k4	7608
<g/>
-	-	kIx~	-
<g/>
1080	[number]	k4	1080
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zoroastrismus	Zoroastrismus	k1gInSc1	Zoroastrismus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Zoroastrismus	Zoroastrismus	k1gInSc4	Zoroastrismus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Avesta	Avesta	k1gFnSc1	Avesta
<g/>
:	:	kIx,	:
anglický	anglický	k2eAgInSc1d1	anglický
překlad	překlad	k1gInSc1	překlad
</s>
</p>
<p>
<s>
Zoroastrijské	Zoroastrijský	k2eAgInPc4d1	Zoroastrijský
texty	text	k1gInPc4	text
online	onlinout	k5eAaPmIp3nS	onlinout
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
