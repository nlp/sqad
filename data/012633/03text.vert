<p>
<s>
Suchá	Suchá	k1gFnSc1	Suchá
údolí	údolí	k1gNnSc2	údolí
McMurdo	McMurdo	k1gNnSc1	McMurdo
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
ve	v	k7c6	v
Viktoriině	Viktoriin	k2eAgFnSc6d1	Viktoriina
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
polární	polární	k2eAgFnSc2d1	polární
stanice	stanice	k1gFnSc2	stanice
McMurdo	McMurdo	k1gNnSc1	McMurdo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
antarktická	antarktický	k2eAgFnSc1d1	antarktická
oáza	oáza	k1gFnSc1	oáza
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
zhruba	zhruba	k6eAd1	zhruba
4800	[number]	k4	4800
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
srážkovém	srážkový	k2eAgInSc6d1	srážkový
stínu	stín	k1gInSc6	stín
Transantarktického	Transantarktický	k2eAgNnSc2d1	Transantarktické
pohoří	pohoří	k1gNnSc2	pohoří
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
nejsušší	suchý	k2eAgNnSc4d3	nejsušší
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
zeměkouli	zeměkoule	k1gFnSc6	zeměkoule
<g/>
.	.	kIx.	.
</s>
<s>
Údolí	údolí	k1gNnSc1	údolí
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
Robert	Robert	k1gMnSc1	Robert
Falcon	Falcon	k1gMnSc1	Falcon
Scott	Scott	k1gMnSc1	Scott
<g/>
.	.	kIx.	.
<g/>
Oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
třemi	tři	k4xCgInPc7	tři
rovnoběžnými	rovnoběžný	k2eAgInPc7d1	rovnoběžný
hlavními	hlavní	k2eAgInPc7d1	hlavní
údolími	údolí	k1gNnPc7	údolí
<g/>
:	:	kIx,	:
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
severu	sever	k1gInSc2	sever
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
Victoria	Victorium	k1gNnPc1	Victorium
Valley	Vallea	k1gFnSc2	Vallea
<g/>
,	,	kIx,	,
Wright	Wright	k1gMnSc1	Wright
Valley	Vallea	k1gFnSc2	Vallea
a	a	k8xC	a
Taylor	Taylor	k1gMnSc1	Taylor
Valley	Vallea	k1gFnSc2	Vallea
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nehostinná	hostinný	k2eNgFnSc1d1	nehostinná
poušť	poušť	k1gFnSc1	poušť
bez	bez	k7c2	bez
sněhového	sněhový	k2eAgInSc2d1	sněhový
příkrovu	příkrov	k1gInSc2	příkrov
a	a	k8xC	a
veškeré	veškerý	k3xTgFnSc2	veškerý
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
minimální	minimální	k2eAgFnPc1d1	minimální
(	(	kIx(	(
<g/>
jen	jen	k6eAd1	jen
několik	několik	k4yIc4	několik
milimetrů	milimetr	k1gInPc2	milimetr
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc4	oblast
navíc	navíc	k6eAd1	navíc
vysušují	vysušovat	k5eAaImIp3nP	vysušovat
katabatické	katabatický	k2eAgInPc1d1	katabatický
větry	vítr	k1gInPc1	vítr
dosahující	dosahující	k2eAgFnSc2d1	dosahující
rychlosti	rychlost	k1gFnSc2	rychlost
až	až	k9	až
320	[number]	k4	320
kilometrů	kilometr	k1gInPc2	kilometr
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
výzkumníkem	výzkumník	k1gMnSc7	výzkumník
Suchých	Suchých	k2eAgNnSc2d1	Suchých
údolí	údolí	k1gNnSc2	údolí
je	být	k5eAaImIp3nS	být
John	John	k1gMnSc1	John
Charles	Charles	k1gMnSc1	Charles
Priscu	Prisca	k1gFnSc4	Prisca
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
také	také	k6eAd1	také
jezera	jezero	k1gNnSc2	jezero
Vida	Vida	k?	Vida
<g/>
,	,	kIx,	,
Vanda	Vanda	k1gFnSc1	Vanda
a	a	k8xC	a
Fryxell	Fryxell	k1gInSc1	Fryxell
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
slanější	slaný	k2eAgFnSc1d2	slanější
než	než	k8xS	než
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Wrightovým	Wrightův	k2eAgNnSc7d1	Wrightovo
údolím	údolí	k1gNnSc7	údolí
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Onyx	onyx	k1gInSc1	onyx
<g/>
,	,	kIx,	,
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
32	[number]	k4	32
km	km	kA	km
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
napájena	napájet	k5eAaImNgFnS	napájet
vodou	voda	k1gFnSc7	voda
z	z	k7c2	z
tajících	tající	k2eAgInPc2d1	tající
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Taylorově	Taylorův	k2eAgNnSc6d1	Taylorovo
údolí	údolí	k1gNnSc6	údolí
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
vodopády	vodopád	k1gInPc1	vodopád
Blood	Blooda	k1gFnPc2	Blooda
Falls	Fallsa	k1gFnPc2	Fallsa
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
podle	podle	k7c2	podle
krvavě	krvavě	k6eAd1	krvavě
červeného	červený	k2eAgNnSc2d1	červené
zbarvení	zbarvení	k1gNnSc2	zbarvení
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
oxidy	oxid	k1gInPc1	oxid
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Suchá	suchý	k2eAgNnPc1d1	suché
údolí	údolí	k1gNnPc1	údolí
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
Speciálně	speciálně	k6eAd1	speciálně
řízená	řízený	k2eAgNnPc4d1	řízené
území	území	k1gNnPc4	území
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
vědecký	vědecký	k2eAgInSc4d1	vědecký
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zdejší	zdejší	k2eAgFnPc1d1	zdejší
podmínky	podmínka	k1gFnPc1	podmínka
(	(	kIx(	(
<g/>
teploty	teplota	k1gFnPc1	teplota
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
a	a	k8xC	a
minimální	minimální	k2eAgFnSc1d1	minimální
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
)	)	kIx)	)
připomínají	připomínat	k5eAaImIp3nP	připomínat
krajinu	krajina	k1gFnSc4	krajina
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Biologové	biolog	k1gMnPc1	biolog
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
přežití	přežití	k1gNnSc4	přežití
organismů	organismus	k1gInPc2	organismus
v	v	k7c6	v
extrémním	extrémní	k2eAgNnSc6d1	extrémní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
:	:	kIx,	:
v	v	k7c6	v
údolích	údolí	k1gNnPc6	údolí
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
endolity	endolit	k1gInPc1	endolit
a	a	k8xC	a
sinice	sinice	k1gFnSc1	sinice
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
probíhaly	probíhat	k5eAaImAgFnP	probíhat
na	na	k7c6	na
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc6d1	vesmírná
stanici	stanice	k1gFnSc6	stanice
experimenty	experiment	k1gInPc4	experiment
s	s	k7c7	s
houbami	houba	k1gFnPc7	houba
rodu	rod	k1gInSc2	rod
Cryomyces	Cryomycesa	k1gFnPc2	Cryomycesa
nalezenými	nalezený	k2eAgFnPc7d1	nalezená
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
údolích	údolí	k1gNnPc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vesmírné	vesmírný	k2eAgFnSc6d1	vesmírná
stanici	stanice	k1gFnSc6	stanice
byly	být	k5eAaImAgFnP	být
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
stejné	stejný	k2eAgFnPc1d1	stejná
atmosféře	atmosféra	k1gFnSc6	atmosféra
i	i	k9	i
silnému	silný	k2eAgNnSc3d1	silné
ultrafialovému	ultrafialový	k2eAgNnSc3d1	ultrafialové
záření	záření	k1gNnSc3	záření
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakým	jaký	k3yQgNnSc7	jaký
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
střetly	střetnout	k5eAaPmAgFnP	střetnout
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
testu	test	k1gInSc2	test
přežilo	přežít	k5eAaPmAgNnS	přežít
60	[number]	k4	60
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
houbových	houbový	k2eAgFnPc2d1	houbová
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
desátá	desátý	k4xOgFnSc1	desátý
buňka	buňka	k1gFnSc1	buňka
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
se	se	k3xPyFc4	se
množit	množit	k5eAaImF	množit
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
nové	nový	k2eAgFnPc4d1	nová
kolonie	kolonie	k1gFnPc4	kolonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
McMurdo	McMurdo	k1gNnSc1	McMurdo
Dry	Dry	k1gFnPc2	Dry
Valleys	Valleysa	k1gFnPc2	Valleysa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.mcmurdodryvalleys.aq/home	[url]	k1gInSc5	http://www.mcmurdodryvalleys.aq/home
</s>
</p>
<p>
<s>
https://web.archive.org/web/20101112061732/http://www.antarctica.ac.uk//about_antarctica/geography/rock/dry_valleys.php	[url]	k1gMnSc1	https://web.archive.org/web/20101112061732/http://www.antarctica.ac.uk//about_antarctica/geography/rock/dry_valleys.php
</s>
</p>
