<s>
Suchá	Suchá	k1gFnSc1	Suchá
údolí	údolí	k1gNnSc2	údolí
McMurdo	McMurdo	k1gNnSc1	McMurdo
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
ve	v	k7c6	v
Viktoriině	Viktoriin	k2eAgFnSc6d1	Viktoriina
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
polární	polární	k2eAgFnSc2d1	polární
stanice	stanice	k1gFnSc2	stanice
McMurdo	McMurdo	k1gNnSc1	McMurdo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
antarktická	antarktický	k2eAgFnSc1d1	antarktická
oáza	oáza	k1gFnSc1	oáza
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
zhruba	zhruba	k6eAd1	zhruba
4800	[number]	k4	4800
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
srážkovém	srážkový	k2eAgInSc6d1	srážkový
stínu	stín	k1gInSc6	stín
Transantarktického	Transantarktický	k2eAgNnSc2d1	Transantarktické
pohoří	pohoří	k1gNnSc2	pohoří
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
nejsušší	suchý	k2eAgNnSc4d3	nejsušší
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
zeměkouli	zeměkoule	k1gFnSc6	zeměkoule
<g/>
.	.	kIx.	.
</s>

