<s>
Waffen-SS	Waffen-SS	k?	Waffen-SS
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Zbraně	zbraň	k1gFnSc2	zbraň
SS	SS	kA	SS
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
Ozbrojené	ozbrojený	k2eAgInPc1d1	ozbrojený
SS	SS	kA	SS
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
jednotky	jednotka	k1gFnPc1	jednotka
SS	SS	kA	SS
<g/>
,	,	kIx,	,
určené	určený	k2eAgInPc4d1	určený
k	k	k7c3	k
běžným	běžný	k2eAgFnPc3d1	běžná
vojenským	vojenský	k2eAgFnPc3d1	vojenská
operacím	operace	k1gFnPc3	operace
<g/>
.	.	kIx.	.
</s>
<s>
Představovaly	představovat	k5eAaImAgFnP	představovat
německé	německý	k2eAgFnPc1d1	německá
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
a	a	k8xC	a
cizinecké	cizinecký	k2eAgFnPc1d1	cizinecká
jednotky	jednotka	k1gFnPc1	jednotka
nacistické	nacistický	k2eAgFnSc2d1	nacistická
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
přibližně	přibližně	k6eAd1	přibližně
950	[number]	k4	950
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
vedeny	vést	k5eAaImNgInP	vést
Heinrichem	Heinrich	k1gMnSc7	Heinrich
Himmlerem	Himmler	k1gMnSc7	Himmler
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
vojenský	vojenský	k2eAgInSc1d1	vojenský
tribunál	tribunál	k1gInSc1	tribunál
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Waffen-SS	Waffen-SS	k1gFnSc4	Waffen-SS
za	za	k7c4	za
zločineckou	zločinecký	k2eAgFnSc4d1	zločinecká
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
se	se	k3xPyFc4	se
z	z	k7c2	z
dvousetčlenné	dvousetčlenný	k2eAgFnSc2d1	dvousetčlenná
jednotky	jednotka	k1gFnSc2	jednotka
Hitlerovy	Hitlerův	k2eAgFnSc2d1	Hitlerova
osobní	osobní	k2eAgFnSc2d1	osobní
ochranky	ochranka	k1gFnSc2	ochranka
(	(	kIx(	(
<g/>
die	die	k?	die
SchutzStaffel	SchutzStaffel	k1gInSc1	SchutzStaffel
der	drát	k5eAaImRp2nS	drát
NSDAP	NSDAP	kA	NSDAP
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
zkratka	zkratka	k1gFnSc1	zkratka
SS	SS	kA	SS
<g/>
)	)	kIx)	)
ještě	ještě	k9	ještě
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Hitler	Hitler	k1gMnSc1	Hitler
nebyl	být	k5eNaImAgMnS	být
kancléřem	kancléř	k1gMnSc7	kancléř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
vůdcem	vůdce	k1gMnSc7	vůdce
NSDAP	NSDAP	kA	NSDAP
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
členů	člen	k1gMnPc2	člen
Waffen-SS	Waffen-SS	k1gFnSc1	Waffen-SS
tehdy	tehdy	k6eAd1	tehdy
rekrutovala	rekrutovat	k5eAaImAgFnS	rekrutovat
z	z	k7c2	z
bývalých	bývalý	k2eAgFnPc2d1	bývalá
Freikorps	Freikorpsa	k1gFnPc2	Freikorpsa
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
členové	člen	k1gMnPc1	člen
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
SS-Panzer	SS-Panzer	k1gInSc1	SS-Panzer
Division	Division	k1gInSc1	Division
"	"	kIx"	"
<g/>
Leibstandarte	Leibstandart	k1gInSc5	Leibstandart
SS	SS	kA	SS
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
Himmlerem	Himmler	k1gInSc7	Himmler
na	na	k7c4	na
Hitlerovu	Hitlerův	k2eAgFnSc4d1	Hitlerova
žádost	žádost	k1gFnSc4	žádost
jako	jako	k8xC	jako
Hitlerova	Hitlerův	k2eAgFnSc1d1	Hitlerova
ochranka	ochranka	k1gFnSc1	ochranka
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
Waffen-SS	Waffen-SS	k1gFnPc1	Waffen-SS
nasazovány	nasazovat	k5eAaImNgFnP	nasazovat
do	do	k7c2	do
běžných	běžný	k2eAgInPc2d1	běžný
vojenských	vojenský	k2eAgInPc2d1	vojenský
úkolů	úkol	k1gInPc2	úkol
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
jako	jako	k8xC	jako
elitní	elitní	k2eAgFnPc1d1	elitní
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vykonávali	vykonávat	k5eAaImAgMnP	vykonávat
stráž	stráž	k1gFnSc4	stráž
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
a	a	k8xC	a
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
byli	být	k5eAaImAgMnP	být
jejich	jejich	k3xOp3gMnPc1	jejich
členové	člen	k1gMnPc1	člen
také	také	k9	také
osobní	osobní	k2eAgFnSc7d1	osobní
stráží	stráž	k1gFnSc7	stráž
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
Waffen-SS	Waffen-SS	k1gFnSc2	Waffen-SS
byli	být	k5eAaImAgMnP	být
převážně	převážně	k6eAd1	převážně
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
<g/>
,	,	kIx,	,
prošli	projít	k5eAaPmAgMnP	projít
dobrým	dobrý	k2eAgInSc7d1	dobrý
tělesným	tělesný	k2eAgInSc7d1	tělesný
tréninkem	trénink	k1gInSc7	trénink
<g/>
,	,	kIx,	,
vynikajícím	vynikající	k2eAgInSc7d1	vynikající
vojenským	vojenský	k2eAgInSc7d1	vojenský
výcvikem	výcvik	k1gInSc7	výcvik
a	a	k8xC	a
důkladným	důkladný	k2eAgNnSc7d1	důkladné
ideologickým	ideologický	k2eAgNnSc7d1	ideologické
zpracováním	zpracování	k1gNnSc7	zpracování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mnohdy	mnohdy	k6eAd1	mnohdy
hraničilo	hraničit	k5eAaImAgNnS	hraničit
s	s	k7c7	s
brainwashingem	brainwashing	k1gInSc7	brainwashing
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
zejména	zejména	k9	zejména
specialisty	specialista	k1gMnPc4	specialista
na	na	k7c4	na
boj	boj	k1gInSc4	boj
s	s	k7c7	s
lehkými	lehký	k2eAgFnPc7d1	lehká
palnými	palný	k2eAgFnPc7d1	palná
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
takovémto	takovýto	k3xDgInSc6	takovýto
základním	základní	k2eAgInSc6d1	základní
výcviku	výcvik	k1gInSc6	výcvik
prošli	projít	k5eAaPmAgMnP	projít
případným	případný	k2eAgInSc7d1	případný
speciálním	speciální	k2eAgInSc7d1	speciální
výcvikem	výcvik	k1gInSc7	výcvik
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
ovládání	ovládání	k1gNnSc6	ovládání
tanku	tank	k1gInSc2	tank
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Waffen-SS	Waffen-SS	k?	Waffen-SS
kromě	kromě	k7c2	kromě
daleko	daleko	k6eAd1	daleko
lepšího	dobrý	k2eAgInSc2d2	lepší
výcviku	výcvik	k1gInSc2	výcvik
<g/>
,	,	kIx,	,
než	než	k8xS	než
měla	mít	k5eAaImAgFnS	mít
většina	většina	k1gFnSc1	většina
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
často	často	k6eAd1	často
dostávali	dostávat	k5eAaImAgMnP	dostávat
horší	zlý	k2eAgInSc4d2	horší
výzbroj	výzbroj	k1gInSc4	výzbroj
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
konfiskovanou	konfiskovaný	k2eAgFnSc4d1	konfiskovaná
rakouské	rakouský	k2eAgFnPc1d1	rakouská
armádě	armáda	k1gFnSc3	armáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
Waffen-SS	Waffen-SS	k1gFnSc2	Waffen-SS
panoval	panovat	k5eAaImAgInS	panovat
také	také	k9	také
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
formální	formální	k2eAgInSc4d1	formální
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
vojákům	voják	k1gMnPc3	voják
než	než	k8xS	než
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
jednotkách	jednotka	k1gFnPc6	jednotka
poněkud	poněkud	k6eAd1	poněkud
volnější	volný	k2eAgInSc4d2	volnější
režim	režim	k1gInSc4	režim
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
armádě	armáda	k1gFnSc6	armáda
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
Waffen-SS	Waffen-SS	k1gFnPc1	Waffen-SS
měly	mít	k5eAaImAgFnP	mít
lepší	dobrý	k2eAgInPc4d2	lepší
příděly	příděl	k1gInPc4	příděl
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
lepší	dobrý	k2eAgFnPc4d2	lepší
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
veliteli	velitel	k1gMnPc7	velitel
a	a	k8xC	a
vojáky	voják	k1gMnPc7	voják
panovala	panovat	k5eAaImAgFnS	panovat
spíše	spíše	k9	spíše
kamarádská	kamarádský	k2eAgFnSc1d1	kamarádská
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
Waffen-SS	Waffen-SS	k1gFnSc2	Waffen-SS
například	například	k6eAd1	například
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
nadřízenému	nadřízený	k1gMnSc3	nadřízený
tykat	tykat	k5eAaImF	tykat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
bojové	bojový	k2eAgFnPc4d1	bojová
ztráty	ztráta	k1gFnPc4	ztráta
Waffen-SS	Waffen-SS	k1gMnPc1	Waffen-SS
představovali	představovat	k5eAaImAgMnP	představovat
dva	dva	k4xCgMnPc4	dva
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
příslušníci	příslušník	k1gMnPc1	příslušník
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
SS-Panzer-Division	SS-Panzer-Division	k1gInSc1	SS-Panzer-Division
Totenkopf	Totenkopf	k1gInSc1	Totenkopf
<g/>
,	,	kIx,	,
zabití	zabití	k1gNnSc1	zabití
československými	československý	k2eAgMnPc7d1	československý
vojáky	voják	k1gMnPc7	voják
při	při	k7c6	při
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Czajankových	Czajankův	k2eAgFnPc2d1	Czajankův
kasáren	kasárny	k1gFnPc2	kasárny
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
Waffen-SS	Waffen-SS	k1gFnSc2	Waffen-SS
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgFnP	účastnit
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
invaze	invaze	k1gFnSc2	invaze
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
nasazovány	nasazovat	k5eAaImNgFnP	nasazovat
především	především	k6eAd1	především
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
zejména	zejména	k9	zejména
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
hrály	hrát	k5eAaImAgFnP	hrát
jednotky	jednotka	k1gFnPc1	jednotka
Waffen-SS	Waffen-SS	k1gFnSc2	Waffen-SS
při	při	k7c6	při
dobývání	dobývání	k1gNnSc6	dobývání
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
,	,	kIx,	,
účastnily	účastnit	k5eAaImAgFnP	účastnit
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
a	a	k8xC	a
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
<g/>
)	)	kIx)	)
třetí	třetí	k4xOgFnSc4	třetí
bitvu	bitva	k1gFnSc4	bitva
o	o	k7c4	o
Charkov	Charkov	k1gInSc4	Charkov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
i	i	k8xC	i
operace	operace	k1gFnSc1	operace
Citadela	citadela	k1gFnSc1	citadela
včetně	včetně	k7c2	včetně
bojů	boj	k1gInPc2	boj
poblíž	poblíž	k7c2	poblíž
Prochorovky	Prochorovka	k1gFnSc2	Prochorovka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Waffen-SS	Waffen-SS	k1gFnSc6	Waffen-SS
byl	být	k5eAaImAgInS	být
brán	brát	k5eAaImNgInS	brát
nejprve	nejprve	k6eAd1	nejprve
ohled	ohled	k1gInSc1	ohled
na	na	k7c4	na
"	"	kIx"	"
<g/>
čistotu	čistota	k1gFnSc4	čistota
rasy	rasa	k1gFnSc2	rasa
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nemuseli	muset	k5eNaImAgMnP	muset
sloužit	sloužit	k5eAaImF	sloužit
"	"	kIx"	"
<g/>
rasově	rasově	k6eAd1	rasově
čistí	čistý	k2eAgMnPc1d1	čistý
<g/>
"	"	kIx"	"
Němci	Němec	k1gMnPc1	Němec
jako	jako	k9	jako
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
jednotkách	jednotka	k1gFnPc6	jednotka
SS	SS	kA	SS
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
z	z	k7c2	z
vojáků	voják	k1gMnPc2	voják
Waffen-SS	Waffen-SS	k1gFnSc2	Waffen-SS
dokonce	dokonce	k9	dokonce
tvořili	tvořit	k5eAaImAgMnP	tvořit
kolaborující	kolaborující	k2eAgMnPc1d1	kolaborující
cizinci	cizinec	k1gMnPc1	cizinec
z	z	k7c2	z
podmaněných	podmaněný	k2eAgFnPc2d1	Podmaněná
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
<g />
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
Slováci	Slovák	k1gMnPc1	Slovák
<g/>
,	,	kIx,	,
Dánové	Dán	k1gMnPc1	Dán
<g/>
,	,	kIx,	,
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
Ázerbajdžánci	Ázerbajdžánec	k1gMnPc1	Ázerbajdžánec
<g/>
,	,	kIx,	,
Vlámové	Vlám	k1gMnPc1	Vlám
<g/>
,	,	kIx,	,
Norové	Nor	k1gMnPc1	Nor
<g/>
,	,	kIx,	,
Finové	Fin	k1gMnPc1	Fin
<g/>
,	,	kIx,	,
Holanďané	Holanďan	k1gMnPc1	Holanďan
<g/>
,	,	kIx,	,
Chorvati	Chorvat	k1gMnPc1	Chorvat
<g/>
,	,	kIx,	,
Lotyši	Lotyš	k1gMnPc1	Lotyš
<g/>
,	,	kIx,	,
Estonci	Estonec	k1gMnPc1	Estonec
a	a	k8xC	a
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
Indové	Ind	k1gMnPc1	Ind
a	a	k8xC	a
Britové	Brit	k1gMnPc1	Brit
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
obyvatelé	obyvatel	k1gMnPc1	obyvatel
zemí	zem	k1gFnPc2	zem
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
vojáci	voják	k1gMnPc1	voják
byli	být	k5eAaImAgMnP	být
nazýváni	nazývat	k5eAaImNgMnP	nazývat
Freiwilligen	Freiwilligen	k1gInSc1	Freiwilligen
(	(	kIx(	(
<g/>
Dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
příslušníci	příslušník	k1gMnPc1	příslušník
takto	takto	k6eAd1	takto
sloužili	sloužit	k5eAaImAgMnP	sloužit
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
jednotky	jednotka	k1gFnPc4	jednotka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
<s>
SS-Freiwilligen	SS-Freiwilligen	k1gInSc1	SS-Freiwilligen
Panzergrenadier	Panzergrenadier	k1gInSc1	Panzergrenadier
Division	Division	k1gInSc1	Division
"	"	kIx"	"
<g/>
Wallonien	Wallonien	k1gInSc1	Wallonien
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
či	či	k8xC	či
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
SS-Panzer	SS-Panzer	k1gInSc1	SS-Panzer
Division	Division	k1gInSc1	Division
"	"	kIx"	"
<g/>
Wiking	Wiking	k1gInSc1	Wiking
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
dopustily	dopustit	k5eAaPmAgFnP	dopustit
jednotky	jednotka	k1gFnPc1	jednotka
Waffen-SS	Waffen-SS	k1gFnSc2	Waffen-SS
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgInSc1d3	nejznámější
Malmédský	Malmédský	k2eAgInSc1d1	Malmédský
masakr	masakr	k1gInSc1	masakr
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jednotky	jednotka	k1gFnPc1	jednotka
Waffen-SS	Waffen-SS	k1gFnSc2	Waffen-SS
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
postřílely	postřílet	k5eAaPmAgInP	postřílet
71	[number]	k4	71
amerických	americký	k2eAgMnPc2d1	americký
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
skupiny	skupina	k1gFnPc1	skupina
Dirlewangera	Dirlewangero	k1gNnSc2	Dirlewangero
a	a	k8xC	a
Kaminského	Kaminský	k2eAgNnSc2d1	Kaminský
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
na	na	k7c6	na
dobytém	dobytý	k2eAgNnSc6d1	dobyté
území	území	k1gNnSc6	území
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
zavraždily	zavraždit	k5eAaPmAgInP	zavraždit
mnoho	mnoho	k4c4	mnoho
civilistů	civilista	k1gMnPc2	civilista
a	a	k8xC	a
také	také	k6eAd1	také
ruské	ruský	k2eAgMnPc4d1	ruský
válečné	válečný	k2eAgMnPc4d1	válečný
zajatce	zajatec	k1gMnPc4	zajatec
<g/>
,	,	kIx,	,
vypálily	vypálit	k5eAaPmAgFnP	vypálit
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
,	,	kIx,	,
prováděly	provádět	k5eAaImAgInP	provádět
ty	ten	k3xDgInPc1	ten
nejohavnější	ohavný	k2eAgInPc1d3	nejohavnější
německé	německý	k2eAgInPc1d1	německý
válečné	válečný	k2eAgInPc1d1	válečný
zločiny	zločin	k1gInPc1	zločin
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
také	také	k9	také
pomáhaly	pomáhat	k5eAaImAgFnP	pomáhat
potlačovat	potlačovat	k5eAaImF	potlačovat
odpor	odpor	k1gInSc4	odpor
polských	polský	k2eAgMnPc2d1	polský
Židů	Žid	k1gMnPc2	Žid
ve	v	k7c6	v
varšavském	varšavský	k2eAgNnSc6d1	Varšavské
ghettu	ghetto	k1gNnSc6	ghetto
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
byly	být	k5eAaImAgFnP	být
jednotky	jednotka	k1gFnPc1	jednotka
Waffen	Waffen	k1gInSc4	Waffen
SS	SS	kA	SS
součástí	součást	k1gFnSc7	součást
uskupení	uskupení	k1gNnSc2	uskupení
tzv.	tzv.	kA	tzv.
Einsatzgruppen	Einsatzgruppen	k2eAgMnSc1d1	Einsatzgruppen
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
činností	činnost	k1gFnSc7	činnost
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
očisťování	očisťování	k1gNnSc1	očisťování
<g/>
"	"	kIx"	"
prostoru	prostor	k1gInSc2	prostor
od	od	k7c2	od
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
"	"	kIx"	"
<g/>
méněcenných	méněcenný	k2eAgFnPc2d1	méněcenná
<g/>
"	"	kIx"	"
ras	rasa	k1gFnPc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
území	území	k1gNnSc2	území
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
zabraly	zabrat	k5eAaPmAgFnP	zabrat
Waffen-SS	Waffen-SS	k1gFnSc7	Waffen-SS
území	území	k1gNnSc2	území
65	[number]	k4	65
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
vyhnaly	vyhnat	k5eAaPmAgFnP	vyhnat
a	a	k8xC	a
zřídily	zřídit	k5eAaPmAgFnP	zřídit
zde	zde	k6eAd1	zde
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
příslušníky	příslušník	k1gMnPc4	příslušník
výcvikový	výcvikový	k2eAgInSc4d1	výcvikový
tábor	tábor	k1gInSc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
prostoru	prostor	k1gInSc6	prostor
byla	být	k5eAaImAgFnS	být
i	i	k9	i
pobočka	pobočka	k1gFnSc1	pobočka
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Flossenbürg	Flossenbürg	k1gMnSc1	Flossenbürg
<g/>
.	.	kIx.	.
</s>
<s>
Waffen	Waffen	k1gInSc1	Waffen
SS	SS	kA	SS
měla	mít	k5eAaImAgFnS	mít
svou	svůj	k3xOyFgFnSc4	svůj
pochodovou	pochodový	k2eAgFnSc4d1	pochodová
hymnu	hymna	k1gFnSc4	hymna
SS	SS	kA	SS
marschiert	marschiert	k1gInSc1	marschiert
in	in	k?	in
Feidesland	Feidesland	k1gInSc1	Feidesland
<g/>
.	.	kIx.	.
</s>
<s>
SS	SS	kA	SS
marchiert	marchiert	k1gMnSc1	marchiert
in	in	k?	in
Feidesland	Feideslando	k1gNnPc2	Feideslando
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
estonskou	estonský	k2eAgFnSc4d1	Estonská
<g/>
,	,	kIx,	,
norskou	norský	k2eAgFnSc4d1	norská
<g/>
,	,	kIx,	,
lotyšskou	lotyšský	k2eAgFnSc4d1	lotyšská
<g/>
,	,	kIx,	,
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
a	a	k8xC	a
brazilskou	brazilský	k2eAgFnSc4d1	brazilská
verzi	verze	k1gFnSc4	verze
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
písně	píseň	k1gFnPc1	píseň
se	se	k3xPyFc4	se
často	často	k6eAd1	často
lišily	lišit	k5eAaImAgFnP	lišit
od	od	k7c2	od
německého	německý	k2eAgInSc2d1	německý
originálu	originál	k1gInSc2	originál
<g/>
.	.	kIx.	.
</s>
<s>
Heinz	Heinz	k1gMnSc1	Heinz
Höhne	Höhn	k1gInSc5	Höhn
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
originální	originální	k2eAgFnSc6d1	originální
práci	práce	k1gFnSc6	práce
Der	drát	k5eAaImRp2nS	drát
Orden	Orden	k1gInSc1	Orden
Totenkopf	Totenkopf	k1gInSc4	Totenkopf
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	se	k3xPyFc4	se
Bezpečnostní	bezpečnostní	k2eAgFnSc7d1	bezpečnostní
službou	služba	k1gFnSc7	služba
SS	SS	kA	SS
<g/>
,	,	kIx,	,
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1942	[number]	k4	1942
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
lze	lze	k6eAd1	lze
prohlásit	prohlásit	k5eAaPmF	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
svými	svůj	k3xOyFgInPc7	svůj
úspěchy	úspěch	k1gInPc7	úspěch
si	se	k3xPyFc3	se
Waffen	Waffen	k2eAgInSc4d1	Waffen
SS	SS	kA	SS
získaly	získat	k5eAaPmAgFnP	získat
úctu	úcta	k1gFnSc4	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
je	být	k5eAaImIp3nS	být
ceněno	ceněn	k2eAgNnSc4d1	ceněno
kamarádství	kamarádství	k1gNnSc4	kamarádství
a	a	k8xC	a
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
důstojníky	důstojník	k1gMnPc7	důstojník
<g/>
,	,	kIx,	,
poddůstojníky	poddůstojník	k1gMnPc7	poddůstojník
a	a	k8xC	a
mužstvem	mužstvo	k1gNnSc7	mužstvo
<g/>
...	...	k?	...
Bohužel	bohužel	k9	bohužel
nelze	lze	k6eNd1	lze
přeslechnout	přeslechnout	k5eAaPmF	přeslechnout
hlasy	hlas	k1gInPc4	hlas
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Waffen	Waffna	k1gFnPc2	Waffna
postrádají	postrádat	k5eAaImIp3nP	postrádat
zkušené	zkušený	k2eAgMnPc4d1	zkušený
důstojníky	důstojník	k1gMnPc4	důstojník
a	a	k8xC	a
vojáci	voják	k1gMnPc1	voják
SS	SS	kA	SS
jsou	být	k5eAaImIp3nP	být
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
lehkomyslně	lehkomyslně	k6eAd1	lehkomyslně
obětováni	obětovat	k5eAaBmNgMnP	obětovat
<g/>
...	...	k?	...
Kritické	kritický	k2eAgInPc1d1	kritický
hlasy	hlas	k1gInPc1	hlas
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Waffen	Waffen	k1gInSc4	Waffen
SS	SS	kA	SS
jsou	být	k5eAaImIp3nP	být
jistým	jistý	k2eAgInSc7d1	jistý
druhem	druh	k1gInSc7	druh
vojenských	vojenský	k2eAgMnPc2d1	vojenský
hlídacích	hlídací	k2eAgMnPc2d1	hlídací
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
SS	SS	kA	SS
jsou	být	k5eAaImIp3nP	být
cvičeni	cvičit	k5eAaImNgMnP	cvičit
k	k	k7c3	k
brutalitě	brutalita	k1gFnSc3	brutalita
a	a	k8xC	a
bezohlednosti	bezohlednost	k1gFnSc3	bezohlednost
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nutnosti	nutnost	k1gFnSc2	nutnost
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
proti	proti	k7c3	proti
jiným	jiný	k2eAgFnPc3d1	jiná
německým	německý	k2eAgFnPc3d1	německá
formacím	formace	k1gFnPc3	formace
<g/>
...	...	k?	...
Waffen	Waffen	k1gInSc4	Waffen
SS	SS	kA	SS
jsou	být	k5eAaImIp3nP	být
tou	ten	k3xDgFnSc7	ten
nejbezohlednější	bezohledný	k2eAgFnSc7d3	nejbezohlednější
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
SS-Panzer-Division	SS-Panzer-Division	k1gInSc1	SS-Panzer-Division
Leibstandarte	Leibstandart	k1gInSc5	Leibstandart
SS	SS	kA	SS
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
1	[number]	k4	1
<g/>
.	.	kIx.	.
tanková	tankový	k2eAgFnSc1d1	tanková
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Leibstandarte	Leibstandart	k1gInSc5	Leibstandart
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
u	u	k7c2	u
Wormhoudtu	Wormhoudt	k1gInSc2	Wormhoudt
zmasakrovala	zmasakrovat	k5eAaPmAgFnS	zmasakrovat
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
SS-Brigadeführera	SS-Brigadeführer	k1gMnSc2	SS-Brigadeführer
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Mohnkeho	Mohnke	k1gMnSc2	Mohnke
80	[number]	k4	80
britských	britský	k2eAgMnPc2d1	britský
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Působila	působit	k5eAaImAgFnS	působit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
na	na	k7c4	na
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnPc1	její
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
střetly	střetnout	k5eAaPmAgFnP	střetnout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Sokolova	Sokolov	k1gInSc2	Sokolov
s	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armádním	armádní	k2eAgInSc7d1	armádní
sborem	sbor	k1gInSc7	sbor
<g/>
.	.	kIx.	.
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1943	[number]	k4	1943
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
bitvy	bitva	k1gFnPc4	bitva
u	u	k7c2	u
Kurska	Kursk	k1gInSc2	Kursk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
těžké	těžký	k2eAgFnPc4d1	těžká
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
páchala	páchat	k5eAaImAgFnS	páchat
masakry	masakr	k1gInPc7	masakr
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1943	[number]	k4	1943
převelena	převelet	k5eAaPmNgFnS	převelet
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
bojovala	bojovat	k5eAaImAgFnS	bojovat
s	s	k7c7	s
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
do	do	k7c2	do
března	březen	k1gInSc2	březen
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
prakticky	prakticky	k6eAd1	prakticky
zlikvidována	zlikvidován	k2eAgFnSc1d1	zlikvidována
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
na	na	k7c4	na
západ	západ	k1gInSc4	západ
do	do	k7c2	do
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
a	a	k8xC	a
určena	určit	k5eAaPmNgFnS	určit
k	k	k7c3	k
odražení	odražení	k1gNnSc3	odražení
spojenecké	spojenecký	k2eAgFnSc2d1	spojenecká
invaze	invaze	k1gFnSc2	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1944	[number]	k4	1944
provedla	provést	k5eAaPmAgFnS	provést
jednotka	jednotka	k1gFnSc1	jednotka
Kampfgruppe	Kampfgrupp	k1gInSc5	Kampfgrupp
Peiper	Peipero	k1gNnPc2	Peipero
tzv.	tzv.	kA	tzv.
Malmédský	Malmédský	k2eAgInSc4d1	Malmédský
masakr	masakr	k1gInSc4	masakr
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
postřílela	postřílet	k5eAaPmAgFnS	postřílet
71	[number]	k4	71
amerických	americký	k2eAgMnPc2d1	americký
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
SS-Panzer-Division	SS-Panzer-Division	k1gInSc1	SS-Panzer-Division
Das	Das	k1gFnSc2	Das
Reich	Reich	k?	Reich
2	[number]	k4	2
<g/>
.	.	kIx.	.
tanková	tankový	k2eAgFnSc1d1	tanková
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Das	Das	k1gFnPc1	Das
Reich	Reich	k?	Reich
-	-	kIx~	-
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
působila	působit	k5eAaImAgFnS	působit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1941	[number]	k4	1941
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
<g/>
,	,	kIx,	,
účastnila	účastnit	k5eAaImAgFnS	účastnit
se	se	k3xPyFc4	se
německé	německý	k2eAgFnSc2d1	německá
invaze	invaze	k1gFnSc2	invaze
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Utrpěla	utrpět	k5eAaPmAgFnS	utrpět
velké	velký	k2eAgFnPc4d1	velká
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
k	k	k7c3	k
odpočinku	odpočinek	k1gInSc3	odpočinek
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
bojů	boj	k1gInPc2	boj
u	u	k7c2	u
Charkova	Charkov	k1gInSc2	Charkov
<g/>
,	,	kIx,	,
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
provedla	provést	k5eAaPmAgFnS	provést
několik	několik	k4yIc4	několik
masakrů	masakr	k1gInPc2	masakr
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
jako	jako	k8xC	jako
odplatu	odplata	k1gFnSc4	odplata
za	za	k7c4	za
útok	útok	k1gInSc4	útok
partyzánů	partyzán	k1gMnPc2	partyzán
oběsili	oběsit	k5eAaPmAgMnP	oběsit
její	její	k3xOp3gMnPc1	její
příslušníci	příslušník	k1gMnPc1	příslušník
99	[number]	k4	99
civilistů	civilista	k1gMnPc2	civilista
v	v	k7c6	v
Tulle	Tulla	k1gFnSc6	Tulla
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
provedla	provést	k5eAaPmAgFnS	provést
masakr	masakr	k1gInSc4	masakr
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Oradour-sur-Glane	Oradourur-Glan	k1gMnSc5	Oradour-sur-Glan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
brutálním	brutální	k2eAgInSc7d1	brutální
způsobem	způsob	k1gInSc7	způsob
zavražděno	zavražděn	k2eAgNnSc4d1	zavražděno
642	[number]	k4	642
civilistů	civilista	k1gMnPc2	civilista
včetně	včetně	k7c2	včetně
207	[number]	k4	207
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
SS-Panzer-Division	SS-Panzer-Division	k1gInSc1	SS-Panzer-Division
Totenkopf	Totenkopf	k1gInSc1	Totenkopf
3	[number]	k4	3
<g/>
.	.	kIx.	.
tanková	tankový	k2eAgFnSc1d1	tanková
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Totenkopf	Totenkopf	k1gInSc1	Totenkopf
-	-	kIx~	-
základem	základ	k1gInSc7	základ
této	tento	k3xDgFnSc2	tento
divize	divize	k1gFnSc2	divize
byly	být	k5eAaImAgFnP	být
jednotky	jednotka	k1gFnPc1	jednotka
SS-Totenkopfverbände	SS-Totenkopfverbänd	k1gMnSc5	SS-Totenkopfverbänd
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
prosluly	proslout	k5eAaPmAgFnP	proslout
vyvražděním	vyvraždění	k1gNnSc7	vyvraždění
polské	polský	k2eAgFnSc2d1	polská
elity	elita	k1gFnSc2	elita
a	a	k8xC	a
likvidací	likvidace	k1gFnSc7	likvidace
polských	polský	k2eAgMnPc2d1	polský
Židů	Žid	k1gMnPc2	Žid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
provedly	provést	k5eAaPmAgFnP	provést
příslušníci	příslušník	k1gMnPc1	příslušník
divize	divize	k1gFnSc2	divize
SS	SS	kA	SS
Totenkopf	Totenkopf	k1gInSc1	Totenkopf
masakr	masakr	k1gInSc1	masakr
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
vesnici	vesnice	k1gFnSc6	vesnice
Le	Le	k1gFnSc2	Le
Paradis	Paradis	k1gFnSc2	Paradis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
postříleli	postřílet	k5eAaPmAgMnP	postřílet
přes	přes	k7c4	přes
sto	sto	k4xCgNnSc4	sto
britských	britský	k2eAgMnPc2d1	britský
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
vzdali	vzdát	k5eAaPmAgMnP	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
operaci	operace	k1gFnSc6	operace
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
útočila	útočit	k5eAaImAgFnS	útočit
divize	divize	k1gFnSc1	divize
z	z	k7c2	z
východního	východní	k2eAgNnSc2d1	východní
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Utrpěla	utrpět	k5eAaPmAgFnS	utrpět
velké	velký	k2eAgFnPc4d1	velká
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
byla	být	k5eAaImAgFnS	být
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
byla	být	k5eAaImAgFnS	být
převelena	převelet	k5eAaPmNgFnS	převelet
na	na	k7c4	na
východní	východní	k2eAgFnSc4d1	východní
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
její	její	k3xOp3gFnPc1	její
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
střetly	střetnout	k5eAaPmAgFnP	střetnout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Sokolova	Sokolov	k1gInSc2	Sokolov
s	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armádním	armádní	k2eAgInSc7d1	armádní
sborem	sbor	k1gInSc7	sbor
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
její	její	k3xOp3gFnPc1	její
některé	některý	k3yIgFnPc1	některý
jednotky	jednotka	k1gFnPc1	jednotka
bojovaly	bojovat	k5eAaImAgFnP	bojovat
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
proti	proti	k7c3	proti
partyzánům	partyzán	k1gMnPc3	partyzán
<g/>
,	,	kIx,	,
divize	divize	k1gFnSc1	divize
Totenkopf	Totenkopf	k1gInSc1	Totenkopf
zůstala	zůstat	k5eAaPmAgFnS	zůstat
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
SS-Polizei-Panzergrenadier-Division	SS-Polizei-Panzergrenadier-Division	k1gInSc1	SS-Polizei-Panzergrenadier-Division
4	[number]	k4	4
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
pancéřových	pancéřový	k2eAgMnPc2d1	pancéřový
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
Polizei	Polize	k1gFnSc6	Polize
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
zmasakrovala	zmasakrovat	k5eAaPmAgFnS	zmasakrovat
300	[number]	k4	300
obyvatel	obyvatel	k1gMnPc2	obyvatel
řecké	řecký	k2eAgFnSc2d1	řecká
vesnice	vesnice	k1gFnSc2	vesnice
Distomo	Distoma	k1gFnSc5	Distoma
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
SS-Panzer-Division	SS-Panzer-Division	k1gInSc1	SS-Panzer-Division
Wiking	Wiking	k1gInSc1	Wiking
5	[number]	k4	5
<g/>
.	.	kIx.	.
tanková	tankový	k2eAgFnSc1d1	tanková
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Wiking	Wiking	k1gInSc4	Wiking
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
SS-Gebirgs-Division	SS-Gebirgs-Division	k1gInSc1	SS-Gebirgs-Division
Nord	Nord	k1gInSc1	Nord
6	[number]	k4	6
<g/>
.	.	kIx.	.
horská	horský	k2eAgFnSc1d1	horská
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Nord	Nord	k1gInSc4	Nord
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
SS-Freiwilligen-Gebirgs-Division	SS-Freiwilligen-Gebirgs-Division	k1gInSc1	SS-Freiwilligen-Gebirgs-Division
Prinz	Prinza	k1gFnPc2	Prinza
Eugen	Eugno	k1gNnPc2	Eugno
7	[number]	k4	7
<g/>
.	.	kIx.	.
dobrovolnická	dobrovolnický	k2eAgFnSc1d1	dobrovolnická
horská	horský	k2eAgFnSc1d1	horská
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Prinz	Prinz	k1gInSc1	Prinz
Eugen	Eugen	k1gInSc1	Eugen
-	-	kIx~	-
bojovala	bojovat	k5eAaImAgFnS	bojovat
proti	proti	k7c3	proti
partyzánům	partyzán	k1gMnPc3	partyzán
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
SS-Kavallerie-Division	SS-Kavallerie-Division	k1gInSc1	SS-Kavallerie-Division
Florian	Florian	k1gMnSc1	Florian
Geyer	Geyer	k1gMnSc1	Geyer
8	[number]	k4	8
<g/>
.	.	kIx.	.
jízdní	jízdní	k2eAgFnSc2d1	jízdní
divize	divize	k1gFnSc2	divize
SS	SS	kA	SS
Florian	Florian	k1gMnSc1	Florian
Geyer	Geyer	k1gMnSc1	Geyer
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
SS-Panzer-Division	SS-Panzer-Division	k1gInSc1	SS-Panzer-Division
Hohenstaufen	Hohenstaufen	k1gInSc1	Hohenstaufen
9	[number]	k4	9
<g/>
.	.	kIx.	.
tanková	tankový	k2eAgFnSc1d1	tanková
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Hohenstaufen	Hohenstaufen	k1gInSc4	Hohenstaufen
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
SS-Panzer-Division	SS-Panzer-Division	k1gInSc1	SS-Panzer-Division
Frundsberg	Frundsberg	k1gInSc1	Frundsberg
10	[number]	k4	10
<g/>
.	.	kIx.	.
tanková	tankový	k2eAgFnSc1d1	tanková
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Frundsberg	Frundsberg	k1gInSc4	Frundsberg
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
SS	SS	kA	SS
Freiwilligen-Panzergrenadier-Division	Freiwilligen-Panzergrenadier-Division	k1gInSc1	Freiwilligen-Panzergrenadier-Division
Nordland	Nordland	k1gInSc1	Nordland
11	[number]	k4	11
<g/>
.	.	kIx.	.
dobrovolnická	dobrovolnický	k2eAgFnSc1d1	dobrovolnická
divize	divize	k1gFnSc1	divize
pancéřových	pancéřový	k2eAgMnPc2d1	pancéřový
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
Nordland	Nordland	k1gInSc4	Nordland
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
SS-Panzer-Division	SS-Panzer-Division	k1gInSc1	SS-Panzer-Division
Hitlerjugend	Hitlerjugend	k1gInSc1	Hitlerjugend
12	[number]	k4	12
<g/>
.	.	kIx.	.
tanková	tankový	k2eAgFnSc1d1	tanková
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Hitlerjugend	Hitlerjugend	k1gInSc4	Hitlerjugend
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
Waffen-Gebirgs-Division	Waffen-Gebirgs-Division	k1gInSc1	Waffen-Gebirgs-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
Handschar	Handschar	k1gInSc1	Handschar
(	(	kIx(	(
<g/>
kroatische	kroatischat	k5eAaPmIp3nS	kroatischat
Nr	Nr	k1gMnSc1	Nr
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
horská	horský	k2eAgFnSc1d1	horská
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Handschar	Handschara	k1gFnPc2	Handschara
(	(	kIx(	(
<g/>
chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
č.	č.	k?	č.
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
-	-	kIx~	-
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
z	z	k7c2	z
chorvatských	chorvatský	k2eAgMnPc2d1	chorvatský
muslimů	muslim	k1gMnPc2	muslim
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
protipartyzánského	protipartyzánský	k2eAgInSc2d1	protipartyzánský
boje	boj	k1gInSc2	boj
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Waffen-Grenadier-Division	Waffen-Grenadier-Division	k1gInSc1	Waffen-Grenadier-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
(	(	kIx(	(
<g/>
ukrainische	ukrainisch	k1gMnSc2	ukrainisch
Nr	Nr	k1gMnSc2	Nr
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
)	)	kIx)	)
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
divize	divize	k1gFnSc1	divize
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
(	(	kIx(	(
<g/>
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
č.	č.	k?	č.
1	[number]	k4	1
<g/>
)	)	kIx)	)
-mimo	imo	k6eAd1	-mimo
jiné	jiný	k2eAgInPc4d1	jiný
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgNnP	účastnit
potlačování	potlačování	k1gNnPc1	potlačování
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
povstání	povstání	k1gNnSc2	povstání
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Waffen-Grenadier-Division	Waffen-Grenadier-Division	k1gInSc1	Waffen-Grenadier-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
(	(	kIx(	(
<g/>
lettische	lettisch	k1gMnSc2	lettisch
Nr	Nr	k1gMnSc2	Nr
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
(	(	kIx(	(
<g/>
lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
č.	č.	k?	č.
1	[number]	k4	1
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
SS-Panzergrenadier-Division	SS-Panzergrenadier-Division	k1gInSc1	SS-Panzergrenadier-Division
Reichsführer-SS	Reichsführer-SS	k1gFnSc2	Reichsführer-SS
16	[number]	k4	16
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
pancéřových	pancéřový	k2eAgMnPc2d1	pancéřový
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
Reichsführer-SS	Reichsführer-SS	k1gFnSc1	Reichsführer-SS
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
provedla	provést	k5eAaPmAgFnS	provést
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
vesnici	vesnice	k1gFnSc6	vesnice
Sant	Sant	k1gInSc1	Sant
<g/>
'	'	kIx"	'
<g/>
Anna	Anna	k1gFnSc1	Anna
di	di	k?	di
Stazzema	Stazzema	k1gFnSc1	Stazzema
masakr	masakr	k1gInSc1	masakr
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
bylo	být	k5eAaImAgNnS	být
povražděno	povraždit	k5eAaPmNgNnS	povraždit
kolem	kolem	k7c2	kolem
560	[number]	k4	560
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
žen	žena	k1gFnPc2	žena
i	i	k8xC	i
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jako	jako	k9	jako
odvetu	odveta	k1gFnSc4	odveta
za	za	k7c4	za
činnost	činnost	k1gFnSc4	činnost
partyzánů	partyzán	k1gMnPc2	partyzán
provedla	provést	k5eAaPmAgFnS	provést
ve	v	k7c6	v
dnech	den	k1gInPc6	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
až	až	k6eAd1	až
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
Marzabottu	Marzabott	k1gInSc6	Marzabott
a	a	k8xC	a
okolních	okolní	k2eAgFnPc6d1	okolní
obcích	obec	k1gFnPc6	obec
masakr	masakr	k1gInSc4	masakr
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
bylo	být	k5eAaImAgNnS	být
povražděno	povraždit	k5eAaPmNgNnS	povraždit
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
civilních	civilní	k2eAgFnPc2d1	civilní
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
mluví	mluvit	k5eAaImIp3nP	mluvit
o	o	k7c4	o
700	[number]	k4	700
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnSc6d1	jiná
o	o	k7c4	o
955	[number]	k4	955
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
bylo	být	k5eAaImAgNnS	být
zmasakrováno	zmasakrovat	k5eAaPmNgNnS	zmasakrovat
250	[number]	k4	250
dětí	dítě	k1gFnPc2	dítě
včetně	včetně	k7c2	včetně
kojenců	kojenec	k1gMnPc2	kojenec
<g/>
,	,	kIx,	,
316	[number]	k4	316
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
142	[number]	k4	142
osob	osoba	k1gFnPc2	osoba
starších	starší	k1gMnPc2	starší
60	[number]	k4	60
let	léto	k1gNnPc2	léto
a	a	k8xC	a
5	[number]	k4	5
kněží	kněz	k1gMnPc2	kněz
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
SS-Panzergrenadier-Division	SS-Panzergrenadier-Division	k1gInSc1	SS-Panzergrenadier-Division
Götz	Götz	k1gInSc1	Götz
von	von	k1gInSc1	von
Berlichingen	Berlichingen	k1gInSc1	Berlichingen
17	[number]	k4	17
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
pancéřových	pancéřový	k2eAgMnPc2d1	pancéřový
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
Götz	Götz	k1gInSc4	Götz
von	von	k1gInSc1	von
Berlichingen	Berlichingen	k1gInSc1	Berlichingen
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
SS-Freiwilligen	SS-Freiwilligen	k1gInSc1	SS-Freiwilligen
Panzergrenadier	Panzergrenadier	k1gInSc1	Panzergrenadier
Division	Division	k1gInSc1	Division
"	"	kIx"	"
<g/>
Horst	Horst	k1gMnSc1	Horst
Wessel	Wessel	k1gMnSc1	Wessel
<g/>
"	"	kIx"	"
18	[number]	k4	18
<g/>
.	.	kIx.	.
dobrovolnická	dobrovolnický	k2eAgFnSc1d1	dobrovolnická
divize	divize	k1gFnSc1	divize
pancéřových	pancéřový	k2eAgMnPc2d1	pancéřový
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
Horst	Horst	k1gMnSc1	Horst
Wessel	Wessel	k1gMnSc1	Wessel
-	-	kIx~	-
část	část	k1gFnSc1	část
divize	divize	k1gFnSc1	divize
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
nasazena	nasadit	k5eAaPmNgNnP	nasadit
proti	proti	k7c3	proti
slovenským	slovenský	k2eAgMnPc3d1	slovenský
povstalcům	povstalec	k1gMnPc3	povstalec
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Waffen-Grenadier-Division	Waffen-Grenadier-Division	k1gInSc1	Waffen-Grenadier-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
(	(	kIx(	(
<g/>
lettische	lettisch	k1gMnSc2	lettisch
Nr	Nr	k1gMnSc2	Nr
<g/>
.2	.2	k4	.2
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
(	(	kIx(	(
<g/>
lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
č.	č.	k?	č.
2	[number]	k4	2
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Waffen-Grenadier-Division	Waffen-Grenadier-Division	k1gInSc1	Waffen-Grenadier-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
(	(	kIx(	(
<g/>
estonische	estonisch	k1gMnSc2	estonisch
Nr	Nr	k1gMnSc2	Nr
<g/>
.1	.1	k4	.1
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
(	(	kIx(	(
<g/>
estonská	estonský	k2eAgFnSc1d1	Estonská
č.	č.	k?	č.
1	[number]	k4	1
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Waffen-Gebirgs-Division	Waffen-Gebirgs-Division	k1gInSc1	Waffen-Gebirgs-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
Skanderbeg	Skanderbega	k1gFnPc2	Skanderbega
(	(	kIx(	(
<g/>
albanische	albanische	k1gFnSc1	albanische
Nr	Nr	k1gFnSc1	Nr
<g/>
.1	.1	k4	.1
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
horská	horský	k2eAgFnSc1d1	horská
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Skanderbeg	Skanderbega	k1gFnPc2	Skanderbega
(	(	kIx(	(
<g/>
albánská	albánský	k2eAgFnSc1d1	albánská
č.	č.	k?	č.
1	[number]	k4	1
<g/>
)	)	kIx)	)
-	-	kIx~	-
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
ji	on	k3xPp3gFnSc4	on
etničtí	etnický	k2eAgMnPc1d1	etnický
Albánci	Albánec	k1gMnPc1	Albánec
z	z	k7c2	z
Kosova	Kosův	k2eAgMnSc2d1	Kosův
22	[number]	k4	22
<g/>
.	.	kIx.	.
<g/>
SS-Freiwilligen-Kavallerie-Division	SS-Freiwilligen-Kavallerie-Division	k1gInSc1	SS-Freiwilligen-Kavallerie-Division
Maria	Maria	k1gFnSc1	Maria
Theresa	Theresa	k1gFnSc1	Theresa
22	[number]	k4	22
<g/>
.	.	kIx.	.
dobrovolnická	dobrovolnický	k2eAgFnSc1d1	dobrovolnická
jízdní	jízdní	k2eAgFnSc1d1	jízdní
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Maria	Maria	k1gFnSc1	Maria
Theresa	Theresa	k1gFnSc1	Theresa
-	-	kIx~	-
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
sloužili	sloužit	k5eAaImAgMnP	sloužit
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
maďarští	maďarský	k2eAgMnPc1d1	maďarský
etničtí	etnický	k2eAgMnPc1d1	etnický
Němci	Němec	k1gMnPc1	Němec
(	(	kIx(	(
<g/>
Volksdeutsche	Volksdeutsche	k1gInSc1	Volksdeutsche
<g/>
)	)	kIx)	)
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
Waffen-Gebirgs-Division	Waffen-Gebirgs-Division	k1gInSc1	Waffen-Gebirgs-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
Kama	Kama	k1gFnSc1	Kama
(	(	kIx(	(
<g/>
kroatische	kroatische	k1gFnSc1	kroatische
Nr	Nr	k1gFnSc1	Nr
<g/>
.2	.2	k4	.2
<g/>
)	)	kIx)	)
23	[number]	k4	23
<g/>
.	.	kIx.	.
horská	horský	k2eAgFnSc1d1	horská
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Kama	Kama	k1gFnSc1	Kama
(	(	kIx(	(
<g/>
chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
č.	č.	k?	č.
2	[number]	k4	2
<g/>
)	)	kIx)	)
-	-	kIx~	-
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
z	z	k7c2	z
německých	německý	k2eAgMnPc2d1	německý
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
etnických	etnický	k2eAgMnPc2d1	etnický
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
bosenských	bosenský	k2eAgMnPc2d1	bosenský
muslimů	muslim	k1gMnPc2	muslim
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
rozpuštěna	rozpustit	k5eAaPmNgFnS	rozpustit
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
a	a	k8xC	a
zbytky	zbytek	k1gInPc1	zbytek
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
součástí	součást	k1gFnPc2	součást
nové	nový	k2eAgInPc1d1	nový
23	[number]	k4	23
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
SS	SS	kA	SS
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
SS-Freiwilligen-Panzergrenadier-Division	SS-Freiwilligen-Panzergrenadier-Division	k1gInSc1	SS-Freiwilligen-Panzergrenadier-Division
Nederland	Nederland	k1gInSc1	Nederland
(	(	kIx(	(
<g/>
niederlandische	niederlandische	k1gFnSc1	niederlandische
Nr	Nr	k1gFnSc1	Nr
<g/>
.1	.1	k4	.1
<g/>
)	)	kIx)	)
23	[number]	k4	23
<g/>
.	.	kIx.	.
dobrovolnická	dobrovolnický	k2eAgFnSc1d1	dobrovolnická
divize	divize	k1gFnSc1	divize
pancéřových	pancéřový	k2eAgMnPc2d1	pancéřový
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
Nederland	Nederland	k1gInSc4	Nederland
(	(	kIx(	(
<g/>
holandská	holandský	k2eAgFnSc1d1	holandská
č.	č.	k?	č.
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Waffen-Gebirgs-Division	Waffen-Gebirgs-Division	k1gInSc1	Waffen-Gebirgs-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
Karstjäger	Karstjäger	k1gInSc4	Karstjäger
24	[number]	k4	24
<g/>
.	.	kIx.	.
horská	horský	k2eAgFnSc1d1	horská
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Karstjäger	Karstjäger	k1gInSc4	Karstjäger
25	[number]	k4	25
<g/>
.	.	kIx.	.
<g/>
Waffen-Grenadier-Division	Waffen-Grenadier-Division	k1gInSc1	Waffen-Grenadier-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
Hunyadi	Hunyad	k1gMnPc1	Hunyad
(	(	kIx(	(
<g/>
ungarische	ungarisch	k1gInPc1	ungarisch
Nr	Nr	k1gFnSc1	Nr
<g/>
.1	.1	k4	.1
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
Hunyadi	Hunyad	k1gMnPc1	Hunyad
(	(	kIx(	(
<g/>
maďarská	maďarský	k2eAgFnSc1d1	maďarská
č.	č.	k?	č.
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
Waffen-Grenadier-Division	Waffen-Grenadier-Division	k1gInSc1	Waffen-Grenadier-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
Hungaria	Hungarium	k1gNnSc2	Hungarium
(	(	kIx(	(
<g/>
ungarische	ungarischat	k5eAaPmIp3nS	ungarischat
Nr	Nr	k1gFnSc1	Nr
<g/>
.2	.2	k4	.2
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
Hungaria	Hungarium	k1gNnSc2	Hungarium
(	(	kIx(	(
<g/>
maďarská	maďarský	k2eAgFnSc1d1	maďarská
č.	č.	k?	č.
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
</s>
<s>
SS-Freiwilligen-Grenadier-Division	SS-Freiwilligen-Grenadier-Division	k1gInSc1	SS-Freiwilligen-Grenadier-Division
Langemarck	Langemarck	k1gInSc1	Langemarck
(	(	kIx(	(
<g/>
flämische	flämische	k1gFnSc1	flämische
Nr	Nr	k1gFnSc1	Nr
<g/>
.1	.1	k4	.1
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
dobrovolnická	dobrovolnický	k2eAgFnSc1d1	dobrovolnická
divize	divize	k1gFnSc1	divize
pancéřových	pancéřový	k2eAgMnPc2d1	pancéřový
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
Langemarck	Langemarck	k1gInSc4	Langemarck
(	(	kIx(	(
<g/>
vlámská	vlámský	k2eAgFnSc1d1	vlámská
č.	č.	k?	č.
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
<s>
SS-Freiwilligen-Grenadier-Division	SS-Freiwilligen-Grenadier-Division	k1gInSc1	SS-Freiwilligen-Grenadier-Division
Wallonien	Wallonien	k1gInSc1	Wallonien
28	[number]	k4	28
<g/>
.	.	kIx.	.
<g/>
dobrovolnická	dobrovolnický	k2eAgFnSc1d1	dobrovolnická
divize	divize	k1gFnSc1	divize
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
Wallonien	Wallonien	k1gInSc4	Wallonien
(	(	kIx(	(
<g/>
valonská	valonský	k2eAgFnSc1d1	valonská
<g/>
)	)	kIx)	)
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
<s>
Waffen-Grenadier-Division	Waffen-Grenadier-Division	k1gInSc1	Waffen-Grenadier-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
(	(	kIx(	(
<g/>
russische	russisch	k1gMnSc2	russisch
Nr	Nr	k1gMnSc2	Nr
<g/>
.1	.1	k4	.1
<g/>
)	)	kIx)	)
29	[number]	k4	29
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
(	(	kIx(	(
<g/>
ruská	ruský	k2eAgFnSc1d1	ruská
č.	č.	k?	č.
1	[number]	k4	1
<g/>
)	)	kIx)	)
-	-	kIx~	-
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
v	v	k7c6	v
Brjansku	Brjansko	k1gNnSc6	Brjansko
jako	jako	k8xS	jako
protikomunistická	protikomunistický	k2eAgFnSc1d1	protikomunistická
občanská	občanský	k2eAgFnSc1d1	občanská
milice	milice	k1gFnSc1	milice
<g/>
.	.	kIx.	.
</s>
<s>
Prováděla	provádět	k5eAaImAgFnS	provádět
protipartyzánské	protipartyzánský	k2eAgFnPc4d1	protipartyzánská
akce	akce	k1gFnPc4	akce
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
a	a	k8xC	a
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
páchala	páchat	k5eAaImAgFnS	páchat
zločiny	zločin	k1gInPc4	zločin
na	na	k7c6	na
civilním	civilní	k2eAgNnSc6d1	civilní
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
<g/>
,	,	kIx,	,
brutálně	brutálně	k6eAd1	brutálně
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
při	při	k7c6	při
povstání	povstání	k1gNnSc6	povstání
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
<g/>
Waffen-Grenadier-Division	Waffen-Grenadier-Division	k1gInSc1	Waffen-Grenadier-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
(	(	kIx(	(
<g/>
italienische	italienisch	k1gMnSc2	italienisch
Nr	Nr	k1gMnSc2	Nr
<g/>
.1	.1	k4	.1
<g/>
)	)	kIx)	)
29	[number]	k4	29
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
(	(	kIx(	(
<g/>
italská	italský	k2eAgFnSc1d1	italská
č.	č.	k?	č.
1	[number]	k4	1
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Waffen-Grenadier-Division	Waffen-Grenadier-Division	k1gInSc1	Waffen-Grenadier-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
(	(	kIx(	(
<g/>
weissruthenische	weissruthenisch	k1gMnSc2	weissruthenisch
Nr	Nr	k1gMnSc2	Nr
<g/>
.1	.1	k4	.1
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
(	(	kIx(	(
<g/>
běloruská	běloruský	k2eAgFnSc1d1	Běloruská
č.	č.	k?	č.
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
i	i	k9	i
jako	jako	k9	jako
ruská	ruský	k2eAgFnSc1d1	ruská
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
-	-	kIx~	-
sloužili	sloužit	k5eAaImAgMnP	sloužit
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
Bělorusové	Bělorus	k1gMnPc1	Bělorus
<g/>
,	,	kIx,	,
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
,	,	kIx,	,
Tataři	Tatar	k1gMnPc1	Tatar
<g/>
,	,	kIx,	,
Arméni	Armén	k1gMnPc1	Armén
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
<s>
SS-Freiwilligen-Grenadier-Division	SS-Freiwilligen-Grenadier-Division	k1gInSc1	SS-Freiwilligen-Grenadier-Division
31	[number]	k4	31
<g/>
.	.	kIx.	.
dobrovolnická	dobrovolnický	k2eAgFnSc1d1	dobrovolnická
divize	divize	k1gFnSc1	divize
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
-	-	kIx~	-
V	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
neoficiálním	neoficiální	k2eAgInSc6d1	neoficiální
názvu	název	k1gInSc6	název
je	být	k5eAaImIp3nS	být
přívlastek	přívlastek	k1gInSc4	přívlastek
"	"	kIx"	"
<g/>
Böhmen-Mahren	Böhmen-Mahrna	k1gFnPc2	Böhmen-Mahrna
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
příslušníky	příslušník	k1gMnPc7	příslušník
byli	být	k5eAaImAgMnP	být
zejména	zejména	k9	zejména
Volksdeutsche	Volksdeutsche	k1gFnSc4	Volksdeutsche
z	z	k7c2	z
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
,	,	kIx,	,
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
působili	působit	k5eAaImAgMnP	působit
i	i	k9	i
Němci	Němec	k1gMnPc1	Němec
z	z	k7c2	z
Protektorátu	protektorát	k1gInSc2	protektorát
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgInPc1	žádný
ověřitelné	ověřitelný	k2eAgInPc1d1	ověřitelný
podklady	podklad	k1gInPc1	podklad
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
tímto	tento	k3xDgInSc7	tento
neoficiálním	neoficiální	k2eAgInSc7d1	neoficiální
titulem	titul	k1gInSc7	titul
označována	označovat	k5eAaImNgFnS	označovat
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
z	z	k7c2	z
personálu	personál	k1gInSc2	personál
a	a	k8xC	a
frekvententů	frekventent	k1gInPc2	frekventent
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
kurzů	kurz	k1gInPc2	kurz
Waffen-SS	Waffen-SS	k1gFnPc2	Waffen-SS
z	z	k7c2	z
území	území	k1gNnSc2	území
Protektorátu	protektorát	k1gInSc2	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
sestavena	sestavit	k5eAaPmNgFnS	sestavit
z	z	k7c2	z
předběžně	předběžně	k6eAd1	předběžně
ad	ad	k7c4	ad
hoc	hoc	k?	hoc
ustavených	ustavený	k2eAgInPc2d1	ustavený
pluků	pluk	k1gInPc2	pluk
"	"	kIx"	"
<g/>
Böhmen	Böhmen	k1gInSc1	Böhmen
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Mähren	Mährno	k1gNnPc2	Mährno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
<s>
SS-Freiwilligen-Grenadier-Division	SS-Freiwilligen-Grenadier-Division	k1gInSc1	SS-Freiwilligen-Grenadier-Division
30	[number]	k4	30
<g/>
.	.	kIx.	.
<g/>
Januar	Januar	k1gInSc1	Januar
32	[number]	k4	32
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
pancéřových	pancéřový	k2eAgMnPc2d1	pancéřový
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
30	[number]	k4	30
<g/>
.	.	kIx.	.
<g/>
Januar	Januar	k1gInSc1	Januar
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
<s>
Waffen-Kavallerie-Division	Waffen-Kavallerie-Division	k1gInSc1	Waffen-Kavallerie-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
(	(	kIx(	(
<g/>
ungarische	ungarisch	k1gMnSc2	ungarisch
Nr	Nr	k1gMnSc2	Nr
<g/>
.3	.3	k4	.3
<g/>
)	)	kIx)	)
33	[number]	k4	33
<g/>
.	.	kIx.	.
jízdní	jízdní	k2eAgFnSc2d1	jízdní
divize	divize	k1gFnSc2	divize
SS	SS	kA	SS
(	(	kIx(	(
<g/>
maďarská	maďarský	k2eAgFnSc1d1	maďarská
č.	č.	k?	č.
3	[number]	k4	3
<g/>
)	)	kIx)	)
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
<s>
Waffen-Grenadier-Division	Waffen-Grenadier-Division	k1gInSc1	Waffen-Grenadier-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
Charlemagne	Charlemagn	k1gMnSc5	Charlemagn
(	(	kIx(	(
<g/>
französische	französischat	k5eAaPmIp3nS	französischat
Nr	Nr	k1gFnSc1	Nr
<g/>
.1	.1	k4	.1
<g/>
)	)	kIx)	)
33	[number]	k4	33
<g/>
.	.	kIx.	.
<g/>
divize	divize	k1gFnSc1	divize
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
Charlemagne	Charlemagn	k1gInSc5	Charlemagn
(	(	kIx(	(
<g/>
francouzská	francouzský	k2eAgFnSc1d1	francouzská
č.	č.	k?	č.
1	[number]	k4	1
<g/>
)	)	kIx)	)
34	[number]	k4	34
<g/>
.	.	kIx.	.
</s>
<s>
SS-Freiwilligen-Grenadier-Division	SS-Freiwilligen-Grenadier-Division	k1gInSc1	SS-Freiwilligen-Grenadier-Division
Landstorm	Landstorm	k1gInSc1	Landstorm
Nederland	Nederland	k1gInSc1	Nederland
34	[number]	k4	34
<g/>
.	.	kIx.	.
<g/>
dobrovolnická	dobrovolnický	k2eAgFnSc1d1	dobrovolnická
divize	divize	k1gFnSc1	divize
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
Landstorm	Landstorm	k1gInSc4	Landstorm
Nederland	Nederland	k1gInSc4	Nederland
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
<s>
SS-	SS-	k?	SS-
und	und	k?	und
Polizei-Grenadier-Division	Polizei-Grenadier-Division	k1gInSc1	Polizei-Grenadier-Division
35	[number]	k4	35
<g/>
.	.	kIx.	.
<g/>
divize	divize	k1gFnSc1	divize
policejních	policejní	k2eAgMnPc2d1	policejní
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
<s>
Waffen-Grenadier-Division	Waffen-Grenadier-Division	k1gInSc1	Waffen-Grenadier-Division
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
36	[number]	k4	36
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
-	-	kIx~	-
z	z	k7c2	z
jednotek	jednotka	k1gFnPc2	jednotka
SS	SS	kA	SS
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
nejhorší	zlý	k2eAgFnSc1d3	nejhorší
pověst	pověst	k1gFnSc1	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Velel	velet	k5eAaImAgMnS	velet
ji	on	k3xPp3gFnSc4	on
Oberführer	Oberführer	k1gMnSc1	Oberführer
SS	SS	kA	SS
Oskar	Oskar	k1gMnSc1	Oskar
Dirlewanger	Dirlewanger	k1gMnSc1	Dirlewanger
-	-	kIx~	-
sadista	sadista	k1gMnSc1	sadista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
pro	pro	k7c4	pro
zneužívání	zneužívání	k1gNnPc4	zneužívání
mladistvých	mladistvý	k1gMnPc2	mladistvý
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
prováděla	provádět	k5eAaImAgFnS	provádět
ty	ten	k3xDgInPc4	ten
nejhorší	zlý	k2eAgInPc4d3	Nejhorší
masakry	masakr	k1gInPc4	masakr
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
a	a	k8xC	a
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
vypalovala	vypalovat	k5eAaImAgFnS	vypalovat
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
znásilňovala	znásilňovat	k5eAaImAgFnS	znásilňovat
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
loupila	loupit	k5eAaImAgFnS	loupit
<g/>
.	.	kIx.	.
</s>
<s>
Běžným	běžný	k2eAgInSc7d1	běžný
způsobem	způsob	k1gInSc7	způsob
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
pacifikace	pacifikace	k1gFnSc2	pacifikace
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
byli	být	k5eAaImAgMnP	být
nahnáni	nahnat	k5eAaPmNgMnP	nahnat
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
nebo	nebo	k8xC	nebo
největší	veliký	k2eAgFnSc2d3	veliký
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byly	být	k5eAaImAgInP	být
poté	poté	k6eAd1	poté
naházeny	naházen	k2eAgInPc1d1	naházen
granáty	granát	k1gInPc1	granát
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
vesnice	vesnice	k1gFnSc1	vesnice
byla	být	k5eAaImAgFnS	být
zapálena	zapálen	k2eAgFnSc1d1	zapálena
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
upáleni	upálit	k5eAaPmNgMnP	upálit
zaživa	zaživa	k6eAd1	zaživa
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
se	se	k3xPyFc4	se
též	též	k9	též
účastnila	účastnit	k5eAaImAgFnS	účastnit
potlačování	potlačování	k1gNnSc2	potlačování
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
povstání	povstání	k1gNnSc2	povstání
a	a	k8xC	a
Varšavského	varšavský	k2eAgNnSc2d1	Varšavské
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
37	[number]	k4	37
<g/>
.	.	kIx.	.
</s>
<s>
SS-Freiwilligen-Kavallerie-Division	SS-Freiwilligen-Kavallerie-Division	k1gInSc1	SS-Freiwilligen-Kavallerie-Division
Lützow	Lützow	k1gFnSc2	Lützow
37	[number]	k4	37
<g/>
.	.	kIx.	.
dobrovolnická	dobrovolnický	k2eAgFnSc1d1	dobrovolnická
jízdní	jízdní	k2eAgFnSc1d1	jízdní
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Lützow	Lützow	k1gFnSc1	Lützow
38	[number]	k4	38
<g/>
.	.	kIx.	.
</s>
<s>
SS-Grenadier-Division	SS-Grenadier-Division	k1gInSc1	SS-Grenadier-Division
Nibelungen	Nibelungen	k1gInSc1	Nibelungen
38	[number]	k4	38
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
pancéřových	pancéřový	k2eAgMnPc2d1	pancéřový
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
Nibelungen	Nibelungen	k1gInSc4	Nibelungen
39	[number]	k4	39
<g/>
.	.	kIx.	.
</s>
<s>
Gebirgsdivision	Gebirgsdivision	k1gInSc1	Gebirgsdivision
der	drát	k5eAaImRp2nS	drát
SS	SS	kA	SS
39	[number]	k4	39
<g/>
.	.	kIx.	.
horská	horský	k2eAgFnSc1d1	horská
divize	divize	k1gFnSc1	divize
SS	SS	kA	SS
Chris	Chris	k1gFnSc1	Chris
Bishop	Bishop	k1gInSc1	Bishop
<g/>
,	,	kIx,	,
SS	SS	kA	SS
<g/>
:	:	kIx,	:
Peklo	peklo	k1gNnSc1	peklo
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
Christopher	Christophra	k1gFnPc2	Christophra
Ailsby	Ailsba	k1gFnSc2	Ailsba
<g/>
,	,	kIx,	,
SS	SS	kA	SS
<g/>
:	:	kIx,	:
Peklo	peklo	k1gNnSc1	peklo
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
Kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
,	,	kIx,	,
Terminologie	terminologie	k1gFnSc1	terminologie
a	a	k8xC	a
symbolika	symbolika	k1gFnSc1	symbolika
užívaná	užívaný	k2eAgFnSc1d1	užívaná
soudobou	soudobý	k2eAgFnSc7d1	soudobá
extremistickou	extremistický	k2eAgFnSc7d1	extremistická
scénou	scéna	k1gFnSc7	scéna
-	-	kIx~	-
Key	Key	k1gFnSc1	Key
Publishing	Publishing	k1gInSc4	Publishing
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Waffen-SS	Waffen-SS	k1gFnSc2	Waffen-SS
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
