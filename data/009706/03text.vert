<p>
<s>
Kyklopové	Kyklop	k1gMnPc1	Kyklop
jsou	být	k5eAaImIp3nP	být
obři	obr	k1gMnPc1	obr
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
okem	oke	k1gNnSc7	oke
uprostřed	uprostřed	k7c2	uprostřed
čela	čelo	k1gNnSc2	čelo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
řecký	řecký	k2eAgInSc1d1	řecký
název	název	k1gInSc1	název
Κ	Κ	k?	Κ
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
kruhové	kruhový	k2eAgNnSc1d1	kruhové
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
kolové	kolový	k2eAgNnSc1d1	kolové
oko	oko	k1gNnSc1	oko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
řeckého	řecký	k2eAgMnSc2d1	řecký
básníka	básník	k1gMnSc2	básník
Homéra	Homér	k1gMnSc2	Homér
tvořili	tvořit	k5eAaImAgMnP	tvořit
národ	národ	k1gInSc4	národ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
žil	žít	k5eAaImAgInS	žít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zvané	zvaný	k2eAgNnSc4d1	zvané
Hyperia	Hyperium	k1gNnPc4	Hyperium
<g/>
,	,	kIx,	,
bydleli	bydlet	k5eAaImAgMnP	bydlet
v	v	k7c6	v
jeskyních	jeskyně	k1gFnPc6	jeskyně
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
také	také	k9	také
byli	být	k5eAaImAgMnP	být
nevzdělaní	vzdělaný	k2eNgMnPc1d1	nevzdělaný
a	a	k8xC	a
suroví	surový	k2eAgMnPc1d1	surový
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
z	z	k7c2	z
Kyklopů	Kyklop	k1gMnPc2	Kyklop
byl	být	k5eAaImAgMnS	být
Polyfémos	Polyfémos	k1gMnSc1	Polyfémos
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
boha	bůh	k1gMnSc2	bůh
Poseidóna	Poseidón	k1gMnSc2	Poseidón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Hésioda	Hésiod	k1gMnSc2	Hésiod
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
tři	tři	k4xCgMnPc1	tři
Kyklopové	Kyklop	k1gMnPc1	Kyklop
–	–	k?	–
Steropés	Steropésa	k1gFnPc2	Steropésa
<g/>
,	,	kIx,	,
Brontés	Brontésa	k1gFnPc2	Brontésa
a	a	k8xC	a
Argés	Argésa	k1gFnPc2	Argésa
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnSc7	jejich
otcem	otec	k1gMnSc7	otec
je	být	k5eAaImIp3nS	být
bůh	bůh	k1gMnSc1	bůh
Úranos	Úranos	k1gInSc1	Úranos
a	a	k8xC	a
matkou	matka	k1gFnSc7	matka
bohyně	bohyně	k1gFnSc2	bohyně
Gaia	Gai	k1gInSc2	Gai
<g/>
.	.	kIx.	.
</s>
<s>
Přidali	přidat	k5eAaPmAgMnP	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
nejvyššímu	vysoký	k2eAgMnSc3d3	nejvyšší
bohu	bůh	k1gMnSc3	bůh
Diovi	Diův	k2eAgMnPc1d1	Diův
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
proti	proti	k7c3	proti
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
Kronovi	Kron	k1gMnSc3	Kron
<g/>
,	,	kIx,	,
a	a	k8xC	a
ukovali	ukovat	k5eAaPmAgMnP	ukovat
mu	on	k3xPp3gMnSc3	on
hromy	hrom	k1gInPc1	hrom
a	a	k8xC	a
blesky	blesk	k1gInPc1	blesk
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
pomocí	pomoc	k1gFnSc7	pomoc
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zbraň	zbraň	k1gFnSc1	zbraň
byla	být	k5eAaImAgFnS	být
společným	společný	k2eAgNnSc7d1	společné
dílem	dílo	k1gNnSc7	dílo
všech	všecek	k3xTgMnPc2	všecek
tří	tři	k4xCgMnPc2	tři
kyklopů	kyklop	k1gMnPc2	kyklop
<g/>
.	.	kIx.	.
</s>
<s>
Steropés	Steropés	k6eAd1	Steropés
zhotovil	zhotovit	k5eAaPmAgInS	zhotovit
blesk	blesk	k1gInSc1	blesk
<g/>
,	,	kIx,	,
Brontés	Brontés	k1gInSc1	Brontés
přidal	přidat	k5eAaPmAgInS	přidat
hrom	hrom	k1gInSc4	hrom
a	a	k8xC	a
Argés	Argés	k1gInSc4	Argés
přidal	přidat	k5eAaPmAgMnS	přidat
jas	jas	k1gInSc4	jas
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Kronovým	Kronův	k2eAgMnPc3d1	Kronův
Titánům	Titán	k1gMnPc3	Titán
Hádovi	Hádes	k1gMnSc3	Hádes
přilbu	přilba	k1gFnSc4	přilba
neviditelnosti	neviditelnost	k1gFnSc2	neviditelnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
dílem	díl	k1gInSc7	díl
jsou	být	k5eAaImIp3nP	být
prý	prý	k9	prý
i	i	k9	i
luk	luk	k1gInSc4	luk
a	a	k8xC	a
šípy	šíp	k1gInPc4	šíp
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
používala	používat	k5eAaImAgFnS	používat
bohyně	bohyně	k1gFnSc1	bohyně
Artemis	Artemis	k1gFnSc1	Artemis
<g/>
.	.	kIx.	.
</s>
<s>
Přilbu	přilba	k1gFnSc4	přilba
neviditelnosti	neviditelnost	k1gFnSc2	neviditelnost
pak	pak	k6eAd1	pak
Hádes	Hádes	k1gMnSc1	Hádes
zapůjčil	zapůjčit	k5eAaPmAgMnS	zapůjčit
Perseovi	Perseus	k1gMnSc3	Perseus
<g/>
,	,	kIx,	,
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
Medúzou	Medúza	k1gFnSc7	Medúza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
považovali	považovat	k5eAaImAgMnP	považovat
Kyklopy	Kyklop	k1gMnPc4	Kyklop
za	za	k7c4	za
syny	syn	k1gMnPc7	syn
storukých	storuký	k2eAgMnPc2d1	storuký
obrů	obr	k1gMnPc2	obr
Hekatoncheirů	Hekatoncheir	k1gInPc2	Hekatoncheir
a	a	k8xC	a
za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
vlast	vlast	k1gFnSc4	vlast
Malou	malý	k2eAgFnSc4d1	malá
Asii	Asie	k1gFnSc4	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
viděli	vidět	k5eAaImAgMnP	vidět
zpravidla	zpravidla	k6eAd1	zpravidla
pomocníky	pomocník	k1gMnPc4	pomocník
boha	bůh	k1gMnSc4	bůh
Vulkána	Vulkán	k2eAgMnSc4d1	Vulkán
(	(	kIx(	(
<g/>
řeckého	řecký	k2eAgMnSc4d1	řecký
Héfaista	Héfaist	k1gMnSc4	Héfaist
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žili	žít	k5eAaImAgMnP	žít
prý	prý	k9	prý
na	na	k7c4	na
Sicílii	Sicílie	k1gFnSc4	Sicílie
<g/>
,	,	kIx,	,
dílnu	dílna	k1gFnSc4	dílna
měli	mít	k5eAaImAgMnP	mít
pod	pod	k7c7	pod
Etnou	Etna	k1gFnSc7	Etna
a	a	k8xC	a
v	v	k7c6	v
sopce	sopka	k1gFnSc6	sopka
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
Liparských	Liparský	k2eAgInPc2d1	Liparský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Kyklopi	Kyklop	k1gMnPc1	Kyklop
dávno	dávno	k6eAd1	dávno
vyhynuli	vyhynout	k5eAaPmAgMnP	vyhynout
<g/>
,	,	kIx,	,
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
množství	množství	k1gNnSc1	množství
jejich	jejich	k3xOp3gNnSc2	jejich
vyobrazení	vyobrazení	k1gNnSc2	vyobrazení
od	od	k7c2	od
antických	antický	k2eAgMnPc2d1	antický
i	i	k8xC	i
moderních	moderní	k2eAgMnPc2d1	moderní
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
proslulé	proslulý	k2eAgFnSc6d1	proslulá
pasáži	pasáž	k1gFnSc6	pasáž
Homérovy	Homérův	k2eAgFnSc2d1	Homérova
Odysseii	Odysseium	k1gNnPc7	Odysseium
se	se	k3xPyFc4	se
hrdina	hrdina	k1gMnSc1	hrdina
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
Odysseus	Odysseus	k1gInSc4	Odysseus
<g/>
,	,	kIx,	,
na	na	k7c6	na
neznámém	známý	k2eNgInSc6d1	neznámý
ostrově	ostrov	k1gInSc6	ostrov
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
kyklopem	kyklop	k1gMnSc7	kyklop
Polyfémem	Polyfém	k1gMnSc7	Polyfém
<g/>
,	,	kIx,	,
synem	syn	k1gMnSc7	syn
Poseidóna	Poseidón	k1gMnSc2	Poseidón
a	a	k8xC	a
nereidovny	nereidovna	k1gFnSc2	nereidovna
Thoosy	Thoosa	k1gFnSc2	Thoosa
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
kyklopy	kyklop	k1gMnPc7	kyklop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyklopové	Kyklop	k1gMnPc1	Kyklop
prý	prý	k9	prý
také	také	k9	také
postavili	postavit	k5eAaPmAgMnP	postavit
"	"	kIx"	"
<g/>
kyklopské	kyklopský	k2eAgFnPc1d1	kyklopská
hradby	hradba	k1gFnPc1	hradba
<g/>
"	"	kIx"	"
měst	město	k1gNnPc2	město
Tiryns	Tirynsa	k1gFnPc2	Tirynsa
a	a	k8xC	a
Mykény	Mykény	k1gFnPc1	Mykény
na	na	k7c6	na
Peloponéském	peloponéský	k2eAgInSc6d1	peloponéský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zvuky	zvuk	k1gInPc4	zvuk
vycházející	vycházející	k2eAgInPc4d1	vycházející
z	z	k7c2	z
nitra	nitro	k1gNnSc2	nitro
sopek	sopka	k1gFnPc2	sopka
byly	být	k5eAaImAgFnP	být
připisovány	připisován	k2eAgFnPc1d1	připisována
jejich	jejich	k3xOp3gFnSc3	jejich
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povídalo	povídat	k5eAaImAgNnS	povídat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgMnPc4	tento
Kyklopy	Kyklop	k1gMnPc4	Kyklop
později	pozdě	k6eAd2	pozdě
zabil	zabít	k5eAaPmAgMnS	zabít
Apollón	Apollón	k1gMnSc1	Apollón
<g/>
,	,	kIx,	,
když	když	k8xS	když
Zeus	Zeus	k1gInSc1	Zeus
sprovodil	sprovodit	k5eAaPmAgInS	sprovodit
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
Asklépia	Asklépium	k1gNnSc2	Asklépium
zásahem	zásah	k1gInSc7	zásah
blesku	blesk	k1gInSc2	blesk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
právě	právě	k9	právě
Kyklopové	Kyklop	k1gMnPc1	Kyklop
zhotovili	zhotovit	k5eAaPmAgMnP	zhotovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Homérovi	Homérův	k2eAgMnPc1d1	Homérův
Kyklopové	Kyklop	k1gMnPc1	Kyklop
==	==	k?	==
</s>
</p>
<p>
<s>
Kyklopové	Kyklop	k1gMnPc1	Kyklop
byli	být	k5eAaImAgMnP	být
obrovští	obrovský	k2eAgMnPc1d1	obrovský
jednoocí	jednooký	k2eAgMnPc1d1	jednooký
netvoři	netvor	k1gMnPc1	netvor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
termínem	termín	k1gInSc7	termín
"	"	kIx"	"
<g/>
Kyklop	Kyklop	k1gMnSc1	Kyklop
<g/>
"	"	kIx"	"
odkazováno	odkazovat	k5eAaImNgNnS	odkazovat
na	na	k7c4	na
Polyféma	Polyfém	k1gMnSc4	Polyfém
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc4	syn
Poseidona	Poseidon	k1gMnSc4	Poseidon
a	a	k8xC	a
Thoose	Thoosa	k1gFnSc6	Thoosa
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tohoto	tento	k3xDgMnSc2	tento
Kyklopa	Kyklop	k1gMnSc2	Kyklop
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
Odysseovi	Odysseus	k1gMnSc3	Odysseus
oslepit	oslepit	k5eAaPmF	oslepit
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgInPc7	svůj
druhy	druh	k1gInPc7	druh
unikl	uniknout	k5eAaPmAgInS	uniknout
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Ztratil	ztratit	k5eAaPmAgMnS	ztratit
však	však	k9	však
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
dvanácti	dvanáct	k4xCc2	dvanáct
lidí	člověk	k1gMnPc2	člověk
plnou	plný	k2eAgFnSc4d1	plná
polovinu	polovina	k1gFnSc4	polovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
příběh	příběh	k1gInSc1	příběh
z	z	k7c2	z
Odyssei	Odysse	k1gFnSc2	Odysse
je	být	k5eAaImIp3nS	být
vtipněji	vtipně	k6eAd2	vtipně
podán	podat	k5eAaPmNgInS	podat
v	v	k7c6	v
satirické	satirický	k2eAgFnSc6d1	satirická
hře	hra	k1gFnSc6	hra
od	od	k7c2	od
Euripida	Euripid	k1gMnSc2	Euripid
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Kyklopové	Kyklop	k1gMnPc5	Kyklop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řecký	řecký	k2eAgMnSc1d1	řecký
básník	básník	k1gMnSc1	básník
ze	z	k7c2	z
Sicílie	Sicílie	k1gFnSc2	Sicílie
jménem	jméno	k1gNnSc7	jméno
Theokritos	Theokritos	k1gMnSc1	Theokritos
napsal	napsat	k5eAaBmAgMnS	napsat
dvě	dva	k4xCgFnPc4	dva
básně	báseň	k1gFnPc4	báseň
přibližně	přibližně	k6eAd1	přibližně
roku	rok	k1gInSc2	rok
275	[number]	k4	275
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
o	o	k7c6	o
Polyfémově	Polyfémův	k2eAgFnSc6d1	Polyfémův
touze	touha	k1gFnSc6	touha
po	po	k7c6	po
Galatei	Galate	k1gFnSc6	Galate
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgFnSc6d1	mořská
nymfě	nymfa	k1gFnSc6	nymfa
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
Galatea	Galatea	k1gFnSc1	Galatea
dávala	dávat	k5eAaImAgFnS	dávat
přednost	přednost	k1gFnSc4	přednost
Ákidovi	Ákida	k1gMnSc3	Ákida
<g/>
,	,	kIx,	,
Polyfémos	Polyfémos	k1gInSc1	Polyfémos
ho	on	k3xPp3gMnSc4	on
zabil	zabít	k5eAaPmAgInS	zabít
obrovským	obrovský	k2eAgInSc7d1	obrovský
hozeným	hozený	k2eAgInSc7d1	hozený
balvanem	balvan	k1gInSc7	balvan
<g/>
.	.	kIx.	.
</s>
<s>
Galatea	Galatea	k1gFnSc1	Galatea
stačila	stačit	k5eAaBmAgFnS	stačit
uprchnou	uprchnout	k5eAaPmIp3nP	uprchnout
a	a	k8xC	a
Ákisova	Ákisův	k2eAgFnSc1d1	Ákisův
krev	krev	k1gFnSc1	krev
vyteklá	vyteklý	k2eAgFnSc1d1	vyteklá
zpod	zpod	k7c2	zpod
balvanu	balvan	k1gInSc2	balvan
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
sicilskou	sicilský	k2eAgFnSc4d1	sicilská
řeku	řeka	k1gFnSc4	řeka
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Walter	Walter	k1gMnSc1	Walter
Burkert	Burkert	k1gMnSc1	Burkert
mezi	mezi	k7c7	mezi
jiným	jiný	k1gMnSc7	jiný
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
starobylé	starobylý	k2eAgFnPc4d1	starobylá
skupiny	skupina	k1gFnPc4	skupina
nebo	nebo	k8xC	nebo
společnosti	společnost	k1gFnPc4	společnost
menšími	malý	k2eAgMnPc7d2	menší
bohy	bůh	k1gMnPc7	bůh
zrcadlí	zrcadlit	k5eAaImIp3nP	zrcadlit
skutečný	skutečný	k2eAgInSc1d1	skutečný
kult	kult	k1gInSc1	kult
společenství	společenství	k1gNnSc2	společenství
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
domněnku	domněnka	k1gFnSc4	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
názvy	název	k1gInPc4	název
Kabeirové	Kabeirový	k2eAgInPc4d1	Kabeirový
<g/>
,	,	kIx,	,
Daktylové	daktylový	k2eAgInPc4d1	daktylový
<g/>
,	,	kIx,	,
Telchinové	Telchinové	k2eAgInSc4d1	Telchinové
a	a	k8xC	a
Kyklopové	Kyklop	k1gMnPc1	Kyklop
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
skrývat	skrývat	k5eAaImF	skrývat
kovářské	kovářský	k2eAgInPc4d1	kovářský
spolky	spolek	k1gInPc4	spolek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mnoho	mnoho	k4c1	mnoho
učenců	učenec	k1gMnPc2	učenec
tak	tak	k9	tak
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
jediném	jediný	k2eAgNnSc6d1	jediné
oku	oko	k1gNnSc6	oko
Kyklopů	Kyklop	k1gMnPc2	Kyklop
mohla	moct	k5eAaImAgFnS	moct
vzejít	vzejít	k5eAaPmF	vzejít
z	z	k7c2	z
pozorování	pozorování	k1gNnSc2	pozorování
práce	práce	k1gFnSc2	práce
skutečných	skutečný	k2eAgMnPc2d1	skutečný
kovářů	kovář	k1gMnPc2	kovář
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
oku	oko	k1gNnSc6	oko
nosili	nosit	k5eAaImAgMnP	nosit
pásku	páska	k1gFnSc4	páska
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
zabránili	zabránit	k5eAaPmAgMnP	zabránit
odlétávání	odlétávání	k1gNnSc4	odlétávání
jisker	jiskra	k1gFnPc2	jiskra
do	do	k7c2	do
obou	dva	k4xCgNnPc2	dva
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Graves	Graves	k1gMnSc1	Graves
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
o	o	k7c4	o
kováře	kovář	k1gMnPc4	kovář
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
tetováni	tetovat	k5eAaImNgMnP	tetovat
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
soustřednými	soustředný	k2eAgInPc7d1	soustředný
kruhy	kruh	k1gInPc7	kruh
na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
úcty	úcta	k1gFnSc2	úcta
ke	k	k7c3	k
slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Kyklopové	Kyklop	k1gMnPc1	Kyklop
v	v	k7c6	v
Homérově	Homérův	k2eAgFnSc6d1	Homérova
Odysseie	Odysseie	k1gFnSc2	Odysseie
jsou	být	k5eAaImIp3nP	být
jiného	jiný	k1gMnSc4	jiný
typu	typ	k1gInSc2	typ
než	než	k8xS	než
v	v	k7c6	v
Hésiodově	Hésiodův	k2eAgFnSc6d1	Hésiodova
práci	práce	k1gFnSc6	práce
"	"	kIx"	"
<g/>
Teogony	Teogona	k1gFnSc2	Teogona
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
O	o	k7c6	o
původu	původ	k1gInSc6	původ
bohů	bůh	k1gMnPc2	bůh
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
byli	být	k5eAaImAgMnP	být
dost	dost	k6eAd1	dost
pozdě	pozdě	k6eAd1	pozdě
přiřazeni	přiřadit	k5eAaPmNgMnP	přiřadit
do	do	k7c2	do
panteonu	panteon	k1gInSc2	panteon
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
neměli	mít	k5eNaImAgMnP	mít
žádné	žádný	k3yNgNnSc4	žádný
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
kováři	kovář	k1gMnPc7	kovář
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
legendy	legenda	k1gFnPc1	legenda
spojené	spojený	k2eAgFnPc1d1	spojená
s	s	k7c7	s
Polyfémem	Polyfém	k1gMnSc7	Polyfém
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
před	před	k7c7	před
Homérovou	Homérův	k2eAgFnSc7d1	Homérova
Odysseiou	Odysseia	k1gFnSc7	Odysseia
nemluvily	mluvit	k5eNaImAgFnP	mluvit
jako	jako	k8xS	jako
o	o	k7c6	o
kyklopovi	kyklop	k1gMnSc6	kyklop
<g/>
.	.	kIx.	.
</s>
<s>
Polyfémos	Polyfémos	k1gInSc1	Polyfémos
mohl	moct	k5eAaImAgInS	moct
původně	původně	k6eAd1	původně
být	být	k5eAaImF	být
nějakým	nějaký	k3yIgInSc7	nějaký
druhem	druh	k1gInSc7	druh
místního	místní	k2eAgMnSc2d1	místní
démona	démon	k1gMnSc2	démon
nebo	nebo	k8xC	nebo
monstra	monstrum	k1gNnPc1	monstrum
<g/>
.	.	kIx.	.
</s>
<s>
Triamantes	Triamantes	k1gInSc1	Triamantes
v	v	k7c6	v
Krétské	krétský	k2eAgFnSc6d1	krétská
legendě	legenda	k1gFnSc6	legenda
naznačoval	naznačovat	k5eAaImAgMnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
venkovskou	venkovský	k2eAgFnSc7d1	venkovská
rasou	rasa	k1gFnSc7	rasa
lidožravých	lidožravý	k2eAgMnPc2d1	lidožravý
obrů	obr	k1gMnPc2	obr
s	s	k7c7	s
třetím	třetí	k4xOgNnSc7	třetí
okem	oke	k1gNnSc7	oke
umístěným	umístěný	k2eAgInSc7d1	umístěný
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Triamantes	Triamantes	k1gMnSc1	Triamantes
popisuje	popisovat	k5eAaImIp3nS	popisovat
jejich	jejich	k3xOp3gNnPc4	jejich
oči	oko	k1gNnPc4	oko
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
nápadně	nápadně	k6eAd1	nápadně
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
Homérovým	Homérův	k2eAgMnPc3d1	Homérův
Kyklopům	Kyklop	k1gMnPc3	Kyklop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
teorie	teorie	k1gFnPc1	teorie
o	o	k7c6	o
možném	možný	k2eAgInSc6d1	možný
původu	původ	k1gInSc6	původ
Kyklopů	Kyklop	k1gMnPc2	Kyklop
je	být	k5eAaImIp3nS	být
legenda	legenda	k1gFnSc1	legenda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
byly	být	k5eAaImAgFnP	být
starověkými	starověký	k2eAgFnPc7d1	starověká
Řeky	Řek	k1gMnPc4	Řek
nacházeny	nacházen	k2eAgMnPc4d1	nacházen
na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
a	a	k8xC	a
Sicílii	Sicílie	k1gFnSc6	Sicílie
lebky	lebka	k1gFnSc2	lebka
trpasličích	trpasličí	k2eAgMnPc2d1	trpasličí
prehistorických	prehistorický	k2eAgMnPc2d1	prehistorický
slonů	slon	k1gMnPc2	slon
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dvakrát	dvakrát	k6eAd1	dvakrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
lebka	lebka	k1gFnSc1	lebka
lidská	lidský	k2eAgFnSc1d1	lidská
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
centrální	centrální	k2eAgFnSc3d1	centrální
nosní	nosní	k2eAgFnSc3d1	nosní
dutině	dutina	k1gFnSc3	dutina
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
chobot	chobot	k1gInSc4	chobot
<g/>
)	)	kIx)	)
v	v	k7c6	v
lebce	lebka	k1gFnSc6	lebka
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
uvěřit	uvěřit	k5eAaPmF	uvěřit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
velký	velký	k2eAgInSc4d1	velký
oční	oční	k2eAgInSc4d1	oční
důlek	důlek	k1gInSc4	důlek
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInPc1d2	menší
<g/>
,	,	kIx,	,
skutečné	skutečný	k2eAgInPc1d1	skutečný
oční	oční	k2eAgInPc1d1	oční
důlky	důlek	k1gInPc1	důlek
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
lebky	lebka	k1gFnSc2	lebka
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
mělké	mělký	k2eAgFnPc1d1	mělká
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
stěží	stěží	k6eAd1	stěží
rozeznatelné	rozeznatelný	k2eAgNnSc1d1	rozeznatelné
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
měli	mít	k5eAaImAgMnP	mít
zřejmě	zřejmě	k6eAd1	zřejmě
malé	malý	k2eAgFnPc4d1	malá
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
žijícími	žijící	k2eAgMnPc7d1	žijící
slony	slon	k1gMnPc7	slon
<g/>
,	,	kIx,	,
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
rozeznat	rozeznat	k5eAaPmF	rozeznat
správný	správný	k2eAgInSc4d1	správný
původ	původ	k1gInSc4	původ
lebek	lebka	k1gFnPc2	lebka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
přijímáno	přijímat	k5eAaImNgNnS	přijímat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kyklop	kyklop	k1gMnSc1	kyklop
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
oko	oko	k1gNnSc1	oko
<g/>
,	,	kIx,	,
Homérův	Homérův	k2eAgInSc1d1	Homérův
popis	popis	k1gInSc1	popis
Polyféma	Polyfém	k1gMnSc2	Polyfém
tomu	ten	k3xDgNnSc3	ten
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
váz	váza	k1gFnPc2	váza
je	být	k5eAaImIp3nS	být
zobrazen	zobrazen	k2eAgMnSc1d1	zobrazen
Odysseus	Odysseus	k1gMnSc1	Odysseus
oslepující	oslepující	k2eAgFnSc2d1	oslepující
Polyféma	Polyfém	k1gMnSc2	Polyfém
pomocí	pomocí	k7c2	pomocí
velké	velký	k2eAgFnSc2d1	velká
vidličky	vidlička	k1gFnSc2	vidlička
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
hroty	hrot	k1gInPc7	hrot
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kyklop	kyklop	k1gMnSc1	kyklop
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgNnPc4	dva
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kýchavice	kýchavice	k1gFnSc1	kýchavice
bílá	bílý	k2eAgFnSc1d1	bílá
(	(	kIx(	(
<g/>
Veratrum	Veratrum	k1gNnSc1	Veratrum
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylinná	bylinný	k2eAgFnSc1d1	bylinná
léčivka	léčivka	k1gFnSc1	léčivka
popsaná	popsaný	k2eAgFnSc1d1	popsaná
Hippokratem	Hippokrat	k1gMnSc7	Hippokrat
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
400	[number]	k4	400
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
alkaloidy	alkaloid	k1gInPc1	alkaloid
cyklopamin	cyklopamin	k1gInSc1	cyklopamin
a	a	k8xC	a
jervin	jervina	k1gFnPc2	jervina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
poruchu	porucha	k1gFnSc4	porucha
zvanou	zvaný	k2eAgFnSc4d1	zvaná
kyklopie	kyklopie	k1gFnSc1	kyklopie
(	(	kIx(	(
<g/>
arinencefalie	arinencefalie	k1gFnSc1	arinencefalie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kyklopové	Kyklop	k1gMnPc1	Kyklop
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Kyklopové	Kyklop	k1gMnPc1	Kyklop
byli	být	k5eAaImAgMnP	být
vděčným	vděčný	k2eAgNnSc7d1	vděčné
tématem	téma	k1gNnSc7	téma
zobrazování	zobrazování	k1gNnSc2	zobrazování
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
největší	veliký	k2eAgFnPc1d3	veliký
popularity	popularita	k1gFnPc1	popularita
se	se	k3xPyFc4	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
dostávalo	dostávat	k5eAaImAgNnS	dostávat
Polyfémovi	Polyfém	k1gMnSc3	Polyfém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Homér	Homér	k1gMnSc1	Homér
–	–	k?	–
Odysseia	Odysseia	k1gFnSc1	Odysseia
(	(	kIx(	(
<g/>
epická	epický	k2eAgFnSc1d1	epická
báseň	báseň	k1gFnSc1	báseň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hésiodos	Hésiodos	k1gInSc1	Hésiodos
–	–	k?	–
O	o	k7c6	o
původu	původ	k1gInSc6	původ
bohů	bůh	k1gMnPc2	bůh
(	(	kIx(	(
<g/>
báseň	báseň	k1gFnSc1	báseň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Eurípidés	Eurípidésa	k1gFnPc2	Eurípidésa
–	–	k?	–
Kyklopové	Kyklop	k1gMnPc1	Kyklop
(	(	kIx(	(
<g/>
satyrské	satyrský	k2eAgNnSc1d1	satyrské
drama	drama	k1gNnSc1	drama
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Sládek	Sládek	k1gMnSc1	Sládek
–	–	k?	–
báseň	báseň	k1gFnSc1	báseň
Kyklop	Kyklop	k1gMnSc1	Kyklop
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
–	–	k?	–
Kyklopův	Kyklopův	k2eAgMnSc1d1	Kyklopův
žalVýtvarné	žalVýtvarný	k2eAgNnSc4d1	žalVýtvarný
umění	umění	k1gNnSc4	umění
</s>
</p>
<p>
<s>
Vulkán	vulkán	k1gInSc1	vulkán
a	a	k8xC	a
Kylópi	Kylópi	k1gNnSc1	Kylópi
od	od	k7c2	od
Tintoretta	Tintorett	k1gInSc2	Tintorett
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1578	[number]	k4	1578
</s>
</p>
<p>
<s>
Kyklópi	Kyklóp	k1gMnPc1	Kyklóp
od	od	k7c2	od
L.	L.	kA	L.
Giordana	Giordana	k1gFnSc1	Giordana
z	z	k7c2	z
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
stoletíSochařství	stoletíSochařství	k1gNnSc2	stoletíSochařství
</s>
</p>
<p>
<s>
Kyklop	Kyklop	k1gMnSc1	Kyklop
od	od	k7c2	od
E.	E.	kA	E.
Palozziho	Palozzi	k1gMnSc4	Palozzi
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
v	v	k7c6	v
Tateově	Tateův	k2eAgFnSc6d1	Tateův
galerii	galerie	k1gFnSc6	galerie
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Graves	Graves	k1gMnSc1	Graves
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Řecké	řecký	k2eAgInPc1d1	řecký
mýty	mýtus	k1gInPc1	mýtus
-	-	kIx~	-
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
–	–	k?	–
BRÁNA	brán	k2eAgFnSc1d1	brána
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7243-266-4	[number]	k4	80-7243-266-4
</s>
</p>
<p>
<s>
Bořek	Bořek	k1gMnSc1	Bořek
Neškudla	Neškudla	k1gMnSc1	Neškudla
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
řeckých	řecký	k2eAgMnPc2d1	řecký
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
mýtů	mýtus	k1gInPc2	mýtus
–	–	k?	–
Libri	Libri	k1gNnSc4	Libri
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7277-125-6	[number]	k4	80-7277-125-6
</s>
</p>
<p>
<s>
EURIPIDÉS	EURIPIDÉS	kA	EURIPIDÉS
<g/>
.	.	kIx.	.
</s>
<s>
Kyklops	Kyklops	k6eAd1	Kyklops
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Král	Král	k1gMnSc1	Král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
A.	A.	kA	A.
Storch	Storch	k1gMnSc1	Storch
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
60	[number]	k4	60
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Kyklopie	kyklopie	k1gFnSc1	kyklopie
</s>
</p>
<p>
<s>
Kyklopské	kyklopský	k2eAgNnSc1d1	kyklopské
zdivo	zdivo	k1gNnSc1	zdivo
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kyklop	kyklop	k1gMnSc1	kyklop
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Výtahy	výtah	k1gInPc1	výtah
z	z	k7c2	z
originálních	originální	k2eAgInPc2d1	originální
řeckých	řecký	k2eAgInPc2d1	řecký
pramenů	pramen	k1gInPc2	pramen
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
</s>
</p>
