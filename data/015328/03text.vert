<s>
MÁV	MÁV	kA
řada	řada	k1gFnSc1
V63	V63	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
substituovaný	substituovaný	k2eAgInSc1d1
infobox	infobox	k1gInSc1
<g/>
.	.	kIx.
<g/>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
převedete	převést	k5eAaPmIp2nP
na	na	k7c4
standardní	standardní	k2eAgFnSc4d1
šablonu	šablona	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
lokomotiva	lokomotiva	k1gFnSc1
řady	řada	k1gFnSc2
630	#num#	k4
(	(	kIx(
<g/>
MÁV	MÁV	kA
<g/>
)	)	kIx)
</s>
<s>
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Výrobce	výrobce	k1gMnSc1
Ganz	Ganz	k1gMnSc1
<g/>
–	–	k?
<g/>
MÁVAG	MÁVAG	kA
</s>
<s>
Rok	rok	k1gInSc1
výroby	výroba	k1gFnSc2
1981	#num#	k4
<g/>
–	–	k?
<g/>
1988	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
vyrobených	vyrobený	k2eAgInPc2d1
kusů	kus	k1gInPc2
<g/>
58	#num#	k4
</s>
<s>
Provozovatel	provozovatel	k1gMnSc1
MÁV	MÁV	kA
</s>
<s>
Období	období	k1gNnSc1
provozu	provoz	k1gInSc2
1981	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
</s>
<s>
Trvalý	trvalý	k2eAgInSc1d1
výkon	výkon	k1gInSc1
3	#num#	k4
680	#num#	k4
kW	kW	kA
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
tažná	tažný	k2eAgFnSc1d1
síla	síla	k1gFnSc1
442	#num#	k4
kN	kN	k?
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
160	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
a	a	k8xC
rozměry	rozměr	k1gInPc1
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
ve	v	k7c6
službě	služba	k1gFnSc6
116	#num#	k4
t	t	k?
</s>
<s>
Délka	délka	k1gFnSc1
přes	přes	k7c4
nárazníky	nárazník	k1gInPc4
19	#num#	k4
596	#num#	k4
mm	mm	kA
</s>
<s>
Rozchod	rozchod	k1gInSc4
kolejí	kolej	k1gFnPc2
<g/>
1	#num#	k4
435	#num#	k4
mm	mm	kA
</s>
<s>
Parametry	parametr	k1gInPc1
pohonu	pohon	k1gInSc2
</s>
<s>
Uspořádání	uspořádání	k1gNnSc1
pojezduCo	pojezduCo	k1gMnSc1
<g/>
´	´	k?
Co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
<g/>
´	´	k?
</s>
<s>
Napájecí	napájecí	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
25	#num#	k4
kV	kV	k?
<g/>
/	/	kIx~
<g/>
50	#num#	k4
Hz	Hz	kA
</s>
<s>
Magyar	Magyar	k1gMnSc1
Államvasutak	Államvasutak	k1gMnSc1
</s>
<s>
Šestinápravové	šestinápravový	k2eAgFnSc2d1
elektrické	elektrický	k2eAgFnSc2d1
lokomotivy	lokomotiva	k1gFnSc2
řady	řada	k1gFnSc2
630	#num#	k4
(	(	kIx(
<g/>
staré	starý	k2eAgNnSc1d1
označení	označení	k1gNnSc1
<g/>
:	:	kIx,
V	v	k7c6
<g/>
63	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přezdívané	přezdívaný	k2eAgInPc4d1
Gigant	gigant	k1gInSc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
nejvýkonnější	výkonný	k2eAgFnPc1d3
maďarské	maďarský	k2eAgFnPc1d1
lokomotivy	lokomotiva	k1gFnPc1
domácí	domácí	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgInPc1
dva	dva	k4xCgInPc1
prototypy	prototyp	k1gInPc1
byly	být	k5eAaImAgInP
vyrobeny	vyrobit	k5eAaPmNgInP
firmou	firma	k1gFnSc7
Ganz	Ganz	k1gMnSc1
<g/>
–	–	k?
<g/>
MÁVAG	MÁVAG	kA
v	v	k7c6
letech	let	k1gInPc6
1974	#num#	k4
–	–	k?
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovala	následovat	k5eAaImAgFnS
sériová	sériový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
56	#num#	k4
kusů	kus	k1gInPc2
v	v	k7c6
letech	let	k1gInPc6
1981	#num#	k4
–	–	k?
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Standardní	standardní	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
lokomotiv	lokomotiva	k1gFnPc2
je	být	k5eAaImIp3nS
130	#num#	k4
a	a	k8xC
140	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
bylo	být	k5eAaImAgNnS
10	#num#	k4
lokomotiv	lokomotiva	k1gFnPc2
upraveno	upravit	k5eAaPmNgNnS
pro	pro	k7c4
maximální	maximální	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
160	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Provoz	provoz	k1gInSc1
</s>
<s>
Lokomotivy	lokomotiva	k1gFnSc2
630	#num#	k4
jsou	být	k5eAaImIp3nP
nasazovány	nasazován	k2eAgInPc1d1
v	v	k7c6
osobní	osobní	k2eAgFnSc6d1
i	i	k8xC
nákladní	nákladní	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
na	na	k7c6
hlavních	hlavní	k2eAgInPc6d1
železničních	železniční	k2eAgInPc6d1
koridorech	koridor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
zajížděly	zajíždět	k5eAaImAgFnP
také	také	k9
do	do	k7c2
Bratislavy	Bratislava	k1gFnSc2
a	a	k8xC
hlouběji	hluboko	k6eAd2
do	do	k7c2
slovenského	slovenský	k2eAgNnSc2d1
vnitrozemí	vnitrozemí	k1gNnSc2
například	například	k6eAd1
až	až	k9
do	do	k7c2
stanic	stanice	k1gFnPc2
Kúty	Kúta	k1gFnSc2
a	a	k8xC
Leopoldov	Leopoldov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
GVD	GVD	kA
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
již	již	k6eAd1
na	na	k7c4
slovenské	slovenský	k2eAgNnSc4d1
území	území	k1gNnSc4
téměř	téměř	k6eAd1
nezajíždějí	zajíždět	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
síť	síť	k1gFnSc4
Správy	správa	k1gFnSc2
železnic	železnice	k1gFnPc2
nebyly	být	k5eNaImAgFnP
nikdy	nikdy	k6eAd1
připuštěny	připustit	k5eAaPmNgFnP
kvůli	kvůli	k7c3
velikosti	velikost	k1gFnSc3
rušivých	rušivý	k2eAgInPc2d1
proudů	proud	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dislokace	dislokace	k1gFnSc1
</s>
<s>
Lokomotivy	lokomotiva	k1gFnPc1
jsou	být	k5eAaImIp3nP
dislokovány	dislokovat	k5eAaBmNgFnP
v	v	k7c6
těchto	tento	k3xDgNnPc6
depech	depo	k1gNnPc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Budapest-Ferencváros	Budapest-Ferencvárosa	k1gFnPc2
<g/>
:	:	kIx,
630.004	630.004	k4
–	–	k?
0	#num#	k4
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
630.032	630.032	k4
–	–	k?
0	#num#	k4
<g/>
37	#num#	k4
<g/>
,	,	kIx,
630.039	630.039	k4
–	–	k?
0	#num#	k4
<g/>
42	#num#	k4
<g/>
,	,	kIx,
630.045	630.045	k4
–	–	k?
0	#num#	k4
<g/>
49	#num#	k4
<g/>
,	,	kIx,
630.106	630.106	k4
<g/>
,	,	kIx,
630.138	630.138	k4
<g/>
,	,	kIx,
630.143	630.143	k4
<g/>
,	,	kIx,
630.144	630.144	k4
<g/>
,	,	kIx,
630.150	630.150	k4
–	–	k?
156	#num#	k4
</s>
<s>
Nyíregyháza	Nyíregyháza	k1gFnSc1
<g/>
:	:	kIx,
630.019	630.019	k4
–	–	k?
0	#num#	k4
<g/>
24	#num#	k4
<g/>
,	,	kIx,
630.026	630.026	k4
–	–	k?
031	#num#	k4
</s>
<s>
Dombóvár	Dombóvár	k1gInSc1
<g/>
:	:	kIx,
630.008	630.008	k4
–	–	k?
0	#num#	k4
<g/>
18	#num#	k4
<g/>
,	,	kIx,
630.025	630.025	k4
</s>
<s>
Jména	jméno	k1gNnPc1
</s>
<s>
Některé	některý	k3yIgFnPc4
lokomotivy	lokomotiva	k1gFnPc4
řady	řada	k1gFnSc2
630	#num#	k4
jsou	být	k5eAaImIp3nP
pojmenovány	pojmenován	k2eAgInPc1d1
podle	podle	k7c2
maďarských	maďarský	k2eAgFnPc2d1
významných	významný	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
630.138	630.138	k4
–	–	k?
Kandó	Kandó	k1gMnSc1
Kálmán	Kálmán	k2eAgMnSc1d1
</s>
<s>
630.143	630.143	k4
–	–	k?
Baross	Baross	k1gInSc1
Gábor	Gábor	k1gInSc1
</s>
<s>
630.152	630.152	k4
–	–	k?
Kossuth	Kossuth	k1gMnSc1
Lajos	Lajos	k1gMnSc1
</s>
<s>
630.154	630.154	k4
–	–	k?
Dr	dr	kA
<g/>
.	.	kIx.
Verebély	Verebéla	k1gMnSc2
László	László	k1gMnSc2
</s>
<s>
630.155	630.155	k4
–	–	k?
Gróf	gróf	k1gMnSc1
Mikó	Mikó	k1gMnSc2
Imre	Imre	k1gNnSc2
</s>
<s>
630.156	630.156	k4
–	–	k?
Gróf	gróf	k1gMnSc1
Széchenyi	Szécheny	k1gFnSc2
István	István	k2eAgMnSc1d1
</s>
<s>
Některé	některý	k3yIgFnPc1
lokomotivy	lokomotiva	k1gFnPc1
dostaly	dostat	k5eAaPmAgFnP
jména	jméno	k1gNnPc1
podle	podle	k7c2
měst	město	k1gNnPc2
<g/>
:	:	kIx,
</s>
<s>
630.013	630.013	k4
<g/>
:	:	kIx,
Dombóvár	Dombóvár	k1gInSc1
</s>
<s>
630.017	630.017	k4
<g/>
:	:	kIx,
Celldömölk	Celldömölk	k1gInSc1
</s>
<s>
630.023	630.023	k4
<g/>
:	:	kIx,
Püspökladány	Püspökladán	k2eAgInPc1d1
</s>
<s>
630.034	630.034	k4
<g/>
:	:	kIx,
Budapest-Ferencváros	Budapest-Ferencvárosa	k1gFnPc2
</s>
<s>
630.035	630.035	k4
<g/>
:	:	kIx,
Cegléd	Cegléd	k1gInSc1
</s>
<s>
630.036	630.036	k4
<g/>
:	:	kIx,
Hatvan	Hatvan	k1gMnSc1
</s>
<s>
630.052	630.052	k4
<g/>
:	:	kIx,
Kiskunhalas	Kiskunhalas	k1gInSc1
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Lokomotiva	lokomotiva	k1gFnSc1
630.008	630.008	k4
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Lokomotiva	lokomotiva	k1gFnSc1
630.012	630.012	k4
ve	v	k7c6
stanici	stanice	k1gFnSc6
Százhalombatta	Százhalombatt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Lokomotiva	lokomotiva	k1gFnSc1
630.020	630.020	k4
ve	v	k7c6
stanici	stanice	k1gFnSc6
Üllő	Üllő	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lokomotiva	lokomotiva	k1gFnSc1
630.039	630.039	k4
v	v	k7c6
Debrecíně	Debrecína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Lokomotiva	lokomotiva	k1gFnSc1
V63	V63	k1gFnSc2
ve	v	k7c6
velikosti	velikost	k1gFnSc6
H	H	kA
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
MÁV	MÁV	kA
V63	V63	k1gMnSc1
sorozat	sorozat	k5eAaImF,k5eAaPmF
na	na	k7c6
maďarské	maďarský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Malý	malý	k2eAgInSc4d1
atlas	atlas	k1gInSc4
lokomotiv	lokomotiva	k1gFnPc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Gradis	Gradis	k1gFnSc1
Bohemia	bohemia	k1gFnSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86925	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Cizí	cizit	k5eAaImIp3nS
vozidla	vozidlo	k1gNnPc4
v	v	k7c4
ČR	ČR	kA
/	/	kIx~
SR	SR	kA
<g/>
,	,	kIx,
s.	s.	k?
275	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www	www	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
chem.	chem.	k?
<g/>
elte	eltat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
hu	hu	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.vonatka.hu/v63.htm	http://www.vonatka.hu/v63.htm	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
MÁV-START	MÁV-START	k?
</s>
<s>
Magyar	Magyar	k1gMnSc1
Államvasutak	Államvasutak	k1gMnSc1
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
MÁV	MÁV	kA
řada	řada	k1gFnSc1
V63	V63	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
MÁV-TRACIÓ	MÁV-TRACIÓ	k1gFnSc1
-	-	kIx~
V63	V63	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
Vonatka	Vonatka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
hu	hu	k0
</s>
<s>
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
Gigant	gigant	k1gInSc1
Club	club	k1gInSc1
<g/>
.	.	kIx.
<g/>
hu	hu	k0
-	-	kIx~
V63	V63	k1gMnPc7
mozdony	mozdona	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Maďarsko	Maďarsko	k1gNnSc1
</s>
