<s>
Šestinápravové	šestinápravový	k2eAgFnSc2d1
elektrické	elektrický	k2eAgFnSc2d1
lokomotivy	lokomotiva	k1gFnSc2
řady	řada	k1gFnSc2
630	#num#	k4
(	(	kIx(
<g/>
staré	starý	k2eAgNnSc1d1
označení	označení	k1gNnSc1
<g/>
:	:	kIx,
V	v	k7c6
<g/>
63	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přezdívané	přezdívaný	k2eAgInPc4d1
Gigant	gigant	k1gInSc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
nejvýkonnější	výkonný	k2eAgFnPc1d3
maďarské	maďarský	k2eAgFnPc1d1
lokomotivy	lokomotiva	k1gFnPc1
domácí	domácí	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>