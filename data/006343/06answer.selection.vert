<s>
Virginia	Virginium	k1gNnPc1	Virginium
Woolfová	Woolfový	k2eAgNnPc1d1	Woolfové
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Adeline	Adelin	k1gInSc5	Adelin
Virginia	Virginium	k1gNnSc2	Virginium
Stephen	Stephen	k1gInSc4	Stephen
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1882	[number]	k4	1882
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
významná	významný	k2eAgFnSc1d1	významná
anglická	anglický	k2eAgFnSc1d1	anglická
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc1d1	literární
kritička	kritička	k1gFnSc1	kritička
<g/>
,	,	kIx,	,
esejistka	esejistka	k1gFnSc1	esejistka
<g/>
,	,	kIx,	,
vydavatelka	vydavatelka	k1gFnSc1	vydavatelka
<g/>
,	,	kIx,	,
filozofka	filozofka	k1gFnSc1	filozofka
a	a	k8xC	a
feministka	feministka	k1gFnSc1	feministka
<g/>
.	.	kIx.	.
</s>
