<s>
Centrum	centrum	k1gNnSc1	centrum
sportovních	sportovní	k2eAgFnPc2d1	sportovní
aktivit	aktivita	k1gFnPc2	aktivita
(	(	kIx(	(
<g/>
CESA	CESA	kA	CESA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
celoškolským	celoškolský	k2eAgNnSc7d1	celoškolské
pracovištěm	pracoviště	k1gNnSc7	pracoviště
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
nepovinnou	povinný	k2eNgFnSc4d1	nepovinná
sportovní	sportovní	k2eAgFnSc4d1	sportovní
výuku	výuka	k1gFnSc4	výuka
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
bakalářského	bakalářský	k2eAgNnSc2d1	bakalářské
<g/>
,	,	kIx,	,
magisterského	magisterský	k2eAgNnSc2d1	magisterské
i	i	k8xC	i
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
všech	všecek	k3xTgFnPc2	všecek
fakult	fakulta	k1gFnPc2	fakulta
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
