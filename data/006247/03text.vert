<s>
Centrum	centrum	k1gNnSc1	centrum
sportovních	sportovní	k2eAgFnPc2d1	sportovní
aktivit	aktivita	k1gFnPc2	aktivita
(	(	kIx(	(
<g/>
CESA	CESA	kA	CESA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
celoškolským	celoškolský	k2eAgNnSc7d1	celoškolské
pracovištěm	pracoviště	k1gNnSc7	pracoviště
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
nepovinnou	povinný	k2eNgFnSc4d1	nepovinná
sportovní	sportovní	k2eAgFnSc4d1	sportovní
výuku	výuka	k1gFnSc4	výuka
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
bakalářského	bakalářský	k2eAgNnSc2d1	bakalářské
<g/>
,	,	kIx,	,
magisterského	magisterský	k2eAgNnSc2d1	magisterské
i	i	k8xC	i
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
všech	všecek	k3xTgFnPc2	všecek
fakult	fakulta	k1gFnPc2	fakulta
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
sportovišť	sportoviště	k1gNnPc2	sportoviště
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
největší	veliký	k2eAgFnSc2d3	veliký
víceúčelové	víceúčelový	k2eAgFnSc2d1	víceúčelová
haly	hala	k1gFnSc2	hala
pro	pro	k7c4	pro
sálové	sálový	k2eAgInPc4d1	sálový
sporty	sport	k1gInPc4	sport
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Pod	pod	k7c7	pod
Palackého	Palackého	k2eAgInSc7d1	Palackého
vrchem	vrch	k1gInSc7	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
podnikatelské	podnikatelský	k2eAgFnSc2d1	podnikatelská
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Centrem	centrum	k1gNnSc7	centrum
sportovních	sportovní	k2eAgFnPc2d1	sportovní
aktivit	aktivita	k1gFnPc2	aktivita
a	a	k8xC	a
fakultami	fakulta	k1gFnPc7	fakulta
stavební	stavební	k2eAgFnSc2d1	stavební
a	a	k8xC	a
chemickou	chemický	k2eAgFnSc4d1	chemická
vyučován	vyučovat	k5eAaImNgInS	vyučovat
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
Management	management	k1gInSc1	management
v	v	k7c6	v
tělesné	tělesný	k2eAgFnSc6d1	tělesná
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Přijímací	přijímací	k2eAgFnSc1d1	přijímací
zkouška	zkouška	k1gFnSc1	zkouška
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
písemného	písemný	k2eAgInSc2d1	písemný
testu	test	k1gInSc2	test
a	a	k8xC	a
z	z	k7c2	z
talentové	talentový	k2eAgFnSc2d1	talentová
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
.	.	kIx.	.
fitness	fitness	k6eAd1	fitness
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
funkční	funkční	k2eAgInSc4d1	funkční
trénink	trénink	k1gInSc4	trénink
kondiční	kondiční	k2eAgNnSc1d1	kondiční
posilování	posilování	k1gNnSc1	posilování
snowboard	snowboard	k1gInSc1	snowboard
aqua	aqua	k6eAd1	aqua
aerobik	aerobik	k1gInSc1	aerobik
futsal	futsat	k5eAaPmAgInS	futsat
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
softbal	softbal	k1gInSc1	softbal
atletika	atletika	k1gFnSc1	atletika
FYZIO	FYZIO	kA	FYZIO
program	program	k1gInSc4	program
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
formy	forma	k1gFnPc4	forma
(	(	kIx(	(
<g/>
cvičení	cvičení	k1gNnSc4	cvičení
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
)	)	kIx)	)
lyžování	lyžování	k1gNnSc4	lyžování
squash	squash	k1gInSc4	squash
badminton	badminton	k1gInSc1	badminton
golf	golf	k1gInSc1	golf
masáž	masáž	k1gFnSc1	masáž
a	a	k8xC	a
regenerace	regenerace	k1gFnSc2	regenerace
stolní	stolní	k2eAgInSc1d1	stolní
tenis	tenis	k1gInSc1	tenis
basketbal	basketbal	k1gInSc4	basketbal
<g />
.	.	kIx.	.
</s>
<s>
pohybové	pohybový	k2eAgFnPc1d1	pohybová
aktivity	aktivita	k1gFnPc1	aktivita
handicapovaných	handicapovaný	k2eAgFnPc2d1	handicapovaná
MTK	MTK	kA	MTK
střelba	střelba	k1gFnSc1	střelba
bojová	bojový	k2eAgNnPc1d1	bojové
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
formy	forma	k1gFnPc4	forma
(	(	kIx(	(
<g/>
karate	karate	k1gNnSc1	karate
<g/>
,	,	kIx,	,
karate	karate	k1gNnSc1	karate
do	do	k7c2	do
<g/>
,	,	kIx,	,
allkampf-jitsu	allkampfits	k1gInSc6	allkampf-jits
<g/>
,	,	kIx,	,
aikido	aikida	k1gFnSc5	aikida
<g/>
,	,	kIx,	,
kobudo	kobuda	k1gFnSc5	kobuda
<g/>
,	,	kIx,	,
taekwondo	taekwondo	k1gNnSc1	taekwondo
<g/>
,	,	kIx,	,
judo	judo	k1gNnSc1	judo
<g/>
)	)	kIx)	)
házená	házená	k1gFnSc1	házená
nohejbal	nohejbal	k1gInSc1	nohejbal
tanec	tanec	k1gInSc1	tanec
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
formy	forma	k1gFnSc2	forma
bowling	bowling	k1gInSc1	bowling
H.E.A.T.	H.E.A.T.	k1gMnSc1	H.E.A.T.
program	program	k1gInSc1	program
(	(	kIx(	(
<g/>
chodecké	chodecký	k2eAgInPc1d1	chodecký
trenažéry	trenažér	k1gInPc1	trenažér
<g/>
)	)	kIx)	)
nordic	nordic	k1gMnSc1	nordic
<g />
.	.	kIx.	.
</s>
<s>
walking	walking	k1gInSc1	walking
(	(	kIx(	(
<g/>
chůze	chůze	k1gFnSc1	chůze
s	s	k7c7	s
holemi	hole	k1gFnPc7	hole
<g/>
)	)	kIx)	)
tenis	tenis	k1gInSc1	tenis
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
formy	forma	k1gFnSc2	forma
břišní	břišní	k2eAgInSc1d1	břišní
tanec	tanec	k1gInSc1	tanec
halový	halový	k2eAgInSc4d1	halový
bouldering	bouldering	k1gInSc4	bouldering
a	a	k8xC	a
horolezectví	horolezectví	k1gNnSc4	horolezectví
plavání	plavání	k1gNnSc2	plavání
veslařské	veslařský	k2eAgInPc1d1	veslařský
trenažéry	trenažér	k1gInPc1	trenažér
(	(	kIx(	(
<g/>
indoor	indoor	k1gInSc1	indoor
rowing	rowing	k1gInSc1	rowing
<g/>
)	)	kIx)	)
bruslení	bruslení	k1gNnPc4	bruslení
horská	horský	k2eAgNnPc4d1	horské
kola	kolo	k1gNnPc4	kolo
plážový	plážový	k2eAgInSc4d1	plážový
volejbal	volejbal	k1gInSc4	volejbal
volejbal	volejbal	k1gInSc4	volejbal
capoeira	capoeira	k6eAd1	capoeira
indoorcycling	indoorcycling	k1gInSc1	indoorcycling
(	(	kIx(	(
<g/>
spinning	spinning	k1gInSc1	spinning
<g/>
)	)	kIx)	)
Port	port	k1gInSc1	port
de	de	k?	de
Bras	Bras	k1gInSc1	Bras
žonglování	žonglování	k1gNnSc1	žonglování
florbal	florbal	k1gInSc1	florbal
in-line	inin	k1gInSc5	in-lin
potápění	potápění	k1gNnSc2	potápění
fotbal	fotbal	k1gInSc4	fotbal
<g/>
/	/	kIx~	/
<g/>
malá	malý	k2eAgFnSc1d1	malá
kopaná	kopaný	k2eAgFnSc1d1	kopaná
kanoistika	kanoistika	k1gFnSc1	kanoistika
šachy	šach	k1gInPc1	šach
frisbee	frisbeat	k5eAaPmIp3nS	frisbeat
kondiční	kondiční	k2eAgInSc1d1	kondiční
trénink	trénink	k1gInSc1	trénink
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
formy	forma	k1gFnSc2	forma
sálová	sálový	k2eAgFnSc1d1	sálová
kopaná	kopaná	k1gFnSc1	kopaná
</s>
