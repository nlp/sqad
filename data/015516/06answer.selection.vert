<s>
Doporučená	doporučený	k2eAgFnSc1d1
denní	denní	k2eAgFnSc1d1
dávka	dávka	k1gFnSc1
(	(	kIx(
<g/>
DDD	DDD	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
angl.	angl.	k?
GDA	GDA	kA
-	-	kIx~
Guideline	Guidelin	k1gInSc5
Daily	Dail	k1gMnPc4
Amounts	Amounts	k1gInSc4
<g/>
,	,	kIx,
RDI	rdít	k5eAaImRp2nS
-	-	kIx~
Reference	reference	k1gFnPc1
Daily	Daila	k1gFnSc2
Intake	Intak	k1gFnSc2
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
RDA	RDA	kA
-	-	kIx~
Recommended	Recommended	k1gMnSc1
Dietary	Dietara	k1gFnSc2
Allowance	Allowance	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc4
používaný	používaný	k2eAgInSc4d1
v	v	k7c6
Česku	Česko	k1gNnSc6
pro	pro	k7c4
vyjádření	vyjádření	k1gNnSc4
potřebného	potřebný	k2eAgInSc2d1
individuálního	individuální	k2eAgInSc2d1
denního	denní	k2eAgInSc2d1
příjmu	příjem	k1gInSc2
živin	živina	k1gFnPc2
(	(	kIx(
<g/>
vitamínů	vitamín	k1gInPc2
<g/>
,	,	kIx,
minerálů	minerál	k1gInPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
považovaný	považovaný	k2eAgInSc1d1
za	za	k7c4
dostatečný	dostatečný	k2eAgInSc4d1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pokryl	pokrýt	k5eAaPmAgInS
potřebu	potřeba	k1gFnSc4
většiny	většina	k1gFnSc2
zdravých	zdravý	k2eAgMnPc2d1
jedinců	jedinec	k1gMnPc2
(	(	kIx(
<g/>
97	#num#	k4
%	%	kIx~
<g/>
-	-	kIx~
<g/>
98	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
bez	bez	k7c2
rozdílu	rozdíl	k1gInSc2
pohlaví	pohlaví	k1gNnSc2
<g/>
)	)	kIx)
v	v	k7c6
každé	každý	k3xTgFnSc6
věkové	věkový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
<g/>
.	.	kIx.
</s>