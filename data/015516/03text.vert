<s>
Doporučená	doporučený	k2eAgFnSc1d1
denní	denní	k2eAgFnSc1d1
dávka	dávka	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážel	odrážet	k5eAaImAgMnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaImRp2nP,k5eAaPmRp2nP
se	se	k3xPyFc4
též	též	k9
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
tam	tam	k6eAd1
nejsou	být	k5eNaImIp3nP
náměty	námět	k1gInPc4
k	k	k7c3
doplnění	doplnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Komentář	komentář	k1gInSc1
<g/>
:	:	kIx,
oba	dva	k4xCgInPc1
zmínené	zmínený	k2eAgInPc1d1
zákony	zákon	k1gInPc1
z	z	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
a	a	k8xC
2004	#num#	k4
jsou	být	k5eAaImIp3nP
dnes	dnes	k6eAd1
už	už	k6eAd1
neplatné	platný	k2eNgNnSc1d1
<g/>
,	,	kIx,
nahradila	nahradit	k5eAaPmAgFnS
je	být	k5eAaImIp3nS
39	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c4
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
zde	zde	k6eAd1
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
jakákoliv	jakýkoliv	k3yIgFnSc1
zmínka	zmínka	k1gFnSc1
</s>
<s>
Doporučená	doporučený	k2eAgFnSc1d1
denní	denní	k2eAgFnSc1d1
dávka	dávka	k1gFnSc1
(	(	kIx(
<g/>
DDD	DDD	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
angl.	angl.	k?
GDA	GDA	kA
-	-	kIx~
Guideline	Guidelin	k1gInSc5
Daily	Dail	k1gMnPc4
Amounts	Amounts	k1gInSc4
<g/>
,	,	kIx,
RDI	rdít	k5eAaImRp2nS
-	-	kIx~
Reference	reference	k1gFnPc1
Daily	Daila	k1gFnSc2
Intake	Intak	k1gFnSc2
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
RDA	RDA	kA
-	-	kIx~
Recommended	Recommended	k1gMnSc1
Dietary	Dietara	k1gFnSc2
Allowance	Allowance	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc4
používaný	používaný	k2eAgInSc4d1
v	v	k7c6
Česku	Česko	k1gNnSc6
pro	pro	k7c4
vyjádření	vyjádření	k1gNnSc4
potřebného	potřebný	k2eAgInSc2d1
individuálního	individuální	k2eAgInSc2d1
denního	denní	k2eAgInSc2d1
příjmu	příjem	k1gInSc2
živin	živina	k1gFnPc2
(	(	kIx(
<g/>
vitamínů	vitamín	k1gInPc2
<g/>
,	,	kIx,
minerálů	minerál	k1gInPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
považovaný	považovaný	k2eAgInSc1d1
za	za	k7c4
dostatečný	dostatečný	k2eAgInSc4d1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pokryl	pokrýt	k5eAaPmAgInS
potřebu	potřeba	k1gFnSc4
většiny	většina	k1gFnSc2
zdravých	zdravý	k2eAgMnPc2d1
jedinců	jedinec	k1gMnPc2
(	(	kIx(
<g/>
97	#num#	k4
%	%	kIx~
<g/>
-	-	kIx~
<g/>
98	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
bez	bez	k7c2
rozdílu	rozdíl	k1gInSc2
pohlaví	pohlaví	k1gNnSc2
<g/>
)	)	kIx)
v	v	k7c6
každé	každý	k3xTgFnSc6
věkové	věkový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Například	například	k6eAd1
základem	základ	k1gInSc7
pro	pro	k7c4
DDD	DDD	kA
určitého	určitý	k2eAgInSc2d1
vitamínu	vitamín	k1gInSc2
je	být	k5eAaImIp3nS
množství	množství	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
k	k	k7c3
udržení	udržení	k1gNnSc3
jeho	jeho	k3xOp3gFnSc2
správné	správný	k2eAgFnSc2d1
hladiny	hladina	k1gFnSc2
v	v	k7c6
krvi	krev	k1gFnSc6
u	u	k7c2
testované	testovaný	k2eAgFnSc2d1
zdravé	zdravý	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
hodnoty	hodnota	k1gFnPc4
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
interpretovat	interpretovat	k5eAaBmF
různě	různě	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
od	od	k7c2
sebe	se	k3xPyFc2
lišit	lišit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porovnáme	porovnat	k5eAaPmIp1nP
<g/>
-li	-li	k?
například	například	k6eAd1
DDD	DDD	kA
některých	některý	k3yIgInPc2
nejdůležitějších	důležitý	k2eAgInPc2d3
vitamínů	vitamín	k1gInPc2
a	a	k8xC
minerálů	minerál	k1gInPc2
s	s	k7c7
hodnotami	hodnota	k1gFnPc7
udávanými	udávaný	k2eAgFnPc7d1
ve	v	k7c6
státech	stát	k1gInPc6
EU	EU	kA
(	(	kIx(
<g/>
RDI	rdít	k5eAaImRp2nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zjistíme	zjistit	k5eAaPmIp1nP
<g/>
,	,	kIx,
že	že	k8xS
hodnoty	hodnota	k1gFnPc4
jsou	být	k5eAaImIp3nP
si	se	k3xPyFc3
velmi	velmi	k6eAd1
podobné	podobný	k2eAgInPc1d1
<g/>
:	:	kIx,
například	například	k6eAd1
DDD	DDD	kA
vitamínu	vitamín	k1gInSc2
C	C	kA
v	v	k7c6
EU	EU	kA
je	být	k5eAaImIp3nS
60	#num#	k4
mg	mg	kA
<g/>
,	,	kIx,
DDD	DDD	kA
riboflavinu	riboflavin	k1gInSc2
v	v	k7c6
ČR	ČR	kA
je	být	k5eAaImIp3nS
1,5	1,5	k4
mg	mg	kA
<g/>
,	,	kIx,
v	v	k7c6
EU	EU	kA
1,6	1,6	k4
mg	mg	kA
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgNnPc1
čísla	číslo	k1gNnPc1
nejsou	být	k5eNaImIp3nP
hodnotami	hodnota	k1gFnPc7
doporučovanými	doporučovaný	k2eAgFnPc7d1
pro	pro	k7c4
optimální	optimální	k2eAgFnSc4d1
výživu	výživa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
tak	tak	k6eAd1
zvláště	zvláště	k6eAd1
v	v	k7c6
případě	případ	k1gInSc6
příjmu	příjem	k1gInSc2
vitaminů	vitamin	k1gInPc2
a	a	k8xC
minerálů	minerál	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
pokládán	pokládat	k5eAaImNgInS
za	za	k7c4
nezbytný	nezbytný	k2eAgInSc4d1,k2eNgInSc4d1
k	k	k7c3
prevenci	prevence	k1gFnSc3
degenerativních	degenerativní	k2eAgFnPc2d1
nemocí	nemoc	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
rakovina	rakovina	k1gFnSc1
a	a	k8xC
aterosklerotické	aterosklerotický	k2eAgNnSc1d1
onemocnění	onemocnění	k1gNnSc1
srdce	srdce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
bylo	být	k5eAaImAgNnS
provedeno	provést	k5eAaPmNgNnS
mnoho	mnoho	k4c1
výzkumů	výzkum	k1gInPc2
<g/>
,	,	kIx,
o	o	k7c6
definitivním	definitivní	k2eAgNnSc6d1
vyhodnocení	vyhodnocení	k1gNnSc6
optimálního	optimální	k2eAgInSc2d1
příjmu	příjem	k1gInSc2
vitamínů	vitamín	k1gInPc2
a	a	k8xC
minerálů	minerál	k1gInPc2
se	se	k3xPyFc4
stále	stále	k6eAd1
diskutuje	diskutovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určité	určitý	k2eAgFnPc4d1
směrnice	směrnice	k1gFnPc4
pro	pro	k7c4
optimální	optimální	k2eAgFnPc4d1
dávky	dávka	k1gFnPc4
byly	být	k5eAaImAgFnP
již	již	k6eAd1
vypracovány	vypracovat	k5eAaPmNgFnP
<g/>
,	,	kIx,
ale	ale	k8xC
nebyly	být	k5eNaImAgFnP
dosud	dosud	k6eAd1
všeobecně	všeobecně	k6eAd1
přijaty	přijmout	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
Pouze	pouze	k6eAd1
v	v	k7c6
případě	případ	k1gInSc6
tuku	tuk	k1gInSc2
jako	jako	k8xC,k8xS
zdroje	zdroj	k1gInSc2
energie	energie	k1gFnSc2
z	z	k7c2
potravy	potrava	k1gFnSc2
jsou	být	k5eAaImIp3nP
optimální	optimální	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
celkem	celkem	k6eAd1
jednotné	jednotný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doporučuje	doporučovat	k5eAaImIp3nS
se	se	k3xPyFc4
tukem	tuk	k1gInSc7
hradit	hradit	k5eAaImF
pouze	pouze	k6eAd1
33	#num#	k4
%	%	kIx~
tělesné	tělesný	k2eAgFnSc2d1
energie	energie	k1gFnSc2
a	a	k8xC
z	z	k7c2
toho	ten	k3xDgNnSc2
ne	ne	k9
více	hodně	k6eAd2
než	než	k8xS
10	#num#	k4
%	%	kIx~
z	z	k7c2
nasycených	nasycený	k2eAgFnPc2d1
mastných	mastný	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vitamíny	vitamín	k1gInPc1
a	a	k8xC
minerální	minerální	k2eAgFnPc1d1
látky	látka	k1gFnPc1
a	a	k8xC
jejich	jejich	k3xOp3gFnPc4
doporučené	doporučený	k2eAgFnPc4d1
denní	denní	k2eAgFnPc4d1
dávky	dávka	k1gFnPc4
dle	dle	k7c2
vyhlášky	vyhláška	k1gFnSc2
číslo	číslo	k1gNnSc1
450	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
a	a	k8xC
novely	novela	k1gFnPc1
330	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
a	a	k8xC
dle	dle	k7c2
Nařízení	nařízení	k1gNnSc2
EP	EP	kA
č.	č.	k?
1169	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
</s>
<s>
Název	název	k1gInSc1
látkyJednotkaMnožství	látkyJednotkaMnožství	k1gNnSc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
Množství	množství	k1gNnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
Množství	množství	k1gNnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vitamín	vitamín	k1gInSc1
Aμ	Aμ	k1gInSc1
<g/>
800800800	#num#	k4
</s>
<s>
Vitamín	vitamín	k1gInSc1
B1	B1	k1gFnSc2
-	-	kIx~
Thiaminmg	Thiaminmg	k1gInSc1
<g/>
1,41	1,41	k4
<g/>
,11	,11	k4
<g/>
,1	,1	k4
</s>
<s>
Vitamín	vitamín	k1gInSc1
B2	B2	k1gFnSc2
-	-	kIx~
Riboflavinmg	Riboflavinmg	k1gInSc1
<g/>
1,61	1,61	k4
<g/>
,41	,41	k4
<g/>
,4	,4	k4
</s>
<s>
Vitamín	vitamín	k1gInSc1
B3	B3	k1gFnSc2
-	-	kIx~
Niacinmg	Niacinmg	k1gInSc1
<g/>
181616	#num#	k4
</s>
<s>
Vitamín	vitamín	k1gInSc1
B5	B5	k1gFnSc1
-	-	kIx~
Kyselina	kyselina	k1gFnSc1
pantothenovámg	pantothenovámg	k1gInSc1
<g/>
666	#num#	k4
</s>
<s>
Vitamín	vitamín	k1gInSc1
B6	B6	k1gFnSc2
-	-	kIx~
Pyridoxinmg	Pyridoxinmg	k1gInSc1
<g/>
21,41	21,41	k4
<g/>
,4	,4	k4
</s>
<s>
Vitamín	vitamín	k1gInSc1
B7	B7	k1gFnSc2
-	-	kIx~
vitamín	vitamín	k1gInSc1
H	H	kA
<g/>
|	|	kIx~
<g/>
Biotinμ	Biotinμ	k1gInSc1
<g/>
1505050	#num#	k4
</s>
<s>
Vitamín	vitamín	k1gInSc1
B9	B9	k1gFnSc1
-	-	kIx~
Kyselina	kyselina	k1gFnSc1
listováμ	listováμ	k1gInSc1
<g/>
200200200	#num#	k4
</s>
<s>
Vitamín	vitamín	k1gInSc1
B12	B12	k1gFnSc2
-	-	kIx~
Kobalaminμ	Kobalaminμ	k1gInSc1
<g/>
12,52	12,52	k4
<g/>
,5	,5	k4
</s>
<s>
Vitamín	vitamín	k1gInSc1
Cmg	Cmg	k1gFnSc2
<g/>
608080	#num#	k4
</s>
<s>
Vitamín	vitamín	k1gInSc1
Dμ	Dμ	k1gInSc1
<g/>
1055	#num#	k4
</s>
<s>
Vitamín	vitamín	k1gInSc1
Emg	Emg	k1gFnSc2
<g/>
101212	#num#	k4
</s>
<s>
Vitamín	vitamín	k1gInSc1
Kμ	Kμ	k1gInSc1
<g/>
757575	#num#	k4
</s>
<s>
Chromμ	Chromμ	k1gInSc1
<g/>
404040	#num#	k4
</s>
<s>
Jódμ	Jódμ	k1gInSc1
<g/>
150150150	#num#	k4
</s>
<s>
Manganmg	Manganmg	k1gInSc1
<g/>
222	#num#	k4
</s>
<s>
Měďmg	Měďmg	k1gInSc1
<g/>
111	#num#	k4
</s>
<s>
Draslíkmg	Draslíkmg	k1gInSc1
<g/>
200020002000	#num#	k4
</s>
<s>
Fosformg	Fosformg	k1gInSc1
<g/>
800700700	#num#	k4
</s>
<s>
Hořčíkmg	Hořčíkmg	k1gInSc1
<g/>
300375375	#num#	k4
</s>
<s>
Selenμ	Selenμ	k1gInSc1
<g/>
555555	#num#	k4
</s>
<s>
Vápníkmg	Vápníkmg	k1gInSc1
<g/>
800800800	#num#	k4
</s>
<s>
Zinekmg	Zinekmg	k1gInSc1
<g/>
151010	#num#	k4
</s>
<s>
Železomg	Železomg	k1gInSc1
<g/>
141414	#num#	k4
</s>
<s>
Chlormg--	Chlormg--	k?
<g/>
800	#num#	k4
</s>
<s>
Fluormg--	Fluormg--	k?
<g/>
3,5	3,5	k4
</s>
<s>
Molybdenμ	Molybdenμ	k?
<g/>
50	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
DDD	DDD	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Novela	novela	k1gFnSc1
330	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc7,k3yRgFnSc7,k3yIgFnSc7
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
vyhláška	vyhláška	k1gFnSc1
Ministerstva	ministerstvo	k1gNnSc2
zdravotnictví	zdravotnictví	k1gNnSc2
č.	č.	k?
450	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c4
označování	označování	k1gNnSc4
výživové	výživový	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
potravin	potravina	k1gFnPc2
</s>
<s>
Nařízení	nařízení	k1gNnSc1
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
a	a	k8xC
Rady	rada	k1gFnSc2
(	(	kIx(
<g/>
EU	EU	kA
<g/>
)	)	kIx)
č.	č.	k?
1169	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2011	#num#	k4
o	o	k7c6
poskytování	poskytování	k1gNnSc6
informací	informace	k1gFnPc2
o	o	k7c6
potravinách	potravina	k1gFnPc6
spotřebitelům	spotřebitel	k1gMnPc3
</s>
