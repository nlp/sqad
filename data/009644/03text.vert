<p>
<s>
Dobytek	dobytek	k1gInSc1	dobytek
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
sudokopytníků	sudokopytník	k1gMnPc2	sudokopytník
chovaných	chovaný	k2eAgMnPc2d1	chovaný
pro	pro	k7c4	pro
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybraný	vybraný	k2eAgInSc4d1	vybraný
dobytek	dobytek	k1gInSc4	dobytek
==	==	k?	==
</s>
</p>
<p>
<s>
lama	lama	k1gFnSc1	lama
alpaka	alpaka	k1gFnSc1	alpaka
<g/>
,	,	kIx,	,
vikuňa	vikuňa	k1gFnSc1	vikuňa
<g/>
,	,	kIx,	,
lama	lama	k1gFnSc1	lama
krotká	krotký	k2eAgFnSc1d1	krotká
<g/>
,	,	kIx,	,
guanako	guanako	k6eAd1	guanako
</s>
</p>
<p>
<s>
koza	koza	k1gFnSc1	koza
domácí	domácí	k2eAgFnSc2d1	domácí
</s>
</p>
<p>
<s>
ovce	ovce	k1gFnSc1	ovce
domácí	domácí	k2eAgFnSc2d1	domácí
</s>
</p>
<p>
<s>
prase	prase	k1gNnSc1	prase
domácí	domácí	k2eAgFnSc2d1	domácí
</s>
</p>
<p>
<s>
tur	tur	k1gMnSc1	tur
domácí	domácí	k1gMnSc1	domácí
<g/>
,	,	kIx,	,
zebu	zebu	k1gMnSc1	zebu
<g/>
,	,	kIx,	,
buvol	buvol	k1gMnSc1	buvol
<g/>
,	,	kIx,	,
gayal	gayal	k1gMnSc1	gayal
<g/>
,	,	kIx,	,
banteng	banteng	k1gMnSc1	banteng
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
skot	skot	k1gInSc1	skot
či	či	k8xC	či
hovězí	hovězí	k2eAgInSc4d1	hovězí
dobytek	dobytek	k1gInSc4	dobytek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dobytek	dobytek	k1gInSc1	dobytek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
dobytek	dobytek	k1gInSc1	dobytek
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
