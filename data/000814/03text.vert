<s>
Michael	Michael	k1gMnSc1	Michael
Edward	Edward	k1gMnSc1	Edward
Palin	Palin	k1gMnSc1	Palin
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
Broomhill	Broomhill	k1gInSc1	Broomhill
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
komik	komik	k1gMnSc1	komik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
šesti	šest	k4xCc2	šest
členů	člen	k1gMnPc2	člen
komediální	komediální	k2eAgFnSc2d1	komediální
skupiny	skupina	k1gFnSc2	skupina
Monty	Monta	k1gFnSc2	Monta
Python	Python	k1gMnSc1	Python
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
historii	historie	k1gFnSc4	historie
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Terrym	Terrym	k1gInSc1	Terrym
Jonesem	Jones	k1gInSc7	Jones
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
později	pozdě	k6eAd2	pozdě
společně	společně	k6eAd1	společně
psal	psát	k5eAaImAgMnS	psát
skeče	skeč	k1gInPc4	skeč
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
show	show	k1gFnPc4	show
na	na	k7c6	na
BBC	BBC	kA	BBC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgInS	vzít
svou	svůj	k3xOyFgFnSc4	svůj
současnou	současný	k2eAgFnSc4d1	současná
manželku	manželka	k1gFnSc4	manželka
Helenu	Helena	k1gFnSc4	Helena
Gibbinsovou	Gibbinsová	k1gFnSc4	Gibbinsová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaImF	věnovat
natáčení	natáčení	k1gNnSc4	natáčení
cestovních	cestovní	k2eAgInPc2d1	cestovní
dokumentů	dokument	k1gInPc2	dokument
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gNnSc4	jeho
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Od	od	k7c2	od
pólu	pól	k1gInSc2	pól
k	k	k7c3	k
pólu	pól	k1gInSc3	pól
nebo	nebo	k8xC	nebo
Kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
za	za	k7c4	za
80	[number]	k4	80
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gNnSc2	jeho
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
udělen	udělen	k2eAgInSc1d1	udělen
Řád	řád	k1gInSc1	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
do	do	k7c2	do
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
prezidentem	prezident	k1gMnSc7	prezident
Royal	Royal	k1gInSc4	Royal
Geographical	Geographical	k1gFnSc2	Geographical
Society	societa	k1gFnSc2	societa
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Michael	Michael	k1gMnSc1	Michael
Palin	Palin	k2eAgMnSc1d1	Palin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Michael	Michael	k1gMnSc1	Michael
Palin	Palin	k1gMnSc1	Palin
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Palinův	Palinův	k2eAgInSc1d1	Palinův
cestovatelský	cestovatelský	k2eAgInSc1d1	cestovatelský
web	web	k1gInSc1	web
</s>
