<s>
Studoval	studovat	k5eAaImAgInS	studovat
historii	historie	k1gFnSc4	historie
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Terrym	Terrym	k1gInSc1	Terrym
Jonesem	Jones	k1gInSc7	Jones
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
později	pozdě	k6eAd2	pozdě
společně	společně	k6eAd1	společně
psal	psát	k5eAaImAgMnS	psát
skeče	skeč	k1gInPc4	skeč
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
show	show	k1gFnPc4	show
na	na	k7c6	na
BBC	BBC	kA	BBC
<g/>
.	.	kIx.	.
</s>
