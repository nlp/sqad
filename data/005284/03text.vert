<s>
Parnas	Parnas	k1gInSc1	Parnas
(	(	kIx(	(
<g/>
také	také	k9	také
Parnassus	Parnassus	k1gInSc1	Parnassus
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Π	Π	k?	Π
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pohoří	pohoří	k1gNnSc1	pohoří
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Korintského	korintský	k2eAgInSc2d1	korintský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
je	být	k5eAaImIp3nS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
výrazným	výrazný	k2eAgInSc7d1	výrazný
velehorským	velehorský	k2eAgInSc7d1	velehorský
reliéfem	reliéf	k1gInSc7	reliéf
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
vděčí	vděčit	k5eAaImIp3nS	vděčit
zejména	zejména	k6eAd1	zejména
zkrasovatělému	zkrasovatělý	k2eAgInSc3d1	zkrasovatělý
vápenci	vápenec	k1gInSc3	vápenec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
hlavním	hlavní	k2eAgInSc7d1	hlavní
stavebním	stavební	k2eAgInSc7d1	stavební
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
Liakoura	Liakoura	k1gFnSc1	Liakoura
(	(	kIx(	(
<g/>
2457	[number]	k4	2457
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Parnas	Parnas	k1gInSc1	Parnas
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
zasvěcen	zasvětit	k5eAaPmNgMnS	zasvětit
Apollonovi	Apollo	k1gMnSc3	Apollo
a	a	k8xC	a
múzám	múza	k1gFnPc3	múza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
hor	hora	k1gFnPc2	hora
stála	stát	k5eAaImAgFnS	stát
slavná	slavný	k2eAgFnSc1d1	slavná
Věštírna	věštírna	k1gFnSc1	věštírna
v	v	k7c6	v
Delfách	Delfy	k1gFnPc6	Delfy
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
a	a	k8xC	a
východ	východ	k1gInSc1	východ
pohoří	pohoří	k1gNnSc2	pohoří
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
údolí	údolí	k1gNnSc4	údolí
řeky	řeka	k1gFnSc2	řeka
Kilisos	Kilisosa	k1gFnPc2	Kilisosa
<g/>
.	.	kIx.	.
</s>
<s>
Jih	jih	k1gInSc1	jih
a	a	k8xC	a
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgInS	obklopit
Korintským	korintský	k2eAgInSc7d1	korintský
zálivem	záliv	k1gInSc7	záliv
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
horstvo	horstvo	k1gNnSc1	horstvo
svažuje	svažovat	k5eAaImIp3nS	svažovat
<g/>
.	.	kIx.	.
</s>
<s>
Skrz	skrz	k7c4	skrz
pohoří	pohoří	k1gNnSc4	pohoří
vede	vést	k5eAaImIp3nS	vést
horská	horský	k2eAgFnSc1d1	horská
silnice	silnice	k1gFnSc1	silnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
bodě	bod	k1gInSc6	bod
překračuje	překračovat	k5eAaImIp3nS	překračovat
silniční	silniční	k2eAgNnSc1d1	silniční
sedlo	sedlo	k1gNnSc1	sedlo
Drosohori	Drosohor	k1gFnSc2	Drosohor
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
od	od	k7c2	od
sebe	se	k3xPyFc2	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Parnas	Parnas	k1gInSc1	Parnas
a	a	k8xC	a
sousední	sousední	k2eAgInSc1d1	sousední
masiv	masiv	k1gInSc1	masiv
Giona	Gion	k1gInSc2	Gion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
běžně	běžně	k6eAd1	běžně
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
velké	velký	k2eAgInPc4d1	velký
ledovcové	ledovcový	k2eAgInPc4d1	ledovcový
kary	kar	k1gInPc4	kar
<g/>
,	,	kIx,	,
morény	moréna	k1gFnPc4	moréna
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
stopy	stopa	k1gFnPc4	stopa
po	po	k7c6	po
nejjižnějších	jižní	k2eAgInPc6d3	nejjižnější
čtvrtohorních	čtvrtohorní	k2eAgInPc6d1	čtvrtohorní
ledovcích	ledovec	k1gInPc6	ledovec
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrchol	vrchol	k1gInSc1	vrchol
Liakoura	Liakour	k1gMnSc2	Liakour
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
protější	protější	k2eAgInSc1d1	protější
hřeben	hřeben	k1gInSc1	hřeben
Gerontovrachos	Gerontovrachosa	k1gFnPc2	Gerontovrachosa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
1300	[number]	k4	1300
-	-	kIx~	-
1730	[number]	k4	1730
m	m	kA	m
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
chránit	chránit	k5eAaImF	chránit
především	především	k9	především
vzácnou	vzácný	k2eAgFnSc4d1	vzácná
floru	flora	k1gFnSc4	flora
(	(	kIx(	(
<g/>
jedle	jedle	k6eAd1	jedle
kefalonská	kefalonský	k2eAgFnSc1d1	kefalonská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fauna	fauna	k1gFnSc1	fauna
zde	zde	k6eAd1	zde
prakticky	prakticky	k6eAd1	prakticky
není	být	k5eNaImIp3nS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
žádným	žádný	k3yNgMnSc7	žádný
větším	veliký	k2eAgMnSc7d2	veliký
savcem	savec	k1gMnSc7	savec
<g/>
.	.	kIx.	.
</s>
<s>
Liakoura	Liakoura	k1gFnSc1	Liakoura
(	(	kIx(	(
<g/>
2457	[number]	k4	2457
m	m	kA	m
<g/>
)	)	kIx)	)
Tsárkos	Tsárkos	k1gMnSc1	Tsárkos
(	(	kIx(	(
<g/>
2415	[number]	k4	2415
m	m	kA	m
<g/>
)	)	kIx)	)
Jerontóvrachos	Jerontóvrachos	k1gMnSc1	Jerontóvrachos
(	(	kIx(	(
<g/>
2395	[number]	k4	2395
m	m	kA	m
<g/>
)	)	kIx)	)
Kalójiros	Kalójirosa	k1gFnPc2	Kalójirosa
(	(	kIx(	(
<g/>
2327	[number]	k4	2327
m	m	kA	m
<g/>
)	)	kIx)	)
Mávra	Mávra	k1gFnSc1	Mávra
Lithária	Lithárium	k1gNnSc2	Lithárium
(	(	kIx(	(
<g/>
2326	[number]	k4	2326
m	m	kA	m
<g/>
)	)	kIx)	)
Voidomáti	Voidomát	k1gMnPc1	Voidomát
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
m	m	kA	m
<g/>
)	)	kIx)	)
Petrítis	Petrítis	k1gInSc1	Petrítis
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
m	m	kA	m
<g/>
)	)	kIx)	)
Pyrgákia	Pyrgákius	k1gMnSc2	Pyrgákius
(	(	kIx(	(
<g/>
1718	[number]	k4	1718
m	m	kA	m
<g/>
)	)	kIx)	)
Na	na	k7c6	na
několika	několik	k4yIc6	několik
svazích	svah	k1gInPc6	svah
jsou	být	k5eAaImIp3nP	být
vybudována	vybudován	k2eAgNnPc1d1	vybudováno
zimní	zimní	k2eAgNnPc1d1	zimní
lyžařská	lyžařský	k2eAgNnPc1d1	lyžařské
střediska	středisko	k1gNnPc1	středisko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Parnas	Parnas	k1gInSc1	Parnas
je	on	k3xPp3gNnSc4	on
znám	znát	k5eAaImIp1nS	znát
svými	svůj	k3xOyFgFnPc7	svůj
příznivými	příznivý	k2eAgFnPc7d1	příznivá
sněhovými	sněhový	k2eAgFnPc7d1	sněhová
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Parnas	Parnas	k1gInSc1	Parnas
je	být	k5eAaImIp3nS	být
mýtická	mýtický	k2eAgFnSc1d1	mýtická
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
zastavila	zastavit	k5eAaPmAgFnS	zastavit
archa	archa	k1gFnSc1	archa
postavená	postavený	k2eAgFnSc1d1	postavená
Deukaliónem	Deukalión	k1gInSc7	Deukalión
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
Pyrrhou	Pyrrha	k1gFnSc7	Pyrrha
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
bůh	bůh	k1gMnSc1	bůh
Zeus	Zeusa	k1gFnPc2	Zeusa
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
potrestat	potrestat	k5eAaPmF	potrestat
lidstvo	lidstvo	k1gNnSc4	lidstvo
za	za	k7c4	za
prostopášný	prostopášný	k2eAgInSc4d1	prostopášný
život	život	k1gInSc4	život
a	a	k8xC	a
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nevážili	vážit	k5eNaImAgMnP	vážit
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
seslal	seslat	k5eAaPmAgMnS	seslat
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
velkou	velký	k2eAgFnSc4d1	velká
potopu	potopa	k1gFnSc4	potopa
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahubila	zahubit	k5eAaPmAgFnS	zahubit
celé	celý	k2eAgNnSc4d1	celé
lidské	lidský	k2eAgNnSc4d1	lidské
pokolení	pokolení	k1gNnSc4	pokolení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
bohů	bůh	k1gMnPc2	bůh
zůstal	zůstat	k5eAaPmAgInS	zůstat
jenom	jenom	k9	jenom
Deukalión	Deukalión	k1gInSc1	Deukalión
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
znovu	znovu	k6eAd1	znovu
zalidnit	zalidnit	k5eAaPmF	zalidnit
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Udělal	udělat	k5eAaPmAgMnS	udělat
to	ten	k3xDgNnSc4	ten
tím	ten	k3xDgInSc7	ten
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
rady	rada	k1gFnSc2	rada
bohů	bůh	k1gMnPc2	bůh
házel	házet	k5eAaImAgMnS	házet
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
přeneseně	přeneseně	k6eAd1	přeneseně
"	"	kIx"	"
<g/>
kosti	kost	k1gFnPc1	kost
země	zem	k1gFnPc1	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těch	ten	k3xDgInPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
hodil	hodit	k5eAaPmAgInS	hodit
Deukalión	Deukalión	k1gInSc1	Deukalión
<g/>
,	,	kIx,	,
povstali	povstat	k5eAaPmAgMnP	povstat
noví	nový	k2eAgMnPc1d1	nový
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
za	za	k7c7	za
Pyrrhou	Pyrrha	k1gFnSc7	Pyrrha
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
z	z	k7c2	z
kamenů	kámen	k1gInPc2	kámen
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgFnSc2d1	řecká
hory	hora	k1gFnSc2	hora
Kašna	kašna	k1gFnSc1	kašna
Parnas	Parnas	k1gInSc1	Parnas
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
Quartier	Quartiero	k1gNnPc2	Quartiero
du	du	k?	du
Montparnasse	Montparnasse	k1gFnSc2	Montparnasse
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Parnas	Parnas	k1gInSc1	Parnas
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
