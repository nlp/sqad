<p>
<s>
Žeberská	Žeberský	k2eAgFnSc1d1	Žeberská
lípa	lípa	k1gFnSc1	lípa
je	být	k5eAaImIp3nS	být
ojedinělou	ojedinělý	k2eAgFnSc7d1	ojedinělá
ukázkou	ukázka	k1gFnSc7	ukázka
pokročilého	pokročilý	k2eAgNnSc2d1	pokročilé
stádia	stádium	k1gNnSc2	stádium
životního	životní	k2eAgInSc2d1	životní
cyklu	cyklus	k1gInSc2	cyklus
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysokému	vysoký	k2eAgInSc3d1	vysoký
věku	věk	k1gInSc3	věk
je	být	k5eAaImIp3nS	být
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
nejstarší	starý	k2eAgInSc4d3	nejstarší
strom	strom	k1gInSc4	strom
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
získala	získat	k5eAaPmAgFnS	získat
podle	podle	k7c2	podle
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
zříceniny	zřícenina	k1gFnSc2	zřícenina
hradu	hrad	k1gInSc2	hrad
Starý	starý	k2eAgInSc4d1	starý
Žeberk	Žeberk	k1gInSc4	Žeberk
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
na	na	k7c4	na
úpatí	úpatí	k1gNnPc4	úpatí
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
hranici	hranice	k1gFnSc6	hranice
Národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Jezerka	Jezerka	k1gFnSc1	Jezerka
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Vesnického	vesnický	k2eAgInSc2d1	vesnický
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
2,2	[number]	k4	2,2
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
zámku	zámek	k1gInSc2	zámek
Jezeří	Jezeří	k1gNnSc2	Jezeří
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Kundratice	Kundratice	k1gFnSc2	Kundratice
u	u	k7c2	u
Chomutova	Chomutov	k1gInSc2	Chomutov
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
stromu	strom	k1gInSc2	strom
vede	vést	k5eAaImIp3nS	vést
žlutě	žlutě	k6eAd1	žlutě
značená	značený	k2eAgFnSc1d1	značená
turistická	turistický	k2eAgFnSc1d1	turistická
stezka	stezka	k1gFnSc1	stezka
od	od	k7c2	od
zámku	zámek	k1gInSc2	zámek
do	do	k7c2	do
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
Pece	Pec	k1gFnSc2	Pec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
název	název	k1gInSc1	název
<g/>
:	:	kIx,	:
Žeberská	Žeberský	k2eAgFnSc1d1	Žeberská
lípa	lípa	k1gFnSc1	lípa
</s>
</p>
<p>
<s>
výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
7	[number]	k4	7
m	m	kA	m
<g/>
,	,	kIx,	,
6	[number]	k4	6
m	m	kA	m
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
výška	výška	k1gFnSc1	výška
koruny	koruna	k1gFnSc2	koruna
<g/>
:	:	kIx,	:
5	[number]	k4	5
m	m	kA	m
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
šířka	šířka	k1gFnSc1	šířka
koruny	koruna	k1gFnSc2	koruna
<g/>
:	:	kIx,	:
10	[number]	k4	10
m	m	kA	m
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
obvod	obvod	k1gInSc1	obvod
<g/>
:	:	kIx,	:
770	[number]	k4	770
cm	cm	kA	cm
<g/>
,	,	kIx,	,
600	[number]	k4	600
cm	cm	kA	cm
<g/>
,	,	kIx,	,
740	[number]	k4	740
cm	cm	kA	cm
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
věk	věk	k1gInSc1	věk
<g/>
:	:	kIx,	:
700	[number]	k4	700
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
700	[number]	k4	700
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
700	[number]	k4	700
<g/>
–	–	k?	–
<g/>
900	[number]	k4	900
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
finalista	finalista	k1gMnSc1	finalista
soutěže	soutěž	k1gFnSc2	soutěž
Strom	strom	k1gInSc1	strom
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sanace	sanace	k1gFnSc1	sanace
<g/>
:	:	kIx,	:
ano	ano	k9	ano
</s>
</p>
<p>
<s>
souřadnice	souřadnice	k1gFnSc1	souřadnice
<g/>
:	:	kIx,	:
50	[number]	k4	50
<g/>
°	°	k?	°
<g/>
32	[number]	k4	32
<g/>
'	'	kIx"	'
<g/>
26.55	[number]	k4	26.55
<g/>
"	"	kIx"	"
<g/>
N	N	kA	N
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
°	°	k?	°
<g/>
28	[number]	k4	28
<g/>
'	'	kIx"	'
<g/>
49.75	[number]	k4	49.75
<g/>
"	"	kIx"	"
<g/>
ELípa	ELípa	k1gFnSc1	ELípa
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
jako	jako	k8xS	jako
památný	památný	k2eAgInSc4d1	památný
strom	strom	k1gInSc4	strom
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stav	stav	k1gInSc1	stav
stromu	strom	k1gInSc2	strom
a	a	k8xC	a
údržba	údržba	k1gFnSc1	údržba
==	==	k?	==
</s>
</p>
<p>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
stál	stát	k5eAaImAgInS	stát
kmen	kmen	k1gInSc1	kmen
lípy	lípa	k1gFnSc2	lípa
vzpřímeně	vzpřímeně	k6eAd1	vzpřímeně
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
patrné	patrný	k2eAgNnSc1d1	patrné
rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
kosterní	kosterní	k2eAgFnPc4d1	kosterní
větve	větev	k1gFnPc4	větev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zbyla	zbýt	k5eAaPmAgFnS	zbýt
již	již	k9	již
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
ohnutá	ohnutý	k2eAgFnSc1d1	ohnutá
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
lípy	lípa	k1gFnSc2	lípa
má	mít	k5eAaImIp3nS	mít
dutinu	dutina	k1gFnSc4	dutina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
ošetřená	ošetřený	k2eAgFnSc1d1	ošetřená
asfaltem	asfalt	k1gInSc7	asfalt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
nezatékalo	zatékat	k5eNaImAgNnS	zatékat
<g/>
.	.	kIx.	.
</s>
<s>
Strom	strom	k1gInSc1	strom
už	už	k9	už
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
podepřen	podepřen	k2eAgMnSc1d1	podepřen
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
kmene	kmen	k1gInSc2	kmen
hnízdily	hnízdit	k5eAaImAgInP	hnízdit
sovy	sova	k1gFnPc4	sova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
vynecháme	vynechat	k5eAaPmIp1nP	vynechat
novodobou	novodobý	k2eAgFnSc4d1	novodobá
historii	historie	k1gFnSc4	historie
související	související	k2eAgFnSc4d1	související
s	s	k7c7	s
těžbou	těžba	k1gFnSc7	těžba
hnědého	hnědý	k2eAgNnSc2d1	hnědé
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zastavila	zastavit	k5eAaPmAgFnS	zastavit
sotva	sotva	k6eAd1	sotva
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
lípy	lípa	k1gFnSc2	lípa
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
živoucím	živoucí	k2eAgInSc7d1	živoucí
symbolem	symbol	k1gInSc7	symbol
protestů	protest	k1gInPc2	protest
proti	proti	k7c3	proti
těžbě	těžba	k1gFnSc3	těžba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
příběh	příběh	k1gInSc1	příběh
stromu	strom	k1gInSc2	strom
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
nedalekým	daleký	k2eNgInSc7d1	nedaleký
zaniklým	zaniklý	k2eAgInSc7d1	zaniklý
hradem	hrad	k1gInSc7	hrad
Starý	starý	k2eAgInSc1d1	starý
Žeberk	Žeberk	k1gInSc1	Žeberk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vystavěli	vystavět	k5eAaPmAgMnP	vystavět
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
už	už	k6eAd1	už
koncem	koncem	k7c2	koncem
století	století	k1gNnSc2	století
čtrnáctého	čtrnáctý	k4xOgMnSc2	čtrnáctý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
za	za	k7c4	za
tak	tak	k6eAd1	tak
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
stihl	stihnout	k5eAaPmAgInS	stihnout
odehrát	odehrát	k5eAaPmF	odehrát
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
příběh	příběh	k1gInSc1	příběh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Hradní	hradní	k2eAgMnSc1d1	hradní
pán	pán	k1gMnSc1	pán
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
střežit	střežit	k5eAaImF	střežit
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
vyhlédl	vyhlédnout	k5eAaPmAgInS	vyhlédnout
děvče	děvče	k1gNnSc4	děvče
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
našel	najít	k5eAaPmAgMnS	najít
zalíbení	zalíbení	k1gNnSc4	zalíbení
<g/>
.	.	kIx.	.
</s>
<s>
Odvlekl	odvléct	k5eAaPmAgMnS	odvléct
dívku	dívka	k1gFnSc4	dívka
proti	proti	k7c3	proti
její	její	k3xOp3gFnSc3	její
vůli	vůle	k1gFnSc3	vůle
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
odchodem	odchod	k1gInSc7	odchod
z	z	k7c2	z
domova	domov	k1gInSc2	domov
nesmířila	smířit	k5eNaPmAgFnS	smířit
a	a	k8xC	a
ve	v	k7c6	v
svém	svůj	k3xOyFgMnSc6	svůj
partnerovi	partner	k1gMnSc6	partner
viděla	vidět	k5eAaImAgFnS	vidět
jen	jen	k9	jen
krutého	krutý	k2eAgMnSc4d1	krutý
pána	pán	k1gMnSc4	pán
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
lásku	láska	k1gFnSc4	láska
nevynutí	vynutit	k5eNaPmIp3nS	vynutit
<g/>
,	,	kIx,	,
svrhl	svrhnout	k5eAaPmAgMnS	svrhnout
svou	svůj	k3xOyFgFnSc4	svůj
partnerku	partnerka	k1gFnSc4	partnerka
z	z	k7c2	z
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Dívka	dívka	k1gFnSc1	dívka
naštěstí	naštěstí	k6eAd1	naštěstí
spadla	spadnout	k5eAaPmAgFnS	spadnout
do	do	k7c2	do
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
ošklivě	ošklivě	k6eAd1	ošklivě
zranila	zranit	k5eAaPmAgFnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Pochroumanou	pochroumaný	k2eAgFnSc4d1	pochroumaná
ji	on	k3xPp3gFnSc4	on
našel	najít	k5eAaPmAgMnS	najít
mladý	mladý	k2eAgMnSc1d1	mladý
sedlák	sedlák	k1gMnSc1	sedlák
z	z	k7c2	z
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
samoty	samota	k1gFnSc2	samota
<g/>
,	,	kIx,	,
odnesl	odnést	k5eAaPmAgMnS	odnést
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
ošetřil	ošetřit	k5eAaPmAgMnS	ošetřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
ji	on	k3xPp3gFnSc4	on
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
mohli	moct	k5eAaImAgMnP	moct
hledat	hledat	k5eAaImF	hledat
<g/>
,	,	kIx,	,
skryl	skrýt	k5eAaPmAgMnS	skrýt
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
staré	starý	k2eAgFnSc2d1	stará
lípy	lípa	k1gFnSc2	lípa
<g/>
.	.	kIx.	.
</s>
<s>
Skutečně	skutečně	k6eAd1	skutečně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Hradnímu	hradní	k2eAgMnSc3d1	hradní
pánovi	pán	k1gMnSc3	pán
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc4	jeho
čin	čin	k1gInSc4	čin
nakonec	nakonec	k6eAd1	nakonec
rozležel	rozležet	k5eAaPmAgMnS	rozležet
a	a	k8xC	a
zaúkoloval	zaúkolovat	k5eAaPmAgMnS	zaúkolovat
zbrojnoše	zbrojnoš	k1gMnSc4	zbrojnoš
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
hledali	hledat	k5eAaImAgMnP	hledat
marně	marně	k6eAd1	marně
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
mladá	mladý	k2eAgFnSc1d1	mladá
paní	paní	k1gFnSc1	paní
zcela	zcela	k6eAd1	zcela
zotavila	zotavit	k5eAaPmAgFnS	zotavit
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
zachráncem	zachránce	k1gMnSc7	zachránce
opustit	opustit	k5eAaPmF	opustit
kraj	kraj	k1gInSc4	kraj
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
z	z	k7c2	z
moci	moc	k1gFnSc2	moc
krutého	krutý	k2eAgMnSc2d1	krutý
pána	pán	k1gMnSc2	pán
a	a	k8xC	a
mohli	moct	k5eAaImAgMnP	moct
vést	vést	k5eAaImF	vést
šťastný	šťastný	k2eAgInSc4d1	šťastný
a	a	k8xC	a
spokojený	spokojený	k2eAgInSc4d1	spokojený
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc4d1	další
zajímavosti	zajímavost	k1gFnPc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2009	[number]	k4	2009
se	se	k3xPyFc4	se
lípa	lípa	k1gFnSc1	lípa
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
soutěže	soutěž	k1gFnSc2	soutěž
Strom	strom	k1gInSc1	strom
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
Rostislav	Rostislav	k1gMnSc1	Rostislav
Vošický	Vošický	k2eAgMnSc1d1	Vošický
a	a	k8xC	a
Vlaďka	Vlaďka	k1gFnSc1	Vlaďka
Kulhavá	kulhavý	k2eAgFnSc1d1	Kulhavá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
se	se	k3xPyFc4	se
objevit	objevit	k5eAaPmF	objevit
i	i	k9	i
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
pořadu	pořad	k1gInSc6	pořad
Paměť	paměť	k1gFnSc4	paměť
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
omezené	omezený	k2eAgFnSc2d1	omezená
stopáže	stopáž	k1gFnSc2	stopáž
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
vynechána	vynechat	k5eAaPmNgFnS	vynechat
a	a	k8xC	a
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
tak	tak	k9	tak
jen	jen	k9	jen
v	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
bonusovém	bonusový	k2eAgInSc6d1	bonusový
dílu	díl	k1gInSc6	díl
o	o	k7c6	o
natáčení	natáčení	k1gNnSc6	natáčení
a	a	k8xC	a
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
o	o	k7c6	o
pořadu	pořad	k1gInSc6	pořad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památné	památný	k2eAgInPc4d1	památný
a	a	k8xC	a
významné	významný	k2eAgInPc4d1	významný
stromy	strom	k1gInPc4	strom
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
==	==	k?	==
</s>
</p>
<p>
<s>
Albrechtický	albrechtický	k2eAgInSc1d1	albrechtický
dub	dub	k1gInSc1	dub
–	–	k?	–
zaniklý	zaniklý	k2eAgInSc1d1	zaniklý
chráněný	chráněný	k2eAgInSc1d1	chráněný
dub	dub	k1gInSc1	dub
u	u	k7c2	u
bývalé	bývalý	k2eAgFnSc2d1	bývalá
obce	obec	k1gFnSc2	obec
Albrechtice	Albrechtice	k1gFnPc1	Albrechtice
</s>
</p>
<p>
<s>
Borovice	borovice	k1gFnSc1	borovice
Schwerinova	Schwerinův	k2eAgFnSc1d1	Schwerinova
v	v	k7c6	v
Mostě	most	k1gInSc6	most
–	–	k?	–
chráněný	chráněný	k2eAgInSc1d1	chráněný
strom	strom	k1gInSc1	strom
</s>
</p>
<p>
<s>
Dub	dub	k1gInSc1	dub
pod	pod	k7c7	pod
Resslem	Ressl	k1gInSc7	Ressl
–	–	k?	–
chráněný	chráněný	k2eAgInSc1d1	chráněný
strom	strom	k1gInSc1	strom
</s>
</p>
<p>
<s>
Jírovec	jírovec	k1gInSc1	jírovec
v	v	k7c6	v
Šumné	šumný	k2eAgFnSc6d1	Šumná
u	u	k7c2	u
Litvínova	Litvínov	k1gInSc2	Litvínov
–	–	k?	–
chráněný	chráněný	k2eAgInSc1d1	chráněný
strom	strom	k1gInSc1	strom
</s>
</p>
<p>
<s>
Lipová	lipový	k2eAgFnSc1d1	Lipová
alej	alej	k1gFnSc1	alej
v	v	k7c6	v
Mostě	most	k1gInSc6	most
–	–	k?	–
chráněná	chráněný	k2eAgFnSc1d1	chráněná
alej	alej	k1gFnSc1	alej
u	u	k7c2	u
Oblastního	oblastní	k2eAgNnSc2d1	oblastní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Mostě	most	k1gInSc6	most
</s>
</p>
<p>
<s>
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Lužici	Lužice	k1gFnSc6	Lužice
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Most	most	k1gInSc1	most
<g/>
)	)	kIx)	)
–	–	k?	–
chráněný	chráněný	k2eAgInSc4d1	chráněný
strom	strom	k1gInSc4	strom
</s>
</p>
<p>
<s>
Lípy	lípa	k1gFnPc1	lípa
v	v	k7c6	v
Horní	horní	k2eAgFnSc6d1	horní
Vsi	ves	k1gFnSc6	ves
u	u	k7c2	u
Litvínova	Litvínov	k1gInSc2	Litvínov
–	–	k?	–
chráněné	chráněný	k2eAgInPc4d1	chráněný
stromy	strom	k1gInPc4	strom
</s>
</p>
<p>
<s>
Lípy	lípa	k1gFnPc1	lípa
v	v	k7c6	v
Janově	Janov	k1gInSc6	Janov
u	u	k7c2	u
Litvínova	Litvínov	k1gInSc2	Litvínov
–	–	k?	–
chráněné	chráněný	k2eAgInPc4d1	chráněný
stromy	strom	k1gInPc4	strom
</s>
</p>
<p>
<s>
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Šumné	šumný	k2eAgFnSc6d1	Šumná
u	u	k7c2	u
Litvínova	Litvínov	k1gInSc2	Litvínov
–	–	k?	–
chráněný	chráněný	k2eAgInSc1d1	chráněný
strom	strom	k1gInSc1	strom
</s>
</p>
<p>
<s>
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Meziboří	Meziboří	k1gNnSc6	Meziboří
(	(	kIx(	(
<g/>
Potoční	potoční	k2eAgFnSc2d1	potoční
ulice	ulice	k1gFnSc2	ulice
<g/>
)	)	kIx)	)
–	–	k?	–
chráněný	chráněný	k2eAgInSc4d1	chráněný
strom	strom	k1gInSc4	strom
</s>
</p>
<p>
<s>
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Meziboří	Meziboří	k1gNnSc6	Meziboří
(	(	kIx(	(
<g/>
Okružní	okružní	k2eAgFnSc2d1	okružní
ulice	ulice	k1gFnSc2	ulice
<g/>
)	)	kIx)	)
–	–	k?	–
chráněný	chráněný	k2eAgInSc4d1	chráněný
strom	strom	k1gInSc4	strom
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Hrušková	Hrušková	k1gFnSc1	Hrušková
<g/>
,	,	kIx,	,
Významné	významný	k2eAgInPc1d1	významný
stromy	strom	k1gInPc1	strom
severočeského	severočeský	k2eAgInSc2d1	severočeský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
Krajské	krajský	k2eAgNnSc1d1	krajské
středisko	středisko	k1gNnSc1	středisko
státní	státní	k2eAgFnSc2d1	státní
památkové	památkový	k2eAgFnSc2d1	památková
péče	péče	k1gFnSc2	péče
a	a	k8xC	a
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
1987	[number]	k4	1987
</s>
</p>
<p>
<s>
kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
Stromy	strom	k1gInPc4	strom
našich	náš	k3xOp1gNnPc2	náš
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
Magistrát	magistrát	k1gInSc1	magistrát
města	město	k1gNnSc2	město
Mostu	most	k1gInSc2	most
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
s.	s.	k?	s.
38	[number]	k4	38
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Jezeří	Jezeřit	k5eAaPmIp3nS	Jezeřit
</s>
</p>
<p>
<s>
Starý	starý	k2eAgInSc1d1	starý
Žeberk	Žeberk	k1gInSc1	Žeberk
</s>
</p>
<p>
<s>
Památný	památný	k2eAgInSc1d1	památný
strom	strom	k1gInSc1	strom
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
Žeberské	Žeberský	k2eAgFnSc6d1	Žeberská
lípě	lípa	k1gFnSc6	lípa
</s>
</p>
<p>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
a	a	k8xC	a
panorama	panorama	k1gNnSc1	panorama
</s>
</p>
