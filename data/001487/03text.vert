<s>
Marie	Marie	k1gFnSc1	Marie
Curie-Skłodowská	Curie-Skłodowská	k1gFnSc1	Curie-Skłodowská
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Maria	Maria	k1gFnSc1	Maria
Salomea	Salomea	k1gFnSc1	Salomea
Skłodowska	Skłodowska	k1gFnSc1	Skłodowska
<g/>
,	,	kIx,	,
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
Maria	Maria	k1gFnSc1	Maria
Skłodowska-Curie	Skłodowska-Curie	k1gFnSc1	Skłodowska-Curie
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1867	[number]	k4	1867
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1934	[number]	k4	1934
Passy	Passa	k1gFnSc2	Passa
<g/>
,	,	kIx,	,
Haute-Savoie	Haute-Savoie	k1gFnSc1	Haute-Savoie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
významná	významný	k2eAgFnSc1d1	významná
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vědkyně	vědkyně	k1gFnSc1	vědkyně
polského	polský	k2eAgInSc2d1	polský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
strávila	strávit	k5eAaPmAgFnS	strávit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
výzkumy	výzkum	k1gInPc4	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejím	její	k3xOp3gInPc3	její
největším	veliký	k2eAgInPc3d3	veliký
úspěchům	úspěch	k1gInPc3	úspěch
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
teorie	teorie	k1gFnSc1	teorie
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
technika	technik	k1gMnSc2	technik
dělení	dělení	k1gNnSc2	dělení
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
izotopů	izotop	k1gInPc2	izotop
objev	objev	k1gInSc4	objev
dvou	dva	k4xCgInPc2	dva
nových	nový	k2eAgInPc2d1	nový
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
<g/>
:	:	kIx,	:
radia	radio	k1gNnSc2	radio
a	a	k8xC	a
polonia	polonium	k1gNnSc2	polonium
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jejím	její	k3xOp3gNnSc7	její
osobním	osobní	k2eAgNnSc7d1	osobní
vedením	vedení	k1gNnSc7	vedení
byly	být	k5eAaImAgInP	být
též	též	k9	též
prováděny	provádět	k5eAaImNgInP	provádět
první	první	k4xOgInPc1	první
výzkumy	výzkum	k1gInPc1	výzkum
léčby	léčba	k1gFnSc2	léčba
rakoviny	rakovina	k1gFnSc2	rakovina
pomocí	pomocí	k7c2	pomocí
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Curie-Skłodowská	Curie-Skłodowská	k1gFnSc1	Curie-Skłodowská
byla	být	k5eAaImAgFnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
vyznamenána	vyznamenat	k5eAaPmNgFnS	vyznamenat
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
obdržela	obdržet	k5eAaPmAgFnS	obdržet
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c7	za
výzkumy	výzkum	k1gInPc7	výzkum
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
Pierrem	Pierr	k1gMnSc7	Pierr
Curie	Curie	k1gMnSc7	Curie
a	a	k8xC	a
objevitelem	objevitel	k1gMnSc7	objevitel
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
Henri	Henr	k1gFnSc2	Henr
Becquerelem	becquerel	k1gInSc7	becquerel
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
jí	on	k3xPp3gFnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
za	za	k7c4	za
izolaci	izolace	k1gFnSc4	izolace
čistého	čistý	k2eAgNnSc2d1	čisté
radia	radio	k1gNnSc2	radio
<g/>
.	.	kIx.	.
</s>
<s>
Maria	Maria	k1gFnSc1	Maria
Salomea	Salomea	k1gFnSc1	Salomea
Skłodowska	Skłodowska	k1gFnSc1	Skłodowska
byla	být	k5eAaImAgFnS	být
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
z	z	k7c2	z
pěti	pět	k4xCc2	pět
dětí	dítě	k1gFnPc2	dítě
učitelského	učitelský	k2eAgInSc2d1	učitelský
páru	pár	k1gInSc2	pár
Władysława	Władysławum	k1gNnSc2	Władysławum
a	a	k8xC	a
Bronisławy	Bronisława	k1gFnSc2	Bronisława
Skłodowských	Skłodowský	k1gMnPc2	Skłodowský
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
nižší	nízký	k2eAgFnSc2d2	nižší
polské	polský	k2eAgFnSc2d1	polská
šlechty	šlechta	k1gFnSc2	šlechta
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
szlachta	szlacht	k1gMnSc2	szlacht
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
a	a	k8xC	a
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgMnS	být
učitelem	učitel	k1gMnSc7	učitel
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
žákyní	žákyně	k1gFnSc7	žákyně
jediné	jediný	k2eAgFnSc2d1	jediná
soukromé	soukromý	k2eAgFnSc2d1	soukromá
dívčí	dívčí	k2eAgFnSc2d1	dívčí
školy	škola	k1gFnSc2	škola
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
učitelkou	učitelka	k1gFnSc7	učitelka
a	a	k8xC	a
ředitelkou	ředitelka	k1gFnSc7	ředitelka
té	ten	k3xDgFnSc2	ten
dívčí	dívčí	k2eAgFnSc2d1	dívčí
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
studovala	studovat	k5eAaImAgFnS	studovat
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Skłodowska	Skłodowska	k1gFnSc1	Skłodowska
složila	složit	k5eAaPmAgFnS	složit
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
tímto	tento	k3xDgNnSc7	tento
jménem	jméno	k1gNnSc7	jméno
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
přijímací	přijímací	k2eAgFnSc2d1	přijímací
zkoušky	zkouška	k1gFnSc2	zkouška
na	na	k7c4	na
fakultu	fakulta	k1gFnSc4	fakulta
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
chemie	chemie	k1gFnSc2	chemie
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
Sorbonny	Sorbonna	k1gFnSc2	Sorbonna
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
den	den	k1gInSc4	den
studovala	studovat	k5eAaImAgFnS	studovat
a	a	k8xC	a
po	po	k7c6	po
večerech	večer	k1gInPc6	večer
doučovala	doučovat	k5eAaImAgFnS	doučovat
a	a	k8xC	a
vydělávala	vydělávat	k5eAaImAgFnS	vydělávat
si	se	k3xPyFc3	se
tak	tak	k9	tak
na	na	k7c4	na
živobytí	živobytí	k1gNnSc4	živobytí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
získala	získat	k5eAaPmAgFnS	získat
licenciát	licenciát	k1gInSc4	licenciát
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
laborantka	laborantka	k1gFnSc1	laborantka
v	v	k7c6	v
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
laboratoři	laboratoř	k1gFnSc6	laboratoř
Lippmanových	Lippmanův	k2eAgInPc2d1	Lippmanův
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
dále	daleko	k6eAd2	daleko
studovala	studovat	k5eAaImAgFnS	studovat
na	na	k7c6	na
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
licenciát	licenciát	k1gMnSc1	licenciát
z	z	k7c2	z
matematiky	matematika	k1gFnSc2	matematika
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
poznala	poznat	k5eAaPmAgFnS	poznat
svého	svůj	k3xOyFgMnSc4	svůj
budoucího	budoucí	k2eAgMnSc4d1	budoucí
manžela	manžel	k1gMnSc4	manžel
Pierra	Pierr	k1gMnSc4	Pierr
Curie	Curie	k1gMnSc4	Curie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
doktorandem	doktorand	k1gMnSc7	doktorand
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
Henri	Henri	k1gNnPc2	Henri
Becquerela	Becquerel	k1gMnSc2	Becquerel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Pierrově	Pierrův	k2eAgInSc6d1	Pierrův
doktorátu	doktorát	k1gInSc6	doktorát
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
vzali	vzít	k5eAaPmAgMnP	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
byl	být	k5eAaImAgInS	být
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
Maria	Maria	k1gFnSc1	Maria
byla	být	k5eAaImAgFnS	být
nábožensky	nábožensky	k6eAd1	nábožensky
vlažná	vlažný	k2eAgFnSc1d1	vlažná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
měli	mít	k5eAaImAgMnP	mít
občanský	občanský	k2eAgInSc4d1	občanský
sňatek	sňatek	k1gInSc4	sňatek
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
roztržce	roztržka	k1gFnSc3	roztržka
Marie	Maria	k1gFnSc2	Maria
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
polskou	polský	k2eAgFnSc7d1	polská
katolickou	katolický	k2eAgFnSc7d1	katolická
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Irè	Irè	k1gFnSc1	Irè
(	(	kIx(	(
<g/>
taktéž	taktéž	k?	taktéž
pozdější	pozdní	k2eAgFnSc1d2	pozdější
nositelka	nositelka	k1gFnSc1	nositelka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Curie	Curie	k1gMnSc1	Curie
doporučil	doporučit	k5eAaPmAgMnS	doporučit
Marii	Maria	k1gFnSc4	Maria
Becquerelovi	Becquerel	k1gMnSc3	Becquerel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
doktorantské	doktorantský	k2eAgNnSc4d1	doktorantské
studium	studium	k1gNnSc4	studium
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
vedením	vedení	k1gNnSc7	vedení
na	na	k7c6	na
zdánlivě	zdánlivě	k6eAd1	zdánlivě
neatraktivní	atraktivní	k2eNgNnSc1d1	neatraktivní
a	a	k8xC	a
pracné	pracný	k2eAgNnSc1d1	pracné
téma	téma	k1gNnSc1	téma
-	-	kIx~	-
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
uranové	uranový	k2eAgFnPc1d1	uranová
rudy	ruda	k1gFnPc1	ruda
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnPc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
plynulo	plynout	k5eAaImAgNnS	plynout
z	z	k7c2	z
podílu	podíl	k1gInSc2	podíl
čistého	čistý	k2eAgInSc2d1	čistý
uranu	uran	k1gInSc2	uran
v	v	k7c6	v
rudě	ruda	k1gFnSc6	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
vědkyně	vědkyně	k1gFnSc1	vědkyně
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
provdaná	provdaný	k2eAgFnSc1d1	provdaná
a	a	k8xC	a
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Marie	Maria	k1gFnSc2	Maria
Curie	curie	k1gNnSc2	curie
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
obtížnou	obtížný	k2eAgFnSc4d1	obtížná
práci	práce	k1gFnSc4	práce
na	na	k7c4	na
dělení	dělení	k1gNnSc4	dělení
uranové	uranový	k2eAgFnSc2d1	uranová
rudy	ruda	k1gFnSc2	ruda
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
chemické	chemický	k2eAgFnPc4d1	chemická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
mladého	mladý	k2eAgMnSc2d1	mladý
chemika	chemik	k1gMnSc2	chemik
A.	A.	kA	A.
Debiernea	Debierneus	k1gMnSc2	Debierneus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dělal	dělat	k5eAaImAgMnS	dělat
licenciát	licenciát	k1gInSc4	licenciát
<g/>
.	.	kIx.	.
</s>
<s>
Hledala	hledat	k5eAaImAgFnS	hledat
sloučeninu	sloučenina	k1gFnSc4	sloučenina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
způsobovala	způsobovat	k5eAaImAgFnS	způsobovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
radioaktivitu	radioaktivita	k1gFnSc4	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Debiernea	Debiernea	k6eAd1	Debiernea
to	ten	k3xDgNnSc1	ten
brzy	brzy	k6eAd1	brzy
přestalo	přestat	k5eAaPmAgNnS	přestat
bavit	bavit	k5eAaImF	bavit
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gMnSc4	on
sám	sám	k3xTgMnSc1	sám
Pierre	Pierr	k1gMnSc5	Pierr
Curie	Curie	k1gMnSc5	Curie
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumy	výzkum	k1gInPc1	výzkum
po	po	k7c6	po
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
vedly	vést	k5eAaImAgFnP	vést
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
objevu	objev	k1gInSc3	objev
polonia	polonium	k1gNnSc2	polonium
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
Marie	Marie	k1gFnSc1	Marie
Curie	Curie	k1gMnPc2	Curie
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
polské	polský	k2eAgFnSc6d1	polská
vlasti	vlast	k1gFnSc6	vlast
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
mnohem	mnohem	k6eAd1	mnohem
radioaktivnějšího	radioaktivní	k2eAgNnSc2d2	radioaktivní
radia	radio	k1gNnSc2	radio
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
gram	gram	k1gInSc1	gram
radia	radio	k1gNnSc2	radio
izolovala	izolovat	k5eAaBmAgNnP	izolovat
ze	z	k7c2	z
smolince	smolinec	k1gInSc2	smolinec
pocházejícího	pocházející	k2eAgInSc2d1	pocházející
z	z	k7c2	z
Jáchymova	Jáchymov	k1gInSc2	Jáchymov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
vyjasnění	vyjasnění	k1gNnSc1	vyjasnění
pravděpodobných	pravděpodobný	k2eAgFnPc2d1	pravděpodobná
příčin	příčina	k1gFnPc2	příčina
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
-	-	kIx~	-
jako	jako	k8xC	jako
efektu	efekt	k1gInSc2	efekt
při	při	k7c6	při
rozpadu	rozpad	k1gInSc6	rozpad
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc4	titul
doktora	doktor	k1gMnSc4	doktor
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
jí	on	k3xPp3gFnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Získání	získání	k1gNnSc1	získání
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Curieovy	Curieův	k2eAgFnSc2d1	Curieova
náhle	náhle	k6eAd1	náhle
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
Sorbonna	Sorbonna	k1gFnSc1	Sorbonna
Pierrovi	Pierr	k1gMnSc3	Pierr
přiznala	přiznat	k5eAaPmAgFnS	přiznat
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
a	a	k8xC	a
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
se	se	k3xPyFc4	se
založením	založení	k1gNnSc7	založení
jeho	jeho	k3xOp3gFnSc2	jeho
vlastní	vlastní	k2eAgFnSc2d1	vlastní
laboratoře	laboratoř	k1gFnSc2	laboratoř
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
Marie	Marie	k1gFnSc1	Marie
stala	stát	k5eAaPmAgFnS	stát
vedoucí	vedoucí	k1gFnSc7	vedoucí
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
také	také	k6eAd1	také
porodila	porodit	k5eAaPmAgFnS	porodit
druhou	druhý	k4xOgFnSc4	druhý
dceru	dcera	k1gFnSc4	dcera
Evu	Eva	k1gFnSc4	Eva
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Curie	Curie	k1gMnSc5	Curie
však	však	k9	však
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1906	[number]	k4	1906
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gNnSc4	on
přejel	přejet	k5eAaPmAgInS	přejet
nákladní	nákladní	k2eAgInSc1d1	nákladní
koňský	koňský	k2eAgInSc1d1	koňský
povoz	povoz	k1gInSc1	povoz
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Curie-Skłodowská	Curie-Skłodowská	k1gFnSc1	Curie-Skłodowská
tak	tak	k6eAd1	tak
ztratila	ztratit	k5eAaPmAgFnS	ztratit
životního	životní	k2eAgMnSc4d1	životní
druha	druh	k1gMnSc4	druh
i	i	k8xC	i
spolupracovníka	spolupracovník	k1gMnSc4	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
fakultní	fakultní	k2eAgFnSc1d1	fakultní
rada	rada	k1gFnSc1	rada
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
zachovat	zachovat	k5eAaPmF	zachovat
katedru	katedra	k1gFnSc4	katedra
vytvořenou	vytvořený	k2eAgFnSc4d1	vytvořená
pro	pro	k7c4	pro
Pierra	Pierr	k1gMnSc4	Pierr
Curie	Curie	k1gMnSc4	Curie
a	a	k8xC	a
svěřila	svěřit	k5eAaPmAgFnS	svěřit
ji	on	k3xPp3gFnSc4	on
Curie-Skłodowské	Curie-Skłodowské	k2eAgFnSc4d1	Curie-Skłodowské
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
řízením	řízení	k1gNnSc7	řízení
laboratoře	laboratoř	k1gFnSc2	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
Curie-Skłodowská	Curie-Skłodowská	k1gFnSc1	Curie-Skłodowská
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ze	z	k7c2	z
stínu	stín	k1gInSc2	stín
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
první	první	k4xOgFnSc7	první
profesorkou	profesorka	k1gFnSc7	profesorka
Sorbonny	Sorbonna	k1gFnSc2	Sorbonna
<g/>
.	.	kIx.	.
</s>
<s>
Vinou	vinou	k7c2	vinou
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
manželé	manžel	k1gMnPc1	manžel
Curieovi	Curieův	k2eAgMnPc1d1	Curieův
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
Pierrovou	Pierrův	k2eAgFnSc7d1	Pierrova
smrtí	smrt	k1gFnSc7	smrt
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
dát	dát	k5eAaPmF	dát
si	se	k3xPyFc3	se
patentovat	patentovat	k5eAaBmF	patentovat
postup	postup	k1gInSc4	postup
přípravy	příprava	k1gFnSc2	příprava
radia	radio	k1gNnSc2	radio
z	z	k7c2	z
rud	ruda	k1gFnPc2	ruda
<g/>
,	,	kIx,	,
ochudili	ochudit	k5eAaPmAgMnP	ochudit
se	se	k3xPyFc4	se
o	o	k7c4	o
značné	značný	k2eAgInPc4d1	značný
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
použitelné	použitelný	k2eAgNnSc4d1	použitelné
např.	např.	kA	např.
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
laboratoře	laboratoř	k1gFnSc2	laboratoř
-	-	kIx~	-
obtížné	obtížný	k2eAgNnSc1d1	obtížné
získávání	získávání	k1gNnSc1	získávání
peněz	peníze	k1gInPc2	peníze
po	po	k7c4	po
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
jejich	jejich	k3xOp3gFnSc2	jejich
kariéry	kariéra	k1gFnSc2	kariéra
ztěžovalo	ztěžovat	k5eAaImAgNnS	ztěžovat
a	a	k8xC	a
značně	značně	k6eAd1	značně
zpomalovalo	zpomalovat	k5eAaImAgNnS	zpomalovat
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
nebyla	být	k5eNaImAgFnS	být
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
hlas	hlas	k1gInSc4	hlas
zvolena	zvolit	k5eAaPmNgNnP	zvolit
do	do	k7c2	do
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
Académie	Académie	k1gFnSc1	Académie
des	des	k1gNnSc2	des
sciences	sciencesa	k1gFnPc2	sciencesa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
však	však	k9	však
dostala	dostat	k5eAaPmAgFnS	dostat
svou	svůj	k3xOyFgFnSc4	svůj
druhou	druhý	k4xOgFnSc4	druhý
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
(	(	kIx(	(
<g/>
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yIgFnSc3	který
přesvědčila	přesvědčit	k5eAaPmAgFnS	přesvědčit
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyčlenila	vyčlenit	k5eAaPmAgFnS	vyčlenit
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
nezávislého	závislý	k2eNgInSc2d1	nezávislý
Ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
radium	radium	k1gNnSc4	radium
(	(	kIx(	(
<g/>
Institut	institut	k1gInSc1	institut
du	du	k?	du
radium	radium	k1gNnSc1	radium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
a	a	k8xC	a
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
prováděly	provádět	k5eAaImAgInP	provádět
výzkumy	výzkum	k1gInPc1	výzkum
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
ústav	ústav	k1gInSc1	ústav
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
líhní	líheň	k1gFnSc7	líheň
nositelů	nositel	k1gMnPc2	nositel
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
cen	cena	k1gFnPc2	cena
-	-	kIx~	-
vyšli	vyjít	k5eAaPmAgMnP	vyjít
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
ještě	ještě	k9	ještě
čtyři	čtyři	k4xCgMnPc1	čtyři
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
i	i	k9	i
Mariina	Mariin	k2eAgFnSc1d1	Mariina
dcera	dcera	k1gFnSc1	dcera
Irè	Irè	k1gFnSc1	Irè
Joliot-Curie	Joliot-Curie	k1gFnSc1	Joliot-Curie
a	a	k8xC	a
Mariin	Mariin	k2eAgMnSc1d1	Mariin
zeť	zeť	k1gMnSc1	zeť
Frédéric	Frédéric	k1gMnSc1	Frédéric
Joliot	Joliot	k1gMnSc1	Joliot
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
získali	získat	k5eAaPmAgMnP	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
objevení	objevení	k1gNnSc4	objevení
uměle	uměle	k6eAd1	uměle
vyvolané	vyvolaný	k2eAgFnSc2d1	vyvolaná
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Curie-Skłodowska	Curie-Skłodowska	k1gFnSc1	Curie-Skłodowska
stala	stát	k5eAaPmAgFnS	stát
šéfem	šéf	k1gMnSc7	šéf
vojenské	vojenský	k2eAgFnSc2d1	vojenská
lékařské	lékařský	k2eAgFnSc2d1	lékařská
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
organizací	organizace	k1gFnSc7	organizace
polních	polní	k2eAgFnPc2d1	polní
rentgenografických	rentgenografický	k2eAgFnPc2d1	rentgenografický
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyšetřily	vyšetřit	k5eAaPmAgFnP	vyšetřit
celkem	celkem	k6eAd1	celkem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
milióny	milión	k4xCgInPc1	milión
případů	případ	k1gInPc2	případ
zranění	zranění	k1gNnPc4	zranění
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
Curie-Skłodowska	Curie-Skłodowsk	k1gInSc2	Curie-Skłodowsk
nadále	nadále	k6eAd1	nadále
vedla	vést	k5eAaImAgFnS	vést
Ústav	ústav	k1gInSc4	ústav
pro	pro	k7c4	pro
rádium	rádium	k1gNnSc4	rádium
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
cestovala	cestovat	k5eAaImAgFnS	cestovat
po	po	k7c6	po
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
její	její	k3xOp3gFnSc1	její
nadace	nadace	k1gFnSc1	nadace
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
zakládat	zakládat	k5eAaImF	zakládat
lékařské	lékařský	k2eAgInPc4d1	lékařský
ústavy	ústav	k1gInPc4	ústav
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Marie	Maria	k1gFnSc2	Maria
Curie-Skłodowské	Curie-Skłodowská	k1gFnSc2	Curie-Skłodowská
na	na	k7c4	na
ústavu	ústava	k1gFnSc4	ústava
také	také	k9	také
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
studoval	studovat	k5eAaImAgMnS	studovat
František	František	k1gMnSc1	František
Běhounek	běhounek	k1gMnSc1	běhounek
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
radioaktivitu	radioaktivita	k1gFnSc4	radioaktivita
v	v	k7c6	v
jáchymovských	jáchymovský	k2eAgInPc6d1	jáchymovský
dolech	dol	k1gInPc6	dol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
Marie	Marie	k1gFnSc1	Marie
Curie-Skłodowska	Curie-Skłodowska	k1gFnSc1	Curie-Skłodowska
sfárala	sfárat	k5eAaPmAgFnS	sfárat
do	do	k7c2	do
dolu	dol	k1gInSc2	dol
Svornost	svornost	k1gFnSc1	svornost
v	v	k7c6	v
Jáchymově	Jáchymov	k1gInSc6	Jáchymov
<g/>
.	.	kIx.	.
</s>
<s>
Absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
také	také	k9	také
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převzala	převzít	k5eAaPmAgFnS	převzít
mnoho	mnoho	k4c4	mnoho
čestných	čestný	k2eAgInPc2d1	čestný
titulů	titul	k1gInPc2	titul
a	a	k8xC	a
také	také	k9	také
dostala	dostat	k5eAaPmAgFnS	dostat
od	od	k7c2	od
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
slavnostně	slavnostně	k6eAd1	slavnostně
gram	gram	k1gInSc4	gram
radia	radio	k1gNnSc2	radio
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
léčbu	léčba	k1gFnSc4	léčba
nemocných	nemocný	k1gMnPc2	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
polského	polský	k2eAgMnSc2d1	polský
prezidenta	prezident	k1gMnSc2	prezident
Moścického	Moścický	k2eAgMnSc2d1	Moścický
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
takovýchto	takovýto	k3xDgInPc2	takovýto
institutů	institut	k1gInPc2	institut
založen	založen	k2eAgInSc4d1	založen
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
vedoucím	vedoucí	k1gMnSc7	vedoucí
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Mariina	Mariin	k2eAgFnSc1d1	Mariina
sestra	sestra	k1gFnSc1	sestra
Bronisława	Bronisława	k1gFnSc1	Bronisława
<g/>
.	.	kIx.	.
</s>
<s>
Maria	Maria	k1gFnSc1	Maria
Curie-Skłodowska	Curie-Skłodowska	k1gFnSc1	Curie-Skłodowska
zemřela	zemřít	k5eAaPmAgFnS	zemřít
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1934	[number]	k4	1934
v	v	k7c6	v
sanatoriu	sanatorium	k1gNnSc6	sanatorium
Sancellemoz	Sancellemoza	k1gFnPc2	Sancellemoza
u	u	k7c2	u
Passy	Passa	k1gFnSc2	Passa
(	(	kIx(	(
<g/>
Haute-Savoie	Haute-Savoie	k1gFnSc1	Haute-Savoie
<g/>
)	)	kIx)	)
na	na	k7c4	na
leukemii	leukemie	k1gFnSc4	leukemie
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
zřejmě	zřejmě	k6eAd1	zřejmě
ionizujícím	ionizující	k2eAgNnSc7d1	ionizující
zářením	záření	k1gNnSc7	záření
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgMnPc7	který
pracovala	pracovat	k5eAaImAgFnS	pracovat
bez	bez	k7c2	bez
ochranných	ochranný	k2eAgInPc2d1	ochranný
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
vedle	vedle	k7c2	vedle
Pierra	Pierr	k1gInSc2	Pierr
v	v	k7c4	v
Sceaux	Sceaux	k1gInSc4	Sceaux
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
zásluhy	zásluha	k1gFnPc4	zásluha
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
pochována	pochován	k2eAgFnSc1d1	pochována
pod	pod	k7c7	pod
kopulí	kopule	k1gFnSc7	kopule
pařížského	pařížský	k2eAgInSc2d1	pařížský
Pantheonu	Pantheon	k1gInSc2	Pantheon
<g/>
.	.	kIx.	.
</s>
