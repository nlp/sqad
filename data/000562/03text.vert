<s>
Přestupný	přestupný	k2eAgInSc1d1	přestupný
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgInSc4d2	delší
než	než	k8xS	než
běžný	běžný	k2eAgInSc4d1	běžný
rok	rok	k1gInSc4	rok
a	a	k8xC	a
vkládá	vkládat	k5eAaImIp3nS	vkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
podle	podle	k7c2	podle
daného	daný	k2eAgInSc2d1	daný
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
solárních	solární	k2eAgFnPc6d1	solární
(	(	kIx(	(
<g/>
juliánský	juliánský	k2eAgInSc4d1	juliánský
<g/>
,	,	kIx,	,
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
<g/>
)	)	kIx)	)
i	i	k8xC	i
lunisolárních	lunisolární	k2eAgInPc6d1	lunisolární
kalendářích	kalendář	k1gInPc6	kalendář
k	k	k7c3	k
vyrovnávání	vyrovnávání	k1gNnSc3	vyrovnávání
rozdílů	rozdíl	k1gInPc2	rozdíl
mezi	mezi	k7c7	mezi
délkou	délka	k1gFnSc7	délka
kalendářního	kalendářní	k2eAgInSc2d1	kalendářní
a	a	k8xC	a
tropického	tropický	k2eAgInSc2d1	tropický
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
kalendářního	kalendářní	k2eAgInSc2d1	kalendářní
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
daná	daný	k2eAgFnSc1d1	daná
určitým	určitý	k2eAgInSc7d1	určitý
počtem	počet	k1gInSc7	počet
dní	den	k1gInPc2	den
nebo	nebo	k8xC	nebo
měsíců	měsíc	k1gInPc2	měsíc
odvozených	odvozený	k2eAgInPc2d1	odvozený
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
rotace	rotace	k1gFnSc2	rotace
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
doby	doba	k1gFnSc2	doba
rotace	rotace	k1gFnSc2	rotace
Měsíce	měsíc	k1gInSc2	měsíc
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tropický	tropický	k2eAgInSc1d1	tropický
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
dobou	doba	k1gFnSc7	doba
oběhu	oběh	k1gInSc2	oběh
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
střídání	střídání	k1gNnSc1	střídání
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
solárních	solární	k2eAgInPc6d1	solární
kalendářích	kalendář	k1gInPc6	kalendář
mívá	mívat	k5eAaImIp3nS	mívat
přestupný	přestupný	k2eAgInSc4d1	přestupný
rok	rok	k1gInSc4	rok
366	[number]	k4	366
dní	den	k1gInPc2	den
místo	místo	k7c2	místo
365	[number]	k4	365
<g/>
,	,	kIx,	,
v	v	k7c4	v
lunisolárních	lunisolární	k2eAgInPc2d1	lunisolární
13	[number]	k4	13
měsíců	měsíc	k1gInPc2	měsíc
místo	místo	k7c2	místo
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Kalendářní	kalendářní	k2eAgInSc1d1	kalendářní
rok	rok	k1gInSc1	rok
má	mít	k5eAaImIp3nS	mít
365	[number]	k4	365
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
protože	protože	k8xS	protože
jeden	jeden	k4xCgInSc1	jeden
tropický	tropický	k2eAgInSc1d1	tropický
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
365,242	[number]	k4	365,242
<g/>
19	[number]	k4	19
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
časem	časem	k6eAd1	časem
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
odchylce	odchylka	k1gFnSc3	odchylka
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nastane	nastat	k5eAaPmIp3nS	nastat
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
a	a	k8xC	a
právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
vkládá	vkládat	k5eAaImIp3nS	vkládat
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
říká	říkat	k5eAaImIp3nS	říkat
přestupný	přestupný	k2eAgInSc4d1	přestupný
den	den	k1gInSc4	den
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
docílí	docílit	k5eAaPmIp3nP	docílit
hodnoty	hodnota	k1gFnPc1	hodnota
365,25	[number]	k4	365,25
dne	den	k1gInSc2	den
na	na	k7c4	na
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
přesná	přesný	k2eAgFnSc1d1	přesná
délka	délka	k1gFnSc1	délka
tropického	tropický	k2eAgInSc2d1	tropický
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
posunu	posun	k1gInSc3	posun
o	o	k7c4	o
7,81	[number]	k4	7,81
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
budou	být	k5eAaImBp3nP	být
oproti	oproti	k7c3	oproti
skutečnosti	skutečnost	k1gFnSc3	skutečnost
přebývat	přebývat	k5eAaImF	přebývat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
upřesnil	upřesnit	k5eAaPmAgMnS	upřesnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
roky	rok	k1gInPc1	rok
dělitelné	dělitelný	k2eAgInPc1d1	dělitelný
100	[number]	k4	100
jsou	být	k5eAaImIp3nP	být
přestupné	přestupný	k2eAgInPc1d1	přestupný
jenom	jenom	k9	jenom
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
dělitelné	dělitelný	k2eAgInPc1d1	dělitelný
také	také	k9	také
400	[number]	k4	400
<g/>
.	.	kIx.	.
</s>
<s>
Přestupné	přestupný	k2eAgFnPc1d1	přestupná
byly	být	k5eAaImAgFnP	být
například	například	k6eAd1	například
roky	rok	k1gInPc1	rok
1600	[number]	k4	1600
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
apod.	apod.	kA	apod.
Přestupné	přestupný	k2eAgInPc1d1	přestupný
nejsou	být	k5eNaImIp3nP	být
roky	rok	k1gInPc1	rok
1700	[number]	k4	1700
<g/>
,	,	kIx,	,
1800	[number]	k4	1800
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
2100	[number]	k4	2100
atd.	atd.	kA	atd.
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
docílí	docílit	k5eAaPmIp3nP	docílit
průměrné	průměrný	k2eAgFnSc2d1	průměrná
hodnoty	hodnota	k1gFnSc2	hodnota
365,242	[number]	k4	365,242
<g/>
5	[number]	k4	5
dne	den	k1gInSc2	den
na	na	k7c4	na
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Odchylka	odchylka	k1gFnSc1	odchylka
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
tedy	tedy	k9	tedy
oproti	oproti	k7c3	oproti
skutečnosti	skutečnost	k1gFnSc3	skutečnost
činí	činit	k5eAaImIp3nS	činit
31	[number]	k4	31
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
budou	být	k5eAaImBp3nP	být
přebývat	přebývat	k5eAaImF	přebývat
za	za	k7c4	za
100	[number]	k4	100
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přestupný	přestupný	k2eAgInSc1d1	přestupný
den	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kalendářní	kalendářní	k2eAgInSc1d1	kalendářní
rok	rok	k1gInSc1	rok
svou	svůj	k3xOyFgFnSc7	svůj
délkou	délka	k1gFnSc7	délka
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
tropickému	tropický	k2eAgInSc3d1	tropický
roku	rok	k1gInSc3	rok
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
dnem	den	k1gInSc7	den
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
přestupný	přestupný	k2eAgInSc4d1	přestupný
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
přestupných	přestupný	k2eAgInPc2d1	přestupný
dnů	den	k1gInPc2	den
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
238	[number]	k4	238
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zaveden	zaveden	k2eAgInSc1d1	zaveden
dodatečný	dodatečný	k2eAgInSc1d1	dodatečný
den	den	k1gInSc1	den
každý	každý	k3xTgInSc4	každý
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
45	[number]	k4	45
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zavedl	zavést	k5eAaPmAgMnS	zavést
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
stejné	stejný	k2eAgNnSc4d1	stejné
počítání	počítání	k1gNnSc4	počítání
do	do	k7c2	do
kalendáře	kalendář	k1gInSc2	kalendář
používaného	používaný	k2eAgInSc2d1	používaný
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
označení	označení	k1gNnSc2	označení
juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chyba	chyba	k1gFnSc1	chyba
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
činí	činit	k5eAaImIp3nS	činit
1	[number]	k4	1
den	den	k1gInSc1	den
na	na	k7c4	na
128	[number]	k4	128
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nahromaděným	nahromaděný	k2eAgInPc3d1	nahromaděný
rozdílům	rozdíl	k1gInPc3	rozdíl
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1582	[number]	k4	1582
dekretem	dekret	k1gInSc7	dekret
papeže	papež	k1gMnSc2	papež
Řehoře	Řehoř	k1gMnSc2	Řehoř
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
kalendář	kalendář	k1gInSc4	kalendář
reformován	reformován	k2eAgInSc4d1	reformován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
gregoriánském	gregoriánský	k2eAgInSc6d1	gregoriánský
kalendáři	kalendář	k1gInSc6	kalendář
se	se	k3xPyFc4	se
přestupný	přestupný	k2eAgInSc4d1	přestupný
den	den	k1gInSc4	den
vkládá	vkládat	k5eAaImIp3nS	vkládat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
beze	beze	k7c2	beze
zbyku	zbyk	k1gInSc2	zbyk
dělitelný	dělitelný	k2eAgInSc4d1	dělitelný
čtyřmi	čtyři	k4xCgNnPc7	čtyři
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
celých	celý	k2eAgNnPc2d1	celé
století	století	k1gNnPc2	století
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
nejsou	být	k5eNaImIp3nP	být
beze	beze	k7c2	beze
zbytku	zbytek	k1gInSc2	zbytek
dělitelná	dělitelný	k2eAgFnSc1d1	dělitelná
400	[number]	k4	400
(	(	kIx(	(
<g/>
roky	rok	k1gInPc1	rok
1600	[number]	k4	1600
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2400	[number]	k4	2400
jsou	být	k5eAaImIp3nP	být
přestupné	přestupný	k2eAgInPc1d1	přestupný
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
1700	[number]	k4	1700
<g/>
,	,	kIx,	,
1800	[number]	k4	1800
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
2100	[number]	k4	2100
nejsou	být	k5eNaImIp3nP	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kalendáři	kalendář	k1gInSc6	kalendář
se	se	k3xPyFc4	se
přestupný	přestupný	k2eAgInSc4d1	přestupný
den	den	k1gInSc4	den
vkládá	vkládat	k5eAaImIp3nS	vkládat
jako	jako	k9	jako
29	[number]	k4	29
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
však	však	k9	však
přestupný	přestupný	k2eAgInSc1d1	přestupný
den	den	k1gInSc1	den
vkládal	vkládat	k5eAaImAgInS	vkládat
za	za	k7c4	za
23	[number]	k4	23
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Caesarovou	Caesarův	k2eAgFnSc7d1	Caesarova
reformou	reforma	k1gFnSc7	reforma
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
kalendář	kalendář	k1gInSc1	kalendář
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
zavedl	zavést	k5eAaPmAgMnS	zavést
král	král	k1gMnSc1	král
Numa	Numa	k1gMnSc1	Numa
Pompilius	Pompilius	k1gMnSc1	Pompilius
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
měl	mít	k5eAaImAgInS	mít
rok	rok	k1gInSc4	rok
355	[number]	k4	355
dnů	den	k1gInPc2	den
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
druhý	druhý	k4xOgInSc4	druhý
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
vkládal	vkládat	k5eAaImAgInS	vkládat
celý	celý	k2eAgInSc1d1	celý
přestupný	přestupný	k2eAgInSc1d1	přestupný
měsíc	měsíc	k1gInSc1	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
svátek	svátek	k1gInSc4	svátek
Terminálií	Terminálie	k1gFnPc2	Terminálie
(	(	kIx(	(
<g/>
svátek	svátek	k1gInSc1	svátek
ukončení	ukončení	k1gNnSc2	ukončení
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
připadal	připadat	k5eAaPmAgInS	připadat
na	na	k7c4	na
23	[number]	k4	23
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
tento	tento	k3xDgInSc4	tento
přestupný	přestupný	k2eAgInSc4d1	přestupný
měsíc	měsíc	k1gInSc4	měsíc
zrušil	zrušit	k5eAaPmAgInS	zrušit
a	a	k8xC	a
místo	místo	k7c2	místo
něj	on	k3xPp3gMnSc2	on
zavedl	zavést	k5eAaPmAgInS	zavést
přestupný	přestupný	k2eAgInSc1d1	přestupný
den	den	k1gInSc1	den
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lunisolárního	lunisolární	k2eAgInSc2d1	lunisolární
kalendáře	kalendář	k1gInSc2	kalendář
je	být	k5eAaImIp3nS	být
délka	délka	k1gFnSc1	délka
kalendářního	kalendářní	k2eAgInSc2d1	kalendářní
měsíce	měsíc	k1gInSc2	měsíc
daná	daný	k2eAgFnSc1d1	daná
oběhem	oběh	k1gInSc7	oběh
Měsíce	měsíc	k1gInSc2	měsíc
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
rok	rok	k1gInSc1	rok
má	mít	k5eAaImIp3nS	mít
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
přestupný	přestupný	k2eAgInSc1d1	přestupný
rok	rok	k1gInSc1	rok
přidává	přidávat	k5eAaImIp3nS	přidávat
přestupný	přestupný	k2eAgInSc4d1	přestupný
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
jich	on	k3xPp3gFnPc2	on
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
běžného	běžný	k2eAgInSc2d1	běžný
lunárního	lunární	k2eAgInSc2d1	lunární
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
354	[number]	k4	354
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
cca	cca	kA	cca
11	[number]	k4	11
dní	den	k1gInPc2	den
kratší	krátký	k2eAgFnSc1d2	kratší
než	než	k8xS	než
trvá	trvat	k5eAaImIp3nS	trvat
oběh	oběh	k1gInSc4	oběh
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hrubému	hrubý	k2eAgNnSc3d1	hrubé
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
posunu	posun	k1gInSc2	posun
je	být	k5eAaImIp3nS	být
přestupný	přestupný	k2eAgInSc4d1	přestupný
každý	každý	k3xTgInSc4	každý
třetí	třetí	k4xOgInSc4	třetí
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
11	[number]	k4	11
dní	den	k1gInPc2	den
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
třetina	třetina	k1gFnSc1	třetina
měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesnější	přesný	k2eAgInPc1d2	přesnější
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgInPc4	tři
přestupné	přestupný	k2eAgInPc4d1	přestupný
roky	rok	k1gInPc4	rok
během	během	k7c2	během
každých	každý	k3xTgNnPc2	každý
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
upřesnění	upřesnění	k1gNnSc3	upřesnění
dojde	dojít	k5eAaPmIp3nS	dojít
použitím	použití	k1gNnSc7	použití
sedmi	sedm	k4xCc2	sedm
přestupných	přestupný	k2eAgNnPc2d1	přestupné
let	léto	k1gNnPc2	léto
během	během	k7c2	během
devatenáctiletého	devatenáctiletý	k2eAgInSc2d1	devatenáctiletý
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
vývoji	vývoj	k1gInSc3	vývoj
dospěl	dochvít	k5eAaPmAgMnS	dochvít
attický	attický	k2eAgInSc4d1	attický
(	(	kIx(	(
<g/>
athénský	athénský	k2eAgInSc4d1	athénský
<g/>
)	)	kIx)	)
kalendář	kalendář	k1gInSc4	kalendář
již	již	k6eAd1	již
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Seznam	seznam	k1gInSc4	seznam
přestupných	přestupný	k2eAgInPc2d1	přestupný
roků	rok	k1gInPc2	rok
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2488	[number]	k4	2488
<g/>
.	.	kIx.	.
</s>
<s>
Gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
Juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
Přestupná	přestupný	k2eAgFnSc1d1	přestupná
sekunda	sekunda	k1gFnSc1	sekunda
Tropický	tropický	k2eAgInSc1d1	tropický
rok	rok	k1gInSc4	rok
Hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
rok	rok	k1gInSc4	rok
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
přestupný	přestupný	k2eAgInSc4d1	přestupný
rok	rok	k1gInSc4	rok
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
přestupný	přestupný	k2eAgInSc1d1	přestupný
rok	rok	k1gInSc1	rok
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
