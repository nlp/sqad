<s>
Let	let	k1gInSc1	let
Apollo	Apollo	k1gNnSc1	Apollo
13	[number]	k4	13
byl	být	k5eAaImAgMnS	být
sedmým	sedmý	k4xOgInSc7	sedmý
pilotovaným	pilotovaný	k2eAgInSc7d1	pilotovaný
letem	let	k1gInSc7	let
programu	program	k1gInSc2	program
Apollo	Apollo	k1gMnSc1	Apollo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
třetí	třetí	k4xOgNnPc1	třetí
přistání	přistání	k1gNnPc1	přistání
lidské	lidský	k2eAgFnSc2d1	lidská
posádky	posádka	k1gFnSc2	posádka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Fra	Fra	k1gFnSc2	Fra
Mauro	Mauro	k1gNnSc1	Mauro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
letu	let	k1gInSc2	let
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výbuchu	výbuch	k1gInSc3	výbuch
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
kyslíkových	kyslíkový	k2eAgFnPc2d1	kyslíková
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vážně	vážně	k6eAd1	vážně
poškodil	poškodit	k5eAaPmAgInS	poškodit
servisní	servisní	k2eAgInSc4d1	servisní
modul	modul	k1gInSc4	modul
<g/>
.	.	kIx.	.
</s>
<s>
Následky	následek	k1gInPc1	následek
výbuchu	výbuch	k1gInSc2	výbuch
nejen	nejen	k6eAd1	nejen
zabránily	zabránit	k5eAaPmAgInP	zabránit
posádce	posádka	k1gFnSc3	posádka
splnit	splnit	k5eAaPmF	splnit
úkol	úkol	k1gInSc4	úkol
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ohrozily	ohrozit	k5eAaPmAgInP	ohrozit
životy	život	k1gInPc1	život
jejích	její	k3xOp3gMnPc2	její
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Řídicí	řídicí	k2eAgNnSc1d1	řídicí
středisko	středisko	k1gNnSc1	středisko
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Manned	Manned	k1gInSc1	Manned
Spacecraft	Spacecraft	k1gInSc1	Spacecraft
Center	centrum	k1gNnPc2	centrum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
"	"	kIx"	"
<g/>
Lyndon	Lyndon	k1gInSc4	Lyndon
B.	B.	kA	B.
Johnson	Johnson	k1gNnSc1	Johnson
Center	centrum	k1gNnPc2	centrum
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
muselo	muset	k5eAaImAgNnS	muset
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
následujících	následující	k2eAgInPc2d1	následující
čtyř	čtyři	k4xCgInPc2	čtyři
dnů	den	k1gInPc2	den
s	s	k7c7	s
vynaložením	vynaložení	k1gNnSc7	vynaložení
nesmírného	smírný	k2eNgNnSc2d1	nesmírné
úsilí	úsilí	k1gNnSc2	úsilí
vyvinout	vyvinout	k5eAaPmF	vyvinout
nouzové	nouzový	k2eAgInPc4d1	nouzový
scénáře	scénář	k1gInPc4	scénář
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
posádku	posádka	k1gFnSc4	posádka
dopravit	dopravit	k5eAaPmF	dopravit
živou	živý	k2eAgFnSc7d1	živá
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Apollo	Apollo	k1gMnSc1	Apollo
13	[number]	k4	13
nesplnilo	splnit	k5eNaPmAgNnS	splnit
zadaný	zadaný	k2eAgInSc4d1	zadaný
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnPc2	jeho
let	léto	k1gNnPc2	léto
legendou	legenda	k1gFnSc7	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Jim	on	k3xPp3gMnPc3	on
Lovell	Lovell	k1gInSc1	Lovell
popsal	popsat	k5eAaPmAgInS	popsat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Lost	Lost	k1gMnSc1	Lost
Moon	Moon	k1gMnSc1	Moon
(	(	kIx(	(
<g/>
Ztracený	ztracený	k2eAgInSc1d1	ztracený
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Ron	Ron	k1gMnSc1	Ron
Howard	Howard	k1gMnSc1	Howard
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
film	film	k1gInSc1	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
Apollo	Apollo	k1gMnSc1	Apollo
13	[number]	k4	13
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
Jima	Jima	k1gMnSc1	Jima
Lovella	Lovella	k1gMnSc1	Lovella
herec	herec	k1gMnSc1	herec
Tom	Tom	k1gMnSc1	Tom
Hanks	Hanks	k1gInSc4	Hanks
<g/>
.	.	kIx.	.
</s>
<s>
Jim	on	k3xPp3gMnPc3	on
Lovell	Lovell	k1gMnSc1	Lovell
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
-	-	kIx~	-
velitel	velitel	k1gMnSc1	velitel
John	John	k1gMnSc1	John
Swigert	Swigert	k1gMnSc1	Swigert
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
Fred	Fred	k1gMnSc1	Fred
Haise	Hais	k1gMnSc2	Hais
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
V	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
je	být	k5eAaImIp3nS	být
uvedený	uvedený	k2eAgInSc1d1	uvedený
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
počet	počet	k1gInSc1	počet
letů	let	k1gInPc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
včetně	včetně	k7c2	včetně
této	tento	k3xDgFnSc2	tento
mise	mise	k1gFnSc2	mise
Podle	podle	k7c2	podle
zvyklostí	zvyklost	k1gFnPc2	zvyklost
v	v	k7c6	v
programu	program	k1gInSc6	program
Apollo	Apollo	k1gMnSc1	Apollo
se	se	k3xPyFc4	se
záložní	záložní	k2eAgFnSc1d1	záložní
posádka	posádka	k1gFnSc1	posádka
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
lety	let	k1gInPc4	let
později	pozdě	k6eAd2	pozdě
stávala	stávat	k5eAaImAgFnS	stávat
posádkou	posádka	k1gFnSc7	posádka
základní	základní	k2eAgInPc1d1	základní
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bychom	by	kYmCp1nP	by
došli	dojít	k5eAaPmAgMnP	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
základní	základní	k2eAgFnSc1d1	základní
posádka	posádka	k1gFnSc1	posádka
Apolla	Apollo	k1gNnSc2	Apollo
13	[number]	k4	13
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
záložní	záložní	k2eAgFnSc1d1	záložní
posádka	posádka	k1gFnSc1	posádka
Apolla	Apollo	k1gNnSc2	Apollo
10	[number]	k4	10
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
Gordon	Gordon	k1gMnSc1	Gordon
Cooper	Cooper	k1gMnSc1	Cooper
-	-	kIx~	-
velitel	velitel	k1gMnSc1	velitel
letu	let	k1gInSc3	let
Donn	donna	k1gFnPc2	donna
Eisele	Eisel	k1gInSc2	Eisel
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
Edgar	Edgar	k1gMnSc1	Edgar
Mittchell	Mittchell	k1gMnSc1	Mittchell
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
Přerozdělování	přerozdělování	k1gNnSc2	přerozdělování
posádek	posádka	k1gFnPc2	posádka
měl	mít	k5eAaImAgMnS	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
Deke	Dek	k1gFnSc2	Dek
Slayton	Slayton	k1gInSc1	Slayton
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	Tom	k1gMnSc3	Tom
se	se	k3xPyFc4	se
ale	ale	k9	ale
nelíbil	líbit	k5eNaImAgInS	líbit
let	let	k1gInSc1	let
Gordona	Gordon	k1gMnSc2	Gordon
Coopera	Cooper	k1gMnSc2	Cooper
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInSc3	jeho
laxnímu	laxní	k2eAgInSc3d1	laxní
přístupu	přístup	k1gInSc3	přístup
při	při	k7c6	při
tréninku	trénink	k1gInSc6	trénink
a	a	k8xC	a
Donna	donna	k1gFnSc1	donna
Eiselena	Eiselen	k2eAgFnSc1d1	Eiselen
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
roztržkám	roztržka	k1gFnPc3	roztržka
mezi	mezi	k7c7	mezi
posádkou	posádka	k1gFnSc7	posádka
při	při	k7c6	při
letu	let	k1gInSc6	let
Apollo	Apollo	k1gNnSc1	Apollo
7	[number]	k4	7
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInSc3	jeho
mimomanželskému	mimomanželský	k2eAgInSc3d1	mimomanželský
poměru	poměr	k1gInSc3	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
chtěl	chtít	k5eAaImAgMnS	chtít
zařadit	zařadit	k5eAaPmF	zařadit
až	až	k9	až
do	do	k7c2	do
letů	let	k1gInPc2	let
programu	program	k1gInSc2	program
Apollo	Apollo	k1gNnSc1	Apollo
applications	applications	k1gInSc1	applications
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zkrácen	zkrácen	k2eAgInSc1d1	zkrácen
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
program	program	k1gInSc4	program
Skylab	Skylaba	k1gFnPc2	Skylaba
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Slayton	Slayton	k1gInSc1	Slayton
složil	složit	k5eAaPmAgInS	složit
novou	nový	k2eAgFnSc4d1	nová
posádku	posádka	k1gFnSc4	posádka
Apolla	Apollo	k1gNnSc2	Apollo
13	[number]	k4	13
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
<g/>
:	:	kIx,	:
Alan	Alan	k1gMnSc1	Alan
Shepard	Shepard	k1gMnSc1	Shepard
-	-	kIx~	-
velitel	velitel	k1gMnSc1	velitel
letu	let	k1gInSc2	let
Stuard	Stuard	k1gMnSc1	Stuard
Roosa	Roos	k1gMnSc2	Roos
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
Edgar	Edgar	k1gMnSc1	Edgar
Mitchell	Mitchell	k1gMnSc1	Mitchell
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
Vedení	vedení	k1gNnSc2	vedení
NASA	NASA	kA	NASA
ovšem	ovšem	k9	ovšem
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shepard	Shepard	k1gInSc1	Shepard
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
na	na	k7c4	na
výcvik	výcvik	k1gInSc4	výcvik
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Shepardova	Shepardův	k2eAgFnSc1d1	Shepardova
posádka	posádka	k1gFnSc1	posádka
přesunula	přesunout	k5eAaPmAgFnS	přesunout
až	až	k9	až
na	na	k7c4	na
let	let	k1gInSc4	let
Apollo	Apollo	k1gNnSc1	Apollo
14	[number]	k4	14
<g/>
,	,	kIx,	,
a	a	k8xC	a
původní	původní	k2eAgFnSc1d1	původní
posádka	posádka	k1gFnSc1	posádka
letu	let	k1gInSc2	let
Apollo	Apollo	k1gNnSc1	Apollo
14	[number]	k4	14
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
záložní	záložní	k2eAgNnSc4d1	záložní
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
lety	let	k1gInPc4	let
níže	nízce	k6eAd2	nízce
-	-	kIx~	-
Apollo	Apollo	k1gNnSc4	Apollo
11	[number]	k4	11
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
přemístila	přemístit	k5eAaPmAgFnS	přemístit
na	na	k7c4	na
Apollo	Apollo	k1gNnSc4	Apollo
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Pilot	pilot	k1gMnSc1	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
záložní	záložní	k2eAgFnSc2d1	záložní
posádky	posádka	k1gFnSc2	posádka
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Anders	Andersa	k1gFnPc2	Andersa
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
nahrazen	nahradit	k5eAaPmNgInS	nahradit
za	za	k7c2	za
Thommase	Thommas	k1gMnSc5	Thommas
Mattinglyho	Mattinglyha	k1gMnSc5	Mattinglyha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
NASA	NASA	kA	NASA
již	již	k6eAd1	již
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
záložní	záložní	k2eAgFnSc4d1	záložní
posádku	posádka	k1gFnSc4	posádka
tvořili	tvořit	k5eAaImAgMnP	tvořit
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Young	Young	k1gMnSc1	Young
-	-	kIx~	-
velitel	velitel	k1gMnSc1	velitel
John	John	k1gMnSc1	John
Swigert	Swigert	k1gMnSc1	Swigert
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
Charles	Charles	k1gMnSc1	Charles
Duke	Duk	k1gMnSc2	Duk
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
Sedm	sedm	k4xCc4	sedm
dní	den	k1gInPc2	den
před	před	k7c7	před
startem	start	k1gInSc7	start
se	se	k3xPyFc4	se
pilot	pilot	k1gMnSc1	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
v	v	k7c6	v
záložní	záložní	k2eAgFnSc6d1	záložní
posádce	posádka	k1gFnSc6	posádka
-	-	kIx~	-
Charles	Charles	k1gMnSc1	Charles
Duke	Duke	k1gInSc4	Duke
nakazil	nakazit	k5eAaPmAgMnS	nakazit
zarděnkami	zarděnky	k1gFnPc7	zarděnky
od	od	k7c2	od
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Thomas	Thomas	k1gMnSc1	Thomas
Mattingly	Mattingla	k1gFnSc2	Mattingla
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
neměl	mít	k5eNaImAgMnS	mít
vytvořené	vytvořený	k2eAgFnPc4d1	vytvořená
protilátky	protilátka	k1gFnPc4	protilátka
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prohození	prohození	k1gNnSc3	prohození
pilotů	pilot	k1gMnPc2	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
a	a	k8xC	a
Mattingly	Mattingla	k1gFnSc2	Mattingla
se	se	k3xPyFc4	se
přemístil	přemístit	k5eAaPmAgInS	přemístit
do	do	k7c2	do
záložní	záložní	k2eAgFnSc2d1	záložní
posádky	posádka	k1gFnSc2	posádka
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
John	John	k1gMnSc1	John
Swigert	Swigert	k1gMnSc1	Swigert
<g/>
.	.	kIx.	.
</s>
<s>
Mattingly	Mattingl	k1gMnPc4	Mattingl
letěl	letět	k5eAaImAgMnS	letět
tedy	tedy	k8xC	tedy
až	až	k6eAd1	až
v	v	k7c6	v
Apollu	Apollo	k1gNnSc6	Apollo
16	[number]	k4	16
s	s	k7c7	s
Youngem	Young	k1gInSc7	Young
a	a	k8xC	a
Dukem	Duk	k1gInSc7	Duk
<g/>
.	.	kIx.	.
</s>
<s>
Záložní	záložní	k2eAgFnSc4d1	záložní
posádku	posádka	k1gFnSc4	posádka
v	v	k7c6	v
době	doba	k1gFnSc6	doba
odletu	odlet	k1gInSc2	odlet
tedy	tedy	k9	tedy
tvořili	tvořit	k5eAaImAgMnP	tvořit
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Young	Young	k1gMnSc1	Young
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
-	-	kIx~	-
velitel	velitel	k1gMnSc1	velitel
letu	let	k1gInSc2	let
Thomas	Thomas	k1gMnSc1	Thomas
Mattingly	Mattingla	k1gMnSc2	Mattingla
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
Charles	Charles	k1gMnSc1	Charles
Duke	Duk	k1gMnSc2	Duk
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
V	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
je	být	k5eAaImIp3nS	být
uvedený	uvedený	k2eAgInSc1d1	uvedený
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
počet	počet	k1gInSc1	počet
letů	let	k1gInPc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
Vance	Vance	k1gMnSc1	Vance
Brand	Brand	k1gMnSc1	Brand
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
Lousma	Lousmum	k1gNnSc2	Lousmum
<g/>
,	,	kIx,	,
Joseph	Josepha	k1gFnPc2	Josepha
Kerwin	Kerwina	k1gFnPc2	Kerwina
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
posádka	posádka	k1gFnSc1	posádka
připravovaná	připravovaný	k2eAgFnSc1d1	připravovaná
pro	pro	k7c4	pro
let	let	k1gInSc4	let
Apolla	Apollo	k1gNnSc2	Apollo
13	[number]	k4	13
měla	mít	k5eAaImAgFnS	mít
původně	původně	k6eAd1	původně
jiné	jiný	k2eAgNnSc4d1	jiné
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
<s>
Pilotem	pilot	k1gInSc7	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
měl	mít	k5eAaImAgInS	mít
totiž	totiž	k9	totiž
původně	původně	k6eAd1	původně
být	být	k5eAaImF	být
Thomas	Thomas	k1gMnSc1	Thomas
Mattingly	Mattingla	k1gFnSc2	Mattingla
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
před	před	k7c7	před
startem	start	k1gInSc7	start
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
posádka	posádka	k1gFnSc1	posádka
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
styku	styk	k1gInSc2	styk
se	s	k7c7	s
zarděnkami	zarděnky	k1gFnPc7	zarděnky
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
Mattingly	Mattingla	k1gFnSc2	Mattingla
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
touto	tento	k3xDgFnSc7	tento
nemocí	nemoc	k1gFnSc7	nemoc
neprošel	projít	k5eNaPmAgInS	projít
a	a	k8xC	a
neměl	mít	k5eNaImAgInS	mít
tak	tak	k9	tak
dostatek	dostatek	k1gInSc1	dostatek
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
Johnem	John	k1gMnSc7	John
Swigertem	Swigert	k1gMnSc7	Swigert
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
stejný	stejný	k2eAgInSc4d1	stejný
post	post	k1gInSc4	post
připravoval	připravovat	k5eAaImAgMnS	připravovat
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
záložní	záložní	k2eAgFnSc2d1	záložní
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Mattingly	Mattingla	k1gFnSc2	Mattingla
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
pilota	pilot	k1gMnSc2	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
letu	let	k1gInSc2	let
Apolla	Apollo	k1gNnSc2	Apollo
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Apollo	Apollo	k1gNnSc1	Apollo
8	[number]	k4	8
<g/>
,	,	kIx,	,
Apollo	Apollo	k1gMnSc1	Apollo
10	[number]	k4	10
a	a	k8xC	a
Apollo	Apollo	k1gMnSc1	Apollo
11	[number]	k4	11
letěly	letět	k5eAaImAgInP	letět
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
bezpečné	bezpečný	k2eAgFnSc6d1	bezpečná
translunární	translunární	k2eAgFnSc6d1	translunární
dráze	dráha	k1gFnSc6	dráha
(	(	kIx(	(
<g/>
free-return	freeeturn	k1gInSc1	free-return
trajectory	trajector	k1gInPc1	trajector
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přivede	přivést	k5eAaPmIp3nS	přivést
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
že	že	k8xS	že
při	při	k7c6	při
průletu	průlet	k1gInSc6	průlet
kolem	kolem	k7c2	kolem
Měsíce	měsíc	k1gInSc2	měsíc
nesníží	snížit	k5eNaPmIp3nP	snížit
svou	svůj	k3xOyFgFnSc4	svůj
rychlost	rychlost	k1gFnSc4	rychlost
zážehem	zážeh	k1gInSc7	zážeh
motorů	motor	k1gInPc2	motor
<g/>
,	,	kIx,	,
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Mimochodem	mimochodem	k6eAd1	mimochodem
-	-	kIx~	-
tuto	tento	k3xDgFnSc4	tento
dráhu	dráha	k1gFnSc4	dráha
poprvé	poprvé	k6eAd1	poprvé
zmínil	zmínit	k5eAaPmAgMnS	zmínit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
románu	román	k1gInSc6	román
Do	do	k7c2	do
Měsíce	měsíc	k1gInSc2	měsíc
Jules	Julesa	k1gFnPc2	Julesa
Verne	Vern	k1gInSc5	Vern
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tu	ten	k3xDgFnSc4	ten
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poruše	porucha	k1gFnSc3	porucha
motorů	motor	k1gInPc2	motor
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
bezpečné	bezpečný	k2eAgFnSc6d1	bezpečná
translunární	translunární	k2eAgFnSc6d1	translunární
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
loď	loď	k1gFnSc1	loď
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
potřeby	potřeba	k1gFnSc2	potřeba
dalšího	další	k2eAgNnSc2d1	další
paliva	palivo	k1gNnSc2	palivo
vrátí	vrátit	k5eAaPmIp3nS	vrátit
bezpečně	bezpečně	k6eAd1	bezpečně
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
translunární	translunární	k2eAgFnSc1d1	translunární
dráha	dráha	k1gFnSc1	dráha
má	mít	k5eAaImIp3nS	mít
ovšem	ovšem	k9	ovšem
rovněž	rovněž	k9	rovněž
své	svůj	k3xOyFgFnPc4	svůj
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
dostat	dostat	k5eAaPmF	dostat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
rovníkovou	rovníkový	k2eAgFnSc4d1	Rovníková
(	(	kIx(	(
<g/>
ekvitoriální	ekvitoriální	k2eAgFnSc4d1	ekvitoriální
<g/>
)	)	kIx)	)
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
kolem	kolem	k7c2	kolem
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přistání	přistání	k1gNnSc4	přistání
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
ležících	ležící	k2eAgFnPc2d1	ležící
kolem	kolem	k7c2	kolem
jeho	jeho	k3xOp3gInSc2	jeho
rovníku	rovník	k1gInSc2	rovník
-	-	kIx~	-
tedy	tedy	k9	tedy
nikoliv	nikoliv	k9	nikoliv
například	například	k6eAd1	například
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Fra	Fra	k1gFnSc2	Fra
Mauro	Mauro	k1gNnSc4	Mauro
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
směřovalo	směřovat	k5eAaImAgNnS	směřovat
Apollo	Apollo	k1gNnSc1	Apollo
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
argumentem	argument	k1gInSc7	argument
proti	proti	k7c3	proti
používání	používání	k1gNnSc3	používání
bezpečné	bezpečný	k2eAgFnSc2d1	bezpečná
translunární	translunární	k2eAgFnSc2d1	translunární
dráhy	dráha	k1gFnSc2	dráha
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
vhodné	vhodný	k2eAgNnSc1d1	vhodné
startovací	startovací	k2eAgNnSc1d1	startovací
okno	okno	k1gNnSc1	okno
<g/>
,	,	kIx,	,
zohledňující	zohledňující	k2eAgNnSc1d1	zohledňující
zejména	zejména	k9	zejména
světelné	světelný	k2eAgFnPc4d1	světelná
podmínky	podmínka	k1gFnPc4	podmínka
v	v	k7c6	v
době	doba	k1gFnSc6	doba
startu	start	k1gInSc2	start
<g/>
,	,	kIx,	,
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
nenastává	nastávat	k5eNaImIp3nS	nastávat
příliš	příliš	k6eAd1	příliš
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
omezuje	omezovat	k5eAaImIp3nS	omezovat
frekvenci	frekvence	k1gFnSc4	frekvence
startů	start	k1gInPc2	start
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Lety	let	k1gInPc1	let
Apollo	Apollo	k1gNnSc1	Apollo
12	[number]	k4	12
<g/>
,	,	kIx,	,
Apollo	Apollo	k1gMnSc1	Apollo
13	[number]	k4	13
a	a	k8xC	a
Apollo	Apollo	k1gMnSc1	Apollo
14	[number]	k4	14
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
H	H	kA	H
mise	mise	k1gFnSc1	mise
(	(	kIx(	(
<g/>
H	H	kA	H
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
hybrid	hybrida	k1gFnPc2	hybrida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
sice	sice	k8xC	sice
zahájily	zahájit	k5eAaPmAgInP	zahájit
let	let	k1gInSc4	let
po	po	k7c6	po
bezpečné	bezpečný	k2eAgFnSc6d1	bezpečná
translunární	translunární	k2eAgFnSc6d1	translunární
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
ji	on	k3xPp3gFnSc4	on
opustily	opustit	k5eAaPmAgFnP	opustit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
přistání	přistání	k1gNnSc4	přistání
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
cílové	cílový	k2eAgFnSc6d1	cílová
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Lety	let	k1gInPc1	let
Apollo	Apollo	k1gNnSc1	Apollo
15	[number]	k4	15
<g/>
,	,	kIx,	,
Apollo	Apollo	k1gMnSc1	Apollo
16	[number]	k4	16
a	a	k8xC	a
Apollo	Apollo	k1gMnSc1	Apollo
17	[number]	k4	17
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
J	J	kA	J
mise	mise	k1gFnSc1	mise
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
lety	let	k1gInPc1	let
byly	být	k5eAaImAgInP	být
rovnou	rovnou	k6eAd1	rovnou
navedeny	navést	k5eAaPmNgInP	navést
na	na	k7c4	na
translunární	translunární	k2eAgFnSc4d1	translunární
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
bezpečnou	bezpečný	k2eAgFnSc7d1	bezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Apollo	Apollo	k1gMnSc1	Apollo
15	[number]	k4	15
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
translunární	translunární	k2eAgFnSc4d1	translunární
dráhu	dráha	k1gFnSc4	dráha
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
řídicích	řídicí	k2eAgInPc2d1	řídicí
motorů	motor	k1gInPc2	motor
RCS	RCS	kA	RCS
(	(	kIx(	(
<g/>
Reactive	Reactiv	k1gInSc5	Reactiv
Control	Control	k1gInSc1	Control
System	Syst	k1gMnSc7	Syst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Apollo	Apollo	k1gMnSc1	Apollo
17	[number]	k4	17
již	již	k6eAd1	již
letělo	letět	k5eAaImAgNnS	letět
po	po	k7c6	po
dráze	dráha	k1gFnSc6	dráha
natolik	natolik	k6eAd1	natolik
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
bezpečné	bezpečný	k2eAgFnSc2d1	bezpečná
translunární	translunární	k2eAgFnSc2d1	translunární
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
mělo	mít	k5eAaImAgNnS	mít
problémy	problém	k1gInPc4	problém
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potíží	potíž	k1gFnPc2	potíž
dostat	dostat	k5eAaPmF	dostat
i	i	k9	i
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
motorů	motor	k1gInPc2	motor
přistávacího	přistávací	k2eAgInSc2d1	přistávací
stupně	stupeň	k1gInSc2	stupeň
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Apollo	Apollo	k1gNnSc1	Apollo
13	[number]	k4	13
bylo	být	k5eAaImAgNnS	být
vyneseno	vynést	k5eAaPmNgNnS	vynést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1970	[number]	k4	1970
v	v	k7c4	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
13	[number]	k4	13
UT	UT	kA	UT
z	z	k7c2	z
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
na	na	k7c6	na
Mysu	mys	k1gInSc6	mys
Canaveral	Canaveral	k1gFnSc2	Canaveral
<g/>
.	.	kIx.	.
</s>
<s>
30	[number]	k4	30
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
40	[number]	k4	40
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
startu	start	k1gInSc6	start
posádka	posádka	k1gFnSc1	posádka
zažehla	zažehnout	k5eAaPmAgFnS	zažehnout
hlavní	hlavní	k2eAgInSc4d1	hlavní
motor	motor	k1gInSc4	motor
servisního	servisní	k2eAgInSc2d1	servisní
modulu	modul	k1gInSc2	modul
a	a	k8xC	a
přešla	přejít	k5eAaPmAgFnS	přejít
tak	tak	k6eAd1	tak
z	z	k7c2	z
bezpečné	bezpečný	k2eAgFnSc2d1	bezpečná
translunární	translunární	k2eAgFnSc2d1	translunární
dráhy	dráha	k1gFnSc2	dráha
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
hybridní	hybridní	k2eAgFnSc1d1	hybridní
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
přistání	přistání	k1gNnSc4	přistání
v	v	k7c6	v
cílové	cílový	k2eAgFnSc6d1	cílová
oblasti	oblast	k1gFnSc6	oblast
Fra	Fra	k1gFnSc2	Fra
Mauro	Mauro	k1gNnSc1	Mauro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čase	čas	k1gInSc6	čas
55	[number]	k4	55
<g/>
:	:	kIx,	:
<g/>
52	[number]	k4	52
po	po	k7c6	po
startu	start	k1gInSc6	start
signalizovaly	signalizovat	k5eAaImAgFnP	signalizovat
přístroje	přístroj	k1gInPc4	přístroj
(	(	kIx(	(
<g/>
telemetrickým	telemetrický	k2eAgInSc7d1	telemetrický
přenosem	přenos	k1gInSc7	přenos
dat	datum	k1gNnPc2	datum
<g/>
)	)	kIx)	)
řídicímu	řídicí	k2eAgNnSc3d1	řídicí
středisku	středisko	k1gNnSc3	středisko
nízký	nízký	k2eAgInSc4d1	nízký
tlak	tlak	k1gInSc4	tlak
ve	v	k7c6	v
vodíkové	vodíkový	k2eAgFnSc6d1	vodíková
nádrži	nádrž	k1gFnSc6	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
poruchu	porucha	k1gFnSc4	porucha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
přirozený	přirozený	k2eAgInSc4d1	přirozený
fyzikální	fyzikální	k2eAgInSc4d1	fyzikální
jev	jev	k1gInSc4	jev
stratifikace	stratifikace	k1gFnSc2	stratifikace
obsahu	obsah	k1gInSc2	obsah
nádrže	nádrž	k1gFnSc2	nádrž
s	s	k7c7	s
kapalným	kapalný	k2eAgInSc7d1	kapalný
vodíkem	vodík	k1gInSc7	vodík
nebo	nebo	k8xC	nebo
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
nádržích	nádrž	k1gFnPc6	nádrž
zařízení	zařízení	k1gNnPc2	zařízení
na	na	k7c4	na
ohřívání	ohřívání	k1gNnSc4	ohřívání
a	a	k8xC	a
promíchávání	promíchávání	k1gNnSc4	promíchávání
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Středisko	středisko	k1gNnSc1	středisko
dalo	dát	k5eAaPmAgNnS	dát
pokyn	pokyn	k1gInSc4	pokyn
zapnout	zapnout	k5eAaPmF	zapnout
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
nádržích	nádrž	k1gFnPc6	nádrž
ohřívače	ohřívač	k1gInSc2	ohřívač
a	a	k8xC	a
promíchávací	promíchávací	k2eAgFnSc2d1	promíchávací
vrtule	vrtule	k1gFnSc2	vrtule
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jack	Jack	k1gMnSc1	Jack
<g/>
"	"	kIx"	"
Swigert	Swigert	k1gMnSc1	Swigert
zapnul	zapnout	k5eAaPmAgMnS	zapnout
promíchávání	promíchávání	k1gNnSc4	promíchávání
(	(	kIx(	(
<g/>
55	[number]	k4	55
<g/>
:	:	kIx,	:
<g/>
53	[number]	k4	53
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
pro	pro	k7c4	pro
kyslíkovou	kyslíkový	k2eAgFnSc4d1	kyslíková
nádrž	nádrž	k1gFnSc4	nádrž
č.	č.	k?	č.
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
byly	být	k5eAaImAgFnP	být
naměřeny	naměřen	k2eAgFnPc1d1	naměřena
anomálie	anomálie	k1gFnPc1	anomálie
elektrických	elektrický	k2eAgFnPc2d1	elektrická
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zpětně	zpětně	k6eAd1	zpětně
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
příznak	příznak	k1gInSc4	příznak
zkratů	zkrat	k1gInPc2	zkrat
v	v	k7c6	v
nádrži	nádrž	k1gFnSc6	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
zpětného	zpětný	k2eAgNnSc2d1	zpětné
vyhodnocení	vyhodnocení	k1gNnSc2	vyhodnocení
telemetrie	telemetrie	k1gFnSc2	telemetrie
byly	být	k5eAaImAgInP	být
senzory	senzor	k1gInPc1	senzor
kyslíkové	kyslíkový	k2eAgFnSc2d1	kyslíková
nádrže	nádrž	k1gFnSc2	nádrž
2	[number]	k4	2
ztraceny	ztraceno	k1gNnPc7	ztraceno
po	po	k7c6	po
nárůstu	nárůst	k1gInSc6	nárůst
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
čase	čas	k1gInSc6	čas
55	[number]	k4	55
<g/>
:	:	kIx,	:
<g/>
54	[number]	k4	54
<g/>
:	:	kIx,	:
<g/>
51	[number]	k4	51
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
minutu	minuta	k1gFnSc4	minuta
a	a	k8xC	a
půl	půl	k1xP	půl
po	po	k7c6	po
zapnutí	zapnutí	k1gNnSc6	zapnutí
promíchávání	promíchávání	k1gNnSc2	promíchávání
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
uslyšela	uslyšet	k5eAaPmAgFnS	uslyšet
výbuch	výbuch	k1gInSc4	výbuch
a	a	k8xC	a
pocítila	pocítit	k5eAaPmAgFnS	pocítit
nezvyklé	zvyklý	k2eNgFnPc4d1	nezvyklá
vibrace	vibrace	k1gFnPc4	vibrace
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řídicím	řídicí	k2eAgNnSc6d1	řídicí
středisku	středisko	k1gNnSc6	středisko
na	na	k7c4	na
1,8	[number]	k4	1,8
sekundy	sekund	k1gInPc7	sekund
vypadly	vypadnout	k5eAaPmAgInP	vypadnout
všechny	všechen	k3xTgInPc1	všechen
telemetrické	telemetrický	k2eAgInPc1d1	telemetrický
údaje	údaj	k1gInPc1	údaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kabině	kabina	k1gFnSc6	kabina
se	se	k3xPyFc4	se
rozsvítily	rozsvítit	k5eAaPmAgFnP	rozsvítit
kontrolky	kontrolka	k1gFnPc1	kontrolka
indikující	indikující	k2eAgFnSc4d1	indikující
nízkou	nízký	k2eAgFnSc4d1	nízká
úroveň	úroveň	k1gFnSc4	úroveň
stejnosměrného	stejnosměrný	k2eAgNnSc2d1	stejnosměrné
napětí	napětí	k1gNnSc2	napětí
na	na	k7c6	na
sběrnici	sběrnice	k1gFnSc6	sběrnice
B	B	kA	B
<g/>
,	,	kIx,	,
jednom	jeden	k4xCgMnSc6	jeden
ze	z	k7c2	z
systémů	systém	k1gInPc2	systém
rozvodu	rozvod	k1gInSc2	rozvod
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
uvnitř	uvnitř	k7c2	uvnitř
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
poškození	poškození	k1gNnPc2	poškození
elektrických	elektrický	k2eAgInPc2d1	elektrický
rozvodů	rozvod	k1gInPc2	rozvod
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
výbuchu	výbuch	k1gInSc2	výbuch
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
postupnou	postupný	k2eAgFnSc7d1	postupná
ztrátou	ztráta	k1gFnSc7	ztráta
funkce	funkce	k1gFnSc2	funkce
palivových	palivový	k2eAgInPc2d1	palivový
článků	článek	k1gInPc2	článek
kvůli	kvůli	k7c3	kvůli
ztrátě	ztráta	k1gFnSc3	ztráta
dodávky	dodávka	k1gFnSc2	dodávka
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
dvou	dva	k4xCgFnPc2	dva
vteřin	vteřina	k1gFnPc2	vteřina
od	od	k7c2	od
ztráty	ztráta	k1gFnSc2	ztráta
senzorů	senzor	k1gInPc2	senzor
v	v	k7c6	v
kyslíkové	kyslíkový	k2eAgFnSc6d1	kyslíková
nádrži	nádrž	k1gFnSc6	nádrž
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
John	John	k1gMnSc1	John
Swigert	Swigert	k1gMnSc1	Swigert
nebo	nebo	k8xC	nebo
Jim	on	k3xPp3gMnPc3	on
Lovell	Lovell	k1gInSc1	Lovell
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc1	údaj
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
i	i	k9	i
v	v	k7c6	v
oficiálních	oficiální	k2eAgInPc6d1	oficiální
dokumentech	dokument	k1gInPc6	dokument
NASA	NASA	kA	NASA
<g/>
)	)	kIx)	)
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
řídicímu	řídicí	k2eAgNnSc3d1	řídicí
středisku	středisko	k1gNnSc3	středisko
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
I	i	k9	i
believe	believat	k5eAaPmIp3nS	believat
we	we	k?	we
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c6	v
had	had	k1gMnSc1	had
a	a	k8xC	a
problem	problo	k1gNnSc7	problo
here	here	k1gNnSc2	here
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Houston	Houston	k1gInSc1	Houston
<g/>
,	,	kIx,	,
we	we	k?	we
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c6	v
had	had	k1gMnSc1	had
a	a	k8xC	a
problem	problo	k1gNnSc7	problo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ne	ne	k9	ne
"	"	kIx"	"
<g/>
Houston	Houston	k1gInSc1	Houston
<g/>
,	,	kIx,	,
we	we	k?	we
have	have	k1gFnSc7	have
a	a	k8xC	a
problem	problo	k1gNnSc7	problo
<g/>
"	"	kIx"	"
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
hláška	hláška	k1gFnSc1	hláška
populární	populární	k2eAgFnSc1d1	populární
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nehodě	nehoda	k1gFnSc3	nehoda
došlo	dojít	k5eAaPmAgNnS	dojít
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1970	[number]	k4	1970
v	v	k7c4	v
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
53	[number]	k4	53
UT	UT	kA	UT
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
již	již	k6eAd1	již
loď	loď	k1gFnSc1	loď
nepohybovala	pohybovat	k5eNaImAgFnS	pohybovat
po	po	k7c6	po
bezpečné	bezpečný	k2eAgFnSc6d1	bezpečná
translunární	translunární	k2eAgFnSc6d1	translunární
dráze	dráha	k1gFnSc6	dráha
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
vzdálena	vzdálen	k2eAgFnSc1d1	vzdálena
321	[number]	k4	321
860	[number]	k4	860
km	km	kA	km
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
:	:	kIx,	:
Zkreslení	zkreslení	k1gNnSc1	zkreslení
hlášení	hlášení	k1gNnSc2	hlášení
ve	v	k7c6	v
všeobecném	všeobecný	k2eAgNnSc6d1	všeobecné
povědomí	povědomí	k1gNnSc6	povědomí
nelze	lze	k6eNd1	lze
přičítat	přičítat	k5eAaImF	přičítat
filmu	film	k1gInSc3	film
Rona	Ron	k1gMnSc2	Ron
Howarda	Howard	k1gMnSc2	Howard
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
hláška	hláška	k1gFnSc1	hláška
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
chybné	chybný	k2eAgFnSc6d1	chybná
populární	populární	k2eAgFnSc6d1	populární
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
film	film	k1gInSc4	film
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
dramatizace	dramatizace	k1gFnSc2	dramatizace
některé	některý	k3yIgFnPc4	některý
nepřesnosti	nepřesnost	k1gFnPc4	nepřesnost
obsahoval	obsahovat	k5eAaImAgMnS	obsahovat
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
nemohlo	moct	k5eNaImAgNnS	moct
řídicí	řídicí	k2eAgNnSc1d1	řídicí
středisko	středisko	k1gNnSc1	středisko
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
přesně	přesně	k6eAd1	přesně
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
výbuchu	výbuch	k1gInSc6	výbuch
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
údajů	údaj	k1gInPc2	údaj
téměř	téměř	k6eAd1	téměř
normalizovala	normalizovat	k5eAaBmAgFnS	normalizovat
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
nabízelo	nabízet	k5eAaImAgNnS	nabízet
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
poruchu	porucha	k1gFnSc4	porucha
měření	měření	k1gNnSc2	měření
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
když	když	k8xS	když
se	se	k3xPyFc4	se
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
hodnoty	hodnota	k1gFnPc1	hodnota
ukazující	ukazující	k2eAgFnSc1d1	ukazující
ztrátu	ztráta	k1gFnSc4	ztráta
obsahu	obsah	k1gInSc2	obsah
kyslíkové	kyslíkový	k2eAgFnSc2d1	kyslíková
nádrže	nádrž	k1gFnSc2	nádrž
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
pozvolný	pozvolný	k2eAgInSc1d1	pozvolný
únik	únik	k1gInSc1	únik
z	z	k7c2	z
kyslíkové	kyslíkový	k2eAgFnSc2d1	kyslíková
nádrže	nádrž	k1gFnSc2	nádrž
č.	č.	k?	č.
1	[number]	k4	1
a	a	k8xC	a
zejména	zejména	k9	zejména
když	když	k8xS	když
Jim	on	k3xPp3gMnPc3	on
Lovell	Lovell	k1gInSc1	Lovell
oznámil	oznámit	k5eAaPmAgInS	oznámit
(	(	kIx(	(
<g/>
56	[number]	k4	56
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
-	-	kIx~	-
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
události	událost	k1gFnSc6	událost
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
looks	looks	k6eAd1	looks
to	ten	k3xDgNnSc1	ten
<g />
.	.	kIx.	.
</s>
<s>
me	me	k?	me
<g/>
,	,	kIx,	,
looking	looking	k1gInSc1	looking
out	out	k?	out
of	of	k?	of
the	the	k?	the
hatch	hatch	k1gMnSc1	hatch
<g/>
,	,	kIx,	,
that	that	k1gMnSc1	that
we	we	k?	we
are	ar	k1gInSc5	ar
venting	venting	k1gInSc1	venting
something	something	k1gInSc1	something
out	out	k?	out
into	into	k6eAd1	into
the	the	k?	the
space	space	k1gFnSc1	space
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
průhledem	průhled	k1gInSc7	průhled
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
nám	my	k3xPp1nPc3	my
něco	něco	k3yInSc1	něco
uniká	unikat	k5eAaImIp3nS	unikat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
řídicím	řídicí	k2eAgNnSc6d1	řídicí
středisku	středisko	k1gNnSc6	středisko
pochopili	pochopit	k5eAaPmAgMnP	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
pouhou	pouhý	k2eAgFnSc4d1	pouhá
poruchu	porucha	k1gFnSc4	porucha
měřicích	měřicí	k2eAgInPc2d1	měřicí
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
něco	něco	k3yInSc1	něco
závažnějšího	závažný	k2eAgNnSc2d2	závažnější
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
promíchávání	promíchávání	k1gNnSc6	promíchávání
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
nádrži	nádrž	k1gFnSc6	nádrž
vzniknul	vzniknout	k5eAaPmAgInS	vzniknout
zkrat	zkrat	k1gInSc1	zkrat
na	na	k7c6	na
elektrickém	elektrický	k2eAgInSc6d1	elektrický
přívodu	přívod	k1gInSc6	přívod
k	k	k7c3	k
motoru	motor	k1gInSc3	motor
promíchávací	promíchávací	k2eAgFnSc2d1	promíchávací
vrtule	vrtule	k1gFnSc2	vrtule
<g/>
,	,	kIx,	,
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
vzňala	vznít	k5eAaPmAgFnS	vznít
teflonová	teflonový	k2eAgFnSc1d1	teflonová
izolace	izolace	k1gFnSc1	izolace
a	a	k8xC	a
otevřený	otevřený	k2eAgInSc1d1	otevřený
oheň	oheň	k1gInSc1	oheň
v	v	k7c6	v
kyslíkové	kyslíkový	k2eAgFnSc6d1	kyslíková
nádrži	nádrž	k1gFnSc6	nádrž
způsobil	způsobit	k5eAaPmAgInS	způsobit
její	její	k3xOp3gInSc1	její
výbuch	výbuch	k1gInSc1	výbuch
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
roztrhnul	roztrhnout	k5eAaPmAgInS	roztrhnout
hliníkový	hliníkový	k2eAgInSc1d1	hliníkový
kryt	kryt	k1gInSc1	kryt
servisního	servisní	k2eAgInSc2d1	servisní
modulu	modul	k1gInSc2	modul
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
jeho	jeho	k3xOp3gFnSc6	jeho
délce	délka	k1gFnSc6	délka
<g/>
.	.	kIx.	.
</s>
<s>
Nádrž	nádrž	k1gFnSc1	nádrž
č.	č.	k?	č.
2	[number]	k4	2
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
zcela	zcela	k6eAd1	zcela
<g/>
,	,	kIx,	,
nádrž	nádrž	k1gFnSc1	nádrž
č.	č.	k?	č.
1	[number]	k4	1
pozvolna	pozvolna	k6eAd1	pozvolna
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
kyslík	kyslík	k1gInSc4	kyslík
trhlinami	trhlina	k1gFnPc7	trhlina
v	v	k7c6	v
potrubí	potrubí	k1gNnSc6	potrubí
nebo	nebo	k8xC	nebo
ventilech	ventil	k1gInPc6	ventil
<g/>
.	.	kIx.	.
</s>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
byl	být	k5eAaImAgInS	být
nezbytně	nezbytně	k6eAd1	nezbytně
potřeba	potřeba	k6eAd1	potřeba
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
dýchání	dýchání	k1gNnSc4	dýchání
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Servisní	servisní	k2eAgInSc1d1	servisní
modul	modul	k1gInSc1	modul
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
totiž	totiž	k9	totiž
palivové	palivový	k2eAgInPc4d1	palivový
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
z	z	k7c2	z
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
elektřinu	elektřina	k1gFnSc4	elektřina
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pokračující	pokračující	k2eAgInSc1d1	pokračující
únik	únik	k1gInSc1	únik
kyslíku	kyslík	k1gInSc2	kyslík
znamenal	znamenat	k5eAaImAgInS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
články	článek	k1gInPc1	článek
přestanou	přestat	k5eAaPmIp3nP	přestat
brzy	brzy	k6eAd1	brzy
definitivně	definitivně	k6eAd1	definitivně
fungovat	fungovat	k5eAaImF	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Velitelský	velitelský	k2eAgInSc1d1	velitelský
modul	modul	k1gInSc1	modul
měl	mít	k5eAaImAgInS	mít
sice	sice	k8xC	sice
akumulátor	akumulátor	k1gInSc1	akumulátor
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
malou	malý	k2eAgFnSc4d1	malá
nádrž	nádrž	k1gFnSc4	nádrž
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
ale	ale	k9	ale
byly	být	k5eAaImAgFnP	být
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
sestup	sestup	k1gInSc4	sestup
zemskou	zemský	k2eAgFnSc7d1	zemská
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
posádka	posádka	k1gFnSc1	posádka
akumulátor	akumulátor	k1gInSc1	akumulátor
k	k	k7c3	k
rozvodu	rozvod	k1gInSc3	rozvod
elektřiny	elektřina	k1gFnSc2	elektřina
připojila	připojit	k5eAaPmAgFnS	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Lunární	lunární	k2eAgInSc1d1	lunární
modul	modul	k1gInSc1	modul
měl	mít	k5eAaImAgInS	mít
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
kyslíkové	kyslíkový	k2eAgFnPc4d1	kyslíková
nádrže	nádrž	k1gFnPc4	nádrž
<g/>
,	,	kIx,	,
zásobu	zásoba	k1gFnSc4	zásoba
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
nabité	nabitý	k2eAgInPc1d1	nabitý
akumulátory	akumulátor	k1gInPc1	akumulátor
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
velkou	velký	k2eAgFnSc7d1	velká
kapacitou	kapacita	k1gFnSc7	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
tedy	tedy	k9	tedy
posloužit	posloužit	k5eAaPmF	posloužit
jako	jako	k9	jako
jakýsi	jakýsi	k3yIgInSc4	jakýsi
záchranný	záchranný	k2eAgInSc4d1	záchranný
člun	člun	k1gInSc4	člun
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
všechny	všechen	k3xTgFnPc1	všechen
zásoby	zásoba	k1gFnPc1	zásoba
byly	být	k5eAaImAgFnP	být
určeny	určit	k5eAaPmNgFnP	určit
k	k	k7c3	k
zásobování	zásobování	k1gNnSc1	zásobování
dvou	dva	k4xCgMnPc2	dva
astronautů	astronaut	k1gMnPc2	astronaut
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
dvou	dva	k4xCgInPc2	dva
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
musely	muset	k5eAaImAgFnP	muset
vystačit	vystačit	k5eAaBmF	vystačit
pro	pro	k7c4	pro
tři	tři	k4xCgMnPc4	tři
astronauty	astronaut	k1gMnPc4	astronaut
a	a	k8xC	a
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
<g/>
,	,	kIx,	,
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
snadným	snadný	k2eAgInSc7d1	snadný
úkolem	úkol	k1gInSc7	úkol
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
oživit	oživit	k5eAaPmF	oživit
lunární	lunární	k2eAgInSc4d1	lunární
modul	modul	k1gInSc4	modul
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
vypadne	vypadnout	k5eAaPmIp3nS	vypadnout
rozvod	rozvod	k1gInSc1	rozvod
elektřiny	elektřina	k1gFnSc2	elektřina
ve	v	k7c6	v
velitelském	velitelský	k2eAgInSc6d1	velitelský
modulu	modul	k1gInSc6	modul
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
systémy	systém	k1gInPc1	systém
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
byly	být	k5eAaImAgFnP	být
totiž	totiž	k9	totiž
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
úspory	úspora	k1gFnSc2	úspora
elektřiny	elektřina	k1gFnSc2	elektřina
během	během	k7c2	během
letu	let	k1gInSc2	let
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
vypnuty	vypnut	k2eAgMnPc4d1	vypnut
a	a	k8xC	a
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
oživení	oživení	k1gNnSc1	oživení
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
postupnému	postupný	k2eAgNnSc3d1	postupné
zapnutí	zapnutí	k1gNnSc3	zapnutí
v	v	k7c6	v
definovaném	definovaný	k2eAgNnSc6d1	definované
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
až	až	k9	až
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
výjimkou	výjimka	k1gFnSc7	výjimka
byla	být	k5eAaImAgNnP	být
elektrická	elektrický	k2eAgNnPc1d1	elektrické
topná	topný	k2eAgNnPc1d1	topné
tělíska	tělísko	k1gNnPc1	tělísko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
temperovala	temperovat	k5eAaBmAgFnS	temperovat
některé	některý	k3yIgInPc4	některý
systémy	systém	k1gInPc4	systém
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
poškození	poškození	k1gNnSc1	poškození
vlivem	vlivem	k7c2	vlivem
extrémně	extrémně	k6eAd1	extrémně
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
panujících	panující	k2eAgFnPc2d1	panující
v	v	k7c6	v
kosmickém	kosmický	k2eAgInSc6d1	kosmický
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
topná	topný	k2eAgNnPc1d1	topné
tělesa	těleso	k1gNnPc1	těleso
byla	být	k5eAaImAgNnP	být
napájena	napájet	k5eAaImNgNnP	napájet
z	z	k7c2	z
rozvodu	rozvod	k1gInSc2	rozvod
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
rozvodu	rozvod	k1gInSc2	rozvod
byla	být	k5eAaImAgFnS	být
napájena	napájet	k5eAaImNgFnS	napájet
i	i	k9	i
relé	relé	k1gNnPc7	relé
tvořící	tvořící	k2eAgInSc4d1	tvořící
přepínač	přepínač	k1gInSc4	přepínač
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
odpojit	odpojit	k5eAaPmF	odpojit
rozvod	rozvod	k1gInSc1	rozvod
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
od	od	k7c2	od
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
a	a	k8xC	a
připojit	připojit	k5eAaPmF	připojit
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
výkonné	výkonný	k2eAgInPc4d1	výkonný
akumulátory	akumulátor	k1gInPc4	akumulátor
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
ale	ale	k9	ale
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
síť	síť	k1gFnSc1	síť
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
již	již	k6eAd1	již
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
poskytnout	poskytnout	k5eAaPmF	poskytnout
výkon	výkon	k1gInSc4	výkon
dostačující	dostačující	k2eAgInSc4d1	dostačující
k	k	k7c3	k
sepnutí	sepnutí	k1gNnSc3	sepnutí
relé	relé	k1gNnSc2	relé
přepínače	přepínač	k1gInSc2	přepínač
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
problém	problém	k1gInSc4	problém
řešilo	řešit	k5eAaImAgNnS	řešit
řídící	řídící	k2eAgNnSc1d1	řídící
středisko	středisko	k1gNnSc1	středisko
při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
simulaci	simulace	k1gFnSc6	simulace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přípravy	příprava	k1gFnSc2	příprava
startu	start	k1gInSc2	start
Apolla	Apollo	k1gNnSc2	Apollo
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
simulována	simulován	k2eAgFnSc1d1	simulována
havárie	havárie	k1gFnSc1	havárie
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
měl	mít	k5eAaImAgInS	mít
lunární	lunární	k2eAgInSc1d1	lunární
modul	modul	k1gInSc1	modul
také	také	k9	také
posloužit	posloužit	k5eAaPmF	posloužit
jako	jako	k8xS	jako
záchranný	záchranný	k2eAgInSc1d1	záchranný
člun	člun	k1gInSc1	člun
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
simulaci	simulace	k1gFnSc6	simulace
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
nepodařilo	podařit	k5eNaPmAgNnS	podařit
lunární	lunární	k2eAgInSc4d1	lunární
modul	modul	k1gInSc4	modul
včas	včas	k6eAd1	včas
oživit	oživit	k5eAaPmF	oživit
a	a	k8xC	a
simulace	simulace	k1gFnSc1	simulace
skončila	skončit	k5eAaPmAgFnS	skončit
"	"	kIx"	"
<g/>
smrtí	smrtit	k5eAaImIp3nP	smrtit
<g/>
"	"	kIx"	"
posádky	posádka	k1gFnPc1	posádka
<g/>
.	.	kIx.	.
</s>
<s>
NASA	NASA	kA	NASA
poté	poté	k6eAd1	poté
tento	tento	k3xDgInSc4	tento
scénář	scénář	k1gInSc4	scénář
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
z	z	k7c2	z
přípravy	příprava	k1gFnSc2	příprava
letů	let	k1gInPc2	let
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
podobné	podobný	k2eAgFnSc2d1	podobná
havárie	havárie	k1gFnSc2	havárie
byla	být	k5eAaImAgFnS	být
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Pracovníci	pracovník	k1gMnPc1	pracovník
řídicího	řídicí	k2eAgNnSc2d1	řídicí
střediska	středisko	k1gNnSc2	středisko
se	se	k3xPyFc4	se
však	však	k9	však
tímto	tento	k3xDgInSc7	tento
scénářem	scénář	k1gInSc7	scénář
přesto	přesto	k6eAd1	přesto
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
oficiální	oficiální	k2eAgInPc4d1	oficiální
úkoly	úkol	k1gInPc4	úkol
<g/>
)	)	kIx)	)
dále	daleko	k6eAd2	daleko
zabývali	zabývat	k5eAaImAgMnP	zabývat
a	a	k8xC	a
získané	získaný	k2eAgFnPc1d1	získaná
zkušenosti	zkušenost	k1gFnPc1	zkušenost
se	se	k3xPyFc4	se
teď	teď	k6eAd1	teď
hodily	hodit	k5eAaImAgFnP	hodit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
využitím	využití	k1gNnSc7	využití
nabytých	nabytý	k2eAgFnPc2d1	nabytá
zkušeností	zkušenost	k1gFnPc2	zkušenost
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
řídicímu	řídicí	k2eAgNnSc3d1	řídicí
středisku	středisko	k1gNnSc3	středisko
za	za	k7c4	za
necelých	celý	k2eNgFnPc2d1	necelá
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
sestavit	sestavit	k5eAaPmF	sestavit
seznam	seznam	k1gInSc4	seznam
úkonů	úkon	k1gInPc2	úkon
nutných	nutný	k2eAgInPc2d1	nutný
k	k	k7c3	k
oživení	oživení	k1gNnSc3	oživení
elektrické	elektrický	k2eAgFnSc2d1	elektrická
sítě	síť	k1gFnSc2	síť
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Klíčový	klíčový	k2eAgInSc1d1	klíčový
trik	trik	k1gInSc1	trik
vycházel	vycházet	k5eAaImAgInS	vycházet
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
lunární	lunární	k2eAgInSc1d1	lunární
modul	modul	k1gInSc1	modul
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
dvě	dva	k4xCgFnPc4	dva
sady	sada	k1gFnPc4	sada
akumulátorů	akumulátor	k1gInPc2	akumulátor
-	-	kIx~	-
výkonnější	výkonný	k2eAgFnSc7d2	výkonnější
umístěnou	umístěný	k2eAgFnSc7d1	umístěná
v	v	k7c6	v
sestupové	sestupový	k2eAgFnSc6d1	sestupová
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
descent	descent	k1gInSc1	descent
stage	stagat	k5eAaPmIp3nS	stagat
<g/>
)	)	kIx)	)
a	a	k8xC	a
méně	málo	k6eAd2	málo
výkonné	výkonný	k2eAgInPc1d1	výkonný
umístěné	umístěný	k2eAgInPc1d1	umístěný
v	v	k7c6	v
návratové	návratový	k2eAgFnSc6d1	návratová
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
ascent	ascent	k1gInSc1	ascent
stage	stag	k1gInSc2	stag
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
využitím	využití	k1gNnSc7	využití
akumulátorů	akumulátor	k1gInPc2	akumulátor
návratové	návratový	k2eAgFnSc2d1	návratová
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
astronautům	astronaut	k1gMnPc3	astronaut
podařilo	podařit	k5eAaPmAgNnS	podařit
sepnout	sepnout	k5eAaPmF	sepnout
relé	relé	k1gNnSc1	relé
přepínače	přepínač	k1gInSc2	přepínač
a	a	k8xC	a
připojit	připojit	k5eAaPmF	připojit
tak	tak	k6eAd1	tak
rozvodnou	rozvodný	k2eAgFnSc4d1	rozvodná
síť	síť	k1gFnSc4	síť
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
k	k	k7c3	k
výkonným	výkonný	k2eAgInPc3d1	výkonný
akumulátorům	akumulátor	k1gInPc3	akumulátor
sestupové	sestupový	k2eAgFnSc2d1	sestupová
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ušetřilo	ušetřit	k5eAaPmAgNnS	ušetřit
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
akumulátoru	akumulátor	k1gInSc6	akumulátor
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
pro	pro	k7c4	pro
fázi	fáze	k1gFnSc4	fáze
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
sestupu	sestup	k1gInSc2	sestup
zemskou	zemský	k2eAgFnSc7d1	zemská
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
vypnout	vypnout	k5eAaPmF	vypnout
navigační	navigační	k2eAgInPc4d1	navigační
(	(	kIx(	(
<g/>
guidance	guidanec	k1gInPc4	guidanec
<g/>
)	)	kIx)	)
systém	systém	k1gInSc1	systém
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
poměrně	poměrně	k6eAd1	poměrně
velkou	velký	k2eAgFnSc4d1	velká
spotřebu	spotřeba	k1gFnSc4	spotřeba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gNnSc4	on
kromě	kromě	k7c2	kromě
palubního	palubní	k2eAgInSc2d1	palubní
počítače	počítač	k1gInSc2	počítač
tvořily	tvořit	k5eAaImAgInP	tvořit
i	i	k9	i
elektricky	elektricky	k6eAd1	elektricky
poháněné	poháněný	k2eAgInPc1d1	poháněný
gyroskopy	gyroskop	k1gInPc1	gyroskop
inerciální	inerciální	k2eAgFnSc2d1	inerciální
plošiny	plošina	k1gFnSc2	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Lunární	lunární	k2eAgInSc1d1	lunární
modul	modul	k1gInSc1	modul
měl	mít	k5eAaImAgInS	mít
identický	identický	k2eAgInSc4d1	identický
navigační	navigační	k2eAgInSc4d1	navigační
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
byl	být	k5eAaImAgMnS	být
oživen	oživen	k2eAgInSc4d1	oživen
elektrický	elektrický	k2eAgInSc4d1	elektrický
rozvod	rozvod	k1gInSc4	rozvod
v	v	k7c6	v
lunárním	lunární	k2eAgInSc6d1	lunární
modulu	modul	k1gInSc6	modul
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
oživit	oživit	k5eAaPmF	oživit
jeho	on	k3xPp3gInSc4	on
navigační	navigační	k2eAgInSc4d1	navigační
systém	systém	k1gInSc4	systém
a	a	k8xC	a
přenést	přenést	k5eAaPmF	přenést
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
data	datum	k1gNnSc2	datum
z	z	k7c2	z
navigačního	navigační	k2eAgInSc2d1	navigační
systému	systém	k1gInSc2	systém
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Nestačilo	stačit	k5eNaBmAgNnS	stačit
ale	ale	k9	ale
jenom	jenom	k6eAd1	jenom
přečíst	přečíst	k5eAaPmF	přečíst
data	datum	k1gNnPc4	datum
z	z	k7c2	z
displeje	displej	k1gFnSc2	displej
ve	v	k7c6	v
velitelském	velitelský	k2eAgInSc6d1	velitelský
modulu	modul	k1gInSc6	modul
a	a	k8xC	a
zadat	zadat	k5eAaPmF	zadat
je	on	k3xPp3gInPc4	on
do	do	k7c2	do
palubního	palubní	k2eAgInSc2d1	palubní
počítače	počítač	k1gInSc2	počítač
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc4	datum
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
přepočítat	přepočítat	k5eAaPmF	přepočítat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
lunární	lunární	k2eAgInSc1d1	lunární
modul	modul	k1gInSc1	modul
byl	být	k5eAaImAgInS	být
oproti	oproti	k7c3	oproti
velitelskému	velitelský	k2eAgInSc3d1	velitelský
modulu	modul	k1gInSc3	modul
o	o	k7c4	o
180	[number]	k4	180
stupňů	stupeň	k1gInPc2	stupeň
otočen	otočen	k2eAgMnSc1d1	otočen
(	(	kIx(	(
<g/>
velitelský	velitelský	k2eAgInSc1d1	velitelský
a	a	k8xC	a
lunární	lunární	k2eAgInSc1d1	lunární
modul	modul	k1gInSc1	modul
byly	být	k5eAaImAgFnP	být
spojeny	spojit	k5eAaPmNgInP	spojit
"	"	kIx"	"
<g/>
čely	čelo	k1gNnPc7	čelo
<g/>
"	"	kIx"	"
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
velitelský	velitelský	k2eAgInSc1d1	velitelský
modul	modul	k1gInSc1	modul
vlastně	vlastně	k9	vlastně
letěl	letět	k5eAaImAgInS	letět
pozpátku	pozpátku	k6eAd1	pozpátku
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
vyřešit	vyřešit	k5eAaPmF	vyřešit
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vrátit	vrátit	k5eAaPmF	vrátit
poškozenou	poškozený	k2eAgFnSc4d1	poškozená
loď	loď	k1gFnSc4	loď
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
sice	sice	k8xC	sice
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
scénář	scénář	k1gInSc1	scénář
"	"	kIx"	"
<g/>
přímého	přímý	k2eAgNnSc2d1	přímé
přerušení	přerušení	k1gNnSc2	přerušení
letu	let	k1gInSc2	let
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
direct	direct	k2eAgInSc1d1	direct
abort	abort	k1gInSc1	abort
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
zažehnout	zažehnout	k5eAaPmF	zažehnout
hlavní	hlavní	k2eAgInSc1d1	hlavní
motor	motor	k1gInSc1	motor
servisního	servisní	k2eAgInSc2d1	servisní
modulu	modul	k1gInSc2	modul
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nP	by
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
spotřebováno	spotřebován	k2eAgNnSc4d1	spotřebováno
veškeré	veškerý	k3xTgNnSc4	veškerý
palivo	palivo	k1gNnSc4	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
manévr	manévr	k1gInSc1	manévr
byl	být	k5eAaImAgInS	být
však	však	k9	však
za	za	k7c4	za
dané	daný	k2eAgFnPc4d1	daná
situace	situace	k1gFnPc4	situace
velmi	velmi	k6eAd1	velmi
riskantní	riskantní	k2eAgFnPc1d1	riskantní
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
totiž	totiž	k9	totiž
neznal	znát	k5eNaImAgMnS	znát
rozsah	rozsah	k1gInSc4	rozsah
poškození	poškození	k1gNnSc2	poškození
servisního	servisní	k2eAgInSc2d1	servisní
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Hrozila	hrozit	k5eAaImAgFnS	hrozit
dvě	dva	k4xCgNnPc4	dva
nebezpečí	nebezpečí	k1gNnPc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgInSc1d1	hlavní
motor	motor	k1gInSc1	motor
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
poškození	poškození	k1gNnSc2	poškození
při	při	k7c6	při
nedávném	dávný	k2eNgInSc6d1	nedávný
výbuchu	výbuch	k1gInSc6	výbuch
neudělí	udělit	k5eNaPmIp3nS	udělit
lodi	loď	k1gFnPc4	loď
potřebný	potřebný	k2eAgInSc1d1	potřebný
impuls	impuls	k1gInSc1	impuls
a	a	k8xC	a
loď	loď	k1gFnSc1	loď
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
při	při	k7c6	při
zažehnutí	zažehnutí	k1gNnSc6	zažehnutí
hlavního	hlavní	k2eAgInSc2d1	hlavní
motoru	motor	k1gInSc2	motor
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
výbuchu	výbuch	k1gInSc3	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Řídicí	řídicí	k2eAgNnSc1d1	řídicí
středisko	středisko	k1gNnSc1	středisko
proto	proto	k8xC	proto
zavrhlo	zavrhnout	k5eAaPmAgNnS	zavrhnout
scénář	scénář	k1gInSc4	scénář
direct	direct	k5eAaPmF	direct
abort	abort	k1gInSc4	abort
a	a	k8xC	a
nechalo	nechat	k5eAaPmAgNnS	nechat
loď	loď	k1gFnSc4	loď
pokračovat	pokračovat	k5eAaImF	pokračovat
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nenacházela	nacházet	k5eNaImAgFnS	nacházet
na	na	k7c6	na
bezpečné	bezpečný	k2eAgFnSc6d1	bezpečná
translunární	translunární	k2eAgFnSc6d1	translunární
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ji	on	k3xPp3gFnSc4	on
tam	tam	k6eAd1	tam
dostat	dostat	k5eAaPmF	dostat
<g/>
?	?	kIx.	?
</s>
<s>
Z	z	k7c2	z
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgInSc2d1	uvedený
důvodu	důvod	k1gInSc2	důvod
nikoliv	nikoliv	k9	nikoliv
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
hlavního	hlavní	k2eAgInSc2d1	hlavní
motoru	motor	k1gInSc2	motor
servisního	servisní	k2eAgInSc2d1	servisní
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Zbývalo	zbývat	k5eAaImAgNnS	zbývat
tedy	tedy	k9	tedy
provést	provést	k5eAaPmF	provést
manévr	manévr	k1gInSc4	manévr
pomocí	pomocí	k7c2	pomocí
motorů	motor	k1gInPc2	motor
sestupové	sestupový	k2eAgFnSc2d1	sestupová
části	část	k1gFnSc2	část
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
zkonstruován	zkonstruovat	k5eAaPmNgInS	zkonstruovat
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
relativně	relativně	k6eAd1	relativně
krátkému	krátký	k2eAgNnSc3d1	krátké
použití	použití	k1gNnSc3	použití
při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
spočítat	spočítat	k5eAaPmF	spočítat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
a	a	k8xC	a
na	na	k7c6	na
jak	jak	k6eAd1	jak
dlouho	dlouho	k6eAd1	dlouho
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
motory	motor	k1gInPc1	motor
zažehnuty	zažehnut	k2eAgInPc1d1	zažehnut
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výpočetním	výpočetní	k2eAgNnSc6d1	výpočetní
středisku	středisko	k1gNnSc6	středisko
ale	ale	k9	ale
neexistoval	existovat	k5eNaImAgInS	existovat
program	program	k1gInSc1	program
pro	pro	k7c4	pro
takový	takový	k3xDgInSc4	takový
výpočet	výpočet	k1gInSc4	výpočet
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
proto	proto	k8xC	proto
třeba	třeba	k6eAd1	třeba
takový	takový	k3xDgInSc4	takový
program	program	k1gInSc4	program
rychle	rychle	k6eAd1	rychle
vytvořit	vytvořit	k5eAaPmF	vytvořit
<g/>
,	,	kIx,	,
spočítat	spočítat	k5eAaPmF	spočítat
parametry	parametr	k1gInPc4	parametr
manévru	manévr	k1gInSc2	manévr
a	a	k8xC	a
parametry	parametr	k1gInPc1	parametr
překontrolovat	překontrolovat	k5eAaPmF	překontrolovat
odborníky	odborník	k1gMnPc4	odborník
v	v	k7c6	v
řídicím	řídicí	k2eAgNnSc6d1	řídicí
středisku	středisko	k1gNnSc6	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
necelých	celý	k2eNgFnPc2d1	necelá
tří	tři	k4xCgFnPc2	tři
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
určit	určit	k5eAaPmF	určit
parametry	parametr	k1gInPc4	parametr
manévru	manévr	k1gInSc2	manévr
pro	pro	k7c4	pro
návrat	návrat	k1gInSc4	návrat
na	na	k7c4	na
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
translunární	translunární	k2eAgFnSc4d1	translunární
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
translunární	translunární	k2eAgFnSc4d1	translunární
dráhu	dráha	k1gFnSc4	dráha
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
vyřešit	vyřešit	k5eAaPmF	vyřešit
další	další	k2eAgInSc4d1	další
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
této	tento	k3xDgFnSc2	tento
dráhy	dráha	k1gFnSc2	dráha
by	by	kYmCp3nS	by
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
přistála	přistát	k5eAaPmAgFnS	přistát
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ale	ale	k8xC	ale
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
během	během	k7c2	během
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
dopravit	dopravit	k5eAaPmF	dopravit
loď	loď	k1gFnSc4	loď
se	s	k7c7	s
záchranným	záchranný	k2eAgInSc7d1	záchranný
týmem	tým	k1gInSc7	tým
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
byly	být	k5eAaImAgInP	být
zapnuty	zapnout	k5eAaPmNgInP	zapnout
pouze	pouze	k6eAd1	pouze
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgInPc1d1	důležitý
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
nepatřilo	patřit	k5eNaImAgNnS	patřit
topení	topení	k1gNnSc1	topení
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
astronauti	astronaut	k1gMnPc1	astronaut
trpěli	trpět	k5eAaImAgMnP	trpět
zimou	zima	k1gFnSc7	zima
blížící	blížící	k2eAgFnSc7d1	blížící
se	se	k3xPyFc4	se
bodu	bůst	k5eAaImIp1nS	bůst
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
snížit	snížit	k5eAaPmF	snížit
spotřebu	spotřeba	k1gFnSc4	spotřeba
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
z	z	k7c2	z
běžných	běžný	k2eAgInPc2d1	běžný
50-75	[number]	k4	50-75
A	A	kA	A
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
na	na	k7c4	na
pouhých	pouhý	k2eAgFnPc2d1	pouhá
12	[number]	k4	12
A.	A.	kA	A.
Přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
spotřebě	spotřeba	k1gFnSc6	spotřeba
elektřiny	elektřina	k1gFnSc2	elektřina
doba	doba	k1gFnSc1	doba
čtyř	čtyři	k4xCgInPc2	čtyři
dnů	den	k1gInPc2	den
zbývajících	zbývající	k2eAgInPc2d1	zbývající
do	do	k7c2	do
přistání	přistání	k1gNnSc2	přistání
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
omezené	omezený	k2eAgFnSc3d1	omezená
kapacitě	kapacita	k1gFnSc3	kapacita
akumulátorů	akumulátor	k1gInPc2	akumulátor
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
příliš	příliš	k6eAd1	příliš
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Existovaly	existovat	k5eAaImAgFnP	existovat
dvě	dva	k4xCgFnPc4	dva
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
upravit	upravit	k5eAaPmF	upravit
dráhu	dráha	k1gFnSc4	dráha
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
loď	loď	k1gFnSc1	loď
přistála	přistát	k5eAaPmAgFnS	přistát
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
první	první	k4xOgFnSc2	první
varianty	varianta	k1gFnSc2	varianta
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
let	let	k1gInSc1	let
zkrátil	zkrátit	k5eAaPmAgInS	zkrátit
o	o	k7c4	o
36	[number]	k4	36
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
druhé	druhý	k4xOgFnSc2	druhý
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zkrátil	zkrátit	k5eAaPmAgMnS	zkrátit
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
varianta	varianta	k1gFnSc1	varianta
ale	ale	k9	ale
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nesla	nést	k5eAaImAgFnS	nést
dvě	dva	k4xCgNnPc4	dva
rizika	riziko	k1gNnPc4	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
by	by	kYmCp3nS	by
musela	muset	k5eAaImAgFnS	muset
okamžitě	okamžitě	k6eAd1	okamžitě
odhodit	odhodit	k5eAaPmF	odhodit
servisní	servisní	k2eAgInSc4d1	servisní
modul	modul	k1gInSc4	modul
a	a	k8xC	a
pokračovat	pokračovat	k5eAaImF	pokračovat
bez	bez	k7c2	bez
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
tepelný	tepelný	k2eAgInSc1d1	tepelný
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
chránící	chránící	k2eAgInSc1d1	chránící
velitelský	velitelský	k2eAgInSc1d1	velitelský
modul	modul	k1gInSc1	modul
před	před	k7c7	před
žárem	žár	k1gInSc7	žár
vznikajícím	vznikající	k2eAgInSc7d1	vznikající
při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
třením	tření	k1gNnSc7	tření
o	o	k7c4	o
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
po	po	k7c4	po
příliš	příliš	k6eAd1	příliš
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
vystaven	vystavit	k5eAaPmNgMnS	vystavit
působení	působení	k1gNnSc3	působení
otevřeného	otevřený	k2eAgInSc2d1	otevřený
vesmíru	vesmír	k1gInSc2	vesmír
(	(	kIx(	(
<g/>
mikrometeoritů	mikrometeorit	k1gInPc2	mikrometeorit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
spotřebováno	spotřebován	k2eAgNnSc1d1	spotřebováno
veškeré	veškerý	k3xTgNnSc4	veškerý
palivo	palivo	k1gNnSc4	palivo
sestupové	sestupový	k2eAgFnSc2d1	sestupová
části	část	k1gFnSc2	část
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
a	a	k8xC	a
nezůstala	zůstat	k5eNaPmAgFnS	zůstat
by	by	kYmCp3nS	by
žádná	žádný	k3yNgFnSc1	žádný
rezerva	rezerva	k1gFnSc1	rezerva
pro	pro	k7c4	pro
eventuální	eventuální	k2eAgFnSc4d1	eventuální
korekci	korekce	k1gFnSc4	korekce
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
přijata	přijat	k2eAgFnSc1d1	přijata
druhá	druhý	k4xOgFnSc1	druhý
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zkrátila	zkrátit	k5eAaPmAgFnS	zkrátit
let	let	k1gInSc4	let
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
uskutečnění	uskutečnění	k1gNnSc3	uskutečnění
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k9	třeba
2	[number]	k4	2
hodiny	hodina	k1gFnPc4	hodina
po	po	k7c4	po
dosažení	dosažení	k1gNnSc4	dosažení
bodu	bod	k1gInSc2	bod
největšího	veliký	k2eAgNnSc2d3	veliký
přiblížení	přiblížení	k1gNnSc2	přiblížení
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
provést	provést	k5eAaPmF	provést
zážeh	zážeh	k1gInSc4	zážeh
motoru	motor	k1gInSc2	motor
sestupové	sestupový	k2eAgFnSc2d1	sestupová
části	část	k1gFnSc2	část
lunárního	lunární	k2eAgInSc2d1	lunární
modelu	model	k1gInSc2	model
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
PC	PC	kA	PC
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
burn	burno	k1gNnPc2	burno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
zážeh	zážeh	k1gInSc4	zážeh
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
velmi	velmi	k6eAd1	velmi
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
řízení	řízení	k1gNnSc3	řízení
použit	použit	k2eAgInSc4d1	použit
palubní	palubní	k2eAgInSc4d1	palubní
počítač	počítač	k1gInSc4	počítač
navigačního	navigační	k2eAgInSc2d1	navigační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
měl	mít	k5eAaImAgMnS	mít
navigační	navigační	k2eAgInSc4d1	navigační
systém	systém	k1gInSc4	systém
velkou	velký	k2eAgFnSc4d1	velká
spotřebu	spotřeba	k1gFnSc4	spotřeba
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nepředpokládalo	předpokládat	k5eNaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
provádět	provádět	k5eAaImF	provádět
ještě	ještě	k9	ještě
nějaký	nějaký	k3yIgInSc4	nějaký
korekční	korekční	k2eAgInSc4d1	korekční
manévr	manévr	k1gInSc4	manévr
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
zážehu	zážeh	k1gInSc2	zážeh
PC	PC	kA	PC
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
burn	burno	k1gNnPc2	burno
navigační	navigační	k2eAgInSc1d1	navigační
systém	systém	k1gInSc1	systém
vypnut	vypnout	k5eAaPmNgInS	vypnout
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
další	další	k2eAgInSc1d1	další
problém	problém	k1gInSc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
stoupala	stoupat	k5eAaImAgFnS	stoupat
koncentrace	koncentrace	k1gFnSc1	koncentrace
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jímání	jímání	k1gNnSc3	jímání
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
byly	být	k5eAaImAgFnP	být
určeny	určit	k5eAaPmNgInP	určit
filtry	filtr	k1gInPc1	filtr
obsahující	obsahující	k2eAgInSc1d1	obsahující
hydroxid	hydroxid	k1gInSc4	hydroxid
lithný	lithný	k2eAgInSc4d1	lithný
(	(	kIx(	(
<g/>
LiOH	LiOH	k1gFnSc4	LiOH
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
kapacita	kapacita	k1gFnSc1	kapacita
instalovaná	instalovaný	k2eAgFnSc1d1	instalovaná
v	v	k7c6	v
lunárním	lunární	k2eAgInSc6d1	lunární
modulu	modul	k1gInSc6	modul
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
obsazení	obsazení	k1gNnSc6	obsazení
dvěma	dva	k4xCgFnPc7	dva
astronauty	astronaut	k1gMnPc7	astronaut
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
dvou	dva	k4xCgInPc2	dva
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
situaci	situace	k1gFnSc6	situace
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
byly	být	k5eAaImAgInP	být
sice	sice	k8xC	sice
podobné	podobný	k2eAgInPc1d1	podobný
filtry	filtr	k1gInPc1	filtr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
jiného	jiné	k1gNnSc2	jiné
tvaru	tvar	k1gInSc6	tvar
a	a	k8xC	a
tak	tak	k6eAd1	tak
nešly	jít	k5eNaImAgFnP	jít
v	v	k7c6	v
lunárním	lunární	k2eAgInSc6d1	lunární
modulu	modul	k1gInSc6	modul
použít	použít	k5eAaPmF	použít
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
však	však	k9	však
předvídal	předvídat	k5eAaImAgMnS	předvídat
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
pracovník	pracovník	k1gMnSc1	pracovník
řídicího	řídicí	k2eAgNnSc2d1	řídicí
střediska	středisko	k1gNnSc2	středisko
již	již	k6eAd1	již
od	od	k7c2	od
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c6	o
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
vymýšlel	vymýšlet	k5eAaImAgMnS	vymýšlet
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
pomůcek	pomůcka	k1gFnPc2	pomůcka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
mají	mít	k5eAaImIp3nP	mít
astronauti	astronaut	k1gMnPc1	astronaut
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
,	,	kIx,	,
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
filtry	filtr	k1gInPc4	filtr
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
lunárním	lunární	k2eAgInSc6d1	lunární
modulu	modul	k1gInSc6	modul
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	jeho	k3xOp3gFnPc2	jeho
instrukcí	instrukce	k1gFnPc2	instrukce
astronauti	astronaut	k1gMnPc1	astronaut
z	z	k7c2	z
ponožky	ponožka	k1gFnSc2	ponožka
<g/>
,	,	kIx,	,
lepicí	lepicí	k2eAgFnSc2d1	lepicí
pásky	páska	k1gFnSc2	páska
<g/>
,	,	kIx,	,
plastového	plastový	k2eAgInSc2d1	plastový
sáčku	sáček	k1gInSc2	sáček
a	a	k8xC	a
plastového	plastový	k2eAgInSc2d1	plastový
obalu	obal	k1gInSc2	obal
letové	letový	k2eAgFnSc2d1	letová
příručky	příručka	k1gFnSc2	příručka
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
adaptér	adaptér	k1gInSc4	adaptér
umožňující	umožňující	k2eAgInSc4d1	umožňující
instalovat	instalovat	k5eAaBmF	instalovat
v	v	k7c6	v
lunárním	lunární	k2eAgInSc6d1	lunární
modulu	modul	k1gInSc6	modul
filtry	filtr	k1gInPc1	filtr
z	z	k7c2	z
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgInS	být
zážeh	zážeh	k1gInSc1	zážeh
PC	PC	kA	PC
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
burn	burn	k1gInSc1	burn
proveden	proveden	k2eAgInSc1d1	proveden
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
loď	loď	k1gFnSc1	loď
odchylovat	odchylovat	k5eAaImF	odchylovat
od	od	k7c2	od
žádoucí	žádoucí	k2eAgFnSc2d1	žádoucí
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
otvor	otvor	k1gInSc1	otvor
na	na	k7c4	na
vypouštění	vypouštění	k1gNnSc4	vypouštění
odpadní	odpadní	k2eAgFnSc2d1	odpadní
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
rozuměj	rozumět	k5eAaImRp2nS	rozumět
moči	moč	k1gFnSc2	moč
<g/>
)	)	kIx)	)
do	do	k7c2	do
volného	volný	k2eAgInSc2d1	volný
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Fungoval	fungovat	k5eAaImAgMnS	fungovat
jako	jako	k9	jako
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
odchylující	odchylující	k2eAgFnSc1d1	odchylující
loď	loď	k1gFnSc1	loď
ze	z	k7c2	z
správné	správný	k2eAgFnSc2d1	správná
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
provést	provést	k5eAaPmF	provést
další	další	k2eAgInPc4d1	další
korekční	korekční	k2eAgInPc4d1	korekční
zážehy	zážeh	k1gInPc4	zážeh
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navigační	navigační	k2eAgInSc1d1	navigační
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
vypnut	vypnout	k5eAaPmNgInS	vypnout
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
loď	loď	k1gFnSc1	loď
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
zážehu	zážeh	k1gInSc2	zážeh
směřovat	směřovat	k5eAaImF	směřovat
správným	správný	k2eAgInSc7d1	správný
směrem	směr	k1gInSc7	směr
<g/>
?	?	kIx.	?
</s>
<s>
Přicházela	přicházet	k5eAaImAgFnS	přicházet
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
pouze	pouze	k6eAd1	pouze
technika	technikum	k1gNnPc1	technikum
používaná	používaný	k2eAgNnPc1d1	používané
již	již	k6eAd1	již
v	v	k7c6	v
programu	program	k1gInSc6	program
Mercury	Mercura	k1gFnSc2	Mercura
a	a	k8xC	a
Gemini	Gemin	k1gMnPc1	Gemin
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
orientaci	orientace	k1gFnSc3	orientace
lodi	loď	k1gFnSc2	loď
použita	použít	k5eAaPmNgFnS	použít
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
dobře	dobře	k6eAd1	dobře
rozpoznatelná	rozpoznatelný	k2eAgFnSc1d1	rozpoznatelná
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
dnem	den	k1gInSc7	den
a	a	k8xC	a
nocí	noc	k1gFnSc7	noc
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
programu	program	k1gInSc6	program
Mercury	Mercura	k1gFnSc2	Mercura
a	a	k8xC	a
Gemini	Gemin	k1gMnPc1	Gemin
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
používána	používán	k2eAgFnSc1d1	používána
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
loď	loď	k1gFnSc1	loď
nacházela	nacházet	k5eAaImAgFnS	nacházet
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
hrozilo	hrozit	k5eAaImAgNnS	hrozit
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
loď	loď	k1gFnSc1	loď
zorientována	zorientovat	k5eAaPmNgFnS	zorientovat
nepřesně	přesně	k6eNd1	přesně
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
tzv.	tzv.	kA	tzv.
vstupní	vstupní	k2eAgInSc4d1	vstupní
koridor	koridor	k1gInSc4	koridor
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
úzký	úzký	k2eAgInSc1d1	úzký
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nP	kdyby
loď	loď	k1gFnSc1	loď
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
pod	pod	k7c7	pod
nedostatečně	dostatečně	k6eNd1	dostatečně
ostrým	ostrý	k2eAgInSc7d1	ostrý
úhlem	úhel	k1gInSc7	úhel
<g/>
,	,	kIx,	,
odrazila	odrazit	k5eAaPmAgFnS	odrazit
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
naopak	naopak	k6eAd1	naopak
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
pod	pod	k7c7	pod
příliš	příliš	k6eAd1	příliš
ostrým	ostrý	k2eAgInSc7d1	ostrý
úhlem	úhel	k1gInSc7	úhel
<g/>
,	,	kIx,	,
shořela	shořet	k5eAaPmAgFnS	shořet
by	by	kYmCp3nS	by
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nadměrného	nadměrný	k2eAgNnSc2d1	nadměrné
tepelného	tepelný	k2eAgNnSc2d1	tepelné
namáhání	namáhání	k1gNnSc2	namáhání
způsobeného	způsobený	k2eAgInSc2d1	způsobený
třením	tření	k1gNnSc7	tření
o	o	k7c4	o
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
vrcholily	vrcholit	k5eAaImAgFnP	vrcholit
v	v	k7c6	v
řídicím	řídicí	k2eAgNnSc6d1	řídicí
středisku	středisko	k1gNnSc6	středisko
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
postupu	postup	k1gInSc2	postup
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
astronauti	astronaut	k1gMnPc1	astronaut
připraví	připravit	k5eAaPmIp3nP	připravit
velitelský	velitelský	k2eAgInSc4d1	velitelský
modul	modul	k1gInSc4	modul
na	na	k7c4	na
sestup	sestup	k1gInSc4	sestup
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
zajistit	zajistit	k5eAaPmF	zajistit
dostatek	dostatek	k1gInSc4	dostatek
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
přistání	přistání	k1gNnSc4	přistání
<g/>
.	.	kIx.	.
</s>
<s>
Akumulátor	akumulátor	k1gInSc1	akumulátor
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
vybit	vybít	k5eAaPmNgInS	vybít
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
před	před	k7c7	před
oživením	oživení	k1gNnSc7	oživení
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Pozemní	pozemní	k2eAgInSc1d1	pozemní
tým	tým	k1gInSc1	tým
proto	proto	k8xC	proto
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
akumulátor	akumulátor	k1gInSc1	akumulátor
dobít	dobít	k5eAaPmF	dobít
z	z	k7c2	z
rozvodné	rozvodný	k2eAgFnSc2d1	rozvodná
sítě	síť	k1gFnSc2	síť
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Velitelský	velitelský	k2eAgInSc1d1	velitelský
modul	modul	k1gInSc1	modul
neměl	mít	k5eNaImAgInS	mít
být	být	k5eAaImF	být
nikdy	nikdy	k6eAd1	nikdy
během	během	k7c2	během
letu	let	k1gInSc2	let
vypnut	vypnout	k5eAaPmNgInS	vypnout
<g/>
.	.	kIx.	.
</s>
<s>
Neexistoval	existovat	k5eNaImAgInS	existovat
proto	proto	k8xC	proto
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gInSc4	on
za	za	k7c2	za
letu	let	k1gInSc2	let
znovu	znovu	k6eAd1	znovu
oživit	oživit	k5eAaPmF	oživit
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
existující	existující	k2eAgInSc1d1	existující
postup	postup	k1gInSc1	postup
oživení	oživení	k1gNnSc2	oživení
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
se	se	k3xPyFc4	se
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
prováděl	provádět	k5eAaImAgInS	provádět
dlouho	dlouho	k6eAd1	dlouho
před	před	k7c7	před
startem	start	k1gInSc7	start
<g/>
.	.	kIx.	.
</s>
<s>
Očekávalo	očekávat	k5eAaImAgNnS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
akumulátor	akumulátor	k1gInSc1	akumulátor
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
nabít	nabít	k5eAaBmF	nabít
na	na	k7c4	na
20	[number]	k4	20
až	až	k9	až
25	[number]	k4	25
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
proto	proto	k8xC	proto
třeba	třeba	k6eAd1	třeba
s	s	k7c7	s
energií	energie	k1gFnSc7	energie
šetřit	šetřit	k5eAaImF	šetřit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
se	se	k3xPyFc4	se
zahajovalo	zahajovat	k5eAaImAgNnS	zahajovat
oživení	oživení	k1gNnSc1	oživení
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
zapnutím	zapnutí	k1gNnSc7	zapnutí
přístrojů	přístroj	k1gInPc2	přístroj
zobrazujících	zobrazující	k2eAgInPc2d1	zobrazující
naměřené	naměřený	k2eAgInPc4d1	naměřený
údaje	údaj	k1gInPc4	údaj
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
sledovat	sledovat	k5eAaImF	sledovat
další	další	k2eAgInSc4d1	další
postup	postup	k1gInSc4	postup
oživování	oživování	k1gNnSc2	oživování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
přístroje	přístroj	k1gInPc4	přístroj
zapnout	zapnout	k5eAaPmF	zapnout
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
před	před	k7c7	před
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
kontrolou	kontrola	k1gFnSc7	kontrola
údajů	údaj	k1gInPc2	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
postup	postup	k1gInSc1	postup
oživení	oživení	k1gNnSc2	oživení
proto	proto	k8xC	proto
museli	muset	k5eAaImAgMnP	muset
astronauti	astronaut	k1gMnPc1	astronaut
bezchybně	bezchybně	k6eAd1	bezchybně
provést	provést	k5eAaPmF	provést
naslepo	naslepo	k6eAd1	naslepo
navzdory	navzdory	k7c3	navzdory
jejich	jejich	k3xOp3gNnSc3	jejich
vyčerpání	vyčerpání	k1gNnSc3	vyčerpání
způsobenému	způsobený	k2eAgInSc3d1	způsobený
zimou	zima	k1gFnSc7	zima
<g/>
,	,	kIx,	,
nedostatkem	nedostatek	k1gInSc7	nedostatek
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
stresem	stres	k1gInSc7	stres
a	a	k8xC	a
pobytem	pobyt	k1gInSc7	pobyt
v	v	k7c6	v
extrémně	extrémně	k6eAd1	extrémně
malém	malý	k2eAgInSc6d1	malý
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
byl	být	k5eAaImAgMnS	být
odhozen	odhozen	k2eAgInSc4d1	odhozen
lunární	lunární	k2eAgInSc4d1	lunární
i	i	k8xC	i
servisní	servisní	k2eAgInSc4d1	servisní
modul	modul	k1gInSc4	modul
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
velitelský	velitelský	k2eAgInSc1d1	velitelský
modul	modul	k1gInSc1	modul
vzdálil	vzdálit	k5eAaPmAgInS	vzdálit
od	od	k7c2	od
servisního	servisní	k2eAgInSc2d1	servisní
modulu	modul	k1gInSc2	modul
<g/>
,	,	kIx,	,
uviděla	uvidět	k5eAaPmAgFnS	uvidět
teprve	teprve	k6eAd1	teprve
posádka	posádka	k1gFnSc1	posádka
rozsah	rozsah	k1gInSc1	rozsah
poškození	poškození	k1gNnSc2	poškození
servisního	servisní	k2eAgInSc2d1	servisní
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celá	k1gFnSc6	celá
jeho	jeho	k3xOp3gFnSc6	jeho
délce	délka	k1gFnSc6	délka
chyběl	chybět	k5eAaImAgInS	chybět
pruh	pruh	k1gInSc1	pruh
hliníkového	hliníkový	k2eAgInSc2d1	hliníkový
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Trhlina	trhlina	k1gFnSc1	trhlina
vedla	vést	k5eAaImAgFnS	vést
až	až	k9	až
k	k	k7c3	k
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
motoru	motor	k1gInSc3	motor
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
poškozen	poškodit	k5eAaPmNgInS	poškodit
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
tedy	tedy	k9	tedy
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgMnS	být
po	po	k7c6	po
nehodě	nehoda	k1gFnSc6	nehoda
zažehnut	zažehnut	k2eAgInSc1d1	zažehnut
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
hrozila	hrozit	k5eAaImAgFnS	hrozit
dvě	dva	k4xCgNnPc4	dva
nebezpečí	nebezpečí	k1gNnPc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
obava	obava	k1gFnSc1	obava
vyplývala	vyplývat	k5eAaImAgFnS	vyplývat
z	z	k7c2	z
možnosti	možnost	k1gFnSc2	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
výbuch	výbuch	k1gInSc1	výbuch
poškodil	poškodit	k5eAaPmAgInS	poškodit
tepelný	tepelný	k2eAgInSc1d1	tepelný
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
teď	teď	k6eAd1	teď
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
atmosférou	atmosféra	k1gFnSc7	atmosféra
nesplní	splnit	k5eNaPmIp3nS	splnit
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
kabina	kabina	k1gFnSc1	kabina
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
shoří	shořet	k5eAaPmIp3nS	shořet
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
obava	obava	k1gFnSc1	obava
padla	padnout	k5eAaPmAgFnS	padnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
čtyřminutovém	čtyřminutový	k2eAgInSc6d1	čtyřminutový
výpadku	výpadek	k1gInSc6	výpadek
spojení	spojení	k1gNnSc2	spojení
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
největšího	veliký	k2eAgNnSc2d3	veliký
tepelného	tepelný	k2eAgNnSc2d1	tepelné
namáhání	namáhání	k1gNnSc2	namáhání
kabiny	kabina	k1gFnSc2	kabina
<g/>
,	,	kIx,	,
způsobeného	způsobený	k2eAgInSc2d1	způsobený
vrstvou	vrstva	k1gFnSc7	vrstva
ionizovaného	ionizovaný	k2eAgInSc2d1	ionizovaný
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
kabiny	kabina	k1gFnSc2	kabina
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
navázat	navázat	k5eAaPmF	navázat
s	s	k7c7	s
navracející	navracející	k2eAgFnSc7d1	navracející
se	se	k3xPyFc4	se
lodí	loď	k1gFnSc7	loď
radiové	radiový	k2eAgNnSc4d1	radiové
spojení	spojení	k1gNnSc4	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
riziko	riziko	k1gNnSc1	riziko
vycházelo	vycházet	k5eAaImAgNnS	vycházet
z	z	k7c2	z
možnosti	možnost	k1gFnSc2	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
selže	selhat	k5eAaPmIp3nS	selhat
systém	systém	k1gInSc4	systém
pro	pro	k7c4	pro
vypuštění	vypuštění	k1gNnSc4	vypuštění
brzdicích	brzdicí	k2eAgInPc2d1	brzdicí
padáků	padák	k1gInPc2	padák
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
nefunkční	funkční	k2eNgNnSc4d1	nefunkční
vlivem	vliv	k1gInSc7	vliv
jeho	jeho	k3xOp3gNnSc2	jeho
vystavení	vystavení	k1gNnSc2	vystavení
extrémně	extrémně	k6eAd1	extrémně
nízké	nízký	k2eAgFnSc6d1	nízká
teplotě	teplota	k1gFnSc6	teplota
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
fázi	fáze	k1gFnSc6	fáze
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vypnut	vypnout	k5eAaPmNgInS	vypnout
napájecí	napájecí	k2eAgInSc1d1	napájecí
systém	systém	k1gInSc1	systém
velitelského	velitelský	k2eAgInSc2d1	velitelský
a	a	k8xC	a
servisního	servisní	k2eAgInSc2d1	servisní
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
pro	pro	k7c4	pro
posádku	posádka	k1gFnSc4	posádka
se	se	k3xPyFc4	se
ani	ani	k9	ani
tato	tento	k3xDgFnSc1	tento
obava	obava	k1gFnSc1	obava
nenaplnila	naplnit	k5eNaPmAgFnS	naplnit
a	a	k8xC	a
velitelský	velitelský	k2eAgInSc1d1	velitelský
modul	modul	k1gInSc1	modul
Apolla	Apollo	k1gNnSc2	Apollo
13	[number]	k4	13
přistál	přistát	k5eAaImAgInS	přistát
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1970	[number]	k4	1970
v	v	k7c4	v
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
41	[number]	k4	41
UT	UT	kA	UT
po	po	k7c6	po
letu	let	k1gInSc6	let
trvajícím	trvající	k2eAgMnSc6d1	trvající
5	[number]	k4	5
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
22	[number]	k4	22
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
54	[number]	k4	54
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
41	[number]	k4	41
sekund	sekunda	k1gFnPc2	sekunda
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Americké	americký	k2eAgFnSc2d1	americká
Samoy	Samoa	k1gFnSc2	Samoa
<g/>
,	,	kIx,	,
4,5	[number]	k4	4,5
km	km	kA	km
od	od	k7c2	od
záchranné	záchranný	k2eAgFnSc2d1	záchranná
lodi	loď	k1gFnSc2	loď
USS	USS	kA	USS
Iwo	Iwo	k1gMnSc1	Iwo
Jima	Jima	k1gMnSc1	Jima
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
popisu	popis	k1gInSc2	popis
průběhu	průběh	k1gInSc2	průběh
letu	let	k1gInSc2	let
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
příčinou	příčina	k1gFnSc7	příčina
nehody	nehoda	k1gFnSc2	nehoda
byla	být	k5eAaImAgFnS	být
kyslíková	kyslíkový	k2eAgFnSc1d1	kyslíková
nádrž	nádrž	k1gFnSc1	nádrž
číslo	číslo	k1gNnSc1	číslo
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
následovalo	následovat	k5eAaImAgNnS	následovat
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
přistání	přistání	k1gNnSc6	přistání
Apolla	Apollo	k1gNnSc2	Apollo
13	[number]	k4	13
<g/>
,	,	kIx,	,
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
nehodě	nehoda	k1gFnSc3	nehoda
vedlo	vést	k5eAaImAgNnS	vést
náhodné	náhodný	k2eAgNnSc1d1	náhodné
zřetězení	zřetězení	k1gNnSc1	zřetězení
několika	několik	k4yIc2	několik
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Dodavatelem	dodavatel	k1gMnSc7	dodavatel
velitelského	velitelský	k2eAgInSc2d1	velitelský
a	a	k8xC	a
servisního	servisní	k2eAgInSc2d1	servisní
modulu	modul	k1gInSc2	modul
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
North	North	k1gMnSc1	North
American	American	k1gMnSc1	American
Rockwell	Rockwell	k1gMnSc1	Rockwell
<g/>
,	,	kIx,	,
subdodavatelem	subdodavatel	k1gMnSc7	subdodavatel
kyslíkových	kyslíkový	k2eAgFnPc2d1	kyslíková
nádrží	nádrž	k1gFnPc2	nádrž
pak	pak	k6eAd1	pak
společnost	společnost	k1gFnSc1	společnost
Beech	Beech	k1gInSc4	Beech
Aircraft	Aircraft	k2eAgInSc1d1	Aircraft
Corporation	Corporation	k1gInSc1	Corporation
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
specifikací	specifikace	k1gFnPc2	specifikace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
North	North	k1gMnSc1	North
American	American	k1gMnSc1	American
Rockwell	Rockwell	k1gMnSc1	Rockwell
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
elektrické	elektrický	k2eAgNnSc1d1	elektrické
zařízení	zařízení	k1gNnSc1	zařízení
nádrže	nádrž	k1gFnSc2	nádrž
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
topná	topný	k2eAgNnPc1d1	topné
tělesa	těleso	k1gNnPc1	těleso
s	s	k7c7	s
termostatickými	termostatický	k2eAgInPc7d1	termostatický
regulátory	regulátor	k1gInPc7	regulátor
a	a	k8xC	a
motory	motor	k1gInPc1	motor
pohánějící	pohánějící	k2eAgFnSc2d1	pohánějící
vrtule	vrtule	k1gFnSc2	vrtule
na	na	k7c4	na
promíchávání	promíchávání	k1gNnSc4	promíchávání
kapalného	kapalný	k2eAgNnSc2d1	kapalné
paliva	palivo	k1gNnSc2	palivo
<g/>
)	)	kIx)	)
napájeno	napájen	k2eAgNnSc1d1	napájeno
ze	z	k7c2	z
stejnosměrné	stejnosměrný	k2eAgFnSc2d1	stejnosměrná
soustavy	soustava	k1gFnSc2	soustava
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
o	o	k7c6	o
napětí	napětí	k1gNnSc6	napětí
28	[number]	k4	28
V.	V.	kA	V.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
vydala	vydat	k5eAaPmAgFnS	vydat
North	North	k1gInSc4	North
American	Americana	k1gFnPc2	Americana
Rockwell	Rockwell	k1gInSc4	Rockwell
změnu	změna	k1gFnSc4	změna
specifikace	specifikace	k1gFnSc2	specifikace
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
elektrické	elektrický	k2eAgNnSc4d1	elektrické
zařízení	zařízení	k1gNnSc4	zařízení
kyslíkových	kyslíkový	k2eAgFnPc2d1	kyslíková
nádrží	nádrž	k1gFnPc2	nádrž
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
raketa	raketa	k1gFnSc1	raketa
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
odpalovací	odpalovací	k2eAgFnSc6d1	odpalovací
rampě	rampa	k1gFnSc6	rampa
<g/>
,	,	kIx,	,
napájeno	napájen	k2eAgNnSc1d1	napájeno
stejnosměrným	stejnosměrný	k2eAgNnSc7d1	stejnosměrné
napětím	napětí	k1gNnSc7	napětí
65	[number]	k4	65
V.	V.	kA	V.
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
zkrátit	zkrátit	k5eAaPmF	zkrátit
dobu	doba	k1gFnSc4	doba
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
a	a	k8xC	a
natlakování	natlakování	k1gNnSc3	natlakování
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Beech	Beech	k1gInSc1	Beech
Aircraft	Aircraft	k2eAgInSc4d1	Aircraft
Corporation	Corporation	k1gInSc4	Corporation
objednala	objednat	k5eAaPmAgFnS	objednat
termostatické	termostatický	k2eAgInPc4d1	termostatický
spínače	spínač	k1gInPc4	spínač
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
regulovat	regulovat	k5eAaImF	regulovat
teplotu	teplota	k1gFnSc4	teplota
uvnitř	uvnitř	k7c2	uvnitř
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
původní	původní	k2eAgFnSc2d1	původní
specifikace	specifikace	k1gFnSc2	specifikace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nádrží	nádrž	k1gFnPc2	nádrž
tak	tak	k9	tak
byly	být	k5eAaImAgInP	být
montovány	montován	k2eAgInPc4d1	montován
termostaty	termostat	k1gInPc4	termostat
navržené	navržený	k2eAgInPc4d1	navržený
pro	pro	k7c4	pro
původní	původní	k2eAgNnSc4d1	původní
napětí	napětí	k1gNnSc4	napětí
sítě	síť	k1gFnSc2	síť
28	[number]	k4	28
V.	V.	kA	V.
Tato	tento	k3xDgFnSc1	tento
chyba	chyba	k1gFnSc1	chyba
by	by	k9	by
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
havárii	havárie	k1gFnSc4	havárie
nezpůsobila	způsobit	k5eNaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
se	se	k3xPyFc4	se
topná	topný	k2eAgNnPc1d1	topné
tělesa	těleso	k1gNnPc1	těleso
zapínala	zapínat	k5eAaImAgNnP	zapínat
vždy	vždy	k6eAd1	vždy
jen	jen	k6eAd1	jen
krátkodobě	krátkodobě	k6eAd1	krátkodobě
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
předchozí	předchozí	k2eAgInPc4d1	předchozí
lety	let	k1gInPc4	let
lodí	loď	k1gFnPc2	loď
Apollo	Apollo	k1gNnSc1	Apollo
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
termostaty	termostat	k1gInPc4	termostat
jejich	jejich	k3xOp3gFnPc2	jejich
kyslíkových	kyslíkový	k2eAgFnPc2d1	kyslíková
nádrží	nádrž	k1gFnPc2	nádrž
byly	být	k5eAaImAgFnP	být
rovněž	rovněž	k9	rovněž
poddimenzovány	poddimenzovat	k5eAaBmNgFnP	poddimenzovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Apolla	Apollo	k1gNnSc2	Apollo
13	[number]	k4	13
však	však	k9	však
následovaly	následovat	k5eAaImAgFnP	následovat
další	další	k2eAgFnPc1d1	další
události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
efektu	efekt	k1gInSc6	efekt
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
havárii	havárie	k1gFnSc3	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Kyslíková	kyslíkový	k2eAgFnSc1d1	kyslíková
nádrž	nádrž	k1gFnSc1	nádrž
číslo	číslo	k1gNnSc1	číslo
2	[number]	k4	2
Apolla	Apollo	k1gNnSc2	Apollo
13	[number]	k4	13
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
let	let	k1gInSc4	let
Apolla	Apollo	k1gNnSc2	Apollo
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
přípravy	příprava	k1gFnSc2	příprava
letu	let	k1gInSc2	let
Apolla	Apollo	k1gNnSc2	Apollo
10	[number]	k4	10
však	však	k8xC	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
pádu	pád	k1gInSc3	pád
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
několika	několik	k4yIc2	několik
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Apolla	Apollo	k1gNnSc2	Apollo
10	[number]	k4	10
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
jistotu	jistota	k1gFnSc4	jistota
zabudována	zabudován	k2eAgFnSc1d1	zabudována
náhradní	náhradní	k2eAgFnSc1d1	náhradní
nádrž	nádrž	k1gFnSc1	nádrž
a	a	k8xC	a
původní	původní	k2eAgFnSc1d1	původní
byla	být	k5eAaImAgFnS	být
odeslána	odeslat	k5eAaPmNgFnS	odeslat
k	k	k7c3	k
inspekci	inspekce	k1gFnSc3	inspekce
<g/>
.	.	kIx.	.
</s>
<s>
Inspekce	inspekce	k1gFnSc1	inspekce
žádné	žádný	k3yNgNnSc4	žádný
poškození	poškození	k1gNnSc4	poškození
nádrže	nádrž	k1gFnSc2	nádrž
neshledala	shledat	k5eNaPmAgFnS	shledat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
namontována	namontovat	k5eAaPmNgFnS	namontovat
do	do	k7c2	do
servisního	servisní	k2eAgInSc2d1	servisní
modulu	modul	k1gInSc2	modul
Apolla	Apollo	k1gNnSc2	Apollo
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
poškozeno	poškozen	k2eAgNnSc1d1	poškozeno
plnicí	plnicí	k2eAgNnSc1d1	plnicí
<g/>
/	/	kIx~	/
<g/>
vypouštěcí	vypouštěcí	k2eAgNnSc1d1	vypouštěcí
potrubí	potrubí	k1gNnSc1	potrubí
uvnitř	uvnitř	k7c2	uvnitř
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
během	během	k7c2	během
přípravy	příprava	k1gFnSc2	příprava
startu	start	k1gInSc2	start
Apolla	Apollo	k1gNnSc2	Apollo
13	[number]	k4	13
<g/>
,	,	kIx,	,
když	když	k8xS	když
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
po	po	k7c6	po
zkušebním	zkušební	k2eAgInSc6d1	zkušební
naplnění	naplnění	k1gNnSc2	naplnění
kapalným	kapalný	k2eAgInSc7d1	kapalný
kyslíkem	kyslík	k1gInSc7	kyslík
nádrž	nádrž	k1gFnSc4	nádrž
vyprázdnit	vyprázdnit	k5eAaPmF	vyprázdnit
<g/>
.	.	kIx.	.
</s>
<s>
Vyprázdnění	vyprázdnění	k1gNnSc1	vyprázdnění
se	se	k3xPyFc4	se
normálně	normálně	k6eAd1	normálně
provádělo	provádět	k5eAaImAgNnS	provádět
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
nádrže	nádrž	k1gFnSc2	nádrž
vháněl	vhánět	k5eAaImAgInS	vhánět
plynný	plynný	k2eAgInSc1d1	plynný
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
vytlačit	vytlačit	k5eAaPmF	vytlačit
kapalný	kapalný	k2eAgInSc1d1	kapalný
kyslík	kyslík	k1gInSc1	kyslík
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
nedařilo	dařit	k5eNaImAgNnS	dařit
nádrž	nádrž	k1gFnSc4	nádrž
vyprázdnit	vyprázdnit	k5eAaPmF	vyprázdnit
<g/>
,	,	kIx,	,
padlo	padnout	k5eAaPmAgNnS	padnout
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
zapnout	zapnout	k5eAaPmF	zapnout
topná	topný	k2eAgNnPc1d1	topné
tělesa	těleso	k1gNnPc1	těleso
uvnitř	uvnitř	k7c2	uvnitř
nádrže	nádrž	k1gFnSc2	nádrž
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
napomoci	napomoct	k5eAaPmF	napomoct
vyprázdnění	vyprázdnění	k1gNnSc2	vyprázdnění
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
zatížení	zatížení	k1gNnSc6	zatížení
termostatu	termostat	k1gInSc2	termostat
napětím	napětí	k1gNnSc7	napětí
65	[number]	k4	65
V	V	kA	V
se	se	k3xPyFc4	se
však	však	k9	však
jeho	jeho	k3xOp3gInPc1	jeho
kontakty	kontakt	k1gInPc1	kontakt
spekly	spéct	k5eAaPmAgInP	spéct
a	a	k8xC	a
termostat	termostat	k1gInSc1	termostat
přestal	přestat	k5eAaPmAgInS	přestat
vypínat	vypínat	k5eAaImF	vypínat
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nP	kdyby
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
někdo	někdo	k3yInSc1	někdo
analyzoval	analyzovat	k5eAaImAgMnS	analyzovat
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
proudu	proud	k1gInSc6	proud
tekoucím	tekoucí	k2eAgInSc6d1	tekoucí
topným	topné	k1gNnSc7	topné
tělesem	těleso	k1gNnSc7	těleso
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgMnS	zjistit
by	by	kYmCp3nS	by
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nepřerušovaný	přerušovaný	k2eNgMnSc1d1	nepřerušovaný
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vydedukovat	vydedukovat	k5eAaPmF	vydedukovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
termostat	termostat	k1gInSc1	termostat
řádně	řádně	k6eAd1	řádně
nefunguje	fungovat	k5eNaImIp3nS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Termostat	termostat	k1gInSc1	termostat
měl	mít	k5eAaImAgInS	mít
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplota	teplota	k1gFnSc1	teplota
uvnitř	uvnitř	k7c2	uvnitř
nádrže	nádrž	k1gFnSc2	nádrž
nepřesáhne	přesáhnout	k5eNaPmIp3nS	přesáhnout
asi	asi	k9	asi
27	[number]	k4	27
°	°	k?	°
<g/>
C.	C.	kA	C.
Jeho	jeho	k3xOp3gInPc1	jeho
porucha	porucha	k1gFnSc1	porucha
však	však	k9	však
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
vyprazdňování	vyprazdňování	k1gNnSc6	vyprazdňování
nádrže	nádrž	k1gFnSc2	nádrž
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
teplota	teplota	k1gFnSc1	teplota
více	hodně	k6eAd2	hodně
než	než	k8xS	než
500	[number]	k4	500
°	°	k?	°
<g/>
C.	C.	kA	C.
Tak	tak	k6eAd1	tak
vysoká	vysoký	k2eAgFnSc1d1	vysoká
teplota	teplota	k1gFnSc1	teplota
poškodila	poškodit	k5eAaPmAgFnS	poškodit
teflonovou	teflonový	k2eAgFnSc4d1	teflonová
izolaci	izolace	k1gFnSc4	izolace
napájení	napájení	k1gNnSc2	napájení
motorů	motor	k1gInPc2	motor
promíchávacích	promíchávací	k2eAgFnPc2d1	promíchávací
vrtulí	vrtule	k1gFnPc2	vrtule
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
když	když	k8xS	když
posádka	posádka	k1gFnSc1	posádka
během	během	k7c2	během
letu	let	k1gInSc2	let
zapnula	zapnout	k5eAaPmAgFnS	zapnout
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
promíchávání	promíchávání	k1gNnSc2	promíchávání
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zkratu	zkrat	k1gInSc3	zkrat
a	a	k8xC	a
následnému	následný	k2eAgInSc3d1	následný
výbuchu	výbuch	k1gInSc3	výbuch
<g/>
.	.	kIx.	.
</s>
