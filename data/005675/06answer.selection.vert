<s>
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
jsou	být	k5eAaImIp3nP	být
hromadná	hromadný	k2eAgFnSc1d1	hromadná
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
sportovní	sportovní	k2eAgFnSc1d1	sportovní
soutěž	soutěž	k1gFnSc1	soutěž
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgFnPc6d1	různá
disciplínách	disciplína	k1gFnPc6	disciplína
a	a	k8xC	a
sportech	sport	k1gInPc6	sport
<g/>
,	,	kIx,	,
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
sportovců	sportovec	k1gMnPc2	sportovec
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
pořádají	pořádat	k5eAaImIp3nP	pořádat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
hrami	hra	k1gFnPc7	hra
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
olympiáda	olympiáda	k1gFnSc1	olympiáda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
