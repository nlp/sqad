<s>
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
jsou	být	k5eAaImIp3nP	být
hromadná	hromadný	k2eAgFnSc1d1	hromadná
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
sportovní	sportovní	k2eAgFnSc1d1	sportovní
soutěž	soutěž	k1gFnSc1	soutěž
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgFnPc6d1	různá
disciplínách	disciplína	k1gFnPc6	disciplína
a	a	k8xC	a
sportech	sport	k1gInPc6	sport
<g/>
,	,	kIx,	,
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
sportovců	sportovec	k1gMnPc2	sportovec
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pořádají	pořádat	k5eAaImIp3nP	pořádat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
hrami	hra	k1gFnPc7	hra
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
olympiáda	olympiáda	k1gFnSc1	olympiáda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zkratkou	zkratka	k1gFnSc7
OH	OH	kA
</s>
<s>
Svého	svůj	k3xOyFgMnSc4	svůj
předchůdce	předchůdce	k1gMnSc4	předchůdce
a	a	k8xC	a
vzor	vzor	k1gInSc4	vzor
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
antických	antický	k2eAgFnPc6d1	antická
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
konaných	konaný	k2eAgInPc2d1	konaný
ve	v	k7c6	v
starém	starý	k2eAgNnSc6d1	staré
Řecku	Řecko	k1gNnSc6	Řecko
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Peloponés	Peloponés	k1gInSc1	Peloponés
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
zakladatelem	zakladatel	k1gMnSc7	zakladatel
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
Hérakles	Hérakles	k1gMnSc1	Hérakles
<g/>
.	.	kIx.	.
</s>
<s>
Pořádají	pořádat	k5eAaImIp3nP	pořádat
se	se	k3xPyFc4	se
jako	jako	k9	jako
<g/>
:	:	kIx,	:
Letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
od	od	k7c2	od
obnovení	obnovení	k1gNnSc2	obnovení
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
Zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
Letní	letní	k2eAgFnSc1d1	letní
i	i	k8xC	i
zimní	zimní	k2eAgFnSc1d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
vždy	vždy	k6eAd1	vždy
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
termíny	termín	k1gInPc1	termín
her	hra	k1gFnPc2	hra
posunuty	posunout	k5eAaPmNgInP	posunout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
vystřídaly	vystřídat	k5eAaPmAgFnP	vystřídat
<g/>
.	.	kIx.	.
</s>
<s>
Samostatná	samostatný	k2eAgFnSc1d1	samostatná
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
organizačně	organizačně	k6eAd1	organizačně
částečně	částečně	k6eAd1	částečně
přidružená	přidružený	k2eAgFnSc1d1	přidružená
je	být	k5eAaImIp3nS	být
šachová	šachový	k2eAgFnSc1d1	šachová
olympiáda	olympiáda	k1gFnSc1	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
oficiálně	oficiálně	k6eAd1	oficiálně
pořádány	pořádán	k2eAgFnPc1d1	pořádána
i	i	k8xC	i
letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
pak	pak	k6eAd1	pak
zimní	zimní	k2eAgFnPc4d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Antické	antický	k2eAgFnSc2d1	antická
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Antické	antický	k2eAgFnPc1d1	antická
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
byly	být	k5eAaImAgFnP	být
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnPc1d3	veliký
a	a	k8xC	a
nejstarší	starý	k2eAgFnPc1d3	nejstarší
z	z	k7c2	z
všeřeckých	všeřecký	k2eAgFnPc2d1	všeřecký
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
pouze	pouze	k6eAd1	pouze
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
měly	mít	k5eAaImAgFnP	mít
hry	hra	k1gFnPc4	hra
zvané	zvaný	k2eAgFnPc4d1	zvaná
héraia	héraius	k1gMnSc4	héraius
podle	podle	k7c2	podle
bohyně	bohyně	k1gFnSc2	bohyně
Héry	Héra	k1gFnSc2	Héra
<g/>
.	.	kIx.	.
</s>
<s>
Konaly	konat	k5eAaImAgInP	konat
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
776	[number]	k4	776
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
393	[number]	k4	393
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
394	[number]	k4	394
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
ediktem	edikt	k1gInSc7	edikt
Theodosius	Theodosius	k1gMnSc1	Theodosius
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
426	[number]	k4	426
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
rozkaz	rozkaz	k1gInSc1	rozkaz
ke	k	k7c3	k
zbourání	zbourání	k1gNnSc3	zbourání
chrámů	chrám	k1gInPc2	chrám
a	a	k8xC	a
soch	socha	k1gFnPc2	socha
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
zkázy	zkáza	k1gFnSc2	zkáza
bylo	být	k5eAaImAgNnS	být
dokonáno	dokonat	k5eAaPmNgNnS	dokonat
zemětřeseními	zemětřesení	k1gNnPc7	zemětřesení
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
522	[number]	k4	522
a	a	k8xC	a
551	[number]	k4	551
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
krůčky	krůček	k1gInPc1	krůček
na	na	k7c6	na
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
prvním	první	k4xOgFnPc3	první
novodobým	novodobý	k2eAgFnPc3d1	novodobá
hrám	hra	k1gFnPc3	hra
patří	patřit	k5eAaImIp3nS	patřit
řeckým	řecký	k2eAgMnPc3d1	řecký
učencům	učenec	k1gMnPc3	učenec
z	z	k7c2	z
Byzance	Byzanc	k1gFnSc2	Byzanc
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
působili	působit	k5eAaImAgMnP	působit
na	na	k7c6	na
italských	italský	k2eAgFnPc6d1	italská
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc3	jejich
žákům	žák	k1gMnPc3	žák
<g/>
.	.	kIx.	.
</s>
<s>
Pietro	Pietro	k6eAd1	Pietro
Paolo	Paolo	k1gNnSc1	Paolo
Vergerio	Vergerio	k1gNnSc1	Vergerio
(	(	kIx(	(
<g/>
1348	[number]	k4	1348
<g/>
–	–	k?	–
<g/>
1419	[number]	k4	1419
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
padovské	padovský	k2eAgFnSc2d1	Padovská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
o	o	k7c6	o
kom	kdo	k3yQnSc6	kdo
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
právo	právo	k1gNnSc4	právo
muže	muž	k1gMnSc2	muž
na	na	k7c4	na
cvičení	cvičení	k1gNnSc4	cvičení
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Vittorino	Vittorino	k1gNnSc1	Vittorino
Ramboldini	Ramboldin	k2eAgMnPc1d1	Ramboldin
de	de	k?	de
Feltre	Feltr	k1gInSc5	Feltr
(	(	kIx(	(
<g/>
1378	[number]	k4	1378
<g/>
–	–	k?	–
<g/>
1446	[number]	k4	1446
<g/>
)	)	kIx)	)
šel	jít	k5eAaImAgMnS	jít
ještě	ještě	k6eAd1	ještě
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
Mantově	Mantova	k1gFnSc6	Mantova
založil	založit	k5eAaPmAgMnS	založit
tělocvičnou	tělocvičný	k2eAgFnSc4d1	Tělocvičná
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
novodobá	novodobý	k2eAgFnSc1d1	novodobá
práce	práce	k1gFnSc1	práce
o	o	k7c6	o
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
vyšla	vyjít	k5eAaPmAgFnS	vyjít
roku	rok	k1gInSc2	rok
1430	[number]	k4	1430
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
dílem	dílo	k1gNnSc7	dílo
florentského	florentský	k2eAgMnSc2d1	florentský
básníka	básník	k1gMnSc2	básník
Matea	Mateus	k1gMnSc2	Mateus
Palmieriho	Palmieri	k1gMnSc2	Palmieri
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1491	[number]	k4	1491
vydal	vydat	k5eAaPmAgMnS	vydat
Virgilius	Virgilius	k1gMnSc1	Virgilius
Polydorus	Polydorus	k1gMnSc1	Polydorus
knihu	kniha	k1gFnSc4	kniha
O	o	k7c6	o
posvátných	posvátný	k2eAgFnPc6d1	posvátná
hrách	hra	k1gFnPc6	hra
starých	starý	k2eAgMnPc2d1	starý
Řeků	Řek	k1gMnPc2	Řek
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1569	[number]	k4	1569
vychází	vycházet	k5eAaImIp3nS	vycházet
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
kniha	kniha	k1gFnSc1	kniha
významného	významný	k2eAgMnSc2d1	významný
humanisty	humanista	k1gMnSc2	humanista
Hieronyma	Hieronymus	k1gMnSc2	Hieronymus
Mercuriala	Mercurial	k1gMnSc2	Mercurial
O	o	k7c6	o
gymnastickém	gymnastický	k2eAgNnSc6d1	gymnastické
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
si	se	k3xPyFc3	se
tyto	tento	k3xDgFnPc4	tento
činnosti	činnost	k1gFnPc4	činnost
nekladly	klást	k5eNaImAgInP	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
znovuobnovení	znovuobnovení	k1gNnSc2	znovuobnovení
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
stupínkem	stupínek	k1gInSc7	stupínek
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
oživení	oživení	k1gNnSc3	oživení
tradice	tradice	k1gFnSc2	tradice
pěstování	pěstování	k1gNnSc2	pěstování
kultury	kultura	k1gFnSc2	kultura
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
novodobé	novodobý	k2eAgFnPc4d1	novodobá
sportovní	sportovní	k2eAgFnPc4d1	sportovní
hry	hra	k1gFnPc4	hra
nesoucí	nesoucí	k2eAgNnSc4d1	nesoucí
označení	označení	k1gNnSc3	označení
olympijské	olympijský	k2eAgFnPc1d1	olympijská
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
Anglické	anglický	k2eAgFnPc1d1	anglická
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
venkovském	venkovský	k2eAgNnSc6d1	venkovské
sídle	sídlo	k1gNnSc6	sídlo
v	v	k7c6	v
Cotswoldu	Cotswold	k1gInSc6	Cotswold
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
roku	rok	k1gInSc2	rok
1604	[number]	k4	1604
Robert	Robert	k1gMnSc1	Robert
Dover	Dover	k1gMnSc1	Dover
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc4	hra
podporoval	podporovat	k5eAaImAgMnS	podporovat
i	i	k9	i
velký	velký	k2eAgMnSc1d1	velký
příznivec	příznivec	k1gMnSc1	příznivec
a	a	k8xC	a
propagátor	propagátor	k1gMnSc1	propagátor
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Jakub	Jakub	k1gMnSc1	Jakub
I.	I.	kA	I.
Hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
konat	konat	k5eAaImF	konat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
za	za	k7c2	za
letního	letní	k2eAgInSc2d1	letní
slunovratu	slunovrat	k1gInSc2	slunovrat
<g/>
,	,	kIx,	,
na	na	k7c6	na
programu	program	k1gInSc6	program
byly	být	k5eAaImAgFnP	být
běžecké	běžecký	k2eAgFnPc1d1	běžecká
disciplíny	disciplína	k1gFnPc1	disciplína
<g/>
,	,	kIx,	,
hod	hod	k1gInSc1	hod
kladivem	kladivo	k1gNnSc7	kladivo
<g/>
,	,	kIx,	,
míčové	míčový	k2eAgFnPc1d1	Míčová
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
šerm	šerm	k1gInSc1	šerm
holemi	hole	k1gFnPc7	hole
a	a	k8xC	a
zápas	zápas	k1gInSc1	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Mohli	moct	k5eAaImAgMnP	moct
se	se	k3xPyFc4	se
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
jak	jak	k6eAd1	jak
šlechtici	šlechtic	k1gMnPc1	šlechtic
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
prostí	prostý	k2eAgMnPc1d1	prostý
občané	občan	k1gMnPc1	občan
<g/>
,	,	kIx,	,
některých	některý	k3yIgFnPc2	některý
disciplín	disciplína	k1gFnPc2	disciplína
dokonce	dokonce	k9	dokonce
i	i	k9	i
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
dočkaly	dočkat	k5eAaPmAgFnP	dočkat
40	[number]	k4	40
ročníků	ročník	k1gInPc2	ročník
a	a	k8xC	a
skončily	skončit	k5eAaPmAgFnP	skončit
nejpozději	pozdě	k6eAd3	pozdě
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
svého	svůj	k3xOyFgMnSc2	svůj
zakladatele	zakladatel	k1gMnSc2	zakladatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
obnovení	obnovení	k1gNnSc2	obnovení
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1760	[number]	k4	1760
německý	německý	k2eAgMnSc1d1	německý
pedagogický	pedagogický	k2eAgMnSc1d1	pedagogický
reformátor	reformátor	k1gMnSc1	reformátor
Johann	Johann	k1gMnSc1	Johann
Bernhard	Bernhard	k1gMnSc1	Bernhard
Basedow	Basedow	k1gMnSc1	Basedow
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
velkým	velký	k2eAgMnSc7d1	velký
propagátorem	propagátor	k1gMnSc7	propagátor
vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
byl	být	k5eAaImAgMnS	být
Johann	Johann	k1gMnSc1	Johann
Christoph	Christoph	k1gMnSc1	Christoph
Friedrich	Friedrich	k1gMnSc1	Friedrich
Gutsmuths	Gutsmuths	k1gInSc4	Gutsmuths
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInSc7	jeho
vlivem	vliv	k1gInSc7	vliv
uspořádaly	uspořádat	k5eAaPmAgFnP	uspořádat
některé	některý	k3yIgFnPc1	některý
tělovýchovné	tělovýchovný	k2eAgFnPc1d1	Tělovýchovná
organizace	organizace	k1gFnPc1	organizace
v	v	k7c6	v
několika	několik	k4yIc6	několik
německých	německý	k2eAgNnPc6d1	německé
městech	město	k1gNnPc6	město
sportovní	sportovní	k2eAgInSc1d1	sportovní
klání	klání	k1gNnSc2	klání
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1859	[number]	k4	1859
se	se	k3xPyFc4	se
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
20	[number]	k4	20
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
stalo	stát	k5eAaPmAgNnS	stát
svědky	svědek	k1gMnPc4	svědek
sportovních	sportovní	k2eAgFnPc2d1	sportovní
klání	klání	k1gNnSc4	klání
300	[number]	k4	300
soutěžících	soutěžící	k1gMnPc2	soutěžící
na	na	k7c6	na
prvních	první	k4xOgFnPc6	první
novořeckých	novořecký	k2eAgFnPc6d1	novořecká
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
uspořádání	uspořádání	k1gNnSc6	uspořádání
měl	mít	k5eAaImAgMnS	mít
hrdina	hrdina	k1gMnSc1	hrdina
osvobozovacích	osvobozovací	k2eAgInPc2d1	osvobozovací
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
Evangelos	Evangelos	k1gMnSc1	Evangelos
Zappas	Zappas	k1gMnSc1	Zappas
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jejich	jejich	k3xOp3gFnSc4	jejich
organizaci	organizace	k1gFnSc4	organizace
věnoval	věnovat	k5eAaImAgInS	věnovat
nejen	nejen	k6eAd1	nejen
značné	značný	k2eAgNnSc4d1	značné
úsilí	úsilí	k1gNnSc4	úsilí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
uspořádat	uspořádat	k5eAaPmF	uspořádat
novořecké	novořecký	k2eAgFnSc2d1	novořecká
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
zrodila	zrodit	k5eAaPmAgFnS	zrodit
v	v	k7c6	v
Pyrgu	Pyrg	k1gInSc6	Pyrg
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
Řecka	Řecko	k1gNnSc2	Řecko
z	z	k7c2	z
turecké	turecký	k2eAgFnSc2d1	turecká
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
místní	místní	k2eAgFnSc1d1	místní
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pro	pro	k7c4	pro
obnovení	obnovení	k1gNnSc4	obnovení
her	hra	k1gFnPc2	hra
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
<g/>
.	.	kIx.	.
</s>
<s>
Měly	mít	k5eAaImAgFnP	mít
se	se	k3xPyFc4	se
konat	konat	k5eAaImF	konat
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
na	na	k7c4	na
Den	den	k1gInSc4	den
řecké	řecký	k2eAgFnSc2d1	řecká
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Nepříznivé	příznivý	k2eNgFnPc1d1	nepříznivá
podmínky	podmínka	k1gFnPc1	podmínka
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
(	(	kIx(	(
<g/>
bažinaté	bažinatý	k2eAgNnSc1d1	bažinaté
území	území	k1gNnSc1	území
plné	plný	k2eAgFnSc2d1	plná
komáru	komár	k1gMnSc3	komár
představovalo	představovat	k5eAaImAgNnS	představovat
velkou	velký	k2eAgFnSc4d1	velká
hrozbu	hrozba	k1gFnSc4	hrozba
malárie	malárie	k1gFnSc2	malárie
<g/>
)	)	kIx)	)
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
hry	hra	k1gFnPc1	hra
konaly	konat	k5eAaImAgFnP	konat
až	až	k6eAd1	až
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
a	a	k8xC	a
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
konaly	konat	k5eAaImAgFnP	konat
ještě	ještě	k9	ještě
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
75	[number]	k4	75
a	a	k8xC	a
89	[number]	k4	89
<g/>
.	.	kIx.	.
</s>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
de	de	k?	de
Coubertin	Coubertin	k1gInSc1	Coubertin
se	se	k3xPyFc4	se
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
věnoval	věnovat	k5eAaPmAgMnS	věnovat
studiu	studio	k1gNnSc3	studio
tělovýchovných	tělovýchovný	k2eAgMnPc2d1	tělovýchovný
systémů	systém	k1gInPc2	systém
v	v	k7c6	v
anglosaském	anglosaský	k2eAgInSc6d1	anglosaský
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
poznatky	poznatek	k1gInPc4	poznatek
zpracoval	zpracovat	k5eAaPmAgInS	zpracovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
knihách	kniha	k1gFnPc6	kniha
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
začlenění	začlenění	k1gNnSc4	začlenění
do	do	k7c2	do
francouzského	francouzský	k2eAgInSc2d1	francouzský
výchovného	výchovný	k2eAgInSc2d1	výchovný
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc4	jeho
literární	literární	k2eAgFnSc4d1	literární
činnost	činnost	k1gFnSc4	činnost
nevzbudila	vzbudit	k5eNaPmAgFnS	vzbudit
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
zájem	zájem	k1gInSc4	zájem
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
přejít	přejít	k5eAaPmF	přejít
k	k	k7c3	k
činům	čin	k1gInPc3	čin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
obnoví	obnovit	k5eAaPmIp3nP	obnovit
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
začal	začít	k5eAaPmAgInS	začít
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
záměr	záměr	k1gInSc4	záměr
shánět	shánět	k5eAaImF	shánět
podporu	podpora	k1gFnSc4	podpora
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
svých	svůj	k3xOyFgMnPc2	svůj
známých	známý	k1gMnPc2	známý
a	a	k8xC	a
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1892	[number]	k4	1892
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
myšlenkou	myšlenka	k1gFnSc7	myšlenka
veřejně	veřejně	k6eAd1	veřejně
na	na	k7c6	na
schůzi	schůze	k1gFnSc6	schůze
k	k	k7c3	k
5	[number]	k4	5
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
založení	založení	k1gNnSc2	založení
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
unie	unie	k1gFnSc2	unie
atletických	atletický	k2eAgInPc2d1	atletický
sportů	sport	k1gInPc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
první	první	k4xOgFnSc2	první
novodobé	novodobý	k2eAgFnSc2d1	novodobá
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Zformuloval	zformulovat	k5eAaPmAgMnS	zformulovat
ji	on	k3xPp3gFnSc4	on
Pierre	Pierr	k1gInSc5	Pierr
de	de	k?	de
Coubertin	Coubertin	k2eAgMnSc1d1	Coubertin
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Antický	antický	k2eAgInSc4d1	antický
ideál	ideál	k1gInSc4	ideál
tělesné	tělesný	k2eAgFnSc2d1	tělesná
krásy	krása	k1gFnSc2	krása
a	a	k8xC	a
duchovní	duchovní	k2eAgFnSc2d1	duchovní
dokonalosti	dokonalost	k1gFnSc2	dokonalost
–	–	k?	–
tzv.	tzv.	kA	tzv.
kalokagathía	kalokagathí	k2eAgMnSc4d1	kalokagathí
–	–	k?	–
posvátný	posvátný	k2eAgInSc4d1	posvátný
mír	mír	k1gInSc4	mír
(	(	kIx(	(
<g/>
ekecheiría	ekecheiría	k6eAd1	ekecheiría
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slavnostní	slavnostní	k2eAgInPc1d1	slavnostní
obřady	obřad	k1gInPc1	obřad
při	při	k7c6	při
zahájení	zahájení	k1gNnSc6	zahájení
antických	antický	k2eAgFnPc2d1	antická
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
ožívají	ožívat	k5eAaImIp3nP	ožívat
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
formě	forma	k1gFnSc6	forma
při	při	k7c6	při
dnešních	dnešní	k2eAgFnPc6d1	dnešní
OH	OH	kA	OH
obohaceny	obohatit	k5eAaPmNgInP	obohatit
o	o	k7c4	o
požadavek	požadavek	k1gInSc4	požadavek
rovnoprávnosti	rovnoprávnost	k1gFnSc2	rovnoprávnost
všech	všecek	k3xTgMnPc2	všecek
sportovců	sportovec	k1gMnPc2	sportovec
bez	bez	k7c2	bez
rasové	rasový	k2eAgFnSc2d1	rasová
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnSc2d1	politická
a	a	k8xC	a
náboženské	náboženský	k2eAgFnSc2d1	náboženská
diskriminace	diskriminace	k1gFnSc2	diskriminace
<g/>
,	,	kIx,	,
o	o	k7c4	o
požadavek	požadavek	k1gInSc4	požadavek
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
internacionalismu	internacionalismus	k1gInSc2	internacionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
olympijská	olympijský	k2eAgFnSc1d1	olympijská
myšlenka	myšlenka	k1gFnSc1	myšlenka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vyjádřena	vyjádřen	k2eAgFnSc1d1	vyjádřena
v	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
článku	článek	k1gInSc6	článek
Olympijské	olympijský	k2eAgFnSc2d1	olympijská
charty	charta	k1gFnSc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
Antické	antický	k2eAgFnPc1d1	antická
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
avšak	avšak	k8xC	avšak
byly	být	k5eAaImAgFnP	být
méně	málo	k6eAd2	málo
idealistické	idealistický	k2eAgFnPc1d1	idealistická
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Olympijský	olympijský	k2eAgInSc4d1	olympijský
slib	slib	k1gInSc4	slib
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgInSc4d1	slavnostní
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
skládají	skládat	k5eAaImIp3nP	skládat
<g/>
:	:	kIx,	:
sportovci	sportovec	k1gMnPc1	sportovec
při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zahájení	zahájení	k1gNnSc6	zahájení
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zahájení	zahájení	k1gNnSc6	zahájení
trenéři	trenér	k1gMnPc1	trenér
při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zahájení	zahájení	k1gNnSc6	zahájení
sportovec	sportovec	k1gMnSc1	sportovec
při	při	k7c6	při
podpisu	podpis	k1gInSc6	podpis
přihlášky	přihláška	k1gFnSc2	přihláška
nový	nový	k2eAgMnSc1d1	nový
člen	člen	k1gMnSc1	člen
MOV	MOV	kA	MOV
Olympijské	olympijský	k2eAgInPc4d1	olympijský
symboly	symbol	k1gInPc4	symbol
a	a	k8xC	a
znaky	znak	k1gInPc4	znak
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
olympijské	olympijský	k2eAgFnPc4d1	olympijská
myšlenky	myšlenka	k1gFnPc4	myšlenka
a	a	k8xC	a
univerzálnost	univerzálnost	k1gFnSc4	univerzálnost
olympijského	olympijský	k2eAgNnSc2d1	Olympijské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
úkoly	úkol	k1gInPc4	úkol
a	a	k8xC	a
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
přesnou	přesný	k2eAgFnSc4d1	přesná
podobu	podoba	k1gFnSc4	podoba
i	i	k9	i
způsoby	způsob	k1gInPc4	způsob
a	a	k8xC	a
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
používání	používání	k1gNnSc4	používání
přesně	přesně	k6eAd1	přesně
definuje	definovat	k5eAaBmIp3nS	definovat
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
charta	charta	k1gFnSc1	charta
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
olympijské	olympijský	k2eAgInPc4d1	olympijský
symboly	symbol	k1gInPc4	symbol
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
symboly	symbol	k1gInPc4	symbol
olympijského	olympijský	k2eAgNnSc2d1	Olympijské
hnutí	hnutí	k1gNnSc2	hnutí
(	(	kIx(	(
<g/>
olympijský	olympijský	k2eAgInSc1d1	olympijský
symbol	symbol	k1gInSc1	symbol
<g/>
,	,	kIx,	,
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
<g/>
,	,	kIx,	,
hymna	hymna	k1gFnSc1	hymna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
emblémy	emblém	k1gInPc1	emblém
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc1	znak
národních	národní	k2eAgInPc2d1	národní
olympijských	olympijský	k2eAgInPc2d1	olympijský
výborů	výbor	k1gInPc2	výbor
(	(	kIx(	(
<g/>
NOV	nov	k1gInSc1	nov
<g/>
)	)	kIx)	)
a	a	k8xC	a
organizačních	organizační	k2eAgInPc2d1	organizační
výborů	výbor	k1gInPc2	výbor
OH	OH	kA	OH
(	(	kIx(	(
<g/>
ZOH	ZOH	kA	ZOH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maskoti	maskot	k1gMnPc1	maskot
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
olympijským	olympijský	k2eAgInSc7d1	olympijský
symbolem	symbol	k1gInSc7	symbol
jsou	být	k5eAaImIp3nP	být
olympijské	olympijský	k2eAgInPc1d1	olympijský
kruhy	kruh	k1gInPc1	kruh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
propojeny	propojit	k5eAaPmNgFnP	propojit
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
kruhů	kruh	k1gInPc2	kruh
představuje	představovat	k5eAaImIp3nS	představovat
pět	pět	k4xCc4	pět
kontinentů	kontinent	k1gInPc2	kontinent
spojených	spojený	k2eAgFnPc2d1	spojená
olympijskou	olympijský	k2eAgFnSc7d1	olympijská
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
šest	šest	k4xCc1	šest
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
bílého	bílý	k2eAgInSc2d1	bílý
podkladu	podklad	k1gInSc2	podklad
<g/>
)	)	kIx)	)
barvy	barva	k1gFnPc1	barva
všech	všecek	k3xTgInPc2	všecek
národů	národ	k1gInPc2	národ
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
obecný	obecný	k2eAgInSc4d1	obecný
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
kruhy	kruh	k1gInPc1	kruh
představují	představovat	k5eAaImIp3nP	představovat
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
kontinenty	kontinent	k1gInPc1	kontinent
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
<g/>
.	.	kIx.	.
</s>
<s>
Baron	baron	k1gMnSc1	baron
de	de	k?	de
Coubertin	Coubertin	k1gInSc1	Coubertin
vnímal	vnímat	k5eAaImAgInS	vnímat
kruhy	kruh	k1gInPc4	kruh
a	a	k8xC	a
barvy	barva	k1gFnPc4	barva
jako	jako	k8xC	jako
dva	dva	k4xCgInPc4	dva
nezávislé	závislý	k2eNgInPc4d1	nezávislý
symboly	symbol	k1gInPc4	symbol
a	a	k8xC	a
MOV	MOV	kA	MOV
výslovně	výslovně	k6eAd1	výslovně
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc1	žádný
z	z	k7c2	z
kruhů	kruh	k1gInPc2	kruh
nereprezentuje	reprezentovat	k5eNaImIp3nS	reprezentovat
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
kontinent	kontinent	k1gInSc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Olympijské	olympijský	k2eAgInPc1d1	olympijský
kruhy	kruh	k1gInPc1	kruh
jsou	být	k5eAaImIp3nP	být
součásti	součást	k1gFnPc4	součást
olympijské	olympijský	k2eAgFnSc2d1	olympijská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
bílý	bílý	k2eAgInSc4d1	bílý
podklad	podklad	k1gInSc4	podklad
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
pět	pět	k4xCc4	pět
olympijských	olympijský	k2eAgInPc2d1	olympijský
kruhů	kruh	k1gInPc2	kruh
<g/>
:	:	kIx,	:
modrý	modrý	k2eAgInSc1d1	modrý
kruh	kruh	k1gInSc1	kruh
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vlevo	vlevo	k6eAd1	vlevo
nahoře	nahoře	k6eAd1	nahoře
<g/>
,	,	kIx,	,
nejblíže	blízce	k6eAd3	blízce
žerdi	žerď	k1gFnSc2	žerď
černý	černý	k2eAgInSc1d1	černý
kruh	kruh	k1gInSc1	kruh
umístěn	umístit	k5eAaPmNgInS	umístit
vedle	vedle	k7c2	vedle
modrého	modré	k1gNnSc2	modré
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
vlajky	vlajka	k1gFnSc2	vlajka
červený	červený	k2eAgInSc4d1	červený
kruh	kruh	k1gInSc4	kruh
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
vedle	vedle	k7c2	vedle
černého	černé	k1gNnSc2	černé
žlutý	žlutý	k2eAgInSc1d1	žlutý
kruh	kruh	k1gInSc1	kruh
propojuje	propojovat	k5eAaImIp3nS	propojovat
modrý	modrý	k2eAgInSc1d1	modrý
a	a	k8xC	a
černý	černý	k2eAgInSc1d1	černý
kruh	kruh	k1gInSc1	kruh
zelený	zelený	k2eAgInSc1d1	zelený
kruh	kruh	k1gInSc1	kruh
propojuje	propojovat	k5eAaImIp3nS	propojovat
černý	černý	k2eAgInSc1d1	černý
a	a	k8xC	a
červený	červený	k2eAgInSc1d1	červený
kruh	kruh	k1gInSc1	kruh
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
poprvé	poprvé	k6eAd1	poprvé
zavlála	zavlát	k5eAaPmAgFnS	zavlát
při	při	k7c6	při
VI	VI	kA	VI
<g/>
.	.	kIx.	.
olympijském	olympijský	k2eAgInSc6d1	olympijský
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
na	na	k7c6	na
olympijském	olympijský	k2eAgInSc6d1	olympijský
stadiónu	stadión	k1gInSc6	stadión
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
OH	OH	kA	OH
se	se	k3xPyFc4	se
vyvěšuje	vyvěšovat	k5eAaImIp3nS	vyvěšovat
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
věnovaly	věnovat	k5eAaPmAgFnP	věnovat
Antverpy	Antverpy	k1gFnPc1	Antverpy
<g/>
,	,	kIx,	,
na	na	k7c6	na
ZOH	ZOH	kA	ZOH
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
věnovalo	věnovat	k5eAaPmAgNnS	věnovat
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
historické	historický	k2eAgFnSc2d1	historická
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
i	i	k9	i
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
heslo	heslo	k1gNnSc4	heslo
Citius	Citius	k1gMnSc1	Citius
<g/>
,	,	kIx,	,
Altius	Altius	k1gMnSc1	Altius
<g/>
,	,	kIx,	,
Fortius	Fortius	k1gMnSc1	Fortius
(	(	kIx(	(
<g/>
Rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
silněji	silně	k6eAd2	silně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
cíl	cíl	k1gInSc1	cíl
olympijského	olympijský	k2eAgNnSc2d1	Olympijské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
úsilí	úsilí	k1gNnSc2	úsilí
o	o	k7c4	o
neustálý	neustálý	k2eAgInSc4d1	neustálý
pokrok	pokrok	k1gInSc4	pokrok
<g/>
.	.	kIx.	.
</s>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
de	de	k?	de
Coubertin	Coubertin	k2eAgMnSc1d1	Coubertin
toto	tento	k3xDgNnSc4	tento
heslo	heslo	k1gNnSc4	heslo
převzal	převzít	k5eAaPmAgInS	převzít
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
<g/>
,	,	kIx,	,
abbé	abbé	k1gMnSc2	abbé
Didona	Didon	k1gMnSc2	Didon
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
symboly	symbol	k1gInPc4	symbol
olympijského	olympijský	k2eAgNnSc2d1	Olympijské
hnutí	hnutí	k1gNnSc2	hnutí
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
6	[number]	k4	6
olympijské	olympijský	k2eAgFnSc2d1	olympijská
charty	charta	k1gFnSc2	charta
výlučným	výlučný	k2eAgNnSc7d1	výlučné
vlastnictvím	vlastnictví	k1gNnSc7	vlastnictví
MOV	MOV	kA	MOV
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
oficiálního	oficiální	k2eAgInSc2d1	oficiální
olympijského	olympijský	k2eAgInSc2d1	olympijský
dne	den	k1gInSc2	den
nesmí	smět	k5eNaImIp3nS	smět
NOV	nov	k1gInSc4	nov
užívat	užívat	k5eAaImF	užívat
olympijské	olympijský	k2eAgInPc4d1	olympijský
symboly	symbol	k1gInPc4	symbol
bez	bez	k7c2	bez
jeho	on	k3xPp3gNnSc2	on
výslovného	výslovný	k2eAgNnSc2d1	výslovné
povolení	povolení	k1gNnSc2	povolení
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
symboly	symbol	k1gInPc4	symbol
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
použít	použít	k5eAaPmF	použít
ke	k	k7c3	k
komerčním	komerční	k2eAgInPc3d1	komerční
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Kombinací	kombinace	k1gFnSc7	kombinace
olympijských	olympijský	k2eAgInPc2d1	olympijský
kruhů	kruh	k1gInPc2	kruh
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
znaky	znak	k1gInPc7	znak
nebo	nebo	k8xC	nebo
atributy	atribut	k1gInPc7	atribut
(	(	kIx(	(
<g/>
např.	např.	kA	např.
se	s	k7c7	s
státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
)	)	kIx)	)
vznikají	vznikat	k5eAaImIp3nP	vznikat
emblémy	emblém	k1gInPc4	emblém
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
NOV	nova	k1gFnPc2	nova
a	a	k8xC	a
organizačních	organizační	k2eAgInPc2d1	organizační
výborů	výbor	k1gInPc2	výbor
OH	OH	kA	OH
(	(	kIx(	(
<g/>
ZOH	ZOH	kA	ZOH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
jsou	být	k5eAaImIp3nP	být
vlastnictvím	vlastnictví	k1gNnSc7	vlastnictví
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
však	však	k9	však
být	být	k5eAaImF	být
schváleny	schválit	k5eAaPmNgInP	schválit
MOV	MOV	kA	MOV
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
používaným	používaný	k2eAgInSc7d1	používaný
symbolem	symbol	k1gInSc7	symbol
OH	OH	kA	OH
(	(	kIx(	(
<g/>
ZOH	ZOH	kA	ZOH
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
maskoty	maskot	k1gInPc4	maskot
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
národní	národní	k2eAgFnSc4d1	národní
tradici	tradice	k1gFnSc4	tradice
hostitelské	hostitelský	k2eAgFnSc2d1	hostitelská
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Olympijský	olympijský	k2eAgInSc1d1	olympijský
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
předepsaných	předepsaný	k2eAgInPc2d1	předepsaný
obřadů	obřad	k1gInPc2	obřad
pro	pro	k7c4	pro
zahájení	zahájení	k1gNnSc4	zahájení
a	a	k8xC	a
ukončení	ukončení	k1gNnSc4	ukončení
OH	OH	kA	OH
a	a	k8xC	a
pro	pro	k7c4	pro
rozdílení	rozdílení	k1gNnSc4	rozdílení
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
konající	konající	k2eAgFnSc7d1	konající
se	se	k3xPyFc4	se
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
olympijském	olympijský	k2eAgInSc6d1	olympijský
stadionu	stadion	k1gInSc6	stadion
<g/>
.	.	kIx.	.
začíná	začínat	k5eAaImIp3nS	začínat
příchodem	příchod	k1gInSc7	příchod
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
země	zem	k1gFnSc2	zem
pořádající	pořádající	k2eAgFnSc2d1	pořádající
OH	OH	kA	OH
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
předsedy	předseda	k1gMnSc2	předseda
MOV	MOV	kA	MOV
a	a	k8xC	a
předsedy	předseda	k1gMnSc2	předseda
organizačního	organizační	k2eAgInSc2d1	organizační
výboru	výbor	k1gInSc2	výbor
her	hra	k1gFnPc2	hra
na	na	k7c4	na
olympijský	olympijský	k2eAgInSc4d1	olympijský
stadion	stadion	k1gInSc4	stadion
<g/>
.	.	kIx.	.
zazní	zaznít	k5eAaPmIp3nS	zaznít
státní	státní	k2eAgFnSc1d1	státní
hymna	hymna	k1gFnSc1	hymna
pořádající	pořádající	k2eAgFnSc2d1	pořádající
země	zem	k1gFnSc2	zem
defilé	defilé	k1gNnSc1	defilé
účastníků	účastník	k1gMnPc2	účastník
–	–	k?	–
každá	každý	k3xTgFnSc1	každý
delegace	delegace	k1gFnSc1	delegace
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
standartou	standarta	k1gFnSc7	standarta
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
země	zem	k1gFnSc2	zem
a	a	k8xC	a
státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Defilé	defilé	k1gNnSc4	defilé
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
delegace	delegace	k1gFnSc1	delegace
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
je	on	k3xPp3gFnPc4	on
delegace	delegace	k1gFnPc4	delegace
hostitelské	hostitelský	k2eAgFnSc2d1	hostitelská
země	zem	k1gFnSc2	zem
<g/>
;	;	kIx,	;
ostatní	ostatní	k2eAgInPc1d1	ostatní
národy	národ	k1gInPc1	národ
defilují	defilovat	k5eAaImIp3nP	defilovat
v	v	k7c6	v
abecedním	abecední	k2eAgInSc6d1	abecední
pořádku	pořádek	k1gInSc6	pořádek
podle	podle	k7c2	podle
jazyka	jazyk	k1gInSc2	jazyk
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hry	hra	k1gFnPc4	hra
pořádá	pořádat	k5eAaImIp3nS	pořádat
<g/>
.	.	kIx.	.
předseda	předseda	k1gMnSc1	předseda
MOV	MOV	kA	MOV
přednese	přednést	k5eAaPmIp3nS	přednést
uvítací	uvítací	k2eAgInSc4d1	uvítací
projev	projev	k1gInSc4	projev
a	a	k8xC	a
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
hlavu	hlava	k1gFnSc4	hlava
pořádajícího	pořádající	k2eAgInSc2d1	pořádající
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pronesl	pronést	k5eAaPmAgMnS	pronést
zahajovací	zahajovací	k2eAgFnSc4d1	zahajovací
formuli	formule	k1gFnSc4	formule
<g/>
.	.	kIx.	.
ozve	ozvat	k5eAaPmIp3nS	ozvat
se	se	k3xPyFc4	se
hlas	hlas	k1gInSc1	hlas
trubek	trubka	k1gFnPc2	trubka
a	a	k8xC	a
za	za	k7c2	za
zvuků	zvuk	k1gInPc2	zvuk
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hymny	hymna	k1gFnSc2	hymna
je	být	k5eAaImIp3nS	být
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
předání	předání	k1gNnSc1	předání
oficiální	oficiální	k2eAgNnSc1d1	oficiální
<g/>
,	,	kIx,	,
olympijské	olympijský	k2eAgFnPc1d1	olympijská
vlajky	vlajka	k1gFnPc1	vlajka
starostovi	starosta	k1gMnSc3	starosta
pořadatelského	pořadatelský	k2eAgNnSc2d1	pořadatelské
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
symbolické	symbolický	k2eAgNnSc1d1	symbolické
vypuštění	vypuštění	k1gNnSc1	vypuštění
holubů	holub	k1gMnPc2	holub
(	(	kIx(	(
<g/>
poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
již	již	k6eAd1	již
neprovádí	provádět	k5eNaImIp3nS	provádět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
příchod	příchod	k1gInSc1	příchod
olympijské	olympijský	k2eAgFnSc2d1	olympijská
štafety	štafeta	k1gFnSc2	štafeta
s	s	k7c7	s
pochodní	pochodeň	k1gFnSc7	pochodeň
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
běžec	běžec	k1gMnSc1	běžec
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
olympijský	olympijský	k2eAgInSc4d1	olympijský
stadion	stadion	k1gInSc4	stadion
a	a	k8xC	a
zažehne	zažehnout	k5eAaPmIp3nS	zažehnout
olympijský	olympijský	k2eAgInSc1d1	olympijský
oheň	oheň	k1gInSc1	oheň
<g/>
.	.	kIx.	.
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
přísaha	přísaha	k1gFnSc1	přísaha
sportovců	sportovec	k1gMnPc2	sportovec
a	a	k8xC	a
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
přísaha	přísaha	k1gFnSc1	přísaha
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
olympijský	olympijský	k2eAgInSc4d1	olympijský
slib	slib	k1gInSc4	slib
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
zahajovacího	zahajovací	k2eAgInSc2d1	zahajovací
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
je	být	k5eAaImIp3nS	být
hrána	hrát	k5eAaImNgFnS	hrát
státní	státní	k2eAgFnSc1d1	státní
hymna	hymna	k1gFnSc1	hymna
hostitelské	hostitelský	k2eAgFnSc2d1	hostitelská
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkonoši	vlajkonoš	k1gMnPc1	vlajkonoš
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
delegací	delegace	k1gFnPc2	delegace
nastoupí	nastoupit	k5eAaPmIp3nP	nastoupit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nosiči	nosič	k1gMnPc7	nosič
standart	standarta	k1gFnPc2	standarta
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
země	zem	k1gFnSc2	zem
na	na	k7c4	na
plochu	plocha	k1gFnSc4	plocha
stadiónu	stadión	k1gInSc2	stadión
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
pořadí	pořadí	k1gNnSc6	pořadí
a	a	k8xC	a
zaujmou	zaujmout	k5eAaPmIp3nP	zaujmout
stejná	stejný	k2eAgNnPc4d1	stejné
místa	místo	k1gNnPc4	místo
jako	jako	k8xS	jako
při	při	k7c6	při
zahájení	zahájení	k1gNnSc6	zahájení
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
nimi	on	k3xPp3gInPc7	on
defiluje	defilovat	k5eAaImIp3nS	defilovat
šest	šest	k4xCc4	šest
závodníků	závodník	k1gMnPc2	závodník
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
delegace	delegace	k1gFnSc2	delegace
v	v	k7c6	v
osmi-	osmi-	k?	osmi-
nebo	nebo	k8xC	nebo
desetistupech	desetistup	k1gInPc6	desetistup
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
státní	státní	k2eAgFnSc2d1	státní
příslušnosti	příslušnost	k1gFnSc2	příslušnost
<g/>
.	.	kIx.	.
zazní	zaznět	k5eAaImIp3nS	zaznět
řecká	řecký	k2eAgFnSc1d1	řecká
státní	státní	k2eAgFnSc1d1	státní
hymna	hymna	k1gFnSc1	hymna
<g/>
.	.	kIx.	.
na	na	k7c4	na
stožár	stožár	k1gInSc4	stožár
stojící	stojící	k2eAgFnSc4d1	stojící
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
středového	středový	k2eAgInSc2d1	středový
stožáru	stožár	k1gInSc2	stožár
(	(	kIx(	(
<g/>
užívaného	užívaný	k2eAgInSc2d1	užívaný
pro	pro	k7c4	pro
vlajky	vlajka	k1gFnPc1	vlajka
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
)	)	kIx)	)
vystoupá	vystoupat	k5eAaPmIp3nS	vystoupat
řecká	řecký	k2eAgFnSc1d1	řecká
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
;	;	kIx,	;
pak	pak	k6eAd1	pak
vystoupá	vystoupat	k5eAaPmIp3nS	vystoupat
na	na	k7c4	na
středový	středový	k2eAgInSc4d1	středový
stožár	stožár	k1gInSc4	stožár
<g />
.	.	kIx.	.
</s>
<s>
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
pořadatelské	pořadatelský	k2eAgFnSc2d1	pořadatelská
země	zem	k1gFnSc2	zem
a	a	k8xC	a
na	na	k7c4	na
stožár	stožár	k1gInSc4	stožár
vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
středového	středový	k2eAgInSc2d1	středový
vlajka	vlajka	k1gFnSc1	vlajka
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bude	být	k5eAaImBp3nS	být
pořádat	pořádat	k5eAaImF	pořádat
příští	příští	k2eAgFnPc4d1	příští
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
zazní	zaznět	k5eAaImIp3nS	zaznět
státní	státní	k2eAgFnSc1d1	státní
hymna	hymna	k1gFnSc1	hymna
země	zem	k1gFnSc2	zem
příštích	příští	k2eAgFnPc2d1	příští
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
předseda	předseda	k1gMnSc1	předseda
MOV	MOV	kA	MOV
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
a	a	k8xC	a
krátkým	krátký	k2eAgInSc7d1	krátký
projevem	projev	k1gInSc7	projev
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
poděkuje	poděkovat	k5eAaPmIp3nS	poděkovat
pořadatelské	pořadatelský	k2eAgFnSc3d1	pořadatelská
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
pozve	pozvat	k5eAaPmIp3nS	pozvat
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
příštích	příští	k2eAgFnPc6d1	příští
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
hry	hra	k1gFnPc1	hra
pak	pak	k6eAd1	pak
ukončí	ukončit	k5eAaPmIp3nP	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Zazní	zaznít	k5eAaPmIp3nS	zaznít
fanfára	fanfára	k1gFnSc1	fanfára
<g/>
.	.	kIx.	.
zhasne	zhasnout	k5eAaPmIp3nS	zhasnout
olympijský	olympijský	k2eAgInSc1d1	olympijský
oheň	oheň	k1gInSc1	oheň
<g/>
.	.	kIx.	.
za	za	k7c2	za
zvuků	zvuk	k1gInPc2	zvuk
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hymny	hymna	k1gFnSc2	hymna
je	být	k5eAaImIp3nS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
ze	z	k7c2	z
stožáru	stožár	k1gInSc2	stožár
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
při	při	k7c6	při
odnášení	odnášení	k1gNnSc6	odnášení
vlajky	vlajka	k1gFnSc2	vlajka
z	z	k7c2	z
olympijského	olympijský	k2eAgInSc2d1	olympijský
stadionu	stadion	k1gInSc2	stadion
je	být	k5eAaImIp3nS	být
pozdravena	pozdravit	k5eAaPmNgFnS	pozdravit
pěti	pět	k4xCc7	pět
dělovými	dělový	k2eAgInPc7d1	dělový
výstřely	výstřel	k1gInPc7	výstřel
a	a	k8xC	a
pěvecké	pěvecký	k2eAgInPc1d1	pěvecký
sbory	sbor	k1gInPc1	sbor
zpívají	zpívat	k5eAaImIp3nP	zpívat
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Medailové	medailový	k2eAgNnSc4d1	medailové
pořadí	pořadí	k1gNnSc4	pořadí
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
soutěže	soutěž	k1gFnSc2	soutěž
a	a	k8xC	a
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
soutěž	soutěž	k1gFnSc1	soutěž
konala	konat	k5eAaImAgFnS	konat
<g/>
.	.	kIx.	.
</s>
<s>
Medaili	medaile	k1gFnSc4	medaile
předává	předávat	k5eAaImIp3nS	předávat
předseda	předseda	k1gMnSc1	předseda
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
nebo	nebo	k8xC	nebo
určený	určený	k2eAgMnSc1d1	určený
člen	člen	k1gMnSc1	člen
MOV	MOV	kA	MOV
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
předsedy	předseda	k1gMnSc2	předseda
příslušné	příslušný	k2eAgFnSc2d1	příslušná
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
sportovní	sportovní	k2eAgFnSc2d1	sportovní
federace	federace	k1gFnSc2	federace
nebo	nebo	k8xC	nebo
jeho	on	k3xPp3gMnSc2	on
náhradníka	náhradník	k1gMnSc2	náhradník
<g/>
.	.	kIx.	.
závodníci	závodník	k1gMnPc1	závodník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
skončili	skončit	k5eAaPmAgMnP	skončit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
<g/>
,	,	kIx,	,
druhém	druhý	k4xOgInSc6	druhý
a	a	k8xC	a
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
vystoupí	vystoupit	k5eAaPmIp3nP	vystoupit
na	na	k7c4	na
stupně	stupeň	k1gInPc4	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
;	;	kIx,	;
vítěz	vítěz	k1gMnSc1	vítěz
na	na	k7c4	na
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
středový	středový	k2eAgInSc4d1	středový
stupeň	stupeň	k1gInSc4	stupeň
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
na	na	k7c4	na
stupeň	stupeň	k1gInSc4	stupeň
po	po	k7c4	po
jeho	jeho	k3xOp3gFnSc4	jeho
pravici	pravice	k1gFnSc4	pravice
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
na	na	k7c4	na
stupeň	stupeň	k1gInSc4	stupeň
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
levici	levice	k1gFnSc6	levice
na	na	k7c4	na
středový	středový	k2eAgInSc4d1	středový
stožár	stožár	k1gInSc4	stožár
je	být	k5eAaImIp3nS	být
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
země	zem	k1gFnSc2	zem
vítěze	vítěz	k1gMnSc2	vítěz
<g/>
,	,	kIx,	,
vlajky	vlajka	k1gFnSc2	vlajka
druhého	druhý	k4xOgMnSc2	druhý
a	a	k8xC	a
třetího	třetí	k4xOgMnSc2	třetí
závodníka	závodník	k1gMnSc2	závodník
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
stožárech	stožár	k1gInPc6	stožár
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
<g/>
.	.	kIx.	.
zazní	zaznít	k5eAaPmIp3nS	zaznít
státní	státní	k2eAgFnSc1d1	státní
hymna	hymna	k1gFnSc1	hymna
vítěze	vítěz	k1gMnSc2	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Olympijský	olympijský	k2eAgInSc1d1	olympijský
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
podobné	podobný	k2eAgInPc4d1	podobný
akty	akt	k1gInPc4	akt
řady	řada	k1gFnSc2	řada
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
sportovních	sportovní	k2eAgFnPc2d1	sportovní
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
organizuje	organizovat	k5eAaBmIp3nS	organizovat
také	také	k9	také
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
letní	letní	k2eAgFnSc1d1	letní
či	či	k8xC	či
zimní	zimní	k2eAgFnSc1d1	zimní
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
dětem	dítě	k1gFnPc3	dítě
a	a	k8xC	a
mládeži	mládež	k1gFnSc3	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Olympiádami	olympiáda	k1gFnPc7	olympiáda
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nazývají	nazývat	k5eAaImIp3nP	nazývat
i	i	k9	i
různé	různý	k2eAgFnPc1d1	různá
místní	místní	k2eAgFnPc1d1	místní
nebo	nebo	k8xC	nebo
školní	školní	k2eAgFnPc1d1	školní
sportovní	sportovní	k2eAgFnPc1d1	sportovní
nebo	nebo	k8xC	nebo
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
soutěže	soutěž	k1gFnPc1	soutěž
napodobující	napodobující	k2eAgFnSc4d1	napodobující
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
olympiádu	olympiáda	k1gFnSc4	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tělesně	tělesně	k6eAd1	tělesně
postižené	postižený	k2eAgMnPc4d1	postižený
sportovce	sportovec	k1gMnPc4	sportovec
bývá	bývat	k5eAaImIp3nS	bývat
pořádána	pořádán	k2eAgFnSc1d1	pořádána
paralympiáda	paralympiáda	k1gFnSc1	paralympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Názvem	název	k1gInSc7	název
olympiáda	olympiáda	k1gFnSc1	olympiáda
byl	být	k5eAaImAgInS	být
inspirován	inspirován	k2eAgMnSc1d1	inspirován
i	i	k8xC	i
název	název	k1gInSc1	název
Československých	československý	k2eAgFnPc2d1	Československá
spartakiád	spartakiáda	k1gFnPc2	spartakiáda
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnSc2	slovo
Olympiáda	olympiáda	k1gFnSc1	olympiáda
a	a	k8xC	a
Olympijský	olympijský	k2eAgInSc1d1	olympijský
(	(	kIx(	(
<g/>
a	a	k8xC	a
slova	slovo	k1gNnPc1	slovo
odvozená	odvozený	k2eAgNnPc1d1	odvozené
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
symboly	symbol	k1gInPc7	symbol
pěti	pět	k4xCc2	pět
kruhů	kruh	k1gInPc2	kruh
a	a	k8xC	a
olympijské	olympijský	k2eAgFnSc2d1	olympijská
pochodně	pochodeň	k1gFnSc2	pochodeň
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
chráněna	chránit	k5eAaImNgFnS	chránit
zákonem	zákon	k1gInSc7	zákon
a	a	k8xC	a
nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
bez	bez	k7c2	bez
speciálního	speciální	k2eAgNnSc2d1	speciální
povolení	povolení	k1gNnSc2	povolení
Olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
povolení	povolení	k1gNnSc4	povolení
má	mít	k5eAaImIp3nS	mít
např.	např.	kA	např.
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
a	a	k8xC	a
smí	smět	k5eAaImIp3nS	smět
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
vzdělávací	vzdělávací	k2eAgFnPc4d1	vzdělávací
znalostní	znalostní	k2eAgFnPc4d1	znalostní
a	a	k8xC	a
dovednostní	dovednostní	k2eAgFnPc4d1	dovednostní
soutěže	soutěž	k1gFnPc4	soutěž
konané	konaný	k2eAgFnPc4d1	konaná
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
školami	škola	k1gFnPc7	škola
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
matematická	matematický	k2eAgFnSc1d1	matematická
olympiáda	olympiáda	k1gFnSc1	olympiáda
<g/>
,	,	kIx,	,
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
olympiáda	olympiáda	k1gFnSc1	olympiáda
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
olympiáda	olympiáda	k1gFnSc1	olympiáda
<g/>
,	,	kIx,	,
astronomická	astronomický	k2eAgFnSc1d1	astronomická
olympiáda	olympiáda	k1gFnSc1	olympiáda
atd.	atd.	kA	atd.
Zvolené	zvolený	k2eAgNnSc1d1	zvolené
pořadatelské	pořadatelský	k2eAgNnSc1d1	pořadatelské
město	město	k1gNnSc1	město
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
má	mít	k5eAaImIp3nS	mít
jednou	jednou	k6eAd1	jednou
pro	pro	k7c4	pro
vždy	vždy	k6eAd1	vždy
právo	právo	k1gNnSc4	právo
k	k	k7c3	k
přídomku	přídomek	k1gInSc3	přídomek
olympijské	olympijský	k2eAgFnSc2d1	olympijská
–	–	k?	–
tedy	tedy	k9	tedy
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Pořadatelská	pořadatelský	k2eAgFnSc1d1	pořadatelská
země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgFnPc3d1	ostatní
zemím	zem	k1gFnPc3	zem
navíc	navíc	k6eAd1	navíc
právo	právo	k1gNnSc1	právo
postavit	postavit	k5eAaPmF	postavit
svůj	svůj	k3xOyFgInSc4	svůj
národní	národní	k2eAgInSc4d1	národní
olympijský	olympijský	k2eAgInSc4d1	olympijský
tým	tým	k1gInSc4	tým
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
olympijských	olympijský	k2eAgInPc6d1	olympijský
kolektivních	kolektivní	k2eAgInPc6d1	kolektivní
sportech	sport	k1gInPc6	sport
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
kvalifikace	kvalifikace	k1gFnPc4	kvalifikace
<g/>
.	.	kIx.	.
</s>
