<s>
Obec	obec	k1gFnSc1	obec
Syrovice	syrovice	k1gFnSc2	syrovice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc3	její
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
826,81	[number]	k4	826,81
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
téměř	téměř	k6eAd1	téměř
1500	[number]	k4	1500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Obcí	obec	k1gFnSc7	obec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
Dyjsko-svrateckém	dyjskovratecký	k2eAgInSc6d1	dyjsko-svratecký
úvalu	úval	k1gInSc6	úval
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
potok	potok	k1gInSc4	potok
Syrůvka	syrůvka	k1gFnSc1	syrůvka
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zde	zde	k6eAd1	zde
také	také	k9	také
pramení	pramenit	k5eAaImIp3nS	pramenit
a	a	k8xC	a
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
vodou	voda	k1gFnSc7	voda
zdejší	zdejší	k2eAgFnSc7d1	zdejší
rybník	rybník	k1gInSc4	rybník
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jižním	jižní	k2eAgInSc6d1	jižní
břehu	břeh	k1gInSc6	břeh
rybníka	rybník	k1gInSc2	rybník
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
budova	budova	k1gFnSc1	budova
bývalého	bývalý	k2eAgInSc2d1	bývalý
vodního	vodní	k2eAgInSc2d1	vodní
mlýna	mlýn	k1gInSc2	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
celé	celý	k2eAgFnSc2d1	celá
obce	obec	k1gFnSc2	obec
je	být	k5eAaImIp3nS	být
zdejší	zdejší	k2eAgInSc1d1	zdejší
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Augustina	Augustin	k1gMnSc2	Augustin
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
zdejší	zdejší	k2eAgInSc4d1	zdejší
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
sídlí	sídlet	k5eAaImIp3nS	sídlet
místní	místní	k2eAgNnSc1d1	místní
sdružení	sdružení	k1gNnSc1	sdružení
Moravské	moravský	k2eAgFnSc2d1	Moravská
hasičské	hasičský	k2eAgFnSc2d1	hasičská
jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
družstvo	družstvo	k1gNnSc1	družstvo
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
1	[number]	k4	1
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
lihovar	lihovar	k1gInSc1	lihovar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
pouťová	pouťový	k2eAgFnSc1d1	pouťová
slavnost	slavnost	k1gFnSc1	slavnost
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
měsíce	měsíc	k1gInSc2	měsíc
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
Augustinské	Augustinský	k2eAgInPc4d1	Augustinský
hody	hod	k1gInPc4	hod
konané	konaný	k2eAgInPc4d1	konaný
poslední	poslední	k2eAgInPc4d1	poslední
víkend	víkend	k1gInSc4	víkend
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vinařskou	vinařský	k2eAgFnSc4d1	vinařská
obec	obec	k1gFnSc4	obec
ve	v	k7c6	v
Znojemské	znojemský	k2eAgFnSc6d1	Znojemská
vinařské	vinařský	k2eAgFnSc6d1	vinařská
podoblasti	podoblast	k1gFnSc6	podoblast
(	(	kIx(	(
<g/>
viniční	viniční	k2eAgFnSc2d1	viniční
tratě	trať	k1gFnSc2	trať
Stará	starý	k2eAgFnSc1d1	stará
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Nad	nad	k7c7	nad
Mlýnem	mlýn	k1gInSc7	mlýn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Syrovice	syrovice	k1gFnSc2	syrovice
získala	získat	k5eAaPmAgFnS	získat
tato	tento	k3xDgFnSc1	tento
vesnice	vesnice	k1gFnSc1	vesnice
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
syrovo	syrovo	k6eAd1	syrovo
(	(	kIx(	(
<g/>
chladno	chladno	k6eAd1	chladno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1294	[number]	k4	1294
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
zrušení	zrušení	k1gNnSc2	zrušení
poddanství	poddanství	k1gNnSc2	poddanství
se	se	k3xPyFc4	se
Syrovice	syrovice	k1gFnPc1	syrovice
dělily	dělit	k5eAaImAgFnP	dělit
na	na	k7c4	na
3	[number]	k4	3
díly	díl	k1gInPc4	díl
s	s	k7c7	s
rozdílnou	rozdílný	k2eAgFnSc7d1	rozdílná
vrchností	vrchnost	k1gFnSc7	vrchnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc3	červenec
1980	[number]	k4	1980
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
integraci	integrace	k1gFnSc3	integrace
obcí	obec	k1gFnPc2	obec
Bratčice	Bratčice	k1gFnSc2	Bratčice
<g/>
,	,	kIx,	,
Ledce	Ledce	k1gFnSc2	Ledce
<g/>
,	,	kIx,	,
Sobotovice	Sobotovice	k1gFnSc2	Sobotovice
a	a	k8xC	a
Syrovice	syrovice	k1gFnSc2	syrovice
pod	pod	k7c4	pod
společný	společný	k2eAgInSc4d1	společný
Místní	místní	k2eAgInSc4d1	místní
národní	národní	k2eAgInSc4d1	národní
výbor	výbor	k1gInSc4	výbor
v	v	k7c6	v
Syrovicích	syrovice	k1gFnPc6	syrovice
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
formálně	formálně	k6eAd1	formálně
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
jednu	jeden	k4xCgFnSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
integrovaném	integrovaný	k2eAgInSc6d1	integrovaný
Místním	místní	k2eAgInSc6d1	místní
národním	národní	k2eAgInSc6d1	národní
výboře	výboř	k1gFnSc2	výboř
měly	mít	k5eAaImAgFnP	mít
Sobotovice	Sobotovice	k1gFnPc1	Sobotovice
13	[number]	k4	13
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
,	,	kIx,	,
Ledce	Ledce	k1gFnSc1	Ledce
14	[number]	k4	14
<g/>
,	,	kIx,	,
Bratčice	Bratčice	k1gFnSc1	Bratčice
21	[number]	k4	21
<g/>
,	,	kIx,	,
a	a	k8xC	a
Syrovice	syrovice	k1gFnSc1	syrovice
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
všechny	všechen	k3xTgFnPc1	všechen
obce	obec	k1gFnPc1	obec
opětovně	opětovně	k6eAd1	opětovně
osamostatnily	osamostatnit	k5eAaPmAgFnP	osamostatnit
<g/>
.	.	kIx.	.
pozdně	pozdně	k6eAd1	pozdně
barokní	barokní	k2eAgInSc1d1	barokní
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Augustina	Augustin	k1gMnSc2	Augustin
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
do	do	k7c2	do
Bratčic	Bratčice	k1gFnPc2	Bratčice
krucifix	krucifix	k1gInSc4	krucifix
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Syrovice	syrovice	k1gFnSc2	syrovice
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Syrovice	syrovice	k1gFnSc2	syrovice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
