<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kometární	kometární	k2eAgInSc1d1	kometární
meteorický	meteorický	k2eAgInSc1d1	meteorický
roj	roj	k1gInSc1	roj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
komety	kometa	k1gFnSc2	kometa
109	[number]	k4	109
<g/>
P	P	kA	P
<g/>
/	/	kIx~	/
<g/>
Swift-Tuttle	Swift-Tuttle	k1gFnSc1	Swift-Tuttle
<g/>
?	?	kIx.	?
</s>
