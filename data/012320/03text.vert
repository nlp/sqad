<p>
<s>
Perseidy	perseida	k1gFnPc1	perseida
jsou	být	k5eAaImIp3nP	být
kometární	kometární	k2eAgInSc4d1	kometární
meteorický	meteorický	k2eAgInSc4d1	meteorický
roj	roj	k1gInSc4	roj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
komety	kometa	k1gFnSc2	kometa
109	[number]	k4	109
<g/>
P	P	kA	P
<g/>
/	/	kIx~	/
<g/>
Swift-Tuttle	Swift-Tuttle	k1gFnSc1	Swift-Tuttle
<g/>
.	.	kIx.	.
</s>
<s>
Perseidy	perseida	k1gFnPc1	perseida
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
radiant	radiant	k1gInSc4	radiant
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Persea	Perseus	k1gMnSc2	Perseus
<g/>
.	.	kIx.	.
</s>
<s>
Roj	roj	k1gInSc1	roj
je	být	k5eAaImIp3nS	být
činný	činný	k2eAgMnSc1d1	činný
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
maximum	maximum	k1gNnSc1	maximum
roje	roj	k1gInSc2	roj
přichází	přicházet	k5eAaImIp3nS	přicházet
mezi	mezi	k7c7	mezi
11	[number]	k4	11
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpnem	srpen	k1gInSc7	srpen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991-1993	[number]	k4	1991-1993
bylo	být	k5eAaImAgNnS	být
při	při	k7c6	při
posledním	poslední	k2eAgInSc6d1	poslední
průchodu	průchod	k1gInSc6	průchod
komety	kometa	k1gFnSc2	kometa
perihelem	perihel	k1gInSc7	perihel
(	(	kIx(	(
<g/>
doba	doba	k1gFnSc1	doba
oběhu	oběh	k1gInSc2	oběh
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
133	[number]	k4	133
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
možno	možno	k6eAd1	možno
pozorovat	pozorovat	k5eAaImF	pozorovat
přes	přes	k7c4	přes
300	[number]	k4	300
meteorů	meteor	k1gInPc2	meteor
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
maximu	maximum	k1gNnSc6	maximum
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
až	až	k9	až
110	[number]	k4	110
meteorů	meteor	k1gInPc2	meteor
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidově	lidově	k6eAd1	lidově
jsou	být	k5eAaImIp3nP	být
nazývány	nazýván	k2eAgFnPc1d1	nazývána
"	"	kIx"	"
<g/>
Slzy	slza	k1gFnPc1	slza
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
světce	světec	k1gMnSc2	světec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc4	svátek
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
(	(	kIx(	(
<g/>
toho	ten	k3xDgNnSc2	ten
dne	den	k1gInSc2	den
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
258	[number]	k4	258
umučen	umučit	k5eAaPmNgMnS	umučit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
informace	informace	k1gFnSc1	informace
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
roji	roj	k1gInSc6	roj
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
36	[number]	k4	36
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
zprávy	zpráva	k1gFnPc1	zpráva
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Římané	Říman	k1gMnPc1	Říman
si	se	k3xPyFc3	se
představovali	představovat	k5eAaImAgMnP	představovat
Perseidy	perseida	k1gFnPc4	perseida
jako	jako	k8xS	jako
ejakulaci	ejakulace	k1gFnSc4	ejakulace
Priapa	Priapos	k1gMnSc2	Priapos
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnPc1	první
známé	známá	k1gFnPc1	známá
pozorování	pozorování	k1gNnSc2	pozorování
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
811	[number]	k4	811
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1762	[number]	k4	1762
objevil	objevit	k5eAaPmAgMnS	objevit
holandský	holandský	k2eAgMnSc1d1	holandský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Pieter	Pieter	k1gMnSc1	Pieter
van	vana	k1gFnPc2	vana
Musschenbroeck	Musschenbroeck	k1gMnSc1	Musschenbroeck
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
srpnová	srpnový	k2eAgFnSc1d1	srpnová
meteorická	meteorický	k2eAgFnSc1d1	meteorická
aktivita	aktivita	k1gFnSc1	aktivita
je	být	k5eAaImIp3nS	být
každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
opakující	opakující	k2eAgFnSc7d1	opakující
událostí	událost	k1gFnSc7	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
uveřejnil	uveřejnit	k5eAaPmAgMnS	uveřejnit
belgický	belgický	k2eAgMnSc1d1	belgický
astronom	astronom	k1gMnSc1	astronom
Adolphe	Adolph	k1gFnSc2	Adolph
Quetelet	Quetelet	k1gInSc1	Quetelet
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
roji	roj	k1gInSc6	roj
"	"	kIx"	"
<g/>
vylétajícím	vylétající	k2eAgInPc3d1	vylétající
<g/>
"	"	kIx"	"
ze	z	k7c2	z
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
Persea	Perseus	k1gMnSc2	Perseus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
shodu	shoda	k1gFnSc4	shoda
drah	draha	k1gFnPc2	draha
Perseid	perseida	k1gFnPc2	perseida
a	a	k8xC	a
komety	kometa	k1gFnPc1	kometa
109	[number]	k4	109
<g/>
P	P	kA	P
<g/>
/	/	kIx~	/
<g/>
Swift	Swift	k1gMnSc1	Swift
<g/>
–	–	k?	–
<g/>
Tuttle	Tuttle	k1gFnSc1	Tuttle
(	(	kIx(	(
<g/>
Swift-Tuttle	Swift-Tuttle	k1gFnSc1	Swift-Tuttle
1862	[number]	k4	1862
III	III	kA	III
<g/>
)	)	kIx)	)
upozornil	upozornit	k5eAaPmAgMnS	upozornit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
italský	italský	k2eAgMnSc1d1	italský
astronom	astronom	k1gMnSc1	astronom
Giovanni	Giovanň	k1gMnSc3	Giovanň
Schiaparelli	Schiaparell	k1gMnSc3	Schiaparell
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Perseidy	perseida	k1gFnSc2	perseida
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Perseidy	perseida	k1gFnSc2	perseida
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
