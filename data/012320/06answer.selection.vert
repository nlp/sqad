<s>
Perseidy	perseida	k1gFnPc1	perseida
jsou	být	k5eAaImIp3nP	být
kometární	kometární	k2eAgInSc4d1	kometární
meteorický	meteorický	k2eAgInSc4d1	meteorický
roj	roj	k1gInSc4	roj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
komety	kometa	k1gFnSc2	kometa
109	[number]	k4	109
<g/>
P	P	kA	P
<g/>
/	/	kIx~	/
<g/>
Swift-Tuttle	Swift-Tuttle	k1gFnSc1	Swift-Tuttle
<g/>
.	.	kIx.	.
</s>
