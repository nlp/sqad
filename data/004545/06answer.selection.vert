<s>
Klasicismus	klasicismus	k1gInSc1	klasicismus
souvisel	souviset	k5eAaImAgInS	souviset
s	s	k7c7	s
racionalistickou	racionalistický	k2eAgFnSc7d1	racionalistická
filozofií	filozofie	k1gFnSc7	filozofie
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
předním	přední	k2eAgMnSc7d1	přední
představitelem	představitel	k1gMnSc7	představitel
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
byl	být	k5eAaImAgMnS	být
René	René	k1gMnSc1	René
Descartes	Descartes	k1gMnSc1	Descartes
<g/>
.	.	kIx.	.
</s>
