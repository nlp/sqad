<s>
Absolutistický	absolutistický	k2eAgInSc1d1	absolutistický
stát	stát	k1gInSc1	stát
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
ústřední	ústřední	k2eAgFnSc7d1	ústřední
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
vtiskoval	vtiskovat	k5eAaImAgMnS	vtiskovat
umění	umění	k1gNnSc4	umění
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
poměrně	poměrně	k6eAd1	poměrně
jednotný	jednotný	k2eAgInSc4d1	jednotný
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Uměleckému	umělecký	k2eAgInSc3d1	umělecký
stylu	styl	k1gInSc3	styl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
říkáme	říkat	k5eAaImIp1nP	říkat
klasicismus	klasicismus	k1gInSc4	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Navazoval	navazovat	k5eAaImAgMnS	navazovat
na	na	k7c4	na
odkaz	odkaz	k1gInSc4	odkaz
antiky	antika	k1gFnSc2	antika
především	především	k6eAd1	především
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zpracovávanými	zpracovávaný	k2eAgFnPc7d1	zpracovávaná
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
omezen	omezit	k5eAaPmNgInS	omezit
jen	jen	k9	jen
na	na	k7c4	na
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
projevil	projevit	k5eAaPmAgInS	projevit
nejsilněji	silně	k6eAd3	silně
a	a	k8xC	a
nejpříznačněji	příznačně	k6eAd3	příznačně
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
úloha	úloha	k1gFnSc1	úloha
francouzské	francouzský	k2eAgFnSc2d1	francouzská
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
vyplývala	vyplývat	k5eAaImAgFnS	vyplývat
z	z	k7c2	z
politického	politický	k2eAgNnSc2d1	politické
a	a	k8xC	a
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
postavení	postavení	k1gNnSc2	postavení
francouzského	francouzský	k2eAgInSc2d1	francouzský
státu	stát	k1gInSc2	stát
i	i	k8xC	i
značné	značný	k2eAgNnSc4d1	značné
rozšíření	rozšíření	k1gNnSc4	rozšíření
znalosti	znalost	k1gFnSc2	znalost
francouzského	francouzský	k2eAgInSc2d1	francouzský
jazyka	jazyk	k1gInSc2	jazyk
jako	jako	k8xS	jako
módního	módní	k2eAgInSc2d1	módní
jazyka	jazyk	k1gInSc2	jazyk
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
bohatého	bohatý	k2eAgNnSc2d1	bohaté
měšťanstva	měšťanstvo	k1gNnSc2	měšťanstvo
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Klasicismus	klasicismus	k1gInSc1	klasicismus
souvisel	souviset	k5eAaImAgInS	souviset
s	s	k7c7	s
racionalistickou	racionalistický	k2eAgFnSc7d1	racionalistická
filozofií	filozofie	k1gFnSc7	filozofie
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
předním	přední	k2eAgMnSc7d1	přední
představitelem	představitel	k1gMnSc7	představitel
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
byl	být	k5eAaImAgMnS	být
René	René	k1gMnSc1	René
Descartes	Descartes	k1gMnSc1	Descartes
<g/>
.	.	kIx.	.
</s>
<s>
Proslulou	proslulý	k2eAgFnSc7d1	proslulá
větou	věta	k1gFnSc7	věta
"	"	kIx"	"
<g/>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jsem	být	k5eAaImIp1nS	být
<g/>
"	"	kIx"	"
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediným	jediný	k2eAgInSc7d1	jediný
pevným	pevný	k2eAgInSc7d1	pevný
bodem	bod	k1gInSc7	bod
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
nelze	lze	k6eNd1	lze
pochybovat	pochybovat	k5eAaImF	pochybovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
představa	představa	k1gFnSc1	představa
sebe	sebe	k3xPyFc4	sebe
samého	samý	k3xTgMnSc4	samý
jakožto	jakožto	k8xS	jakožto
bytosti	bytost	k1gFnSc3	bytost
myslící	myslící	k2eAgFnPc1d1	myslící
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
mysticismu	mysticismus	k1gInSc3	mysticismus
baroka	baroko	k1gNnSc2	baroko
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
dualismem	dualismus	k1gInSc7	dualismus
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
duše	duše	k1gFnSc2	duše
stavěl	stavět	k5eAaImAgInS	stavět
klasicismus	klasicismus	k1gInSc1	klasicismus
střídmost	střídmost	k1gFnSc4	střídmost
a	a	k8xC	a
rozumovou	rozumový	k2eAgFnSc4d1	rozumová
kázeň	kázeň	k1gFnSc4	kázeň
<g/>
.	.	kIx.	.
</s>
<s>
Měřítkem	měřítko	k1gNnSc7	měřítko
krásy	krása	k1gFnSc2	krása
byla	být	k5eAaImAgFnS	být
klasicismu	klasicismus	k1gInSc2	klasicismus
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
rozvaha	rozvaha	k1gFnSc1	rozvaha
a	a	k8xC	a
rozumovost	rozumovost	k1gFnSc1	rozumovost
<g/>
,	,	kIx,	,
životním	životní	k2eAgInSc7d1	životní
ideálem	ideál	k1gInSc7	ideál
vyrovnanost	vyrovnanost	k1gFnSc1	vyrovnanost
<g/>
.	.	kIx.	.
</s>
<s>
Klasicismus	klasicismus	k1gInSc1	klasicismus
zdůraznil	zdůraznit	k5eAaPmAgInS	zdůraznit
podřízenost	podřízenost	k1gFnSc4	podřízenost
soukromých	soukromý	k2eAgInPc2d1	soukromý
cílů	cíl	k1gInPc2	cíl
nadosobním	nadosobní	k2eAgMnPc3d1	nadosobní
<g/>
,	,	kIx,	,
veřejným	veřejný	k2eAgMnPc3d1	veřejný
úkolům	úkol	k1gInPc3	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
klasicisté	klasicista	k1gMnPc1	klasicista
neobraceli	obracet	k5eNaImAgMnP	obracet
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
oblasti	oblast	k1gFnSc6	oblast
intimní	intimní	k2eAgMnSc1d1	intimní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
viděli	vidět	k5eAaImAgMnP	vidět
jej	on	k3xPp3gMnSc4	on
vždy	vždy	k6eAd1	vždy
ve	v	k7c6	v
sféře	sféra	k1gFnSc6	sféra
veřejné	veřejný	k2eAgFnSc6d1	veřejná
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
umělecké	umělecký	k2eAgFnSc3d1	umělecká
dokonalosti	dokonalost	k1gFnSc3	dokonalost
měla	mít	k5eAaImAgFnS	mít
klasicistům	klasicista	k1gMnPc3	klasicista
napomáhat	napomáhat	k5eAaImF	napomáhat
pevně	pevně	k6eAd1	pevně
stanovená	stanovený	k2eAgNnPc4d1	stanovené
estetická	estetický	k2eAgNnPc4d1	estetické
pravidla	pravidlo	k1gNnPc4	pravidlo
a	a	k8xC	a
předpisy	předpis	k1gInPc4	předpis
kompozice	kompozice	k1gFnSc2	kompozice
dramatu	drama	k1gNnSc2	drama
měla	mít	k5eAaImAgFnS	mít
např.	např.	kA	např.
zachovávat	zachovávat	k5eAaImF	zachovávat
Aristotelovu	Aristotelův	k2eAgFnSc4d1	Aristotelova
zásadu	zásada	k1gFnSc4	zásada
tří	tři	k4xCgFnPc2	tři
jednot	jednota	k1gFnPc2	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
se	se	k3xPyFc4	se
rozlišovala	rozlišovat	k5eAaImAgFnS	rozlišovat
na	na	k7c4	na
nízkou	nízký	k2eAgFnSc4d1	nízká
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc4d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
také	také	k9	také
byly	být	k5eAaImAgInP	být
přesně	přesně	k6eAd1	přesně
od	od	k7c2	od
sebe	se	k3xPyFc2	se
odděleny	oddělit	k5eAaPmNgInP	oddělit
komika	komika	k1gFnSc1	komika
a	a	k8xC	a
tragičnost	tragičnost	k1gFnSc1	tragičnost
<g/>
.	.	kIx.	.
</s>
<s>
Tragédie	tragédie	k1gFnSc1	tragédie
jako	jako	k8xC	jako
žánr	žánr	k1gInSc1	žánr
nejvznešenější	vznešený	k2eAgFnSc1d3	nejvznešenější
byla	být	k5eAaImAgFnS	být
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
látkám	látka	k1gFnPc3	látka
ze	z	k7c2	z
života	život	k1gInSc2	život
vysokých	vysoký	k2eAgFnPc2d1	vysoká
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hrdiny	hrdina	k1gMnPc4	hrdina
a	a	k8xC	a
děj	děj	k1gInSc4	děj
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
většinou	většinou	k6eAd1	většinou
antika	antika	k1gFnSc1	antika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
směly	smět	k5eAaImAgFnP	smět
vystupovat	vystupovat	k5eAaImF	vystupovat
i	i	k9	i
postavy	postava	k1gFnPc4	postava
lidové	lidový	k2eAgFnPc4d1	lidová
a	a	k8xC	a
náměty	námět	k1gInPc7	námět
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
dotýkat	dotýkat	k5eAaImF	dotýkat
současnosti	současnost	k1gFnPc4	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
klasicismu	klasicismus	k1gInSc2	klasicismus
většinou	většinou	k6eAd1	většinou
neprojevují	projevovat	k5eNaImIp3nP	projevovat
otevřeně	otevřeně	k6eAd1	otevřeně
osobní	osobní	k2eAgInPc4d1	osobní
názory	názor	k1gInPc4	názor
k	k	k7c3	k
dobové	dobový	k2eAgFnSc3d1	dobová
problematice	problematika	k1gFnSc3	problematika
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
zobrazení	zobrazení	k1gNnSc1	zobrazení
závazných	závazný	k2eAgFnPc2d1	závazná
norem	norma	k1gFnPc2	norma
společenské	společenský	k2eAgFnSc2d1	společenská
mravnosti	mravnost	k1gFnSc2	mravnost
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
soukromí	soukromí	k1gNnSc1	soukromí
bývá	bývat	k5eAaImIp3nS	bývat
utajeno	utajit	k5eAaPmNgNnS	utajit
<g/>
,	,	kIx,	,
citová	citový	k2eAgFnSc1d1	citová
složka	složka	k1gFnSc1	složka
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
složce	složka	k1gFnSc3	složka
racionální	racionální	k2eAgInSc4d1	racionální
<g/>
.	.	kIx.	.
</s>
<s>
Převažují	převažovat	k5eAaImIp3nP	převažovat
ty	ten	k3xDgInPc4	ten
žánry	žánr	k1gInPc4	žánr
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
složka	složka	k1gFnSc1	složka
rozumová	rozumový	k2eAgFnSc1d1	rozumová
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
složku	složka	k1gFnSc4	složka
emocionální	emocionální	k2eAgFnSc2d1	emocionální
–	–	k?	–
v	v	k7c6	v
poezii	poezie	k1gFnSc6	poezie
didaktická	didaktický	k2eAgFnSc1d1	didaktická
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
moralistická	moralistický	k2eAgNnPc1d1	moralistické
vyprávění	vyprávění	k1gNnPc1	vyprávění
<g/>
,	,	kIx,	,
esej	esej	k1gInSc1	esej
a	a	k8xC	a
úvaha	úvaha	k1gFnSc1	úvaha
<g/>
,	,	kIx,	,
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
veršovaná	veršovaný	k2eAgFnSc1d1	veršovaná
tragédie	tragédie	k1gFnSc1	tragédie
předkládající	předkládající	k2eAgFnSc1d1	předkládající
obecenstvu	obecenstvo	k1gNnSc3	obecenstvo
soubor	soubor	k1gInSc1	soubor
mravních	mravní	k2eAgInPc2d1	mravní
příkladů	příklad	k1gInPc2	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Příznačný	příznačný	k2eAgInSc1d1	příznačný
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc1	vznik
literárních	literární	k2eAgInPc2d1	literární
salónů	salón	k1gInPc2	salón
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
někdejší	někdejší	k2eAgNnSc4d1	někdejší
dvorské	dvorský	k2eAgNnSc4d1	dvorské
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
o	o	k7c6	o
umění	umění	k1gNnSc1	umění
a	a	k8xC	a
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
se	se	k3xPyFc4	se
literární	literární	k2eAgNnPc1d1	literární
díla	dílo	k1gNnPc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nova	k1gFnSc3	nova
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
literární	literární	k2eAgFnSc2d1	literární
akademie	akademie	k1gFnSc2	akademie
bdí	bdít	k5eAaImIp3nP	bdít
nad	nad	k7c7	nad
čistotou	čistota	k1gFnSc7	čistota
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
ukázňují	ukázňovat	k5eAaImIp3nP	ukázňovat
formu	forma	k1gFnSc4	forma
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souvisí	souviset	k5eAaImIp3nS	souviset
poměrná	poměrný	k2eAgFnSc1d1	poměrná
uniformita	uniformita	k1gFnSc1	uniformita
klasicistické	klasicistický	k2eAgFnSc2d1	klasicistická
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
literárního	literární	k2eAgNnSc2d1	literární
díla	dílo	k1gNnSc2	dílo
k	k	k7c3	k
čtenáři	čtenář	k1gMnSc3	čtenář
–	–	k?	–
spisovatel	spisovatel	k1gMnSc1	spisovatel
netvoří	tvořit	k5eNaImIp3nS	tvořit
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
ochránce	ochránce	k1gMnSc4	ochránce
jako	jako	k8xC	jako
v	v	k7c6	v
době	doba	k1gFnSc6	doba
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pro	pro	k7c4	pro
mnohem	mnohem	k6eAd1	mnohem
širší	široký	k2eAgInSc4d2	širší
čtenářský	čtenářský	k2eAgInSc4d1	čtenářský
okruh	okruh	k1gInSc4	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Ideovou	ideový	k2eAgFnSc7d1	ideová
a	a	k8xC	a
politickou	politický	k2eAgFnSc7d1	politická
orientací	orientace	k1gFnSc7	orientace
hájí	hájit	k5eAaImIp3nS	hájit
a	a	k8xC	a
zdůvodňuje	zdůvodňovat	k5eAaImIp3nS	zdůvodňovat
soudobý	soudobý	k2eAgInSc4d1	soudobý
společenský	společenský	k2eAgInSc4d1	společenský
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
zájmy	zájem	k1gInPc4	zájem
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1	francouzský
klasicismus	klasicismus	k1gInSc1	klasicismus
zrodil	zrodit	k5eAaPmAgInS	zrodit
tři	tři	k4xCgFnPc4	tři
velké	velký	k2eAgFnPc4d1	velká
dramatiky	dramatika	k1gFnPc4	dramatika
<g/>
:	:	kIx,	:
Pierre	Pierr	k1gMnSc5	Pierr
Corneille	Corneill	k1gMnSc5	Corneill
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
tragédií	tragédie	k1gFnSc7	tragédie
Cid	Cid	k1gFnSc2	Cid
největší	veliký	k2eAgInSc4d3	veliký
ohlasu	ohlas	k1gInSc3	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Diskutovalo	diskutovat	k5eAaImAgNnS	diskutovat
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
v	v	k7c6	v
salónech	salón	k1gInPc6	salón
i	i	k8xC	i
v	v	k7c6	v
nedlouho	dlouho	k6eNd1	dlouho
předtím	předtím	k6eAd1	předtím
založené	založený	k2eAgFnSc3d1	založená
akademii	akademie	k1gFnSc3	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Rodrigo	Rodrigo	k6eAd1	Rodrigo
zabije	zabít	k5eAaPmIp3nS	zabít
hraběte	hrabě	k1gMnSc4	hrabě
Gormu	Gorma	k1gFnSc4	Gorma
<g/>
,	,	kIx,	,
otce	otka	k1gFnSc3	otka
své	svůj	k3xOyFgInPc4	svůj
milé	milý	k2eAgInPc4d1	milý
Chimeny	Chimen	k1gInPc4	Chimen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
urazil	urazit	k5eAaPmAgMnS	urazit
Rodrigova	Rodrigův	k2eAgMnSc4d1	Rodrigův
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
Chimena	Chimen	k2eAgFnSc1d1	Chimena
miluje	milovat	k5eAaImIp3nS	milovat
Rodriga	Rodriga	k1gFnSc1	Rodriga
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nad	nad	k7c7	nad
jejím	její	k3xOp3gInSc7	její
citem	cit	k1gInSc7	cit
převáží	převážet	k5eAaImIp3nS	převážet
čest	čest	k1gFnSc1	čest
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
dívka	dívka	k1gFnSc1	dívka
žádá	žádat	k5eAaImIp3nS	žádat
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
potrestal	potrestat	k5eAaPmAgMnS	potrestat
vraha	vrah	k1gMnSc4	vrah
<g/>
.	.	kIx.	.
</s>
<s>
Dramatická	dramatický	k2eAgFnSc1d1	dramatická
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
za	za	k7c4	za
níž	nízce	k6eAd2	nízce
oba	dva	k4xCgMnPc1	dva
hrdinové	hrdina	k1gMnPc1	hrdina
trpí	trpět	k5eAaImIp3nP	trpět
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
milují	milovat	k5eAaImIp3nP	milovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přitom	přitom	k6eAd1	přitom
trvají	trvat	k5eAaImIp3nP	trvat
na	na	k7c6	na
představách	představa	k1gFnPc6	představa
cti	čest	k1gFnSc2	čest
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
končí	končit	k5eAaImIp3nS	končit
smírně	smírně	k6eAd1	smírně
zásahem	zásah	k1gInSc7	zásah
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Rodrigo	Rodrigo	k1gMnSc1	Rodrigo
odčinil	odčinit	k5eAaPmAgMnS	odčinit
svou	svůj	k3xOyFgFnSc4	svůj
vinu	vina	k1gFnSc4	vina
udatností	udatnost	k1gFnPc2	udatnost
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Corneille	Corneille	k6eAd1	Corneille
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
tvůrcem	tvůrce	k1gMnSc7	tvůrce
moderní	moderní	k2eAgFnSc2d1	moderní
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
dal	dát	k5eAaPmAgInS	dát
pevnou	pevný	k2eAgFnSc4d1	pevná
kompoziční	kompoziční	k2eAgFnSc4d1	kompoziční
zákonitost	zákonitost	k1gFnSc4	zákonitost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
hrdiny	hrdina	k1gMnPc7	hrdina
nedrtí	drtit	k5eNaImIp3nP	drtit
náhodný	náhodný	k2eAgInSc1d1	náhodný
osud	osud	k1gInSc1	osud
ani	ani	k8xC	ani
nejsou	být	k5eNaImIp3nP	být
hříčkou	hříčka	k1gFnSc7	hříčka
svých	svůj	k3xOyFgFnPc2	svůj
vášní	vášeň	k1gFnPc2	vášeň
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
ze	z	k7c2	z
střetnutí	střetnutí	k1gNnSc2	střetnutí
osobních	osobní	k2eAgInPc2d1	osobní
citů	cit	k1gInPc2	cit
a	a	k8xC	a
nadosobními	nadosobní	k2eAgFnPc7d1	nadosobní
povinnostmi	povinnost	k1gFnPc7	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Tragédii	tragédie	k1gFnSc4	tragédie
psychologickou	psychologický	k2eAgFnSc4d1	psychologická
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Jean	Jean	k1gMnSc1	Jean
Racine	Racin	k1gMnSc5	Racin
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
hrách	hra	k1gFnPc6	hra
vzniká	vznikat	k5eAaImIp3nS	vznikat
střetnutím	střetnutí	k1gNnSc7	střetnutí
citu	cit	k1gInSc2	cit
s	s	k7c7	s
egoistickými	egoistický	k2eAgFnPc7d1	egoistická
vášněmi	vášeň	k1gFnPc7	vášeň
<g/>
.	.	kIx.	.
</s>
<s>
Antické	antický	k2eAgInPc1d1	antický
náměty	námět	k1gInPc1	námět
mívají	mívat	k5eAaImIp3nP	mívat
u	u	k7c2	u
Racina	Racino	k1gNnSc2	Racino
jen	jen	k9	jen
antické	antický	k2eAgNnSc4d1	antické
roucho	roucho	k1gNnSc4	roucho
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nimi	on	k3xPp3gFnPc7	on
se	se	k3xPyFc4	se
rýsují	rýsovat	k5eAaImIp3nP	rýsovat
současné	současný	k2eAgFnPc4d1	současná
postavy	postava	k1gFnPc4	postava
a	a	k8xC	a
soudobé	soudobý	k2eAgInPc4d1	soudobý
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
milostných	milostný	k2eAgInPc6d1	milostný
příbězích	příběh	k1gInPc6	příběh
zobrazoval	zobrazovat	k5eAaImAgInS	zobrazovat
Racine	Racin	k1gInSc5	Racin
zhoubný	zhoubný	k2eAgInSc1d1	zhoubný
vliv	vliv	k1gInSc1	vliv
vášně	vášeň	k1gFnSc2	vášeň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ničí	ničit	k5eAaImIp3nS	ničit
nejen	nejen	k6eAd1	nejen
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
jí	on	k3xPp3gFnSc7	on
podlehnou	podlehnout	k5eAaPmIp3nP	podlehnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
lidi	člověk	k1gMnPc4	člověk
jim	on	k3xPp3gInPc3	on
blízké	blízký	k2eAgFnPc1d1	blízká
<g/>
.	.	kIx.	.
</s>
<s>
Racinovo	Racinův	k2eAgNnSc1d1	Racinův
dramatické	dramatický	k2eAgNnSc1d1	dramatické
umění	umění	k1gNnSc1	umění
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
vrcholům	vrchol	k1gInPc3	vrchol
klasické	klasický	k2eAgFnSc2d1	klasická
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
;	;	kIx,	;
velmi	velmi	k6eAd1	velmi
výstižně	výstižně	k6eAd1	výstižně
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
psychologii	psychologie	k1gFnSc4	psychologie
svých	svůj	k3xOyFgMnPc2	svůj
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ženských	ženský	k2eAgFnPc2d1	ženská
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Komedie	komedie	k1gFnSc1	komedie
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
období	období	k1gNnSc6	období
klasicismu	klasicismus	k1gInSc2	klasicismus
schůdnější	schůdný	k2eAgFnSc4d2	schůdnější
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
životní	životní	k2eAgFnSc2d1	životní
reality	realita	k1gFnSc2	realita
než	než	k8xS	než
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mohla	moct	k5eAaImAgFnS	moct
navazovat	navazovat	k5eAaImF	navazovat
na	na	k7c4	na
lidové	lidový	k2eAgFnPc4d1	lidová
a	a	k8xC	a
pololidové	pololidový	k2eAgFnPc4d1	pololidová
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
blízko	blízko	k6eAd1	blízko
ke	k	k7c3	k
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
dramatický	dramatický	k2eAgMnSc1d1	dramatický
autor	autor	k1gMnSc1	autor
komedií	komedie	k1gFnPc2	komedie
byl	být	k5eAaImAgMnS	být
Moliè	Moliè	k1gMnSc1	Moliè
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
jej	on	k3xPp3gMnSc4	on
přitahovalo	přitahovat	k5eAaImAgNnS	přitahovat
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgInS	působit
u	u	k7c2	u
hereckých	herecký	k2eAgFnPc2d1	herecká
kočovných	kočovný	k2eAgFnPc2d1	kočovná
společností	společnost	k1gFnPc2	společnost
jako	jako	k8xS	jako
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
frašek	fraška	k1gFnPc2	fraška
a	a	k8xC	a
komedií	komedie	k1gFnPc2	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
královského	královský	k2eAgNnSc2d1	královské
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
hrách	hra	k1gFnPc6	hra
předváděl	předvádět	k5eAaImAgMnS	předvádět
soudobou	soudobý	k2eAgFnSc4d1	soudobá
pestrou	pestrý	k2eAgFnSc4d1	pestrá
mravní	mravní	k2eAgFnSc4d1	mravní
problematiku	problematika	k1gFnSc4	problematika
<g/>
.	.	kIx.	.
</s>
<s>
Problematika	problematika	k1gFnSc1	problematika
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
široká	široký	k2eAgFnSc1d1	široká
–	–	k?	–
týkala	týkat	k5eAaImAgFnS	týkat
se	se	k3xPyFc4	se
postavení	postavení	k1gNnSc4	postavení
ženy	žena	k1gFnSc2	žena
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
i	i	k8xC	i
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
mravního	mravní	k2eAgInSc2d1	mravní
cynismu	cynismus	k1gInSc2	cynismus
feudálů	feudál	k1gMnPc2	feudál
<g/>
,	,	kIx,	,
chorobné	chorobný	k2eAgFnPc1d1	chorobná
lakoty	lakota	k1gFnPc1	lakota
<g/>
,	,	kIx,	,
úlisnosti	úlisnost	k1gFnPc1	úlisnost
<g/>
,	,	kIx,	,
náboženského	náboženský	k2eAgNnSc2d1	náboženské
pokrytectví	pokrytectví	k1gNnSc2	pokrytectví
<g/>
,	,	kIx,	,
snahy	snaha	k1gFnSc2	snaha
měšťáků	měšťák	k1gMnPc2	měšťák
o	o	k7c6	o
napodobení	napodobení	k1gNnSc6	napodobení
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Nejodvážnější	odvážný	k2eAgMnPc1d3	nejodvážnější
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
komedií	komedie	k1gFnPc2	komedie
byla	být	k5eAaImAgFnS	být
Tartuffe	Tartuffe	k1gMnSc5	Tartuffe
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
odhalovala	odhalovat	k5eAaImAgFnS	odhalovat
pokrytectví	pokrytectví	k1gNnSc3	pokrytectví
a	a	k8xC	a
svatouškovství	svatouškovství	k1gNnSc3	svatouškovství
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
titulní	titulní	k2eAgFnSc7d1	titulní
postavou	postava	k1gFnSc7	postava
mířila	mířit	k5eAaImAgFnS	mířit
proti	proti	k7c3	proti
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
důvěru	důvěra	k1gFnSc4	důvěra
měšťana	měšťan	k1gMnSc2	měšťan
Orgona	Orgon	k1gMnSc2	Orgon
<g/>
,	,	kIx,	,
ovládá	ovládat	k5eAaImIp3nS	ovládat
ho	on	k3xPp3gInSc4	on
a	a	k8xC	a
nabývá	nabývat	k5eAaImIp3nS	nabývat
vlivu	vliv	k1gInSc2	vliv
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gFnSc7	jeho
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
si	se	k3xPyFc3	se
vzít	vzít	k5eAaPmF	vzít
jeho	jeho	k3xOp3gFnSc4	jeho
dceru	dcera	k1gFnSc4	dcera
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
rodiny	rodina	k1gFnSc2	rodina
se	se	k3xPyFc4	se
marně	marně	k6eAd1	marně
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
překazit	překazit	k5eAaPmF	překazit
tyto	tento	k3xDgInPc4	tento
záměry	záměr	k1gInPc4	záměr
<g/>
.	.	kIx.	.
</s>
<s>
Orgon	Orgon	k1gInSc1	Orgon
zcela	zcela	k6eAd1	zcela
podléhá	podléhat	k5eAaImIp3nS	podléhat
Tartuffovi	Tartuffe	k1gMnSc3	Tartuffe
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
mu	on	k3xPp3gMnSc3	on
připíše	připsat	k5eAaPmIp3nS	připsat
i	i	k9	i
svůj	svůj	k3xOyFgInSc4	svůj
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
jen	jen	k9	jen
králův	králův	k2eAgInSc4d1	králův
zásah	zásah	k1gInSc4	zásah
zabrání	zabránit	k5eAaPmIp3nS	zabránit
úplnému	úplný	k2eAgNnSc3d1	úplné
zničení	zničení	k1gNnSc3	zničení
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
nejproslulejší	proslulý	k2eAgFnSc7d3	nejproslulejší
Moliè	Moliè	k1gFnSc7	Moliè
komedií	komedie	k1gFnPc2	komedie
je	být	k5eAaImIp3nS	být
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1	titulní
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
lichvář	lichvář	k1gMnSc1	lichvář
Harpagon	Harpagon	k1gMnSc1	Harpagon
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
pro	pro	k7c4	pro
peníze	peníz	k1gInPc4	peníz
obětovat	obětovat	k5eAaBmF	obětovat
všechno	všechen	k3xTgNnSc4	všechen
a	a	k8xC	a
ztráta	ztráta	k1gFnSc1	ztráta
pokladnice	pokladnice	k1gFnSc2	pokladnice
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
znamená	znamenat	k5eAaImIp3nS	znamenat
životní	životní	k2eAgInSc1d1	životní
tragédii	tragédie	k1gFnSc3	tragédie
<g/>
,	,	kIx,	,
poloviční	poloviční	k2eAgNnSc1d1	poloviční
šílenství	šílenství	k1gNnSc1	šílenství
a	a	k8xC	a
nenávist	nenávist	k1gFnSc1	nenávist
k	k	k7c3	k
celému	celý	k2eAgNnSc3d1	celé
lidstvu	lidstvo	k1gNnSc3	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
Moliè	Moliè	k1gFnSc1	Moliè
komedie	komedie	k1gFnSc2	komedie
<g/>
:	:	kIx,	:
Škola	škola	k1gFnSc1	škola
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
Misantrop	misantrop	k1gMnSc1	misantrop
<g/>
,	,	kIx,	,
Měšťák	měšťák	k1gMnSc1	měšťák
šlechticem	šlechtic	k1gMnSc7	šlechtic
<g/>
,	,	kIx,	,
Šibalství	šibalství	k1gNnSc1	šibalství
Skapinova	Skapinův	k2eAgNnSc2d1	Skapinův
a	a	k8xC	a
Zdravý	zdravý	k2eAgMnSc1d1	zdravý
nemocný	nemocný	k1gMnSc1	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
humor	humor	k1gInSc1	humor
byl	být	k5eAaImAgInS	být
útočnou	útočný	k2eAgFnSc7d1	útočná
zbraní	zbraň	k1gFnSc7	zbraň
proti	proti	k7c3	proti
předsudkům	předsudek	k1gInPc3	předsudek
a	a	k8xC	a
zlořádům	zlořád	k1gInPc3	zlořád
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
zdravého	zdravý	k2eAgInSc2d1	zdravý
rozumu	rozum	k1gInSc2	rozum
a	a	k8xC	a
bodré	bodrý	k2eAgFnSc2d1	bodrá
lidové	lidový	k2eAgFnSc2d1	lidová
morálky	morálka	k1gFnSc2	morálka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
postavách	postava	k1gFnPc6	postava
svých	svůj	k3xOyFgFnPc2	svůj
komedií	komedie	k1gFnPc2	komedie
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Moliè	Moliè	k1gMnSc1	Moliè
velkolepé	velkolepý	k2eAgFnSc2d1	velkolepá
typy	typa	k1gFnSc2	typa
právě	právě	k9	právě
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
objevil	objevit	k5eAaPmAgInS	objevit
pravdivou	pravdivý	k2eAgFnSc4d1	pravdivá
podstatu	podstata	k1gFnSc4	podstata
lidských	lidský	k2eAgInPc2d1	lidský
charakterů	charakter	k1gInPc2	charakter
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
dobách	doba	k1gFnPc6	doba
a	a	k8xC	a
společenských	společenský	k2eAgInPc6d1	společenský
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Protipólem	protipól	k1gInSc7	protipól
vznešené	vznešený	k2eAgFnSc2d1	vznešená
klasicistické	klasicistický	k2eAgFnSc2d1	klasicistická
tragédie	tragédie	k1gFnSc2	tragédie
byla	být	k5eAaImAgFnS	být
italská	italský	k2eAgFnSc1d1	italská
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
u	u	k7c2	u
lidového	lidový	k2eAgNnSc2d1	lidové
publika	publikum	k1gNnSc2	publikum
neobyčejně	obyčejně	k6eNd1	obyčejně
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
neměly	mít	k5eNaImAgFnP	mít
pevnou	pevný	k2eAgFnSc4d1	pevná
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
herci	herec	k1gMnPc1	herec
děj	děj	k1gInSc4	děj
improvizovali	improvizovat	k5eAaImAgMnP	improvizovat
podle	podle	k7c2	podle
stručně	stručně	k6eAd1	stručně
nastíněného	nastíněný	k2eAgInSc2d1	nastíněný
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Komedie	komedie	k1gFnSc1	komedie
měla	mít	k5eAaImAgFnS	mít
své	svůj	k3xOyFgFnPc4	svůj
ustálené	ustálený	k2eAgFnPc4d1	ustálená
postavy	postava	k1gFnPc4	postava
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bohatý	bohatý	k2eAgMnSc1d1	bohatý
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
sluhové	sluhová	k1gFnPc1	sluhová
<g/>
,	,	kIx,	,
služka	služka	k1gFnSc1	služka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
základu	základ	k1gInSc2	základ
společensky	společensky	k6eAd1	společensky
nezávazné	závazný	k2eNgFnSc2d1	nezávazná
komedie	komedie	k1gFnSc2	komedie
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
zakladatel	zakladatel	k1gMnSc1	zakladatel
italské	italský	k2eAgFnSc2d1	italská
národní	národní	k2eAgFnSc2d1	národní
veselohry	veselohra	k1gFnSc2	veselohra
Carlo	Carlo	k1gNnSc4	Carlo
Goldoni	Goldoň	k1gFnSc3	Goldoň
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
komedie	komedie	k1gFnPc1	komedie
byly	být	k5eAaImAgFnP	být
výstižnými	výstižný	k2eAgFnPc7d1	výstižná
charakteristikami	charakteristika	k1gFnPc7	charakteristika
soudobého	soudobý	k2eAgInSc2d1	soudobý
měšťanského	měšťanský	k2eAgInSc2d1	měšťanský
světa	svět	k1gInSc2	svět
jako	jako	k8xS	jako
například	například	k6eAd1	například
Chytrá	chytrý	k2eAgFnSc1d1	chytrá
vdova	vdova	k1gFnSc1	vdova
nebo	nebo	k8xC	nebo
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
typů	typ	k1gInPc2	typ
jako	jako	k8xC	jako
Jemnostpán	jemnostpán	k1gMnSc1	jemnostpán
markýz	markýz	k1gMnSc1	markýz
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
dílo	dílo	k1gNnSc4	dílo
Goldoniho	Goldoni	k1gMnSc2	Goldoni
se	se	k3xPyFc4	se
pokládá	pokládat	k5eAaImIp3nS	pokládat
komedie	komedie	k1gFnSc1	komedie
Poprask	poprask	k1gInSc1	poprask
na	na	k7c6	na
laguně	laguna	k1gFnSc6	laguna
s	s	k7c7	s
žárlivou	žárlivý	k2eAgFnSc7d1	žárlivá
mileneckou	milenecký	k2eAgFnSc7d1	milenecká
dvojicí	dvojice	k1gFnSc7	dvojice
z	z	k7c2	z
lidového	lidový	k2eAgNnSc2d1	lidové
rybářského	rybářský	k2eAgNnSc2d1	rybářské
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mohutného	mohutný	k2eAgInSc2d1	mohutný
rozvoje	rozvoj	k1gInSc2	rozvoj
dramatu	drama	k1gNnSc2	drama
byl	být	k5eAaImAgInS	být
vývoj	vývoj	k1gInSc1	vývoj
poezie	poezie	k1gFnSc2	poezie
mnohem	mnohem	k6eAd1	mnohem
skromnější	skromný	k2eAgFnSc1d2	skromnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klasicistní	klasicistní	k2eAgFnSc6d1	klasicistní
poezii	poezie	k1gFnSc6	poezie
hovoří	hovořit	k5eAaImIp3nS	hovořit
daleko	daleko	k6eAd1	daleko
víc	hodně	k6eAd2	hodně
rozum	rozum	k1gInSc4	rozum
než	než	k8xS	než
srdce	srdce	k1gNnSc4	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Převládá	převládat	k5eAaImIp3nS	převládat
proto	proto	k8xC	proto
popisná	popisný	k2eAgFnSc1d1	popisná
<g/>
,	,	kIx,	,
úvahová	úvahový	k2eAgFnSc1d1	úvahová
a	a	k8xC	a
satirická	satirický	k2eAgFnSc1d1	satirická
báseň	báseň	k1gFnSc1	báseň
–	–	k?	–
epigram	epigram	k1gInSc1	epigram
<g/>
,	,	kIx,	,
satira	satira	k1gFnSc1	satira
<g/>
,	,	kIx,	,
zábavná	zábavný	k2eAgFnSc1d1	zábavná
poéma	poéma	k1gFnSc1	poéma
a	a	k8xC	a
bajka	bajka	k1gFnSc1	bajka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
drobnějších	drobný	k2eAgInPc6d2	drobnější
básnických	básnický	k2eAgInPc6d1	básnický
útvarech	útvar	k1gInPc6	útvar
měl	mít	k5eAaImAgInS	mít
neobyčejný	obyčejný	k2eNgInSc1d1	neobyčejný
úspěch	úspěch	k1gInSc1	úspěch
Jean	Jean	k1gMnSc1	Jean
de	de	k?	de
la	la	k1gNnPc2	la
Fontaine	Fontain	k1gInSc5	Fontain
sbírkou	sbírka	k1gFnSc7	sbírka
bajek	bajka	k1gFnPc2	bajka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
ohlas	ohlas	k1gInSc1	ohlas
pronikl	proniknout	k5eAaPmAgInS	proniknout
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
znalosti	znalost	k1gFnSc2	znalost
starších	starý	k2eAgMnPc2d2	starší
autorů	autor	k1gMnPc2	autor
bajek	bajka	k1gFnPc2	bajka
od	od	k7c2	od
Ezopa	Ezop	k1gMnSc2	Ezop
přes	přes	k7c4	přes
středověké	středověký	k2eAgFnPc4d1	středověká
moralistické	moralistický	k2eAgFnPc4d1	moralistická
bajky	bajka	k1gFnPc4	bajka
<g/>
.	.	kIx.	.
</s>
<s>
Shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
mravní	mravní	k2eAgNnSc1d1	mravní
naučení	naučení	k1gNnSc1	naučení
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
praktické	praktický	k2eAgFnSc2d1	praktická
životní	životní	k2eAgFnSc2d1	životní
morálky	morálka	k1gFnSc2	morálka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
bajky	bajka	k1gFnSc2	bajka
aktualizuje	aktualizovat	k5eAaBmIp3nS	aktualizovat
k	k	k7c3	k
potřebám	potřeba	k1gFnPc3	potřeba
společenské	společenský	k2eAgFnSc2d1	společenská
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
ARNE	Arne	k1gMnSc1	Arne
<g/>
,	,	kIx,	,
Novák	Novák	k1gMnSc1	Novák
<g/>
.	.	kIx.	.
</s>
<s>
Stručné	stručný	k2eAgFnPc4d1	stručná
dějiny	dějiny	k1gFnPc4	dějiny
literatury	literatura	k1gFnSc2	literatura
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
:	:	kIx,	:
R.	R.	kA	R.
Promberger	Promberger	k1gMnSc1	Promberger
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Klasicismus	klasicismus	k1gInSc1	klasicismus
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
-	-	kIx~	-
<g/>
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
153	[number]	k4	153
<g/>
-	-	kIx~	-
<g/>
198	[number]	k4	198
<g/>
.	.	kIx.	.
</s>
