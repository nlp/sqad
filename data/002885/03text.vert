<s>
Kálení	kálení	k1gNnSc1	kálení
Rafanů	Rafan	k1gInPc2	Rafan
ve	v	k7c6	v
Veletržním	veletržní	k2eAgInSc6d1	veletržní
paláci	palác	k1gInSc6	palác
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Veletržního	veletržní	k2eAgInSc2d1	veletržní
paláce	palác	k1gInSc2	palác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
veřejně	veřejně	k6eAd1	veřejně
vykáleli	vykálet	k5eAaPmAgMnP	vykálet
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
v	v	k7c6	v
expozici	expozice	k1gFnSc6	expozice
českého	český	k2eAgNnSc2d1	české
a	a	k8xC	a
slovenského	slovenský	k2eAgNnSc2d1	slovenské
umění	umění	k1gNnSc2	umění
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
-	-	kIx~	-
blízko	blízko	k7c2	blízko
děl	dělo	k1gNnPc2	dělo
autorů	autor	k1gMnPc2	autor
Milana	Milan	k1gMnSc2	Milan
Knížáka	Knížák	k1gMnSc2	Knížák
a	a	k8xC	a
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Berana	Beran	k1gMnSc2	Beran
-	-	kIx~	-
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
způsobu	způsob	k1gInSc3	způsob
vedení	vedení	k1gNnSc2	vedení
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
umělci	umělec	k1gMnPc1	umělec
Marek	marka	k1gFnPc2	marka
Meduna	Meduna	k1gFnSc1	Meduna
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Motejzík	Motejzík	k1gMnSc1	Motejzík
(	(	kIx(	(
<g/>
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
Rafani	Rafan	k1gMnPc5	Rafan
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Ondřej	Ondřej	k1gMnSc1	Ondřej
Brody	Brod	k1gInPc4	Brod
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Franta	Franta	k1gMnSc1	Franta
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Magid	Magid	k1gInSc1	Magid
<g/>
,	,	kIx,	,
student	student	k1gMnSc1	student
UMPRUM	umprum	k1gInSc1	umprum
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odešli	odejít	k5eAaPmAgMnP	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Událost	událost	k1gFnSc1	událost
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
tisková	tiskový	k2eAgFnSc1d1	tisková
mluvčí	mluvčí	k1gFnSc1	mluvčí
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
pak	pak	k6eAd1	pak
podala	podat	k5eAaPmAgFnS	podat
trestní	trestní	k2eAgNnSc4d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
výsledek	výsledek	k1gInSc1	výsledek
ale	ale	k9	ale
nebyl	být	k5eNaImAgInS	být
nijak	nijak	k6eAd1	nijak
výrazně	výrazně	k6eAd1	výrazně
medializován	medializovat	k5eAaImNgInS	medializovat
<g/>
.	.	kIx.	.
</s>
<s>
Výkaly	výkal	k1gInPc4	výkal
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
nikdy	nikdy	k6eAd1	nikdy
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
je	on	k3xPp3gFnPc4	on
uklidila	uklidit	k5eAaPmAgFnS	uklidit
uklízečka	uklízečka	k1gFnSc1	uklízečka
Vendula	Vendula	k1gFnSc1	Vendula
Vlachová	Vlachová	k1gFnSc1	Vlachová
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
ČTK	ČTK	kA	ČTK
z	z	k7c2	z
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
převzaté	převzatý	k2eAgFnSc2d1	převzatá
dalšími	další	k2eAgMnPc7d1	další
médii	médium	k1gNnPc7	médium
bylo	být	k5eAaImAgNnS	být
účastníků	účastník	k1gMnPc2	účastník
akce	akce	k1gFnPc4	akce
šest	šest	k4xCc1	šest
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
tři	tři	k4xCgInPc4	tři
byli	být	k5eAaImAgMnP	být
členy	člen	k1gMnPc7	člen
umělecké	umělecký	k2eAgFnSc2d1	umělecká
skupiny	skupina	k1gFnSc2	skupina
Rafani	Rafaň	k1gFnSc3	Rafaň
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
prohlášení	prohlášení	k1gNnSc2	prohlášení
zveřejněného	zveřejněný	k2eAgNnSc2d1	zveřejněné
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
samotnými	samotný	k2eAgMnPc7d1	samotný
aktéry	aktér	k1gMnPc7	aktér
bylo	být	k5eAaImAgNnS	být
účastníků	účastník	k1gMnPc2	účastník
pět	pět	k4xCc1	pět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Marek	Marek	k1gMnSc1	Marek
Meduna	Meduna	k1gFnSc1	Meduna
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Motejzík	Motejzík	k1gMnSc1	Motejzík
(	(	kIx(	(
<g/>
tyto	tento	k3xDgFnPc4	tento
dva	dva	k4xCgInPc4	dva
ČTK	ČTK	kA	ČTK
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
jako	jako	k9	jako
členy	člen	k1gMnPc4	člen
skupiny	skupina	k1gFnSc2	skupina
Rafani	Rafan	k1gMnPc1	Rafan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Ondřej	Ondřej	k1gMnSc1	Ondřej
Brody	Brod	k1gInPc4	Brod
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Franta	Franta	k1gMnSc1	Franta
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Magid	Magida	k1gFnPc2	Magida
<g/>
.	.	kIx.	.
</s>
<s>
Účastník	účastník	k1gMnSc1	účastník
akce	akce	k1gFnSc2	akce
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Rafani	Rafaň	k1gFnSc6	Rafaň
Marek	Marek	k1gMnSc1	Marek
Meduna	Meduna	k1gFnSc1	Meduna
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
vyjádření	vyjádření	k1gNnSc4	vyjádření
k	k	k7c3	k
akci	akce	k1gFnSc3	akce
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
agentura	agentura	k1gFnSc1	agentura
ČTK	ČTK	kA	ČTK
a	a	k8xC	a
převzala	převzít	k5eAaPmAgNnP	převzít
další	další	k2eAgNnPc1d1	další
média	médium	k1gNnPc1	médium
<g/>
,	,	kIx,	,
nesdělil	sdělit	k5eNaPmAgInS	sdělit
účel	účel	k1gInSc4	účel
akce	akce	k1gFnSc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
ČTK	ČTK	kA	ČTK
ji	on	k3xPp3gFnSc4	on
dávala	dávat	k5eAaImAgFnS	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
nesouhlasnými	souhlasný	k2eNgInPc7d1	nesouhlasný
projevy	projev	k1gInPc7	projev
vůči	vůči	k7c3	vůči
koncepci	koncepce	k1gFnSc3	koncepce
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
při	při	k7c6	při
vedení	vedení	k1gNnSc6	vedení
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
ředitel	ředitel	k1gMnSc1	ředitel
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
<g/>
,	,	kIx,	,
a	a	k8xC	a
připomněla	připomnět	k5eAaPmAgFnS	připomnět
akci	akce	k1gFnSc4	akce
umělecké	umělecký	k2eAgFnSc2d1	umělecká
skupiny	skupina	k1gFnSc2	skupina
PodeBal	PodeBal	k1gInSc1	PodeBal
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
nalepila	nalepit	k5eAaPmAgFnS	nalepit
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
výlohy	výloha	k1gFnSc2	výloha
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
budovy	budova	k1gFnSc2	budova
části	část	k1gFnSc3	část
cihlu	cihla	k1gFnSc4	cihla
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
cihlu	cihla	k1gFnSc4	cihla
prolétající	prolétající	k2eAgFnSc6d1	prolétající
sklem	sklo	k1gNnSc7	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
poškození	poškození	k1gNnSc4	poškození
výlohy	výloha	k1gFnSc2	výloha
bylo	být	k5eAaImAgNnS	být
Národní	národní	k2eAgNnSc1d1	národní
galerií	galerie	k1gFnSc7	galerie
podáno	podán	k2eAgNnSc1d1	podáno
trestní	trestní	k2eAgNnSc1d1	trestní
oznámení	oznámení	k1gNnSc1	oznámení
a	a	k8xC	a
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
náhradu	náhrada	k1gFnSc4	náhrada
škody	škoda	k1gFnSc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
k	k	k7c3	k
události	událost	k1gFnSc3	událost
připojily	připojit	k5eAaPmAgFnP	připojit
charakteristiku	charakteristika	k1gFnSc4	charakteristika
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
východisek	východisko	k1gNnPc2	východisko
Rafanů	Rafan	k1gInPc2	Rafan
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgMnPc3	jenž
patří	patřit	k5eAaImIp3nS	patřit
provokace	provokace	k1gFnSc1	provokace
<g/>
,	,	kIx,	,
angažovaná	angažovaný	k2eAgFnSc1d1	angažovaná
tvorba	tvorba	k1gFnSc1	tvorba
reagující	reagující	k2eAgFnSc1d1	reagující
na	na	k7c4	na
problémy	problém	k1gInPc4	problém
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
otevírání	otevírání	k1gNnSc1	otevírání
tabuizovaných	tabuizovaný	k2eAgInPc2d1	tabuizovaný
a	a	k8xC	a
neřešených	řešený	k2eNgInPc2d1	neřešený
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prohlášení	prohlášení	k1gNnSc6	prohlášení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
5	[number]	k4	5
účastníků	účastník	k1gMnPc2	účastník
vydalo	vydat	k5eAaPmAgNnS	vydat
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgNnSc4	svůj
jednání	jednání	k1gNnSc4	jednání
označili	označit	k5eAaPmAgMnP	označit
za	za	k7c4	za
performanci	performance	k1gFnSc4	performance
"	"	kIx"	"
<g/>
Bez	bez	k7c2	bez
názvu	název	k1gInSc2	název
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
znepokojení	znepokojení	k1gNnSc2	znepokojení
stavem	stav	k1gInSc7	stav
NG	NG	kA	NG
a	a	k8xC	a
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
Knížák	Knížák	k1gMnSc1	Knížák
vede	vést	k5eAaImIp3nS	vést
<g/>
,	,	kIx,	,
a	a	k8xC	a
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
názor	názor	k1gInSc1	názor
na	na	k7c4	na
vedení	vedení	k1gNnSc4	vedení
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
a	a	k8xC	a
forma	forma	k1gFnSc1	forma
jeho	jeho	k3xOp3gNnSc2	jeho
vyjádření	vyjádření	k1gNnSc2	vyjádření
je	být	k5eAaImIp3nS	být
osobní	osobní	k2eAgFnSc7d1	osobní
věcí	věc	k1gFnSc7	věc
každého	každý	k3xTgMnSc2	každý
občana	občan	k1gMnSc2	občan
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
ovšem	ovšem	k9	ovšem
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
státní	státní	k2eAgFnSc4d1	státní
instituci	instituce	k1gFnSc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Konstatovali	konstatovat	k5eAaBmAgMnP	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
forma	forma	k1gFnSc1	forma
performance	performance	k1gFnSc2	performance
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
jistě	jistě	k6eAd1	jistě
vulgární	vulgární	k2eAgFnSc1d1	vulgární
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
překračovala	překračovat	k5eAaImAgFnS	překračovat
meze	mez	k1gFnPc4	mez
slušného	slušný	k2eAgNnSc2d1	slušné
chování	chování	k1gNnSc2	chování
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
položili	položit	k5eAaPmAgMnP	položit
rétorickou	rétorický	k2eAgFnSc4d1	rétorická
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	on	k3xPp3gMnPc4	on
překročili	překročit	k5eAaPmAgMnP	překročit
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ač	ač	k8xS	ač
nejsme	být	k5eNaImIp1nP	být
exhibicionisty	exhibicionista	k1gMnPc7	exhibicionista
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
postavili	postavit	k5eAaPmAgMnP	postavit
"	"	kIx"	"
<g/>
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
vedle	vedle	k7c2	vedle
způsobů	způsob	k1gInPc2	způsob
chování	chování	k1gNnSc2	chování
ředitele	ředitel	k1gMnSc2	ředitel
NG	NG	kA	NG
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Magdalena	Magdalena	k1gFnSc1	Magdalena
Čechlovská	Čechlovský	k2eAgFnSc1d1	Čechlovská
v	v	k7c6	v
Hospodářských	hospodářský	k2eAgFnPc6d1	hospodářská
novinách	novina	k1gFnPc6	novina
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
napsala	napsat	k5eAaPmAgFnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Rafani	Rafan	k1gMnPc1	Rafan
vykáleli	vykálet	k5eAaPmAgMnP	vykálet
"	"	kIx"	"
<g/>
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
podobě	podoba	k1gFnSc3	podoba
stálé	stálý	k2eAgFnSc2d1	stálá
expozice	expozice	k1gFnSc2	expozice
současného	současný	k2eAgNnSc2d1	současné
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Média	médium	k1gNnSc2	médium
tuto	tento	k3xDgFnSc4	tento
akci	akce	k1gFnSc4	akce
připomínala	připomínat	k5eAaImAgFnS	připomínat
při	pře	k1gFnSc4	pře
příležitostí	příležitost	k1gFnPc2	příležitost
dalších	další	k2eAgFnPc2d1	další
akcí	akce	k1gFnPc2	akce
skupiny	skupina	k1gFnSc2	skupina
i	i	k9	i
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ČTK	ČTK	kA	ČTK
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
někteří	některý	k3yIgMnPc1	některý
kritici	kritik	k1gMnPc1	kritik
a	a	k8xC	a
teoretici	teoretik	k1gMnPc1	teoretik
provokativní	provokativní	k2eAgFnSc2d1	provokativní
akce	akce	k1gFnSc2	akce
Rafanů	Rafan	k1gInPc2	Rafan
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
<g/>
.	.	kIx.	.
</s>
<s>
Kritik	kritik	k1gMnSc1	kritik
Petr	Petr	k1gMnSc1	Petr
Volf	Volf	k1gMnSc1	Volf
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2007	[number]	k4	2007
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
akci	akce	k1gFnSc4	akce
ve	v	k7c6	v
Veletržním	veletržní	k2eAgInSc6d1	veletržní
paláci	palác	k1gInSc6	palác
ve	v	k7c6	v
stručném	stručný	k2eAgInSc6d1	stručný
výčtu	výčet	k1gInSc6	výčet
dvou	dva	k4xCgFnPc2	dva
akcí	akce	k1gFnPc2	akce
Rafanů	Rafan	k1gMnPc2	Rafan
a	a	k8xC	a
připomněl	připomnět	k5eAaPmAgMnS	připomnět
<g/>
,	,	kIx,	,
že	že	k8xS	že
skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Chalupeckého	Chalupecký	k2eAgMnSc2d1	Chalupecký
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2008	[number]	k4	2008
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
o	o	k7c4	o
udělení	udělení	k1gNnSc4	udělení
ceny	cena	k1gFnSc2	cena
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
o	o	k7c6	o
skupině	skupina	k1gFnSc6	skupina
Ztohoven	Ztohovna	k1gFnPc2	Ztohovna
za	za	k7c4	za
vpašování	vpašování	k1gNnSc4	vpašování
mystifikačního	mystifikační	k2eAgInSc2d1	mystifikační
jaderného	jaderný	k2eAgInSc2d1	jaderný
výbuchu	výbuch	k1gInSc2	výbuch
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
do	do	k7c2	do
vysílání	vysílání	k1gNnSc2	vysílání
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rafani	Rafan	k1gMnPc1	Rafan
"	"	kIx"	"
<g/>
nemají	mít	k5eNaImIp3nP	mít
odvahu	odvaha	k1gFnSc4	odvaha
<g/>
"	"	kIx"	"
a	a	k8xC	a
při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
akcích	akce	k1gFnPc6	akce
opakovaně	opakovaně	k6eAd1	opakovaně
jednali	jednat	k5eAaImAgMnP	jednat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
neriskovali	riskovat	k5eNaBmAgMnP	riskovat
nic	nic	k6eAd1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Šli	jít	k5eAaImAgMnP	jít
se	se	k3xPyFc4	se
vykálet	vykálet	k5eAaPmF	vykálet
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místo	místo	k1gNnSc1	místo
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
doprostřed	doprostřed	k7c2	doprostřed
sálu	sál	k1gInSc2	sál
a	a	k8xC	a
sundali	sundat	k5eAaPmAgMnP	sundat
kalhoty	kalhoty	k1gFnPc4	kalhoty
<g/>
,	,	kIx,	,
schovali	schovat	k5eAaPmAgMnP	schovat
se	se	k3xPyFc4	se
za	za	k7c4	za
panel	panel	k1gInSc4	panel
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Stejnými	stejný	k2eAgNnPc7d1	stejné
slovy	slovo	k1gNnPc7	slovo
bagatelizoval	bagatelizovat	k5eAaImAgInS	bagatelizovat
význam	význam	k1gInSc4	význam
jiné	jiný	k2eAgFnSc2d1	jiná
akce	akce	k1gFnSc2	akce
skupiny	skupina	k1gFnSc2	skupina
Rafani	Rafaň	k1gFnSc3	Rafaň
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Můj	můj	k3xOp1gInSc1	můj
scénář	scénář	k1gInSc1	scénář
není	být	k5eNaImIp3nS	být
ukradnout	ukradnout	k5eAaPmF	ukradnout
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
pečlivě	pečlivě	k6eAd1	pečlivě
ho	on	k3xPp3gMnSc4	on
zatavit	zatavit	k5eAaPmF	zatavit
a	a	k8xC	a
opatrně	opatrně	k6eAd1	opatrně
spustit	spustit	k5eAaPmF	spustit
do	do	k7c2	do
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
zase	zase	k9	zase
nepoškozený	poškozený	k2eNgMnSc1d1	nepoškozený
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
ho	on	k3xPp3gInSc4	on
vzít	vzít	k5eAaPmF	vzít
<g/>
,	,	kIx,	,
mrsknout	mrsknout	k5eAaPmF	mrsknout
ho	on	k3xPp3gMnSc4	on
tam	tam	k6eAd1	tam
a	a	k8xC	a
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomhle	tenhle	k3xDgInSc6	tenhle
směru	směr	k1gInSc6	směr
má	mít	k5eAaImIp3nS	mít
můj	můj	k3xOp1gInSc4	můj
obdiv	obdiv	k1gInSc4	obdiv
skupina	skupina	k1gFnSc1	skupina
Ztohoven	Ztohovna	k1gFnPc2	Ztohovna
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
nebojí	bát	k5eNaImIp3nP	bát
riskovat	riskovat	k5eAaBmF	riskovat
a	a	k8xC	a
jdou	jít	k5eAaImIp3nP	jít
do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
i	i	k9	i
s	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
možného	možný	k2eAgInSc2d1	možný
trestu	trest	k1gInSc2	trest
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgInSc1	ten
atomový	atomový	k2eAgInSc1d1	atomový
hřib	hřib	k1gInSc1	hřib
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
ti	ten	k3xDgMnPc1	ten
panáčci	panáček	k1gMnPc1	panáček
na	na	k7c6	na
semaforech	semafor	k1gInPc6	semafor
<g/>
.	.	kIx.	.
</s>
<s>
Rafani	Rafan	k1gMnPc1	Rafan
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
koncentrovanou	koncentrovaný	k2eAgFnSc7d1	koncentrovaná
srabárnou	srabárna	k1gFnSc7	srabárna
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Umělcovo	umělcův	k2eAgNnSc4d1	umělcovo
hovno	hovno	k1gNnSc4	hovno
Rafani	Rafan	k1gMnPc1	Rafan
se	se	k3xPyFc4	se
vykáleli	vykálet	k5eAaPmAgMnP	vykálet
v	v	k7c6	v
galerii	galerie	k1gFnSc6	galerie
<g/>
,	,	kIx,	,
Novinky	novinka	k1gFnPc1	novinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
ČTK	ČTK	kA	ČTK
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
Rafani	Rafan	k1gMnPc1	Rafan
pokáleli	pokálet	k5eAaPmAgMnP	pokálet
Národní	národní	k2eAgFnSc4d1	národní
galerii	galerie	k1gFnSc4	galerie
<g/>
,	,	kIx,	,
Bleskově	bleskově	k6eAd1	bleskově
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
ČTK	ČTK	kA	ČTK
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
Protestní	protestní	k2eAgInSc4d1	protestní
kálení	kálení	k1gNnSc1	kálení
umělců	umělec	k1gMnPc2	umělec
v	v	k7c6	v
expozici	expozice	k1gFnSc6	expozice
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Ahaswebovy	Ahaswebův	k2eAgFnPc4d1	Ahaswebův
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
</s>
