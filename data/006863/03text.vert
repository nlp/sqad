<s>
Kanton	Kanton	k1gInSc1	Kanton
Saint-Martin-de-Valamas	Saint-Martine-Valamas	k1gInSc1	Saint-Martin-de-Valamas
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
Canton	Canton	k1gInSc1	Canton
de	de	k?	de
Saint-Martin-de-Valamas	Saint-Martine-Valamas	k1gInSc1	Saint-Martin-de-Valamas
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgInSc4d1	francouzský
kanton	kanton	k1gInSc4	kanton
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Ardè	Ardè	k1gFnSc2	Ardè
v	v	k7c6	v
regionu	region	k1gInSc6	region
Rhône-Alpes	Rhône-Alpes	k1gInSc1	Rhône-Alpes
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
11	[number]	k4	11
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Arcens	Arcens	k6eAd1	Arcens
Borée	Borée	k1gFnSc1	Borée
Chanéac	Chanéac	k1gFnSc1	Chanéac
Intres	intres	k1gInSc4	intres
Lachapelle-sous-Chanéac	Lachapelleous-Chanéac	k1gInSc1	Lachapelle-sous-Chanéac
La	la	k1gNnSc1	la
Rochette	Rochett	k1gInSc5	Rochett
Saint-Clément	Saint-Clément	k1gInSc1	Saint-Clément
Saint-Jean-Roure	Saint-Jean-Rour	k1gMnSc5	Saint-Jean-Rour
Saint-Julien-Boutiè	Saint-Julien-Boutiè	k1gFnSc2	Saint-Julien-Boutiè
Saint-Martial	Saint-Martial	k1gMnSc1	Saint-Martial
Saint-Martin-de-Valamas	Saint-Martine-Valamas	k1gMnSc1	Saint-Martin-de-Valamas
</s>
