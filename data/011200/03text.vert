<p>
<s>
Svitek	svitek	k1gInSc1	svitek
je	být	k5eAaImIp3nS	být
svinutý	svinutý	k2eAgInSc1d1	svinutý
pás	pás	k1gInSc1	pás
ohebného	ohebný	k2eAgInSc2d1	ohebný
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
fotografického	fotografický	k2eAgInSc2d1	fotografický
filmu	film	k1gInSc2	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
speciálním	speciální	k2eAgInSc6d1	speciální
významu	význam	k1gInSc6	význam
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
pás	pás	k1gInSc1	pás
papíru	papír	k1gInSc2	papír
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgInSc2d1	jiný
psacího	psací	k2eAgInSc2d1	psací
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Svitek	svitek	k1gInSc4	svitek
byla	být	k5eAaImAgFnS	být
nejběžnější	běžný	k2eAgFnSc1d3	nejběžnější
forma	forma	k1gFnSc1	forma
knihy	kniha	k1gFnSc2	kniha
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
bohoslužbě	bohoslužba	k1gFnSc6	bohoslužba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Delší	dlouhý	k2eAgInPc1d2	delší
texty	text	k1gInPc1	text
se	se	k3xPyFc4	se
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
psaly	psát	k5eAaImAgInP	psát
na	na	k7c4	na
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
pás	pás	k1gInSc4	pás
(	(	kIx(	(
<g/>
3	[number]	k4	3
až	až	k9	až
7	[number]	k4	7
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgMnSc1d1	vzniklý
slepením	slepení	k1gNnSc7	slepení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
listů	list	k1gInPc2	list
papyru	papyr	k1gInSc2	papyr
nebo	nebo	k8xC	nebo
pergamenu	pergamen	k1gInSc2	pergamen
<g/>
.	.	kIx.	.
</s>
<s>
Psalo	psát	k5eAaImAgNnS	psát
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
sloupcích	sloupec	k1gInPc6	sloupec
napříč	napříč	k7c3	napříč
pásu	pás	k1gInSc3	pás
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
při	při	k7c6	při
převíjení	převíjení	k1gNnSc6	převíjení
pásu	pás	k1gInSc2	pás
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
čtenář	čtenář	k1gMnSc1	čtenář
viděl	vidět	k5eAaImAgMnS	vidět
vždy	vždy	k6eAd1	vždy
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
dva	dva	k4xCgInPc4	dva
sloupce	sloupec	k1gInPc4	sloupec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
usnadnění	usnadnění	k1gNnSc4	usnadnění
převíjení	převíjení	k1gNnSc2	převíjení
bývaly	bývat	k5eAaImAgInP	bývat
později	pozdě	k6eAd2	pozdě
svitky	svitek	k1gInPc1	svitek
opatřeny	opatřen	k2eAgInPc1d1	opatřen
dřevěnými	dřevěný	k2eAgFnPc7d1	dřevěná
tyčkami	tyčka	k1gFnPc7	tyčka
a	a	k8xC	a
rukojeťmi	rukojeť	k1gFnPc7	rukojeť
<g/>
.	.	kIx.	.
</s>
<s>
Svitky	svitek	k1gInPc1	svitek
se	se	k3xPyFc4	se
ukládaly	ukládat	k5eAaImAgInP	ukládat
na	na	k7c4	na
police	police	k1gFnPc4	police
v	v	k7c6	v
knihovnách	knihovna	k1gFnPc6	knihovna
a	a	k8xC	a
přenášely	přenášet	k5eAaImAgFnP	přenášet
se	se	k3xPyFc4	se
v	v	k7c6	v
koších	koš	k1gInPc6	koš
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
pozdně	pozdně	k6eAd1	pozdně
starověký	starověký	k2eAgInSc1d1	starověký
obraz	obraz	k1gInSc1	obraz
sv.	sv.	kA	sv.
Pavla	Pavla	k1gFnSc1	Pavla
s	s	k7c7	s
košem	koš	k1gInSc7	koš
svitků	svitek	k1gInPc2	svitek
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
svitku	svitek	k1gInSc2	svitek
je	být	k5eAaImIp3nS	být
jednoduchost	jednoduchost	k1gFnSc1	jednoduchost
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
patří	patřit	k5eAaImIp3nS	patřit
omezená	omezený	k2eAgFnSc1d1	omezená
délka	délka	k1gFnSc1	délka
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
antické	antický	k2eAgInPc1d1	antický
spisy	spis	k1gInPc1	spis
musely	muset	k5eAaImAgInP	muset
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
kratší	krátký	k2eAgInSc4d2	kratší
"	"	kIx"	"
<g/>
knihy	kniha	k1gFnSc2	kniha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
svitky	svitek	k1gInPc1	svitek
<g/>
)	)	kIx)	)
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
čtenář	čtenář	k1gMnSc1	čtenář
musel	muset	k5eAaImAgMnS	muset
svitek	svitek	k1gInSc4	svitek
držet	držet	k5eAaImF	držet
oběma	dva	k4xCgFnPc7	dva
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
delším	dlouhý	k2eAgInSc6d2	delší
svitku	svitek	k1gInSc6	svitek
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
kniha	kniha	k1gFnSc1	kniha
znamenala	znamenat	k5eAaImAgFnS	znamenat
vždy	vždy	k6eAd1	vždy
svitek	svitek	k1gInSc4	svitek
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
"	"	kIx"	"
<g/>
otevřít	otevřít	k5eAaPmF	otevřít
knihu	kniha	k1gFnSc4	kniha
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
rozvinout	rozvinout	k5eAaPmF	rozvinout
knihu	kniha	k1gFnSc4	kniha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Svitky	svitek	k1gInPc1	svitek
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
ilustrované	ilustrovaný	k2eAgFnPc1d1	ilustrovaná
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
,	,	kIx,	,
z	z	k7c2	z
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
,	,	kIx,	,
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
i	i	k9	i
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejslavnější	slavný	k2eAgInPc4d3	nejslavnější
svitky	svitek	k1gInPc4	svitek
patří	patřit	k5eAaImIp3nP	patřit
Kumránské	Kumránský	k2eAgInPc4d1	Kumránský
svitky	svitek	k1gInPc4	svitek
s	s	k7c7	s
biblickými	biblický	k2eAgInPc7d1	biblický
i	i	k8xC	i
jinými	jiný	k2eAgInPc7d1	jiný
hebrejskými	hebrejský	k2eAgInPc7d1	hebrejský
texty	text	k1gInPc7	text
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
až	až	k9	až
1	[number]	k4	1
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
objevené	objevený	k2eAgFnSc6d1	objevená
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
objevena	objeven	k2eAgFnSc1d1	objevena
nejstarší	starý	k2eAgFnSc1d3	nejstarší
přesně	přesně	k6eAd1	přesně
datovaná	datovaný	k2eAgFnSc1d1	datovaná
tištěná	tištěný	k2eAgFnSc1d1	tištěná
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
svitek	svitek	k1gInSc1	svitek
"	"	kIx"	"
<g/>
Diamantové	diamantový	k2eAgFnSc2d1	Diamantová
sútry	sútra	k1gFnSc2	sútra
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
868	[number]	k4	868
<g/>
;	;	kIx,	;
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
stránky	stránka	k1gFnPc1	stránka
se	se	k3xPyFc4	se
tiskly	tisknout	k5eAaImAgFnP	tisknout
z	z	k7c2	z
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
ne	ne	k9	ne
z	z	k7c2	z
pohyblivých	pohyblivý	k2eAgFnPc2d1	pohyblivá
liter	litera	k1gFnPc2	litera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
začal	začít	k5eAaPmAgInS	začít
na	na	k7c6	na
západě	západ	k1gInSc6	západ
svitky	svitek	k1gInPc4	svitek
vytlačovat	vytlačovat	k5eAaImF	vytlačovat
kodex	kodex	k1gInSc1	kodex
<g/>
,	,	kIx,	,
vázaná	vázaný	k2eAgFnSc1d1	vázaná
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
listovat	listovat	k5eAaImF	listovat
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
římských	římský	k2eAgInPc2d1	římský
svazků	svazek	k1gInPc2	svazek
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
užívali	užívat	k5eAaImAgMnP	užívat
zejména	zejména	k9	zejména
právníci	právník	k1gMnPc1	právník
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
výhodou	výhoda	k1gFnSc7	výhoda
kodexu	kodex	k1gInSc2	kodex
byl	být	k5eAaImAgMnS	být
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc4d2	veliký
rozsah	rozsah	k1gInSc4	rozsah
(	(	kIx(	(
<g/>
stovky	stovka	k1gFnPc4	stovka
listů	list	k1gInPc2	list
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snazší	snadný	k2eAgNnSc4d2	snazší
hledání	hledání	k1gNnSc4	hledání
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
obsahu	obsah	k1gInSc2	obsah
(	(	kIx(	(
<g/>
indexu	index	k1gInSc2	index
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
těžký	těžký	k2eAgInSc1d1	těžký
kodex	kodex	k1gInSc1	kodex
mohl	moct	k5eAaImAgInS	moct
ležet	ležet	k5eAaImF	ležet
na	na	k7c6	na
pultu	pult	k1gInSc6	pult
nebo	nebo	k8xC	nebo
na	na	k7c6	na
stole	stol	k1gInSc6	stol
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
kodexů	kodex	k1gInPc2	kodex
snad	snad	k9	snad
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
a	a	k8xC	a
možná	možná	k9	možná
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
i	i	k9	i
vývoj	vývoj	k1gInSc4	vývoj
biblického	biblický	k2eAgInSc2d1	biblický
kánonu	kánon	k1gInSc2	kánon
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
svitky	svitek	k1gInPc1	svitek
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
užívat	užívat	k5eAaImF	užívat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
do	do	k7c2	do
kodexu	kodex	k1gInSc2	kodex
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
vybrat	vybrat	k5eAaPmF	vybrat
závazný	závazný	k2eAgInSc4d1	závazný
soubor	soubor	k1gInSc4	soubor
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
pevně	pevně	k6eAd1	pevně
daném	daný	k2eAgNnSc6d1	dané
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
<g/>
Přes	přes	k7c4	přes
zřejmé	zřejmý	k2eAgFnPc4d1	zřejmá
výhody	výhoda	k1gFnPc4	výhoda
kodexu	kodex	k1gInSc2	kodex
se	s	k7c7	s
svitky	svitek	k1gInPc7	svitek
běžně	běžně	k6eAd1	běžně
užívaly	užívat	k5eAaImAgInP	užívat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
středověk	středověk	k1gInSc4	středověk
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
uspořádání	uspořádání	k1gNnSc6	uspořádání
<g/>
,	,	kIx,	,
že	že	k8xS	že
sloupce	sloupec	k1gInPc1	sloupec
či	či	k8xC	či
"	"	kIx"	"
<g/>
stránky	stránka	k1gFnSc2	stránka
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgFnP	být
orientovány	orientovat	k5eAaBmNgFnP	orientovat
po	po	k7c6	po
délce	délka	k1gFnSc6	délka
svitku	svitek	k1gInSc2	svitek
a	a	k8xC	a
svitek	svitek	k1gInSc1	svitek
se	se	k3xPyFc4	se
odvíjel	odvíjet	k5eAaImAgInS	odvíjet
přes	přes	k7c4	přes
hranu	hrana	k1gFnSc4	hrana
pultu	pult	k1gInSc2	pult
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ortodoxní	ortodoxní	k2eAgFnSc6d1	ortodoxní
židovské	židovský	k2eAgFnSc6d1	židovská
bohoslužbě	bohoslužba	k1gFnSc6	bohoslužba
se	se	k3xPyFc4	se
svitek	svitek	k1gInSc1	svitek
dodnes	dodnes	k6eAd1	dodnes
užívá	užívat	k5eAaImIp3nS	užívat
pro	pro	k7c4	pro
předčítání	předčítání	k1gNnSc4	předčítání
Tóry	tóra	k1gFnSc2	tóra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
</s>
</p>
<p>
<s>
Kodex	kodex	k1gInSc1	kodex
</s>
</p>
<p>
<s>
Kumránské	Kumránský	k2eAgInPc1d1	Kumránský
svitky	svitek	k1gInPc1	svitek
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
svitek	svitek	k1gInSc1	svitek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Svitek	svitek	k1gInSc1	svitek
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
terminologické	terminologický	k2eAgFnSc6d1	terminologická
databázi	databáze	k1gFnSc6	databáze
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
a	a	k8xC	a
informační	informační	k2eAgFnSc2d1	informační
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
TDKIV	TDKIV	kA	TDKIV
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Scienceworld	Scienceworld	k1gInSc1	Scienceworld
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Svitek	svitek	k1gInSc1	svitek
<g/>
,	,	kIx,	,
kodex	kodex	k1gInSc1	kodex
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
R.	R.	kA	R.
Cartier	Cartier	k1gMnSc1	Cartier
<g/>
,	,	kIx,	,
Histoire	Histoir	k1gInSc5	Histoir
de	de	k?	de
la	la	k0	la
lecture	lectur	k1gMnSc5	lectur
dans	dans	k1gInSc1	dans
le	le	k?	le
monde	mond	k1gInSc5	mond
occidental	occidentat	k5eAaPmAgInS	occidentat
<g/>
.	.	kIx.	.
</s>
<s>
Paris	Paris	k1gMnSc1	Paris
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
Z.	Z.	kA	Z.
Tobolka	tobolka	k1gFnSc1	tobolka
<g/>
,	,	kIx,	,
Kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
</s>
</p>
