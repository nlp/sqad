<s>
Cha-cha-cha	Chahaha	k1gMnSc1	Cha-cha-cha
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
soutěžních	soutěžní	k2eAgInPc2d1	soutěžní
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
se	se	k3xPyFc4	se
zařadil	zařadit	k5eAaPmAgMnS	zařadit
jako	jako	k9	jako
poslední	poslední	k2eAgMnSc1d1	poslední
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
zjednodušené	zjednodušený	k2eAgFnSc6d1	zjednodušená
formě	forma	k1gFnSc6	forma
se	se	k3xPyFc4	se
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
také	také	k9	také
v	v	k7c6	v
tanečních	taneční	k2eAgInPc6d1	taneční
kurzech	kurz	k1gInPc6	kurz
<g/>
.	.	kIx.	.
</s>
