<s>
Bílá	bílý	k2eAgFnSc1d1
mensurální	mensurální	k2eAgFnSc1d1
notace	notace	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1
mensurální	mensurální	k2eAgFnSc1d1
notace	notace	k1gFnSc1
je	být	k5eAaImIp3nS
způsob	způsob	k1gInSc1
zápisu	zápis	k1gInSc2
not	nota	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
vznikl	vzniknout	k5eAaPmAgInS
přibližně	přibližně	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1430	#num#	k4
<g/>
–	–	k?
<g/>
1600	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
přibližně	přibližně	k6eAd1
1430	#num#	k4
<g/>
–	–	k?
<g/>
1600	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
odstranění	odstranění	k1gNnSc3
barevnosti	barevnost	k1gFnSc2
not	nota	k1gFnPc2
a	a	k8xC
ustálila	ustálit	k5eAaPmAgFnS
se	se	k3xPyFc4
podoba	podoba	k1gFnSc1
dvou	dva	k4xCgFnPc2
barev	barva	k1gFnPc2
-	-	kIx~
s	s	k7c7
černou	černý	k2eAgFnSc7d1
hlavičkou	hlavička	k1gFnSc7
(	(	kIx(
<g/>
vyplněnou	vyplněná	k1gFnSc7
<g/>
)	)	kIx)
a	a	k8xC
s	s	k7c7
bílou	bílý	k2eAgFnSc7d1
hlavičkou	hlavička	k1gFnSc7
(	(	kIx(
<g/>
nevyplněnou	vyplněný	k2eNgFnSc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nota	nota	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
mít	mít	k5eAaImF
oválný	oválný	k2eAgInSc1d1
tvar	tvar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
17	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
je	být	k5eAaImIp3nS
zápis	zápis	k1gInSc1
doplněn	doplnit	k5eAaPmNgInS
taktovou	taktový	k2eAgFnSc7d1
čarou	čára	k1gFnSc7
→	→	k?
používané	používaný	k2eAgInPc1d1
dodnes	dodnes	k6eAd1
(	(	kIx(
<g/>
s	s	k7c7
doplněním	doplnění	k1gNnSc7
dynamiky	dynamika	k1gFnSc2
a	a	k8xC
ostatních	ostatní	k2eAgFnPc2d1
značek	značka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
vynálezem	vynález	k1gInSc7
knihtisku	knihtisk	k1gInSc2
měly	mít	k5eAaImAgInP
chrámové	chrámový	k2eAgInPc1d1
sbory	sbor	k1gInPc1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
obvykle	obvykle	k6eAd1
pouze	pouze	k6eAd1
jediný	jediný	k2eAgInSc1d1
rukopisný	rukopisný	k2eAgInSc1d1
exemplář	exemplář	k1gInSc1
chorální	chorální	k2eAgFnSc2d1
skladby	skladba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
bylo	být	k5eAaImAgNnS
při	při	k7c6
rozšiřování	rozšiřování	k1gNnSc6
sborů	sbor	k1gInPc2
nutné	nutný	k2eAgNnSc1d1
psát	psát	k5eAaImF
stále	stále	k6eAd1
větší	veliký	k2eAgFnPc4d2
noty	nota	k1gFnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
je	on	k3xPp3gInPc4
všichni	všechen	k3xTgMnPc1
zpěváci	zpěvák	k1gMnPc1
ze	z	k7c2
sborníku	sborník	k1gInSc2
přečetli	přečíst	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
úspoře	úspora	k1gFnSc3
času	čas	k1gInSc2
i	i	k8xC
inkoustu	inkoust	k1gInSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
psaly	psát	k5eAaImAgInP
pouze	pouze	k6eAd1
obrysy	obrys	k1gInPc1
not	nota	k1gFnPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
vznikly	vzniknout	k5eAaPmAgFnP
bílé	bílý	k2eAgFnPc1d1
<g/>
,	,	kIx,
„	„	k?
<g/>
duté	dutý	k2eAgFnSc2d1
<g/>
“	“	k?
noty	nota	k1gFnSc2
(	(	kIx(
<g/>
podobné	podobný	k2eAgNnSc1d1
celým	celý	k2eAgFnPc3d1
či	či	k8xC
půlovým	půlový	k2eAgFnPc3d1
notám	nota	k1gFnPc3
dnešní	dnešní	k2eAgFnSc2d1
notace	notace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
pro	pro	k7c4
přechod	přechod	k1gInSc4
k	k	k7c3
bílé	bílý	k2eAgFnSc3d1
notaci	notace	k1gFnSc3
byl	být	k5eAaImAgMnS
nahrazení	nahrazení	k1gNnSc4
silných	silný	k2eAgInPc2d1
pergamenů	pergamen	k1gInPc2
tenkým	tenký	k2eAgInSc7d1
papírem	papír	k1gInSc7
v	v	k7c6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
se	se	k3xPyFc4
inkoust	inkoust	k1gInSc1
při	při	k7c6
vyplňování	vyplňování	k1gNnSc6
not	nota	k1gFnPc2
propíjel	propíjet	k5eAaImAgInS
skrz	skrz	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP
také	také	k9
nové	nový	k2eAgFnPc1d1
noty	nota	k1gFnPc1
s	s	k7c7
menší	malý	k2eAgFnSc7d2
hodnotou	hodnota	k1gFnSc7
<g/>
,	,	kIx,
takže	takže	k8xS
notová	notový	k2eAgFnSc1d1
zásoba	zásoba	k1gFnSc1
nyní	nyní	k6eAd1
vypadala	vypadat	k5eAaPmAgFnS,k5eAaImAgFnS
následovně	následovně	k6eAd1
<g/>
:	:	kIx,
Notové	notový	k2eAgFnSc2d1
značky	značka	k1gFnSc2
bílé	bílý	k2eAgFnSc2d1
mensurální	mensurální	k2eAgFnSc2d1
notace	notace	k1gFnSc2
</s>
<s>
Maxima	Maxima	k1gFnSc1
</s>
<s>
Longa	Longa	k1gFnSc1
</s>
<s>
Brevis	Brevis	k1gFnSc1
</s>
<s>
Semibrevis	Semibrevis	k1gFnSc1
</s>
<s>
Minima	minimum	k1gNnPc1
</s>
<s>
Semiminima	Semiminima	k1gFnSc1
</s>
<s>
Fusa	Fusa	k1gFnSc1
neboli	neboli	k8xC
chroma	chroma	k1gFnSc1
a	a	k8xC
také	také	k9
semifusa	semifus	k1gMnSc4
čili	čili	k8xC
semichroma	semichrom	k1gMnSc4
</s>
<s>
Podobná	podobný	k2eAgNnPc1d1
hesla	heslo	k1gNnPc1
</s>
<s>
Mensurální	mensurální	k2eAgFnSc1d1
notace	notace	k1gFnSc1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
mensurální	mensurální	k2eAgFnSc1d1
notace	notace	k1gFnSc1
</s>
<s>
Franko	franko	k6eAd1
Kolínský	kolínský	k2eAgInSc1d1
</s>
<s>
Neumová	neumový	k2eAgFnSc1d1,k2eNgFnSc1d1
notace	notace	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Středověk	středověk	k1gInSc1
|	|	kIx~
hudba	hudba	k1gFnSc1
</s>
