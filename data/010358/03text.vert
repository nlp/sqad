<p>
<s>
Znělá	znělý	k2eAgFnSc1d1	znělá
palatální	palatální	k2eAgFnSc1d1	palatální
frikativa	frikativa	k1gFnSc1	frikativa
je	být	k5eAaImIp3nS	být
souhláska	souhláska	k1gFnSc1	souhláska
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
nikoliv	nikoliv	k9	nikoliv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
znělým	znělý	k2eAgInSc7d1	znělý
protějškem	protějšek	k1gInSc7	protějšek
je	být	k5eAaImIp3nS	být
neznělá	znělý	k2eNgFnSc1d1	neznělá
palatální	palatální	k2eAgFnSc1d1	palatální
frikativa	frikativa	k1gFnSc1	frikativa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
IPA	IPA	kA	IPA
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
jako	jako	k9	jako
ʝ	ʝ	k?	ʝ
<g/>
,	,	kIx,	,
v	v	k7c6	v
SAMPA	SAMPA	kA	SAMPA
jako	jako	k8xS	jako
j	j	k?	j
<g/>
\	\	kIx~	\
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Způsob	způsob	k1gInSc1	způsob
artikulace	artikulace	k1gFnSc2	artikulace
<g/>
:	:	kIx,	:
třená	třený	k2eAgFnSc1d1	třená
souhláska	souhláska	k1gFnSc1	souhláska
(	(	kIx(	(
<g/>
frikativa	frikativa	k1gFnSc1	frikativa
<g/>
)	)	kIx)	)
patřící	patřící	k2eAgFnSc1d1	patřící
mezi	mezi	k7c4	mezi
sykavky	sykavka	k1gFnPc4	sykavka
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
úžiny	úžina	k1gFnSc2	úžina
(	(	kIx(	(
<g/>
konstrikce	konstrikce	k1gFnSc1	konstrikce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
staví	stavit	k5eAaBmIp3nS	stavit
do	do	k7c2	do
proudu	proud	k1gInSc2	proud
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
šum	šum	k1gInSc1	šum
-	-	kIx~	-
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
též	též	k6eAd1	též
označení	označení	k1gNnSc4	označení
úžinová	úžinový	k2eAgFnSc1d1	úžinová
souhláska	souhláska	k1gFnSc1	souhláska
(	(	kIx(	(
<g/>
konstriktiva	konstriktiva	k1gFnSc1	konstriktiva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Místo	místo	k7c2	místo
artikulace	artikulace	k1gFnSc2	artikulace
<g/>
:	:	kIx,	:
středopatrová	středopatrový	k2eAgFnSc1d1	středopatrový
souhláska	souhláska	k1gFnSc1	souhláska
(	(	kIx(	(
<g/>
palatála	palatála	k1gFnSc1	palatála
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uzávěra	uzávěra	k1gFnSc1	uzávěra
se	se	k3xPyFc4	se
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
mezi	mezi	k7c7	mezi
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
tvrdým	tvrdý	k2eAgNnSc7d1	tvrdé
patrem	patro	k1gNnSc7	patro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znělost	znělost	k1gFnSc1	znělost
<g/>
:	:	kIx,	:
znělá	znělý	k2eAgFnSc1d1	znělá
souhláska	souhláska	k1gFnSc1	souhláska
-	-	kIx~	-
při	při	k7c6	při
artikulaci	artikulace	k1gFnSc6	artikulace
hlasivky	hlasivka	k1gFnSc2	hlasivka
kmitají	kmitat	k5eAaImIp3nP	kmitat
<g/>
.	.	kIx.	.
</s>
<s>
Neznělým	znělý	k2eNgInSc7d1	neznělý
protějškem	protějšek	k1gInSc7	protějšek
je	být	k5eAaImIp3nS	být
c.	c.	k?	c.
</s>
</p>
<p>
<s>
Ústní	ústní	k2eAgFnSc1d1	ústní
souhláska	souhláska	k1gFnSc1	souhláska
-	-	kIx~	-
vzduch	vzduch	k1gInSc1	vzduch
prochází	procházet	k5eAaImIp3nS	procházet
při	při	k7c6	při
artikulaci	artikulace	k1gFnSc6	artikulace
ústní	ústní	k2eAgFnSc7d1	ústní
dutinou	dutina	k1gFnSc7	dutina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Středová	středový	k2eAgFnSc1d1	středová
souhláska	souhláska	k1gFnSc1	souhláska
-	-	kIx~	-
vzduch	vzduch	k1gInSc1	vzduch
proudí	proudit	k5eAaPmIp3nS	proudit
převážně	převážně	k6eAd1	převážně
přes	přes	k7c4	přes
střed	střed	k1gInSc4	střed
jazyka	jazyk	k1gInSc2	jazyk
spíše	spíše	k9	spíše
než	než	k8xS	než
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gInPc4	jeho
boky	bok	k1gInPc4	bok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pulmonická	Pulmonický	k2eAgFnSc1d1	Pulmonický
egresivní	egresivní	k2eAgFnSc1d1	egresivní
hláska	hláska	k1gFnSc1	hláska
-	-	kIx~	-
vzduch	vzduch	k1gInSc1	vzduch
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
artikulaci	artikulace	k1gFnSc6	artikulace
vytlačován	vytlačován	k2eAgInSc4d1	vytlačován
z	z	k7c2	z
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nizozemštině	nizozemština	k1gFnSc6	nizozemština
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
jako	jako	k9	jako
j	j	k?	j
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ja	ja	k?	ja
-	-	kIx~	-
ano	ano	k9	ano
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
novořečtině	novořečtina	k1gFnSc6	novořečtina
spřežkou	spřežka	k1gFnSc7	spřežka
γ	γ	k?	γ
<g/>
,	,	kIx,	,
ve	v	k7c6	v
švédštině	švédština	k1gFnSc6	švédština
jako	jako	k8xC	jako
j	j	k?	j
(	(	kIx(	(
<g/>
jord	jord	k1gMnSc1	jord
-	-	kIx~	-
hlína	hlína	k1gFnSc1	hlína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
někdy	někdy	k6eAd1	někdy
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
písmeno	písmeno	k1gNnSc1	písmeno
y	y	k?	y
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sayo	sayo	k1gNnSc4	sayo
-	-	kIx~	-
šaty	šat	k1gInPc4	šat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
