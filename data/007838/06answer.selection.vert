<s>
Původní	původní	k2eAgInSc1d1	původní
seriál	seriál	k1gInSc1	seriál
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k9	jako
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Original	Original	k1gMnSc1	Original
Series	Series	k1gMnSc1	Series
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zkratkou	zkratka	k1gFnSc7	zkratka
TOS	TOS	kA	TOS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
celou	celý	k2eAgFnSc4d1	celá
existenci	existence	k1gFnSc4	existence
fikčního	fikční	k2eAgInSc2d1	fikční
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
