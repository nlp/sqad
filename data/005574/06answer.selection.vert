<s>
Lamanšský	lamanšský	k2eAgInSc1d1	lamanšský
průliv	průliv	k1gInSc1	průliv
neboli	neboli	k8xC	neboli
kanál	kanál	k1gInSc1	kanál
La	la	k1gNnSc2	la
Manche	Manch	k1gInSc2	Manch
(	(	kIx(	(
<g/>
též	též	k9	též
Anglický	anglický	k2eAgInSc1d1	anglický
kanál	kanál	k1gInSc1	kanál
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
také	také	k9	také
Britský	britský	k2eAgInSc1d1	britský
průplav	průplav	k1gInSc1	průplav
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
ostrov	ostrov	k1gInSc4	ostrov
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
