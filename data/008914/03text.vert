<p>
<s>
Steven	Steven	k2eAgMnSc1d1	Steven
Allan	Allan	k1gMnSc1	Allan
Spielberg	Spielberg	k1gMnSc1	Spielberg
<g/>
,	,	kIx,	,
KBE	KBE	kA	KBE
<g/>
,	,	kIx,	,
OMRI	OMRI	kA	OMRI
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1946	[number]	k4	1946
Cincinnati	Cincinnati	k1gFnPc2	Cincinnati
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
komerčně	komerčně	k6eAd1	komerčně
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejpopulárnějším	populární	k2eAgMnPc3d3	nejpopulárnější
režisérům	režisér	k1gMnPc3	režisér
filmové	filmový	k2eAgFnSc2d1	filmová
historie	historie	k1gFnSc2	historie
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gInPc4	jeho
filmy	film	k1gInPc4	film
Čelisti	čelist	k1gFnSc2	čelist
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
E.T.	E.T.	k1gMnSc1	E.T.
–	–	k?	–
Mimozemšťan	mimozemšťan	k1gMnSc1	mimozemšťan
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
kasovní	kasovní	k2eAgInPc1d1	kasovní
rekordy	rekord	k1gInPc1	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
produkční	produkční	k2eAgFnSc2d1	produkční
společnosti	společnost	k1gFnSc2	společnost
DreamWorks	DreamWorksa	k1gFnPc2	DreamWorksa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
natočil	natočit	k5eAaBmAgMnS	natočit
mnoho	mnoho	k4c1	mnoho
filmů	film	k1gInPc2	film
různých	různý	k2eAgInPc2d1	různý
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Spielbergovy	Spielbergův	k2eAgFnPc1d1	Spielbergova
rané	raný	k2eAgFnPc1d1	raná
sci-fi	scii	k1gFnPc1	sci-fi
a	a	k8xC	a
dobrodružné	dobrodružný	k2eAgInPc1d1	dobrodružný
filmy	film	k1gInPc1	film
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
staly	stát	k5eAaPmAgFnP	stát
typickými	typický	k2eAgInPc7d1	typický
hollywoodskými	hollywoodský	k2eAgInPc7d1	hollywoodský
trháky	trhák	k1gInPc7	trhák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
filmy	film	k1gInPc1	film
začaly	začít	k5eAaPmAgInP	začít
zabývat	zabývat	k5eAaImF	zabývat
humanistickými	humanistický	k2eAgFnPc7d1	humanistická
otázkami	otázka	k1gFnPc7	otázka
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
holokaust	holokaust	k1gInSc1	holokaust
(	(	kIx(	(
<g/>
Schindlerův	Schindlerův	k2eAgInSc1d1	Schindlerův
seznam	seznam	k1gInSc1	seznam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
transatlantického	transatlantický	k2eAgInSc2d1	transatlantický
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
(	(	kIx(	(
<g/>
Amistad	Amistad	k1gInSc4	Amistad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
válkou	válka	k1gFnSc7	válka
(	(	kIx(	(
<g/>
Říše	říše	k1gFnSc1	říše
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
Zachraňte	zachránit	k5eAaPmRp2nP	zachránit
vojína	vojín	k1gMnSc4	vojín
Ryana	Ryan	k1gMnSc4	Ryan
<g/>
,	,	kIx,	,
Válečný	válečný	k2eAgMnSc1d1	válečný
kůň	kůň	k1gMnSc1	kůň
a	a	k8xC	a
Most	most	k1gInSc1	most
špionů	špion	k1gMnPc2	špion
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
terorismem	terorismus	k1gInSc7	terorismus
(	(	kIx(	(
<g/>
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
další	další	k2eAgInPc4d1	další
známé	známý	k2eAgInPc4d1	známý
projekty	projekt	k1gInPc4	projekt
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
série	série	k1gFnSc1	série
filmů	film	k1gInPc2	film
s	s	k7c7	s
Indiana	Indiana	k1gFnSc1	Indiana
Jonesem	Jones	k1gInSc7	Jones
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
a	a	k8xC	a
raná	raný	k2eAgFnSc1d1	raná
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
na	na	k7c6	na
předměstích	předměstí	k1gNnPc6	předměstí
Haddonfieldu	Haddonfield	k1gInSc2	Haddonfield
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
a	a	k8xC	a
Scottsdalu	Scottsdal	k1gInSc2	Scottsdal
v	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
<g/>
.	.	kIx.	.
</s>
<s>
Spielbergova	Spielbergův	k2eAgNnPc1d1	Spielbergovo
díla	dílo	k1gNnPc1	dílo
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
kinematografie	kinematografie	k1gFnSc2	kinematografie
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
stránku	stránka	k1gFnSc4	stránka
–	–	k?	–
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
nesporně	sporně	k6eNd1	sporně
komerčním	komerční	k2eAgMnSc7d1	komerční
režisérem	režisér	k1gMnSc7	režisér
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gInPc4	jeho
tvorba	tvorba	k1gFnSc1	tvorba
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
vysoký	vysoký	k2eAgInSc1d1	vysoký
filmařský	filmařský	k2eAgInSc1d1	filmařský
standard	standard	k1gInSc1	standard
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměřuje	zaměřovat	k5eNaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
filmový	filmový	k2eAgInSc4d1	filmový
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInPc1	jeho
filmy	film	k1gInPc1	film
sahají	sahat	k5eAaImIp3nP	sahat
od	od	k7c2	od
sci-fi	scii	k1gFnSc2	sci-fi
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc2	komedie
<g/>
,	,	kIx,	,
dramatu	drama	k1gNnSc2	drama
až	až	k9	až
po	po	k7c4	po
horor	horor	k1gInSc4	horor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
upoutal	upoutat	k5eAaPmAgMnS	upoutat
pozornost	pozornost	k1gFnSc4	pozornost
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Čelisti	čelist	k1gFnSc2	čelist
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
další	další	k2eAgInPc4d1	další
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
filmy	film	k1gInPc4	film
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
série	série	k1gFnSc1	série
filmů	film	k1gInPc2	film
s	s	k7c7	s
Indianou	Indiana	k1gFnSc7	Indiana
Jonesem	Jones	k1gMnSc7	Jones
<g/>
,	,	kIx,	,
série	série	k1gFnPc4	série
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Blízká	blízký	k2eAgNnPc1d1	blízké
setkání	setkání	k1gNnPc1	setkání
třetího	třetí	k4xOgMnSc2	třetí
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
E.T.	E.T.	k1gMnSc1	E.T.
–	–	k?	–
Mimozemšťan	mimozemšťan	k1gMnSc1	mimozemšťan
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oscara	Oscara	k1gFnSc1	Oscara
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
získal	získat	k5eAaPmAgInS	získat
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
za	za	k7c4	za
snímek	snímek	k1gInSc4	snímek
Schindlerův	Schindlerův	k2eAgInSc4d1	Schindlerův
seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
také	také	k9	také
obdržel	obdržet	k5eAaPmAgMnS	obdržet
ocenění	ocenění	k1gNnSc4	ocenění
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Dalšího	další	k2eAgMnSc4d1	další
Oscara	Oscar	k1gMnSc4	Oscar
obdržel	obdržet	k5eAaPmAgMnS	obdržet
za	za	k7c4	za
režii	režie	k1gFnSc4	režie
filmu	film	k1gInSc2	film
Zachraňte	zachránit	k5eAaPmRp2nP	zachránit
vojína	vojín	k1gMnSc4	vojín
Ryana	Ryan	k1gMnSc4	Ryan
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
i	i	k8xC	i
jiným	jiný	k2eAgNnPc3d1	jiné
odvětvím	odvětví	k1gNnPc3	odvětví
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
producentem	producent	k1gMnSc7	producent
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
zakladatelem	zakladatel	k1gMnSc7	zakladatel
společnosti	společnost	k1gFnSc2	společnost
DreamWorks	DreamWorksa	k1gFnPc2	DreamWorksa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
DreamWorks	DreamWorks	k1gInSc4	DreamWorks
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
založil	založit	k5eAaPmAgInS	založit
společně	společně	k6eAd1	společně
s	s	k7c7	s
hudebním	hudební	k2eAgMnSc7d1	hudební
producentem	producent	k1gMnSc7	producent
Davidem	David	k1gMnSc7	David
Geffenem	Geffen	k1gMnSc7	Geffen
a	a	k8xC	a
bývalým	bývalý	k2eAgMnSc7d1	bývalý
Disneyho	Disneyha	k1gFnSc5	Disneyha
manažerem	manažer	k1gMnSc7	manažer
Jeffreym	Jeffreym	k1gInSc1	Jeffreym
Katzenbergem	Katzenberg	k1gInSc7	Katzenberg
společnost	společnost	k1gFnSc1	společnost
Dream	Dream	k1gInSc1	Dream
Works	Works	kA	Works
SKG	SKG	kA	SKG
<g/>
;	;	kIx,	;
založili	založit	k5eAaPmAgMnP	založit
tak	tak	k9	tak
čistě	čistě	k6eAd1	čistě
filmovou	filmový	k2eAgFnSc4d1	filmová
produkční	produkční	k2eAgFnSc4d1	produkční
společnost	společnost	k1gFnSc4	společnost
s	s	k7c7	s
kapitálovým	kapitálový	k2eAgNnSc7d1	kapitálové
vybavením	vybavení	k1gNnSc7	vybavení
odpovídajícím	odpovídající	k2eAgInSc6d1	odpovídající
rozsahu	rozsah	k1gInSc6	rozsah
koncernu	koncern	k1gInSc2	koncern
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
stávající	stávající	k2eAgNnPc1d1	stávající
studia	studio	k1gNnPc1	studio
zanikala	zanikat	k5eAaImAgNnP	zanikat
v	v	k7c6	v
nadnárodních	nadnárodní	k2eAgInPc6d1	nadnárodní
koncernech	koncern	k1gInPc6	koncern
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
firma	firma	k1gFnSc1	firma
s	s	k7c7	s
těžištěm	těžiště	k1gNnSc7	těžiště
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
animovaného	animovaný	k2eAgInSc2d1	animovaný
filmu	film	k1gInSc2	film
hodlala	hodlat	k5eAaImAgFnS	hodlat
tématicky	tématicky	k6eAd1	tématicky
navázat	navázat	k5eAaPmF	navázat
na	na	k7c4	na
někdejší	někdejší	k2eAgFnSc4d1	někdejší
Disneyho	Disney	k1gMnSc2	Disney
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
filmů	film	k1gInPc2	film
Princ	princ	k1gMnSc1	princ
egyptský	egyptský	k2eAgMnSc1d1	egyptský
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slepičí	slepičí	k2eAgInSc1d1	slepičí
úlet	úlet	k1gInSc1	úlet
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
Shrek	Shrek	k6eAd1	Shrek
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
trikových	trikový	k2eAgInPc2d1	trikový
filmů	film	k1gInPc2	film
je	být	k5eAaImIp3nS	být
DreamWorks	DreamWorks	k1gInSc4	DreamWorks
díky	díky	k7c3	díky
projektům	projekt	k1gInPc3	projekt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
akční	akční	k2eAgMnSc1d1	akční
Peacemaker	Peacemaker	k1gMnSc1	Peacemaker
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
filmem	film	k1gInSc7	film
Woodyho	Woody	k1gMnSc2	Woody
Allena	Allen	k1gMnSc2	Allen
Hollywood	Hollywood	k1gInSc1	Hollywood
v	v	k7c6	v
koncích	konec	k1gInPc6	konec
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
momentálně	momentálně	k6eAd1	momentálně
aktivní	aktivní	k2eAgFnSc7d1	aktivní
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
filmových	filmový	k2eAgInPc6d1	filmový
žánrech	žánr	k1gInPc6	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
firemním	firemní	k2eAgInSc6d1	firemní
logu	log	k1gInSc6	log
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazen	k2eAgMnSc1d1	vyobrazen
chlapec	chlapec	k1gMnSc1	chlapec
jedoucí	jedoucí	k2eAgMnSc1d1	jedoucí
na	na	k7c6	na
půlměsíci	půlměsíc	k1gInSc6	půlměsíc
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
vhazuje	vhazovat	k5eAaImIp3nS	vhazovat
udici	udice	k1gFnSc4	udice
do	do	k7c2	do
rybníka	rybník	k1gInSc2	rybník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Daniel	Daniel	k1gMnSc1	Daniel
Kotheschule	Kotheschule	k1gFnSc2	Kotheschule
<g/>
,	,	kIx,	,
Frankfurter	Frankfurter	k1gMnSc1	Frankfurter
Rundschau	Rundschaus	k1gInSc2	Rundschaus
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
===	===	k?	===
Celkový	celkový	k2eAgInSc1d1	celkový
přínos	přínos	k1gInSc1	přínos
===	===	k?	===
</s>
</p>
<p>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
výdělek	výdělek	k1gInSc1	výdělek
Spielbergových	Spielbergův	k2eAgInPc2d1	Spielbergův
filmů	film	k1gInPc2	film
činí	činit	k5eAaImIp3nS	činit
15	[number]	k4	15
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
komerčně	komerčně	k6eAd1	komerčně
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
režisérem	režisér	k1gMnSc7	režisér
filmové	filmový	k2eAgFnSc2d1	filmová
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nadace	nadace	k1gFnSc2	nadace
===	===	k?	===
</s>
</p>
<p>
<s>
Příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
úspěchu	úspěch	k1gInSc2	úspěch
filmu	film	k1gInSc2	film
Schindlerův	Schindlerův	k2eAgInSc1d1	Schindlerův
seznam	seznam	k1gInSc1	seznam
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
kapitálový	kapitálový	k2eAgInSc4d1	kapitálový
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
americkou	americký	k2eAgFnSc4d1	americká
nadaci	nadace	k1gFnSc4	nadace
Survivors	Survivorsa	k1gFnPc2	Survivorsa
of	of	k?	of
the	the	k?	the
Shoah	Shoah	k1gMnSc1	Shoah
Visual	Visual	k1gMnSc1	Visual
History	Histor	k1gMnPc4	Histor
Foundation	Foundation	k1gInSc4	Foundation
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
nadace	nadace	k1gFnSc2	nadace
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
co	co	k3yQnSc4	co
možná	možná	k9	možná
nejkratší	krátký	k2eAgFnSc6d3	nejkratší
době	doba	k1gFnSc6	doba
provést	provést	k5eAaPmF	provést
a	a	k8xC	a
veřejně	veřejně	k6eAd1	veřejně
zpřístupnit	zpřístupnit	k5eAaPmF	zpřístupnit
co	co	k3yInSc4	co
možná	možná	k9	možná
nejvíce	hodně	k6eAd3	hodně
interview	interview	k1gNnSc4	interview
s	s	k7c7	s
vysoce	vysoce	k6eAd1	vysoce
nadanými	nadaný	k2eAgInPc7d1	nadaný
přeživšími	přeživší	k2eAgInPc7d1	přeživší
pamětníky	pamětník	k1gInPc7	pamětník
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
standardizovaná	standardizovaný	k2eAgFnSc1d1	standardizovaná
metoda	metoda	k1gFnSc1	metoda
a	a	k8xC	a
montáž	montáž	k1gFnSc1	montáž
těchto	tento	k3xDgNnPc2	tento
interview	interview	k1gNnPc2	interview
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
hranému	hraný	k2eAgInSc3d1	hraný
filmu	film	k1gInSc3	film
se	s	k7c7	s
stereotypním	stereotypní	k2eAgInSc7d1	stereotypní
záběrem	záběr	k1gInSc7	záběr
dotazovaných	dotazovaný	k2eAgMnPc2d1	dotazovaný
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
jejich	jejich	k3xOp3gFnSc2	jejich
rodiny	rodina	k1gFnSc2	rodina
představují	představovat	k5eAaImIp3nP	představovat
hlavní	hlavní	k2eAgInPc4d1	hlavní
kritické	kritický	k2eAgInPc4d1	kritický
body	bod	k1gInPc4	bod
práce	práce	k1gFnSc2	práce
nadace	nadace	k1gFnSc2	nadace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Aktivita	aktivita	k1gFnSc1	aktivita
nadace	nadace	k1gFnSc2	nadace
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
kromě	kromě	k7c2	kromě
archivu	archiv	k1gInSc2	archiv
také	také	k9	také
publikování	publikování	k1gNnSc4	publikování
na	na	k7c4	na
CD-ROM	CD-ROM	k1gFnPc4	CD-ROM
a	a	k8xC	a
různé	různý	k2eAgFnPc1d1	různá
televizní	televizní	k2eAgFnPc1d1	televizní
produkce	produkce	k1gFnPc1	produkce
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
realizované	realizovaný	k2eAgNnSc1d1	realizované
významnými	významný	k2eAgMnPc7d1	významný
režiséry	režisér	k1gMnPc7	režisér
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Andrzej	Andrzej	k1gMnSc1	Andrzej
Wajda	Wajda	k1gMnSc1	Wajda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Režijní	režijní	k2eAgFnSc2d1	režijní
filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Oscar	Oscar	k1gInSc1	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
za	za	k7c4	za
filmy	film	k1gInPc4	film
Schindlerův	Schindlerův	k2eAgInSc1d1	Schindlerův
seznam	seznam	k1gInSc1	seznam
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zachraňte	zachránit	k5eAaPmRp2nP	zachránit
vojína	vojín	k1gMnSc4	vojín
Ryana	Ryan	k1gMnSc4	Ryan
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
glóbus	glóbus	k1gInSc1	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
za	za	k7c4	za
filmy	film	k1gInPc4	film
Schindlerův	Schindlerův	k2eAgInSc1d1	Schindlerův
seznam	seznam	k1gInSc1	seznam
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zachraňte	zachránit	k5eAaPmRp2nP	zachránit
vojína	vojín	k1gMnSc4	vojín
Ryana	Ryan	k1gMnSc4	Ryan
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
BAFTA	BAFTA	kA	BAFTA
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
a	a	k8xC	a
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
za	za	k7c4	za
film	film	k1gInSc4	film
Schindlerův	Schindlerův	k2eAgInSc1d1	Schindlerův
seznam	seznam	k1gInSc1	seznam
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
Cecila	Cecil	k1gMnSc2	Cecil
B.	B.	kA	B.
DeMilla	DeMilla	k1gMnSc1	DeMilla
–	–	k?	–
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
Čestný	čestný	k2eAgMnSc1d1	čestný
César	César	k1gMnSc1	César
–	–	k?	–
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Steven	Steven	k2eAgMnSc1d1	Steven
Spielberg	Spielberg	k1gMnSc1	Spielberg
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Philip	Philip	k1gMnSc1	Philip
M.	M.	kA	M.
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
,	,	kIx,	,
Steven	Steven	k2eAgMnSc1d1	Steven
Spielberg	Spielberg	k1gMnSc1	Spielberg
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Jota	jota	k1gNnSc1	jota
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Steven	Steven	k2eAgInSc4d1	Steven
Spielberg	Spielberg	k1gInSc4	Spielberg
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Steven	Stevna	k1gFnPc2	Stevna
Spielberg	Spielberg	k1gMnSc1	Spielberg
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Steven	Steven	k2eAgMnSc1d1	Steven
Spielberg	Spielberg	k1gMnSc1	Spielberg
</s>
</p>
<p>
<s>
Steven	Steven	k2eAgMnSc1d1	Steven
Spielberg	Spielberg	k1gMnSc1	Spielberg
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Steven	Steven	k2eAgMnSc1d1	Steven
Spielberg	Spielberg	k1gMnSc1	Spielberg
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Steven	Steven	k2eAgMnSc1d1	Steven
Spielberg	Spielberg	k1gMnSc1	Spielberg
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Steven	Steven	k2eAgMnSc1d1	Steven
Spielberg	Spielberg	k1gMnSc1	Spielberg
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
