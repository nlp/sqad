<s>
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
komerčně	komerčně	k6eAd1	komerčně
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
?	?	kIx.	?
</s>
