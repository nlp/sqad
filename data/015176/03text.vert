<s>
Knut	knuta	k1gFnPc2
Nordahl	Nordahl	k1gFnSc2
</s>
<s>
Knut	knuta	k1gFnPc2
Nordahl	Nordahl	k1gFnSc2
Knut	knuta	k1gFnPc2
Nordahl	Nordahl	k1gFnSc2
s	s	k7c7
cenou	cena	k1gFnSc7
pro	pro	k7c4
švédského	švédský	k2eAgMnSc4d1
fotbalistu	fotbalista	k1gMnSc4
rokuOsobní	rokuOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Celé	celý	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Knut	knuta	k1gFnPc2
Erik	Erika	k1gFnPc2
Alexander	Alexandra	k1gFnPc2
Nordahl	Nordahl	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1920	#num#	k4
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Hörnefors	Hörnefors	k1gInSc1
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
Datum	datum	k1gNnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1984	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
64	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
Föllinge	Föllinge	k1gNnSc1
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
</s>
<s>
Konec	konec	k1gInSc1
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
Profesionální	profesionální	k2eAgFnSc2d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
1941	#num#	k4
<g/>
–	–	k?
<g/>
19491950	#num#	k4
<g/>
–	–	k?
<g/>
19521952	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
IFK	IFK	kA
Norrköping	Norrköping	k1gInSc1
AS	as	k9
Řím	Řím	k1gInSc1
Degerfors	Degerforsa	k1gFnPc2
IF	IF	kA
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
<g/>
**	**	k?
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
Švédsko	Švédsko	k1gNnSc4
<g/>
26	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Fotbal	fotbal	k1gInSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
LOH	LOH	kA
1948	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
</s>
<s>
MS	MS	kA
1950	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Příbuzenstvo	příbuzenstvo	k1gNnSc1
</s>
<s>
bratr	bratr	k1gMnSc1
</s>
<s>
Bertil	Bertit	k5eAaPmAgMnS,k5eAaImAgMnS
Nordahl	Nordahl	k1gMnSc1
-	-	kIx~
fotbalista	fotbalista	k1gMnSc1
</s>
<s>
bratr	bratr	k1gMnSc1
</s>
<s>
Gunnar	Gunnar	k1gMnSc1
Nordahl	Nordahl	k1gMnSc1
-	-	kIx~
fotbalista	fotbalista	k1gMnSc1
</s>
<s>
bratr	bratr	k1gMnSc1
</s>
<s>
Gösta	Gösta	k1gMnSc1
Nordahl	Nordahl	k1gMnSc1
-	-	kIx~
fotbalista	fotbalista	k1gMnSc1
</s>
<s>
bratr	bratr	k1gMnSc1
</s>
<s>
Göran	Göran	k1gMnSc1
Nordahl	Nordahl	k1gMnSc1
-	-	kIx~
fotbalista	fotbalista	k1gMnSc1
</s>
<s>
Knut	Knut	k1gMnSc1
Erik	Erik	k1gMnSc1
Alexander	Alexander	k1gMnSc1
Nordahl	Nordahl	k1gMnSc1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1920	#num#	k4
Hörnefors	Hörnefors	k1gInSc1
—	—	k?
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1984	#num#	k4
Föllinge	Föllinge	k1gNnSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
švédský	švédský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
<g/>
,	,	kIx,
hrající	hrající	k2eAgMnSc1d1
na	na	k7c6
postu	post	k1gInSc6
obránce	obránce	k1gMnSc2
nebo	nebo	k8xC
záložníka	záložník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
olympijským	olympijský	k2eAgMnSc7d1
vítězem	vítěz	k1gMnSc7
z	z	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
pětice	pětice	k1gFnSc2
bratrů	bratr	k1gMnPc2
ze	z	k7c2
severošvédské	severošvédský	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
Hörneforsu	Hörnefors	k1gInSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
všichni	všechen	k3xTgMnPc1
hráli	hrát	k5eAaImAgMnP
fotbal	fotbal	k1gInSc4
<g/>
:	:	kIx,
Bertil	Bertil	k1gMnSc1
Nordahl	Nordahl	k1gMnSc1
a	a	k8xC
Gunnar	Gunnar	k1gMnSc1
Nordahl	Nordahl	k1gMnSc1
byli	být	k5eAaImAgMnP
rovněž	rovněž	k9
v	v	k7c6
mužstvu	mužstvo	k1gNnSc6
olympijských	olympijský	k2eAgMnPc2d1
vítězů	vítěz	k1gMnPc2
<g/>
,	,	kIx,
nejmladší	mladý	k2eAgNnPc4d3
dvojčata	dvojče	k1gNnPc4
Gösta	Gösto	k1gNnSc2
Nordahl	Nordahl	k1gFnPc2
a	a	k8xC
Göran	Görana	k1gFnPc2
Nordahl	Nordahl	k1gFnSc2
působila	působit	k5eAaImAgFnS
jen	jen	k9
na	na	k7c6
ligové	ligový	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
S	s	k7c7
klubem	klub	k1gInSc7
IFK	IFK	kA
Norrköping	Norrköping	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
pětkrát	pětkrát	k6eAd1
mistrem	mistr	k1gMnSc7
Švédska	Švédsko	k1gNnSc2
(	(	kIx(
<g/>
1943	#num#	k4
<g/>
,	,	kIx,
1945	#num#	k4
<g/>
,	,	kIx,
1946	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
a	a	k8xC
1948	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
dvakrát	dvakrát	k6eAd1
vítězem	vítěz	k1gMnSc7
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
1943	#num#	k4
a	a	k8xC
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
obdržel	obdržet	k5eAaPmAgMnS
jako	jako	k9
třetí	třetí	k4xOgMnSc1
z	z	k7c2
bratrů	bratr	k1gMnPc2
v	v	k7c6
řadě	řada	k1gFnSc6
cenu	cena	k1gFnSc4
pro	pro	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
švédského	švédský	k2eAgMnSc4d1
hráče	hráč	k1gMnSc4
roku	rok	k1gInSc2
Guldbollen	Guldbollen	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zatímco	zatímco	k8xS
Bertil	Bertil	k1gFnSc4
a	a	k8xC
Gunnar	Gunnar	k1gInSc4
odešli	odejít	k5eAaPmAgMnP
po	po	k7c6
olympiádě	olympiáda	k1gFnSc6
do	do	k7c2
profesionální	profesionální	k2eAgFnSc2d1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
a	a	k8xC
byli	být	k5eAaImAgMnP
tak	tak	k6eAd1
vyřazeni	vyřadit	k5eAaPmNgMnP
z	z	k7c2
reprezentace	reprezentace	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
vyžadoval	vyžadovat	k5eAaImAgInS
amatérský	amatérský	k2eAgInSc1d1
status	status	k1gInSc1
<g/>
,	,	kIx,
Knut	knuta	k1gFnPc2
nastoupil	nastoupit	k5eAaPmAgInS
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
1950	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgNnSc6,k3yIgNnSc6,k3yRgNnSc6
odehrál	odehrát	k5eAaPmAgInS
tři	tři	k4xCgInPc4
zápasy	zápas	k1gInPc4
a	a	k8xC
skončil	skončit	k5eAaPmAgInS
se	s	k7c7
svým	svůj	k3xOyFgInSc7
týmem	tým	k1gInSc7
na	na	k7c6
třetím	třetí	k4xOgNnSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
šampionátu	šampionát	k1gInSc6
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
italského	italský	k2eAgNnSc2d1
AS	as	k1gNnSc2
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
kterému	který	k3yQgInSc3,k3yRgInSc3,k3yIgInSc3
spolu	spolu	k6eAd1
s	s	k7c7
krajanem	krajan	k1gMnSc7
Stigem	Stig	k1gMnSc7
Sundqvistem	Sundqvist	k1gMnSc7
pomohl	pomoct	k5eAaPmAgMnS
roku	rok	k1gInSc2
1952	#num#	k4
k	k	k7c3
návratu	návrat	k1gInSc3
do	do	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
www.umea.se	www.umea.se	k1gFnSc1
<g/>
.	.	kIx.
www.umea.se	www.umea.se	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sweden	Swedna	k1gFnPc2
-	-	kIx~
Footballer	Footballer	k1gInSc1
of	of	k?
the	the	k?
Year	Year	k1gInSc1
(	(	kIx(
<g/>
Guldbollen	Guldbollen	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
RSSSF	RSSSF	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
http://www.forza27.com/the-history-of-as-roma-part-3/	http://www.forza27.com/the-history-of-as-roma-part-3/	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
na	na	k7c6
National	National	k1gFnSc6
Football	Footballa	k1gFnPc2
Teams	Teamsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
na	na	k7c4
eu-football	eu-football	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Knut	knuta	k1gFnPc2
Nordahl	Nordahl	k1gFnPc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Medailisté	medailista	k1gMnPc1
–	–	k?
fotbal	fotbal	k1gInSc4
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1948	#num#	k4
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Torsten	Torsten	k2eAgInSc1d1
Lindberg	Lindberg	k1gInSc1
•	•	k?
Knut	knuta	k1gFnPc2
Nordahl	Nordahl	k1gMnSc1
•	•	k?
Erik	Erik	k1gMnSc1
Nilsson	Nilsson	k1gMnSc1
•	•	k?
Birger	Birgero	k1gNnPc2
Rosengren	Rosengrno	k1gNnPc2
•	•	k?
Bertil	Bertil	k1gMnSc2
Nordahl	Nordahl	k1gMnSc2
•	•	k?
Sune	sunout	k5eAaImIp3nS
Andersson	Andersson	k1gMnSc1
•	•	k?
Kjell	Kjell	k1gMnSc1
Rosen	rosen	k2eAgMnSc1d1
•	•	k?
Gunnar	Gunnar	k1gMnSc1
Gren	Gren	k1gMnSc1
•	•	k?
Gunnar	Gunnar	k1gMnSc1
Nordahl	Nordahl	k1gMnSc1
•	•	k?
Henry	Henry	k1gMnSc1
Carlsson	Carlsson	k1gMnSc1
•	•	k?
Nils	Nils	k1gInSc1
Liedholm	Liedholm	k1gInSc1
•	•	k?
Börje	Börj	k1gFnSc2
Leander	Leander	k1gMnSc1
•	•	k?
Kalle	Kalle	k1gInSc1
Svensson	Svensson	k1gMnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
George	Georg	k1gMnSc2
Raynor	Raynor	k1gMnSc1
(	(	kIx(
<g/>
Anglie	Anglie	k1gFnSc1
<g/>
)	)	kIx)
Jugoslávie	Jugoslávie	k1gFnSc1
</s>
<s>
Ljubomir	Ljubomir	k1gMnSc1
Lovrić	Lovrić	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Brozović	Brozović	k1gMnSc1
•	•	k?
Branko	branka	k1gFnSc5
Stanković	Stanković	k1gMnSc1
•	•	k?
Zlatko	Zlatka	k1gFnSc5
Čajkovski	Čajkovski	k1gNnPc7
•	•	k?
Miodrag	Miodrag	k1gMnSc1
Jovanović	Jovanović	k1gMnSc1
•	•	k?
Aleksandar	Aleksandar	k1gMnSc1
Atanacković	Atanacković	k1gMnSc1
•	•	k?
Zvonko	Zvonko	k1gNnSc4
Cimermančić	Cimermančić	k1gFnSc2
•	•	k?
Rajko	rajka	k1gFnSc5
Mitić	Mitić	k1gMnSc3
•	•	k?
Stjepan	Stjepan	k1gMnSc1
Bobek	Bobek	k1gMnSc1
•	•	k?
Željko	Željka	k1gFnSc5
Čajkovski	Čajkovski	k1gNnPc7
•	•	k?
Bernard	Bernard	k1gMnSc1
Vukas	Vukas	k1gMnSc1
•	•	k?
Franjo	Franjo	k1gMnSc1
Šoštarić	Šoštarić	k1gMnSc1
•	•	k?
Prvoslav	Prvoslav	k1gMnSc1
Mihajlović	Mihajlović	k1gMnSc1
•	•	k?
Franjo	Franjo	k1gMnSc1
Wölfl	Wölfl	k1gMnSc1
•	•	k?
Kosta	Kosta	k1gMnSc1
Tomašević	Tomašević	k1gMnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Milorad	Milorad	k1gInSc1
Arsenijević	Arsenijević	k1gFnPc2
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Eigil	Eigil	k1gMnSc1
Nielsen	Nielsen	k2eAgMnSc1d1
•	•	k?
Viggo	Viggo	k1gMnSc1
Jensen	Jensen	k2eAgMnSc1d1
•	•	k?
Knud	Knud	k1gMnSc1
Borge	Borg	k1gFnSc2
Overgaard	Overgaard	k1gMnSc1
•	•	k?
Axel	Axel	k1gInSc1
Pilmark	Pilmark	k1gInSc1
•	•	k?
Dion	Dion	k1gInSc1
Ø	Ø	k1gMnSc1
•	•	k?
Ivan	Ivan	k1gMnSc1
Jensen	Jensen	k2eAgMnSc1d1
•	•	k?
Johannes	Johannes	k1gMnSc1
Plöger	Plöger	k1gMnSc1
•	•	k?
Knud	Knud	k1gMnSc1
Lundberg	Lundberg	k1gMnSc1
•	•	k?
Carl	Carl	k1gMnSc1
Aage	Aag	k1gFnSc2
Præ	Præ	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Hansen	Hansen	k2eAgMnSc1d1
•	•	k?
Jø	Jø	k1gInSc1
Leschly	Leschly	k1gMnSc1
Sø	Sø	k1gFnPc2
•	•	k?
Holger	Holger	k1gMnSc1
Seebach	Seebach	k1gMnSc1
•	•	k?
Karl	Karl	k1gMnSc1
Aage	Aag	k1gFnSc2
Hansen	Hansen	k1gInSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Reg	Reg	k1gMnSc1
Mountford	Mountford	k1gMnSc1
(	(	kIx(
<g/>
Anglie	Anglie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Medailisté	medailista	k1gMnPc1
-	-	kIx~
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
1950	#num#	k4
Uruguay	Uruguay	k1gFnSc1
</s>
<s>
Julio	Julio	k1gMnSc1
César	César	k1gMnSc1
Britos	Britos	k1gMnSc1
•	•	k?
Juan	Juan	k1gMnSc1
Burgueñ	Burgueñ	k1gMnSc1
•	•	k?
Schubert	Schubert	k1gMnSc1
Gambetta	Gambetta	k1gMnSc1
•	•	k?
Alcides	Alcides	k1gMnSc1
Ghiggia	Ghiggium	k1gNnSc2
•	•	k?
Juan	Juan	k1gMnSc1
Carlos	Carlos	k1gMnSc1
González	González	k1gMnSc1
•	•	k?
Matías	Matías	k1gMnSc1
González	González	k1gMnSc1
•	•	k?
William	William	k1gInSc1
Martínez	Martínez	k1gInSc1
•	•	k?
Roque	Roqu	k1gInSc2
Máspoli	Máspole	k1gFnSc3
•	•	k?
Oscar	Oscar	k1gInSc1
Míguez	Míguez	k1gInSc1
•	•	k?
Rubén	Rubén	k1gInSc1
Morán	Morán	k1gInSc1
•	•	k?
Washington	Washington	k1gInSc1
Ortuñ	Ortuñ	k1gMnSc1
•	•	k?
Aníbal	Aníbal	k1gInSc1
Paz	Paz	k1gMnSc1
•	•	k?
Julio	Julio	k1gMnSc1
Pérez	Pérez	k1gMnSc1
•	•	k?
Rodolfo	Rodolfo	k1gMnSc1
Pini	Pini	k1gNnSc2
•	•	k?
Luis	Luisa	k1gFnPc2
Rijo	Rijo	k1gMnSc1
•	•	k?
Víctor	Víctor	k1gMnSc1
Rodríguez	Rodríguez	k1gMnSc1
Andrade	Andrad	k1gInSc5
•	•	k?
Carlos	Carlos	k1gMnSc1
Romero	Romero	k1gNnSc1
•	•	k?
Juan	Juan	k1gMnSc1
Alberto	Alberta	k1gFnSc5
Schiaffino	Schiaffin	k2eAgNnSc1d1
•	•	k?
Eusebio	Eusebio	k1gNnSc1
Tejera	Tejero	k1gNnSc2
•	•	k?
Obdulio	Obdulio	k1gMnSc1
Varela	Varel	k1gMnSc2
–	–	k?
•	•	k?
Ernesto	Ernesta	k1gMnSc5
Vidal	Vidal	k1gMnSc1
•	•	k?
Héctor	Héctor	k1gMnSc1
Vilches	Vilches	k1gMnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Juan	Juan	k1gMnSc1
López	López	k1gMnSc1
Brazílie	Brazílie	k1gFnSc2
</s>
<s>
Adã	Adã	k6eAd1
•	•	k?
Ademir	Ademira	k1gFnPc2
•	•	k?
Alfredo	Alfredo	k1gNnSc4
•	•	k?
Augusto	Augusta	k1gMnSc5
da	da	k?
Costa	Costo	k1gNnSc2
–	–	k?
•	•	k?
Baltazar	Baltazar	k1gMnSc1
•	•	k?
Barbosa	Barbosa	k1gFnSc1
•	•	k?
Bauer	Bauer	k1gMnSc1
•	•	k?
Bigode	Bigod	k1gInSc5
•	•	k?
Carlos	Carlos	k1gMnSc1
José	José	k1gNnSc2
Castilho	Castil	k1gMnSc2
•	•	k?
Chico	Chico	k1gMnSc1
•	•	k?
Danilo	danit	k5eAaImAgNnS
•	•	k?
Ely	Ela	k1gFnSc2
•	•	k?
Friaça	Friaça	k1gMnSc1
•	•	k?
Jair	Jair	k1gMnSc1
•	•	k?
Juvenal	Juvenal	k1gMnSc1
•	•	k?
Maneca	Maneca	k1gMnSc1
•	•	k?
Nena	Nena	k1gMnSc1
•	•	k?
Nílton	Nílton	k1gInSc1
Santos	Santos	k1gMnSc1
•	•	k?
Noronha	Noronha	k1gMnSc1
•	•	k?
Rodrigues	Rodrigues	k1gMnSc1
•	•	k?
Rui	Rui	k1gMnSc1
•	•	k?
Zizinho	Zizin	k1gMnSc4
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Flavio	Flavio	k1gMnSc1
Costa	Costa	k1gMnSc1
Švédsko	Švédsko	k1gNnSc4
</s>
<s>
Olle	Olle	k6eAd1
Å	Å	k1gInSc1
•	•	k?
Sune	sunout	k5eAaImIp3nS
Andersson	Andersson	k1gMnSc1
•	•	k?
Ivan	Ivan	k1gMnSc1
Bodin	Bodin	k1gMnSc1
•	•	k?
Ingvar	Ingvar	k1gInSc1
Gärd	Gärd	k1gInSc1
•	•	k?
Hasse	Hasse	k1gFnSc2
Jeppson	Jeppson	k1gMnSc1
•	•	k?
Gunnar	Gunnar	k1gMnSc1
Johansson	Johansson	k1gMnSc1
•	•	k?
Egon	Egon	k1gMnSc1
Jönsson	Jönsson	k1gMnSc1
•	•	k?
Torsten	Torsten	k2eAgMnSc1d1
Lindberg	Lindberg	k1gMnSc1
•	•	k?
Arne	Arne	k1gMnSc1
Må	Må	k1gMnSc1
•	•	k?
Bror	Bror	k1gMnSc1
Mellberg	Mellberg	k1gMnSc1
•	•	k?
Erik	Erik	k1gMnSc1
Nilsson	Nilsson	k1gMnSc1
–	–	k?
•	•	k?
Stellan	Stellan	k1gInSc1
Nilsson	Nilsson	k1gInSc1
•	•	k?
Knut	knuta	k1gFnPc2
Nordahl	Nordahl	k1gMnSc1
•	•	k?
Karl-Erik	Karl-Erik	k1gMnSc1
Palmér	Palmér	k1gMnSc1
•	•	k?
Ingvar	Ingvar	k1gInSc1
Rydell	Rydell	k1gInSc1
•	•	k?
Lennart	Lennart	k1gInSc1
Samuelsson	Samuelsson	k1gInSc1
•	•	k?
Lennart	Lennart	k1gInSc1
Skoglund	Skoglund	k1gMnSc1
•	•	k?
Stig	Stig	k1gMnSc1
Sundqvist	Sundqvist	k1gMnSc1
•	•	k?
Kalle	Kalle	k1gInSc1
Svensson	Svensson	k1gMnSc1
•	•	k?
Kurt	Kurt	k1gMnSc1
Svensson	Svensson	k1gMnSc1
•	•	k?
Tore	Tor	k1gMnSc5
Svensson	Svensson	k1gNnSc1
•	•	k?
Börje	Börj	k1gInSc2
Tapper	Tappra	k1gFnPc2
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
George	Georg	k1gMnSc2
Raynor	Raynor	k1gMnSc1
</s>
<s>
Fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1946	#num#	k4
<g/>
:	:	kIx,
Gunnar	Gunnar	k1gInSc1
Gren	Gren	k1gInSc1
•	•	k?
1947	#num#	k4
<g/>
:	:	kIx,
Gunnar	Gunnar	k1gInSc1
Nordahl	Nordahl	k1gFnSc2
•	•	k?
1948	#num#	k4
<g/>
:	:	kIx,
Bertil	Bertil	k1gFnSc1
Nordahl	Nordahl	k1gFnSc2
•	•	k?
1949	#num#	k4
<g/>
:	:	kIx,
Knut	knuta	k1gFnPc2
Nordahl	Nordahl	k1gFnSc2
•	•	k?
1950	#num#	k4
<g/>
:	:	kIx,
Erik	Erika	k1gFnPc2
Nilsson	Nilsson	k1gInSc1
•	•	k?
1951	#num#	k4
<g/>
:	:	kIx,
Olle	Oll	k1gInSc2
Å	Å	k1gInSc1
•	•	k?
1952	#num#	k4
<g/>
:	:	kIx,
Kalle	Kall	k1gInSc6
Svensson	Svensson	k1gInSc4
•	•	k?
1953	#num#	k4
<g/>
:	:	kIx,
Bengt	Bengt	k2eAgInSc1d1
Gustavsson	Gustavsson	k1gInSc1
•	•	k?
1954	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Sven-Ove	Sven-Oev	k1gFnSc2
Svensson	Svenssona	k1gFnPc2
•	•	k?
1955	#num#	k4
<g/>
:	:	kIx,
Gösta	Gösto	k1gNnSc2
Löfgren	Löfgrno	k1gNnPc2
•	•	k?
1956	#num#	k4
<g/>
:	:	kIx,
Gösta	Göst	k1gInSc2
Sandberg	Sandberg	k1gInSc1
•	•	k?
1957	#num#	k4
<g/>
:	:	kIx,
Å	Å	k1gInSc2
Johansson	Johansson	k1gInSc1
•	•	k?
1958	#num#	k4
<g/>
:	:	kIx,
Orvar	Orvar	k1gInSc1
Bergmark	Bergmark	k1gInSc1
•	•	k?
1959	#num#	k4
<g/>
:	:	kIx,
Agne	Agn	k1gInSc2
Simonsson	Simonsson	k1gInSc1
•	•	k?
1960	#num#	k4
<g/>
:	:	kIx,
Torbjörn	Torbjörn	k1gInSc1
Jonsson	Jonsson	k1gInSc1
•	•	k?
1961	#num#	k4
<g/>
:	:	kIx,
Bengt	Bengt	k2eAgInSc1d1
Nyholm	Nyholm	k1gInSc1
•	•	k?
1962	#num#	k4
<g/>
:	:	kIx,
Prawitz	Prawitz	k1gMnSc1
Öberg	Öberg	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1963	#num#	k4
<g/>
:	:	kIx,
Harry	Harra	k1gFnSc2
Bild	Bild	k1gInSc1
•	•	k?
1964	#num#	k4
<g/>
:	:	kIx,
Hans	hansa	k1gFnPc2
Mild	Mild	k1gInSc1
•	•	k?
1965	#num#	k4
<g/>
:	:	kIx,
Bo	Bo	k?
Larsson	Larsson	k1gInSc1
•	•	k?
1966	#num#	k4
<g/>
:	:	kIx,
Ove	Ove	k1gFnSc1
Kindvall	Kindvall	k1gInSc1
•	•	k?
1967	#num#	k4
<g/>
:	:	kIx,
Ingvar	Ingvar	k1gInSc1
Svahn	Svahn	k1gInSc1
•	•	k?
1968	#num#	k4
<g/>
:	:	kIx,
Björn	Björn	k1gInSc1
Nordqvist	Nordqvist	k1gInSc1
•	•	k?
1969	#num#	k4
<g/>
:	:	kIx,
Tommy	Tomma	k1gFnSc2
Svensson	Svensson	k1gInSc1
•	•	k?
1970	#num#	k4
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Olsson	Olssona	k1gFnPc2
•	•	k?
1971	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Ronnie	Ronnie	k1gFnSc1
Hellström	Hellström	k1gInSc1
•	•	k?
1972	#num#	k4
<g/>
:	:	kIx,
Ralf	Ralf	k1gInSc1
Edström	Edström	k1gInSc1
•	•	k?
1973	#num#	k4
<g/>
:	:	kIx,
Bo	Bo	k?
Larsson	Larsson	k1gInSc1
•	•	k?
1974	#num#	k4
<g/>
:	:	kIx,
Ralf	Ralf	k1gInSc1
Edström	Edström	k1gInSc1
•	•	k?
1975	#num#	k4
<g/>
:	:	kIx,
Kent	Kent	k2eAgInSc1d1
Karlsson	Karlsson	k1gInSc1
•	•	k?
1976	#num#	k4
<g/>
:	:	kIx,
Anders	Anders	k1gInSc1
Linderoth	Linderoth	k1gInSc1
•	•	k?
1977	#num#	k4
<g/>
:	:	kIx,
Roy	Roy	k1gMnSc1
Andersson	Anderssona	k1gFnPc2
•	•	k?
1978	#num#	k4
<g/>
:	:	kIx,
Ronnie	Ronnie	k1gFnSc1
Hellström	Hellström	k1gInSc1
•	•	k?
1979	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Jan	Jan	k1gMnSc1
Möller	Möller	k1gMnSc1
•	•	k?
1980	#num#	k4
<g/>
:	:	kIx,
Rolf	Rolf	k1gInSc1
Zetterlund	Zetterlund	k1gInSc1
•	•	k?
1981	#num#	k4
<g/>
:	:	kIx,
Thomas	Thomas	k1gMnSc1
Ravelli	Ravelle	k1gFnSc4
•	•	k?
1982	#num#	k4
<g/>
:	:	kIx,
Torbjörn	Torbjörn	k1gInSc1
Nilsson	Nilsson	k1gInSc1
•	•	k?
1983	#num#	k4
<g/>
:	:	kIx,
Glenn	Glenn	k1gInSc1
Hysén	Hysén	k1gInSc1
•	•	k?
1984	#num#	k4
<g/>
:	:	kIx,
Sven	Sven	k1gInSc1
Dahlkvist	Dahlkvist	k1gInSc1
•	•	k?
1985	#num#	k4
<g/>
:	:	kIx,
Glenn	Glenn	k1gInSc1
Strömberg	Strömberg	k1gInSc1
•	•	k?
1986	#num#	k4
<g/>
:	:	kIx,
Robert	Robert	k1gMnSc1
Prytz	Prytza	k1gFnPc2
•	•	k?
1987	#num#	k4
<g/>
:	:	kIx,
Peter	Peter	k1gMnSc1
Larsson	Larsson	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1988	#num#	k4
<g/>
:	:	kIx,
Glenn	Glenn	k1gInSc1
Hysén	Hysén	k1gInSc1
•	•	k?
1989	#num#	k4
<g/>
:	:	kIx,
Jonas	Jonas	k1gInSc1
Thern	Thern	k1gInSc1
•	•	k?
1990	#num#	k4
<g/>
:	:	kIx,
Tomas	Tomas	k1gInSc1
Brolin	Brolin	k1gInSc1
•	•	k?
1991	#num#	k4
<g/>
:	:	kIx,
Anders	Anders	k1gInSc1
Limpar	Limpar	k1gInSc1
•	•	k?
1992	#num#	k4
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Eriksson	Erikssona	k1gFnPc2
•	•	k?
1993	#num#	k4
<g/>
:	:	kIx,
Martin	Martin	k2eAgInSc1d1
Dahlin	Dahlin	k1gInSc1
•	•	k?
1994	#num#	k4
<g/>
:	:	kIx,
Tomas	Tomas	k1gInSc1
Brolin	Brolin	k1gInSc1
•	•	k?
1995	#num#	k4
<g/>
:	:	kIx,
Patrik	Patrik	k1gMnSc1
Andersson	Anderssona	k1gFnPc2
•	•	k?
1996	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Roland	Roland	k1gInSc1
Nilsson	Nilsson	k1gInSc1
•	•	k?
1997	#num#	k4
<g/>
:	:	kIx,
Pär	Pär	k1gFnSc1
Zetterberg	Zetterberg	k1gInSc1
•	•	k?
1998	#num#	k4
<g/>
:	:	kIx,
Henrik	Henrik	k1gMnSc1
Larsson	Larssona	k1gFnPc2
•	•	k?
1999	#num#	k4
<g/>
:	:	kIx,
Stefan	Stefan	k1gMnSc1
Schwarz	Schwarz	k1gMnSc1
•	•	k?
2000	#num#	k4
<g/>
:	:	kIx,
Magnus	Magnus	k1gMnSc1
Hedman	Hedman	k1gMnSc1
•	•	k?
2001	#num#	k4
<g/>
:	:	kIx,
Patrik	Patrik	k1gMnSc1
Andersson	Anderssona	k1gFnPc2
•	•	k?
2002	#num#	k4
<g/>
:	:	kIx,
Fredrik	Fredrika	k1gFnPc2
Ljungberg	Ljungberg	k1gInSc1
•	•	k?
2003	#num#	k4
<g/>
:	:	kIx,
Olof	Olof	k1gInSc1
Mellberg	Mellberg	k1gInSc1
•	•	k?
2004	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Henrik	Henrik	k1gMnSc1
Larsson	Larsson	k1gMnSc1
•	•	k?
2005	#num#	k4
<g/>
:	:	kIx,
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gFnSc2
•	•	k?
2006	#num#	k4
<g/>
:	:	kIx,
Fredrik	Fredrika	k1gFnPc2
Ljungberg	Ljungberg	k1gInSc1
•	•	k?
2007	#num#	k4
<g/>
:	:	kIx,
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gFnSc2
•	•	k?
2008	#num#	k4
<g/>
:	:	kIx,
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gFnSc2
•	•	k?
2009	#num#	k4
<g/>
:	:	kIx,
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gFnSc2
•	•	k?
2010	#num#	k4
<g/>
:	:	kIx,
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gFnSc2
•	•	k?
2011	#num#	k4
<g/>
:	:	kIx,
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gFnSc2
•	•	k?
2012	#num#	k4
<g/>
:	:	kIx,
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gFnSc2
•	•	k?
2013	#num#	k4
<g/>
:	:	kIx,
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gFnSc2
•	•	k?
2014	#num#	k4
<g/>
:	:	kIx,
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gFnSc2
•	•	k?
2015	#num#	k4
<g/>
:	:	kIx,
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gFnSc2
•	•	k?
2016	#num#	k4
<g/>
:	:	kIx,
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gFnSc2
•	•	k?
2017	#num#	k4
<g/>
:	:	kIx,
Andreas	Andreas	k1gInSc1
Granqvist	Granqvist	k1gInSc1
•	•	k?
2018	#num#	k4
<g/>
:	:	kIx,
Victor	Victor	k1gMnSc1
Lindelöf	Lindelöf	k1gMnSc1
•	•	k?
2019	#num#	k4
<g/>
:	:	kIx,
Victor	Victor	k1gMnSc1
Lindelöf	Lindelöf	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Švédsko	Švédsko	k1gNnSc1
</s>
