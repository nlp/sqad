<s>
Skořicovník	skořicovník	k1gInSc1	skořicovník
čínský	čínský	k2eAgInSc1d1	čínský
(	(	kIx(	(
<g/>
Cinnamomum	Cinnamomum	k1gInSc1	Cinnamomum
cassia	cassium	k1gNnSc2	cassium
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Barmy	Barma	k1gFnSc2	Barma
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Thajsku	Thajsko	k1gNnSc6	Thajsko
a	a	k8xC	a
Indonésii	Indonésie	k1gFnSc6	Indonésie
<g/>
.	.	kIx.	.
</s>
