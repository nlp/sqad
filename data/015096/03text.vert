<s>
Okres	okres	k1gInSc1
Bludenz	Bludenza	k1gFnPc2
</s>
<s>
Okres	okres	k1gInSc1
Bludenz	Bludenza	k1gFnPc2
Bezirk	Bezirk	k1gInSc1
Bludenz	Bludenz	k1gMnSc1
Geografie	geografie	k1gFnSc1
</s>
<s>
okres	okres	k1gInSc1
Bludenz	Bludenz	k1gInSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
Rakouska	Rakousko	k1gNnSc2
</s>
<s>
okres	okres	k1gInSc1
Bludenz	Bludenz	k1gInSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
Vorarlberska	Vorarlbersko	k1gNnSc2
Hlavní	hlavní	k2eAgFnSc6d1
město	město	k1gNnSc1
</s>
<s>
Bludenz	Bludenz	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
47	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
9	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1	#num#	k4
287,52	287,52	k4
km²	km²	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
61	#num#	k4
383	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
47,7	47,7	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
regionu	region	k1gInSc2
Nadřazený	nadřazený	k2eAgInSc1d1
celek	celek	k1gInSc1
</s>
<s>
Vorarlbersko	Vorarlbersko	k1gNnSc1
Druh	druh	k1gInSc1
celku	celek	k1gInSc3
</s>
<s>
okres	okres	k1gInSc1
Podřízené	podřízený	k2eAgInPc1d1
celky	celek	k1gInPc1
</s>
<s>
29	#num#	k4
obcí	obec	k1gFnPc2
Označení	označení	k1gNnSc4
vozidel	vozidlo	k1gNnPc2
</s>
<s>
BZ	bz	k0
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.vorarlberg.at/bhbludenz	www.vorarlberg.at/bhbludenz	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Okres	okres	k1gInSc1
Bludenz	Bludenz	k1gInSc1
je	být	k5eAaImIp3nS
okres	okres	k1gInSc1
v	v	k7c6
rakouské	rakouský	k2eAgFnSc6d1
spolkové	spolkový	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
Vorarlbersko	Vorarlbersko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
1287,52	1287,52	k4
km²	km²	k?
a	a	k8xC
žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
61	#num#	k4
383	#num#	k4
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlem	sídlo	k1gNnSc7
okresu	okres	k1gInSc2
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
Bludenz	Bludenz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okres	okres	k1gInSc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
člení	členit	k5eAaImIp3nS
na	na	k7c4
29	#num#	k4
obcí	obec	k1gFnPc2
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
jedno	jeden	k4xCgNnSc1
město	město	k1gNnSc1
a	a	k8xC
dva	dva	k4xCgInPc4
městysy	městys	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Města	město	k1gNnSc2
a	a	k8xC
obce	obec	k1gFnSc2
</s>
<s>
Město	město	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Bludenz	Bludenz	k1gMnSc1
</s>
<s>
Městysy	městys	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Nenzing	Nenzing	k1gInSc1
</s>
<s>
Schruns	Schruns	k6eAd1
</s>
<s>
Obce	obec	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Bartholomäberg	Bartholomäberg	k1gMnSc1
</s>
<s>
Blons	Blons	k6eAd1
</s>
<s>
Bludesch	Bludesch	k1gMnSc1
</s>
<s>
Brand	Brand	k1gMnSc1
</s>
<s>
Bürs	Bürs	k6eAd1
</s>
<s>
Bürserberg	Bürserberg	k1gMnSc1
</s>
<s>
Dalaas	Dalaas	k1gMnSc1
</s>
<s>
Fontanella	Fontanella	k6eAd1
</s>
<s>
Gaschurn	Gaschurn	k1gMnSc1
</s>
<s>
Innerbraz	Innerbraz	k1gInSc1
</s>
<s>
Klösterle	Klösterle	k6eAd1
</s>
<s>
Lech	Lech	k1gMnSc1
</s>
<s>
Lorüns	Lorüns	k6eAd1
</s>
<s>
Ludesch	Ludesch	k1gMnSc1
</s>
<s>
Nüziders	Nüziders	k6eAd1
</s>
<s>
Raggal	Raggal	k1gMnSc1
</s>
<s>
Sankt	Sankt	k1gInSc1
Anton	anton	k1gInSc1
im	im	k?
Montafon	Montafon	k1gInSc1
</s>
<s>
Sankt	Sankt	k1gInSc1
Gallenkirch	Gallenkircha	k1gFnPc2
</s>
<s>
Sankt	Sankt	k1gInSc1
Gerold	Gerolda	k1gFnPc2
</s>
<s>
Silbertal	Silbertat	k5eAaPmAgMnS
</s>
<s>
Sonntag	Sonntag	k1gMnSc1
</s>
<s>
Stallehr	Stallehr	k1gMnSc1
</s>
<s>
Thüringen	Thüringen	k1gInSc1
</s>
<s>
Thüringerberg	Thüringerberg	k1gMnSc1
</s>
<s>
Tschagguns	Tschagguns	k6eAd1
</s>
<s>
Vandans	Vandans	k6eAd1
</s>
<s>
Obce	obec	k1gFnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Bludenz	Bludenza	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Okres	okres	k1gInSc1
Bludenz	Bludenz	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Okresy	okres	k1gInPc1
spolkové	spolkový	k2eAgFnSc2d1
země	zem	k1gFnSc2
Vorarlbersko	Vorarlbersko	k1gNnSc1
</s>
<s>
Bludenz	Bludenz	k1gMnSc1
•	•	k?
Bregenz	Bregenz	k1gMnSc1
•	•	k?
Dornbirn	Dornbirn	k1gMnSc1
•	•	k?
Feldkirch	Feldkirch	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
369171	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4266829-3	4266829-3	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
132526338	#num#	k4
</s>
