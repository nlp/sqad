<p>
<s>
Lofoty	Lofot	k1gInPc1	Lofot
jsou	být	k5eAaImIp3nP	být
souostroví	souostroví	k1gNnSc4	souostroví
při	při	k7c6	při
norském	norský	k2eAgNnSc6d1	norské
pobřeží	pobřeží	k1gNnSc6	pobřeží
tvořené	tvořený	k2eAgFnPc1d1	tvořená
asi	asi	k9	asi
80	[number]	k4	80
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
jsou	být	k5eAaImIp3nP	být
Austvå	Austvå	k1gFnSc1	Austvå
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
Hinnø	Hinnø	k1gFnSc2	Hinnø
<g/>
,	,	kIx,	,
Skrova	Skrovo	k1gNnSc2	Skrovo
<g/>
,	,	kIx,	,
Gimsø	Gimsø	k1gMnSc1	Gimsø
<g/>
,	,	kIx,	,
Vestvå	Vestvå	k1gMnSc1	Vestvå
<g/>
,	,	kIx,	,
Flakstadø	Flakstadø	k1gMnSc1	Flakstadø
<g/>
,	,	kIx,	,
Moskenesø	Moskenesø	k1gMnSc1	Moskenesø
<g/>
,	,	kIx,	,
Væ	Væ	k1gMnSc1	Væ
a	a	k8xC	a
Rø	Rø	k1gMnSc1	Rø
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
označovány	označovat	k5eAaImNgFnP	označovat
množným	množný	k2eAgNnSc7d1	množné
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
v	v	k7c6	v
norštině	norština	k1gFnSc6	norština
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc1	jejich
název	název	k1gInSc1	název
–	–	k?	–
Lofoten	Lofoten	k2eAgInSc1d1	Lofoten
–	–	k?	–
jednotným	jednotný	k2eAgNnSc7d1	jednotné
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
složeným	složený	k2eAgNnSc7d1	složené
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
lo	lo	k?	lo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
rys	rys	k1gInSc1	rys
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
fot	fot	k1gInSc1	fot
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
noha	noha	k1gFnSc1	noha
<g/>
)	)	kIx)	)
a	a	k8xC	a
určitého	určitý	k2eAgInSc2d1	určitý
členu	člen	k1gInSc2	člen
"	"	kIx"	"
<g/>
en	en	k?	en
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Lofoten	Lofotno	k1gNnPc2	Lofotno
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgFnSc7d1	moderní
formou	forma	k1gFnSc7	forma
slova	slovo	k1gNnSc2	slovo
Lofotr	Lofotr	k1gMnSc1	Lofotr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc1d1	starý
název	název	k1gInSc1	název
ostrova	ostrov	k1gInSc2	ostrov
Vestvå	Vestvå	k1gFnSc2	Vestvå
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Lofoty	Lofota	k1gFnPc1	Lofota
vybíhají	vybíhat	k5eAaImIp3nP	vybíhat
z	z	k7c2	z
norské	norský	k2eAgFnSc2d1	norská
pevniny	pevnina	k1gFnSc2	pevnina
do	do	k7c2	do
Atlantiku	Atlantik	k1gInSc2	Atlantik
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
100	[number]	k4	100
až	až	k9	až
300	[number]	k4	300
kilometrů	kilometr	k1gInPc2	kilometr
severně	severně	k6eAd1	severně
od	od	k7c2	od
severního	severní	k2eAgInSc2d1	severní
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
mezi	mezi	k7c4	mezi
67	[number]	k4	67
<g/>
.	.	kIx.	.
a	a	k8xC	a
68	[number]	k4	68
<g/>
.	.	kIx.	.
stupněm	stupeň	k1gInSc7	stupeň
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
norské	norský	k2eAgFnSc2d1	norská
pevniny	pevnina	k1gFnSc2	pevnina
je	být	k5eAaImIp3nS	být
souostroví	souostroví	k1gNnSc1	souostroví
odděleno	oddělit	k5eAaPmNgNnS	oddělit
Vestfjordem	Vestfjord	k1gInSc7	Vestfjord
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgInPc1d3	nejvýznamnější
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
propojeny	propojen	k2eAgInPc1d1	propojen
tunely	tunel	k1gInPc1	tunel
a	a	k8xC	a
silničními	silniční	k2eAgInPc7d1	silniční
mosty	most	k1gInPc7	most
</s>
</p>
<p>
<s>
Lofoty	Lofot	k1gInPc1	Lofot
jsou	být	k5eAaImIp3nP	být
součásti	součást	k1gFnPc4	součást
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
nor	nora	k1gFnPc2	nora
<g/>
.	.	kIx.	.
fylke	fylke	k1gFnSc1	fylke
<g/>
)	)	kIx)	)
Nordland	Nordland	k1gInSc1	Nordland
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Svolvæ	Svolvæ	k1gMnSc1	Svolvæ
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Austvå	Austvå	k1gFnSc2	Austvå
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Lofotech	Lofot	k1gInPc6	Lofot
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc4	šest
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
nor	nora	k1gFnPc2	nora
<g/>
.	.	kIx.	.
kommunen	kommunen	k1gInSc1	kommunen
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Vå	Vå	k1gFnSc1	Vå
<g/>
,	,	kIx,	,
Vestvå	Vestvå	k1gFnSc1	Vestvå
<g/>
,	,	kIx,	,
Flakstad	Flakstad	k1gInSc1	Flakstad
<g/>
,	,	kIx,	,
Moskenes	Moskenes	k1gMnSc1	Moskenes
<g/>
,	,	kIx,	,
Væ	Væ	k1gMnSc1	Væ
a	a	k8xC	a
Rø	Rø	k1gMnSc1	Rø
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
24	[number]	k4	24
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
1227	[number]	k4	1227
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severně	severně	k6eAd1	severně
a	a	k8xC	a
východně	východně	k6eAd1	východně
od	od	k7c2	od
Lofot	Lofota	k1gFnPc2	Lofota
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
další	další	k2eAgNnPc1d1	další
souostroví	souostroví	k1gNnPc1	souostroví
<g/>
,	,	kIx,	,
Vesterály	Vesterál	k1gInPc1	Vesterál
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Lofot	Lofota	k1gFnPc2	Lofota
odděleno	oddělen	k2eAgNnSc1d1	odděleno
úžinou	úžina	k1gFnSc7	úžina
Raftsund	Raftsunda	k1gFnPc2	Raftsunda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
turisty	turist	k1gMnPc4	turist
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
Trollfjord	Trollfjord	k1gInSc4	Trollfjord
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
Austvå	Austvå	k1gFnSc2	Austvå
je	být	k5eAaImIp3nS	být
administrativně	administrativně	k6eAd1	administrativně
součástí	součást	k1gFnSc7	součást
obce	obec	k1gFnSc2	obec
Hadsel	Hadsela	k1gFnPc2	Hadsela
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
považována	považován	k2eAgFnSc1d1	považována
již	již	k9	již
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
Vesterál	Vesterála	k1gFnPc2	Vesterála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Lofoty	Lofota	k1gFnPc1	Lofota
jsou	být	k5eAaImIp3nP	být
obývané	obývaný	k2eAgFnPc1d1	obývaná
lidmi	člověk	k1gMnPc7	člověk
asi	asi	k9	asi
šest	šest	k4xCc1	šest
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
obyvatelé	obyvatel	k1gMnPc1	obyvatel
živili	živit	k5eAaImAgMnP	živit
hlavně	hlavně	k6eAd1	hlavně
lovem	lov	k1gInSc7	lov
a	a	k8xC	a
rybolovem	rybolov	k1gInSc7	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vikinské	vikinský	k2eAgFnSc2d1	vikinská
éry	éra	k1gFnSc2	éra
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
mnoho	mnoho	k4c1	mnoho
osad	osada	k1gFnPc2	osada
se	s	k7c7	s
sídly	sídlo	k1gNnPc7	sídlo
vikinských	vikinský	k2eAgMnPc2d1	vikinský
náčelníků	náčelník	k1gMnPc2	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
Kopie	kopie	k1gFnSc1	kopie
náčelníkova	náčelníkův	k2eAgNnSc2d1	náčelníkovo
sídla	sídlo	k1gNnSc2	sídlo
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
v	v	k7c6	v
Borgu	Borg	k1gInSc6	Borg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
vikinského	vikinský	k2eAgNnSc2d1	vikinské
musea	museum	k1gNnSc2	museum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
čtrnáctého	čtrnáctý	k4xOgNnSc2	čtrnáctý
století	století	k1gNnSc2	století
ovládali	ovládat	k5eAaImAgMnP	ovládat
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
rybami	ryba	k1gFnPc7	ryba
obchodníci	obchodník	k1gMnPc1	obchodník
z	z	k7c2	z
Bergenu	Bergen	k1gInSc2	Bergen
<g/>
.	.	kIx.	.
</s>
<s>
Bergen	Bergen	k1gInSc1	Bergen
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
administrativním	administrativní	k2eAgNnSc7d1	administrativní
centrem	centrum	k1gNnSc7	centrum
vybírajícím	vybírající	k2eAgNnSc7d1	vybírající
daně	daň	k1gFnSc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
života	život	k1gInSc2	život
na	na	k7c6	na
Lofotech	Lofot	k1gInPc6	Lofot
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
závisela	záviset	k5eAaImAgFnS	záviset
na	na	k7c6	na
výsledcích	výsledek	k1gInPc6	výsledek
rybolovu	rybolov	k1gInSc2	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
největší	veliký	k2eAgFnSc3d3	veliký
přistěhovalecké	přistěhovalecký	k2eAgFnSc3d1	přistěhovalecká
vlně	vlna	k1gFnSc3	vlna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc2	vznik
současných	současný	k2eAgNnPc2d1	současné
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Krajina	Krajina	k1gFnSc1	Krajina
==	==	k?	==
</s>
</p>
<p>
<s>
Lofotská	Lofotský	k2eAgFnSc1d1	Lofotský
krajina	krajina	k1gFnSc1	krajina
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
holá	holý	k2eAgFnSc1d1	holá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
lidé	člověk	k1gMnPc1	člověk
většinu	většina	k1gFnSc4	většina
stromů	strom	k1gInPc2	strom
pokáceli	pokácet	k5eAaPmAgMnP	pokácet
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
výstavby	výstavba	k1gFnSc2	výstavba
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
svých	svůj	k3xOyFgFnPc2	svůj
obydlí	obydlet	k5eAaPmIp3nS	obydlet
a	a	k8xC	a
konstrukcí	konstrukce	k1gFnSc7	konstrukce
pro	pro	k7c4	pro
sušení	sušení	k1gNnSc4	sušení
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
Lofot	Lofota	k1gFnPc2	Lofota
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
významnému	významný	k2eAgNnSc3d1	významné
obnovení	obnovení	k1gNnSc3	obnovení
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osídlena	osídlen	k2eAgFnSc1d1	osídlena
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tam	tam	k6eAd1	tam
vítr	vítr	k1gInSc1	vítr
a	a	k8xC	a
mořské	mořský	k2eAgInPc1d1	mořský
proudy	proud	k1gInPc1	proud
nepůsobí	působit	k5eNaImIp3nP	působit
tam	tam	k6eAd1	tam
silně	silně	k6eAd1	silně
jako	jako	k8xC	jako
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
až	až	k9	až
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1200	[number]	k4	1200
m	m	kA	m
vysoké	vysoký	k2eAgFnSc2d1	vysoká
hory	hora	k1gFnSc2	hora
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
zastavit	zastavit	k5eAaPmF	zastavit
či	či	k8xC	či
zmírnit	zmírnit	k5eAaPmF	zmírnit
výkyvy	výkyv	k1gInPc4	výkyv
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
ostrovy	ostrov	k1gInPc7	ostrov
mohou	moct	k5eAaImIp3nP	moct
působením	působení	k1gNnPc3	působení
slapových	slapový	k2eAgInPc2d1	slapový
sil	síla	k1gFnPc2	síla
vznikat	vznikat	k5eAaImF	vznikat
silné	silný	k2eAgInPc1d1	silný
a	a	k8xC	a
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
mořské	mořský	k2eAgInPc1d1	mořský
proudy	proud	k1gInPc1	proud
a	a	k8xC	a
vodní	vodní	k2eAgInPc1d1	vodní
víry	vír	k1gInPc1	vír
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
proudem	proud	k1gInSc7	proud
je	být	k5eAaImIp3nS	být
Mahlstrom	Mahlstrom	k1gInSc1	Mahlstrom
známý	známý	k2eAgInSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
Moskenstraumen	Moskenstraumen	k2eAgMnSc1d1	Moskenstraumen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klima	klima	k1gNnSc1	klima
==	==	k?	==
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
působení	působení	k1gNnSc3	působení
Golfského	golfský	k2eAgInSc2d1	golfský
proudu	proud	k1gInSc2	proud
je	být	k5eAaImIp3nS	být
klima	klima	k1gNnSc1	klima
na	na	k7c6	na
Lofotech	Lofot	k1gInPc6	Lofot
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
poloze	poloha	k1gFnSc3	poloha
za	za	k7c7	za
severním	severní	k2eAgInSc7d1	severní
polárním	polární	k2eAgInSc7d1	polární
kruhem	kruh	k1gInSc7	kruh
relativně	relativně	k6eAd1	relativně
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Lofot	Lofota	k1gFnPc2	Lofota
je	být	k5eAaImIp3nS	být
nejsevernějším	severní	k2eAgNnSc7d3	nejsevernější
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
nikdy	nikdy	k6eAd1	nikdy
neklesá	klesat	k5eNaImIp3nS	klesat
pod	pod	k7c4	pod
nulu	nula	k1gFnSc4	nula
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
tak	tak	k6eAd1	tak
významnou	významný	k2eAgFnSc4d1	významná
klimatickou	klimatický	k2eAgFnSc4d1	klimatická
anomálii	anomálie	k1gFnSc4	anomálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
obživy	obživa	k1gFnSc2	obživa
Lofoťanů	Lofoťan	k1gMnPc2	Lofoťan
jsou	být	k5eAaImIp3nP	být
turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
<g/>
,	,	kIx,	,
rybolov	rybolov	k1gInSc1	rybolov
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
spojená	spojený	k2eAgFnSc1d1	spojená
odvětví	odvětvit	k5eAaPmIp3nS	odvětvit
<g/>
.	.	kIx.	.
</s>
<s>
Lovu	lov	k1gInSc3	lov
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
koná	konat	k5eAaImIp3nS	konat
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
stovky	stovka	k1gFnPc1	stovka
malých	malý	k2eAgFnPc2d1	malá
rybářských	rybářský	k2eAgFnPc2d1	rybářská
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
úlovku	úlovek	k1gInSc2	úlovek
tvoří	tvořit	k5eAaImIp3nP	tvořit
dospělé	dospělý	k2eAgFnPc1d1	dospělá
tresky	treska	k1gFnPc1	treska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
suší	sušit	k5eAaImIp3nS	sušit
<g/>
.	.	kIx.	.
</s>
<s>
Sušené	sušený	k2eAgFnSc2d1	sušená
tresky	treska	k1gFnSc2	treska
(	(	kIx(	(
<g/>
nor	nora	k1gFnPc2	nora
<g/>
.	.	kIx.	.
</s>
<s>
Tø	Tø	k?	Tø
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
exportovány	exportován	k2eAgInPc1d1	exportován
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
platí	platit	k5eAaImIp3nS	platit
moratorium	moratorium	k1gNnSc1	moratorium
na	na	k7c4	na
těžbu	těžba	k1gFnSc4	těžba
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
provádějí	provádět	k5eAaImIp3nP	provádět
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
geologické	geologický	k2eAgInPc4d1	geologický
průzkumy	průzkum	k1gInPc4	průzkum
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
případné	případný	k2eAgFnSc2d1	případná
pozdější	pozdní	k2eAgFnSc2d2	pozdější
těžby	těžba	k1gFnSc2	těžba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Silnice	silnice	k1gFnSc1	silnice
E10	E10	k1gFnPc2	E10
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
Å	Å	k?	Å
a	a	k8xC	a
táhne	táhnout	k5eAaImIp3nS	táhnout
se	se	k3xPyFc4	se
dalších	další	k2eAgFnPc2d1	další
170	[number]	k4	170
km	km	kA	km
do	do	k7c2	do
Fiskebø	Fiskebø	k1gFnSc2	Fiskebø
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
použít	použít	k5eAaPmF	použít
trajekt	trajekt	k1gInSc4	trajekt
do	do	k7c2	do
Melbu	Melb	k1gInSc2	Melb
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
jsou	být	k5eAaImIp3nP	být
provozovány	provozovat	k5eAaImNgFnP	provozovat
též	též	k9	též
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Lofotech	Lofot	k1gInPc6	Lofot
není	být	k5eNaImIp3nS	být
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Bodø	Bodø	k1gFnSc2	Bodø
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
Lofast	Lofast	k1gInSc1	Lofast
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
část	část	k1gFnSc1	část
silničního	silniční	k2eAgNnSc2d1	silniční
spojení	spojení	k1gNnSc2	spojení
Lofotských	Lofotský	k2eAgInPc2d1	Lofotský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Fiskebø	Fiskebø	k1gFnSc2	Fiskebø
přes	přes	k7c4	přes
ostrov	ostrov	k1gInSc4	ostrov
Hinnø	Hinnø	k1gFnPc2	Hinnø
do	do	k7c2	do
Gullesfjordbottnu	Gullesfjordbottno	k1gNnSc3	Gullesfjordbottno
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
jsou	být	k5eAaImIp3nP	být
tudíž	tudíž	k8xC	tudíž
Lofoty	Lofot	k1gInPc1	Lofot
přímo	přímo	k6eAd1	přímo
dosažitelné	dosažitelný	k2eAgInPc1d1	dosažitelný
silniční	silniční	k2eAgFnSc7d1	silniční
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lodě	loď	k1gFnPc1	loď
Hurtigrutenu	Hurtigruten	k1gInSc2	Hurtigruten
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
na	na	k7c6	na
Lofotech	Lofot	k1gInPc6	Lofot
v	v	k7c6	v
Stamsundu	Stamsund	k1gInSc6	Stamsund
a	a	k8xC	a
Svolvæ	Svolvæ	k1gFnSc6	Svolvæ
<g/>
.	.	kIx.	.
</s>
<s>
Svolvæ	Svolvæ	k?	Svolvæ
je	být	k5eAaImIp3nS	být
pravidelnými	pravidelný	k2eAgInPc7d1	pravidelný
trajekty	trajekt	k1gInPc7	trajekt
propojen	propojit	k5eAaPmNgInS	propojit
se	s	k7c7	s
Skutvikem	Skutvik	k1gInSc7	Skutvik
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
vede	vést	k5eAaImIp3nS	vést
silnice	silnice	k1gFnSc1	silnice
E	E	kA	E
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
významným	významný	k2eAgNnSc7d1	významné
lodním	lodní	k2eAgNnSc7d1	lodní
spojením	spojení	k1gNnSc7	spojení
je	být	k5eAaImIp3nS	být
linka	linka	k1gFnSc1	linka
trajektu	trajekt	k1gInSc2	trajekt
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
z	z	k7c2	z
Moskenes	Moskenesa	k1gFnPc2	Moskenesa
do	do	k7c2	do
Bodø	Bodø	k1gFnSc2	Bodø
<g/>
.	.	kIx.	.
</s>
<s>
Katamarány	katamarán	k1gInPc1	katamarán
spojují	spojovat	k5eAaImIp3nP	spojovat
Bodø	Bodø	k1gMnPc4	Bodø
a	a	k8xC	a
Svolvæ	Svolvæ	k1gMnPc4	Svolvæ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Vesterály	Vesterál	k1gMnPc4	Vesterál
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
trajektem	trajekt	k1gInSc7	trajekt
z	z	k7c2	z
Fiskebø	Fiskebø	k1gFnSc2	Fiskebø
do	do	k7c2	do
Melbu	Melb	k1gInSc2	Melb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Lofotech	Lofot	k1gInPc6	Lofot
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
tři	tři	k4xCgNnPc1	tři
letiště	letiště	k1gNnPc1	letiště
<g/>
,	,	kIx,	,
u	u	k7c2	u
Svolvæ	Svolvæ	k1gFnPc2	Svolvæ
<g/>
,	,	kIx,	,
Leknes	Leknesa	k1gFnPc2	Leknesa
a	a	k8xC	a
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Rø	Rø	k1gFnSc2	Rø
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Væ	Væ	k1gFnSc2	Væ
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
heliport	heliport	k1gInSc1	heliport
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
malé	malý	k2eAgNnSc1d1	malé
letiště	letiště	k1gNnSc1	letiště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
nebezpečným	bezpečný	k2eNgInPc3d1	nebezpečný
větrům	vítr	k1gInPc3	vítr
se	se	k3xPyFc4	se
již	již	k9	již
po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
letiště	letiště	k1gNnPc1	letiště
realizují	realizovat	k5eAaBmIp3nP	realizovat
několikrát	několikrát	k6eAd1	několikrát
denně	denně	k6eAd1	denně
lety	let	k1gInPc4	let
do	do	k7c2	do
Bodø	Bodø	k1gFnSc2	Bodø
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Rorbu	Rorb	k1gInSc2	Rorb
–	–	k?	–
rybářský	rybářský	k2eAgInSc1d1	rybářský
domek	domek	k1gInSc1	domek
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Lofoten	Lofotno	k1gNnPc2	Lofotno
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lofoty	Lofota	k1gFnSc2	Lofota
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lofoty	Lofota	k1gFnPc1	Lofota
na	na	k7c6	na
webu	web	k1gInSc6	web
VisitNorway	VisitNorwaa	k1gFnSc2	VisitNorwaa
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
