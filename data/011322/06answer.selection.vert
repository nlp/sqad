<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Lofot	Lofota	k1gFnPc2	Lofota
je	být	k5eAaImIp3nS	být
nejsevernějším	severní	k2eAgNnSc7d3	nejsevernější
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
nikdy	nikdy	k6eAd1	nikdy
neklesá	klesat	k5eNaImIp3nS	klesat
pod	pod	k7c4	pod
nulu	nula	k1gFnSc4	nula
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
tak	tak	k6eAd1	tak
významnou	významný	k2eAgFnSc4d1	významná
klimatickou	klimatický	k2eAgFnSc4d1	klimatická
anomálii	anomálie	k1gFnSc4	anomálie
<g/>
.	.	kIx.	.
</s>
