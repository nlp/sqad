<s>
Moliè	Moliè	k?	Moliè
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Poquelin	Poquelin	k2eAgInSc1d1	Poquelin
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1622	[number]	k4	1622
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1673	[number]	k4	1673
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
období	období	k1gNnSc2	období
francouzského	francouzský	k2eAgInSc2d1	francouzský
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
věku	věk	k1gInSc3	věk
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
měšťana	měšťan	k1gMnSc2	měšťan
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
rodiny	rodina	k1gFnSc2	rodina
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
komediantem	komediant	k1gMnSc7	komediant
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
kočovné	kočovný	k2eAgFnSc2d1	kočovná
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
založil	založit	k5eAaPmAgMnS	založit
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
divadelní	divadelní	k2eAgFnSc4d1	divadelní
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
hrami	hra	k1gFnPc7	hra
proslavil	proslavit	k5eAaPmAgInS	proslavit
<g/>
,	,	kIx,	,
především	především	k9	především
mezi	mezi	k7c7	mezi
chudinou	chudina	k1gFnSc7	chudina
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgMnS	zabývat
se	se	k3xPyFc4	se
tzv.	tzv.	kA	tzv.
nízkým	nízký	k2eAgNnSc7d1	nízké
dramatem	drama	k1gNnSc7	drama
<g/>
,	,	kIx,	,
především	především	k9	především
komedií	komedie	k1gFnSc7	komedie
a	a	k8xC	a
fraškou	fraška	k1gFnSc7	fraška
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
hrách	hra	k1gFnPc6	hra
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
a	a	k8xC	a
zesměšňoval	zesměšňovat	k5eAaImAgMnS	zesměšňovat
společnost	společnost	k1gFnSc4	společnost
(	(	kIx(	(
<g/>
předváděl	předvádět	k5eAaImAgMnS	předvádět
mravy	mrav	k1gInPc4	mrav
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dostával	dostávat	k5eAaImAgInS	dostávat
se	se	k3xPyFc4	se
do	do	k7c2	do
častých	častý	k2eAgInPc2d1	častý
sporů	spor	k1gInPc2	spor
s	s	k7c7	s
královským	královský	k2eAgInSc7d1	královský
dvorem	dvůr	k1gInSc7	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
objevují	objevovat	k5eAaImIp3nP	objevovat
na	na	k7c6	na
jevištích	jeviště	k1gNnPc6	jeviště
<g/>
.	.	kIx.	.
</s>
<s>
Moliè	Moliè	k?	Moliè
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejslavnějším	slavný	k2eAgMnPc3d3	nejslavnější
dramatikům	dramatik	k1gMnPc3	dramatik
éry	éra	k1gFnSc2	éra
francouzského	francouzský	k2eAgInSc2d1	francouzský
i	i	k8xC	i
světového	světový	k2eAgInSc2d1	světový
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgMnS	začínat
jako	jako	k9	jako
herec	herec	k1gMnSc1	herec
v	v	k7c6	v
kočovné	kočovný	k2eAgFnSc6d1	kočovná
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
vlastního	vlastní	k2eAgInSc2d1	vlastní
kočovného	kočovný	k2eAgInSc2d1	kočovný
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c4	po
třináct	třináct	k4xCc4	třináct
let	léto	k1gNnPc2	léto
po	po	k7c6	po
francouzském	francouzský	k2eAgInSc6d1	francouzský
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
<g/>
,	,	kIx,	,
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
také	také	k9	také
na	na	k7c6	na
královském	královský	k2eAgInSc6d1	královský
dvoře	dvůr	k1gInSc6	dvůr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
často	často	k6eAd1	často
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
hudebním	hudební	k2eAgMnSc7d1	hudební
skladatelem	skladatel	k1gMnSc7	skladatel
Jeanem-Baptistem	Jeanem-Baptist	k1gInSc7	Jeanem-Baptist
Lullym	Lullym	k1gInSc1	Lullym
<g/>
.	.	kIx.	.
</s>
<s>
Moliè	Moliè	k?	Moliè
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
zejména	zejména	k9	zejména
tzv.	tzv.	kA	tzv.
nízkým	nízký	k2eAgInPc3d1	nízký
literárním	literární	k2eAgInPc3d1	literární
žánrům	žánr	k1gInPc3	žánr
(	(	kIx(	(
<g/>
satira	satira	k1gFnSc1	satira
<g/>
,	,	kIx,	,
bajka	bajka	k1gFnSc1	bajka
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
fraška	fraška	k1gFnSc1	fraška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
odvážná	odvážný	k2eAgFnSc1d1	odvážná
zejména	zejména	k9	zejména
v	v	k7c6	v
kritice	kritika	k1gFnSc6	kritika
mravů	mrav	k1gInPc2	mrav
a	a	k8xC	a
společenských	společenský	k2eAgInPc2d1	společenský
poměrů	poměr	k1gInPc2	poměr
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
satirických	satirický	k2eAgFnPc6d1	satirická
veselohrách	veselohra	k1gFnPc6	veselohra
zesměšňoval	zesměšňovat	k5eAaImAgInS	zesměšňovat
pokrytectví	pokrytectví	k1gNnSc4	pokrytectví
<g/>
,	,	kIx,	,
šlechtu	šlechta	k1gFnSc4	šlechta
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
snobskou	snobský	k2eAgFnSc4d1	snobská
morálku	morálka	k1gFnSc4	morálka
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zatracovanou	zatracovaný	k2eAgFnSc4d1	zatracovaná
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Tvořil	tvořit	k5eAaImAgInS	tvořit
v	v	k7c6	v
typově	typově	k6eAd1	typově
barvité	barvitý	k2eAgFnSc6d1	barvitá
škále	škála	k1gFnSc6	škála
veseloher	veselohra	k1gFnPc2	veselohra
<g/>
,	,	kIx,	,
od	od	k7c2	od
lidové	lidový	k2eAgFnSc2d1	lidová
frašky	fraška	k1gFnSc2	fraška
po	po	k7c4	po
propracovanou	propracovaný	k2eAgFnSc4d1	propracovaná
charakterovou	charakterový	k2eAgFnSc4d1	charakterová
zápletku	zápletka	k1gFnSc4	zápletka
<g/>
.	.	kIx.	.
</s>
<s>
Problematika	problematika	k1gFnSc1	problematika
Moliè	Moliè	k1gFnSc2	Moliè
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
(	(	kIx(	(
<g/>
lakota	lakota	k1gFnSc1	lakota
<g/>
,	,	kIx,	,
pokrytectví	pokrytectví	k1gNnSc1	pokrytectví
<g/>
,	,	kIx,	,
zištnost	zištnost	k1gFnSc1	zištnost
<g/>
,	,	kIx,	,
cynismus	cynismus	k1gInSc1	cynismus
<g/>
,	,	kIx,	,
postavení	postavení	k1gNnSc1	postavení
žen	žena	k1gFnPc2	žena
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
je	být	k5eAaImIp3nS	být
také	také	k9	také
zřetelná	zřetelný	k2eAgFnSc1d1	zřetelná
inspirace	inspirace	k1gFnSc1	inspirace
komedií	komedie	k1gFnPc2	komedie
dell	della	k1gFnPc2	della
<g/>
'	'	kIx"	'
<g/>
arte	arte	k1gFnPc2	arte
<g/>
.	.	kIx.	.
</s>
<s>
Moliè	Moliè	k?	Moliè
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Poquelin	Poquelin	k2eAgInSc1d1	Poquelin
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1622	[number]	k4	1622
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
dvorní	dvorní	k2eAgMnSc1d1	dvorní
čalouník	čalouník	k1gMnSc1	čalouník
<g/>
.	.	kIx.	.
</s>
<s>
Moliére	Moliér	k1gMnSc5	Moliér
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
clermontské	clermontský	k2eAgFnSc6d1	clermontský
jezuitské	jezuitský	k2eAgFnSc6d1	jezuitská
koleji	kolej	k1gFnSc6	kolej
<g/>
,	,	kIx,	,
právnický	právnický	k2eAgInSc1d1	právnický
diplom	diplom	k1gInSc1	diplom
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
Orléansu	Orléans	k1gInSc6	Orléans
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1643	[number]	k4	1643
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
za	za	k7c2	za
patronace	patronace	k1gFnSc2	patronace
dramatika	dramatik	k1gMnSc2	dramatik
Pierra	Pierr	k1gMnSc2	Pierr
Corneille	Corneill	k1gMnSc2	Corneill
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Madeleine	Madeleine	k1gFnSc7	Madeleine
Béjátrovu	Béjátrův	k2eAgFnSc4d1	Béjátrův
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
herci	herec	k1gMnPc7	herec
divadelní	divadelní	k2eAgInSc4d1	divadelní
soubor	soubor	k1gInSc4	soubor
zvaný	zvaný	k2eAgInSc4d1	zvaný
Skvělé	skvělý	k2eAgNnSc4d1	skvělé
divadlo	divadlo	k1gNnSc4	divadlo
(	(	kIx(	(
<g/>
Illustre	Illustr	k1gInSc5	Illustr
théâtre	théâtr	k1gInSc5	théâtr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nekompromitoval	kompromitovat	k5eNaBmAgMnS	kompromitovat
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
používat	používat	k5eAaImF	používat
jméno	jméno	k1gNnSc4	jméno
Moliè	Moliè	k1gFnSc2	Moliè
<g/>
.	.	kIx.	.
</s>
<s>
Pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
dobýt	dobýt	k5eAaPmF	dobýt
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nedařilo	dařit	k5eNaImAgNnS	dařit
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
skončil	skončit	k5eAaPmAgMnS	skončit
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
pro	pro	k7c4	pro
dlužníky	dlužník	k1gMnPc4	dlužník
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
divadelními	divadelní	k2eAgFnPc7d1	divadelní
společnostmi	společnost	k1gFnPc7	společnost
kočoval	kočovat	k5eAaImAgMnS	kočovat
převážně	převážně	k6eAd1	převážně
po	po	k7c6	po
jihu	jih	k1gInSc6	jih
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1658	[number]	k4	1658
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
v	v	k7c4	v
Rouenu	Rouen	k2eAgFnSc4d1	Rouen
společnost	společnost	k1gFnSc4	společnost
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
složila	složit	k5eAaPmAgFnS	složit
hold	hold	k1gInSc4	hold
P.	P.	kA	P.
Corneillovi	Corneill	k1gMnSc3	Corneill
uváděním	uvádění	k1gNnSc7	uvádění
jeho	jeho	k3xOp3gNnPc2	jeho
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
podporu	podpora	k1gFnSc4	podpora
králova	králův	k2eAgMnSc2d1	králův
bratra	bratr	k1gMnSc2	bratr
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
tak	tak	k6eAd1	tak
možnost	možnost	k1gFnSc4	možnost
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
Malém	malý	k2eAgInSc6d1	malý
Bourbonském	bourbonský	k2eAgInSc6d1	bourbonský
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
střídal	střídat	k5eAaImAgInS	střídat
s	s	k7c7	s
italskými	italský	k2eAgMnPc7d1	italský
herci	herec	k1gMnPc7	herec
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1660	[number]	k4	1660
pak	pak	k6eAd1	pak
v	v	k7c6	v
Palais	Palais	k1gFnSc6	Palais
Royal	Royal	k1gInSc4	Royal
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
získal	získat	k5eAaPmAgInS	získat
přízeň	přízeň	k1gFnSc4	přízeň
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Lullym	Lullymum	k1gNnPc2	Lullymum
organizátorem	organizátor	k1gInSc7	organizátor
královských	královský	k2eAgFnPc2d1	královská
slavností	slavnost	k1gFnPc2	slavnost
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
přiznán	přiznán	k2eAgInSc4d1	přiznán
královský	královský	k2eAgInSc4d1	královský
důchod	důchod	k1gInSc4	důchod
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1665	[number]	k4	1665
jeho	on	k3xPp3gNnSc2	on
soubor	soubor	k1gInSc1	soubor
nesl	nést	k5eAaImAgInS	nést
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
divadelní	divadelní	k2eAgFnSc1d1	divadelní
společnost	společnost	k1gFnSc1	společnost
králova	králův	k2eAgFnSc1d1	králova
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
úspěch	úspěch	k1gInSc1	úspěch
na	na	k7c6	na
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
novátorství	novátorství	k1gNnSc1	novátorství
<g/>
,	,	kIx,	,
společenská	společenský	k2eAgFnSc1d1	společenská
angažovanost	angažovanost	k1gFnSc1	angažovanost
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
i	i	k9	i
úspěch	úspěch	k1gInSc1	úspěch
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
ho	on	k3xPp3gMnSc2	on
nutily	nutit	k5eAaImAgFnP	nutit
bojovat	bojovat	k5eAaImF	bojovat
celé	celý	k2eAgInPc4d1	celý
roky	rok	k1gInPc4	rok
proti	proti	k7c3	proti
intrikám	intrika	k1gFnPc3	intrika
<g/>
,	,	kIx,	,
osočování	osočování	k1gNnSc2	osočování
a	a	k8xC	a
pronásledování	pronásledování	k1gNnSc2	pronásledování
jeho	jeho	k3xOp3gMnPc2	jeho
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
i	i	k9	i
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
konkurenty	konkurent	k1gMnPc4	konkurent
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1673	[number]	k4	1673
po	po	k7c6	po
konci	konec	k1gInSc6	konec
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
představení	představení	k1gNnSc2	představení
své	svůj	k3xOyFgFnSc2	svůj
poslední	poslední	k2eAgFnSc2d1	poslední
hry	hra	k1gFnSc2	hra
Zdravý	zdravý	k1gMnSc1	zdravý
nemocný	nemocný	k1gMnSc1	nemocný
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tímto	tento	k3xDgNnSc7	tento
představením	představení	k1gNnSc7	představení
Moliè	Moliè	k1gMnSc2	Moliè
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
bude	být	k5eAaImBp3nS	být
špatně	špatně	k6eAd1	špatně
hrát	hrát	k5eAaImF	hrát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Chvěl	chvět	k5eAaImAgInS	chvět
se	se	k3xPyFc4	se
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
mu	on	k3xPp3gMnSc3	on
hořela	hořet	k5eAaImAgFnS	hořet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ruce	ruka	k1gFnPc4	ruka
měl	mít	k5eAaImAgMnS	mít
studené	studený	k2eAgNnSc1d1	studené
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
rozhodně	rozhodně	k6eAd1	rozhodně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
návrh	návrh	k1gInSc4	návrh
kolegů	kolega	k1gMnPc2	kolega
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
představení	představení	k1gNnSc1	představení
zrušilo	zrušit	k5eAaPmAgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Nechtěl	chtít	k5eNaImAgMnS	chtít
zklamat	zklamat	k5eAaPmF	zklamat
početné	početný	k2eAgNnSc4d1	početné
publikum	publikum	k1gNnSc4	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
horečce	horečka	k1gFnSc6	horečka
sváděl	svádět	k5eAaImAgInS	svádět
zoufalý	zoufalý	k2eAgInSc1d1	zoufalý
boj	boj	k1gInSc1	boj
s	s	k7c7	s
přívaly	příval	k1gInPc7	příval
kašle	kašel	k1gInSc2	kašel
a	a	k8xC	a
krvavými	krvavý	k2eAgInPc7d1	krvavý
hleny	hlen	k1gInPc7	hlen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
překrýval	překrývat	k5eAaImAgInS	překrývat
smíchem	smích	k1gInSc7	smích
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vše	všechen	k3xTgNnSc1	všechen
vypadalo	vypadat	k5eAaImAgNnS	vypadat
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
ve	v	k7c6	v
scénáři	scénář	k1gInSc6	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
hra	hra	k1gFnSc1	hra
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
,	,	kIx,	,
diváci	divák	k1gMnPc1	divák
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
se	se	k3xPyFc4	se
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
přidal	přidat	k5eAaPmAgMnS	přidat
i	i	k9	i
de	de	k?	de
Lully	Lulla	k1gFnPc1	Lulla
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
však	však	k9	však
nedostavil	dostavit	k5eNaPmAgMnS	dostavit
<g/>
)	)	kIx)	)
začali	začít	k5eAaPmAgMnP	začít
nadšeně	nadšeně	k6eAd1	nadšeně
tleskat	tleskat	k5eAaImF	tleskat
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
však	však	k9	však
neměl	mít	k5eNaImAgMnS	mít
tušení	tušení	k1gNnSc4	tušení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Moliè	Moliè	k1gMnSc1	Moliè
svedl	svést	k5eAaPmAgMnS	svést
poslední	poslední	k2eAgInSc4d1	poslední
boj	boj	k1gInSc4	boj
o	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
zmizel	zmizet	k5eAaPmAgMnS	zmizet
z	z	k7c2	z
jeviště	jeviště	k1gNnSc2	jeviště
<g/>
,	,	kIx,	,
spadl	spadnout	k5eAaPmAgInS	spadnout
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
silně	silně	k6eAd1	silně
kašlat	kašlat	k5eAaImF	kašlat
a	a	k8xC	a
na	na	k7c6	na
rtech	ret	k1gInPc6	ret
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
objevily	objevit	k5eAaPmAgFnP	objevit
krvavé	krvavý	k2eAgFnPc1d1	krvavá
sraženiny	sraženina	k1gFnPc1	sraženina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
kolegové	kolega	k1gMnPc1	kolega
nevěděli	vědět	k5eNaImAgMnP	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
Moliè	Moliè	k1gFnSc1	Moliè
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
,	,	kIx,	,
a	a	k8xC	a
odvezli	odvézt	k5eAaPmAgMnP	odvézt
ho	on	k3xPp3gMnSc4	on
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
dostal	dostat	k5eAaPmAgMnS	dostat
další	další	k2eAgInSc4d1	další
záchvat	záchvat	k1gInSc4	záchvat
a	a	k8xC	a
v	v	k7c4	v
deset	deset	k4xCc4	deset
hodin	hodina	k1gFnPc2	hodina
večer	večer	k6eAd1	večer
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Moliè	Moliè	k?	Moliè
nestačil	stačit	k5eNaBmAgInS	stačit
přijmout	přijmout	k5eAaPmF	přijmout
svátost	svátost	k1gFnSc4	svátost
umírajících	umírající	k2eAgMnPc2d1	umírající
a	a	k8xC	a
odříci	odříct	k5eAaPmF	odříct
se	se	k3xPyFc4	se
,,	,,	k?	,,
<g/>
hříšné	hříšný	k2eAgFnPc4d1	hříšná
<g/>
"	"	kIx"	"
herecké	herecký	k2eAgFnPc4d1	herecká
profese	profes	k1gFnPc4	profes
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
kněz	kněz	k1gMnSc1	kněz
dostavil	dostavit	k5eAaPmAgMnS	dostavit
<g/>
)	)	kIx)	)
a	a	k8xC	a
kněžstvo	kněžstvo	k1gNnSc1	kněžstvo
jeho	jeho	k3xOp3gFnSc2	jeho
farnosti	farnost	k1gFnSc2	farnost
se	se	k3xPyFc4	se
zdráhalo	zdráhat	k5eAaImAgNnS	zdráhat
jej	on	k3xPp3gNnSc4	on
křesťansky	křesťansky	k6eAd1	křesťansky
pohřbít	pohřbít	k5eAaPmF	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
krále	král	k1gMnSc2	král
však	však	k8xC	však
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1673	[number]	k4	1673
ve	v	k7c6	v
večerních	večerní	k2eAgFnPc6d1	večerní
hodinách	hodina	k1gFnPc6	hodina
pochován	pochovat	k5eAaPmNgInS	pochovat
podle	podle	k7c2	podle
církevních	církevní	k2eAgInPc2d1	církevní
obřadů	obřad	k1gInPc2	obřad
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
bez	bez	k7c2	bez
zádušních	zádušní	k2eAgFnPc2d1	zádušní
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
<g/>
)	)	kIx)	)
na	na	k7c6	na
Svatojosefském	Svatojosefský	k2eAgInSc6d1	Svatojosefský
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Montmartre	Montmartr	k1gMnSc5	Montmartr
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
části	část	k1gFnSc6	část
hřbitova	hřbitov	k1gInSc2	hřbitov
určené	určený	k2eAgFnPc4d1	určená
pro	pro	k7c4	pro
sebevrahy	sebevrah	k1gMnPc4	sebevrah
a	a	k8xC	a
nekřtěňátka	nekřtěňátko	k1gNnPc4	nekřtěňátko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
exhumovány	exhumován	k2eAgInPc1d1	exhumován
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
spočívají	spočívat	k5eAaImIp3nP	spočívat
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
hřbitově	hřbitov	k1gInSc6	hřbitov
Pè	Pè	k1gFnSc2	Pè
Lachaise	Lachaise	k1gFnSc2	Lachaise
<g/>
.	.	kIx.	.
</s>
<s>
Moliè	Moliè	k?	Moliè
dílo	dílo	k1gNnSc1	dílo
tvoří	tvořit	k5eAaImIp3nS	tvořit
převážně	převážně	k6eAd1	převážně
satirické	satirický	k2eAgFnPc1d1	satirická
komedie	komedie	k1gFnPc1	komedie
<g/>
,	,	kIx,	,
veršované	veršovaný	k2eAgFnPc1d1	veršovaná
(	(	kIx(	(
<g/>
Misantrop	misantrop	k1gMnSc1	misantrop
<g/>
)	)	kIx)	)
i	i	k9	i
prozaické	prozaický	k2eAgFnPc1d1	prozaická
(	(	kIx(	(
<g/>
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tematicky	tematicky	k6eAd1	tematicky
se	se	k3xPyFc4	se
zaměřující	zaměřující	k2eAgFnSc1d1	zaměřující
na	na	k7c4	na
nešvary	nešvara	k1gMnPc4	nešvara
francouzské	francouzský	k2eAgFnSc2d1	francouzská
vyšší	vysoký	k2eAgFnSc2d2	vyšší
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
lakota	lakota	k1gFnSc1	lakota
<g/>
,	,	kIx,	,
naivita	naivita	k1gFnSc1	naivita
<g/>
,	,	kIx,	,
pokrytectví	pokrytectví	k1gNnSc1	pokrytectví
<g/>
,	,	kIx,	,
hypochondři	hypochondr	k1gMnPc1	hypochondr
a	a	k8xC	a
především	především	k6eAd1	především
snobství	snobství	k1gNnSc1	snobství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zápletka	zápletka	k1gFnSc1	zápletka
komedií	komedie	k1gFnPc2	komedie
nebývá	bývat	k5eNaImIp3nS	bývat
příliš	příliš	k6eAd1	příliš
důmyslná	důmyslný	k2eAgFnSc1d1	důmyslná
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
točí	točit	k5eAaImIp3nS	točit
kolem	kolem	k7c2	kolem
sbližování	sbližování	k1gNnSc2	sbližování
milenců	milenec	k1gMnPc2	milenec
nebo	nebo	k8xC	nebo
nevěry	nevěra	k1gFnSc2	nevěra
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
ovšem	ovšem	k9	ovšem
velmi	velmi	k6eAd1	velmi
důmyslný	důmyslný	k2eAgInSc1d1	důmyslný
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
humor	humor	k1gInSc1	humor
-	-	kIx~	-
zde	zde	k6eAd1	zde
vždy	vždy	k6eAd1	vždy
činí	činit	k5eAaImIp3nS	činit
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
cílovou	cílový	k2eAgFnSc7d1	cílová
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
která	který	k3yRgFnSc1	který
hra	hra	k1gFnSc1	hra
určena	určit	k5eAaPmNgFnS	určit
-	-	kIx~	-
a	a	k8xC	a
břitkou	břitký	k2eAgFnSc4d1	břitká
a	a	k8xC	a
výstižnou	výstižný	k2eAgFnSc4d1	výstižná
satiru	satira	k1gFnSc4	satira
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
postavy	postava	k1gFnPc1	postava
a	a	k8xC	a
fráze	fráze	k1gFnSc1	fráze
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
her	hra	k1gFnPc2	hra
jsou	být	k5eAaImIp3nP	být
natolik	natolik	k6eAd1	natolik
známé	známý	k2eAgFnPc1d1	známá
<g/>
,	,	kIx,	,
že	že	k8xS	že
přešly	přejít	k5eAaPmAgInP	přejít
i	i	k9	i
do	do	k7c2	do
běžné	běžný	k2eAgFnSc2d1	běžná
řeči	řeč	k1gFnSc2	řeč
(	(	kIx(	(
<g/>
např.	např.	kA	např.
slovo	slovo	k1gNnSc1	slovo
harpagon	harpagon	k1gInSc1	harpagon
označuje	označovat	k5eAaImIp3nS	označovat
lakomce	lakomec	k1gMnSc4	lakomec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Létající	létající	k2eAgMnSc1d1	létající
doktor	doktor	k1gMnSc1	doktor
(	(	kIx(	(
<g/>
1645	[number]	k4	1645
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc1	Le
Médecin	Médecin	k1gMnSc1	Médecin
volant	volant	k1gInSc1	volant
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fraška	fraška	k1gFnSc1	fraška
Moliè	Moliè	k1gFnPc2	Moliè
kočovné	kočovný	k2eAgFnSc2d1	kočovná
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Podvodný	podvodný	k2eAgMnSc1d1	podvodný
lékař	lékař	k1gMnSc1	lékař
nebo	nebo	k8xC	nebo
Příležitost	příležitost	k1gFnSc1	příležitost
dělá	dělat	k5eAaImIp3nS	dělat
lékaře	lékař	k1gMnPc4	lékař
<g/>
,	,	kIx,	,
Žárlivý	žárlivý	k2eAgMnSc1d1	žárlivý
Petřík	Petřík	k1gMnSc1	Petřík
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1650	[number]	k4	1650
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Jalousie	Jalousie	k1gFnSc2	Jalousie
du	du	k?	du
barbouillé	barbouillý	k2eAgFnPc1d1	barbouillý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fraška	fraška	k1gFnSc1	fraška
Moliè	Moliè	k1gFnSc1	Moliè
<g />
.	.	kIx.	.
</s>
<s>
kočovné	kočovný	k2eAgFnPc1d1	kočovná
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
Potřeštěnec	potřeštěnec	k1gMnSc1	potřeštěnec
(	(	kIx(	(
<g/>
1655	[number]	k4	1655
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Étourdi	Étourd	k1gMnPc1	Étourd
ou	ou	k0	ou
les	les	k1gInSc1	les
Contretemps	Contretemps	k1gInSc1	Contretemps
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Ztřeštěnec	ztřeštěnec	k1gMnSc1	ztřeštěnec
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
Popleta	popleta	k1gFnSc1	popleta
aneb	aneb	k?	aneb
láska	láska	k1gFnSc1	láska
s	s	k7c7	s
překážkami	překážka	k1gFnPc7	překážka
<g/>
,	,	kIx,	,
též	též	k9	též
jako	jako	k9	jako
Janek	Janka	k1gFnPc2	Janka
<g/>
,	,	kIx,	,
Hoře	hora	k1gFnSc6	hora
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
1656	[number]	k4	1656
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc7	Le
Dépit	Dépit	k2eAgInSc1d1	Dépit
amoureux	amoureux	k1gInSc1	amoureux
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
v	v	k7c6	v
Béziersu	Béziers	k1gInSc6	Béziers
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Trampoty	trampota	k1gFnPc1	trampota
zamilovaných	zamilovaný	k1gMnPc2	zamilovaný
<g/>
,	,	kIx,	,
Zamilovaný	zamilovaný	k2eAgMnSc1d1	zamilovaný
doktor	doktor	k1gMnSc1	doktor
(	(	kIx(	(
<g/>
1658	[number]	k4	1658
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc1	Le
Docteur	Docteur	k1gMnSc1	Docteur
amoureux	amoureux	k1gInSc1	amoureux
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fraška	fraška	k1gFnSc1	fraška
<g/>
,	,	kIx,	,
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
komedií	komedie	k1gFnSc7	komedie
dell	dell	k1gMnSc1	dell
<g/>
'	'	kIx"	'
<g/>
arte	arte	k1gInSc1	arte
<g/>
,	,	kIx,	,
úspěšně	úspěšně	k6eAd1	úspěšně
sehraná	sehraný	k2eAgFnSc1d1	sehraná
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Louvre	Louvre	k1gInSc1	Louvre
před	před	k7c7	před
králem	král	k1gMnSc7	král
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
počátek	počátek	k1gInSc4	počátek
Moliè	Moliè	k1gFnSc2	Moliè
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
u	u	k7c2	u
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
Směšné	směšný	k2eAgFnSc2d1	směšná
preciózky	preciózka	k1gFnSc2	preciózka
(	(	kIx(	(
<g/>
1659	[number]	k4	1659
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Précieuses	Précieuses	k1gMnSc1	Précieuses
ridicules	ridicules	k1gMnSc1	ridicules
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
jednoaktová	jednoaktový	k2eAgFnSc1d1	jednoaktová
komedie	komedie	k1gFnSc1	komedie
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
<g/>
,	,	kIx,	,
smělý	smělý	k2eAgInSc1d1	smělý
satirický	satirický	k2eAgInSc1d1	satirický
útok	útok	k1gInSc1	útok
na	na	k7c4	na
aristokracii	aristokracie	k1gFnSc4	aristokracie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
salónech	salón	k1gInPc6	salón
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
vyumělkované	vyumělkovaný	k2eAgNnSc4d1	vyumělkované
nepřirozené	přirozený	k2eNgNnSc4d1	nepřirozené
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
mluvila	mluvit	k5eAaImAgFnS	mluvit
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
stejně	stejně	k6eAd1	stejně
nepřirozenou	přirozený	k2eNgFnSc7d1	nepřirozená
a	a	k8xC	a
vyumělkovanou	vyumělkovaný	k2eAgFnSc7d1	vyumělkovaná
řečí	řeč	k1gFnSc7	řeč
o	o	k7c6	o
konvenčních	konvenční	k2eAgInPc6d1	konvenční
a	a	k8xC	a
banálních	banální	k2eAgInPc6d1	banální
problémech	problém	k1gInPc6	problém
<g/>
.	.	kIx.	.
</s>
<s>
Sganarelle	Sganarella	k1gFnSc3	Sganarella
aneb	aneb	k?	aneb
Domnělý	domnělý	k2eAgMnSc1d1	domnělý
paroháč	paroháč	k1gMnSc1	paroháč
(	(	kIx(	(
<g/>
1660	[number]	k4	1660
<g/>
,	,	kIx,	,
Sganarelle	Sganarelle	k1gFnSc1	Sganarelle
ou	ou	k0	ou
le	le	k?	le
Cocu	Cocum	k1gNnSc3	Cocum
imaginaire	imaginair	k1gMnSc5	imaginair
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoaktová	jednoaktový	k2eAgFnSc1d1	jednoaktová
komedie	komedie	k1gFnSc1	komedie
řešící	řešící	k2eAgFnSc4d1	řešící
otázku	otázka	k1gFnSc4	otázka
svobody	svoboda	k1gFnSc2	svoboda
citů	cit	k1gInPc2	cit
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Skoroměl	Skoroměl	k1gMnSc1	Skoroměl
čili	čili	k8xC	čili
Domnělý	domnělý	k2eAgMnSc1d1	domnělý
rohoun	rohoun	k1gMnSc1	rohoun
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
Garcia	Garcium	k1gNnSc2	Garcium
Navarrský	navarrský	k2eAgInSc4d1	navarrský
aneb	aneb	k?	aneb
Žárlivý	žárlivý	k2eAgMnSc1d1	žárlivý
princ	princ	k1gMnSc1	princ
(	(	kIx(	(
<g/>
1661	[number]	k4	1661
<g/>
,	,	kIx,	,
Dom	Dom	k?	Dom
Garcie	Garcie	k1gFnSc1	Garcie
de	de	k?	de
Navarre	Navarr	k1gInSc5	Navarr
<g />
.	.	kIx.	.
</s>
<s>
ou	ou	k0	ou
le	le	k?	le
Prince	princa	k1gFnSc3	princa
jaloux	jaloux	k1gInSc1	jaloux
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ne	ne	k9	ne
příliš	příliš	k6eAd1	příliš
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
heroická	heroický	k2eAgFnSc1d1	heroická
komedie	komedie	k1gFnSc1	komedie
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
vysoké	vysoký	k2eAgFnSc2d1	vysoká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
pro	pro	k7c4	pro
muže	muž	k1gMnSc4	muž
(	(	kIx(	(
<g/>
1661	[number]	k4	1661
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
École	École	k1gInSc1	École
des	des	k1gNnSc1	des
maris	maris	k1gInSc1	maris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mravoličná	mravoličný	k2eAgFnSc1d1	mravoličná
veršovaná	veršovaný	k2eAgFnSc1d1	veršovaná
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
dějstvích	dějství	k1gNnPc6	dějství
ze	z	k7c2	z
života	život	k1gInSc2	život
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Protivové	Protivové	k?	Protivové
(	(	kIx(	(
<g/>
1661	[number]	k4	1661
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Fâcheux	Fâcheux	k1gInSc1	Fâcheux
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie-balet	komediealet	k1gInSc1	komedie-balet
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
Pierre	Pierr	k1gInSc5	Pierr
Beauchamp	Beauchamp	k1gInSc1	Beauchamp
a	a	k8xC	a
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lully	Lull	k1gInPc1	Lull
<g/>
,	,	kIx,	,
satirické	satirický	k2eAgInPc1d1	satirický
obrázky	obrázek	k1gInPc1	obrázek
členů	člen	k1gInPc2	člen
dvorské	dvorský	k2eAgFnSc2d1	dvorská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Lidé	člověk	k1gMnPc1	člověk
obtížní	obtížný	k2eAgMnPc1d1	obtížný
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
(	(	kIx(	(
<g/>
1662	[number]	k4	1662
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
École	École	k1gFnSc1	École
<g />
.	.	kIx.	.
</s>
<s>
des	des	k1gNnSc1	des
femmes	femmesa	k1gFnPc2	femmesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mravoličná	mravoličný	k2eAgFnSc1d1	mravoličná
veršovaná	veršovaný	k2eAgFnSc1d1	veršovaná
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
pěti	pět	k4xCc6	pět
dějstvích	dějství	k1gNnPc6	dějství
řešící	řešící	k2eAgInSc4d1	řešící
problém	problém	k1gInSc4	problém
výchovy	výchova	k1gFnSc2	výchova
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
postavení	postavení	k1gNnSc2	postavení
ženy	žena	k1gFnSc2	žena
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
Kritika	kritika	k1gFnSc1	kritika
školy	škola	k1gFnSc2	škola
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
(	(	kIx(	(
<g/>
1663	[number]	k4	1663
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Critique	Critiqu	k1gFnSc2	Critiqu
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
école	école	k1gInSc1	école
des	des	k1gNnSc1	des
femmes	femmes	k1gInSc1	femmes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
<g />
.	.	kIx.	.
</s>
<s>
Moliè	Moliè	k?	Moliè
napsal	napsat	k5eAaPmAgInS	napsat
jako	jako	k9	jako
obranu	obrana	k1gFnSc4	obrana
svých	svůj	k3xOyFgFnPc2	svůj
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
autorův	autorův	k2eAgInSc1d1	autorův
literární	literární	k2eAgInSc1d1	literární
a	a	k8xC	a
estetický	estetický	k2eAgInSc1d1	estetický
manifest	manifest	k1gInSc1	manifest
<g/>
,	,	kIx,	,
Versaillská	Versaillský	k2eAgFnSc1d1	Versaillská
improvizace	improvizace	k1gFnSc1	improvizace
(	(	kIx(	(
<g/>
1663	[number]	k4	1663
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Impromptu	impromptu	k1gNnPc2	impromptu
de	de	k?	de
Versailles	Versailles	k1gFnSc2	Versailles
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
polemická	polemický	k2eAgFnSc1d1	polemická
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
jednom	jeden	k4xCgInSc6	jeden
dějství	dějství	k1gNnSc6	dějství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
Moliè	Moliè	k1gFnSc1	Moliè
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
své	svůj	k3xOyFgMnPc4	svůj
herce	herec	k1gMnPc4	herec
při	při	k7c6	při
<g />
.	.	kIx.	.
</s>
<s>
divadelní	divadelní	k2eAgFnSc3d1	divadelní
zkoušce	zkouška	k1gFnSc3	zkouška
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
neustále	neustále	k6eAd1	neustále
rušena	rušen	k2eAgFnSc1d1	rušena
nepřátelsky	přátelsky	k6eNd1	přátelsky
naladěnými	naladěný	k2eAgMnPc7d1	naladěný
příslušníky	příslušník	k1gMnPc7	příslušník
dvorské	dvorský	k2eAgFnSc2d1	dvorská
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
Sňatek	sňatek	k1gInSc1	sňatek
z	z	k7c2	z
donucení	donucení	k1gNnSc2	donucení
(	(	kIx(	(
<g/>
1664	[number]	k4	1664
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc1	Le
Mariage	Mariag	k1gInSc2	Mariag
forcé	forcá	k1gFnSc2	forcá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie-balet	komediealet	k1gInSc1	komedie-balet
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
Jeana-Baptisty	Jeana-Baptista	k1gMnSc2	Jeana-Baptista
Lullyho	Lully	k1gMnSc2	Lully
přepracovaná	přepracovaný	k2eAgFnSc1d1	přepracovaná
do	do	k7c2	do
jednoaktové	jednoaktový	k2eAgFnSc2d1	jednoaktová
frašky	fraška	k1gFnSc2	fraška
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Manžel	manžel	k1gMnSc1	manžel
z	z	k7c2	z
donucení	donucení	k1gNnSc2	donucení
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Princezna	princezna	k1gFnSc1	princezna
z	z	k7c2	z
Ellidy	Ellida	k1gFnSc2	Ellida
(	(	kIx(	(
<g/>
1664	[number]	k4	1664
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Princesse	Princesse	k1gFnSc2	Princesse
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Élide	Élid	k1gMnSc5	Élid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pastorální	pastorální	k2eAgFnSc1d1	pastorální
komedie	komedie	k1gFnSc1	komedie
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
Jeana-Baptisty	Jeana-Baptista	k1gMnSc2	Jeana-Baptista
Lullyho	Lully	k1gMnSc2	Lully
napsaná	napsaný	k2eAgFnSc1d1	napsaná
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
několikadenní	několikadenní	k2eAgFnSc4d1	několikadenní
slavnost	slavnost	k1gFnSc4	slavnost
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
,	,	kIx,	,
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
neboli	neboli	k8xC	neboli
Pokrytec	pokrytec	k1gMnSc1	pokrytec
(	(	kIx(	(
<g/>
1664	[number]	k4	1664
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
ou	ou	k0	ou
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Imposteur	Imposteur	k1gMnSc1	Imposteur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostře	ostro	k6eAd1	ostro
satirická	satirický	k2eAgFnSc1d1	satirická
veršovaná	veršovaný	k2eAgFnSc1d1	veršovaná
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
pěti	pět	k4xCc6	pět
jednáních	jednání	k1gNnPc6	jednání
spjatá	spjatý	k2eAgFnSc1d1	spjatá
s	s	k7c7	s
politickou	politický	k2eAgFnSc7d1	politická
situací	situace	k1gFnSc7	situace
Francie	Francie	k1gFnSc2	Francie
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgMnSc1d1	titulní
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
svatoušek	svatoušek	k1gMnSc1	svatoušek
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vetře	vetřít	k5eAaPmIp3nS	vetřít
do	do	k7c2	do
přízně	přízeň	k1gFnSc2	přízeň
zámožného	zámožný	k2eAgMnSc2d1	zámožný
měšťana	měšťan	k1gMnSc2	měšťan
Orgona	Orgon	k1gMnSc2	Orgon
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
ovládne	ovládnout	k5eAaPmIp3nS	ovládnout
celou	celý	k2eAgFnSc4d1	celá
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Orgon	Orgon	k1gInSc1	Orgon
mu	on	k3xPp3gMnSc3	on
věří	věřit	k5eAaImIp3nS	věřit
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
mu	on	k3xPp3gMnSc3	on
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
i	i	k8xC	i
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
odhalení	odhalení	k1gNnSc4	odhalení
přichází	přicházet	k5eAaImIp3nS	přicházet
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
Orgona	Orgon	k1gMnSc2	Orgon
vyhání	vyhánět	k5eAaImIp3nS	vyhánět
z	z	k7c2	z
domu	dům	k1gInSc2	dům
a	a	k8xC	a
dostává	dostávat	k5eAaImIp3nS	dostávat
ho	on	k3xPp3gNnSc4	on
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
odhalen	odhalit	k5eAaPmNgMnS	odhalit
a	a	k8xC	a
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
míří	mířit	k5eAaImIp3nS	mířit
na	na	k7c4	na
církev	církev	k1gFnSc4	církev
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1669	[number]	k4	1669
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
o	o	k7c6	o
nevýznamnější	významný	k2eNgFnSc6d2	nevýznamnější
Moliè	Moliè	k1gFnSc6	Moliè
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
aneb	aneb	k?	aneb
Kamenná	kamenný	k2eAgFnSc1d1	kamenná
hostina	hostina	k1gFnSc1	hostina
(	(	kIx(	(
<g/>
1665	[number]	k4	1665
<g/>
,	,	kIx,	,
Dom	Dom	k?	Dom
Juan	Juan	k1gMnSc1	Juan
ou	ou	k0	ou
le	le	k?	le
Festin	Festin	k1gInSc1	Festin
de	de	k?	de
pierre	pierr	k1gMnSc5	pierr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
Moliè	Moliè	k1gFnPc2	Moliè
vrcholných	vrcholný	k2eAgFnPc2d1	vrcholná
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
tragikomedie	tragikomedie	k1gFnSc1	tragikomedie
o	o	k7c6	o
pěti	pět	k4xCc6	pět
dějstvích	dějství	k1gNnPc6	dějství
<g/>
,	,	kIx,	,
útočící	útočící	k2eAgMnSc1d1	útočící
opět	opět	k6eAd1	opět
na	na	k7c4	na
církev	církev	k1gFnSc4	církev
a	a	k8xC	a
pokrytectví	pokrytectví	k1gNnSc4	pokrytectví
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
autor	autor	k1gMnSc1	autor
svérázným	svérázný	k2eAgInSc7d1	svérázný
způsobem	způsob	k1gInSc7	způsob
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
starý	starý	k2eAgInSc4d1	starý
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
námět	námět	k1gInSc4	námět
o	o	k7c6	o
španělském	španělský	k2eAgNnSc6d1	španělské
svůdci	svůdce	k1gMnSc3	svůdce
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tato	tento	k3xDgFnSc1	tento
hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
dlouho	dlouho	k6eAd1	dlouho
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
,	,	kIx,	,
Moliè	Moliè	k1gFnSc1	Moliè
text	text	k1gInSc1	text
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
až	až	k9	až
roku	rok	k1gInSc2	rok
1682	[number]	k4	1682
<g/>
.	.	kIx.	.
</s>
<s>
Láska	láska	k1gFnSc1	láska
lékařem	lékař	k1gMnSc7	lékař
(	(	kIx(	(
<g/>
1665	[number]	k4	1665
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Amour	Amour	k1gMnSc1	Amour
médecin	médecin	k1gMnSc1	médecin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie-balet	komediealet	k1gInSc1	komedie-balet
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lully	Lullo	k1gNnPc7	Lullo
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k8xC	jako
Doktor	doktor	k1gMnSc1	doktor
Láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
Lékařem	lékař	k1gMnSc7	lékař
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
(	(	kIx(	(
<g/>
1666	[number]	k4	1666
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc1	Le
Médecin	Médecina	k1gFnPc2	Médecina
malgré	malgrý	k2eAgFnSc2d1	malgrý
lui	lui	k?	lui
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
dějstvích	dějství	k1gNnPc6	dějství
<g/>
,	,	kIx,	,
Misantrop	misantrop	k1gMnSc1	misantrop
aneb	aneb	k?	aneb
Zamilovaný	zamilovaný	k1gMnSc1	zamilovaný
mrzout	mrzout	k1gMnSc1	mrzout
(	(	kIx(	(
<g/>
1666	[number]	k4	1666
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc5	Le
Misanthrope	Misanthrop	k1gMnSc5	Misanthrop
ou	ou	k0	ou
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Atrabilaire	Atrabilair	k1gInSc5	Atrabilair
amoureux	amoureux	k1gInSc1	amoureux
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veršovaná	veršovaný	k2eAgFnSc1d1	veršovaná
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
pěti	pět	k4xCc6	pět
dějstvích	dějství	k1gNnPc6	dějství
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
vrcholných	vrcholný	k2eAgNnPc2d1	vrcholné
autorových	autorův	k2eAgNnPc2d1	autorovo
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
nabízí	nabízet	k5eAaImIp3nS	nabízet
stále	stále	k6eAd1	stále
aktuální	aktuální	k2eAgNnSc4d1	aktuální
téma	téma	k1gNnSc4	téma
<g/>
:	:	kIx,	:
má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
pragmatickému	pragmatický	k2eAgMnSc3d1	pragmatický
politikaření	politikařený	k2eAgMnPc1d1	politikařený
a	a	k8xC	a
profitovat	profitovat	k5eAaBmF	profitovat
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
má	mít	k5eAaImIp3nS	mít
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
svůj	svůj	k3xOyFgInSc4	svůj
protest	protest	k1gInSc4	protest
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stáhne	stáhnout	k5eAaPmIp3nS	stáhnout
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
postojů	postoj	k1gInPc2	postoj
není	být	k5eNaImIp3nS	být
lidsky	lidsky	k6eAd1	lidsky
důstojné	důstojný	k2eAgNnSc1d1	důstojné
řešení	řešení	k1gNnSc1	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Mélicerte	Mélicrat	k5eAaPmRp2nP	Mélicrat
(	(	kIx(	(
<g/>
1666	[number]	k4	1666
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pastorální	pastorální	k2eAgFnSc1d1	pastorální
heroická	heroický	k2eAgFnSc1d1	heroická
komedie	komedie	k1gFnSc1	komedie
ve	v	k7c6	v
verších	verš	k1gInPc6	verš
<g/>
,	,	kIx,	,
Komická	komický	k2eAgFnSc1d1	komická
pastorála	pastorála	k1gFnSc1	pastorála
(	(	kIx(	(
<g/>
1667	[number]	k4	1667
<g/>
,	,	kIx,	,
Pastorale	Pastoral	k1gMnSc5	Pastoral
comique	comiquus	k1gMnSc5	comiquus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pastorální	pastorální	k2eAgFnSc1d1	pastorální
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
Sicilián	Sicilián	k1gMnSc1	Sicilián
aneb	aneb	k?	aneb
láska	láska	k1gFnSc1	láska
malířem	malíř	k1gMnSc7	malíř
(	(	kIx(	(
<g/>
1667	[number]	k4	1667
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc1	Le
Sicilien	Sicilina	k1gFnPc2	Sicilina
ou	ou	k0	ou
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Amour	Amour	k1gMnSc1	Amour
peintre	peintr	k1gMnSc5	peintr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoaktová	jednoaktový	k2eAgFnSc1d1	jednoaktová
komedie	komedie	k1gFnSc1	komedie
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
<g/>
,	,	kIx,	,
Amfitryon	Amfitryon	k1gMnSc1	Amfitryon
(	(	kIx(	(
<g/>
1668	[number]	k4	1668
<g/>
,	,	kIx,	,
Amphitryon	Amphitryon	k1gInSc1	Amphitryon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinotajná	jinotajný	k2eAgFnSc1d1	jinotajná
veršovaná	veršovaný	k2eAgFnSc1d1	veršovaná
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
jednáních	jednání	k1gNnPc6	jednání
na	na	k7c4	na
antický	antický	k2eAgInSc4d1	antický
námět	námět	k1gInSc4	námět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
Moliè	Moliè	k1gMnSc1	Moliè
zesměšnil	zesměšnit	k5eAaPmAgMnS	zesměšnit
mravy	mrav	k1gInPc4	mrav
dvořanů	dvořan	k1gMnPc2	dvořan
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
vrcholných	vrcholný	k2eAgNnPc2d1	vrcholné
autorových	autorův	k2eAgNnPc2d1	autorovo
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Dudek	Dudek	k1gMnSc1	Dudek
aneb	aneb	k?	aneb
Napálený	napálený	k2eAgMnSc1d1	napálený
manžel	manžel	k1gMnSc1	manžel
(	(	kIx(	(
<g/>
1668	[number]	k4	1668
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Dandin	Dandina	k1gFnPc2	Dandina
ou	ou	k0	ou
le	le	k?	le
Mari	Mar	k1gInSc3	Mar
confondu	confond	k1gInSc2	confond
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
dějstvích	dějství	k1gNnPc6	dějství
uvedená	uvedený	k2eAgFnSc1d1	uvedená
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k9	jako
komedie-balet	komediealet	k5eAaPmF	komedie-balet
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
Jeana-Baptisty	Jeana-Baptista	k1gMnSc2	Jeana-Baptista
Lullyho	Lully	k1gMnSc2	Lully
o	o	k7c6	o
typickém	typický	k2eAgMnSc6d1	typický
měšťákovi	měšťák	k1gMnSc6	měšťák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
peníze	peníz	k1gInPc4	peníz
může	moct	k5eAaImIp3nS	moct
<g />
.	.	kIx.	.
</s>
<s>
koupit	koupit	k5eAaPmF	koupit
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Jíra	Jíra	k1gMnSc1	Jíra
Danda	Danda	k1gMnSc1	Danda
aneb	aneb	k?	aneb
Chudák	chudák	k1gMnSc1	chudák
manžel	manžel	k1gMnSc1	manžel
nebo	nebo	k8xC	nebo
jako	jako	k8xC	jako
Žorž	Žorž	k1gFnSc1	Žorž
Dandin	Dandina	k1gFnPc2	Dandina
aneb	aneb	k?	aneb
Nerovný	rovný	k2eNgMnSc1d1	nerovný
manžel	manžel	k1gMnSc1	manžel
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
(	(	kIx(	(
<g/>
1668	[number]	k4	1668
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Avare	Avar	k1gMnSc5	Avar
ou	ou	k0	ou
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
École	Écol	k1gMnSc2	Écol
du	du	k?	du
mensonge	mensong	k1gMnSc2	mensong
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snad	snad	k9	snad
nejznámější	známý	k2eAgFnSc1d3	nejznámější
Moliè	Moliè	k1gFnSc1	Moliè
komedie	komedie	k1gFnSc2	komedie
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
titulním	titulní	k2eAgMnSc7d1	titulní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
vdovec	vdovec	k1gMnSc1	vdovec
<g/>
,	,	kIx,	,
lichvář	lichvář	k1gMnSc1	lichvář
a	a	k8xC	a
necitelný	citelný	k2eNgMnSc1d1	necitelný
lakomec	lakomec	k1gMnSc1	lakomec
Harpagon	Harpagon	k1gMnSc1	Harpagon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
peníze	peníz	k1gInPc4	peníz
schopen	schopen	k2eAgMnSc1d1	schopen
obětovat	obětovat	k5eAaBmF	obětovat
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
i	i	k8xC	i
rodinu	rodina	k1gFnSc4	rodina
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
jeho	on	k3xPp3gNnSc2	on
bohatství	bohatství	k1gNnSc2	bohatství
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
představuje	představovat	k5eAaImIp3nS	představovat
ztrátu	ztráta	k1gFnSc4	ztráta
smyslu	smysl	k1gInSc2	smysl
bytí	bytí	k1gNnSc2	bytí
a	a	k8xC	a
ztrátu	ztráta	k1gFnSc4	ztráta
zdravého	zdravý	k2eAgInSc2d1	zdravý
rozumu	rozum	k1gInSc2	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
postava	postava	k1gFnSc1	postava
je	být	k5eAaImIp3nS	být
tragikomická	tragikomický	k2eAgFnSc1d1	tragikomická
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
Moliè	Moliè	k1gMnSc3	Moliè
důkaz	důkaz	k1gInSc1	důkaz
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
peníze	peníz	k1gInPc1	peníz
přirozeně	přirozeně	k6eAd1	přirozeně
deformují	deformovat	k5eAaImIp3nP	deformovat
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
mezilidské	mezilidský	k2eAgInPc4d1	mezilidský
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Moliè	Moliè	k?	Moliè
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
silnou	silný	k2eAgFnSc4d1	silná
inspiraci	inspirace	k1gFnSc4	inspirace
z	z	k7c2	z
Plautovy	Plautův	k2eAgFnSc2d1	Plautova
antické	antický	k2eAgFnSc2d1	antická
Komedie	komedie	k1gFnSc2	komedie
o	o	k7c6	o
hrnci	hrnec	k1gInSc6	hrnec
(	(	kIx(	(
<g/>
Aulularia	Aulularium	k1gNnSc2	Aulularium
<g/>
)	)	kIx)	)
Pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Prasečkova	Prasečkův	k2eAgInSc2d1	Prasečkův
(	(	kIx(	(
<g/>
1669	[number]	k4	1669
<g/>
,	,	kIx,	,
Monsieur	Monsieur	k1gMnSc1	Monsieur
de	de	k?	de
Pourceaugnac	Pourceaugnac	k1gInSc1	Pourceaugnac
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie-balet	komediealet	k1gInSc1	komedie-balet
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
jednáních	jednání	k1gNnPc6	jednání
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
Jeana-Baptisty	Jeana-Baptista	k1gMnSc2	Jeana-Baptista
Lullyho	Lully	k1gMnSc2	Lully
napsaná	napsaný	k2eAgFnSc1d1	napsaná
pro	pro	k7c4	pro
královské	královský	k2eAgFnPc4d1	královská
slavnosti	slavnost	k1gFnPc4	slavnost
v	v	k7c4	v
Chambord	Chambord	k1gInSc4	Chambord
<g/>
,	,	kIx,	,
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
které	který	k3yIgNnSc4	který
autor	autor	k1gMnSc1	autor
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
živý	živý	k2eAgInSc4d1	živý
typ	typ	k1gInSc4	typ
přihlouplého	přihlouplý	k2eAgMnSc2d1	přihlouplý
pošlechtěného	pošlechtěný	k2eAgMnSc2d1	pošlechtěný
měšťana	měšťan	k1gMnSc2	měšťan
<g/>
,	,	kIx,	,
Skvělí	skvělý	k2eAgMnPc1d1	skvělý
milenci	milenec	k1gMnPc1	milenec
(	(	kIx(	(
<g/>
1670	[number]	k4	1670
<g/>
,	,	kIx,	,
Les	les	k1gInSc4	les
Amants	Amantsa	k1gFnPc2	Amantsa
magnifiques	magnifiquesa	k1gFnPc2	magnifiquesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pětiaktová	pětiaktový	k2eAgFnSc1d1	pětiaktová
pastorální	pastorální	k2eAgFnSc1d1	pastorální
komedie	komedie	k1gFnSc1	komedie
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
<g/>
,	,	kIx,	,
Měšťák	měšťák	k1gMnSc1	měšťák
šlechticem	šlechtic	k1gMnSc7	šlechtic
(	(	kIx(	(
<g/>
1670	[number]	k4	1670
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc6	Le
Bourgeois	Bourgeois	k1gFnSc1	Bourgeois
gentilhomme	gentilhomit	k5eAaPmRp1nP	gentilhomit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Jeho	jeho	k3xOp3gFnSc1	jeho
Urozenost	urozenost	k1gFnSc1	urozenost
<g />
.	.	kIx.	.
</s>
<s>
pan	pan	k1gMnSc1	pan
měšťák	měšťák	k1gMnSc1	měšťák
<g/>
,	,	kIx,	,
komedie-balet	komediealet	k1gInSc1	komedie-balet
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
o	o	k7c6	o
pěti	pět	k4xCc6	pět
jednáních	jednání	k1gNnPc6	jednání
napsaná	napsaný	k2eAgFnSc1d1	napsaná
rovněž	rovněž	k9	rovněž
pro	pro	k7c4	pro
královské	královský	k2eAgFnPc4d1	královská
slavnosti	slavnost	k1gFnPc4	slavnost
v	v	k7c4	v
Chambord	Chambord	k1gInSc4	Chambord
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
Jeana-Baptisty	Jeana-Baptista	k1gMnSc2	Jeana-Baptista
Lullyho	Lully	k1gMnSc2	Lully
<g/>
,	,	kIx,	,
satira	satira	k1gFnSc1	satira
na	na	k7c4	na
chudnoucí	chudnoucí	k2eAgFnSc4d1	chudnoucí
šlechtu	šlechta	k1gFnSc4	šlechta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nucena	nucen	k2eAgFnSc1d1	nucena
obracet	obracet	k5eAaImF	obracet
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
pojem	pojem	k1gInSc4	pojem
šlechtické	šlechtický	k2eAgFnSc2d1	šlechtická
cti	čest	k1gFnSc2	čest
na	na	k7c4	na
bohatnoucí	bohatnoucí	k2eAgMnPc4d1	bohatnoucí
měšťáky	měšťák	k1gMnPc4	měšťák
s	s	k7c7	s
žádostmi	žádost	k1gFnPc7	žádost
o	o	k7c4	o
finanční	finanční	k2eAgFnSc4d1	finanční
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Psyché	psyché	k1gFnSc1	psyché
(	(	kIx(	(
<g/>
1671	[number]	k4	1671
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mytologická	mytologický	k2eAgFnSc1d1	mytologická
baletní	baletní	k2eAgFnSc1d1	baletní
tragikomedie	tragikomedie	k1gFnSc1	tragikomedie
napsaná	napsaný	k2eAgFnSc1d1	napsaná
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Pierrem	Pierr	k1gMnSc7	Pierr
Corneillem	Corneill	k1gMnSc7	Corneill
a	a	k8xC	a
Philippem	Philipp	k1gMnSc7	Philipp
Quinaultem	Quinault	k1gInSc7	Quinault
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lully	Lull	k1gInPc1	Lull
<g/>
,	,	kIx,	,
Skapinova	Skapinův	k2eAgNnPc1d1	Skapinův
šibalství	šibalství	k1gNnPc1	šibalství
(	(	kIx(	(
<g/>
1671	[number]	k4	1671
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Fourberies	Fourberies	k1gInSc1	Fourberies
de	de	k?	de
Scapin	Scapin	k1gInSc1	Scapin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tříaktová	tříaktový	k2eAgFnSc1d1	tříaktová
komedie	komedie	k1gFnSc1	komedie
v	v	k7c6	v
proze	proza	k1gFnSc6	proza
s	s	k7c7	s
ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavu	postava	k1gFnSc4	postava
všemi	všecek	k3xTgFnPc7	všecek
<g />
.	.	kIx.	.
</s>
<s>
mastmi	mast	k1gFnPc7	mast
mazaného	mazaný	k2eAgMnSc2d1	mazaný
taškáře	taškář	k1gMnSc2	taškář
<g/>
,	,	kIx,	,
Hraběnka	hraběnka	k1gFnSc1	hraběnka
z	z	k7c2	z
Nouzova	Nouzov	k1gInSc2	Nouzov
(	(	kIx(	(
<g/>
1671	[number]	k4	1671
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Comtesse	Comtesse	k1gFnSc2	Comtesse
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Escarbagnas	Escarbagnas	k1gMnSc1	Escarbagnas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
<g/>
,	,	kIx,	,
morální	morální	k2eAgFnSc1d1	morální
a	a	k8xC	a
psychologická	psychologický	k2eAgFnSc1d1	psychologická
karikatura	karikatura	k1gFnSc1	karikatura
venkovské	venkovský	k2eAgFnSc2d1	venkovská
preciosky	precioska	k1gFnSc2	precioska
<g/>
,	,	kIx,	,
snažící	snažící	k2eAgFnSc2d1	snažící
se	se	k3xPyFc4	se
napodobovat	napodobovat	k5eAaImF	napodobovat
styl	styl	k1gInSc4	styl
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
aristokratické	aristokratický	k2eAgFnSc2d1	aristokratická
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
Učené	učený	k2eAgFnPc1d1	učená
ženy	žena	k1gFnPc1	žena
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1672	[number]	k4	1672
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Femmes	Femmes	k1gMnSc1	Femmes
Savantes	Savantes	k1gMnSc1	Savantes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
ve	v	k7c6	v
verších	verš	k1gInPc6	verš
o	o	k7c6	o
pěti	pět	k4xCc6	pět
jednáních	jednání	k1gNnPc6	jednání
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
autor	autor	k1gMnSc1	autor
zesměšňuje	zesměšňovat	k5eAaImIp3nS	zesměšňovat
preciósky	preciósky	k6eAd1	preciósky
zabývající	zabývající	k2eAgMnSc1d1	zabývající
se	se	k3xPyFc4	se
amatérsky	amatérsky	k6eAd1	amatérsky
a	a	k8xC	a
povrchně	povrchně	k6eAd1	povrchně
podle	podle	k7c2	podle
dobové	dobový	k2eAgFnSc2d1	dobová
módy	móda	k1gFnSc2	móda
různými	různý	k2eAgFnPc7d1	různá
vědami	věda	k1gFnPc7	věda
<g/>
,	,	kIx,	,
Zdravý	zdravý	k2eAgMnSc1d1	zdravý
nemocný	nemocný	k1gMnSc1	nemocný
(	(	kIx(	(
<g/>
1673	[number]	k4	1673
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc1	Le
Malade	Malad	k1gInSc5	Malad
imaginaire	imaginair	k1gMnSc5	imaginair
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie-balet	komediealet	k1gInSc4	komedie-balet
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
jednáních	jednání	k1gNnPc6	jednání
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
Marc-Antoine	Marc-Antoin	k1gInSc5	Marc-Antoin
Charpentier	Charpentiero	k1gNnPc2	Charpentiero
<g/>
)	)	kIx)	)
s	s	k7c7	s
titulní	titulní	k2eAgFnSc7d1	titulní
postavou	postava	k1gFnSc7	postava
měšťáckého	měšťácký	k2eAgMnSc2d1	měšťácký
sobce	sobec	k1gMnSc2	sobec
a	a	k8xC	a
hloupého	hloupý	k2eAgMnSc2d1	hloupý
hypochondra	hypochondr	k1gMnSc2	hypochondr
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Argana	Argan	k1gMnSc2	Argan
dohrál	dohrát	k5eAaPmAgMnS	dohrát
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
představení	představení	k1gNnSc4	představení
této	tento	k3xDgFnSc2	tento
komedie	komedie	k1gFnSc2	komedie
<g/>
,	,	kIx,	,
Moliè	Moliè	k1gMnSc1	Moliè
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgNnPc2d1	české
vydání	vydání	k1gNnSc2	vydání
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
rozsáhlost	rozsáhlost	k1gFnSc4	rozsáhlost
divadelní	divadelní	k2eAgInPc4d1	divadelní
programy	program	k1gInPc4	program
s	s	k7c7	s
texty	text	k1gInPc7	text
her	hra	k1gFnPc2	hra
a	a	k8xC	a
také	také	k9	také
různá	různý	k2eAgNnPc4d1	různé
přepracování	přepracování	k1gNnPc4	přepracování
původních	původní	k2eAgInPc2d1	původní
autorových	autorův	k2eAgInPc2d1	autorův
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Bezděčný	bezděčný	k2eAgMnSc1d1	bezděčný
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Host	host	k1gMnSc1	host
<g/>
.	.	kIx.	.
</s>
<s>
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
1825	[number]	k4	1825
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Matěj	Matěj	k1gMnSc1	Matěj
Jozef	Jozef	k1gMnSc1	Jozef
Sychra	Sychra	k1gMnSc1	Sychra
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1868	[number]	k4	1868
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Emanuel	Emanuel	k1gMnSc1	Emanuel
František	František	k1gMnSc1	František
Züngel	Züngel	k1gMnSc1	Züngel
<g/>
,	,	kIx,	,
Skoroměl	Skoroměl	k1gMnSc1	Skoroměl
čili	čili	k8xC	čili
Domnělý	domnělý	k2eAgMnSc1d1	domnělý
rohoun	rohoun	k1gMnSc1	rohoun
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
Misanthrop	Misanthrop	k1gInSc1	Misanthrop
<g/>
,	,	kIx,	,
Ignác	Ignác	k1gMnSc1	Ignác
Leopold	Leopolda	k1gFnPc2	Leopolda
Kober	kobra	k1gFnPc2	kobra
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Preininger	Preininger	k1gMnSc1	Preininger
<g/>
,	,	kIx,	,
Šibalství	šibalství	k1gNnPc4	šibalství
Skapinova	Skapinův	k2eAgNnPc4d1	Skapinův
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Julius	Julius	k1gMnSc1	Julius
Zeyer	Zeyer	k1gMnSc1	Zeyer
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1900	[number]	k4	1900
a	a	k8xC	a
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
<g/>
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
a	a	k8xC	a
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
Misantrop	misantrop	k1gMnSc1	misantrop
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Dandin	Dandin	k2eAgMnSc1d1	Dandin
čili	čili	k8xC	čili
Ošálený	ošálený	k2eAgMnSc1d1	ošálený
manžel	manžel	k1gMnSc1	manžel
<g/>
,	,	kIx,	,
Kamilla	Kamilla	k1gMnSc1	Kamilla
Neumannová	Neumannová	k1gFnSc1	Neumannová
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Arnošt	Arnošt	k1gMnSc1	Arnošt
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
aneb	aneb	k?	aneb
Kamenný	kamenný	k2eAgInSc4d1	kamenný
kvas	kvas	k1gInSc4	kvas
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Kočí	Kočí	k1gFnSc1	Kočí
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Hanuš	Hanuš	k1gMnSc1	Hanuš
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Amfitryon	Amfitryon	k1gMnSc1	Amfitryon
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Petr	Petr	k1gMnSc1	Petr
Křička	Křička	k1gMnSc1	Křička
<g/>
,	,	kIx,	,
Létavý	létavý	k2eAgMnSc1d1	létavý
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
Springer	Springer	k1gMnSc1	Springer
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
M.	M.	kA	M.
Klos	Klos	k1gInSc1	Klos
<g/>
,	,	kIx,	,
Směšné	směšný	k2eAgInPc1d1	směšný
precieusy	precieus	k1gInPc1	precieus
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Hanuš	Hanuš	k1gMnSc1	Hanuš
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Versaillesské	Versaillesské	k2eAgNnPc2d1	Versaillesské
impromptu	impromptu	k1gNnPc2	impromptu
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Gintl	Gintl	k1gMnSc1	Gintl
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
Zora	Zora	k1gFnSc1	Zora
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Gintl	Gintl	k1gMnSc1	Gintl
<g/>
,	,	kIx,	,
Sganarelle	Sganarelle	k1gFnSc1	Sganarelle
čili	čili	k8xC	čili
Domnělý	domnělý	k2eAgMnSc1d1	domnělý
paroháč	paroháč	k1gMnSc1	paroháč
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1922	[number]	k4	1922
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Otokar	Otokar	k1gMnSc1	Otokar
Fischer	Fischer	k1gMnSc1	Fischer
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
Lékařem	lékař	k1gMnSc7	lékař
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Antonín	Antonín	k1gMnSc1	Antonín
Bernášek	Bernášek	k1gMnSc1	Bernášek
<g/>
,	,	kIx,	,
Zdravý	zdravý	k2eAgMnSc1d1	zdravý
nemocný	nemocný	k1gMnSc1	nemocný
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bedřich	Bedřich	k1gMnSc1	Bedřich
Frida	Frida	k1gMnSc1	Frida
a	a	k8xC	a
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Gintl	Gintl	k1gMnSc1	Gintl
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Janek	Janek	k1gMnSc1	Janek
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
Lidé	člověk	k1gMnPc1	člověk
obtížní	obtížný	k2eAgMnPc1d1	obtížný
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
Tramopty	Tramopt	k1gMnPc4	Tramopt
zamilovaných	zamilovaný	k1gMnPc2	zamilovaný
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
Učené	učený	k2eAgFnPc1d1	učená
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
Melicerta	Melicerta	k1gFnSc1	Melicerta
<g/>
:	:	kIx,	:
Heroická	heroický	k2eAgFnSc1d1	heroická
pastorála	pastorála	k1gFnSc1	pastorála
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
manželů	manžel	k1gMnPc2	manžel
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
Amphitryon	Amphitryon	k1gMnSc1	Amphitryon
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
Sganarel	Sganarel	k1gMnSc1	Sganarel
čili	čili	k8xC	čili
Domnělý	domnělý	k2eAgMnSc1d1	domnělý
paroháč	paroháč	k1gMnSc1	paroháč
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Skapinova	Skapinův	k2eAgNnPc1d1	Skapinův
šibalství	šibalství	k1gNnPc1	šibalství
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
Garcia	Garcius	k1gMnSc2	Garcius
Navarrský	navarrský	k2eAgMnSc1d1	navarrský
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
<g/>
,	,	kIx,	,
Žárlivý	žárlivý	k2eAgMnSc1d1	žárlivý
princ	princ	k1gMnSc1	princ
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
Školy	škola	k1gFnSc2	škola
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
Versailleská	versailleský	k2eAgFnSc1d1	Versailleská
improvisace	improvisace	k1gFnSc1	improvisace
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
Žorž	Žorž	k1gFnSc1	Žorž
Dandin	Dandina	k1gFnPc2	Dandina
aneb	aneb	k?	aneb
Nerovný	rovný	k2eNgMnSc1d1	nerovný
manžel	manžel	k1gMnSc1	manžel
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
Misantrop	misantrop	k1gMnSc1	misantrop
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Hanuš	Hanuš	k1gMnSc1	Hanuš
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
aneb	aneb	k?	aneb
Kamenný	kamenný	k2eAgInSc1d1	kamenný
kvas	kvas	k1gInSc1	kvas
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Hanuš	Hanuš	k1gMnSc1	Hanuš
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
Příležitost	příležitost	k1gFnSc1	příležitost
dělá	dělat	k5eAaImIp3nS	dělat
lékaře	lékař	k1gMnPc4	lékař
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Lácha	Lácha	k1gMnSc1	Lácha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jindřich	Jindřich	k1gMnSc1	Jindřich
Hořejší	Hořejší	k1gMnSc1	Hořejší
<g/>
,	,	kIx,	,
Směšné	směšný	k2eAgInPc4d1	směšný
précieusy	précieus	k1gInPc4	précieus
<g/>
,	,	kIx,	,
Manžel	manžel	k1gMnSc1	manžel
z	z	k7c2	z
donucení	donucení	k1gNnSc2	donucení
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Doktor	doktor	k1gMnSc1	doktor
Láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Hanuš	Hanuš	k1gMnSc1	Hanuš
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
Lékařem	lékař	k1gMnSc7	lékař
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Hanuš	Hanuš	k1gMnSc1	Hanuš
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
Amfitryon	Amfitryon	k1gMnSc1	Amfitryon
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Petr	Petr	k1gMnSc1	Petr
Křička	Křička	k1gMnSc1	Křička
<g/>
,	,	kIx,	,
Božská	božský	k2eAgFnSc1d1	božská
láska	láska	k1gFnSc1	láska
[	[	kIx(	[
<g/>
Amphitryon	Amphitryon	k1gInSc1	Amphitryon
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Českomoravský	českomoravský	k2eAgInSc1d1	českomoravský
kompas	kompas	k1gInSc1	kompas
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
hrou	hra	k1gFnSc7	hra
Rozbitý	rozbitý	k2eAgInSc1d1	rozbitý
džbán	džbán	k1gInSc1	džbán
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svazku	svazek	k1gInSc6	svazek
"	"	kIx"	"
<g/>
KLEISTOVY	KLEISTOVY	kA	KLEISTOVY
VESELOHRY	veselohra	k1gFnSc2	veselohra
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přebásnil	přebásnit	k5eAaPmAgMnS	přebásnit
Miloš	Miloš	k1gMnSc1	Miloš
Hlávka	Hlávka	k1gMnSc1	Hlávka
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
Českomoravský	českomoravský	k2eAgInSc1d1	českomoravský
kompas	kompas	k1gInSc1	kompas
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Misantrop	misantrop	k1gMnSc1	misantrop
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgInSc1d1	československý
kompas	kompas	k1gInSc1	kompas
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
kompas	kompas	k1gInSc1	kompas
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Jíra	Jíra	k1gMnSc1	Jíra
Danda	Danda	k1gMnSc1	Danda
aneb	aneb	k?	aneb
Chudák	chudák	k1gMnSc1	chudák
manžel	manžel	k1gMnSc1	manžel
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
kompas	kompas	k1gInSc1	kompas
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Erik	Erik	k1gMnSc1	Erik
Adolf	Adolf	k1gMnSc1	Adolf
Saudek	Saudek	k1gMnSc1	Saudek
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
Osvěta	osvěta	k1gFnSc1	osvěta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Zdravý	zdravý	k2eAgMnSc1d1	zdravý
nemocný	nemocný	k1gMnSc1	nemocný
<g/>
,	,	kIx,	,
Osvěta	osvěta	k1gFnSc1	osvěta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Učené	učený	k2eAgFnPc1d1	učená
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
ČLDJ	ČLDJ	kA	ČLDJ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Misantrop	misantrop	k1gMnSc1	misantrop
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgInSc1d1	československý
kompas	kompas	k1gInSc1	kompas
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Hry	hra	k1gFnPc1	hra
I.	I.	kA	I.
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hry	hra	k1gFnPc4	hra
Žárlivý	žárlivý	k2eAgMnSc1d1	žárlivý
Petřík	Petřík	k1gMnSc1	Petřík
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Směšné	směšný	k2eAgFnPc1d1	směšná
preciosky	precioska	k1gFnPc1	precioska
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
Kritika	kritika	k1gFnSc1	kritika
Školy	škola	k1gFnSc2	škola
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
Versailleská	versailleský	k2eAgFnSc1d1	Versailleská
improvisace	improvisace	k1gFnSc1	improvisace
<g/>
,	,	kIx,	,
Hry	hra	k1gFnPc1	hra
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hry	hra	k1gFnSc2	hra
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Misantrop	misantrop	k1gMnSc1	misantrop
a	a	k8xC	a
Amfitryon	Amfitryon	k1gMnSc1	Amfitryon
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
pro	pro	k7c4	pro
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
ČDLJ	ČDLJ	kA	ČDLJ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
ČDLJ	ČDLJ	kA	ČDLJ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
Pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Prasečkova	Prasečkův	k2eAgInSc2d1	Prasečkův
<g/>
,	,	kIx,	,
ČDLJ	ČDLJ	kA	ČDLJ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatoplu	Svatopla	k1gFnSc4	Svatopla
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Misantrop	misantrop	k1gMnSc1	misantrop
<g/>
,	,	kIx,	,
ČDLJ	ČDLJ	kA	ČDLJ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
J.	J.	kA	J.
Z.	Z.	kA	Z.
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Hry	hra	k1gFnPc1	hra
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1955	[number]	k4	1955
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hry	hra	k1gFnPc4	hra
Jiří	Jiří	k1gMnSc1	Jiří
Dudek	Dudek	k1gMnSc1	Dudek
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Prasečkova	Prasečkův	k2eAgInSc2d1	Prasečkův
a	a	k8xC	a
Měšťák	měšťák	k1gMnSc1	měšťák
šlechticem	šlechtic	k1gMnSc7	šlechtic
<g/>
,	,	kIx,	,
Hry	hra	k1gFnPc1	hra
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hry	hra	k1gFnPc4	hra
Skapinova	Skapinův	k2eAgNnSc2d1	Skapinův
šibalství	šibalství	k1gNnSc2	šibalství
<g/>
,	,	kIx,	,
Hraběnka	hraběnka	k1gFnSc1	hraběnka
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
Nouzova	Nouzov	k1gInSc2	Nouzov
<g/>
,	,	kIx,	,
Učené	učený	k2eAgFnPc4d1	učená
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
Zdravý	zdravý	k2eAgMnSc1d1	zdravý
nemocný	nemocný	k1gMnSc1	nemocný
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
ČDLJ	ČDLJ	kA	ČDLJ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Skapinova	Skapinův	k2eAgNnPc1d1	Skapinův
šibalství	šibalství	k1gNnPc1	šibalství
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Erik	Erik	k1gMnSc1	Erik
Adolf	Adolf	k1gMnSc1	Adolf
Saudek	Saudek	k1gMnSc1	Saudek
<g/>
,	,	kIx,	,
Zdravý	zdravý	k2eAgMnSc1d1	zdravý
nemocný	nemocný	k1gMnSc1	nemocný
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Jíra	Jíra	k1gMnSc1	Jíra
Danda	Danda	k1gMnSc1	Danda
aneb	aneb	k?	aneb
Chudák	chudák	k1gMnSc1	chudák
manžel	manžel	k1gMnSc1	manžel
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Erik	Erik	k1gMnSc1	Erik
Adolf	Adolf	k1gMnSc1	Adolf
Saudek	Saudek	k1gMnSc1	Saudek
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
Amfitryon	Amfitryon	k1gMnSc1	Amfitryon
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatoplu	Svatopla	k1gFnSc4	Svatopla
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Misantrop	misantrop	k1gMnSc1	misantrop
<g/>
,	,	kIx,	,
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
J.	J.	kA	J.
Z.	Z.	kA	Z.
Novák	Novák	k1gMnSc1	Novák
a	a	k8xC	a
František	František	k1gMnSc1	František
<g />
.	.	kIx.	.
</s>
<s>
Vrba	vrba	k1gFnSc1	vrba
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
J.	J.	kA	J.
Z.	Z.	kA	Z.
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
a	a	k8xC	a
Erik	Erik	k1gMnSc1	Erik
Adolf	Adolf	k1gMnSc1	Adolf
Saudek	Saudek	k1gMnSc1	Saudek
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Eva	Eva	k1gFnSc1	Eva
Bezděková	Bezděková	k1gFnSc1	Bezděková
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Konečný	Konečný	k1gMnSc1	Konečný
<g/>
,	,	kIx,	,
Ztřeštěnec	ztřeštěnec	k1gMnSc1	ztřeštěnec
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Vrba	Vrba	k1gMnSc1	Vrba
<g/>
,	,	kIx,	,
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Antonín	Antonín	k1gMnSc1	Antonín
<g />
.	.	kIx.	.
</s>
<s>
Přidal	přidat	k5eAaPmAgMnS	přidat
<g/>
,	,	kIx,	,
Jeho	jeho	k3xOp3gFnSc1	jeho
Urozenost	urozenost	k1gFnSc1	urozenost
pan	pan	k1gMnSc1	pan
měšťák	měšťák	k1gMnSc1	měšťák
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
J.	J.	kA	J.
Z.	Z.	kA	Z.
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Vrba	Vrba	k1gMnSc1	Vrba
<g/>
,	,	kIx,	,
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
Artur	Artur	k1gMnSc1	Artur
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g />
.	.	kIx.	.
</s>
<s>
Mikeš	Mikeš	k1gMnSc1	Mikeš
<g/>
,	,	kIx,	,
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
Větrné	větrný	k2eAgInPc1d1	větrný
mlýny	mlýn	k1gInPc1	mlýn
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Antonín	Antonín	k1gMnSc1	Antonín
Přidal	přidat	k5eAaPmAgMnS	přidat
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
<g/>
,	,	kIx,	,
Artur	Artur	k1gMnSc1	Artur
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Konečný	Konečný	k1gMnSc1	Konečný
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
Misantrop	misantrop	k1gMnSc1	misantrop
<g/>
,	,	kIx,	,
Artur	Artur	k1gMnSc1	Artur
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mikeš	Mikeš	k1gMnSc1	Mikeš
<g/>
,	,	kIx,	,
Amfitryon	Amfitryon	k1gMnSc1	Amfitryon
<g/>
,	,	kIx,	,
Artur	Artur	k1gMnSc1	Artur
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mikeš	Mikeš	k1gMnSc1	Mikeš
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Artur	Artur	k1gMnSc1	Artur
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mikeš	Mikeš	k1gMnSc1	Mikeš
<g/>
.	.	kIx.	.
</s>
