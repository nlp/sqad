<s>
Moliè	Moliè	k?	Moliè
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Poquelin	Poquelin	k2eAgInSc1d1	Poquelin
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1622	[number]	k4	1622
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1673	[number]	k4	1673
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
období	období	k1gNnSc2	období
francouzského	francouzský	k2eAgInSc2d1	francouzský
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
věku	věk	k1gInSc3	věk
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
