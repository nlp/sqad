<p>
<s>
Bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
pás	pás	k1gInSc1	pás
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc4	zařízení
používané	používaný	k2eAgNnSc4d1	používané
v	v	k7c6	v
dopravních	dopravní	k2eAgInPc6d1	dopravní
prostředcích	prostředek	k1gInPc6	prostředek
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
pasažérů	pasažér	k1gMnPc2	pasažér
a	a	k8xC	a
pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
následků	následek	k1gInPc2	následek
případné	případný	k2eAgFnSc2d1	případná
nehody	nehoda	k1gFnSc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
něho	on	k3xPp3gNnSc2	on
je	být	k5eAaImIp3nS	být
pasažér	pasažér	k1gMnSc1	pasažér
připoután	připoutat	k5eAaPmNgMnS	připoutat
k	k	k7c3	k
sedadlu	sedadlo	k1gNnSc3	sedadlo
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
dětskou	dětský	k2eAgFnSc7d1	dětská
autosedačkou	autosedačka	k1gFnSc7	autosedačka
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
dětským	dětský	k2eAgInSc7d1	dětský
zádržným	zádržný	k2eAgInSc7d1	zádržný
systémem	systém	k1gInSc7	systém
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
zádržné	zádržný	k2eAgInPc4d1	zádržný
bezpečnostní	bezpečnostní	k2eAgInPc4d1	bezpečnostní
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
361	[number]	k4	361
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
O	o	k7c6	o
provozu	provoz	k1gInSc6	provoz
na	na	k7c6	na
pozemních	pozemní	k2eAgFnPc6d1	pozemní
komunikacích	komunikace	k1gFnPc6	komunikace
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
používání	používání	k1gNnSc1	používání
až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
výjimek	výjimka	k1gFnPc2	výjimka
povinné	povinný	k2eAgFnPc4d1	povinná
<g/>
,	,	kIx,	,
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
této	tento	k3xDgFnSc2	tento
povinnosti	povinnost	k1gFnSc2	povinnost
hrozí	hrozit	k5eAaImIp3nS	hrozit
v	v	k7c6	v
bodovém	bodový	k2eAgInSc6d1	bodový
systému	systém	k1gInSc6	systém
ztráta	ztráta	k1gFnSc1	ztráta
2	[number]	k4	2
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
pásy	pás	k1gInPc1	pás
vybaveny	vybavit	k5eAaPmNgInP	vybavit
i	i	k9	i
dálkové	dálkový	k2eAgInPc1d1	dálkový
autobusy	autobus	k1gInPc1	autobus
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
a	a	k8xC	a
cestující	cestující	k1gMnPc1	cestující
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
autobusech	autobus	k1gInPc6	autobus
jsou	být	k5eAaImIp3nP	být
povinni	povinen	k2eAgMnPc1d1	povinen
se	se	k3xPyFc4	se
poutat	poutat	k5eAaImF	poutat
<g/>
.	.	kIx.	.
<g/>
Pásy	pás	k1gInPc1	pás
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
typy	typ	k1gInPc4	typ
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
je	být	k5eAaImIp3nS	být
pasažér	pasažér	k1gMnSc1	pasažér
připoután	připoután	k2eAgMnSc1d1	připoután
(	(	kIx(	(
<g/>
spojen	spojen	k2eAgInSc1d1	spojen
s	s	k7c7	s
autem	auto	k1gNnSc7	auto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
bodové	bodový	k2eAgInPc1d1	bodový
až	až	k9	až
7	[number]	k4	7
<g/>
bodové	bodový	k2eAgInPc1d1	bodový
<g/>
,	,	kIx,	,
druhů	druh	k1gInPc2	druh
pásu	pás	k1gInSc2	pás
však	však	k8xC	však
existuje	existovat	k5eAaImIp3nS	existovat
podstatně	podstatně	k6eAd1	podstatně
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
je	on	k3xPp3gInPc4	on
dále	daleko	k6eAd2	daleko
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
samonavíjecí	samonavíjecí	k2eAgInSc4d1	samonavíjecí
(	(	kIx(	(
<g/>
jiné	jiný	k2eAgFnPc1d1	jiná
už	už	k6eAd1	už
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
nabídce	nabídka	k1gFnSc6	nabídka
asi	asi	k9	asi
ani	ani	k8xC	ani
nenajdeme	najít	k5eNaPmIp1nP	najít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
automatickým	automatický	k2eAgInSc7d1	automatický
napínačem	napínač	k1gInSc7	napínač
dále	daleko	k6eAd2	daleko
snižujícím	snižující	k2eAgInSc7d1	snižující
riziko	riziko	k1gNnSc4	riziko
poranění	poranění	k1gNnPc2	poranění
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nehody	nehoda	k1gFnSc2	nehoda
atd.	atd.	kA	atd.
V	v	k7c6	v
autech	aut	k1gInPc6	aut
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používají	používat	k5eAaImIp3nP	používat
3	[number]	k4	3
<g/>
bodové	bodový	k2eAgInPc1d1	bodový
a	a	k8xC	a
2	[number]	k4	2
<g/>
bodové	bodový	k2eAgFnSc6d1	bodová
(	(	kIx(	(
<g/>
břišní	břišní	k2eAgFnSc6d1	břišní
<g/>
)	)	kIx)	)
pásy	pás	k1gInPc1	pás
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
první	první	k4xOgFnSc7	první
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
pásu	pás	k1gInSc2	pás
už	už	k6eAd1	už
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
všestranný	všestranný	k2eAgMnSc1d1	všestranný
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
George	Georg	k1gMnSc2	Georg
Cayley	Caylea	k1gMnSc2	Caylea
(	(	kIx(	(
<g/>
1773	[number]	k4	1773
–	–	k?	–
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
byl	být	k5eAaImAgInS	být
bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
pás	pás	k1gInSc1	pás
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
letectví	letectví	k1gNnSc6	letectví
<g/>
,	,	kIx,	,
všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Louis	Louis	k1gMnSc1	Louis
Renault	renault	k1gInSc4	renault
5	[number]	k4	5
<g/>
bodový	bodový	k2eAgInSc4d1	bodový
bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
pás	pás	k1gInSc4	pás
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yRgNnSc2	který
automobilka	automobilka	k1gFnSc1	automobilka
Volvo	Volvo	k1gNnSc1	Volvo
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
pás	pás	k1gInSc4	pás
3	[number]	k4	3
<g/>
bodový	bodový	k2eAgInSc1d1	bodový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgInP	být
činěny	činěn	k2eAgInPc1d1	činěn
pokusy	pokus	k1gInPc1	pokus
pás	pás	k1gInSc4	pás
integrovat	integrovat	k5eAaBmF	integrovat
do	do	k7c2	do
konstrukce	konstrukce	k1gFnSc2	konstrukce
dveří	dveře	k1gFnPc2	dveře
a	a	k8xC	a
sedačky	sedačka	k1gFnSc2	sedačka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
usednutí	usednutí	k1gNnSc6	usednutí
a	a	k8xC	a
zavření	zavření	k1gNnSc6	zavření
dveří	dveře	k1gFnPc2	dveře
cestující	cestující	k1gFnSc4	cestující
automaticky	automaticky	k6eAd1	automaticky
připoután	připoután	k2eAgInSc1d1	připoután
aniž	aniž	k8xS	aniž
to	ten	k3xDgNnSc1	ten
záleželo	záležet	k5eAaImAgNnS	záležet
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
vůli	vůle	k1gFnSc4	vůle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
sériových	sériový	k2eAgInPc6d1	sériový
vozech	vůz	k1gInPc6	vůz
šlo	jít	k5eAaImAgNnS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
raritu	rarita	k1gFnSc4	rarita
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInPc1d1	dnešní
vozy	vůz	k1gInPc1	vůz
se	se	k3xPyFc4	se
spokojí	spokojit	k5eAaPmIp3nP	spokojit
s	s	k7c7	s
čidlem	čidlo	k1gNnSc7	čidlo
ve	v	k7c6	v
sponě	spona	k1gFnSc6	spona
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
po	po	k7c6	po
rozjezdu	rozjezd	k1gInSc6	rozjezd
vozu	vůz	k1gInSc2	vůz
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
při	při	k7c6	při
nezapnutí	nezapnutí	k1gNnSc6	nezapnutí
zvukový	zvukový	k2eAgInSc4d1	zvukový
a	a	k8xC	a
optický	optický	k2eAgInSc4d1	optický
signál	signál	k1gInSc4	signál
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
u	u	k7c2	u
řidiče	řidič	k1gMnSc2	řidič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgInS	být
bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
pás	pás	k1gInSc1	pás
ve	v	k7c6	v
výbavě	výbava	k1gFnSc6	výbava
modelu	model	k1gInSc2	model
Tucker	Tucker	k1gInSc4	Tucker
Sedan	sedan	k1gInSc1	sedan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
zdokonalili	zdokonalit	k5eAaPmAgMnP	zdokonalit
bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
pás	pás	k1gInSc4	pás
bratři	bratr	k1gMnPc1	bratr
Kenneth	Kenneth	k1gMnSc1	Kenneth
Ligon	Ligon	k1gMnSc1	Ligon
a	a	k8xC	a
Bob	Bob	k1gMnSc1	Bob
Ligon	Ligon	k1gMnSc1	Ligon
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
používání	používání	k1gNnSc2	používání
v	v	k7c6	v
autech	aut	k1gInPc6	aut
(	(	kIx(	(
<g/>
patent	patent	k1gInSc4	patent
použila	použít	k5eAaPmAgFnS	použít
automobilka	automobilka	k1gFnSc1	automobilka
Ford	ford	k1gInSc1	ford
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
zahrnula	zahrnout	k5eAaPmAgFnS	zahrnout
bezpečnostní	bezpečnostní	k2eAgInPc4d1	bezpečnostní
pásy	pás	k1gInPc4	pás
do	do	k7c2	do
standardní	standardní	k2eAgFnSc2d1	standardní
výbavy	výbava	k1gFnSc2	výbava
automobilka	automobilka	k1gFnSc1	automobilka
Volvo	Volvo	k1gNnSc4	Volvo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
určila	určit	k5eAaPmAgFnS	určit
bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
pás	pás	k1gInSc4	pás
za	za	k7c4	za
povinnou	povinný	k2eAgFnSc4d1	povinná
součást	součást	k1gFnSc4	součást
výbavy	výbava	k1gFnSc2	výbava
automobilů	automobil	k1gInPc2	automobil
byla	být	k5eAaImAgFnS	být
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
tehdejším	tehdejší	k2eAgMnSc6d1	tehdejší
Československu	Československo	k1gNnSc3	Československo
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
vyhláškou	vyhláška	k1gFnSc7	vyhláška
č.	č.	k?	č.
80	[number]	k4	80
<g/>
/	/	kIx~	/
<g/>
1966	[number]	k4	1966
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
§	§	k?	§
3	[number]	k4	3
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
)	)	kIx)	)
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
zavedena	zaveden	k2eAgFnSc1d1	zavedena
povinnost	povinnost	k1gFnSc1	povinnost
používat	používat	k5eAaImF	používat
bezpečnostní	bezpečnostní	k2eAgInPc4d1	bezpečnostní
pásy	pás	k1gInPc4	pás
na	na	k7c6	na
předních	přední	k2eAgNnPc6d1	přední
sedadlech	sedadlo	k1gNnPc6	sedadlo
aut	auto	k1gNnPc2	auto
vybavených	vybavený	k2eAgNnPc2d1	vybavené
povinně	povinně	k6eAd1	povinně
bezpečnostními	bezpečnostní	k2eAgInPc7d1	bezpečnostní
pásy	pás	k1gInPc7	pás
při	při	k7c6	při
jízdách	jízda	k1gFnPc6	jízda
mimo	mimo	k7c4	mimo
obec	obec	k1gFnSc4	obec
<g/>
.	.	kIx.	.
</s>
<s>
Kuriozitou	kuriozita	k1gFnSc7	kuriozita
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhláška	vyhláška	k1gFnSc1	vyhláška
č.	č.	k?	č.
139	[number]	k4	139
<g/>
/	/	kIx~	/
<g/>
1968	[number]	k4	1968
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
sice	sice	k8xC	sice
nařizovala	nařizovat	k5eAaImAgFnS	nařizovat
povinnost	povinnost	k1gFnSc1	povinnost
vybavovat	vybavovat	k5eAaImF	vybavovat
vozidla	vozidlo	k1gNnSc2	vozidlo
bezpečnostními	bezpečnostní	k2eAgInPc7d1	bezpečnostní
pásy	pás	k1gInPc7	pás
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
automobily	automobil	k1gInPc1	automobil
Moskvič	moskvič	k1gInSc1	moskvič
a	a	k8xC	a
Volha	Volha	k1gFnSc1	Volha
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
výnosem	výnos	k1gInSc7	výnos
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
č.	č.	k?	č.
<g/>
j.	j.	k?	j.
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
/	/	kIx~	/
<g/>
69	[number]	k4	69
SMD	SMD	kA	SMD
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1969	[number]	k4	1969
dostaly	dostat	k5eAaPmAgInP	dostat
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
výjimku	výjimka	k1gFnSc4	výjimka
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
neměly	mít	k5eNaImAgInP	mít
kotevní	kotevní	k2eAgInPc1d1	kotevní
úchyty	úchyt	k1gInPc1	úchyt
pro	pro	k7c4	pro
pásy	pás	k1gInPc4	pás
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
tedy	tedy	k9	tedy
nebylo	být	k5eNaImAgNnS	být
kam	kam	k6eAd1	kam
namontovat	namontovat	k5eAaPmF	namontovat
<g/>
.	.	kIx.	.
</s>
<s>
Vyhláškou	vyhláška	k1gFnSc7	vyhláška
č.	č.	k?	č.
100	[number]	k4	100
<g/>
/	/	kIx~	/
<g/>
1975	[number]	k4	1975
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1976	[number]	k4	1976
tato	tento	k3xDgFnSc1	tento
povinnost	povinnost	k1gFnSc1	povinnost
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
na	na	k7c4	na
všechna	všechen	k3xTgNnPc4	všechen
sedadla	sedadlo	k1gNnPc4	sedadlo
povinně	povinně	k6eAd1	povinně
vybavená	vybavený	k2eAgNnPc4d1	vybavené
bezpečnostními	bezpečnostní	k2eAgNnPc7d1	bezpečnostní
pásy	pás	k1gInPc1	pás
<g/>
,	,	kIx,	,
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
vyhláška	vyhláška	k1gFnSc1	vyhláška
č.	č.	k?	č.
99	[number]	k4	99
<g/>
/	/	kIx~	/
<g/>
1989	[number]	k4	1989
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
pásy	pás	k1gInPc1	pás
používány	používat	k5eAaImNgInP	používat
i	i	k9	i
v	v	k7c6	v
obcích	obec	k1gFnPc6	obec
<g/>
.	.	kIx.	.
</s>
<s>
Odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
za	za	k7c4	za
upoutání	upoutání	k1gNnSc4	upoutání
spolucestujících	spolucestující	k1gFnPc2	spolucestující
přešla	přejít	k5eAaPmAgFnS	přejít
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
osobních	osobní	k2eAgInPc2d1	osobní
vozů	vůz	k1gInPc2	vůz
z	z	k7c2	z
řidiče	řidič	k1gInSc2	řidič
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgMnPc4d1	jednotlivý
cestující	cestující	k1gMnPc4	cestující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
povinném	povinný	k2eAgNnSc6d1	povinné
zavádění	zavádění	k1gNnSc6	zavádění
pásů	pás	k1gInPc2	pás
se	se	k3xPyFc4	se
ve	v	k7c6	v
veřejnosti	veřejnost	k1gFnSc6	veřejnost
často	často	k6eAd1	často
vyskytovaly	vyskytovat	k5eAaImAgFnP	vyskytovat
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
pásy	pás	k1gInPc1	pás
jsou	být	k5eAaImIp3nP	být
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mohou	moct	k5eAaImIp3nP	moct
ztížit	ztížit	k5eAaPmF	ztížit
opuštění	opuštění	k1gNnSc3	opuštění
auta	auto	k1gNnSc2	auto
po	po	k7c6	po
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
obavy	obava	k1gFnPc1	obava
se	se	k3xPyFc4	se
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nepotvrdily	potvrdit	k5eNaPmAgFnP	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
vozit	vozit	k5eAaImF	vozit
s	s	k7c7	s
sebou	se	k3xPyFc7	se
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
potřeby	potřeba	k1gFnSc2	potřeba
vyproštění	vyproštění	k1gNnSc2	vyproštění
osob	osoba	k1gFnPc2	osoba
ostrý	ostrý	k2eAgInSc4d1	ostrý
nůž	nůž	k1gInSc4	nůž
či	či	k8xC	či
speciální	speciální	k2eAgInSc4d1	speciální
řezák	řezák	k1gInSc4	řezák
na	na	k7c4	na
pásy	pás	k1gInPc4	pás
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
význam	význam	k1gInSc4	význam
pásů	pás	k1gInPc2	pás
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
jsou	být	k5eAaImIp3nP	být
pásy	pás	k1gInPc1	pás
nejúčinnější	účinný	k2eAgInPc1d3	nejúčinnější
do	do	k7c2	do
rychlosti	rychlost	k1gFnSc2	rychlost
50	[number]	k4	50
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
airbagem	airbag	k1gInSc7	airbag
<g/>
.	.	kIx.	.
</s>
<s>
Airbagy	airbag	k1gInPc1	airbag
použité	použitý	k2eAgInPc1d1	použitý
bez	bez	k7c2	bez
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
pásů	pás	k1gInPc2	pás
fungují	fungovat	k5eAaImIp3nP	fungovat
podstatně	podstatně	k6eAd1	podstatně
hůř	zle	k6eAd2	zle
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
následky	následek	k1gInPc1	následek
nehody	nehoda	k1gFnSc2	nehoda
dokonce	dokonce	k9	dokonce
zvýšit	zvýšit	k5eAaPmF	zvýšit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Full	Full	k1gInSc1	Full
Size	Size	k1gInSc1	Size
Airbag	airbag	k1gInSc4	airbag
<g/>
"	"	kIx"	"
nahrazující	nahrazující	k2eAgInPc4d1	nahrazující
pásy	pás	k1gInPc4	pás
použitý	použitý	k2eAgInSc4d1	použitý
v	v	k7c6	v
zámoří	zámoří	k1gNnSc6	zámoří
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
evropský	evropský	k2eAgInSc1d1	evropský
airbag	airbag	k1gInSc1	airbag
je	být	k5eAaImIp3nS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
pro	pro	k7c4	pro
součinnost	součinnost	k1gFnSc4	součinnost
s	s	k7c7	s
pásy	pás	k1gInPc7	pás
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
pásů	pás	k1gInPc2	pás
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
nepřipoutaní	připoutaný	k2eNgMnPc1d1	nepřipoutaný
spolucestující	spolucestující	k1gMnPc1	spolucestující
na	na	k7c6	na
zadních	zadní	k2eAgNnPc6d1	zadní
sedadlech	sedadlo	k1gNnPc6	sedadlo
-	-	kIx~	-
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
totiž	totiž	k9	totiž
při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
vymrštěni	vymrštit	k5eAaPmNgMnP	vymrštit
a	a	k8xC	a
zranit	zranit	k5eAaPmF	zranit
osoby	osoba	k1gFnPc4	osoba
na	na	k7c6	na
předních	přední	k2eAgNnPc6d1	přední
sedadlech	sedadlo	k1gNnPc6	sedadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
organizace	organizace	k1gFnSc2	organizace
BESIP	BESIP	kA	BESIP
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
nepoužívání	nepoužívání	k1gNnSc4	nepoužívání
pásů	pás	k1gInPc2	pás
riziko	riziko	k1gNnSc4	riziko
smrti	smrt	k1gFnSc2	smrt
při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
na	na	k7c6	na
předním	přední	k2eAgNnSc6d1	přední
sedadle	sedadlo	k1gNnSc6	sedadlo
6	[number]	k4	6
<g/>
krát	krát	k6eAd1	krát
<g/>
,	,	kIx,	,
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
dokonce	dokonce	k9	dokonce
8	[number]	k4	8
<g/>
krát	krát	k6eAd1	krát
(	(	kIx(	(
<g/>
u	u	k7c2	u
řidičů	řidič	k1gMnPc2	řidič
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
dokonce	dokonce	k9	dokonce
14	[number]	k4	14
<g/>
krát	krát	k6eAd1	krát
mimo	mimo	k7c4	mimo
obec	obec	k1gFnSc4	obec
<g/>
,	,	kIx,	,
12,8	[number]	k4	12,8
<g/>
krát	krát	k6eAd1	krát
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pasažéři	pasažér	k1gMnPc1	pasažér
na	na	k7c6	na
zadních	zadní	k2eAgInPc6d1	zadní
sedalech	sedal	k1gInPc6	sedal
podle	podle	k7c2	podle
téhož	týž	k3xTgInSc2	týž
zdroje	zdroj	k1gInSc2	zdroj
umírají	umírat	k5eAaImIp3nP	umírat
3,9	[number]	k4	3,9
<g/>
x	x	k?	x
častěji	často	k6eAd2	často
než	než	k8xS	než
připoutaní	připoutaný	k2eAgMnPc1d1	připoutaný
<g/>
,	,	kIx,	,
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
4,2	[number]	k4	4,2
<g/>
x.	x.	k?	x.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgNnPc2d1	jiné
studií	studio	k1gNnPc2	studio
by	by	kYmCp3nP	by
dopravní	dopravní	k2eAgFnPc4d1	dopravní
nehody	nehoda	k1gFnPc4	nehoda
mohlo	moct	k5eAaImAgNnS	moct
přežit	přežit	k2eAgMnSc1d1	přežit
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
pás	pás	k1gInSc4	pás
zapnutý	zapnutý	k2eAgInSc4d1	zapnutý
<g/>
.	.	kIx.	.
<g/>
Řada	řada	k1gFnSc1	řada
těhotných	těhotný	k2eAgFnPc2d1	těhotná
žen	žena	k1gFnPc2	žena
nepoužívá	používat	k5eNaImIp3nS	používat
pásy	pás	k1gInPc4	pás
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
poškodit	poškodit	k5eAaPmF	poškodit
plod	plod	k1gInSc4	plod
při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
a	a	k8xC	a
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
jim	on	k3xPp3gMnPc3	on
nepohodlné	pohodlný	k2eNgNnSc1d1	nepohodlné
<g/>
,	,	kIx,	,
gynekolog	gynekolog	k1gMnSc1	gynekolog
Jiří	Jiří	k1gMnSc1	Jiří
Kepák	Kepák	k1gMnSc1	Kepák
však	však	k9	však
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
"	"	kIx"	"
<g/>
zjevné	zjevný	k2eAgInPc1d1	zjevný
důkazy	důkaz	k1gInPc1	důkaz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
používání	používání	k1gNnSc1	používání
pásů	pás	k1gInPc2	pás
v	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
bezpečné	bezpečný	k2eAgNnSc1d1	bezpečné
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
i	i	k9	i
u	u	k7c2	u
těhotných	těhotný	k2eAgFnPc2d1	těhotná
žen	žena	k1gFnPc2	žena
snižuje	snižovat	k5eAaImIp3nS	snižovat
riziko	riziko	k1gNnSc1	riziko
smrti	smrt	k1gFnSc2	smrt
či	či	k8xC	či
poranění	poranění	k1gNnSc2	poranění
ženy	žena	k1gFnSc2	žena
samotné	samotný	k2eAgFnSc2d1	samotná
i	i	k8xC	i
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dětská	dětský	k2eAgFnSc1d1	dětská
autosedačka	autosedačka	k1gFnSc1	autosedačka
</s>
</p>
<p>
<s>
Brzdy	brzda	k1gFnPc1	brzda
</s>
</p>
<p>
<s>
Airbag	airbag	k1gInSc1	airbag
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
a	a	k8xC	a
zdroje	zdroj	k1gInPc1	zdroj
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
pás	pás	k1gInSc4	pás
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Sicherheitsgurt	Sicherheitsgurta	k1gFnPc2	Sicherheitsgurta
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Seat_belt	Seat_belt	k1gInSc4	Seat_belt
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
pásy	pás	k1gInPc1	pás
<g/>
,	,	kIx,	,
iBESIP	iBESIP	k?	iBESIP
<g/>
,	,	kIx,	,
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
nedatováno	datován	k2eNgNnSc1d1	nedatováno
</s>
</p>
<p>
<s>
Pás	pás	k1gInSc1	pás
života	život	k1gInSc2	život
</s>
</p>
<p>
<s>
Bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
pásy	pás	k1gInPc1	pás
–	–	k?	–
Dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
usmrcených	usmrcený	k2eAgFnPc2d1	usmrcená
by	by	kYmCp3nS	by
přežily	přežít	k5eAaPmAgFnP	přežít
nehodu	nehoda	k1gFnSc4	nehoda
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
měly	mít	k5eAaImAgFnP	mít
zapnutý	zapnutý	k2eAgInSc4d1	zapnutý
bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
pás	pás	k1gInSc4	pás
<g/>
,	,	kIx,	,
Autoklub	autoklub	k1gInSc4	autoklub
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
nedatováno	datovat	k5eNaImNgNnS	datovat
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
kampaně	kampaň	k1gFnSc2	kampaň
"	"	kIx"	"
<g/>
Na	na	k7c4	na
co	co	k9	co
pásy	pás	k1gInPc1	pás
<g/>
?	?	kIx.	?
</s>
<s>
Zlomte	zlomit	k5eAaPmRp2nP	zlomit
vaz	vaz	k1gInSc4	vaz
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
zahájené	zahájený	k2eAgInPc4d1	zahájený
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Šikl	Šikl	k1gMnSc1	Šikl
<g/>
:	:	kIx,	:
Proč	proč	k6eAd1	proč
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
používat	používat	k5eAaImF	používat
bezpečnostní	bezpečnostní	k2eAgInPc4d1	bezpečnostní
pásy	pás	k1gInPc4	pás
<g/>
,	,	kIx,	,
Autorevue	Autorevu	k1gInPc4	Autorevu
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
MUDr.	MUDr.	kA	MUDr.
Jiří	Jiří	k1gMnSc1	Jiří
Kepák	Kepák	k1gMnSc1	Kepák
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
<g/>
:	:	kIx,	:
Bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
pásy	pás	k1gInPc1	pás
kolem	kolem	k7c2	kolem
těhotných	těhotný	k2eAgFnPc2d1	těhotná
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
měsíčník	měsíčník	k1gMnSc1	měsíčník
Policista	policista	k1gMnSc1	policista
<g/>
,	,	kIx,	,
č.	č.	k?	č.
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
Policie	policie	k1gFnSc1	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
odbor	odbor	k1gInSc1	odbor
prevence	prevence	k1gFnSc2	prevence
kriminality	kriminalita	k1gFnSc2	kriminalita
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
používat	používat	k5eAaImF	používat
bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
pásy	pás	k1gInPc1	pás
v	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
<g/>
,	,	kIx,	,
Doktorka	doktorka	k1gFnSc1	doktorka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
neuveden	uveden	k2eNgMnSc1d1	neuveden
<g/>
,	,	kIx,	,
jako	jako	k9	jako
zdroj	zdroj	k1gInSc1	zdroj
uvedena	uvést	k5eAaPmNgFnS	uvést
ČTK	ČTK	kA	ČTK
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Čech	Čechy	k1gFnPc2	Čechy
<g/>
:	:	kIx,	:
Bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
pásy	pás	k1gInPc1	pás
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
vyvinuté	vyvinutý	k2eAgFnPc4d1	vyvinutá
<g/>
"	"	kIx"	"
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
Novinky	novinka	k1gFnPc1	novinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
</s>
</p>
