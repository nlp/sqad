<s>
Daň	daň	k1gFnSc1
z	z	k7c2
přidané	přidaný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
</s>
<s>
Daň	daň	k1gFnSc1
z	z	k7c2
přidané	přidaný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
DPH	DPH	kA
<g/>
)	)	kIx)
tvoří	tvořit	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
nejdůležitějších	důležitý	k2eAgInPc2d3
příjmů	příjem	k1gInPc2
státního	státní	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Platí	platit	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
všichni	všechen	k3xTgMnPc1
při	při	k7c6
nákupu	nákup	k1gInSc6
většiny	většina	k1gFnSc2
zboží	zboží	k1gNnSc2
a	a	k8xC
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
také	také	k9
někdy	někdy	k6eAd1
říká	říkat	k5eAaImIp3nS
univerzální	univerzální	k2eAgFnSc4d1
daň	daň	k1gFnSc4
nebo	nebo	k8xC
též	též	k9
všestranná	všestranný	k2eAgFnSc1d1
daň	daň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princip	princip	k1gInSc4
této	tento	k3xDgFnSc2
daně	daň	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
dodavatel	dodavatel	k1gMnSc1
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
registrován	registrovat	k5eAaBmNgMnS
jako	jako	k9
plátce	plátce	k1gMnSc1
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
odvést	odvést	k5eAaPmF
z	z	k7c2
obchodu	obchod	k1gInSc2
část	část	k1gFnSc1
hodnoty	hodnota	k1gFnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
obchod	obchod	k1gInSc1
předmětem	předmět	k1gInSc7
daně	daň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
odběratel	odběratel	k1gMnSc1
si	se	k3xPyFc3
za	za	k7c2
jistých	jistý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
může	moct	k5eAaImIp3nS
zažádat	zažádat	k5eAaPmF
o	o	k7c6
vrácení	vrácení	k1gNnSc6
daně	daň	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
při	při	k7c6
obchodu	obchod	k1gInSc6
dodavateli	dodavatel	k1gMnSc3
(	(	kIx(
<g/>
plátci	plátce	k1gMnSc3
<g/>
)	)	kIx)
zaplatil	zaplatit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
výhodou	výhoda	k1gFnSc7
této	tento	k3xDgFnSc2
daně	daň	k1gFnSc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
snadno	snadno	k6eAd1
vymáhá	vymáhat	k5eAaImIp3nS
a	a	k8xC
subjekty	subjekt	k1gInPc1
se	se	k3xPyFc4
jejímu	její	k3xOp3gMnSc3
placení	placený	k2eAgMnPc1d1
mohou	moct	k5eAaImIp3nP
hůře	zle	k6eAd2
vyhnout	vyhnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
typ	typ	k1gInSc1
nepřímé	přímý	k2eNgFnSc2d1
daně	daň	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
dopředu	dopředu	k6eAd1
jednoznačně	jednoznačně	k6eAd1
určit	určit	k5eAaPmF
daňového	daňový	k2eAgMnSc4d1
poplatníka	poplatník	k1gMnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
osobu	osoba	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
bude	být	k5eAaImBp3nS
v	v	k7c6
konečné	konečný	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
daň	daň	k1gFnSc4
platit	platit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definována	definován	k2eAgFnSc1d1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
pouze	pouze	k6eAd1
osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
konkrétní	konkrétní	k2eAgInSc4d1
nepřímou	přímý	k2eNgFnSc4d1
daň	daň	k1gFnSc4
odvádí	odvádět	k5eAaImIp3nP
státu	stát	k1gInSc3
<g/>
,	,	kIx,
tedy	tedy	k9
plátce	plátce	k1gMnPc4
daně	daň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
daň	daň	k1gFnSc1
zavedena	zaveden	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1993	#num#	k4
a	a	k8xC
nahradila	nahradit	k5eAaPmAgFnS
tak	tak	k9
dříve	dříve	k6eAd2
používanou	používaný	k2eAgFnSc4d1
daň	daň	k1gFnSc4
z	z	k7c2
obratu	obrat	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Sazby	sazba	k1gFnPc1
DPH	DPH	kA
v	v	k7c6
některých	některý	k3yIgFnPc6
evropských	evropský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
</s>
<s>
Státzákladní	Státzákladný	k2eAgMnPc1d1
sazbasnížená	sazbasnížený	k2eAgNnPc1d1
sazba	sazba	k1gFnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
<g/>
21	#num#	k4
%	%	kIx~
<g/>
15	#num#	k4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
%	%	kIx~
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
21	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
12	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
%	%	kIx~
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
<g/>
25	#num#	k4
%	%	kIx~
<g/>
0	#num#	k4
%	%	kIx~
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
20	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
9	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
24	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
14	#num#	k4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
%	%	kIx~
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
20	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
10	#num#	k4
(	(	kIx(
<g/>
5,5	5,5	k4
<g/>
)	)	kIx)
<g/>
%	%	kIx~
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
25	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
10	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
17	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
14	#num#	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
%	%	kIx~
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
<g/>
27	#num#	k4
%	%	kIx~
<g/>
18	#num#	k4
%	%	kIx~
</s>
<s>
Malta	Malta	k1gFnSc1
<g/>
18	#num#	k4
%	%	kIx~
<g/>
5	#num#	k4
%	%	kIx~
</s>
<s>
Německo	Německo	k1gNnSc1
<g/>
19	#num#	k4
%	%	kIx~
<g/>
7	#num#	k4
%	%	kIx~
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
21	#num#	k4
%	%	kIx~
<g/>
6	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
21	#num#	k4
<g/>
%	%	kIx~
daň	daň	k1gFnSc1
z	z	k7c2
pojištění	pojištění	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Polsko	Polsko	k1gNnSc1
<g/>
23	#num#	k4
%	%	kIx~
<g/>
8	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
%	%	kIx~
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
<g/>
20	#num#	k4
%	%	kIx~
<g/>
10	#num#	k4
%	%	kIx~
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
24	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
13	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
%	%	kIx~
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
<g/>
20	#num#	k4
%	%	kIx~
<g/>
10	#num#	k4
%	%	kIx~
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
22	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
9,5	9,5	k4
<g/>
%	%	kIx~
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
<g/>
25	#num#	k4
%	%	kIx~
<g/>
12	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
%	%	kIx~
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
7,7	7,7	k4
%	%	kIx~
</s>
<s>
2,5	2,5	k4
(	(	kIx(
<g/>
3,7	3,7	k4
<g/>
)	)	kIx)
%	%	kIx~
</s>
<s>
Rozpočet	rozpočet	k1gInSc1
ČR	ČR	kA
<g/>
,	,	kIx,
příjmy	příjem	k1gInPc4
z	z	k7c2
DPH	DPH	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
a	a	k8xC
Francie	Francie	k1gFnSc1
byly	být	k5eAaImAgFnP
prvními	první	k4xOgFnPc7
zeměmi	zem	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
zavedly	zavést	k5eAaPmAgFnP
DPH	DPH	kA
ve	v	k7c6
formě	forma	k1gFnSc6
obecné	obecný	k2eAgFnSc2d1
spotřební	spotřební	k2eAgFnSc2d1
daně	daň	k1gFnSc2
během	během	k7c2
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Tvůrce	tvůrce	k1gMnSc4
DPH	DPH	kA
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
zvané	zvaný	k2eAgFnPc1d1
taxe	taxe	k1gFnPc1
sur	sur	k?
la	la	k1gNnSc1
valeur	valeur	k1gMnSc1
ajoutée	ajouté	k1gMnSc2
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
TVA	TVA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
francouzský	francouzský	k2eAgMnSc1d1
ekonom	ekonom	k1gMnSc1
Maurice	Maurika	k1gFnSc3
Lauré	Laurý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
použil	použít	k5eAaPmAgInS
daň	daň	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
stal	stát	k5eAaPmAgMnS
ředitelem	ředitel	k1gMnSc7
francouzské	francouzský	k2eAgFnSc2d1
daňové	daňový	k2eAgFnSc2d1
správy	správa	k1gFnSc2
(	(	kIx(
<g/>
Direction	Direction	k1gInSc1
générale	générale	k6eAd1
des	des	k1gNnSc4
impôts	impôtsa	k1gFnPc2
<g/>
)	)	kIx)
v	v	k7c6
kolonii	kolonie	k1gFnSc6
Ivory	Ivora	k1gFnSc2
Coast	Coast	k1gInSc1
(	(	kIx(
<g/>
Côte	Côte	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Ivoire	Ivoir	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
úspěšném	úspěšný	k2eAgInSc6d1
experimentu	experiment	k1gInSc6
byla	být	k5eAaImAgFnS
ve	v	k7c4
Francii	Francie	k1gFnSc4
zavedena	zavést	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Daň	daň	k1gFnSc1
se	se	k3xPyFc4
z	z	k7c2
počátku	počátek	k1gInSc2
vztahovala	vztahovat	k5eAaImAgFnS
pouze	pouze	k6eAd1
na	na	k7c4
velké	velký	k2eAgFnPc4d1
korporace	korporace	k1gFnPc4
<g/>
,	,	kIx,
časem	časem	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
rozšířila	rozšířit	k5eAaPmAgFnS
na	na	k7c4
všechny	všechen	k3xTgInPc4
druhy	druh	k1gInPc4
zboží	zboží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
tvoří	tvořit	k5eAaImIp3nS
DPH	DPH	kA
ve	v	k7c6
Francii	Francie	k1gFnSc6
přes	přes	k7c4
polovinu	polovina	k1gFnSc4
příjmů	příjem	k1gInPc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Princip	princip	k1gInSc1
</s>
<s>
Princip	princip	k1gInSc1
daně	daň	k1gFnSc2
z	z	k7c2
přidané	přidaný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
odvedení	odvedení	k1gNnSc6
daně	daň	k1gFnSc2
pouze	pouze	k6eAd1
z	z	k7c2
rozdílu	rozdíl	k1gInSc2
ceny	cena	k1gFnSc2
mezi	mezi	k7c7
vstupy	vstup	k1gInPc7
a	a	k8xC
výstupy	výstup	k1gInPc7
<g/>
,	,	kIx,
tedy	tedy	k8xC
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
o	o	k7c4
kolik	kolik	k4yRc4,k4yIc4,k4yQc4
se	se	k3xPyFc4
cena	cena	k1gFnSc1
zboží	zboží	k1gNnSc2
u	u	k7c2
něho	on	k3xPp3gNnSc2
zvýší	zvýšit	k5eAaPmIp3nS
(	(	kIx(
<g/>
kolik	kolik	k9
k	k	k7c3
hodnotě	hodnota	k1gFnSc3
přidá	přidat	k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Subjekt	subjekt	k1gInSc1
platí	platit	k5eAaImIp3nS
dodavatelům	dodavatel	k1gMnPc3
cenu	cena	k1gFnSc4
včetně	včetně	k7c2
této	tento	k3xDgFnSc2
daně	daň	k1gFnSc2
a	a	k8xC
dodavatel	dodavatel	k1gMnSc1
sám	sám	k3xTgMnSc1
dostává	dostávat	k5eAaImIp3nS
zaplaceno	zaplatit	k5eAaPmNgNnS
za	za	k7c4
zboží	zboží	k1gNnSc4
včetně	včetně	k7c2
této	tento	k3xDgFnSc2
daně	daň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
státního	státní	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
pak	pak	k6eAd1
odvede	odvést	k5eAaPmIp3nS
rozdíl	rozdíl	k1gInSc1
mezi	mezi	k7c7
obdrženou	obdržený	k2eAgFnSc7d1
a	a	k8xC
zaplacenou	zaplacený	k2eAgFnSc7d1
daní	daň	k1gFnSc7
<g/>
,	,	kIx,
případně	případně	k6eAd1
mu	on	k3xPp3gMnSc3
může	moct	k5eAaImIp3nS
daň	daň	k1gFnSc4
být	být	k5eAaImF
vrácena	vrácen	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
</s>
<s>
První	první	k4xOgFnSc1
<g/>
,	,	kIx,
snížená	snížený	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
DPH	DPH	kA
v	v	k7c6
Česku	Česko	k1gNnSc6
činí	činit	k5eAaImIp3nS
15	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
vývoje	vývoj	k1gInSc2
inkasa	inkaso	k1gNnSc2
daně	daň	k1gFnSc2
z	z	k7c2
přidané	přidaný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1993	#num#	k4
až	až	k9
2016	#num#	k4
v	v	k7c6
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
</s>
<s>
Obchodník	obchodník	k1gMnSc1
zakoupí	zakoupit	k5eAaPmIp3nS
od	od	k7c2
svého	svůj	k3xOyFgMnSc2
dodavatele	dodavatel	k1gMnSc2
zboží	zboží	k1gNnSc2
za	za	k7c4
1150	#num#	k4
Kč	Kč	kA
–	–	k?
z	z	k7c2
toho	ten	k3xDgNnSc2
1000	#num#	k4
Kč	Kč	kA
tvoří	tvořit	k5eAaImIp3nS
cena	cena	k1gFnSc1
zboží	zboží	k1gNnSc2
a	a	k8xC
150	#num#	k4
Kč	Kč	kA
tvoří	tvořit	k5eAaImIp3nS
DPH	DPH	kA
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
dodavatel	dodavatel	k1gMnSc1
odvede	odvést	k5eAaPmIp3nS
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchodník	obchodník	k1gMnSc1
zboží	zboží	k1gNnSc2
rozveze	rozvézt	k5eAaPmIp3nS
svým	svůj	k3xOyFgMnPc3
odběratelům	odběratel	k1gMnPc3
a	a	k8xC
prodá	prodat	k5eAaPmIp3nS
za	za	k7c4
1725	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgNnSc2
1500	#num#	k4
Kč	Kč	kA
činí	činit	k5eAaImIp3nS
cena	cena	k1gFnSc1
zboží	zboží	k1gNnSc2
a	a	k8xC
225	#num#	k4
Kč	Kč	kA
DPH	DPH	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vyčíslení	vyčíslení	k1gNnSc6
daně	daň	k1gFnSc2
obchodník	obchodník	k1gMnSc1
od	od	k7c2
DPH	DPH	kA
na	na	k7c6
výstupu	výstup	k1gInSc6
(	(	kIx(
<g/>
225	#num#	k4
Kč	Kč	kA
<g/>
)	)	kIx)
odečte	odečíst	k5eAaPmIp3nS
DPH	DPH	kA
na	na	k7c6
vstupu	vstup	k1gInSc6
(	(	kIx(
<g/>
150	#num#	k4
Kč	Kč	kA
<g/>
)	)	kIx)
a	a	k8xC
odvede	odvést	k5eAaPmIp3nS
rozdíl	rozdíl	k1gInSc1
<g/>
:	:	kIx,
75	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
vznikla	vzniknout	k5eAaPmAgFnS
daňová	daňový	k2eAgFnSc1d1
povinnost	povinnost	k1gFnSc1
odvést	odvést	k5eAaPmF
státu	stát	k1gInSc2
75	#num#	k4
Kč	Kč	kA
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
záporného	záporný	k2eAgInSc2d1
rozdílu	rozdíl	k1gInSc2
mu	on	k3xPp3gMnSc3
vznikne	vzniknout	k5eAaPmIp3nS
nadměrný	nadměrný	k2eAgInSc1d1
odpočet	odpočet	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
mu	on	k3xPp3gNnSc3
finanční	finanční	k2eAgInSc4d1
úřad	úřad	k1gInSc4
vrátí	vrátit	k5eAaPmIp3nS
na	na	k7c4
účet	účet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
je	být	k5eAaImIp3nS
samozřejmě	samozřejmě	k6eAd1
značně	značně	k6eAd1
zjednodušen	zjednodušen	k2eAgMnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ilustroval	ilustrovat	k5eAaBmAgInS
princip	princip	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
různé	různý	k2eAgFnPc1d1
sazby	sazba	k1gFnPc1
daně	daň	k1gFnSc2
a	a	k8xC
lze	lze	k6eAd1
odpočíst	odpočíst	k5eAaPmF
i	i	k9
DPH	DPH	kA
ze	z	k7c2
zboží	zboží	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
obchodník	obchodník	k1gMnSc1
potřebuje	potřebovat	k5eAaImIp3nS
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Přidaná	přidaný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
</s>
<s>
Daní	danit	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
vlastně	vlastně	k9
hrubá	hrubý	k2eAgFnSc1d1
marže	marže	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
„	„	k?
<g/>
přidaná	přidaný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předmět	předmět	k1gInSc1
daně	daň	k1gFnSc2
</s>
<s>
Předmětem	předmět	k1gInSc7
daně	daň	k1gFnSc2
je	být	k5eAaImIp3nS
dodání	dodání	k1gNnSc1
zboží	zboží	k1gNnSc2
<g/>
,	,	kIx,
převod	převod	k1gInSc4
nemovitosti	nemovitost	k1gFnSc2
nebo	nebo	k8xC
poskytnutí	poskytnutí	k1gNnSc4
služby	služba	k1gFnSc2
za	za	k7c4
úplatu	úplata	k1gFnSc4
fyzickou	fyzický	k2eAgFnSc7d1
nebo	nebo	k8xC
právnickou	právnický	k2eAgFnSc7d1
osobou	osoba	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
samostatně	samostatně	k6eAd1
uskutečňuje	uskutečňovat	k5eAaImIp3nS
ekonomické	ekonomický	k2eAgFnSc3d1
činnosti	činnost	k1gFnSc3
<g/>
,	,	kIx,
s	s	k7c7
místem	místo	k1gNnSc7
plnění	plnění	k1gNnSc2
v	v	k7c6
tuzemsku	tuzemsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmětem	předmět	k1gInSc7
daně	daň	k1gFnSc2
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
také	také	k9
pořízení	pořízení	k1gNnSc1
zboží	zboží	k1gNnSc2
z	z	k7c2
jiného	jiný	k2eAgInSc2d1
členského	členský	k2eAgInSc2d1
státu	stát	k1gInSc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
za	za	k7c4
úplatu	úplata	k1gFnSc4
<g/>
,	,	kIx,
uskutečněné	uskutečněný	k2eAgNnSc4d1
v	v	k7c6
tuzemsku	tuzemsko	k1gNnSc6
osobou	osoba	k1gFnSc7
povinnou	povinný	k2eAgFnSc7d1
k	k	k7c3
dani	daň	k1gFnSc3
v	v	k7c6
rámci	rámec	k1gInSc6
uskutečňování	uskutečňování	k1gNnSc2
ekonomické	ekonomický	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
a	a	k8xC
také	také	k9
pořízení	pořízení	k1gNnSc4
nového	nový	k2eAgInSc2d1
dopravního	dopravní	k2eAgInSc2d1
prostředku	prostředek	k1gInSc2
z	z	k7c2
jiného	jiný	k2eAgInSc2d1
členského	členský	k2eAgInSc2d1
státu	stát	k1gInSc2
za	za	k7c4
úplatu	úplata	k1gFnSc4
osobou	osoba	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
není	být	k5eNaImIp3nS
osobou	osoba	k1gFnSc7
povinnou	povinný	k2eAgFnSc7d1
k	k	k7c3
dani	daň	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
předmětem	předmět	k1gInSc7
daně	daň	k1gFnSc2
dovoz	dovoz	k1gInSc1
zboží	zboží	k1gNnSc2
s	s	k7c7
místem	místo	k1gNnSc7
plnění	plnění	k1gNnSc2
v	v	k7c6
tuzemsku	tuzemsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zdanitelné	zdanitelný	k2eAgNnSc1d1
plnění	plnění	k1gNnSc1
je	být	k5eAaImIp3nS
plnění	plnění	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
předmětem	předmět	k1gInSc7
daně	daň	k1gFnSc2
a	a	k8xC
není	být	k5eNaImIp3nS
osvobozené	osvobozený	k2eAgNnSc1d1
od	od	k7c2
daně	daň	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vynětí	vynětí	k1gNnSc1
z	z	k7c2
předmětu	předmět	k1gInSc2
daně	daň	k1gFnSc2
</s>
<s>
Ze	z	k7c2
zákona	zákon	k1gInSc2
č.	č.	k?
235	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
dani	daň	k1gFnSc6
z	z	k7c2
přidané	přidaný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
§	§	k?
2	#num#	k4
<g/>
a	a	k8xC
není	být	k5eNaImIp3nS
předmětem	předmět	k1gInSc7
daně	daň	k1gFnSc2
pořízení	pořízení	k1gNnSc2
zboží	zboží	k1gNnSc2
z	z	k7c2
jiného	jiný	k2eAgInSc2d1
členského	členský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
dodání	dodání	k1gNnSc1
tohoto	tento	k3xDgNnSc2
zboží	zboží	k1gNnSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
tuzemsku	tuzemsko	k1gNnSc6
osvobozeno	osvobodit	k5eAaPmNgNnS
od	od	k7c2
daně	daň	k1gFnSc2
podle	podle	k7c2
§	§	k?
68	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
až	až	k9
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
v	v	k7c6
členském	členský	k2eAgInSc6d1
státě	stát	k1gInSc6
zahájení	zahájení	k1gNnSc2
odeslání	odeslání	k1gNnSc2
nebo	nebo	k8xC
přepravy	přeprava	k1gFnSc2
tohoto	tento	k3xDgNnSc2
zboží	zboží	k1gNnSc2
předmětem	předmět	k1gInSc7
daně	daň	k1gFnSc2
s	s	k7c7
použitím	použití	k1gNnSc7
zvláštního	zvláštní	k2eAgInSc2d1
režimu	režim	k1gInSc2
pro	pro	k7c4
obchodníky	obchodník	k1gMnPc4
se	s	k7c7
specifickým	specifický	k2eAgNnSc7d1
zbožím	zboží	k1gNnSc7
(	(	kIx(
<g/>
použité	použitý	k2eAgNnSc1d1
zboží	zboží	k1gNnSc1
<g/>
,	,	kIx,
umělecká	umělecký	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
<g/>
,	,	kIx,
sběratelské	sběratelský	k2eAgInPc4d1
předměty	předmět	k1gInPc4
a	a	k8xC
starožitnosti	starožitnost	k1gFnPc4
<g/>
)	)	kIx)
nebo	nebo	k8xC
s	s	k7c7
použitím	použití	k1gNnSc7
pro	pro	k7c4
prodej	prodej	k1gInSc4
veřejnou	veřejný	k2eAgFnSc7d1
dražbou	dražba	k1gFnSc7
a	a	k8xC
nebo	nebo	k8xC
s	s	k7c7
použitím	použití	k1gNnSc7
přechodného	přechodný	k2eAgInSc2d1
režimu	režim	k1gInSc2
pro	pro	k7c4
použité	použitý	k2eAgInPc4d1
dopravní	dopravní	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Zboží	zboží	k1gNnSc1
není	být	k5eNaImIp3nS
předmětem	předmět	k1gInSc7
daně	daň	k1gFnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
celková	celkový	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
pořízení	pořízení	k1gNnSc2
zboží	zboží	k1gNnSc2
z	z	k7c2
jiného	jiný	k2eAgInSc2d1
členského	členský	k2eAgInSc2d1
státu	stát	k1gInSc2
nepřekročila	překročit	k5eNaPmAgFnS
hranici	hranice	k1gFnSc4
326	#num#	k4
000	#num#	k4
Kč	Kč	kA
v	v	k7c6
příslušném	příslušný	k2eAgNnSc6d1
ani	ani	k8xC
v	v	k7c6
předcházejícím	předcházející	k2eAgInSc6d1
roce	rok	k1gInSc6
a	a	k8xC
bylo	být	k5eAaImAgNnS
pořízeno	pořízen	k2eAgNnSc1d1
osobou	osoba	k1gFnSc7
povinnou	povinný	k2eAgFnSc7d1
k	k	k7c3
dani	daň	k1gFnSc3
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
tuzemsku	tuzemsko	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
není	být	k5eNaImIp3nS
plátcem	plátce	k1gMnSc7
nebo	nebo	k8xC
osvobozenou	osvobozený	k2eAgFnSc7d1
osobou	osoba	k1gFnSc7
<g/>
,	,	kIx,
<g/>
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
není	být	k5eNaImIp3nS
plátcem	plátce	k1gMnSc7
nebo	nebo	k8xC
osobou	osoba	k1gFnSc7
povinnou	povinný	k2eAgFnSc7d1
k	k	k7c3
dani	daň	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
uskutečňuje	uskutečňovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
plnění	plnění	k1gNnSc1
osvobozená	osvobozený	k2eAgFnSc1d1
od	od	k7c2
daně	daň	k1gFnSc2
bez	bez	k7c2
nároku	nárok	k1gInSc2
na	na	k7c4
odpočet	odpočet	k1gInSc4
daně	daň	k1gFnSc2
nebo	nebo	k8xC
na	na	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
se	se	k3xPyFc4
v	v	k7c6
jiném	jiný	k2eAgInSc6d1
členském	členský	k2eAgInSc6d1
státě	stát	k1gInSc6
vztahuje	vztahovat	k5eAaImIp3nS
společný	společný	k2eAgInSc4d1
režim	režim	k1gInSc4
daňového	daňový	k2eAgInSc2d1
paušálu	paušál	k1gInSc2
pro	pro	k7c4
zemědělce	zemědělec	k1gMnPc4
a	a	k8xC
nebo	nebo	k8xC
právnickou	právnický	k2eAgFnSc7d1
osobou	osoba	k1gFnSc7
nepovinnou	povinný	k2eNgFnSc7d1
k	k	k7c3
dani	daň	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Vynětí	vynětí	k1gNnSc1
z	z	k7c2
předmětu	předmět	k1gInSc2
daně	daň	k1gFnSc2
se	se	k3xPyFc4
nevztahují	vztahovat	k5eNaImIp3nP
na	na	k7c4
pořízení	pořízení	k1gNnSc4
zboží	zboží	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
předmětem	předmět	k1gInSc7
spotřební	spotřební	k2eAgFnSc2d1
daně	daň	k1gFnSc2
nebo	nebo	k8xC
pořízení	pořízení	k1gNnSc4
nového	nový	k2eAgInSc2d1
dopravního	dopravní	k2eAgInSc2d1
prostředku	prostředek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plátce	plátce	k1gMnSc1
DPH	DPH	kA
</s>
<s>
Plátcem	plátce	k1gMnSc7
daně	daň	k1gFnSc2
z	z	k7c2
přidané	přidaný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
je	být	k5eAaImIp3nS
každý	každý	k3xTgInSc1
subjekt	subjekt	k1gInSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
<g/>
,	,	kIx,
provozovnou	provozovna	k1gFnSc7
či	či	k8xC
místem	místo	k1gNnSc7
podnikání	podnikání	k1gNnSc2
registrovaný	registrovaný	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
plátce	plátce	k1gMnSc1
DPH	DPH	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
se	s	k7c7
plátcem	plátce	k1gMnSc7
musí	muset	k5eAaImIp3nP
povinně	povinně	k6eAd1
stát	stát	k5eAaPmF,k5eAaImF
subjekt	subjekt	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
obrat	obrat	k1gInSc1
přesáhl	přesáhnout	k5eAaPmAgInS
za	za	k7c4
12	#num#	k4
po	po	k7c6
sobě	se	k3xPyFc3
jdoucích	jdoucí	k2eAgInPc2d1
kalendářních	kalendářní	k2eAgInPc2d1
měsíců	měsíc	k1gInPc2
částku	částka	k1gFnSc4
1	#num#	k4
000	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimku	výjimek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
uskutečňuje	uskutečňovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
plnění	plnění	k1gNnSc1
osvobozená	osvobozený	k2eAgFnSc1d1
od	od	k7c2
daně	daň	k1gFnSc2
bez	bez	k7c2
nároku	nárok	k1gInSc2
na	na	k7c4
odpočet	odpočet	k1gInSc4
daně	daň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přihlášku	přihláška	k1gFnSc4
k	k	k7c3
registraci	registrace	k1gFnSc3
musí	muset	k5eAaImIp3nP
podat	podat	k5eAaPmF
do	do	k7c2
15	#num#	k4
dnů	den	k1gInPc2
po	po	k7c6
skončení	skončení	k1gNnSc6
termínu	termín	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
subjekt	subjekt	k1gInSc4
překročil	překročit	k5eAaPmAgInS
stanovený	stanovený	k2eAgInSc4d1
limit	limit	k1gInSc4
a	a	k8xC
plátcem	plátce	k1gMnSc7
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
prvního	první	k4xOgInSc2
dne	den	k1gInSc2
druhého	druhý	k4xOgInSc2
měsíce	měsíc	k1gInSc2
následujícího	následující	k2eAgInSc2d1
po	po	k7c6
měsíci	měsíc	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
překročil	překročit	k5eAaPmAgInS
stanovený	stanovený	k2eAgInSc1d1
obrat	obrat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
to	ten	k3xDgNnSc1
neudělá	udělat	k5eNaPmIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
finanční	finanční	k2eAgInSc1d1
úřad	úřad	k1gInSc1
povinen	povinen	k2eAgInSc1d1
ze	z	k7c2
zákona	zákon	k1gInSc2
udělit	udělit	k5eAaPmF
pokutu	pokuta	k1gFnSc4
10	#num#	k4
%	%	kIx~
ze	z	k7c2
všech	všecek	k3xTgInPc2
příjmů	příjem	k1gInPc2
za	za	k7c4
období	období	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
plátcem	plátce	k1gMnSc7
daně	daň	k1gFnSc2
a	a	k8xC
nebyl	být	k5eNaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2010	#num#	k4
se	s	k7c7
plátcem	plátce	k1gMnSc7
DPH	DPH	kA
stává	stávat	k5eAaImIp3nS
každý	každý	k3xTgMnSc1
český	český	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
poskytne	poskytnout	k5eAaPmIp3nS
nebo	nebo	k8xC
přijme	přijmout	k5eAaPmIp3nS
službu	služba	k1gFnSc4
od	od	k7c2
podnikatele	podnikatel	k1gMnSc2
registrovaného	registrovaný	k2eAgMnSc2d1
k	k	k7c3
DPH	DPH	kA
v	v	k7c6
jiném	jiný	k2eAgInSc6d1
státu	stát	k1gInSc6
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tedy	tedy	k8xC
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
výšku	výška	k1gFnSc4
obratu	obrat	k1gInSc2
(	(	kIx(
<g/>
od	od	k7c2
1	#num#	k4
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
zákon	zákon	k1gInSc1
určuje	určovat	k5eAaImIp3nS
speciální	speciální	k2eAgFnPc4d1
situace	situace	k1gFnPc4
<g/>
,	,	kIx,
za	za	k7c2
kterých	který	k3yIgInPc2,k3yRgInPc2,k3yQgInPc2
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
okamžitě	okamžitě	k6eAd1
se	se	k3xPyFc4
k	k	k7c3
DPH	DPH	kA
registrovat	registrovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firmy	firma	k1gFnPc1
i	i	k8xC
podnikatelé	podnikatel	k1gMnPc1
se	se	k3xPyFc4
taktéž	taktéž	k?
můžou	můžou	k?
stát	stát	k1gInSc1
dobrovolným	dobrovolný	k2eAgMnSc7d1
plátcem	plátce	k1gMnSc7
DPH	DPH	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
státem	stát	k1gInSc7
vedený	vedený	k2eAgInSc1d1
registr	registr	k1gInSc1
plátců	plátce	k1gMnPc2
DPH	DPH	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
můžete	moct	k5eAaImIp2nP
zjistit	zjistit	k5eAaPmF
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
subjekt	subjekt	k1gInSc4
plátce	plátce	k1gMnSc2
DPH	DPH	kA
či	či	k8xC
nikoli	nikoli	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poplatník	poplatník	k1gMnSc1
DPH	DPH	kA
</s>
<s>
Daňový	daňový	k2eAgInSc1d1
subjekt	subjekt	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ze	z	k7c2
zákona	zákon	k1gInSc2
hradí	hradit	k5eAaImIp3nS
daň	daň	k1gFnSc4
z	z	k7c2
předmětu	předmět	k1gInSc2
svého	svůj	k3xOyFgNnSc2
zdanění	zdanění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Úprava	úprava	k1gFnSc1
DPH	DPH	kA
ze	z	k7c2
strany	strana	k1gFnSc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
vymezuje	vymezovat	k5eAaImIp3nS
zboží	zboží	k1gNnSc4
a	a	k8xC
služby	služba	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
mohou	moct	k5eAaImIp3nP
(	(	kIx(
<g/>
ale	ale	k8xC
nemusí	muset	k5eNaImIp3nS
<g/>
)	)	kIx)
být	být	k5eAaImF
předmětem	předmět	k1gInSc7
snížené	snížený	k2eAgFnPc4d1
sazby	sazba	k1gFnPc4
(	(	kIx(
<g/>
či	či	k8xC
snížených	snížený	k2eAgFnPc2d1
sazeb	sazba	k1gFnPc2
<g/>
)	)	kIx)
DPH	DPH	kA
<g/>
,	,	kIx,
a	a	k8xC
stanovovala	stanovovat	k5eAaImAgFnS
minimální	minimální	k2eAgFnSc4d1
výši	výše	k1gFnSc4
základní	základní	k2eAgFnSc2d1
sazby	sazba	k1gFnSc2
DPH	DPH	kA
(	(	kIx(
<g/>
15	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
minimální	minimální	k2eAgFnSc4d1
výši	výše	k1gFnSc4
snížené	snížený	k2eAgFnSc2d1
sazby	sazba	k1gFnSc2
DPH	DPH	kA
(	(	kIx(
<g/>
5	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
stanovuje	stanovovat	k5eAaImIp3nS
zboží	zboží	k1gNnSc4
a	a	k8xC
služby	služba	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
od	od	k7c2
DPH	DPH	kA
osvobozeny	osvobodit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povinnost	povinnost	k1gFnSc1
minimální	minimální	k2eAgFnSc2d1
sazby	sazba	k1gFnSc2
15	#num#	k4
%	%	kIx~
vypršela	vypršet	k5eAaPmAgFnS
koncem	koncem	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
a	a	k8xC
Evropská	evropský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
plánu	plán	k1gInSc6
ji	on	k3xPp3gFnSc4
obnovovat	obnovovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2013	#num#	k4
byla	být	k5eAaImAgFnS
základní	základní	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
v	v	k7c6
členských	členský	k2eAgInPc6d1
státech	stát	k1gInPc6
EU	EU	kA
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
15	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
Lucembursko	Lucembursko	k1gNnSc1
<g/>
)	)	kIx)
až	až	k9
27	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
Maďarsko	Maďarsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
státy	stát	k1gInPc4
kromě	kromě	k7c2
Dánska	Dánsko	k1gNnSc2
také	také	k9
měly	mít	k5eAaImAgInP
alespoň	alespoň	k9
jednu	jeden	k4xCgFnSc4
sníženou	snížený	k2eAgFnSc4d1
sazbu	sazba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
zvýšilo	zvýšit	k5eAaPmAgNnS
Lucembursko	Lucembursko	k1gNnSc1
základní	základní	k2eAgFnSc4d1
sazbu	sazba	k1gFnSc4
DPH	DPH	kA
na	na	k7c4
17	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sazby	sazba	k1gFnPc1
DPH	DPH	kA
</s>
<s>
Daň	daň	k1gFnSc1
z	z	k7c2
přidané	přidaný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
do	do	k7c2
tří	tři	k4xCgFnPc2
sazeb	sazba	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
základní	základní	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
DPH	DPH	kA
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
21	#num#	k4
%	%	kIx~
</s>
<s>
první	první	k4xOgFnSc1
snížená	snížený	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
DPH	DPH	kA
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
15	#num#	k4
%	%	kIx~
</s>
<s>
druhá	druhý	k4xOgFnSc1
snížená	snížený	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
DPH	DPH	kA
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
10	#num#	k4
%	%	kIx~
</s>
<s>
Plátci	plátce	k1gMnPc1
DPH	DPH	kA
musí	muset	k5eAaImIp3nP
na	na	k7c4
svá	svůj	k3xOyFgNnPc4
uskutečněná	uskutečněný	k2eAgNnPc4d1
zdanitelná	zdanitelný	k2eAgNnPc4d1
plnění	plnění	k1gNnPc4
uplatňovat	uplatňovat	k5eAaImF
základní	základní	k2eAgFnSc4d1
sazbu	sazba	k1gFnSc4
DPH	DPH	kA
<g/>
,	,	kIx,
pokud	pokud	k8xS
zákon	zákon	k1gInSc1
nestanoví	stanovit	k5eNaPmIp3nS
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uplatňovat	uplatňovat	k5eAaImF
sníženou	snížený	k2eAgFnSc7d1
nebo	nebo	k8xC
druhou	druhý	k4xOgFnSc4
sníženou	snížený	k2eAgFnSc4d1
sazbu	sazba	k1gFnSc4
DPH	DPH	kA
mohou	moct	k5eAaImIp3nP
jen	jen	k9
v	v	k7c6
případech	případ	k1gInPc6
vyjmenovaných	vyjmenovaný	k2eAgInPc2d1
v	v	k7c6
zákoně	zákon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc4
sníženou	snížený	k2eAgFnSc4d1
sazbu	sazba	k1gFnSc4
se	se	k3xPyFc4
uplatňuje	uplatňovat	k5eAaImIp3nS
na	na	k7c4
teplo	teplo	k1gNnSc4
<g/>
,	,	kIx,
chlad	chlad	k1gInSc4
a	a	k8xC
zboží	zboží	k1gNnSc4
uvedené	uvedený	k2eAgNnSc4d1
v	v	k7c6
příloze	příloha	k1gFnSc6
3	#num#	k4
zákona	zákon	k1gInSc2
o	o	k7c6
DPH	DPH	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dani	daň	k1gFnSc3
také	také	k9
podléhají	podléhat	k5eAaImIp3nP
služby	služba	k1gFnPc1
uvedené	uvedený	k2eAgFnPc1d1
v	v	k7c6
příloze	příloha	k1gFnSc6
2	#num#	k4
<g/>
,	,	kIx,
kam	kam	k6eAd1
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
služby	služba	k1gFnPc4
fitcenter	fitcentrum	k1gNnPc2
a	a	k8xC
posiloven	posilovna	k1gFnPc2
a	a	k8xC
ubytovací	ubytovací	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
prosince	prosinec	k1gInSc2
2016	#num#	k4
platí	platit	k5eAaImIp3nS
první	první	k4xOgFnSc1
snížená	snížený	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
15	#num#	k4
%	%	kIx~
také	také	k9
pro	pro	k7c4
stravovací	stravovací	k2eAgFnPc4d1
služby	služba	k1gFnPc4
a	a	k8xC
podávání	podávání	k1gNnSc6
nápojů	nápoj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
sem	sem	k6eAd1
veškeré	veškerý	k3xTgFnPc4
restaurační	restaurační	k2eAgFnPc4d1
<g/>
,	,	kIx,
cateringové	cateringový	k2eAgFnPc4d1
a	a	k8xC
ostatní	ostatní	k2eAgFnPc4d1
stravovací	stravovací	k2eAgFnPc4d1
služby	služba	k1gFnPc4
a	a	k8xC
podávání	podávání	k1gNnSc1
nápojů	nápoj	k1gInPc2
–	–	k?
v	v	k7c6
příloze	příloha	k1gFnSc6
2	#num#	k4
uvedeny	uvést	k5eAaPmNgInP
pod	pod	k7c7
kódem	kód	k1gInSc7
56	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Podávání	podávání	k1gNnSc2
alkoholických	alkoholický	k2eAgInPc2d1
nápojů	nápoj	k1gInPc2
nebo	nebo	k8xC
tabákových	tabákový	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
má	mít	k5eAaImIp3nS
stále	stále	k6eAd1
základní	základní	k2eAgFnSc4d1
sazbu	sazba	k1gFnSc4
DPH	DPH	kA
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
dovozu	dovoz	k1gInSc2
uměleckých	umělecký	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
sběratelských	sběratelský	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
a	a	k8xC
starožitností	starožitnost	k1gFnPc2
uvedených	uvedený	k2eAgFnPc2d1
v	v	k7c6
zákonu	zákon	k1gInSc6
se	se	k3xPyFc4
uplatňuje	uplatňovat	k5eAaImIp3nS
první	první	k4xOgFnSc1
snížená	snížený	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
daně	daň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
dodání	dodání	k1gNnSc6
zboží	zboží	k1gNnSc2
nebo	nebo	k8xC
pořízení	pořízení	k1gNnSc2
zboží	zboží	k1gNnSc2
z	z	k7c2
jiného	jiný	k2eAgInSc2d1
členského	členský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
obsahují	obsahovat	k5eAaImIp3nP
druhy	druh	k1gInPc7
zboží	zboží	k1gNnSc2
podléhající	podléhající	k2eAgMnSc1d1
různým	různý	k2eAgFnPc3d1
sazbám	sazba	k1gFnPc3
daně	daň	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
uplatní	uplatnit	k5eAaPmIp3nS
nejvyšší	vysoký	k2eAgFnSc1d3
z	z	k7c2
těchto	tento	k3xDgFnPc2
sazeb	sazba	k1gFnPc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
není	být	k5eNaImIp3nS
dotčena	dotčen	k2eAgFnSc1d1
možnost	možnost	k1gFnSc1
uplatnit	uplatnit	k5eAaPmF
u	u	k7c2
každého	každý	k3xTgInSc2
druhu	druh	k1gInSc2
zboží	zboží	k1gNnSc2
příslušnou	příslušný	k2eAgFnSc4d1
sazbu	sazba	k1gFnSc4
daně	daň	k1gFnSc2
samostatně	samostatně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
snížená	snížený	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
<g/>
,	,	kIx,
platná	platný	k2eAgFnSc1d1
od	od	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
<g/>
,	,	kIx,
činí	činit	k5eAaImIp3nS
10	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uplatníme	uplatnit	k5eAaPmIp1nP
ji	on	k3xPp3gFnSc4
na	na	k7c4
položky	položka	k1gFnPc4
uvedené	uvedený	k2eAgFnPc4d1
v	v	k7c6
příloze	příloha	k1gFnSc6
3	#num#	k4
<g/>
a	a	k8xC
<g/>
)	)	kIx)
zákona	zákon	k1gInSc2
o	o	k7c6
DPH	DPH	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc3
sazbě	sazba	k1gFnSc3
podléhají	podléhat	k5eAaImIp3nP
např.	např.	kA
knihy	kniha	k1gFnPc1
<g/>
,	,	kIx,
léky	lék	k1gInPc1
<g/>
,	,	kIx,
nenahraditelná	nahraditelný	k2eNgFnSc1d1
dětská	dětský	k2eAgFnSc1d1
výživa	výživa	k1gFnSc1
<g/>
,	,	kIx,
suroviny	surovina	k1gFnPc1
určené	určený	k2eAgFnPc1d1
k	k	k7c3
výrobě	výroba	k1gFnSc3
potravin	potravina	k1gFnPc2
pro	pro	k7c4
lidi	člověk	k1gMnPc4
s	s	k7c7
celiakií	celiakie	k1gFnSc7
a	a	k8xC
fenylketonurií	fenylketonurie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Samovyměření	Samovyměření	k1gNnSc1
daně	daň	k1gFnSc2
</s>
<s>
Samovyměření	Samovyměření	k1gNnSc1
daně	daň	k1gFnSc2
z	z	k7c2
přidané	přidaný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
je	být	k5eAaImIp3nS
novým	nový	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
si	se	k3xPyFc3
vynutil	vynutit	k5eAaPmAgInS
přeshraniční	přeshraniční	k2eAgInSc4d1
obchod	obchod	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dovozce	dovozce	k1gMnSc1
zboží	zboží	k1gNnSc2
a	a	k8xC
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vyměřuje	vyměřovat	k5eAaImIp3nS
daň	daň	k1gFnSc4
na	na	k7c6
výstupu	výstup	k1gInSc6
<g/>
,	,	kIx,
si	se	k3xPyFc3
sám	sám	k3xTgMnSc1
vyměří	vyměřit	k5eAaPmIp3nS
daň	daň	k1gFnSc4
na	na	k7c6
vstupu	vstup	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tu	tu	k6eAd1
pak	pak	k6eAd1
uplatní	uplatnit	k5eAaPmIp3nS
jako	jako	k9
odpočet	odpočet	k1gInSc4
při	při	k7c6
vyúčtování	vyúčtování	k1gNnSc6
s	s	k7c7
finančním	finanční	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Minimální	minimální	k2eAgFnPc4d1
DPH	DPH	kA
v	v	k7c6
EU	EU	kA
činí	činit	k5eAaImIp3nS
5	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
dáno	dát	k5eAaPmNgNnS
právem	právo	k1gNnSc7
EU	EU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Režim	režim	k1gInSc1
přenesení	přenesení	k1gNnSc2
daňové	daňový	k2eAgFnSc2d1
povinnosti	povinnost	k1gFnSc2
</s>
<s>
Tuzemské	tuzemský	k2eAgNnSc1d1
přenesení	přenesení	k1gNnSc1
daňové	daňový	k2eAgFnSc2d1
povinnosti	povinnost	k1gFnSc2
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
nákupu	nákup	k1gInSc2
zlata	zlato	k1gNnSc2
<g/>
,	,	kIx,
pořízení	pořízení	k1gNnSc1
nemovitostí	nemovitost	k1gFnPc2
<g/>
,	,	kIx,
stavebních	stavební	k2eAgFnPc2d1
a	a	k8xC
montážních	montážní	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
,	,	kIx,
dodávek	dodávka	k1gFnPc2
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
<g/>
,	,	kIx,
poskytování	poskytování	k1gNnSc1
telekomunikačních	telekomunikační	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
prodeji	prodej	k1gInSc3
herních	herní	k2eAgFnPc2d1
konzolí	konzole	k1gFnPc2
<g/>
,	,	kIx,
tabletů	tablet	k1gInPc2
či	či	k8xC
notebooků	notebook	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
české	český	k2eAgFnSc2d1
sazby	sazba	k1gFnSc2
DPH	DPH	kA
</s>
<s>
Obdobízákladní	Obdobízákladný	k2eAgMnPc1d1
sazbasnížená	sazbasnížený	k2eAgFnSc1d1
sazbadruhá	sazbadruhat	k5eAaPmIp3nS,k5eAaImIp3nS
snížená	snížený	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1993	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
199423	#num#	k4
%	%	kIx~
<g/>
5	#num#	k4
%	%	kIx~
<g/>
–	–	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1995	#num#	k4
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
200422	#num#	k4
%	%	kIx~
<g/>
5	#num#	k4
%	%	kIx~
<g/>
–	–	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2004	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
200719	#num#	k4
%	%	kIx~
<g/>
5	#num#	k4
%	%	kIx~
<g/>
–	–	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
200919	#num#	k4
%	%	kIx~
<g/>
9	#num#	k4
%	%	kIx~
<g/>
–	–	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
201120	#num#	k4
%	%	kIx~
<g/>
10	#num#	k4
%	%	kIx~
<g/>
–	–	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
201220	#num#	k4
%	%	kIx~
<g/>
14	#num#	k4
%	%	kIx~
<g/>
–	–	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
201421	#num#	k4
%	%	kIx~
<g/>
15	#num#	k4
%	%	kIx~
<g/>
–	–	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
–	–	k?
Dosud	dosud	k6eAd1
<g/>
21	#num#	k4
%	%	kIx~
<g/>
15	#num#	k4
%	%	kIx~
<g/>
10	#num#	k4
%	%	kIx~
<g/>
*	*	kIx~
</s>
<s>
*	*	kIx~
<g/>
Platí	platit	k5eAaImIp3nS
jen	jen	k9
pro	pro	k7c4
zboží	zboží	k1gNnSc4
uvedené	uvedený	k2eAgFnSc2d1
v	v	k7c6
příslušné	příslušný	k2eAgFnSc6d1
příloze	příloha	k1gFnSc6
zákona	zákon	k1gInSc2
o	o	k7c6
DPH	DPH	kA
(	(	kIx(
<g/>
příloha	příloha	k1gFnSc1
č.	č.	k?
3	#num#	k4
<g/>
a	a	k8xC
k	k	k7c3
1.1	1.1	k4
<g/>
.2015	.2015	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
např.	např.	kA
pro	pro	k7c4
kojeneckou	kojenecký	k2eAgFnSc4d1
výživu	výživa	k1gFnSc4
<g/>
,	,	kIx,
vybrané	vybraný	k2eAgInPc4d1
mlýnské	mlýnský	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
,	,	kIx,
léky	lék	k1gInPc4
<g/>
,	,	kIx,
knihy	kniha	k1gFnPc4
a	a	k8xC
hudebniny	hudebnina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
DPH	DPH	kA
Státní	státní	k2eAgInSc1d1
rozpočet	rozpočet	k1gInSc1
<g/>
,	,	kIx,
Daňové	daňový	k2eAgInPc1d1
příjmy	příjem	k1gInPc1
-	-	kIx~
DPH	DPH	kA
<g/>
,	,	kIx,
kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
1	#num#	k4
2	#num#	k4
HELGASON	HELGASON	kA
<g/>
,	,	kIx,
Agnar	Agnar	k1gMnSc1
Freyr	Freyr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unleashing	Unleashing	k1gInSc1
the	the	k?
'	'	kIx"
<g/>
money	monea	k1gMnSc2
machine	machinout	k5eAaPmIp3nS
<g/>
'	'	kIx"
<g/>
:	:	kIx,
the	the	k?
domestic	domestice	k1gFnPc2
political	politicat	k5eAaPmAgInS
foundations	foundations	k1gInSc1
of	of	k?
VAT	vat	k1gInSc1
adoption	adoption	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Socio-Economic	Socio-Economic	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
797	#num#	k4
<g/>
–	–	k?
<g/>
813	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
ser	srát	k5eAaImRp2nS
<g/>
/	/	kIx~
<g/>
mwx	mwx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Les	les	k1gInSc1
recettes	recettes	k1gMnSc1
fiscales	fiscales	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Minister	Minister	k1gMnSc1
of	of	k?
the	the	k?
Economy	Econom	k1gInPc4
<g/>
,	,	kIx,
Industry	Industr	k1gInPc4
and	and	k?
Employment	Employment	k1gInSc4
(	(	kIx(
<g/>
France	Franc	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
30	#num#	k4
October	October	k1gInSc1
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
french	french	k1gInSc1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.lidovky.cz/dane-kultura-a-konkurence-0y4-/ln_noviny.asp?c=A110318_000065_ln_noviny_sko&	http://www.lidovky.cz/dane-kultura-a-konkurence-0y4-/ln_noviny.asp?c=A110318_000065_ln_noviny_sko&	k?
<g/>
↑	↑	k?
Zákon	zákon	k1gInSc1
o	o	k7c6
dani	daň	k1gFnSc6
z	z	k7c2
přidané	přidaný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
-	-	kIx~
Část	část	k1gFnSc1
I.	I.	kA
-	-	kIx~
Hlava	hlava	k1gFnSc1
1	#num#	k4
-	-	kIx~
Obecná	obecný	k2eAgNnPc1d1
ustanovení	ustanovení	k1gNnPc1
<g/>
.	.	kIx.
business	business	k1gInSc1
<g/>
.	.	kIx.
<g/>
center	centrum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Daň	daň	k1gFnSc1
z	z	k7c2
přidané	přidaný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
(	(	kIx(
<g/>
DPH	DPH	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.businessinfo.cz	www.businessinfo.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
o	o	k7c6
dani	daň	k1gFnSc6
z	z	k7c2
přidané	přidaný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
-	-	kIx~
Část	část	k1gFnSc1
I.	I.	kA
-	-	kIx~
Hlava	hlava	k1gFnSc1
1	#num#	k4
-	-	kIx~
Obecná	obecný	k2eAgNnPc1d1
ustanovení	ustanovení	k1gNnPc1
<g/>
.	.	kIx.
business	business	k1gInSc1
<g/>
.	.	kIx.
<g/>
center	centrum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Finanční	finanční	k2eAgFnSc1d1
správa	správa	k1gFnSc1
:	:	kIx,
Změny	změna	k1gFnPc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
DPH	DPH	kA
při	při	k7c6
podávání	podávání	k1gNnSc6
souhrnného	souhrnný	k2eAgNnSc2d1
hlášení	hlášení	k1gNnSc2
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Poskytujete	poskytovat	k5eAaImIp2nP
služby	služba	k1gFnPc4
v	v	k7c6
rámci	rámec	k1gInSc6
EU	EU	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
Stáváte	stávat	k5eAaImIp2nP
se	s	k7c7
plátcem	plátce	k1gMnSc7
DPH	DPH	kA
–	–	k?
Podnikatel	podnikatel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
http://www.yourtaxes.cz/cz/danova-registrace	http://www.yourtaxes.cz/cz/danova-registrace	k1gFnSc2
<g/>
↑	↑	k?
Napovíme	napovědět	k5eAaPmIp1nP
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
P.	P.	kA
Mach	Mach	k1gMnSc1
<g/>
:	:	kIx,
EU	EU	kA
už	už	k6eAd1
nebude	být	k5eNaImBp3nS
požadovat	požadovat	k5eAaImF
minimální	minimální	k2eAgFnSc4d1
sazbu	sazba	k1gFnSc4
DPH	DPH	kA
15	#num#	k4
%	%	kIx~
|	|	kIx~
Svobodní	svobodný	k2eAgMnPc1d1
<g/>
↑	↑	k?
EU	EU	kA
To	to	k9
Extend	Extend	k1gInSc1
Minimum	minimum	k1gNnSc1
15	#num#	k4
Percent	percento	k1gNnPc2
VAT	vata	k1gFnPc2
Rate	Rate	k1gFnSc1
<g/>
↑	↑	k?
VAT	vat	k1gInSc1
Rates	Rates	k1gMnSc1
Applied	Applied	k1gMnSc1
in	in	k?
the	the	k?
Member	Member	k1gMnSc1
States	States	k1gMnSc1
of	of	k?
the	the	k?
European	European	k1gInSc1
Union	union	k1gInSc1
<g/>
↑	↑	k?
Sazby	sazba	k1gFnSc2
DPH	DPH	kA
aktuálně	aktuálně	k6eAd1
-	-	kIx~
jakpodnikat	jakpodnikat	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.jakpodnikat.cz	www.jakpodnikat.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1
přidaná	přidaný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
</s>
<s>
Identifikační	identifikační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
osoby	osoba	k1gFnSc2
</s>
<s>
Daňové	daňový	k2eAgNnSc1d1
identifikační	identifikační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
DPH	DPH	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
https://web.archive.org/web/20130101034435/http://zakony.centrum.cz/zakon-o-dani-z-pridane-hodnoty-prilohy	https://web.archive.org/web/20130101034435/http://zakony.centrum.cz/zakon-o-dani-z-pridane-hodnoty-priloh	k1gInPc1
</s>
<s>
http://business.center.cz/business/pravo/zakony/dph/	http://business.center.cz/business/pravo/zakony/dph/	k?
</s>
<s>
https://www.zakonyprolidi.cz/cs/2004-235	https://www.zakonyprolidi.cz/cs/2004-235	k4
</s>
<s>
https://www.vypocet-dph.cz/	https://www.vypocet-dph.cz/	k?
–	–	k?
Jednoduchá	jednoduchý	k2eAgFnSc1d1
DPH	DPH	kA
kalkulačka	kalkulačka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
https://adisreg.mfcr.cz/cgi-bin/adis/idph/int_dp_prij.cgi	https://adisreg.mfcr.cz/cgi-bin/adis/idph/int_dp_prij.cgi	k1gNnSc1
-	-	kIx~
registr	registr	k1gInSc1
subjektů	subjekt	k1gInPc2
DPH	DPH	kA
spravující	spravující	k2eAgNnSc1d1
Ministerstvo	ministerstvo	k1gNnSc1
financí	finance	k1gFnPc2
ČR	ČR	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Daně	daň	k1gFnPc4
platné	platný	k2eAgFnPc4d1
v	v	k7c4
České	český	k2eAgFnPc4d1
repubice	repubice	k1gFnPc4
Přímé	přímý	k2eAgFnSc2d1
daně	daň	k1gFnSc2
</s>
<s>
Daň	daň	k1gFnSc1
z	z	k7c2
příjmů	příjem	k1gInPc2
•	•	k?
Daň	daň	k1gFnSc1
silniční	silniční	k2eAgFnSc1d1
•	•	k?
Daň	daň	k1gFnSc1
z	z	k7c2
nabytí	nabytí	k1gNnSc2
nemovitých	movitý	k2eNgFnPc2d1
věcí	věc	k1gFnPc2
•	•	k?
Daň	daň	k1gFnSc4
z	z	k7c2
nemovitých	movitý	k2eNgFnPc2d1
věcí	věc	k1gFnPc2
Nepřímé	přímý	k2eNgFnSc2d1
daně	daň	k1gFnSc2
</s>
<s>
Spotřební	spotřební	k2eAgFnSc1d1
daň	daň	k1gFnSc1
(	(	kIx(
<g/>
selektivní	selektivní	k2eAgFnSc1d1
daň	daň	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Daň	daň	k1gFnSc1
z	z	k7c2
přidané	přidaný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
(	(	kIx(
<g/>
univerzální	univerzální	k2eAgFnSc1d1
daň	daň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4038416-0	4038416-0	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
1679	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85141932	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85141932	#num#	k4
</s>
