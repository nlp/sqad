<p>
<s>
Bedřichovští	Bedřichovský	k2eAgMnPc1d1	Bedřichovský
z	z	k7c2	z
Lomné	lomný	k2eAgNnSc1d1	Lomné
bylo	být	k5eAaImAgNnS	být
jméno	jméno	k1gNnSc1	jméno
staročeské	staročeský	k2eAgFnSc2d1	staročeská
vladycké	vladycký	k2eAgFnSc2d1	vladycká
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
erbu	erb	k1gInSc2	erb
lilie	lilie	k1gFnSc2	lilie
v	v	k7c6	v
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
společného	společný	k2eAgMnSc4d1	společný
původu	původa	k1gMnSc4	původa
s	s	k7c7	s
vladyky	vladyka	k1gMnPc7	vladyka
z	z	k7c2	z
Čechtic	Čechtice	k1gFnPc2	Čechtice
<g/>
,	,	kIx,	,
Protivínskými	protivínský	k2eAgInPc7d1	protivínský
z	z	k7c2	z
Pohnání	pohnání	k1gNnPc2	pohnání
<g/>
,	,	kIx,	,
z	z	k7c2	z
Košíně	Košín	k1gInSc6	Košín
<g/>
,	,	kIx,	,
Beřkovskými	Beřkovský	k2eAgInPc7d1	Beřkovský
ze	z	k7c2	z
Šebířova	Šebířův	k2eAgNnSc2d1	Šebířův
<g/>
,	,	kIx,	,
ze	z	k7c2	z
Chmelného	Chmelný	k2eAgNnSc2d1	Chmelný
<g/>
,	,	kIx,	,
Sedleckými	sedlecký	k2eAgMnPc7d1	sedlecký
od	od	k7c2	od
Dubu	dub	k1gInSc2	dub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc4	jméno
měli	mít	k5eAaImAgMnP	mít
po	po	k7c4	po
Lomné	lomný	k2eAgFnPc4d1	Lomná
a	a	k8xC	a
Bedřichovicích	Bedřichovice	k1gFnPc6	Bedřichovice
<g/>
.	.	kIx.	.
</s>
<s>
Bohuněk	Bohuněk	k1gInSc1	Bohuněk
z	z	k7c2	z
Lomné	lomný	k2eAgFnSc2d1	Lomná
(	(	kIx(	(
<g/>
1509	[number]	k4	1509
<g/>
–	–	k?	–
<g/>
1531	[number]	k4	1531
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Bedřichovicích	Bedřichovice	k1gFnPc6	Bedřichovice
měl	mít	k5eAaImAgMnS	mít
syny	syn	k1gMnPc7	syn
Pavla	Pavel	k1gMnSc2	Pavel
(	(	kIx(	(
<g/>
1509	[number]	k4	1509
<g/>
–	–	k?	–
<g/>
1559	[number]	k4	1559
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
(	(	kIx(	(
<g/>
1543	[number]	k4	1543
<g/>
–	–	k?	–
<g/>
1556	[number]	k4	1556
<g/>
)	)	kIx)	)
a	a	k8xC	a
Petra	Petra	k1gFnSc1	Petra
(	(	kIx(	(
<g/>
1543	[number]	k4	1543
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
si	se	k3xPyFc3	se
vložili	vložit	k5eAaPmAgMnP	vložit
v	v	k7c4	v
zemské	zemský	k2eAgFnPc4d1	zemská
desky	deska	k1gFnPc4	deska
Bedřichovice	Bedřichovice	k1gFnSc2	Bedřichovice
r.	r.	kA	r.
1543	[number]	k4	1543
se	se	k3xPyFc4	se
strýci	strýc	k1gMnSc6	strýc
svými	svůj	k3xOyFgMnPc7	svůj
Adamem	Adam	k1gMnSc7	Adam
<g/>
,	,	kIx,	,
Petrem	Petr	k1gMnSc7	Petr
<g/>
,	,	kIx,	,
Bohuňkem	Bohuňk	k1gInSc7	Bohuňk
a	a	k8xC	a
Václavem	Václav	k1gMnSc7	Václav
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
až	až	k6eAd1	až
do	do	k7c2	do
r.	r.	kA	r.
1565	[number]	k4	1565
na	na	k7c6	na
Lhotce	Lhotka	k1gFnSc6	Lhotka
u	u	k7c2	u
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pavlův	Pavlův	k2eAgMnSc1d1	Pavlův
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
díky	díky	k7c3	díky
sňatku	sňatek	k1gInSc3	sňatek
s	s	k7c7	s
Kateřinou	Kateřina	k1gFnSc7	Kateřina
z	z	k7c2	z
Nasevrk	Nasevrk	k1gInSc1	Nasevrk
(	(	kIx(	(
<g/>
1559	[number]	k4	1559
<g/>
)	)	kIx)	)
držel	držet	k5eAaImAgMnS	držet
Bolechovice	Bolechovice	k1gFnPc4	Bolechovice
a	a	k8xC	a
Bedřichovice	Bedřichovice	k1gFnPc4	Bedřichovice
měl	mít	k5eAaImAgMnS	mít
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
Václav	Václav	k1gMnSc1	Václav
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
a	a	k8xC	a
Vilém	Vilém	k1gMnSc1	Vilém
se	se	k3xPyFc4	se
asi	asi	k9	asi
roku	rok	k1gInSc2	rok
1574	[number]	k4	1574
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
pak	pak	k6eAd1	pak
zapsal	zapsat	k5eAaPmAgMnS	zapsat
Bolechovice	Bolechovice	k1gFnPc4	Bolechovice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
1586	[number]	k4	1586
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sestrám	sestra	k1gFnPc3	sestra
Marjáně	Marjána	k1gFnSc6	Marjána
a	a	k8xC	a
Aléně	Aléna	k1gFnSc6	Aléna
<g/>
.	.	kIx.	.
</s>
<s>
Marjána	Marjána	k1gFnSc1	Marjána
vdaná	vdaný	k2eAgFnSc1d1	vdaná
Vesecká	Vesecká	k1gFnSc1	Vesecká
přežila	přežít	k5eAaPmAgFnS	přežít
sestru	sestra	k1gFnSc4	sestra
a	a	k8xC	a
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1622	[number]	k4	1622
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc4	její
statek	statek	k1gInSc4	statek
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
její	její	k3xOp3gFnSc3	její
dceři	dcera	k1gFnSc3	dcera
Anně	Anna	k1gFnSc3	Anna
Kateřině	Kateřina	k1gFnSc3	Kateřina
Měděncové	Měděncový	k2eAgFnSc2d1	Měděncový
z	z	k7c2	z
Vesce	Vesec	k1gInSc2	Vesec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
mladší	mladý	k2eAgMnSc1d2	mladší
(	(	kIx(	(
<g/>
snad	snad	k9	snad
tentýž	týž	k3xTgMnSc1	týž
<g/>
,	,	kIx,	,
co	co	k9	co
nejmladší	mladý	k2eAgMnPc1d3	nejmladší
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
)	)	kIx)	)
koupil	koupit	k5eAaPmAgMnS	koupit
r.	r.	kA	r.
1578	[number]	k4	1578
Zátvor	zátvor	k1gInSc4	zátvor
a	a	k8xC	a
prodal	prodat	k5eAaPmAgMnS	prodat
ho	on	k3xPp3gNnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1584	[number]	k4	1584
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
seděl	sedět	k5eAaImAgMnS	sedět
do	do	k7c2	do
r.	r.	kA	r.
1593	[number]	k4	1593
na	na	k7c6	na
Hrnčířích	Hrnčíř	k1gMnPc6	Hrnčíř
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svoji	svůj	k3xOyFgFnSc4	svůj
manželkou	manželka	k1gFnSc7	manželka
Julianou	Julianá	k1gFnSc4	Julianá
z	z	k7c2	z
Tamfeldu	Tamfeld	k1gInSc2	Tamfeld
měl	mít	k5eAaImAgMnS	mít
děti	dítě	k1gFnPc4	dítě
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
<g/>
,	,	kIx,	,
Elišku	Eliška	k1gFnSc4	Eliška
a	a	k8xC	a
Lidmilu	Lidmila	k1gFnSc4	Lidmila
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejichž	jejichž	k3xOyRp3gInPc6	jejichž
osudech	osud	k1gInPc6	osud
nic	nic	k6eAd1	nic
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
tehdy	tehdy	k6eAd1	tehdy
zchudla	zchudnout	k5eAaPmAgFnS	zchudnout
a	a	k8xC	a
ztratila	ztratit	k5eAaPmAgFnS	ztratit
se	se	k3xPyFc4	se
z	z	k7c2	z
lidské	lidský	k2eAgFnSc2d1	lidská
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgInPc2d1	český
<g/>
,	,	kIx,	,
moravských	moravský	k2eAgInPc2d1	moravský
a	a	k8xC	a
slezských	slezský	k2eAgInPc2d1	slezský
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
rodů	rod	k1gInPc2	rod
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
