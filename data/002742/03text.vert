<s>
Velká	velký	k2eAgFnSc1d1	velká
Javořina	Javořina	k1gFnSc1	Javořina
(	(	kIx(	(
<g/>
místně	místně	k6eAd1	místně
Velká	velký	k2eAgFnSc1d1	velká
Javorina	Javorina	k1gFnSc1	Javorina
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
Veľká	Veľký	k2eAgFnSc1d1	Veľká
Javorina	Javorina	k1gFnSc1	Javorina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
Bílých	bílý	k2eAgInPc6d1	bílý
Karpatech	Karpaty	k1gInPc6	Karpaty
na	na	k7c6	na
moravsko-slovenském	moravskolovenský	k2eAgNnSc6d1	moravsko-slovenské
pomezí	pomezí	k1gNnSc6	pomezí
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
970	[number]	k4	970
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
tohoto	tento	k3xDgNnSc2	tento
pohoří	pohoří	k1gNnSc2	pohoří
a	a	k8xC	a
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
okresu	okres	k1gInSc2	okres
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
hory	hora	k1gFnSc2	hora
prochází	procházet	k5eAaImIp3nS	procházet
hranice	hranice	k1gFnSc1	hranice
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	s	k7c7	s
Slovenskem	Slovensko	k1gNnSc7	Slovensko
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
televizní	televizní	k2eAgMnSc1d1	televizní
a	a	k8xC	a
rádiový	rádiový	k2eAgInSc1d1	rádiový
vysílač	vysílač	k1gInSc1	vysílač
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholová	vrcholový	k2eAgFnSc1d1	vrcholová
část	část	k1gFnSc1	část
Javorina	Javorina	k1gFnSc1	Javorina
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Javorina	Javorina	k1gFnSc1	Javorina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
pralesovitý	pralesovitý	k2eAgInSc4d1	pralesovitý
porost	porost	k1gInSc4	porost
na	na	k7c6	na
vápnitém	vápnitý	k2eAgInSc6d1	vápnitý
flyši	flyš	k1gInSc6	flyš
severního	severní	k2eAgInSc2d1	severní
svahu	svah	k1gInSc2	svah
a	a	k8xC	a
společenstvo	společenstvo	k1gNnSc1	společenstvo
horské	horský	k2eAgFnSc2d1	horská
louky	louka	k1gFnSc2	louka
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
a	a	k8xC	a
severním	severní	k2eAgInSc6d1	severní
svahu	svah	k1gInSc6	svah
Velké	velký	k2eAgFnSc2d1	velká
Javořiny	Javořina	k1gFnSc2	Javořina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
165	[number]	k4	165
hektarů	hektar	k1gInPc2	hektar
vyloučen	vyloučen	k2eAgInSc1d1	vyloučen
jakýkoli	jakýkoli	k3yIgInSc1	jakýkoli
zásah	zásah	k1gInSc1	zásah
do	do	k7c2	do
lesního	lesní	k2eAgInSc2d1	lesní
porostu	porost	k1gInSc2	porost
-	-	kIx~	-
na	na	k7c6	na
Javořině	Javořina	k1gFnSc6	Javořina
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
prales	prales	k1gInSc1	prales
<g/>
.	.	kIx.	.
</s>
<s>
Vysílač	vysílač	k1gInSc1	vysílač
Velká	velký	k2eAgFnSc1d1	velká
Javořina	Javořina	k1gFnSc1	Javořina
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
135	[number]	k4	135
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
anténních	anténní	k2eAgInPc2d1	anténní
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
procházela	procházet	k5eAaImAgFnS	procházet
hranice	hranice	k1gFnSc1	hranice
jím	on	k3xPp3gInSc7	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1997	[number]	k4	1997
leží	ležet	k5eAaImIp3nS	ležet
jeho	jeho	k3xOp3gInPc4	jeho
pozemky	pozemek	k1gInPc4	pozemek
čistě	čistě	k6eAd1	čistě
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
km	km	kA	km
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vrch	vrch	k1gInSc4	vrch
Jelenec	Jelenec	k1gInSc4	Jelenec
dříve	dříve	k6eAd2	dříve
využívaný	využívaný	k2eAgInSc4d1	využívaný
jako	jako	k8xC	jako
vojenský	vojenský	k2eAgInSc4d1	vojenský
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
vrchu	vrch	k1gInSc6	vrch
zbyla	zbýt	k5eAaPmAgFnS	zbýt
po	po	k7c6	po
armádě	armáda	k1gFnSc6	armáda
chátrající	chátrající	k2eAgFnSc6d1	chátrající
48	[number]	k4	48
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
telekomunikační	telekomunikační	k2eAgFnSc1d1	telekomunikační
věž	věž	k1gFnSc1	věž
(	(	kIx(	(
<g/>
6	[number]	k4	6
stupňů	stupeň	k1gInPc2	stupeň
po	po	k7c6	po
8	[number]	k4	8
metrech	metr	k1gInPc6	metr
<g/>
,	,	kIx,	,
šestiboká	šestiboký	k2eAgNnPc1d1	šestiboké
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Javořině	Javořina	k1gFnSc6	Javořina
konají	konat	k5eAaImIp3nP	konat
letní	letní	k2eAgFnPc1d1	letní
Slavnosti	slavnost	k1gFnPc1	slavnost
bratrství	bratrství	k1gNnSc2	bratrství
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Moravanů	Moravan	k1gMnPc2	Moravan
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
podobných	podobný	k2eAgFnPc2d1	podobná
akcí	akce	k1gFnPc2	akce
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
už	už	k6eAd1	už
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tudy	tudy	k6eAd1	tudy
ještě	ještě	k6eAd1	ještě
procházela	procházet	k5eAaImAgFnS	procházet
hranice	hranice	k1gFnPc4	hranice
dvou	dva	k4xCgInPc2	dva
částí	část	k1gFnPc2	část
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
podporu	podpora	k1gFnSc4	podpora
československé	československý	k2eAgFnSc2d1	Československá
vzájemnosti	vzájemnost	k1gFnSc2	vzájemnost
usiloval	usilovat	k5eAaImAgMnS	usilovat
i	i	k9	i
kněz	kněz	k1gMnSc1	kněz
Antonín	Antonín	k1gMnSc1	Antonín
Šuránek	Šuránek	k1gMnSc1	Šuránek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Javořiny	Javořina	k1gFnSc2	Javořina
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
zbudovat	zbudovat	k5eAaPmF	zbudovat
kapli	kaple	k1gFnSc4	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
Matky	matka	k1gFnSc2	matka
jednoty	jednota	k1gFnSc2	jednota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
shromáždění	shromáždění	k1gNnSc2	shromáždění
konala	konat	k5eAaImAgFnS	konat
na	na	k7c6	na
Javořině	Javořina	k1gFnSc6	Javořina
mše	mše	k1gFnSc2	mše
svatá	svatat	k5eAaImIp3nS	svatat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
zbudována	zbudován	k2eAgFnSc1d1	zbudována
boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
Marie	Maria	k1gFnSc2	Maria
Málkové	Málková	k1gFnSc2	Málková
z	z	k7c2	z
Nivnice	Nivnice	k1gFnPc1	Nivnice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
shromáždění	shromáždění	k1gNnSc2	shromáždění
zabil	zabít	k5eAaPmAgMnS	zabít
blesk	blesk	k1gInSc4	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
odpor	odpor	k1gInSc4	odpor
nastupujícího	nastupující	k2eAgInSc2d1	nastupující
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
již	již	k6eAd1	již
vybudovat	vybudovat	k5eAaPmF	vybudovat
nestihlo	stihnout	k5eNaPmAgNnS	stihnout
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Javořina	Javořina	k1gFnSc1	Javořina
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
turistickým	turistický	k2eAgInSc7d1	turistický
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Odlesněný	odlesněný	k2eAgInSc1d1	odlesněný
hřeben	hřeben	k1gInSc1	hřeben
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vynikající	vynikající	k2eAgInSc1d1	vynikající
rozhled	rozhled	k1gInSc1	rozhled
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c6	na
V	V	kA	V
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
viditelný	viditelný	k2eAgInSc1d1	viditelný
blízký	blízký	k2eAgInSc1d1	blízký
Povážský	povážský	k2eAgInSc1d1	povážský
Inovec	Inovec	k1gInSc1	Inovec
a	a	k8xC	a
hroty	hrot	k1gInPc7	hrot
Strážovských	Strážovský	k2eAgInPc2d1	Strážovský
vrchů	vrch	k1gInPc2	vrch
včetně	včetně	k7c2	včetně
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
Strážova	Strážův	k2eAgInSc2d1	Strážův
(	(	kIx(	(
<g/>
1214	[number]	k4	1214
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c7	za
nimi	on	k3xPp3gInPc7	on
lze	lze	k6eAd1	lze
rozeznat	rozeznat	k5eAaPmF	rozeznat
pohoří	pohoří	k1gNnSc3	pohoří
Žiar	Žiar	k1gInSc1	Žiar
a	a	k8xC	a
Vtáčnik	Vtáčnik	k1gInSc1	Vtáčnik
<g/>
,	,	kIx,	,
vrcholy	vrchol	k1gInPc1	vrchol
Rakytova	Rakytův	k2eAgNnSc2d1	Rakytovo
a	a	k8xC	a
Ploské	ploský	k2eAgFnPc1d1	Ploská
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Fatře	Fatra	k1gFnSc6	Fatra
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
hřebene	hřeben	k1gInSc2	hřeben
Nízkých	nízký	k2eAgFnPc2d1	nízká
Tater	Tatra	k1gFnPc2	Tatra
a	a	k8xC	a
pásmo	pásmo	k1gNnSc1	pásmo
Malé	Malé	k2eAgFnSc2d1	Malé
Fatry	Fatra	k1gFnSc2	Fatra
od	od	k7c2	od
výrazné	výrazný	k2eAgFnSc2d1	výrazná
siluety	silueta	k1gFnSc2	silueta
Kľaku	Kľak	k1gInSc2	Kľak
(	(	kIx(	(
<g/>
1352	[number]	k4	1352
m	m	kA	m
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
vlevo	vlevo	k6eAd1	vlevo
vystrčený	vystrčený	k2eAgInSc4d1	vystrčený
masiv	masiv	k1gInSc4	masiv
Velkého	velký	k2eAgInSc2d1	velký
Rozsutce	Rozsutka	k1gFnSc6	Rozsutka
<g/>
.	.	kIx.	.
</s>
<s>
Severnímu	severní	k2eAgInSc3d1	severní
obzoru	obzor	k1gInSc3	obzor
v	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
dominují	dominovat	k5eAaImIp3nP	dominovat
Velký	velký	k2eAgInSc4d1	velký
Lopeník	Lopeník	k1gInSc4	Lopeník
(	(	kIx(	(
<g/>
912	[number]	k4	912
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Chmeľová	Chmeľová	k1gFnSc1	Chmeľová
<g/>
,	,	kIx,	,
za	za	k7c7	za
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
uspořádanými	uspořádaný	k2eAgInPc7d1	uspořádaný
hřebeny	hřeben	k1gInPc7	hřeben
Bílých	bílý	k2eAgInPc2d1	bílý
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
,	,	kIx,	,
Javorníků	Javorník	k1gInPc2	Javorník
<g/>
,	,	kIx,	,
Vizovických	vizovický	k2eAgInPc2d1	vizovický
a	a	k8xC	a
Hostýnských	hostýnský	k2eAgInPc2d1	hostýnský
vrchů	vrch	k1gInPc2	vrch
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
nejvyšší	vysoký	k2eAgInPc1d3	Nejvyšší
vrcholy	vrchol	k1gInPc1	vrchol
Moravskoslezských	moravskoslezský	k2eAgFnPc2d1	Moravskoslezská
Beskyd	Beskydy	k1gFnPc2	Beskydy
<g/>
:	:	kIx,	:
Radhošť	Radhošť	k1gFnSc1	Radhošť
<g/>
,	,	kIx,	,
Kněhyně	kněhyně	k1gFnSc1	kněhyně
<g/>
,	,	kIx,	,
Smrk	smrk	k1gInSc1	smrk
<g/>
,	,	kIx,	,
Lysá	Lysá	k1gFnSc1	Lysá
hora	hora	k1gFnSc1	hora
a	a	k8xC	a
Travný	travný	k2eAgInSc1d1	travný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nebývá	bývat	k5eNaImIp3nS	bývat
inverze	inverze	k1gFnSc1	inverze
<g/>
,	,	kIx,	,
pozorovatelné	pozorovatelný	k2eAgFnPc1d1	pozorovatelná
Pálava	Pálava	k1gFnSc1	Pálava
<g/>
,	,	kIx,	,
Chřiby	Chřiby	k1gInPc1	Chřiby
a	a	k8xC	a
Drahanská	Drahanský	k2eAgFnSc1d1	Drahanská
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnPc1d1	tvořící
se	se	k3xPyFc4	se
oblaka	oblaka	k1gNnPc1	oblaka
udávají	udávat	k5eAaImIp3nP	udávat
polohu	poloha	k1gFnSc4	poloha
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Dukovany	Dukovany	k1gInPc1	Dukovany
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
Malé	Malé	k2eAgInPc1d1	Malé
Karpaty	Karpaty	k1gInPc1	Karpaty
a	a	k8xC	a
na	na	k7c4	na
JV	JV	kA	JV
Tribeč	Tribeč	k1gMnSc1	Tribeč
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zvláště	zvláště	k6eAd1	zvláště
výborné	výborný	k2eAgFnSc6d1	výborná
dohlednosti	dohlednost	k1gFnSc6	dohlednost
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
východě	východ	k1gInSc6	východ
štíty	štít	k1gInPc4	štít
Západních	západní	k2eAgFnPc2d1	západní
Tater	Tatra	k1gFnPc2	Tatra
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
nad	nad	k7c7	nad
náhorní	náhorní	k2eAgFnSc7d1	náhorní
rovinou	rovina	k1gFnSc7	rovina
Oderských	oderský	k2eAgInPc2d1	oderský
vrchů	vrch	k1gInPc2	vrch
i	i	k9	i
140	[number]	k4	140
km	km	kA	km
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
klenba	klenba	k1gFnSc1	klenba
Hrubého	Hrubého	k2eAgInSc2d1	Hrubého
Jeseníku	Jeseník	k1gInSc2	Jeseník
s	s	k7c7	s
Pradědem	praděd	k1gMnSc7	praděd
a	a	k8xC	a
na	na	k7c4	na
JZ	JZ	kA	JZ
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
asi	asi	k9	asi
180	[number]	k4	180
km	km	kA	km
Alpy	Alpy	k1gFnPc4	Alpy
s	s	k7c7	s
masivem	masiv	k1gInSc7	masiv
Schneebergu	Schneeberg	k1gInSc2	Schneeberg
(	(	kIx(	(
<g/>
2075	[number]	k4	2075
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
masivy	masiv	k1gInPc7	masiv
alpského	alpský	k2eAgNnSc2d1	alpské
předhůří	předhůří	k1gNnSc2	předhůří
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Velké	velký	k2eAgFnSc2d1	velká
Javořiny	Javořina	k1gFnSc2	Javořina
vede	vést	k5eAaImIp3nS	vést
vícero	vícero	k1gNnSc1	vícero
turistických	turistický	k2eAgFnPc2d1	turistická
tras	trasa	k1gFnPc2	trasa
z	z	k7c2	z
moravské	moravský	k2eAgFnSc2d1	Moravská
i	i	k8xC	i
slovenské	slovenský	k2eAgFnSc2d1	slovenská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
500	[number]	k4	500
m	m	kA	m
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
<g/>
,	,	kIx,	,
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
turistická	turistický	k2eAgFnSc1d1	turistická
Holubyho	Holuby	k1gMnSc4	Holuby
chata	chata	k1gFnSc1	chata
<g/>
.	.	kIx.	.
</s>
<s>
Veľká	Veľký	k2eAgFnSc1d1	Veľká
Javorina	Javorina	k1gFnSc1	Javorina
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Bílé	bílý	k2eAgInPc1d1	bílý
Karpaty	Karpaty	k1gInPc1	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Nové	Nové	k2eAgNnSc1d1	Nové
Mesto	Mesto	k1gNnSc1	Mesto
nad	nad	k7c4	nad
Váhom	Váhom	k1gInSc4	Váhom
v	v	k7c6	v
Trenčínském	trenčínský	k2eAgInSc6d1	trenčínský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
či	či	k8xC	či
novelizováno	novelizovat	k5eAaBmNgNnS	novelizovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
na	na	k7c6	na
rozloze	rozloha	k1gFnSc6	rozloha
82,980	[number]	k4	82,980
<g/>
0	[number]	k4	0
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Ochranné	ochranný	k2eAgNnSc1d1	ochranné
pásmo	pásmo	k1gNnSc1	pásmo
nebylo	být	k5eNaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Velká	velký	k2eAgFnSc1d1	velká
Javořina	Javořina	k1gFnSc1	Javořina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Javořina	Javořina	k1gFnSc1	Javořina
Velká	velká	k1gFnSc1	velká
Javořina	Javořina	k1gFnSc1	Javořina
-	-	kIx~	-
místo	místo	k7c2	místo
setkání	setkání	k1gNnSc2	setkání
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
Hey	Hey	k1gFnPc2	Hey
<g/>
,	,	kIx,	,
what	whata	k1gFnPc2	whata
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
that	thata	k1gFnPc2	thata
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
Velká	velký	k2eAgFnSc1d1	velká
Javořina	Javořina	k1gFnSc1	Javořina
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Veľká	Veľký	k2eAgFnSc1d1	Veľká
Javorina	Javorina	k1gFnSc1	Javorina
<g/>
,	,	kIx,	,
Štátny	Štátno	k1gNnPc7	Štátno
zoznam	zoznam	k6eAd1	zoznam
osobitne	osobitnout	k5eAaPmIp3nS	osobitnout
chránených	chránený	k2eAgMnPc2d1	chránený
častí	častit	k5eAaImIp3nS	častit
prírody	príroda	k1gFnPc4	príroda
SR	SR	kA	SR
Chránené	Chránená	k1gFnSc2	Chránená
územia	územia	k1gFnSc1	územia
<g/>
,	,	kIx,	,
Štátna	Štáten	k2eAgFnSc1d1	Štátna
ochrana	ochrana	k1gFnSc1	ochrana
prírody	príroda	k1gFnSc2	príroda
Slovenskej	Slovenskej	k?	Slovenskej
republiky	republika	k1gFnSc2	republika
</s>
