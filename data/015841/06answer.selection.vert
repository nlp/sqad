<s>
Třída	třída	k1gFnSc1
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
programu	program	k1gInSc2
DD	DD	kA
21	#num#	k4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
získat	získat	k5eAaPmF
32	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
by	by	kYmCp3nP
ve	v	k7c6
službě	služba	k1gFnSc6
nahradily	nahradit	k5eAaPmAgFnP
fregaty	fregata	k1gFnPc1
třídy	třída	k1gFnSc2
Oliver	Olivra	k1gFnPc2
Hazard	hazard	k2eAgFnSc2d1
Perry	Perra	k1gFnSc2
a	a	k8xC
torpédoborce	torpédoborec	k1gInSc2
třídy	třída	k1gFnSc2
Spruance	Spruance	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
byla	být	k5eAaImAgFnS
koncepce	koncepce	k1gFnSc1
plánované	plánovaný	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
přepracována	přepracovat	k5eAaPmNgFnS
a	a	k8xC
namísto	namísto	k7c2
DD	DD	kA
21	#num#	k4
byla	být	k5eAaImAgFnS
poté	poté	k6eAd1
označována	označován	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
DD	DD	kA
<g/>
(	(	kIx(
<g/>
X	X	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>