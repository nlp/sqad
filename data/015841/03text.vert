<s>
Třída	třída	k1gFnSc1
Zumwalt	Zumwalta	k1gFnPc2
</s>
<s>
Třída	třída	k1gFnSc1
Zumwalt	Zumwalt	k1gMnSc1
USS	USS	kA
Zumwalt	Zumwalt	k1gMnSc1
(	(	kIx(
<g/>
DDG-	DDG-	k1gFnSc1
<g/>
1000	#num#	k4
<g/>
)	)	kIx)
při	pře	k1gFnSc3
první	první	k4xOgFnSc2
plavběObecné	plavběObecný	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Uživatelé	uživatel	k1gMnPc1
</s>
<s>
US	US	kA
Navy	Navy	k?
Typ	typ	k1gInSc1
</s>
<s>
torpédoborec	torpédoborec	k1gInSc1
Lodě	loď	k1gFnSc2
</s>
<s>
3	#num#	k4
Zahájení	zahájení	k1gNnSc1
stavby	stavba	k1gFnPc1
</s>
<s>
2008	#num#	k4
Uvedení	uvedení	k1gNnPc1
do	do	k7c2
služby	služba	k1gFnSc2
</s>
<s>
2016	#num#	k4
Osud	osud	k1gInSc1
</s>
<s>
aktivní	aktivní	k2eAgMnSc1d1
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
třída	třída	k1gFnSc1
Arleigh	Arleigh	k1gInSc1
Burke	Burke	k1gInSc1
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Výtlak	výtlak	k1gInSc4
</s>
<s>
15	#num#	k4
900	#num#	k4
t	t	k?
Délka	délka	k1gFnSc1
</s>
<s>
183	#num#	k4
m	m	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
24,5	24,5	k4
m	m	kA
Ponor	ponor	k1gInSc1
</s>
<s>
8,41	8,41	k4
m	m	kA
Pohon	pohon	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
plynová	plynový	k2eAgFnSc1d1
turbína	turbína	k1gFnSc1
<g/>
47	#num#	k4
500	#num#	k4
koní	kůň	k1gMnPc2
Rychlost	rychlost	k1gFnSc1
</s>
<s>
30	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
56	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
Posádka	posádka	k1gFnSc1
</s>
<s>
142	#num#	k4
Výzbroj	výzbroj	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
155	#num#	k4
mm	mm	kA
kanón	kanón	k1gInSc4
<g/>
2	#num#	k4
<g/>
×	×	k?
30	#num#	k4
mm	mm	kA
Mk	Mk	k1gFnSc2
46	#num#	k4
<g/>
Mk	Mk	k1gFnSc1
574	#num#	k4
<g/>
×	×	k?
RIM-162	RIM-162	k1gFnSc6
ESSM	ESSM	kA
<g/>
1	#num#	k4
<g/>
×	×	k?
Tomahawk	tomahawk	k1gInSc4
nebo	nebo	k8xC
1	#num#	k4
<g/>
×	×	k?
ASROC	ASROC	kA
Letadla	letadlo	k1gNnPc1
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
SH-60	SH-60	k1gFnSc1
Seahawk	Seahawk	k1gMnSc1
Radar	radar	k1gInSc1
</s>
<s>
SPY-3	SPY-3	k4
</s>
<s>
Třída	třída	k1gFnSc1
Zumwalt	Zumwalt	k1gInSc1
(	(	kIx(
<g/>
DDG-	DDG-	k1gFnSc1
<g/>
1000	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
třída	třída	k1gFnSc1
raketových	raketový	k2eAgFnPc2d1
stealth	stealth	k2
torpédoborců	torpédoborec	k1gInPc2
Námořnictva	námořnictvo	k1gNnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
víceúčelové	víceúčelový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
projektované	projektovaný	k2eAgFnPc1d1
s	s	k7c7
důrazem	důraz	k1gInSc7
na	na	k7c4
schopnost	schopnost	k1gFnSc4
provádění	provádění	k1gNnSc2
protizemních	protizemní	k2eAgInPc2d1
úderů	úder	k1gInPc2
a	a	k8xC
pobřežních	pobřežní	k2eAgFnPc2d1
operací	operace	k1gFnPc2
(	(	kIx(
<g/>
littorial	littorial	k1gInSc1
warfare	warfar	k1gMnSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Námořnictvo	námořnictvo	k1gNnSc1
prochází	procházet	k5eAaImIp3nP
řadou	řada	k1gFnSc7
modernizací	modernizace	k1gFnSc7
právě	právě	k6eAd1
s	s	k7c7
cílem	cíl	k1gInSc7
zlepšit	zlepšit	k5eAaPmF
své	svůj	k3xOyFgFnPc4
schopnosti	schopnost	k1gFnPc4
pobřežního	pobřežní	k2eAgInSc2d1
boje	boj	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
kromě	kromě	k7c2
těchto	tento	k3xDgInPc2
torpédoborců	torpédoborec	k1gInPc2
plánovalo	plánovat	k5eAaImAgNnS
získat	získat	k5eAaPmF
též	též	k9
nové	nový	k2eAgInPc4d1
křižníky	křižník	k1gInPc4
(	(	kIx(
<g/>
program	program	k1gInSc4
CG	cg	kA
<g/>
(	(	kIx(
<g/>
X	X	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
zrušen	zrušen	k2eAgInSc1d1
roku	rok	k1gInSc2
2010	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
fregaty	fregata	k1gFnSc2
(	(	kIx(
<g/>
program	program	k1gInSc1
Littoral	Littoral	k1gMnSc1
Combat	Combat	k1gMnSc1
Ship	Ship	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Třída	třída	k1gFnSc1
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
programu	program	k1gInSc2
DD	DD	kA
21	#num#	k4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
získat	získat	k5eAaPmF
32	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
by	by	kYmCp3nP
ve	v	k7c6
službě	služba	k1gFnSc6
nahradily	nahradit	k5eAaPmAgFnP
fregaty	fregata	k1gFnPc1
třídy	třída	k1gFnSc2
Oliver	Olivra	k1gFnPc2
Hazard	hazard	k2eAgFnSc2d1
Perry	Perra	k1gFnSc2
a	a	k8xC
torpédoborce	torpédoborec	k1gInSc2
třídy	třída	k1gFnSc2
Spruance	Spruance	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
byla	být	k5eAaImAgFnS
koncepce	koncepce	k1gFnSc1
plánované	plánovaný	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
přepracována	přepracovat	k5eAaPmNgFnS
a	a	k8xC
namísto	namísto	k7c2
DD	DD	kA
21	#num#	k4
byla	být	k5eAaImAgFnS
poté	poté	k6eAd1
označována	označován	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
DD	DD	kA
<g/>
(	(	kIx(
<g/>
X	X	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Torpédoborce	torpédoborec	k1gInPc1
třídy	třída	k1gFnSc2
Zumwalt	Zumwalta	k1gFnPc2
jsou	být	k5eAaImIp3nP
největší	veliký	k2eAgNnPc1d3
plavidla	plavidlo	k1gNnPc1
své	svůj	k3xOyFgFnPc4
kategorie	kategorie	k1gFnPc4
v	v	k7c6
historii	historie	k1gFnSc6
amerického	americký	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
první	první	k4xOgFnPc1
americké	americký	k2eAgFnPc1d1
válečné	válečný	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
vybavené	vybavený	k2eAgFnSc2d1
tzv.	tzv.	kA
integrovaným	integrovaný	k2eAgInSc7d1
pohonným	pohonný	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stavba	stavba	k1gFnSc1
</s>
<s>
USS	USS	kA
Zumwalt	Zumwalt	k1gMnSc1
(	(	kIx(
<g/>
DDG-	DDG-	k1gFnSc1
<g/>
1000	#num#	k4
<g/>
)	)	kIx)
po	po	k7c6
spuštění	spuštění	k1gNnSc6
na	na	k7c4
vodu	voda	k1gFnSc4
</s>
<s>
Původně	původně	k6eAd1
plánovaná	plánovaný	k2eAgFnSc1d1
série	série	k1gFnSc1
32	#num#	k4
torpédoborců	torpédoborec	k1gMnPc2
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
byla	být	k5eAaImAgNnP
z	z	k7c2
důvodu	důvod	k1gInSc2
nedostatku	nedostatek	k1gInSc2
financí	finance	k1gFnPc2
omezena	omezit	k5eAaPmNgFnS
na	na	k7c4
pouhé	pouhý	k2eAgFnPc4d1
tři	tři	k4xCgFnPc4
jednotky	jednotka	k1gFnPc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
byla	být	k5eAaImAgFnS
přiobjednána	přiobjednán	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
několika	několik	k4yIc2
starších	starý	k2eAgMnPc2d2
<g/>
,	,	kIx,
ale	ale	k8xC
výrazně	výrazně	k6eAd1
levnějších	levný	k2eAgInPc2d2
torpédoborců	torpédoborec	k1gInPc2
třídy	třída	k1gFnSc2
Arleigh	Arleigha	k1gFnPc2
Burke	Burk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
2008	#num#	k4
byl	být	k5eAaImAgInS
loděnici	loděnice	k1gFnSc4
Bath	Bath	k1gInSc1
Iron	iron	k1gInSc4
Works	Works	kA
zadán	zadán	k2eAgInSc4d1
kontrakt	kontrakt	k1gInSc4
na	na	k7c4
stavbu	stavba	k1gFnSc4
první	první	k4xOgFnSc2
jednotky	jednotka	k1gFnSc2
USS	USS	kA
Zumwalt	Zumwalt	k1gInSc1
a	a	k8xC
u	u	k7c2
loděnice	loděnice	k1gFnSc2
Northrop	Northrop	k1gInSc1
Grumman	Grumman	k1gMnSc1
Shipbuilding	Shipbuilding	k1gInSc1
byla	být	k5eAaImAgFnS
objednána	objednat	k5eAaPmNgFnS
rovněž	rovněž	k9
stavba	stavba	k1gFnSc1
druhé	druhý	k4xOgFnSc2
jednotky	jednotka	k1gFnSc2
USS	USS	kA
Michael	Michael	k1gMnSc1
Monsoor	Monsoor	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Konstrukční	konstrukční	k2eAgFnSc2d1
práce	práce	k1gFnSc2
na	na	k7c6
první	první	k4xOgFnSc6
jednotce	jednotka	k1gFnSc6
začaly	začít	k5eAaPmAgFnP
v	v	k7c6
únoru	únor	k1gInSc6
2009	#num#	k4
<g/>
,	,	kIx,
na	na	k7c4
druhé	druhý	k4xOgNnSc4
v	v	k7c6
březnu	březno	k1gNnSc6
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ceremoniál	ceremoniál	k1gInSc1
založení	založení	k1gNnSc2
kýlu	kýl	k1gInSc3
Zumwaltu	Zumwalt	k1gInSc2
proběhl	proběhnout	k5eAaPmAgInS
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Vstup	vstup	k1gInSc1
do	do	k7c2
služby	služba	k1gFnSc2
se	se	k3xPyFc4
u	u	k7c2
těchto	tento	k3xDgFnPc2
dvou	dva	k4xCgFnPc2
lodí	loď	k1gFnPc2
očekával	očekávat	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
2013	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
stavba	stavba	k1gFnSc1
však	však	k9
nabrala	nabrat	k5eAaPmAgFnS
zpoždění	zpoždění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
2015	#num#	k4
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
kvůli	kvůli	k7c3
problémům	problém	k1gInPc3
s	s	k7c7
instalací	instalace	k1gFnSc7
komplexní	komplexní	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
budou	být	k5eAaImBp3nP
první	první	k4xOgInSc4
dvě	dva	k4xCgFnPc4
jednotky	jednotka	k1gFnPc1
do	do	k7c2
služby	služba	k1gFnSc2
uvedeny	uveden	k2eAgInPc4d1
v	v	k7c6
listopadu	listopad	k1gInSc6
2015	#num#	k4
a	a	k8xC
na	na	k7c4
podzim	podzim	k1gInSc4
2016	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
avšak	avšak	k8xC
program	program	k1gInSc1
nabral	nabrat	k5eAaPmAgInS
další	další	k2eAgNnPc4d1
zpoždění	zpoždění	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prototypová	prototypový	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
Zumwalt	Zumwalta	k1gFnPc2
zahájila	zahájit	k5eAaPmAgFnS
zkoušky	zkouška	k1gFnPc4
až	až	k6eAd1
v	v	k7c6
prosinci	prosinec	k1gInSc6
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
služby	služba	k1gFnSc2
vstoupila	vstoupit	k5eAaPmAgFnS
v	v	k7c6
říjnu	říjen	k1gInSc6
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Stavba	stavba	k1gFnSc1
třetí	třetí	k4xOgFnSc2
jednotky	jednotka	k1gFnSc2
USS	USS	kA
Lyndon	Lyndon	k1gMnSc1
B.	B.	kA
Johnson	Johnson	k1gMnSc1
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2012	#num#	k4
v	v	k7c6
loděnicích	loděnice	k1gFnPc6
General	General	k1gFnSc2
Dynamics-Bath	Dynamics-Bath	k1gInSc1
Iron	iron	k1gInSc1
Works	Works	kA
a	a	k8xC
vstup	vstup	k1gInSc4
do	do	k7c2
služby	služba	k1gFnSc2
se	se	k3xPyFc4
předpokládá	předpokládat	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jednotky	jednotka	k1gFnPc1
třídy	třída	k1gFnSc2
Zumwalt	Zumwalta	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
JménoZahájení	JménoZahájení	k1gNnSc1
stavbyObjednánaZaložení	stavbyObjednánaZaložení	k1gNnSc2
kýluSpuštěnaVstup	kýluSpuštěnaVstup	k1gInSc1
do	do	k7c2
službyStatus	službyStatus	k1gInSc4
</s>
<s>
USS	USS	kA
Zumwalt	Zumwalt	k1gMnSc1
(	(	kIx(
<g/>
DDG-	DDG-	k1gFnSc1
<g/>
1000	#num#	k4
<g/>
)	)	kIx)
<g/>
2008200917	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
201128	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
aktivní	aktivní	k2eAgFnSc2d1
</s>
<s>
USS	USS	kA
Michael	Michael	k1gMnSc1
Monsoor	Monsoor	k1gMnSc1
(	(	kIx(
<g/>
DDG-	DDG-	k1gFnSc1
<g/>
1001	#num#	k4
<g/>
)	)	kIx)
<g/>
2008201023	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
201321	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
201626	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2019	#num#	k4
<g/>
aktivní	aktivní	k2eAgFnSc4d1
</s>
<s>
USS	USS	kA
Lyndon	Lyndon	k1gMnSc1
B.	B.	kA
Johnson	Johnson	k1gMnSc1
(	(	kIx(
<g/>
DDG-	DDG-	k1gFnSc1
<g/>
1002	#num#	k4
<g/>
)	)	kIx)
<g/>
2011201230	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
20179	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2018	#num#	k4
<g/>
ve	v	k7c6
stavbě	stavba	k1gFnSc6
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Tvary	tvar	k1gInPc1
budoucích	budoucí	k2eAgMnPc2d1
torpédoborců	torpédoborec	k1gMnPc2
byly	být	k5eAaImAgFnP
vyzkoušeny	vyzkoušet	k5eAaPmNgFnP
na	na	k7c6
technologickém	technologický	k2eAgMnSc6d1
demonstrátoru	demonstrátor	k1gMnSc6
Sea	Sea	k1gFnPc2
Jet	jet	k5eAaImF
</s>
<s>
Zkoušky	zkouška	k1gFnPc1
155	#num#	k4
<g/>
mm	mm	kA
kanónu	kanón	k1gInSc2
pro	pro	k7c4
třídu	třída	k1gFnSc4
Zumwalt	Zumwalta	k1gFnPc2
</s>
<s>
Celý	celý	k2eAgInSc1d1
trup	trup	k1gInSc1
je	být	k5eAaImIp3nS
tvarován	tvarovat	k5eAaImNgInS
pro	pro	k7c4
dosažení	dosažení	k1gNnSc4
co	co	k9
nejmenšího	malý	k2eAgInSc2d3
radarového	radarový	k2eAgInSc2d1
odrazu	odraz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypadá	vypadat	k5eAaPmIp3nS,k5eAaImIp3nS
proto	proto	k8xC
dosti	dosti	k6eAd1
nekonvenčně	konvenčně	k6eNd1
a	a	k8xC
svým	svůj	k3xOyFgInSc7
tvarem	tvar	k1gInSc7
připomíná	připomínat	k5eAaImIp3nS
ponorku	ponorka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gFnSc6
konstrukci	konstrukce	k1gFnSc6
jsou	být	k5eAaImIp3nP
široce	široko	k6eAd1
uplatněny	uplatněn	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
stealth	stealtha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
palubě	paluba	k1gFnSc6
bude	být	k5eAaImBp3nS
umístěno	umístit	k5eAaPmNgNnS
20	#num#	k4
vertikálních	vertikální	k2eAgFnPc2d1
vypouštěcích	vypouštěcí	k2eAgFnPc2d1
sil	síla	k1gFnPc2
nového	nový	k2eAgInSc2d1
typu	typ	k1gInSc2
MK	MK	kA
57	#num#	k4
<g/>
,	,	kIx,
označených	označený	k2eAgFnPc2d1
jako	jako	k9
Peripheral	Peripheral	k1gFnPc1
Vertical	Vertical	k1gFnPc2
Launch	Launcha	k1gFnPc2
System	Syst	k1gInSc7
(	(	kIx(
<g/>
PVLS	PVLS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sila	silo	k1gNnPc1
budou	být	k5eAaImBp3nP
rozmístěna	rozmístit	k5eAaPmNgNnP
po	po	k7c6
obvodu	obvod	k1gInSc6
lodi	loď	k1gFnSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nemohla	moct	k5eNaImAgFnS
být	být	k5eAaImF
vyřazena	vyřadit	k5eAaPmNgFnS
jediným	jediný	k2eAgInSc7d1
zásahem	zásah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
každém	každý	k3xTgInSc6
z	z	k7c2
nich	on	k3xPp3gInPc2
budou	být	k5eAaImBp3nP
4	#num#	k4
střely	střela	k1gFnPc4
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
tedy	tedy	k9
80	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nich	on	k3xPp3gInPc6
budou	být	k5eAaImBp3nP
umístěny	umístěn	k2eAgInPc1d1
střely	střel	k1gInPc1
s	s	k7c7
plochou	plochý	k2eAgFnSc7d1
dráhou	dráha	k1gFnSc7
letu	let	k1gInSc2
Tomahawk	tomahawk	k1gInSc1
<g/>
,	,	kIx,
raketová	raketový	k2eAgNnPc4d1
torpéda	torpédo	k1gNnPc4
ASROC	ASROC	kA
<g/>
,	,	kIx,
protiletadlové	protiletadlový	k2eAgInPc4d1
střely	střel	k1gInPc4
Standard	standard	k1gInSc1
SM-3	SM-3	k1gMnPc2
a	a	k8xC
RIM-162	RIM-162	k1gMnPc2
ESSM	ESSM	kA
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
tuto	tento	k3xDgFnSc4
třídu	třída	k1gFnSc4
byl	být	k5eAaImAgInS
vyvíjen	vyvíjet	k5eAaImNgInS
zcela	zcela	k6eAd1
nový	nový	k2eAgInSc1d1
zbraňový	zbraňový	k2eAgInSc1d1
systém	systém	k1gInSc1
označený	označený	k2eAgInSc1d1
jako	jako	k8xS,k8xC
Advanced	Advanced	k1gInSc1
Gun	Gun	k1gFnSc2
System	Syst	k1gInSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
základem	základ	k1gInSc7
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
dva	dva	k4xCgInPc1
automatické	automatický	k2eAgInPc1d1
155	#num#	k4
<g/>
mm	mm	kA
kanóny	kanón	k1gInPc4
s	s	k7c7
rychlostí	rychlost	k1gFnSc7
střelby	střelba	k1gFnSc2
12	#num#	k4
ran	rána	k1gFnPc2
za	za	k7c4
minutu	minuta	k1gFnSc4
a	a	k8xC
dostřelem	dostřel	k1gInSc7
100	#num#	k4
námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kanón	kanón	k1gInSc1
měl	mít	k5eAaImAgInS
mít	mít	k5eAaImF
zlepšenou	zlepšený	k2eAgFnSc4d1
schopnost	schopnost	k1gFnSc4
napadnání	napadnání	k1gNnSc2
pozemních	pozemní	k2eAgInPc2d1
cílů	cíl	k1gInPc2
a	a	k8xC
díky	díky	k7c3
novému	nový	k2eAgInSc3d1
typu	typ	k1gInSc3
"	"	kIx"
<g/>
inteligentní	inteligentní	k2eAgInSc1d1
<g/>
"	"	kIx"
munice	munice	k1gFnPc1
měl	mít	k5eAaImAgInS
představovat	představovat	k5eAaImF
alternativu	alternativa	k1gFnSc4
k	k	k7c3
raketovým	raketový	k2eAgFnPc3d1
zbraním	zbraň	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc1
munice	munice	k1gFnSc2
však	však	k9
byl	být	k5eAaImAgInS
ukončen	ukončit	k5eAaPmNgInS
neúspěchem	neúspěch	k1gInSc7
díky	díky	k7c3
extrémně	extrémně	k6eAd1
vysokým	vysoký	k2eAgFnPc3d1
cenám	cena	k1gFnPc3
střeliva	střelivo	k1gNnSc2
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
převyšovaly	převyšovat	k5eAaImAgFnP
cenu	cena	k1gFnSc4
soudobých	soudobý	k2eAgFnPc2d1
řízených	řízený	k2eAgFnPc2d1
střel	střela	k1gFnPc2
s	s	k7c7
plochou	plochý	k2eAgFnSc7d1
drahou	draha	k1gFnSc7
letu	let	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrušení	zrušení	k1gNnSc1
projektu	projekt	k1gInSc2
vývoje	vývoj	k1gInSc2
a	a	k8xC
zavádění	zavádění	k1gNnSc1
této	tento	k3xDgFnSc2
nové	nový	k2eAgFnSc2d1
munice	munice	k1gFnSc2
do	do	k7c2
služby	služba	k1gFnSc2
tak	tak	k8xS,k8xC
postavilo	postavit	k5eAaPmAgNnS
tyto	tento	k3xDgFnPc4
lodě	loď	k1gFnPc4
do	do	k7c2
unikátní	unikátní	k2eAgFnSc2d1
situace	situace	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
s	s	k7c7
nadsázkou	nadsázka	k1gFnSc7
popisována	popisovat	k5eAaImNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
první	první	k4xOgFnPc4
lodě	loď	k1gFnPc4
zavedené	zavedený	k2eAgFnPc4d1
do	do	k7c2
služby	služba	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
sice	sice	k8xC
mají	mít	k5eAaImIp3nP
kanony	kanon	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
nemají	mít	k5eNaImIp3nP
pro	pro	k7c4
ně	on	k3xPp3gNnSc4
střelivo	střelivo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Hlavňovou	hlavňový	k2eAgFnSc4d1
výzbroj	výzbroj	k1gFnSc4
budou	být	k5eAaImBp3nP
doplňovat	doplňovat	k5eAaImF
dva	dva	k4xCgInPc4
30	#num#	k4
<g/>
mm	mm	kA
kanóny	kanón	k1gInPc1
Bushmaster	Bushmastra	k1gFnPc2
II	II	kA
Mk	Mk	k1gFnSc1
46	#num#	k4
pro	pro	k7c4
blízkou	blízký	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
(	(	kIx(
<g/>
původně	původně	k6eAd1
lodě	loď	k1gFnPc1
měly	mít	k5eAaImAgFnP
nést	nést	k5eAaImF
dva	dva	k4xCgInPc4
57	#num#	k4
<g/>
mm	mm	kA
kanóny	kanón	k1gInPc1
Mk	Mk	k1gFnSc2
110	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
zádi	záď	k1gFnPc4
lodí	loď	k1gFnPc2
bude	být	k5eAaImBp3nS
umístěna	umístit	k5eAaPmNgFnS
plošina	plošina	k1gFnSc1
pro	pro	k7c4
operace	operace	k1gFnPc4
až	až	k9
dvou	dva	k4xCgInPc2
vrtulníků	vrtulník	k1gInPc2
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
budou	být	k5eAaImBp3nP
vybaveny	vybavit	k5eAaPmNgInP
hangárem	hangár	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohon	pohon	k1gInSc1
zajišťují	zajišťovat	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
plynové	plynový	k2eAgFnPc1d1
turbíny	turbína	k1gFnPc1
Rolls-Royce	Rolls-Royec	k1gInSc2
MT	MT	kA
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
rychlost	rychlost	k1gFnSc1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
30	#num#	k4
uzlů	uzel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
původní	původní	k2eAgFnSc1d1
koncepce	koncepce	k1gFnSc1
plavidla	plavidlo	k1gNnSc2
byla	být	k5eAaImAgFnS
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
založena	založit	k5eAaPmNgFnS
na	na	k7c6
předpokladu	předpoklad	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
významné	významný	k2eAgNnSc1d1
omezením	omezení	k1gNnSc7
osádky	osádka	k1gFnSc2
plavidla	plavidlo	k1gNnSc2
<g/>
,	,	kIx,
dovolené	dovolený	k2eAgFnPc1d1
použitím	použití	k1gNnSc7
nejmodernějších	moderní	k2eAgFnPc2d3
technologií	technologie	k1gFnPc2
dovolí	dovolit	k5eAaPmIp3nS
snížít	snížít	k5eAaPmF
celkové	celkový	k2eAgInPc4d1
provozní	provozní	k2eAgInPc4d1
náklady	náklad	k1gInPc4
během	během	k7c2
životního	životní	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
samotná	samotný	k2eAgFnSc1d1
zvýšená	zvýšený	k2eAgFnSc1d1
cena	cena	k1gFnSc1
plavidla	plavidlo	k1gNnSc2
nebude	být	k5eNaImBp3nS
představovat	představovat	k5eAaImF
výraznou	výrazný	k2eAgFnSc4d1
nevýhodu	nevýhoda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
koncepce	koncepce	k1gFnSc1
se	se	k3xPyFc4
ovšem	ovšem	k9
stala	stát	k5eAaPmAgFnS
předmětem	předmět	k1gInSc7
kritiky	kritika	k1gFnSc2
a	a	k8xC
značných	značný	k2eAgFnPc2d1
přetrvávajících	přetrvávající	k2eAgFnPc2d1
pochybností	pochybnost	k1gFnPc2
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
vývoje	vývoj	k1gInSc2
a	a	k8xC
zaváděním	zavádění	k1gNnSc7
prvních	první	k4xOgFnPc2
jednotek	jednotka	k1gFnPc2
do	do	k7c2
služby	služba	k1gFnSc2
projevovali	projevovat	k5eAaImAgMnP
různé	různý	k2eAgInPc4d1
operační	operační	k2eAgInPc4d1
a	a	k8xC
technické	technický	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
si	se	k3xPyFc3
na	na	k7c4
jednu	jeden	k4xCgFnSc4
stranu	strana	k1gFnSc4
vyžádali	vyžádat	k5eAaPmAgMnP
neplánované	plánovaný	k2eNgNnSc4d1
zvýšení	zvýšení	k1gNnSc4
osádky	osádka	k1gFnSc2
o	o	k7c4
téměř	téměř	k6eAd1
50	#num#	k4
<g/>
%	%	kIx~
a	a	k8xC
současně	současně	k6eAd1
ukazovali	ukazovat	k5eAaImAgMnP
operační	operační	k2eAgFnPc4d1
nevýhody	nevýhoda	k1gFnPc4
spojené	spojený	k2eAgFnPc4d1
s	s	k7c7
omezením	omezení	k1gNnSc7
osádky	osádka	k1gFnSc2
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
plavidla	plavidlo	k1gNnSc2
<g/>
,	,	kIx,
vůči	vůči	k7c3
předchozím	předchozí	k2eAgNnPc3d1
plavidlům	plavidlo	k1gNnPc3
zařazeným	zařazený	k2eAgFnPc3d1
do	do	k7c2
výzbroje	výzbroj	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
DDG	DDG	kA
1000	#num#	k4
Zumwalt	Zumwalt	k2eAgInSc1d1
Class	Class	k1gInSc1
-	-	kIx~
Multimission	Multimission	k1gInSc1
Destroyer	Destroyer	k1gMnSc1
<g/>
:	:	kIx,
Advanced	Advanced	k1gMnSc1
Technology	technolog	k1gMnPc7
Surface	Surface	k1gFnSc2
Combatants	Combatants	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naval-technology	Naval-technolog	k1gMnPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
U.	U.	kA
<g/>
S.	S.	kA
Navy	Navy	k?
Next	Next	k2eAgInSc4d1
Generation	Generation	k1gInSc4
Destroyer	Destroyer	k1gMnSc1
Zumwalt	Zumwalt	k1gMnSc1
(	(	kIx(
<g/>
DDG	DDG	kA
1000	#num#	k4
<g/>
)	)	kIx)
Headed	Headed	k1gMnSc1
to	ten	k3xDgNnSc4
Sea	Sea	k1gFnSc3
for	forum	k1gNnPc2
Trials	Trials	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navyrecognition	Navyrecognition	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Navy	Navy	k?
Awards	Awards	k1gInSc1
Contracts	Contractsa	k1gFnPc2
for	forum	k1gNnPc2
Zumwalt	Zumwalt	k2eAgInSc1d1
Class	Class	k1gInSc1
Destroyers	Destroyers	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navy	Navy	k?
<g/>
.	.	kIx.
<g/>
mil	míle	k1gFnPc2
<g/>
,	,	kIx,
2008-02-14	2008-02-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Keel	Keel	k1gInSc1
Laid	Laida	k1gFnPc2
for	forum	k1gNnPc2
First	First	k1gMnSc1
DDG	DDG	kA
1000	#num#	k4
Destroyer	Destroyero	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navsea	Navseum	k1gNnPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GROHMANN	GROHMANN	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zumwalt	Zumwalt	k1gInSc1
se	s	k7c7
zpožděním	zpoždění	k1gNnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armadninoviny	Armadninovina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2015-03-11	2015-03-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
US	US	kA
Navy	Navy	k?
Names	Names	k1gInSc1
Next	Nexta	k1gFnPc2
Zumwalt-Class	Zumwalt-Class	k1gInSc1
Destroyer	Destroyer	k1gMnSc1
USS	USS	kA
Lydon	Lydon	k1gMnSc1
B.	B.	kA
Johnson	Johnson	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
navaltoday	navaltoday	k1gInPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2012-04-17	2012-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GROHMANN	GROHMANN	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Torpédoborec	torpédoborec	k1gInSc1
Zumwalt	Zumwalt	k1gInSc4
byl	být	k5eAaImAgInS
spuštěn	spustit	k5eAaPmNgInS
na	na	k7c4
vodu	voda	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armadninoviny	Armadninovina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
U.	U.	kA
<g/>
S.	S.	kA
Navy	Navy	k?
Commissions	Commissions	k1gInSc1
its	its	k?
Newest	Newest	k1gInSc1
and	and	k?
Most	most	k1gInSc1
Technologically	Technologicalla	k1gFnSc2
Advanced	Advanced	k1gMnSc1
Surface	Surface	k1gFnSc2
Ship	Ship	k1gMnSc1
<g/>
,	,	kIx,
USS	USS	kA
Zumwalt	Zumwalt	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navyrecognition	Navyrecognition	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Třída	třída	k1gFnSc1
Zumwalt	Zumwalta	k1gFnPc2
přichází	přicházet	k5eAaImIp3nS
o	o	k7c4
57	#num#	k4
<g/>
mm	mm	kA
kanóny	kanón	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATM	ATM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Září	září	k1gNnSc1
2014	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
67	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
4823	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ZAJAC	ZAJAC	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Torpédoborce	torpédoborec	k1gInSc2
třídy	třída	k1gFnSc2
Zumwalt	Zumwalta	k1gFnPc2
<g/>
:	:	kIx,
Předražení	předražený	k2eAgMnPc1d1
futurističtí	futuristický	k2eAgMnPc1d1
válečníci	válečník	k1gMnPc1
US	US	kA
Navy	Navy	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATM	ATM	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
48	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
60	#num#	k4
až	až	k9
64	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
4823	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ZAJAC	ZAJAC	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Torpédoborce	torpédoborec	k1gInSc2
třídy	třída	k1gFnSc2
Zumwalt	Zumwalt	k1gInSc1
<g/>
:	:	kIx,
Návrat	návrat	k1gInSc1
do	do	k7c2
budoucnosti	budoucnost	k1gFnSc2
ve	v	k7c6
stylu	styl	k1gInSc6
US	US	kA
Navy	Navy	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATM	ATM	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
48	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
58	#num#	k4
až	až	k9
63	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
4823	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ZAJAC	ZAJAC	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Torpédoborce	torpédoborec	k1gInSc2
třídy	třída	k1gFnSc2
Zumwalt	Zumwalta	k1gFnPc2
<g/>
:	:	kIx,
Luxusní	luxusní	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
moderních	moderní	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATM	ATM	kA
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
48	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
62	#num#	k4
až	až	k9
67	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
4823	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
amerických	americký	k2eAgMnPc2d1
torpédoborců	torpédoborec	k1gMnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
třída	třída	k1gFnSc1
Zumwalt	Zumwalt	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Více	hodně	k6eAd2
o	o	k7c6
celé	celý	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
Zumwalt	Zumwalta	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Web	web	k1gInSc1
výrobce	výrobce	k1gMnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Americké	americký	k2eAgInPc1d1
torpédoborce	torpédoborec	k1gInPc1
třídy	třída	k1gFnSc2
Zumwalt	Zumwalt	k1gMnSc1
</s>
<s>
Zumwalt	Zumwalt	k1gMnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Monsoor	Monsoor	k1gMnSc1
•	•	k?
Lyndon	Lyndon	k1gMnSc1
B.	B.	kA
Johnson	Johnson	k1gMnSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
třída	třída	k1gFnSc1
Arleigh	Arleigha	k1gFnPc2
Burke	Burke	k1gFnSc1
Seznam	seznam	k1gInSc1
amerických	americký	k2eAgMnPc2d1
torpédoborců	torpédoborec	k1gMnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Loďstvo	loďstvo	k1gNnSc1
</s>
