<s>
Chad	Chad	k6eAd1	Chad
Michael	Michael	k1gMnSc1	Michael
Murray	Murraa	k1gFnSc2	Murraa
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
rolí	role	k1gFnSc7	role
Lucase	Lucasa	k1gFnSc6	Lucasa
Scott	Scott	k1gMnSc1	Scott
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
stanice	stanice	k1gFnSc2	stanice
The	The	k1gFnSc2	The
CW	CW	kA	CW
One	One	k1gMnSc2	One
Tree	Tre	k1gMnSc2	Tre
Hill	Hill	k1gMnSc1	Hill
a	a	k8xC	a
rolemi	role	k1gFnPc7	role
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Moderní	moderní	k2eAgMnSc1d1	moderní
Popelka	Popelka	k1gMnSc1	Popelka
<g/>
,	,	kIx,	,
Mezi	mezi	k7c7	mezi
námi	my	k3xPp1nPc7	my
děvčaty	děvče	k1gNnPc7	děvče
a	a	k8xC	a
Dům	dům	k1gInSc1	dům
voskových	voskový	k2eAgFnPc2d1	vosková
figurín	figurína	k1gFnPc2	figurína
<g/>
.	.	kIx.	.
</s>
<s>
Chad	Chad	k1gMnSc1	Chad
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Buffalu	Buffal	k1gInSc6	Buffal
v	v	k7c6	v
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
malý	malý	k2eAgInSc1d1	malý
ho	on	k3xPp3gInSc4	on
opustila	opustit	k5eAaPmAgFnS	opustit
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
vychováván	vychovávat	k5eAaImNgMnS	vychovávat
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
Rexem	Rex	k1gMnSc7	Rex
Murrayem	Murray	k1gMnSc7	Murray
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgMnPc4	tři
bratry	bratr	k1gMnPc4	bratr
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc2	jeden
nevlastního	vlastní	k2eNgMnSc2d1	nevlastní
bratra	bratr	k1gMnSc2	bratr
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
nevlastní	vlastní	k2eNgFnSc4d1	nevlastní
sestru	sestra	k1gFnSc4	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
Clarence	Clarenec	k1gInPc4	Clarenec
High	Higha	k1gFnPc2	Higha
School	Schoola	k1gFnPc2	Schoola
<g/>
.	.	kIx.	.
</s>
<s>
Zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
fotbal	fotbal	k1gInSc4	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
modelingu	modeling	k1gInSc3	modeling
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
Skechers	Skechers	k1gInSc4	Skechers
<g/>
,	,	kIx,	,
Tommyho	Tommy	k1gMnSc4	Tommy
Hilfingera	Hilfinger	k1gMnSc4	Hilfinger
a	a	k8xC	a
pro	pro	k7c4	pro
Gucci	Gucce	k1gFnSc4	Gucce
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
role	role	k1gFnSc2	role
v	v	k7c6	v
seriálu	seriál	k1gInSc2	seriál
Gilmorova	Gilmorův	k2eAgNnPc1d1	Gilmorův
děvčata	děvče	k1gNnPc1	děvče
<g/>
.	.	kIx.	.
</s>
<s>
Zahrál	zahrát	k5eAaPmAgMnS	zahrát
si	se	k3xPyFc3	se
bohatého	bohatý	k2eAgMnSc4d1	bohatý
chlapce	chlapec	k1gMnSc4	chlapec
Tristana	Tristan	k1gMnSc4	Tristan
DuGreye	DuGrey	k1gMnSc4	DuGrey
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
host	host	k1gMnSc1	host
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
několika	několik	k4yIc6	několik
televizních	televizní	k2eAgInPc6d1	televizní
seriálech	seriál	k1gInPc6	seriál
<g/>
:	:	kIx,	:
Undresses	Undresses	k1gMnSc1	Undresses
<g/>
,	,	kIx,	,
Diagnóza	diagnóza	k1gFnSc1	diagnóza
vražda	vražda	k1gFnSc1	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2001	[number]	k4	2001
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
také	také	k9	také
roli	role	k1gFnSc4	role
Charlieho	Charlie	k1gMnSc2	Charlie
Todda	Todd	k1gMnSc2	Todd
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
populárním	populární	k2eAgInSc6d1	populární
seriálu	seriál	k1gInSc6	seriál
Dawsonův	Dawsonův	k2eAgInSc4d1	Dawsonův
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
televizní	televizní	k2eAgFnSc6d1	televizní
obrazovce	obrazovka	k1gFnSc6	obrazovka
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
i	i	k9	i
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Aftermath	Aftermath	k1gMnSc1	Aftermath
a	a	k8xC	a
The	The	k1gMnSc1	The
Lone	Lone	k1gFnPc2	Lone
Wolf	Wolf	k1gMnSc1	Wolf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Mezi	mezi	k7c7	mezi
námi	my	k3xPp1nPc7	my
děvčaty	děvče	k1gNnPc7	děvče
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roli	role	k1gFnSc6	role
Jakea	Jake	k1gInSc2	Jake
<g/>
,	,	kIx,	,
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Jamie	Jamie	k1gFnSc2	Jamie
Lee	Lea	k1gFnSc3	Lea
Curtis	Curtis	k1gFnSc2	Curtis
a	a	k8xC	a
Lindsay	Lindsaa	k1gFnSc2	Lindsaa
Lohan	Lohana	k1gFnPc2	Lohana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
získal	získat	k5eAaPmAgMnS	získat
hlavní	hlavní	k2eAgFnSc3d1	hlavní
roli	role	k1gFnSc3	role
Lucase	Lucasa	k1gFnSc3	Lucasa
Scotta	Scott	k1gInSc2	Scott
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
stanice	stanice	k1gFnSc2	stanice
The	The	k1gFnSc2	The
WB	WB	kA	WB
One	One	k1gMnSc4	One
Tree	Tre	k1gMnSc4	Tre
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
stal	stát	k5eAaPmAgInS	stát
hitem	hit	k1gInSc7	hit
stanice	stanice	k1gFnSc2	stanice
s	s	k7c7	s
přes	přes	k7c4	přes
4,5	[number]	k4	4,5
miliony	milion	k4xCgInPc1	milion
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
seriál	seriál	k1gInSc4	seriál
získal	získat	k5eAaPmAgMnS	získat
několik	několik	k4yIc4	několik
nominací	nominace	k1gFnPc2	nominace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgFnPc2	dva
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
Teen	Teen	k1gNnSc4	Teen
Choice	Choice	k1gFnSc2	Choice
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
si	se	k3xPyFc3	se
zahrá	zahrát	k5eAaPmIp3nS	zahrát
lpo	lpo	k?	lpo
boku	bok	k1gInSc2	bok
Hilarie	Hilarie	k1gFnSc2	Hilarie
Burtonové	Burtonový	k2eAgFnPc1d1	Burtonová
<g/>
,	,	kIx,	,
Jamese	Jamese	k1gFnPc1	Jamese
Laffertyho	Lafferty	k1gMnSc2	Lafferty
a	a	k8xC	a
poznal	poznat	k5eAaPmAgMnS	poznat
zde	zde	k6eAd1	zde
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
manželku	manželka	k1gFnSc4	manželka
Sophii	Sophie	k1gFnSc4	Sophie
Bushovou	Bushová	k1gFnSc4	Bushová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
hrál	hrát	k5eAaImAgMnS	hrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Hilary	Hilara	k1gFnSc2	Hilara
Duffové	Duffový	k2eAgFnSc2d1	Duffová
v	v	k7c6	v
romantickém	romantický	k2eAgInSc6d1	romantický
snímku	snímek	k1gInSc6	snímek
Moderní	moderní	k2eAgMnSc1d1	moderní
Popelka	Popelka	k1gMnSc1	Popelka
postavu	postava	k1gFnSc4	postava
Austina	Austin	k2eAgMnSc2d1	Austin
Amese	Ames	k1gMnSc2	Ames
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vydělal	vydělat	k5eAaPmAgInS	vydělat
přes	přes	k7c4	přes
70	[number]	k4	70
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Dům	dům	k1gInSc1	dům
voskových	voskový	k2eAgFnPc2d1	vosková
figurín	figurína	k1gFnPc2	figurína
v	v	k7c6	v
roli	role	k1gFnSc6	role
Nicka	nicka	k1gFnSc1	nicka
Jonese	Jonese	k1gFnSc1	Jonese
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
kritiky	kritika	k1gFnSc2	kritika
hodnocen	hodnotit	k5eAaImNgMnS	hodnotit
negativně	negativně	k6eAd1	negativně
<g/>
,	,	kIx,	,
vydělal	vydělat	k5eAaPmAgInS	vydělat
přes	přes	k7c4	přes
68	[number]	k4	68
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
snímku	snímek	k1gInSc6	snímek
Země	zem	k1gFnSc2	zem
zatracených	zatracený	k2eAgFnPc2d1	zatracená
<g/>
,	,	kIx,	,
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Samuela	Samuel	k1gMnSc4	Samuel
L.	L.	kA	L.
Jacksona	Jackson	k1gMnSc4	Jackson
a	a	k8xC	a
Jessicy	Jessicy	k1gInPc4	Jessicy
Biel	Biela	k1gFnPc2	Biela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neobjeví	objevit	k5eNaPmIp3nS	objevit
v	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
sérii	série	k1gFnSc6	série
seriálu	seriál	k1gInSc2	seriál
One	One	k1gFnSc2	One
Tree	Tree	k1gFnPc2	Tree
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
ve	v	k7c6	v
videoklipu	videoklip	k1gInSc6	videoklip
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Alicii	Alicie	k1gFnSc4	Alicie
Keys	Keys	k1gInSc4	Keys
k	k	k7c3	k
písničce	písnička	k1gFnSc3	písnička
"	"	kIx"	"
<g/>
Un-Thinkable	Un-Thinkable	k1gFnSc1	Un-Thinkable
(	(	kIx(	(
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Ready	ready	k0	ready
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
jako	jako	k9	jako
Ethan	ethan	k1gInSc1	ethan
McAllister	McAllistra	k1gFnPc2	McAllistra
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
filmu	film	k1gInSc6	film
stanice	stanice	k1gFnSc2	stanice
Lifetime	Lifetim	k1gInSc5	Lifetim
Před	před	k7c7	před
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
ve	v	k7c6	v
vánočním	vánoční	k2eAgInSc6d1	vánoční
filmu	film	k1gInSc6	film
stanice	stanice	k1gFnSc2	stanice
ABC	ABC	kA	ABC
Family	Famila	k1gFnSc2	Famila
Vánoční	vánoční	k2eAgMnSc1d1	vánoční
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Ashley	Ashlea	k1gFnSc2	Ashlea
Benson	Bensona	k1gFnPc2	Bensona
a	a	k8xC	a
Christiny	Christina	k1gFnSc2	Christina
Millan	Millana	k1gFnPc2	Millana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Fruitvale	Fruitvala	k1gFnSc3	Fruitvala
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
ocenění	ocenění	k1gNnPc4	ocenění
na	na	k7c6	na
Filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
Sundance	Sundance	k1gFnSc2	Sundance
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
do	do	k7c2	do
role	role	k1gFnSc2	role
agenta	agent	k1gMnSc2	agent
Jacka	Jacek	k1gMnSc2	Jacek
Thompsona	Thompson	k1gMnSc2	Thompson
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
seriálu	seriál	k1gInSc2	seriál
Agent	agent	k1gMnSc1	agent
Carter	Carter	k1gMnSc1	Carter
<g/>
.	.	kIx.	.
</s>
<s>
Chad	Chad	k1gMnSc1	Chad
se	se	k3xPyFc4	se
zasnoubil	zasnoubit	k5eAaPmAgMnS	zasnoubit
se	s	k7c7	s
Sophii	Sophium	k1gNnPc7	Sophium
Bush	Bush	k1gMnSc1	Bush
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
a	a	k8xC	a
vzali	vzít	k5eAaPmAgMnP	vzít
se	se	k3xPyFc4	se
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
v	v	k7c6	v
Santa	Sant	k1gMnSc2	Sant
Monice	Monika	k1gFnSc6	Monika
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
rozchod	rozchod	k1gInSc4	rozchod
oznámili	oznámit	k5eAaPmAgMnP	oznámit
v	v	k7c6	v
září	září	k1gNnSc6	září
2005	[number]	k4	2005
a	a	k8xC	a
dvojice	dvojice	k1gFnSc1	dvojice
se	se	k3xPyFc4	se
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
začal	začít	k5eAaPmAgInS	začít
chodit	chodit	k5eAaImF	chodit
s	s	k7c7	s
Kenzie	Kenzie	k1gFnPc1	Kenzie
Dalton	Dalton	k1gInSc1	Dalton
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
poznal	poznat	k5eAaPmAgMnS	poznat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
získala	získat	k5eAaPmAgFnS	získat
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Miss	miss	k1gFnSc2	miss
Severní	severní	k2eAgFnSc2d1	severní
Karolíny	Karolína	k1gFnSc2	Karolína
Teen	Teena	k1gFnPc2	Teena
a	a	k8xC	a
zahrála	zahrát	k5eAaPmAgFnS	zahrát
si	se	k3xPyFc3	se
menší	malý	k2eAgFnSc4d2	menší
roli	role	k1gFnSc4	role
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
One	One	k1gFnSc2	One
Tree	Tree	k1gFnPc2	Tree
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
se	se	k3xPyFc4	se
zasnoubila	zasnoubit	k5eAaPmAgFnS	zasnoubit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
své	své	k1gNnSc4	své
sedmi-leté	sedmietý	k2eAgNnSc4d1	sedmi-letý
zasnoubení	zasnoubení	k1gNnSc4	zasnoubení
odvolali	odvolat	k5eAaPmAgMnP	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
do	do	k7c2	do
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
chodil	chodit	k5eAaImAgInS	chodit
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
Nicky	nicka	k1gFnSc2	nicka
Whelan	Whelan	k1gInSc1	Whelan
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
-	-	kIx~	-
Teen	Teen	k1gNnSc4	Teen
Choice	Choice	k1gFnSc2	Choice
Awards	Awardsa	k1gFnPc2	Awardsa
-	-	kIx~	-
Průlomová	průlomový	k2eAgFnSc1d1	průlomová
filmová	filmový	k2eAgFnSc1d1	filmová
hvězda	hvězda	k1gFnSc1	hvězda
-	-	kIx~	-
muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
za	za	k7c4	za
Moderní	moderní	k2eAgFnSc4d1	moderní
Popelku	Popelka	k1gFnSc4	Popelka
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
-	-	kIx~	-
Teen	Teen	k1gNnSc4	Teen
Choice	Choice	k1gFnSc2	Choice
Awards	Awardsa	k1gFnPc2	Awardsa
-	-	kIx~	-
Průlomová	průlomový	k2eAgFnSc1d1	průlomová
televizní	televizní	k2eAgFnSc1d1	televizní
hvězda	hvězda	k1gFnSc1	hvězda
-	-	kIx~	-
muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
za	za	k7c7	za
One	One	k1gFnSc7	One
Tree	Tree	k1gFnSc1	Tree
Hill	Hill	k1gMnSc1	Hill
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
Teen	Teen	k1gNnSc4	Teen
Choice	Choice	k1gFnSc2	Choice
Awards	Awardsa	k1gFnPc2	Awardsa
-	-	kIx~	-
Herec	herec	k1gMnSc1	herec
v	v	k7c6	v
akčním	akční	k2eAgInSc6d1	akční
<g/>
/	/	kIx~	/
<g/>
dobrodružném	dobrodružný	k2eAgInSc6d1	dobrodružný
<g/>
/	/	kIx~	/
<g/>
thrillerovém	thrillerový	k2eAgInSc6d1	thrillerový
filmu	film	k1gInSc6	film
(	(	kIx(	(
<g/>
za	za	k7c4	za
Dům	dům	k1gInSc4	dům
voskových	voskový	k2eAgFnPc2d1	vosková
figurín	figurína	k1gFnPc2	figurína
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chad	Chada	k1gFnPc2	Chada
Michael	Michael	k1gMnSc1	Michael
Murray	Murraa	k1gFnSc2	Murraa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Chad	Chado	k1gNnPc2	Chado
Michael	Michael	k1gMnSc1	Michael
Murray	Murraa	k1gFnSc2	Murraa
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Chad	Chada	k1gFnPc2	Chada
Michael	Michael	k1gMnSc1	Michael
Murray	Murraa	k1gFnSc2	Murraa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
