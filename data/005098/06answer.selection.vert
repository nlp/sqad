<s>
Démokritos	Démokritos	k1gMnSc1	Démokritos
představil	představit	k5eAaPmAgMnS	představit
filosofickou	filosofický	k2eAgFnSc4d1	filosofická
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
nelze	lze	k6eNd1	lze
hmotu	hmota	k1gFnSc4	hmota
dělit	dělit	k5eAaImF	dělit
donekonečna	donekonečna	k6eAd1	donekonečna
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
úrovni	úroveň	k1gFnSc6	úroveň
existují	existovat	k5eAaImIp3nP	existovat
dále	daleko	k6eAd2	daleko
nedělitelné	dělitelný	k2eNgFnPc1d1	nedělitelná
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
označil	označit	k5eAaPmAgMnS	označit
slovem	slovem	k6eAd1	slovem
atomos	atomos	k1gMnSc1	atomos
(	(	kIx(	(
<g/>
ἄ	ἄ	k?	ἄ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
