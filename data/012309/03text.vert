<p>
<s>
Klíč	klíč	k1gInSc1	klíč
je	být	k5eAaImIp3nS	být
technická	technický	k2eAgFnSc1d1	technická
pomůcka	pomůcka	k1gFnSc1	pomůcka
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
zamykání	zamykání	k1gNnSc3	zamykání
a	a	k8xC	a
odemykání	odemykání	k1gNnSc3	odemykání
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Existenci	existence	k1gFnSc4	existence
klíčů	klíč	k1gInPc2	klíč
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
již	již	k6eAd1	již
Homér	Homér	k1gMnSc1	Homér
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Odyssea	Odyssea	k1gFnSc1	Odyssea
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Penelopou	Penelopa	k1gFnSc7	Penelopa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
opatrovala	opatrovat	k5eAaImAgFnS	opatrovat
klíč	klíč	k1gInSc4	klíč
z	z	k7c2	z
bronzu	bronz	k1gInSc2	bronz
a	a	k8xC	a
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
Klíče	klíč	k1gInSc2	klíč
tedy	tedy	k9	tedy
znali	znát	k5eAaImAgMnP	znát
staří	starý	k2eAgMnPc1d1	starý
Řekové	Řek	k1gMnPc1	Řek
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
archeologické	archeologický	k2eAgFnSc2d1	archeologická
vykopávky	vykopávka	k1gFnSc2	vykopávka
v	v	k7c6	v
Thébách	Théby	k1gFnPc6	Théby
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
zámků	zámek	k1gInPc2	zámek
s	s	k7c7	s
klíči	klíč	k1gInPc7	klíč
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
==	==	k?	==
Provedení	provedení	k1gNnSc1	provedení
==	==	k?	==
</s>
</p>
<p>
<s>
Klíče	klíč	k1gInPc1	klíč
se	se	k3xPyFc4	se
lišily	lišit	k5eAaImAgInP	lišit
svým	svůj	k3xOyFgNnSc7	svůj
provedením	provedení	k1gNnSc7	provedení
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
zdobené	zdobený	k2eAgFnPc1d1	zdobená
zejména	zejména	k9	zejména
na	na	k7c6	na
očku	očko	k1gNnSc6	očko
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
držení	držení	k1gNnSc3	držení
rukou	ruka	k1gFnPc2	ruka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
různě	různě	k6eAd1	různě
tvarované	tvarovaný	k2eAgFnPc1d1	tvarovaná
<g/>
,	,	kIx,	,
lišily	lišit	k5eAaImAgFnP	lišit
se	se	k3xPyFc4	se
vahou	váha	k1gFnSc7	váha
<g/>
,	,	kIx,	,
délkou	délka	k1gFnSc7	délka
i	i	k8xC	i
použitím	použití	k1gNnSc7	použití
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
zdobné	zdobný	k2eAgFnPc1d1	zdobná
úpravy	úprava	k1gFnPc1	úprava
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
až	až	k9	až
do	do	k7c2	do
sklonku	sklonek	k1gInSc2	sklonek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
technický	technický	k2eAgInSc1d1	technický
vývoj	vývoj	k1gInSc1	vývoj
znamenal	znamenat	k5eAaImAgInS	znamenat
složitější	složitý	k2eAgInSc1d2	složitější
zámek	zámek	k1gInSc1	zámek
oproti	oproti	k7c3	oproti
zjednodušeným	zjednodušený	k2eAgInPc3d1	zjednodušený
klíčům	klíč	k1gInPc3	klíč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
některých	některý	k3yIgInPc2	některý
důležitých	důležitý	k2eAgInPc2d1	důležitý
klíčů	klíč	k1gInPc2	klíč
bylo	být	k5eAaImAgNnS	být
odznakem	odznak	k1gInSc7	odznak
důstojnosti	důstojnost	k1gFnSc2	důstojnost
<g/>
,	,	kIx,	,
nosívaly	nosívat	k5eAaImAgFnP	nosívat
se	se	k3xPyFc4	se
zavěšené	zavěšený	k2eAgFnPc1d1	zavěšená
na	na	k7c6	na
krku	krk	k1gInSc6	krk
na	na	k7c6	na
zlatém	zlatý	k2eAgInSc6d1	zlatý
řetězu	řetěz	k1gInSc6	řetěz
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
klíče	klíč	k1gInPc1	klíč
u	u	k7c2	u
královských	královský	k2eAgInPc2d1	královský
dvorů	dvůr	k1gInPc2	dvůr
měly	mít	k5eAaImAgFnP	mít
svá	svůj	k3xOyFgNnPc4	svůj
pojmenování	pojmenování	k1gNnSc4	pojmenování
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Komorní	komorní	k2eAgInSc1d1	komorní
klíč	klíč	k1gInSc1	klíč
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
klíče	klíč	k1gInSc2	klíč
==	==	k?	==
</s>
</p>
<p>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
klíč	klíč	k1gInSc1	klíč
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
–	–	k?	–
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
vybavena	vybavit	k5eAaPmNgFnS	vybavit
očkem	očko	k1gNnSc7	očko
<g/>
,	,	kIx,	,
umožňujícím	umožňující	k2eAgNnSc7d1	umožňující
klíč	klíč	k1gInSc4	klíč
zavěsit	zavěsit	k5eAaPmF	zavěsit
<g/>
,	,	kIx,	,
a	a	k8xC	a
trnu	trnout	k5eAaImIp1nS	trnout
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
klíče	klíč	k1gInSc2	klíč
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vsouvá	vsouvat	k5eAaImIp3nS	vsouvat
do	do	k7c2	do
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mechanismu	mechanismus	k1gInSc2	mechanismus
zámku	zámek	k1gInSc2	zámek
bývá	bývat	k5eAaImIp3nS	bývat
trn	trn	k1gInSc1	trn
vybaven	vybavit	k5eAaPmNgInS	vybavit
zuby	zub	k1gInPc7	zub
nebo	nebo	k8xC	nebo
drážkami	drážka	k1gFnPc7	drážka
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
i	i	k8xC	i
drážky	drážka	k1gFnPc1	drážka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
i	i	k8xC	i
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
trnu	trn	k1gInSc2	trn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Určení	určení	k1gNnSc1	určení
klíčů	klíč	k1gInPc2	klíč
==	==	k?	==
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
původní	původní	k2eAgNnSc4d1	původní
použití	použití	k1gNnSc4	použití
k	k	k7c3	k
odemykání	odemykání	k1gNnSc3	odemykání
zámků	zámek	k1gInPc2	zámek
dveří	dveře	k1gFnPc2	dveře
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
zámky	zámek	k1gInPc4	zámek
a	a	k8xC	a
klíče	klíč	k1gInPc4	klíč
k	k	k7c3	k
nedobytným	dobytný	k2eNgFnPc3d1	nedobytná
pokladnám	pokladna	k1gFnPc3	pokladna
<g/>
,	,	kIx,	,
trezorům	trezor	k1gInPc3	trezor
<g/>
,	,	kIx,	,
patentním	patentní	k2eAgInPc3d1	patentní
zámkům	zámek	k1gInPc3	zámek
(	(	kIx(	(
<g/>
DOZ	DOZ	kA	DOZ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Paklíč	paklíč	k1gInSc4	paklíč
==	==	k?	==
</s>
</p>
<p>
<s>
Zjednodušený	zjednodušený	k2eAgInSc1d1	zjednodušený
typ	typ	k1gInSc1	typ
klíče	klíč	k1gInSc2	klíč
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
tzv.	tzv.	kA	tzv.
planžety	planžet	k1gInPc1	planžet
(	(	kIx(	(
<g/>
technika	technika	k1gFnSc1	technika
vyháčkování	vyháčkování	k1gNnSc2	vyháčkování
zámku	zámek	k1gInSc2	zámek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgMnS	používat
zámečníky	zámečník	k1gMnPc7	zámečník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
rovněž	rovněž	k9	rovněž
bytovými	bytový	k2eAgMnPc7d1	bytový
zloději	zloděj	k1gMnPc7	zloděj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úpravy	úprava	k1gFnPc4	úprava
klíčů	klíč	k1gInPc2	klíč
==	==	k?	==
</s>
</p>
<p>
<s>
Výrobou	výroba	k1gFnSc7	výroba
klíčů	klíč	k1gInPc2	klíč
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
zámečnické	zámečnický	k2eAgFnPc1d1	zámečnická
provozovny	provozovna	k1gFnPc1	provozovna
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
i	i	k9	i
hobbymarkety	hobbymarket	k1gInPc1	hobbymarket
či	či	k8xC	či
nákupní	nákupní	k2eAgNnPc1d1	nákupní
centra	centrum	k1gNnPc1	centrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sběratelství	sběratelství	k1gNnPc2	sběratelství
klíčů	klíč	k1gInPc2	klíč
==	==	k?	==
</s>
</p>
<p>
<s>
Vytváření	vytváření	k1gNnSc1	vytváření
sbírek	sbírka	k1gFnPc2	sbírka
klíčů	klíč	k1gInPc2	klíč
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
sběratelských	sběratelský	k2eAgInPc2d1	sběratelský
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
největším	veliký	k2eAgInSc6d3	veliký
českém	český	k2eAgInSc6d1	český
Klubu	klub	k1gInSc6	klub
sběratelů	sběratel	k1gMnPc2	sběratel
kuriozit	kuriozita	k1gFnPc2	kuriozita
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
sekce	sekce	k1gFnSc2	sekce
Různé	různý	k2eAgFnSc2d1	různá
<g/>
.	.	kIx.	.
</s>
<s>
Sbírky	sbírka	k1gFnPc1	sbírka
klíčů	klíč	k1gInPc2	klíč
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
řady	řada	k1gFnSc2	řada
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
UHLÁŘ	uhlář	k1gMnSc1	uhlář
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Technická	technický	k2eAgFnSc1d1	technická
ochrana	ochrana	k1gFnSc1	ochrana
objektů	objekt	k1gInPc2	objekt
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
Mechanické	mechanický	k2eAgInPc1d1	mechanický
zábranné	zábranný	k2eAgInPc1d1	zábranný
systémy	systém	k1gInPc1	systém
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Policejní	policejní	k2eAgFnSc1d1	policejní
akademie	akademie	k1gFnSc1	akademie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7251	[number]	k4	7251
<g/>
-	-	kIx~	-
<g/>
312	[number]	k4	312
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
(	(	kIx(	(
<g/>
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Klika	klika	k1gFnSc1	klika
(	(	kIx(	(
<g/>
dveře	dveře	k1gFnPc1	dveře
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Petlice	petlice	k1gFnSc1	petlice
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
klíč	klíč	k1gInSc4	klíč
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
