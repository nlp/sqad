<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velkou	velký	k2eAgFnSc4d1	velká
nezralou	zralý	k2eNgFnSc4d1	nezralá
buňku	buňka	k1gFnSc4	buňka
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
v	v	k7c4	v
basofilní	basofilní	k2eAgInSc4d1	basofilní
normoblast	normoblast	k1gInSc4	normoblast
a	a	k8xC	a
následně	následně	k6eAd1	následně
přes	přes	k7c4	přes
polychromatofilní	polychromatofilní	k2eAgInSc4d1	polychromatofilní
erytroblast	erytroblast	k1gInSc4	erytroblast
<g/>
,	,	kIx,	,
ortochromní	ortochromní	k2eAgInSc4d1	ortochromní
erytroblast	erytroblast	k1gInSc4	erytroblast
a	a	k8xC	a
retikulocyt	retikulocyt	k1gInSc4	retikulocyt
a	a	k8xC	a
až	až	k9	až
ve	v	k7c4	v
vlastní	vlastní	k2eAgFnSc4d1	vlastní
červenou	červený	k2eAgFnSc4d1	červená
krvinku	krvinka	k1gFnSc4	krvinka
–	–	k?	–
erytrocyt	erytrocyt	k1gInSc1	erytrocyt
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
už	už	k6eAd1	už
jádro	jádro	k1gNnSc1	jádro
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
