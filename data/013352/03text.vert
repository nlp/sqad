<p>
<s>
Proerytroblast	Proerytroblast	k1gInSc1	Proerytroblast
(	(	kIx(	(
<g/>
pronormoblast	pronormoblast	k1gInSc1	pronormoblast
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
prekursorová	prekursorový	k2eAgFnSc1d1	prekursorový
buňka	buňka	k1gFnSc1	buňka
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
z	z	k7c2	z
pěti	pět	k4xCc2	pět
fází	fáze	k1gFnPc2	fáze
tvorby	tvorba	k1gFnSc2	tvorba
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
(	(	kIx(	(
<g/>
erytropoéza	erytropoéza	k1gFnSc1	erytropoéza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velkou	velký	k2eAgFnSc4d1	velká
nezralou	zralý	k2eNgFnSc4d1	nezralá
buňku	buňka	k1gFnSc4	buňka
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
v	v	k7c4	v
basofilní	basofilní	k2eAgInSc4d1	basofilní
normoblast	normoblast	k1gInSc4	normoblast
a	a	k8xC	a
následně	následně	k6eAd1	následně
přes	přes	k7c4	přes
polychromatofilní	polychromatofilní	k2eAgInSc4d1	polychromatofilní
erytroblast	erytroblast	k1gInSc4	erytroblast
<g/>
,	,	kIx,	,
ortochromní	ortochromní	k2eAgInSc4d1	ortochromní
erytroblast	erytroblast	k1gInSc4	erytroblast
a	a	k8xC	a
retikulocyt	retikulocyt	k1gInSc4	retikulocyt
a	a	k8xC	a
až	až	k9	až
ve	v	k7c4	v
vlastní	vlastní	k2eAgFnSc4d1	vlastní
červenou	červený	k2eAgFnSc4d1	červená
krvinku	krvinka	k1gFnSc4	krvinka
–	–	k?	–
erytrocyt	erytrocyt	k1gInSc1	erytrocyt
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
už	už	k6eAd1	už
jádro	jádro	k1gNnSc1	jádro
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Proerytroblast	Proerytroblast	k1gInSc1	Proerytroblast
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
jako	jako	k8xC	jako
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
vyzrálé	vyzrálý	k2eAgInPc4d1	vyzrálý
erytrocyty	erytrocyt	k1gInPc4	erytrocyt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
kulatý	kulatý	k2eAgInSc1d1	kulatý
nebo	nebo	k8xC	nebo
oválný	oválný	k2eAgInSc1d1	oválný
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
buňku	buňka	k1gFnSc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Cytoplazma	cytoplazma	k1gFnSc1	cytoplazma
je	být	k5eAaImIp3nS	být
sytě	sytě	k6eAd1	sytě
modrofialová	modrofialový	k2eAgFnSc1d1	modrofialová
mívá	mívat	k5eAaImIp3nS	mívat
laločnaté	laločnatý	k2eAgInPc1d1	laločnatý
výběžky	výběžek	k1gInPc1	výběžek
a	a	k8xC	a
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádná	žádný	k3yNgFnSc1	žádný
granula	granula	k1gFnSc1	granula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Erytroblast	Erytroblast	k1gInSc1	Erytroblast
</s>
</p>
