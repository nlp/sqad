<s>
Ivan	Ivan	k1gMnSc1
Tichon	Tichon	k1gMnSc1
</s>
<s>
Ivan	Ivan	k1gMnSc1
Tichon	Tichon	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1976	#num#	k4
(	(	kIx(
<g/>
44	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Hloŭ	Hloŭ	k1gMnPc4
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Grodženská	Grodženský	k2eAgFnSc1d1
universita	universita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
kladivář	kladivář	k1gMnSc1
a	a	k8xC
atlet	atlet	k1gMnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc1
vlasti	vlast	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
Řád	řád	k1gInSc1
cti	čest	k1gFnSc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
Řád	řád	k1gInSc1
vlasti	vlast	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
třídyZasloužilý	třídyZasloužilý	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
sportu	sport	k1gInSc2
Běloruské	běloruský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Atletika	atletika	k1gFnSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
LOH	LOH	kA
2008	#num#	k4
</s>
<s>
hod	hod	k1gInSc1
kladivem	kladivo	k1gNnSc7
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
LOH	LOH	kA
2016	#num#	k4
</s>
<s>
hod	hod	k1gInSc1
kladivem	kladivo	k1gNnSc7
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MS	MS	kA
2003	#num#	k4
</s>
<s>
hod	hod	k1gInSc1
kladivem	kladivo	k1gNnSc7
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MS	MS	kA
2007	#num#	k4
</s>
<s>
hod	hod	k1gInSc1
kladivem	kladivo	k1gNnSc7
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
ME	ME	kA
2006	#num#	k4
</s>
<s>
hod	hod	k1gInSc1
kladivem	kladivo	k1gNnSc7
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
ME	ME	kA
2016	#num#	k4
</s>
<s>
hod	hod	k1gInSc1
kladivem	kladivo	k1gNnSc7
</s>
<s>
Ivan	Ivan	k1gMnSc1
Tichon	Tichon	k1gMnSc1
(	(	kIx(
<g/>
bělorusky	bělorusky	k6eAd1
<g/>
:	:	kIx,
І	І	k?
Ц	Ц	k?
<g/>
,	,	kIx,
čti	číst	k5eAaImRp2nS
Ivan	Ivan	k1gMnSc1
Cichan	Cichan	k1gMnSc1
<g/>
,	,	kIx,
*	*	kIx~
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1976	#num#	k4
Hlasievicy	Hlasievica	k1gFnSc2
u	u	k7c2
Slonimi	Sloni	k1gFnPc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
běloruský	běloruský	k2eAgMnSc1d1
kladivář	kladivář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvakrát	dvakrát	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
<g/>
,	,	kIx,
jednou	jednou	k6eAd1
mistrem	mistr	k1gMnSc7
Evropy	Evropa	k1gFnSc2
a	a	k8xC
vlastní	vlastnit	k5eAaImIp3nS
také	také	k9
bronz	bronz	k1gInSc4
z	z	k7c2
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
vyhrál	vyhrát	k5eAaPmAgMnS
Tichon	Tichon	k1gMnSc1
titul	titul	k1gInSc4
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Sydney	Sydney	k1gNnSc6
absolvoval	absolvovat	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
olympijský	olympijský	k2eAgInSc4d1
start	start	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
hrách	hra	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
tam	tam	k6eAd1
obsadil	obsadit	k5eAaPmAgMnS
čtvrté	čtvrtý	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Tichon	Tichon	k1gMnSc1
poprvé	poprvé	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
i	i	k9
šampionem	šampion	k1gMnSc7
letní	letní	k2eAgFnSc2d1
univerziády	univerziáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
Aténách	Atény	k1gFnPc6
vybojoval	vybojovat	k5eAaPmAgInS
původně	původně	k6eAd1
bronzovou	bronzový	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
za	za	k7c7
Japoncem	Japonec	k1gMnSc7
Murofušim	Murofušima	k1gFnPc2
<g/>
,	,	kIx,
hodem	hod	k1gInSc7
79,81	79,81	k4
m	m	kA
mu	on	k3xPp3gNnSc3
chyběly	chybět	k5eAaImAgFnP
ke	k	k7c3
zlatu	zlato	k1gNnSc3
více	hodně	k6eAd2
než	než	k8xS
tři	tři	k4xCgInPc4
metry	metr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
se	se	k3xPyFc4
probojoval	probojovat	k5eAaPmAgMnS
po	po	k7c6
diskvalifikaci	diskvalifikace	k1gFnSc6
Maďara	maďar	k1gInSc2
Adriana	Adriana	k1gFnSc1
Annuse	Annuse	k1gFnSc1
kvůli	kvůli	k7c3
dopingu	doping	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
byl	být	k5eAaImAgInS
však	však	k9
doping	doping	k1gInSc1
prokázán	prokázán	k2eAgInSc1d1
i	i	k8xC
Tichonovi	Tichon	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
v	v	k7c6
Helsinkách	Helsinky	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
se	se	k3xPyFc4
původně	původně	k6eAd1
vrátil	vrátit	k5eAaPmAgInS
na	na	k7c4
první	první	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
záda	záda	k1gNnPc4
mu	on	k3xPp3gMnSc3
kryl	krýt	k5eAaImAgMnS
jeho	jeho	k3xOp3gFnSc3
krajan	krajan	k1gMnSc1
Vadim	Vadim	k?
Děvjatovskij	Děvjatovskij	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oběma	dva	k4xCgMnPc3
kladivářům	kladivář	k1gMnPc3
však	však	k9
bylo	být	k5eAaImAgNnS
při	při	k7c6
zpětné	zpětný	k2eAgFnSc6d1
kontrole	kontrola	k1gFnSc6
vzorků	vzorek	k1gInPc2
prokázáno	prokázán	k2eAgNnSc1d1
užití	užití	k1gNnSc1
dopingu	doping	k1gInSc2
a	a	k8xC
byly	být	k5eAaImAgFnP
jim	on	k3xPp3gInPc3
odebrány	odebrán	k2eAgFnPc1d1
medaile	medaile	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zazářil	zazářit	k5eAaPmAgMnS
ale	ale	k9
také	také	k9
na	na	k7c6
otevřeném	otevřený	k2eAgNnSc6d1
mistrovství	mistrovství	k1gNnSc6
Běloruska	Bělorusko	k1gNnSc2
v	v	k7c6
Brestu	Brest	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
slavil	slavit	k5eAaImAgInS
vítězství	vítězství	k1gNnSc6
výkonem	výkon	k1gInSc7
86,73	86,73	k4
m	m	kA
<g/>
,	,	kIx,
pouhý	pouhý	k2eAgInSc4d1
centimetr	centimetr	k1gInSc4
za	za	k7c4
19	#num#	k4
let	léto	k1gNnPc2
starým	starý	k2eAgInSc7d1
světovým	světový	k2eAgInSc7d1
rekordem	rekord	k1gInSc7
Jurije	Jurije	k1gMnSc2
Sedycha	Sedych	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
v	v	k7c6
Göteborgu	Göteborg	k1gInSc6
mistrem	mistr	k1gMnSc7
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInSc4d1
titul	titul	k1gInSc4
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
si	se	k3xPyFc3
zajistil	zajistit	k5eAaPmAgInS
vynikajícím	vynikající	k2eAgInSc7d1
posledním	poslední	k2eAgInSc7d1
pokusem	pokus	k1gInSc7
na	na	k7c6
světovém	světový	k2eAgInSc6d1
šampionátu	šampionát	k1gInSc6
v	v	k7c6
Ósace	Ósaka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
užšího	úzký	k2eAgNnSc2d2
finále	finále	k1gNnSc2
se	se	k3xPyFc4
sice	sice	k8xC
dostal	dostat	k5eAaPmAgMnS
až	až	k6eAd1
jako	jako	k8xS,k8xC
poslední	poslední	k2eAgInSc1d1
osmý	osmý	k4xOgInSc4
a	a	k8xC
ještě	ještě	k9
před	před	k7c7
závěrečnými	závěrečný	k2eAgInPc7d1
pokusy	pokus	k1gInPc7
neměl	mít	k5eNaImAgMnS
jistou	jistý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
ale	ale	k9
z	z	k7c2
pátého	pátý	k4xOgNnSc2
místa	místo	k1gNnSc2
zaútočil	zaútočit	k5eAaPmAgInS
nejlepším	dobrý	k2eAgMnSc7d3
světovým	světový	k2eAgInSc7d1
výkonem	výkon	k1gInSc7
roku	rok	k1gInSc2
83,63	83,63	k4
m.	m.	k?
</s>
<s>
Prvním	první	k4xOgMnSc7
trenérem	trenér	k1gMnSc7
Tichona	Tichona	k1gFnSc1
byl	být	k5eAaImAgMnS
Igor	Igor	k1gMnSc1
Cycorin	Cycorin	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nyní	nyní	k6eAd1
ho	on	k3xPp3gMnSc4
vede	vést	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
vynikající	vynikající	k2eAgMnSc1d1
kladivář	kladivář	k1gMnSc1
<g/>
,	,	kIx,
olympijský	olympijský	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
Sergej	Sergej	k1gMnSc1
Litvinov	Litvinovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Soukromý	soukromý	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Tichon	Tichon	k1gInSc1
žije	žít	k5eAaImIp3nS
v	v	k7c6
Hrodně	Hrodna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
ženatý	ženatý	k2eAgMnSc1d1
s	s	k7c7
Volhou	Volha	k1gFnSc7
Tichonovou	Tichonový	k2eAgFnSc7d1
<g/>
,	,	kIx,
bývalou	bývalý	k2eAgFnSc7d1
diskařkou	diskařka	k1gFnSc7
a	a	k8xC
mají	mít	k5eAaImIp3nP
syna	syn	k1gMnSc4
Ivana	Ivan	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Ivan	Ivan	k1gMnSc1
Tsikhan	Tsikhan	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Při	při	k7c6
atletickém	atletický	k2eAgInSc6d1
MS	MS	kA
2005	#num#	k4
dopovali	dopovat	k5eAaImAgMnP
tři	tři	k4xCgMnPc1
vítězové	vítěz	k1gMnPc1
vrhačských	vrhačský	k2eAgFnPc2d1
disciplín	disciplína	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2013-03-08	2013-03-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tikhon	Tikhon	k1gNnSc1
1	#num#	k4
<g/>
cm	cm	kA
away	awaa	k1gFnSc2
from	from	k6eAd1
Sedykh	Sedykh	k1gInSc4
<g/>
’	’	k?
<g/>
s	s	k7c7
‘	‘	k?
<g/>
ancient	ancient	k1gMnSc1
<g/>
’	’	k?
World	World	k1gMnSc1
Hammer	Hammer	k1gMnSc1
record	record	k1gMnSc1
-	-	kIx~
Belarussian	Belarussian	k1gMnSc1
Open	Open	k1gMnSc1
Championships	Championshipsa	k1gFnPc2
REPORT	report	k1gInSc1
<g/>
,	,	kIx,
zpráva	zpráva	k1gFnSc1
na	na	k7c6
webu	web	k1gInSc6
IAAF	IAAF	kA
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ivan	Ivan	k1gMnSc1
Tichon	Tichon	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Ivan	Ivan	k1gMnSc1
Tichon	Tichon	k1gMnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
IAAF	IAAF	kA
</s>
<s>
Ivan	Ivan	k1gMnSc1
Tichon	Tichon	k1gMnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
И	И	k?
Т	Т	k?
<g/>
,	,	kIx,
portrét	portrét	k1gInSc1
Tichona	Tichona	k1gFnSc1
k	k	k7c3
olympijským	olympijský	k2eAgFnPc3d1
hrám	hra	k1gFnPc3
v	v	k7c6
Aténách	Atény	k1gFnPc6
<g/>
,	,	kIx,
web	web	k1gInSc1
banky	banka	k1gFnSc2
BelSwissBank	BelSwissBanka	k1gFnPc2
<g/>
,	,	kIx,
sponzora	sponzor	k1gMnSc2
Běloruského	běloruský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
olympijského	olympijský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mistři	mistr	k1gMnPc1
světa	svět	k1gInSc2
v	v	k7c6
hodu	hod	k1gInSc6
kladivem	kladivo	k1gNnSc7
</s>
<s>
1983	#num#	k4
<g/>
:	:	kIx,
Sergej	Sergej	k1gMnSc1
Litvinov	Litvinov	k1gInSc1
•	•	k?
1987	#num#	k4
<g/>
:	:	kIx,
Sergej	Sergej	k1gMnSc1
Litvinov	Litvinov	k1gInSc1
•	•	k?
1991	#num#	k4
<g/>
:	:	kIx,
Jurij	Jurij	k1gFnSc1
Sedych	Sedych	k1gInSc1
•	•	k?
1993	#num#	k4
<g/>
:	:	kIx,
Andrej	Andrej	k1gMnSc1
Abduvalijev	Abduvalijev	k1gFnSc2
•	•	k?
1995	#num#	k4
<g/>
:	:	kIx,
Andrej	Andrej	k1gMnSc1
Abduvalijev	Abduvalijev	k1gFnSc2
•	•	k?
1997	#num#	k4
<g/>
:	:	kIx,
Heinz	Heinz	k1gInSc1
Weis	Weis	k1gInSc1
•	•	k?
1999	#num#	k4
<g/>
:	:	kIx,
Karsten	Karsten	k2eAgInSc1d1
Kobs	Kobs	k1gInSc1
•	•	k?
2001	#num#	k4
<g/>
:	:	kIx,
Szymon	Szymon	k1gInSc1
Ziółkowski	Ziółkowsk	k1gFnSc2
•	•	k?
2003	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Ivan	Ivan	k1gMnSc1
Tichon	Tichon	k1gMnSc1
•	•	k?
2005	#num#	k4
<g/>
:	:	kIx,
Szymon	Szymon	k1gInSc1
Ziółkowski	Ziółkowsk	k1gFnSc2
•	•	k?
2007	#num#	k4
<g/>
:	:	kIx,
Ivan	Ivan	k1gMnSc1
Tichon	Tichona	k1gFnPc2
•	•	k?
2009	#num#	k4
<g/>
:	:	kIx,
Primož	Primož	k1gFnSc1
Kozmus	Kozmus	k1gInSc1
•	•	k?
2011	#num#	k4
<g/>
:	:	kIx,
Kódži	Kódž	k1gFnSc6
Murofuši	Murofuše	k1gFnSc4
•	•	k?
2013	#num#	k4
<g/>
:	:	kIx,
Paweł	Paweł	k1gFnSc1
Fajdek	Fajdek	k1gInSc1
•	•	k?
2015	#num#	k4
<g/>
:	:	kIx,
Paweł	Paweł	k1gFnSc1
Fajdek	Fajdek	k1gInSc1
•	•	k?
2017	#num#	k4
<g/>
:	:	kIx,
Paweł	Paweł	k1gFnSc1
Fajdek	Fajdek	k1gInSc1
•	•	k?
2019	#num#	k4
<g/>
:	:	kIx,
Paweł	Paweł	k1gMnSc1
Fajdek	Fajdek	k1gMnSc1
</s>
<s>
Mistři	mistr	k1gMnPc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
hodu	hod	k1gInSc6
kladivem	kladivo	k1gNnSc7
</s>
<s>
1934	#num#	k4
<g/>
:	:	kIx,
Ville	Ville	k1gFnSc1
Pörhölä	Pörhölä	k1gFnSc2
•	•	k?
1938	#num#	k4
<g/>
:	:	kIx,
Karl	Karla	k1gFnPc2
Hein	Hein	k1gInSc1
•	•	k?
1946	#num#	k4
<g/>
:	:	kIx,
Bo	Bo	k?
Ericson	Ericson	k1gInSc1
•	•	k?
1950	#num#	k4
<g/>
:	:	kIx,
Sverre	Sverr	k1gMnSc5
Strandli	Strandli	k1gMnSc5
•	•	k?
1954	#num#	k4
<g/>
:	:	kIx,
Michail	Michail	k1gInSc1
Krivonosov	Krivonosov	k1gInSc1
•	•	k?
1958	#num#	k4
<g/>
:	:	kIx,
Tadeusz	Tadeusz	k1gInSc1
Rut	rout	k5eAaImNgInS
•	•	k?
1962	#num#	k4
<g/>
:	:	kIx,
Gyula	Gyula	k1gFnSc1
Zsivótzky	Zsivótzka	k1gFnSc2
•	•	k?
1966	#num#	k4
<g/>
:	:	kIx,
Romuald	Romuald	k1gInSc1
Klim	Klim	k1gInSc1
•	•	k?
1969	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Anatolij	Anatolij	k1gMnSc1
Bondarčuk	Bondarčuk	k1gMnSc1
•	•	k?
1971	#num#	k4
<g/>
:	:	kIx,
Uwe	Uwe	k1gFnSc1
Beyer	Beyer	k1gInSc1
•	•	k?
1974	#num#	k4
<g/>
:	:	kIx,
Alexej	Alexej	k1gMnSc1
Spiridonov	Spiridonov	k1gInSc1
•	•	k?
1978	#num#	k4
<g/>
:	:	kIx,
Jurij	Jurij	k1gFnSc1
Sedych	Sedych	k1gInSc1
•	•	k?
1982	#num#	k4
<g/>
:	:	kIx,
Jurij	Jurij	k1gFnSc1
Sedych	Sedych	k1gInSc1
•	•	k?
1986	#num#	k4
<g/>
:	:	kIx,
Jurij	Jurij	k1gFnSc1
Sedych	Sedych	k1gInSc1
•	•	k?
1990	#num#	k4
<g/>
:	:	kIx,
Igor	Igor	k1gMnSc1
Astapkovič	Astapkovič	k1gMnSc1
•	•	k?
1994	#num#	k4
<g/>
:	:	kIx,
Vasilij	Vasilij	k1gMnSc1
Sidorenko	Sidorenka	k1gFnSc5
•	•	k?
1998	#num#	k4
<g/>
:	:	kIx,
Tibor	Tibor	k1gMnSc1
Gécsek	Gécska	k1gFnPc2
•	•	k?
2002	#num#	k4
<g/>
:	:	kIx,
Adrián	Adrián	k1gMnSc1
Annus	Annus	k1gMnSc1
•	•	k?
2006	#num#	k4
<g/>
:	:	kIx,
Ivan	Ivan	k1gMnSc1
Tichon	Tichona	k1gFnPc2
•	•	k?
2010	#num#	k4
<g/>
:	:	kIx,
Libor	Libor	k1gMnSc1
Charfreitag	Charfreitaga	k1gFnPc2
•	•	k?
2012	#num#	k4
<g/>
:	:	kIx,
Krisztián	Krisztián	k1gMnSc1
Pars	Parsa	k1gFnPc2
•	•	k?
2014	#num#	k4
<g/>
:	:	kIx,
Krisztián	Krisztián	k1gMnSc1
Pars	Parsa	k1gFnPc2
•	•	k?
2016	#num#	k4
<g/>
:	:	kIx,
Paweł	Paweł	k1gFnSc1
Fajdek	Fajdek	k1gInSc1
•	•	k?
2018	#num#	k4
<g/>
:	:	kIx,
Wojciech	Wojciech	k1gInSc1
Nowicki	Nowick	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
