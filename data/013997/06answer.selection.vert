<s>
Československé	československý	k2eAgFnPc1d1
státní	státní	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
(	(	kIx(
<g/>
ČSD	ČSD	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
státní	státní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zabývala	zabývat	k5eAaImAgFnS
správou	správa	k1gFnSc7
železniční	železniční	k2eAgFnSc2d1
infrastruktury	infrastruktura	k1gFnSc2
a	a	k8xC
provozováním	provozování	k1gNnSc7
železniční	železniční	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
celého	celý	k2eAgNnSc2d1
tehdejšího	tehdejší	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
a	a	k8xC
provozovala	provozovat	k5eAaImAgNnP
dopravu	doprava	k1gFnSc4
i	i	k9
na	na	k7c6
několika	několik	k4yIc6
soukromých	soukromý	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
<g/>
.	.	kIx.
</s>